
#include "Intern.hpp"

#include "DiskManager.hpp"

#include "MasterBootRecord.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Disk Manager Object
//

// Instantiator

IDiskManager * Create_DiskManager(UINT uPriority)
{
	CDiskManager *p = New CDiskManager(uPriority);

	return p;
	}

// Constructor

CDiskManager::CDiskManager(UINT uPriority)
{
	StdSetRef();

	piob->RegisterSingleton("fs.diskman", 0, (IDiskManager *) this);

	m_pVolMan = NULL;

	AfxGetObject("volman", 0, IVolumeManager, m_pVolMan);
	
	m_pMutex  = Create_Mutex();

	m_pEvent  = Create_AutoEvent();

	m_pThread = CreateThread(TaskDiskManager, uPriority, this, 0);

	Init();

	AddDiagCmds();
	}

// Destructor

CDiskManager::~CDiskManager(void)
{
	DestroyThread(m_pThread);

	m_pMutex->Release();

	m_pEvent->Release();
	}

// IUnknown

HRESULT CDiskManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiskManager);

	StdQueryInterface(IDiskManager);

	return E_NOINTERFACE;
	}

ULONG CDiskManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CDiskManager::Release(void)
{
	StdRelease();
	}

// IDiskManager

UINT CDiskManager::RegisterHost(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		m_pMutex->Wait(FOREVER);
		
		for( UINT i = 0; i < elements(Disk.m_HostList); i ++ ) {

			CHost &Host = Disk.m_HostList[i];

			if( !Host.m_uActive ) {

				Host.m_uActive    = 1;

				Host.m_iDisk      = NOTHING;

				Host.m_uFlags     = 0;

				Host.m_nLockRead  = 0;

				Host.m_nLockWrite = 0;

				Host.m_pPrev      = NULL;

				Host.m_pNext      = NULL;

				Host.m_pLockWrite = Create_ManualEvent();

				Disk.m_uHostCount ++;

				m_pMutex->Free();

				return i;
				}
			}

		m_pMutex->Free();
		}

	return NOTHING;
	}

void CDiskManager::UnregisterHost(UINT iDisk, UINT iHost)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		if( iHost < elements(Disk.m_HostList) ) {
			
			CHost &Host = Disk.m_HostList[iHost];

			if( Host.m_uActive ) {
				
				m_pMutex->Wait(FOREVER);
				
				Host.m_uActive    = 0;

				Host.m_uFlags     = 0;

				Host.m_nLockRead  = 0;

				Host.m_nLockWrite = 0;

				Host.m_iDisk      = NOTHING;

				Host.m_pLockWrite->Release();

				Disk.m_uHostCount --;

				m_pMutex->Free();
				}
			}
		}
	}

UINT CDiskManager::RegisterDisk(IBlockDevice *pDevice)
{
	m_pMutex->Wait(FOREVER);

	for( UINT i = 0; i < elements(m_DiskList); i ++ ) {

		CDisk   &Disk = m_DiskList[i];

		CVolume &Vol  = Disk.m_VolList[0];

		if( Disk.m_pDev == NULL ) {

			Disk.m_pDev	  = pDevice;

			Disk.m_pCache	  = Create_BlockCache(pDevice, cacheWriteThrough);

			Disk.m_uFlags	  = 0;
			
			Disk.m_uVolCount  = 0;
			
			Disk.m_uHostCount = 0;

			Disk.m_nLockCount = 0;

			Disk.m_uLockFlags = 0;

			Disk.m_pLockHead  = NULL;

			Disk.m_pLockTail  = NULL;

			Disk.m_pSemaphore = Create_Semaphore();

			Disk.m_pMutex	  = Create_Mutex();

			Disk.m_pLockRead  = Create_ManualEvent();

			Disk.m_pSemaphore->Signal(1);

			Vol.m_cName       = AllocMap(Disk.m_iIndex, 0);

			m_uDiskCount++;

			m_pMutex->Free();

			pDevice->Attach(m_pEvent);
		
			return i;
			}
		}

	m_pMutex->Free();

	return NOTHING;
	}

UINT CDiskManager::FindDisk(CHAR cDrive)
{
	return MapDrive(cDrive);
	}

UINT CDiskManager::FindVolume(CHAR cDrive)
{
	UINT iDisk = MapDrive(cDrive);

	UINT iVol  = MapVolume(cDrive);

	return GetVolumeIndex(iDisk, iVol);
	}

bool CDiskManager::CheckDisk(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		ClrFlags(Disk, flagEjectPend | flagEjectDone);

		m_pEvent->Set();

		return true;
		}		

	return false;
	}

bool CDiskManager::EjectDisk(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		SetFlags(Disk, flagEjectPend);

		ClrFlags(Disk, flagEjectDone);

		m_pEvent->Set();

		return true;
		}		

	return false;
	}

bool CDiskManager::FormatDisk(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		SetFlags(Disk, flagFormat);

		m_pEvent->Set();

		return true;
		}		

	return false;
	}

UINT CDiskManager::GetDiskCount(void)
{
	return m_uDiskCount;
	}

IBlockDevice * CDiskManager::GetDisk(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk]; 

		return Disk.m_pDev;
		}		

	return NULL;
	}

IBlockCache * CDiskManager::GetDiskCache(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		return Disk.m_pCache;
		}		

	return NULL;
	}

bool CDiskManager::GetDiskReady(UINT iDisk)
{
	switch( GetDiskStatus(iDisk) ) {

		case diskEmpty:
		case diskFormat:
		case diskCheck:
		case diskInvalid:
		
			return false;

		case diskReady:
		case diskLocked:

			return true;
		}
	
	return false;
	}

UINT CDiskManager::GetDiskStatus(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		if( !(Disk.m_uFlags & flagPresent) ) {

			return diskEmpty;
			}

		if( Disk.m_uFlags & (flagEjectPend | flagEjectDone) ) {

			return diskEmpty;
			}

		if( Disk.m_uFlags & flagFormat ) {

			return diskFormat;
			}
				
		if( Disk.m_uFlags & flagCheck ) {

			return diskCheck;
			}

		if( Disk.m_uLockFlags & (diskLockRead | diskLockWrite) ) {

			return diskLocked;
			}

		if( Disk.m_uVolCount ) {

			if( m_pVolMan->IsVolumeMounted(Disk.m_VolList[0].m_iIndex) ) {

				return diskReady;
				}
			}

		return diskInvalid;
		}

	return diskEmpty;
	}

UINT CDiskManager::GetVolumeCount(UINT iDisk)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		return Disk.m_uVolCount;
		}		

	return 0;
	}

UINT CDiskManager::GetVolumeIndex(UINT iDisk, UINT n)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		if( n < Disk.m_uVolCount ) {

			CVolume &Volume = Disk.m_VolList[n];

			return Volume.m_iIndex;
			}
		}		

	return NOTHING;
	}

bool CDiskManager::WaitDiskLock(UINT iHost, UINT iDisk, UINT uType, UINT uWait)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		if( iHost < elements(Disk.m_HostList) ) {

			CHost &Host = Disk.m_HostList[iHost];

			return WaitDiskLock(Disk, Host, uType, uWait);
			}
		}

	return false;
	}

bool CDiskManager::FreeDiskLock(UINT iHost, UINT iDisk, UINT uType)
{
	if( iDisk < m_uDiskCount ) {

		CDisk &Disk = m_DiskList[iDisk];

		if( iHost < elements(Disk.m_HostList) ) {

			CHost &Host = Disk.m_HostList[iHost];

			return FreeDiskLock(Disk, Host, uType);
			}
		}

	return false;
	}

// IDiagProvider

UINT CDiskManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}

// Task Entry

void CDiskManager::TaskEntry(void)
{
	for(;;) {

		m_pEvent->Wait(FOREVER);

		for( UINT i = 0; i < elements(m_DiskList); i ++ ) {

			CDisk &Disk = m_DiskList[i];

			if( Disk.m_pDev != NULL ) {

				if( Disk.m_uFlags & flagEjectPend ) {

					UnmountVolumes(Disk);

					SetFlags(Disk, flagEjectDone);

					ClrFlags(Disk, flagEjectPend);
					}

				if( Disk.m_uFlags & flagFormat ) {

					UnmountVolumes(Disk);

					FormatVolumes(Disk);

					ClrFlags(Disk, flagFormat);
					}

				if( Disk.m_pDev->IsReady() ) {

					if( Disk.m_uFlags & flagEjectDone ) {

						continue;
						}

					MountVolumes(Disk);

					SetFlags(Disk, flagPresent);
					}
				else { 
					UnmountVolumes(Disk);

					ClrFlags(Disk, flagPresent | flagEjectDone);
					}
				}
			}
		}
	}

// Init

void CDiskManager::Init(void)
{
	memset(&m_DiskList, 0, sizeof(m_DiskList));

	memset(&m_DriveMap, 0, sizeof(m_DriveMap));

	m_uDiskCount = 0;

	for( UINT iDisk = 0; iDisk < elements(m_DiskList); iDisk ++ ) {

		CDisk &Disk = m_DiskList[iDisk];

		Disk.m_iIndex = iDisk;

		for( UINT iHost = 0; iHost < elements(Disk.m_HostList); iHost ++ ) {

			CHost &Host = Disk.m_HostList[iHost];

			Host.m_iIndex = iHost;
			}
		}
	}

// Flags

void CDiskManager::SetFlags(CDisk &Disk, UINT uFlags)
{
	m_pMutex->Wait(FOREVER);

	Disk.m_uFlags |= uFlags;

	m_pMutex->Free();
	}

void CDiskManager::ClrFlags(CDisk &Disk, UINT uFlags)
{
	m_pMutex->Wait(FOREVER);

	Disk.m_uFlags &= ~uFlags;

	m_pMutex->Free();
	}

// Volumes

UINT CDiskManager::MapDrive(CHAR cDisk) const
{
	UINT iMap = (cDisk & ~0x20) - 'A';

	if( iMap < elements(m_DriveMap) ) {

		return m_DriveMap[iMap].m_Disk;
		}

	return NOTHING;
	}

UINT CDiskManager::MapVolume(CHAR cDisk) const
{
	UINT iMap = (cDisk & ~0x20) - 'A';

	if( iMap < elements(m_DriveMap) ) {

		return m_DriveMap[iMap].m_Vol;
		}

	return NOTHING;
	}

CHAR CDiskManager::AllocMap(UINT iDisk, UINT iVolume)
{
	for( UINT i = 2; i < elements(m_DriveMap); i ++ ) {

		CMap &Map = m_DriveMap[i];

		if( !Map.m_Used ) {

			Map.m_Disk = iDisk;

			Map.m_Vol  = iVolume;

			Map.m_Used = true;

			return CHAR('A' + i);
			}
		}

	return NULL;
	}

void CDiskManager::FreeMap(CHAR cName)
{
	UINT iMap = (cName & ~0x20) - 'A';

	if( iMap < elements(m_DriveMap) ) {

		CMap &Map = m_DriveMap[iMap];

		Map.m_Used = false;
		}
	}

void CDiskManager::FormatVolumes(CDisk &Disk)
{
	if( !Disk.m_uVolCount ) {

		Disk.m_pSemaphore->Wait(FOREVER);

		PBYTE pData = Disk.m_pCache->LockSector(0, FALSE);

		if( pData ) {

			CMasterBootRecord *pBoot = (CMasterBootRecord *) pData;

			pBoot->LocalToHost();

			if( pBoot->IsValid() ) {

				for( UINT n = 0; n < elements(pBoot->m_Partitions); n ++ ) {

					CPartitionEntry const &Partition = pBoot->GetPartition(n);

					Partition.Dump();

					if( !Partition.IsValid() ) {
						
						continue;
						}
					
					CVolume &Volume = Disk.m_VolList[Disk.m_uVolCount];

					if( !Volume.m_cName ) {

						Volume.m_cName = AllocMap(Disk.m_iIndex, Disk.m_uVolCount);
						}

					UINT iVolume = m_pVolMan->FormatVolume(Disk.m_iIndex, Volume.m_cName, Partition.m_dwRelativeSector, Partition.m_dwTotalSectors);

					if( iVolume != NOTHING ) {

						Volume.m_iIndex = iVolume;

						Disk.m_uVolCount++;
						}
					}

				pBoot->HostToLocal();
				}
			else {
				pBoot->HostToLocal();
				
				CVolume &Volume = Disk.m_VolList[Disk.m_uVolCount];

				if( !Volume.m_cName ) {

					Volume.m_cName = AllocMap(Disk.m_iIndex, Disk.m_uVolCount);
					}

				UINT iVolume = m_pVolMan->FormatVolume(Disk.m_iIndex, Volume.m_cName, 0, Disk.m_pDev->GetSectorCount());

				if( iVolume != NOTHING ) {

					Volume.m_iIndex = iVolume;

					Disk.m_uVolCount++;
					}
				}

			Disk.m_pCache->UnlockSector(0, false);
			}

		Disk.m_pSemaphore->Signal(1);
		}
	}

void CDiskManager::MountVolumes(CDisk &Disk)
{
	if( !Disk.m_uVolCount ) {

		Disk.m_pSemaphore->Wait(FOREVER);

		PBYTE pData = Disk.m_pCache->LockSector(0, FALSE);

		if( pData ) {

			CMasterBootRecord *pBoot = (CMasterBootRecord *) pData;

			pBoot->LocalToHost();

			if( pBoot->IsValid() ) {

				for( UINT n = 0; n < elements(pBoot->m_Partitions); n ++ ) {

					CPartitionEntry const &Partition = pBoot->GetPartition(n);

					Partition.Dump();

					if( !Partition.IsValid() ) {
						
						continue;
						}
					
					CVolume &Volume = Disk.m_VolList[Disk.m_uVolCount];

					if( !Volume.m_cName ) {

						Volume.m_cName = AllocMap(Disk.m_iIndex, Disk.m_uVolCount);
						}

					UINT iVolume = m_pVolMan->MountVolume(Disk.m_iIndex, Volume.m_cName, Partition.m_dwRelativeSector);

					if( iVolume != NOTHING ) {

						Volume.m_iIndex = iVolume;

						Disk.m_uVolCount++;
						}
					}

				pBoot->HostToLocal();
				}
			else {
				pBoot->HostToLocal();
				
				CVolume &Volume = Disk.m_VolList[Disk.m_uVolCount];

				if( !Volume.m_cName ) {

					Volume.m_cName = AllocMap(Disk.m_iIndex, Disk.m_uVolCount);
					}

				UINT iVolume = m_pVolMan->MountVolume(Disk.m_iIndex, Volume.m_cName, 0);

				if( iVolume != NOTHING ) {

					Volume.m_iIndex = iVolume;

					Disk.m_uVolCount++;
					}
				}

			Disk.m_pCache->UnlockSector(0, false);
			}

		Disk.m_pSemaphore->Signal(1);
		}
	}

void CDiskManager::UnmountVolumes(CDisk &Disk)
{
	if( Disk.m_uVolCount ) {

		Disk.m_pSemaphore->Wait(FOREVER);

		for( UINT i = 0; i < Disk.m_uVolCount; i ++ ) {

			CVolume &Volume = Disk.m_VolList[i];

			m_pVolMan->UnMountVolume(Volume.m_iIndex);
			}

		Disk.m_uVolCount = 0;

		Disk.m_pSemaphore->Signal(1);
		}
	}

// Disk Locks

bool CDiskManager::WaitDiskLock(CDisk &Disk, CHost &Host, UINT uType, UINT uWait)
{
	switch( uType ) {

		case diskLockRead:
						
			return WaitReadLock(Disk, Host, uWait);

		case diskLockWrite:

			return WaitWriteLock(Disk, Host, uWait);
		}

	return false;
	}

bool CDiskManager::FreeDiskLock(CDisk &Disk, CHost &Host, UINT uType)
{
	switch( uType ) {

		case diskLockRead:

			return FreeReadLock(Disk, Host);

		case diskLockWrite:

			return FreeWriteLock(Disk, Host);
		}

	return false;
	}

bool CDiskManager::WaitWriteLock(CDisk &Disk, CHost &Host, UINT uWait)
{
	Debug("WaitWriteLock(Disk=%d, Host=%d, Wait=%d) - Request", Disk.m_iIndex, Host.m_iIndex, uWait);

	for(;;) {
	
		Disk.m_pMutex->Wait(FOREVER);

		Host.m_uFlags |= flagPromote;

		if( !(Disk.m_uLockFlags & (flagPromote | diskLockWrite)) ) { 

			if( Disk.m_nLockCount == 1 && Disk.m_pLockHead == &Host ) {

				Host.m_uFlags     &= ~flagPromote;

				Disk.m_uLockFlags |= flagPromote;

				Disk.m_uLockFlags &= ~diskLockRead;

				Disk.m_pLockRead->Clear();

				Host.m_pLockWrite->Set();
				}
			}
		
		Disk.m_pMutex->Free();
			
		UINT uObject = WaitMultiple(Disk.m_pSemaphore, Host.m_pLockWrite, uWait);

		if( uObject != 0 ) {
			
			Disk.m_pMutex->Wait(FOREVER);

			AfxAssert(Host.m_nLockWrite >= 0);
			
			if( uObject == 1 ) {

				Host.m_pLockWrite->Set();
				}

			if( uObject == 2 ) {

				if( !(Disk.m_uLockFlags & (diskLockWrite | flagPromote)) ) {

					Disk.m_pMutex->Free();

					continue;
					}
				}

			Host.m_uFlags &= ~flagPromote; 

			if( !Host.m_nLockWrite++ ) {

				Disk.m_uLockFlags |= diskLockWrite;

				if( Host.m_iDisk == NOTHING ) {

					Host.m_iDisk = Disk.m_iIndex;

					CHost *pHost = &Host;

					AfxListAppend(Disk.m_pLockHead, Disk.m_pLockTail, pHost, m_pNext, m_pPrev);
					}
				}

			Debug("WaitWriteLock(Disk=%d, Host=%, dWait=%d) - Locked(W%d, R%d)", Disk.m_iIndex, Host.m_iIndex, uWait, Host.m_nLockWrite, Host.m_nLockRead);
			
			Disk.m_pMutex->Free();

			return true;
			}

		Debug("WaitWriteLock(Disk=%d, Host=%d, Wait=%d) - Timeout", Disk.m_iIndex, Host.m_iIndex, uWait);
	
		return false;
		}
	}

bool CDiskManager::WaitReadLock(CDisk &Disk, CHost &Host, UINT uWait)
{
	Debug("WaitReadLock(Disk=%d, Host=%d, Wait=%d) - Request", Disk.m_iIndex, Host.m_iIndex, uWait);

	for(;;) {

		UINT uObject = WaitMultiple(Disk.m_pSemaphore, Disk.m_pLockRead, Host.m_pLockWrite, uWait);

		if( uObject != 0 ) {

			Disk.m_pMutex->Wait(FOREVER);

			AfxAssert(Host.m_nLockRead >= 0);

			if( uObject == 1 ) {

				Disk.m_pLockRead->Set();
				}

			if( uObject == 2 ) {

				if( !(Disk.m_uLockFlags & diskLockRead) ) {

					Disk.m_pMutex->Free();

					continue;
					}
				}

			if( uObject == 3 ) {

				if( !(Disk.m_uLockFlags & diskLockWrite) ) {

					Disk.m_pMutex->Free();

					continue;
					}
				}

			if( !Host.m_nLockRead++ ) {

				Disk.m_uLockFlags |= diskLockRead;

				if( Host.m_iDisk == NOTHING ) {

					Host.m_iDisk = Disk.m_iIndex;

					CHost *pHost = &Host;

					AfxListAppend(Disk.m_pLockHead, Disk.m_pLockTail, pHost, m_pNext, m_pPrev);
					}
				
				Disk.m_nLockCount ++;
				}

			Debug("WaitReadLock(Disk=%d, Host=%d, Wait=%d) - Locked(W%d, R%d)", Disk.m_iIndex, Host.m_iIndex, uWait, Host.m_nLockWrite, Host.m_nLockRead);
			
			Disk.m_pMutex->Free();

			return true;
			}

		Debug("WaitReadLock(Disk=%d, Host=%d, Wait=%d) - Timeout", Disk.m_iIndex, Host.m_iIndex, uWait);

		return false;
		}
	}

bool CDiskManager::FreeWriteLock(CDisk &Disk, CHost &Host)
{
	Disk.m_pMutex->Wait(FOREVER);

	Debug("FreeWriteLock(Disk=%d, Host=%d) - Request(W%d, R%d)", Disk.m_iIndex, Host.m_iIndex, Host.m_nLockWrite, Host.m_nLockRead);

	AfxAssert(Host.m_nLockWrite > 0);

	if( !--Host.m_nLockWrite ) {

		Disk.m_uLockFlags &= ~diskLockWrite;

		if( !Host.m_nLockRead && Host.m_iDisk != NOTHING ) {

			Host.m_iDisk = NOTHING;

			CHost *pHost = &Host;
	
			AfxListRemove(Disk.m_pLockHead, Disk.m_pLockTail, pHost, m_pNext, m_pPrev);
			}

		if( Disk.m_uLockFlags & flagPromote ) {

			Disk.m_uLockFlags &= ~flagPromote;

			if( Disk.m_nLockCount ) {

				Disk.m_pLockRead->Set();

				Disk.m_uLockFlags |= diskLockRead;
				}
			}

		Host.m_pLockWrite->Clear();

		if( !(Disk.m_uLockFlags & diskLockRead) ) {  

			Disk.m_pLockRead->Clear();
	
			Disk.m_pSemaphore->Signal(1);

			Debug("FreeWriteLock(Disk=%d, Host=%d) - Unlocked", Disk.m_iIndex, Host.m_iIndex);
			}
		else {
			Disk.m_pLockRead->Set();
			}
		}

	Disk.m_pMutex->Free();

	return true;
	}

bool CDiskManager::FreeReadLock(CDisk &Disk, CHost &Host)
{
	Disk.m_pMutex->Wait(FOREVER);

	Debug("FreeReadLock(Disk=%d, Host=%d) - Request(W%d, R%d)", Disk.m_iIndex, Host.m_iIndex, Host.m_nLockWrite, Host.m_nLockRead);

	AfxAssert(Host.m_nLockRead > 0);

	if( !--Host.m_nLockRead ) {

		AfxAssert(Disk.m_nLockCount > 0);

		if( !Host.m_nLockWrite && Host.m_iDisk != NOTHING ) {

			Host.m_iDisk = NOTHING;

			CHost *pHost = &Host;
	
			AfxListRemove(Disk.m_pLockHead, Disk.m_pLockTail, pHost, m_pNext, m_pPrev);
			}
	
		Disk.m_nLockCount --;

		if( Disk.m_nLockCount == 1 ) {

			CHost *pHost = Disk.m_pLockHead;

			if( pHost->m_uFlags & flagPromote ) {

				pHost->m_uFlags   &= ~flagPromote;

				Disk.m_uLockFlags |= flagPromote;

				Disk.m_uLockFlags &= ~diskLockRead;

				Disk.m_pLockRead->Clear();

				pHost->m_pLockWrite->Set();
				}
			}

		if( Disk.m_nLockCount == 0 ) {

			Disk.m_uLockFlags &= ~diskLockRead;

			Disk.m_pLockRead->Clear();

			if( !(Disk.m_uLockFlags & (diskLockWrite | flagPromote)) ) {  

				Host.m_pLockWrite->Clear();
				
				Disk.m_pSemaphore->Signal(1);

				Debug("FreeReadLock(Disk=%d, Host=%d) - Unlocked", Disk.m_iIndex, Host.m_iIndex);
				}
			}
		}
		
	Disk.m_pMutex->Free();

	return true;
	}

// Diagnostics

BOOL CDiskManager::AddDiagCmds(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		UINT uProv = pDiag->RegisterProvider(this, "disk");

		pDiag->RegisterCommand(uProv, 1, "status");

		pDiag->Release();

		return true;
		}

	#endif

	return false;
	}

UINT CDiskManager::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(8);

		pOut->SetColumn(0, "Index",   "%u");
		pOut->SetColumn(1, "Host",    "%u");
		pOut->SetColumn(2, "Volumes", "%u");
		pOut->SetColumn(3, "Flags",   "%s");
		pOut->SetColumn(4, "Locks",   "%u");
		pOut->SetColumn(5, "State",   "%s");
		pOut->SetColumn(6, "RD Lock", "%u");
		pOut->SetColumn(7, "WR Lock", "%u");

		pOut->AddHead();

		pOut->AddRule('-');

		char sFlags[2][8];

		for( UINT i = 0; i < elements(m_DiskList); i ++ ) {

			CDisk   &Disk = m_DiskList[i];

			CVolume &Vol  = Disk.m_VolList[0];

			pOut->AddRow();

			pOut->SetData(0, Disk.m_iIndex);
			
			if( Disk.m_pDev != NULL ) {
						
				sFlags[0][0] = 0;

				strcat(sFlags[0], Disk.m_uFlags & flagPresent   ? "A" : ".");

				strcat(sFlags[0], Disk.m_uFlags & flagEjectPend ? "E" : ".");

				strcat(sFlags[0], Disk.m_uFlags & flagEjectDone ? "X" : ".");

				sFlags[1][0] = 0;

				strcat(sFlags[1], Disk.m_uLockFlags & diskLockRead ?  "R" : ".");

				strcat(sFlags[1], Disk.m_uLockFlags & diskLockWrite ? "W" : ".");

				pOut->SetData(2, Disk.m_uVolCount);

				pOut->SetData(3, sFlags[0]);
				
				pOut->SetData(4, Disk.m_nLockCount);
				
				pOut->SetData(5, sFlags[1]);
				}

			pOut->EndRow();

			for( UINT n = 0; n < elements(Disk.m_HostList); n ++ ) {
	
				CHost const &Host = Disk.m_HostList[n];
				
				if( Host.m_uActive ) {

					pOut->AddRow();
		
					pOut->SetData(1, Host.m_iIndex);

					pOut->SetData(6, Host.m_nLockRead);

					pOut->SetData(7, Host.m_nLockWrite);

					pOut->EndRow();
					}
				}
			}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

// Diagnostics

void CDiskManager::Debug(PCTXT pFormat, ...) const
{
	#if defined(_XDEBUG)
	
	va_list pArgs;

	va_start(pArgs, pFormat);

	char sText[256];

	vsprintf(sText, pFormat, pArgs);

	AfxTrace("Disk Manager   : %-8.8s : ", GetCurrentThread()->GetName());

	AfxTrace(sText);

	AfxTrace("\n");
	
	#endif
	}

// Task Entry

int CDiskManager::TaskDiskManager(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("DiskManager");

	((CDiskManager *) pParam)->TaskEntry();

	return 0;
	}

// End of File
