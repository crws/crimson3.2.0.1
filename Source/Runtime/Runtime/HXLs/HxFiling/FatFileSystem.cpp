
#include "Intern.hpp"

#include "FatFileSystem.hpp"

#include "FatDirName.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat File System
//

// Constructor 

CFatFileSystem::CFatFileSystem(UINT iDisk, UINT iVolume, UINT uBoot) : CFilingSystem(iDisk, iVolume)
{
	m_uFirst	= uBoot;

	m_uData		= NOTHING;

	m_uFat0		= NOTHING;

	m_uFat1		= NOTHING;

	m_uRoot		= NOTHING;

	m_uRootSize	= NOTHING;

	m_uSectorSize	= NOTHING;

	m_uClusterSize	= NOTHING;

	m_pMutex        = Create_Mutex();

	InitFileHandles();
	}

// Destructor

CFatFileSystem::~CFatFileSystem(void)
{
	m_pMutex->Wait(FOREVER);

	m_pMutex->Free();
	}

// Enumeration

BOOL CFatFileSystem::GetFirst(FSIndex &Index)
{
	if( AssertDataIndex(Index) ) {

		MakeRefIndex(Index);

		return IsValid(Index) || LocateNext(Index);
		}

	return false;
	}

BOOL CFatFileSystem::GetNext(FSIndex &Index)
{
	if( AssertRefIndex(Index) ) {

		return LocateNext(Index);
		}

	return false;
	}

BOOL CFatFileSystem::GetName(FSIndex const &Index, CFilename &Filename)
{
	if( AssertRefIndex(Index) ) {

		return LookupName(Index, Filename);
		}

	return false;
	}

BOOL CFatFileSystem::GetPath(FSIndex const &Index, CFilename &Filename)
{
	FSIndex Dir;

	Dir.m_iCluster = Index.m_iCluster;

	Dir.m_iIndex   = Index.m_iIndex;

	while( !IsRoot(Dir) ) {

		CFilename ThisName;

		UINT iData     = Dir.m_iCluster;

		Dir.m_iCluster = ParentDir(Dir);

		MakeDataIndex(Dir);

		if( LookupData(Dir, iData, ThisName) ) {

			Filename = ThisName + Filename;
			
			continue;
			}

		return false;
		}

	if( Filename.IsEmpty() ) {
		
		Filename = "\\";
		}

	return true;
	}

time_t CFatFileSystem::GetUnixTime(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.GetUnixTime();
			}
		}

	return 0;
	}

UINT CFatFileSystem::GetPackedTime(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.GetPackedTime();
			}
		}

	return 0;
	}

UINT CFatFileSystem::GetSize(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.m_dwSize;
			}
		}

	return 0;
	}

BOOL CFatFileSystem::FindName(CFilename &Name, FSIndex &Index)
{
	return LocateName(Name, Index);
	}

BOOL CFatFileSystem::IsValid(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( ReadEntry(Index, Entry) ) {

			return !Entry.IsEmpty() && !Entry.IsFree();
			}
		}

	return false;
	}

BOOL CFatFileSystem::IsDirectory(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.IsDirectory();
			}
		}

	return false;
	}

BOOL CFatFileSystem::IsFile(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.IsFile();
			}
		}

	return false;
	}

BOOL CFatFileSystem::IsVolume(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.IsVolume();
			}
		}

	return false;
	}

BOOL CFatFileSystem::IsSpecial(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.IsSpecial();
			}
		}

	return false;
	}

BOOL CFatFileSystem::GetArchive(FSIndex const &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( LocateShort(Index, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			return Short.IsArchive();
			}
		}

	return false;
	}

// Directory

BOOL CFatFileSystem::LocateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	Debug(debugFile, "LocateDir(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	if( AssertDataIndex(Dir) ) {
	
		FSIndex iScan;

		iScan.m_iCluster = Dir.m_iCluster;

		iScan.m_iIndex   = Dir.m_iIndex;

		Index.m_iCluster = Dir.m_iCluster;

		Index.m_iIndex   = Dir.m_iIndex;

		DiskWaitReadLock();

		char szName[256];

		for( BOOL fOk = Name.MoveTop(szName); fOk; fOk = Name.MoveDown(szName) ) {

			if( !szName[0] ) {

				GetRoot(iScan);

				GetRoot(Index);

				continue;
				}

			if( LocateName(CFilename(szName), iScan) ) {

				Index.m_iCluster = iScan.m_iCluster;

				Index.m_iIndex   = iScan.m_iIndex;

				if( MoveDir(Index, iScan) ) {

					continue;
					}
				}

			DiskFreeReadLock();

			return false;
			}

		DiskFreeReadLock();
		
		return true;
		}

	return false;
	}

BOOL CFatFileSystem::CreateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	Debug(debugFile, "CreateDir(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	if( AssertDataIndex(Dir) ) {

		DiskWaitWriteLock();

		FSIndex iScan;

		iScan.m_iCluster = Dir.m_iCluster;

		iScan.m_iIndex   = Dir.m_iIndex;

		char szName[256];

		for( BOOL fOk = Name.MoveTop(szName); fOk; fOk = Name.MoveDown(szName) ) {

			if( !szName[0] ) {

				GetRoot(iScan);

				GetRoot(Index);

				continue;
				}

			HFILE hParent = OpenHandle(iScan.m_iCluster);

			if( hParent != NULL ) {

				WaitWriteLock(hParent);

				CFilename Filename(szName);
				
				if( LocateName(Filename, iScan) ) {

					Index.m_iCluster = iScan.m_iCluster;

					Index.m_iIndex   = iScan.m_iIndex;

					FreeWriteLock(hParent);

					CloseHandle(hParent);

					if( MoveDir(Index, iScan) ) {

						continue;
						}

					DiskFreeWriteLock();

					Debug(debugWarn, "CreateDir(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Dir.m_iCluster);
			
					return false;
					}

				if( !LocateName(Filename, iScan) && CreateName(Filename, iScan) ) {

					CFatDirEntry NameEntry;

					Index.m_iCluster = iScan.m_iCluster;

					Index.m_iIndex   = iScan.m_iIndex;

					if( LocateShort(iScan, NameEntry) ) {

						UINT iCluster = AllocData(m_uClusterSize, true);

						if( iCluster != entryInvalid ) {

							CFatDirShort &NameShort = NameEntry.GetShort();
						
							HFILE hCluster = OpenHandle(iCluster);

							if( hCluster != NULL ) {

								WaitWriteLock(hCluster);

								NameShort.MakeDirectory();

								NameShort.SetFirstCluster(iCluster);

								if( WriteEntry(iScan, NameEntry) ) {

									CFatDirEntry Dot1, Dot2;

									Dot1.GetShort().CreateDot(NameEntry);

									Dot2.GetShort().CreateDot2(NameEntry, iScan.m_iCluster == GetRootCluster() ? 0 : iScan.m_iCluster);

									if( WriteEntry(iCluster, 0, Dot1) && WriteEntry(iCluster, 1, Dot2) ) {

										FreeWriteLock(hCluster);

										FreeWriteLock(hParent);

										CloseHandle(hCluster);

										CloseHandle(hParent);

										iScan.m_iCluster = iCluster;

										iScan.m_iIndex   = UINT(indexCluster);

										continue;
										}
									}
						
								FreeWriteLock(hCluster);

								CloseHandle(hCluster);
								}

							FreeData(iCluster);
							}
						}
					}

				FreeWriteLock(hParent);

				CloseHandle(hParent);
				}

			DiskFreeWriteLock();

			Debug(debugWarn, "CreateDir(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Dir.m_iCluster);
			
			return false;
			}	

		DiskFreeWriteLock();

		return true;
		}

	return false;
	}

BOOL CFatFileSystem::MoveDir(FSIndex const &Dir, FSIndex &Index)
{
	Debug(debugFile, "MoveDir(Dir=0x%8.8X, Entry=0x%8.8X)", Dir.m_iCluster, Dir.m_iIndex);

	if( IsRoot(Dir) && IsDataIndex(Dir) ) {

		GetRoot(Index);

		return true;
		}

	if( AssertRefIndex(Dir) ) {

		DiskWaitReadLock();

		CFatDirEntry Entry;

		if( ReadEntry(Dir, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			CFatDirLong  &Long  = Entry.GetLong();

			Index.m_iCluster = Dir.m_iCluster;

			Index.m_iIndex   = Dir.m_iIndex;

			if( Entry.IsLong() ) {

				UINT iIndex = Dir.m_iIndex + Long.GetOrder();

				ReadEntry(Dir.m_iCluster, iIndex, Entry);
				}

			if( Short.IsDirectory() ) {

				MakeDataIndex(Index);

				Index.m_iCluster = Short.GetFirstCluster();

				if( Index.m_iCluster == rootEntry ) { 

					GetRoot(Index);
					}
				}
				
			DiskFreeReadLock();

			return true;
			}

		DiskFreeReadLock();
		}

	Debug(debugWarn, "MoveDir(Dir=0x%8.8X, Entry=0x%8.8X) - FAILED", Dir.m_iCluster, Dir.m_iIndex);

	return false;
	}

UINT CFatFileSystem::ScanDir(FSIndex const &Dir, FSIndex *&pList, UINT uFlags)
{
	Debug(debugFile, "ScanDir(Dir=0x%8.8X, Entry=0x%8.8X, Flags=0x%4.4X)", Dir.m_iCluster, Dir.m_iIndex, uFlags);

	pList = NULL;

	if( AssertDataIndex(Dir) ) {

		DiskWaitReadLock();

		HFILE hCluster = OpenHandle(Dir.m_iCluster);

		if( hCluster != NULL ) {

			UINT uCount = 0;

			WaitReadLock(hCluster);

			for( UINT nPass = 0; nPass < 2; nPass ++ ) {

				uCount = 0;

				FSIndex iScan;

				MakeDataIndex(iScan);

				iScan.m_iCluster = Dir.m_iCluster;

				for( BOOL fOk = GetFirst(iScan); fOk; fOk = GetNext(iScan) ) {

					if( (uFlags & (scanAll | scanFile)) == scanFile ) {

						if( !IsFile(iScan) ) {

							continue;
							}
						}

					if( (uFlags & (scanAll | scanDir)) == scanDir ) {

						if( !IsDirectory(iScan) ) {

							continue;
							}
						}
						
					if( pList ) {

						pList[uCount].m_iCluster = iScan.m_iCluster;

						pList[uCount].m_iIndex   = iScan.m_iIndex;
						}

					uCount++;
					}

				if( !nPass && uCount ) {

					pList = New FSIndex[ uCount ];
					}
				}

			if( uFlags & (scanOrder | scanReverse) ) {

				SortList(pList, uCount, uFlags);
				}

			FreeReadLock(hCluster);
	
			CloseHandle(hCluster);
				
			DiskFreeReadLock();

			return uCount;
			}			
	
		DiskFreeReadLock();
		}

	return 0;
	}

BOOL CFatFileSystem::RenameDir(CFilename const &Name, FSIndex &Index)
{
	Debug(debugFile, "RenameDir(Name='%s', Dir=0x%8.8X, Entry=0x%8.8X)", PCTXT(Name), Index.m_iCluster, Index.m_iIndex);

	if( AssertRefIndex(Index) && !Name.HasDrive() && !Name.HasDirectory()) {

		FSIndex iOld = Index;

		DiskWaitWriteLock();

		HFILE hCluster = OpenHandle(Index.m_iCluster);

		if( hCluster != NULL ) {

			CFatDirEntry OldEntry;

			WaitWriteLock(hCluster);

			if( LocateShort(iOld, OldEntry) ) {

				FSIndex Find = Index;

				MakeDataIndex(Find);

				if( !LocateName(Name, Find) && ModifyName(Name, Index) ) {
				
					CFatDirEntry NewEntry;

					if( LocateShort(Index, NewEntry) ) {

						CFatDirShort &NewShort = NewEntry.GetShort();

						CFatDirShort &OldShort = OldEntry.GetShort();

						NewShort.m_bAttribute       = OldShort.m_bAttribute;
						NewShort.m_bCreateTimeTenth = OldShort.m_bCreateTimeTenth;
						NewShort.m_wCreateTime      = OldShort.m_wCreateTime;
						NewShort.m_wCreateDate      = OldShort.m_wCreateDate;
						NewShort.m_wFirstClusterHi  = OldShort.m_wFirstClusterHi;
						NewShort.m_wFirstClusterLo  = OldShort.m_wFirstClusterLo;
						NewShort.m_dwSize           = OldShort.m_dwSize;

						if( WriteEntry(Index, NewEntry) ) {

							FreeWriteLock(hCluster);

							CloseHandle(hCluster);

							DiskFreeWriteLock();

							return true;
							}
						}
					}
				}

			FreeWriteLock(hCluster);

			CloseHandle(hCluster);
			}

		DiskFreeWriteLock();
		}

	Debug(debugWarn, "RenameDir(Name='%s', Dir=0x%8.8X, Entry=0x%8.8X) - FAILED", PCTXT(Name), Index.m_iCluster, Index.m_iIndex);

	return false;
	}

BOOL CFatFileSystem::EmptyDir(FSIndex const &Index)
{
	Debug(debugFile, "EmptyDir(Dir=0x%8.8X, Entry=0x%8.8X)", Index.m_iCluster, Index.m_iIndex);

	DiskWaitWriteLock();

	FSIndex Dir = Index;

	if( IsDataIndex(Dir) || MoveDir(Index, Dir) ) {
		
		HFILE hCluster = OpenHandle(Dir.m_iCluster);

		if( hCluster != NULL ) {

			WaitWriteLock(hCluster);

			FSIndex iScan;

			iScan.m_iCluster = Dir.m_iCluster;

			iScan.m_iIndex   = IsRoot(Dir) ? 0 : 1;

			UINT uErrorCount = 0;

			while( LocateNext(iScan) ) {

				if( IsFile(iScan) ) {

					if( !DeleteFile(iScan) ) {

						uErrorCount ++;
						}

					continue;
					}

				if( IsDirectory(iScan) ) {

					if( !EmptyDir(iScan) ) {

						uErrorCount ++;
						}

					if( !RemoveDir(iScan) ) {

						uErrorCount ++;
						}
						
					continue;
					}
				}

			FreeWriteLock(hCluster);
	
			CloseHandle(hCluster);
				
			DiskFreeWriteLock();

			if( uErrorCount ) {

				Debug( debugWarn, 
					"EmptyDir(Dir=0x%8.8X, Entry=0x%8.8X) - FAILED : Error Count %d", 
					Index.m_iCluster, Index.m_iIndex, uErrorCount
					);

				return false;
				}
			
			return true;
			}
		}
	
	DiskFreeWriteLock();

	Debug(debugWarn, "EmptyDir(Dir=0x%8.8X, Entry=0x%8.8X) - FAILED", Index.m_iCluster, Index.m_iIndex);

	return false;
	}

BOOL CFatFileSystem::RemoveDir(FSIndex const &Index)
{
	Debug(debugFile, "RemoveDir(Dir=0x%8.8X, Entry=0x%8.8X)", Index.m_iCluster, Index.m_iIndex);

	if( AssertRefIndex(Index) ) {

		DiskWaitWriteLock();

		HFILE hCluster = OpenHandle(Index.m_iCluster);

		if( hCluster != NULL ) {

			FSIndex iFree = Index;

			CFatDirEntry Entry;

			WaitWriteLock(hCluster);

			if( LocateShort(iFree, Entry) ) {

				CFatDirShort &Short = Entry.GetShort();

				if( FreeName(Index) ) {

					UINT iDataCluster = Short.GetFirstCluster(); 

					if( iDataCluster == entryFree || FreeData(iDataCluster) ) {

						FreeWriteLock(hCluster);

						CloseHandle(hCluster);

						DiskFreeWriteLock();

						return true;
						}
					}
				}

			FreeWriteLock(hCluster);

			CloseHandle(hCluster);
			}

		DiskFreeWriteLock();
		}

	Debug(debugWarn, "RemoveDir(Dir=0x%8.8X, Entry=0x%8.8X) - FAILED", Index.m_iCluster, Index.m_iIndex);

	return false;
	}

UINT CFatFileSystem::ParentDir(FSIndex const &Index)
{
	UINT iParent = NOTHING;

	if( !IsRoot(Index) ) {

		DiskWaitReadLock();

		CFatDirEntry Entry;

		if( ReadEntry(Index.m_iCluster, 1, Entry) ) {

			CFatDirShort &Short = Entry.GetShort();

			iParent = Short.GetFirstCluster();

			if( iParent == rootEntry ) { 

				iParent = GetRootCluster();
				}
			}
		
		DiskFreeReadLock();
		}

	return iParent;
	}

// File

HFILE CFatFileSystem::OpenDirectory(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	if( AssertDataIndex(Dir) ) {

		CFilename DirName;
	
		if( !Name.GetDirectory(DirName) ) {

			Index.m_iCluster = Dir.m_iCluster;

			Index.m_iIndex   = Dir.m_iIndex;

			return OpenHandle(Index.m_iCluster);
			}

		if( LocateDir(DirName, Dir, Index) ) {

			if( IsDataIndex(Index) || MoveDir(Index, Index) ) {

				return OpenHandle(Index.m_iCluster);
				}
			}
		}

	return NULL;
	}

BOOL CFatFileSystem::LocateFile(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	Debug(debugFile, "LocateFile(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	DiskWaitReadLock();

	HFILE hDirectory = OpenDirectory(Name, Dir, Index);
	
	if( hDirectory != NULL ) {

		WaitReadLock(hDirectory);

		CFilename Filename = Name.GetName();

		if( LocateName(Filename, Index) && IsFile(Index) ) {

			FreeReadLock(hDirectory);

			CloseHandle(hDirectory);

			DiskFreeReadLock();

			return true;
			}

		FreeReadLock(hDirectory);

		CloseHandle(hDirectory);
		}

	DiskFreeReadLock();

	Debug(debugFile, "LocateFile(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Dir.m_iCluster);

	return false;
	}

HFILE CFatFileSystem::CreateFile(CFilename const &Name, FSIndex const &Dir)
{
	Debug(debugFile, "CreateFile(Name='%s', Dir=%d)", PCTXT(Name), Dir.m_iCluster);

	DiskWaitWriteLock();

	FSIndex Index;

	HFILE   hDirectory = OpenDirectory(Name, Dir, Index);
	
	if( hDirectory != NULL ) {

		HFILE hFile = CreateFile(hDirectory, Name.GetName());

		CloseHandle(hDirectory);

		DiskFreeWriteLock();

		return hFile;
		}

	DiskFreeWriteLock();

	Debug(debugFile, "CreateFile(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Dir.m_iCluster);

	return NULL;
	}

HFILE CFatFileSystem::CreateFile(HFILE hDirectory, CFilename const &Name)
{
	Debug(debugFile, "CreateFile(Name='%s')", PCTXT(Name));

	if( hDirectory != NULL ) {

		DiskWaitWriteLock();

		WaitWriteLock(hDirectory);

		FSIndex Index = ((CHandle *) hDirectory)->m_Index;

		MakeDataIndex(Index);

		if( !LocateName(Name, Index) && CreateName(Name, Index) ) {

			HFILE hFile = OpenHandle(Index);

			if( hFile != NULL ) {

				WaitWriteLock(hFile);
						
				CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;
											
				pFile32->m_Short.m_iCluster = Index.m_iCluster;

				pFile32->m_Short.m_iIndex   = Index.m_iIndex;

				if( LocateShort(pFile32->m_Short, pFile32->m_Entry) ) {

					pFile32->m_Entry.GetShort().MakeArchive();

					if( WriteEntry(pFile32->m_Short, pFile32->m_Entry) ) {

						pFile32->m_nOpen = 1;

						pFile32->m_Mode  = fileRead | fileWrite;

						FreeWriteLock(hDirectory);

						FreeWriteLock(hFile);

						DiskWaitLock(pFile32->m_Mode);

						DiskFreeWriteLock();

						return hFile;
						}
					}

				FreeWriteLock(hFile);

				CloseHandle(hFile);
				}
					
			FreeName(Index);
			}

		FreeWriteLock(hDirectory);

		DiskFreeWriteLock();
		}

	Debug(debugFile, "CreateFile(Name='%s') - FAILED", PCTXT(Name));

	return NULL;
	}

HFILE CFatFileSystem::OpenFile(CFilename const &Name, FSIndex const &Dir, UINT Mode)
{
	Debug(debugFile, "OpenFile(Name='%s', Dir=0x%8.8X, Mode=0x%4.4X)", PCTXT(Name), Dir.m_iCluster, Mode);

	Mode |= (Mode & fileCreate) ? fileWrite : 0;

	DiskWaitLock(Mode);

	FSIndex Index;

	HFILE   hDirectory = OpenDirectory(Name, Dir, Index);

	if( hDirectory != NULL ) {

		WaitReadLock(hDirectory);

		CFilename Filename = Name.GetName();
		
		if( LocateName(Filename, Index) && IsFile(Index) ) {

			HFILE hFile = OpenFile(Index, Mode);

			FreeReadLock(hDirectory);

			CloseHandle(hDirectory);

			DiskFreeLock(Mode);

			return hFile;
			}

		FreeReadLock(hDirectory);

		if( Mode & fileCreate ) {

			HFILE hFile = CreateFile(hDirectory, Filename);

			CloseHandle(hDirectory);

			DiskFreeLock(Mode);

			return hFile;
			}

		CloseHandle(hDirectory);
		}

	DiskFreeLock(Mode);

	Debug(debugFile, "OpenFile(Name='%s', Dir=0x%8.8X, Mode=0x%4.4X) - FAILED", PCTXT(Name), Dir.m_iCluster, Mode);

	return NULL;
	}

HFILE CFatFileSystem::OpenFile(FSIndex const &Index, UINT Mode)
{
	Debug(debugFile, "OpenFile(Dir=0x%8.8X, Entry=0x%8.8X, Mode=0x%4.4X)", Index.m_iCluster, Index.m_iIndex, Mode);

	if( AssertRefIndex(Index) ) {

		DiskWaitLock(Mode);

		HFILE hDirectory = OpenHandle(Index.m_iCluster);

		if( hDirectory != NULL ) {

			WaitReadLock(hDirectory);

			HFILE hFile = OpenHandle(Index);

			if( hFile != NULL ) {

				m_pMutex->Wait(FOREVER);

				CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

				if( AtomicIncrement(&pFile32->m_nOpen) == 1 ) {

					pFile32->m_Short.m_iCluster = Index.m_iCluster;

					pFile32->m_Short.m_iIndex   = Index.m_iIndex;

					LocateShort(pFile32->m_Short, pFile32->m_Entry);
					}
							
				bool fReadOnly  = pFile32->m_Entry.GetShort().IsReadOnly() ? true : false; 

				bool fWriteMode = (Mode & fileWrite) ? true : false;

				if( !fReadOnly || !fWriteMode ) {

					DiskWaitLock(Mode & ~pFile32->m_Mode);

					pFile32->m_Mode |= Mode;

					m_pMutex->Free();

					FreeReadLock(hDirectory);

					CloseHandle(hDirectory);

					DiskFreeLock(Mode);

					Debug( debugFile, 
					       "OpenFile(Dir=0x%8.8X, Entry=0x%8.8X, Mode=0x%4.4X) Ref=%d, Mode=0x%4.4X", 
					       Index.m_iCluster, Index.m_iIndex, Mode, pFile32->m_nOpen, pFile32->m_Mode 
					       );

					return hFile;
					}

				AtomicDecrement(&pFile32->m_nOpen);

				m_pMutex->Free();

				CloseHandle(hFile);
				}

			FreeReadLock(hDirectory);

			CloseHandle(hDirectory);
			}

		DiskFreeLock(Mode);
		}

	Debug(debugWarn, "OpenFile(Dir=0x%8.8X, Entry=0x%8.8X, Mode=0x%4.4X) - FAILED", Index.m_iCluster, Index.m_iIndex, Mode);

	return NULL;
	}

BOOL CFatFileSystem::CloseFile(HFILE hFile)
{
	Debug(debugFile, "CloseFile(File=0x%8.8X)", hFile);

	if( hFile ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		m_pMutex->Wait(FOREVER);

		if( AtomicDecrement(&pFile32->m_nOpen) == 0 ) {

			Debug(debugFile, "CloseFile(File=0x%8.8X) - File Closed.", hFile);

			DiskFreeLock(pFile32->m_Mode);

			pFile32->m_Mode = 0;
			}

		m_pMutex->Free();

		CloseHandle(hFile);

		return true;
		}

	Debug(debugWarn, "CloseFile(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

BOOL CFatFileSystem::DeleteFile(CFilename const &Name, FSIndex const &Dir)
{
	Debug(debugFile, "DeleteFile(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	HFILE hFile = OpenFile(Name, Dir, fileWrite);

	if( hFile ) {

		if( DeleteFile(hFile) ) {

			CloseFile(hFile);

			return true;
			}

		CloseFile(hFile);
		}

	Debug(debugWarn, "DeleteFile(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Dir.m_iCluster);

	return false;
	}

BOOL CFatFileSystem::DeleteFile(FSIndex const &Index)
{
	Debug(debugFile, "DeleteFile(Dir=0x%8.8X, Entry=0x%8.8X)", Index.m_iCluster, Index.m_iIndex);

	HFILE hFile = OpenFile(Index, fileWrite);

	if( hFile ) {

		if( DeleteFile(hFile) ) {

			CloseFile(hFile);

			return true;
			}

		CloseFile(hFile);
		}

	Debug(debugWarn, "DeleteFile(Dir=0x%8.8X, Entry=0x%8.8X) - FAILED", Index.m_iCluster, Index.m_iIndex);

	return false;
	}

BOOL CFatFileSystem::DeleteFile(HFILE hFile)
{
	Debug(debugFile, "DeleteFile(File=0x%8.8X)", hFile);

	if( WaitWriteLock(hFile) ) {

		CHandle   *pFile   = (CHandle   *) hFile;

		CHandle32 *pFile32 = (CHandle32 *) pFile->m_pObject;

		if( pFile32->m_nOpen == 1 ) {

			if( pFile32->m_Mode & fileWrite ) {

				UINT iData = pFile32->m_Entry.GetShort().GetFirstCluster();
				
				pFile32->m_Entry.MakeEmpty();

				if( iData == entryFree || FreeData(iData) ) {
					
					FreeName(pFile->m_Index);
					
					FreeWriteLock(hFile);

					return true;
					}
				}
			}
	
		FreeWriteLock(hFile);
		}

	Debug(debugWarn, "DeleteFile(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

BOOL CFatFileSystem::RenameFile(HFILE hFile, CFilename const &Name)
{
	Debug(debugFile, "RenameFile(File=0x%8.8X, Name='%s')", hFile, PCTXT(Name));

	if( !Name.HasDrive() && !Name.HasDirectory() ) {

		if( WaitWriteLock(hFile) ) {

			CHandle   *pFile   = (CHandle   *) hFile;

			CHandle32 *pFile32 = (CHandle32 *) pFile->m_pObject;

			if( pFile32->m_nOpen && (pFile32->m_Mode & fileWrite) ) {

				HFILE hDirectory = OpenHandle(pFile->m_Index.m_iCluster);

				if( hDirectory != NULL ) {

					WaitWriteLock(hDirectory);

					FSIndex Find = pFile->m_Index;

					MakeDataIndex(Find);

					if( !LocateName(Name, Find) && ModifyName(Name, pFile->m_Index) ) {
			
						pFile32->m_Short.m_iCluster = pFile->m_Index.m_iCluster;

						pFile32->m_Short.m_iIndex   = pFile->m_Index.m_iIndex;
			
						CFatDirEntry NewEntry;

						if( LocateShort(pFile32->m_Short, NewEntry) ) {

							CFatDirShort &Short = NewEntry.GetShort();

							CFatDirShort &Entry = pFile32->m_Entry.GetShort();

							Short.m_bAttribute       = Entry.m_bAttribute;
							Short.m_bCreateTimeTenth = Entry.m_bCreateTimeTenth;
							Short.m_wCreateTime      = Entry.m_wCreateTime;
							Short.m_wCreateDate      = Entry.m_wCreateDate;
							Short.m_wFirstClusterHi  = Entry.m_wFirstClusterHi;
							Short.m_wFirstClusterLo  = Entry.m_wFirstClusterLo;
							Short.m_dwSize           = Entry.m_dwSize;

							Short.MakeArchive();

							if( WriteEntry(pFile32->m_Short, NewEntry) ) {

								memcpy(Entry.m_sText, Short.m_sText, sizeof(Short.m_sText));

								FreeWriteLock(hDirectory);

								FreeWriteLock(hFile);

								CloseHandle(hDirectory);

								return true;
								}
							}
						}

					FreeWriteLock(hDirectory);

					CloseHandle(hDirectory);
					}
				}

			FreeWriteLock(hFile);
			}
		}
		
	Debug(debugWarn, "RenameFile(File=0x%8.8X, Name='%s') - FAILED", hFile, PCTXT(Name));

	return false;
	}

BOOL CFatFileSystem::FileSetLength(HFILE hFile, UINT uSize)
{
	if( WaitWriteLock(hFile) ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		if( pFile32->m_nOpen && (pFile32->m_Mode & fileWrite) ) {

			CFatDirShort &Short = pFile32->m_Entry.GetShort();

			if( Short.m_dwSize == uSize ) {

				FreeWriteLock(hFile);

				return true;
				}

			if( FileAllocData(hFile, uSize) ) {
					
				Short.m_dwSize = uSize;

				Short.MakeArchive();

				if( WriteEntry(pFile32->m_Short, pFile32->m_Entry) ) {

					FreeWriteLock(hFile);

					return true;
					}
				}
			}

		FreeWriteLock(hFile);
		}

	Debug(debugWarn, "FileSetLength(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

UINT CFatFileSystem::FileGetLength(HFILE hFile)
{
	if( WaitReadLock(hFile) ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		if( pFile32->m_nOpen ) {

			UINT uSize = pFile32->m_Entry.GetShort().m_dwSize;

			FreeReadLock(hFile);

			return uSize;
			}

		FreeReadLock(hFile);
		}

	Debug(debugWarn, "FileGetLength(File=0x%8.8X) - FAILED", hFile);

	return 0;
	}

BOOL CFatFileSystem::FileSetUnixTime(HFILE hFile, time_t time)
{
	if( WaitWriteLock(hFile) ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		if( pFile32->m_nOpen && (pFile32->m_Mode & fileWrite) ) {

			pFile32->m_Entry.GetShort().SetUnixTime(time);

			if( WriteEntry(pFile32->m_Short, pFile32->m_Entry) ) {

				FreeWriteLock(hFile);

				return true;
				}
			}

		FreeWriteLock(hFile);
		}

	Debug(debugWarn, "FileSetTime(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

time_t CFatFileSystem::FileGetUnixTime(HFILE hFile)
{
	if( WaitReadLock(hFile) ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		if( pFile32->m_nOpen ) {

			time_t t = pFile32->m_Entry.GetShort().GetUnixTime();

			FreeReadLock(hFile);

			return t;
			}

		FreeReadLock(hFile);
		}

	Debug(debugWarn, "FileGetTime(File=0x%8.8X) - FAILED", hFile);

	return 0;
	}

BOOL CFatFileSystem::FileGetIndex(HFILE hFile, FSIndex &Index)
{
	if( WaitReadLock(hFile) ) {

		CHandle *pFile   = (CHandle *) hFile;

		Index.m_iCluster = pFile->m_Index.m_iCluster;

		Index.m_iIndex   = pFile->m_Index.m_iIndex;

		FreeReadLock(hFile);

		return true;
		}

	Debug(debugWarn, "FileGetIndex(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

BOOL CFatFileSystem::FileAllocData(HFILE hFile, UINT uSize)
{
	Debug(debugFile, "FileAllocData(File=0x%8.8X, Count=0x%8.8X)", hFile, uSize);

	if( WaitWriteLock(hFile) ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		if( pFile32->m_nOpen && (pFile32->m_Mode & fileWrite) ) {

			CFatDirShort &Entry = pFile32->m_Entry.GetShort();

			UINT uCluster = Entry.GetFirstCluster();

			if( uSize ) {

				if( uCluster == entryFree ) {

					uCluster = AllocData(uSize, false);

					Entry.SetFirstCluster(uCluster);
					}
				else {
					ReAllocData(uCluster, uSize, false);
					}
				}
			else {
				if( uCluster != entryFree ) {

					FreeData(uCluster);

					Entry.SetFirstCluster(entryFree);
					}
				}

			FreeWriteLock(hFile);

			return true;
			}
		
		FreeWriteLock(hFile);
		}

	Debug(debugWarn, "FileAllocData(File=0x%8.8X, Count=0x%8.8X) - FAILED", hFile, uSize);

	return false;
	}

UINT CFatFileSystem::FileWriteData(HFILE hFile, PCBYTE pData, UINT uOffset, UINT uCount)
{
	Debug(debugFile, "FileWriteData(hFile=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", hFile, uOffset, uCount);

	if( WaitWriteLock(hFile) ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		if( pFile32->m_Mode & fileWrite ) {

			CFatDirShort &Entry = pFile32->m_Entry.GetShort();

			UINT uSize = uOffset + uCount;

			if( uSize <= Entry.m_dwSize || FileSetLength(hFile, uSize) ) {

				bool f1 = Entry.UpdateTimeStamps();

				bool f2 = Entry.MakeArchive();

				if( (!f1 && !f2) || WriteEntry(pFile32->m_Short, pFile32->m_Entry) ) {

					uCount = WriteData(Entry.GetFirstCluster(), uOffset, pData, uCount);

					FreeWriteLock(hFile);

					return uCount;
					}
				}
			}
		
		FreeWriteLock(hFile);
		}
	
	Debug(debugWarn, "FileWriteData(hFile=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X) - FAILED", hFile, uOffset, uCount);

	return NOTHING;
	}

UINT CFatFileSystem::FileReadData(HFILE hFile, PBYTE pData, UINT uOffset, UINT uCount)
{
	Debug(debugFile, "FileReadData(hFile=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", hFile, uOffset, uCount);

	if( WaitWriteLock(hFile) ) {

		CHandle32 *pFile32 = (CHandle32 *)((CHandle *) hFile)->m_pObject;

		if( pFile32->m_Mode & fileRead ) {

			CFatDirShort &Entry = pFile32->m_Entry.GetShort();

			MakeMin(uCount, Entry.m_dwSize - uOffset);

			if( !Entry.UpdateAccessTime() || WriteEntry(pFile32->m_Short, pFile32->m_Entry) ) {

				FreeWriteLock(hFile);

				WaitReadLock(hFile);

				uCount = ReadData(Entry.GetFirstCluster(), uOffset, pData, uCount);

				FreeReadLock(hFile);

				return uCount;
				}
			}

		FreeWriteLock(hFile);
		}

	Debug(debugWarn, "FileReadData(hFile=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X) - FAILED", hFile, uOffset, uCount);

	return NOTHING;
	}

// File Handles

void CFatFileSystem::InitFileHandles(void)
{
	memset(m_Handle32, 0x00, sizeof(m_Handle32));
	
	for( UINT i = 0; i < constMaxHandles; i ++ ) {

		m_Handle[i].m_pObject = &m_Handle32[i];
		}
	}

void CFatFileSystem::OnInitHandle(HFILE hFile)
{
	CHandle32 *pHandle = (CHandle32 *)((CHandle *) hFile)->m_pObject;

	pHandle->m_nOpen = 0;

	pHandle->m_Mode  = 0;
	}

// Layout Function

UINT CFatFileSystem::GetRootCluster(void) const
{
	return 0;
	}

UINT CFatFileSystem::GetFirstSector(UINT uCluster) const
{
	return 0;
	}

// Indexes

BOOL CFatFileSystem::IsRefIndex(FSIndex const &Index) const
{
	return Index.m_iIndex != indexCluster;
	}

BOOL CFatFileSystem::IsDataIndex(FSIndex const &Index) const
{
	return Index.m_iIndex == indexCluster;
	}

void CFatFileSystem::MakeRefIndex(FSIndex &Index) const
{
	Index.m_iIndex = indexFirst;
	}

void CFatFileSystem::MakeDataIndex(FSIndex &Index) const
{
	Index.m_iIndex = UINT(indexCluster);
	}

BOOL CFatFileSystem::AssertRefIndex(FSIndex const &Index) const
{
	if( !IsRefIndex(Index) ) {

		Debug(debugWarn, "AssertRefIndex(Cluster=0x%8.8X, Index=0x%8.8X) - FAILED", Index.m_iCluster, Index.m_iIndex);

		return false;
		}

	return true;
	}

BOOL CFatFileSystem::AssertDataIndex(FSIndex const &Index) const
{
	if( !IsDataIndex(Index) ) {

		Debug(debugWarn, "AssertDataIndex(Cluster=0x%8.8X, Index=0x%8.8X) - FAILED", Index.m_iCluster, Index.m_iIndex);

		return false;
		}

	return true;
	}

// Names

BOOL CFatFileSystem::AllocName(UINT uCount, FSIndex &Index)
{
	Debug(debugFile, "AllocName(Count=0x%8.8X, Cluster=0x%8.8X)", uCount, Index.m_iCluster);

	if( AssertDataIndex(Index) ) {
		
		UINT uLimit = GetEntryLimit(Index.m_iCluster);

		UINT iFound = NOTHING;

		UINT uSpace = 0;

		BOOL fLast  = false;

		BOOL fFixed = Index.m_iCluster == rootEntry;

		for( UINT i = 0; i < uLimit; i ++ ) {

			CFatDirEntry Entry;

			if( ReadEntry(Index.m_iCluster, i, Entry) ) {

				if( Entry.IsLast() ) {

					uSpace = uLimit - i;

					iFound = i;

					fLast  = true;

					break;
					}

				if( Entry.IsFree() ) {

					if( iFound == NOTHING ) {

						iFound = i;
					
						uSpace = 0;
						}

					if( ++uSpace < uCount ) {

						continue;
						}
				
					break;
					}

				iFound = NOTHING;

				continue;
				}

			Debug(debugWarn, "AllocName(Count=0x%8.8X) - FAILED", uCount);

			return false;
			}

		if( iFound != NOTHING && uSpace >= uCount ) {

			Index.m_iIndex = iFound;

			return true;
			}

		UINT uAlloc = (uLimit + uCount) * sizeof(CFatDirEntry);

		if( !fFixed ) {

			if( ReAllocData(Index.m_iCluster, uAlloc, true) != NOTHING ) {

				Index.m_iIndex = fLast ? iFound : uLimit;

				return true;
				}
			}
		}

	Debug(debugWarn, "AllocName(Count=0x%8.8X) - FAILED", uCount);

	return false;
	}

BOOL CFatFileSystem::FreeName(FSIndex const &Index)
{
	Debug(debugFile, "FreeName(Cluster=0x%8.8X, Index=0x%8.8X)", Index.m_iCluster, Index.m_iIndex);

	if( AssertRefIndex(Index) ) {

		FSIndex iFree = Index;

		CFatDirEntry Entry;

		if( ReadEntry(iFree, Entry) ) {

			CFatDirEntry Free;

			Free.GetShort().MakeFree();

			if( Entry.IsLong() ) {
				
				UINT uCount = Entry.GetLong().GetOrder();

				while( uCount -- ) {

					WriteEntry(iFree, Free);

					iFree.m_iIndex++;
					}
				}

			WriteEntry(iFree, Free);
			
			return true;
			}
		}
	
	return false;
	}

BOOL CFatFileSystem::LookupName(FSIndex const &Index, CFilename &Name)
{
	Debug(debugFile, "LookupName(Dir=0x%8.8X, Entry=0x%8.8X)", Index.m_iCluster, Index.m_iIndex);

	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( ReadEntry(Index, Entry) ) {

			CFatDirName Filename;

			Filename.Import(Entry);

			UINT uCount = 0;
		
			while( Entry.IsLong() ) {

				uCount ++;

				if( ReadEntry(Index.m_iCluster, Index.m_iIndex + uCount, Entry) ) {

					Filename.Import(Entry);
				
					continue;
					}

				return false;
				}
			
			Name = PCTXT(Filename);

			return true;
			}
		}

	return false;
	}

BOOL CFatFileSystem::LookupData(FSIndex const &Index, UINT iData, CFilename &Name)
{
	Debug(debugFile, "LookupData(Cluster=0x%8.8X, Index=0x%8.8X)", Index.m_iCluster, Index.m_iIndex);

	if( AssertDataIndex(Index) ) {

		FSIndex Scan;

		Scan.m_iCluster = Index.m_iCluster;

		Scan.m_iIndex   = indexFirst;

		do {
			CFatDirEntry Entry;

			if( !ReadEntry(Scan, Entry) ) {

				return false;
				}

			if( Entry.IsLong() ) {

				UINT iIndex = Scan.m_iIndex + Entry.GetLong().GetOrder();
					
				if( !ReadEntry(Scan.m_iCluster, iIndex, Entry) ) {

					return false;
					}
				}

			if( Entry.GetShort().GetFirstCluster() == iData ) {

				return LookupName(Scan, Name);
				}

			} while( LocateNext(Scan) );
		}

	return false;
	}

BOOL CFatFileSystem::LocateName(CFilename const &Name, FSIndex &Index)
{
	if( AssertDataIndex(Index) ) {

		FSIndex iScan;

		MakeRefIndex(iScan);

		iScan.m_iCluster = Index.m_iCluster;

		do {
			CFilename Scan;

			LookupName(iScan, Scan);

			if( Scan == Name ) {

				Index.m_iIndex = iScan.m_iIndex;

				return true;
				}

			} while( LocateNext(iScan) );
		}

	return false;
	}

BOOL CFatFileSystem::CreateName(CFilename const &Name, FSIndex &Index)
{
	Debug(debugFile, "CreateName(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Index.m_iCluster);

	CFatDirName Filename = Name.GetPath();

	if( Filename.IsShort() || MakeUnique(Filename, Index) ) {

		UINT uCount = Filename.GetEntryCount();

		if( AllocName(uCount, Index) ) {

			for( UINT iEntry = 0; iEntry < uCount; iEntry ++ ) {

				CFatDirEntry Entry;

				Filename.Export(iEntry, Entry);

				UINT iIndex = uCount - iEntry - 1;

				if( !WriteEntry(Index.m_iCluster, Index.m_iIndex + iIndex, Entry) ) {
					
					Debug(debugWarn, "CreateName(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Index.m_iCluster);

					return false;
					}
				}

			return true;
			}
		}

	Debug(debugWarn, "CreateName(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Index.m_iCluster);

	return false;
	}

BOOL CFatFileSystem::ModifyName(CFilename const &Name, FSIndex &Index)
{
	if( FreeName(Index) ) {

		MakeDataIndex(Index);

		return CreateName(Name, Index);
		}
	
	return false;
	}

BOOL CFatFileSystem::LocateNext(FSIndex &Index)
{
	if( LocateShort(Index) ) {

		UINT uLimit = GetEntryLimit(Index.m_iCluster);

		while( ++Index.m_iIndex < uLimit ) {

			CFatDirEntry Entry;

			if( ReadEntry(Index, Entry) ) {

				Entry.Dump();

				if( Entry.IsLast() ) {

					break;
					}

				if( Entry.IsEmpty() ) {

					continue;
					}

				if( Entry.IsFree() ) {

					continue;
					}

				return true;
				}

			break;
			}
		}

	return false;
	}

BOOL CFatFileSystem::LocateShort(FSIndex &Index)
{
	if( AssertRefIndex(Index) ) {

		CFatDirEntry Entry;

		if( ReadEntry(Index, Entry) ) {

			if( Entry.IsLong() ) {
					
				Index.m_iIndex += Entry.GetLong().GetOrder();
				}

			return true;
			}
		}

	return false;
	}

BOOL CFatFileSystem::LocateShort(FSIndex &Index, CFatDirEntry &Entry)
{
	if( AssertRefIndex(Index) ) {

		if( ReadEntry(Index, Entry) ) {

			if( Entry.IsLong() ) {

				Index.m_iIndex += Entry.GetLong().GetOrder();

				return ReadEntry(Index, Entry);
				}

			return true;
			}
		}

	return false;
	}

BOOL CFatFileSystem::LocateShort(FSIndex const &Index, CFatDirEntry &Entry)
{
	if( AssertRefIndex(Index) ) {

		if( ReadEntry(Index, Entry) ) {

			if( Entry.IsLong() ) {

				UINT iCluster = Index.m_iCluster;

				UINT iIndex   = Index.m_iIndex + Entry.GetLong().GetOrder();

				return ReadEntry(iCluster, iIndex, Entry);
				}

			return true;
			}
		}

	return false;
	}

BOOL CFatFileSystem::MakeUnique(CFatDirName &Filename, FSIndex const &Index)
{
	Debug(debugFile, "MakeUnique(Name='%s', Dir=0x%8.8X)", PCTXT(Filename), Index.m_iCluster);

	if( AssertDataIndex(Index) ) {

		UINT uLimit = GetEntryLimit(Index.m_iCluster);

		for( UINT n = 1; ; n++ ) {

			PCTXT pBasis = Filename.MakeBasis(n);

			UINT  uCount = strlen(pBasis);

			BOOL  fFound = false;

			for( UINT iScan = 0; iScan < uLimit; iScan ++ ) {

				CFatDirEntry Entry;

				if( ReadEntry(Index.m_iCluster, iScan, Entry) ) {

					if( Entry.IsLast() ) {

						break;
						}

					if( Entry.IsFree() ) {

						continue;
						}

					if( Entry.IsLong() ) {

						CFatDirLong &Long = Entry.GetLong();

						iScan += (Long.GetOrder() - 1);

						continue;
						}

					if( Entry.IsShort() ) {

						CFatDirShort &Short = Entry.GetShort();

						char sText[13];
	
						Short.GetFullName(sText);

						if( !memcmp(pBasis, sText, uCount) ) {

							fFound = true;

							break;
							}
						}
				
					continue;
					}

				Debug(debugWarn, "MakeUnique(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Filename), Index.m_iCluster);
		
				return false;
				}

			if( !fFound ) {

				return true;
				}
			}
		}

	return false;
	}

// Directory

BOOL CFatFileSystem::GetEntryLimit(UINT uCluster)
{
	return uCluster == rootEntry ? m_uRootSize : GetAlloc(uCluster) / sizeof(FatDirEntry);
	}

BOOL CFatFileSystem::MapEntry(UINT uCluster, UINT uEntry, CDataSector &Ptr)
{
	UINT uOffset = uEntry * sizeof(FatDirEntry);

	if( uCluster != rootEntry ) {

		uCluster       = FindNext(uCluster, uOffset / m_uClusterSize);

		Ptr.m_uSector  = (uOffset % m_uClusterSize) / m_uSectorSize;

		Ptr.m_uOffset  = (uOffset % m_uClusterSize) % m_uSectorSize;
		
		Ptr.m_uSector += GetFirstSector(uCluster - 2);

		Ptr.m_uSector += m_uData;
		}
	else {
		Ptr.m_uSector  = uOffset / m_uSectorSize;

		Ptr.m_uOffset  = uOffset % m_uSectorSize;

		Ptr.m_uSector += m_uRoot;
		}

	return true;
	}

BOOL CFatFileSystem::ReadEntry(UINT uCluster, UINT uEntry, CFatDirEntry &Entry)
{
	Debug(debugData, "ReadEntry(Cluster=0x%8.8X, Entry=0x%8.8X)", uCluster, uEntry);

	CDataSector Sector;

	if( MapEntry(uCluster, uEntry, Sector) ) {

		if( ReadSector(Sector, PBYTE(&Entry), sizeof(Entry)) ) {

			Entry.HostToLocal();

			return Entry.IsValid();
			}	
		}

	Debug(debugWarn, "ReadEntry(Cluster=0x%8.8X, Entry=0x%8.8X) - FAILED", uCluster, uEntry);

	return false;
	}

BOOL CFatFileSystem::WriteEntry(UINT uCluster, UINT uEntry, CFatDirEntry &Entry)
{
	Debug(debugData, "WriteEntry(Cluster=0x%8.8X, Entry=0x%8.8X)", uCluster, uEntry);

	CDataSector Sector;

	if( MapEntry(uCluster, uEntry, Sector) ) {

		Entry.LocalToHost();

		if( WriteSector(Sector, PBYTE(&Entry), sizeof(Entry)) ) {

			Entry.HostToLocal();

			return true;
			}

		Entry.HostToLocal();
		}

	Debug(debugWarn, "WriteEntry(Cluster=0x%8.8X, Entry=0x%8.8X) - FAILED", uCluster, uEntry);

	return false;
	}

BOOL CFatFileSystem::ReadEntry(FSIndex const &Index, CFatDirEntry &Entry)
{
	return ReadEntry(Index.m_iCluster, Index.m_iIndex, Entry);
	}

BOOL CFatFileSystem::WriteEntry(FSIndex const &Index, CFatDirEntry &Entry)
{
	return WriteEntry(Index.m_iCluster, Index.m_iIndex, Entry);
	}

// Data IO

BOOL CFatFileSystem::MapData(UINT uCluster, UINT uSector, UINT uOffset, CDataSector &Ptr) const
{
	AfxAssert( uSector < m_uClusterSize );

	Ptr.m_uSector  = uSector;

	Ptr.m_uOffset  = uOffset;

	Ptr.m_uSector += m_uData;

	Ptr.m_uSector += GetFirstSector(uCluster - 2);

	return true;
	}

UINT CFatFileSystem::WriteData(UINT uCluster, UINT uOffset, PCBYTE pData, UINT uCount)
{
	Debug(debugData, "WriteData(Cluster=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", uCluster, uOffset, uCount);

	return XferData(uCluster, uOffset, PBYTE(pData), uCount, xferWrite);
	}

UINT CFatFileSystem::ReadData(UINT uCluster, UINT uOffset, PBYTE pData, UINT uCount)
{
	Debug(debugData, "ReadData(Cluster=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", uCluster, uOffset, uCount);

	return XferData(uCluster, uOffset, pData, uCount, xferRead);
	}

BOOL CFatFileSystem::ZeroData(UINT uCluster, UINT uOffset, UINT uCount)
{
	Debug(debugData, "ZeroData(Cluster=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", uCluster, uOffset, uCount);

	return XferData(uCluster, uOffset, NULL, uCount, xferZero);
	}

UINT CFatFileSystem::XferData(UINT uCluster, UINT uOffset, PBYTE pData, UINT uCount, UINT Type)
{
	UINT uChain = uOffset / m_uClusterSize;
 
	UINT uIndex = uOffset % m_uClusterSize;

	UINT uTotal = 0;
	
	uCluster = FindNext(uCluster, uChain); 

	while( uCluster != NOTHING ) {

		UINT uSector  = uIndex / m_uSectorSize;

		UINT iData    = uIndex % m_uSectorSize;

		UINT uSectors = m_uClusterSize / m_uSectorSize;

		for( UINT iSector = uSector; iSector < uSectors; iSector ++ ) {

			UINT uThis = Min(uCount - uTotal, m_uSectorSize - iData);

			if( uThis ) {

				CDataSector Data;

				if( MapData(uCluster, iSector, iData, Data) ) {

					iData = 0;

					if( Type == xferRead ) {

						if( !ReadSector(Data, pData, uThis) ) {

							return uTotal;
							}

						pData += uThis;
						}

					if( Type == xferWrite ) {

						if( !WriteSector(Data, pData, uThis) ) {

							return uTotal;
							}

						pData += uThis;
						}

					if( Type == xferZero ) {

						if( !ZeroSector(Data, uThis) ) {

							return 0;
							}
						}
				
					uTotal += uThis;

					continue;
					}
				}
			
			return uTotal;
			}

		uIndex   = 0;

		uCluster = FindNext(uCluster);
		}

	return uTotal;
	}

// Data Allocation

UINT CFatFileSystem::GetAlloc(UINT uCluster)
{
	UINT uCount = FindCount(uCluster);

	return uCount != entryInvalid ? uCount * m_uClusterSize : entryInvalid;
	}

UINT CFatFileSystem::AllocData(UINT uAlloc, BOOL fZero)
{
	return entryInvalid;
	}

UINT CFatFileSystem::ReAllocData(UINT uCluster, UINT uAlloc, BOOL fZero)
{
	return entryInvalid;
	}

BOOL CFatFileSystem::FreeData(UINT uCluster)
{
	return false;
	}

// Chain

UINT CFatFileSystem::FindNext(UINT uCluster)
{
	return entryInvalid;
	}

UINT CFatFileSystem::FindNext(UINT uCluster, UINT uCount)
{
	return entryInvalid;
	}

UINT CFatFileSystem::FindLast(UINT uCluster)
{
	return entryInvalid;
	}

UINT CFatFileSystem::FindCount(UINT uCluster)
{
	return entryInvalid;
	}

UINT CFatFileSystem::FindFree(void)
{
	return entryInvalid;
	}

UINT CFatFileSystem::FindFreeCount(void)
{
	return entryInvalid;
	}

// Data

BOOL CFatFileSystem::ReadCluster(UINT uCluster, PBYTE pData)
{
	return ReadCluster(uCluster, pData, m_uClusterSize);
	}

BOOL CFatFileSystem::WriteCluster(UINT uCluster, PCBYTE pData)
{
	return WriteCluster(uCluster, pData, m_uClusterSize);
	}

BOOL CFatFileSystem::ReadCluster(UINT uCluster, PBYTE pData, UINT uCount)
{
	Debug(debugData, "ReadCluster(Cluster=0x%8.8X, Count=%d)", uCluster, uCount);

	UINT uSector = m_uData + GetFirstSector(uCluster - 2);

	while( uCount ) {

		UINT uThis = Min(uCount, m_uSectorSize);

		if( ReadSector(uSector, pData, uThis) ) {

			uCount -= uThis;

			pData  += uThis;

			uSector++;
			
			continue;
			}

		Debug(debugData, "ReadCluster(Cluster=0x%8.8X, Count=%d) - FAILED", uCluster, uCount);

		return false;
		}

	return true;
	}

BOOL CFatFileSystem::WriteCluster(UINT uCluster, PCBYTE pData, UINT uCount)
{
	Debug(debugData, "WriteCluster(Cluster=0x%8.8X, Count=%d)", uCluster, uCount);

	UINT uSector = m_uData + GetFirstSector(uCluster - 2);

	while( uCount ) {

		UINT uThis = Min(uCount, m_uSectorSize);

		if( WriteSector(uSector, pData, uThis) ) {

			uCount -= uThis;

			pData  += uThis;

			uSector++;
			
			continue;
			}

		Debug(debugWarn, "WriteCluster(Cluster=0x%8.8X, Count=%d) - FAILED", uCluster, uCount);

		return false;
		}

	return true;
	}

// End of File
