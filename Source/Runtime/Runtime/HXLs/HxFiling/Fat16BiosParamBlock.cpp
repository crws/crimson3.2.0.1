
#include "Intern.hpp"

#include "Fat16BiosParamBlock.hpp"

#include "PartitionTable.hpp"

#include "FatDirEntry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Partition Table Object
//

CFat16BiosParamBlock::CFat16BiosParamBlock(void)
{
	memset(this, 0x00, sizeof(Fat16BiosParamBlock));
	}

CFat16BiosParamBlock::CFat16BiosParamBlock(CFat16BiosParamBlock const &That)
{
	memcpy(this, &That, sizeof(Fat16BiosParamBlock));
	}

// Initialisation

void CFat16BiosParamBlock::Init(CPartitionEntry const &Partition)
{
	memset(this, 0x00, sizeof(Fat16BiosParamBlock));

	m_wSectorSize	 = 512;
	m_wReservedSize	 = 1;
	m_bFatCount	 = 2;
	m_bMedia	 = 0xF8;
	m_wRootSize      = 512;
	m_wTrackSize	 = 0;
	m_wHeadCount	 = Partition.m_bEndHead - Partition.m_bStartHead + 1;
	m_dwHiddenSize	 = Partition.m_dwRelativeSector;
	m_dwTotalSectors = Partition.m_dwTotalSectors;
	m_wTotalSectors  = Partition.m_dwTotalSectors < 0x10000 ? WORD(Partition.m_dwTotalSectors) : 0; 
	m_dwTotalSectors = Partition.m_dwTotalSectors < 0x10000 ? 0 : Partition.m_dwTotalSectors;
	m_bClusterSize	 = BYTE(FindClusterSize());
	m_wFat16Size	 = WORD(FindFatSize());
	}

// Conversion

void CFat16BiosParamBlock::HostToLocal(void)
{
	m_wSectorSize	 = HostToIntel(m_wSectorSize);
	m_wReservedSize	 = HostToIntel(m_wReservedSize);
	m_wRootSize	 = HostToIntel(m_wRootSize);
	m_wTotalSectors	 = HostToIntel(m_wTotalSectors);
	m_wFat16Size	 = HostToIntel(m_wFat16Size);
	m_wTrackSize	 = HostToIntel(m_wTrackSize);
	m_wHeadCount	 = HostToIntel(m_wHeadCount);
	m_dwHiddenSize	 = HostToIntel(m_dwHiddenSize);
	m_dwTotalSectors = HostToIntel(m_dwTotalSectors);
	}

void CFat16BiosParamBlock::LocalToHost(void)
{
	m_wSectorSize	 = IntelToHost(m_wSectorSize);
	m_wReservedSize	 = IntelToHost(m_wReservedSize);
	m_wRootSize	 = IntelToHost(m_wRootSize);
	m_wTotalSectors	 = IntelToHost(m_wTotalSectors);
	m_wFat16Size	 = IntelToHost(m_wFat16Size);
	m_wTrackSize	 = IntelToHost(m_wTrackSize);
	m_wHeadCount	 = IntelToHost(m_wHeadCount);
	m_dwHiddenSize	 = IntelToHost(m_dwHiddenSize);
	m_dwTotalSectors = IntelToHost(m_dwTotalSectors);
	}

// Attributes

BOOL CFat16BiosParamBlock::IsValid(void) const
{
	if( !m_wReservedSize ) {

		return false;
		}

	if( !m_wRootSize ) {

		return false;
		}

	if( !m_wFat16Size ) {

		return false;
		}

	if( m_wTotalSectors ) {

		return false;
		}

	if( !CheckSectorSize() ) {

		return false;
		}

	if( !CheckClusterSize() ) {

		return false;
		}

	if( GetDataClusters() < 4085 || GetDataClusters() > 65524 ) {
			
		return false;
		}

	return true;
	}

UINT CFat16BiosParamBlock::GetClusterSize(void) const
{
	return m_wSectorSize * m_bClusterSize;
	}

UINT CFat16BiosParamBlock::GetRootDirSize(void) const
{
	return DWORD(m_wRootSize) * sizeof(CFatDirEntry);
	}

UINT CFat16BiosParamBlock::GetRootDirSectors(void) const
{
	return (GetRootDirSize() + m_wSectorSize - 1) / m_wSectorSize;
	}

INT64 CFat16BiosParamBlock::GetDataSize(void) const
{
	return INT64(GetDataSectors()) * INT64(m_wSectorSize);
	}

DWORD CFat16BiosParamBlock::GetDataSectors(void) const
{
	return GetTotalSectors() - m_wReservedSize - (m_wFat16Size * m_bFatCount) - GetRootDirSectors();
	}

DWORD CFat16BiosParamBlock::GetDataClusters(void) const
{
	return GetDataSectors() / m_bClusterSize;
	}

DWORD CFat16BiosParamBlock::GetFirstSector(DWORD dwCluster) const
{
	return dwCluster * m_bClusterSize;
	}

DWORD CFat16BiosParamBlock::GetFirstFatSector(WORD wFat) const
{
	return m_wReservedSize + (m_wFat16Size * wFat);
	}

DWORD CFat16BiosParamBlock::GetFirstRootDirSector(void) const
{
	return m_wReservedSize + (m_wFat16Size * m_bFatCount);
	}

DWORD CFat16BiosParamBlock::GetFirstDataSector(void) const
{
	return m_wReservedSize + (m_wFat16Size * m_bFatCount) + GetRootDirSectors();
	}

DWORD CFat16BiosParamBlock::GetFatPerSector(void) const
{
	return m_wSectorSize / sizeof(WORD);
	}

DWORD CFat16BiosParamBlock::GetTotalSectors(void) const
{
	return m_wTotalSectors ? m_wTotalSectors : m_dwTotalSectors;
	}

// Dump

void CFat16BiosParamBlock::Dump(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nFat16 Bios Param Block\n");

	AfxTrace("Status                = %s\n",      IsValid() ? "OK" : "Invalid");
	AfxTrace("Sector Size           = %d\n",      m_wSectorSize);
	AfxTrace("Cluster Size          = %d\n",      m_bClusterSize);
	AfxTrace("Reserved Size         = %d\n",      m_wReservedSize);
	AfxTrace("Number of FATs        = %d\n",      m_bFatCount);
	AfxTrace("Root Size             = %d\n",      m_wRootSize);
	AfxTrace("Track Size            = %d\n",      m_wTrackSize);
	AfxTrace("Head Count            = %d\n",      m_wHeadCount);
	AfxTrace("Hidden Size           = 0x%8.8X\n", m_dwHiddenSize);
	AfxTrace("Total Size            = %u\n",      m_dwTotalSectors);
	AfxTrace("Cluster Size          = %d\n",      GetClusterSize());
	AfxTrace("Fat Size              = 0x%4.4X\n", m_wFat16Size);
	AfxTrace("Root Dir Secs         = %d\n",      GetRootDirSectors());
	AfxTrace("Data Size             = %llu\n",    GetDataSize());
	AfxTrace("Data Secs             = %u\n",      GetDataSectors());
	AfxTrace("Data Clusters         = %u\n",      GetDataClusters());
	AfxTrace("First FAT Sec0        = 0x%8.8X\n", GetFirstFatSector(0));
	AfxTrace("First FAT Sec1        = 0x%8.8X\n", GetFirstFatSector(1));
	AfxTrace("First Root Dir Sector = 0x%8.8X\n", GetFirstRootDirSector());
	AfxTrace("First Data Sector     = 0x%8.8X\n", GetFirstDataSector());

	#endif
	}

// Implementation

BOOL CFat16BiosParamBlock::CheckSectorSize(void) const
{
	switch( m_wSectorSize ) {

		case 512:
		case 1024:
		case 2048:
		case 4096:

			return true;
		}

	return false;
	}


BOOL CFat16BiosParamBlock::CheckClusterSize(void) const
{
	switch( m_bClusterSize ) {

		case 1:
		case 2:
		case 4:
		case 8:
		case 16:
		case 32:
		case 64:
		case 128:

			return m_wSectorSize * m_bClusterSize <= (32 * 1024);
		}

	return false;
	}

UINT CFat16BiosParamBlock::FindClusterSize(void) const
{
	struct CLookup
	{
		DWORD dwTotal;
		BYTE  bSize;
		
		};

	static CLookup const Lookup[] = {

		{       8400,  0 },	
		{      32680,  2 },	
		{     262144,  4 },	
		{     524288,  8 },	
		{    1048576, 16 },	
		{    2097152, 32 }, 
		{    4194304, 64 }, 
		{ 0xFFFFFFFF,  0 }

		};

	for( int n = 0; n < elements(Lookup); n ++ ) {
		
		CLookup const &Entry = Lookup[n];

		if( m_dwTotalSectors <= Entry.dwTotal ) {

			return Entry.bSize;
			}
		}
	
	return 0;
	}

WORD CFat16BiosParamBlock::FindFatSize(void) const
{
	DWORD dwTmpVal1 = GetTotalSectors() - (m_wReservedSize + GetRootDirSectors());

	DWORD dwTmpVal2 = 256 * m_bClusterSize + m_bFatCount;

	return WORD((dwTmpVal1 + dwTmpVal2 - 1) / dwTmpVal2);
	}

// End of File