
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_VolumeManager_HPP

#define	INCLUDE_VolumeManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Volume Manager Object
//

class CVolumeManager : public IVolumeManager
{
	public:
		// Constructor
		CVolumeManager(void);

		// Destructor
		~CVolumeManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Methods
		UINT		METHOD FormatVolume(UINT iDisk, CHAR cDrive, UINT uBoot, UINT uSize);
		UINT		METHOD MountVolume(UINT iDisk, CHAR cDrive, UINT uBoot);
		bool		METHOD UnMountVolume(UINT iVolume);
		bool		METHOD UnMountVolumes(void);
		bool		METHOD IsVolumeMounted(UINT iVolume);
		CHAR		METHOD GetDriveLetter(UINT iVolume);
		IFilingSystem * METHOD GetFileSystem(void);
		IFilingSystem * METHOD GetFileSystem(UINT iVolume);
		IFilingSystem * METHOD GetTaskVolume(void);
		bool		METHOD SetTaskVolume(IFilingSystem *pFilingSystem);

	protected:
		// File System Types
		enum
		{
			typeFree,
			typeUnknown,
			typeFat12,
			typeFat16,
			typeFat32,
			};

		// Volume Info
		struct CVolume
		{
			char		m_sName[32];
			UINT		m_uType;
			UINT		m_iDisk;
			CHAR            m_cDrive;
			IFilingSystem * m_pSystem;
			IBlockCache   * m_pCache;
			IBlockDevice  * m_pBlock;
			DWORD 		m_dwBoot;
			DWORD		m_dwSize;
			};

		// Data
		ULONG	       m_uRefs;
		IMutex       * m_pMutex;
		IDiskManager * m_pManager;
		CVolume        m_Volumes[16];

		// Implementation
		void FindDiskManager(void);
		BOOL InitVolumeType(UINT iVolume);
		BOOL FindVolumeType(UINT iVolume);
		BOOL InitBootSector(UINT iVolume, PBYTE pBoot);
		BOOL InitBootFat16(UINT iVolume, PBYTE pBoot);
		BOOL InitBootFat32(UINT iVolume, PBYTE pBoot);
		UINT ParseBootSector(PBYTE pBoot) const;
		BOOL ParseBootFat16(PBYTE pBoot) const;
		BOOL ParseBootFat32(PBYTE pBoot) const;

		// File System Creation
		BOOL IsFileSystemSupported(UINT iVolume) const;
		void MakeFileSystem(UINT iVolume);
		void InitFileSystem(UINT iVolume);
		void KillFileSystem(UINT iVolume);
		void StartFileSystem(UINT iVolume);
		void StopFileSystem(UINT iVolume);

		// Slot Handling
		void InitSlots(void);
		UINT FindFreeSlot(void);
		void FreeSlot(UINT iSlot);
	};

// End of File

#endif
