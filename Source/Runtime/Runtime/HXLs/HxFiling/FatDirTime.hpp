
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FatDirTime_HPP

#define	INCLUDE_FatDirTime_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Fat Time Structure 
//

#pragma pack(1)

struct FatTime 
{
	WORD  m_wSec  : 5;
	WORD  m_wMin  : 6;
	WORD  m_wHour : 5;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat Time Object 
//

class CFatTime : public FatTime
{
	public:
		// Constructor
		CFatTime(void);

		// Attributes
		BOOL IsValid(void) const;

		// Operations
		time_t Get(void) const;
		void   Set(time_t time);
//		void  Get(CTime &Time) const;
//		void  Set(CTime const &Time);
	};

//////////////////////////////////////////////////////////////////////////
//
// Fat Date Structure 
//

#pragma pack(1)

struct FatDate 
{
	WORD  m_wDate  : 5;
	WORD  m_wMonth : 4;
	WORD  m_wYear  : 7;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat Date Object 
//

class CFatDate : public FatDate
{
	public:
		// Constructor
		CFatDate(void);

		// Attributes
		BOOL IsValid(void) const;

		// Operations
		time_t Get(void) const;
		void   Set(time_t time);
//		void  Get(CTime &Time) const;
//		void  Set(CTime const &Time);
	};

// End of File

#endif

