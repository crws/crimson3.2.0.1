
#include "Intern.hpp"

#include "FatDirLong.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory Long Entry
//

// Constructor

CFatDirLong::CFatDirLong(void)
{
	}

// Conversion

void CFatDirLong::HostToLocal(void)
{
	HostToLocal(m_wName1, elements(m_wName1)); 

	HostToLocal(m_wName2, elements(m_wName2)); 

	HostToLocal(m_wName3, elements(m_wName3)); 
	}

void CFatDirLong::LocalToHost(void)
{
	LocalToHost(m_wName1, elements(m_wName1)); 

	LocalToHost(m_wName2, elements(m_wName2)); 

	LocalToHost(m_wName3, elements(m_wName3)); 
	}

// Attributes

BOOL CFatDirLong::IsValid(void) const
{
	if( IsEmpty() ) {

		return false;
		}
	
	if( IsFree() ) {

		return true;
		}
	
	if( !IsLong() ) {

		return false;
		}

	if( !CheckText(m_wName1, elements(m_wName1)) ) {

		return false;
		}

	if( !CheckText(m_wName2, elements(m_wName2)) ) {

		return false;
		}

	if( !CheckText(m_wName3, elements(m_wName3)) ) {

		return false;
		}

	return true;
	}

BOOL CFatDirLong::IsEmpty(void) const
{
	return m_bOrder == 0;
	}

BOOL CFatDirLong::IsFree(void) const
{
	return m_bOrder == constFree;
	}

BOOL CFatDirLong::IsLong(void) const
{
	return ((m_bAttribute & attrLongMask) == attrLong) && m_bOrder != constFree;
	}

BOOL CFatDirLong::IsLast(void) const
{
	return (m_bOrder & constLastLong) == constLastLong;
	}

BOOL CFatDirLong::IsFirst(void) const
{
	return (m_bOrder & ~constLastLong) == 1;
	}

BYTE CFatDirLong::GetOrder(void) const
{
	return m_bOrder & ~constLastLong;
	}

// Operations

void CFatDirLong::MakeEmpty(void)
{
	m_bOrder     = 0;

	m_bAttribute = attrLong;

	m_bType      = 0;

	m_bChecksum  = 0;

	m_wZero      = 0;
	
	ClearText(m_wName1, elements(m_wName1));

	ClearText(m_wName2, elements(m_wName2));

	ClearText(m_wName3, elements(m_wName3));
	}

void CFatDirLong::MakeFree(void) 
{
	memset(this, 0, sizeof(FatDirLong));
	}

void CFatDirLong::MakeLast(void)
{
	m_bOrder |= constLastLong;
	}

UINT CFatDirLong::GetLength(void) const
{
	UINT uLen = 0;

	uLen += GetTextLength(m_wName1, elements(m_wName1));
	
	uLen += GetTextLength(m_wName2, elements(m_wName2));
	
	uLen += GetTextLength(m_wName3, elements(m_wName3));
	
	return uLen;
	}

BOOL CFatDirLong::GetText(PTXT pName) const
{
	pName += GetText(pName, m_wName1, elements(m_wName1));

	pName += GetText(pName, m_wName2, elements(m_wName2));

	pName += GetText(pName, m_wName3, elements(m_wName3));
	
	return true;
	}

UINT CFatDirLong::SetText(PCTXT pText)
{
	UINT uLen = strlen(pText) + 1;

	UINT uPtr = 0;
	
	ClearText(m_wName1, elements(m_wName1));

	ClearText(m_wName2, elements(m_wName2));

	ClearText(m_wName3, elements(m_wName3));

	uPtr += SetText(pText + uPtr, m_wName1, Min(elements(m_wName1), uLen - uPtr));
	
	uPtr += SetText(pText + uPtr, m_wName2, Min(elements(m_wName2), uLen - uPtr));
	
	uPtr += SetText(pText + uPtr, m_wName3, Min(elements(m_wName3), uLen - uPtr));

	return GetLength();
	}

// Dump

void CFatDirLong::Dump(void) const
{
	#if defined(_XDEBUG)

	char sText[14];

	GetText(sText +  0, m_wName1, elements(m_wName1));
	GetText(sText +  5, m_wName2, elements(m_wName2));
	GetText(sText + 11, m_wName3, elements(m_wName3));
	
	AfxTrace("\nFat Directory Long Entry\n");

	AfxTrace("Order               = 0x%2.2X\n", m_bOrder);
	AfxTrace("Attributes          = 0x%2.2X\n", m_bAttribute);
	AfxTrace("Type                = 0x%2.2X\n", m_bType);
	AfxTrace("Checksum            = 0x%2.2X\n", m_bChecksum);
	AfxTrace("Name                = %s\n",      sText);
	
	#endif
	}

// Implementation

void CFatDirLong::HostToLocal(PWORD pData, UINT uCount)
{
	for( UINT i = 0; i < uCount; i ++ ) {

		pData[i] = HostToIntel(pData[i]);
		}
	}

void CFatDirLong::LocalToHost(PWORD pData, UINT uCount)
{
	for( UINT i = 0; i < uCount; i ++ ) {

		pData[i] = IntelToHost(pData[i]);
		}
	}

BOOL CFatDirLong::CheckText(PCWORD pData, UINT uCount) const
{
	for( UINT u = 0; u < uCount && pData[u]; u ++ ) {
		
		WORD w = pData[u];
	
		if( w && w < 0x20 ) {

			return false;
			}

		if( w == 0x05 && u == 0 ) {

			continue;
			}

		switch( w ) {

			case 0x22 :
			case 0x2A :
			case 0x2F :
			case 0x3A :
			case 0x3C :
			case 0x3E :
			case 0x3F :
			case 0x5C :
			case 0x7C :

				return false;
			}
		}

	return true;
	}

void CFatDirLong::ClearText(PWORD pData, UINT uCount)
{
	memset(pData, 0xFF, uCount * sizeof(WORD));
	}

UINT CFatDirLong::GetTextLength(PCWORD pData, UINT uCount) const
{
	for( UINT i = 0; i < uCount; i ++ ) {

		if( !pData[i] || pData[i] == 0xFFFF ) {

			return i;
			}
		}

	return uCount;
	}

UINT CFatDirLong::GetText(PTXT pText, PCWORD pData, UINT uCount) const
{
	for( UINT i = 0; i < uCount; i ++ ) {

		if( !pData[i] || pData[i] == 0xFFFF ) {

			*pText = 0;
			
			return i;
			}

		*pText++ = LOBYTE(pData[i]);
		}

	return uCount;
	}

UINT CFatDirLong::SetText(PCTXT pText, PWORD pData, UINT uCount)
{
	for( UINT i = 0; i < uCount; i ++ ) { 

		pData[i] = pText[i];
		}

	return uCount;
	}

// End of File