
#include "Intern.hpp"

#include "BlockCache.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Block Cache Object
//

// Instantiator

IBlockCache * Create_BlockCache(IBlockDevice *pDev, UINT uPolicy)
{
	CBlockCache *p = New CBlockCache(pDev, uPolicy);

	return p;
	}

// Constructor

CBlockCache::CBlockCache(IBlockDevice *pDev, UINT uPolicy)
{
	StdSetRef();

	m_pDev    = pDev;

	m_uPolicy = uPolicy;
	
	m_pMutex  = Create_Mutex();

	CacheAlloc();
	}

// Destructor

CBlockCache::~CBlockCache(void)
{
	CacheKill();
	}

// IUnknown

HRESULT CBlockCache::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IBlockCache);

	StdQueryInterface(IBlockCache);

	return E_NOINTERFACE;
	}

ULONG CBlockCache::AddRef(void)
{
	StdAddRef();
	}

ULONG CBlockCache::Release(void)
{
	StdRelease();
	}

// Methods

UINT CBlockCache::GetSectorCount(void)
{
	return m_pDev->GetSectorCount();
	}

UINT CBlockCache::GetSectorSize(void)
{
	return m_pDev->GetSectorSize();
	}

BOOL CBlockCache::Flush(void)
{
	m_pMutex->Wait(FOREVER);

	for( UINT i = 0; i < constP1; i ++ ) {

		if( !CacheCommitSlot(i) ) {

			m_pMutex->Free();

			return FALSE;
			}
		}

	m_pMutex->Free();
	
	return TRUE;
	}

void CBlockCache::Invalidate(void)
{
	m_pMutex->Wait(FOREVER);

	for( UINT i = 0; i < constP1; i ++ ) {

		CEntry &Entry = m_Cache[i];

		Entry.m_uSector = NOTHING;
		}

	m_pMutex->Free();
	}

PBYTE CBlockCache::LockSector(UINT uSector, BOOL fSkipRead)
{
	m_pMutex->Wait(FOREVER);

	UINT uSlot = CacheFindEntry(uSector);

	if( uSlot == NOTHING ) {

		uSlot = CacheFindSpare(uSector);

		if( uSlot == NOTHING ) {

			uSlot = CacheFindOldest(uSector);

			if( uSlot == NOTHING ) {

				m_pMutex->Free();

				return NULL;
				}
			}

		CEntry &Entry = m_Cache[uSlot];

		if( fSkipRead || (m_pDev && m_pDev->ReadSector(uSector, Entry.m_pData)) ) {

			Entry.m_uSector = uSector;

			Entry.m_uLock   = 1;

			Entry.m_uTime   = GetTickCount();

			Entry.m_fDirty  = FALSE;

			m_pMutex->Free();

			return Entry.m_pData;
			}
		}
	else {
		CEntry &Entry = m_Cache[uSlot];

		Entry.m_uLock = Entry.m_uLock + 1;

		Entry.m_uTime =  GetTickCount();

		m_pMutex->Free();

		return Entry.m_pData;
		}

	m_pMutex->Free();

	return NULL;
	}

BOOL CBlockCache::CommitSector(UINT uSector)
{
	m_pMutex->Wait(FOREVER);

	UINT uSlot = CacheFindEntry(uSector);

	if( uSlot != NOTHING ) {

		CEntry &Entry = m_Cache[uSlot];

		if( Entry.m_uLock ) {
			
			if( Entry.m_fDirty ) {

				CacheCommitSlot(uSlot);
				}

			Entry.m_uLock--;

			m_pMutex->Free();

			return TRUE;
			}
		}

	m_pMutex->Free();

	return false;
	}

BOOL CBlockCache::UnlockSector(UINT uSector, BOOL fDirty)
{
	m_pMutex->Wait(FOREVER);

	UINT uSlot = CacheFindEntry(uSector);

	if( uSlot != NOTHING ) {

		CEntry &Entry = m_Cache[uSlot];

		if( Entry.m_uLock ) {

			if( fDirty ) {

				if( !Entry.m_fDirty ) {

					Entry.m_fDirty = TRUE;
					}

				if( m_uPolicy == cacheWriteThrough ) {

					CacheCommitSlot(uSlot);
					}
				}

			Entry.m_uLock--;

			m_pMutex->Free();

			return TRUE;
			}
		}

	m_pMutex->Free();

	return false;
	}

// Implementation

void CBlockCache::CacheAlloc(void)
{
	for( UINT i = 0; i < constP1; i ++ ) {

		CEntry &Entry = m_Cache[i];

		Entry.m_uSector = NOTHING;

		Entry.m_pData   = New BYTE[ GetSectorSize() ];
		}
	}

void CBlockCache::CacheKill(void)
{
	for( UINT i = 0; i < constP1; i ++ ) {

		CEntry &Entry = m_Cache[i];

		delete [] Entry.m_pData;
		}
	}

UINT CBlockCache::CacheFindEntry(UINT uSector)
{
	UINT i = uSector % constP1;

	for( UINT n = 0; n < constP1; n++ ) {

		CEntry &Entry = m_Cache[i];

		if( Entry.m_uSector == uSector ) {

			return i;
			}

		i = ((i + constP2) % constP1);
		}

	return NOTHING;
	}

UINT CBlockCache::CacheFindSpare(UINT uSector)
{
	UINT i = uSector % constP1;

	for( UINT n = 0; n < constP1; n++ ) {

		CEntry &Entry = m_Cache[i];

		if( Entry.m_uSector == NOTHING ) {

			return i;
			}

		i = ((i + constP2) % constP1);
		}

	return NOTHING;
	}

UINT CBlockCache::CacheFindOldest(UINT uSector)
{
	for(;;) {

		UINT uOld  = NOTHING;

		UINT uTime =  GetTickCount();

		UINT uDead = ToTicks(1000);

		UINT uMax  = 0;

		UINT i     = uSector % constP1;

		for( UINT n = 0; n < constP1; n++ ) {

			CEntry &Entry = m_Cache[i];

			if( !Entry.m_uLock ) {

				UINT uAge = uTime - Entry.m_uTime;

				if( uAge >= uDead ) {

					if( CacheFreeSlot(i) ) {

						return i;
						}
					}

				if( uAge >= uMax ) {

					uMax = uAge;

					uOld = i;
					}
				}

			i = ((i + constP2) % constP1);
			}

		if( uOld < NOTHING ) {

			if( CacheFreeSlot(uOld) ) {

				return uOld;
				}

			m_Cache[uOld].m_uTime = uTime;

			continue;
			}

		return NOTHING;
		}
	}

BOOL CBlockCache::CacheCommitSlot(UINT uSlot)
{
	CEntry &Entry = m_Cache[uSlot];

	if( Entry.m_uSector < NOTHING ) {

		if( Entry.m_fDirty ) {

			if( !m_pDev->WriteSector(Entry.m_uSector, Entry.m_pData) ) {

				return FALSE;
				}

			Entry.m_fDirty = FALSE;
			}
		}

	return TRUE;
	}

BOOL CBlockCache::CacheFreeSlot(UINT uSlot)
{
	CEntry &Entry = m_Cache[uSlot];

	if( Entry.m_uSector < NOTHING ) {

		if( CacheCommitSlot(uSlot) ) {

			Entry.m_uSector = NOTHING;

			return true;
			}

		return false;
		}

	return true;
	}

// End of File