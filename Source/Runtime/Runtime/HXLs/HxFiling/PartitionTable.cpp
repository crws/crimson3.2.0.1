
#include "Intern.hpp"

#include "PartitionTable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Partition Table Object
//

CPartitionEntry::CPartitionEntry(void)
{
	Init();
	}

// Initialisation

void CPartitionEntry::Init(void)
{
	memset(this, 0, sizeof(Partition));

	m_bBootable = 1;
	
	m_bFileSys  = typeFat32L;
	}

// Conversion

void CPartitionEntry::HostToLocal(void)
{
	m_dwRelativeSector = HostToIntel(m_dwRelativeSector);

	m_dwTotalSectors   = HostToIntel(m_dwTotalSectors);
	}

void CPartitionEntry::LocalToHost(void)
{
	m_dwRelativeSector = IntelToHost(m_dwRelativeSector);

	m_dwTotalSectors   = IntelToHost(m_dwTotalSectors);
	}

// Attributes

BOOL CPartitionEntry::IsValid(void) const
{
	switch( m_bFileSys ) {
		
		case typeFat12:
		case typeFat16S:
		case typeFat16L:
		case typeFat32:
		case typeFat32L:
		case typeFat16B:

			return true;
		}

	return false;
	}

BYTE CPartitionEntry::GetStartSector(void) const
{
	return m_bStartSector & 0x3F;
	}

WORD CPartitionEntry::GetStartCylinder(void) const
{
	return WORD(m_bStartCylinder) | (WORD(m_bStartSector & 0xC0) << 2);
	}

BYTE CPartitionEntry::GetEndSector(void) const
{
	return m_bEndSector & 0x3F;
	}

WORD CPartitionEntry::GetEndCylinder(void) const
{
	return WORD(m_bEndCylinder) | (WORD(m_bEndSector & 0xC0) << 2);
	}

// Operations

void CPartitionEntry::SetBeg(WORD wCylinder, BYTE bHead, BYTE bSector)
{
	m_bStartHead     = bHead;

	m_bStartCylinder = BYTE(wCylinder);

	m_bStartSector   = (bSector & 0x3F) | BYTE((wCylinder >> 2) & 0xC0);
	}

void CPartitionEntry::SetEnd(WORD wCylinder, BYTE bHead, BYTE bSector)
{
	m_bEndHead     = bHead;

	m_bEndCylinder = BYTE(wCylinder);

	m_bEndSector   = (bSector & 0x3F) | BYTE((wCylinder >> 2) & 0xC0);
	}

// Debug

void CPartitionEntry::Dump(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("File System Partition Table\n");

	AfxDump(PBYTE(this), sizeof(Partition));

	AfxTrace("Status         %s\n", IsValid() ? "OK" : "Invalid");
	AfxTrace("Filing System  %d\n", m_bFileSys);
	AfxTrace("Start Head     %d\n", m_bStartHead);
	AfxTrace("Start Sector   %d\n", GetStartSector());
	AfxTrace("Start Cylinder %d\n", GetStartCylinder());
	AfxTrace("End Head       %d\n", m_bEndHead);
	AfxTrace("End Sector     %d\n", GetEndSector());
	AfxTrace("End Cylinder   %d\n", GetEndCylinder());
	AfxTrace("Preceeding     %d\n", m_dwRelativeSector);
	AfxTrace("Size           %d\n", m_dwTotalSectors);
	
	#endif
	}

// End of File