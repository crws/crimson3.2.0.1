
#include "Intern.hpp"

#include "Fat32FSInfo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat32 FS Info Structure
//

// Constructor

CFat32FSInfo::CFat32FSInfo(void)
{
	}

// Initialisation

void CFat32FSInfo::Init(void)
{
	memset(this, 0x00, sizeof(Fat32FSInfo));

	m_dwLeadSig   = constLeadSig;
	
	m_dwStrucSig  = constStrucSig;
	
	m_dwTrailSig  = constTrailSig;

	m_dwNextFree  = 0xFFFFFFFF;

	m_dwFreeCount = 0xFFFFFFFF;
	}

// Conversion

void CFat32FSInfo::HostToLocal(void)
{
	m_dwLeadSig   = HostToIntel(m_dwLeadSig);
	m_dwStrucSig  = HostToIntel(m_dwStrucSig);
	m_dwFreeCount = HostToIntel(m_dwFreeCount);
	m_dwNextFree  = HostToIntel(m_dwNextFree);
	m_dwTrailSig  = HostToIntel(m_dwTrailSig);
	}

void CFat32FSInfo::LocalToHost(void)
{
	m_dwLeadSig   = IntelToHost(m_dwLeadSig);
	m_dwStrucSig  = IntelToHost(m_dwStrucSig);
	m_dwFreeCount = IntelToHost(m_dwFreeCount);
	m_dwNextFree  = IntelToHost(m_dwNextFree);
	m_dwTrailSig  = IntelToHost(m_dwTrailSig);
	}

// Attributes

BOOL CFat32FSInfo::IsValid(void) const
{
	if( m_dwLeadSig != constLeadSig ) {
		
		return false;
		}
	
	if( m_dwStrucSig != constStrucSig ) {
		
		return false;
		}
	
	if( m_dwTrailSig != constTrailSig ) {

		return false;
		}

	return true;
	}

// Dump

void CFat32FSInfo::Dump(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nFat32 FSInfo Sector\n");

	AfxTrace("Status              = %s\n",      IsValid() ? "OK" : "Invalid");
	AfxTrace("Lead Signature      = 0x%8.8X\n", m_dwLeadSig);
	AfxTrace("Struc Signature     = 0x%8.8X\n", m_dwStrucSig);
	AfxTrace("Free Count          = 0x%8.8X\n", m_dwFreeCount);
	AfxTrace("Next Free           = 0x%8.8X\n", m_dwNextFree);
	AfxTrace("Trail Signature     = 0x%8.8X\n", m_dwTrailSig);
	
	#endif
	}

// End of File