
#include "Intern.hpp"

#include "TaskLocalStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Task Volume 
//

CTaskVolume::CTaskVolume(void) : CThreadData(TLS_FILE_VOL)
{
	m_iVolume = NOTHING;
	}

//////////////////////////////////////////////////////////////////////////
//
// Task Directory 
//

CTaskDirectory::CTaskDirectory(void) : CThreadData(TLS_FILE_DIR) 
{
	m_Index.m_iCluster = 0;

	m_Index.m_iIndex   = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Task Drive 
//

CTaskDrive::CTaskDrive(void) : CThreadData(TLS_FILE_DRIVE)
{
	m_cDrive = NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Task Path 
//

CTaskPath::CTaskPath(void) : CThreadData(TLS_FILE_PATH)
{
	m_cDrive           = NULL;

	m_iVolume          = NOTHING;

	m_Index.m_iCluster = 0;

	m_Index.m_iIndex   = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Task Ref 
//

// Constructor

CTaskRef::CTaskRef(void) : CThreadData(TLS_FILE_REF)
{
	m_pRef = NULL;

	m_uRef = 0;
	}

// Destructor

CTaskRef::~CTaskRef(void)
{
	AfxRelease(m_pRef);
	}

// End of File
