
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_MasterBootRecord_HPP

#define	INCLUDE_MasterBootRecord_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PartitionTable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Record
//

#pragma pack(1)

struct MasterBootRecord
{
	BYTE	   m_bCode[446];
	Partition  m_Partitions[4];
	WORD	   m_wMagic;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Partition Entry
//

class CMasterBootRecord : public MasterBootRecord
{
	public:
		// Constructor
		CMasterBootRecord(void);

		// Initialisation
		void Init(void);
		void Init(PCBYTE pBoot, UINT uSize);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL IsValid(void) const;
		
		// Operations
		CPartitionEntry const & GetPartition(UINT iIndex) const;

	protected:
		// Constants
		enum
		{
			constMagic = 0xAA55,
			};
	};

// End of File

#endif

