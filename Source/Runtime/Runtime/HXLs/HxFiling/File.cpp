
#include "Intern.hpp"

#include "File.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Instantiators

static IUnknown * Create_File(PCTXT pName)
{
	return New CFile();
	}

global IFile * Create_File(IFilingSystem *pSystem)
{
	CFile *pFile = New CFile(pSystem);

	return pFile;
	}

// Registration

global void Register_File(void)
{
	piob->RegisterInstantiator("fs.file", Create_File);
	}

// Constructor

CFile::CFile(void)
{
	StdSetRef();

	m_pVolMan  = NULL;

	m_pDiskMan = NULL;

	m_pFileMan = NULL;
	
	m_pSystem  = NULL;

	m_hFile    = NULL;

	m_cDrive   = NULL;

	AfxGetObject("fs.volman",  0, IVolumeManager, m_pVolMan);

	AfxGetObject("fs.diskman", 0, IDiskManager,   m_pDiskMan);

	AfxGetObject("fs.fileman", 0, IFileManager,   m_pFileMan);
	}

CFile::CFile(IFilingSystem *pSystem)
{
	m_pVolMan  = NULL;

	m_pDiskMan = NULL;

	m_pFileMan = NULL;
	
	m_pSystem  = NULL;

	m_hFile    = NULL;

	m_cDrive   = NULL;

	AfxGetObject("fs.volman",  0, IVolumeManager, m_pVolMan);

	AfxGetObject("fs.diskman", 0, IDiskManager,   m_pDiskMan);

	AfxGetObject("fs.fileman", 0, IFileManager,   m_pFileMan);

	Attach(pSystem);
	}

// Destrictor

CFile::~CFile(void)
{
	Close();

	Detach();

	AfxRelease(m_pVolMan);

	AfxRelease(m_pDiskMan);

	AfxRelease(m_pFileMan);
	}

// IUnknown

HRESULT CFile::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IFile);

	StdQueryInterface(IFile);

	return E_NOINTERFACE;
	}

ULONG CFile::AddRef(void)
{
	StdAddRef();
	}

ULONG CFile::Release(void)
{
	StdRelease();
	}

// IFile Initialization

BOOL CFile::Attach(IFilingSystem *pFileSystem)
{
	Detach();

	if( m_pFileMan->RegisterTask(pFileSystem) ) {

		m_cDrive  = pFileSystem->GetDrive();

		m_pSystem = pFileSystem;

		return TRUE;
		}

	return FALSE;
	}

// IFile Attributes

HFILE CFile::GetHandle(void)
{
	return m_hFile;
	}

UINT CFile::GetLength(void)
{
	if( IsValid() ) {

		return m_pSystem->FileGetLength(m_hFile);
		}

	return 0;
	}

// IFile Management

BOOL CFile::Create(CFilename const &Name)
{
	Close();
	
	if( FindFileSystem(Name) ) {

		m_hFile = m_pSystem->CreateFile(Name);

		if( IsValid() ) { 

			m_uPos  = 0;

			return true;
			}
		}

	Detach();

	return false;
	}

BOOL CFile::Create(CFilename const &Name, FSIndex const &Dir)
{
	Close();

	if( FindFileSystem(Name) ) {

		m_hFile = m_pSystem->CreateFile(Name, Dir);

		if( IsValid() ) { 

			m_uPos  = 0;

			return true;
			}
		}

	Detach();

	return false;
	}

BOOL CFile::Open(CFilename const &Name, UINT Mode)
{
	Close();

	if( Mode & fileWrite ) {

		Mode |= fileRead;
		}

	if( FindFileSystem(Name) ) {

		m_hFile = m_pSystem->OpenFile(Name, Mode);

		if( IsValid() ) { 

			m_uPos  = 0;

			return true;
			}
		}

	Detach();

	return false;
	}

BOOL CFile::Open(CFilename const &Name, FSIndex const &Dir, UINT Mode)
{
	Close();

	if( Mode & fileWrite ) {

		Mode |= fileRead;
		}

	if( FindFileSystem(Name) ) {

		m_hFile = m_pSystem->OpenFile(Name, Dir, Mode);

		if( IsValid() ) { 

			m_uPos  = 0;

			return true;
			}
		}

	Detach();

	return false;
	}

BOOL CFile::Open(FSIndex const &Index, UINT Mode)
{
	Close();

	if( Mode & fileWrite ) {

		Mode |= fileRead;
		}

	if( FindFileSystem(CFilename("")) ) {

		m_hFile = m_pSystem->OpenFile(Index, Mode);

		if( IsValid() ) { 

			m_uPos  = 0;

			return true;
			}
		}

	Detach();

	return false;
	}

BOOL CFile::Close(void)
{
	if( IsValid() ) {

		m_pSystem->CloseFile(m_hFile);

		m_hFile = NULL;

		Detach();

		return true;
		}

	Detach();

	return false;
	}

BOOL CFile::Delete(void)
{
	if( IsValid() ) {

		m_pSystem->DeleteFile(m_hFile);

		Close();

		return true;
		}
	
	return false;
	}

BOOL CFile::SetLength(UINT uLength)
{
	if( IsValid() ) {

		return m_pSystem->FileSetLength(m_hFile, uLength);
		}

	return false;
	}

BOOL CFile::Rename(CFilename const &Name)
{
	if( IsValid() ) {

		return m_pSystem->RenameFile(m_hFile, Name);
		}

	return false;
	}

// IFile I/O

UINT CFile::Read(PBYTE pData, UINT uCount)
{
	if( IsValid() ) {

		uCount = m_pSystem->FileReadData(m_hFile, pData, m_uPos, uCount);

		if( uCount != NOTHING ) {

			m_uPos += uCount;

			return uCount;
			}
		}

	return NOTHING;
	}

UINT CFile::Write(PCBYTE pData, UINT uCount)
{
	if( IsValid() ) {

		uCount = m_pSystem->FileWriteData(m_hFile, pData, m_uPos, uCount);

		if( uCount != NOTHING ) {

			m_uPos += uCount;

			return uCount;
			}
		}

	return NOTHING;
	}

BOOL CFile::SetPos(INT nOffset, UINT Mode)
{
	INT nPos;

	switch( Mode ) {

		case seekBeg:

			nPos = 0;

			break;

		case seekCur:

			if( !nOffset ) {

				return true;
				}

			nPos = INT(m_uPos);

			break;

		case seekEnd:

			nPos = INT(GetLength());

			break;

		default:

			return false;
		}

	if( nOffset > 0 ) {

		INT nLength = INT(GetLength());

		if( nPos + nOffset >= nLength ) {

			m_uPos = UINT(nLength);

			return true;
			}
		}

	if( nOffset < 0 ) {

		if( nOffset + nPos < 0 ) {

			m_uPos = UINT(0);

			return true;
			}
		}

	m_uPos = UINT(nPos + nOffset);

	return true;
	}

BOOL CFile::SetBeg(void)
{
	m_uPos = 0;

	return true;
	}

BOOL CFile::SetEnd(void)
{
	m_uPos = GetLength();

	return true;
	}

UINT CFile::GetPos(void)
{
	return m_uPos;
	}

BOOL CFile::SetUnixTime(time_t time)
{
	return m_pSystem->FileSetUnixTime(m_hFile, time);
	}

time_t CFile::GetUnixTime(void)
{
	return m_pSystem->FileGetUnixTime(m_hFile);
	}

// Implementation

BOOL CFile::IsValid(void)
{
	return m_hFile != NULL;
	}

BOOL CFile::FindFileSystem(CFilename const &Filename)
{
	if( Filename.HasDrive() ) {

		Detach();

		m_cDrive  = Filename.GetDrive();

		m_pSystem = m_pFileMan->FindFileSystem(m_cDrive);

		return m_pSystem != NULL;
		}

	if( m_pSystem ) {

		if( !m_pSystem->IsMounted() ) {

			m_cDrive = m_pSystem->GetDrive();

			Detach();

			m_pSystem = m_pFileMan->FindFileSystem(m_cDrive);
			}
		}

	if( !m_pSystem ) {
		
		if( m_cDrive != CHAR(NULL) ) {

			m_pSystem = m_pFileMan->FindFileSystem(m_cDrive);
			}
		}

	if( !m_pSystem ) {

		m_pSystem = m_pFileMan->FindFileSystem();
		}

	return m_pSystem && m_pSystem->IsMounted();
	}

void CFile::Detach(void)
{
	m_pFileMan->FreeFileSystem(m_pSystem, false);

	m_pSystem = NULL;
	}

// End of File
