
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Fat32BiosParamBlock_HPP

#define	INCLUDE_Fat32BiosParamBlock_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Objects
//

class CPartitionEntry;

//////////////////////////////////////////////////////////////////////////
//
// Bios Param Block
//

#pragma pack(1)

struct Fat32BiosParamBlock
{
	WORD	m_wSectorSize;
	BYTE	m_bClusterSize;
	WORD	m_wReservedSize;
	BYTE	m_bFatCount;
	WORD	m_wRootSize;
	WORD	m_wTotalSectors;
	BYTE	m_bMedia;
	WORD	m_wFat16Size;
	WORD	m_wTrackSize;
	WORD	m_wHeadCount;
	DWORD	m_dwHiddenSize;
	DWORD	m_dwTotalSectors;
	DWORD	m_dwFat32Size;
	WORD	m_wFlags;
	WORD	m_wVersion;
	DWORD	m_dwRootCluster;
	WORD	m_wFSInfoSector;
	WORD	m_wBootSector;
	BYTE	m_bReserved[12];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat32 Bios Param Block
//

class CFat32BiosParamBlock : public Fat32BiosParamBlock
{
	public:
		// Constructor
		CFat32BiosParamBlock(void);
		CFat32BiosParamBlock(CFat32BiosParamBlock const &That);

		// Initialisation
		void Init(CPartitionEntry const &Partition);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL  IsValid(void) const;
		UINT  GetClusterSize(void) const;
		UINT  GetRootDirSectors(void) const;
		INT64 GetDataSize(void) const;
		DWORD GetDataSectors(void) const;
		DWORD GetDataClusters(void) const;
		DWORD GetFirstSector(DWORD dwCluster) const;
		DWORD GetFirstFatSector(WORD wFat) const;
		DWORD GetFirstDataSector(void) const;
		DWORD GetFatPerSector(void) const;

		// Dump
		void Dump(void) const;

	protected:
		// Implementation
		BOOL  CheckSectorSize(void) const;
		BOOL  CheckClusterSize(void) const;
		UINT  FindClusterSize(void) const;
		DWORD FindFatSize(void) const;
		WORD  FindReservedSize(void) const;
		UINT  FindFlags(void) const;
		void  AlignRegions(void);
	};

// End of File

#endif

