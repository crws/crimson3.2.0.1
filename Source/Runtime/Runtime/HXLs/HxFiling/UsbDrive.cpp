
#include "Intern.hpp"

#include "UsbDrive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Object
//

// Instantiator

static IUnknown * Create_UsbDrive(PCTXT pName)
{
	return (IBlockDevice *) New CUsbDrive;
	}

// Registration

void Register_UsbDrive(void)
{
	piob->RegisterInstantiator("fs.usbdrive", Create_UsbDrive);
	}

// Constructor

CUsbDrive::CUsbDrive(void)
{
	m_pMutex  = Create_Mutex();
	
	m_pDriver = NULL;

	m_pEvent  = NULL;
	}

// Destructor

CUsbDrive::~CUsbDrive(void)
{
	m_pMutex->Release();
	}

// IUnknown

HRESULT CUsbDrive::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostFuncEvents);

	return CBlockDevice::QueryInterface(riid, ppObject);
	}

ULONG CUsbDrive::AddRef(void)
{
	return CBlockDevice::AddRef();
	}

ULONG CUsbDrive::Release(void)
{
	return CBlockDevice::Release();
	}

// IBlockDevice

BOOL CUsbDrive::IsReady(void)
{
	if( LockDriver() ) {

		if( !m_pDriver->TestReady() ) {

			CheckError();

			SetPolling(1000);

			FreeDriver();

			return false;
			}

		SetPolling(0);

		FreeDriver();

		return true;
		}

	return false;
	}

void CUsbDrive::Attach(IEvent *pEvent)
{
	m_pEvent = pEvent;
	}

void CUsbDrive::Arrival(PVOID pDriver)
{
	m_pDriver = (IUsbHostMassStorage *) pDriver;

	m_pDriver->AddRef();
				
	InitPolling();
	}

void CUsbDrive::Removal(void)
{
	if( LockDriver() ) {

		m_pDriver->Release();

		m_pDriver = NULL;

		FreeDriver();
		}

	FireEvent();
	}

UINT CUsbDrive::GetSectorCount(void)
{
	if( LockDriver() ) {

		DWORD dwTotal;

		DWORD dwLength;

		if( !m_pDriver->ReadCapacity(dwTotal, dwLength) ) {

			CheckError();

			FreeDriver();

			return 0;
			}		
		
		FreeDriver();

		return dwTotal;
		}

	return 0;
	}

UINT CUsbDrive::GetCylinderCount(void)
{
	return 0;
	}

UINT CUsbDrive::GetHeadCount(void)
{
	return 1;
	}

UINT CUsbDrive::GetSectorsPerHead(void)
{
	return 0;
	}

BOOL CUsbDrive::WriteSector(UINT uSector, PCBYTE pData)
{
	if( LockDriver() ) {

		ResetPolling();
	
		if( !m_pDriver->Write(uSector, PBYTE(pData)) ) {

			CheckError();

			FreeDriver();

			return false;
			}

		FreeDriver();
	
		return CBlockDevice::WriteSector(uSector, pData);
		}

	return false;
	}

BOOL CUsbDrive::ReadSector(UINT uSector, PBYTE pData)
{
	if( LockDriver() ) {

		ResetPolling();

		if( !m_pDriver->Read(uSector, pData) ) {

			CheckError();

			FreeDriver();
			
			return false;
			}
		
		FreeDriver();

		return CBlockDevice::ReadSector(uSector, pData);
		}

	return false;
	}

// IUsbHostFunEvents

void CUsbDrive::OnPoll(UINT uLapsed)
{
	if( m_pEvent && m_uPoll ) {

		m_uIdle += uLapsed;

		if( m_uIdle > m_uPoll ) {

			FireEvent();

			m_uIdle = 0;
			}
		}
	}

void CUsbDrive::OnData(void)
{
	}

void CUsbDrive::OnTerm(void)
{
	}

// Implementation

BOOL CUsbDrive::CheckError(void)
{
	if( LockDriver() ) {

		ResetPolling();

		BYTE bSK, bASC, bASCQ;

		if( m_pDriver->RequestSense(bSK, bASC, bASCQ) ) {

			if( bSK == 0x6 ) {

				if( bASC == 0x28 ) {

					FireEvent(); 
					}
				}
			}
		
		FreeDriver();
		}

	return false;
	}

void CUsbDrive::InitPolling(void)
{
	m_uIdle = 0;

	m_uPoll = 1000;

	m_pDriver->Bind((IUsbHostFuncEvents *) this);
	}

void CUsbDrive::ResetPolling(void)
{
	m_uIdle = 0;
	}

void CUsbDrive::SetPolling(UINT uInterval)
{
	m_uPoll = uInterval;
	}

void CUsbDrive::FireEvent(void)
{
	if( m_pEvent ) m_pEvent->Set();
	}

BOOL CUsbDrive::LockDriver(void)
{
	m_pMutex->Wait(FOREVER);

	if( !m_pDriver ) {

		m_pMutex->Free();

		return false;
		}

	return true;
	}

void CUsbDrive::FreeDriver(void)
{
	m_pMutex->Free();
	}

// End of File