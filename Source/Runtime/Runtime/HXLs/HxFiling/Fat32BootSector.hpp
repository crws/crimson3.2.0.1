
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Fat32BootSector_HPP

#define	INCLUDE_Fat32BootSector_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "Fat32BiosParamBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Boot Sector
//

#pragma pack(1)

struct Fat32BootSector
{
	BYTE			m_bBootCode[3];
	BYTE			m_bOemName[8];
	Fat32BiosParamBlock	m_BiosParamBlock;
	BYTE			m_bDriveNum;
	BYTE			m_bReserved1;
	BYTE			m_bBootSig;
	DWORD			m_dwVolId;
	BYTE			m_bVolLabel[11];
	BYTE			m_bFileSystem[8];
	BYTE			m_bPadding[420];
	WORD			m_wMagic;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat32 Boot Sector
//

class CFat32BootSector : public Fat32BootSector
{
	public:
		// Constructor
		CFat32BootSector(void);
		CFat32BootSector(CPartitionEntry const &That);

		// Initialisation
		void Init(CPartitionEntry const &Partition);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL  IsValid(void) const;
		UINT  GetClusterSize(void) const;
		UINT  GetRootDirSectors(void) const;
		INT64 GetDataSize(void) const;
		DWORD GetDataSectors(void) const;
		DWORD GetDataClusters(void) const;
		DWORD GetFirstSector(DWORD dwCluster) const;
		DWORD GetFirstFatSector(WORD wFat) const;
		DWORD GetFirstDataSector(void) const;
		DWORD GetFatSectors(void) const;
		DWORD GetFatPerSector(void) const;
		DWORD GetRootDirCluster(void) const;

		// Bios Parameter Block
		CFat32BiosParamBlock & GetBiosParamBlock(void) const;

		// Dump
		void Dump(void) const;

	protected:
		// Constants
		enum
		{
			constExtended	= 0x29,
			constMagic	= 0xAA55,
			};
	};

// End of File

#endif

