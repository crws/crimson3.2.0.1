
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_PartitionTable_HPP

#define	INCLUDE_PartitionTable_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Partition Entry
//

#pragma pack(1)

struct Partition
{
	BYTE	m_bBootable : 1;
	BYTE	m_bDriveNo  : 7;
	BYTE	m_bStartHead;
	BYTE    m_bStartSector;
	BYTE	m_bStartCylinder;
	BYTE	m_bFileSys;
	BYTE	m_bEndHead;
	BYTE	m_bEndSector;
	BYTE	m_bEndCylinder;
	DWORD	m_dwRelativeSector;
	DWORD	m_dwTotalSectors;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Partition Entry
//

class CPartitionEntry : public Partition
{
	public:
		// Constructor
		CPartitionEntry(void);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Initialisation
		void Init(void);

		// Attributes
		BOOL IsValid(void) const;
		BYTE GetStartSector(void) const;
		WORD GetStartCylinder(void) const;
		BYTE GetEndSector(void) const;
		WORD GetEndCylinder(void) const;
		
		// Operations
		void SetBeg(WORD wCylinder, BYTE bHead, BYTE bSector);
		void SetEnd(WORD wCylinder, BYTE bHead, BYTE bSector);

		// Debug
		void Dump(void) const;

		// Types
		enum
		{
			typeUnused	= 0x00,
			typeFat12	= 0x01,
			typeFat16S	= 0x04,
			typeFat16L	= 0x06,
			typeFat32	= 0x0B,
			typeFat32L	= 0x0C,
			typeFat16B	= 0x0E,
			};
	};

// End of File

#endif

