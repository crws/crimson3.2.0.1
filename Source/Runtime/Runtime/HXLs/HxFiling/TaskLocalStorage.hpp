
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_TaskLocalStorage_HPP

#define	INCLUDE_TaskLocalStorage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Task Volume 
//

class CTaskVolume : public CThreadData
{
	public: 
		// Constructor
		CTaskVolume(void);

		// Data;
		UINT m_iVolume;
	};

//////////////////////////////////////////////////////////////////////////
//
// Task Directory 
//

class CTaskDirectory : public CThreadData
{
	public: 
		// Constructor
		CTaskDirectory(void);

		// Data;
		FSIndex m_Index;
	};

//////////////////////////////////////////////////////////////////////////
//
// Task Drive 
//

class CTaskDrive : public CThreadData
{
	public: 
		// Constructor
		CTaskDrive(void);

		// Data;
		CHAR m_cDrive;
	};

//////////////////////////////////////////////////////////////////////////
//
// Task Path 
//

class CTaskPath : public CThreadData
{
	public: 
		// Constructor
		CTaskPath(void);

		// Data;
		CHAR    m_cDrive;
		UINT    m_iVolume;
		FSIndex m_Index;
	};

//////////////////////////////////////////////////////////////////////////
//
// Task Ref 
//

class CTaskRef : public CThreadData
{
	public: 
		// Constructor
		CTaskRef(void);

		// Destructor
		~CTaskRef(void);

		// Data;
		IFilingSystem * m_pRef;
		UINT            m_uRef;
	};

// End of File

#endif
