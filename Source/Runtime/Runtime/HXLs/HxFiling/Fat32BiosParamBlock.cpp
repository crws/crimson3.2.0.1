
#include "Intern.hpp"

#include "Fat32BiosParamBlock.hpp"

#include "PartitionTable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Partition Table Object
//

CFat32BiosParamBlock::CFat32BiosParamBlock(void)
{
	memset(this, 0x00, sizeof(Fat32BiosParamBlock));
	}

CFat32BiosParamBlock::CFat32BiosParamBlock(CFat32BiosParamBlock const &That)
{
	memcpy(this, &That, sizeof(Fat32BiosParamBlock));
	}

// Initialisation

void CFat32BiosParamBlock::Init(CPartitionEntry const &Partition)
{
	memset(this, 0x00, sizeof(Fat32BiosParamBlock));

	m_wSectorSize	 = 512;
	m_wReservedSize	 = 32;
	m_bFatCount	 = 2;
	m_bMedia	 = 0xF8;
	m_wTrackSize	 = 0;
	m_wHeadCount	 = Partition.m_bEndHead - Partition.m_bStartHead + 1;
	m_dwHiddenSize	 = Partition.m_dwRelativeSector;
	m_dwTotalSectors = Partition.m_dwTotalSectors;
	m_bClusterSize	 = BYTE(FindClusterSize());
	m_dwFat32Size	 = UINT(FindFatSize());
	m_wFlags	 = WORD(FindFlags());
	m_wVersion	 = 0;
	m_dwRootCluster	 = 2;
	m_wFSInfoSector	 = 1;
	m_wBootSector	 = 6;

	AlignRegions();
	}

// Conversion

void CFat32BiosParamBlock::HostToLocal(void)
{
	m_wSectorSize	 = HostToIntel(m_wSectorSize);
	m_wReservedSize	 = HostToIntel(m_wReservedSize);
	m_wRootSize	 = HostToIntel(m_wRootSize);
	m_wTotalSectors	 = HostToIntel(m_wTotalSectors);
	m_wFat16Size	 = HostToIntel(m_wFat16Size);
	m_wTrackSize	 = HostToIntel(m_wTrackSize);
	m_wHeadCount	 = HostToIntel(m_wHeadCount);
	m_dwHiddenSize	 = HostToIntel(m_dwHiddenSize);
	m_dwTotalSectors = HostToIntel(m_dwTotalSectors);
	m_dwFat32Size	 = HostToIntel(m_dwFat32Size);
	m_wFlags	 = HostToIntel(m_wFlags);
	m_wVersion	 = HostToIntel(m_wVersion);
	m_dwRootCluster	 = HostToIntel(m_dwRootCluster);
	m_wFSInfoSector	 = HostToIntel(m_wFSInfoSector);
	m_wBootSector	 = HostToIntel(m_wBootSector);
	}

void CFat32BiosParamBlock::LocalToHost(void)
{
	m_wSectorSize	 = IntelToHost(m_wSectorSize);
	m_wReservedSize	 = IntelToHost(m_wReservedSize);
	m_wRootSize	 = IntelToHost(m_wRootSize);
	m_wTotalSectors	 = IntelToHost(m_wTotalSectors);
	m_wFat16Size	 = IntelToHost(m_wFat16Size);
	m_wTrackSize	 = IntelToHost(m_wTrackSize);
	m_wHeadCount	 = IntelToHost(m_wHeadCount);
	m_dwHiddenSize	 = IntelToHost(m_dwHiddenSize);
	m_dwTotalSectors = IntelToHost(m_dwTotalSectors);
	m_dwFat32Size	 = IntelToHost(m_dwFat32Size);
	m_wFlags	 = IntelToHost(m_wFlags);
	m_wVersion	 = IntelToHost(m_wVersion);
	m_dwRootCluster	 = IntelToHost(m_dwRootCluster);
	m_wFSInfoSector	 = IntelToHost(m_wFSInfoSector);
	m_wBootSector	 = IntelToHost(m_wBootSector);
	}

// Attributes

BOOL CFat32BiosParamBlock::IsValid(void) const
{
	if( !m_wReservedSize ) {

		return false;
		}

	if( m_wRootSize ) {

		return false;
		}

	if( m_wFat16Size || !m_dwFat32Size ) {

		return false;
		}

	if( m_wTotalSectors ) {

		return false;
		}

	if( !CheckSectorSize() ) {

		return false;
		}

	if( !CheckClusterSize() ) {

		return false;
		}

	if( GetDataClusters() < 65525 ) {

		return false;
		}

	return true;
	}

UINT CFat32BiosParamBlock::GetClusterSize(void) const
{
	return m_wSectorSize * m_bClusterSize;
	}

UINT CFat32BiosParamBlock::GetRootDirSectors(void) const
{
	return 0;
	}

INT64 CFat32BiosParamBlock::GetDataSize(void) const
{
	return INT64(GetDataSectors()) * INT64(m_wSectorSize);
	}

DWORD CFat32BiosParamBlock::GetDataSectors(void) const
{
	return m_dwTotalSectors - m_wReservedSize - (m_dwFat32Size * m_bFatCount) - GetRootDirSectors();
	}

DWORD CFat32BiosParamBlock::GetDataClusters(void) const
{
	return GetDataSectors() / m_bClusterSize;
	}

DWORD CFat32BiosParamBlock::GetFirstSector(DWORD dwCluster) const
{
	return dwCluster * m_bClusterSize;
	}

DWORD CFat32BiosParamBlock::GetFirstFatSector(WORD wFat) const
{
	return m_wReservedSize + (m_dwFat32Size * wFat);
	}

DWORD CFat32BiosParamBlock::GetFirstDataSector(void) const
{
	return m_wReservedSize + (m_dwFat32Size * m_bFatCount) + GetRootDirSectors();
	}

DWORD CFat32BiosParamBlock::GetFatPerSector(void) const
{
	return m_wSectorSize / sizeof(DWORD);
	}

// Dump

void CFat32BiosParamBlock::Dump(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nFat32 Bios Param Block\n");

	AfxTrace("Status              = %s\n",      IsValid() ? "OK" : "Invalid");
	AfxTrace("Sector Size         = %d\n",      m_wSectorSize);
	AfxTrace("Cluster Size        = %d\n",      m_bClusterSize);
	AfxTrace("Reserved Size       = %d\n",      m_wReservedSize);
	AfxTrace("Number of FATs      = %d\n",      m_bFatCount);
	AfxTrace("Root Size           = %d\n",      m_wRootSize);
	AfxTrace("Track Size          = %d\n",      m_wTrackSize);
	AfxTrace("Head Count          = %d\n",      m_wHeadCount);
	AfxTrace("Flags               = 0x%4.4X\n", m_wFlags);
	AfxTrace("Version             = 0x%4.4X\n", m_wVersion);
	AfxTrace("Root Dir Cluster    = 0x%8.8X\n", m_dwRootCluster);
	AfxTrace("Info Sector         = 0x%4.4X\n", m_wFSInfoSector);
	AfxTrace("Boot Sector         = 0x%4.4X\n", m_wBootSector);
	AfxTrace("Hidden Size         = 0x%8.8X\n", m_dwHiddenSize);
	AfxTrace("Total Size          = %u\n",      m_dwTotalSectors);
	AfxTrace("Cluster Size        = %d\n",      GetClusterSize());
	AfxTrace("Fat Size            = 0x%8.8X\n", m_dwFat32Size);
	AfxTrace("Root Dir Secs       = %d\n",      GetRootDirSectors());
	AfxTrace("Data Size           = %llu\n",    GetDataSize());
	AfxTrace("Data Secs           = %u\n",      GetDataSectors());
	AfxTrace("Data Clusters       = %u\n",      GetDataClusters());
	AfxTrace("First FAT Sec0      = 0x%8.8X\n", GetFirstFatSector(0));
	AfxTrace("First FAT Sec1      = 0x%8.8X\n", GetFirstFatSector(1));
	AfxTrace("First Data Sector   = 0x%8.8X\n", GetFirstDataSector());

	#endif
	}

// Implementation

BOOL CFat32BiosParamBlock::CheckSectorSize(void) const
{
	switch( m_wSectorSize ) {

		case 512:
		case 1024:
		case 2048:
		case 4096:

			return true;
		}

	return false;
	}


BOOL CFat32BiosParamBlock::CheckClusterSize(void) const
{
	switch( m_bClusterSize ) {

		case 1:
		case 2:
		case 4:
		case 8:
		case 16:
		case 32:
		case 64:
		case 128:

			return m_wSectorSize * m_bClusterSize <= (32 * 1024);
		}

	return false;
	}

UINT CFat32BiosParamBlock::FindClusterSize(void) const
{
	struct CLookup
	{
		DWORD dwTotal;
		BYTE  bSize;
		
		};

	static CLookup const Lookup[] = {

		{      66600,  0  },
		{     532480,  1  },
		{   16777216,  8  },
		{   33554432, 16  },
		{   67108864, 32  },
		{ 0xFFFFFFFF, 64  },

		};

	for( int n = 0; n < elements(Lookup); n ++ ) {
		
		CLookup const &Entry = Lookup[n];

		if( m_dwTotalSectors <= Entry.dwTotal ) {

			return Entry.bSize;
			}
		}
	
	return 0;
	}

DWORD CFat32BiosParamBlock::FindFatSize(void) const
{
	DWORD dwTmpVal1 = m_dwTotalSectors - (m_wReservedSize + GetRootDirSectors());

	DWORD dwTmpVal2 = (256 * m_bClusterSize + m_bFatCount) / 2;

	return (dwTmpVal1 + dwTmpVal2 - 1) / dwTmpVal2;
	}

WORD CFat32BiosParamBlock::FindReservedSize(void) const
{
	DWORD dwFatSize = m_dwFat32Size * 2;

	DWORD dwAlign   = 8192;

	return WORD((((dwFatSize + dwAlign - 1) / dwAlign) * dwAlign) - dwFatSize);
	}

UINT CFat32BiosParamBlock::FindFlags(void) const
{
	return 0;
	}

void CFat32BiosParamBlock::AlignRegions(void)
{
	for(;;) {

		WORD wLastSize   = m_wReservedSize;
		 
		m_wReservedSize  = FindReservedSize();

		m_dwFat32Size	 = FindFatSize();
	
		m_wReservedSize  = FindReservedSize();

		if( wLastSize == m_wReservedSize ) {

			break;
			}
		}
	}

// End of File