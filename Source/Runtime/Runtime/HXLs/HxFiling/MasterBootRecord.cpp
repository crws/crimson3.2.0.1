
#include "Intern.hpp"

#include "MasterBootRecord.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Partition Table Object
//

CMasterBootRecord::CMasterBootRecord(void)
{
	Init();
	}

// Initialisation

void CMasterBootRecord::Init(void)
{
	memset(this, 0x00, sizeof(MasterBootRecord));

	m_wMagic = constMagic;
	}

void CMasterBootRecord::Init(PCBYTE pBoot, UINT uSize)
{
	if( uSize <= sizeof(m_bCode) ) {

		memcpy(m_bCode, pBoot, uSize);

		memset(m_bCode + uSize, 0, sizeof(MasterBootRecord) - uSize);

		m_wMagic = constMagic;
		}
	}

// Conversion

void CMasterBootRecord::HostToLocal(void)
{
	for( UINT i = 0; i < elements(m_Partitions); i++ ) {

		CPartitionEntry &Entry = (CPartitionEntry &) GetPartition(i);

		Entry.HostToLocal();
		}

	m_wMagic = HostToIntel(m_wMagic);
	}

void CMasterBootRecord::LocalToHost(void)
{
	for( UINT i = 0; i < elements(m_Partitions); i++ ) {

		CPartitionEntry &Entry = (CPartitionEntry &) GetPartition(i);

		Entry.LocalToHost();
		}

	m_wMagic = IntelToHost(m_wMagic);
	}

// Attributes

BOOL CMasterBootRecord::IsValid(void) const
{
	return m_wMagic == constMagic && GetPartition(0).IsValid();
	}

// Operations

CPartitionEntry const & CMasterBootRecord::GetPartition(UINT iIndex) const
{
	return (CPartitionEntry &) m_Partitions[iIndex];
	}

// End of File