
#include "Intern.hpp"

#include "FatDirEntry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory Entry
//

// Constructor

CFatDirEntry::CFatDirEntry(void)
{
	}

// Conversion

void CFatDirEntry::HostToLocal(void)
{
	if( IsLong() ) {

		GetLong().HostToLocal();
		}
	else {
		GetShort().HostToLocal();
		}
	}

void CFatDirEntry::LocalToHost(void)
{
	if( IsLong() ) {

		GetLong().LocalToHost();
		}
	else {
		GetShort().LocalToHost();
		}
	}

// Attributes

BOOL CFatDirEntry::IsValid(void) const
{
	return IsLong() ? GetLong().IsValid() : GetShort().IsValid();
	}

BOOL CFatDirEntry::IsLong(void) const
{
	return GetShort().IsLong();
	}

BOOL CFatDirEntry::IsShort(void) const
{
	return GetShort().IsShort();
	}

BOOL CFatDirEntry::IsFree(void) const
{
	return IsLong() ? GetLong().IsFree() : GetShort().IsFree();
	}

BOOL CFatDirEntry::IsLast(void) const
{
	return IsLong() ? false : GetShort().IsLast();
	}

BOOL CFatDirEntry::IsEmpty(void) const
{
	return IsLong() ? GetLong().IsEmpty() : GetShort().IsEmpty();
	}

// Init

void CFatDirEntry::MakeEmpty(void)
{
	GetShort().MakeEmpty();
	}

// Operations

CFatDirShort & CFatDirEntry::GetShort(void) const
{
	return (CFatDirShort &) m_s;
	}

CFatDirLong & CFatDirEntry::GetLong (void) const
{
	return (CFatDirLong &) m_l;
	}

// Operators

CFatDirEntry::operator CFatDirShort & (void) const
{
	return (CFatDirShort &) m_s;
	}

CFatDirEntry::operator CFatDirLong & (void) const
{
	return (CFatDirLong &) m_l;
	}

CFatDirEntry::operator CFatDirShort * (void) const
{
	return (CFatDirShort *) &m_s;
	}

CFatDirEntry::operator CFatDirLong  * (void) const
{
	return (CFatDirLong *) &m_l;
	}

// Dump

void CFatDirEntry::Dump(void) const
{
	if( IsLong() ) {

		GetLong().Dump();
		}
	else { 
		GetShort().Dump();
		}
	}

// End of File