
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_SdDrive_HPP

#define	INCLUDE_SdDrive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BlockDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SD Drive 
//

class CSdDrive : public CBlockDevice
{
	public:
		// Constructor
		CSdDrive(void);

		// Destructor
		~CSdDrive(void);

		// IDeviceEx
		BOOL METHOD Open(IUnknown *pUnk);

		// IBlockDevice
		BOOL IsReady(void);
		void Attach(IEvent *pEvent);
		UINT GetSectorCount(void);
		UINT GetCylinderCount(void);
		UINT GetHeadCount(void);
		UINT GetSectorsPerHead(void);
		BOOL WriteSector(UINT uSector, PCBYTE pData);
		BOOL ReadSector(UINT uSector, PBYTE pData);

	protected:
		// Data
		ISdHost * m_pSdHost;
		IMutex  * m_pMutex;
	};

// End of File

#endif
