
#include "Intern.hpp"

#include "FatDirName.hpp"

#include "FatDirEntry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory Name
//

// Constructors

CFatDirName::CFatDirName(void)
{
	Clear();
	}

CFatDirName::CFatDirName(PCTXT pText)
{
	m_uSize  = strlen(pText);

	m_bCheck = 0x00;

	m_fShort = CFatDirShort().CheckName(pText);

	MakeMin(m_uSize, sizeof(m_sText));
	
	memcpy(m_sText, pText, m_uSize + 1);
	}

CFatDirName::CFatDirName(CFatDirName const &That)
{
	m_uSize  = That.m_uSize;

	m_bCheck = That.m_bCheck;

	m_fShort = That.m_fShort;

	memcpy(m_sText, That.m_sText, m_uSize);
	}

// Attributes

UINT CFatDirName::GetLength(void) const
{
	return m_uSize;
	}

BOOL CFatDirName::IsShort(void) const
{
	return m_fShort;
	}

BOOL CFatDirName::IsLong(void) const
{
	return !m_fShort;
	}

// Operations

void CFatDirName::Clear(void)
{
	m_sText[0] = NULL;

	m_uSize    = 0;

	m_bCheck   = 0x00;

	m_fShort   = true;
	}

PCTXT CFatDirName::MakeBasis(UINT uTail)
{
	UINT uLimit = 8 - GetTailLength(uTail);

	UINT uCount = GetStripped(m_sBasis, uLimit);

	UINT uFind  = FindExtension();
	
	uCount += sprintf(m_sBasis + uCount, "~%d", Min(uTail, UINT(999999)));

	if( uFind != NOTHING ) {

		uCount += GetExtension(m_sBasis + uCount);
		}

	strupr(m_sBasis);
	
	return m_sBasis;
	}

UINT CFatDirName::GetEntryCount(void) const
{
	return IsLong() ? 1 + (m_uSize + constSizeLong) / constSizeLong : 1;
	}

BOOL CFatDirName::Export(UINT iEntry, CFatDirEntry &Entry)
{
	if( iEntry == 0 ) {

		CFatDirShort &Short = Entry.GetShort();

		Short.MakeEmpty();

		if( IsLong() ) {

			Short.Create(m_sBasis);

			m_bCheck = Short.GetChecksum();
			}
		else {
			Short.Create(m_sText);
			}

		return true;
		}

	if( iEntry < GetEntryCount() ) {

		UINT uPtr = (iEntry - 1) * constSizeLong;

		CFatDirLong &Long = Entry.GetLong();

		Long.MakeEmpty();

		Long.SetText(m_sText + uPtr);

		Long.m_bChecksum = m_bCheck;

		Long.m_bOrder    = BYTE(iEntry);

		if( iEntry == GetEntryCount() - 1 ) {

			Long.MakeLast();
			}
		
		return true;
		}

	return false;
	}

BOOL CFatDirName::Import(CFatDirEntry const &Entry)
{
	if( Entry.IsLong() ) {

		m_fShort = false;
		
		CFatDirLong &Long = Entry.GetLong();

		UINT uPtr = (Long.GetOrder() - 1) * constSizeLong;

		Long.GetText(&m_sText[uPtr]);

		if( Long.IsLast() ) {

			m_bCheck = Long.m_bChecksum;

			m_uSize  = uPtr + Long.GetLength();

			m_sText[m_uSize] = 0;
			
			return true;
			}

		return Long.m_bChecksum == m_bCheck;
		}
	else {
		CFatDirShort &Short = Entry.GetShort();

		if( m_fShort || Short.GetChecksum() != m_bCheck ) { 

			Short.GetFullName(m_sText);

			m_uSize = strlen(m_sText);
			}

		return true;
		}
	}

// Comparison Operators

BOOL CFatDirName::operator == (CFatDirName const &That) const
{
	if( m_uSize != That.m_uSize ) {

		return false;
		}

	if( memcmp(m_sText, That.m_sText, m_uSize) ) {

		return false;
		}

	return true;
	}

BOOL CFatDirName::operator == (PCTXT pText) const
{
	for( UINT i1 = 0, i2 = 0; ; i1 ++, i2 ++ ) {

		char c = pText[i1];

		char s = m_sText[i2];

		if( c >= 'a' && c <= 'z' ) {

			c &= ~0x20;
			}

		if( s >= 'a' && s <= 'z' ) {

			s &= ~0x20;
			}

		if( c == '?' || c == s ) {

			if( !s ) {

				return true;
				}

			continue;
			}

		if( c == '*' ) {
			
			c = pText[i1 + 1];

			if( c >= 'a' && c <= 'z' ) {

				c &= ~0x20;
				}
						
			if( c == s ) {

				i2 --;

				continue;
				}
			
			if( !s ) {

				return true;
				}

			i1 --;
			
			continue;
			}

		if( c == s ) {

			if( !s ) {

				return true;
				}

			continue;
			}

		return false;
		}

	return false;
	}

// Conversion Operators

CFatDirName::operator PTXT (void) const
{
	return PTXT(m_sText);
	}

// Implementation

UINT CFatDirName::Count(CHAR c) const
{
	UINT uCount = 0;

	for( UINT i = 0; i < m_uSize; i ++ ) {

		if( m_sText[i] == c ) {

			uCount++;
			}
		}

	return uCount;
	}

UINT CFatDirName::FindExtension(void) const
{
	for( INT n = m_uSize - 1; n >= 0; n -- ) {

		if( m_sText[n] == '.' ) {

			return n + 1;
			}
		}

	return NOTHING;
	}

UINT CFatDirName::GetStripped(PTXT pText, UINT uSize) const
{
	UINT uPeriodTotal = Count('.');

	UINT uPeriodCount = 0;

	UINT uCount       = 0;
	
	for( UINT i = 0; i < m_uSize; i ++ ) {

		CHAR c = m_sText[i];

		if( c == '.' ) {

			if( ++uPeriodCount < uPeriodTotal ) {

				continue;
				}

			break;
			}

		if( c == ' ' ) {

			continue;
			}
	
		pText[uCount++] = c;

		if( uCount == uSize ) {

			break;
			}
		}

	return uCount;
	}

UINT CFatDirName::GetExtension(PTXT pText) const
{
	UINT uFind  = FindExtension();

	if( uFind != NOTHING ) {

		UINT uCount = 0;

		*pText++ = '.';

		while( uFind < m_uSize ) {

			CHAR c = m_sText[uFind++];

			if( c == ' ' ) {

				continue;
				}
	
			pText[uCount++] = c;

			if( uCount == 3 ) {

				break;
				}
			}

		pText[uCount] = 0;

		return uCount + 1;
		}

	return 0;
	}

UINT CFatDirName::GetTailLength(UINT uTail) const
{
	UINT n = 10;
	
	for( UINT uLen = 1; uLen < 6; uLen ++ ) {

		if( uTail < n ) {

			return uLen + 1;
			}

		n *= 10;
		}
	
	return 7;
	}

// End of File