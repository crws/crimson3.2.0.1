
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_UsbDrive_HPP

#define	INCLUDE_UsbDrive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BlockDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Drive 
//

class CUsbDrive : public CBlockDevice, public IUsbHostFuncEvents
{
	public:
		// Constructor
		CUsbDrive(void);

		// Destructor
		~CUsbDrive(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IBlockDevice
		BOOL IsReady(void);
		void Attach(IEvent *pEvent);
		void Arrival(PVOID pDevice);
		void Removal(void);
		UINT GetSectorCount(void);
		UINT GetCylinderCount(void);
		UINT GetHeadCount(void);
		UINT GetSectorsPerHead(void);
		BOOL WriteSector(UINT uSector, PCBYTE pData);
		BOOL ReadSector(UINT uSector, PBYTE pData);

		// IUsbHostFunEvents
		void METHOD OnPoll(UINT uLapsed);
		void METHOD OnData(void);
		void METHOD OnTerm(void);

	protected:
		// Data
		IUsbHostMassStorage * m_pDriver;
		IEvent              * m_pEvent;
		IMutex              * m_pMutex;
		UINT                  m_uPoll;
		UINT                  m_uIdle;

		// Implementation
		BOOL CheckError(void);
		void InitPolling(void);
		void ResetPolling(void);
		void SetPolling(UINT uPeriod);
		void FireEvent(void);
		BOOL LockDriver(void);
		void FreeDriver(void);
	};

// End of File

#endif
