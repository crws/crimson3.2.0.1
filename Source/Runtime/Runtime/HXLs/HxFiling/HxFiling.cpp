
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_SdDrive(void);

extern void Register_UsbDrive(void);

extern void Register_File(void);

extern void Register_RlosFileCopier(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxFiling(void)
{
	Create_VolumeManager();

	Create_DiskManager(15000);

	Create_FileManager();

	Register_SdDrive();

	Register_UsbDrive();

	Register_File();

//	Register_RlosFileCopier();
	}

void Revoke_HxFiling(void)
{
	piob->RevokeGroup("fs.");
	}

// End of File
