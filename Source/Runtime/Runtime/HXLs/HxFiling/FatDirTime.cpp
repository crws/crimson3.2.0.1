
#include "Intern.hpp"

#include "FatDirTime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat Time Object
//

// Constructor

CFatTime::CFatTime(void)
{
	}

// Attributes

BOOL CFatTime::IsValid(void) const
{
	if( m_wSec > 29 ) {

		return false;
		}

	if( m_wMin > 59 ) {

		return false;
		}

	if( m_wHour > 23 ) {

		return false;
		}

	return true;
	}

// Operations

time_t CFatTime::Get(void) const
{
	return (2 * m_wSec) + (60 * (m_wMin + (60 * m_wHour)));
	}

void CFatTime::Set(time_t time) 
{
	m_wHour = time / 3600 % 24;
	m_wMin  = time /   60 % 60;
	m_wSec  = time /    2 % 30;
	}

/*void CFatTime::Get(CTime &Time) const
{
	Time.uHours   = m_wHour;

	Time.uMinutes = m_wMin;
	
	Time.uSeconds = m_wSec * 2;
	}

void CFatTime::Set(CTime const &Time)
{
	m_wHour = Time.uHours;
	
	m_wMin  = Time.uMinutes;
	
	m_wSec  = Time.uSeconds / 2;
	}*/

//////////////////////////////////////////////////////////////////////////
//
// Fat Date Object
//

// Constructor

CFatDate::CFatDate(void)
{
	}

// Attributes

BOOL CFatDate::IsValid(void) const
{
	if( m_wDate < 1 || m_wDate > 31 ) {

		return false;
		}

	if( m_wMonth < 1 || m_wMonth > 12 ) {

		return false;
		}

	return true;
	}

// Operations

time_t CFatDate::Get(void) const
{
	struct tm tm = { 0 };

	tm.tm_year = m_wYear  + 80;
	tm.tm_mon  = m_wMonth - 1;
	tm.tm_mday = m_wDate  + 0;

	return timegm(&tm);
	}

void CFatDate::Set(time_t time)
{
	struct tm *tm = gmtime(&time);

	m_wDate  = tm->tm_mday;
	m_wMonth = tm->tm_mon  + 1;
	m_wYear  = tm->tm_year - 80;
	}

/*void CFatDate::Get(CTime &Time) const
{
	Time.uDate  = m_wDate;
	
	Time.uMonth = m_wMonth;
	
	Time.uYear  = m_wYear + 1980;
	
	Time.uDay   = 0;
	}

void CFatDate::Set(CTime const &Time)
{
	m_wDate  = Time.uDate;

	m_wMonth = Time.uMonth;
	
	m_wYear  = Time.uYear >= 1980 ? Time.uYear - 1980 : Time.uYear + 20;
	}*/

// End of File