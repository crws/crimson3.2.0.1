
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FatDirEntry_HPP

#define	INCLUDE_FatDirEntry_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "FatDirShort.hpp"

#include "FatDirLong.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Directory Entry
//

struct FatDirEntry
{
	union
	{
		FatDirShort m_s;
		FatDirLong  m_l;
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory Entry
//

class CFatDirEntry : public FatDirEntry
{
	public:
		// Constructor
		CFatDirEntry(void);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL IsValid(void) const;
		BOOL IsLong(void) const;
		BOOL IsShort(void) const;
		BOOL IsFree(void) const;
		BOOL IsLast(void) const;
		BOOL IsEmpty(void) const;

		// Init
		void MakeEmpty(void);

		// Operations
		CFatDirShort & GetShort(void) const;
		CFatDirLong  & GetLong (void) const;
		
		// Operators
		operator CFatDirShort & (void) const;
		operator CFatDirLong  & (void) const;
		operator CFatDirShort * (void) const;
		operator CFatDirLong  * (void) const;

		// Dump
		void Dump(void) const;
	};

// End of File

#endif

