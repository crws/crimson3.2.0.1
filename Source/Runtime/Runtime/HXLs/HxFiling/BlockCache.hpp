
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_BlockCache_HPP

#define	INCLUDE_BlockCache_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Block Cache Object
//

class CBlockCache : public IBlockCache
{
	public:
		// Constructor
		CBlockCache(IBlockDevice *pDev, UINT uPolicy);

		// Destructor
		~CBlockCache(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Methods
		UINT  GetSectorCount(void);
		UINT  GetSectorSize(void);
		BOOL  Flush(void);
		void  Invalidate(void);
		PBYTE LockSector(UINT uSector, BOOL fSkipRead);
		BOOL  CommitSector(UINT uSector);
		BOOL  UnlockSector(UINT uSector, BOOL fDirty);

	protected:
		// Constants
		enum 
		{
			constP1	= 31,
			constP2	= 13,
			};
		
		// Cache Entry
		struct CEntry
		{
			UINT  m_uSector;
			UINT  m_uLock;
			UINT  m_uTime;
			BOOL  m_fDirty;
			PBYTE m_pData;
			};

		// Data
		ULONG	       m_uRefs;
		IMutex	     * m_pMutex;
		IBlockDevice * m_pDev;
		CEntry         m_Cache[constP1];
		UINT	       m_uPolicy;

		// Implementation
		void CacheAlloc(void);
		void CacheKill(void);
		UINT CacheFindEntry(UINT uSector);
		UINT CacheFindSpare(UINT uSector);
		UINT CacheFindOldest(UINT uSector);
		BOOL CacheCommitSlot(UINT uSlot);
		BOOL CacheFreeSlot(UINT uSlot);
	};

// End of File

#endif
