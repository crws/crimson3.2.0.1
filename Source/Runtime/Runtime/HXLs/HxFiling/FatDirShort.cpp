
#include "Intern.hpp"

#include "FatDirShort.hpp"

#include "FatDirTime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory
//

// Constructor

CFatDirShort::CFatDirShort(void)
{
	}

// Creation

BOOL CFatDirShort::Create(PCTXT pName)
{
	MakeEmpty();

	if( SetFullName(pName) && !IsSpecial() ) {

		UpdateTimeStamps();

		m_wCreateTime = m_wWriteTime;
		
		m_wCreateDate = m_wWriteDate;
	
		return true;
		}

	return false;
	}

BOOL CFatDirShort::CreateFile(PCTXT pName, DWORD dwCluster, BYTE bFlags)
{
	MakeEmpty();

	if( SetFullName(pName) && !IsSpecial() ) {

		UpdateTimeStamps();

		m_wCreateTime = m_wWriteTime;
		
		m_wCreateDate = m_wWriteDate;

		m_bAttribute  = bFlags;
		
		SetFirstCluster(dwCluster);
		
		return true;
		}

	return false;
	}

BOOL CFatDirShort::CreateDir(PCTXT pName, DWORD dwCluster, BYTE bFlags)
{
	MakeEmpty();

	if( SetFullName(pName) && !IsSpecial() ) {

		UpdateTimeStamps();

		m_wCreateTime = m_wWriteTime;
		
		m_wCreateDate = m_wWriteDate;

		m_bAttribute  = bFlags | attrDir;
		
		SetFirstCluster(dwCluster);
		
		return true;
		}

	return false;
	}

BOOL CFatDirShort::CreateDot(CFatDirShort const &Dir)
{
	MakeEmpty();

	MakeDot();

	m_bAttribute     |= attrDir;

	m_wFirstClusterHi = Dir.m_wFirstClusterHi;

	m_wFirstClusterLo = Dir.m_wFirstClusterLo;

	m_wCreateTime     = Dir.m_wCreateTime;

	m_wCreateDate     = Dir.m_wCreateDate;

	m_wWriteTime      = Dir.m_wWriteTime;

	m_wWriteDate      = Dir.m_wWriteDate;

	m_wDateAccess     = Dir.m_wDateAccess;

	return true;
	}

BOOL CFatDirShort::CreateDot2(CFatDirShort const &Dir, CFatDirShort const &Parent)
{
	MakeEmpty();

	MakeDotDot();

	m_bAttribute     |= attrDir;

	m_wFirstClusterHi = Parent.m_wFirstClusterHi;

	m_wFirstClusterLo = Parent.m_wFirstClusterLo;

	m_wCreateTime     = Dir.m_wCreateTime;

	m_wCreateDate     = Dir.m_wCreateDate;

	m_wWriteTime      = Dir.m_wWriteTime;

	m_wWriteDate      = Dir.m_wWriteDate;

	m_wDateAccess     = Dir.m_wDateAccess;

	return true;
	}

BOOL CFatDirShort::CreateDot2(CFatDirShort const &Dir, UINT uParent)
{
	MakeEmpty();

	MakeDotDot();

	m_bAttribute     |= attrDir;

	m_wFirstClusterHi = HIWORD(uParent);

	m_wFirstClusterLo = LOWORD(uParent);

	m_wCreateTime     = Dir.m_wCreateTime;

	m_wCreateDate     = Dir.m_wCreateDate;

	m_wWriteTime      = Dir.m_wWriteTime;

	m_wWriteDate      = Dir.m_wWriteDate;

	m_wDateAccess     = Dir.m_wDateAccess;

	return true;
	}

// Conversion

void CFatDirShort::HostToLocal(void)
{
	m_wCreateTime     = HostToIntel(m_wCreateTime);
	m_wCreateDate     = HostToIntel(m_wCreateDate);
	m_wDateAccess     = HostToIntel(m_wDateAccess);
	m_wFirstClusterHi = HostToIntel(m_wFirstClusterHi);
	m_wWriteTime      = HostToIntel(m_wWriteTime);
	m_wWriteDate      = HostToIntel(m_wWriteDate);
	m_wFirstClusterLo = HostToIntel(m_wFirstClusterLo);
	m_dwSize          = HostToIntel(m_dwSize);
	}

void CFatDirShort::LocalToHost(void)
{
	m_wCreateTime     = IntelToHost(m_wCreateTime);
	m_wCreateDate     = IntelToHost(m_wCreateDate);
	m_wDateAccess     = IntelToHost(m_wDateAccess);
	m_wFirstClusterHi = IntelToHost(m_wFirstClusterHi);
	m_wWriteTime      = IntelToHost(m_wWriteTime);
	m_wWriteDate      = IntelToHost(m_wWriteDate);
	m_wFirstClusterLo = IntelToHost(m_wFirstClusterLo);
	m_dwSize          = IntelToHost(m_dwSize);
	}

// Attributes

BOOL CFatDirShort::IsValid(void) const
{
	if( IsFree() ) {

		return true;
		}

	if( IsLong() ) {

		return false;
		}

	if( !IsSpecial() && !CheckName() ) {

		return false;
		}

	if( IsDirectory() ) {

		if( m_dwSize > (65536 * 32) ) {

			return false;
			}

		return true;
		}

	if( IsFile() ) {

		if( IsSpecial() ) {

			return false;
			}

		return true;
		}

	if( IsVolume() ) {

		return true;
		}

	return false;
	}

BOOL CFatDirShort::IsEmpty(void) const
{
	return m_sText[0] == 0x00;
	}

BOOL CFatDirShort::IsFree(void) const
{
	return BYTE(m_sText[0]) == constFree || BYTE(m_sText[0]) == constLast;
	}

BOOL CFatDirShort::IsLast(void) const
{
	return BYTE(m_sText[0]) == constLast;
	}

BOOL CFatDirShort::IsDot(void) const
{
	return m_sText[0] == '.' && m_sText[1] == ' ';
	}

BOOL CFatDirShort::IsDotDot(void) const
{
	return m_sText[0] == '.' && m_sText[1] == '.' && m_sText[2] == ' ';
	}

BOOL CFatDirShort::IsSpecial(void) const
{
	return IsDot() || IsDotDot();
	}

BOOL CFatDirShort::IsReadOnly(void) const
{
	return m_bAttribute & attrReadOnly;
	}

BOOL CFatDirShort::IsHidden(void) const
{
	return m_bAttribute & attrHidden;
	}

BOOL CFatDirShort::IsSystem(void) const
{
	return m_bAttribute & attrSystem;
	}

BOOL CFatDirShort::IsArchive(void) const
{
	return m_bAttribute & attrArchive;
	}

BOOL CFatDirShort::IsFile(void) const
{
	return (m_bAttribute & (attrDir | attrVolume)) == 0x00;
	}

BOOL CFatDirShort::IsDirectory(void) const
{
	return (m_bAttribute & (attrDir | attrVolume)) == attrDir;
	}

BOOL CFatDirShort::IsVolume(void) const
{
	return (m_bAttribute & (attrDir | attrVolume)) == attrVolume;
	}

BOOL CFatDirShort::IsShort(void) const
{
	return ((m_bAttribute & attrLongMask) != attrLong) && m_sText[0] != CHAR(BYTE(constFree));
	}

BOOL CFatDirShort::IsLong(void) const
{
	return ((m_bAttribute & attrLongMask) == attrLong) && m_sText[0] != CHAR(BYTE(constFree));
	}

DWORD CFatDirShort::GetFirstCluster(void) const
{
	return MAKELONG(m_wFirstClusterLo, m_wFirstClusterHi);
	}

time_t CFatDirShort::GetUnixTime(void) const
{
	time_t t = 0;

	t += ((CFatTime &) m_wWriteTime).Get();

	t += ((CFatDate &) m_wWriteDate).Get();

	return t;
	}

DWORD CFatDirShort::GetPackedTime(void) const
{
	return MAKELONG(m_wWriteTime, m_wWriteDate);	
	}

// Operations

void CFatDirShort::MakeEmpty(void)
{
	memset(this, 0x00, sizeof(FatDirShort));
	}

void CFatDirShort::MakeFree(void)
{
	MakeEmpty();

	m_sText[0] = BYTE(constFree);
	}

void CFatDirShort::MakeFreeLast(void)
{
	MakeEmpty();

	m_sText[0] = constLast;
	}

void CFatDirShort::MakeReadOnly(void)
{
	m_bAttribute |= attrReadOnly;
	}

void CFatDirShort::MakeHidden(void)
{
	m_bAttribute |= attrHidden;
	}

bool CFatDirShort::MakeArchive(void)
{
	if( !(m_bAttribute & attrArchive) ) {
	
		m_bAttribute |= attrArchive;

		return true;
		}

	return false;
	}

void CFatDirShort::MakeDirectory(void)
{
	m_bAttribute |= attrDir;
	}

bool CFatDirShort::ClearArchive(void)
{
	if( m_bAttribute & attrArchive ) {
	
		m_bAttribute |= attrArchive;

		return true;
		}

	return false;
	}

bool CFatDirShort::UpdateTimeStamps(void)
{
	timeval t;	

	if( !gettimeofday(&t, NULL) ) {

		WORD  wTime = m_wWriteTime;

		WORD  wDate = m_wWriteDate;

		((CFatTime &) m_wWriteTime).Set(t.tv_sec);

		((CFatDate &) m_wWriteDate).Set(t.tv_sec);

		if( m_wWriteTime != wTime || m_wWriteDate != wDate ) {

			m_wDateAccess = m_wWriteDate;

			return true;
			}
		}

	return false;
	}

bool CFatDirShort::UpdateAccessTime(void)
{
	timeval t;	

	if( !gettimeofday(&t, NULL) ) {

		WORD  wDate = m_wDateAccess;

		((CFatDate &) m_wDateAccess).Set(t.tv_sec);

		return m_wDateAccess != wDate;
		}

	return false;
	}

void CFatDirShort::SetFirstCluster(DWORD dwCluster)
{
	m_wFirstClusterLo = LOWORD(dwCluster);

	m_wFirstClusterHi = HIWORD(dwCluster);
	}

void CFatDirShort::SetUnixTime(time_t time)
{
	((CFatTime &) m_wWriteTime).Set(time);

	((CFatDate &) m_wWriteDate).Set(time);

	m_wDateAccess = m_wWriteDate;
	}

void CFatDirShort::SetPackedTime(DWORD Time)
{
	m_wCreateTime = LOWORD(Time);
		
	m_wCreateDate = HIWORD(Time);

	m_wWriteTime  = m_wCreateTime;

	m_wWriteDate  = m_wCreateDate;

	m_wDateAccess = m_wWriteDate;
	}

UINT CFatDirShort::GetFullName(PTXT pText) const
{
	UINT uLen = GetBareName(pText);

	if( GetExtensionLen() ) {

		pText[uLen++] = '.';

		uLen += GetExtension(pText + uLen);
		}

	return uLen;
	}

UINT CFatDirShort::GetBareName(PTXT pText) const
{
	if( IsEmpty() ) {

		pText[0] = 0;

		return 0;
		}

	if( IsSpecial() ) {

		UINT uLen = IsDotDot() ? 2 : IsDot() ? 1 : 0;

		memcpy(pText, m_sText, uLen);

		pText[uLen] = 0;

		return uLen;
		}

	UINT uLen = GetBareNameLen();
	
	memcpy(pText, m_sText, uLen);

	RemoveGuard(pText);

	pText[uLen] = 0;

	return uLen;
	}

UINT CFatDirShort::GetExtension(PTXT pText) const
{
	if( !IsEmpty() && !IsSpecial() ) {

		UINT uLen = GetExtensionLen();

		memcpy(pText, m_sText + 8, uLen);

		pText[uLen] = 0;
		
		return uLen;
		}

	pText[0] = 0;

	return 0;
	}

BOOL CFatDirShort::SetFullName(PCTXT pText)
{
	PTXT pFind = PTXT(strchr(pText, '.'));

	if( pFind ) {

		if( pFind == pText ) {

			if( !pFind[1] ) {

				MakeDot();
				
				return true;
				}
			
			if( pFind[1] == '.' ) {
				
				MakeDotDot();

				return true;
				}
			}
		
		if( !SetBareName(pText, pFind - pText) ) {

			return false;
			}

		if( !SetExtension(pFind + 1) ) {

			return false;
			}

		return TRUE;
		}

	if( strlen(pText) > 8 ) {

		if( !SetBareName(pText, 8) ) {

			return false;
			}

		if( !SetExtension(pText + 8) ) {

			return false;
			}

		return true;
		}

	return SetBareName(pText);
	}

BOOL CFatDirShort::SetBareName(PCTXT pName)
{
	return SetBareName(pName, pName ? strlen(pName) : 0);
	}

BOOL CFatDirShort::SetExtension(PCTXT pName)
{
	return SetExtension(pName, pName ? strlen(pName) : 0);
	}

BYTE CFatDirShort::GetChecksum(void) const
{
	BYTE  bSum = 0;

	PBYTE pSrc = PBYTE(m_sText);

	for( UINT i = sizeof(m_sText); i != 0; i-- ) {

		bSum = ((bSum & 1) ? 0x80 : 0) + (bSum >> 1) + *pSrc++;
		}

	return bSum;
	}

BOOL CFatDirShort::CheckName(PCTXT pText) const
{
	UINT iFind = NOTHING;

	for( UINT i = 0; ; i++ ) {

		char c = pText[i];

		if( c == '.' ) {

			if( i == 0 && pText[1] == 0 ) {

				return true;
				}
				
			if( i == 1 && pText[2] == 0 ) {

				return true;
				}

			if( iFind == NOTHING ) {
				
				iFind = i;

				continue;
				}

			return false;
			}

		if( c == char(NULL) ) {

			if( iFind != NOTHING ) {

				if( iFind > 8 ) {

					return false;
					}

				if( (i - iFind) > 4 ) {

					return false;
					}

				return true;
				}

			return i <= 8; 
			}

		if( !CheckName(c) ) {

			return false;
			}
		}

	return false;
	}

// Dump

void CFatDirShort::Dump(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nFat Directory Short Entry\n");

	AfxTrace("Status              = %s\n",      IsValid() ? "OK" : "Invalid");
	AfxTrace("Name                = %s\n",      m_sText);
	AfxTrace("Attribute           = 0x%4.4X\n", m_bAttribute);
	AfxTrace("NtrReserved         = 0x%4.4X\n", m_bNtrReserved);
	AfxTrace("CreateTimeTenth     = 0x%4.4X\n", m_bCreateTimeTenth);
	AfxTrace("CreateTime          = 0x%4.4X\n", m_wCreateTime);
	AfxTrace("CreateDate          = 0x%4.4X\n", m_wCreateDate);
	AfxTrace("DateAccess          = 0x%4.4X\n", m_wDateAccess);
	AfxTrace("FirstClusterHi      = 0x%4.4X\n", m_wFirstClusterHi);
	AfxTrace("WriteTime           = 0x%4.4X\n", m_wWriteTime);
	AfxTrace("WriteDate           = 0x%4.4X\n", m_wWriteDate);
	AfxTrace("FirstClusterLo      = 0x%4.4X\n", m_wFirstClusterLo);
	AfxTrace("Size                = 0x%8.8X\n", m_dwSize);
	
	#endif
	}

// Implementation

void CFatDirShort::MakeDot(void)
{
	m_sText[0] = '.';
	
	memset(m_sText + 1, 0x20, sizeof(m_sText) - 1);
	}

void CFatDirShort::MakeDotDot(void)
{
	m_sText[0] = '.';
	
	m_sText[1] = '.';
	
	memset(m_sText + 2, 0x20, sizeof(m_sText) - 2);
	}

UINT CFatDirShort::GetFullNameLen(void) const
{
	return GetBareNameLen() + GetExtensionLen();
	}

UINT CFatDirShort::GetBareNameLen(void) const
{
	for( UINT n = 0; n < 8; n ++ ) {

		if( m_sText[n] != 0x20 ) {

			continue;
			}

		return n; 
		}

	return 8;
	}

UINT CFatDirShort::GetExtensionLen(void) const
{
	for( UINT n = 0; n < 3; n ++ ) {

		if( m_sText[8 + n] != 0x20 ) {

			continue;
			}

		return n; 
		}

	return 3;
	}

BOOL CFatDirShort::SetBareName(PCTXT pName, UINT uLen)
{
	if( uLen && uLen <= 8 ) {

		memcpy(m_sText, pName, uLen);

		memset(m_sText + uLen, 0x20, sizeof(m_sText) - uLen);

		MakeUpper(0, uLen);

		CheckGuard(m_sText);
		
		return CheckName();
		}

	return false;
	}

BOOL CFatDirShort::SetExtension(PCTXT pName, UINT uLen)
{
	if( uLen == 0 ) {

		memset(m_sText + 8, 0x20, 3);

		return true;
		}

	if( uLen <= 3 ) {

		memcpy(m_sText + 8, pName, uLen);
			
		memset(m_sText + 8 + uLen, 0x20, 3 - uLen);

		MakeUpper(8, uLen);

		return CheckName();
		}

	return false;
	}

BOOL CFatDirShort::CheckName(void) const
{
	for( UINT u = 0; u < sizeof(m_sText); u ++ ) {
		
		char c = m_sText[u];
	
		if( c == 0x05 && u == 0 ) {

			continue;
			}

		if( !CheckName(c) ) {

			return false;
			}
		}

	return true;
	}

BOOL CFatDirShort::CheckName(CHAR c) const
{
	if( c < 0x20 ) {

		return false;
		}

	if( c >= 'a' && c <= 'z' ) {

		return false;
		}
			
	switch( c ) {

		case 0x22 :
		case 0x2A :
		case 0x2B :
		case 0x2C :
		case 0x2E :
		case 0x2F :
		case 0x3A :
		case 0x3B :
		case 0x3C :
		case 0x3D :
		case 0x3E :
		case 0x3F :
		case 0x5B :
		case 0x5C :
		case 0x5D :
		case 0x7C :

			return false;
		}

	return true;
	}

void CFatDirShort::RemoveGuard(PTXT pName) const
{
	if( pName && pName[0] == 0x05 ) {

		pName[0] = BYTE(0xE5);
		}
	}

void CFatDirShort::CheckGuard(PTXT pName) const
{
	if( pName && pName[0] == 0xE5 ) {

		pName[0] = BYTE(0x05);
		}
	}

void CFatDirShort::MakeUpper(void)
{
	MakeUpper(0, sizeof(m_sText));
	}

void CFatDirShort::MakeUpper(UINT iStart, UINT uCount)
{
	UINT iEnd = iStart + uCount;

	MakeMin(iStart, sizeof(m_sText));

	MakeMin(iEnd,   sizeof(m_sText));

	for( UINT i = iStart; i < iEnd; i ++ ) {

		char &c = m_sText[i];

		if( c >= 'a' && c <= 'z' ) {

			c &= ~Bit(5);
			}
		}
	}

// End of File