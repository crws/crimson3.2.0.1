
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Fat16BootSector_HPP

#define	INCLUDE_Fat16BootSector_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "Fat16BiosParamBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Boot Sector
//

#pragma pack(1)

struct Fat16BootSector
{
	BYTE			m_bBootCode[3];
	BYTE			m_bOemName[8];
	Fat16BiosParamBlock	m_BiosParamBlock;
	BYTE			m_bDriveNum;
	BYTE			m_bReserved1;
	BYTE			m_bBootSig;
	DWORD			m_dwVolId;
	BYTE			m_bVolLabel[11];
	BYTE			m_bFileSystem[8];
	BYTE			m_bPadding[448];
	WORD			m_wMagic;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat16 Boot Sector
//

class CFat16BootSector : public Fat16BootSector
{
	public:
		// Constructor
		CFat16BootSector(void);
		CFat16BootSector(CPartitionEntry const &That);

		// Initialisation
		void Init(CPartitionEntry const &Partition);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL  IsValid(void) const;
		UINT  GetClusterSize(void) const;
		UINT  GetRootDirSize(void) const;
		UINT  GetRootDirSectors(void) const;
		INT64 GetDataSize(void) const;
		DWORD GetDataSectors(void) const;
		DWORD GetDataClusters(void) const;
		DWORD GetFirstSector(DWORD dwCluster) const;
		DWORD GetFirstFatSector(WORD wFat) const;
		DWORD GetFirstRootDirSector(void) const;
		DWORD GetFirstDataSector(void) const;
		DWORD GetFatSectors(void) const;
		DWORD GetFatPerSector(void) const;
		DWORD GetTotalSectors(void) const;

		// Bios Parameter Block
		CFat16BiosParamBlock & GetBiosParamBlock(void) const;

		// Dump
		void Dump(void) const;

	protected:
		// Constants
		enum
		{
			constExtended	= 0x29,
			constMagic	= 0xAA55,
			};
	};

// End of File

#endif

