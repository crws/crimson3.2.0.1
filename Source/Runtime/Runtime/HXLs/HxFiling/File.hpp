
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_File_HPP

#define	INCLUDE_File_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

class CFile : public IFile
{
	public:
		// Constructors
		CFile(void);
		CFile(IFilingSystem *pSystem);

		// Destructor
		~CFile(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IFile Initialization
		BOOL Attach(IFilingSystem *pFileSystem);

		// IFile Attributes
		HFILE GetHandle(void);
		UINT  GetLength(void);

		// IFile Management
		BOOL Create(CFilename const &Name);
		BOOL Create(CFilename const &Name, FSIndex const &Dir);
		BOOL Open(CFilename const &Name, UINT Mode);
		BOOL Open(CFilename const &Name, FSIndex const &Dir, UINT Mode);
		BOOL Open(FSIndex const &Index, UINT Mode);
		BOOL Close(void);
		BOOL Delete(void);
		BOOL SetLength(UINT uLenght);
		BOOL Rename(CFilename const &Name);

		// IFile I/O
		UINT   Read(PBYTE pData, UINT uCount);
		UINT   Write(PCBYTE pData, UINT uCount);
		BOOL   SetPos(INT nOffset, UINT Mode);
		BOOL   SetBeg(void);
		BOOL   SetEnd(void);
		UINT   GetPos(void);
		BOOL   SetUnixTime(time_t time);
		time_t GetUnixTime(void);

	protected:
		// Data
		ULONG	         m_uRefs;
		HFILE		 m_hFile;
		CHAR             m_cDrive;
		IVolumeManager * m_pVolMan;
		IDiskManager   * m_pDiskMan;
		IFileManager   * m_pFileMan;
		IFilingSystem  * m_pSystem;
		UINT             m_uPos;

		// Implementation
		BOOL IsValid(void);
		BOOL FindFileSystem(CFilename const &Name);
		void Detach(void);
	};

// End of File

#endif
