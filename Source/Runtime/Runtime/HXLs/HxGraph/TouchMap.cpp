
#include "Intern.hpp"

#include "TouchMap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Touch Map Implementation
//

// Instantiator

static IUnknown * Create_TouchMap(PCTXT pName)
{
	return New CTouchMap;
	}

// Registration

global void Register_TouchMap(void)
{
	piob->RegisterInstantiator("graph.touchmap", Create_TouchMap);
	}

// Constructor

CTouchMap::CTouchMap(void)
{
	StdSetRef();

	m_xOffset = 0;

	m_yOffset = 0;

	m_fData   = FALSE;

	m_fSet    = TRUE;
	}

// Destructor

CTouchMap::~CTouchMap(void)
{
	if( m_fData ) {

		delete [] m_pData;
		}
	}

// IUnknown

HRESULT CTouchMap::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ITouchMap);

	StdQueryInterface(ITouchMap);

	return E_NOINTERFACE;
	}

ULONG CTouchMap::AddRef(void)
{
	StdAddRef();
	}

ULONG CTouchMap::Release(void)
{
	StdRelease();
	}

// Initialization

void CTouchMap::Create(ITouchScreen *pTouch)
{
	pTouch->GetCellSize (m_xCell, m_yCell);

	pTouch->GetDispCells(m_xSize, m_ySize);

	m_nStride = (m_xSize + 7) / 8;

	m_nTotal  = m_ySize * m_nStride;

	m_pData   = New BYTE [ m_nTotal ];

	m_fData   = TRUE;

	ClearMap();
	}

void CTouchMap::Create(IGdi *pGdi)
{
	m_xCell   = 4;
	
	m_yCell   = 4;

	m_xSize   = (pGdi->GetCx() + m_xCell - 1) / m_xCell;

	m_ySize   = (pGdi->GetCy() + m_yCell - 1) / m_yCell;

	m_nStride = (m_xSize + 7) / 8;

	m_nTotal  = m_ySize * m_nStride;

	m_pData   = New BYTE[m_nTotal];

	m_fData   = TRUE;

	ClearMap();
}

void CTouchMap::Create(int xSize, int ySize, int xCell, int yCell, PBYTE pData)
{
	m_xCell   = xCell;
	
	m_yCell   = yCell;

	m_xSize   = (xSize + m_xCell - 1) / m_xCell;
	
	m_ySize   = (ySize + m_yCell - 1) / m_yCell;

	m_nStride = (m_xSize + 7) / 8;

	m_nTotal  = m_ySize * m_nStride;

	m_pData   = pData ? pData : New BYTE [ m_nTotal ];

	m_fData   = pData ? FALSE : TRUE;

	ClearMap();
	}

// Buffer Access

PBYTE CTouchMap::GetBuffer(void)
{
	return m_pData;
	}

// Offset Control

void CTouchMap::SetOffset(int xOffset, int yOffset)
{
	m_xOffset = xOffset;

	m_yOffset = yOffset;
	}

// Color Selection

void CTouchMap::SetColor(BOOL fSet)
{
	m_fSet = fSet;
	}

void CTouchMap::ClearMap(void)
{
	memset(m_pData, 0, m_nTotal);
	}
	
// Drawing Operations

void CTouchMap::SetCell(int x, int y)
{
	x += m_xOffset;

	y += m_yOffset;

	ToCell(x, y);
	
	if( x >= 0 && x < m_xSize ) {

		if( y >= 0 && y < m_ySize ) {

			IntSetCell(x, y);
			}
		}
	}

void CTouchMap::FillRect(int x1, int y1, int x2, int y2)
{
	x1 += m_xOffset;

	y1 += m_yOffset;

	x2 += m_xOffset;

	y2 += m_yOffset;

	ToCell(x1, y1);

	ToCellRoundUp(x2, y2);

	IntFillRect(x1, y1, x2, y2);
	}

void CTouchMap::FillEllipse(int x1, int y1, int x2, int y2, UINT uType)
{
	x1 += m_xOffset;

	y1 += m_yOffset;

	x2 += m_xOffset;

	y2 += m_yOffset;

	ToCell(x1, y1);

	ToCellRoundUp(x2, y2);

	PrepEllipse(x1, y1, x2, y2, uType);

	DrawEllipse4();
	}

void CTouchMap::FillWedge(int x1, int y1, int x2, int y2, UINT uType)
{
	ToCell(x1, y1);

	ToCellRoundUp(x2, y2);

	if( uType == etQuad1 || uType == etQuad3 ) {

		m_dx = (uType == etQuad1) ? x2 - 1 : x1;

		PlotLine(x1, y1, x2 - 1, y2 - 1);
		}

	if( uType == etQuad2 || uType == etQuad4 ) {

		m_dx = (uType == etQuad2) ? x1 : x2 - 1;

		PlotLine(x2 - 1, y1, x1, y2 - 1);
		}
	}

// Diagnostics

void CTouchMap::Show(IGdi *pGdi)
{
	pGdi->SetForeColor(GetRGB(15, 0, 0));

	for( int y = 0; y < m_ySize; y++ ) {

		PBYTE p = m_pData + y * m_nStride;

		BYTE  b = 0x80;

		for( int x = 0; x < m_xSize; x++ ) {

			if( *p & b ) {

				int x1 = x  * m_xCell;

				int y1 = y  * m_yCell;

				int x2 = x1 + m_xCell;
	
				int y2 = y1 + m_yCell;

				pGdi->FillRect(x1, y1, x2 - 1, y2 - 1);
				}

			if( !(b >>= 1) ) {

				b = 0x80;

				p++;
				}
			}
		}
	}

// Conversion

void CTouchMap::ToCell(int &x, int &y)
{
	x = x / m_xCell;
	
	y = y / m_yCell;
	}

void CTouchMap::ToCellRoundUp(int &x, int &y)
{
	x = (x + m_xCell - 1) / m_xCell;
	
	y = (y + m_yCell - 1) / m_yCell;
	}

void CTouchMap::ToPixel(int &x, int &y)
{
	x = x * m_xCell;
	
	y = x * m_yCell;
	}

// Basic Operations

void CTouchMap::IntSetCell(int x, int y)
{
	PBYTE p = m_pData + (x + y * m_nStride) / 8;

	BYTE  s = 0x80 >> (x % 8);

	if( m_fSet ) {

		*p |=  s;
		}
	else
		*p &= ~s;
	}

void CTouchMap::IntFillRect(int x1, int y1, int x2, int y2)
{
	int dx = x2 - x1;

	while( y1 < y2 ) {

		HorzFill(x1, y1, dx);
		
		y1++;
		}
	}

void CTouchMap::HorzFill(int x, int y, int n)
{
	if( y >= 0 && y < m_ySize ) {

		MakeMin(n, m_xSize - x);
	
		if( x < 0 ) {
		
			n = n + x;

			x = 0;
			}

		IntHorzFill(x, y, n);
		}
	}

void CTouchMap::IntHorzFill(int x, int y, int n)
{
	BYTE  s = 0x80   >> (x % 8);

	PBYTE p = m_pData + (x / 8) + y * m_nStride;

	while( n-- ) {

		if( m_fSet ) {

			*p |=  s;
			}
		else
			*p &= ~s;

		if( !(s >>= 1) ) {
			
			s = 0x80;

			p++;
			}
		}
	}

// Line Helpers

void CTouchMap::PlotLineHorz(int x, int y, int n)
{
	if( m_dx > x ) {

		n = m_dx - x + 1;
		}
	else {
		n = n + x - m_dx;
			
		x = m_dx;
		}

	HorzFill(x, y, n);
	}

void CTouchMap::PlotLineVert(int x, int y, int n)
{
	if( m_dx > x ) {
	
		IntFillRect(x, y, m_dx + 1, y + n);

		return;
		}
	
	IntFillRect(m_dx, y, x + 1, y + n);
	}

void CTouchMap::PlotLine(int x1, int y1, int x2, int y2)
{
	x1 += m_xOffset;

	y1 += m_yOffset;

	x2 += m_xOffset;

	y2 += m_yOffset;

	int h = x2 - x1;
	
	int v = y2 - y1;
	
	BOOL fPosX = (h >= 0);

	BOOL fPosY = (v >= 0);
	
	h = abs(h);
	
	v = abs(v);

	if( !h ) {
	
		PlotLineVert(x1, fPosY ? y1 : y2, v + 1);
			
		return;
		}

	if( !v ) {
	
		PlotLineHorz(fPosX ? x1 : x2, y1, h + 1);
			
		return;
		}

	if( h >= v ) {
	
		if( fPosX ) {

			if( fPosY ) {

				LineCodeH(x1 + x, y + 1);

				return;
				}

			LineCodeH(x1 + x, y - 1);
			}
		else {
			if( fPosY ) {

				LineCodeH(x1 - e + 1, y + 1);

				return;
				}

			LineCodeH(x1 - e + 1, y - 1);
			}
		}
	else {
		if( fPosX ) {

			if( fPosY ) {

				LineCodeV(y1 + y, x + 1);

				return;
				}

			LineCodeV(y1 - e + 1, x + 1);
			}
		else {
			if( fPosY ) {

				LineCodeV(y1 + y, x - 1);

				return;
				}

			LineCodeV(y1 - e + 1, x - 1);
			}
		}
	}

// Ellipse Helpers

void CTouchMap::PrepEllipse(int x1, int y1, int x2, int y2, UINT uType)
{
	switch( uType ) {

		case etWhole:
			m_uDraw = drawQuad1 | drawQuad2 | drawQuad3 | drawQuad4;
			break;

		case etHalf1:
			m_uDraw = drawQuad1 | drawQuad4;
			break;

		case etHalf2:
			m_uDraw = drawQuad1 | drawQuad2;
			break;

		case etHalf3:
			m_uDraw = drawQuad2 | drawQuad3;
			break;

		case etHalf4:
			m_uDraw = drawQuad3 | drawQuad4;
			break;

		case etQuad1:
			m_uDraw = drawQuad1;
			break;

		case etQuad2:
			m_uDraw = drawQuad2;
			break;

		case etQuad3:
			m_uDraw = drawQuad3;
			break;

		case etQuad4:
			m_uDraw = drawQuad4;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf2:
		case etHalf4:
			m_xn = (x1 + x2) / 2;
			break;

		case etHalf1:
		case etQuad1:
		case etQuad4:
			m_xn = x1;
			break;

		case etHalf3:
		case etQuad2:
		case etQuad3:
			m_xn = x2 - 1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf1:
		case etHalf3:
			m_yn = (y1 + y2) / 2;
			break;

		case etHalf2:
		case etQuad1:
		case etQuad2:
			m_yn = y2 - 1;
			break;

		case etHalf4:
		case etQuad3:
		case etQuad4:
			m_yn = y1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf2:
		case etHalf4:
			m_xp = m_xn - 1 + (x1 + x2) % 2;
			break;

		case etHalf1:
		case etQuad1:
		case etQuad4:
			m_xp = x1;
			break;

		case etHalf3:
		case etQuad2:
		case etQuad3:
			m_xp = x2 - 1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf1:
		case etHalf3:
			m_yp = m_yn - 1 + (y1 + y2) % 2;
			break;

		case etHalf2:
		case etQuad1:
		case etQuad2:
			m_yp = y2 - 1;
			break;

		case etHalf4:
		case etQuad3:
		case etQuad4:
			m_yp = y1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf2:
		case etHalf4:
			m_rx = (x2 - x1) / 2;
			break;

		case etHalf1:
		case etHalf3:
		case etQuad1:
		case etQuad2:
		case etQuad3:
		case etQuad4:
			m_rx = (x2 - x1) - 1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf1:
		case etHalf3:
			m_ry = (y2 - y1) / 2;
			break;

		case etHalf2:
		case etHalf4:
		case etQuad1:
		case etQuad2:
		case etQuad3:
		case etQuad4:
			m_ry = (y2 - y1) - 1;
			break;
		}
	}

void CTouchMap::PlotEllipse4(int x, int y)
{
	if( m_dy != y ) {

		if( (m_uDraw & drawQuad1) && (m_uDraw & drawQuad2) ) {

			HorzFill(m_xn - x, m_yn - y, 2 * x + m_xp - m_xn + 1);
			}
		else {
			if( m_uDraw & drawQuad1 ) {

				HorzFill(m_xp, m_yn - y, x + 1);
				}

			if( m_uDraw & drawQuad2 ) {

				HorzFill(m_xn - x, m_yn - y, x + 1);
				}
			}

		if( (m_uDraw & drawQuad3) && (m_uDraw & drawQuad4) ) {

			HorzFill(m_xn - x, m_yp + y, 2 * x + m_xp - m_xn + 1);
			}
		else {
			if( m_uDraw & drawQuad3 ) {

				HorzFill(m_xn - x, m_yp + y, x + 1);
				}

			if( m_uDraw & drawQuad4 ) {

				HorzFill(m_xp, m_yp + y, x + 1);
				}
			}

		m_dy = y;
		}
	}

void CTouchMap::DrawEllipse4(void)
{
	int t1 = m_rx * m_rx;
	int t2 = t1 << 1;
	int t3 = t2 << 1;
	int t4 = m_ry * m_ry;
	int t5 = t4 << 1;
	int t6 = t5 << 1;
	int t7 = m_rx * t5;
	int t8 = t7 << 1;
	int t9 = 0;

	int d1 = t2 - t7 + (t4 >> 1);
	int d2 = (t1 >> 1) - t8 + t5;

	int xp = m_rx;
	int yp = 0;

	m_dy = m_ry;
	
	while( d2 < 0 ) {

		PlotEllipse4(xp, yp);
	
		yp += 1;
		t9 += t3;
		
		if( d1 < 0 ) {
			d1 += t9 + t2;
			d2 += t9;
			}
		else {
			xp -= 1;
			t8 -= t6;
			d1 += t9 + t2 - t8;
			d2 += t9 + t5 - t8;
			}
		}

	do {
		PlotEllipse4(xp, yp);

		xp -= 1;
		t8 -= t6;
		
		if( d2 < 0 ) {
			yp += 1;
			t9 += t3;
			d2 += t9 + t5 - t8;
			}
		else
			d2 += t5 - t8;

		} while( xp >= 0 );
	}

// End of File
