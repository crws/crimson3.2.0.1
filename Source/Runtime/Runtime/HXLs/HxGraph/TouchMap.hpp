
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TouchMap_HPP
	
#define	INCLUDE_TouchMap_HPP

//////////////////////////////////////////////////////////////////////////
//
// Touch Map Implementation
//

class CTouchMap : public ITouchMap
{
	public:
		// Constructor
		CTouchMap(void);

		// Destructor
		~CTouchMap(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Initialization
		void Create(ITouchScreen *pTouch);
		void Create(IGdi *pGdi);
		void Create(int xSize, int ySize, int xCell, int yCell, PBYTE pData);

		// Buffer Access
		PBYTE GetBuffer(void);

		// Offset Control
		void SetOffset(int xOffset, int yOffset);

		// Color Selection
		void SetColor(BOOL fSet);
		void ClearMap(void);
	
		// Drawing Operations
		void SetCell(int x, int y);
		void FillRect(int x1, int y1, int x2, int y2);
		void FillEllipse(int x1, int y1, int x2, int y2, UINT uType);
		void FillWedge(int x1, int y1, int x2, int y2, UINT uType);

		// Diagnostics
		void Show(IGdi *pGdi);

	protected:
		// Draw Flags
		enum
		{
			drawQuad1	= 0x10,
			drawQuad2	= 0x20,
			drawQuad3	= 0x40,
			drawQuad4	= 0x80,
			};

		// Data Members
		ULONG m_uRefs;
		int   m_xSize;
		int   m_ySize;
		int   m_xCell;
		int   m_yCell;
		int   m_xOffset;
		int   m_yOffset;
		int   m_nStride;
		int   m_nTotal;
		PBYTE m_pData;
		BOOL  m_fData;
		BOOL  m_fSet;
		UINT  m_uDraw;
		int   m_dx;
		int   m_dy;
		int   m_rx;
		int   m_ry;
		int   m_xp;
		int   m_yp;
		int   m_xn;
		int   m_yn;

		// Conversion
		void ToCell(int &x, int &y);
		void ToCellRoundUp(int &x, int &y);
		void ToPixel(int &x, int &y);

		// Basic Operations
		void IntSetCell(int x, int y);
		void IntFillRect(int x1, int y1, int x2, int y2);
		void HorzFill(int x, int y, int n);
		void IntHorzFill(int x, int y, int n);

		// Line Helpers
		void PlotLineHorz(int x, int y, int n);
		void PlotLineVert(int x, int y, int n);
		void PlotLine(int x1, int y1, int x2, int y2);

		// Ellipse Helpers
		void PrepEllipse(int x1, int y1, int x2, int y2, UINT uType);
		void PlotEllipse4(int x, int y);
		void DrawEllipse4(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Fixed Fraction
//

static inline int Fraction(int a, int b)
{
	return (((a << 16) + (b / 2)) / b);
	}


//////////////////////////////////////////////////////////////////////////
//
// Line Macros
//

#define	LineCodeH(spos, step)				\
							\
	BEGIN {						\
							\
	int x = 0;					\
							\
	int y = y1;					\
							\
	int s = Fraction(h + 1, v + 1);			\
							\
	int t = 0;					\
							\
	do {						\
		t = t + s;				\
							\
		int e = int((t + 32768) >> 16);		\
							\
		PlotLineHorz(spos, y, e - x);		\
							\
		y = step;				\
							\
		x = e;					\
							\
		} while( v-- );				\
	} END						\

#define	LineCodeV(spos, step)				\
							\
	BEGIN {						\
							\
	int x = x1;					\
							\
	int y = 0;					\
							\
	int s = Fraction(v + 1, h + 1);			\
							\
	int t = 0;					\
							\
	do {						\
		t = t + s;				\
							\
		int e = int((t + 32768) >> 16);		\
							\
		PlotLineVert(x, spos, e - y);		\
							\
		x = step;				\
							\
		y = e;					\
							\
		} while( h-- );				\
	} END						\

// End of File

#endif
