
#include "Intern.hpp"

#include "GdiGeneric.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Gdi Implementation
//

// Macros

#define	IsValidLine() ((x2-x1)||(y2-y1))

#define	IsValidArea() ((x2>x1)&&(y2>y1))

// Constants

int const CGdiGeneric::styleBase = 16384;

int const CGdiGeneric::styleDiag = int(1.41421356 * styleBase);

int const CGdiGeneric::initMin   = +999999;

int const CGdiGeneric::initMax   = -999999;

int const CGdiGeneric::trigSize  = 90;

int const CGdiGeneric::trigStep  = 90 / trigSize;

int const CGdiGeneric::trigBits  = 16;

int const CGdiGeneric::trigScale = (1 << trigBits);

int const CGdiGeneric::tranScale = (1 << 8);

int const CGdiGeneric::sampBits  = 2;

int const CGdiGeneric::sampScale = (1 << sampBits);

int const CGdiGeneric::sampMask  = sampScale - 1;

int const CGdiGeneric::sampLimit = sampScale * sampScale;

// Standard Fonts

IGdiFont * CGdiGeneric::m_pStdFont[fontCount];

IGdiFont * CGdiGeneric::m_pDefFont = NULL;

// Trig Table

int * CGdiGeneric::m_pTrig = NULL;

// Constructor

CGdiGeneric::CGdiGeneric(void)
{
	StdSetRef();

	memset(&m_OldFont,  0xEB, sizeof(CLogFont ));

	memset(&m_OldBrush, 0xEB, sizeof(CLogBrush));

	memset(&m_OldPen,   0xEB, sizeof(CLogPen  ));

	m_cx        = 0;

	m_cy        = 0;

	m_cb	    = 0;

	m_xb        = NULL;

	m_o1	    = 0;

	m_o2        = 0;

	m_uBrushPtr = 0;

	m_uPenPtr   = 0;

	m_uFontPtr  = 0;

	SetIdentity();

	ResetAll();
	}

// Destructor

CGdiGeneric::~CGdiGeneric(void)
{
	delete [] m_xb;
	}

// Initialization

void CGdiGeneric::InitTables(void)
{
	MakeTrigTable();

	MakeStdFonts();
	}

void CGdiGeneric::FreeTables(void)
{
	FreeTrigTable();

	FreeStdFonts();
	}

// IUnknown

HRESULT CGdiGeneric::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IGdi);

	StdQueryInterface(IGdi);

	return E_NOINTERFACE;
	}

ULONG CGdiGeneric::AddRef(void)
{
	StdAddRef();
	}

ULONG CGdiGeneric::Release(void)
{
	StdRelease();
	}

// Initialization

void CGdiGeneric::Create(int cx, int cy, PVOID pData)
{
	m_cx = cx;

	m_cy = cy;

	m_cb = Max(m_cx, m_cy);

	m_xb = New int [ m_cb ];

	m_o1 = m_cb;

	m_o2 = 0;
	}

// Palette Access

DWORD CGdiGeneric::GetPaletteEntry(UINT uEntry)
{
	return ::GetPaletteEntry(uEntry);
	}

// Attributes

int CGdiGeneric::GetCx(void)
{
	return m_cx;
	}

int CGdiGeneric::GetCy(void)
{
	return m_cy;
	}

// Transformation

void CGdiGeneric::SetIdentity(void)
{
	MakeIdentity(m_M);

	m_fComplex   = FALSE;

	m_fTransform = FALSE;
	}

void CGdiGeneric::SetTransform(M3 const &M)
{
	m_M          = M;

	m_fComplex   = IsComplex();

	m_fTransform = TRUE;
	}

void CGdiGeneric::AddTransform(M3 const &M)
{
	if( m_fTransform ) {

		M3 R = {{{0,0,0},{0,0,0},{0,0,0}}};

		for( int i = 0; i < 3; i++ ) {

			for( int j = 0; j < 3; j++ ) {

				int s = 0;

				for( int k = 0; k < 3; k++ ) {

					s += m_M.e[k][j] * M.e[i][k];
					}

				R.e[i][j] = s / tranScale;
				}
			}

		m_M        = R;

		m_fComplex = IsComplex();

		return;
		}

	SetTransform(M);
	}

void CGdiGeneric::AddRotation(int nDegrees)
{
	if( nDegrees % 360 ) {

		if( m_fTransform ) {

			M3 M;

			MakeRotation(M, nDegrees);

			AddTransform(M);
			}
		else {
			MakeRotation(m_M, nDegrees);

			m_fComplex   = TRUE;
		
			m_fTransform = TRUE;
			}
		}
	}

void CGdiGeneric::AddMovement(int dx, int dy)
{
	if( dx || dy ) {

		if( m_fTransform ) {

			M3 M;

			MakeMovement(M, dx, dy);

			AddTransform(M);
			}
		else {
			MakeMovement(m_M, dx, dy);

			m_fComplex   = FALSE;

			m_fTransform = TRUE;
			}
		}
	}

void CGdiGeneric::AddMirror(BOOL x, BOOL y)
{
	if( x || y ) {

		if( m_fTransform ) {

			M3 M;

			MakeMirror  (M, x, y);

			AddTransform(M);
			}
		else {
			MakeMirror  (m_M, x, y);

			m_fComplex   = TRUE;
		
			m_fTransform = TRUE;
			}
		}
	}

void CGdiGeneric::MakeIdentity(M3 &M)
{
	int i = tranScale;

	M.e[0][0] = i; M.e[0][1] = 0; M.e[0][2] = 0;
	M.e[1][0] = 0; M.e[1][1] = i; M.e[1][2] = 0;
	M.e[2][0] = 0; M.e[2][1] = 0; M.e[2][2] = i;
	}

void CGdiGeneric::MakeRotation(M3 &M, int nDegrees)
{
	if( nDegrees % 360 ) {

		MakeTrigTable();

		int i = tranScale;

		int c = Cos(nDegrees) / (trigScale / tranScale);

		int s = Sin(nDegrees) / (trigScale / tranScale);

		M.e[0][0] = c; M.e[0][1] = -s; M.e[0][2] = 0;
		M.e[1][0] = s; M.e[1][1] =  c; M.e[1][2] = 0;
		M.e[2][0] = 0; M.e[2][1] =  0; M.e[2][2] = i;

		return;
		}

	MakeIdentity(M);
	}

void CGdiGeneric::MakeMovement(M3 &M, int dx, int dy)
{
	if( dx || dy ) {

		int i = tranScale;

		M.e[0][0] = i; M.e[0][1] = 0; M.e[0][2] = i * dx;
		M.e[1][0] = 0; M.e[1][1] = i; M.e[1][2] = i * dy;
		M.e[2][0] = 0; M.e[2][1] = 0; M.e[2][2] = i;

		return;
		}

	MakeIdentity(M);
	}

void CGdiGeneric::MakeMirror(M3 &M, BOOL x, BOOL y)
{
	if( x || y ) {

		int i = x ? -tranScale : +tranScale;

		int j = y ? -tranScale : +tranScale;

		int k = tranScale;

		M.e[0][0] = i; M.e[0][1] = 0; M.e[0][2] = 0;
		M.e[1][0] = 0; M.e[1][1] = j; M.e[1][2] = 0;
		M.e[2][0] = 0; M.e[2][1] = 0; M.e[2][2] = k;

		return;
		}

	MakeIdentity(M);
	}

// Combined Attributes

void CGdiGeneric::ResetAll(void)
{
	ResetFont();

	ResetBrush();

	ResetPen();
	}

void CGdiGeneric::PushAll(void)
{
	PushFont();

	PushBrush();

	PushPen();
	}

void CGdiGeneric::PullAll(void)
{
	PullFont();

	PullBrush();

	PullPen();
	}

void CGdiGeneric::ResetBoth(void)
{
	ResetBrush();

	ResetPen();
	}

void CGdiGeneric::PushBoth(void)
{
	PushBrush();

	PushPen();
	}

void CGdiGeneric::PullBoth(void)
{
	PullBrush();

	PullPen();
	}

void CGdiGeneric::SetBackMode(UINT Trans)
{
	m_Brush.m_Trans = Trans;
	
	m_Pen.  m_Trans = Trans;

	m_Font. m_Trans = Trans;

	DirtyBoth();

	DirtyFont();
	}

void CGdiGeneric::SetForeColor(COLOR Fore)
{
	m_Brush.m_Fore = Fore;

	m_Brush.m_Rich = 0;
	
	m_Pen.  m_Fore = Fore;

	m_Font. m_Fore = Fore;

	DirtyBoth();

	DirtyFont();
	}

void CGdiGeneric::SetBackColor(COLOR Back)
{
	m_Brush.m_Back = Back;
	
	m_Pen.  m_Back = Back;

	m_Font. m_Back = Back;

	DirtyBoth();

	DirtyFont();
	}

// Text Attributes

void CGdiGeneric::ResetFont(void)
{
	m_Font.m_pFont  = m_pDefFont;

	m_Font.m_Trans  = modeOpaque;

	m_Font.m_Smooth = 1;

	m_Font.m_Fore   = GetRGB(0xFF, 0xFF, 0xFF);
	
	m_Font.m_Back   = GetRGB(0x00, 0x00, 0x00);

	DirtyFont();
	}

void CGdiGeneric::PushFont(void)
{
	if( m_uFontPtr < elements(m_FontStack) ) {

		m_FontStack[m_uFontPtr++] = m_Font;
		}
	}

void CGdiGeneric::PullFont(void)
{
	if( m_uFontPtr > 0 ) {

		m_Font = m_FontStack[--m_uFontPtr];

		DirtyFont();
		}
	}

void CGdiGeneric::SelectFont(CLogFont const &Font)
{
	m_Font = Font;

	DirtyFont();
	}

void CGdiGeneric::SelectFont(UINT uFont)
{
	if( uFont ) {

		if( uFont < elements(m_pStdFont) ) {

			m_Font.m_pFont = m_pStdFont[uFont];

			DirtyFont();

			return;
			}
		}

	SelectFont(m_pDefFont);
	}

void CGdiGeneric::SelectFont(IGdiFont *pFont)
{
	if( pFont ) {

		if( HIWORD(pFont) ) {

			m_Font.m_pFont = pFont;

			DirtyFont();
			}
		else
			SelectFont(LOWORD(pFont));

		return;
		}

	SelectFont(m_pDefFont);
	}

void CGdiGeneric::GetFont(CLogFont &Font)
{
	Font = m_Font;
	}

void CGdiGeneric::SetTextTrans(UINT Trans)
{
	m_Font.m_Trans = Trans;

	DirtyFont();
	}

void CGdiGeneric::SetTextSmooth(UINT Smooth)
{
	m_Font.m_Smooth = Smooth;

	DirtyFont();
	}

void CGdiGeneric::SetTextFore(COLOR Fore)
{
	m_Font.m_Fore = Fore;

	DirtyFont();
	}

void CGdiGeneric::SetTextBack(COLOR Back)
{
	m_Font.m_Back = Back;

	DirtyFont();
	}

// Brush Attributes

void CGdiGeneric::ResetBrush(void)
{
	m_Brush.m_Style = brushFore;

	m_Brush.m_Trans = modeOpaque;
	
	m_Brush.m_Fore  = GetRGB(0xFF, 0xFF, 0xFF);
	
	m_Brush.m_Back  = GetRGB(0x00, 0x00, 0x00);

	m_Brush.m_Rich  = 0;

	m_Brush.m_Bits  = NULL;

	m_Brush.m_bcx   = 0;

	m_Brush.m_bcy   = 0;

	DirtyBrush();
	}

void CGdiGeneric::PushBrush(void)
{
	if( m_uBrushPtr < elements(m_BrushStack) ) {

		m_BrushStack[m_uBrushPtr++] = m_Brush;
		}
	}

void CGdiGeneric::PullBrush(void)
{
	if( m_uBrushPtr > 0 ) {

		m_Brush = m_BrushStack[--m_uBrushPtr];

		DirtyBrush();
		}
	}

void CGdiGeneric::SelectBrush(CLogBrush const &Brush)
{
	m_Brush = Brush;

	DirtyBrush();
	}

void CGdiGeneric::GetBrush(CLogBrush &Brush)
{
	Brush = m_Brush;
	}

void CGdiGeneric::SelectBrush(UINT Style)
{
	m_Brush.m_Style = Style;

	DirtyBrush();
	}

void CGdiGeneric::SetBrushStyle(UINT Style)
{
	m_Brush.m_Style = Style;

	DirtyBrush();
	}

void CGdiGeneric::SetBrushTrans(UINT Trans)
{
	m_Brush.m_Trans = Trans;

	DirtyBrush();
	}

void CGdiGeneric::SetBrushFore(COLOR Fore)
{
	m_Brush.m_Fore = Fore;

	m_Brush.m_Rich = 0;

	DirtyBrush();
	}

void CGdiGeneric::SetBrushFore(DWORD Rich)
{	
	m_Brush.m_Rich = Rich | (0xFF << 24);

	DirtyBrush();
	}

void CGdiGeneric::SetBrushBack(COLOR Back)
{
	m_Brush.m_Back = Back;

	DirtyBrush();
	}

void CGdiGeneric::SetBrushBits(PCVOID pd, int cx, int cy)
{
	m_Brush.m_Style = brushBitmap;

	m_Brush.m_Bits  = pd;

	m_Brush.m_bcx   = cx;

	m_Brush.m_bcy   = cy;

	DirtyBrush();
	}

// Pen Attributes

void CGdiGeneric::ResetPen(void)
{
	m_Pen.m_Style = penFore;
	
	m_Pen.m_Trans = modeOpaque;
	
	m_Pen.m_Width = 1;
	
	m_Pen.m_Caps  = capsBoth;
	
	m_Pen.m_Fore  = GetRGB(0xFF, 0xFF, 0xFF);
	
	m_Pen.m_Back  = GetRGB(0x00, 0x00, 0x00);

	DirtyPen();
	}

void CGdiGeneric::PushPen(void)
{
	if( m_uPenPtr < elements(m_PenStack) ) {

		m_PenStack[m_uPenPtr++] = m_Pen;
		}
	}

void CGdiGeneric::PullPen(void)
{
	if( m_uPenPtr > 0 ) {

		m_Pen = m_PenStack[--m_uPenPtr];

		DirtyPen();
		}
	}

void CGdiGeneric::SelectPen(CLogPen const &Pen)
{
	m_Pen = Pen;

	DirtyPen();
	}

void CGdiGeneric::GetPen(CLogPen &Pen)
{
	Pen = m_Pen;
	}

void CGdiGeneric::SelectPen(UINT Style)
{
	m_Pen.m_Style = Style;
	
	m_Pen.m_Width = 1;
	
	m_Pen.m_Caps  = capsNone;

	DirtyPen();
	}

void CGdiGeneric::SetPenStyle(UINT Style)
{
	m_Pen.m_Style = Style;

	DirtyPen();
	}

void CGdiGeneric::SetPenTrans(UINT Trans)
{
	m_Pen.m_Trans = Trans;

	DirtyPen();
	}

void CGdiGeneric::SetPenWidth(UINT Width)
{
	m_Pen.m_Width = Width ? (Width + 1) / 2 : 1;

	DirtyPen();
	}

void CGdiGeneric::SetPenCaps(UINT Caps)
{
	m_Pen.m_Caps = Caps;

	DirtyPen();
	}

void CGdiGeneric::SetPenFore(COLOR Fore)
{
	m_Pen.m_Fore = Fore;

	DirtyPen();
	}

void CGdiGeneric::SetPenBack(COLOR Back)
{
	m_Pen.m_Back = Back;

	DirtyPen();
	}

// Clearing

void CGdiGeneric::ClearScreen(COLOR Color)
{
	PushBrush();

	ResetBrush();

	SetBrushFore(Color);

	CheckBrush();

	FillArea(0, 0, m_cx, m_cy);

	PullBrush();
	}

// Pixel Access

void CGdiGeneric::SetPixel(int x, int y, COLOR Color)
{
	IntSetPixel(x, y, Color);
	}

// Text Metrics

int CGdiGeneric::GetTextHeight(PCTXT p)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		return m_cyFont;
		}

	return 0;
	}

int CGdiGeneric::GetTextWidth(PCTXT p)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		int tx = 0;

		if( m_fFontProp ) {

			while( *p ) {

				tx += m_Font.m_pFont->GetGlyphWidth(*p++);
				}
			}
		else {
			while( *p++ ) {

				tx += m_cxFont;
				}
			}

		return tx;
		}

	return 0;
	}

int CGdiGeneric::GetTextHeight(TCHAR c)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		return m_cyFont;
		}

	return 0;
	}

int CGdiGeneric::GetTextWidth(TCHAR c)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		if( m_fFontProp ) {

			return m_Font.m_pFont->GetGlyphWidth(c);
			}

		return m_cxFont;
		}

	return 0;
	}

// Text Output

void CGdiGeneric::TextOut(int x, int y, PCTXT p, UINT n)
{
	IGdiFont *pFont;

	if( (pFont = m_Font.m_pFont) ) {

		if( 1 || !m_fComplex ) {

			Transform(x, y);

			if( y < m_cy ) {

				CheckFont();

				pFont->InitBurst(this, m_Font);

				if( y > -m_cyFont) {

					if( x < 0 ) {

						int gx = m_cxFont;

						while( *p && n ) {

							if( m_fFontProp ) {

								WCHAR c = BYTE(*p);

								gx = pFont->GetGlyphWidth(c);
								}

							if( x > -gx ) {

								break;
								}

							x += gx;

							p += 1;

							n -= 1;
							}
						}

					while( *p && n ) {
								
						WCHAR c = BYTE(*p++);

						pFont->DrawGlyph(this, x, y, c);

						if( x >= m_cx ) {

							break;
							}

						n -= 1;
						}
					}

				pFont->BurstDone(this, m_Font);
				}
			}
		}
	}

void CGdiGeneric::TextOut(int x, int y, PCTXT p)
{
	TextOut(x, y, p, NOTHING);
	}

// Text Metrics

int CGdiGeneric::GetTextHeight(PCUTF p)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		return m_cyFont;
		}

	return 0;
	}

int CGdiGeneric::GetTextWidth(PCUTF p)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		int tx = 0;

		if( m_fFontProp ) {

			while( *p ) {

				tx += m_Font.m_pFont->GetGlyphWidth(*p++);
				}
			}
		else {
			while( *p++ ) {

				tx += m_cxFont;
				}
			}

		return tx;
		}

	return 0;
	}

int CGdiGeneric::GetTextHeight(WCHAR c)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		return m_cyFont;
		}

	return 0;
	}

int CGdiGeneric::GetTextWidth(WCHAR c)
{
	if( m_Font.m_pFont ) {

		CheckFont();

		if( m_fFontProp ) {

			return m_Font.m_pFont->GetGlyphWidth(c);
			}

		return m_cxFont;
		}

	return 0;
	}

// Text Output

void CGdiGeneric::TextOut(int x, int y, PCUTF p, UINT n)
{
	IGdiFont *pFont;

	if( (pFont = m_Font.m_pFont) ) {

		if( 1 || !m_fComplex ) {

			Transform(x, y);

			if( y < m_cy ) {

				CheckFont();

				pFont->InitBurst(this, m_Font);

				if( y > -m_cyFont) {

					if( x < 0 ) {

						int gx = m_cxFont;

						while( *p && n ) {

							if( m_fFontProp ) {

								gx = pFont->GetGlyphWidth(*p);
								}

							if( x > -gx ) {

								break;
								}

							x += gx;

							p += 1;

							n -= 1;
							}
						}

					while( *p && n ) {

						pFont->DrawGlyph(this, x, y, *p++);

						if( x >= m_cx ) {

							break;
							}

						n -= 1;
						}
					}

				pFont->BurstDone(this, m_Font);
				}
			}
		}
	}

void CGdiGeneric::TextOut(int x, int y, PCUTF p)
{
	TextOut(x, y, p, NOTHING);
	}

// Bitmaps

void CGdiGeneric::BitBlt(int x, int y, int cx, int cy, int s, PCBYTE p, UINT rop)
{
	PCWORD pw = PCWORD(p);

	for( int r = 0; r < cy; r++ ) {

		for( int c = 0; c < cx; c++ ) {

			WORD w = *pw++;

			if( w < 0xFFFF ) {

				IntSetPixel(x + c, y + r, w);
				}
			}

		pw += s / 2 - cx;
		}
	}

// Line Drawing

void CGdiGeneric::MoveTo(int x1, int y1)
{
	m_x0 = x1;

	m_y0 = y1;
	}

void CGdiGeneric::LineTo(int x1, int y1)
{
	DrawLine(m_x0, m_y0, x1, y1);

	m_x0 = x1;
	
	m_y0 = y1;
	}

void CGdiGeneric::DrawLine(int x1, int y1, int x2, int y2)
{
	if( CheckPen() ) {

		Transform(x1, y1, x2, y2);

		InitStyle();

		LineHelp(x1, y1, x2, y2);
		}

	m_x0 = x2;
	
	m_y0 = y2;
	}

// Polygon Drawing

void CGdiGeneric::DrawPolygon(P2 const *pList, UINT uCount, DWORD dwRound)
{
	if( CheckPen() ) {

		if( !m_fPenStyle && !m_fComplex ) {

			if( m_Pen.m_Width == 1 ) {

				DrawPolygonScan(pList, uCount, dwRound);

				return;
				}

			if( LOWORD(dwRound) ) {

				UINT n;

				for( n = 0; n < uCount; n++ ) {

					int x1 = pList[n].x;
					int y1 = pList[n].y;

					int x2 = pList[(n+1) % uCount].x;
					int y2 = pList[(n+1) % uCount].y;

					Transform(x1, y1, x2, y2);

					int dx = x2 - x1;
					int dy = y2 - y1;

					if( dwRound & (0x00001 << n) ) {

						// NOTE -- Even though elliptical curved edges
						// are not quite correct, they are good enough!
						}
					else {
						if( dx && dy ) {

							break;
							}
						}
					}

				if( n == uCount ) {

					PushBrush();

					m_Brush.m_Style = brushFore;

					m_Brush.m_Trans = modeOpaque;
					
					m_Brush.m_Fore  = m_fPenFore ? m_Pen.m_Fore : m_Pen.m_Back;

					m_Brush.m_Rich  = 0;

					DirtyBrush();

					DrawPolygonFill(pList, uCount, dwRound);

					PullBrush();

					return;
					}
				}
			}

		DrawPolygonJoin(pList, uCount, dwRound);
		}
	}

void CGdiGeneric::FillPolygon(P2 const *pList, UINT uCount, DWORD dwRound)
{
	ShadePolygon(pList, uCount, dwRound, NULL);
	}

// Shaded Polygon

void CGdiGeneric::ShadePolygon(P2 const *pList1, UINT uCount1, DWORD dwRound, PSHADER pShade)
{
	if( CheckBrush() || pShade ) {
		
		CAutoArray<CEdge> pEdge(uCount1);

		UINT uUsed = 0;

		BOOL fHorz = pShade ? (*pShade)(this,0,0) : FALSE;

		int yp = initMin;
		int ye = initMax;
		int xp = initMin;
		int xe = initMax;

		AddEdges(pEdge, uUsed, fHorz, pList1, uCount1, dwRound, xp, xe, yp, ye);

		OrderEdgesByY(pEdge, uUsed);

		CEdge *pHead = NULL;

		CEdge *pTail = NULL;

		int    yi    = yp;

		UINT   uEdge = 0;

		BOOL   fDraw = TRUE;

		while( yp <= ye ) {

			if( pShade ) {

				if( (*pShade)(this, yp - yi, ye - yi + 1) ) {

					fDraw = CheckBrush();
					}
				}

			while( uEdge < uUsed ) {

				if( pEdge[uEdge].yp == yp ) {

					CEdge *p = pEdge + uEdge;

					AfxListAppend(pHead, pTail, p, pNext, pPrev);

					uEdge++;

					continue;
					}

				break;
				}

			if( pHead ) {

				TrackEdges(pHead);

				if( !fDraw ) {

					CEdge *p1 = pHead;

					while( p1 ) {

						if( yp >= p1->ye ) {

							AfxListRemove(pHead, pTail, p1, pNext, pPrev);
							}

						p1 = p1->pNext;
						}
					}
				else {
					OrderEdgesByX(pHead, pTail);

					CEdge *p0 = NULL;

					CEdge *p1 = pHead;

					while( p1 ) {

						if( yp < p1->ye ) {

							if( p0 ) {

								if( fHorz ) {

									FillArea(yp, p0->x1, yp+1, p1->x2+1);
									}
								else
									FillArea(p0->x1, yp, p1->x2+1, yp+1);

								p0 = NULL;
								}
							else
								p0 = p1;
							}
						else {
							if( fHorz ) {

								FillArea(yp, p1->x1, yp+1, p1->x2+1);
								}
							else
								FillArea(p1->x1, yp, p1->x2+1, yp+1);

							AfxListRemove(pHead, pTail, p1, pNext, pPrev);
							}

						p1 = p1->pNext;
						}
					}
				}

			yp += 1;
			}
		}
	}

void CGdiGeneric::ShadePolygon(P2 const *pList1, UINT uCount1, P2 const *pList2, UINT uCount2, DWORD dwRound, PSHADER pShade)
{
	if( CheckBrush() || pShade ) {

		// Build a list of edges, one for each pair of vertices, including
		// the wrap-around from the last to the first. We might end up with
		// fewer actual edges if any are degenerate. The edge structures
		// contain the diffential constants and accumulators used to figure
		// out the x-wise coverage for each y coordinate. They will be updated
		// during the drawing process, and also added and removed from our
		// active list to determine the pixel coverage to be generated.

		CAutoArray<CEdge> pEdge(uCount1 + uCount2);

		// Initialize the shader and figure out if we're in horz mode,
		// in which case we'll be swapping the x and y coordinates to
		// allow the fill to procede left-to-right.

		UINT uUsed = 0;

		BOOL fHorz = pShade ? (*pShade)(this, 0, 0) : FALSE;

		// Used to track the extremities of the figure.

		int yp = initMin;
		int ye = initMax;
		int xp = initMin;
		int xe = initMax;

		// Add the two sets of edges to our list.

		AddEdges(pEdge, uUsed, fHorz, pList1, uCount1, dwRound, xp, xe, yp, ye);
		
		AddEdges(pEdge, uUsed, fHorz, pList2, uCount2, dwRound, xp, xe, yp, ye);

		// Keep a copy of the initial y position for shading.

		int  yi = yp;

		// Sort the edges by y-coordinate so that we can more
		// efficiently spot which edges are to be added to the 
		// active list by working from the head of the list.

		OrderEdgesByY(pEdge, uUsed);

		// Start with an empty active edge list.

		CEdge *pHead = NULL;

		CEdge *pTail = NULL;

		// Reset the next edge to check and the flag used
		// to track whether the shader has set a null brush
		// such that we don't really need to draw anything.

		UINT uEdge = 0;

		BOOL fDraw = TRUE;

		// Loop around for each oversampled pixel.

		while( yp <= ye ) {

			// If we have a shader, give it the chance to update
			// the current brush, and if it does so, find out if
			// we have a non-null brush and need to draw.

			if( pShade ) {

				if( (*pShade)(this, yp - yi, ye - yi + 1) ) {

					fDraw = CheckBrush();
					}
				}

			// Check to see if we have new active edges.

			while( uEdge < uUsed ) {

				if( pEdge[uEdge].yp == yp ) {

					// Add the new edge to the active list and
					// walk down the list to see if we need to
					// add any more edges.

					CEdge *p = pEdge + uEdge;

					AfxListAppend(pHead, pTail, p, pNext, pPrev);

					uEdge++;

					continue;
					}

				break;
				}

			if( pHead ) {

				// Call the routine to set the x1 and x2 members of
				// each active edge to the pixel coverage, and to run
				// the difference engine to track the edge.

				TrackEdges(pHead);

				// Remove completed edges from the active list.

				for( CEdge *ps = pHead; ps; ) {

					if( ps->ye == yp ) {

						// On last line, only remove horizontal edges
						// as we need the rest to get a nice finish.

						if( yp < ye || ps->et == edgeHorizontal ) {

							AfxListRemove(pHead, pTail, ps, pNext, pPrev);
							}
						}

					ps = ps->pNext;
					}

				if( pHead && fDraw && yp >= 0 ) {

					// Sort the edges left-to-right.
						
					OrderEdgesByX(pHead, pTail);

					// Loop through pairs of edges, alternating
					// between drawing and skipping as we go.

					CEdge *ps = pHead;

					CEdge *p1 = NULL;

					while( ps ) {

						if( p1 ) {

							if( fHorz ) {

								FillArea(yp, p1->x1, yp+1, ps->x2+1);
								}
							else
								FillArea(p1->x1, yp, ps->x2+1, yp+1);
	
							p1 = NULL;
							}
						else
							p1 = ps;

						ps = ps->pNext;
						}
					}
				}

			// Move on to next row.

			if( ++yp == (fHorz ? m_cx : m_cy) ) {

				break;
				}
			}
		}
	}

// Smooth Polygon

void CGdiGeneric::SmoothPolygon(P2 const *pList1, UINT uCount1, P2 const *pList2, UINT uCount2, DWORD dwRound, PSHADER pShade, int nAlpha)
{
	if( CheckBrush() || pShade ) {

		// Build a list of edges, one for each pair of vertices, including
		// the wrap-around from the last to the first. We might end up with
		// fewer actual edges if any are degenerate. The edge structures
		// contain the diffential constants and accumulators used to figure
		// out the x-wise coverage for each y coordinate. They will be updated
		// during the drawing process, and also added and removed from our
		// active list to determine the pixel coverage to be generated.

		CAutoArray<CEdge> pEdge(uCount1 + uCount2);

		// Initialize the shader and figure out if we're in horz mode,
		// in which case we'll be swapping the x and y coordinates to
		// allow the fill to procede left-to-right.

		UINT uUsed = 0;

		BOOL fHorz = pShade ? (*pShade)(this, 0, 0) : FALSE;

		// Used to track the extremities of the figure.

		int yp = initMin;
		int ye = initMax;
		int xp = initMin;
		int xe = initMax;

		// Add the two sets of edges to our list.

		AddEdges(pEdge, uUsed, fHorz, pList1, uCount1, dwRound, xp, xe, yp, ye);
		
		AddEdges(pEdge, uUsed, fHorz, pList2, uCount2, dwRound, xp, xe, yp, ye);

		// Figure out the where to start in regular coordinates
		// and keep a copy of the initial y position for shading.

		int  yd = yp / sampScale;

		int  yi = yp;

		// Zero out the array used for pixel tracking coverage, and
		// reset the markers for the first and last pixels touched.

		ArrayZero(m_xb, m_cb);

		m_o1 = m_cb;

		m_o2 = 0;

		// Sort the edges by y-coordinate so that we can more
		// efficiently spot which edges are to be added to the 
		// active list by working from the head of the list.

		OrderEdgesByY(pEdge, uUsed);

		// Start with an empty active edge list.

		CEdge *pHead = NULL;

		CEdge *pTail = NULL;

		// Reset the next edge to check and the flag used
		// to track whether the shader has set a null brush
		// such that we don't really need to draw anything.

		UINT uEdge = 0;

		BOOL fDraw = TRUE;

		// Loop around for each oversampled pixel.

		while( yp <= ye ) {

			// If we have a shader, give it the chance to update
			// the current brush, and if it does so, find out if
			// we have a non-null brush and need to draw.

			if( pShade ) {

				if( (*pShade)(this, yp - yi, ye - yi + 1) ) {

					fDraw = CheckBrush();
					}
				}

			// Check to see if we have new active edges.

			while( uEdge < uUsed ) {

				if( pEdge[uEdge].yp == yp ) {

					// Add the new edge to the active list and
					// walk down the list to see if we need to
					// add any more edges.

					CEdge *p = pEdge + uEdge;

					AfxListAppend(pHead, pTail, p, pNext, pPrev);

					uEdge++;

					continue;
					}

				break;
				}

			if( pHead ) {

				// Call the routine to set the x1 and x2 members of
				// each active edge to the pixel coverage, and to run
				// the difference engine to track the edge.

				TrackEdges(pHead);

				// Remove completed edges from the active list.

				for( CEdge *ps = pHead; ps; ) {

					if( ps->ye == yp ) {

						// On last line, only remove horizontal edges
						// as we need the rest to get a nice finish.

						if( yp < ye || ps->et == edgeHorizontal ) {

							AfxListRemove(pHead, pTail, ps, pNext, pPrev);
							}
						}

					ps = ps->pNext;
					}

				if( pHead && fDraw && yd >= 0 ) {

					// Sort the edges left-to-right.
						
					OrderEdgesByX(pHead, pTail);

					// Loop through pairs of edges, alternating
					// between drawing and skipping as we go.

					CEdge *ps = pHead;

					CEdge *p1 = NULL;

					while( ps ) {

						if( p1 ) {

							AccumAlpha(p1->x1, ps->x2);

							p1 = NULL;
							}
						else
							p1 = ps;

						ps = ps->pNext;
						}
					}
				}

			// If we're at the end of a real pixel row, we need to
			// output a row of pixels based on the coverage information
			// we have accumulated. Perform the same operation at the
			// end of the figure to deal with leftover pixels.

			if( yp == ye || yp % sampScale == ((yp < 0) ? -1 : sampScale - 1) ) {

				// Have we generated any coverage?

				if( m_o2 >= m_o1 ) {

					// Apply alpha blending if required. This
					// is pretty expensive and could be improved
					// via a table since m_xb can't be greater
					// than sampLimit.

					if( nAlpha && nAlpha < 256 ) {

						for( int n = m_o1; n <= m_o2; n++ ) {

							m_xb[n] *= nAlpha;
							
							m_xb[n] /= 256;
							}
						}

					// Draw the coverage in the required direction.
				
					if( fHorz ) {

						BlendVert(yd);
						}
					else
						BlendHorz(yd);

					// Reset the coverage accumulators using the
					// coverage limits, and then reset the coverage
					// limits themselves.

					ArrayZero(m_xb + m_o1, m_o2 - m_o1 + 1);

					m_o1 = m_cb;

					m_o2 = 0;
					}

				// Move on to next real row.

				if( ++yd == (fHorz ? m_cx : m_cy) ) {

					break;
					}
				}

			// Move on to next oversampled row.

			yp++;
			}
		}
	}

// Shaded Figures

void CGdiGeneric::ShadeEllipse(int x1, int y1, int x2, int y2, UINT uType, PSHADER pShade)
{
	if( IsValidArea() && CheckBrush() ) {

		PrepEllipse(drawLine, x1, y1, x2, y2, uType);

		if( FALSE && !m_fComplex ) {
			
			if( uType == etWhole ) {

				P2 *pp = (P2 *) alloca(8 * sizeof(P2));

				int pn = MakeEllipse(pp);

				ShadePolygon(pp, pn, 0x00FF00FF, pShade);

				return;
				}
			}

		MakeTrigTable();

		int  da = TrigStep(x2 - x1, y2 - y1);

		int  np = m_ar / da + 2;

		int  pn = 0;

		P2 * pp = (P2 *) alloca(np * sizeof(P2));

		for( int a = 0; a <= m_ar; a += da ) {

			int cx = TrigFactor(Cos(m_a0 - a), m_rx);
			int cy = TrigFactor(Sin(m_a0 - a), m_ry);

			pp[pn].x = (cx >= 0) ? m_xp + cx : m_xn + cx;
			pp[pn].y = (cy <= 0) ? m_yp - cy : m_yn - cy;

			pn++;
			}

		if( uType == etWhole ) {

			pn--;
			}

		if( uType >= etQuad1 && uType <= etQuad4 ) {

			pp[pn].x = m_xp;
			pp[pn].y = m_yp;

			pn++;
			}

		ShadePolygon(pp, pn, 0, pShade);
		}
	}

void CGdiGeneric::ShadeRect(int x1, int y1, int x2, int y2,	PSHADER pShade)
{
	if( IsValidArea() && CheckBrush() ) {

		P2 List[4];

		x2 -= 1;
		y2 -= 1;

		List[0].x = x1; List[0].y = y1;
		List[1].x = x2; List[1].y = y1;
		List[2].x = x2; List[2].y = y2;
		List[3].x = x1; List[3].y = y2;

		ShadePolygon(List, 4, 0, pShade);
		}
	}

void CGdiGeneric::ShadeRounded(int x1, int y1, int x2, int y2, int r, PSHADER pShade)
{
	if( IsValidArea() && CheckBrush() ) {

		if( m_fComplex ) {

			ShadeRect(x1, y1, x2, y2, pShade);
			}
		else {
			P2 List[8];

			DWORD dwRound = MakeRounded(List, x1, y1, x2, y2, r);

			ShadePolygon(List, 8, dwRound, pShade);
			}
		}
	}

void CGdiGeneric::ShadeWedge(int x1, int y1, int x2, int y2, UINT uType, PSHADER pShade)
{
	if( IsValidArea() && CheckBrush() ) {

		P2 List[3];

		if( MakeWedge(List, x1, y1, x2, y2, uType) ) {

			ShadePolygon(List, 3, 0, pShade);
			}
		}
	}

// Rectangle Drawing

void CGdiGeneric::DrawRect(int x1, int y1, int x2, int y2)
{
	if( IsValidArea() && CheckPen() ) {

		if( m_fComplex || (m_Pen.m_Width > 1 && m_Pen.m_Caps) ) {

			x2 -= 1;
			y2 -= 1;

			MoveTo(x1, y1);
			LineTo(x2, y1);
			LineTo(x2, y2);
			LineTo(x1, y2);
			LineTo(x1, y1);
			}
		else {
			Normalize(x1, y1, x2, y2);

			int t0 = m_Pen.m_Width;

			int t1 = t0 - 1;
			
			int t2 = 2 * t1 + 1;

			int cx = x2 - x1 - 1;
						
			int cy = y2 - y1 - 1;

			if( m_fPenStyle ) {

				SetStyleStep(styleBase / t2);

				HorzStrokeFwd(x1-t1, y1-t1, cx, t2);
				VertStrokeFwd(x2-t0, y1-t1, t2, cy);
				HorzStrokeRev(x1+t0, y2-t0, cx, t2);
				VertStrokeRev(x1-t1, y1+t0, t2, cy);
				}
			else {
				LineFill(x1-t1, y1-t1, cx, t2, m_fPenFore);
				LineFill(x2-t0, y1-t1, t2, cy, m_fPenFore);
				LineFill(x1+t0, y2-t0, cx, t2, m_fPenFore);
				LineFill(x1-t1, y1+t0, t2, cy, m_fPenFore);
				}
			}
		}
	}

void CGdiGeneric::FillRect(int x1, int y1, int x2, int y2)
{
	if( IsValidArea() && CheckBrush() ) {

		if( m_fComplex ) {

			ShadeRect(x1, y1, x2, y2, NULL);
			}
		else {
			Normalize(x1, y1, x2, y2);

			FillArea (x1, y1, x2, y2);
			}
		}
	}
	
// Rounded Rectangles

void CGdiGeneric::DrawRounded(int x1, int y1, int x2, int y2, int r)
{
	if( IsValidArea() && CheckPen() ) {

		if( m_fComplex ) {

			PushPen();

			m_Pen.m_Caps = capsEnd;

			DrawRect(x1, y1, x2, y2);

			PullPen();
			}
		else {
			P2 List[8];

			DWORD dwRound = MakeRounded(List, x1, y1, x2, y2, r);

			DrawPolygon(List, 8, dwRound);
			}
		}
	}

void CGdiGeneric::FillRounded(int x1, int y1, int x2, int y2, int r)
{
	ShadeRounded(x1, y1, x2, y2, r, NULL);
	}

// Ellipses

void CGdiGeneric::DrawEllipse(int x1, int y1, int x2, int y2, UINT uType)
{
	if( IsValidArea() && CheckPen() ) {

		if( m_fComplex || m_fPenStyle || m_Pen.m_Width > 1 ) {

			PrepEllipse(drawLine, x1, y1, x2, y2, uType);

			if( uType == etWhole ) {

				if( !m_fPenStyle ) {

					P2 *pp = (P2 *) alloca(8 * sizeof(P2));

					int pn = MakeEllipse(pp);

					DrawPolygon(pp, pn, 0x00FF00FF);

					return;
					}
				}

			MakeTrigTable();

			PushPen();

			UINT Caps = m_Pen.m_Caps;

			int da = TrigStep(x2 - x1, y2 - y1);

			int x0 = 0;
			int y0 = 0;

			InitStyle();

			for( int a = 0; a <= m_ar; a += da ) {

				int cx = TrigFactor(Cos(m_a0 - a), m_rx);
				int cy = TrigFactor(Sin(m_a0 - a), m_ry);

				int xp = (cx >= 0) ? m_xp + cx : m_xn + cx;
				int yp = (cy <= 0) ? m_yp - cy : m_yn - cy;

				Transform(xp, yp);

				if( a ) {

					BOOL fDraw = !(a % 90);

					if( fDraw == FALSE ) {

						int ps = m_Pen.m_Width * 8;

						int dx = Abs(xp - x0);
						
						int dy = Abs(yp - y0);

						if( dx + dy > Max(ps, 1) ) {

							fDraw = TRUE;
							}
						}

					if( fDraw ) {

						if( a == da && uType > etWhole ) {

							m_Pen.m_Caps = (Caps | capsEnd);

							ForcePen();
							}

						if( a == m_ar && uType > etWhole ) {

							m_Pen.m_Caps = (Caps & capsEnd);

							ForcePen();
							}

						if( a == da ) {

							LineHelp(x0, y0, xp, yp);

							m_Pen.m_Caps = capsEnd;

							ForcePen();
							}
						else
							LineHelp(x0, y0, xp, yp);

						x0 = xp;
						y0 = yp;
						}
					}
				else {
					x0 = xp;
					y0 = yp;
					}
				}

			PullPen();
			}
		else {
			Normalize(x1, y1, x2, y2);

			PrepEllipse(drawLine, x1, y1, x2, y2, uType);

			if( uType == etWhole && m_rx == m_ry ) {

				DrawCircle();

				return;
				}

			DrawEllipse();
			}
		}
	}

void CGdiGeneric::FillEllipse(int x1, int y1, int x2, int y2, UINT uType)
{
	if( IsValidArea() && CheckBrush() ) {

		if( m_fComplex ) {

			ShadeEllipse(x1, y1, x2, y2, uType, NULL);
			}
		else {
			Normalize(x1, y1, x2, y2);

			PrepEllipse(drawFill, x1, y1, x2, y2, uType);

			if( uType == etWhole && m_rx == m_ry ) {

				DrawCircle();

				return;
				}

			DrawEllipse();
			}
		}
	}

// Wedges

void CGdiGeneric::DrawWedge(int x1, int y1, int x2, int y2, UINT uType)
{
	if( IsValidArea() && CheckPen() ) {

		P2 List[3];

		if( MakeWedge(List, x1, y1, x2, y2, uType) ) {

			DrawPolygon(List, 3, 0);
			}
		}
	}

void CGdiGeneric::FillWedge(int x1, int y1, int x2, int y2, UINT uType)
{
	ShadeWedge(x1, y1, x2, y2, uType, NULL);
	}

// Line Drawing Helpers

void CGdiGeneric::HorzStrokeFwd(int x, int y, int cx, int cy)
{
	if( m_fPenStyle ) {

		BOOL fLast = TRUE;

		int  nSize = 0;

		for( int n = 0; n < cx; n++ ) {

			if( GetPixelStyle() == fLast ) {

				nSize += 1;
				}
			else {
				LineFill(x, y, nSize, cy, fLast);

				x     = x + nSize;

				nSize = 1;

				fLast = !fLast;
				}
			}

		LineFill(x, y, nSize, cy, fLast);

		return;
		}

	LineFill(x, y, cx, cy, m_fPenFore);
	}

void CGdiGeneric::HorzStrokeRev(int x, int y, int cx, int cy)
{
	if( m_fPenStyle ) {

		x += cx;

		BOOL fLast = TRUE;

		int  nSize = 0;

		for( int n = 0; n < cx; n++ ) {

			if( GetPixelStyle() == fLast ) {

				nSize += 1;
				}
			else {
				LineFill(x -= nSize, y, nSize, cy, fLast);

				nSize = 1;

				fLast = !fLast;
				}
			}

		LineFill(x -= nSize, y, nSize, cy, fLast);

		return;
		}

	LineFill(x, y, cx, cy, m_fPenFore);
	}

void CGdiGeneric::VertStrokeFwd(int x, int y, int cx, int cy)
{
	if( m_fPenStyle ) {

		BOOL fLast = TRUE;

		int  nSize = 0;

		for( int n = 0; n < cy; n++ ) {

			if( GetPixelStyle() == fLast ) {

				nSize += 1;
				}
			else {
				LineFill(x, y, cx, nSize, fLast);

				y     = y + nSize;

				nSize = 1;

				fLast = !fLast;
				}
			}

		LineFill(x, y, cx, nSize, fLast);

		return;
		}

	LineFill(x, y, cx, cy, m_fPenFore);
	}

void CGdiGeneric::VertStrokeRev(int x, int y, int cx, int cy)
{
	if( m_fPenStyle ) {

		y += cy;

		BOOL fLast = TRUE;

		int  nSize = 0;

		for( int n = 0; n < cy; n++ ) {

			if( GetPixelStyle() == fLast ) {

				nSize += 1;
				}
			else {
				LineFill(x, y -= nSize, cx, nSize, fLast);

				nSize = 1;

				fLast = !fLast;
				}
			}

		LineFill(x, y -= nSize, cx, nSize, fLast);

		return;
		}

	LineFill(x, y, cx, cy, m_fPenFore);
	}

void CGdiGeneric::RoundEndCap(int x, int y, int n)
{
	if( n ) {

		int xp = 0;
		int yp = n;

		int t1 = 3;
		int t2 = 5 - 2 * n;
		int d1 = 1 - n;

		do {
			LineCap(x - yp, x + yp, y + xp);
			
			LineCap(x - yp, x + yp, y - xp);

			if( d1 < -1 ) {

				d1 += t1;
				t2 += 2;
				}
			else {
				LineCap(x - xp, x + xp, y + yp);
				
				LineCap(x - xp, x + xp, y - yp);

				d1 += t2;
				t2 += 4;
				yp -= 1;
				}

			xp += 1;
			t1 += 2;

			} while( yp >= xp );
		}
	}

// Line Drawing Implementation

void CGdiGeneric::LineHelp(int x1, int y1, int x2, int y2)
{
	if( IsValidLine() ) {

		if( m_Pen.m_Width > 1 ) {

			if( m_fPenStyle ) {

				LineThick(x1, y1, x2, y2);

				SaveStyle();
				}
			else
				LineThick(x1, y1, x2, y2);
			}
		else
			LineThin(x1, y1, x2, y2);
		}
	}

void CGdiGeneric::LineThin(int x1, int y1, int x2, int y2)
{
	int dx = 1;
	int dy = 1;

	int u  = x2 - x1;
	int v  = y2 - y1;

	if( u < 0 ) { u = -u; dx = -dx; }
	if( v < 0 ) { v = -v; dy = -dy; }

	int ku = u + u;
	int kv = v + v;

	if( u == 0 ) {

		// Vertical Lines

		SetStyleStep(styleBase);

		if( dy > 0 ) {

			VertStrokeFwd(x1, y1, 1, v);
			}
		else
			VertStrokeRev(x1, y1 - v + 1, 1, v);

		return;
		}

	if( v == 0 ) {

		// Horizontal Lines

		SetStyleStep(styleBase);

		if( dx > 0 ) {

			HorzStrokeFwd(x1, y1, u, 1);
			}
		else
			HorzStrokeRev(x1 - u + 1, y1, u, 1);

		return;
		}

	if( u == v ) {

		// 45-degree Lines
	
		if( m_fPenStyle ) {

			if( !IsSimpleStyle() ) {

				SetStyleStep(styleDiag);
				}
			else
				SetStyleStep(styleBase);

			while( u-- ) {

				LineSet(x1, y1, GetPixelStyle());

				x1 += dx;
				y1 += dy;
				}
			}
		else {
			while( u-- ) {

				LineSet(x1, y1, TRUE);

				x1 += dx;
				y1 += dy;
				}
			}

		return;
		}

	if( u >= v ) {

		// X-Major Lines

		if( m_fPenStyle ) {

			if( !IsSimpleStyle() ) {

				SetStyleStep(Hypot(u, v, styleBase) / u);
				}
			else
				SetStyleStep(styleBase);
			}

		HorzMajor(x1, y1, u, v, ku, kv, dx, dy, 0);

		return;
		}

	if( v >= u ) {

		// Y-Major Lines

		if( m_fPenStyle ) {

			if( !IsSimpleStyle() ) {

				SetStyleStep(Hypot(u, v, styleBase) / v);
				}
			else
				SetStyleStep(styleBase);
			}

		VertMajor(x1, y1, u, v, ku, kv, dx, dy, 0);

		return;
		}
	}

void CGdiGeneric::LineThick(int x1, int y1, int x2, int y2)
{
	int dx = 1;
	int dy = 1;

	int u  = x2 - x1;
	int v  = y2 - y1;

	int tp = Hypot(u, v, 1);

	if( u < 0 ) { u = -u; dx = -dx; }
	if( v < 0 ) { v = -v; dy = -dy; }

	int ku = u + u;
	int kv = v + v;

	int th = 2 * (m_Pen.m_Width - 1) + 1;

	int td = (th - 1) / 2;

	int tk = th * tp;

	int d0 = 0;
	int d1 = 0;
	int d2 = 0;
	int dd = 0;

	if( m_Pen.m_Caps & capsStart ) {

		RoundEndCap(x1, y1, td);
		}

	if( m_Pen.m_Caps & capsEnd ) {

		RoundEndCap(x2, y2, td);
		}

	if( u == 0 ) {

		// Vertical Lines

		SetStyleStep(styleBase / th);

		if( dy > 0 ) {

			VertStrokeFwd(x1 - td, y1, th, v);
			}
		else
			VertStrokeRev(x1 - td, y1 - v + 1, th, v);

		return;
		}

	if( v == 0 ) {

		// Horizontal Lines
		
		SetStyleStep(styleBase / th);

		if( dx > 0 ) {

			HorzStrokeFwd(x1, y1 - td, u, th);
			}
		else
			HorzStrokeRev(x1 - u + 1, y1 - td, u, th);

		return;
		}

	if( u >= v ) {

		// X-Major Lines

		SetStyleStep((tp * styleBase) / u / th);

		int x2 = x1;
		int y2 = y1;

		int kd = kv - ku;
		int kt = u  - kv;

		while( dd < tk ) {

			if( !dd ) {

				HorzMajor(x1, y1, u, v, ku, kv, dx, dy, -d1);
				}
			else {
				HorzMajor(x1, y1, u, v, ku, kv, dx, dy, -d1);
				
				HorzMajor(x2, y2, u, v, ku, kv, dx, dy, -d2);
				}

			if( d0 < kt ) {

				y1 += dy;
				y2 -= dy;
				}
			else {
				dd += kv;
				d0 -= ku;

				if( d1 < kt ) {
					   
					x1 -= dx;
					y1 += dy;
					d1 += kv;

					x2 += dx;
					y2 -= dy;
					d2 -= kv;
					}
				else {
					x1 -= dx;
					x2 += dx;
					
					d1 += kd;
					d2 -= kd;

					if( dd >= tk ) break;

					HorzMajor(x1, y1, u, v, ku, kv, dx, dy, -d1);
					
					HorzMajor(x2, y2, u, v, ku, kv, dx, dy, -d2);

					y1 += dy;
					y2 -= dy;
					}
				}

			dd += ku;
			d0 += kv;
			}

		return;
		}

	if( v >= u ) {

		// Y-Major Lines

		SetStyleStep((tp * styleBase) / v / th);

		int x2 = x1;
		int y2 = y1;

		int kd = ku - kv;
		int kt = v  - ku;

		while( dd < tk ) {

			if( !dd ) {

				VertMajor(x1, y1, u, v, ku, kv, dx, dy, -d1);
				}
			else {
				VertMajor(x1, y1, u, v, ku, kv, dx, dy, -d1);
				
				VertMajor(x2, y2, u, v, ku, kv, dx, dy, -d2);
				}

			if( d0 < kt ) {

				x1 += dx;
				x2 -= dx;
				}
			else {
				dd += ku;
				d0 -= kv;

				if( d1 < kt ) {
					   
					x1 += dx;
					y1 -= dy;
					d1 += ku;

					x2 -= dx;
					y2 += dy;
					d2 -= ku;
					}
				else {
					y1 -= dy;
					d1 += kd;

					y2 += dy;
					d2 -= kd;

					if( dd >= tk ) break;

					VertMajor(x1, y1, u, v, ku, kv, dx, dy, -d1);
					
					VertMajor(x2, y2, u, v, ku, kv, dx, dy, -d2);

					x1 += dx;
					x2 -= dx;
					}
				}

			dd += kv;
			d0 += ku;
			}

		return;
		}
	}

void CGdiGeneric::HorzMajor(int x1, int y1, int u, int v, int ku, int kv, int dx, int dy, int d0)
{
	int np = 0;

	int kd = kv - ku;
	int kt = u  - kv;

	if( dx > 0 ) {

		// Left-to-Right

		LoadStyle();

		while( np < u ) {

			int nr = 1;

			while( d0 <= kt && np + nr <= u ) {
				
				d0 += kv;
				nr += 1;
				}

			HorzStrokeFwd(x1, y1, nr, 1);

			x1 += nr;
			y1 += dy;
			d0 += kd;
			np += nr;
			}
		}
	else {
		// Right-to-Left

		LoadStyle();

		while( np < u ) {

			int nr = 1;

			while( d0 <= kt && np + nr <= u ) {
				
				d0 += kv;
				nr += 1;
				}

			HorzStrokeRev(x1 - nr + 1, y1, nr, 1);

			x1 -= nr;
			y1 += dy;
			d0 += kd;
			np += nr;
			}
		}
	}

void CGdiGeneric::VertMajor(int x1, int y1, int u, int v, int ku, int kv, int dx, int dy, int d0)
{
	int np = 0;

	int kd = ku - kv;
	int kt = v  - ku;

	if( dy > 0 ) {

		// Top-to-Bottom
	
		LoadStyle();

		while( np < v ) {

			int nr = 1;

			while( d0 <= kt && np + nr <= v ) {
				
				d0 += ku;
				nr += 1;
				}

			VertStrokeFwd(x1, y1, 1, nr);

			y1 += nr;
			x1 += dx;
			d0 += kd;
			np += nr;
			}
		}
	else {
		// Bottom-to-Top

		LoadStyle();

		while( np < v ) {

			int nr = 1;

			while( d0 <= kt && np + nr <= v ) {
				
				d0 += ku;
				nr += 1;
				}

			VertStrokeRev(x1, y1 - nr + 1, 1, nr);

			y1 -= nr;
			x1 += dx;
			d0 += kd;
			np += nr;
			}
		}
	}

// Line Styling

void CGdiGeneric::InitStyle(void)
{
	m_pi        = 0;

	m_dwPenInit = 1;
	}

void CGdiGeneric::SaveStyle(void)
{
	m_pi        = m_pc;

	m_dwPenInit = m_dwPenTest;
	}

void CGdiGeneric::LoadStyle(void)
{
	m_pc        = m_pi;

	m_dwPenTest = m_dwPenInit;
	}

void CGdiGeneric::SetStyleStep(int ps)
{
	m_ps = ps;

	LoadStyle();
	}

BOOL CGdiGeneric::IsSimpleStyle(void)
{
	return m_dwPenMask == 0x55555555 || m_dwPenMask == 0x11111111;
	}

BOOL CGdiGeneric::GetPixelStyle(void)
{
	m_pc += m_ps;

	while( m_pc >= styleBase ) {

		if( !(m_dwPenTest <<= 1) ) {

			m_dwPenTest = 1;
			}

		m_pc -= styleBase;
		}
	
	return !!(m_dwPenMask & m_dwPenTest);
	}

// Polygon Helpers

void CGdiGeneric::DrawPolygonJoin(P2 const *pList, UINT uCount, DWORD dwRound)
{
	BOOL fPull = FALSE;

	if( m_Pen.m_Caps ) {

		PushPen();

		m_Pen.m_Caps = capsEnd;

		fPull        = TRUE;

		ForcePen();
		}

	InitStyle();

	for( UINT n = 0; n < uCount; n++ ) {

		int x1 = pList[n].x;
		int y1 = pList[n].y;

		int x2 = pList[(n+1) % uCount].x;
		int y2 = pList[(n+1) % uCount].y;

		Transform(x1, y1, x2, y2);

		if( dwRound & (0x00001 << n) ) {

			int sx = Sign(x2 - x1);
			
			int sy = Sign(y2 - y1);

			if( sx == +1 && sy == +1 ) {

				if( dwRound & (0x10000 << n) ) {

					DrawEllipse(x1, y1, x2+1, y2+1, etQuad3);
					}
				else
					DrawEllipse(x1, y1, x2+1, y2+1, etQuad1);

				continue;
				}

			if( sx == +1 && sy == -1 ) {

				if( dwRound & (0x10000 << n) ) {

					DrawEllipse(x1, y2, x2+1, y1+1, etQuad4);
					}
				else
					DrawEllipse(x1, y2, x2+1, y1+1, etQuad2);

				continue;
				}

			if( sx == -1 && sy == -1 ) {

				if( dwRound & (0x10000 << n) ) {

					DrawEllipse(x2, y2, x1+1, y1+1, etQuad1);
					}
				else
					DrawEllipse(x2, y2, x1+1, y1+1, etQuad3);

				continue;
				}

			if( sx == -1 && sy == +1 ) {

				if( dwRound & (0x10000 << n) ) {

					DrawEllipse(x2, y1, x1+1, y2+1, etQuad2);
					}
				else
					DrawEllipse(x2, y1, x1+1, y2+1, etQuad4);

				continue;
				}
			}

		LineHelp(x1, y1, x2, y2);
		}

	if( fPull ) {

		PullPen();
		}
	}

void CGdiGeneric::DrawPolygonScan(P2 const *pList, UINT uCount, DWORD dwRound)
{
	CAutoArray<CEdge> pEdge(uCount);

	UINT uUsed = 0;

	int yp = initMin;
	int ye = initMax;

	for( UINT n = 0; n < uCount; n++ ) {

		int x1 = pList[n].x;
		int y1 = pList[n].y;

		int x2 = pList[(n+1) % uCount].x;
		int y2 = pList[(n+1) % uCount].y;

		Transform(x1, y1, x2, y2);
		
		if( IsValidLine() ) {

			CEdge *p = pEdge + uUsed;
				
			UINT   e = 0;

			if( LOWORD(dwRound) & (1 << n) ) {

				BOOL fTest = !(x2 > x1);

				BOOL fFlip = !(HIWORD(dwRound) & (1 << n));

				if( fFlip ^ fTest ) {

					e = edgeArcAbove;
					}
				else
					e = edgeArcBelow;
				}

			if( y1 <= y2 ) {

				p->xp = x1;
				p->yp = y1;
				p->xe = x2;
				p->ye = y2;
				}
			else {
				p->xp = x2;
				p->yp = y2;
				p->xe = x1;
				p->ye = y1;
				}

			MakeMin(yp, p->yp);
			MakeMax(ye, p->ye);

			SetupEdge(p, e);

			uUsed++;
			}
		}

	OrderEdgesByY(pEdge, uUsed);

	CEdge *pHead = NULL;

	CEdge *pTail = NULL;

	UINT   uEdge = 0;

	while( yp <= ye ) {

		while( uEdge < uUsed ) {

			if( pEdge[uEdge].yp == yp ) {

				CEdge *p = pEdge + uEdge;

				AfxListAppend(pHead, pTail, p, pNext, pPrev);

				uEdge++;

				continue;
				}

			break;
			}

		if( pHead ) {

			TrackEdges(pHead);

			OrderEdgesByX(pHead, pTail);

			CEdge *p0 = NULL;

			CEdge *p1 = pHead;

			while( p1 ) {

				if( yp < p1->ye ) {

					if( p0 ) {

						if( p1->et == edgeHorizontal ) {

							int c0 = p0->x2 + 1 - p0->x1;

							LineFill(p0->x1, yp, c0, 1, m_fPenFore);
							}
						else {
							int c0 = p0->x2 + 1 - p0->x1;
							
							int c1 = p1->x2 + 1 - p1->x1;

							LineFill(p0->x1, yp, c0, 1, m_fPenFore);

							LineFill(p1->x1, yp, c1, 1, m_fPenFore);
							}

						p0 = NULL;
						}
					else
						p0 = p1;
					}
				else {
					int c1 = p1->x2 - p1->x1 + 1;

					LineFill(p1->x1, yp, c1, 1, m_fPenFore);

					AfxListRemove(pHead, pTail, p1, pNext, pPrev);
					}

				p1 = p1->pNext;
				}
			}

		yp += 1;
		}
	}

void CGdiGeneric::DrawPolygonFill(P2 const *pList, UINT uCount, DWORD dwRound)
{
	int pw = m_Pen.m_Width;

	int pa = pw - 1;

	int ps = 2 * pa + 1;

	for( UINT n = 0; n < uCount; n++ ) {

		int m  = (n+1) % uCount;

		int x1 = pList[n].x;
		int y1 = pList[n].y;

		int x2 = pList[m].x;
		int y2 = pList[m].y;

		Transform(x1, y1, x2, y2);

		int dx = x2 - x1;
		int dy = y2 - y1;

		if( dwRound & (0x00001 << n) ) {

			BOOL fTest = ((dx < 0) == (dy < 0));

			BOOL fFlip = (dwRound & (0x10000 << n));

			P2 p[4];

			p[0] = pList[n];
			p[1] = pList[n];
			p[2] = pList[m];
			p[3] = pList[m];

			if( fTest ^ !fFlip ) {

				p[0].x -= pa;
				p[1].x += pa;

				if( fFlip ) {

					p[2].y -= pa;
					p[3].y += pa;
					}
				else {
					p[2].y += pa;
					p[3].y -= pa;
					}
				}
			else {
				p[0].y -= pa;
				p[1].y += pa;

				if( fFlip ) {

					p[2].x += pa;
					p[3].x -= pa;
					}
				else {
					p[2].x -= pa;
					p[3].x += pa;
					}
				}

			DWORD dwWork = 0x0008000A;

			if( fFlip ) {

				dwWork ^= 0xFFFF0000;
				}

			FillPolygon(p, 4, dwWork);
			}
		else {
			if( m_Pen.m_Caps  ) {

				if( !dy ) {

					if( dx > 0 ) {

						LineFill(x1, y1-pa, 0+dx, ps, m_fPenFore);
						}
					else
						LineFill(x2, y1-pa, 1-dx, ps, m_fPenFore);
					}

				if( !dx ) {

					if( dy > 0 ) {

						LineFill(x1-pa, y1, ps, 0+dy, m_fPenFore);
						}			      
					else
						LineFill(x1-pa, y2, ps, 1-dy, m_fPenFore);
					}
				}
			else {
				if( !dy ) {

					if( dx > 0 ) {

						LineFill(x1-pa, y1-pa, pw+pa+dx, ps, m_fPenFore);
						}
					else
						LineFill(x2-pa, y1-pa, pw+pa-dx, ps, m_fPenFore);
					}

				if( !dx ) {

					if( dy > 0 ) {

						LineFill(x1-pa, y1-pa, ps, pw+pa+dy, m_fPenFore);
						}			      
					else
						LineFill(x1-pa, y2-pa, ps, pw+pa-dy, m_fPenFore);
					}
				}
			}

		if( m_Pen.m_Caps  ) {

			RoundEndCap(x1, y1, pa);
			}
		}
	}

// Edge Management

STRONG_INLINE void CGdiGeneric::AddEdges(CEdge *pEdge, UINT &uUsed, BOOL fHorz, P2 const *pList, UINT uCount, DWORD dwRound, int &xp, int &xe, int &yp, int &ye)
{
	for( UINT n = 0; n < uCount; n++ ) {

		// Figure out the start and end points.

		int x1 = pList[n].x;
		int y1 = pList[n].y;

		int x2 = pList[(n+1) % uCount].x;
		int y2 = pList[(n+1) % uCount].y;

		if( fHorz ) {

			Swap(x1, y1);
			Swap(x2, y2);
			}

		if( IsValidLine() ) {

			// Apply the current transformation.

			Transform(x1, y1, x2, y2);

			// Figure out the edge orientation if we're tracing an arc.
			
			CEdge *p = pEdge + uUsed;

			UINT   e = 0;
					
			if( dwRound & (0x00001 << n) ) {

				BOOL fTest = !(x2 > x1);

				BOOL fFlip = !(dwRound & (0x10000 << n));

				if( fFlip ^ fTest ) {

					e = edgeArcAbove;
					}
				else
					e = edgeArcBelow;
				}

			// Normalize the edge endpoints by y-coordinate so we are
			// always increasing in value as we go from yp to ye. Note
			// that the x-coordinate will not always be normalized
			// and that xp might be greater than xe.

			if( y1 <= y2 ) {

				p->xp = x1;
				p->yp = y1;
				p->xe = x2;
				p->ye = y2;
				}
			else {
				p->xp = x2;
				p->yp = y2;
				p->xe = x1;
				p->ye = y1;
				}

			// Accumulate the extremities. Again, note that we
			// have to consider both x-coordinates but only one
			// of the y-coordinates as they are normalized.

			MakeMin(xp, Min(p->xe, p->xp));
			MakeMax(xe, Max(p->xe, p->xp));

			MakeMin(yp, p->yp);
			MakeMax(ye, p->ye);

			// Setup the differential engine for the edge.

			SetupEdge(p, e);

			uUsed++;
			}
		}
	}

STRONG_INLINE void CGdiGeneric::OrderEdgesByX(CEdge * &pHead, CEdge * &pTail)
{
	for(;;) {

		BOOL   fDone = TRUE;

		CEdge *pEdge = pHead;

		while( pEdge->pNext ) {

			if( CompareX(pEdge, pEdge->pNext) > 0 ) {

				CEdge *n = pEdge->pNext;

				AfxListRemove( pHead, pTail,
					       pEdge,
					       pNext, pPrev
					       );

				AfxListInsert( pHead, pTail,
					       pEdge,
					       pNext, pPrev,
					       pEdge->pNext->pNext
					       );

				fDone = FALSE;

				pEdge = n;
				}
			else
				pEdge = pEdge->pNext;
			}

		if( fDone ) {

			break;
			}
		}
	}

void CGdiGeneric::OrderEdgesByY(CEdge *pList, UINT uCount)
{
	qsort(pList, uCount, sizeof(CEdge), (INT(*)(PCVOID, PCVOID)) CompareY);

	for( UINT n = 0; n < uCount; n++ ) pList[n].en = n;
	}

void CGdiGeneric::SetupEdge(CEdge *p, UINT uRound)
{
	p->dx = 1;
	p->dy = 1;

	p->u = p->xe - p->xp;
	p->v = p->ye - p->yp;

	if( p->u < 0 ) { p->u = -p->u; p->dx = -p->dx; }
	if( p->v < 0 ) { p->v = -p->v; p->dy = -p->dy; }

	if( !p->u || !p->v ) {

		if( p->v ) {

			p->x1 = p->xp;
			p->x2 = p->xp;

			p->et = edgeVertical;
			}
		else
			p->et = edgeHorizontal;
		}
	else {
		if( uRound ) {

			int u = p->u;
			int v = p->v;

			if( uRound == edgeArcBelow ) {

				Swap(u, v);
				}

			p->t1 = v     * v;
			p->t2 = p->t1 * 2;
			p->t3 = p->t2 * 2;
			p->t4 = u     * u;
			p->t5 = p->t4 * 2;
			p->t6 = p->t5 * 2;
			p->t7 = p->t5 * v;
			p->t8 = p->t7 * 2;
			p->t9 = 0;

			p->d0 = 0;
			p->d1 = p->t2 - p->t7 + p->t4 / 2;
			p->d2 = p->t1 / 2 - p->t8 + p->t5;
			p->np = 0;

			p->et = uRound;
			}
		else {
			if( p->u == p->v ) {

				p->et = edgeDiagonal;
				}
			else {
				p->ku = p->u + p->u;
				p->kv = p->v + p->v;

				if( p->u > p->v ) {

					p->kd = p->kv - p->ku;
					p->kt = p->u  - p->kv;

					p->et = edgeHorzMajor;
					}
				else {
					p->kd = p->ku - p->kv;
					p->kt = p->v  - p->ku;

					p->et = edgeVertMajor;
					}

				p->np = 0;
				p->d0 = 0;
				}
			}
		}

	p->pNext = NULL;
	p->pPrev = NULL;
	}

STRONG_INLINE void CGdiGeneric::TrackEdges(CEdge *p)
{
	while( p ) {

		TrackEdge(p);

		p = p->pNext;
		}
	}

STRONG_INLINE void CGdiGeneric::TrackEdge(CEdge *p)
{
	if( p->et == edgeVertical ) {

		return;
		}

	if( p->et == edgeHorizontal ) {

		if( p->dx > 0 ) {

			p->x1 = p->xp;
			p->x2 = p->xe;
			}
		else {
			p->x1 = p->xe;
			p->x2 = p->xp;
			}

		return;
		}

	if( p->et == edgeDiagonal ) {

		p->x1 = p->xp;
		p->x2 = p->xp;

		p->xp = p->xp + p->dx;

		return;
		}

	if( p->et == edgeHorzMajor ) {

		int xp = p->xp;

		while( p->d0 <= p->kt && p->np < p->u ) {
			
			p->d0 += p->kv;
			p->xp += p->dx;
			p->np += 1;
			}

		p->d0 += p->kd;
		p->xp += p->dx;
		p->np += 1;

		if( p->dx > 0 ) {

			p->x1 = xp;
			p->x2 = p->xp - 1;
			}
		else {
			p->x1 = p->xp + 1;
			p->x2 = xp;
			}

		return;
		}

	if( p->et == edgeVertMajor ) {

		p->x1 = p->xp;
		p->x2 = p->xp;

		if( p->d0 > p->kt ) {

			p->xp += p->dx;
			p->d0 += p->kd;
			}
		else
			p->d0 += p->ku;

		return;
		}

	if( p->et == edgeArcAbove ) {

		if( !p->d0 && p->d2 >= 0 ) {

			p->d0 = 1;
			}

		if( !p->d0 ) {

			int xp = p->xp;

			p->xp += p->dx;
			p->t9 += p->t3;

			while( p->d1 < 0 && p->d2 < 0 ) {

				p->xp += p->dx;
				p->t9 += p->t3;
				p->d1 += p->t9 + p->t2;
				p->d2 += p->t9;
				}

			if( p->d2 < 0 ) {

				p->t8 -= p->t6;
				p->d1 += p->t9 + p->t2 - p->t8;
				p->d2 += p->t9 + p->t5 - p->t8;
				}
			else
				p->d0 = 1;

			if( p->dx > 0 ) {

				p->x1 = xp;
				p->x2 = p->xp - 1;
				}
			else {
				p->x1 = p->xp + 1;
				p->x2 = xp;
				}
			}
		else {
			p->x1 = p->xp;
			p->x2 = p->xp;

			p->t8 -= p->t6;
		
			if( p->d2 < 0 ) {
				
				p->xp += p->dx;
				p->t9 += p->t3;
				p->d2 += p->t9 + p->t5 - p->t8;
				}
			else
				p->d2 += p->t5 - p->t8;
			}
		}

	if( p->et == edgeArcBelow ) {

		if( !p->d0 && p->d2 >= 0 ) {

			p->d0 = 1;
			}

		if( !p->d0 ) {

			p->x1 = p->xp;
			p->x2 = p->xp;

			p->t9 += p->t3;
		
			if( p->d1 < 0 ) {

				p->d1 += p->t9 + p->t2;
				p->d2 += p->t9;
				}
			else {
				p->xp += p->dx;
				p->np += 1;
				p->t8 -= p->t6;
				p->d1 += p->t9 + p->t2 - p->t8;
				p->d2 += p->t9 + p->t5 - p->t8;
				}

			if( p->d2 >= 0 ) {

				p->d0 = 1;
				}
			}
		else {
			int xp = p->xp;
		
			p->xp += p->dx;
			p->np += 1;
			p->t8 -= p->t6;

			while( p->d2 >= 0 && p->np <= p->u ) {

				p->d2 += p->t5 - p->t8;
				p->xp += p->dx;
				p->np += 1;
				p->t8 -= p->t6;
				}

			p->t9 += p->t3;
			p->d2 += p->t9 + p->t5 - p->t8;

			if( p->dx > 0 ) {

				p->x1 = xp;
				p->x2 = p->xp - 1;
				}
			else {
				p->x1 = p->xp + 1;
				p->x2 = xp;
				}
			}

		return;
		}
	}

// Edge Sorting

int CGdiGeneric::CompareX(CEdge *p1, CEdge *p2)
{
	if( p1->x1 > p2->x1 ) {

		return +1;
		}

	if( p1->x1 < p2->x1 ) {

		return -1;
		}

	if( p1->x2 > p2->x2 ) {

		return +1;
		}

	if( p1->x2 < p2->x2 ) {

		return -1;
		}

	return 0;
	}

int CGdiGeneric::CompareY(CEdge *p1, CEdge *p2)
{
	if( p1->yp > p2->yp ) {

		return +1;
		}

	if( p1->yp < p2->yp ) {

		return -1;
		}

	return 0;
	}

// Alpha Accumulation

STRONG_INLINE BOOL CGdiGeneric::AccumAlpha(int x1, int x2)
{
	MakeMax(x1, 0);

	MakeMax(x2, 0);

	MakeMin(x1, (m_cb << sampBits) - 1);

	MakeMin(x2, (m_cb << sampBits) - 1);

	if( x2 >= x1 ) {

		int nb = x2 - x1 + 1;

		int pb = x1 >> sampBits;

		MakeMin(m_o1, pb);

		if( x1 & sampMask ) {

			int nr = sampScale - (x1 & sampMask);

			MakeMin(nr, nb);

			m_xb[pb++] += nr;

			nb         -= nr;
			}

		while( nb >= sampScale ) {

			m_xb[pb++] += sampScale;

			nb         -= sampScale;
			}

		if( nb ) {

			m_xb[pb++] += nb;
			}

		MakeMax(m_o2, pb-1);

		return TRUE;
		}

	return FALSE;
	}

// Rounded Rect Building

DWORD CGdiGeneric::MakeRounded(P2 *p, int x1, int y1, int x2, int y2, int r)
{
	x2 -= 1;
	y2 -= 1;

	p[0].x = x1 + r; p[0].y = y1    ;
	p[1].x = x2 - r; p[1].y = y1    ;
	p[2].x = x2    ; p[2].y = y1 + r;
	p[3].x = x2    ; p[3].y = y2 - r;
	p[4].x = x2 - r; p[4].y = y2    ;
	p[5].x = x1 + r; p[5].y = y2    ;
	p[6].x = x1    ; p[6].y = y2 - r;
	p[7].x = x1    ; p[7].y = y1 + r;

	return 0xAA;
	}

// Wedge Building

BOOL CGdiGeneric::MakeWedge(P2 *p, int x1, int y1, int x2, int y2, UINT uType)
{
	switch( uType ) {

		case etQuad1:

			p[0].x = x1;     p[0].y = y1;
			p[1].x = x2 - 1; p[1].y = y1;
			p[2].x = x2 - 1; p[2].y = y2 - 1;

			return TRUE;

		case etQuad2:

			p[0].x = x1;     p[0].y = y1;
			p[1].x = x1;     p[1].y = y2 - 1;
			p[2].x = x2 - 1; p[2].y = y1;

			return TRUE;

		case etQuad3:

			p[0].x = x1;     p[0].y = y1;
			p[1].x = x1;     p[1].y = y2 - 1;
			p[2].x = x2 - 1; p[2].y = y2 - 1;

			return TRUE;

		case etQuad4:

			p[0].x = x2 - 1; p[0].y = y1;
			p[1].x = x1;     p[1].y = y2 - 1;
			p[2].x = x2 - 1; p[2].y = y2 - 1;

			return TRUE;
		}

	return FALSE;
	}

// Ellipse Helpers

void CGdiGeneric::PrepEllipse(UINT uMode, int x1, int y1, int x2, int y2, UINT uType)
{
	m_dm = uMode;

	switch( uType ) {

		case etWhole:
			m_dm |= drawQuad1 | drawQuad2 | drawQuad3 | drawQuad4;
			break;

		case etHalf1:
			m_dm |= drawQuad1 | drawQuad4;
			break;

		case etHalf2:
			m_dm |= drawQuad1 | drawQuad2;
			break;

		case etHalf3:
			m_dm |= drawQuad2 | drawQuad3;
			break;

		case etHalf4:
			m_dm |= drawQuad3 | drawQuad4;
			break;

		case etQuad1:
			m_dm |= drawQuad1;
			break;

		case etQuad2:
			m_dm |= drawQuad2;
			break;

		case etQuad3:
			m_dm |= drawQuad3;
			break;

		case etQuad4:
			m_dm |= drawQuad4;
			break;
		}

	switch( uType ) {

		case etWhole:
			m_a0 = 0;
			m_ar = 360;
			break;

		case etHalf1:
			m_a0 = 90;
			m_ar = 180;
			break;

		case etHalf2:
			m_a0 = 180;
			m_ar = 180;
			break;

		case etHalf3:
			m_a0 = 270;
			m_ar = 180;
			break;

		case etHalf4:
			m_a0 =   0;
			m_ar = 180;
			break;

		case etQuad1:
			m_a0 = 90;
			m_ar = 90;
			break;

		case etQuad2:
			m_a0 = 180;
			m_ar = 90;
			break;
		
		case etQuad3:
			m_a0 = 270;
			m_ar = 90;
			break;
		
		case etQuad4:
			m_a0 = 0;
			m_ar = 90;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf2:
		case etHalf4:
			m_xn = (x1 + x2) / 2;
			break;

		case etHalf1:
		case etQuad1:
		case etQuad4:
			m_xn = x1;
			break;

		case etHalf3:
		case etQuad2:
		case etQuad3:
			m_xn = x2 - 1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf1:
		case etHalf3:
			m_yn = (y1 + y2) / 2;
			break;

		case etHalf2:
		case etQuad1:
		case etQuad2:
			m_yn = y2 - 1;
			break;

		case etHalf4:
		case etQuad3:
		case etQuad4:
			m_yn = y1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf2:
		case etHalf4:
			m_xp = m_xn - 1 + (x1 + x2) % 2;
			break;

		case etHalf1:
		case etQuad1:
		case etQuad4:
			m_xp = x1;
			break;

		case etHalf3:
		case etQuad2:
		case etQuad3:
			m_xp = x2 - 1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf1:
		case etHalf3:
			m_yp = m_yn - 1 + (y1 + y2) % 2;
			break;

		case etHalf2:
		case etQuad1:
		case etQuad2:
			m_yp = y2 - 1;
			break;

		case etHalf4:
		case etQuad3:
		case etQuad4:
			m_yp = y1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf2:
		case etHalf4:
			m_rx = (x2 - x1) / 2;
			break;

		case etHalf1:
		case etHalf3:
		case etQuad1:
		case etQuad2:
		case etQuad3:
		case etQuad4:
			m_rx = (x2 - x1) - 1;
			break;
		}

	switch( uType ) {

		case etWhole:
		case etHalf1:
		case etHalf3:
			m_ry = (y2 - y1) / 2;
			break;

		case etHalf2:
		case etHalf4:
		case etQuad1:
		case etQuad2:
		case etQuad3:
		case etQuad4:
			m_ry = (y2 - y1) - 1;
			break;
		}
	}

int CGdiGeneric::MakeEllipse(P2 *pp)
{
	int pn = 0;

	pp[pn  ].x = m_xp + m_rx;
	pp[pn++].y = m_yp;

	if( m_yp != m_yn ) {
		
		pp[pn  ].x = m_xp + m_rx;
		pp[pn++].y = m_yp;
		}

	pp[pn  ].x = m_xp;
	pp[pn++].y = m_yn - m_ry;

	if( m_xp != m_xn ) {
		
		pp[pn  ].x = m_xn;
		pp[pn++].y = m_yn - m_ry;
		}

	pp[pn  ].x = m_xn - m_rx;
	pp[pn++].y = m_yn;

	if( m_yp != m_yn ) {
		
		pp[pn  ].x = m_xn - m_rx;
		pp[pn++].y = m_yp;
		}

	pp[pn  ].x = m_xn;
	pp[pn++].y = m_yp + m_ry;

	if( m_xp != m_xn ) {
		
		pp[pn  ].x = m_xp;
		pp[pn++].y = m_yp + m_ry;
		}

	return pn;
	}

void CGdiGeneric::DrawCircle(void)
{
	int t1 = 3;
	int t2 = 5 - 2 * m_rx;
	int d1 = 1 - m_rx;

	int xp = 0;
	int yp = m_rx;

	do {
		PlotCircle(m_xn - xp, m_yp + yp);
		PlotCircle(m_xn - xp, m_yn - yp);
		PlotCircle(m_xn - yp, m_yp + xp);
		PlotCircle(m_xn - yp, m_yn - xp);

		if( !(m_dm & drawFill) ) {
		
			PlotCircle(m_xp + xp, m_yp + yp);
			PlotCircle(m_xp + xp, m_yn - yp);
			PlotCircle(m_xp + yp, m_yp + xp);
			PlotCircle(m_xp + yp, m_yn - xp);
			}

		if( d1 < -1 ) {
			
			d1 += t1;
			t2 += 2;
			}
		else {
			d1 += t2;
			t2 += 4;
			yp -= 1;
			}

		xp += 1;
		t1 += 2;

		} while( yp >= xp );
	}

void CGdiGeneric::PlotCircle(int x, int y)
{
	if( m_dm & drawFill ) {

		int cx = m_xn - 2 * x + m_xp;

		PlotEllipse(x, y, cx);

		return;
		}

	LineSet(x, y, m_fPenFore);
	}

void CGdiGeneric::DrawEllipse(void)
{
	int t1 = m_rx * m_rx;
	int t2 = t1   * 2;
	int t3 = t2   * 2;
	int t4 = m_ry * m_ry;
	int t5 = t4   * 2;
	int t6 = t5   * 2;
	int t7 = m_rx * t5;
	int t8 = t7   * 2;
	int t9 = 0;

	int d1 = t2 - t7 + t4 / 2;
	int d2 = t1 / 2 - t8 + t5;

	int xp = m_rx;
	int yp = 0;

	m_dy = m_ry;
	
	while( d2 < 0 ) {

		PlotEllipse(xp, yp);
	
		yp += 1;
		t9 += t3;
		
		if( d1 < 0 ) {

			d1 += t9 + t2;
			d2 += t9;
			}
		else {
			xp -= 1;
			t8 -= t6;
			d1 += t9 + t2 - t8;
			d2 += t9 + t5 - t8;
			}
		}

	do {
		PlotEllipse(xp, yp);

		xp -= 1;
		t8 -= t6;
		
		if( d2 < 0 ) {
			
			yp += 1;
			t9 += t3;
			d2 += t9 + t5 - t8;
			}
		else
			d2 += t5 - t8;

		} while( xp >= 0 );
	}

void CGdiGeneric::PlotEllipse(int x, int y)
{
	if( m_dm & drawFill ) {

		if( y - m_dy ) {

			if( (m_dm & drawQuad1) && (m_dm & drawQuad2) ) {

				PlotEllipse(m_xn - x, m_yn - y, 2 * x + m_xp - m_xn + 1);
				}
			else {
				if( m_dm & drawQuad1 ) {

					PlotEllipse(m_xp, m_yn - y, x + 1);
					}

				if( m_dm & drawQuad2 ) {

					PlotEllipse(m_xn - x, m_yn - y, x + 1);
					}
				}

			if( (m_dm & drawQuad3) && (m_dm & drawQuad4) ) {

				PlotEllipse(m_xn - x, m_yp + y, 2 * x + m_xp - m_xn + 1);
				}
			else {
				if( m_dm & drawQuad3 ) {

					PlotEllipse(m_xn - x, m_yp + y, x + 1);
					}

				if( m_dm & drawQuad4 ) {

					PlotEllipse(m_xp, m_yp + y, x + 1);
					}
				}

			m_dy = y;
			}

		return;
		}

	if( m_dm & drawQuad1 ) {
		
		LineSet(m_xp + x, m_yn - y, m_fPenFore);
		}

	if( m_dm & drawQuad2 ) {
		
		LineSet(m_xn - x, m_yn - y, m_fPenFore);
		}

	if( m_dm & drawQuad3 ) {
		
		LineSet(m_xn - x, m_yp + y, m_fPenFore);
		}

	if( m_dm & drawQuad4 ) {
		
		LineSet(m_xp + x, m_yp + y, m_fPenFore);
		}
	}

void CGdiGeneric::PlotEllipse(int x, int y, int cx)
{
	FillArea(x, y, x + cx, y + 1);
	}

// Tool Management

void CGdiGeneric::DirtyBoth(void)
{
	DirtyBrush();

	DirtyPen();
	}

void CGdiGeneric::DirtyFont(void)
{
	m_fFontDirty = TRUE;
	}

void CGdiGeneric::DirtyBrush(void)
{
	m_fBrushDirty = TRUE;
	}

void CGdiGeneric::DirtyPen(void)
{
	m_fPenDirty = TRUE;
	}

void CGdiGeneric::CheckBoth(void)
{
	CheckBrush();

	CheckPen();
	}

void CGdiGeneric::CheckFont(void)
{
	if( m_fFontDirty ) {

		if( memcmp(&m_Font, &m_OldFont, sizeof(CLogFont)) ) {

			ForceFont();
			}
		else
			m_fFontDirty = FALSE;
		}
	}

BOOL CGdiGeneric::CheckBrush(void)
{
	if( m_fBrushDirty ) {

		if( memcmp(&m_Brush, &m_OldBrush, sizeof(CLogBrush)) ) {

			ForceBrush();
			}
		else
			m_fBrushDirty = FALSE;
		}

	return m_uBrushMode != brushNull;
	}

BOOL CGdiGeneric::CheckPen(void)
{
	if( m_fPenDirty ) {

		if( memcmp(&m_Pen, &m_OldPen, sizeof(CLogPen)) ) {

			ForcePen();
			}
		else
			m_fPenDirty = FALSE;
		}

	return !m_fPenNull;
	}

void CGdiGeneric::ForceBoth(void)
{
	ForceBrush();

	ForcePen();
	}

void CGdiGeneric::ForceFont(void)
{
	if( m_Font.m_pFont ) {

		m_fFontProp  = m_Font.m_pFont->IsProportional();

		m_cxFont     = m_Font.m_pFont->GetGlyphWidth(0);

		m_cyFont     = m_Font.m_pFont->GetGlyphHeight(0);

		OnNewFont();
		}

	m_OldFont    = m_Font;

	m_fFontDirty = FALSE;
	}

void CGdiGeneric::ForceBrush(void)
{
	switch( m_Brush.m_Style ) {

		case brushNull:
			m_uBrushMode = fillNull;
			break;

		case brushFore:
		case brushBack:
			m_uBrushMode = fillSolid;
			break;

		case brushBitmap:
			m_uBrushMode = fillBitmap;
			break;

		default:
			m_uBrushMode = fillPattern;
			break;
		}

	OnNewBrush();

	m_OldBrush    = m_Brush;

	m_fBrushDirty = FALSE;
	}

void CGdiGeneric::ForcePen(void)
{
	if( m_Pen.m_Style ) {

		static DWORD const dwPenData[] = {

			0x00000000,	// penBlack
			0xFFFFFFFF,	// penWhite
			0x55555555,	// penGray
			0x11111111,	// penDotted
			0x3F3F3F3F,	// penDashed
			0x27272727,	// penDotDash
			
			};

		m_dwPenMask = dwPenData[m_Pen.m_Style - 1];

		m_fPenStyle = (m_Pen.m_Style >= penGray);

		m_fPenFore  = (m_Pen.m_Style != penBack);

		m_fPenNull  = FALSE;

		OnNewPen();
		}
	else
		m_fPenNull  = TRUE;

	m_OldPen    = m_Pen;

	m_fPenDirty = FALSE;
	}

// Transformation

BOOL CGdiGeneric::IsComplex(void)
{
	if( Abs(m_M.e[0][0]) == m_M.e[2][2] ) {

		if( Abs(m_M.e[1][1]) == m_M.e[2][2] ) {

			if( m_M.e[0][1] == 0 && m_M.e[1][0] == 0 ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CGdiGeneric::Normalize(int &x1, int &y1, int &x2, int &y2)
{
	if( m_fTransform ) {

		Transform(x1, y1);

		Transform(x2, y2);

		if( x1 > x2 ) {

			Swap(x1, x2);
			}

		if( y1 > y2 ) {

			Swap(y1, y2);
			}

		return TRUE;
		}

	return FALSE;
	}

void CGdiGeneric::Transform(int &x1, int &y1, int &x2, int &y2)
{
	Transform(x1, y1);

	Transform(x2, y2);
	}

void CGdiGeneric::Transform(int &x1, int &y1)
{
	if( m_fTransform ) {

		int k1 = 1;

		int nx = x1 * m_M.e[0][0] + y1 * m_M.e[0][1] + k1 * m_M.e[0][2];
		int ny = x1 * m_M.e[1][0] + y1 * m_M.e[1][1] + k1 * m_M.e[1][2];
		int nk = x1 * m_M.e[2][0] + y1 * m_M.e[2][1] + k1 * m_M.e[2][2];

		x1 = nx / nk;
		y1 = ny / nk;
		}
	}

// Font Library

BOOL CGdiGeneric::MakeStdFonts(void)
{
	extern IGdiFont * Create_StdFont(UINT uFont);

	extern IGdiFont * Create_BigFont(int  ySize);

	extern IGdiFont * Create_HeiFont(int  ySize);

	extern IGdiFont * Create_HeiBold(int  ySize);

	UINT n;

	for( n = 0; n < fontHei16; n++ ) {

		m_pStdFont[n] = Create_StdFont(n);
		}

	m_pStdFont[n++] = Create_HeiFont(16);

	m_pStdFont[n++] = Create_HeiBold(16);

	m_pStdFont[n++] = Create_HeiFont(10);

	m_pStdFont[n++] = Create_HeiBold(10);

	m_pStdFont[n++] = Create_HeiFont(12);

	m_pStdFont[n++] = Create_HeiBold(12);

	m_pStdFont[n++] = Create_HeiFont(14);

	m_pStdFont[n++] = Create_HeiBold(14);

	m_pStdFont[n++] = Create_HeiFont(18);

	m_pStdFont[n++] = Create_HeiBold(18);

	m_pStdFont[n++] = Create_HeiFont(20);

	m_pStdFont[n++] = Create_HeiBold(20);

	m_pStdFont[n++] = Create_HeiFont(24);

	m_pStdFont[n++] = Create_HeiBold(24);

	m_pStdFont[n++] = Create_BigFont(24);

	m_pStdFont[n++] = Create_BigFont(32);
	
	m_pStdFont[n++] = Create_BigFont(64);
	
	m_pStdFont[n++] = Create_BigFont(96);

	m_pDefFont      = m_pStdFont[fontHei16];

	return TRUE;
	}

BOOL CGdiGeneric::FreeStdFonts(void)
{
	for( UINT n = 0; n < elements(m_pStdFont); n++ ) {
		
		if( m_pStdFont[n] ) {

			m_pStdFont[n]->Release();
			}
		}

	return TRUE;
	}

// Trig Table

BOOL CGdiGeneric::MakeTrigTable(void)
{
	if( !m_pTrig ) {

		m_pTrig      = New int [ trigSize + 1];

		double pi    = acos(double(-1));

		int    theta = 0;

		for( int n = 0; n <= trigSize; n++ ) {

			m_pTrig[n] = int(trigScale * cos(theta * pi / 180));

			theta      = theta + trigStep;
			}
		}

	return TRUE;
	}

BOOL CGdiGeneric::FreeTrigTable(void)
{
	if( m_pTrig ) {

		delete [] m_pTrig;
		}

	return TRUE;
	}

// Math Helpers

int CGdiGeneric::TrigStep(int cx, int cy)
{
	return 6;
	}

int CGdiGeneric::TrigFactor(int r, int f)
{
	return (r * f + (trigScale >> 1)) >> trigBits;
	}

int CGdiGeneric::Cos(int d)
{
	while( d <  0   ) d += 360;

	while( d >= 360 ) d -= 360;

	int n = (d + trigStep / 2) / trigStep;
	
	if( d <  90 ) return  m_pTrig[n];

	if( d < 180 ) return -m_pTrig[2 * trigSize - n];

	if( d < 270 ) return -m_pTrig[n - 2 * trigSize];
	
	return m_pTrig[4 * trigSize - n];
	}

int CGdiGeneric::Sin(int d)
{
	return Cos(d - 90);
	}

int CGdiGeneric::Hypot(int u, int v, int n)
{
	int k1 = u*u + v*v;

	int k2 = n*n;

	int mi = 1 << 28;

	if( k1 < mi / k2 ) {

		return FastRoot(k1 * k2);
		}

	return int(sqrt(double(k1)) * n);
	}

int CGdiGeneric::FastRoot(int n)
{
	if( n > 1 ) {

		int a = 4 * n;

		int g = n;

		while( g > 30000 ) {
			
			g = g / 2;
			}
		
		for( int i = 0; i < 15; i++ ) {
		
			int e = (g * g - a) / (2 * g);

			if( !e ) break;
					
			g = g - e;
			}

		return (g + 1) / 2;
		}

	if( n > 0 ) {

		return 1;
		}

	return 0;
	}

int CGdiGeneric::Abs(int x)
{
	return (x<0)?(-x):(+x);
	}

int CGdiGeneric::Sign(int x)
{
	return (x<0)?-1:((x>0)?+1:0);
	}

void CGdiGeneric::Swap(int &a, int &b)
{
	int c;

	c = a;
	a = b;
	b = c;
	}

// End of File
