
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_Gdi(void);

extern void Register_Region(void);

extern void Register_TouchMap(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxGraph(void)
{
	Register_Gdi();
	
	Register_Region();
	
	Register_TouchMap();
	}

void Revoke_HxGraph(void)
{
	piob->RevokeGroup("graph.");
	}

// End of File
