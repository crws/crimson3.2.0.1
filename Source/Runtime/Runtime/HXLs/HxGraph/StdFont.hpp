
// This file does not include intern.hpp
//
// See the notes in the font data files!
//

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_StdFont_HPP
	
#define	INCLUDE_StdFont_HPP

//////////////////////////////////////////////////////////////////////////
//
// Standard Font Object
//

class CStdFont : public IGdiFont
{
	public:
		// Constructor
		CStdFont(UINT uFont);

		// Destructor
		~CStdFont(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Attributes
		BOOL IsProportional(void);
		int  GetBaseLine   (void);
		BOOL GlyphAvailable(WORD c);
		int  GetGlyphWidth (WORD c);
		int  GetGlyphHeight(WORD c);

		// Operations
		void InitBurst(IGdi *pGdi, CLogFont const &Font);
		void DrawGlyph(IGdi *pGdi, int &x, int &y, WORD c);
		void BurstDone(IGdi *pGdi, CLogFont const &Font);

	protected:
		// Font Structure
		struct FONT
		{
			WORD	m_CharY;
			WORD	m_FontX;
			WORD	m_CharX[256];
			WORD	m_CharP[256];
			BYTE	m_bData[1];
			};

		// Data Members
		FONT  * m_pFont;
		UINT    m_uFont;
		UINT    m_uStride;
		BOOL    m_fFill;
		int     m_xSpace;
		int     m_xDigit;
		int     m_ySize;

		// Font Data
		static BYTE const m_bFont1[];
		static BYTE const m_bFont2[];
		static BYTE const m_bFont3[];
		static BYTE const m_bFont4[];
		static BYTE const m_bFont5[];
		static BYTE const m_bFont6[];
		static BYTE const m_bFont7[];
		static BYTE const m_bFont8[];

		// Implementation
		void ValidateChar(WORD &c);
		BOOL IsFixedDigit(WORD c);
		BOOL IsSpace(WORD c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Font Attributes
//

#ifdef  __GNUC__

#define FONTDATA const __attribute__((aligned(16)))

#else

#define	FONTDATA const

#endif

// End of File

#endif
