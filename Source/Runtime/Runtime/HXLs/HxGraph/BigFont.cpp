
#include "Intern.hpp"

#include "BigFont.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GDI Version 2.0
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Big Numeric Font
//

// Instantiator

IGdiFont * Create_BigFont(int ySize)
{
	return New CBigFont(ySize);
	}

// Constructor

CBigFont::CBigFont(int ySize)
{
	switch( (m_ySize = ySize) ) {

		case 24:
			m_pData  = m_bFontData0;
			m_pCode  = m_wFontCode0;
			m_pSize  = m_xFontSize0;
			m_pFind  = m_wFontFind0;
			m_uCount = m_uCount0;
			m_xSpace = 5;
			break;

		case 32:
			m_pData  = m_bFontData1;
			m_pCode  = m_wFontCode1;
			m_pSize  = m_xFontSize1;
			m_pFind  = m_wFontFind1;
			m_uCount = m_uCount1;
			m_xSpace = 3;
			break;

		case 64:
			m_pData  = m_bFontData2;
			m_pCode  = m_wFontCode2;
			m_pSize  = m_xFontSize2;
			m_pFind  = m_wFontFind2;
			m_uCount = m_uCount2;
			m_xSpace = 4;
			break;

		case 96:
			m_pData  = m_bFontData3;
			m_pCode  = m_wFontCode3;
			m_pSize  = m_xFontSize3;
			m_pFind  = m_wFontFind3;
			m_uCount = m_uCount3;
			m_xSpace = 5;
			break;
		}

	m_xDigit = GetGlyphWidth(L'4');

	m_fFill  = FALSE;
	}

// Destructor

CBigFont::~CBigFont(void)
{
	}

// IUnknown

HRESULT CBigFont::QueryInterface(REFIID riid, void **ppObject)
{
	return E_NOINTERFACE;
	}

ULONG CBigFont::AddRef(void)
{
	return 1;
	}

ULONG CBigFont::Release(void)
{
	delete this;

	return 0;
	}

// Attributes

BOOL CBigFont::IsProportional(void)
{
	return TRUE;
	}

int CBigFont::GetBaseLine(void)
{
	return 4;
	}

int CBigFont::GetGlyphWidth(WORD c)
{
	PVOID p;

	switch( c ) {

		case spaceNormal:
		case spaceNoBreak:

			return m_xSpace;
		
		case spaceNarrow:

			return m_xSpace / 2;

		case spaceHair:

			return 1;

		case spaceFigure:

			return m_xDigit;
		}

	if( IsFixed(c) ) {

		return m_xDigit;
		}

	if( c >= 'a' && c <= 'z' ) {

		c -= 'a';

		c += 'A';

		return GetGlyphWidth(c);
		}

	if( c == 0x0478 ) {

		int x1 = GetGlyphWidth('O');
		
		int x2 = GetGlyphWidth('y');

		return x1 + x2;
		}

	if( c == 0x0479 ) {

		int x1 = GetGlyphWidth('o');
		
		int x2 = GetGlyphWidth('y');

		return x1 + x2;
		}

	if( (p = bsearch(&c, m_pCode, m_uCount, sizeof(WORD), SortFunc)) ) {

		UINT n = PWORD(p) - m_pCode;

		return m_pSize[n];
		}

	return GetGlyphWidth('?');
	}

int CBigFont::GetGlyphHeight(WORD c)
{
	return m_ySize;
	}

// Operations

void CBigFont::InitBurst(IGdi *pGdi, CLogFont const &Font)
{
	if( Font.m_Trans == modeOpaque ) {

		pGdi->PushBrush();

		pGdi->SelectBrush(brushFore);

		pGdi->SetBrushFore(Font.m_Back);

		m_fFill = TRUE;
		}

	m_Fore = Font.m_Fore;

	m_Back = Font.m_Back;
	}

void CBigFont::DrawGlyph(IGdi *pGdi, int &x, int &y, WORD c)
{
	PVOID p;

	if( IsSpace(c) ) {

		int cx = GetGlyphWidth(c);

		if( m_fFill ) {

			pGdi->FillRect(x, y, x+cx, y+m_ySize);
			}

		x += cx;

		return;
		}

	if( IsFixed(c) ) {

		if( IsFixedDigit(c) ) {

			c -= digitFixed;

			c += digitSimple;
			}
		else {
			c -= letterFixed;

			c += letterSimple;
			}

		int cx = GetGlyphWidth(c);

		int x1 = (m_xDigit - cx + 1) / 2;

		int x2 = (m_xDigit - cx + 0) / 2;

		if( c == '1' ) {

			x1 -= 1;

			x2 += 1;
			}

		if( x1 ) {
			
			if( m_fFill ) {

				pGdi->FillRect(x, y, x+x1, y+m_ySize);
				}
	
			x += x1;
			}

		if( TRUE ) {

			DrawGlyph(pGdi, x, y, c);
			}

		if( x2 ) {
			
			if( m_fFill ) {

				pGdi->FillRect(x, y, x+x2, y+m_ySize);
				}

			x += x2;
			}

		return;
		}

	if( c >= 'a' && c <= 'z' ) {

		c -= 'a';

		c += 'A';

		DrawGlyph(pGdi, x, y, c);

		return;
		}

	if( c == 0x0478 ) {

		DrawGlyph(pGdi, x, y, 'O');
	
		DrawGlyph(pGdi, x, y, 'y');

		return;
		}

	if( c == 0x0479 ) {

		DrawGlyph(pGdi, x, y, 'o');
	
		DrawGlyph(pGdi, x, y, 'y');

		return;
		}

	if( (p = bsearch(&c, m_pCode, m_uCount, sizeof(WORD), SortFunc)) ) {

		UINT  n = PWORD(p) - m_pCode;

		int  cx = m_pSize[n];

		int  cy = m_ySize;

		if( m_fFill ) {

			pGdi->CharBlt( x,
				       y,
				       cx,
				       cy,
				       0,
				       PBYTE(m_pData + m_pFind[n]),
				       m_Back,
				       m_Fore
				       );
			}
		else {
			pGdi->CharBlt( x,
				       y,
				       cx,
				       cy,
				       0,
				       PBYTE(m_pData + m_pFind[n]),
				       m_Fore
				       );
			}

		x += cx;

		return;
		}

	DrawGlyph(pGdi, x, y, '?');
	}

void CBigFont::BurstDone(IGdi *pGdi, CLogFont const &Font)
{
	if( m_fFill ) {

		pGdi->PullBrush();

		m_fFill = FALSE;
		}
	}

// Implementation

BOOL CBigFont::IsFixed(WORD c)
{
	return IsFixedDigit(c) || IsFixedLetter(c);
	}

BOOL CBigFont::IsFixedDigit(WORD c)
{
	if( c >= digitFixed && c < digitFixed + 10 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CBigFont::IsFixedLetter(WORD c)
{
	if( c >= letterFixed && c < letterFixed + 6 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CBigFont::IsSpace(WORD c)
{
	switch( c ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;
		}

	return FALSE;
	}

// Sort Function

int CBigFont::SortFunc(PCVOID p1, PCVOID p2)
{
	WORD w1 = PWORD(p1)[0];

	WORD w2 = PWORD(p2)[0];

	if( w1 < w2 ) return -1;

	if( w1 > w2 ) return +1;

	return 0;
	}

// End of File
