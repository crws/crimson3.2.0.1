
#include "Intern.hpp"

#include "Palette.hpp"

#include "GdiGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Palette Access
//

DWORD GetPaletteEntry(UINT uEntry)
{
	#define	RGB(r,g,b) MAKELONG(MAKEWORD(b,g),r)
	
	uEntry *= 3;

	if( uEntry < elements(StdPalette) ) {

		return RGB( StdPalette[uEntry + 0],
			    StdPalette[uEntry + 1],
			    StdPalette[uEntry + 2]
			    );
		}

	return 0;
	}

// End of File
