
// This file does not include intern.hpp
//
// See the notes in the font data files!
//

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HeiFont_HPP
	
#define	INCLUDE_HeiFont_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hei Font Object (Regular)
//

class CHeiFont : public IGdiFont
{
	public:
		// Constructor
		CHeiFont(int ySize);

		// Destructor
		~CHeiFont(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Attributes
		BOOL IsProportional(void);
		int  GetBaseLine   (void);
		int  GetGlyphWidth (WORD c);
		int  GetGlyphHeight(WORD c);

		// Operations
		void InitBurst(IGdi *pGdi, CLogFont const &Font);
		void DrawGlyph(IGdi *pGdi, int &x, int &y, WORD c);
		void BurstDone(IGdi *pGdi, CLogFont const &Font);

	protected:
		// Font 0 Data
		static BYTE const m_bFontData0[];
		static WORD const m_wFontCode0[];
		static BYTE const m_xFontSize0[];
		static UINT const m_uFontFind0[];
		static UINT const m_uCount0;
		
		// Font 1 Data
		static BYTE const m_bFontData1[];
		static WORD const m_wFontCode1[];
		static BYTE const m_xFontSize1[];
		static UINT const m_uFontFind1[];
		static UINT const m_uCount1;
		
		// Font 2 Data
		static BYTE const m_bFontData2[];
		static WORD const m_wFontCode2[];
		static BYTE const m_xFontSize2[];
		static UINT const m_uFontFind2[];
		static UINT const m_uCount2;
		
		// Font 3 Data
		static BYTE const m_bFontData3[];
		static WORD const m_wFontCode3[];
		static BYTE const m_xFontSize3[];
		static UINT const m_uFontFind3[];
		static UINT const m_uCount3;

		// Font 4 Data
		static BYTE const m_bFontData4[];
		static WORD const m_wFontCode4[];
		static BYTE const m_xFontSize4[];
		static UINT const m_uFontFind4[];
		static UINT const m_uCount4;

		// Font 5 Data
		static BYTE const m_bFontData5[];
		static WORD const m_wFontCode5[];
		static BYTE const m_xFontSize5[];
		static UINT const m_uFontFind5[];
		static UINT const m_uCount5;

		// Font 6 Data
		static BYTE const m_bFontData6[];
		static WORD const m_wFontCode6[];
		static BYTE const m_xFontSize6[];
		static UINT const m_uFontFind6[];
		static UINT const m_uCount6;

		// Data Members
		int    m_ySize;
		PCBYTE m_pData;
		PCWORD m_pCode;
		PCBYTE m_pSize;
		PCUINT m_pFind;
		UINT   m_uCount;
		int    m_xExtra;
		int    m_xSpace;
		int    m_xDigit;
		BOOL   m_fFill;

		// Implementation
		BOOL  IsFixed(WORD c);
		BOOL  IsSpace(WORD c);
		BOOL  IsExplicitCode(WORD c);
		PVOID FindGlyph(WORD c);

		// Sort Function
		static int SortFunc(PCVOID p1, PCVOID p2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Hei Font Object (Regular)
//

// Font Attributes

#ifdef  __GNUC__

#define FONTDATA const __attribute__((aligned(16)))

#else

#define	FONTDATA const

#endif

// End of File

#endif
