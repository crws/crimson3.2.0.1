
#include "Intern.hpp"

#include "HeiFont.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Gdi Version 2.0
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hei Font Object (Regular)
//

// Instantiator

IGdiFont * Create_HeiFont(int ySize)
{
	return New CHeiFont(ySize);
	}

// Constructor

CHeiFont::CHeiFont(int ySize)
{
	switch( (m_ySize = ySize) ) {

		case 16:
			m_pData  = m_bFontData0;
			m_pCode  = m_wFontCode0;
			m_pSize  = m_xFontSize0;
			m_pFind  = m_uFontFind0;
			m_uCount = m_uCount0;
			m_xSpace = 5;
			m_xExtra = 0;
			break;

		case 10:
			m_pData  = m_bFontData1;
			m_pCode  = m_wFontCode1;
			m_pSize  = m_xFontSize1;
			m_pFind  = m_uFontFind1;
			m_uCount = m_uCount1;
			m_xSpace = 2;
			m_xExtra = 0;
			break;

		case 12:
			m_pData  = m_bFontData2;
			m_pCode  = m_wFontCode2;
			m_pSize  = m_xFontSize2;
			m_pFind  = m_uFontFind2;
			m_uCount = m_uCount2;
			m_xSpace = 3;
			m_xExtra = 1;
			break;

		case 14:
			m_pData  = m_bFontData3;
			m_pCode  = m_wFontCode3;
			m_pSize  = m_xFontSize3;
			m_pFind  = m_uFontFind3;
			m_uCount = m_uCount3;
			m_xSpace = 4;
			m_xExtra = 1;
			break;

		case 18:
			m_pData  = m_bFontData4;
			m_pCode  = m_wFontCode4;
			m_pSize  = m_xFontSize4;
			m_pFind  = m_uFontFind4;
			m_uCount = m_uCount4;
			m_xSpace = 4;
			m_xExtra = 1;
			break;

		case 20:
			m_pData  = m_bFontData5;
			m_pCode  = m_wFontCode5;
			m_pSize  = m_xFontSize5;
			m_pFind  = m_uFontFind5;
			m_uCount = m_uCount5;
			m_xSpace = 5;
			m_xExtra = 2;
			break;

		case 24:
			m_pData  = m_bFontData6;
			m_pCode  = m_wFontCode6;
			m_pSize  = m_xFontSize6;
			m_pFind  = m_uFontFind6;
			m_uCount = m_uCount6;
			m_xSpace = 6;
			m_xExtra = 2;
			break;
		}

	m_xDigit = GetGlyphWidth(L'4');

	m_fFill  = FALSE;
	}

// Destructor

CHeiFont::~CHeiFont(void)
{
	}

// IUnknown

HRESULT CHeiFont::QueryInterface(REFIID riid, void **ppObject)
{
	return E_NOINTERFACE;
	}

ULONG CHeiFont::AddRef(void)
{
	return 0;
	}

ULONG CHeiFont::Release(void)
{
	delete this;

	return 0;
	}

// Attributes

BOOL CHeiFont::IsProportional(void)
{
	return TRUE;
	}

int CHeiFont::GetBaseLine(void)
{
	return 4;
	}

int CHeiFont::GetGlyphWidth(WORD c)
{
	PVOID p;

	switch( c ) {

		case spaceNormal:
		case spaceNoBreak:

			return m_xSpace;

		case spaceNarrow:

			return m_xSpace / 2;

		case spaceHair:

			return 1;

		case spaceFigure:

			return m_xDigit;
		}

	if( IsFixed(c) ) {

		return m_xDigit;
		}

	if( c == 0x0478 ) {

		int x1 = GetGlyphWidth('O');
		
		int x2 = GetGlyphWidth('y');

		return x1 + x2;
		}

	if( c == 0x0479 ) {

		int x1 = GetGlyphWidth('o');
		
		int x2 = GetGlyphWidth('y');

		return x1 + x2;
		}

	if( (p = FindGlyph(c)) ) {

		UINT n = PWORD(p) - m_pCode;

		int cx = m_pSize[n];

		return cx + m_xExtra;
		}
	
	if( IsExplicitCode(c) ) {

		return 0;
		}

	return GetGlyphWidth('?');
	}

int CHeiFont::GetGlyphHeight(WORD c)
{
	return m_ySize;
	}

// Operations

void CHeiFont::InitBurst(IGdi *pGdi, CLogFont const &Font)
{
	if( Font.m_Trans == modeOpaque ) {

		pGdi->PushBrush();

		pGdi->SelectBrush(brushFore);

		pGdi->SetBrushFore(Font.m_Back);

		m_fFill = TRUE;
		}
	}

void CHeiFont::DrawGlyph(IGdi *pGdi, int &x, int &y, WORD c)
{
	PVOID p;

	if( IsSpace(c) ) {

		int cx = GetGlyphWidth(c);

		if( m_fFill ) {

			pGdi->FillRect(x, y, x+cx, y+m_ySize);
			}

		x += cx;

		return;
		}

	if( IsFixed(c) ) {

		c -= digitFixed;

		c += digitSimple;

		int cx = GetGlyphWidth(c);

		int cy = m_ySize;

		int x1, x2;
		
		if( c == ',' || c == '.' ) {

			x1 = 0;

			x2 = m_xDigit - cx;
			}
		else {
			x1 = (m_xDigit - cx + 0) / 2;

			x2 = (m_xDigit - cx + 1) / 2;

			if( c == '1' ) {

				x1 -= 1;

				x2 += 1;
				}
			}

		if( x1 ) {
			
			if( m_fFill ) {

				pGdi->FillRect(x, y, x+x1, y+cy);
				}

			x += x1;
			}

		if( TRUE ) {

			DrawGlyph(pGdi, x, y, c);
			}

		if( x2 ) {
			
			if( m_fFill ) {

				pGdi->FillRect(x, y, x+x2, y+cy);
				}

			x += x2;
			}

		return;
		}

	if( c == 0x0478 ) {

		DrawGlyph(pGdi, x, y, 'O');
	
		DrawGlyph(pGdi, x, y, 'y');

		return;
		}

	if( c == 0x0479 ) {

		DrawGlyph(pGdi, x, y, 'o');
	
		DrawGlyph(pGdi, x, y, 'y');

		return;
		}

	if( (p = FindGlyph(c)) ) {

		UINT n = PWORD(p) - m_pCode;

		int cx = m_pSize[n];

		int cy = m_ySize;

		if( m_xExtra >= 1 ) {

			if( m_fFill ) {

				pGdi->FillRect(x, y, x+1, y+cy);
				}

			x += 1;
			}

		if( TRUE ) {

			PBYTE ps = PBYTE(m_pData + m_pFind[n]);

			pGdi->CharBlt( x,
				       y,
				       cx,
				       cy,
				       0,
				       ps
				       );

			x += cx;
			}

		if( m_xExtra == 2 ) {

			if( m_fFill ) {

				pGdi->FillRect(x, y, x+1, y+cy);
				}

			x += 1;
			}

		return;
		}

	if( IsExplicitCode(c) ) {

		return;
		}

	DrawGlyph(pGdi, x, y, '?');
	}

void CHeiFont::BurstDone(IGdi *pGdi, CLogFont const &Font)
{
	if( m_fFill ) {

		pGdi->PullBrush();

		m_fFill = FALSE;
		}
	}

// Implementation

BOOL CHeiFont::IsFixed(WORD c)
{
	return c >= 0xFF01 && c <= 0xFF5D;
	}

BOOL CHeiFont::IsSpace(WORD c)
{
	switch( c ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;
		}

	return FALSE;
	}

BOOL CHeiFont::IsExplicitCode(WORD c)
{
	switch( c ) {

		case uniLRE:
		case uniRLE:
		case uniLRO:
		case uniRLO:
		case uniLRM:
		case uniRLM:
		case uniPDF:

			return TRUE;
		}

	return FALSE;
	}

PVOID CHeiFont::FindGlyph(WORD c)
{
	PVOID p;

	if( c >= 0x21 && c <= 0x7E ) {

		return PVOID(m_pCode + c - 0x21);
		}

	if( (p = bsearch(&c, m_pCode, m_uCount, sizeof(WORD), SortFunc)) ) {

		return p;
		}

	return NULL;
	}

// Sort Function

int CHeiFont::SortFunc(PCVOID p1, PCVOID p2)
{
	WORD w1 = PWORD(p1)[0];

	WORD w2 = PWORD(p2)[0];

	if( w1 < w2 ) return -1;

	if( w1 > w2 ) return +1;

	return 0;
	}

// End of File
