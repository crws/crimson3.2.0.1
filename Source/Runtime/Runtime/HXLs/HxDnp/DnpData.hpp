
//////////////////////////////////////////////////////////////////////////
//
// DNP System Support
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnpData_HPP

#define	INCLUDE_DnpData_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "DnpSys.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef TMWTYPES_ANALOG_VALUE			dnpANA;
typedef DNPDEFS_FILE_CMD_STAT			dnpFCS;
typedef DNPDEFS_FILE_TFER_STAT			dnpFTS;
typedef DNPDEFS_FILE_TYPE			dnpFT;
typedef DNPDEFS_FILE_PERMISSIONS		dnpFP;
typedef DNPDEFS_FILE_MODE			dnpFM;
typedef TMWDEFS_CLASS_MASK			dnpMASK;
typedef TMWDEFS_EVENT_MODE			dnpEM;
typedef DNPDEFS_CROB_ST				dnpCRST;
typedef DNPDEFS_CTLSTAT				dnpCTST;

//////////////////////////////////////////////////////////////////////////
//
// DNP SCL API Master Prototypes
//

void	DataM_processIIN(dnpSES *pSession, dnpWORD *pIIN);
void	DataM_storeIIN(void *pHandle, dnpWORD pointNumber, dnpBOOL value);
void *	DataM_init(dnpSES *pSession, void *pUserHandle);
void	DataM_close(void *pHandle);
void	DataM_storeReadTime(void *pHandle, dnpTIME *pTimeStamp);
dnpBOOL DataM_storeBinaryInput(void *pHdl, dnpWORD pt, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeDoubleInput(void *pHdl, dnpWORD pt, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeBinaryOutput(void *pHdl, dnpWORD pt, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeBinaryCmdStatus(void *pHdl, dnpWORD pt, dnpUCHAR s, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeBinaryCounter(void *pHdl, dnpWORD pt, dnpLONG val, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeFrozenCounter(void *pHdl, dnpWORD pt, dnpLONG val, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeAnalogInput(void *pHdl, dnpWORD pt, dnpANA *pVal, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeAnalogInputDeadband(void *pHdl, dnpWORD pt, dnpANA *pValue);
dnpBOOL DataM_storeAnalogOutput(void *pHdl, dnpWORD pt, dnpANA *pVal, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeAnalogCmdStatus(void *pHdl, dnpWORD pt, dnpANA *pVal, dnpUCHAR s, dnpBOOL isE, dnpTIME *pTS);
dnpBOOL DataM_storeRestartTime(void *pHandle, dnpLONG time);
dnpBOOL DataM_storeString(void *pHdl, dnpWORD pt, dnpUCHAR *pStrBuf, dnpUCHAR strLen);
dnpBOOL DataM_storeActiveConfig(void *pHdl, dnpUCHAR i, dnpLONG d, dnpUCHAR s, dnpUCHAR *pStr, dnpUCHAR len);
dnpBOOL DataM_storeVirtualTerminal(void *pHandle, dnpWORD pt, dnpUCHAR *pVtermBuf, dnpUCHAR vtermLen);
dnpBOOL DataM_storeFileAuthKey(void *pHandle, dnpLONG authKey);
dnpBOOL DataM_storeFileStatus(void *pHdl, dnpLONG h, dnpLONG fs, dnpWORD b, dnpWORD r, dnpFCS s, dnpWORD o, const dnpCHAR *pO);
dnpBOOL DataM_storeFileData(void *pHdl, dnpLONG h, dnpLONG n, dnpBOOL f, dnpWORD b, const dnpUCHAR *pData);
dnpBOOL DataM_storeFileDataStatus(void *pHdl, dnpLONG h, dnpLONG n, dnpBOOL f, dnpFTS s, dnpWORD o, const dnpCHAR *pO);
dnpBOOL DataM_storeFileInfo(void *pHdl, dnpWORD o, dnpWORD n, dnpFT t, dnpLONG s, dnpTIME *pTime, dnpFP p, const dnpCHAR *pN);
dnpLONG DataM_getFileAuthKey(void *pHandle);
dnpBOOL DataM_openLocalFile(void *pHandle, const dnpCHAR *pLocalFileName, dnpFM fileMode);
dnpBOOL DataM_closeLocalFile(void *pHandle);
dnpBOOL DataM_getLocalFileInfo(void *pHandle, const dnpCHAR *pName, dnpLONG *pSize, dnpTIME *pTime);
dnpWORD DataM_readLocalFile(void *pHandle, dnpUCHAR *pBuf, dnpWORD bufSize, dnpBOOL *pLastBlock);

//////////////////////////////////////////////////////////////////////////
//
// DNP SCL API Slave Prototypes
//

void *	 DataS_init(dnpSES *pSession, void *pUserHandle);
void	 DataS_close(void *pHandle);
void	 DataS_getIIN(dnpSES *pSession, dnpWORD *pIIN);
dnpWORD  DataS_IINQuantity(void *pHandle);
dnpBOOL  DataS_IINRead(void *pHandle, dnpWORD pointNumber);
void	 DataS_coldRestart(dnpSES *pSession);
void	 DataS_warmRestart(dnpSES *pSession);
void	 DataS_setTime(void *pHandle, dnpTIME *pNewTime);
void	 DataS_unsolEventMask(void *pHandle, dnpMASK unsolEventMask);
void	 DataS_eventAndStaticRead(void *pHandle, dnpBOOL inProgress);
void	 DataS_funcCode(void *pHandle, char functionCode, dnpBOOL inProgress);
dnpWORD	 DataS_binInQuantity(void *pHandle);
void *	 DataS_binInGetPoint(void *pHandle, dnpWORD pointNum);
dnpUCHAR DataS_binInDefVariation(void *pPoint);
dnpMASK  DataS_binInEventClass(void *pPoint);
dnpBOOL	 DataS_binInIsClass0(void *pPoint);
dnpUCHAR DataS_binInEventDefVariation(void *pPoint, dnpMASK classMask);
dnpEM	 DataS_binInEventMode(void *pPoint);
dnpBOOL  DataS_binInAssignClass(void *pPoint, dnpMASK classMask);
void	 DataS_binInRead(void *pPoint, dnpUCHAR *pFlags);
dnpBOOL  DataS_binInChanged(void *pPoint, dnpUCHAR *pFlags);
dnpWORD  DataS_binOutQuantity(void *pHandle);
void *   DataS_binOutGetPoint(void *pHandle, dnpWORD pointNum);
dnpUCHAR DataS_binOutDefVariation(  void *pPoint);
dnpMASK  DataS_binOutEventClass(void *pPoint);
dnpBOOL  DataS_binOutIsClass0(void *pPoint);
dnpUCHAR DataS_binOutEventDefVariation(void *pPoint, dnpMASK classMask);
dnpEM	 DataS_binOutEventMode(void *pPoint);
dnpBOOL  DataS_binOutAssignClass(void *pPoint, dnpMASK classMask);
void	 DataS_binOutRead(void *pPoint, dnpUCHAR *pFlags);
dnpBOOL  DataS_binOutChanged(void *pPoint, dnpUCHAR *pFlags);
dnpBOOL  DataS_binOutWrite(void *pPoint, dnpUCHAR value);
dnpUCHAR DataS_binOutGetControlMask(void *pPoint);
dnpCRST	 DataS_binOutSelect(void *pPt, dnpUCHAR ctrlCode, dnpUCHAR ct, dnpLONG onTime, dnpLONG offTime);
dnpCRST  DataS_binOutOperate(void *pPt, dnpUCHAR ctrlCode, dnpUCHAR ct, dnpLONG onTime, dnpLONG offTime);
dnpCRST  DataS_binOutSelPatternMask(void *pH, dnpUCHAR ctrl, dnpUCHAR ct, dnpLONG on, dnpLONG off, dnpWORD f, dnpWORD l, dnpUCHAR *pMask);
dnpCRST  DataS_binOutOpPatternMask(void *pH, dnpUCHAR ctrl, dnpUCHAR ct, dnpLONG on, dnpLONG off, dnpWORD f, dnpWORD l, dnpUCHAR *pMask);
dnpMASK	 DataS_binOutCmdEventClass(void *pPoint);
dnpUCHAR DataS_binOutCmdEventDefVariation(void *pPoint, dnpMASK classMask);
dnpEM	 DataS_binOutCmdEventMode(void *pPoint);
dnpBOOL  DataS_binOutCmdChanged(void *pPoint, dnpUCHAR *pStatus);
dnpBOOL  DataS_binOutCmdAssignClass(void *pPoint, dnpMASK classMask);
dnpWORD  DataS_binCntrQuantity(void *pHandle);
void *	 DataS_binCntrGetPoint(void *pHandle, dnpWORD pointNum);
void *   DataS_binCntrGetFrzPoint(void *pHandle, dnpWORD pointNum);
dnpMASK  DataS_binCntrEventClass(void *pPoint);
dnpBOOL  DataS_binCntrIsClass0(void *pPoint);
dnpEM	 DataS_binCntrEventMode(void *pPoint);
dnpBOOL  DataS_binCntrAssignClass(void *pPoint, dnpMASK classMask);
dnpUCHAR DataS_binCntrDefVariation(void *pPoint);
dnpUCHAR DataS_binCntrEventDefVariation(void *pPoint, dnpMASK classMask);
void	 DataS_binCntrRead(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags);
dnpBOOL  DataS_binCntrChanged(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags);
dnpBOOL  DataS_binCntrFreeze(void *pPoint, dnpBOOL clearAfterFreeze);
dnpWORD	 DataS_frznCntrQuantity(void *pHandle);
void *   DataS_frznCntrGetPoint(void *pHandle, dnpWORD pointNum);
dnpMASK  DataS_frznCntrEventClass(void *pPoint);
dnpBOOL  DataS_frznCntrIsClass0(void *pPoint);
dnpBOOL  DataS_frznCntrAssignClass(void *pPoint, dnpMASK classMask);
dnpUCHAR DataS_frznCntrDefVariation(void *pPoint);
dnpUCHAR DataS_frznCntrEventDefVariation(void *pPoint, dnpMASK classMask);
dnpEM	 DataS_frznCntrEventMode(void *pPoint);
void	 DataS_frznCntrRead(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags, dnpTIME *pTimeOfFreeze);
dnpBOOL  DataS_frznCntrChanged(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags);
dnpWORD  DataS_anlgInQuantity(void *pHandle);
void *	 DataS_anlgInGetPoint(void *pHandle, dnpWORD pointNum);
dnpUCHAR DataS_anlgInDefVariation(void *pPoint);
dnpUCHAR DataS_anlgInEventDefVariation(void *pPoint, dnpMASK classMask);
dnpEM	 DataS_anlgInEventMode(void *pPoint);
dnpMASK  DataS_anlgInEventClass(void *pPoint);
dnpBOOL  DataS_anlgInIsClass0(void *pPoint);
dnpBOOL  DataS_anlgInAssignClass(void *pPoint, dnpMASK classMask);
void     DataS_anlgInRead(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags);
dnpBOOL  DataS_anlgInChanged(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags);
dnpWORD	 DataS_anlgInDBandQuantity(void *pHandle);
void *	 DataS_anlgInDBandGetPoint(void *pHandle, dnpWORD pointNum);
dnpUCHAR DataS_anlgInDbandDefVar(void *pPoint);
void	 DataS_anlgInDBandRead(void *pPoint, dnpANA *pValue);
dnpBOOL  DataS_anlgInDBandWrite(void *pPoint, dnpANA *pValue);
dnpWORD	 DataS_anlgOutQuantity(void *pHandle);
void *   DataS_anlgOutGetPoint(void *pHandle, dnpWORD pointNum);
dnpUCHAR DataS_anlgOutDefVariation(void *pPoint);
dnpUCHAR DataS_anlgOutEventDefVariation(void *pPoint, dnpMASK classMask);
dnpEM	 DataS_anlgOutEventMode(void *pPoint);
dnpMASK  DataS_anlgOutEventClass(void *pPoint);
dnpBOOL  DataS_anlgOutIsClass0(void *pPoint);
dnpBOOL  DataS_anlgOutAssignClass(void *pPoint, dnpMASK classMask);
void     DataS_anlgOutRead(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags);
dnpBOOL  DataS_anlgOutChanged(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags);
dnpCTST  DataS_anlgOutSelect(void *pPoint, dnpANA *pValue);
dnpCTST  DataS_anlgOutOperate(void *pPoint, dnpANA *pValue);
dnpUCHAR DataS_anlgOutCmdEventDefVariation(void *pPoint, dnpMASK classMask);
dnpEM	 DataS_anlgOutCmdEventMode(void *pPoint);
dnpMASK	 DataS_anlgOutCmdEventClass(void *pPoint);
dnpBOOL  DataS_anlgOutCmdAssignClass(void *pPoint, dnpMASK classMask);
dnpBOOL  DataS_anlgOutCmdChanged(void *pPoint, dnpANA *pValue, dnpUCHAR *pStatus);
dnpWORD  DataS_dblInQuantity(void *pHandle);
void *	 DataS_dblInGetPoint(void *pHandle, dnpWORD pointNum);
dnpUCHAR DataS_dblInDefVariation(void *pPoint);
dnpEM	 DataS_dblInEventMode(void *pPoint);
dnpMASK  DataS_dblInEventClass(void *pPoint);
dnpBOOL  DataS_dblInIsClass0(void *pPoint);
dnpUCHAR DataS_dblInEventDefVariation(void *pPoint, dnpMASK classMask);
dnpBOOL  DataS_dblInAssignClass(void *pPoint, dnpMASK classMask);
void	 DataS_dblInRead(void *pPoint, dnpUCHAR *pFlags);
dnpBOOL	 DataS_dblInChanged(void *pPoint, dnpUCHAR *pFlags);
dnpWORD	 DataS_strQuantity(void *pHandle);
void *   DataS_strGetPoint(void *pHandle, dnpWORD pointNum);
dnpEM	 DataS_strEventMode(void *pPoint);
dnpMASK  DataS_strEventClass(void *pPoint);
dnpBOOL  DataS_strAssignClass(void *pPoint, dnpMASK classMask);
dnpBOOL  DataS_strIsClass0(void *pPoint);
void	 DataS_strRead(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength);
dnpBOOL  DataS_strWrite(void *pPoint, dnpUCHAR *pBuf, dnpUCHAR bufLength);
dnpBOOL  DataS_strChanged(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength);
dnpWORD  DataS_vtermQuantity(void *pHandle);
void *   DataS_vtermGetPoint(void *pHandle, dnpWORD pointNum);
dnpEM	 DataS_vtermEventMode(void *pPoint);
dnpMASK  DataS_vtermEventClass(void *pPoint);
dnpBOOL  DataS_vtermAssignClass(void *pPoint, dnpMASK classMask);
void	 DataS_vtermRead(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength);
dnpBOOL  DataS_vtermWrite(void *pPoint, dnpUCHAR *pBuf, dnpUCHAR bufLength);
dnpBOOL  DataS_vtermChanged(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength);
dnpMASK  DataS_fileEventClass(void *pHandle);
dnpBOOL  DataS_fileAssignClass(void *pHandle, dnpMASK classMask);
dnpFCS	 DataS_getFileInfo(dnpSES *pSess, dnpCHAR *pName, dnpFT *pType, dnpLONG *pSize, dnpTIME *pCreated, dnpFP *pPerm);
dnpFTS	 DataS_readFileInfo(dnpSES *pSes, dnpLONG h, dnpWORD c, dnpCHAR *pName, dnpBOOL *pL, dnpFT *pT, dnpLONG *pS, dnpTIME *pTime, dnpFP *pPerm);
dnpBOOL  DataS_getAuthentication(dnpSES *pSess, dnpCHAR *pUsername, dnpCHAR *pPassword, dnpLONG *pAuthKey);
dnpFCS	 DataS_deleteFile(dnpSES *pSession, dnpCHAR *pFilename, dnpLONG authKey);
dnpFCS	 DataS_openFile(dnpSES *pSes, dnpCHAR *pName, dnpLONG ak, dnpFM m, dnpWORD *pMax, dnpFP *pPerm, dnpTIME *pTime, dnpLONG *pH, dnpLONG *pS, dnpFT *pT);
dnpFCS	 DataS_closeFile(dnpSES *pSession, dnpLONG handle);
dnpFTS	 DataS_readFile(dnpSES *pSession, dnpLONG handle, dnpBOOL *pLast, dnpWORD *pBytesRead, dnpUCHAR *pBuf);
void	 dataS_confirmFileRead(dnpSES *pSession, dnpLONG handle);
dnpFTS	 DataS_writeFile(dnpSES *pSession, dnpLONG handle, dnpBOOL last, dnpWORD numBytes, dnpUCHAR *pBuf);

// End of File

#endif
