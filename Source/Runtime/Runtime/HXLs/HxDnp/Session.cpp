
#include "Intern.hpp"

#include "Session.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP Session - Device
//

// Constructor

CSession::CSession(IDnpChannel *pChan, WORD wDest, WORD wTO, DWORD dwLink)
{
	StdSetRef();

	m_fOnline   = FALSE;

	m_pChannel  = pChan;

	m_pDatabase = NULL;

	m_pSession  = NULL;

	Init();
	}

// Destructor

CSession::~CSession(void)
{
	}

// IUnknown

HRESULT CSession::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDnpSession);

	StdQueryInterface(IDnpSession);

	return E_NOINTERFACE;
	}

ULONG CSession::AddRef(void)
{
	StdAddRef();
	}

ULONG CSession::Release(void)
{
	StdRelease();
	}

// IDnpSession

BOOL METHOD CSession::Open(IDnpChannel * pChan)
{
	return FALSE;
	}

BOOL METHOD CSession::Close(void)
{
	return FALSE;
	}

BOOL METHOD CSession::Ping(void)
{	
	return m_fOnline;
	}

UINT METHOD CSession::Validate(BYTE o, WORD i, BYTE t, UINT uCount)
{
	UINT u = 0;

	if( m_pDatabase ) {

		CUserTable * pTable = m_pDatabase->Find(o);

		if( pTable ) {

			CUserData * pUser = NULL;

			while( u < uCount ) {

				pUser = pTable->Find(i + u, t, TRUE);

				if( !pUser ) {

					return u;
					}

				u++;
				}
			}
		}

	return u;
	}

UINT METHOD CSession::GetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {

			CUserData * pUser = pTable->Find(i + u, t);

			if( pUser ) {

				pData = pUser->GetData(pData);
				
				u++;

				if( u % 100 == 0 ) {

					Sleep(10);
					}

				continue;
				}

			break;				
			}

		return u;
		}

	return CCODE_ERROR;
	}

UINT METHOD CSession::GetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {
		
			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pData[u] = pUser->GetFlags();

				u++;

				if( u % 100 == 0 ) {

					Sleep(10);
					}

				continue;
				}

			break;				
			}

		return u;
		}

	return CCODE_ERROR;
	}

UINT METHOD CSession::GetTimeStamp(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {
		
			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pData[u] = pUser->GetTimeStamp();

				u++;

				if( u % 100 == 0 ) {

					Sleep(10);
					}

				continue;
				}

			break;				
			}

		return u;
		}

	return CCODE_ERROR;
	}

UINT METHOD CSession::GetClass(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {
		
			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pData[u] = pUser->GetClass();

				u++;

				if( u % 100 == 0 ) {

					Sleep(10);
					}

				continue;
				}

			break;				
			}

		return u;
		}

	return CCODE_ERROR;
	}

UINT METHOD CSession::GetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {
		
			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pData[u] = pUser->GetOnTime();

				u++;

				if( u % 100 == 0 ) {

					Sleep(10);
					}

				continue;
				}

			break;				
			}

		return u;
		}

	return CCODE_ERROR;
	}

UINT METHOD CSession::GetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {
		
			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pData[u] = pUser->GetOffTime();

				u++;

				if( u % 100 == 0 ) {

					Sleep(10);
					}

				continue;
				}

			break;				
			}

		return u;
		}

	return CCODE_ERROR;
	}

// Implementation

void CSession::Init(void)
{
	m_fTimeout = FALSE;

	m_fSuccess = FALSE;

	m_fFail    = FALSE;
	}

void CSession::Online(BOOL fOnline)
{
	m_fOnline = fOnline;

	if( m_fOnline ) {

		m_fTimeout = FALSE;
		}
	}

void CSession::Timeout(void)
{
	m_fTimeout = TRUE;
	}

void CSession::Success(void)
{
	m_fSuccess = TRUE;
	}

void CSession::SetFail(void)
{
	m_fFail    = TRUE;
	
	m_fTimeout = TRUE;
	}

// Friends

global void StatCallback(void * pVoid, TMWSESN_STAT_EVENT Event, void * pNull)
{
	if( DNP_SESS_DEBUG ) {

		AfxTrace("\nStat Callback %u %8.8x %8.8x", Event, pVoid, pNull); 
		}

	CSession * pSession = (CSession *)pVoid;

	if( pSession ) {

		switch( Event ) {

			case TMWSESN_STAT_ERROR:		pSession->Timeout();	break;
			case TMWSESN_STAT_ASDU_RECEIVED:	pSession->Success();					
			case TMWSESN_STAT_ONLINE:		pSession->Online(1);	break;
			case TMWSESN_STAT_OFFLINE:		pSession->Online(0);	break;
			case TMWSESN_STAT_REQUEST_TIMEOUT:	pSession->Timeout();	break;
			case TMWSESN_STAT_REQUEST_FAILED:	pSession->SetFail();	break;
			default:							break;
			/*TMWSESN_STAT_OFFLINE,
			TMWSESN_STAT_ASDU_SENT,
			TMWSESN_STAT_ASDU_RECEIVED,
			TMWSESN_STAT_EVENT_OVERFLOW,
			TMWSESN_STAT_DNPEVENT_SENT,
			TMWSESN_STAT_DNPEVENT_CONFIRM,
 			TMWSESN_STAT_DNPUNSOL_TIMER_START,
			TMWSESN_STAT_REQUEST_TIMEOUT,
			TMWSESN_STAT_REQUEST_FAILED,*/
			}

		if( DNP_SESS_DEBUG ) {

			if( Event == TMWSESN_STAT_DNPEVENT_SENT ) {

				TMWSESN_STAT_DNPEVENT * pEvent = (TMWSESN_STAT_DNPEVENT *)pNull;

				if( pEvent ) {

					AfxTrace("\nEvent Sent %u %u", pEvent->group, pEvent->point);
					}
				}

			if( Event == TMWSESN_STAT_DNPEVENT_CONFIRM ) {

				TMWSESN_STAT_DNPEVENT * pEvent = (TMWSESN_STAT_DNPEVENT *)pNull;

				if( pEvent ) {

					AfxTrace("\nEvent Confirm %u %u", pEvent->group, pEvent->point);
					}
				}
			}
		}
	}

// End of File
