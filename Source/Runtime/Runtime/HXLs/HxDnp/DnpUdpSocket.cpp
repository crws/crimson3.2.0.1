
#include "Intern.hpp"

#include "DnpUdpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Dnp UDP Socket
//

// Instantiator

ISocket * Create_DnpUdpSocket(IDnpChannelConfig * pConfig)
{
	return New CDnpUdpSocket(pConfig);
}

// Constructor

CDnpUdpSocket::CDnpUdpSocket(IDnpChannelConfig * pConfig)
{
	AfxNewObject("sock-udp", ISocket, m_pSocket);

	m_pRxBuff = NULL;

	m_IP	  = pConfig->GetIP();

	m_wPort   = pConfig->GetUdpPort();

	StdSetRef();
}

// Destructor

CDnpUdpSocket::~CDnpUdpSocket(void)
{
	AfxRelease(m_pRxBuff);

	m_pSocket->Release();
}

// IUnknown

HRESULT CDnpUdpSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
}

ULONG CDnpUdpSocket::AddRef(void)
{
	StdAddRef();
}

ULONG CDnpUdpSocket::Release(void)
{
	StdRelease();
}

// ISocket Methods

HRESULT CDnpUdpSocket::Listen(WORD Loc)
{
	return m_pSocket->Listen(Loc);
}

HRESULT CDnpUdpSocket::Listen(IPADDR const &IP, WORD Loc)
{
	return m_pSocket->Listen(IP, Loc);
}

HRESULT CDnpUdpSocket::Connect(IPADDR const &IP, WORD Rem)
{
	return m_pSocket->Connect(IP, Rem);
}

HRESULT CDnpUdpSocket::Connect(IPADDR const &IP, WORD Rem, WORD Loc)
{
	return m_pSocket->Connect(IP, Rem, Loc);
}

HRESULT CDnpUdpSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	return m_pSocket->Connect(IP, IF, Rem, Loc);
}

HRESULT CDnpUdpSocket::Recv(PBYTE pData, UINT &uSize)
{
	if( !m_pRxBuff ) {

		if( m_pSocket->Recv(m_pRxBuff) == S_OK ) {

			RemHeader();
		}
	}

	if( m_pRxBuff ) {

		MakeMin(uSize, m_pRxBuff->GetSize());

		memcpy(pData, m_pRxBuff->StripHead(uSize), uSize);

		if( !m_pRxBuff->GetSize() ) {

			BuffRelease(m_pRxBuff);

			m_pRxBuff = NULL;
		}

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CDnpUdpSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	return E_FAIL;
}

HRESULT CDnpUdpSocket::Send(PBYTE pData, UINT &uSize)
{
	return E_FAIL;
}

HRESULT CDnpUdpSocket::Recv(CBuffer * &pBuff)
{
	return E_FAIL;
}

HRESULT CDnpUdpSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	return E_FAIL;
}

HRESULT CDnpUdpSocket::Send(CBuffer *pBuff)
{
	if( pBuff ) {

		AddHeader(pBuff);

		return m_pSocket->Send(pBuff);
	}

	return E_FAIL;
}

HRESULT CDnpUdpSocket::GetLocal(IPADDR &IP)
{
	return m_pSocket->GetLocal(IP);
}

HRESULT CDnpUdpSocket::GetRemote(IPADDR &IP)
{
	IP = m_IP;

	return S_OK;
}

HRESULT CDnpUdpSocket::GetLocal(IPADDR &IP, WORD &Port)
{
	return m_pSocket->GetLocal(IP, Port);
}

HRESULT CDnpUdpSocket::GetRemote(IPADDR &IP, WORD &Port)
{
	IP   = m_IP;

	Port = m_wPort;

	return S_OK;
}

HRESULT CDnpUdpSocket::GetPhase(UINT &Phase)
{
	return m_pSocket->GetPhase(Phase);
}

HRESULT CDnpUdpSocket::SetOption(UINT uOption, UINT uValue)
{
	return m_pSocket->SetOption(uOption, uValue);
}

HRESULT CDnpUdpSocket::Abort(void)
{
	return m_pSocket->Abort();
}

HRESULT CDnpUdpSocket::Close(void)
{
	return m_pSocket->Close();
}

// Implementation

void CDnpUdpSocket::RemHeader(void)
{
	PBYTE pHead = m_pRxBuff->StripHead(6);

	m_IP.m_b1 = pHead[0];
	m_IP.m_b2 = pHead[1];
	m_IP.m_b3 = pHead[2];
	m_IP.m_b4 = pHead[3];

	m_wPort   = MAKEWORD(pHead[5], pHead[4]);
}

void CDnpUdpSocket::AddHeader(CBuffer * &pBuff)
{
	UINT uSize = 6;

	PBYTE pHead = pBuff->AddHead(uSize);

	pHead[0] = PBYTE(&m_IP)[0];
	pHead[1] = PBYTE(&m_IP)[1];
	pHead[2] = PBYTE(&m_IP)[2];
	pHead[3] = PBYTE(&m_IP)[3];

	pHead[4] = HIBYTE(m_wPort);
	pHead[5] = LOBYTE(m_wPort);
}

// End of File
