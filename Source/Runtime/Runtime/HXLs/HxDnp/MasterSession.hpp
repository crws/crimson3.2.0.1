
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MasterSession_HPP

#define	INCLUDE_MasterSession_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "Session.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Master Session Object - Device
//

class CMasterSession : public CSession, public IDnpMasterSession
{
	public:
		// Constructor
		CMasterSession(IDnpChannel * pChan, WORD wDest, WORD wTO, DWORD dwLink, void * pCfg);

		// Destructor
		~CMasterSession(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnpSession
		BOOL METHOD Open(IDnpChannel * pChan);
		BOOL METHOD Close(void);
		BOOL METHOD Ping(void);
		UINT METHOD Validate(BYTE o, WORD i, BYTE t, UINT uCount);
		UINT METHOD GetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount);
		UINT METHOD GetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetTimeStamp(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetClass(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount);

		// IDnpMasterSession
		BOOL METHOD PollClass(BYTE bClass);
		BOOL METHOD Poll(BYTE bObject, WORD i, UINT uCount);
		BOOL METHOD PollEvents(BYTE bObject, WORD i, UINT uCount);
		BOOL METHOD SyncTime(void);
		BOOL METHOD ColdRestart(void);
		BOOL METHOD WarmRestart(void);
		BOOL METHOD UnsolicitedEnable(BYTE bMask);
		BOOL METHOD UnsolicitedDisable(BYTE bMask);
		UINT METHOD AssignClass(BYTE bObject, WORD i, PDWORD pData, UINT uCount);
		BOOL METHOD AnalogCmd(WORD i, BYTE t, PDWORD pData, UINT uCount);
		BOOL METHOD BinaryCmd(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount);
		BOOL METHOD AnalogDbd(WORD i, BYTE t, PDWORD pData, UINT uCount);
		BOOL METHOD FreezeCtr(WORD i, PDWORD pData);
		UINT METHOD GetFeedBack(void);
										
	protected:
		// Data Members
		cfgMSes	 m_Config;
		mReqDesc m_ReqDesc;
		
		// Implementation
		void InitRequest(void * func, void * v);
		BOOL Response(TMWSESN_TX_DATA * pData);

		// Helpers
		BOOL IsTimedOut(UINT uTime, UINT uSpan);
		
		// Callback Functions
		friend void PingCallback(void *pVoid, DNPCHNL_RESPONSE_INFO * pResponse);
		friend void ReadGroupCallback(void * pVoid, DNPCHNL_RESPONSE_INFO *pResponse);
		friend void PollEventsCallback(void * pVoid, DNPCHNL_RESPONSE_INFO *pResponse);
		friend void WriteCallback(void * pVoid, DNPCHNL_RESPONSE_INFO * pResponse);
		friend void UnsolCallback(void * pVoid, MDNPSESN_UNSOL_RESP_INFO *pResponse);
	};

// End of File

#endif
