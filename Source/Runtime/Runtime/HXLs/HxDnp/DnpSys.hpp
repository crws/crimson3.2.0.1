
//////////////////////////////////////////////////////////////////////////
//
// DNP System Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnpSys_HPP

#define	INCLUDE_DnpSys_HPP

/////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef TMWTYPES_UINT				dnpUINT;
typedef TMWTYPES_CHAR				dnpCHAR;
typedef TMWDIAG_ANLZ_ID				dnpDIAG;
typedef TMWDEFS_RESOURCE_LOCK			dnpLOCK;
typedef TMWTYPES_MILLISECONDS			dnpMS;
typedef TMWDTIME				dnpTIME;
typedef TMWTYPES_BOOL				dnpBOOL;
typedef TMWTARG_CONFIG				dnpCNFG;
typedef TMWCHNL					dnpCHAN;
typedef TMWTYPES_USHORT				dnpWORD;
typedef TMWTARG_CHANNEL_RECEIVE_CBK_FUNC	dnpFRX;
typedef TMWTARG_CHECK_ADDRESS_FUNC		dnpFCHK;
typedef TMWSESN					dnpSES;
typedef TMWSCTR					dnpSEC;
typedef TMWTYPES_UCHAR				dnpUCHAR;
typedef TMWTYPES_ULONG				dnpLONG;

//////////////////////////////////////////////////////////////////////////
//
// DNP SCL API Prototypes
//

void		Sys_Init(void);
void *		Sys_Alloc(dnpUINT numBytes);
void		Sys_Free(void * pBuff);
int		Sys_Snprintf(dnpCHAR *buf, dnpUINT count, const dnpCHAR *format, ...);
void		Sys_PutDiagString(const dnpDIAG * pAnlzId, const dnpCHAR *pString);
void		Sys_LockInit(dnpLOCK *pLock);
void		Sys_LockShare(dnpLOCK *pLock, dnpLOCK *pLock1);
void		Sys_LockSection(dnpLOCK *pLock);
void		Sys_UnlockSection(dnpLOCK *pLock);
void		Sys_LockDelete(dnpLOCK *pLock);
dnpMS		Sys_GetMSTime(void);
void		Sys_GetDateTime(dnpTIME *pDateTime);
dnpBOOL		Sys_SetDateTime(dnpTIME *pDateTime);
void *		Sys_InitChannel(const void * pUser, dnpCNFG * pCfg, dnpCHAN * pChan);
void		Sys_DeleteChannel(void * pCtx);
const dnpCHAR *	Sys_GetChannelName(void * pCtx);
const dnpCHAR *	Sys_GetChannelInfo(void * pCtx);
dnpBOOL		Sys_OpenChannel(void *pCtx, dnpFRX pRx, dnpFCHK pChk, void *pParam);
void		Sys_CloseChannel(void * pCtx);
const dnpCHAR * Sys_GetSessionName(dnpSES *pSession);
const dnpCHAR * Sys_GetSectorName(dnpSEC *pSector);
dnpMS		Sys_GetTransmitReady(void *pCtx);
dnpWORD		Sys_Rx(void *pCtx, dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst);
dnpBOOL		Sys_Tx(void *pCtx, dnpUCHAR *pBuff, dnpWORD Bytes);
dnpBOOL		Sys_TxUDP(void *pCtx, dnpUCHAR UDPPort, dnpUCHAR *pBuff, dnpWORD numBytes);

// End of File

#endif
