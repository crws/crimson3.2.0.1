
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "StdEnv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Utils
//

#define strcpy_s(d, n, s)	strcpy(d, s)

#define strncpy_s(d, a, s, b)	strncpy(d, s, b)

//////////////////////////////////////////////////////////////////////////
//
// System
//

STRONG_INLINE static void SystemReboot(void)
{
	IPlatform * pPlatform = NULL;

	AfxGetObject("platform", 0, IPlatform, pPlatform);

	pPlatform->ResetSystem();

	AfxRelease(pPlatform);
}

// End of File

#endif
