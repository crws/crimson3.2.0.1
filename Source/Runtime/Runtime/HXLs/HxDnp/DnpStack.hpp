
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnpStack_HPP

#define	INCLUDE_DnpStack_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Stack Headers
//

#include "tmwscl/utils/tmwappl.h"
#include "tmwscl/utils/tmwdb.h"
#include "tmwscl/utils/tmwphys.h"
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtarg.h"

#include "tmwscl/dnp/dnpchnl.h"
#include "tmwscl/dnp/dnplink.h"
#include "tmwscl/dnp/mdnpbrm.h"
#include "tmwscl/dnp/mdnpsesn.h"
#include "tmwscl/dnp/sdnpsesn.h"

#include "tmwscl/dnp/sdnpo002.h"
#include "tmwscl/dnp/sdnpo004.h"
#include "tmwscl/dnp/sdnpo010.h"
#include "tmwscl/dnp/sdnpo011.h"
#include "tmwscl/dnp/sdnpo013.h"
#include "tmwscl/dnp/sdnpo022.h"
#include "tmwscl/dnp/sdnpo023.h"
#include "tmwscl/dnp/sdnpo032.h"
#include "tmwscl/dnp/sdnpo034.h"
#include "tmwscl/dnp/sdnpo042.h"
#include "tmwscl/dnp/sdnpo043.h"

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define MAX_OBJ		addrNamed
#define MAX_VAR		2

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef TMWAPPL			dnpAPP;
typedef TMWCHNL			dnpChan;
typedef TMWSESN			dnpSess;
typedef TMWTARG_CONFIG		cfgTarg;
typedef DNPCHNL_CONFIG		cfgChan;
typedef DNPTPRT_CONFIG		cfgPort;
typedef DNPLINK_CONFIG		cfgLink;
typedef TMWPHYS_CONFIG		cfgPhys;
typedef MDNPSESN_CONFIG		cfgMSes;
typedef SDNPSESN_CONFIG		cfgSSes;
typedef TMWTargConfigStruct	cfgStrt;

typedef MDNPBRM_REQ_DESC	mReqDesc;


//////////////////////////////////////////////////////////////////////////
//
//  Enumerations
//

enum Port {
	
	portTCP = 0,
	portUDP = 1,
	portIP  = 2,
	};

enum Dirty {

	dirtySys = 0,
	dirtyScl = 1,
	dirtyDat = 2,
	};

// End of File

#endif
