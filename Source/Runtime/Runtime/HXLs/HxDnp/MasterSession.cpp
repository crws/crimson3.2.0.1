
#include "Intern.hpp"

#include "MasterSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP Master Session - Device
//

// Constructor

CMasterSession::CMasterSession(IDnpChannel * pChan, WORD wDest, WORD wTO, DWORD dwLink, void * pCfg) : CSession(pChan, wDest, wTO, dwLink)
{
	mdnpsesn_initConfig(&m_Config);

	m_Config.destination = wDest;

	m_Config.source = pChan->GetSource();

	m_Config.defaultResponseTimeout = wTO;

	m_pDatabase = new CUserObjects;

	if( Open(pChan) ) {

		m_pSession->pUserData = m_pDatabase;

		mdnpdata_init(m_pSession, m_pDatabase);

		/* Let the SCL Handle Unsolicited Messages, otherwise set callback and handle !

		//mdnpsesn_setUnsolUserCallback(m_pSession, UnsolCallback, m_pSession);

		*/

		mdnpbrm_initReqDesc(&m_ReqDesc, m_pSession);

		m_ReqDesc.pUserCallback = TMWDEFS_NULL;

		m_ReqDesc.pUserCallbackParam = TMWDEFS_NULL;

		m_Config.pStatCallback = StatCallback;

		m_Config.pStatCallbackParam = this;

		m_Config.linkStatusPeriod   = dwLink;

		mdnpsesn_setSessionConfig(m_pSession, &m_Config);
	}
}

// Destructor

CMasterSession::~CMasterSession(void)
{
	Close();

	delete m_pDatabase;
}

// IUnknown

HRESULT CMasterSession::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDnpMasterSession);

	return CSession::QueryInterface(riid, ppObject);
}

ULONG CMasterSession::AddRef(void)
{
	return CSession::AddRef();
}

ULONG CMasterSession::Release(void)
{
	return CSession::Release();
}

// IDnpSession

BOOL METHOD CMasterSession::Open(IDnpChannel *pChan)
{
	m_pSession = mdnpsesn_openSession((dnpChan *) pChan->GetChannel(), &m_Config, m_pDatabase);

	return m_pSession ? TRUE : FALSE;
}

BOOL METHOD CMasterSession::Close(void)
{
	if( m_pSession ) {

		if( mdnpsesn_closeSession(m_pSession) ) {

			m_pSession = NULL;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL METHOD CMasterSession::Ping(void)
{
	InitRequest((void *) PingCallback, this);

	if( !Response(mdnpbrm_integrityPoll(&m_ReqDesc)) ) {

		return FALSE;
	}

	return TRUE;
}

UINT METHOD CMasterSession::Validate(BYTE o, WORD i, BYTE t, UINT uCount)
{
	return CSession::Validate(o, i, t, uCount);
}

UINT METHOD CMasterSession::GetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {

			CUserData * pUser = pTable->Find(i + u, t);

			if( pUser && pUser->MatchType(t) ) {

				pData = pUser->GetData(pData);

				u++;

				if( u % 100 == 0 ) {

					Sleep(10);
				}

				continue;
			}

			break;
		}

		return u ? u : CCODE_ERROR;
	}

	return CCODE_ERROR;
}

UINT METHOD CMasterSession::GetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetFlags(o, i, pData, uCount);
}

UINT METHOD CMasterSession::GetTimeStamp(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetTimeStamp(o, i, pData, uCount);
}

UINT METHOD CMasterSession::GetClass(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetClass(o, i, pData, uCount);
}

UINT METHOD CMasterSession::GetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetOnTime(o, i, pData, uCount);
}

UINT METHOD CMasterSession::GetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetOffTime(o, i, pData, uCount);
}

// IDnpMasterSession

BOOL METHOD CMasterSession::PollClass(BYTE bClass)
{
	BYTE bQual = DNPDEFS_QUAL_ALL_POINTS;

	InitRequest((void *) ReadGroupCallback, this);

	return Response(mdnpbrm_readClass(&m_ReqDesc, NULL, bQual, 0, (bClass & 1) ? TRUE : FALSE,
		(bClass & 2) ? TRUE : FALSE,
					  (bClass & 4) ? TRUE : FALSE,
					  (bClass & 8) ? TRUE : FALSE));
}

BOOL METHOD CMasterSession::Poll(BYTE bObject, WORD i, UINT uCount)
{
	BYTE bQual = DNPDEFS_QUAL_ALL_POINTS;

	CUserData * pUser = m_pDatabase->Find(bObject, i);

	if( pUser ) {

		BYTE v = pUser->GetVariation();

		InitRequest((void *) ReadGroupCallback, this);

		return Response(mdnpbrm_readGroup(&m_ReqDesc, NULL, bObject, v, bQual, i, TMWTYPES_USHORT(uCount)));
	}

	return FALSE;
}

BOOL METHOD CMasterSession::PollEvents(BYTE bObject, WORD i, UINT uCount)
{
	if( m_pDatabase ) {

		CUserTable * pTable = m_pDatabase->Find(bObject);

		if( pTable ) {

			BYTE bMask = m_pDatabase->GetUnsolicitMsgMask();

			BOOL fPoll = FALSE;

			for( UINT u = 0; u < uCount; u++ ) {

				CUserData * pUser = pTable->Find(i);

				if( pUser && !(pUser->GetClassMask() & bMask) ) {

					fPoll = TRUE;

					break;
				}
			}

			if( fPoll ) {

				InitRequest((void *) PollEventsCallback, this);

				return Response(mdnpbrm_eventPoll(&m_ReqDesc));
			}
		}
	}

	return TRUE;
}

BOOL METHOD CMasterSession::SyncTime(void)
{
	InitRequest((void *) WriteCallback, this);

	MDNPBRM_SYNC_TYPE Type = m_pChannel->GetConfig() ? MDNPBRM_SYNC_TYPE_LAN : MDNPBRM_SYNC_TYPE_SERIAL;

	return Response(mdnpbrm_timeSync(&m_ReqDesc, Type, TRUE));
}

BOOL METHOD CMasterSession::ColdRestart(void)
{
	InitRequest((void *) WriteCallback, this);

	return Response(mdnpbrm_coldRestart(&m_ReqDesc));
}

BOOL METHOD CMasterSession::WarmRestart(void)
{
	InitRequest((void *) WriteCallback, this);

	return Response(mdnpbrm_warmRestart(&m_ReqDesc));
}

BOOL METHOD CMasterSession::UnsolicitedEnable(BYTE bMask)
{
	InitRequest((void *) WriteCallback, this);

	return Response(mdnpbrm_unsolEnable(&m_ReqDesc,
		(bMask & 0x1) ? TRUE : FALSE,
					    (bMask & 0x2) ? TRUE : FALSE,
					    (bMask & 0x4) ? TRUE : FALSE));
}

BOOL METHOD CMasterSession::UnsolicitedDisable(BYTE bMask)
{
	InitRequest((void *) WriteCallback, this);

	return Response(mdnpbrm_unsolDisable(&m_ReqDesc,
		(bMask & 0x1) ? TRUE : FALSE,
					     (bMask & 0x2) ? TRUE : FALSE,
					     (bMask & 0x4) ? TRUE : FALSE));
}

UINT METHOD CMasterSession::AssignClass(BYTE bObject, WORD i, PDWORD pData, UINT uCount)
{
	UINT u;

	for( u = 0; u < uCount; u++ ) {

		CUserData * pUser = m_pDatabase->Find(bObject, i);

		if( pUser ) {

			BYTE bMask = pUser->MakeClassMask(BYTE(pData[u]));

			BYTE bQual = DNPDEFS_QUAL_16BIT_START_STOP;

			InitRequest((void *) WriteCallback, this);

			if( Response(mdnpbrm_assignClass(&m_ReqDesc,
							 NULL,
							 bMask,
							 bObject,
							 bQual,
							 TMWTYPES_USHORT(u + i),
							 TMWTYPES_USHORT(u + i),
							 NULL)) ) {

				pUser->SetClass(BYTE(pData[u]));

				continue;
			}
		}

		return u;
	}

	return u ? u : CCODE_ERROR;
}

BOOL METHOD CMasterSession::AnalogCmd(WORD i, BYTE t, PDWORD pData, UINT uCount)
{
	CUserData * pUser = m_pDatabase->Find(DNPDEFS_OBJ_40_ANA_OUT_STATUSES, i);

	if( pUser ) {

		MDNPBRM_ANALOG_INFO * info = new MDNPBRM_ANALOG_INFO[uCount];

		if( info ) {

			for( UINT u = 0; u < uCount; u++ ) {

				pUser = m_pDatabase->Find(DNPDEFS_OBJ_40_ANA_OUT_STATUSES, i + u);

				if( pUser ) {

					info[u].pointNumber = TMWTYPES_USHORT(i + u);

					pData = pUser->SetValue(&info[u].value, pData);
				}
			}

			BYTE v = pUser->GetVariation();

			InitRequest((void *) WriteCallback, this);

			if( Response(mdnpbrm_analogCommand(&m_ReqDesc,
							   NULL,
							   DNPDEFS_FC_SELECT,
							   0x3,
							   0,
							   DNPDEFS_QUAL_16BIT_INDEX,
							   TMWTYPES_UCHAR(v),
							   TMWTYPES_UCHAR(uCount),
							   info)) ) {

				delete[] info;

				return TRUE;
			}

			delete[] info;
		}
	}

	return FALSE;
}

BOOL METHOD CMasterSession::BinaryCmd(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount)
{
	MDNPBRM_CROB_INFO * info = e ? NULL : new MDNPBRM_CROB_INFO[uCount];

	CUserTable * pTable = m_pDatabase->Find(o);

	for( UINT u = 0; u < uCount; u++ ) {

		CUserData * pUser = pTable->Find(i + u, t);

		if( pData ) {

			if( !info ) {

				switch( e ) {

					case 4: pUser->SetOnTime(pData[u]);	break;
					case 5: pUser->SetOffTime(pData[u]);	break;
				}


				continue;
			}

			info[u].pointNumber = TMWTYPES_USHORT(i + u);

			pUser->SetCtrl(pData[u]);

			info[u].control = DNPDEFS_CROB_CTRL(pUser->GetCtrl());

			info[u].onTime  = pUser->GetOnTime();

			info[u].offTime = pUser->GetOffTime();
		}
	}

	if( !info ) {

		return TRUE;
	}

	InitRequest((void *) WriteCallback, this);

	if( Response(mdnpbrm_binaryCommand(&m_ReqDesc,
					   NULL,
					   DNPDEFS_FC_SELECT,
					   0x3,
					   0,
					   DNPDEFS_QUAL_16BIT_INDEX,
					   TMWTYPES_UCHAR(uCount),
					   info)) ) {

		delete[] info;

		return TRUE;
	}

	delete[] info;

	return FALSE;
}

BOOL METHOD CMasterSession::AnalogDbd(WORD i, BYTE t, PDWORD pData, UINT uCount)
{
	CUserData * pUser = m_pDatabase->Find(DNPDEFS_OBJ_34_ANA_INPUT_DBANDS, i);

	if( pUser ) {

		MDNPBRM_ANALOG_INFO * info = new MDNPBRM_ANALOG_INFO[uCount];

		if( info ) {

			for( UINT u = 0; u < uCount; u++ ) {

				pUser = m_pDatabase->Find(DNPDEFS_OBJ_34_ANA_INPUT_DBANDS, i + u);

				if( pUser ) {

					info[u].pointNumber = TMWTYPES_USHORT(i + u);

					pData = pUser->SetValue(&info[u].value, pData);
				}
			}

			BYTE v = pUser->GetVariation();

			InitRequest((void *) WriteCallback, this);

			if( Response(mdnpbrm_writeDeadband(&m_ReqDesc,
							   NULL,
							   DNPDEFS_QUAL_16BIT_INDEX,
							   v,
							   TMWTYPES_UCHAR(uCount),
							   info)) ) {

				delete[] info;

				return TRUE;
			}

			delete[] info;
		}
	}

	return FALSE;
}

BOOL METHOD CMasterSession::FreezeCtr(WORD i, PDWORD pData)
{
	if( !(pData[0] & 0x1) ) {

		return TRUE;
	}

	InitRequest((void *) WriteCallback, this);

	return Response(mdnpbrm_freezeCounters(&m_ReqDesc,
					       NULL,
					       ((pData[0] & 0x2) ? TRUE : FALSE),
					       ((pData[0] & 0x4) ? TRUE : FALSE),
					       DNPDEFS_QUAL_16BIT_START_STOP,
					       i,
					       i,
					       FALSE));
}

UINT METHOD CMasterSession::GetFeedBack(void)
{
	if( m_fFail ) {

		UINT uFeedback = m_pDatabase->GetFeedback();

		m_fFail = FALSE;

		return uFeedback;
	}

	return 0;
}

// Implementation

void CMasterSession::InitRequest(void * func, void * v)
{
	m_ReqDesc.pUserCallback = (DNPCHNL_CALLBACK_FUNC) func;

	m_ReqDesc.pUserCallbackParam = v;

	Init();
}

BOOL CMasterSession::Response(TMWSESN_TX_DATA * pData)
{
	UINT uTick = GetTickCount();

	while( !IsTimedOut(uTick, m_Config.defaultResponseTimeout) && !m_fSuccess && !m_fTimeout ) {

		m_pChannel->Service();

		Sleep(10);
	}

	return m_fSuccess && !m_fTimeout;
}

// Helpers

BOOL CMasterSession::IsTimedOut(UINT uTime, UINT uSpan)
{
	return int(GetTickCount() - uTime - ToTicks(uSpan)) >= 0;
}

// Friends

global void PingCallback(void *pVoid, DNPCHNL_RESPONSE_INFO * pResponse)
{
	if( DNP_SESS_DEBUG ) {

		AfxTrace("\nPing callback");
	}

	CMasterSession * pSession = (CMasterSession *) pVoid;

	if( pSession ) {

		pSession->Online(TRUE);
	}
}

global void ReadGroupCallback(void * pVoid, DNPCHNL_RESPONSE_INFO *pResponse)
{
	if( DNP_SESS_DEBUG ) {

		AfxTrace("\nRead Group Callback %u %8.8x", pResponse->pRxData, pVoid);
	}

	CMasterSession * pSession = (CMasterSession *) pVoid;

	if( pSession ) {

	}
}

global void PollEventsCallback(void * pVoid, DNPCHNL_RESPONSE_INFO *pResponse)
{
	if( DNP_SESS_DEBUG ) {

		AfxTrace("\nPoll Events Callback %u %8.8x", pResponse->pRxData, pVoid);
	}

	CMasterSession * pSession = (CMasterSession *) pVoid;

	if( pSession ) {

	}
}

global void WriteCallback(void * pVoid, DNPCHNL_RESPONSE_INFO * pResponse)
{
	if( DNP_SESS_DEBUG ) {

		AfxTrace("\nWrite Callback %u %8.8x", pResponse->pRxData, pVoid);
	}

	CMasterSession * pSession = (CMasterSession *) pVoid;

	if( pSession ) {

	}
}

// Friends

global void UnsolCallback(void * pVoid, MDNPSESN_UNSOL_RESP_INFO *pCall)
{
	if( DNP_SESS_DEBUG ) {

		AfxTrace("\nMaster Unsolicited Callback");
	}

// This is handled by the SCL !!!
}

// End of File
