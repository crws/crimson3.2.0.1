
#include "Intern.hpp"

#include "MasterChannel.hpp"

#include "MasterSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP Master Channel - Port
//

// Constructor

CMasterChannel::CMasterChannel(IDataHandler * pHandler, WORD wSrc) : CChannel(pHandler, wSrc)
{
	for( UINT s = 0; s < elements(m_pSession); s++ ) {

		m_pSession[s] = NULL;
	}

	m_fMaster = TRUE;
}

CMasterChannel::CMasterChannel(IDnpChannelConfig * pConfig, WORD wSrc) : CChannel(pConfig, wSrc)
{
	for( UINT s = 0; s < elements(m_pSession); s++ ) {

		m_pSession[s] = NULL;
	}

	m_fMaster = TRUE;
}

// Destructor

CMasterChannel::~CMasterChannel(void)
{
}

// IDnpChannel

BOOL  CMasterChannel::Open(void)
{
	return CChannel::Open();
}

IDnpSession *  CMasterChannel::OpenSession(WORD wDest, WORD wTO, DWORD dwLink, void * pCfg)
{
	CMasterSession * pSession = New CMasterSession(this, wDest, wTO, dwLink, pCfg);

	if( pSession ) {

		if( RecordSession(pSession) ) {

			return (IDnpMasterSession *) pSession;
		}

		delete pSession;
	}

	return NULL;
}

// End of File

