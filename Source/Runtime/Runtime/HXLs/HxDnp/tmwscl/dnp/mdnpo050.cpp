/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mdnpo050.c
 * description: DNP Master functionality for Object 50 Time and Date
 */
#include "intern.hpp"
#include "dnpdiag.h"
#include "mdnpo050.h"
#include "mdnpdata.h"
#include "mdnpdiag.h"
#include "dnpdtime.h"

#include "tmwdb.h"
#include "tmwtarg.h"

#if MDNPDATA_SUPPORT_OBJ50

#if TMWCNFG_SUPPORT_ASYNCH_DB
typedef struct MDNPO50DataStruct {
  TMWDB_DATA tmw;
  TMWDTIME timeStamp;
} MDNPO50_DATA;

/* function: _dbStoreFunc */
static TMWTYPES_BOOL TMWDEFS_LOCAL _dbStoreFunc(
  TMWDB_DATA *pData)
{
  MDNPO50_DATA *pDNPData = (MDNPO50_DATA *)pData;
  mdnpdata_storeReadTime(pData->pDbHandle, &pDNPData->timeStamp);
  return(TMWDEFS_TRUE);
}

/* function: _storeReadTime */
static void TMWDEFS_LOCAL _storeReadTime(
  void *pDbHandle, 
  TMWDTIME *pTimeStamp)
{
  /* Allocate new time data structure */
  MDNPO50_DATA *pDNPData = (MDNPO50_DATA *)tmwtarg_alloc(sizeof(MDNPO50_DATA));

  /* Initialize data indepdendent parts */
  tmwdb_initData((TMWDB_DATA *)pDNPData, pDbHandle, _dbStoreFunc);

  /* Initialize data dependent parts */
  pDNPData->timeStamp = *pTimeStamp;

  /* Store in database queue */
  if(!tmwdb_addEntry((TMWDB_DATA *)pDNPData))
    tmwtarg_free(pDNPData);
}
#else
/* function: _storeReadTime */
static void TMWDEFS_LOCAL _storeReadTime(
  void *pDbHandle, 
  TMWDTIME *pTimeStamp)
{
  mdnpdata_storeReadTime(pDbHandle, pTimeStamp);
}
#endif /* TMWCNFG_SUPPORT_ASYNCH_DB */


/* function: mdnpo050_readObj50v1 */
TMWTYPES_BOOL TMWDEFS_GLOBAL mdnpo050_readObj50v1(
  TMWSESN *pSession,
  DNPUTIL_RX_MSG *pRxFragment,
  DNPUTIL_OBJECT_HEADER *pObjHeader)
{
  TMWTYPES_MS_SINCE_70 msSince70;
  TMWDTIME dateTime;
  MDNPSESN *pMDNPSession = (MDNPSESN *)pSession;

  /* Verify qualifier is 8 bit limited, which is the only one allowed */
  if(pObjHeader->qualifier != DNPDEFS_QUAL_8BIT_LIMITED_QTY)
  {
    MDNPDIAG_ERROR(pSession->pChannel, pSession, MDNPDIAG_INV_QUALIFIER);
    return(TMWDEFS_FALSE);
  }

  dnpdtime_readMsSince70(&msSince70, pRxFragment->pMsgBuf + pRxFragment->offset);
  pRxFragment->offset += 6;

  dnpdtime_msSince70ToDateTime(&dateTime, &msSince70);

  DNPDIAG_SHOW_TIME_AND_DATE(pSession, &dateTime, TMWDEFS_NULL);

  _storeReadTime(pMDNPSession->pDbHandle, &dateTime);

  return(TMWDEFS_TRUE);
}
#endif /* MDNPDATA_SUPPORT_OBJ50 */
