
#include "Intern.hpp"

#include "SlaveChannel.hpp"

#include "SlaveSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP Slave Channel - Port
//

// Constructor

CSlaveChannel::CSlaveChannel(IDataHandler * pHandler, WORD wSrc) : CChannel(pHandler, wSrc)
{
	m_fMaster = FALSE;

	m_pSession[0] = NULL;
}

CSlaveChannel::CSlaveChannel(IDnpChannelConfig * pConfig, WORD wSrc) : CChannel(pConfig, wSrc)
{
	m_fMaster = FALSE;

	m_pSession[0] = NULL;
}

// Destructor

CSlaveChannel::~CSlaveChannel(void)
{
}

// IDnpChannel

IDnpSession * METHOD CSlaveChannel::OpenSession(WORD wDest, WORD wTO, DWORD dwLink, void * pCfg)
{
	CSlaveSession * pSession = New CSlaveSession(this, wDest, wTO, dwLink, pCfg);

	if( pSession ) {

		if( RecordSession(pSession) ) {

			pSession->Online(TRUE);

			return (IDnpSlaveSession *) pSession;
		}

		delete pSession;
	}

	return NULL;
}

// End of File

