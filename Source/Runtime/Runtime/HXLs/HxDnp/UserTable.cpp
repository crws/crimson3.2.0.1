#include "Intern.hpp"

#include "UserTable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP User List
//

// Constructor

CUserTable::CUserTable(BYTE bObject, BYTE bMode, ITimeZone * pZone)
{
	m_pHead   = NULL;

	m_pTail   = NULL;

	m_uCount  = 0;

	m_bObject = bObject;

	m_bMode	  = bMode;

	m_pZone   = pZone;
}

// Destructor

CUserTable::~CUserTable(void)
{
	Free();
}

// Attributes

CUserData * CUserTable::GetHead(void) const
{
	return m_pHead;
}

CUserData * CUserTable::GetTail(void) const
{
	return m_pTail;
}

UINT CUserTable::GetCount(void) const
{
	return m_uCount;
}

BYTE CUserTable::GetMode(void) const
{
	return m_bMode;
}

// Managment

void CUserTable::Clear(void)
{
	Free();

	m_pHead  = NULL;

	m_pTail  = NULL;

	m_uCount = 0;
}

CUserData * CUserTable::Append(CUserData *pData)
{
	m_uCount = max(m_uCount + 1, pData->GetIndex() + 1);

	pData->SetList(this);

	AfxListAppend(m_pHead, m_pTail, pData, m_pNext, m_pPrev);

	return pData;
}

CUserData * CUserTable::Remove(CUserData *pData)
{
	m_uCount = max(m_uCount - 1, pData->GetIndex() - 1);

	pData->SetList(NULL);

	AfxListRemove(m_pHead, m_pTail, pData, m_pNext, m_pPrev);

	return pData;
}

CUserData * CUserTable::Insert(CUserData *pData, CUserData * pScan)
{
	m_uCount = max(m_uCount + 1, pData->GetIndex() + 1);

	pData->SetList(this);

	AfxListInsert(m_pHead, m_pTail, pData, m_pNext, m_pPrev, pScan);

	return pData;
}

CUserData * CUserTable::Find(UINT uIndex, BYTE bType, BOOL fAppend)
{
	return FindNext(m_pHead, uIndex, bType, fAppend);
}

CUserData * CUserTable::FindNext(CUserData * pScan, UINT uIndex, BYTE bType, BOOL fAppend)
{
	CUserData * pData = NULL;

	while( pScan ) {

		if( pScan->m_uIndex == uIndex ) {

			if( bType != addrLongAsLong ) {

				pScan->m_Type = bType;
			}

			return pScan;
		}

		if( pScan->m_uIndex > uIndex ) {

			pData = new CUserData(m_bObject, uIndex, bType, m_bMode, m_pZone);

			if( pData ) {

				Insert(pData, pScan);

				PrintTable();
			}

			return pData;
		}

		pScan = pScan->m_pNext;
	}

	if( fAppend ) {

		pData = new CUserData(m_bObject, uIndex, bType, m_bMode, m_pZone);

		if( pData ) {

			Append(pData);

			PrintTable();
		}

		return pData;
	}

	return NULL;
}

// Implementation

void CUserTable::Free(void)
{
	CUserData *pData = m_pHead;

	while( pData ) {

		CUserData *pNext = pData->GetNext();

		delete pData;

		pData = pNext;
	}
}

// Debugging Help

void CUserTable::PrintTable(void)
{
	CUserData * pData = m_pHead;

	UINT u = 0;

	while( pData ) {

		pData->PrintData();

		pData = pData->GetNext();

		u++;
	}
}

// End of File
