
#include "Intern.hpp"

#include "Channel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP Channel - Port
//

// Constructor

CChannel::CChannel(IDataHandler * pHandler, WORD wSrc)
{
	StdSetRef();

	InitChannel(wSrc);

	m_pConfig->m_pIO = pHandler;

	InitConfig();
}

CChannel::CChannel(IDnpChannelConfig * pConfig, WORD wSrc)
{
	StdSetRef();

	InitChannel(wSrc);

	m_pConfig->m_pIO = pConfig;

	InitConfig();

	SetNetwork();
}

// Destructor

CChannel::~CChannel(void)
{
	delete m_pConfig;

	delete[] m_pSessions;

	m_pEthernet->Release();

	m_pSerial->Release();
}

// IUnknown

HRESULT CChannel::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDnpChannel);

	StdQueryInterface(IDnpChannel);

	return E_NOINTERFACE;
}

ULONG CChannel::AddRef(void)
{
	StdAddRef();
}

ULONG CChannel::Release(void)
{
	StdRelease();
}

// IDnpChannel 

BOOL METHOD CChannel::Open(void)
{
	if( m_pApp ) {

		m_pChannel = dnpchnl_openChannel(m_pApp, &m_pConfig->m_Cfg,
						 &m_pConfig->m_Port,
						 &m_pConfig->m_Link,
						 &m_pConfig->m_Phys,
						 this,
						 &m_pConfig->m_Strt);

		if( m_pChannel ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL METHOD CChannel::Close(void)
{
	if( m_pChannel ) {

		if( dnpchnl_closeChannel(m_pChannel) ) {

			CloseSocket(FALSE, portTCP);

			CloseSocket(FALSE, portUDP);

			m_pChannel = NULL;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

IDnpSession * METHOD CChannel::OpenSession(WORD wDest, WORD wTO, DWORD dwLink, void * pCfg)
{
	return NULL;
}

BOOL METHOD CChannel::CloseSession(WORD wDest)
{
	return FALSE;
}

BOOL METHOD CChannel::CloseSessions(void)
{
	for( BYTE s = 0; s < m_bSess; s++ ) {

		if( m_pSessions[s] ) {

			m_pSessions[s]->Close();

			m_pSessions[s] = NULL;
		}
	}

	return TRUE;
}

BOOL METHOD CChannel::EnableSessions(BOOL fEnable)
{
	if( !fEnable ) {

		for( BYTE s = 0; s < m_bSess; s++ ) {

			if( m_pSessions[s] ) {

				m_pSessions[s]->Online(FALSE);

				m_pSessions[s]->Init();
			}
		}
	}

	return TRUE;
}

WORD METHOD CChannel::GetSource(void)
{
	return m_wSrc;
}

IDnpChannelConfig * CChannel::GetConfig(void)
{
	if( m_pConfig ) {

		return (IDnpChannelConfig *) m_pConfig->m_pIO;
	}

	return NULL;
}

void * METHOD CChannel::GetChannel(void)
{
	return m_pChannel;
}

void METHOD CChannel::Service(void)
{
	tmwappl_checkForInput(m_pApp);

	tmwpltmr_checkTimer();
}

void METHOD CChannel::Share(BOOL fOpen, UINT uBackOff)
{
	if( fOpen ) {

		UINT dt = GetTickCount() - m_uLast[portTCP];

		UINT tt = ToTicks(uBackOff);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
		}

		return;
	}

	m_pEthernet->Wait(FOREVER);

	CloseSocket(FALSE, portTCP);

	m_pEthernet->Free();
}

// Operations

BOOL CChannel::MatchChannel(void * pIO, WORD wSrc, BOOL fMaster)
{
	IDnpChannelConfig * pConfig = GetConfig();

	if( pConfig ) {

		return (pConfig == pIO && m_wSrc == wSrc && m_fMaster == fMaster);
	}

	return FALSE;
}

void CChannel::EnableChannel(BOOL fEnable)
{
	m_fEnable = fEnable;
}

WORD CChannel::RxData(dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst)
{
	if( IsNetwork() ) {

		return RxEthernet(pBuff, Bytes, Time, pTO, pFirst);
	}

	return RxSerial(pBuff, Bytes, Time, pTO, pFirst);
}

BOOL CChannel::TxData(dnpUCHAR *pBuff, dnpWORD Bytes)
{
	if( IsNetwork() ) {

		return TxEthernet(pBuff, Bytes);
	}

	return TxSerial(pBuff, Bytes);
}

BOOL CChannel::TxUDP(dnpUCHAR Port, dnpUCHAR *pBuff, dnpWORD Bytes)
{
	return TxEthernet(pBuff, Bytes, portUDP);
}

WORD CChannel::GetTxReady(void)
{
	IMutex * pMutex  = IsNetwork() ? m_pEthernet : m_pSerial;

	if( pMutex ) {

		if( pMutex->Wait(0) ) {

			pMutex->Free();

			return 0;
		}
	}

	return 100;
}

BOOL CChannel::IsMaster(void)
{
	return m_fMaster;
}

// Implementation

void CChannel::InitChannel(WORD wSrc)
{
	m_wSrc		 = wSrc;

	m_pConfig	 = new Channel;

	m_pChannel	 = NULL;

	m_pApp		 = NULL;

	m_bSess		 = 0;

	m_pSessions	 = NULL;

	m_fEnable	 = FALSE;

	m_pSock[portTCP] = NULL;

	m_pSock[portUDP] = NULL;

	m_uLast[portTCP] = GetTickCount();

	m_uLast[portUDP] = GetTickCount();

	m_pEthernet	 = Create_Mutex();

	m_pSerial	 = Create_Mutex();
}

BOOL CChannel::InitConfig(void)
{
	m_pApp = tmwappl_initApplication();

	if( m_pApp ) {

		tmwtarg_initConfig(&m_Target);

		dnpchnl_initConfig(&m_pConfig->m_Cfg,
				   &m_pConfig->m_Port,
				   &m_pConfig->m_Link,
				   &m_pConfig->m_Phys);

		return TRUE;
	}

	return FALSE;
}

BOOL CChannel::RecordSession(CSession * pSess)
{
	BYTE bOld = m_bSess;

	BYTE bNew = bOld + 1;

	CSession ** pOld = m_pSessions;

	CSession ** pNew = new CSession *[bNew];

	for( BYTE c = 0; c < bNew; c++ ) {

		if( c == bOld ) {

			pNew[c] = pSess;

			break;
		}

		pNew[c] = pOld[c];
	}

	m_pSessions = pNew;

	m_bSess = bNew;

	delete[] pOld;

	return TRUE;
}

WORD CChannel::RxSerial(dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst)
{
	UINT uPtr  = 0;

	UINT uTimer = 0;

	if( m_pConfig && m_pSerial->Wait(0) ) {

		IDataHandler * pData = (IDataHandler *) m_pConfig->m_pIO;

		if( pData ) {

			UINT uByte = 0;

			UINT uTime = pTO && *pTO ? Time : 100;

			uTimer = uTime;

			SetTimer(uTime);

			while( GetTimer() ) {

				if( (uByte = pData->Read(uTime)) < NOTHING ) {

					pBuff[uPtr] = dnpUCHAR(uByte);

					uPtr++;

					if( uPtr == Bytes ) {

						break;
					}

					if( !pTO ) {

						SetTimer(uTime);

						uTimer += uTime;
					}

					continue;
				}

				Sleep(10);
			}
		}

		m_pSerial->Free();
	}

	return WORD(uPtr);
}

BOOL CChannel::TxSerial(dnpUCHAR *pBuff, dnpWORD Bytes)
{
	if( m_pConfig && m_pSerial->Wait(1000) ) {

		IDataHandler * pData = (IDataHandler *) m_pConfig->m_pIO;

		if( pData ) {

			UINT uWrite = pData->Write(pBuff, Bytes, FOREVER);

			if( uWrite == Bytes ) {

				m_pSerial->Free();

				return TRUE;
			}
		}

		m_pSerial->Free();
	}

	return FALSE;
}

WORD CChannel::RxEthernet(dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst)
{
	UINT uPtr  = 0;

	for( UINT u = portTCP; u < portIP && uPtr == 0; u++ ) {

		if( m_pEthernet->Wait(0) ) {

			if( OpenSocket(BYTE(u), TRUE) ) {

				ISocket * pSock = m_pSock[u];

				if( pSock ) {

					SetTimer(100);

					while( GetTimer() ) {

						UINT uSize = Bytes - uPtr;

						if( pSock->Recv(pBuff + uPtr, uSize) == S_OK ) {

							if( uSize ) {

								uPtr += uSize;

								if( uPtr == Bytes ) {

									if( CheckRemote(BYTE(u)) ) {

										m_uLast[u] = GetTickCount();

										m_pEthernet->Free();

										return WORD(uPtr);
									}

									m_pEthernet->Free();

									return 0;
								}
							}

							SetTimer(100);

							continue;
						}

						if( !CheckSocket(BYTE(u)) ) {

							SetFail();

							break;
						}

						Sleep(10);
					}
				}
			}

			m_pEthernet->Free();
		}
	}

	return WORD(uPtr);
}

BOOL CChannel::TxEthernet(dnpUCHAR *pBuff, dnpWORD Bytes, BYTE bEnum)
{
	if( m_pEthernet->Wait(1000) ) {

		CBuffer * pBuffer = BuffAllocate(Bytes);

		if( pBuffer ) {

			PBYTE pData = pBuffer->AddTail(Bytes);

			memcpy(pData, pBuff, Bytes);

			for( UINT u = bEnum; u < portIP; u++ ) {

				if( OpenSocket(BYTE(u)) ) {

					ISocket * pSock = m_pSock[u];

					if( pSock ) {

						SetTimer(1000);

						while( GetTimer() ) {

							if( pSock->Send(pBuffer) == S_OK ) {

								m_uLast[u] = GetTickCount();

								m_pEthernet->Free();

								return TRUE;
							}
							else {
								UINT Phase = 0;

								pSock->GetPhase(Phase);

								if( Phase == PHASE_OPEN ) {

									Sleep(5);

									continue;
								}

								break;
							}
						}
					}
				}
			}

			BuffRelease(pBuffer);
		}

		m_pEthernet->Free();
	}

	return FALSE;
}

void CChannel::SetFail(void)
{
	IDnpChannelConfig * pConfig = GetConfig();

	if( pConfig ) {

		pConfig->SetFail();
	}
}

// Socket Management

ISocket * CChannel::CreateSocket(BYTE bEnum)
{
	ISocket *pSock = NULL;

	if( bEnum == portTCP ) {

		AfxNewObject("sock-tcp", ISocket, pSock);
	}

	if( bEnum == portUDP ) {

		AfxNewObject("sock-udp", ISocket, pSock);
	}

	AfxAssert(pSock);

	return pSock;
}

BOOL CChannel::CheckSocket(BYTE bEnum)
{
	if( m_pSock[bEnum] ) {

		UINT Phase;

		m_pSock[bEnum]->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE, bEnum);

			return FALSE;
		}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE, bEnum);

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CChannel::OpenSocket(BYTE bEnum, BOOL fRx)
{
	if( CheckSocket(bEnum) ) {

		if( IsActive(bEnum) ) {

			return TRUE;
		}

		CloseSocket(TRUE, bEnum);
	}

	if( IsTCP(bEnum) ) {

		return OpenTCP();
	}

	if( IsUDP(bEnum) ) {

		return OpenUDP();
	}

	return FALSE;
}

BOOL CChannel::OpenTCP(void)
{
	IDnpChannelConfig * pConfig = GetConfig();

	if( pConfig ) {

		if( IsOnlyUDP() ) {

			return FALSE;
		}

		if( (m_pSock[portTCP] = CreateSocket(portTCP)) ) {

			WORD Port = pConfig->GetTcpPort();

			if( m_fMaster ) {

				IPADDR IP = pConfig->GetIP();

				if( m_pSock[portTCP]->Connect(IP, Port) == S_OK ) {

					SetTimer(1000);

					while( GetTimer() ) {

						UINT Phase;

						m_pSock[portTCP]->GetPhase(Phase);

						if( Phase == PHASE_OPEN ) {

							m_uLast[portTCP] = GetTickCount();

							return TRUE;
						}

						if( Phase == PHASE_CLOSING ) {

							m_pSock[portTCP]->Close();
						}

						if( Phase == PHASE_ERROR ) {

							break;
						}

						Sleep(10);
					}

					CloseSocket(TRUE, portTCP);
				}
			}
			else {
				m_pSock[portTCP]->Listen(Port);

				m_uLast[portTCP] = GetTickCount();

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CChannel::OpenUDP(void)
{
	IDnpChannelConfig * pConfig = GetConfig();

	if( pConfig ) {

		if( IsOnlyTCP() ) {

			return FALSE;
		}

		if( !m_pSock[portUDP] ) {

			if( (m_pSock[portUDP] = Create_DnpUdpSocket(GetConfig())) ) {

				m_pSock[portUDP]->SetOption(OPT_ADDRESS, 1);

				m_pSock[portUDP]->SetOption(OPT_RECV_QUEUE, 12);

				m_pSock[portUDP]->Listen(pConfig->GetUdpPort());

				m_uLast[portUDP] = GetTickCount();

				return TRUE;
			}

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

void CChannel::CloseSocket(BOOL fAbort, BYTE bEnum)
{
	if( m_pSock[bEnum] ) {

		if( fAbort )
			m_pSock[bEnum]->Abort();
		else
			m_pSock[bEnum]->Close();

		m_pSock[bEnum]->Release();

		m_pSock[bEnum] = NULL;
	}
}

void CChannel::SetNetwork(void)
{
	IDnpChannelConfig * pConfig = GetConfig();

	if( pConfig ) {

		UINT uTCP = pConfig->GetTcpPort();

		UINT uUDP = pConfig->GetUdpPort();

		if( uTCP == 0 ) {

			m_pConfig->m_Link.networkType = DNPLINK_NETWORK_UDP_ONLY;
		}

		else if( uUDP == 0 ) {

			m_pConfig->m_Link.networkType = DNPLINK_NETWORK_TCP_ONLY;
		}

		else {
			m_pConfig->m_Link.networkType = DNPLINK_NETWORK_TCP_UDP;
		}
	}
}

BOOL CChannel::IsActive(BYTE bEnum)
{
	if( IsNetwork() && IsTCP(bEnum) ) {

		IDnpChannelConfig * pConfig = GetConfig();

		if( pConfig ) {

			UINT uTick = GetTickCount();

			UINT uTO   = pConfig->GetConnectionTimeout();

			if( int(uTick - m_uLast[bEnum] - ToTicks(uTO)) >= 0 ) {

				return FALSE;
			}
		}
	}

	return TRUE;
}

BOOL CChannel::IsNetwork(void)
{
	if( m_pConfig ) {

		return  m_pConfig->m_Link.networkType == DNPLINK_NETWORK_TCP_UDP	||
			m_pConfig->m_Link.networkType == DNPLINK_NETWORK_UDP_ONLY	||
			m_pConfig->m_Link.networkType == DNPLINK_NETWORK_TCP_ONLY;
	}

	return FALSE;
}

BOOL CChannel::IsTCP(BYTE bEnum)
{
	return bEnum == portTCP;
}

BOOL CChannel::IsUDP(BYTE bEnum)
{
	return bEnum == portUDP;
}

BOOL CChannel::IsOnlyTCP(void)
{
	return m_pConfig->m_Link.networkType == DNPLINK_NETWORK_TCP_ONLY;
}

BOOL CChannel::IsOnlyUDP(void)
{
	return m_pConfig->m_Link.networkType == DNPLINK_NETWORK_UDP_ONLY;
}

BOOL CChannel::CheckRemote(BYTE bEnum)
{
	IDnpChannelConfig * pConfig = GetConfig();

	if( pConfig ) {

		CIpAddr IP;

		ISocket * pSock = m_pSock[bEnum];

		if( pSock ) {

			if( pSock->GetRemote(IP) == S_OK ) {

				if( IP == pConfig->GetIP() ) {

					return TRUE;
				}

				SetFail();
			}
		}
	}

	return FALSE;
}

// End of File

