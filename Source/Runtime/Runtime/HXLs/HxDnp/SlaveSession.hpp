
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SlaveSession_HPP

#define	INCLUDE_SlaveSession_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "Session.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Slave Structures
//

struct CEventConfig {

	BYTE m_bMode;
	WORD m_wLimit;
	};

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Slave Enumerations
//

enum EventDef {

	eventBI,
	eventDBI,
	eventBO,
	eventC,
	eventFC,
	eventAI,
	eventAO,
	eventTotal,
	};

enum EventMask {

	maskAiCalc = 1,
	};

//////////////////////////////////////////////////////////////////////////
//
// Slave Session Object - Device
//

class CSlaveSession : public CSession, public IDnpSlaveSession
{
	public:
		// Constructor
		CSlaveSession(IDnpChannel * pChan, WORD wDest, WORD wTO, DWORD dwLink, void * pCfg);

		// Destructor
		~CSlaveSession(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnpSession
		BOOL METHOD Open(IDnpChannel * pChan);
		BOOL METHOD Close(void);
		BOOL METHOD Ping(void);
		UINT METHOD Validate(BYTE o, WORD i, BYTE t, UINT uCount);
		UINT METHOD GetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount);
		UINT METHOD GetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetTimeStamp(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetClass(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount);

		// IDnpSlaveSession
		UINT METHOD SetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount, BYTE eMask);
		UINT METHOD SetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount, BYTE eMask);
		UINT METHOD SetClass(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD SetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD SetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount);
				
	protected:
		// Data Members
		BYTE    m_bAiCalc;
		cfgSSes m_Config;

		// Implementation
		void SetEventConfig(CEventConfig * pCfg);
		BOOL IsReadOnly(BYTE bObject, BYTE e);
		BOOL IsWriteOnly(BYTE bObject, BYTE e);
		BOOL AddEvent(CUserData * pUser, PDWORD pData, BOOL fValue, dnpTIME * pTime, BYTE eMask);
		
		// Event Help
		BOOL IsAnalog (BYTE bObject);
		BOOL IsCounter(BYTE bObject);
		BOOL IsBinary (BYTE bObject);
		BOOL GetDeadband(BYTE bObject, UINT uIndex, BYTE bType, dnpANA * pDeadband);

		// Helpers
		BOOL SendUnsolicited(CUserData * pUser, BYTE eMask);
		BYTE GetObject(UINT uEvent);
	};

// End of File

#endif
