
#include "intern.hpp"

#include "Dnp3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// CDnp3 Object Creation
//

// Instantiator

global IDnp * Create_Dnp3(void)
{
	CDnp3 *p = New CDnp3;

	return p;
}

// Static Data

CDnp3 * CDnp3::m_pThis = NULL;

// Constructor

CDnp3::CDnp3(void)
{
	StdSetRef();

	m_pThis     = this;

	m_fInit     = FALSE;

	m_bChan	    = 0;

	m_pChannels = NULL;

	//InitLocks();
}

// Destructor

CDnp3::~CDnp3(void)
{
	m_pThis = NULL;

	delete[] m_pChannels;
}

// Item Location

CDnp3 * CDnp3::Locate(void)
{
	return m_pThis;
}

// IUnknown

HRESULT CDnp3::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDnp);

	StdQueryInterface(IDnp);

	return E_NOINTERFACE;
}

ULONG CDnp3::AddRef(void)
{
	StdAddRef();
}

ULONG CDnp3::Release(void)
{
	StdRelease();
}

// IDnp

IDnpChannelConfig * METHOD CDnp3::GetConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO)
{
	return New CChannelConfig(IP, IP2, wTcp, wUdp, wTO);
}

BOOL METHOD CDnp3::Init(void)
{
	Sys_Init();

	tmwtimer_initialize();

	return TRUE;
}

void METHOD CDnp3::Service(void)
{
}

IDnpChannel * METHOD CDnp3::OpenChannel(IDataHandler *pHandler, WORD wSrc, BOOL fMaster)
{
	CChannel * pChan = FindChannel(pHandler, wSrc, fMaster);

	if( !pChan ) {

		pChan = MakeChannel(pHandler, wSrc, fMaster);
	}

	if( pChan ) {

		if( pChan->Open() ) {

			return (IDnpChannel *) pChan;
		}

		delete pChan;
	}

	return NULL;
}

IDnpChannel * METHOD CDnp3::OpenChannel(IDnpChannelConfig *pConfig, WORD wSrc, BOOL fMaster)
{
	CChannel * pChan = FindChannel(pConfig, wSrc, fMaster);

	if( !pChan ) {

		pChan = MakeChannel(pConfig, wSrc, fMaster);
	}

	if( pChan ) {

		if( pChan->Open() ) {

			return (IDnpChannel *) pChan;
		}

		delete pChan;
	}

	return NULL;
}

BOOL METHOD CDnp3::CloseChannel(void *pVoid, WORD wSrc, BOOL fMaster)
{
	CChannel * pChan = FindChannel(pVoid, wSrc, fMaster);

	if( pChan ) {

		return pChan->Close();
	}

	return TRUE;
}

IDnpSession * METHOD CDnp3::OpenSession(IDnpChannel *pChannel, WORD wDest, WORD wTO, DWORD dwLink, void *pCfg)
{
	if( pChannel ) {

		return pChannel->OpenSession(wDest, wTO, dwLink, pCfg);
	}

	return NULL;
}

BOOL METHOD CDnp3::CloseSession(IDnpSession *pSession)
{
	if( pSession ) {

		return pSession->Close();
	}

	return TRUE;
}

void METHOD CDnp3::SetColdStart(void)
{
	SystemReboot();
}


void METHOD CDnp3::SetWarmStart(void)
{
	if( m_pChannels ) {

		for( BYTE c = 0; c < m_bChan; c++ ) {

			m_pChannels[c]->EnableSessions(FALSE);
		}
	}
}

void METHOD CDnp3::InitLock(void *pLock)
{
	if( FindLock(pLock) ) {

		return;
	}

	for( UINT u = 0; u < elements(m_pLock); u++ ) {

		if( !m_pLock[u].m_pLock ) {

			m_pLock[u].m_pLock = pLock;

			m_pLock[u].m_pMutex->Free();

			return;
		}
	}
}

void METHOD CDnp3::Lock(void *pLock)
{
	LOCK * pWait = FindLock(pLock);

	if( pWait ) {

		pWait->m_pMutex->Wait(FOREVER);
	}
}

void METHOD CDnp3::Free(void *pLock)
{
	LOCK * pFree = FindLock(pLock);

	if( pFree ) {

		pFree->m_pMutex->Free();
	}
}

void METHOD CDnp3::DeleteLock(void *pLock)
{
	LOCK * pDel = FindLock(pLock);

	if( pDel ) {

		pDel->m_pLock = NULL;

		pDel->m_pMutex->Free();
	}
}

// Implementation

CChannel * CDnp3::MakeChannel(IDataHandler * pHand, WORD wSrc, BOOL fMaster)
{
	CChannel * pChan = NULL;

	if( fMaster ) {

		pChan = new CMasterChannel(pHand, wSrc);
	}
	else {
		pChan = new CSlaveChannel(pHand, wSrc);
	}

	if( pChan ) {

		RecordChannel(pChan);
	}

	return pChan;
}

CChannel * CDnp3::MakeChannel(IDnpChannelConfig * pConfig, WORD wSrc, BOOL fMaster)
{
	CChannel * pChan = NULL;

	if( fMaster ) {

		pChan = new CMasterChannel(pConfig, wSrc);
	}
	else {
		pChan = new CSlaveChannel(pConfig, wSrc);
	}

	if( pChan ) {

		RecordChannel(pChan);
	}

	return pChan;
}

BOOL CDnp3::RecordChannel(CChannel * pChan)
{
	BYTE bOld = m_bChan;

	BYTE bNew = bOld + 1;

	CChannel ** pOld = m_pChannels;

	CChannel ** pNew = new CChannel *[bNew];

	for( BYTE c = 0; c < bNew; c++ ) {

		if( c == bOld ) {

			pNew[c] = pChan;

			break;
		}

		pNew[c] = pOld[c];
	}

	m_pChannels = pNew;

	m_bChan = bNew;

	delete[] pOld;

	return TRUE;
}

CChannel * CDnp3::FindChannel(void * pVoid, WORD wSrc, BOOL fMaster)
{
	for( BYTE c = 0; c < m_bChan; c++ ) {

		CChannel * pChan = m_pChannels[c];

		if( pChan ) {

			if( pChan->MatchChannel(pVoid, wSrc, fMaster) ) {

				return m_pChannels[c];
			}
		}
	}

	return NULL;
}

// Helpers

void CDnp3::InitLocks(void)
{
	for( UINT u = 0; u < elements(m_pLock); u++ ) {

		m_pLock[u].m_pLock  = NULL;

		m_pLock[u].m_pMutex = Create_Mutex();
	}
}

LOCK * CDnp3::FindLock(void * pLock)
{
	for( UINT u = 0; u < elements(m_pLock); u++ ) {

		if( m_pLock[u].m_pLock == pLock ) {

			return &m_pLock[u];
		}
	}

	return NULL;
}

// End of File
