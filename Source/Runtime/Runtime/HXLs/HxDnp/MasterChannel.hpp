
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MasterChannel_HPP

#define	INCLUDE_MasterChannel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "Channel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Master Channel Object - Port
//

class CMasterChannel : public CChannel
{
	public:
		// Constructor
		CMasterChannel(IDataHandler * pHandler, WORD wSrc);
		CMasterChannel(IDnpChannelConfig * pConfig, WORD wSrc);

		// Destructor
		~CMasterChannel(void);

		// IDnpChannel
		BOOL           METHOD Open(void);
		IDnpSession *  METHOD OpenSession(WORD wDest, WORD wTO, DWORD dwLink, void * pCfg);

	protected:
		// Data Members
		cfgMSes * m_pSession[64];
};

// End of File

#endif
