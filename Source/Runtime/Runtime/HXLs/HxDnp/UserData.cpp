
#include "Intern.hpp"

#include "UserData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Macros

#define ABS(x, y) (Max(x, y) - Min(x, y))

//////////////////////////////////////////////////////////////////////////
//
// DNP User Data 
//

// Constructor

CUserData::CUserData(BYTE bObject, UINT uIndex, BYTE bType, BYTE bMode, ITimeZone * pZone)
{
	m_bObject = bObject;

	m_uIndex  = uIndex;

	m_bMode   = bMode;

	for( UINT u = 0; u < elements(m_Value); u++ ) {

		m_Value[u] = 0;

		m_Event[u] = NOTHING;
	}

	m_bStatus    = 0;

	m_TimeStamp  = 0;

	m_uLast	     = 0;

	m_Ctrl	     = 0;

	m_On	     = 0;

	m_Off	     = 0;

	m_Type	     = bType;

	m_IIN	     = NOTHING;

	m_bClass     = 0;

	m_bDataType  = 0;

	m_pList	     = NULL;

	m_pPrev	     = NULL;

	m_pNext	     = NULL;

	m_pMutex     = Create_Mutex();

	m_pZone	    = pZone;

	m_fDirty[dirtySys] = FALSE;

	m_fDirty[dirtyScl] = FALSE;
}
	
// Destructor

CUserData::~CUserData(void)
{
	m_pMutex->Release();
}


// Access

BOOL CUserData::SetList(CUserTable * pList)
{
	if( !m_pList ) {

		m_pList = pList;

		return TRUE;
	}

	return FALSE;
}

CUserData * CUserData::GetNext(void) const
{
	return m_pNext;
}


CUserData * CUserData::GetPrev(void) const
{
	return m_pPrev;
}

// Data Access

BYTE CUserData::GetClass(void)
{
	return m_bClass;
}

BYTE CUserData::GetClassMask(void)
{
	return MakeClassMask(m_bClass);
}

BYTE CUserData::MakeClassMask(BYTE bClass)
{
	BYTE bMask = bClass;

	if( bClass == 3 ) {

		bMask = 4;
	}

	MakeMin(bMask, 4);

	return bMask;
}

BOOL CUserData::SetClass(BYTE bClass)
{
	if( bClass == 4 ) {

		bClass = 3;
	}

	if( bClass < 4 ) {

		m_pMutex->Wait(FOREVER);

		m_bClass = bClass;

		m_pMutex->Free();

		return TRUE;
	}

	return FALSE;
}

BYTE CUserData::GetObject(void)
{
	return m_bObject;
}

BOOL CUserData::IsDirty(BYTE bEnum)
{
	m_pMutex->Wait(FOREVER);

	if( m_fDirty[bEnum] ) {

		m_fDirty[bEnum] = FALSE;

		m_pMutex->Free();

		return TRUE;
	}

	m_pMutex->Free();

	return FALSE;
}

DWORD CUserData::GetTimeStamp(void)
{
	return m_TimeStamp;
}

BYTE CUserData::GetFlags(void)
{
	return m_bStatus | DNPDEFS_DBAS_FLAG_ON_LINE;
}

BYTE CUserData::GetEventFlags(void)
{
	return m_bStatus;
}

UINT CUserData::GetIndex(void)
{
	return m_uIndex;
}

BYTE CUserData::GetMode(void)
{
	if( IsEvent() ) {

		return m_bMode;
	}

	return 0;
}

BYTE CUserData::GetType(void)
{
	return m_Type;
}

BOOL CUserData::IsEvent(dnpANA * pNew, dnpANA * pDeadband, BOOL fLastEvent)
{
	if( m_bObject == 30 ) {

		dnpANA  Current;

		SetValue(&Current, fLastEvent ? m_Event : m_Value);

		switch( Current.type ) {

			case TMWTYPES_ANALOG_TYPE_SHORT:

				return pDeadband->value.sval < abs(pNew->value.sval - Current.value.sval);

			case TMWTYPES_ANALOG_TYPE_USHORT:

				return pDeadband->value.usval < abs(pNew->value.usval - Current.value.usval);

			case TMWTYPES_ANALOG_TYPE_LONG:

				return pDeadband->value.lval < abs(pNew->value.lval - Current.value.lval);

			case TMWTYPES_ANALOG_TYPE_ULONG:

				return pDeadband->value.ulval < TMWTYPES_ULONG(abs(pNew->value.ulval - Current.value.ulval));

			case TMWTYPES_ANALOG_TYPE_CHAR:

				return pDeadband->value.cval < abs(pNew->value.cval - Current.value.cval);

			case TMWTYPES_ANALOG_TYPE_UCHAR:

				return pDeadband->value.ucval < abs(pNew->value.ucval - Current.value.ucval);

			case TMWTYPES_ANALOG_TYPE_SFLOAT:

				return IsRealEvent(pNew, pDeadband, &Current);

			case TMWTYPES_ANALOG_TYPE_SCALED:

				return pDeadband->value.scaled.lval < abs(pNew->value.scaled.lval - Current.value.scaled.lval);

			case TMWTYPES_ANALOG_TYPE_DOUBLE:

				return pDeadband->value.dval < ABS(pNew->value.dval, Current.value.dval);

			case TMWTYPES_ANALOG_TYPE_DSCALED:

				return pDeadband->value.dscaled.dval < ABS(pNew->value.dscaled.dval, Current.value.dscaled.dval);
		}
	}

	return TRUE;
}

// System Access

PDWORD CUserData::GetData(PDWORD pData)
{
	m_pMutex->Wait(FOREVER);

	pData[0] = m_Value[0];

	if( IsDouble() ) {

		pData[1] = m_Value[1];

		pData++;
	}

	m_pMutex->Free();

	switch( m_bObject ) {

		// Single bit
		case 1:
		case 2:
		case 10:
		case 11:
		case 12:
		case 13: pData[0] = pData[0] >> 7;	break;

		// Double bit
		case 3:
		case 4:	 pData[0] = pData[0] >> 6;	break;
	}

	pData++;

	return pData;
}

PDWORD CUserData::SetData(PDWORD pData, dnpTIME * pTime)
{
	DWORD dValue = pData[0];

	switch( m_bObject ) {

		// Single bit
		case 1:
		case 2:
		case 10:
		case 11:
		case 12:
		case 13: dValue  = dValue ? dValue << 7 : 0;

			dValue |= DNPDEFS_DBAS_FLAG_ON_LINE;	break;

	       // Double bit
		case 3:
		case 4:	 dValue  = dValue ? dValue << 6 : 0;

			dValue |= DNPDEFS_DBAS_FLAG_ON_LINE;	break;
	}

	m_pMutex->Wait(FOREVER);

	m_Value[0]  = dValue;

	pData++;

	if( IsDouble() ) {

		m_Value[1] = pData[0];

		pData++;
	}

	m_uLast = GetTickCount() | 1;

	m_fDirty[dirtySys] = TRUE;

	if( pTime ) {

		SetTimeStamp(pTime);
	}

	m_pMutex->Free();

	return pData;
}

BOOL CUserData::GetEvent(PDWORD pData)
{
	if( m_Event[0] < NOTHING ) {

		if( m_Event[1] < NOTHING ) {

			pData[0] = m_Event[0];

			pData[1] = m_Event[1];

			return TRUE;
		}
	}

	return FALSE;
}

void CUserData::SetEvent(PDWORD pData)
{
	if( m_bObject == 30 ) {

		m_Event[0] = pData[0];

		m_Event[1] = IsDouble() ? pData[1] : 0;
	}
}

void CUserData::SetFlag(BYTE bFlag)
{
	m_pMutex->Wait(FOREVER);

	m_bStatus = bFlag;

	m_fDirty[dirtySys]  = TRUE;

	m_pMutex->Free();
}

UINT  CUserData::GetCtrl(void)
{
	return m_Ctrl;
}

UINT  CUserData::GetOnTime(void)
{
	return m_On;
}

UINT  CUserData::GetOffTime(void)
{
	return m_Off;
}

void  CUserData::SetCtrl(DWORD dwCtrl)
{
	m_Ctrl = dwCtrl;
}

void  CUserData::SetOnTime(DWORD dwOn)
{
	m_On = dwOn;
}


void  CUserData::SetOffTime(DWORD dwOff)
{
	m_Off = dwOff;
}

PDWORD CUserData::SetValue(dnpANA * pVal, PDWORD pData)
{
	if( pVal ) {

		CReal64 r;

		switch( m_Type ) {

			case addrBitAsBit:
			case addrByteAsByte:	pVal->type = TMWTYPES_ANALOG_TYPE_CHAR;

				pVal->value.cval = TMWTYPES_CHAR(pData[0]);

				pData++;

				return pData;

			case addrWordAsWord:	pVal->type = TMWTYPES_ANALOG_TYPE_SHORT;

				pVal->value.sval = TMWTYPES_SHORT(pData[0]);

				pData++;

				return pData;

			case addrLongAsLong:	pVal->type = TMWTYPES_ANALOG_TYPE_LONG;

				pVal->value.lval = TMWTYPES_LONG(pData[0]);

				pData++;

				return pData;

			case addrRealAsReal:	pVal->type = TMWTYPES_ANALOG_TYPE_SFLOAT;

				pVal->value.ulval = TMWTYPES_ULONG(pData[0]);

				pData++;

				return pData;

			case typeDouble:	pVal->type = TMWTYPES_ANALOG_TYPE_DOUBLE;

				r.i[1] = pData[0];

				pData++;

				r.i[0] = pData[0];

				pData++;

				pVal->value.dval = r.d;

				return pData;
		}
	}

	return pData;
}

BOOL CUserData::MatchType(BYTE bType)
{
	if( m_Type == bType ) {

		switch( m_Type ) {

			case typeDouble:        return IsDoubleData();

			case addrWordAsWord:
			case addrLongAsLong:	return IsWordData() || IsLongData();

			case addrRealAsReal:	return IsRealData();
		}

		return TRUE;
	}

	return FALSE;
}

// DNP SCL Access

void CUserData::GetBinary(dnpUCHAR *f)
{
	m_pMutex->Wait(FOREVER);

	*f = TMWTYPES_UCHAR(m_Value[0] | m_bStatus | DNPDEFS_DBAS_FLAG_ON_LINE);

	m_pMutex->Free();
}

void CUserData::GetValue(dnpLONG * pVal, dnpUCHAR *f, dnpTIME * pTimeStamp)
{
	m_pMutex->Wait(FOREVER);

	*pVal = m_Value[0];

	*f = m_bStatus | DNPDEFS_DBAS_FLAG_ON_LINE;

	if( pTimeStamp ) {

		SetTimeStamp(pTimeStamp);
	}

	m_pMutex->Free();
}

void CUserData::GetValue(dnpANA *pVal, dnpUCHAR *f)
{
	m_pMutex->Wait(FOREVER);

	SetValue(pVal, m_Value);

	if( f ) {

		*f = m_bStatus | DNPDEFS_DBAS_FLAG_ON_LINE;
	}

	m_pMutex->Free();
}

BYTE CUserData::GetVariation(BYTE bObject)
{
	BYTE bVar  = 2;

	bObject = bObject ? bObject : m_bObject;

	switch( bObject ) {

		case 20:	// counter
			bVar = m_Type == addrLongAsLong ? 1 : 2;	break;

		case 30:	// analog input

			if( IsDouble() ) {

				bVar = 6;

				break;
			}

			bVar = m_Type == addrLongAsLong ? 1 : 2;
			bVar = m_Type == addrRealAsReal ? 5 : bVar;	break;


		case 21:	// frozen counter
		case 22:	// counter change event
		case 23:	// frozen counter event
			bVar = m_Type == addrLongAsLong ? 5 : 6;	break;

		case 32:	// analog change event
		case 42:	// analog output events
		case 43:	// analog output cmd events

			if( IsDouble() ) {

				bVar = 8;

				break;
			}

			bVar = m_Type == addrLongAsLong ? 3 : 4;
			bVar = m_Type == addrRealAsReal ? 7 : bVar;	break;


		case 34:	// analog input deadband
			bVar = m_Type == addrLongAsLong ? 2 : 1;
			bVar = m_Type == addrRealAsReal ? 3 : bVar;	break;

		case 40:	// analog output status
		case 41:	// analog output cmd 

			if( IsDouble() ) {

				bVar = 4;

				break;
			}

			bVar = m_Type == addrLongAsLong ? 1 : 2;
			bVar = m_Type == addrRealAsReal ? 3 : bVar;	break;

	}

	return bVar;
}

void  CUserData::SetBinary(dnpUCHAR f, dnpBOOL isE, dnpTIME * pTS)
{
	m_pMutex->Wait(FOREVER);

	SetTimeStamp(pTS);

	SetFlags(f & 0x3F);

	m_Value[0] = f;

	m_uLast = GetTickCount() | 1;

	m_pMutex->Free();
}

void  CUserData::SetValue(dnpWORD Val, dnpTIME *pTS, dnpUCHAR f, dnpBOOL isE)
{
	m_pMutex->Wait(FOREVER);

	SetTimeStamp(pTS);

	SetFlags(f);

	m_Value[0]  = Val;

	m_uLast = GetTickCount() | 1;

	m_pMutex->Free();
}

void  CUserData::SetValue(dnpLONG Val, dnpTIME *pTS, dnpUCHAR f, dnpBOOL isE)
{
	m_pMutex->Wait(FOREVER);

	SetTimeStamp(pTS);

	SetFlags(f);

	m_Value[0]  = Val;

	m_uLast = GetTickCount() | 1;

	m_pMutex->Free();
}

void  CUserData::SetValue(dnpANA *pVal, dnpTIME *pTS, dnpUCHAR f, dnpBOOL isE)
{
	m_pMutex->Wait(FOREVER);

	SetFlags(f);

	CReal64 r;

	m_bDataType = BYTE(pVal->type);

	switch( pVal->type ) {

		case TMWTYPES_ANALOG_TYPE_SHORT:	m_Value[0] = pVal->value.sval;		break;

		case TMWTYPES_ANALOG_TYPE_USHORT:	m_Value[0] = pVal->value.usval;		break;

		case TMWTYPES_ANALOG_TYPE_LONG:		m_Value[0] = pVal->value.lval;		break;

		case TMWTYPES_ANALOG_TYPE_ULONG:	m_Value[0] = pVal->value.ulval;		break;

		case TMWTYPES_ANALOG_TYPE_CHAR:		m_Value[0] = pVal->value.cval;		break;

		case TMWTYPES_ANALOG_TYPE_UCHAR:	m_Value[0] = pVal->value.ucval;		break;

		case TMWTYPES_ANALOG_TYPE_SFLOAT:	m_Value[0] = pVal->value.ulval;		break;

		case TMWTYPES_ANALOG_TYPE_SCALED:	m_Value[0] = pVal->value.scaled.lval;	break;

		case TMWTYPES_ANALOG_TYPE_DOUBLE:	r.d = pVal->value.dval;

			m_Value[0] = r.i[1];

			m_Value[1] = r.i[0];			break;

		case TMWTYPES_ANALOG_TYPE_DSCALED:	r.d = pVal->value.dscaled.dval;

			m_Value[0] = r.i[1];

			m_Value[1] = r.i[0];			break;
	}

	SetTimeStamp(pTS);

	m_uLast = GetTickCount() | 1;

	m_pMutex->Free();
}

void CUserData::SetOperate(dnpANA *pVal, dnpTIME *pTS, dnpUCHAR f, dnpBOOL isE)
{
	m_pMutex->Wait(FOREVER);

	m_fDirty[dirtyScl] = TRUE;

	m_pMutex->Free();

	SetValue(pVal, pTS, f, isE);
}

void CUserData::SetOperate(dnpUCHAR f, dnpBOOL isE, dnpTIME * pTS)
{
	m_pMutex->Wait(FOREVER);

	m_fDirty[dirtyScl] = TRUE;

	m_pMutex->Free();

	SetBinary(f, isE, pTS);
}

// Implementation

void CUserData::SetTimeStamp(dnpTIME *pTime)
{
	if( pTime ) {

		struct tm tm = { 0 };

		tm.tm_sec   = pTime->mSecsAndSecs / 1000;
		tm.tm_min   = pTime->minutes;
		tm.tm_hour  = pTime->hour;
		tm.tm_mday  = pTime->dayOfMonth - 0;
		tm.tm_mon   = pTime->month      - 1;
		tm.tm_year  = pTime->year       - 1900;

		m_TimeStamp = timegm(&tm);

		m_TimeStamp = 1 * (m_TimeStamp - 852076800);
				
		return;
	}

	if( !IsEvent() ) {

		struct timeval tv;

		gettimeofday(&tv, NULL);

		if( m_pZone ) {

			INT nOffset = m_pZone->GetOffset();

			INT nDst    = m_pZone->GetDst();

			tv.tv_sec  -= -60 * (nOffset + (nDst ? 60 : 0));

			tv.tv_sec   = 1 * (tv.tv_sec - 852076800);
		}

		m_TimeStamp = tv.tv_sec;
	}
}

void CUserData::SetFlags(dnpUCHAR flag)
{
	m_bStatus = flag;
}

BOOL CUserData::IsEvent(void)
{
	switch( m_bObject ) {

		case 2:
		case 4:
		case 11:
		case 13:
		case 22:
		case 23:
		case 32:
		case 42:
		case 43:
			return TRUE;
	}

	return FALSE;
}

BOOL CUserData::IsDouble(void)
{
	return m_Type == typeDouble;
}

BOOL CUserData::IsDoubleData(void)
{
	return m_bDataType >= TMWTYPES_ANALOG_TYPE_DOUBLE;
}

BOOL CUserData::IsWordData(void)
{
	return m_bDataType <= TMWTYPES_ANALOG_TYPE_USHORT;
}

BOOL CUserData::IsRealData(void)
{
	return m_bDataType == TMWTYPES_ANALOG_TYPE_SFLOAT;
}

BOOL CUserData::IsLongData(void)
{
	switch( m_bDataType ) {

		case TMWTYPES_ANALOG_TYPE_LONG:
		case TMWTYPES_ANALOG_TYPE_ULONG:
		case TMWTYPES_ANALOG_TYPE_SCALED:

			return TRUE;
	}

	return FALSE;
}

BOOL CUserData::IsRealEvent(dnpANA * pNew, dnpANA * pDeadband, dnpANA * pCurrent)
{
	float Deadband = *((float *) &pDeadband->value.ulval);

	float Current  = *((float *) &pCurrent->value.ulval);

	float Value    = *((float *) &pNew->value.ulval);

	return Deadband < ABS(Value, Current);
}

// Debugging Help

void CUserData::PrintData(void)
{
}

// End of File
