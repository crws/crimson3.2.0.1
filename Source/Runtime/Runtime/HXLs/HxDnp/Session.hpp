
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Session_HPP

#define	INCLUDE_Session_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "DnpStack.hpp"

#include "UserObjects.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Debug Help
//

#define DNP_SESS_DEBUG 0

//////////////////////////////////////////////////////////////////////////
//
// Session Object - Device
//

class CSession : public IDnpSession
{
	public:
		// Constructor
		CSession(IDnpChannel * pChan, WORD wDest, WORD wTO, DWORD dwLink);

		// Destructor
		~CSession(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnpSession
		BOOL METHOD Open(IDnpChannel * pChan);
		BOOL METHOD Close(void);
		BOOL METHOD Ping(void);
		UINT METHOD Validate(BYTE o, WORD i, BYTE t, UINT uCount);
		UINT METHOD GetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount);
		UINT METHOD GetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetTimeStamp(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetClass(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount);
		UINT METHOD GetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount);

		// Access
		void Init(void);
		void Online(BOOL fOnline);
				
	protected:
		// Data Members
		ULONG           m_uRefs;
		dnpSess   *	m_pSession;
		CUserObjects *	m_pDatabase;
		BOOL		m_fOnline;
		IDnpChannel  *	m_pChannel;
		BOOL		m_fSuccess;
		BOOL		m_fTimeout;
		WORD		m_FailMask;
		BOOL		m_fFail;
				
		// Implementation
		void Success(void);
		void Timeout(void);
		void SetFail(void);
	
		// Friends
		friend void StatCallback(void *pVoid, TMWSESN_STAT_EVENT Event, void *pStat);
	};

// End of File

#endif
