
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern IDnp * Create_Dnp3(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxDnp(void)
{
	piob->RegisterSingleton("comms.dnp3", 0, Create_Dnp3());
	}

void Revoke_HxDnp(void)
{
	piob->RevokeSingleton("comms.dnp3", 0);
	}

// End of File
