
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UserTable_HPP

#define	INCLUDE_UserTable_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "UserData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User Data Table 
//

class CUserTable
{
	public:
		// Constructor
		CUserTable(BYTE bObject, BYTE bMode, ITimeZone * pZone);

		// Destructor
		~CUserTable(void);

		// Attributes
		CUserData * GetHead(void)  const;
		CUserData * GetTail(void)  const;
		UINT        GetCount(void) const;
		BYTE	    GetMode(void)  const;
		
		// Managment
		void        Clear(void);
		CUserData * Append(CUserData *pData);
		CUserData * Remove(CUserData *pData);
		CUserData * Insert(CUserData *pData, CUserData * pScan);
		CUserData * Find(UINT uIndex, BYTE bType = addrLongAsLong, BOOL fAppend = FALSE);
		CUserData * FindNext(CUserData * pScan, UINT uIndex, BYTE bType = addrLongAsLong, BOOL fAppend = FALSE);
				
	protected:

		// Data Members
		CUserData * m_pHead;
		CUserData * m_pTail;
		UINT	    m_uCount;
		BYTE	    m_bObject;
		BYTE	    m_bMode;
		ITimeZone * m_pZone;

		// Implementation
		void Free(void);
		
		// Debugging Help
		void PrintTable(void);
	};

// End of File

#endif
