
#include "Intern.hpp"

#include "ChannelConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP Channel Configuration
//

// Constructor

CChannelConfig::CChannelConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO)
{
	StdSetRef();

	m_IP     = IP;

	m_IP2    = IP2;

	m_Tcp    = wTcp;

	m_Udp    = wUdp;

	m_TO     = wTO;

	m_fFail  = FALSE;
	}

// IUnknown

HRESULT CChannelConfig::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDnpChannelConfig);

	StdQueryInterface(IDnpChannelConfig);

	return E_NOINTERFACE;
	}

ULONG CChannelConfig::AddRef(void)
{
	StdAddRef();
	}

ULONG CChannelConfig::Release(void)
{
	StdRelease();
	}

// IDnpChannelConfig 

IPADDR METHOD CChannelConfig::GetIP(void)
{
	return m_fFail ? m_IP2 : m_IP;
	}

WORD METHOD CChannelConfig::GetTcpPort(void)
{
	return m_Tcp;
	}

WORD METHOD CChannelConfig::GetUdpPort(void)
{
	return m_Udp;
	}

WORD METHOD CChannelConfig::GetConnectionTimeout(void)
{
	return m_TO;
	}

void  METHOD CChannelConfig::SetFail(void)
{
	if( !(IPREF(m_IP2).IsEmpty()) ) {

		m_fFail = !m_fFail;
	}
}

// End of File

