
#include "Intern.hpp"

#include "UserObjects.hpp"

#include "DnpData.hpp"

#include "Dnp3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Debug Help
//

#define DNP_DATA_DEBUG 0

//////////////////////////////////////////////////////////////////////////
//
// Database Support API for DNP SCL (Master)
//

void DataM_processIIN(dnpSES *pSession, dnpWORD *pIIN)
{
	if( DNP_DATA_DEBUG ) {

		/////////////////////////////////////////////////
		//
		// TODO:  Clear any bits before SCL processing ??
		//
		//
		//DNPDEFS_IIN_RESTART		/* IIN bit 1.7 */
		//DNPDEFS_IIN_TROUBLE		/* IIN bit 1.6 */	
		//DNPDEFS_IIN_LOCAL		/* IIN bit 1.5 */
		//DNPDEFS_IIN_NEED_TIME		/* IIN bit 1.4 */
		//DNPDEFS_IIN_CLASS_3		/* IIN bit 1.3 */
		//DNPDEFS_IIN_CLASS_2		/* IIN bit 1.2 */
		//DNPDEFS_IIN_CLASS_1		/* IIN bit 1.1 */
		//DNPDEFS_IIN_ALL_STATIONS	/* IIN bit 1.0 */
		//DNPDEFS_IIN_BAD_CONFIG	/* IIN bit 2.5 */
		//DNPDEFS_IIN_ALREADY_EXECUTING	/* IIN bit 2.4 */
		//DNPDEFS_IIN_BUFFER_OVFL	/* IIN bit 2.3 */
		//DNPDEFS_IIN_PARAM_ERROR       /* IIN bit 2.2 */
		//DNPDEFS_IIN_OBJECT_UNKNOWN	/* IIN bit 2.1 */
		//DNPDEFS_IIN_BAD_FUNCTION	/* IIN bit 2.0 */
		
		AfxTrace("DataM_processIIN %8.8x\n", *pIIN);
		}

	CUserObjects * pDb = (CUserObjects * )pSession->pUserData;

	if( pDb ) {

		pDb->SetFeedback(*pIIN);
		}
	}

void DataM_storeIIN(void *pHandle, dnpWORD pointNumber, dnpBOOL value)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeIIN\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		pDB->SetIIbit(BYTE(pointNumber), value);
		}
	}

void * DataM_init(dnpSES *pSession, void *pUserHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_init %8.8x %8.8x\n", pSession, pUserHandle);
		}

	return pUserHandle;
	}

void DataM_close(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_close\n");
		}
	}

void DataM_storeReadTime(void *pHandle, dnpTIME *pTimeStamp)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeReadTime\n");
		}
	}

dnpBOOL DataM_storeBinaryInput(void *pHdl, dnpWORD pt, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeBinaryInput\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_2_BIN_CHNG_EVENTS : DNPDEFS_OBJ_1_BIN_INPUTS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetBinary(f, isE, pTS);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeDoubleInput(void *pHdl, dnpWORD pt, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeDoubleInput\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_4_DBL_CHNG_EVENTS : DNPDEFS_OBJ_3_DBL_INPUTS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetBinary(f, isE, pTS);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeBinaryOutput(void *pHdl, dnpWORD pt, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeBinaryOutput\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_11_BIN_OUT_EVENTS : DNPDEFS_OBJ_10_BIN_OUTS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetBinary(f, isE, pTS);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeBinaryCmdStatus(void *pHdl, dnpWORD pt, dnpUCHAR s, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeBinaryCmdStatus\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_13_BIN_CMD_EVENTS : DNPDEFS_OBJ_12_BIN_OUT_CTRLS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetBinary(s, isE, pTS);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeBinaryCounter(void *pHdl, dnpWORD pt, dnpLONG val, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeBinaryCounter\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_22_CNTR_EVENTS : DNPDEFS_OBJ_20_RUNNING_CNTRS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetValue(val, pTS, f, isE);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeFrozenCounter(void *pHdl, dnpWORD pt, dnpLONG val, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeFrozenCounter\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_23_FCTR_EVENTS : DNPDEFS_OBJ_21_FROZEN_CNTRS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetValue(val, pTS, f, isE);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeAnalogInput(void *pHdl, dnpWORD pt, dnpANA *pVal, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeAnalogInput\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_32_ANA_CHNG_EVENTS : DNPDEFS_OBJ_30_ANA_INPUTS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetValue(pVal, pTS, f, isE);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeAnalogInputDeadband(void *pHdl, dnpWORD pt, dnpANA *pValue)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeAnalogInputDeadband\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = DNPDEFS_OBJ_34_ANA_INPUT_DBANDS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetValue(pValue);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeAnalogOutput(void *pHdl, dnpWORD pt, dnpANA *pVal, dnpUCHAR f, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeAnalogOutput\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_42_ANA_OUT_EVENTS : DNPDEFS_OBJ_40_ANA_OUT_STATUSES;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetValue(pVal, pTS, f, isE);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeAnalogCmdStatus(void *pHdl, dnpWORD pt, dnpANA *pVal, dnpUCHAR s, dnpBOOL isE, dnpTIME *pTS)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeAnalogCmdStatus\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHdl;

	if( pDB ) {

		BYTE bObject = isE ? DNPDEFS_OBJ_43_ANA_CMD_EVENTS : DNPDEFS_OBJ_41_ANA_OUT_CTRLS;

		CUserData * pData = pDB->Find(bObject, pt);

		if( pData ) {

			pData->SetValue(pVal, pTS, s, isE);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataM_storeRestartTime(void *pHandle, dnpLONG time)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeRestartTime\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeString(void *pHdl, dnpWORD pt, dnpUCHAR *pStrBuf, dnpUCHAR strLen)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeString\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeActiveConfig(void *pHdl, dnpUCHAR i, dnpLONG d, dnpUCHAR s, dnpUCHAR *pStr, dnpUCHAR len)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeActiveConfig\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeVirtualTerminal(void *pHandle, dnpWORD pt, dnpUCHAR *pVtermBuf, dnpUCHAR vtermLen)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeVirtualTerminal\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeFileAuthKey(void *pHandle, dnpLONG authKey)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeFileAuthKey\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeFileStatus(void *pHdl, dnpLONG h, dnpLONG fs, dnpWORD b, dnpWORD r, dnpFCS s, dnpWORD o, const dnpCHAR *pO)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeFileStatus\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeFileData(void *pHdl, dnpLONG h, dnpLONG n, dnpBOOL f, dnpWORD b, const dnpUCHAR *pData)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeFileData\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeFileDataStatus(void *pHdl, dnpLONG h, dnpLONG n, dnpBOOL f, dnpFTS s, dnpWORD o, const dnpCHAR *pO)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeFileDataStatus\n");
		}

	return FALSE;
	}

dnpBOOL DataM_storeFileInfo(void *pHdl, dnpWORD o, dnpWORD n, dnpFT t, dnpLONG s, dnpTIME *pTime, dnpFP p, const dnpCHAR *pN)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_storeFileInfo\n");
		}

	return FALSE;
	}

dnpLONG DataM_getFileAuthKey(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_getFileAuthKey\n");
		}

	return 0;
	}

dnpBOOL DataM_openLocalFile(void *pHandle, const dnpCHAR *pLocalFileName, dnpFM fileMode)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_openLocalFile\n");
		}

	return FALSE;
	}

dnpBOOL DataM_closeLocalFile(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_closeLocalFile\n");
		}

	return FALSE;
	}

dnpBOOL DataM_getLocalFileInfo(void *pHandle, const dnpCHAR *pName, dnpLONG *pSize, dnpTIME *pTime)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_getLocalFileInfo\n");
		}

	return FALSE;
	}

dnpWORD DataM_readLocalFile(void *pHandle, dnpUCHAR *pBuf, dnpWORD bufSize, dnpBOOL *pLastBlock)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataM_readLocalFile\n");
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Database Support API for DNP SCL (Slave)
//

void * DataS_init(dnpSES *pSession, void *pUserHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_init\n");
		}

	return pUserHandle;
	}

void DataS_close(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_close\n");
		}
	}

void DataS_getIIN(dnpSES *pSession, dnpWORD *pIIN)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_getIIN\n");
		}

	// No need to modify - most bits handled by SCL !
	}

dnpWORD DataS_IINQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_IINQuantity\n");
		}

	return 16;
	}

dnpBOOL DataS_IINRead(void *pHandle, dnpWORD pointNumber)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_IINRead\n");
		}

	// No private IIN's implemented - SCL handles the rest !

	return FALSE;
	}

void DataS_coldRestart(dnpSES *pSession) 
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_coldRestart\n");
		}

	IDnp * pDnp = CDnp3::Locate();

	if( pDnp ) {

		pDnp->SetColdStart();
		}
	}

void DataS_warmRestart(dnpSES *pSession) 
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_warmRestart\n");
		}

	IDnp * pDnp = CDnp3::Locate();

	if( pDnp ) {

		pDnp->SetWarmStart();
		}
	}

void DataS_setTime(void *pHandle, dnpTIME *pNewTime)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_setTime\n");
		}

	Sys_SetDateTime(pNewTime);
	}

void DataS_unsolEventMask(void *pHandle, dnpMASK unsolEventMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_unsolEventMask\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		pDB->SetUnsolicitMsgMask(unsolEventMask);
		}
	}

void DataS_eventAndStaticRead(void *pHandle, dnpBOOL inProgress)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_eventAndStaticRead\n");
		}
	}

void DataS_funcCode(void *pHandle, char functionCode, dnpBOOL inProgress)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_funcCode\n");
		}
	}

dnpWORD	DataS_binInQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_1_BIN_INPUTS);

		if( pTable ) {

			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_binInGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_1_BIN_INPUTS);

		if( pTable ) {

			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

dnpUCHAR DataS_binInDefVariation(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 1;
	}

dnpMASK DataS_binInEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL	DataS_binInIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInIsClass0\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClass() <= 3;
		}

	return TRUE;
	}

dnpUCHAR DataS_binInEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(2);
		}

	return 3;
	}

dnpEM DataS_binInEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpBOOL DataS_binInAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? TRUE : FALSE;
		}

	return FALSE;
	}

void DataS_binInRead(void *pPoint, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetBinary(pFlags);
		}
	}

dnpBOOL DataS_binInChanged(void *pPoint, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binInChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetBinary(pFlags);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpWORD DataS_binOutQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_10_BIN_OUTS);

		if( pTable ) {

			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_binOutGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_10_BIN_OUTS);

		if( pTable ) {

			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

dnpUCHAR DataS_binOutDefVariation(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 2;
	}

dnpMASK DataS_binOutEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_binOutIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutIsClass0\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClass() <= 3;
		}

	return TRUE;
	}

dnpUCHAR DataS_binOutEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(11);
		}

	return 3;
	}

dnpEM DataS_binOutEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpBOOL DataS_binOutAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? TRUE : FALSE;
		}

	return FALSE;
	}

void DataS_binOutRead(void *pPoint, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetBinary(pFlags);
		}
	}

dnpBOOL DataS_binOutChanged(void *pPoint, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetBinary(pFlags);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL  DataS_binOutWrite(void *pPoint, dnpUCHAR value)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutWrite\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->SetBinary(value);

		return TRUE;
		}

	return FALSE;
	}

dnpUCHAR DataS_binOutGetControlMask(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutGetControlMask\n");
		}

	return ((SDNPDATA_CROB_CTRL)0);
	}

dnpCRST DataS_binOutSelect(void *pPt, dnpUCHAR ctrlCode, dnpUCHAR ct, dnpLONG onTime, dnpLONG offTime)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutSelect\n");
		}

	CUserData * pData = (CUserData *)pPt;

	if( pData ) {

		if( ctrlCode == DNPDEFS_CROB_CTRL_LATCH_ON || ctrlCode == DNPDEFS_CROB_CTRL_LATCH_OFF ) {

			return DNPDEFS_CROB_ST_SUCCESS;
			}
		}

	return DNPDEFS_CROB_ST_NOT_SUPPORTED;
	}

dnpCRST DataS_binOutOperate(void *pPt, dnpUCHAR ctrlCode, dnpUCHAR ct, dnpLONG onTime, dnpLONG offTime)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutOperate\n");
		}

	CUserData * pData = (CUserData *)pPt;

	if( pData ) {

		if( ctrlCode == DNPDEFS_CROB_CTRL_LATCH_ON || ctrlCode == DNPDEFS_CROB_CTRL_LATCH_OFF ) {

			pData->SetOperate((ctrlCode == DNPDEFS_CROB_CTRL_LATCH_ON) << 7);

			return DNPDEFS_CROB_ST_SUCCESS;
			}
		}

	return DNPDEFS_CROB_ST_NOT_SUPPORTED;
	}

dnpCRST DataS_binOutSelPatternMask(void *pH, dnpUCHAR ctrl, dnpUCHAR ct, dnpLONG on, dnpLONG off, dnpWORD f, dnpWORD l, dnpUCHAR *pMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutSelPatternMask\n");
		}

	return DNPDEFS_CROB_ST_NOT_SUPPORTED;
	}

dnpCRST DataS_binOutOpPatternMask(void *pH, dnpUCHAR ctrl, dnpUCHAR ct, dnpLONG on, dnpLONG off, dnpWORD f, dnpWORD l, dnpUCHAR *pMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutOpPatternMask\n");
		}

	return DNPDEFS_CROB_ST_NOT_SUPPORTED;
	}

dnpMASK DataS_binOutCmdEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutCmdEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpUCHAR DataS_binOutCmdEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutCmdEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(13);
		}

	return 1;
	}

dnpEM DataS_binOutCmdEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutCmdEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpBOOL DataS_binOutCmdChanged(void *pPoint, dnpUCHAR *pStatus)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutCmdChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetBinary(pStatus);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataS_binOutCmdAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binOutCmdAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? true : false;
		}

	return FALSE;
	}

dnpWORD DataS_binCntrQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_20_RUNNING_CNTRS);

		if( pTable ) {
			
			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_binCntrGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_20_RUNNING_CNTRS);

		if( pTable ) {

			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

void * DataS_binCntrGetFrzPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrGetFrzPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_20_RUNNING_CNTRS);

		if( pTable ) {

			CUserData * pCtr = pTable->Find(pointNum);

			if( pCtr ) {

				CUserData * pFrz = pDB->Find(DNPDEFS_OBJ_21_FROZEN_CNTRS)->Find(pointNum);

				if( pFrz ) {

					DWORD Data[1];

					pCtr->GetData(Data);

					pFrz->SetValue(Data[0]);

					return pCtr;
					}
				}
			}
		}

	return NULL;
	}

dnpMASK DataS_binCntrEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_binCntrIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrIsClass0\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClass() <= 3;
		}

	return TRUE;
	}

dnpEM DataS_binCntrEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_MOST_RECENT;
	}

dnpBOOL DataS_binCntrAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? true : false;
		}

	return FALSE;
	}

dnpUCHAR DataS_binCntrDefVariation(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 5;
	}

dnpUCHAR DataS_binCntrEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(22);
		}

	return 1;
	}

void DataS_binCntrRead(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetValue(pValue, pFlags);
		}
	}

dnpBOOL DataS_binCntrChanged(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetValue(pValue, pFlags);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpBOOL DataS_binCntrFreeze(void *pPoint, dnpBOOL clearAfterFreeze)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_binCntrFreeze\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( clearAfterFreeze ) {

			pData->SetValue(dnpLONG(0));
			}

		return TRUE;
		}

	return FALSE;
	}

dnpWORD	DataS_frznCntrQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_21_FROZEN_CNTRS);

		if( pTable ) {
			
			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_frznCntrGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_21_FROZEN_CNTRS);

		if( pTable ) {
			
			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

dnpMASK DataS_frznCntrEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_frznCntrIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrIsClass0\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClass() <= 3;
		}

	return TRUE;
	}

dnpBOOL DataS_frznCntrAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? true : false;
		}

	return FALSE;
	}

dnpUCHAR DataS_frznCntrDefVariation(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 9;
	}

dnpUCHAR DataS_frznCntrEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(23);
		}

	return 1;
	}

dnpEM DataS_frznCntrEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

void DataS_frznCntrRead(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags, dnpTIME *pTimeOfFreeze)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetValue(pValue, pFlags, pTimeOfFreeze);
		}
	}

dnpBOOL DataS_frznCntrChanged(void *pPoint, dnpLONG *pValue, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_frznCntrChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetValue(pValue, pFlags);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpWORD DataS_anlgInQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_30_ANA_INPUTS);

		if( pTable ) {

			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_anlgInGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_30_ANA_INPUTS);

		if( pTable ) {

			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

dnpUCHAR DataS_anlgInDefVariation(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 3;
	}

dnpUCHAR DataS_anlgInEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(32);
		}

	return 1;
	}

dnpEM DataS_anlgInEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpMASK DataS_anlgInEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_anlgInIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInIsClass0\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClass() <= 3;
		}

	return TRUE;
	}

dnpBOOL DataS_anlgInAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? true : false;
		}

	return FALSE;
	}

void DataS_anlgInRead(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetValue(pValue, pFlags);
		}
	}

dnpBOOL DataS_anlgInChanged(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetValue(pValue, pFlags);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpWORD	DataS_anlgInDBandQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInDBandQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_34_ANA_INPUT_DBANDS);

		if( pTable ) {

			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_anlgInDBandGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInDBandGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_34_ANA_INPUT_DBANDS);

		if( pTable ) {

			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

dnpUCHAR DataS_anlgInDbandDefVar(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInDbandDefVar\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 2;
	}

void DataS_anlgInDBandRead(void *pPoint, dnpANA *pValue)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInDBandRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetValue(pValue, NULL);
		}
	}

dnpBOOL DataS_anlgInDBandWrite(void *pPoint, dnpANA *pValue)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgInDBandWrite\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->SetValue(pValue);

		return TRUE;
		}

	return FALSE;
	}

dnpWORD DataS_anlgOutQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_40_ANA_OUT_STATUSES);

		if( pTable ) {

			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_anlgOutGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_40_ANA_OUT_STATUSES);

		if( pTable ) {

			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

dnpUCHAR DataS_anlgOutDefVariation(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 2;
	}

dnpUCHAR DataS_anlgOutEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(42);
		}

	return 2;
	}

dnpEM DataS_anlgOutEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpMASK DataS_anlgOutEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_anlgOutIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutIsClass0\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClass() <= 3;
		}

	return TRUE;
	}

dnpBOOL DataS_anlgOutAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? true : false;
		}

	return FALSE;
	}

void DataS_anlgOutRead(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetValue(pValue, pFlags);
		}	
	}

dnpBOOL DataS_anlgOutChanged(void *pPoint, dnpANA *pValue, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetValue(pValue, pFlags);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpCTST DataS_anlgOutSelect(void *pPoint, dnpANA *pValue)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutSelect\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return DNPDEFS_CTLSTAT_SUCCESS;
		}

	return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
	}

dnpCTST DataS_anlgOutOperate(void *pPoint, dnpANA *pValue)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutOperate\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->SetOperate(pValue);

		return DNPDEFS_CTLSTAT_SUCCESS;
		}

	return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
	}

dnpUCHAR DataS_anlgOutCmdEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutCmdEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(43);
		}

	return 2;
	}

dnpEM DataS_anlgOutCmdEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutCmdEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpMASK DataS_anlgOutCmdEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutCmdEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_anlgOutCmdAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutCmdAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? true : false;
		}

	return FALSE;
	}

dnpBOOL DataS_anlgOutCmdChanged(void *pPoint, dnpANA *pValue, dnpUCHAR *pStatus)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_anlgOutCmdChanged\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetValue(pValue, pStatus);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpWORD DataS_dblInQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInQuantity\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_3_DBL_INPUTS);

		if( pTable ) {

			return dnpWORD(pTable->GetCount());
			}
		}

	return 0;
	}

void * DataS_dblInGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInGetPoint\n");
		}

	CUserObjects * pDB = (CUserObjects *)pHandle;

	if( pDB ) {

		CUserTable * pTable = pDB->Find(DNPDEFS_OBJ_3_DBL_INPUTS);

		if( pTable ) {

			return pTable->Find(pointNum);
			}
		}

	return NULL;
	}

dnpUCHAR DataS_dblInDefVariation(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation();
		}

	return 1;
	}

dnpEM DataS_dblInEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInEventMode\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return (dnpEM) pData->GetMode();
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpMASK DataS_dblInEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInEventClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClassMask();
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_dblInIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInIsClass0\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetClass() <= 3;
		}

	return TRUE;
	}

dnpUCHAR DataS_dblInEventDefVariation(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInEventDefVariation\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		return pData->GetVariation(4);
		}

	return 3;
	}

dnpBOOL DataS_dblInAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInAssignClass\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) { 

		return pData->SetClass(classMask) ? true : false;
		}

	return FALSE;
	}

void DataS_dblInRead(void *pPoint, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInRead\n");
		}

	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		pData->GetBinary(pFlags);
		}
	}

dnpBOOL	DataS_dblInChanged(void *pPoint, dnpUCHAR *pFlags)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_dblInChanged\n");
		}
	
	CUserData * pData = (CUserData *)pPoint;

	if( pData ) {

		if( pData->IsDirty(dirtySys) ) {

			pData->GetBinary(pFlags);

			return TRUE;
			}
		}

	return FALSE;
	}

dnpWORD	DataS_strQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strQuantity\n");
		}

	return 0;
	}

void * DataS_strGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strGetPoint\n");
		}

	return NULL;
	}

dnpEM DataS_strEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strEventMode\n");
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpMASK DataS_strEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strEventClass\n");
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_strAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strAssignClass\n");
		}

	return FALSE;
	}

dnpBOOL DataS_strIsClass0(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strIsClass0\n");
		}

	return TRUE;
	}

void DataS_strRead(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strRead\n");
		}
	}

dnpBOOL DataS_strWrite(void *pPoint, dnpUCHAR *pBuf, dnpUCHAR bufLength)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strWrite\n");
		}

	return FALSE;
	}

dnpBOOL DataS_strChanged(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_strChanged\n");
		}

	return FALSE;
	}

dnpWORD DataS_vtermQuantity(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermQuantity\n");
		}

	return 0;
	}

void * DataS_vtermGetPoint(void *pHandle, dnpWORD pointNum)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermGetPoint\n");
		}

	return NULL;
	}

dnpEM DataS_vtermEventMode(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermEventMode\n");
		}

	return TMWDEFS_EVENT_MODE_SOE;
	}

dnpMASK DataS_vtermEventClass(void *pPoint)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermEventClass\n");
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_vtermAssignClass(void *pPoint, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermAssignClass\n");
		}

	return FALSE;
	}

void DataS_vtermRead(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermRead\n");
		}
	}

dnpBOOL DataS_vtermWrite(void *pPoint, dnpUCHAR *pBuf, dnpUCHAR bufLength)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermWrite\n");
		}

	return FALSE;
	}

dnpBOOL DataS_vtermChanged(void *pPoint, dnpUCHAR maxLength, dnpUCHAR *pBuf, dnpUCHAR *pLength)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_vtermChanged\n");
		}

	return FALSE;
	}

dnpMASK DataS_fileEventClass(void *pHandle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_fileEventClass\n");
		}

	return TMWDEFS_CLASS_MASK_NONE;
	}

dnpBOOL DataS_fileAssignClass(void *pHandle, dnpMASK classMask)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_fileAssignClass\n");
		}

	return FALSE;
	}

dnpFCS DataS_getFileInfo(dnpSES *pSess, dnpCHAR *pName, dnpFT *pType, dnpLONG *pSize, dnpTIME *pCreated, dnpFP *pPerm)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_getFileInfo\n");
		}

	return DNPDEFS_FILE_CMD_STAT_DENIED;
	}

dnpFTS DataS_readFileInfo(dnpSES *pSes, dnpLONG h, dnpWORD c, dnpCHAR *pName, dnpBOOL *pL, dnpFT *pT, dnpLONG *pS, dnpTIME *pTime, dnpFP *pPerm)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_readFileInfo\n");
		}

	return DNPDEFS_FILE_TFER_STAT_NOT_OPEN;
	}

dnpBOOL DataS_getAuthentication(dnpSES *pSess, dnpCHAR *pUsername, dnpCHAR *pPassword, dnpLONG *pAuthKey)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_getAuthentication\n");
		}

	return FALSE;
	}

dnpFCS DataS_deleteFile(dnpSES *pSession, dnpCHAR *pFilename, dnpLONG authKey)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_deleteFile\n");
		}

	return DNPDEFS_FILE_CMD_STAT_DENIED;
	}

dnpFCS DataS_openFile(dnpSES *pSes, dnpCHAR *pName, dnpLONG ak, dnpFM m, dnpWORD *pMax, dnpFP *pPerm, dnpTIME *pTime, dnpLONG *pH, dnpLONG *pS, dnpFT *pT)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_openFile\n");
		}

	return DNPDEFS_FILE_CMD_STAT_DENIED;
	}

dnpFCS DataS_closeFile(dnpSES *pSession, dnpLONG handle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_closeFile\n");
		}

	return DNPDEFS_FILE_CMD_STAT_DENIED;
	}

dnpFTS DataS_readFile(dnpSES *pSession, dnpLONG handle, dnpBOOL *pLast, dnpWORD *pBytesRead, dnpUCHAR *pBuf)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_readFile\n");
		}

	return DNPDEFS_FILE_TFER_STAT_NOT_OPEN;
	}

void dataS_confirmFileRead(dnpSES *pSession, dnpLONG handle)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("dataS_confirmFileRead\n");
		}

	}

dnpFTS DataS_writeFile(dnpSES *pSession, dnpLONG handle, dnpBOOL last, dnpWORD numBytes, dnpUCHAR *pBuf)
{
	if( DNP_DATA_DEBUG ) {

		AfxTrace("DataS_writeFile\n");
		}

	return DNPDEFS_FILE_TFER_STAT_NOT_OPEN;
	}



// End of File
