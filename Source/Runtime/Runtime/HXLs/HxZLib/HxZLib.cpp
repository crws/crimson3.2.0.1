
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_ZLib(void);

extern void Revoke_ZLib(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxZLib(void)
{
	Register_ZLib();
	}

void Revoke_HxZLib(void)
{
	Revoke_ZLib();
	}

// End of File
