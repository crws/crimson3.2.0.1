
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Queue_HPP

#define	INCLUDE_Queue_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMacAddr;

//////////////////////////////////////////////////////////////////////////
//
// Queue Protocol
//

class CQueue : public IQueue
{
	public:
		// Constructor
		CQueue(void);

		// Destructor
		~CQueue(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		
		// IProtocol Methods
		BOOL Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask);
		BOOL CreateSocket(ISocket * &pSocket);
		BOOL Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff);
		BOOL Send(void);
		BOOL Poll(void);
		BOOL NetStat(IDiagOutput *pOut);
		BOOL SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff);

		// IQueue Methods
		BOOL Queue(UINT uFace, IPREF Gate, CBuffer *pBuff);

	protected:
		// Queue Entry
		struct CEntry
		{
			UINT      m_uFace;
			CIpAddr   m_Gate;
			CBuffer * m_pBuff;
			};

		// Data Members
		ULONG	        m_uRefs;
		IRouter	      * m_pRouter;
		INetUtilities * m_pUtils;
		UINT	        m_uMask;
		IMutex        * m_pLock;
		ISemaphore    * m_pFlag;
		CEntry          m_Queue[64];
		UINT            m_uHead;
		UINT            m_uTail;
	};

// End of File

#endif

