
#include "Intern.hpp"

#include "ModemCell.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ShortMessage.hpp"

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cellular Connection
//

// Static Data

char const CModemCell::m_Hex[] = "0123456789ABCDEF";

// Constructor

CModemCell::CModemCell(CPpp *pPpp, CConfigPpp const &Config) : CModem(pPpp, Config)
{
	m_pSMS = Config.m_pSMS;

	if( m_pSMS ) {

		m_pSMS->SetSend(this);
	}

	m_pSendNum = NULL;

	m_pSendMsg = NULL;

	m_uInst    = Config.m_DevInst;

	m_uSignal  = 99;

	StdSetRef();

	piob->RegisterSingleton("net.cell", m_uInst, this);
}

// Destructor

CModemCell::~CModemCell(void)
{
	AddRef();

	piob->RevokeSingleton("net.cell", m_uInst);

	if( m_pSMS ) {

		m_pSMS->SetSend(NULL);
	}

	/*free(m_pSendNum);*/

	/*free(m_pSendMsg);*/
}

// Overridables

BOOL CModemCell::Init(void)
{
	DWORD dwSubMask;

	if( BasicInit() ) {

		dwSubMask = 0;//(m_InitMask & 0x0F00) >> 8;			

		if( 1/*m_InitMask & 0x4000*/ ) {

			m_uSMSMode = 0;
		}
		else {
			m_uSMSMode = 1;
		}

		PCTXT pList[] = {

			"+IFC=0,0",
			"+ICF=3",
			"+FCLASS=0",
			"+CBST=7,0,0",
			NULL
		};

		if( ModemList(pList, dwSubMask) ) {

			if( m_fServer ) {

				if( 1/*m_InitMask & 0x1000*/ ) {

					ModemCommand("+CSNS=4");
				}
			}

			if( m_pSMS ) {

				if( 1/*m_InitMask & 0x2000*/ ) {

					if( m_uSMSMode == 0 ) {

						ModemCommand("+CMGF=0");
					}
					else
						ModemCommand("+CMGF=1");
				}

				PCTXT pList[] = {

					"+CNMI=0",
					NULL
				};

				if( 1/*m_InitMask & 0x8000*/ ) {

					if( !ModemList(pList) ) {

						return FALSE;
					}
				}
			}

			m_fRing = FALSE;

			return TRUE;
		}
	}

	return FALSE;
}

// IUnknown

HRESULT CModemCell::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ICellStatus);

	StdQueryInterface(ICellStatus);

	return E_NOINTERFACE;
}

ULONG CModemCell::AddRef(void)
{
	StdAddRef();
}

ULONG CModemCell::Release(void)
{
	StdRelease();
}

// ICellStatus

BOOL CModemCell::GetCellStatus(CCellStatusInfo &Info)
{
	Info.m_fValid    = TRUE;

	Info.m_fOnline   = (m_uActSession && m_uActSession == m_uReqSession);

	Info.m_fReqOff   = FALSE;

	Info.m_fRegister = (m_uSignal != 99);

	Info.m_fRoam     = FALSE;

	Info.m_Network   = "Unknown";

	Info.m_Service   = "Unknown";

	Info.m_uSignal   = m_uSignal;

	return TRUE;
}

BOOL CModemCell::SendCommand(PCTXT pCmd)
{
	return FALSE;
}

// SMS Methods

BOOL CModemCell::SendSMS(PCTXT pNumber, PCTXT pMessage)
{
	if( !m_pSendNum ) {

		if( m_pPpp->GetThread() ) {

			m_pSendNum = strdup(pNumber);

			m_pSendMsg = strdup(pMessage);

			return TRUE;
		}
	}

	Sleep(1000);

	return FALSE;
}

// SMS Support

BOOL CModemCell::CheckMessages(BOOL fRecv)
{
	if( m_pSMS ) {

		if( fRecv ) {

			if( IsModemReady() && CheckService() ) {

				SetStatus("SMS");

				CheckIncoming();

				if( m_pSendMsg ) {

					CheckOutgoing();
				}

				return TRUE;
			}

			return FALSE;
		}
		else {
			if( m_pSendMsg ) {

				if( IsModemReady() && CheckService() ) {

					SetStatus("SMS");

					CheckOutgoing();

					return TRUE;
				}

				return FALSE;
			}
		}
	}

	Sleep(50);

	return TRUE;
}

void CModemCell::CheckIncoming(void)
{
	UINT uIndex[16];

	UINT uCount = 0;

	while( (uCount = ListMessages(uIndex, elements(uIndex))) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CShortMessage Msg;

			ReadMessage(uIndex[n], Msg);

			PCTXT pNum = Msg.GetNumber();

			PCTXT pMsg = Msg.GetMessage();

			if( m_pSMS->RecvSMS(pNum, pMsg) ) {

				DeleteMessage(uIndex[n]);

				continue;
			}

			return;
		}
	}
}

void CModemCell::CheckOutgoing(void)
{
	while( m_pSendMsg ) {

		CShortMessage Msg;

		Msg.SetNumber(m_pSendNum);

		Msg.SetMessage(m_pSendMsg);

		if( SendMessage(Msg) ) {

			free(m_pSendNum);

			free(m_pSendMsg);

			m_pSendNum = NULL;

			m_pSendMsg = NULL;

			Sleep(500);
		}
		else
			break;
	}
}

BOOL CModemCell::SendMessage(CShortMessage &Msg)
{
	if( m_uSMSMode == 0 ) {

		UINT uCount = Msg.BuildPDU(m_bPDU);

		while( uCount == 95 ) {

			// NOTE -- Hack for modem that chokes on length of 95.

			Msg.AppendSpace();

			uCount = Msg.BuildPDU(m_bPDU);
		}

		char sSend[128];

		SPrintf(sSend, "+CMGS=%u", uCount - 1);

		WriteCommand(sSend);

		if( CheckReply() == codePrompt ) {

			Sleep(100);

			if( SendBody(m_bPDU, uCount) ) {

				return TRUE;
			}

			WaitQuiet();
		}
	}
	else {
		char sSend[128];

		SPrintf(sSend, "+CMGS=\"%s\"", Msg.GetNumber());

		WriteCommand(sSend);

		if( CheckReply() == codePrompt ) {

			Sleep(100);

			PCTXT pMsg = Msg.GetMessage();

			m_pData->ClearRx();

			m_pData->Write(PCBYTE(pMsg), strlen(pMsg), FOREVER);

			m_pData->Write(0x1A, FOREVER);

			UINT uCode = CheckReply();

			if( uCode == codeExtended ) {

				if( !strncmp(m_sRest, "CMGS:", 5) ) {

					if( CheckReply() == codeOK ) {

						return TRUE;
					}
				}
			}

			if( uCode == codeOK ) {

				return TRUE;
			}
		}

		return FALSE;
	}

	return FALSE;
}

UINT CModemCell::ListMessages(PUINT pIndex, UINT uLimit)
{
	UINT uCount = 0;

	if( m_uSMSMode == 0 ) {

		WriteCommand("+CMGL=4");
	}
	else
		WriteCommand("+CMGL=\"ALL\"");

	for( ;;) {

		UINT uCode = CheckReply();

		if( uCode == codeOK ) {

			break;
		}

		if( uCode == codeExtended ) {

			if( !strncmp(m_sRest, "CMGL:", 5) ) {

				if( uCount < uLimit ) {

					pIndex[uCount] = strtol(m_sRest + 5, NULL, 10);

					uCount++;
				}

				SkipBody();

				continue;
			}
		}

		WaitQuiet();

		break;
	}

	return uCount;
}

BOOL CModemCell::DeleteMessage(UINT uIndex)
{
	char sKill[128];

	SPrintf(sKill, "+CMGD=%u", uIndex);

	WriteCommand(sKill);

	return CheckReply() == codeOK;
}

BOOL CModemCell::ReadMessage(UINT uIndex, CShortMessage &Msg)
{
	char sRead[128];

	SPrintf(sRead, "+CMGR=%u", uIndex);

	WriteCommand(sRead);

	if( CheckReply() == codeExtended ) {

		if( !strncmp(m_sRest, "CMGR:", 5) ) {

			Msg.SetIndex(uIndex);

			ParseBody(Msg);

			if( m_uSMSMode == 1 ) {

				char delims[] = "\"\"\"\"\",,\"\"";

				char *result = NULL;

				int i = 0;

				result = strtok(m_sRest, delims);

				while( result != NULL ) {

					switch( i ) {

						case 2:
							Msg.SetNumber(result);
							break;
					}

					i++;

					result = strtok(NULL, delims);
				}
			}

		/*Msg.Dump();*/

			return CheckReply() == codeOK;
		}

		WaitQuiet();
	}

	return FALSE;
}

BOOL CModemCell::SendBody(PCBYTE pPDU, UINT uCount)
{
	m_pData->ClearRx();

	for( UINT n = 0; n < uCount; n++ ) {

		BYTE bData = pPDU[n];

		m_pData->Write(m_Hex[bData/16], FOREVER);

		m_pData->Write(m_Hex[bData%16], FOREVER);
	}

	m_pData->Write(0x1A, FOREVER);

	if( CheckReply(30000) == codeExtended ) {

		if( !strncmp(m_sRest, "CMGS:", 5) ) {

			if( CheckReply() == codeOK ) {

				return TRUE;
			}
		}
	}

	m_pData->Write(0x1B, FOREVER);

	return FALSE;
}

BOOL CModemCell::ParseBody(CShortMessage &Msg)
{
	SetTimer(10000);

	UINT uState = 0;

	BYTE bData  = 0;

	UINT uPos   = 0;

	for( ;;) {

		UINT uData;

		if( (uData = m_pData->Read(GetTimer())) == NOTHING ) {

			break;
		}

		if( uData == '\n' ) {

			if( m_uSMSMode == 0 ) {

				Msg.ParsePDU(m_bPDU);
			}
			else {
				m_bPDU[uPos] = 0;

				Msg.SetMessage(PCTXT(m_bPDU));
			}

			return TRUE;
		}

		if( !uState ) {

			if( m_uSMSMode == 0 ) {

				bData  = FromHex(uData);

				uState = 1;
			}
			else {
				if( uData != '\r' ) {

					m_bPDU[uPos++] = BYTE(toascii(uData));
				}
			}
		}
		else {
			bData <<= 4;

			bData  |= FromHex(uData);

			m_bPDU[uPos++] = bData;

			uState = 0;
		}
	}

	return FALSE;
}

BOOL CModemCell::SkipBody(void)
{
	SetTimer(10000);

	for( ;;) {

		UINT uData;

		if( (uData = m_pData->Read(GetTimer())) == NOTHING ) {

			break;
		}

		if( uData == '\n' ) {

			return TRUE;
		}
	}

	return FALSE;
}

BYTE CModemCell::FromHex(UINT uData)
{
	if( uData >= '0' && uData <= '9' ) {

		return BYTE(uData - '0');
	}

	if( uData >= 'A' && uData <= 'F' ) {

		return BYTE(uData - 'A' + 10);
	}

	if( uData >= 'a' && uData <= 'f' ) {

		return BYTE(uData - 'a' + 10);
	}

	return 0;
}

// Implementation

void CModemCell::CheckSignal(void)
{
	if( ModemCommand("+CSQ") == codeExtended ) {

		if( !strncmp(m_sRest, "CSQ:", 4) ) {

			if( GetReply(1000) == codeOK ) {

				m_uSignal = strtol(m_sRest+4, NULL, 10);

				SPrintf(m_sStatus, "IDLE %u", m_uSignal);

				Sleep(500);

				return;
			}
		}
	}

	m_uSignal = 99;

	SetStatus("IDLE 99");
}

BOOL CModemCell::CheckService(void)
{
	if( ModemCommand("+CREG?") == codeExtended ) {

		if( !strncmp(m_sRest, "CREG", 4) ) {

			PCTXT pFind = strchr(m_sRest, ',');

			UINT  uCode = pFind ? atoi(pFind + 1) : 0;

			if( GetReply(1000) == codeOK ) {

				switch( uCode ) {

					case 1:
					case 5:
						return TRUE;
				}
			}
		}

		TcpDebug(OBJ_MODEM, LEV_WARN, "no service\n");
	}

	return FALSE;
}

UINT CModemCell::CheckReply(UINT uTimeout)
{
	UINT uCode;

	for( ;;) {

		if( (uCode = GetReply(uTimeout)) == codeRing ) {

			m_fRing = TRUE;

			continue;
		}

		return uCode;
	}
}

UINT CModemCell::CheckReply(void)
{
	return CheckReply(10000);
}

BOOL CModemCell::CheckRinging(void)
{
	if( m_fRing ) {

		m_fRing = FALSE;

		return TRUE;
	}

	return GetReply(5000) == codeRing;
}

void CModemCell::WaitQuiet(void)
{
}

// End of File
