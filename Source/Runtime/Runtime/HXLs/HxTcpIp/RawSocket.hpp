
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RawSocket_HPP

#define	INCLUDE_RawSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MacHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRaw;

//////////////////////////////////////////////////////////////////////////
//
// Raw Socket
//

class CRawSocket : public ISocket
{
	public:
		// Constructor
		CRawSocket(void);

		// Binding
		void Bind(CRaw *pRaw);

		// Attributes
		BOOL IsFree(void) const;

		// Operations
		void Create(void);
		void NetStat(IDiagOutput *pOut);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket Methods
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &Ip, WORD Loc);
		HRM Connect(IPADDR const &Ip, WORD Rem);
		HRM Connect(IPADDR const &Ip, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Recv(CBuffer * &pBuff);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal (IPADDR &Ip);
		HRM GetRemote(IPADDR &Ip);
		HRM GetLocal (IPADDR &Ip, WORD &Port);
		HRM GetRemote(IPADDR &Ip, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

		// Event Handlers
		BOOL OnTest(CBuffer *pBuff);
		BOOL OnRecv(CBuffer *pBuff);
		BOOL OnSend(void);

		// Linked List
		PVOID        m_pRoot;
		CRawSocket * m_pNext;
		CRawSocket * m_pPrev;

	protected:
		// Static Data
		static UINT m_NextPort;

		// Data Members
		ULONG	  m_uRefs;
		CRaw	* m_pRaw;
		BOOL	  m_fUsed;
		BOOL	  m_fOpen;
		CBuffer	* m_pTxBuff;
		CBuffer	* m_pRxBuff[4];
		UINT	  m_uRxHead;
		UINT	  m_uRxTail;
		CIpAddr   m_Multi;
		WORD	  m_wMatch;

		// Implementation
		void ClearRx(void);
		void ClearTx(void);
		void WaitTx(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE BOOL CRawSocket::OnTest(CBuffer *pBuff)
{
	if( unlikely(m_fOpen) ) {

		UINT  uSize = pBuff->GetSize();

		PBYTE pData = pBuff->GetData();

		if( uSize >= 16 ) {

			CMacHeader *pHead = (CMacHeader *) pData;

			if( pHead->m_Type >= 1536 ) {

				if( !m_wMatch || pHead->m_Type == m_wMatch ) {

					return TRUE;
					}
				}
			else {
				PBYTE pDest = PBYTE(pHead + 1);

				if( !m_wMatch || *pDest == LOBYTE(m_wMatch) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// End of File

#endif

