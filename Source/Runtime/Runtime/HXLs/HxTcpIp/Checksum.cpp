
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Checksum Generation
//

global WORD  Checksum(PBYTE pData, UINT uCount);
global WORD  Checksum(PBYTE pData, UINT uCount, DWORD Load);
global DWORD BasicSum(PBYTE pData, UINT uCount);
global DWORD BasicSum(PBYTE pData, UINT uCount, DWORD Load);

///////////////////////////////////////////////////////////////////////
//								
// Checksum Generation
//

global WORD Checksum(PBYTE pData, UINT uCount)
{
	return Checksum(pData, uCount, 0);
	}

global WORD Checksum(PBYTE pData, UINT uCount, DWORD Load)
{
	DWORD Sum = BasicSum(pData, uCount, Load);

	while( HIWORD(Sum) ) {

		DWORD D1 = HIWORD(Sum);

		DWORD D2 = LOWORD(Sum);

		Sum = D1 + D2;
		}

	return HostToNet(LOWORD(~Sum));
	}

global DWORD BasicSum(PBYTE pData, UINT uCount)
{
	return BasicSum(pData, uCount, 0);
	}

global DWORD BasicSum(PBYTE pData, UINT uCount, DWORD Load)
{
	DWORD Sum = Load;

	while( uCount > 1 ) {

		WORD Data = NetToHost(PWORD(pData)[0]);

		Sum += Data;

		pData  += sizeof(WORD);

		uCount -= sizeof(WORD);
		}

	if( uCount ) {

		WORD Data = NetToHost(PWORD(pData)[0]);

		Sum += (Data & 0xFF00);
		}

	return Sum;
	}

// End of File
