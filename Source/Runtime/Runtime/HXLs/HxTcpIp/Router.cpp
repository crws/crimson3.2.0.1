
#include "Intern.hpp"

#include "Router.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IcmpProtocol.hpp"

#include "IpHeader.hpp"

#include "PseudoHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// IP Router
//

// Instance Pointer

CRouter * CRouter::m_pThis = NULL;

// Registration

global void Register_Router(void)
{
	piob->RegisterSingleton("net.ip", 0, (IRouter *) New CRouter);
}

global void Revoke_Router(void)
{
	piob->RevokeSingleton("net.ip", 0);
}

// Constructor

CRouter::CRouter(void)
{
	AfxAssert(!m_pThis);

	StdSetRef();

	m_pThis    = this;

	m_uProts   = 0;

	m_uFaces   = 0;

	m_uRoutes  = 0;

	m_uMultis  = 0;

	m_hThread  = NULL;

	m_fRouting = FALSE;

	m_fFarSide = FALSE;

	m_uSend	   = 0;

	m_pSend	   = Create_ManualEvent();

	m_pLock1   = Create_Rutex();

	m_pLock2   = Create_Qutex();

	piob->RegisterInstantiator("net.sock-tcp", Create_SockTcp);

	piob->RegisterInstantiator("net.sock-udp", Create_SockUdp);

	piob->RegisterInstantiator("net.sock-raw", Create_SockRaw);

	DiagRegister();

	AddLoopback();
}

// Destructor

CRouter::~CRouter(void)
{
	if( m_hThread ) {

		DestroyThread(m_hThread);
	}

	for( UINT i = 0; i < m_uFaces; i++ ) {

		CFace &Face = m_Face[i];

		AfxRelease(Face.m_pPacket);
	}

	for( UINT p = 0; p < m_uProts; p++ ) {

		CProt &Prot = m_Prot[p];

		AfxRelease(Prot.m_pDriver);
	}

	m_pSend->Release();

	m_pLock1->Release();

	m_pLock2->Release();

	DiagRevoke();

	piob->RevokeInstantiator("net.sock-tcp");

	piob->RevokeInstantiator("net.sock-udp");

	piob->RevokeInstantiator("net.sock-raw");

	m_pThis = NULL;
}

// IUnknown

HRESULT CRouter::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IRouter);

	StdQueryInterface(IRouter);

	StdQueryInterface(INetUtilities);

	StdQueryInterface(IPacketSink);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
}

ULONG CRouter::AddRef(void)
{
	StdAddRef();
}

ULONG CRouter::Release(void)
{
	StdRelease();
}

// IRouter Configuration

BOOL CRouter::AddEthernet(CConfigEth const &Config)
{
	IPacketDriver *pPack = Create_Ethernet(Config);

	if( pPack ) {

		BOOL fSafe = TRUE;

		UINT uFace = AddInterface(pPack, Config.m_Ip, Config.m_Mask, fSafe);

		if( uFace < NOTHING ) {

			AddRoutes(uFace);

			SetGateway(uFace, Config.m_Gate);

			return TRUE;
		}

		pPack->Release();
	}

	TcpDebug(OBJ_ROUTER, LEV_ERROR, "unable to add ethernet driver\n");

	return FALSE;
}

BOOL CRouter::AddPpp(CConfigPpp const &Config)
{
	IPacketDriver *pPack = Create_Ppp(Config);

	if( pPack ) {

		BOOL fSafe = (Config.m_uMode != pppClient);

		UINT uFace = AddInterface(pPack, IP_EMPTY, IP_BROAD, fSafe);

		if( uFace < NOTHING ) {

			AddRoutes(uFace);

			return TRUE;
		}

		pPack->Release();
	}

	TcpDebug(OBJ_ROUTER, LEV_ERROR, "unable to add ppp driver\n");

	return FALSE;
}

UINT CRouter::AddInterface(IPacketDriver *pPacket, IPREF Ip, IPREF Mask, BOOL fSafe)
{
	if( m_uFaces < elements(m_Face) ) {

		CFace &Face = m_Face[m_uFaces];

		Face.m_pPacket = pPacket;

		Face.m_Name    = pPacket->GetDeviceName();

		Face.m_Desc    = Face.m_Name;

		Face.m_Id      = NOTHING;

		Face.m_Ip      = Ip;

		Face.m_Mask    = Mask;

		Face.m_fSafe   = fSafe;

		Face.m_fUp     = FALSE;

		return m_uFaces++;
	}

	return NOTHING;
}

UINT CRouter::AddNullInterface(void)
{
	if( m_uFaces < elements(m_Face) ) {

		CFace &Face = m_Face[m_uFaces];

		Face.m_pPacket = NULL;

		Face.m_Name    = "null";

		Face.m_Desc    = Face.m_Name;

		Face.m_fSafe   = FALSE;

		Face.m_fUp     = FALSE;

		return m_uFaces++;
	}

	return NOTHING;
}

BOOL CRouter::AddLoopback(void)
{
	if( m_uFaces == 0 ) {

		IPacketDriver *pPack = Create_Loopback();

		if( pPack ) {

			CIpAddr Addr(127, 0, 0, 1);

			CIpAddr Mask(255, 0, 0, 0);

			BOOL fSafe = TRUE;

			UINT uFace = AddInterface(pPack, Addr, Mask, fSafe);

			if( uFace < NOTHING ) {

				AddRoute(uFace, Addr, Mask, Addr);

				return TRUE;
			}

			pPack->Release();
		}

		return FALSE;
	}

	return FALSE;
}

void CRouter::EnableRouting(BOOL fFarSide)
{
	m_fFarSide = fFarSide;

	m_fRouting = TRUE;
}

// IRouter User APIs

BOOL CRouter::Open(void)
{
	AddProtocols();

	OpenInterfaces();

	UpdateMulticast();

	OpenProtocols();

	m_hThread = CreateClientThread(this, 0, 80000);

	return TRUE;
}

BOOL CRouter::Close(void)
{
	if( m_hThread ) {

		// TODO -- Kill open TCP sockets, and interlock to ensure
		// that when closed, nothing can happen from sockets!!!

		DestroyThread(m_hThread);

		for( UINT i = 0; i < m_uFaces; i++ ) {

			CFace &Face = m_Face[i];

			if( Face.m_pPacket ) {

				Face.m_pPacket->Close();

				Face.m_pPacket->Release();

				Face.m_pPacket = NULL;
			}
		}

		m_uFaces  = 0;

		m_uRoutes = 0;

		m_hThread = NULL;

		AddLoopback();
	}

	return TRUE;
}

ISocket * CRouter::CreateSocket(WORD ID)
{
	ISocket *pSocket = NULL;

	for( UINT p = 0; p < m_uProts; p++ ) {

		CProt &Prot = m_Prot[p];

		if( ID == Prot.m_ID ) {

			Prot.m_pDriver->CreateSocket(pSocket);

			if( !pSocket ) {

				TcpDebug(OBJ_ROUTER, LEV_WARN, "no socket available\n");

				return NULL;
			}

			return pSocket;
		}
	}

	TcpDebug(OBJ_ROUTER, LEV_ERROR, "unknown socket protocol %4.4X\n", ID);

	return NULL;
}

// IRouter Protocol Calls

DWORD CRouter::GetPseudoSum(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff)
{
	CIpAddr Gate;

	UINT n = FindRoute(Dest, From, Gate);

	if( n < NOTHING ) {

		CPsHeader Pseudo;

		Pseudo.m_Dest     = Dest;

		Pseudo.m_Src      = From;

		Pseudo.m_Protocol = BYTE(Prot);

		Pseudo.m_Zero     = 0;

		Pseudo.m_Length   = HostToNet(WORD(pBuff->GetSize()));

		return Pseudo.GetChecksum();
	}

	return 0;
}

BOOL CRouter::Send(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff, IProtocol *pProt)
{
	if( Dest == IP_BROAD || Prot == IP_RAW ) {

		BOOL fSend = FALSE;

		UINT uInit = pBuff->GetInit();

		for( UINT i = 1; i < m_uFaces; i++ ) {

			if( m_Face[i].m_pPacket ) {

				CIpAddr  Mask;

				CMacAddr Mac;

				GetConfig(i, From, Mask, Mac);

				pBuff->SetInit(uInit);

				pProt->SetHeader(Dest, From, pBuff);

				AddHeader(From, Dest, Prot, pBuff);

				if( Send(i, Dest, pBuff) ) {

					fSend = TRUE;
				}
			}
		}

		return fSend;
	}
	else {
		CIpAddr Gate;

		UINT n = FindRoute(Dest, From, Gate);

		if( n < NOTHING ) {

			if( Prot & IP_STEALTH ) {

				if( !m_Face[n].m_fSafe ) {

					return TRUE;
				}

				Prot &= ~IP_STEALTH;
			}

			pProt->SetHeader(Dest, From, pBuff);

			AddHeader(From, Dest, Prot, pBuff);

			return Send(n, Gate, pBuff);
		}

		return FALSE;
	}
}

BOOL CRouter::Send(UINT uFace, IPREF Gate, CBuffer *pBuff)
{
	if( m_Face[uFace].m_pPacket->Send(Gate, pBuff) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CRouter::Queue(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff)
{
	CIpAddr Gate;

	UINT n = FindRoute(Dest, From, Gate);

	if( n < NOTHING ) {

		if( Prot & IP_STEALTH ) {

			if( !m_Face[n].m_fSafe ) {

				BuffRelease(pBuff);

				return TRUE;
			}

			Prot &= ~IP_STEALTH;
		}

		UINT      uSize = pBuff->GetSize();

		CIpHeader *pIp  = BuffAddHead(pBuff, CIpHeader);

		pIp->SetHeader(From, Dest, Prot, uSize);

		pIp->HostToNet();

		pIp->AddChecksum();

		return Queue(n, Gate, pBuff);
	}

	BuffRelease(pBuff);

	return FALSE;
}

BOOL CRouter::Queue(UINT uFace, IPREF Gate, CBuffer *pBuff)
{
	return m_pQueue->Queue(uFace, Gate, pBuff);
}

void CRouter::SendReq(UINT uMask, BOOL fState)
{
	CAutoLock Lock(m_pLock1);

	if( fState ) {

		if( m_uSend == 0 ) {

			m_uSend = uMask;

			m_pSend->Set();
		}
		else
			m_uSend |= uMask;
	}
	else {
		if( m_uSend == uMask ) {

			m_uSend = 0;

			m_pSend->Clear();
		}
		else
			m_uSend &= ~uMask;
	}
}

void CRouter::CopyOptions(ISocket *pSock, IPREF Ip)
{
	UINT uFace = FindRoute(Ip);

	if( uFace < NOTHING ) {

		CFace &Face   = m_Face[uFace];

		UINT  uList[] = { OPT_RECV_MSS, OPT_SEND_MSS };

		for( UINT n = 0; n < elements(uList); n++ ) {

			UINT uOption = uList[n];

			UINT uValue  = 0;

			if( Face.m_pPacket->GetSockOption(uOption, uValue) ) {

				pSock->SetOption(uOption, uValue);
			}
		}
	}
}

void CRouter::EnableMulticast(IPREF Ip, BOOL fEnable)
{
	CAutoLock Lock(m_pLock2);

	BOOL fUpdate = FALSE;

	UINT n;

	for( n = 0; n < m_uMultis; n++ ) {

		if( m_Multi[n].m_Ip == Ip ) {

			break;
		}
	}

	if( fEnable ) {

		if( n < m_uMultis ) {

			m_Multi[n].m_uRefs++;
		}
		else {
			m_Multi[n].m_Ip    = Ip;

			m_Multi[n].m_uRefs = 1;

			m_uMultis++;

			fUpdate = TRUE;
		}
	}
	else {
		if( n < m_uMultis ) {

			if( !--m_Multi[n].m_uRefs ) {

				for( UINT i = n; i < m_uMultis - 1; i++ ) {

					m_Multi[i] = m_Multi[i+1];
				}

				m_uMultis--;

				fUpdate = TRUE;
			}
		}
	}

	if( fUpdate ) {

		UpdateMulticast();
	}
}

// IRouter Packet Calls

BOOL CRouter::SetConfig(UINT uFace, IPREF Ip, IPREF Mask)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			if( Ip.IsEmpty() ) {

				CheckRoutes(uFace, FALSE);
			}

			if( Face.m_pPacket->SetIpAddress(Ip, Mask) ) {

				CAutoLock Lock(m_pLock1);

				Face.m_Ip   = Ip;

				Face.m_Mask = Mask;

				Lock.Free();

				RemoveRoutes(uFace);

				AddRoutes(uFace);

				if( !Ip.IsEmpty() ) {

					CheckRoutes(uFace, TRUE);
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CRouter::GetConfig(UINT uFace, CIpAddr &Ip, CIpAddr &Mask, CMacAddr &MAC)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			CAutoLock Lock(m_pLock1);

			if( Face.m_Ip.IsExperimental() ) {

				Ip   = IP_EMPTY;

				Mask = IP_EMPTY;
			}
			else {
				Ip   = Face.m_Ip;

				Mask = Face.m_Mask;
			}

			Face.m_pPacket->GetMacAddress(MAC);

			return TRUE;
		}
	}

	return FALSE;
}

// IRouter Route Management

void CRouter::SetGateway(UINT uFace, IPREF Ip)
{
	if( !Ip.IsEmpty() ) {

		RemoveRoute(NOTHING, IP_EMPTY, IP_EMPTY);

		AddRoute(uFace, IP_EMPTY, IP_EMPTY, Ip);
	}

	m_Face[uFace].m_pPacket->SetIpGateway(Ip);
}

BOOL CRouter::GetGateway(CIpAddr &Ip)
{
	CIpAddr Dest, From;

	return FindRoute(Dest, From, Ip) != NOTHING;
}

void CRouter::AddRoute(UINT uFace, IPREF Ip, IPREF Mask, IPREF Gate)
{
	CAutoLock Lock(m_pLock1);

	CRoute &Route = m_Route[m_uRoutes];

	BOOL   fLocal = (Ip == IP_EMPTY || Ip == IP_BROAD || Gate == IP_EMPTY);

	Route.m_uFace = uFace;

	Route.m_Ip    = Ip & Mask;

	Route.m_Mask  = Mask;

	Route.m_Gate  = Gate;

	Route.m_uTarg = fLocal ? uFace : FindInterface(Gate);

	m_uRoutes++;
}

void CRouter::RemoveRoute(UINT uFace, IPREF Ip, IPREF Mask)
{
	for( UINT r = 0; r < m_uRoutes; r++ ) {

		CRoute &Route = m_Route[r];

		if( uFace == NOTHING || Route.m_uFace == uFace ) {

			if( Route.m_Ip == Ip ) {

				if( Route.m_Mask == Mask ) {

					CAutoLock Lock(m_pLock1);

					for( UINT s = 0; s < m_uRoutes - r - 1; s++ ) {

						m_Route[r+s] = m_Route[r+s+1];
					}

					m_uRoutes--;

					r--;
				}
			}
		}
	}
}

void CRouter::AddRoutes(UINT uFace)
{
	CFace &Face = m_Face[uFace];

	if( Face.m_Ip.IsEmpty() ) {

		if( Face.m_Mask != IP_BROAD ) {

			AddRoute(uFace, IP_BROAD, IP_BROAD, IP_EMPTY);
		}
	}
	else {
		if( !Face.m_Ip.IsExperimental() ) {

			AddRoute(uFace, Face.m_Ip, IP_BROAD, IP_LOOP);

			if( Face.m_Mask != IP_BROAD ) {

				AddRoute(uFace, Face.m_Ip, Face.m_Mask, Face.m_Ip);

				AddRoute(uFace, IP_BROAD, IP_BROAD, Face.m_Ip);

				AddRoute(uFace, CIpAddr(224, 0, 0, 0), CIpAddr(240, 0, 0, 0), Face.m_Ip);

				AddRoute(uFace, IP_EXPER, IP_BROAD, Face.m_Ip);
			}
		}
		else
			AddRoute(uFace, IP_EXPER, IP_BROAD, Face.m_Ip);
	}
}

void CRouter::RemoveRoutes(UINT uFace)
{
	for( UINT r = 0; r < m_uRoutes; r++ ) {

		CRoute &Route = m_Route[r];

		if( Route.m_uFace == uFace ) {

			if( Route.m_Gate.IsEmpty() ) {

				if( !Route.m_Ip.IsBroadcast() ) {

					continue;
				}
			}

			CAutoLock Lock(m_pLock1);

			for( UINT s = 0; s < m_uRoutes - r - 1; s++ ) {

				m_Route[r+s] = m_Route[r+s+1];
			}

			m_uRoutes--;

			r--;
		}
	}
}

// INetUtilities

BOOL CRouter::SetInterfaceNames(CStringArray const &Names)
{
	if( Names.GetCount() == m_uFaces ) {

		for( UINT uFace = 0; uFace < m_uFaces; uFace++ ) {

			CFace &Face = m_Face[uFace];

			if( Face.m_Name != Names[uFace] ) {

				AfxAssert(FALSE);

				return FALSE;
			}
		}

		return TRUE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

BOOL CRouter::SetInterfaceDescs(CStringArray const &Descs)
{
	for( UINT uFace = 0; uFace < m_uFaces; uFace++ ) {

		CFace &Face = m_Face[uFace];

		Face.m_Desc = Descs[uFace];
	}

	return TRUE;
}

BOOL CRouter::SetInterfacePaths(CStringArray const &Paths)
{
	for( UINT uFace = 0; uFace < m_uFaces; uFace++ ) {

		CFace &Face = m_Face[uFace];

		Face.m_Path = Paths[uFace];
	}

	return TRUE;
}

BOOL CRouter::SetInterfaceIds(CUIntArray const &Ids)
{
	for( UINT uFace = 0; uFace < m_uFaces; uFace++ ) {

		CFace &Face = m_Face[uFace];

		Face.m_Id   = Ids[uFace];
	}

	return TRUE;
}

UINT CRouter::GetInterfaceCount(void)
{
	return m_uFaces;
}

UINT CRouter::FindInterface(CString const &Name)
{
	for( UINT uFace = 0; uFace < m_uFaces; uFace++ ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_Name == Name ) {

			return uFace;
		}
	}

	return NOTHING;
}

UINT CRouter::FindInterface(UINT Id)
{
	for( UINT uFace = 0; uFace < m_uFaces; uFace++ ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_Id == Id ) {

			return uFace;
		}
	}

	return NOTHING;
}

BOOL CRouter::HasInterfaceType(UINT uType)
{
	// TODO -- Store the type and a type mask!!!

	for( UINT uFace = 0; uFace < m_uFaces; uFace++ ) {

		if( GetInterfaceType(uFace) == uType ) {

			return TRUE;
		}
	}

	return FALSE;
}

UINT CRouter::GetInterfaceType(UINT uFace)
{
	// TODO -- Store the type and a type mask!!!

	if( uFace < m_uFaces ) {

		CFace   const &Face = m_Face[uFace];

		CString const &Name = Face.m_Name;

		if( Name == "lo" ) {

			return IT_LOOPBACK;
		}

		if( Name.StartsWith("eth") ) {

			return IT_ETHERNET;
		}

		if( Name.StartsWith("ppp") ) {

			return IT_CELLULAR;
		}
	}

	return IT_UNKNOWN;
}

UINT CRouter::GetInterfaceOrdinal(UINT uFace)
{
	if( uFace < m_uFaces ) {

		// LATER -- This isn't very pretty, but it works...

		CFace   const &Face = m_Face[uFace];

		CString const &Desc = Face.m_Desc;

		UINT           uPos = Desc.FindRev(' ');

		if( uPos < NOTHING ) {

			return atoi(Desc.Mid(uPos + 1)) - 1;
		}

		return 0;
	}

	return NOTHING;
}

BOOL CRouter::GetInterfaceName(UINT uFace, CString &Name)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			Name = Face.m_Name;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRouter::GetInterfaceDesc(UINT uFace, CString &Desc)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			Desc = Face.m_Desc;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRouter::GetInterfacePath(UINT uFace, CString &Path)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			Path = Face.m_Path;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRouter::GetInterfaceMac(UINT uFace, MACADDR &Mac)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			if( Face.m_pPacket->GetMacAddress(Mac) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CRouter::GetInterfaceAddr(UINT uFace, IPADDR &Addr)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_fUp ) {

			Addr = Face.m_Ip;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRouter::GetInterfaceMask(UINT uFace, IPADDR &Mask)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_fUp ) {

			Mask = Face.m_Mask;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRouter::GetInterfaceGate(UINT uFace, IPADDR &Gate)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_fUp ) {

			CIpAddr Test;

			if( GetGateway(Test) ) {

				if( Test.OnSubnet(Face.m_Ip, Face.m_Mask) ) {

					Gate = Test;

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CRouter::GetInterfaceStatus(UINT uFace, CString &Status)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			Status = Face.m_pPacket->GetStatus();

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRouter::IsInterfaceUp(UINT uFace)
{
	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		return Face.m_fUp;
	}

	return FALSE;
}

DWORD CRouter::GetOption(BYTE bType)
{
	UINT Data = 0;

	for( UINT i = 0; i < m_uFaces; i++ ) {

		CFace &Face = m_Face[i];

		if( Face.m_pPacket ) {

			if( Face.m_pPacket->GetDhcpOption(bType, Data) ) {

				return Data;
			}
		}
	}

	return 0;
}

DWORD CRouter::GetOption(BYTE bType, UINT uFace)
{
	UINT Data = 0;

	if( uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			if( Face.m_pPacket->GetDhcpOption(bType, Data) ) {

				return Data;
			}
		}
	}

	return 0;
}

BOOL CRouter::IsBroadcast(IPADDR const &IP)
{
	UINT uFace = FindRoute(IP);

	if( uFace < NOTHING ) {

		CIpAddr const &Ref = (CIpAddr const &) IP;

		return Ref.IsBroadcast(m_Face[uFace].m_Mask);
	}

	return FALSE;
}

BOOL CRouter::GetBroadcast(IPADDR const &IP, IPADDR &Broad)
{
	UINT uFace = FindRoute(IP);

	if( uFace < NOTHING ) {

		CIpAddr &Ref = (CIpAddr &) Broad;

		Ref = m_Face[uFace].m_Ip;

		Ref.MakeBroadcast(m_Face[uFace].m_Mask);

		return TRUE;
	}

	return FALSE;
}

BOOL CRouter::EnumBroadcast(UINT &uFace, IPADDR &Broad)
{
	CIpAddr &Ref = (CIpAddr &) Broad;

	for( UINT n = 0; n < m_uFaces; n++ ) {

		uFace = (uFace + 1) % m_uFaces;

		Ref   = m_Face[uFace].m_Ip;

		if( !Ref.IsLoopback() && !Ref.IsExperimental() ) {

			Ref.MakeBroadcast(m_Face[uFace].m_Mask);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRouter::IsFastAddress(IPADDR const &IP)
{
	UINT uFace = FindRoute(IP);

	if( uFace > 0 && uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		if( Face.m_pPacket ) {

			UINT Data;

			if( Face.m_pPacket->GetDhcpOption(0xFF, Data) ) {

				return TRUE;
			}
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CRouter::IsSafeAddress(IPADDR const &IP)
{
	UINT uFace = FindRoute(IP);

	if( uFace > 0 && uFace < m_uFaces ) {

		CFace &Face = m_Face[uFace];

		return Face.m_fSafe;
	}

	return TRUE;
}

BOOL CRouter::GetDefaultGateway(IPADDR const &IP, IPADDR &Gate)
{
	CIpAddr Test;

	if( GetGateway(Test) ) {

		Gate = Test;

		return TRUE;
	}

	return FALSE;
}

UINT CRouter::Ping(IPADDR const &IP, UINT uTimeout)
{
	ISocket *pSocket = CreateSocket(IP_ICMP);

	if( pSocket ) {

		pSocket->Connect(IP, 0, 0);

		BYTE bData[52];

		BYTE bRecv[52];

		UINT uSize = sizeof(bData);

		for( UINT n = 0; n < uSize; n++ ) {

			bData[n] = 'A' + n % 26;
		}

		if( pSocket->Send(bData, uSize) == S_OK ) {

			SetTimer(uTimeout);

			while( GetTimer() ) {

				uSize = sizeof(bRecv);

				if( pSocket->Recv(bRecv, uSize) == S_OK ) {

					UINT uLeft = GetTimer();

					pSocket->Close();

					pSocket->Release();

					if( uSize == sizeof(bRecv) ) {

						if( !memcmp(bData, bRecv, uSize) ) {

							return uTimeout - uLeft;
						}
					}

					return NOTHING;
				}

				Sleep(5);
			}
		}

		pSocket->Close();

		pSocket->Release();
	}

	return NOTHING;
}

// IPacketSink

void CRouter::OnLinkActive(UINT uFace)
{
	if( uFace < m_uFaces ) {

		if( !m_Face[uFace].m_fUp ) {

			AfxTrace("ip: interface %u up\n", uFace);

			m_Face[uFace].m_fUp = TRUE;
		}
	}
}

void CRouter::OnLinkDown(UINT uFace)
{
	if( uFace < m_uFaces ) {

		if( m_Face[uFace].m_fUp ) {

			AfxTrace("ip: interface %u down\n", uFace);

			m_Face[uFace].m_fUp = FALSE;
		}
	}
}

void CRouter::OnPacket(UINT uFace, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	CIpHeader * pIp = BuffStripHead(pBuff, CIpHeader);

	if( pIp->m_Version == 4 ) {

		if( pIp->TestChecksum() ) {

			pIp->NetToHost();

			UINT uSize = pBuff->GetSize();

			UINT uData = pIp->GetDataLength();

			if( uSize > uData ) {

				UINT uStrip = uSize - uData;

				pBuff->StripTail(uStrip);
			}

			if( uData > uSize ) {

				TcpDebug(OBJ_ROUTER, LEV_WARN, "data size invalid\n");

				return;
			}

			if( pIp->m_Fragment & 0x2FFF ) {

				TcpDebug(OBJ_ROUTER, LEV_WARN, "fragments not supported\n");

				return;
			}

			if( !RoutePacket(uFace, pBuff, pIp) ) {

				CPsHeader Pseudo(pIp);

				for( UINT p = 0; p < m_uProts; p++ ) {

					CProt &Prot = m_Prot[p];

					if( pIp->m_Protocol == Prot.m_ID ) {

						UINT uOpts = pIp->GetOptsLength();

						pBuff->StripHead(uOpts);

						if( !Prot.m_pDriver->Recv(uFace, m_Face[uFace].m_Ip, Pseudo, pBuff) ) {

							TcpDebug(OBJ_ROUTER, LEV_TRACE, "unknown port\n");

							SendUnreachable(pIp, UNREACH_PORT);
						}
						else
							Buff.TakeOver();

						return;
					}
				}

				if( !pIp->m_IpDest.IsGroup() ) {

					TcpDebug(OBJ_ROUTER, LEV_WARN, "unknown protocol %u\n", pIp->m_Protocol);

					SendUnreachable(pIp, UNREACH_PROT);
				}
			}

			return;
		}

		TcpDebug(OBJ_ROUTER, LEV_WARN, "checksum invalid\n");

		return;
	}

	TcpDebug(OBJ_ROUTER, LEV_WARN, "version invalid\n");
}

// IClientProcess

BOOL CRouter::TaskInit(UINT uTask)
{
	SetThreadName("IpRouter");

	return TRUE;
}

INT CRouter::TaskExec(UINT uTask)
{
	UINT t1 = GetTickCount();

	for( ;;) {

		UINT t2 = GetTickCount();

		PollForSend();

		if( t2 - t1 > ToTicks(100) ) {

			PollProtocols();

			PollDrivers();

			t1 = t2;
		}

		m_pSend->Wait(100);
	}
}

void CRouter::TaskStop(UINT uTask)
{
}

void CRouter::TaskTerm(UINT uTask)
{
}

// IDiagProvider

UINT CRouter::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);

		case 2:
			return DiagConfig(pOut, pCmd);

		case 3:
			return DiagRoutes(pOut, pCmd);
	}

	return 0;
}

// ICMP Calls

void CRouter::SendUnreachable(CIpHeader *pIp, BYTE bCode)
{
	CIcmp *pIcmp = (CIcmp *) m_Prot[2].m_pDriver;

	pIcmp->SendUnreachable(pIp, bCode);
}

void CRouter::SendTimeExceeded(CIpHeader *pIp, BYTE bCode)
{
	CIcmp *pIcmp = (CIcmp *) m_Prot[2].m_pDriver;

	pIcmp->SendTimeExceeded(pIp, bCode);
}

void CRouter::SendHostRedirect(CIpHeader *pIp, IPREF Ip)
{
	CIcmp *pIcmp = (CIcmp *) m_Prot[2].m_pDriver;

	pIcmp->SendHostRedirect(pIp, Ip);
}

// Packet Routing

BOOL CRouter::RoutePacket(UINT uFace, CBuffer *pBuff, CIpHeader *pIp)
{
	CAutoBuffer Buff(pBuff);

	CFace &Face = m_Face[uFace];

	if( Face.m_Ip.IsExperimental() ) {

		if( pIp->m_IpDest.IsExperimental() ) {

			Buff.TakeOver();

			return FALSE;
		}

		return TRUE;
	}

	if( !Face.m_Ip.IsLoopback() ) {

		if( pIp->m_IpDest == Face.m_Ip ) {

			Buff.TakeOver();

			return FALSE;
		}

		if( pIp->m_IpDest.IsBroadcast(Face.m_Mask) ) {

			Buff.TakeOver();

			return FALSE;
		}

		if( pIp->m_IpDest.IsMulticast() ) {

			Buff.TakeOver();

			return FALSE;
		}

		if( pIp->m_IpDest.IsExperimental() ) {

			Buff.TakeOver();

			return FALSE;
		}

		if( pIp->m_TTL <= 1 ) {

			SendTimeExceeded(pIp, EXCEED_TTL);
		}
		else {
			if( m_fRouting ) {

				CIpAddr From;

				CIpAddr Gate;

				UINT uDest = FindRoute(pIp->m_IpDest, From, Gate);

				if( uDest < NOTHING ) {

					if( uDest == 0 ) {

						if( !m_fFarSide ) {

							SendUnreachable(pIp, UNREACH_NET);

							return TRUE;
						}

						return FALSE;
					}

					if( uDest == uFace ) {

						if( Gate.IsEmpty() ) {

							Buff.TakeOver();

							return FALSE;
						}

						SendHostRedirect(pIp, Gate);

						return TRUE;
					}

					CIpHeader *pIp = BuffAddHead(pBuff, CIpHeader);

					pIp->m_TTL -= 1;

					pIp->HostToNet();

					pIp->AddChecksum();

					Queue(uDest, Gate, pBuff);

					return TRUE;
				}
			}

			SendUnreachable(pIp, UNREACH_NET);
		}

		return TRUE;
	}

	Buff.TakeOver();

	return FALSE;
}

UINT CRouter::FindRoute(CIpAddr &Dest, CIpAddr &From, CIpAddr &Gate)
{
	if( Dest.IsDirected() ) {

		UINT n = Dest.GetDirection();

		Dest.MakeBroadcast();

		Gate.MakeBroadcast();

		return n;
	}
	else {
		CAutoLock Lock(m_pLock1);

		for( UINT p = 0; p < 2; p++ ) {

			for( UINT r = 0; r < m_uRoutes; r++ ) {

				CRoute &Route = m_Route[r];

				if( p == 0 && Route.m_Ip.IsEmpty() ) {

					continue;
				}

				if( Dest.OnSubnet(Route.m_Ip, Route.m_Mask) ) {

					if( Route.m_uTarg < NOTHING ) {

						if( From.IsEmpty() ) {

							if( Route.m_uFace < NOTHING ) {

								From = m_Face[Route.m_uFace].m_Ip;
							}
							else
								From = m_Face[Route.m_uTarg].m_Ip;
						}

						if( Route.m_Gate == m_Face[Route.m_uTarg].m_Ip ) {

							Gate = Dest;
						}
						else
							Gate = Route.m_Gate;

						return Route.m_uTarg;
					}
				}
			}
		}
	}

	TcpDebug(OBJ_ROUTER, LEV_WARN, "address %s is unreachable\n", PCTXT(Dest.GetAsText()));

	return NOTHING;
}

UINT CRouter::FindRoute(IPREF Dest)
{
	CIpAddr Work = Dest;

	CIpAddr From;

	CIpAddr Gate;

	return FindRoute(Work, From, Gate);
}

UINT CRouter::FindInterface(IPREF Dest)
{
	CAutoLock Lock(m_pLock1);

	for( UINT i = 0; i < m_uFaces; i++ ) {

		CFace &Face = m_Face[i];

		if( Face.m_Ip.IsEmpty() ) {

			continue;
		}

		if( Dest.OnSubnet(Face.m_Ip, Face.m_Mask) ) {

			return i;
		}
	}

	return NOTHING;
}

// Implementation

void CRouter::AddProtocols(void)
{
	if( !m_uProts ) {

		AddProtocol(Create_TCP(), IP_TCP);

		AddProtocol(Create_UDP(), IP_UDP);

		AddProtocol(Create_ICMP(), IP_ICMP);

		AddProtocol(Create_Raw(), IP_RAW);

		m_pQueue = Create_Queue();

		AddProtocol(m_pQueue, IP_QUEUE);
	}
}

BOOL CRouter::AddProtocol(IProtocol *pProt, WORD ID)
{
	if( m_uProts < elements(m_Prot) ) {

		TcpDebug(OBJ_ROUTER, LEV_INFO, "protocol %2.2X added\n", ID);

		CProt &Prot = m_Prot[m_uProts];

		Prot.m_pDriver = pProt;

		Prot.m_ID      = ID;

		Prot.m_uMask   = (1 << m_uProts);

		m_uProts++;

		return TRUE;
	}

	TcpDebug(OBJ_ROUTER, LEV_ERROR, "unable to add protocol %2.2X\n", ID);

	return FALSE;
}

void CRouter::OpenInterfaces(void)
{
	for( UINT i = 0; i < m_uFaces; i++ ) {

		CFace &Face = m_Face[i];

		if( Face.m_pPacket ) {

			Face.m_pPacket->Bind(this);

			Face.m_pPacket->Bind(this, i);

			Face.m_pPacket->Open();
		}
	}
}

void CRouter::OpenProtocols(void)
{
	for( UINT p = 0; p < m_uProts; p++ ) {

		CProt &Prot = m_Prot[p];

		UINT  uMask = Prot.m_uMask;

		Prot.m_pDriver->Bind(this, this, uMask);
	}
}

void CRouter::PollForSend(void)
{
	if( m_uSend ) {

		BOOL fFail = FALSE;

		for( UINT p = 0; p < m_uProts; p++ ) {

			CProt &Prot = m_Prot[p];

			if( m_uSend & Prot.m_uMask ) {

				if( !Prot.m_pDriver->Send() ) {

					fFail = TRUE;
				}
			}
		}

		if( fFail ) {

			Sleep(100);
		}
	}
}

void CRouter::PollProtocols(void)
{
	for( UINT p = 0; p < m_uProts; p++ ) {

		CProt &Prot = m_Prot[p];

		if( Prot.m_pDriver ) {

			Prot.m_pDriver->Poll();
		}
	}
}

void CRouter::PollDrivers(void)
{
	for( UINT i = 0; i < m_uFaces; i++ ) {

		CFace &Face = m_Face[i];

		if( Face.m_pPacket ) {

			Face.m_pPacket->Poll();
		}
	}
}

void CRouter::AddHeader(CIpAddr From, CIpAddr Dest, WORD Prot, CBuffer * pBuff)
{
	UINT       uSize = pBuff->GetSize();

	CIpHeader *pIp   = BuffAddHead(pBuff, CIpHeader);

	pIp->SetHeader(From, Dest, Prot, uSize);

	pIp->HostToNet();

	pIp->AddChecksum();
}

void CRouter::CheckRoutes(UINT uFace, BOOL fAdd)
{
	CFace &Face = m_Face[uFace];

	for( UINT r = 0; r < m_uRoutes; r++ ) {

		CRoute &Route = m_Route[r];

		if( Route.m_uFace == NOTHING ) {

			if( Route.m_Gate.OnSubnet(Face.m_Ip, Face.m_Mask) ) {

				Route.m_uTarg = fAdd ? uFace : NOTHING;
			}
		}
	}
}

void CRouter::UpdateMulticast(void)
{
	IPADDR *pList = New IPADDR[m_uMultis];

	UINT n, f;

	for( n = 0; n < m_uMultis; n++ ) {

		pList[n] = m_Multi[n].m_Ip;
	}

	for( f = 0; f < m_uFaces; f++ ) {

		if( m_Face[f].m_pPacket ) {

			m_Face[f].m_pPacket->SetMulticast(pList, m_uMultis);
		}
	}

	delete[] pList;
}

// Diagnostics

BOOL CRouter::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "ip");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		pDiag->RegisterCommand(m_uProv, 2, "config");

		pDiag->RegisterCommand(m_uProv, 3, "routes");

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

BOOL CRouter::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

UINT CRouter::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(5);

		pOut->SetColumn(0, "Prot", "%s");
		pOut->SetColumn(1, "Local", "%s");
		pOut->SetColumn(2, "Remote", "%s");
		pOut->SetColumn(3, "State", "%s");
		pOut->SetColumn(4, "Details", "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		for( UINT p = 0; p < m_uProts; p++ ) {

			CProt &Prot = m_Prot[p];

			Prot.m_pDriver->NetStat(pOut);
		}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CRouter::DiagConfig(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(5);

		pOut->SetColumn(0, "Index", "%u");
		pOut->SetColumn(1, "Name", "%s");
		pOut->SetColumn(2, "Address", "%s");
		pOut->SetColumn(3, "Mask", "%s");
		pOut->SetColumn(4, "Status", "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		for( UINT i = 0; i < m_uFaces; i++ ) {

			CFace &Face = m_Face[i];

			if( Face.m_pPacket ) {

				pOut->AddRow();

				pOut->SetData(0, i);

				pOut->SetData(1, PCTXT(Face.m_Name));

				pOut->SetData(2, PCTXT(Face.m_Ip.GetAsText()));

				pOut->SetData(3, PCTXT(Face.m_Mask.GetAsText()));

				pOut->SetData(4, PCTXT(Face.m_pPacket->GetStatus()));

				pOut->EndRow();
			}
		}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CRouter::DiagRoutes(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(5);

		pOut->SetColumn(0, "Group", "%u");
		pOut->SetColumn(1, "Address", "%s");
		pOut->SetColumn(2, "Mask", "%s");
		pOut->SetColumn(3, "Gateway", "%s");
		pOut->SetColumn(4, "Interface", "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		for( UINT i = 0; i < m_uRoutes; i++ ) {

			CRoute &Route = m_Route[i];

			pOut->AddRow();

			pOut->SetData(0, Route.m_uFace == NOTHING ? 100 : Route.m_uFace);

			pOut->SetData(1, PCTXT(Route.m_Ip.GetAsText()));

			pOut->SetData(2, PCTXT(Route.m_Mask.GetAsText()));

			pOut->SetData(3, PCTXT(Route.m_Gate.GetAsText()));

			if( Route.m_uTarg < NOTHING ) {

				pOut->SetData(4, PCTXT(m_Face[Route.m_uTarg].m_Ip.GetAsText()));
			}
			else
				pOut->SetData(4, "Unreachable");

			pOut->EndRow();
		}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

// Instantiators

IUnknown * CRouter::Create_SockTcp(PCTXT pName)
{
	return m_pThis->CreateSocket(IP_TCP);
}

IUnknown * CRouter::Create_SockUdp(PCTXT pName)
{
	return m_pThis->CreateSocket(IP_UDP);
}

IUnknown * CRouter::Create_SockRaw(PCTXT pName)
{
	return m_pThis->CreateSocket(IP_RAW);
}

// End of File
