
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debugging Support
//

// Macros

#define ENABLE FALSE

// Static

static IMutex * m_pLock = NULL;

// Code

static PCTXT GetObject(UINT Object)
{
	switch( Object ) {

		case OBJ_NIC:		return "nic";
		case OBJ_BUFFER:	return "buffer";
		case OBJ_ETHERNET:	return "ethernet";
		case OBJ_ARP:		return "arp";
		case OBJ_LOOPBACK:	return "loopback";
		case OBJ_ROUTER:	return "router";
		case OBJ_ICMP:		return "icmp";
		case OBJ_DHCP:		return "dhcp";
		case OBJ_UDP:		return "udp";
		case OBJ_TCP:		return "tcp";
		case OBJ_TEST:		return "test";
		case OBJ_PPP:		return "ppp";
		case OBJ_LCP:		return "lcp";
		case OBJ_IPCP:		return "ipcp";
		case OBJ_PAP:		return "pap";
		case OBJ_CHAP:		return "chap";
		case OBJ_HDLC:		return "hdlc";
		case OBJ_MODEM:		return "modem";
		case OBJ_QUEUE:		return "queue";
		case OBJ_RAW:		return "raw";
		case OBJ_DYNDNS:	return "dyndns";

		}

	return "xxx";
	}

static PCTXT GetLevel(UINT Level)
{
	switch( Level ) {

		case LEV_ERROR:		return "error";
		case LEV_WARN:		return "warning";
		case LEV_INFO:		return "info";
		case LEV_TRACE:		return "trace";
		case LEV_DETAIL:	return "detail";

		}

	return "xxx";
	}

global BOOL TcpDebugInit(void)
{
	if( !m_pLock ) {

		AfxNewObject("exec.qutex", IMutex, m_pLock);

		return TRUE;
		}

	return FALSE;
	}

global void TcpDebugLock(void)
{
	#if ENABLE

	m_pLock->Wait(FOREVER);

	#endif
	}

global void TcpDebugFree(void)
{
	#if ENABLE

	m_pLock->Free();

	#endif
	}

global void TcpDebugArgs(UINT Object, UINT Level, PCTXT pText, va_list pArgs)
{
	#if ENABLE

	if( Object == OBJ_TEST || Object == OBJ_TCP || Level <= LEV_TRACE ) {

		PCTXT pObject = GetObject(Object);

		PCTXT pLevel  = GetLevel(Level);

		TcpDebugLock();

		AfxTrace("%3.3u : %-7.7s : %-8.8s : ", GetTickCount() % 1000, pLevel, pObject);

		AfxTraceArgs(pText, pArgs);

		TcpDebugFree();
		}

	#endif
	}

global void TcpDebug(UINT Object, UINT Level, PCTXT pText, ...)
{
	#if ENABLE

	va_list pArgs;

	va_start(pArgs, pText);

	TcpDebugArgs(Object, Level, pText, pArgs);

	va_end(pArgs);

	#endif
	}

// End of File
