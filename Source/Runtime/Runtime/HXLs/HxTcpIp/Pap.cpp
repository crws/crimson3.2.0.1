
#include "Intern.hpp"

#include "Pap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PPP Password Authentication Protocol
//

// Constructor

CPap::CPap(CPpp *pPpp, CConfigPpp const &Config) : CPppLayer(pPpp, PROT_PAP)
{
	m_fServer = (Config.m_uMode == pppServer);

	m_bID     = 0;

	m_uObj    = OBJ_PAP;

	m_uCount  = 0;

	m_uTimer  = 0;

	m_uRetry  = ToTicks(5000);

	strcpy(m_sUser, Config.m_sUser);

	strcpy(m_sPass, Config.m_sPass);
	}

// Destructor

CPap::~CPap(void)
{
	}

// Management

void CPap::LowerLayerUp(void)
{
	if( m_fServer ) {

		m_uCount = 1;

		StartTimer();
		}
	else {
		m_uCount = 4;

		StartTimer();

		SendCredentials();
		}
	}

void CPap::LowerLayerDown(void)
{
	StopTimer();
	}

void CPap::OnTime(void)
{
	if( m_uTimer && GetTickCount() - m_uStart >= m_uTimer ) {

		if( m_uCount-- ) {

			StartTimer();

			SendCredentials();
			}
		else {
			StopTimer();

			ThisLayerDown();
			}
		}
	}

// Frame Handing

void CPap::OnRecv(CBuffer *pBuff)
{
	m_pLcp = BuffStripHead(pBuff, CLcpFrame);

	m_pLcp->NetToHost();

	TcpDebug(OBJ_PAP, LEV_TRACE, "recv code %u\n", m_pLcp->m_bCode);

	if( m_fServer ) {

		switch( m_pLcp->m_bCode ) {

			case papCredentials:

				if( CheckCredentials() ) {

					SendResponse(papSuccess);
				
					StopTimer();
				
					ThisLayerUp();
					}
				else {
					SendResponse(papFailure);
				
					StopTimer();
				
					ThisLayerDown();
					}
				break;
			}
		}
	else {
		switch( m_pLcp->m_bCode ) {

			case papSuccess:

				StopTimer();
				
				ThisLayerUp();
				
				break;

			case papFailure:

				StopTimer();
				
				ThisLayerDown();
				
				break;
			}
		}
	}

// Implementation

void CPap::AddText(CBuffer *pBuff, PCTXT pText)
{
	UINT  uSize = strlen(pText);

	PBYTE pData = pBuff->AddTail(uSize + 1);

	*pData = BYTE(uSize);

	memcpy(pData + 1, pText, uSize);
	}

void CPap::PutPAP(CBuffer *pBuff, BYTE bCode, BYTE bID)
{
	TcpDebug(OBJ_PAP, LEV_TRACE, "send code %u\n", bCode);

	CLcpFrame *pRep = BuffAddHead(pBuff, CLcpFrame);

	pRep->m_bCode   = bCode;

	pRep->m_bID     = bID;

	pRep->m_wLength = WORD(pBuff->GetSize());

	pRep->HostToNet();

	m_pPpp->PutPPP(m_wProtocol, pBuff);

	BuffRelease(pBuff);
	}

void CPap::SendResponse(BYTE bCode)
{
	CBuffer *pBuff = BuffAllocate(0);

	PutPAP(pBuff, bCode, m_bID++);
	}

void CPap::SendCredentials(void)
{
	CBuffer *pBuff = BuffAllocate(0);

	AddText(pBuff, m_sUser);

	AddText(pBuff, m_sPass);

	PutPAP(pBuff, papCredentials, m_bID++);
	}

BOOL CPap::CheckCredentials(void)
{
	PBYTE pData = m_pLcp->m_bData;

	BYTE  bLen1 = pData[0];

	BYTE  bLen2 = pData[1+bLen1];

	PTXT  pUser = PTXT(pData+1);

	PTXT  pPass = PTXT(pData+2+bLen1);

	pUser[bLen1] = 0;

	pPass[bLen2] = 0;

	return !strcasecmp(pUser, m_sUser) && !strcmp(pPass, m_sPass);
	}

void CPap::StartTimer(void)
{
	m_uStart = GetTickCount();

	m_uTimer = m_uRetry;
	}

void CPap::StopTimer(void)
{
	m_uCount = 0;

	m_uTimer = 0;
	}

// End of File
