
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ModemGprs_HPP

#define	INCLUDE_ModemGprs_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ModemCell.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Celluar GPRS Connection
//

class CModemCellGprs : public CModemCell
{
	public:
		// Constructor
		CModemCellGprs(CPpp *pPpp, CConfigPpp const &Config);

		// Overridables
		BOOL Init(void);
		BOOL Idle(void);
		BOOL Connect(BOOL &fOkay);
		BOOL HangUp(void);

	protected:
		// Data Members
		char m_sCont[90];
		UINT m_uLast;
	};

// End of File

#endif
