
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DhcpOption_HPP

#define	INCLUDE_DhcpOption_HPP

//////////////////////////////////////////////////////////////////////////
//
// DHCP Options
//

enum DhcpOption
{
	dhcpPAD		= 0,
	dhcpSUBNET	= 1,
	dhcpTIMEZONE	= 2,
	dhcpROUTER	= 3,
	dhcpDNS		= 6,
	dhcpHOST	= 12,
	dhcpDOMAIN	= 15,
	dhcpIPTTL	= 23,
	dhcpIPBROADCAST	= 28,
	dhcpIPROUTE	= 33,
	dhcpARP		= 35,
	dhcpTCPTTL	= 37,
	dhcpNTP		= 42,
	dhcpREQUEST	= 50,
	dhcpIPLEASE	= 51,
	dhcpOVERLOAD	= 52,
	dhcpMSGTYPE	= 53,
	dhcpSERVER	= 54,
	dhcpPARAM	= 55,
	dhcpMSG		= 56,
	dhcpRENEW	= 58,
	dhcpREBIND	= 59,
	dhcpSMTP	= 69,
	dhcpFQDN	= 81,
	dhcpEND		= 255,
	};

//////////////////////////////////////////////////////////////////////////
//
// DHCP Option
//

#pragma pack(1)

struct DHCPOPTION
{
	BYTE m_Type;
	BYTE m_Len;
	BYTE m_Data[128];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////////
//
// DHCP Option Wrapper
//

class CDhcpOption : public DHCPOPTION
{
	public:
		// Creation
		void Create(BYTE Type);
		void Create(BYTE Type, BYTE bData);
		void Create(BYTE Type, PCTXT pText);
		void Create(BYTE Type, IPREF Ip);

		// Attributes
		BOOL   IsEnd(void) const;
		BOOL   IsPad(void) const;
		BYTE   GetByte(UINT &Ptr) const;
		WORD   GetWord(UINT &Ptr) const;
		DWORD  GetLong(UINT &Ptr) const;
		PCTXT  GetText(UINT &Ptr) const;
		IPADDR GetAddr(UINT &Ptr) const;
		UINT   GetSize(void) const;

		// Operations
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddText(PCTXT pText);
		void AddAddr(IPREF Ip);
		void AddHex(PCBYTE pData, UINT uCount);
		void AddHexByte(BYTE bData);
		void AddHexChar(BYTE bData);

	private:
		// Constructor
		CDhcpOption(void);
	};

// End of File

#endif
