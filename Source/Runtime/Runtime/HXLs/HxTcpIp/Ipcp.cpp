
#include "Intern.hpp"

#include "Ipcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PPP Internet Protocol Control Protocol
//

// Constructor

CIpcp::CIpcp(CPpp *pPPP, CConfigPpp const &Config) : CPppNegotiate(pPPP, PROT_IPCP)
{
	m_fServer = (Config.m_uMode == pppServer);

	m_LocAddr = (DWORD &) Config.m_LocAddr;

	m_RemAddr = (DWORD &) Config.m_RemAddr;

	m_uObj    = OBJ_IPCP;
	}

// Destructor

CIpcp::~CIpcp(void)
{
	}

// Attributes

IPREF CIpcp::GetLocAddr(void) const
{
	return *((CIpAddr *) &m_LocAddr);
	}

IPREF CIpcp::GetRemAddr(void) const
{
	return *((CIpAddr *) &m_RemAddr);
	}

IPREF CIpcp::GetDNS1(void) const
{
	return *((CIpAddr *) &m_DNS1);
	}

IPREF CIpcp::GetDNS2(void) const
{
	return *((CIpAddr *) &m_DNS2);
	}

BOOL CIpcp::GetHeadComp(void) const
{
	return m_fRemHeadComp;
	}

// Remote Options

BOOL CIpcp::RemoteReject(BYTE bType, PBYTE pData)
{
	switch( bType ) {

		case optHeadComp:

			return TRUE;

		case optIPAddress:
			
			return FALSE;

		case optNameServer1:
		case optNameServer2:

			return TRUE;
		
		case optNetbiosServer1:
		case optNetbiosServer2:

			return TRUE;
		}

	return TRUE;
	}

BOOL CIpcp::RemoteNak(BYTE bType, PBYTE pData)
{
	switch( bType ) {

		case optHeadComp:

			return TRUE;

		case optIPAddress:

			if( m_fServer ) {

				OPTADDR *opt = (OPTADDR *) pData;

				if( opt->m_Addr == m_RemAddr ) {

					return FALSE;
					}

				opt->m_Addr = m_RemAddr;

				return TRUE;
				}

			return FALSE;

		case optNameServer1:
		case optNameServer2:

			return TRUE;
		
		case optNetbiosServer1:
		case optNetbiosServer2:

			return TRUE;
		}

	return TRUE;
	}

void CIpcp::RemoteDefault(void)
{
	if( !m_fServer ) {

		m_RemAddr = 0;
		}

	m_fRemHeadComp = FALSE;
	}

void CIpcp::RemoteAgreed(PCBYTE pData, UINT uSize)
{
	for( UINT n = 0; n < uSize; ) {

		BYTE bType = pData[n+0];

		BYTE bSize = pData[n+1];

		switch( bType ) {

			case optHeadComp:

				m_fRemHeadComp = TRUE;

				break;

			case optIPAddress:

				if( !m_fServer ) {

					OPTADDR *opt = (OPTADDR *) (pData+n);

					m_RemAddr    = opt->m_Addr;
					}
				break;
			}

		n += bSize;
		}

	TcpDebug(m_uObj, LEV_TRACE, "remote agreed\n");

	ShowRemote();
	}

// Local Options

void CIpcp::LocalDefault(void)
{
	if( !m_fServer ) {

		m_LocAddr = 0;

		m_DNS1    = NOTHING;

		m_DNS2    = NOTHING;
		}

	m_fLocHeadComp = FALSE;
	}

void CIpcp::LocalRequest(CBuffer *pBuff)
{
	if( m_fLocHeadComp ) {

		AddOptHeadComp(pBuff);
		}

	if( !m_fServer ) {

		if( m_DNS1 ) {

			AddOptDNS1(pBuff);
			}

		if( m_DNS2 ) {

			AddOptDNS2(pBuff);
			}
		}

	AddOptAddress(pBuff);
	}

BOOL CIpcp::LocalReject(BYTE bType, PBYTE pData)
{
	switch( bType ) {
		
		case optIPAddress:

			return FALSE;
		
		case optNameServer1:

			m_DNS1 = 0;

			return TRUE;
		
		case optNameServer2:

			m_DNS2 = 0;

			return TRUE;

		case optHeadComp:

			m_fLocHeadComp = FALSE;

			return TRUE;
		}

	return FALSE;
	}

BOOL CIpcp::LocalNak(BYTE bType, PBYTE pData)
{
	switch( bType ) {

		case optIPAddress:

			if( !m_fServer ) {

				OPTADDR *opt = (OPTADDR *) pData;

				m_LocAddr    = opt->m_Addr;
				
				return TRUE;
				}

			return FALSE;

		case optNameServer1:

			if( !m_fServer ) {

				OPTADDR *opt = (OPTADDR *) pData;

				m_DNS1       = opt->m_Addr;
				
				return TRUE;
				}

			return FALSE;

		case optNameServer2:

			if( !m_fServer ) {

				OPTADDR *opt = (OPTADDR *) pData;

				m_DNS2       = opt->m_Addr;
				
				return TRUE;
				}

			return FALSE;
		}

	return FALSE;
	}

void CIpcp::LocalAgreed(void)
{
	TcpDebug(m_uObj, LEV_TRACE, "local agreed\n");

	ShowLocal();
	}

// Option Building

void CIpcp::AddOptAddress(CBuffer *pBuff)
{
	OPTADDR opt;
	
	opt.m_bType = optIPAddress;
	
	opt.m_bSize = sizeof(opt);
	
	opt.m_Addr  = m_LocAddr;

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

void CIpcp::AddOptDNS1(CBuffer *pBuff)
{
	OPTADDR opt;
	
	opt.m_bType = optNameServer1;
	
	opt.m_bSize = sizeof(opt);
	
	opt.m_Addr  = (m_DNS1 == NOTHING) ? 0 : m_DNS1;

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

void CIpcp::AddOptDNS2(CBuffer *pBuff)
{
	OPTADDR opt;
	
	opt.m_bType = optNameServer2;
	
	opt.m_bSize = sizeof(opt);
	
	opt.m_Addr  = (m_DNS2 == NOTHING) ? 0 : m_DNS2;

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

void CIpcp::AddOptHeadComp(CBuffer *pBuff)
{
	OPTBASIC opt;
	
	opt.m_bType = optHeadComp;

	opt.m_bSize = sizeof(opt);

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

// Debugging

BOOL CIpcp::ShowRemote(void)
{
	TcpDebug(m_uObj, LEV_TRACE, "headcomp is %u\n", m_fRemHeadComp);

	TcpDebug(m_uObj, LEV_TRACE, "remaddr  is %s\n", PCTXT(CIpAddr(m_RemAddr).GetAsText()));

	return TRUE;
	}

BOOL CIpcp::ShowLocal(void)
{
	TcpDebug(m_uObj, LEV_TRACE, "headcomp is %u\n", 0);

	TcpDebug(m_uObj, LEV_TRACE, "locaddr  is %s\n", PCTXT(CIpAddr(m_LocAddr).GetAsText()));

	TcpDebug(m_uObj, LEV_TRACE, "dns1     is %s\n", PCTXT(CIpAddr(m_DNS1).GetAsText()));

	TcpDebug(m_uObj, LEV_TRACE, "dns2     is %s\n", PCTXT(CIpAddr(m_DNS2).GetAsText()));

	return TRUE;
	}

// End of File
