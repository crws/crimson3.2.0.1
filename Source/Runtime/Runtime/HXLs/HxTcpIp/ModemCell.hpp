
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ModemCell_HPP

#define	INCLUDE_ModemCell_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Modem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CShortMessage;

//////////////////////////////////////////////////////////////////////////
//
// Celluar Connection
//

class CModemCell : public CModem,
		   public ICellStatus,
		   public ISendSMS
{
	public:
		// Constructor
		CModemCell(CPpp *pPpp, CConfigPpp const &Config);

		// Destructor
		~CModemCell(void);

		// Overridables
		BOOL Init(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		
		// ICellStatus
		BOOL METHOD GetCellStatus(CCellStatusInfo &Info);
		BOOL METHOD SendCommand(PCTXT pCmd);

		// SMS Methods
		BOOL SendSMS(PCTXT pNumber, PCTXT pMessage);

	protected:
		// Static Data
		static char const m_Hex[];

		// Data Members
		ULONG	   m_uRefs;
		UINT	   m_uInst;
		UINT       m_uSignal;
		IRecvSMS * m_pSMS;
		BYTE       m_bPDU[1024];
		UINT	   m_uSMSMode;
		PTXT       m_pSendNum;
		PTXT       m_pSendMsg;
		BOOL	   m_fRing;

		// SMS Support
		BOOL CheckMessages(BOOL fRecv);
		void CheckIncoming(void);
		void CheckOutgoing(void);
		BOOL SendMessage(CShortMessage &Msg);
		UINT ListMessages(PUINT pIndex, UINT uLimit);
		BOOL DeleteMessage(UINT uIndex);
		BOOL ReadMessage(UINT uIndex, CShortMessage &Msg);
		BOOL SendBody(PCBYTE pPDU, UINT uCount);
		BOOL ParseBody(CShortMessage &Msg);
		BOOL SkipBody(void);
		BYTE FromHex(UINT uData);

		// Implementation
		void CheckSignal(void);
		BOOL CheckService(void);
		UINT CheckReply(UINT uTimeout);
		UINT CheckReply(void);
		BOOL CheckRinging(void);
		void WaitQuiet(void);
	};

// End of File

#endif
