
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ipcp_HPP

#define	INCLUDE_Ipcp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PppNegotiate.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPpp;

//////////////////////////////////////////////////////////////////////////
//
// IPCP Options
//

enum
{
	optHeadComp		= 2,
	optIPAddress		= 3,
	optNameServer1		= 129,
	optNameServer2		= 131,
	optNetbiosServer1	= 130,
	optNetbiosServer2	= 132,
	};

//////////////////////////////////////////////////////////////////////////
//
// IPCP Option -- IP Address
//

#pragma pack(1)

struct OPTADDR
{
	BYTE	m_bType;
	BYTE	m_bSize;
	DWORD	m_Addr;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// PPP Internet Protocol Control Protocol
//

class CIpcp : public CPppNegotiate
{
	public:
		// Constructor
		CIpcp(CPpp *pPpp, CConfigPpp const &Config);

		// Destructor
		~CIpcp(void);

		// Attributes
		IPREF GetLocAddr (void) const;
		IPREF GetRemAddr (void) const;
		IPREF GetDNS1    (void) const;
		IPREF GetDNS2    (void) const;
		BOOL  GetHeadComp(void) const;

	protected:
		// Data Members
		BOOL  m_fServer;
		DWORD m_LocAddr;
		DWORD m_RemAddr;
		DWORD m_DNS1;
		DWORD m_DNS2;
		BOOL  m_fLocHeadComp;
		BOOL  m_fRemHeadComp;

		// Remote Options
		BOOL RemoteReject (BYTE   bType, PBYTE pData);
		BOOL RemoteNak    (BYTE   bType, PBYTE pData);
		void RemoteDefault(void);
		void RemoteAgreed (PCBYTE pData, UINT  uSize);

		// Local Options
		void LocalDefault(void);
		void LocalRequest(CBuffer *pBuff);
		BOOL LocalReject (BYTE bType, PBYTE pData);
		BOOL LocalNak    (BYTE bType, PBYTE pData);
		void LocalAgreed (void);

		// Option Building
		void AddOptAddress (CBuffer *pBuff);
		void AddOptDNS1    (CBuffer *pBuff);
		void AddOptDNS2    (CBuffer *pBuff);
		void AddOptHeadComp(CBuffer *pBuff);

		// Debugging
		BOOL ShowRemote(void);
		BOOL ShowLocal(void);
	};

// End of File

#endif
