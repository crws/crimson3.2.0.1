
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IcmpHeader_HPP

#define	INCLUDE_IcmpHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// ICMP Header
//

#pragma pack(1)

struct ICMPHEADER
{
	BYTE m_Type;
	BYTE m_Code;
	WORD m_Checksum;
	
	union
	{
		struct
		{
			WORD m_Ident;
			WORD m_Sequence;

			} m_Ping;

		struct
		{
			IPADDR m_Ip;
			
			} m_Redirect;

		struct
		{
			DWORD m_Empty;

			} m_Empty;
		};
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// ICMP Header Wrapper
//

class CIcmpHeader : public ICMPHEADER
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);

		// Attributes
		BOOL TestChecksum(UINT uSize) const;

		// Operations
		void AddChecksum(UINT uSize);

	private:
		// Constructor
		CIcmpHeader(void);

	protected:
		// Implementation
		WORD CalcChecksum(UINT uSize) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Conversion

STRONG_INLINE void CIcmpHeader::NetToHost(void)
{
	switch( m_Type ) {

		case ICMP_PING:
		case ICMP_PONG:

			m_Ping.m_Ident    = ::NetToHost(m_Ping.m_Ident);

			m_Ping.m_Sequence = ::NetToHost(m_Ping.m_Sequence);

			break;
		}
	}

STRONG_INLINE void CIcmpHeader::HostToNet(void)
{
	switch( m_Type ) {

		case ICMP_PING:
		case ICMP_PONG:

			m_Ping.m_Ident    = ::HostToNet(m_Ping.m_Ident);

			m_Ping.m_Sequence = ::HostToNet(m_Ping.m_Sequence);

			break;
		}
	}

// End of File

#endif

