
#include "Intern.hpp"

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emplolyed Classes
//

#include "Modem.hpp"
#include "Lcp.hpp"
#include "Pap.hpp"
#include "Chap.hpp"
#include "Ipcp.hpp"
#include "Hdlc.hpp"
#include "PppDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern CModem * Create_Modem(CPpp *pPpp, CConfigPpp const &Config);

//////////////////////////////////////////////////////////////////////////
//
// PPP Frame Wrapper
//

// Conversion

void CPppFrame::NetToHost(void)
{
	m_wProtocol = ::NetToHost(m_wProtocol);
	}

void CPppFrame::HostToNet(void)
{
	m_wProtocol = ::HostToNet(m_wProtocol);
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Core Protocol
//

// Constructor

CPpp::CPpp(CPppDriver *pDriver, CConfigPpp const &Config)
{
	m_pDriver  = pDriver;

	m_fServer  = (Config.m_uMode == pppServer);

	m_RemAddr  = Config.m_RemAddr;

	m_RemMask  = Config.m_RemMask;

	m_fGateway = Config.m_fGate;

	m_uTimeout = Config.m_uTimeout;

	m_hThread  = NULL;

	m_pMutex   = Create_Mutex();

	m_pRouter  = NULL;
		   
	m_pSink    = NULL;

	m_uFace    = NOTHING;

	m_pModem   = Create_Modem(this, Config);

	m_pLcp     = new CLcp (this, Config);

	m_pPap     = new CPap (this, Config);

	m_pChap    = new CChap(this, Config);

	m_pIpcp	   = new CIpcp(this, Config);

	m_pHdlc    = new CHdlc(this, m_pModem);

	m_fOpen    = FALSE;

	m_uPhase   = phaseIdle;

	m_fNetwork = FALSE;

	m_uCount   = 0;

	m_uTimer1  = 0;

	m_uTimer2  = 0;

	SetStatus("");
	}

// Destructor

CPpp::~CPpp(void)
{
	delete m_pIpcp;

	delete m_pChap;

	delete m_pPap;

	delete m_pLcp;

	delete m_pHdlc;

	delete m_pModem;

	m_pMutex->Release();
	}

// Attributes

BOOL CPpp::IsDirect(void) const
{
	return m_pModem->IsDirect();
	}

PCTXT CPpp::GetStatus(void) const
{
	if( strlen(m_sStatus) ) {

		return m_sStatus;
		}

	return m_pModem->GetStatus();
	}

HTHREAD CPpp::GetThread(void) const
{
	return m_hThread;
	}

// Operations

void CPpp::Bind(IRouter *pRouter, IPacketSink *pSink, UINT uFace)
{
	m_pRouter = pRouter;

	m_pSink   = pSink;

	m_uFace   = uFace;
	}

void CPpp::Open(void)
{
	ClaimData();

	m_hThread = GetCurrentThread();

	if( !m_fOpen ) {

		m_fOpen = TRUE;

		SetLinkIdle();

		RegisterOnDemand();
		}

	FreeData();
	}

void CPpp::Close(void)
{
	ClaimData();

	if( m_fOpen ) {

		switch( m_uPhase ) {

			case phaseConnectLink:
			case phaseAuthenticate:
			case phaseConnectNetwork:
			case phaseConnected:
			case phaseDisconnect:

				if( IsDirect() ) {

					m_fOpen = FALSE;

					DisconnectLink();
					}
				else {
					m_fOpen = FALSE;

					m_pHdlc->Close();

					SetLinkIdle();
					}

				break;
			}
		}

	FreeData();
	}

void CPpp::Service(void)
{
	m_pModem->Service();
	}

void CPpp::StopTask(void)
{
	m_pModem->StopTask();
	}

// Timed Events

void CPpp::OnTime(void)
{
	ClaimData();

	if( m_uPhase == phaseConnected ) {

		CheckTimer1();
		}
		
	if( m_uPhase == phaseConnected ) {

		CheckTimer2();
		}

	m_pModem->OnTime();

	m_pHdlc ->OnTime();

	m_pLcp  ->OnTime();

	m_pPap  ->OnTime();

	m_pChap ->OnTime();

	m_pIpcp ->OnTime();

	FreeData();
	}

// Layer Events

void CPpp::OnLayerEvent(WORD wProtocol, UINT uEvent)
{
	ClaimData();

	if( wProtocol == PROT_MODEM ) {

		switch( uEvent ) {

			case layerFinished:

				OnModemFinished();

				break;

			case layerUp:

				m_pHdlc->LowerLayerUp();

				break;

			case layerDown:

				m_pHdlc->LowerLayerDown();

				break;
			}
		}

	if( wProtocol == PROT_HDLC ) {

		switch( uEvent ) {

			case layerStarted:

				m_pModem->Open();

				break;

			case layerFinished:

				m_pModem->Close();

				break;

			case layerUp:

				SetStatus("NEG LCP");

				m_pLcp->LowerLayerUp();

				break;

			case layerDown:

				SetStatus("");

				m_pLcp->LowerLayerDown();

				break;

			case layerFailed:

				OnDataFailed();

				break;
			}
		}

	if( wProtocol == PROT_LCP ) {

		switch( uEvent ) {

			case layerUp:

				OnLinkUp();

				break;

			case layerDown:

				OnLinkDown();

				break;

			case layerStarted:

				m_pHdlc->Open();

				break;

			case layerFinished:

				OnLinkFinished();

				break;

			case layerFailed:

				OnLinkFailed();

				break;
			}
		}

	if( wProtocol == m_wAuth ) {

		switch( uEvent ) {

			case layerUp:

				OnAuthPassed();

				break;

			case layerDown:

				OnAuthFailed();

				break;
			}
		}

	if( wProtocol == PROT_IPCP ) {

		switch( uEvent ) {

			case layerUp:

				OnNetworkUp();

				break;

			case layerDown:

				OnNetworkDown();

				break;

			case layerFinished:

				OnNetworkFinished();

				break;

			case layerFailed:

				OnNetworkFailed();

				break;
			}
		}

	FreeData();
	}

// Frame Handling

void CPpp::OnRecv(CBuffer *pBuff)
{
	ClaimData();

	WORD wProtocol  = 0;

	CPppFrame *pPPP = BuffStripHead(pBuff, CPppFrame);

	pPPP->NetToHost();

	if( HIBYTE(wProtocol = pPPP->m_wProtocol) & 0x01 ) {

		*pBuff->AddHead(1) = LOBYTE(wProtocol);

		wProtocol >>= 8;
		}

	m_pDriver->Capture(wProtocol, pBuff, FALSE);

	switch( wProtocol ) {
		
		case PROT_LCP:

			switch( m_uPhase ) {

				case phaseConnectLink:
				case phaseAuthenticate:
				case phaseConnectNetwork:
				case phaseConnected:
				case phaseDisconnect:

					m_pLcp->OnRecv(pBuff);

					break;
				}

			BuffRelease(pBuff);
			
			break;

		case PROT_PAP:

			if( m_wAuth == PROT_PAP ) {

				switch( m_uPhase ) {

					case phaseAuthenticate:

						m_pPap->OnRecv(pBuff);

						break;
					}
				}

			BuffRelease(pBuff);

			break;

		case PROT_CHAP:

			if( m_wAuth == PROT_CHAP ) {

				switch( m_uPhase ) {

					case phaseAuthenticate:
					case phaseConnectNetwork:
					case phaseConnected:

						m_pChap->OnRecv(pBuff);

						break;
					}
				}

			BuffRelease(pBuff);

			break;

		case PROT_IPCP:

			switch( m_uPhase ) {

				case phaseConnectNetwork:
				case phaseConnected:
				case phaseDisconnect:

					m_pIpcp->OnRecv(pBuff);

					break;
				}

			BuffRelease(pBuff);
			
			break;

		case PROT_IP:

			// LATER -- Support IP header compression.

			switch( m_uPhase ) {

				case phaseConnected:

					ReloadTimer2();

					FreeData();

					m_pSink->OnPacket(m_uFace, pBuff);

					return;
				}

			BuffRelease(pBuff);

			break;

		default:
			switch( m_uPhase ) {

				case phaseConnectLink:
				case phaseAuthenticate:
				case phaseConnectNetwork:
				case phaseConnected:
				case phaseDisconnect:

					m_pLcp->ProtocolReject(wProtocol, pBuff);

					break;
				}

			BuffRelease(pBuff);
			
			break;
		}

	ReloadTimer2();

	FreeData();
	}

// Frame Sending

void CPpp::PutPPP(WORD wProtocol, CBuffer *pBuff)
{
	if( wProtocol == PROT_LCP ) {

		switch( m_uPhase ) {

			case phaseConnectLink:
			case phaseAuthenticate:
			case phaseConnectNetwork:
			case phaseConnected:
			case phaseDisconnect:

				PutFrame(wProtocol, pBuff);
				
				return;
			}
		}
	
	if( wProtocol == PROT_PAP ) {
		
		if( m_wAuth == PROT_PAP ) {

			switch( m_uPhase ) {

				case phaseAuthenticate:

					PutFrame(wProtocol, pBuff);
					
					return;
				}
			}
		}

	if( wProtocol == PROT_CHAP ) {
		
		if( m_wAuth == PROT_CHAP ) {

			switch( m_uPhase ) {

				case phaseAuthenticate:
				case phaseConnectNetwork:
				case phaseConnected:

					PutFrame(wProtocol, pBuff);
					
					return;
				}
			}
		}

	if( wProtocol == PROT_IPCP ) {

		switch( m_uPhase ) {

			case phaseConnectNetwork:
			case phaseConnected:
			case phaseDisconnect:

				PutFrame(wProtocol, pBuff);
				
				return;
			}
		}

	BuffRelease(pBuff);
	}

void CPpp::PutIP(CBuffer *pBuff)
{
	ClaimData();

	switch( m_uPhase ) {

		case phaseIdle:

			if( OnDemand() ) {

				m_uCount = 4;

				ConnectLink();
				}
			break;

		case phaseConnected:

			// LATER -- Support IP header compression.

			ReloadTimer1();

			FreeData();

			PutFrame(PROT_IP, pBuff);
			
			return;
		}

	FreeData();
	}

// Events

void CPpp::OnModemFinished(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "modem finished\n");

	switch( m_uPhase ) {

		case phaseConnectLink:

			m_pHdlc->Close();

			SetLinkIdle();

			break;
		}
	}

void CPpp::OnDataFailed(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "modem dropped\n");

	switch( m_uPhase ) {

		case phaseConnectLink:
		case phaseAuthenticate:
		case phaseConnectNetwork:
		case phaseConnected:
		case phaseDisconnect:

			m_pHdlc->Close();

			SetLinkIdle();

			break;
		}
	}

void CPpp::OnLinkUp(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "link up\n");

	switch( m_uPhase ) {

		case phaseConnectLink:

			ReadLinkOptions();

			if( m_wAuth ) {

				Authenticate();

				break;
				}

			ConnectNet();

			break;
		}
	}

void CPpp::OnLinkDown(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "link down\n");

	switch( m_uPhase ) {

		case phaseConnectNetwork:
		case phaseConnected:

			m_pIpcp->LowerLayerDown();

		case phaseAuthenticate:

			switch( m_wAuth ) {

				case PROT_CHAP:

					m_pChap->LowerLayerDown();

					break;

				case PROT_PAP:

					m_pPap->LowerLayerDown();

					break;
				}

			DisconnectLink();

			break;
		}
	}

void CPpp::OnLinkFinished(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "link finished\n");

	switch( m_uPhase ) {

		case phaseConnectLink:
		case phaseDisconnect:

			m_pHdlc->Close();

			SetLinkIdle();

			break;

		default:
			m_pHdlc->Close();

			break;
		}
	}

void CPpp::OnLinkFailed(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "link failed\n");

	switch( m_uPhase ) {

		case phaseConnectLink:
		case phaseAuthenticate:
		case phaseConnectNetwork:
		case phaseConnected:

			DisconnectLink();

			break;
		}
	}

void CPpp::OnAuthPassed(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "auth passed\n");

	switch( m_uPhase ) {

		case phaseAuthenticate:

			ConnectNet();

			break;
		}
	}

void CPpp::OnAuthFailed(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "auth failed\n");

	switch( m_uPhase ) {

		case phaseAuthenticate:

			if( TRUE || m_fServer ) {

				// NOTE: Disconnect even if we're the client in
				// case failure was as a result of a dead link.

				DisconnectLink();
				}
			break;
		}
	}

void CPpp::OnNetworkUp(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "network up\n");

	EnableNetwork();
	}

void CPpp::OnNetworkDown(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "network down\n");

	DisableNetwork();
	}

void CPpp::OnNetworkFinished(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "network finished\n");

	switch( m_uPhase ) {

		case phaseConnectNetwork:
		case phaseConnected:

			DisconnectLink();

			break;
		}
	}

void CPpp::OnNetworkFailed(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "network failed\n");

	switch( m_uPhase ) {

		case phaseConnectNetwork:
		case phaseConnected:

			DisconnectLink();

			break;
		}
	}

// Actions

void CPpp::SetLinkIdle(void)
{
	SetStatus("");

	TcpDebug(OBJ_PPP, LEV_TRACE, "set link idle\n");

	DisableNetwork();

	m_pLcp ->Reset();

	m_pIpcp->Reset();

	if( m_fOpen ) {
		
		if( m_uCount || !OnDemand() ) {

			if( m_uCount ) {
				
				m_uCount--;
				}

			ConnectLink();

			return;
			}
		}

	SetPhase(phaseIdle);
	}

void CPpp::ConnectLink(void)
{
	TcpDebug(OBJ_PPP, LEV_TRACE, "connect link\n");

	SetPhase(phaseConnectLink);
	
	m_pLcp->Open();
	}

void CPpp::Authenticate(void)
{
	SetStatus("AUTH");

	TcpDebug(OBJ_PPP, LEV_TRACE, "authenticate\n");

	switch( m_wAuth ) {

		case PROT_CHAP:

			SetPhase(phaseAuthenticate);

			m_pChap->LowerLayerUp();

			return;

		case PROT_PAP:

			SetPhase(phaseAuthenticate);

			m_pPap->LowerLayerUp();

			return;
		}

	DisconnectLink();
	}

void CPpp::ConnectNet(void)
{
	SetStatus("NEG IPCP");

	TcpDebug(OBJ_PPP, LEV_TRACE, "connect net\n");

	SetPhase(phaseConnectNetwork);

	m_pIpcp->LowerLayerUp();

	m_pIpcp->Open();
	}

void CPpp::DisconnectLink(void)
{
	SetStatus("");

	TcpDebug(OBJ_PPP, LEV_TRACE, "disconnect link\n");

	SetPhase(phaseDisconnect);
	
	m_pLcp->Close();
	}

void CPpp::EnableNetwork(void)
{
	SetStatus("UP");

	if( !m_fNetwork ) {

		EnableInterface();

		RegisterRoutes();

		IPREF DNS1 = m_pIpcp->GetDNS1();

		m_pDriver->LinkActive(TRUE, DNS1);

		m_fNetwork = TRUE;
		}

	m_uCount = 0;

	ReloadTimer1();

	ReloadTimer2();

	SetPhase(phaseConnected);
	}

void CPpp::DisableNetwork(void)
{
	if( m_fNetwork ) {

		IPREF DNS1 = IP_EMPTY;

		m_pDriver->LinkActive(FALSE, DNS1);

		DisableInterface();

		if( m_fOpen ) {

			RegisterOnDemand();
			}

		m_fNetwork = FALSE;
		}
	}

void CPpp::ReloadTimer1(void)
{
	ReloadTimer(m_uStart1, m_uTimer1);
	}

void CPpp::ReloadTimer2(void)
{
	ReloadTimer(m_uStart2, m_uTimer2);
	}

void CPpp::CheckTimer1(void)
{
	CheckTimer(m_uStart1, m_uTimer1);
	}

void CPpp::CheckTimer2(void)
{
	CheckTimer(m_uStart2, m_uTimer2);
	}

void CPpp::ReloadTimer(UINT &uStart, UINT &uTimer)
{
	if( !m_uTimeout ) {

		if( !m_fServer ) {

			uStart = time(NULL);

			uTimer = 600;
			}
		else
			uTimer = 0;
		}
	else {
		uStart = time(NULL);

		uTimer = m_uTimeout;
		}
	}

void CPpp::CheckTimer(UINT &uStart, UINT &uTimer)
{
	if( uTimer ) {

		UINT uTime = time(NULL);

		UINT uGone = uTime - uStart;

		if( uGone >= uTimer ) {

			TcpDebug(OBJ_PPP, LEV_TRACE, "activity timeout\n");

			DisconnectLink();

			uTimer = 0;
			}
		}
	}

// Route Management

void CPpp::EnableInterface(void)
{
	if( !m_pIpcp->GetRemAddr().IsEmpty() ) {

		m_pRouter->AddRoute(  m_uFace,
				      m_pIpcp->GetRemAddr(),
				      IP_BROAD,
				      m_pIpcp->GetLocAddr()
				      );
		}

	m_pRouter->SetConfig( m_uFace, 
			      m_pIpcp->GetLocAddr(),
			      IP_BROAD
			      );
	}

void CPpp::DisableInterface(void)
{
	m_pRouter->SetConfig( m_uFace,
			      IP_EMPTY,
			      IP_BROAD
			      );
	}

void CPpp::RegisterRoutes(void)
{
	if( m_fGateway ) {

		m_pRouter->SetGateway( m_uFace,
				       m_pIpcp->GetLocAddr()
				       );
		}
	else {
		if( !m_fServer ) {

			if( m_RemAddr.GetValue() ) {

				m_pRouter->AddRoute( m_uFace,
						     m_RemAddr,
						     m_RemMask,
						     m_pIpcp->GetLocAddr()
						     );
				}
			}
		}
	}

void CPpp::RegisterOnDemand(void)
{
	if( OnDemand() ) {

		if( m_fGateway ) {

			m_pRouter->SetGateway( m_uFace,
					       IP_BROAD
					       );
			}
		else {
			if( m_RemAddr.GetValue() ) {

				m_pRouter->AddRoute( m_uFace,
						     m_RemAddr,
						     m_RemMask,
						     IP_EMPTY
						     );
				}
			}
		}
	}

// Implementation

void CPpp::SetPhase(UINT uPhase)
{
	m_uPhase = uPhase;
	}

void CPpp::ReadLinkOptions(void)
{
	m_wAuth = m_pLcp->GetAuthProtocol();

	UINT uCharMap  = m_pLcp->GetCharMap();

	BOOL fProtComp = m_pLcp->GetProtComp();

	BOOL fAddrComp = m_pLcp->GetAddrComp();

	m_pHdlc->SetOptions(uCharMap, fProtComp, fAddrComp);
	}

BOOL CPpp::OnDemand(void)
{
	return !m_fServer && m_uTimeout;
	}

void CPpp::ClaimData(void)
{
	m_pMutex->Wait(FOREVER);
	}

void CPpp::FreeData(void)
{
	m_pMutex->Free();
	}

void CPpp::SetStatus(PCTXT pStatus)
{
	strcpy(m_sStatus, pStatus);
	}

void CPpp::PutFrame(WORD wProtocol, CBuffer *pBuff)
{
	m_pDriver->Capture(wProtocol, pBuff, TRUE);

	m_pHdlc->PutFrame(wProtocol, pBuff);
	}

// End of File
