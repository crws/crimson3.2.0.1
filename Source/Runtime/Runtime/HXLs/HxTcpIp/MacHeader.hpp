
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MacHeader_HPP

#define	INCLUDE_MacHeader_HPP

//////////////////////////////////////////////////////////////////////////////
//
// MAC Header
//

#pragma pack(1)

struct MACHEADER
{
	CMacAddr m_Dest;
	CMacAddr m_Src;
	WORD	 m_Type;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////////
//
// MAC Header Wrapper
//

class CMacHeader : public MACHEADER
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);
	};

//////////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE void CMacHeader::NetToHost(void)
{
	m_Type = ::NetToHost(m_Type);
	}

STRONG_INLINE void CMacHeader::HostToNet(void)
{
	m_Type = ::HostToNet(m_Type);
	}

// End of File

#endif
