
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PppNegotiate_HPP

#define	INCLUDE_PppNegotiate_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PppLayer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// LCP States
//

enum
{
	stateInitial		= 0,
	stateStarting		= 1,
	stateClosed		= 2,
	stateStopped		= 3,
	stateClosing		= 4,
	stateStopping		= 5,
	stateReqSent		= 6,
	stateAckRcvd		= 7,
	stateAckSent		= 8,
	stateOpened		= 9,
	};

//////////////////////////////////////////////////////////////////////////
//
// LCP Codes
//

enum
{
	codeConfigRequest	= 1,
	codeConfigAck		= 2,
	codeConfigNak		= 3,
	codeConfigReject	= 4,
	codeTerminateRequest	= 5,
	codeTerminateAck	= 6,
	codeCodeReject		= 7,
	codeProtocolReject	= 8,
	codeIdentification	= 12,
	codeTimeRemaining	= 13,
	};

//////////////////////////////////////////////////////////////////////////
//
// LCP Option -- Basic
//

#pragma pack(1)

struct OPTBASIC
{
	BYTE	m_bType;
	BYTE	m_bSize;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// PPP Options Negotiator
//

class CPppNegotiate : public CPppLayer
{
	public:
		// Constructor
		CPppNegotiate(CPpp *pPpp, WORD Protocol);

		// Destructor
		~CPppNegotiate(void);

		// Management
		void Reset(void);
		void Open(void);
		void Close(void);
		void LowerLayerUp(void);
		void LowerLayerDown(void);
		void OnTime(void);

		// Frame Handling
		void OnRecv(CBuffer *pBuff);

	protected:
		// Data Members
		CLcpFrame * m_pLcp;
		BYTE	    m_bID;
		UINT	    m_uState;
		UINT	    m_uLocLimit;
		UINT	    m_uRemLimit;
		UINT	    m_uCount;
		BOOL	    m_fTimer;
		UINT	    m_uTimer;
		UINT	    m_uStart;
		UINT	    m_uRetry;

		// Internal Events
		void OnTimeoutPos(void);
		void OnTimeoutNeg(void);
		void OnRecvConfigRequest(void);
		void OnRecvConfigAck(void);
		void OnRecvConfigNak(void);
		void OnRecvConfigReject(void);
		void OnRecvConfigNakRej(void);
		void OnRecvTerminateRequest(void);
		void OnRecvTerminateAck(void);
		void OnRecvCodeUnknown(CBuffer *pBuff);

		// Actions
		void InitRestartCount(void);
		void ZeroRestartCount(void);
		void LoadRestartTimer(void);
		void StopRestartTimer(void);
		void SendConfigRequest(BOOL fInit);
		BOOL SendConfigReply(void);
		void SendTerminateRequest(void);
		void SendTerminateAck(void);
		void SendCodeReject(CBuffer *pBuff);
		void SendEchoReply(void);

		// Remote Options
		BOOL RemoteReject(BYTE bID, PBYTE pData, UINT uSize);
		BOOL RemoteNak   (BYTE bID, PBYTE pData, UINT uSize);
		
		// Remote Options
		virtual BOOL RemoteReject (BYTE  bType, PBYTE pData);
		virtual BOOL RemoteNak    (BYTE  bType, PBYTE pData);
		virtual void RemoteDefault(void);
		virtual void RemoteAgreed (PCBYTE pData, UINT  uSize);

		// Local Options
		virtual void LocalDefault(void);
		virtual void LocalRequest(CBuffer *pBuff);
		virtual BOOL LocalReject (BYTE bType, PBYTE pData);
		virtual BOOL LocalNak    (BYTE bType, PBYTE pData);
		virtual void LocalAgreed (void);
		
		// Extended Codes
		virtual BOOL CodeUnknown(void);

		// Loop Detection
		BOOL CheckLocalLoop(void);
		BOOL CheckRemoteLoop(void);

		// Implementation
		void PutFrame(CBuffer *pBuff, BYTE bCode, BYTE bID);
		void SetState(UINT uState);
	};

// End of File

#endif
