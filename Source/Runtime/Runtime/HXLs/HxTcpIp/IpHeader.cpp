
#include "Intern.hpp"

#include "IpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// IP Header Wrapper Class
//

// Static Data

WORD CIpHeader::m_NextId = 0;

// Attributes

WORD CIpHeader::GetDataLength(void) const
{
	return m_TotalLen - m_HdrLen * sizeof(DWORD);
	}

WORD CIpHeader::GetOptsLength(void) const
{
	return m_HdrLen * sizeof(DWORD) - 20;
	}

BOOL CIpHeader::TestChecksum(void) const
{
	if( !m_Checksum ) {

		return TRUE;
		}

	if( !CalcChecksum() ) {

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CIpHeader::SetHeader(IPREF IpSrc, IPREF IpDest, WORD Prot, UINT uSize)
{
	m_Version  = 4;

	m_HdrLen   = 5;
	
	m_Type	   = 0;

	m_TotalLen = WORD(uSize + m_HdrLen * sizeof(DWORD));
	
	m_Id	   = m_NextId++;
	
	m_Fragment = 0;
	
	m_TTL	   = IpDest.IsMulticast() ? 1 : 128;
	
	m_Protocol = BYTE(Prot);
	
	m_Checksum = 0;
	
	m_IpSrc	   = IpSrc;
	
	m_IpDest   = IpDest;
	}

void CIpHeader::AddChecksum(void)
{
	m_Checksum = 0;

	m_Checksum = CalcChecksum();
	}

// Implementation

WORD CIpHeader::CalcChecksum(void) const
{
	return Checksum(PBYTE(this), m_HdrLen * 4);
	}

// End of File
