
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ArpFrame_HPP

#define	INCLUDE_ArpFrame_HPP

//////////////////////////////////////////////////////////////////////////
//
// ARP Frame
//

#pragma pack(1)

struct ARPFRAME
{
	WORD		m_HardwareType;
	WORD		m_ProtocolType;
	BYTE		m_HardwareLen;
	BYTE		m_ProtocolLen;
	WORD		m_Opcode;
	CMacAddr	m_MacSrc;
	CIpAddr		m_IpSrc;
	CMacAddr	m_MacDest;
	CIpAddr		m_IpDest;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////////
//
// ARP Frame Wrapper
//

class CARPFrame : public ARPFRAME
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);

		// Attributes
		BOOL IsReq(void) const;
		BOOL IsRep(void) const;

		// Operations
		void MakeReq(MACREF MacSrc, IPREF IpSrc, IPREF IpDest);
		void MakeRep(MACREF MacSrc);

	private:
		// Constructor
		CARPFrame(void);
	};

//////////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Conversion

STRONG_INLINE void CARPFrame::NetToHost(void)
{
	m_HardwareType = ::NetToHost(m_HardwareType);

	m_ProtocolType = ::NetToHost(m_ProtocolType);

	m_Opcode       = ::NetToHost(m_Opcode);
	}

STRONG_INLINE void CARPFrame::HostToNet(void)
{
	m_HardwareType = ::HostToNet(m_HardwareType);

	m_ProtocolType = ::HostToNet(m_ProtocolType);

	m_Opcode       = ::HostToNet(m_Opcode);
	}

// Attributes

STRONG_INLINE BOOL CARPFrame::IsReq(void) const
{
	if( m_HardwareType == HW_ETHERNET ) {
		
		if( m_ProtocolType == ET_IP ) {
			
			if( m_Opcode == ARP_REQUEST ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

STRONG_INLINE BOOL CARPFrame::IsRep(void) const
{
	if( m_HardwareType == HW_ETHERNET ) {
		
		if( m_ProtocolType == ET_IP ) {
			
			if( m_Opcode == ARP_REPLY ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// End of File

#endif

