
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ppp_HPP

#define	INCLUDE_Ppp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPppDriver;
class CModem;
class CHdlc;
class CLcp;
class CPap;
class CChap;
class CIpcp;

//////////////////////////////////////////////////////////////////////////
//
// PPP Protocols
//

#define	PROT_MODEM	WORD(0xFFFF)
#define	PROT_HDLC	WORD(0xFFFE)
#define	PROT_LCP	WORD(0xC021)
#define	PROT_PAP	WORD(0xC023)
#define	PROT_CHAP	WORD(0xC223)
#define	PROT_IPCP	WORD(0x8021)
#define	PROT_IP		WORD(0x0021)
#define	PROT_NONE	WORD(0x0000)

//////////////////////////////////////////////////////////////////////////
//
// PPP Phases
//

enum
{
	phaseIdle		= 0,
	phaseConnectLink	= 1,
	phaseAuthenticate	= 2,
	phaseConnectNetwork	= 3,
	phaseConnected		= 4,
	phaseDisconnect		= 5,
	phaseDone		= 6
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Frame
//

#pragma pack(1)

struct PPPFRAME
{
	BYTE	m_bAddress;
	BYTE	m_bControl;
	WORD	m_wProtocol;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// PPP Frame Wrapper
//

class CPppFrame : public PPPFRAME
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);

		// Data Members
		BYTE m_bData[];

	private:
		// Constructor
		CPppFrame(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Core Protocol
//

class CPpp
{
	public:
		// Constructor
		CPpp(CPppDriver *pDriver, CConfigPpp const &Config);

		// Destructor
		~CPpp(void);

		// Attributes
		BOOL    IsDirect (void) const;
		PCTXT   GetStatus(void) const;
		HTHREAD GetThread(void) const;

		// Operations
		void Bind(IRouter *pRouter, IPacketSink *pSink, UINT uFace);
		void Open(void);
		void Close(void);
		void Service(void);
		void StopTask(void);

		// Timed Events
		void OnTime(void);

		// Layer Events
		void OnLayerEvent(WORD wProtocol, UINT uEvent);

		// Frame Handling
		void OnRecv(CBuffer *pBuff);

		// Frame Sending
		void PutPPP(WORD wProtocol, CBuffer *pBuff);
		void PutIP (CBuffer *pBuff);

	protected:
		// Data Members
		CPppDriver  * m_pDriver;
		BOOL	      m_fServer;
		BOOL	      m_fGateway;
		CIpAddr	      m_RemAddr;
		CIpAddr	      m_RemMask;
		UINT	      m_uTimeout;
		HTHREAD       m_hThread;
		IMutex      * m_pMutex;
		IRouter     * m_pRouter;
		IPacketSink * m_pSink;
		UINT          m_uFace;
		CModem      * m_pModem;
		CHdlc       * m_pHdlc;
		CLcp        * m_pLcp;
		CPap        * m_pPap;
		CChap       * m_pChap;
		CIpcp       * m_pIpcp;
		BOOL	      m_fOpen;
		UINT	      m_uPhase;
		BOOL	      m_fNetwork;
		WORD	      m_wAuth;
		UINT	      m_uCount;
		UINT	      m_uStart1;
		UINT	      m_uStart2;
		UINT	      m_uTimer1;
		UINT	      m_uTimer2;
		char	      m_sStatus[21];

		// Events
		void OnModemFinished(void);
		void OnDataFailed(void);
		void OnLinkUp(void);
		void OnLinkDown(void);
		void OnLinkFinished(void);
		void OnLinkFailed(void);
		void OnAuthPassed(void);
		void OnAuthFailed(void);
		void OnNetworkUp(void);
		void OnNetworkDown(void);
		void OnNetworkFinished(void);
		void OnNetworkFailed(void);

		// Actions
		void SetLinkIdle(void);
		void ConnectLink(void);
		void Authenticate(void);
		void ConnectNet(void);
		void DisconnectLink(void);
		void EnableNetwork(void);
		void DisableNetwork(void);
		void ReloadTimer1(void);
		void ReloadTimer2(void);
		void CheckTimer1(void);
		void CheckTimer2(void);
		void ReloadTimer(UINT &uStart, UINT &uTimer);
		void CheckTimer(UINT &uStart, UINT &uTimer);

		// Route Management
		void EnableInterface(void);
		void DisableInterface(void);
		void RegisterRoutes(void);
		void RegisterOnDemand(void);

		// Implementation
		void SetPhase(UINT uPhase);
		void ReadLinkOptions(void);
		BOOL OnDemand(void);
		void ClaimData(void);
		void FreeData(void);
		void SetStatus(PCTXT pStatus);
		void PutFrame(WORD wProtocol, CBuffer *pBuff);
	};

// End of File

#endif
