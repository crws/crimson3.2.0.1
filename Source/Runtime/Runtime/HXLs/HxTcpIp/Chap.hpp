
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Chap_HPP

#define	INCLUDE_Chap_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PppLayer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPpp;

class CLcpFrame;

//////////////////////////////////////////////////////////////////////////
//
// CHAP Codes
//

enum {
	chapChallenge	= 1,
	chapResponse	= 2,
	chapSuccess	= 3,
	chapFailure	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Challenge Handshake Authentication Protocol
//

class CChap : public CPppLayer
{
	public:
		// Constructor
		CChap(CPpp *pPpp, CConfigPpp const &Config);

		// Destructor
		~CChap(void);

		// Management
		void LowerLayerUp(void);
		void LowerLayerDown(void);
		void OnTime(void);

		// Frame Handling
		void OnRecv(CBuffer *pBuff);

	protected:
		// Data Members
		BOOL	    m_fServer;
		char	    m_sPass[32];
		char	    m_sUser[32];
		CLcpFrame * m_pLcp;
		BYTE	    m_bID;
		UINT	    m_uCount;
		UINT	    m_uStart;
		UINT	    m_uTimer;
		UINT	    m_uRetry;
		BYTE        m_bChallenge[16];
		BYTE        m_bResponse [16];

		// Implementation
		void AddValue(CBuffer *pBuff, PBYTE pValue, UINT uSize);
		void AddText (CBuffer *pBuff, PCTXT pText);
		void PutCHAP (CBuffer *pBuff, BYTE bCode, BYTE bID);
		void SendResponse(BYTE bCode);
		void SendChallenge(void);
		BOOL CheckResponse(void);
		void StartTimer(void);
		void StopTimer(void);

		// Challenge Support
		void MakeChallenge(void);
		void MakeResponse(void);
		void Swap(PBYTE pData, UINT uSize);
		void Add(PBYTE pBuff, UINT &uPtr, PBYTE pData, UINT uSize);
	};

// End of File

#endif
