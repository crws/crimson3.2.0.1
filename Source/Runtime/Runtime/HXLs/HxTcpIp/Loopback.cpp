
#include "Intern.hpp"

#include "Loopback.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Loopback Packet Driver
//

// Instantiator

IPacketDriver * Create_Loopback(void)
{
	return New CLoopbackDriver;
	}

// Constructor

CLoopbackDriver::CLoopbackDriver(void)
{
	StdSetRef();

	m_pSink = NULL;

	m_uFace = NOTHING;
	}

// IUnknown

HRESULT CLoopbackDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPacketDriver);

	StdQueryInterface(IPacketDriver);

	return E_NOINTERFACE;
	}

ULONG CLoopbackDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CLoopbackDriver::Release(void)
{
	StdRelease();
	}

// IPacketDriver

bool CLoopbackDriver::Bind(IRouter *pRouter)
{
	return true;
	}

bool CLoopbackDriver::Bind(IPacketSink *pSink, UINT uFace)
{
	m_pSink = pSink;

	m_uFace	= uFace;
	
	return true;
	}

CString CLoopbackDriver::GetDeviceName(void)
{
	return "lo";
	}

CString CLoopbackDriver::GetStatus(void)
{
	return "UP";
	}

bool CLoopbackDriver::SetIpAddress(IPREF Ip, IPREF Mask)
{
	return false;
	}

bool CLoopbackDriver::SetIpGateway(IPREF Gate)
{
	return false;
	}

bool CLoopbackDriver::GetMacAddress(MACADDR &Addr)
{
	return false;
	}

bool CLoopbackDriver::GetDhcpOption(BYTE bType, UINT &uValue)
{
	if( bType == 0xFF ) {

		uValue = 1;

		return true;
		}

	return false;
	}

bool CLoopbackDriver::GetSockOption(UINT uCode, UINT &uValue)
{
	return false;
	}

bool CLoopbackDriver::SetMulticast(IPADDR *pList, UINT uCount)
{
	return false;
	}

bool CLoopbackDriver::Open(void)
{
	m_pSink->OnLinkActive(m_uFace);

	return true;
	}

bool CLoopbackDriver::Close(void)
{
	m_pSink->OnLinkDown(m_uFace);

	return true;
	}

void CLoopbackDriver::Poll(void)
{
	}

bool CLoopbackDriver::Send(IPREF Ip, CBuffer *pBuff)
{
	CBuffer *pCopy = pBuff->MakeCopy();

	if( pCopy ) {

		m_pSink->OnPacket(m_uFace, pCopy);
		}

	return true;
	}

// End of File
