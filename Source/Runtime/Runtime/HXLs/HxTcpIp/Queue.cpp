
#include "Intern.hpp"

#include "Queue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Queue Protocol
//

// Instantiator

IQueue * Create_Queue(void)
{
	return New CQueue;
	}

// Constructor

CQueue::CQueue(void)
{
	StdSetRef();

	m_pRouter = NULL;

	m_pUtils  = NULL;

	m_uMask   = 0;

	m_pLock   = Create_Rutex();

	m_pFlag   = Create_Semaphore();

	m_uHead   = 0;

	m_uTail   = 0;

	m_pFlag->Signal(elements(m_Queue) - 1);
	}

// Destructor

CQueue::~CQueue(void)
{
	m_pLock->Release();

	m_pFlag->Release();
	}

// IUnknown

HRESULT CQueue::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IQueue);

	StdQueryInterface(IQueue);

	StdQueryInterface(IProtocol);

	return E_NOINTERFACE;
	}

ULONG CQueue::AddRef(void)
{
	StdAddRef();
	}

ULONG CQueue::Release(void)
{
	StdRelease();
	}

// IQueue

BOOL CQueue::Queue(UINT uFace, IPREF Gate, CBuffer *pBuff)
{
	if( m_pFlag->Wait(250) ) {

		CAutoLock Lock(m_pLock);

		CEntry &Entry = m_Queue[m_uTail];

		m_uTail       = (m_uTail + 1) % elements(m_Queue);

		Entry.m_uFace = uFace;

		Entry.m_Gate  = Gate;

		Entry.m_pBuff = pBuff;

		m_pRouter->SendReq(m_uMask, TRUE);

		return TRUE;
		}

	TcpDebug(OBJ_QUEUE, LEV_WARN, "protocol busy\n");

	BuffRelease(pBuff);

	return FALSE;
	}

// IProtocol Methods

BOOL CQueue::Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask)
{
	m_pRouter = pRouter;

	m_pUtils  = pUtils;

	m_uMask   = uMask;

	return TRUE;
	}

BOOL CQueue::CreateSocket(ISocket * &pSocket)
{
	pSocket = NULL;

	return FALSE;
	}

BOOL CQueue::Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff)
{	
	return TRUE;
	}

BOOL CQueue::Send(void)
{
	if( m_uHead != m_uTail ) {

		CEntry &Entry = m_Queue[m_uHead];

		m_uHead       = (m_uHead + 1) % elements(m_Queue);

		if( m_uHead == m_uTail ) {

			m_pRouter->SendReq(m_uMask, FALSE);
			}

		m_pRouter->Send(Entry.m_uFace, Entry.m_Gate, Entry.m_pBuff);

		BuffRelease(Entry.m_pBuff);

		m_pFlag->Signal(1);

		return TRUE;
		}

	return TRUE;
	}

BOOL CQueue::Poll(void)
{
	return TRUE;
	}

BOOL CQueue::NetStat(IDiagOutput *pOut)
{
	return TRUE;
	}

BOOL CQueue::SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	return TRUE;
	}

// End of File
