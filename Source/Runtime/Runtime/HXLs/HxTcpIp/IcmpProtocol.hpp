
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IcmpProtocol_HPP

#define	INCLUDE_IcmpProtocol_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpHeader;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IcmpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ICMP Protocol
//

class CIcmp : public IThreadNotify, public IProtocol
{
	public:
		// Constructor
		CIcmp(void);

		// Destructor
		~CIcmp(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IThreadNotify
		UINT METHOD OnThreadCreate(IThread *pThread, UINT uIndex);
		void METHOD OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam);

		// IProtocol Methods
		BOOL Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask);
		BOOL CreateSocket(ISocket * &pSocket);
		BOOL Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff);
		BOOL Send(void);
		BOOL Poll(void);
		BOOL NetStat(IDiagOutput *pOut);
		BOOL SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff);

		// Operations
		BOOL SendVisible(IPREF Dest, IPREF From, CBuffer *pBuff);
		BOOL SendStealth(IPREF Dest, IPREF From, CBuffer *pBuff);
		BOOL SendPing(IPREF Dest, CBuffer *pBuff);
		BOOL SendPing(IPREF Dest, PBYTE pData, UINT uSize);
		BOOL SendUnreachable(CIpHeader *pIp, BYTE bCode);
		BOOL SendTimeExceeded(CIpHeader *pIp, BYTE bCode);
		BOOL SendHostRedirect(CIpHeader *pIp, IPREF Gateway);
		void FreeSocket(CIcmpSocket *pSock);

		// Public Data
		IMutex * m_pLock;

	protected:
		// List Root
		struct CListRoot
		{
			CIcmpSocket * m_pHead;
			CIcmpSocket * m_pTail;
			};

		// Data Members
		ULONG           m_uRefs;
		IRouter	      * m_pRouter;
		INetUtilities * m_pUtils;
		BOOL	        m_fNotify;
		UINT	        m_uMask;
		WORD	        m_wSeq;
		CIcmpSocket     m_Socket;

		// Implementation
		void AddSocket(CIcmpSocket *pSock);
	};

// End of File

#endif
