
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UdpHeader_HPP

#define	INCLUDE_UdpHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// UDP Header
//

#pragma pack(1)

struct UDPHEADER
{
	WORD	m_LocPort;
	WORD	m_RemPort;
	WORD	m_Length;
	WORD	m_Checksum;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// UDP Header Wrapper
//

class CUdpHeader : public UDPHEADER
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);

		// Attributes
		BOOL TestChecksum(PSREF Ps);

		// Operations
		void AddChecksum(DWORD Load);

	private:
		// Constructor
		CUdpHeader(void);

	protected:
		// Implementation
		WORD CalcChecksum(DWORD Load) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Conversion

STRONG_INLINE void CUdpHeader::NetToHost(void)
{
	m_LocPort = ::NetToHost(m_LocPort);

	m_RemPort = ::NetToHost(m_RemPort);
	
	m_Length  = ::NetToHost(m_Length);
	}

STRONG_INLINE void CUdpHeader::HostToNet(void)
{
	m_LocPort = ::HostToNet(m_LocPort);

	m_RemPort = ::HostToNet(m_RemPort);
	
	m_Length  = ::HostToNet(m_Length);
	}

// End of File

#endif
