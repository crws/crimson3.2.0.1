
#include "Intern.hpp"

#include "TcpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "PseudoHeader.hpp"

#include "TcpHeader.hpp"

#include "TcpProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP Socket
//

// Static Data

UINT CTcpSocket::m_NextPort = NOTHING;

UINT debugControl           = 0;

UINT debugPort		    = NOTHING;

// Constructor

CTcpSocket::CTcpSocket(void)
{
	StdSetRef();

	m_pTcp     = NULL;

	m_uState   = stateFree;

	m_fReqSend = FALSE;
}

// Destructor

CTcpSocket::~CTcpSocket(void)
{
}

// Binding

void CTcpSocket::Bind(CTcp *pTcp, UINT uIndex)
{
	m_pTcp   = pTcp;

	m_uIndex = uIndex;

	m_uLast  = GetTickCount();
}

// Attributes

BOOL CTcpSocket::IsFree(void) const
{
	return m_uState == stateFree;
}

BOOL CTcpSocket::IsWaiting(void) const
{
	if( m_fReqFree ) {

		switch( m_uState ) {

			case stateTimeWait:

				return TRUE;
		}
	}

	return FALSE;
}

UINT CTcpSocket::GetWait(void) const
{
	return m_KillTime;
}

// Operations

void CTcpSocket::Create(void)
{
	InitRetryList();

	InitData();

	DefaultTuning();

	AddRef();
}

BOOL CTcpSocket::EndWait(void)
{
	if( IsWaiting() ) {

		SetState(stateClosed);
	}

	return IsFree();
}

BOOL CTcpSocket::EndClose(void)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	if( m_fReqFree ) {

		switch( m_uState ) {

			case stateFinWait1:
			case stateFinWait2:

				Abort();

				return TRUE;
		}
	}

	return FALSE;
}

void CTcpSocket::NetStat(IDiagOutput *pOut)
{
	if( m_uState != stateFree ) {

		pOut->AddRow();

		pOut->SetData(0, "TCP");

		pOut->SetData(1, PCTXT(CPrintf("%-15s : %-5u", PCTXT(m_LocIP.GetAsText()), m_LocPort)));

		pOut->SetData(2, PCTXT(CPrintf("%-15s : %-5u", PCTXT(m_RemIP.GetAsText()), m_RemPort)));

		pOut->SetData(3, GetStateText(m_uState));

		pOut->EndRow();
	}
}

// IUnknown

HRESULT CTcpSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
}

ULONG CTcpSocket::AddRef(void)
{
	StdAddRef();
}

ULONG CTcpSocket::Release(void)
{
	if( m_uRefs > 1 ) {

		if( AtomicDecrement(&m_uRefs) == 1 ) {

			Debug(debugUser, "user", "release");

			m_pTcp->FreeSocket(this);

			CAutoLock Lock(m_pTcp->m_pLock);

			switch( m_uState ) {

				case stateFree:

					return 1;

				case stateLinger:

					StartKillTimer();

					SetState(stateTimeWait);

					break;

				case stateError:
				case stateInit:
				case stateClosed:

					if( !m_fReqRST ) {

						SetState(stateFree);

						UpdateSendRequest();

						return 1;
					}
					break;

				case stateEstab:

					if( !m_fReqClose ) {

						SetState(stateError);

						m_fReqRST = TRUE;

						UpdateSendRequest();
					}
					break;
			}

			m_fReqFree = TRUE;

			return 1;
		}

		return m_uRefs;
	}

	AfxAssert(FALSE);

	return m_uRefs;
}

// ISocket

HRESULT CTcpSocket::Listen(WORD Loc)
{
	Debug(debugUser, "user", "open passive");

	if( m_uState == stateInit ) {

		m_fActive   = FALSE;

		m_LocIP     = IP_EMPTY;

		m_RemIP     = IP_EMPTY;

		m_LocPort   = Loc ? Loc : (32768 + (m_NextPort++ % 16384));

		m_RemPort   = 0;

		m_pTcp->Enable(m_LocPort);

		SetState(stateListen);

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CTcpSocket::Listen(IPADDR const &IP, WORD Loc)
{
	return E_FAIL;
}

HRESULT CTcpSocket::Connect(IPADDR const &IP, WORD Rem)
{
	if( m_NextPort == NOTHING ) {

		m_NextPort = rand();
	}

	return Connect(IP, Rem, 32768 + (m_NextPort++ % 16384));
}

HRESULT CTcpSocket::Connect(IPADDR const &IP, WORD Rem, WORD Loc)
{
	Debug(debugUser, "user", "open active");

	if( m_uState == stateInit ) {

		m_fActive  = TRUE;

		m_LocIP    = IP_EMPTY;

		m_RemIP	   = IP;

		m_LocPort  = Loc;

		m_RemPort  = Rem;

		m_SendISN  = MAKELONG(rand(), rand());

		m_SendUna  = m_SendISN;

		m_SendNxt  = m_SendISN;

		m_SendWnd  = 1;

		m_fReqSYN  = TRUE;

		m_pTcp->Enable(m_LocPort);

		m_pTcp->CopyOptions(this, m_RemIP);

		SetState(stateSynSent);

		UpdateSendRequest();

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CTcpSocket::Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc)
{
	// TODO -- This should be implemented for RLOS...

	return Connect(Ip, Rem, Loc);
}

HRESULT CTcpSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pData, uSize) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CTcpSocket::Recv(PBYTE pData, UINT &uSize)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	if( m_pRxBuff ) {

		UINT uData = m_pRxBuff->GetSize();

		UINT uRecv = min(uSize, uData);

		memcpy(pData, m_pRxBuff->GetData(), uRecv);

		if( uRecv == uData ) {

			BuffRelease(m_pRxBuff);

			m_pRxBuff = NULL;
		}
		else
			m_pRxBuff->StripHead(uRecv);

		CalcRecvWindow();

		UpdateSendRequest();

		uSize = uRecv;

		return S_OK;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CTcpSocket::Send(PBYTE pData, UINT &uSize)
{
	if( pData && uSize ) {

		Debug(debugData, "data", "send %u", uSize);

		CAutoLock Lock(m_pTcp->m_pLock);

		switch( m_uState ) {

			case stateSynSent:
			case stateSynRcvd:
			case stateEstab:
			case stateCloseWait:

				if( QueueData(pData, uSize) ) {

					UpdateSendRequest();

					return S_OK;
				}
				break;
		}
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CTcpSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pBuff) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CTcpSocket::Recv(CBuffer * &pBuff)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	if( m_pRxBuff ) {

		pBuff = m_pRxBuff;

		m_pRxBuff = NULL;

		CalcRecvWindow();

		UpdateSendRequest();

		return S_OK;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CTcpSocket::Send(CBuffer *pBuff)
{
	if( pBuff ) {

		UINT uSize = pBuff->GetSize();

		if( uSize ) {

			Debug(debugData, "data", "send %u", uSize);

			CAutoLock Lock(m_pTcp->m_pLock);

			switch( m_uState ) {

				case stateSynSent:
				case stateSynRcvd:
				case stateEstab:
				case stateCloseWait:

					if( QueueData(pBuff, uSize) ) {

						UpdateSendRequest();

						return S_OK;
					}
					break;
			}
		}
	}

	return E_FAIL;
}

HRESULT CTcpSocket::GetLocal(IPADDR &IP)
{
	IP = m_LocIP;

	return S_OK;
}

HRESULT CTcpSocket::GetRemote(IPADDR &IP)
{
	IP = m_RemIP;

	return S_OK;
}

HRESULT CTcpSocket::GetLocal(IPADDR &IP, WORD &Port)
{
	IP   = m_LocIP;

	Port = m_LocPort;

	return S_OK;
}

HRESULT CTcpSocket::GetRemote(IPADDR &IP, WORD &Port)
{
	IP   = m_RemIP;

	Port = m_RemPort;

	return S_OK;
}

HRESULT CTcpSocket::GetPhase(UINT &Phase)
{
	switch( m_uState ) {

		case stateInit:
		case stateListen:

			Phase = PHASE_IDLE;

			break;

		case stateSynSent:
		case stateSynRcvd:

			Phase = PHASE_OPENING;

			break;

		case stateEstab:

			Phase = PHASE_OPEN;

			break;

		case stateCloseWait:

			Phase = m_pRxBuff ? PHASE_OPEN : PHASE_CLOSING;

			break;

		case stateFinWait1:
		case stateFinWait2:

			Phase = m_fLinger ? PHASE_OPEN : PHASE_ERROR;

			break;

		case stateLinger:

			Phase = m_pRxBuff ? PHASE_OPEN : PHASE_CLOSING;

			break;

		case stateClosing:
		case stateLastAck:
		case stateTimeWait:
		case stateClosed:
		case stateError:

			Phase = PHASE_ERROR;

			break;
	}

	return S_OK;
}

HRESULT CTcpSocket::SetOption(UINT uOption, UINT uValue)
{
	switch( uOption ) {

		case OPT_SEND_MSS:
		{
			CAutoLock Lock(m_pTcp->m_pLock);

			m_SendMax = WORD(uValue);

			m_SendMSS = min(m_SendMSS, m_SendMax);

			return S_OK;
		}

		case OPT_RECV_MSS:
		{
			m_RecvMSS = WORD(uValue);

			return S_OK;
		}

		case OPT_SEND_QUEUE:
		{
			CAutoLock Lock(m_pTcp->m_pLock);

			if( m_uState == stateInit ) {

				m_uRetry = min(uValue, elements(m_Retry));

				return S_OK;
			}

			return E_FAIL;
		}

		case OPT_LINGER:
		{
			m_fLinger = uValue ? TRUE : FALSE;

			return S_OK;
		}

		case OPT_LOCAL:
		{
			CAutoLock Lock(m_pTcp->m_pLock);

			if( m_uState == stateInit ) {

				LocalTuning();

				return S_OK;
			}

			return E_FAIL;
		}

		case OPT_KEEP_ALIVE:
		{
			m_fKeepAlive = uValue ? TRUE : FALSE;

			return S_OK;
		}

		case OPT_NAGLE:
		{
			m_fNagle = uValue ? TRUE : FALSE;

			return S_OK;
		}
	}

	return E_FAIL;
}

HRESULT CTcpSocket::Abort(void)
{
	Debug(debugUser, "user", "abort");

	CAutoLock Lock(m_pTcp->m_pLock);

	switch( m_uState ) {

		case stateFree:
		case stateLinger:
		case stateError:

			break;

		case stateSynRcvd:
		case stateEstab:
		case stateFinWait1:
		case stateFinWait2:
		case stateCloseWait:

			SetState(stateError);

			m_fReqRST = TRUE;

			UpdateSendRequest();

			break;

		default:

			SetState(stateError);

			UpdateSendRequest();

			break;
	}

	return S_OK;
}

HRESULT CTcpSocket::Close(void)
{
	Debug(debugUser, "user", "close");

	CAutoLock Lock(m_pTcp->m_pLock);

	if( !m_fReqClose ) {

		if( m_pTxBuff ) {

			m_fReqClose = TRUE;

			return S_OK;
		}

		MakeClosed();

		UpdateSendRequest();
	}

	return S_OK;
}

// Event Handlers

BOOL CTcpSocket::OnRecv(PSREF PS, CTcpHeader *pTcp, CBuffer *pBuff)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	if( IsAlive() ) {

		if( TakeSegment(PS, pTcp) ) {

			Debug(debugRecv, "recv", pTcp);

			switch( m_uState ) {

				case stateListen:

					if( !m_pTcp->AcceptIP(PS.m_Src) ) {

						BuffRelease(pBuff);
					}
					else
						RecvInListen(PS, pTcp, pBuff);

					break;

				case stateSynSent:

					RecvInSynSent(PS, pTcp, pBuff);

					break;

				case stateSynRcvd:
				case stateEstab:
				case stateFinWait1:
				case stateFinWait2:
				case stateCloseWait:
				case stateClosing:
				case stateLastAck:
				case stateTimeWait:

					StartPingTimer();

					RecvInOther(pTcp, pBuff);

					break;

				default:

					return FALSE;
			}

			UpdateSendRequest();

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CTcpSocket::OnSend(void)
{
	if( m_fReqSend ) {

		CAutoLock Lock(m_pTcp->m_pLock);

		if( IsAlive() ) {

			CBuffer *pBuff;

			if( GetRetry(pBuff) ) {

				if( IsRetryExhausted() ) {

					SetState(stateError);

					UpdateSendRequest();
				}
				else {
					CTcpHeader *pTcp = (CTcpHeader *) pBuff->GetData();

					if( pTcp->m_FlagACK ) {

						pTcp->m_Ack = m_RecvNxt;

						m_fReqACK   = FALSE;

						m_fReqSoon  = FALSE;

						m_fReqPing  = FALSE;
					}

					UpdateSendRequest();

					StartPingTimer();

					Lock.Free();

					Debug(debugRept, "rept", pTcp);

					LockRetry();

					m_pTcp->Send(m_RemIP, m_LocIP, pBuff, TRUE);

					FreeRetry();
				}

				return TRUE;
			}

			if( m_fReqRST ) {

				CBuffer *pBuff = BuffAllocate(0);

				if( pBuff ) {

					CTcpHeader *pTcp = BuffAddHead(pBuff, CTcpHeader);

					pTcp->Init();

					pTcp->m_FlagRST = TRUE;

					pTcp->m_LocPort = m_LocPort;

					pTcp->m_RemPort = m_RemPort;

					pTcp->m_Seq     = m_SendNxt;

					m_fReqRST = FALSE;

					UpdateSendRequest();

					StartPingTimer();

					Lock.Free();

					Debug(debugSend, "send", pTcp);

					m_pTcp->Send(m_RemIP, m_LocIP, pBuff, FALSE);

					return TRUE;
				}

				return FALSE;
			}

			if( CanSendSegment() ) {

				CTcpHeader *pTcp  = NULL;

				CBuffer    *pBuff = m_pTxBuff;

				if( !pBuff ) {

					pBuff = BuffAllocate(0);

					if( pBuff ) {

						pTcp = BuffAddHead(pBuff, CTcpHeader);

						pTcp->Init();

						pTcp->m_LocPort = m_LocPort;

						pTcp->m_RemPort = m_RemPort;
					}
					else {
						return FALSE;
					}
				}
				else {
					pTcp = (CTcpHeader *) pBuff->GetData();

					UINT uSize = pTcp->GetDataSize();

					UINT uLeft = GetSendLimit();

					UINT uSend = min(uLeft, m_SendMSS);

					if( uSize > uSend ) {

						CTcpHeader *pSplitTCP  = NULL;

						CBuffer    *pSplitBuff = BuffAllocate(uSend);

						if( pSplitBuff ) {

							pSplitTCP = BuffAddHead(pSplitBuff, CTcpHeader);

							pSplitTCP->Init();

							pSplitTCP->m_LocPort = m_LocPort;

							pSplitTCP->m_RemPort = m_RemPort;

							PBYTE pData = pBuff->GetData() + pTcp->GetHeaderSize();

							memcpy(pSplitBuff->AddTail(uSend),
							       pData,
							       uSend
							);

							memcpy(pData,
							       pData + uSend,
							       uSize - uSend
							);

							pBuff->StripTail(uSend);

							pTcp->SetDataSize(uSize - uSend);

							pSplitTCP->SetDataSize(uSend);

							pTcp  = pSplitTCP;

							pBuff = pSplitBuff;
						}
						else {
							return FALSE;
						}
					}
					else {
						m_pTxBuff = NULL;

						CheckClose();
					}

					pTcp->m_FlagPSH = TRUE;
				}

				if( !m_fReqSYN || m_fReqACK ) {

					pTcp->m_FlagACK = TRUE;

					pTcp->m_Ack	= m_RecvNxt;
				}

				pTcp->m_FlagSYN = m_fReqSYN ? TRUE : FALSE;

				pTcp->m_FlagFIN = m_fReqFIN ? TRUE : FALSE;

				m_fReqSYN       = FALSE;

				m_fReqFIN       = FALSE;

				m_fReqPing      = FALSE;

				if( pTcp->m_FlagSYN ) {

					pTcp->AddMaxSegSize(pBuff, m_RecvMSS);
				}

				if( pTcp->m_FlagACK ) {

					m_fReqACK  = FALSE;

					m_fReqSoon = FALSE;
				}

				pTcp->m_Seq = m_SendNxt;

				pTcp->m_Wnd = m_RecvWnd;

				m_RecvMax   = m_RecvNxt + m_RecvWnd;

				m_SendNxt  += pTcp->GetSymbols();

				AddRetry(pBuff);

				UpdateSendRequest();

				Lock.Free();

				Debug(debugSend, "send", pTcp);

				LockRetry();

				if( !m_pTcp->Send(m_RemIP, m_LocIP, pBuff, TRUE) ) {

					CAutoLock Lock(m_pTcp->m_pLock);

					if( pTcp->m_FlagSYN && !pTcp->m_FlagACK ) {

						SetState(stateError);

						UpdateSendRequest();
					}
				}

				FreeRetry();

				return TRUE;
			}

			if( m_fReqACK || m_fReqPing ) {

				pBuff = BuffAllocate(0);

				if( pBuff ) {

					CTcpHeader *pTcp = BuffAddHead(pBuff, CTcpHeader);

					pTcp->Init();

					pTcp->m_FlagACK	= TRUE;

					pTcp->m_LocPort = m_LocPort;

					pTcp->m_RemPort = m_RemPort;

					pTcp->m_Seq     = m_fReqACK ? m_SendNxt : (m_SendNxt - 1);

					pTcp->m_Ack     = m_RecvNxt;

					pTcp->m_Wnd     = m_RecvWnd;

					m_RecvMax  = m_RecvNxt + m_RecvWnd;

					m_fReqACK  = FALSE;

					m_fReqSoon = FALSE;

					m_fReqPing = FALSE;

					UpdateSendRequest();

					Lock.Free();

					Debug(debugSend, "sack", pTcp);

					m_pTcp->Send(m_RemIP, m_LocIP, pBuff, FALSE);

					return TRUE;
				}

				return FALSE;
			}
		}

		UpdateSendRequest();
	}

	return TRUE;
}

BOOL CTcpSocket::OnPoll(void)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	UINT uTime = GetTickCount();

	UINT uGone = uTime - m_uLast;

	if( IsAlive() ) {

		if( m_fReqSoon ) {

			if( INT(m_SoonTime -= uGone) <= 0 ) {

				m_fReqACK  = TRUE;

				m_fReqSoon = FALSE;
			}
		}

		if( m_TranTime ) {

			if( INT(m_TranTime -= uGone) <= 0 ) {

				if( m_fActive ) {

					SetState(stateError);
				}
				else
					ResetListen();

				m_TranTime = 0;
			}
		}

		if( m_KillTime ) {

			if( INT(m_KillTime -= uGone) <= 0 ) {

				SetState(stateClosed);

				m_KillTime = 0;
			}
		}

		if( m_PingTime ) {

			if( INT(m_PingTime -= uGone) <= 0 ) {

				if( IsPingable() ) {

					m_PingSent = 1;

					RequestPing();

					StartPongTimer();
				}
				else
					m_PingTime = 0;
			}
		}

		if( m_PongTime ) {

			if( INT(m_PongTime -= uGone) <= 0 ) {

				if( IsPingable() ) {

					if( m_PingSent++ < 8 ) {

						RequestPing();

						StartPongTimer();
					}
					else {
						SetState(stateError);

						m_PongTime = 0;
					}
				}
				else
					m_PongTime = 0;
			}
		}

		PollRetryList(uGone);

		UpdateSendRequest();
	}

	m_uLast = uTime;

	return TRUE;
}

// Implementation

void CTcpSocket::InitData(void)
{
	// Socket State

	m_uState	= stateInit;
	m_LocIP         = IP_EMPTY;
	m_RemIP         = IP_EMPTY;
	m_LocPort	= 0;
	m_RemPort	= 0;
	m_KillTime	= 0;
	m_SoonTime	= 0;
	m_TranTime	= 0;
	m_PingTime	= 0;
	m_PongTime	= 0;

	// Send Queue

	m_pTxBuff	= NULL;
	m_uRetry	= 2;
	m_uLocked       = 0;
	m_fReqACK	= FALSE;
	m_fReqSYN	= FALSE;
	m_fReqFIN	= FALSE;
	m_fReqRST	= FALSE;
	m_fReqSend	= FALSE;
	m_fReqSoon	= FALSE;
	m_fReqPing      = FALSE;

	// Socket State

	m_fActive	= FALSE;
	m_fReqClose	= FALSE;
	m_fReqFree	= FALSE;

	// Socket Config

	m_fLinger	= FALSE;
	m_fKeepAlive    = FALSE;
	m_fNagle        = TRUE;

	// Send Data

	m_SendMax	= 1460;
	m_SendMSS	= 536;
	m_SendISN	= 0;
	m_SendUna	= 0;
	m_SendNxt	= 0;
	m_SendWnd	= 0;
	m_SendWL1	= 0;
	m_SendWL2	= 0;

	// Recv Queue

	m_pRxBuff	= NULL;

	// Recv Data

	m_RecvMSS	= 1460;
	m_RecvISN	= 0;
	m_RecvNxt	= 0;
	m_RecvMax	= 0;
	m_RecvWnd	= 1460;

	// Pend List

	m_uPendCount	= 0;
	m_uPendLimit    = 0;
}

void CTcpSocket::LocalTuning(void)
{
	m_TripDamp	= 2;
	m_TripTime	= ToTicks(100);
	m_RetryTime	= ToTicks(400);
	m_RetryCount	= 8;
	m_KillDelay	= ToTicks(2000);
	m_TranDelay	= ToTicks(1000);
	m_AckDelay	= ToTicks(100);
	m_PingDelay	= ToTicks(4000);
	m_PongDelay	= ToTicks(400);
}

void CTcpSocket::DefaultTuning(void)
{
	m_TripDamp	= 2;
	m_TripTime	= ToTicks(1000);
	m_RetryTime	= ToTicks(4000);
	m_RetryCount	= 8;
	m_KillDelay	= ToTicks(15000);
	m_TranDelay	= ToTicks(22000);
	m_AckDelay	= ToTicks(500);
	m_PingDelay	= ToTicks(20000);
	m_PongDelay	= ToTicks(2000);
}

void CTcpSocket::ResetListen(void)
{
	FreeRetryList();

	FreePendingList();

	DefaultTuning();

	m_LocIP   = IP_EMPTY;

	m_RemIP   = IP_EMPTY;

	m_RemPort = 0;

	SetState(stateListen);
}

void CTcpSocket::StartKillTimer(void)
{
	m_KillTime = m_KillDelay;
}

void CTcpSocket::StartTranTimer(UINT n)
{
	m_TranTime = m_TranDelay * n;
}

void CTcpSocket::StartPingTimer(void)
{
	if( m_fKeepAlive && IsPingable() ) {

		m_PingTime = m_PingDelay;

		m_PongTime = 0;
	}
	else {
		m_PingTime = 0;

		m_PongTime = 0;
	}
}

void CTcpSocket::StartPongTimer(void)
{
	m_PongTime = m_PongDelay * ((1+m_PingSent) / 2);

	m_PingTime = 0;
}

void CTcpSocket::StopKillTimer(void)
{
	m_KillTime = 0;
}

void CTcpSocket::StopTranTimer(void)
{
	m_TranTime = 0;
}

void CTcpSocket::SetState(UINT uState)
{
	if( m_uState != uState ) {

		Debug(debugState,
		      "stat",
		      "From %s to %s",
		      GetStateText(m_uState),
		      GetStateText(uState)
		);

		m_uState = uState;

		CheckRelease();
	}
}

void CTcpSocket::CheckRelease(void)
{
	switch( m_uState ) {

		case stateSynRcvd:
		case stateSynSent:

			StartTranTimer(1);

			break;

		case stateListen:
		case stateEstab:

			StopTranTimer();

			StartPingTimer();

			break;

		case stateTimeWait:

			StopTranTimer();

			FreeRetryList();

			FreePendingList();

			break;

		case stateClosed:

			StopTranTimer();

			StopKillTimer();

			FreeRetryList();

			CheckFree();

			break;

		case stateError:

			FreeRetryList();

			FreePendingList();

			FreeRxBuff();

			FreeTxBuff();

			CheckFree();

			break;
	}
}

void CTcpSocket::CheckFree(void)
{
	if( m_fReqFree ) {

		m_fReqFree = FALSE;

		SetState(stateFree);
	}
}

void CTcpSocket::CheckClose(void)
{
	if( m_fReqClose ) {

		MakeClosed();

		m_fReqClose = FALSE;
	}
}

void CTcpSocket::MakeClosed(void)
{
	switch( m_uState ) {

		case stateInit:
		case stateSynSent:
		case stateListen:

			SetState(stateClosed);

			break;

		case stateSynRcvd:
		case stateEstab:

			m_fReqFIN = TRUE;

			SetState(stateFinWait1);

			break;

		case stateCloseWait:

			m_fReqFIN = TRUE;

			SetState(stateLastAck);

			break;
	}

	if( !m_fLinger ) {

		StartTranTimer(2);
	}

	FreeRxBuff();
}

void CTcpSocket::FreeRxBuff(void)
{
	if( m_pRxBuff ) {

		BuffRelease(m_pRxBuff);

		m_pRxBuff = NULL;
	}
}

void CTcpSocket::FreeTxBuff(void)
{
	if( m_pTxBuff ) {

		BuffRelease(m_pTxBuff);

		m_pTxBuff = NULL;
	}
}

void CTcpSocket::CalcTripTime(UINT Trip)
{
	if( Trip < 30000 ) {

		DWORD a = m_TripTime * m_TripDamp;

		DWORD b = Trip * (10 - m_TripDamp);

		m_TripTime  = (a + b) / 10;

		m_TripDamp  = min(m_TripDamp + 1, 8);

		////////

		m_RetryTime = min(4 * m_TripTime, ToTicks(60000));

		m_KillDelay = min(8 * m_TripTime, ToTicks(60000));

		m_TranDelay = min(16 * m_TripTime, ToTicks(60000));

		m_PingDelay = min(16 * m_TripTime, ToTicks(60000));

		m_PongDelay = min(4 * m_TripTime, ToTicks(60000));

		m_AckDelay  = m_TripTime / 2;

		////////

		m_RetryTime = Limit(m_RetryTime, 400, 30000);

		m_KillDelay = Limit(m_KillDelay, 800, 60000);

		m_TranDelay = Limit(m_TranDelay, 4000, 60000);

		m_PingDelay = Limit(m_PingDelay, 4000, 60000);

		m_PongDelay = Limit(m_PongDelay, 400, 60000);

		m_AckDelay  = Limit(m_AckDelay, 100, 500);
	}
}

UINT CTcpSocket::Limit(UINT a, UINT b, UINT c)
{
	if( a < (b = ToTicks(b)) ) {

		return b;
	}

	if( a >(c = ToTicks(c)) ) {

		return c;
	}

	return a;
}

BOOL CTcpSocket::IsAlive(void)
{
	switch( m_uState ) {

		case stateInit:
		case stateFree:

			return FALSE;

		case stateError:

			return m_fReqRST;
	}

	return TRUE;
}

BOOL CTcpSocket::IsPingable(void)
{
	switch( m_uState ) {

		case stateEstab:

			return TRUE;
	}

	return FALSE;
}

// Queue Control

void CTcpSocket::UpdateSendRequest(void)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	if( IsDataAvailable() ) {

		if( !m_fReqSend ) {

			m_fReqSend = TRUE;

			m_pTcp->SendReq(TRUE);
		}
	}
	else {
		if( m_fReqSend ) {

			m_fReqSend = FALSE;

			if( m_fReqFree ) {

				CheckRelease();
			}

			m_pTcp->SendReq(FALSE);
		}
	}
}

BOOL CTcpSocket::IsDataAvailable(void)
{
	if( IsAlive() ) {

		if( m_fReqACK || m_fReqRST || m_fReqPing )
			return TRUE;

		if( m_uState == stateClosed )
			return FALSE;

		if( IsRetryNeeded() )
			return TRUE;

		if( CanSendSegment() )
			return TRUE;
	}

	return FALSE;
}

BOOL CTcpSocket::CanSendSegment(void)
{
	if( !IsRetryListFull() ) {

		if( m_SendWnd > m_SendNxt - m_SendUna ) {

			if( m_fReqSYN || m_fReqFIN )
				return TRUE;

			if( m_pTxBuff ) {

				if( m_fReqACK )
					return TRUE;

				if( m_SendUna == m_SendNxt )
					return TRUE;

				if( !IsSmallSegment() )
					return TRUE;

				return FALSE;
			}
		}

		if( IsZeroWindowSYN(m_SendWnd) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CTcpSocket::IsSmallSegment(void)
{
	if( m_fNagle ) {

		CTcpHeader *pTcp = (CTcpHeader *) m_pTxBuff->GetData();

		UINT       uSize = pTcp->GetDataSize();

		return uSize <= 8;
	}

	return FALSE;
}

void CTcpSocket::DelayedAck(void)
{
	if( !m_fReqACK ) {

		if( m_AckDelay >= ToTicks(10) ) {

			if( !m_fReqSoon ) {

				m_SoonTime = m_AckDelay;

				m_fReqSoon = TRUE;
			}

			return;
		}

		m_fReqACK  = TRUE;

		m_fReqPing = FALSE;
	}
}

void CTcpSocket::InstantAck(void)
{
	m_fReqACK  = TRUE;

	m_fReqSoon = FALSE;

	m_fReqPing = FALSE;
}

void CTcpSocket::RequestPing(void)
{
	m_fReqPing = TRUE;
}

// Segment Queueing

BOOL CTcpSocket::CanQueueSegment(void)
{
	return !m_pTxBuff;
}

void CTcpSocket::QueueSegment(CTcpHeader *pTcp, CBuffer *pBuff)
{
	pTcp->m_LocPort = m_LocPort;

	pTcp->m_RemPort = m_RemPort;

	m_pTxBuff = pBuff;
}

BOOL CTcpSocket::QueueData(PBYTE pData, UINT &uSize)
{
	if( m_pTxBuff ) {

		UINT uTail = m_pTxBuff->GetTailSpace();

		UINT uSend = min(uTail, uSize);

		if( uSend ) {

			CTcpHeader *pTcp  = (CTcpHeader *) m_pTxBuff->GetData();

			UINT        uData = pTcp->GetDataSize();

			MakeMin(uSend, m_SendMSS - uData);

			if( uSend ) {

				memcpy(m_pTxBuff->AddTail(uSend),
				       pData,
				       uSend
				);

				pTcp->SetDataSize(uData + uSend);

				uSize = uSend;

				return TRUE;
			}
		}

		return FALSE;
	}

	if( CanQueueSegment() ) {

		CBuffer *pBuff = BuffAllocate(1460);

		if( pBuff ) {

			CTcpHeader *pTcp = BuffAddHead(pBuff, CTcpHeader);

			pTcp->Init();

			UINT uTail = pBuff->GetTailSpace();

			UINT uSend = min(m_SendMSS, min(uTail, uSize));

			pTcp->SetDataSize(uSend);

			memcpy(pBuff->AddTail(uSend),
			       pData,
			       uSend
			);

			QueueSegment(pTcp, pBuff);

			uSize = uSend;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CTcpSocket::QueueData(CBuffer *pBuff, UINT uSize)
{
	if( !m_pTxBuff ) {

		CTcpHeader *pTcp = BuffAddHead(pBuff, CTcpHeader);

		pTcp->Init();

		pTcp->SetDataSize(uSize);

		QueueSegment(pTcp, pBuff);

		return TRUE;
	}

	return FALSE;
}

// Receive State Machine

BOOL CTcpSocket::TakeSegment(PSREF PS, CTcpHeader *pTcp)
{
	// Most matching already done by OnTest.

	if( m_RemPort ) {

		WORD SendMSS;

		if( pTcp->GetMaxSegSize(SendMSS) ) {

			m_SendMSS = min(SendMSS, m_SendMax);
		}

		return TRUE;
	}
	else {
		if( pTcp->m_FlagSYN ) {

			WORD SendMSS;

			if( TRUE ) {

				m_pTcp->CopyOptions(this, PS.m_Src);
			}

			if( pTcp->GetMaxSegSize(SendMSS) ) {

				m_SendMSS = min(SendMSS, m_SendMax);
			}

			return TRUE;
		}
	}

	return FALSE;
}

void CTcpSocket::RecvInListen(PSREF PS, CTcpHeader *pTcp, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	if( pTcp->m_FlagRST ) {

		return;
	}

	if( pTcp->m_FlagACK ) {

		m_fReqRST = TRUE;

		return;
	}

	if( pTcp->m_FlagSYN ) {

		m_LocIP    = PS.m_Dest;

		m_RemIP	   = PS.m_Src;

		m_RemPort  = pTcp->m_LocPort;

		m_RecvISN  = pTcp->m_Seq;

		m_RecvNxt  = m_RecvISN + 1;

		m_SendISN  = MAKELONG(rand(), rand());

		m_SendUna  = m_SendISN;

		m_SendNxt  = m_SendISN;

		ReadSendWindow(pTcp);

		m_fReqSYN = TRUE;

		m_fReqACK = TRUE;

		SetState(stateSynRcvd);
	}
}

void CTcpSocket::RecvInSynSent(PSREF PS, CTcpHeader *pTcp, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	BOOL fValidAck = FALSE;

	if( pTcp->m_FlagACK ) {

		DWORD Ack = pTcp->m_Ack;

		if( LTE(Ack, m_SendISN) || GT(Ack, m_SendNxt) ) {

			if( !pTcp->m_FlagRST ) {

				m_fReqRST = TRUE;
			}

			return;
		}

		if( LTE(m_SendUna, Ack) && LTE(Ack, m_SendNxt) ) {

			fValidAck = TRUE;
		}
	}

	if( pTcp->m_FlagRST ) {

		if( fValidAck ) {

			SetState(stateError);
		}

		return;
	}

	if( pTcp->m_FlagSYN ) {

		m_LocIP   = PS.m_Dest;

		m_RecvISN = pTcp->m_Seq;

		m_RecvNxt = m_RecvISN + 1;

		if( pTcp->m_FlagACK ) {

			if( GT(pTcp->m_Ack, m_SendUna) ) {

				m_SendUna = pTcp->m_Ack;

				ScanRetryList();
			}
		}

		if( GT(m_SendUna, m_SendISN) ) {

			ReadSendWindow(pTcp);

			SetState(stateEstab);

			m_fActive = TRUE;

			InstantAck();

			return;
		}

		m_fReqSYN = TRUE;

		m_fReqACK = TRUE;

		SetState(stateSynRcvd);
	}
}

void CTcpSocket::RecvInOther(CTcpHeader *pTcp, CBuffer *pBuff)
{
	if( IsPending(pTcp) ) {

		if( !pTcp->m_FlagRST ) {

			DelayedAck();
		}

		BuffRelease(pBuff);

		return;
	}

	if( !IsAcceptable(pTcp) ) {

		if( !pTcp->m_FlagRST ) {

			InstantAck();
		}

		BuffRelease(pBuff);

		return;
	}

	if( GT(pTcp->m_Seq, m_RecvNxt) ) {

		if( pTcp->m_FlagACK ) {

			HandleAck(pTcp, pBuff);
		}

		if( !pTcp->m_FlagRST ) {

			AddPending(pTcp, pBuff);

			return;
		}
	}

	for( ;;) {

		HandleSeg(pTcp, pBuff);

		if( HasPending(pTcp) ) {

			if( pTcp->m_Seq == m_RecvNxt ) {

				GetPending(pBuff);

				Debug(debugRecv, "pend", pTcp);

				continue;
			}
		}

		break;
	}
}

void CTcpSocket::HandleSeg(CTcpHeader *pTcp, CBuffer *pBuff)
{
	BOOL fKeep = FALSE;

	UINT uSize = 0;

	if( pTcp->m_FlagRST ) {

		switch( m_uState ) {

			case stateSynRcvd:

				if( m_fActive )
					SetState(stateError);
				else
					ResetListen();

				break;

			case stateEstab:
			case stateCloseWait:

				SetState(stateError);

				break;

			case stateFinWait1:
			case stateFinWait2:
			case stateClosing:
			case stateLastAck:
			case stateTimeWait:

				SetState(stateClosed);

				break;
		}

		BuffRelease(pBuff);

		return;
	}

	if( pTcp->m_FlagSYN ) {

		switch( m_uState ) {

			case stateSynRcvd:
			case stateEstab:
			case stateFinWait1:
			case stateFinWait2:
			case stateCloseWait:
			case stateClosing:
			case stateLastAck:
			case stateTimeWait:

				m_fReqRST = TRUE;

				SetState(stateClosed);

				break;
		}

		BuffRelease(pBuff);

		return;
	}

	if( !pTcp->m_FlagACK ) {

		BuffRelease(pBuff);

		return;
	}

	if( !HandleAck(pTcp, pBuff) ) {

		BuffRelease(pBuff);

		return;
	}

	if( (uSize = pTcp->GetDataSize()) ) {

		switch( m_uState ) {

			case stateFinWait1:
			case stateFinWait2:

				if( !m_fLinger ) {

					m_RecvNxt = pTcp->m_Seq + pTcp->GetSymbolsNoFin();

					PurgePendingList();

					CalcRecvWindow();

					InstantAck();

					break;
				}

			case stateEstab:

				if( !m_pRxBuff ) {

					if( pBuff->GetSize() + pBuff->GetTailSpace() >= m_RecvWnd ) {

						m_pRxBuff = pBuff;

						fKeep     = TRUE;
					}
					else {
						if( (m_pRxBuff = BuffAllocate(m_RecvWnd)) ) {

							PBYTE pDest = m_pRxBuff->AddTail(uSize);

							PBYTE pSrc  = pBuff->GetData();

							memcpy(pDest, pSrc, uSize);
						}
						else {
							TcpDebug(OBJ_TCP, LEV_TRACE, "rx data dropped\n");

							BuffRelease(pBuff);

							return;
						}
					}
				}
				else {
					if( uSize <= m_pRxBuff->GetTailSpace() ) {

						PBYTE pDest = m_pRxBuff->AddTail(uSize);

						PBYTE pSrc  = pBuff->GetData();

						memcpy(pDest, pSrc, uSize);
					}
					else {
						TcpDebug(OBJ_TCP, LEV_TRACE, "rx buffer full\n");

						BuffRelease(pBuff);

						return;
					}
				}

				m_RecvNxt = pTcp->m_Seq + pTcp->GetSymbolsNoFin();

				PurgePendingList();

				CalcRecvWindow();

				DelayedAck();

				break;
		}
	}

	if( pTcp->m_FlagFIN ) {

		DWORD Seq = pTcp->m_Seq + pTcp->GetSymbols();

		if( LT(m_RecvNxt, Seq) ) {

			m_RecvNxt = Seq;

			InstantAck();
		}

		switch( m_uState ) {

			case stateSynRcvd:
			case stateEstab:

				SetState(stateCloseWait);

				break;

			case stateFinWait1:

				if( m_SendUna == m_SendNxt ) {

					if( m_fLinger && m_pRxBuff ) {

						SetState(stateLinger);
					}
					else {
						StartKillTimer();

						SetState(stateTimeWait);
					}
				}
				else
					SetState(stateClosing);

				break;

			case stateFinWait2:

				if( m_fLinger && m_pRxBuff ) {

					SetState(stateLinger);
				}
				else {
					StartKillTimer();

					SetState(stateTimeWait);
				}

				break;

			case stateTimeWait:

				StartKillTimer();

				break;
		}
	}

	if( !fKeep ) {

		BuffRelease(pBuff);
	}
}

BOOL CTcpSocket::HandleAck(CTcpHeader *pTcp, CBuffer *pBuff)
{
	DWORD Seq = pTcp->m_Seq;

	DWORD Ack = pTcp->m_Ack;

	if( m_uState == stateSynRcvd ) {

		if( LTE(m_SendUna, Ack) && LTE(Ack, m_SendNxt) ) {

			ReadSendWindow(pTcp);

			SetState(stateEstab);

			m_fActive = TRUE;
		}
		else {
			m_fReqRST = TRUE;

			return FALSE;
		}
	}

	BOOL fNewAck = FALSE;

	switch( m_uState ) {

		case stateEstab:
		case stateFinWait1:
		case stateFinWait2:
		case stateCloseWait:
		case stateClosing:
		case stateLastAck:

			if( LTE(m_SendUna, Ack) && LTE(Ack, m_SendNxt) ) {

				if( LT(m_SendUna, Ack) ) {

					m_SendUna = Ack;

					fNewAck   = TRUE;

					ScanRetryList();
				}

				if( LT(m_SendWL1, Seq) || (m_SendWL1 == Seq && LTE(m_SendWL2, Ack)) ) {

					ReadSendWindow(pTcp);
				}
			}
			else {
				if( GT(Ack, m_SendNxt) ) {

					DelayedAck();

					return FALSE;
				}
			}
			break;
	}

	if( fNewAck && m_SendUna == m_SendNxt ) {

		switch( m_uState ) {

			case stateFinWait1:

				SetState(stateFinWait2);

				break;

			case stateClosing:

				StartKillTimer();

				SetState(stateTimeWait);

				break;

			case stateLastAck:

				SetState(stateClosed);

				return FALSE;
		}
	}

	return TRUE;
}

// Window Management

void CTcpSocket::ReadSendWindow(CTcpHeader *pTcp)
{
	m_SendWnd = pTcp->m_Wnd;

	m_SendWL1 = pTcp->m_Seq;

	m_SendWL2 = pTcp->m_Ack;
}

void CTcpSocket::CalcRecvWindow(void)
{
	if( !m_pRxBuff ) {

		if( m_RecvWnd < 1460 ) {

			if( GTE(m_RecvNxt + 730, m_RecvMax) ) {

				switch( m_uState ) {

					case stateEstab:
					case stateFinWait1:
					case stateFinWait2:

						InstantAck();

						break;
				}
			}

			m_RecvWnd = 1460;
		}

		return;
	}

	m_RecvWnd = WORD(min(1460, m_pRxBuff->GetTailSpace()));
}

UINT CTcpSocket::GetSendLimit(void)
{
	return m_SendWnd - (m_SendNxt - m_SendUna);
}

// Pending Segments

BOOL CTcpSocket::IsPending(CTcpHeader *pTcp)
{
	for( UINT i = 0; i < m_uPendCount; i++ ) {

		CBuffer    * pBuff = m_pPendBuff[i];

		INT          nStep = m_nPendStep[i];

		CTcpHeader * pPend = (CTcpHeader *) (pBuff->GetData() + nStep);

		if( pTcp->m_Seq == pPend->m_Seq ) {

			if( pTcp->GetDataSize() == pPend->GetDataSize() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CTcpSocket::AddPending(CTcpHeader *pTcp, CBuffer *pBuff)
{
	if( pTcp->GetDataSize() ) {

		if( m_uPendCount < m_uPendLimit ) {

			UINT i, j;

			for( i = 0; i < m_uPendCount; i++ ) {

				CBuffer    * pBuff = m_pPendBuff[i];

				INT          nStep = m_nPendStep[i];

				CTcpHeader * pPend = (CTcpHeader *) (pBuff->GetData() + nStep);

				if( LTE(pTcp->m_Seq, pPend->m_Seq) ) {

					break;
				}
			}

			for( j = m_uPendCount; j > i; j-- ) {

				m_pPendBuff[j+1] = m_pPendBuff[j];

				m_nPendStep[j+1] = m_nPendStep[j];
			}

			m_pPendBuff[i] = pBuff;

			m_nPendStep[i] = PBYTE(pTcp) - pBuff->GetData();

			m_uPendCount++;

			return TRUE;
		}
	}

	BuffRelease(pBuff);

	return FALSE;
}

BOOL CTcpSocket::HasPending(CTcpHeader * &pTcp)
{
	if( m_uPendCount ) {

		CBuffer    * pBuff = m_pPendBuff[0];

		INT          nStep = m_nPendStep[0];

		CTcpHeader * pPend = (CTcpHeader *) (pBuff->GetData() + nStep);

		pTcp = pPend;

		return TRUE;
	}

	return FALSE;
}

void CTcpSocket::GetPending(CBuffer * &pBuff)
{
	pBuff = m_pPendBuff[0];

	for( UINT i = 1; i < m_uPendCount; i++ ) {

		m_pPendBuff[i-1] = m_pPendBuff[i];

		m_nPendStep[i-1] = m_nPendStep[i];
	}

	m_uPendCount--;
}

void CTcpSocket::PurgePendingList(void)
{
	CTcpHeader *pTcp;

	while( HasPending(pTcp) ) {

		if( LTE(pTcp->m_Seq + pTcp->GetDataSize(), m_RecvNxt) ) {

			CBuffer *pBuff;

			GetPending(pBuff);

			BuffRelease(pBuff);

			continue;
		}

		break;
	}
}

void CTcpSocket::FreePendingList(void)
{
	for( UINT i = 0; i < m_uPendCount; i++ ) {

		BuffRelease(m_pPendBuff[i]);
	}

	m_uPendCount = 0;
}

// Retry Management

void CTcpSocket::LockRetry(void)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	if( !m_uLocked++ ) {

		m_fGotACK = FALSE;
	}
}

void CTcpSocket::FreeRetry(void)
{
	CAutoLock Lock(m_pTcp->m_pLock);

	if( !--m_uLocked ) {

		if( m_fGotACK ) {

			BOOL fFree = FALSE;

			for( ;;) {

				CRetry &Retry = m_Retry[0];

				if( Retry.m_pBuff ) {

					if( Retry.m_fAck ) {

						CBuffer *pBuff = Retry.m_pBuff;

						UINT r;

						for( r = 0; r < m_uRetry - 1; r++ ) {

							m_Retry[r] = m_Retry[r+1];
						}

						CRetry &Retry = m_Retry[r];

						fFree         = TRUE;

						memset(&Retry, 0, sizeof(Retry));

						BuffRelease(pBuff);

						continue;
					}
				}

				break;
			}

			if( fFree ) {

				UpdateSendRequest();
			}
		}
	}
}

void CTcpSocket::InitRetryList(void)
{
	for( UINT r = 0; r < elements(m_Retry); r++ ) {

		CRetry &Retry = m_Retry[r];

		memset(&Retry, 0, sizeof(Retry));
	}
}

void CTcpSocket::FreeRetryList(void)
{
	for( UINT r = 0; r < elements(m_Retry); r++ ) {

		CRetry &Retry = m_Retry[r];

		if( Retry.m_pBuff ) {

			BuffRelease(Retry.m_pBuff);
		}

		memset(&Retry, 0, sizeof(Retry));
	}
}

void CTcpSocket::PollRetryList(UINT uGone)
{
	for( UINT r = 0; r < m_uRetry; r++ ) {

		CRetry &Retry = m_Retry[r];

		if( Retry.m_pBuff && !Retry.m_fAck ) {

			if( Retry.m_Timer ) {

				if( INT(Retry.m_Timer -= uGone) <= 0 ) {

					Retry.m_Timer = 0;
				}
			}
		}
	}
}

void CTcpSocket::ScanRetryList(void)
{
	LockRetry();

	for( UINT r = 0; r < m_uRetry; r++ ) {

		CRetry &Retry = m_Retry[r];

		if( Retry.m_pBuff ) {

			if( LTE(Retry.m_Last, m_SendUna) ) {

				UINT uTime   = Retry.m_Stamp;

				UINT uTrip   = GetTickCount() - uTime;

				Retry.m_fAck = TRUE;

				m_fGotACK    = TRUE;

				CalcTripTime(uTrip);

				continue;
			}
		}

		break;
	}

	FreeRetry();
}

BOOL CTcpSocket::IsRetryNeeded(void)
{
	return m_Retry[0].m_pBuff && m_Retry[0].m_fAck == 0 && m_Retry[0].m_Timer == 0;
}

BOOL CTcpSocket::IsRetryExhausted(void)
{
	return m_Retry[0].m_Count == 0;
}

BOOL CTcpSocket::IsRetryListFull(void)
{
	return m_Retry[m_uRetry-1].m_pBuff ? TRUE : FALSE;
}

void CTcpSocket::AddRetry(CBuffer *pBuff)
{
	for( UINT r = 0; r < m_uRetry; r++ ) {

		CRetry &Retry = m_Retry[r];

		if( !Retry.m_pBuff ) {

			Retry.m_pBuff = pBuff;

			Retry.m_Last  = m_SendNxt;

			Retry.m_Stamp = GetTickCount();

			Retry.m_Timer = m_RetryTime;

			Retry.m_Count = m_RetryCount;

			Retry.m_fAck  = FALSE;

			break;
		}
	}
}

BOOL CTcpSocket::GetRetry(CBuffer * &pBuff)
{
	if( IsRetryNeeded() ) {

		CRetry &Retry = m_Retry[0];

		Retry.m_Timer = m_RetryTime;

		Retry.m_Count = Retry.m_Count - 1;

		pBuff = Retry.m_pBuff;

		return TRUE;
	}

	return FALSE;
}

// Segment Testing

BOOL CTcpSocket::IsAcceptable(CTcpHeader *pTcp)
{
	DWORD Seq = pTcp->m_Seq;

	WORD  Len = WORD(pTcp->GetSymbols());

	if( !Len ) {

		switch( m_uState ) {

			// NOTE -- We are a little more tolerant in these states when we get a RST
			// segment as it is possible that the other end has decided not to accept
			// data after its own Close and that it sends a broken reset with a SEQ
			// equal to our last ACKed sequence number. We assume that all our ACKs
			// except the one for the FIN have got through, so we accept the RST even
			// if it has the same SEQ as the FIN. I am not sure whether we ought to
			// have a wider window in these states in case other ACKs haven't made it.

			case stateCloseWait:
			case stateLastAck:

				if( pTcp->m_FlagRST ) {

					if( pTcp->m_FlagACK == 0 ) {

						if( Seq == m_RecvNxt - 1 ) {

							return TRUE;
						}
					}
				}
				break;
		}

		if( m_RecvWnd ) {

			if( InRecvWindow(Seq) ) {

				return TRUE;
			}
		}
		else {
			if( Seq == m_RecvNxt ) {

				return TRUE;
			}
		}
	}
	else {
		if( m_RecvWnd ) {

			if( InRecvWindow(Seq) ) {

				return TRUE;
			}

			if( InRecvWindow(Seq + Len - 1) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CTcpSocket::InRecvWindow(DWORD Seq)
{
	if( LTE(m_RecvNxt, Seq) ) {

		if( LT(Seq, m_RecvNxt + m_RecvWnd) ) {

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CTcpSocket::IsZeroWindowSYN(WORD Window)
{
	return (Window == 0 && m_uState == stateSynRcvd);
}

// State Text

PCTXT CTcpSocket::GetStateText(UINT uState)
{
	switch( uState ) {

		case stateFree:
			return "FREE";

		case stateInit:
			return "INITIAL";

		case stateClosed:
			return "CLOSED";

		case stateListen:
			return "LISTEN";

		case stateSynSent:
			return "SYN-SENT";

		case stateSynRcvd:
			return "SYN-RCVD";

		case stateEstab:
			return "ESTABLISHED";

		case stateFinWait1:
			return "FIN-WAIT1";

		case stateFinWait2:
			return "FIN-WAIT2";

		case stateClosing:
			return "CLOSING";

		case stateCloseWait:
			return "CLOSE-WAIT";

		case stateLastAck:
			return "LAST-ACK";

		case stateLinger:
			return "LINGER";

		case stateTimeWait:
			return "TIME-WAIT";

		case stateError:
			return "ERROR";
	}

	return "UNKNOWN";
}

// Debugging

void CTcpSocket::Debug(UINT Mask, PCTXT pTag, PCTXT pFormat, ...)
{
	if( m_LocPort == debugPort || (debugControl & Mask) ) {

		TcpDebugLock();

		Debug(pTag);

		va_list pArgs;

		va_start(pArgs, pFormat);

		AfxTraceArgs(pFormat, pArgs);

		va_end(pArgs);

		AfxTrace("\n");

		TcpDebugFree();
	}
}

void CTcpSocket::Debug(UINT Mask, PCTXT pTag, CTcpHeader *pTcp)
{
	if( m_LocPort == debugPort || (debugControl & Mask) ) {

		TcpDebugLock();

		Debug(pTag);

		pTcp->PrintShort();

		TcpDebugFree();
	}
}

void CTcpSocket::Debug(PCTXT pTag)
{
	TcpDebug(OBJ_TCP, LEV_TRACE, "%X : %4.4s : ", m_uIndex, pTag);
}

// End of File
