
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RawProtocol_HPP

#define	INCLUDE_RawProtocol_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RawSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw Protocol
//

class CRaw : public IThreadNotify, public IProtocol
{
	public:
		// Constructor
		CRaw(void);

		// Destructor
		~CRaw(void);

		// Operations
		void Enable(void);
		BOOL Send(CRawSocket *pSock, CBuffer *pBuff);
		void SendReq(BOOL fState);
		void EnableMulticast(IPREF Ip, BOOL fEnable);
		void FreeSocket(CRawSocket *pSock);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IThreadNotify
		UINT METHOD OnThreadCreate(IThread *pThread, UINT uIndex);
		void METHOD OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam);

		// IProtocol
		BOOL Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask);
		BOOL CreateSocket(ISocket * &pSocket);
		BOOL Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff);
		BOOL Send(void);
		BOOL Poll(void);
		BOOL NetStat(IDiagOutput *pOut);
		BOOL SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff);

		// Public Data
		IMutex * m_pLock;

	protected:
		// List Root
		struct CListRoot
		{
			CRawSocket * m_pHead;
			CRawSocket * m_pTail;
			};

		// Data Members
		ULONG		     m_uRefs;
		IRouter	      *	     m_pRouter;
		INetUtilities *      m_pUtils;
		UINT		     m_uMask;
		BOOL		     m_fEnable;
		CRawSocket	     m_Sock[16];
		UINT		     m_uSend;
		CArray<CMacAddr>     m_Nics;
		CMap<CMacAddr, UINT> m_Seen;

		// Implementation
		void BindSockets(void);
		BOOL StoreMacs(UINT uFace, CBuffer *pBuff);
		void FindLocalMacs(void);
		void AddSocket(CRawSocket *pSock);
	};

// End of File

#endif
