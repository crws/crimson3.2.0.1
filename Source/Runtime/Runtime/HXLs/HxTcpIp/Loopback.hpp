
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Loopback_HPP

#define	INCLUDE_Loopback_HPP

//////////////////////////////////////////////////////////////////////////
//
// Loopback Packet Driver
//

class CLoopbackDriver : public IPacketDriver
{
	public:
		// Constructor
		CLoopbackDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPacketDriver
		bool    Bind(IRouter *pRouter);
		bool    Bind(IPacketSink *pSink, UINT uFace);
		CString GetDeviceName(void);
		CString GetStatus(void);
		bool    SetIpAddress(IPREF Ip, IPREF Mask);
		bool	SetIpGateway(IPREF Gate);
		bool    GetMacAddress(MACADDR &Addr);
		bool    GetDhcpOption(BYTE bType, UINT &uValue);
		bool    GetSockOption(UINT uCode, UINT &uValue);
		bool    SetMulticast(IPADDR *pList, UINT uCount);
		bool    Open(void);
		bool    Close(void);
		void    Poll(void);
		bool    Send(IPREF Ip, CBuffer *pBuff);

	protected:
		// Data Members
		ULONG	      m_uRefs;
		IPacketSink * m_pSink;
		UINT          m_uFace;
	};

// End of File

#endif

