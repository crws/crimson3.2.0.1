
#include "Intern.hpp"

#include "ModemHspa2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cellular HSPA Connection v2
//

// Cellular Data

global BOOL g_fCellValid     = FALSE;

global char g_sCellMode[128] = {0};

global char g_sCellIMEI[128] = {0};

global char g_sCellName[128] = {0};

global UINT g_uCellBars      = 0;

// Constructor

CModemCellHspa2::CModemCellHspa2(CPpp *pPpp, CConfigPpp const &Config) : CModemCellGprs(pPpp, Config)
{
	}

// Destructor

CModemCellHspa2::~CModemCellHspa2(void)
{
	g_fCellValid = FALSE;
	}

// Overrideables

BOOL CModemCellHspa2::Init(void)
{
	if( FactoryReset() ) {

		/*if( FALSE ) {

			switch( m_uLocation ) {

				// TODO -- These always error for me!

				case 0:
					ModemCommand("+WMBS=4");
					break;
				
				case 1:
					ModemCommand("+WMBS=5");
					break;
				}
			}*/

		// NOTE -- This has to be static or something goes wrong
		// between the compiler and our relocator, and we end up
		// with invalid text pointers in the array!

		static PCTXT pList[] = {

			"#GPIO=1,0,2",
			"#SLED=2",
			"+ICF=3",
			"+FCLASS=0",
			"+CBST=7,0,0",
			"&D1",
			NULL
			};

		if( ModemList(pList) ) {

			switch( ModemCommand("+CGACT=0,1") ) {

				case codeError:
				case codeNoCarrier:
				case codeOK:

					PCTXT pList[] = {

						m_sCont,
						"+CGQMIN=1,0,0,0,0,0",
						"+CGQREQ=1,0,0,0,0,0",
						NULL
						};

					if( ModemList(pList) ) {

						if( ModemCommand("#CGMI") == codeOK ) {
								
							strcpy(g_sCellName, m_sLine + 6);

							strcat(g_sCellName, " ");
							
							if( ModemCommand("#CGMM") == codeOK ) {
									
								strcat(g_sCellName, m_sLine + 6);
								}
							}
							
						if( ModemCommand("#CGSN") == codeOK || 1 ) {

							strcpy(g_sCellIMEI, m_sLine + 6);
							}

						strcpy(g_sCellMode, "HSPA+");

						g_uCellBars  = 0;

						g_fCellValid = TRUE;

						m_uLast      = GetTickCount();

						return TRUE;
						}
					break;
				}
			}
		}

	return FALSE;
	}

BOOL CModemCellHspa2::Kick(void)
{
	return TRUE;
	}

BOOL CModemCellHspa2::Idle(void)
{
	return CModemCellGprs::Idle();
	}

BOOL CModemCellHspa2::Poll(void)
{
/*	static UINT n = 0;

	if( ++n == 30 ) {

		if( PulseDTR() ) {

			ModemCommand("+CSQ");

			ModemCommand("O");
			}

		n = 0;
		}

*/	return TRUE;
	}

BOOL CModemCellHspa2::Connect(BOOL &fOkay)
{
	if( CheckMessages(FALSE) ) {

		SetStatus("CONNECTING");

		if( ModemCommand("") == codeOK ) {

			switch( ModemCommand("D*99***1#") ) {
				
				case codeConnect:

					SetStatus("CONNECTED");

					m_uSignal = 100;

					Sleep(100);

					return TRUE;

				case codeNoCarrier:

					Sleep(1000);

				case codeBusy:
				case codeNoAnswer:

					SetStatus("IDLE");

					fOkay = TRUE;

					return FALSE;
				}
			}
		}

	return FALSE;
	}

BOOL CModemCellHspa2::HangUp(void)
{
	if( SendAttention() == codeNoCarrier ) {

		ModemCommand("+CGACT=0,1");

		return TRUE;
		}
		
	return FALSE;
	}

// Implementation

BOOL CModemCellHspa2::FactoryReset(void)
{
	for( UINT h = 0; h < 3; h++ ) {

		if( h == 1 ) {

			SendAttention();
			}

		if( h == 2 ) {

			SendLinkTerminate();
			}

		switch( ModemCommand("") ) {

			case codeNone:

				if( h < 2 )
					break;

			case codeOK:
			case codeZero:
			case codeEcho:

				BlindCommand("&F1", 200);

				BlindCommand("E0",  200);

				switch( ModemCommand("") ) {

					case codeOK:
					case codeEcho:

						if( ModemCommand("") == codeOK ) {

							return TRUE;
							}

						return FALSE;
					}

				break;
				}
		}

	return FALSE;
	}

BOOL CModemCellHspa2::PulseDTR(void)
{
	TcpDebug(OBJ_MODEM, LEV_TRACE, "pulse dtr\n");

	m_pPort->SetOutput(0, FALSE);

	Sleep(1000);

	m_pPort->SetOutput(0, TRUE);

	GetReply(1000);

	return TRUE;
	}

UINT CModemCellHspa2::SendAttention(void)
{
	UINT uCode = GetReply(1500);

	if( uCode != codeNoCarrier ) {

		TcpDebug(OBJ_MODEM, LEV_TRACE, "send attention\n");

		m_pData->Write('+', FOREVER);
		m_pData->Write('+', FOREVER);
		m_pData->Write('+', FOREVER);

		uCode = GetReply(1250);
		}
	
	m_pData->ClearRx();

	return uCode;
	}

void CModemCellHspa2::SendLinkTerminate(void)
{
	// This is used when the modem won't respond to our
	// attempts to get it initialized. We assume the link
	// is up and it's ignoring our attention sequence,
	// so we send a canned LCP link terminate sequence.

	TcpDebug(OBJ_MODEM, LEV_TRACE, "send link terminate\n");

	static BYTE bTerm[] = { 0x7E,
				0xFF, 0x7D, 0x23, 0xC0, 0x21, 0x7D, 0x25, 0x7D, 0x21, 0x7D, 0x20, 0x7D, 0x24,
				0x3D, 0xC7, 0x7E
				};

	Sleep(1000);

	m_pData->Write(0x11, FOREVER);

	m_pData->Write(bTerm, sizeof(bTerm), FOREVER);

	GetReply(1250);
	}

// End of File
