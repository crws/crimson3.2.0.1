
#include "Intern.hpp"

#include "UdpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "PseudoHeader.hpp"

////////////////////////////////////////////////////////////////////////
//
// UDP Header Wrapper Class
//

// Attributes

BOOL CUdpHeader::TestChecksum(PSREF Ps)
{
	BOOL fOkay;

	switch( m_Checksum ) {

		case 0x0000:
			
			fOkay = TRUE;

			break;

		case 0xFFFF:

			fOkay = !CalcChecksum(Ps.GetChecksum());

			if( !fOkay ) {
				
				m_Checksum = 0x0000;

				fOkay = !CalcChecksum(Ps.GetChecksum());

				m_Checksum = 0xFFFF;
				}

			break;

		default:

			fOkay = !CalcChecksum(Ps.GetChecksum());

			break;
		}

	return fOkay;
	}

// Operations

void CUdpHeader::AddChecksum(DWORD Load)
{
	m_Checksum = 0x0000;

	if( (m_Checksum = CalcChecksum(Load)) ) {

		return;
		}

	m_Checksum = 0xFFFF;
	}

// Implementation

WORD CUdpHeader::CalcChecksum(DWORD Load) const
{
	UINT uSize = ::NetToHost(m_Length);

	return Checksum(PBYTE(this), uSize, Load);
	}

// End of File
