
#include "Intern.hpp"

#include "ModemGprs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cellular GPRS Connection
//

// Constructor

CModemCellGprs::CModemCellGprs(CPpp *pPpp, CConfigPpp const &Config) : CModemCell(pPpp, Config)
{
	SPrintf( m_sCont,
		 "+CGDCONT=1,\"IP\",\"%s\",\"0.0.0.0\",0,0",
		 Config.m_sDial
		 );
	}

// Overridables

BOOL CModemCellGprs::Init(void)
{
	if( CModemCell::Init() ) {

		switch( ModemCommand("+CGACT=0,1") ) {

			case codeError:
			case codeNoCarrier:
			case codeOK:

				PCTXT pList[] = {

					m_sCont,
					"+CGQMIN=1,0,0,0,0,0",
					"+CGQREQ=1,0,0,0,0,0",
					NULL
					};

				if( ModemList(pList) ) {

					m_uLast = GetTickCount();

					return TRUE;
					}
				break;
			}
		}

	return FALSE;
	}

BOOL CModemCellGprs::Idle(void)
{
	UINT uTime = GetTickCount();

	UINT uGone = uTime - m_uLast;

	if( uGone >= ToTicks(5000) ) {

		if( !IsModemReady() ) {

			return FALSE;
			}

		CheckMessages(TRUE);

		m_uLast = uTime;
		}

	if( !CheckMessages(FALSE) ) {

		return FALSE;
		}

	CheckSignal();

	return TRUE;
	}

BOOL CModemCellGprs::Connect(BOOL &fOkay)
{
	if( !CheckMessages(FALSE) ) {

		return FALSE;
		}
	else {
		SetStatus("CONNECTING");

		if( ModemCommand("") == codeOK ) {

			char sDial[128];

			SPrintf(sDial, "D*99***1#");

			if( ModemCommand(sDial) == codeConnect ) {

				SetStatus("CONNECTED");

				m_uSignal = 100;

				Sleep(1000);

				return TRUE;
				}

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CModemCellGprs::HangUp(void)
{
	if( CModem::HangUp() ) {

		switch( ModemCommand("+CGACT=0,1") ) {
			
			case codeNoCarrier:

				return TRUE;
	
			case codeOK:

				return TRUE;
			}
		}
		
	return FALSE;
	}

// End of File
