
#include "Intern.hpp"

#include "Lcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ppp.hpp"

#include "Hdlc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PPP Link Control Protocol
//

// Constructor

CLcp::CLcp(CPpp *pPpp, CConfigPpp const &Config) : CPppNegotiate(pPpp, PROT_LCP)
{
	m_fServer = (Config.m_uMode == pppServer);

	m_fCHAP   = Config.m_fCHAP;

	m_uObj    = OBJ_LCP;
	}

// Destructor

CLcp::~CLcp(void)
{
	}

// Attributes

UINT CLcp::GetCharMap(void) const
{
	return m_uRemMap;
	}

WORD CLcp::GetAuthProtocol(void) const
{
	return m_fServer ? m_wLocAuth : m_wRemAuth;
	}

BOOL CLcp::GetProtComp(void) const
{
	return m_fRemProtComp;
	}

BOOL CLcp::GetAddrComp(void) const
{
	return m_fRemAddrComp;
	}

// Operations

void CLcp::ProtocolReject(WORD wProtocol, CBuffer *pBuff)
{
	TcpDebug(OBJ_LCP, LEV_TRACE, "rejecting protocol %4.4X\n", wProtocol);

	UINT     uSize = pBuff->GetSize();

	CBuffer *pSend = BuffAllocate(2 + uSize);

	*PWORD(pSend->AddTail(2)) = HostToNet(wProtocol);

	memcpy(pSend->AddTail(uSize), pBuff->GetData(), uSize);

	PutFrame(pSend, codeProtocolReject, m_bID++);
	}

// Remote Options

BOOL CLcp::RemoteReject(BYTE bType, PBYTE pData)
{
	switch( bType ) {

		case optMaximumReceive:
		case optAsyncCharMap:

			return FALSE;

		case optProtComp:
		case optAddrComp:

			return FALSE;

		case optMagicNumber:

			BEGIN {
				OPTMAGIC *opt = (OPTMAGIC *) pData;

				if( opt->m_dwMagic == m_dwMagic ) {

					TcpDebug(m_uObj, LEV_WARN, "loopback detected\n");

					opt->m_bSize = 0;

					return TRUE;
					}
				} END;

			return FALSE;

		case optAuthProtocol:

			return m_fServer;
		}

	return TRUE;
	}

BOOL CLcp::RemoteNak(BYTE bType, PBYTE pData)
{
	switch( bType ) {

		case optMaximumReceive:
		case optAsyncCharMap:

			return FALSE;

		case optProtComp:
		case optAddrComp:

			return FALSE;

		case optMagicNumber:
			
			return FALSE;

		case optAuthProtocol:

			if( !m_fServer ) {
				
				OPTCHAP *opt = (OPTCHAP *) pData;

				if( NetToHost(opt->m_wProtocol) == PROT_PAP ) {

					return FALSE;
					}

				if( NetToHost(opt->m_wProtocol) == PROT_CHAP ) {

					if( m_fCHAP ) {
						
						if( opt->m_bMethod == 5 ) {

							return FALSE;
							}
						}
					}

				opt->m_bSize     = sizeof(OPTAUTH);

				opt->m_wProtocol = HostToNet(PROT_PAP);

				return TRUE;
				}

			return TRUE;
		}

	return TRUE;
	}

void CLcp::RemoteDefault(void)
{
	m_wRemAuth     = 0;

	m_wRemMRU      = 1500;

	m_fRemProtComp = FALSE;

	m_fRemAddrComp = FALSE;
	}

void CLcp::RemoteAgreed(PCBYTE pData, UINT uSize)
{
	for( UINT n = 0; n < uSize; ) {

		BYTE bType = pData[n+0];

		BYTE bSize = pData[n+1];

		switch( bType ) {

			case optAsyncCharMap:

				BEGIN
				{
					OPTMAP *opt = (OPTMAP *) (pData+n);

					m_uRemMap = NetToHost(opt->m_dwMap);
					
					} END;
				break;

			case optMaximumReceive:

				BEGIN {
					OPTMRU *opt = (OPTMRU *) (pData+n);

					m_wRemMRU = NetToHost(opt->m_wMRU);

					} END;
				break;

			case optAuthProtocol:

				BEGIN {
					OPTAUTH *opt = (OPTAUTH *) (pData+n);

					m_wRemAuth = NetToHost(opt->m_wProtocol);

					} END;
				break;
			
			case optProtComp:

				m_fRemProtComp = TRUE;
				
				break;
			
			case optAddrComp:

				m_fRemAddrComp = TRUE;

				break;
			}

		n += bSize;
		}

	TcpDebug(m_uObj, LEV_TRACE, "remote agreed\n");

	ShowRemote();
	}

// Local Options

void CLcp::LocalDefault(void)
{
	m_wLocAuth     = m_fCHAP ? PROT_CHAP : PROT_PAP;

	m_fLocProtComp = TRUE;

	m_fLocAddrComp = TRUE;

	m_dwMagic      = HostToNet(DWORD(MAKELONG(rand(), rand())));
	}

void CLcp::LocalRequest(CBuffer *pBuff)
{
	if( m_dwMagic ) {

		AddOptMagicNumber(pBuff);
		}

	if( m_fServer ) {

		AddOptAuthProtocol(pBuff);
		}

	if( m_fLocProtComp ) {

		AddOptProtComp(pBuff);
		}

	if( m_fLocAddrComp ) {

		AddOptAddrComp(pBuff);
		}

	AddOptCharMap(pBuff);
	}

BOOL CLcp::LocalReject(BYTE bType, PBYTE pData)
{
	switch( bType ) {

		case optProtComp:

			m_fLocProtComp = FALSE;

			return TRUE;

		case optAddrComp:

			m_fLocAddrComp = FALSE;

			return TRUE;

		case optMagicNumber:

			m_dwMagic = 0;

			return TRUE;
		}

	return FALSE;
	}

BOOL CLcp::LocalNak(BYTE bType, PBYTE pData)
{
	switch( bType ) {

		case optAuthProtocol:

			if( m_fServer ) {

				if( m_fCHAP ) {

					OPTCHAP *opt = (OPTCHAP *) pData;

					if( opt->m_wProtocol == NetToHost(PROT_CHAP) ) {

						if( opt->m_bMethod == 5 ) {

							return TRUE;
							}

						// NOTE: If someone asks for CHAP with a method
						// other than the default, offer them PAP instead.

						m_wLocAuth = PROT_PAP;

						return TRUE;
						}

					if( opt->m_wProtocol == NetToHost(PROT_PAP) ) {

						m_wLocAuth = PROT_PAP;

						return TRUE;
						}
					}
				}

			return FALSE;

		case optMagicNumber:

			m_dwMagic = 0;

			return TRUE;
		}

	return FALSE;
	}

void CLcp::LocalAgreed(void)
{
	TcpDebug(m_uObj, LEV_TRACE, "local agreed\n");

	ShowLocal();
	}

// Option Building

void CLcp::AddOptMagicNumber(CBuffer *pBuff)
{
	OPTMAGIC opt;
	
	opt.m_bType   = optMagicNumber;

	opt.m_bSize   = sizeof(opt);
	
	opt.m_dwMagic = m_dwMagic;

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

void CLcp::AddOptCharMap(CBuffer *pBuff)
{
	OPTMAP opt;
	
	opt.m_bType = optAsyncCharMap;

	opt.m_bSize = sizeof(opt);
	
	opt.m_dwMap = HostToNet(DWORD((1<<CR) | (1<<XON) | (1<<XOFF)));

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

void CLcp::AddOptAuthProtocol(CBuffer *pBuff)
{
	if( m_wLocAuth == PROT_CHAP ) {

		OPTCHAP opt;

		opt.m_bType      = optAuthProtocol;

		opt.m_bSize      = sizeof(opt);
		
		opt.m_wProtocol  = HostToNet(m_wLocAuth);

		opt.m_bMethod    = 5;

		memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
		}
	
	if( m_wLocAuth == PROT_PAP ) {

		OPTAUTH opt;
		
		opt.m_bType     = optAuthProtocol;

		opt.m_bSize     = sizeof(opt);
		
		opt.m_wProtocol = HostToNet(m_wLocAuth);

		memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
		}
	}

void CLcp::AddOptProtComp(CBuffer *pBuff)
{
	OPTBASIC opt;
	
	opt.m_bType = optProtComp;

	opt.m_bSize = sizeof(opt);

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

void CLcp::AddOptAddrComp(CBuffer *pBuff)
{
	OPTBASIC opt;
	
	opt.m_bType = optAddrComp;

	opt.m_bSize = sizeof(opt);

	memcpy(pBuff->AddTail(sizeof(opt)), &opt, sizeof(opt));
	}

// Debugging

BOOL CLcp::ShowRemote(void)
{
	if( !m_fServer ) {

		TcpDebug(m_uObj, LEV_TRACE, "auth     is %4.4X\n", m_wRemAuth);
		}

	TcpDebug(m_uObj, LEV_TRACE, "mru      is %u\n",    m_wRemMRU);
	TcpDebug(m_uObj, LEV_TRACE, "map      is %8.8X\n", m_uRemMap);
	TcpDebug(m_uObj, LEV_TRACE, "protcomp is %u\n",    m_fRemProtComp);
	TcpDebug(m_uObj, LEV_TRACE, "addrcomp is %u\n",    m_fRemAddrComp);

	return TRUE;
	}

BOOL CLcp::ShowLocal(void)
{
	if( m_fServer ) {

		TcpDebug(m_uObj, LEV_TRACE, "auth     is %4.4X\n", m_wLocAuth);
		}

	TcpDebug(m_uObj, LEV_TRACE, "protcomp is %u\n", m_fLocProtComp);
	TcpDebug(m_uObj, LEV_TRACE, "addrcomp is %u\n", m_fLocAddrComp);

	return TRUE;
	}

// End of File
