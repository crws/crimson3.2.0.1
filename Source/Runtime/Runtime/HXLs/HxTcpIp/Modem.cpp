
#include "Intern.hpp"

#include "Modem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ppp.hpp"

#include "Hdlc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PPP Modem Object
//

// Constant

#define timeDelay 20

// Reply Table

CModem::CReply const CModem::m_Reply[] = {

	{	"0\n",			codeZero		},
	{	"AT*",			codeEcho		},
	{	"BUSY\n",		codeBusy		},
	{	"CLIENT",		codeClient		},
	{	"CONNECT*",		codeConnect		},
	{	"ERROR\n",		codeError		},
	{	"NO ANSWER\n",		codeNoAnswer		},
	{	"NO CARRIER\n",		codeNoCarrier		},
	{	"NO DIAL TONE\n",	codeNoDialTone		},
	{	"OK\n",			codeOK			},
	{	"RING*",		codeRing		},
	{	"SERVER",		codeServer		},
	{	"+*",			codeExtended		},
	{	">",			codePrompt		},

	};

// Constructor

CModem::CModem(CPpp *pPpp, CConfigPpp const &Config) : CPppLayer(pPpp, PROT_MODEM)
{
	m_Config      = Config;

	m_fServer     = (Config.m_uMode == pppServer);

	m_fDirect     = FALSE;

	m_uObj        = OBJ_MODEM;

	m_pPort       = NULL;

	m_fExpand     = FALSE;

	m_uReqSession = 0;

	m_uActSession = 0;

	m_uSeqSession = 1;

	m_fStopReq    = FALSE;

	strcpy(m_sInit, Config.m_sInit);

	ModemLog(NULL, NULL);

	SetStatus("CLOSED");
	}

// Destructor

CModem::~CModem(void)
{
	}

// Management

void CModem::Open(void)
{
	if( !m_fStopReq ) {

		TcpDebug(OBJ_MODEM, LEV_TRACE, "request open\n");

		m_uSeqSession = m_uSeqSession % 10000 + 1;

		m_uReqSession = m_uSeqSession;
		}
	}

void CModem::Close(void)
{
	TcpDebug(OBJ_MODEM, LEV_TRACE, "request close\n");

	m_uReqSession = 0;
	}

void CModem::OnDropped(void)
{
	m_uActSession = 0;
	}

void CModem::OnTime(void)
{
	}

// Attributes

BOOL CModem::IsDirect(void) const
{
	return m_fDirect;
	}

PCTXT CModem::GetStatus(void) const
{
	return m_sStatus;
	}

// Operations

void CModem::Bind(CHdlc *pHdlc)
{
	m_pHdlc= pHdlc;
	}

void CModem::Service(void)
{
	while( !m_fStopReq ) {

		if( OpenPort() ) {

			UINT uLast = GetTickCount();

			UINT uTime = 0;

			BOOL fInit = TRUE;

			SetStatus("INIT");

			ModemInit();

			for(;;) {

				if( m_uActSession == m_uReqSession ) {

					if( m_uActSession ) {

						for( UINT n = 0; n < 256; n++ ) {

							UINT uData = m_pData->Read(50);

							if( uData < NOTHING ) {

								m_pHdlc->OnRecvByte(BYTE(uData));

								m_uRxCount += 1;
								}
							else
								break;
							}
						}
					else {
						if( m_fStopReq ) {

							break;
							}
						
						if( !Idle() ) {

							SetStatus("INIT");

							ModemInit();

							fInit = TRUE;
							}
						}
					}
				else {
					if( m_uReqSession ) {

						BOOL fOkay = FALSE;

						if( !fInit ) {

							SetStatus("INIT");

							ModemInit();

							fInit = TRUE;
							}

						if( !NewSession(fOkay) ) {
						
							if( !fOkay ) {

								fInit = FALSE;
								}
							}
						}
					else
						EndSession();
					}

				if( (uTime = GetTickCount()) - uLast >= ToTicks(1000) ) {

					m_pPpp->OnTime();

					if( m_uActSession ) {

						if( m_uActSession == m_uReqSession ) {

							if( !strcmp(m_pPpp->GetStatus(), "UP") ) {
							
								if( !Poll() ) {

									// TODO -- Do what?
									}
								}
							}
						}

					uLast = uTime;
					}
				}

			ClosePort();
			}
		else {
			TcpDebug(OBJ_MODEM, LEV_TRACE, "wait for port\n");

			Sleep(1000);
			}
		}
	}

void CModem::StopTask(void)
{
	m_fStopReq = TRUE;
	}

BOOL CModem::PutData(PCBYTE pData, UINT uSize)
{
	if( m_uActSession && m_uActSession == m_uReqSession ) {

		UINT uTime = NOTHING;

		m_pData->Write(NULL, 0, uTime);		

		m_pData->Write(pData, uSize, uTime);

		m_uTxCount += uSize;

		return TRUE;
		}

	return FALSE;
	}

// Overridables

BOOL CModem::Init(void)
{
	return TRUE;
	}

BOOL CModem::Kick(void)
{
	ModemAttention();

	BlindCommand("H0");

	return TRUE;
	}

BOOL CModem::Idle(void)
{
	SetStatus("IDLE");

	Sleep(50);

	return TRUE;
	}

BOOL CModem::Poll(void)
{
	return TRUE;
	}

BOOL CModem::Connect(BOOL &fOkay)
{
	return FALSE;
	}

BOOL CModem::HangUp(void)
{
	for( UINT n = 0; n < 5; n++ ) {

		ModemAttention();

		if( ModemCommand("H0") == codeOK ) {

			break;
			}
		}
		
	return TRUE;
	}

// Session Management

BOOL CModem::NewSession(BOOL &fOkay)
{
	TcpDebug(OBJ_MODEM, LEV_TRACE, "start session\n");

	ThisLayerStarted();

	m_uNewSession = m_uReqSession;

	if( Connect(fOkay) ) {

		TcpDebug(OBJ_MODEM, LEV_TRACE, "connected\n");

		m_uTxCount    = 0;

		m_uRxCount    = 0;

		m_uActSession = m_uNewSession;

		ThisLayerUp();

		SetStatus("CONNECTED");

		return TRUE;
		}

	TcpDebug(OBJ_MODEM, LEV_TRACE, "connect failed\n");

	m_uActSession = 0;

	m_uReqSession = 0;

	ThisLayerFinished();

	return FALSE;
	}

void CModem::EndSession(void)
{
	m_uActSession = 0;

	TcpDebug(OBJ_MODEM, LEV_TRACE, "hanging up\n");

	SetStatus("HANGING UP");

	HangUp();

	TcpDebug(OBJ_MODEM, LEV_TRACE, "disconnected\n");
	
	char t[64], r[64];

	SPrintf(t, "session tx count was %u bytes", m_uTxCount);

	SPrintf(r, "session rx count was %u bytes", m_uRxCount);

	ModemLog("info", t);

	ModemLog("info", r);

	TcpDebug(OBJ_MODEM, LEV_TRACE, "%s\n", t);

	TcpDebug(OBJ_MODEM, LEV_TRACE, "%s\n", r);

	ThisLayerDown();
	}

BOOL CModem::EndConnect(void)
{
	return m_fStopReq || m_uReqSession != m_uNewSession;
	}

// Modem Interface

void CModem::ModemInit(void)
{
	for(;;) {

		if( Init() ) {
			
			if( ExtraInit() ) {

				break;
				}
			}

		if( m_fStopReq ) {

			break;
			}

		if( m_uReqSession ) {

			TcpDebug(OBJ_MODEM, LEV_TRACE, "not ready\n");
				
			ThisLayerStarted();

			ThisLayerFinished();
			}

		Kick();
		}
	}

BOOL CModem::BasicInit(void)
{		
/*	if( m_InitMask & 0x80 ) {

		BlindCommand("&F");
		}

*/	BlindCommand("E0");

	if( ModemCommand("H0Q0V1") == codeOK ) {

		if( ModemCommand("L1M1X3") == codeOK ) {

			/*if( m_uLocation == 0 ) {
			
				ModemCommand("+WMBS=4");
				}
			else {
				if( m_uLocation == 1 ) {
			
					ModemCommand("+WMBS=5");
					}
				}*/

			PCTXT pList[] = { 
				
				"&C1",
				"&D0",
				"&H0",
				"&I0",
				"&R1",
				"&S0",
				"&K0",				
				NULL
				};
			
			if( ModemCommand(pList, NOTHING/*m_InitMask*/) == codeOK ) {

				if( ModemCommand("S0=0") == codeOK ) {
					
					if( ModemCommand("S2=43S12=25") == codeOK ) {
					
						return TRUE;
						}
					
					if( ModemCommand("S2=43S12=25") == codeError ) {
					
						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CModem::ExtraInit(void)
{
	if( m_sInit[0] ) {

		return ModemCommand(m_sInit) == codeOK;
		}

	return TRUE;
	}

BOOL CModem::ModemList(PCTXT *pList)
{
	while( *pList ) {

		if( ModemCommand(*pList) == codeOK ) {

			pList++;

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CModem::ModemList(PCTXT *pList, DWORD dwMask)
{	
	UINT n;

	for( n = 0; pList[n]; n++ );

	DWORD dwTest = 1 << (n - 1);		

	while( dwTest ) {		

		if( dwMask & dwTest ) {						

			if( !(ModemCommand(*pList) == codeOK)) {								
			
				return FALSE;
				}
		
			}
		pList++;

		dwTest >>= 1;	

		}

	return TRUE;
	}

UINT CModem::ModemCommand(PCTXT pText)
{
	UINT uTimeout;
	
	WriteCommand(pText);

	switch( pText[0] ) {
	
		case 0:
			uTimeout = 500;
			break;

		case 'O':
			uTimeout = 1000;
			break;

		case 'A':
			uTimeout = 60000;
			break;
			
		case 'D':
			uTimeout = 60000;
			break;
			
		case '+':
			uTimeout = 10000;
			break;
			
		case '*':
			uTimeout = 10000;
			break;

		default:
			uTimeout = 2000;
			break;
		}	

	return GetReply(uTimeout);
	}

UINT CModem::ModemCommand(PCTXT *pList, DWORD dwMask)
{
	if( dwMask ) {

		UINT n;

		for( n = 0; pList[n]; n++ );

		if( n ) {

			char sWork[128];

			PCHAR p1     = sWork;

			DWORD dwTest = 1 << (n - 1);

			while( dwTest ) {

				if( dwMask & dwTest ) {

					strcpy(p1, *pList);

					p1 += strlen(*pList);
					}

				pList++;

				dwTest >>= 1;
				}

			return ModemCommand(sWork);
			}
		}

	return codeOK;
	}

BOOL CModem::IsModemReady(void)
{
	return ModemCommand("") == codeOK;
	}

void CModem::ModemAttention(void)
{
	TcpDebug(OBJ_MODEM, LEV_TRACE, "send attention\n");

	ModemLog("send", "<attention>");

	Sleep(1250);

	m_pData->Write('+', FOREVER);
	m_pData->Write('+', FOREVER);
	m_pData->Write('+', FOREVER);

	GetReply(1250);
	
	m_pData->ClearRx();
	}

void CModem::BlindCommand(PCTXT pText)
{
	WriteCommand(pText);

	Sleep(1000);
	
	m_pData->ClearRx();
	}

void CModem::BlindCommand(PCTXT pText, UINT uSleep)
{
	WriteCommand(pText);

	Sleep(uSleep);
	
	m_pData->ClearRx();
	}

void CModem::WriteCommand(PCTXT pText)
{
	if( m_uReply ) {

		UINT uWrite = GetTickCount();
		
		if( uWrite - m_uReply < ToTicks(timeDelay) ) {

			Sleep(timeDelay - ToTime(uWrite - m_uReply));
			}
		}

	TcpDebug(OBJ_MODEM, LEV_TRACE, "send [%s]\n", pText);

	char s[128] = { 0 };

	strcat(s, "AT");

	strcat(s, pText);

	strcat(s, "\r");

	m_pData->Write(PCBYTE(s), strlen(s), FOREVER);

	ModemLog("send", *pText ? pText : "<null command>");
	}

void CModem::DumpConfig(void)
{	
	WriteCommand("#CGSN");

	SetTimer(1000);

	for(;;) {

		UINT uData;

		if( (uData = m_pData->Read(GetTimer())) == NOTHING ) {

			break;
			}

		AfxTrace("%c", uData);

		SetTimer(200);
		}
	}

// Reply Parser

UINT CModem::GetReply(UINT uTimeout)
{
	SetTimer(uTimeout);

	BOOL fLine  = FALSE;

	UINT uLine  = 0;

	UINT uFirst = 0;

	UINT uLast  = elements(m_Reply) - 1;

	UINT uPos   = 0;

	for(;;) {

		UINT uData;

		if( (uData = m_pData->Read(GetTimer())) == NOTHING ) {

			break;
			}

		/*AfxTrace(isprint(uData) ? "['%c']" : "[%2.2X]", uData);*/

		if( fLine ) {

			if( uData == '\r' ) {

				fLine = FALSE;
				}
			else {
				if( uLine < elements(m_sLine) - 1 ) {

					m_sLine[uLine++] = char(uData);

					m_sLine[uLine  ] = 0;
					}
				}
			}
		else {
			if( uData == '#' ) {

				fLine = TRUE;

				continue;
				}
			}

		for( UINT uLoop = 0; uLoop < 2; ) {

			if( uFirst > uLast ) {

				uFirst = 0;

				uLast  = elements(m_Reply) - 1;

				uPos   = 0;

				uLoop  = uLoop + 1;

				continue;
				}

			if( MatchReply(uData, uFirst, uPos) ) {				

				while( uLast > uFirst ) {					

					if( !MatchReply(uData, uLast, uPos) ) {

						uLast--;
						}
					else
						break;
					}

				if( !StepToNext(uData, uFirst, uPos) ) {

					char sCode[64]   = { 0 };				
					
					UINT uCode       = m_Reply[uFirst].m_uCode;					

					m_sRest[m_uRest] = 0;

					m_uReply         = GetTickCount();

					/*AfxTrace("\n");*/

					if( !m_uRest ) {

						TcpDebug( OBJ_MODEM,
							  LEV_TRACE,
							  "reply is %u\n",
							  uCode
							  );

						SPrintf(sCode, "%u", uCode);
						}
					else {
						TcpDebug( OBJ_MODEM,
							  LEV_TRACE,
							  "reply is %u %s\n",
							  uCode,
							  m_sRest
							  );

						SPrintf(sCode, "%u %s", uCode, m_sRest);
						}

					/*if( TRUE ) {

						AfxTrace("Extra: ");

						SetTimer(250);

						for(;;) {

							UINT uData;

							if( (uData = m_pData->Read(GetTimer())) == NOTHING ) {

								break;
								}

							AfxTrace(isprint(uData) ? "['%c']" : "[%2.2X]", uData);

							SetTimer(250);
							}

						AfxTrace("\n\n");
						}*/

					ModemLog("recv", sCode);					
						
					return uCode;  					
					}

				break;
				}

			uFirst++;
			}
		}

	m_uReply = 0;

	TcpDebug(OBJ_MODEM, LEV_TRACE, "no reply\n");

	ModemLog("recv", "<no reply>");

	return codeNone;
	}

BOOL CModem::MatchReply(UINT uData, UINT uReply, UINT uPos)
{
	UINT uTemp = m_Reply[uReply].m_pText[uPos];

	switch( uTemp ) {

		case '*':
			
			if( m_uRest < sizeof(m_sRest) ) {

				if( uData > 32 || (uData == 32 && m_uRest) ) {
			
					m_sRest[m_uRest++] = char(uData);
					}

				return TRUE;
				}
			
			return FALSE;

		case '\n':

			m_uRest = 0;

			return uData == '\n' || uData == '\r';

		default:
			
			m_uRest = 0;

			return uData == uTemp;
		}

	return TRUE;
	}

BOOL CModem::StepToNext(UINT uData, UINT uReply, UINT &uPos)
{
	UINT uTemp = m_Reply[uReply].m_pText[uPos];	

	switch( uTemp ) {

		case '\n':

			return uData != '\n';

		case '*':
			
			return uData != '\n';
		}	

	if( m_Reply[uReply].m_pText[++uPos] ) {

		return TRUE;
		}

	return FALSE;
	}

// Serial Support

BOOL CModem::OpenPort(void)
{
	AfxGetObject(m_Config.m_DevName, m_Config.m_DevInst, IPortObject, m_pPort);

	if( m_pPort ) {

		AfxNewObject("data-d", IDataHandler, m_pData);
		
		m_pPort->Bind(m_pData);

		CSerialConfig Config;

		memset(&Config, 0, sizeof(Config));

		Config.m_uPhysical = physicalRS232;
		Config.m_uBaudRate = 115200;
		Config.m_uDataBits = 8;
		Config.m_uStopBits = 1;
		Config.m_uParity   = 0;
		Config.m_uFlags    = flagNoBatchSend;

		m_pData->SetTxSize(3500);

		m_pData->SetRxSize(2000);

		if( m_pPort->Open(Config) ) {

			m_pPort->SetOutput(0, TRUE);

			return TRUE;
			}

		m_pData->Release();
		}

	return FALSE;
	}

void CModem::ClosePort(void)
{
	if( m_pPort ) {

		m_pPort->Close();

		m_pPort->Release();

		m_pPort = NULL;

		m_pData->Release();
		}
	}

// Status

void CModem::SetStatus(PCTXT pStatus)
{
	strcpy(m_sStatus, pStatus);
	}

// Logging

BOOL CModem::ModemLog(PCTXT p1, PCTXT p2)
{
/*	if( m_fLogFile ) {

		::ModemLog(WORD(m_uPort >> 8), p1, p2);

		return TRUE;
		}

*/	return FALSE;
	}

// End of File
