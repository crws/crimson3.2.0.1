
#include "Intern.hpp"

#include "Ethernet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ArpFrame.hpp"

#include "IpHeader.hpp"

#include "TcpHeader.hpp"

#include "MacHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Packet Driver
//

// Constants

static UINT const arpDelay   = 80;

static UINT const arpCount   = 2;

static UINT const macMinSize = 50;

// Instantiator

IPacketDriver * Create_Ethernet(CConfigEth const &Config)
{
	return New CEthernetDriver(Config);
}

// Constructor

CEthernetDriver::CEthernetDriver(CConfigEth const &Config)
{
	StdSetRef();

	m_Config    = Config;

	m_pNic      = NULL;

	m_pLock     = Create_Rutex();

	m_pArp      = NULL;

	m_pDhcp     = NULL;

	m_fDhcp     = false;

	m_pRouter   = NULL;

	m_pSink     = NULL;

	m_uFace	    = NOTHING;

	m_hThread   = NULL;

	m_fActive   = false;

	m_Local     = CIpAddr(169, 254, 0, 1);

	m_pPendHead = NULL;

	m_pPendTail = NULL;

	m_fCapture  = false;

	m_pDiag     = NULL;

	m_uProv     = 0;

	AfxGetObject(m_Config.m_DevName, m_Config.m_DevInst, INic, m_pNic);

	SetStatus("CLOSED");
}

// Destructor

CEthernetDriver::~CEthernetDriver(void)
{
	DiagRevoke();

	piob->RevokeSingleton("net.pcap", m_uFace);

	AfxRelease(m_pNic);

	AfxRelease(m_pLock);

	delete m_pArp;

	delete m_pDhcp;
}

// IUnknown

HRESULT CEthernetDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPacketDriver);

	StdQueryInterface(IPacketDriver);

	StdQueryInterface(IPacketCapture);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CEthernetDriver::AddRef(void)
{
	StdAddRef();
}

ULONG CEthernetDriver::Release(void)
{
	StdRelease();
}

// IPacket

bool CEthernetDriver::Bind(IRouter *pRouter)
{
	m_pRouter = pRouter;

	return true;
}

bool CEthernetDriver::Bind(IPacketSink *pSink, UINT uFace)
{
	m_pSink = pSink;

	m_uFace = uFace;

	piob->RegisterSingleton("net.pcap", uFace, (IPacketCapture *) this);

	return true;
}

CString CEthernetDriver::GetDeviceName(void)
{
	return m_Config.m_Display;
}

CString CEthernetDriver::GetStatus(void)
{
	return m_Stat;
}

bool CEthernetDriver::SetIpAddress(IPREF Ip, IPREF Mask)
{
	if( m_fActive ) {

		if( !Ip.IsEmpty() ) {

			if( m_Config.m_Ip != Ip ) {

				for( UINT n = 0; n < 8; n++ ) {

					if( Send(Ip, NULL) ) {

						return false;
					}

					Sleep(10+10*(rand()%5));
				}
			}
		}
	}

	m_Config.m_Ip   = Ip;

	m_Config.m_Mask = Mask;

	return true;
}

bool CEthernetDriver::SetIpGateway(IPREF Gate)
{
	m_Config.m_Gate = Gate;

	return true;
}

bool CEthernetDriver::GetMacAddress(MACADDR &Addr)
{
	if( m_pNic ) {

		m_pNic->ReadMac(Addr);

		return true;
	}

	return false;
}

bool CEthernetDriver::GetDhcpOption(BYTE bType, UINT &Data)
{
	if( bType ) {

		if( bType == 0xFF ) {

			Data = 1;

			return true;
		}

		if( m_pDhcp ) {

			Data = m_pDhcp->GetOption(bType);

			return true;
		}

		return false;
	}

	Data = m_pDhcp ? 1 : 0;

	return true;
}

bool CEthernetDriver::GetSockOption(UINT uCode, UINT &uValue)
{
	switch( uCode ) {

		case OPT_SEND_MSS:

			if( m_Config.m_SendMSS ) {

				uValue = m_Config.m_SendMSS;

				return true;
			}

			return false;

		case OPT_RECV_MSS:

			if( m_Config.m_RecvMSS ) {

				uValue = m_Config.m_RecvMSS;

				return true;
			}

			return false;
	}

	return false;
}

bool CEthernetDriver::SetMulticast(IPADDR *pList, UINT uList)
{
	if( m_pNic ) {

		if( !pList || !uList ) {

			m_pNic->SetMulticast(NULL, 0);
		}
		else {
			CMacAddr *pMac = New CMacAddr[uList];

			for( UINT n = 0; n < uList; n++ ) {

				pMac[n].MakeMulticast(pList[n]);
			}

			m_pNic->SetMulticast(pMac, uList);

			delete[] pMac;
		}

		return true;
	}

	return false;
}

bool CEthernetDriver::Open(void)
{
	for( SetTimer(2000); GetTimer(); ) {

		if( m_pNic ) {

			bool fFast = m_Config.m_fFast ? true : false;

			bool fFull = m_Config.m_fFull ? true : false;

			if( m_pNic->Open(fFast, fFull) ) {

				m_pNic->ReadMac(m_Mac);

				m_pDhcp   = m_Config.m_Ip.IsEmpty() ? New CDhcp : NULL;

				m_pArp    = New CArpCache;

				m_hThread = CreateClientThread(this, 0, 81000);

				SetStatus("OPEN");

				DiagRegister();

				return true;
			}

			return false;
		}

		Sleep(250);

		AfxGetObject(m_Config.m_DevName, m_Config.m_DevInst, INic, m_pNic);
	}

	return false;
}

bool CEthernetDriver::Close(void)
{
	if( m_hThread ) {

		DiagRevoke();

		m_pSink->OnLinkDown(m_uFace);

		DestroyThread(m_hThread);

		m_hThread = NULL;

		m_pNic->Close();

		EmptyPendingList();

		SetStatus("CLOSED");

		return true;
	}

	return false;
}

void CEthernetDriver::Poll(void)
{
	if( m_pDhcp ) {

		if( !m_fDhcp ) {

			m_pDhcp->Bind(m_pRouter, m_uFace);

			m_pDhcp->SetMAC(m_Mac);

			m_fDhcp = true;
		}

		m_pDhcp->Service();
	}

	PollPendingList();
}

bool CEthernetDriver::Send(IPREF Ip, CBuffer *pBuff)
{
	if( !m_pNic || !m_hThread ) {

		return false;
	}

	if( !SimpleSend(Ip, pBuff) ) {

		CIpAddr Dest = Ip;

		if( FindDest(Dest, pBuff) ) {

			CMacAddr Mac;

			if( m_pArp->Find(Dest, Mac) ) {

				if( Mac.IsEmpty() ) {

					// We have a null entry in the ARP table, which means
					// that ARPs for this IP have failed. We thus do not
					// bother pending the frame, but send an ARP in any
					// case in the hope that we get a reply at some point.

					SendArpRequest(Dest);

					return false;
				}

				if( pBuff ) {

					// We have a normal send to an IP for which we know
					// the Mac, so just send it and be done. This code
					// is skipped for a NULL buffer, which is used to
					// indicate an ARP probe.

					SendFrame(Mac, ET_IP, pBuff);
				}

				return true;
			}

			if( !pBuff ) {

				// This is an ARP probe to see if an address is in use.
				// We implement some lazy handling that sends a request
				// and then loops while checking to see if the IP has
				// been added to the cache. This is inefficient, but is
				// only used to check for duplicate IPs.

				TcpDebug(OBJ_ETHERNET, LEV_TRACE, "send ip probe\n");

				SendArpRequest(Dest);

				for( UINT n = 0; n < 4; n++ ) {

					Sleep(5);

					if( m_pArp->Find(Dest, Mac) ) {

						return true;
					}
				}

				return false;
			}

		// We don't have the Mac in the ARP table, so add it to
		// the pending list. Note that this call copies the buffer
		// as the original must not be touched after Send returns.

			AddToPendingList(Dest, pBuff);

			return true;
		}

		return false;
	}

	return true;
}

// IPacketCapture

BOOL CEthernetDriver::IsCaptureRunning(void)
{
	return m_fCapture ? TRUE : FALSE;
}

PCSTR CEthernetDriver::GetCaptureFilter(void)
{
	return m_CaptFilter.GetFilter();
}

UINT CEthernetDriver::GetCaptureSize(void)
{
	return m_fCapture ? 0 : m_CaptBuffer.GetSize();
}

BOOL CEthernetDriver::CopyCapture(PBYTE pData, UINT uSize)
{
	if( !m_fCapture ) {

		UINT uUsed = m_CaptBuffer.GetSize();

		if( uUsed && uSize <= uUsed ) {

			memcpy(pData, m_CaptBuffer.GetData(), uUsed);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CEthernetDriver::StartCapture(PCSTR pFilter)
{
	if( !m_fCapture ) {

		if( m_CaptFilter.Parse(pFilter) ) {

			m_CaptBuffer.Initialize(1);

			m_fCapture = true;

			return TRUE;
		}

		m_CaptBuffer.Empty();
	}

	return FALSE;
}

void CEthernetDriver::StopCapture(void)
{
	m_fCapture = false;
}

void CEthernetDriver::KillCapture(void)
{
	m_fCapture = false;

	m_CaptBuffer.Empty();
}

// IDiagProvider

UINT CEthernetDriver::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	UINT uCmd;

	switch( (uCmd = pCmd->GetCode()) ) {

		case 1:
			return DiagStatus(pOut, pCmd);

		case 2:
		case 3:
			return m_pArp->RunDiagCmd(pOut, uCmd - 1);

		case 4:
			return m_pDhcp->RunDiagCmd(pOut, uCmd - 3);

		case 5:
			return DiagAll(pOut, pCmd);
	}

	return 0;
}

// IClientProcess

BOOL CEthernetDriver::TaskInit(UINT uTask)
{
	SetThreadName("Ethernet");

	return TRUE;
}

INT CEthernetDriver::TaskExec(UINT uTask)
{
	for( ;;) {

		if( TRUE ) {

			WaitLink();

			TcpDebug(OBJ_ETHERNET, LEV_TRACE, "%s link active\n", PCTXT(m_Config.m_Display));
		}

		if( m_pDhcp ) {

			m_pDhcp->LinkActive(TRUE);
		}

		if( TRUE ) {

			SetStatus("UP");

			m_pSink->OnLinkActive(m_uFace);

			m_fActive = TRUE;

			WorkLink();

			m_fActive = FALSE;

			m_pSink->OnLinkDown(m_uFace);

			EmptyPendingList();

			SetStatus("DOWN");

			TcpDebug(OBJ_ETHERNET, LEV_TRACE, "%s link broken\n", PCTXT(m_Config.m_Display));
		}

		if( m_pDhcp ) {

			m_pDhcp->LinkActive(FALSE);
		}
	}

	return 0;
}

void CEthernetDriver::TaskStop(UINT uTask)
{
}

void CEthernetDriver::TaskTerm(UINT uTask)
{
}

// Task Implementation

void CEthernetDriver::WaitLink(void)
{
	for( ;;) {

		if( m_pNic->WaitLink(FOREVER) ) {

			break;
		}
	}
}

void CEthernetDriver::WorkLink(void)
{
	for( ;;) {

		CBuffer *pBuff = NULL;

		if( !m_pNic->ReadData(pBuff, FOREVER) ) {

			break;
		}

		if( pBuff ) {

			UINT uSize = pBuff->GetSize();

			if( uSize >= sizeof(MACHEADER) ) {

				CMacHeader *pHeader = (CMacHeader *) pBuff->GetData();

				pHeader->NetToHost();

				if( pHeader->m_Type >= 1536 ) {

					switch( pHeader->m_Type ) {

						case ET_IP:

							if( !m_Config.m_Ip.IsExperimental() ) {

								pBuff->StripHead(sizeof(MACHEADER));

								HandleIp(pHeader, pBuff);
							}

							break;

						case ET_ARP:

							if( !m_Config.m_Ip.IsExperimental() ) {

								pBuff->StripHead(sizeof(MACHEADER));

								HandleArp(pHeader, pBuff);
							}

							break;

						case ET_GRE_ISO:
						case ET_PUP:
						case ET_REVARP:
						case ET_NS:
						case ET_SPRITE:
						case ET_TRAIL:
						case ET_MOPDL:
						case ET_MOPRC:
						case ET_DN:
						case ET_LAT:
						case ET_SCA:
						case ET_LANBRIDGE:
						case ET_DECDNS:
						case ET_DECDTS:
						case ET_VEXP:
						case ET_VPROD:
						case ET_ATALK:
						case ET_AARP:
						case ET_8021Q:
						case ET_IPX:
						case ET_IPV6:
						case ET_PPP:
						case ET_MPCP:
						case ET_SLOW:
						case ET_MPLS:
						case ET_MPLS_MULTI:
						case ET_PPPOED:
						case ET_PPPOES:
						case ET_INTEL:
						case ET_JUMBO:
						case ET_LLDP:
						case ET_EAPOL:
						case ET_RRCP:
						case ET_LOOPBACK:
						case ET_VMAN:
						case ET_CFM_OLD:
						case ET_CFM:

							BuffRelease(pBuff);

							break;

						case ET_NVIEW:

							HandleRaw(pBuff);

							break;

						default:

							HandleRaw(pBuff);

							break;
					}
				}
				else {
					UINT uSize = pHeader->m_Type + sizeof(MACHEADER);

					UINT uMore = pBuff->GetSize() - uSize;

					pBuff->StripTail(uMore);

					HandleRaw(pBuff);
				}

				continue;
			}

			TcpDebug(OBJ_ETHERNET, LEV_WARN, "packet too small\n");

			BuffRelease(pBuff);
		}
	}
}

// Frame Handlers

void CEthernetDriver::HandleIp(CMacHeader *pHeader, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	if( pBuff->GetSize() >= sizeof(IPHEADER) ) {

		CIpHeader *pIP = (CIpHeader *) pBuff->GetData();

		if( !pHeader->m_Dest.IsBroadcast() ) {

			m_pArp->Validate(pHeader->m_Src);
		}

		if( m_fCapture ) {

			if( m_CaptFilter.StoreIp(pIP, FALSE) ) {

				PBYTE pRaw = PBYTE(pIP)       - sizeof(MACHEADER);

				UINT  uRaw = pBuff->GetSize() +  sizeof(MACHEADER);

				pHeader->HostToNet();

				m_CaptBuffer.Capture(pRaw, uRaw);
			}
		}

		m_pSink->OnPacket(m_uFace, Buff.TakeOver());

		return;
	}

	TcpDebug(OBJ_ETHERNET, LEV_WARN, "ip packet too small\n");
}

void CEthernetDriver::HandleArp(CMacHeader *pHeader, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	if( pBuff->GetSize() >= sizeof(ARPFRAME) ) {

		if( m_fCapture ) {

			if( m_CaptFilter.StoreArp(FALSE) ) {

				PBYTE pRaw = pBuff->GetData() - sizeof(MACHEADER);

				UINT  uRaw = pBuff->GetSize() + sizeof(MACHEADER);

				pHeader->HostToNet();

				m_CaptBuffer.Capture(pRaw, uRaw);
			}
		}

		if( TRUE ) {

			CARPFrame *pArp = BuffStripHead(pBuff, CARPFrame);

			pArp->NetToHost();

			if( pArp->m_IpDest == pArp->m_IpSrc ) {

				// This is a gratutious ARP frame, so add the
				// address pair to our cache whether or not it
				// is actually on our subnet.

				AddArpEntry(pArp->m_IpSrc, pArp->m_MacSrc, TRUE);

				return;
			}

			if( pArp->IsReq() ) {

				if( !m_Config.m_Ip.IsEmpty() ) {

					if( pArp->m_IpDest == m_Config.m_Ip ) {

						// This is an ARP request, so we send our reply.

						AddArpEntry(pArp->m_IpSrc, pArp->m_MacSrc, FALSE);

						SendArpReply(pBuff);
					}
				}

				return;
			}

			if( pArp->IsRep() ) {

				CIpAddr From = m_Config.m_Ip;

				if( pArp->m_IpDest == From ) {

					// This is an ARP reply to us. We add the address pair
					// to our cache, and then we scan the pending list to
					// see if there are any frames that can now be sent.

					AddArpEntry(pArp->m_IpSrc, pArp->m_MacSrc, TRUE);

					ScanPendingList(pArp->m_IpSrc, pArp->m_MacSrc);
				}

				return;
			}
		}

		TcpDebug(OBJ_ETHERNET, LEV_WARN, "arp packet unknown\n");

		return;
	}

	TcpDebug(OBJ_ETHERNET, LEV_WARN, "arp packet too small\n");
}

void CEthernetDriver::HandleRaw(CBuffer *pBuff)
{
	UINT      uSize = pBuff->GetSize();

	CIpHeader *pIP  = BuffAddHead(pBuff, CIpHeader);

	pIP->SetHeader(IP_EXPER, IP_EXPER, IP_RAW, uSize);

	pIP->HostToNet();

	pIP->AddChecksum();

	m_pSink->OnPacket(m_uFace, pBuff);
}

// ARP Helpers

BOOL CEthernetDriver::SendArpReply(CBuffer *pBuff)
{
	TcpDebug(OBJ_ARP, LEV_TRACE, "send reply\n");

	CARPFrame *pArp = BuffAddHead(pBuff, CARPFrame);

	pArp->MakeRep(m_Mac);

	pArp->HostToNet();

	if( !SendFrame(pArp->m_MacDest, ET_ARP, pBuff) ) {

		TcpDebug(OBJ_ARP, LEV_WARN, "cannot send reply\n");

		return FALSE;
	}

	return TRUE;
}

BOOL CEthernetDriver::SendArpRequest(IPREF Ip)
{
	CAutoBuffer Buff(sizeof(CARPFrame));

	if( Buff ) {

		CARPFrame *pArp = BuffAddTail(Buff, CARPFrame);

		CIpAddr    From = m_Config.m_Ip;

		pArp->MakeReq(m_Mac, From, Ip);

		pArp->HostToNet();

		if( SendFrame(MAC_BROAD, ET_ARP, Buff) ) {

			return TRUE;
		}

		TcpDebug(OBJ_ARP, LEV_WARN, "cannot send request\n");

		return FALSE;
	}

	TcpDebug(OBJ_ARP, LEV_WARN, "cannot allocate request\n");

	return FALSE;
}

BOOL CEthernetDriver::AddArpEntry(IPREF Ip, MACREF Mac, BOOL fForce)
{
	if( fForce || Ip.OnSubnet(m_Config.m_Ip, m_Config.m_Mask) ) {

		m_pArp->Add(Ip, Mac);

		return TRUE;
	}

	return FALSE;
}

void CEthernetDriver::SetNotFound(IPREF Ip)
{
	m_pArp->Add(Ip, MAC_EMPTY);
}

// Send Helpers

BOOL CEthernetDriver::SimpleSend(IPREF Ip, CBuffer *pBuff)
{
	// All of these packets can be sent without reference
	// to the ARP table, so we handle them here and let the
	// caller know that we've transmitted the frame.

	if( Ip.IsExperimental() ) {

		// We use the experimental address to indicate a
		// packet that will be sent raw, without the IP
		// header or any other header information.

		BuffStripHead(pBuff, CIpHeader);

		PadData(pBuff);

		m_pNic->SendData(pBuff, FOREVER);

		return TRUE;
	}

	if( m_Config.m_Ip.IsExperimental() ) {

		// If the interface is configured with just the
		// experimental address, it means that it can only
		// send raw frames, so anything else we'll drop.

		return TRUE;
	}

	if( Ip.IsBroadcast(m_Config.m_Mask) ) {

		// Broadcasts are sent to a well-known Mac.

		SendFrame(MAC_BROAD, ET_IP, pBuff);

		return TRUE;
	}

	if( Ip.IsMulticast() ) {

		// Multicasts are sent to a well-known Mac.

		CMacAddr Mac;

		Mac.MakeMulticast(Ip);

		SendFrame(Mac, ET_IP, pBuff);

		return TRUE;
	}

	return FALSE;
}

BOOL CEthernetDriver::FindDest(CIpAddr &Ip, CBuffer *pBuff)
{
	if( IsLinkLocal(Ip) ) {

		if( pBuff ) {

			CIpHeader *pIP = (CIpHeader *) pBuff->GetData();

			Ip = pIP->m_IpDest;

			return TRUE;
		}

		return FALSE;
	}

	if( !pBuff ) {

		return TRUE;
	}

	if( !Ip.OnSubnet(m_Config.m_Ip, m_Config.m_Mask) ) {

		TcpDebug(OBJ_ETHERNET, LEV_WARN, "host not on subnet\n");

		return FALSE;
	}

	return TRUE;
}

BOOL CEthernetDriver::SendFrame(MACREF Mac, WORD Type, CBuffer *pBuff)
{
	PBYTE pData = pBuff->GetData();

	UINT  uData = pBuff->GetSize();

	PadData(pBuff);

	CMacHeader *pHeader = BuffAddHead(pBuff, CMacHeader);

	pHeader->m_Dest = Mac;

	pHeader->m_Type = Type;

	pHeader->HostToNet();

	if( m_fCapture ) {

		PBYTE pRaw  = pData - sizeof(MACHEADER);

		UINT  uRaw  = uData + sizeof(MACHEADER);

		if( Type == ET_ARP ) {

			if( m_CaptFilter.StoreArp(TRUE) ) {

				m_CaptBuffer.Capture(pRaw, uRaw);
			}
		}

		if( Type == ET_IP ) {

			CIpHeader *pIP = (CIpHeader *) pData;

			if( m_CaptFilter.StoreIp(pIP, TRUE) ) {

				m_CaptBuffer.Capture(pRaw, uRaw);
			}
		}
	}

	if( m_pNic->SendData(pBuff, FOREVER) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CEthernetDriver::PadData(CBuffer *pBuff)
{
	if( FALSE ) {

		if( pBuff->GetSize() < macMinSize ) {

			UINT uExtra = macMinSize - pBuff->GetSize();

			memset(pBuff->AddTail(uExtra), 0x00, uExtra);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CEthernetDriver::IsLinkLocal(IPREF Ip)
{
	if( Ip.IsEmpty() ) {

		return TRUE;
	}

	if( Ip == m_Local ) {

		return TRUE;
	}

	return FALSE;
}

// Pending ARP List

BOOL CEthernetDriver::AddToPendingList(IPREF Dest, CBuffer *pBuff)
{
	CBuffer *pCopy = pBuff->MakeCopy();

	if( pCopy ) {

		CPending *pPend = New CPending;

		pPend->m_pBuff  = pCopy;

		pPend->m_Dest   = Dest;

		CAutoLock Lock(m_pLock);

		CPending *pScan;

		for( pScan = m_pPendHead; pScan; pScan = pScan->m_pNext ) {

			if( pScan->m_Dest == pPend->m_Dest ) {

				break;
			}
		}

	// The ordering is important here. We put any packets for the
	// same IP together in the list, but in reverse order. This is
	// easier with our Insert macro, but it does mean that we have to
	// scan backwards when sending the frames at a later point if we
	// are to preserve the original ordering.

		AfxListInsert(m_pPendHead,
			      m_pPendTail,
			      pPend,
			      m_pNext,
			      m_pPrev,
			      pScan
		);

		if( !pScan ) {

			// We only send an ARP request the first time a given IP appears
			// in the pending list, so this entry is always the last in a
			// given grouping for a specific IP. This ordering is important
			// later when we're processing these timeouts. Note also that we
			// use a long timeout for the gateway to handle situations where
			// it is not on a local segment and could take a long time to
			// reply as occurs with satellite modems!

			pPend->m_uLeft = arpCount;

			pPend->m_uWait = (Dest == m_Config.m_Gate) ? ToTicks(5000) : arpDelay;

			pPend->m_uTime = GetTickCount();

			Lock.Free();

			SendArpRequest(Dest);
		}
		else {
			pPend->m_uLeft = 0;

			pPend->m_uWait = 0;

			pPend->m_uTime = 0;
		}

		return TRUE;
	}

	return FALSE;
}

void CEthernetDriver::EmptyPendingList(void)
{
	CAutoLock Lock(m_pLock);

	while( m_pPendHead ) {

		CPending *pNext = m_pPendHead->m_pNext;

		m_pPendHead->m_pBuff->Release();

		delete m_pPendHead;

		m_pPendHead = pNext;
	}

	m_pPendHead = NULL;

	m_pPendTail = NULL;
}

BOOL CEthernetDriver::ScanPendingList(IPREF Dest, MACREF Mac)
{
	// We are looking for packets that we can now send as a result
	// of an ARP reply. We scan the list in reverse order as it was
	// constructed such that the newer packets are at the start of
	// a run of identical IPs. This ensures that we send the packets
	// in the correct sequence. We also copy the packets to a new
	// list so we can release the mutex and handle the transmission
	// without blocking other threads that need access.

	CAutoLock Lock(m_pLock);

	CPending *pScan = m_pPendTail;

	CPending *pHead = NULL;

	CPending *pTail = NULL;

	while( pScan ) {

		CPending *pPrev = pScan->m_pPrev;

		if( pScan->m_Dest == Dest ) {

			AfxListRemove(m_pPendHead,
				      m_pPendTail,
				      pScan,
				      m_pNext,
				      m_pPrev
			);

			AfxListAppend(pHead,
				      pTail,
				      pScan,
				      m_pNext,
				      m_pPrev
			);
		}

		pScan = pPrev;
	}

	Lock.Free();

	if( pHead ) {

		while( pHead ) {

			CPending *pNext = pHead->m_pNext;

			SendFrame(Mac, ET_IP, pHead->m_pBuff);

			pHead->m_pBuff->Release();

			delete pHead;

			pHead = pNext;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CEthernetDriver::PollPendingList(void)
{
	CAutoLock Lock(m_pLock);

	CIpAddr   Send[8];

	UINT      uSend = 0;

	UINT      uTime = GetTickCount();

	CPending *pScan = m_pPendTail;

	while( pScan ) {

		CPending *pPrev = pScan->m_pPrev;

		if( pScan->m_uLeft ) {

			if( uTime - pScan->m_uTime >= pScan->m_uWait ) {

				if( !--pScan->m_uLeft ) {

					// We are out of retries, so we need to remove this entry
					// from the list. But there might be other entries for the
					// same Ip earlier in the list, so we first check for those
					// and remove them as required.

					while( pPrev && pPrev->m_Dest == pScan->m_Dest ) {

						CPending *pBack = pPrev->m_pPrev;

						AfxListRemove(m_pPendHead,
							      m_pPendTail,
							      pPrev,
							      m_pNext,
							      m_pPrev
						);

						pPrev->m_pBuff->Release();

						delete pPrev;

						pPrev = pBack;
					}

				// Before we do anything else, we add a null entry to
				// the ARP cache for this Ip to change the ARP logic
				// such that future frames don't get pended but instead
				// are returned as undeliverable.

					SetNotFound(pScan->m_Dest);

					// Now we can safety remove this entry from this
					// list, free its buffer and delete the record.

					AfxListRemove(m_pPendHead,
						      m_pPendTail,
						      pScan,
						      m_pNext,
						      m_pPrev
					);

					pScan->m_pBuff->Release();

					delete pScan;
				}
				else {
					// We build up a list of up to N requests that
					// we shall be sending this time as result of
					// timeouts. These will be handled at the end
					// of function once the mutex is released.

					if( uSend < elements(Send) ) {

						Send[uSend++]  = pScan->m_Dest;

						pScan->m_uTime = uTime;
					}
					else {
						// This list is full so there is no point
						// scanning any further this time, but we
						// didn't touch the tick values so any
						// timed-out entries will stay that way.

						break;
					}
				}
			}
		}

		pScan = pPrev;
	}

	Lock.Free();

	if( uSend ) {

		// Now we can send whatever
		// ARP retries are required.

		for( UINT n = 0; n < uSend; n++ ) {

			SendArpRequest(Send[n]);
		}

		return TRUE;
	}

	return FALSE;
}

// Status

void CEthernetDriver::SetStatus(PCTXT pStatus)
{
	strcpy(m_Stat, pStatus);
}

// Diagnostics

BOOL CEthernetDriver::DiagRegister(void)
{
	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, m_Config.m_Display);

		m_pDiag->RegisterCommand(m_uProv, 1, "status");

		m_pDiag->RegisterCommand(m_uProv, 2, "arp.clear");

		m_pDiag->RegisterCommand(m_uProv, 3, "arp.status");

		if( m_pDhcp ) {

			m_pDiag->RegisterCommand(m_uProv, 4, "dhcp.status");
		}

		m_pDiag->RegisterCommand(m_uProv, 5, "all");

		return TRUE;
	}

	return FALSE;
}

BOOL CEthernetDriver::DiagRevoke(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;

		return TRUE;
	}

	return FALSE;
}

UINT CEthernetDriver::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		CMacAddr Mac;

		m_pNic->ReadMac(Mac);

		pOut->AddPropList();

		pOut->AddProp("MAC Address", Mac.GetAsText());

		pOut->EndPropList();

		NICDIAG Diag = { 0 };

		m_pNic->GetCounters(Diag);

		pOut->AddTable(4);

		pOut->SetColumn(0, "Type", "%s");
		pOut->SetColumn(1, "Count", "%u");
		pOut->SetColumn(2, "Error", "%u");
		pOut->SetColumn(3, "Discard", "%u");

		pOut->AddHead();

		pOut->AddRule('-');

		pOut->AddRow();
		pOut->SetData(0, "Send");
		pOut->SetData(1, Diag.m_TxCount);
		pOut->SetData(2, Diag.m_TxFail);
		pOut->SetData(3, Diag.m_TxDisc);
		pOut->EndRow();

		pOut->AddRow();
		pOut->SetData(0, "Recv");
		pOut->SetData(1, Diag.m_RxCount);
		pOut->SetData(2, Diag.m_RxOver);
		pOut->SetData(3, Diag.m_RxDisc);
		pOut->EndRow();

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CEthernetDriver::DiagAll(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	DiagStatus(pOut, pCmd);

	if( m_pDhcp ) {

		m_pDhcp->RunDiagCmd(pOut, 1);
	}

	m_pArp->RunDiagCmd(pOut, 2);

	return 0;
}

// End of File
