
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TcpHeader_HPP

#define	INCLUDE_TcpHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// TCP Header
//

#pragma pack(1)

#if defined(AEON_LITTLE_ENDIAN)

struct TCPHEADER
{
	WORD	m_LocPort;
	WORD	m_RemPort;
	DWORD   m_Seq;
	DWORD   m_Ack;

	WORD	m_FlagFIN:1;
	WORD	m_FlagSYN:1;
	WORD	m_FlagRST:1;
	WORD	m_FlagPSH:1;
	WORD	m_FlagACK:1;
	WORD	m_FlagURG:1;
	WORD	m_Unused:6;
	WORD	m_Offset:4;
	
	WORD	m_Wnd;
	WORD	m_Checksum;
	WORD	m_Urgent;
	};

#else

struct TCPHEADER
{
	WORD	m_LocPort;
	WORD	m_RemPort;
	DWORD   m_Seq;
	DWORD   m_Ack;

	WORD	m_Offset:4;
	WORD	m_Unused:6;
	WORD	m_FlagURG:1;
	WORD	m_FlagACK:1;
	WORD	m_FlagPSH:1;
	WORD	m_FlagRST:1;
	WORD	m_FlagSYN:1;
	WORD	m_FlagFIN:1;
	
	WORD	m_Wnd;
	WORD	m_Checksum;
	WORD	m_Urgent;
	};

#endif

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// TCP Header Wrapper
//

class CTcpHeader : public TCPHEADER
{
	public:
		// Conversion
		void NetToHost(UINT uSize);
		void HostToNet(void);

		// Attributes
		BOOL TestChecksum(PSREF PS, UINT uSize);
		UINT GetHeaderSize(void) const;
		UINT GetOptionsSize(void) const;
		UINT GetDataSize(void) const;
		UINT GetSymbols(void) const;
		UINT GetSymbolsNoFin(void) const;
		BOOL GetMaxSegSize(WORD &wSize) const;

		// Operations
		void Init(void);
		void AddMaxSegSize(CBuffer *pBuff, WORD wSize);
		void AddChecksum(DWORD Load, UINT uSize);
		void SetDataSize(UINT uSize);
		void PrintShort(void);
		void Print(void);

	private:
		// Constructor
		CTcpHeader(void);

	protected:
		// Implementation
		WORD CalcChecksum(DWORD Load, UINT uSize) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Conversion

STRONG_INLINE void CTcpHeader::NetToHost(UINT uSize)
{
	WORD &Flip = PWORD(&m_Ack + 1)[0];

	m_LocPort  = ::NetToHost(m_LocPort);

	m_RemPort  = ::NetToHost(m_RemPort);
	
	m_Seq      = ::NetToHost(m_Seq);
	
	m_Ack      = ::NetToHost(m_Ack);

	m_Wnd      = ::NetToHost(m_Wnd);

	m_Urgent   = ::NetToHost(m_Urgent);

	Flip       = ::NetToHost(Flip);

	// Cache the size in the checksum field for now.

	m_Checksum = WORD(uSize - GetHeaderSize());
	}

STRONG_INLINE void CTcpHeader::HostToNet(void)
{
	WORD &Flip = PWORD(&m_Ack + 1)[0];

	m_LocPort  = ::HostToNet(m_LocPort);

	m_RemPort  = ::HostToNet(m_RemPort);
	
	m_Seq      = ::HostToNet(m_Seq);
	
	m_Ack      = ::HostToNet(m_Ack);

	m_Wnd      = ::HostToNet(m_Wnd);

	m_Urgent   = ::HostToNet(m_Urgent);

	Flip       = ::NetToHost(Flip);
	}

// End of File

#endif

