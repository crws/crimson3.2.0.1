
#include "Intern.hpp"

#include "Dhcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// DHCP Client
//

// Constructor

CDhcp::CDhcp(void)
{
	m_pRouter  = NULL;

	m_uFace    = NOTHING;

	m_pSocket  = NULL;

	m_Net[0]   = 0;

	m_fActive  = false;

	m_State    = stateIdle;

	m_Cmd      = 0;

	m_TimeZone = 0;

	m_fApipa   = false;
	}

// Destructor

CDhcp::~CDhcp(void)
{
	}

// Management

void CDhcp::LinkActive(bool fActive)
{
	if( (m_fActive = fActive) ) {

		m_State = stateWait;

		m_Last	= GetTickCount();

		m_Secs  = 0;

		return;
		}

	DropApipa();

	SetState(stateIdle);
	}

// Attributes

DWORD CDhcp::GetOption(BYTE Type) const
{
	switch( Type ) {

		case dhcpTIMEZONE:

			return m_TimeZone;

		case dhcpNTP:

			return m_NTP.m_dw;

		case dhcpSMTP:

			return m_SMTP.m_dw;

		case dhcpDNS:

			return m_DNS.m_dw;
		}

	return 0;
	}

// Operations

void CDhcp::Bind(IRouter *pRouter, UINT uFace)
{
	m_pRouter = pRouter;

	m_uFace   = uFace;
	}

void CDhcp::SetMAC(MACREF MAC)
{
	m_Mac = MAC;
	}

void CDhcp::Service(void)
{
	if( GetTickCount() - m_Last >= ToTicks(1000) ) {

		m_Last += ToTicks(1000);

		m_Secs += 1;
		}

	if( m_Cmd == stateRenew ) {

		m_Cmd = 0;

		if( m_State == stateBound ) {

			SetState(stateRenew);

			StateTx();

			return;
			}

		if( m_State == stateIdle && m_fActive ) {

			m_State = stateWait;

			m_Last	= GetTickCount();

			m_Secs  = 0;

			return;
			}
		}

	if( m_Cmd == stateRelease ) {

		m_Cmd = 0;

		if( m_State == stateBound ) {

			SetState(stateRelease);

			StateTx();

			return;
			}
		}

	while( StateRx() ) {

		StateTx();
		}
	}

// Diagnostics

UINT CDhcp::RunDiagCmd(IDiagOutput *pOut, UINT uCmd)
{
	switch( uCmd ) {

		case 1:
			return DiagStatus(pOut);

		case 2:
			return DiagRelease(pOut);

		case 3:
			return DiagRenew(pOut);
		}

	return 0;
	}

// Socket Management

void CDhcp::OpenSocket(bool fServer)
{
	if( !m_pSocket ) {

		m_pSocket = m_pRouter->CreateSocket(IP_UDP);

		if( !fServer ) {

			CIpAddr Host;

			Host.MakeDirected(m_uFace);

			m_pSocket->Connect(Host, 67, 68);
			}
		else {
			CIpAddr Host = m_Server;
			
			m_pSocket->Connect(Host, 67, 68);
			}
		}
	}

void CDhcp::CloseSocket(void)
{
	if( m_pSocket ) {

		m_pSocket->Close();

		m_pSocket->Release();

		m_pSocket = NULL;
		}
	}

// Message Handlers

bool CDhcp::SendDiscover(void)
{
	CAutoBuffer Buff(sizeof(CDhcpFrame));

	if( Buff ) {

		TcpDebug(OBJ_DHCP, LEV_TRACE, "interface %u discover\n", m_uFace);

		CDhcpFrame *pDhcp = BuffAddTail(Buff, CDhcpFrame);

		UINT        uSize = pDhcp->MakeDiscover(m_Mac);

		AdjustPacket(Buff, uSize);

		pDhcp->HostToNet();

		m_pSocket->Send(Buff.TakeOver());

		return true;
		}

	TcpDebug(OBJ_DHCP, LEV_WARN, "cannot allocate discover\n");

	return false;
	}

bool CDhcp::SendRequest(void)
{
	CAutoBuffer Buff(sizeof(CDhcpFrame));

	if( Buff ) {

		TcpDebug(OBJ_DHCP, LEV_TRACE, "interface %u request\n", m_uFace);

		CDhcpFrame *pDhcp = BuffAddTail(Buff, CDhcpFrame);

		UINT        uSize = pDhcp->MakeRequest(m_Mac, m_Client, m_Server, m_Net);

		AdjustPacket(Buff, uSize);

		pDhcp->HostToNet();

		m_pSocket->Send(Buff.TakeOver());

		return true;
		}

	TcpDebug(OBJ_DHCP, LEV_WARN, "cannot allocate request\n");

	return false;
	}

bool CDhcp::SendRenew(bool fServer)
{
	CAutoBuffer Buff(sizeof(CDhcpFrame));

	if( Buff ) {

		TcpDebug(OBJ_DHCP, LEV_TRACE, "interface %u renew\n", m_uFace);

		CDhcpFrame *pDhcp = BuffAddTail(Buff, CDhcpFrame);

		UINT        uSize = pDhcp->MakeRenew(m_Mac, m_Client, m_Net, fServer);

		AdjustPacket(Buff, uSize);

		pDhcp->HostToNet();

		m_pSocket->Send(Buff.TakeOver());

		return true;
		}

	TcpDebug(OBJ_DHCP, LEV_WARN, "cannot allocate renew\n");

	return false;
	}

bool CDhcp::SendRelease(bool fServer)
{
	CAutoBuffer Buff(sizeof(CDhcpFrame));

	if( Buff ) {

		TcpDebug(OBJ_DHCP, LEV_TRACE, "interface %u release\n", m_uFace);

		CDhcpFrame *pDhcp = BuffAddTail(Buff, CDhcpFrame);

		UINT        uSize = pDhcp->MakeRelease(m_Mac, m_Client, m_Net, fServer);

		AdjustPacket(Buff, uSize);

		pDhcp->HostToNet();

		m_pSocket->Send(Buff.TakeOver());

		return true;
		}

	TcpDebug(OBJ_DHCP, LEV_WARN, "cannot allocate release\n");

	return false;
	}

bool CDhcp::RecvOffer(void)
{
	for(;;) {
	
		CAutoBuffer Buff;

		if( m_pSocket->Recv(Buff) == S_OK ) {

			DropApipa();

			CDhcpFrame *pDhcp = (CDhcpFrame *) Buff->GetData();

			if( pDhcp->m_Mac == m_Mac ) {

				pDhcp->NetToHost();

				if( pDhcp->MatchID() ) {

					if( pDhcp->GetType() == msgOFFER ) {

						m_Client = pDhcp->m_IpY;

						ScanOptions(pDhcp, Buff->GetSize());

						return true;
						}
					}
				}

			continue;
			}

		break;
		}

	return false;
	}

WORD CDhcp::RecvAck(void)
{
	for(;;) {
	
		CAutoBuffer Buff;

		if( m_pSocket->Recv(Buff) == S_OK ) {

			DropApipa();

			CDhcpFrame *pDhcp = (CDhcpFrame *) Buff->GetData();

			if( pDhcp->m_Mac == m_Mac ) {

				pDhcp->NetToHost();

				if( pDhcp->MatchID() ) {

					if( pDhcp->GetType() == msgACK ) {

						ScanOptions(pDhcp, Buff->GetSize());

						return msgACK;
						}

					if( pDhcp->GetType() == msgNAK ) {

						return msgNAK;
						}
					}
				}

			continue;
			}

		break;
		}

	return 0;
	}

// State Machine

bool CDhcp::StateRx(void)
{
	WORD Ack;

	switch( m_State ) {

		case stateWait:

			if( m_Secs >= 1 ) {

				SetState(stateInit);

				return true;
				}

			return false;

		case stateInit:

			SetState(stateDiscover);

			return true;

		case stateDiscover:

			if( RecvOffer() ) {

				SetState(stateRequest);

				return true;
				}

			if( CheckTimeout() ) {

				SetState(stateDiscover);

				return true;
				}

			break;

		case stateRequest:

			if( (Ack = RecvAck()) == msgACK ) {

				CloseSocket();

				SetState(stateBound);

				return true;
				}

			if( Ack == msgNAK ) {

				SetState(stateDiscover);

				return true;
				}
				
			if( CheckTimeout() ) {

				SetState(stateDiscover);

				return true;
				}

			break;

		case stateBound:

			if( m_Secs >= (m_Renew ? m_Renew : 2 * m_Lease / 4) ) {

				SetState(stateRenew);

				return true;
				}

			break;

		case stateRenew:

			if( m_Secs >= (m_Rebind ? m_Rebind : 3 * m_Lease / 4) ) {

				CloseSocket();

				SetState(stateRebind);

				return true;
				}

			if( (Ack = RecvAck()) == msgACK ) {

				CloseSocket();

				SetState(stateRenewed);

				return true;
				}

			if( Ack == msgNAK ) {

				SetState(stateInit);

				return true;
				}

			if( CheckTimeout() ) {

				SetState(stateRenew);

				return true;
				}

			break;

		case stateRebind:

			if( m_Secs >= m_Lease ) {

				CloseSocket();

				SetState(stateInit);

				return true;
				}

			if( (Ack = RecvAck()) == msgACK ) {

				CloseSocket();

				SetState(stateRenewed);

				return true;
				}

			if( Ack == msgNAK ) {

				SetState(stateInit);

				return true;
				}

			if( CheckTimeout() ) {

				SetState(stateRebind);

				return true;
				}

			break;

		case stateRelease:

			CloseSocket();

			SetState(stateReleased);

			return true;
		}

	return false;
	}

void CDhcp::StateTx(void)
{
	switch( m_State ) {

		case stateDiscover:

			OpenSocket(false);

			StartTimeout(true);

			MakeApipa();

			SendDiscover();

			break;

		case stateRequest:

			StartTimeout(false);

			SendRequest();

			break;

		case stateRenew:

			OpenSocket(true);

			StartTimeout(false);

			SendRenew(true);

			break;

		case stateRebind:

			OpenSocket(false);

			StartTimeout(false);

			SendRenew(false);

			break;

		case stateRelease:

			OpenSocket(true);

			StartTimeout(false);

			SendRelease(true);

			break;
		}
	}

// Implementation

void CDhcp::AdjustPacket(CBuffer *pBuff, UINT uSize)
{
	uSize = (uSize + 3) & ~3;

	uSize = max(uSize, 300);

	pBuff->StripTail(pBuff->GetSize() - uSize);
	}

void CDhcp::ScanOptions(CDhcpFrame *pFrame, UINT uSize)
{
	BYTE bOver = ScanOptions(pFrame->m_Opts, uSize);

	if( bOver & 0x01 ) {

		ScanOptions( PBYTE (pFrame->m_File),
			     sizeof(pFrame->m_File)
			     );
		}

	if( bOver & 0x02 ) {

		ScanOptions( PBYTE (pFrame->m_Name),
			     sizeof(pFrame->m_Name)
			     );
		}
	}

BYTE CDhcp::ScanOptions(PBYTE pData, UINT uSize)
{
	if( PDWORD(pData)[0] == ::HostToNet(DHCP_MAGIC) ) {

		pData += sizeof(DWORD);

		uSize -= sizeof(DWORD);

		BYTE bOver = 0;

		UINT uData = 0;

		while( uData < uSize ) {

			CDhcpOption *pOption = (CDhcpOption *) (pData + uData);

			if( pOption->IsEnd() ) {

				uData++;

				break;
				}

			if( pOption->IsPad() ) {

				uData++;

				continue;
				}

			UINT Ptr = 0;

			switch( pOption->m_Type ) {

				case dhcpOVERLOAD:

					bOver = pOption->GetByte(Ptr);

					break;

				case dhcpSERVER:

					m_Server = pOption->GetAddr(Ptr);
				
					break;
				
				case dhcpDOMAIN:

					strcpy(m_Net, pOption->GetText(Ptr));

					break;

				case dhcpROUTER:

					m_Gate = pOption->GetAddr(Ptr);

					break;

				case dhcpSUBNET:

					m_Mask = pOption->GetAddr(Ptr);

					break;
			
				case dhcpRENEW:
				
					m_Renew  = pOption->GetLong(Ptr);
				
					break;

				case dhcpREBIND:
				
					m_Rebind = pOption->GetLong(Ptr);
				
					break;

				case dhcpIPLEASE:
				
					m_Lease  = pOption->GetLong(Ptr);
				
					break;

				case dhcpTIMEZONE:
				
					m_TimeZone = pOption->GetLong(Ptr);

					break;
		
				case dhcpNTP:
				
					m_NTP  = pOption->GetAddr(Ptr);

					break;
		
				case dhcpSMTP:
				
					m_SMTP = pOption->GetAddr(Ptr);

					break;

				case dhcpDNS:

					m_DNS = pOption->GetAddr(Ptr);

					break;
				}

			uData += pOption->GetSize();
			}

		return bOver;
		}

	return 0;
	}

void CDhcp::SetState(UINT State)
{
	if( m_State != State ) {

		if( State == stateInit || State == stateIdle ) {

			if( !(m_State == stateWait) ) {

				CloseSocket();

				m_pRouter->SetConfig(m_uFace, IP_EMPTY, IP_EMPTY);

				TcpDebug(OBJ_DHCP, LEV_WARN, "interface %u lease lost\n", m_uFace);
				}
			}

		if( State == stateBound ) {

			m_Secs = 0;

			m_pRouter->SetConfig (m_uFace, m_Client, m_Mask);

			m_pRouter->SetGateway(m_uFace, m_Gate);

			TcpDebug(OBJ_DHCP, LEV_INFO, "interface %u lease obtained for %s\n",  m_uFace, PCTXT(m_Client.GetAsText()));
			}

		if( State == stateRenewed ) {

			m_Secs = 0;

			State  = stateBound;

			TcpDebug(OBJ_DHCP, LEV_INFO, "interface %u lease renewed\n", m_uFace);
			}

		if( State == stateReleased ) {

			m_Secs = 0;

			State  = stateIdle;

			m_pRouter->SetConfig(m_uFace, IP_EMPTY, IP_EMPTY);

			TcpDebug(OBJ_DHCP, LEV_INFO, "interface %u lease released\n", m_uFace);
			}

		m_State = State;

		m_Try   = 0;
		}
	}

void CDhcp::StartTimeout(bool fStep)
{
	ClearRecv();

	m_Try   = m_Try + 1;

	m_Limit = ToTicks(fStep ? rand() % 500 + 750 + 250 * Min(m_Try, 16u) : 5000);

	m_Start = GetTickCount();
	}

bool CDhcp::CheckTimeout(void)
{
	return GetTickCount() - m_Start >= m_Limit;
	}

bool CDhcp::ClearRecv(void)
{
	CBuffer *pBuff;

	while( m_pSocket->Recv(pBuff) == S_OK ) {

		BuffRelease(pBuff);
		}

	return true;
	}

// Apipa Implementation

bool CDhcp::MakeApipa(void)
{
	if( !m_fApipa ) {

		if( m_Try >= 10 ) {

			BYTE b1 = 1 + m_Mac.m_Addr[4] % 253;

			BYTE b2 = 1 + m_Mac.m_Addr[5] % 253;

			LONG s1 = MAKELONG(MAKEWORD(b1,b1),MAKEWORD(b1,b1));

			LONG s2 = MAKELONG(MAKEWORD(b2,b2),MAKEWORD(b2,b2));

			for( UINT n = 0; n < 64; n++ ) {

				CIpAddr Addr(169,254,b1,b2);

				CIpAddr Mask(255,255,0,0);
				
				if( m_pRouter->SetConfig(m_uFace, Addr, Mask) ) {

					m_fApipa = true;

					return true;
					}

				b1 = 1 + Random(s1) % 253;

				b2 = 1 + Random(s2) % 253;
				}
			}
		}

	return false;
	}

bool CDhcp::DropApipa(void)
{
	if( m_fApipa ) {

		m_pRouter->SetConfig(m_uFace, IP_EMPTY, IP_EMPTY);

		m_fApipa = false;

		return true;
		}

	return false;
	}

WORD CDhcp::Random(LONG &Seed)
{
	Seed = (Seed * 125) % 2796203;

	return LOWORD(Seed);
	}

// Diagnostics

UINT CDhcp::DiagStatus(IDiagOutput *pOut)
{
	pOut->AddPropList();

	pOut->AddProp("State",	  "%u",  m_State);
	pOut->AddProp("Apipa",    "%u",  m_fApipa);
	pOut->AddProp("Active",   "%u",  m_fActive);
	pOut->AddProp("Try",	  "%u",  m_Try);
	pOut->AddProp("Client",   "%s",  PCTXT(m_Client.GetAsText()));
	pOut->AddProp("Server",   "%s",  PCTXT(m_Server.GetAsText()));
	pOut->AddProp("Mask",	  "%s",  PCTXT(m_Mask.GetAsText  ()));
	pOut->AddProp("Gate",	  "%s",  PCTXT(m_Gate.GetAsText  ()));
	pOut->AddProp("NTP",	  "%s",  PCTXT(m_NTP.GetAsText ()));
	pOut->AddProp("SMTP",	  "%s",  PCTXT(m_SMTP.GetAsText()));
	pOut->AddProp("DNS",	  "%s",  PCTXT(m_DNS.GetAsText ()));
	pOut->AddProp("Net",      "%s",  m_Net);
	pOut->AddProp("TimeZone", "%u",  m_TimeZone);

	pOut->EndPropList();

	return 0;
	}

UINT CDhcp::DiagRelease(IDiagOutput *pOut)
{
	if( m_State == stateBound ) {

		m_Cmd = stateRelease;
		}
	else
		pOut->Error("invalid state");

	return 0;
	}

UINT CDhcp::DiagRenew(IDiagOutput *pOut)
{
	if( m_State == stateBound || (m_State == stateIdle && m_fActive) ) {

		m_Cmd = stateRenew;
		}
	else
		pOut->Error("invalid state");

	return 0;
	}

// End of File
