
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_Router(void);

extern void Revoke_Router(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxTcpIp(void)
{
	Register_Router();

	TcpDebugInit();
	}

void Revoke_HxTcpIp(void)
{
	Revoke_Router();
	}

// End of File
