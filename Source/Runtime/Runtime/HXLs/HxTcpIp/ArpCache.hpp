
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ArpCache_HPP

#define	INCLUDE_ArpCache_HPP

//////////////////////////////////////////////////////////////////////////
//
// ARP Cache
//

class CArpCache
{
	public:
		// Constructor
		CArpCache(void);

		// Destructor
		~CArpCache(void);

		// Diagnostics
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, UINT uCmd);

		// Attributes
		BOOL Find(IPREF Ip, CMacAddr &Mac);

		// Operations
		void Add(IPREF Ip, MACREF Mac);
		void Validate(MACREF Mac);
		void Poll(IPREF Ip);

	protected:
		// Cache Entry
		struct CEntry
		{
			BOOL	 m_fUsed;
			CIpAddr  m_Ip;
			CMacAddr m_Mac;
			UINT     m_Time;
			};

		// Data Members
		IMutex * m_pLock;
		CEntry   m_List[256];
		UINT     m_uLast;

		// Diagnostics
		UINT DiagFlush(IDiagOutput *pOut);
		UINT DiagShow(IDiagOutput *pOut);
	};

// End of File

#endif
