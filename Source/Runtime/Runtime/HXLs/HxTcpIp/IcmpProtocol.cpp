
#include "Intern.hpp"

#include "IcmpProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IcmpHeader.hpp"

#include "IpHeader.hpp"

#include "PseudoHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ICMP Protocol
//

// Creation Function

IProtocol * Create_ICMP(void)
{
	return New CIcmp;
	}

// Constructor

CIcmp::CIcmp(void)
{
	StdSetRef();

	m_pLock   = Create_Rutex();

	m_pRouter = NULL;

	m_pUtils  = NULL;

	m_fNotify = FALSE;

	m_uMask   = 0;

	m_wSeq    = WORD(rand());

	m_Socket.Bind(this);
	}

// Destructor

CIcmp::~CIcmp(void)
{
	AfxRelease(m_pLock);
	}

// IUnknown

HRESULT CIcmp::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IProtocol);

	StdQueryInterface(IThreadNotify);

	StdQueryInterface(IProtocol);

	return E_NOINTERFACE;
	}

ULONG CIcmp::AddRef(void)
{
	StdAddRef();
	}

ULONG CIcmp::Release(void)
{
	StdRelease();
	}

// IThreadNotify

UINT CIcmp::OnThreadCreate(IThread *pThread, UINT uIndex)
{
	CListRoot *pRoot = New CListRoot;

	pRoot->m_pHead = NULL;

	pRoot->m_pTail = NULL;

	return UINT(pRoot);
	}

void CIcmp::OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam)
{
	CListRoot *pRoot = (CListRoot *) uParam;

	if( pRoot->m_pHead ) {

		AfxTrace("tcpip: thread %u exiting with open icmp sockets\n", pThread->GetIdent());

		while( pRoot->m_pHead ) {

			CIcmpSocket *pSock = pRoot->m_pHead;

			pSock->Release();

			AfxAssert(pRoot->m_pHead != pSock);
			}
		}

	delete pRoot;
	}

// IProtocol

BOOL CIcmp::Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask)
{
	m_pRouter = pRouter;

	m_pUtils  = pUtils;

	m_uMask   = uMask;

	return TRUE;
	}

BOOL CIcmp::CreateSocket(ISocket * &pSocket)
{
	if( m_Socket.IsFree() ) {

		m_Socket.Create();

		AddSocket(&m_Socket);

		pSocket = &m_Socket;

		return TRUE;
		}

	pSocket = NULL;

	return FALSE;
	}

BOOL CIcmp::Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	UINT uSize = pBuff->GetSize();

	if( likely(uSize >= sizeof(ICMPHEADER)) ) {

		CIcmpHeader *pIcmp = (CIcmpHeader *) pBuff->GetData();

		if( likely(pBuff->TestFlag(packetSumsValid) || pIcmp->TestChecksum(uSize)) ) {

			if( likely(!Ps.m_Dest.IsBroadcast()) ) {

				pIcmp->NetToHost();

				switch( pIcmp->m_Type ) {

					case ICMP_PING:

						TcpDebug(OBJ_ICMP, LEV_TRACE, "recv ping request\n");

						pIcmp->m_Type = ICMP_PONG;

						pIcmp->HostToNet();

						pIcmp->AddChecksum(uSize);

						SendVisible(Ps.m_Src, Ps.m_Dest, Buff.TakeOver());

						break;

					case ICMP_PONG:

						TcpDebug(OBJ_ICMP, LEV_TRACE, "recv ping reply\n");

						pBuff->StripHead(sizeof(CIcmpHeader));

						m_Socket.OnRecv(Ps, Buff.TakeOver());

						break;

					default:
						// LATER -- Accept host redirects?

						TcpDebug(OBJ_ICMP, LEV_TRACE, "unknown request\n");

						break;
					}

				return TRUE;
				}

			TcpDebug(OBJ_ICMP, LEV_WARN, "icmp broadcast\n");

			return TRUE;
			}

		TcpDebug(OBJ_ICMP, LEV_WARN, "checksum invalid\n");

		return TRUE;
		}
	
	TcpDebug(OBJ_ICMP, LEV_WARN, "packet too small\n");

	return TRUE;
	}

BOOL CIcmp::Send(void)
{
	return TRUE;
	}

BOOL CIcmp::Poll(void)
{
	return TRUE;
	}

BOOL CIcmp::NetStat(IDiagOutput *pOut)
{
	m_Socket.NetStat(pOut);
		
	return TRUE;
	}

BOOL CIcmp::SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	return TRUE;
	}

// Operations

BOOL CIcmp::SendVisible(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	return m_pRouter->Queue(Dest, From, IP_ICMP, pBuff);
	}

BOOL CIcmp::SendStealth(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	return m_pRouter->Queue(Dest, From, IP_ICMP | IP_STEALTH, pBuff);
	}

BOOL CIcmp::SendPing(IPREF Dest, CBuffer *pBuff)
{
	TcpDebug(OBJ_ICMP, LEV_TRACE, "send ping request\n");

	CIcmpHeader *pIcmp = BuffAddHead(pBuff, CIcmpHeader);

	pIcmp->m_Type = ICMP_PING;

	pIcmp->m_Code = 0;

	pIcmp->m_Ping.m_Ident    = WORD(DWORD(this));

	pIcmp->m_Ping.m_Sequence = m_wSeq++;

	pIcmp->HostToNet();

	pIcmp->AddChecksum(pBuff->GetSize());

	return SendVisible(Dest, IP_EMPTY, pBuff);
	}

BOOL CIcmp::SendPing(IPREF Dest, PBYTE pData, UINT uSize)
{
	CBuffer *pBuff = BuffAllocate(uSize);

	if( likely(pBuff) ) {

		memcpy(pBuff->AddTail(uSize), pData, uSize);

		return SendPing(Dest, pBuff);
		}

	return FALSE;
	}

BOOL CIcmp::SendUnreachable(CIpHeader *pIp, BYTE bCode)
{
	if( likely(m_fNotify) ) {
		
		if( likely(!pIp->m_IpDest.IsSpecial()) ) {

			TcpDebug(OBJ_ICMP, LEV_TRACE, "send unreachable\n");

			UINT     uSize = sizeof(CIpHeader) + 8;

			CBuffer *pBuff = BuffAllocate(uSize);

			if( likely(pBuff) ) {

				pIp->HostToNet();

				memcpy(pBuff->AddTail(uSize), PBYTE(pIp), uSize);

				CIcmpHeader *pIcmp = BuffAddHead(pBuff, CIcmpHeader);

				pIcmp->m_Type = ICMP_UNREACH;

				pIcmp->m_Code = bCode;

				pIcmp->m_Empty.m_Empty = 0;

				pIcmp->HostToNet();

				pIcmp->AddChecksum(pBuff->GetSize());

				return SendStealth(pIp->m_IpSrc, pIp->m_IpDest, pBuff);
				}
			}
		}

	return FALSE;
	}

BOOL CIcmp::SendTimeExceeded(CIpHeader *pIp, BYTE bCode)
{
	if( likely(m_fNotify) ) {
		
		if( likely(!pIp->m_IpDest.IsSpecial()) ) {

			TcpDebug(OBJ_ICMP, LEV_TRACE, "send time exceeded\n");

			UINT     uSize = sizeof(CIpHeader) + 8;

			CBuffer *pBuff = BuffAllocate(uSize);

			if( likely(pBuff) ) {

				pIp->HostToNet();

				memcpy(pBuff->AddTail(uSize), PBYTE(pIp), uSize);

				CIcmpHeader *pIcmp = BuffAddHead(pBuff, CIcmpHeader);

				pIcmp->m_Type = ICMP_EXCEED;

				pIcmp->m_Code = bCode;

				pIcmp->m_Empty.m_Empty = 0;

				pIcmp->HostToNet();

				pIcmp->AddChecksum(pBuff->GetSize());

				return SendStealth(pIp->m_IpSrc, pIp->m_IpDest, pBuff);
				}
			}
		}

	return FALSE;
	}

BOOL CIcmp::SendHostRedirect(CIpHeader *pIp, IPREF Gateway)
{
	if( likely(m_fNotify) ) {
		
		if( likely(!pIp->m_IpDest.IsSpecial()) ) {

			TcpDebug(OBJ_ICMP, LEV_TRACE, "send host redirect\n");

			UINT     uSize = sizeof(CIpHeader) + 8;

			CBuffer *pBuff = BuffAllocate(uSize);

			if( likely(pBuff) ) {

				pIp->HostToNet();

				memcpy(pBuff->AddTail(uSize), PBYTE(pIp), uSize);

				CIcmpHeader *pIcmp = BuffAddHead(pBuff, CIcmpHeader);

				pIcmp->m_Type = ICMP_REDIRECT;

				pIcmp->m_Code = 1;

				pIcmp->m_Redirect.m_Ip = Gateway;

				pIcmp->HostToNet();

				pIcmp->AddChecksum(pBuff->GetSize());

				return SendStealth(pIp->m_IpSrc, pIp->m_IpDest, pBuff);
				}
			}
		}

	return FALSE;
	}

void CIcmp::FreeSocket(CIcmpSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) pSock->m_pRoot;

	AfxListRemove(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);
	}

// Implementation

void CIcmp::AddSocket(CIcmpSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) AddThreadNotify(this);

	AfxListAppend(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);

	pSock->m_pRoot = pRoot;
	}

// End of File
