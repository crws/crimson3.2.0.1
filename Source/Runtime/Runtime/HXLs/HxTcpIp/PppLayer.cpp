
#include "Intern.hpp"

#include "PppLayer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PPP Layer Base Class
//

// Constructor

CPppLayer::CPppLayer(CPpp *pPpp, WORD wProtocol)
{
	m_pPpp      = pPpp;

	m_wProtocol = wProtocol;
	}

// Actions

void CPppLayer::ThisLayerUp(void)
{
	m_pPpp->OnLayerEvent(m_wProtocol, layerUp);
	}

void CPppLayer::ThisLayerDown(void)
{
	m_pPpp->OnLayerEvent(m_wProtocol, layerDown);
	}

void CPppLayer::ThisLayerStarted(void)
{
	m_pPpp->OnLayerEvent(m_wProtocol, layerStarted);
	}

void CPppLayer::ThisLayerFinished(void)
{
	m_pPpp->OnLayerEvent(m_wProtocol, layerFinished);
	}

void CPppLayer::ThisLayerFailed(void)
{
	m_pPpp->OnLayerEvent(m_wProtocol, layerFailed);
	}

// Implementation

BOOL CPppLayer::ShowMessage(CBuffer *pBuff)
{
	return FALSE;
	}

// End of File
