
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IcmpSocket_HPP

#define	INCLUDE_IcmpSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIcmp;

//////////////////////////////////////////////////////////////////////////
//
// ICMP Socket
//

class CIcmpSocket : public ISocket
{
	public:
		// Constructor
		CIcmpSocket(void);

		// Binding
		void Bind(CIcmp *pICMP);

		// Attributes
		BOOL IsFree(void) const;

		// Operations
		void Create(void);
		void NetStat(IDiagOutput *pOut);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket Methods
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &Ip, WORD Loc);
		HRM Connect(IPADDR const &Ip, WORD Rem);
		HRM Connect(IPADDR const &Ip, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Recv(CBuffer * &pBuff);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal (IPADDR &Ip);
		HRM GetRemote(IPADDR &Ip);
		HRM GetLocal (IPADDR &Ip, WORD &Port);
		HRM GetRemote(IPADDR &Ip, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

		// Event Handlers
		BOOL OnRecv(PSREF Ps, CBuffer *pBuff);
		
		// Linked List
		PVOID         m_pRoot;
		CIcmpSocket * m_pNext;
		CIcmpSocket * m_pPrev;

	protected:
		// Data Members
		ULONG	  m_uRefs;
		CIcmp   * m_pIcmp;
		BOOL	  m_fUsed;
		BOOL      m_fOpen;
		CIpAddr	  m_Ip;
		CBuffer * m_pRxBuff;

		// Implementation
		void ClearRx(void);
	};

// End of File

#endif
