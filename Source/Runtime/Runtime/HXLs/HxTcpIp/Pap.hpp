
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Pap_HPP

#define	INCLUDE_Pap_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PppLayer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPpp;

class CLcpFrame;

//////////////////////////////////////////////////////////////////////////
//
// PAP Codes
//

enum {
	papCredentials	= 1,
	papSuccess	= 2,
	papFailure	= 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Password Authentiation Protocol
//

class CPap : public CPppLayer
{
	public:
		// Constructor
		CPap(CPpp *pPPP, CConfigPpp const &Config);

		// Destructor
		~CPap(void);

		// Management
		void LowerLayerUp(void);
		void LowerLayerDown(void);
		void OnTime(void);

		// Frame Handling
		void OnRecv(CBuffer *pBuff);

	protected:
		// Data Members
		BOOL	    m_fServer;
		char	    m_sPass[32];
		char	    m_sUser[32];
		CLcpFrame * m_pLcp;
		BYTE	    m_bID;
		UINT	    m_uCount;
		UINT	    m_uStart;
		UINT	    m_uTimer;
		UINT	    m_uRetry;

		// Implementation
		void AddText(CBuffer *pBuff, PCTXT pText);
		void PutPAP(CBuffer *pBuff, BYTE bCode, BYTE bID);
		void SendResponse(BYTE bCode);
		void SendCredentials(void);
		BOOL CheckCredentials(void);
		void StartTimer(void);
		void StopTimer(void);
	};

// End of File

#endif
