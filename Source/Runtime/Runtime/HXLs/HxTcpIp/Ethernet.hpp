
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ethernet_HPP

#define	INCLUDE_Ethernet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Clases
//

#include "ArpCache.hpp"

#include "Dhcp.hpp"

#include "PcapFilterIp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMacHeader;

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Packet Driver
//

class CEthernetDriver : public IPacketDriver,
			public IPacketCapture,
			public IClientProcess,
			public IDiagProvider
{
	public:
		// Constructor
		CEthernetDriver(CConfigEth const &Config);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		
		// IPacketDriver
		bool    Bind(IRouter *pRouter);
		bool    Bind(IPacketSink *pSink, UINT uFace);
		CString GetDeviceName(void);
		CString GetStatus(void);
		bool    SetIpAddress(IPREF Ip, IPREF Mask);
		bool	SetIpGateway(IPREF Gate);
		bool    GetMacAddress(MACADDR &Addr);
		bool    GetDhcpOption(BYTE bType, UINT &uValue);
		bool    GetSockOption(UINT uCode, UINT &uValue);
		bool    SetMulticast(IPADDR *pList, UINT uCount);
		bool    Open(void);
		bool    Close(void);
		void    Poll(void);
		bool    Send(IPREF Ip, CBuffer *pBuff);

		// IPacketCapture
		BOOL  METHOD IsCaptureRunning(void);
		PCSTR METHOD GetCaptureFilter(void);
		UINT  METHOD GetCaptureSize(void);
		BOOL  METHOD CopyCapture(PBYTE pData, UINT uSize);
		BOOL  METHOD StartCapture(PCSTR pFilter);
		void  METHOD StopCapture(void);
		void  METHOD KillCapture(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Pending Arp Request
		struct CPending
		{
			CBuffer  * m_pBuff;
			CIpAddr    m_Dest;
			UINT	   m_uLeft;
			UINT	   m_uWait;
			UINT	   m_uTime;
			CPending * m_pNext;
			CPending * m_pPrev;
			};

		// Data Members
                ULONG		 m_uRefs;
                CConfigEth       m_Config;
                IMutex         * m_pLock;
                INic           * m_pNic;
                CDhcp	       * m_pDhcp;
                bool		 m_fDhcp;
                CArpCache      * m_pArp;
                IRouter        * m_pRouter;
                IPacketSink    * m_pSink;
                UINT	         m_uFace;
                HTHREAD		 m_hThread;
                bool		 m_fActive;
                CMacAddr         m_Mac;
                CIpAddr	         m_Local;
                CPending       * m_pPendHead;
                CPending       * m_pPendTail;
                char	         m_Stat[24];
		bool		 m_fCapture;
		CPcapFilterIp	 m_CaptFilter;
		CPcapBuffer	 m_CaptBuffer;
		IDiagManager   * m_pDiag;
		UINT		 m_uProv;

                // Destructor
		~CEthernetDriver(void);

		// Task Implementation
		void WaitLink(void);
		void WorkLink(void);

		// Frame Handlers
		void HandleIp(CMacHeader *pHeader, CBuffer *pBuff);
		void HandleArp(CMacHeader *pHeader, CBuffer *pBuff);
		void HandleRaw(CBuffer *pBuff);

		// ARP Helpers
		BOOL SendArpReply(CBuffer *pBuff);
		BOOL SendArpRequest(IPREF Ip);
		BOOL AddArpEntry(IPREF Ip, MACREF Mac, BOOL fForce);
		void SetNotFound(IPREF Ip);

		// Send Helpers
		BOOL SimpleSend(IPREF Ip, CBuffer *pBuff);
		BOOL FindDest(CIpAddr &Ip, CBuffer *pBuff);
		BOOL SendFrame(MACREF Mac, WORD Type, CBuffer *pBuff);
		BOOL PadData(CBuffer *pBuff);
		BOOL IsLinkLocal(IPREF Ip);

		// Pending ARP List
		BOOL AddToPendingList(IPREF Dest, CBuffer *pBuff);
		void EmptyPendingList(void);
		BOOL ScanPendingList(IPREF Dest, MACREF Mac);
		BOOL PollPendingList(void);

		// Status
		void SetStatus(PCTXT pStatus);

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagAll(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
