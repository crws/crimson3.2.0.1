
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PcapFilterIp_HPP

#define	INCLUDE_PcapFilterIp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpHeader;

//////////////////////////////////////////////////////////////////////////
//
// Packet Capture Filter with IP Support
//

class CPcapFilterIp : public CPcapFilter
{
	public:
		// Filtering
		BOOL StoreIp(CIpHeader *pIP, BOOL fSend) const;
	};

// End of File

#endif
