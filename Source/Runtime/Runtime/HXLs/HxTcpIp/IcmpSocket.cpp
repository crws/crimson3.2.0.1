
#include "Intern.hpp"

#include "IcmpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IcmpProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ICMP Socket
//

// Constructor

CIcmpSocket::CIcmpSocket(void)
{
	StdSetRef();

	m_fUsed   = FALSE;

	m_fOpen   = FALSE;

	m_pIcmp   = NULL;

	m_pRxBuff = NULL;
	}

// Binding

void CIcmpSocket::Bind(CIcmp *pICMP)
{
	m_pIcmp = pICMP;
	}

// Attributes

BOOL CIcmpSocket::IsFree(void) const
{
	return !m_fUsed;
	}

// Operations

void CIcmpSocket::Create(void)
{
	m_fUsed = TRUE;

	AddRef();
	}

void CIcmpSocket::NetStat(IDiagOutput *pOut)
{
	if( m_fUsed ) {

		pOut->AddRow();

		pOut->SetData(0, "ICMP");
		
		pOut->SetData(1, PCTXT(CPrintf("%-5u", 0)));
		
		pOut->SetData(2, PCTXT(CPrintf("%-15s : %-5u", PCTXT(m_Ip.GetAsText()), 0)));

		pOut->SetData(3, "");

		pOut->EndRow();
		}
	}

// IUnknown

HRESULT CIcmpSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
	}

ULONG CIcmpSocket::AddRef(void)
{
	StdAddRef();
	}

ULONG CIcmpSocket::Release(void)
{
	if( m_uRefs > 1 ) {

		if( AtomicDecrement(&m_uRefs) == 1 ) {

			m_fOpen = FALSE;

			m_fUsed = FALSE;

			m_pIcmp->FreeSocket(this);

			return 0;
			}

		return m_uRefs;
		}

	AfxAssert(FALSE);

	return m_uRefs;
	}

// ISocket

HRESULT CIcmpSocket::Listen(WORD Loc)
{
	return E_FAIL;
	}

HRESULT CIcmpSocket::Listen(IPADDR const &Ip, WORD Loc)
{
	return E_FAIL;
	}

HRESULT CIcmpSocket::Connect(IPADDR const &Ip, WORD Rem)
{
	return Connect(Ip, 0, 0);
	}

HRESULT CIcmpSocket::Connect(IPADDR const &Ip, WORD Rem, WORD Loc)
{
	if( likely(!m_fOpen) ) {

		m_Ip	= Ip;

		m_fOpen = TRUE;

		return S_OK;
		}

	return E_FAIL;
	}

HRESULT CIcmpSocket::Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc)
{
	// TODO -- This should be implemented for RLOS...

	return Connect(Ip, Rem, Loc);
}

HRESULT CIcmpSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pData, uSize) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CIcmpSocket::Recv(PBYTE pData, UINT &uSize)
{
	if( likely(m_fOpen) ) {

		if( likely(m_pRxBuff) ) {

			UINT uData = m_pRxBuff->GetSize();

			UINT uCopy = Min(uSize, uData);

			memcpy(pData, m_pRxBuff->GetData(), uCopy);

			BuffRelease(m_pRxBuff);

			m_pRxBuff = NULL;

			uSize     = uCopy;

			return S_OK;
			}
		}

	return E_FAIL;
	}

HRESULT CIcmpSocket::Send(PBYTE pData, UINT &uSize)
{
	if( likely(m_fOpen) ) {

		if( m_pIcmp->SendPing(m_Ip, pData, uSize) ) {

			return S_OK;
			}
		}

	uSize = 0;

	return E_FAIL;
	}

HRESULT CIcmpSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pBuff) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CIcmpSocket::Recv(CBuffer * &pBuff)
{
	if( likely(m_fOpen) ) {

		if( likely(m_pRxBuff) ) {

			pBuff     = m_pRxBuff;

			m_pRxBuff = NULL;

			return S_OK;
			}
		}

	pBuff = NULL;

	return E_FAIL;
	}

HRESULT CIcmpSocket::Send(CBuffer *pBuff)
{
	if( likely(m_fOpen) ) {

		if( m_pIcmp->SendPing(m_Ip, pBuff) ) {

			return S_OK;
			}
		}
 
	return E_FAIL;
	}

HRESULT CIcmpSocket::GetLocal(IPADDR &Ip)
{
	return E_FAIL;
	}

HRESULT CIcmpSocket::GetRemote(IPADDR &Ip)
{
	Ip = m_Ip;

	return S_OK;
	}

HRESULT CIcmpSocket::GetLocal(IPADDR &Ip, WORD &Port)
{
	return E_FAIL;
	}

HRESULT CIcmpSocket::GetRemote(IPADDR &Ip, WORD &Port)
{
	return E_FAIL;
	}

HRESULT CIcmpSocket::GetPhase(UINT &Phase)
{
	Phase = m_fOpen ? PHASE_OPEN : PHASE_IDLE;

	return S_OK;
	}

HRESULT CIcmpSocket::SetOption(UINT uOption, UINT uValue)
{
	return E_FAIL;
	}

HRESULT CIcmpSocket::Abort(void)
{
	if( likely(m_fOpen) ) {

		CAutoLock Lock(m_pIcmp->m_pLock);

		m_fOpen = FALSE;

		ClearRx();
		}

	return S_OK;
	}

HRESULT CIcmpSocket::Close(void)
{
	if( likely(m_fOpen) ) {

		CAutoLock Lock(m_pIcmp->m_pLock);

		m_fOpen = FALSE;

		ClearRx();
		}

	return S_OK;
	}

// Event Handlers

BOOL CIcmpSocket::OnRecv(PSREF Ps, CBuffer *pBuff)
{
	CAutoLock   Lock(m_pIcmp->m_pLock);

	CAutoBuffer Buff(pBuff);

	if( likely(m_fOpen) ) {

		if( likely(!m_pRxBuff) ) {

			m_pRxBuff = Buff.TakeOver();

			return TRUE;
			}

		Lock.Free();

		TcpDebug(OBJ_ICMP, LEV_WARN, "socket rx overflow\n");

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CIcmpSocket::ClearRx(void)
{
	if( m_pRxBuff ) {

		BuffRelease(m_pRxBuff);

		m_pRxBuff = NULL;
		}
	}

// End of File
