
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ModemHspa2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modem Instantiation
//

CModem * Create_Modem(CPpp *pPpp, CConfigPpp const &Config)
{
	switch( Config.m_uConnect ) {

		case connectCellHSPA2:

			return New CModemCellHspa2(pPpp, Config);
		}

	return NULL;
	}

// End of File
