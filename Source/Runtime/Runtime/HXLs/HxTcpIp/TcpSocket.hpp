
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TcpSocket_HPP

#define	INCLUDE_TcpSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TcpHeader.hpp"

#include "PseudoHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTcp;

//////////////////////////////////////////////////////////////////////////
//
// Debug Flags
//

enum DebugFlag
{
	debugUser	= 0x01,
	debugData	= 0x02,
	debugState	= 0x04,
	debugSend	= 0x08,
	debugRept	= 0x10,
	debugRecv	= 0x20,
	debugNone	= 0x00,
	debugAll	= 0xFF,
	};

//////////////////////////////////////////////////////////////////////////
//
// Sequence Macros
//

#define	LT(a, b)		(LONG(DWORD(a) - DWORD(b)) <  0)
#define	GT(a, b)		(LONG(DWORD(a) - DWORD(b)) >  0)

#define	LTE(a, b)		(LONG(DWORD(a) - DWORD(b)) <= 0)
#define	GTE(a, b)		(LONG(DWORD(a) - DWORD(b)) >= 0)

//////////////////////////////////////////////////////////////////////////
//
// Socket States
//

enum SockState
{
	stateFree	=  0,
	stateInit	=  1,
	stateClosed	=  2,
	stateListen	=  3,
	stateSynSent	=  4,
	stateSynRcvd	=  5,
	stateEstab	=  6,
	stateFinWait1	=  7,
	stateFinWait2	=  8,
	stateClosing	=  9,
	stateCloseWait	= 10,
	stateLastAck	= 11,
	stateLinger	= 12,
	stateTimeWait	= 13,
	stateError	= 14,
	};

//////////////////////////////////////////////////////////////////////////
//
// TCP Socket
//

class CTcpSocket : public ISocket
{
	public:
		// Constructor
		CTcpSocket(void);

		// Destructor
		~CTcpSocket(void);

		// Binding
		void Bind(CTcp *pTcp, UINT uIndex);

		// Attributes
		BOOL IsFree(void) const;
		BOOL IsWaiting(void) const;
		UINT GetWait(void) const;

		// Operations
		void Create(void);
		BOOL EndWait(void);
		BOOL EndClose(void);
		void NetStat(IDiagOutput *pOut);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket Methods
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &IP, WORD Loc);
		HRM Connect(IPADDR const &IP, WORD Rem);
		HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Recv(CBuffer * &pBuff);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal (IPADDR &IP);
		HRM GetRemote(IPADDR &IP);
		HRM GetLocal (IPADDR &IP, WORD &Port);
		HRM GetRemote(IPADDR &IP, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

		// Event Handlers
		BOOL OnTest(PSREF PS, CTcpHeader *pTcp, BOOL &fHit);
		BOOL OnRecv(PSREF PS, CTcpHeader *pTcp, CBuffer *pBuff);
		BOOL OnSend(void);
		BOOL OnPoll(void);

		// Linked List
		PVOID	     m_pRoot;
		CTcpSocket * m_pNext;
		CTcpSocket * m_pPrev;

	protected:
		// Retry Entry
		struct CRetry
		{
			CBuffer	* m_pBuff;
			DWORD	  m_Last;
			UINT	  m_Stamp;
			UINT	  m_Timer;
			UINT	  m_Count;
			BOOL	  m_fAck;
			};

		// Static Data
		static UINT m_NextPort;

		// Initial Data
		ULONG     m_uRefs;
		CTcp    * m_pTcp;
		UINT	  m_uIndex;
		UINT	  m_uLast;

		// Socket State
		UINT      m_uState;
		CIpAddr   m_LocIP;
		CIpAddr   m_RemIP;
		WORD      m_LocPort;
		WORD      m_RemPort;
		UINT	  m_KillTime;
		UINT	  m_SoonTime;
		UINT	  m_TranTime;
		UINT	  m_PingTime;
		UINT	  m_PingSent;
		UINT	  m_PongTime;

		// Tuning Data
		UINT      m_TripDamp;
		UINT	  m_TripTime;
		UINT	  m_RetryTime;
		UINT	  m_RetryCount;
		UINT	  m_KillDelay;
		UINT	  m_TranDelay;
		UINT	  m_AckDelay;
		UINT      m_PingDelay;
		UINT      m_PongDelay;

		// Send Queue
		CBuffer * m_pTxBuff;
		UINT	  m_uLocked;
		UINT	  m_uRetry;
		CRetry    m_Retry[4];
		UINT	  m_fGotACK:1;
		UINT	  m_fReqACK:1;
		UINT	  m_fReqSYN:1;
		UINT	  m_fReqFIN:1;
		UINT	  m_fReqRST:1;
		UINT	  m_fReqSend:1;
		UINT	  m_fReqSoon:1;
		UINT	  m_fReqPing:1;

		// Socket State
		UINT      m_fActive:1;
		UINT	  m_fReqClose:1;
		UINT	  m_fReqFree:1;

		// Socket Config
		UINT	  m_fLinger:1;
		UINT	  m_fKeepAlive:1;
		UINT	  m_fNagle:1;

		// Send Data
		WORD	  m_SendMax;
		WORD	  m_SendMSS;
		DWORD	  m_SendISN;
		DWORD	  m_SendUna;
		DWORD	  m_SendNxt;
		WORD	  m_SendWnd;
		DWORD	  m_SendWL1;
		DWORD	  m_SendWL2;

		// Recv Queue
		CBuffer * m_pRxBuff;

		// Recv Data
		WORD	  m_RecvMSS;
		DWORD	  m_RecvISN;
		DWORD	  m_RecvNxt;
		DWORD	  m_RecvMax;
		WORD	  m_RecvWnd;

		// Pend List
		CBuffer * m_pPendBuff[8];
		INT	  m_nPendStep[8];
		UINT      m_uPendCount;
		UINT      m_uPendLimit;

		// Implementation
		void InitData(void);
		void DefaultTuning(void);
		void LocalTuning(void);
		void ResetListen(void);
		void StartKillTimer(void);
		void StartTranTimer(UINT n);
		void StartPingTimer(void);
		void StartPongTimer(void);
		void StopKillTimer(void);
		void StopTranTimer(void);
		void SetState(UINT uState);
		void CheckRelease(void);
		void CheckFree(void);
		void CheckClose(void);
		void MakeClosed(void);
		void FreeRxBuff(void);
		void FreeTxBuff(void);
		void CalcTripTime(UINT Trip);
		UINT Limit(UINT a, UINT b, UINT c);
		BOOL IsAlive(void);
		BOOL IsPingable(void);

		// Queue Control
		void UpdateSendRequest(void);
		BOOL IsDataAvailable(void);
		BOOL CanSendSegment(void);
		BOOL IsSmallSegment(void);
		void DelayedAck(void);
		void InstantAck(void);
		void RequestPing(void);

		// Segment Queueing
		BOOL CanQueueSegment(void);
		void QueueSegment(CTcpHeader *pTcp, CBuffer *pBuff);
		BOOL QueueData(PBYTE pData, UINT &uSize);
		BOOL QueueData(CBuffer *pBuff, UINT uSize);

		// Receive State Machine
		BOOL TakeSegment(PSREF PS, CTcpHeader *pTcp);
		void RecvInListen(PSREF PS, CTcpHeader *pTcp, CBuffer *pBuff);
		void RecvInSynSent(PSREF PS, CTcpHeader *pTcp, CBuffer *pBuff);
		void RecvInOther(CTcpHeader *pTcp, CBuffer *pBuff);
		void HandleSeg(CTcpHeader *pTcp, CBuffer *pBuff);
		BOOL HandleAck(CTcpHeader *pTcp, CBuffer *pBuff);

		// Window Management
		void ReadSendWindow(CTcpHeader *pTcp);
		void CalcRecvWindow(void);
		UINT GetSendLimit(void);

		// Pending Segments
		BOOL IsPending(CTcpHeader *pTcp);
		BOOL AddPending(CTcpHeader *pTcp, CBuffer *pBuff);
		BOOL HasPending(CTcpHeader * &pTcp);
		void GetPending(CBuffer * &pBuff);
		void PurgePendingList(void);
		void FreePendingList(void);

		// Retry Management
		void LockRetry(void);
		void FreeRetry(void);
		void InitRetryList(void);
		void FreeRetryList(void);
		void PollRetryList(UINT uGone);
		void ScanRetryList(void);
		BOOL IsRetryNeeded(void);
		BOOL IsRetryExhausted(void);
		BOOL IsRetryListFull(void);
		void AddRetry(CBuffer *  pBuff);
		BOOL GetRetry(CBuffer * &pBuff);

		// Segment Testing
		BOOL IsAcceptable(CTcpHeader *pTcp);
		BOOL InRecvWindow(DWORD Seq);
		BOOL IsZeroWindowSYN(WORD Window);

		// State Text
		PCTXT GetStateText(UINT uState);

		// Debugging
		void Debug(UINT Mask, PCTXT pTag, PCTXT pFormat, ...);
		void Debug(UINT Mask, PCTXT pTag, CTcpHeader *pTcp);
		void Debug(PCTXT pTag);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE BOOL CTcpSocket::OnTest(PSREF PS, CTcpHeader *pTcp, BOOL &fHit)
{
	if( likely(m_uState == stateFree || m_uState == stateInit) ) {

		return FALSE;
		}

	if( unlikely(pTcp->m_RemPort == m_LocPort) ) {

		if( m_RemIP.IsEmpty() || m_RemIP == PS.m_Src ) {

			if( m_RemPort ) {

				if( pTcp->m_LocPort == m_RemPort ) {

					return TRUE;
					}
				}
			else {
				if( pTcp->m_FlagSYN ) {

					return TRUE;
					}
				}
			}

		fHit = TRUE;
		}

	return FALSE;
	}

// End of File

#endif
