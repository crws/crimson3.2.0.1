
#include "Intern.hpp"

#include "IcmpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// ICMP Header Wrapper Class
//

// Attributes

BOOL CIcmpHeader::TestChecksum(UINT uSize) const
{
	return !CalcChecksum(uSize);
	}

// Operations

void CIcmpHeader::AddChecksum(UINT uSize)
{
	m_Checksum = 0;

	m_Checksum = CalcChecksum(uSize);
	}

// Implementation

WORD CIcmpHeader::CalcChecksum(UINT uSize) const
{
	return Checksum(PBYTE(this), uSize);
	}

// End of File
