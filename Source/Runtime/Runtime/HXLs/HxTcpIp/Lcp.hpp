
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Lcp_HPP

#define	INCLUDE_Lcp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PppNegotiate.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPpp;

//////////////////////////////////////////////////////////////////////////
//
// LCP Options
//

enum
{
	optMaximumReceive	= 1,
	optAsyncCharMap		= 2,
	optAuthProtocol		= 3,
	optQualityProtocol	= 4,
	optMagicNumber		= 5,
	optProtComp		= 7,
	optAddrComp		= 8,
	};

//////////////////////////////////////////////////////////////////////////
//
// LCP Option -- Character Map
//

#pragma pack(1)

struct OPTMAP
{
	BYTE	m_bType;
	BYTE	m_bSize;
	DWORD	m_dwMap;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// LCP Option -- Magic Number
//

#pragma pack(1)

struct OPTMAGIC
{
	BYTE	m_bType;
	BYTE	m_bSize;
	DWORD	m_dwMagic;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// LCP Option -- MRU
//

#pragma pack(1)

struct OPTMRU
{
	BYTE	m_bType;
	BYTE	m_bSize;
	WORD	m_wMRU;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// LCP Option -- Authentication
//

#pragma pack(1)

struct OPTAUTH
{

	BYTE	m_bType;
	BYTE	m_bSize;
	WORD	m_wProtocol;
	BYTE	m_bData[];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// LCP Option -- CHAP
//

#pragma pack(1)

struct OPTCHAP
{

	BYTE	m_bType;
	BYTE	m_bSize;
	WORD	m_wProtocol;
	BYTE	m_bMethod;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// PPP Link Control Protocol
//

class CLcp : public CPppNegotiate
{
	public:
		// Constructor
		CLcp(CPpp *pPPP, CConfigPpp const &Config);

		// Destructor
		~CLcp(void);

		// Attributes
		UINT GetCharMap(void) const;
		WORD GetAuthProtocol(void) const;
		BOOL GetProtComp(void) const;
		BOOL GetAddrComp(void) const;

		// Operations
		void ProtocolReject(WORD wProtocol, CBuffer *pBuff);

	protected:
		// Data Members
		BOOL  m_fServer;
		BOOL  m_fCHAP;
		WORD  m_wRemMRU;
		WORD  m_wRemAuth;
		UINT  m_uRemMap;
		BOOL  m_fRemProtComp;
		BOOL  m_fRemAddrComp;
		WORD  m_wLocAuth;
		BOOL  m_fLocProtComp;
		BOOL  m_fLocAddrComp;
		DWORD m_dwMagic;

		// Remote Options
		BOOL RemoteReject (BYTE   bType, PBYTE pData);
		BOOL RemoteNak    (BYTE   bType, PBYTE pData);
		void RemoteAgreed (PCBYTE pData, UINT  uSize);
		void RemoteDefault(void);

		// Local Options
		void LocalDefault(void);
		void LocalRequest(CBuffer *pBuff);
		BOOL LocalReject (BYTE bType, PBYTE pData);
		BOOL LocalNak    (BYTE bType, PBYTE pData);
		void LocalAgreed (void);

		// Option Building
		void AddOptMagicNumber (CBuffer *pBuff);
		void AddOptCharMap     (CBuffer *pBuff);
		void AddOptAuthProtocol(CBuffer *pBuff);
		void AddOptProtComp    (CBuffer *pBuff);
		void AddOptAddrComp    (CBuffer *pBuff);

		// Debugging
		BOOL ShowRemote(void);
		BOOL ShowLocal(void);
	};

// End of File

#endif
