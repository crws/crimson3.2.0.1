
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UdpProtocol_HPP

#define	INCLUDE_UdpProtocol_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UdpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// UDP Protocol
//

class CUdp : public IThreadNotify, public IProtocol
{
	public:
		// Constructor
		CUdp(void);

		// Destructor
		~CUdp(void);

		// Attributes
		BOOL AcceptIP(IPREF Ip) const;

		// Operations
		void Enable(WORD wPort);
		BOOL Send(IPREF Dest, IPREF From, CBuffer *pBuff);
		void SendReq(BOOL fState);
		void EnableMulticast(IPREF Ip, BOOL fEnable);
		void FreeSocket(CUdpSocket *pSock);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IThreadNotify
		UINT METHOD OnThreadCreate(IThread *pThread, UINT uIndex);
		void METHOD OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam);

		// IProtocol
		BOOL Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask);
		BOOL CreateSocket(ISocket * &pSocket);
		BOOL Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff);
		BOOL Send(void);
		BOOL Poll(void);
		BOOL NetStat(IDiagOutput *pOut);
		BOOL SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff);

		// Public Data
		IMutex * m_pLock;

	protected:
		// List Root
		struct CListRoot
		{
			CUdpSocket * m_pHead;
			CUdpSocket * m_pTail;
			};

		// Data Members
		ULONG	        m_uRefs;
		DWORD	        m_Used[2048];
		IRouter	      * m_pRouter;
		INetUtilities * m_pUtils;
		UINT	        m_uMask;
		CUdpSocket      m_Sock[64];
		UINT	        m_uSend;

		// Implementation
		void BindSockets(void);
		BOOL IsPortUsed(WORD wPort);
		void AddSocket(CUdpSocket *pSock);
	};

// End of File

#endif
