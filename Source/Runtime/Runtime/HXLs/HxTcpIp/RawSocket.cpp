\
#include "Intern.hpp"

#include "RawSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack Implementation
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "RawProtocol.hpp"

#include "MacHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw Socket
//

// Static Data

UINT CRawSocket::m_NextPort = NOTHING;

// Constructor

CRawSocket::CRawSocket(void)
{
	StdSetRef();

	m_fUsed   = FALSE;

	m_fOpen   = FALSE;

	m_pRaw    = NULL;

	m_pTxBuff = NULL;

	m_uRxHead = 0;

	m_uRxTail = 0;
	}

// Binding

void CRawSocket::Bind(CRaw *pRaw)
{
	m_pRaw = pRaw;
	}

// Attributes

BOOL CRawSocket::IsFree(void) const
{
	return !m_fUsed;
	}

// Operations

void CRawSocket::Create(void)
{
	m_fUsed = TRUE;

	AddRef();
	}

void CRawSocket::NetStat(IDiagOutput *pOut)
{
	if( m_fUsed ) {

		pOut->AddRow();

		pOut->SetData(0, "Raw");
		
		pOut->SetData(1, PCTXT(CPrintf("%-15s : %-5u", PCTXT(CIpAddr(CIpAddr::m_Exper).GetAsText()), m_wMatch)));
		
		pOut->SetData(2, PCTXT(CPrintf("%-15s : %-5u", PCTXT(CIpAddr(CIpAddr::m_Exper).GetAsText()), 0)));
		
		pOut->SetData(3, "");

		pOut->EndRow();
		}
	}

// IUnknown

HRESULT CRawSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
	}

ULONG CRawSocket::AddRef(void)
{
	StdAddRef();
	}

ULONG CRawSocket::Release(void)
{
	if( m_uRefs > 1 ) {

		if( AtomicDecrement(&m_uRefs) == 1 ) {

			WaitTx();

			Abort();

			m_pRaw->FreeSocket(this);

			m_fUsed = FALSE;

			return 1;
			}

		return m_uRefs;
		}

	AfxAssert(FALSE);

	return m_uRefs;
	}

// ISocket

HRM CRawSocket::Listen(WORD Loc)
{
	if( likely(!m_fOpen) ) {

		m_pRaw->Enable();

		m_Multi.MakeEmpty();

		m_wMatch = Loc;

		m_fOpen  = TRUE;

		return S_OK;
		}

	return E_FAIL;
	}

HRM CRawSocket::Listen(IPADDR const &Ip, WORD Loc)
{
	if( Listen(Loc) == S_OK ) {

		if( IPREF(Ip).IsMulticast() ) {

			m_pRaw->EnableMulticast(Ip, TRUE);

			m_Multi = Ip;
			}

		return S_OK;
		}

	return E_FAIL;
	}

HRM CRawSocket::Connect(IPADDR const &Ip, WORD Rem)
{
	return Listen(Ip, Rem);
	}

HRM CRawSocket::Connect(IPADDR const &Ip, WORD Rem, WORD Loc)
{
	return E_FAIL;
	}

HRESULT CRawSocket::Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc)
{
	return E_FAIL;
}

HRESULT CRawSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pData, uSize) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	uSize = 0;

	return E_FAIL;
}

HRM CRawSocket::Recv(PBYTE pData, UINT &uSize)
{
	if( likely(m_fOpen) ) {

		if( likely(m_uRxHead != m_uRxTail) ) {

			CBuffer *pBuff = m_pRxBuff[m_uRxHead];

			m_uRxHead      = (m_uRxHead + 1) % elements(m_pRxBuff);

			UINT     uData = pBuff->GetSize();

			UINT	 uCopy = min(uSize, uData);

			memcpy(pData, pBuff->GetData(), uCopy);

			BuffRelease(pBuff);

			uSize = uCopy;

			return S_OK;
			}
		}

	uSize = 0;

	return E_FAIL;
	}

HRM CRawSocket::Send(PBYTE pData, UINT &uSize)
{
	if( likely(m_fOpen) ) {

		for( UINT p = 0; p < 2; p++ ) {

			if( likely(!m_pTxBuff) ) {
		
				CBuffer *pBuff = BuffAllocate(uSize);

				if( likely(pBuff) ) {

					pBuff->AddTail(uSize);

					memcpy(pBuff->GetData(), pData, uSize);

					CAutoLock Lock(m_pRaw->m_pLock);

					m_pTxBuff = pBuff;

					m_pRaw->SendReq(TRUE);

					return S_OK;
					}

				TcpDebug(OBJ_RAW, LEV_WARN, "cannot allocate buffer\n");

				uSize = 0;

				return E_FAIL;
				}

			Sleep(5);
			}

		TcpDebug(OBJ_RAW, LEV_WARN, "socket tx overflow\n");
		}

	uSize = 0;

	return E_FAIL;
	}

HRM CRawSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pBuff) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRM CRawSocket::Recv(CBuffer * &pBuff)
{
	if( likely(m_fOpen) ) {

		if( likely(m_uRxHead != m_uRxTail) ) {

			pBuff     = m_pRxBuff[m_uRxHead];

			m_uRxHead = (m_uRxHead + 1) % elements(m_pRxBuff);

			return S_OK;
			}
		}

	pBuff = NULL;

	return E_FAIL;
	}

HRM CRawSocket::Send(CBuffer *pBuff)
{
	if( likely(m_fOpen) ) {

		for( UINT p = 0; p < 2; p++ ) {

			if( likely(!m_pTxBuff) ) {
		
				CAutoLock Lock(m_pRaw->m_pLock);

				m_pTxBuff = pBuff;

				m_pRaw->SendReq(TRUE);

				return S_OK;
				}

			Sleep(5);
			}

		TcpDebug(OBJ_RAW, LEV_WARN, "socket tx overflow\n");
		}

	return E_FAIL;
	}

HRM CRawSocket::GetLocal(IPADDR &Ip)
{
	Ip = IP_EXPER;

	return S_OK;
	}

HRM CRawSocket::GetRemote(IPADDR &Ip)
{
	Ip = IP_EXPER;

	return S_OK;
	}

HRM CRawSocket::GetLocal(IPADDR &Ip, WORD &Port)
{
	Ip   = IP_EXPER;

	Port = m_wMatch;

	return S_OK;
	}

HRM CRawSocket::GetRemote(IPADDR &Ip, WORD &Port)
{
	Ip   = IP_EXPER;

	Port = 0;

	return S_OK;
	}

HRM CRawSocket::GetPhase(UINT &Phase)
{
	Phase = m_fOpen ? PHASE_OPEN : PHASE_IDLE;

	return S_OK;
	}

HRM CRawSocket::SetOption(UINT uOption, UINT uValue)
{
	return E_FAIL;
	}

HRM CRawSocket::Abort(void)
{
	if( likely(m_fOpen) ) {

		if( m_Multi.IsMulticast() ) {

			m_pRaw->EnableMulticast(m_Multi, FALSE);
			}

		CAutoLock Lock(m_pRaw->m_pLock);

		m_fOpen = FALSE;

		ClearRx();

		ClearTx();
		}

	return S_OK;
	}

HRM CRawSocket::Close(void)
{
	if( likely(m_fOpen) ) {

		if( m_Multi.IsMulticast() ) {

			m_pRaw->EnableMulticast(m_Multi, FALSE);
			}

		CAutoLock Lock(m_pRaw->m_pLock);

		m_fOpen = FALSE;

		ClearRx();
		}

	return S_OK;
	}

// Event Handlers

BOOL CRawSocket::OnRecv(CBuffer *pBuff)
{
	// Port matching already done by OnTest.

	CAutoLock   Lock(m_pRaw->m_pLock);

	CAutoBuffer Buff(pBuff);

	if( likely(m_fOpen) ) {

		UINT uNext = (m_uRxTail + 1) % elements(m_pRxBuff);

		if( likely(uNext != m_uRxHead) ) {

			m_pRxBuff[m_uRxTail] = Buff.TakeOver();

			m_uRxTail            = uNext;

			return TRUE;
			}

		Lock.Free();

		TcpDebug(OBJ_RAW, LEV_WARN, "socket rx overflow\n");

		return FALSE;
		}

	return FALSE;
	}

BOOL CRawSocket::OnSend(void)
{
	if( m_pTxBuff ) {

		m_pRaw->Send(this, m_pTxBuff);

		CAutoLock Lock(m_pRaw->m_pLock);

		m_pTxBuff = NULL;

		m_pRaw->SendReq(FALSE);

		return TRUE;
		}

	return TRUE;
	}

// Implementation

void CRawSocket::ClearRx(void)
{
	while( m_uRxHead != m_uRxTail ) {

		CBuffer *pBuff = m_pRxBuff[m_uRxHead];

		m_uRxHead      = (m_uRxHead + 1) % elements(m_pRxBuff);

		BuffRelease(pBuff);
		}
	}

void CRawSocket::ClearTx(void)
{
	if( m_pTxBuff ) {

		BuffRelease(m_pTxBuff);

		m_pTxBuff = NULL;

		m_pRaw->SendReq(FALSE);
		}
	}

void CRawSocket::WaitTx(void)
{
	while( m_pTxBuff ) {
		
		Sleep(10);
		}
	}

// End of File
