
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "StdEnv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

extern IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//
// Stack Headers
//

#include <opcua.h>

#include <opcua_builtintypes.h>

//////////////////////////////////////////////////////////////////////////
//
// Identifiers
//

#include "OpcIds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define OpcAlloc(n, t)		((t *) (n ? OpcUa_Memory_Alloc((n) * sizeof(t)) : NULL))

#define OpcReAlloc(p, n, t)	((t *) OpcUa_Memory_ReAlloc(p, (n) * sizeof(t)))
	
//////////////////////////////////////////////////////////////////////////
//
// Crimson Type System
//

typedef int   C3INT;

typedef float C3REAL;

enum
{
	typeVoid    = 0,
	typeInteger = 1,
	typeReal    = 2,
	typeString  = 3
	};

inline C3REAL I2R(DWORD d)
{
	return *((C3REAL *) &d);
	}

inline DWORD R2I(C3REAL r)
{
	return *((DWORD *) &r);
	}

// End of File

#endif
