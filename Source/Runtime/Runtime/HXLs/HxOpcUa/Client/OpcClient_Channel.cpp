
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Channel Management

BOOL COpcClient::CreateChannel(CDevice *pDevice)
{
	AfxTrace("opc: createchannel\n");

	OpcUa_Channel_Create(&pDevice->m_hChannel, OpcUa_Channel_SerializerType_Binary);

	OpcUa_String ReqPolicy;

	OpcUa_String_Initialize(&ReqPolicy);

	OpcUa_String_AttachReadOnly(&ReqPolicy, OpcUa_SecurityPolicy_None);

	try {
		OpcUa_StatusCode Code = OpcUa_Channel_Connect(	pDevice->m_hChannel,
								PTXT(PCTXT(pDevice->m_Uri)),
								ChannelCallback,
								pDevice,
								&m_Certificate,
								&m_PrivateKey,
								&m_Certificate,
								&m_PkiConfig,
								&ReqPolicy,
								60 * 30 * 1000,
								OpcUa_MessageSecurityMode_None,
								60 * 1000
								);

		OpcUa_String_Clear(&ReqPolicy);

		if( Code == OpcUa_Good ) {

			return TRUE;
			}
		}

	catch(CExecCancel const &)
	{
		OpcUa_Channel_Delete(&pDevice->m_hChannel);

		throw;
		}

	OpcUa_Channel_Delete(&pDevice->m_hChannel);

	return FALSE;
	}

BOOL COpcClient::DeleteChannel(CDevice *pDevice)
{
	if( pDevice->m_hChannel ) {
	
		AfxTrace("opc: deletechannel\n");

		OpcUa_Channel_Delete(&pDevice->m_hChannel);
		}

	return TRUE;
	}

void COpcClient::ChannelEvent(CDevice *pDevice, OpcUa_Channel_Event eEvent, OpcUa_StatusCode uStatus)
{
	if( eEvent == eOpcUa_Channel_Event_Disconnected ) {

		AfxTrace("opc: disconnected\n");

		pDevice->m_fBroken = TRUE;
		}
	}

// Channel Callback

OpcUa_StatusCode COpcClient::ChannelCallback( OpcUa_Channel       hChannel,
					      OpcUa_Void        * pCallbackData,
					      OpcUa_Channel_Event eEvent,
					      OpcUa_StatusCode    uStatus
					      )
{
	CDevice *pDevice = (CDevice *) pCallbackData;

	pDevice->m_pClient->ChannelEvent(pDevice, eEvent, uStatus);

	return OpcUa_Good;
	}

// End of File
