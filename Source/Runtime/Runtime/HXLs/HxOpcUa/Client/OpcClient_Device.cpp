
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Device Management

INDEX COpcClient::OpenDevice(PCTXT pName, PCTXT pUser, PCTXT pPass)
{
	CAutoPointer<CDevice> pDevice(New CDevice);

	if( InitDevice(pDevice) ) {

		pDevice->m_Uri   = pName;

		pDevice->m_Short = pDevice->m_Uri.Mid(pDevice->m_Uri.Find(':') + 3);

		pDevice->m_User  = pUser;

		pDevice->m_Pass  = pPass;

		return m_Devices.Append(pDevice.TakeOver());
		}

	return NULL;
	}

BOOL COpcClient::CloseDevice(INDEX Index)
{
	CAutoGuard Guard;

	if( Index ) {

		CDevice *pDevice = m_Devices[Index];

		if( pDevice->m_hChannel ) {

			if( pDevice->m_fSession ) {

				CloseSession(pDevice);
				}

			DeleteChannel(pDevice);
			}

		m_Devices.Remove(Index);

		FreeDevice(pDevice);

		return TRUE;
		}

	return FALSE;
	}

BOOL COpcClient::CheckDevice(CDevice *pDevice, BOOL &fBusy)
{
	if( pDevice->m_uState == stateCreateChannel ) {

		if( !pDevice->m_fBroken ) {

			if( CreateChannel(pDevice) ) {

				pDevice->m_uState = stateFindServers;

				fBusy = TRUE;

				return FALSE;
				}
			}

		pDevice->m_fBroken = FALSE;

		return FALSE;
		}

	if( pDevice->m_uState == stateFindServers ) {

		if( !pDevice->m_fBroken ) {

			if( pDevice->m_nServers || FindServers(pDevice) ) {

				pDevice->m_uState = stateCreateSession;

				fBusy = TRUE;

				return FALSE;
				}
			}

		DeleteChannel(pDevice);

		pDevice->m_uState  = stateCreateChannel;

		pDevice->m_fBroken = FALSE;

		return FALSE;
		}

	if( pDevice->m_uState == stateCreateSession ) {

		if( !pDevice->m_fBroken ) {

			if( pDevice->m_fSession || CreateSession(pDevice) ) {

				pDevice->m_uState = stateActivateSession;

				fBusy = TRUE;

				return FALSE;
				}
			}

		FreeServers(pDevice);

		DeleteChannel(pDevice);

		pDevice->m_uState  = stateCreateChannel;

		pDevice->m_fBroken = FALSE;

		return FALSE;
		}

	if( pDevice->m_uState == stateActivateSession ) {

		if( !pDevice->m_fBroken ) {

			BOOL fWrong = FALSE;

			if( ActivateSession(pDevice, fWrong) ) {

				pDevice->m_uState = stateConnected;

				AfxTrace("opc: %s connected\n", PCTXT(pDevice->m_Short));

				fBusy = TRUE;

				return TRUE;
				}

			if( fWrong ) {

				DeleteSession(pDevice);

				pDevice->m_uState = stateCreateSession;

				fBusy = TRUE;

				return FALSE;
				}
			}

		DeleteSession(pDevice);

		FreeServers(pDevice);

		DeleteChannel(pDevice);

		pDevice->m_uState  = stateCreateChannel;

		pDevice->m_fBroken = FALSE;

		return FALSE;
		}

	if( pDevice->m_uState == stateConnected ) {

		if( pDevice->m_fBroken ) {

			DeleteChannel(pDevice);

			pDevice->m_uState  = stateCreateChannel;

			pDevice->m_fBroken = FALSE;

			fBusy = TRUE;

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL COpcClient::InitDevice(CDevice *pDevice)
{
	AfxTrace("opc: initdevice\n");

	pDevice->m_pClient       = this;

	pDevice->m_uState        = stateCreateChannel;

	pDevice->m_fBroken       = FALSE;

	pDevice->m_RequestHandle = 0;

	pDevice->m_hChannel      = NULL;
	
	pDevice->m_nServers      = 0;
	
	pDevice->m_pServers      = NULL;

	pDevice->m_fSession      = FALSE;

	OpcUa_NodeId_Initialize(&pDevice->m_AuthenticationToken);

	OpcUa_ByteString_Initialize(&pDevice->m_ServerNonce);

	OpcUa_ByteString_Initialize(&pDevice->m_ServerCertificate);

	pDevice->m_NoOfServerEndpoints = 0;

	pDevice->m_pServerEndpoints    = NULL;

	pDevice->m_pEndpoint           = NULL;

	return TRUE;
	}

BOOL COpcClient::FreeDevice(CDevice *pDevice)
{
	AfxTrace("opc: freedevice\n");

	FreeServers(pDevice);

	DeleteSession(pDevice);

	delete pDevice;

	return TRUE;
	}

BOOL COpcClient::CheckResponse(CDevice *pDevice, OpcUa_StatusCode &Code, OpcUa_RequestHeader &Req, OpcUa_ResponseHeader &Res)
{
	if( Code == OpcUa_Good ) {

		if( Res.RequestHandle != Req.RequestHandle ) {

			Code = OpcUa_Bad;
			}
		else {
			if( Res.ServiceResult ) {

				Code = Res.ServiceResult;
				}
			}
		}

	if( Code == OpcUa_BadTimeout || Code == OpcUa_BadCommunicationError ) {

		AfxTrace("opc: %s comms failed\n", PCTXT(pDevice->m_Short));

		pDevice->m_fBroken = TRUE;
		}

	return Code == OpcUa_Good;
	}

BOOL COpcClient::FillRequestHeader(OpcUa_RequestHeader *pHeader, CDevice *pDevice)
{
	OpcUa_RequestHeader_Initialize(pHeader);

	pHeader->Timestamp   = OpcUa_DateTime_UtcNow();

	pHeader->TimeoutHint = 15 * 1000;

	if( pDevice ) {

		pHeader->RequestHandle = ++pDevice->m_RequestHandle;

		CopyNodeId(&pHeader->AuthenticationToken, &pDevice->m_AuthenticationToken);
		}

	return TRUE;
	}

// End of File
