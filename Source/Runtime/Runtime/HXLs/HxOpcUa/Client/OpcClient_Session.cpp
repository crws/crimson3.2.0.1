
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Session Management

BOOL COpcClient::CreateSession(CDevice *pDevice)
{
	AfxTrace("opc: %s createsession\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, NULL);

	OpcUa_ApplicationDescription ClientDescription;

	FillClientData(&ClientDescription);

	OpcUa_ApplicationDescription *pServer = pDevice->m_pServers;

	OpcUa_String ServerUri;
	
	OpcUa_String EndpointUrl;
	
	OpcUa_String SessionName;

	OpcUa_String_Initialize(&ServerUri);

	OpcUa_String_Initialize(&EndpointUrl);

	OpcUa_String_Initialize(&SessionName);

	OpcUa_String_AttachCopy(&ServerUri,   pServer->ApplicationUri.strContent);
	
	OpcUa_String_AttachCopy(&EndpointUrl, pDevice->m_Uri);
	
	OpcUa_String_AttachCopy(&SessionName, CPrintf("Aeon-Session-%8.8X", pDevice));

	OpcUa_ByteString ClientNonce;

	FillClientNonce(&ClientNonce);

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_NodeId			  SessionId;

	OpcUa_Double			  RevisedSessionTimeout = 0;

	OpcUa_Int32			  NoOfServerSoftwareCertificates = 0;

	OpcUa_SignedSoftwareCertificate * pServerSoftwareCertificates    = NULL;

	OpcUa_SignatureData		  ServerSignature;

	OpcUa_UInt32			  MaxRequestMessageSize;

	OpcUa_NodeId_Initialize(&SessionId);

	OpcUa_SignatureData_Initialize(&ServerSignature);

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_CreateSession(	pDevice->m_hChannel,
									&RequestHeader,
									&ClientDescription,
									&ServerUri,
									&EndpointUrl,
									&SessionName,
									&ClientNonce,
									&m_Certificate,
									60 * 1000,
									64 * 1024,
									&ResponseHeader,
									&SessionId,
									&pDevice->m_AuthenticationToken,
									&RevisedSessionTimeout,
									&pDevice->m_ServerNonce,
									&pDevice->m_ServerCertificate,
									&pDevice->m_NoOfServerEndpoints,
									&pDevice->m_pServerEndpoints,
									&NoOfServerSoftwareCertificates,
									&pServerSoftwareCertificates,
									&ServerSignature,
									&MaxRequestMessageSize
									);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

			// We need to find an endpoint description that matches the URI we used
			// and that has an approprtiate security mode. We make two passes through
			// the list, relaxing the matching criteria on the second attempt.

			for( int p = 0; p < 2; p++ ) {

				CString Find = pDevice->m_Uri;

				if( p == 1 ) {

					// On the second pass, we just try to match the
					// protocol identifier in case we can't find an
					// exact match to the IP or DNS name.

					Find = Find.Left(Find.Find(':') + 1);
					}

				for( int n = 0; n < pDevice->m_NoOfServerEndpoints; n++ ) {

					OpcUa_EndpointDescription *pEndpoint = pDevice->m_pServerEndpoints + n;

					if( CString(pEndpoint->EndpointUrl.strContent).StartsWith(Find) ) {

						if( pEndpoint->SecurityMode == OpcUa_MessageSecurityMode_None ) {

							pDevice->m_pEndpoint = pEndpoint;

							pDevice->m_fSession  = TRUE;

							break;
							}
						}
					}

				if( pDevice->m_fSession ) {

					break;
					}
				}

			if( !pDevice->m_fSession ) {

				AfxTrace("opc: cannot find suitable endpoint description\n");

				Code = OpcUa_Bad;
				}
			}

		if( Code != OpcUa_Good ) {

			// Reply Release

			OpcUa_NodeId_Clear(&pDevice->m_AuthenticationToken);

			for( int n = 0; n < pDevice->m_NoOfServerEndpoints; n++ ) {

				OpcUa_EndpointDescription *pEndpoint = pDevice->m_pServerEndpoints + n;

				OpcUa_EndpointDescription_Clear(pEndpoint);
				}

			OpcUa_Free(pDevice->m_pServerEndpoints);

			pDevice->m_NoOfServerEndpoints = 0;

			pDevice->m_pServerEndpoints    = NULL;
			}

		// Parameter Release

		OpcUa_RequestHeader_Clear(&RequestHeader);

		OpcUa_ApplicationDescription_Clear(&ClientDescription);

		OpcUa_String_Clear(&ServerUri);
	
		OpcUa_String_Clear(&EndpointUrl);
	
		OpcUa_String_Clear(&SessionName);

		OpcUa_ByteString_Clear(&ClientNonce);

		OpcUa_ResponseHeader_Clear(&ResponseHeader);

		OpcUa_NodeId_Clear(&SessionId);

		OpcUa_Free(pServerSoftwareCertificates);

		OpcUa_SignatureData_Clear(&ServerSignature);

		// Check for Success

		return Code == OpcUa_Good;
		}

	catch(CExecCancel const &)
	{
		// Reply Release

		OpcUa_NodeId_Clear(&pDevice->m_AuthenticationToken);

		for( int n = 0; n < pDevice->m_NoOfServerEndpoints; n++ ) {

			OpcUa_EndpointDescription *pEndpoint = pDevice->m_pServerEndpoints + n;

			OpcUa_EndpointDescription_Clear(pEndpoint);
			}

		OpcUa_Free(pDevice->m_pServerEndpoints);

		// Parameter Release

		OpcUa_RequestHeader_Clear(&RequestHeader);

		OpcUa_ApplicationDescription_Clear(&ClientDescription);

		OpcUa_String_Clear(&ServerUri);
	
		OpcUa_String_Clear(&EndpointUrl);
	
		OpcUa_String_Clear(&SessionName);

		OpcUa_ByteString_Clear(&ClientNonce);

		OpcUa_ResponseHeader_Clear(&ResponseHeader);

		OpcUa_NodeId_Clear(&SessionId);

		OpcUa_Free(pServerSoftwareCertificates);

		OpcUa_SignatureData_Clear(&ServerSignature);

		throw;
		}
	}

BOOL COpcClient::DeleteSession(CDevice *pDevice)
{
	if( pDevice->m_fSession ) {

		AfxTrace("opc: %s deletesession\n", PCTXT(pDevice->m_Short));

		OpcUa_NodeId_Clear(&pDevice->m_AuthenticationToken);

		OpcUa_ByteString_Clear(&pDevice->m_ServerNonce);

		OpcUa_ByteString_Clear(&pDevice->m_ServerCertificate);

		for( int n = 0; n < pDevice->m_NoOfServerEndpoints; n++ ) {

			OpcUa_EndpointDescription *pEndpoint = pDevice->m_pServerEndpoints + n;

			OpcUa_EndpointDescription_Clear(pEndpoint);
			}

		OpcUa_Free(pDevice->m_pServerEndpoints);

		pDevice->m_NoOfServerEndpoints = 0;

		pDevice->m_pServerEndpoints    = NULL;

		pDevice->m_fSession = FALSE;
		}

	return TRUE;
	}

BOOL COpcClient::CloseSession(CDevice *pDevice)
{
	AfxTrace("opc: %s closesession\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_CloseSession(	pDevice->m_hChannel,
									&RequestHeader,
									TRUE,
									&ResponseHeader
									);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

			DeleteSession(pDevice);
			}

		// Parameter Release

		OpcUa_RequestHeader_Clear(&RequestHeader);

		OpcUa_ResponseHeader_Clear(&ResponseHeader);

		// Check for Success

		return Code == OpcUa_Good;
		}

	catch(CExecCancel const &)
	{
		// Parameter Release

		OpcUa_RequestHeader_Clear(&RequestHeader);

		OpcUa_ResponseHeader_Clear(&ResponseHeader);

		throw;
		}
	}

BOOL COpcClient::ActivateSession(CDevice *pDevice, BOOL &fWrong)
{
	AfxTrace("opc: %s activatesession\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	OpcUa_SignatureData ClientSignature;

	OpcUa_SignatureData_Initialize(&ClientSignature);

	OpcUa_ExtensionObject UserIdentityToken;

	OpcUa_ExtensionObject_Initialize(&UserIdentityToken);

	OpcUa_SignatureData UserTokenSignature;

	OpcUa_SignatureData_Initialize(&UserTokenSignature);

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32	       NoOfResults         = 0;

	OpcUa_StatusCode     * pResults            = NULL;

	OpcUa_Int32	       NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_Bad;

		if( EncodeUserIdentity(pDevice, &UserIdentityToken) ) {

			Code = OpcUa_ClientApi_ActivateSession( pDevice->m_hChannel,
								&RequestHeader,
								&ClientSignature,
								0,
								NULL,
								0,
								NULL,
								&UserIdentityToken,
								&UserTokenSignature,
								&ResponseHeader,
								&pDevice->m_ServerNonce,
								&NoOfResults,
								&pResults,
								&NoOfDiagnosticInfos,
								&pDiagnosticInfos
								);
			}

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

			}
		else {
			if( Code == OpcUa_BadSessionIdInvalid ) {

				fWrong = TRUE;
				}
			}

		// Parameter Release

		OpcUa_RequestHeader_Clear(&RequestHeader);

		OpcUa_SignatureData_Clear(&ClientSignature);

		OpcUa_ExtensionObject_Clear(&UserIdentityToken);

		OpcUa_SignatureData_Clear(&UserTokenSignature);

		OpcUa_ResponseHeader_Clear(&ResponseHeader);

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pResults);

		// Check for Success

		return Code == OpcUa_Good;
		}

	catch(CExecCancel const &)
	{
		// Parameter Release

		OpcUa_RequestHeader_Clear(&RequestHeader);

		OpcUa_SignatureData_Clear(&ClientSignature);

		OpcUa_ExtensionObject_Clear(&UserIdentityToken);

		OpcUa_SignatureData_Clear(&UserTokenSignature);

		OpcUa_ResponseHeader_Clear(&ResponseHeader);

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pResults);

		throw;
		}
	}

BOOL COpcClient::FillClientData(OpcUa_ApplicationDescription *pDesc)
{
	OpcUa_ApplicationDescription_Initialize(pDesc);

	OpcUa_String_AttachCopy(&pDesc->ApplicationUri,         "http://www.redlion.net/crimson");

	OpcUa_String_AttachCopy(&pDesc->ProductUri,             "http://www.redlion.net");

	OpcUa_String_AttachCopy(&pDesc->ApplicationName.Text,   "AeonClient");

	OpcUa_String_AttachCopy(&pDesc->ApplicationName.Locale, "en");
	
	pDesc->ApplicationType = OpcUa_ApplicationType_Client;

	return TRUE;
	}

BOOL COpcClient::FillClientNonce(OpcUa_ByteString *pNonce)
{
	OpcUa_ByteString_Initialize(pNonce);

	pNonce->Length = 32;

	pNonce->Data   = OpcAlloc(pNonce->Length, BYTE);

	AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

	if( pEntropy ) {

		pEntropy->GetEntropy(pNonce->Data, pNonce->Length);

		return TRUE;
		}

	return FALSE;
	}

BOOL COpcClient::EncodeUserIdentity(CDevice *pDevice, OpcUa_ExtensionObject *pExt)
{
	OpcUa_ExtensionObject_Initialize(pExt);

	if( pDevice->m_User.IsEmpty() ) {

		OpcUa_UserTokenPolicy const *pPolicy;

		if( FindTokenPolicy(pDevice, pPolicy, OpcUa_UserTokenType_Anonymous) ) {

			return EncodeAnonymousIdentity(pDevice, pExt, pPolicy);
			}

		return FALSE;
		}
	else {
		OpcUa_UserTokenPolicy const *pPolicy;

		if( FindTokenPolicy(pDevice, pPolicy, OpcUa_UserTokenType_UserName) ) {

			return EncodeUserNameIdentity(pDevice, pExt, pPolicy, pDevice->m_User, pDevice->m_Pass);
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL COpcClient::EncodeAnonymousIdentity(CDevice *pDevice, OpcUa_ExtensionObject *pExt, OpcUa_UserTokenPolicy const *pPolicy)
{
	pExt->Encoding			       = OpcUa_ExtensionObjectEncoding_EncodeableObject;

	pExt->TypeId.ServerIndex               = 0;

	pExt->TypeId.NodeId.NamespaceIndex     = 0;
						
	pExt->TypeId.NodeId.IdentifierType     = OpcUa_IdentifierType_Numeric;
						
	pExt->TypeId.NodeId.Identifier.Numeric = OpcUaId_AnonymousIdentityToken_Encoding_DefaultBinary;

	OpcUa_String_AttachCopy(&pExt->TypeId.NamespaceUri, "");

	OpcUa_AnonymousIdentityToken *pToken    = OpcAlloc(1, OpcUa_AnonymousIdentityToken);

	OpcUa_AnonymousIdentityToken_Initialize(pToken);

	pExt->BodySize                     = -1;

	pExt->Body.EncodeableObject.Type   = &OpcUa_AnonymousIdentityToken_EncodeableType;
					
	pExt->Body.EncodeableObject.Object = pToken;

	OpcUa_String_AttachCopy(&pToken->PolicyId, pPolicy->PolicyId.strContent);

	return TRUE;
	}

BOOL COpcClient::EncodeUserNameIdentity(CDevice *pDevice, OpcUa_ExtensionObject *pExt, OpcUa_UserTokenPolicy const *pPolicy, PCTXT pUser, PCTXT pPass)
{
	static PCTXT pList[] = {

		OpcUa_SecurityPolicy_Basic256,
		OpcUa_SecurityPolicy_Basic128Rsa15,
		OpcUa_SecurityPolicy_Basic256Sha256,
		OpcUa_SecurityPolicy_Aes128Sha256RsaOaep
		};

	PCTXT pMethod = NULL;

	bool  fOkay   = false;

	if( !pPolicy->SecurityPolicyUri.uLength ) {

		fOkay = true;
		}
	else {
		for( UINT n = 0; n < elements(pList); n++ ) {

			if( !strcasecmp(pList[n], pPolicy->SecurityPolicyUri.strContent) ) {

				pMethod = strchr(pPolicy->SecurityPolicyUri.strContent, '#') + 1;

				fOkay   = true;

				break;
				}
			}
		}

	if( fOkay ) {

		pExt->Encoding			       = OpcUa_ExtensionObjectEncoding_EncodeableObject;

		pExt->TypeId.ServerIndex               = 0;

		pExt->TypeId.NodeId.NamespaceIndex     = 0;
						
		pExt->TypeId.NodeId.IdentifierType     = OpcUa_IdentifierType_Numeric;
						
		pExt->TypeId.NodeId.Identifier.Numeric = OpcUaId_UserNameIdentityToken_Encoding_DefaultBinary;

		OpcUa_String_AttachCopy(&pExt->TypeId.NamespaceUri, "");

		OpcUa_UserNameIdentityToken *pToken    = OpcAlloc(1, OpcUa_UserNameIdentityToken);

		OpcUa_UserNameIdentityToken_Initialize(pToken);

		pExt->BodySize                     = -1;

		pExt->Body.EncodeableObject.Type   = &OpcUa_UserNameIdentityToken_EncodeableType;
					
		pExt->Body.EncodeableObject.Object = pToken;

		OpcUa_String_AttachCopy(&pToken->PolicyId, pPolicy->PolicyId.strContent);

		OpcUa_String_AttachCopy(&pToken->UserName, pUser);

		if( !pPolicy->SecurityPolicyUri.uLength ) {

			pToken->Password.Length = strlen(pPass);

			pToken->Password.Data   = OpcAlloc(pToken->Password.Length, OpcUa_Byte);

			memcpy(pToken->Password.Data, pPass, pToken->Password.Length);

			return TRUE;
			}
		else {
			CByteArray Secret;

			UINT uPass  = strlen(pPass);

			UINT uNonce = (pDevice->m_ServerNonce.Length > 0) ? pDevice->m_ServerNonce.Length : 0;

			UINT uTotal = uPass + uNonce;

			Secret.Expand(4 + uTotal);

			Secret.Append(LOBYTE(LOWORD(uTotal)));
			Secret.Append(HIBYTE(LOWORD(uTotal)));
			Secret.Append(LOBYTE(HIWORD(uTotal)));
			Secret.Append(HIBYTE(HIWORD(uTotal)));

			Secret.Append(PCBYTE(pPass), uPass);

			Secret.Append(pDevice->m_ServerNonce.Data, uNonce);

			CByteArray Data;

			CString    Name;

			bool fCrypt = EncryptSecret( Data,
						     Name,
						     pMethod,
						     pDevice->m_ServerCertificate.Data,
						     pDevice->m_ServerCertificate.Length,
						     Secret
						     );

			if( fCrypt ) {

				OpcUa_String_AttachCopy(&pToken->EncryptionAlgorithm, Name);

				pToken->Password.Length = Data.GetCount();

				pToken->Password.Data   = OpcAlloc(pToken->Password.Length, OpcUa_Byte);

				memcpy(pToken->Password.Data, Data.GetPointer(), pToken->Password.Length);

				return TRUE;
				}

			AfxTrace("opc: user name encryption failed\n");

			return FALSE;
			}
		}

	AfxTrace("opc: user name encryption policy unsupported\n");

	return FALSE;
	}

BOOL COpcClient::FindTokenPolicy(CDevice *pDevice, OpcUa_UserTokenPolicy const * &pPolicy, int TokenType)
{
	OpcUa_EndpointDescription const *pEndpoint = pDevice->m_pEndpoint;

	for( int n = 0; n < pEndpoint->NoOfUserIdentityTokens; n++ ) {

		pPolicy = pEndpoint->UserIdentityTokens + n;

		if( pPolicy->TokenType == TokenType ) {

			return TRUE;
			}
		}

	AfxTrace("opc: cannot find suitable token policy\n");

	return FALSE;
	}

// End of File
