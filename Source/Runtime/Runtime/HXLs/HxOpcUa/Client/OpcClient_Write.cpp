
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#define AssignToScalar(t, v)		\
					\
	case OpcUaId_##t:		\
	Value.Value.t = OpcUa_##t(v);	\
	break				\

#define AssignToArray(t, v)					\
								\
	case OpcUaId_##t:					\
	Value.Value.Array.Value.t##Array[n] = OpcUa_##t(v);	\
	break							\

#define AllocArray(t, c)						\
									\
	case OpcUaId_##t:						\
	Value.Value.Array.Value.t##Array = OpcAlloc(c, OpcUa_##t);	\
	break;								\

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Externals

clink long   wcstol(wchar_t const *, wchar_t * *, int);

clink double wcstod(wchar_t const *, wchar_t * *);

// Write Support

bool COpcClient::WriteValues(CDevice *pDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData)
{
	AfxTrace("opc: %s writevalues\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	INT               nNodes = uList;

	OpcUa_WriteValue *pNodes = OpcAlloc(nNodes, OpcUa_WriteValue);

	{
		for( INT n = 0; n < nNodes; n++ ) {

			UINT              uNode = pList[n];

			OpcUa_WriteValue *pNode = pNodes + n;

			OpcUa_WriteValue_Initialize(pNode);

			pNode->AttributeId = OpcUa_Attributes_Value;

			CopyNodeId(&pNode->NodeId, &pDevice->m_Nodes[uNode]);

			pNode->Value.StatusCode     = OpcUa_Good;

			pNode->Value.Value.Datatype = BYTE(pDevice->m_Types[uNode]);

			EncodeValue(pNode->Value.Value, pType[n], pData[n]);
		}
	}

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32            NoOfResults         = 0;

	OpcUa_StatusCode    *  pResults            = NULL;

	OpcUa_Int32            NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_Write(pDevice->m_hChannel,
							      &RequestHeader,
							      nNodes,
							      pNodes,
							      &ResponseHeader,
							      &NoOfResults,
							      &pResults,
							      &NoOfDiagnosticInfos,
							      &pDiagnosticInfos
		);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

		}

		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_WriteValue *pNode = pNodes + n;

				OpcUa_WriteValue_Clear(pNode);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		// Check for Success

		return Code == OpcUa_Good;
	}

	catch( CExecCancel const & )
	{
		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_WriteValue *pNode = pNodes + n;

				OpcUa_WriteValue_Clear(pNode);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		throw;
	}
}

bool COpcClient::WriteArray(CDevice *pDevice, UINT uNode, UINT uType, UINT uStart, UINT uCount, PCDWORD pData)
{
	AfxTrace("opc: %s writearray\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	INT               nNodes = 1;

	OpcUa_WriteValue *pNodes = OpcAlloc(nNodes, OpcUa_WriteValue);

	{
		for( INT n = 0; n < nNodes; n++ ) {

			OpcUa_WriteValue *pNode = pNodes + n;

			OpcUa_WriteValue_Initialize(pNode);

			pNode->AttributeId = OpcUa_Attributes_Value;

			CopyNodeId(&pNode->NodeId, &pDevice->m_Nodes[uNode]);

			pNode->Value.StatusCode      = OpcUa_Good;

			pNode->Value.Value.Datatype  = BYTE(pDevice->m_Types[uNode]);

			pNode->Value.Value.ArrayType = 1;

			if( uStart < NOTHING ) {

				char range[32];

				sprintf(range, (uCount == 1) ? "%u" : "%u:%u", uStart, uStart + uCount - 1);

				OpcUa_String_AttachCopy(&pNode->IndexRange, range);
			}
	
			EncodeArray(pNode->Value.Value, uType, uCount, pData);
		}
	}

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32            NoOfResults         = 0;

	OpcUa_StatusCode    *  pResults            = NULL;

	OpcUa_Int32            NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_Write(pDevice->m_hChannel,
							      &RequestHeader,
							      nNodes,
							      pNodes,
							      &ResponseHeader,
							      &NoOfResults,
							      &pResults,
							      &NoOfDiagnosticInfos,
							      &pDiagnosticInfos
		);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

		}

		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_WriteValue *pNode = pNodes + n;

				OpcUa_WriteValue_Clear(pNode);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		// Check for Success

		return Code == OpcUa_Good;
	}

	catch( CExecCancel const & )
	{
		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_WriteValue *pNode = pNodes + n;

				OpcUa_WriteValue_Clear(pNode);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		throw;
	}
}

void COpcClient::EncodeValue(OpcUa_Variant &Value, UINT Type, DWORD Data)
{
	if( Type == typeInteger ) {

		C3INT d = C3INT(Data);

		switch( Value.Datatype ) {

			AssignToScalar(Double, d);
			AssignToScalar(Float, d);
			AssignToScalar(Byte, d);
			AssignToScalar(SByte, d);
			AssignToScalar(Int16, d);
			AssignToScalar(UInt16, d);
			AssignToScalar(Int32, d);
			AssignToScalar(UInt32, d);
			AssignToScalar(Int64, d);
			AssignToScalar(UInt64, d);

			case OpcUaId_Boolean:

				Value.Value.Boolean = d ? 1 : 0;

				break;

			case OpcUaId_DateTime:

				TimeFromUnix(Value.Value.DateTime, Data + 9862 * 60 * 60 * 24);

				break;

			case OpcUaId_String:

				OpcUa_String_AttachCopy(&Value.Value.String, Format(d));

				break;
		}
	}

	if( Type == typeReal ) {

		C3REAL d = I2R(Data);

		switch( Value.Datatype ) {

			AssignToScalar(Double, d);
			AssignToScalar(Float, d);
			AssignToScalar(Byte, d);
			AssignToScalar(SByte, d);
			AssignToScalar(Int16, d);
			AssignToScalar(UInt16, d);
			AssignToScalar(Int32, d);
			AssignToScalar(UInt32, d);
			AssignToScalar(Int64, d);
			AssignToScalar(UInt64, d);

			case OpcUaId_Boolean:

				Value.Value.Boolean = d ? 1 : 0;

				break;

			case OpcUaId_DateTime:

				Value.Value.DateTime.t = 0;

				break;

			case OpcUaId_String:

				OpcUa_String_AttachCopy(&Value.Value.String, Format(d));

				break;
		}
	}

	if( Type == typeString ) {

		PCUTF s = PCUTF(Data);

		PTXT  p = NULL;

		switch( Value.Datatype ) {

			AssignToScalar(Double, wcstod(s, NULL));
			AssignToScalar(Float, wcstod(s, NULL));
			AssignToScalar(Byte, wcstol(s, NULL, 10));
			AssignToScalar(SByte, wcstol(s, NULL, 10));
			AssignToScalar(Int16, wcstol(s, NULL, 10));
			AssignToScalar(UInt16, wcstol(s, NULL, 10));
			AssignToScalar(Int32, wcstol(s, NULL, 10));
			AssignToScalar(UInt32, wcstol(s, NULL, 10));
			AssignToScalar(Int64, wcstol(s, NULL, 10));
			AssignToScalar(UInt64, wcstol(s, NULL, 10));

			case OpcUaId_Boolean:

				if( !wcscasecmp(s, L"true") || !wcscasecmp(s, L"on") || wcstol(s, NULL, 10) > 0 ) {

					Value.Value.Boolean = 1;
				}
				else
					Value.Value.Boolean = 0;

				break;

			case OpcUaId_DateTime:

				Value.Value.DateTime.t = 0;

				break;

			case OpcUaId_String:

				p = UtfEncode(s);

				OpcUa_String_AttachCopy(&Value.Value.String, p);

				delete[] p;

				break;
		}
	}
}

void COpcClient::EncodeArray(OpcUa_Variant &Value, UINT Type, UINT uCount, PCDWORD pData)
{
	Value.Value.Array.Length = uCount;

	switch( Value.Datatype ) {

		AllocArray(Double, uCount);
		AllocArray(Float, uCount);
		AllocArray(Byte, uCount);
		AllocArray(SByte, uCount);
		AllocArray(Int16, uCount);
		AllocArray(UInt16, uCount);
		AllocArray(Int32, uCount);
		AllocArray(UInt32, uCount);
		AllocArray(Int64, uCount);
		AllocArray(UInt64, uCount);
		AllocArray(Boolean, uCount);
		AllocArray(DateTime, uCount);
	}

	for( UINT n = 0; n < uCount; n++ ) {

		if( Type == typeInteger ) {

			C3INT d = C3INT(pData[n]);

			switch( Value.Datatype ) {

				AssignToArray(Double, d);
				AssignToArray(Float, d);
				AssignToArray(Byte, d);
				AssignToArray(SByte, d);
				AssignToArray(Int16, d);
				AssignToArray(UInt16, d);
				AssignToArray(Int32, d);
				AssignToArray(UInt32, d);
				AssignToArray(Int64, d);
				AssignToArray(UInt64, d);

				case OpcUaId_Boolean:

					Value.Value.Array.Value.BooleanArray[n] = d ? 1 : 0;

					break;

				case OpcUaId_DateTime:

					TimeFromUnix(Value.Value.Array.Value.DateTimeArray[n], d + 9862 * 60 * 60 * 24);

					break;
			}
		}

		if( Type == typeReal ) {

			C3REAL d = I2R(pData[n]);

			switch( Value.Datatype ) {

				AssignToArray(Double, d);
				AssignToArray(Float, d);
				AssignToArray(Byte, d);
				AssignToArray(SByte, d);
				AssignToArray(Int16, d);
				AssignToArray(UInt16, d);
				AssignToArray(Int32, d);
				AssignToArray(UInt32, d);
				AssignToArray(Int64, d);
				AssignToArray(UInt64, d);

				case OpcUaId_Boolean:

					Value.Value.Array.Value.BooleanArray[n] = d ? 1 : 0;

					break;

				case OpcUaId_DateTime:

					Value.Value.Array.Value.DateTimeArray[n].t = 0;

					break;
			}
		}
	}
}

// End of File
