
#include "Intern.hpp"

#include "OpcNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OpcDataModel.hpp"

#include "OpcReferenceTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Base Node
//

// Constructors

COpcNode::COpcNode(COpcNode const &That) :
	m_pModel(That.m_pModel),
	m_Id(That.m_Id),
	m_Class(That.m_Class),
	m_BrowseName(That.m_BrowseName),
	m_DisplayName(That.m_DisplayName),
	m_References(That.m_References),
	m_fPrivate(That.m_fPrivate)
{
	m_pModel->Register(this);
}

COpcNode::COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, UINT Value) :
	m_pModel(pModel),
	m_Id(Namespace, Value),
	m_Class(Class),
	m_fPrivate(FALSE)
{
	m_pModel->Register(this);
}

COpcNode::COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, CString const &Value) :
	m_pModel(pModel),
	m_Id(Namespace, Value),
	m_Class(Class),
	m_fPrivate(FALSE)
{
	m_pModel->Register(this);
}

COpcNode::COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, CGuid const &Value) :
	m_pModel(pModel),
	m_Id(Namespace, Value),
	m_Class(Class),
	m_fPrivate(FALSE)
{
	m_pModel->Register(this);
}

COpcNode::COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, CByteArray const &Value) :
	m_pModel(pModel),
	m_Id(Namespace, Value),
	m_Class(Class),
	m_fPrivate(FALSE)
{
	m_pModel->Register(this);
}

// Destructor

COpcNode::~COpcNode(void)
{
	for( UINT n = 0; n < m_References.GetCount(); n++ ) {

		delete m_References[n];
	}

	m_References.Empty();

	m_pModel->Unregister(this);
}

// Assignment

COpcNode & COpcNode::operator = (COpcNode const &That)
{
	m_pModel->Unregister(this);

	m_pModel      = That.m_pModel;

	m_Id	      = That.m_Id;

	m_Class       = That.m_Class;

	m_BrowseName  = That.m_BrowseName;

	m_DisplayName = That.m_DisplayName;

	m_References  = That.m_References;

	m_pModel->Register(this);

	return ThisObject;
}

// Attributes

CString COpcNode::Describe(bool fClass) const
{
	if( fClass ) {

		return CPrintf("%s (%s,%s)", PCTXT(GetIdAsText()), PCTXT(GetClassAsText()), PCTXT(m_BrowseName));
	}

	return CPrintf("%s (%s)", PCTXT(GetIdAsText()), PCTXT(m_BrowseName));
}

CString COpcNode::GetClassAsText(void) const
{
	switch( m_Class ) {

		case classObject:	 return "Object";
		case classVariable:	 return "Variable";
		case classMethod:	 return "Method";
		case classObjectType:	 return "ObjectType";
		case classVariableType:	 return "VariableType";
		case classReferenceType: return "ReferenceType";
		case classDataType:	 return "DataType";
		case classView:		 return "View";
	}

	return "Unspecified";
}

COpcNode * COpcNode::GetReferenceNode(UINT r) const
{
	return m_pModel->FindNode(m_References[r]->GetTargetId(), false);
}

// Operations

void COpcNode::SetBrowseName(CString const &Name)
{
	m_BrowseName = Name;
}

void COpcNode::SetDisplayName(CString const &Name)
{
	m_DisplayName = Name;
}

void COpcNode::SetNames(CString const &Name)
{
	m_BrowseName  = Name;

	m_DisplayName = Name;
}

void COpcNode::AddReference(COpcNodeId const &Ref, COpcNodeId const &Target, bool fInverse)
{
	COpcReference *pRef = New COpcReference(m_pModel, Ref, Target, fInverse);

	m_References.Append(pRef);
}

void COpcNode::AddReference(UINT nsRef, UINT idRef, UINT nsTarget, UINT idTarget)
{
	return AddReference(COpcNodeId(nsRef, idRef), COpcNodeId(nsTarget, idTarget), false);
}

void COpcNode::AddInverses(void)
{
	for( UINT n = 0; n < m_References.GetCount(); n++ ) {

		COpcReference *pRef = m_References[n];

		if( pRef->NeedInverse() ) {

			COpcNode *pTarget = m_pModel->FindNode(pRef->GetTargetId(), false);

			pTarget->AddReference(pRef->GetTypeId(), m_Id, true);
		}
	}
}

// Validation

bool COpcNode::Validate(void)
{
	for( UINT n = 0; n < m_References.GetCount(); n++ ) {

		COpcReference *pRef = m_References[n];

		if( !m_pModel->HasReferenceType(pRef->GetTypeId()) ) {

			AfxTrace("opc: node %s reference %u has bad type of %s\n",
				 PCTXT(Describe(true)),
				 1+n,
				 PCTXT(pRef->GetTypeId().GetAsText())
			);
		}

		if( !m_pModel->HasNode(pRef->GetTargetId()) ) {

			COpcReferenceTypeNode *pType = m_pModel->FindReferenceType(pRef->GetTypeId(), false);

			AfxTrace("opc: node %s reference %u of type %s has bad target of %s\n",
				 PCTXT(Describe(true)),
				 1+n,
				 PCTXT(pType->Describe(false)),
				 PCTXT(pRef->GetTargetId().GetAsText())
			);
		}
	}

	return true;
}

// End of File
