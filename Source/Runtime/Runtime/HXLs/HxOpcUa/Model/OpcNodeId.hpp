
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcNodeId_HPP

#define INCLUDE_OpcNodeId_HPP

//////////////////////////////////////////////////////////////////////////
//
// Node Identifier
//

class COpcNodeId : public OpcUa_NodeId
{
	public:
		// Constructors
		COpcNodeId(void);
		COpcNodeId(COpcNodeId   const &That);
		COpcNodeId(OpcUa_NodeId const &That);
		COpcNodeId(UINT Namespace, UINT Value);
		COpcNodeId(UINT Namespace, CString const &Value);
		COpcNodeId(UINT Namespace, CGuid const &Value);
		COpcNodeId(UINT Namespace, CByteArray const &Value);

		// Destructor
		~COpcNodeId(void);

		// Assignment
		COpcNodeId & operator = (COpcNodeId   const &That);
		COpcNodeId & operator = (OpcUa_NodeId const &That);

		// Operators
		bool operator == (OpcUa_NodeId const &That) const;

		// Comparison
		friend int AfxCompare(COpcNodeId const &a, COpcNodeId const &b);

		// Attributes
		bool       IsNull(void) const;
		UINT       GetType(void) const;
		bool       IsType(UINT Type) const;
		WORD       GetNamespace(void) const;
		UINT       GetNumericValue(void) const;
		CString    GetStringValue(void) const;
		CGuid      GetGuidValue(void) const;
		CByteArray GetOpaqueValue(void) const;
		CString    GetAsText(void) const;

		// Operations
		CString Encode(void) const;
		bool    Decode(CString code);

	protected:
		// Implementation
		void InitFrom(OpcUa_NodeId const &That);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE bool COpcNodeId::IsNull(void) const
{
	return IsType(OpcUa_IdType_Numeric) && Identifier.Numeric == 0;
	}

STRONG_INLINE UINT COpcNodeId::GetType(void) const
{
	return IdentifierType;
	}

STRONG_INLINE bool COpcNodeId::IsType(UINT Type) const
{
	return IdentifierType == Type;
	}

STRONG_INLINE WORD COpcNodeId::GetNamespace(void) const
{
	return WORD(NamespaceIndex);
	}

STRONG_INLINE UINT COpcNodeId::GetNumericValue(void) const
{
	return Identifier.Numeric;
	}

STRONG_INLINE CString COpcNodeId::GetStringValue(void) const
{
	return Identifier.String.strContent;
	}

STRONG_INLINE CGuid COpcNodeId::GetGuidValue(void) const
{
	return (GUID &) *Identifier.Guid;
	}

STRONG_INLINE CByteArray COpcNodeId::GetOpaqueValue(void) const
{
	return CByteArray(Identifier.ByteString.Data, Identifier.ByteString.Length);
	}

// End of File

#endif
