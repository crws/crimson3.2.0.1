
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcReference_HPP

#define INCLUDE_OpcReference_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNodeId.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COpcDataModel;

//////////////////////////////////////////////////////////////////////////
//
// Reference
//

class COpcReference
{
	public:
		// Constructors
		COpcReference(void);
		COpcReference(COpcReference const &That);
		COpcReference(COpcDataModel *pModel, COpcNodeId const &TypeId, COpcNodeId const &TargetId, bool fInverse);

		// Assignment
		COpcReference & operator = (COpcReference const &That);

		// Attributes
		COpcNodeId const & GetTypeId(void) const;
		COpcNodeId const & GetTargetId(void) const;
		bool               IsInverse(void) const;
		CString            GetTargetUri(void) const;
		bool               NeedInverse(void) const;

	protected:
		// Data Members	
		COpcDataModel * m_pModel;
		COpcNodeId      m_TypeId;
		COpcNodeId      m_TargetId;
		bool            m_fInverse;
		CString         m_TargetUri;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE COpcNodeId const & COpcReference::GetTypeId(void) const
{
	return m_TypeId;
	}

STRONG_INLINE COpcNodeId const & COpcReference::GetTargetId(void) const
{
	return m_TargetId;
	}

STRONG_INLINE bool COpcReference::IsInverse(void) const
{
	return m_fInverse;
	}

STRONG_INLINE CString COpcReference::GetTargetUri(void) const
{
	return m_TargetUri;
	}

// End of File

#endif
