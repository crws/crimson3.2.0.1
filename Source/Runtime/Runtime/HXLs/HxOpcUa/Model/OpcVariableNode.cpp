
#include "Intern.hpp"

#include "OpcVariableNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OpcDataModel.hpp"

#include "OpcVariableTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Variable Node
//

// Constructors

COpcVariableNode::COpcVariableNode(COpcDataModel *pModel, UINT Namespace, UINT Value) : COpcNode(pModel, classVariable, Namespace, Value)
{
	m_fHistory = false;
	}

COpcVariableNode::COpcVariableNode(COpcDataModel *pModel, UINT Namespace, CString const &Value) : COpcNode(pModel, classVariable, Namespace, Value)
{
	m_fHistory = false;
	}

COpcVariableNode::COpcVariableNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value) : COpcNode(pModel, classVariable, Namespace, Value)
{
	m_fHistory = false;
	}

COpcVariableNode::COpcVariableNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value) : COpcNode(pModel, classVariable, Namespace, Value)
{
	m_fHistory = false;
	}

COpcVariableNode::COpcVariableNode(COpcVariableNode const &That) : COpcNode(That)
{
	m_DataType = That.m_DataType;
	
	m_Rank     = That.m_Rank;
	
	m_Dim1     = That.m_Dim1;
	
	m_Access   = That.m_Access;
	
	m_fHistory = That.m_fHistory;
	}

// Assignment

COpcVariableNode COpcVariableNode::operator = (COpcVariableNode const &That)
{
	COpcNode::operator = (That);

	m_DataType = That.m_DataType;
	
	m_Rank     = That.m_Rank;
	
	m_Dim1     = That.m_Dim1;
	
	m_Access   = That.m_Access;
	
	m_fHistory = That.m_fHistory;

	return ThisObject;
	}

// Operations

void COpcVariableNode::SetDataType(UINT nsType, UINT idType)
{
	m_DataType = COpcNodeId(nsType, idType);
	}

void COpcVariableNode::SetRank(INT Rank, UINT Dim1)
{
	m_Rank = Rank;

	m_Dim1 = Dim1;
	}

void COpcVariableNode::SetAccess(UINT Access)
{
	m_fPrivate = (Access & 0x0100) ? TRUE : FALSE;

	m_Access   = LOBYTE(Access);
	}

void COpcVariableNode::SetHistory(UINT uHistory)
{
	m_uHistory = uHistory;

	m_fHistory = true;
	}

// Validation

bool COpcVariableNode::Validate(void)
{
	if( !m_pModel->HasDataType(m_DataType) ) {

		AfxTrace( "opc: node %s has bad data type of %s\n",
				PCTXT(Describe(true)),
				PCTXT(m_DataType.GetAsText())
				);
		}

	return COpcNode::Validate();
	}

// End of File
