
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcDataTypeNode_HPP

#define INCLUDE_OpcDataTypeNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Type Node
//

class COpcDataTypeNode : public COpcNode
{
	public:
		// Constructors
		COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract);
		COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract);
		COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract);
		COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract);
		COpcDataTypeNode(COpcDataTypeNode const &That);

		// Assignment
		COpcDataTypeNode operator = (COpcDataTypeNode const &That);

		// Attributes
		bool IsAbstract(void) const;

	protected:
		// Data Members
		bool m_fAbstract;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE bool COpcDataTypeNode::IsAbstract(void) const
{
	return m_fAbstract;
	}

// End of File

#endif
