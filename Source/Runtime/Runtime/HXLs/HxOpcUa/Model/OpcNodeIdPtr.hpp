
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcNodeIdPtr_HPP

#define INCLUDE_OpcNodeIdPtr_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COpcNodeId;

//////////////////////////////////////////////////////////////////////////
//
// Node Identifier Pointer
//

class COpcNodeIdPtr
{
	public:
		// Constructors
		COpcNodeIdPtr(COpcNodeIdPtr const &That);
		COpcNodeIdPtr(COpcNodeId const &Node);

		// Assignment
		COpcNodeIdPtr & operator = (COpcNodeIdPtr const &That);

		// Comparison
		friend int AfxCompare(COpcNodeIdPtr const &a, COpcNodeIdPtr const &b);

		// Operators
		COpcNodeId const & operator * (void) const;

	protected:
		// Data Members
		COpcNodeId const * m_pPtr;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Operators

STRONG_INLINE COpcNodeId const & COpcNodeIdPtr::operator * (void) const
{
	return *m_pPtr;
	}

// End of File

#endif
