
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcVariableTypeNode_HPP

#define INCLUDE_OpcVariableTypeNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Variable Type Node
//

class COpcVariableTypeNode : public COpcNode
{
	public:
		// Constructors
		COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract);
		COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract);
		COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract);
		COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract);
		COpcVariableTypeNode(COpcVariableTypeNode const &That);

		// Assignment
		COpcVariableTypeNode operator = (COpcVariableTypeNode const &That);

		// Attributes
		COpcNodeId const & GetDataType(void) const;
		UINT               GetArrayDimensions(void) const;
		bool		   IsAbstract(void) const;

		// Operations
		void SetDataType(UINT nsType, UINT idType);
		void SetRank(INT Rank);

		// Validation
		bool Validate(void);

	protected:
		// Data Members
		COpcNodeId m_DataType;
		INT        m_Rank;
		bool       m_fAbstract;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE COpcNodeId const & COpcVariableTypeNode::GetDataType(void) const
{
	return m_DataType;
	}

STRONG_INLINE UINT COpcVariableTypeNode::GetArrayDimensions(void) const
{
	switch( m_Rank ) {

		case OpcUa_ValueRanks_Any:
		case OpcUa_ValueRanks_Scalar:

			return 0;

		case OpcUa_ValueRanks_OneDimension:

			return 1;
		}

	return 0;
	}

STRONG_INLINE bool COpcVariableTypeNode::IsAbstract(void) const
{
	return m_fAbstract;
	}

// End of File

#endif
