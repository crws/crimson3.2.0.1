
#include "Intern.hpp"

#include "OpcDataTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Type Node
//

// Constructors

COpcDataTypeNode::COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcDataTypeNode::COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcDataTypeNode::COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcDataTypeNode::COpcDataTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcDataTypeNode::COpcDataTypeNode(COpcDataTypeNode const &That) : COpcNode(That)
{
	m_fAbstract = That.m_fAbstract;
	}

// Assignment

COpcDataTypeNode COpcDataTypeNode::operator = (COpcDataTypeNode const &That)
{
	COpcNode::operator = (That);

	m_fAbstract = That.m_fAbstract;

	return ThisObject;
	}

// End of File
