
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcDataModel_HPP

#define INCLUDE_OpcDataModel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COpcNode;
class COpcNodeId;
class COpcNodeIdPtr;
class COpcReferenceTypeNode;
class COpcVariableNode;

//////////////////////////////////////////////////////////////////////////
//
// Data Model
//

class COpcDataModel : public IOpcUaDataModel
{
	public:
		// Constructor
		COpcDataModel(void);

		// Destructor
		~COpcDataModel(void);

		// Operations
		void Lock(void);
		void Free(void);
		void Register(COpcNode *pNode);
		void Unregister(COpcNode *pNode);
		void AddInverses(void);
		void PublishChanges(void);
		void ClearChanges(void);
		bool Validate(void);

		// Attributes
		bool HasNode(COpcNodeId const &Id) const;
		bool HasNode(COpcNodeId const &Id, UINT Class) const;
		bool HasReferenceType(COpcNodeId const &Id) const;
		bool HasDataType(COpcNodeId const &Id) const;
		bool HasChanges(void) const;
		bool HasHistorizingNodes(void) const;
		UINT GetHistorizingCount(void) const;

		// History Nodes
		COpcVariableNode * GetHistorizingNode(UINT uSlot) const;

		// Location
		COpcNode              * FindNode(COpcNodeId const &Id, bool fOptional) const;
		COpcReferenceTypeNode * FindReferenceType(COpcNodeId const &Type, bool fOptional) const;

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IOpcUaDataModel
		void AddObject(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name);
		void AddVariable(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name, UINT nsData, UINT idData, INT Rank, UINT Dim1, UINT Access, bool fHistory);
		void AddObjectType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract);
		void AddVariableType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, UINT nsData, UINT idData, INT Rank, bool fAbstract);
		void AddDataType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract);
		void AddReferenceType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract, bool fSymmetric);
		void AddReference(UINT nsNode, UINT idNode, UINT nsRef, UINT idRef, UINT nsTarget, UINT idTarget);
		void AddOrganizes(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget);
		void AddProperty (UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget);
		void AddComponent(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget);
		void AddMandatoryRule(UINT nsNode, UINT idNode);
	
	protected:
		// Data Members
		ULONG			       m_uRefs;
		IMutex			     * m_pMutex;
		CArray   <COpcNode *>          m_NodeList;
		CZeroMap <COpcNodeIdPtr, UINT> m_NodeMap;
		CArray   <COpcVariableNode *>  m_HistList;
		bool			       m_fChanges;
		bool			       m_fDestruct;
	};

// End of File

#endif
