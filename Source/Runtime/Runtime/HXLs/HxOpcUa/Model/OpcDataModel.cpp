
#include "Intern.hpp"

#include "OpcDataModel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OpcDataTypeNode.hpp"
#include "OpcNode.hpp"
#include "OpcNodeIdPtr.hpp"
#include "OpcObjectNode.hpp"
#include "OpcObjectTypeNode.hpp"
#include "OpcReferenceTypeNode.hpp"
#include "OpcVariableNode.hpp"
#include "OpcVariableTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Model
//

// Constructor

COpcDataModel::COpcDataModel(void)
{
	StdSetRef();

	m_fChanges  = false;

	m_fDestruct = false;

	m_pMutex    = Create_Qutex();
	}

// Destructor

COpcDataModel::~COpcDataModel(void)
{
	m_fDestruct = true;

	for( UINT n = 0; n < m_NodeList.GetCount(); n++ ) {

		delete m_NodeList[n];
		}

	m_NodeList.Empty();
	
	m_NodeMap.Empty();

	m_pMutex->Release();
	}

// Operations

void COpcDataModel::Lock(void)
{
	m_pMutex->Wait(FOREVER);
	}

void COpcDataModel::Free(void)
{
	m_pMutex->Free();
	}

void COpcDataModel::Register(COpcNode *pNode)
{
	UINT uIndex  = m_NodeList.Append(pNode);

	BOOL fInsert = m_NodeMap.Insert(pNode->GetId(), 1 + uIndex);

	AfxAssert(fInsert);
	}

void COpcDataModel::Unregister(COpcNode *pNode)
{
	if( !m_fDestruct ) {

		UINT uIndex = m_NodeMap[pNode->GetId()];

		AfxAssert(uIndex);

		m_NodeList.Remove(uIndex - 1);
		}
	}

void COpcDataModel::AddInverses(void)
{
	for( UINT n = 0; n < m_NodeList.GetCount(); n++ ) {

		COpcNode *pNode = m_NodeList[n];

		pNode->AddInverses();
		}
	}

void COpcDataModel::PublishChanges(void)
{
	m_fChanges = true;
	}

void COpcDataModel::ClearChanges(void)
{
	m_fChanges = false;
	}

bool COpcDataModel::Validate(void)
{
	for( UINT n = 0; n < m_NodeList.GetCount(); n++ ) {

		COpcNode *pNode = m_NodeList[n];

		if( !pNode->Validate() ) {

			return false;
			}
		}

	return true;
	}

// Attributes

bool COpcDataModel::HasNode(COpcNodeId const &Id) const
{
	return m_NodeMap[Id] > 0;
	}

bool COpcDataModel::HasNode(COpcNodeId const &Id, UINT Class) const
{
	UINT uIndex = m_NodeMap[Id];

	if( uIndex > 0 ) {

		COpcNode *pNode = m_NodeList[uIndex - 1];

		return pNode->IsClass(Class);
		}

	return false;
	}

bool COpcDataModel::HasReferenceType(COpcNodeId const &Id) const
{
	return HasNode(Id, COpcNode::classReferenceType);
	}

bool COpcDataModel::HasDataType(COpcNodeId const &Id) const
{
	if( Id.IsType(OpcUa_IdType_Numeric) ) {

		if( Id.GetNamespace() == 0 ) {

			// Assume default types are valid.

			return true;
			}
		}

	return HasNode(Id, COpcNode::classDataType);
	}

bool COpcDataModel::HasChanges(void) const
{
	return m_fChanges;
	}

bool COpcDataModel::HasHistorizingNodes(void) const
{
	return !m_HistList.IsEmpty();
	}

UINT COpcDataModel::GetHistorizingCount(void) const
{
	return m_HistList.GetCount();
	}

// History Nodes

COpcVariableNode * COpcDataModel::GetHistorizingNode(UINT uSlot) const
{
	return m_HistList[uSlot];
	}

// Location

COpcNode * COpcDataModel::FindNode(COpcNodeId const &Id, bool fOptional) const
{
	UINT uIndex = m_NodeMap[Id];

	if( !uIndex ) {

		AfxAssert(fOptional);

		return NULL;
		}

	return m_NodeList[uIndex - 1];
	}

COpcReferenceTypeNode * COpcDataModel::FindReferenceType(COpcNodeId const &Type, bool fOptional) const
{
	UINT uIndex = m_NodeMap[Type];

	if( !uIndex ) {

		AfxAssert(fOptional);

		return NULL;
		}

	COpcNode *pNode = m_NodeList[uIndex - 1];

	AfxAssert(pNode->IsClass(COpcNode::classReferenceType));

	return (COpcReferenceTypeNode *) pNode;
	}

// IUnknown

HRESULT COpcDataModel::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IOpcUaDataModel);

	StdQueryInterface(IOpcUaDataModel);

	return E_NOINTERFACE;
	}

ULONG COpcDataModel::AddRef(void)
{
	StdAddRef();
	}

ULONG COpcDataModel::Release(void)
{
	StdRelease();
	}

// IOpcUaDataModel

void COpcDataModel::AddObject(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name)
{
	COpcObjectNode *pNode = New COpcObjectNode(this, nsNode, idNode, 0);

	AddReference(nsNode, idNode, 0, OpcUaId_HasTypeDefinition, nsType, idType);

	pNode->SetNames(Name);
	}

void COpcDataModel::AddVariable(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name, UINT nsData, UINT idData, INT Rank, UINT Dim1, UINT Access, bool fHistory)
{
	COpcVariableNode *pNode = New COpcVariableNode(this, nsNode, idNode);
	
	if( nsType || idType ) {

		AddReference(nsNode, idNode, 0, OpcUaId_HasTypeDefinition, nsType, idType);
		}

	if( fHistory ) {

		UINT uSlot = m_HistList.Append(pNode);

		pNode->SetHistory(uSlot);
		}

	pNode->SetNames(Name);

	pNode->SetDataType(nsData, idData);

	pNode->SetRank(Rank ? Rank : OpcUa_ValueRanks_Scalar, Dim1);

	pNode->SetAccess(Access);
	}

void COpcDataModel::AddObjectType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract)
{
	COpcObjectTypeNode *pNode = New COpcObjectTypeNode(this, nsNode, idNode, fAbstract);

	if( nsBase || idBase ) {

		AddReference(nsBase, idBase, 0, OpcUaId_HasSubtype, nsNode, idNode);
		}

	pNode->SetNames(Name);
	}

void COpcDataModel::AddVariableType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, UINT nsData, UINT idData, INT Rank, bool fAbstract)
{
	COpcVariableTypeNode *pNode = New COpcVariableTypeNode(this, nsNode, idNode, fAbstract);
	
	if( nsBase || idBase ) {

		AddReference(nsBase, idBase, 0, OpcUaId_HasSubtype, nsNode, idNode);
		}

	pNode->SetNames(Name);

	pNode->SetDataType(nsData, idData);

	pNode->SetRank(Rank ? Rank : OpcUa_ValueRanks_Scalar);
	}

void COpcDataModel::AddDataType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract)
{
	COpcDataTypeNode *pNode = New COpcDataTypeNode(this, nsNode, idNode, fAbstract);

	if( nsBase || idBase ) {

		AddReference(nsBase, idBase, 0, OpcUaId_HasSubtype, nsNode, idNode);
		}

	pNode->SetNames(Name);
	}

void COpcDataModel::AddReferenceType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract, bool fSymmetric)
{
	COpcReferenceTypeNode *pNode = New COpcReferenceTypeNode(this, nsNode, idNode, fAbstract, fSymmetric);
	
	if( nsBase || idBase ) {

		AddReference(nsBase, idBase, 0, OpcUaId_HasSubtype, nsNode, idNode);
		}

	pNode->SetNames(Name);

	pNode->FindInverses();
	}

void COpcDataModel::AddReference(UINT nsNode, UINT idNode, UINT nsRef, UINT idRef, UINT nsTarget, UINT idTarget)
{
	UINT uIndex = m_NodeMap[COpcNodeId(nsNode, idNode)];

	AfxAssert(uIndex);

	m_NodeList[uIndex - 1]->AddReference(nsRef, idRef, nsTarget, idTarget);
	}

void COpcDataModel::AddOrganizes(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)
{
	AddReference(nsNode, idNode, 0, OpcUaId_Organizes, nsTarget, idTarget);
	}

void COpcDataModel::AddProperty(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)
{
	AddReference(nsNode, idNode, 0, OpcUaId_HasProperty, nsTarget, idTarget);
	}

void COpcDataModel::AddComponent(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)
{
	AddReference(nsNode, idNode, 0, OpcUaId_HasComponent, nsTarget, idTarget);
	}

void COpcDataModel::AddMandatoryRule(UINT nsNode, UINT idNode)
{
	AddReference(nsNode, idNode, 0, OpcUaId_HasModellingRule, 0, OpcUaId_ModellingRule_Mandatory);
	}

// End of File
