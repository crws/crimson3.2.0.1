
#include "Intern.hpp"

#include "OpcVariableTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OpcDataModel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Variable Type Node
//

// Constructors

COpcVariableTypeNode::COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcVariableTypeNode::COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcVariableTypeNode::COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcVariableTypeNode::COpcVariableTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract) : COpcNode(pModel, classDataType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcVariableTypeNode::COpcVariableTypeNode(COpcVariableTypeNode const &That) : COpcNode(That)
{
	m_DataType  = That.m_DataType;
	
	m_Rank      = That.m_Rank;

	m_fAbstract = That.m_fAbstract;
	}

// Assignment

COpcVariableTypeNode COpcVariableTypeNode::operator = (COpcVariableTypeNode const &That)
{
	COpcNode::operator = (That);

	m_DataType  = That.m_DataType;
	
	m_Rank      = That.m_Rank;

	m_fAbstract = That.m_fAbstract;

	return ThisObject;
	}

// Operations

void COpcVariableTypeNode::SetDataType(UINT nsType, UINT idType)
{
	m_DataType = COpcNodeId(nsType, idType);
	}

void COpcVariableTypeNode::SetRank(INT Rank)
{
	m_Rank = Rank;
	}

// Validation

bool COpcVariableTypeNode::Validate(void)
{
	if( !m_pModel->HasDataType(m_DataType) ) {

		AfxTrace( "opc: node %s has bad data type of %s\n",
			  PCTXT(Describe(true)),
			  PCTXT(m_DataType.GetAsText())
			  );
		}

	return COpcNode::Validate();
	}

// End of File
