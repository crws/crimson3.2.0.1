
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcNode_HPP

#define INCLUDE_OpcNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNodeId.hpp"

#include "OpcReference.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Reference Array
//

typedef CArray <COpcReference *> COpcRefArray;

//////////////////////////////////////////////////////////////////////////
//
// Base Node
//

class COpcNode
{
public:
	// Classes
	enum
	{
		classUnspecified   = 0,
		classObject        = 1,
		classVariable      = 2,
		classMethod        = 4,
		classObjectType    = 8,
		classVariableType  = 16,
		classReferenceType = 32,
		classDataType      = 64,
		classView          = 128
	};

	// Constructors
	COpcNode(COpcNode const &That);
	COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, UINT Value);
	COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, CString const &Value);
	COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, CGuid const &Value);
	COpcNode(COpcDataModel *pModel, UINT Class, UINT Namespace, CByteArray const &Value);

	// Destructor
	~COpcNode(void);

	// Assignment
	COpcNode & operator = (COpcNode const &That);

	// Attributes
	CString               Describe(bool fClass) const;
	CString               GetClassAsText(void) const;
	COpcNodeId    const & GetId(void) const;
	CString               GetIdAsText(void) const;
	UINT		      GetClass(void) const;
	bool		      IsClass(UINT Class) const;
	CString               GetBrowseName(void) const;
	CString               GetDisplayName(void) const;
	UINT		      GetReferenceCount(void) const;
	COpcReference const & GetReference(UINT r) const;
	COpcNodeId    const & GetReferenceId(UINT r) const;
	COpcNode            * GetReferenceNode(UINT r) const;
	BOOL		      IsPrivate(void) const;

	// Operations
	void SetBrowseName(CString const &Name);
	void SetDisplayName(CString const &Name);
	void SetNames(CString const &Name);
	void AddReference(COpcNodeId const &Ref, COpcNodeId const &Target, bool fInverse);
	void AddReference(UINT nsRef, UINT idRef, UINT nsTarget, UINT idTarget);
	void AddInverses(void);

	// Validation
	virtual bool Validate(void);

protected:
	// Data Members
	COpcDataModel * m_pModel;
	COpcNodeId      m_Id;
	UINT	        m_Class;
	CString         m_BrowseName;
	CString         m_DisplayName;
	COpcRefArray    m_References;
	BOOL		m_fPrivate;
};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE COpcNodeId const & COpcNode::GetId(void) const
{
	return m_Id;
}

STRONG_INLINE CString COpcNode::GetIdAsText(void) const
{
	return m_Id.GetAsText();
}

STRONG_INLINE UINT COpcNode::GetClass(void) const
{
	return m_Class;
}

STRONG_INLINE bool COpcNode::IsClass(UINT Class) const
{
	return m_Class == Class;
}

STRONG_INLINE CString COpcNode::GetBrowseName(void) const
{
	return m_BrowseName;
}

STRONG_INLINE CString COpcNode::GetDisplayName(void) const
{
	return m_DisplayName;
}

STRONG_INLINE UINT COpcNode::GetReferenceCount(void) const
{
	return m_References.GetCount();
}

STRONG_INLINE COpcReference const & COpcNode::GetReference(UINT r) const
{
	return *m_References[r];
}

STRONG_INLINE COpcNodeId const & COpcNode::GetReferenceId(UINT r) const
{
	return m_References[r]->GetTargetId();
}

STRONG_INLINE BOOL COpcNode::IsPrivate(void) const
{
	return m_fPrivate;
}

// End of File

#endif
