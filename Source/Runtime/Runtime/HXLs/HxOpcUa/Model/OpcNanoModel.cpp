
#include "Intern.hpp"

#include "OpcNanoModel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Nano Data Model
//

// Operations

void COpcNanoModel::AddStandard(void)
{
	// Objects

	AddObject(0, OpcUaId_RootFolder,					0, OpcUaId_FolderType,				"Root"			);
	AddObject(0, OpcUaId_ObjectsFolder,					0, OpcUaId_FolderType,				"Objects"		);
	AddObject(0, OpcUaId_TypesFolder,					0, OpcUaId_FolderType,				"Types"			);
	AddObject(0, OpcUaId_ViewsFolder,					0, OpcUaId_FolderType,				"Views"			);
	AddObject(0, OpcUaId_Server,						0, OpcUaId_ServerType,				"Server"		);
	AddObject(0, OpcUaId_ReferenceTypesFolder,				0, OpcUaId_FolderType,				"ReferenceTypes"	);
	AddObject(0, OpcUaId_DataTypesFolder,					0, OpcUaId_FolderType,				"DataTypes"		);
	AddObject(0, OpcUaId_ObjectTypesFolder,					0, OpcUaId_FolderType,				"ObjectTypes"		);
	AddObject(0, OpcUaId_VariableTypesFolder,				0, OpcUaId_FolderType,				"VariableTypes"		);
	AddObject(0, OpcUaId_Server_ServerCapabilities,				0, OpcUaId_ServerType_ServerCapabilities,	"ServerCapabilities"	);
	AddObject(0, OpcUaId_Server_ServerDiagnostics,				0, OpcUaId_ServerType_ServerDiagnostics,	"ServerDiagnostics"	);
	AddObject(0, OpcUaId_Server_VendorServerInfo,				0, OpcUaId_ServerType_VendorServerInfo,		"VendorServerInfo"	);
	AddObject(0, OpcUaId_Server_ServerRedundancy,				0, OpcUaId_ServerType_ServerRedundancy,		"ServerRedundancy"	);
	AddObject(0, OpcUaId_Server_ServerCapabilities_ModellingRules,		0, OpcUaId_FolderType,				"ModellingRules"	);
	AddObject(0, OpcUaId_Server_ServerCapabilities_AggregateFunctions,	0, OpcUaId_FolderType,				"AggregateFunctions"	);
	AddObject(0, OpcUaId_ModellingRule_Mandatory,				0, OpcUaId_ModellingRuleType,			"Mandatory"		);
	AddObject(0, OpcUaId_ModellingRule_Optional,				0, OpcUaId_ModellingRuleType,			"Optional"		);

	// Organization

	AddOrganizes(0, OpcUaId_RootFolder,		0, OpcUaId_ObjectsFolder	);
	AddOrganizes(0, OpcUaId_RootFolder,		0, OpcUaId_TypesFolder		);
	AddOrganizes(0, OpcUaId_RootFolder,		0, OpcUaId_ViewsFolder		);
	AddOrganizes(0, OpcUaId_ObjectsFolder,		0, OpcUaId_Server		);
	AddOrganizes(0, OpcUaId_TypesFolder,		0, OpcUaId_ReferenceTypesFolder	);
	AddOrganizes(0, OpcUaId_TypesFolder,		0, OpcUaId_DataTypesFolder	);
	AddOrganizes(0, OpcUaId_TypesFolder,		0, OpcUaId_ObjectTypesFolder	);
	AddOrganizes(0, OpcUaId_TypesFolder,		0, OpcUaId_VariableTypesFolder	);
	AddOrganizes(0, OpcUaId_ReferenceTypesFolder,	0, OpcUaId_References		);
	AddOrganizes(0, OpcUaId_DataTypesFolder,	0, OpcUaId_BaseDataType		);
	AddOrganizes(0, OpcUaId_ObjectTypesFolder,	0, OpcUaId_BaseObjectType	);
	AddOrganizes(0, OpcUaId_VariableTypesFolder,	0, OpcUaId_BaseVariableType	);

	// Server

	AddProperty (0, OpcUaId_Server, 0, OpcUaId_Server_ServerArray		);
	AddProperty (0, OpcUaId_Server, 0, OpcUaId_Server_NamespaceArray	);
	AddProperty (0, OpcUaId_Server, 0, OpcUaId_Server_ServiceLevel		);
	AddProperty (0, OpcUaId_Server, 0, OpcUaId_Server_Auditing		);
	AddComponent(0, OpcUaId_Server, 0, OpcUaId_Server_ServerStatus		);
	AddComponent(0, OpcUaId_Server, 0, OpcUaId_Server_ServerCapabilities	);
	AddComponent(0, OpcUaId_Server, 0, OpcUaId_Server_ServerDiagnostics	);
	AddComponent(0, OpcUaId_Server, 0, OpcUaId_Server_VendorServerInfo	);
	AddComponent(0, OpcUaId_Server, 0, OpcUaId_Server_ServerRedundancy	);

	// Server Capabilities

	AddProperty (0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_ServerProfileArray		);
	AddProperty (0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_LocaleIdArray			);
	AddProperty (0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_MinSupportedSampleRate		);
	AddProperty (0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_MaxBrowseContinuationPoints	);
	AddProperty (0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_MaxQueryContinuationPoints	);
	AddProperty (0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_MaxHistoryContinuationPoints	);
	AddProperty (0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_SoftwareCertificates		);
	AddComponent(0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_ModellingRules			);
	AddComponent(0, OpcUaId_Server_ServerCapabilities, 0, OpcUaId_Server_ServerCapabilities_AggregateFunctions		);

	// Server Diagnostics

	AddProperty(0, OpcUaId_Server_ServerDiagnostics, 0, OpcUaId_Server_ServerDiagnostics_EnabledFlag);

	// Server Redundancy

	AddProperty(0, OpcUaId_Server_ServerRedundancy, 0, OpcUaId_Server_ServerRedundancy_RedundancySupport);

	// Object Types

	AddObjectType(0, OpcUaId_BaseObjectType,		  0, 0,				"BaseObjectType",		   true );
	AddObjectType(0, OpcUaId_FolderType,			  0, OpcUaId_BaseObjectType,	"FolderType",			   false);
	AddObjectType(0, OpcUaId_ServerType,			  0, OpcUaId_BaseObjectType,	"ServerType",			   false);
	AddObjectType(0, OpcUaId_ModellingRuleType,		  0, OpcUaId_BaseObjectType,	"ModellingRuleType",		   false);
	AddObjectType(0, OpcUaId_ServerType_ServerCapabilities,	  0, OpcUaId_BaseObjectType,	"ServerCapabilitiesType",	   false);
	AddObjectType(0, OpcUaId_ServerType_ServerDiagnostics,	  0, OpcUaId_BaseObjectType,	"ServerDiagnosticsType",	   false);
	AddObjectType(0, OpcUaId_ServerType_VendorServerInfo,	  0, OpcUaId_BaseObjectType,	"VendorServerInfoType",		   false);
	AddObjectType(0, OpcUaId_ServerType_ServerRedundancy,	  0, OpcUaId_BaseObjectType,	"ServerRedundancyType",		   false);
	AddObjectType(0, OpcUaId_HistoricalDataConfigurationType, 0, OpcUaId_BaseObjectType,	"HistoricalDataConfigurationType", false);

	// Modelling Rules

	AddComponent(0, OpcUaId_ModellingRuleType, 0, OpcUaId_ModellingRule_Mandatory);
	AddComponent(0, OpcUaId_ModellingRuleType, 0, OpcUaId_ModellingRule_Optional );

	// Data Types

	AddDataType(0, OpcUaId_BaseDataType,	0, 0,				"BaseDataType", true);
	AddDataType(0, OpcUaId_Enumeration,	0, OpcUaId_BaseDataType,	"Enumeration",  true);
	AddDataType(0, OpcUaId_ServerState,	0, OpcUaId_Enumeration,		"ServerState",  true);

	// Reference Types

	AddReferenceType(0, OpcUaId_References,			0, 0,					"References",			true,	true );
	AddReferenceType(0, OpcUaId_HierarchicalReferences,	0, OpcUaId_References,			"HierarchicalReferences",	true,	false);
	AddReferenceType(0, OpcUaId_NonHierarchicalReferences,	0, OpcUaId_References,			"NonHierarchicalReferences",	true,	true );
	AddReferenceType(0, OpcUaId_HasChild,			0, OpcUaId_HierarchicalReferences,	"HasChild",			true,	false);
	AddReferenceType(0, OpcUaId_Organizes,			0, OpcUaId_HierarchicalReferences,	"Organizes",			false,	false);
	AddReferenceType(0, OpcUaId_HasEventSource,		0, OpcUaId_HierarchicalReferences,	"HasEventSource",		false,	false);
	AddReferenceType(0, OpcUaId_HasNotifier,		0, OpcUaId_HasEventSource,		"HasNotifier",			false,	false);
	AddReferenceType(0, OpcUaId_Aggregates,			0, OpcUaId_HasChild,			"Aggregates",			true,	false);
	AddReferenceType(0, OpcUaId_HasSubtype,			0, OpcUaId_HasChild,			"HasSubtype",			false,	false);
	AddReferenceType(0, OpcUaId_HasProperty,		0, OpcUaId_Aggregates,			"HasProperty",			false,	false);
	AddReferenceType(0, OpcUaId_HasComponent,		0, OpcUaId_Aggregates,			"HasComponent",			false,	false);
	AddReferenceType(0, OpcUaId_HasHistoricalConfiguration,	0, OpcUaId_Aggregates,			"HasHistoricalConfiguration",	false,	false);
	AddReferenceType(0, OpcUaId_HasOrderedComponent,	0, OpcUaId_HasComponent,		"HasOrderedComponent",		false,	false);
	AddReferenceType(0, OpcUaId_HasTypeDefinition,		0, OpcUaId_NonHierarchicalReferences,	"HasTypeDefinition",		false,	false);
	AddReferenceType(0, OpcUaId_HasModellingRule,		0, OpcUaId_NonHierarchicalReferences,	"HasModellingRule",		false,	false);

	// Variable Types

	AddVariableType(0, OpcUaId_BaseVariableType,		0, 0,					"BaseVariableType",		0, OpcUaId_BaseDataType,	9,	true);
	AddVariableType(0, OpcUaId_PropertyType,		0, OpcUaId_BaseVariableType,		"PropertyType",			0, OpcUaId_BaseDataType,	0,	false);
	AddVariableType(0, OpcUaId_BaseDataVariableType,	0, OpcUaId_BaseVariableType,		"BaseDataVariableType",		0, OpcUaId_BaseDataType,	0,	false);
	AddVariableType(0, OpcUaId_BuildInfoType,		0, OpcUaId_BaseDataVariableType,	"BuildInfoType",		0, OpcUaId_BuildInfo,		0,	false);
	AddVariableType(0, OpcUaId_ServerStatusType,		0, OpcUaId_BaseDataVariableType,	"ServerStatusType",		0, OpcUaId_BuildInfo,		0,	false);

	// Variables

	AddVariable(0, OpcUaId_Server_ServerStatus,					0, OpcUaId_ServerStatusType,		"ServerStatus",			0, OpcUaId_ServerStatusDataType,	0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_State,				0, OpcUaId_BaseDataVariableType,	"State",			0, OpcUaId_ServerState,			0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_ServerState_EnumStrings,					0, OpcUaId_ServerState,			"EnumStrings",			0, OpcUaId_String,			1, 8,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerArray,					0, OpcUaId_PropertyType,		"ServerArray",			0, OpcUaId_String,			1, 1,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_NamespaceArray,					0, OpcUaId_PropertyType,		"NamespaceArray",		0, OpcUaId_String,			1, 2,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerCapabilities_ServerProfileArray,		0, OpcUaId_PropertyType,		"ServerProfileArray",		0, OpcUaId_String,			1, 1,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerCapabilities_LocaleIdArray,			0, OpcUaId_PropertyType,		"LocaleIdArray",		0, OpcUaId_LocaleId,			1, 1,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerCapabilities_MinSupportedSampleRate,	0, OpcUaId_PropertyType,		"MinSupportedSampleRate",	0, OpcUaId_Duration,			0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerCapabilities_MaxBrowseContinuationPoints,	0, OpcUaId_PropertyType,		"MaxBrowseContinuationPoints",	0, OpcUaId_UInt32,			0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerCapabilities_MaxQueryContinuationPoints,	0, OpcUaId_PropertyType,		"MaxQueryContinuationPoints",	0, OpcUaId_Int16,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerCapabilities_MaxHistoryContinuationPoints,	0, OpcUaId_PropertyType,		"MaxHistoryContinuationPoints",	0, OpcUaId_Int16,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerCapabilities_SoftwareCertificates,		0, OpcUaId_PropertyType,		"SoftwareCertificates",		0, OpcUaId_SignedSoftwareCertificate,	0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServiceLevel,					0, OpcUaId_PropertyType,		"ServiceLevel",			0, OpcUaId_Byte,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_StartTime,				0, OpcUaId_BaseDataVariableType,	"StartTime",			0, OpcUaId_UtcTime,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_ShutdownReason,			0, OpcUaId_BaseDataVariableType,	"ShutdownReason",		0, OpcUaId_Byte,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_SecondsTillShutdown,			0, OpcUaId_BaseDataVariableType,	"SecondsTillShutdown",		0, OpcUaId_Byte,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_CurrentTime,				0, OpcUaId_BaseDataVariableType,	"CurrentTime",			0, OpcUaId_UtcTime,			0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_BuildInfo,				0, OpcUaId_BuildInfoType,		"BuildInfo",			0, OpcUaId_Byte,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_BuildInfo_BuildDate,			0, OpcUaId_BaseDataVariableType,	"BuildDate",			0, OpcUaId_UtcTime,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_BuildInfo_BuildNumber,		0, OpcUaId_BaseDataVariableType,	"BuildNumber",			0, OpcUaId_String,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_BuildInfo_ManufacturerName,		0, OpcUaId_BaseDataVariableType,	"ManufacturerName",		0, OpcUaId_String,			0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_BuildInfo_ProductName,		0, OpcUaId_BaseDataVariableType,	"ProductName",			0, OpcUaId_String,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_BuildInfo_ProductUri,		0, OpcUaId_BaseDataVariableType,	"ProductUri",			0, OpcUaId_String,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_ServerStatus_BuildInfo_SoftwareVersion,		0, OpcUaId_BaseDataVariableType,	"SoftwareVersion",		0, OpcUaId_String,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_Server_Auditing,						0, OpcUaId_PropertyType,		"Auditing",			0, OpcUaId_Boolean,			0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerDiagnostics_EnabledFlag,			0, OpcUaId_PropertyType,		"EnabledFlag",			0, OpcUaId_Boolean,			0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	AddVariable(0, OpcUaId_Server_ServerRedundancy_RedundancySupport,		0, OpcUaId_PropertyType,		"RedundancySupport",		0, OpcUaId_RedundancySupport,		0, 0,		OpcUa_AccessLevels_CurrentRead,		false);
	
/*	AddVariable(0, OpcUaId_ServerStatusType_State,					0, OpcUaId_BaseDataVariableType,	"State",			0, OpcUaId_ServerState,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_ServerStatusType_StartTime,				0, OpcUaId_BaseDataVariableType,	"StartTime",			0, OpcUaId_UtcTime,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_ServerStatusType_ShutdownReason,				0, OpcUaId_BaseDataVariableType,	"ShutdownReason",		0, OpcUaId_LocalizedText,		0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_ServerStatusType_SecondsTillShutdown,			0, OpcUaId_BaseDataVariableType,	"SecondsTillShutdown",		0, OpcUaId_UInt32,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_ServerStatusType_CurrentTime,				0, OpcUaId_BaseDataVariableType,	"CurrentTime",			0, OpcUaId_UtcTime,			0, 0,		OpcUa_AccessLevels_None,		false);
	AddVariable(0, OpcUaId_ServerStatusType_BuildInfo,				0, OpcUaId_BuildInfoType,		"BuildInfo",			0, OpcUaId_BuildInfo,			0, 0,		OpcUa_AccessLevels_None,		false);

	// Modelling Rules

	AddMandatoryRule(0, OpcUaId_ServerStatusType_State);
	AddMandatoryRule(0, OpcUaId_ServerStatusType_StartTime);
	AddMandatoryRule(0, OpcUaId_ServerStatusType_ShutdownReason);
	AddMandatoryRule(0, OpcUaId_ServerStatusType_SecondsTillShutdown);
	AddMandatoryRule(0, OpcUaId_ServerStatusType_CurrentTime);
	AddMandatoryRule(0, OpcUaId_ServerStatusType_BuildInfo);

*/	// Server Status

	AddComponent(0, OpcUaId_Server_ServerStatus, 0, OpcUaId_Server_ServerStatus_State);
	AddComponent(0, OpcUaId_Server_ServerStatus, 0, OpcUaId_Server_ServerStatus_StartTime);
	AddComponent(0, OpcUaId_Server_ServerStatus, 0, OpcUaId_Server_ServerStatus_ShutdownReason);
	AddComponent(0, OpcUaId_Server_ServerStatus, 0, OpcUaId_Server_ServerStatus_SecondsTillShutdown);
	AddComponent(0, OpcUaId_Server_ServerStatus, 0, OpcUaId_Server_ServerStatus_CurrentTime);
	AddComponent(0, OpcUaId_Server_ServerStatus, 0, OpcUaId_Server_ServerStatus_BuildInfo);

	// Build Info

	AddComponent(0, OpcUaId_Server_ServerStatus_BuildInfo, 0, OpcUaId_Server_ServerStatus_BuildInfo_BuildDate);	
	AddComponent(0, OpcUaId_Server_ServerStatus_BuildInfo, 0, OpcUaId_Server_ServerStatus_BuildInfo_BuildNumber);
	AddComponent(0, OpcUaId_Server_ServerStatus_BuildInfo, 0, OpcUaId_Server_ServerStatus_BuildInfo_ManufacturerName);
	AddComponent(0, OpcUaId_Server_ServerStatus_BuildInfo, 0, OpcUaId_Server_ServerStatus_BuildInfo_ProductName);	
	AddComponent(0, OpcUaId_Server_ServerStatus_BuildInfo, 0, OpcUaId_Server_ServerStatus_BuildInfo_ProductUri);
	AddComponent(0, OpcUaId_Server_ServerStatus_BuildInfo, 0, OpcUaId_Server_ServerStatus_BuildInfo_SoftwareVersion);

/*	// Server Status Type

	AddComponent(0, OpcUaId_ServerStatusType, 0, OpcUaId_ServerStatusType_State);
	AddComponent(0, OpcUaId_ServerStatusType, 0, OpcUaId_ServerStatusType_StartTime);
	AddComponent(0, OpcUaId_ServerStatusType, 0, OpcUaId_ServerStatusType_ShutdownReason);
	AddComponent(0, OpcUaId_ServerStatusType, 0, OpcUaId_ServerStatusType_SecondsTillShutdown);
	AddComponent(0, OpcUaId_ServerStatusType, 0, OpcUaId_ServerStatusType_CurrentTime);
	AddComponent(0, OpcUaId_ServerStatusType, 0, OpcUaId_ServerStatusType_BuildInfo);
*/	}

// End of File
