
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcReferenceTypeNode_HPP

#define INCLUDE_OpcReferenceTypeNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Reference Type Node
//

class COpcReferenceTypeNode : public COpcNode
{
	public:
		// Constructors
		COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract, bool fSymmetric);
		COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract, bool fSymmetric);
		COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract, bool fSymmetric);
		COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract, bool fSymmetric);
		COpcReferenceTypeNode(COpcReferenceTypeNode const &That);

		// Assignment
		COpcReferenceTypeNode operator = (COpcReferenceTypeNode const &That);

		// Attributes
		bool    NeedInverse(void) const;
		bool    IsAbstract(void) const;
		bool    IsSymmetric(void) const;
		CString GetInverseName(void) const;
		CString GetInverseLocale(void) const;

		// Operations
		void FindInverses(void);

	protected:
		// Data Members
		bool    m_fAbstract;
		bool    m_fSymmetric;
		CString m_BrowseInverse;
		CString m_DisplayInverse;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE bool COpcReferenceTypeNode::NeedInverse(void) const
{
	return !m_fAbstract && !m_fSymmetric;
	}

STRONG_INLINE bool COpcReferenceTypeNode::IsAbstract(void) const
{
	return m_fAbstract;
	}

STRONG_INLINE bool COpcReferenceTypeNode::IsSymmetric(void) const
{
	return m_fSymmetric;
	}

STRONG_INLINE CString COpcReferenceTypeNode::GetInverseName(void) const
{
	return m_DisplayInverse;
	}

STRONG_INLINE CString COpcReferenceTypeNode::GetInverseLocale(void) const
{
	return "en";
	}

// End of File

#endif
