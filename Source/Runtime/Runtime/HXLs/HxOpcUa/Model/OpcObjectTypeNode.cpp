
#include "Intern.hpp"

#include "OpcObjectTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Type Node
//

// Constructors

COpcObjectTypeNode::COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract) : COpcNode(pModel, classObjectType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcObjectTypeNode::COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract) : COpcNode(pModel, classObjectType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcObjectTypeNode::COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract) : COpcNode(pModel, classObjectType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcObjectTypeNode::COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract) : COpcNode(pModel, classObjectType, Namespace, Value)
{
	m_fAbstract = fAbstract;
	}

COpcObjectTypeNode::COpcObjectTypeNode(COpcObjectTypeNode const &That) : COpcNode(That)
{
	m_fAbstract = That.m_fAbstract;
	}

// Assignment

COpcObjectTypeNode COpcObjectTypeNode::operator = (COpcObjectTypeNode const &That)
{
	COpcNode::operator = (That);

	m_fAbstract = That.m_fAbstract;

	return ThisObject;
	}

// End of File
