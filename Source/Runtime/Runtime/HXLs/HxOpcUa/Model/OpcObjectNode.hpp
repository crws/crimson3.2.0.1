
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcObjectNode_HPP

#define INCLUDE_OpcObjectNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Node
//

class COpcObjectNode : public COpcNode
{
	public:
		// Constructors
		COpcObjectNode(COpcDataModel *pModel, UINT Namespace, UINT Value, UINT Notifier);
		COpcObjectNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, UINT Notifier);
		COpcObjectNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, UINT Notifier);
		COpcObjectNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, UINT Notifier);
		COpcObjectNode(COpcObjectNode const &That);

		// Assignment
		COpcObjectNode operator = (COpcObjectNode const &That);

		// Attributes
		BYTE GetEventNotifier(void) const;

	protected:
		// Data Members
		UINT m_Notifier;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE BYTE COpcObjectNode::GetEventNotifier(void) const
{
	return BYTE(m_Notifier);
	}

// End of File

#endif
