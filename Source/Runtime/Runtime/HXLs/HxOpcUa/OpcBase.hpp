
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcBase_HPP

#define INCLUDE_OpcBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Model/OpcNodeId.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Base Class
//

class COpcBase
{
	protected:
		// Platform Data
		static OpcUa_Handle  m_hPlatform;
		static UINT volatile m_uPlatRefs;
		static HTHREAD	     m_hInitTask;

		// Platform Operations
		static void BaseInit(void);
		static void BaseStop(void);
		static void BaseTerm(void);

		// Platform Config
		static void LoadConfig(OpcUa_ProxyStubConfiguration &Config);

		// Time Support
		static void  TimeFromTimeval(OpcUa_DateTime &t, timeval const &time);
		static void  TimeFromUnix(OpcUa_DateTime &t, DWORD time);
		static DWORD UnixFromTime(OpcUa_DateTime const &t, DWORD def);

		// Copy Helpers
		static void CopyNotificationMessage(OpcUa_NotificationMessage *pDest,  OpcUa_NotificationMessage const *pFrom);
		static void CopyExtensionObject(OpcUa_ExtensionObject *pDest, OpcUa_ExtensionObject const *pFrom);
		static void CopyDataChangeNotification(OpcUa_DataChangeNotification *pDest, OpcUa_DataChangeNotification const *pFrom);
		static void CopyEventNotificationList(OpcUa_EventNotificationList *pDest, OpcUa_EventNotificationList const *pFrom);
		static void CopyMonitoredItemNotification(OpcUa_MonitoredItemNotification *pDest, OpcUa_MonitoredItemNotification const *pFrom);
		static void CopyEventFieldList(OpcUa_EventFieldList *pDest, OpcUa_EventFieldList const *pFrom);
		static void CopyDataValue(OpcUa_DataValue *pDest, OpcUa_DataValue const *pFrom);
		static void CopyVariant(OpcUa_Variant *pDest, OpcUa_Variant const *pFrom);
		static void CopyExpandedNodeId(OpcUa_ExpandedNodeId *pDest, OpcUa_ExpandedNodeId const *pFrom);
		static void CopyNodeId(OpcUa_NodeId *pDest, OpcUa_NodeId const *pFrom);

		// UTF Support
		static PTXT UtfEncode(PCUTF s);
		static PUTF UtfDecode(PCTXT s);
	};

// End of File

#endif
