
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../Model/OpcDataModel.hpp"

#include "../Model/OpcNode.hpp"

#include "../Model/OpcVariableNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Services

OpcUa_StatusCode COpcServer::Write( OpcUa_Endpoint              hEndpoint,
				    OpcUa_Handle                hContext,
				    const OpcUa_RequestHeader * pRequestHeader,
				    OpcUa_Int32                 nNoOfNodesToWrite,
				    const OpcUa_WriteValue *    pNodesToWrite,
				    OpcUa_ResponseHeader *      pResponseHeader,
				    OpcUa_Int32 *               pNoOfResults,
				    OpcUa_StatusCode **         pResults,
				    OpcUa_Int32 *               pNoOfDiagnosticInfos,
				    OpcUa_DiagnosticInfo **     pDiagnosticInfos
				    )
{
	AfxTrace("opc: write\n");

	*pNoOfDiagnosticInfos = 0;
	
	*pDiagnosticInfos     = OpcUa_Null;

	UINT n;

	if( (n = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, false)) < NOTHING ) {

		CSession &Session = m_Session[n];

		if( !nNoOfNodesToWrite ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);

			return OpcUa_BadNothingToDo;
			}
		else {
			*pResults = OpcAlloc(nNoOfNodesToWrite, OpcUa_StatusCode);

			for( int m = 0; m < nNoOfNodesToWrite; m++ ) {
				
				OpcUa_WriteValue const *pWrite  = pNodesToWrite + m;

				OpcUa_StatusCode       *pResult = *pResults     + m;

				WriteNode(Session, pResult, pWrite, m);
				}

			*pNoOfResults = nNoOfNodesToWrite;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

			return OpcUa_Good;
			}
		}

	return pResponseHeader->ServiceResult;
	}

// Write Helpers

void COpcServer::WriteNode(CSession &Session, OpcUa_StatusCode *pResult, OpcUa_WriteValue const *pWrite, UINT uIndex)
{
	m_pModel->Lock();

	COpcNodeId const &NodeId = pWrite->NodeId;

	COpcNode         *pNode  = m_pModel->FindNode(NodeId, true);

	if( pNode ) {

		AfxTrace( "    %2u) %s.%u (%s)\n",
			  uIndex,
			  PCTXT(pNode->GetId().GetAsText()),
			  pWrite->AttributeId,
			  PCTXT(pNode->GetBrowseName())
			  );

		if( pNode->GetClass() == OpcUa_NodeClass_Variable ) {

			COpcVariableNode *pVar = (COpcVariableNode *) pNode;

			if( pWrite->AttributeId == OpcUa_Attributes_Value ) {

				*pResult = WriteValue(Session, pVar, &pWrite->Value.Value);

				m_pModel->Free();

				return;
				}
			}

		*pResult = OpcUa_BadWriteNotSupported;

		m_pModel->Free();

		return;
		}

	*pResult = OpcUa_BadNodeIdUnknown;

	m_pModel->Free();
	}

UINT COpcServer::WriteValue(CSession &Session, COpcVariableNode *pNode, OpcUa_Variant const *pValue)
{
	if( pNode->GetId().IsType(OpcUa_IdType_Numeric) ) {

		UINT ns = pNode->GetId().GetNamespace();
	
		UINT id = pNode->GetId().GetNumericValue();

		if( pNode->GetValueRank() == OpcUa_ValueRanks_Scalar ) {

			UINT uType = GetWireType(pNode->GetDataType().GetNumericValue());

			if( uType == pValue->Datatype ) {

				SetValue(pNode, Session.m_uIndex, ns, id, uType, &pValue->Value);

				return OpcUa_Good;
				}

			return OpcUa_BadTypeMismatch;
			}

		return OpcUa_Good;
		}

	return OpcUa_BadWriteNotSupported;
	}

void COpcServer::SetValue(COpcVariableNode *pNode, UINT si, UINT ns, UINT id, UINT uType, OpcUa_VariantUnion const *pValue)
{
	switch( uType ) {

		case OpcUaId_Double:

			SetValueDouble(si, ns, id, 0, pValue->Double);
				
			break;

		case OpcUaId_String:

			SetValueString(si, ns, id, 0, pValue->String.strContent);

			break;

		case OpcUaId_Int32:

			SetValueInt(si, ns, id, 0, pValue->Int32);

			break;

		case OpcUaId_Int64:

			SetValueInt64(si, ns, id, 0, pValue->Int64);

			break;

		default:

			AfxAssert(FALSE);
		}
	}

// End of File
