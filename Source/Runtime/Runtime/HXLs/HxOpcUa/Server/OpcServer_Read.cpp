
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../Model/OpcDataModel.hpp"
#include "../Model/OpcDataTypeNode.hpp"
#include "../Model/OpcNode.hpp"
#include "../Model/OpcObjectNode.hpp"
#include "../Model/OpcObjectTypeNode.hpp"
#include "../Model/OpcReference.hpp"
#include "../Model/OpcReferenceTypeNode.hpp"
#include "../Model/OpcVariableNode.hpp"
#include "../Model/OpcVariableTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#define AssignScalar(t, f)				\
							\
	case OpcUaId_##t:				\
	pValue->t = OpcUa_##t(f(si, ns, id, 0));	\
	break						\

#define AllocArray(t)								\
										\
	case OpcUaId_##t:							\
	pResult->Value.Value.Array.Value.t##Array = OpcAlloc(uSize, OpcUa_##t);	\
	break									\

#define AssignArray(t, f)					\
								\
	case OpcUaId_##t:					\
	for( n = 0; n < uSize; n++ ) {				\
	pValue->t##Array[n] = OpcUa_##t(f(si, ns, id, n));	\
	}							\
	break							\

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Services

OpcUa_StatusCode COpcServer::Read( OpcUa_Endpoint              hEndpoint,
				   OpcUa_Handle                hContext,
				   const OpcUa_RequestHeader * pRequestHeader,
				   OpcUa_Double                nMaxAge,
				   OpcUa_TimestampsToReturn    eTimestampsToReturn,
				   OpcUa_Int32                 nNoOfNodesToRead,
				   const OpcUa_ReadValueId *   pNodesToRead,
				   OpcUa_ResponseHeader *      pResponseHeader,
				   OpcUa_Int32 *               pNoOfResults,
				   OpcUa_DataValue **          pResults,
				   OpcUa_Int32 *               pNoOfDiagnosticInfos,
				   OpcUa_DiagnosticInfo **     pDiagnosticInfos
				   )
{
	AfxTrace("opc: read\n");

	*pNoOfDiagnosticInfos = 0;
	
	*pDiagnosticInfos     = OpcUa_Null;

	UINT n;

	if( (n = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, false)) < NOTHING ) {

		CSession &Session = m_Session[n];

		if( !nNoOfNodesToRead ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);

			return OpcUa_BadNothingToDo;
			}
		else {
			*pResults = OpcAlloc(nNoOfNodesToRead, OpcUa_DataValue);

			for( int m = 0; m < nNoOfNodesToRead; m++ ) {
				
				OpcUa_ReadValueId const *pRead   = pNodesToRead + m;

				OpcUa_DataValue         *pResult = *pResults    + m;

				ReadNode(Session, pResult, pRead, eTimestampsToReturn, m);
				}

			*pNoOfResults = nNoOfNodesToRead;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

			return OpcUa_Good;
			}
		}

	return pResponseHeader->ServiceResult;
	}

// Read Helpers

void COpcServer::ReadNode(CSession &Session, OpcUa_DataValue *pResult, OpcUa_ReadValueId const *pRead, OpcUa_TimestampsToReturn eTimestampsToReturn, UINT uIndex)
{
	m_pModel->Lock();

	COpcNodeId const &NodeId = pRead->NodeId;

	COpcNode         *pNode  = m_pModel->FindNode(NodeId, true);

	OpcUa_DataValue_Initialize(pResult);

	if( pNode ) {

		pResult->StatusCode = OpcUa_BadAttributeIdInvalid;

		if( uIndex < NOTHING ) {

			/*AfxTrace( "    %2u) %s.%u (%s)\n",
				  uIndex,
				  PCTXT(pNode->GetId().GetAsText()),
				  pRead->AttributeId,
				  PCTXT(pNode->GetBrowseName())
				  );*/
			}

		if( pRead->AttributeId >= 1 && pRead->AttributeId <= 7 ) {

			if( pRead->AttributeId == OpcUa_Attributes_NodeId ) {

				pResult->Value.Value.NodeId  = OpcAlloc(1, OpcUa_NodeId);

				*pResult->Value.Value.NodeId = pNode->GetId();

				FillArrayType(pResult, OpcUaId_NodeId, OpcUa_VariantArrayType_Scalar, 0);

				pResult->StatusCode = OpcUa_Good;
				}

			if( pRead->AttributeId == OpcUa_Attributes_NodeClass ) {

				pResult->Value.Value.Int32 = pNode->GetClass();

				FillArrayType(pResult, OpcUaId_Int32, OpcUa_VariantArrayType_Scalar, 0);

				pResult->StatusCode = OpcUa_Good;
				}

			if( pRead->AttributeId == OpcUa_Attributes_BrowseName ) {

				pResult->Value.Value.QualifiedName = OpcAlloc(1, OpcUa_QualifiedName);

				OpcUa_QualifiedName_Initialize(pResult->Value.Value.QualifiedName);

				OpcUa_String_AttachCopy(&pResult->Value.Value.QualifiedName->Name, PTXT(PCTXT(pNode->GetBrowseName())));

				pResult->Value.Value.QualifiedName->NamespaceIndex = pNode->GetId().GetNamespace();

				FillArrayType(pResult, OpcUaId_QualifiedName, OpcUa_VariantArrayType_Scalar, 0);
						
				pResult->StatusCode = OpcUa_Good;
				}

			if( pRead->AttributeId == OpcUa_Attributes_DisplayName ) {

				pResult->Value.Value.LocalizedText = OpcAlloc(1, OpcUa_LocalizedText);

				OpcUa_LocalizedText_Initialize(pResult->Value.Value.LocalizedText);

				OpcUa_String_AttachCopy(&pResult->Value.Value.LocalizedText->Text,   PTXT(PCTXT(pNode->GetDisplayName())));

				OpcUa_String_AttachCopy(&pResult->Value.Value.LocalizedText->Locale, "en");

				FillArrayType(pResult, OpcUaId_LocalizedText, OpcUa_VariantArrayType_Scalar, 0);

				pResult->StatusCode = OpcUa_Good;
				}

			if( pRead->AttributeId == OpcUa_Attributes_Description ) {

				if( pNode->GetClass() == OpcUa_NodeClass_Variable || pNode->GetClass() == OpcUa_NodeClass_Object ) {

					CString Desc;

					FetchDesc(Desc, NodeId);

					pResult->Value.Value.LocalizedText = OpcAlloc(1, OpcUa_LocalizedText);

					OpcUa_LocalizedText_Initialize(pResult->Value.Value.LocalizedText);

					OpcUa_String_AttachCopy(&pResult->Value.Value.LocalizedText->Text,   PTXT(PCTXT(Desc)));

					OpcUa_String_AttachCopy(&pResult->Value.Value.LocalizedText->Locale, "en");

					FillArrayType(pResult, OpcUaId_LocalizedText, OpcUa_VariantArrayType_Scalar, 0);

					pResult->StatusCode = OpcUa_Good;
					}
				else
					pResult->StatusCode = OpcUa_BadNotReadable;
				}
						
			if( pRead->AttributeId == OpcUa_Attributes_WriteMask || pRead->AttributeId == OpcUa_Attributes_UserWriteMask ) {

				pResult->Value.Value.UInt32 = 0;

				FillArrayType(pResult, OpcUaId_UInt32, OpcUa_VariantArrayType_Scalar, 0);

				pResult->StatusCode = OpcUa_Good;
				}
			}
		else {
			if( pNode->GetClass() == OpcUa_NodeClass_Variable ) {

				COpcVariableNode *pVar = (COpcVariableNode *) pNode;

				if( pRead->AttributeId >= 13 && pRead->AttributeId <= 20 ) {

					if( pRead->AttributeId == OpcUa_Attributes_Value ) {

						pResult->StatusCode = FetchValue(Session, pVar, OpcUa_Null, pResult);

						AssignTimestamp(pResult, eTimestampsToReturn, NodeId);
						}

					if( pRead->AttributeId == OpcUa_Attributes_DataType ) {

						pResult->Value.Value.NodeId = OpcAlloc(1, OpcUa_NodeId);

						OpcUa_NodeId_Initialize(pResult->Value.Value.NodeId);

						*pResult->Value.Value.NodeId = pVar->GetDataType();

						FillArrayType(pResult, OpcUaId_NodeId, OpcUa_VariantArrayType_Scalar, 0);

						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_ValueRank ) {

						pResult->Value.Value.Int32 = pVar->GetValueRank();

						FillArrayType(pResult, OpcUaId_Int32, OpcUa_VariantArrayType_Scalar, 0);

						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_ArrayDimensions ) {

						if( pVar->GetValueRank() != OpcUa_ValueRanks_Scalar ) {

							UINT n = pVar->GetArrayDimensions();

							pResult->Value.Value.Array.Value.UInt32Array = OpcAlloc(n, OpcUa_UInt32);

							for( UINT d = 0; d < n; d++ ) {

								pResult->Value.Value.Array.Value.UInt32Array[d] = pVar->GetArrayDimension(1+d);
								}

							FillArrayType(pResult, OpcUaId_UInt32, OpcUa_VariantArrayType_Array, n);
							}

						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_AccessLevel ) {

						pResult->Value.Value.Byte = pVar->GetAccessLevel();
									
 						FillArrayType(pResult, OpcUaId_Byte, OpcUa_VariantArrayType_Scalar, 0);
									
						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_UserAccessLevel ) {
									
						pResult->Value.Value.Byte = pVar->GetUserAccessLevel();

						FillArrayType(pResult, OpcUaId_Byte, OpcUa_VariantArrayType_Scalar, 0);

						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_MinimumSamplingInterval ) {

						pResult->Value.Value.Double = OpcUa_MinimumSamplingIntervals_Indeterminate;

						FillArrayType(pResult,OpcUaId_Double, OpcUa_VariantArrayType_Scalar, 0);
									
						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_Historizing ) {

						pResult->Value.Value.Boolean = pVar->IsHistorizing();

						FillArrayType(pResult, OpcUaId_Boolean, OpcUa_VariantArrayType_Scalar, 0);

						pResult->StatusCode = OpcUa_Good;
						}
					}
				}

			if( pNode->GetClass() == OpcUa_NodeClass_VariableType ) {

				COpcVariableTypeNode *pType = (COpcVariableTypeNode *) pNode;

				if( pRead->AttributeId == 8 || (pRead->AttributeId >= 13 && pRead->AttributeId <= 17) ) {

					if( pRead->AttributeId == OpcUa_Attributes_Value ) {

						pResult->StatusCode = FetchValue(Session, pType, OpcUa_Null, pResult);

						AssignTimestamp(pResult, eTimestampsToReturn, NodeId);
						}

					if( pRead->AttributeId == OpcUa_Attributes_DataType ) {

						pResult->Value.Value.NodeId = OpcAlloc(1, OpcUa_NodeId);

						OpcUa_NodeId_Initialize(pResult->Value.Value.NodeId);

						*pResult->Value.Value.NodeId= pType->GetDataType();

						FillArrayType(pResult, OpcUaId_NodeId, OpcUa_VariantArrayType_Scalar, 0);

						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_ArrayDimensions ) {

						pResult->Value.Value.UInt32 = pType->GetArrayDimensions();

						FillArrayType(pResult, OpcUaId_UInt32, OpcUa_VariantArrayType_Scalar, 0);

						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_IsAbstract ) {

						pResult->Value.Value.Boolean = pType->IsAbstract();

						FillArrayType(pResult, OpcUaId_Boolean, OpcUa_VariantArrayType_Scalar, 0);
								
						pResult->StatusCode = OpcUa_Good;
						}
					}
				}

			if( pNode->GetClass() == OpcUa_NodeClass_Object ) {

				COpcObjectNode *pObj = (COpcObjectNode *) pNode;

				if( pRead->AttributeId == OpcUa_Attributes_EventNotifier ) {

					pResult->Value.Value.Byte = pObj->GetEventNotifier();

					FillArrayType(pResult, OpcUaId_Byte, OpcUa_VariantArrayType_Scalar, 0);

					pResult->StatusCode = OpcUa_Good;
					}
				}

			if( pNode->GetClass() == OpcUa_NodeClass_ObjectType ) {

				COpcObjectTypeNode *pType = (COpcObjectTypeNode *) pNode;

				if( pRead->AttributeId == OpcUa_Attributes_IsAbstract ) {

					pResult->Value.Value.Boolean = pType->IsAbstract();

					FillArrayType(pResult, OpcUaId_Boolean, OpcUa_VariantArrayType_Scalar, 0);

					pResult->StatusCode = OpcUa_Good;
					}
				}

			if( pNode->GetClass() == OpcUa_NodeClass_DataType ) {

				COpcDataTypeNode *pType = (COpcDataTypeNode *) pNode;

				if( pRead->AttributeId == OpcUa_Attributes_IsAbstract ) {

					pResult->Value.Value.Boolean = pType->IsAbstract();

					FillArrayType(pResult, OpcUaId_Boolean, OpcUa_VariantArrayType_Scalar, 0);
							
					pResult->StatusCode = OpcUa_Good;
					}
				}

			if( pNode->GetClass() == OpcUa_NodeClass_ReferenceType ) {

				COpcReferenceTypeNode *pType = (COpcReferenceTypeNode *) pNode;

				if( pRead->AttributeId >= 8 && pRead->AttributeId <= 10 ) {

					if( pRead->AttributeId == OpcUa_Attributes_IsAbstract ) {

						pResult->Value.Value.Boolean = pType->IsAbstract();

						FillArrayType(pResult, OpcUaId_Boolean, OpcUa_VariantArrayType_Scalar, 0);
								
						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_Symmetric ) {

						pResult->Value.Value.Boolean = pType->IsSymmetric();

						FillArrayType(pResult, OpcUaId_Boolean, OpcUa_VariantArrayType_Scalar, 0);
								
						pResult->StatusCode = OpcUa_Good;
						}

					if( pRead->AttributeId == OpcUa_Attributes_InverseName ) {

						pResult->Value.Value.LocalizedText = OpcAlloc(1, OpcUa_LocalizedText);

						OpcUa_LocalizedText_Initialize(pResult->Value.Value.LocalizedText);

						OpcUa_String_AttachCopy(&pResult->Value.Value.LocalizedText->Text,   PTXT(PCTXT(pType->GetInverseName())));
									
						OpcUa_String_AttachCopy(&pResult->Value.Value.LocalizedText->Locale, PTXT(PCTXT(pType->GetInverseLocale())));
									
						FillArrayType(pResult, OpcUaId_LocalizedText, OpcUa_VariantArrayType_Scalar, 0);
									
						pResult->StatusCode = OpcUa_Good;
						}
					}
				}
			}

		m_pModel->Free();

		return;
		}

	pResult->StatusCode = OpcUa_BadNodeIdUnknown;

	m_pModel->Free();
	}

void COpcServer::AssignTimestamp(OpcUa_DataValue *pResult, UINT uTime, COpcNodeId const &NodeId)
{
	if( uTime != OpcUa_TimestampsToReturn_Neither ) {

		OpcUa_DateTime dt = OpcUa_DateTime_UtcNow();

		if( uTime == OpcUa_TimestampsToReturn_Both || uTime == OpcUa_TimestampsToReturn_Source ) {

			timeval t = { 0 };

			if( NodeId.IsType(OpcUa_IdType_Numeric) ) {

				UINT ns = NodeId.GetNamespace();

				UINT id = NodeId.GetNumericValue();

				if( m_pHost->GetSourceTimeStamp(t, ns, id) ) {

					TimeFromTimeval(pResult->SourceTimestamp, t);
					}
				else
					pResult->SourceTimestamp = dt;
				}
			else
				pResult->SourceTimestamp = dt;
			}

		if( uTime == OpcUa_TimestampsToReturn_Both || uTime == OpcUa_TimestampsToReturn_Server ) {

			pResult->ServerTimestamp = dt;
			}
		}
	}

void COpcServer::FillArrayType(OpcUa_DataValue *pResult, UINT uType, UINT uArray, UINT uSize)
{
	FillArrayType(&pResult->Value, uType, uArray, uSize);
	}

void COpcServer::FillArrayType(OpcUa_Variant *pValue, UINT uType, UINT uArray, UINT uSize)
{
	pValue->ArrayType = BYTE(uArray);
	
	pValue->Datatype  = BYTE(uType);

	if( uArray == OpcUa_VariantArrayType_Array ) {

		pValue->Value.Array.Length = uSize;
		}
	}

UINT COpcServer::FetchValue(CSession &Session, COpcVariableTypeNode *pNode, OpcUa_String *pIndex, OpcUa_DataValue *pResult)
{
	return OpcUa_BadNotReadable;
	}

UINT COpcServer::FetchValue(CSession &Session, COpcVariableNode *pNode, OpcUa_String *pIndex, OpcUa_DataValue *pResult)
{
	if( pNode->GetId().IsType(OpcUa_IdType_Numeric) ) {

		UINT ns = pNode->GetId().GetNamespace();
	
		UINT id = pNode->GetId().GetNumericValue();

		if( SkipValue(Session.m_uIndex, ns, id) ) {

			return OpcUa_BadNotReadable;
			}

		if( pNode->GetValueRank() == OpcUa_ValueRanks_Scalar ) {

			UINT uType = GetWireType(pNode->GetDataType().GetNumericValue());

			GetValue(&pResult->Value.Value, pNode, Session.m_uIndex, ns, id, uType);

			FillArrayType(pResult, uType, OpcUa_VariantArrayType_Scalar, 0);
			}
		else {
			UINT uType = GetWireType(pNode->GetDataType().GetNumericValue());

			UINT uSize = pNode->GetArrayDimension(1);

			AfxAssert(uSize);

			switch( uType ) {

				AllocArray(Double);
				AllocArray(Float);
				AllocArray(Byte);
				AllocArray(SByte);
				AllocArray(Int16);
				AllocArray(UInt16);
				AllocArray(Int32);
				AllocArray(UInt32);
				AllocArray(Int64);
				AllocArray(UInt64);
				AllocArray(Boolean);
				AllocArray(DateTime);
				AllocArray(String);

				default:

					AfxAssert(FALSE);
				}

			GetArray(&pResult->Value.Value.Array.Value, pNode, Session.m_uIndex, ns, id, uType, uSize);
				
			FillArrayType(pResult, uType, OpcUa_VariantArrayType_Array, uSize);
			}

		return OpcUa_Good;
		}

	return OpcUa_BadNotReadable;
	}

bool COpcServer::FetchDesc(CString &Desc, COpcNodeId const &NodeId)
{
	if( NodeId.IsType(OpcUa_IdType_Numeric) ) {

		UINT ns = NodeId.GetNamespace();
	
		UINT id = NodeId.GetNumericValue();

		return m_pHost->GetDesc(Desc, ns, id);
		}

	return false;
	}

void COpcServer::GetValue(OpcUa_VariantUnion *pValue, COpcVariableNode *pNode, UINT si, UINT ns, UINT id, UINT uType)
{
	switch( uType ) {

		AssignScalar(Double,  GetValueDouble);
		AssignScalar(Float,   GetValueDouble);
		AssignScalar(Byte,    GetValueInt   );
		AssignScalar(SByte,   GetValueInt   );
		AssignScalar(Int16,   GetValueInt   );
		AssignScalar(UInt16,  GetValueInt   );
		AssignScalar(Int32,   GetValueInt   );
		AssignScalar(UInt32,  GetValueInt   );
		AssignScalar(Int64,   GetValueInt64 );
		AssignScalar(UInt64,  GetValueInt64 );
		AssignScalar(Boolean, GetValueInt   );

		case OpcUaId_DateTime:

			GetValueTime(pValue->DateTime, si, ns, id, 0);

			break;

		case OpcUaId_String:

			OpcUa_String_Initialize(&pValue->String);

			OpcUa_String_AttachCopy(&pValue->String, PTXT(PCTXT(GetValueString(si, ns, id, 0))));

			break;

		default:

			AfxAssert(FALSE);
		}
	}

void COpcServer::GetArray(OpcUa_VariantArrayUnion *pValue, COpcVariableNode *pNode, UINT si, UINT ns, UINT id, UINT uType, UINT uSize)
{
	UINT n;

	switch( uType ) {
		
		AssignArray(Double,  GetValueDouble);
		AssignArray(Float,   GetValueDouble);
		AssignArray(Byte,    GetValueInt   );
		AssignArray(SByte,   GetValueInt   );
		AssignArray(Int16,   GetValueInt   );
		AssignArray(UInt16,  GetValueInt   );
		AssignArray(Int32,   GetValueInt   );
		AssignArray(UInt32,  GetValueInt   );
		AssignArray(Int64,   GetValueInt64 );
		AssignArray(UInt64,  GetValueInt64 );
		AssignArray(Boolean, GetValueInt   );

		case OpcUaId_DateTime:

			for( n = 0; n < uSize; n++ ) {

				GetValueTime(pValue->DateTimeArray[n], si, ns, id, 0);
				}

			break;

		case OpcUaId_String:

			for( n = 0; n < uSize; n++ ) {

				OpcUa_String_Initialize(&pValue->StringArray[n]);

				OpcUa_String_AttachCopy(&pValue->StringArray[n], PTXT(PCTXT(GetValueString(si, ns, id, n))));
				}

			break;

		default:

			AfxAssert(FALSE);
		}
	}

// End of File
