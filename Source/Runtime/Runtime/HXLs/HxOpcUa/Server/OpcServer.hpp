
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcServer_HPP

#define INCLUDE_OpcServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../OpcBase.hpp"

#include "../Model/OpcNanoModel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <opcua_string.h>
#include <opcua_datetime.h>
#include <opcua_p_types.h>
#include <opcua_thread.h>
#include <opcua_string.h>
#include <opcua_memory.h>
#include <opcua_core.h>
#include <opcua_serverstub.h>

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COpcNode;
class COpcNodeId;
class COpcNanoModel;
class COpcReference;
class COpcVariableNode;
class COpcVariableTypeNode;

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

class COpcServer : public COpcBase, public IOpcUaServer
{
public:
	// Constructor
	COpcServer(void);

	// Destructor
	~COpcServer(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IOpcUaServer
	void OpcFree(void);
	void OpcLoad(COpcUaServerConfig const &Config);
	bool OpcInit(IOpcUaServerHost *pHost, CString Endpoint);
	bool OpcExec(void);
	bool OpcStop(void);
	bool OpcTerm(void);

protected:
	// Subscription States
	enum
	{
		subNormal,
		subLate,
		subKeepAlive
	};

	// Monitored Item
	struct CMonitoredItem
	{
		UINT			   m_uIndex;
		OpcUa_MonitoringMode       m_Mode;
		OpcUa_MonitoringParameters m_Parameters;
		OpcUa_TimestampsToReturn   m_eTimestampsToReturn;
		OpcUa_ReadValueId	   m_ReadValueId;
		UINT			   m_DataType;
		DWORD			   m_History[2];
		UINT			   m_Queued;
		UINT			   m_SendTime;
		UINT			   m_LastTime;
	};

	// Notification Data
	struct CNotification
	{
		UINT			    m_uSub;
		UINT			    m_nPriority;
		CArray <CMonitoredItem *>   m_List;
		OpcUa_NotificationMessage * m_pMsg;
	};

	// Subscription Data
	struct CSubscription
	{
		UINT			 m_uIndex;
		UINT			 m_uState;
		OpcUa_Double		 m_nPublishingInterval;
		OpcUa_UInt32		 m_nLifetimeCount;
		OpcUa_UInt32		 m_nMaxKeepAliveCount;
		OpcUa_UInt32		 m_nMaxNotificationsPerPublish;
		bool			 m_fPublishingEnabled;
		UINT			 m_nPriority;
		double			 m_Timer;
		UINT			 m_uKeepAlive;
		UINT			 m_uItemId;
		UINT			 m_uSeqNum;
		CMap  <UINT, INDEX>      m_Index;
		CList <CMonitoredItem *> m_Items;
	};

	// Session Data
	struct CSession
	{
		COpcServer	      * m_pServer;
		UINT			m_uIndex;
		UINT			m_uToken;
		IEvent		      * m_pEvent;
		bool			m_fInUse;
		bool			m_fActive;
		bool			m_fAuthed;
		OpcUa_UInt32		m_ChannelId;
		OpcUa_Double		m_Timeout;
		OpcUa_UInt32		m_IdleTime;
		OpcUa_Timer		m_hTimer;
		INDEX			m_NextSub;
		CMap  <UINT, INDEX>     m_Index;
		CList <CSubscription *> m_Subs;
		CList <CNotification *> m_Avail;
		CList <CNotification *> m_Saved;
		CByteArray		m_Nonce;
	};

	// History Data
	struct CHistoryData
	{
		DWORD		m_Secs;
		OpcUa_DateTime  m_Time;
		int             m_nCount;
		OpcUa_Variant * m_pData;
	};

	// History File
	struct CHistoryFile
	{
		CString	m_DataName;
		CString m_IndxName;
		DWORD   m_TimeBegin;
		DWORD   m_TimeEnd;
		UINT    m_uSize;

		// Sorting Support
		int operator > (CHistoryFile const &t) const
		{
			return m_TimeBegin > t.m_TimeBegin;
		}
	};

	// Data Members
	ULONG						m_uRefs;
	UINT						m_uDebug;
	IOpcUaServerHost			      * m_pHost;
	CStringArray					m_Endpoint;
	OpcUa_DateTime					m_StartTime;
	UINT						m_nMaxRefs;
	IMutex					      * m_pMutex;
	COpcNanoModel				      * m_pModel;
	CByteArray					m_Cert;
	CByteArray					m_Priv;
	OpcUa_Endpoint					m_hEndpoint;
	CArray <OpcUa_ServiceType *>			m_Services;
	CSession					m_Session[10];
	UINT						m_uSessId;
	UINT						m_uSubId;
	OpcUa_ByteString				m_OpcCert;
	OpcUa_Key					m_OpcPriv;
	OpcUa_P_OpenSSL_CertificateStore_Config		m_OpcPki;
	OpcUa_UInt32					m_nOpcSec;
	OpcUa_Endpoint_SecurityPolicyConfiguration    *	m_pOpcSec;

	// Filing Data
	bool                  m_fHistUsed;
	bool		      m_fHistRead;
	HTHREAD		      m_hHistThread;
	IMutex              * m_pHistDataMutex;
	IMutex              * m_pHistWipeMutex;
	int		      m_nHistCount;
	UINT		      m_uHistReadLimit;
	UINT		      m_uHistSample;
	CString		      m_HistFilePath;
	DWORD		      m_HistDataBase;
	CString		      m_HistDataName;
	CString		      m_HistIndxName;
	CAutoFile             m_HistDataFile;
	CAutoFile	      m_HistIndxFile;
	UINT		      m_uHistQuota;
	UINT		      m_uHistTime;
	UINT64		      m_uHistTotal;
	UINT64		      m_uHistDiskSize;
	UINT64		      m_uHistTotalLimit;
	CArray <CHistoryFile> m_HistFileList;
	CList  <CHistoryData> m_HistBuffer;
	BYTE		      m_bHistGuid[16];

	// Implementation
	void InitSecurity(void);
	void InitModel(void);
	void AddServices(void);
	void AddService(UINT uType, OpcUa_EncodeableType *pResp, OpcUa_PfnBeginInvokeService *pBegin, OpcUa_PfnInvokeService *pInvoke);
	void FillResponse(OpcUa_ResponseHeader *pRep, const OpcUa_RequestHeader *pReq, OpcUa_StatusCode Status);
	bool GetDateTimeDiff(OpcUa_DateTime Value1, OpcUa_DateTime Value2, OpcUa_UInt32 *puResult);
	void SetSecurityNone(OpcUa_Endpoint_SecurityPolicyConfiguration *pSec);
	void CopyBytes(OpcUa_ByteString *pBytes, CByteArray const &Source);

	// Crypto Support
	bool DecryptSecret(CByteArray &Secret, CString const &Method, CByteArray const &Data);

	// Endpoint Helpers
	void FillServerData(OpcUa_ApplicationDescription *pDesc);
	void GetEndpoints(OpcUa_Int32 *pNoOfEndpoints, OpcUa_EndpointDescription **ppEndpoints);

	// Session Helpers
	UINT MakeSessionToken(UINT n);
	UINT FindSession(OpcUa_Endpoint hEndpoint, OpcUa_Handle hContext, OpcUa_RequestHeader const *pRequestHeader, OpcUa_ResponseHeader *pResponseHeader, bool fActive);
	void CreateSessionNonce(CSession &Session);
	void TickleSession(CSession &Session, UINT msTime);
	bool CheckUserIdentity(CSession &Session, OpcUa_ExtensionObject const *pUser);

	// Browse Helpers
	UINT BrowseNode(CSession &Session, OpcUa_BrowseResult *pResult, OpcUa_BrowseDescription *pDesc, COpcNode *pNode, UINT uStart, UINT uLimit);
	bool IsChildNode(COpcNodeId const &DescType, COpcNodeId const &RefType, bool fIncSub);
	bool CheckMask(DWORD dwMask, DWORD dwAttr);
	bool CheckDirection(COpcReference const &Ref, OpcUa_BrowseDirection BrowseDir);

	// Read Helpers
	void ReadNode(CSession &Session, OpcUa_DataValue *pResult, OpcUa_ReadValueId const *pRead, OpcUa_TimestampsToReturn eTimestampsToReturn, UINT uIndex);
	void AssignTimestamp(OpcUa_DataValue *pResult, UINT uTime, COpcNodeId const &NodeId);
	void FillArrayType(OpcUa_DataValue *pResult, UINT uType, UINT uArray, UINT uSize);
	void FillArrayType(OpcUa_Variant *pValue, UINT uType, UINT uArray, UINT uSize);
	UINT FetchValue(CSession &Session, COpcVariableNode *pNode, OpcUa_String *pIndex, OpcUa_DataValue *pResult);
	UINT FetchValue(CSession &Session, COpcVariableTypeNode *pNode, OpcUa_String *pIndex, OpcUa_DataValue *pResult);
	bool FetchDesc(CString &Desc, COpcNodeId const &NodeId);
	void GetValue(OpcUa_VariantUnion *pValue, COpcVariableNode *pNode, UINT si, UINT ns, UINT id, UINT uType);
	void GetArray(OpcUa_VariantArrayUnion *pValue, COpcVariableNode *pNode, UINT si, UINT ns, UINT id, UINT uType, UINT uSize);

	// Write Helpers
	void WriteNode(CSession &Session, OpcUa_StatusCode *pResult, OpcUa_WriteValue const *pWrite, UINT uIndex);
	UINT WriteValue(CSession &Session, COpcVariableNode *pNode, OpcUa_Variant const *pValue);
	void SetValue(COpcVariableNode *pNode, UINT si, UINT ns, UINT id, UINT uType, OpcUa_VariantUnion const *pValue);

	// Subscription Helpers
	void SetSubsForNewSession(CSession &Session);
	void DeleteSubsForSession(CSession &Session);
	void DeleteSub(CSession &Session, CSubscription *pSub);
	void InsertNotification(CList <CNotification *> &List, CNotification *pNot);
	void TransferNotificationsForSub(CList <CNotification *> &Dest, CList <CNotification *> &List, CSubscription *pSub, OpcUa_Int32 *pnAvail, OpcUa_UInt32 **ppAvail);
	void RemoveNotificationsForSub(CList <CNotification *> &List, CSubscription *pSub);
	void RemoveNotificationsForItem(CList <CNotification *> &List, CMonitoredItem *pItem, bool fAvail);
	void DeleteNotificationList(CList <CNotification *> &List, bool fAvail);
	void DeleteNotification(CNotification *pNot);
	void TickleSubs(CSession &Session, UINT msTime);
	void QueueNotificationBurst(CSession &Session, CSubscription *pSub, CArray <CMonitoredItem *> const &List, PCUINT pCodes);
	void QueueNotificationMessage(CSession &Session, CSubscription *pSub, CArray <CMonitoredItem *> const &List, PCUINT pCodes);
	void GetSavedNotifications(CSession &Session, UINT uSub, OpcUa_Int32 *pnAvail, OpcUa_UInt32 **ppAvail);
	void ProcessAcks(CSession &Session, OpcUa_Int32 *pnResult, OpcUa_StatusCode **ppResult, OpcUa_Int32 nAck, OpcUa_SubscriptionAcknowledgement const *pAck);
	void CoalesceNotifications(CSession &Session, CList <CNotification *> &List);
	int  FindMaxNotificationsForSub(CSession &Session, UINT uSub);

	// History Helpers
	void HistoryInit(void);
	void HistoryPoll(void);
	void HistoryExec(void);
	void HistoryStop(void);
	void HistoryTerm(void);
	void HistoryLockData(void);
	void HistoryFreeData(void);
	void HistoryLockWipe(void);
	void HistoryFreeWipe(void);
	void FindHistoryData(CArray <CHistoryData> &Data, bool &fCont, OpcUa_ReadRawModifiedDetails const *pDetails, UINT uTimeout);
	void FindHistoryDataDisk(CArray <CHistoryData> &Data, bool &fCont, DWORD rb, DWORD re, UINT np);
	void FindHistoryDataList(CArray <CHistoryData> &Data, bool &fCont, DWORD rb, DWORD re, UINT np);
	void FreeHistoryData(CArray <CHistoryData> const &Data);
	void FreeHistoryData(CHistoryData const &Row);
	bool SaveHistoryData(DWORD t, OpcUa_Variant *pValues, int nCount);
	bool OpenSaveFiles(DWORD t);
	bool CloseSaveFiles(void);
	bool LoadHistoryData(CAutoFile &File, OpcUa_Variant *pValues, int nCount);
	void EncodeHistoryData(CByteArray &Data, OpcUa_Variant *pValues, int nCount);
	void EncodeHistoryData(CByteArray &Data, OpcUa_Variant *pValue);
	void DecodeHistoryData(PCBYTE &pData, OpcUa_Variant *pValue);
	bool ScanExistingFiles(void);
	bool OpenFile(CAutoFile &File, CString const &Name, BOOL fWrite);
	void CheckTotalSize(void);
	void Print64(PCTXT pName, UINT64 n);

	// Time Formatting
	CString FormatTime(DWORD t);
	CString FormatTime(OpcUa_DateTime const &dt);

	// History Thread
	static int HistoryProc(HTHREAD hThread, void *pParam, UINT uParam);

	// Value Access
	bool    SkipValue(UINT si, UINT ns, UINT id);
	void    SetValueDouble(UINT si, UINT ns, UINT id, UINT n, double d);
	void    SetValueInt(UINT si, UINT ns, UINT id, UINT n, int d);
	void    SetValueInt64(UINT si, UINT ns, UINT id, UINT n, INT64 d);
	void    SetValueString(UINT si, UINT ns, UINT id, UINT n, PCTXT d);
	double  GetValueDouble(UINT si, UINT ns, UINT id, UINT n);
	UINT    GetValueInt(UINT si, UINT ns, UINT id, UINT n);
	UINT64  GetValueInt64(UINT si, UINT ns, UINT id, UINT n);
	CString GetValueString(UINT si, UINT ns, UINT id, UINT n);
	void    GetValueTime(OpcUa_DateTime &t, UINT si, UINT ns, UINT id, UINT n);
	bool    HasEvents(CArray <UINT> &List, UINT ns, UINT id);
	bool    InitHistory(UINT si, UINT ns, UINT id, UINT uType, DWORD *pHistory);
	bool    HasChanged(UINT si, UINT ns, UINT id, UINT uType, DWORD *pHistory);
	void    KillHistory(UINT si, UINT ns, UINT id, UINT uType, DWORD *pHistory);
	UINT	GetWireType(UINT uType);

	// Services
	OpcUa_StatusCode GetEndpoints(OpcUa_Endpoint               hEndpoint,
				      OpcUa_Handle                 hContext,
				      OpcUa_RequestHeader *        pRequestHeader,
				      OpcUa_String *               pEndpointUrl,
				      OpcUa_Int32                  nNoOfLocaleIds,
				      OpcUa_String *               pLocaleIds,
				      OpcUa_Int32                  nNoOfProfileUris,
				      OpcUa_String *               pProfileUris,
				      OpcUa_ResponseHeader *       pResponseHeader,
				      OpcUa_Int32 *                pNoOfEndpoints,
				      OpcUa_EndpointDescription ** ppEndpoints
	);

	OpcUa_StatusCode CreateSession(OpcUa_Endpoint                       hEndpoint,
				       OpcUa_Handle                         hContext,
				       const OpcUa_RequestHeader *          pRequestHeader,
				       const OpcUa_ApplicationDescription * pClientDescription,
				       const OpcUa_String *                 pServerUri,
				       const OpcUa_String *                 pEndpointUrl,
				       const OpcUa_String *                 pSessionName,
				       const OpcUa_ByteString *             pClientNonce,
				       const OpcUa_ByteString *             pClientCertificate,
				       OpcUa_Double                         nRequestedSessionTimeout,
				       OpcUa_UInt32                         nMaxResponseMessageSize,
				       OpcUa_ResponseHeader *               pResponseHeader,
				       OpcUa_NodeId *                       pSessionId,
				       OpcUa_NodeId *                       pAuthenticationToken,
				       OpcUa_Double *                       pRevisedSessionTimeout,
				       OpcUa_ByteString *                   pServerNonce,
				       OpcUa_ByteString *                   pServerCertificate,
				       OpcUa_Int32 *                        pNoOfServerEndpoints,
				       OpcUa_EndpointDescription **         pServerEndpoints,
				       OpcUa_Int32 *                        pNoOfServerSoftwareCertificates,
				       OpcUa_SignedSoftwareCertificate **   pServerSoftwareCertificates,
				       OpcUa_SignatureData *                pServerSignature,
				       OpcUa_UInt32 *                       pMaxRequestMessageSize
	);

	OpcUa_StatusCode ActivateSession(OpcUa_Endpoint                          hEndpoint,
					 OpcUa_Handle                            hContext,
					 const OpcUa_RequestHeader *             pRequestHeader,
					 const OpcUa_SignatureData *             pClientSignature,
					 OpcUa_Int32                             nNoOfClientSoftwareCertificates,
					 const OpcUa_SignedSoftwareCertificate * pClientSoftwareCertificates,
					 OpcUa_Int32                             nNoOfLocaleIds,
					 const OpcUa_String *                    pLocaleIds,
					 const OpcUa_ExtensionObject *           pUserIdentityToken,
					 const OpcUa_SignatureData *             pUserTokenSignature,
					 OpcUa_ResponseHeader *                  pResponseHeader,
					 OpcUa_ByteString *                      pServerNonce,
					 OpcUa_Int32 *                           pNoOfResults,
					 OpcUa_StatusCode **                     pResults,
					 OpcUa_Int32 *                           pNoOfDiagnosticInfos,
					 OpcUa_DiagnosticInfo **                 pDiagnosticInfos
	);

	OpcUa_StatusCode CloseSession(OpcUa_Endpoint              hEndpoint,
				      OpcUa_Handle                hContext,
				      const OpcUa_RequestHeader * pRequestHeader,
				      OpcUa_Boolean               bDeleteSubscriptions,
				      OpcUa_ResponseHeader *      pResponseHeader
	);

	OpcUa_StatusCode Browse(OpcUa_Endpoint                 hEndpoint,
				OpcUa_Handle                   hContext,
				const OpcUa_RequestHeader *    pRequestHeader,
				const OpcUa_ViewDescription *  pView,
				OpcUa_UInt32                   nRequestedMaxReferencesPerNode,
				OpcUa_Int32                    nNoOfNodesToBrowse,
				OpcUa_BrowseDescription *	pNodesToBrowse,
				OpcUa_ResponseHeader *         pResponseHeader,
				OpcUa_Int32 *                  pNoOfResults,
				OpcUa_BrowseResult **          pResults,
				OpcUa_Int32 *                  pNoOfDiagnosticInfos,
				OpcUa_DiagnosticInfo **        pDiagnosticInfos
	);

	OpcUa_StatusCode BrowseNext(OpcUa_Endpoint              hEndpoint,
				    OpcUa_Handle                hContext,
				    const OpcUa_RequestHeader * pRequestHeader,
				    OpcUa_Boolean               bReleaseContinuationPoints,
				    OpcUa_Int32                 nNoOfContinuationPoints,
				    const OpcUa_ByteString *    pContinuationPoints,
				    OpcUa_ResponseHeader *      pResponseHeader,
				    OpcUa_Int32 *               pNoOfResults,
				    OpcUa_BrowseResult **       pResults,
				    OpcUa_Int32 *               pNoOfDiagnosticInfos,
				    OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode TranslateBrowsePathsToNodeIds(OpcUa_Endpoint              hEndpoint,
						       OpcUa_Handle                hContext,
						       const OpcUa_RequestHeader * pRequestHeader,
						       OpcUa_Int32                 nNoOfBrowsePaths,
						       const OpcUa_BrowsePath *    pBrowsePaths,
						       OpcUa_ResponseHeader *      pResponseHeader,
						       OpcUa_Int32 *               pNoOfResults,
						       OpcUa_BrowsePathResult **   pResults,
						       OpcUa_Int32 *               pNoOfDiagnosticInfos,
						       OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode Read(OpcUa_Endpoint              hEndpoint,
			      OpcUa_Handle                hContext,
			      const OpcUa_RequestHeader * pRequestHeader,
			      OpcUa_Double                nMaxAge,
			      OpcUa_TimestampsToReturn    eTimestampsToReturn,
			      OpcUa_Int32                 nNoOfNodesToRead,
			      const OpcUa_ReadValueId *   pNodesToRead,
			      OpcUa_ResponseHeader *      pResponseHeader,
			      OpcUa_Int32 *               pNoOfResults,
			      OpcUa_DataValue **          pResults,
			      OpcUa_Int32 *               pNoOfDiagnosticInfos,
			      OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode Write(OpcUa_Endpoint              hEndpoint,
			       OpcUa_Handle                hContext,
			       const OpcUa_RequestHeader * pRequestHeader,
			       OpcUa_Int32                 nNoOfNodesToWrite,
			       const OpcUa_WriteValue *    pNodesToWrite,
			       OpcUa_ResponseHeader *      pResponseHeader,
			       OpcUa_Int32 *               pNoOfResults,
			       OpcUa_StatusCode **         pResults,
			       OpcUa_Int32 *               pNoOfDiagnosticInfos,
			       OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode FindServers(OpcUa_Endpoint                  hEndpoint,
				     OpcUa_Handle                    hContext,
				     const OpcUa_RequestHeader *     pRequestHeader,
				     const OpcUa_String *            pEndpointUrl,
				     OpcUa_Int32                     nNoOfLocaleIds,
				     const OpcUa_String *            pLocaleIds,
				     OpcUa_Int32                     nNoOfServerUris,
				     const OpcUa_String *            pServerUris,
				     OpcUa_ResponseHeader *          pResponseHeader,
				     OpcUa_Int32 *                   pNoOfServers,
				     OpcUa_ApplicationDescription ** pServers
	);

	OpcUa_StatusCode CreateSubscription(OpcUa_Endpoint              hEndpoint,
					    OpcUa_Handle                hContext,
					    const OpcUa_RequestHeader * pRequestHeader,
					    OpcUa_Double                nRequestedPublishingInterval,
					    OpcUa_UInt32                nRequestedLifetimeCount,
					    OpcUa_UInt32                nRequestedMaxKeepAliveCount,
					    OpcUa_UInt32                nMaxNotificationsPerPublish,
					    OpcUa_Boolean               bPublishingEnabled,
					    OpcUa_Byte                  nPriority,
					    OpcUa_ResponseHeader *      pResponseHeader,
					    OpcUa_UInt32 *              pSubscriptionId,
					    OpcUa_Double *              pRevisedPublishingInterval,
					    OpcUa_UInt32 *              pRevisedLifetimeCount,
					    OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
	);

	OpcUa_StatusCode ModifySubscription(OpcUa_Endpoint              hEndpoint,
					    OpcUa_Handle                hContext,
					    const OpcUa_RequestHeader * pRequestHeader,
					    OpcUa_UInt32                nSubscriptionId,
					    OpcUa_Double                nRequestedPublishingInterval,
					    OpcUa_UInt32                nRequestedLifetimeCount,
					    OpcUa_UInt32                nRequestedMaxKeepAliveCount,
					    OpcUa_UInt32                nMaxNotificationsPerPublish,
					    OpcUa_Byte                  nPriority,
					    OpcUa_ResponseHeader *      pResponseHeader,
					    OpcUa_Double *              pRevisedPublishingInterval,
					    OpcUa_UInt32 *              pRevisedLifetimeCount,
					    OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
	);

	OpcUa_StatusCode TransferSubscriptions(OpcUa_Endpoint              hEndpoint,
					       OpcUa_Handle                hContext,
					       const OpcUa_RequestHeader * pRequestHeader,
					       OpcUa_Int32                 nNoOfSubscriptionIds,
					       const OpcUa_UInt32 *        pSubscriptionIds,
					       OpcUa_Boolean               bSendInitialValues,
					       OpcUa_ResponseHeader *      pResponseHeader,
					       OpcUa_Int32 *               pNoOfResults,
					       OpcUa_TransferResult **     pResults,
					       OpcUa_Int32 *               pNoOfDiagnosticInfos,
					       OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode SetPublishingMode(OpcUa_Endpoint              hEndpoint,
					   OpcUa_Handle                hContext,
					   const OpcUa_RequestHeader * pRequestHeader,
					   OpcUa_Boolean               bPublishingEnabled,
					   OpcUa_Int32                 nNoOfSubscriptionIds,
					   const OpcUa_UInt32 *        pSubscriptionIds,
					   OpcUa_ResponseHeader *      pResponseHeader,
					   OpcUa_Int32 *               pNoOfResults,
					   OpcUa_StatusCode **         pResults,
					   OpcUa_Int32 *               pNoOfDiagnosticInfos,
					   OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode DeleteSubscriptions(OpcUa_Endpoint              hEndpoint,
					     OpcUa_Handle                hContext,
					     const OpcUa_RequestHeader * pRequestHeader,
					     OpcUa_Int32                 nNoOfSubscriptionIds,
					     const OpcUa_UInt32 *        pSubscriptionIds,
					     OpcUa_ResponseHeader *      pResponseHeader,
					     OpcUa_Int32 *               pNoOfResults,
					     OpcUa_StatusCode **         pResults,
					     OpcUa_Int32 *               pNoOfDiagnosticInfos,
					     OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode CreateMonitoredItems(OpcUa_Endpoint                           hEndpoint,
					      OpcUa_Handle                             hContext,
					      const OpcUa_RequestHeader *              pRequestHeader,
					      OpcUa_UInt32                             nSubscriptionId,
					      OpcUa_TimestampsToReturn                 eTimestampsToReturn,
					      OpcUa_Int32                              nNoOfItemsToCreate,
					      const OpcUa_MonitoredItemCreateRequest * pItemsToCreate,
					      OpcUa_ResponseHeader *                   pResponseHeader,
					      OpcUa_Int32 *                            pNoOfResults,
					      OpcUa_MonitoredItemCreateResult **       pResults,
					      OpcUa_Int32 *                            pNoOfDiagnosticInfos,
					      OpcUa_DiagnosticInfo **                  pDiagnosticInfos
	);

	OpcUa_StatusCode ModifyMonitoredItems(OpcUa_Endpoint                           hEndpoint,
					      OpcUa_Handle                             hContext,
					      const OpcUa_RequestHeader *              pRequestHeader,
					      OpcUa_UInt32                             nSubscriptionId,
					      OpcUa_TimestampsToReturn                 eTimestampsToReturn,
					      OpcUa_Int32                              nNoOfItemsToModify,
					      const OpcUa_MonitoredItemModifyRequest * pItemsToModify,
					      OpcUa_ResponseHeader *                   pResponseHeader,
					      OpcUa_Int32 *                            pNoOfResults,
					      OpcUa_MonitoredItemModifyResult **       pResults,
					      OpcUa_Int32 *                            pNoOfDiagnosticInfos,
					      OpcUa_DiagnosticInfo **                  pDiagnosticInfos
	);

	OpcUa_StatusCode SetMonitoringMode(OpcUa_Endpoint              hEndpoint,
					   OpcUa_Handle                hContext,
					   const OpcUa_RequestHeader * pRequestHeader,
					   OpcUa_UInt32                nSubscriptionId,
					   OpcUa_MonitoringMode        eMonitoringMode,
					   OpcUa_Int32                 nNoOfMonitoredItemIds,
					   const OpcUa_UInt32 *        pMonitoredItemIds,
					   OpcUa_ResponseHeader *      pResponseHeader,
					   OpcUa_Int32 *               pNoOfResults,
					   OpcUa_StatusCode **         pResults,
					   OpcUa_Int32 *               pNoOfDiagnosticInfos,
					   OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode DeleteMonitoredItems(OpcUa_Endpoint              hEndpoint,
					      OpcUa_Handle                hContext,
					      const OpcUa_RequestHeader * pRequestHeader,
					      OpcUa_UInt32                nSubscriptionId,
					      OpcUa_Int32                 nNoOfMonitoredItemIds,
					      const OpcUa_UInt32 *        pMonitoredItemIds,
					      OpcUa_ResponseHeader *      pResponseHeader,
					      OpcUa_Int32 *               pNoOfResults,
					      OpcUa_StatusCode **         pResults,
					      OpcUa_Int32 *               pNoOfDiagnosticInfos,
					      OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	OpcUa_StatusCode Publish(OpcUa_Endpoint                            hEndpoint,
				 OpcUa_Handle                              hContext,
				 const OpcUa_RequestHeader *               pRequestHeader,
				 OpcUa_Int32                               nNoOfSubscriptionAcknowledgements,
				 const OpcUa_SubscriptionAcknowledgement * pSubscriptionAcknowledgements,
				 OpcUa_ResponseHeader *                    pResponseHeader,
				 OpcUa_UInt32 *                            pSubscriptionId,
				 OpcUa_Int32 *                             pNoOfAvailableSequenceNumbers,
				 OpcUa_UInt32 **                           pAvailableSequenceNumbers,
				 OpcUa_Boolean *                           pMoreNotifications,
				 OpcUa_NotificationMessage *               pNotificationMessage,
				 OpcUa_Int32 *                             pNoOfResults,
				 OpcUa_StatusCode **                       pResults,
				 OpcUa_Int32 *                             pNoOfDiagnosticInfos,
				 OpcUa_DiagnosticInfo **                   pDiagnosticInfos
	);

	OpcUa_StatusCode Republish(OpcUa_Endpoint              hEndpoint,
				   OpcUa_Handle                hContext,
				   const OpcUa_RequestHeader * pRequestHeader,
				   OpcUa_UInt32                nSubscriptionId,
				   OpcUa_UInt32                nRetransmitSequenceNumber,
				   OpcUa_ResponseHeader *      pResponseHeader,
				   OpcUa_NotificationMessage * pNotificationMessage
	);

	OpcUa_StatusCode HistoryRead(OpcUa_Endpoint                   hEndpoint,
				     OpcUa_Handle                     hContext,
				     const OpcUa_RequestHeader *      pRequestHeader,
				     const OpcUa_ExtensionObject *    pHistoryReadDetails,
				     OpcUa_TimestampsToReturn         eTimestampsToReturn,
				     OpcUa_Boolean                    bReleaseContinuationPoints,
				     OpcUa_Int32                      nNoOfNodesToRead,
				     const OpcUa_HistoryReadValueId * pNodesToRead,
				     OpcUa_ResponseHeader *           pResponseHeader,
				     OpcUa_Int32 *                    pNoOfResults,
				     OpcUa_HistoryReadResult **       pResults,
				     OpcUa_Int32 *                    pNoOfDiagnosticInfos,
				     OpcUa_DiagnosticInfo **          pDiagnosticInfos
	);

	OpcUa_StatusCode Call(OpcUa_Endpoint                  hEndpoint,
			      OpcUa_Handle                    hContext,
			      const OpcUa_RequestHeader*      pRequestHeader,
			      OpcUa_Int32                     nNoOfMethodsToCall,
			      const OpcUa_CallMethodRequest * pMethodsToCall,
			      OpcUa_ResponseHeader*           pResponseHeader,
			      OpcUa_Int32*                    pNoOfResults,
			      OpcUa_CallMethodResult**        pResults,
			      OpcUa_Int32*                    pNoOfDiagnosticInfos,
			      OpcUa_DiagnosticInfo**          pDiagnosticInfos
	);

	// Callbacks
	void EndpointCallback(OpcUa_Endpoint          hEndpoint,
			      OpcUa_Endpoint_Event    eEvent,
			      OpcUa_StatusCode        uStatus,
			      OpcUa_UInt32            uSecureChannelId,
			      OpcUa_ByteString *      pbsClientCertificate,
			      OpcUa_String *          pSecurityPolicy,
			      OpcUa_UInt16            uSecurityMode
	);

	// Stubs
	static OpcUa_StatusCode TimerCallbackStub(OpcUa_Void   * pvCallbackData,
						  OpcUa_Timer    hTimer,
						  OpcUa_UInt32   msecElapsed
	);

	static OpcUa_StatusCode EndpointCallbackStub(OpcUa_Endpoint          hEndpoint,
						     OpcUa_Void *            pCallbackData,
						     OpcUa_Endpoint_Event    eEvent,
						     OpcUa_StatusCode        uStatus,
						     OpcUa_UInt32            uSecureChannelId,
						     OpcUa_ByteString *      pbsClientCertificate,
						     OpcUa_String *          pSecurityPolicy,
						     OpcUa_UInt16            uSecurityMode
	);

	static OpcUa_StatusCode GetEndpointsStub(OpcUa_Endpoint               hEndpoint,
						 OpcUa_Handle                 hContext,
						 OpcUa_RequestHeader *        pRequestHeader,
						 OpcUa_String *               pEndpointUrl,
						 OpcUa_Int32                  nNoOfLocaleIds,
						 OpcUa_String *               pLocaleIds,
						 OpcUa_Int32                  nNoOfProfileUris,
						 OpcUa_String *               pProfileUris,
						 OpcUa_ResponseHeader *       pResponseHeader,
						 OpcUa_Int32 *                pNoOfEndpoints,
						 OpcUa_EndpointDescription ** ppEndpoints
	);

	static OpcUa_StatusCode CreateSessionStub(OpcUa_Endpoint                       hEndpoint,
						  OpcUa_Handle                         hContext,
						  const OpcUa_RequestHeader *          pRequestHeader,
						  const OpcUa_ApplicationDescription * pClientDescription,
						  const OpcUa_String *                 pServerUri,
						  const OpcUa_String *                 pEndpointUrl,
						  const OpcUa_String *                 pSessionName,
						  const OpcUa_ByteString *             pClientNonce,
						  const OpcUa_ByteString *             pClientCertificate,
						  OpcUa_Double                         nRequestedSessionTimeout,
						  OpcUa_UInt32                         nMaxResponseMessageSize,
						  OpcUa_ResponseHeader *               pResponseHeader,
						  OpcUa_NodeId *                       pSessionId,
						  OpcUa_NodeId *                       pAuthenticationToken,
						  OpcUa_Double *                       pRevisedSessionTimeout,
						  OpcUa_ByteString *                   pServerNonce,
						  OpcUa_ByteString *                   pServerCertificate,
						  OpcUa_Int32 *                        pNoOfServerEndpoints,
						  OpcUa_EndpointDescription **         pServerEndpoints,
						  OpcUa_Int32 *                        pNoOfServerSoftwareCertificates,
						  OpcUa_SignedSoftwareCertificate **   pServerSoftwareCertificates,
						  OpcUa_SignatureData *                pServerSignature,
						  OpcUa_UInt32 *                       pMaxRequestMessageSize
	);

	static OpcUa_StatusCode ActivateSessionStub(OpcUa_Endpoint                          hEndpoint,
						    OpcUa_Handle                            hContext,
						    const OpcUa_RequestHeader *             pRequestHeader,
						    const OpcUa_SignatureData *             pClientSignature,
						    OpcUa_Int32                             nNoOfClientSoftwareCertificates,
						    const OpcUa_SignedSoftwareCertificate * pClientSoftwareCertificates,
						    OpcUa_Int32                             nNoOfLocaleIds,
						    const OpcUa_String *                    pLocaleIds,
						    const OpcUa_ExtensionObject *           pUserIdentityToken,
						    const OpcUa_SignatureData *             pUserTokenSignature,
						    OpcUa_ResponseHeader *                  pResponseHeader,
						    OpcUa_ByteString *                      pServerNonce,
						    OpcUa_Int32 *                           pNoOfResults,
						    OpcUa_StatusCode **                     pResults,
						    OpcUa_Int32 *                           pNoOfDiagnosticInfos,
						    OpcUa_DiagnosticInfo **                 pDiagnosticInfos
	);

	static OpcUa_StatusCode CloseSessionStub(OpcUa_Endpoint              hEndpoint,
						 OpcUa_Handle                hContext,
						 const OpcUa_RequestHeader * pRequestHeader,
						 OpcUa_Boolean               bDeleteSubscriptions,
						 OpcUa_ResponseHeader *      pResponseHeader
	);

	static OpcUa_StatusCode BrowseStub(OpcUa_Endpoint                 hEndpoint,
					   OpcUa_Handle                   hContext,
					   const OpcUa_RequestHeader *    pRequestHeader,
					   const OpcUa_ViewDescription *  pView,
					   OpcUa_UInt32                   nRequestedMaxReferencesPerNode,
					   OpcUa_Int32                    nNoOfNodesToBrowse,
					   OpcUa_BrowseDescription *	   pNodesToBrowse,
					   OpcUa_ResponseHeader *         pResponseHeader,
					   OpcUa_Int32 *                  pNoOfResults,
					   OpcUa_BrowseResult **          pResults,
					   OpcUa_Int32 *                  pNoOfDiagnosticInfos,
					   OpcUa_DiagnosticInfo **        pDiagnosticInfos
	);

	static OpcUa_StatusCode BrowseNextStub(OpcUa_Endpoint              hEndpoint,
					       OpcUa_Handle                hContext,
					       const OpcUa_RequestHeader * pRequestHeader,
					       OpcUa_Boolean               bReleaseContinuationPoints,
					       OpcUa_Int32                 nNoOfContinuationPoints,
					       const OpcUa_ByteString *    pContinuationPoints,
					       OpcUa_ResponseHeader *      pResponseHeader,
					       OpcUa_Int32 *               pNoOfResults,
					       OpcUa_BrowseResult **       pResults,
					       OpcUa_Int32 *               pNoOfDiagnosticInfos,
					       OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode TranslateBrowsePathsToNodeIdsStub(OpcUa_Endpoint              hEndpoint,
								  OpcUa_Handle                hContext,
								  const OpcUa_RequestHeader * pRequestHeader,
								  OpcUa_Int32                 nNoOfBrowsePaths,
								  const OpcUa_BrowsePath *    pBrowsePaths,
								  OpcUa_ResponseHeader *      pResponseHeader,
								  OpcUa_Int32 *               pNoOfResults,
								  OpcUa_BrowsePathResult **   pResults,
								  OpcUa_Int32 *               pNoOfDiagnosticInfos,
								  OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode ReadStub(OpcUa_Endpoint              hEndpoint,
					 OpcUa_Handle                hContext,
					 const OpcUa_RequestHeader * pRequestHeader,
					 OpcUa_Double                nMaxAge,
					 OpcUa_TimestampsToReturn    eTimestampsToReturn,
					 OpcUa_Int32                 nNoOfNodesToRead,
					 const OpcUa_ReadValueId *   pNodesToRead,
					 OpcUa_ResponseHeader *      pResponseHeader,
					 OpcUa_Int32 *               pNoOfResults,
					 OpcUa_DataValue **          pResults,
					 OpcUa_Int32 *               pNoOfDiagnosticInfos,
					 OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode WriteStub(OpcUa_Endpoint              hEndpoint,
					  OpcUa_Handle                hContext,
					  const OpcUa_RequestHeader * pRequestHeader,
					  OpcUa_Int32                 nNoOfNodesToWrite,
					  const OpcUa_WriteValue *    pNodesToWrite,
					  OpcUa_ResponseHeader *      pResponseHeader,
					  OpcUa_Int32 *               pNoOfResults,
					  OpcUa_StatusCode **         pResults,
					  OpcUa_Int32 *               pNoOfDiagnosticInfos,
					  OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode FindServersStub(OpcUa_Endpoint                  hEndpoint,
						OpcUa_Handle                    hContext,
						const OpcUa_RequestHeader *     pRequestHeader,
						const OpcUa_String *            pEndpointUrl,
						OpcUa_Int32                     nNoOfLocaleIds,
						const OpcUa_String *            pLocaleIds,
						OpcUa_Int32                     nNoOfServerUris,
						const OpcUa_String *            pServerUris,
						OpcUa_ResponseHeader *          pResponseHeader,
						OpcUa_Int32 *                   pNoOfServers,
						OpcUa_ApplicationDescription ** pServers
	);

	static OpcUa_StatusCode CreateSubscriptionStub(OpcUa_Endpoint              hEndpoint,
						       OpcUa_Handle                hContext,
						       const OpcUa_RequestHeader * pRequestHeader,
						       OpcUa_Double                nRequestedPublishingInterval,
						       OpcUa_UInt32                nRequestedLifetimeCount,
						       OpcUa_UInt32                nRequestedMaxKeepAliveCount,
						       OpcUa_UInt32                nMaxNotificationsPerPublish,
						       OpcUa_Boolean               bPublishingEnabled,
						       OpcUa_Byte                  nPriority,
						       OpcUa_ResponseHeader *      pResponseHeader,
						       OpcUa_UInt32 *              pSubscriptionId,
						       OpcUa_Double *              pRevisedPublishingInterval,
						       OpcUa_UInt32 *              pRevisedLifetimeCount,
						       OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
	);

	static OpcUa_StatusCode ModifySubscriptionStub(OpcUa_Endpoint              hEndpoint,
						       OpcUa_Handle                hContext,
						       const OpcUa_RequestHeader * pRequestHeader,
						       OpcUa_UInt32                nSubscriptionId,
						       OpcUa_Double                nRequestedPublishingInterval,
						       OpcUa_UInt32                nRequestedLifetimeCount,
						       OpcUa_UInt32                nRequestedMaxKeepAliveCount,
						       OpcUa_UInt32                nMaxNotificationsPerPublish,
						       OpcUa_Byte                  nPriority,
						       OpcUa_ResponseHeader *      pResponseHeader,
						       OpcUa_Double *              pRevisedPublishingInterval,
						       OpcUa_UInt32 *              pRevisedLifetimeCount,
						       OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
	);

	static OpcUa_StatusCode TransferSubscriptionsStub(OpcUa_Endpoint              hEndpoint,
							  OpcUa_Handle                hContext,
							  const OpcUa_RequestHeader * pRequestHeader,
							  OpcUa_Int32                 nNoOfSubscriptionIds,
							  const OpcUa_UInt32 *        pSubscriptionIds,
							  OpcUa_Boolean               bSendInitialValues,
							  OpcUa_ResponseHeader *      pResponseHeader,
							  OpcUa_Int32 *               pNoOfResults,
							  OpcUa_TransferResult **     pResults,
							  OpcUa_Int32 *               pNoOfDiagnosticInfos,
							  OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode SetPublishingModeStub(OpcUa_Endpoint              hEndpoint,
						      OpcUa_Handle                hContext,
						      const OpcUa_RequestHeader * pRequestHeader,
						      OpcUa_Boolean               bPublishingEnabled,
						      OpcUa_Int32                 nNoOfSubscriptionIds,
						      const OpcUa_UInt32 *        pSubscriptionIds,
						      OpcUa_ResponseHeader *      pResponseHeader,
						      OpcUa_Int32 *               pNoOfResults,
						      OpcUa_StatusCode **         pResults,
						      OpcUa_Int32 *               pNoOfDiagnosticInfos,
						      OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode DeleteSubscriptionsStub(OpcUa_Endpoint              hEndpoint,
							OpcUa_Handle                hContext,
							const OpcUa_RequestHeader * pRequestHeader,
							OpcUa_Int32                 nNoOfSubscriptionIds,
							const OpcUa_UInt32 *        pSubscriptionIds,
							OpcUa_ResponseHeader *      pResponseHeader,
							OpcUa_Int32 *               pNoOfResults,
							OpcUa_StatusCode **         pResults,
							OpcUa_Int32 *               pNoOfDiagnosticInfos,
							OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode CreateMonitoredItemsStub(OpcUa_Endpoint                           hEndpoint,
							 OpcUa_Handle                             hContext,
							 const OpcUa_RequestHeader *              pRequestHeader,
							 OpcUa_UInt32                             nSubscriptionId,
							 OpcUa_TimestampsToReturn                 eTimestampsToReturn,
							 OpcUa_Int32                              nNoOfItemsToCreate,
							 const OpcUa_MonitoredItemCreateRequest * pItemsToCreate,
							 OpcUa_ResponseHeader *                   pResponseHeader,
							 OpcUa_Int32 *                            pNoOfResults,
							 OpcUa_MonitoredItemCreateResult **       pResults,
							 OpcUa_Int32 *                            pNoOfDiagnosticInfos,
							 OpcUa_DiagnosticInfo **                  pDiagnosticInfos
	);

	static OpcUa_StatusCode ModifyMonitoredItemsStub(OpcUa_Endpoint                           hEndpoint,
							 OpcUa_Handle                             hContext,
							 const OpcUa_RequestHeader *              pRequestHeader,
							 OpcUa_UInt32                             nSubscriptionId,
							 OpcUa_TimestampsToReturn                 eTimestampsToReturn,
							 OpcUa_Int32                              nNoOfItemsToModify,
							 const OpcUa_MonitoredItemModifyRequest * pItemsToModify,
							 OpcUa_ResponseHeader *                   pResponseHeader,
							 OpcUa_Int32 *                            pNoOfResults,
							 OpcUa_MonitoredItemModifyResult **       pResults,
							 OpcUa_Int32 *                            pNoOfDiagnosticInfos,
							 OpcUa_DiagnosticInfo **                  pDiagnosticInfos
	);

	static OpcUa_StatusCode SetMonitoringModeStub(OpcUa_Endpoint              hEndpoint,
						      OpcUa_Handle                hContext,
						      const OpcUa_RequestHeader * pRequestHeader,
						      OpcUa_UInt32                nSubscriptionId,
						      OpcUa_MonitoringMode        eMonitoringMode,
						      OpcUa_Int32                 nNoOfMonitoredItemIds,
						      const OpcUa_UInt32 *        pMonitoredItemIds,
						      OpcUa_ResponseHeader *      pResponseHeader,
						      OpcUa_Int32 *               pNoOfResults,
						      OpcUa_StatusCode **         pResults,
						      OpcUa_Int32 *               pNoOfDiagnosticInfos,
						      OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode DeleteMonitoredItemsStub(OpcUa_Endpoint              hEndpoint,
							 OpcUa_Handle                hContext,
							 const OpcUa_RequestHeader * pRequestHeader,
							 OpcUa_UInt32                nSubscriptionId,
							 OpcUa_Int32                 nNoOfMonitoredItemIds,
							 const OpcUa_UInt32 *        pMonitoredItemIds,
							 OpcUa_ResponseHeader *      pResponseHeader,
							 OpcUa_Int32 *               pNoOfResults,
							 OpcUa_StatusCode **         pResults,
							 OpcUa_Int32 *               pNoOfDiagnosticInfos,
							 OpcUa_DiagnosticInfo **     pDiagnosticInfos
	);

	static OpcUa_StatusCode PublishStub(OpcUa_Endpoint                            hEndpoint,
					    OpcUa_Handle                              hContext,
					    const OpcUa_RequestHeader *               pRequestHeader,
					    OpcUa_Int32                               nNoOfSubscriptionAcknowledgements,
					    const OpcUa_SubscriptionAcknowledgement * pSubscriptionAcknowledgements,
					    OpcUa_ResponseHeader *                    pResponseHeader,
					    OpcUa_UInt32 *                            pSubscriptionId,
					    OpcUa_Int32 *                             pNoOfAvailableSequenceNumbers,
					    OpcUa_UInt32 **                           pAvailableSequenceNumbers,
					    OpcUa_Boolean *                           pMoreNotifications,
					    OpcUa_NotificationMessage *               pNotificationMessage,
					    OpcUa_Int32 *                             pNoOfResults,
					    OpcUa_StatusCode **                       pResults,
					    OpcUa_Int32 *                             pNoOfDiagnosticInfos,
					    OpcUa_DiagnosticInfo **                   pDiagnosticInfos
	);

	static OpcUa_StatusCode RepublishStub(OpcUa_Endpoint              hEndpoint,
					      OpcUa_Handle                hContext,
					      const OpcUa_RequestHeader * pRequestHeader,
					      OpcUa_UInt32                nSubscriptionId,
					      OpcUa_UInt32                nRetransmitSequenceNumber,
					      OpcUa_ResponseHeader *      pResponseHeader,
					      OpcUa_NotificationMessage * pNotificationMessage
	);

	static OpcUa_StatusCode HistoryReadStub(OpcUa_Endpoint                   hEndpoint,
						OpcUa_Handle                     hContext,
						const OpcUa_RequestHeader *      pRequestHeader,
						const OpcUa_ExtensionObject *    pHistoryReadDetails,
						OpcUa_TimestampsToReturn         eTimestampsToReturn,
						OpcUa_Boolean                    bReleaseContinuationPoints,
						OpcUa_Int32                      nNoOfNodesToRead,
						const OpcUa_HistoryReadValueId * pNodesToRead,
						OpcUa_ResponseHeader *           pResponseHeader,
						OpcUa_Int32 *                    pNoOfResults,
						OpcUa_HistoryReadResult **       pResults,
						OpcUa_Int32 *                    pNoOfDiagnosticInfos,
						OpcUa_DiagnosticInfo **          pDiagnosticInfos
	);

	static OpcUa_StatusCode CallStub(OpcUa_Endpoint                  hEndpoint,
					 OpcUa_Handle                    hContext,
					 const OpcUa_RequestHeader*      pRequestHeader,
					 OpcUa_Int32                     nNoOfMethodsToCall,
					 const OpcUa_CallMethodRequest * pMethodsToCall,
					 OpcUa_ResponseHeader*           pResponseHeader,
					 OpcUa_Int32*                    pNoOfResults,
					 OpcUa_CallMethodResult**        pResults,
					 OpcUa_Int32*                    pNoOfDiagnosticInfos,
					 OpcUa_DiagnosticInfo**          pDiagnosticInfos
	);

	// Debugging
	void AfxTrace(PCTXT pText, ...);
	void SubTrace(PCTXT pText, ...);
};

// End of File

#endif
