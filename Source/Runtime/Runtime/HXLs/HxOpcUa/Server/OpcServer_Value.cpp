
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../Model/OpcDataModel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Value Access

bool COpcServer::SkipValue(UINT si, UINT ns, UINT id)
{
	if( ns == 0 ) {

		switch( id ) {

			case OpcUaId_Server_ServerCapabilities_MaxQueryContinuationPoints:
			case OpcUaId_Server_ServerCapabilities_MaxHistoryContinuationPoints:
			case OpcUaId_Server_ServerCapabilities_SoftwareCertificates:
			case OpcUaId_Server_ServerStatus:
			case OpcUaId_Server_ServerStatus_ShutdownReason:
			case OpcUaId_Server_ServerStatus_SecondsTillShutdown:
			case OpcUaId_Server_ServerStatus_BuildInfo:
			case OpcUaId_Server_ServerStatus_BuildInfo_BuildDate:
			case OpcUaId_Server_ServerStatus_BuildInfo_BuildNumber:

				return true;

			case OpcUaId_ServerStatusType_State:
			case OpcUaId_ServerStatusType_StartTime:
			case OpcUaId_ServerStatusType_ShutdownReason:
			case OpcUaId_ServerStatusType_SecondsTillShutdown:
			case OpcUaId_ServerStatusType_CurrentTime:
			case OpcUaId_ServerStatusType_BuildInfo:

				return true;
			}

		return false;
		}

	return m_pHost->SkipValue(si, ns, id);
	}

void COpcServer::SetValueDouble(UINT si, UINT ns, UINT id, UINT n, double d)
{
	m_pHost->SetValueDouble(si, ns, id, n, d);
	}

void COpcServer::SetValueInt(UINT si, UINT ns, UINT id, UINT n, int d)
{
	m_pHost->SetValueInt(si, ns, id, n, d);
	}

void COpcServer::SetValueInt64(UINT si, UINT ns, UINT id, UINT n, INT64 d)
{
	m_pHost->SetValueInt64(si, ns, id, n, d);
	}

void COpcServer::SetValueString(UINT si, UINT ns, UINT id, UINT n, PCTXT d)
{
	m_pHost->SetValueString(si, ns, id, n, d);
	}

double COpcServer::GetValueDouble(UINT si, UINT ns, UINT id, UINT n)
{
	if( ns == 0 ) {

		switch( id ) {
	
			case OpcUaId_Server_ServerCapabilities_MinSupportedSampleRate:

				return 0;
			}

		return 0;
		}

	return m_pHost->GetValueDouble(si, ns, id, n);
	}

UINT COpcServer::GetValueInt(UINT si, UINT ns, UINT id, UINT n)
{
	if( ns == 0 ) {

		switch( id ) {

			case OpcUaId_Server_ServerStatus_State:

				return 0;

			case OpcUaId_Server_ServerCapabilities_MaxBrowseContinuationPoints:

				return 100;

			case OpcUaId_Server_ServerDiagnostics_EnabledFlag:

				return false;

			case OpcUaId_Server_Auditing:

				return false;

			case OpcUaId_Server_ServerRedundancy_RedundancySupport:

				return 0;

			case OpcUaId_Server_ServiceLevel:

				return 255;
			}

		return 0;
		}

	return m_pHost->GetValueInt(si, ns, id, n);
	}

UINT64 COpcServer::GetValueInt64(UINT si, UINT ns, UINT id, UINT n)
{
	return m_pHost->GetValueInt64(si, ns, id, n);
	}

CString COpcServer::GetValueString(UINT si, UINT ns, UINT id, UINT n)
{
	if( ns == 0 ) {

		switch( id ) {

			case OpcUaId_EnumStrings:

				switch( n ) {

					case 0: return "RUNNING";
					case 1: return "FAILED";
					case 2: return "NO_CONFIGURATION";
					case 3: return "SUSPENDED";
					case 4: return "SHUPDOWN";
					case 5: return "TEST";
					case 6: return "COMMUNICATIONFAULT";
					case 7: return "UNKNOWN";
					}

				break;

			case OpcUaId_Server_NamespaceArray:

				switch( n ) {

					case 0: return "http://opcfoundation.org/UA/";
					case 1: return "http://nanonamespace";
					}

				break;

			case OpcUaId_Server_ServerCapabilities_ServerProfileArray:

				switch( n ) {

					case 0: return "http://opcfoundation.org/UA-Profile/Server/NanoEmbeddedDevice";
					}

				break;

			case OpcUaId_Server_ServerCapabilities_LocaleIdArray:

				switch( n ) {

					case 0: return "en";
					}

				break;

			case OpcUaId_Server_ServerArray:
			case OpcUaId_Server_ServerStatus_BuildInfo_ManufacturerName:
			case OpcUaId_Server_ServerStatus_BuildInfo_ProductName:
			case OpcUaId_Server_ServerStatus_BuildInfo_ProductUri:
			case OpcUaId_Server_ServerStatus_BuildInfo_ApplicationUri:
			case OpcUaId_Server_ServerStatus_BuildInfo_SoftwareVersion:
			case OpcUaId_Server_ServerStatus_BuildInfo_BuildNumber:

				return m_pHost->GetValueString(si, ns, id, n);
			}

		return "";
		}

	return m_pHost->GetValueString(si, ns, id, n);
	}

void COpcServer::GetValueTime(OpcUa_DateTime &t, UINT si, UINT ns, UINT id, UINT n)
{
	if( ns == 0 ) {

		switch( id ) {

			case OpcUaId_Server_ServerStatus_StartTime:

				t = m_StartTime;

				return;

			case OpcUaId_Server_ServerStatus_CurrentTime:

				t = OpcUa_DateTime_UtcNow();

				return;
			}
		}

	TimeFromTimeval(t, m_pHost->GetValueTime(si, ns, id, n));
	}

bool COpcServer::HasEvents(CArray <UINT> &List, UINT ns, UINT id)
{
	if( ns == 0 && id == OpcUaId_Server ) {

		if( m_pModel->HasChanges() ) {

			List.Append(OpcUaId_GeneralModelChangeEventType);

			m_pModel->ClearChanges();

			return true;
			}
		}

	return m_pHost->HasEvents(List, ns, id);
	}

bool COpcServer::InitHistory(UINT si, UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	if( ns == 0 || !m_pHost->HasCustomHistory(ns, id) ) {

		switch( GetWireType(uType) ) {

			case OpcUaId_Double:
			case OpcUaId_Float:

				*((double *) pHistory) = GetValueDouble(si, ns, id, 0);

				return true;

			case OpcUaId_Byte:
			case OpcUaId_SByte:
			case OpcUaId_Int16:
			case OpcUaId_UInt16:
			case OpcUaId_Int32:
			case OpcUaId_UInt32:
			case OpcUaId_Boolean:

				*((int *) pHistory) = GetValueInt(si, ns, id, 0);

				return true;

			case OpcUaId_Int64:
			case OpcUaId_UInt64:

				*((INT64 *) pHistory) = GetValueInt64(si, ns, id, 0);

				return true;

			case OpcUaId_DateTime:
	
				GetValueTime((OpcUa_DateTime &) *pHistory, si, ns, id, 0);

				return true;

			case OpcUaId_String:

				* ((CString **) pHistory) = New CString;

				**((CString **) pHistory) = GetValueString(si, ns, id, 0);

				return true;
			}

		return false;
		}

	return m_pHost->InitHistory(ns, id, uType, pHistory);
	}

bool COpcServer::HasChanged(UINT si, UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	if( ns == 0 || !m_pHost->HasCustomHistory(ns, id) ) {

		switch( GetWireType(uType) ) {

			case OpcUaId_Double:
			case OpcUaId_Float:

				if( true ) {

					double dv = GetValueDouble(si, ns, id, 0);

					if( *((double *) pHistory) != dv ) {

						*((double *) pHistory) = dv;

						return true;
						}
					}

				return false;

			case OpcUaId_Byte:
			case OpcUaId_SByte:
			case OpcUaId_Int16:
			case OpcUaId_UInt16:
			case OpcUaId_Int32:
			case OpcUaId_UInt32:
			case OpcUaId_Boolean:

				if( true ) {

					int iv = GetValueInt(si, ns, id, 0);

					if( *((int *) pHistory) != iv ) {

						*((int *) pHistory) = iv;

						return true;
						}
					}

				return false;

			case OpcUaId_Int64:
			case OpcUaId_UInt64:

				if( true ) {

					INT64 iv = GetValueInt64(si, ns, id, 0);

					if( *((INT64 *) pHistory) != iv ) {

						*((INT64 *) pHistory) = iv;

						return true;
						}
					}

				return false;

			case OpcUaId_DateTime:

				if( true ) {

					OpcUa_DateTime dt;
	
					GetValueTime(dt, si, ns, id, 0);

					if( memcmp(&dt, pHistory, sizeof(dt)) ) {

						(OpcUa_DateTime &) pHistory[0] = dt;

						return true;
						}
					}

				return false;

			case OpcUaId_String:

				if( true ) {

					CString sv = GetValueString(si, ns, id, 0);

					if( sv.CompareC(**((CString **) pHistory)) ) {

						**((CString **) pHistory) = sv;

						return true;
						}
					}

				return false;
			}

		return false;
		}

	return m_pHost->HasChanged(ns, id, uType, pHistory);
	}

void COpcServer::KillHistory(UINT si, UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	if( ns == 0 || !m_pHost->HasCustomHistory(ns, id) ) {

		if( GetWireType(uType) == OpcUaId_String ) {

			delete *((CString **) pHistory);
			}

		return;
		}

	m_pHost->KillHistory(ns, id, uType, pHistory);
	}

UINT COpcServer::GetWireType(UINT uType)
{
	switch( uType ) {

		case OpcUaId_Double:
		case OpcUaId_Float:
		case OpcUaId_Byte:
		case OpcUaId_SByte:
		case OpcUaId_Int16:
		case OpcUaId_UInt16:
		case OpcUaId_Int32:
		case OpcUaId_UInt32:
		case OpcUaId_Int64:
		case OpcUaId_UInt64:
		case OpcUaId_Boolean:
		case OpcUaId_DateTime:
		case OpcUaId_String:

			return uType;

		case OpcUaId_Duration:

			return OpcUaId_Double;

		case OpcUaId_ServerState:
		case OpcUaId_ServerStatusDataType:
		case OpcUaId_RedundancySupport:

			return OpcUaId_Int32;

		case OpcUaId_LocaleId:
		case OpcUaId_SignedSoftwareCertificate:

			return OpcUaId_String;

		case OpcUaId_UtcTime:

			return OpcUaId_DateTime;
		}

	return m_pHost->GetWireType(uType);
	}

// End of File
