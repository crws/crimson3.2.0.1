
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Crypto Support

bool COpcServer::DecryptSecret(CByteArray &Secret, CString const &Method, CByteArray const &Data)
{
	UINT uFlags = 0;

	if( Method == OpcUa_AlgorithmUri_Encryption_Rsa15 ) {

		uFlags = rsaPadPkcs15;
	}

	if( Method == OpcUa_AlgorithmUri_Encryption_RsaOaep ) {

		uFlags = rsaPadOaep;
	}

	if( Method == OpcUa_AlgorithmUri_Encryption_RsaOaepSha256 ) {

		uFlags = rsaPadOaep256;
	}

	if( uFlags ) {

		AfxNewAutoObject(Cipher, "crypto.cipher-rsa", ICryptoAsym);

		if( Cipher ) {

			uFlags |= asymKeyPrivate | rsaFromDER;

			if( Cipher->Initialize(m_Priv, "", uFlags) ) {

				Cipher->Decrypt(Secret, Data.data());

				Cipher->Finalize();

				return true;
			}
		}
	}

	return false;
}

// End of File
