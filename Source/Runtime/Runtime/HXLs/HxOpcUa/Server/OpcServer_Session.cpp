
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Services

OpcUa_StatusCode COpcServer::CreateSession(OpcUa_Endpoint                       hEndpoint,
					   OpcUa_Handle                         hContext,
					   const OpcUa_RequestHeader *          pRequestHeader,
					   const OpcUa_ApplicationDescription * pClientDescription,
					   const OpcUa_String *                 pServerUri,
					   const OpcUa_String *                 pEndpointUrl,
					   const OpcUa_String *                 pSessionName,
					   const OpcUa_ByteString *             pClientNonce,
					   const OpcUa_ByteString *             pClientCertificate,
					   OpcUa_Double                         nRequestedSessionTimeout,
					   OpcUa_UInt32                         nMaxResponseMessageSize,
					   OpcUa_ResponseHeader *               pResponseHeader,
					   OpcUa_NodeId *                       pSessionId,
					   OpcUa_NodeId *                       pAuthenticationToken,
					   OpcUa_Double *                       pRevisedSessionTimeout,
					   OpcUa_ByteString *                   pServerNonce,
					   OpcUa_ByteString *                   pServerCertificate,
					   OpcUa_Int32 *                        pNoOfServerEndpoints,
					   OpcUa_EndpointDescription **         pServerEndpoints,
					   OpcUa_Int32 *                        pNoOfServerSoftwareCertificates,
					   OpcUa_SignedSoftwareCertificate **   pServerSoftwareCertificates,
					   OpcUa_SignatureData *                pServerSignature,
					   OpcUa_UInt32 *                       pMaxRequestMessageSize
)
{
	for( int n = 0; n < elements(m_Session); n++ ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[n];

		if( !Session.m_fInUse ) {

			m_pMutex->Free();

			AfxTrace("opc: open session %u\n", n);

			Session.m_fInUse   = true;

			Session.m_fActive  = false;

			Session.m_uToken   = MakeSessionToken(n);

			Session.m_Timeout  = Max(Min(nRequestedSessionTimeout, 10000), 60000);

			Session.m_IdleTime = 0;

			CreateSessionNonce(Session);

			SetSubsForNewSession(Session);

			OpcUa_Timer_Create(&Session.m_hTimer, 250, &TimerCallbackStub, OpcUa_Null, &Session);

			OpcUa_Endpoint_GetMessageSecureChannelId(hEndpoint, hContext, &Session.m_ChannelId);

			*pRevisedSessionTimeout                  = Session.m_Timeout;
			
			pSessionId->IdentifierType               = OpcUa_IdentifierType_Numeric;
			
			pSessionId->NamespaceIndex               = 0;

			pSessionId->Identifier.Numeric           = 1 + n;

			pAuthenticationToken->IdentifierType     = OpcUa_IdentifierType_Numeric;
			
			pAuthenticationToken->NamespaceIndex     = 0;

			pAuthenticationToken->Identifier.Numeric = Session.m_uToken;

			*pNoOfServerSoftwareCertificates         = 0;
			
			*pServerSoftwareCertificates             = OpcUa_Null;

			CopyBytes(pServerNonce, Session.m_Nonce);

			CopyBytes(pServerCertificate, m_Cert);

			OpcUa_SignatureData_Initialize(pServerSignature);

			*pMaxRequestMessageSize = nMaxResponseMessageSize;

			GetEndpoints(pNoOfServerEndpoints, pServerEndpoints);

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

			return OpcUa_Good;
		}

		m_pMutex->Free();
	}

	FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadTooManySessions);

	return OpcUa_Bad;
}

OpcUa_StatusCode COpcServer::ActivateSession(OpcUa_Endpoint                          hEndpoint,
					     OpcUa_Handle                            hContext,
					     const OpcUa_RequestHeader *             pRequestHeader,
					     const OpcUa_SignatureData *             pClientSignature,
					     OpcUa_Int32                             nNoOfClientSoftwareCertificates,
					     const OpcUa_SignedSoftwareCertificate * pClientSoftwareCertificates,
					     OpcUa_Int32                             nNoOfLocaleIds,
					     const OpcUa_String *                    pLocaleIds,
					     const OpcUa_ExtensionObject *           pUserIdentityToken,
					     const OpcUa_SignatureData *             pUserTokenSignature,
					     OpcUa_ResponseHeader *                  pResponseHeader,
					     OpcUa_ByteString *                      pServerNonce,
					     OpcUa_Int32 *                           pNoOfResults,
					     OpcUa_StatusCode **                     pResults,
					     OpcUa_Int32 *                           pNoOfDiagnosticInfos,
					     OpcUa_DiagnosticInfo **                 pDiagnosticInfos
)
{
	*pDiagnosticInfos     = OpcUa_Null;

	*pNoOfDiagnosticInfos = 0;

	UINT n;

	if( (n = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, false)) < NOTHING ) {

		CSession &Session = m_Session[n];

		if( !CheckUserIdentity(Session, pUserIdentityToken) ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadIdentityTokenRejected);

			return OpcUa_BadIdentityTokenRejected;
		}

		if( !Session.m_fActive ) {

			AfxTrace("opc: activate session %u\n", n);

			Session.m_fActive = true;
		}
		else {
			AfxTrace("opc: switch session %u\n", n);

			OpcUa_Endpoint_GetMessageSecureChannelId(hEndpoint, hContext, &Session.m_ChannelId);
		}

		*pResults     = OpcUa_Null;

		*pNoOfResults = 0;

		CreateSessionNonce(Session);

		CopyBytes(pServerNonce, Session.m_Nonce);

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		return OpcUa_Good;
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::CloseSession(OpcUa_Endpoint              hEndpoint,
					  OpcUa_Handle                hContext,
					  const OpcUa_RequestHeader * pRequestHeader,
					  OpcUa_Boolean               bDeleteSubscriptions,
					  OpcUa_ResponseHeader *      pResponseHeader
)
{
	UINT n;

	if( (n = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, false)) < NOTHING ) {

		CSession &Session = m_Session[n];

		AfxTrace("opc: close session %u\n", n);

		OpcUa_Timer_Delete(&Session.m_hTimer);

		if( bDeleteSubscriptions ) {

			DeleteSubsForSession(Session);
		}

		Session.m_fInUse  = false;

		Session.m_fActive = false;

		m_pHost->CloseSession(Session.m_uIndex);

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		return OpcUa_Good;
	}

	return pResponseHeader->ServiceResult;
}

// Session Helpers

UINT COpcServer::MakeSessionToken(UINT n)
{
	UINT v = 0;

	AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

	while( !v ) {

		if( pEntropy ) {

			pEntropy->GetEntropy(PBYTE(&v), sizeof(v));

			v %= 900000000;

			v += 100000000;
		}
		else {
			v = ++m_uSessId;
		}

		for( UINT s = 0; s < elements(m_Session); s++ ) {

			if( m_Session[s].m_uToken == v ) {

				v = 0;

				break;
			}
		}
	}

	m_uSessId = v;

	return v;
}

UINT COpcServer::FindSession(OpcUa_Endpoint hEndpoint, OpcUa_Handle hContext, OpcUa_RequestHeader const *pRequestHeader, OpcUa_ResponseHeader *pResponseHeader, bool fActive)
{
	m_pMutex->Wait(FOREVER);

	UINT n = NOTHING;

	if( pRequestHeader->AuthenticationToken.IdentifierType == OpcUa_IdentifierType_Numeric ) {

		if( pRequestHeader->AuthenticationToken.NamespaceIndex == 0 ) {

			for( UINT s = 0; s < elements(m_Session); s++ ) {

				if( m_Session[s].m_uToken == pRequestHeader->AuthenticationToken.Identifier.Numeric ) {

					n = s;

					break;
				}
			}
		}
	}

	if( n < NOTHING ) {

		CSession &Session = m_Session[n];

		if( Session.m_fInUse ) {

			// Session is only valid if it is active, or if we are
			// prepared to accept a non-active channel in response.

			if( !fActive || Session.m_fActive ) {

				// If we're looking for an active session or if
				// the found session is inactive and therefore
				// about to be activated, check to make sure it
				// is associated with this channel.

				if( fActive || !Session.m_fActive ) {

					OpcUa_UInt32 ChannelId;

					OpcUa_Endpoint_GetMessageSecureChannelId(hEndpoint, hContext, &ChannelId);

					if( ChannelId != Session.m_ChannelId ) {

						FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSecurityChecksFailed);

						m_pMutex->Free();

						AfxTrace("opc: session channel check failed\n");

						return NOTHING;
					}
				}

				Session.m_IdleTime = 0;

				m_pMutex->Free();

				return n;
			}

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSessionNotActivated);

			m_pMutex->Free();

			return NOTHING;
		}

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSessionIdInvalid);

		m_pMutex->Free();

		return NOTHING;
	}

	FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSecurityChecksFailed);

	m_pMutex->Free();

	return NOTHING;
}

void COpcServer::CreateSessionNonce(CSession &Session)
{
	Session.m_Nonce.SetCount(32);

	AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

	pEntropy->GetEntropy(Session.m_Nonce.data(), Session.m_Nonce.size());
}

void COpcServer::TickleSession(CSession &Session, UINT msTime)
{
	m_pMutex->Wait(FOREVER);

	if( (Session.m_IdleTime += msTime) > Session.m_Timeout ) {

		AfxTrace("opc: session %u timeout\n", Session.m_uIndex);

		OpcUa_Timer_Delete(&Session.m_hTimer);

		Session.m_fInUse   = false;

		Session.m_fActive  = false;

		m_pHost->CloseSession(Session.m_uIndex);

		DeleteSubsForSession(Session);
	}
	else
		TickleSubs(Session, msTime);

	m_pMutex->Free();
}

bool COpcServer::CheckUserIdentity(CSession &Session, OpcUa_ExtensionObject const *pUser)
{
	if( pUser->Encoding == OpcUa_ExtensionObjectEncoding_None ) {

		Session.m_fAuthed = false;

		return m_pHost->OpenSession(Session.m_uIndex, "", "");
	}

	if( pUser->TypeId.NodeId.Identifier.Numeric == OpcUaId_AnonymousIdentityToken_Encoding_DefaultBinary ) {

		Session.m_fAuthed = false;

		return m_pHost->OpenSession(Session.m_uIndex, "", "");
	}

	if( pUser->TypeId.NodeId.Identifier.Numeric == OpcUaId_UserNameIdentityToken_Encoding_DefaultBinary ) {

		OpcUa_UserNameIdentityToken *pToken = (OpcUa_UserNameIdentityToken *) pUser->Body.EncodeableObject.Object;

		CString User = pToken->UserName.strContent;

		if( pToken->Password.Length > 0 ) {

			if( pToken->EncryptionAlgorithm.uLength ) {

				CString    Method(pToken->EncryptionAlgorithm.strContent, pToken->EncryptionAlgorithm.uLength);

				CByteArray RxData(pToken->Password.Data, pToken->Password.Length);

				CByteArray Secret;

				if( DecryptSecret(Secret, Method, RxData) ) {

					if( Secret.size() >= 4 + Session.m_Nonce.size() ) {

						UINT uSize = IntelToHost(*PDWORD(Secret.data()));

						if( uSize == Secret.size() - 4 ) {

							UINT uPass = uSize - Session.m_Nonce.size();

							if( !memcmp(Secret.data() + 4 + uPass, Session.m_Nonce.data(), Session.m_Nonce.size()) ) {

								CString Pass(PCSTR(Secret.data() + 4), uPass);

								Session.m_fAuthed = true;

								return m_pHost->OpenSession(Session.m_uIndex, User, Pass);
							}
						}
					}
				}

				return false;
			}
			else {
				CString Pass(PCSTR(pToken->Password.Data), pToken->Password.Length);

				Session.m_fAuthed = true;

				return m_pHost->OpenSession(Session.m_uIndex, User, Pass);
			}
		}

		Session.m_fAuthed = true;

		return m_pHost->OpenSession(Session.m_uIndex, User, "");
	}

	return false;
}

// End of File
