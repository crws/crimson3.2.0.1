
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Services

OpcUa_StatusCode COpcServer::CreateSubscription(OpcUa_Endpoint              hEndpoint,
						OpcUa_Handle                hContext,
						const OpcUa_RequestHeader * pRequestHeader,
						OpcUa_Double                nRequestedPublishingInterval,
						OpcUa_UInt32                nRequestedLifetimeCount,
						OpcUa_UInt32                nRequestedMaxKeepAliveCount,
						OpcUa_UInt32                nMaxNotificationsPerPublish,
						OpcUa_Boolean               bPublishingEnabled,
						OpcUa_Byte                  nPriority,
						OpcUa_ResponseHeader *      pResponseHeader,
						OpcUa_UInt32 *              pSubscriptionId,
						OpcUa_Double *              pRevisedPublishingInterval,
						OpcUa_UInt32 *              pRevisedLifetimeCount,
						OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
)
{
	UINT s;

	AfxTrace("opc: createsubscription\n");

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		CSession &Session = m_Session[s];

		if( Session.m_Subs.GetCount() >= 100 ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadTooManySubscriptions);
		}
		else {
			CSubscription *pSub  = New CSubscription;

			pSub->m_uIndex			    = m_uSubId++;

			pSub->m_uState			    = subNormal;

			pSub->m_nPublishingInterval         = Max(nRequestedPublishingInterval, 250);

			pSub->m_nLifetimeCount	            = Max(nRequestedLifetimeCount, UINT(5000 / pSub->m_nPublishingInterval));

			pSub->m_nMaxKeepAliveCount          = Max(nRequestedMaxKeepAliveCount, 10);

			pSub->m_nMaxNotificationsPerPublish = nMaxNotificationsPerPublish;

			pSub->m_nPriority                   = nPriority;

			pSub->m_fPublishingEnabled          = bPublishingEnabled ? true : false;

			pSub->m_Timer			    = 0;

			pSub->m_uKeepAlive		    = 0;

			pSub->m_uItemId			    = 2000;

			pSub->m_uSeqNum			    = 1;

			m_pMutex->Wait(FOREVER);

			INDEX i = Session.m_Subs.Append(pSub);

			Session.m_Index.Insert(pSub->m_uIndex, i);

			m_pMutex->Free();

			*pSubscriptionId	    = pSub->m_uIndex;

			*pRevisedPublishingInterval = pSub->m_nPublishingInterval;

			*pRevisedLifetimeCount      = pSub->m_nLifetimeCount;

			*pRevisedMaxKeepAliveCount  = pSub->m_nMaxKeepAliveCount;

			AfxTrace("     %u\n", pSub->m_uIndex);

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
		}
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::ModifySubscription(OpcUa_Endpoint              hEndpoint,
						OpcUa_Handle                hContext,
						const OpcUa_RequestHeader * pRequestHeader,
						OpcUa_UInt32                nSubscriptionId,
						OpcUa_Double                nRequestedPublishingInterval,
						OpcUa_UInt32                nRequestedLifetimeCount,
						OpcUa_UInt32                nRequestedMaxKeepAliveCount,
						OpcUa_UInt32                nMaxNotificationsPerPublish,
						OpcUa_Byte                  nPriority,
						OpcUa_ResponseHeader *      pResponseHeader,
						OpcUa_Double *              pRevisedPublishingInterval,
						OpcUa_UInt32 *              pRevisedLifetimeCount,
						OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
)
{
	UINT s;

	AfxTrace("opc: modifysubscription\n");

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[s];

		INDEX    i        = Session.m_Index.FindName(nSubscriptionId);

		if( Session.m_Subs.Failed(i) ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSubscriptionIdInvalid);
		}
		else {
			INDEX          p    = Session.m_Index[i];

			CSubscription *pSub = Session.m_Subs[p];

			AfxTrace("     %u\n", pSub->m_uIndex);

			pSub->m_nPublishingInterval         = Max(nRequestedPublishingInterval, 250);

			pSub->m_nLifetimeCount	            = Max(nRequestedLifetimeCount, UINT(5000 / pSub->m_nPublishingInterval));

			pSub->m_nMaxKeepAliveCount          = Max(nRequestedMaxKeepAliveCount, 10);

			pSub->m_nMaxNotificationsPerPublish = nMaxNotificationsPerPublish;

			pSub->m_nPriority                   = nPriority;

			*pRevisedPublishingInterval = pSub->m_nPublishingInterval;

			*pRevisedLifetimeCount      = pSub->m_nLifetimeCount;

			*pRevisedMaxKeepAliveCount  = pSub->m_nMaxKeepAliveCount;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
		}

		m_pMutex->Free();
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::TransferSubscriptions(OpcUa_Endpoint              hEndpoint,
						   OpcUa_Handle                hContext,
						   const OpcUa_RequestHeader * pRequestHeader,
						   OpcUa_Int32                 nNoOfSubscriptionIds,
						   const OpcUa_UInt32 *        pSubscriptionIds,
						   OpcUa_Boolean               bSendInitialValues,
						   OpcUa_ResponseHeader *      pResponseHeader,
						   OpcUa_Int32 *               pNoOfResults,
						   OpcUa_TransferResult **     pResults,
						   OpcUa_Int32 *               pNoOfDiagnosticInfos,
						   OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	UINT s;

	AfxTrace("opc: transfersubscription\n");

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		CSession &Session = m_Session[s];

		if( !nNoOfSubscriptionIds ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);
		}
		else {
			*pResults = OpcAlloc(nNoOfSubscriptionIds, OpcUa_TransferResult);

			m_pMutex->Wait(FOREVER);

			for( INT m = 0; m < nNoOfSubscriptionIds; m++ ) {

				UINT		      SubId   = pSubscriptionIds[m];

				OpcUa_TransferResult *pResult = *pResults + m;

				bool                  fHit    = false;

				OpcUa_TransferResult_Initialize(pResult);

				AfxTrace("     transferring %u ", SubId);

				for( int n = 0; n < elements(m_Session); n++ ) {

					CSession &From = m_Session[n];

					if( From.m_fInUse ) {

						INDEX i = From.m_Index.FindName(SubId);

						if( !From.m_Index.Failed(i) ) {

							AfxTrace("\n");

							AfxTrace("     avail = %u\n", From.m_Avail.GetCount());

							AfxTrace("     saved = %u\n", From.m_Saved.GetCount());

							INDEX          s    = From.m_Index[i];

							CSubscription *pSub = From.m_Subs[s];

							TransferNotificationsForSub(Session.m_Avail,
										    From.m_Avail,
										    pSub,
										    NULL,
										    NULL
							);

							TransferNotificationsForSub(Session.m_Saved,
										    From.m_Saved,
										    pSub,
										    &pResult->NoOfAvailableSequenceNumbers,
										    &pResult->AvailableSequenceNumbers
							);

							if( From.m_NextSub == s ) {

								From.m_Subs.GetNext(From.m_NextSub);
							}

							From.m_Subs.Remove(s);

							From.m_Index.Remove(i);

							INDEX j = Session.m_Subs.Append(pSub);

							Session.m_Index.Insert(pSub->m_uIndex, j);

							fHit = true;

							break;
						}
					}
				}

				if( fHit ) {

					pResult->StatusCode = OpcUa_Good;

					AfxTrace("(found)\n");
				}
				else {
					pResult->NoOfAvailableSequenceNumbers = 0;

					pResult->AvailableSequenceNumbers     = NULL;

					pResult->StatusCode = OpcUa_BadSubscriptionIdInvalid;

					AfxTrace("(not found)\n");
				}
			}

			m_pMutex->Free();

			*pNoOfResults = nNoOfSubscriptionIds;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
		}
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::SetPublishingMode(OpcUa_Endpoint              hEndpoint,
					       OpcUa_Handle                hContext,
					       const OpcUa_RequestHeader * pRequestHeader,
					       OpcUa_Boolean               bPublishingEnabled,
					       OpcUa_Int32                 nNoOfSubscriptionIds,
					       const OpcUa_UInt32 *        pSubscriptionIds,
					       OpcUa_ResponseHeader *      pResponseHeader,
					       OpcUa_Int32 *               pNoOfResults,
					       OpcUa_StatusCode **         pResults,
					       OpcUa_Int32 *               pNoOfDiagnosticInfos,
					       OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	UINT s;

	AfxTrace("opc: setpublishingmode\n");

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		CSession &Session = m_Session[s];

		if( !nNoOfSubscriptionIds ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);
		}
		else {
			*pResults = OpcAlloc(nNoOfSubscriptionIds, OpcUa_StatusCode);

			m_pMutex->Wait(FOREVER);

			for( INT m = 0; m < nNoOfSubscriptionIds; m++ ) {

				UINT		  SubId   = pSubscriptionIds[m];

				OpcUa_StatusCode *pResult = *pResults + m;

				INDEX		  i       = Session.m_Index.FindName(SubId);

				if( !Session.m_Subs.Failed(i) ) {

					INDEX          n    = Session.m_Index[i];

					CSubscription *pSub = Session.m_Subs[n];

					AfxTrace("     %u\n", pSub->m_uIndex);

					if( !bPublishingEnabled ) {

						RemoveNotificationsForSub(Session.m_Avail, pSub);
					}

					pSub->m_fPublishingEnabled = bPublishingEnabled ? true : false;

					pSub->m_Timer              = 0;

					*pResult = OpcUa_Good;

					continue;
				}

				*pResult = OpcUa_BadSubscriptionIdInvalid;
			}

			m_pMutex->Free();

			*pNoOfResults = nNoOfSubscriptionIds;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
		}
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::DeleteSubscriptions(OpcUa_Endpoint              hEndpoint,
						 OpcUa_Handle                hContext,
						 const OpcUa_RequestHeader * pRequestHeader,
						 OpcUa_Int32                 nNoOfSubscriptionIds,
						 const OpcUa_UInt32 *        pSubscriptionIds,
						 OpcUa_ResponseHeader *      pResponseHeader,
						 OpcUa_Int32 *               pNoOfResults,
						 OpcUa_StatusCode **         pResults,
						 OpcUa_Int32 *               pNoOfDiagnosticInfos,
						 OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	UINT s;

	AfxTrace("opc: deletesubscriptions\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		CSession &Session = m_Session[s];

		if( !nNoOfSubscriptionIds ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);
		}
		else {
			*pResults = OpcAlloc(nNoOfSubscriptionIds, OpcUa_StatusCode);

			m_pMutex->Wait(FOREVER);

			for( INT m = 0; m < nNoOfSubscriptionIds; m++ ) {

				UINT		  SubId   = pSubscriptionIds[m];

				OpcUa_StatusCode *pResult = *pResults + m;

				INDEX		  i       = Session.m_Index.FindName(SubId);

				if( !Session.m_Subs.Failed(i) ) {

					INDEX          n    = Session.m_Index[i];

					CSubscription *pSub = Session.m_Subs[n];

					if( Session.m_NextSub == n ) {

						Session.m_Subs.GetNext(Session.m_NextSub);
					}

					Session.m_Subs.Remove(n);

					Session.m_Index.Remove(i);

					RemoveNotificationsForSub(Session.m_Avail, pSub);

					RemoveNotificationsForSub(Session.m_Saved, pSub);

					DeleteSub(Session, pSub);

					*pResult = OpcUa_Good;

					continue;
				}

				*pResult = OpcUa_BadSubscriptionIdInvalid;
			}

			m_pMutex->Free();

			*pNoOfResults = nNoOfSubscriptionIds;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
		}
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::CreateMonitoredItems(OpcUa_Endpoint                           hEndpoint,
						  OpcUa_Handle                             hContext,
						  const OpcUa_RequestHeader *              pRequestHeader,
						  OpcUa_UInt32                             nSubscriptionId,
						  OpcUa_TimestampsToReturn                 eTimestampsToReturn,
						  OpcUa_Int32                              nNoOfItemsToCreate,
						  const OpcUa_MonitoredItemCreateRequest * pItemsToCreate,
						  OpcUa_ResponseHeader *                   pResponseHeader,
						  OpcUa_Int32 *                            pNoOfResults,
						  OpcUa_MonitoredItemCreateResult **       pResults,
						  OpcUa_Int32 *                            pNoOfDiagnosticInfos,
						  OpcUa_DiagnosticInfo **                  pDiagnosticInfos
)
{
	UINT s;

	AfxTrace("opc: createmonitoreditems\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[s];

		INDEX    i        = Session.m_Index.FindName(nSubscriptionId);

		if( Session.m_Subs.Failed(i) ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSubscriptionIdInvalid);
		}
		else {
			INDEX          p    = Session.m_Index[i];

			CSubscription *pSub = Session.m_Subs[p];

			AfxTrace("     %u\n", pSub->m_uIndex);

			if( !nNoOfItemsToCreate ) {

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);
			}
			else {
				CArray <CMonitoredItem *> DataList;

				CArray <CMonitoredItem *> EventList;

				CArray <UINT>             CodesList;

				*pResults = OpcAlloc(nNoOfItemsToCreate, OpcUa_MonitoredItemCreateResult);

				for( int m = 0; m < nNoOfItemsToCreate; m++ ) {

					OpcUa_MonitoredItemCreateRequest const *pCreate = pItemsToCreate + m;

					OpcUa_ReadValueId		 const *pRead   = &pCreate->ItemToMonitor;

					OpcUa_MonitoredItemCreateResult        *pResult = *pResults      + m;

					OpcUa_MonitoredItemCreateResult_Initialize(pResult);

					pResult->StatusCode = OpcUa_BadAttributeIdInvalid;

					OpcUa_DataValue Test;

					OpcUa_DataValue_Initialize(&Test);

					ReadNode(Session, &Test, pRead, OpcUa_TimestampsToReturn_Neither, m);

					if( OpcUa_IsGood(Test.StatusCode) ) {

						if( Test.Value.ArrayType == OpcUa_VariantArrayType_Scalar ) {

							CMonitoredItem *pItem        = New CMonitoredItem;

							pItem->m_uIndex              = ++pSub->m_uItemId;

							pItem->m_Mode                = pCreate->MonitoringMode;

							pItem->m_Parameters          = pCreate->RequestedParameters;

							pItem->m_eTimestampsToReturn = eTimestampsToReturn;

							pItem->m_ReadValueId         = *pRead;

							pItem->m_Queued              = 0;

							pItem->m_DataType            = Test.Value.Datatype;

							pItem->m_LastTime            = GetTickCount();

							pItem->m_SendTime            = pItem->m_LastTime;

							bool fInit = false;

							if( pRead->AttributeId == OpcUa_Attributes_EventNotifier ) {

								// TODO -- Add support for the filter object? Right
								// now we only really use this for model change events
								// from the server object so it doesn't matter...

								if( pItem->m_Mode == OpcUa_MonitoringMode_Reporting ) {

									EventList.Append(pItem);

									CodesList.Append(0);
								}

								fInit = true;
							}

							if( pRead->AttributeId == OpcUa_Attributes_Value ) {

								COpcNodeId const &FilterId = (COpcNodeId &) pCreate->RequestedParameters.Filter.TypeId.NodeId;

								if( FilterId.IsNull() ) {

									fInit = InitHistory(Session.m_uIndex,
											    pItem->m_ReadValueId.NodeId.NamespaceIndex,
											    pItem->m_ReadValueId.NodeId.Identifier.Numeric,
											    pItem->m_DataType,
											    pItem->m_History
									);

									if( fInit ) {

										if( pItem->m_Mode != OpcUa_MonitoringMode_Disabled ) {

											// The spec says in 5.12.1.3 that we should create an
											// initial report when switching to anything other than
											// Disabled mode, but it's a bit contradictory as it
											// also says we don't report in Sampling. We interpret
											// this loosely to ensure notifications are sent.

											DataList.Append(pItem);
										}
									}
								}
								else
									pResult->StatusCode = OpcUa_BadFilterNotAllowed;
							}

							if( fInit ) {

								MakeMax(pItem->m_Parameters.QueueSize, 60);

								pItem->m_Parameters.SamplingInterval = pSub->m_nPublishingInterval;

								pResult->MonitoredItemId             = pItem->m_uIndex;

								pResult->RevisedQueueSize            = pItem->m_Parameters.QueueSize;

								pResult->RevisedSamplingInterval     = pItem->m_Parameters.SamplingInterval;

								pResult->StatusCode                  = OpcUa_Good;

								INDEX i = pSub->m_Items.Append(pItem);

								pSub->m_Index.Insert(pItem->m_uIndex, i);
							}
							else
								delete pItem;
						}
					}
					else
						pResult->StatusCode = Test.StatusCode;

					OpcUa_DataValue_Clear(&Test);
				}

				QueueNotificationBurst(Session, pSub, DataList, NULL);

				QueueNotificationBurst(Session, pSub, EventList, CodesList.GetPointer());

				*pNoOfResults = nNoOfItemsToCreate;

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
			}
		}

		m_pMutex->Free();
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::ModifyMonitoredItems(OpcUa_Endpoint                           hEndpoint,
						  OpcUa_Handle                             hContext,
						  const OpcUa_RequestHeader *              pRequestHeader,
						  OpcUa_UInt32                             nSubscriptionId,
						  OpcUa_TimestampsToReturn                 eTimestampsToReturn,
						  OpcUa_Int32                              nNoOfItemsToModify,
						  const OpcUa_MonitoredItemModifyRequest * pItemsToModify,
						  OpcUa_ResponseHeader *                   pResponseHeader,
						  OpcUa_Int32 *                            pNoOfResults,
						  OpcUa_MonitoredItemModifyResult **       pResults,
						  OpcUa_Int32 *                            pNoOfDiagnosticInfos,
						  OpcUa_DiagnosticInfo **                  pDiagnosticInfos
)
{
	UINT s;

	AfxTrace("opc: modifymonitoreditems\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[s];

		INDEX    i        = Session.m_Index.FindName(nSubscriptionId);

		if( Session.m_Subs.Failed(i) ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSubscriptionIdInvalid);
		}
		else {
			INDEX          p    = Session.m_Index[i];

			CSubscription *pSub = Session.m_Subs[p];

			AfxTrace("     %u\n", pSub->m_uIndex);

			if( !nNoOfItemsToModify ) {

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);
			}
			else {
				*pResults = OpcAlloc(nNoOfItemsToModify, OpcUa_MonitoredItemModifyResult);

				for( int m = 0; m < nNoOfItemsToModify; m++ ) {

					OpcUa_MonitoredItemModifyRequest const *pModify = pItemsToModify + m;

					OpcUa_MonitoredItemModifyResult        *pResult = *pResults      + m;

					OpcUa_MonitoredItemModifyResult_Initialize(pResult);

					INDEX i = pSub->m_Index.FindName(pModify->MonitoredItemId);

					if( !pSub->m_Index.Failed(i) ) {

						INDEX           n     = pSub->m_Index[i];

						CMonitoredItem *pItem = pSub->m_Items[n];

						if( pModify->RequestedParameters.ClientHandle != pItem->m_Parameters.ClientHandle ) {

							// If the client handle has changed, we do not want
							// to send any notifications using the old handle so
							// we strip them out of the queue.

							RemoveNotificationsForItem(Session.m_Avail, pItem, true);
						}

						pItem->m_Parameters                  = pModify->RequestedParameters;

						pItem->m_Parameters.SamplingInterval = pSub->m_nPublishingInterval;

						MakeMax(pItem->m_Parameters.QueueSize, 60);

						pResult->RevisedQueueSize            = pItem->m_Parameters.QueueSize;

						pResult->RevisedSamplingInterval     = pItem->m_Parameters.SamplingInterval;

						pResult->StatusCode		     = OpcUa_Good;
					}
					else
						pResult->StatusCode = OpcUa_BadMonitoredItemIdInvalid;
				}

				*pNoOfResults = nNoOfItemsToModify;

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
			}
		}

		m_pMutex->Free();
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::DeleteMonitoredItems(OpcUa_Endpoint              hEndpoint,
						  OpcUa_Handle                hContext,
						  const OpcUa_RequestHeader * pRequestHeader,
						  OpcUa_UInt32                nSubscriptionId,
						  OpcUa_Int32                 nNoOfMonitoredItemIds,
						  const OpcUa_UInt32 *        pMonitoredItemIds,
						  OpcUa_ResponseHeader *      pResponseHeader,
						  OpcUa_Int32 *               pNoOfResults,
						  OpcUa_StatusCode **         pResults,
						  OpcUa_Int32 *               pNoOfDiagnosticInfos,
						  OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	UINT s;

	AfxTrace("opc: deletemonitoreditems\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[s];

		INDEX    i        = Session.m_Index.FindName(nSubscriptionId);

		if( Session.m_Subs.Failed(i) ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSubscriptionIdInvalid);
		}
		else {
			INDEX          p    = Session.m_Index[i];

			CSubscription *pSub = Session.m_Subs[p];

			AfxTrace("     %u\n", pSub->m_uIndex);

			if( !nNoOfMonitoredItemIds ) {

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);
			}
			else {
				*pResults = OpcAlloc(nNoOfMonitoredItemIds, OpcUa_StatusCode);

				for( int m = 0; m < nNoOfMonitoredItemIds; m++ ) {

					INDEX i = pSub->m_Index.FindName(pMonitoredItemIds[m]);

					if( !pSub->m_Index.Failed(i) ) {

						INDEX           n     = pSub->m_Index[i];

						CMonitoredItem *pItem = pSub->m_Items[n];

						RemoveNotificationsForItem(Session.m_Avail, pItem, true);

						RemoveNotificationsForItem(Session.m_Saved, pItem, false);

						pSub->m_Items.Remove(n);

						pSub->m_Index.Remove(i);

						delete pItem;

						(*pResults)[m] = OpcUa_Good;
					}
					else
						(*pResults)[m] = OpcUa_BadMonitoredItemIdInvalid;
				}

				*pNoOfResults = nNoOfMonitoredItemIds;

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
			}
		}

		m_pMutex->Free();
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::SetMonitoringMode(OpcUa_Endpoint              hEndpoint,
					       OpcUa_Handle                hContext,
					       const OpcUa_RequestHeader * pRequestHeader,
					       OpcUa_UInt32                nSubscriptionId,
					       OpcUa_MonitoringMode        eMonitoringMode,
					       OpcUa_Int32                 nNoOfMonitoredItemIds,
					       const OpcUa_UInt32 *        pMonitoredItemIds,
					       OpcUa_ResponseHeader *      pResponseHeader,
					       OpcUa_Int32 *               pNoOfResults,
					       OpcUa_StatusCode **         pResults,
					       OpcUa_Int32 *               pNoOfDiagnosticInfos,
					       OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	UINT s;

	AfxTrace("opc: setmonitoringmode\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[s];

		INDEX    i        = Session.m_Index.FindName(nSubscriptionId);

		if( Session.m_Subs.Failed(i) ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSubscriptionIdInvalid);
		}
		else {
			INDEX          p    = Session.m_Index[i];

			CSubscription *pSub = Session.m_Subs[p];

			if( !nNoOfMonitoredItemIds ) {

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);
			}
			else {
				CArray <CMonitoredItem *> DataList;

				*pResults = OpcAlloc(nNoOfMonitoredItemIds, OpcUa_StatusCode);

				for( int m = 0; m < nNoOfMonitoredItemIds; m++ ) {

					INDEX i = pSub->m_Index.FindName(pMonitoredItemIds[m]);

					if( !pSub->m_Index.Failed(i) ) {

						INDEX           n     = pSub->m_Index[i];

						CMonitoredItem *pItem = pSub->m_Items[n];

						if( pItem->m_Mode != eMonitoringMode ) {

							if( eMonitoringMode == OpcUa_MonitoringMode_Disabled ) {

								RemoveNotificationsForItem(Session.m_Avail, pItem, true);
							}

							if( pItem->m_Mode == OpcUa_MonitoringMode_Disabled ) {

								DataList.Append(pItem);
							}

							pItem->m_Mode = eMonitoringMode;
						}

						(*pResults)[m] = OpcUa_Good;
					}
					else
						(*pResults)[m] = OpcUa_BadMonitoredItemIdInvalid;
				}

				QueueNotificationBurst(Session, pSub, DataList, NULL);

				*pNoOfResults = nNoOfMonitoredItemIds;

				FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);
			}
		}

		m_pMutex->Free();
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::Publish(OpcUa_Endpoint                            hEndpoint,
				     OpcUa_Handle                              hContext,
				     const OpcUa_RequestHeader *               pRequestHeader,
				     OpcUa_Int32                               nNoOfSubscriptionAcknowledgements,
				     const OpcUa_SubscriptionAcknowledgement * pSubscriptionAcknowledgements,
				     OpcUa_ResponseHeader *                    pResponseHeader,
				     OpcUa_UInt32 *                            pSubscriptionId,
				     OpcUa_Int32 *                             pNoOfAvailableSequenceNumbers,
				     OpcUa_UInt32 **                           pAvailableSequenceNumbers,
				     OpcUa_Boolean *                           pMoreNotifications,
				     OpcUa_NotificationMessage *               pNotificationMessage,
				     OpcUa_Int32 *                             pNoOfResults,
				     OpcUa_StatusCode **                       pResults,
				     OpcUa_Int32 *                             pNoOfDiagnosticInfos,
				     OpcUa_DiagnosticInfo **                   pDiagnosticInfos
)
{
	UINT s;

	SubTrace("opc: publish\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[s];

		ProcessAcks(Session, pNoOfResults, pResults, nNoOfSubscriptionAcknowledgements, pSubscriptionAcknowledgements);

		bool fHit = false;

		for( UINT p = 0; p < 2; p++ ) {

			if( !Session.m_Avail.IsEmpty() ) {

				CoalesceNotifications(Session, Session.m_Avail);

				INDEX			   Head = Session.m_Avail.GetHead();

				CNotification             *pNot = Session.m_Avail[Head];

				OpcUa_NotificationMessage *pMsg = pNot->m_pMsg;

				CopyNotificationMessage(pNotificationMessage, pMsg);

				for( UINT n = 0; n < pNot->m_List.GetCount(); n++ ) {

					CMonitoredItem *pItem = pNot->m_List[n];

					pItem->m_Queued--;
				}

				*pSubscriptionId    = pNot->m_uSub;

				*pMoreNotifications = !Session.m_Avail.IsEmpty();

				SubTrace("     reply with %u.%u\n", pNot->m_uSub, pMsg->SequenceNumber);

				Session.m_Avail.Remove(Head);

				Session.m_Saved.Append(pNot);

				// Notice we add the available sequence numbers to the message
				// after we have added the current notification to the list. The
				// specification is unclear on whether the current notification
				// should be included, but UAExpert complains if it is not.

				GetSavedNotifications(Session, *pSubscriptionId, pNoOfAvailableSequenceNumbers, pAvailableSequenceNumbers);

				fHit = true;

				break;
			}

			if( p == 0 ) {

				m_pMutex->Free();

				if( !Session.m_pEvent->Wait(125) ) {

					m_pMutex->Wait(FOREVER);

					break;
				}

				m_pMutex->Wait(FOREVER);
			}
		}

		if( !fHit ) {

			pNotificationMessage->SequenceNumber = 0;

			*pSubscriptionId    = 0;

			*pMoreNotifications = FALSE;

			for( UINT p = 0; p < Session.m_Subs.GetCount(); p++ ) {

				if( !Session.m_NextSub ) {

					Session.m_NextSub = Session.m_Subs.GetHead();
				}

				if( Session.m_NextSub ) {

					CSubscription *pSub = Session.m_Subs[Session.m_NextSub];

					Session.m_Subs.GetNext(Session.m_NextSub);

					// We always send a keep-alive message for some subscription
					// as clients such as UA-Expert complain in their debug log if
					// we send a Publish message with a zero subscription id, even
					// though the specification says this is okay...

					if( true || ++pSub->m_uKeepAlive >= pSub->m_nMaxKeepAliveCount ) {

						SubTrace("     keep alive %u\n", pSub->m_uIndex);

						pNotificationMessage->SequenceNumber = pSub->m_uSeqNum;

						*pSubscriptionId    = pSub->m_uIndex;

						*pMoreNotifications = FALSE;

						pSub->m_uKeepAlive  = 0;

						GetSavedNotifications(Session, *pSubscriptionId, pNoOfAvailableSequenceNumbers, pAvailableSequenceNumbers);

						break;
					}

					continue;
				}

				break;
			}

			pNotificationMessage->PublishTime          = OpcUa_DateTime_UtcNow();

			pNotificationMessage->NoOfNotificationData = 0;

			pNotificationMessage->NotificationData     = NULL;
		}

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		m_pMutex->Free();
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::Republish(OpcUa_Endpoint              hEndpoint,
				       OpcUa_Handle                hContext,
				       const OpcUa_RequestHeader * pRequestHeader,
				       OpcUa_UInt32                nSubscriptionId,
				       OpcUa_UInt32                nRetransmitSequenceNumber,
				       OpcUa_ResponseHeader *      pResponseHeader,
				       OpcUa_NotificationMessage * pNotificationMessage
)
{
	UINT s;

	if( (s = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true)) < NOTHING ) {

		m_pMutex->Wait(FOREVER);

		CSession &Session = m_Session[s];

		for( INDEX n = Session.m_Saved.GetHead(); !Session.m_Saved.Failed(n); Session.m_Saved.GetNext(n) ) {

			CNotification *pNot = Session.m_Saved[n];

			if( pNot->m_uSub == nSubscriptionId ) {

				if( pNot->m_pMsg->SequenceNumber == nRetransmitSequenceNumber ) {

					OpcUa_NotificationMessage *pMsg = pNot->m_pMsg;

					CopyNotificationMessage(pNotificationMessage, pMsg);

					FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

					m_pMutex->Free();

					return OpcUa_Good;
				}
			}
		}

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadSequenceNumberUnknown);

		m_pMutex->Free();
	}

	return pResponseHeader->ServiceResult;
}

// Subscription Helpers

void COpcServer::SetSubsForNewSession(CSession &Session)
{
	DeleteSubsForSession(Session);

	Session.m_NextSub = NULL;
}

void COpcServer::DeleteSubsForSession(CSession &Session)
{
	for( INDEX n = Session.m_Subs.GetHead(); !Session.m_Subs.Failed(n); Session.m_Subs.GetNext(n) ) {

		CSubscription *pSub = Session.m_Subs[n];

		DeleteSub(Session, pSub);
	}

	Session.m_Subs.Empty();

	Session.m_Index.Empty();

	DeleteNotificationList(Session.m_Avail, true);

	DeleteNotificationList(Session.m_Saved, false);
}

void COpcServer::DeleteSub(CSession &Session, CSubscription *pSub)
{
	for( INDEX n = pSub->m_Items.GetHead(); !pSub->m_Items.Failed(n); pSub->m_Items.GetNext(n) ) {

		CMonitoredItem *pItem = pSub->m_Items[n];

		KillHistory(Session.m_uIndex,
			    pItem->m_ReadValueId.NodeId.NamespaceIndex,
			    pItem->m_ReadValueId.NodeId.Identifier.Numeric,
			    pItem->m_DataType,
			    pItem->m_History
		);

		delete pSub->m_Items[n];
	}

	pSub->m_Items.Empty();

	pSub->m_Index.Empty();

	delete pSub;
}

void COpcServer::InsertNotification(CList <CNotification *> &List, CNotification *pNot)
{
	INDEX n = List.GetTail();

	while( !List.Failed(n) && pNot->m_nPriority > List[n]->m_nPriority ) {

		List.GetPrev(n);
	}

	if( List.Failed(n) ) {

		n = List.GetHead();
	}
	else
		List.GetNext(n);

	List.Insert(n, pNot);
}

void COpcServer::TransferNotificationsForSub(CList <CNotification *> &Dest, CList <CNotification *> &List, CSubscription *pSub, OpcUa_Int32 *pnAvail, OpcUa_UInt32 **ppAvail)
{
	UINT i = pnAvail ? 0 : 1;

	for( UINT p = i; p < 2; p++ ) {

		INDEX m = List.GetHead();

		INDEX t = m;

		INT   c = 0;

		while( !List.Failed(m) ) {

			List.GetNext(t);

			CNotification *pNot = List[m];

			if( pNot->m_uSub == pSub->m_uIndex ) {

				if( p == 0 ) {

					c++;
				}

				if( p == 1 ) {

					if( pnAvail ) {

						OpcUa_NotificationMessage *pMsg = pNot->m_pMsg;

						(*ppAvail)[c++] = pMsg->SequenceNumber;

						Dest.Append(pNot);
					}
					else
						InsertNotification(Dest, pNot);

					List.Remove(m);
				}
			}

			m = t;
		}

		if( p == 0 ) {

			*pnAvail = c;

			*ppAvail = OpcAlloc(*pnAvail, OpcUa_UInt32);
		}
	}
}

void COpcServer::RemoveNotificationsForSub(CList <CNotification *> &List, CSubscription *pSub)
{
	INDEX m = List.GetHead();

	INDEX t = m;

	while( !List.Failed(m) ) {

		List.GetNext(t);

		CNotification *pNot = List[m];

		if( pNot->m_uSub == pSub->m_uIndex ) {

			for( UINT n = 0; n < pNot->m_List.GetCount(); n++ ) {

				CMonitoredItem *pItem = pNot->m_List[n];

				pItem->m_Queued--;
			}

			DeleteNotification(pNot);

			List.Remove(m);
		}

		m = t;
	}
}

void COpcServer::RemoveNotificationsForItem(CList <CNotification *> &List, CMonitoredItem *pItem, bool fAvail)
{
	if( !fAvail || pItem->m_Queued ) {

		INDEX m = List.GetHead();

		INDEX t = m;

		while( !List.Failed(m) ) {

			List.GetNext(t);

			CNotification             *pNot = List[m];

			OpcUa_NotificationMessage *pMsg = pNot->m_pMsg;

			// This is expensive, but it does not happen very
			// often so we can forgive the lack of efficiency.

			for( UINT n = 0; n < pNot->m_List.GetCount(); n++ ) {

				if( pNot->m_List[n] == pItem ) {

					pNot->m_List.Remove(n);

					if( fAvail ) {

						// If we're searching the available list, we
						// need to remove the item from the notification
						// itself to make sure it is not transmitted. For
						// the saved list, we don't bother as it should
						// still be included in a retransimission.

						if( pNot->m_List.IsEmpty() ) {

							// We removed the last item so we can delete
							// the notification message as it is empty.

							DeleteNotification(pNot);

							List.Remove(m);
						}
						else {
							// We need to remove the change notification
							// relating to this data item to avoid confusing
							// the client with items it is not expecting.

							if( pItem->m_ReadValueId.AttributeId == OpcUa_Attributes_EventNotifier ) {

								void                            *e = pMsg->NotificationData->Body.EncodeableObject.Object;

								OpcUa_EventNotificationList     *d = (OpcUa_EventNotificationList *) e;

								INT				&c = d->NoOfEvents;

								OpcUa_EventFieldList		*p = d->Events;

								OpcUa_EventFieldList_Clear(p + n);

								memmove(p + n, p + n + 1, sizeof(*p) * (c - 1 - n));

								c--;
							}

							if( pItem->m_ReadValueId.AttributeId == OpcUa_Attributes_Value ) {

								void                            *e = pMsg->NotificationData->Body.EncodeableObject.Object;

								OpcUa_DataChangeNotification    *d = (OpcUa_DataChangeNotification *) e;

								INT				&c = d->NoOfMonitoredItems;

								OpcUa_MonitoredItemNotification *p = d->MonitoredItems;

								OpcUa_MonitoredItemNotification_Clear(p + n);

								memmove(p + n, p + n + 1, sizeof(*p) * (c - 1 - n));

								c--;
							}
						}

						if( !--pItem->m_Queued ) {

							return;
						}
					}

					n--;
				}
			}

			m = t;
		}
	}
}

void COpcServer::DeleteNotificationList(CList <CNotification *> &List, bool fAvail)
{
	for( INDEX m = List.GetHead(); !List.Failed(m); List.GetNext(m) ) {

		CNotification *pNot = List[m];

		if( fAvail ) {

			for( UINT n = 0; n < pNot->m_List.GetCount(); n++ ) {

				CMonitoredItem *pItem = pNot->m_List[n];

				pItem->m_Queued--;
			}
		}

		DeleteNotification(pNot);
	}

	List.Empty();
}

void COpcServer::DeleteNotification(CNotification *pNot)
{
	OpcUa_NotificationMessage *pMsg = pNot->m_pMsg;

	OpcUa_NotificationMessage_Clear(pMsg);

	OpcUa_Free(pMsg);

	delete pNot;
}

void COpcServer::TickleSubs(CSession &Session, UINT msTime)
{
	for( INDEX n = Session.m_Subs.GetHead(); !Session.m_Subs.Failed(n); Session.m_Subs.GetNext(n) ) {

		CSubscription *pSub = Session.m_Subs[n];

		if( pSub->m_fPublishingEnabled ) {

			if( (pSub->m_Timer += msTime) >= pSub->m_nPublishingInterval ) {

				CArray <CMonitoredItem *> ListData;

				CArray <CMonitoredItem *> ListEvent;

				CArray <UINT>             ListCodes;

				UINT Time = GetTickCount();

				UINT Life = ToTicks(UINT(pSub->m_nLifetimeCount * pSub->m_nPublishingInterval));

				for( INDEX m = pSub->m_Items.GetHead(); !pSub->m_Items.Failed(m); pSub->m_Items.GetNext(m) ) {

					CMonitoredItem *pItem = pSub->m_Items[m];

					if( pItem->m_Mode == OpcUa_MonitoringMode_Reporting ) {

						if( pItem->m_Queued < pItem->m_Parameters.QueueSize ) {

							if( pItem->m_ReadValueId.AttributeId == OpcUa_Attributes_EventNotifier ) {

								UINT ns = pItem->m_ReadValueId.NodeId.NamespaceIndex;

								UINT id = pItem->m_ReadValueId.NodeId.Identifier.Numeric;

								CArray <UINT> Events;

								if( HasEvents(Events, ns, id) ) {

									for( UINT n = 0; n < Events.GetCount(); n++ ) {

										ListEvent.Append(pItem);

										ListCodes.Append(Events[n]);
									}
								}
							}

							if( pItem->m_ReadValueId.AttributeId == OpcUa_Attributes_Value ) {

								bool fChange = HasChanged(Session.m_uIndex,
											  pItem->m_ReadValueId.NodeId.NamespaceIndex,
											  pItem->m_ReadValueId.NodeId.Identifier.Numeric,
											  pItem->m_DataType,
											  pItem->m_History
								);

								if( !fChange ) {

									// Queue even unchanged data values when 75% of the
									// lifetime has been reached. This gives some extra
									// time for things to get transmitted etc.

									if( Time - pItem->m_LastTime >= 3 * Life / 4 ) {

										// But do not retransmit more frequently than 10% of
										// the lifetime and in any case never more than one
										// every 5s, ensuring that we leave time for the ACK.

										if( Time - pItem->m_SendTime >= Min(5000, Life / 5) ) {

											fChange = true;
										}
									}
								}

								if( fChange ) {

									ListData.Append(pItem);

									pItem->m_SendTime = Time;
								}
							}
						}
						else
							AfxTrace("opc: queue overflow\n");
					}
				}

				if( pSub->m_Timer >= 1.5 * pSub->m_nPublishingInterval ) {

					pSub->m_Timer  = 0;
				}
				else
					pSub->m_Timer -= pSub->m_nPublishingInterval;

				QueueNotificationBurst(Session, pSub, ListData, NULL);

				QueueNotificationBurst(Session, pSub, ListEvent, ListCodes.GetPointer());
			}
		}
	}
}

void COpcServer::QueueNotificationBurst(CSession &Session, CSubscription *pSub, CArray <CMonitoredItem *> const &List, PCUINT pCodes)
{
	if( List.GetCount() ) {

		if( pSub->m_nMaxNotificationsPerPublish ) {

			UINT uLimit = pSub->m_nMaxNotificationsPerPublish;

			UINT uCount = List.GetCount();

			if( uCount > uLimit ) {

				UINT uFrom = 0;

				while( uFrom < uCount ) {

					UINT uChunk = Min(uLimit, uCount - uFrom);

					CArray <CMonitoredItem *> Chunk(List.GetPointer() + uFrom, uChunk);

					QueueNotificationMessage(Session, pSub, Chunk, pCodes ? pCodes + uFrom : NULL);

					uFrom += uChunk;
				}

				Session.m_pEvent->Set();

				pSub->m_uKeepAlive = 0;

				return;
			}
		}

		QueueNotificationMessage(Session, pSub, List, pCodes);

		Session.m_pEvent->Set();

		pSub->m_uKeepAlive = 0;
	}
}

void COpcServer::QueueNotificationMessage(CSession &Session, CSubscription *pSub, CArray <CMonitoredItem *> const &List, PCUINT pCodes)
{
	SubTrace("opc: queuenotification\n");

	SubTrace("     %u.%u\n", pSub->m_uIndex, pSub->m_uSeqNum);

	CNotification             *pNot = New CNotification;

	OpcUa_NotificationMessage *pMsg = OpcAlloc(1, OpcUa_NotificationMessage);

	OpcUa_NotificationMessage_Initialize(pMsg);

	pNot->m_pMsg		   = pMsg;

	pNot->m_uSub		   = pSub->m_uIndex;

	pNot->m_nPriority          = pSub->m_nPriority;

	pMsg->PublishTime          = OpcUa_DateTime_UtcNow();

	pMsg->SequenceNumber       = pSub->m_uSeqNum++;

	pMsg->NoOfNotificationData = 1;

	pMsg->NotificationData     = OpcAlloc(pMsg->NoOfNotificationData, OpcUa_ExtensionObject);

	SubTrace("     slot %u\n", Session.m_Avail.GetCount());

	SubTrace("     time %s\n", PCTXT(FormatTime(pMsg->PublishTime)));

	if( !pSub->m_uSeqNum ) {

		pSub->m_uSeqNum = 1;
	}

	for( INT m = 0; m < pMsg->NoOfNotificationData; m++ ) {

		OpcUa_ExtensionObject *pExt = pMsg->NotificationData + m;

		OpcUa_ExtensionObject_Initialize(pExt);

		pExt->Encoding			       = OpcUa_ExtensionObjectEncoding_EncodeableObject;

		pExt->TypeId.ServerIndex               = 0;

		pExt->TypeId.NodeId.NamespaceIndex     = 0;

		pExt->TypeId.NodeId.IdentifierType     = OpcUa_IdentifierType_Numeric;

		OpcUa_String_AttachCopy(&pExt->TypeId.NamespaceUri, "");

		if( pCodes ) {

			pExt->TypeId.NodeId.Identifier.Numeric  = OpcUaId_EventNotificationList_Encoding_DefaultBinary;

			OpcUa_EventNotificationList *pEventList = OpcAlloc(1, OpcUa_EventNotificationList);

			////////

			pExt->BodySize                     = -1;

			pExt->Body.EncodeableObject.Type   = &OpcUa_EventNotificationList_EncodeableType;

			pExt->Body.EncodeableObject.Object = pEventList;

			OpcUa_EventNotificationList_Initialize(pEventList);

			////////

			pEventList->NoOfEvents = List.GetCount();

			pEventList->Events     = OpcAlloc(pEventList->NoOfEvents, OpcUa_EventFieldList);

			////////

			for( INT i = 0; i < pEventList->NoOfEvents; i++ ) {

				CMonitoredItem       *pItem  = List[i];

				COpcNodeId const     &NodeId = (COpcNodeId &) pItem->m_ReadValueId.NodeId;

				OpcUa_EventFieldList *pList = pEventList->Events + i;

				OpcUa_EventFieldList_Initialize(pList);

				pList->ClientHandle = pItem->m_Parameters.ClientHandle;

				if( pCodes[i] ) {

					SubTrace("    %2u) %s.%u (Event %u)\n",
						 i,
						 PCTXT(NodeId.GetAsText()),
						 pItem->m_ReadValueId.AttributeId,
						 pCodes[i]
					);

					pList->NoOfEventFields = 1;

					pList->EventFields     = OpcAlloc(pList->NoOfEventFields, OpcUa_Variant);

					pList->EventFields[0].Value.NodeId = OpcAlloc(1, OpcUa_NodeId);

					pList->EventFields[0].Value.NodeId->NamespaceIndex     = 0;

					pList->EventFields[0].Value.NodeId->IdentifierType     = OpcUa_IdType_Numeric;

					pList->EventFields[0].Value.NodeId->Identifier.Numeric = pCodes[i];

					FillArrayType(pList->EventFields + 0, OpcUaId_NodeId, OpcUa_VariantArrayType_Scalar, 0);
				}
				else {
					SubTrace("    %2u) %s.%u (No Events)\n",
						 i,
						 PCTXT(NodeId.GetAsText()),
						 pItem->m_ReadValueId.AttributeId
					);

					pList->NoOfEventFields = 0;

					pList->EventFields     = NULL;
				}

				pItem->m_Queued++;

				pNot->m_List.Append(pItem);
			}
		}
		else {
			pExt->TypeId.NodeId.Identifier.Numeric    = OpcUaId_DataChangeNotification_Encoding_DefaultBinary;

			OpcUa_DataChangeNotification *pDataChange = OpcAlloc(1, OpcUa_DataChangeNotification);

			////////

			pExt->BodySize                     = -1;

			pExt->Body.EncodeableObject.Type   = &OpcUa_DataChangeNotification_EncodeableType;

			pExt->Body.EncodeableObject.Object = pDataChange;

			OpcUa_DataChangeNotification_Initialize(pDataChange);

			////////

			pDataChange->NoOfDiagnosticInfos = 0;

			pDataChange->DiagnosticInfos     = NULL;

			pDataChange->NoOfMonitoredItems  = List.GetCount();

			pDataChange->MonitoredItems      = OpcAlloc(pDataChange->NoOfMonitoredItems, OpcUa_MonitoredItemNotification);

			////////

			for( INT i = 0; i < pDataChange->NoOfMonitoredItems; i++ ) {

				CMonitoredItem                  *pItem = List[i];

				OpcUa_MonitoredItemNotification *pData = pDataChange->MonitoredItems + i;

				OpcUa_MonitoredItemNotification_Initialize(pData);

				pData->ClientHandle = pItem->m_Parameters.ClientHandle;

				ReadNode(Session, &pData->Value, &pItem->m_ReadValueId, pItem->m_eTimestampsToReturn, i);

				if( pData->Value.SourceTimestamp.t ) {

					UINT k = UINT(pSub->m_nPublishingInterval * 10000);

					pData->Value.SourceTimestamp.t /= k;

					pData->Value.SourceTimestamp.t *= k;
				}

				pItem->m_Queued++;

				pNot->m_List.Append(pItem);
			}
		}
	}

	InsertNotification(Session.m_Avail, pNot);
}

void COpcServer::GetSavedNotifications(CSession &Session, UINT uSub, OpcUa_Int32 *pnAvail, OpcUa_UInt32 **ppAvail)
{
	*pnAvail = Session.m_Saved.GetCount();

	*ppAvail = OpcAlloc(*pnAvail, OpcUa_UInt32);

	INT c    = 0;

	for( INDEX n = Session.m_Saved.GetHead(); !Session.m_Saved.Failed(n); Session.m_Saved.GetNext(n) ) {

		CNotification *pNot = Session.m_Saved[n];

		if( pNot->m_uSub == uSub ) {

			OpcUa_NotificationMessage *pMsg = pNot->m_pMsg;

			(*ppAvail)[c] = pMsg->SequenceNumber;

			if( !c ) {

				SubTrace("     avail ");
			}

			SubTrace("%u.%u ", uSub, (*ppAvail)[c]);

			c++;
		}
	}

	if( c ) {

		SubTrace("\n");
	}

	*pnAvail = c;
}

void COpcServer::ProcessAcks(CSession &Session, OpcUa_Int32 *pnResult, OpcUa_StatusCode **ppResult, OpcUa_Int32 nAck, OpcUa_SubscriptionAcknowledgement const *pAck)
{
	*pnResult = nAck;

	*ppResult = OpcAlloc(nAck, OpcUa_StatusCode);

	for( INT m = 0; m < nAck; m++ ) {

		OpcUa_SubscriptionAcknowledgement const &Ack = pAck[m];

		SubTrace("     ack recieved for %u.%u ", Ack.SubscriptionId, Ack.SequenceNumber);

		if( Session.m_Index.Failed(Session.m_Index.FindName(Ack.SubscriptionId)) ) {

			(*ppResult)[m] = OpcUa_BadSubscriptionIdInvalid;

			SubTrace("(no subscription)\n");
		}
		else {
			// We could use an index for this, but we almost always
			// hit the first item as that is how the acks arrive.

			bool fHit = false;

			UINT Time = GetTickCount();

			for( INDEX n = Session.m_Saved.GetHead(); !Session.m_Saved.Failed(n); Session.m_Saved.GetNext(n) ) {

				CNotification *pNot = Session.m_Saved[n];

				if( pNot->m_uSub == Ack.SubscriptionId ) {

					if( pNot->m_pMsg->SequenceNumber == Ack.SequenceNumber ) {

						for( UINT i = 0; i < pNot->m_List.GetCount(); i++ ) {

							CMonitoredItem *pItem = pNot->m_List[i];

							if( pItem ) {

								pItem->m_LastTime = Time;
							}
						}

						DeleteNotification(pNot);

						Session.m_Saved.Remove(n);

						(*ppResult)[m] = OpcUa_Good;

						SubTrace("(matched)\n");

						fHit = true;

						break;
					}
				}
			}

			if( !fHit ) {

				(*ppResult)[m] = OpcUa_BadSequenceNumberUnknown;

				SubTrace("(no sequence)\n");
			}
		}
	}
}

void COpcServer::CoalesceNotifications(CSession &Session, CList <CNotification *> &List)
{
	if( List.GetCount() > 1 ) {

		OpcUa_DataChangeNotification *pNewDataChange = NULL;

		OpcUa_EventNotificationList  *pNewEventList  = NULL;

		INDEX             Head  = List.GetHead();

		CNotification    *pNot0 = List[Head];

		UINT              uType = pNot0->m_pMsg->NotificationData->TypeId.NodeId.Identifier.Numeric;

		int               nMax  = FindMaxNotificationsForSub(Session, pNot0->m_uSub);

		for( int p = 0; p < 2; p++ ) {

			INDEX Scan  = Head;

			int   nHits = 0;

			int   nFind = 0;

			while( !List.Failed(Scan) ) {

				CNotification *pNot1 = List[Scan];

				if( pNot1->m_pMsg->NotificationData->TypeId.NodeId.Identifier.Numeric == uType ) {

					if( pNot1->m_uSub == pNot0->m_uSub ) {

						void *pObject = pNot1->m_pMsg->NotificationData->Body.EncodeableObject.Object;

						if( uType == OpcUaId_DataChangeNotification_Encoding_DefaultBinary ) {

							OpcUa_DataChangeNotification *pOldDataChange = (OpcUa_DataChangeNotification *) pObject;

							if( p == 0 ) {

								if( nMax && nFind + pOldDataChange->NoOfMonitoredItems > nMax ) {

									break;
								}

								nFind += pOldDataChange->NoOfMonitoredItems;
							}

							if( p == 1 ) {

								for( int i = 0; i < pOldDataChange->NoOfMonitoredItems; i++ ) {

									OpcUa_MonitoredItemNotification       *pDest = pNewDataChange->MonitoredItems + nFind++;

									OpcUa_MonitoredItemNotification const *pFrom = pOldDataChange->MonitoredItems + i;

									CopyMonitoredItemNotification(pDest, pFrom);
								}
							}

							nHits += 1;
						}

						if( uType == OpcUaId_EventNotificationList_Encoding_DefaultBinary ) {

							OpcUa_EventNotificationList *pOldEventList = (OpcUa_EventNotificationList *) pObject;

							if( p == 0 ) {

								if( nMax && nFind + pOldEventList->NoOfEvents > nMax ) {

									break;
								}

								nFind += pOldEventList->NoOfEvents;
							}

							if( p == 1 ) {

								for( int i = 0; i < pOldEventList->NoOfEvents; i++ ) {

									OpcUa_EventFieldList       *pDest = pNewEventList->Events + nFind++;

									OpcUa_EventFieldList const *pFrom = pNewEventList->Events + i;

									CopyEventFieldList(pDest, pFrom);
								}
							}

							nHits += 1;
						}

						List.GetNext(Scan);

						continue;
					}
				}

				break;
			}

			if( p == 0 ) {

				if( nHits > 1 ) {

					if( uType == OpcUaId_DataChangeNotification_Encoding_DefaultBinary ) {

						AfxTrace("opc: found %u data changes to coalesce with total of %u items\n", nHits, nFind);

						pNewDataChange = OpcAlloc(1, OpcUa_DataChangeNotification);

						OpcUa_DataChangeNotification_Initialize(pNewDataChange);

						pNewDataChange->NoOfDiagnosticInfos = 0;

						pNewDataChange->DiagnosticInfos     = NULL;

						pNewDataChange->NoOfMonitoredItems  = nFind;

						pNewDataChange->MonitoredItems      = OpcAlloc(pNewDataChange->NoOfMonitoredItems, OpcUa_MonitoredItemNotification);
					}

					if( uType == OpcUaId_EventNotificationList_Encoding_DefaultBinary ) {

						AfxTrace("opc: found %u event lists to coalesce with total of %u events\n", nHits, nFind);

						pNewEventList = OpcAlloc(1, OpcUa_EventNotificationList);

						OpcUa_EventNotificationList_Initialize(pNewEventList);

						pNewEventList->NoOfEvents = nFind;

						pNewEventList->Events     = OpcAlloc(pNewEventList->NoOfEvents, OpcUa_EventFieldList);
					}

					continue;
				}

				break;
			}

			if( p == 1 ) {

				CNotification             *pNot = New CNotification;

				OpcUa_NotificationMessage *pMsg = OpcAlloc(1, OpcUa_NotificationMessage);

				OpcUa_NotificationMessage_Initialize(pMsg);

				pNot->m_pMsg		    = pMsg;

				pNot->m_uSub		    = pNot0->m_uSub;

				pNot->m_nPriority           = pNot0->m_nPriority;

				pMsg->PublishTime           = OpcUa_DateTime_UtcNow();

				pMsg->SequenceNumber        = pNot0->m_pMsg->SequenceNumber;

				pMsg->NoOfNotificationData  = 1;

				pMsg->NotificationData      = OpcAlloc(pMsg->NoOfNotificationData, OpcUa_ExtensionObject);

				OpcUa_ExtensionObject *pExt = pMsg->NotificationData;

				OpcUa_ExtensionObject_Initialize(pExt);

				pExt->Encoding			       = OpcUa_ExtensionObjectEncoding_EncodeableObject;

				pExt->TypeId.ServerIndex               = 0;

				pExt->TypeId.NodeId.NamespaceIndex     = 0;

				pExt->TypeId.NodeId.IdentifierType     = OpcUa_IdentifierType_Numeric;

				OpcUa_String_AttachCopy(&pExt->TypeId.NamespaceUri, "");

				pExt->TypeId.NodeId.Identifier.Numeric = uType;

				if( uType == OpcUaId_DataChangeNotification_Encoding_DefaultBinary ) {

					pExt->BodySize                     = -1;

					pExt->Body.EncodeableObject.Type   = &OpcUa_DataChangeNotification_EncodeableType;

					pExt->Body.EncodeableObject.Object = pNewDataChange;
				}

				if( uType == OpcUaId_EventNotificationList_Encoding_DefaultBinary ) {

					pExt->BodySize                     = -1;

					pExt->Body.EncodeableObject.Type   = &OpcUa_EventNotificationList_EncodeableType;

					pExt->Body.EncodeableObject.Object = pNewEventList;
				}

				for( INDEX Scan = List.GetHead(); nHits; ) {

					CNotification *pSrc = List[Scan];

					if( pSrc->m_pMsg->NotificationData->TypeId.NodeId.Identifier.Numeric == uType ) {

						if( pSrc->m_uSub == pNot->m_uSub ) {

							pNot->m_List.Append(pSrc->m_List);

							DeleteNotification(pSrc);

							INDEX Prev = Scan;

							List.GetNext(Scan);

							List.Remove(Prev);

							nHits--;

							continue;
						}
					}

					List.GetNext(Scan);
				}

				List.Insert(List.GetHead(), pNot);
			}
		}
	}
}

int COpcServer::FindMaxNotificationsForSub(CSession &Session, UINT uSub)
{
	INDEX i = Session.m_Index.FindName(uSub);

	if( !Session.m_Subs.Failed(i) ) {

		INDEX          p    = Session.m_Index[i];

		CSubscription *pSub = Session.m_Subs[p];

		return pSub->m_nMaxNotificationsPerPublish;
	}

	return 0;
}

// End of File
