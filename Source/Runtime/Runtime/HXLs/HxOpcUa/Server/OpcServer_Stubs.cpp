
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Stubs

OpcUa_StatusCode COpcServer::TimerCallbackStub(OpcUa_Void   * pvCallbackData,
					       OpcUa_Timer    hTimer,
					       OpcUa_UInt32   msecElapsed
)
{
	CSession &Session = *((CSession *) pvCallbackData);

	Session.m_pServer->TickleSession(Session, msecElapsed);

	return OpcUa_Good;
}

OpcUa_StatusCode COpcServer::EndpointCallbackStub(OpcUa_Endpoint          hEndpoint,
						  OpcUa_Void *            pCallbackData,
						  OpcUa_Endpoint_Event    eEvent,
						  OpcUa_StatusCode        uStatus,
						  OpcUa_UInt32            uSecureChannelId,
						  OpcUa_ByteString *      pbsClientCertificate,
						  OpcUa_String *          pSecurityPolicy,
						  OpcUa_UInt16            uSecurityMode
)
{
	((COpcServer *) pCallbackData)->EndpointCallback(hEndpoint,
							 eEvent,
							 uStatus,
							 uSecureChannelId,
							 pbsClientCertificate,
							 pSecurityPolicy,
							 uSecurityMode
	);

	return OpcUa_Good;
}

OpcUa_StatusCode COpcServer::GetEndpointsStub(OpcUa_Endpoint               hEndpoint,
					      OpcUa_Handle                 hContext,
					      OpcUa_RequestHeader *        pRequestHeader,
					      OpcUa_String *               pEndpointUrl,
					      OpcUa_Int32                  nNoOfLocaleIds,
					      OpcUa_String *               pLocaleIds,
					      OpcUa_Int32                  nNoOfProfileUris,
					      OpcUa_String *               pProfileUris,
					      OpcUa_ResponseHeader *       pResponseHeader,
					      OpcUa_Int32 *                pNoOfEndpoints,
					      OpcUa_EndpointDescription ** ppEndpoints
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->GetEndpoints(hEndpoint,
			       hContext,
			       pRequestHeader,
			       pEndpointUrl,
			       nNoOfLocaleIds,
			       pLocaleIds,
			       nNoOfProfileUris,
			       pProfileUris,
			       pResponseHeader,
			       pNoOfEndpoints,
			       ppEndpoints
	);
}

OpcUa_StatusCode COpcServer::CreateSessionStub(OpcUa_Endpoint                       hEndpoint,
					       OpcUa_Handle                         hContext,
					       const OpcUa_RequestHeader *          pRequestHeader,
					       const OpcUa_ApplicationDescription * pClientDescription,
					       const OpcUa_String *                 pServerUri,
					       const OpcUa_String *                 pEndpointUrl,
					       const OpcUa_String *                 pSessionName,
					       const OpcUa_ByteString *             pClientNonce,
					       const OpcUa_ByteString *             pClientCertificate,
					       OpcUa_Double                         nRequestedSessionTimeout,
					       OpcUa_UInt32                         nMaxResponseMessageSize,
					       OpcUa_ResponseHeader *               pResponseHeader,
					       OpcUa_NodeId *                       pSessionId,
					       OpcUa_NodeId *                       pAuthenticationToken,
					       OpcUa_Double *                       pRevisedSessionTimeout,
					       OpcUa_ByteString *                   pServerNonce,
					       OpcUa_ByteString *                   pServerCertificate,
					       OpcUa_Int32 *                        pNoOfServerEndpoints,
					       OpcUa_EndpointDescription **         pServerEndpoints,
					       OpcUa_Int32 *                        pNoOfServerSoftwareCertificates,
					       OpcUa_SignedSoftwareCertificate **   pServerSoftwareCertificates,
					       OpcUa_SignatureData *                pServerSignature,
					       OpcUa_UInt32 *                       pMaxRequestMessageSize
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->CreateSession(hEndpoint,
				hContext,
				pRequestHeader,
				pClientDescription,
				pServerUri,
				pEndpointUrl,
				pSessionName,
				pClientNonce,
				pClientCertificate,
				nRequestedSessionTimeout,
				nMaxResponseMessageSize,
				pResponseHeader,
				pSessionId,
				pAuthenticationToken,
				pRevisedSessionTimeout,
				pServerNonce,
				pServerCertificate,
				pNoOfServerEndpoints,
				pServerEndpoints,
				pNoOfServerSoftwareCertificates,
				pServerSoftwareCertificates,
				pServerSignature,
				pMaxRequestMessageSize
	);
}

OpcUa_StatusCode COpcServer::ActivateSessionStub(OpcUa_Endpoint                          hEndpoint,
						 OpcUa_Handle                            hContext,
						 const OpcUa_RequestHeader *             pRequestHeader,
						 const OpcUa_SignatureData *             pClientSignature,
						 OpcUa_Int32                             nNoOfClientSoftwareCertificates,
						 const OpcUa_SignedSoftwareCertificate * pClientSoftwareCertificates,
						 OpcUa_Int32                             nNoOfLocaleIds,
						 const OpcUa_String *                    pLocaleIds,
						 const OpcUa_ExtensionObject *           pUserIdentityToken,
						 const OpcUa_SignatureData *             pUserTokenSignature,
						 OpcUa_ResponseHeader *                  pResponseHeader,
						 OpcUa_ByteString *                      pServerNonce,
						 OpcUa_Int32 *                           pNoOfResults,
						 OpcUa_StatusCode **                     pResults,
						 OpcUa_Int32 *                           pNoOfDiagnosticInfos,
						 OpcUa_DiagnosticInfo **                 pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->ActivateSession(hEndpoint,
				  hContext,
				  pRequestHeader,
				  pClientSignature,
				  nNoOfClientSoftwareCertificates,
				  pClientSoftwareCertificates,
				  nNoOfLocaleIds,
				  pLocaleIds,
				  pUserIdentityToken,
				  pUserTokenSignature,
				  pResponseHeader,
				  pServerNonce,
				  pNoOfResults,
				  pResults,
				  pNoOfDiagnosticInfos,
				  pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::CloseSessionStub(OpcUa_Endpoint              hEndpoint,
					      OpcUa_Handle                hContext,
					      const OpcUa_RequestHeader * pRequestHeader,
					      OpcUa_Boolean               bDeleteSubscriptions,
					      OpcUa_ResponseHeader *      pResponseHeader
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->CloseSession(hEndpoint,
			       hContext,
			       pRequestHeader,
			       bDeleteSubscriptions,
			       pResponseHeader
	);
}

OpcUa_StatusCode COpcServer::BrowseStub(OpcUa_Endpoint                 hEndpoint,
					OpcUa_Handle                   hContext,
					const OpcUa_RequestHeader *    pRequestHeader,
					const OpcUa_ViewDescription *  pView,
					OpcUa_UInt32                   nRequestedMaxReferencesPerNode,
					OpcUa_Int32                    nNoOfNodesToBrowse,
					OpcUa_BrowseDescription *	pNodesToBrowse,
					OpcUa_ResponseHeader *         pResponseHeader,
					OpcUa_Int32 *                  pNoOfResults,
					OpcUa_BrowseResult **          pResults,
					OpcUa_Int32 *                  pNoOfDiagnosticInfos,
					OpcUa_DiagnosticInfo **        pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->Browse(hEndpoint,
			 hContext,
			 pRequestHeader,
			 pView,
			 nRequestedMaxReferencesPerNode,
			 nNoOfNodesToBrowse,
			 pNodesToBrowse,
			 pResponseHeader,
			 pNoOfResults,
			 pResults,
			 pNoOfDiagnosticInfos,
			 pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::BrowseNextStub(OpcUa_Endpoint              hEndpoint,
					    OpcUa_Handle                hContext,
					    const OpcUa_RequestHeader * pRequestHeader,
					    OpcUa_Boolean               bReleaseContinuationPoints,
					    OpcUa_Int32                 nNoOfContinuationPoints,
					    const OpcUa_ByteString *    pContinuationPoints,
					    OpcUa_ResponseHeader *      pResponseHeader,
					    OpcUa_Int32 *               pNoOfResults,
					    OpcUa_BrowseResult **       pResults,
					    OpcUa_Int32 *               pNoOfDiagnosticInfos,
					    OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->BrowseNext(hEndpoint,
			     hContext,
			     pRequestHeader,
			     bReleaseContinuationPoints,
			     nNoOfContinuationPoints,
			     pContinuationPoints,
			     pResponseHeader,
			     pNoOfResults,
			     pResults,
			     pNoOfDiagnosticInfos,
			     pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::TranslateBrowsePathsToNodeIdsStub(OpcUa_Endpoint              hEndpoint,
							       OpcUa_Handle                hContext,
							       const OpcUa_RequestHeader * pRequestHeader,
							       OpcUa_Int32                 nNoOfBrowsePaths,
							       const OpcUa_BrowsePath *    pBrowsePaths,
							       OpcUa_ResponseHeader *      pResponseHeader,
							       OpcUa_Int32 *               pNoOfResults,
							       OpcUa_BrowsePathResult **   pResults,
							       OpcUa_Int32 *               pNoOfDiagnosticInfos,
							       OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->TranslateBrowsePathsToNodeIds(hEndpoint,
						hContext,
						pRequestHeader,
						nNoOfBrowsePaths,
						pBrowsePaths,
						pResponseHeader,
						pNoOfResults,
						pResults,
						pNoOfDiagnosticInfos,
						pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::ReadStub(OpcUa_Endpoint              hEndpoint,
				      OpcUa_Handle                hContext,
				      const OpcUa_RequestHeader * pRequestHeader,
				      OpcUa_Double                nMaxAge,
				      OpcUa_TimestampsToReturn    eTimestampsToReturn,
				      OpcUa_Int32                 nNoOfNodesToRead,
				      const OpcUa_ReadValueId *   pNodesToRead,
				      OpcUa_ResponseHeader *      pResponseHeader,
				      OpcUa_Int32 *               pNoOfResults,
				      OpcUa_DataValue **          pResults,
				      OpcUa_Int32 *               pNoOfDiagnosticInfos,
				      OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->Read(hEndpoint,
		       hContext,
		       pRequestHeader,
		       nMaxAge,
		       eTimestampsToReturn,
		       nNoOfNodesToRead,
		       pNodesToRead,
		       pResponseHeader,
		       pNoOfResults,
		       pResults,
		       pNoOfDiagnosticInfos,
		       pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::WriteStub(OpcUa_Endpoint              hEndpoint,
				       OpcUa_Handle                hContext,
				       const OpcUa_RequestHeader * pRequestHeader,
				       OpcUa_Int32                 nNoOfNodesToWrite,
				       const OpcUa_WriteValue *    pNodesToWrite,
				       OpcUa_ResponseHeader *      pResponseHeader,
				       OpcUa_Int32 *               pNoOfResults,
				       OpcUa_StatusCode **         pResults,
				       OpcUa_Int32 *               pNoOfDiagnosticInfos,
				       OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->Write(hEndpoint,
			hContext,
			pRequestHeader,
			nNoOfNodesToWrite,
			pNodesToWrite,
			pResponseHeader,
			pNoOfResults,
			pResults,
			pNoOfDiagnosticInfos,
			pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::FindServersStub(OpcUa_Endpoint                  hEndpoint,
					     OpcUa_Handle                    hContext,
					     const OpcUa_RequestHeader *     pRequestHeader,
					     const OpcUa_String *            pEndpointUrl,
					     OpcUa_Int32                     nNoOfLocaleIds,
					     const OpcUa_String *            pLocaleIds,
					     OpcUa_Int32                     nNoOfServerUris,
					     const OpcUa_String *            pServerUris,
					     OpcUa_ResponseHeader *          pResponseHeader,
					     OpcUa_Int32 *                   pNoOfServers,
					     OpcUa_ApplicationDescription ** pServers
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->FindServers(hEndpoint,
			      hContext,
			      pRequestHeader,
			      pEndpointUrl,
			      nNoOfLocaleIds,
			      pLocaleIds,
			      nNoOfServerUris,
			      pServerUris,
			      pResponseHeader,
			      pNoOfServers,
			      pServers
	);
}

OpcUa_StatusCode COpcServer::CreateSubscriptionStub(OpcUa_Endpoint              hEndpoint,
						    OpcUa_Handle                hContext,
						    const OpcUa_RequestHeader * pRequestHeader,
						    OpcUa_Double                nRequestedPublishingInterval,
						    OpcUa_UInt32                nRequestedLifetimeCount,
						    OpcUa_UInt32                nRequestedMaxKeepAliveCount,
						    OpcUa_UInt32                nMaxNotificationsPerPublish,
						    OpcUa_Boolean               bPublishingEnabled,
						    OpcUa_Byte                  nPriority,
						    OpcUa_ResponseHeader *      pResponseHeader,
						    OpcUa_UInt32 *              pSubscriptionId,
						    OpcUa_Double *              pRevisedPublishingInterval,
						    OpcUa_UInt32 *              pRevisedLifetimeCount,
						    OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->CreateSubscription(hEndpoint,
				     hContext,
				     pRequestHeader,
				     nRequestedPublishingInterval,
				     nRequestedLifetimeCount,
				     nRequestedMaxKeepAliveCount,
				     nMaxNotificationsPerPublish,
				     bPublishingEnabled,
				     nPriority,
				     pResponseHeader,
				     pSubscriptionId,
				     pRevisedPublishingInterval,
				     pRevisedLifetimeCount,
				     pRevisedMaxKeepAliveCount
	);
}

OpcUa_StatusCode COpcServer::ModifySubscriptionStub(OpcUa_Endpoint              hEndpoint,
						    OpcUa_Handle                hContext,
						    const OpcUa_RequestHeader * pRequestHeader,
						    OpcUa_UInt32                nSubscriptionId,
						    OpcUa_Double                nRequestedPublishingInterval,
						    OpcUa_UInt32                nRequestedLifetimeCount,
						    OpcUa_UInt32                nRequestedMaxKeepAliveCount,
						    OpcUa_UInt32                nMaxNotificationsPerPublish,
						    OpcUa_Byte                  nPriority,
						    OpcUa_ResponseHeader *      pResponseHeader,
						    OpcUa_Double *              pRevisedPublishingInterval,
						    OpcUa_UInt32 *              pRevisedLifetimeCount,
						    OpcUa_UInt32 *              pRevisedMaxKeepAliveCount
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->ModifySubscription(hEndpoint,
				     hContext,
				     pRequestHeader,
				     nSubscriptionId,
				     nRequestedPublishingInterval,
				     nRequestedLifetimeCount,
				     nRequestedMaxKeepAliveCount,
				     nMaxNotificationsPerPublish,
				     nPriority,
				     pResponseHeader,
				     pRevisedPublishingInterval,
				     pRevisedLifetimeCount,
				     pRevisedMaxKeepAliveCount
	);
}

OpcUa_StatusCode COpcServer::TransferSubscriptionsStub(OpcUa_Endpoint              hEndpoint,
						       OpcUa_Handle                hContext,
						       const OpcUa_RequestHeader * pRequestHeader,
						       OpcUa_Int32                 nNoOfSubscriptionIds,
						       const OpcUa_UInt32 *        pSubscriptionIds,
						       OpcUa_Boolean               bSendInitialValues,
						       OpcUa_ResponseHeader *      pResponseHeader,
						       OpcUa_Int32 *               pNoOfResults,
						       OpcUa_TransferResult **     pResults,
						       OpcUa_Int32 *               pNoOfDiagnosticInfos,
						       OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->TransferSubscriptions(hEndpoint,
					hContext,
					pRequestHeader,
					nNoOfSubscriptionIds,
					pSubscriptionIds,
					bSendInitialValues,
					pResponseHeader,
					pNoOfResults,
					pResults,
					pNoOfDiagnosticInfos,
					pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::SetPublishingModeStub(OpcUa_Endpoint              hEndpoint,
						   OpcUa_Handle                hContext,
						   const OpcUa_RequestHeader * pRequestHeader,
						   OpcUa_Boolean               bPublishingEnabled,
						   OpcUa_Int32                 nNoOfSubscriptionIds,
						   const OpcUa_UInt32 *        pSubscriptionIds,
						   OpcUa_ResponseHeader *      pResponseHeader,
						   OpcUa_Int32 *               pNoOfResults,
						   OpcUa_StatusCode **         pResults,
						   OpcUa_Int32 *               pNoOfDiagnosticInfos,
						   OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->SetPublishingMode(hEndpoint,
				    hContext,
				    pRequestHeader,
				    bPublishingEnabled,
				    nNoOfSubscriptionIds,
				    pSubscriptionIds,
				    pResponseHeader,
				    pNoOfResults,
				    pResults,
				    pNoOfDiagnosticInfos,
				    pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::DeleteSubscriptionsStub(OpcUa_Endpoint              hEndpoint,
						     OpcUa_Handle                hContext,
						     const OpcUa_RequestHeader * pRequestHeader,
						     OpcUa_Int32                 nNoOfSubscriptionIds,
						     const OpcUa_UInt32 *        pSubscriptionIds,
						     OpcUa_ResponseHeader *      pResponseHeader,
						     OpcUa_Int32 *               pNoOfResults,
						     OpcUa_StatusCode **         pResults,
						     OpcUa_Int32 *               pNoOfDiagnosticInfos,
						     OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->DeleteSubscriptions(hEndpoint,
				      hContext,
				      pRequestHeader,
				      nNoOfSubscriptionIds,
				      pSubscriptionIds,
				      pResponseHeader,
				      pNoOfResults,
				      pResults,
				      pNoOfDiagnosticInfos,
				      pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::CreateMonitoredItemsStub(OpcUa_Endpoint                           hEndpoint,
						      OpcUa_Handle                             hContext,
						      const OpcUa_RequestHeader *              pRequestHeader,
						      OpcUa_UInt32                             nSubscriptionId,
						      OpcUa_TimestampsToReturn                 eTimestampsToReturn,
						      OpcUa_Int32                              nNoOfItemsToCreate,
						      const OpcUa_MonitoredItemCreateRequest * pItemsToCreate,
						      OpcUa_ResponseHeader *                   pResponseHeader,
						      OpcUa_Int32 *                            pNoOfResults,
						      OpcUa_MonitoredItemCreateResult **       pResults,
						      OpcUa_Int32 *                            pNoOfDiagnosticInfos,
						      OpcUa_DiagnosticInfo **                  pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->CreateMonitoredItems(hEndpoint,
				       hContext,
				       pRequestHeader,
				       nSubscriptionId,
				       eTimestampsToReturn,
				       nNoOfItemsToCreate,
				       pItemsToCreate,
				       pResponseHeader,
				       pNoOfResults,
				       pResults,
				       pNoOfDiagnosticInfos,
				       pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::ModifyMonitoredItemsStub(OpcUa_Endpoint                           hEndpoint,
						      OpcUa_Handle                             hContext,
						      const OpcUa_RequestHeader *              pRequestHeader,
						      OpcUa_UInt32                             nSubscriptionId,
						      OpcUa_TimestampsToReturn                 eTimestampsToReturn,
						      OpcUa_Int32                              nNoOfItemsToModify,
						      const OpcUa_MonitoredItemModifyRequest * pItemsToModify,
						      OpcUa_ResponseHeader *                   pResponseHeader,
						      OpcUa_Int32 *                            pNoOfResults,
						      OpcUa_MonitoredItemModifyResult **       pResults,
						      OpcUa_Int32 *                            pNoOfDiagnosticInfos,
						      OpcUa_DiagnosticInfo **                  pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->ModifyMonitoredItems(hEndpoint,
				       hContext,
				       pRequestHeader,
				       nSubscriptionId,
				       eTimestampsToReturn,
				       nNoOfItemsToModify,
				       pItemsToModify,
				       pResponseHeader,
				       pNoOfResults,
				       pResults,
				       pNoOfDiagnosticInfos,
				       pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::SetMonitoringModeStub(OpcUa_Endpoint              hEndpoint,
						   OpcUa_Handle                hContext,
						   const OpcUa_RequestHeader * pRequestHeader,
						   OpcUa_UInt32                nSubscriptionId,
						   OpcUa_MonitoringMode        eMonitoringMode,
						   OpcUa_Int32                 nNoOfMonitoredItemIds,
						   const OpcUa_UInt32 *        pMonitoredItemIds,
						   OpcUa_ResponseHeader *      pResponseHeader,
						   OpcUa_Int32 *               pNoOfResults,
						   OpcUa_StatusCode **         pResults,
						   OpcUa_Int32 *               pNoOfDiagnosticInfos,
						   OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->SetMonitoringMode(hEndpoint,
				    hContext,
				    pRequestHeader,
				    nSubscriptionId,
				    eMonitoringMode,
				    nNoOfMonitoredItemIds,
				    pMonitoredItemIds,
				    pResponseHeader,
				    pNoOfResults,
				    pResults,
				    pNoOfDiagnosticInfos,
				    pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::DeleteMonitoredItemsStub(OpcUa_Endpoint              hEndpoint,
						      OpcUa_Handle                hContext,
						      const OpcUa_RequestHeader * pRequestHeader,
						      OpcUa_UInt32                nSubscriptionId,
						      OpcUa_Int32                 nNoOfMonitoredItemIds,
						      const OpcUa_UInt32 *        pMonitoredItemIds,
						      OpcUa_ResponseHeader *      pResponseHeader,
						      OpcUa_Int32 *               pNoOfResults,
						      OpcUa_StatusCode **         pResults,
						      OpcUa_Int32 *               pNoOfDiagnosticInfos,
						      OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->DeleteMonitoredItems(hEndpoint,
				       hContext,
				       pRequestHeader,
				       nSubscriptionId,
				       nNoOfMonitoredItemIds,
				       pMonitoredItemIds,
				       pResponseHeader,
				       pNoOfResults,
				       pResults,
				       pNoOfDiagnosticInfos,
				       pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::PublishStub(OpcUa_Endpoint                            hEndpoint,
					 OpcUa_Handle                              hContext,
					 const OpcUa_RequestHeader *               pRequestHeader,
					 OpcUa_Int32                               nNoOfSubscriptionAcknowledgements,
					 const OpcUa_SubscriptionAcknowledgement * pSubscriptionAcknowledgements,
					 OpcUa_ResponseHeader *                    pResponseHeader,
					 OpcUa_UInt32 *                            pSubscriptionId,
					 OpcUa_Int32 *                             pNoOfAvailableSequenceNumbers,
					 OpcUa_UInt32 **                           pAvailableSequenceNumbers,
					 OpcUa_Boolean *                           pMoreNotifications,
					 OpcUa_NotificationMessage *               pNotificationMessage,
					 OpcUa_Int32 *                             pNoOfResults,
					 OpcUa_StatusCode **                       pResults,
					 OpcUa_Int32 *                             pNoOfDiagnosticInfos,
					 OpcUa_DiagnosticInfo **                   pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->Publish(hEndpoint,
			  hContext,
			  pRequestHeader,
			  nNoOfSubscriptionAcknowledgements,
			  pSubscriptionAcknowledgements,
			  pResponseHeader,
			  pSubscriptionId,
			  pNoOfAvailableSequenceNumbers,
			  pAvailableSequenceNumbers,
			  pMoreNotifications,
			  pNotificationMessage,
			  pNoOfResults,
			  pResults,
			  pNoOfDiagnosticInfos,
			  pDiagnosticInfos
	);
}


OpcUa_StatusCode COpcServer::RepublishStub(OpcUa_Endpoint              hEndpoint,
					   OpcUa_Handle                hContext,
					   const OpcUa_RequestHeader * pRequestHeader,
					   OpcUa_UInt32                nSubscriptionId,
					   OpcUa_UInt32                nRetransmitSequenceNumber,
					   OpcUa_ResponseHeader *      pResponseHeader,
					   OpcUa_NotificationMessage * pNotificationMessage
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->Republish(hEndpoint,
			    hContext,
			    pRequestHeader,
			    nSubscriptionId,
			    nRetransmitSequenceNumber,
			    pResponseHeader,
			    pNotificationMessage
	);
}

OpcUa_StatusCode COpcServer::HistoryReadStub(OpcUa_Endpoint                   hEndpoint,
					     OpcUa_Handle                     hContext,
					     const OpcUa_RequestHeader *      pRequestHeader,
					     const OpcUa_ExtensionObject *    pHistoryReadDetails,
					     OpcUa_TimestampsToReturn         eTimestampsToReturn,
					     OpcUa_Boolean                    bReleaseContinuationPoints,
					     OpcUa_Int32                      nNoOfNodesToRead,
					     const OpcUa_HistoryReadValueId * pNodesToRead,
					     OpcUa_ResponseHeader *           pResponseHeader,
					     OpcUa_Int32 *                    pNoOfResults,
					     OpcUa_HistoryReadResult **       pResults,
					     OpcUa_Int32 *                    pNoOfDiagnosticInfos,
					     OpcUa_DiagnosticInfo **          pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->HistoryRead(hEndpoint,
			      hContext,
			      pRequestHeader,
			      pHistoryReadDetails,
			      eTimestampsToReturn,
			      bReleaseContinuationPoints,
			      nNoOfNodesToRead,
			      pNodesToRead,
			      pResponseHeader,
			      pNoOfResults,
			      pResults,
			      pNoOfDiagnosticInfos,
			      pDiagnosticInfos
	);
}

OpcUa_StatusCode COpcServer::CallStub(OpcUa_Endpoint                  hEndpoint,
				      OpcUa_Handle                    hContext,
				      const OpcUa_RequestHeader*      pRequestHeader,
				      OpcUa_Int32                     nNoOfMethodsToCall,
				      const OpcUa_CallMethodRequest * pMethodsToCall,
				      OpcUa_ResponseHeader*           pResponseHeader,
				      OpcUa_Int32*                    pNoOfResults,
				      OpcUa_CallMethodResult**        pResults,
				      OpcUa_Int32*                    pNoOfDiagnosticInfos,
				      OpcUa_DiagnosticInfo**          pDiagnosticInfos
)
{
	COpcServer *p;

	OpcUa_Endpoint_GetCallbackData(hEndpoint, (void **) &p);

	return p->Call(hEndpoint,
		       hContext,
		       pRequestHeader,
		       nNoOfMethodsToCall,
		       pMethodsToCall,
		       pResponseHeader,
		       pNoOfResults,
		       pResults,
		       pNoOfDiagnosticInfos,
		       pDiagnosticInfos
	);
}

// End of File
