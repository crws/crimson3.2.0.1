
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../Model/OpcDataModel.hpp"
#include "../Model/OpcDataTypeNode.hpp"
#include "../Model/OpcNode.hpp"
#include "../Model/OpcObjectNode.hpp"
#include "../Model/OpcObjectTypeNode.hpp"
#include "../Model/OpcReference.hpp"
#include "../Model/OpcReferenceTypeNode.hpp"
#include "../Model/OpcVariableNode.hpp"
#include "../Model/OpcVariableTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Externals

clink int mapname(char *name, int add);

// Continuation

struct CContPoint
{
	UINT			 uMagic1;
	OpcUa_TimestampsToReturn eTimestampsToReturn;
	OpcUa_DateTime		 StartTime;
	OpcUa_DateTime		 EndTime;
	UINT			 NumValuesPerNode;
	UINT			 uMagic2;
	};

// Services

OpcUa_StatusCode COpcServer::HistoryRead( OpcUa_Endpoint                   hEndpoint,
					  OpcUa_Handle                     hContext,
					  const OpcUa_RequestHeader *      pRequestHeader,
					  const OpcUa_ExtensionObject *    pHistoryReadDetails,
					  OpcUa_TimestampsToReturn         eTimestampsToReturn,
					  OpcUa_Boolean                    bReleaseContinuationPoints,
					  OpcUa_Int32                      nNoOfNodesToRead,
					  const OpcUa_HistoryReadValueId * pNodesToRead,
					  OpcUa_ResponseHeader *           pResponseHeader,
					  OpcUa_Int32 *                    pNoOfResults,
					  OpcUa_HistoryReadResult **       pResults,
					  OpcUa_Int32 *                    pNoOfDiagnosticInfos,
					  OpcUa_DiagnosticInfo **          pDiagnosticInfos
					  )
{
	AfxTrace("opc: historyread\n");

	*pNoOfDiagnosticInfos = 0;
	
	*pDiagnosticInfos     = OpcUa_Null;

	UINT n;

	if( (n = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, false)) < NOTHING ) {

		CSession &Session = m_Session[n];

		if( !nNoOfNodesToRead ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);

			return OpcUa_BadNothingToDo;
			}

		if( bReleaseContinuationPoints ) {

			AfxTrace("     release\n");

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

			return OpcUa_Good;
			}

		if( pHistoryReadDetails->TypeId.NodeId.Identifier.Numeric == OpcUaId_ReadRawModifiedDetails_Encoding_DefaultBinary ) {

			DWORD z1 = GetTickCount();

			UINT  nr = 0;

			OpcUa_ReadRawModifiedDetails *pDetails = (OpcUa_ReadRawModifiedDetails *) pHistoryReadDetails->Body.EncodeableObject.Object;

			////////
			//
			// TODO -- GetBounds handling?
			//         Reverse order data?
			//         Special read first? !!!
			//
			////////

			if( pNodesToRead->ContinuationPoint.Length > 0 ) {

				CContPoint *pCont = (CContPoint *) pNodesToRead->ContinuationPoint.Data;

				if( pCont->uMagic1 == UINT(this) && pCont->uMagic2 == UINT(m_StartTime.dwLowDateTime) ) {

					AfxTrace("opc: using continuation point\n");

					eTimestampsToReturn        = pCont->eTimestampsToReturn;

					pDetails->StartTime        = pCont->StartTime;

					pDetails->EndTime          = pCont->EndTime;

					pDetails->NumValuesPerNode = pCont->NumValuesPerNode;
					}
				else {
					AfxTrace("opc: invalid continuation point\n");

					FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadContinuationPointInvalid);

					return OpcUa_BadContinuationPointInvalid;
					}
				}

			////////

			OpcUa_DateTime ServerTime = OpcUa_DateTime_UtcNow();

			bool           fSendCont  = false;

			CArray <CHistoryData> Data;

			if( m_fHistUsed ) {

				FindHistoryData(Data, fSendCont, pDetails, pRequestHeader->TimeoutHint);

				nr = Data.GetCount();
				}

			////////

			*pResults = OpcAlloc(nNoOfNodesToRead, OpcUa_HistoryReadResult);

			for( int m = 0; m < nNoOfNodesToRead; m++ ) {
				
				OpcUa_HistoryReadValueId const *pRead   = pNodesToRead + m;

				OpcUa_HistoryReadResult        *pResult = *pResults    + m;

				OpcUa_ExtensionObject          *pExt    = &pResult->HistoryData;

				OpcUa_HistoryReadResult_Initialize(pResult);

				m_pModel->Lock();

				COpcNodeId const &NodeId = pRead->NodeId;

				COpcNode         *pNode  = m_pModel->FindNode(NodeId, true);

				if( pNode ) {

					if( !m || m == nNoOfNodesToRead - 1 ) {
					
						AfxTrace( "    %u) %s (%s) for %u\n",
							  m,
							  PCTXT(pNode->GetId().GetAsText()),
							  PCTXT(pNode->GetBrowseName()),
							  pDetails->NumValuesPerNode
							  );
						}

					if( pNode->GetClass() == OpcUa_NodeClass_Variable ) {

						COpcVariableNode *pVar = (COpcVariableNode *) pNode;

						pExt->Encoding			       = OpcUa_ExtensionObjectEncoding_EncodeableObject;

						pExt->TypeId.ServerIndex               = 0;

						pExt->TypeId.NodeId.NamespaceIndex     = 0;
						
						pExt->TypeId.NodeId.IdentifierType     = OpcUa_IdentifierType_Numeric;

						pExt->TypeId.NodeId.Identifier.Numeric = OpcUaId_HistoryData_Encoding_DefaultBinary;

						OpcUa_String_AttachCopy(&pExt->TypeId.NamespaceUri, "");

						OpcUa_HistoryData *pHistoryData = OpcAlloc(1, OpcUa_HistoryData);
					
						pExt->BodySize                     = -1;

						pExt->Body.EncodeableObject.Type   = &OpcUa_HistoryData_EncodeableType;
					
						pExt->Body.EncodeableObject.Object = pHistoryData;

						if( pVar->IsHistorizing() && (Session.m_fAuthed || !pVar->IsPrivate()) ) {

							UINT i = pVar->GetHistorySlot();

							pHistoryData->NoOfDataValues = Data.GetCount();

							pHistoryData->DataValues     = OpcAlloc(pHistoryData->NoOfDataValues, OpcUa_DataValue);

							for( int v = 0; v < pHistoryData->NoOfDataValues; v++ ) {

								CHistoryData const &History = Data[v];

								OpcUa_DataValue    *pValue  = pHistoryData->DataValues + v;

								pValue->ServerPicoseconds = 0;

								pValue->SourcePicoseconds = 0;

								switch( eTimestampsToReturn ) {
									
									case OpcUa_TimestampsToReturn_Server:

										pValue->ServerTimestamp   = ServerTime;

										pValue->SourceTimestamp.t = 0;

										break;
									
									case OpcUa_TimestampsToReturn_Source:

										pValue->ServerTimestamp.t = 0;

										pValue->SourceTimestamp   = History.m_Time;

										break;
									
									case OpcUa_TimestampsToReturn_Both:

										pValue->ServerTimestamp = ServerTime;

										pValue->SourceTimestamp = History.m_Time;

										break;
									
									case OpcUa_TimestampsToReturn_Neither:
									case OpcUa_TimestampsToReturn_Invalid:

										pValue->ServerTimestamp.t = 0;

										pValue->SourceTimestamp.t = 0;

										break;
									}
								
								OpcUa_Variant *pFrom = History.m_pData + i;

								if( pFrom->Datatype == OpcUaId_String || pFrom->Datatype == OpcUaId_NodeId ) {

									CopyVariant(&pValue->Value, pFrom);
									}
								else {
									// Inline copy for performance...

									pValue->Value.ArrayType   = pFrom->ArrayType;

									pValue->Value.Datatype    = pFrom->Datatype;

									pValue->Value.Reserved    = pFrom->Reserved;

									pValue->Value.Value.Int64 = pFrom->Value.Int64;
									}

								pValue->StatusCode = OpcUa_Good;
								}

							if( fSendCont ) {

								CContPoint *pCont          = OpcAlloc(1, CContPoint);

								pCont->uMagic1             = UINT(this);

								pCont->eTimestampsToReturn = eTimestampsToReturn;

								pCont->StartTime           = pDetails->StartTime;

								pCont->EndTime             = pDetails->EndTime;

								pCont->NumValuesPerNode    = pDetails->NumValuesPerNode;

								pCont->uMagic2             = UINT(m_StartTime.dwLowDateTime);

								if( pHistoryData->NoOfDataValues ) {

									if( pCont->NumValuesPerNode ) {

										pCont->NumValuesPerNode -= pHistoryData->NoOfDataValues;
										}

									pCont->StartTime    = Data[pHistoryData->NoOfDataValues-1].m_Time;

									pCont->StartTime.t += 10000000LL;
									}

								pResult->ContinuationPoint.Data   = PBYTE(pCont);

								pResult->ContinuationPoint.Length = sizeof(CContPoint);
								}

							pResult->StatusCode = OpcUa_Good;
							}
						else {
							pHistoryData->NoOfDataValues = 0;

							pHistoryData->DataValues     = NULL;

							pResult->StatusCode = OpcUa_Good;
							}
						}
					else
						pResult->StatusCode = OpcUa_BadNodeIdUnknown;
					}
				else
					pResult->StatusCode = OpcUa_BadNodeIdUnknown;

				m_pModel->Free();
				}

			FreeHistoryData(Data);

			*pNoOfResults = nNoOfNodesToRead;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

			DWORD z2 = GetTickCount();

			UINT  ms = ToTime(z2-z1);

			static UINT lt = 0;

			AfxTrace("opc: replied in %u vs. %u", ms, pRequestHeader->TimeoutHint);

			if( ms > lt ) {

				AfxTrace(" *** NEW RECORD");

				lt = ms;
				}
			else
				AfxTrace(" vs. record of %u", lt);

			AfxTrace("\n");

			if( ms ) {

				UINT rr = (10000 * nr) / ms;

				AfxTrace("opc: ratio is %u.%u:1\n", rr/10, rr%10);
				}

			return OpcUa_Good;
			}

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadHistoryOperationUnsupported);

		return OpcUa_BadHistoryOperationUnsupported;
		}

	return pResponseHeader->ServiceResult;
	}

// History Helpers

void COpcServer::HistoryInit(void)
{
	// If our data model contains historicizing nodes and we can get access
	// to the filing system, configure everything we need for recording and
	// read any existing files from the disk. We can then create the thread
	// that we'll use for subsequent disk writes.

	if( m_pModel->HasHistorizingNodes() ) {

		char path[MAX_PATH] = "C:\\OPCUA";

		mapname(path, 1);

		m_nHistCount      = m_pModel->GetHistorizingCount();
				         
		m_uHistReadLimit  = 6000;
				         
		m_HistFilePath    = path;
				         
		m_uHistTotal      = 0;

		m_fHistUsed       = true;

		m_fHistRead       = false;

		m_pHistDataMutex  = NULL;

		m_pHistWipeMutex  = NULL;

		m_hHistThread     = NULL;

		return;
		}

	m_fHistUsed = false;
	}

void COpcServer::HistoryPoll(void)
{
	// This method is called from the OPC main loop. We use this thread
	// to record historical tags to a memory buffer. This allows us to
	// log precisely without worrying about disk access speed.

	if( m_fHistUsed ) {

		m_pHistDataMutex = Create_Mutex();

		m_pHistWipeMutex = Create_Mutex();

		m_hHistThread    = CreateThread(HistoryProc, 2263, this, 0);

		DWORD e0 = 0;

		for(;;) {

			// Figure out the current time slice based on the
			// sample time and the start time of that slice.

			OpcUa_DateTime t1 = OpcUa_DateTime_UtcNow();

			DWORD          s1 = UnixFromTime(t1, 0);

			DWORD          e1 = s1 / m_uHistSample;

			DWORD          s2 = e1 * m_uHistSample;

			// Check if we're in a new slice.

			if( e0 != e1 ) {

				// And that we're not initializing.

				if( e0 ) {

					HistoryLockData();

					// Make sure the buffer isn't overflowing. Maybe we
					// should wrap around at this point, but right now
					// we just stop saving.

					if( m_HistBuffer.GetCount() < 3600 ) {

						HistoryFreeData();

						// Create a history data structure based on
						// the current time and with variants for all
						// the data values we are going to record.

						CHistoryData Row;

						Row.m_Secs   = s2;

						TimeFromUnix(Row.m_Time, Row.m_Secs);

						Row.m_nCount = m_nHistCount;

						Row.m_pData  = New OpcUa_Variant [ m_nHistCount ];

						// Load the variant array with the data values.

						for( int i = 0; i < m_nHistCount; i++ ) {

							COpcVariableNode *pNode  = m_pModel->GetHistorizingNode(i);

							OpcUa_Variant    *pValue = Row.m_pData + i;

							OpcUa_Variant_Initialize(pValue);

							if( pNode->GetId().IsType(OpcUa_IdType_Numeric) ) {

								UINT ns = pNode->GetId().GetNamespace();
	
								UINT id = pNode->GetId().GetNumericValue();

								if( !SkipValue(2000, ns, id) ) {

									if( pNode->GetValueRank() == OpcUa_ValueRanks_Scalar ) {

										UINT uType = GetWireType(pNode->GetDataType().GetNumericValue());

										GetValue(&pValue->Value, pNode, 2000, ns, id, uType);

										FillArrayType(pValue, uType, OpcUa_VariantArrayType_Scalar, 0);
										}
									}
								}
							}

						// And append it to the history buffer.

						HistoryLockData();

						m_HistBuffer.Append(Row);
						}

					HistoryFreeData();
					}

				// Record the latest time slice.

				e0 = e1;
				}

			// Wait a while before checking again.

			Sleep(100);
			}
		}
	}

void COpcServer::HistoryExec(void)
{
	// This method is called by our own thread. It is responsible for
	// saving data from the memory queue to disk. We sequence things
	// such that data can be in both locations. This is okay as dups
	// will be dropped during the read process.

	if( m_fHistUsed ) {

		// Start by reading the exists data files. This will
		// set the m_fHistRead flag that will be used to enable
		// the reading of historical data.

		ScanExistingFiles();

		HistoryLockData();

		AfxTrace("opc: %u readings accumulated during scan\n", m_HistBuffer.GetCount());

		HistoryFreeData();

		DWORD e0 = 0;

		for(;;) {

			// Figure out the current time slice based
			// on the fixed 60-second saving interval.

			OpcUa_DateTime t1 = OpcUa_DateTime_UtcNow();

			DWORD          s1 = UnixFromTime(t1, 0);

			DWORD          e1 = s1 / 60;

			// Check if we're in a new slice.

			if( e0 != e1 ) {

				// And that we're not initializing.

				if( e0 ) {

					AfxTrace("opc: saving history\n");

					HistoryLockData();

					if( !m_HistBuffer.IsEmpty() ) {

						// Get the current extent of the buffer.

						INDEX Head = m_HistBuffer.GetHead();

						INDEX Tail = m_HistBuffer.GetTail();

						INDEX Scan = Head;

						HistoryFreeData();

						// And set a timer to avoid locking the data
						// files for so long that we prevent reads.

						SetTimer(2000);

						for(;;) {

							// Find the history data we want to write.

							CHistoryData const &Data = m_HistBuffer[Scan];

							// And check the file hogging timer.

							if( !GetTimer() ) {

								AfxTrace("opc: hog limit reached\n");

								// If the timer has elapsed, close the files
								// to allow any readers to get in. They will
								// be opened automatically by SaveHistoryData.

								CloseSaveFiles();

								Sleep(1000);

								SetTimer(2000);
								}

							// Save the history data to disk.

							if( SaveHistoryData(Data.m_Secs, Data.m_pData, Data.m_nCount) ) {

								// If we're not at the end, move on to the next row.

								if( Scan == Tail ) {

									break;
									}

								m_HistBuffer.GetNext(Scan);

								continue;
								}

							break;
							}

						// Walk the list again and free the data. We lock everything
						// during this process to make sure that the reader doesn't get
						// confused about what data is on disk and what is in memory.

						HistoryLockWipe();

						HistoryLockData();

						for( Scan = Head;; ) {

							INDEX               Last = Scan;

							CHistoryData const &Data = m_HistBuffer[Scan];

							FreeHistoryData(Data);

							if( Scan == Tail ) {

								m_HistBuffer.Remove(Last);

								break;
								}

							m_HistBuffer.GetNext(Scan);

							m_HistBuffer.Remove (Last);
							}

						HistoryFreeData();

						HistoryFreeWipe();
						}
					else
						HistoryFreeData();

					// Tidy up everything.

					CloseSaveFiles();

					// Check total disk usage.

					CheckTotalSize();
					}

				// Record the latest time slice.

				e0 = e1;
				}

			// Wait a while before checking again.

			Sleep(100);
			}
		}
	}

void COpcServer::HistoryStop(void)
{
	// Stop operation by killing our thread.

	if( m_fHistUsed ) {

		if( m_hHistThread ) {

			DestroyThread(m_hHistThread);
			}
		}
	}

void COpcServer::HistoryTerm(void)
{
	// Shut down by tidying everything up.

	if( m_fHistUsed ) {

		AfxRelease(m_pHistDataMutex);

		AfxRelease(m_pHistWipeMutex);
		}
	}

void COpcServer::HistoryLockData(void)
{
	// Claim the data consistency mutex.

	m_pHistDataMutex->Wait(FOREVER);
	}

void COpcServer::HistoryFreeData(void)
{
	// Free the data consistency mutex.

	m_pHistDataMutex->Free();
	}

void COpcServer::HistoryLockWipe(void)
{
	// Claim the data wiping mutex.

	m_pHistWipeMutex->Wait(FOREVER);
	}

void COpcServer::HistoryFreeWipe(void)
{
	// Free the data wiping mutex.
	
	m_pHistWipeMutex->Free();
	}

void COpcServer::FindHistoryData(CArray <CHistoryData> &Data, bool &fCont, OpcUa_ReadRawModifiedDetails const *pDetails, UINT uTimeout)
{
	// This method is used to read history data rows from either disk
	// files or the in-memory buffer. We start by making sure that all
	// the historical files have been loaded.

	if( m_fHistRead ) {

		// We have to be careful not to take too much time so we set a timer
		// to a share of the timeout hint. When this expires, the methods that
		// we call will abort and indicate continuation is required.

		AfxTrace("opc: using %u%% of timeout hint of %u\n", m_uHistTime, uTimeout);

		SetTimer(m_uHistTime * uTimeout / 100);

		// Find the start and end times in Unix format.

		DWORD rb = UnixFromTime(pDetails->StartTime, 0);

		DWORD re = UnixFromTime(pDetails->EndTime,   NOTHING);

		UINT  np = pDetails->NumValuesPerNode;

		AfxTrace("opc: looking from %s to %s for %u\n", PCTXT(FormatTime(rb)), PCTXT(FormatTime(re)), np);

		// We don't want anything wiped during this. It is okay for data
		// to be written to disk, but it must not be removed from the buffer
		// as it's possible that we'd then fail to read it. This way, we
		// might come across it twice, but the adjustment to the start time
		// after the disk read avoids any duplication.

		HistoryLockWipe();

		// Start by checking the disk files for data.

		FindHistoryDataDisk(Data, fCont, rb, re, np);

		// If continuation is required or we have reached the requested
		// number of points, we are done. But other wise we need to look
		// in the in-memory buffer for additional rows.

		if( !(fCont || Data.GetCount() == np) ) {

			// If we have read data from disk, adjust the read begin
			// value so that we don't duplicate data that is in the
			// process of being written to disk.

			if( !Data.IsEmpty() ) {

				CHistoryData const &Last = Data[Data.GetCount()-1];

				rb = Last.m_Secs + 1;
				}

			// And then see what we can find locally.

			FindHistoryDataList(Data, fCont, rb, re, np);
			}

		HistoryFreeWipe();

		AfxTrace("opc: total of %u found\n", Data.GetCount());

		if( fCont ) {

			AfxTrace("opc: continuation will be provided\n");
			}

		if( Data.GetCount() ) {

			AfxTrace("opc: first is %s\n", PCTXT(FormatTime(Data[0].m_Time)));

			AfxTrace("opc: last  is %s\n", PCTXT(FormatTime(Data[Data.GetCount()-1].m_Time)));
			}
		}
	else {
		// We're not running so wait a while and return no data but
		// a continuation point. This will force the client to retry
		// and eventually we shall be ready to reply with data.

		DWORD rb = UnixFromTime(pDetails->StartTime, 0);

		DWORD re = UnixFromTime(pDetails->EndTime,   NOTHING);

		UINT  np = pDetails->NumValuesPerNode;

		AfxTrace("opc: looking from %s to %s for %u\n", PCTXT(FormatTime(rb)), PCTXT(FormatTime(re)), np);

		AfxTrace("opc: not running so punt on read\n");

		Sleep(Min(5000, uTimeout / 4));

		fCont = true;
		}
	}

void COpcServer::FindHistoryDataDisk(CArray <CHistoryData> &Data, bool &fCont, DWORD rb, DWORD re, UINT np)
{
	// This method is used to scan the list of disk files for data
	// values that correspond to the specfied time window. We add
	// any rows to the array, and set fCont if a continuation is
	// needed as a result of a timeout or too much data.

	bool fDone = false;

	// Keep the data locked while we are examining
	// the list of files, but free it otherwise for
	// better performance.

	HistoryLockData();

	for( UINT f = 0; !fDone && f < m_HistFileList.GetCount(); f++ ) {

		if( GetTimer() ) {

			// Find the file list entry.

			CHistoryFile File = m_HistFileList[f];

			HistoryFreeData();

			// If the end of the target range is beyond the start
			// of this file, we need to consider the file contents.

			if( re >= File.m_TimeBegin ) {

				// If the start of the target range is before the end
				// of this file, we need to consider the file contents.

				if( rb <= File.m_TimeEnd ) {

					// Figure out the number of entries.

					UINT ci = (File.m_TimeEnd - File.m_TimeBegin) / m_uHistSample + 1;

					// And open the associated index file.

					CAutoFile FileIndx;

					if( OpenFile(FileIndx, File.m_IndxName, FALSE) ) {

						AfxTrace("opc: opened file %s for read\n", PCTXT(File.m_IndxName));

						// Allocate memory for and then read the index contents.

						PDWORD pi = New DWORD [ ci ];

						if( FileIndx.Read(PBYTE(pi), ci * sizeof(DWORD)) == ci * sizeof(DWORD) ) {

							// Figure out the area we're actually interested in.

							DWORD  db = Max(rb, File.m_TimeBegin);

							DWORD  de = Min(re, File.m_TimeEnd);

							UINT   nb = (db - File.m_TimeBegin) / m_uHistSample;

							UINT   ne = (de - File.m_TimeBegin) / m_uHistSample;

							// And open the data file to read the actual data.

							CAutoFile FileData;

							if( OpenFile(FileData, File.m_DataName, FALSE) ) {

								AfxTrace("opc: opened file %s for read\n", PCTXT(File.m_DataName));

								// For each slot, check the index for a non-zero
								// value that would indicate that data is present.

								for( UINT n = nb; n <= ne; n++ ) {

									DWORD p = pi[n];

									if( p ) {

										// Create a history data structure based on
										// the record time and with variants for all
										// the data values we are going to read.

										CHistoryData Row;

										Row.m_Secs = File.m_TimeBegin + n * m_uHistSample;

										TimeFromUnix(Row.m_Time, Row.m_Secs);

										Row.m_nCount = m_nHistCount;

										Row.m_pData  = New OpcUa_Variant [ Row.m_nCount ];

										// Find the correct position in the data file.

										FileData.Seek(p);

										// And read the data.

										if( LoadHistoryData(FileData, Row.m_pData, Row.m_nCount) ) {

											// Append it to the data set.

											UINT uCount = 1 + Data.Append(Row);

											// If we've timed out or have enough data, we can stop now.

											if( !GetTimer() || uCount == np || uCount == m_uHistReadLimit ) {

												// If we're not done, set the continuation flag.

												if( uCount != np ) {

													fCont = true;
													}

												fDone = true;

												break;
												}

											continue;
											}

										// The read failed so we need to tidy up this row. Right
										// now, we just go around again and try to read the next
										// one. Not sure if we should fail at this point...
											
										delete [] Row.m_pData;
										}
									}

								// Close the data file.

								FileData.Close();
								}
							}

						// Free the index data.

						delete [] pi;

						// Close the index file.

						FileIndx.Close();
						}
					}

				// Keep looking for other usable files.

				HistoryLockData();

				continue;
				}

			// Past end of requested range so we're done.

			HistoryLockData();

			break;
			}
		else {
			AfxTrace("opc: timeout hint expired\n");

			fCont = true;

			// Timer run out so stop and continue later.

			break;
			}
		}

	HistoryFreeData();
	}

void COpcServer::FindHistoryDataList(CArray <CHistoryData> &Data, bool &fCont, DWORD rb, DWORD re, UINT np)
{
	// TODO -- This either needs guards or auto arrays!!!

	// This method is used to scan the in-memory buffer for data
	// values that correspond to the specfied time window. We add
	// any rows to the array, and set fCont if a continuation is
	// needed as a result of a timeout or too much data.

	HistoryLockData();

	if( !m_HistBuffer.IsEmpty() ) {

		// Get the current extent of the buffer.

		INDEX Head = m_HistBuffer.GetHead();

		INDEX Tail = m_HistBuffer.GetTail();

		INDEX Scan = Head;

		HistoryFreeData();

		for(;;) {

			if( GetTimer() ) {

				// Find the row in the buffer.

				CHistoryData const &Src = m_HistBuffer[Scan];

				// If the end of the target range is beyond the time
				// of this row, we need to consider the row contents.

				if( re >= Src.m_Secs ) {

					// If the start of the target range is before the time
					// of this row, we need to consider the row contents.

					if( rb <= Src.m_Secs ) {

						// Create a history data structure based on
						// the record time and with variants for all
						// the data values we are going to read.

						CHistoryData Row;

						Row.m_Secs   = Src.m_Secs;

						Row.m_Time   = Src.m_Time;

						Row.m_nCount = Src.m_nCount;

						Row.m_pData  = New OpcUa_Variant [ Row.m_nCount ];

						// Copy all the data across.

						for( int n = 0; n < Row.m_nCount; n++ ) {

							CopyVariant(Row.m_pData + n, Src.m_pData + n);
							}

						// Append it to the data set.

						UINT uCount = 1 + Data.Append(Row);

						// If we're done or have enough data, we can stop now.

						if( uCount == np || uCount == m_uHistReadLimit ) {

							// If we're not done and if we may have more
							// data available, set the continuation flag.

							if( uCount != np ) {

								if( Scan != Tail ) {

									fCont = true;
									}
								}

							break;
							}
						}

					// If we are not at the end of the
					// list, keep looking at other rows.

					if( Scan == Tail ) {

						break;
						}

					m_HistBuffer.GetNext(Scan);

					continue;
					}

				// Past end of requested range so we're done.

				break;
				}
			else {
				fCont = true;

				// Timer run out so stop and continue later.

				break;
				}
			}

		HistoryLockData();
		}

	HistoryFreeData();
	}

void COpcServer::FreeHistoryData(CArray <CHistoryData> const &Data)
{
	// Free all the contents of an array of history data items.

	for( UINT r = 0; r < Data.GetCount(); r++ ) {

		CHistoryData const &Row = Data[r];

		FreeHistoryData(Row);
		}
	}

void COpcServer::FreeHistoryData(CHistoryData const &Row)
{
	// Free the variants in a particular history data row.

	for( int c = 0; c < Row.m_nCount; c++ ) {

		OpcUa_Variant_Clear(Row.m_pData + c);
		}

	delete [] Row.m_pData;
	}

bool COpcServer::SaveHistoryData(DWORD t, OpcUa_Variant *pValues, int nCount)
{
	// Write a set of variants to disk. We use the time stamp to
	// figure out which file to use. The data goes a the end of
	// the data file, and the index file is updated to contain
	// a pointer to the data in the appropriate time slot.

	if( OpenSaveFiles(t) ) {

		// Build a blob contaning the variant data.

		CByteArray Data;

		EncodeHistoryData(Data, pValues, nCount);

		// And write it to disk with various metadata.

		UINT uData = m_HistDataFile.Tell();

		UINT uSize = Data.GetCount();

		m_HistDataFile.Write(PCBYTE("SSSS"), 4);

		m_HistDataFile.Write(PCBYTE(&nCount), sizeof(nCount));

		m_HistDataFile.Write(PCBYTE(&uSize), sizeof(uSize));

		m_HistDataFile.Write(Data.GetPointer(), Data.GetCount());

		m_HistDataFile.Write(PCBYTE("EEEE"), 4);

		UINT uDone = m_HistDataFile.Tell();

		// If it wrote correctly, we can update the index file.

		if( uDone == uData + uSize + 16 ) {

			// Figure out the index file slot location.

			UINT uIndx = 16 + (t - m_HistDataBase) / m_uHistSample * 4;

			// Check if the index file is too short.

			if( m_HistIndxFile.GetSize() < uIndx ) {

				// And if so, extend it by appending zeroes
				// until we are at the length that we need.

				m_HistIndxFile.SeekEnd();

				while( m_HistIndxFile.Tell() < uIndx ) {

					DWORD z = 0;

					m_HistIndxFile.Write(PCBYTE(&z), sizeof(z));
					}
				}

			// And write the data pointer to the index.

			m_HistIndxFile.Seek(uIndx);

			m_HistIndxFile.Write(PCBYTE(&uData), sizeof(uData));

			// Now we're done, we can update the history file
			// record to contain the start time if it's new,
			// and to contain the end time in any case.

			HistoryLockData();

			UINT          uEnd = m_HistFileList.GetCount() - 1;

			CHistoryFile &File = (CHistoryFile &) m_HistFileList[uEnd];

			if( !File.m_TimeBegin ) {

				File.m_TimeBegin = m_HistDataBase;
				}

			File.m_TimeEnd = t;

			m_uHistTotal  -= File.m_uSize;

			File.m_uSize   = uDone;

			m_uHistTotal  += File.m_uSize;

			HistoryFreeData();

			// If the index or data files have become too big,
			// clear the file name so that we'll move on to a
			// new file next time we write.

			if( uIndx > 64 * 1024 || uDone > 2 * 1024 * 1024 ) {

				CloseSaveFiles();

				m_HistDataName.Empty();
				}

			return true;
			}

		CloseSaveFiles();
		}

	return false;
	}

bool COpcServer::OpenSaveFiles(DWORD t)
{
	// Open the files needed to save to a particular time.

	if( !m_HistIndxFile ) {

		for( int p = 0; p < 2; p++ ) {

			bool fNew = false;

			if( m_HistDataName.IsEmpty() ) {

				m_HistDataName.Printf("%8.8u.D%2.2u", t / 100, t % 100);

				m_HistIndxName.Printf("%8.8u.N%2.2u", t / 100, t % 100);

				m_HistDataBase = t;

				fNew           = true;
				}

			if( OpenFile(m_HistIndxFile, m_HistIndxName, TRUE) ) {

				AfxTrace("opc: opened file %s for write\n", PCTXT(m_HistIndxName));

				if( OpenFile(m_HistDataFile, m_HistDataName, TRUE) ) {

					AfxTrace("opc: opened file %s for write\n", PCTXT(m_HistDataName));

					m_HistDataFile.SeekEnd();

					if( fNew ) {

						AfxTrace("opc: adding file %s\n", PCTXT(m_HistDataName));

						CHistoryFile File;

						File.m_DataName  = m_HistDataName;

						File.m_IndxName  = m_HistIndxName;

						File.m_TimeBegin = 0;

						File.m_TimeEnd   = 0;

						File.m_uSize     = 0;

						HistoryLockData();

						m_HistFileList.Append(File);

						HistoryFreeData();
						}

					return true;
					}

				m_HistIndxFile.Close();
				}

			// TODO -- Is this risky during shutdown?

			AfxTrace("opc: deleting %s after failed open\n", PCTXT(m_HistDataName));

			unlink(m_HistFilePath + m_HistDataName);

			unlink(m_HistFilePath + m_HistIndxName);
			}

		return false;
		}

	return true;
	}

bool COpcServer::CloseSaveFiles(void)
{
	// Close the files we use to save data.

	if( m_HistIndxFile ) {

		AfxTrace("opc: closing file\n");

		m_HistDataFile.Close();

		m_HistIndxFile.Close();

		return true;
		}

	return false;
	}

bool COpcServer::LoadHistoryData(CAutoFile &File, OpcUa_Variant *pValues, int nCount)
{
	// Read a set of variants from disk.

	char s[4] = {0};

	if( File.Read(PBYTE(s), 4) == 4 && !strncmp(s, "SSSS", 4) ) {

		int nTest;

		if( File.Read(PBYTE(&nTest), sizeof(nTest)) == sizeof(nTest) ) {

			if( nTest == nCount ) {

				UINT uSize;

				if( File.Read(PBYTE(&uSize), sizeof(uSize)) == sizeof(uSize) ) {

					if( uSize > 0 && uSize < 64 * 1024 ) {

						CByteArray Data;

						Data.SetCount(uSize);

						if( File.Read(PBYTE(Data.GetPointer()), uSize) == uSize ) { 

							char e[4] = {0};

							if( File.Read(PBYTE(e), 4) == 4 && !strncmp(e, "EEEE", 4) ) {

								PCBYTE pData = Data.GetPointer();

								for( int i = 0; i < nCount; i++ ) {

									COpcVariableNode *pNode  = m_pModel->GetHistorizingNode(i);

									OpcUa_Variant    *pValue = pValues + i;

									// Inline initialization for performance...

									pValue->ArrayType   = BYTE(OpcUa_VariantArrayType_Scalar);
									
									pValue->Datatype    = BYTE(GetWireType(pNode->GetDataType().GetNumericValue()));

									pValue->Reserved    = 0;

									pValue->Value.Int64 = 0;

									DecodeHistoryData(pData, pValue);
									}

								return true;
								}
							}
						}
					}
				}
			}
		}

	AfxTrace("opc: failed to read row\n");

	return false;
	}

void COpcServer::EncodeHistoryData(CByteArray &Data, OpcUa_Variant *pValues, int nCount)
{
	// Encode a set of variants into a byte array.

	for( int n = 0; n < nCount; n++ ) {

		OpcUa_Variant *pValue = pValues + n;

		EncodeHistoryData(Data, pValue);
		}
	}

void COpcServer::EncodeHistoryData(CByteArray &Data, OpcUa_Variant *pValue)
{
	UINT uSize;

	// Encode a single variant into a byte array.

	switch( pValue->Datatype ) {
		
		case OpcUaId_Boolean:
		case OpcUaId_Byte:
		case OpcUaId_SByte:

			Data.Append(PCBYTE(&pValue->Value.Byte), 1);

			break;

		case OpcUaId_Int16:
		case OpcUaId_UInt16:

			Data.Append(PCBYTE(&pValue->Value.Int16), 2);

			break;

		case OpcUaId_Int32:
		case OpcUaId_UInt32:
		case OpcUaId_Float:

			Data.Append(PCBYTE(&pValue->Value.Int32), 4);

			break;
		
		case OpcUaId_Int64:
		case OpcUaId_UInt64:
		case OpcUaId_Double:
		case OpcUaId_DateTime:

			Data.Append(PCBYTE(&pValue->Value.Int64), 8);

			break;
		
		case OpcUaId_String:

			uSize = pValue->Value.String.uLength + 1;

			Data.Append(PCBYTE(&uSize), 4);

			Data.Append(PCBYTE(pValue->Value.String.strContent), uSize);

			break;
		}
	}

void COpcServer::DecodeHistoryData(PCBYTE &pData, OpcUa_Variant *pValue)
{
	// Decode a single variant from a byte array.

	UINT   uSize;

	// Take a copy to maximize compiler optimization.

	PCBYTE pCopy = pData;

	switch( pValue->Datatype ) {
		
		case OpcUaId_Boolean:
		case OpcUaId_Byte:
		case OpcUaId_SByte:

			PBYTE(&pValue->Value.Byte)[0] = *pCopy++;

			break;

		case OpcUaId_Int16:
		case OpcUaId_UInt16:

			PBYTE(&pValue->Value.Int16)[0] = *pCopy++;
			PBYTE(&pValue->Value.Int16)[1] = *pCopy++;

			break;

		case OpcUaId_Int32:
		case OpcUaId_UInt32:
		case OpcUaId_Float:

			PBYTE(&pValue->Value.Int32)[0] = *pCopy++;
			PBYTE(&pValue->Value.Int32)[1] = *pCopy++;
			PBYTE(&pValue->Value.Int32)[2] = *pCopy++;
			PBYTE(&pValue->Value.Int32)[3] = *pCopy++;

			break;
		
		case OpcUaId_Int64:
		case OpcUaId_UInt64:
		case OpcUaId_Double:
		case OpcUaId_DateTime:

			PBYTE(&pValue->Value.Int64)[0] = *pCopy++;
			PBYTE(&pValue->Value.Int64)[1] = *pCopy++;
			PBYTE(&pValue->Value.Int64)[2] = *pCopy++;
			PBYTE(&pValue->Value.Int64)[3] = *pCopy++;
			PBYTE(&pValue->Value.Int64)[4] = *pCopy++;
			PBYTE(&pValue->Value.Int64)[5] = *pCopy++;
			PBYTE(&pValue->Value.Int64)[6] = *pCopy++;
			PBYTE(&pValue->Value.Int64)[7] = *pCopy++;

			break;
		
		case OpcUaId_String:

			(PBYTE(&uSize)[0]) = *pCopy++;
			(PBYTE(&uSize)[1]) = *pCopy++;
			(PBYTE(&uSize)[2]) = *pCopy++;
			(PBYTE(&uSize)[3]) = *pCopy++;

			OpcUa_String_AttachCopy(&pValue->Value.String, PTXT(pCopy));

			pCopy += uSize;

			break;
		}

	pData = pCopy;
	}

bool COpcServer::ScanExistingFiles(void)
{
	// Look for any existing files that contain data that reflects
	// previous historical information and add them to our list of
	// data files for consideration.

	mkdir(m_HistFilePath, 0);

	CAutoDirentList List;

	if( List.ScanFiles(m_HistFilePath) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CFilename Name(List[n]->d_name);

			CString   DataName(Name);

			CString   IndxName(Name);

			if( DataName[9] == 'D' ) {

				IndxName.SetAt(9, 'N');

				CAutoFile FileData;

				if( OpenFile(FileData, DataName, FALSE) ) {

					CAutoFile FileIndx;

					if( OpenFile(FileIndx, IndxName, FALSE) ) {

						UINT  uPos = FileIndx.Tell();

						UINT  uEnd = FileIndx.GetSize();

						UINT  ci   = (uEnd - uPos) / 4;

						if( ci ) {

							AfxTrace("opc: using existing file %s\n", PCTXT(DataName));

							CHistoryFile File;

							File.m_DataName  = DataName;

							File.m_IndxName  = IndxName;

							File.m_TimeBegin = atoi(DataName.Left(8) + DataName.Right(2));
						
							File.m_TimeEnd   = File.m_TimeBegin + (ci - 1) * m_uHistSample;

							File.m_uSize     = FileData.GetSize();

							m_uHistTotal    += File.m_uSize;

							m_HistFileList.Append(File);

							Sleep(100);
							}

						continue;
						}
					}

				unlink(m_HistFilePath + DataName);

				Sleep(100);

				unlink(m_HistFilePath + IndxName);

				Sleep(100);
				}
			}

		m_HistFileList.Sort();

		// TODO -- Need an API to get the disk size!!!

		m_uHistDiskSize   = 1 * 1024 * 1024 * 1024;

		m_uHistTotalLimit = m_uHistQuota * m_uHistDiskSize / 100;

		CheckTotalSize();

		m_fHistRead = true;

		return true;
		}

	m_fHistRead = true;

	return false;
	}

bool COpcServer::OpenFile(CAutoFile &File, CString const &Name, BOOL fWrite)
{
	// Open a file, and either add or validate the header
	// used to match the data file to the current config.

	if( fWrite ) {

		File.Open(m_HistFilePath + Name, "r+", "w+");
		}
	else
		File.Open(m_HistFilePath + Name, "r");

	if( File ) {

		if( File.GetSize() == 0 ) {

			if( File.Write(m_bHistGuid, 16) == 16 ) {

				return true;
				}
			}
		else {
			BYTE bTest[16];

			if( File.Read(bTest, 16) == 16 ) {

				if( !memcmp(bTest, m_bHistGuid, 16) ) {

					return true;
					}
				}
			}

		File.Close();
		}

	AfxTrace("opc: failed to open %s\n", PCTXT(Name));

	return false;
	}

void COpcServer::CheckTotalSize(void)
{
	// TODO -- Need API to get free space!!!

	UINT uFree = 100 * 1024 * 1024;

	while( uFree < 100 * 1024 * 1024 || m_uHistTotal > m_uHistTotalLimit ) {

		AfxTrace("opc: low on disk space\n");

		if( !m_HistFileList.IsEmpty() ) {

			// Lock the wipe mutex and remove the oldest
			// file from the active list so we can work
			// on deleting it without a lock.

			HistoryLockWipe();

			HistoryLockData();

			CHistoryFile File = m_HistFileList[0];

			m_HistFileList.Remove(0);

			HistoryFreeData();

			HistoryFreeWipe();

			// We can now delete the file.

			AfxTrace("opc: deleting %s\n", PCTXT(File.m_DataName));

			unlink(m_HistFilePath + File.m_DataName);

			Sleep(100);

			AfxTrace("opc: deleting %s\n", PCTXT(File.m_IndxName));

			unlink(m_HistFilePath + File.m_IndxName);

			Sleep(100);

			// And update the total size.

			HistoryLockData();

			m_uHistTotal -= File.m_uSize;

			HistoryFreeData();

			continue;
			}

		AfxTrace("opc: all files deleted\n");

		break;
		}

	AfxTrace("opc: disk space is good\n");
	}

void COpcServer::Print64(PCTXT pName, UINT64 n)
{
	char s[32];

	int    i = 0;

	UINT64 f = 1;

	while( f < n ) {
		
		f *= 10;
		}

	while( f /= 10 ) {

		int d = int(n / f) % 10;

		s[i++] = char('0' + d);

		s[i  ] = 0;
		}

	AfxTrace("%s = %s\n", pName, s);
	}

// Time Formatting

CString COpcServer::FormatTime(DWORD t)
{
	OpcUa_DateTime dt;

	TimeFromUnix(dt, t);

	char s[64];

	OpcUa_DateTime_GetStringFromDateTime(dt, s, sizeof(s));

	return s;
	}

CString COpcServer::FormatTime(OpcUa_DateTime const &dt)
{
	char s[64];

	OpcUa_DateTime_GetStringFromDateTime(dt, s, sizeof(s));

	return s;
	}

// History Thread

int COpcServer::HistoryProc(IThread *pThread, PVOID pParam, UINT uParam)
{
	// Thunk for calling our thread.

	SetThreadName("OpcUa.Save");

	((COpcServer *) pParam)->HistoryExec();

	return 0;
	}

// End of File
