
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Services

OpcUa_StatusCode COpcServer::GetEndpoints(OpcUa_Endpoint               hEndpoint,
					  OpcUa_Handle                 hContext,
					  OpcUa_RequestHeader *        pRequestHeader,
					  OpcUa_String *               pEndpointUrl,
					  OpcUa_Int32                  nNoOfLocaleIds,
					  OpcUa_String *               pLocaleIds,
					  OpcUa_Int32                  nNoOfProfileUris,
					  OpcUa_String *               pProfileUris,
					  OpcUa_ResponseHeader *       pResponseHeader,
					  OpcUa_Int32 *                pNoOfEndpoints,
					  OpcUa_EndpointDescription ** ppEndpoints
)
{
	int n = 0;

	for( n = 0; n < nNoOfProfileUris; n++ ) {

		PCTXT pUri = OpcUa_String_GetRawString(pProfileUris + n);

		// Partial Match?

		if( !strcasecmp(pUri, OpcUa_TransportProfile_UaTcp) ) {

			break;
		}
	}

	if( n && n == nNoOfProfileUris ) {

		*pNoOfEndpoints = 0;

		*ppEndpoints    = OpcUa_Null;

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		return OpcUa_Good;
	}

	GetEndpoints(pNoOfEndpoints, ppEndpoints);

	FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

	return OpcUa_Good;
}

// Callbacks

void COpcServer::EndpointCallback(OpcUa_Endpoint          hEndpoint,
				  OpcUa_Endpoint_Event    eEvent,
				  OpcUa_StatusCode        uStatus,
				  OpcUa_UInt32            uSecureChannelId,
				  OpcUa_ByteString *      pbsClientCertificate,
				  OpcUa_String *          pSecurityPolicy,
				  OpcUa_UInt16            uSecurityMode
)
{
}

// Endpoint Helpers

void COpcServer::FillServerData(OpcUa_ApplicationDescription *pDesc)
{
	OpcUa_ApplicationDescription_Initialize(pDesc);

	OpcUa_String_AttachCopy(&pDesc->ApplicationUri, PTXT(PCTXT(m_pHost->GetValueString(1000, 0, OpcUaId_Server_ServerStatus_BuildInfo_ApplicationUri, 0))));

	OpcUa_String_AttachCopy(&pDesc->ProductUri, PTXT(PCTXT(m_pHost->GetValueString(1000, 0, OpcUaId_Server_ServerStatus_BuildInfo_ProductUri, 0))));

	OpcUa_String_AttachCopy(&pDesc->ApplicationName.Text, PTXT(PCTXT(m_pHost->GetValueString(1000, 0, OpcUaId_Server_ServerArray, 0))));

	OpcUa_String_AttachCopy(&pDesc->ApplicationName.Locale, "en");

	pDesc->ApplicationType = OpcUa_ApplicationType_Server;

	pDesc->DiscoveryUrls   = OpcAlloc(m_Endpoint.GetCount(), OpcUa_String);

	for( UINT n = 0; n < m_Endpoint.GetCount(); n++ ) {

		OpcUa_String_Initialize(pDesc->DiscoveryUrls + n);

		OpcUa_String_AttachCopy(pDesc->DiscoveryUrls + n, PTXT(PCTXT(m_Endpoint[n])));
	}

	pDesc->NoOfDiscoveryUrls = m_Endpoint.GetCount();
}

void COpcServer::GetEndpoints(OpcUa_Int32 *pNoOfEndpoints, OpcUa_EndpointDescription **ppEndpoints)
{
	OpcUa_EndpointDescription *pEndpoints = OpcAlloc(m_Endpoint.GetCount(), OpcUa_EndpointDescription);

	for( UINT n = 0; n < m_Endpoint.GetCount(); n++ ) {

		OpcUa_EndpointDescription *pEndpoint = pEndpoints + n;

		OpcUa_EndpointDescription_Initialize(pEndpoint);

		OpcUa_String_AttachCopy(&pEndpoint->EndpointUrl, PTXT(PCTXT(m_Endpoint[n])));

		OpcUa_String_AttachCopy(&pEndpoint->TransportProfileUri, OpcUa_TransportProfile_UaTcp);

		FillServerData(&pEndpoint->Server);

		OpcUa_String_AttachCopy(&pEndpoint->SecurityPolicyUri, OpcUa_SecurityPolicy_None);

		pEndpoint->SecurityMode = OpcUa_MessageSecurityMode_None;

		////////

		static PCTXT pList[] = {

			OpcUa_SecurityPolicy_Basic256,
			OpcUa_SecurityPolicy_Basic128Rsa15,
			OpcUa_SecurityPolicy_Basic256Sha256,
			OpcUa_SecurityPolicy_Aes128Sha256RsaOaep
			OpcUa_SecurityPolicy_None,
			NULL,
		};

		// Skip all but last two if we don't have a certificate
		// that we can use to perform the password cryptography.

		UINT uList = m_Cert.IsEmpty() ? (elements(pList) - 2) : 0;

		pEndpoint->NoOfUserIdentityTokens = elements(pList) - uList;

		pEndpoint->UserIdentityTokens     = OpcAlloc(pEndpoint->NoOfUserIdentityTokens, OpcUa_UserTokenPolicy);

		for( INT n = 0; n < pEndpoint->NoOfUserIdentityTokens; n++ ) {

			OpcUa_UserTokenPolicy *pPolicy = pEndpoint->UserIdentityTokens + n;

			OpcUa_UserTokenPolicy_Initialize(pPolicy);

			if( pList[uList] ) {

				CPrintf Name("UserName_%s", strchr(pList[uList], '#') + 1);

				pPolicy->TokenType = OpcUa_UserTokenType_UserName;

				OpcUa_String_AttachCopy(&pPolicy->PolicyId, Name);

				OpcUa_String_AttachCopy(&pPolicy->SecurityPolicyUri, pList[uList]);
			}
			else {
				pPolicy->TokenType = OpcUa_UserTokenType_Anonymous;

				OpcUa_String_AttachCopy(&pPolicy->PolicyId, "Anonymous");

				OpcUa_String_AttachCopy(&pPolicy->SecurityPolicyUri, pList[uList]);
			}

			uList++;
		}

		pEndpoint->SecurityLevel = 0;
	}

	*pNoOfEndpoints = m_Endpoint.GetCount();

	*ppEndpoints    = pEndpoints;
}

// End of File
