
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Services

OpcUa_StatusCode COpcServer::FindServers( OpcUa_Endpoint                  hEndpoint,
					  OpcUa_Handle                    hContext,
					  const OpcUa_RequestHeader *     pRequestHeader,
					  const OpcUa_String *            pEndpointUrl,
					  OpcUa_Int32                     nNoOfLocaleIds,
					  const OpcUa_String *            pLocaleIds,
					  OpcUa_Int32                     nNoOfServerUris,
					  const OpcUa_String *            pServerUris,
					  OpcUa_ResponseHeader *          pResponseHeader,
					  OpcUa_Int32 *                   pNoOfServers,
					  OpcUa_ApplicationDescription ** pServers
					  )
{
	CString Name = m_pHost->GetValueString(1000, 0, OpcUaId_Server_ServerArray, 0);

	int n;

	for( n = 0; n < nNoOfServerUris; n++ ) {

		PCTXT pUri = OpcUa_String_GetRawString(pServerUris + n);

		if( Name.StartsWith(pUri) ) {

			break;
			}
		}

	if( n && n == nNoOfServerUris ) {

		pNoOfServers = 0;
	
		pServers     = OpcUa_Null;

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		return OpcUa_Good;
		}
	
	*pServers = OpcAlloc(1, OpcUa_ApplicationDescription);

	FillServerData(*pServers);

	*pNoOfServers = 1;

	FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

	return OpcUa_Good;
	}

// End of File
