/* ========================================================================
 * Copyright (c) 2005-2018 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

/******************************************************************************************************/
/* Platform Portability Layer                                                                         */
/* Modify the content of this file according to the thread implementation on your system.             */
/* This is the pthreads implementation.                                                               */
/* For Information about pthread see http://sourceware.org/pthreads-win32/.                           */
/******************************************************************************************************/

/* System Headers */
#include <StdEnv.hpp>

/* UA platform definitions */
#include <opcua_p_internal.h>

/* additional UA dependencies */
#include <opcua_memory.h>
#include <opcua_p_memory.h>
#include <opcua_semaphore.h>
#include <opcua_mutex.h>

/* own headers */
#include <opcua_thread.h>
#include <opcua_p_thread.h>
#include <opcua_p_openssl.h>

/*============================================================================
 * Thread Info Block
 *===========================================================================*/
struct CThreadInfo
{
    IThread                     * pThread;
    OpcUa_PfnInternalThreadMain * pEntry;
    OpcUa_Void                  * pArgs;
    };

/*============================================================================
 * Thread Procedure
 *===========================================================================*/
static int ThreadProc(IThread *pThread, void *pParam, UINT uParam)
{
    SetThreadName("OpcUa.Utils");

    CThreadInfo *pInfo = (CThreadInfo *) pParam;

    (*pInfo->pEntry)(pInfo->pArgs);

#if OPCUA_REQUIRE_OPENSSL
    OpcUa_P_OpenSSL_Thread_Cleanup();
#endif

    ExitThread(0);

    return OpcUa_Null;
}

/*============================================================================
 * Create Raw Thread
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Thread_Create(OpcUa_RawThread* pRawThread)
{
    CThreadInfo *pInfo = (CThreadInfo *) OpcUa_P_Memory_Alloc(sizeof(CThreadInfo));

    pInfo->pThread = NULL;
    pInfo->pEntry  = NULL;
    pInfo->pArgs   = NULL;

    *pRawThread = pInfo;

    return OpcUa_Good;
}

/*============================================================================
 * Delete Raw Thread
 *===========================================================================*/
clink OpcUa_Void OPCUA_DLLCALL OpcUa_P_Thread_Delete(OpcUa_RawThread* pRawThread)
{
    if( pRawThread == OpcUa_Null || *pRawThread == OpcUa_Null )
    {
        return;
    }
    else
    {
        CThreadInfo *pInfo = (CThreadInfo *) *pRawThread;

	if( pInfo->pThread )
	{
	    pInfo->pThread->Wait(FOREVER);

	    pInfo->pThread->Release();
	}

        pInfo->pThread = NULL;
        pInfo->pEntry  = NULL;
        pInfo->pArgs   = NULL;

	OpcUa_P_Memory_Free(pInfo);
    
	*pRawThread = OpcUa_Null;
    }
}

/*============================================================================
 * Start Thread
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Thread_Start( OpcUa_RawThread             pThread,
                                                           OpcUa_PfnInternalThreadMain pfnStartFunction,
                                                           OpcUa_Void*                 pArguments)
{
    if( pThread == OpcUa_Null )
    {
        return OpcUa_BadInvalidArgument;
    }

    CThreadInfo *pInfo = (CThreadInfo *) pThread;

    pInfo->pEntry  = pfnStartFunction;
    pInfo->pArgs   = pArguments;
    pInfo->pThread = CreateThread(ThreadProc, 2264, pInfo, 0);

    return OpcUa_Good;
}

/*============================================================================
 * Send the Thread to Sleep
 *===========================================================================*/
OpcUa_Void OPCUA_DLLCALL OpcUa_P_Thread_Sleep(OpcUa_UInt32 msecTimeout)
{
    Sleep(msecTimeout);
}

/*============================================================================
 * Get Current Thread Id
 *===========================================================================*/
OpcUa_UInt32 OPCUA_DLLCALL OpcUa_P_Thread_GetCurrentThreadId(void)
{
    return GetThreadIndex();
}
