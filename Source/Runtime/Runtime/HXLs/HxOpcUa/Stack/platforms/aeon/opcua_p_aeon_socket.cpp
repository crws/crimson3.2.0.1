/* ========================================================================
 * Copyright (c) 2005-2018 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

/******************************************************************************************************/
/** @file platform indepented implementation of the socketmanager/socket architecture                 */
/******************************************************************************************************/

/* System Headers */
#include <StdEnv.hpp>

/* File Header */
AfxFileHeader();

/* UA platform definitions */
#include <opcua_p_internal.h>

/* UA platform definitions */
#include <opcua_datetime.h>

#include <opcua_p_semaphore.h>
#include <opcua_p_thread.h>
#include <opcua_p_mutex.h>
#include <opcua_p_utilities.h>
#include <opcua_p_memory.h>

/* own headers */
#include <opcua_p_aeon_socket.h>

/* Aeon Socket Manager */
#include "AeonSocketManager.hpp"

/*============================================================================
 * Initialize the platform network interface
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_InitializeNetwork(void)
{
    return OpcUa_Good;
}

/*============================================================================
 * Clean the platform network interface up.
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_CleanupNetwork(void)
{
    return OpcUa_Good;
}

/*============================================================================
 * Convert OpcUa_StringA into binary ip address
 *===========================================================================*/
clink OpcUa_UInt32 OPCUA_DLLCALL OpcUa_P_Socket_InetAddr(OpcUa_StringA a_sRemoteAddress)
{
    AfxAssert(FALSE);
    return 0;
}

/*============================================================================
 * Get the name of the local host.
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_GetHostName( OpcUa_CharA* a_pBuffer,
                                                                 OpcUa_UInt32 a_uiBufferLength)
{
    AfxAssert(FALSE);
    return OpcUa_Good;
}

/*============================================================================
 * Get last socket error
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_GetLastError(OpcUa_Socket a_pSocket)
{
    AfxAssert(FALSE);
    return OpcUa_Good;
}

/*============================================================================
 * Read Socket.
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_Read( OpcUa_Socket    a_pSocket,
                                                          OpcUa_Byte*     a_pBuffer,
                                                          OpcUa_UInt32    a_uBufferSize,
                                                          OpcUa_UInt32*   a_pBytesRead)
{
    CAeonSocketRecord *pSocket = (CAeonSocketRecord *) a_pSocket;

    return pSocket->m_pManager->SockRead( pSocket->m_uIndex,
					  a_pBuffer,
					  a_uBufferSize,
					  a_pBytesRead
					  );
}

/*============================================================================
 * Write Socket.
 *===========================================================================*/
clink OpcUa_Int32 OPCUA_DLLCALL OpcUa_P_Socket_Write( OpcUa_Socket    a_pSocket,
                                                      OpcUa_Byte*     a_pBuffer,
                                                      OpcUa_UInt32    a_uBufferSize,
                                                      OpcUa_Boolean   a_bBlock)
{
    CAeonSocketRecord *pSocket = (CAeonSocketRecord *) a_pSocket;

    return pSocket->m_pManager->SockWrite( pSocket->m_uIndex,
					   a_pBuffer,
					   a_uBufferSize
					   );
}

/*============================================================================
 * Get IP Address and Port Number of the Peer
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_GetPeerInfo( OpcUa_Socket a_pSocket,
                                                                 OpcUa_CharA* a_achPeerInfoBuffer,
                                                                 OpcUa_UInt32 a_uiPeerInfoBufferSize)
{
    CAeonSocketRecord *pSocket = (CAeonSocketRecord *) a_pSocket;

    return pSocket->m_pManager->SockGetPeerInfo( pSocket->m_uIndex,
					         a_achPeerInfoBuffer,
					         a_uiPeerInfoBufferSize
					         );
}

/*============================================================================
 * Set Socket User Data
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_SetUserData( OpcUa_Socket a_pSocket,
                                                                 OpcUa_Void*  a_pvUserData)
{
    CAeonSocketRecord *pSocket = (CAeonSocketRecord *) a_pSocket;

    return pSocket->m_pManager->SockSetUserData( pSocket->m_uIndex,
					         a_pvUserData
					         );
}

/*============================================================================
 * Close Socket.
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Socket_Close(OpcUa_Socket a_pSocket)
{
    CAeonSocketRecord *pSocket = (CAeonSocketRecord *) a_pSocket;

    OpcUa_StatusCode uStatus = pSocket->m_pManager->SockClose(pSocket->m_uIndex);

    return uStatus;
}

/*============================================================================
 * Network Byte Order Conversion Helper Functions
 *===========================================================================*/
clink OpcUa_UInt32 OPCUA_DLLCALL OpcUa_P_Socket_NToHL(OpcUa_UInt32 a_netLong)
{
    return NetToHost(DWORD(a_netLong));
}

clink OpcUa_UInt16 OPCUA_DLLCALL OpcUa_P_Socket_NToHS(OpcUa_UInt16 a_netShort)
{
    return NetToHost(WORD(a_netShort));
}

clink OpcUa_UInt32 OPCUA_DLLCALL OpcUa_P_Socket_HToNL(OpcUa_UInt32 a_hstLong)
{
    return HostToNet(DWORD(a_hstLong));
}

clink OpcUa_UInt16 OPCUA_DLLCALL OpcUa_P_Socket_HToNS(OpcUa_UInt16 a_hstShort)
{
    return NetToHost(WORD(a_hstShort));
}
