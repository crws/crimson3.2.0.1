/* ========================================================================
 * Copyright (c) 2005-2018 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

/******************************************************************************************************/
/* Platform Portability Layer                                                                         */
/* Modify the content of this file according to the mutex implementation on your system.              */
/******************************************************************************************************/

/* System Headers */

#include <StdEnv.hpp>

/* UA platform definitions */
#include <opcua_p_internal.h>
#include <opcua_p_memory.h>

/* own headers */
#include <opcua_mutex.h>
#include <opcua_p_mutex.h>

#if OPCUA_MUTEX_ERROR_CHECKING
#include <opcua_p_thread.h>
#endif


/*============================================================================
 * Allocate the mutex.
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Mutex_CreateImp(OpcUa_Mutex* phMutex)
{
    IMutex *pMutex = Create_Mutex();

    *phMutex = (OpcUa_Mutex) pMutex;

    return OpcUa_Good;
}

/*============================================================================
 * Clear and free the mutex.
 *===========================================================================*/
clink OpcUa_Void OPCUA_DLLCALL OpcUa_P_Mutex_DeleteImp(OpcUa_Mutex* phMutex)
{
    IMutex *pMutex = (IMutex *) *phMutex;

    pMutex->Release();

    *phMutex = OpcUa_Null;
}

/*============================================================================
 * Lock the mutex.
 *===========================================================================*/
clink OpcUa_Void OPCUA_DLLCALL OpcUa_P_Mutex_LockImp(OpcUa_Mutex hMutex)
{
    IMutex *pMutex = (IMutex *) hMutex;

    pMutex->Wait(FOREVER);
}

/*============================================================================
 * Unlock the mutex.
 *===========================================================================*/
clink OpcUa_Void OPCUA_DLLCALL OpcUa_P_Mutex_UnlockImp(OpcUa_Mutex hMutex)
{
    IMutex *pMutex = (IMutex *) hMutex;

    pMutex->Free();

    return;
}
