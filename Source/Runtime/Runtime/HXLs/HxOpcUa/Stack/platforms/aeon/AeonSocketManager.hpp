
#include "../../Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AeonSocketManager_HPP

#define INCLUDE_AeonSocketManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Stack Headers
//

#include <opcua_p_internal.h>
#include <opcua_datetime.h>
#include <opcua_p_semaphore.h>
#include <opcua_p_thread.h>
#include <opcua_p_mutex.h>
#include <opcua_p_utilities.h>
#include <opcua_p_memory.h>

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CAeonSocketRecord;

class CAeonSocketManager;

clink OpcUa_StatusCode OpcUa_P_SocketManager_Suspend(void);

//////////////////////////////////////////////////////////////////////////
//
// Aeon Socket Record
//

class CAeonSocketRecord
{
	public:
		CAeonSocketManager         * m_pManager;
		UINT                         m_uIndex;
		int			     m_fUsed;
		int                          m_fActive;
		UINT			     m_uType;
		UINT			     m_uParent;
		OpcUa_Socket_EventCallback   m_pfnSocketCallBack;
		OpcUa_Void                 * m_pCallbackData;
		ISocket                    * m_pSocket;
		BOOL			     m_fOpened;
		BOOL			     m_fClosed;
		IThread                    * m_pThread;
		BOOL			     m_fWrite;
		CIpAddr			     m_PeerAddr;
		WORD			     m_PeerPort;
	};

//////////////////////////////////////////////////////////////////////////
//
// Aeon Socket Manager
//

class CAeonSocketManager : public IClientProcess
{
	public:
		// Constructor
		CAeonSocketManager(int nCount, UINT uFlags);

		// Destructor
		~CAeonSocketManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// Management
		BOOL Open(void);
		BOOL Close(void);

		// Server Creation
		UINT CreateServer( OpcUa_StringA               sAddress,
				   OpcUa_Boolean               fListenOnAllInterfaces,
				   OpcUa_Socket_EventCallback  pfnSocketCallBack,
				   OpcUa_Void   *              pCallbackData,
				   OpcUa_Socket *              pSocketRecord
				   );

		// Client Creation
		UINT CreateClient( OpcUa_StringA               sRemoteAddress,
				   OpcUa_UInt16                uLocalPort,
				   OpcUa_Socket_EventCallback  pfnSocketCallBack,
				   OpcUa_Void   *              pCallbackData,
				   OpcUa_Socket *              pSocketRecord
				   );
		
		// Socket Operations
		UINT SockRead(UINT uIndex, OpcUa_Byte *pBuffer, OpcUa_UInt32 uBufferSize, OpcUa_UInt32 *pBytesRead);
		UINT SockWrite(UINT uIndex, OpcUa_Byte *pBuffer, OpcUa_UInt32 uBufferSize);
		UINT SockGetPeerInfo(UINT uIndex, OpcUa_CharA *pBuffer, OpcUa_UInt32 uBufferSize);
		UINT SockSetUserData(UINT uIndex, OpcUa_Void *pData);
		UINT SockClose(UINT uIndex);

	protected:
		// Static Data
		static DWORD m_dwUsed;

		// Socket Types
		enum
		{
			typeListen = 1,
			typeServer = 2,
			typeClient = 3
			};

		// Activity Codes
		enum
		{
			codeIdle = 1,
			codeBusy = 2,
			codeDone = 3,
			};

		// Data Members
		ULONG		    m_uRefs;
		int                 m_nCount;
		UINT                m_uFlags;
		UINT		    m_uPort;
		BOOL		    m_fMulti;
		DWORD		    m_dwLevel;
		UINT		    m_uSockLevel;
		IThread           * m_pThread;
		CAeonSocketRecord * m_pList;
		IDnsResolver      * m_pDns;
		INT volatile	    m_nBusy;

		// Implementation
		UINT RunEntry(UINT uSlot);
		UINT RunListen(UINT uSlot, CAeonSocketRecord *pSocket);
		UINT RunServer(UINT uSlot, CAeonSocketRecord *pSocket);
		UINT RunClient(UINT uSlot, CAeonSocketRecord *pSocket);
		void TidySocket(CAeonSocketRecord *pSocket);
		void TidyChildren(UINT uSlot);
		void Notify(CAeonSocketRecord *pSocket, UINT uCode);
		BOOL FindResolver(void);
		BOOL ResolveName(CArray <CIpAddr> &List, PCTXT pName);

		// Friends
		friend OpcUa_StatusCode OpcUa_P_SocketManager_Suspend(void);
	};

// End of File

#endif
