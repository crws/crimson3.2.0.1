/* ========================================================================
 * Copyright (c) 2005-2018 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

/******************************************************************************************************/
/* Platform Portability Layer                                                                         */
/* Modify the content of this file according to the semaphore implementation on your system.          */
/******************************************************************************************************/

#include <StdEnv.hpp>

/* UA platform definitions */
#include <opcua_p_internal.h>

/* additional UA dependencies */
#include <opcua_datetime.h>
#include <opcua_utilities.h>

/* own headers */
#include <opcua_semaphore.h>
#include <opcua_p_semaphore.h>

/*============================================================================
 * Create a Semaphore
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Semaphore_Create( OpcUa_Semaphore* a_Semaphore,
                                                               OpcUa_UInt32     a_uInitalValue,
                                                               OpcUa_UInt32     a_uMaxRange)
{
    ISemaphore *pSemaphore = Create_Semaphore();

    pSemaphore->Signal(a_uInitalValue);

    *a_Semaphore = (OpcUa_Semaphore) pSemaphore;

    return OpcUa_Good;
}

/*============================================================================
 * Delete the semaphore.
 *===========================================================================*/
clink OpcUa_Void OPCUA_DLLCALL OpcUa_P_Semaphore_Delete(OpcUa_Semaphore* pRawSemaphore)
{
    ISemaphore *pSemaphore = (ISemaphore *) *pRawSemaphore;

    pSemaphore->Release();

    *pRawSemaphore = NULL;
}

/*============================================================================
 * Wait
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Semaphore_Wait(OpcUa_Semaphore RawSemaphore)
{
    ISemaphore *pSemaphore = (ISemaphore *) RawSemaphore;

    pSemaphore->Wait(FOREVER);

    return OpcUa_Good;
}

/*============================================================================
 * Wait for a maximum of seconds.
 *===========================================================================*/
OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Semaphore_TimedWait( OpcUa_Semaphore a_RawSemaphore,
                                                            OpcUa_UInt32    a_msecTimeout)
{
    ISemaphore *pSemaphore = (ISemaphore *) a_RawSemaphore;

    if(pSemaphore->Wait(a_msecTimeout))
        return OpcUa_Good;

    return OpcUa_GoodNonCriticalTimeout;
}

/*============================================================================
 * Post (free)
 *===========================================================================*/
OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_Semaphore_Post(  OpcUa_Semaphore RawSemaphore,
                                                        OpcUa_UInt32    uReleaseCount)
{
    ISemaphore *pSemaphore = (ISemaphore *) RawSemaphore;

    pSemaphore->Signal(uReleaseCount);

    return OpcUa_Good;
}
