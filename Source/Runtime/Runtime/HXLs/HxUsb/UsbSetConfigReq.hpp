
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSetConfigReq_HPP

#define	INCLUDE_UsbSetConfigReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Config Standard Device Requeset
//

class CUsbSetConfigReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbSetConfigReq(WORD wConfig);

		// Operations
		void Init(WORD wConfig);
	};

// End of File

#endif
