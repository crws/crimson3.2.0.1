
#include "Intern.hpp"  

#include "UsbFuncStack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbFuncDriver.hpp"

#include "UsbFuncCompDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IUsbDriver * Create_UsbFuncDriver(UINT uPriority);

extern IUsbDriver * Create_UsbCompDriver(void);

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Stack
//

// Instantiator

IUsbFuncStack * Create_UsbFuncStack(UINT uPriority)
{
	CUsbFuncStack *p = New CUsbFuncStack(uPriority);

	return p;
	}

// Constructor

CUsbFuncStack::CUsbFuncStack(UINT uPriority)
{
	StdSetRef();
	
	m_pCompDrv = (IUsbFuncCompDriver *) Create_UsbCompDriver(); 
		
	m_pFuncDrv = (IUsbFuncDriver     *) Create_UsbFuncDriver(uPriority);

	m_pHwDrv   = NULL;

	SetVendor (0x1037, "Red Lion Controls");
				
	SetProduct(0x0001, "Tacoma Tester");
	}

// Destructor

CUsbFuncStack::~CUsbFuncStack(void)
{
	m_pCompDrv->Release();

	m_pFuncDrv->Release();
	}

// IUnknown

HRESULT CUsbFuncStack::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbFuncStack);

	StdQueryInterface(IUsbFuncStack);

	return E_NOINTERFACE;
	}

ULONG CUsbFuncStack::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbFuncStack::Release(void)
{
	StdRelease();
	}

// IUsbFuncStack

void CUsbFuncStack::Bind(IUsbFuncHardwareDriver *pDriver)
{
	if( !m_pHwDrv && pDriver ) {

		m_pHwDrv = pDriver;

		m_pHwDrv  ->Bind(m_pFuncDrv);

		m_pFuncDrv->Bind(m_pCompDrv);
		}
	}

void CUsbFuncStack::Bind(IUsbFuncEvents *pAppDriver)
{
	m_pCompDrv->Bind(pAppDriver);
	}

void CUsbFuncStack::SetVendor(WORD wId, PCTXT pName)
{
	m_pCompDrv->SetVendor(wId, pName);
	}

void CUsbFuncStack::SetProduct(WORD wId, PCTXT pName)
{
	m_pCompDrv->SetProduct(wId, pName);
	}

void CUsbFuncStack::SetSerialNumber(PCTXT pText)
{
	m_pCompDrv->SetSerialNumber(pText);
	}

BOOL CUsbFuncStack::Start(void)
{
	if( m_pCompDrv->Init() && m_pCompDrv->Start() ) {

		return true;
		}

	return false;
	}

BOOL CUsbFuncStack::Stop(void)
{
	return m_pCompDrv->Stop();
	}

// End of File
