
#include "Intern.hpp"

#include "CdcNtbSize.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// NTB Size
//

// Constructor

CCdcNtbSize::CCdcNtbSize(void)
{
	}

// Endianess

void CCdcNtbSize::HostToCdc(void)
{
	m_dwMaxSize = HostToIntel(m_dwMaxSize);
	m_wMaxCount = HostToIntel(m_wMaxCount);
	}

void CCdcNtbSize::CdcToHost(void)
{
	m_dwMaxSize = IntelToHost(m_dwMaxSize);
	m_wMaxCount = IntelToHost(m_wMaxCount);
	}

// Operations

void CCdcNtbSize::Init(void)
{
	memset(this, 0, sizeof(CdcNtbSize));
	}

// End of File
