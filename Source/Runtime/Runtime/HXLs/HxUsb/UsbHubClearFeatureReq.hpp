
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubClearFeatureReq_HPP

#define	INCLUDE_UsbHubClearFeatureReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Clear Feature Request
//

class CUsbHubClearFeatureReq : public CUsbClassReq
{
	public:
		// Constructor
		CUsbHubClearFeatureReq(WORD wFeature);

		// Operations
		void Init(WORD wFeature);
	};

// End of File

#endif
