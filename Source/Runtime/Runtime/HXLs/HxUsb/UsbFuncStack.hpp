
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbFuncStack_HPP

#define	INCLUDE_UsbFuncStack_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Stack
//

class CUsbFuncStack : public IUsbFuncStack
{
	public:
		// Constructor
		CUsbFuncStack(UINT uPriority);

		// Destructor
		~CUsbFuncStack(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbFuncStack
		void METHOD Bind(IUsbFuncHardwareDriver *pHardware);
		void METHOD Bind(IUsbFuncEvents *pAppDriver);
		void METHOD SetVendor(WORD wId, PCTXT pName);
		void METHOD SetProduct(WORD wId, PCTXT pName);
		void METHOD SetSerialNumber(PCTXT pText);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);

	protected:
		// Data
		ULONG		         m_uRefs;
		IUsbFuncHardwareDriver * m_pHwDrv;
		IUsbFuncDriver         * m_pFuncDrv;
		IUsbFuncCompDriver     * m_pCompDrv;
	};

// End of File

#endif
