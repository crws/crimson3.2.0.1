
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbClearFeatureReq_HPP

#define	INCLUDE_UsbClearFeatureReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Clear Feature Device Requeset
//

class CUsbClearFeatureReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbClearFeatureReq(void);

		// Operations
		void Init        (void);
		void SetDevice   (WORD wFeature);
		void SetInterface(WORD wFeature, WORD wInterface);
		void SetEndpoint (WORD wFeature, WORD wEndpoint);
	};

// End of File

#endif
