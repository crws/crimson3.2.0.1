
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbFuncLoopback_HPP

#define	INCLUDE_UsbFuncLoopback_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbFuncAppDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Loopback Function Driver
//

class CUsbFuncLoopback : public CUsbFuncAppDriver
{
	public:
		// Constructor
		CUsbFuncLoopback(UINT uPriority);

		// Destructor
		~CUsbFuncLoopback(void);

		// IUsbEvents
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);

		// IUsbFuncEvents
		BOOL METHOD OnGetInterface(UsbDesc &Desc);
		BOOL METHOD OnGetEndpoint(UsbDesc &Desc);

	protected:
		// Data
		UINT      m_iEndptSend;
		UINT      m_iEndptRecv;
		BYTE      m_bData[2048];
		IThread * m_pThread;

		// Task Entry
		void TaskEntry(void);

		// Friends
		static int TaskUsbLoopback(IThread *pThread, void *pParam, UINT uParam); 
	};

// End of File

#endif
