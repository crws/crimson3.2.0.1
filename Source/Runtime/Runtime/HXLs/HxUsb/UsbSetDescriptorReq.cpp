
#include "Intern.hpp"

#include "UsbSetDescriptorReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Descriptor Standard Device Request
//

// Constructor

CUsbSetDescriptorReq::CUsbSetDescriptorReq(void)
{
	Init();
	} 

// Operations

void CUsbSetDescriptorReq::Init(void)
{
	CUsbGetDescriptorReq::Init();

	m_bRequest  = reqSetDesc;

	m_Direction = dirHostToDev;
	}

// End of File
