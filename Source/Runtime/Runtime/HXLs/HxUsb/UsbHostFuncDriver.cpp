
#include "Intern.hpp"

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Function Driver 
//

// Constructor

CUsbHostFuncDriver::CUsbHostFuncDriver(void)
{
	m_uState = usbInit;

	m_pDev   = NULL;

	m_pSink  = NULL;

	m_pName  = "Host Func Driver";
	}

// IUnknown

HRESULT CUsbHostFuncDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHostFuncDriver);

	StdQueryInterface(IUsbHostFuncDriver);

	return E_NOINTERFACE;
	}

ULONG CUsbHostFuncDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHostFuncDriver::Release(void)
{
	StdRelease();
	}

// IUsbHostFuncDriver

void CUsbHostFuncDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	m_pDev = pDevice;

	m_iInt = iInterface;
	}

void CUsbHostFuncDriver::Bind(IUsbHostFuncEvents *pSink)
{
	m_pSink = pSink;
	}

void CUsbHostFuncDriver::GetDevice(IUsbDevice *&pDev)
{
	pDev = m_pDev;
	}

BOOL CUsbHostFuncDriver::SetRemoveLock(BOOL fLock)
{
	if( fLock ) {

		Hal_Critical(true);	

		if( m_uState == usbOpen ) {

			if( m_pDev->SetRemoveLock(true) ) {

				Hal_Critical(false);

				return true;
				}
			}

		Hal_Critical(false);

		return false;
		}
		
	return m_pDev->SetRemoveLock(false);
	}

UINT CUsbHostFuncDriver::GetVendor(void)
{
	return m_pDev->GetDevDesc().m_wVendorID;
	}

UINT CUsbHostFuncDriver::GetProduct(void)
{
	return m_pDev->GetDevDesc().m_wProductID;
	}

UINT CUsbHostFuncDriver::GetClass(void)
{
	UINT Class = m_pDev->GetDevDesc().m_bClass;

	if( !Class ) {

		UINT i;

		UsbInterfaceDesc *p = (UsbInterfaceDesc *) m_pDev->GetCfgDesc().FindInterface(i, m_iInt);

		if( p ) {

			Class = p->m_bClass;
			}
		}
	
	return Class;	
	}

UINT CUsbHostFuncDriver::GetSubClass(void)
{
	if( !m_pDev->GetDevDesc().m_bClass ) {

		UINT i;

		UsbInterfaceDesc *p = (UsbInterfaceDesc *) m_pDev->GetCfgDesc().FindInterface(i, m_iInt);

		return p ? p->m_bSubClass : 0;
		}
	
	return m_pDev->GetDevDesc().m_bSubClass;	
	}

UINT CUsbHostFuncDriver::GetProtocol(void)
{
	if( !m_pDev->GetDevDesc().m_bClass ) {

		UINT i;

		UsbInterfaceDesc *p = (UsbInterfaceDesc *) m_pDev->GetCfgDesc().FindInterface(i, m_iInt);

		return p ? p->m_bProtocol : 0;
		}
	
	return m_pDev->GetDevDesc().m_bProtocol;	
	}

UINT CUsbHostFuncDriver::GetInterface(void)
{
	return m_iInt;
	}

BOOL CUsbHostFuncDriver::GetActive(void)
{
	return m_uState == usbOpen;
	}

BOOL CUsbHostFuncDriver::Open(CUsbDescList const &Config)
{
	return Open();
	}

BOOL CUsbHostFuncDriver::Close(void)
{
	if( m_uState != usbClosed ) {

		m_uState = usbClosed;

		if( m_pSink ) {

			m_pSink->OnTerm();
			}
		}

	return TRUE;
	}

void CUsbHostFuncDriver::Poll(UINT uLapsed)
{
	if( m_uState == usbOpen ) {

		if( m_pSink ) {

			m_pSink->OnPoll(uLapsed);
			}
		}
	}

// Implementation

BOOL CUsbHostFuncDriver::Open(void)
{
	if( m_pDev ) {

		m_uState = usbOpen; 

		return TRUE;
		}

	return FALSE;
	}

// End of File
