
#include "Intern.hpp"  

#include "UsbHostStack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbSetAddressReq.hpp"

#include "UsbGetDescriptorReq.hpp"

#include "UsbSetConfigReq.hpp"

#include "UsbClearFeatureReq.hpp"

#include "UsbGetConfigReq.hpp"

#include "UsbGetStatusReq.hpp"

#include "UsbSetFeatureReq.hpp"

#include "UsbSetInterfaceReq.hpp"

#include "UsbGetInterfaceReq.hpp"

#include "UsbHubGetDescReq.hpp"

#include "UsbHubDesc.hpp"

#include "UsbStringDesc.hpp"

#include "UsbDescList.hpp"

#include "UsbDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Stack
//

// Instantiator

IUsbHostStack * Create_UsbHostStack(UINT uPriority)
{
	CUsbHostStack *p = New CUsbHostStack(uPriority);

	return p;
	}

// Constructor

CUsbHostStack::CUsbHostStack(UINT uPriority)
{
	StdSetRef();

	m_pRun    = Create_AutoEvent();

	m_pStop   = Create_AutoEvent();

	m_pDone   = Create_ManualEvent();

	m_pPoll	  = Create_AutoEvent();

	m_pThread = CreateThread(TaskUsbHostStack, uPriority, this, 0);

	m_pTimer  = CreateTimer();

	ResetCounters();
	
	InitSink();

	InitControllerList();

	DiagRegister();
	}

// Destructor

CUsbHostStack::~CUsbHostStack(void)
{
	DiagRevoke();

	FreeSink();

	m_pThread->Destroy();

	m_pTimer->Release();

	m_pRun->Release();

	m_pStop->Release();

	m_pDone->Release();

	m_pPoll->Release();
	}

// IUnknown

HRESULT CUsbHostStack::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHostStack);

	StdQueryInterface(IUsbHostStack);

	StdQueryInterface(IUsbHostControllerEvents);

	StdQueryInterface(IUsbEvents);

	StdQueryInterface(IUsbHubEvents);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CUsbHostStack::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHostStack::Release(void)
{
	StdRelease();
	}

// IUsbHostStack

BOOL CUsbHostStack::Init(void)
{
	InitUrbs();

	return InitControllers();
	}

BOOL CUsbHostStack::Start(void)
{
	InitUrbs();	
	
	if( StartControllers() ) {

		m_pStop->Clear();

		m_pRun->Set();
		
		return true;
		}

	return false;
	}

BOOL CUsbHostStack::Stop(void) 
{
	m_pRun->Clear();

	m_pDone->Clear();
	
	m_pStop->Set();
	
	m_pDone->Wait(FOREVER);

	RemoveDevices();

	StopControllers();

	KillDevices();

	return true;
	}

BOOL CUsbHostStack::Attach(IUsbSystem *pSink)
{
	for( UINT i = 0; i < elements(m_pSink); i ++ ) {

		if( !m_pSink[i] ) {
				
			m_pSink[i] = pSink;
			
			m_pSink[i]->AddRef();
		
			return true;
			}
		}

	return false;
	}

IUsbHostControllerDriver * CUsbHostStack::GetHostController(UINT iCtrl)
{
	if( iCtrl < elements(m_pCtrl) ) {

		return m_pCtrl[iCtrl];
		}

	return NULL;
	}

IUsbHostInterfaceDriver * CUsbHostStack::GetHostInterface(UINT iCtrl, UINT iHost)
{
	IUsbHostControllerDriver *pCtrl = GetHostController(iCtrl);

	return pCtrl ? pCtrl->GetInterface(iHost) : NULL;
	}

IUsbDevice * CUsbHostStack::GetDevice(UsbDevPath const &Path)
{
	for( CUsbDevice *p = m_DevList.GetHead(); p; p = m_DevList.GetNext(p) ) {

		if( !p->IsRemoved() ) {

			if( Path.a.dwAddr == p->GetAddr() ) {

				UsbPortPath const &Port = p->GetPortPath();
			
				if( Path.a.dwCtrl == Port.a.dwCtrl && Path.a.dwHost == Port.a.dwHost ) {

					return p;
					}
				}
			}
		}

	return NULL;
	}

IUsbDevice * CUsbHostStack::GetDevice(UsbPortPath const &Path)
{
	for( CUsbDevice *p = m_DevList.GetHead(); p; p = m_DevList.GetNext(p) ) {

		if( Path.dw == p->GetPortPath().dw ) {

			if( !p->IsRemoved() ) {

				return p;
				}
			}
		}

	return NULL;
	}

IUsbDevice * CUsbHostStack::GetDevice(UsbTreePath const &Path)
{
	for( CUsbDevice *p = m_DevList.GetHead(); p; p = m_DevList.GetNext(p) ) {

		if( Path.dw == p->GetTreePath().dw ) {

			if( !p->IsRemoved() ) {

				return p;
				}
			}
		}

	return NULL;
	}

IUsbHostFuncDriver * CUsbHostStack::GetDriver(UsbDevPath const &Path)
{
	IUsbDevice *pDev = GetDevice(Path);

	return pDev ? pDev->GetDriver(Path.a.dwInterface) : NULL;
	}

IUsbPipe * CUsbHostStack::GetPipe(UsbDevPath const &Path)
{
	IUsbDevice *pDev = GetDevice(Path);

	return pDev ? pDev->GetPipe(Path.a.dwPipe, Path.a.dwDirIn) : NULL;
	}

UINT CUsbHostStack::GetPortSpeed(UsbPortPath const &Path)
{
	if( Path.a.dwHubAddr ) {

		UsbDevPath DevPath = { { Path.a.dwHubAddr, Path.a.dwHost, Path.a.dwCtrl, 0, 0, 0 } };

		IUsbDevice *pDev = GetDevice(DevPath);

		if( pDev ) {

			IUsbHostFuncDriver *pDrv = pDev->GetDriver(0);

			if( pDrv && pDrv->GetClass() == devHub ) {

				return ((IUsbHubDriver *) pDrv)->GetSpeed(Path.a.dwHubPort);
				}
			}
		
		return 0;
		}
	else {
		IUsbHostInterfaceDriver *p = GetHostInterface(Path.a.dwCtrl, Path.a.dwHost);

		return p->GetPortSpeed(Path.a.dwPort);
		}
	}

BOOL CUsbHostStack::ResetPort(UsbPortPath const &Path)
{
	if( Path.a.dwHubAddr ) {

		UsbDevPath DevPath = { { Path.a.dwHubAddr, Path.a.dwHost, Path.a.dwCtrl, 0, 0, 0 } };

		IUsbDevice *pDev = GetDevice(DevPath);

		if( pDev ) {

			IUsbHostFuncDriver *pDrv = pDev->GetDriver(0);

			if( pDrv && pDrv->GetClass() == devHub ) {

				return ((IUsbHubDriver *) pDrv)->ResetPort(Path.a.dwHubPort);
				}
			}

		AfxTrace("CUsbHostStack::ResetPort - Hub not found (Hub=%d, Port=%d)\n", Path.a.dwHubAddr, Path.a.dwHubPort);
		}
	else {
		IUsbHostControllerDriver *p = GetHostController(Path.a.dwCtrl);

		if( p ) {

			return p->ResetPort(Path.a.dwPort);
			}

		AfxTrace("CUsbHostStack::ResetPort - Port not found (Ctrl=%d, Port=%d)\n", Path.a.dwCtrl, Path.a.dwPort);
		}

	return false;
	}

BOOL CUsbHostStack::ResetPort(UsbTreePath const &Path)
{
	IUsbSystemPortMapper *pMapper = NULL;

	AfxGetObject("usbport", 0, IUsbSystemPortMapper, pMapper);

	if( pMapper ) {

		UINT iGpio = pMapper->GetPortReset(Path);

		UINT uObj  = HIBYTE(iGpio);

		UINT uPin  = LOBYTE(iGpio);

		if( iGpio != NOTHING ) {

			IGpio *pGpio;

			AfxGetObject("gpio", uObj, IGpio, pGpio);

			if( pGpio ) {

				pGpio->SetState(uPin, !pGpio->GetState(uPin));

				Sleep(5);
			
				pGpio->SetState(uPin, !pGpio->GetState(uPin));
				
				pGpio->Release();

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CUsbHostStack::SetAddress(IUsbPipe *pPipe, BYTE bAddr)
{
	CUsbSetAddressReq Req(bAddr);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::ClearDevFeature(IUsbPipe *pPipe, WORD wSelect)
{
	CUsbClearFeatureReq Req;

	Req.SetDevice(wSelect);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::ClearIntFeature(IUsbPipe *pPipe, WORD wSelect, WORD wInterface)
{
	CUsbClearFeatureReq Req;

	Req.SetInterface(wSelect, wInterface);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::ClearEndFeature(IUsbPipe *pPipe, WORD wSelect, WORD wEndpoint)
{
	CUsbClearFeatureReq Req;

	Req.SetEndpoint(wSelect, wEndpoint);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::SetDevFeature(IUsbPipe *pPipe, WORD wSelect)
{
	CUsbSetFeatureReq Req;

	Req.SetDevice(wSelect);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::SetIntFeature(IUsbPipe *pPipe, WORD wSelect, WORD wInterface)
{
	CUsbSetFeatureReq Req;

	Req.SetInterface(wSelect, wInterface);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::SetEndFeature(IUsbPipe *pPipe, WORD wSelect, WORD wEndpoint)
{
	CUsbSetFeatureReq Req;

	Req.SetEndpoint(wSelect, wEndpoint);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::GetConfig(IUsbPipe *pPipe, BYTE &bConfig)
{
	CUsbGetConfigReq Req;

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req, &bConfig, sizeof(bConfig)) == sizeof(bConfig);
	}

BOOL CUsbHostStack::SetConfig(IUsbPipe *pPipe, BYTE bConfig)
{
	CUsbSetConfigReq Req(bConfig);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::GetInterface(IUsbPipe *pPipe, WORD wInt, BYTE &bAlt)
{
	CUsbGetInterfaceReq Req(wInt);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req, &bAlt, sizeof(bAlt)) == sizeof(bAlt);
	}

BOOL CUsbHostStack::SetInterface(IUsbPipe *pPipe, WORD wInt, BYTE bAlt)
{
	CUsbSetInterfaceReq Req(bAlt, wInt);

	Req.HostToUsb();

	return pPipe->CtrlTrans(Req);
	}

BOOL CUsbHostStack::GetDevStatus(IUsbPipe *pPipe, WORD &wStatus)
{
	CUsbGetStatusReq Req;

	Req.SetDevice();

	Req.HostToUsb();

	if( pPipe->CtrlTrans(Req, PBYTE(&wStatus), sizeof(wStatus)) == sizeof(wStatus) ) {

		wStatus = IntelToHost(wStatus);

		return true;
		}

	return false;
	}

BOOL CUsbHostStack::GetIntStatus(IUsbPipe *pPipe, WORD wInterface, WORD &wStatus)
{
	CUsbGetStatusReq Req;

	Req.SetInterface(wInterface);

	Req.HostToUsb();

	if( pPipe->CtrlTrans(Req, PBYTE(&wStatus), sizeof(wStatus)) == sizeof(wStatus) ) {

		wStatus = IntelToHost(wStatus);

		return true;
		}

	return false;
	}

BOOL CUsbHostStack::GetEndStatus(IUsbPipe *pPipe, WORD wEndpoint, WORD &wStatus)
{
	CUsbGetStatusReq Req;

	Req.SetEndpoint(wEndpoint);

	Req.HostToUsb();

	if( pPipe->CtrlTrans(Req, PBYTE(&wStatus), sizeof(wStatus)) == sizeof(wStatus) ) {

		wStatus = IntelToHost(wStatus);

		return true;
		}

	return false;
	}

BOOL CUsbHostStack::GetDevDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen)
{
	CUsbGetDescriptorReq Req;

	Req.SetDevice(uLen);

	Req.HostToUsb();

	if( pPipe->CtrlTrans(Req, pData, uLen) == uLen ) {
		
		if( uLen == sizeof(UsbDeviceDesc) ) {

			CUsbDeviceDesc &Desc = (CUsbDeviceDesc &) pData[0];

			Desc.UsbToHost();
				
			return Desc.IsValid();
			}

		return true;
		}

	return false;
	}

BOOL CUsbHostStack::GetConfigDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen)
{
	CUsbGetDescriptorReq Req;

	Req.SetConfig(0, uLen);

	Req.HostToUsb();

	if( pPipe->CtrlTrans(Req, pData, uLen) == uLen ) {

		if( uLen >= sizeof(UsbConfigDesc) ) {

			CUsbDescList List;

			List.Attach(pData, uLen);

			List.UsbToHost();

			return List.IsValid();
			}
		
		return true;
		}

	return false;
	}

BOOL CUsbHostStack::GetHubDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen)
{
	CUsbHubGetDescReq Req(0);

	Req.HostToUsb();

	if( pPipe->CtrlTrans(Req, pData, uLen) == uLen ) {

		if( uLen >= sizeof(UsbHubDesc) ) {

			CUsbHubDesc &Desc = (CUsbHubDesc &) pData[0];

			Desc.UsbToHost();

			return Desc.IsValid();
			}

		return true;
		}

	return false;
	}

BOOL CUsbHostStack::GetStringDesc(IUsbPipe *pPipe, UINT iIdx, PTXT pText, UINT uLen)
{
	CUsbGetDescriptorReq Req;

	UINT  uSize = sizeof(UsbDesc) + uLen * sizeof(WORD);

	PBYTE pBuff = PBYTE(alloca(uSize));
	
	Req.SetString(iIdx, uSize, 0);

	Req.HostToUsb();
	
	if( (uLen = pPipe->CtrlTrans(Req, pBuff, uSize)) != NOTHING ) {

		CUsbStringDesc &Desc = (CUsbStringDesc &) pBuff[0];

		if( Desc.IsValid() ) {

			Desc.UsbToHost();

			Desc.Debug();

			Desc.Get(pText, uLen);

			return true;
			}
		}

	return false;
	}

UsbIor * CUsbHostStack::AllocUrb(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	UINT uNext = (m_iFreeHead + 1) % elements(m_pUrbFree);

	if( uNext != m_iFreeTail ) {

		UsbIor *pUrb = m_pUrbFree[m_iFreeHead];
	
		pUrb->m_uStatus = UsbIor::statAlloc;

		pUrb->m_pParam  = NULL;

		pUrb->m_uParam  = 0;

		m_iFreeHead = uNext;

		#if defined(_DEBUG)	

		m_uDiagUrb ++;

		MakeMax(m_uDiagUrbMax, m_uDiagUrb);

		#endif
		
		Hal_LowerIrql(uSave);

		return pUrb;
		}

	AfxTrace("CUsbHostStack::AllocUrb - Failed\n");

	Hal_LowerIrql(uSave);

	return NULL;
	}

void CUsbHostStack::FreeUrb(UsbIor *pUrb)
{
	AfxAssert(pUrb->m_uStatus > UsbIor::statInit);

	pUrb->m_uStatus = UsbIor::statInit;

	if( pUrb->m_pParam && pUrb->m_uParam ) {

		AfxRelease((IUsbPipeEvents *) pUrb->m_uParam);

		pUrb->m_pParam = NULL;

		pUrb->m_uParam = NULL;
		}
		
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	m_pUrbFree[m_iFreeTail] = pUrb;

	m_iFreeTail = (m_iFreeTail + 1) % elements(m_pUrbFree);

	#if defined(_DEBUG)	

	m_uDiagCount += 1;
	
	m_uDiagData  += pUrb->m_uCount;

	m_uDiagUrb    = m_uDiagUrb ? m_uDiagUrb - 1 : m_uDiagUrb;

	#endif

	Hal_LowerIrql(uSave);
	}

void CUsbHostStack::FreeUrbs(IUsbPipe *pPipe)
{
	for( UINT i = 0; i < elements(m_Urbs); i ++ ) {

		UsbIor &Urb = m_Urbs[i];

		if( Urb.m_uStatus >= UsbIor::statAlloc ) {

			if( Urb.m_iEndpt == pPipe->GetEndpoint() ) {

				AfxTrace("Urb Resource Leak! Addr %d\n", pPipe->GetDevice()->GetAddr());

				if( Urb.m_pParam && Urb.m_uParam ) {

					AfxRelease((IUsbPipeEvents *) Urb.m_uParam);

					Urb.m_pParam = NULL;

					Urb.m_uParam = NULL;
					}
				}
			}
		}
	}

BOOL CUsbHostStack::Transfer(IUsbPipe *pPipe, UsbIor *pUrb)
{
	IUsbPipeEvents *pEvents = NULL;

	pPipe->QueryInterface(AfxAeonIID(IUsbPipeEvents), (void **) &pEvents);

	pUrb->m_pParam = pPipe;

	pUrb->m_uParam = UINT(pEvents);

	pUrb->m_uFlags |= UsbIor::flagNotify;

	#if defined(_DEBUG)	

	pUrb->m_uFlags |= (DWORD(pPipe->GetDevice()->GetAddr()) << 24);

	pUrb->m_uFlags |= (DWORD(pPipe->GetEndpointAddr()) << 20);

	#endif

	return pPipe->GetDevice()->GetHostI()->Transfer(*pUrb);
	}

BOOL CUsbHostStack::GetDriverClassOverride(UINT uVendor, UINT uProduct)
{
	return m_DriverLib.GetClassOverride(uVendor, uProduct);
	}

BOOL CUsbHostStack::GetDriverClassFilter(UINT uVendor, UINT uProduct)
{
	return m_DriverLib.GetClassFilter(uVendor, uProduct);
	}

IUsbHostFuncDriver * CUsbHostStack::CreateDriver(UINT uClass, UINT uSubClass, UINT uProtocol)
{
	IUsbHostFuncDriver *p = m_DriverLib.CreateDriver(uClass, uSubClass, uProtocol);

	InitDriver(p);

	return p;
	}

IUsbHostFuncDriver * CUsbHostStack::CreateDriver(UINT uVendor, UINT uProduct, UINT uClass, UINT uSubClass, UINT uProtocol)
{
	IUsbHostFuncDriver *p = m_DriverLib.CreateDriver(uVendor, uProduct, uClass, uSubClass, uProtocol);

	InitDriver(p);

	return p;
	}

// IUsbEvents

BOOL CUsbHostStack::GetDriverInterface(IUsbDriver *&pDriver)
{
	pDriver = NULL;

	return false;
	}

void CUsbHostStack::OnBind(IUsbDriver *pDriver)
{
	BindController(pDriver);
	}

void CUsbHostStack::OnInit(void)
{
	}

void CUsbHostStack::OnStart(void)
{
	}

void CUsbHostStack::OnStop(void)
{
	}

// IUsbHostControllerEvents

void CUsbHostStack::OnDeviceArrival(UsbPortPath &Path)
{
	#if defined(_XDEBUG)	

	AfxTrace("CUsbHostStack::OnDeviceArrival\n");
	
	AfxTrace("Path.Controller   %d\n", Path.a.dwCtrl);
	AfxTrace("Path.Host         %d\n", Path.a.dwHost);
	AfxTrace("Path.Port         %d\n", Path.a.dwPort);
	AfxTrace("Path.HubAddr      %d\n", Path.a.dwHubAddr);
	AfxTrace("Path.HubPort      %d\n", Path.a.dwHubPort);

	#endif

	CUsbDevice * pDevice = MakeDevice();

	if( pDevice ) {

		pDevice->SetPath(Path);

		pDevice->SetHostC(GetHostController(Path.a.dwCtrl));

		pDevice->SetHostI(GetHostInterface(Path.a.dwCtrl, Path.a.dwHost));

		pDevice->SetSpeed(GetPortSpeed(Path));

		pDevice->SetPath(FindTree(Path));

		pDevice->OnArrival();
		}
	}

void CUsbHostStack::OnDeviceRemoval(UsbPortPath &Path)
{
	#if defined(_XDEBUG)	

	AfxTrace("CUsbHostStack::OnDeviceRemoval\n");

	AfxTrace("Path.Controller   %d\n", Path.a.dwCtrl);
	AfxTrace("Path.Host         %d\n", Path.a.dwHost);
	AfxTrace("Path.Port         %d\n", Path.a.dwPort);
	AfxTrace("Path.HubAddr      %d\n", Path.a.dwHubAddr);
	AfxTrace("Path.HubPort      %d\n", Path.a.dwHubPort);
	
	#endif
	
	CUsbDevice *pDevice = (CUsbDevice *) GetDevice(Path);

	if( pDevice ) {
		
		if( pDevice->IsHub() ) {

			BYTE bHubAddr = pDevice->GetAddr();

			for( CUsbDevice *p = m_DevList.GetHead(); p; p = m_DevList.GetNext(p) ) {

				UsbPortPath Scan = p->GetPortPath();

				if( Scan.a.dwCtrl == Path.a.dwCtrl && Scan.a.dwHost == Path.a.dwHost ) {

					if( Scan.a.dwHubAddr == bHubAddr ) {

						OnDeviceRemoval(Scan);
						}
					}
				}
			}

		pDevice->OnRemoval();
		}
	}

void CUsbHostStack::OnDriverStarted(IUsbHostFuncDriver *pDriver)
{
	/*AfxTrace("UsbHostStack.OnDriverStarted\n");*/
	
	for( UINT i = 0; i < elements(m_pSink); i ++ ) {

		if( m_pSink[i] ) {

			m_pSink[i]->OnDeviceArrival(pDriver);
			}
		}
	}

void CUsbHostStack::OnDriverStopped(IUsbHostFuncDriver *pDriver)
{
	/*AfxTrace("UsbHostStack.OnDriverStopped\n");*/

	for( UINT i = 0; i < elements(m_pSink); i ++ ) {

		if( m_pSink[i] ) {
				
			m_pSink[i]->OnDeviceRemoval(pDriver);
			}
		}
	}

void CUsbHostStack::OnTransferDone(UsbIor &Urb)
{
	if( Urb.m_pParam && Urb.m_uParam ) {

		((IUsbPipeEvents *) Urb.m_uParam)->OnTransfer(Urb);
		}
	}

// IUsbHubEvents

void CUsbHostStack::OnHubDeviceArrival(UsbPortPath &Path)
{
	OnDeviceArrival(Path);
	}

void CUsbHostStack::OnHubDeviceRemoval(UsbPortPath &Path)
{
	OnDeviceRemoval(Path);
	}

// IEventSink

void CUsbHostStack::OnEvent(UINT uLine, UINT uParam)
{
	m_pPoll->Set();
	}

// IDiagProvider

UINT CUsbHostStack::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);

		case 2:
			return DiagDevices(pOut, pCmd);

		case 3:
			return DiagReset(pOut, pCmd);

		case 4:
			return DiagComms(pOut, pCmd);
		}

	return 0;
	}

// Task Entry

void CUsbHostStack::TaskEntry(void)
{
	DWORD t1 = NOTHING;

	DWORD t2 = NOTHING;

	m_pTimer->SetHook(this, 0);

	m_pTimer->SetPeriod(50);

	for(;;) {

		UINT uWait = WaitMultiple(m_pRun, m_pStop, m_pPoll, FOREVER);

		if( uWait == 1 ) {

			t1 = NOTHING;

			t2 = NOTHING;
			
			m_pTimer->Enable(true);
			
			continue;
			}

		if( uWait == 2 ) {

			m_pTimer->Enable(false);

			m_pPoll->Clear();

			m_pDone->Set();

			continue;
			}

		if( uWait == 3 ) {

			t2 = GetTickCount();

			if( t2 >= t1 ) {

				DWORD t3 = ToTime(t2 - t1);

				PollControllers(t3);

				PollDevices(t3);
				}

			t1 = GetTickCount();

			continue;
			}
		}
	}

// Controllers

void CUsbHostStack::InitControllerList(void)
{
	memset(m_pCtrl, 0, sizeof(m_pCtrl));
	}

BOOL CUsbHostStack::InitControllers(void)
{
	for( UINT i = 0; i < elements(m_pCtrl); i ++ ) {

		IUsbHostControllerDriver *p = NULL;

		AfxGetObject("usbctrl-c", i, IUsbHostControllerDriver, p);

		if( p ) {

			p->Bind((IUsbHostStack *) this);

			AfxAssert(m_pCtrl[i]);

			p->Init();
			}
		}

	return true;
	}

BOOL CUsbHostStack::StartControllers(void)
{
	for( UINT i = 0; i < elements(m_pCtrl); i ++ ) {

		if( m_pCtrl[i] ) {

			if( !m_pCtrl[i]->Start() ) {

				return false;
				}
			}
		}
		
	return true;
	}

BOOL CUsbHostStack::StopControllers(void)
{
	for( UINT i = 0; i < elements(m_pCtrl); i ++ ) {

		if( m_pCtrl[i] ) {

			if( !m_pCtrl[i]->Stop() ) {
					
				return false;
				}
			}
		}
		
	return true;
	}

BOOL CUsbHostStack::BindController(IUsbDriver *pDriver)
{
	for( UINT i = 0; i < elements(m_pCtrl); i ++ ) {

		if( !m_pCtrl[i] ) {

			m_pCtrl[i] = (IUsbHostControllerDriver *) pDriver;

			m_pCtrl[i]->SetIndex(i);

			return true;
			}
		}

	return false;
	}

void CUsbHostStack::PollControllers(UINT uLapsed)
{
	for( UINT i = 0; i < elements(m_pCtrl); i ++ ) {

		if( m_pCtrl[i] ) {

			m_pCtrl[i]->Poll(uLapsed);
			}
		}
	}

void CUsbHostStack::EnableEvents(BOOL fEnable)
{
	for( UINT i = 0; i < elements(m_pCtrl); i ++ ) {

		if( m_pCtrl[i] ) {

			m_pCtrl[i]->EnableEvents(fEnable);
			}
		}
	}

// Topology

UsbTreePath CUsbHostStack::FindTree(UsbPortPath const &Path)
{
	UsbTreePath Tree = { { Path.a.dwCtrl, Path.a.dwHost, 1, Path.a.dwPort, 0, 0, 0, 0, 0 } };

	UsbPortPath Port = Path;

	while( Port.a.dwHubAddr ) {
			
		Tree.a.dwPort6 = Tree.a.dwPort5;
		Tree.a.dwPort5 = Tree.a.dwPort4;
		Tree.a.dwPort4 = Tree.a.dwPort3;
		Tree.a.dwPort3 = Tree.a.dwPort2;

		Tree.a.dwPort2 = Port.a.dwHubPort;

		Tree.a.dwTier ++;

		UsbDevPath DevPath = { { Port.a.dwHubAddr, Path.a.dwHost, Path.a.dwCtrl, 0, 0, 0 } };

		IUsbDevice *pDev = GetDevice(DevPath);

		if( pDev ) {

			Port.dw = pDev->GetPortPath().dw;

			continue;
			}

		break;
		}

	#if defined(_XDEBUG)	

	AfxTrace("CUsbHostStack::FindTree\n");
	
	AfxTrace("Tree.Controller   %d\n", Tree.a.dwCtrl);
	AfxTrace("Tree.Host         %d\n", Tree.a.dwHost);
	AfxTrace("Tree.Tier         %d\n", Tree.a.dwTier);
	AfxTrace("Tree.Port 1       %d\n", Tree.a.dwPort1);
	AfxTrace("Tree.Port 2       %d\n", Tree.a.dwPort2);
	AfxTrace("Tree.Port 3       %d\n", Tree.a.dwPort3);
	AfxTrace("Tree.Port 4       %d\n", Tree.a.dwPort4);
	AfxTrace("Tree.Port 5       %d\n", Tree.a.dwPort5);
	AfxTrace("Tree.Port 6       %d\n", Tree.a.dwPort6);

	#endif
	
	return Tree;
	}

// Devices

CUsbDevice * CUsbHostStack::MakeDevice(void)
{
	CUsbDevice *pDevice = New CUsbDevice();

	pDevice->SetHostStack(this);
	
	EnableEvents(false);
	
	m_DevList.Append(pDevice);

	EnableEvents(true);

	return pDevice;
	}

void CUsbHostStack::KillDevice(CUsbDevice *pDevice)
{
	m_DevList.Remove(pDevice);

	delete pDevice;
	}

void CUsbHostStack::KillDevices(void)
{
	while( m_DevList.GetHead() ) {

		KillDevice(m_DevList.GetHead());
		}
	}

BOOL CUsbHostStack::ResetDevice(CUsbDevice *pDevice)
{
	return ResetPort(pDevice->GetTreePath()) || ResetPort(pDevice->GetPortPath());
	}

void CUsbHostStack::PollDevices(UINT uLapsed)
{
	CUsbDevice *pDev = m_DevList.GetHead();

	while( pDev ) {

		CUsbDevice *pNext = m_DevList.GetNext(pDev);

		pDev->OnPoll(uLapsed);

		if( pDev->GetState() == CUsbDevice::devError ) {

			AfxTrace("CUsbHostStack::Reset Recovery Device Addr %d\n", pDev->GetAddr());

			ResetDevice(pDev);
			}

		if( pDev->GetState() == CUsbDevice::devDestroyed ) {

			KillDevice(pDev);
			}

		pDev = pNext;
		}
	}

void CUsbHostStack::RemoveDevices(void)
{
	CUsbDevice *pDev = m_DevList.GetHead();

	while( pDev ) {

		pDev->OnRemoval();

		pDev = m_DevList.GetNext(pDev);
		}

	while( m_DevList.GetHead() ) {

		PollDevices(5);

		Sleep(5);
		}
	}

// Drivers

void CUsbHostStack::InitDriver(IUsbHostFuncDriver *pDriver)
{
	if( pDriver ) {

		if( pDriver->GetClass() == devHub ) {

			IUsbHubDriver *pHub = (IUsbHubDriver *) pDriver;

			pHub->Bind((IUsbHubEvents *) this);
			}
		}
	}

// Sink List

void CUsbHostStack::InitSink(void)
{	
	memset(m_pSink, 0, sizeof(m_pSink));
	}

void CUsbHostStack::FreeSink(void)
{
	for( UINT i = 0; i < elements(m_pSink); i ++ ) {

		if( m_pSink[i] ) {
				
			m_pSink[i]->Release();
			
			m_pSink[i] = NULL;
			}
		}
	}

// IO Request Blocks

void CUsbHostStack::InitUrbs(void)
{
	memset(m_Urbs, 0, sizeof(m_Urbs));

	m_iFreeHead = 0;

	m_iFreeTail = 0;

	#if defined(_DEBUG)	

	m_uDiagUrb  = 0;

	#endif

	for( UINT n = 0; n < elements(m_Urbs); n++ ) {

		m_Urbs[n].m_uStatus = UsbIor::statAlloc;

		FreeUrb(&m_Urbs[n]);
		}
	}

// Diagnostics

void CUsbHostStack::ResetCounters(void)
{
	#if defined(_DEBUG)	

	m_uDiagTime = GetTickCount();

	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	m_uDiagCount  = 0;
	
	m_uDiagData   = 0;

	m_uDiagUrbMax = 0;
	
	Hal_LowerIrql(uSave);

	#endif
	}

bool CUsbHostStack::DiagRegister(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "usb");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		pDiag->RegisterCommand(m_uProv, 2, "devices");

		pDiag->RegisterCommand(m_uProv, 3, "reset");

		pDiag->RegisterCommand(m_uProv, 4, "comms");

		return true;
		}

	#endif

	return false;
	}

bool CUsbHostStack::DiagRevoke(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return true;
		}

	#endif

	return false;
	}

UINT CUsbHostStack::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		UINT uCount  = m_uDiagCount;

		UINT uData   = m_uDiagData;

		UINT uPeriod = (1000 - 1 + ToTime(GetTickCount() - m_uDiagTime)) / 1000;

		pOut->AddPropList();

		pOut->AddProp("Transact Count", "%u", uCount);
		pOut->AddProp("Transact Rate",  "%u", uCount / uPeriod);
		pOut->AddProp("Transfer Count", "%u", uData);
		pOut->AddProp("Transfer Rate",  "%u", uData / uPeriod);
		pOut->AddProp("Urb Alloc",      "%u", m_uDiagUrbMax);
		pOut->AddProp("Period",         "%u", ToTime(GetTickCount() - m_uDiagTime));

		pOut->EndPropList();
		
		ResetCounters();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CUsbHostStack::DiagDevices(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(11);

		pOut->SetColumn(0,  "Ctrl",     "%u");
		pOut->SetColumn(1,  "Host",     "%u");
		pOut->SetColumn(2,  "Class",    "%s");
		pOut->SetColumn(3,  "Addr",     "%u");
		pOut->SetColumn(4,  "Vendor",   "%4.4X");
		pOut->SetColumn(5,  "Product",  "%4.4X");
		pOut->SetColumn(6,  "Speed",    "%s");
		pOut->SetColumn(7,  "Drivers",  "%d");
		pOut->SetColumn(8,  "Hub Addr", "%u");
		pOut->SetColumn(9,  "Hub Port", "%d");
		pOut->SetColumn(10, "State",    "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		for( CUsbDevice *p = m_DevList.GetHead(); p; p = m_DevList.GetNext(p) ) {

			pOut->AddRow();

			pOut->SetData(0,  p->GetHostC()->GetIndex());
			pOut->SetData(1,  p->GetHostI()->GetIndex());
			pOut->SetData(2,  p->IsHub() ? "Hub" : "");
			pOut->SetData(3,  p->GetAddr());
			pOut->SetData(4,  p->GetDevDesc().m_wVendorID);
			pOut->SetData(5,  p->GetDevDesc().m_wProductID);
			pOut->SetData(6,  p->GetSpeed() == usbSpeedHigh ? "480M" : p->GetSpeed() == usbSpeedFull ? "12M" : "1.5M");
			pOut->SetData(7,  p->GetDriverCount());
			pOut->SetData(8,  p->GetPortPath().a.dwHubAddr);
			pOut->SetData(9,  p->GetPortPath().a.dwHubPort);
			pOut->SetData(10, p->GetStateText());
			
			pOut->EndRow();
			}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CUsbHostStack::DiagReset(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	UINT uAddr = pCmd->GetArgCount() ? atoi(pCmd->GetArg(0)) : 0;

	CUsbDevice *pDev = m_DevList.GetHead();

	while( pDev ) {

		if( pDev->GetAddr() == uAddr ) {

			ResetDevice(pDev);
			}

		pDev = m_DevList.GetNext(pDev);
		}

	return 0;

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CUsbHostStack::DiagComms(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("FreeHead", "%u", m_iFreeHead);
		pOut->AddProp("FreeTail", "%u", m_iFreeTail);

		pOut->EndPropList();

		pOut->AddTable(8);

		pOut->SetColumn(0,  "Num",	"%u");
		pOut->SetColumn(1,  "Urb",      "%8.8X");
		pOut->SetColumn(2,  "Endpoint", "%8.8X");
		pOut->SetColumn(3,  "Flags",    "%4.4X");
		pOut->SetColumn(4,  "Status",   "%u");
		pOut->SetColumn(5,  "Count",    "%u");
		pOut->SetColumn(6,  "Addr",     "%d");
		pOut->SetColumn(7,  "Endpt",    "%d");
		
		pOut->AddHead();

		pOut->AddRule('-');

		for( UINT i = 0; i < elements(m_Urbs); i ++ ) {
					
			if( m_Urbs[i].m_uStatus >= UsbIor::statAlloc ) {

				pOut->AddRow();

				pOut->SetData(0, i);
				pOut->SetData(1, &m_Urbs[i]);
				pOut->SetData(2, m_Urbs[i].m_iEndpt);
				pOut->SetData(3, m_Urbs[i].m_uFlags & 0x0FFFFFFF);
				pOut->SetData(4, m_Urbs[i].m_uStatus);
				pOut->SetData(5, m_Urbs[i].m_uCount);
				pOut->SetData(6, m_Urbs[i].m_uFlags >> 24);
				pOut->SetData(7, (m_Urbs[i].m_uFlags >> 20) & 0xF);
				
				pOut->EndRow();
				}
			}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

// Task Entry

int CUsbHostStack::TaskUsbHostStack(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("UsbHost");

	((CUsbHostStack *) pParam)->TaskEntry();

	return 0;
	}

// End of File
