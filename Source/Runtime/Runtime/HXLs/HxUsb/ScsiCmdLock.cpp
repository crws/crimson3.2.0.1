
#include "Intern.hpp"

#include "ScsiCmdLock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Lock Media
//

// Constructor

CScsiCmdLock::CScsiCmdLock(void)
{
	}

// Endianess

void CScsiCmdLock::HostToScsi(void)
{
	}

void CScsiCmdLock::ScsiToHost(void)
{
	}

// Operations

void CScsiCmdLock::Init(bool fLock)
{
	memset(this, 0, sizeof(ScsiCmdLock));

	m_bOpcode  = cmdLockMedia;
	
	m_bPrevent = fLock;
	}

// End of File
