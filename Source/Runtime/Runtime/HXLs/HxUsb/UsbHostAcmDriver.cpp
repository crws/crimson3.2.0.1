
#include "Intern.hpp"  

#include "UsbHostAcmDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbClassReq.hpp"

#include "CdcLineCoding.hpp"

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Comms Device Class Driver
//

// Instantiator

IUsbHostFuncDriver * Create_AcmDriver(void)
{
	IUsbHostAcm *p = (IUsbHostAcm *) New CUsbHostAcmDriver;

	return p;
	}

// Constructor

CUsbHostAcmDriver::CUsbHostAcmDriver(void)
{
	m_pName     = "Comms Device Class Driver";

	m_pCall     = NULL;

	m_pAbstract = NULL;

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostAcmDriver::~CUsbHostAcmDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");
	}

// IUnknown

HRESULT CUsbHostAcmDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostAcm);

	return CUsbHostCdcDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostAcmDriver::AddRef(void)
{
	return CUsbHostCdcDriver::AddRef();
	}

ULONG CUsbHostAcmDriver::Release(void)
{
	return CUsbHostCdcDriver::Release();
	}

// IHostFuncDriver

void CUsbHostAcmDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostCdcDriver::Bind(pDevice, iInterface);
	}

void CUsbHostAcmDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostCdcDriver::Bind(pSink);
	}

void CUsbHostAcmDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostCdcDriver::GetDevice(pDev);
	}

BOOL CUsbHostAcmDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostCdcDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostAcmDriver::GetVendor(void)
{
	return CUsbHostCdcDriver::GetVendor();
	}

UINT CUsbHostAcmDriver::GetProduct(void)
{
	return CUsbHostCdcDriver::GetProduct();
	}

UINT CUsbHostAcmDriver::GetClass(void)
{
	return CUsbHostCdcDriver::GetClass();
	}

UINT CUsbHostAcmDriver::GetSubClass(void)
{
	return classAbstract;
	}

UINT CUsbHostAcmDriver::GetInterface(void)
{
	return CUsbHostCdcDriver::GetInterface();
	}

UINT CUsbHostAcmDriver::GetProtocol(void)
{
	return protV250;
	}

BOOL CUsbHostAcmDriver::GetActive(void)
{
	return CUsbHostCdcDriver::GetActive();
	}

BOOL CUsbHostAcmDriver::Open(CUsbDescList const &List)
{
	if( ParseConfig() ) {	

		return CUsbHostCdcDriver::Open(List);
		}

	return FALSE;
	}

BOOL CUsbHostAcmDriver::Close(void)
{
	return CUsbHostCdcDriver::Close();
	}

void CUsbHostAcmDriver::Poll(UINT uLapsed)
{
	CUsbHostCdcDriver::Poll(uLapsed);
	}

// IUsbHostCdc

BOOL CUsbHostAcmDriver::SendEncapCommand(PBYTE pData, UINT uLen)
{
	return CUsbHostCdcDriver::SendEncapCommand(pData, uLen);
	}

UINT CUsbHostAcmDriver::RecvEncapResponse(PBYTE pData, UINT uLen)
{
	return CUsbHostCdcDriver::RecvEncapResponse(pData, uLen);
	}

BOOL CUsbHostAcmDriver::SendData(PCTXT pData, bool fAsync)
{
	return CUsbHostCdcDriver::SendData(pData, fAsync);
	}

BOOL CUsbHostAcmDriver::SendData(PCBYTE pData, UINT uLen, bool fAsync)
{
	return CUsbHostCdcDriver::SendData(pData, uLen, fAsync);
	}

UINT CUsbHostAcmDriver::RecvData(PBYTE pData, UINT uLen, bool fAsync)
{
	return CUsbHostCdcDriver::RecvData(pData, uLen, fAsync);
	}

UINT CUsbHostAcmDriver::WaitSend(UINT uTimeout)
{
	return CUsbHostCdcDriver::WaitSend(uTimeout);
	}

UINT CUsbHostAcmDriver::WaitRecv(UINT uTimeout)
{
	return CUsbHostCdcDriver::WaitRecv(uTimeout);
	}

void CUsbHostAcmDriver::KillSend(void)
{
	CUsbHostCdcDriver::KillSend();
	}

void CUsbHostAcmDriver::KillRecv(void)
{
	CUsbHostCdcDriver::KillRecv();
	}

void CUsbHostAcmDriver::KillAsync(void)
{
	CUsbHostCdcDriver::KillAsync();
	}

// IUsbHostAcm

BOOL CUsbHostAcmDriver::SetCommFeatureState(WORD wState)
{
	Trace(debugCmds, "SetCommFeatureState State=0x%4.4X", wState);

	if( HasCommFeatureSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirHostToDev;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqSetCommFeature;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wLength   = sizeof(WORD);

		Req.m_wValue    = 1;
		
		Req.HostToUsb();

		wState = HostToIntel(wState);

		return m_pCtrl->CtrlTrans(Req, PBYTE(&wState), sizeof(WORD)) == sizeof(WORD);
		}

	Trace(debugWarn, "Command not supported");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::GetCommFeatureState(WORD &wState)
{
	Trace(debugCmds, "GetCommFeatureState");

	if( HasCommFeatureSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirDevToHost;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqGetCommFeature;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wLength   = sizeof(WORD);

		Req.m_wValue    = 1;
		
		Req.HostToUsb();

		if( m_pCtrl->CtrlTrans(Req, PBYTE(&wState), sizeof(WORD)) == sizeof(WORD) ) {

			wState = IntelToHost(wState);

			Trace(debugCmds, "State=0x%4.4X", wState);
			
			return TRUE;
			}

		return FALSE;
		}

	Trace(debugWarn, "Command not supported");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::ClearCommFeatureState(void)
{
	Trace(debugCmds, "ClearCommFeatureState");

	if( HasCommFeatureSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirHostToDev;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqClearCommFeature;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wValue    = 1;
		
		Req.HostToUsb();

		return m_pCtrl->CtrlTrans(Req);
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::SetCommFeatureCountry(WORD wCountry)
{
	Trace(debugCmds, "SetCommFeatureCountry Country=0x%4.4X", wCountry);

	if( HasCommFeatureSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirHostToDev;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqSetCommFeature;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wLength   = sizeof(WORD);

		Req.m_wValue    = 2;
		
		Req.HostToUsb();

		wCountry = HostToIntel(wCountry);

		return m_pCtrl->CtrlTrans(Req, PBYTE(&wCountry), sizeof(WORD)) == sizeof(WORD);
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::GetCommFeatureCountry(WORD &wCountry)
{
	Trace(debugCmds, "GetCommFeatureCountry");

	if( HasCommFeatureSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirDevToHost;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqGetCommFeature;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wLength   = sizeof(WORD);

		Req.m_wValue    = 2;
		
		Req.HostToUsb();

		if( m_pCtrl->CtrlTrans(Req, PBYTE(&wCountry), sizeof(WORD)) == sizeof(WORD) ) {

			wCountry = IntelToHost(wCountry);

			return TRUE;
			}
		
		return FALSE;
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::ClearCommFeatureCountry(void)
{
	Trace(debugCmds, "ClearCommFeatureCountry");

	if( HasCommFeatureSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirHostToDev;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqClearCommFeature;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wValue    = 2;
		
		Req.HostToUsb();

		return m_pCtrl->CtrlTrans(Req);
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::SetLineCoding(CSerialConfig const &Config)
{
	Trace(debugCmds, "SetLineCoding Baud=%d Data=%d Stop=%d Parity=%d", Config.m_uBaudRate, Config.m_uDataBits, Config.m_uStopBits, Config.m_uParity);

	if( HasLineControlSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirHostToDev;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqSetLineCoding;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wLength   = sizeof(CdcLineCoding);
		
		Req.HostToUsb();

		CCdcLineCoding Data;

		Data.Init(Config);

		Data.HostToCdc();

		return m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data);
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::GetLineCoding(CSerialConfig &Config)
{
	Trace(debugCmds, "GetLineCoding");

	if( HasLineControlSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirDevToHost;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqGetLineCoding;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wLength   = sizeof(CdcLineCoding);
		
		Req.HostToUsb();

		CCdcLineCoding Data;

		if( m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data) ) {

			Data.CdcToHost();

			Data.Get(Config);

			Trace(debugCmds, "Baud   %d", Config.m_uBaudRate);

			Trace(debugCmds, "Data   %d", Config.m_uDataBits);

			Trace(debugCmds, "Stop   %d", Config.m_uStopBits);

			Trace(debugCmds, "Parity %d", Config.m_uParity);

			return TRUE;
			}
		
		return FALSE;
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::SetControlLineState(bool fRTS, bool fDTR)
{
	Trace(debugCmds, "SetControlLineState RTS=%d DTR=%d", fRTS, fDTR);

	if( HasLineControlSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirHostToDev;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqSetCtrlLineState;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wValue   |= (fDTR ? Bit(0) : 0);

		Req.m_wValue   |= (fRTS ? Bit(1) : 0);

		Req.HostToUsb();

		return m_pCtrl->CtrlTrans(Req);
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::SendBreak(WORD wDuration)
{
	Trace(debugCmds, "SendBreak Duration=%d", wDuration);

	if( HasSendBreakSupport() ) {

		CUsbClassReq Req;

		Req.m_Direction = dirHostToDev;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqSendBreak;

		Req.m_wIndex    = m_iInt;
		
		Req.m_wValue    = wDuration;

		Req.HostToUsb();

		return m_pCtrl->CtrlTrans(Req);
		}

	Trace(debugWarn, "Command not supported.");

	return FALSE;
	}

BOOL CUsbHostAcmDriver::VendorRequest(BYTE bReq, WORD wAddr, WORD wValue)
{
	Trace(debugCmds, "Vendor Request Req=%2.2X Addr=%4.4X Value=%4.4X", bReq, wAddr, wValue);

	CUsbDeviceReq Req;

	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;

	Req.m_bRequest  = bReq;

	Req.m_wValue    = wValue;

	Req.m_wIndex    = wAddr;

	Req.m_wLength   = 0;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

// Implememtation

bool CUsbHostAcmDriver::HasCommFeatureSupport(void)
{
	return m_pAbstract && m_pAbstract->m_bCaps & Bit(0);
	}

bool CUsbHostAcmDriver::HasLineControlSupport(void)
{
	return m_pAbstract && m_pAbstract->m_bCaps & Bit(1);
	}

bool CUsbHostAcmDriver::HasSendBreakSupport(void)
{
	return m_pAbstract && m_pAbstract->m_bCaps & Bit(2);
	}

bool CUsbHostAcmDriver::ParseConfig(void)
{
	CUsbDescList const &List = m_pDev->GetCfgDesc();	
	
	UINT iIndex;

	List.FindInterface(iIndex, m_iInt);

	for(;;) {

		CdcFuncDesc *p = (CdcFuncDesc *) List.Enum(descCsInterface, iIndex);

		if( p ) {

			switch( p->m_bSubType ) {

				case descCall: 

					if( !m_pCall ) {
						
						m_pCall = (CdcCallDesc *) p;
						}

					break;

				case descAbstract:

					if( !m_pAbstract ) {
					
						m_pAbstract = (CdcAbstractDesc *) p;
						}

					break;
				}

			continue;
			}
		
		break;
		}

	return m_pAbstract;
	}

// End of File
