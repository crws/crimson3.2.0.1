
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDeviceReq_HPP

#define	INCLUDE_UsbDeviceReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Requeset
//

class CUsbDeviceReq : public UsbDeviceReq
{
	public:
		// Constructor
		CUsbDeviceReq(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Init
		void Init(void);

		// Data Access
		PBYTE GetData(void) const;
		UINT  GetSize(void) const;

		// Debug
		void Debug(void) const;
	};

// End of File

#endif
