
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubGetPortStatusReq_HPP

#define	INCLUDE_UsbHubGetPortStatusReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Get Port Status Request
//

class CUsbHubGetPortStatusReq : public CUsbClassReq
{
	public:
		// Constructor
		CUsbHubGetPortStatusReq(WORD wPort);

		// Operations
		void Init(WORD wPort);
	};

// End of File

#endif
