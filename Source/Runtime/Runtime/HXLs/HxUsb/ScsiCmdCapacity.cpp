
#include "Intern.hpp"

#include "ScsiCmdCapacity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Read Capacity
//

// Constructor

CScsiCmdCapacity::CScsiCmdCapacity(void)
{
	}

// Endianess

void CScsiCmdCapacity::HostToScsi(void)
{
	m_wBlockAddr = HostToMotor(m_wBlockAddr);
	}

void CScsiCmdCapacity::ScsiToHost(void)
{
	m_wBlockAddr = MotorToHost(m_wBlockAddr);
	}

// Operations

void CScsiCmdCapacity::Init(void)
{
	memset(this, 0, sizeof(ScsiCmdCapacity));

	m_bOpcode = cmdReadCapacity;
	}

// End of File
