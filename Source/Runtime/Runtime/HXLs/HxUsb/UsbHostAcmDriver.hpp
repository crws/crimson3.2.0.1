
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostAcmDriver_HPP

#define	INCLUDE_UsbHostAcmDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostCdcDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Comms Device Class Driver
//

class CUsbHostAcmDriver : public CUsbHostCdcDriver, public IUsbHostAcm
{
	public:
		// Constructor
		CUsbHostAcmDriver(void);

		// Destructor
		~CUsbHostAcmDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostCdc
		BOOL METHOD SendEncapCommand(PBYTE pData, UINT uLen);
		UINT METHOD RecvEncapResponse(PBYTE pData, UINT uLen);
		BOOL METHOD SendData(PCTXT pData, bool fAsync);
		BOOL METHOD SendData(PCBYTE pData, UINT uLen, bool fAsync);
		UINT METHOD RecvData(PBYTE  pData, UINT uLen, bool fAsync);
		UINT METHOD WaitSend(UINT uTimeout);
		UINT METHOD WaitRecv(UINT uTimeout);
		void METHOD KillSend(void);
		void METHOD KillRecv(void);
		void METHOD KillAsync(void);

		// IUsbHostAcm
		BOOL METHOD SetCommFeatureState(WORD wState);
		BOOL METHOD GetCommFeatureState(WORD &wState);
		BOOL METHOD ClearCommFeatureState(void);
		BOOL METHOD SetCommFeatureCountry(WORD wCountry);
		BOOL METHOD GetCommFeatureCountry(WORD &wCountry);
		BOOL METHOD ClearCommFeatureCountry(void);
		BOOL METHOD SetLineCoding(CSerialConfig const &Config);
		BOOL METHOD GetLineCoding(CSerialConfig &Config);
		BOOL METHOD SetControlLineState(bool fRTS, bool fDTR);
		BOOL METHOD SendBreak(WORD wDuration);
		BOOL METHOD VendorRequest(BYTE bReq, WORD wAddr, WORD wValue);

	protected:
		// Data
		CdcAbstractDesc * m_pAbstract;
		CdcCallDesc	* m_pCall;

		// Implementation
		bool ParseConfig(void);
		bool HasCommFeatureSupport(void);
		bool HasLineControlSupport(void);
		bool HasSendBreakSupport(void);
	};

// End of File

#endif
