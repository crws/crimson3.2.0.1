
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostModuleGenericDriver_HPP

#define	INCLUDE_UsbHostModuleGenericDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostModuleDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Module Generic Driver
//

class CUsbHostModuleGenericDriver : public CUsbHostModuleDriver, public IUsbHostModuleGeneric
{
	public:
		// Constructor
		CUsbHostModuleGenericDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostModule
		BOOL METHOD Reset(void);
		BOOL METHOD ReadVersion(BYTE Version[16]); 
		BOOL METHOD SendHeartbeat(void);
		
		// IUsbHostModuleGeneric
		BOOL METHOD SendData(PCBYTE pData, UINT uCount);
		UINT METHOD RecvData(PBYTE  pData, UINT uCount);
		BOOL METHOD SendDataAsync(PCBYTE pData, UINT uCount);
		UINT METHOD RecvDataAsync(PBYTE pData, UINT uCount);
		UINT METHOD WaitAsyncSend(UINT uTimeout);
		UINT METHOD WaitAsyncRecv(UINT uTimeout);
		BOOL METHOD KillAsyncSend(void);
		BOOL METHOD KillAsyncRecv(void);
		BOOL METHOD Shutdown(void);

	protected:
		// Interface Commands
		enum
		{
			cmdShutdown = 0x01,
			};
	};

// End of File

#endif
