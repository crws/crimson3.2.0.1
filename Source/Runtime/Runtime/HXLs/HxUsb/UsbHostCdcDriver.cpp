
#include "Intern.hpp"  

#include "UsbHostCdcDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbClassReq.hpp"

#include "CdcLineCoding.hpp"

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Comms Device Class Driver
//

// Instantiator

IUsbHostFuncDriver * Create_CdcDriver(void)
{
	IUsbHostCdc *p = (IUsbHostCdc *) New CUsbHostCdcDriver;

	return p;
	}

// Constructor

CUsbHostCdcDriver::CUsbHostCdcDriver(void)
{
	m_pName  = "Comms Device Class Driver";

	m_Debug  = debugWarn;

	m_pUnion = NULL;

	m_pCtrl  = NULL;
	
	m_pPoll  = NULL;
	
	m_pSend  = NULL;

	m_pRecv  = NULL;

	m_pData  = NULL;

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostCdcDriver::~CUsbHostCdcDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");

	FreeData();
	}

// IUnknown

HRESULT CUsbHostCdcDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostCdc);

	return CUsbHostFuncDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostCdcDriver::AddRef(void)
{
	return CUsbHostFuncDriver::AddRef();
	}

ULONG CUsbHostCdcDriver::Release(void)
{
	return CUsbHostFuncDriver::Release();
	}

// IHostFuncDriver

void CUsbHostCdcDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncDriver::Bind(pDevice, iInterface);
	}

void CUsbHostCdcDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostCdcDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncDriver::GetDevice(pDev);
	}

BOOL CUsbHostCdcDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostCdcDriver::GetVendor(void)
{
	return CUsbHostFuncDriver::GetVendor();
	}

UINT CUsbHostCdcDriver::GetProduct(void)
{
	return CUsbHostFuncDriver::GetProduct();
	}

UINT CUsbHostCdcDriver::GetClass(void)
{
	return devComms;
	}

UINT CUsbHostCdcDriver::GetSubClass(void)
{
	return classAbstract;
	}

UINT CUsbHostCdcDriver::GetInterface(void)
{
	return CUsbHostFuncDriver::GetInterface();
	}

UINT CUsbHostCdcDriver::GetProtocol(void)
{
	return protV250;
	}

BOOL CUsbHostCdcDriver::GetActive(void)
{
	return CUsbHostFuncDriver::GetActive();
	}

BOOL CUsbHostCdcDriver::Open(CUsbDescList const &List)
{
	Trace(debugInfo, "Open");

	if( ParseConfig() && FindEndpoints() ) {

		m_fPend = false;

		return CUsbHostFuncDriver::Open();
		}

	return FALSE;
	}

BOOL CUsbHostCdcDriver::Close(void)
{
	Trace(debugInfo, "Close");

	return CUsbHostFuncDriver::Close();
	}

void CUsbHostCdcDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncDriver::Poll(uLapsed);

	if( m_fPend ) {

		UINT uCount;

		BOOL fResult;

		if( m_pPoll->WaitAsync(0, fResult, uCount) ) {

			m_fPend = false;

			if( fResult && uCount >= sizeof(CUsbDeviceReq) ) {

				for( UINT i = 0; i < uCount; i += sizeof(CUsbDeviceReq) ) {

					CUsbDeviceReq &Req = (CUsbDeviceReq &) m_pData[i];

					Req.UsbToHost();

					i += Req.m_wLength;

					if( Req.m_Direction != dirDevToHost ) {

						continue;
						}

					if( Req.m_Type != reqClass ) {

						continue;
						}

					if( Req.m_Recipient != recInterface ) {

						continue;
						}

					OnEvent(Req);
					}
				}
			}
		}

	if( m_pPoll && !m_fPend ) {

		if( m_pPoll->RecvBulk(m_pData, m_uData, true) != NOTHING ) {
					
			m_fPend = true;
			}
		}
	}

// IUsbHostCdc

BOOL CUsbHostCdcDriver::SendEncapCommand(PBYTE pData, UINT uLen)
{
	Trace(debugCmds, "SendEncapCommand Count=%d", uLen);

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSendEncap;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = uLen;
		
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, pData, uLen) == uLen;
	}

UINT CUsbHostCdcDriver::RecvEncapResponse(PBYTE pData, UINT uLen)
{
	Trace(debugCmds, "RecvEncapCommand Count=%d", uLen);

	CUsbClassReq Req;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqRecvEncap;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = uLen;
		
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, pData, uLen);
	}

BOOL CUsbHostCdcDriver::SendData(PCTXT pData, bool fAsync)
{
	return SendData(PBYTE(pData), strlen(pData), fAsync);
	}

BOOL CUsbHostCdcDriver::SendData(PCBYTE pData, UINT uLen, bool fAsync)
{
	Trace(debugCmds, "SendData Count=%d", uLen);

	return m_pSend && m_pSend->SendBulk(PBYTE(pData), uLen, fAsync);
	}

UINT CUsbHostCdcDriver::RecvData(PBYTE pData, UINT uLen, bool fAsync)
{
	Trace(debugCmds, "RecvData Count=%d", uLen);

	if( m_pRecv ) {
		
		uLen = m_pRecv->RecvBulk(pData, uLen, fAsync);

		if( uLen != NOTHING ) {

			if( !fAsync ) {

				Trace(debugData, "Recv Actual=%d", uLen);

				/*AfxDump(pData, uLen);*/
				}

			return uLen;
			}	
		}

	return NOTHING;
	}

UINT CUsbHostCdcDriver::WaitSend(UINT uTimeout)
{
	Trace(debugData, "WaitSend Timeout=%d", uTimeout);

	BOOL fOk    = false;

	UINT uCount = 0;
	
	if( m_pSend->WaitAsync(uTimeout, fOk, uCount) ) {

		return fOk ? uCount : 0;
		}

	return NOTHING;
	}

UINT CUsbHostCdcDriver::WaitRecv(UINT uTimeout)
{
	Trace(debugData, "WaitRecv Timeout=%d", uTimeout);

	BOOL fOk    = false;

	UINT uCount = 0;
	
	if( m_pRecv->WaitAsync(uTimeout, fOk, uCount) ) {

		if( fOk && uCount ) {

			Trace(debugData, "Recv Actual=%d", uCount);

			return uCount;
			}

		return 0;
		}

	return NOTHING;
	}

void CUsbHostCdcDriver::KillSend(void)
{
	m_pSend->KillAsync();
	}

void CUsbHostCdcDriver::KillRecv(void)
{
	m_pRecv->KillAsync();
	}

void CUsbHostCdcDriver::KillAsync(void)
{
	m_pSend->KillAsync();

	m_pRecv->KillAsync();

	if( m_pPoll ) {

		m_pPoll->KillAsync();
		}
	}

// Notifications

void CUsbHostCdcDriver::OnEvent(UsbDeviceReq const &Req)
{
	}

// Implememtation

bool CUsbHostCdcDriver::ParseConfig(void)
{
	CUsbDescList const &List = m_pDev->GetCfgDesc();	
	
	UINT iIndex;

	List.FindInterface(iIndex, m_iInt);

	for(;;) {

		CdcFuncDesc *p = (CdcFuncDesc *) List.Enum(descCsInterface, iIndex);

		if( p ) {

			if( p->m_bSubType == descUnion ) {

				m_pUnion = (CdcUnionDesc *) p;

				return true;
				}

			continue;
			}
		
		return false;
		}
	}

bool CUsbHostCdcDriver::FindEndpoints(void)
{
	return FindCtrlEndpoint() && FindPollEndpoint() && FindDataEndpoint();
	}

bool CUsbHostCdcDriver::FindCtrlEndpoint(void)
{
	m_pCtrl = m_pDev->GetCtrlPipe();

	return m_pCtrl != NULL;
	}

bool CUsbHostCdcDriver::FindPollEndpoint(void)
{
	if( m_pUnion ) {
				
		CUsbDescList const &List = m_pDev->GetCfgDesc();	
	
		UINT iIndex;

		List.FindInterface(iIndex, m_iInt);

		UsbInterfaceDesc *p = (UsbInterfaceDesc *) List.FindInterface(iIndex, m_pUnion->m_bControl);

		if( p &&  p->m_bClass == devComms ) {
			
			if( p->m_bEndpoints == 1 ) {

				UsbEndpointDesc *pEp = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

				if( pEp ) {

					m_pPoll = m_pDev->GetPipe(pEp->m_bAddr, pEp->m_bDirIn);

					m_iPoll = pEp->m_bAddr | (pEp->m_bDirIn ? Bit(7) : 0);

					m_uData = pEp->m_wMaxPacket;
					
					Trace(debugCmds, "INT Endpoint 0x%2.2X (Size=%d)", m_iPoll, m_uData);

					if( m_pPoll ) {

						m_pData = New BYTE[m_uData];
						}
					else {
						Trace(debugWarn, "INT endpoint NOT implemented!");
						}
					
					return true;
					}
				}
			}
		}

	return false;
	}

bool CUsbHostCdcDriver::FindDataEndpoint(void)
{
	if( m_pUnion ) {
				
		CUsbDescList const &List = m_pDev->GetCfgDesc();	
	
		UINT iIndex;

		for( UINT iAlt = 0; iAlt < 4; iAlt++ ) {

			UINT iInterface = m_pUnion->m_bInterface[0];

			UsbInterfaceDesc *p = (UsbInterfaceDesc *) List.FindInterface(iIndex, iInterface, iAlt);

			if( p ) {

				if( p->m_bClass == classData && p->m_bEndpoints == 2 ) {

					UsbEndpointDesc *pEp0 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

					UsbEndpointDesc *pEp1 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

					if( pEp0 && pEp1 ) {

						if( pEp0->m_bDirIn ) {

							m_pRecv = m_pDev->GetPipe(pEp0->m_bAddr, pEp0->m_bDirIn);

							m_pSend = m_pDev->GetPipe(pEp1->m_bAddr, pEp1->m_bDirIn);

							m_iRecv = pEp0->m_bAddr | Bit(7);

							m_iSend = pEp1->m_bAddr;
							}
						else {
							m_pSend = m_pDev->GetPipe(pEp0->m_bAddr, pEp0->m_bDirIn);

							m_pRecv = m_pDev->GetPipe(pEp1->m_bAddr, pEp1->m_bDirIn);

							m_iSend = pEp0->m_bAddr;

							m_iRecv = pEp1->m_bAddr | Bit(7);
							}

						Trace(debugCmds, "IN  Endpoint 0x%2.2X", m_iRecv);
	
						Trace(debugCmds, "OUT Endpoint 0x%2.2X", m_iSend);

						if( m_pSend != NULL && m_pRecv != NULL ) {

							Trace(iAlt ? debugInfo : debugNone, "Alternate Interface Required.");

							return true;
							}
						}
					}

				m_pDev->SetAltInterface(iInterface, iAlt+1);

				continue;
				}

			break;
			}
		}

	return false;
	}

void CUsbHostCdcDriver::FreeData(void)
{
	if( m_pData ) {

		delete [] m_pData;

		m_pData = NULL;
		}
	}

// End of File
