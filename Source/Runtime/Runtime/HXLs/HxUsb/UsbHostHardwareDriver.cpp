
#include "Intern.hpp"

#include "UsbHostHardwareDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Macros  
//

#define RoundUp(x, n)	((x + (n-1)) & ~(n-1))

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Hardware Driver
//

// Constructor

CUsbHostHardwareDriver::CUsbHostHardwareDriver(void)
{
	m_pUpperDrv = NULL;
	
	m_pLowerDrv = NULL;

	m_iIndex    = NOTHING;
	}

// Destructor

CUsbHostHardwareDriver::~CUsbHostHardwareDriver(void)
{
	AfxRelease(m_pUpperDrv);

	AfxRelease(m_pLowerDrv);
	}

// IUnknown

HRESULT CUsbHostHardwareDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHostInterfaceDriver);

	StdQueryInterface(IUsbHostInterfaceDriver);

	StdQueryInterface(IUsbDriver);

	StdQueryInterface(IUsbHostHardwareEvents);

	StdQueryInterface(IUsbEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbHostHardwareDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHostHardwareDriver::Release(void)
{
	StdRelease();
	}

// IUsbDriver

BOOL CUsbHostHardwareDriver::Bind(IUsbEvents *pDriver)
{
	if( !m_pUpperDrv && pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostInterfaceEvents), (void **) &m_pUpperDrv);

		if( m_pUpperDrv ) {

			m_pUpperDrv->OnBind(this);
		
			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::Bind(IUsbDriver *pDriver)
{
	if( !m_pUpperDrv && pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostInterfaceEvents), (void **) &m_pUpperDrv);

		if( m_pUpperDrv ) {

			m_pUpperDrv->OnBind(this);
		
			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::Init(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {
		
		if( m_pLowerDrv->Init() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUsbHostHardwareDriver::Start(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {
		
		if( m_pLowerDrv->Start() ) {

			InitAddress();

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::Stop(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {

		EnableEvents(false);
		
		return m_pLowerDrv->Stop();
		}

	return FALSE;
	}
		
// IUsbHostInterfaceDriver

UINT CUsbHostHardwareDriver::GetType(void)
{
	return NOTHING;
	}

UINT CUsbHostHardwareDriver::GetIndex(void)
{
	return m_iIndex;
	}

UINT CUsbHostHardwareDriver::GetPortCount(void)
{
	return NOTHING;
	}

BOOL CUsbHostHardwareDriver::GetPortConnect(UINT iPort)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::GetPortEnabled(UINT iPort)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::GetPortCurrent(UINT iPort)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::GetPortSuspend(UINT iPort)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::GetPortReset(UINT iPort)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::GetPortPower(UINT iPort)
{
	return FALSE;
	}

UINT CUsbHostHardwareDriver::GetPortSpeed(UINT iPort)
{
	return usbSpeedHigh;
	}

void CUsbHostHardwareDriver::SetIndex(UINT iIndex)
{
	m_iIndex = iIndex;
	}

void CUsbHostHardwareDriver::SetPortEnable(UINT iPort, BOOL fSet)
{
	}

void CUsbHostHardwareDriver::SetPortSuspend(UINT iPort, BOOL fset)
{
	}

void CUsbHostHardwareDriver::SetPortResume(UINT iPort, BOOL fSet)
{
	}

void CUsbHostHardwareDriver::SetPortReset(UINT iPort, BOOL fSet)
{
	}

void CUsbHostHardwareDriver::SetPortPower(UINT iPort, BOOL fSet)
{
	}

void CUsbHostHardwareDriver::EnableEvents(BOOL fEnable)
{
	}

BOOL CUsbHostHardwareDriver::SetEndptDevAddr(DWORD iEndpt, BYTE bAddr)
{
	return FALSE;
	}

UINT CUsbHostHardwareDriver::GetEndptDevAddr(DWORD iEndpt)
{
	return NOTHING;
	}

BOOL CUsbHostHardwareDriver::SetEndptAddr(DWORD iEndpt, BYTE bAddr)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::SetEndptMax(DWORD iEndpt, WORD wMaxPacket)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::SetEndptHub(DWORD iEndpt, BYTE bHub, BYTE bPort)
{
	return FALSE;
	}

DWORD CUsbHostHardwareDriver::MakeDevice(IUsbDevice *pDevice)
{
	return NOTHING;
	}

void CUsbHostHardwareDriver::KillDevice(DWORD iDev)
{
	}

BOOL CUsbHostHardwareDriver::CheckConfig(DWORD iDev)
{
	return TRUE;
	}

DWORD CUsbHostHardwareDriver::MakeEndptCtrl(DWORD iDev, UINT uSpeed)
{
	return NOTHING;
	}

DWORD CUsbHostHardwareDriver::MakeEndptAsync(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc)
{
	return NOTHING;
	}

DWORD CUsbHostHardwareDriver::MakeEndptInt(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc)
{
	return NOTHING;
	}

DWORD CUsbHostHardwareDriver::MakeEndptIso(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc)
{
	return NOTHING;
	}

BOOL CUsbHostHardwareDriver::ResetEndpt(DWORD iEndpt)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::KillEndpt(DWORD iEndpt)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::Transfer(UsbIor &Urb)
{
	return FALSE;
	}

BOOL CUsbHostHardwareDriver::AbortPending(DWORD iEndpt)
{
	return FALSE;
	}

UINT CUsbHostHardwareDriver::AllocAddress(void)
{
	return m_Address.Allocate();
	}

void CUsbHostHardwareDriver::FreeAddress(UINT uAddr)
{
	m_Address.Free(uAddr);
	}

BOOL CUsbHostHardwareDriver::LockDefAddr(BOOL fLock)
{
	return fLock ? m_Address.LockDefault() : m_Address.FreeDefault();
	}

// IUsbEvents

BOOL CUsbHostHardwareDriver::GetDriverInterface(IUsbDriver *&pDriver)
{
	pDriver = (IUsbHostInterfaceDriver *) this;

	return TRUE;
	}

void CUsbHostHardwareDriver::OnBind(IUsbDriver *pDriver)
{
	AfxRelease(m_pLowerDrv);

	m_pLowerDrv = NULL;

	pDriver->QueryInterface(AfxAeonIID(IUsbHostHardwareDriver), (void **) &m_pLowerDrv);
	}

void CUsbHostHardwareDriver::OnInit(void)
{
	m_pUpperDrv->OnInit();
	}

void CUsbHostHardwareDriver::OnStart(void)
{
	m_pUpperDrv->OnStart();
	}

void CUsbHostHardwareDriver::OnStop(void)
{
	m_pUpperDrv->OnStop();
	}
		
// IUsbHostHardwareEvents

void CUsbHostHardwareDriver::OnEvent(void)
{
	}

// Init

void CUsbHostHardwareDriver::InitAddress(void)
{
	m_Address.Init();
	}

// Memory Helpers

PVOID CUsbHostHardwareDriver::AllocNonCached(UINT uAlloc)
{
	uAlloc = RoundUp(uAlloc, 64);

	PVOID pMem  = malloc(uAlloc);

	DWORD Alias = phal->GetNonCachedAlias(DWORD(pMem));

	DWORD Phy   = phal->VirtualToPhysical(DWORD(pMem));

	ProcPurgeDataCache(Phy, uAlloc);

	return PVOID(Alias);
	}

PVOID CUsbHostHardwareDriver::AllocNonCached(UINT uAlloc, UINT uAlign)
{
	uAlign = RoundUp(uAlign, 64);

	uAlloc = RoundUp(uAlloc, 64);

	PVOID pMem  = memalign(uAlign, uAlloc);

	DWORD Alias = phal->GetNonCachedAlias(DWORD(pMem));

	DWORD Phy   = phal->VirtualToPhysical(DWORD(pMem));

	ProcPurgeDataCache(Phy, uAlloc);

	return PVOID(Alias);
	}

PVOID CUsbHostHardwareDriver::AllocNonCachedInit(UINT uSize)
{
	PVOID pMem = AllocNonCached(uSize);

	memset(pMem, 0, uSize);

	return pMem;
	}

PVOID CUsbHostHardwareDriver::AllocNonCachedInit(UINT uSize, UINT uAlign)
{
	PVOID pMem = AllocNonCached(uSize, uAlign);

	memset(pMem, 0, uSize);

	return pMem;
	}

void CUsbHostHardwareDriver::FreeNonCached(PVOID pMem)
{
	if( pMem ) {

		DWORD Alias = phal->GetCachedAlias(DWORD(pMem));

		free(PVOID(Alias));
		}
	}

DWORD CUsbHostHardwareDriver::VirtualToPhysical(PCVOID p)
{
	return phal->VirtualToPhysical(DWORD(p));
	}

DWORD CUsbHostHardwareDriver::VirtualToPhysical(DWORD d)
{
	return phal->VirtualToPhysical(d);
	}
	
PVOID CUsbHostHardwareDriver::PhysicalToVirtual(PVOID p)
{
	return PVOID(phal->PhysicalToVirtual(DWORD(p), false));
	}

PVOID CUsbHostHardwareDriver::PhysicalToVirtual(DWORD d)
{
	return PVOID(phal->PhysicalToVirtual(d, false));
	}

PVOID CUsbHostHardwareDriver::PhysicalToVirtual(PVOID p, bool fCached)
{
	return PVOID(phal->PhysicalToVirtual(DWORD(p), fCached));
	}

PVOID CUsbHostHardwareDriver::PhysicalToVirtual(DWORD d, bool fCached)
{
	return PVOID(phal->PhysicalToVirtual(d, fCached));
	}

// End of File
