
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostControllerDriver_HPP

#define	INCLUDE_UsbHostControllerDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Controller Base Driver
//

class CUsbHostControllerDriver : public CUsbDriver, public IUsbHostControllerDriver, public IUsbHostInterfaceEvents
{
	public:
		// Constructor
		CUsbHostControllerDriver(void);

		// Destructor
		~CUsbHostControllerDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pDriver);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);

		// IUsbHostControllerDriver
		BOOL			  METHOD Bind(IUsbHostStack *pStack);
		UINT			  METHOD GetIndex(void);
		void		          METHOD SetIndex(UINT iIndex);
		void                      METHOD Poll(UINT uLapsed);
		void                      METHOD EnableEvents(BOOL fEnable);
		BOOL			  METHOD ResetPort(UINT uPort);
		IUsbHostInterfaceDriver * METHOD GetInterface(UINT i);
		IUsbHostEnhancedDriver  * METHOD GetEnhanced(void);
		IUsbHostInterfaceDriver * METHOD GetCompanion(UINT i);

		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);

		// IUsbHostInterfaceEvents
		void METHOD OnPortConnect(UsbPortPath &Route);
		void METHOD OnPortRemoval(UsbPortPath &Route);
		void METHOD OnPortCurrent(UsbPortPath &Route);
		void METHOD OnPortEnable(UsbPortPath &Route);
		void METHOD OnPortReset(UsbPortPath &Route);
		void METHOD OnTransfer(UsbIor &Urb);

	protected:
		// Port State
		enum
		{
			stateDetached,
			stateAttached,
			statePowered,
			stateReset,
			stateResetDelay,
			stateCheckSpeed,
			stateConnected,
			stateOverCurrent,
			stateRestart,
			};

		// Data
		IUsbHostControllerEvents * m_pUpperDrv;
		UINT                       m_iIndex;

		// Device
		void OnDeviceArrival(UsbPortPath &Path);
		void OnDeviceRemoval(UsbPortPath &Path);

		// Implementation
		BOOL CheckTimer(UINT &uTimer, UINT uLapsed);
	};

// End of File

#endif
