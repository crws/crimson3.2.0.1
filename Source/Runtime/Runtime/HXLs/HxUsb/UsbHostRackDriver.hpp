
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostRackDriver_HPP

#define	INCLUDE_UsbHostRackDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncBulkDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Rack/Module Driver
//

class CUsbHostRackDriver : public CUsbHostFuncBulkDriver
{
	public:
		// Constructor
		CUsbHostRackDriver(void);

		// Destructor
		~CUsbHostRackDriver(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		
	protected:
		// Data
		UINT m_uPort;

		// Implementation
		void Init(void);
	};

// End of File

#endif
