
#include "Intern.hpp"

#include "UsbHubClearPortFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Clear Port Feature Request
//

// Constructor

CUsbHubClearPortFeatureReq::CUsbHubClearPortFeatureReq(WORD wFeature, WORD wPort)
{
	Init(wFeature, wPort);
	} 

// Operations

void CUsbHubClearPortFeatureReq::Init(WORD wFeature, WORD wPort)
{
	CUsbClassReq::Init();

	m_bRequest  = reqClearFeature;

	m_Direction = dirHostToDev;

	m_Recipient = recPort;

	m_wValue    = wFeature;

	m_wIndex    = wPort;
	}

// End of File
