
#include "Intern.hpp"

#include "UsbHostSmcNicDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Smc Nic Driver
//

// Instantiator

IUsbHostFuncDriver * Create_SmcNicDriver(void)
{
	IUsbNetwork *p = (IUsbNetwork *) New CUsbHostSmcNicDriver;

	return p;
	}

// Constructor

CUsbHostSmcNicDriver::CUsbHostSmcNicDriver(void)
{
	m_pName  = "Host Smc Nic Driver";

	m_Debug  = debugWarn;

	m_uPhy   = 0x01;

	m_uFlags = 0x00;

	m_uMulti = 0;

	m_pMulti = NULL;
	
	AfxGetObject("buffman", 0, IBufferManager, m_pBuffMan);

	m_pLinkUp = Create_ManualEvent();

	m_pWaitTx = Create_Semaphore();

	m_pWaitTx->Signal(1);

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostSmcNicDriver::~CUsbHostSmcNicDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");

	m_pBuffMan->Release();

	m_pLinkUp->Release();

	m_pWaitTx->Release();

	delete [] m_pMulti;
	}

// IUnknown

HRESULT CUsbHostSmcNicDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbNetwork);

	return CUsbHostFuncDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostSmcNicDriver::AddRef(void)
{
	return CUsbHostFuncDriver::AddRef();
	}

ULONG CUsbHostSmcNicDriver::Release(void)
{
	return CUsbHostFuncDriver::Release();
	}

// IHostFuncDriver

void CUsbHostSmcNicDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncDriver::Bind(pDevice, iInterface);
	}

void CUsbHostSmcNicDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncDriver::Bind(pSink);
	}

void CUsbHostSmcNicDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncDriver::GetDevice(pDev);
	}

BOOL CUsbHostSmcNicDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostSmcNicDriver::GetVendor(void)
{
	return CUsbHostFuncDriver::GetVendor();
	}

UINT CUsbHostSmcNicDriver::GetProduct(void)
{
	return CUsbHostFuncDriver::GetProduct();
	}

UINT CUsbHostSmcNicDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostSmcNicDriver::GetSubClass(void)
{
	return devVendor;
	}

UINT CUsbHostSmcNicDriver::GetProtocol(void)
{
	return devVendor;
	}

UINT CUsbHostSmcNicDriver::GetInterface(void)
{
	return CUsbHostFuncDriver::GetInterface();
	}

BOOL CUsbHostSmcNicDriver::GetActive(void)
{
	return CUsbHostFuncDriver::GetActive();
	}

BOOL CUsbHostSmcNicDriver::Open(CUsbDescList const &List)
{
	Trace(debugInfo, "Open");

	if( FindPipes(List) ) {

		m_uNicState = nicInitial;

		m_fPollPend = false;

		return CUsbHostFuncDriver::Open();
		}

	return FALSE;
	}

BOOL CUsbHostSmcNicDriver::Close(void)
{
	Trace(debugInfo, "Close");

	return CUsbHostFuncDriver::Close();
	}

void CUsbHostSmcNicDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncDriver::Poll(uLapsed);

	if( m_uNicState >= nicOpen ) {

		if( !m_fPollPend ) {

			m_pPoll->RecvBulk(PBYTE(&m_dwPoll), sizeof(m_dwPoll), true);

			m_fPollPend = true;
			}

		if( m_fPollPend ) {

			UINT uCount;

			BOOL fResult;

			if( m_pPoll->WaitAsync(0, fResult, uCount) ) {

				if( m_dwPoll & intPhyInt ) {

					DWORD dwStatus;
					
					PhyGet(phyStatus, dwStatus);

					PhyGet(phyInt,    dwStatus);

					PhyPut(phyInt,    dwStatus);

					if( dwStatus & Bit(4) ) {
						
						OnLinkDown();
						}

					if( dwStatus & Bit(6) ) {

						OnLinkUp();
						}
					}

				RegPut(regIntSts, m_dwPoll);

				m_fPollPend = false;
				}
			}
		}
	}

// IUsbNetwork

BOOL CUsbHostSmcNicDriver::Startup(BOOL fFast, BOOL fFull)
{
	Trace(debugInfo, "Startup");

	if( m_uState == usbOpen && m_uNicState == nicClosed ) {

		ResetController();

		ResetPhy();

		ConfigController();

		ConfigPhy(fFast, fFull);

		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbHostSmcNicDriver::Shutdown(void)
{
	Trace(debugInfo, "Shutdown");

	if( m_uNicState >= nicOpen ) {

		if( m_uNicState == nicActive ) {

			OnLinkDown();
			}

		if( m_uState == usbOpen ) {

			m_uNicState = nicClosed;
			
			ResetController();

			ResetPhy();

			ResetCounters();
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUsbHostSmcNicDriver::InitMac(MACADDR const &Addr)
{
	Trace(debugInfo, "InitMac");

	if( m_uNicState == nicInitial ) {

		m_MacAddr   = Addr;

		m_uNicState = nicClosed;

		return TRUE;
		}

	return FALSE;
	}

void CUsbHostSmcNicDriver::ReadMac(MACADDR &Addr)
{
	Addr = m_MacAddr;
	}

UINT CUsbHostSmcNicDriver::GetCapabilities(void)
{
	return nicCheckSums | nicAddSums;
	}

void CUsbHostSmcNicDriver::SetFlags(UINT uFlags)
{
	m_uFlags = uFlags;
	}

BOOL CUsbHostSmcNicDriver::IsLinkActive(void)
{
	return m_uState == usbOpen && m_uNicState == nicActive;
	}

BOOL CUsbHostSmcNicDriver::WaitLink(UINT uTime)
{
	return m_pLinkUp->Wait(uTime);
	}

BOOL CUsbHostSmcNicDriver::SendData(CBuffer *pBuff)
{
	Trace(debugCmds, "SendData(Count=%d)", pBuff->GetSize());

	if( m_uNicState == nicActive ) {

		AddMac(pBuff->GetData());

		UINT  uCount = pBuff->GetSize();

		DWORD &d1    = (DWORD &) pBuff->AddHead(sizeof(DWORD))[0];

		DWORD &d0    = (DWORD &) pBuff->AddHead(sizeof(DWORD))[0];

		d0 = txcmdaFirstSeg | txcmdaLastSeg | uCount;

		d1 = uCount;
		
		m_pWaitTx->Wait(FOREVER);

		if( m_pSend->SendBulk(pBuff->GetData(), pBuff->GetSize(), false) ) {

			m_pWaitTx->Signal(1);

			return TRUE;
			}

		m_pWaitTx->Signal(1);

		Trace(debugWarn, "SendData Command Failed");
		}
	
	return FALSE;
	}

BOOL CUsbHostSmcNicDriver::RecvData(CBuffer * &pBuff)
{
	Trace(debugCmds, "RecvData");

	if( m_uNicState == nicActive ) {

		for(;;) {

			UINT uCount = m_pRecv->RecvBulk(m_bRxData, sizeof(m_bRxData), false);

			if( uCount != NOTHING && uCount != 0 ) {

				DWORD &Flags = (DWORD &) m_bRxData[0];

				if( !(Flags & rxErr) ) {

					if( TakeFrame(m_bRxData) ) {

						uCount -= sizeof(DWORD);

						bool fIp = MotorToHost((WORD &) m_bRxData[16]) == 0x0800;

						if( !(m_uFlags & nicOnlyIp) || fIp ) {

							CBuffer *pWork  = m_pBuffMan->Allocate(uCount);

							if( pWork ) {

								PBYTE pDest = pWork->AddTail(uCount);

								memcpy(pDest, m_bRxData + sizeof(DWORD), uCount);

								if( fIp ) {

									pWork->SetFlag(packetTypeIp);
									}

								pWork->SetFlag(packetTypeValid);

								pWork->SetFlag(packetSumsValid);
				
								pBuff = pWork;

								return TRUE;
								}
							}
						}
					}
				
				continue;
				}

			break;
			}

		Trace(debugWarn, "RecvData Command Failed");
		}

	pBuff = NULL;

	return FALSE;
	}

void CUsbHostSmcNicDriver::GetCounters(NICDIAG &Diag)
{
	if( RegGetStats(false) && RegGetStats(true) ) {

		Diag.m_TxCount = m_TxStats.m_dwGood;
		Diag.m_TxFail  = m_TxStats.m_dwBad;
		Diag.m_TxDisc  = 0;
		Diag.m_RxCount = m_RxStats.m_dwGood;
		Diag.m_RxOver  = m_RxStats.m_dwDropped;
		Diag.m_RxDisc  = m_RxStats.m_dwRunt + m_RxStats.m_dwBad + m_RxStats.m_dwTooLong;
		}
	}

void CUsbHostSmcNicDriver::ResetCounters(void)
{
	}

// Mac Management

BOOL CUsbHostSmcNicDriver::SetMulticast(MACADDR const *pList, UINT uList)
{
	if( pList && uList ) {

		delete [] m_pMulti;

		m_pMulti = New MACADDR[ uList ];

		m_uMulti = uList;

		memcpy(m_pMulti, pList, sizeof(MACADDR) * m_uMulti);
		}
	else {
		delete [] m_pMulti;

		m_pMulti = NULL;

		m_uMulti = 0;
		}

	LoadMultiFilter();
	
	return TRUE;
	}

bool CUsbHostSmcNicDriver::ResetController(void)
{
	if( RegSet(regHwCfg, hwResetL, hwResetL) ) {

		SetTimer(1000);

		while( GetTimer() ) {

			if( RegTst(regHwCfg, hwResetL, ~hwResetL) ) { 
			
				return true;
				}
			}

		Trace(debugWarn, "Reset Timeout");
		}

	return false;
	}

bool CUsbHostSmcNicDriver::ConfigController(void)
{
	// TODO -- Burst Cap

	// TODO -- TX COE, FLOW

	// TODO -- RX COE, VLAN
	
	// TODO -- Multipacket Rx

	ConfigLeds();

	RegPut(regHwCfg,  hwBir);

	RegPut(regMacCr,  0x00000000);

	RegPut(regAddrL,  (PDWORD(&m_MacAddr)[0]) & 0xFFFFFFFF);
	
	RegPut(regAddrH,  (PDWORD(&m_MacAddr)[1]) & 0x0000FFFF);

	RegPut(regFlow,   0x00000000);

	RegPut(regAfxCfg, 0x00F830A1);
	
	RegPut(regIntCtl, intPhyInt);

	m_uNicState = nicOpen;
	
	return true;
	}

void CUsbHostSmcNicDriver::ConfigLeds(void)
{
	RegPut(regLedGpioCfg, (1 << 24) | (1 << 20));
	}

void CUsbHostSmcNicDriver::StartRx(void)
{
	RegSet(regRxCfg, rxcfgFlush, rxcfgFlush);

	RegSet(regMacCr, macRxEn, macRxEn);
	}

void CUsbHostSmcNicDriver::StartTx(void)
{
	RegSet(regMacCr, macTxEn, macTxEn);

	RegSet(regTxCfg, txcfgOn, txcfgOn);

	RegSet(regTxCfg, txcfgFlush, txcfgFlush);
	}

void CUsbHostSmcNicDriver::StopRx(void)
{
	RegSet(regIntSts, intRxStop, ~intRxStop);

	RegSet(regMacCr, macRxEn, ~macRxEn);

	for( UINT i = 0; i < 20; i ++ ) {

		if( RegTst(regIntSts, intRxStop, intRxStop) ) {

			return;
			}
		}
	
	Trace(debugWarn, "Failed to stop rx path");
	}

void CUsbHostSmcNicDriver::StopTx(void)
{
	RegSet(regIntSts, intTxStop, ~intTxStop);

	RegSet(regMacCr, macTxEn, ~macTxEn);

	for( UINT i = 0; i < 20; i ++ ) {

		if( RegTst(regIntSts, intTxStop, intTxStop) ) {

			return;
			}
		}
	
	Trace(debugWarn, "Failed to stop tx path");
	}

void CUsbHostSmcNicDriver::FlushRx(void)
{
	RegSet(regRxCfg, rxcfgFlush, rxcfgFlush);
	}

void CUsbHostSmcNicDriver::FlushTx(void)
{
	RegSet(regTxCfg, txcfgFlush, txcfgFlush);
	}

void CUsbHostSmcNicDriver::AddMac(PBYTE pData)
{
	pData[ 6] = m_MacAddr.m_Addr[0];
	pData[ 7] = m_MacAddr.m_Addr[1];
	pData[ 8] = m_MacAddr.m_Addr[2];
	pData[ 9] = m_MacAddr.m_Addr[3];
	pData[10] = m_MacAddr.m_Addr[4];
	pData[11] = m_MacAddr.m_Addr[5];
	}

void CUsbHostSmcNicDriver::LoadMultiFilter(void)
{
	DWORD Upper = 0;

	DWORD Lower = 0;

	for( UINT n = 0; n < m_uMulti; n++ ) {

		UINT uHash = GetMacHash(m_pMulti[n]);

		if( uHash < 32 ) {

			Lower |= (1 << uHash);
			}
		else {
			uHash -= 32;

			Upper |= (1 << uHash);
			}
		}

	RegPut(regHashH, Upper);

	RegPut(regHashL, Lower);
	
	UINT SetMask	= macHpFilt;

	UINT ClearMask	= macPrms | macMcpas;

	DWORD MacCr = 0;

	RegGet(regMacCr, MacCr);

	MacCr |= SetMask;

	MacCr &= ~ClearMask;

	RegPut(regMacCr, MacCr);
	}

UINT CUsbHostSmcNicDriver::GetMacHash(MACADDR const &Mac)
{
	DWORD Poly = 0xEDB88320;

	DWORD CRC  = 0xFFFFFFFF;

	for( UINT i = 0; i < 6; i++ ) {

		DWORD Data = Mac.m_Addr[i];

		for( UINT n = 0; n < 8; n++ ) {

			BYTE bit = (CRC ^ Data) & 1;

			CRC >>= 1;

			if( bit ) {

				CRC ^= Poly;
				}

			Data >>= 1;
			}
		}
	
	return	((CRC & 0x01) << 5) |
		((CRC & 0x02) << 3) |
		((CRC & 0x04) << 1) |
		((CRC & 0x08) >> 1) |
		((CRC & 0x10) >> 3) |
		((CRC & 0x20) >> 5) ;
	}

// Phy Management

bool CUsbHostSmcNicDriver::ResetPhy(void)
{
	DWORD dwValue;

	if( RegSet(regPmCtrl, pmPhyReset, pmPhyReset) ) {

		SetTimer(1000);

		while( GetTimer() ) {

			if( RegTst(regPmCtrl, pmPhyReset, ~pmPhyReset) ) {

				return true;
				}
			}

		Trace(debugWarn, "Phy Reset Timeout");
		}

	return false;
	}

void CUsbHostSmcNicDriver::ConfigPhy(bool fAllowFast, bool fAllowFull)
{
	DWORD dwCaps = 0x00000021;

	if( fAllowFast ) {
		
		dwCaps |= Bit(7);
		}

	if( fAllowFull ) {
		
		dwCaps |= ((dwCaps & 0x000000A0) << 1);
		}

	PhyPut(phyAutoNegAdv, dwCaps);

	PhyPut(phyControl,    Bit(12));

	PhyPut(phyInt,        0xFFFF);

	PhyPut(phyIntMask,    Bit(6) | Bit(4));
	}

void CUsbHostSmcNicDriver::OnLinkUp(void)
{
	Trace(debugInfo, "Link Up");

	StartRx();

	StartTx();

	m_uNicState = nicActive;

	while( m_pWaitTx->Wait(5) );

	m_pWaitTx->Signal(1);

	m_pLinkUp->Set();
	}

void CUsbHostSmcNicDriver::OnLinkDown(void)
{
	Trace(debugInfo, "Link Down");

	if( m_uNicState == nicActive ) {
				
		m_pLinkUp->Clear();

		StopRx();

		StopTx();

		while( m_pWaitTx->Wait(5) );

		m_pWaitTx->Signal(1);

		m_uNicState = nicOpen;
		}
	}

// Prom Access

bool CUsbHostSmcNicDriver::PromRead(WORD wAddr, PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "PromRead(Addr=0x%4.4X, Count=%d)", wAddr, uCount);

	if( (wAddr + uCount - 1) & ~promAddrMask ) { 

		Trace(debugWarn, "Invalid Address");

		return false;
		}

	while( uCount -- ) {

		if( PromRead(wAddr, *pData) ) {
		
			wAddr ++;

			pData ++;

			continue;
			}

		return false;
		}

	return true;
	}

bool CUsbHostSmcNicDriver::PromWrite(WORD wAddr, PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "PromWrite(Addr=0x%4.4X, Count=%d)", wAddr, uCount);

	if( (wAddr + uCount - 1) & ~promAddrMask ) { 

		Trace(debugWarn, "Invalid Address");

		return false;
		}

	while( uCount -- ) {

		if( PromWrite(wAddr, *pData) ) {
		
			wAddr ++;

			pData ++;

			continue;
			}

		return false;
		}

	return true;
	}

bool CUsbHostSmcNicDriver::PromRead(WORD wAddr, BYTE &bData)
{
	Trace(debugCmds, "PromRead(Addr=0x%4.4X)", wAddr);

	if( wAddr & ~promAddrMask ) {

		Trace(debugWarn, "Invalid Address");

		return false;
		}

	if( PromCmd(promRead | wAddr) ) {

		DWORD dwData;
				
		if( RegGet(regPromData, dwData) ) {

			bData = dwData & promDataMask;

			Trace(debugData, "EEPROM Data = 0x%2.2X", bData);

			return true;
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::PromWrite(WORD wAddr, BYTE bData)
{
	Trace(debugCmds, "PromWrite(Addr=0x%4.4X, Data=0x%2.2X)", wAddr, bData);

	if( wAddr & ~promAddrMask ) {

		Trace(debugWarn, "Invalid Address");

		return false;
		}

	if( PromCmd(promWriteEn) ) {

		if( RegPut(regPromData, bData) ) {
				
			DWORD dwCmd = promWrite | wAddr;

			return PromCmd(dwCmd);
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::PromCmd(UINT uCmd)
{
	Trace(debugCmds, "PromCmd(Cmd=0x%8.8X)", uCmd);

	if( RegPut(regPromCmd, promTimeout) ) {

		DWORD dwCmd = promBusy | uCmd;

		if( RegPut(regPromCmd, dwCmd) ) {

			SetTimer(10000);

			while( GetTimer() ) {
			
				DWORD dwStatus;
		
				if( RegGet(regPromCmd, dwStatus) ) {

					if( dwStatus & promBusy ) {

						Sleep(10);
				
						continue;
						}

					if( dwStatus & promTimeout ) {

						break;
						}

					return true;
					}
					
				return false;
				}

			Trace(debugWarn, "EEPROM Timeout");
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::PromReload(void)
{
	Trace(debugCmds, "PromReload");

	DWORD dwStatus;

	if( PromCmd(promReload) ) {

		DWORD dwStatus;

		if( RegGet(regPromCmd, dwStatus) ) {

			if( dwStatus & promLoaded ) {

				RegPut(regPromCmd, promLoaded);
			
				return true;
				}

			Trace(debugWarn, "Reload Failed");
			}
		}
	
	return false;
	}

// Ram Access

bool CUsbHostSmcNicDriver::RamRead(DWORD dwAddr, DWORD &dwData)
{
	Trace(debugCmds, "RamRead(Addr=0x%8.8X)", dwAddr);

	if( RamWait(0) ) {

		if( RegPut(regRamAddr, dwAddr) && RegPut(regRamCmd, 0) ) {
			
			if( RamWait(50) ) {

				if( RegGet(regRamData0, dwData) ) {

					Trace(debugData, "Ram Data : 0x%8.8X", dwData);

					return true;
					}
				}
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::RamWrite(DWORD dwAddr, DWORD  dwData)
{
	Trace(debugCmds, "RamWrite(Addr=0x%8.8X, Data=0x%8.8X)", dwAddr, dwData);

	if( RamWait(0) ) {

		if( RegPut(regRamAddr, dwAddr) ) {
			
			if( RegPut(regRamData0, dwData) ) {

				if( RegPut(regRamCmd, 1) ) {

					return RamWait(50);
					}
				}
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::RamWait(UINT uTimeout)
{
	SetTimer(uTimeout);

	do {
		DWORD dwStatus;

		if( RegGet(regRamSel, dwStatus) ) {
			
			if( (dwStatus & ramselReady) != ramselReady ) {

				if( uTimeout ) {

					Sleep(10);
					}

				continue;
				}

			return true;
			}
		
		} while( GetTimer() );

	Trace(debugWarn, "Ram Timeout");

	return false;
	}

// Mii Register Access

bool CUsbHostSmcNicDriver::PhyGet(UINT uReg, DWORD &dwData)
{
	Trace(debugCmds, "PhyGet(Reg=0x%4.4X)", uReg);

	if( PhyWait(0) ) {
	
		DWORD dwAddr = ((m_uPhy & 0x1F) << 11) | ((uReg & 0x1F) << 6) | miiRead;

		if( RegPut(regMiiAddr, dwAddr) ) {

			if( PhyWait(1000) ) {

				if( RegGet(regMiiData, dwData) ) {

					Trace(debugData, "PHY Read Data = 0x%8.8X", dwData);
					
					return true;
					}
				}
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::PhyPut(UINT uReg, DWORD dwData)
{
	Trace(debugCmds, "PhyPut(Reg=0x%4.4X, Data=0x%8.8X)", uReg, dwData);

	if( PhyWait(0) ) {
	
		if( RegPut(regMiiData, dwData) ) {

			DWORD dwAddr = ((m_uPhy & 0x1F) << 11) | ((uReg & 0x1F) << 6) | miiWrite;

			if( RegPut(regMiiAddr, dwAddr) ) {

				return PhyWait(1000);
				}
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::PhyWait(UINT uTimeout)
{
	SetTimer(uTimeout);

	do {
		DWORD dwStatus;
		
		if( RegGet(regMiiAddr, dwStatus) ) {
			
			if( dwStatus & miiBusy ) { 
						
				if( uTimeout ) {
				
					Sleep(10);
					}
				
				continue;
				}

			return true;
			}

		} while( GetTimer() );
	
	Trace(debugWarn, "PHY Not Ready");
		
	return false;
	}

// Gpio

bool CUsbHostSmcNicDriver::GpioSet(UINT uBit, bool fState)
{
	Trace(debugCmds, "GpioSet(Bit=%2.2d, State=%d)", uBit, fState);

	if( uBit < 8 ) {

		DWORD dwData;

		if( RegGet(regGpioCfg, dwData) ) {

			dwData |=  Bit(uBit +  8);

			dwData &= ~Bit(uBit + 24);
			
			if( fState ) {
				
				dwData |= Bit(uBit);
				}
			else {
				dwData &= ~Bit(uBit);
				}

			return RegPut(regGpioCfg, dwData);
			}
		}

	return false;
	}

// Register Access

bool CUsbHostSmcNicDriver::RegSet(WORD wReg, DWORD dwMask, DWORD dwData)
{
	DWORD dwValue;

	if( RegGet(wReg, dwValue) ) {

		dwData  &=  dwMask;

		dwValue &= ~dwMask;

		dwValue |= dwData;

		return RegPut(wReg, dwValue);
		}

	return false;
	}

bool CUsbHostSmcNicDriver::RegTst(WORD wReg, DWORD dwMask, DWORD dwData)
{
	DWORD dwValue;

	if( RegGet(wReg, dwValue) ) {

		return (dwValue & dwMask) == (dwData & dwMask);
		}

	return false;
	}

bool CUsbHostSmcNicDriver::RegGet(WORD wReg, DWORD &dwData)
{
	Trace(debugCmds, "RegGet(Reg=0x%4.4X)", wReg);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdReadReg;

	Req.m_wIndex    = wReg;

	Req.m_wLength   = sizeof(DWORD);
	
	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&dwData), sizeof(dwData)) == sizeof(DWORD) ) {

		Trace(debugData, "Reg Data = 0x%8.8X", dwData); 

		return true;
		}

	Trace(debugWarn, "Communication Error");

	return false;
	}

bool CUsbHostSmcNicDriver::RegPut(WORD wReg, DWORD dwData)
{
	Trace(debugCmds, "RegPut(Reg=0x%4.4X, Data=0x%8.8X)", wReg, dwData);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdWriteReg;

	Req.m_wIndex    = wReg;

	Req.m_wLength   = sizeof(DWORD);
	
	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&dwData), sizeof(dwData)) != sizeof(DWORD) ) {

		Trace(debugWarn, "Communication Error");

		return false;
		}

	return true;
	}

bool CUsbHostSmcNicDriver::RegGetStats(bool fRx)
{
	Trace(debugCmds, "RegGetStats(%s)", fRx ? "RX" : "TX");

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdGetStats;

	Req.m_wIndex    = fRx ? 0 : 1;

	Req.m_wLength   = fRx ? sizeof(CRxStats) : sizeof(CTxStats);
	
	PBYTE pData     = fRx ? PBYTE(&m_RxStats) : PBYTE(&m_TxStats);

	UINT  uSize     = Req.m_wLength;

	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, pData, uSize) == uSize ) {

		if( fRx ) {

			Trace(debugData, "RX Status Block");
				
			Trace(debugData, "Good      0x%8.8X", m_RxStats.m_dwGood);
			Trace(debugData, "Crc       0x%8.8X", m_RxStats.m_dwCrc);
			Trace(debugData, "Runt      0x%8.8X", m_RxStats.m_dwRunt);
			Trace(debugData, "Align     0x%8.8X", m_RxStats.m_dwAlignment);
			Trace(debugData, "Size      0x%8.8X", m_RxStats.m_dwTooLong);
			Trace(debugData, "Col       0x%8.8X", m_RxStats.m_dwCollision);
			Trace(debugData, "Bad       0x%8.8X", m_RxStats.m_dwBad);
			Trace(debugData, "Dropped   0x%8.8X", m_RxStats.m_dwDropped);
			}
		else {
			Trace(debugData, "TX Status Block");
				
			Trace(debugData, "Good      0x%8.8X", m_TxStats.m_dwGood);
			Trace(debugData, "Pause     0x%8.8X", m_TxStats.m_dwPause);
			Trace(debugData, "Single    0x%8.8X", m_TxStats.m_dwSingleCol);
			Trace(debugData, "Mult      0x%8.8X", m_TxStats.m_dwMultipleCol);
			Trace(debugData, "Excessive 0x%8.8X", m_TxStats.m_dwExcessiveCol);
			Trace(debugData, "Late      0x%8.8X", m_TxStats.m_dwLateCol);
			Trace(debugData, "Underrun  0x%8.8X", m_TxStats.m_dwUnderrun);
			Trace(debugData, "Deferral  0x%8.8X", m_TxStats.m_dwDeferral);
			Trace(debugData, "Carrier   0x%8.8X", m_TxStats.m_dwCarrier);
			Trace(debugData, "Bad       0x%8.8X", m_TxStats.m_dwBad);
			}

		return true;
		}

	Trace(debugWarn, "Communication Error");

	return false;
	}

// Implementation

bool CUsbHostSmcNicDriver::FindPipes(CUsbDescList const &List)
{
	UINT iIndex;

	UsbInterfaceDesc *pInt = (UsbInterfaceDesc *) List.FindInterface(iIndex, m_iInt);

	if( pInt && pInt->m_bEndpoints == 3 ) {

		UsbEndpointDesc *pEp0 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		UsbEndpointDesc *pEp1 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		UsbEndpointDesc *pEp2 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		if( pEp0 && pEp1 && pEp2 ) {

			m_pCtrl = m_pDev->GetCtrlPipe();

			m_pRecv = m_pDev->GetPipe(pEp0->m_bAddr, pEp0->m_bDirIn);

			m_pSend = m_pDev->GetPipe(pEp1->m_bAddr, pEp1->m_bDirIn);

			m_pPoll = m_pDev->GetPipe(pEp2->m_bAddr, pEp2->m_bDirIn); 

			m_iRecv = pEp0->m_bAddr | Bit(7);

			m_iSend = pEp1->m_bAddr;

			m_iPoll = pEp2->m_bAddr | Bit(7);

			Trace(debugCmds, "IN  Endpoint 0x%2.2X", m_iRecv);
	
			Trace(debugCmds, "OUT Endpoint 0x%2.2X", m_iSend);

			Trace(debugCmds, "INT Endpoint 0x%2.2X", m_iPoll);
			
			return m_pCtrl != NULL && m_pSend != NULL && m_pRecv != NULL && m_pPoll != NULL;
			}
		}

	return false;
	}

bool CUsbHostSmcNicDriver::TakeFrame(PBYTE pData)
{
	if( m_pMulti ) {

		// NOTE -- If we have multicast enabled, we have to filter for
		// addresses that get by the hash filter in the chip but which are
		// not included in our multicast list. Note that we explicitly
		// test for broadcast and allow it, as it won't be in the list but
		// it does meet the definition of the multicast address.

		MACADDR const *pAddr = (MACADDR const *) pData;

		if( pAddr->m_Addr[0] & 0x01 ) {

			static MACADDR Cast = { { 0xFF, 0xFF,
						  0xFF, 0xFF,
						  0xFF, 0xFF
						  } };

			if( memcmp(pAddr, &Cast, sizeof(MACADDR)) ) {

				UINT n;

				for( n = 0; n < m_uMulti; n++ ) {

					if( !memcmp(pAddr, m_pMulti + n, sizeof(MACADDR)) ) {

						break;
						}
					}

				if( n == m_uMulti ) {

					return false;
					}
				}
			}
		}

	return true;
	}

// End of File
