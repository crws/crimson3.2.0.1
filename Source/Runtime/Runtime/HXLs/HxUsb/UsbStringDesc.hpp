
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbStringDesc_HPP

#define	INCLUDE_UsbStringDesc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb String Descriptor
//

class CUsbStringDesc : public UsbStringDesc
{
	public:
		// Constructor
		CUsbStringDesc(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;
		BOOL IsNull(void) const;
		UINT GetCount(void) const;
		
		// Operations
		void Init(void);
		void Set(PCTXT pText);
		void Set(WORD  wData);
		void Set(PWORD pwData, UINT uCount);
		void Get(PTXT pText, UINT n) const;

		// Debug
		void Debug(void);
	};

// End of File

#endif
