
#include "Intern.hpp"

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Standard Device Request
//

// Constructor

CUsbStandardReq::CUsbStandardReq(void)
{
	Init();
	} 

// Operations

void CUsbStandardReq::Init(void)
{
	CUsbDeviceReq::Init();

	m_Type = reqStandard;
	}

// End of File
