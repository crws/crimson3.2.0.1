
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbFuncAppDriver_HPP

#define	INCLUDE_UsbFuncAppDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Application Driver
//

class CUsbFuncAppDriver : public CUsbDriver, public IUsbFuncEvents
{
	public:
		// Constructor
		CUsbFuncAppDriver(void);

		// Destructor
		~CUsbFuncAppDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);

		// IUsbFuncEvents
		void METHOD OnState(UINT uState);
		BOOL METHOD OnSetupClass(UsbDeviceReq &Req);
		BOOL METHOD OnSetupVendor(UsbDeviceReq &Req);
		BOOL METHOD OnSetupOther(UsbDeviceReq &Req);
		BOOL METHOD OnGetDevice(UsbDesc &Desc);
		BOOL METHOD OnGetQualifier(UsbDesc &Desc);
		BOOL METHOD OnGetConfig(UsbDesc &Desc);
		BOOL METHOD OnGetInterface(UsbDesc &Desc);
		BOOL METHOD OnGetEndpoint(UsbDesc &Desc);
		BOOL METHOD OnGetString(UsbDesc *&pDesc, UINT iIndex);

	protected:
		// Data
		IUsbFuncCompDriver * m_pLowerDrv;
		UINT                 m_iInterface;
		IEvent             * m_pRunning;
		bool		     m_fOpen;

		// Transfer Helpers
		BOOL Shutdown(void);
		BOOL WaitRunning(DWORD dwTimeout);
		UINT GetMaxPacket(UINT iEndpt);
		BOOL SendCtrlStatus(UINT iEndpt);
		BOOL RecvCtrlStatus(UINT iEndpt);
		BOOL SendCtrl(UINT iEndpt, PCBYTE pData, UINT uLen);
		BOOL RecvCtrl(UINT iEndpt, PBYTE  pData, UINT uLen);
		UINT SendBulk(UINT iEndpt, PCBYTE pData, UINT uLen, UINT uTimeout);
		UINT RecvBulk(UINT iEndpt, PBYTE  pData, UINT uLen, UINT uTimeout);
		BOOL SetStall(UINT iEndpt, BOOL fStall);
		BOOL GetStall(UINT iEndpt);
	};

// End of File

#endif
