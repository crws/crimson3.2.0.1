
#include "Intern.hpp"

#include "UsbSetFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Feature Standard Device Request
//

// Constructor

CUsbSetFeatureReq::CUsbSetFeatureReq(void)
{
	Init();
	} 

// Operations

void CUsbSetFeatureReq::Init(void)
{
	CUsbClearFeatureReq::Init();

	m_bRequest = reqSetFeature;
	}

// End of File
