
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiBulkStatus_HPP

#define	INCLUDE_ScsiBulkStatus_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Status Block Wrapper
//

class CScsiBulkStatus : public ScsiBulkStatus
{
	public:
		// Constructor
		CScsiBulkStatus(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Attributes
		bool IsValid(void) const;
		bool IsPassed(void) const;
		bool IsFailed(void) const;
		
		// Operations
		void Init(void);

		// Debug
		void Dump(void);
	};

// End of File

#endif
