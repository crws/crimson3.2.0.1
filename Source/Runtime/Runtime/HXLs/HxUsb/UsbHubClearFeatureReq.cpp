
#include "Intern.hpp"

#include "UsbHubClearFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Clear Feature Request
//

// Constructor

CUsbHubClearFeatureReq::CUsbHubClearFeatureReq(WORD wFeature)
{
	Init(wFeature);
	} 

// Operations

void CUsbHubClearFeatureReq::Init(WORD wFeature)
{
	CUsbClassReq::Init();

	m_bRequest  = reqClearFeature;

	m_Direction = dirHostToDev;

	m_Recipient = recHub;

	m_wValue    = wFeature;
	}

// End of File
