
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDeviceQual_HPP

#define	INCLUDE_UsbDeviceQual_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUsbDeviceDesc;

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Qaulifier Descriptor
//

class CUsbDeviceQual : public UsbDeviceQual
{
	public:
		// Constructor
		CUsbDeviceQual(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Init
		void Init(void);
		void InitFrom(CUsbDeviceDesc const &Dev);

		// Debug
		void Debug(void);
	};

// End of File

#endif
