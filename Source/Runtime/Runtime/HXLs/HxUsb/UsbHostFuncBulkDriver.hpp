
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostFuncBulkDriver_HPP

#define	INCLUDE_UsbHostFuncBulkDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Function Bulk Driver
//

class CUsbHostFuncBulkDriver : public CUsbHostFuncDriver, public IUsbHostBulkDriver
{
	public:
		// Constructor
		CUsbHostFuncBulkDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);
		
		// IUsbHostBulkDriver
		BOOL METHOD SendBulk(PCBYTE pData, UINT uCount);
		UINT METHOD RecvBulk(PBYTE  pData, UINT uCount);
		BOOL METHOD SendBulk(PCBYTE pData, UINT uCount, BOOL fAsync);
		UINT METHOD RecvBulk(PBYTE  pData, UINT uCount, BOOL fAsync);
		BOOL METHOD SendBulk(CBuffer *pBuff);
		BOOL METHOD RecvBulk(CBuffer *&pBuff);
		UINT METHOD WaitAsyncSend(UINT uTimeout);
		UINT METHOD WaitAsyncRecv(UINT uTimeout);
		BOOL METHOD KillAsyncSend(void);
		BOOL METHOD KillAsyncRecv(void);

	protected:
		// Data
		IUsbPipe   * m_pCtrl;
		IUsbPipe   * m_pSend;
		IUsbPipe   * m_pRecv;
		UINT	     m_iSend;
		UINT	     m_iRecv;

		// Implementation
		BOOL FindPipes(CUsbDescList const &List);
	};

// End of File

#endif
