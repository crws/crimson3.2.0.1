
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostExtensible_HPP

#define	INCLUDE_UsbHostExtensible_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostHardwareDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb eXtensible Host Controller Interface
//

class CUsbHostExtensible : public CUsbHostHardwareDriver
{
	public:
		// Constructor
		CUsbHostExtensible(void);

		// Destructor
		~CUsbHostExtensible(void);

		// IUsbDriver
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);
		
		// IUsbHostInterfaceDriver
		UINT  METHOD GetType(void);
		UINT  METHOD GetPortCount(void);
		BOOL  METHOD GetPortConnect(UINT iPort);
		BOOL  METHOD GetPortEnabled(UINT iPort);
		BOOL  METHOD GetPortCurrent(UINT iPort);
		BOOL  METHOD GetPortSuspend(UINT iPort);
		BOOL  METHOD GetPortReset(UINT iPort);
		BOOL  METHOD GetPortPower(UINT iPort);
		UINT  METHOD GetPortSpeed(UINT iPort);
		void  METHOD SetPortEnable(UINT iPort, BOOL fSet);
		void  METHOD SetPortSuspend(UINT iPort, BOOL fset);
		void  METHOD SetPortResume(UINT iPort, BOOL fSet);
		void  METHOD SetPortReset(UINT iPort, BOOL fSet);
		void  METHOD SetPortPower(UINT iPort, BOOL fSet);
		void  METHOD EnableEvents(BOOL fEnable);
		BOOL  METHOD SetEndptDevAddr(DWORD iEndpt, BYTE bAddr);
		UINT  METHOD GetEndptDevAddr(DWORD iEndpt);
		BOOL  METHOD SetEndptAddr(DWORD iEndpt, BYTE bAddr);
		BOOL  METHOD SetEndptMax(DWORD iEndpt, WORD wMaxPacket);
		BOOL  METHOD SetEndptHub(DWORD iEndpt, BYTE bHub, BYTE bPort);
		DWORD METHOD MakeDevice(IUsbDevice *pDevice);
		void  METHOD KillDevice(DWORD iDev);
		BOOL  METHOD CheckConfig(DWORD iDev);
		DWORD METHOD MakeEndptCtrl(DWORD iDev, UINT uSpeed);
		DWORD METHOD MakeEndptAsync(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc);
		DWORD METHOD MakeEndptInt(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc);
		BOOL  METHOD KillEndpt(DWORD iIndex);
		BOOL  METHOD ResetEndpt(DWORD iIndex);
		BOOL  METHOD Transfer(UsbIor &Urb);
		BOOL  METHOD AbortPending(DWORD iEndpt);
		UINT  METHOD AllocAddress(void);
		void  METHOD FreeAddress(UINT uAddr);
		
		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);
		
		// IUsbHostHardwareEvents
		void METHOD OnEvent(void);

	protected:
		// Constants
		enum
		{
			constCmdRingSize	= 8,
			constCmdRingLinks	= 1,
			constEventRingSize	= 16,
			constEventRingSegments	= 1,
			constTrbRingSize	= 8,
			constTrbRingLinks	= 1,
			constMaxEndpoints	= 32,
			constMaxEndpointData	= 8192,
			};

		// Capability Registers
		enum
		{
			capCapLength		= 0x0000 / sizeof(DWORD),
			capHcsParams1		= 0x0004 / sizeof(DWORD),
			capHcsParams2		= 0x0008 / sizeof(DWORD),
			capHcsParams3		= 0x000C / sizeof(DWORD),
			capHccParams1		= 0x0010 / sizeof(DWORD),
			capDoorbell		= 0x0014 / sizeof(DWORD),
			capRuntime 		= 0x0018 / sizeof(DWORD),
			capHccParams2		= 0x001C / sizeof(DWORD),
			};

		// Operational Registers
		enum
		{
			regUsbCmd		= 0x0000 / sizeof(DWORD),
			regUsbSts		= 0x0004 / sizeof(DWORD),
			regPageSize		= 0x0008 / sizeof(DWORD),
			regDevCtrl		= 0x0014 / sizeof(DWORD),
			regCmdRingLo		= 0x0018 / sizeof(DWORD),
			regCmdRingHi		= 0x001C / sizeof(DWORD),
			regDevCtxLo		= 0x0030 / sizeof(DWORD),
			regDevCtxHi		= 0x0034 / sizeof(DWORD),
			regConfig		= 0x0038 / sizeof(DWORD),
			regPortSC		= 0x0400 / sizeof(DWORD),
			regPortPower		= 0x0404 / sizeof(DWORD),
			regPortLink		= 0x0408 / sizeof(DWORD),
			regPortLpm		= 0x040C / sizeof(DWORD),
			};

		// Runtime Registers
		enum 
		{
			regMfIndex		= 0x0000 / sizeof(DWORD),
			regIntMan		= 0x0020 / sizeof(DWORD),
			regIntMod		= 0x0024 / sizeof(DWORD),
			regIntTableSize		= 0x0028 / sizeof(DWORD),
			regIntTableLo		= 0x0030 / sizeof(DWORD),
			regIntTableHi		= 0x0034 / sizeof(DWORD),
			regIntDequeueLo		= 0x0038 / sizeof(DWORD),
			regIntDequeueHi		= 0x003C / sizeof(DWORD),
			};

		// Structural 1
		struct CSParams1
		{
			DWORD	m_dwMaxSlots	:  8;
			DWORD	m_dwMaxInts	: 11;
			DWORD	m_dwReserved    :  5;
			DWORD	m_dwMaxPorts	:  8;
			};

		// Structural 2
		struct CSParams2
		{
			DWORD	m_dwIst		:  4;
			DWORD	m_dwErstMax	:  4;
			DWORD			: 13;
			DWORD	m_dwMaxBuffHi	:  5;
			DWORD	m_dwScratchRes	:  1;
			DWORD	m_dwMaxBuffLo	:  5;
			};

		// Structural 3
		struct CSParams3
		{
			DWORD	m_dwU1ExitLat	:  8;
			DWORD			:  8;
			DWORD	m_dwU2ExitLat	: 16;
			};

		// Capability 1
		struct CCParams1
		{
			DWORD	m_dw64Bit	:  1;
			DWORD	m_dwBandNeg	:  1;
			DWORD	m_dwCtxSize	:  1;
			DWORD	m_dwPortPwrCtrl	:  1;
			DWORD	m_dwPortLeds	:  1;
			DWORD	m_dwLightReset	:  1;
			DWORD	m_dwLatencyTol	:  1;
			DWORD	m_dwNoSecondSid	:  1;
			DWORD	m_dwParseEvData	:  1;
			DWORD	m_dwShortPacket	:  1;
			DWORD	m_dwSec		:  1;
			DWORD	m_dwContigFrame	:  1;
			DWORD	m_dwMaxPriStm	:  4;
			DWORD	m_dwExtCaps	: 16;
			};

		// Capability 2
		struct CCParams2
		{
			DWORD	m_dwU3c		:  1;
			DWORD	m_dwCmc		:  1;
			DWORD	m_dwFsc		:  1;
			DWORD	m_dwCtc		:  1;
			DWORD	m_dwLec		:  1;
			DWORD	m_dwCic		:  1;
			DWORD			: 26;
			};

		// Extended Capability Codes
		enum
		{
			codeLegacy		=  1,
			codeProtocol		=  2,
			codePower		=  3,
			codeVirtual		=  4,
			codeMessage		=  5,
			codeMemory		=  6,
			codeDebug		= 10,
			debugMessageEx		= 17,
			};

		// Command Register Bits
		enum 
		{
			cmdRunStop		= Bit( 0),
			cmdReset		= Bit( 1),
			cmdIntEnable		= Bit( 2),
			cmdSysErrEnable		= Bit( 3),
			cmdLightReset		= Bit( 7),
			cmdSave			= Bit( 8),
			cmdRestore		= Bit( 9),
			cmdWrapEnable		= Bit(10),
			cmdU3Enable		= Bit(11),
			cmdShortEnable		= Bit(12),
			cmdCemEnable		= Bit(13),
			};

		// Status Register Bits
		enum 
		{
			statHalted		= Bit( 0),
			statSysErr		= Bit( 2),
			statEvent		= Bit( 3),
			statPort		= Bit( 4),
			statSave		= Bit( 8),
			statRestore		= Bit( 9),
			statSaveRestorErr	= Bit(10),
			statNotReady		= Bit(11),
			statCtrlErr		= Bit(12),
			};

		// Port Register Bits
		enum
		{
			portConnect		= Bit( 0),
			portEnabled		= Bit( 1),
			portOverCurrent		= Bit( 3),
			portReset		= Bit( 4),
			portLinkStatus		= 5,
			portPower		= Bit( 9),
			portSpeed		= 10,
			portLeds		= 14,
			portLinkStrobe		= Bit(16),
			portConnectChange	= Bit(17),
			portEnableChange	= Bit(18),
			portWarmResetChange	= Bit(19),
			portOverCurrentChange	= Bit(20),
			portResetChange		= Bit(21),
			portLinkStateChange	= Bit(22),
			portConfigError		= Bit(23),
			portColdAttach		= Bit(24),
			portWakeOnConnect	= Bit(25),
			portWakeOnDisconnect	= Bit(26),
			portWakeOnOverCurrent	= Bit(27),
			portDeviceRemovable	= Bit(30), 
			portWarmReset		= Bit(31), 
			};

		// Command Ring Register Bits
		enum
		{
			ringCycleState		= Bit( 0),
			ringStop		= Bit( 1),
			ringAbort		= Bit( 2),
			ringRunning		= Bit( 3),
			};

		// Event Ring Bits
		enum
		{
			eventBusy		= Bit(3),
			};

		// Interrupt Management Bits
		enum
		{
			intPending		= Bit(0),
			intEnable		= Bit(1),
			};

		// Doorbell Targets
		enum
		{
			doorbellCommand		=  0,
			doorbellControl		=  1,
			doorbellEndpt1Out	=  2,
			doorbellEndpt1In	=  3,
			};

		// Endpoint Types
		enum
		{
			endptInvalid		=  0,
			endptIsochOut		=  1,
			endptBulkOut		=  2,
			endptIntOut		=  3,
			endptControl		=  4,
			endptIsochIn		=  5, 
			endptBulkIn		=  6,
			endptIntIn		=  7,
			};

		// Endoint State
		enum
		{
			stateDisabled		=  0,
			stateRunning		=  1,
			stateHalted		=  2, 
			stateStopped		=  3,
			stateError		=  4,
			};

		// Slot State
		enum
		{
			slotDisabled		=  0,
			slotDefault		=  1, 
			slotAddressed		=  2, 
			slotConfigured		=  3,
			};

		// Speed Ids
		enum
		{
			speedFull		= 1,
			speedLow		= 2,
			speedHigh		= 3, 
			speedSuper		= 4,
			};

		// TRB Types
		enum
		{
			trbReserved		=  0,
			trbNormal		=  1,
			trbSetup		=  2,
			trbData			=  3,
			trbStatus		=  4,
			trbIsoch		=  5,
			trbLink			=  6,
			trbEvent		=  7,
			trbNop			=  8,
			trbEnableSlot		=  9,
			trbDisableSlot		= 10,
			trbAddressDev		= 11,
			trbConfigEndpt		= 12,
			trbEvalContext		= 13,
			trbResetEndpt		= 14,
			trbStopEndpt		= 15,
			trbSetDequePtr		= 16,
			trbResetDev		= 17,
			trbForceEvent		= 18,
			trbNegBandwidth		= 19,
			trbSetLatencyTol	= 20,
			trbGetPortBandwidth	= 21,
			trbForceHeader		= 22,
			trbNopCmd		= 23,
			trbTransfer		= 32,
			trbCmdDone		= 33,
			trbPortStatus		= 34,
			trbBandwidthReq		= 35,
			trbDoorbell		= 36,
			trbHostCtrl		= 37,
			trbDevNotify		= 38,
			trbIndexWrap		= 39,
			trbVendorFirst		= 48,
			trbVendorLast		= 63,
			};
		
		// TRB Complettion Codes
		enum
		{
			codeInvalid		=  0,
			codeSuccess		=  1,
			codeDataBuff		=  2,
			codeBabble		=  3,
			codeTransact		=  4,
			codeError		=  5,
			codeStall		=  6,
			codeResource		=  7,
			codeBandwidth		=  8,
			codeNoSlots		=  9,
			codeBadStream		= 10,
			codeSlotDisabled	= 11,
			codeEndptDisabled	= 12,
			codeShort		= 13,
			codeRingUnderrun	= 14,
			codeRingOverrun		= 15,
			codeVFEventRingFull	= 16,
			codeBadParam		= 17,
			codeBandwidthOverrun	= 18,
			codeContextState	= 19,
			codeNoPing		= 20,
			codeEventRingFull	= 21,
			codeDevice		= 22,
			codeMissedService	= 23,
			codeCmdRingStopped	= 24,
			codeCmdAborted		= 25,
			codeStopped		= 26,
			codeBadLength		= 27,
			codeStoppedShortPacket	= 28,
			codeBadMaxLatency	= 29,
			codeIsochOverrun	= 31,
			codeEventLost		= 32,
			codeUndefined		= 33,
			codeBadStreamId		= 34,
			codeSecondaryBandwidth	= 35,
			codeSplitTransact	= 36,
			};
		
		// TRB Flags
		enum
		{
			flagCycle		= Bit(0),
			flagToggle		= Bit(1),
			flagIoShort		= Bit(2),
			flagIoComp		= Bit(5),
			flagImmediate		= Bit(6),
			flagNoSetAddr		= Bit(9),
			flagDeconfig		= Bit(9),
			flagPreserve		= Bit(9),
			};

		// Transfer Types
		enum
		{
			setupNoDataStage	= 0,
			setupOutData		= 2,
			setupInData		= 3,
			};
	
		// TRB
		struct ALIGN(16) CTrb
		{
			DWORD		m_dwParam0;
			DWORD		m_dwParam1;
			DWORD		m_dwStatus;
			DWORD		m_dwFlags	: 10;
			DWORD		m_dwType	:  6;
			DWORD		m_dwControl	: 16;
			};

		// Normal Transfer Request Block
		struct ALIGN(16) CTrbNormal
		{
			DWORD		m_dwBufPtrLo;
			DWORD		m_dwBufPtrHi;
			DWORD		m_dwLength	: 17;
			DWORD		m_dwSize	:  5;
			DWORD		m_dwIntTarget	: 10;
			DWORD		m_dwCylce	:  1;
			DWORD		m_dwNextValid	:  1;
			DWORD		m_dwIntOnShort	:  1;
			DWORD		m_dwNoSnoop	:  1;
			DWORD		m_dwChain	:  1;
			DWORD		m_dwIntOnComp	:  1;
			DWORD		m_dwActualData	:  1;
			DWORD				:  2;
			DWORD		m_dwBlockEvent	:  1;
			DWORD		m_dwType	:  6;
			DWORD				: 16;
			};

		// Completion Event TRB
		struct ALIGN(64) CCmdEventTrb
		{
			DWORD		m_dwPointerLo;
			DWORD		m_dwPointerHi;
			DWORD		m_dwParam	: 24;
			DWORD		m_dwCode	:  8;
			DWORD		m_dwFlags	: 10;
			DWORD		m_dwType	:  6;
			DWORD		m_dwVfid	:  8;
			DWORD		m_dwSlotId	:  8;
			};

		// Slot Context
		struct ALIGN(32) CSlotCtx
		{
			DWORD		m_dwRouteStr	: 20;
			DWORD		m_dwSpeed	:  4;
			DWORD				:  1;
			DWORD		m_dwMulti	:  1;
			DWORD		m_dwHub		:  1;
			DWORD volatile	m_dwContexts	:  5;
			DWORD		m_dwLatency	: 16;
			DWORD		m_dwRootHubPort	:  8;
			DWORD		m_dwPortCount	:  8;
			DWORD		m_dwHubSlot	:  8;
			DWORD		m_dwHubPort	:  8;
			DWORD		m_dwThinkTime	:  2;
			DWORD				:  4;
			DWORD		m_dwInterrupter	: 10;
			DWORD volatile	m_dwDevAddr	:  8;
			DWORD				: 19;
			DWORD volatile	m_dwState	:  5;
			DWORD		m_dwPad[4];
			};

		// Endpoint Context
		struct ALIGN(32) CEndptCtx
		{
			DWORD volatile	m_dwState	:  3;
			DWORD				:  5;
			DWORD		m_dwMult	:  2;
			DWORD		m_dwMaxPrimStms	:  5;
			DWORD		m_dwLinearStms	:  1;
			DWORD		m_dwInterval	:  8;
			DWORD		m_dwServiceTime	:  8;
			DWORD				:  1;
			DWORD		m_dwErrorCount	:  2;
			DWORD		m_dwType	:  3;
			DWORD				:  1;
			DWORD		m_dwHostDisable	:  1;
			DWORD		m_dwMaxBusrt	:  8;
			DWORD		m_dwMaxPacket	: 16;
			DWORD		m_dwDequeueState:  1;
			DWORD				:  3;
			DWORD		m_dwDequeuePtrLo: 28;
			DWORD		m_dwDequeuePtrHi    ;
			DWORD		m_dwAvgTrbLen	: 16;
			DWORD		m_dwMaxEsit	: 16;
			DWORD		m_dwPad[3];
			};

		// Stream Context
		struct ALIGN(32) CStmCtx
		{
			DWORD		m_dwDequeueState:  1;
			DWORD		m_dwType	:  3;
			DWORD		m_dwDequeuePtrLo: 28;
			DWORD		m_dwDequeuePtrHi    ;
			DWORD		m_dwStopped	: 24;
			DWORD				:  8;
			DWORD		m_dwPad;
			};

		// Input Control Context
		struct ALIGN(32) CControlCtx
		{
			DWORD		m_dwDropCtx;
			DWORD		m_dwAddCtx;
			DWORD		m_dwPad[5];
			DWORD		m_dwConfig	: 8;
			DWORD		m_dwInterface	: 8;
			DWORD		m_dwAlternate	: 8;
			DWORD				: 8;
			};

		// Port Bandwidth Context
		struct CPortCtx
		{
			BYTE		m_bBandwidth[32];
			};

		// Event Segment Table
		struct ALIGN(32) CSegment
		{
			DWORD		m_dwBaseLo;
			DWORD		m_dwBaseHi;
			DWORD		m_dwSize;
			DWORD		m_dwPad;
			};

		// Endpoint Object
		struct CEndpoint
		{
			UINT            m_iIndex;
			CEndptCtx     * m_pContext;
			CTrb          * m_pTrbRing;
			UsbIor	      * m_pUrbRing[constTrbRingSize];
			PVBYTE          m_pUrbData[constTrbRingSize];
			UINT volatile	m_iTrbHead;
			UINT volatile	m_iTrbTail;
			bool volatile	m_fTrbPCS;
			bool volatile   m_fTrbCCS;
			PVBYTE	        m_pData;
			};
		
		// Device Object
		struct CDevice
		{
			IUsbDevice    * m_pDevice; 
			PVDWORD	        m_pInputCtx;
			PVDWORD         m_pOutputCtx;
			CEndpoint     * m_pEndptObj[constMaxEndpoints];
			};

		// Data
		IMutex			* m_pLock;
		PVDWORD	                  m_pCaps;
		PVDWORD			  m_pCapsEx;
		PVDWORD                   m_pOperate;
		PVDWORD			  m_pRuntime;
		PDWORD			  m_pDoorbell;
		PDWORD			  m_pDevCtxList;
		CDevice  	       ** m_pDevObjList;
		CTrb			* m_pCmdRing;
		UINT volatile		  m_iCmdHead;
		UINT volatile		  m_iCmdTail;
		bool volatile		  m_fCmdCycle;
		CCmdEventTrb volatile	* m_pCmdComp;
		ISemaphore		* m_pCmdLimit;
		IEvent			* m_pCmdEvent;
		CTrb  volatile 		* m_pEventRing;
		CSegment		* m_pEventTable;
		UINT			  m_iEventHead;
		UINT			  m_iEventTail;
		bool			  m_fEventCycle;
		PDWORD			  m_pScratchpad;
		PVOID			  m_pScratchBuf;
		CUsbHostAddress		  m_Address;

		// Event Handlers
		void OnCoreEvent(void);
		void OnTransferEvent(CTrb const &Trb);
		void OnCommandEvent(CTrb const &Trb);
		void OnPortStatusEvent(CTrb const &Trb);
		void OnHostEvent(CTrb const &Trb);
		void OnPortConnect(UINT nPort, bool fConnect);
		void OnPortReset(UINT nPort, bool fReset);
		void OnPortEnable(UINT nPort, bool fEnable);
		void OnPortCurrent(UINT nPort, bool fOverCurrent);

		// Capabilities
		UINT   GetCapsLen(void) const;
		PDWORD EnumExtCaps(UINT &iEnum) const;
		PDWORD GetExtCapsType(UINT uCode) const;
		UINT   GetVersion(void) const;
		UINT   GetMaxSlots(void) const;
		UINT   GetNumPorts(void) const;
		bool   GetPowerCtrl(void) const;
		bool   GetContext64(void) const; 
		UINT   GetPageSize(void) const;
		UINT   GetScratchCount(void) const;

		// Operations
		void InitAddress(void);
		void InitController(void);
		void ResetController(void);
		void StartController(void);
		void StopController(void);
		void EnableEvents(void);
		void DisableEvents(void);

		// Transfers
		UINT AllocTransfer(CEndpoint *pEndpt);
		bool QueueTransfer(CEndpoint *pEndpt, UINT iSlot);
		bool ExecTransfer(CEndpoint *pEndpt, UINT iSlot);
		void CompTransfer(CEndpoint *pEndpt, CTrb const &Trb);
		void AbortTransfers(CEndpoint *pEndpt);

		// Commands
		bool CmdNop(void);
		UINT CmdEnableSlot(void);
		bool CmdDisableSlot(UINT iSlot);
		bool CmdAddressDevice(UINT iSlot, PCDWORD pCtx);
		bool CmdConfigEndpoint(UINT iSlot, PCDWORD pCtx);
		bool CmdDeConfigEndpoint(UINT iSlot);
		bool CmdEvalContext(UINT iSlot, PCDWORD pCtx);
		bool CmdResetEndpoint(UINT iSlot, UINT iEndpt, bool fPreserve);
		bool CmdStopEndpoint(UINT iSlot, UINT iEndpt);
		bool CmdSetDeqeuePtr(UINT iSlot, UINT iEndpt, UINT iStm, DWORD dwPtr);
		bool CmdResetDevice(UINT iSlot);

		// Command Ring
		void MakeCmdRing(void);
		void InitCmdRing(void);
		void FreeCmdRing(void);
		UINT AllocCmd(void);
		bool ExecCmd(void);

		// Event Ring
		void MakeEventRing(void);
		void InitEventRing(void);
		void FreeEventRing(void);

		// Scratch Pad
		void MakeScratchpad(void);
		void FreeScratchpad(void);

		// Device
		void	  MakeDeviceList(void);
		void	  InitDeviceList(void);
		void	  FreeDeviceList(void);
		PVDWORD   MakeDeviceCtx(void);
		PVDWORD   MakeInputCtx(void);
		void	  FreeDeviceCtx(PVDWORD pCtx);
		void      FreeInputCtx(PVDWORD pCtx);
		CDevice * MakeDeviceObj(void);
		void      FreeDeviceObj(CDevice *pObj);
		PVDWORD   GetContext(PVDWORD pBase, UINT i);
		void	  SetDeviceCtxItem(UINT iSlot, PVDWORD pCtx);
		void	  CheckHubConfig(UINT iSlot);
		void      CheckDevConfig(UINT iSlot);
		DWORD	  FindDeviceSlot(BYTE bAddr);

		// Endpoints
		CEndpoint * MakeEndpointObj(void);
		void        InitEndpointObjRing(CEndpoint *pObj);
		void        FreeEndpointObj(CEndpoint *pObj);
		UINT        AddrToIndex(BYTE bAddr, bool fDirIn);
		DWORD       FindEndpointInterval(UsbEndpointDesc const &Desc, UINT uSpeed);

		// Port Helpers
		void  PortPower(BOOL fOn);
		DWORD PortGet(UINT i, DWORD Mask);
		void  PortSet(UINT i, DWORD Mask);
		void  PortClr(UINT i, DWORD Mask);

		// Debug
		void DumpHardwareCaps(void) const;
		void DumpHardwareCapsEx(void) const;
		void DumpHardware(void) const;
		void DumpRuntime(void) const;
		void DumpCmdRing(void) const;
		void DumpEventRing(void) const;
		void DumpTrbRing(CTrb const *pTrb, UINT uCount) const;
		void DumpTrbData(CTrb const &Trb, UINT i) const;
		void DumpContext(CSlotCtx const &Ctx, UINT i) const;
		void DumpContext(CEndptCtx const &Ctx, UINT n) const;
		void DumpContext(CStmCtx const &Ctx) const;
		void DumpContext(CControlCtx const &Ctx) const;
		void DumpContext(CPortCtx const &Ctx) const;
		void DumpOutputContext(PCDWORD pCtx);
		void DumpInputContext(PCDWORD pCtx);
	};

// End of File

#endif
