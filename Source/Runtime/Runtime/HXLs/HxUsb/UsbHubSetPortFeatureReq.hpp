
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubSetPortFeatureReq_HPP

#define	INCLUDE_UsbHubSetPortFeatureReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Set Port Feature Request
//

class CUsbHubSetPortFeatureReq : public CUsbClassReq
{
	public:
		// Constructor
		CUsbHubSetPortFeatureReq(WORD wFeature, WORD wPort);

		// Operations
		void Init(WORD wFeature, WORD wPort);
	};

// End of File

#endif
