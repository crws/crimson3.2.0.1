
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiCapicity_HPP

#define	INCLUDE_ScsiCapicity_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Capacity Data Object
//

class CScsiCapacity : public ScsiCapacity
{
	public:
		// Constructor
		CScsiCapacity(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
