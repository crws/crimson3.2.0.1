
#include "Intern.hpp"

#include "ScsiCmdStartStop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Start Stop 
//

// Constructor

CScsiCmdStartStop::CScsiCmdStartStop(void)
{
	}

// Endianess

void CScsiCmdStartStop::HostToScsi(void)
{
	}

void CScsiCmdStartStop::ScsiToHost(void)
{
	}

// Operations

void CScsiCmdStartStop::Init(bool fStart)
{
	memset(this, 0, sizeof(ScsiCmdStartStop));

	m_bOpcode    = cmdStartStop;
	
	m_bStart     = fStart;

	m_bImmediate = true;

	m_bPower     = true;

	m_bEject     = false;
	}

// End of File
