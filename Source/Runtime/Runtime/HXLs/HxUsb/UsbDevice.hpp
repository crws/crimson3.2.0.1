
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDevice_HPP

#define	INCLUDE_UsbDevice_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDeviceDesc.hpp"

#include "UsbDescList.hpp"

#include "UsbHubDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Device
//

class CUsbDevice : public IUsbDevice, public IUsbDeviceEvents
{
	public:
		// Constructor
		CUsbDevice(void);

		// Destructor
		~CUsbDevice(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDevice
		UsbDeviceDesc const      & METHOD GetDevDesc(void);
		CUsbDescList  const      & METHOD GetCfgDesc(void);
		UsbHubDesc    const      & METHOD GetHubDesc(void);
		UINT                       METHOD GetSpeed(void);
		BYTE		           METHOD GetAddr(void);
		IUsbPipe	         * METHOD GetCtrlPipe(void);
		IUsbPipe	         * METHOD GetPipe(BYTE bAddr, BOOL fDirIn);
		void                       METHOD SetHostStack(IUsbHostStack *pStack);
		IUsbHostStack            * METHOD GetHostStack(void);	
		IUsbHostControllerDriver * METHOD GetHostC(void);
		IUsbHostInterfaceDriver  * METHOD GetHostI(void);
		IUsbHubDriver		 * METHOD GetParentHub(void);
		UINT			   METHOD GetDriverCount(void);
		IUsbHostFuncDriver       * METHOD GetDriver(UINT iInt);
		UsbPortPath const        & METHOD GetPortPath(void);
		UsbTreePath const        & METHOD GetTreePath(void);
		void			   METHOD SetObject(UINT i, PVOID pObject);
		PVOID			   METHOD GetObject(UINT i);
		BOOL			   METHOD SetRemoveLock(BOOL fLock);
		BOOL			   METHOD HasError(void) const;
		void			   METHOD SetError(void);
		UINT			   METHOD GetState(void) const;
		PCTXT			   METHOD GetStateText(void) const;
		BOOL			   METHOD IsHub(void) const;
		BOOL			   METHOD IsReady(void) const;
		BOOL			   METHOD IsRemoved(void) const;
		void			   METHOD SetPath(UsbPortPath const &Path);
		void			   METHOD SetPath(UsbTreePath const &Path);
		void			   METHOD SetHostC(IUsbHostControllerDriver *pHost);
		void			   METHOD SetHostI(IUsbHostInterfaceDriver  *pHost);
		void			   METHOD SetSpeed(UINT uSpeed);
		BOOL			   METHOD SetAltInterface(UINT iInt, UINT iAlt);
		BOOL		           METHOD GetString(UINT iStr, PTXT pStr, UINT uLen);
		
		// IUsbDeviceEvents
		void METHOD OnArrival(void);
		void METHOD OnRemoval(void);
		void METHOD OnPoll(UINT uLapsed);

		// Device State
		enum 
		{
			devInit,
			devAddress,
			devDiscover,
			devConfigure,
			devReady,
			devRemoved,
			devDestroyed,
			devError,
			};
		
	protected:
		// Pipe Context
		struct CPipeCtx
		{
			UsbEndpointDesc  * m_pDesc;
			IUsbPipe         * m_pPipe;
			IUsbPipeEvents   * m_pEvent;
			};

		// Interface Context
		struct CInterfaceCtx
		{
			IUsbHostFuncDriver * m_pDriver;
			UsbInterfaceDesc   * m_pDesc;
			CPipeCtx             m_PipeList[4];
			PVOID		     m_pObject;
			}; 

		// Data
		ULONG			   m_uRefs;
		IUsbHostStack            * m_pStack;
		CUsbDevice		 * m_pNext;
		CUsbDevice		 * m_pPrev;
		IUsbHostControllerDriver * m_pHostC;
		IUsbHostControllerEvents * m_pEvent;
		IUsbHostInterfaceDriver  * m_pHostI;
		UsbPortPath                m_Path;
		UsbTreePath		   m_Tree;
		CUsbDeviceDesc	           m_DevDesc;
		CUsbDescList               m_CfgDesc;
		CUsbHubDesc                m_HubDesc;
		UINT			   m_uSpeed;
		UINT			   m_uState;
		BYTE			   m_bAddr;
		IUsbPipe		 * m_pCtrl;
		CInterfaceCtx              m_IntList[16];
		INT			   m_nLock;
		UINT			   m_uDrvCount;
		UINT			   m_iDev;

		// State Handlers
		BOOL OnInit(void);
		BOOL OnAddress(void);
		BOOL OnDiscover(void);
		BOOL OnConfigure(void);
		BOOL OnDestroy(void);
		BOOL OnReady(UINT uLapsed);

		// Device 
		void MakeDevice(void);
		void FreeDevice(void);

		// Interface
		void MakeInterfaces(UINT uCount);
		void FreeInterfaces(void);

		// Drivers
		void MakeDrivers(void);
		void OpenDrivers(void);
		void PollDrivers(UINT uLapsed);
		void StopDrivers(void);
		void FreeDrivers(void);

		// Pipes
		void MakePipes(void);
		void MakePipes(UINT iInterface);
		void PollPipes(UINT uLapsed);
		void FreePipes(UINT iInterface);
		void FreePipes(void);

		// Control Pipe
		BOOL SetAddr(BYTE bAddr);
		BOOL InitControl(void);
		void FreeAddress(void);
		void FreeControl(void);

		// Check
		bool CheckClass(bool fStrict);
		bool CheckDevice(void);
		bool CheckConfig(void);

		// Friend Classes
		friend class CUsbDeviceList;
	};

// End of File

#endif
