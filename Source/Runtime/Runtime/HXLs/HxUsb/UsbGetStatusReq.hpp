
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbGetStatusReq_HPP

#define	INCLUDE_UsbGetStatusReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Status Standard Device Requeset
//

class CUsbGetStatusReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbGetStatusReq(void);

		// Operations
		void Init        (void);
		void SetDevice   (void);
		void SetInterface(WORD wInterface);
		void SetEndpoint (WORD wEndpoint);
	};

// End of File

#endif
