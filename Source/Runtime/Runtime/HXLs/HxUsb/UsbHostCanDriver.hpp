
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostCanDriver_HPP

#define	INCLUDE_UsbHostCanDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostModuleDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Can Driver
//

class CUsbHostCanDriver : public CUsbHostModuleDriver, public IUsbHostCan
{
	public:
		// Constructor
		CUsbHostCanDriver(void);

		// Destructor
		~CUsbHostCanDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostModule
		BOOL METHOD Reset(void);
		BOOL METHOD ReadVersion(BYTE Version[16]); 
		BOOL METHOD SendHeartbeat(void);
		
		// IUsbHostCan
		BOOL METHOD SetBaudRate(UINT uBaud);
		BOOL METHOD SetFilter(UINT iFilter, DWORD Data, DWORD Mask);
		BOOL METHOD SetLed(UINT iLed, UINT uState);
		BOOL METHOD SetRun(BOOL fRun);
		BOOL METHOD SetLatency(UINT uLatency);
		BOOL METHOD GetError(void);
		BOOL METHOD SendFrame(UsbCanFrame *pFrame, UINT uCount);
		UINT METHOD RecvFrame(UsbCanFrame *pFrame, UINT uCount);
		BOOL METHOD SendFrameAsync(UsbCanFrame *pFrame, UINT uCount);
		UINT METHOD RecvFrameAsync(UsbCanFrame *pFrame, UINT uCount);
		UINT METHOD WaitAsyncSend(UINT uTimeout);
		UINT METHOD WaitAsyncRecv(UINT uTimeout);
		BOOL METHOD KillAsyncSend(void);
		BOOL METHOD KillAsyncRecv(void);
		
	protected:
		// Commands
		enum
		{
			cmdSetBaud	= 0x01,
			cmdSetFilter	= 0x02,
			cmdSetLed	= 0x03,
			cmdSetRun	= 0x04,
			cmdGetError	= 0x05,
			};
	};

// End of File

#endif
