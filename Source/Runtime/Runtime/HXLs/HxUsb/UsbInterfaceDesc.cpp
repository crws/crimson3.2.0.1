
#include "Intern.hpp"

#include "UsbInterfaceDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Interface Descriptor
//

// Constructor

CUsbInterfaceDesc::CUsbInterfaceDesc(void)
{
	Init();
	}

// Endianess

void CUsbInterfaceDesc::HostToUsb(void)
{
	}

void CUsbInterfaceDesc::UsbToHost(void)
{
	}

// Attributes

BOOL CUsbInterfaceDesc::IsValid(void) const
{
	return m_bType == descInterface && m_bLength == sizeof(UsbInterfaceDesc);
	}

// Init

void CUsbInterfaceDesc::Init(void)
{
	memset(this, 0, sizeof(UsbInterfaceDesc));
	
	m_bLength = sizeof(UsbInterfaceDesc);
	
	m_bType	  = descInterface;
	}

// Debug

void CUsbInterfaceDesc::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\nUsb Interface Descriptor\n");

	AfxTrace("Type         = %d\n",      m_bType);
	AfxTrace("Len          = %d\n",      m_bLength);
	AfxTrace("This         = 0x%2.2X\n", m_bThis);
	AfxTrace("Alt Settings = 0x%2.2X\n", m_bAltSetting);
	AfxTrace("Endpoints    = 0x%2.2X\n", m_bEndpoints);
	AfxTrace("Class        = 0x%2.2X\n", m_bClass);
	AfxTrace("Sub Class    = 0x%2.2X\n", m_bSubClass);
	AfxTrace("Protocol     = 0x%2.2X\n", m_bProtocol);
	AfxTrace("iName        = 0x%2.2X\n", m_bNameIdx);

	#endif
	}

// End of File
