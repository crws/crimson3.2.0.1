
#include "Intern.hpp"

#include "ScsiCmdInquiry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Inquiry
//

// Constructor

CScsiCmdInquiry::CScsiCmdInquiry(void)
{
	}

// Endianess

void CScsiCmdInquiry::HostToScsi(void)
{
	}

void CScsiCmdInquiry::ScsiToHost(void)
{
	}

// Operations

void CScsiCmdInquiry::Init(void)
{
	memset(this, 0, sizeof(ScsiCmdInquiry));

	m_bOpcode = cmdInquiry;
	}

void CScsiCmdInquiry::Init(BYTE bAllocLen)
{
	Init();	

	m_bAllocLength = bAllocLen;
	}

void CScsiCmdInquiry::InitVital(BYTE bAllocLen, BYTE bPageOpcode)
{
	Init(bAllocLen);

	m_bEvpd       = true;

	m_bPageOpcode = bPageOpcode;
	}

void CScsiCmdInquiry::InitSupport(BYTE bAllocLen, BYTE bPageOpcode)
{
	Init(bAllocLen);

	m_bCmdDt      = true;

	m_bPageOpcode = bPageOpcode;
	}

// End of File
