
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHid_HPP

#define	INCLUDE_UsbHid_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hid Sub Class
//

enum
{
	classNone		= 0x00,
	classBoot		= 0x01,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Request Types
//

enum
{
	reqGetReport		= 0x01,
	reqGetIdle		= 0x02,
	reqGetProtocol		= 0x03,
	reqSetReport		= 0x09,
	reqSetIdle		= 0x0A,
	reqSetProtocol		= 0x0B,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Descriptor Types
//

enum
{
	descHid			= 0x21,
	descReport		= 0x22,
	descPhysical		= 0x23,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Protocols
//

enum
{
	protBoot		= 0x00,
	protReport		= 0x01,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Item Types
//

enum
{
	itemMain		= 0x00,
	itemGlobal		= 0x01,
	itemLocal		= 0x02,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Global Items
//

enum
{
	globalUsagePage		= 0x00,
	globalLogMin		= 0x01,
	globalLogMax		= 0x02,
	globalPhyMin		= 0x03,
	globalPhyMax		= 0x04,
	globalUnitExp		= 0x05,
	globalUnit		= 0x06,
	globalRepSize		= 0x07,
	globalRepID		= 0x08,
	globalRepCount		= 0x09,
	globalPush		= 0x0A,
	globalPop		= 0x0B,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Local Items
//

enum
{
	localUsage		= 0x00,
	localUsageMin		= 0x01,
	localUsageMax		= 0x02,
	localDesIndex		= 0x03,
	localDesMin		= 0x04,
	localDesMax		= 0x05,
	localStrIndex		= 0x07,
	localStrMin		= 0x08,
	localStrMax		= 0x09,
	localDelim		= 0x0A,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Main Items
//

enum
{
	mainInput		= 0x08,
	mainOutput		= 0x09,
	mainCollection		= 0x0A,
	mainFeature		= 0x0B,
	mainEnd			= 0x0C,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Usage Pages
//

enum
{
	pageDesktop		= 0x01,
	pageKeyboard		= 0x07,
	pageLeds		= 0x08,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Desktop
//

enum
{
	desktopMouse		= 0x02,
	desktopKeyboard		= 0x06,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Report Types
//

enum
{
	reportInput		= 0x01,
	reportOutput		= 0x02,
	reportFeature		= 0x03,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Descriptor List
//

#pragma pack(1)

struct HidList	
{
	BYTE	m_bType;
	WORD	m_wSize;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Descriptor
//

struct HidDesc : public UsbDesc
{
	WORD	m_wHid;
	BYTE	m_bCountry;
	BYTE	m_bNum;
	HidList	m_List[1];
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Item Base
//

struct HidItem
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	m_bSize : 2;
	BYTE    m_bType : 2;
	BYTE    m_bTag  : 4;

	#else

	BYTE    m_bTag  : 4;
	BYTE    m_bType : 2;
	BYTE	m_bSize : 2;

	#endif

	bool IsItemShort(void) const { return m_bTag != 0x0F; };
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Short Item
//

struct HidShort : public HidItem
{
	BYTE	m_bData[];
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Long Item
//

struct HidLong : public HidItem
{
	BYTE	m_bDataSize;
	BYTE	m_bLongTag;
	BYTE	m_bData[];
	};

#pragma pack()

// End of File

#endif
