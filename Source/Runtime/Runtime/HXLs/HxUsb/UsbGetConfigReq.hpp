
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbGetConfigReq_HPP

#define	INCLUDE_UsbGetConfigReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Config Standard Device Requeset
//

class CUsbGetConfigReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbGetConfigReq(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
