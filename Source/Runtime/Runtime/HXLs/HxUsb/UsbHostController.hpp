
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostController_HPP

#define	INCLUDE_UsbHostController_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostControllerDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Controller Driver
//

class CUsbHostController : public CUsbHostControllerDriver
{
	public:
		// Constructor
		CUsbHostController(void);

		// IUsbDriver
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);

		// IUsbHostControllerDriver
		void                      METHOD Poll(UINT uLapsed);
		void                      METHOD EnableEvents(BOOL fEnable);
		BOOL			  METHOD ResetPort(UINT uPort);
		IUsbHostInterfaceDriver * METHOD GetInterface(UINT i);
		IUsbHostEnhancedDriver  * METHOD GetEnhanced(void);
		IUsbHostInterfaceDriver * METHOD GetCompanion(UINT i);

		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);

		// IUsbHostInterfaceEvents
		void METHOD OnPortConnect(UsbPortPath &Route);
		void METHOD OnPortRemoval(UsbPortPath &Route);
		void METHOD OnPortCurrent(UsbPortPath &Route);

	protected:
		// Flags
		enum 
		{
			flagReqReset	= Bit(0)
			};

		// Port Context
		struct CPort
		{
			UINT                      m_uState;
			UINT                      m_uTimer;
			IUsbHostInterfaceDriver * m_pHost;
			UINT                      m_uSpeed;
			UINT			  m_uFlags;
			};

		// Data
		IUsbHostEnhancedDriver   * m_pEnhanced;
		IUsbHostInterfaceDriver  * m_pCompanion[2];
		CPort                    * m_pPorts;
		UINT                       m_uPorts;

		// Ports
		void InitPorts(void);
		void MakePorts(void);
		void KillPorts(void);
		UINT FindPorts(void);
		void PollPorts(UINT uLapsed);
	};

// End of File

#endif
