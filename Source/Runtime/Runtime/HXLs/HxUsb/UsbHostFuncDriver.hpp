
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostFuncDriver_HPP

#define	INCLUDE_UsbHostFuncDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Function Driver
//

class CUsbHostFuncDriver : public CUsbDriver, public IUsbHostFuncDriver
{
	public:
		// Constructor
		CUsbHostFuncDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);
		
	protected:
		// Data
		UINT                 m_uState;
		IUsbDevice         * m_pDev;
		IUsbHostFuncEvents * m_pSink;
		UINT                 m_iInt;

		// Implementation
		BOOL Open(void);
	};

// End of File

#endif
