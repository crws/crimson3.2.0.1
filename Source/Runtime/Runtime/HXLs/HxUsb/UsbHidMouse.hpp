
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHidMouse_HPP

#define	INCLUDE_UsbHidMouse_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hid Framework
//

#include "Hid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHidMapper.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hid Mouse Mapper
//

class CUsbHidMouse : public CUsbHidMapper
{
	public:
		// Constructor
		CUsbHidMouse(void);
				
		// Destructor
		~CUsbHidMouse(void);
		
		// IUsbHidMapper
		BOOL SetReport(PCBYTE pReport, UINT uSize);
		UINT GetUsagePage(void);
		UINT GetUsageType(void);
		void SetConfig(PCBYTE pConfig, UINT uSize);
		void Poll(void);

	protected:
		//  Contants
		enum 
		{
			constDelay = 500,
			constRate  = 10,
			};

		// Data
		IInputQueue * m_pInput;
		IPointer    * m_pPointer;
		IDisplay    * m_pDisplay;
		bool	      m_fEnable;
		INT	      m_nScale;
		UINT	      m_uDelay;
		int	      m_xMaxPos;
		int	      m_yMaxPos;
		int	      m_xMaxRaw;
		int	      m_yMaxRaw;
		int	      m_xPos;
		int	      m_yPos;
		int	      m_xRaw;
		int	      m_yRaw;
		bool	      m_fShow;
		UINT	      m_uHide;
		bool	      m_fPress;
		UINT	      m_uRepeat;
		
		// Helpers
		void Configure(void);
		bool ParseReport(void);
		
		// Implementation
		void CheckRepeat(void);
		void ResetPointer(void);
		void ShowPointer(void);
		void HidePointer(void);
		void PollPointer(void);
		void Move(int &nData, int nMax, int nRel);
		void Clip(int &nData, int nMax, int nMargin);
		INT  ScaleDown(int nData);
		INT  ScaleUp(int nData);
	};

// End of File

#endif
