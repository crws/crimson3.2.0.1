
#include "Intern.hpp"

#include "UsbHostModuleDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Module Driver
//

// Constructor

CUsbHostModuleDriver::CUsbHostModuleDriver(void)
{
	m_pName  = "Host Module Driver";

	m_Debug  = debugWarn;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostModuleDriver::~CUsbHostModuleDriver(void)
{
	}

// IUnknown

HRESULT CUsbHostModuleDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostModule);

	return CUsbHostFuncBulkDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostModuleDriver::AddRef(void)
{
	return CUsbHostFuncBulkDriver::AddRef();
	}

ULONG CUsbHostModuleDriver::Release(void)
{
	return CUsbHostFuncBulkDriver::Release();
	}

// IUsbHostFuncDriver

void CUsbHostModuleDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncBulkDriver::Bind(pDevice, iInterface);
	}

void CUsbHostModuleDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncBulkDriver::Bind(pSink);
	}

void CUsbHostModuleDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncBulkDriver::GetDevice(pDev);
	}

BOOL CUsbHostModuleDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncBulkDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostModuleDriver::GetVendor(void)
{
	return CUsbHostFuncBulkDriver::GetVendor();
	}

UINT CUsbHostModuleDriver::GetProduct(void)
{
	return CUsbHostFuncBulkDriver::GetProduct();
	}

UINT CUsbHostModuleDriver::GetClass(void)
{
	return CUsbHostFuncBulkDriver::GetClass();
	}

UINT CUsbHostModuleDriver::GetSubClass(void)
{
	return CUsbHostFuncBulkDriver::GetSubClass();
	}

UINT CUsbHostModuleDriver::GetProtocol(void)
{
	return CUsbHostFuncBulkDriver::GetProtocol();
	}

UINT CUsbHostModuleDriver::GetInterface(void)
{
	return CUsbHostFuncBulkDriver::GetInterface();
	}

BOOL CUsbHostModuleDriver::GetActive(void)
{
	return CUsbHostFuncBulkDriver::GetActive();
	}

BOOL CUsbHostModuleDriver::Open(CUsbDescList const &List)
{
	return CUsbHostFuncBulkDriver::Open(List);
	}

BOOL CUsbHostModuleDriver::Close(void)
{
	return CUsbHostFuncBulkDriver::Close();
	}

void CUsbHostModuleDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncBulkDriver::Poll(uLapsed);
	}

// IUsbHostModule

BOOL CUsbHostModuleDriver::Reset(void)
{
	Trace(debugCmds, "Reset");
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdReset;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostModuleDriver::ReadVersion(BYTE bVersion[16])
{
	Trace(debugCmds, "Read Version");
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdVersion;

	Req.m_wLength   = 16;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, bVersion, 16);
	}

BOOL CUsbHostModuleDriver::SendHeartbeat(void)
{
	Trace(debugCmds, "Heartbeat");
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdHeartbeat;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

// End of File
