
#include "Intern.hpp"

#include "UsbFuncCompDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbInterfaceDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Composite Function Driver
//

// Instantiator

IUsbDriver * Create_UsbCompDriver(void)
{
	CUsbFuncCompDriver *p = New CUsbFuncCompDriver();

	return (IUsbFuncCompDriver *) p;
	}

// Constructor

CUsbFuncCompDriver::CUsbFuncCompDriver(void)
{
	StdSetRef();

	m_uCount = 0; 

	m_pLowerDrv = NULL;

	m_sLangIds.Set(0x0409);
	}

// Destructor

CUsbFuncCompDriver::~CUsbFuncCompDriver(void)
{
	FreeInterfaces();

	AfxRelease(m_pLowerDrv);
	}

// IUnknown

HRESULT CUsbFuncCompDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbFuncCompDriver);

	StdQueryInterface(IUsbFuncCompDriver);

	StdQueryInterface(IUsbFuncDriver);

	StdQueryInterface(IUsbDriver);

	StdQueryInterface(IUsbFuncEvents);

	StdQueryInterface(IUsbEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbFuncCompDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbFuncCompDriver::Release(void)
{
	StdRelease();
	}

// IUsbDriver

BOOL CUsbFuncCompDriver::Bind(IUsbEvents *pDriver)
{
	if( pDriver ) {

		IUsbFuncEvents *pEvents = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncEvents), (void **) &pEvents);

		if( pEvents ) {

			if( BindInterface(pEvents) ) {

				return true;
				}

			pEvents->Release();
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::Bind(IUsbDriver *pDriver)
{
	if( pDriver ) {

		IUsbFuncEvents *pEvents = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncEvents), (void **) &pEvents);
			
		if( pEvents ) {

			if( BindInterface(pEvents) ) {

				return true;
				}

			pEvents->Release();
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::Init(void)
{
	if( m_pLowerDrv ) {

		MapEndpoints();

		return m_pLowerDrv->Init();
		}

	return false;
	}

BOOL CUsbFuncCompDriver::Start(void)
{
	return m_pLowerDrv ? m_pLowerDrv->Start() : false;
	}

BOOL CUsbFuncCompDriver::Stop(void)
{
	return m_pLowerDrv ? m_pLowerDrv->Stop() : false;
	}

// IUsbFunctionDriver

UINT CUsbFuncCompDriver::GetState(void) 
{
	return m_pLowerDrv ? m_pLowerDrv->GetState() : NOTHING;
	}

UINT CUsbFuncCompDriver::GetMaxPacket(UINT iEndpt) 
{
	return m_pLowerDrv ? m_pLowerDrv->GetMaxPacket(iEndpt) : 0;
	}

BOOL CUsbFuncCompDriver::SendCtrlStatus(UINT iEndpt)
{
	return m_pLowerDrv ? m_pLowerDrv->SendCtrlStatus(iEndpt) : false;
	}

BOOL CUsbFuncCompDriver::RecvCtrlStatus(UINT iEndpt)
{
	return m_pLowerDrv ? m_pLowerDrv->RecvCtrlStatus(iEndpt) : false;
	}

BOOL CUsbFuncCompDriver::SendCtrl(UINT iEndpt, PCBYTE pData, UINT uLen)
{
	return m_pLowerDrv ? m_pLowerDrv->SendCtrl(iEndpt, pData, uLen) : false;
	}

BOOL CUsbFuncCompDriver::RecvCtrl(UINT iEndpt, PBYTE pData, UINT uLen)
{
	return m_pLowerDrv ? m_pLowerDrv->RecvCtrl(iEndpt, pData, uLen) : false;
	}

UINT CUsbFuncCompDriver::SendBulk(UINT iEndpt, PCBYTE pData, UINT uLen, UINT uTimeout)
{
	return m_pLowerDrv ? m_pLowerDrv->SendBulk(iEndpt, pData, uLen, uTimeout) : NOTHING;
	}

UINT CUsbFuncCompDriver::RecvBulk(UINT iEndpt, PBYTE pData, UINT uLen, UINT uTimeout)
{
	return m_pLowerDrv ? m_pLowerDrv->RecvBulk(iEndpt, pData, uLen, uTimeout) : NOTHING;
	}

BOOL CUsbFuncCompDriver::SetStall(UINT iEndpt, BOOL fStall)
{
	return m_pLowerDrv ? m_pLowerDrv->SetStall(iEndpt, fStall) : false;
	}

BOOL CUsbFuncCompDriver::GetStall(UINT iEndpt)
{
	return m_pLowerDrv ? m_pLowerDrv->GetStall(iEndpt) : false;
	}

// IUsbFuncCompDriver

void CUsbFuncCompDriver::SetLanguages(PWORD pwLangId, UINT uCount)
{
	m_sLangIds.Set(pwLangId, uCount);
	}

void CUsbFuncCompDriver::SetVendor(WORD wId, PCTXT pName)
{
	m_wVendor = wId;

	m_sVendor.Set(pName);
	}

void CUsbFuncCompDriver::SetProduct(WORD wId, PCTXT pName)
{
	m_wProduct = wId;

	m_sProduct.Set(pName);
	}

void CUsbFuncCompDriver::SetSerialNumber(PCTXT pText)
{
	m_sSerial.Set(pText);
	}

UINT CUsbFuncCompDriver::FindInterface(IUsbEvents *pDriver)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		if( m_Interfaces[i].m_pDrv == (IUsbFuncEvents *) pDriver ) {

			return i;
			}
		}

	return NOTHING;
	}

UINT CUsbFuncCompDriver::GetFirsEndptAddr(UINT iInterface)
{
	if( iInterface < m_uCount ) {

		return m_Interfaces[iInterface].m_uAddr;
		}

	return NOTHING;
	}

// IUsbEvents

void CUsbFuncCompDriver::OnBind(IUsbDriver *pDriver)
{
	AfxRelease(m_pLowerDrv);

	m_pLowerDrv = NULL;

	pDriver->QueryInterface(AfxAeonIID(IUsbFuncDriver), (void **) &m_pLowerDrv);
	}

void CUsbFuncCompDriver::OnInit(void)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		m_Interfaces[i].m_pDrv->OnInit();
		}
	}

void CUsbFuncCompDriver::OnStart(void)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		m_Interfaces[i].m_pDrv->OnStart();
		}
	}

void CUsbFuncCompDriver::OnStop(void)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		m_Interfaces[i].m_pDrv->OnStop();
		}
	}

// IUsbFuncEvents

void CUsbFuncCompDriver::OnState(UINT uState)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		m_Interfaces[i].m_pDrv->OnState(uState);
		}
	}

BOOL CUsbFuncCompDriver::OnSetupClass(UsbDeviceReq &Req)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		if( m_Interfaces[i].m_pDrv->OnSetupClass(Req) ) {

			return true;
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::OnSetupVendor(UsbDeviceReq &Req)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		if( m_Interfaces[i].m_pDrv->OnSetupVendor(Req) ) {

			return true;
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::OnSetupOther(UsbDeviceReq &Req)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		if( m_Interfaces[i].m_pDrv->OnSetupOther(Req) ) {

			return true;
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::OnGetDevice(UsbDesc &Desc)
{
	if( Desc.m_bType == descDevice ) {

		UsbDeviceDesc &Dev = (UsbDeviceDesc &) Desc;

		Dev.m_wVendorID	    = m_wVendor;
		
		Dev.m_wProductID    = m_wProduct;
		
		Dev.m_wDeviceVer    = 0x0100;

		Dev.m_bVendorIdx    = m_sVendor.IsNull()  ? 0 : 1;
		
		Dev.m_bProductIdx   = m_sProduct.IsNull() ? 0 : 2;
		
		Dev.m_bSerialNumIdx = m_sSerial.IsNull()  ? 0 : 3;
		
		return true;
		}

	return false;
	}

BOOL CUsbFuncCompDriver::OnGetQualifier(UsbDesc &Desc)
{
	return false;
	}

BOOL CUsbFuncCompDriver::OnGetConfig(UsbDesc &Desc)
{
	if( Desc.m_bType == descConfig ) {

		UsbConfigDesc &Cfg = (UsbConfigDesc &) Desc;

		if( Cfg.m_bConfig == 1 ) {

			Cfg.m_bInterfaces = BYTE(m_uCount);

			return m_uCount > 0;
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::OnGetInterface(UsbDesc &Desc)
{
	if( Desc.m_bType == descInterface ) {

		UsbInterfaceDesc &Int = (UsbInterfaceDesc &) Desc;

		if( Int.m_bThis < m_uCount ) {

			return m_Interfaces[Int.m_bThis].m_pDrv->OnGetInterface(Desc);
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::OnGetEndpoint(UsbDesc &Desc)
{
	if( Desc.m_bType == descEndpoint ) {

		UsbEndpointDesc &Ep = (UsbEndpointDesc &) Desc;

		UINT i = FindInterface(Ep.m_bAddr);

		if( i < NOTHING ) {

			Ep.m_bAddr -= BYTE(m_Interfaces[i].m_uAddr - 1);

			if( m_Interfaces[i].m_pDrv->OnGetEndpoint(Desc) ) {

				Ep.m_bAddr += BYTE(m_Interfaces[i].m_uAddr - 1);

				return true;
				}
			}
		}

	return false;
	}

BOOL CUsbFuncCompDriver::OnGetString(UsbDesc *&pDesc, UINT i)
{
	switch( i ) {

		case 0: 
			if( !m_sVendor.IsNull() || !m_sProduct.IsNull() || !m_sSerial.IsNull() ) {

				pDesc = &m_sLangIds;

				return true;
				}

			return false;

		case 1: 
			pDesc = &m_sVendor;

			return true;

		case 2:
			pDesc = &m_sProduct;

			return true;

		case 3:
			pDesc = &m_sSerial;

			return true;
		}

	return false;
	}

// Interface Helpers

void CUsbFuncCompDriver::MapEndpoints(void)
{
	UINT uEndptAddr = 1;

	for( UINT i = 0; i < m_uCount; i ++ ) {

		UsbInterfaceDesc Desc;

		if( m_Interfaces[i].m_pDrv->OnGetInterface(Desc) ) {

			m_Interfaces[i].m_uEndpts = Desc.m_bEndpoints;

			m_Interfaces[i].m_uAddr   = uEndptAddr;

			uEndptAddr += Desc.m_bEndpoints;

			continue;
			}

		break;
		}
	}

BOOL CUsbFuncCompDriver::BindInterface(IUsbFuncEvents *pDriver)
{
	if( pDriver ) {
	
		if( m_uCount < elements(m_Interfaces) ) {

			m_Interfaces[m_uCount].m_pDrv    = pDriver;

			m_Interfaces[m_uCount].m_uEndpts = 0;

			m_Interfaces[m_uCount].m_uAddr   = 0;

			m_uCount ++;

			pDriver->OnBind(this);

			return true;
			}
		}

	return false;
	}

UINT CUsbFuncCompDriver::FindInterface(UINT uEndpt) const
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		if( uEndpt >= m_Interfaces[i].m_uAddr ) {

			if( uEndpt < (m_Interfaces[i].m_uAddr + m_Interfaces[i].m_uEndpts) ) {

				return i;
				}
			}
		}

	return NOTHING;
	}

void CUsbFuncCompDriver::FreeInterfaces(void)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		m_Interfaces[i].m_pDrv->Release();
		}

	m_uCount = 0;
	}

// End of File
