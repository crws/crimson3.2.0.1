
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbIdentifiers_HPP

#define	INCLUDE_UsbIdentifiers_HPP

//////////////////////////////////////////////////////////////////////////
//
// Vendor
//

enum
{
	vidFtdi			= 0x0403,
	vidSmc			= 0x0424,
	vidExar			= 0x04E2,
	vidRedLion		= 0x1037,
	vidTelit		= 0x1BC7,
	};

//////////////////////////////////////////////////////////////////////////
//
// Product
//

enum 
{
	pidTest			= 0x0001,

	pidSerial232485		= 0x1000,
	pidSerial232		= 0x1001,
	pidSerial485		= 0x1002,
	pidDevNet		= 0x1003,
	pidCan			= 0x1004,
	pidProfibus		= 0x1005,
	pidPid1			= 0x1006,
	pidPid2			= 0x1007,
	pidUin4			= 0x1008,
	pidOut4			= 0x1009,
	pidDio14		= 0x100A,
	pidTc8			= 0x100B,
	pidIni8			= 0x100C,
	pidInv8			= 0x100D,
	pidRtd6			= 0x100E,
	pidSg1			= 0x100F,
	pidJ1939		= 0x1010,
	pidSerial		= 0x1011,
	pidDnp3Dongle		= 0x1012,
	pidDnp3			= 0x1013,
	pidCanCdl		= 0x1014,
	pidTester		= 0x1015,
	pidRate			= 0x1016,
	pidDevNetBoot		= 0x1103,
	pidCanBoot		= 0x1104,
	pidProfibusBoot		= 0x1105,
	pidJ1939Boot		= 0x1110,
	pidSerialBoot		= 0x1111,
	pidDnp3Boot		= 0x1113,
	pidCanCdlBoot		= 0x1114,

	pidFtdi2232		= 0x6010,
	pidFtdi4232		= 0x6011,

	pidSmcLan9512		= 0xEC00,
	pidSmcHub2514		= 0x2514,
	pidSmcHub2604		= 0x2640,
	pidSmcUsd		= 0x4041,

	pidTelitHspa		= 0x0021,
	picTelitLte		= 0x0036,

	pidExarHub		= 0x0802,
	pidExarNic		= 0x1300,
	pidExarUartA		= 0x1400,
	pidExarUartB		= 0x1401,
	pidExarUartC		= 0x1402,
	pidExarUartD		= 0x1403,
	pidExarI2C		= 0x1100,
	pidExarEdge		= 0x1200,

	pidWiFiHub		= 0x1207,
	pidWiFiUart		= 0x1600,
	pidWiFiBluetooth	= 0x1601,
	pidWiFiEdge		= 0x1204,
	pidWiFiNic		= 0x1205,
	pidWifiI2C		= 0x1206,

	pidSerialHub		= 0x1217,
	pidSerialUart232A	= 0x1610,
	pidSerialUart232B	= 0x1611,
	pidSerialUart485A	= 0x1612,
	pidSerialUart485B	= 0x1613,
	pidSerialEdge		= 0x1214,
	pidSerialI2C		= 0x1216,
	};

//////////////////////////////////////////////////////////////////////////
//
// Red Lion Class Codes
//

enum
{
	subclassTest		= 0x0001,
	subclassBoot		= 0x0002,
	subclassDongle		= 0x0003,
	subclassComms  		= 0x0004,
	subclassRack		= 0x0005,
	subclassDriver		= 0x0006,
	};

//////////////////////////////////////////////////////////////////////////
//
// Red Lion Protocol Codes
//

enum
{
	protoSerial		= 0x0001,
	protoCan		= 0x0002,
	protoDevNet		= 0x0003,
	protoProfibus		= 0x0004,
	protoJ1939		= 0x0005,
	protoCdl		= 0x0006,
	protoDnp3		= 0x0007,
	protoTest		= 0x0008,
	};

// End of File

#endif
