
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostUniversal_HPP

#define	INCLUDE_UsbHostUniversal_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostHardwareDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Universal Host Controller Interface
//

class CUsbHostUniversal : public CUsbHostHardwareDriver
{
	public:
		// Constructor
		CUsbHostUniversal(void);
	};

// End of File

#endif
