
#include "Intern.hpp"

#include "UsbHostUniversal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Universal Host Controller Interface
//

// Constructor

CUsbHostUniversal::CUsbHostUniversal(void)
{
	m_pName	 = "Universal Host Driver";
	}

// End of File

