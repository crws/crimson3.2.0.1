
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostProfibusDriver_HPP

#define	INCLUDE_UsbHostProfibusDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostModuleDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Profibus Driver
//

class CUsbHostProfibusDriver : public CUsbHostModuleDriver, public IUsbHostProfibus
{
	public:
		// Constructor
		CUsbHostProfibusDriver(void);

		// Destructor
		~CUsbHostProfibusDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostModule
		BOOL METHOD Reset(void);
		BOOL METHOD ReadVersion(BYTE Version[16]); 
		BOOL METHOD SendHeartbeat(void);
		
		// IUsbHostProfibus
		BOOL METHOD IsOnline(void);
		BOOL METHOD SetRun(BOOL fRun);
		BOOL METHOD SetStation(BYTE bStation);
		UINT METHOD GetInputDataSize(void);
		UINT METHOD GetOutputDataSize(void);
		BOOL METHOD PutData(PCBYTE pData, UINT uCount);
		UINT METHOD GetData(PBYTE  pData, UINT uCount);
		BOOL METHOD PutDataAsync(PCBYTE pData, UINT uCount);
		UINT METHOD GetDataAsync(PBYTE pData, UINT uCount);
		UINT METHOD WaitAsyncPut(UINT uTimeout);
		UINT METHOD WaitAsyncGet(UINT uTimeout);
		BOOL METHOD KillAsyncPut(void);
		BOOL METHOD KillAsyncGet(void);
		
	protected:
		// Commands
		enum
		{
			cmdOnline  = 0x01,
			cmdSetRun  = 0x02,
			cmdSetDrop = 0x03,
			cmdGetPut  = 0x04,
			cmdGetGet  = 0x05,
			};
	};

// End of File

#endif
