
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiInquiry_HPP

#define	INCLUDE_ScsiInquiry_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Inquiry Data Object
//

class CScsiInquiry : public ScsiInquiry
{
	public:
		// Constructor
		CScsiInquiry(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Operations
		void Init(void);
		void Init(PTXT pVendor, PTXT pProduct);

		// Debug
		void Dump(void);
	};

// End of File

#endif
