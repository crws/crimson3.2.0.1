
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbFuncScsiDriver_HPP

#define	INCLUDE_UsbFuncScsiDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

#include "UsbFuncAppDriver.hpp"

#include "ScsiBulkCmd.hpp"

#include "ScsiBulkStatus.hpp"

#include "ScsiSense.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Filing System Access
//

#include "../../StdEnv/IFileSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Scsi Driver
//

class CUsbFuncScsiDriver : public CUsbFuncAppDriver, public IEventSink
{
	public:
		// Constructor
		CUsbFuncScsiDriver(UINT uPriority, UINT iDisk);

		// Destructor
		~CUsbFuncScsiDriver(void);

		// IUsbEvents
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);

		// IUsbFuncEvents
		BOOL METHOD OnGetInterface(UsbDesc &Desc);
		BOOL METHOD OnGetEndpoint(UsbDesc &Desc);
		BOOL METHOD OnSetupClass(UsbDeviceReq &Req);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Constants
		enum {
			constTimeoutShort	=  8000,
			constTimeoutLong	= 25000
			};

		// Connection State
		enum 
		{
			stateNew,
			stateIdle,
			stateActive,
			stateMounting,
			stateMounted,
			stateLocked,
			stateAutoLocked,
			stateUnlock,
			stateBusy,
			stateReady,
			stateNotReady,
			statePolling,
			};

		// File System Synchronisation
		enum
		{
			lockNone,
			lockRead,
			lockWrite,
			};

		// Debug Mask
		enum
		{
			debugSession = debugApp << 0,
			deubgPolling = debugApp << 1
			};
		
		// Data
		IDiskManager   * m_pDiskManager;
		IVolumeManager * m_pVolumeManager;
		IBlockDevice   * m_pDisk;
		IBlockCache    * m_pCache;
		UINT             m_iHost;
		UINT             m_iDisk;
		UINT	         m_iVolume;
		IThread        * m_pThread;
		ITimer         * m_pTimer;
		UINT             m_iEndptSend;
		UINT             m_iEndptRecv;
		CScsiBulkCmd     m_Cbw;
		CScsiBulkStatus  m_Csw;
		CScsiSense       m_SenseKey;
		UINT		 m_uTimeout;
		BYTE             m_bBuff[16384];
		UINT		 m_uState;
		UINT             m_uLockReq;
		UINT		 m_uLockAct;
		UINT		 m_uCmdLast;
		UINT		 m_uCmdThis;
		bool		 m_fMountPend;
		UINT		 m_uPollLast;
		UINT		 m_uPollThis;
		UINT             m_uPollPeriod;
		UINT             m_uWriteCount;
		UINT             m_uReadCount;
		UINT		 m_uCachedMin;
		UINT             m_uCachedMax;

		// Task Entry
		void TaskEntry(void);

		// Commands
		UINT ProcessCmd(void);
		UINT OnTestReady(void);
		UINT OnInquiry(void);
		UINT OnReqSense(void);
		UINT OnReadCapacity(void);
		UINT OnReadCapacity16(void);
		UINT OnSendDiag(void);
		UINT OnRead6(void);
		UINT OnRead10(void);
		UINT OnRead(DWORD dwLba, DWORD dwCount);
		UINT OnWrite6(void);
		UINT OnWrite10(void);
		UINT OnWrite(DWORD dwLBA, DWORD dwCount);
		UINT OnVerify10(void);
		UINT OnLockMedia(void);
		UINT OnStartStop(void);

		// Session Tracking 
		void SessionNew(void);
		bool SessionCheck(UINT uState);
		void SessionEnd(bool fSurprise);
		void SessionClear(void);
		bool SessionDetectPolling(UINT uCmd);
		bool SessionDetectMounted(void);
		bool SessionCanWrite(DWORD dwLBA);
		void SessionTimeout(void);
		UINT SessionFindTimeout(void);
		
		// Command Helpers
		UINT CheckAutoLock(DWORD dwMin, DWORD dwMax);
		UINT CheckReady(UINT uTimeout);
		UINT CheckBusy(UINT uTimeout);
		UINT CheckDrive(void);
		UINT CheckDirty(void);
		UINT CheckRefresh(void);
		void ClearMedia(void);

		// Cached Tracking
		void CachedClear(void);
		void CachedUpdate(DWORD dwBlock);
		void CachedUpdate(DWORD dwBlock, UINT uCount);
		bool CachedIsEmpty(void);

		// Synch Locks
		void SetLock(UINT uLock);
		void FreeLock(void);
		bool WaitLock(UINT uTimeout);
		bool ReadLockWait(UINT uTimeout);
		bool ReadLockFree(void);
		bool WriteLockWait(UINT uTimeout);
		bool WriteLockFree(void);

		// Bulk Transport
		bool RecvCmd(void);
		bool SendStatus(UINT uStatus);
		UINT RecvData(PBYTE pData, UINT uCount);
		bool SendData(PBYTE pData, UINT uCount);
		
		// Friends
		static int TaskUsbFuncScsi(IThread *pThread, void *pParam, UINT uParam); 
	};

// End of File

#endif
