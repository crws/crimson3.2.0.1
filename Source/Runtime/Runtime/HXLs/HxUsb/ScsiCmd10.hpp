
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiCmd10_HPP

#define	INCLUDE_ScsiCmd10_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Descriptor Block 10
//

class CScsiCmd10 : public ScsiCmd10
{
	public:
		// Constructor
		CScsiCmd10(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Operations
		void Init(void);
		void Init(BYTE bOpcode);
		void Init(BYTE bOpcode, BYTE bFlags);
		void Init(BYTE bOpcode, BYTE bFlags, WORD wLength);
		void Init(BYTE bOpcode, BYTE bFlags, WORD wLength, DWORD dwLBA);
	};

// End of File

#endif
