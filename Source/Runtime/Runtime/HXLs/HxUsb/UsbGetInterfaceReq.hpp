
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbGetInterfaceReq_HPP

#define	INCLUDE_UsbGetInterfaceReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Interface Standard Device Requeset
//

class CUsbGetInterfaceReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbGetInterfaceReq(WORD wIndex);

		// Operations
		void Init(WORD wIndex);
	};

// End of File

#endif
