
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostDevNetDriver_HPP

#define	INCLUDE_UsbHostDevNetDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostModuleDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host DeviceNet Driver
//

class CUsbHostDevNetDriver : public CUsbHostModuleDriver, public IUsbHostDevNet
{
	public:
		// Constructor
		CUsbHostDevNetDriver(void);

		// Destructor
		~CUsbHostDevNetDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostModule
		BOOL METHOD Reset(void);
		BOOL METHOD ReadVersion(BYTE Version[16]); 
		BOOL METHOD SendHeartbeat(void);
		
		// IUsbHostDevNet
		BOOL METHOD SetRun(BOOL fRun);
		BOOL METHOD SetBaudRate(UINT uBaud);
		BOOL METHOD SetDrop(UINT uDrop);
		BOOL METHOD EnableBitStrobe(WORD wRecvSize, WORD wSendSize);
		BOOL METHOD EnablePolled(WORD wRecvSize, WORD wSendSize);
		BOOL METHOD EnableData(WORD wRecvSize, WORD wSendSize);
		BOOL METHOD SendAsync(PCBYTE pData, UINT uCount);
		UINT METHOD RecvAsync(PBYTE  pData, UINT uCount);
		UINT METHOD WaitSend(UINT uTimeout);
		UINT METHOD WaitRecv(UINT uTimeout);
		BOOL METHOD KillSend(void);
		BOOL METHOD KillRecv(void);
		
	protected:
		// Commands
		enum
		{
			cmdSetRun	= 0x01,
			cmdSetBaud	= 0x02,
			cmdSetDrop	= 0x03,
			cmdSetBitStrobe	= 0x04,
			cmdSetPolled	= 0x05,
			cmdSetData	= 0x06,
			cmdPutData	= 0x07,
			cmdGetData	= 0x08,
			};

		// Data
		BYTE m_bData[512-1];
		UINT m_uSeq;
	};

// End of File

#endif
