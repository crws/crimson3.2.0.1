
#include "Intern.hpp"  

#include "UsbHostEcmDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbClassReq.hpp"

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Ethernet Device Class Driver
//

// Instantiator

IUsbHostFuncDriver * Create_EcmDriver(void)
{
	IUsbHostEcm *p = (IUsbHostEcm *) New CUsbHostEcmDriver;

	return p;
	}

// Constructor

CUsbHostEcmDriver::CUsbHostEcmDriver(void)
{
	m_pName = "Ethernet Device Class Driver";

	m_pLink = Create_ManualEvent();

	m_pStop = Create_ManualEvent();

	m_pNet  = NULL;

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostEcmDriver::~CUsbHostEcmDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");

	m_pLink->Release();

	m_pStop->Release();
	}

// IUnknown

HRESULT CUsbHostEcmDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostEcm);

	return CUsbHostCdcDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostEcmDriver::AddRef(void)
{
	return CUsbHostCdcDriver::AddRef();
	}

ULONG CUsbHostEcmDriver::Release(void)
{
	return CUsbHostCdcDriver::Release();
	}

// IHostFuncDriver

void CUsbHostEcmDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostCdcDriver::Bind(pDevice, iInterface);
	}

void CUsbHostEcmDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostCdcDriver::Bind(pSink);
	}

void CUsbHostEcmDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostCdcDriver::GetDevice(pDev);
	}

BOOL CUsbHostEcmDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostCdcDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostEcmDriver::GetVendor(void)
{
	return CUsbHostCdcDriver::GetVendor();
	}

UINT CUsbHostEcmDriver::GetProduct(void)
{
	return CUsbHostCdcDriver::GetProduct();
	}

UINT CUsbHostEcmDriver::GetClass(void)
{
	return CUsbHostCdcDriver::GetClass();
	}

UINT CUsbHostEcmDriver::GetSubClass(void)
{
	return classEthernet;
	}

UINT CUsbHostEcmDriver::GetInterface(void)
{
	return CUsbHostCdcDriver::GetInterface();
	}

UINT CUsbHostEcmDriver::GetProtocol(void)
{
	return protNone;
	}

BOOL CUsbHostEcmDriver::GetActive(void)
{
	return CUsbHostCdcDriver::GetActive();
	}

BOOL CUsbHostEcmDriver::Open(CUsbDescList const &List)
{
	if( CUsbHostCdcDriver::Open(List) ) {

		ParseNetwork();
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbHostEcmDriver::Close(void)
{
	if( CUsbHostCdcDriver::Close() ) {

		m_pStop->Set();

		return TRUE;
		}

	return FALSE;
	}

void CUsbHostEcmDriver::Poll(UINT uLapsed)
{
	CUsbHostCdcDriver::Poll(uLapsed);
	}

// IUsbHostCdc

BOOL CUsbHostEcmDriver::SendEncapCommand(PBYTE pData, UINT uLen)
{
	return CUsbHostCdcDriver::SendEncapCommand(pData, uLen);
	}

UINT CUsbHostEcmDriver::RecvEncapResponse(PBYTE pData, UINT uLen)
{
	return CUsbHostCdcDriver::RecvEncapResponse(pData, uLen);
	}

BOOL CUsbHostEcmDriver::SendData(PCTXT pData, bool fAsync)
{
	return CUsbHostCdcDriver::SendData(pData, fAsync);
	}

BOOL CUsbHostEcmDriver::SendData(PCBYTE pData, UINT uLen, bool fAsync)
{
	return CUsbHostCdcDriver::SendData(pData, uLen, fAsync);
	}

UINT CUsbHostEcmDriver::RecvData(PBYTE pData, UINT uLen, bool fAsync)
{
	return CUsbHostCdcDriver::RecvData(pData, uLen, fAsync);
	}

UINT CUsbHostEcmDriver::WaitSend(UINT uTimeout)
{
	return CUsbHostCdcDriver::WaitSend(uTimeout);
	}

UINT CUsbHostEcmDriver::WaitRecv(UINT uTimeout)
{
	return CUsbHostCdcDriver::WaitRecv(uTimeout);
	}

void CUsbHostEcmDriver::KillSend(void)
{
	CUsbHostCdcDriver::KillSend();
	}

void CUsbHostEcmDriver::KillRecv(void)
{
	CUsbHostCdcDriver::KillRecv();
	}

void CUsbHostEcmDriver::KillAsync(void)
{
	CUsbHostCdcDriver::KillAsync();
	}

// IUsbHostEcm

BOOL CUsbHostEcmDriver::IsLinkActive(void)
{
	return m_pLink->Wait(10);
	}

BOOL CUsbHostEcmDriver::WaitLinkActive(UINT uTime)
{
	return WaitMultiple(m_pLink, m_pStop, uTime) == 1;
	}

void CUsbHostEcmDriver::GetMac(MACADDR &Addr)
{
	Addr = m_Mac;
	}

UINT CUsbHostEcmDriver::GetMaxFilters(void)
{
	return m_pNet->m_wNumMulti & 0x7FFF;
	}

BOOL CUsbHostEcmDriver::GetFilterHashing(void)
{
	return (m_pNet->m_wNumMulti & Bit(15)) ? FALSE : TRUE;	
	}

UINT CUsbHostEcmDriver::GetCapabilities(void)
{
	return m_pNet->m_dwNetStats;
	}

BOOL CUsbHostEcmDriver::SetMulticastFilters(MACADDR const *pList, UINT uList)
{
	Trace(debugCmds, "SetMulticastFilters");

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSetEnetMultFilter;

	Req.m_wIndex    = m_iInt;

	Req.m_wValue    = uList;

	Req.m_wLength   = uList * sizeof(MACADDR);

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, PBYTE(pList), uList * sizeof(MACADDR)) == (uList * sizeof(MACADDR));
	}

BOOL CUsbHostEcmDriver::SetPacketFilters(WORD wFilter)
{
	Trace(debugCmds, "SetPacketFilters");

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSetEnetPacketFilter;

	Req.m_wIndex    = m_iInt;

	Req.m_wValue    = wFilter;
		
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostEcmDriver::GetStatistics(UINT iSelect, DWORD &Data)
{
	Trace(debugCmds, "GetStatistics(Select=%d)", iSelect);

	if( m_pNet->m_dwNetStats & Bit(iSelect-1) ) {

		CUsbClassReq Req;

		Req.m_Direction = dirDevToHost;

		Req.m_Recipient = recInterface;
			
		Req.m_bRequest  = reqGetEnetStats;

		Req.m_wIndex    = m_iInt;

		Req.m_wLength   = sizeof(DWORD);

		Req.m_wValue    = iSelect;
		
		Req.HostToUsb();

		if( m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(DWORD)) == sizeof(DWORD) ) {

			Data = IntelToHost(Data);

			Trace(debugCmds, "Data=0x%8.8X", Data);
			
			return TRUE;
			}
		}

	Data = 0;

	return FALSE;
	}

// Notifications

void CUsbHostEcmDriver::OnEvent(UsbDeviceReq const &Req)
{
	switch( Req.m_bRequest ) {

		case reqConnection:

			OnConnect(Req);

			break;

		case reqSpeedChange:

			OnSpeed(Req);

			break;
		}
	}

void CUsbHostEcmDriver::OnConnect(UsbDeviceReq const &Req)
{
	if( Req.m_wValue == 1 ) {

		Trace(debugInfo, "Link Up");

		m_pLink->Set();
		}
	else {
		Trace(debugInfo, "Link Down");

		m_pLink->Clear();

		KillSend();

		KillRecv();
		}
	}

void CUsbHostEcmDriver::OnSpeed(UsbDeviceReq const &Req)
{
	if( Req.m_wLength == 8 ) {

		PDWORD pData = PDWORD(DWORD(&Req) + sizeof(Req));

		DWORD dwUpstream   = IntelToHost(pData[0]);

		DWORD dwDownstream = IntelToHost(pData[1]);

		Trace(debugInfo, "Connect Speed Up=%d, Down=%d", dwUpstream, dwDownstream);
		}
	}

// Implementation 

bool CUsbHostEcmDriver::ParseNetwork(void)
{
	CUsbDescList const &List = m_pDev->GetCfgDesc();	
	
	UINT iIndex;

	List.FindInterface(iIndex, m_iInt);

	for(;;) {

		CdcFuncDesc *p = (CdcFuncDesc *) List.Enum(descCsInterface, iIndex);

		if( p ) {

			if( p->m_bSubType == descEthernet ) {

				m_pNet = (CCdcNetwork *) p;

				m_pNet->CdcToHost();

				char sText[16];

				m_pDev->GetString(m_pNet->m_iMacAddr, sText, sizeof(sText));

				m_Mac = CMacAddr(sText);

				Trace(debugInfo, "iMacAddress = %s", sText);

				Trace(debugInfo, "Statistics  = 0x%8.8X", m_pNet->m_dwNetStats);

				Trace(debugInfo, "Max Segment = %d", m_pNet->m_wMaxSegment);

				Trace(debugInfo, "Filters     = 0x%8.8X", m_pNet->m_wNumMulti);
								
				return true;
				}

			continue;
			}
		
		return false;
		}
	}

// End of File
