
#include "Intern.hpp"

#include "UsbFuncScsiDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Classes
//

#include "ScsiInquiry.hpp"

#include "ScsiCapacity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Command Classes
//

#include "ScsiCmdInquiry.hpp"

#include "ScsiCmdCapacity.hpp"

#include "ScsiCmdDiag.hpp"

#include "ScsiCmd6.hpp"

#include "ScsiCmd10.hpp"

#include "ScsiCmdLock.hpp"

#include "ScsiCmdStartStop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Scsi Driver
//

// Instantiator 

IUsbFuncEvents * Create_UsbFuncScsiDriver(UINT uPriority, UINT iDisk)
{
	CUsbFuncScsiDriver *p = New CUsbFuncScsiDriver(uPriority, iDisk);

	return p;
	}

// Constructor

CUsbFuncScsiDriver::CUsbFuncScsiDriver(UINT uPriority, UINT iDisk)
{
	AfxGetObject("diskman", 0, IDiskManager, m_pDiskManager);

	AfxGetObject("volman",  0, IVolumeManager, m_pVolumeManager);

	m_pTimer   = CreateTimer();

	m_pName    = "Mass Storage Driver (Slave)";

	m_Debug    = debugErr;

	m_pDisk    = m_pDiskManager->GetDisk(iDisk);

	m_pCache   = m_pDiskManager->GetDiskCache(iDisk);
	
	m_iVolume  = m_pDiskManager->GetVolumeIndex(m_iDisk, 0);

	m_iDisk    = iDisk;

	m_iHost	   = NOTHING;

	m_uState   = stateIdle;

	m_uLockReq = lockNone;

	m_uLockAct = lockNone;
	
	m_pThread  = CreateThread(TaskUsbFuncScsi, uPriority, this, 0);

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbFuncScsiDriver::~CUsbFuncScsiDriver(void)
{
	m_pThread->Destroy();

	m_pTimer->Release();

	m_pDiskManager->Release();

	m_pVolumeManager->Release();
	}

// IUsbEvents

void CUsbFuncScsiDriver::OnInit(void)
{
	m_iEndptRecv = m_pLowerDrv->GetFirsEndptAddr(m_iInterface) + 0;

	m_iEndptSend = m_pLowerDrv->GetFirsEndptAddr(m_iInterface) + 1;
	}

void CUsbFuncScsiDriver::OnStart(void)
{
	m_uTimeout = 0;

	m_pTimer->SetPeriod(1000);

	m_pTimer->SetHook(this, 0);
	
	m_pTimer->Enable(true);
	}

void CUsbFuncScsiDriver::OnStop(void)
{
	m_pTimer->Enable(false);
	}

// IUsbFuncEvents

BOOL CUsbFuncScsiDriver::OnGetInterface(UsbDesc &Desc)
{
	UsbInterfaceDesc &Int = (UsbInterfaceDesc &) Desc;

	Int.m_bEndpoints = 2;
			
	Int.m_bClass	 = devStorage;
			
	Int.m_bSubClass	 = 0x06;
			
	Int.m_bProtocol	 = 0x50;
			
	return true;
	}

BOOL CUsbFuncScsiDriver::OnGetEndpoint(UsbDesc &Desc)
{
	UsbEndpointDesc &Ep = (UsbEndpointDesc &) Desc;

	if( Ep.m_bAddr == 1 ) {

		Ep.m_bDirIn    = false;

		Ep.m_bTransfer = eptBulk;

		return true;
		}

	if( Ep.m_bAddr == 2 ) {

		Ep.m_bDirIn    = true;

		Ep.m_bTransfer = eptBulk;

		return true;
		}

	return false;
	}

BOOL CUsbFuncScsiDriver::OnSetupClass(UsbDeviceReq &Req)
{
	if( Req.m_Recipient == recInterface ) {

		if( Req.m_bRequest == 0xFE ) {

			Trace(debugCmds, "Class Request - Get Max Lun");

			BYTE b = 0;
			
			SendCtrl(0, &b, 1);

			RecvCtrlStatus(0);
			
			return true;
			}
		}

	return false;
	}

// IEventSink

void CUsbFuncScsiDriver::OnEvent(UINT uLine, UINT uParam)
{
	if( m_uTimeout ) {

		if( m_uTimeout <= 1000 ) {

			m_uTimeout = 0;

			SessionTimeout();
			}
		else {
			m_uTimeout -= 1000;
			}
		}
	}

// Test

void CUsbFuncScsiDriver::TaskEntry(void)
{
	for(;;) {

		WaitRunning(FOREVER);

		SessionNew();

		m_SenseKey.Clear();

		while( m_fOpen ) {

			if( RecvCmd() ) {

				UINT uStatus = ProcessCmd();

				if( uStatus == bulkPassed ) {

					SendStatus(uStatus);

					if( m_SenseKey.IsValid() ) {

						m_SenseKey.Debug();

						m_SenseKey.Clear();
						}
					}
				else {
					if( m_Cbw.IsDataIn() && m_Cbw.m_dwTransfer ) {

						SetStall(m_iEndptSend, true);
						}

					SendStatus(uStatus);
					}
				}  
			}
		
		SessionEnd(true);

		Trace(debugInfo, "Driver Stopped");
		}
	}

// Commands

UINT CUsbFuncScsiDriver::ProcessCmd(void)
{
	ScsiCmd &Cmd = (ScsiCmd &) m_Cbw.m_bCmd[0];

	if( Cmd.m_bOpcode != cmdReqSense ) {

		if( SessionDetectPolling(Cmd.m_bOpcode) ) {

			SessionCheck(statePolling);
			}
		else {
			SessionCheck(stateActive);
			}
		}

	switch( Cmd.m_bOpcode ) {
		
		case cmdTestRdy: 
		
			return OnTestReady();

		case cmdInquiry:
			
			return OnInquiry();

		case cmdReqSense:
			
			return OnReqSense();
		
		case cmdReadCapacity:
			
			return OnReadCapacity();

		case cmdSendDiag:
			
			return OnSendDiag();

		case cmdRead6:
			
			return OnRead6();
		
		case cmdRead10:
			
			return OnRead10();

		case cmdWrite6:
			
			return OnWrite6();

		case cmdWrite10:
			
			return OnWrite10();

		case cmdVerify10:
			return OnVerify10();

		case cmdLockMedia:
			
			return OnLockMedia();

		case cmdStartStop:
			
			return OnStartStop();

		case cmdReserve6:
		case cmdReserve10:
		case cmdRelease6:
		case cmdRelease10:
			
			return bulkPassed;
		}

	Trace(debugCmds, "Unsupported Command %2.2X", Cmd.m_bOpcode); 
	
	m_SenseKey.Clear();

	m_SenseKey.Set(senseIllegalReq, addInvalidCdb);
		
	return bulkFailed;
	}

UINT CUsbFuncScsiDriver::OnTestReady(void)
{
	Trace( debugData, "OnTestReady Cache %8.8X - %8.8X, Write Count %d (local) %d (this)", 
	       m_uCachedMin, 
	       m_uCachedMax,
	       m_pDisk->GetWriteCount(),
	       m_uWriteCount
	       );

	if( CheckDrive() != bulkPassed ) {

		return bulkFailed;
		}

	if( CheckBusy(constTimeoutShort) != bulkPassed ) {

		CheckDirty();

		return bulkFailed;
		}

	return CheckDirty();
	}

UINT CUsbFuncScsiDriver::OnInquiry(void)
{	
	Trace(debugInfo, "OnInquiry");

	if( m_Cbw.m_bCmdLength < sizeof(ScsiCmdInquiry) ) {

		return bulkPhaseError;
		}
	
	if( m_Cbw.m_dwTransfer <  sizeof(ScsiInquiry)) {

		return bulkPhaseError;
		}

	CScsiCmdInquiry &Cmd = (CScsiCmdInquiry &) m_Cbw.m_bCmd[0];

	Cmd.ScsiToHost();

	if( Cmd.m_bCmdDt ) {

		Trace(debugWarn, "Inquiry Command Data Noty Supported");

		m_SenseKey.Set(senseIllegalReq, addInvalidCdb);
		
		return bulkFailed;
		}

	if( Cmd.m_bEvpd ) {

		Trace(debugWarn, "Inquiry Vital Product Data Not Supported");

		m_SenseKey.Set(senseIllegalReq, addInvalidCdb);

		return bulkFailed;
		}

	CScsiInquiry &Inq = (CScsiInquiry &) m_bBuff;

	Inq.Init("HMI", "Flash Card");

	Inq.HostToScsi();

	m_Cbw.m_dwTransfer -= sizeof(CScsiInquiry);
		
	return SendData(m_bBuff, sizeof(CScsiInquiry)) ? bulkPassed : bulkPhaseError;
	}

UINT CUsbFuncScsiDriver::OnReqSense(void)
{	
	Trace(debugInfo, "OnReqSense");

	if( m_Cbw.m_dwTransfer < 0x0E ) {
		
		return bulkPhaseError;
		}

	UINT uCount = sizeof(ScsiSense);
	
	MakeMin(uCount, m_Cbw.m_dwTransfer);

	memcpy(m_bBuff, &m_SenseKey, uCount);
	
	CScsiSense &Sense = (CScsiSense &) m_bBuff[0];
	
	Sense.HostToScsi();
		
	m_Cbw.m_dwTransfer -= uCount;

	return SendData(m_bBuff, uCount) ? bulkPassed : bulkPhaseError;
	}

UINT CUsbFuncScsiDriver::OnReadCapacity(void)
{	
	Trace(debugInfo, "OnReadCapacity");

	if( m_Cbw.m_bCmdLength < sizeof(ScsiCmdCapacity) ) {

		return bulkPhaseError;
		}
	
	if(  m_Cbw.m_dwTransfer < sizeof(ScsiCapacity) ) {

		return bulkPhaseError;
		}

	CScsiCmdCapacity &Cmd = (CScsiCmdCapacity &) m_Cbw.m_bCmd[0];

	Cmd.ScsiToHost();

	if( Cmd.m_bRelAddr ) {
	
		Trace(debugWarn, "Relative Address Not Supported");

		m_SenseKey.Set(senseIllegalReq, addInvalidCdb);
		
		return bulkFailed;
		}

	if( CheckReady(constTimeoutShort) == bulkPassed ) {

		CScsiCapacity &Capacity = (CScsiCapacity &) m_bBuff[0];

		Capacity.m_dwBlockAddr = m_pDisk->GetSectorCount() - 1;

		Capacity.m_dwBlockLen  = m_pDisk->GetSectorSize();
		
		Trace(debugInfo, "Addr=0x%8.8X, Len=%8.8X", Capacity.m_dwBlockAddr, Capacity.m_dwBlockLen);
		
		Capacity.HostToScsi();

		m_Cbw.m_dwTransfer -= sizeof(CScsiCapacity);
			
		return SendData(m_bBuff, sizeof(CScsiCapacity)) ? bulkPassed : bulkPhaseError;
		}

	return bulkFailed;
	}

UINT CUsbFuncScsiDriver::OnReadCapacity16(void)
{	
	Trace(debugInfo, "OnReadCapacity16");

	return bulkFailed;
	}

UINT CUsbFuncScsiDriver::OnSendDiag(void)
{
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmdDiag) ) {

		CScsiCmdDiag &Cmd = (CScsiCmdDiag &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		if( !Cmd.m_bSelfTest ) {

			Trace(debugWarn, "Invalid Parameter");

			m_SenseKey.Set(senseIllegalReq, addInvalidCdb);
			
			return bulkFailed;
			}

		#if defined(TODO)
		
		// TODO

		if( !m_pDrive->ExecDiag() ) {

			m_SenseKey.Set(senseHWError, addInvalidCdb);
			
			return bulkFailed;
			}

		#endif

		return bulkPassed;
		}
	
	return bulkPhaseError;
	}

UINT CUsbFuncScsiDriver::OnRead6(void)
{	
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmd6) ) {

		CScsiCmd6 &Cmd = (CScsiCmd6 &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		return OnRead(Cmd.GetLBA(), Cmd.m_bLength);
		}

	return bulkPhaseError;
	}

UINT CUsbFuncScsiDriver::OnRead10(void)
{	
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmd10) ) {

		CScsiCmd10 &Cmd = (CScsiCmd10 &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		ScsiReadWriteFlags &Flags = (ScsiReadWriteFlags &) Cmd.m_bFlags;

		if( Flags.m_bRelAddr ) {
		
			Trace(debugWarn, "Relative Addresses Not Supported");

			m_SenseKey.Set(senseIllegalReq, addInvalidCdb);
		
			return bulkFailed;
			}

		return OnRead(Cmd.m_dwLba, Cmd.m_wLength);
		}

	return bulkPhaseError;
	}

UINT CUsbFuncScsiDriver::OnWrite6(void)
{	
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmd6) ) {

		CScsiCmd6 &Cmd = (CScsiCmd6 &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		return OnWrite(Cmd.GetLBA(), Cmd.m_bLength);
		}

	return bulkPhaseError;
	}
	
UINT CUsbFuncScsiDriver::OnWrite10(void)
{	
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmd10) ) {

		CScsiCmd10 &Cmd = (CScsiCmd10 &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		ScsiReadWriteFlags &Flags = (ScsiReadWriteFlags &) Cmd.m_bFlags;

		if( Flags.m_bRelAddr ) {
		
			Trace(debugWarn, "Relative Addresses Not Supported");

			m_SenseKey.Set(senseIllegalReq, addInvalidCdb);
		
			return bulkFailed;
			}

		return OnWrite(Cmd.m_dwLba, Cmd.m_wLength);
		}

	return bulkPhaseError;
	}
	
UINT CUsbFuncScsiDriver::OnRead(DWORD dwLBA, DWORD dwCount)
{	
	// LATER -- Overlapped I/O

	DWORD dwBlock    = dwLBA;
	
	DWORD dwBlockEnd = dwBlock + dwCount;

	DWORD dwBlockLen = m_pDisk->GetSectorSize();

	Trace( debugData, "Block %8.8X - %8.8X   :  R   Range %8.8X - %8.8X", 
	       dwBlock, 
	       dwBlockEnd, 
	       m_uCachedMin, 
	       m_uCachedMax
	       );

	if( OnTestReady() == bulkPassed ) {
		
		while( dwBlock < dwBlockEnd ) {

			if( !m_pDisk->ReadSector(dwBlock, m_bBuff) ) {
			
				if( CheckDrive() != bulkPassed ) {

					Trace(debugWarn, "Read Failed Media Not Ready");

					return bulkFailed;
					}

				Trace(debugWarn, "Read Failed H/W Error");
				
				m_SenseKey.Set(senseHWError);
					
				return bulkFailed;
				}

			m_uReadCount ++;

			if( !SendData(m_bBuff, dwBlockLen) ) {

				Trace(debugWarn, "Read Faild Transport Error");
					
				m_SenseKey.Set(senseIllegalReq, addLBARange);
				
				return bulkFailed;
				}

			CachedUpdate(dwBlock);

			m_Cbw.m_dwTransfer -= dwBlockLen;

			dwBlock            += 1;
			}

		SessionCheck(stateMounted);

		return bulkPassed;
		}

	return bulkFailed;
	}

UINT CUsbFuncScsiDriver::OnWrite(DWORD dwLBA, DWORD dwBlocks)
{	
	// LATER -- Overlapped I/O

	DWORD dwBlock    = dwLBA;
	
	DWORD dwBlockEnd = dwBlock + dwBlocks;

	DWORD dwBlockLen = m_pDisk->GetSectorSize();

	Trace( debugData, "Block %8.8X - %8.8X   :  W   Range %8.8X - %8.8X", 
	       dwBlock, 
	       dwBlockEnd, 
	       m_uCachedMin, 
	       m_uCachedMax
	       );
	
	UINT  uResult = CheckAutoLock(dwBlock, dwBlockEnd);
		
	DWORD dwBytes = 0;

	PBYTE pBlock  = NULL;

	while( dwBlock < dwBlockEnd ) {

		UINT uCount = 0; 
		
		while( uCount < dwBlockLen ) {

			PBYTE pData = &m_bBuff[uCount];

			UINT  uSize = dwBlockLen - uCount;

			if( (uSize = RecvData(pData, uSize)) == NOTHING ) {
			
				m_SenseKey.Set(senseCmdAborted, addSysBufFull, qualSysBufFull);
				
				return bulkFailed;
				}

			uCount += uSize;
			}

		if( uResult == bulkPassed ) {

			if( !m_pDisk->WriteSector(dwBlock, m_bBuff) ) {
			
				if( CheckDrive() != bulkPassed ) {

					Trace(debugWarn, "Write Failed Media Not Ready");

					m_SenseKey.Set(senseNotReady, addNoMedium);
					}
				else {
					Trace(debugWarn, "Write Failed H/W");

					m_SenseKey.Set(senseHWError);
					}

				uResult = bulkFailed;
				}
			else {
				m_uWriteCount ++;
				}
			}

		m_Cbw.m_dwTransfer -= dwBlockLen;

		dwBlock            += 1;
		}

	return uResult;
	}

UINT CUsbFuncScsiDriver::OnVerify10(void)
{
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmd10) ) {

		CScsiCmd10 &Cmd = (CScsiCmd10 &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		ScsiVerifyFlags &Flags = (ScsiVerifyFlags &) Cmd.m_bFlags;
		
		if( Flags.m_bCheck || Flags.m_bBlank ) {

			m_SenseKey.Set(senseIllegalReq, addInvalidCdb);

			return bulkFailed;
			}

		#if defined(TODO)

		if( !m_pDrive->RawVerify(Cmd.m_dwLba, Cmd.m_wLength) ) {

			m_SenseKey.Set(senseMisCompare);

			CheckDrive();

			return bulkFailed;
			}

		#endif
		
		CachedUpdate(Cmd.m_dwLba, Cmd.m_wLength);

		return bulkPassed;
		}

	return bulkPhaseError;
	}
	
UINT CUsbFuncScsiDriver::OnLockMedia(void)
{
	Trace(debugInfo, "OnLockMedia");
	
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmdLock) ) {

		CScsiCmdLock &Cmd = (CScsiCmdLock &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		Trace(debugInfo, "Lock Media : Prevent %x", Cmd.m_bPrevent);

		if( CheckDrive() == bulkPassed ) {

			if( Cmd.m_bPrevent ) {

				SessionCheck(stateLocked);

				return CheckBusy(constTimeoutShort);
				}
			else {
				if( CheckBusy(constTimeoutShort) == bulkPassed ) {

					SessionCheck(stateUnlock);

					return bulkPassed;
					}

				return bulkFailed;
				}
			}
		
		return bulkFailed;
		}

	return bulkPhaseError;
	}

UINT CUsbFuncScsiDriver::OnStartStop(void)
{
	Trace(debugInfo, "OnStartStop");
	
	if( m_Cbw.m_bCmdLength >= sizeof(ScsiCmdStartStop) ) {

		CScsiCmdStartStop &Cmd = (CScsiCmdStartStop &) m_Cbw.m_bCmd[0];

		Cmd.ScsiToHost();

		if( CheckReady(constTimeoutLong) == bulkPassed ) {

			if( Cmd.m_bEject ) {

				if( !Cmd.m_bStart ) {

					Trace(debugInfo, "Eject Medium");

					m_pDiskManager->EjectDisk(m_iDisk);
					}
				}
			
			return bulkPassed;
			}

		return bulkFailed;
		}

	return bulkPhaseError;
	}

// Connection Tracking 

void CUsbFuncScsiDriver::SessionNew(void)
{
	m_iHost = m_pDiskManager->RegisterHost(m_iDisk);

	SessionClear();

	SessionCheck(stateNew);
	}

bool CUsbFuncScsiDriver::SessionCheck(UINT uState)
{
	switch( uState ) {

		case stateNew:

			if( m_uState == stateIdle ) {
			
				Trace(debugSession, "*** Session New");

				m_uState     = stateNew;

				m_fMountPend = false;

				return true;
				}

			return false;

		case stateIdle:

			if( m_uState == stateNew ) {

				return false;
				}

			if( m_uState != stateIdle ) {

				Trace(debugSession, "*** Session Idle");

				SetLock(lockNone);

				m_uState     = stateIdle;

				m_fMountPend = false;

				return true;
				}
		
			return false;

		case statePolling:

			if( m_uState == stateLocked || m_uState == stateBusy ) {

				return false;
				}

			if( m_uState != stateIdle ) {
			
				Trace(debugSession, "*** Polling -> Idle");

				SetLock(lockNone);

				m_uState = stateIdle;

				return true;
				}

			return false;

		case stateActive:

			if( m_uState == stateNew || m_uState == stateIdle ) {
				
				SetLock(lockRead);

				if( m_uState == stateNew || m_fMountPend ) {
				
					Trace(debugSession, "*** Session Mounting");

					ClearMedia();
				
					m_uState = stateMounting;
					}
				else {
					Trace(debugSession, "*** Session Active");

					m_uState = stateActive;
					}

				return true;
				}
		
			return false;
		
		case stateMounting:

			if( !m_fMountPend ) {

				if( m_uState == stateIdle ) {

					SessionCheck(stateActive);
					}

				if( m_uState == stateActive || m_uState == stateBusy ) {

					Trace(debugSession, "*** Session Mounting");

					m_uState     = stateMounting;

					m_fMountPend = true; 
					
					return true;
					}
				}
		
			return false;

		case stateMounted:

			if( m_uState == stateMounting ) {

				if( SessionDetectMounted() ) {

					Trace(debugSession, "*** Session Mounted -> Active");

					m_uState     = stateActive;

					m_fMountPend = false;

					m_pDisk->ClearCounters();

					return true;
					}
				}
		
			return false;

		case stateLocked:

			if( m_uState == stateAutoLocked ) {

				Trace(debugSession, "*** Session Locked");

				m_uState = stateLocked;

				return true;
				}

			if( m_uState == stateActive ) {

				SetLock(lockWrite);

				Trace(debugSession, "*** Session Locked");

				m_uState = stateLocked;

				return true;
				}

			return false;

		case stateAutoLocked:

			if( m_uState == stateActive || m_uState == stateMounting ) {

				Trace(debugSession, "*** Session AutoLocked");

				SetLock(lockWrite);
				
				m_uState = stateAutoLocked;

				return true;
				}

			return false;

		case stateUnlock:

			if( m_uState == stateLocked || m_uState == stateAutoLocked ) {

				Trace(debugSession, "*** Session Unlock -> Active");

				SetLock(lockRead);

				m_uState = stateActive;

				return true;
				}
		
			return false;

		case stateBusy:

			if( m_uState == stateActive ) {

				Trace(debugSession, "*** Session Busy");

				m_uState = stateBusy;

				return true;
				}
		
			return false;

		case stateReady:

			if( m_uState == stateBusy ) {

				Trace(debugSession, "*** Session Ready -> Active");

				m_uState = stateActive;

				return true;
				}

			return false;

		case stateNotReady:

			m_iVolume = NOTHING;

			if( m_uState == stateMounting || m_uState == stateLocked || m_uState == stateAutoLocked ) {

				Trace(debugSession, "**** Session NOT Ready -> Active");

				m_uState     = stateActive;

				m_fMountPend = false;

				SetLock(lockRead);
				
				return true;
				}

			return false;
		}
	
	return false;
	}

void CUsbFuncScsiDriver::SessionEnd(bool fSurprise)
{
	Trace(debugSession, "Session End");

	SessionCheck(stateIdle);

	m_pDiskManager->UnregisterHost(m_iDisk, m_iHost);

	m_iHost = NOTHING;
	}

void CUsbFuncScsiDriver::SessionClear(void)
{
	m_uCmdLast = NOTHING;

	m_uCmdThis = NOTHING;
	}

bool CUsbFuncScsiDriver::SessionDetectPolling(UINT uCmd)
{
	m_uCmdLast = m_uCmdThis;

	m_uCmdThis = uCmd;

	if( uCmd == cmdTestRdy ) {

		m_uPollLast = m_uPollThis;

		m_uPollThis = GetTickCount();

		if( m_uCmdLast == cmdTestRdy ) {

			UINT uPeriod;
			
			if( m_uPollThis >= m_uPollLast ) {
				
				uPeriod = m_uPollThis - m_uPollLast;
				}
			else {
				uPeriod = UINT(-1) - m_uPollLast + m_uPollThis;
				}

			uPeriod = ToTime(uPeriod);

			Trace(deubgPolling, "Poll Period Raw : %d", uPeriod);

			if( uPeriod < 1100 ) {

				if( uPeriod > m_uPollPeriod || uPeriod > 500  ) {

					m_uPollPeriod *= 2;
					
					m_uPollPeriod += uPeriod;
				
					m_uPollPeriod /= 3;
					}
				else
					m_uPollPeriod  = uPeriod;

				Trace(deubgPolling, "Poll Period Damped : %d", m_uPollPeriod);
	
				if( m_uPollPeriod > 500 ) {

					if( uPeriod > m_uPollPeriod ) {

						m_uPollPeriod = 1000;
						}

					return true;
					}

				return false;
				}
			}
		}

	m_uPollPeriod = 1000;

	return false;
	}

bool CUsbFuncScsiDriver::SessionDetectMounted(void)
{
	if( m_pDiskManager->GetDiskStatus(m_iDisk) != diskEmpty ) {

		if( !CachedIsEmpty() ) {

			// LATER -- Assumptions about MBR
			
			if( m_uCachedMax >= m_pDisk->GetSectorsPerHead() ) {

				return true;
				}
			}
		}

	return false;
	}

bool CUsbFuncScsiDriver::SessionCanWrite(DWORD dwLBA)
{
	if( m_uState == stateLocked || m_uState == stateAutoLocked ) {

		return true;
		}

	if( !m_pVolumeManager->IsVolumeMounted(m_iVolume) ) {

		return true;
		}

	if( m_pDiskManager->GetDiskStatus(m_iDisk) == diskEmpty ) {

		return true;
		}

	// LATER -- Assumptions about MBR

	if( dwLBA < m_pDisk->GetSectorsPerHead() ) {

		return true;
		}

	return false;
	}

void CUsbFuncScsiDriver::SessionTimeout(void)
{
	SessionCheck(stateIdle);
	}

UINT CUsbFuncScsiDriver::SessionFindTimeout(void)
{
	switch( m_uState ) {

		case stateActive:
			return 2000;

		case stateAutoLocked:
			return 10000;

		case stateBusy:
			return 30000;

		case stateMounting:
			return 60000;

		case stateNew:
		case stateIdle:
		case stateLocked:
			return FOREVER;
		}

	return FOREVER;
	}

// Command Helpers

UINT CUsbFuncScsiDriver::CheckAutoLock(DWORD dwMin, DWORD dwMax)
{
	if( OnTestReady() == bulkPassed ) {

		if( !SessionCanWrite(dwMin) || !SessionCanWrite(dwMax) )  {

			Trace(debugSession, "Auto Lock");

			if( SessionCheck(stateAutoLocked) ) {

				return CheckReady(5000);
				}

			Trace(debugWarn, "Write Failed No Guard");

			m_SenseKey.Set(senseDataProtect, addWriteProtect, qualWriteLockSW);

			return bulkFailed;
			}
	
		return bulkPassed;
		}
	
	return bulkFailed;
	}

UINT CUsbFuncScsiDriver::CheckReady(UINT uTimeout)
{
	if( CheckDrive() != bulkPassed ) {

		return bulkFailed;
		}

	if( CheckBusy(uTimeout) != bulkPassed ) {

		return bulkFailed;
		}

	return bulkPassed;
	}

UINT CUsbFuncScsiDriver::CheckBusy(UINT uTimeout)
{
	if( !WaitLock(uTimeout) ) {
	
		Trace(debugInfo, "File System Busy");

		SessionCheck(stateBusy);
			
		m_SenseKey.Set(senseNotReady, addNotReady, qualBusy);

		return bulkFailed;
		}

	SessionCheck(stateReady);
	
	return bulkPassed;
	}

UINT CUsbFuncScsiDriver::CheckDrive(void)
{
	if( m_pDiskManager->GetDiskStatus(m_iDisk) == diskEmpty ) {

		Trace(debugInfo, "File System Not Ready");

		SessionCheck(stateNotReady);

		m_SenseKey.Set(senseNotReady, addNoMedium);
		
		return bulkFailed;
		}
	
	#if defined(TODO)
	
	// TODO

	if( m_pDrive->IsEjecting() ) {

		Trace(debugInfo, "File System Not Ready");

		SessionCheck(stateNotReady);

		m_SenseKey.Set(senseNotReady, addNoMedium);
		
		return bulkFailed;
		}

	#endif

	return bulkPassed;
	}

UINT CUsbFuncScsiDriver::CheckDirty(void)
{
	if( m_iVolume != m_pDiskManager->GetVolumeIndex(m_iDisk, 0) ) {
		
		Trace(debugInfo, "File System Media Change Detected");

		m_iVolume = m_pDiskManager->GetVolumeIndex(m_iDisk, 0);
		
		return CheckRefresh();
		}

	if( m_pDisk->GetWriteCount() != m_uWriteCount ) {

		Trace(debugInfo, "File System Change Detected");

		return CheckRefresh();
		}
	
	return bulkPassed;
	}

UINT CUsbFuncScsiDriver::CheckRefresh(void)
{
	ClearMedia();

	if( SessionCheck(stateMounting) ) {
	
		SessionClear();

		Trace(debugInfo, "Refresh Signalled");
		
		m_SenseKey.Set(senseAttention, addMediumChange);

		return bulkFailed;
		}

	return bulkPassed;
	}

void CUsbFuncScsiDriver::ClearMedia(void)
{
	CachedClear();

	m_pDisk->ClearCounters();

	m_uReadCount  = 0;

	m_uWriteCount = 0;
	}

// Cache Tracking

void CUsbFuncScsiDriver::CachedClear(void)
{
	m_uCachedMin = 0xFFFFFFFF;

	m_uCachedMax = 0;
	}

void CUsbFuncScsiDriver::CachedUpdate(DWORD dwBlock)
{
	MakeMin(m_uCachedMin, dwBlock);
	
	MakeMax(m_uCachedMax, dwBlock);
	}

void CUsbFuncScsiDriver::CachedUpdate(DWORD dwBlock, UINT uCount)
{
	CachedUpdate(dwBlock);
	
	CachedUpdate(dwBlock + uCount - 1);
	}

bool CUsbFuncScsiDriver::CachedIsEmpty(void)
{
	return m_uCachedMin == 0xFFFFFFFF && m_uCachedMax == 0;
	}

// Synch Locks

void CUsbFuncScsiDriver::SetLock(UINT uLock)
{
	if( uLock != m_uLockAct && uLock != m_uLockReq ) {

		FreeLock();

		if( uLock == lockRead ) {
			
			m_uLockReq = lockRead;

			WaitLock(0);

			return;
			}

		if( uLock == lockWrite ) {
			
			m_uLockReq = lockWrite;

			WaitLock(0);

			return;
			}
		}
	}

void CUsbFuncScsiDriver::FreeLock(void)
{
	m_uLockReq = lockNone;

	if( m_uLockAct == lockRead ) {

		ReadLockFree();

		m_uLockAct = lockNone;
		
		return;
		}

	if( m_uLockAct == lockWrite ) {

		m_pCache->Invalidate();

		WriteLockFree();

		m_uLockAct = lockNone;

		return;
		}
	}

bool CUsbFuncScsiDriver::WaitLock(UINT uTimeout)
{
	if( m_uLockReq == m_uLockAct ) {

		return true;
		}

	if( m_uLockReq == lockNone ) {

		return true;
		}

	if( m_uLockReq == lockRead ) {

		if( ReadLockWait(uTimeout) ) {

			m_pCache->Flush();

			m_uLockAct = lockRead;

			return true;
			}

		return false;
		}

	if( m_uLockReq == lockWrite ) {

		if( WriteLockWait(uTimeout) ) {

			m_pCache->Flush();

			m_uLockAct = lockWrite;

			return true;
			}

		return false;
		}

	return false;
	}

bool CUsbFuncScsiDriver::ReadLockWait(UINT uTimeout)
{
	return m_pDiskManager->WaitDiskLock(m_iHost, m_iDisk, diskLockRead, uTimeout);
	}

bool CUsbFuncScsiDriver::ReadLockFree(void)
{
	return m_pDiskManager->FreeDiskLock(m_iHost, m_iDisk, diskLockRead);
	}

bool CUsbFuncScsiDriver::WriteLockWait(UINT uTimeout)
{
	return m_pDiskManager->WaitDiskLock(m_iHost, m_iDisk, diskLockWrite, uTimeout);
	}

bool CUsbFuncScsiDriver::WriteLockFree(void)
{
	return m_pDiskManager->FreeDiskLock(m_iHost, m_iDisk, diskLockWrite);
	}

// Bulk Transport

bool CUsbFuncScsiDriver::RecvCmd(void)
{
	if( RecvData(PBYTE(&m_Cbw), sizeof(m_Cbw)) == sizeof(m_Cbw) ) {

		m_Cbw.ScsiToHost();

		if( !m_Cbw.IsValid() ) {

			Trace(debugWarn, "CBW Invalid");

			m_Cbw.Dump();

			return false;
			}

		return true;
		}

	Trace(debugWarn, "RecvCmd Failed");

	return false;
	}

bool CUsbFuncScsiDriver::SendStatus(UINT uStatus)
{
	m_Csw.Init();

	m_Csw.m_dwTag     = m_Cbw.m_dwTag;

	m_Csw.m_dwResidue = m_Cbw.m_dwTransfer;

	m_Csw.m_bStatus   = uStatus;

	m_Csw.Dump();

	m_Csw.HostToScsi();

	return SendBulk(m_iEndptSend, PBYTE(&m_Csw), sizeof(m_Csw), FOREVER) == sizeof(m_Csw);
	}

UINT CUsbFuncScsiDriver::RecvData(PBYTE pData, UINT uCount)
{
	m_uTimeout = SessionFindTimeout();

	return RecvBulk(m_iEndptRecv, pData, uCount, FOREVER);
	}

bool CUsbFuncScsiDriver::SendData(PBYTE pData, UINT uCount)
{
	return SendBulk(m_iEndptSend, pData, uCount, FOREVER);
	}

// Task Entry

int CUsbFuncScsiDriver::TaskUsbFuncScsi(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("Scsi");

	((CUsbFuncScsiDriver *) pParam)->TaskEntry();

	return 0;
	}

// End of File
