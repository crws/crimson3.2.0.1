
#include "Intern.hpp"

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Class Requeset
//

// Constructor

CUsbClassReq::CUsbClassReq(void)
{
	Init();
	} 

// Init

void CUsbClassReq::Init(void)
{
	CUsbDeviceReq::Init();

	m_Type = reqClass;
	}

// End of File
