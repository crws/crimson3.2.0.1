
#include "Intern.hpp"

#include "UsbHubGetStatusReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Get Status Request
//

// Constructor

CUsbHubGetStatusReq::CUsbHubGetStatusReq(void)
{
	Init();
	} 

// Operations

void CUsbHubGetStatusReq::Init(void)
{
	CUsbClassReq::Init();

	m_bRequest  = reqGetStatus;

	m_Direction = dirDevToHost;

	m_Recipient = recHub;

	m_wLength   = 4;
	}

// End of File
