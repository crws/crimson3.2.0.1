
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDescList_HPP

#define	INCLUDE_UsbDescList_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

struct UsbDesc;         
class  CUsbConfigDesc;   
class  CUsbInterfaceDesc;
class  CUsbEndpointDesc; 
class  CUsbInterfaceDesc;

//////////////////////////////////////////////////////////////////////////
//
// Usb Descriptor Collection
//

class CUsbDescList
{
	public:
		// Constructor
		CUsbDescList(void);

		// Destructor
		~CUsbDescList(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Operations
		void Attach(PBYTE pData, UINT uSize);
		void Take  (PBYTE pData, UINT uLen);
		void Detach(void);
		UINT GetIndexStart(void) const;
		UINT FindIndex(PCBYTE pData) const;
		BOOL IsIndexValid(UINT iIndex) const;

		// Enumerations
		UsbDesc           * Enum(UINT uType, UINT& iIndex) const;
		CUsbConfigDesc    * EnumConfig(UINT &iIndex) const;
		CUsbInterfaceDesc * EnumInterface(UINT &iIndex) const;
		CUsbInterfaceDesc * EnumInterface(UINT &Index, UINT uAltSetting) const;
		CUsbEndpointDesc  * EnumEndpoint(UINT &iIndex) const;
		CUsbConfigDesc    * FindConfig(UINT &iIndex) const;
		CUsbInterfaceDesc * FindInterface(UINT &iIndex, UINT iIdent) const;
		CUsbInterfaceDesc * FindInterface(UINT &iIndex, UINT iIdent, UINT iAltSetting) const;
		
		// Debug
		void Debug(void);

	protected:
		// Data
		PBYTE m_pData;
		UINT  m_uSize;
		BOOL  m_fOwner;
	};

// End of File

#endif
