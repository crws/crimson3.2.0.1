
#include "Intern.hpp"

#include "UsbHostFtdiDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDescList.hpp"

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Ftdi Driver
//

// Instantiator

IUsbHostFtdi * Create_FtdiDriver(void)
{
	IUsbHostFtdi *p = (IUsbHostFtdi *) New CUsbHostFtdiDriver;

	return p;
	}

// Constructor

CUsbHostFtdiDriver::CUsbHostFtdiDriver(void)
{
	m_uPort  = 0;

	m_pName  = "Host FTDI Driver";

	m_Debug  = debugWarn;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostFtdiDriver::~CUsbHostFtdiDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");
	}

// IUnknown

HRESULT CUsbHostFtdiDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostFtdi);

	return CUsbHostFuncDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostFtdiDriver::AddRef(void)
{
	return CUsbHostFuncDriver::AddRef();
	}

ULONG CUsbHostFtdiDriver::Release(void)
{
	return CUsbHostFuncDriver::Release();
	}

// IHostFuncDriver

void CUsbHostFtdiDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncDriver::Bind(pDevice, iInterface);

	m_uPort = iInterface + 1;
	}

void CUsbHostFtdiDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostFtdiDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncDriver::GetDevice(pDev);
	}

BOOL CUsbHostFtdiDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostFtdiDriver::GetVendor(void)
{
	return CUsbHostFuncDriver::GetVendor();
	}

UINT CUsbHostFtdiDriver::GetProduct(void)
{
	return CUsbHostFuncDriver::GetProduct();
	}

UINT CUsbHostFtdiDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostFtdiDriver::GetSubClass(void)
{
	return devVendor;
	}

UINT CUsbHostFtdiDriver::GetProtocol(void)
{
	return devVendor;
	}

UINT CUsbHostFtdiDriver::GetInterface(void)
{
	return CUsbHostFuncDriver::GetInterface();
	}

BOOL CUsbHostFtdiDriver::GetActive(void)
{
	return CUsbHostFuncDriver::GetActive();
	}

BOOL CUsbHostFtdiDriver::Open(CUsbDescList const &List)
{
	Trace(debugInfo, "Open");

	if( FindPipes(List) ) {

		m_uState = usbOpen; 

		return CUsbHostFuncDriver::Open();
		}

	return false;
	}

BOOL CUsbHostFtdiDriver::Close(void)
{
	Trace(debugInfo, "Close");

	return CUsbHostFuncDriver::Close();
	}

void CUsbHostFtdiDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncDriver::Poll(uLapsed);
	}

// Operations

BOOL CUsbHostFtdiDriver::Reset(BOOL fClearTx, BOOL fClearRx)
{
	Trace(debugCmds, "Reset ClearTx=%d ClearRx=%d", fClearTx, fClearRx);
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdReset;

	Req.m_wValue    = (fClearTx && fClearRx) ? 0x00 : fClearTx ? 0x02 : 0x01;
	
	Req.m_wIndex    = m_uPort;
	
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::ModemCtrl(BYTE bMask, BYTE bState)
{
	Trace(debugCmds, "ModemCtrl Mask=0x%2.2X Set=%2.2X", bMask, bState);
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdModemCtrl;

	Req.m_wValue    = MAKEWORD(bState, bMask);
	
	Req.m_wIndex    = m_uPort;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::SetFlowCtrl(BYTE bMask, BYTE bXOn, BYTE bXOff)
{
	Trace(debugCmds, "SetFlowCtrl Mask=0x%2.2X bXOn=0x%2.2X bXOff=0x%2.2X", bMask, bXOn, bXOff);
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdSetFlowCtrl;

	Req.m_wValue    = MAKEWORD(bXOn, bXOff);
	
	Req.m_wIndex    = MAKEWORD(m_uPort, bMask);
		
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::SetBaudRate(UINT uBaud)
{
	Trace(debugCmds, "SetBaudRate Baud=%d", uBaud);

	static const BYTE bAdd8[8] = 
	{ 
		0x00,
		0x03,
		0x02,
		0x00,
		0x01,
		0x01,
		0x02,
		0x03,
		};

	UINT uDiv    = 3000000 / uBaud;

	UINT uAdd    = ((3000000 % uBaud) * 8 + uBaud/2) / uBaud;

	WORD wAddVal = WORD(bAdd8[uAdd]) << 14;

	BYTE bAddIdx = (uAdd > 4 || uAdd == 3) ? Bit(0) : 0; 

	Trace(debugData, "Div    = %d", uDiv);

	Trace(debugData, "Add    = %d", uAdd);

	Trace(debugData, "AddVal = 0x%4.4X", wAddVal);

	Trace(debugData, "AddIdx = 0x%4.4X", bAddIdx);
			
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdSetBaudRate;

	Req.m_wValue    = uDiv | wAddVal;
	
	Req.m_wIndex    = MAKEWORD(m_uPort, bAddIdx);
	
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::SetData(UINT uBits, UINT uParity, UINT uStop, BOOL fBreak)
{
	Trace(debugCmds, "SetData Bits=%d Parity=%d Stop=%d Break=%d", uBits, uParity, uStop, fBreak);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdSetData;

	Req.m_wValue    = uBits;

	Req.m_wValue   |= (uParity << 8);

	Req.m_wValue   |= ((uStop == 2) ? Bit(12) : 0);

	Req.m_wValue   |= (fBreak ? Bit(14) : 0);

	Req.m_wIndex    = m_uPort;
	
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::GetModemStat(WORD &wStatus)
{
	Trace(debugCmds, "GetModemStat");
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdGetModemStat;

	Req.m_wIndex    = m_uPort;

	Req.m_wLength   = sizeof(WORD);
	
	Req.HostToUsb();
	
	if( m_pCtrl->CtrlTrans(Req, PBYTE(&wStatus), sizeof(WORD)) == sizeof(WORD) ) {

		wStatus = ::IntelToHost(wStatus);

		Trace(debugData, "Status 0x%8.8X\n", wStatus);
		
		return true;
		}

	return false;
	}

BOOL CUsbHostFtdiDriver::SetLatTimer(BYTE bLatency)
{
	Trace(debugCmds, "SetLatTimer %d", bLatency);
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdSetLatTimer;

	Req.m_wValue    = bLatency;

	Req.m_wIndex    = m_uPort;
	
	Req.HostToUsb();
	
	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::GetLatTimer(BYTE &bLatency)
{
	Trace(debugCmds, "GetLatTimer");
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdGetLatTimer;

	Req.m_wIndex    = m_uPort;

	Req.m_wLength   = 1;
	
	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, &bLatency, 1) == 1 ) {

		Trace(debugData, "Latency %d", bLatency); 

		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbHostFtdiDriver::SetBitMode(BYTE bMode, BYTE bMask)
{
	Trace(debugCmds, "SetBitMode Mode=0x%2.2X Mask=0x%2.2X", bMode, bMask);
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdSetBitMode;

	Req.m_wValue    = MAKEWORD(bMask, bMode);

	Req.m_wIndex    = m_uPort;
	
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::GetBitMode(BYTE &bData)
{
	Trace(debugCmds, "GetBitMode");
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdGetBitMode;

	Req.m_wIndex    = m_uPort;

	Req.m_wLength   = 1;
	
	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, &bData, 1) == 1 ) {

		Trace(debugData, "I/O Pins 0x%2.2X", bData); 

		return TRUE;
		}

	return FALSE;
	}

UINT CUsbHostFtdiDriver::Send(PBYTE pData, UINT uLen, BOOL fAsync)
{
	Trace(debugData, "Send Port=%d Count=%d Async=%d", m_uPort, uLen, fAsync);

	return m_pSend->SendBulk(pData, uLen, fAsync) ? uLen : 0;
	}

UINT CUsbHostFtdiDriver::Recv(PBYTE pData, UINT uLen, BOOL fAsync)
{
	Trace(debugData, "Recv Port=%d Count=%d Async=%d", m_uPort, uLen, fAsync);

	uLen = m_pRecv->RecvBulk(pData, uLen, fAsync);

	if( uLen != NOTHING ) {

		if( !fAsync ) {

			Trace(debugData, "Recv Actual=%d", uLen);

			AfxDump(pData, uLen);
			}

		return uLen;
		}	

	return NOTHING;
	}

UINT CUsbHostFtdiDriver::WaitSend(UINT uTimeout)
{
	Trace(debugData, "WaitSend Timeout=%d", uTimeout);

	BOOL fOk    = false;

	UINT uCount = 0;
	
	if( m_pSend->WaitAsync(uTimeout, fOk, uCount) ) {

		return fOk ? uCount : 0;
		}

	return NOTHING;
	}

UINT CUsbHostFtdiDriver::WaitRecv(UINT uTimeout)
{
	Trace(debugData, "WaitRecv Timeout=%d", uTimeout);

	BOOL fOk    = false;

	UINT uCount = 0;
	
	if( m_pRecv->WaitAsync(uTimeout, fOk, uCount) ) {

		if( fOk && uCount ) {

			Trace(debugData, "Recv Actual=%d", uCount);

			return uCount;
			}

		return 0;
		}

	return NOTHING;
	}

void CUsbHostFtdiDriver::KillAsync(void)
{
	m_pSend->KillAsync();

	m_pRecv->KillAsync();
	}

BOOL CUsbHostFtdiDriver::PromRead(BYTE bAddr, WORD &wData)
{
	Trace(debugCmds, "PromRead Addr=0x%2.2X", bAddr);
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdPromRead;

	Req.m_wIndex    = bAddr;

	Req.m_wLength   = sizeof(WORD);
	
	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&wData), sizeof(WORD)) == sizeof(WORD) ) {

		wData = ::IntelToHost(wData);

		Trace(debugData, "Data 0x%4.4X", wData); 

		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbHostFtdiDriver::PromWrite(BYTE bAddr, WORD wData)
{
	Trace(debugCmds, "PromWrite Addr=0x%2.2X Data=0x%4.4X", bAddr, wData);
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdPromWrite;

	Req.m_wValue    = wData;

	Req.m_wIndex    = bAddr;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostFtdiDriver::PromErase(void)
{
	Trace(debugCmds, "PromErase");
	
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = cmdPromErase;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

WORD CUsbHostFtdiDriver::PromChecksum(void)
{
	WORD wCrc = 0xAAAA;

	for( UINT i = 0; i < promChecksum; i ++ ) {

		WORD w;
		
		PromRead(i, w);

		wCrc ^= w;

		wCrc  = (wCrc << 1) | (wCrc >> 15);
		}

	return wCrc;
	}

BOOL CUsbHostFtdiDriver::SetOutputLo(BYTE bMask, BYTE bData)
{
	Trace(debugCmds, "SetOutputLo Mask=0x%2.2X Data=0x%2.2X", bMask, bData);

	BYTE bCmd[3] = { 0x80, bData, bMask };

	return m_pSend->SendBulk(bCmd, sizeof(bCmd), false);
	}

BOOL CUsbHostFtdiDriver::SetOutputHi(BYTE bMask, BYTE bData)
{
	Trace(debugCmds, "SetOutputHi Mask=0x%2.2X Data=0x%2.2X", bMask, bData);

	BYTE bCmd[3] = { 0x82, 0x00, bMask };

	return m_pSend->SendBulk(bCmd, sizeof(bCmd), false);
	}

BYTE CUsbHostFtdiDriver::GetInputLo(void)
{
	Trace(debugCmds, "GetInputLo");

	BYTE bCmd = 0x81;

	if( m_pSend->SendBulk(&bCmd, sizeof(bCmd), false) ) {
					
		BYTE bData[3];
		
		if( m_pRecv->RecvBulk(bData, sizeof(bData), false) == sizeof(bData) ) {

			return bData[2];
			}
		}

	return 0;
	}

BYTE CUsbHostFtdiDriver::GetInputHi(void)
{
	Trace(debugCmds, "GetInputHi");

	BYTE bCmd = 0x83;

	if( m_pSend->SendBulk(&bCmd, sizeof(bCmd), false) ) {
					
		BYTE bData[3];
		
		if( m_pRecv->RecvBulk(bData, sizeof(bData), false) == sizeof(bData) ) {

			return bData[2];
			}
		}

	return 0;
	}

// Implementation

bool CUsbHostFtdiDriver::FindPipes(CUsbDescList const &List)
{
	UINT iIndex;

	UsbInterfaceDesc *pInt = (UsbInterfaceDesc *) List.FindInterface(iIndex, m_iInt);

	if( pInt && pInt->m_bEndpoints == 2 ) {

		UsbEndpointDesc *pEp0 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		UsbEndpointDesc *pEp1 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		if( pEp0 && pEp1 ) {

			m_pCtrl = m_pDev->GetCtrlPipe();

			if( pEp0->m_bDirIn ) {

				m_pRecv = m_pDev->GetPipe(pEp0->m_bAddr, pEp0->m_bDirIn);

				m_pSend = m_pDev->GetPipe(pEp1->m_bAddr, pEp1->m_bDirIn);

				m_iRecv = pEp0->m_bAddr | Bit(7);

				m_iSend = pEp1->m_bAddr;
				}
			else {
				m_pSend = m_pDev->GetPipe(pEp0->m_bAddr, pEp0->m_bDirIn);

				m_pRecv = m_pDev->GetPipe(pEp1->m_bAddr, pEp1->m_bDirIn);

				m_iSend = pEp0->m_bAddr;

				m_iRecv = pEp1->m_bAddr | Bit(7);
				}

			Trace(debugCmds, "IN  Endpoint 0x%2.2X", m_iRecv);
	
			Trace(debugCmds, "OUT Endpoint 0x%2.2X", m_iSend);

			return m_pCtrl != NULL && m_pSend != NULL && m_pRecv != NULL;
			}
		}

	return false;
	}

// End of File
