
#include "Intern.hpp"

#include "UsbHidDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hid Descriptor
//

// Constructor

CUsbHidDesc::CUsbHidDesc(void)
{
	Init();
	}

// Endianess

void CUsbHidDesc::HostToUsb(void)
{
	m_wHid = HostToIntel(m_wHid);

	for( UINT i = 0; i < m_bNum; i ++ ) {

		m_List[i].m_wSize = HostToIntel(m_List[i].m_wSize);
		}
	}

void CUsbHidDesc::UsbToHost(void)
{
	m_wHid = IntelToHost(m_wHid);

	for( UINT i = 0; i < m_bNum; i ++ ) {

		m_List[i].m_wSize = IntelToHost(m_List[i].m_wSize);
		}
	}

// Attributes

bool CUsbHidDesc::IsValid(void)
{
	return m_bType == descHid && m_bLength >= sizeof(HidDesc);
	}

// Init

void CUsbHidDesc::Init(void)
{
	memset(this, 0, sizeof(HidDesc));
	
	m_bType	  = descHid;

	m_bLength = sizeof(HidDesc);
	}

// Child List

UINT CUsbHidDesc::GetDescType(UINT uIdx)
{
	return uIdx < m_bNum ? m_List[uIdx].m_bType : 0;	
	}

UINT CUsbHidDesc::GetDescSize(UINT uIdx)
{
	return uIdx < m_bNum ? m_List[uIdx].m_wSize : 0;	
	}

UINT CUsbHidDesc::FindDesc(UINT bType)
{
	for( UINT i = 0; i < m_bNum; i ++ ) {

		if( m_List[i].m_bType == bType ) {

			return i;
			}
		}

	return NOTHING;
	}

UINT CUsbHidDesc::GetReportSize(void)
{
	return GetDescSize(FindDesc(descReport));
	}

// Debug

void CUsbHidDesc::Debug(void)
{
	#if defined(_XDEBUG)	

	AfxTrace("\nHid Descriptor\n");

	AfxTrace("Type          = 0x%2.2X\n",  m_bType);
	AfxTrace("Len           = %d\n",       m_bLength);
	AfxTrace("Hid           = 0x%4.4X\n",  m_wHid);
	AfxTrace("Country       = 0x%2.2X\n",  m_bCountry);
	AfxTrace("Num           = 0x%2.2X\n",  m_bNum);
	
	#endif
	}

// End of File
