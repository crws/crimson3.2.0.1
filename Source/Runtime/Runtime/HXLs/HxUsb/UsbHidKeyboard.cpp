
#include "Intern.hpp"  

#include "UsbHidKeyboard.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hid Keyboard Mapper
//

// Instantiator

IUsbHidMapper * Create_HidKeyboardMapper(void)
{
	IUsbHidMapper *p = New CUsbHidKeyboard;

	return p;
	}

// Constructor

CUsbHidKeyboard::CUsbHidKeyboard(void)
{
	m_pName     = "Hid Keyboard Mapper";

	m_Debug	    = debugWarn;

	m_pKeyState = NULL;

	m_wScan     = 0;

	m_fCaps     = false;

	m_fEnable   = false;

	m_pMap[0]   = UsageUS1;

	m_pMap[1]   = UsageUS2;

	AfxGetObject("input-d", 0, IInputQueue, m_pInput);

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHidKeyboard::~CUsbHidKeyboard(void)
{
	Trace(debugInfo, "Driver Destroyed");

	AfxRelease(m_pInput);
	}

// IUsbHidMapper

BOOL CUsbHidKeyboard::SetReport(PCBYTE pReport, UINT uSize)
{
	ParseReportDesc(pReport, uSize);

	// NOTE -- Override report size to match boot protocol.

	m_uRecvSize = 64;

	m_uKeyArray = GetRecvSize(0) ? GetRecvSize(0) - 2 : 0;

	InitBuffers();

	Configure();

	UpdateLEDs();

	return TRUE;
	}

UINT CUsbHidKeyboard::GetUsagePage(void)
{
	return pageDesktop;
	}

UINT CUsbHidKeyboard::GetUsageType(void)
{
	return desktopKeyboard;
	}

void CUsbHidKeyboard::SetConfig(PCBYTE pConfig, UINT uSize)
{
	if( uSize >= 1 * sizeof(UINT) ) {

		if( m_fEnable != PUINT(pConfig)[0] ) {

			m_fEnable = PUINT(pConfig)[0];
			}
		}

	if( uSize >= 2 * sizeof(UINT) ) {

		switch( PUINT(pConfig)[1] ) {

			case 0:
				m_pMap[0] = UsageUS1;
				m_pMap[1] = UsageUS2;

				break;

			case 1: 
				m_pMap[0] = UsageUK1;
				m_pMap[1] = UsageUK2;

				break;
			}
		}
	}

void CUsbHidKeyboard::Poll(void)
{
	while( RecvReport() ) {

		ParseReport();
		}

	CheckRepeat();
	}

// Helpers

void CUsbHidKeyboard::Configure(void)
{
	m_pHostDriver->SetIdle(0);

	m_pHostDriver->SetProtocol(protBoot);
	}

void CUsbHidKeyboard::UpdateLEDs(void)
{
	if( m_uSendSize ) {

		FormatReport();

		SendReport();
		}
	}

bool CUsbHidKeyboard::ParseReport(void)
{
	m_wScan++;
	
	m_bModifier = m_pRecvBuff[0];

	for( UINT i = 2; i < GetRecvSize(0); i ++ ) {

		BYTE bKey = m_pRecvBuff[i];

		if( bKey ) {

			if( FindKeyState(bKey) == NOTHING ) {

				UINT iKey = SaveKeyState(bKey);

				ProcessKey(iKey, true);
				}
			} 
		}

	for( UINT i = 0; i < m_uKeyArray; i ++ ) {

		if( m_pKeyState[i].m_bCode && m_pKeyState[i].m_wScan != m_wScan ) {
			
			ProcessKey(i, false);

			FreeKeyState(i);
			}
		}
	
	return true;
	}

void CUsbHidKeyboard::CheckRepeat(void)
{
	for( UINT i = 0; i < m_uKeyArray; i ++ ) {

		CKeyState &k = m_pKeyState[i];
	
		if( k.m_bCode && GetTickCount() > k.m_uRepTime ) {

			BYTE bKey = LookupKey(k.m_bCode, m_bModifier);

			if( bKey ) {

				if( m_fEnable ) {
						
					CInput Input;

					Input.x.i.m_Type   = typeKey;

					Input.x.i.m_State  = stateRepeat;

					Input.x.i.m_Local  = TRUE;

					Input.x.d.k.m_Code = bKey;

					m_pInput->Store(Input.m_Ref);
					}

				k.m_uRepTime = GetTickCount() + ToTicks(1000 / constRate);
				}
			}
		}
	}

void CUsbHidKeyboard::FormatReport(void)
{
	m_pSendBuff[0]  = ledNum;

	m_pSendBuff[0] |= (m_fCaps ? ledCaps : 0);
	}
		
// Buffers

void CUsbHidKeyboard::InitBuffers(void)
{
	FreeBuffers();

	m_pKeyState = New CKeyState[ m_uKeyArray ];
	
	memset(m_pKeyState, 0, sizeof(CKeyState) * m_uKeyArray);

	CUsbHidMapper::InitBuffers();
	}

void CUsbHidKeyboard::FreeBuffers(void)
{
	if( m_pKeyState ) {

		delete [] m_pKeyState;

		m_pKeyState = NULL;
		}

	CUsbHidMapper::FreeBuffers();
	}

// Implmentation

UINT CUsbHidKeyboard::FindKeyState(BYTE bCode)
{
	for( UINT i = 0; i < m_uKeyArray; i ++ ) {

		if( m_pKeyState[i].m_bCode == bCode ) {

			m_pKeyState[i].m_wScan = m_wScan;

			return i;
			}
		}

	return NOTHING;
	}

UINT CUsbHidKeyboard::SaveKeyState(BYTE bCode)
{
	for( UINT i = 0; i < m_uKeyArray; i ++ ) {

		if( !m_pKeyState[i].m_bCode ) {

			m_pKeyState[i].m_bCode    = bCode;

			m_pKeyState[i].m_wScan    = m_wScan;

			m_pKeyState[i].m_uRepTime = GetTickCount() + ToTicks(constDelay); 
			
			return i;
			}
		}

	return NOTHING;
	}

void CUsbHidKeyboard::FreeKeyState(UINT iIndex)
{
	if( iIndex < m_uKeyArray ) {
	
		m_pKeyState[ iIndex ].m_bCode = 0;
		}
	}

void CUsbHidKeyboard::ProcessKey(UINT iKey, bool fDown)
{
	CKeyState &k = m_pKeyState[iKey];

	if( k.m_bCode == 0x39 && fDown ) {
		
		m_fCaps = !m_fCaps;

		UpdateLEDs();
		}
	else {
		if( m_fEnable ) {

			BYTE bKey = LookupKey(k.m_bCode, m_bModifier);

			if( bKey ) {
	
				CInput Input;
		
				Input.x.i.m_Type   = typeKey;

				Input.x.i.m_State  = fDown ? stateDown : stateUp;

				Input.x.i.m_Local  = TRUE;

				Input.x.d.k.m_Code = bKey;

				m_pInput->Store(Input.m_Ref);
				}
			}
		}
	}

BYTE CUsbHidKeyboard::LookupKey(BYTE bKey, BYTE bMask)
{
	if( bKey < elements(UsageUS1) ) {
	
		if( bMask & keyShift ) {

			return m_pMap[1][bKey];
			}

		bKey = m_pMap[0][bKey];

		if( bMask & keyAlt ) {

			if( bKey >= 0x80 && bKey <= 0x8F ) {

				bKey += 0x10; 
				
				return bKey;
				}
			}

		if( m_fCaps ) {
			
			if( bKey >= 'a' && bKey <= 'z' ) {

				bKey -= 'a';

				bKey += 'A';

				return bKey;
				}
			}

		return bKey;
		}

	return 0;	
	}

// End of File
