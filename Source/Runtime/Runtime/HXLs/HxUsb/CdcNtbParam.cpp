
#include "Intern.hpp"

#include "CdcNtbParam.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// NTB Param Block
//

// Constructor

CCdcNtbParam::CCdcNtbParam(void)
{
	}

// Endianess

void CCdcNtbParam::HostToCdc(void)
{
	m_wLength	= HostToIntel(m_wLength);
	m_wFormats	= HostToIntel(m_wFormats);
	m_dwRecvSize	= HostToIntel(m_dwRecvSize);
	m_wRecvDivisor	= HostToIntel(m_wRecvDivisor);
	m_wRecvReminder	= HostToIntel(m_wRecvReminder);
	m_wRecvAlign	= HostToIntel(m_wRecvAlign);
	m_dwSendSize	= HostToIntel(m_dwSendSize);
	m_wSendDivisor	= HostToIntel(m_wSendDivisor);
	m_wSendReminder	= HostToIntel(m_wSendReminder);
	m_wSendAlign	= HostToIntel(m_wSendAlign);
	m_wSendCount	= HostToIntel(m_wSendCount);
	}

void CCdcNtbParam::CdcToHost(void)
{
	m_wLength	= IntelToHost(m_wLength);
	m_wFormats	= IntelToHost(m_wFormats);
	m_dwRecvSize	= IntelToHost(m_dwRecvSize);
	m_wRecvDivisor	= IntelToHost(m_wRecvDivisor);
	m_wRecvReminder	= IntelToHost(m_wRecvReminder);
	m_wRecvAlign	= IntelToHost(m_wRecvAlign);
	m_dwSendSize	= IntelToHost(m_dwSendSize);
	m_wSendDivisor	= IntelToHost(m_wSendDivisor);
	m_wSendReminder	= IntelToHost(m_wSendReminder);
	m_wSendAlign	= IntelToHost(m_wSendAlign);
	m_wSendCount	= IntelToHost(m_wSendCount);
	}

// Operations

void CCdcNtbParam::Init(void)
{
	memset(this, 0, sizeof(CdcNtbParam));
	}

// End of File
