
//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSetInterfaceReq_HPP

#define	INCLUDE_UsbSetInterfaceReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Interface Standard Device Requeset
//

class CUsbSetInterfaceReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbSetInterfaceReq(WORD wAlt, WORD wIndex);

		// Operations
		void Init(WORD wAlt, WORD wIndex);
	};

// End of File

#endif
