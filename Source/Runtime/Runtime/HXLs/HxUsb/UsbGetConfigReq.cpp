
#include "Intern.hpp"

#include "UsbGetConfigReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Config Standard Device Request
//

// Constructor

CUsbGetConfigReq::CUsbGetConfigReq(void)
{
	Init();
	} 

// Operations

void CUsbGetConfigReq::Init(void)
{
	CUsbStandardReq::Init();

	m_bRequest  = reqGetConfig;

	m_Direction = dirDevToHost;

	m_Recipient = recDevice;

	m_wLength   = 1;
	}

// End of File
