
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostEcmDriver_HPP

#define	INCLUDE_UsbHostEcmDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostCdcDriver.hpp"

#include "CdcNetwork.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host CDC Ethernet Class Driver
//

class CUsbHostEcmDriver : public CUsbHostCdcDriver, public IUsbHostEcm
{
	public:
		// Constructor
		CUsbHostEcmDriver(void);

		// Destructor
		~CUsbHostEcmDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostCdc
		BOOL METHOD SendEncapCommand(PBYTE pData, UINT uLen);
		UINT METHOD RecvEncapResponse(PBYTE pData, UINT uLen);
		BOOL METHOD SendData(PCTXT pData, bool fAsync);
		BOOL METHOD SendData(PCBYTE pData, UINT uLen, bool fAsync);
		UINT METHOD RecvData(PBYTE  pData, UINT uLen, bool fAsync);
		UINT METHOD WaitSend(UINT uTimeout);
		UINT METHOD WaitRecv(UINT uTimeout);
		void METHOD KillSend(void);
		void METHOD KillRecv(void);
		void METHOD KillAsync(void);

		// IUsbHostEcm
		BOOL METHOD IsLinkActive(void);
		BOOL METHOD WaitLinkActive(UINT uTime);
		void METHOD GetMac(MACADDR &Addr);
		UINT METHOD GetMaxFilters(void);
		BOOL METHOD GetFilterHashing(void);
		UINT METHOD GetCapabilities(void);
		BOOL METHOD SetMulticastFilters(MACADDR const *pList, UINT uList);
		BOOL METHOD SetPacketFilters(WORD wFilter);
		BOOL METHOD GetStatistics(UINT iSelector, DWORD &Data);

		// Packet Filter
		enum
		{
			typeMulticast		= Bit(4),
			typeBroadcast		= Bit(3),
			typeDirected		= Bit(2),
			typeMulticastAll	= Bit(1),
			typeAll			= Bit(0),
			};

		// Feature Selector
		enum
		{
			selTxOk			= 0x01,
			selRxOk			= 0x02,
			selTxErr		= 0x03,
			selRxErr		= 0x04,
			selRxNoBuff		= 0x05,
			selTxDirectedBytes	= 0x06,
			selTxDirectedFrames	= 0x07,
			selTxMultiBytes		= 0x08,
			selTxMutliFrames	= 0x09,
			selTxBroadcastBytes	= 0x0A,
			selTxBroadcastFrames	= 0x0B,
			selRxDirectedBytes	= 0x0C,
			selRxDirectedFrames	= 0x0D,
			selRxMultiBytes		= 0x0E,
			selRxMutliFrames	= 0x0F,
			selRxBroadcastBytes	= 0x10,
			selRxBroadcastFrames	= 0x11,
			selRxCrcErr		= 0x12,
			selTxQueueLen		= 0x13,
			selRxAlignErr		= 0x14,
			selTxCollisionOne	= 0x15,
			selTxCollisionMore	= 0x16,
			selTxDeferred		= 0x17,
			selTxCollisionMax	= 0x18,
			selRxOverrun		= 0x19,
			selTxUnderrun		= 0x1A,
			selTxHeartbeatFail	= 0x1B,
			selTxTimesLost		= 0x1C,
			selTxCollisionLate	= 0x1D,
			};

	protected:
		// Data
		CCdcNetwork * m_pNet;
		MACADDR       m_Mac;
		IEvent      * m_pLink;
		IEvent      * m_pStop;

		// Notifications
		void OnEvent(UsbDeviceReq const &Req);
		void OnConnect(UsbDeviceReq const &Req);
		void OnSpeed(UsbDeviceReq const &Req);

		// Implementation 
		bool ParseNetwork(void);
	};

// End of File

#endif
