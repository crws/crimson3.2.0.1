
#include "UsbHostEnhanced.hpp"

#include "UsbEndpointDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Framework
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#undef  Reg

#define Reg(x)	(m_pBase[reg##x])

#undef  Caps

#define Caps(x)	(m_pCaps[cap##x])

//////////////////////////////////////////////////////////////////////////
//
// USB Enhanced Host Controller Interface
//

// Instantiator 

IUsbHostInterfaceDriver * Create_UsbHostEnhanced(void)
{
	CUsbHostEnhanced *p = New CUsbHostEnhanced;

	return p;
	}

// Constructor

CUsbHostEnhanced::CUsbHostEnhanced(void)
{
	m_pCaps    = NULL;
	
	m_pBase    = NULL;
	
	m_pQhList  = NULL; 
	
	m_uQhSize  = 0;
	
	m_pTdList  = NULL;
	
	m_pTrList  = NULL;
	
	m_uTdSize  = 0;

	m_pLock    = NULL;

	m_pQhLimit = NULL; 

	m_pAdvance = Create_ManualEvent();
	
	AfxGetObject("buffman", 0, IBufferManager, m_pBuffMan);	
	}

// Destructor

CUsbHostEnhanced::~CUsbHostEnhanced(void)
{
	AfxRelease(m_pAdvance);

	AfxRelease(m_pBuffMan);

	AfxRelease(m_pLock);

	AfxRelease(m_pQhLimit);
	}

// IUsbDriver

BOOL CUsbHostEnhanced::Init(void)
{
	if( CUsbHostHardwareDriver::Init() ) {

		MakePeriodic();
						
		MakeQueueHeads();

		MakeTransfers();

		MakeFlags();

		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbHostEnhanced::Start(void)
{
	if( CUsbHostHardwareDriver::Start() ) {
		
		StopController();

		ResetController();

		InitController();

		InitPeriodic();
			
		InitQueueHeads();

		InitAsync();

		InitTransfers();

		StartController();

		PortPower(true);

		EnableEvents(true);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CUsbHostEnhanced::Stop(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {

		EnableEvents(false);
		
		AsyncEnable(false);

		PeriodicEnable(false);

		PortPower(false);

		StopController();

		return CUsbHostHardwareDriver::Stop();
		}

	return FALSE;
	}
		
// IUsbHostInterfaceDriver

UINT CUsbHostEnhanced::GetType(void) 
{
	return usbEhci;
	}

UINT CUsbHostEnhanced::GetPortCount(void)
{
	return GetSParams().m_dwPorts;
	}

BOOL CUsbHostEnhanced::GetPortConnect(UINT iPort)
{
	return PortGet(iPort, portConnect);
	}

BOOL CUsbHostEnhanced::GetPortEnabled(UINT iPort)
{
	return PortGet(iPort, portEnable);
	}

BOOL CUsbHostEnhanced::GetPortCurrent(UINT iPort)
{
	return PortGet(iPort, portHiCurrent);
	}

BOOL CUsbHostEnhanced::GetPortSuspend(UINT iPort)
{
	return PortGet(iPort, portSuspend);
	}

BOOL CUsbHostEnhanced::GetPortReset(UINT iPort)
{
	return PortGet(iPort, portReset);
	}

BOOL CUsbHostEnhanced::GetPortPower(UINT iPort)
{
	return PortGet(iPort, portPower);
	}

UINT CUsbHostEnhanced::GetPortSpeed(UINT iPort)
{
	return usbSpeedHigh;
	}

void CUsbHostEnhanced::SetPortEnable(UINT iPort, BOOL fSet)
{
	if( !fSet ) {	

		PortClr(iPort, portEnable);
		}
	}

void CUsbHostEnhanced::SetPortSuspend(UINT iPort, BOOL fSet)
{
	if( fSet ) {	

		PortSet(iPort, portSuspend);
		}
	}

void CUsbHostEnhanced::SetPortResume(UINT iPort, BOOL fSet)
{
	PortSet(iPort, portResume, fSet);	
	}

void CUsbHostEnhanced::SetPortReset(UINT iPort, BOOL fSet)
{
	PortSet(iPort, portReset, fSet);	
	}

void CUsbHostEnhanced::SetPortPower(UINT iPort, BOOL fSet)
{
	PortSet(iPort, portPower, fSet);	
	}

void CUsbHostEnhanced::EnableEvents(BOOL fEnable) 
{
	DWORD dwMask = intTransfer | intPortChange | intSysError | intAsyncAdv;

	if( fEnable ) {

		Reg(UsbIntr) |= dwMask;
		}
	else {
		Reg(UsbIntr) &= ~dwMask;
		}
	}

BOOL CUsbHostEnhanced::SetEndptDevAddr(DWORD iIndex, BYTE bAddr)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh  = (CQueueHead *) iIndex;
		
		pQh->m_dwDevAddr = bAddr;
		
		return true;
		}

	return false;
	}

UINT CUsbHostEnhanced::GetEndptDevAddr(DWORD iIndex)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh  = (CQueueHead *) iIndex;
		
		return pQh->m_dwDevAddr;
		}

	return NOTHING;
	}

BOOL CUsbHostEnhanced::SetEndptAddr(DWORD iIndex, BYTE bAddr)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh = (CQueueHead *) iIndex;
		
		pQh->m_dwEndpt  = bAddr;
		
		return true;
		}

	return false;
	}

BOOL CUsbHostEnhanced::SetEndptMax(DWORD iIndex, WORD wMaxPacket)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh    = (CQueueHead *) iIndex;
		
		pQh->m_dwMaxPacket = wMaxPacket;
		
		return true;
		}

	return false;
	}

BOOL CUsbHostEnhanced::SetEndptHub(DWORD iIndex, BYTE bHub, BYTE bPort)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh  = (CQueueHead *) iIndex;
		
		pQh->m_dwHubAddr = bHub;

		pQh->m_dwPortNum = bPort;
		
		return true;
		}

	return false;
	}

DWORD CUsbHostEnhanced::MakeEndptCtrl(DWORD iDev, UINT uSpeed)
{
	CQueueHead *pQh = AllocQueueHead();

	if( pQh ) {

		pQh->m_dwDevAddr    = 0;
		pQh->m_dwEndpt	    = 0;
		pQh->m_dwSpeed	    = uSpeed;
		pQh->m_dwToggleCtrl = true;
		pQh->m_dwHeadFlag   = false;
		pQh->m_dwMaxPacket  = uSpeed == usbSpeedLow  ? 8 : 64;
		pQh->m_dwControl    = uSpeed == usbSpeedHigh ? false :  true;
		pQh->m_dwNakReload  = 7;
		pQh->m_dwHubAddr    = 0;
		pQh->m_dwPortNum    = 0;
		pQh->m_dwPipeMult   = 3;
		pQh->m_CurrentTDPtr = 0;

		pQh->m_Transfer.m_Next.m_dwTerm = true;

		pQh->m_Transfer.m_Alt.m_dwTerm  = true;

		AsyncInsert(pQh);

		return DWORD(pQh);
		}

	return NOTHING;
	}

DWORD CUsbHostEnhanced::MakeEndptAsync(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc)
{
	CQueueHead *pQh = AllocQueueHead();

	if( pQh ) {

		pQh->m_dwDevAddr    = 0;
		pQh->m_dwEndpt	    = Desc.m_bAddr;
		pQh->m_dwSpeed	    = uSpeed;
		pQh->m_dwToggleCtrl = false;
		pQh->m_dwHeadFlag   = false;
		pQh->m_dwMaxPacket  = Desc.m_wMaxPacket;
		pQh->m_dwControl    = false;
		pQh->m_dwNakReload  = 7;
		pQh->m_dwHubAddr    = 0;
		pQh->m_dwPortNum    = 0;
		pQh->m_dwPipeMult   = uSpeed == usbSpeedHigh ? 3 : 1;
		pQh->m_CurrentTDPtr = 0;

		pQh->m_Transfer.m_Next.m_dwTerm = true;

		pQh->m_Transfer.m_Alt.m_dwTerm  = true;

		AsyncInsert(pQh);

		return DWORD(pQh);
		}

	return NOTHING;
	}

DWORD CUsbHostEnhanced::MakeEndptInt(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc)
{
	CQueueHead *pQh = AllocQueueHead();

	if( pQh ) {

		pQh->m_dwDevAddr    = 0;
		pQh->m_dwEndpt	    = Desc.m_bAddr;
		pQh->m_dwSpeed	    = uSpeed;
		pQh->m_dwToggleCtrl = false;
		pQh->m_dwHeadFlag   = false;
		pQh->m_dwMaxPacket  = Desc.m_wMaxPacket;
		pQh->m_dwControl    = false;
		pQh->m_dwNakReload  = 1;
		pQh->m_dwHubAddr    = 0;
		pQh->m_dwPortNum    = 0;
		pQh->m_dwPipeMult   = 1;
		pQh->m_CurrentTDPtr = 0;
		pQh->m_dwFrameSMask = 0x01;
		pQh->m_dwFrameCMask = 0x1C;

		pQh->m_Transfer.m_Next.m_dwTerm = true;

		pQh->m_Transfer.m_Alt.m_dwTerm  = true;

		PeriodicInsert(pQh, Desc.m_bInterval);

		return DWORD(pQh);
		}

	return NOTHING;
	}

BOOL CUsbHostEnhanced::KillEndpt(DWORD iIndex)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh = (CQueueHead *) iIndex;

		if( pQh->m_Link.m_dwType == ptrQueueHead ) {

			AsyncRemove(pQh);

			PeriodicRemove(pQh);

			AbortTransfers(pQh);

			FreeTransfers(pQh);

			FreeQueueHead(pQh);

			return true;
			}
		}

	return false;
	}

BOOL CUsbHostEnhanced::ResetEndpt(DWORD iIndex)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh = (CQueueHead *) iIndex;

		pQh->m_Transfer.m_dwToggle = false;

		return true;
		}

	return false;
	}

BOOL CUsbHostEnhanced::Transfer(UsbIor &Urb)
{
	if( (Reg(UsbSts) & statHalted) || !(Reg(UsbCmd) & cmdRun) ) {

		/* AfxTrace("CUsbHostEnhanced::Transfer - Controller Halted\n"); */
		
		return false;
		}
	
	if( Urb.m_iEndpt == NOTHING ) {

		AfxTrace("CUsbHostEnhanced::Transfer - Invalid Queue Head\n");
		
		return false;
		}

	if( Urb.m_uCount > constTdData ) {

		AfxTrace("CUsbHostEnhanced::Transfer - Invalid Transfer Size\n");
		
		return false;
		}

	if( !(Urb.m_uFlags & (Urb.flagSetup | Urb.flagIn | Urb.flagOut)) ) {

		AfxTrace("CUsbHostEnhanced::Transfer - Unknown PID\n");
	
		return false;
		}

	CQueueHead *pQh = (CQueueHead *) Urb.m_iEndpt;

	if( pQh->m_Transfer.m_dwHalted ) {

		pQh->m_Transfer.m_dwHalted = false;
		}

	CTransfer *pTr = AllocTransfer(pQh);

	if( pTr == NULL ) {

		AfxTrace("CUsbHostEnhanced::Transfer - Failed Transfer Allocation\n");

		return false;
		}

	CTransferDesc *pTd = pTr->m_pTd;

	pTr->m_pUrb = (Urb.m_uFlags & Urb.flagNotify) ? &Urb : NULL;

	if( Urb.m_uFlags & Urb.flagSetup ) {

		pTd->m_dwPID = pidSetup;
		}

	else if( Urb.m_uFlags & Urb.flagIn ) {

		pTd->m_dwPID = pidIn;
		}

	else if( Urb.m_uFlags & Urb.flagOut ) {

		pTd->m_dwPID = pidOut;
		}

	if( Urb.m_uFlags & (Urb.flagData0 | Urb.flagData1) ) {

		pQh->m_dwToggleCtrl = true;
			
		pTd->m_dwToggle     = (Urb.m_uFlags & Urb.flagData1) ? true : false;
		}
	else {
		pQh->m_dwToggleCtrl = false;
		}

	pTd->m_dwIoc   = (Urb.m_uFlags & Urb.flagNotify) ? true : false;
		
	pTd->m_dwTotal = Urb.m_uCount;

	Urb.m_uStatus  = Urb.statActive;

	if( Urb.m_pData ) {

		if( Urb.m_uFlags & (Urb.flagOut | Urb.flagSetup) ) {

			m_pLowerDrv->MemCpy(pTr->m_pData, PBYTE(Urb.m_pData), Urb.m_uCount);
			}
		}

	StartTransfer(pQh, pTr);

	return true;
	}

BOOL CUsbHostEnhanced::AbortPending(DWORD iIndex)
{
	if( iIndex != NOTHING ) {

		CQueueHead *pQh = (CQueueHead *) iIndex;

		if( pQh->m_Link.m_dwType == ptrQueueHead ) {

			if( AsyncListed(pQh) ) {

				AsyncRemove(pQh);

				AbortTransfers(pQh);
				
				pQh->m_Transfer.m_Next.m_dwTerm = true;

				pQh->m_Transfer.m_Alt.m_dwTerm  = true;

				pQh->m_Transfer.m_dwActive      = false;
								
				AsyncInsert(pQh);
				
				return true;
				}

			if( PeriodicListed(pQh) ) {

				PeriodicRemove(pQh);

				AbortTransfers(pQh);

				pQh->m_Transfer.m_Next.m_dwTerm = true;

				pQh->m_Transfer.m_Alt.m_dwTerm  = true;

				pQh->m_Transfer.m_dwActive      = false;

				PeriodicInsert(pQh, 0);
				
				return true;
				}
			}
		}

	return false;
	}

// IUsbHostEnhancedDriver

BOOL CUsbHostEnhanced::GetGlobalOwner(void)
{
	return Reg(CfgFlag);
	}

BOOL CUsbHostEnhanced::GetPortOwner(UINT iPort)
{
	return PortGet(iPort, portOwner);
	}

void CUsbHostEnhanced::SetGlobalOwner(BOOL fSet)
{
	Reg(CfgFlag) = fSet ? 1 : 0;
	}

void CUsbHostEnhanced::SetPortOwner(UINT iPort, BOOL fSet)
{
	PortSet(iPort, portOwner, fSet);
	}

// IUsbEvents

void CUsbHostEnhanced::OnBind(IUsbDriver *pDriver)
{
	CUsbHostHardwareDriver::OnBind(pDriver);

	m_pCaps = PCDWORD(m_pLowerDrv->GetBaseAddr());
		
	m_pBase = PVDWORD(m_pLowerDrv->GetBaseAddr() + GetCapsLen());

	DumpHardwareCaps();
	}

// IUsbHostHardwareEvents

void CUsbHostEnhanced::OnEvent(void)
{
	for(;;) {

		DWORD dwEvent = Reg(UsbSts) & Reg(UsbIntr);

		if( dwEvent & intPortChange ) {

			Reg(UsbSts) = intPortChange;

			OnPortChange();
			}

		if( dwEvent & intTransfer ) {

			Reg(UsbSts) = intTransfer;

			OnTransfer();
			}

		if( dwEvent & intAsyncAdv ) {

			Reg(UsbSts) = intAsyncAdv;

			OnAsyncAdvance();
			}

		if( dwEvent & intError ) {

			Reg(UsbSts) = intError;

			OnError();
			}

		if( dwEvent & intSysError ) {	

			Reg(UsbSts) = intSysError;

			OnSysError();
			}

		if( !dwEvent ) break;
		}
	}

// Init

void CUsbHostEnhanced::MakeFlags(void)
{
	m_pLock    = Create_Mutex();

	m_pQhLimit = Create_Semaphore();
	}

void CUsbHostEnhanced::InitController(void)
{
	Reg(UsbCmd)    = 0x00080000;

	Reg(UsbSts)   |= 0x0000003F;
	
	Reg(CtrlSegt)  = 0x00000000;
	
	Reg(FrameList) = 0x00000000;
	
	Reg(AsyncList) = 0x00000000;

	Reg(CfgFlag)   = cfgFlag;
	}

void CUsbHostEnhanced::ResetController(void)
{
	if( Reg(UsbSts) & statHalted ) {

		Reg(UsbCmd) |= cmdReset;

		while( Reg(UsbCmd) & cmdReset );
		}
	}

void CUsbHostEnhanced::StartController(void)
{
	if( Reg(UsbSts) & statHalted ) {

		Reg(UsbCmd) |= cmdRun;
		}
	}

void CUsbHostEnhanced::StopController(void)
{
	if( !(Reg(UsbSts) & statHalted) ) {

		Reg(UsbCmd) &= ~cmdRun;

		while( !(Reg(UsbSts) & statHalted) );
		}
	}

// Capabilities

UINT CUsbHostEnhanced::GetCapsLen(void) const
{
	return BYTE(Caps(CapLength));
	}

CUsbHostEnhanced::CSParams & CUsbHostEnhanced::GetSParams(void) const
{
	return (CSParams &) Caps(HcsParams);
	}

CUsbHostEnhanced::CCParams & CUsbHostEnhanced::GetCParams(void) const
{
	return (CCParams &) Caps(HccParams);
	}

// Event Handlers

void CUsbHostEnhanced::OnPortChange(void)
{
	static const DWORD dwMask = ~(portConnectChange | portEnableChange | portHiCurrentChange);

	for( UINT n = 0; n < GetSParams().m_dwPorts; n ++ ) {

		DWORD volatile &PortSc = Reg(PortSc + n);
				
		if( PortSc & portConnectChange ) {

			PortSc = (PortSc & dwMask) | portConnectChange;

			OnPortConnect(n);
			}

		if( PortSc & portEnableChange ) {

			PortSc = (PortSc & dwMask) | portEnableChange;

			OnPortEnable(n);
			}

		if( PortSc & portHiCurrentChange ) {

			PortSc = (PortSc & dwMask) | portHiCurrentChange;

			OnPortCurrent(n);
			}
		}
	}

void CUsbHostEnhanced::OnPortConnect(UINT nPort)
{
	UsbPortPath Path = { { nPort, m_iIndex, 0, 0, 0 } };
	
	if( Reg(PortSc + nPort) & portConnect ) {

		m_pUpperDrv->OnPortConnect(Path);
		}
	else {
		m_pUpperDrv->OnPortRemoval(Path);
		}
	}

void CUsbHostEnhanced::OnPortEnable(UINT nPort)
{
	UsbPortPath Path = { { nPort, m_iIndex, 0, 0, 0 } };
	
	m_pUpperDrv->OnPortEnable(Path);
	}

void CUsbHostEnhanced::OnPortCurrent(UINT nPort)
{
	UsbPortPath Path = { { nPort, m_iIndex, 0, 0, 0 } };
	
	m_pUpperDrv->OnPortCurrent(Path);
	}

void CUsbHostEnhanced::OnTransfer(void)
{
	CTransfer *pTr = m_pUsedHead;

	while( pTr ) {

		CTransfer     *pNext = pTr->m_pNext;

		CTransferDesc *pTd   = pTr->m_pTd;

		if( !pTd->m_dwActive ) {

			if( pTr->m_pUrb ) {

				UsbIor &Urb = *pTr->m_pUrb;

				if( pTd->m_dwHalted ) {

					if( pTd->m_dwBabble ) {

						AfxTrace("CUsbHostEnhanced::OnTransfer - Babble (Addr=%d, Endpoint=%d)\n", pTr->m_pQh->m_dwDevAddr, pTr->m_pQh->m_dwEndpt);
						}
					
					Urb.m_uStatus = UsbIor::statHalted;
					}

				else if( pTd->m_dwError ) {

					AfxTrace("CUsbHostEnhanced::OnTransfer - Error (Addr=%d, Endpoint=%d)\n", pTr->m_pQh->m_dwDevAddr, pTr->m_pQh->m_dwEndpt);

					Urb.m_uStatus = UsbIor::statFailed;
					}
				else {
					Urb.m_uStatus = UsbIor::statPassed;

					Urb.m_uCount -= pTd->m_dwTotal;

					if( pTd->m_dwPID == pidIn && Urb.m_pData ) {

						m_pLowerDrv->MemCpy(PBYTE(Urb.m_pData), pTr->m_pData, Urb.m_uCount);
						}
					}

				m_pUpperDrv->OnTransfer(*pTr->m_pUrb);
				};

			FreeTransfer(pTr);
			}
		
		pTr = pNext;
		}
	}

void CUsbHostEnhanced::OnAsyncAdvance(void)
{
	m_pAdvance->Set();
	}

void CUsbHostEnhanced::OnError(void)
{
	AfxTrace("CUsbHostEnhanced::OnError\n");
	}

void CUsbHostEnhanced::OnSysError(void)
{
	AfxTrace("CUsbHostEnhanced::OnSysError\n");
	}

// Async Schedule

void CUsbHostEnhanced::InitAsync(void)
{
	m_pQhHead = NULL;

	MakeEndptCtrl(NOTHING, usbSpeedHigh);
	}

void CUsbHostEnhanced::AsyncEnable(BOOL fSet)
{
	if( (Reg(UsbCmd) & cmdAsyncEnable) == (Reg(UsbSts) & statAsync) ) { 

		if( fSet ) {

			Reg(UsbCmd) |= cmdAsyncEnable;

			while( !(Reg(UsbSts) & statAsync) );
			}
		else {
			Reg(UsbCmd) &= ~cmdAsyncEnable;

			while( (Reg(UsbSts) & statAsync) );
			}
		}
	}

void CUsbHostEnhanced::AsyncInsert(CQueueHead *pQh)
{
	DWORD dwReg = MemToReg(pQh);
	
	pQh->m_Link.m_dwTerm = false;

	pQh->m_Link.m_dwType = ptrQueueHead; 

	m_pLock->Wait(FOREVER);

	if( !m_pQhHead ) {

		m_pQhHead           = pQh;

		pQh->m_dwHeadFlag   = true;
		
		pQh->m_Link.m_dwPtr = PtrToLink(dwReg);

		Reg(AsyncList)      = dwReg;

		Reg(UsbCmd)        |= cmdAsyncEnable;
		}
	else {
		pQh->m_Link.m_dwPtr       = m_pQhHead->m_Link.m_dwPtr;

		m_pQhHead->m_Link.m_dwPtr = PtrToLink(dwReg);
		}

	m_pLock->Free();
	}

void CUsbHostEnhanced::AsyncRemove(CQueueHead *pQh)
{
	m_pLock->Wait(FOREVER);

	if( m_pQhHead ) {

		DWORD dwLink = PtrToLink(MemToReg(pQh));

		CQueueHead *pScan = m_pQhHead;

		do {
			if( pScan->m_Link.m_dwPtr == dwLink ) {

				if( pScan == pQh ) {

					AsyncEnable(false);

					m_pQhHead = NULL;
					}
				else {
					if( pQh->m_dwHeadFlag ) {

						pScan->m_dwHeadFlag = true;
						}
				
					pScan->m_Link.m_dwPtr = pQh->m_Link.m_dwPtr;

					AsyncWaitAdvance();
					}

				break;
				}

			pScan = (CQueueHead *) RegToMem(LinkToPtr(pScan->m_Link.m_dwPtr));
			
			} while( pScan != m_pQhHead );
		}

	m_pLock->Free();
	}

bool CUsbHostEnhanced::AsyncListed(CQueueHead *pQh)
{
	m_pLock->Wait(FOREVER);

	if( m_pQhHead ) {

		CQueueHead *pScan = m_pQhHead;

		do {
			if( pScan == pQh ) {

				m_pLock->Free();

				return true;
				}

			pScan = (CQueueHead *) RegToMem(LinkToPtr(pScan->m_Link.m_dwPtr));
			
			} while( pScan != m_pQhHead );
		}

	m_pLock->Free();

	return false;
	}

void CUsbHostEnhanced::AsyncWaitAdvance(void)
{
	m_pAdvance->Clear();

	Reg(UsbCmd) |= cmdDoorbell;

	if( !m_pAdvance->Wait(100) ) {

		// EMC Failure mode recovery mechanism

		AfxTrace("CUsbHostEnhanced::AsyncWaitAdvance - Timeout\n");

		Reg(UsbCmd) &= ~cmdRun;

		PortPower(false);

		Sleep(250);

		Reg(UsbCmd) |= cmdRun;

		PortPower(true);
		}
	}

// Perdiodic Schedule

void CUsbHostEnhanced::InitPeriodic(void)
{
	memset(m_pPfLink, 0, constPeriodicSize * sizeof(CLink));

	memset(m_pPfHead, 0, constPeriodicPoll * sizeof(CQueueHead)); 

	for( UINT i = 0; i < constPeriodicPoll; i ++ )  {

		m_pPfHead[i].m_Link.m_dwType = ptrQueueHead;

		m_pPfHead[i].m_dwFrameSMask  = 1;

		m_pPfHead[i].m_Link.m_dwTerm = true;
		}

	for( UINT i = 0; i < constPeriodicSize; i ++ )  {

		m_pPfLink[i].m_dwType = ptrQueueHead;

		m_pPfLink[i].m_dwPtr  = PtrToLink(MemToReg(&m_pPfHead[i % constPeriodicPoll]));
		}

	Reg(FrameList) = MemToReg(m_pPfLink);

	Reg(UsbCmd)   |= cmdPeriodicEnable;
	}

void CUsbHostEnhanced::MakePeriodic(void)
{
	UINT uSizeHead  = constPeriodicPoll * sizeof(CQueueHead);

	UINT uSizeLink  = constPeriodicSize * sizeof(CLink);

	UINT uAlignHead = m_pLowerDrv->GetMemAlign(usbMemQueueHeadDesc);

	UINT uAlignLink = m_pLowerDrv->GetMemAlign(usbMemPeriodicList);

	m_pPfHead = (CQueueHead *) AllocNonCached(uSizeHead, uAlignHead);

	m_pPfLink = (CLink *)      AllocNonCached(uSizeLink, uAlignLink);
	}

void CUsbHostEnhanced::KillPeriodic(void)
{
	if( m_pPfLink ) {

		FreeNonCached(m_pPfLink);

		m_pPfLink = NULL;
		}

	if( m_pPfHead ) {

		FreeNonCached(m_pPfHead);

		m_pPfHead = NULL;
		}
	}

void CUsbHostEnhanced::PeriodicInsert(CQueueHead *pQh, UINT uPoll)
{
	DWORD dwReg = MemToReg(pQh);
	
	pQh->m_Link.m_dwTerm = true;

	pQh->m_Link.m_dwType = ptrQueueHead; 

	m_pLock->Wait(FOREVER);

	CQueueHead *pScan = NULL;

	UINT uCount = NOTHING;

	UINT iScan  = 0;

	UINT iStep  = (constPeriodicPoll / 2) - 1;

	for( UINT i = 0; i < constPeriodicPoll; i ++ ) {

		CQueueHead *p = &m_pPfHead[iScan % constPeriodicPoll];

		UINT uLinked = 0;
		
		while( !p->m_Link.m_dwTerm ) {

			uLinked ++;

			p = (CQueueHead *) RegToMem(LinkToPtr(p->m_Link.m_dwPtr));
			}

		if( uLinked < uCount ) {

			uCount = uLinked;

			pScan  = p;
			}

		if( !uCount ) {

			break;
			}

		iScan += iStep;
		}

	pScan->m_Link.m_dwPtr  = PtrToLink(dwReg);

	pScan->m_Link.m_dwTerm = false;

	m_pLock->Free();
	}

void CUsbHostEnhanced::PeriodicRemove(CQueueHead *pQh)
{
	m_pLock->Wait(FOREVER);

	DWORD dwLink = PtrToLink(MemToReg(pQh));

	for( UINT i = 0; i < constPeriodicPoll; i ++ ) {

		CQueueHead *pScan = &m_pPfHead[i];

		while( !pScan->m_Link.m_dwTerm ) {

			if( pScan->m_Link.m_dwPtr == dwLink ) {

				pScan->m_Link.m_dwTerm = pQh->m_Link.m_dwTerm; 

				pScan->m_Link.m_dwPtr  = pQh->m_Link.m_dwPtr;

				m_pLock->Free();

				return;
				}

			pScan = (CQueueHead *) RegToMem(LinkToPtr(pScan->m_Link.m_dwPtr));
			}
		}

	m_pLock->Free();
	}

bool CUsbHostEnhanced::PeriodicListed(CQueueHead *pQh)
{
	DWORD dwLink = PtrToLink(MemToReg(pQh));

	for( UINT i = 0; i < constPeriodicPoll; i ++ ) {

		CQueueHead *pScan = &m_pPfHead[i];

		while( !pScan->m_Link.m_dwTerm ) {

			if( pScan->m_Link.m_dwPtr == dwLink ) {

				m_pLock->Free();

				return true;
				}

			pScan = (CQueueHead *) RegToMem(LinkToPtr(pScan->m_Link.m_dwPtr));
			}
		}

	m_pLock->Free();
	
	return false;
	}

void CUsbHostEnhanced::PeriodicEnable(BOOL fSet)
{
	if( (Reg(UsbCmd) & cmdPeriodicEnable) == (Reg(UsbSts) & statPeriodic) ) { 

		if( fSet ) {

			Reg(UsbCmd) |= cmdPeriodicEnable;

			while( !(Reg(UsbSts) & statPeriodic) );
			}
		else {
			Reg(UsbCmd) &= ~cmdPeriodicEnable;

			while( (Reg(UsbSts) & statPeriodic) );
			}
		}
	}

// Queue Head

void CUsbHostEnhanced::InitQueueHeads(void)
{
	memset(m_pQhList, 0, m_uQhSize * sizeof(CQueueHead));

	m_pQhHead = NULL;

	m_pQhFree = NULL;
	
	for( UINT i = 0; i < m_uQhSize; i ++ )  {

		m_pQhList[i].m_Link.m_dwType = ptrQueueHead;
		
		FreeQueueHead(&m_pQhList[i]);
		}
	}

void CUsbHostEnhanced::MakeQueueHeads(void)
{
	m_uQhSize   = constQhLimit;

	UINT uSize  = constQhLimit * sizeof(CQueueHead);

	UINT uAlign = m_pLowerDrv->GetMemAlign(usbMemQueueHeadDesc);

	m_pQhList   = (CQueueHead *) AllocNonCached(uSize, uAlign);
	}

void CUsbHostEnhanced::KillQueueHeads(void)
{
	if( m_pQhList ) {

		FreeNonCached(m_pQhList);

		m_pQhList = NULL;

		m_pQhFree = NULL;

		m_uQhSize = 0;
		}
	}

CUsbHostEnhanced::CQueueHead * CUsbHostEnhanced::AllocQueueHead(void)
{
	m_pQhLimit->Wait(FOREVER);

	m_pLock->Wait(FOREVER);

	if( m_pQhFree ) {

		CQueueHead *p = m_pQhFree;

		m_pQhFree = (CQueueHead *) LinkToPtr(p->m_Link.m_dwPtr);

		memset(p, 0, sizeof(CQueueHead));

		m_pLock->Free();

		return p;
		}

	AfxTrace("CUsbHostEnhanced - Queue Head Serialisation!\n");

	m_pLock->Free();
	
	return NULL;
	}

void CUsbHostEnhanced::FreeQueueHead(CQueueHead *pQueue)
{
	m_pLock->Wait(FOREVER);

	pQueue->m_Link.m_dwTerm = true;

	if( m_pQhFree ) {

		pQueue->m_Link.m_dwPtr = PtrToLink(m_pQhFree);

		m_pQhFree              = pQueue;
		}
	else {
		m_pQhFree              = pQueue;

		pQueue->m_Link.m_dwPtr = NULL;
		}

	m_pLock->Free();

	m_pQhLimit->Signal(1);
	}

// Transfer Descriptors

void CUsbHostEnhanced::InitTransfers(void)
{
	memset(m_pTrList, 0, sizeof(CTransfer)     * m_uTdSize);

	memset(m_pTdList, 0, sizeof(CTransferDesc) * m_uTdSize);

	m_pUsedHead = NULL;

	m_pUsedTail = NULL;

	m_iFreeHead = 0;

	m_iFreeTail = 0;

	for( UINT i = 0; i < m_uTdSize; i ++ ) {

		CTransfer     &Tr   = m_pTrList[i];

		CTransferDesc &Td   = m_pTdList[i];

		Tr.m_pTd            = &Td;
		
		Tr.m_dwTdPhy        = MemToReg(Tr.m_pTd);

		Tr.m_pData          = m_pTrData + (i * constTdData);
		
		Td.m_Next.m_dwTerm  = true;
		
		DWORD dwAddr = MemToReg(DWORD(Tr.m_pData));

		for( UINT n = 0; n < elements(Td.m_BufPtr); n ++ ) {

			Td.m_BufPtr[n] = dwAddr;

			dwAddr &= 0xFFFFF000;

			dwAddr += 0x00001000;
			}

		m_pFreeList[i] = &Tr;
		}
	}

void CUsbHostEnhanced::MakeTransfers(void)
{
	m_uTdSize   = constTdLimit;

	m_pTrList   = New CTransfer [ m_uTdSize ];

	m_pTrData   = PBYTE(AllocNonCached(m_uTdSize * constTdData, 4096)); 

	UINT uSize  = m_uTdSize * sizeof(CTransferDesc);

	UINT uAlign = m_pLowerDrv->GetMemAlign(usbMemTransferDesc);

	m_pTdList   = (CTransferDesc *) AllocNonCached(uSize, uAlign);
	}

void CUsbHostEnhanced::KillTransfers(void)
{
	if( m_pTdList ) {

		FreeNonCached(m_pTdList);

		FreeNonCached(m_pTrData);

		m_pTdList = NULL;

		m_pTrList = NULL;

		m_pTrData = NULL;

		m_uTdSize = 0;
		}
	}

CUsbHostEnhanced::CTransfer * CUsbHostEnhanced::AllocTransfer(CQueueHead *pQh)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	UINT uNext = (m_iFreeHead + 1) % elements(m_pFreeList);

	if( uNext != m_iFreeTail ) {

		CTransfer     *pTr = m_pFreeList[m_iFreeHead];
	
		CTransferDesc *pTd = pTr->m_pTd;

		memset(pTd, 0, sizeof(*pTd) - sizeof(pTd->m_BufPtr));
						
		pTd->m_dwErrorCount  = 3;
			
		pTd->m_Next.m_dwTerm = true;

		pTd->m_Alt.m_dwTerm  = true;

		pTd->m_dwActive      = true;

		pTd->m_BufPtr[0]    &= 0xFFFFF000;
			
		pTr->m_pQh           = pQh;
			
		m_iFreeHead          = uNext;

		AfxListAppend(m_pUsedHead, m_pUsedTail, pTr, m_pNext, m_pPrev); 

		Hal_LowerIrql(uSave);

		return pTr;
		}

	Hal_LowerIrql(uSave);

	AfxTrace("CUsbHostEnhanced - Transfer Serialisation!\n");

	return NULL;
	}

void CUsbHostEnhanced::StartTransfer(CQueueHead *pQh, CTransfer *pTr)
{
	CTransferDesc *pTd = &pQh->m_Transfer;

	while( !pTd->m_Next.m_dwTerm ) {

		pTd = (CTransferDesc *) RegToMem(LinkToPtr(pTd->m_Next.m_dwPtr));
		}

	pTr->m_pTd->m_Next.m_dwPtr  = 0;

	pTr->m_pTd->m_Next.m_dwTerm = true;

	pTd->m_Next.m_dwPtr         = PtrToLink(pTr->m_dwTdPhy);

	pTd->m_Next.m_dwTerm        = false;
	}

void CUsbHostEnhanced::FreeTransfer(CTransfer *pTr)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	AfxListRemove(m_pUsedHead, m_pUsedTail, pTr, m_pNext, m_pPrev);

	m_pFreeList[m_iFreeTail] = pTr;

	m_iFreeTail = (m_iFreeTail + 1) % elements(m_pFreeList);

	Hal_LowerIrql(uSave);
	}

void CUsbHostEnhanced::FreeTransfers(void)
{
	CTransfer *pScan = m_pUsedHead;

	while( pScan ) {

		CTransfer *pNext = pScan->m_pNext;

		FreeTransfer(pScan);
		
		pScan = pNext;
		}
	}

void CUsbHostEnhanced::FreeTransfers(CQueueHead *pQh)
{
	CTransfer *pScan = m_pUsedHead;

	while( pScan ) {

		CTransfer *pNext = pScan->m_pNext;

		if( pScan->m_pQh == pQh ) {
			
			FreeTransfer(pScan);
			}
		
		pScan = pNext;
		}
	}

void CUsbHostEnhanced::AbortTransfers(CQueueHead *pQh)
{
	CTransfer *pScan = m_pUsedHead;

	while( pScan ) {

		CTransfer *pNext = pScan->m_pNext;

		if( pScan->m_pQh == pQh ) {

			if( pScan->m_pTd->m_dwActive ) {

				if( pScan->m_pUrb ) {

					AfxTrace("CUsbHostEnhanced::AbortTransfer (Addr=%d, Endpoint=%d)\n", pScan->m_pQh->m_dwDevAddr, pScan->m_pQh->m_dwEndpt);

					pScan->m_pUrb->m_uStatus = UsbIor::statFailed;

					m_pUpperDrv->OnTransfer(*pScan->m_pUrb);
					}
				}

			FreeTransfer(pScan);
			}
		
		pScan = pNext;
		}
	}

// Port Helpers

void CUsbHostEnhanced::PortPower(BOOL fOn)
{
	if( Caps(HcsParams) & (1 << 4) ) {

		for( UINT n = 0; n < GetSParams().m_dwPorts; n ++ ) {
		
			PortSet(n, portPower, fOn);
			}
		}
	}

BOOL CUsbHostEnhanced::PortGet(UINT i, DWORD dwMask)
{
	return (Reg(PortSc + i) & dwMask) ? true : false;
	}

void CUsbHostEnhanced::PortSet(UINT i, DWORD dwMask, BOOL fSet)
{
	if( fSet ) {

		Reg(PortSc + i) |=  dwMask;
		}
	else {
		Reg(PortSc + i) &= ~dwMask;
		}
	}

void CUsbHostEnhanced::PortSet(UINT i, DWORD dwMask)
{
	Reg(PortSc + i) |=  dwMask;
	}

void CUsbHostEnhanced::PortClr(UINT i, DWORD dwMask)
{
	Reg(PortSc + i) &= ~dwMask;
	}

// Debug

void CUsbHostEnhanced::DumpHardware(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("\nHardware\n\n");	

	AfxTrace("USBCMD    : 0x%8.8X\n", Reg(UsbCmd));
	AfxTrace("USBSTS    : 0x%8.8X\n", Reg(UsbSts));
	AfxTrace("USBINTR   : 0x%8.8X\n", Reg(UsbIntr));
	AfxTrace("FRINDEX   : 0x%8.8X\n", Reg(FrIndex));
	AfxTrace("CTRLSEGT  : 0x%8.8X\n", Reg(CtrlSegt));
	AfxTrace("FRAMELIST : 0x%8.8X\n", Reg(FrameList));
	AfxTrace("ASYNCLIST : 0x%8.8X\n", Reg(AsyncList));
	AfxTrace("CFGFLAG   : 0x%8.8X\n", Reg(CfgFlag));
	AfxTrace("PORTSC    : 0x%8.8X\n", Reg(PortSc));

	#endif
	}

void CUsbHostEnhanced::DumpAsyncList(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("Asynchronous List\n");

	CQueueHead *p = m_pQhHead;
	
	while( p ) {

		DumpQueueHead(*p);
				
		p = (CQueueHead *) RegToMem(LinkToPtr(p->m_Link.m_dwPtr));

		if( p->m_dwHeadFlag ) break;
		};

	#endif
	}

void CUsbHostEnhanced::DumpHardwareCaps(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("\nHardware Capabilities\n\n");	

	AfxTrace("HCSPARAMS       @: 0x%8.8X\n", m_pCaps); 
	AfxTrace("HCSPARAMS        : 0x%8.8X\n", Caps(HcsParams)); 
	AfxTrace("Ports            : %d\n",      GetSParams().m_dwPorts);
	AfxTrace("m_dwPwrCtrl      : %d\n",      GetSParams().m_dwPwrCtrl);
	AfxTrace("Routing          : %d\n",      GetSParams().m_dwRouting);
	AfxTrace("NPPC             : %d\n",      GetSParams().m_dwNPPC);
	AfxTrace("NCC              : %d\n",      GetSParams().m_dwNCC);
	AfxTrace("PortLeds         : %d\n",      GetSParams().m_dwPortLeds);
	AfxTrace("DebugPort        : %d\n\n",    GetSParams().m_dwDebugPort);
	AfxTrace("HCCPARAMS        : 0x%8.8X\n", Caps(HccParams)); 
	AfxTrace("wWide            : %d\n",      GetCParams().m_dwWide);
	AfxTrace("VarFrame         : %d\n",      GetCParams().m_dwVarFrame);
	AfxTrace("AsyncPark        : %d\n",      GetCParams().m_dwAsyncPark);
	AfxTrace("IsoSched         : %d\n",      GetCParams().m_dwIsoSched);
	AfxTrace("ExtCaps          : %d\n\n",    GetCParams().m_dwExtCaps);

	#endif
	}

void CUsbHostEnhanced::DumpPeriodic(void)
{
	#if defined(_XDEBUG)	

	AfxTrace("DumpPeriodic : Period %ms\n", constPeriodicPoll);

	for( UINT i = 0; i < constPeriodicPoll; i ++ ) {

		AfxTrace("[%4.4d] : 0x%8.8X\n", i, &m_pPfHead[i]);

		if( true ) {

			DumpQueueHead(m_pPfHead[i]);
			}
		}

	#endif
	}

void CUsbHostEnhanced::DumpQueueList(void)
{
	#if defined(_XDEBUG)	

	AfxTrace("DumpQueueList (Size=%d)\n", sizeof(CQueueHead));

	AfxTrace("Size %d\n", m_uQhSize);

	for( UINT i = 0; i < m_uQhSize; i ++ ) {

		AfxTrace("[%4.4d] : 0x%8.8X\n", i, &m_pQhList[i]);

		if( true ) {

			DumpQueueHead(m_pQhList[i]);
			}
		}

	AfxTrace("\nFree List\n");

	for( CQueueHead *p = m_pQhFree; p; p = (CQueueHead *) LinkToPtr(p->m_Link.m_dwPtr) ) {

		AfxTrace("0x%8.8X\n", p);
		}

	#endif
	}

void CUsbHostEnhanced::DumpQueueHead(CQueueHead const &q) const
{
	#if defined(_XDEBUG)	

	AfxTrace("QueueHead {0x%8.8X}\n", &q);

	AfxTrace("m_Link.m_dwTerm  : %d\n",    q.m_Link.m_dwTerm);
	AfxTrace("m_Link.m_dwType  : %d\n",    q.m_Link.m_dwType);
	AfxTrace("m_Link.m_dwPtr   : %8.8X\n", q.m_Link.m_dwPtr);
	AfxTrace("m_Link.m_pPtr    : %8.8X\n", RegToMem(LinkToPtr(q.m_Link.m_dwPtr)));
	AfxTrace("m_dwDevAddr      : %d\n",    q.m_dwDevAddr);
	AfxTrace("m_dwInactiveReq  : %d\n",    q.m_dwInactiveReq);
	AfxTrace("m_dwEndpt        : %d\n",    q.m_dwEndpt);
	AfxTrace("m_dwSpeed        : %d\n",    q.m_dwSpeed);
	AfxTrace("m_dwToggleCtrl   : %d\n",    q.m_dwToggleCtrl);
	AfxTrace("m_dwHeadFlag     : %d\n",    q.m_dwHeadFlag);
	AfxTrace("m_dwMaxPacket    : %d\n",    q.m_dwMaxPacket);
	AfxTrace("m_dwControl      : %d\n",    q.m_dwControl);
	AfxTrace("m_dwNakReload    : %d\n",    q.m_dwNakReload);
	AfxTrace("m_dwFrameSMask   : %d\n",    q.m_dwFrameSMask);
	AfxTrace("m_dwFrameCMask   : %d\n",    q.m_dwFrameCMask);
	AfxTrace("m_dwHubAddr      : %d\n",    q.m_dwHubAddr);
	AfxTrace("m_dwPortNum      : %d\n",    q.m_dwPortNum);
	AfxTrace("m_dwPipeMult     : %d\n",    q.m_dwPipeMult);
	AfxTrace("m_CurrentTDPtr   : %8.8X\n", q.m_CurrentTDPtr);

	DumpTransferDesc(q.m_Transfer);

	AfxTrace("\n");

	#endif
	}

void CUsbHostEnhanced::DumpTransferDesc(CTransferDesc const &t) const
{
	#if defined(_XDEBUG)	

	AfxTrace("Transfer Descriptor {%8.8X}\n", &t);

	AfxTrace("m_Next.m_dwTerm  : %8.8X\n", t.m_Next.m_dwTerm);
	AfxTrace("m_Next.m_dwType  : %8.8X\n", t.m_Next.m_dwType);
	AfxTrace("m_Next.m_dwPtr   : %8.8X\n", t.m_Next.m_dwPtr);
	AfxTrace("m_Alt.m_dwTerm   : %8.8X\n", t.m_Alt.m_dwTerm);
	AfxTrace("m_Alt.m_dwType   : %8.8X\n", t.m_Alt.m_dwType);
	AfxTrace("m_Alt.m_dwPtr    : %8.8X\n", t.m_Alt.m_dwPtr);
	AfxTrace("m_dwPing         : %d\n",    t.m_dwPing);
	AfxTrace("m_dwSplit        : %d\n",    t.m_dwSplit);
	AfxTrace("m_dwMissed       : %d\n",    t.m_dwMissed);
	AfxTrace("m_dwError        : %d\n",    t.m_dwError);
	AfxTrace("m_dwBabble       : %d\n",    t.m_dwBabble);
	AfxTrace("m_dwBuffer       : %d\n",    t.m_dwBuffer);
	AfxTrace("m_dwHalted       : %d\n",    t.m_dwHalted);
	AfxTrace("m_dwActive       : %d\n",    t.m_dwActive);
	AfxTrace("m_dwPID          : %d\n",    t.m_dwPID);
	AfxTrace("m_dwErrorCount   : %d\n",    t.m_dwErrorCount);
	AfxTrace("m_dwCurrPage     : %d\n",    t.m_dwCurrPage);
	AfxTrace("m_dwIoc          : %d\n",    t.m_dwIoc);
	AfxTrace("m_dwTotal        : %d\n",    t.m_dwTotal);
	AfxTrace("m_dwToggle       : %d\n",    t.m_dwToggle);
	AfxTrace("m_BufPtr[0]      : %8.8X\n", t.m_BufPtr[0]);
	AfxTrace("m_BufPtr[1]      : %8.8X\n", t.m_BufPtr[1]);
	AfxTrace("m_BufPtr[2]      : %8.8X\n", t.m_BufPtr[2]);
	AfxTrace("m_BufPtr[3]      : %8.8X\n", t.m_BufPtr[3]);
	AfxTrace("m_BufPtr[4]      : %8.8X\n", t.m_BufPtr[4]);

	AfxTrace("\n");

	#endif
	}

// End of File
