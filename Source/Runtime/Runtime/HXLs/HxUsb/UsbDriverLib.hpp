
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDriverLib_HPP

#define	INCLUDE_UsbDriverLib_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Driver Library
//

class CUsbDriverLib
{
	public:
		// Classification
		bool GetClassOverride(UINT uVendor, UINT uProduct) const;
		bool GetClassFilter(UINT uVendor, UINT uProduct) const;

		// Creation
		IUsbHostFuncDriver * CreateDriver(UINT uClass, UINT uSubClass, UINT uProtocol) const;
		IUsbHostFuncDriver * CreateDriver(UINT uVendor, UINT uProduct, UINT uClass, UINT uSubClass, UINT uProtocol) const;
	};

// End of File

#endif
