
#include "Intern.hpp"

#include "CdcNetwork.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CDC Network Descriptor
//

// Constructor

CCdcNetwork::CCdcNetwork(void)
{
	}

// Endianess

void CCdcNetwork::HostToCdc(void)
{
	m_dwNetStats  = HostToIntel(m_dwNetStats);

	m_wMaxSegment = HostToIntel(m_wMaxSegment);

	m_wNumMulti   = HostToIntel(m_wNumMulti);
	}

void CCdcNetwork::CdcToHost(void)
{
	m_dwNetStats  = IntelToHost(m_dwNetStats);

	m_wMaxSegment = IntelToHost(m_wMaxSegment);

	m_wNumMulti   = IntelToHost(m_wNumMulti);
	}

// Operations

void CCdcNetwork::Init(void)
{
	memset(this, 0, sizeof(CdcNetDesc));
	}

// End of File
