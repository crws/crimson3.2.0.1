
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostScsiDriver_HPP

#define	INCLUDE_UsbHostScsiDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CScsiBulkCmd;
class CScsiBulkStatus;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Scsi Driver
//

class CUsbHostScsiDriver : public CUsbHostFuncDriver, public IUsbHostMassStorage
{
	public:
		// Constructor
		CUsbHostScsiDriver(void);

		// Destructor
		~CUsbHostScsiDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IMassStorageDriver
		BOOL METHOD TestReady(void);
		BOOL METHOD ReadInquiry(void);
		BOOL METHOD RequestSense(BYTE &bSk, BYTE &bAsc, BYTE &bAscq);
		BOOL METHOD ReadCapacity(DWORD &dwBlocks, DWORD &dwLength);
		BOOL METHOD Read(DWORD dwBlock, PBYTE pData);
		BOOL METHOD Write(DWORD dwBlock, PBYTE pData);
		BOOL METHOD Verify(DWORD dwBlock, UINT uCount);
		BOOL METHOD StartStop(bool fStart);
		BOOL METHOD LockMedia(bool fLock);
		BOOL METHOD SendDiag(void);

	protected:
		// Data
		IUsbPipe        * m_pCtrl;
		IUsbPipe        * m_pSend;
		IUsbPipe        * m_pRecv;
		UINT              m_iSend;
		UINT              m_iRecv;
		DWORD		  m_dwTag;
		CScsiBulkCmd    * m_pCbw;
		CScsiBulkStatus * m_pCsw;
		PBYTE		  m_pBuff;
		PBYTE             m_pData;

		// Implementation
		void ResetRecovery(void);
		bool Transact(void);
		bool SendCmd(void);
		bool RecvStatus(void);
		bool SendBulk(PBYTE pData, UINT  uCount);
		bool RecvBulk(PBYTE pData, UINT &uCount, UINT uTimeout);
		bool CheckSendStall(void);
		bool CheckRecvStall(void);
		bool CheckStall(IUsbPipe *pPipe);
		bool FindPipes(CUsbDescList const &List);
	};

// End of File

#endif
