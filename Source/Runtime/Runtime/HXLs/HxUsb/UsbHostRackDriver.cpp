
#include "Intern.hpp"

#include "UsbHostRackDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbIdentifiers.hpp"

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Rack Driver
//

IUsbHostBulkDriver * Create_RackDriver(void)
{
	IUsbHostBulkDriver *p = New CUsbHostRackDriver;

	return p;
	}

// Constructor

CUsbHostRackDriver::CUsbHostRackDriver(void)
{
	m_uPort  = 0;

	m_pName  = "Host Rack Driver";

	m_Debug  = debugErr | debugWarn;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostRackDriver::~CUsbHostRackDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");
	}

// IHostFuncDriver

void CUsbHostRackDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncBulkDriver::Bind(pDevice, iInterface);

	m_uPort = iInterface + 1;
	}

UINT CUsbHostRackDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostRackDriver::GetSubClass(void)
{
	return subclassRack;
	}

UINT CUsbHostRackDriver::GetProtocol(void)
{
	return devVendor;
	}

BOOL CUsbHostRackDriver::Open(CUsbDescList const &List)
{
	Trace(debugInfo, "Open");

	if( CUsbHostFuncBulkDriver::Open(List) ) {

		Init();

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CUsbHostRackDriver::Init(void)
{
	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recDevice;
		
	Req.m_bRequest  = 0x02;

	Req.m_wValue	= 0x0002;

	Req.m_wIndex    = 0x0000;

	Req.m_wLength   = 0x0000;
	
	Req.HostToUsb();

	m_pCtrl->CtrlTrans(Req);
	}

// End of File
