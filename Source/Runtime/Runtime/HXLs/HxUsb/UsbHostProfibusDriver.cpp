
#include "Intern.hpp"

#include "UsbHostProfibusDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Can Driver
//

// Instantiator

IUsbHostProfibus * Create_ProfibusDriver(void)
{
	IUsbHostProfibus *p = (IUsbHostProfibus *) New CUsbHostProfibusDriver;

	return p;
	}

// Constructor

CUsbHostProfibusDriver::CUsbHostProfibusDriver(void)
{
	m_pName  = "Host Profibus Driver";

	m_Debug  = debugWarn;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostProfibusDriver::~CUsbHostProfibusDriver(void)
{
	}

// IUnknown

HRESULT CUsbHostProfibusDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostProfibus);

	return CUsbHostModuleDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostProfibusDriver::AddRef(void)
{
	return CUsbHostModuleDriver::AddRef();
	}

ULONG CUsbHostProfibusDriver::Release(void)
{
	return CUsbHostModuleDriver::Release();
	}

// IHostFuncDriver

void CUsbHostProfibusDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostModuleDriver::Bind(pDevice, iInterface);
	}

void CUsbHostProfibusDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostModuleDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostProfibusDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostModuleDriver::GetDevice(pDev);
	}

BOOL CUsbHostProfibusDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostModuleDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostProfibusDriver::GetVendor(void)
{
	return CUsbHostModuleDriver::GetVendor();
	}

UINT CUsbHostProfibusDriver::GetProduct(void)
{
	return CUsbHostModuleDriver::GetProduct();
	}

UINT CUsbHostProfibusDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostProfibusDriver::GetSubClass(void)
{
	return subclassComms;
	}

UINT CUsbHostProfibusDriver::GetProtocol(void)
{
	return protoProfibus;
	}

UINT CUsbHostProfibusDriver::GetInterface(void)
{
	return CUsbHostModuleDriver::GetInterface();
	}

BOOL CUsbHostProfibusDriver::GetActive(void)
{
	return CUsbHostModuleDriver::GetActive();
	}

BOOL CUsbHostProfibusDriver::Open(CUsbDescList const &List)
{
	return CUsbHostModuleDriver::Open(List);
	}

BOOL CUsbHostProfibusDriver::Close(void)
{
	return CUsbHostModuleDriver::Close();
	}

void CUsbHostProfibusDriver::Poll(UINT uLapsed)
{
	CUsbHostModuleDriver::Poll(uLapsed);
	}

// IUsbHostModuleDriver

BOOL CUsbHostProfibusDriver::Reset(void)
{
	return CUsbHostModuleDriver::Reset(); 
	}

BOOL CUsbHostProfibusDriver::ReadVersion(BYTE bVersion[16])
{
	return CUsbHostModuleDriver::ReadVersion(bVersion);
	}

BOOL CUsbHostProfibusDriver::SendHeartbeat(void)
{
	return CUsbHostModuleDriver::SendHeartbeat();
	}

// IUsbHostProfibusDriver

BOOL CUsbHostProfibusDriver::IsOnline(void)
{
	Trace(debugCmds, "IsOnline");

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdOnline;

	Req.m_wLength   = 1;

	Req.HostToUsb();

	BYTE bOnline;

	return m_pCtrl->CtrlTrans(Req, &bOnline, sizeof(bOnline)) == sizeof(bOnline) ? bOnline : 0;
	}

BOOL CUsbHostProfibusDriver::SetRun(BOOL fRun)
{
	Trace(debugCmds, "SetRun(%d)", fRun);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetRun;

	Req.m_wValue    = fRun ? 1 : 0;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostProfibusDriver::SetStation(BYTE bStation)
{
	Trace(debugCmds, "SetStation(%d)", bStation);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetDrop;

	Req.m_wValue    = bStation;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

UINT CUsbHostProfibusDriver::GetInputDataSize(void)
{
	Trace(debugCmds, "GetInputDataSize");

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdGetPut;

	Req.m_wLength   = 2;

	Req.HostToUsb();

	WORD wSize;

	return m_pCtrl->CtrlTrans(Req, PBYTE(&wSize), sizeof(wSize)) == sizeof(wSize) ? IntelToHost(wSize) : 0;
	}

UINT CUsbHostProfibusDriver::GetOutputDataSize(void)
{
	Trace(debugCmds, "GetInputDataSize");

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdGetGet;

	Req.m_wLength   = 2;

	Req.HostToUsb();

	WORD wSize;

	return m_pCtrl->CtrlTrans(Req, PBYTE(&wSize), sizeof(wSize)) == sizeof(wSize) ? IntelToHost(wSize) : 0;
	}

BOOL CUsbHostProfibusDriver::PutData(PCBYTE pData, UINT uCount)
{
	Trace(debugCmds, "PutData(Count=%d)", uCount);

	return SendBulk(pData, uCount);
	}

UINT CUsbHostProfibusDriver::GetData(PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "GetData(Count=%d)", uCount);

	return RecvBulk(pData, uCount);
	}

BOOL CUsbHostProfibusDriver::PutDataAsync(PCBYTE pData, UINT uCount)
{
	Trace(debugCmds, "PutDataAsync(Count=%d)", uCount);

	return SendBulk(pData, uCount, true);
	}

UINT CUsbHostProfibusDriver::GetDataAsync(PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "GetDataAsync(Count=%d)", uCount);

	return RecvBulk(pData, uCount, true);
	}

UINT CUsbHostProfibusDriver::WaitAsyncPut(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncPut(Timeout=%d)", uTimeout);

	return WaitAsyncSend(uTimeout);
	}

UINT CUsbHostProfibusDriver::WaitAsyncGet(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncGet(Timeout=%d)", uTimeout);

	return WaitAsyncRecv(uTimeout);
	}

BOOL CUsbHostProfibusDriver::KillAsyncPut(void)
{
	Trace(debugCmds, "KillAsyncPut");

	return KillAsyncSend();
	}

BOOL CUsbHostProfibusDriver::KillAsyncGet(void)
{
	Trace(debugCmds, "KillAsyncGet");

	return KillAsyncRecv();
	}

// End of File
