
#include "Intern.hpp"

#include "UsbHostCanDriver.hpp"

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Can Driver
//

// Instantiator

IUsbHostCan * Create_CanDriver(void)
{
	IUsbHostCan *p = (IUsbHostCan *) New CUsbHostCanDriver;

	return p;
	}

// Constructor

CUsbHostCanDriver::CUsbHostCanDriver(void)
{
	m_pName  = "Host Can Driver";

	m_Debug  = debugWarn;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostCanDriver::~CUsbHostCanDriver(void)
{
	}

// IUnknown

HRESULT CUsbHostCanDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostCan);

	return CUsbHostModuleDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostCanDriver::AddRef(void)
{
	return CUsbHostModuleDriver::AddRef();
	}

ULONG CUsbHostCanDriver::Release(void)
{
	return CUsbHostModuleDriver::Release();
	}

// IHostFuncDriver

void CUsbHostCanDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostModuleDriver::Bind(pDevice, iInterface);
	}

void CUsbHostCanDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostModuleDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostCanDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostModuleDriver::GetDevice(pDev);
	}

BOOL CUsbHostCanDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostModuleDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostCanDriver::GetVendor(void)
{
	return CUsbHostModuleDriver::GetVendor();
	}

UINT CUsbHostCanDriver::GetProduct(void)
{
	return CUsbHostModuleDriver::GetProduct();
	}

UINT CUsbHostCanDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostCanDriver::GetSubClass(void)
{
	return subclassComms;
	}

UINT CUsbHostCanDriver::GetProtocol(void)
{
	return CUsbHostModuleDriver::GetProtocol();
	}

UINT CUsbHostCanDriver::GetInterface(void)
{
	return CUsbHostModuleDriver::GetInterface();
	}

BOOL CUsbHostCanDriver::GetActive(void)
{
	return CUsbHostModuleDriver::GetActive();
	}

BOOL CUsbHostCanDriver::Open(CUsbDescList const &List)
{
	return CUsbHostModuleDriver::Open(List);
	}

BOOL CUsbHostCanDriver::Close(void)
{
	return CUsbHostModuleDriver::Close();
	}

void CUsbHostCanDriver::Poll(UINT uLapsed)
{
	CUsbHostModuleDriver::Poll(uLapsed);
	}

// IUsbHostModuleDriver

BOOL CUsbHostCanDriver::Reset(void)
{
	return CUsbHostModuleDriver::Reset(); 
	}

BOOL CUsbHostCanDriver::ReadVersion(BYTE bVersion[16])
{
	return CUsbHostModuleDriver::ReadVersion(bVersion);
	}

BOOL CUsbHostCanDriver::SendHeartbeat(void)
{
	return CUsbHostModuleDriver::SendHeartbeat();
	}

// IUsbHostCanDriver

BOOL CUsbHostCanDriver::SetBaudRate(UINT uBaud)
{
	Trace(debugCmds, "SetBaudRate(%d)", uBaud);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_wIndex    = m_iInt;

	Req.m_bRequest  = cmdSetBaud;

	Req.m_wLength   = sizeof(DWORD);

	Req.HostToUsb();

	DWORD dwBaud = ::IntelToHost(DWORD(uBaud));
	
	return m_pCtrl->CtrlTrans(Req, PBYTE(&dwBaud), sizeof(dwBaud));
	}

BOOL CUsbHostCanDriver::SetFilter(UINT iFilter, DWORD Data, DWORD Mask)
{
	Trace(debugCmds, "SetFilter(Filter=%d, Data=0x%8.8X, Mask=0x%8.8X)", iFilter, Data, Mask);

	DWORD dwData[3];

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_wIndex    = m_iInt;

	Req.m_bRequest  = cmdSetFilter;

	Req.m_wLength   = sizeof(dwData);

	Req.HostToUsb();

	dwData[0] = ::IntelToHost(DWORD(iFilter));
	
	dwData[1] = ::IntelToHost(Data);

	dwData[2] = ::IntelToHost(Mask);

	return m_pCtrl->CtrlTrans(Req, PBYTE(dwData), sizeof(dwData));
	}

BOOL CUsbHostCanDriver::SetLed(UINT iLed, UINT uState)
{
	Trace(debugCmds, "SetLed(Led=%d, State=%d)", iLed, uState);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_wIndex    = m_iInt;

	Req.m_bRequest  = cmdSetLed;

	Req.m_wValue    = uState;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostCanDriver::SetRun(BOOL fRun)
{
	Trace(debugCmds, "SetRun(%d)", fRun);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_wIndex    = m_iInt;

	Req.m_bRequest  = cmdSetRun;

	Req.m_wValue    = fRun ? 1 : 0;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostCanDriver::SetLatency(UINT uLatency)
{
	Trace(debugCmds, "SetLatency(%d)", uLatency);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_wIndex    = m_iInt;

	Req.m_bRequest  = cmdSetLatency;

	Req.m_wValue    = uLatency;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostCanDriver::GetError(void)
{
	Trace(debugCmds, "GetError");

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
		
	Req.m_wIndex    = m_iInt;

	Req.m_bRequest  = cmdGetError;

	Req.m_wLength   = 1;

	Req.HostToUsb();

	BYTE bError;

	return m_pCtrl->CtrlTrans(Req, &bError, 1) ? bError : 0;
	}

BOOL CUsbHostCanDriver::SendFrame(UsbCanFrame *pFrame, UINT uCount)
{
	Trace(debugCmds, "SendFrame(Count=%d)", uCount);

	return CUsbHostModuleDriver::SendBulk(PCBYTE(pFrame), uCount * sizeof(UsbCanFrame));
	}

UINT CUsbHostCanDriver::RecvFrame(UsbCanFrame *pFrame, UINT uCount)
{
	Trace(debugCmds, "RecvFrame(Count=%d)", uCount);

	uCount = CUsbHostModuleDriver::RecvBulk(PBYTE(pFrame), uCount * sizeof(UsbCanFrame));

	return uCount < NOTHING ? uCount / sizeof(UsbCanFrame) : NOTHING;
	}

BOOL CUsbHostCanDriver::SendFrameAsync(UsbCanFrame *pFrame, UINT uCount)
{
	Trace(debugCmds, "SendFrameAsync(Count=%d)", uCount);

	return CUsbHostModuleDriver::SendBulk(PCBYTE(pFrame), uCount * sizeof(UsbCanFrame), true);
	}

UINT CUsbHostCanDriver::RecvFrameAsync(UsbCanFrame *pFrame, UINT uCount)
{
	Trace(debugCmds, "RecvFrameAsync(Count=%d)", uCount);

	return CUsbHostModuleDriver::RecvBulk(PBYTE(pFrame), uCount * sizeof(UsbCanFrame), true);
	}

UINT CUsbHostCanDriver::WaitAsyncSend(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncSend(Timeout=%d)", uTimeout);

	return CUsbHostModuleDriver::WaitAsyncSend(uTimeout);
	}

UINT CUsbHostCanDriver::WaitAsyncRecv(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncRecv(Timeout=%d)", uTimeout);

	UINT uCount = CUsbHostModuleDriver::WaitAsyncRecv(uTimeout);

	return uCount < NOTHING ? uCount / sizeof(UsbCanFrame) : NOTHING;
	}

BOOL CUsbHostCanDriver::KillAsyncSend(void)
{
	Trace(debugCmds, "KillAsyncSend");

	return CUsbHostModuleDriver::KillAsyncSend();
	}

BOOL CUsbHostCanDriver::KillAsyncRecv(void)
{
	Trace(debugCmds, "KillAsyncRecv");

	return CUsbHostModuleDriver::KillAsyncRecv();
	}

// End of File
