
#include "Intern.hpp"

#include "UsbConfigDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Configuration Descriptor Wrapper
//

// Constructor

CUsbConfigDesc::CUsbConfigDesc(void)
{
	Init();
	} 

// Endianess

void CUsbConfigDesc::HostToUsb(void)
{
	m_wTotal = HostToIntel(m_wTotal);
	}

void CUsbConfigDesc::UsbToHost(void)
{
	m_wTotal = IntelToHost(m_wTotal);
	}

// Attributes

BOOL CUsbConfigDesc::IsValid(void) const
{
	return m_bType == descConfig && m_bLength == sizeof(UsbConfigDesc);
	}

// Init

void CUsbConfigDesc::Init(void)
{
	memset(this, 0, sizeof(UsbConfigDesc));
	
	m_bLength = sizeof(UsbConfigDesc);
	
	m_bType	  = descConfig;

	m_wTotal  = sizeof(UsbConfigDesc);
	}

// Debug

void CUsbConfigDesc::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\nUsb Config Descriptor\n");

	AfxTrace("Type         = %d\n",      m_bType);
	AfxTrace("Len          = %d\n",      m_bLength);
	AfxTrace("Total        = 0x%4.4X\n", m_wTotal);
	AfxTrace("Interfaces   = 0x%2.2X\n", m_bInterfaces);
	AfxTrace("Config       = 0x%2.2X\n", m_bConfig);
	AfxTrace("Index        = 0x%2.2X\n", m_bIndex);
	AfxTrace("Wakeup       = 0x%2.2X\n", m_bWakeup);
	AfxTrace("Self Power   = 0x%2.2X\n", m_bSelfPowered);
	AfxTrace("Max Power    = 0x%2.2X\n", m_bMaxPower);

	#endif
	}

// End of File
