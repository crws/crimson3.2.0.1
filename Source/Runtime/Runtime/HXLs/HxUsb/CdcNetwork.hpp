
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CdcNetwork_HPP

#define	INCLUDE_CdcNetwork_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cdc Framework
//

#include "Cdc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cdc Network Desc
//

class CCdcNetwork : public CdcNetDesc
{
	public:
		// Constructor
		CCdcNetwork(void);

		// Endianess
		void HostToCdc(void);
		void CdcToHost(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
