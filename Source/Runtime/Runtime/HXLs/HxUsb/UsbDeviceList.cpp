
#include "Intern.hpp"  

#include "UsbDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Device List
//

// Constructor

CUsbDeviceList::CUsbDeviceList(void)
{
	m_pHead = NULL;

	m_pTail = NULL;
	}

// Attributes

CUsbDevice * CUsbDeviceList::GetHead(void) const
{
	return m_pHead;
	}
		
CUsbDevice * CUsbDeviceList::GetTail(void) const
{
	return m_pTail;
	}

CUsbDevice * CUsbDeviceList::GetPrev(CUsbDevice *pDevice) const
{
	return pDevice ? pDevice->m_pPrev : NULL;
	}

CUsbDevice * CUsbDeviceList::GetNext(CUsbDevice *pDevice) const
{
	return pDevice ? pDevice->m_pNext : NULL;
	}
		
// Operations

void CUsbDeviceList::Append(CUsbDevice *pDevice)
{
	AfxListAppend(m_pHead, m_pTail, pDevice, m_pNext, m_pPrev);
	}

void CUsbDeviceList::Remove(CUsbDevice *pDevice)
{
	AfxListRemove(m_pHead, m_pTail, pDevice, m_pNext, m_pPrev);
	};

void CUsbDeviceList::Insert(CUsbDevice *pDevice, CUsbDevice *pBefore)
{
	AfxListInsert(m_pHead, m_pTail, pDevice, m_pNext, m_pPrev, pBefore);
	}

// End of File
