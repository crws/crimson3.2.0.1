
#include "Intern.hpp"

#include "UsbClearFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Clear Feature Standard Device Request
//

// Constructor

CUsbClearFeatureReq::CUsbClearFeatureReq(void)
{
	Init();
	} 

// Operations

void CUsbClearFeatureReq::Init(void)
{
	CUsbStandardReq::Init();

	m_bRequest  = reqClearFeature;

	m_Direction = dirHostToDev;
	}

void CUsbClearFeatureReq::SetDevice(WORD wFeature)
{
	m_Recipient = recDevice;

	m_wValue    = wFeature;
	}

void CUsbClearFeatureReq::SetInterface(WORD wFeature, WORD wInterface)
{
	m_Recipient = recInterface;

	m_wValue    = wFeature;

	m_wIndex    = wInterface;
	}

void CUsbClearFeatureReq::SetEndpoint(WORD wFeature, WORD wEndpoint)
{
	m_Recipient = recEndpoint;

	m_wValue    = wFeature;

	m_wIndex    = wEndpoint;
	}

// End of File
