
#include "Intern.hpp"

#include "UsbHubSetFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Set Feature Request
//

// Constructor

CUsbHubSetFeatureReq::CUsbHubSetFeatureReq(WORD wFeature)
{
	Init(wFeature);
	} 

// Operations

void CUsbHubSetFeatureReq::Init(WORD wFeature)
{
	CUsbClassReq::Init();

	m_bRequest  = reqSetFeature;

	m_Direction = dirHostToDev;

	m_Recipient = recHub;

	m_wValue    = wFeature;
	}

// End of File
