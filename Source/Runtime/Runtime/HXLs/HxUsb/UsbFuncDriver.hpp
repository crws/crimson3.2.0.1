
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbFuncDriver_HPP

#define	INCLUDE_UsbFuncDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbDeviceDesc.hpp"

#include "UsbDeviceQual.hpp"

#include "UsbConfigDesc.hpp"

#include "UsbInterfaceDesc.hpp"

#include "UsbEndpointDesc.hpp"

#include "UsbStringDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Driver
//

class CUsbFuncDriver : public IUsbFuncDriver, public IUsbFuncHardwareEvents
{
	public:
		// Constructor
		CUsbFuncDriver(UINT uPriority);

		// Destructor
		~CUsbFuncDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pDriver);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);
		
		// IUsbFuncDriver
		UINT METHOD GetState(void);
		UINT METHOD GetMaxPacket(UINT iEndpt);
		BOOL METHOD SendCtrlStatus(UINT iEndpt);
		BOOL METHOD RecvCtrlStatus(UINT iEndpt);
		BOOL METHOD SendCtrl(UINT iEndpt, PCBYTE pData, UINT uLen);
		BOOL METHOD RecvCtrl(UINT iEndpt, PBYTE  pData, UINT uLen);
		UINT METHOD SendBulk(UINT iEndpt, PCBYTE pData, UINT uLen, UINT uTimeout);
		UINT METHOD RecvBulk(UINT iEndpt, PBYTE  pData, UINT uLen, UINT uTimeout);
		BOOL METHOD SetStall(UINT iEndpt, BOOL fStall);
		BOOL METHOD GetStall(UINT iEndpt);
		
		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);
		
		// IUsbFuncHardwareEvents
		void METHOD OnConnect(void);
		void METHOD OnDisconnect(void);
		void METHOD OnReset(void);
		void METHOD OnSuspend(void);
		void METHOD OnResume(void);
		BOOL METHOD OnSetup(PCBYTE pData);
		void METHOD OnTransfer(UsbIor &Urb);

	protected:
		// Data
		ULONG                    m_uRefs;
		IUsbFuncHardwareDriver * m_pLowerDrv;
		IUsbFuncEvents         * m_pUpperDrv;
		CUsbDeviceReq            m_Req;
		UINT volatile	         m_uStateNow;
		UINT volatile	         m_uStateWas;
		UsbIor                   m_Urbs[16];
		CUsbDeviceDesc		 m_Device;
		CUsbDeviceQual		 m_Qualifier;
		CUsbConfigDesc		 m_Config;
		CUsbInterfaceDesc        m_Interfaces[8];
		CUsbEndpointDesc         m_Endpoints[16];
		UINT                     m_uEndpointCount;
		PBYTE                    m_pConfigData;
		IThread		       * m_pThread;
		IEvent		       * m_pFlag;
		ISemaphore	       * m_pLimit;
		IMutex		       * m_pLock;
		
		// Task Entry
		void Service(void);

		// Transfer
		UINT Transfer(UINT iEndpt, PBYTE pData, UINT uLen, BOOL fIn, UINT uTimeout);
				
		// Framework Event Handlers
		BOOL OnStandard(void);
		BOOL OnClass(void);
		BOOL OnVendor(void);
		BOOL OnOther(void);
		BOOL OnGetStatus(void);
		BOOL OnClearFeature(void);
		BOOL OnSetAddress(void);
		BOOL OnGetDescriptor(void);
		BOOL OnGetConfig(void);
		BOOL OnSetConfig(void);
		BOOL OnGetDeviceDesc(void);
		BOOL OnGetDeviceQual(void);
		BOOL OnGetConfigDesc(UINT iIndex);
		BOOL OnGetStringDesc(UINT iIndex);

		// Descriptors
		BOOL BuildDescriptors(void);
		BOOL BuildDeviceDescriptor(void);
		BOOL BuildQualifierDescriptor(void);
		BOOL BuildConfigDescriptor(void);
		BOOL BuildInterfaceDescriptors(void);
		BOOL BuildEndpointDescriptors(void);
		void CheckDescriptors(void);
		void MakeDescriptorData(void);
		void FreeDescriptorData(void);
		
		// IO Request Blocks
		void MakeUrb(void);
		void InitUrb(void);
		UINT AllocUrb(void);
		void FreeUrb(UINT i);
		void KillUrb(void);

		// State 
		void  SetState(UINT uState);
		PCTXT GetStateText(UINT uState) const;

		// Debug
		void DumpUrbs(void) const;
		void DumpUrb(UINT i) const;

		// Entry Point
		static int TaskUsbFuncDriver(IThread *pThread, void *pParam, UINT uParam); 
	};

// End of File

#endif
