
#include "Intern.hpp"

#include "ScsiSense.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Sense Data Object
//

// Constructor

CScsiSense::CScsiSense(void)
{
	}

// Endianess

void CScsiSense::HostToScsi(void)
{
	m_dwInfo    = HostToMotor(m_dwInfo);

	m_dwCmdInfo = HostToMotor(m_dwCmdInfo);

	m_wSksData  = HostToMotor(m_wSksData);
	}

void CScsiSense::ScsiToHost(void)
{
	m_dwInfo    = MotorToHost(m_dwInfo);

	m_dwCmdInfo = MotorToHost(m_dwCmdInfo);

	m_wSksData  = MotorToHost(m_wSksData);
	}

// Attributes

BOOL CScsiSense::IsValid(void) const
{
	return m_bSenseKey != senseNone;
	}

// Operations

void CScsiSense::Init(void)
{
	memset(this, 0, sizeof(ScsiSense));

	m_bRespCode  = 0x70;

	m_bAddLength = 0x0A;
	}

void CScsiSense::Clear(void)
{
	Init();
	}

void CScsiSense::Set(BYTE bSense)
{
	m_bSenseKey = bSense;

	m_bAsc      = 0;

	m_bAscq     = 0;
	}

void CScsiSense::Set(BYTE bSense, BYTE bAsc)
{
	m_bSenseKey = bSense;

	m_bAsc      = bAsc;

	m_bAscq     = 0;
	}

void CScsiSense::Set(BYTE bSense, BYTE bAsc, BYTE bAscq)
{
	m_bSenseKey = bSense;

	m_bAsc      = bAsc;

	m_bAscq     = bAscq;
	}

void CScsiSense::SetInfo(DWORD dwInfo)
{
	m_bValid = 1;

	m_dwInfo = dwInfo;
	}

// Debug

void CScsiSense::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("CScsiSense\n");

	AfxTrace("Key  : 0x%2.2X\n", m_bSenseKey);

	AfxTrace("Asc  : 0x%2.2X\n", m_bAsc);

	AfxTrace("Ascq : 0x%2.2X\n", m_bSenseKey);

	#endif
	}

// End of File
