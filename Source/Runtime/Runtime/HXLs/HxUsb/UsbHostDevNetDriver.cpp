
#include "Intern.hpp"

#include "UsbHostDevNetDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host DeviceNet Driver
//

// Instantiator

IUsbHostDevNet * Create_DevNetDriver(void)
{
	IUsbHostDevNet *p = (IUsbHostDevNet *) New CUsbHostDevNetDriver;

	return p;
	}

// Constructor

CUsbHostDevNetDriver::CUsbHostDevNetDriver(void)
{
	m_pName  = "Host DevNet Driver";

	m_Debug  = debugWarn | debugErr;

	m_uSeq   = 0;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostDevNetDriver::~CUsbHostDevNetDriver(void)
{
	}

// IUnknown

HRESULT CUsbHostDevNetDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostDevNet);

	return CUsbHostModuleDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostDevNetDriver::AddRef(void)
{
	return CUsbHostModuleDriver::AddRef();
	}

ULONG CUsbHostDevNetDriver::Release(void)
{
	return CUsbHostModuleDriver::Release();
	}

// IHostFuncDriver

void CUsbHostDevNetDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostModuleDriver::Bind(pDevice, iInterface);
	}

void CUsbHostDevNetDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostModuleDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostDevNetDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostModuleDriver::GetDevice(pDev);
	}

BOOL CUsbHostDevNetDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostModuleDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostDevNetDriver::GetVendor(void)
{
	return CUsbHostModuleDriver::GetVendor();
	}

UINT CUsbHostDevNetDriver::GetProduct(void)
{
	return CUsbHostModuleDriver::GetProduct();
	}

UINT CUsbHostDevNetDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostDevNetDriver::GetSubClass(void)
{
	return subclassComms;
	}

UINT CUsbHostDevNetDriver::GetProtocol(void)
{
	return protoDevNet;
	}

UINT CUsbHostDevNetDriver::GetInterface(void)
{
	return CUsbHostModuleDriver::GetInterface();
	}

BOOL CUsbHostDevNetDriver::GetActive(void)
{
	return CUsbHostModuleDriver::GetActive();
	}

BOOL CUsbHostDevNetDriver::Open(CUsbDescList const &List)
{
	return CUsbHostModuleDriver::Open(List);
	}

BOOL CUsbHostDevNetDriver::Close(void)
{
	return CUsbHostModuleDriver::Close();
	}

void CUsbHostDevNetDriver::Poll(UINT uLapsed)
{
	CUsbHostModuleDriver::Poll(uLapsed);
	}

// IUsbHostModuleDriver

BOOL CUsbHostDevNetDriver::Reset(void)
{
	return CUsbHostModuleDriver::Reset(); 
	}

BOOL CUsbHostDevNetDriver::ReadVersion(BYTE bVersion[16])
{
	return CUsbHostModuleDriver::ReadVersion(bVersion);
	}

BOOL CUsbHostDevNetDriver::SendHeartbeat(void)
{
	return CUsbHostModuleDriver::SendHeartbeat();
	}

// IUsbHostDevNetDriver

BOOL CUsbHostDevNetDriver::SetRun(BOOL fRun)
{
	Trace(debugCmds, "SetRun(%d)", fRun);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetRun;

	Req.m_wValue    = fRun ? 1 : 0;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostDevNetDriver::SetBaudRate(UINT uBaud)
{
	Trace(debugCmds, "SetBaudRate(%d)", uBaud);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetBaud;

	Req.m_wIndex    = LOWORD(uBaud);

	Req.m_wValue    = HIWORD(uBaud);

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostDevNetDriver::SetDrop(UINT uDrop)
{
	Trace(debugCmds, "SetDrop(%d)", uDrop);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetDrop;

	Req.m_wValue    = uDrop;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostDevNetDriver::EnableBitStrobe(WORD wRecvSize, WORD wSendSize)
{
	Trace(debugCmds, "EnableBitStrobe(%d, %d)", wRecvSize, wSendSize);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetBitStrobe;

	Req.m_wIndex    = wRecvSize;

	Req.m_wValue    = wSendSize;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostDevNetDriver::EnablePolled(WORD wRecvSize, WORD wSendSize)
{
	Trace(debugCmds, "EnablePolled(%d, %d)", wRecvSize, wSendSize);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetPolled;

	Req.m_wIndex    = wRecvSize;

	Req.m_wValue    = wSendSize;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostDevNetDriver::EnableData(WORD wRecvSize, WORD wSendSize)
{
	Trace(debugCmds, "EnableData(%d, %d)", wRecvSize, wSendSize);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetData;

	Req.m_wIndex    = wRecvSize;

	Req.m_wValue    = wSendSize;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostDevNetDriver::SendAsync(PCBYTE pData, UINT uCount)
{
	return SendBulk(pData, uCount, true);
	}

UINT CUsbHostDevNetDriver::RecvAsync(PBYTE pData, UINT uCount)
{
	return RecvBulk(pData, uCount, true);
	}

UINT CUsbHostDevNetDriver::WaitSend(UINT uTimeout)
{
	return WaitAsyncSend(uTimeout);
	}

UINT CUsbHostDevNetDriver::WaitRecv(UINT uTimeout)
{
	return WaitAsyncRecv(uTimeout);
	}

BOOL CUsbHostDevNetDriver::KillSend(void)
{
	return KillAsyncSend();
	}

BOOL CUsbHostDevNetDriver::KillRecv(void)
{
	return KillAsyncRecv();
	}

// End of File
