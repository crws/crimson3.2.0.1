
#include "Intern.hpp"

#include "UsbHostControllerDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Controller Driver
//

// Constructor

CUsbHostControllerDriver::CUsbHostControllerDriver(void)
{
	m_pUpperDrv = NULL;

	m_iIndex    = NOTHING;
	}

// Destructor

CUsbHostControllerDriver::~CUsbHostControllerDriver(void)
{
	AfxRelease(m_pUpperDrv);
	}

// IUnknown

HRESULT CUsbHostControllerDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHostControllerDriver);

	StdQueryInterface(IUsbHostControllerDriver);

	StdQueryInterface(IUsbDriver);

	StdQueryInterface(IUsbHostInterfaceEvents);

	StdQueryInterface(IUsbEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbHostControllerDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHostControllerDriver::Release(void)
{
	StdRelease();
	}

// IUsbDriver

BOOL CUsbHostControllerDriver::Bind(IUsbEvents *pDriver)
{
	if( pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostControllerEvents), (void **) &m_pUpperDrv);

		if( m_pUpperDrv ) {

			m_pUpperDrv->OnBind((IUsbHostControllerDriver *) this);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUsbHostControllerDriver::Bind(IUsbDriver *pDriver)
{
	if( pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostControllerEvents), (void **) &m_pUpperDrv);

		if( m_pUpperDrv ) {

			m_pUpperDrv->OnBind((IUsbHostControllerDriver *) this);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUsbHostControllerDriver::Init(void)
{
	return TRUE;
	}

BOOL CUsbHostControllerDriver::Start(void)
{
	return TRUE;
	}

BOOL CUsbHostControllerDriver::Stop(void)
{
	return TRUE;
	}
		
// IUsbHostControllerDriver

BOOL CUsbHostControllerDriver::Bind(IUsbHostStack *pStack)
{
	if( pStack ) {

		pStack->QueryInterface(AfxAeonIID(IUsbHostControllerEvents), (void **) &m_pUpperDrv);

		if( m_pUpperDrv ) {

			m_pUpperDrv->OnBind((IUsbHostControllerDriver *) this);

			return TRUE;
			}
		}

	return FALSE;
	}

void CUsbHostControllerDriver::SetIndex(UINT iIndex)
{
	m_iIndex = iIndex;
	}

UINT CUsbHostControllerDriver::GetIndex(void)
{
	return m_iIndex;
	}

void CUsbHostControllerDriver::Poll(UINT uLapsed)
{
	}

void CUsbHostControllerDriver::EnableEvents(BOOL fEnable)
{
	}

BOOL CUsbHostControllerDriver::ResetPort(UINT uPort)
{
	return FALSE;
	}

// Host Interface Drivers

IUsbHostInterfaceDriver * CUsbHostControllerDriver::GetInterface(UINT iHost)
{
	return NULL;
	}

IUsbHostEnhancedDriver * CUsbHostControllerDriver::GetEnhanced(void)
{
	return NULL;
	}

IUsbHostInterfaceDriver * CUsbHostControllerDriver::GetCompanion(UINT i)
{
	return NULL;
	}

// IUsbEvents

void CUsbHostControllerDriver::OnBind(IUsbDriver *pDriver)
{
	}

void CUsbHostControllerDriver::OnInit(void)
{
	if( m_pUpperDrv ) {

		m_pUpperDrv->OnInit();
		}
	}

void CUsbHostControllerDriver::OnStart(void)
{
	if( m_pUpperDrv ) {

		m_pUpperDrv->OnStart();
		}
	}

void CUsbHostControllerDriver::OnStop(void)
{
	if( m_pUpperDrv ) {

		m_pUpperDrv->OnStop();
		}
	}
		
// IUsbHostInterfaceEvents

void CUsbHostControllerDriver::OnPortConnect(UsbPortPath &Path)
{
	}

void CUsbHostControllerDriver::OnPortRemoval(UsbPortPath &Path)
{
	}

void CUsbHostControllerDriver::OnPortCurrent(UsbPortPath &Path)
{
	}

void CUsbHostControllerDriver::OnPortEnable(UsbPortPath &Path)
{
	}

void CUsbHostControllerDriver::OnPortReset(UsbPortPath &Route)
{
	}

void CUsbHostControllerDriver::OnTransfer(UsbIor &Urb)
{
	m_pUpperDrv->OnTransferDone(Urb);
	}

// Device

void CUsbHostControllerDriver::OnDeviceArrival(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostControllerDriver::OnDeviceArrival(iPort=%d)\n", Path.a.dwPort);*/

	Path.a.dwCtrl = m_iIndex;

	m_pUpperDrv->OnDeviceArrival(Path);
	}

void CUsbHostControllerDriver::OnDeviceRemoval(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostControllerDriver::OnDeviceRemoval(iPort=%d)\n", Path.a.dwPort);*/
	
	Path.a.dwCtrl = m_iIndex;

	m_pUpperDrv->OnDeviceRemoval(Path);
	}

BOOL CUsbHostControllerDriver::CheckTimer(UINT &uTimer, UINT uLapsed)
{
	if( uTimer ) {	

		if( uTimer > uLapsed ) {

			uTimer -= uLapsed;

			return true;
			}

		uTimer = 0;

		return false;
		}
	
	return true;
	}

// End of File
