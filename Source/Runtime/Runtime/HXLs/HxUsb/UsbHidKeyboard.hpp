
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHidKeyboard_HPP

#define	INCLUDE_UsbHidKeyboard_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hid Framework
//

#include "Hid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHidMapper.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hid Keyboard Usage Table US
//

static BYTE const UsageUS1[] = 
{
	0x00, 0x00, 0x00, 0x00,  'a',  'b',  'c',  'd',
	 'e',  'f',  'g',  'h',  'i',  'j',  'k',  'l',
	 'm',  'n',  'o',  'p',  'q',  'r',  's',  't',
	 'u',  'v',  'w',  'x',  'y',  'z',  '1',  '2',
	 '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',
	0x0D, 0x1B, 0x7F, 0x09,  ' ',  '-',  '=',  '[',
	 ']', '\\', 0x00,  ';',  '\'',  '`',  ',',  '.',
	 '/', 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
	0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09,
	0x08, 0x0A, 0x0B, 0x00,  '/',  '*',  '-',  '+',
	0x0D,  '1',  '2',  '3',  '4',  '5',  '6',  '7',
	 '8',  '9',  '0',  '.',	0x00, 0xA2, 0x00, 0x00	
	};

static BYTE const UsageUS2[] = 
{
	0x00, 0x00, 0x00, 0x00,  'A',  'B',  'C',  'D',
	 'E',  'F',  'G',  'H',  'I',  'J',  'K',  'L',
	 'M',  'N',  'O',  'P',  'Q',  'R',  'S',  'T',
	 'U',  'V',  'W',  'X',  'Y',  'Z',  '!',  '@',
	 '#',  '$',  '%',  '^',  '&',  '*',  '(',  ')',
	0x0D, 0x1B, 0x7F, 0x08,  ' ',  '_',  '+',  '{',
	 '}',  '|', 0x00,  ':',  '"',  '~',  '<',  '>',
	 '?', 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
	0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09,
	0x08, 0x0A, 0x0B, 0x00,  '/',  '*',  '-',  '+',
	0x0D,  '1',  '2',  '3',  '4',  '5',  '6',  '7',
	'8',   '9',  '0',  '.', 0x00, 0xA2, 0x00, 0x00
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Keyboard Usage Table UK 
//

static BYTE const UsageUK1[] = 
{
	0x00, 0x00, 0x00, 0x00,  'a',  'b',  'c',  'd',
	 'e',  'f',  'g',  'h',  'i',  'j',  'k',  'l',
	 'm',  'n',  'o',  'p',  'q',  'r',  's',  't',
	 'u',  'v',  'w',  'x',  'y',  'z',  '1',  '2',
	 '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',
	0x0D, 0x1B, 0x7F, 0x09,  ' ',  '-',  '=',  '[',
	 ']', '\\',  '#',  ';', '\'',  '`',  ',',  '.',
	 '/', 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
	0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09,
	0x08, 0x0A, 0x0B, 0x00,  '/',  '*',  '-',  '+',
	0x0D,  '1',  '2',  '3',  '4',  '5',  '6',  '7',
	 '8',  '9',  '0',  '.', 0x00, 0xA2, 0x00, 0x00
	};

static BYTE const UsageUK2[] = 
{
	0x00, 0x00, 0x00, 0x00,  'A',  'B',  'C',  'D',
	 'E',  'F',  'G',  'H',  'I',  'J',  'K',  'L',
	 'M',  'N',  'O',  'P',  'Q',  'R',  'S',  'T',
	 'U',  'V',  'W',  'X',  'Y',  'Z',  '!',  '"',
	 '�',  '$',  '%',  '^',  '&',  '*',  '(',  ')',
	0x0D, 0x1B, 0x7F, 0x08,  ' ',  '_',  '+',  '{',
	 '}',  '|',  '~',  ':',  '@',  '~',  '<',  '>',
	 '?', 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
	0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09,
	0x08, 0x0A, 0x0B, 0x00,  '/',  '*',  '-',  '+',
	0x0D,  '1',  '2',  '3',  '4',  '5',  '6',  '7',
	 '8',  '9',  '0',  '.', 0x00, 0xA2, 0x00, 0x00
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Keyboard Mapper
//

class CUsbHidKeyboard : public CUsbHidMapper
{
	public:
		// Constructor
		CUsbHidKeyboard(void);

		// IUsbHidMapper
		BOOL METHOD SetReport(PCBYTE pReport, UINT uSize);
		UINT METHOD GetUsagePage(void);
		UINT METHOD GetUsageType(void);
		void METHOD SetConfig(PCBYTE pConfig, UINT uSize);
		void METHOD Poll(void);

	protected:
		//  Contants
		enum 
		{
			constDelay = 500,
			constRate  = 10,
			};

		// Modifiers
		enum 
		{
			keyCtrl	   = 0x11,
			keyShift   = 0x22,
			keyAlt	   = 0x44,
			};

		// Leds
		enum 
		{
			ledNum	   = 0x01,
			ledCaps	   = 0x02,
			ledScroll  = 0x04,
			};

		// Key State 
		struct CKeyState
		{
			BYTE	m_bCode;
			WORD	m_wScan;
			UINT	m_uRepTime;
			};

		// Data
		IInputQueue * m_pInput;
		CKeyState   * m_pKeyState;
		BYTE          m_bModifier;
		UINT          m_uKeyArray;
		WORD          m_wScan;
		bool          m_fCaps;
		bool	      m_fEnable;
		PCBYTE        m_pMap[2];
		
		// Helpers
		void Configure(void);
		void UpdateLEDs(void);
		bool ParseReport(void);
		void CheckRepeat(void);
		void FormatReport(void);
		
		// Buffers
		void InitBuffers(void);
		void FreeBuffers(void);

		// Implmentation
		UINT FindKeyState(BYTE bKey);
		UINT SaveKeyState(BYTE bKey);
		void FreeKeyState(UINT iKey);
		void ProcessKey(UINT iKey, bool fDown);
		BYTE LookupKey(BYTE bKey, BYTE bMask);
		
		// Destructor
		~CUsbHidKeyboard(void);
	};

// End of File

#endif
