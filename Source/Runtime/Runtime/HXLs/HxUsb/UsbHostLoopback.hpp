
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostSmcNicDriver_HPP

#define	INCLUDE_UsbHostSmcNicDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncBulkDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Loopback Driver
//

class CUsbHostLoopback : public CUsbHostFuncBulkDriver
{
	public:
		// Constructor
		CUsbHostLoopback(void);

		// Destructor
		~CUsbHostLoopback(void);

		// IHostFuncDriver
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
	};

// End of File

#endif
