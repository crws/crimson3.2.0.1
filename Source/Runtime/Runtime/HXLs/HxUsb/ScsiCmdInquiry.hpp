
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiCmdInquiry_HPP

#define	INCLUDE_ScsiCmdInquiry_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Inquiry
//

class CScsiCmdInquiry : public ScsiCmdInquiry
{
	public:
		// Constructor
		CScsiCmdInquiry(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Operations
		void Init(void);
		void Init(BYTE bAllocLen);
		void InitVital(BYTE bAllocLen, BYTE bPageOpcode);
		void InitSupport(BYTE bAllocLen, BYTE bPageOpcode);
	};

// End of File

#endif
