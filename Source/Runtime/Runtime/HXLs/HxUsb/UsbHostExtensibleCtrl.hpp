
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostExtensibleCtrl_HPP

#define	INCLUDE_UsbHostExtensibleCtrl_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostControllerDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Extensible Controller Driver
//

class CUsbHostExtensibleCtrl : public CUsbHostControllerDriver
{
	public:
		// Constructor
		CUsbHostExtensibleCtrl(void);

		// IUsbDriver
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);

		// IUsbHostControllerDriver
		void                      METHOD Poll(UINT uLapsed);
		void                      METHOD EnableEvents(BOOL fEnable);
		BOOL			  METHOD ResetPort(UINT uPort);
		IUsbHostInterfaceDriver * METHOD GetInterface(UINT i);

		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);

		// IUsbHostInterfaceEvents
		void OnPortConnect(UsbPortPath &Route);
		void OnPortRemoval(UsbPortPath &Route);
		void OnPortCurrent(UsbPortPath &Route);
		void OnPortReset(UsbPortPath &Route);

	protected:
		// Flags
		enum 
		{
			flagReqReset	= Bit(0)
			};

		// Port Context
		struct CPort
		{
			UINT m_uState;
			UINT m_uTimer;
			UINT m_uSpeed;
			UINT m_uFlags;
			};

		// Data
		IUsbHostInterfaceDriver  * m_pLowerDrv;
		CPort                    * m_pPorts;
		UINT                       m_uPorts;

		// Ports
		void InitPorts(void);
		void MakePorts(void);
		void KillPorts(void);
		UINT FindPorts(void);
		void PollPorts(UINT uLapsed);
	};

// End of File

#endif
