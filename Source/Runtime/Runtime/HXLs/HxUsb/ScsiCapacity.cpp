
#include "Intern.hpp"

#include "ScsiCapacity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Capacity Data Object
//

// Constructor

CScsiCapacity::CScsiCapacity(void)
{
	}

// Endianess

void CScsiCapacity::HostToScsi(void)
{
	m_dwBlockAddr = HostToMotor(m_dwBlockAddr);
	
	m_dwBlockLen  = HostToMotor(m_dwBlockLen);
	}

void CScsiCapacity::ScsiToHost(void)
{
	m_dwBlockAddr = MotorToHost(m_dwBlockAddr);
	
	m_dwBlockLen  = MotorToHost(m_dwBlockLen);
	}

// Operations

void CScsiCapacity::Init(void)
{
	memset(this, 0, sizeof(ScsiCapacity));
	}

// End of File
