
#include "Intern.hpp"

#include "UsbHostModuleGenericDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Module Generic Driver
//

// Instantiator

IUsbHostModuleGeneric * Create_ModuleDriver(void)
{
	IUsbHostModuleGeneric *p = (IUsbHostModuleGeneric *) New CUsbHostModuleGenericDriver;

	return p;
	}

// Constructor

CUsbHostModuleGenericDriver::CUsbHostModuleGenericDriver(void)
{
	m_pName  = "Host Generic Module Driver";

	m_Debug  = debugWarn | debugErr;
	
	Trace(debugInfo, "Driver Created");
	}

// IUnknown

HRESULT CUsbHostModuleGenericDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostModuleGeneric);

	return CUsbHostModuleDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostModuleGenericDriver::AddRef(void)
{
	return CUsbHostModuleDriver::AddRef();
	}

ULONG CUsbHostModuleGenericDriver::Release(void)
{
	return CUsbHostModuleDriver::Release();
	}

// IHostFuncDriver

void CUsbHostModuleGenericDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostModuleDriver::Bind(pDevice, iInterface);
	}

void CUsbHostModuleGenericDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostModuleDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostModuleGenericDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostModuleDriver::GetDevice(pDev);
	}

BOOL CUsbHostModuleGenericDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostModuleDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostModuleGenericDriver::GetVendor(void)
{
	return CUsbHostModuleDriver::GetVendor();
	}

UINT CUsbHostModuleGenericDriver::GetProduct(void)
{
	return CUsbHostModuleDriver::GetProduct();
	}

UINT CUsbHostModuleGenericDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostModuleGenericDriver::GetSubClass(void)
{
	return CUsbHostModuleDriver::GetSubClass();
	}

UINT CUsbHostModuleGenericDriver::GetProtocol(void)
{
	return CUsbHostModuleDriver::GetProtocol();
	}

UINT CUsbHostModuleGenericDriver::GetInterface(void)
{
	return CUsbHostModuleDriver::GetInterface();
	}

BOOL CUsbHostModuleGenericDriver::GetActive(void)
{
	return CUsbHostModuleDriver::GetActive();
	}

BOOL CUsbHostModuleGenericDriver::Open(CUsbDescList const &List)
{
	return CUsbHostModuleDriver::Open(List);
	}

BOOL CUsbHostModuleGenericDriver::Close(void)
{
	return CUsbHostModuleDriver::Close();
	}

void CUsbHostModuleGenericDriver::Poll(UINT uLapsed)
{
	CUsbHostModuleDriver::Poll(uLapsed);
	}

// IUsbHostModuleDriver

BOOL CUsbHostModuleGenericDriver::Reset(void)
{
	return CUsbHostModuleDriver::Reset(); 
	}

BOOL CUsbHostModuleGenericDriver::ReadVersion(BYTE bVersion[16])
{
	return CUsbHostModuleDriver::ReadVersion(bVersion);
	}

BOOL CUsbHostModuleGenericDriver::SendHeartbeat(void)
{
	return CUsbHostModuleDriver::SendHeartbeat();
	}

// IUsbHostModuleGeneric

BOOL CUsbHostModuleGenericDriver::SendData(PCBYTE pData, UINT uCount)
{
	Trace(debugCmds, "SendData(Count=%d)", uCount);

	return CUsbHostModuleDriver::SendBulk(pData, uCount);
	}

UINT CUsbHostModuleGenericDriver::RecvData(PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "RecvData(Count=%d)", uCount);

	return CUsbHostModuleDriver::RecvBulk(pData, uCount);
	}

BOOL CUsbHostModuleGenericDriver::SendDataAsync(PCBYTE pData, UINT uCount)
{
	Trace(debugCmds, "SendDataAsync(Count=%d)", uCount);

	return CUsbHostModuleDriver::SendBulk(pData, uCount, true);
	}

UINT CUsbHostModuleGenericDriver::RecvDataAsync(PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "RecvDataAsync(Count=%d)", uCount);

	return CUsbHostModuleDriver::RecvBulk(pData, uCount, true);
	}

UINT CUsbHostModuleGenericDriver::WaitAsyncSend(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncSend(Timeout=%d)", uTimeout);

	return CUsbHostModuleDriver::WaitAsyncSend(uTimeout);
	}

UINT CUsbHostModuleGenericDriver::WaitAsyncRecv(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncRecv(Timeout=%d)", uTimeout);

	return CUsbHostModuleDriver::WaitAsyncRecv(uTimeout);
	}

BOOL CUsbHostModuleGenericDriver::KillAsyncSend(void)
{
	Trace(debugCmds, "KillAsyncSend");

	return CUsbHostModuleDriver::KillAsyncSend();
	}

BOOL CUsbHostModuleGenericDriver::KillAsyncRecv(void)
{
	Trace(debugCmds, "KillAsyncRecv");

	return CUsbHostModuleDriver::KillAsyncRecv();
	}

BOOL CUsbHostModuleGenericDriver::Shutdown(void)
{
	Trace(debugCmds, "Shutdown");

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_wIndex    = m_iInt;

	Req.m_bRequest  = cmdShutdown;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

// End of File
