
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiCmd6_HPP

#define	INCLUDE_ScsiCmd6_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Descriptor Block 6
//

class CScsiCmd6 : public ScsiCmd6
{
	public:
		// Constructor
		CScsiCmd6(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Attributes
		DWORD GetLBA(void);

		// Operations
		void Init(void);
		void Init(BYTE bOpcode);
		void Init(BYTE bOpcode, BYTE bLength);
		void Init(BYTE bOpcode, BYTE bLength, DWORD dwLBA);
	};

// End of File

#endif
