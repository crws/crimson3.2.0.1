
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostNcmDriver_HPP

#define	INCLUDE_UsbHostNcmDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostEcmDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host CDC Network Control Class Driver
//

class CUsbHostNcmDriver : public CUsbHostEcmDriver, public IUsbHostNcm
{
	public:
		// Constructor
		CUsbHostNcmDriver(void);

		// Destructor
		~CUsbHostNcmDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostCdc
		BOOL METHOD SendEncapCommand(PBYTE pData, UINT uLen);
		UINT METHOD RecvEncapResponse(PBYTE pData, UINT uLen);
		BOOL METHOD SendData(PCTXT pData, bool fAsync);
		BOOL METHOD SendData(PCBYTE pData, UINT uLen, bool fAsync);
		UINT METHOD RecvData(PBYTE  pData, UINT uLen, bool fAsync);
		UINT METHOD WaitSend(UINT uTimeout);
		UINT METHOD WaitRecv(UINT uTimeout);
		void METHOD KillSend(void);
		void METHOD KillRecv(void);
		void METHOD KillAsync(void);

		// IUsbHostEcm
		BOOL METHOD IsLinkActive(void);
		BOOL METHOD WaitLinkActive(UINT uTime);
		void METHOD GetMac(MACADDR &Addr);
		UINT METHOD GetMaxFilters(void);
		BOOL METHOD GetFilterHashing(void);
		UINT METHOD GetCapabilities(void);
		BOOL METHOD SetMulticastFilters(MACADDR const *pList, UINT uList);
		BOOL METHOD SetPacketFilters(WORD wFilter);
		BOOL METHOD GetStatistics(UINT iSelector, DWORD &Data);

		// IUsbHostNcm
		BOOL METHOD GetNtbParams(CdcNtbParam &Param);
		BOOL METHOD GetNetAddress(MACADDR &Addr); 
		BOOL METHOD SetNetAddress(MACADDR const &Addr);
		UINT METHOD GetNtbFormat(void);
		BOOL METHOD SetNtbFormat(UINT uFormat);
		BOOL METHOD GetNtbInputSize(UINT &uSize, UINT &uCount);
		BOOL METHOD SetNtbInputSize(UINT uSize, UINT  uCount);
		UINT METHOD GetMaxDatagramSize(void);
		BOOL METHOD SetMaxDatagramSize(UINT uSize);
		UINT METHOD GetCrcMode(void);
		BOOL METHOD SetCrcMode(UINT uMode);

		// Formats
		enum
		{
			formatNtb16	= 0x0000,
			formatNtb32	= 0x0001,
			};

	protected:
		// Data
		CdcNcmDesc * m_pNcm;

		// Implementation
		bool ParseConfig(void);
	};

// End of File

#endif
