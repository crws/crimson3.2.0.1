
#include "Intern.hpp"  

#include "UsbHubDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Command Classes
//

#include "UsbHubClearFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Classes
//

#include "UsbHubDesc.hpp"

#include "UsbHubClearPortFeatureReq.hpp"

#include "UsbHubGetDescReq.hpp"

#include "UsbHubGetStatusReq.hpp"

#include "UsbHubGetPortStatusReq.hpp"

#include "UsbHubSetFeatureReq.hpp"

#include "UsbHubSetPortFeatureReq.hpp"

#include "UsbSetInterfaceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Driver
//

// Instantiator

IUsbHubDriver * Create_HubDriver(void)
{
	IUsbHubDriver *p = New CUsbHubDriver;

	return p;
	}

// Constructor

CUsbHubDriver::CUsbHubDriver(void)
{
	m_pName    = "Usb Hub Driver";
		    
	m_Debug    = debugErr | debugWarn;
		    
	m_pCtrl    = NULL;
		    
	m_pBulk    = NULL;
		    
	m_pNotify  = NULL;
		    
	m_uFlags   = 0;
		    
	m_fWait    = false;
		    
	m_uLock    = NOTHING;
		    
	m_uTimer   = constTimerOff;

	m_fMultiTT = false;

	AfxGetObject("leds", 0, ILeds, m_pLeds);
	}

// Destructor

CUsbHubDriver::~CUsbHubDriver(void)
{
	AfxRelease(m_pLeds);
	}

// IUnknown

HRESULT CUsbHubDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHubDriver);

	return CUsbHostFuncDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHubDriver::AddRef(void)
{
	return CUsbHostFuncDriver::AddRef();
	}

ULONG CUsbHubDriver::Release(void)
{
	return CUsbHostFuncDriver::Release();
	}

// IHostFuncDriver

void CUsbHubDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncDriver::Bind(pDevice, iInterface);
	}

void CUsbHubDriver::Bind(IUsbHostFuncEvents *pSink)
{
	}

void CUsbHubDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncDriver::GetDevice(pDev);
	}

BOOL CUsbHubDriver::SetRemoveLock(BOOL fLock)
{
	return FALSE;
	}

UINT CUsbHubDriver::GetVendor(void)
{
	return CUsbHostFuncDriver::GetVendor();
	}

UINT CUsbHubDriver::GetProduct(void)
{
	return CUsbHostFuncDriver::GetProduct();
	}

UINT CUsbHubDriver::GetClass(void)
{
	return devHub;
	}

UINT CUsbHubDriver::GetSubClass(void)
{
	return 0;
	}

UINT CUsbHubDriver::GetProtocol(void)
{
	return 0;
	}

UINT CUsbHubDriver::GetInterface(void)
{
	return CUsbHostFuncDriver::GetInterface();
	}

BOOL CUsbHubDriver::GetActive(void)
{
	return CUsbHostFuncDriver::GetActive();
	}

BOOL CUsbHubDriver::Open(CUsbDescList const &List)
{
	if( FindPipes(List) ) {	

		EnableMultiTT(List);

		if( GetDescriptor((PBYTE) &m_HubDesc, sizeof(m_HubDesc)) ) {
				
			MakePorts();

			m_fWait = false;

			return CUsbHostFuncDriver::Open();
			}
		}

	return false;
	}

BOOL CUsbHubDriver::Close(void)
{
	CUsbHostFuncDriver::Close();

	KillComms();

	FreeLocks();

	FreePorts();

	return true;
	}

void CUsbHubDriver::Poll(UINT uLapsed)
{
	PollPorts(uLapsed);

	PollEvents(uLapsed);

	PollPoll(uLapsed);
	}

// IHubDriver

BOOL CUsbHubDriver::Bind(IUsbHubEvents *pSink)
{
	m_pNotify = pSink;

	return true;
	}

BOOL CUsbHubDriver::SetLedMap(UsbHubLedMap const &Map)
{
	memcpy(&m_LedMap, &Map, sizeof(Map));

	m_uFlags |= flagLedExternal;

	return true;
	}

BOOL CUsbHubDriver::ResetPort(WORD wPort)
{
	Trace(debugInfo, "ResetPort(wPort=%d)", wPort);

	if( wPort && wPort <= m_HubDesc.m_bPorts ) {

		CPort &Port = m_pPorts[wPort-1];

		Port.m_uFlags |= flagReqReset;
		
		return TRUE;
		}

	return FALSE;
	}

UINT CUsbHubDriver::GetSpeed(WORD wPort)
{
	if( wPort && wPort <= m_HubDesc.m_bPorts ) {

		return m_pPorts[wPort - 1].m_uSpeed;
		}

	return 0;
	}

BOOL CUsbHubDriver::GetDescriptor(PBYTE pData, UINT uLen)
{
	CUsbHubGetDescReq Req(0);

	Req.HostToUsb();

	if( CtrlTrans(Req, pData, uLen) >= sizeof(UsbHubDesc) ) {

		CUsbHubDesc &Desc = (CUsbHubDesc &) pData[0];

		Desc.UsbToHost();

		Desc.Debug();
				
		return Desc.IsValid();
		}

	return false;
	}

BOOL CUsbHubDriver::GetStatus(WORD &wStatus, WORD &wChange)
{
	CUsbHubGetStatusReq Req;

	Req.HostToUsb();

	WORD wData[2];

	if( CtrlTrans(Req, PBYTE(&wData), sizeof(wData)) == sizeof(wData) ) {

		wStatus = ::IntelToHost(wData[0]);

		wChange = ::IntelToHost(wData[1]);

		return true;
		}

	return false;
	}

BOOL CUsbHubDriver::GetPortStatus(WORD wPort, WORD &wStatus, WORD &wChange)
{
	CUsbHubGetPortStatusReq Req(wPort);

	Req.HostToUsb();

	WORD wData[2];

	if( CtrlTrans(Req, PBYTE(&wData), sizeof(wData)) == sizeof(wData) ) {

		wStatus = ::IntelToHost(wData[0]);

		wChange = ::IntelToHost(wData[1]);

		return true;
		}

	return false;
	}

BOOL CUsbHubDriver::SetFeature(WORD wFeature)
{
	CUsbHubSetFeatureReq Req(wFeature);

	Req.HostToUsb();

	return CtrlTrans(Req);
	}

BOOL CUsbHubDriver::ClrFeature(WORD wFeature)
{
	CUsbHubClearFeatureReq Req(wFeature);

	Req.HostToUsb();

	return CtrlTrans(Req);
	}

BOOL CUsbHubDriver::SetPortFeature(WORD wPort, WORD wFeature)
{
	CUsbHubSetPortFeatureReq Req(wFeature, wPort);

	Req.HostToUsb();

	return CtrlTrans(Req);
	}

BOOL CUsbHubDriver::ClrPortFeature(WORD wPort, WORD wFeature)
{
	CUsbHubClearPortFeatureReq Req(wFeature, wPort);

	Req.HostToUsb();

	return CtrlTrans(Req);
	}

BOOL CUsbHubDriver::HasMultiTT(void)
{
	return m_fMultiTT;
	}

// Feature Helpers

BOOL CUsbHubDriver::ClrPortEnable(WORD wPort)
{
	Trace(debugCmds, "ClearPortEnable(wPort=%d)", wPort);

	return ClrPortFeature(wPort, selPortEnable);
	}

BOOL CUsbHubDriver::ClrPortSuspend(WORD wPort)
{
	Trace(debugCmds, "ClearPortSuspend(wPort=%d)", wPort);

	return ClrPortFeature(wPort, selPortSuspend);
	}

BOOL CUsbHubDriver::ClrPortPower(WORD wPort)
{
	Trace(debugCmds, "ClrPortPower(wPort=%d)", wPort);

	return ClrPortFeature(wPort, selPortPower);
	}

BOOL CUsbHubDriver::ClrIndicator(WORD wPort)
{
	Trace(debugCmds, "ClearPortIndicator(wPort=%d)", wPort);

	return ClrPortFeature(wPort, selPortIndicator);
	}

BOOL CUsbHubDriver::ClrPortEnableChange(WORD wPort)
{
	return ClrPortFeature(wPort, selCPortEnable);
	}

BOOL CUsbHubDriver::ClrPortConnectChange(WORD wPort)
{
	return ClrPortFeature(wPort, selCPortConnection);
	}

BOOL CUsbHubDriver::ClrPortResetChange(WORD wPort)
{
	return ClrPortFeature(wPort, selCPortReset);
	}

BOOL CUsbHubDriver::ClrPortCurrentChange(WORD wPort)
{
	return ClrPortFeature(wPort, selCPortOverCurrnt);
	}

BOOL CUsbHubDriver::SetPortReset(WORD wPort)
{
	Trace(debugCmds, "SetPortReset(wPort=%d)", wPort);

	return SetPortFeature(wPort, selPortReset);
	}

BOOL CUsbHubDriver::SetPortSuspend(WORD wPort)
{
	Trace(debugCmds, "SetPortSuspend(wPort=%d)", wPort);

	return SetPortFeature(wPort, selPortSuspend);
	}

BOOL CUsbHubDriver::SetPortPower(WORD wPort)
{
	Trace(debugCmds, "SetPortPower(wPort=%d)", wPort);

	return SetPortFeature(wPort, selPortPower);
	}

BOOL CUsbHubDriver::SetPortIndicator(WORD wPort)
{
	Trace(debugCmds, "SetPortIndicator(iPort=%d)", wPort);

	return SetPortFeature(wPort, selPortIndicator);
	}

// Device 

void CUsbHubDriver::OnDeviceArrival(UINT iPort)
{
	Trace(debugInfo, "OnDeviceArrival(iPort=%d)", iPort);

	UsbPortPath Path = m_pDev->GetPortPath();

	Path.a.dwHubPort = iPort + 1;

	Path.a.dwHubAddr = m_pDev->GetAddr();

	m_pNotify->OnHubDeviceArrival(Path);
	}

void CUsbHubDriver::OnDeviceRemoval(UINT iPort)
{
	Trace(debugInfo, "OnDeviceRemoval(iPort=%d)", iPort);

	UsbPortPath Path = m_pDev->GetPortPath();

	Path.a.dwHubPort = iPort + 1;

	Path.a.dwHubAddr = m_pDev->GetAddr();

	m_pNotify->OnHubDeviceRemoval(Path);
	}

// Port Events

void CUsbHubDriver::OnPortConnect(UINT iPort)
{
	Trace(debugInfo, "OnPortConnect(iPort=%d)", iPort);

	CPort &Port = m_pPorts[iPort];

	if( Port.m_uState == stateEnabled ) {

		Trace(debugState, "iPort=%d %s -> %s", iPort, GetStateText(stateEnabled), GetStateText(stateAttached));

		Port.m_uState  = stateAttached;

		Port.m_uTimer1 = constTimerAttach;
		}
	}

void CUsbHubDriver::OnPortReset(UINT iPort, WORD wStatus)
{
	Trace(debugInfo, "OnPortReset(iPort=%d, Status=0x%4.4X)", iPort, wStatus);

	CPort &Port = m_pPorts[iPort];

	if( Port.m_uState == stateReset ) {

		Trace(debugState, "iPort=%d %s -> %s", iPort, GetStateText(stateReset), GetStateText(stateConnected));

		Port.m_uState = stateConnected;

		Port.m_uSpeed = usbSpeedFull;

		if( wStatus & portLowSpeed ) {
							
			Port.m_uSpeed = usbSpeedLow;
			}

		if( wStatus & portHighSpeed ) {

			Port.m_uSpeed = usbSpeedHigh;
			}

		LedStatus(iPort);

		OnDeviceArrival(iPort);

		m_uLock = NOTHING;
		}
	}

void CUsbHubDriver::OnPortRemoval(UINT iPort)
{
	Trace(debugInfo, "OnPortRemoval(iPort=%d)", iPort);

	FreeLock(iPort);

	CPort &Port = m_pPorts[iPort];

	if( Port.m_uState > stateEnabled && Port.m_uState < stateRestart ) {

		Trace(debugState, "iPort=%d %s -> %s", iPort, GetStateText(Port.m_uState), GetStateText(stateEnabled));

		Port.m_uState  = stateEnabled;

		Port.m_uTimer1 = constTimerOff;

		Port.m_uTimer2 = constTimerDog;

		OnDeviceRemoval(iPort);

		LedStatus(iPort);
		}
	else {
		Port.m_uTimer1 = constTimerPort;
		
		Port.m_uTimer2 = constTimerDog;
		}
	}

void CUsbHubDriver::OnPortCurrent(UINT iPort)
{
	Trace(debugWarn, "OnPortCurrent(iPort=%d)", iPort);

	CPort &Port = m_pPorts[iPort];

	if( Port.m_uState != stateOverCurrent ) {

		Trace(debugState, "iPort=%d %s -> %s", iPort, GetStateText(Port.m_uState), GetStateText(stateOverCurrent));

		UINT nPort = iPort + 1;

		ClrPortPower(nPort);

		ClrPortEnable(nPort);

		Port.m_uState  = stateOverCurrent;

		Port.m_uTimer1 = constTimerCurrent;

		LedStatus(iPort);
		}
	}

// Ports

void CUsbHubDriver::MakePorts(void)
{
	m_pPorts = New CPort [ m_HubDesc.m_bPorts ];

	memset(m_pPorts, 0, sizeof(CPort) * m_HubDesc.m_bPorts);
	}

void CUsbHubDriver::PollEvents(UINT uLapsed)
{
	for(;;) {

		if( !m_fWait ) {

			m_bEvent = 0;

			if( m_pBulk ) {

				if( m_pBulk->RecvBulk(&m_bEvent, 1, true) != NOTHING ) {

					m_fWait = true;
					}

				return;
				}
			}
		else {
			BOOL fOk;

			UINT uCount;
			
			m_fWait = m_pBulk->WaitAsync(0, fOk, uCount) ? false : true;

			if( m_fWait || !fOk || uCount != 1 ) {

				return;
				}
			}

		Trace(debugData, "Event[0x%2.2X]", m_bEvent);

		for( UINT iPort = 0; iPort < m_HubDesc.m_bPorts; iPort ++ ) { 

			UINT nPort = iPort + 1;

			if( !m_pBulk || (m_bEvent & Bit(nPort)) ) {

				CheckPort(iPort);
				}
			}
		}
	}

void CUsbHubDriver::PollPorts(UINT uLapsed)
{
	for( UINT i = 0; i < m_HubDesc.m_bPorts; i ++ ) {

		CPort &Port = m_pPorts[i];

		UINT  nPort = i + 1;

		switch( Port.m_uState ) { 

			case stateInit:

				Trace(debugState, "iPort=%d %s -> %s", i, GetStateText(stateInit), GetStateText(stateEnabled));

				SetPortPower(nPort);

				Port.m_uState  = stateEnabled;

				Port.m_uTimer1 = constTimerOff;

				Port.m_uTimer2 = constTimerDog;

				LedStatus(i);

				break;

			case stateEnabled:

				if( !CheckTimer(Port.m_uTimer1, uLapsed) ) {

					CheckPort(i);
					}

				if( false && !CheckTimer(Port.m_uTimer2, uLapsed) ) {

					// NOTE � This feature must be selectively enabled for racks with unganged power control.

					Trace(debugState, "iPort=%d %s -> %s", i, GetStateText(stateEnabled), GetStateText(stateRestart));

					ClrPortPower(nPort);

					Port.m_uState  = stateRestart;
					
					Port.m_uTimer1 = constTimerRestart;

					LedStatus(i);
					}

				break;

			case stateAttached:

				if( !CheckTimer(Port.m_uTimer1, uLapsed) ) {

					Trace(debugState, "iPort=%d %s -> %s", i, GetStateText(stateAttached), GetStateText(statePowered));

					ClrPortEnable(nPort);
					
					Port.m_uState = statePowered;

					LedStatus(i);
					}

				break;

			case statePowered:

				if( m_pDev->GetHostI()->LockDefAddr(true) ) {

					Trace(debugState, "iPort=%d %s -> %s", i, GetStateText(statePowered), GetStateText(stateReset));

					SetPortReset(nPort);

					Port.m_uState = stateReset;

					m_uLock = i;

					LedStatus(i);
					}

				break;

			case stateOverCurrent:
			case stateRestart:

				if( !CheckTimer(Port.m_uTimer1, uLapsed) ) {

					Trace(debugState, "iPort=%d %s -> %s", i, GetStateText(Port.m_uState), GetStateText(stateEnabled));

					Port.m_uState  = stateEnabled;

					Port.m_uTimer2 = constTimerDog; 

					SetPortPower(nPort);

					LedStatus(i);
					}
			
				break;

			case stateConnected:

				if( Port.m_uFlags & flagReqReset ) {

					Port.m_uFlags &= ~flagReqReset;
					
					Trace(debugState, "iPort=%d %s -> %s", i, GetStateText(stateConnected), GetStateText(stateRestart));

					OnPortRemoval(i);

					ClrPortPower(nPort);

					Port.m_uState  = stateRestart;
					
					Port.m_uTimer1 = constTimerRestart;

					LedStatus(i);
					}

				break;
			}
		}
	} 

void CUsbHubDriver::CheckPort(UINT iPort)
{
	CPort &Port = m_pPorts[iPort];

	UINT  nPort = iPort + 1;

	for(;;) {

		WORD wStatus = 0;
		
		WORD wChange = 0;

		if( GetPortStatus(nPort, wStatus, wChange) ) {

			wChange |= (wStatus ^ Port.m_wStatus);

			Port.m_wStatus = wStatus;

			Trace(debugData, "PortStatus(iPort=%d, Change=0x%4.4X, Status=0x%4.4X)", iPort, wChange, wStatus);

			if( wChange & portConnect ) {

				ClrPortConnectChange(nPort);

				if( Port.m_wStatus & portConnect ) {

					OnPortRemoval(iPort);

					OnPortConnect(iPort);
					}
				else {
					OnPortRemoval(iPort);
					}
				
				continue;
				}

			if( wChange & portReset ) {

				ClrPortResetChange(nPort);

				if( !(wStatus & portReset) ) {

					OnPortReset(iPort, wStatus);
					}

				continue;
				}

			if( wChange & portOverCurrent ) {

				ClrPortCurrentChange(nPort);

				if( wStatus & portOverCurrent ) {

					OnPortCurrent(iPort);

					continue;
					}
				
				if( !(wStatus & portPower) ) {

					OnPortCurrent(iPort);

					continue;
					}

				continue;
				}

			if( wChange & portEnable ) {

				ClrPortEnableChange(nPort);

				continue;
				}
			}

		break;
		}
	}

void CUsbHubDriver::FreePorts(void)
{
	if( m_pPorts ) {

		delete [] m_pPorts;

		m_pPorts = NULL;
		}
	}

void CUsbHubDriver::FreeLocks(void)
{
	if( m_uLock != NOTHING ) {
		
		FreeLock(m_uLock);
		}
	}

void CUsbHubDriver::FreeLock(UINT iPort)
{
	if( m_uLock != NOTHING && iPort == m_uLock ) {

		Trace(debugWarn, "Default Lock forced release");

		m_pDev->GetHostI()->LockDefAddr(false);
		
		m_uLock = NOTHING;
		}
	}

void CUsbHubDriver::KillComms(void)
{
	if( m_pBulk && m_fWait ) {

		BOOL fOk;

		UINT uCount;
			
		if( !m_pBulk->WaitAsync(1000, fOk, uCount) ) {

			Trace(debugWarn, "Forced comms shutdown.");
		
			m_pBulk->KillAsync();
			}

		m_fWait = false;
		}
	}

void CUsbHubDriver::LedStatus(UINT iPort)
{
	if( m_uFlags & flagLedExternal && m_pLeds ) {

		if( m_LedMap.m_uLed1[iPort] && m_LedMap.m_uLed2[iPort] ) {

			switch( m_pPorts[iPort].m_uState ) {
			
				case stateInit:
				case stateEnabled:
				case stateAttached:
				case statePowered:
				case stateReset:

					m_pLeds->SetLed(m_LedMap.m_uLed1[iPort], false);

					m_pLeds->SetLed(m_LedMap.m_uLed2[iPort], false);

					break;

				case stateConnected:

					m_pLeds->SetLed(m_LedMap.m_uLed1[iPort], true);

					m_pLeds->SetLed(m_LedMap.m_uLed2[iPort], false);

					break;

				case stateOverCurrent:
				case stateRestart:

					m_pLeds->SetLed(m_LedMap.m_uLed1[iPort], false);

					m_pLeds->SetLed(m_LedMap.m_uLed2[iPort], true);

					break;
				}
			}
		}
	}

// Hub

void CUsbHubDriver::PollPoll(UINT uLapsed)
{
	if( !CheckTimer(m_uTimer, uLapsed) ) {

		WORD wStatus, wChange;
		
		GetStatus(wStatus, wChange);
		}
	}

BOOL CUsbHubDriver::CtrlTrans(UsbDeviceReq &Req)
{
	m_uTimer = constTimerPoll;

	return m_pCtrl ? m_pCtrl->CtrlTrans(Req) : FALSE;
	}

UINT CUsbHubDriver::CtrlTrans(UsbDeviceReq &Req, PBYTE pData, UINT uLen)
{
	m_uTimer = constTimerPoll;

	return m_pCtrl ? m_pCtrl->CtrlTrans(Req, pData, uLen) : NOTHING;
	}

// Helpers

BOOL CUsbHubDriver::FindPipes(CUsbDescList const &List)
{
	UINT iIndex = List.GetIndexStart();
	
	UsbInterfaceDesc *pInt = (UsbInterfaceDesc *) List.EnumInterface(iIndex);

	if( pInt && pInt->m_bEndpoints == 1 ) {

		UsbEndpointDesc *pEndpt = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		if( pEndpt ) {
		
			m_pCtrl = m_pDev->GetCtrlPipe();

			m_pBulk = m_pDev->GetPipe(pEndpt->m_bAddr, pEndpt->m_bDirIn);

			m_iBulk = pEndpt->m_bAddr | Bit(7);
			
			return m_pCtrl != NULL && m_pBulk != NULL;
			}
		}
	
	return false;
	}

BOOL CUsbHubDriver::EnableMultiTT(CUsbDescList const &List)
{
	if( m_pDev->GetDevDesc().m_bProtocol == 2 ) {

		UINT iIndex = List.GetIndexStart();

		List.EnumInterface(iIndex);

		UsbInterfaceDesc *pInt = (UsbInterfaceDesc *) List.EnumInterface(iIndex);

		if( pInt && pInt->m_bThis == m_iInt ) {
			
			if( pInt->m_bAltSetting && pInt->m_bProtocol == 2 ) {

				CUsbSetInterfaceReq Req(pInt->m_bAltSetting, m_iInt);

				if( m_pCtrl->CtrlTrans(Req) ) {

					Trace(debugInfo, "MultiTT Enabled.");
						
					m_fMultiTT = true;

					return true;
					}
				}
			}
		}

	return false;
	}

BOOL CUsbHubDriver::CheckTimer(UINT &uTimer, UINT uLapsed)
{
	if( uTimer ) {

		if( uTimer > uLapsed ) {

			uTimer -= uLapsed;

			return true;
			}

		uTimer = constTimerOff;
						
		return false;
		}

	return true;
	}

PTXT CUsbHubDriver::GetStateText(UINT uState) const
{
	#if defined(_XDEBUG)	

	switch( uState ) {

		case stateInit:	       return "INIT";
		case stateEnabled:     return "ENABLED";
		case stateAttached:    return "ATTACHED";
		case statePowered:     return "POWERED";
		case stateReset:       return "RESET";
		case stateConnected:   return "CONNECTED";
		case stateOverCurrent: return "OVER CURRENT";
		case stateRestart:     return "RESTART";
		}

	#endif

	return "";
	}

// End of File
