
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbFuncCompDriver_HPP

#define	INCLUDE_UsbFuncCompDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbStringDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Composite Function Driver
//

class CUsbFuncCompDriver : public IUsbFuncCompDriver, public IUsbFuncEvents
{
	public:
		// Constructor
		CUsbFuncCompDriver(void);

		// Destructor
		~CUsbFuncCompDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pDriver);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);
		
		// IUsbFuncDriver
		UINT METHOD GetState(void);
		UINT METHOD GetMaxPacket(UINT iEndpt);
		BOOL METHOD SendCtrlStatus(UINT iEndpt);
		BOOL METHOD RecvCtrlStatus(UINT iEndpt);
		BOOL METHOD SendCtrl(UINT iEndpt, PCBYTE pData, UINT uLen);
		BOOL METHOD RecvCtrl(UINT iEndpt, PBYTE  pData, UINT uLen);
		UINT METHOD SendBulk(UINT iEndpt, PCBYTE pData, UINT uLen, UINT uTimeout);
		UINT METHOD RecvBulk(UINT iEndpt, PBYTE  pData, UINT uLen, UINT uTimeout);
		BOOL METHOD SetStall(UINT iEndpt, BOOL fStall);
		BOOL METHOD GetStall(UINT iEndpt);

		// IUsbFuncCompDriver
		void METHOD SetLanguages(PWORD pwLangId, UINT uCount);
		void METHOD SetVendor(WORD wId, PCTXT pName);
		void METHOD SetProduct(WORD wId, PCTXT pName);
		void METHOD SetSerialNumber(PCTXT pText);
		UINT METHOD FindInterface(IUsbEvents *pDriver);
		UINT METHOD GetFirsEndptAddr(UINT iInterface);
						
		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);

		// IUsbFuncEvents
		void METHOD OnState(UINT uState);
		BOOL METHOD OnSetupClass(UsbDeviceReq &Req);
		BOOL METHOD OnSetupVendor(UsbDeviceReq &Req);
		BOOL METHOD OnSetupOther(UsbDeviceReq &Req);
		BOOL METHOD OnGetDevice(UsbDesc &Desc);
		BOOL METHOD OnGetQualifier(UsbDesc &Desc);
		BOOL METHOD OnGetConfig(UsbDesc &Desc);
		BOOL METHOD OnGetInterface(UsbDesc &Desc);
		BOOL METHOD OnGetEndpoint(UsbDesc &Desc);
		BOOL METHOD OnGetString(UsbDesc *&pDesc, UINT iIndex);

	protected:
		// Interface Informaiont
		struct CInterface
		{
			IUsbFuncEvents * m_pDrv;
			UINT             m_uEndpts;
			UINT             m_uAddr;
			};
		
		// Data
		ULONG            m_uRefs;
		IUsbFuncDriver * m_pLowerDrv;
		CInterface       m_Interfaces[4];
		UINT             m_uCount;
		WORD             m_wVendor;
		WORD		 m_wProduct;
		CUsbStringDesc   m_sLangIds;
		CUsbStringDesc   m_sVendor;
		CUsbStringDesc   m_sProduct;
		CUsbStringDesc   m_sSerial;

		// Interface Helpers
		void MapEndpoints(void);
		BOOL BindInterface(IUsbFuncEvents *pDriver);
		UINT FindInterface(UINT uEndpt) const;
		void FreeInterfaces(void);
	};

// End of File

#endif
