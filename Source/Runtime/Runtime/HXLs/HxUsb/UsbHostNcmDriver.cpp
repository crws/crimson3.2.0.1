
#include "Intern.hpp"  

#include "UsbHostNcmDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbClassReq.hpp"

#include "CdcNtbParam.hpp"

#include "CdcNtbSize.hpp"

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Ethernet Device Class Driver
//

// Instantiator

IUsbHostFuncDriver * Create_NcmDriver(void)
{
	IUsbHostNcm *p = (IUsbHostNcm *) New CUsbHostNcmDriver;

	return p;
	}

// Constructor

CUsbHostNcmDriver::CUsbHostNcmDriver(void)
{
	m_pName = "Ethernet Device Class Driver";

	m_pNcm  = NULL;
	}

// Destructor

CUsbHostNcmDriver::~CUsbHostNcmDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");

	m_pLink->Release();
	}

// IUnknown

HRESULT CUsbHostNcmDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostNcm);

	return CUsbHostEcmDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostNcmDriver::AddRef(void)
{
	return CUsbHostEcmDriver::AddRef();
	}

ULONG CUsbHostNcmDriver::Release(void)
{
	return CUsbHostEcmDriver::Release();
	}

// IHostFuncDriver

void CUsbHostNcmDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostEcmDriver::Bind(pDevice, iInterface);
	}

void CUsbHostNcmDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostEcmDriver::Bind(pSink);
	}

void CUsbHostNcmDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostEcmDriver::GetDevice(pDev);
	}

BOOL CUsbHostNcmDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostEcmDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostNcmDriver::GetVendor(void)
{
	return CUsbHostEcmDriver::GetVendor();
	}

UINT CUsbHostNcmDriver::GetProduct(void)
{
	return CUsbHostEcmDriver::GetProduct();
	}

UINT CUsbHostNcmDriver::GetClass(void)
{
	return CUsbHostEcmDriver::GetClass();
	}

UINT CUsbHostNcmDriver::GetSubClass(void)
{
	return classNetworkControl;
	}

UINT CUsbHostNcmDriver::GetInterface(void)
{
	return CUsbHostEcmDriver::GetInterface();
	}

UINT CUsbHostNcmDriver::GetProtocol(void)
{
	return protNone;
	}

BOOL CUsbHostNcmDriver::GetActive(void)
{
	return CUsbHostEcmDriver::GetActive();
	}

BOOL CUsbHostNcmDriver::Open(CUsbDescList const &List)
{
	if( ParseConfig() ) {

		return CUsbHostCdcDriver::Open(List);
		}

	return FALSE;
	}

BOOL CUsbHostNcmDriver::Close(void)
{
	return CUsbHostEcmDriver::Close();
	}

void CUsbHostNcmDriver::Poll(UINT uLapsed)
{
	CUsbHostEcmDriver::Poll(uLapsed);
	}

// IUsbHostCdc

BOOL CUsbHostNcmDriver::SendEncapCommand(PBYTE pData, UINT uLen)
{
	return CUsbHostEcmDriver::SendEncapCommand(pData, uLen);
	}

UINT CUsbHostNcmDriver::RecvEncapResponse(PBYTE pData, UINT uLen)
{
	return CUsbHostEcmDriver::RecvEncapResponse(pData, uLen);
	}

BOOL CUsbHostNcmDriver::SendData(PCTXT pData, bool fAsync)
{
	return CUsbHostEcmDriver::SendData(pData, fAsync);
	}

BOOL CUsbHostNcmDriver::SendData(PCBYTE pData, UINT uLen, bool fAsync)
{
	return CUsbHostEcmDriver::SendData(pData, uLen, fAsync);
	}

UINT CUsbHostNcmDriver::RecvData(PBYTE pData, UINT uLen, bool fAsync)
{
	return CUsbHostEcmDriver::RecvData(pData, uLen, fAsync);
	}

UINT CUsbHostNcmDriver::WaitSend(UINT uTimeout)
{
	return CUsbHostEcmDriver::WaitSend(uTimeout);
	}

UINT CUsbHostNcmDriver::WaitRecv(UINT uTimeout)
{
	return CUsbHostEcmDriver::WaitRecv(uTimeout);
	}

void CUsbHostNcmDriver::KillSend(void)
{
	CUsbHostEcmDriver::KillSend();
	}

void CUsbHostNcmDriver::KillRecv(void)
{
	CUsbHostEcmDriver::KillRecv();
	}

void CUsbHostNcmDriver::KillAsync(void)
{
	CUsbHostEcmDriver::KillAsync();
	}

// IUsbHostEcm

BOOL CUsbHostNcmDriver::IsLinkActive(void)
{
	return CUsbHostEcmDriver::IsLinkActive();
	}

BOOL CUsbHostNcmDriver::WaitLinkActive(UINT uTime)
{
	return CUsbHostEcmDriver::WaitLinkActive(uTime);
	}

void CUsbHostNcmDriver::GetMac(MACADDR &Addr)
{
	GetNetAddress(Addr);
	}

UINT CUsbHostNcmDriver::GetMaxFilters(void)
{
	return CUsbHostEcmDriver::GetMaxFilters();
	}

BOOL CUsbHostNcmDriver::GetFilterHashing(void)
{
	return CUsbHostEcmDriver::GetFilterHashing();	
	}

UINT CUsbHostNcmDriver::GetCapabilities(void)
{
	return CUsbHostEcmDriver::GetCapabilities();
	}

BOOL CUsbHostNcmDriver::SetMulticastFilters(MACADDR const *pList, UINT uList)
{
	return CUsbHostEcmDriver::SetMulticastFilters(pList, uList);
	}

BOOL CUsbHostNcmDriver::SetPacketFilters(WORD wFilter)
{
	return CUsbHostEcmDriver::SetPacketFilters(wFilter);
	}

BOOL CUsbHostNcmDriver::GetStatistics(UINT iSelect, DWORD &Data)
{
	return CUsbHostEcmDriver::GetStatistics(iSelect, Data);
	}

// IUsbHostNcm

BOOL METHOD CUsbHostNcmDriver::GetNtbParams(CdcNtbParam &Param)
{
	Trace(debugCmds, "GetNtbParams");

	CUsbClassReq Req;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqGetNTBParams;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(CdcNtbParam);

	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&Param), sizeof(Param)) == sizeof(Param) ) {

		((CCdcNtbParam &) Param).HostToCdc();

		return TRUE;
		}

	return FALSE;
	}

BOOL METHOD CUsbHostNcmDriver::GetNetAddress(MACADDR &Addr)
{
	Trace(debugCmds, "GetNetAddress");

	CUsbClassReq Req;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqGetNetAddress;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(Addr);

	Req.HostToUsb();

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&Addr), sizeof(Addr)) == sizeof(Addr) ) {

		#if defined(_DEBUG)

		if( m_Debug & debugCmds ) {

			CMacAddr &Mac = (CMacAddr &) Addr;
			
			AfxTrace("Mac = %s", Mac.GetAsText());
			}

		#endif	
		
		return TRUE;
		}

	return FALSE;
	}

BOOL METHOD CUsbHostNcmDriver::SetNetAddress(MACADDR const &Addr)
{
	Trace(debugCmds, "SetNetAddress");

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSetNetAddress;

	Req.m_wLength   = sizeof(Addr);

	Req.m_wIndex    = m_iInt;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, PBYTE(&Addr), sizeof(Addr)) == sizeof(Addr);
	}

UINT METHOD CUsbHostNcmDriver::GetNtbFormat(void)
{
	Trace(debugCmds, "GetNtbFormat");

	CUsbClassReq Req;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqGetNTBFormat;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = 2;

	Req.HostToUsb();

	WORD wData;

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&wData), sizeof(WORD)) == sizeof(WORD) ) {

		wData = IntelToHost(wData);

		return wData;
		}

	return NOTHING;
	}

BOOL METHOD CUsbHostNcmDriver::SetNtbFormat(UINT uFormat)
{
	Trace(debugCmds, "SetNtbFormat(%d)", uFormat);

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSetNTBFormat;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = 2;

	Req.HostToUsb();

	WORD wData = HostToIntel(WORD(uFormat));

	return m_pCtrl->CtrlTrans(Req, PBYTE(&wData), sizeof(WORD)) == sizeof(WORD);
	}

BOOL METHOD CUsbHostNcmDriver::GetNtbInputSize(UINT &uSize, UINT &uCount)
{
	Trace(debugCmds, "GetNtbInputSize");

	bool fExtended = m_pNcm->m_bCaps & Bit(5);

	CUsbClassReq Req;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqGetNTBSize;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = fExtended ? sizeof(CdcNtbSize) : sizeof(DWORD);

	Req.HostToUsb();

	if( fExtended ) {

		CCdcNtbSize Data;

		if( m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data) ) {

			Data.CdcToHost();

			uSize  = Data.m_dwMaxSize;

			uCount = Data.m_wMaxCount; 

			return TRUE;
			}
		}
	else {
		DWORD Data;

		if( m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data) ) {

			uSize  = IntelToHost(Data);

			uCount = 0;

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL METHOD CUsbHostNcmDriver::SetNtbInputSize(UINT uSize, UINT uCount)
{
	Trace(debugCmds, "SetNtbInputSize(Size=%d, Count=%d)", uSize, uCount);

	bool fExtended = m_pNcm->m_bCaps & Bit(5);

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSetNTBSize;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = fExtended ? sizeof(CdcNtbSize) : sizeof(DWORD);

	Req.HostToUsb();

	if( fExtended ) {

		CCdcNtbSize Data;

		Data.m_dwMaxSize = uSize;

		Data.m_wMaxCount = uCount;

		Data.HostToCdc();

		return m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data);
		}
	else {
		DWORD Data = HostToIntel(DWORD(uSize));

		return m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data);
		}
	}

UINT METHOD CUsbHostNcmDriver::GetMaxDatagramSize(void)
{
	Trace(debugCmds, "GetMaxDatagramSize");

	CUsbClassReq Req;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqGetMaxSize;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(WORD);

	Req.HostToUsb();

	WORD Data;

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data) ) {

		Data = IntelToHost(Data);

		return Data;
		}

	return FALSE;
	}

BOOL METHOD CUsbHostNcmDriver::SetMaxDatagramSize(UINT uSize)
{
	Trace(debugCmds, "SetMaxDatagramSize(%d)", uSize);

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSetMaxSize;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(WORD);

	Req.HostToUsb();

	WORD Data = HostToIntel(WORD(uSize));

	return m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data);
	}

UINT METHOD CUsbHostNcmDriver::GetCrcMode(void)
{
	Trace(debugCmds, "GetCrcMode");

	CUsbClassReq Req;

	Req.m_Direction = dirDevToHost;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqGetCrcMode;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(WORD);

	Req.HostToUsb();

	WORD Data;

	if( m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data) ) {

		Data = IntelToHost(Data);

		return Data;
		}

	return FALSE;
	}

BOOL METHOD CUsbHostNcmDriver::SetCrcMode(UINT uMode)
{
	Trace(debugCmds, "SetCrcMode(%d)", uMode);

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
			
	Req.m_bRequest  = reqSetCrcMode;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(WORD);

	Req.HostToUsb();

	WORD Data = HostToIntel(WORD(uMode));

	return m_pCtrl->CtrlTrans(Req, PBYTE(&Data), sizeof(Data)) == sizeof(Data);
	}

// Implememtation

bool CUsbHostNcmDriver::ParseConfig(void)
{
	CUsbDescList const &List = m_pDev->GetCfgDesc();	
	
	UINT iIndex;

	List.FindInterface(iIndex, m_iInt);

	for(;;) {

		CdcFuncDesc *p = (CdcFuncDesc *) List.Enum(descCsInterface, iIndex);

		if( p ) {

			if( p->m_bSubType == descNetworkControl ) {

				m_pNcm = (CdcNcmDesc *) p;

				return true;
				}

			continue;
			}
		
		return false;
		}
	}

// End of File
