
#include "Intern.hpp"

#include "UsbDeviceQual.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

#include "UsbDeviceDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Qaulifier Descriptor
//

// Constructor

CUsbDeviceQual::CUsbDeviceQual(void)
{
	Init();
	}

// Endianess

void CUsbDeviceQual::HostToUsb(void)
{
	m_wVersion = HostToIntel(m_wVersion);
	}

void CUsbDeviceQual::UsbToHost(void)
{
	m_wVersion = IntelToHost(m_wVersion);
	}

// Attributes

BOOL CUsbDeviceQual::IsValid(void) const
{
	return m_bType == descDeviceQual && m_bLength == sizeof(UsbDeviceQual);
	}

// Init

void CUsbDeviceQual::Init(void)
{
	memset(this, 0, sizeof(UsbDeviceQual));
	
	m_bLength  = sizeof(UsbDeviceQual);
	
	m_bType	   = descDeviceQual;

	m_wVersion = 0x0200;
	}

void CUsbDeviceQual::InitFrom(CUsbDeviceDesc const &Dev)
{
	Init();

	m_wVersion   = Dev.m_wVersion;
	
	m_bClass     = Dev.m_bClass;
	
	m_bSubClass  = Dev.m_bSubClass;
	
	m_bProtocol  = Dev.m_bProtocol;
	
	m_bMaxPacket = Dev.m_bMaxPacket;
	}

// Debug

void CUsbDeviceQual::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\nUsb Device Qualifier Descriptor\n");

	AfxTrace("Type         = %d\n",      m_bType);
	AfxTrace("Len          = %d\n",      m_bLength);
	AfxTrace("Version      = 0x%4.4X\n", m_wVersion);
	AfxTrace("Class        = 0x%2.2X\n", m_bClass);
	AfxTrace("Subclass     = 0x%2.2X\n", m_bSubClass);
	AfxTrace("Protocol     = 0x%2.2X\n", m_bProtocol);
	AfxTrace("Max Packet   = %3.3d\n",   m_bMaxPacket);
	AfxTrace("Config Count = %d\n",      m_bConfigCount);

	#endif
	}

// End of File
