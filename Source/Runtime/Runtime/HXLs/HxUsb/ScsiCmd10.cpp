
#include "Intern.hpp"

#include "ScsiCmd10.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Descriptor Block 10
//

// Constructor

CScsiCmd10::CScsiCmd10(void)
{
	}

// Endianess

void CScsiCmd10::HostToScsi(void)
{
	m_dwLba   = HostToMotor(m_dwLba);

	m_wLength = HostToMotor(m_wLength); 
	}

void CScsiCmd10::ScsiToHost(void)
{
	m_dwLba   = MotorToHost(m_dwLba);

	m_wLength = MotorToHost(m_wLength); 
	}

// Operations

void CScsiCmd10::Init(void)
{
	memset(this, 0, sizeof(ScsiCmd));
	}

void CScsiCmd10::Init(BYTE bOpcode)
{
	Init(bOpcode, 0, 0, 0);
	}

void CScsiCmd10::Init(BYTE bOpcode, BYTE bFlags)
{
	Init(bOpcode, bFlags, 0, 0);
	}

void CScsiCmd10::Init(BYTE bOpcode, BYTE bFlags, WORD wLength)
{
	Init(bOpcode, bFlags, wLength, 0);
	}

void CScsiCmd10::Init(BYTE bOpcode, BYTE bFlags, WORD wLength, DWORD dwLBA)
{
	Init();

	m_bOpcode = bOpcode;

	m_bFlags  = bFlags;

	m_wLength = wLength;

	m_dwLba   = dwLBA;
	}

// End of File
