
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbConfigDesc_HPP

#define	INCLUDE_UsbConfigDesc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Configuration Descriptor Wrapper
//

class CUsbConfigDesc : public UsbConfigDesc
{
	public:
		// Constructor
		CUsbConfigDesc(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Init
		void Init(void);

		// Debug
		void Debug(void);
	};

// End of File

#endif
