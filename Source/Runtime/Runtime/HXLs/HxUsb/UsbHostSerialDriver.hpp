
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostSerialDriver_HPP

#define	INCLUDE_UsbHostSerialDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostModuleDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Serial Driver
//

class CUsbHostSerialDriver : public CUsbHostModuleDriver, public IUsbHostSerial
{
	public:
		// Constructor
		CUsbHostSerialDriver(void);

		// Destructor
		~CUsbHostSerialDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostModule
		BOOL METHOD Reset(void);
		BOOL METHOD ReadVersion(BYTE Version[16]); 
		BOOL METHOD SendHeartbeat(void);
		
		// IUsbHostSerial
		BOOL METHOD SetRun(BOOL fRun);
		BOOL METHOD SetPhysical(UINT uPhysical);
		BOOL METHOD SetBaud(UINT uBaud);
		BOOL METHOD SetFormat(UINT uData, UINT uStop, UINT Parity);
		BOOL METHOD SetFlags(UINT uFlags);
		BOOL METHOD SetLatency(UINT uLatency);
		BOOL METHOD SendData(PCBYTE pData, UINT uCount);
		UINT METHOD RecvData(PBYTE  pData, UINT uCount);
		BOOL METHOD SendDataAsync(PCBYTE pData, UINT uCount);
		UINT METHOD RecvDataAsync(PBYTE pData, UINT uCount);
		UINT METHOD WaitAsyncSend(UINT uTimeout);
		UINT METHOD WaitAsyncRecv(UINT uTimeout);
		BOOL METHOD KillAsyncSend(void);
		BOOL METHOD KillAsyncRecv(void);
		
	protected:
		// Commands
		enum
		{
			cmdSetRun	= 0x01,
			cmdSetPhysical	= 0x02,
			cmdSetBaud	= 0x03,
			cmdSetFormat	= 0x04,
			cmdSetFlags	= 0x05,
			};
	};

// End of File

#endif
