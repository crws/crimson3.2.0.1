
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubDriver_HPP

#define	INCLUDE_UsbHubDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUsbDescList;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Driver
//

class CUsbHubDriver : public CUsbHostFuncDriver, public IUsbHubDriver
{
	public:
		// Constructor
		CUsbHubDriver(void);

		// Destructor
		~CUsbHubDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IHubDriver
		BOOL METHOD Bind(IUsbHubEvents *pSink);
		BOOL METHOD SetLedMap(UsbHubLedMap const &Map);
		BOOL METHOD ResetPort(WORD wPort);
		UINT METHOD GetSpeed(WORD wPort);
		BOOL METHOD GetDescriptor(PBYTE pData, UINT uLen);
		BOOL METHOD GetStatus(WORD &wStatus, WORD &wChange);
		BOOL METHOD GetPortStatus(WORD wPort, WORD &wStatus, WORD &wChange);
		BOOL METHOD SetFeature(WORD wFeature);
		BOOL METHOD ClrFeature(WORD wFeature);
		BOOL METHOD SetPortFeature(WORD wPort, WORD wFeature);
		BOOL METHOD ClrPortFeature(WORD wPort, WORD wFeature);
		BOOL METHOD EnableMultiTTMode(void);
		BOOL METHOD HasMultiTT(void);

	protected:
		// Constants
		enum
		{
			constTimerOff	  = 0,
			constTimerPort	  = 50,
			constTimerAttach  = 100, 
			constTimerRestart = 1000,
			constTimerCurrent = 10000,
			constTimerPoll	  = 5000,
			constTimerDog     = 10000,
			};

		// Port State
		enum
		{
			stateInit,
			stateEnabled,
			stateAttached,
			statePowered,
			stateReset,
			stateConnected,
			stateOverCurrent,
			stateRestart,
			};

		// Flags
		enum
		{	
			flagReqReset	= Bit(0),
			flagLedExternal = Bit(1),
			};

		// Port Context
		struct CPort
		{
			UINT         m_uState;
			UINT	     m_uTimer1;
			UINT	     m_uTimer2;
			UINT         m_uSpeed;
			UINT	     m_uFlags;
			WORD         m_wStatus;
			IUsbDevice * m_pDev;
			};

		// Data
		IUsbPipe            * m_pCtrl;
		IUsbPipe            * m_pBulk;
		UINT                  m_iBulk;
		UsbHubDesc 	      m_HubDesc;
		CPort               * m_pPorts;
		IUsbHubEvents       * m_pNotify;
		bool		      m_fWait;
		BYTE 		      m_bEvent;
		UINT 		      m_uLock;
		UINT                  m_uTimer;
		bool		      m_fMultiTT;
		UINT		      m_uFlags;
		UsbHubLedMap          m_LedMap;
		ILeds              *  m_pLeds;

		// Feature Helpers
		BOOL ClrPortEnable(WORD wPort);
		BOOL ClrPortSuspend(WORD wPort);
		BOOL ClrPortPower(WORD wPort);
		BOOL ClrIndicator(WORD wPort);
		BOOL ClrPortEnableChange(WORD wPort);
		BOOL ClrPortConnectChange(WORD wPort);
		BOOL ClrPortCurrentChange(WORD wPort);
		BOOL ClrPortResetChange(WORD wPort);
		BOOL SetPortReset(WORD wPort);
		BOOL SetPortSuspend(WORD wPort);
		BOOL SetPortPower(WORD wPort);
		BOOL SetPortIndicator(WORD wPort);

		// Device
		void OnDeviceArrival(UINT iPort);
		void OnDeviceRemoval(UINT iPort);
		
		// Port Events
		void OnPortConnect(UINT iPort);
		void OnPortReset(UINT iPort, WORD wStatus);
		void OnPortRemoval(UINT iPort);
		void OnPortCurrent(UINT iPort);
		
		// Ports
		void MakePorts(void);
		void PollEvents(UINT uLapsed);
		void PollPorts(UINT uLapsed);
		void CheckPort(UINT iPort);
		void FreePorts(void);
		void FreeLocks(void);
		void FreeLock(UINT iPort);
		void KillComms(void);
		void LedStatus(UINT iPort);

		// Hub
		void PollPoll(UINT uLapsed);
		BOOL CtrlTrans(UsbDeviceReq &Req);
		UINT CtrlTrans(UsbDeviceReq &Req, PBYTE pData, UINT uLen);

		// Helpers
		BOOL FindPipes(CUsbDescList const &List);
		BOOL EnableMultiTT(CUsbDescList const &List);
		BOOL CheckTimer(UINT &uTimer, UINT uLapsed);
		PTXT GetStateText(UINT uState) const;
	};

// End of File

#endif
