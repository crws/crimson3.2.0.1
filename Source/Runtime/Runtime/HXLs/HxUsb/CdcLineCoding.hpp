
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CdcLineCoding_HPP

#define	INCLUDE_CdcLineCoding_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cdc Framework
//

#include "Cdc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cdc Line Coding
//

class CCdcLineCoding : public CdcLineCoding
{
	public:
		// Constructor
		CCdcLineCoding(void);

		// Endianess
		void HostToCdc(void);
		void CdcToHost(void);

		// Operations
		void Init(void);
		void Init(CSerialConfig const &Config);
		void Get(CSerialConfig &Config);
	};

// End of File

#endif
