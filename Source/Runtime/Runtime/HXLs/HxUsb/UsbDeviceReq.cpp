
#include "Intern.hpp"

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Request
//

// Constructor

CUsbDeviceReq::CUsbDeviceReq(void)
{
	Init();
	} 

// Endianess

void CUsbDeviceReq::HostToUsb(void)
{
	m_wValue  = HostToIntel(m_wValue);

	m_wIndex  = HostToIntel(m_wIndex);

	m_wLength = HostToIntel(m_wLength);
	}

void CUsbDeviceReq::UsbToHost(void)
{
	m_wValue  = IntelToHost(m_wValue);

	m_wIndex  = IntelToHost(m_wIndex);

	m_wLength = IntelToHost(m_wLength);
	}

// Init

void CUsbDeviceReq::Init(void)
{
	memset(this, 0, sizeof(UsbDeviceReq));
	}

// Data Access

PBYTE CUsbDeviceReq::GetData(void) const
{
	return PBYTE(this);
	}

UINT CUsbDeviceReq::GetSize(void) const
{
	return sizeof(UsbDeviceReq);
	}

// Debug

void CUsbDeviceReq::Debug(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("Usb Device Request 0x%8.8X\n", this);

	AfxTrace("Type       : 0x%2.2X\n", m_Type);
	AfxTrace("Recipient  : 0x%2.2X\n", m_Recipient);
	AfxTrace("Request    : 0x%2.2X\n", m_bRequest);
	AfxTrace("Value      : 0x%4.4X\n", m_wValue);
	AfxTrace("Index      : 0x%4.4X\n", m_wIndex);
	AfxTrace("Length     : 0x%4.4X\n", m_wLength);
	AfxTrace("Direction  : %s\n",      m_Direction ? "Device -> Host" : "Host -> Device"); 

	#endif
	}

// End of File
