
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostHardwareDriver_HPP

#define	INCLUDE_UsbHostHardwareDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDriver.hpp"

#include "UsbHostAddress.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Hardware Driver
//

class CUsbHostHardwareDriver : public CUsbDriver, public IUsbHostInterfaceDriver, public IUsbHostHardwareEvents
{
	public:
		// Constructor
		CUsbHostHardwareDriver(void);

		// Destructor
		~CUsbHostHardwareDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pDriver);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);
		
		// IUsbHostInterfaceDriver
		UINT  METHOD GetType(void);
		UINT  METHOD GetIndex(void);
		UINT  METHOD GetPortCount(void);
		BOOL  METHOD GetPortConnect(UINT iPort);
		BOOL  METHOD GetPortEnabled(UINT iPort);
		BOOL  METHOD GetPortCurrent(UINT iPort);
		BOOL  METHOD GetPortSuspend(UINT iPort);
		BOOL  METHOD GetPortReset(UINT iPort);
		BOOL  METHOD GetPortPower(UINT iPort);
		UINT  METHOD GetPortSpeed(UINT iPort);
		void  METHOD SetIndex(UINT iIndex);
		void  METHOD SetPortEnable(UINT iPort, BOOL fSet);
		void  METHOD SetPortSuspend(UINT iPort, BOOL fset);
		void  METHOD SetPortResume(UINT iPort, BOOL fSet);
		void  METHOD SetPortReset(UINT iPort, BOOL fSet);
		void  METHOD SetPortPower(UINT iPort, BOOL fSet);
		void  METHOD EnableEvents(BOOL fEnable);
		BOOL  METHOD SetEndptDevAddr(DWORD iEndpt, BYTE bAddr);
		UINT  METHOD GetEndptDevAddr(DWORD iEndpt);
		BOOL  METHOD SetEndptAddr(DWORD iEndpt, BYTE bAddr);
		BOOL  METHOD SetEndptMax(DWORD iEndpt, WORD wMaxPacket);
		BOOL  METHOD SetEndptHub(DWORD iEndpt, BYTE bHub, BYTE bPort);
		DWORD METHOD MakeDevice(IUsbDevice *pDevice);
		void  METHOD KillDevice(DWORD iDev);
		BOOL  METHOD CheckConfig(DWORD iDev);
		DWORD METHOD MakeEndptCtrl(DWORD iDev, UINT uSpeed);
		DWORD METHOD MakeEndptAsync(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc);
		DWORD METHOD MakeEndptInt(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc);
		DWORD METHOD MakeEndptIso(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc);
		BOOL  METHOD ResetEndpt(DWORD iEndpt);
		BOOL  METHOD KillEndpt(DWORD iEndpt);
		BOOL  METHOD Transfer(UsbIor &Urb);
		BOOL  METHOD AbortPending(DWORD iEndpt);
		UINT  METHOD AllocAddress(void);
		void  METHOD FreeAddress(UINT uAddr);
		BOOL  METHOD LockDefAddr(BOOL fLock);

		// IUsbEvents
		BOOL METHOD GetDriverInterface(IUsbDriver *&pDriver);
		void METHOD OnBind(IUsbDriver *pDriver);
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);
		
		// IUsbHostHardwareEvents
		void METHOD OnEvent(void);

	protected:
		// Data
		IUsbHostInterfaceEvents * m_pUpperDrv;
		IUsbHostHardwareDriver  * m_pLowerDrv;
		UINT                      m_iIndex;
		CUsbHostAddress		  m_Address;

		// Implementation
		void InitAddress(void);

		// Memory Helpers
		PVOID AllocNonCached(UINT uSize);
		PVOID AllocNonCached(UINT uSize, UINT uAlign);
		PVOID AllocNonCachedInit(UINT uSize);
		PVOID AllocNonCachedInit(UINT uSize, UINT uAlign);
		void  FreeNonCached(PVOID pMem);
		DWORD VirtualToPhysical(PCVOID p);
		DWORD VirtualToPhysical(DWORD d);
		PVOID PhysicalToVirtual(PVOID);
		PVOID PhysicalToVirtual(DWORD d);
		PVOID PhysicalToVirtual(PVOID p, bool fCached);
		PVOID PhysicalToVirtual(DWORD d, bool fCached);
	};

// End of File

#endif
