
#include "Intern.hpp"

#include "UsbGetStatusReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Status Standard Device Request
//

// Constructor

CUsbGetStatusReq::CUsbGetStatusReq(void)
{
	Init();
	} 

// Operations

void CUsbGetStatusReq::Init(void)
{
	CUsbStandardReq::Init();

	m_bRequest = reqGetStatus;

	m_wLength  = 2;

	m_Direction = dirDevToHost;
	}

void CUsbGetStatusReq::SetDevice(void)
{
	m_Recipient = recDevice;
	}

void CUsbGetStatusReq::SetInterface(WORD wInterface)
{
	m_Recipient = recInterface;

	m_wIndex    = wInterface;
	}

void CUsbGetStatusReq::SetEndpoint(WORD wEndpoint)
{
	m_Recipient = recEndpoint;

	m_wIndex    = wEndpoint;
	}

// End of File
