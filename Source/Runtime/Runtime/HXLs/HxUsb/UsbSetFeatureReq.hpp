
//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSetFeatureReq_HPP

#define	INCLUDE_UsbSetFeatureReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClearFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Feature Device Requeset
//

class CUsbSetFeatureReq : public CUsbClearFeatureReq
{
	public:
		// Constructor
		CUsbSetFeatureReq(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
