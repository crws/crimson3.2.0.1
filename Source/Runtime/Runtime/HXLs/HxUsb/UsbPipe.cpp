
#include "Intern.hpp"  

#include "UsbPipe.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDevice.hpp"

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Pipe
//

// Instantiator

global IUsbPipe * Create_UsbPipe(void)
{
	CUsbPipe *p = New CUsbPipe();

	return (IUsbPipe *) p;
	}

// Constructor

CUsbPipe::CUsbPipe(void)
{
	StdSetRef();

	m_iEndpt   = NOTHING;

	m_pDevice  = NULL;

	m_pStack   = NULL;

	m_pSink    = NULL;

	m_uError   = 0;

	m_bAddr    = 0;

	m_iApcHead = 0;

	m_iApcTail = 0;

	m_pDone    = Create_ManualEvent();
	
	m_pMutex   = Create_Mutex();
	}

// Destructor

CUsbPipe::~CUsbPipe(void)
{
	TermApc();

	m_pDone->Release();
	
	m_pMutex->Release();
	}

// IUnknown

HRESULT CUsbPipe::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbPipe);

	StdQueryInterface(IUsbPipe);

	StdQueryInterface(IUsbPipeEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbPipe::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbPipe::Release(void)
{
	StdRelease();
	}

// IPipe

void CUsbPipe::SetDevice(IUsbDevice *pDev)
{
	m_pDevice = pDev;

	m_pStack  = pDev->GetHostStack();
	}

IUsbDevice * CUsbPipe::GetDevice(void)
{
	return m_pDevice;
	}

void CUsbPipe::SetEndpoint(DWORD iEndpt)
{
	m_iEndpt = iEndpt;
	}

DWORD CUsbPipe::GetEndpoint(void)
{
	return m_iEndpt;
	}

void CUsbPipe::SetDirIn(BOOL fIn)
{
	m_fIn = fIn;
	}

BOOL CUsbPipe::GetDirIn(void)
{
	return m_fIn;
	}

void CUsbPipe::SetEndpointAddr(BYTE bAddr)
{
	m_bAddr = bAddr;
	}

BYTE CUsbPipe::GetEndpointAddr(void)
{
	return m_bAddr;
	}

BYTE CUsbPipe::GetFeatureAddr(void)
{
	return m_bAddr | (m_fIn ? Bit(7) : 0);
	}

void CUsbPipe::SetMaxPacket(WORD wMaxPacket)
{
	if( m_pDevice ) {
		
		m_pDevice->GetHostI()->SetEndptMax(m_iEndpt, wMaxPacket);
		}
	}

void CUsbPipe::Claim(void)
{
	GuardThread(true);

	m_pMutex->Wait(FOREVER);
	}

void CUsbPipe::Free(void)
{
	m_pMutex->Free();

	GuardThread(false);
	}

BOOL CUsbPipe::SendSetup(PBYTE pData, UINT uCount, UINT uFlags)
{
	uFlags |= (UsbIor::flagSetup | UsbIor::flagData0 | UsbIor::flagControl | UsbIor::flagQueue);
	
	return Transfer(pData, uCount, uFlags);
	}

BOOL CUsbPipe::SendSetupData(PBYTE pData, UINT uCount)
{
	UINT uFlags = UsbIor::flagOut | UsbIor::flagData1 | UsbIor::flagControl | UsbIor::flagQueue;

	return Transfer(pData, uCount, uFlags);
	}

UINT CUsbPipe::RecvSetupData(PBYTE pData, UINT uCount)
{
	UINT uFlags = UsbIor::flagIn | UsbIor::flagData1 | UsbIor::flagShortOk | UsbIor::flagControl | UsbIor::flagQueue;

	return Transfer(pData, uCount, uFlags) ? m_uCount : NOTHING;
	}

BOOL CUsbPipe::SendStatus(void)
{
	UINT uFlags = UsbIor::flagOut | UsbIor::flagData1 | UsbIor::flagControl | UsbIor::flagStatusStage;

	return Transfer(NULL, 0, uFlags);
	}

BOOL CUsbPipe::RecvStatus(void)
{
	UINT uFlags = UsbIor::flagIn | UsbIor::flagData1 | UsbIor::flagControl | UsbIor::flagStatusStage;

	return Transfer(NULL, 0, uFlags);
	}

BOOL CUsbPipe::SendBulk(PBYTE pData, UINT uCount, BOOL fAsync)
{
	UINT uFlags = UsbIor::flagOut;

	if( fAsync ) {

		uFlags |= UsbIor::flagAsync;

		if( m_pSink ) {

			uFlags |= UsbIor::flagCompletion;
			}
		}

	return Transfer(pData, uCount, uFlags);
	}

UINT CUsbPipe::RecvBulk(PBYTE pData, UINT uCount, BOOL fAsync)
{
	UINT uFlags = UsbIor::flagIn | UsbIor::flagShortOk;

	if( fAsync ) {

		uFlags |= UsbIor::flagAsync;

		if( m_pSink ) {

			uFlags |= UsbIor::flagCompletion;
			}
		}

	if( Transfer(pData, uCount, uFlags) ) {
		
		return fAsync ? 1 : m_uCount;
		}
	
	return NOTHING;
	}

BOOL CUsbPipe::WaitAsync(UINT uTimeout, BOOL &fOk, UINT &uCount)
{
	if( m_pDone->Wait(uTimeout) ) {	

		fOk     = m_uStatus == UsbIor::statPassed;

		uCount  = m_uCount;

		return true;
		}

	return false;
	}

BOOL CUsbPipe::KillAsync(void)
{	
	TermApc();

	if( m_pDevice ) {

		return m_pDevice->GetHostI()->AbortPending(m_iEndpt);
		}
	
	return false;
	}

BOOL CUsbPipe::CtrlTrans(UsbDeviceReq &Req)
{
	Claim();

	CUsbDeviceReq const &r = (CUsbDeviceReq &) Req;

	if( SendSetup(r.GetData(), r.GetSize(), UsbIor::flagNoDataStage) ) {

		bool fOk = RecvStatus();

		Free();

		return fOk;
		}

	Free();

	return false;
	}

UINT CUsbPipe::CtrlTrans(UsbDeviceReq &Req, PBYTE pData, UINT uLen)
{
	Claim();

	CUsbDeviceReq const &r = (CUsbDeviceReq &) Req;

	if( r.m_Direction == dirDevToHost ) {
		
		if( SendSetup(r.GetData(), r.GetSize(), UsbIor::flagInDataStage ) ) {

			uLen = RecvSetupData(pData, uLen);

			if( uLen != NOTHING ) {

				if( SendStatus() ) {

					Free();

					return uLen;
					}
				}
			}
		}
	else {
		if( SendSetup(r.GetData(), r.GetSize(), UsbIor::flagOutDataStage ) ) {
		
			if( SendSetupData(pData, uLen) ) {

				if( RecvStatus() ) {

					Free();

					return uLen;
					}
				}
			}
		}

	Free();
	
	return NOTHING;
	}

BOOL CUsbPipe::IsHalted(void)
{
	return m_uStatus == UsbIor::statHalted;
	}

BOOL CUsbPipe::ClearStall(void)
{
	/*AfxTrace("CUsbPipe::ClearStall Addr=%d, Endpoint=%2.2X\n", m_pDevice->GetAddr(), GetFeatureAddr());*/

	if( !m_pDevice->IsRemoved() ) {

		ClearHaltCondition();

		ResetEndpoint();

		return true;
		}

	return false;
	}

void CUsbPipe::RequestCompletion(IUsbHostFuncEvents *pSink)
{
	m_pSink = pSink;
	}

// IPipeEvents

void CUsbPipe::OnTransfer(UsbIor &Urb)
{
	m_uCount += Urb.m_uCount;

	m_uStatus = Urb.m_uStatus;

	m_pDone->Set();

	if( Urb.m_uFlags & UsbIor::flagCompletion ) {

		if( m_pSink ) {

			m_pSink->OnData();
			}
		}

	CheckError(Urb);

	m_pStack->FreeUrb(&Urb);
	}

void CUsbPipe::OnPoll(UINT uLapsed)
{
	ExecApc();
	}

// Comms

void CUsbPipe::TermApc(void)
{
	while( m_iApcHead != m_iApcTail ) {

		UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

		UsbIor *pUrb = m_pApcList[m_iApcTail];

		m_iApcTail = (m_iApcTail + 1) % elements(m_pApcList);

		Hal_LowerIrql(uSave);

		if( pUrb->m_uFlags & UsbIor::flagCommand ) {

			m_pStack->FreeUrb(pUrb);
			}
		else {
			pUrb->m_uStatus = UsbIor::statFailed;

			OnTransfer(*pUrb);
			}
		}
	}

void CUsbPipe::ExecApc(void)
{
	while( m_iApcHead != m_iApcTail ) {

		UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

		UsbIor *pUrb = m_pApcList[m_iApcTail];

		m_iApcTail = (m_iApcTail + 1) % elements(m_pApcList);

		Hal_LowerIrql(uSave);

		if( pUrb->m_uFlags & UsbIor::flagCommand ) {

			ExecCmd(UINT(pUrb->m_pData));

			m_pStack->FreeUrb(pUrb);

			continue;
			}

		if( !m_pStack->Transfer(this, pUrb) ) {

			pUrb->m_uStatus = UsbIor::statFailed;

			OnTransfer(*pUrb);
			}
		}
	}

BOOL CUsbPipe::ExecCmd(UINT uCommand)
{
	switch( uCommand ) {

		case cmdClearStall:

			m_uStatus = UsbIor::statFailed;
				
			return ClearStall();
		}

	return false;
	}

BOOL CUsbPipe::QueueApc(UINT uCommand)
{
	return QueueApc(PBYTE(uCommand), 0, UsbIor::flagCommand);
	}

BOOL CUsbPipe::QueueApc(PBYTE pData, UINT uCount, UINT uFlags)
{
	GuardThread(true);

	UsbIor *pUrb = m_pStack->AllocUrb();

	if( pUrb ) {

		pUrb->m_pData  = pData;

		pUrb->m_uCount = uCount;

		pUrb->m_iEndpt = m_iEndpt;

		pUrb->m_uFlags = uFlags;

		m_uCount       = 0;

		m_pDone->Clear();

		UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

		UINT uNext = (m_iApcHead + 1) % elements(m_pApcList);

		if( uNext != m_iApcTail ) {

			m_pApcList[m_iApcHead] = pUrb;
	
			m_iApcHead = uNext;

			Hal_LowerIrql(uSave);

			GuardThread(false);

			return true;
			}

		Hal_LowerIrql(uSave);

		m_pStack->FreeUrb(pUrb);
		}

	GuardThread(false);

	return false;
	}

BOOL CUsbPipe::Transfer(PBYTE pData, UINT uCount, UINT uFlags)	
{
	if( !m_pDevice || !m_pStack ) {

		/*AfxTrace("CUsbPipe[%8.8X]::Transfer - Invalid State.\n", m_iEndpt);*/

		return false;
		}

	if( m_pDevice->IsRemoved() || m_pDevice->HasError() ) {

		/*AfxTrace("CUsbPipe[%8.8X]::Transfer - Device Removed or Remove Pending.\n", m_iEndpt);*/

		return false;
		}

	if( !(uFlags & UsbIor::flagControl) ) {

		if( IsHalted() ) {

			if( uFlags & UsbIor::flagAsync ) {

				if( !CanWait() ) {

					QueueApc(cmdClearStall);

					QueueApc(pData, uCount, uFlags);

					return true;
					}
				}

			ClearStall();

			m_uStatus = UsbIor::statFailed;
			}
		}
	else {
		if( m_uStatus == UsbIor::statStall ) {

			ResetEndpoint();
			}
		}

	GuardThread(true);

	UsbIor *pUrb = m_pStack->AllocUrb();

	if( pUrb ) {

		pUrb->m_pData  = pData;

		pUrb->m_uCount = uCount;

		pUrb->m_iEndpt = m_iEndpt;

		pUrb->m_uFlags = uFlags;

		m_uCount       = 0;

		m_pDone->Clear();

		if( m_pStack->Transfer(this, pUrb) ) {

			GuardThread(false);

			if( uFlags & UsbIor::flagAsync ) {

				return true;
				}

			UINT uTimeout = (uFlags & UsbIor::flagControl) ? 1000 : FOREVER;

			if( m_pDone->Wait(uTimeout) ) {

				if( m_uStatus == UsbIor::statPassed ) {

					if( uCount == m_uCount ) {

						return true;
						}

					if( uFlags & UsbIor::flagShortOk ) {

						return true;
						}

					/*AfxTrace("CUsbPipe[%8.8X]::Transfer Req=%d, Act=%d, Flags=0x%8.8X - FAILED.\n", m_iEndpt, uCount, m_uCount, uFlags);*/

					return false;
					}

				/*AfxTrace("CUsbPipe[%8.8X]::Transfer - FAILED Error Code=%d.\n", m_iEndpt, m_uStatus);*/

				return false;
				}

			/*AfxTrace("CUsbPipe[%8.8X]::Transfer - Timeout Addr %d.\n", m_iEndpt, m_pDevice->GetAddr());*/

			KillAsync();

			SetError();

			return false;
			}

		/*AfxTrace("CUsbPipe[%8.8X]::Transfer - Rejected.\n", m_iEndpt);*/

		CheckError();

		m_pStack->FreeUrb(pUrb);
		}

	return false;
	}

void CUsbPipe::ClearHaltCondition(void)
{
	m_pStack->ClearEndFeature(m_pDevice->GetCtrlPipe(), 0, GetFeatureAddr());
	}

void CUsbPipe::ResetEndpoint(void)
{
	m_pDevice->GetHostI()->ResetEndpt(m_iEndpt);
	}

BOOL CUsbPipe::CanWait(void)
{
	if( Hal_GetIrql() >= IRQL_DISPATCH ) {

		return false;
		}

	if( !GetCurrentThread() ) {

		return false;
		}

	if( GetCurrentThread()->GetIndex() <= 1 ) {

		return false;
		}

	return true;
	}

void CUsbPipe::SetError(void)
{
	if( m_pDevice ) {

		m_pDevice->SetError();
		}
	}

void CUsbPipe::CheckError(void)
{
	if( ++m_uError > 3 ) {

		/*AfxTrace("CUsbPipe[%8.8X]::CheckError - Reset Device Repeated Error.\n", m_iEndpt);*/

		SetError();
		}
	}

void CUsbPipe::CheckError(UsbIor const &Urb)
{
	switch( Urb.m_uStatus ) {

		case UsbIor::statPassed:

			m_uError = 0;

			break;

		case UsbIor::statFatal:

			/*AfxTrace("CUsbPipe[%8.8X]::CheckError - Reset Device Fatal Error.\n", m_iEndpt);*/

			SetError();

			break;

		case UsbIor::statFailed:

			if( Urb.m_uFlags & UsbIor::flagControl ) {

				/*AfxTrace("CUsbPipe[%8.8X]::CheckError - Reset Device Control Endpoint.\n", m_iEndpt);*/

				SetError();

				break;
				}

			if( ++m_uError > 3 ) {

				/*AfxTrace("CUsbPipe[%8.8X]::CheckError - Reset Device Repeated Error.\n", m_iEndpt);*/

				SetError();

				break;
				}

			break;

		case UsbIor::statStall:

			if( ++m_uError > 1 ) {

				/*AfxTrace("CUsbPipe[%8.8X]::CheckError(STALL) - Reset Device Repeated Error.\n", m_iEndpt);*/

				SetError();

				break;
				}

			break;

		case UsbIor::statHalted:

			if( Urb.m_uFlags & UsbIor::flagControl ) {

				break;
				}

			if( ++m_uError > 3 ) {

				/*AfxTrace("CUsbPipe[%8.8X]::CheckError(HALT) - Reset Device Repeated Error.\n", m_iEndpt);*/

				SetError();

				break;
				}

			break;
		}
	}

// End of File
