
#include "Intern.hpp"  

#include "UsbHostHidDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDescList.hpp"

#include "UsbGetDescriptorReq.hpp"

#include "UsbClassReq.hpp"

#include "UsbHidDesc.hpp"

#include "UsbHidReport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mapping Ojbects
//

extern IUsbHidMapper * Create_HidKeyboardMapper(void);

extern IUsbHidMapper * Create_HidMouseMapper(void);

//////////////////////////////////////////////////////////////////////////
//
// Hid Driver
//

// Instantiator

IUsbHostHid * Create_HidDriver(void)
{
	IUsbHostHid *p = New CUsbHostHidDriver;

	return p;
	}

// Constructor

CUsbHostHidDriver::CUsbHostHidDriver(void)
{
	m_pName	      = "Hid Driver";

	m_Debug	      = debugWarn;

	m_pReportData = NULL;
	
	m_uReportSize = 0;

	m_pMapper     = NULL;

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostHidDriver::~CUsbHostHidDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");

	FreeReport();

	FreeMapper();
	}

// IUnknown

HRESULT CUsbHostHidDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostHid);

	return CUsbHostFuncDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostHidDriver::AddRef(void)
{
	return CUsbHostFuncDriver::AddRef();
	}

ULONG CUsbHostHidDriver::Release(void)
{
	return CUsbHostFuncDriver::Release();
	}

// IHostFuncDriver

void CUsbHostHidDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncDriver::Bind(pDevice, iInterface);
	}

void CUsbHostHidDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncDriver::Bind(pSink);
	}

void CUsbHostHidDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncDriver::GetDevice(pDev);
	}

BOOL CUsbHostHidDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostHidDriver::GetVendor(void)
{
	return CUsbHostFuncDriver::GetVendor();
	}

UINT CUsbHostHidDriver::GetProduct(void)
{
	return CUsbHostFuncDriver::GetProduct();
	}

UINT CUsbHostHidDriver::GetClass(void)
{
	return devHid;
	}

UINT CUsbHostHidDriver::GetSubClass(void)
{
	return classBoot;
	}

UINT CUsbHostHidDriver::GetInterface(void)
{
	return CUsbHostFuncDriver::GetInterface();
	}

UINT CUsbHostHidDriver::GetProtocol(void)
{
	return protReport;
	}

BOOL CUsbHostHidDriver::GetActive(void)
{
	return CUsbHostFuncDriver::GetActive();
	}

BOOL CUsbHostHidDriver::Open(CUsbDescList const &List)
{
	Trace(debugInfo, "Open");

	if( FindPipes(List) && FindConfig(List) ) {	

		return CUsbHostFuncDriver::Open();
		}

	return false;
	}

BOOL CUsbHostHidDriver::Close(void)
{
	Trace(debugInfo, "Close");

	return CUsbHostFuncDriver::Close();
	}

void CUsbHostHidDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncDriver::Poll(uLapsed);

	if( m_pMapper ) {

		m_pMapper->Poll();
		}
	}

// IUsbHostHid

UINT CUsbHostHidDriver::GetUsagePage(void)
{
	return m_pMapper ? m_pMapper->GetUsagePage() : NOTHING;
	}

UINT CUsbHostHidDriver::GetUsageType(void)
{
	return m_pMapper ? m_pMapper->GetUsageType() : NOTHING;
	}

BOOL CUsbHostHidDriver::GetReportData(PCBYTE &pData, UINT &uSize)
{
	if( m_pReportData ) {	

		pData = m_pReportData;

		uSize = m_uReportSize;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbHostHidDriver::GetReportDesc(PBYTE pData, UINT uSize)
{
	CUsbGetDescriptorReq Req;

	Req.m_Recipient = recInterface;

	Req.m_wValue    = MAKEWORD(0, descReport);

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = uSize; 

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, pData, uSize);
	}

BOOL CUsbHostHidDriver::GetReport(WORD wType, WORD wId, PBYTE pData, UINT uSize)
{
	Trace(debugCmds, "GetReport");

	CUsbClassReq Req;

	Req.m_Recipient = recInterface;

	Req.m_bRequest  = reqGetReport;

	Req.m_Direction = dirDevToHost;
	
	Req.m_wValue    = MAKEWORD(wId, wType);

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = uSize;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, pData, uSize);
	}

BOOL CUsbHostHidDriver::SetReport(WORD wType, WORD wId, PBYTE pData, UINT uSize)
{
	Trace(debugCmds, "SetReport(Type=%d, Id=%d, Len=%d)", wType, wId, uSize);

	CUsbClassReq Req;

	Req.m_Recipient = recInterface;

	Req.m_bRequest  = reqSetReport;

	Req.m_Direction = dirHostToDev;
	
	Req.m_wValue    = MAKEWORD(wId, wType);

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = uSize;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, pData, uSize);
	}

BOOL CUsbHostHidDriver::GetIdle(BYTE &bIdle)
{
	Trace(debugCmds, "GetIdle");

	CUsbClassReq Req;

	Req.m_Recipient = recInterface;

	Req.m_bRequest  = reqGetIdle;

	Req.m_Direction = dirDevToHost;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(bIdle);

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, &bIdle, sizeof(bIdle));
	}

BOOL CUsbHostHidDriver::SetIdle(BYTE bIdle)
{
	Trace(debugCmds, "SetIdle(%d)", bIdle);

	CUsbClassReq Req;

	Req.m_Recipient = recInterface;

	Req.m_bRequest  = reqSetIdle;

	Req.m_Direction = dirHostToDev;

	Req.m_wIndex    = m_iInt;

	Req.m_wValue    = MAKEWORD(0, bIdle);
	
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostHidDriver::GetProtocol(BYTE &bProtocol)
{
	Trace(debugCmds, "GetProtocol");

	CUsbClassReq Req;

	Req.m_Recipient = recInterface;

	Req.m_bRequest  = reqGetProtocol;

	Req.m_Direction = dirDevToHost;

	Req.m_wIndex    = m_iInt;

	Req.m_wLength   = sizeof(bProtocol);

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req, &bProtocol, sizeof(bProtocol));
	}

BOOL CUsbHostHidDriver::SetProtocol(BYTE bProtocol)
{
	Trace(debugCmds, "SetProtocol(%d)", bProtocol);

	CUsbClassReq Req;

	Req.m_Recipient = recInterface;

	Req.m_bRequest  = reqSetProtocol;

	Req.m_Direction = dirHostToDev;

	Req.m_wIndex    = m_iInt;

	Req.m_wValue    = bProtocol;
	
	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostHidDriver::RecvReport(PBYTE pData, UINT uCount)
{
	return m_pRecv->RecvBulk(pData, uCount, true);
	}

UINT CUsbHostHidDriver::WaitReport(UINT uTimeout)
{
	BOOL fOk;

	UINT uCount;

	if( m_pRecv->WaitAsync(uTimeout, fOk, uCount) ) {
		
		return fOk ? uCount : 0;
		}

	return NOTHING;
	}

void CUsbHostHidDriver::SetConfig(PCBYTE pConfig, UINT uSize)
{
	if( m_pMapper ) {

		m_pMapper->SetConfig(pConfig, uSize);
		}
	}

// Implementation

bool CUsbHostHidDriver::FindPipes(CUsbDescList const &List)
{
	UINT iIndex = List.GetIndexStart();
	
	UsbInterfaceDesc *pInt = (UsbInterfaceDesc *) List.EnumInterface(iIndex);

	if( pInt && pInt->m_bEndpoints >= 1 ) {

		UsbEndpointDesc *pEp = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		while( pEp ) {

			if( pEp->m_bDirIn ) {

				m_pCtrl = m_pDev->GetCtrlPipe();

				m_pRecv = m_pDev->GetPipe(pEp->m_bAddr, pEp->m_bDirIn);

				m_iRecv = pEp->m_bAddr | Bit(7);

				return m_pCtrl != NULL && m_pRecv != NULL;
				}

			pEp = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);
			}
		}
	
	return false;
	}

bool CUsbHostHidDriver::FindConfig(CUsbDescList const &List)
{
	UINT iIndex = List.GetIndexStart();
	
	CUsbHidDesc *pDesc = (CUsbHidDesc *) List.Enum(descHid, iIndex);

	if( pDesc ) {

		pDesc->UsbToHost();
		
		pDesc->Debug();

		if( pDesc->m_List[0].m_bType == descReport ) {

			AllocReport(pDesc->m_List[0].m_wSize);

			if( GetReportDesc(m_pReportData, m_uReportSize) ) {
			
				ParseReport();

				if( !StartMapper() ) {

					FreeMapper();

					return false;
					}
			
				return true;
				}
			}
		}

	return false;
	}

void CUsbHostHidDriver::ParseReport(void)
{
	CUsbHidReport Rep;
		
	Rep.Attach(m_pReportData, m_uReportSize);

	UINT i = Rep.GetFirst();

	if( !Rep.Failed(i) ) {

		HidShort const *p = (HidShort const *) Rep[i];

		if( p->m_bType == itemGlobal && p->m_bTag == globalUsagePage ) {

			UINT uPage = p->m_bData[0];

			if( p->m_bSize == 2 ) {

				uPage |= p->m_bData[1] << 8;
				}

			Rep.GetNext(i);

			if( !Rep.Failed(i) ) {

				p = (HidShort const *) Rep[i];

				if( p->m_bType == itemLocal && p->m_bTag == localUsage ) {

					UINT uUsage = p->m_bData[0];

					Trace(debugInfo, "Page %d, Usage %d", uPage, uUsage);

					CreateMapper(uPage, uUsage);
					}
				}
			}
		}
	}

void CUsbHostHidDriver::CreateMapper(UINT uPage, UINT uUsage)
{
	FreeMapper();

	if( uPage == pageDesktop ) {

		if( uUsage == desktopKeyboard ) {

			Trace(debugInfo, "Keyboard device attached");
			
			m_pMapper = Create_HidKeyboardMapper();

			return;
			}

		if( uUsage == desktopMouse ) {

			Trace(debugInfo, "Mouse device attached");
			
			m_pMapper = Create_HidMouseMapper();

			return;
			}

		return;
		}
	}

bool CUsbHostHidDriver::StartMapper(void)
{
	if( m_pMapper ) {

		m_pMapper->Bind(this);

		if( m_pMapper->SetReport(m_pReportData, m_uReportSize) ) {

			return true;
			}
		}

	return false;
	}

void CUsbHostHidDriver::FreeMapper(void)
{
	if( m_pMapper ) {

		m_pMapper->Release();

		m_pMapper = NULL;
		}
	}

void CUsbHostHidDriver::AllocReport(UINT uSize)
{
	FreeReport();

	m_pReportData = New BYTE [uSize];

	m_uReportSize = uSize;
	}

void CUsbHostHidDriver::FreeReport(void)
{
	if( m_pReportData ) {

		delete [] m_pReportData;

		m_pReportData = NULL;

		m_uReportSize = 0;
		}
	}

// End of File
