
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiCmdDiag_HPP

#define	INCLUDE_ScsiCmdDiag_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Send Diagnostic
//

class CScsiCmdDiag : public ScsiCmdDiag
{
	public:
		// Constructor
		CScsiCmdDiag(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Operations
		void Init(void);
		void Init(WORD wLength);
	};

// End of File

#endif
