
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostFtdiDriver_HPP

#define	INCLUDE_UsbHostFtdiDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Ftdi Driver
//

class CUsbHostFtdiDriver : public CUsbHostFuncDriver, public IUsbHostFtdi
{
	public:
		// Constructor
		CUsbHostFtdiDriver(void);

		// Destructor
		~CUsbHostFtdiDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);
		
		// IHostFtdi
		BOOL METHOD Reset(BOOL fClearTx, BOOL fClearRx);
		BOOL METHOD ModemCtrl(BYTE bMask, BYTE bState);
		BOOL METHOD SetFlowCtrl(BYTE bMask, BYTE bXOn, BYTE bXOff);
		BOOL METHOD SetBaudRate(UINT uBaud);
		BOOL METHOD SetData(UINT uBits, UINT uParity, UINT uStop, BOOL fBreak);
		BOOL METHOD GetModemStat(WORD &wStatus);
		BOOL METHOD SetLatTimer(BYTE bLatency);
		BOOL METHOD GetLatTimer(BYTE &bLatency);
		BOOL METHOD SetBitMode(BYTE bMode, BYTE bMask);
		BOOL METHOD GetBitMode(BYTE &bData);
		UINT METHOD Send(PBYTE pData, UINT uLen, BOOL fAsync);
		UINT METHOD Recv(PBYTE pData, UINT uLen, BOOL fAsync);
		UINT METHOD WaitSend(UINT uTimeout);
		UINT METHOD WaitRecv(UINT uTimeout);
		void METHOD KillAsync(void);
		BOOL METHOD PromRead(BYTE bAddr, WORD &wData);
		BOOL METHOD PromWrite(BYTE bAddr, WORD wData);
		BOOL METHOD PromErase(void);
		WORD METHOD PromChecksum(void);
		BOOL METHOD SetOutputLo(BYTE bMask, BYTE bData);
		BOOL METHOD SetOutputHi(BYTE bMask, BYTE bData);
		BYTE METHOD GetInputLo(void);
		BYTE METHOD GetInputHi(void);
		
	protected:
		// Commands
		enum
		{
			cmdReset	= 0x00,
			cmdModemCtrl	= 0x01,
			cmdSetFlowCtrl	= 0x02,
			cmdSetBaudRate	= 0x03,
			cmdSetData	= 0x04,
			cmdGetModemStat	= 0x05,
			cmdSetEventChar	= 0x06,
			cmdSetErrorChar	= 0x07,
			cmdSetLatTimer	= 0x09,
			cmdGetLatTimer	= 0x0A,
			cmdSetBitMode	= 0x0B,
			cmdGetBitMode	= 0x0C,
			cmdPromRead	= 0x90,
			cmdPromWrite	= 0x91,
			cmdPromErase	= 0x92
			};

		// Eeprom Layout
		enum
		{
			promMode	= 0x00,
			promVid		= 0x01,
			promPid		= 0x02,
			promBcdRel	= 0x03,
			promMaxPower	= 0x04,
			promConfig	= 0x05,
			promPortDrive	= 0x06,
			promChecksum	= 0x3F,
			};

		// Modem Control Bits
		enum
		{
			modemDTR	= Bit(0),
			modemRTS	= Bit(1),
			};

		// Data
		UINT	     m_uPort;
		IUsbPipe   * m_pCtrl;
		IUsbPipe   * m_pSend;
		IUsbPipe   * m_pRecv;
		UINT	     m_iSend;
		UINT	     m_iRecv;

		// Implementation
		bool FindPipes(CUsbDescList const &List);
	};

// End of File

#endif
