
#include "Intern.hpp"

#include "UsbHubGetPortStatusReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Get Port Status Request
//

// Constructor

CUsbHubGetPortStatusReq::CUsbHubGetPortStatusReq(WORD wPort)
{
	Init(wPort);
	} 

// Operations

void CUsbHubGetPortStatusReq::Init(WORD wPort)
{
	CUsbClassReq::Init();

	m_bRequest  = reqGetStatus;

	m_Direction = dirDevToHost;

	m_Recipient = recPort;

	m_wIndex    = wPort;
	
	m_wLength   = 4;
	}

// End of File
