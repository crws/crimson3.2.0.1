
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHidDesc_HPP

#define	INCLUDE_UsbHidDesc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hid Framework
//

#include "Hid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hid Descriptor
//

class CUsbHidDesc : public HidDesc
{
	public:
		// Constructor
		CUsbHidDesc(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		bool IsValid(void);

		// Init
		void Init(void);

		// Child List
		UINT GetDescType(UINT uIdx);
		UINT GetDescSize(UINT uIdx);
		UINT FindDesc(UINT bType);
		UINT GetReportSize(void);
		
		// Debug
		void Debug(void);
	};

// End of File

#endif
