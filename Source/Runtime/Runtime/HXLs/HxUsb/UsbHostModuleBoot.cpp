
#include "Intern.hpp"

#include "UsbHostModuleBoot.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Module Boot Loader Driver
//

IUsbHostModuleBoot * Create_ModuleBootDriver(void)
{
	IUsbHostModuleBoot *p = New CUsbHostModuleBootDriver;

	return p;
	}

// Constructor

CUsbHostModuleBootDriver::CUsbHostModuleBootDriver(void)
{
	m_pName  = "Host Module Boot Driver";

	m_Debug  = debugWarn | debugErr;

	m_wTag   = 0;

	m_fWait = false;
		
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostModuleBootDriver::~CUsbHostModuleBootDriver(void)
{
	}

// IUnknown

HRESULT CUsbHostModuleBootDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostModuleBoot);

	return CUsbHostFuncBulkDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostModuleBootDriver::AddRef(void)
{
	return CUsbHostFuncBulkDriver::AddRef();
	}

ULONG CUsbHostModuleBootDriver::Release(void)
{
	return CUsbHostFuncBulkDriver::Release();
	}

// IHostFuncDriver

void CUsbHostModuleBootDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncBulkDriver::Bind(pDevice, iInterface);
	}

void CUsbHostModuleBootDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncBulkDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);
	}

void CUsbHostModuleBootDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncBulkDriver::GetDevice(pDev);
	}

BOOL CUsbHostModuleBootDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncBulkDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostModuleBootDriver::GetVendor(void)
{
	return CUsbHostFuncBulkDriver::GetVendor();
	}

UINT CUsbHostModuleBootDriver::GetProduct(void)
{
	return CUsbHostFuncBulkDriver::GetProduct();
	}

UINT CUsbHostModuleBootDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostModuleBootDriver::GetSubClass(void)
{
	return subclassBoot;
	}

UINT CUsbHostModuleBootDriver::GetProtocol(void)
{
	return devVendor;
	}

UINT CUsbHostModuleBootDriver::GetInterface(void)
{
	return CUsbHostFuncBulkDriver::GetInterface();
	}

BOOL CUsbHostModuleBootDriver::GetActive(void)
{
	return CUsbHostFuncBulkDriver::GetActive();
	}

BOOL CUsbHostModuleBootDriver::Open(CUsbDescList const &List)
{
	return CUsbHostFuncBulkDriver::Open(List);
	}

BOOL CUsbHostModuleBootDriver::Close(void)
{
	return CUsbHostFuncBulkDriver::Close();
	}

void CUsbHostModuleBootDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncBulkDriver::Poll(uLapsed);
	}

// IUsbHostModuleBoot

UINT CUsbHostModuleBootDriver::CheckVersion(PCBYTE pVersion, UINT uSize)
{
	Trace(debugCmds, "CheckVersion");

	m_Cb.m_bOpcode   = bootCheckVersion;

	m_Cb.m_wBulkSize = 0;

	m_Cb.m_bDataSize = uSize;

	memcpy(m_Cb.m_bData, pVersion, uSize);

	return Transact();
	}

UINT CUsbHostModuleBootDriver::ClearProgram(UINT uSize)
{
	Trace(debugCmds, "ClearProgram(0x%8.8X)", uSize);

	m_Cb.m_bOpcode   = bootClearProgram;

	m_Cb.m_wBulkSize = 0;

	m_Cb.m_bDataSize = sizeof(DWORD);

	((DWORD &) m_Cb.m_bData[0]) = HostToIntel(DWORD(uSize));

	return Transact();
	}

UINT CUsbHostModuleBootDriver::WriteProgram(UINT uAddr, PCBYTE pData, UINT uSize)
{
	Trace(debugCmds, "WriteProgram(Addr=0x%8.8X, Size=%d)", uAddr, uSize);

	if( uSize <= 8192 ) {
	
		m_Cb.m_bOpcode   = bootWriteProgram;

		m_Cb.m_bDataSize = 6;

		m_Cb.m_wBulkSize = uSize;

		((DWORD &) m_Cb.m_bData[0]) = HostToIntel(DWORD(uAddr));

		((WORD  &) m_Cb.m_bData[4]) = HostToIntel(WORD(uSize));

		return Transact(PBYTE(pData), uSize, true);
		}

	Trace(debugWarn, "WriteProgram(Addr=0x%8.8X, Size=%d) Error", uAddr, uSize);

	return respFail;
	}

UINT CUsbHostModuleBootDriver::WriteVersion(PCBYTE pVersion, UINT uSize)
{
	Trace(debugCmds, "WriteVersion");

	m_Cb.m_bOpcode   = bootWriteVersion;

	m_Cb.m_wBulkSize = 0;

	m_Cb.m_bDataSize = uSize;

	memcpy(m_Cb.m_bData, pVersion, uSize);

	return Transact();
	}

UINT CUsbHostModuleBootDriver::StartProgram(void)
{
	Trace(debugCmds, "StartProgram");

	m_Cb.m_bOpcode   = bootStartProgram;

	m_Cb.m_wBulkSize = 0;

	m_Cb.m_bDataSize = 0;

	return Transact();
	}

UINT CUsbHostModuleBootDriver::WaitComplete(void)
{
	if( m_fWait ) {

		if( WaitAsyncRecv(0) == NOTHING ) {

			return respBlock;
			}

		m_Sb.m_dwSig     = IntelToHost(m_Sb.m_dwSig);

		m_Sb.m_wBulkSize = IntelToHost(m_Sb.m_wBulkSize);

		m_Sb.m_wTag      = IntelToHost(m_Sb.m_wTag);

		m_fWait          = false;
		}

	if( m_Sb.m_dwSig != constSigSB ) {

		Trace(debugWarn, "Invalid SB Sig");
				
		return respFail;
		}
	
	if( m_Sb.m_wTag != m_wTag ) {

		Trace(debugWarn, "Invalid SB Sequence");

		return respFail;
		}

	if( m_Sb.m_bOpcode == opReplyFalse ) {
			
		return respFalse;
		}

	if( m_Sb.m_bOpcode == opReplyTrue || m_Sb.m_bOpcode == opAck ) {
			
		return respPass;
		}

	return respFail;
	}

// Implementation

bool CUsbHostModuleBootDriver::SendHead(void)
{
	m_Cb.m_dwSig	 = constSigCB;
	
	m_Cb.m_wTag	 = ++m_wTag;

	m_Cb.m_bService  = servBoot;

	m_Cb.m_dwSig	 = HostToIntel(m_Cb.m_dwSig);
	
	m_Cb.m_wTag	 = HostToIntel(m_Cb.m_wTag);

	m_Cb.m_wBulkSize = HostToIntel(m_Cb.m_wBulkSize);

	return SendBulk(PCBYTE(&m_Cb), sizeof(m_Cb));
	}

bool CUsbHostModuleBootDriver::RecvStat(void)
{
	if( RecvBulk(PBYTE(&m_Sb), sizeof(m_Sb), true) != NOTHING ) {

		m_fWait = true;

		return true;
		}

	return false;
	}

UINT CUsbHostModuleBootDriver::Transact(void)
{
	return Transact(NULL, 0, false);
	}

UINT CUsbHostModuleBootDriver::Transact(PBYTE pData, UINT uSize, bool fSend)
{
	if( m_fWait ) {

		KillAsyncRecv();

		m_fWait = false;
		}

	if( SendHead() ) {

		if( pData && uSize ) {

			if( fSend ) {

				if( !SendBulk(pData, uSize) ) {

					return respFail;
					}
				}
			else {
				if( !RecvBulk(pData, uSize) ) {

					return respFail;
					}
				}
			}

		if( RecvStat() ) {
			
			return respBlock;
			}
		}
	
	return respFail;
	}

// End of File
