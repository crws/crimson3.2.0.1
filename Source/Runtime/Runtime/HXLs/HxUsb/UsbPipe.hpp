
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbPipe_HPP

#define	INCLUDE_UsbPipe_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Pipe
//

class CUsbPipe : public IUsbPipe, public IUsbPipeEvents
{
	public:
		// Constructor
		CUsbPipe(void);

		// Desstructor
		~CUsbPipe(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbPipe
		void	     METHOD SetDevice(IUsbDevice *pDev);
		IUsbDevice * METHOD GetDevice(void);
		void	     METHOD SetEndpoint(DWORD iEndpoint);
		DWORD        METHOD GetEndpoint(void);
		void	     METHOD SetDirIn(BOOL fIn);
		BOOL	     METHOD GetDirIn(void);
		void	     METHOD SetEndpointAddr(BYTE bAddr);
		BYTE         METHOD GetEndpointAddr(void);
		BYTE	     METHOD GetFeatureAddr(void);
		void	     METHOD SetMaxPacket(WORD wMaxPacket);
		void         METHOD Claim(void);
		void         METHOD Free(void);
		BOOL         METHOD SendSetup(PBYTE pData, UINT uCount, UINT uFlags);
		BOOL         METHOD SendSetupData(PBYTE pData, UINT uCount);
		UINT         METHOD RecvSetupData(PBYTE pData, UINT uCount);
		BOOL         METHOD SendStatus(void);
		BOOL         METHOD RecvStatus(void);
		BOOL         METHOD SendBulk(PBYTE pData, UINT uCount, BOOL fAsync);
		UINT         METHOD RecvBulk(PBYTE pData, UINT uCount, BOOL fAsync);
		BOOL         METHOD WaitAsync(UINT uTimeout, BOOL &fOk, UINT &uCount);
		BOOL         METHOD KillAsync(void);
		BOOL         METHOD CtrlTrans(UsbDeviceReq &Req);
		UINT         METHOD CtrlTrans(UsbDeviceReq &Req, PBYTE pData, UINT n);
		BOOL         METHOD IsHalted(void);
		BOOL         METHOD ClearStall(void);
		void         METHOD RequestCompletion(IUsbHostFuncEvents *pSink);
		
		// IUsbPipeEvents
		void METHOD OnTransfer(UsbIor &Urb);
		void METHOD OnPoll(UINT uLapsed);
		
	protected:
		// Commands
		enum
		{
			cmdClearStall,
			};

		// Data
		ULONG		     m_uRefs;
		DWORD		     m_iEndpt;
		BYTE                 m_bAddr;
		BOOL		     m_fIn;
		UINT volatile        m_uCount;
		IEvent             * m_pDone;
		IMutex             * m_pMutex;
		UINT volatile	     m_uStatus;
		IUsbDevice         * m_pDevice;
		IUsbHostStack      * m_pStack;
		IUsbHostFuncEvents * m_pSink;
		DWORD		     m_uError;
		UsbIor             * m_pApcList[8];
		UINT volatile	     m_iApcHead;
		UINT volatile	     m_iApcTail;
		
		// Comms
		void TermApc(void);
		void ExecApc(void);
		BOOL ExecCmd(UINT uCommand);
		BOOL QueueApc(UINT uCommand);
		BOOL QueueApc(PBYTE pData, UINT uCount, UINT uFlags);
		BOOL Transfer(PBYTE pData, UINT uCount, UINT uFlags);
		void ClearHaltCondition(void);
		void ResetEndpoint(void);
		BOOL CanWait(void);
		void SetError(void);
		void CheckError(void);
		void CheckError(UsbIor const &Urb);
	};

// End of File

#endif
