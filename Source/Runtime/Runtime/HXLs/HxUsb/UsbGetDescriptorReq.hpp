
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbGetDescriptorReq_HPP

#define	INCLUDE_UsbGetDescriptorReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Descriptor Standard Device Requeset
//

class CUsbGetDescriptorReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbGetDescriptorReq(void);

		// Operations
		void Init     (void);
		void SetDevice(WORD wLength);
		void SetQualifier(WORD wLength);
		void SetConfig(WORD wIndex, WORD wLength);
		void SetString(WORD wIndex, WORD wLength, WORD wLangId);
	};

// End of File

#endif
