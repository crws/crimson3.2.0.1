
#include "Intern.hpp"

#include "CdcLineCoding.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CDC Line Coding
//

// Constructor

CCdcLineCoding::CCdcLineCoding(void)
{
	}

// Endianess

void CCdcLineCoding::HostToCdc(void)
{
	m_dwBaud = HostToIntel(m_dwBaud);
	}

void CCdcLineCoding::CdcToHost(void)
{
	m_dwBaud = IntelToHost(m_dwBaud);
	}

// Operations

void CCdcLineCoding::Init(void)
{
	memset(this, 0, sizeof(CdcLineCoding));
	}

void CCdcLineCoding::Init(CSerialConfig const &Config)
{
	m_dwBaud  = Config.m_uBaudRate;

	m_bStop   = (Config.m_uStopBits) == 1 ? 0 : 2;

	m_bParity = Config.m_uParity;

	m_bData   = Config.m_uDataBits;
	}

void CCdcLineCoding::Get(CSerialConfig &Config)
{
	Config.m_uPhysical = 0;
	
	Config.m_uBaudRate = m_dwBaud;
	
	Config.m_uDataBits = m_bData;
	
	Config.m_uStopBits = (m_bStop == 0) ? 1 : 2;
	
	Config.m_uParity   = m_bParity;
	
	Config.m_uFlags    = 0;
	
	Config.m_bDrop     = 0;
	}

// End of File
