
#include "Intern.hpp"

#include "UsbHostSerialDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Serial Driver
//

// Instantiator

IUsbHostSerial * Create_SerialDriver(void)
{
	IUsbHostSerial *p = (IUsbHostSerial *) New CUsbHostSerialDriver;

	return p;
	}

// Constructor

CUsbHostSerialDriver::CUsbHostSerialDriver(void)
{
	m_pName  = "Host Serial Driver";

	m_Debug  = debugWarn;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostSerialDriver::~CUsbHostSerialDriver(void)
{
	}

// IUnknown

HRESULT CUsbHostSerialDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostSerial);

	return CUsbHostModuleDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostSerialDriver::AddRef(void)
{
	return CUsbHostModuleDriver::AddRef();
	}

ULONG CUsbHostSerialDriver::Release(void)
{
	return CUsbHostModuleDriver::Release();
	}

// IHostFuncDriver

void CUsbHostSerialDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostModuleDriver::Bind(pDevice, iInterface);
	}

void CUsbHostSerialDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostModuleDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostSerialDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostModuleDriver::GetDevice(pDev);
	}

BOOL CUsbHostSerialDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostModuleDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostSerialDriver::GetVendor(void)
{
	return CUsbHostModuleDriver::GetVendor();
	}

UINT CUsbHostSerialDriver::GetProduct(void)
{
	return CUsbHostModuleDriver::GetProduct();
	}

UINT CUsbHostSerialDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostSerialDriver::GetSubClass(void)
{
	return subclassComms;
	}

UINT CUsbHostSerialDriver::GetProtocol(void)
{
	return CUsbHostModuleDriver::GetProtocol();
	}

UINT CUsbHostSerialDriver::GetInterface(void)
{
	return CUsbHostModuleDriver::GetInterface();
	}

BOOL CUsbHostSerialDriver::GetActive(void)
{
	return CUsbHostModuleDriver::GetActive();
	}

BOOL CUsbHostSerialDriver::Open(CUsbDescList const &List)
{
	return CUsbHostModuleDriver::Open(List);
	}

BOOL CUsbHostSerialDriver::Close(void)
{
	return CUsbHostModuleDriver::Close();
	}

void CUsbHostSerialDriver::Poll(UINT uLapsed)
{
	CUsbHostModuleDriver::Poll(uLapsed);
	}

// IUsbHostModuleDriver

BOOL CUsbHostSerialDriver::Reset(void)
{
	return CUsbHostModuleDriver::Reset(); 
	}

BOOL CUsbHostSerialDriver::ReadVersion(BYTE bVersion[16])
{
	return CUsbHostModuleDriver::ReadVersion(bVersion);
	}

BOOL CUsbHostSerialDriver::SendHeartbeat(void)
{
	return CUsbHostModuleDriver::SendHeartbeat();
	}

// IUsbHostSerialDriver

BOOL CUsbHostSerialDriver::SetRun(BOOL fRun)
{
	Trace(debugCmds, "SetRun(%d)", fRun);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetRun;

	Req.m_wIndex    = GetInterface();

	Req.m_wValue    = fRun ? 1 : 0;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostSerialDriver::SetPhysical(UINT uPhysical)
{
	Trace(debugCmds, "SetPhysical(%d)", uPhysical);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetPhysical;

	Req.m_wIndex    = GetInterface();

	Req.m_wValue    = uPhysical;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostSerialDriver::SetBaud(UINT uBaud)
{
	Trace(debugCmds, "SetBaudRate(%d)", uBaud);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetBaud;

	Req.m_wIndex    = GetInterface();

	Req.m_wValue    = uBaud / 100;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostSerialDriver::SetFormat(UINT uData, UINT uStop, UINT Parity)
{
	Trace(debugCmds, "SetFormat(Data=%d, Stop=%d, Parity=%d)", uData, uStop, Parity);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetFormat;

	Req.m_wIndex    = GetInterface();

	Req.m_wValue    = uData | (uStop << 8) | (Parity << 12);

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostSerialDriver::SetFlags(UINT uFlags)
{
	Trace(debugCmds, "SetFlags(0x%4.4X)", uFlags);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetFlags;

	Req.m_wIndex    = GetInterface();

	Req.m_wValue    = uFlags;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostSerialDriver::SetLatency(UINT uLatency)
{
	Trace(debugCmds, "SetLatency(%d)", uLatency);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetLatency;

	Req.m_wIndex    = GetInterface();

	Req.m_wValue    = uLatency;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostSerialDriver::SendData(PCBYTE pData, UINT uCount)
{
	Trace(debugCmds, "SendData(Count=%d)", uCount);

	return CUsbHostModuleDriver::SendBulk(pData, uCount);
	}

UINT CUsbHostSerialDriver::RecvData(PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "RecvData(Count=%d)", uCount);

	return CUsbHostModuleDriver::RecvBulk(pData, uCount);
	}

BOOL CUsbHostSerialDriver::SendDataAsync(PCBYTE pData, UINT uCount)
{
	Trace(debugCmds, "SendDataAsync(Count=%d)", uCount);

	return CUsbHostModuleDriver::SendBulk(pData, uCount, true);
	}

UINT CUsbHostSerialDriver::RecvDataAsync(PBYTE pData, UINT uCount)
{
	Trace(debugCmds, "RecvDataAsync(Count=%d)", uCount);

	return CUsbHostModuleDriver::RecvBulk(pData, uCount, true);
	}

UINT CUsbHostSerialDriver::WaitAsyncSend(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncSend(Timeout=%d)", uTimeout);

	return CUsbHostModuleDriver::WaitAsyncSend(uTimeout);
	}

UINT CUsbHostSerialDriver::WaitAsyncRecv(UINT uTimeout)
{
	Trace(debugCmds, "WaitAsyncRecv(Timeout=%d)", uTimeout);

	return CUsbHostModuleDriver::WaitAsyncRecv(uTimeout);
	}

BOOL CUsbHostSerialDriver::KillAsyncSend(void)
{
	Trace(debugCmds, "KillAsyncSend");

	return CUsbHostModuleDriver::KillAsyncSend();
	}

BOOL CUsbHostSerialDriver::KillAsyncRecv(void)
{
	Trace(debugCmds, "KillAsyncRecv");

	return CUsbHostModuleDriver::KillAsyncRecv();
	}

// End of File
