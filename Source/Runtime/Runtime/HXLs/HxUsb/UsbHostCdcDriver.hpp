
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostCdcDriver_HPP

#define	INCLUDE_UsbHostCdcDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Framework
//

#include "Cdc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Comms Device Class Driver
//

class CUsbHostCdcDriver : public CUsbHostFuncDriver, public IUsbHostCdc
{
	public:
		// Constructor
		CUsbHostCdcDriver(void);

		// Destructor
		~CUsbHostCdcDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostCdc
		BOOL METHOD SendEncapCommand(PBYTE pData, UINT uLen);
		UINT METHOD RecvEncapResponse(PBYTE pData, UINT uLen);
		BOOL METHOD SendData(PCTXT pData, bool fAsync);
		BOOL METHOD SendData(PCBYTE pData, UINT uLen, bool fAsync);
		UINT METHOD RecvData(PBYTE  pData, UINT uLen, bool fAsync);
		UINT METHOD WaitSend(UINT uTimeout);
		UINT METHOD WaitRecv(UINT uTimeout);
		void METHOD KillSend(void);
		void METHOD KillRecv(void);
		void METHOD KillAsync(void);

	protected:
		// Data
		CdcUnionDesc    * m_pUnion;
		IUsbPipe	* m_pCtrl;
		IUsbPipe	* m_pPoll;
		IUsbPipe	* m_pSend;
		IUsbPipe	* m_pRecv;
		UINT		  m_iPoll;
		UINT	  	  m_iSend;
		UINT		  m_iRecv;
		PBYTE             m_pData;
		UINT              m_uData;
		BOOL		  m_fPend;

		// Notifications
		virtual void OnEvent(UsbDeviceReq const &Req);

		// Implementation
		bool ParseConfig(void);
		bool FindEndpoints(void);
		bool FindCtrlEndpoint(void);
		bool FindPollEndpoint(void);
		bool FindDataEndpoint(void);
		void FreeData(void);
	};

// End of File

#endif
