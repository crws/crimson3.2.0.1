
#include "Intern.hpp"

#include "UsbCan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Can Port
//

// Instantiator

IPortObject * Create_UsbCan(IUsbHostFuncDriver *pDriver, WORD wProtocol)
{
	CUsbCan *p = New CUsbCan(pDriver, wProtocol);

	p->Open();

	return p;
}

// Constructor

CUsbCan::CUsbCan(IUsbHostFuncDriver *pDriver, WORD wProtocol)
{
	m_pDriver   = (IUsbHostCan *) pDriver;

	m_wProtocol = wProtocol;

	m_pHandler  = NULL;

	m_pTimer    = CreateTimer();
}

// Destructor

CUsbCan::~CUsbCan(void)
{
	m_pTimer->Release();
}

// IDevice

BOOL METHOD CUsbCan::Open(void)
{
	BindDriver(m_pDriver);

	m_pTimer->SetPeriod(5);

	m_pTimer->SetHook(this, 0);

	return TRUE;
}

// IPortObject

void METHOD CUsbCan::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
}

UINT METHOD CUsbCan::GetPhysicalMask(void)
{
	switch( m_wProtocol ) {

		case protoCan:
			return Bit(physicalCAN);

		case protoJ1939:
			return Bit(physicalJ1939);
	}

	return 0;
}

BOOL METHOD CUsbCan::Open(CSerialConfig const &Config)
{
	if( !m_fOpen ) {

		m_Config = Config;

		m_fPdo   = Config.m_uFlags & flagPrivate;

		m_fExt   = Config.m_uFlags & flagExtID;

		m_fPass  = Config.m_uFlags & flagExtPass;

		LockPnp();

		InitDriver();

		ClearData();

		m_fOpen = true;

		m_pHandler->OnOpen(m_Config);

		m_pTimer->Enable(true);

		StartDriver();

		FreePnp();

		return true;
	}

	return false;
}

void METHOD CUsbCan::Close(void)
{
	if( m_fOpen ) {

		LockPnp();

		m_fOpen = false;

		WaitDone(5000);

		StopDriver();

		TermDriver();

		m_pTimer->Enable(false);

		SuckDry();

		m_pHandler->OnClose();

		FreePnp();
	}
}

void METHOD CUsbCan::Send(BYTE bData)
{
	if( m_fOpen ) {

		WaitDone(FOREVER);

		if( TxStore(bData) ) {

			TxFrame();
		}
		else {
			for( ;;) {

				if( !m_pHandler->OnTxData(bData) ) {

					m_pHandler->OnTxDone();

					break;
				}

				if( TxStore(bData) ) {

					m_pHandler->OnTxDone();

					if( TxFrame() ) {

						break;
					}
				}
			}
		}
	}
}

void METHOD CUsbCan::SetOutput(UINT uOutput, BOOL fSet)
{
	if( m_fPdo ) {

		LockPnp();

		if( IsPresent() ) {

			m_pDriver->SetLed(0, UINT(fSet));
		}

		FreePnp();
	}
}

BOOL METHOD CUsbCan::GetInput(UINT uInput)
{
	LockPnp();

	if( IsPresent() ) {

		if( m_pDriver->GetError() ) {

			FreePnp();

			return TRUE;
		}
	}

	FreePnp();

	return FALSE;
}

// IUsbHostFuncEvents

void METHOD CUsbCan::OnPoll(UINT uLapsed)
{
	if( m_fOpen ) {

		PollIdle();
	}
}

void METHOD CUsbCan::OnData(void)
{
	if( m_fOpen ) {

		OnRecv();

		OnSend();
	}
}

// IEventSink

void CUsbCan::OnEvent(UINT uLine, UINT uParam)
{
	if( m_fOpen ) {

		m_uHeartbeat++;

		if( m_fPdo ) {

			m_pHandler->OnTimer();
		}
	}
}

// Overridables 

void CUsbCan::OnDriverBind(IUsbHostFuncDriver *pDriver)
{
	m_pDriver = (IUsbHostCan *) pDriver;

	m_pDriver->Bind(this);

	m_pDriver->SetLed(0, ledError);
}

void CUsbCan::OnInitDriver(void)
{
	m_pDriver->SetRun(false);

	m_pDriver->SetBaudRate(m_Config.m_uBaudRate);

	m_pDriver->SetLatency(10);

	m_pDriver->SetLed(0, ledRun);

	InitFilter();
}

void CUsbCan::OnStartDriver(void)
{
	m_pDriver->SetRun(true);

	StartRecv();
}

void CUsbCan::OnStopDriver(void)
{
	m_pDriver->SetLed(0, ledOff);

	m_pDriver->SetRun(false);
}

void CUsbCan::OnTermDriver(void)
{
	m_pDriver->KillAsyncRecv();

	m_pDriver->KillAsyncSend();
}

// Implementation

void CUsbCan::ClearData(void)
{
	m_uRecvState = stateIdle;

	m_uSendState = stateIdle;

	m_uTxCount   = 0;

	m_uRxCount   = 0;

	m_uHeartbeat = 0;
}

void CUsbCan::InitFilter(void)
{
	DWORD dwIdent = 0;

	DWORD dwMask  = 0;

	if( !m_fPdo ) {

		if( m_Config.m_bDrop <= 0x7F ) {

			dwIdent = 0x600 | m_Config.m_bDrop;

			dwMask  = 0x780 | 0x07F;
		}
		else {
			dwIdent = 0x580;

			dwMask  = 0x780;
		}
	}

	if( !m_fExt ) {

		dwIdent <<= 18;

		dwMask  <<= 18;
	}

	m_pDriver->SetFilter(0, dwIdent, dwMask);
}

void CUsbCan::OnSend(void)
{
	if( m_uSendState == stateWait ) {

		if( m_pDriver->WaitAsyncSend(0) == NOTHING ) {

			return;
		}

		m_uSendState = stateIdle;

		for( ;;) {

			BYTE bData;

			if( !m_pHandler->OnTxData(bData) ) {

				m_pHandler->OnTxDone();

				return;
			}

			if( TxStore(bData) ) {

				m_uSendState = stateWait;

				m_pHandler->OnTxDone();

				if( TxFrame() ) {

					return;
				}
			}
		}
	}
}

void CUsbCan::OnRecv(void)
{
	for( ;;) {

		if( m_uRecvState == stateRecv ) {

			if( m_pDriver->RecvFrameAsync(m_RxFrame, elements(m_RxFrame)) != NOTHING ) {

				m_uRecvState = stateWait;

				continue;
			}

			return;
		}

		if( m_uRecvState == stateWait ) {

			m_uRxCount = m_pDriver->WaitAsyncRecv(0);

			if( m_uRxCount != NOTHING ) {

				if( m_uRxCount ) {

					RxFrame();
				}

				m_uRecvState = stateRecv;

				continue;
			}

			return;
		}

		if( m_uRecvState == stateFail ) {

			m_uRecvState = stateRecv;

			return;
		}

		break;
	}
}

void CUsbCan::PollIdle(void)
{
	if( m_uHeartbeat > ToTicks(1000) ) {

		m_pDriver->SendHeartbeat();

		m_uHeartbeat = 0;
	}
}

bool CUsbCan::TxStore(BYTE bData)
{
	PBYTE pFrame = PBYTE(&m_TxFrame);

	pFrame[m_uTxCount++] = bData;

	if( !m_fExt && m_uTxCount == 4 ) {

		pFrame[m_uTxCount++] = 0;

		pFrame[m_uTxCount++] = 0;
	}

	return m_uTxCount == sizeof(m_TxFrame);
}

bool CUsbCan::TxFrame(void)
{
	if( !m_fPass ) {

		if( m_fExt ) {

			m_TxFrame.m_bFlags = flagExt;
		}
		else {
			m_TxFrame.m_bFlags = 0;
		}
	}
	else {
		if( m_TxFrame.m_bCount & 0x80 ) {

			m_TxFrame.m_bFlags = flagExt;
		}
		else {
			m_TxFrame.m_bFlags = 0;
		}
	}

	m_uTxCount   = 0;

	m_uHeartbeat = 0;

	return StartSend();
}

void CUsbCan::RxFrame(void)
{
	m_uHeartbeat = 0;

	for( UINT i = 0; i < m_uRxCount; i++ ) {

		UsbCanFrame const &Frame = m_RxFrame[i];

		bool fExt = Frame.m_bFlags & flagExt;

		if( (m_fExt && fExt) || (!m_fExt && !fExt) || m_fPass ) {

			PBYTE p = PBYTE(&Frame);

			for( UINT n = 0; n < sizeof(Frame); n++ ) {

				m_pHandler->OnRxData(p[n]);

				if( !m_fExt && n == 3 ) {

					n += 2;
				}
			}

			m_pHandler->OnRxDone();
		}
	}
}

bool CUsbCan::StartRecv(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( IsPresent() && IsRunning() ) {

		if( m_pDriver->RecvFrameAsync(m_RxFrame, elements(m_RxFrame)) != NOTHING ) {

			m_uRecvState = stateWait;

			Hal_LowerIrql(uSave);

			return true;
		}
	}

	Hal_LowerIrql(uSave);

	return false;
}

bool CUsbCan::StartSend(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( IsPresent() && IsRunning() ) {

		if( m_pDriver->SendFrameAsync(&m_TxFrame, 1) ) {

			m_uSendState = stateWait;

			Hal_LowerIrql(uSave);

			return true;
		}
	}

	m_uSendState = stateIdle;

	Hal_LowerIrql(uSave);

	return false;
}

void CUsbCan::SuckDry(void)
{
	BYTE bData;

	while( m_pHandler->OnTxData(bData) );

	m_pHandler->OnTxDone();
}

void CUsbCan::WaitDone(UINT uTimeout)
{
	if( m_uSendState != stateIdle ) {

		m_pDriver->WaitAsyncSend(uTimeout);
	}
}

// End of File
