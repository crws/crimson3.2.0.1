
#include "Intern.hpp"

#include "UsbProfibusExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Can Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbProfibusExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbProfibusExpansion(pDriver);

	return p;
	}

// Constructor

CUsbProfibusExpansion::CUsbProfibusExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansionSerial(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbProfibusExpansion::GetName(void)
{
	return "GMPB";
	}

UINT METHOD CUsbProfibusExpansion::GetClass(void)
{
	return rackClassComms;
	}

UINT METHOD CUsbProfibusExpansion::GetPower(void)
{
	return 29;
	}

BOOL METHOD CUsbProfibusExpansion::HasBootLoader(void)
{
	return TRUE;
	}

IDevice * METHOD CUsbProfibusExpansion::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return Create_UsbProfibus(pDriver);
	}

// IExpansionSerial

UINT METHOD CUsbProfibusExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD METHOD CUsbProfibusExpansion::GetPortMask(void)
{
	return Bit(physicalRS485);
	}

UINT METHOD CUsbProfibusExpansion::GetPortType(UINT iPort)
{
	return rackProfibus;
	}

// End of File
