
#include "Intern.hpp"

#include "UsbProfibus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Profibus Port
//

// Instantiator

IProfibusPort * Create_UsbProfibus(IUsbHostFuncDriver *pDriver)
{
	CUsbProfibus *p = New CUsbProfibus(pDriver);

	p->Open();

	return p;
	}

// Constructor

CUsbProfibus::CUsbProfibus(IUsbHostFuncDriver *pDriver)
{
	m_pDriver = (IUsbHostProfibus *) pDriver;
	}

// Destructor

CUsbProfibus::~CUsbProfibus(void)
{
	}

// IUnknown

HRESULT METHOD CUsbProfibus::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IProfibusPort);

	return CUsbPort::QueryInterface(riid, ppObject);
	}

ULONG METHOD CUsbProfibus::AddRef(void)
{
	return CUsbPort::AddRef();
	}

ULONG METHOD CUsbProfibus::Release(void)
{
	return CUsbPort::Release();
	}

// IDevice

BOOL METHOD CUsbProfibus::Open(void)
{
	BindDriver(m_pDriver);
	
	return TRUE;
	}

// IPortObject

void METHOD CUsbProfibus::Bind(IPortHandler *pHandler)
{
	}

void METHOD CUsbProfibus::Bind(IPortSwitch *pSwitch)
{
	}

UINT METHOD CUsbProfibus::GetPhysicalMask(void)
{
	return Bit(physicalProfibus);
}

BOOL METHOD CUsbProfibus::Open(CSerialConfig const &Config)
{
	if( !m_fOpen ) {

		m_Config = Config;

		LockPnp();

		InitDriver();

		m_fOpen   = true;

		m_fOnline = false;
										
		InitData();

		StartDriver();

		FreePnp();

		return TRUE;
		}

	return FALSE;
	}

void METHOD CUsbProfibus::Close(void)
{
	if( m_fOpen ) {

		LockPnp();

		m_fOpen   = false;

		m_fOnline = false;

		StopDriver();

		TermDriver();

		FreeData();

		FreePnp();
		}
	}

void METHOD CUsbProfibus::Send(BYTE bData)
{
	}

void METHOD CUsbProfibus::SetBreak(BOOL fBreak)
{
	}

void METHOD CUsbProfibus::EnableInterrupts(BOOL fEnable)
{
	}

void METHOD CUsbProfibus::SetOutput(UINT uOutput, BOOL fSet)
{
	}

BOOL METHOD CUsbProfibus::GetInput(UINT uInput)
{
	return FALSE;
	}

DWORD METHOD CUsbProfibus::GetHandle(void)
{
	return CUsbPort::GetHandle();
	}

// IProfibusPort

void METHOD CUsbProfibus::Poll(void)
{
	}

BOOL METHOD CUsbProfibus::IsOnline(void)
{
	return CheckOnline() && CheckComms();
	}

UINT METHOD CUsbProfibus::GetOutputSize(void)
{
	return m_uGetSize;
	}

UINT METHOD CUsbProfibus::GetInputSize(void)
{
	return m_uPutSize;
	}

PBYTE METHOD CUsbProfibus::GetOutputBuffer(void)
{
	LockPnp();

	if( IsRunning() ) {

		if( !m_fWaitGet ) {

			if( m_pDriver->GetDataAsync(m_pGetBuff[m_uGetBuff], m_uGetSize) != NOTHING ) {

				m_fWaitGet = true;
				}
			}

		if( m_fWaitGet ) { 

			UINT nCount = m_pDriver->WaitAsyncGet(50);

			if( nCount != NOTHING ) {

				m_fWaitGet = false;

				if( nCount != 0 ) {

					m_pGetData = m_pGetBuff[m_uGetBuff];

					m_uGetBuff = (m_uGetBuff + 1) % elements(m_pGetBuff);
					}
				}
			}
		}

	FreePnp();

	return m_pGetData;
	}

BOOL METHOD CUsbProfibus::PutInputData(PCBYTE pData, UINT uSize)
{
	LockPnp();

	if( IsRunning() ) {

		if( m_pDriver->PutDataAsync(pData, uSize) != NOTHING ) {

			UINT uCount = m_pDriver->WaitAsyncPut(30000);

			if( uCount == NOTHING ) {

				m_pDriver->KillAsyncPut();

				m_uDataErr ++;

				FreePnp();

				return false;
				}

			if( uCount != 0 ) {

				FreePnp();

				return true;
				}
			}
		}

	FreePnp();

	m_uDataErr ++;

	return false;
	} 

// Overridables 

void CUsbProfibus::OnDriverBind(IUsbHostFuncDriver *pDriver)
{
	m_pDriver = (IUsbHostProfibus *) pDriver;
	}

void CUsbProfibus::OnInitDriver(void)
{
	m_pDriver->SetRun(false);

	m_pDriver->SetStation(m_Config.m_bDrop);
	}

void CUsbProfibus::OnStartDriver(void)
{
	m_pDriver->SetRun(true);
	}

void CUsbProfibus::OnStopDriver(void)
{
	m_pDriver->SetRun(false);
	}

void CUsbProfibus::OnTermDriver(void)
{
	AbortComms();
	}

// Implementation 

bool CUsbProfibus::CheckOnline(void)
{
	LockPnp();

	if( IsPresent() && m_pDriver->IsOnline() ) {

		if( m_fOnline ) {

			if( m_uGetSize != m_pDriver->GetOutputDataSize() ) {

				SetOffline();
				}

			if( m_uPutSize != m_pDriver->GetInputDataSize() ) {

				SetOffline();
				}
			}

		if( !m_fOnline ) {

			SetOnline();
			}

		FreePnp();
				
		return true;
		}
	else {
		if( m_fOnline ) {

			SetOffline();
			}

		FreePnp();

		return false;
		}
	}

void CUsbProfibus::SetOnline(void)
{
	if( !m_fOnline ) {

		m_uGetSize = m_pDriver->GetOutputDataSize();

		m_uPutSize = m_pDriver->GetInputDataSize();

		m_pGetBuff[0] = New BYTE[ m_uGetSize ];

		m_pGetBuff[1] = New BYTE[ m_uGetSize ];
			
		m_fOnline = true;
		}
	}

void CUsbProfibus::SetOffline(void)
{
	if( m_fOnline ) {

		AbortComms();

		FreeData();

		InitData();
			
		m_fOnline = false;
		}
	}

bool CUsbProfibus::CheckComms(void)
{
	if( m_uDataErr ) {

		m_uDataErr = 0;

		return false;
		}

	return true;
	}

void CUsbProfibus::AbortComms(void)
{
	if( !IsRemoved() ) {

		m_pDriver->KillAsyncGet();

		m_pDriver->KillAsyncPut();
		}
	}

void CUsbProfibus::InitData(void)
{
	m_uGetSize    = 0;
			
	m_uPutSize    = 0;
		
	m_fWaitGet    = false;

	m_uGetBuff    = 0;

	m_pGetBuff[0] = NULL;

	m_pGetBuff[1] = NULL;

	m_pGetData    = NULL;

	m_uDataErr    = 0;
	}

void CUsbProfibus::FreeData(void)
{
	if( m_pGetBuff[0] ) {

		delete [] m_pGetBuff[0];

		delete [] m_pGetBuff[1];

		m_pGetBuff[0] = NULL;

		m_pGetBuff[1] = NULL;
		}
	}

// End of File
