
#include "Intern.hpp"

#include "UsbCanExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Expansion System
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Can Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbCanExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbCanExpansion(pDriver);

	return p;
	}

// Constructor

CUsbCanExpansion::CUsbCanExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansionSerial(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbCanExpansion::GetName(void)
{
	switch( m_wProtocol ) {

		case protoCan:
			return "GMCAN";

		case protoJ1939:
			return "GMJ1939";
		}

	return NULL;
	}

UINT METHOD CUsbCanExpansion::GetClass(void)
{
	return rackClassComms;
	}

UINT METHOD CUsbCanExpansion::GetPower(void)
{
	return 12;
	}

BOOL METHOD CUsbCanExpansion::HasBootLoader(void)
{
	return TRUE;
	}

IDevice * METHOD CUsbCanExpansion::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return Create_UsbCan(pDriver, m_wProtocol);
	}

// IExpansionSerial

UINT METHOD CUsbCanExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD METHOD CUsbCanExpansion::GetPortMask(void)
{
	return Bit(physicalRS485);
	}

UINT METHOD CUsbCanExpansion::GetPortType(UINT iPort)
{
	switch( m_wProtocol ) {

		case protoCan:	 return rackCan;
		case protoJ1939: return rackJ1939;
		}

	return rackNone;
	}

// End of File
