
#include "Intern.hpp"

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb PnP Port
//

CUsbPort::CUsbPort(void)
{
	}

// IUnknown

HRESULT METHOD CUsbPort::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDevice);
	StdQueryInterface(IPortObject);
	StdQueryInterface(IUsbHostFuncEvents);
	StdQueryInterface(IExpansionPnp);

	return CUsbModule::QueryInterface(riid, ppObject);
	}

ULONG METHOD CUsbPort::AddRef(void)
{
	return CUsbModule::AddRef();
	}

ULONG METHOD CUsbPort::Release(void)
{
	return CUsbModule::Release();
	}

// IDevice

BOOL METHOD CUsbPort::Open(void)
{
	return TRUE;
	}

// IPortObject

void METHOD CUsbPort::Bind(IPortHandler *pHandler)
{
	}

void METHOD CUsbPort::Bind(IPortSwitch  *pSwitch)
{
	}

BOOL METHOD CUsbPort::Open(CSerialConfig const &Config)
{
	return FALSE;
	}

void METHOD CUsbPort::Close(void)
{
	}

void METHOD CUsbPort::Send(BYTE bData)
{
	}

void METHOD CUsbPort::SetBreak(BOOL fBreak)
{
	}

void METHOD CUsbPort::EnableInterrupts(BOOL fEnable)
{
	CUsbModule::EnableInterrupts(fEnable);	
	}

void METHOD CUsbPort::SetOutput(UINT uOutput, BOOL fOn)
{
	}

BOOL METHOD CUsbPort::GetInput(UINT uInput)
{
	return FALSE;
	}

DWORD METHOD CUsbPort::GetHandle(void)
{
	return CUsbModule::GetHandle();
	}

// IUsbHostFuncEvents

void METHOD CUsbPort::OnPoll(UINT uLapsed)
{
	}

void METHOD CUsbPort::OnData(void)
{
	}

void METHOD CUsbPort::OnTerm(void)
{
	}

// End of File
