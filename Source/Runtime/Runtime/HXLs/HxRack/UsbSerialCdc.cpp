
#include "Intern.hpp"

#include "UsbSerialCdc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial CDC ACM Modem Port
//

// Instantiator

IPortObject * Create_UsbSerialCdc(IUsbHostFuncDriver *pDriver)
{
	CUsbSerialCdc *p = New CUsbSerialCdc(pDriver);

	p->Open();

	return p;
}

// Constructor

CUsbSerialCdc::CUsbSerialCdc(IUsbHostFuncDriver *pDriver)
{
	m_pDriver  = (IUsbHostAcm *) pDriver;

	m_pHandler = NULL;

	m_pTimer   = CreateTimer();
}

// Destructor

CUsbSerialCdc::~CUsbSerialCdc(void)
{
	m_pTimer->Release();
}

// IDevice

BOOL METHOD CUsbSerialCdc::Open(void)
{
	BindDriver(m_pDriver);

	m_pTimer->SetPeriod(5);

	m_pTimer->SetHook(this, 0);

	return TRUE;
}

// ISerialPort

void METHOD CUsbSerialCdc::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
}

UINT METHOD CUsbSerialCdc::GetPhysicalMask(void)
{
	return Bit(physicalRS232);
}

BOOL METHOD CUsbSerialCdc::Open(CSerialConfig const &Config)
{
	if( !m_fOpen ) {

		m_Config = Config;

		LockPnp();

		InitDriver();

		m_fOpen = true;

		m_pHandler->OnOpen(Config);

		m_pTimer->Enable(true);

		StartDriver();

		FreePnp();

		return true;
	}

	return false;
}

void METHOD CUsbSerialCdc::Close(void)
{
	if( m_fOpen ) {

		LockPnp();

		m_fOpen = false;

		WaitIdle(5000);

		StopDriver();

		TermDriver();

		m_pTimer->Enable(false);

		m_pHandler->OnClose();

		FreePnp();
	}
}

void METHOD CUsbSerialCdc::Send(BYTE bData)
{
	if( m_fOpen ) {

		UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

		m_fTxDone    = false;

		m_bTxData[0] = bData;

		m_uTxCount   = 1;

		if( m_uTxState == stateIdle ) {

			Hal_LowerIrql(uSave);

			PullData();

			StartSend();

			return;
		}

		Hal_LowerIrql(uSave);
	}
}

void METHOD CUsbSerialCdc::SetBreak(BOOL fBreak)
{
	LockPnp();

	if( IsPresent() && IsRunning() ) {

		m_pDriver->SendBreak(50);
	}

	FreePnp();
}

void METHOD CUsbSerialCdc::SetOutput(UINT uOutput, BOOL fOn)
{
	LockPnp();

	if( IsPresent() && IsRunning() ) {

		m_pDriver->SetControlLineState(true, fOn);
	}

	FreePnp();
}

// IUsbHostFuncEvents

void METHOD CUsbSerialCdc::OnData(void)
{
	if( m_fOpen ) {

		OnSend();

		OnRecv();
	}
}

// IEventSink

void CUsbSerialCdc::OnEvent(UINT uLine, UINT uParam)
{
	if( m_fOpen ) {

		m_pHandler->OnTimer();
	}
}

// Overridables 

void CUsbSerialCdc::OnDriverBind(IUsbHostFuncDriver *pDriver)
{
	m_pDriver = (IUsbHostAcm *) pDriver;

	m_pDriver->Bind(this);
}

void CUsbSerialCdc::OnInitDriver(void)
{
	m_pDriver->SetLineCoding(m_Config);

	m_pDriver->SetControlLineState(true, true);
}

void CUsbSerialCdc::OnStartDriver(void)
{
	m_uRxState = stateIdle;

	m_uTxState = stateIdle;

	m_fTxDone  = true;

	StartRecv();
}

void CUsbSerialCdc::OnStopDriver(void)
{
}

void CUsbSerialCdc::OnTermDriver(void)
{
	m_pDriver->KillAsync();
}

// Implementation

void CUsbSerialCdc::OnRecv(void)
{
	if( m_uRxState == stateWait ) {

		m_uRxCount = m_pDriver->WaitRecv(0);

		if( m_uRxCount != NOTHING ) {

			m_uRxState = stateIdle;

			PushData();

			StartRecv();
		}
	}
}

void CUsbSerialCdc::OnSend(void)
{
	if( m_uTxState == stateWait ) {

		if( m_pDriver->WaitSend(0) != NOTHING ) {

			m_uTxState = stateIdle;

			if( !m_fTxDone ) {

				PullData();
			}

			if( m_uTxCount ) {

				StartSend();
			}
		}
	}
}

void CUsbSerialCdc::StartSend(void)
{
	LockPnp();

	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( IsPresent() && IsRunning() ) {

		if( m_pDriver->SendData(m_bTxData, m_uTxCount, true) != NOTHING ) {

			m_uTxState = stateWait;
		}
	}

	m_uTxCount = 0;

	Hal_LowerIrql(uSave);

	FreePnp();
}

void CUsbSerialCdc::StartRecv(void)
{
	LockPnp();

	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( IsPresent() && IsRunning() ) {

		if( m_pDriver->RecvData(m_bRxData, sizeof(m_bRxData), true) != NOTHING ) {

			m_uRxState = stateWait;
		}
	}

	Hal_LowerIrql(uSave);

	FreePnp();
}

void CUsbSerialCdc::PushData(void)
{
	if( m_uRxCount ) {

		for( UINT i = 0; i < m_uRxCount; i++ ) {

			m_pHandler->OnRxData(m_bRxData[i]);
		}

		m_pHandler->OnRxDone();

		m_uRxCount = 0;
	}
}

void CUsbSerialCdc::PullData(void)
{
	while( m_uTxCount < sizeof(m_bTxData) ) {

		if( m_pHandler->OnTxData(m_bTxData[m_uTxCount]) ) {

			m_uTxCount++;

			continue;
		}

		m_fTxDone = true;

		m_pHandler->OnTxDone();

		break;
	}
}

void CUsbSerialCdc::WaitIdle(UINT uTimeout)
{
	if( m_uTxState != stateIdle ) {

		LockPnp();

		if( IsPresent() ) {

			m_pDriver->WaitSend(uTimeout);
		}

		FreePnp();
	}
}

// End of File
