
#include "Intern.hpp"

#include "UsbSerialFtdiExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial Ftdi Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbSerialFtdiExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbSerialFtdiExpansion(pDriver);

	return p;
	}

// Constructor

CUsbSerialFtdiExpansion::CUsbSerialFtdiExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansionSerial(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbSerialFtdiExpansion::GetName(void)
{
	switch( m_wProduct ) {

		case pidSerial232485:	return "RS232/485 Option Card";
		case pidSerial232:	return "RS232 Option Card";
		case pidSerial485:	return "RS485 Option Card";
		}

	return NULL;
	}

UINT METHOD CUsbSerialFtdiExpansion::GetClass(void)
{
	return rackClassComms;
	}

UINT METHOD CUsbSerialFtdiExpansion::GetPower(void)
{
	switch( m_wProduct ) {

		case pidSerial232485:	return 0;
		case pidSerial232:	return 0;
		case pidSerial485:	return 0;
		}

	return 0;
	}

// IExpansionSerial

UINT METHOD CUsbSerialFtdiExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD METHOD CUsbSerialFtdiExpansion::GetPortMask(void)
{
	switch( m_wProduct ) {

		case pidSerial232:

			return Bit(physicalRS232);

		case pidSerial485:

			return Bit(physicalRS422Master) | Bit(physicalRS422Slave) | Bit(physicalRS485);

		case pidSerial232485:

			return m_uIndex ? Bit(physicalRS422Master) | Bit(physicalRS422Slave) | Bit(physicalRS485) : Bit(physicalRS232);			
		}

	return physicalNone;
	}

UINT METHOD CUsbSerialFtdiExpansion::GetPortType(UINT iPort)
{
	switch( m_wProduct ) {

		case pidSerial232:

			return physicalRS232;

		case pidSerial485:

			return physicalRS485;

		case pidSerial232485:

			return m_uIndex ? physicalRS485 : physicalRS232;			
		}

	return physicalNone;
	}

// End of File
