
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbExpansionSerial_HPP

#define	INCLUDE_UsbExpansionSerial_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Expansion Serial Object
//

class CUsbExpansionSerial : public CUsbExpansion, public IExpansionSerial
{
	public:
		// Constructor
		CUsbExpansionSerial(IUsbHostFuncDriver *pDriver);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExpansionInterface
		PCTXT		METHOD GetName(void);
		UINT	        METHOD GetIdent(void);
		UINT		METHOD GetClass(void);
		UINT		METHOD GetIndex(void);
		UINT		METHOD GetPower(void);
		BOOL		METHOD HasBootLoader(void);
		IDevice       * METHOD MakeObject(IUsbHostFuncDriver *pDrv);
		IExpansionPnp * METHOD QueryPnpInterface(IDevice *pObj);

		// IExpansionSerial
		UINT  METHOD GetPortCount(void);
		DWORD METHOD GetPortMask(void);
		UINT  METHOD GetPortType(UINT iPort);
	};

// End of File

#endif


