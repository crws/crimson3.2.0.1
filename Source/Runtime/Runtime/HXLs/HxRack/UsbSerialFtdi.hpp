
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSerialFtdi_HPP

#define	INCLUDE_UsbSerialFtdi_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial Ftdi Port
//

class CUsbSerialFtdi : public CUsbPort, public IEventSink
{
public:
	// Constructor
	CUsbSerialFtdi(IUsbHostFuncDriver *pDriver);

	// Destructor
	~CUsbSerialFtdi(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void  METHOD Bind(IPortHandler *pHandler);
	void  METHOD Bind(IPortSwitch  *pSwitch);
	UINT  METHOD GetPhysicalMask(void);
	BOOL  METHOD Open(CSerialConfig const &Config);
	void  METHOD Close(void);
	void  METHOD Send(BYTE bData);
	void  METHOD SetOutput(UINT uOutput, BOOL fOn);
	BOOL  METHOD GetInput(UINT uInput);

	// IUsbHostFuncEvents
	void METHOD OnData(void);

	// IEventSink
	void OnEvent(UINT uLine, UINT uParam);

protected:
	// Recv Poll State
	enum
	{
		stateIdle,
		stateRecv,
		stateSend,
		stateWait,
		stateFail,
	};

	// Data Members
	IUsbHostFtdi  * m_pDriver;
	IPortHandler  * m_pHandler;
	CSerialConfig   m_Config;
	IPortSwitch   * m_pSwitch;
	ITimer        * m_pTimer;
	bool	        m_fOpen;
	BYTE		m_bTxData[512];
	BYTE		m_bRxData[512];
	UINT            m_uRxCount;
	UINT	        m_uTxCount;
	UINT  volatile  m_uSendState;
	UINT  volatile  m_uRecvState;
	bool		m_fCts;
	bool		m_fClosing;
	UINT            m_uSaveIrql;

	// Implementation
	bool InitUart(void);
	bool InitBaudRate(void);
	bool InitFormat(void);
	bool InitPhysical(void);
	void OnRecv(void);
	void OnSend(void);
	void StartSend(void);
	void StartRecv(void);
	void WaitIdle(void);
};

// End of File

#endif
