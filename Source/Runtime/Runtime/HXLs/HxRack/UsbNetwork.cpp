
#include "Intern.hpp"

#include "UsbNetwork.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Network Port
//

// Instantiator

INic * Create_UsbNetwork(IUsbHostFuncDriver *pDriver)
{
	CUsbNetwork *p = New CUsbNetwork(pDriver);

	p->Open();

	return p;
	}

// Constructor

CUsbNetwork::CUsbNetwork(IUsbHostFuncDriver *pDriver)
{
	StdSetRef();

	m_pNetwork = (IUsbNetwork *) pDriver;

	m_fOpen    = false;

	m_pNetwork->AddRef();

	DiagRegister();
	}

// Destructor

CUsbNetwork::~CUsbNetwork(void)
{
	DiagRevoke();

	m_pNetwork->Release();
	}

// IUnknown

HRESULT METHOD CUsbNetwork::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (INic);

	StdQueryInterface(INic);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG METHOD CUsbNetwork::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CUsbNetwork::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CUsbNetwork::Open(void)
{
	return TRUE;
	}

// INic

bool METHOD CUsbNetwork::Open(bool fFast, bool fFull)
{
	if( m_pNetwork->Startup(fFast, fFull) ) {

		m_fOpen = true;

		m_pNetwork->SetRemoveLock(true);

		return true;
		}
	
	return false;
	}

bool METHOD CUsbNetwork::Close(void)
{
	if( m_pNetwork->Shutdown() ) {

		m_fOpen = false;

		m_pNetwork->SetRemoveLock(false);

		return true;
		}

	return false;
	}

bool METHOD CUsbNetwork::InitMac(MACADDR const &Addr)
{
	return m_pNetwork->InitMac(Addr);
	}

void METHOD CUsbNetwork::ReadMac(MACADDR &Addr)
{
	m_pNetwork->ReadMac(Addr);
	}

UINT METHOD CUsbNetwork::GetCapabilities(void)
{
	return nicFilterIp | nicCheckSums | nicAddSums;
	}

void METHOD CUsbNetwork::SetFlags(UINT uFlags)
{
	m_pNetwork->SetFlags(uFlags);
	}

bool METHOD CUsbNetwork::SetMulticast(MACADDR const *pList, UINT uList)
{
	return m_pNetwork->SetMulticast(pList, uList);
	}

bool METHOD CUsbNetwork::IsLinkActive(void)
{
	return m_pNetwork->IsLinkActive();
	}

bool METHOD CUsbNetwork::WaitLink(UINT uTime)
{
	return m_pNetwork->WaitLink(uTime);
	}

bool METHOD CUsbNetwork::SendData(CBuffer *pBuff, UINT uTime)
{
	return m_pNetwork->SendData(pBuff);
	}

bool METHOD CUsbNetwork::ReadData(CBuffer *&pBuff, UINT uTime)
{
	return m_pNetwork->RecvData(pBuff);
	}

void METHOD CUsbNetwork::GetCounters(NICDIAG &Diag)
{
	return m_pNetwork->GetCounters(Diag);
	}

void METHOD CUsbNetwork::ResetCounters(void)
{
	}

// IDiagProvider

UINT METHOD CUsbNetwork::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}

// Implementation

bool CUsbNetwork::DiagRegister(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "nic1");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		return true;
		}

	#endif

	return false;
	}

bool CUsbNetwork::DiagRevoke(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return true;
		}

	#endif

	return false;
	}

UINT CUsbNetwork::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		if( m_fOpen ) {

			NICDIAG Diag;

			m_pNetwork->GetCounters(Diag);

			pOut->AddProp("RxCount",   "%u", Diag.m_RxCount);
			pOut->AddProp("RxDisc",    "%u", Diag.m_RxDisc);
			pOut->AddProp("RxOver",    "%u", Diag.m_RxOver);
			pOut->AddProp("TxCount",   "%u", Diag.m_TxCount);
			pOut->AddProp("TxDisc",    "%u", Diag.m_TxDisc);
			pOut->AddProp("TxFail",    "%u", Diag.m_TxFail);
			}

		pOut->EndPropList();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

// End of File
