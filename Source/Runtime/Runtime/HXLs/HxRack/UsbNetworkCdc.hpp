
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbNetworkCdc_HPP
	
#define	INCLUDE_UsbNetworkCdc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "UsbModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Network Cdc
//

class CUsbNetworkCdc : public CUsbModule, public INic, public IDiagProvider
{
	public:
		// Constructor
		CUsbNetworkCdc(IUsbHostFuncDriver *pDriver);

		// Destructor
		~CUsbNetworkCdc(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);
		
		// INic
		bool METHOD Open(bool fFast, bool fFull);
		bool METHOD Close(void);
		bool METHOD InitMac(MACADDR const &Addr);
		void METHOD ReadMac(MACADDR &Addr);
		UINT METHOD GetCapabilities(void);
		void METHOD SetFlags(UINT uFlags);
		bool METHOD SetMulticast(MACADDR const *pList, UINT uList);
		bool METHOD IsLinkActive(void);
		bool METHOD WaitLink(UINT uTime);
		bool METHOD SendData(CBuffer *  pBuff, UINT uTime);
		bool METHOD ReadData(CBuffer * &pBuff, UINT uTime);
		void METHOD GetCounters(NICDIAG &Diag);
		void METHOD ResetCounters(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		IUsbHostEcm * m_pNetwork;
		ISemaphore  * m_pTxLimit;
		ISemaphore  * m_pRxLimit;
		UINT          m_uFlags;
		UINT          m_uFilters;
		BYTE          m_bData[2048];
		MACADDR	    * m_pMulti;
		UINT	      m_uMulti;
		UINT          m_uProv;

		// Packet Filter
		enum
		{
			typeMulticast	 = Bit(4),
			typeBroadcast	 = Bit(3),
			typeDirected	 = Bit(2),
			typeMulticastAll = Bit(1),
			typeAll		 = Bit(0),
			};

		// Overridables
		void OnDriverBind(IUsbHostFuncDriver *pDriver);
		void OnInitDriver(void);

		// Implementation
		bool TakeFrame(PBYTE pData);
		void FreeMulti(void);

		// Diagnostics
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
