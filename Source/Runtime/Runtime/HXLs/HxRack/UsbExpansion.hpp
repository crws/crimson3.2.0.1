
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbExpansion_HPP

#define	INCLUDE_UsbExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Expansion Object
//

class CUsbExpansion : public IExpansionInterface
{
	public:
		// Constructor
		CUsbExpansion(IUsbHostFuncDriver *pDriver);

		// Destructor
		virtual ~CUsbExpansion(void);
		
		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExpansionInterface
		PCTXT		METHOD GetName(void);
		UINT	        METHOD GetIdent(void);
		UINT		METHOD GetClass(void);
		UINT		METHOD GetIndex(void);
		UINT		METHOD GetPower(void);
		BOOL		METHOD HasBootLoader(void);
		IDevice       * METHOD MakeObject(IUsbHostFuncDriver *pDrv);
		IExpansionPnp * METHOD QueryPnpInterface(IDevice *pObj);

	protected:
		// Data
		ULONG m_uRefs;
		UINT  m_uIndex;
		WORD  m_wVendor;
		WORD  m_wProduct;
		WORD  m_wProtocol;
	};

// End of File

#endif


