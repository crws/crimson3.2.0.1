
#include "Intern.hpp"

#include "UsbCdl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb CAN Port
//

// Instantiator

IPortObject * Create_UsbCdl(IUsbHostFuncDriver *pDriver)
{
	CUsbCdl *p = New CUsbCdl(pDriver);

	p->Open();

	return p;
}

// Constructor

CUsbCdl::CUsbCdl(IUsbHostFuncDriver *pDriver)
{
}

// Destructor

CUsbCdl::~CUsbCdl(void)
{
}

// IPortObject

UINT METHOD CUsbCdl::GetPhysicalMask(void)
{
	return Bit(physicalCatLink);
}

// End of File
