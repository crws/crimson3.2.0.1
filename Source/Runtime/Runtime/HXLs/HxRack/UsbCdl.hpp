
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbCdl_HPP

#define	INCLUDE_UsbCdl_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb CDL Port
//

class CUsbCdl : public CUsbPort
{
public:
	// Constructor
	CUsbCdl(IUsbHostFuncDriver *pDriver);

	// Destructor
	~CUsbCdl(void);

	// IPortObject
	UINT METHOD GetPhysicalMask(void);
};

// End of File

#endif
