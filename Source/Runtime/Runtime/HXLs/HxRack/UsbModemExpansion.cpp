
#include "Intern.hpp"

#include "UsbModemExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Modem Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbModemExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbModemExpansion(pDriver);

	return p;
	}

// Constructor

CUsbModemExpansion::CUsbModemExpansion(IUsbHostFuncDriver *pDriver) : CUsbSerialCdcExpansion(pDriver)
{
	}

// IExpansionInterface

PCTXT CUsbModemExpansion::GetName(void)
{
	if( m_wVendor == vidTelit ) {
		
		if( m_wProduct == pidTelitHspa ) {

			return "GMHSPA";
			}
		
		if( m_wProduct == picTelitLte ) {
			
			return "LTE-ACM";
			}
		}

	return CUsbSerialCdcExpansion::GetName();
	}

UINT CUsbModemExpansion::GetPower(void)
{
	if( m_wVendor == vidTelit ) {
		
		if( m_wProduct == pidTelitHspa ) {

			return 85;
			}
		
		if( m_wProduct == picTelitLte ) {
			
			return 50;
			}
		}

	return 0;
	}

IDevice * CUsbModemExpansion::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return Create_UsbSerialCdc(pDriver);
	}

// IExpansionSerial

UINT CUsbModemExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD CUsbModemExpansion::GetPortMask(void)
{
	return Bit(physicalRS232);
	}

UINT CUsbModemExpansion::GetPortType(UINT iPort)
{
	return rackSerial;
	}

// End of File
