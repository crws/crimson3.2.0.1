
#include "Intern.hpp"

#include "UsbGenericExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Generic Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbGenericExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbGenericExpansion(pDriver);

	return p;
	}

// Constructor

CUsbGenericExpansion::CUsbGenericExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansionSerial(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbGenericExpansion::GetName(void)
{
	switch( m_wProtocol ) {

		case protoCan : return "GMCAN*";
		case protoCdl : return "GMCDL*";
		case protoDnp3: return "GMDNP*";
		case protoTest: return "GMDNP*";
		}
	
	return "GMREM";
	}

UINT METHOD CUsbGenericExpansion::GetClass(void)
{
	return rackClassComms;
	}

UINT METHOD CUsbGenericExpansion::GetPower(void)
{
	return 12;
	}

BOOL METHOD CUsbGenericExpansion::HasBootLoader(void)
{
	return TRUE;
	}

IDevice * METHOD CUsbGenericExpansion::MakeObject(IUsbHostFuncDriver *pDrv)
{
	return Create_UsbGeneric(pDrv);
	}

// IExpansionSerial

UINT METHOD CUsbGenericExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD METHOD CUsbGenericExpansion::GetPortMask(void)
{
	switch( m_wProtocol ) {

		case protoCan : return Bit(physicalRS485);
		case protoCdl : return Bit(physicalRS485);
		case protoTest: return Bit(physicalRS485);
		}

	return 0;
	}

UINT METHOD CUsbGenericExpansion::GetPortType(UINT iPort)
{
	switch( m_wProtocol ) {

		case protoCan : return rackCan;
		case protoCdl : return rackCatLink;
		case protoTest: return rackTest;
		}
		
	return rackNone;
	}

// End of File
