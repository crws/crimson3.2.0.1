
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSerialFtdiExpansion_HPP

#define	INCLUDE_UsbSerialFtdiExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansionSerial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial Ftdi Expansion Object
//

class CUsbSerialFtdiExpansion : public CUsbExpansionSerial
{
	public:
		// Constructor
		CUsbSerialFtdiExpansion(IUsbHostFuncDriver *pDriver);

		// IExpansionInterface
		PCTXT METHOD GetName(void);
		UINT  METHOD GetIdent(void);
		UINT  METHOD GetClass(void);
		UINT  METHOD GetIndex(void);
		UINT  METHOD GetPower(void);

		// IExpansionSerial
		UINT  METHOD GetPortCount(void);
		DWORD METHOD GetPortMask(void);
		UINT  METHOD GetPortType(UINT iPort);
	};

// End of File

#endif


