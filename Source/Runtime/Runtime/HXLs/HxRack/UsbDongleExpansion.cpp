
#include "Intern.hpp"

#include "UsbDongleExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Dongle Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbDongleExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = New CUsbDongleExpansion(pDriver);

	return p;
	}

// Constructor

CUsbDongleExpansion::CUsbDongleExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansion(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbDongleExpansion::GetName(void)
{
	switch( m_wProduct ) {

		case pidDnp3Dongle:  return "GMLIC";
		}
	
	return "Unknown Dongle";
	}

UINT METHOD CUsbDongleExpansion::GetClass(void)
{
	return rackClassLicense;
	}

UINT METHOD CUsbDongleExpansion::GetPower(void)
{
	switch( m_wProduct ) {

		case pidDnp3Dongle:  return 11;
		}

	return 0;
	}

// End of File
