
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbRackExpansion_HPP

#define	INCLUDE_UsbRackExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Rack Expansion Object
//

class CUsbRackExpansion : public CUsbExpansion, public IExpansionRack
{
	public:
		// Constructor
		CUsbRackExpansion(IUsbHostFuncDriver *pDriver);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExpansionInterface
		PCTXT		METHOD GetName(void);
		UINT	        METHOD GetIdent(void);
		UINT		METHOD GetClass(void);
		UINT		METHOD GetIndex(void);
		UINT		METHOD GetPower(void);
		BOOL		METHOD HasBootLoader(void);
		IDevice       * METHOD MakeObject(IUsbHostFuncDriver *pDrv);
		IExpansionPnp * METHOD QueryPnpInterface(IDevice *pObj);

		// ExpansionRack
		void METHOD RegisterPower(IExpansionInterface *pDriver);
		void METHOD UnregisterPower(IExpansionInterface *pDriver);

	protected:
		// Data
		IUsbHubDriver * m_pHub;
		UINT            m_uPower;

		// Implementation
		UINT GetMaxPower(void);
		void KillRackPower(void);
	};

// End of File

#endif


