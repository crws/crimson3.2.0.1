
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSerialCdc_HPP

#define	INCLUDE_UsbSerialCdc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial CDC ACM Port
//

class CUsbSerialCdc : public CUsbPort, public IEventSink
{
public:
	// Constructor
	CUsbSerialCdc(IUsbHostFuncDriver *pDriver);

	// Destructor
	~CUsbSerialCdc(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void METHOD Bind(IPortHandler *pHandler);
	UINT METHOD GetPhysicalMask(void);
	BOOL METHOD Open(CSerialConfig const &Config);
	void METHOD Close(void);
	void METHOD Send(BYTE bData);
	void METHOD SetBreak(BOOL fBreak);
	void METHOD SetOutput(UINT uOutput, BOOL fOn);

	// IUsbHostFuncEvents
	void METHOD OnData(void);

	// IEventSink
	void OnEvent(UINT uLine, UINT uParam);

protected:
	// Recv Poll State
	enum
	{
		stateIdle,
		stateRecv,
		stateWait,
		stateData,
	};

	// Data Members
	IUsbHostAcm   * m_pDriver;
	IPortHandler  * m_pHandler;
	CSerialConfig   m_Config;
	ITimer        * m_pTimer;
	BYTE		m_bTxData[512];
	BYTE		m_bRxData[2048];
	UINT            m_uRxCount;
	UINT            m_uTxCount;
	UINT volatile   m_uRxState;
	UINT volatile   m_uTxState;
	BOOL volatile   m_fTxDone;

	// Overridables
	void OnDriverBind(IUsbHostFuncDriver *pDriver);
	void OnInitDriver(void);
	void OnStartDriver(void);
	void OnStopDriver(void);
	void OnTermDriver(void);

	// Implementation
	void OnRecv(void);
	void OnSend(void);
	void StartSend(void);
	void StartRecv(void);
	void PushData(void);
	void PullData(void);
	void WaitIdle(UINT uTimeout);
};

// End of File

#endif
