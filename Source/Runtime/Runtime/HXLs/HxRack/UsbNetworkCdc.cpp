
#include "Intern.hpp"

#include "UsbNetworkCdc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Network Port
//

// Instantiator

IDevice * Create_UsbNetworkCdc(IUsbHostFuncDriver *pDriver)
{
	CUsbNetworkCdc *p = New CUsbNetworkCdc(pDriver);

	p->Open();

	return p;
	}

// Constructor

CUsbNetworkCdc::CUsbNetworkCdc(IUsbHostFuncDriver *pDriver)
{
	StdSetRef();

	m_pNetwork = (IUsbHostEcm *) pDriver;

	m_uFlags   = 0;

	m_pMulti   = NULL;

	m_uMulti   = 0;

	m_pRxLimit = Create_Semaphore();

	m_pTxLimit = Create_Semaphore();

	m_uFilters = typeBroadcast | typeDirected;

	DiagRegister();
	}

// Destructor

CUsbNetworkCdc::~CUsbNetworkCdc(void)
{
	DiagRevoke();

	m_pTxLimit->Release();

	m_pRxLimit->Release();

	FreeMulti();
	}

// IUnknown

HRESULT METHOD CUsbNetworkCdc::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(INic);

	StdQueryInterface(IDiagProvider);

	return CUsbModule::QueryInterface(riid, ppObject);
	}

ULONG METHOD CUsbNetworkCdc::AddRef(void)
{
	return CUsbModule::AddRef();
	}

ULONG METHOD CUsbNetworkCdc::Release(void)
{
	return CUsbModule::Release();
	}

// IDevice

BOOL METHOD CUsbNetworkCdc::Open(void)
{
	BindDriver(m_pNetwork);

	return TRUE;
	}

// INic

bool METHOD CUsbNetworkCdc::Open(bool fFast, bool fFull)
{
	if( !m_fOpen ) {

		LockPnp();

		m_fOpen = true;

		InitDriver();

		StartDriver();

		m_pRxLimit->Signal(1);

		m_pTxLimit->Signal(1);

		FreePnp();

		return true;
		}
	
	return false;
	}

bool METHOD CUsbNetworkCdc::Close(void)
{
	if( m_fOpen ) {

		LockPnp();

		m_fOpen = false;

		StopDriver();

		TermDriver();

		m_pRxLimit->Wait(FOREVER);

		m_pTxLimit->Wait(FOREVER);
		
		FreePnp();

		return true;
		}

	return false;
	}

bool METHOD CUsbNetworkCdc::InitMac(MACADDR const &Addr)
{
	return false;
	}

void METHOD CUsbNetworkCdc::ReadMac(MACADDR &Addr)
{
	// TODO -- Notify change.

	LockPnp();

	if( !IsRemoved() ) {

		m_pNetwork->GetMac(Addr);
		}

	FreePnp();
	}

UINT METHOD CUsbNetworkCdc::GetCapabilities(void)
{
	return nicFilterIp | nicCheckSums | nicAddSums;
	}

void METHOD CUsbNetworkCdc::SetFlags(UINT uFlags)
{
	m_uFlags = uFlags;
	}

bool METHOD CUsbNetworkCdc::SetMulticast(MACADDR const *pList, UINT uList)
{
	FreeMulti();

	LockPnp();

	if( IsPresent() ) {

		if( m_pNetwork->GetFilterHashing() ) {

			if( pList && uList ) {

				m_pMulti = New MACADDR[ uList ];

				m_uMulti = uList;

				memcpy(m_pMulti, pList, sizeof(MACADDR) * m_uMulti);

				m_uFilters |= typeMulticast;

				if( m_pNetwork->SetMulticastFilters(pList, uList) ) {

					if( m_pNetwork->SetPacketFilters(m_uFilters) ) {

						FreePnp();

						return true;
						}
					}
				}
			else {
				m_uFilters &= ~typeMulticast;

				m_uFilters &= ~typeMulticastAll;

				if( m_pNetwork->SetPacketFilters(m_uFilters) ) {

					if( m_pNetwork->SetMulticastFilters(NULL, 0) ) {

						FreePnp();

						return true;
						}
					}
				}
			}
		}

	FreePnp();

	return false;
	}

bool METHOD CUsbNetworkCdc::IsLinkActive(void)
{
	LockPnp();

	if( IsPresent() && IsRunning() ) {
		
		if( m_pNetwork->IsLinkActive() ) {

			FreePnp();

			return true;
			}
		}

	FreePnp();

	return false;
	}

bool METHOD CUsbNetworkCdc::WaitLink(UINT uTime)
{
	for(;;) {

		LockPnp();

		if( IsPresent() && IsRunning() ) {

			if( m_pNetwork->WaitLinkActive(uTime) ) {

				FreePnp();

				return true;
				}

			FreePnp();

			return false;
			}

		UINT uDelay = Min(uTime, UINT(100));

		if( uDelay ) {

			Sleep(uDelay);

			if( uTime != NOTHING ) {

				uTime -= uDelay;
				}
				
			continue;
			}
		
		return false;
		}
	}

bool METHOD CUsbNetworkCdc::SendData(CBuffer *pBuff, UINT uTime)
{
	LockPnp();

	if( IsPresent() && IsRunning() ) {

		if( IsLinkActive() ) {

			if( m_pTxLimit->Wait(uTime) ) {

				if( m_pNetwork->SendData(pBuff->GetData(), pBuff->GetSize(), false) ) {

					m_pTxLimit->Signal(1);
			
					FreePnp();
				
					return true;
					}

				m_pTxLimit->Signal(1);
				}
			}
		}

	FreePnp();

	return false;
	}

bool METHOD CUsbNetworkCdc::ReadData(CBuffer *&pBuff, UINT uTime)
{
	LockPnp();

	if( IsPresent() && IsRunning() ) {

		if( IsLinkActive() ) {

			while( m_pRxLimit->Wait(uTime) ) {

				UINT uCount = m_pNetwork->RecvData(m_bData, sizeof(m_bData), false);

				if( uCount != NOTHING && uCount != 0 ) {

					if( TakeFrame(m_bData) ) {

						BOOL fIp = MotorToHost((WORD &) m_bData[12]) == 0x0800;

						if( !(m_uFlags & nicOnlyIp) || fIp ) {

							CBuffer *pWork = BuffAllocate(uCount);

							if( pWork ) {

								PBYTE pDest = pWork->AddTail(uCount);

								memcpy(pDest, m_bData, uCount);

								if( fIp ) {

									pWork->SetFlag(packetTypeIp);
									}

								pWork->SetFlag(packetTypeValid);

								pWork->SetFlag(packetSumsValid);

								pBuff = pWork;

								m_pRxLimit->Signal(1);

								FreePnp();

								return true;
								}
							}
						}

					m_pRxLimit->Signal(1);

					continue;
					}

				m_pRxLimit->Signal(1);
			
				break;
				}
			}
		}

	FreePnp();

	pBuff = NULL;

	return false;
	}

void METHOD CUsbNetworkCdc::GetCounters(NICDIAG &Diag)
{
	LockPnp();

	if( !IsRemoved() ) { 

		m_pNetwork->GetStatistics(0x01, (DWORD &) Diag.m_TxCount);

		m_pNetwork->GetStatistics(0x03, (DWORD &) Diag.m_TxFail );
	
		m_pNetwork->GetStatistics(0x18, (DWORD &) Diag.m_TxDisc );
	
		m_pNetwork->GetStatistics(0x02, (DWORD &) Diag.m_RxCount);
	
		m_pNetwork->GetStatistics(0x19, (DWORD &) Diag.m_RxOver );
	
		m_pNetwork->GetStatistics(0x12, (DWORD &) Diag.m_RxDisc );
		}

	FreePnp();
	}

void METHOD CUsbNetworkCdc::ResetCounters(void)
{
	}

// Overridables 

void CUsbNetworkCdc::OnDriverBind(IUsbHostFuncDriver *pDriver)
{
	m_pNetwork = (IUsbHostEcm *) pDriver;
	}

void CUsbNetworkCdc::OnInitDriver(void)
{
	m_pNetwork->SetMulticastFilters(m_pMulti, m_uMulti);

	m_pNetwork->SetPacketFilters(m_uFilters);
	}

// IDiagProvider

UINT METHOD CUsbNetworkCdc::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}

// Implementation

bool CUsbNetworkCdc::TakeFrame(PBYTE pData)
{
	if( m_pMulti ) {

		// NOTE -- If we have multicast enabled, we have to filter for
		// addresses that get by the hash filter in the chip but which are
		// not included in our multicast list. Note that we explicitly
		// test for broadcast and allow it, as it won't be in the list but
		// it does meet the definition of the multicast address.

		MACADDR const *pAddr = (MACADDR const *) pData;

		if( pAddr->m_Addr[0] & 0x01 ) {

			static MACADDR Cast = { { 0xFF, 0xFF,
						  0xFF, 0xFF,
						  0xFF, 0xFF
						  } };

			if( memcmp(pAddr, &Cast, sizeof(MACADDR)) ) {

				UINT n;

				for( n = 0; n < m_uMulti; n++ ) {

					if( !memcmp(pAddr, m_pMulti + n, sizeof(MACADDR)) ) {

						break;
						}
					}

				if( n == m_uMulti ) {

					return false;
					}
				}
			}
		}

	return true;
	}

void CUsbNetworkCdc::FreeMulti(void)
{
	if( m_pMulti ) {

		delete [] m_pMulti;

		m_pMulti = NULL;

		m_uMulti = 0;
		}
	}

// Diagnostics

bool CUsbNetworkCdc::DiagRegister(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "nic2");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		return true;
		}

	#endif

	return false;
	}

bool CUsbNetworkCdc::DiagRevoke(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return true;
		}

	#endif

	return false;
	}

UINT CUsbNetworkCdc::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		if( m_fOpen ) {

			NICDIAG Diag;

			GetCounters(Diag);

			pOut->AddProp("RxCount", "%u", Diag.m_RxCount);
			pOut->AddProp("RxDisc",  "%u", Diag.m_RxDisc);
			pOut->AddProp("RxOver",  "%u", Diag.m_RxOver);
			pOut->AddProp("TxCount", "%u", Diag.m_TxCount);
			pOut->AddProp("TxDisc",  "%u", Diag.m_TxDisc);
			pOut->AddProp("TxFail",  "%u", Diag.m_TxFail);
			}

		pOut->EndPropList();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

// End of File
