
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Shared_UsbModule_HPP
	
#define	INCLUDE_Shared_UsbModule_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Module
//

class CUsbModule : public IExpansionPnp
{
	public:
		// Constructor
		CUsbModule(void);

		// Destructor
		virtual ~CUsbModule(void);

		// Attributes
		bool  IsPresent(void) const;
		bool  IsRemoved(void) const;
		bool  IsRunning(void) const;
		DWORD GetHandle(void) const;

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExpansionPnp
		void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver);
		void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver);

	protected:
		// Data 
		ULONG                m_uRefs;
		IEvent             * m_pEvent;
		IUsbHostFuncDriver * m_pFuncDriver;
		bool		     m_fOpen;
		bool		     m_fFuncOpen;
		int		     m_nLockPnp;
		bool		     m_fRemovePend;
		UINT		     m_uSaveIrql;

		// Pnp
		void LockPnp(void);
		void FreePnp(void);

		// Driver
		void BindDriver(IUsbHostFuncDriver *pDriver);
		void InitDriver(void);
		void StartDriver(void);
		void StopDriver(void);
		void TermDriver(void);
		void FreeDriver(void);

		// Overridables
		virtual void OnDriverBind(IUsbHostFuncDriver *pDriver);
		virtual void OnInitDriver(void);
		virtual void OnStartDriver(void);
		virtual void OnStopDriver(void);
		virtual void OnTermDriver(void);

		// Implementation
		void EnableInterrupts(bool fEnable);	
	};

// End of File

#endif
