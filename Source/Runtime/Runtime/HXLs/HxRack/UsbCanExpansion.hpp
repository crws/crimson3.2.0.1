
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbCanExpansion_HPP

#define	INCLUDE_UsbCanExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansionSerial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Can Expansion Object
//

class CUsbCanExpansion : public CUsbExpansionSerial
{
	public:
		// Constructor
		CUsbCanExpansion(IUsbHostFuncDriver *pDriver);

		// IExpansionInterface
		PCTXT	  METHOD GetName(void);
		UINT	  METHOD GetClass(void);
		UINT	  METHOD GetPower(void);
		BOOL	  METHOD HasBootLoader(void);
		IDevice * METHOD MakeObject(IUsbHostFuncDriver *pDrv);

		// IExpansionSerial
		UINT  METHOD GetPortCount(void);
		DWORD METHOD GetPortMask(void);
		UINT  METHOD GetPortType(UINT iPort);
	};

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IPortObject * Create_UsbCan(IUsbHostFuncDriver *pDriver, WORD wProtocol);

// End of File

#endif


