
#include "Intern.hpp"

#include "UsbDevNet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb DeviceNet Port
//

// Instantiator

IPortObject * Create_UsbDevNet(IUsbHostFuncDriver *pDriver)
{
	CUsbDevNet *p = New CUsbDevNet(pDriver);

	p->Bind();

	return (IDeviceNetPort *) p;
	}

// Constructor

CUsbDevNet::CUsbDevNet(IUsbHostFuncDriver *pDriver)
{
	m_pDriver   = (IUsbHostDevNet *) pDriver;

	m_uBaud     = 0;

	m_bMac      = 0;
	
	m_wBitRecv  = 0;
	
	m_wBitSend  = 0;
	
	m_wPollRecv = 0;
	
	m_wPollSend = 0;
	
	m_wDataRecv = 0;
	
	m_wDataSend = 0;

	m_pTimer    = CreateTimer();
	}

// Destructor

CUsbDevNet::~CUsbDevNet(void)
{
	m_pTimer->Release();
	}

// Bind

void CUsbDevNet::Bind(void)
{
	BindDriver(m_pDriver);

	m_pTimer->SetPeriod(100);

	m_pTimer->SetHook(this, 0);
	}

// IUnknown

HRESULT METHOD CUsbDevNet::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDeviceNetPort);

	return CUsbPort::QueryInterface(riid, ppObject);
	}

ULONG METHOD CUsbDevNet::AddRef(void)
{
	return CUsbPort::AddRef();
	}

ULONG METHOD CUsbDevNet::Release(void)
{
	return CUsbPort::Release();
	}

// IDevice

BOOL METHOD CUsbDevNet::Open(void)
{
	if( !m_fOpen && m_uBaud ) {

		LockPnp();
			
		InitDriver();

		m_fOpen   = true;

		m_fOnline = false;

		InitData();

		InitBuffers();

		m_pTimer->Enable(true);

		StartDriver();

		FreePnp();

		return TRUE;
		}
	
	return FALSE;
	}

// IPortObject

void METHOD CUsbDevNet::Bind(IPortHandler *pHandler)
{
	}

void METHOD CUsbDevNet::Bind(IPortSwitch *pSwitch)
{
	}

UINT METHOD CUsbDevNet::GetPhysicalMask(void)
{
	return Bit(physicalDeviceNet);
}

BOOL METHOD CUsbDevNet::Open(CSerialConfig const &Config)
{
	m_uBaud = Config.m_uBaudRate;

	return TRUE;
	}

void METHOD CUsbDevNet::Close(void)
{
	if( m_fOpen ) {

		LockPnp();

		m_fOpen = false;

		WaitWrite();

		StopDriver();

		TermDriver();

		FreeBuffers();

		m_pTimer->Enable(false);

		FreePnp();
		}
	}

void METHOD CUsbDevNet::Send(BYTE bData)
{
	}

void METHOD CUsbDevNet::SetBreak(BOOL fBreak)
{
	}

void METHOD CUsbDevNet::EnableInterrupts(BOOL fEnable)
{
	}

void METHOD CUsbDevNet::SetOutput(UINT uOutput, BOOL fSet)
{
	}

BOOL METHOD CUsbDevNet::GetInput(UINT uInput)
{
	return FALSE;
	}

DWORD METHOD CUsbDevNet::GetHandle(void)
{
	return CUsbPort::GetHandle();
	}

// IDeviceNetPort

void METHOD CUsbDevNet::SetMac(BYTE bMac)
{
	m_bMac = bMac;
	}

BOOL METHOD CUsbDevNet::EnableBitStrobe(WORD wRecvSize, WORD wSendSize)
{
	m_wBitRecv = wRecvSize;

	m_wBitSend = wSendSize;

	return TRUE;
	}

BOOL METHOD CUsbDevNet::EnablePolled(WORD wRecvSize, WORD wSendSize)
{
	m_wPollRecv = wRecvSize;

	m_wPollSend = wSendSize;

	return TRUE;
	}

BOOL METHOD CUsbDevNet::EnableData(WORD wRecvSize, WORD wSendSize)
{
	m_wDataRecv = wRecvSize;

	m_wDataSend = wSendSize;

	return TRUE;
	}

BOOL METHOD CUsbDevNet::IsOnLine(void)
{
	if( m_fOpen && IsPresent() ) {
		
		CheckRead();

		return m_fOnline;
		}

	return FALSE;
	}

UINT METHOD CUsbDevNet::Read(UINT uId, PBYTE pData, UINT uSize)
{
	if( m_fOpen && IsPresent() ) {

		CheckRead();

		UINT iBuff = FindBuffer(uId);

		if( iBuff != NOTHING ) {

			CData &Data = m_Data[iBuff];

			if( Data.m_fDirty ) {

				MakeMin(uSize, Data.m_uSize);

				memcpy(pData, Data.m_pData, uSize);

				Data.m_fDirty = false;
				
				return uSize;
				}

			return 0;
			}
		}

	return NOTHING;
	}

UINT METHOD CUsbDevNet::Write(UINT uId, PCBYTE pData, UINT uSize)
{
	if( m_fOpen && IsPresent() ) {

		WaitWrite();

		MakeMin(uSize, sizeof(m_bBuffSend) - 3);

		m_bBuffSend[0] = uId;

		m_bBuffSend[1] = LOBYTE(LOWORD(uSize));

		m_bBuffSend[2] = HIBYTE(LOWORD(uSize));

		memcpy(&m_bBuffSend[3], pData, uSize);

		if( m_pDriver->SendAsync(m_bBuffSend, uSize + 3) != NOTHING ) {

			m_fWaitSend = true;
			
			return uSize;
			}
		}
	
	return NOTHING;
	}

// IEventSink

void CUsbDevNet::OnEvent(UINT uLine, UINT uParam)
{
	if( m_fOpen ) {

		m_uHeartbeat += 100;
		}
	}

// Overridables

void CUsbDevNet::OnDriverBind(IUsbHostFuncDriver *pDriver)
{
	m_pDriver = (IUsbHostDevNet *) pDriver;
	}

void CUsbDevNet::OnInitDriver(void)
{
	m_pDriver->SetRun(false);

	m_pDriver->SetBaudRate(m_uBaud);

	m_pDriver->SetDrop(m_bMac);

	if( m_wBitRecv || m_wBitSend ) {

		m_pDriver->EnableBitStrobe(m_wBitRecv, m_wBitSend);
		}

	if( m_wPollRecv || m_wPollSend ) {

		m_pDriver->EnablePolled(m_wPollRecv, m_wPollSend);
		}

	if( m_wDataRecv || m_wDataSend ) {

		m_pDriver->EnableData(m_wDataRecv, m_wDataSend);
		}
	}

void CUsbDevNet::OnStartDriver(void)
{
	m_pDriver->SetRun(true);
	}

void CUsbDevNet::OnStopDriver(void)
{
	m_pDriver->SetRun(false);
	}

void CUsbDevNet::OnTermDriver(void)
{
	m_pDriver->KillSend();

	m_pDriver->KillRecv();
	}

// Implementation 

void CUsbDevNet::InitData(void)
{
	m_fWaitRecv  = false;

	m_fWaitSend  = false;

	m_uHeartbeat = 0;
	}

void CUsbDevNet::WaitWrite(void)
{
	LockPnp();

	if( m_fWaitSend ) {

		if( !IsRemoved() ) {

			m_pDriver->WaitSend(FOREVER);
			}

		m_fWaitSend  = false;

		m_uHeartbeat = 0;
		}

	FreePnp();
	}

void CUsbDevNet::CheckRead(void)
{
	LockPnp();

	if( !IsRemoved() ) {

		for(;;) {

			if( !m_fWaitRecv ) {

				if( m_pDriver->RecvAsync(m_bBuffRecv, sizeof(m_bBuffRecv)) != NOTHING ) {

					m_fWaitRecv = true;

					continue;
					}

				break;
				}

			if( m_fWaitRecv ) {

				UINT uCount = m_pDriver->WaitRecv(0);

				PBYTE pData = m_bBuffRecv;

				if( uCount != NOTHING ) {

					while( uCount ) {

						UINT uAssy = pData[0];

						UINT uSize = MAKEWORD(pData[1], pData[2]);

						if( uAssy == 0 ) {

							m_fOnline = pData[3];
							}
						else {
							UINT iBuff = AllocBuffer(uAssy, uSize);

							if( iBuff != NOTHING ) {

								CData &Data = m_Data[iBuff];

								memcpy(Data.m_pData, pData+3, uSize);

								Data.m_fDirty = true;
								}
							}

						pData  += (uSize + 3);

						uCount -= (uSize + 3);
						}

					m_uHeartbeat = 0;

					m_fWaitRecv  = false;

					continue;
					}
				}
			
			break;
			}

		if( m_uHeartbeat > 1000 ) {

			m_pDriver->SendHeartbeat();

			m_uHeartbeat = 0;
			}
		}

	FreePnp();
	}

// Buffers

void CUsbDevNet::InitBuffers(void)
{
	memset(m_Data, 0, sizeof(m_Data));
	}

UINT CUsbDevNet::FindBuffer(UINT uId)
{
	for( UINT i = 0; i < elements(m_Data); i ++ ) {

		if( m_Data[i].m_uId == uId ) {

			return i;
			}
		}

	return NOTHING;
	}

UINT CUsbDevNet::AllocBuffer(UINT uId, UINT uSize)
{
	UINT i = FindBuffer(uId);

	if( i == NOTHING ) {
		
		for( i = 0; i < elements(m_Data); i ++ ) {

			if( m_Data[i].m_pData == NULL && !m_Data[i].m_uId ) {

				m_Data[i].m_uId   = uId;

				m_Data[i].m_pData = New BYTE[ uSize ];

				m_Data[i].m_uSize = uSize;

				return i;
				}
			}

		return NOTHING;
		}

	if( m_Data[i].m_uSize < uSize ) {

		delete [] m_Data[i].m_pData;

		m_Data[i].m_pData = New BYTE[ uSize ];

		m_Data[i].m_uSize = uSize;
		}

	return i;
	}

void CUsbDevNet::FreeBuffers(void)
{
	for( UINT i = 0; i < elements(m_Data); i ++ ) {

		if( m_Data[i].m_pData ) {

			delete m_Data[i].m_pData;

			m_Data[i].m_pData = NULL;
			}
		}
	}

// End of File
