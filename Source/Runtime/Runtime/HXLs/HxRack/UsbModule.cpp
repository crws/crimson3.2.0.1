
#include "Intern.hpp"

#include "UsbModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb PnP Port
//

CUsbModule::CUsbModule(void)
{
	StdSetRef();

	m_pEvent      = Create_ManualEvent();

	m_pFuncDriver = NULL;

	m_fOpen       = false;

	m_fFuncOpen   = true;

	m_fRemovePend = false;

	m_nLockPnp    = 0;

	m_pEvent->Set();
	}

// Destructor

CUsbModule::~CUsbModule(void)
{
	FreeDriver();

	m_pEvent->Release();
	}

// Attributes

bool CUsbModule::IsPresent(void) const
{
	return m_pFuncDriver && !m_fRemovePend;
	}

bool CUsbModule::IsRemoved(void) const
{
	return !m_pFuncDriver;
	}

bool CUsbModule::IsRunning(void) const
{
	return m_fOpen && m_fFuncOpen && !m_fRemovePend;
	}

DWORD CUsbModule::GetHandle(void) const
{
	return DWORD(m_pFuncDriver);
	}

// IUnknown

HRESULT METHOD CUsbModule::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IExpansionPnp);

	StdQueryInterface(IExpansionPnp);

	return E_NOINTERFACE;
	}

ULONG METHOD CUsbModule::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CUsbModule::Release(void)
{
	StdRelease();
	}

// IExpansionPnp

void METHOD CUsbModule::OnDeviceArrival(IUsbHostFuncDriver *pDriver)
{
	if( m_pEvent->Wait(5000) ) {

		OnDeviceRemoval(m_pFuncDriver);
	
		BindDriver(pDriver);

		if( m_fOpen ) {

			InitDriver();
		
			StartDriver();
			}
		}
	}

void METHOD CUsbModule::OnDeviceRemoval(IUsbHostFuncDriver *pDriver)
{
	LockPnp();
	
	m_fRemovePend = true;

	FreePnp();
	}

// Pnp

void CUsbModule::LockPnp(void)
{
	if( AtomicIncrement(&m_nLockPnp) == 1 ) {

		m_pEvent->Clear();
		}
	}

void CUsbModule::FreePnp(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( m_nLockPnp == 1 ) {

		if( m_fRemovePend ) {

			m_fFuncOpen = false;
			
			Hal_LowerIrql(uSave);
			
			TermDriver();
			
			FreeDriver();

			uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

			m_fRemovePend = false;
			}

		m_pEvent->Set();

		m_nLockPnp = 0;
		}
	else {
		m_nLockPnp--;
		}

	Hal_LowerIrql(uSave);
	}

// Driver

void CUsbModule::BindDriver(IUsbHostFuncDriver *pDriver)
{
	m_pFuncDriver = pDriver;

	m_pFuncDriver->AddRef();

	m_pFuncDriver->SetRemoveLock(true);

	OnDriverBind(pDriver);
	}

void CUsbModule::InitDriver(void)
{
	LockPnp();

	if( IsPresent() ) {
		
		OnInitDriver();
		}

	FreePnp();
	}

void CUsbModule::StartDriver(void)
{
	LockPnp();

	if( IsPresent() ) {

		m_fFuncOpen = true;

		OnStartDriver();
		}

	FreePnp();
	}

void CUsbModule::StopDriver(void)
{
	LockPnp();

	if( IsPresent() ) {

		m_fFuncOpen = false;

		OnStopDriver();
		}

	FreePnp();
	}

void CUsbModule::TermDriver(void)
{
	LockPnp();

	if( !IsRemoved() ) {

		m_fFuncOpen = false;

		OnTermDriver();
		}

	FreePnp();
	}

void CUsbModule::FreeDriver(void)
{
	if( m_pFuncDriver ) {

		m_pFuncDriver->SetRemoveLock(false);

		m_pFuncDriver->Release();

		m_pFuncDriver = NULL;
		}
	}

// Overridables

void CUsbModule::OnDriverBind(IUsbHostFuncDriver *pDriver)
{
	}

void CUsbModule::OnInitDriver(void)
{
	}

void CUsbModule::OnStartDriver(void)
{
	}

void CUsbModule::OnStopDriver(void)
{
	}

void CUsbModule::OnTermDriver(void)
{
	}

// Implementation

void CUsbModule::EnableInterrupts(bool fEnable)	
{
	if( fEnable ) {	

		Hal_LowerIrql(m_uSaveIrql);
		}
	else {
		m_uSaveIrql = Hal_RaiseIrql(IRQL_MAXIMUM);
		}
	}

// End of File
