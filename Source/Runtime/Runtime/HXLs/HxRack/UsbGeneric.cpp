
#include "Intern.hpp"

#include "UsbGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Generic Port
//

// Instantiator

IPortObject * Create_UsbGeneric(IUsbHostFuncDriver *pDriver)
{
	CUsbGeneric *p = New CUsbGeneric(pDriver);

	p->Open();

	return p;
	}

// Constructor

CUsbGeneric::CUsbGeneric(IUsbHostFuncDriver *pDriver)
{
	m_pDriver  = (IUsbHostModuleGeneric *) pDriver;

	m_pHandler = NULL;
	}

// IDevice

BOOL METHOD CUsbGeneric::Open(void)
{
	BindDriver(m_pDriver);

	return TRUE;
	}

// IPortObject

void METHOD CUsbGeneric::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
	}

UINT METHOD CUsbGeneric::GetPhysicalMask(void)
{
	// TODO -- What does this need to return?!!!!

	return 0;
}

BOOL METHOD CUsbGeneric::Open(CSerialConfig const &Config)
{
	if( !m_fOpen ) {

		AfxTrace("CUsbGeneric::Open()\n");

		m_Config = Config;

		LockPnp();

		InitDriver();

		InitData();

		m_fOpen = true;

		m_pHandler->OnOpen(m_Config);

		StartDriver();

		FreePnp();

		return TRUE;
		}
	
	return FALSE;
	}

void METHOD CUsbGeneric::Close(void)
{
	if( m_fOpen ) {

		AfxTrace("CUsbGeneric::Close()\n");

		LockPnp();

		m_fOpen = false;

		WaitDone(5000);

		StopDriver();

		TermDriver();

		SuckDry();

		m_pHandler->OnClose();

		FreePnp();
		}
	}

void METHOD CUsbGeneric::Send(BYTE bData)
{
	if( m_fOpen ) {

		if( WaitDone(5000) ) {

			m_uSendCount = 0;

			m_bSendData[m_uSendCount++] = bData;

			PullData();

			StartSend();
			}
		}
	}

// IUsbHostFuncEvents

void METHOD CUsbGeneric::OnData(void)
{
	if( m_fOpen ) {

		OnRecv();

		OnSend();
		}
	}

// Overridables 

void CUsbGeneric::OnDriverBind(IUsbHostFuncDriver *pDriver)
{
	m_pDriver = (IUsbHostModuleGeneric *) pDriver;

	m_pDriver->Bind(this);
	}

void CUsbGeneric::OnStartDriver(void)
{
	StartRecv();
	}

void CUsbGeneric::OnTermDriver(void)
{
	m_pDriver->Shutdown();

	m_pDriver->KillAsyncRecv();

	m_pDriver->KillAsyncSend();
	}

// Implementation

void CUsbGeneric::InitData(void)
{
	m_uRecvState = stateIdle;

	m_uSendState = stateIdle;

	m_uSendCount = 0;

	m_uRecvCount = 0;
	}

void CUsbGeneric::OnSend(void)
{
	if( m_uSendState == stateWait ) {

		if( m_pDriver->WaitAsyncSend(0) == NOTHING ) {

			return;
			}

		for(;;) {
			
			if( !PullData() ) {
			
				m_uSendState = stateIdle;

				m_pHandler->OnTxDone();
			
				return;
				}

			if( StartSend() ) {

				return;
				}
			}
		}
	}

void CUsbGeneric::OnRecv(void)
{
	for(;;) {
		
		if( m_uRecvState == stateRecv ) {

			if( m_pDriver->RecvDataAsync(m_bRecvData, sizeof(m_bRecvData)) != NOTHING ) {

				m_uRecvState = stateWait;

				continue;
				}

			return;
			}

		if( m_uRecvState == stateWait ) {

			m_uRecvCount = m_pDriver->WaitAsyncRecv(0);

			if( m_uRecvCount != NOTHING ) {

				PushData();

				m_uRecvState = stateRecv;

				continue;
				}
			
			return;
			}

		break;
		}
	}

bool CUsbGeneric::StartRecv(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( IsPresent() ) {

		if( m_pDriver->RecvDataAsync(m_bRecvData, sizeof(m_bRecvData)) != NOTHING ) {

			m_uRecvState = stateWait;

			Hal_LowerIrql(uSave);

			return true;
			}
		}

	Hal_LowerIrql(uSave);

	return false;
	}

bool CUsbGeneric::StartSend(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( IsPresent() ) {
	
		if( m_pDriver->SendDataAsync(m_bSendData, m_uSendCount) ) {

			m_uSendCount = 0;  

			m_uSendState = stateWait;

			Hal_LowerIrql(uSave);

			return true;
			}
		}

	m_uSendState = stateIdle;

	Hal_LowerIrql(uSave);
			
	return false;
	}

bool CUsbGeneric::PullData(void)
{
	for( UINT nCount = 0;; nCount++ ) {

		if( m_pHandler->OnTxData(m_bSendData[m_uSendCount]) ) {

			if( ++m_uSendCount < sizeof(m_bSendData) ) {

				continue;
				}
			}
		
		return nCount != 0;
		}

	return false;
	}

bool CUsbGeneric::PushData(void)
{
	if( m_uRecvCount ) {

		for( UINT i = 0; i < m_uRecvCount; i ++ ) {

			m_pHandler->OnRxData(m_bRecvData[i]);
			}

		m_pHandler->OnRxDone();

		m_uRecvCount = 0;

		return true;
		}

	return false;
	}

void CUsbGeneric::SuckDry(void)
{
	BYTE bData;

	while( m_pHandler->OnTxData(bData) );

	m_pHandler->OnTxDone();
	}

bool CUsbGeneric::WaitDone(UINT uTimeout)
{
	if( m_uSendState != stateIdle ) {

		return m_pDriver->WaitAsyncSend(uTimeout) != NOTHING;
		}

	return true;
	}

// End of File
