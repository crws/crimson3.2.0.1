
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbModuleBoot_HPP
	
#define	INCLUDE_UsbModuleBoot_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Module Boot Loader
//

class CUsbModuleBoot : public IUsbHostFuncEvents
{
	public:
		// Constructor
		CUsbModuleBoot(IUsbHostFuncDriver *pDriver, IExpansionFirmware *pFirm);

		// Destructor
		~CUsbModuleBoot(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncEvents
		void METHOD OnPoll(UINT uLapsed);
		void METHOD OnData(void);
		void METHOD OnTerm(void);

	protected:
		// State
		enum
		{
			stateIdle,
			stateSendCheck,
			stateWaitCheck,
			stateSendClear,
			stateWaitClear,
			stateSendProg,
			stateWaitProg,
			stateSendGuid,
			stateWaitGuid,
			stateSendRun,
			stateError,
			};

		// Data
		IUsbHostModuleBoot * m_pDriver;
		IExpansionFirmware * m_pFirm;
		ULONG                m_uRefs;
		UINT		     m_uState;
		UINT                 m_uAddr;
		UINT		     m_uSize;
		PCBYTE		     m_pData;
		PCBYTE		     m_pGuid;
		UINT		     m_uDelay;
		bool		     m_fLock;

		// State Handlers
		void OnSendCheck(void);
		void OnWaitCheck(void);
		void OnSendClear(void);
		void OnWaitClear(void);
		void OnSendProg(void);
		void OnWaitProg(void);
		void OnSendGuid(void);
		void OnWaitGuid(void);
		void OnSendRun(void);
		void OnError(void);

		// Implementation
		bool CheckWait(UINT uPass, UINT uFail, UINT uError);
		bool SetFirmwareLock(bool fSet);
	};

// End of File

#endif
