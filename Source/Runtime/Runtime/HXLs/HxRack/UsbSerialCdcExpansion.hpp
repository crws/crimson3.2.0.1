
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSerialCdcExpansion_HPP

#define	INCLUDE_UsbSerialCdcExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansionSerial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial CDC Expansion Object
//

class CUsbSerialCdcExpansion : public CUsbExpansionSerial
{
	public:
		// Constructor
		CUsbSerialCdcExpansion(IUsbHostFuncDriver *pDriver);

		// IExpansionInterface
		PCTXT	  METHOD GetName(void);
		UINT	  METHOD GetPower(void);
		IDevice * METHOD MakeObject(IUsbHostFuncDriver *pDrv);

		// IExpansionSerial
		UINT  METHOD GetPortCount(void);
		DWORD METHOD GetPortMask(void);
		UINT  METHOD GetPortType(UINT iPort);
	};

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IPortObject * Create_UsbSerialCdc(IUsbHostFuncDriver *pDriver);

// End of File

#endif


