
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbCan_HPP

#define	INCLUDE_UsbCan_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Can Port
//

class CUsbCan : public CUsbPort, public IEventSink
{
public:
	// Constructor
	CUsbCan(IUsbHostFuncDriver *pDriver, WORD wProtocol);

	// Destructor
	~CUsbCan(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void METHOD Bind(IPortHandler *pHandler);
	UINT METHOD GetPhysicalMask(void);
	BOOL METHOD Open(CSerialConfig const &Config);
	void METHOD Close(void);
	void METHOD Send(BYTE bData);
	void METHOD SetOutput(UINT uOutput, BOOL fOn);
	BOOL METHOD GetInput(UINT uInput);

	// IUsbHostFuncEvents
	void METHOD OnPoll(UINT uLapsed);
	void METHOD OnData(void);

	// IEventSink
	void OnEvent(UINT uLine, UINT uParam);

protected:
	// Poll State
	enum
	{
		stateIdle,
		stateSend,
		stateRecv,
		stateWait,
		stateFail,
	};

	// Data
	IUsbHostCan  * m_pDriver;
	WORD	       m_wProtocol;
	IPortHandler * m_pHandler;
	ITimer 	     * m_pTimer;
	CSerialConfig  m_Config;
	UINT	       m_uMode;
	UINT volatile  m_uSendState;
	UINT volatile  m_uRecvState;
	bool	       m_fPdo;
	bool	       m_fExt;
	bool	       m_fPass;
	UsbCanFrame    m_TxFrame;
	UsbCanFrame    m_RxFrame[64];
	UINT	       m_uTxCount;
	UINT	       m_uRxCount;
	UINT	       m_uHeartbeat;

	// Overridables
	void OnDriverBind(IUsbHostFuncDriver *pDriver);
	void OnInitDriver(void);
	void OnTermDriver(void);
	void OnStartDriver(void);
	void OnStopDriver(void);

	// Implementation 
	void ClearData(void);
	void InitFilter(void);
	void OnRecv(void);
	void OnSend(void);
	void PollIdle(void);
	bool StartSend(void);
	bool StartRecv(void);
	bool TxStore(BYTE bData);
	bool TxFrame(void);
	void RxFrame(void);
	void SuckDry(void);
	void WaitDone(UINT uTimeout);
};

// End of File

#endif
