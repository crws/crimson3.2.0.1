
#include "Intern.hpp"

// Static Data

static DWORD m_dwLink = 0xBA000000;

static DWORD m_dwBase = 0x20000C00;

static WORD  m_wModel = 0;

static BOOL  m_fGuid  = FALSE;

static PBYTE m_pData;

static UINT  m_uData;

static PBYTE m_pWork;

static UINT  m_uWork;

static BYTE  m_bGuid[16];

// Externals

extern "C" int fastlz_compress(const void *input, int length, void *output);

// Prototypes

void main(int nArg, char *pArg[]);
void Error(char const *pText, ...);
bool LoadFile(char const *pName);
bool SaveFile(char const *pName, bool fComp);
void FixVirt(CHeader *pHead);
void FixCode(CHeader *pHead);
void FixData(CHeader *pHead);
void FixHead(CHeader *pHead);
void FixJump(UINT n, DWORD &dwAddr);

// Code

void main(int nArg, char *pArg[])
{
	if( nArg >= 2 ) {

		bool fComp = false;

		bool fBase = false;

		char sName[MAX_PATH] = { 0 };

		char sSave[MAX_PATH] = { 0 };

		for( int n = 1; n < nArg; n++ ) {

			char *pScan = pArg[n];

			if( *pScan == '-' || *pScan == '/' ) {

				if( tolower(pScan[1]) == 'c' ) {

					if( !fComp ) {

						fComp = true;

						continue;
						}

					Error("repeated switch");
					}

				if( tolower(pScan[1]) == 'b' ) {

					if( !fBase ) {

						if( ++n < nArg ) {

							char const *pHex = pArg[n];

							char       *pEnd = NULL;

							if( pHex[0] == '0' && (pHex[1] == 'x' || pHex[2] == 'X') ) {

								pHex += 2;
								}

							m_dwBase = strtoul(pHex, &pEnd, 16);

							if( !*pEnd ) {

								fBase = true;

								continue;
								}

							Error("invalid base address");
							}

						Error("missing base address");
						}

					Error("repeated base address");
					}

				if( tolower(pScan[1]) == 'o' ) {

					if( !sSave[0] ) {

						if( ++n < nArg ) {

							strcpy(sSave, pArg[n]);

							continue;
							}

						Error("missing output file");
						}

					Error("repeated output file");
					}

				if( tolower(pScan[1]) == 'm' ) {

					if( !m_wModel ) {

						if( ++n < nArg ) {

							if( (m_wModel = atoi(pArg[n])) ) {

								continue;
								}

							Error("invalid model identifier");
							}

						Error("missing model identifier");
						}

					Error("repeated model identifier");
					}

				if( tolower(pScan[1]) == 'g' ) {

					if( !m_fGuid ) {

						m_fGuid = TRUE;

						continue;
						}

					Error("repeated switch");
					}

				Error("unknown switch");
				}

			if( !sName[0] ) {

				strcpy(sName, pScan);

				continue;
				}

			Error("multiple filenames");
			}

		if( sName[0] ) {

			if( LoadFile(sName) ) {

				CHeader *pHead = (CHeader *) m_pData;

				if( !strcmp(pHead->sMagic, "AEON APP") ) {

					if( pHead->wMajorVersion == 1 && pHead->wMinorVersion == 0 ) {

						if( pHead->dwFlags & 1 ) {
	
							m_dwLink  = pHead->dwBase;

							UINT fix1 = pHead->dwAddrFix2 - pHead->dwAddrFix1;
					
							UINT fix2 = pHead->dwAddrFix3 - pHead->dwAddrFix2;

							if( fix1 == fix2 ) {

								FixVirt(pHead);

								FixCode(pHead);

								FixData(pHead);

								FixHead(pHead);

								if( sSave[0] ) {

									if( SaveFile(sSave, fComp) ) {

										exit(0);
										}
									}
								else {
									strcpy(sSave, sName);

									strcat(sSave, ".tmp");
	
									if( SaveFile(sSave, fComp) ) {

										if( _unlink(sName) >= 0 ) {

											if( rename(sSave, sName) >= 0 ) {

												exit(0);
												}

											Error("the output file could not be renamed");
											}

										_unlink(sSave);

										Error("the intput file could not be deleted");
										}
									}

								Error("the output file could not be created");
								}

							Error("the relocation segments do not match in size");
							}

						Error("the file has no relocation information");
						}

					Error("the file version is not supported");
					}

				Error("the file does not contain an Aeon application");
				}

			Error("the input file could not be opened");
			}

		Error("missing filename");
		}

	fprintf(stderr, "usage: fixup [-c] [-g] [-m <model>] [-b <base>] [-o <out-file>] <in-file>\n\n");

	exit(2);
	}

void Error(char const *pText, ...)
{
	va_list va;

	va_start(va, pText);

	char s[1024];

	vsprintf(s, pText, va);

	fprintf(stderr, "fixup: %s\n", s);

	exit(1);
	}

bool LoadFile(char const *pName)
{
	FILE *pFile = fopen(pName, "rb");

	if( pFile ) {

		fseek(pFile, 0, SEEK_END);

		m_uData = ftell(pFile);

		m_uWork = m_uData + 2048;

		m_pWork = new BYTE [ m_uWork ];

		m_pData = m_pWork + 2048;

		memset(m_pWork, 0, 2048);

		fseek(pFile, 0, SEEK_SET);

		fread(m_pData, 1, m_uData, pFile);

		fclose(pFile);

		return true;
		}

	return false;
	}

bool SaveFile(char const *pName, bool fComp)
{
	FILE *pFile = fopen(pName, "wb");

	if( pFile ) {

		if( fComp ) {

			PBYTE pComp = new BYTE [ m_uWork ];

			UINT  uComp = fastlz_compress(m_pWork, m_uWork, pComp);

			fwrite(pComp, 1, uComp, pFile);

			delete [] pComp;
			}
		else
			fwrite(m_pWork, 1, m_uWork, pFile);

		if( m_fGuid ) {

			char cMark01[8] = "BOOT-->";
			char cMark02[8] = "DATA-->";
			char cMark03[8] = "GUID-->";

			fwrite(cMark01, 1, sizeof(cMark01), pFile);

			fprintf(pFile, "%-8.8u", 27);

			fwrite(cMark02, 1, sizeof(cMark02), pFile);

			fprintf(pFile, "%-8.8u", 3200);

			fwrite(cMark03, 1, sizeof(cMark03), pFile);

			fwrite(m_bGuid, 1, sizeof(m_bGuid), pFile);
			}

		fclose(pFile);

		return true;
		}

	return false;
	}

void FixVirt(CHeader *pHead)
{
	UINT   x = 0;

	UINT   s = pHead->dwAddrFix1 - pHead->dwAddrVTab;

	UINT   c = s / sizeof(DWORD);

	PDWORD p = PDWORD(m_pData + pHead->dwAddrVTab - m_dwLink);

	bool   u = false;

	for( UINT n = 0; n < c; n++ ) {

		// For vtable relocations, we have to watch out for
		// pointer offset values, which are negative. Since
		// we do not have the relocation records, we have to
		// apply a heuristic to figure out what really needs
		// relocating and what does not. It seems to work!

		// Note that this section can also contain RTTI if we
		// are using that option, so we have to watch out for
		// small integer values that represent type flags. We
		// skip those by nothing the zero high-order word.

		if( ((p[n] & 0xFF000000) == (m_dwLink & 0xFF000000)) && ((p[n] & 0xF0000000) ^ 0xF0000000) ) {

			if( (u || p[n] % 4 == 0) && p[n] >= pHead->dwAddrVTab && p[n] < pHead->dwAddrData ) {

				p[n] -= m_dwLink;

				p[n] += m_dwBase;

				x++;
				}
			else
				Error("unexpected vtable entry 0x%8.8X", p[n]);
			}
		}
	}

void FixCode(CHeader *pHead)
{
	UINT   x = 0;

	UINT   s = pHead->dwAddrFix2 - pHead->dwAddrFix1;

	UINT   c = s / sizeof(DWORD);

	PDWORD p = PDWORD(m_pData + pHead->dwAddrFix1 - m_dwLink);

	PDWORD d = PDWORD(m_pData + pHead->dwAddrFix2 - m_dwLink);

	bool   u = true;

	for( UINT n = 0; n < c; n++ ) {

		if( p[n] > 0 ) {

			if( (u || p[n] % 4 == 0) && p[n] >= pHead->dwBase && p[n] <= pHead->dwAddrLast ) {

				if( d[n] ) {

					Error("unexpected value in fixup segment");
					}

				p[n] -= m_dwLink;

				p[n] += m_dwBase;

				d[n]  = p[n];

				x++;
				}
			else
				Error("unexpected code fixup 0x%8.8X", p[n]);
			}
		}
	}

void FixData(CHeader *pHead)
{
	UINT   x = 0;

	UINT   s = pHead->dwAddrText - pHead->dwAddrFix3;

	UINT   c = s / sizeof(DWORD);

	PDWORD p = PDWORD(m_pData + pHead->dwAddrFix3 - m_dwLink);

	bool   u = true;

	for( UINT n = 0; n < c; n++ ) {

		// For data relocations, we could have all sorts of
		// stuff mixed in here and we can't easily tell the
		// relocatable stuff from the rest as we don't have
		// the relocation records. We apply a heuristic to
		// figure out what really needs relocating and what
		// does not. It seems to work, especially when the
		// original file is based at a distinctive address.

		if( p[n] > 0 ) {

			if( (u || p[n] % 4 == 0) && p[n] >= pHead->dwAddrFix3 && p[n] <= pHead->dwAddrLast ) {

				p[n] -= m_dwLink;

				p[n] += m_dwBase;

				x++;
				}
			}
		}
	}

void FixHead(CHeader *pHead)
{
	FixJump(0, pHead->dwEntry);

	FixJump(1, pHead->dwExit);

	pHead->wModel   = m_wModel;

	pHead->dwBase   = m_dwBase;

	pHead->dwFlags &= ~1;

	UuidCreate((UUID *) m_bGuid);
		
	memcpy(pHead->bGuid, m_bGuid, sizeof(m_bGuid));
	}

void FixJump(UINT n, DWORD &dwAddr)
{
	if( dwAddr ) {

		if( dwAddr - m_dwLink < 32 * 1024 * 1024 ) {

			// Jump via "B dwAddr"

			dwAddr = (0xEA000000 | (((dwAddr - m_dwLink) >> 2) - 2 - n));

			return;
			}

		Error("entry table fixup out of range");
		}

	// Return via "BX LR"

	dwAddr = 0xE12FFF1E;
	}

// End of File
