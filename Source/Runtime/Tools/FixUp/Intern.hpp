
#define  _CRT_SECURE_NO_WARNINGS

#include <Windows.h>

#include <stdio.h>

struct CHeader
{
	DWORD	dwEntry;
	DWORD	dwExit;
	char	sMagic[10];
	WORD    wModel;
	WORD	wMajorVersion;
	WORD	wMinorVersion;
	DWORD	dwFlags;
	DWORD	dwBase;
	DWORD	dwAddrVTab;
	DWORD	dwAddrFix1;
	DWORD	dwAddrFix2;
	DWORD	dwAddrFix3;
	DWORD	dwAddrText;
	DWORD	dwAddrData;
	DWORD	dwAddrLast;
	DWORD	dwZero;
	BYTE	bGuid[16];
	};
