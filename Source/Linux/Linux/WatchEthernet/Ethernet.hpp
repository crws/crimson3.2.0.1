

#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Interface
//

class CEthernet : public CInterface
{
public:
	// Constructor
	CEthernet(string const &face, string const &root);

	// Destructor
	~CEthernet(void);

protected:
	// States
	enum
	{
		stateSetLinkUp   = 2,
		stateConfigure   = 3,
		stateWaitCarrier = 4,
		stateConnected   = 5,
	};

	// Data Members
	string m_watch;
	string m_wdata;
	string m_idata;

	// Overridables
	bool OnConfigure(void) override;
	int  OnExecute(void) override;
	void OnStopping(void) override;
	void OnNewState(void) override;
	bool OnCommand(string const &cmd) override;

	// Implementation
	bool   FindSled(void);
	bool   IsPresent(void);
	bool   SetAutoNegotiate(void);
	bool   SetLinkPhysical(bool full, bool fast);
	bool   TestCarrier(bool &carrier);
	int    GetLinkSpeed(void);
	bool   IsLinkFull(void);
	string GetStateName(void);
	bool   WriteStatus(void);
};

// End of File
