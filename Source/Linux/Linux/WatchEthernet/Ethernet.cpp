
#include "Intern.hpp"

#include "Ethernet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Interface
//

// Constructor

CEthernet::CEthernet(string const &face, string const &root) : CInterface(face, root)
{
	ClearStatus();
}

// Destructor

CEthernet::~CEthernet(void)
{
}

// Overridables

bool CEthernet::OnConfigure(void)
{
	m_watch = GetConfig("watch", m_face.c_str());

	m_idata = "/sys/class/net/" + m_face;

	m_wdata = "/sys/class/net/" + m_watch;

	return FindSled();
}

int CEthernet::OnExecute(void)
{
	switch( m_state ) {

		case stateNotPresent:
		{
			if( GetMonotonic() >= m_timer + (m_first ? 120 : 60) ) {

				AfxTrace("hardware not responding\n");

				ResetHardware();

				m_timer = GetMonotonic();

				return 0;
			}

			if( IsPresent() ) {

				AfxTrace("found device for %s\n", m_face.c_str());

				m_state = stateSetLinkUp;

				m_first = false;

				return 500;
			}

			return 250;
		}
		break;

		case stateSetLinkUp:
		{
			if( SetLinkState(true) ) {

				m_state = stateConfigure;

				return 0;
			}
		}
		break;

		case stateConfigure:
		{
			if( GetConfig("auto", true) ) {

				if( GetConfig("virtual", false) || SetAutoNegotiate() ) {

					if( SetLinkMtu(GetConfig("mtu", 1500)) ) {

						m_state = stateWaitCarrier;

						return 0;
					}
				}
			}
			else {
				bool full = GetConfig("full", true);

				bool fast = GetConfig("fast", true);

				if( SetLinkPhysical(full, fast) ) {

					if( SetLinkMtu(GetConfig("mtu", 1500)) ) {

						m_state = stateWaitCarrier;

						return 0;
					}
				}
			}
		}

		case stateWaitCarrier:
		{
			bool carrier;

			if( TestCarrier(carrier) ) {

				if( carrier ) {

					m_state = stateConnected;

					m_ctime = GetMonotonic();
				}

				return 500;
			}
		}
		break;

		case stateConnected:
		{
			bool carrier;

			if( TestCarrier(carrier) ) {

				if( !carrier ) {

					m_state = stateWaitCarrier;
				}

				WriteStatus();

				return 500;
			}
		}
		break;
	}

	m_state = stateNotPresent;

	m_ctime = 0;

	return 250;
}

void CEthernet::OnNewState(void)
{
	if( m_state == stateConnected ) {

		RunLinkStart(true);
	}

	if( m_prev == stateConnected ) {

		RunLinkStart(false);
	}

	WriteStatus();
}

void CEthernet::OnStopping(void)
{
	if( m_state == stateConnected ) {

		RunLinkStart(false);

		m_ctime = 0;
	}

	SetLinkState(false);

	ClearStatus();
}

bool CEthernet::OnCommand(string const &cmd)
{
	if( cmd == "reset" || cmd == "failed" ) {

		ResetHardware();

		return true;
	}

	return false;
}

// Implementation

bool CEthernet::FindSled(void)
{
	if( m_face.size() == 6 ) {

		if( m_face.substr(0, 5) == "eth0s" ) {

			m_sled = atoi(m_face.substr(5).c_str());

			return true;
		}
	}

	if( m_face.size() == 4 ) {

		if( m_face.substr(0, 3) == "eth" ) {

			return true;
		}
	}

	return false;
}

bool CEthernet::IsPresent(void)
{
	ifstream stm(m_idata + "/type");

	if( stm.good() ) {

		if( m_idata == m_wdata ) {

			return true;
		}
		else {
			ifstream stm(m_wdata + "/type");

			if( stm.good() ) {

				return true;
			}
		}
	}

	return false;
}

bool CEthernet::SetAutoNegotiate(void)
{
	CPrintf cmd("ethtool -s %s autoneg on", m_face.c_str());

	AfxTrace("%s\n", cmd.c_str());

	return !system(cmd);
}

bool CEthernet::SetLinkPhysical(bool full, bool fast)
{
	auto s1 = fast ? 100 : 10;

	auto s2 = full ? "full" : "half";

	CPrintf cmd("ethtool -s %s autoneg off speed %u duplex %s", m_face.c_str(), s1, s2);

	AfxTrace("%s\n", cmd.c_str());

	return !system(cmd);
}

bool CEthernet::TestCarrier(bool &carrier)
{
	ifstream stm(m_wdata + "/carrier");

	if( stm.good() ) {

		int n = -1;

		stm >> n;

		if( n >= 0 ) {

			carrier = n ? true : false;

			return true;
		}
	}

	return false;
}

int CEthernet::GetLinkSpeed(void)
{
	ifstream stm(m_idata + "/speed");

	if( stm.good() ) {

		int n = -1;

		stm >> n;

		if( n >= 0 ) {

			return n;
		}
	}

	return 0;
}

bool CEthernet::IsLinkFull(void)
{
	ifstream stm(m_idata + "/duplex");

	if( stm.good() ) {

		string s;

		stm >> s;

		return s == "full";
	}

	return false;
}

string CEthernet::GetStateName(void)
{
	switch( m_state ) {

		case stateSetLinkUp:
			return "Configuring Link";

		case stateConfigure:
			return "Configuring Link";

		case stateWaitCarrier:
			return "Waiting for Carrier";

		case stateConnected:
			return "Connected";
	}

	return CInterface::GetStateName();
}

bool CEthernet::WriteStatus(void)
{
	string   tmp(m_status + ".tmp");

	ofstream stm(tmp);

	if( stm.good() ) {

		bool online = (m_state == stateConnected);

		stm << "{\n";

		stm << "\t\"state\": \"" << GetStateName().c_str() << "\",\n";

		stm << "\t\"speed\": " << GetLinkSpeed() << ",\n";

		stm << "\t\"full\": " << (IsLinkFull() ? 1 : 0) << ",\n";

		stm << "\t\"ctime\": " << m_ctime << "\n";

		stm << "}\n";

		stm.close();

		unlink(m_status.c_str());

		rename(tmp.c_str(), m_status.c_str());

		return true;
	}

	return false;
}

// End of File
