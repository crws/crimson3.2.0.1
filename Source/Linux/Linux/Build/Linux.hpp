
////////////////////////////////////////////////////////////////////////////////
//	
// GCC-Linux Intellisense Support
//
// Copyright (c) 2019-2020 Granby Consulting LLC
//
// Placed in the Public Domain.
//

#pragma once

////////////////////////////////////////////////////////////////////////////////
//	
// Intellisense Support
//

#if defined(__INTELLISENSE__)

#pragma push_macro("__GNUC__")

#undef  __GNUC__

#define __signed__		signed
#define __builtin_va_list	char *
#define __builtin_va_start(v,l)	do { v = NULL; } while(0)
#define __builtin_va_end(v)	do { v = NULL; } while(0)
#define __builtin_va_arg(v,l)	((l) 0)
#define	__builtin_va_copy(d,s)	do { } while(0)
#define	__typeof(x)		locale_t
#define __SIZE_TYPE__		unsigned int
#define __CHAR_BIT__		8
#define _GLIBCXX_USE_CXX11_ABI	0
#define _GLIBCXX_ABI_TAG_CXX11
#define _GLIBCXX_BITS_STD_ABS_H
#define _GLIBCXX_CSTDLIB
#define __need_ptrdiff_t
#define __ARM_PCS_VFP
#define __asm(x)
#define __sync_fetch_and_add(a,b) ((void) 0)
#define __sync_fetch_and_sub(a,b) ((void) 0)

#ifdef  __cplusplus

#undef  __cplusplus

#define __cplusplus		_MSVC_LANG

#endif

#include <stdc-predef.h>

#else

#include <stdarg.h>

#endif

////////////////////////////////////////////////////////////////////////////////
//	
// Standard Headers
//

#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <syslog.h>
#include <termios.h>
#include <unistd.h>

#include <algorithm>
#include <functional>
#include <fstream>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
//	
// Intellisense Support
//

#if defined(__INTELLISENSE__)

#pragma pop_macro("__GNUC__")

#endif

////////////////////////////////////////////////////////////////////////////////
//	
// Macros
//

#define elements(x) (sizeof(x)/sizeof(*(x)))

#define AfxTouch(x) ((void) (x))

#define global      /**/

////////////////////////////////////////////////////////////////////////////////
//	
// Type Definitions
//

typedef unsigned char  BYTE;

typedef unsigned int   UINT;

typedef unsigned short WORD;

typedef unsigned long  DWORD;

typedef BYTE * PBYTE;

typedef BYTE const * PCBYTE;

typedef char * PTXT;

typedef char const * PCTXT;

typedef char * PSTR;

typedef char const * PCSTR;

typedef vector<BYTE> bytes;

typedef long long INT64;

// End of File
