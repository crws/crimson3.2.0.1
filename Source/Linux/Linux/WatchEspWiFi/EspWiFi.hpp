
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// ESP Wi-Fi Interface
//

class CEspWiFi : public CGenericModem
{
public:
	// Constructor
	CEspWiFi(string const &face, string const &root);

	// Destructor
	~CEspWiFi(void);

protected:
	// States
	enum
	{
		stateCheckVersion = 2,
		stateFindMac      = 3,
		stateInitialize   = 4,
		stateConnecting   = 5,
		stateConnected    = 6,
		stateListening    = 7
	};

	// Data Members
	string  m_dev1;
	string	m_scan;
	string	m_network;
	string	m_apmac;
	string  m_version;
	int	m_channel;
	int	m_signal;

	// Overridables
	bool OnConfigure(void) override;
	int  OnExecute(void) override;
	void OnStopping(void) override;
	void OnNewState(void) override;
	bool OnCommand(string const &cmd) override;

	// Device Commands
	bool CheckVersion(int &code);
	bool GetMacAddress(int &code);
	bool DisableWiFi(int &code);
	bool InitStation(int &code);
	bool InitAccessPoint(int &code);
	bool IsConnected(int &code);
	bool ScanNetworks(void);

	// Implementation
	bool   FindSled(void);
	string GetStateName(void);
	void   ClearStatus(void);
	void   ResetStatus(void);
	bool   WriteStatus(void);
	int    ScaleSignal(int s);
};

// End of File
