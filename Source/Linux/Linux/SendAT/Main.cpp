
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 AT Command Sender
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../ATLib/ModemChannel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

// Static Data

static bool m_debug = false;

// Prototypes

static bool IsUnderDebug(void);
static bool GetProcStatusEntry(string &data, string const &proc, string const &name);

// Code

int main(int nArg, char const *pArg[])
{
	m_debug = IsUnderDebug();

	if( nArg == 3 || nArg == 4 ) {

		string dev = "/dev/xrcontrol_";

		char const *p = pArg[1];

		switch( tolower(*p) ) {

			case 'm':
				dev = "/dev/cellular_modem";
				p++;
				break;

			case 'g':
				dev = "/dev/cellular_gps";
				p++;
				break;

			case 'x':
				p++;
				break;
		}

		CModemChannel chan(dev + p);

		if( chan.IsPresent() ) {

			chan.SetTrace(false);

			chan.SetEcho(false);

			printf("SEND\t%s\n", pArg[2]);

			vector<string> lines;

			int ms = (nArg == 4) ? atoi(pArg[3]) : 1000;

			int rc = chan.Command(lines, pArg[2], ms);

			if( !lines.empty() ) {

				for( size_t p = 1; p < lines.size(); p++ ) {

					printf("EXTRA\t%s\n", lines[p].c_str());
				}

				printf("REPLY\t%s\n", lines[0].c_str());
			}

			return rc;
		}

		return 100;
	}

	fprintf(stderr, "usage: SendAT <id> <command> [<timeout>]\n");

	return 200;
}

void AfxTrace(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	if( !m_debug ) {

		vsyslog(LOG_NOTICE, p, v);
	}
	else {
		vprintf(p, v);
	}

	va_end(v);
}

void AfxTestPipe(void)
{
}

static bool IsUnderDebug(void)
{
	string ppid;

	if( GetProcStatusEntry(ppid, "self", "PPid") ) {

		string name;

		if( GetProcStatusEntry(name, ppid, "Name") ) {

			if( name == "gdbserver" || name == "gdb" ) {

				return true;
			}
		}
	}

	return false;
}

static bool GetProcStatusEntry(string &data, string const &proc, string const &name)
{
	char s[128];

	sprintf(s, "/proc/%s/status", proc.c_str());

	ifstream stm(s);

	if( stm.good() ) {

		while( !stm.eof() ) {

			string line;

			getline(stm, line);

			size_t n = line.find(':');

			if( n != string::npos ) {

				if( line.substr(0, n) == name ) {

					int d = line.find_first_not_of(" \t", n + 1);

					if( d != string::npos ) {

						int e = line.find_last_not_of(" \t\r\n");

						data = line.substr(d, e - d + 1);

						return true;
					}
				}
			}
		}
	}

	return false;
}

// End of File
