
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// VTable
//

struct snmp_methods
{
	int (*pfnSetRoot)(char const *root);
	int (*pfnAddScalar)(size_t key, char const *path, char const *name);
	int (*pfnSetCallback)(int (*pfnCallback)(void *app, void *ctx, size_t key), void *app);
	int (*pfnReplyString)(void *ctx, char const *text);
	int (*pfnReplyInteger)(void *ctx, int data);
	int (*pfnReplyInt64)(void *ctx, long long data);
};

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

struct snmp_methods * m_vtab;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

static int Callback(void *app, void *ctx, size_t key)
{
	(m_vtab->pfnReplyString)(ctx, "Hello SO");

	return 1;
}

extern "C" void SnmpEntry(snmp_methods *vtab)
{
	m_vtab = vtab;

	(m_vtab->pfnSetRoot)("1.3.6.1.4.1.1890.2");

	(m_vtab->pfnAddScalar)(1, "1.1", "testThing1");

	(m_vtab->pfnAddScalar)(1, "1.3", "testThing2");

	(m_vtab->pfnSetCallback)(Callback, NULL);
}

// End of File
