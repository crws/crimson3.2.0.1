
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Reachability Check
//

// Static Data

static	int	 m_fd    = -1;

// Prototypes

global	bool	IsReachable(char const *name);
static	UINT	ResolveName(char const *name);
static	bool	OpenNetLink(void);
static	bool	CloseNetLink(void);
static	bool	SendGetRoutes(void);
static	ssize_t	RecvMessage(msghdr &msg, int flags);
static	bool	RecvMessage(bytes &data);
static	UINT	GetUnsigned(map<WORD, bytes> &attr, WORD type, UINT none);

// Code

global bool IsReachable(char const *name)
{
	UINT addr = ResolveName(name);

	if( addr ) {

		if( OpenNetLink() && SendGetRoutes() ) {

			bytes data;

			if( RecvMessage(data) ) {

				vector<string> cmds;

				nlmsghdr const *h = (nlmsghdr const *) data.data();

				size_t          s = data.size();

				while( NLMSG_OK(h, s) ) {

					if( h->nlmsg_flags & NLM_F_DUMP_INTR ) {

						break;
					}

					if( h->nlmsg_type == NLMSG_ERROR ) {

						break;
					}

					rtmsg const *r = (rtmsg const *) NLMSG_DATA(h);

					if( r->rtm_family == AF_INET ) {

						map<WORD, bytes> attr;

						rtattr const *a = RTM_RTA(r);

						size_t        s = h->nlmsg_len;

						while( RTA_OK(a, s) ) {

							PCBYTE d = PCBYTE(a+1);

							size_t n = a->rta_len - sizeof(*a);

							attr.insert(make_pair(a->rta_type, bytes(d, d + n)));

							a = RTA_NEXT(a, s);
						}

						if( r->rtm_dst_len ) {

							UINT dest = GetUnsigned(attr, RTA_DST, 0);

							UINT mask = (1UL << r->rtm_dst_len) - 1;

							if( (addr & mask) == (dest & mask) ) {

								CloseNetLink();

								return true;
							}
						}
					}

					h = NLMSG_NEXT(h, s);
				}
			}
		}

		CloseNetLink();
	}

	return false;
}

static UINT ResolveName(char const *name)
{
	addrinfo *res = NULL;

	getaddrinfo(name, NULL, NULL, &res);

	if( res ) {

		UINT a = ((UINT *) (res->ai_addr->sa_data + 2))[0];

		freeaddrinfo(res);

		return a;
	}

	freeaddrinfo(res);

	return 0;
}

static bool OpenNetLink(void)
{
	if( (m_fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) >= 0 ) {

		sockaddr_nl saddr = { 0 };

		saddr.nl_family = AF_NETLINK;

		saddr.nl_pid    = getpid();

		if( bind(m_fd, (sockaddr *) &saddr, sizeof(saddr)) >= 0 ) {

			return true;
		}

		close(m_fd);

		m_fd = -1;
	}

	return false;
}

static bool CloseNetLink(void)
{
	if( m_fd >= 0 ) {

		close(m_fd);

		m_fd = -1;

		return true;
	}

	return false;
}

static bool SendGetRoutes(void)
{
	struct
	{
		nlmsghdr nlh;
		rtmsg	 rtm;

	} req;

	req.nlh.nlmsg_type  = RTM_GETROUTE;
	req.nlh.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
	req.nlh.nlmsg_len   = sizeof(req);
	req.nlh.nlmsg_seq   = time(NULL);
	req.rtm.rtm_family  = AF_INET;

	return send(m_fd, &req, sizeof(req), 0) == sizeof(req);
}

ssize_t RecvMessage(msghdr &msg, int flags)
{
	for( ;;) {

		ssize_t s = recvmsg(m_fd, &msg, flags);

		if( s > 0 ) {

			return s;
		}

		if( s < 0 ) {

			if( errno == EINTR || errno == EAGAIN ) {

				continue;
			}
		}

		return -1;
	}
}

static bool RecvMessage(bytes &data)
{
	sockaddr_nl addr = { 0 };

	iovec       iov  = { 0 };

	msghdr      msg  = { 0 };

	msg.msg_name    = &addr;
	msg.msg_namelen = sizeof(addr);
	msg.msg_iov     = &iov;
	msg.msg_iovlen  = 1;

	ssize_t r;

	if( (r = RecvMessage(msg, MSG_PEEK | MSG_TRUNC)) > 0 ) {

		if( addr.nl_family == AF_NETLINK ) {

			if( addr.nl_pid == 0 ) {

				data.resize(r);

				iov.iov_base = data.data();

				iov.iov_len  = data.size();

				if( RecvMessage(msg, 0) == data.size() ) {

					return true;
				}
			}
		}
	}

	return false;
}

static UINT GetUnsigned(map<WORD, bytes> &attr, WORD type, UINT none)
{
	auto i = attr.find(type);

	if( i == attr.end() ) {

		return none;
	}

	return *((UINT *) i->second.data());
}

// End of File
