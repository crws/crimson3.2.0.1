
#pragma  once

#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// GcCore Class Library
//
// Copyright (c) 2018 Granby Consulting LLC
//
// All Rights Reserved
//

#ifndef INCLUDE_Base64_HPP

#define INCLUDE_Base64_HPP

////////////////////////////////////////////////////////////////////////////////
//	
// Base64 Encoder
//

class CBase64
{
public:
	// Encoding Options
	enum Encoding
	{
		encBasic   = 0,
		encNoBreak = 1,
		encUrl     = 2,
	};

	// Operations
	static string ToBase64(PCBYTE p, size_t n, int enc = encNoBreak);
	static string ToBase64(bytes const &d, int enc = encNoBreak);
	static string ToBase64(string const &s, int enc = encNoBreak);
	static bytes  ToBytes(string const &s);
	static string ToAnsi(string const &s);

protected:
	// Decode Helper
	template<typename dtype> static bool Decode(dtype &d, string const &s, bool z);

	// Size Estimation
	static size_t GetEncodeSize(size_t s, int enc) noexcept;
	static size_t GetDecodeSize(size_t s) noexcept;

	// Encoding List
	static char const * GetList(int enc) noexcept;

	// Character Decode
	static int Decode(char c) noexcept;
};

// End of File

#endif
