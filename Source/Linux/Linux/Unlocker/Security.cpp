
#include "Intern.hpp"

#include "Security.hpp"

#include "../MakePassword/Words.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Library Headers
//

#include <cryptoauthlib.h>

#include <atcacert/atcacert_client.h>

#include <atcacert/atcacert_host_hw.h>

//////////////////////////////////////////////////////////////////////////
//
// Certificate Templates
//

extern "C" const atcacert_def_t SIGNER_CERTIFICATE_DEFINITION;

extern "C" const atcacert_def_t DEVICE_CERTIFICATE_DEFINITION;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define MAGIC_DEV_INFO	0x23094820

#define MAGIC_DEV_KEY	0x98325408

//////////////////////////////////////////////////////////////////////////
//
// Hardware Security Module
//

// Constructor

CSecurity::CSecurity(void)
{
	m_uGroup = SW_GROUP_1;
}

// Destructor

CSecurity::~CSecurity(void)
{
}

// IDevice

bool CSecurity::Open(void)
{
	if( OpenLibrary() ) {

		if( FindIssuerKey() && ReadSignerCert() && ReadDeviceCert() ) {

			if( !ReadDeviceInfo() ) {

				ImportDeviceInfo();
			}

			ReadDeviceKey();

			return true;
		}
	}

	return false;
}

// IFeatures

UINT CSecurity::GetEnabledGroup(void)
{
	return m_uGroup;
}

bool CSecurity::GetDeviceInfo(CSecureDeviceInfo &Info)
{
	Info = m_Info;

	return true;
}

UINT CSecurity::GetDeviceCode(PBYTE pData, UINT uData)
{
	UINT uRead = min(uData, sizeof(m_DeviceNum));

	memcpy(pData, m_DeviceNum, uRead);

	return uRead;
}

bool CSecurity::InstallUnlock(PCBYTE pData, UINT uData)
{
	string Text(PCSTR(pData), uData);

	vector<string> List;

	Tokenize(List, Text, '\n');

	if( VerifyKeyFile(List) ) {

		DWORD dwMagic = MAGIC_DEV_KEY;

		DWORD dwSize  = uData;

		if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 8, 0, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

			if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 8, 4, PBYTE(&dwSize), sizeof(dwSize)) ) {

				UINT uWork = 4 * ((uData + 3) / 4);

				if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 8, 8, pData, uWork) ) {

					return true;
				}
			}
		}
	}

	return false;
}

// Device Information

string CSecurity::GetDeviceModel(void)
{
	return string(PCSTR(m_Info.m_bModel), 4);
}

string CSecurity::GetSerialNumber(void)
{
	return PCSTR(m_Info.m_bSerial);
}

// Password Generator

string CSecurity::MakePassword(void)
{
	BYTE rand[32] = { 0 };

	for( ;;) {

		if( !atcab_random(rand) ) {

			string w1 = words[((UINT *) rand)[0] % elements(words)];

			string w2 = words[((UINT *) rand)[1] % elements(words)];

			if( w1.size() + w2.size() > 16 ) {

				continue;
			}

			if( w2.substr(w2.size()-3) == "ing" ) {

				if( w1.substr(w1.size()-3) == "ing" ) {

					continue;
				}

				string w3 = w1;

				w1 = w2;

				w2 = w3;
			}

			int nn = ((UINT *) rand)[2] % 10000;

			return CPrintf("%s-%s-%4.4u", w1.c_str(), w2.c_str(), nn);
		}

		return "";
	}
}

// HSM Support

bool CSecurity::OpenLibrary(void)
{
	static ATCAIfaceCfg dc;

	dc.iface_type            = ATCA_I2C_IFACE;
	dc.devtype               = ATECC608A;
	dc.atcai2c.baud          = 1000000;
	dc.atcai2c.bus           = 0;
	dc.atcai2c.slave_address = 0xC0;
	dc.wake_delay	         = 800;
	dc.rx_retries            = 3;
	dc.cfg_data              = NULL;

	return !atcab_init(&dc);
}

bool CSecurity::FindIssuerKey(void)
{
	MakeKey(m_IssuerKey,
		"61afe5953e6cc23bcc959f436a5d3e3e3a5e3952f7e8159de67dbb6cc704bd68",
		"f983604f24b0ab67019d80518d82ed26a583c0835bd73a31eebe2bde25344e34"
	);

	return true;
}

bool CSecurity::ReadSignerCert(void)
{
	atcacert_def_t const *pDef = &SIGNER_CERTIFICATE_DEFINITION;

	m_SignerSize = sizeof(m_SignerCert);

	if( !atcacert_read_cert(pDef, m_IssuerKey, m_SignerCert, &m_SignerSize) ) {

		if( !atcacert_get_subj_public_key(pDef, m_SignerCert, m_SignerSize, m_SignerKey) ) {

			return true;
		}
	}

	return false;
}

bool CSecurity::ReadDeviceCert(void)
{
	atcacert_def_t const *pDef = &DEVICE_CERTIFICATE_DEFINITION;

	m_DeviceSize = sizeof(m_DeviceCert);

	if( !atcacert_read_cert(pDef, m_SignerKey, m_DeviceCert, &m_DeviceSize) ) {

		if( !atcacert_get_subj_public_key(pDef, m_DeviceCert, m_DeviceSize, m_DeviceKey) ) {

			size_t size = sizeof(m_DeviceNum);

			if( !atcacert_get_cert_sn(pDef, m_DeviceCert, m_DeviceSize, m_DeviceNum, &size) ) {

				return true;
			}
		}
	}

	return false;
}

bool CSecurity::MakeKey(PBYTE key, PCSTR x, PCSTR y)
{
	int  p = 0;

	char c[3];

	for( int i = 0; x[i]; i+=2 ) {

		c[0] = x[i+0];
		c[1] = x[i+1];
		c[2] = 0;

		key[p++] = BYTE(strtoul(c, NULL, 16));
	}

	for( int i = 0; y[i]; i+=2 ) {

		c[0] = y[i+0];
		c[1] = y[i+1];
		c[2] = 0;

		key[p++] = BYTE(strtoul(c, NULL, 16));
	}

	return true;
}

// Device Info

bool CSecurity::ReadDeviceInfo(void)
{
	DWORD dwMagic = 0;

	if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 13, 0, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

		if( dwMagic == MAGIC_DEV_INFO ) {

			DWORD dwSize = 0;

			if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 13, 4, PBYTE(&dwSize), sizeof(dwSize)) ) {

				if( dwSize == sizeof(CSecureDeviceInfo) ) {

					if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 13, 8, PBYTE(&m_Info), sizeof(m_Info)) ) {

						return true;
					}
				}
			}
		}
	}

	return false;
}

bool CSecurity::ImportDeviceInfo(void)
{
	memset(&m_Info, 0, sizeof(m_Info));

	string Model  = ReadBoot("model_name", "");

	string Serial = ReadBoot("btserial", "");

	Model = Model.substr(0, 4) + Model.substr(6, 2);

	memcpy(m_Info.m_bModel, Model.data(), Model.size());

	memcpy(m_Info.m_bSerial, Serial.data(), Serial.size());

	return true;
}

bool CSecurity::VerifyKeyFile(vector<string> const &List)
{
	if( List.size() == 5 ) {

		string Test;

		for( UINT n = 0; n < elements(m_DeviceNum); n++ ) {

			Test += CPrintf("%2.2X", m_DeviceNum[n]);
		}

		if( List[0] == "K00" && List[1] == Test ) {

			return true;
		}
	}

	return false;
}

bool CSecurity::ReadDeviceKey(void)
{
	DWORD dwMagic = 0;

	if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 8, 0, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

		if( dwMagic == MAGIC_DEV_KEY ) {

			DWORD dwSize = 0;

			if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 8, 4, PBYTE(&dwSize), sizeof(dwSize)) ) {

				UINT uWork = 4 * ((dwSize / 4) + 3);

				if( uWork >= 100 && uWork <= 300 ) {

					bytes Data(uWork, 0);

					if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 8, 8, Data.data(), uWork) ) {

						string      Text(PCSTR(Data.data()), dwSize);

						vector<string> List;

						Tokenize(List, Text, '\n');

						if( VerifyKeyFile(List) ) {

							DWORD c1 = strtoul(List[2].c_str(), NULL, 16);

							DWORD c2 = strtoul(List[3].c_str(), NULL, 16);

							m_uGroup = WORD(c2);

							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

// Implementation

string CSecurity::ReadFile(PCTXT pName, PCTXT pDef)
{
	int fd = open(pName, O_RDONLY, 0);

	if( fd >= 0 ) {

		char s[128];

		int r = read(fd, s, sizeof(s));

		if( r < sizeof(s) ) {

			s[r] = 0;

			return s;
		}

		close(fd);
	}

	return pDef;
}

string CSecurity::ReadSkvs(PCTXT pKey, PCTXT pDef)
{
	return ReadFile(CPrintf("/tmp/skvs.d/%s", pKey), pDef);
}

string CSecurity::ReadBoot(PCTXT pKey, PCTXT pDef)
{
	string c = string("/sbin/fw_printenv ") + pKey;

	FILE * f = popen(c.c_str(), "r");

	if( f ) {

		char b[256] = { 0 };

		fgets(b, sizeof(b), f);

		if( b[0] ) {

			string s(b, strlen(b)-1);

			return s.substr(s.find('=')+1, string::npos);
		}
	}

	return pDef;
}


size_t CSecurity::Tokenize(vector<string> &list, string text, char sep)
{
	size_t p;

	while( !text.empty() ) {

		if( (p = text.find(sep)) == string::npos ) {

			list.push_back(text);

			break;
		}

		list.push_back(text.substr(0, p));

		text.erase(0, p+1);
	}

	return text.size();
}

// End of File
