
#include "Intern.hpp"

#include "ShortMessage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Splitters and Combiners
//

#define LOWORD(l) ((WORD)(((DWORD)(l)) & 0xFFFF))

#define HIWORD(l) ((WORD)((((DWORD)(l)) >> 16) & 0xFFFF))

#define LOBYTE(w) ((BYTE)(((DWORD)(w)) & 0xFF))

#define HIBYTE(w) ((BYTE)((((DWORD)(w)) >> 8) & 0xFF))

#define MAKEWORD(l, h) ((WORD)(((BYTE)(l)) | (((WORD )((BYTE)(h))) <<  8)))

#define MAKELONG(l, h) ((LONG)(((WORD)(l)) | (((DWORD)((WORD)(h))) << 16)))

//////////////////////////////////////////////////////////////////////////
//
// Short Message Object
//

// Lookup Table

BYTE const CShortMessage::m_bMap[] = {

	 64, 163,  36, 165, 232, 233, 249, 236, 242, 199,  10, 216, 248,  13, 197, 229,
	'?',  95, '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', 198, 230, 223, 201,
	 32,  33,  34,  35, 164,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
	 48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
	161,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
	 80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90, 196, 214, 209, 220, 167,
	191,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
	112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 228, 246, 241, 252, 224

};

// Constructor

CShortMessage::CShortMessage(void)
{
	m_uIndex   = UINT(-1);

	m_pNumber  = NULL;

	m_pMessage = NULL;
}

// Destructor

CShortMessage::~CShortMessage(void)
{
	FreeData();
}

// Attributes

UINT CShortMessage::GetIndex(void) const
{
	return m_uIndex;
}

PCTXT CShortMessage::GetNumber(void) const
{
	return m_pNumber ? m_pNumber : "";
}

PCTXT CShortMessage::GetMessage(void) const
{
	return m_pMessage ? m_pMessage : "";
}

// Operations

void CShortMessage::SetIndex(UINT uIndex)
{
	m_uIndex = uIndex;
}

void CShortMessage::SetNumber(PCTXT pNumber)
{
	free(m_pNumber);

	m_pNumber = strdup(pNumber);
}

void CShortMessage::SetMessage(PCTXT pMessage)
{
	free(m_pMessage);

	m_fUnicode = HasUnicode(pMessage);

	m_pMessage = strdup(pMessage);
}

void CShortMessage::AppendSpace(void)
{
	UINT uSize = strlen(m_pMessage) + 2;

	m_pMessage = PTXT(realloc(m_pMessage, uSize));

	m_pMessage[uSize - 2] = ' ';

	m_pMessage[uSize - 1] = 0;
}

void CShortMessage::ParsePDU(PCBYTE pData)
{
	FreeData();

	ParseServiceCenter(pData);

	ParseMessageType(pData);

	ParseNumber(pData);

	ParseEncoding(pData);

	ParseTimeStamp(pData);

	ParseMessage(pData);
}

UINT CShortMessage::BuildPDU(PBYTE pData)
{
	PCBYTE pPrev = pData;

	BuildServiceCenter(pData);

	BuildMessageType(pData);

	BuildNumber(pData);

	BuildEncoding(pData);

	BuildTimeStamp(pData);

	BuildMessage(pData);

	return pData - pPrev;
}

// Debugging

void CShortMessage::Dump(void)
{
	AfxTrace("%u:%s:%s\n", m_uIndex, m_pNumber, m_pMessage);
}

// PDU Parsing

void CShortMessage::ParseServiceCenter(PCBYTE &pData)
{
	BYTE bSize = *pData++;

	pData += bSize;
}

void CShortMessage::ParseMessageType(PCBYTE &pData)
{
	*pData++;
}

void CShortMessage::ParseNumber(PCBYTE &pData)
{
	BYTE bSize = *pData++;

	BYTE bType = *pData++;

	BYTE bData = *pData++;

	UINT uBits = 8;

	UINT uPos  = 0;

	m_pNumber  = PTXT(malloc(bSize + 1));

	while( bSize-- ) {

		m_pNumber[uPos++] = '0' + (bData & 15);

		bData >>= 4;

		uBits  -= 4;

		if( bSize && !uBits ) {

			bData = *pData++;

			uBits = 8;
		}
	}

	m_pNumber[uPos++] = 0;

	AfxTouch(bType);
}

void CShortMessage::ParseEncoding(PCBYTE &pData)
{
	BYTE bProt = *pData++;

	BYTE bCode = *pData++;

	m_fUnicode = (bCode & 0x08) ? true : false;

	AfxTouch(bProt);
}

void CShortMessage::ParseTimeStamp(PCBYTE &pData)
{
	pData += 7;
}

void CShortMessage::ParseMessage(PCBYTE &pData)
{
	if( m_fUnicode ) {

		BYTE  bSize = *pData++ / 2;

		UINT  uRead = 0;

		m_pMessage  = PTXT(malloc(bSize * 8 + 1));

		for( UINT n = 0; n < bSize; n++ ) {

			BYTE hi = *pData++;

			BYTE lo = *pData++;

			if( !hi ) {

				m_pMessage[uRead++] = lo;
			}
			else {
				// TODO -- Consider UTF8?!!!

				WORD c = MAKEWORD(lo, hi);

				m_pMessage[uRead++] = '&';

				m_pMessage[uRead++] = '#';

				m_pMessage[uRead++] = '0' + (c / 10000) % 10;
				m_pMessage[uRead++] = '0' + (c / 1000) % 10;
				m_pMessage[uRead++] = '0' + (c / 100) % 10;
				m_pMessage[uRead++] = '0' + (c / 10) % 10;
				m_pMessage[uRead++] = '0' + (c / 1) % 10;

				m_pMessage[uRead++] = ';';
			}
		}

		m_pMessage[uRead++] = 0;

		m_pMessage = PTXT(realloc(m_pMessage, uRead));
	}
	else {
		BYTE bSize = pData[0];

		WORD wData = MAKEWORD(pData[1], pData[2]);

		UINT uBits = 16;

		UINT uPos  = 0;

		m_pMessage = PTXT(malloc(bSize + 1));

		pData += 3;

		while( bSize-- ) {

			m_pMessage[uPos++] = m_bMap[wData & 0x7F];

			wData >>= 7;

			uBits  -= 7;

			if( bSize && uBits < 7 ) {

				wData |= (*pData++ << uBits);

				uBits += 8;
			}
		}

		m_pMessage[uPos++] = 0;
	}
}

// PDU Building

void CShortMessage::BuildServiceCenter(PBYTE &pData)
{
	*pData++ = 0x00;
}

void CShortMessage::BuildMessageType(PBYTE &pData)
{
	*pData++ = 0x11;

	*pData++ = 0x00;
}

void CShortMessage::BuildNumber(PBYTE &pData)
{
	BYTE bSize = strlen(m_pNumber);

	BYTE bData = 0;

	UINT uBits = 0;

	UINT uPos  = 0;

	*pData++ = bSize;

	*pData++ = 0x91;

	while( bSize-- ) {

		bData |= (m_pNumber[uPos++] - '0') << uBits;

		uBits += 4;

		if( uBits == 8 ) {

			*pData++ = bData;

			bData = 0;

			uBits = 0;
		}
	}

	if( uBits == 4 ) {

		bData |= 0xF0;

		*pData++ = bData;
	}
}

void CShortMessage::BuildEncoding(PBYTE &pData)
{
	*pData++ = m_fUnicode ? 0x00 : 0x00;

	*pData++ = m_fUnicode ? 0x08 : 0x00;
}

void CShortMessage::BuildTimeStamp(PBYTE &pData)
{
	*pData++ = 0xAA;
}

void CShortMessage::BuildMessage(PBYTE &pData)
{
	if( m_fUnicode ) {

		BYTE bSize = GetUnicodeCount(m_pMessage);

		PTXT p     = m_pMessage;

		*pData++   = 2 * bSize;

		while( *p ) {

			char c = *p++;

			if( c == '&' ) {

				if( p[0] == '#' && p[6] == ';' ) {

					WORD uni = atoi(p+1);

					*pData++ = HIBYTE(uni);

					*pData++ = LOBYTE(uni);

					p += 7;

					continue;
				}
			}

			*pData++ = 0;

			*pData++ = c;
		}
	}
	else {
		BYTE bSize = strlen(m_pMessage);

		WORD wData = 0;

		UINT uBits = 0;

		UINT uPos  = 0;

		*pData++   = bSize;

		while( bSize-- ) {

			wData |= Encode(m_pMessage[uPos++]) << uBits;

			uBits += 7;

			if( uBits >= 8 ) {

				*pData++ = LOBYTE(wData);

				wData  >>= 8;

				uBits   -= 8;
			}
		}

		if( uBits ) {

			*pData++ = LOBYTE(wData);
		}
	}
}

// Implementation

BYTE CShortMessage::Encode(char cData)
{
	if( UINT(cData) < elements(m_bMap) ) {

		if( m_bMap[int(cData)] == cData ) {

			return cData;
		}
	}

	for( UINT n = 0; n < elements(m_bMap); n++ ) {

		if( m_bMap[n] == cData ) {

			return n;
		}
	}

	return Encode('?');
}

void CShortMessage::FreeData(void)
{
	free(m_pNumber);

	free(m_pMessage);

	m_pNumber  = NULL;

	m_pMessage = NULL;
}

BYTE CShortMessage::GetUnicodeCount(PCTXT p)
{
	BYTE u = 0;

	while( *p ) {

		char c = *p++;

		if( c == '&' ) {

			if( p[0] == '#' && p[6] == ';' ) {

				p += 7;
			}
		}

		u++;
	}

	return u;
}

bool CShortMessage::HasUnicode(PCTXT p)
{
	while( *p ) {

		char c = *p++;

		if( c == '&' ) {

			if( p[0] == '#' && p[6] == ';' ) {

				return true;
			}
		}
	}

	return false;
}

// End of File
