
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Telit Modem Interface
//

class CTelitModem : public CGenericModem
{
public:
	// Constructor
	CTelitModem(string const &face, string const &root);

	// Destructor
	~CTelitModem(void);

protected:
	// States
	enum
	{
		stateResetState    = 2,
		stateSelectSim     = 3,
		stateWaitForSim    = 4,
		stateListCarriers  = 5,
		stateCheckModel	   = 6,
		stateReadSimInfo   = 7,
		stateCheckFirmware = 8,
		stateInitialize    = 9,
		stateRegister1     = 10,
		stateRegister2     = 11,
		stateAttach        = 12,
		stateReady         = 13,
		stateConnect       = 14,
		stateConnected     = 15,
		statePowerSave	   = 16,
	};

	// Data Interfaces
	enum
	{
		dataNCM,
		dataECM,
		dataMBIM,
	};

	// SIM Info
	struct CSim
	{
		bool	valid;
		string	imsi;
		string	iccid;
		string	ccode;
		string	cname;
	};

	// Data Members
	string			  m_dev1;
	string			  m_dev2;
	string			  m_dev3;
	unique_ptr<CModemChannel> m_aux;
	string			  m_model;
	map<string, string>	  m_carriers;
	string			  m_time;
	string			  m_location;
	int			  m_sim;
	int			  m_ims;
	CSim			  m_sinfo[2];
	bool			  m_5Hz;
	int			  m_ctx;
	int			  m_net;
	bool			  m_req;
	string			  m_ipdata;
	int			  m_cycle;
	int			  m_signal;
	int			  m_register;
	bool			  m_roam;
	string			  m_version;
	string			  m_network;
	string			  m_imei;
	int			  m_ctype;
	string			  m_addr;
	int			  m_seq;
	int			  m_data;
	bool			  m_reqoff;
	bool			  m_power;
	int			  m_pcycle;
	int			  m_pconnect;
	int			  m_pmin;
	int			  m_pmax;
	time_t			  m_ptime;
	time_t			  m_itime;
	long long		  m_txlast;

	// Overridables
	bool OnConfigure(void) override;
	int  OnExecute(void) override;
	void OnStopping(void) override;
	void OnNewState(void) override;
	bool OnCommand(string const &cmd) override;

	// Modem Commands
	bool EnterKnownState(int &code);
	bool PerformBasicInit(int &code);
	bool ListCarriers(int &code);
	bool FindModel(int &code);
	bool SelectSim(int &code, int sim);
	bool IsSimReady(int &code);
	bool ReadSimInfo(int &code);
	bool CheckModel(int &code);
	bool CheckFirmware(int &code);
	bool ConfigureRadio(int &code);
	bool InitializeContexts(int &code);
	bool InitializeDataContext(int &code);
	bool InitializeImsContext(int &code);
	bool IsRegistered(int &code, bool &deny);
	bool FindNetwork(int &code);
	bool IsAttached(int &code);
	bool CheckStatus(int &code);
	bool CheckConnected(int &code);
	bool FindConnected(int &code, string &connect);
	bool FindConnected(int &code, string &connect, vector<string> &r);
	bool DataConnect(int &code);
	bool DataConnectEcm(int &code, map<string, string> &env);
	bool DataConnectNcm(int &code, map<string, string> &env);
	bool DataConnectMbim(int &code, map<string, string> &env);
	bool DataDisconnect(int &code, bool kill);
	bool SetFunction(int &code, int func, bool reset);
	bool SetContext(int &code, int ctx, string const &set);
	bool SendMessage(int &code, string const &rcpt, string body);
	bool ReadMessage(string &rcpt, string &body, vector<string> const &data, string const &line);
	bool TestMessage(vector<int> &kill, vector<string> const &data);
	bool HardDetach(int &code);
	bool SoftDetach(int &code);
	bool ArpPing(string const &gate);

	// Service Checks
	bool CheckServices(bool online);
	bool CheckMessages(int &code);
	bool CheckMessageSend(int &code);
	bool CheckMessageRecv(int &code);
	bool CheckGps(int &code);
	bool CheckClock(int &code);

	// Implementation
	bool   FindSled(void);
	string GetStateName(void);
	void   ClearStatus(void);
	void   ResetStatus(void);
	bool   WriteStatus(void);
	bool   FlipSim(bool force);
	bool   CheckFallback(void);
	bool   FireEvent(string const &event, map<string, string> const &env, bool ipt);
	bool   SendInitialPing(void);
	bool   CheckPowerEvent(int mins, bool base);
	void   EnterPowerSave(void);
	void   InitIdleTest(void);
	bool   TestLinkIdle(void);
	string GetSimConfig(string const &key, string const &def);
	int    GetSimConfig(string const &key, int def);
	string GetService(int ctype);
	bool   IsModel(string model);
	bool   HasDualFirmware(void);
	bool   HasFixedApn(void);
	string GetFixedApn(void);
	void   StripQuotes(vector<string> &lines);
	void   StripQuotes(string &text);
	string GetAddr(string const &eight);
	string GetMask(string const &eight);
	string GetCommonMask(string const &a1, string const &a2);
	DWORD  GetIpFromString(string const &text);
	string GetStringFromIp(DWORD ip);
	string Combine(string const &a, string const &b);
	size_t SplitBody(string &body, size_t limit);
	BYTE   FromHex(char c);
};

// End of File
