
#include "Intern.hpp"

#include "TelitModem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../ATLib/ModemChannel.hpp"

#include "../AtLib/MbimPort.hpp"

#include "ShortMessage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Telit Modem
//

// Constructor

CTelitModem::CTelitModem(string const &face, string const &root) : CGenericModem(face, root)
{
	m_time     = m_root + "/time";

	m_location = m_root + "/location";

	m_sim	   = 0;

	m_ctx      = 1;

	m_ims	   = 0;

	m_net      = 31;

	m_req      = true;

	m_seq      = int(GetMonotonic());

	m_data     = dataNCM;

	m_reqoff   = false;

	m_power    = false;

	m_ptime    = 0;

	m_sinfo[0].valid = false;

	m_sinfo[1].valid = false;

	system("mkdir -p /tmp/crimson/sms/send");

	system("mkdir -p /tmp/crimson/sms/done");

	system("mkdir -p /tmp/crimson/sms/recv");

	ClearStatus();

	ResetStatus();
}

// Destructor

CTelitModem::~CTelitModem(void)
{
}

// Overridables

bool CTelitModem::OnConfigure(void)
{
	if( FindSled() ) {

		if( (m_power = GetConfig("power", 0)) ) {

			m_pcycle   = GetConfig("pcycle", 0);

			m_pconnect = GetConfig("pconnect", 0);

			m_pmin     = GetConfig("pmin", 0);

			m_pmax     = GetConfig("pmax", 0);
		}

		m_sim = GetConfig("slot", 0);

		return true;
	}

	return false;
}

int CTelitModem::OnExecute(void)
{
	m_cycle  = (m_cycle + 1) % 14400;

	int wait = 250;

	int code = CModemChannel::codeOkay;

	if( m_state < stateConnected ) {

		if( m_power ) {

			if( CheckPowerEvent(m_pconnect, false) ) {

				AfxTrace("abandoning connection attempt\n");

				EnterPowerSave();

				return 0;
			}
		}
	}

	switch( m_state ) {

		case stateNotPresent:
		{
			if( GetMonotonic() >= m_timer + (m_first ? 120 : 60) ) {

				AfxTrace("hardware not responding\n");

				ResetHardware();

				m_timer = GetMonotonic();

				return 0;
			}

			if( m_cmd->IsPresent() && m_aux->IsPresent() ) {

				if( m_cmd->Command("") == CModemChannel::codeOkay ) {

					if( m_aux->Command("") == CModemChannel::codeOkay ) {

						AfxTrace("found channels for %s\n", m_face.c_str());

						map<string, string> env;

						env.insert(make_pair("interface", m_face));

						SetLinkState(true);

						m_state = stateResetState;

						m_5Hz   = false;

						m_first = false;

						return 0;
					}
				}
			}
		}
		break;

		case stateResetState:
		{
			if( EnterKnownState(code) ) {

				m_state = stateSelectSim;

				return 0;
			}
		}
		break;

		case stateSelectSim:
		{
			if( SelectSim(code, m_sim) ) {

				m_state = stateWaitForSim;

				return 0;
			}
		}
		break;

		case stateWaitForSim:
		{
			if( GetMonotonic() >= m_timer + 60 ) {

				AfxTrace("SIM not responding\n");

				if( FlipSim(false) ) {

					return 0;
				}

				ResetHardware();

				m_state = stateNotPresent;

				return 0;
			}

			if( IsSimReady(code) ) {

				m_state = stateListCarriers;

				return 500;
			}

			wait = 250;
		}
		break;

		case stateListCarriers:
		{
			if( ListCarriers(code) && FindModel(code) ) {

				m_state = stateCheckModel;

				return 0;
			}
		}
		break;

		case stateCheckModel:
		{
			if( CheckModel(code) ) {

				m_state = stateReadSimInfo;

				return 0;
			}
			else {
				if( code == CModemChannel::codeBusy ) {

					return 500;
				}

				m_state = stateNotPresent;

				return 0;
			}
		}
		break;

		case stateReadSimInfo:
		{
			if( ReadSimInfo(code) ) {

				m_state = stateCheckFirmware;

				return 0;
			}
		}
		break;

		case stateCheckFirmware:
		{
			if( CheckFirmware(code) ) {

				m_state = stateInitialize;

				return 0;
			}
			else {
				m_state = stateNotPresent;

				return 5000;
			}
		}
		break;

		case stateInitialize:
		{
			if( PerformBasicInit(code) ) {

				if( IsModel("LE910C1") ? SoftDetach(code) : SetFunction(code, 4, false) ) {

					if( InitializeContexts(code) ) {

						if( IsModel("LE910C1") || SetFunction(code, 1, false) ) {

							if( ConfigureRadio(code) ) {

								m_state = stateRegister1;

								return 0;
							}
						}
					}

					wait = 1000;
				}
			}
		}
		break;

		case stateRegister1:
		{
			m_state = stateRegister2;

			return 0;
		}

		case stateRegister2:
		{
			bool deny = false;

			if( m_timer ) {

				if( GetMonotonic() >= m_timer + 120 ) {

					AfxTrace("failed to register\n");

					if( FlipSim(false) ) {

						return 0;
					}
					else {
						m_state = stateRegister1;

						return 0;
					}
				}
			}

			if( IsRegistered(code, deny) ) {

				if( !m_roam || GetSimConfig("roam", 0) ) {

					m_state = stateAttach;

					return 0;
				}

				if( FlipSim(false) ) {

					AfxTrace("switch on roaming\n");

					return 0;
				}

				AfxTrace("roaming not allowed\n");

				wait = 10000;
			}
			else {
				if( deny ) {

					if( ++m_count > 10 ) {

						if( FlipSim(false) ) {

							AfxTrace("switch on denied\n");

							return 0;
						}
					}
				}
				else {
					m_count = 0;
				}

				CheckServices(false);

				wait = 1000;
			}
		}
		break;

		case stateAttach:
		{
			if( GetMonotonic() >= m_timer + 10 ) {

				m_state = stateInitialize;

				return 0;
			}

			if( IsAttached(code) ) {

				m_cycle = 0;

				m_state = stateReady;

				return 0;
			}

			CheckServices(false);

			wait = 1000;
		}
		break;

		case stateReady:
		{
			if( m_req ) {

				if( DataConnect(code) ) {

					SendInitialPing();

					InitIdleTest();

					m_ctime = GetMonotonic();

					m_state = stateConnected;

					return 0;
				}
			}
			else {
				bool save = m_cmd->SetTrace(false);

				if( CheckStatus(code) && CheckServices(true) ) {

					wait = 1000;
				}

				m_cmd->SetTrace(save);
			}
		}
		break;

		case stateConnected:
		{
			if( !m_req ) {

				m_state = stateReady;

				return 0;
			}
			else {
				if( m_power ) {

					if( !m_pmin || CheckPowerEvent(m_pmin, true) ) {

						if( TestLinkIdle() || CheckPowerEvent(m_pmax, true) ) {

							EnterPowerSave();

							return 0;
						}

						m_reqoff = true;
					}
				}

				bool save = m_cmd->SetTrace(false);

				bool tick = !(m_cycle % 12);

				if( tick && !CheckConnected(code) ) {

					m_state = stateRegister2;

					m_cmd->SetTrace(save);

					return 0;
				}
				else {
					if( tick && CheckFallback() ) {

						return 0;
					}

					if( CheckStatus(code) && CheckServices(true) ) {

						wait = 1000;
					}
				}

				m_cmd->SetTrace(save);
			}
		}
		break;

		case statePowerSave:
		{
			if( GetMonotonic() >= m_ptime ) {

				m_state = stateNotPresent;

				return 0;
			}

			return 1000;
		}
		break;
	}

	if( code == CModemChannel::codeOkay ) {

		return wait;
	}

	if( code == CModemChannel::codeFailed || m_state == stateResetState ) {

		m_state = stateNotPresent;

		ResetStatus();

		return 1000;
	}

	m_state = stateResetState;

	ResetStatus();

	return 1000;
}

void CTelitModem::OnNewState(void)
{
	if( m_state == stateNotPresent ) {

		if( m_power ) {

			if( !m_ptime ) {

				m_ptime = GetMonotonic();
			}
		}
	}

	if( m_state < stateReady && m_state < m_prev ) {

		if( ++m_loops == 5 ) {

			AfxTrace("too many cycles\n");

			ResetHardware();

			m_state = stateNotPresent;
		}
	}

	if( m_prev == stateConnected ) {

		int code;

		DataDisconnect(code, true);

		RunLinkStart(false);

		m_reqoff = false;
	}

	if( m_prev == stateNone || m_prev == statePowerSave ) {

		PowerHardware(true);
	}

	if( m_state == statePowerSave ) {

		PowerHardware(false);
	}

	WriteStatus();
}

void CTelitModem::OnStopping(void)
{
	if( m_state == statePowerSave ) {

		PowerHardware(true);
	}

	if( m_state == stateConnected ) {

		int code;

		DataDisconnect(code, true);

		RunLinkStart(false);
	}

	ClearStatus();
}

bool CTelitModem::OnCommand(string const &cmd)
{
	if( cmd == "reset" || cmd == "failed" ) {

		ResetHardware();

		m_state = stateNotPresent;

		return true;
	}

	if( cmd == "flip" ) {

		FlipSim(false);

		return true;
	}

	return false;
}

// Modem Commands

bool CTelitModem::EnterKnownState(int &code)
{
	char const *cmd[] = {

		"E1",
		"E1",
		"Z",
		"+CMEE=2",
		"#GPIO=1,0,2",
		"#SLED=2"
	};

	for( size_t n = 0; n < elements(cmd); n++ ) {

		if( !Okay(code, cmd[n]) && n > 0 ) {

			return false;
		}
	}

	return true;
}

bool CTelitModem::PerformBasicInit(int &code)
{
	char const * const *list = NULL;

	if( IsModel("V2") ) {

		static char const *cmd[] = {

			"#PSNT=0",
			"+CGSMS=3",
			"+CNMI=0",
			"#PORTCFG=11",
			"$GPSD=5",
			"$GPSGPIO=5,6,8,7",
			"$GPSSERSPEED=9600",
			"#CCLKMODE=1",
			"#NITZ=3,0",
			NULL
		};

		list = cmd;
	}
	else {
		static char const *cmd[] = {

			"#PSNT=0",
			"+CGSMS=3",
			"+CNMI=0",
			"#CCLKMODE=1",
			"#NITZ=15,0",
			"#CLKSRC=0",
			NULL
		};

		list = cmd;
	}

	if( list ) {

		for( size_t n = 0; list[n]; n++ ) {

			if( !Okay(code, list[n]) ) {

				return false;
			}
		}

		if( Okay(code, "+CMGF=%u", m_ims ? 1 : 0) ) {

			return true;
		}
	}

	return false;
}

bool CTelitModem::ListCarriers(int &code)
{
	if( m_carriers.empty() ) {

		bool save = m_cmd->SetTrace(false);

		vector<string> lines;

		if( Okay(code, lines, "+COPN") ) {

			m_carriers.clear();

			for( auto const &line : lines ) {

				vector<string> r;

				if( ParseReply(r, line, 2) ) {

					StripQuotes(r);

					m_carriers.insert(make_pair(r[0], r[1]));
				}
			}

			m_cmd->SetTrace(save);

			return true;
		}

		m_cmd->SetTrace(save);

		m_carriers.clear();

		return false;
	}

	return true;
}

bool CTelitModem::FindModel(int &code)
{
	vector<string> lines;

	if( Okay(code, lines, "+GMM") ) {

		if( lines.size() >= 2 ) {

			m_model = lines[1];

			return true;
		}
	}

	m_model = "Unknown";

	return true;
}

bool CTelitModem::SelectSim(int &code, int sim)
{
	vector<int> r;

	if( OkayAndParse(code, r, 2, "#GPIO=2,2,1") ) {

		int state = (sim == 0);

		if( r[0] == 1 ) {

			if( r[1] == state ) {

				return true;
			}
		}

		if( Okay(code, "#SIMDET=0") ) {

			usleep(200 * 1000);

			if( Okay(code, "#GPIO=2,%u,1,1", state) ) {

				usleep(200 * 1000);

				if( Okay(code, "#SIMDET=1") ) {

					return true;
				}
			}
		}
	}

	return false;
}

bool CTelitModem::IsSimReady(int &code)
{
	for( int p = 0; p < 2; p++ ) {

		vector<string> lines;

		if( Okay(code, lines, "+CPIN?") ) {

			if( lines.size() >= 2 ) {

				vector<string> r;

				if( ParseReply(r, lines[1], 1) ) {

					if( p == 0 ) {

						if( r[0] == "SIM PIN" ) {

							string pin(GetSimConfig("pin", ""));

							if( !pin.empty() ) {

								if( Okay(code, "+CPIN=%s", pin.c_str()) ) {

									continue;
								}
							}

							return false;
						}
					}

					if( r[0] == "READY" ) {

						return true;
					}
				}
			}
		}
		else {
			if( code == CModemChannel::codeError ) {

				if( lines[0] == "+CME ERROR: SIM not inserted" ) {

					code = CModemChannel::codeOkay;
				}
			}

			if( code == CModemChannel::codeError ) {

				if( lines[0] == "+CME ERROR: SIM busy" ) {

					code = CModemChannel::codeOkay;

					return false;
				}
			}
		}
	}

	return false;
}

bool CTelitModem::ReadSimInfo(int &code)
{
	vector<string> lines;

	if( Okay(code, lines, "+CIMI") ) {

		if( lines.size() >= 2 ) {

			CSim &s = m_sinfo[m_sim];

			s.imsi  = lines[1];

			vector<string> r;

			if( OkayAndParse(code, r, 1, "#CCID") ) {

				s.iccid = r[0];

				s.ccode = s.imsi.substr(0, 6);

				auto it = m_carriers.find(s.ccode);

				if( it == m_carriers.end() ) {

					s.ccode = s.imsi.substr(0, 5);

					it = m_carriers.find(s.ccode);
				}

				if( it == m_carriers.end() ) {

					s.cname = "UNKNOWN";
				}
				else
					s.cname = it->second;

				s.valid = true;

				AfxTrace("sim is %s\n", s.cname.c_str());

				return true;
			}
		}
	}

	return false;
}

bool CTelitModem::CheckModel(int &code)
{
	if( IsModel("LE910C1") ) {

		vector<string> lines;

		if( Okay(code, lines, "#USBCFG?") ) {

			if( lines.size() >= 2 ) {

				vector<int> r;

				if( ParseReply(r, lines[1], 1) ) {

					int cfg = 2;

					if( r[0] == cfg ) {

						if( r[0] == 2 ) {

							m_data = dataMBIM;
						}

						if( r[0] == 4 ) {

							m_data = dataECM;
						}

						return true;
					}

					if( Okay(code, "#USBCFG=%d", cfg) ) {

						return false;
					}
				}
			}
		}
		else {
			if( code == CModemChannel::codeError ) {

				if( lines[0] == "+CME ERROR: operation not supported" ) {

					code = CModemChannel::codeBusy;
				}
			}
		}

		return false;
	}

	return true;
}

bool CTelitModem::CheckFirmware(int &code)
{
	vector<int> r { 0 };

	if( !HasDualFirmware() || OkayAndParse(code, r, 1, "#FWSWITCH?") ) {

		int fw = 0;

		string name(m_sinfo[m_sim].cname);

		string test("verizon");

		for( char &c : name ) {

			c = tolower(c);
		}

		if( name == test || (name.find(test + ' ') != string::npos || name.find(' ' + test) != string::npos) ) {

			fw = 1;
		}

		if( r[0] == fw ) {

			if( r[0] == 0 ) {

				m_ctx = 1;

				m_ims = 0;

				switch( GetSimConfig("speed", 0) ) {

					case 0: m_net = 31; break;
					case 1: m_net = 28; break;
					case 2: m_net = 22; break;
				}
			}
			else {
				m_ctx = 3;

				m_ims = 1;

				m_net = 28;
			}

			vector<string> r;

			if( OkayAndParse(code, r, 1, "#CGMR") ) {

				m_version = r[0];

				vector<string> lines;

				if( Okay(code, lines, "+CGSN") ) {

					if( lines.size() >= 2 ) {

						m_imei = lines[1];
					}

					return true;
				}
			}

			return false;
		}

		Okay(code, "#FWSWITCH=%u,1", fw);

		m_loops = 0;

		sleep(5);
	}

	return false;
}

bool CTelitModem::ConfigureRadio(int &code)
{
	vector<int> r;

	vector<string> x;

	if( OkayAndParse(code, x, 1, "+WS46=?") ) {

		bool found = false;

		for( auto const &i : x ) {

			size_t n = i.size();

			if( n ) {

				auto it = i.find('-');

				if( it != string::npos ) {

					if( m_net >= atoi(i.substr(0, it).c_str()) ) {

						if( m_net <= atoi(i.substr(it+1, n).c_str()) ) {

							found = true;

							break;
						}
					}
				}

				if( i[0] == '(' ) {

					if( m_net == atoi(i.substr(1).c_str()) ) {

						found = true;

						break;
					}
				}

				if( m_net == atoi(i.c_str()) ) {

					found = true;

					break;
				}
			}
		}

		if( !found ) {

			string val = *x.rbegin();

			auto it = val.find('-');

			if( it != string::npos ) {

				m_net = atoi(val.substr(it+1).c_str());
			}
			else {
				m_net = atoi(val.c_str());
			}
		}

		if( Okay(code, "+WS46=%u", m_net) ) {

			if( OkayAndParse(code, r, 1, "#AUTOBND?") ) {

				int req = GetSimConfig("band", 0) ? 0 : 2;

				if( r[0] == req ) {

					if( req == 2 ) {

						return true;
					}
				}

				vector<string> lines;

				if( Okay(code, lines, "#BND=?") ) {

					if( lines.size() >= 2 ) {

						int m4g = atoi(lines[1].substr(lines[1].rfind('-') + 1).c_str());

						int b3g = GetSimConfig("band3g", 1);

						int b4g = GetSimConfig("band4g", 0xFFFFFF) & m4g;

						if( r[0] == req ) {

							if( OkayAndParse(code, r, 3, "#BND?") ) {

								if( r[1] == b3g && r[2] == b4g ) {

									return true;
								}
							}
						}

						Okay(code, "#AUTOBND=%u", req);

						UINT k = 32768;

						if( req == 0 ) {

							while( k && !Okay(code, "#BND=0,%u,%u", b3g, b4g) ) {

								b4g &= ~k;

								k >>= 1;
							}
						}

						ResetHardware();

						code = CModemChannel::codeFailed;
					}
				}
			}
		}
	}

	return false;
}

bool CTelitModem::InitializeContexts(int &code)
{
	return InitializeDataContext(code) && (!m_ims || InitializeImsContext(code));
}

bool CTelitModem::InitializeDataContext(int &code)
{
	if( SetContext(code, m_ctx, "") ) {

		string apn = HasFixedApn() ? GetFixedApn() : GetSimConfig("apn", "");

		if( !apn.empty() ) {

			CPrintf set("\"IP\",\"%s\"", apn.c_str());

			if( SetContext(code, m_ctx, set) ) {

				int     auth = GetSimConfig("auth", 0);

				CPrintf cmd;

				if( !auth ) {

					cmd.Printf("#PDPAUTH=%u,0", m_ctx);
				}
				else {
					string user(GetSimConfig("user", ""));

					string pass(GetSimConfig("pass", ""));

					cmd.Printf("#PDPAUTH=%u,%u,%s,%s", m_ctx, auth, user.c_str(), pass.c_str());
				}

				if( Okay(code, cmd) ) {

					return true;
				}
			}
		}
		else {
			AfxTrace("[hint] blank APN detected\n");
		}
	}

	return false;
}

bool CTelitModem::InitializeImsContext(int &code)
{
	if( SetContext(code, m_ims, "") ) {

		string  apn(GetSimConfig("ims", "vzwims"));

		CPrintf set("\"IPV4V6\",\"%s\"", apn.c_str());

		if( SetContext(code, m_ims, set) ) {

			if( Okay(code, "#PDPAUTH=%u,0", 1) ) {

				if( Okay(code, "#IMSPDPSET=\"%s\"", apn.c_str()) ) {

					return true;
				}
			}
		}
	}

	return false;
}

bool CTelitModem::IsRegistered(int &code, bool &deny)
{
	char const *list[] = {

		"+CREG?",
		"+CEREG?"
	};

	for( size_t n = 0; n < elements(list); n++ ) {

		vector<int> r;

		if( OkayAndParse(code, r, 2, list[n]) ) {

			switch( r[1] ) {

				case 1:
					m_register = 1;

					m_roam     = false;

					return FindNetwork(code);

				case 5:
					m_register = 2;

					m_roam     = true;

					return FindNetwork(code);

				case 3:
					deny = true;

					break;
			}
		}
	}

	ResetStatus();

	return false;
}

bool CTelitModem::FindNetwork(int &code)
{
	vector<string> r;

	if( OkayAndParse(code, r, 4, "+COPS?") ) {

		StripQuotes(r);

		m_network = r[2];

		m_ctype   = atoi(r[3].c_str());

		return true;
	}

	return false;
}

bool CTelitModem::IsAttached(int &code)
{
	vector<int> r;

	if( OkayAndParse(code, r, 1, "+CGATT?") ) {

		if( r[0] == 1 ) {

			return true;
		}
	}

	return false;
}

bool CTelitModem::CheckStatus(int &code)
{
	vector<int> r;

	if( OkayAndParse(code, r, 2, "+CSQ") ) {

		m_signal = r[0];

		WriteStatus();

		return true;
	}

	return false;
}

bool CTelitModem::CheckConnected(int &code)
{
	bool deny = false;

	if( IsRegistered(code, deny) ) {

		vector<string> lines;

		if( Okay(code, lines, "+CGCONTRDP=") ) {

			if( lines.size() >= 2 ) {

				for( size_t i = 1; i < lines.size(); i++ ) {

					vector<string> r;

					if( ParseReply(r, lines[i], 5) ) {

						if( atoi(r[0].c_str()) == m_ctx ) {

							if( m_ipdata == lines[i] ) {

								return true;
							}

							AfxTrace("ip settings changed\n");

							break;
						}
					}
				}
			}
		}
	}

	return false;
}

bool CTelitModem::FindConnected(int &code, string &connect)
{
	vector<string> r;

	return FindConnected(code, connect, r);
}

bool CTelitModem::FindConnected(int &code, string &connect, vector<string> &r)
{
	vector<string> lines;

	if( Okay(code, lines, "+CGCONTRDP=") ) {

		if( lines.size() >= 2 ) {

			for( size_t i = 1; i < lines.size(); i++ ) {

				if( ParseReply(r, lines[i], 5) ) {

					if( atoi(r[0].c_str()) == m_ctx ) {

						connect = lines[i];

						return true;
					}
				}
			}
		}
	}

	return false;
}

bool CTelitModem::DataConnect(int &code)
{
	AfxTrace("connecting\n");

	map<string, string> env;

	string ipt;

	if( m_data == dataECM ) {

		if( !DataConnectEcm(code, env) ) {

			return false;
		}
	}

	if( m_data == dataNCM ) {

		if( !DataConnectNcm(code, env) ) {

			return false;
		}

		ipt = GetConfig("ipt", "");
	}

	if( m_data == dataMBIM ) {

		if( !DataConnectMbim(code, env) ) {

			return false;
		}

		ipt = GetConfig("ipt", "");
	}

	if( !env.empty() ) {

		if( !ipt.empty() ) {

			env.insert(make_pair("ipt", ipt));

			env.insert(make_pair("rule", GetConfig("rule", "1000")));

			env.insert(make_pair("metric", GetConfig("metric", "3000")));

			env.insert(make_pair("secaddr", GetConfig("secaddr", "192.168.255.254")));

			env.insert(make_pair("secgate", GetConfig("secgate", "192.168.255.255")));

			env.insert(make_pair("secmask", GetConfig("secmask", "192.168.255.254")));
		}

		RunLinkStart(true);

		SetLinkState(true);

		FireEvent("bound", env, !ipt.empty());

		if( ipt.empty() ) {

			SendInitialPing();
		}

		return true;
	}

	AfxTrace("no suitable ip address\n");

	code = CModemChannel::codeError;

	return false;
}

bool CTelitModem::DataConnectEcm(int &code, map<string, string> &env)
{
	string addr = GetConfig("secaddr", "192.168.255.254");

	string gate = GetConfig("secgate", "192.168.255.255");

	string mask = GetCommonMask(addr, gate);

	Okay(code, "#ECMC=0,0,\"%s\"", addr.c_str());

	Okay(code, "#ECMC=0,1,\"%s\"", mask.c_str());

	Okay(code, "#ECMC=0,2,\"%s\"", gate.c_str());

	Okay(code, "#ECMC=0,5,\"02:05:e4:00:00:0%u\"", m_sled);

	CPrintf c("#ECM=%u,0", m_ctx);

	if( Okay(code, m_cmd->Command(c, 30000)) ) {

		vector<string> r;

		if( FindConnected(code, m_ipdata, r) ) {

			StripQuotes(r);

			env.insert(make_pair("interface", m_face));

			env.insert(make_pair("ip", addr));

			env.insert(make_pair("subnet", mask));

			if( GetConfig("router", 0) ) {

				env.insert(make_pair("defaultroute", "yes"));

				env.insert(make_pair("router", gate));

				env.insert(make_pair("common", GetCommonMask(addr, gate)));
			}

			if( GetConfig("dns", 0) && r.size() >= 7 ) {

				env.insert(make_pair("dns", Combine(r[5], r[6])));
			}

			m_addr = GetAddr(r[3]);

			if( !ArpPing(gate) ) {

				if( !HardDetach(code) ) {

					ResetHardware();
				}

				m_state = stateNotPresent;

				code    = CModemChannel::codeFailed;

				return false;
			}
		}
	}

	return true;
}

bool CTelitModem::DataConnectNcm(int &code, map<string, string> &env)
{
	if( Okay(code, "#NCM=1,%u", m_ctx) ) {

		vector<string> lines;

		if( Okay(code, lines, "+CGACT=1,%u", m_ctx) ) {

			int need = CModemChannel::codeConnect;

			if( Test(code, need, "+CGDATA=\"M-RAW_IP\",%u", m_ctx) ) {

				vector<string> r;

				if( FindConnected(code, m_ipdata, r) && r.size() >=7 ) {

					StripQuotes(r);

					env.insert(make_pair("interface", m_face));

					env.insert(make_pair("ip", (m_addr = GetAddr(r[3]))));

					env.insert(make_pair("subnet", GetMask(r[3])));

					if( GetConfig("router", 0) ) {

						env.insert(make_pair("defaultroute", "yes"));

						env.insert(make_pair("router", r[4]));

						env.insert(make_pair("common", GetCommonMask(m_addr, r[4])));
					}

					if( GetConfig("dns", 0) ) {

						env.insert(make_pair("dns", Combine(r[5], r[6])));
					}
				}
			}
		}
		else {
			if( code == CModemChannel::codeError ) {

				if( lines.size() ) {

					if( lines[0].find("(#33)") != string::npos ) {

						AfxTrace("[hint] check for incorrect APN\n");
					}
				}
			}
		}
	}

	return true;
}

bool CTelitModem::DataConnectMbim(int &code, map<string, string> &env)
{
	CMbimPort dev(m_dev3);

	if( dev.OpenPort() ) {

		// LE910C1 does not always come online if not shutdown gracefully i.e.after
		// a power cycle or sled reset. Opening and closing a loopback data connection 
		// prior to the actual data stream to clear the session. 

		string apn = HasFixedApn() ? GetFixedApn() : GetSimConfig("apn", "");

		for( int n = 0; n < 2; n++ ) {

			if( dev.Open() ) {

				bool ready;

				if( dev.QuerySubscriberReady(ready) && ready ) {

					string apn, user, pass;

					int auth;

					if( n == 0 ) {

						apn  = "loopback";

						auth = 0;
					}
					else {
						apn  = HasFixedApn() ? GetFixedApn() : GetSimConfig("apn", "");

						auth = GetSimConfig("auth", 0);

						if( auth ) {

							user = GetSimConfig("user", "");

							pass = GetSimConfig("pass", "");
						}
					}

					if( dev.Connect(apn, user, pass, auth) ) {

						if( n ) {

							string gate, mask, dns;

							if( !dev.QueryIpConfig(m_addr, gate, mask, dns) || m_addr.empty() || dns.empty() ) {

								vector<string> r;

								if( FindConnected(code, m_ipdata, r) ) {

									StripQuotes(r);

									if( m_addr.empty() ) {

										m_addr = GetAddr(r[3]);

										mask   = GetMask(r[4]);

										gate   = "0.0.0.0";
									}

									if( dns.empty() && r.size() >= 7 ) {

										dns = Combine(r[5], r[6]);
									}
								}
							}
							else {
								AfxTrace("mbim \"%s\",\"%s\",\"%s\",\"%s\"\n", m_addr.c_str(), gate.c_str(), mask.c_str(), dns.c_str());

								FindConnected(code, m_ipdata);
							}

							if( !m_addr.empty() ) {

								env.insert(make_pair("interface", m_face));

								env.insert(make_pair("ip", m_addr));

								env.insert(make_pair("subnet", mask));

								if( GetConfig("router", 0) ) {

									env.insert(make_pair("defaultroute", "yes"));

									env.insert(make_pair("router", gate));

									env.insert(make_pair("common", GetCommonMask(m_addr, gate)));
								}

								if( GetConfig("dns", 0) && !dns.empty() ) {

									env.insert(make_pair("dns", dns));
								}

								return true;
							}
						}

						dev.Disconnect();
					}
				}

				dev.Close();
			}
		}

		dev.ClosePort();

		code = CModemChannel::codeFailed;
	}

	return false;
}

bool CTelitModem::DataDisconnect(int &code, bool kill)
{
	AfxTrace("disconnecting\n");

	ResetStatus();

	if( m_data == dataECM ) {

		if( IsModel("LE910C1") ) {

			// The CFUN=4 command causes SIM access issues.

			if( Okay(code, m_cmd->Command("#ECMD=0", 60000)) ) {

				return true;
			}
		}
	}

	if( m_data == dataMBIM ) {

		CMbimPort dev(m_dev3);

		if( dev.OpenPort() ) {

			bool state;

			if( dev.QueryConnect(state) && state ) {

				dev.Disconnect();
			}

			dev.Close();

			dev.ClosePort();
		}

		return true;
	}

	if( SetFunction(code, 4, false) ) {

		if( kill || SetFunction(code, 1, false) ) {

			return true;
		}
	}

	return false;
}

bool CTelitModem::SetFunction(int &code, int func, bool reset)
{
	CPrintf c("+CFUN=%u", func);

	if( func == 1 && reset ) {

		c += ",1";
	}

	return Okay(code, m_cmd->Command(c, 15000));
}

bool CTelitModem::SetContext(int &code, int ctx, string const &set)
{
	CPrintf c("+CGDCONT=%u", ctx);

	if( !set.empty() ) {

		c += ',';

		c += set;
	}

	return Okay(code, c);
}

bool CTelitModem::SendMessage(int &code, string const &rcpt, string body)
{
	// TODO -- Carriage returns etc.? !!!
	// TODO -- Continuation messages? !!!

	bool save = m_cmd->SetTrace(true);

	for( ;;) {

		size_t pos  = SplitBody(body, 160);

		bool   sent = false;

		if( m_ims ) {

			// Text Mode

			if( Test(code, CModemChannel::codePrompt, "+CMGS=%s", rcpt.c_str()) ) {

				if( (code = m_cmd->SendText(body.substr(0, pos))) == CModemChannel::codeOkay ) {

					sent = true;
				}
			}
		}
		else {
			CShortMessage Msg;

			Msg.SetNumber(rcpt.c_str());

			Msg.SetMessage(body.substr(0, pos).c_str());

			BYTE   pdu[256];

			size_t count = Msg.BuildPDU(pdu);

			if( Test(code, CModemChannel::codePrompt, "+CMGS=%u", count-1) ) {

				if( (code = m_cmd->SendHex(pdu, count)) == CModemChannel::codeOkay ) {

					sent = true;
				}
			}
		}

		if( sent ) {

			if( pos < body.size() ) {

				body.erase(0, pos);

				continue;
			}

			m_cmd->SetTrace(save);

			return true;
		}

		break;
	}

	m_cmd->SetTrace(save);

	return false;
}

bool CTelitModem::ReadMessage(string &rcpt, string &body, vector<string> const &data, string const &line)
{
	if( m_ims ) {

		rcpt = (data[2].size() == 10) ? ("1" + data[2]) : data[2];

		body = line;

		return true;
	}
	else {
		vector<BYTE> bytes;

		for( size_t c = 0; c + 1 < line.size(); c += 2 ) {

			BYTE b = ((FromHex(line[c+0]) << 4) | FromHex(line[c+1]));

			bytes.push_back(b);
		}

		CShortMessage Msg;

		Msg.ParsePDU(bytes.data());

		rcpt = Msg.GetNumber();

		body = Msg.GetMessage();

		return true;
	}

	return false;
}

bool CTelitModem::TestMessage(vector<int> &kill, vector<string> const &data)
{
	if( m_ims ) {

		if( data[1].substr(0, 4) == "REC " ) {

			kill.push_back(atoi(data[0].c_str()));

			return data[1] == "REC UNREAD";
		}
	}
	else {
		int status = atoi(data[1].c_str());

		if( status == 0 || status == 1 ) {

			kill.push_back(atoi(data[0].c_str()));

			return status == 0;
		}
	}

	return false;
}

bool CTelitModem::HardDetach(int &code)
{
	if( SetFunction(code, 4, false) ) {

		if( SetFunction(code, 1, true) ) {

			return true;
		}
	}

	return false;
}

bool CTelitModem::SoftDetach(int &code)
{
	if( m_data != dataECM || Okay(code, "#ECMD=0") ) {

		if( Okay(code, "+CGATT=0") ) {

			if( Okay(code, "+CGACT=0") ) {

				return true;
			}
		}
	}

	return false;
}

bool CTelitModem::ArpPing(string const &gate)
{
	CPrintf s("/bin/arping -c 1 -I %s %s", m_face.c_str(), gate.c_str());

	AfxTrace("%s\n", s.c_str());

	return system(s) == 0;
}

// Service Checks

bool CTelitModem::CheckServices(bool online)
{
	int code;

	if( online ) {

		CheckMessages(code);

		CheckGps(code);

		CheckClock(code);
	}
	else {
		CheckGps(code);

		CheckClock(code);
	}

	return true;
}

bool CTelitModem::CheckMessages(int &code)
{
	return CheckMessageSend(code) && CheckMessageRecv(code);
}

bool CTelitModem::CheckMessageSend(int &code)
{
	string send = "/tmp/crimson/sms/send/";

	string done = "/tmp/crimson/sms/done/";

	bool   fail = false;

	struct dirent **list;

	size_t c;

	if( (c = scandir(send.c_str(), &list, nullptr, nullptr)) > 0 ) {

		int sms = GetConfig("sms", 1);

		for( size_t n = 0; n < c; n++ ) {

			struct dirent *file = list[n];

			if( !fail ) {

				string name(file->d_name);

				if( name.size() > 3 ) {

					size_t p = name.size() - 3;

					if( name.substr(p) == ".go" ) {

						bool     okay = false;

						string   data = name.substr(0, p);

						ifstream istm(send + data);

						if( istm.good() ) {

							if( sms >= 1 ) {

								string rcpt, body;

								if( getline(istm, rcpt) && getline(istm, body) ) {

									if( SendMessage(code, rcpt, body) ) {

										AfxTrace("message sent to %s\n", rcpt.c_str());

										okay = true;
									}
								}
							}
						}

						unlink((send + data).c_str());

						unlink((send + name).c_str());

						ofstream(done + data + ".done") << (okay ? "1\n" : "0\n");
					}
				}

			}

			free(file);
		}

		free(list);
	}

	return !fail;
}

bool CTelitModem::CheckMessageRecv(int &code)
{
	vector<string> lines;

	if( Okay(code, lines, "+CMGL=%s", m_ims ? "\"ALL\"" : "4") ) {

		if( lines.size() > 1 ) {

			int sms = GetConfig("sms", 1);

			vector<int> kill;

			for( size_t m = 1; m < lines.size(); m += 2 ) {

				vector<string> r;

				if( ParseReply(r, lines[m], 4) ) {

					string rcpt, body;

					StripQuotes(r);

					if( TestMessage(kill, r) ) {

						if( ReadMessage(rcpt, body, r, lines[m+1]) ) {

							AfxTrace("message recieved from %s\n", rcpt.c_str());

							string   recv = "/tmp/crimson/sms/recv/";

							string   name = CPrintf("%8.8X", m_seq++);

							ofstream data(recv + name);

							if( data.good() ) {

								data << rcpt << '\n';

								data << body << '\n';

								data.close();

								ofstream(recv + name + ".go");
							}
						}
					}
				}
			}

			for( auto i = kill.rbegin(); i != kill.rend(); i++ ) {

				Okay(code, "+CMGD=%u", *i);
			}
		}
	}

	return true;
}

bool CTelitModem::CheckGps(int &code)
{
	int hide;

	if( GetConfig("gps", 0) ) {

		vector<int> r;

		if( OkayAndParse(code, r, 1, "$GPSP?") ) {

			if( r[0] == 1 || Okay(code, "$GPSP=1") ) {

				vector<string> r;

				if( OkayAndParse(hide, r, 11, "$GPSACP") ) {

					if( !m_5Hz ) {

						if( IsModel("LE910C1") || Okay(hide, "$GNSS5HZ=1") ) {

							m_5Hz = true;
						}
					}

					timeval tv = { 0 };

					gettimeofday(&tv, NULL);

					double lat = 0;
					double lon = 0;
					double alt = 0;
					int    fix = 0;

					if( !r[1].empty() && !r[2].empty() ) {

						if( (fix = atoi(r[5].c_str())) >= 2 ) {

							lat = atoi(r[1].substr(0, 2).c_str()) + strtod(r[1].substr(2).c_str(), NULL) / 60;

							lon = atoi(r[2].substr(0, 3).c_str()) + strtod(r[2].substr(3).c_str(), NULL) / 60;

							if( r[1][r[1].size()-1] == 'S' ) {

								lat *= -1;
							}

							if( r[2][r[2].size()-1] == 'W' ) {

								lon *= -1;
							}

							if( fix >= 3 ) {

								alt = strtod(r[4].c_str(), NULL) * 3.28084;
							}
						}
					}

					if( fix > 1 ) {

						struct tm time = { 0 };

						time.tm_mday =   0 + atoi(r[9].substr(0, 2).c_str());
						time.tm_mon  =  -1 + atoi(r[9].substr(2, 2).c_str());
						time.tm_year = 100 + atoi(r[9].substr(4, 2).c_str());

						time.tm_hour = atoi(r[0].substr(0, 2).c_str());
						time.tm_min  = atoi(r[0].substr(2, 2).c_str());
						time.tm_sec  = atoi(r[0].substr(4, 2).c_str());

						int ts = timegm(&time);

						int tm = atoi(r[0].substr(7, 3).c_str());

						int rs = tv.tv_sec;

						int rm = tv.tv_usec / 1000;

						string   tmp(m_location + ".tmp");

						ofstream stm(tmp);

						if( stm.good() ) {

							stm << "{\n";

							stm << "\t\"fix\": " << fix << ",\n";

							if( fix >= 2 ) {

								stm.precision(7);

								stm << "\t\"lat\": " << std::fixed << lat << ",\n";

								stm << "\t\"long\": " << std::fixed << lon << ",\n";

								if( fix >= 3 ) {

									stm.precision(2);

									stm << "\t\"alt\": " << std::fixed << alt << ",\n";
								}
							}

							if( ts >= 1577836800 ) {

								stm << "\t\"ts\": " << ts << ",\n";

								stm << "\t\"tm\": " << tm << ",\n";

								stm << "\t\"rs\": " << rs << ",\n";

								stm << "\t\"rm\": " << rm << "\n";
							}

							stm << "}\n";

							stm.close();

							unlink(m_location.c_str());

							rename(tmp.c_str(), m_location.c_str());

							return true;
						}
					}
				}
			}
		}

		unlink(m_location.c_str());

		return false;
	}

	return true;
}

bool CTelitModem::CheckClock(int &code)
{
	int hide;

	if( m_register && GetConfig("time", 0) ) {

		bool valid = false;

		if( IsModel("LE910C1") ) {

			vector<int> r;

			if( OkayAndParse(hide, r, 2, "#CLKSRC?") ) {

				if( r[1] != 0 ) {

					valid = true;
				}
			}
		}

		if( IsModel("V2") || valid ) {

			vector<string> r;

			if( OkayAndParse(hide, r, 2, "#CCLK?") ) {

				timeval tv = { 0 };

				gettimeofday(&tv, NULL);

				if( !r[0].empty() && !r[1].empty() ) {

					StripQuotes(r[0]);

					if( r.size() == 3 ) {

						StripQuotes(r[2]);
					}

					struct tm time = { 0 };

					time.tm_year = 100 + atoi(r[0].substr(0, 2).c_str());
					time.tm_mon  =  -1 + atoi(r[0].substr(3, 2).c_str());
					time.tm_mday =   0 + atoi(r[0].substr(6, 2).c_str());

					time.tm_hour = atoi(r[1].substr(0, 2).c_str());
					time.tm_min  = atoi(r[1].substr(3, 2).c_str());
					time.tm_sec  = atoi(r[1].substr(6, 2).c_str());

					int ts = timegm(&time);

					if( ts >= 1577836800 ) {

						int tm = 500;

						int rs = tv.tv_sec;

						int rm = tv.tv_usec / 1000;

						int ds = 9999;

						int tz = 9999;

						if( r[1].size() > 8 ) {

							ds = (r.size() == 3) ? atoi(r[2].c_str()) * 60 : 0;

							tz = atoi(r[1].substr(8).c_str()) * 15 - ds;
						}

						if( IsModel("LE910C1") ) {

							ts = ts - 60 * tz;
						}

						string   tmp(m_time + ".tmp");

						ofstream stm(tmp);

						if( stm.good() ) {

							stm << "{\n";

							stm << "\t\"ts\": " << ts << ",\n";

							stm << "\t\"tm\": " << tm << ",\n";

							stm << "\t\"rs\": " << rs << ",\n";

							stm << "\t\"rm\": " << rm << ",\n";

							stm << "\t\"tz\": " << tz << ",\n";

							stm << "\t\"ds\": " << ds << "\n";

							stm << "}\n";

							stm.close();

							unlink(m_time.c_str());

							rename(tmp.c_str(), m_time.c_str());

							return true;

						}
					}
				}
			}
		}

		unlink(m_time.c_str());

		return false;
	}

	return true;
}

// Implementation

bool CTelitModem::FindSled(void)
{
	if( m_face.size() == 5 ) {

		if( m_face.substr(0, 4) == "wwan" ) {

			m_sled = 1 + atoi(m_face.substr(4).c_str());

			m_dev1 = CPrintf("/dev/cellular_modem%u", m_sled);

			m_dev2 = CPrintf("/dev/cellular_gps%u", m_sled);

			m_dev3 = CPrintf("/dev/cellular_wdm%u", m_sled);

			m_cmd  = unique_ptr<CModemChannel>(new CModemChannel(m_dev1));

			m_aux  = unique_ptr<CModemChannel>(new CModemChannel(m_dev2));

			return true;
		}
	}

	return false;
}

string CTelitModem::GetStateName(void)
{
	switch( m_state ) {

		case stateResetState:
			return "Initializing Modem";

		case stateSelectSim:
			return "Selecting SIM";

		case stateWaitForSim:
			return "Waiting for SIM";

		case stateListCarriers:
			return "Listing Carriers";

		case stateCheckModel:
			return "Checking Provisioning";

		case stateReadSimInfo:
			return "Reading SIM Data";

		case stateCheckFirmware:
			return "Checking Firmware";

		case stateInitialize:
			return "Configuring Modem";

		case stateRegister1:
		case stateRegister2:
			return "Registering";

		case stateAttach:
			return "Attaching";

		case stateReady:
			return "Ready";

		case stateConnect:
			return "Connecting";

		case stateConnected:
			return "Connected";

		case statePowerSave:
			return "Powered Down";
	}

	return CInterface::GetStateName();
}

void CTelitModem::ClearStatus(void)
{
	unlink(m_time.c_str());

	unlink(m_location.c_str());

	CInterface::ClearStatus();
}

void CTelitModem::ResetStatus(void)
{
	m_signal   = 0;

	m_register = 0;

	m_roam     = false;

	m_ctype    = 99;

	m_ctime    = 0;

	m_network.clear();

	m_addr.clear();
}

bool CTelitModem::WriteStatus(void)
{
	string   tmp(m_status + ".tmp");

	ofstream stm(tmp);

	if( stm.good() ) {

		bool online = (m_state == stateConnected);

		stm << "{\n";

		stm << "\t\"state\": \"" << GetStateName().c_str() << "\",\n";

		stm << "\t\"slot\": " << m_sim << ",\n";

		stm << "\t\"model\": \"" << m_model << "\",\n";

		stm << "\t\"version\": \"" << m_version << "\",\n";

		stm << "\t\"carrier\": \"" << m_sinfo[m_sim].cname << "\",\n";

		stm << "\t\"iccid\": \"" << m_sinfo[m_sim].iccid << "\",\n";

		stm << "\t\"imsi\": \"" << m_sinfo[m_sim].imsi << "\",\n";

		stm << "\t\"imei\": \"" << m_imei << "\",\n";

		stm << "\t\"register\": " << (m_register ? "true" : "false") << ",\n";

		stm << "\t\"roam\": " << (m_roam ? "true" : "false") << ",\n";

		stm << "\t\"online\": " << (online ? "true" : "false") << ",\n";

		stm << "\t\"reqoff\": " << (m_reqoff ? "true" : "false") << ",\n";

		stm << "\t\"network\": \"" << m_network << "\",\n";

		stm << "\t\"service\": \"" << GetService(m_ctype).c_str() << "\",\n";

		stm << "\t\"signal\": " << m_signal << ",\n";

		stm << "\t\"addr\": \"" << m_addr.c_str() << "\",\n";

		stm << "\t\"ctime\": " << m_ctime << ",\n";

		AddTraffic(stm);

		stm << "\t\"valid\": true\n";

		stm << "}\n";

		stm.close();

		unlink(m_status.c_str());

		rename(tmp.c_str(), m_status.c_str());

		return true;
	}

	return false;
}

bool CTelitModem::FlipSim(bool force)
{
	if( m_state != statePowerSave ) {

		if( force || GetConfig("switch", 0) ) {

			m_sim = 1 - m_sim;

			if( m_state >= stateSelectSim ) {

				m_state = stateSelectSim;

				ResetStatus();
			}

			return true;
		}
	}

	return false;
}

bool CTelitModem::CheckFallback(void)
{
	if( GetConfig("switch", 0) == 2 ) {

		if( m_sim != GetConfig("slot", 0) ) {

			if( GetMonotonic() - m_ctime >= 60 * GetConfig("fallback", 30) ) {

				AfxTrace("falling back to preferred SIM\n");

				FlipSim(true);

				return true;
			}
		}
	}

	return false;
}

bool CTelitModem::FireEvent(string const &event, map<string, string> const &env, bool ipt)
{
	extern bool IsUnderDebug(void);

	AfxTrace("run script for %s\n", event.c_str());

	if( !IsUnderDebug() ) {

		int pid = fork();

		if( pid >= 0 ) {

			if( !pid ) {

				CPrintf script("/opt/crimson/scripts/c3-%s-event", ipt ? "ipt" : "dhcp");

				char **pArg = new char *[3];

				char **pEnv = new char *[env.size() + 1];

				size_t n = 0;

				for( auto i : env ) {

					pEnv[n++] = strdup((i.first + "=" + i.second).c_str());
				}

				pEnv[n] = nullptr;

				pArg[0] = strdup(script.c_str());

				pArg[1] = strdup(event.c_str());

				pArg[2] = nullptr;

				execve(pArg[0], pArg, pEnv);

				AfxTrace("failed to run script\n");

				exit(255);
			}
			else {
				int status;

				if( waitpid(pid, &status, 0) == pid ) {

					if( WIFEXITED(status) ) {

						AfxTrace("script completed (%u)\n", WEXITSTATUS(status));

						if( WEXITSTATUS(status) == 0 ) {

							return true;
						}
					}
				}
			}
		}

		return false;
	}

	return true;
}

bool CTelitModem::SendInitialPing(void)
{
	system(CPrintf("/bin/ping -c 1 -w 1 -I %s 8.8.8.8", m_face.c_str()));

	return true;
}

bool CTelitModem::CheckPowerEvent(int mins, bool base)
{
	if( mins ) {

		if( GetMonotonic() >= (base ? m_ctime : m_ptime) + 60 * mins ) {

			return true;
		}
	}

	return false;
}

void CTelitModem::EnterPowerSave(void)
{
	AfxTrace("entering power save mode\n");

	m_state  = statePowerSave;

	m_ptime += 60 * m_pcycle;
}

void CTelitModem::InitIdleTest(void)
{
	m_itime  = 0;

	m_txlast = 0;
}

bool CTelitModem::TestLinkIdle(void)
{
	if( GetMonotonic() >= m_itime + 5 ) {

		if( m_itime && m_txnow == m_txlast ) {

			AfxTrace("idle link detected\n");

			return true;
		}

		m_txlast = m_txnow;

		m_itime  = GetMonotonic();
	}

	return false;
}

string CTelitModem::GetSimConfig(string const &key, string const &def)
{
	auto i = m_config.find(key + (m_sim ? '2' : '1'));

	return (i == m_config.end()) ? def : i->second;
}

int CTelitModem::GetSimConfig(string const &key, int def)
{
	auto i = m_config.find(key + (m_sim ? '2' : '1'));

	return (i == m_config.end()) ? def : atoi(i->second.c_str());
}

string CTelitModem::GetService(int ctype)
{
	switch( ctype ) {

		case 99:
			return "None";

		case 0:
		case 1:
			return "2G";

		case 2:
		case 4:
		case 5:
		case 6:
			return "3G";

		case 7:
			return "4G";
	}

	return CPrintf("%u", ctype);
}

bool CTelitModem::IsModel(string model)
{
	return m_model.find(model) != string::npos;
}

bool CTelitModem::HasDualFirmware(void)
{
	return IsModel("LE910-NA V2");
}

bool CTelitModem::HasFixedApn(void)
{
	CSim &s = m_sinfo[m_sim];

	if( s.imsi.length() >= 6 ) {

		string code(s.imsi.substr(0, 6));

		if( code == "302220" || code == "302221" || code == "302760" ) {

			return true;
		}
	}

	return false;
}

string CTelitModem::GetFixedApn(void)
{
	CSim &s = m_sinfo[m_sim];

	if( s.imsi.length() >= 6 ) {

		string code(s.imsi.substr(0, 6));

		if( code == "302220" || code == "302221" ) {

			return "isp.telus.com";
		}

		if( code == "302760" ) {

			return "m2m.telus.iot";
		}
	}

	return "";
}

void CTelitModem::StripQuotes(vector<string> &lines)
{
	for( string &s : lines ) {

		size_t n = s.length();

		if( s[0] == '"' ) {

			s = s.substr(1, --n);
		}

		if( s[n-1] == '"' ) {

			s = s.substr(0, --n);
		}
	}
}

void CTelitModem::StripQuotes(string &text)
{
	size_t n;

	if( (n = text.size()) ) {

		if( text[0] == '"' ) {

			text.erase(0, 1);

			n--;
		}
	}

	if( n ) {

		if( text[n-1] == '"' ) {

			text.erase(n-1, 1);
		}
	}
}

string CTelitModem::GetAddr(string const &eight)
{
	size_t p = -1;

	for( int n = 0; n < 4; n++ ) {

		p = eight.find('.', p+1);
	}

	return eight.substr(0, p);
}

string CTelitModem::GetMask(string const &eight)
{
	size_t p = -1;

	for( int n = 0; n < 4; n++ ) {

		p = eight.find('.', p+1);
	}

	return eight.substr(p+1);
}

string CTelitModem::GetCommonMask(string const &a1, string const &a2)
{
	DWORD ip1  = GetIpFromString(a1);

	DWORD ip2  = GetIpFromString(a2);

	DWORD mask = 0xFFFFFFFF;

	while( (ip1 & mask) != (ip2 & mask) ) {

		mask <<= 1;
	}

	return GetStringFromIp(mask);
}

DWORD CTelitModem::GetIpFromString(string const &text)
{
	in_addr in = { 0 };

	inet_aton(text.c_str(), &in);

	return ntohl(in.s_addr);
}

string CTelitModem::GetStringFromIp(DWORD ip)
{
	DWORD s = htonl(ip);

	PBYTE p = PBYTE(&s);

	return CPrintf("%u.%u.%u.%u", p[0], p[1], p[2], p[3]);
}

string CTelitModem::Combine(string const &a, string const &b)
{
	string c = a;

	if( !b.empty() ) {

		if( !c.empty() ) {

			c += ' ';
		}

		c += b;
	}

	return c;
}

size_t CTelitModem::SplitBody(string &body, size_t limit)
{
	size_t pos = body.size();

	if( pos > limit ) {

		size_t gap = body.rfind(' ', pos);

		if( gap < 3 * limit / 4 ) {

			pos = limit;
		}
		else {
			pos = gap;

			while( pos && body[pos-1] == ' ' ) {

				pos--;
			}

			body.erase(pos, gap - pos + 1);
		}
	}

	return pos;
}

BYTE CTelitModem::FromHex(char c)
{
	if( c >= '0' && c <= '9' ) {

		return BYTE(c - '0');
	}

	if( c >= 'a' && c <= 'f' ) {

		return BYTE(10 + c - 'a');
	}

	if( c >= 'A' && c <= 'F' ) {

		return BYTE(10 + c - 'A');
	}

	return 0;
}

// End of File
