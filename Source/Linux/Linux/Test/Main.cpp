
#include <fcntl.h>
#include <linux/serial.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <termios.h>
#include <unistd.h>

// Typedefs

typedef unsigned char BYTE;

// Prototypes

static	void *	MasterThread(void *arg);
static	void *	SlaveThread(void *arg);
static	bool	SendFrame(int fd);
static	bool	RecvFrame(int fd, int ms);
static	int	OpenPort(char const *name);
static	bool	SetGpio(int gpio, int state);
static	void	MakeRealTime(void);

// Code

int main(void)
{
	SetGpio(3*32+16, true);

	SetGpio(3*32+17, true);

	SetGpio(0*32+12, false);

	SetGpio(0*32+13, false);

	pthread_t t1, t2;

	pthread_create(&t1, nullptr, MasterThread, nullptr);

	pthread_create(&t2, nullptr, SlaveThread, nullptr);

	pthread_join(t1, nullptr);

	pthread_join(t2, nullptr);

	return 0;
}

static void * MasterThread(void *arg)
{
	MakeRealTime();

	int fd = OpenPort("/dev/ttyS3");

	for( ;;) {

		SendFrame(fd);

		RecvFrame(fd, 500);
	}

	close(fd);

	return nullptr;
}

static void * SlaveThread(void *arg)
{
	MakeRealTime();

	int fd = OpenPort("/dev/ttyS2");

	for( ;;) {

		if( RecvFrame(fd, 0) ) {

			SendFrame(fd);
		}
	}

	close(fd);

	return nullptr;
}

static bool SendFrame(int fd)
{
	BYTE b[64];

	b[0] = '[';

	int n = rand();

	memset(b+1, 'A' + n % 26, sizeof(b)-2);

	b[sizeof(b)-1] = ']';

	write(fd, b, sizeof(b));

	return true;
}

static bool RecvFrame(int fd, int ms)
{
	int rs = 0;

	int rp = 0;

	BYTE rx[128];

	for( ;;) {

		fd_set set;

		FD_ZERO(&set);

		FD_SET(fd, &set);

		timeval tv = { 0 };

		tv.tv_usec = 1000 * ms;

		if( select(fd+1, &set, nullptr, nullptr, ms ? &tv : nullptr) > 0 ) {

			BYTE b[64];

			int n = read(fd, b, sizeof(b));

			for( int i = 0; i < n; i++ ) {

				switch( rs ) {

					case 0:
					{
						if( b[i] == '[' ) {

							rs = 1;
							rp = 0;
						}
					}
					break;

					case 1:
					{
						if( b[i] == ']' ) {

							return true;
						}
						else {
							if( rp < sizeof(rx) ) {

								rx[rp++] = b[i];
							}
							else {
								return false;
							}
						}
					}
					break;
				}
			}

			continue;
		}

		return false;
	}
}

static int OpenPort(char const *name)
{
	int fd = open(name, O_RDWR | O_NOCTTY);

	if( fd >= 0 ) {

		struct termios cfg = { 0 };

		cfg.c_iflag  = IGNBRK;
		cfg.c_cflag  = CREAD | CLOCAL | B9600;
		cfg.c_cflag |= CS8;
		cfg.c_ispeed = B9600;
		cfg.c_ospeed = B9600;

		ioctl(fd, TCSETS, &cfg);

		struct serial_rs485 r = { 0 };

		r.delay_rts_after_send  = 0;
		r.delay_rts_before_send = 0;
		r.flags		        = SER_RS485_ENABLED | SER_RS485_RTS_AFTER_SEND;

		ioctl(fd, TIOCSRS485, &r);

		return fd;
	}

	return -1;
}

static bool SetGpio(int gpio, int state)
{
	char name[64];

	sprintf(name, "/sys/class/gpio/gpio%d/value", gpio);

	int fd = open(name, O_WRONLY, 0);

	if( fd >= 0 ) {

		char cData = state ? '1' : '0';

		write(fd, &cData, sizeof(cData));

		close(fd);

		return true;
	}

	return false;
}

static void MakeRealTime(void)
{
	#if 0

	int policy = 1;

	sched_param param;

	param.__sched_priority = 92;

	sched_setscheduler(0, policy, &param);

	#endif
}

// End of File
