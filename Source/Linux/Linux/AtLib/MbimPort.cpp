
#include "Intern.hpp"

#include "MbimPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// MBIM Port
//

// Constants

static const CMbimPort::Guid basic_connect =
{
	{ 0xA2, 0x89, 0xCC, 0x33 },
	{ 0xBC, 0xBB },
	{ 0x8B, 0x4F },
	{ 0xB6, 0xB0 },
	{ 0x13, 0x3E, 0xC2, 0xAA, 0xE6, 0xDF }
};

static const CMbimPort::Guid type_internet =
{
	{ 0x7E, 0x5E, 0x2A, 0x7E },
	{ 0x4E, 0x6F },
	{ 0x72, 0x72 },
	{ 0x73, 0x6B },
	{ 0x65, 0x6E, 0x7E, 0x5E, 0x2A, 0x7E }
};

// Constructor

CMbimPort::CMbimPort(string const &dev) : m_dev(dev)
{
	m_fd = 0;
}

// Destructor

CMbimPort::~CMbimPort(void)
{
	ClosePort();
}

// Attributes

bool CMbimPort::IsOpen(void) const
{
	return m_fd > 0;
}

bool CMbimPort::IsPresent(void) const
{
	struct stat s;

	return stat(m_dev.c_str(), &s) == 0;
}

// Operations

bool CMbimPort::OpenPort(void)
{
	if( !m_fd ) {

		auto fd = open(m_dev.c_str(), O_RDWR);

		if( fd > 0 ) {

			WORD max;

			if( !ioctl(fd, IOCTL_WDM_MAX_COMMAND, &max) ) {

				m_send.resize(max);

				m_recv.resize(max);

				m_fd  = fd;

				m_msg = 0;

				return true;
			}
		}
	}

	return false;
}

bool CMbimPort::ClosePort(void)
{
	if( m_fd ) {

		close(m_fd);

		m_fd = 0;

		return true;
	}

	return false;
}

bool CMbimPort::Open(void)
{
	AfxTrace("mbim open\n");

	NewMessage(cmdOpen);

	Open_Msg *m = (Open_Msg *) m_send.data();

	m->m_len = sizeof(Open_Msg);

	m->m_max = m_send.size();

	if( Transact() > 0 ) {

		OpenDone *r = (OpenDone *) m_recv.data();

		if( r->m_stat == 0 ) {

			return true;
		}
	}

	return false;
}

bool CMbimPort::Close(void)
{
	AfxTrace("mbim close\n");

	NewMessage(cmdClose);

	if( Transact() > 0 ) {

		OpenDone *r = (OpenDone *) m_recv.data();

		if( r->m_stat == 0 ) {

			return true;
		}
	}

	return false;
}

bool CMbimPort::QuerySubscriberReady(bool &f)
{
	AfxTrace("mbim query subscriber ready\n");

	NewBasicConnect(cidSubscriber, propGet);

	if( Transact() > 0 ) {

		SubscriberInfo *r = (SubscriberInfo *) m_recv.data();

		if( r->m_ready == 1 ) {

			f = true;
		}
		else {
			f = false;
		}

		return true;
	}

	return false;
}

bool CMbimPort::QueryRegistration(bool &s)
{
	AfxTrace("mbim query registration\n");

	NewBasicConnect(cidRegister, propGet);

	if( Transact() > 0 ) {

		RegistrationInfo *r = (RegistrationInfo *) m_recv.data();

		if( r->m_reg == 2 || r->m_reg == 3 ) {

			s = true;
		}
		else {
			s = false;
		}
		return true;
	}

	return false;
}

bool CMbimPort::SetPacketService(bool attach)
{
	AfxTrace("mbim set packet service %d\n", attach);

	NewBasicConnect(cidPacket, propSet, sizeof(PacketServiceMsg) - sizeof(CommandMsg));

	PacketServiceMsg *m = (PacketServiceMsg *) m_send.data();

	m->m_action = attach ? 0 : 1;

	if( Transact() > 0 ) {

		PacketServiceInfo *r = (PacketServiceInfo *) m_recv.data();

		if( attach ) {

			return r->m_state == 2;
		}
		else {
			return r->m_state == 3 || r->m_state == 4;
		}

		return true;
	}

	return false;
}

bool CMbimPort::QueryPacketService(bool &s)
{
	AfxTrace("mbim query packet service\n");

	NewBasicConnect(cidPacket, propGet);

	if( Transact() > 0 ) {

		PacketServiceInfo *r = (PacketServiceInfo *) m_recv.data();

		if( r->m_state == 2 ) {

			s = true;
		}
		else {
			s = false;
		}

		return true;
	}

	return false;
}

bool CMbimPort::QueryIpConfig(string &addr, string &gate, string &mask, string &dns)
{
	AfxTrace("mbim query ip config\n");

	NewBasicConnect(cidIpCconfig, propGet, sizeof(IPConfigInfo) - sizeof(CommandMsg));

	IPConfigInfo *m = (IPConfigInfo *) m_send.data();

	m->m_session = 0;

	if( Transact() > 0 ) {

		IPConfigInfo *r = (IPConfigInfo *) m_recv.data();

		if( r->m_session == m->m_session ) {

			if( r->m_ip4config & (1 << 0) ) {

				PBYTE p = PBYTE(&r->m_session) + r->m_ip4off;

				DWORD n = (DWORD &) p[0];

				DWORD m = ((1 << n) -1) << (32-n);

				addr = CPrintf("%d.%d.%d.%d", p[4], p[5], p[6], p[7]);

				mask = CPrintf("%d.%d.%d.%d", BYTE(m >> 24), BYTE(m >> 16), BYTE(m >> 8), BYTE(m));
			}

			if( r->m_ip4config & (1 << 1) ) {

				PBYTE p = PBYTE(&r->m_session) + r->m_ip4gateoff;

				gate = CPrintf("%d.%d.%d.%d", p[0], p[1], p[2], p[3]);
			}

			if( r->m_ip4config & (1 << 2) ) {

				PBYTE p = PBYTE(&r->m_session) + r->m_ip4dsnoff;

				dns = CPrintf("%d.%d.%d.%d", p[0], p[1], p[2], p[3]);

				if( r->m_ip4dnscount > 1 ) {

					dns += " ";

					dns += CPrintf("%d.%d.%d.%d", p[4], p[5], p[6], p[7]);
				}
			}

			return true;
		}
	}

	return false;
}

bool CMbimPort::QueryConnect(bool &state)
{
	AfxTrace("mbim query connect\n");

	NewBasicConnect(cidConnect, propGet, sizeof(ConnectInfo) - sizeof(CommandMsg));

	ConnectInfo *m = (ConnectInfo *) m_send.data();

	m->m_session = 0;

	if( Transact() > 0 ) {

		ConnectInfo *r = (ConnectInfo *) m_recv.data();

		if( r->m_stat ==  0 ) {

			if( r->m_state == 1 ) {

				state = true;
			}
			else {
				state = false;
			}

			return true;
		}
	}

	return false;
}

bool CMbimPort::Connect(void)
{
	AfxTrace("mbim connect default\n");

	NewBasicConnect(cidConnect, propSet, sizeof(ConnectMsg) - sizeof(CommandMsg));

	ConnectMsg *m = (ConnectMsg *) m_send.data();

	m->m_session  = 0;

	m->m_activate = 1;

	m->m_iptype   = 1;

	memcpy(&m->m_serv, &basic_connect, sizeof(Guid));

	memcpy(&m->m_context, &type_internet, sizeof(Guid));

	if( Transact() > 0 ) {

		ConnectInfo *r = (ConnectInfo *) m_recv.data();

		if( r->m_stat ==  0 ) {

			if( r->m_state == 1 ) {

				return true;
			}
		}
	}

	return false;
}

bool CMbimPort::Connect(string apn)
{
	AfxTrace("mbim connect %s\n", apn.c_str());

	NewBasicConnect(cidConnect, propSet, sizeof(ConnectMsg) - sizeof(CommandMsg) + apn.length() * sizeof(WORD));

	ConnectMsg *m = (ConnectMsg *) m_send.data();

	m->m_session  = 0;

	m->m_activate = 1;

	m->m_iptype   = 1;

	memcpy(&m->m_serv, &basic_connect, sizeof(Guid));

	memcpy(&m->m_context, &type_internet, sizeof(Guid));

	m->m_accessoff = sizeof(ConnectMsg) - sizeof(CommandMsg);

	m->m_accesslen = apn.length() * sizeof(WORD);

	WORD *pdata = (WORD *) m->m_data;

	for( UINT n = 0; n < apn.length(); *pdata++ = apn[n++] );

	if( Transact() > 0 ) {

		ConnectInfo *r = (ConnectInfo *) m_recv.data();

		if( r->m_stat ==  0 ) {

			if( r->m_state == 1 ) {

				return true;
			}
		}
	}

	return false;
}


bool CMbimPort::Connect(string apn, string user, string pass, int auth)
{
	AfxTrace("mbim connect %s user %s auth %u\n", apn.c_str(), user.c_str(), auth);

	NewBasicConnect(cidConnect, propSet, sizeof(ConnectMsg) - sizeof(CommandMsg));

	ConnectMsg *m = (ConnectMsg *) m_send.data();

	m->m_session  = 0;

	m->m_activate = 1;

	m->m_auth     = auth;

	m->m_iptype   = 1;

	memcpy(&m->m_serv, &basic_connect, sizeof(Guid));

	memcpy(&m->m_context, &type_internet, sizeof(Guid));

	auto offset = sizeof(ConnectMsg) - sizeof(CommandMsg);

	int  count  = 0;

	WORD *pdata = (WORD *) m->m_data;

	if( !apn.empty() ) {

		m->m_accessoff	= offset + count;

		m->m_accesslen	= apn.length() * sizeof(WORD);

		for( UINT n = 0; n < apn.length(); pdata[count++] = apn[n++] );

		while( count & 1 ) pdata[count++] = 0;
	}

	if( !user.empty() ) {

		m->m_useroff = offset + count * sizeof(WORD);

		m->m_userlen = user.length() * sizeof(WORD);

		for( UINT n = 0; n < user.length(); pdata[count++] = user[n++] );

		while( count & 1 ) pdata[count++] = 0;
	}

	if( !pass.empty() ) {

		m->m_passoff = offset + count * sizeof(WORD);

		m->m_passlen = pass.length() * sizeof(WORD);

		for( UINT n = 0; n < pass.length(); pdata[count++] = pass[n++] );
	}

	m->m_len  += count * sizeof(WORD);

	m->m_info += count * sizeof(WORD);

	if( Transact() > 0 ) {

		ConnectInfo *r = (ConnectInfo *) m_recv.data();

		if( r->m_stat ==  0 ) {

			if( r->m_state == 1 ) {

				return true;
			}
		}
	}

	return false;
}

bool CMbimPort::Disconnect(void)
{
	AfxTrace("mbim disconnect\n");

	NewBasicConnect(cidConnect, propSet, sizeof(ConnectMsg) - sizeof(CommandMsg));

	ConnectMsg *m = (ConnectMsg *) m_send.data();

	m->m_session  = 0;

	m->m_activate = 0;

	memcpy(&m->m_serv, &basic_connect, sizeof(Guid));

	memcpy(&m->m_context, &type_internet, sizeof(Guid));

	if( Transact() > 0 ) {

		ConnectInfo *r = (ConnectInfo *) m_recv.data();

		if( r->m_stat ==  0 ) {

			if( r->m_state == 3 || r->m_state == 4 ) {

				return true;
			}
		}
	}

	return false;
}

// Implementation

ssize_t CMbimPort::NewMessage(int type)
{
	Header *m = (Header *) m_send.data();

	m->m_type  = type;

	m->m_len   = sizeof(Header);

	m->m_trans = ++m_msg;

	return sizeof(Header);
}

ssize_t CMbimPort::NewBasicConnect(int cid, int cmd, int info)
{
	CommandMsg *m = (CommandMsg *) m_send.data();

	memset(m, 0x00, sizeof(CommandMsg) + info);

	m->m_type  = cmdCommand;

	m->m_len   = sizeof(CommandMsg) + info;

	m->m_trans = ++m_msg;

	m->m_total = 1;

	m->m_cid   = cid;

	m->m_cmd   = cmd;

	m->m_info  = info;

	memcpy(&m->m_serv, &basic_connect, sizeof(Guid));

	return sizeof(CommandMsg);
}

ssize_t CMbimPort::Transact(void)
{
	Header *hs = (Header *) m_send.data();

	Dump("Send", m_send, hs->m_len);

	if( Send(m_send, hs->m_len) == hs->m_len ) {

		for( ;;) {

			Header *hr = (Header *) m_recv.data();

			if( Recv(m_recv, 10000) > 0 ) {

				Dump("Recv", m_recv, hr->m_len);

				if( hs->m_trans == hr->m_trans ) {

					if( hr->m_type == (hs->m_type | 0x80000000) ) {

						if( hs->m_type == cmdCommand ) {

							CommandMsg  *m = (CommandMsg *) m_send.data();

							CommandDone *d = (CommandDone *) m_recv.data();

							if( m->m_cid != d->m_cid ) {

								AfxTrace("mbimm command error\n");

								return -1;
							}

							if( memcmp(&m->m_serv, &d->m_serv, sizeof(Guid)) ) {

								AfxTrace("mbimm command service error\n");

								return -1;
							}

							if( d->m_stat != 0 ) {

								AfxTrace("mbimm command failed %d\n", d->m_stat);

								return -1;
							}
						}

						return hr->m_len;
					}
				}
			}
			else {
				AfxTrace("mbim command imeout\n");

				return -1;
			}
		}
	}

	return -1;
}

ssize_t CMbimPort::Send(bytes &b, ssize_t n)
{
	if( m_fd ) {

		return write(m_fd, b.data(), n);
	}

	return -1;
}

ssize_t CMbimPort::Recv(bytes &b, int ms)
{
	if( m_fd ) {

		fd_set set;

		FD_ZERO(&set);

		FD_SET(m_fd, &set);

		timeval tv;

		tv.tv_sec  = (ms / 1000);

		tv.tv_usec = (ms % 1000) * 1000;

		int s = select(m_fd+1, &set, NULL, NULL, &tv);

		if( s >= 0 ) {

			memset(b.data(), 0x00, b.size());

			if( FD_ISSET(m_fd, &set) ) {

				return read(m_fd, b.data(), b.size());
			}

			return 0;
		}
	}

	return -1;
}

// Diag

void CMbimPort::Dump(string s, bytes &b, int n)
{
	#if 0

	AfxTrace("%s(%d)", s.c_str(), n);

	for( UINT i = 0; i < n; i++ ) {

		if( !(i % 16) ) {

			AfxTrace("\n%8.8X ", i);
		}

		AfxTrace("%2.2X ", b[i]);
	}

	AfxTrace("\n");

	#endif
}


// End of File
