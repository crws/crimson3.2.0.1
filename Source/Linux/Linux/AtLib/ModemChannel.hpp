
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SerialPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modem Command Channel
//

class CModemChannel
{
public:
	// Constructor
	CModemChannel(string const &dev);

	// Destructor
	~CModemChannel(void);

	// Attributes
	bool IsPresent(void) const;

	// Operations
	void SetEcho(bool echo);
	bool SetTrace(bool trace);
	int  Command(vector<string> &lines, string const &cmd, int ms = 5000);
	int  Command(string const &cmd, int ms = 5000);
	int  SendHex(BYTE const *data, size_t size);
	int  SendText(string const &text);

	// Global Trace
	static void SetGlobalTrace(bool trace);

	// Responses
	enum
	{
		codeOkay       = 0,
		codeConnect    = 1,
		codeRing       = 2,
		codeError      = 4,
		codeNoDialtone = 6,
		codeBusy       = 7,
		codeNoAnswer   = 8,
		codePrompt     = 9,
		codeFailed     = 100,
		codeTimeout    = 101
	};

protected:
	// Static Data
	static bool m_global;

	// Data Members
	CSerialPort      m_port;
	map<string, int> m_map;
	bool		 m_trace;
	bool		 m_echo;

	// Implementation
	int  Exec(vector<string> *lines, string const &send, bool echo, int ms);
	int  Decode(string const &s);
	void LoadMap(void);
};

// End of File
