
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Serial Port
//

class CSerialPort
{
public:
	// Constructor
	CSerialPort(string const &dev);

	// Destructor
	~CSerialPort(void);

	// Attributes
	bool IsOpen(void) const;
	bool IsPresent(void) const;

	// Operations
	bool    Open(void);
	bool    Close(void);
	bool    Flush(void);
	bool    Write(void const *p, size_t n);
	ssize_t Read(void *p, size_t n, int ms);
	bool    Write(string const &s);
	bool    Write(bytes const &b);
	bool    Read(bytes &b, int ms);

protected:
	// Data Members
	string m_dev;
	int    m_fd;
};

// End of File
