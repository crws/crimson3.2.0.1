
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// MBIM Port
//

class CMbimPort
{
public:
	// Constructor
	CMbimPort(string const &dev);

	// Destructor
	~CMbimPort(void);

	// Attributes
	bool IsOpen(void) const;
	bool IsPresent(void) const;

	// Operations
	bool OpenPort(void);
	bool ClosePort(void);
	bool Open(void);
	bool Close(void);
	bool QuerySubscriberReady(bool &f);
	bool QueryRegistration(bool &s);
	bool SetPacketService(bool attach);
	bool QueryPacketService(bool &s);
	bool QueryIpConfig(string &ip, string &gate, string &mask, string &dns);
	bool QueryConnect(bool &state);
	bool Connect(void);
	bool Connect(string apn);
	bool Connect(string apn, string usr, string pwd, int auth);
	bool Disconnect(void);

	// Types
public:
	struct Guid
	{
		BYTE a[4];
		BYTE b[2];
		BYTE c[2];
		BYTE d[2];
		BYTE e[6];
	};

protected:
	// Commands
	enum
	{
		cmdOpen			= 1,
		cmdClose		= 2,
		cmdCommand		= 3,
		cmdHostError		= 4,
	};

	// Query
	enum
	{
		propGet			= 0,
		propSet			= 1,
	};

	enum
	{
		cidDeviceCaps		= 1,
		cidSubscriber		= 2,
		cidRadio		= 3,
		cidPin			= 4,
		cidPinList		= 5,
		cidHome			= 6,
		cidPreferred		= 7,
		cidVisible		= 8,
		cidRegister		= 9,
		cidPacket		= 10,
		cidSignal		= 11,
		cidConnect		= 12,
		cidProvisioned		= 13,
		cidService		= 14,
		cidIpCconfig		= 15,
		cidDeviceServices	= 16,
		cidSubscribeList	= 19,
		cidStatistics		= 20,
		cidNetworkIdle		= 21,
		cidEmergency		= 22,
		cidPacketFilters	= 23,
		cidMulticarrier		= 24,
	};

	// Headers

	struct Header
	{
		DWORD		m_type;
		DWORD		m_len;
		DWORD		m_trans;
	};

	struct Fragmented : Header
	{
		DWORD		m_total;
		DWORD		m_current;
	};

	// Messages

	struct Open_Msg : Header
	{
		DWORD		m_max;
	};

	struct CloseMsg : Header
	{
	};

	struct OpenDone : Header
	{
		DWORD		m_stat;
	};

	struct CommandMsg : Fragmented 
	{
		Guid		m_serv;
		DWORD		m_cid;
		DWORD		m_cmd;
		DWORD		m_info;
	};

	struct CommandDone : Fragmented
	{
		Guid		m_serv;
		DWORD		m_cid;
		DWORD		m_stat;
		DWORD		m_info;
	};

	struct PacketServiceMsg : CommandMsg
	{
		DWORD		m_action;
	};

	struct PacketServiceInfo : CommandDone
	{
		DWORD		m_error;
		DWORD		m_state;
		DWORD		m_available;
		__uint64_t	m_uplink;
		__uint64_t	m_dnlink;
	};

	struct ConnectMsg : CommandMsg
	{
		DWORD		m_session;
		DWORD		m_activate;
		DWORD		m_accessoff;
		DWORD		m_accesslen;
		DWORD		m_useroff;
		DWORD		m_userlen;
		DWORD		m_passoff;
		DWORD		m_passlen;
		DWORD		m_comp;
		DWORD		m_auth;
		DWORD		m_iptype;
		Guid		m_context;
		BYTE		m_data[];
	};

	struct ConnectInfo : CommandDone
	{
		DWORD		m_session;
		DWORD		m_state;
		DWORD		m_voicestate;
		DWORD		m_iptype;
		Guid		m_context;
		DWORD 		m_neterror;
	};

	struct SubscriberInfo : CommandDone
	{
		DWORD		m_ready;
		DWORD		m_subscriberoff;
		DWORD		m_subscriberlen;
		DWORD		m_simoff;
		DWORD		m_simlen;
		DWORD		m_info;
		DWORD		m_numbers;
		DWORD		m_list[];
	};

	struct RegistrationInfo : CommandDone
	{
		DWORD		m_error;
		DWORD		m_reg;
		DWORD		m_mode;
		DWORD		m_data;
		DWORD		m_class;
		DWORD		m_idoff;
		DWORD		m_idlen;
		DWORD		m_nameoff;
		DWORD		m_namelen;
		DWORD		m_roamoff;
		DWORD		m_roamlen;
		DWORD		m_flags;
		BYTE		m_buff[];
	};

	struct IPConfigInfo : CommandDone
	{
		DWORD		m_session;
		DWORD		m_ip4config;
		DWORD		m_ip6config;
		DWORD		m_ip4count;
		DWORD		m_ip4off;
		DWORD		m_ip6count;
		DWORD		m_ip6off;
		DWORD		m_ip4gateoff;
		DWORD		m_ip6gateoff;
		DWORD		m_ip4dnscount;
		DWORD		m_ip4dsnoff;
		DWORD		m_ip6dnscount;
		DWORD		m_ip6dsnoff;
		DWORD		m_ip4mtu;
		DWORD		m_ip6mtu;
	};

	struct PacketStatsInfo : CommandDone
	{
		DWORD		m_rxdiscards;
		DWORD		m_rxerrors;
		__uint64_t	m_rxbytes;
		__uint64_t	m_rxpackets;
		__uint64_t	m_txbytes;
		__uint64_t	m_txpackets;
		DWORD		m_txerrors;
		DWORD		m_txdiscards;
	};

	// Data
	string	m_dev;
	int	m_fd;
	bytes	m_send;
	bytes   m_recv;
	int	m_msg;

	// Implementation
	ssize_t NewMessage(int type);
	ssize_t NewBasicConnect(int cid, int cmd, int info = 0);
	ssize_t Transact(void);
	ssize_t Send(bytes &b, ssize_t);
	ssize_t Recv(bytes &f, int ms);

	// Diag
	void Dump(string s, bytes &b, int n);
};

// End of File
