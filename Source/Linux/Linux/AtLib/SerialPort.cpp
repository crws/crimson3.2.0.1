
#include "Intern.hpp"

#include "SerialPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Serial Port
//

// Constructor

CSerialPort::CSerialPort(string const &dev) : m_dev(dev)
{
	m_fd = 0;
}

// Destructor

CSerialPort::~CSerialPort(void)
{
	Close();
}

// Attributes

bool CSerialPort::IsOpen(void) const
{
	return m_fd > 0;
}

bool CSerialPort::IsPresent(void) const
{
	struct stat s;

	return stat(m_dev.c_str(), &s) == 0;
}

// Operations

bool CSerialPort::Open(void)
{
	if( !m_fd ) {

		auto fd = open(m_dev.c_str(), O_RDWR | O_NOCTTY);

		if( fd > 0 ) {

			struct termios cfg = { 0 };

			ioctl(fd, TCGETS, &cfg);

			cfg.c_iflag  = IGNBRK;
			cfg.c_oflag  = 0;
			cfg.c_cflag  = CREAD | CLOCAL | B115200;
			cfg.c_lflag  = 0;
			cfg.c_cflag &= ~CSIZE;
			cfg.c_cflag |=  CS8;
			cfg.c_cflag &= ~CSTOPB;
			cfg.c_cflag &= ~PARENB;
			cfg.c_ispeed = B115200;
			cfg.c_ospeed = B115200;

			memset(cfg.c_cc, 0, sizeof(cfg.c_cc));

			ioctl(fd, TCSETS, &cfg);

			m_fd = fd;

			return true;
		}
	}

	return false;
}

bool CSerialPort::Close(void)
{
	if( m_fd ) {

		close(m_fd);

		m_fd = 0;

		return true;
	}

	return false;
}

bool CSerialPort::Write(void const *p, size_t n)
{
	if( m_fd ) {

		return write(m_fd, p, n) == n;
	}

	return false;
}

bool CSerialPort::Flush(void)
{
	BYTE c[128];

	while( read(m_fd, c, sizeof(c)) > 0 );

	return true;
}

ssize_t CSerialPort::Read(void *p, size_t n, int ms)
{
	if( m_fd ) {

		fd_set set;

		FD_ZERO(&set);

		FD_SET(m_fd, &set);

		timeval tv;

		tv.tv_sec  = (ms / 1000);

		tv.tv_usec = (ms % 1000) * 1000;

		int s = select(m_fd+1, &set, NULL, NULL, &tv);

		if( s >= 0 ) {

			if( FD_ISSET(m_fd, &set) ) {

				return read(m_fd, p, n);
			}

			return 0;
		}
	}

	return -1;
}

bool CSerialPort::Write(string const &s)
{
	return Write(s.data(), s.size());
}

bool CSerialPort::Write(bytes const &b)
{
	return Write(b.data(), b.size());
}

bool CSerialPort::Read(bytes &b, int ms)
{
	if( m_fd ) {

		for( ;;) {

			size_t n = 1024;

			size_t s = b.size();

			b.resize(s + n);

			PBYTE   p = b.data() + s;

			ssize_t r = Read(p, n, ms);

			if( r >= 0 ) {

				b.resize(s + r);

				if( r == n ) {

					ms = 0;

					continue;
				}

				return true;
			}

			return false;
		}
	}
}

// End of File
