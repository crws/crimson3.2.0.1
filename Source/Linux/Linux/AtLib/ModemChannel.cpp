
#include "Intern.hpp"

#include "ModemChannel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Modem Command Channel
//

// Static Data

bool CModemChannel::m_global = false;

// Constructor

CModemChannel::CModemChannel(string const &dev) : m_port(dev)
{
	LoadMap();

	m_echo  = true;

	m_trace = true;
}

// Destructor

CModemChannel::~CModemChannel(void)
{
}

// Attributes

bool CModemChannel::IsPresent(void) const
{
	return m_port.IsPresent();
}

// Operations

void CModemChannel::SetEcho(bool echo)
{
	m_echo = echo;
}

bool CModemChannel::SetTrace(bool trace)
{
	bool old = m_trace;

	m_trace  = trace;

	return old;
}

int CModemChannel::Command(vector<string> &lines, string const &cmd, int ms)
{
	lines.clear();

	return Exec(&lines, "AT" + cmd, !m_echo, ms);
}

int CModemChannel::Command(string const &cmd, int ms)
{
	return Exec(nullptr, "AT" + cmd, !m_echo, ms);
}

int CModemChannel::SendHex(BYTE const *data, size_t size)
{
	string save;

	char const *hex = "0123456789ABCDEF";

	for( size_t n = 0; n < size; n++ ) {

		save += hex[data[n]/16];

		save += hex[data[n]%16];
	}

	save += 0x1A;

	return Exec(nullptr, save, !m_echo, 5000);
}

int CModemChannel::SendText(string const &text)
{
	return Exec(nullptr, text + '\x1A', !m_echo, 5000);
}

// Global Trace

void CModemChannel::SetGlobalTrace(bool trace)
{
	m_global = trace;
}

// Implementation

int CModemChannel::Exec(vector<string> *lines, string const &send, bool echo, int ms)
{
	if( m_port.IsOpen() || m_port.Open() ) {

		AfxTestPipe();

		m_port.Flush();

		if( m_global || m_trace ) {

			if( !send.empty() && !isprint(send[send.size()-1]) ) {

				AfxTrace("send %s\n", send.substr(0, send.size()-1).c_str());
			}
			else {
				AfxTrace("send %s\n", send.c_str());
			}
		}

		if( m_port.Write(send + '\r') ) {

			string line;

			for( ;;) {

				bytes b;

				if( m_port.Read(b, ms) ) {

					if( b.size() ) {

						for( auto c : b ) {

							if( line.empty() ) {

								if( isprint(c) ) {

									line += char(c);
								}
							}
							else {
								if( c == ' ' ) {

									if( line.size() == 1 && line[0] == '>' ) {

										if( m_global || m_trace ) {

											AfxTrace("recv prompt\n");
										}

										return codePrompt;
									}
								}

								if( c == '\r' ) {

									if( line == send ) {

										echo = true;
									}
									else {
										if( m_global || m_trace ) {

											AfxTrace("recv %s\n", line.c_str());
										}

										if( echo ) {

											string err("+CME ERROR: ");

											if( line.substr(0, err.size()) == err ) {

												if( lines ) {

													lines->insert(lines->begin(), line);
												}

												return codeError;
											}

											if( isalpha(line[0]) ) {

												int code = Decode(line);

												if( code >= 0 ) {

													if( lines ) {

														lines->insert(lines->begin(), line);
													}

													return code;
												}
											}

											if( lines ) {

												lines->push_back(line);
											}
										}
									}

									line.clear();

									continue;
								}

								line += char(c);
							}
						}

						continue;
					}

					m_port.Close();

					return codeTimeout;
				}
			}
		}
	}

	m_port.Close();

	return codeFailed;
}

int CModemChannel::Decode(string const &s)
{
	auto i = m_map.find(s);

	if( i == m_map.end() ) {

		string f = "CONNECT ";

		if( s.substr(0, f.size()) == f ) {

			return codeConnect;
		}

		return -1;
	}

	return i->second;
}

void CModemChannel::LoadMap(void)
{
	m_map.insert(make_pair("OK", codeOkay));
	m_map.insert(make_pair("CONNECT", codeConnect));
	m_map.insert(make_pair("RING", codeRing));
	m_map.insert(make_pair("ERROR", codeError));
	m_map.insert(make_pair("NO DIALTONE", codeNoDialtone));
	m_map.insert(make_pair("BUSY", codeBusy));
	m_map.insert(make_pair("NO ANSWER", codeNoAnswer));
}

// End of File
