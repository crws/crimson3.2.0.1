
#pragma  once

#include "Intern.hpp"

#include "Printf.hpp"

extern void AfxTrace(char const *p, ...);

extern void AfxVTrace(char const *p, va_list v);

extern void AfxTestPipe(void);
