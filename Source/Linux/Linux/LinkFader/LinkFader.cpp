
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

// Static Data

static	string			m_name  = "LinkFader";

static	bool			m_run	= false;

static	bool			m_stop  = false;

static	bool			m_fore  = false;

static	bool			m_debug = false;

static	bool			m_fast  = false;

static	int			m_fd    = -1;

static	string			m_lock;

static	string			m_face;

static	string			m_watch;

static	string			m_conf;

static	string			m_cmd;

static	map<string, string>	m_config;

// Prototypes

global	int	main(int nArg, char *pArg[]);
global	void	AfxTrace(char const *p, ...);
global	void	AfxVTrace(char const *p, va_list v);
global	bool	IsUnderDebug(void);
static	void	CheckParent(void);
static	void	OnStopSignal(int sig);
static	void	OnPipeSignal(int sig);
static	bool	GetProcStatusEntry(string &data, string const &proc, string const &name);
static	bool	ParseCommandLine(int nArg, char *pArg[]);
static	void	Error(char const *p, ...);
static	void	ShowUsage(void);
static	bool	LoadConfig(void);
static	string	GetConfig(char const *name, char const *def);
static	UINT	GetConfig(char const *name, UINT uDef, UINT uMin, UINT uMax);
static	void	MainLoop(void);
static	bool	TestLockFile(void);
static	void	MakeLockFile(void);
static	void	KillLockFile(void);
static	bool	TestLink(bool link);
static	bool	OpenNetLink(void);
static	bool	CloseNetLink(void);
static	bool	SendGetRoutes(void);
static	ssize_t	RecvMessage(msghdr &msg, int flags);
static	bool	RecvMessage(bytes &data);
static	UINT	GetUnsigned(map<WORD, bytes> &attr, WORD type, UINT none);
static	string	GetInterfaceName(map<WORD, bytes> &attr, WORD type, string const &none);
static	string	GetAddress(map<WORD, bytes> &attr, WORD type);
static	string	MakeRoute(string const &dest, int bits, string const &gate, UINT metric, string const &name);
static	bool	FadeRoutes(bool out);
static	bool	SendFailed(void);
static	INT64	GetTraffic(bool send);

// Code

global int main(int nArg, char *pArg[])
{
	CheckParent();

	if( m_debug ) {

		signal(SIGUSR1, OnStopSignal);

		signal(SIGPIPE, OnPipeSignal);
	}
	else {
		signal(SIGTERM, OnStopSignal);

		signal(SIGPIPE, OnPipeSignal);
	}

	if( ParseCommandLine(nArg, pArg) ) {

		if( !m_debug && !m_fore ) {

			AfxTrace("starting up and forking\n");

			if( fork() ) {

				return 0;
			}
		}

		if( TestLockFile() ) {

			MakeLockFile();

			LoadConfig();

			MainLoop();

			KillLockFile();

			AfxTrace("exiting\n");

			return 0;
		}

		Error("already running");
	}

	ShowUsage();

	return 2;
}

global void AfxTrace(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	AfxVTrace(p, v);

	va_end(v);
}

global void AfxVTrace(char const *p, va_list v)
{
	if( !m_debug ) {

		vsyslog(LOG_NOTICE, p, v);
	}

	vprintf(p, v);
}

global bool IsUnderDebug(void)
{
	return m_debug;
}

static void CheckParent(void)
{
	string ppid;

	if( GetProcStatusEntry(ppid, "self", "PPid") ) {

		string name;

		if( GetProcStatusEntry(name, ppid, "Name") ) {

			if( name == "gdbserver" || name == "gdb" ) {

				m_debug = true;
			}

			if( name == "InitC32" ) {

				m_fore = true;
			}
		}
	}
}

static void OnStopSignal(int sig)
{
	m_stop = true;
}

static void OnPipeSignal(int sig)
{
	signal(SIGPIPE, OnPipeSignal);
}

static bool GetProcStatusEntry(string &data, string const &proc, string const &name)
{
	ifstream stm(CPrintf("/proc/%s/status", proc.c_str()));

	if( stm.good() ) {

		while( !stm.eof() ) {

			string line;

			getline(stm, line);

			size_t n = line.find(':');

			if( n != string::npos ) {

				if( line.substr(0, n) == name ) {

					int d = line.find_first_not_of(" \t", n + 1);

					if( d != string::npos ) {

						int e = line.find_last_not_of(" \t\r\n");

						data = line.substr(d, e - d + 1);

						return true;
					}
				}
			}
		}
	}

	return false;
}

static bool ParseCommandLine(int nArg, char *pArg[])
{
	int c;

	while( (c = getopt(nArg, pArg, "fi:c:p:")) != -1 ) {

		switch( c ) {

			case 'f':

				m_fore = true;

				break;

			case 'i':

				if( !optarg ) {

					Error("missing interface name");
				}

				m_face = strdup(optarg);

				break;

			case 'c':

				if( !optarg ) {

					Error("missing config file");
				}

				m_conf = strdup(optarg);

				break;

			case 'd':

				if( !optarg ) {

					Error("missing command file");
				}

				m_cmd = strdup(optarg);

				break;

			case 'p':

				if( !optarg ) {

					Error("missing lock file");
				}

				m_lock = strdup(optarg);

				break;

			case '?':

				Error("syntax error");

				break;

			default:

				Error("unexpected switch %c", c);

				break;
		}
	}

	if( m_face.empty() ) {

		Error("must specify interface");
	}

	if( m_conf.empty() ) {

		m_conf = "/vap/opt/crimson/config/net/" + m_face + "-fade-conf";
	}

	if( m_cmd.empty() ) {

		m_cmd = "/tmp/crimson/face/" + m_face + "/command";
	}

	if( m_lock.empty() ) {

		m_lock = "/var/run/c3-" + m_face + "-fade.pid";
	}

	return true;
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	if( m_run ) {

		KillLockFile();
	}

	if( !m_debug ) {

		vsyslog(LOG_ERR, p, v);
	}

	fprintf(stderr, "%s: ", m_name.c_str());

	vfprintf(stderr, p, v);

	fprintf(stderr, "\n");

	va_end(v);

	exit(1);
}

static void ShowUsage(void)
{
	fprintf(stderr, "usage: %s -i <interface> [-c <config-file>] [-p <pid-file>]\n", m_name.c_str());
}

static bool LoadConfig(void)
{
	ifstream stm(m_conf);

	if( stm.good() ) {

		while( !stm.eof() ) {

			string line;

			getline(stm, line);

			if( !line.empty() ) {

				if( line[0] != '#' ) {

					size_t n = line.find('=');

					if( n != string::npos ) {

						int d = line.find_first_not_of(" \t", n + 1);

						if( d != string::npos ) {

							int e = line.find_last_not_of(" \t\r\n");

							string key = line.substr(0, n);

							string val = line.substr(d, e - d + 1);

							m_config.insert(make_pair(key, val));
						}
					}
				}
			}
		}

		m_watch = GetConfig("watch", m_face.c_str());

		m_fast  = (m_watch.substr(0, 3) == "eth");

		return true;
	}

	return false;
}

static string GetConfig(char const *name, char const *def)
{
	auto i = m_config.find(name);

	if( i != m_config.end() ) {

		return i->second;
	}

	return def;
}

static UINT GetConfig(char const *name, UINT uDef, UINT uMin, UINT uMax)
{
	auto i = m_config.find(name);

	if( i != m_config.end() ) {

		UINT v = atoi(i->second.c_str());

		if( v ) {

			if( v < uMin ) {

				return uDef;
			}

			if( v > uMax ) {

				return uMax;
			}

			return v;
		}
	}

	return uDef;
}

static void MainLoop(void)
{
	if( true ) {

		bool  link   = false;

		bool  init   = true;

		INT64 last  = 0;

		bool  action = GetConfig("action", 0, 0, 1);

		bool  idle   = GetConfig("idle", 0, 0, 1);

		UINT  period = GetConfig("period", m_fast ? 5 : 20, 1, 600);

		while( !m_stop ) {

			bool test = link;

			if( init && !m_fast ) {

				usleep(period * 1000 * 1000 / 4);
			}

			if( idle && link ) {

				INT64 recv = GetTraffic(false);

				if( recv == last ) {

					test = TestLink(link);

					last = GetTraffic(false);
				}
				else {
					last = recv;
				}
			}
			else {
				test = TestLink(link);
			}

			if( init || link != test ) {

				link = test;

				init = false;

				AfxTrace("link status for %s changed to %s\n", m_face.c_str(), link ? "good" : "bad");

				if( !link && action == 1 ) {

					for( ;;) {

						if( SendFailed() ) {

							while( !m_stop ) {

								usleep(100 * 1000);
							}

							return;
						}

						usleep(100 * 1000);
					}
				}

				FadeRoutes(!link);
			}

			usleep(period * 1000 * 1000);
		}

		return;
	}

	Error("invalid interface");
}

static bool TestLockFile(void)
{
	if( !m_fore && !m_lock.empty() ) {

		ifstream file(m_lock);

		if( file.good() ) {

			pid_t pid;

			file >> pid;

			// Kill with signal of zero is a test for the
			// existance of a valid process of that pid...

			int r;

			if( (r = kill(pid, 0)) == 0 ) {

				return false;
			}
		}

		file.close();

		KillLockFile();
	}

	return true;
}

static void MakeLockFile(void)
{
	if( !m_fore ) {

		ofstream(m_lock) << getpid() << '\n';
	}

	m_run = true;
}

static void KillLockFile(void)
{
	if( !m_fore ) {

		unlink(m_lock.c_str());
	}
}

static bool TestLink(bool link)
{
	UINT   mode  = GetConfig("mode", 1, 1, 1);

	string host  = GetConfig("name", "");

	UINT   burst = GetConfig("burst", 4, 1, 20);

	UINT   good  = GetConfig("success", max(1u, (burst * 3 + 2) / 4), 1, 20);

	UINT   bad   = GetConfig("failure", max(1u, (burst * 1 + 2) / 4), 1, 20);

	UINT   delay = GetConfig("delay", m_fast ? 2 : 4, 1, 600);

	UINT   pass  = 0;

	UINT   targ  = (link ? bad : good);

	switch( mode ) {

		case 1:
		{
			if( host.empty() ) {

				host = "8.8.8.8";
			}

			if( delay == 0 ) {

				delay = 1;
			}
		}
		break;
	}

	for( UINT n = 0; n < burst && pass < targ; n++ ) {

		switch( mode ) {

			case 1:
			{
				CPrintf cmd("ping -I %s -W %u -c 1 %s", m_watch.c_str(), delay, host.c_str());

				if( system(cmd) == 0 ) {

					pass++;
				}
			}
			break;
		}
	}

	return pass == targ;
}

static bool OpenNetLink(void)
{
	if( (m_fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) >= 0 ) {

		sockaddr_nl saddr = { 0 };

		saddr.nl_family = AF_NETLINK;

		saddr.nl_pid    = getpid();

		if( bind(m_fd, (sockaddr *) &saddr, sizeof(saddr)) >= 0 ) {

			return true;
		}

		close(m_fd);

		m_fd = -1;
	}

	return false;
}

static bool CloseNetLink(void)
{
	if( m_fd >= 0 ) {

		close(m_fd);

		m_fd = -1;

		return true;
	}

	return false;
}

static bool SendGetRoutes(void)
{
	struct
	{
		nlmsghdr nlh;
		rtmsg	 rtm;

	} req;

	req.nlh.nlmsg_type  = RTM_GETROUTE;
	req.nlh.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
	req.nlh.nlmsg_len   = sizeof(req);
	req.nlh.nlmsg_seq   = time(NULL);
	req.rtm.rtm_family  = AF_INET;

	return send(m_fd, &req, sizeof(req), 0) == sizeof(req);
}

ssize_t RecvMessage(msghdr &msg, int flags)
{
	for( ;;) {

		ssize_t s = recvmsg(m_fd, &msg, flags);

		if( s > 0 ) {

			return s;
		}

		if( s < 0 ) {

			if( errno == EINTR || errno == EAGAIN ) {

				continue;
			}
		}

		return -1;
	}
}

static bool RecvMessage(bytes &data)
{
	sockaddr_nl addr = { 0 };

	iovec       iov  = { 0 };

	msghdr      msg  = { 0 };

	msg.msg_name    = &addr;
	msg.msg_namelen = sizeof(addr);
	msg.msg_iov     = &iov;
	msg.msg_iovlen  = 1;

	ssize_t r;

	if( (r = RecvMessage(msg, MSG_PEEK | MSG_TRUNC)) > 0 ) {

		if( addr.nl_family == AF_NETLINK ) {

			if( addr.nl_pid == 0 ) {

				data.resize(r);

				iov.iov_base = data.data();

				iov.iov_len  = data.size();

				if( RecvMessage(msg, 0) == data.size() ) {

					return true;
				}
			}
		}
	}

	return false;
}

static UINT GetUnsigned(map<WORD, bytes> &attr, WORD type, UINT none)
{
	auto i = attr.find(type);

	if( i == attr.end() ) {

		return none;
	}

	return *((UINT *) i->second.data());
}

static string GetInterfaceName(map<WORD, bytes> &attr, WORD type, string const &none)
{
	UINT f = GetUnsigned(attr, type, 0);

	if( f ) {

		char buff[256];

		if_indextoname(f, buff);

		return buff;
	}

	return none;
}

static string GetAddress(map<WORD, bytes> &attr, WORD type)
{
	UINT a = GetUnsigned(attr, type, 0);

	if( a ) {

		char buff[256];

		inet_ntop(AF_INET, &a, buff, sizeof(buff));

		return buff;
	}

	return "";
}

static string MakeRoute(string const &dest, int bits, string const &gate, UINT metric, string const &name)
{
	string r;

	if( !dest.empty() ) {

		r += dest;

		r += CPrintf("/%u ", bits);
	}
	else {
		r += "default ";
	}

	if( !gate.empty() ) {

		r += "via ";

		r += gate;

		r += " ";
	}

	r += CPrintf("metric %4u ", metric);

	r += "dev ";

	r += name;

	return r;
}

static bool FadeRoutes(bool out)
{
	if( OpenNetLink() && SendGetRoutes() ) {

		bytes data;

		if( RecvMessage(data) ) {

			vector<string> cmds;

			UINT face = if_nametoindex(m_watch.c_str());

			for( UINT p = 0; p < 2; p++ ) {

				nlmsghdr const *h = (nlmsghdr const *) data.data();

				size_t          s = data.size();

				while( NLMSG_OK(h, s) ) {

					if( h->nlmsg_flags & NLM_F_DUMP_INTR ) {

						break;
					}

					if( h->nlmsg_type == NLMSG_ERROR ) {

						break;
					}

					rtmsg const *r = (rtmsg const *) NLMSG_DATA(h);

					if( r->rtm_family == AF_INET ) {

						map<WORD, bytes> attr;

						rtattr const *a = RTM_RTA(r);

						size_t        s = h->nlmsg_len;

						while( RTA_OK(a, s) ) {

							PCBYTE d = PCBYTE(a+1);

							size_t n = a->rta_len - sizeof(*a);

							attr.insert(make_pair(a->rta_type, bytes(d, d + n)));

							a = RTA_NEXT(a, s);
						}

						if( GetUnsigned(attr, RTA_TABLE, 0) == 254 ) {

							if( GetUnsigned(attr, RTA_OIF, 0) == face ) {

								string dest = GetAddress(attr, RTA_DST);

								if( dest.empty() || r->rtm_dst_len < 32 ) {

									string gate = GetAddress(attr, RTA_GATEWAY);

									UINT   met1 = GetUnsigned(attr, RTA_PRIORITY, 0);

									UINT   met2 = (met1 % 2000) + (out ? 2000 : 0);

									if( met1 != met2 ) {

										if( (p == 0) == gate.empty() ) {

											string del = MakeRoute(dest, r->rtm_dst_len, gate, met1, m_watch.c_str());

											string add = MakeRoute(dest, r->rtm_dst_len, gate, met2, m_watch.c_str());

											cmds.push_back("ip route del " + del);

											cmds.push_back("ip route add " + add);
										}
									}
								}
							}
						}
					}

					h = NLMSG_NEXT(h, s);
				}
			}

			for( auto const &cmd : cmds ) {

				AfxTrace("%s\n", cmd.c_str());

				system(cmd.c_str());
			}

			CloseNetLink();

			return true;
		}
	}

	CloseNetLink();

	return false;
}

static bool SendFailed(void)
{
	string   tmp(m_cmd + ".tmp");

	ofstream stm(tmp);

	if( stm.good() ) {

		stm << "failed\n";

		stm.close();

		if( unlink(m_cmd.c_str()) == 0 ) {

			if( rename(tmp.c_str(), m_cmd.c_str()) == 0 ) {

				return true;
			}
		}

		unlink(tmp.c_str());
	}

	return false;
}

static INT64 GetTraffic(bool send)
{
	CPrintf cmd("iptables -t mangle --list %s -v -x -w", send ? "POSTROUTING" : "PREROUTING");

	FILE *file = popen(cmd, "r");

	if( file ) {

		string find = ' ' + m_face + ' ';

		for( ;;) {

			char line[256] = { 0 };

			fgets(line, sizeof(line), file);

			if( line[0] ) {

				if( strstr(line, find.c_str()) ) {

					size_t n1 = strspn(line, " ");

					size_t n2 = strcspn(line + n1, " ") + n1;

					size_t n3 = strspn(line + n2, " ") + n2;

					INT64  tb = strtoll(line + n3, NULL, 10);

					fclose(file);

					return tb;
				}

				continue;
			}

			break;
		}

		fclose(file);
	}

	return 0;
}

// End of File
