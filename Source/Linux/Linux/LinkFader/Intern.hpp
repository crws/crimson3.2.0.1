
#ifndef INCLUDE_INTERN_HPP

#define INCLUDE_INTERN_HPP

#include "../Build/Linux.hpp"

#include "Printf.hpp"

#include <arpa/inet.h>
#include <net/if.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>

#endif
