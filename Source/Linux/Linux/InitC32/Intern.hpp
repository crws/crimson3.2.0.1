
#ifndef INCLUDE_INTERN_HPP

#define INCLUDE_INTERN_HPP

#include "../Build/Linux.hpp"

#include <semaphore.h>

#include <sys/mman.h>

#include <sys/shm.h>

#if defined(_DEBUG)

#include "../../../../Build/Bin/Version/Debug/version.hxx"

#else

#include "../../../../Build/Bin/Version/Release/version.hxx"

#endif

#endif
