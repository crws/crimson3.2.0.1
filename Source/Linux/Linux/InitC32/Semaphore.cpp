
#include "Intern.hpp"

#include "Semaphore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linux Semaphore Class
//
// Copyright (c) 2018 Granby Consulting LLC
//
// Placed in the Public Domain.
//

////////////////////////////////////////////////////////////////////////////////
//	
// Semaphore Class
//

// Constructor

CSemaphore::CSemaphore(void)
{
	sprintf(m_sName, "/sem.%u.%8.8X", getpid(), this);

	m_nMem = sizeof(sem_t);

	m_hMem = shm_open(m_sName, O_RDWR | O_CREAT | O_TRUNC, 0666);

	ftruncate(m_hMem, m_nMem);

	m_pSem = (sem_t *) mmap(0, m_nMem, PROT_READ | PROT_WRITE, MAP_SHARED, m_hMem, 0);

	sem_init(m_pSem, 1, 0);
}

// Destructor

CSemaphore::~CSemaphore(void)
{
	sem_destroy(m_pSem);

	munmap(m_pSem, m_nMem);

	close(m_hMem);

	shm_unlink(m_sName);
}

// Operations

void CSemaphore::Wait(void)
{
	sem_wait(m_pSem);
}

void CSemaphore::Post(void)
{
	sem_post(m_pSem);
}

// End of File
