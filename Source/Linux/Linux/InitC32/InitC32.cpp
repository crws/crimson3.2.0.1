
#include "Intern.hpp"

#include <linux/reboot.h>

#include <sys/reboot.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Initialization
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

// Other Headers

#include "Printf.hpp"

#include "Semaphore.hpp"

// Constants

#define	lockFile	"/var/run/InitC32.pid"

#define resetRequest	"/etc/reset.me"

#define	pipePath	"/tmp/crimson/init"

#define	pipeName	"/tmp/crimson/init/init.pipe"

#define magicHead	'H23C'

#define magicTail	'E23C'

#define	timeGood	45

#define	timeOnce	30

#define	timeExit	5

#define timeRestart	2

// Program States

enum
{
	stateRunning,
	stateStopping,
	stateKilling,
	stateRestart,
};

// Command Codes

enum
{
	cmdStart,
	cmdRun,
	cmdStop,
	cmdDump,
	cmdSignal
};

// Program Data

struct CProgram
{
	CProgram(string const &tag, string const &line, int count, bool once, int pid)
	{
		m_tag   = tag;

		m_line  = line;

		m_count = count;

		m_once  = once;

		m_state = stateRunning;

		m_pid   = pid;

		m_sid   = 0;

		m_inode = GetNode();

		m_delay = time(nullptr);
	}

	bool HasNodeChanged(void)
	{
		return GetNode() != m_inode;
	}

	ino_t GetNode(void)
	{
		size_t f = m_line.find('|');

		string p = (f == string::npos) ? m_line : m_line.substr(0, f);

		struct stat s;

		stat(p.c_str(), &s);

		return s.st_ino;
	}

	string	m_tag;
	string	m_line;
	int	m_state;
	int	m_count;
	bool    m_once;
	pid_t	m_pid;
	pid_t	m_sid;
	ino_t	m_inode;
	time_t	m_delay;
};

// Static Data

static	pthread_mutex_t		m_mutex;

static	CSemaphore *		m_psem  = nullptr;

static	bool			m_stop  = false;

static	bool			m_seen  = false;

static	bool			m_done  = false;

static	int			m_mode  = 0;

static	int			m_child = 0;

static	int			m_pipe;

static	int			m_pair[2];

static	string			m_log;

static	map<pid_t, CProgram>	m_programs;

static	map<string, pid_t>	m_tags;

static	ino_t			m_node;

// Prototypes

global	int	main(int nArg, char const *pArg[]);
static	void	Error(char const *pText, ...);
static	void	AfxTrace(char const *p, ...);
static	bool	MakeLockFile(void);
static	bool	TestLockFile(void);
static	void	KillLockFile(void);
static	void	OnTerm(int sig);
static	void	OnPipe(int sig);
static	void	MainLoop(void);
static	void	CheckCommands(void);
static	void	CheckChildren(void);
static	void	CheckTimeouts(void);
static	void	CheckLogPipe(void);
static	void	CheckShutdown(void);
static	void	CheckReset(void);
static	bool	StopChangedPrograms(void);
static	bool	ReadPipe(int pipe, pid_t &sid, int &code, string &cmd);
static	bool	SendPipe(int pipe, int code, void const *send, size_t size);
static	bool	SendCommand(int code, string const &send);
static	bool	StartProgram(string const &tag, string const &line, int count, bool once);
static	void	ParseLine(vector<char *> &args, string const &line);
static	string	FindPath(string const &program);
static	bool	WaitRead(int fd1, int fd2, int ms);
static	bool	WaitRead(int fd, int ms);
static	bool	SendResponse(pid_t sid);
static	bool	CheckUpdate(CProgram &p);
static	bool	HasCrimsonUpdate(void);
static	bool	HasDistroUpdate(void);
static	bool	RunUpdate(CProgram &p);
static	void *	UpdateThread(void *arg);
static	bool	UpdateCrimson(void);
static	bool	UpdateDistro(void);
static	void	SetLogMode(int mode);
static	string	ReadGeneration(void);
static	void	ProcessDump(CProgram const &p, string const &dump);
static	void *	CompressThread(void *arg);
static	void	LimitUsage(string const &path, string const &match);
static	bool	FindFiles(vector<string> &list, string const &path, string const &match);
static	ino_t	GetOurNode(void);
static	void	KillCrimsonConfig(void);
static	void	KillCrimsonUpdate(void);
static	void	KillSystemConfig(void);

// Code

int main(int nArg, char const *pArg[])
{
	signal(SIGTERM, OnTerm);

	signal(SIGPIPE, OnPipe);

	if( nArg >= 2 ) {

		string cmd(pArg[1]);

		if( nArg == 2 ) {

			if( cmd == "debug" ) {

				MainLoop();

				exit(0);
			}

			if( cmd == "start-service" ) {

				if( TestLockFile() ) {

					CheckReset();

					int pid = fork();

					if( pid >= 0 ) {

						if( pid == 0 ) {

							MakeLockFile();

							MainLoop();

							KillLockFile();
						}

						exit(0);
					}

					Error("failed to fork");
				}

				Error("already running");
			}

			if( cmd == "stop-service" ) {

				ifstream stm(lockFile);

				if( stm.good() ) {

					int pid;

					stm >> pid;

					kill(pid, SIGTERM);

					return 0;
				}

				Error("not running");
			}

			if( cmd == "dump" ) {

				SendCommand(cmdDump, "");

				return 0;
			}

			Error("unknown command");
		}

		if( nArg >= 4 ) {

			if( cmd == "start" || cmd == "run" ) {

				string tag  = pArg[2];

				string line = pArg[3];

				for( int n = 4; n < nArg; n++ ) {

					line += '|';

					line += pArg[n];
				}

				if( cmd == "start" ) {

					SendCommand(cmdStart, tag + '|' + line);
				}
				else {
					SendCommand(cmdRun, tag + '|' + line);
				}

				return 0;
			}
		}

		if( nArg == 3 ) {

			if( cmd == "stop" || cmd == "signal" ) {

				string tag = pArg[2];

				if( cmd == "stop" ) {

					SendCommand(cmdStop, tag);
				}
				else {
					SendCommand(cmdSignal, tag);
				}

				return 0;
			}
		}

		Error("syntax error");
	}
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	SetLogMode(1);

	vsyslog(LOG_ERR, p, v);

	va_end(v);

	exit(1);
}

static void AfxTrace(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	SetLogMode(1);

	vsyslog(LOG_NOTICE, p, v);

	va_end(v);
}

static bool MakeLockFile(void)
{
	ofstream stm(lockFile);

	if( stm.good() ) {

		stm << getpid();

		return true;
	}

	return false;
}

static bool TestLockFile(void)
{
	ifstream stm(lockFile);

	if( stm.good() ) {

		pid_t pid;

		stm >> pid;

		// Kill with signal of zero is a test for the
		// existance of a valid process of that pid...

		int r;

		if( (r = kill(pid, 0)) == 0 ) {

			return false;
		}

		stm.close();

		KillLockFile();
	}

	return true;
}

static void KillLockFile(void)
{
	unlink(lockFile);
}

static void OnTerm(int sig)
{
	m_stop = true;
}

static void OnPipe(int sig)
{
}

static void MainLoop(void)
{
	m_node = GetOurNode();

	mkdir("/tmp/crimson", 0755);

	mkdir("/tmp/crimson/init", 0755);

	m_psem = new CSemaphore;

	pthread_mutex_init(&m_mutex, nullptr);

	mkfifo(pipeName, 0666);

	m_pipe = open(pipeName, O_RDWR | O_NDELAY | O_CLOEXEC);

	pipe(m_pair);

	while( !m_done ) {

		WaitRead(m_pipe, m_pair[0], 250);

		CheckCommands();

		CheckChildren();

		CheckTimeouts();

		CheckLogPipe();

		CheckShutdown();
	}

	close(m_pair[0]);

	close(m_pair[1]);

	close(m_pipe);

	unlink(pipeName);

	pthread_mutex_destroy(&m_mutex);

	delete m_psem;
}

static void CheckCommands(void)
{
	if( !m_stop ) {

		pid_t  sid;

		int    code;

		string cmd;

		if( ReadPipe(m_pipe, sid, code, cmd) ) {

			if( code == cmdStart || code == cmdRun ) {

				size_t bar = cmd.find('|');

				if( bar != string::npos ) {

					string tag  = cmd.substr(0, bar);

					string line = cmd.substr(bar+1);

					size_t spos = line.find('|');

					auto   i    = m_tags.find(tag);

					if( i != m_tags.end() ) {

						auto j = m_programs.find(i->second);

						if( j != m_programs.end() ) {

							auto &p = j->second;

							AfxTrace("%s already running\n", p.m_tag.c_str());

							kill(p.m_pid, SIGKILL);

							SendResponse(p.m_sid);

							m_programs.erase(j);

							m_tags.erase(i);
						}
					}

					if( spos == line.npos ) {

						line = FindPath(line) + line;
					}
					else {
						string prog = line.substr(0, spos);

						line.insert(0, FindPath(prog));
					}

					AfxTrace("starting %s\n", tag.c_str());

					StartProgram(tag, line, 0, code == cmdRun);
				}

				SendResponse(sid);
			}

			if( code == cmdStop ) {

				auto i = m_tags.find(cmd);

				if( i != m_tags.end() ) {

					auto j = m_programs.find(i->second);

					if( j != m_programs.end() ) {

						auto &p = j->second;

						switch( p.m_state ) {

							case stateRunning:
							{
								AfxTrace("stopping %s\n", p.m_tag.c_str());

								kill(p.m_pid, SIGTERM);

								p.m_state = stateStopping;

								p.m_delay = time(nullptr);

								p.m_sid   = sid;
							}
							break;

							case stateRestart:
							{
								m_programs.erase(j);

								m_tags.erase(i);

								SendResponse(sid);
							}
							break;

							default:
							{
								SendResponse(sid);
							}
							break;
						}

						return;
					}
				}

				SendResponse(sid);
			}

			if( code == cmdSignal ) {

				auto i = m_tags.find(cmd);

				if( i != m_tags.end() ) {

					auto j = m_programs.find(i->second);

					if( j != m_programs.end() ) {

						auto &p = j->second;

						switch( p.m_state ) {

							case stateRunning:
							{
								AfxTrace("signalling %s\n", p.m_tag.c_str());

								kill(p.m_pid, SIGUSR1);
							}
							break;
						}
					}
				}

				SendResponse(sid);
			}

			if( code == cmdDump ) {

				int n = 1;

				AfxTrace("========\n");

				for( auto i : m_programs ) {

					auto   &p   = i.second;

					string line = p.m_line;

					for( size_t b = 0; (b = line.find('|', b)) != string::npos; line[b++] = ' ' );

					AfxTrace("%2u) %-12s %u %s\n", n++, p.m_tag.c_str(), p.m_state, line.c_str());
				}

				AfxTrace("========\n");

				SendResponse(sid);
			}
		}
	}
}

static void CheckChildren(void)
{
	for( ;;) {

		int status;

		int pid;

		if( m_child ) {

			// Have to use slow wait if other children are running.

			for( auto &p : m_programs ) {

				if( (pid = waitpid(p.second.m_pid, &status, WNOHANG)) >= 0 ) {

					break;
				}
			}
		}
		else {
			pid = waitpid(0, &status, WNOHANG);
		}

		if( pid > 0 ) {

			auto i = m_programs.find(pid);

			if( i != m_programs.end() ) {

				auto &p = i->second;

				if( WIFEXITED(status) ) {

					int code = WEXITSTATUS(status);

					if( code < 255 ) {

						AfxTrace("%s exited with code %u\n", p.m_tag.c_str(), code);
					}
					else {
						AfxTrace("%s failed to start\n", p.m_tag.c_str());
					}
				}
				else {
					if( WIFSIGNALED(status) ) {

						int code = WTERMSIG(status);

						AfxTrace("%s exited with signal %u\n", p.m_tag.c_str(), code);

						if( WCOREDUMP(status) ) {

							AfxTrace("%s created a core dump\n", p.m_tag.c_str());

							ProcessDump(p, CPrintf("/tmp/core/%u.dump", pid));
						}
					}
				}

				if( p.m_once ) {

					if( p.m_state == stateRunning ) {

						p.m_state = stateStopping;
					}
				}

				switch( p.m_state ) {

					case stateRunning:
					{
						if( p.m_tag == "c32" ) {

							int fd = open("/./tmp/crimson/leds/LedManager.pipe", O_WRONLY);

							if( fd >= 0 ) {

								char const *p = "FF0000-200-200,FFFFFF-200-200\n";

								write(fd, p, strlen(p));

								close(fd);
							}

							if( WIFEXITED(status) ) {

								if( WEXITSTATUS(status) == 55 ) {

									AfxTrace("deliberate restart\n");

									p.m_count = 0;

									CheckUpdate(p);
								}

								if( WEXITSTATUS(status) == 66 ) {

									AfxTrace("execute order 66\n");

									p.m_state = stateRestart;

									p.m_delay = 0;

									system("/sbin/reboot");
								}

								if( WEXITSTATUS(status) == 0 ) {

									CheckUpdate(p);
								}
							}
							else {
								CheckUpdate(p);
							}
						}
						else {
							AfxTrace("%s stopped unexpectedly\n", p.m_tag.c_str());

							p.m_state = stateRestart;

							p.m_delay = time(nullptr);
						}
					}
					break;

					case stateStopping:
					case stateKilling:
					{
						AfxTrace("%s stopped\n", p.m_tag.c_str());

						if( p.m_once ) {

							string pipe = "/tmp/crimson/init/" + p.m_tag + ".pipe";

							unlink(pipe.c_str());
						}

						if( true ) {

							SendResponse(p.m_sid);

							m_tags.erase(p.m_tag);

							m_programs.erase(i);
						}

						if( m_stop && m_programs.empty() ) {

							m_done = true;
						}
					}
					break;
				}
			}

			continue;
		}

		break;
	}
}

static void CheckTimeouts(void)
{
	for( auto i = m_programs.begin(); i != m_programs.end(); ) {

		auto  j = i++;

		auto &p = j->second;

		switch( p.m_state ) {

			case stateRunning:
			{
				if( p.m_delay ) {

					int wait = timeGood;

					if( p.m_once ) {

						wait = timeOnce;
					}

					if( p.m_tag == "c32" ) {

						wait = wait * 2;
					}

					if( time(nullptr) >= p.m_delay + wait ) {

						if( p.m_once ) {

							AfxTrace("%s ran too long\n", p.m_tag.c_str());

							kill(p.m_pid, SIGTERM);

							p.m_state = stateStopping;

							p.m_delay = time(nullptr);
						}
						else {
							AfxTrace("%s considered stable\n", p.m_tag.c_str());

							p.m_count = 0;

							p.m_delay = 0;
						}
					}
				}
			}
			break;

			case stateStopping:
			case stateKilling:
			{
				if( p.m_delay ) {

					int wait = timeExit;

					if( time(nullptr) >= p.m_delay + wait ) {

						AfxTrace("killing %s\n", p.m_tag.c_str());

						kill(p.m_pid, SIGKILL);

						p.m_state = stateKilling;

						p.m_delay = time(nullptr);
					}
				}
			}
			break;

			case stateRestart:
			{
				if( p.m_delay ) {

					int  wait = timeRestart;

					bool c32  = false;

					if( p.m_tag == "c32" ) {

						if( p.m_delay == 1 ) {

							if( StopChangedPrograms() ) {

								p.m_delay = time(nullptr) + timeRestart;
							}
						}

						c32 = true;
					}
					else {
						wait <<= min(5, p.m_count);
					}

					if( time(nullptr) >= p.m_delay + wait ) {

						string tag   = p.m_tag;

						string lines = p.m_line;

						int    count = p.m_count + 1;

						m_programs.erase(j);

						m_tags.erase(tag);

						if( c32 ) {

							if( count >= 4 ) {

								AfxTrace("killing crimson config\n");

								KillCrimsonConfig();
							}

							if( count >= 6 ) {

								AfxTrace("killing crimson update\n");

								KillCrimsonUpdate();
							}

							if( count >= 8 ) {

								AfxTrace("killing system config\n");

								KillSystemConfig();
							}
						}

						AfxTrace("restarting %s for attempt %u\n", tag.c_str(), count);

						StartProgram(tag, lines, count, false);
					}
				}
			}
			break;
		}
	}
}

static void CheckLogPipe(void)
{
	while( WaitRead(m_pair[0], 0) ) {

		char    data[4096];

		ssize_t size = read(m_pair[0], data, sizeof(data)-1);

		if( size > 0 ) {

			data[size] = 0;

			char *pos = data;

			char *end;

			while( (end = strchr(pos, '\n')) ) {

				m_log.append(pos, end);

				SetLogMode(2);

				syslog(LOG_NOTICE, "%s\n", m_log.c_str());

				m_log.clear();

				pos = end+1;
			}

			m_log.append(pos);

			continue;
		}

		break;
	}
}

static void CheckReset(void)
{
	struct stat s;

	if( stat(resetRequest, &s) == 0 ) {

		AfxTrace("clearing configuration\n");

		KillCrimsonConfig();

		KillCrimsonUpdate();

		KillSystemConfig();

		unlink(resetRequest);
	}
}

static void CheckShutdown(void)
{
	if( m_stop && !m_seen ) {

		for( auto i = m_programs.begin(); i != m_programs.end(); ) {

			auto  j = i++;

			auto &p = j->second;

			switch( p.m_state ) {

				case stateRunning:
				{
					AfxTrace("stopping %s\n", p.m_tag.c_str());

					kill(p.m_pid, SIGTERM);

					p.m_state = stateStopping;

					p.m_delay = time(nullptr);
				}
				break;

				case stateRestart:
				{
					m_tags.erase(p.m_tag);

					m_programs.erase(j);
				}
				break;
			}
		}

		if( m_programs.empty() ) {

			m_done = true;
		}

		m_seen = true;
	}
}

static bool StopChangedPrograms(void)
{
	// TODO -- This won't catch SO changes...

	bool hit = false;

	for( auto i : m_programs ) {

		auto &p = i.second;

		if( p.m_tag != "c32" && p.HasNodeChanged() ) {

			AfxTrace("stopping %s on binary change\n", p.m_tag.c_str());

			kill(p.m_pid, SIGTERM);

			hit = true;
		}
	}

	return hit;
}

static bool ReadPipe(int pipe, pid_t &sid, int &code, string &cmd)
{
	if( WaitRead(pipe, 0) ) {

		int head[4];

		while( read(pipe, head, sizeof(head)) == sizeof(head) ) {

			if( head[0] == magicHead ) {

				size_t size = head[3];

				if( size >= 0 && size <= 4096 ) {

					bytes data(size+1, 0);

					if( read(pipe, data.data(), size) == size ) {

						int tail[1];

						if( read(pipe, tail, sizeof(tail)) == sizeof(tail) ) {

							if( tail[0] == magicTail ) {

								sid  = head[1];

								code = head[2];

								cmd  = (char const *) data.data();

								return true;
							}
						}
					}
				}
			}

			BYTE empty[128];

			while( read(pipe, empty, sizeof(empty)) > 0 );
		}
	}

	return false;
}

static bool SendPipe(int pipe, int code, void const *send, size_t size)
{
	bytes data(size + 5 * sizeof(int), 0);

	int *p = (int *) data.data();

	*p++ = magicHead;

	*p++ = getpid();

	*p++ = code;

	*p++ = size;

	memcpy(p, send, size);

	p = (int *) (PBYTE(p) + size);

	*p++ = magicTail;

	return write(pipe, data.data(), data.size()) == data.size();
}

static bool SendCommand(int code, string const &send)
{
	CPrintf name("%s/%u.pipe", pipePath, getpid());

	if( !mkfifo(name, 0666) ) {

		int wait = open(name, O_RDONLY | O_NDELAY | O_CLOEXEC);

		if( wait >= 0 ) {

			int pipe = open(pipeName, O_RDWR | O_NDELAY | O_CLOEXEC);

			if( pipe >= 0 ) {

				if( SendPipe(pipe, code, send.data(), send.size()) ) {

					close(pipe);

					if( WaitRead(wait, 10000) ) {

						close(wait);

						unlink(name);

						return true;
					}
					else {
						AfxTrace("timeout on reply\n");
					}
				}
				else {
					AfxTrace("unable to send command\n");
				}
			}
			else {
				AfxTrace("unable to open send pipe\n");
			}

			close(wait);
		}
		else {
			AfxTrace("unable to open wait pipe\n");
		}

		unlink(name);
	}
	else {
		AfxTrace("unable to create pipe\n");
	}

	return false;
}

static bool StartProgram(string const &tag, string const &line, int count, bool once)
{
	pthread_mutex_lock(&m_mutex);

	int pid = fork();

	if( pid >= 0 ) {

		if( !pid ) {

			vector<char *> args;

			ParseLine(args, line);

			m_psem->Wait();

			dup2(open("/dev/null", O_RDONLY), 0);

			close(m_pair[0]);

			if( once ) {

				string pipe = "/tmp/crimson/init/" + tag + ".pipe";

				mkfifo(pipe.c_str(), 0666);

				int fp = open(pipe.c_str(), O_WRONLY);

				dup2(fp, 1);

				dup2(fp, 2);

				for( auto a : args ) {

					printf(a ? "%s " : "\n\n", a);
				}

				fflush(stdout);

				close(m_pair[1]);
			}
			else {
				if( tag == "c32" ) {

					dup2(m_pair[1], 1);

					dup2(m_pair[1], 2);
				}
				else {
					dup2(open("/dev/null", O_WRONLY), 1);

					dup2(open("/dev/null", O_WRONLY), 2);

					close(m_pair[1]);
				}
			}

			execv(args[0], args.data());

			exit(255);
		}
		else {
			m_programs.try_emplace(pid, tag, line, count, once, pid);

			m_tags.try_emplace(tag, pid);

			m_psem->Post();

			pthread_mutex_unlock(&m_mutex);

			return true;
		}
	}

	pthread_mutex_unlock(&m_mutex);

	return false;
}

static void ParseLine(vector<char *> &args, string const &line)
{
	for( size_t s = 0; s != string::npos; ) {

		size_t p = line.find('|', s);

		string a = (p == line.npos) ? line.substr(s) : line.substr(s, p-s);

		args.push_back(strdup(a.c_str()));

		s = (p == string::npos) ? p : p + 1;
	}

	args.push_back(nullptr);
}

static string FindPath(string const &program)
{
	if( program.find('/') == string::npos ) {

		static char const *path[] = {

			"/opt/crimson/bin/",
			"/bin/",
			"/sbin/",
			"/usr/local/",
			"/usr/bin/",
			"/usr/sbin/",
		};

		for( size_t n = 0; n < elements(path); n++ ) {

			if( ifstream(path[n] + program).good() ) {

				return path[n];
			}
		}
	}

	return "";
}

static bool WaitRead(int fd1, int fd2, int ms)
{
	fd_set set;

	FD_ZERO(&set);

	FD_SET(fd1, &set);

	FD_SET(fd2, &set);

	timeval tv;

	tv.tv_sec  = 0;

	tv.tv_usec = 1000 * ms;

	for( ;;) {

		int r = select(max(fd1, fd2)+1, &set, nullptr, nullptr, &tv);

		if( r == -EAGAIN || r == -EINTR ) {

			continue;
		}

		return r > 0;
	}
}

static bool WaitRead(int fd, int ms)
{
	fd_set set;

	FD_ZERO(&set);

	FD_SET(fd, &set);

	timeval tv;

	tv.tv_sec  = 0;

	tv.tv_usec = 1000 * ms;

	for( ;;) {

		int r = select(fd+1, &set, nullptr, nullptr, &tv);

		if( r == -EAGAIN || r == -EINTR ) {

			continue;
		}

		return r > 0;
	}
}

static bool SendResponse(pid_t sid)
{
	if( sid ) {

		CPrintf name("%s/%u.pipe", pipePath, sid);

		int pipe = open(name, O_WRONLY | O_NDELAY);

		if( pipe >= 0 ) {

			BYTE b = 1;

			write(pipe, &b, 1);

			close(pipe);

			return true;
		}
	}

	return false;
}

static bool CheckUpdate(CProgram &p)
{
	if( HasCrimsonUpdate() ) {

		p.m_state = stateRestart;

		p.m_delay = 0;

		RunUpdate(p);

		return true;
	}
	else {
		if( ifstream("/tmp/debug").good() ) {

			p.m_state = stateRestart;

			p.m_delay = 0;

			return false;
		}

		p.m_state = stateRestart;

		p.m_delay = time(nullptr);

		return false;
	}
}

static bool HasCrimsonUpdate(void)
{
	int g1 = open("/opt/crimson/.bin/flip.me", O_RDONLY);

	if( g1 >= 0 ) {

		close(g1);

		return true;
	}

	return false;
}

static bool HasDistroUpdate(void)
{
	int g1 = open("/flip.me", O_RDONLY);

	if( g1 >= 0 ) {

		close(g1);

		return true;
	}

	return false;
}

static bool RunUpdate(CProgram &p)
{
	AfxTrace("performing update\n");

	pthread_t id;

	pthread_create(&id, NULL, UpdateThread, &p);

	pthread_detach(id);

	return true;
}

static void * UpdateThread(void *arg)
{
	CProgram &p = *((CProgram *) arg);

	string last = ReadGeneration();

	bool distro = HasDistroUpdate();

	__sync_fetch_and_add(&m_child, 1);

	UpdateCrimson();

	if( distro ) {

		UpdateDistro();
	}

	if( distro || ReadGeneration() != last || GetOurNode() != m_node ) {

		AfxTrace("reboot needed after upgrade\n");

		system("/sbin/reboot");
	}
	else {
		p.m_delay = 1;
	}

	__sync_fetch_and_sub(&m_child, 1);

	return NULL;
}

static bool UpdateCrimson(void)
{
	int pid = fork();

	if( pid >= 0 ) {

		if( !pid ) {

			dup2(open("/dev/null", O_RDONLY), 0);

			close(m_pair[0]);

			dup2(m_pair[1], 1);

			dup2(m_pair[1], 2);

			char const *args[] = {

				"/opt/crimson/.bin/Flipper",
				"--part2",
				"/opt/crimson/bin",
				NULL
			};

			execv(args[0], (char **) args);

			exit(255);
		}

		waitpid(pid, NULL, 0);
	}

	unlink("/opt/crimson/bin/flip.me");

	return true;
}

static bool UpdateDistro(void)
{
	int pid = fork();

	if( pid >= 0 ) {

		if( !pid ) {

			dup2(open("/dev/null", O_RDONLY), 0);

			close(m_pair[0]);

			dup2(m_pair[1], 1);

			dup2(m_pair[1], 2);

			char const *args[] = {

				"/opt/crimson/bin/Flipper",
				"--part2",
				"/bin",
				"/boot",
				"/lib",
				"/sbin",
				"/usr",
				NULL
			};

			execv(args[0], (char **) args);

			exit(255);
		}

		waitpid(pid, NULL, 0);

		unlink("/flip.me");

		sync();

		reboot(LINUX_REBOOT_CMD_RESTART);
	}

	unlink("/flip.me");

	return true;
}

static void SetLogMode(int mode)
{
	if( m_mode != mode ) {

		if( m_mode ) {

			closelog();
		}

		switch( (m_mode = mode) ) {

			case 1:
			{
				openlog("c3-run", 0, LOG_USER);
			}
			break;

			case 2:
			{
				openlog("c3-app", 0, LOG_USER);
			}
			break;
		}
	}
}

static string ReadGeneration(void)
{
	string gen;

	ifstream stm("/opt/crimson/bin/generation");

	if( stm.good() ) {

		stm >> gen;
	}

	return gen;
}

static void ProcessDump(CProgram const &p, string const &dump)
{
	mkdir("/vap/opt/crimson/logs", 0755);

	time_t tv   = time(NULL);

	tm    *tp   = gmtime(&tv);

	string prog = p.m_line.substr(0, p.m_line.find('|')).c_str();

	string base = prog.substr(prog.rfind('/')+1);

	string type = C3_BRANCH;

	type.erase(0, type.find('-') + 1);

	type.erase(type.find('-'), type.npos);

	CPrintf name("core-%2.2u%2.2u%2.2u-%2.2u%2.2u%2.2u-%s-%s-%4.4u-%u.dump",
		     tp->tm_year%100,
		     tp->tm_mon+1,
		     tp->tm_mday,
		     tp->tm_hour,
		     tp->tm_min,
		     tp->tm_sec,
		     base.c_str(),
		     type.c_str(),
		     C3_BUILD,
		     C3_HOTFIX
	);

	string *list = new string[2];

	list[0] = dump;

	list[1] = name;

	pthread_t id;

	pthread_create(&id, NULL, CompressThread, list);

	pthread_detach(id);
}

static void * CompressThread(void *arg)
{
	string *list = (string *) arg;

	string const &dump = list[0];

	string const &name = list[1];

	__sync_fetch_and_add(&m_child, 1);

	AfxTrace("compressing core dump\n");

	rename(dump.c_str(), ("/tmp/core/" + name).c_str());

	system(CPrintf("gzip /tmp/core/%s", name.c_str()));

	system(CPrintf("mv /tmp/core/%s.gz /vap/opt/crimson/logs", name.c_str()));

	sync();

	AfxTrace("core dump archived\n");

	__sync_fetch_and_sub(&m_child, 1);

	LimitUsage("/vap/opt/crimson/logs", "core-");

	delete[] list;

	return NULL;
}

static void LimitUsage(string const &path, string const &match)
{
	vector<string> list;

	if( FindFiles(list, path, match) ) {

		long long total = 0;

		long long limit = 10 * 1024 * 1024;

		for( auto const &name : list ) {

			string full = path + '/' + name;

			if( total >= limit ) {

				unlink(full.c_str());
			}
			else {
				struct stat s;

				if( stat(full.c_str(), &s) == 0 ) {

					if( (total += s.st_size) >= limit ) {

						unlink(full.c_str());
					}
				}
			}
		}
	}
}

static bool FindFiles(vector<string> &list, string const &path, string const &match)
{
	DIR *dir = opendir(path.c_str());

	if( dir ) {

		struct dirent *entry;

		while( (entry = readdir(dir)) ) {

			if( entry->d_type == DT_REG ) {

				string name(entry->d_name);

				if( name.substr(0, match.size()) == match ) {

					list.push_back(entry->d_name);
				}
			}
		}

		closedir(dir);

		sort(list.begin(), list.end(), greater<string>());

		return true;
	}

	return false;
}

static ino_t GetOurNode(void)
{
	struct stat s;

	stat("/opt/crimson/bin/InitC32", &s);

	return s.st_ino;
}

static void KillCrimsonConfig(void)
{
	system("rm -rf /vap/opt/crimson/appbase");
}

static void KillCrimsonUpdate(void)
{
	system("rm -rf /vap/opt/crimson/disk/C/update");
}

static void KillSystemConfig(void)
{
	system("rm -rf /vap/opt/crimson/sysbase");
}

// End of File
