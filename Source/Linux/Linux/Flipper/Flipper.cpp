
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Image Flipper
//
// Copyright (c) 2020 Red Lion Controls
//
// All Rights Reserved
//

// Static Data

static	uint64	m_t = 0;

static	uint64	m_b = 0;

// Prototypes

global	int	main(int nArg, char *pArg[]);
static	void	AfxTrace(char const *p, ...);
static	void	Error(char const *p, ...);
static	void	AfxDump(void const *pData, size_t uCount);
static	bool	DeepCopy(string const &spath, string const &upath, bool alias);
static	bool	DeepCopyFrom(string const &spath, string const &dpath, bool alias);
static	bool	DeepRemove(string const &path);
static	string	MakeDestPath(string const &spath);
static	string	Combine(string const &path, string const &file);
static	bool	ReadDirectory(set<string> &files, set<string> &links, set<string> &folds, DIR *dir);
static	bool	AreFilesSame(string const &src, string const &dst);
static	bool	AreLinksSame(string const &src, string const &dst);
static	bool	CopyFile(string const &src, string const &dst, bool make);
static	bool	CopyLink(string const &src, string const &dst, bool make);
static	uint64	GetTickCount(void);

// Code

global int main(int nArg, char *pArg[])
{
	if( nArg > 1 ) {

		int pf = 0;

		int pt = 3;

		int sa = 1;

		int ad = 2;

		if( !strcmp(pArg[1], "--part1") ) {

			pt = 2;

			sa = 2;

			ad = 2;
		}

		if( !strcmp(pArg[1], "--part2") ) {

			pf = 2;

			sa = 2;

			ad = 1;
		}

		if( (nArg - sa) % ad == 0 ) {

			m_t = GetTickCount();

			for( int p = pf; p < pt; p++ ) {

				AfxTrace("*** Pass %u started at %ums\n", 1+p, int(GetTickCount() - m_t));

				bool alias = (p == 0);

				for( int n = sa; n < nArg; n+=ad ) {

					string spath = pArg[n+0];

					if( p < 2 ) {

						string upath = pArg[n+1];

						DeepCopy(spath, upath, alias);
					}
					else {
						string dpath = MakeDestPath(spath);

						string tpath = dpath + ".tmp";

						rename(spath.c_str(), tpath.c_str());
						rename(dpath.c_str(), spath.c_str());
						rename(tpath.c_str(), dpath.c_str());
					}
				}

				AfxTrace("=== Sync at %ums\n", int(GetTickCount() - m_t));

				sync();
			}

			AfxTrace("*** Done at %ums (%u bytes)\n", int(GetTickCount() - m_t), int(m_b));

			return 0;
		}
	}

	return 1;
}

static void AfxTrace(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	vfprintf(stdout, p, v);

	fflush(stdout);

	va_end(v);
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	fprintf(stderr, "%s: ", "Flipper");

	vfprintf(stderr, p, v);

	fprintf(stderr, "\n");

	va_end(v);

	exit(1);
}

global void AfxDump(void const *pData, size_t uCount)
{
	if( pData ) {

		char const * p = (char const *) pData;

		size_t       s = 0;

		for( size_t n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				AfxTrace("%8.8X : %4.4X : ", p + n, n);

				s = n;
			}

			if( true ) {

				AfxTrace("%2.2X ", p[n]);
			}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				AfxTrace(" ");

				for( size_t j = n; j % 0x10 < 0xF; j++ ) {

					AfxTrace("   ");
				}

				for( size_t i = 0; i <= n - s; i++ ) {

					byte b = p[s+i];

					if( b >= 32 && b < 127 ) {

						AfxTrace("%c", b);
					}
					else
						AfxTrace(".");
				}

				AfxTrace("\n");
			}
		}
	}
}

static bool DeepCopy(string const &spath, string const &upath, bool alias)
{
	AfxTrace("=== Processing %s at %ums\n", spath.c_str(), int(GetTickCount() - m_t));

	string dpath = MakeDestPath(spath);

	if( alias ) {

		return DeepCopyFrom(spath, dpath, true);
	}

	return DeepCopyFrom(upath, dpath, false);
}

static bool DeepCopyFrom(string const &spath, string const &dpath, bool alias)
{
	DIR *sd = opendir(spath.c_str());

	if( sd ) {

		// Get access to the destination, creating it with the
		// same permissions as the source if it does not exist.

		DIR *dd = opendir(dpath.c_str());

		if( !dd ) {

			struct stat s;

			stat(spath.c_str(), &s);

			mkdir(dpath.c_str(), s.st_mode & 0777);

			if( !(dd = opendir(dpath.c_str())) ) {

				Error("failed to open destination %s", dpath.c_str());

				return false;
			}
		}

		set<string> sfiles, dfiles;

		set<string> sfolds, dfolds;

		if( alias ) {

			// This is used when we are copying a folder into its shadow
			// copy. We use hard links to minimize the amount of extra disk
			// space that we consume by making the copy.

			ReadDirectory(sfiles, sfiles, sfolds, sd);

			ReadDirectory(dfiles, dfiles, dfolds, dd);

			for( auto const &sfile : sfiles ) {

				auto di = dfiles.find(sfile);

				string sc = Combine(spath, sfile);

				string dc = Combine(dpath, sfile);

				if( di == dfiles.end() ) {

					AfxTrace("link file %s\n", dc.c_str());

					link(sc.c_str(), dc.c_str());
				}
				else {
					struct stat ss, ds;

					stat(sc.c_str(), &ss);

					stat(dc.c_str(), &ds);

					if( ss.st_dev != ds.st_dev || ss.st_ino != ds.st_ino ) {

						AfxTrace("link file %s\n", dc.c_str());

						unlink(dc.c_str());

						link(sc.c_str(), dc.c_str());
					}

					dfiles.erase(di);
				}
			}
		}
		else {
			// This is used when we are copying the update into the shadow
			// copy. We check for changes, and copy the updated files or
			// symlinks as appropriate. The copy routines unlink what will
			// be a hard link from the prior operation, so that the shadow
			// copy ends up containing a combination of hard links to the
			// old copy and copies of the new files and symlinks.

			set<string> slinks, dlinks;

			ReadDirectory(sfiles, slinks, sfolds, sd);

			ReadDirectory(dfiles, dlinks, dfolds, dd);

			for( auto const &sfile : sfiles ) {

				auto di = dfiles.find(sfile);

				string sc = Combine(spath, sfile);

				string dc = Combine(dpath, sfile);

				if( di == dfiles.end() ) {

					CopyFile(sc, dc, true);
				}
				else {
					if( !AreFilesSame(sc, dc) ) {

						CopyFile(sc, dc, false);
					}

					dfiles.erase(di);
				}

				dlinks.erase(sfile);
			}

			for( auto const &slink: slinks ) {

				auto di = dlinks.find(slink);

				string sc = Combine(spath, slink);

				string dc = Combine(dpath, slink);

				if( di == dlinks.end() ) {

					CopyLink(sc, dc, true);
				}
				else {
					if( !AreLinksSame(sc, dc) ) {

						CopyLink(sc, dc, false);
					}

					dlinks.erase(di);
				}

				dfiles.erase(slink);
			}

			for( auto const &dlink : dlinks ) {

				string dc = Combine(dpath, dlink);

				AfxTrace("kill file %s\n", dc.c_str());

				unlink(dc.c_str());
			}
		}

		// Remove any files that aren't needed any more.

		for( auto const &dfile : dfiles ) {

			string dc = Combine(dpath, dfile);

			AfxTrace("kill file %s\n", dc.c_str());

			unlink(dc.c_str());
		}

		// Repeat the process for any subdirectories.

		for( auto const &sfold : sfolds ) {

			auto di = dfolds.find(sfold);

			string sc = Combine(spath, sfold);

			string dc = Combine(dpath, sfold);

			if( di != dfolds.end() ) {

				dfolds.erase(di);
			}

			DeepCopyFrom(sc, dc, alias);
		}

		// Remove any subdirectories that aren't needed.

		for( auto const &dfold: dfolds ) {

			string dc = Combine(dpath, dfold);

			AfxTrace("kill path %s\n", dc.c_str());

			DeepRemove(dc);
		}

		// All done for this location!

		closedir(sd);

		closedir(dd);

		return true;
	}

	Error("failed to open source %s", spath.c_str());

	return false;
}

static bool DeepRemove(string const &path)
{
	DIR *dir = opendir(path.c_str());

	if( dir ) {

		set<string> files, folds;

		ReadDirectory(files, files, folds, dir);

		for( auto const &file : files ) {

			unlink(Combine(path, file).c_str());
		}

		for( auto const &fold: folds ) {

			DeepRemove(Combine(path, fold));
		}

		closedir(dir);

		rmdir(path.c_str());

		return true;
	}

	return false;
}

static string MakeDestPath(string const &spath)
{
	size_t p = spath.rfind('/');

	if( p == string::npos ) {

		Error("invalid path %s", spath.c_str());
	}

	return spath.substr(0, p+1) + '.' + spath.substr(p+1);
}

static string Combine(string const &path, string const &file)
{
	if( path[path.size()-1] == '/' || file[0] == '/' ) {

		return path + file;
	}

	return path + '/' + file;
}

static bool ReadDirectory(set<string> &files, set<string> &links, set<string> &folds, DIR *dir)
{
	dirent *dp;

	while( (dp = readdir(dir)) ) {

		if( dp->d_type == DT_REG ) {

			files.insert(dp->d_name);
		}

		if( dp->d_type == DT_LNK ) {

			links.insert(dp->d_name);
		}

		if( dp->d_type == DT_DIR ) {

			if( dp->d_name[0] == '.' ) {

				if( dp->d_name[1] == '.' ) {

					if( dp->d_name[2] == 0 ) {

						continue;
					}
				}

				if( dp->d_name[1] == 0 ) {

					continue;
				}
			}

			folds.insert(dp->d_name);
		}
	}

	return true;
}

static bool AreFilesSame(string const &src, string const &dst)
{
	struct stat ss, ds;

	stat(src.c_str(), &ss);

	stat(dst.c_str(), &ds);

	if( ss.st_mode == ds.st_mode ) {

		if( ss.st_size == ds.st_size ) {

			int fs = open(src.c_str(), O_RDONLY);

			if( fs >= 0 ) {

				int fd = open(dst.c_str(), O_RDONLY);

				if( fd >= 0 ) {

					static byte bs[256 * 1024];

					static byte bd[256 * 1024];

					for( ;;) {

						ssize_t rs = read(fs, bs, sizeof(bs));

						ssize_t rd = read(fd, bd, sizeof(bd));

						if( rs >= 0 && rs == rd ) {

							if( !memcmp(bs, bd, rs) ) {

								if( rs < sizeof(bs) ) {

									close(fs);

									close(fd);

									return true;
								}

								continue;
							}
						}

						break;
					}
				}

				close(fd);
			}

			close(fs);
		}
	}

	return false;
}

static bool AreLinksSame(string const &src, string const &dst)
{
	char st[256], dt[256];

	ssize_t sr = readlink(src.c_str(), st, sizeof(st));

	ssize_t dr = readlink(dst.c_str(), dt, sizeof(dt));

	if( sr > 0 && dr > 0 ) {

		st[sr] = 0;

		dt[dr] = 0;

		if( !strcmp(st, dt) ) {

			return true;
		}
	}

	return false;
}

static bool CopyFile(string const &src, string const &dst, bool make)
{
	int fs = open(src.c_str(), O_RDONLY);

	if( fs >= 0 ) {

		if( true || !make ) {

			unlink(dst.c_str());
		}

		struct stat s;

		stat(src.c_str(), &s);

		int fd = open(dst.c_str(), O_CREAT | O_WRONLY, s.st_mode & 0777);

		if( fd >= 0 ) {

			uint64 t = GetTickCount();

			size_t c = 0;

			byte   buff[65536];

			for( ;;) {

				ssize_t r = read(fs, buff, sizeof(buff));

				if( r >= 0 ) {

					if( r == 0 ) {

						int dt = max(1, int(GetTickCount() - t));

						AfxTrace("%s file %s (%u bytes in %ums at %u kbps)\n", make ? "make" : "edit", dst.c_str(), c, dt, c / dt);

						m_b += c;

						close(fd);

						close(fs);

						return true;
					}

					write(fd, buff, r);

					c += r;

					continue;
				}

				break;
			}

			close(fd);
		}

		close(fs);
	}

	Error("failed to copy file %s to %s", src.c_str(), dst.c_str());

	return false;
}

static bool CopyLink(string const &src, string const &dst, bool make)
{
	char st[256];

	ssize_t sr = readlink(src.c_str(), st, sizeof(st));

	if( sr > 0 ) {

		st[sr] = 0;

		if( true || !make ) {

			unlink(dst.c_str());
		}

		if( symlink(st, dst.c_str()) == 0 ) {

			AfxTrace("%s link %s\n", make ? "make" : "edit", dst.c_str());

			return true;
		}
	}

	Error("failed to copy link %s to %s", src.c_str(), dst.c_str());

	return false;
}

static uint64 GetTickCount(void)
{
	struct timespec tv;

	clock_gettime(CLOCK_REALTIME, &tv);

	uint64 ms;

	ms  = tv.tv_sec  * uint64(1000);

	ms += tv.tv_nsec / uint64(1000 * 1000);

	return ms;
}

// End of File
