
#include "Intern.hpp"

// Prototypes

static	int	m_fd	  = -1;

static	int	m_pd	  = -1;

static	string	m_pipe	  = "/tmp/crimson/syslog/syslog.pipe";

static	string	m_logs	  = "/var/log/messages";

static	bool	m_sigpipe = false;

static	char	m_store[65536];

static	size_t	m_limit = sizeof(m_store) - 1;

static	int	m_avail;

static	bool	m_first;

// Code

global	int	main(void);
static	void	OnSigPipe(int n);
static	void	MakePipe(void);
static	bool	OpenPipe(void);
static	bool	OpenLogs(void);
static	bool	PumpData(void);
static	bool	TestPipe(int ms);

global int main(void)
{
	MakePipe();

	for( ;;) {

		// The pipe will only open when there is a valid
		// listener and the call will block until then.

		if( OpenPipe() ) {

			while( !m_sigpipe ) {

				if( OpenLogs() ) {

					while( !m_sigpipe ) {

						// Check the pipe status without sleeping.

						if( !TestPipe(0) ) {

							m_sigpipe = true;

							continue;
						}

						// Pump the next line into the pipe.

						if( PumpData() ) {

							// Do not worry about anything else
							// until we have pumped all our data.

							continue;
						}

						// Did the log file get closed from rotation?

						if( m_fd < 0 ) {

							// Keep trying to reopen the file.

							if( (m_fd = open(m_logs.c_str(), O_RDONLY)) < 0 ) {

								usleep(100 * 1000);

								continue;
							}
						}
						else {
							// Read whatever data is available. Not that this will
							// not block as we'll be at the end of the file, so we
							// can't use select or anything fancy. Instead, we use
							// our pipe test routine to sleep before we try again.

							ssize_t fill = read(m_fd, m_store + m_avail, m_limit - m_avail - 1);

							if( fill >= 0 ) {

								if( fill > 0 ) {

									// Keep a null at the end of the buffer.

									m_store[m_avail += fill] = 0;
								}
								else {
									// Check if the pipe has been disconnected, as if
									// so, set the signal to shut things down. This also
									// serves as our polling delay.

									if( !TestPipe(50) ) {

										m_sigpipe = true;
									}
								}
							}
							else {
								// Close on error.

								close(m_fd);

								m_fd = -1;
							}
						}
					}
				}
				else {
					// Wait for logs to reappear.

					usleep(100 * 1000);
				}
			}

			close(m_pd);

			m_pd = -1;
		}
		else {
			// Wait to retry opening of pipe.

			usleep(100 * 1000);
		}
	}
}

static void OnSigPipe(int n)
{
	signal(SIGPIPE, OnSigPipe);

	m_sigpipe = true;
}

static void MakePipe(void)
{
	system(("mkdir -p " + m_pipe.substr(0, m_pipe.rfind('/'))).c_str());

	mkfifo(m_pipe.c_str(), 0666);
}

static bool OpenPipe(void)
{
	if( (m_pd = open(m_pipe.c_str(), O_WRONLY)) >= 0 ) {

		m_sigpipe = false;

		return true;
	}

	return false;
}

static bool OpenLogs(void)
{
	if( (m_fd = open(m_logs.c_str(), O_RDONLY)) >= 0 ) {

		struct stat s = { 0 };

		if( fstat(m_fd, &s) == 0 ) {

			// Preload the buffer with the last m_limit characters.

			lseek(m_fd, (s.st_size > m_limit) ? (s.st_size - m_limit) : 0, 0);

			if( (m_avail = read(m_fd, m_store, m_limit)) >= 0 ) {

				m_store[m_avail] = 0;

				m_first = true;

				return true;
			}
		}

		close(m_fd);
	}

	return false;
}

static bool PumpData(void)
{
	if( m_avail ) {

		char *end = strchr(m_store, '\n');

		if( end ) {

			// Don't output first line as it may be partial
			// as a result of the preloading from the file.

			if( !m_first ) {

				write(m_pd, m_store, end - m_store + 1);
			}

			// Check for rotation and close file if required.

			if( !strncmp(m_store, "!!! syslogd", 11) ) {

				close(m_fd);

				m_fd = -1;
			}

			// Shuffle the data down ready for the next line.

			m_avail -= end + 1 - m_store;

			memmove(m_store, end + 1, m_avail + 1);

			m_first = false;

			return true;
		}
	}

	return false;
}

static bool TestPipe(int ms)
{
	// Check if the pipe has been closed, sleeping until it
	// has or until the timeout. The trick we use is based
	// on the fact that if the reader has closed their end
	// of the pipe, select will indicate it's now readable!

	fd_set fds;

	FD_ZERO(&fds);

	FD_SET(m_pd, &fds);

	timeval tv;

	tv.tv_sec  = 0;

	tv.tv_usec = ms * 1000;

	select(m_pd+1, &fds, NULL, NULL, &tv);

	return FD_ISSET(m_pd, &fds) ? false : true;
}

// End of File
