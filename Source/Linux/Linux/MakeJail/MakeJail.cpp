
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Jail Creator
//
// Copyright (c) 2020 Red Lion Controls
//
// All Rights Reserved
//

// Static Data

static	uint64	m_t = 0;

static	uint64	m_b = 0;

// Prototypes

global	int	main(int nArg, char *pArg[]);
static	void	AfxTrace(char const *p, ...);
static	void	Error(char const *p, ...);
static	void	AfxDump(void const *pData, size_t uCount);
static	string	Combine(string const &path, string const &file);
static	bool	ReadManifest(set<string> &list, string root);
static	bool	ReadDirectory(set<string> &files, set<string> &links, set<string> &folds, DIR *dir);
static	bool	MakeLinkRelative(char *link, string const &base);
static	bool	DeepMakeDirectory(string const &path);
static	bool	DeepRemove(string const &path);
static	bool	DeepCopy(string const &spath, string const &dpath, bool force);
static	bool	AreFilesSame(string const &src, string const &dst);
static	bool	AreLinksSame(string const &src, string const &dst);
static	bool	CopyFile(string const &src, string const &dst, bool make);
static	bool	CopyLink(string const &src, string const &dst, bool make);
static	uint64	GetTickCount(void);

// Code

global int main(int nArg, char *pArg[])
{
	string base = "/vap/opt/crimson/crates/base";

	DeepMakeDirectory(base);

	vector<string> roots;

	roots.push_back("/bin");
	roots.push_back("/sbin");
	roots.push_back("/lib");
	roots.push_back("/usr");

	for( string const &root : roots ) {

		DeepCopy(root, base + root, false);

		int q = 0;
	}

	return 1;
}

static void AfxTrace(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	vfprintf(stdout, p, v);

	fflush(stdout);

	va_end(v);
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	fprintf(stderr, "%s: ", "MakeJail");

	vfprintf(stderr, p, v);

	fprintf(stderr, "\n");

	va_end(v);

	exit(1);
}

global void AfxDump(void const *pData, size_t uCount)
{
	if( pData ) {

		char const * p = (char const *) pData;

		size_t       s = 0;

		for( size_t n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				AfxTrace("%8.8X : %4.4X : ", p + n, n);

				s = n;
			}

			if( true ) {

				AfxTrace("%2.2X ", p[n]);
			}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				AfxTrace(" ");

				for( size_t j = n; j % 0x10 < 0xF; j++ ) {

					AfxTrace("   ");
				}

				for( size_t i = 0; i <= n - s; i++ ) {

					byte b = p[s+i];

					if( b >= 32 && b < 127 ) {

						AfxTrace("%c", b);
					}
					else
						AfxTrace(".");
				}

				AfxTrace("\n");
			}
		}
	}
}

static string Combine(string const &path, string const &file)
{
	if( path[path.size()-1] == '/' || file[0] == '/' ) {

		return path + file;
	}

	return path + '/' + file;
}

static bool ReadManifest(set<string> &list, string root)
{
	root += '/';

	ifstream stm(root + "jail.manifest");

	if( stm.good() ) {

		while( !stm.eof() ) {

			string line;

			getline(stm, line);

			if( line.size() ) {

				list.insert(root + line);
			}
		}
	}

	return true;
}

static bool ReadDirectory(set<string> &files, set<string> &links, set<string> &folds, DIR *dir)
{
	dirent *dp;

	while( (dp = readdir(dir)) ) {

		if( dp->d_type == DT_REG ) {

			files.insert(dp->d_name);
		}

		if( dp->d_type == DT_LNK ) {

			links.insert(dp->d_name);
		}

		if( dp->d_type == DT_DIR ) {

			if( dp->d_name[0] == '.' ) {

				if( dp->d_name[1] == '.' ) {

					if( dp->d_name[2] == 0 ) {

						continue;
					}
				}

				if( dp->d_name[1] == 0 ) {

					continue;
				}
			}

			folds.insert(dp->d_name);
		}
	}

	return true;
}

static bool MakeLinkRelative(char *link, string const &base)
{
	if( link[0] == '/' ) {

		string spath(base);

		string tpath(link);

		spath.erase(spath.rfind('/'), string::npos);

		tpath.erase(tpath.rfind('/'), string::npos);

		string upath;

		string dpath;

		while( spath != tpath ) {

			if( spath.size() ) {

				upath += "../";

				spath.erase(spath.rfind('/'), string::npos);
			}

			if( tpath.size() ) {

				dpath = '/' + dpath;

				dpath = tpath.substr(tpath.rfind('/') + 1) + dpath;

				tpath.erase(tpath.rfind('/'), string::npos);
			}
		}

		string name = upath + dpath + (strrchr(link, '/') + 1);

		strcpy(link, name.c_str());

		return true;
	}

	return false;
}

static bool DeepMakeDirectory(string const &path)
{
	size_t find = 1;

	for( ;;) {

		find = path.find('/', find);

		if( find == string::npos ) {

			if( !chdir(path.c_str()) || !mkdir(path.c_str(), 0777) ) {

				return true;
			}
		}
		else {
			string left = path.substr(0, find);

			if( !chdir(left.c_str()) || !mkdir(left.c_str(), 0777) ) {

				find = find + 1;

				continue;
			}
		}

		return false;
	}
}

static bool DeepRemove(string const &path)
{
	DIR *dir = opendir(path.c_str());

	if( dir ) {

		set<string> files, folds;

		ReadDirectory(files, files, folds, dir);

		for( auto const &file : files ) {

			unlink(Combine(path, file).c_str());
		}

		for( auto const &fold: folds ) {

			DeepRemove(Combine(path, fold));
		}

		closedir(dir);

		rmdir(path.c_str());

		return true;
	}

	return false;
}

static bool DeepCopy(string const &spath, string const &dpath, bool force)
{
	DIR *sd = opendir(spath.c_str());

	if( sd ) {

		DIR *dd = opendir(dpath.c_str());

		if( !dd ) {

			struct stat s;

			stat(spath.c_str(), &s);

			mkdir(dpath.c_str(), s.st_mode & 0777);

			if( !(dd = opendir(dpath.c_str())) ) {

				Error("failed to open destination %s", dpath.c_str());

				return false;
			}
		}

		set<string> list;

		if( ReadManifest(list, spath) ) {

			if( list.size() == 1 ) {

				if( *list.begin() == "*" ) {

					force = true;
				}
			}

			bool done = false;

			set<string> sfiles, slinks, sfolds;

			set<string> dfiles, dlinks, dfolds;

			ReadDirectory(sfiles, slinks, sfolds, sd);

			ReadDirectory(dfiles, dlinks, dfolds, dd);

			for( auto const &sfile : sfiles ) {

				string sc = Combine(spath, sfile);

				string dc = Combine(dpath, sfile);

				if( force || list.find(sc) != list.end() ) {

					auto di = dfiles.find(sfile);

					if( di == dfiles.end() ) {

						CopyFile(sc, dc, true);
					}
					else {
						if( !AreFilesSame(sc, dc) ) {

							CopyFile(sc, dc, false);
						}

						dfiles.erase(di);
					}

					dlinks.erase(sfile);

					done = true;
				}
			}

			for( auto const &slink: slinks ) {

				string sc = Combine(spath, slink);

				string dc = Combine(dpath, slink);

				if( force || list.find(sc) != list.end() ) {

					auto di = dlinks.find(slink);

					if( di == dlinks.end() ) {

						CopyLink(sc, dc, true);
					}
					else {
						if( !AreLinksSame(sc, dc) ) {

							CopyLink(sc, dc, false);
						}

						dlinks.erase(di);
					}

					dfiles.erase(slink);

					done = true;
				}
			}

			for( auto const &sfold : sfolds ) {

				string sc = Combine(spath, sfold);

				string dc = Combine(dpath, sfold);

				if( force || list.find(sc) != list.end() ) {

					auto di = dfolds.find(sfold);

					if( DeepCopy(sc, dc, force) ) {

						if( di != dfolds.end() ) {

							dfolds.erase(di);
						}

						done = true;
					}
					else {
						dfolds.insert(sfold);
					}
				}
				else {
					dfolds.insert(sfold);
				}
			}

			for( auto const &dfile : dfiles ) {

				string dc = Combine(dpath, dfile);

				unlink(dc.c_str());
			}

			for( auto const &dlink : dlinks ) {

				string dc = Combine(dpath, dlink);

				unlink(dc.c_str());
			}

			for( auto const &dfold: dfolds ) {

				string dc = Combine(dpath, dfold);

				DeepRemove(dc);
			}

			closedir(sd);

			closedir(dd);

			return done;
		}

		Error("failed to open source %s", spath.c_str());
	}

	Error("failed to read manifest for %s", spath.c_str());

	return false;
}

static bool AreFilesSame(string const &src, string const &dst)
{
	struct stat ss, ds;

	stat(src.c_str(), &ss);

	stat(dst.c_str(), &ds);

	if( ss.st_mode == ds.st_mode ) {

		if( ss.st_size == ds.st_size ) {

			int fs = open(src.c_str(), O_RDONLY);

			if( fs >= 0 ) {

				int fd = open(dst.c_str(), O_RDONLY);

				if( fd >= 0 ) {

					static byte bs[256 * 1024];

					static byte bd[256 * 1024];

					for( ;;) {

						ssize_t rs = read(fs, bs, sizeof(bs));

						ssize_t rd = read(fd, bd, sizeof(bd));

						if( rs >= 0 && rs == rd ) {

							if( !memcmp(bs, bd, rs) ) {

								if( rs < sizeof(bs) ) {

									close(fs);

									close(fd);

									return true;
								}

								continue;
							}
						}

						break;
					}
				}

				close(fd);
			}

			close(fs);
		}
	}

	return false;
}

static bool AreLinksSame(string const &src, string const &dst)
{
	char st[256], dt[256];

	ssize_t sr = readlink(src.c_str(), st, sizeof(st));

	ssize_t dr = readlink(dst.c_str(), dt, sizeof(dt));

	if( sr > 0 && dr > 0 ) {

		st[sr] = 0;

		dt[dr] = 0;

		MakeLinkRelative(st, src);

		if( !strcmp(st, dt) ) {

			return true;
		}
	}

	return false;
}

static bool CopyFile(string const &src, string const &dst, bool make)
{
	int fs = open(src.c_str(), O_RDONLY);

	if( fs >= 0 ) {

		if( !make ) {

			unlink(dst.c_str());
		}

		struct stat s;

		stat(src.c_str(), &s);

		int fd = open(dst.c_str(), O_CREAT | O_WRONLY, s.st_mode & 0x0FFF);

		if( fd >= 0 ) {

			uint64 t = GetTickCount();

			size_t c = 0;

			byte   buff[65536];

			for( ;;) {

				ssize_t r = read(fs, buff, sizeof(buff));

				if( r >= 0 ) {

					if( r == 0 ) {

						int dt = max(1, int(GetTickCount() - t));

						AfxTrace("%s file %s (%u bytes in %ums at %u kbps)\n", make ? "make" : "edit", dst.c_str(), c, dt, c / dt);

						m_b += c;

						close(fd);

						close(fs);

						return true;
					}

					write(fd, buff, r);

					c += r;

					continue;
				}

				break;
			}

			close(fd);
		}

		close(fs);
	}

	Error("failed to copy file %s to %s", src.c_str(), dst.c_str());

	return false;
}

static bool CopyLink(string const &src, string const &dst, bool make)
{
	char st[256];

	ssize_t sr = readlink(src.c_str(), st, sizeof(st));

	if( sr > 0 ) {

		st[sr] = 0;

		MakeLinkRelative(st, src);

		if( !make ) {

			unlink(dst.c_str());
		}

		if( symlink(st, dst.c_str()) == 0 ) {

			AfxTrace("%s link %s\n", make ? "make" : "edit", dst.c_str());

			return true;
		}
	}

	Error("failed to copy link %s to %s", src.c_str(), dst.c_str());

	return false;
}

static uint64 GetTickCount(void)
{
	struct timespec tv;

	clock_gettime(CLOCK_REALTIME, &tv);

	uint64 ms;

	ms  = tv.tv_sec  * uint64(1000);

	ms += tv.tv_nsec / uint64(1000 * 1000);

	return ms;
}

// End of File
