
#include "Intern.hpp"

int main(int nArg, char *pArg[])
{
	if( nArg == 2 ) {

		addrinfo hint = { 0 };

		addrinfo *res = NULL;

		hint.ai_family    = AF_INET;
		hint.ai_socktype  = SOCK_STREAM;
		hint.ai_flags     = AI_PASSIVE;
		hint.ai_protocol  = IPPROTO_TCP;

		getaddrinfo(pArg[1], NULL, &hint, &res);

		if( res ) {

			vector<UINT> list;

			for( addrinfo *walk = res; walk; walk = walk->ai_next ) {

				if( walk->ai_addr->sa_family == AF_INET ) {

					UINT a = ((UINT *) (walk->ai_addr->sa_data + 2))[0];

					list.push_back(a);
				}
			}
			
			freeaddrinfo(res);

			if( list.size() ) {

				char buff[256];

				inet_ntop(AF_INET, &list[rand() % list.size()], buff, sizeof(buff));

				printf("%s\n", buff);

				return 0;
			}
		}

		return 1;
	}

	fprintf(stderr, "usage: Resolve <hostname>\n");

	return 2;
}
