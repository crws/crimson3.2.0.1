
#pragma  once

#include "Intern.hpp"

#include <fstream>

typedef unsigned char  BYTE;

typedef unsigned short WORD;

typedef unsigned long  DWORD;

typedef BYTE * PBYTE;

typedef BYTE const * PCBYTE;

#define global
