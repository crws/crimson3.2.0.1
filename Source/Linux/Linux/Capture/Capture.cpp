
#include "Intern.hpp"

#include <pcap/pcap.h>

// Static Data

static	string		m_device;
static	string		m_filter;
static	string		m_output;
static	size_t		m_limit;
static	int		m_mode;
static	vector<BYTE>	m_data;
static	bool		m_stop;

// Prototypes

global	int	main(int nArg, char *pArg[]);
static	bool	ParseCommandLine(int nArg, char *pArg[]);
static	void	Error(char const *p, ...);
static	void	ShowUsage(void);
static	void	AddFileHeader(void);
static	void	PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData);
static	bool	ApplyFilter(pcap_t *p);
static	void	OnSignal(int sig);

// Code

global int main(int nArg, char *pArg[])
{
	m_mode = 1;

	if( ParseCommandLine(nArg, pArg) ) {

		signal(SIGTERM, OnSignal);

		m_output = "/tmp/crimson/pcap/" + m_device + ".pcap";

		m_limit  = 1024 * 1024;

		m_stop   = false;

		char err[PCAP_ERRBUF_SIZE];

		pcap_t *p = pcap_open_live(m_device.c_str(), 65536, false, 0, err);

		if( p ) {

			if( ApplyFilter(p) ) {

				AddFileHeader();

				pcap_setnonblock(p, 1, err);

				while( !m_stop ) {

					if( !pcap_dispatch(p, 1, PacketProc, nullptr) ) {

						usleep(10000);
					}
				}

				pcap_close(p);

				ofstream stm(m_output);

				if( stm.good() ) {

					stm.write((char *) m_data.data(), m_data.size());

					stm.close();

					return 0;
				}

				unlink(m_output.c_str());

				return 1;
			}

			pcap_close(p);
		}

		unlink(m_output.c_str());

		return 2;
	}

	return 3;
}

static bool ParseCommandLine(int nArg, char *pArg[])
{
	int c;

	while( (c = getopt(nArg, pArg, "i:f:m:")) != -1 ) {

		switch( c ) {

			case 'i':

				if( !optarg ) {

					Error("missing interface name");
				}

				m_device = strdup(optarg);

				break;

			case 'f':

				if( !optarg ) {

					Error("missing filter");
				}

				m_filter = strdup(optarg);

				break;

			case 'm':

				if( !optarg ) {

					Error("missing mode");
				}

				m_mode = atoi(optarg);

				break;

			case '?':

				Error("syntax error");

				break;

			default:

				Error("unexpected switch %c", c);

				break;
		}
	}

	if( m_device.empty() ) {

		Error("must specify interface");
	}

	return true;
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	fprintf(stderr, "%s: ", "Capture");

	vfprintf(stderr, p, v);

	fprintf(stderr, "\n");

	va_end(v);

	exit(1);
}

static void ShowUsage(void)
{
	fprintf(stderr, "usage: Capture...\n");
}

static void AddFileHeader(void)
{
	struct pcap_hdr
	{
		DWORD magic_number;   /* magic number */
		WORD  version_major;  /* major version number */
		WORD  version_minor;  /* minor version number */
		DWORD thiszone;       /* GMT to local correction */
		DWORD sigfigs;        /* accuracy of timestamps */
		DWORD snaplen;        /* max length of captured packets, in octets */
		DWORD network;        /* data link type */
	};

	int n = 1;

	switch( m_mode ) {

		case 1:
			n = 1;
			break;

		case 2:
			n = 101;
			break;
	}

	pcap_hdr h;

	h.magic_number  = 0xa1b2c3d4;
	h.version_major = 2;
	h.version_minor = 4;
	h.thiszone      = 0;
	h.sigfigs       = 0;
	h.snaplen       = 65536;
	h.network       = n;

	m_data.insert(m_data.end(), PCBYTE(&h), PCBYTE(&h+1));
}

static void PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData)
{
	if( m_data.size() < m_limit ) {

		struct pcaprec_hdr
		{
			DWORD ts_sec;         /* timestamp seconds */
			DWORD ts_usec;        /* timestamp microseconds */
			DWORD incl_len;       /* number of octets of packet saved in file */
			DWORD orig_len;       /* actual length of packet */
		};

		pcaprec_hdr h;

		timeval tv = { 0 };

		gettimeofday(&tv, NULL);

		h.ts_sec   = tv.tv_sec;
		h.ts_usec  = tv.tv_usec;
		h.incl_len = pInfo->caplen;
		h.orig_len = pInfo->len;

		m_data.insert(m_data.end(), PCBYTE(&h), PCBYTE(&h+1));

		m_data.insert(m_data.end(), pData, pData + pInfo->caplen);
	}
}

static bool ApplyFilter(pcap_t *p)
{
	bpf_program Program;

	string      Output;

	string	    Input(m_filter);

	map<string, string> Parts;

	while( !Input.empty() ) {

		size_t split = Input.find(',');

		string token = Input.substr(0, split);

		Input.erase(0, (split == string::npos) ? split : split+1);

		size_t equal = token.find('=');

		string prot  = token.substr(0, equal);

		string port  = (equal == string::npos) ? "" : token.substr(equal+1);

		if( prot == "udp" ) {

			Parts[prot] = (port == "all") ? "udp" : ("udp port " + port);
		}

		if( prot == "tcp" ) {

			if( port == "all" ) {

				// We use insert so that if 'web' has already set the filter, we don't overwrite.

				string web = "(tcp port 80) or (tcp port 443) or (tcp port 4443) or (tcp port 8080)";

				Parts.insert(make_pair(prot, "tcp and not (" + web + ")"));
			}
			else {
				Parts[prot] = "tcp port " + port;
			}
		}

		if( prot == "web" ) {

			Parts["tcp"] = "tcp";
		}

		if( prot == "icmp" || prot == "arp" ) {

			Parts.insert(make_pair(prot, prot));
		}
	}

	for( auto const &Part : Parts ) {

		if( !Output.empty() ) {

			Output += " or ";
		}

		Output += "(" + Part.second + ")";
	}

	if( pcap_compile(p, &Program, Output.c_str(), 1, 0) >= 0 ) {

		pcap_setfilter(p, &Program);

		pcap_freecode(&Program);

		return true;
	}

	return false;
}

static void OnSignal(int sig)
{
	m_stop = true;
}

// End of File
