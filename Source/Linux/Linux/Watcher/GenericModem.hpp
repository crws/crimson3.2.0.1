
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Interface.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CModemChannel;

//////////////////////////////////////////////////////////////////////////
//
// Generic Modem
//

class CGenericModem : public CInterface
{
public:
	// Constructor
	CGenericModem(string const &face, string const &root);

	// Destructor
	~CGenericModem(void);

protected:
	// Data Members
	unique_ptr<CModemChannel> m_cmd;

	// Implementation
	bool Okay(int &code, int exec);
	bool Test(int &code, int need, int exec);
	bool Okay(int &code, vector<string> &lines, char const *p, ...);
	bool Test(int &code, int need, vector<string> &lines, char const *p, ...);
	bool Okay(int &code, char const *p, ...);
	bool Test(int &code, int need, char const *p, ...);
	bool ParseReply(vector<string> &r, string const &s, size_t m);
	bool ParseReply(vector<int> &r, string const &s, size_t m);
	bool OkayAndParse(int &code, vector<string> &r, size_t m, char const *p, ...);
	bool OkayAndParse(int &code, vector<int> &r, size_t m, char const *p, ...);
};

// End of File
