
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../ATLib/ModemChannel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

// Hooks

extern CInterface * Create_Interface(string const &face, string const &root);

// Global Data

global	string	g_conf;

// Static Data

static	string	m_name  = "Watcher";

static	bool	m_fore  = false;

static	bool	m_run   = false;

static	bool	m_stop  = false;

static	bool	m_debug = false;

static	int	m_fp    = -1;

static	string	m_lock;

static	string	m_face;

static	string	m_conf;

static	string	m_root;

static	string	m_pipe;

// Prototypes

global int  WatchMain(int nArg, char *pArg[]);
global void AfxTrace(char const *p, ...);
global void AfxVTrace(char const *p, va_list v);
global void AfxTestPipe(void);
global bool IsUnderDebug(void);
static void CheckParent(void);
static void OnStopSignal(int sig);
static void OnPipeSignal(int sig);
static bool GetProcStatusEntry(string &data, string const &proc, string const &name);
static bool ParseCommandLine(int nArg, char *pArg[]);
static void Error(char const *p, ...);
static void ShowUsage(void);
static void MainLoop(void);
static bool TestLockFile(void);
static void MakeLockFile(void);
static void KillLockFile(void);
static void MakePipe(void);

// Code

global int WatchMain(int nArg, char *pArg[])
{
	CheckParent();

	m_name = pArg[0];

	m_name = m_name.substr(m_name.rfind('/')+1);

	if( m_debug ) {

		signal(SIGUSR1, OnStopSignal);

		signal(SIGPIPE, OnPipeSignal);
	}
	else {
		signal(SIGTERM, OnStopSignal);

		signal(SIGPIPE, OnPipeSignal);
	}

	if( ParseCommandLine(nArg, pArg) ) {

		if( !m_debug && !m_fore ) {

			AfxTrace("starting up and forking\n");

			if( fork() ) {

				return 0;
			}
		}

		if( TestLockFile() ) {

			MakeLockFile();

			MainLoop();

			KillLockFile();

			AfxTrace("exiting\n");

			return 0;
		}

		Error("already running");
	}

	ShowUsage();

	return 2;
}

global void AfxTrace(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	AfxVTrace(p, v);

	va_end(v);
}

global void AfxVTrace(char const *p, va_list v)
{
	AfxTestPipe();

	if( m_fp >= 0 ) {

		CPrintf s;

		s.VPrintf(p, v);

		ssize_t w = write(m_fp, s.data(), s.size());

		if( w < 0 ) {

			CModemChannel::SetGlobalTrace(false);

			close(m_fp);

			m_fp = -1;
		}
	}

	if( !m_debug ) {

		vsyslog(LOG_NOTICE, p, v);
	}

	vprintf(p, v);
}

global void AfxTestPipe(void)
{
	if( !m_pipe.empty() ) {

		if( m_fp < 0 ) {

			if( (m_fp = open(m_pipe.c_str(), O_WRONLY | O_NDELAY | O_CLOEXEC)) >= 0 ) {

				CModemChannel::SetGlobalTrace(true);
			}
		}
	}
}

global bool IsUnderDebug(void)
{
	return m_debug;
}

static void CheckParent(void)
{
	string ppid;

	if( GetProcStatusEntry(ppid, "self", "PPid") ) {

		string name;

		if( GetProcStatusEntry(name, ppid, "Name") ) {

			if( name == "gdbserver" || name == "gdb" ) {

				m_debug = true;
			}

			if( name == "InitC32" ) {

				m_fore = true;
			}
		}
	}
}

static void OnStopSignal(int sig)
{
	m_stop = true;
}

static void OnPipeSignal(int sig)
{
	signal(SIGPIPE, OnPipeSignal);
}

static bool GetProcStatusEntry(string &data, string const &proc, string const &name)
{
	ifstream stm(CPrintf("/proc/%s/status", proc.c_str()));

	if( stm.good() ) {

		while( !stm.eof() ) {

			string line;

			getline(stm, line);

			size_t n = line.find(':');

			if( n != string::npos ) {

				if( line.substr(0, n) == name ) {

					int d = line.find_first_not_of(" \t", n + 1);

					if( d != string::npos ) {

						int e = line.find_last_not_of(" \t\r\n");

						data = line.substr(d, e - d + 1);

						return true;
					}
				}
			}
		}
	}

	return false;
}

static bool ParseCommandLine(int nArg, char *pArg[])
{
	int c;

	while( (c = getopt(nArg, pArg, "fi:c:p:")) != -1 ) {

		switch( c ) {

			case 'f':

				m_fore = true;

				break;

			case 'i':

				if( !optarg ) {

					Error("missing interface name");
				}

				m_face = strdup(optarg);

				break;

			case 'c':

				if( !optarg ) {

					Error("missing config file");
				}

				m_conf = strdup(optarg);

				break;

			case 'p':

				if( !optarg ) {

					Error("missing lock file");
				}

				m_lock = strdup(optarg);

				break;

			case '?':

				Error("syntax error");

				break;

			default:

				Error("unexpected switch %c", c);

				break;
		}
	}

	if( m_face.empty() ) {

		Error("must specify interface");
	}

	if( m_conf.empty() ) {

		m_conf = "/vap/opt/crimson/config/net/" + m_face + g_conf + "-conf";
	}

	if( m_lock.empty() ) {

		m_lock = "/var/run/c3-" + m_face + "-watch.pid";
	}

	return true;
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	if( m_run ) {

		KillLockFile();
	}

	if( !m_debug ) {

		vsyslog(LOG_ERR, p, v);
	}

	fprintf(stderr, "%s: ", m_name.c_str());

	vfprintf(stderr, p, v);

	fprintf(stderr, "\n");

	va_end(v);

	exit(1);
}

static void ShowUsage(void)
{
	fprintf(stderr, "usage: %s -i <interface> [-c <config-file>] [-p <pid-file>]\n", m_name.c_str());
}

static void MainLoop(void)
{
	CInterface *pFace;

	m_root = "/tmp/crimson/face/" + m_face;

	m_pipe = m_root + "/monitor.pipe";

	MakePipe();

	if( (pFace = Create_Interface(m_face, m_root)) ) {

		if( pFace->Load(m_conf) ) {

			while( !pFace->IsStopped() ) {

				int c = pFace->Run();

				if( c ) {

					usleep(c * 1000);
				}

				if( m_stop ) {

					AfxTrace("termination signal\n");

					pFace->Stop();

					m_stop = false;
				}
			}

			if( m_fp >= 0 ) {

				close(m_fp);

				CModemChannel::SetGlobalTrace(false);
			}

			unlink(m_pipe.c_str());

			delete pFace;

			return;
		}

		delete pFace;

		Error("invalid configuration");
	}

	Error("invalid interface");
}

static bool TestLockFile(void)
{
	if( !m_fore && !m_lock.empty() ) {

		ifstream file(m_lock);

		if( file.good() ) {

			pid_t pid;

			file >> pid;

			// Kill with signal of zero is a test for the
			// existance of a valid process of that pid...

			int r;

			if( (r = kill(pid, 0)) == 0 ) {

				return false;
			}
		}

		file.close();

		KillLockFile();
	}

	return true;
}

static void MakeLockFile(void)
{
	if( !m_fore ) {

		ofstream(m_lock) << getpid() << '\n';
	}

	m_run = true;
}

static void KillLockFile(void)
{
	if( !m_fore ) {

		unlink(m_lock.c_str());
	}
}

static void MakePipe(void)
{
	// TODO -- Check if directory exists!!!

	system(("mkdir -p " + m_root).c_str());

	mkfifo(m_pipe.c_str(), 0666);
}

// End of File
