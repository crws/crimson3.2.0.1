
#include "Intern.hpp"

#include "GenericModem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../ATLib/ModemChannel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Modem
//

// Constructor

CGenericModem::CGenericModem(string const &face, string const &root) : CInterface(face, root)
{
}

// Destructor

CGenericModem::~CGenericModem(void)
{
}

// Implementation

bool CGenericModem::Okay(int &code, int exec)
{
	return (code = exec) == CModemChannel::codeOkay;
}

bool CGenericModem::Test(int &code, int need, int exec)
{
	return !m_stop && (code = exec) == need;
}

bool CGenericModem::Okay(int &code, vector<string> &lines, char const *p, ...)
{
	if( !m_stop ) {

		CPrintf s;

		va_list v;

		va_start(v, p);

		s.VPrintf(p, v);

		bool ok = Okay(code, m_cmd->Command(lines, s));

		va_end(v);

		return ok && !m_stop;
	}

	return false;
}

bool CGenericModem::Test(int &code, int need, vector<string> &lines, char const *p, ...)
{
	if( !m_stop ) {

		CPrintf s;

		va_list v;

		va_start(v, p);

		s.VPrintf(p, v);

		bool ok = Test(code, need, m_cmd->Command(lines, s));

		va_end(v);

		return ok && !m_stop;
	}

	return false;
}

bool CGenericModem::Okay(int &code, char const *p, ...)
{
	if( !m_stop ) {

		CPrintf s;

		va_list v;

		va_start(v, p);

		s.VPrintf(p, v);

		bool ok = Okay(code, m_cmd->Command(s));

		va_end(v);

		return ok && !m_stop;
	}

	return false;
}

bool CGenericModem::Test(int &code, int need, char const *p, ...)
{
	if( !m_stop ) {

		CPrintf s;

		va_list v;

		va_start(v, p);

		s.VPrintf(p, v);

		bool ok = Test(code, need, m_cmd->Command(s));

		va_end(v);

		return ok && !m_stop;
	}

	return false;
}

bool CGenericModem::ParseReply(vector<string> &r, string const &s, size_t m)
{
	r.clear();

	size_t p = s.find(':');

	if( p != string::npos ) {

		p = s.find_first_not_of(" /t", p+1);

		if( p != string::npos ) {

			string t = s.substr(p);

			size_t f = 0;

			if( !t.empty() ) {

				for( ;;) {

					if( (p = t.find(',', f)) == string::npos ) {

						r.push_back(t.substr(f));

						break;
					}

					r.push_back(t.substr(f, p-f));

					f = p + 1;
				}

				return r.size() >= m;
			}
		}
	}

	return false;
}

bool CGenericModem::ParseReply(vector<int> &r, string const &s, size_t m)
{
	r.clear();

	vector<string> x;

	if( ParseReply(x, s, m) ) {

		for( auto const &i : x ) {

			size_t n = i.size();

			if( n ) {

				if( i[0] == '(' ) {

					r.push_back(atoi(i.substr(1).c_str()));

					continue;
				}

				if( i[n-1] == ')' ) {

					r.push_back(atoi(i.substr(0, n-1).c_str()));

					continue;
				}

				r.push_back(atoi(i.c_str()));
			}
			else {
				r.push_back(0);
			}
		}

		return true;
	}

	return false;
}

bool CGenericModem::OkayAndParse(int &code, vector<string> &r, size_t m, char const *p, ...)
{
	CPrintf s;

	va_list v;

	va_start(v, p);

	s.VPrintf(p, v);

	vector<string> lines;

	if( Okay(code, m_cmd->Command(lines, s)) && !m_stop ) {

		for( size_t n = 1; n < lines.size(); n++ ) {

			size_t c = lines[n].find(':');

			if( c != string::npos ) {

				if( lines[n].substr(0, c) == string(p, c) ) {

					if( ParseReply(r, lines[n], m) ) {

						va_end(v);

						return true;
					}
				}
			}
		}
	}

	va_end(v);

	return false;
}

bool CGenericModem::OkayAndParse(int &code, vector<int> &r, size_t m, char const *p, ...)
{
	CPrintf s;

	va_list v;

	va_start(v, p);

	s.VPrintf(p, v);

	vector<string> lines;

	if( Okay(code, m_cmd->Command(lines, s)) && !m_stop ) {

		for( size_t n = 1; n < lines.size(); n++ ) {

			size_t c = lines[n].find(':');

			if( c != string::npos ) {

				if( lines[n].substr(0, c) == string(p, c) ) {

					if( ParseReply(r, lines[n], m) ) {

						va_end(v);

						return true;
					}
				}
			}
		}
	}

	va_end(v);

	return false;
}

// End of File
