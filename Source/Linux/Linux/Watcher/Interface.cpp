
#include "Intern.hpp"

#include "Interface.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Generic Interface
//

// Constructor

CInterface::CInterface(string const &face, string const &root) : m_face(face), m_root(root)
{
	m_status  = m_root + "/status";

	m_command = m_root + "/command";

	m_scripts = "/opt/crimson/scripts";

	m_sled    = -1;

	m_state   = stateNotPresent;

	m_prev    = stateNone;

	m_first   = true;

	m_loops   = 0;

	m_timer   = 0;

	m_count   = 0;

	m_stop    = false;

	m_ctime   = 0;

	m_mtime   = 0;

	m_dtime   = 0;

	m_rxbase  = 0;

	m_txbase  = 0;

	m_rxnow   = 0;

	m_txnow   = 0;
}

// Destructor

CInterface::~CInterface(void)
{
}

// Attributes

bool CInterface::IsStopped(void) const
{
	return m_state == stateStopped;
}

// Operations

bool CInterface::Load(string const &conf)
{
	ifstream stm(conf);

	if( stm.good() ) {

		while( !stm.eof() ) {

			string line;

			getline(stm, line);

			if( !line.empty() ) {

				if( line[0] != '#' ) {

					size_t n = line.find('=');

					if( n != string::npos ) {

						int d = line.find_first_not_of(" \t", n + 1);

						if( d != string::npos ) {

							int e = line.find_last_not_of(" \t\r\n");

							string key = line.substr(0, n);

							string val = line.substr(d, e - d + 1);

							m_config.insert(make_pair(key, val));
						}
					}
				}
			}
		}

		OnConfigure();

		return true;
	}

	return false;
}

int CInterface::Run(void)
{
	if( m_stop ) {

		AfxTrace("stop requested\n");

		m_stop = false;

		OnStopping();

		m_state = stateStopped;

		return 0;
	}

	if( true ) {

		CheckCommand();
	}

	if( m_prev != m_state ) {

		AfxTrace("%s switched to state %u\n", m_face.c_str(), m_state);

		OnNewState();

		m_timer = GetMonotonic();

		m_count = 0;

		m_prev  = m_state;
	}

	return OnExecute();
}

void CInterface::Stop(void)
{
	m_stop = true;
}

// Implementation

bool CInterface::PowerHardware(bool on)
{
	if( m_sled >= 0 ) {

		CPrintf cmd("sledctl %u %s", m_sled, on ? "on" : "off");

		AfxTrace("%s\n", cmd.c_str());

		System(cmd);

		return true;
	}

	return false;
}

bool CInterface::ResetHardware(void)
{
	if( m_sled >= 0 ) {

		CPrintf cmd("sledctl %u reset", m_sled);

		AfxTrace("%s\n", cmd.c_str());

		System(cmd);

		m_loops = 0;

		return true;
	}

	return false;
}

bool CInterface::SetLinkState(bool up)
{
	CPrintf cmd("ip link set dev %s %s", m_face.c_str(), up ? "up" : "down");

	AfxTrace("%s\n", cmd.c_str());

	return System(cmd);
}

bool CInterface::RunLinkStart(bool start, string const &arg)
{
	return RunLinkScript(start ? "start" : "stop", arg);
}

bool CInterface::RunLinkScript(string const &event, string const &arg)
{
	CPrintf cmd("%s/c3-if%s %s%s", m_scripts.c_str(), event.c_str(), m_face.c_str(), arg.c_str());

	AfxTrace("%s\n", cmd.c_str());

	return System(cmd);
}

bool CInterface::SetLinkMtu(int mtu)
{
	CPrintf cmd("ip link set dev %s mtu %u", m_face.c_str(), mtu);

	AfxTrace("%s\n", cmd.c_str());

	return System(cmd);
}

bool CInterface::System(string const &cmd)
{
	return !system((cmd + " > /dev/null 2>&1").c_str());
}

string CInterface::GetConfig(string const &key, char const *def)
{
	auto i = m_config.find(key);

	return (i == m_config.end()) ? def : i->second;
}

int CInterface::GetConfig(string const &key, int def)
{
	auto i = m_config.find(key);

	return (i == m_config.end()) ? def : atoi(i->second.c_str());
}

bool CInterface::GetConfig(string const &key, bool def)
{
	auto i = m_config.find(key);

	return (i == m_config.end()) ? def : (i->second == "1");
}

bool CInterface::CheckCommand(void)
{
	ifstream stm(m_command);

	if( stm.good() ) {

		string cmd;

		stm >> cmd;

		stm.close();

		OnCommand(cmd);

		unlink(m_command.c_str());

		return true;
	}

	return false;
}

void CInterface::ClearStatus(void)
{
	unlink(m_status.c_str());
}

string CInterface::GetStateName(void)
{
	switch( m_state ) {

		case stateNotPresent:
			return "Waiting for Hardware";

		case stateStopped:
			return "Stopped";
	}

	return CPrintf("State %u", m_state);
}

bool CInterface::AddTraffic(ofstream &stm)
{
	time_t test = GetMonotonic();

	if( test > m_dtime ) {

		for( int p = 0; p < 2; p++ ) {

			CPrintf cmd("iptables -t mangle --list %s -v -x -w", p ? "POSTROUTING" : "PREROUTING");

			FILE *file = popen(cmd, "r");

			if( file ) {

				string find = ' ' + m_face + ' ';

				for( ;;) {

					char line[256] = { 0 };

					fgets(line, sizeof(line), file);

					if( line[0] ) {

						if( strstr(line, find.c_str()) ) {

							size_t n1 = strspn(line, " ");

							size_t n2 = strcspn(line + n1, " ") + n1;

							size_t n3 = strspn(line + n2, " ") + n2;

							(p ? m_txnow : m_rxnow) = strtoll(line + n3, NULL, 10);

							break;
						}

						continue;
					}

					break;
				}

				fclose(file);
			}

			m_dtime = test;
		}
	}

	if( !m_mtime ) {

		m_mtime  = m_dtime;

		m_txbase = m_txnow;

		m_rxbase = m_rxnow;
	}

	stm << "\t\"mxtime\": " << m_mtime << ",\n";

	stm << "\t\"txbytes\": " << m_txnow - m_txbase << ",\n";

	stm << "\t\"rxbytes\": " << m_rxnow - m_rxbase << ",\n";

	return true;
}

time_t CInterface::GetMonotonic(void)
{
	timespec ts = { 0 };

	clock_gettime(CLOCK_MONOTONIC, &ts);

	return ts.tv_sec;
}

// End of File
