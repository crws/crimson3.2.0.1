
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Generic Interface
//

class CInterface
{
public:
	// Constructor
	CInterface(string const &face, string const &root);

	// Destructor
	virtual ~CInterface(void);

	// Attributes
	bool IsStopped(void) const;

	// Operations
	bool Load(string const &conf);
	int  Run(void);
	void Stop(void);

protected:
	// States
	enum {
		stateNone		= 0,
		stateNotPresent		= 1,
		stateStopped		= 100
	};

	// Data Members
	string			m_face;
	string			m_root;
	string			m_scripts;
	string			m_status;
	string			m_command;
	int			m_sled;
	bool			m_stop;
	map<string, string>     m_config;
	int			m_state;
	int			m_prev;
	bool			m_first;
	int			m_loops;
	time_t			m_timer;
	int			m_count;
	time_t			m_ctime;
	time_t			m_mtime;
	time_t			m_dtime;
	long long		m_rxbase;
	long long		m_txbase;
	long long		m_rxnow;
	long long		m_txnow;

	// Overridables
	virtual bool OnConfigure(void)		  = 0;
	virtual int  OnExecute(void)              = 0;
	virtual void OnNewState(void)		  = 0;
	virtual void OnStopping(void)             = 0;
	virtual bool OnCommand(string const &cmd) = 0;

	// Implementation
	bool   PowerHardware(bool on);
	bool   ResetHardware(void);
	bool   SetLinkState(bool up);
	bool   SetLinkMtu(int mtu);
	bool   RunLinkStart(bool start, string const &arg = "");
	bool   RunLinkScript(string const &event, string const &arg = "");
	bool   System(string const &cmd);
	string GetConfig(string const &key, char const *def);
	int    GetConfig(string const &key, int def);
	bool   GetConfig(string const &key, bool def);
	bool   CheckCommand(void);
	void   ClearStatus(void);
	string GetStateName(void);
	bool   AddTraffic(ofstream &stm);
	time_t GetMonotonic(void);
};

// End of File
