@echo off

call target win32

nmake -nologo -f %3\makefile %1 Config=%2 ProjectDir=%3 TargetPath=%4 ObjPath=%5
