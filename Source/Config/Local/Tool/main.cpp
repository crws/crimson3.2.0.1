
#include "intern.hpp"

// Translation

struct CTrans
{
	CString	m_From;
	CString m_To[4];
	BOOL    m_fUsed;
	};

// Typedefs

typedef CMap  <CString, CTrans *> CLex;

typedef CMap  <CString, CTrans *> CTry;

typedef CTree <CString>	          CSkip;

// Static Data

static	CLex		m_Lex;

static	CTry		m_Try;

static	CSkip		m_Skip;

static	UINT		m_uLang;

static	BOOL		m_fDrv;

static	CStringArray	m_Locs;

static	CString		m_File;

static	CString		m_Save;

static	CString		m_Type;

static	CString		m_Name;

static	CString		m_Key;

static	FILE *		m_pFile;

static	FILE *		m_pSave;

static	FILE *		m_pMaster;

static	UINT		m_fn = 0;

static	UINT		m_sn = 0;

// Prototypes

global	void	main(int nArg, char *pArgs[]);
static	void	LoadSkipList(void);
static	BOOL	LoadLexicons(CString Path);
static	BOOL	LoadLexicon(CString File);
static	UINT	Score(CTrans *pTrans);
static	BOOL	GetWide(FILE *pFile, CString &Line);
static	BOOL	GetLine(FILE *pFile, CString &Line);
static	void	PutLine(CString Line);
static	BOOL	MakeNarrow(CString &Line);
static	BOOL	Skip(CString Name);
static	INT64	FindLatestRcc(CString Path);
static	INT64	FindLatestLoc(void);
static	INT64	GetFileTime(CString File);
static	BOOL	ScanSource(CString Path);
static	BOOL	ScanFile(void);
static	BOOL	SkipType(PCTXT pType);
static	void	SaveMasterHeader(void);
static	void	SaveToMaster(CString Find, char cStatus, CTrans *pTrans);
static	BOOL	Lookup(CString Find, CString &Repl);
static	INT	Replace(CString &Line, UINT uPos, UINT uEnd);
static	void	ParseType(CString &Line);
static	void	ParseLine(CString &Line);
static	void	ParsePages(CString &Line);
static	void	ParseSchema(CString &Line, BOOL fWide);
static	INT	ParseField(CString &Line, UINT uPos, UINT uEnd);
static	void	ParsePage(CString &Line, BOOL fWide);
static	void	ParseDialog(CString &Line);
static	void	ParseMenu(CString &Line);
static	void	ParseAccel(CString &Line);
static	void	ParseString(CString &Line);
static	BOOL	IsHexEight(CString const &Text);
static	void	DumpUnused(void);

// Code

global	void	main(int nArg, char *pArgs[])
{
	if( nArg == 2 || nArg == 3 ) {

		INT64   TimeRcc = 1;

		INT64   TimeLoc = 0;

		char    cSep    = '\\';

		CString Path    = pArgs[0];

		CString Scan    = pArgs[1];

		if( Scan.Right(1)[0] == cSep ) {

			UINT uLen = Scan.GetLength();

			Scan.Delete(uLen - 1, 1);
			}

		if( nArg == 3 ) {

			FILE *pFile = fopen(pArgs[2], "rb");

			if( pFile ) {

				TimeRcc = FindLatestRcc(Scan + cSep);

				TimeLoc = FindLatestLoc();

				fclose(pFile);
				}
			}
		else {
			TimeRcc = FindLatestRcc(Scan + cSep);

			TimeLoc = FindLatestLoc();
			}

		if( TimeRcc > TimeLoc ) {

			UINT uPos1 = Path.FindRev(cSep);

			UINT uPos2 = Path.Left(uPos1-1).FindRev(cSep);

			UINT uPos3 = Path.Left(uPos2-1).FindRev(cSep); 

			Path = Path.Left(uPos3) + cSep + "data" + cSep;

			if( LoadLexicons(Path) ) {

				for( int n = 0; n < 20; n++ ) {

					if( (m_pMaster = fopen(Path + "out.txt", "wb")) ) {

						LoadSkipList();

						SaveMasterHeader();

						ScanSource(Scan + cSep);

						DumpUnused();

						fclose(m_pMaster);

						exit(0);
						}

					if( errno == EACCES ) {

						printf("local: retrying %s...\n", Path + "out.txt");

						Sleep(50);

						continue;
						}

					break;
					}

				printf("local: unable to open output file %s (%d)\n", Path + "out.txt", errno);

				exit(2);
				}

			printf("local: unable to open lexicon\n");

			exit(2);
			}

		exit(0);
		}

	printf("usage: local <path> {<flag-file>}\n");

	exit(1);
	}

static	void	LoadSkipList(void)
{
	m_Skip.Insert("%s");
	m_Skip.Insert("<caption>");
	m_Skip.Insert("0\\x00B0");
	m_Skip.Insert("CAPTION");
	m_Skip.Insert("MS Shell Dlg");
	m_Skip.Insert("false");
	m_Skip.Insert("true");
	m_Skip.Insert("ms");
	}

static	BOOL	LoadLexicons(CString Path)
{
	if( !LoadLexicon(Path + "lex.txt") ) {

		return FALSE;
		}
	
	if( !LoadLexicon(Path + "drv.txt") ) {

		return FALSE;
		}

	return TRUE;
	}

static	BOOL	LoadLexicon(CString File)
{
	FILE *pLex = fopen(File, "rb");

	if( pLex ) {

		if( getc(pLex) == 0xFF && getc(pLex) == 0xFE ) {

			CString Line;

			GetWide(pLex, Line);

			for(;;) {

				if( GetWide(pLex, Line) ) {

					CStringArray Part;

					Line.Tokenize(Part, '\t');

					BOOL fValid = FALSE;

					for( UINT n = 6; n < 11; n++ ) {

						if( Part[n][0] == '"' ) {

							CString Text = Part[n];

							UINT    uLen = Text.GetLength();

							if( Text[uLen-1] == '"' ) {

								Text = Text.Mid(1, uLen-2);
							
								Text.Replace("\"\"", "\\x0022");

								Part.SetAt(n, Text);
								}
							else
								printf("[%s]\n", Text);
							}

						if( !fValid ) {
						
							if( n >= 7 ) {

								if( Part[n].GetLength() ) {

									fValid = TRUE;
									}
								}
							}
						}

					if( fValid ) {

						if( Part[4][0] - 'G' ) {

							CString Key;

							Key += Part[0].ToLower();
							Key += '|';
							Key += Part[1];
							Key += '|';
							Key += Part[2];
							Key += '|';
							Key += Part[6];

							if( m_Lex.Failed(m_Lex.FindName(Key)) ) {

								CTrans *pTrans  = New CTrans;

								pTrans->m_From  = Part[6];
							
								pTrans->m_To[0] = Part[ 7];
								pTrans->m_To[1] = Part[ 8];
								pTrans->m_To[2] = Part[ 9];
								pTrans->m_To[3] = Part[10];

								pTrans->m_fUsed = FALSE;

								INDEX Find = m_Try.FindName(Part[6]);

								if( !m_Try.Failed(Find) ) {

									CTrans *pLast = m_Try.GetData(Find);

									if( Score(pTrans) > Score(pLast) ) {

										m_Try.SetData(Find, pTrans);
										}
									}
								else
									m_Try.Insert(Part[6], pTrans);
								
								m_Lex.Insert(Key, pTrans);
								}
							}
						}

					continue;
					}

				break;
				}

			fclose(pLex);

			return TRUE;
			}

		fclose(pLex);
		}

	return FALSE;
	}

static	UINT	Score(CTrans *pTrans)
{
	UINT s = 0;

	for( UINT n = 0; n < 4; n++ ) {

		if( pTrans->m_To[n].GetLength() ) {

			s++;
			}
		}

	return s;
	}

static	BOOL	GetWide(FILE *pFile, CString &Line)
{
	PTXT pLine = PTXT(_alloca(8192));

	PTXT pScan = pLine;

	PTXT pHex  = "0123456789ABCDEF";

	for(;;) {
		
		int lo = getc(pFile);

		int hi = getc(pFile);

		if( hi == EOF || lo == EOF ) {

			return FALSE;
			}

		if( hi || lo > 127 ) {

			*pScan++ = '\\';
			*pScan++ = 'x';
			*pScan++ = pHex[(hi>>4)&15];
			*pScan++ = pHex[(hi>>0)&15];
			*pScan++ = pHex[(lo>>4)&15];
			*pScan++ = pHex[(lo>>0)&15];
			}
		else {
			if( lo == 0x0D ) {

				continue;
				}

			if( lo == 0x0A ) {

				*pScan++ = 0;

				break;
				}

			*pScan++ = lo;
			}
		}

	Line = pLine;

	return TRUE;
	}

static	BOOL	GetLine(FILE *pFile, CString &Line)
{
	PTXT pLine = PTXT(_alloca(8192));

	PTXT pScan = pLine;

	PTXT pHex  = "0123456789ABCDEF";

	for(;;) {
		
		int lo = getc(pFile);

		int hi = 0;

		if( lo == EOF ) {

			return FALSE;
			}

		if( lo > 127 ) {

			*pScan++ = '\\';
			*pScan++ = 'x';
			*pScan++ = pHex[(hi>>4)&15];
			*pScan++ = pHex[(hi>>0)&15];
			*pScan++ = pHex[(lo>>4)&15];
			*pScan++ = pHex[(lo>>0)&15];
			}
		else {
			if( lo == 0x0D ) {

				continue;
				}

			if( lo == 0x0A ) {

				*pScan++ = 0;

				break;
				}

			*pScan++ = lo;
			}
		}

	Line = pLine;

	return TRUE;
	}

static	void	PutHead(void)
{
	switch( m_uLang ) {

		case 0:
			PutLine("LANGUAGE LANG_FRENCH, SUBLANG_FRENCH\n");
			break;

		case 1:
			PutLine("LANGUAGE LANG_GERMAN, SUBLANG_GERMAN\n");
			break;

		case 2:
			PutLine("LANGUAGE LANG_SPANISH, SUBLANG_SPANISH_MODERN\n");
			break;

		case 3:
			PutLine("LANGUAGE LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED\n");
			break;
		}
	}

static	void	PutLine(CString Line)
{
	Line.Replace("\t\t\t", " ");
	Line.Replace("\t\t",   " ");
	Line.Replace("\t",     " ");

	fprintf(m_pSave, "%s\n", Line);
	}

static	BOOL	MakeNarrow(CString &Line)
{
	UINT uPos = 0;

	for(;;) {

		uPos = Line.Find("\\x", uPos);

		if( uPos < NOTHING ) {

			if( Line[uPos+2] == '0' && Line[uPos+3] == '0' ) {

				Line.Delete(uPos+2, 2);

				uPos = uPos + 4;

				continue;
				}

			if( m_uLang < 3 ) {

				if( Line[uPos+2] == 0x32 && Line[uPos+3] == 0x30 ) {

					// EN DASH

					return TRUE;
					}

				printf("local: unexpected unicode in non-Chinese");

				exit(2);
				}

			return FALSE;
			}

		break;
		}

	return TRUE;
	}

static	BOOL	Skip(CString Name)
{
	PCTXT pSkip[] = {	"testing",
				"client",
				"version",
				"utils",
				"noloc"
				};

	for( UINT n = 0; n < elements(pSkip); n++ ) {

		if( Name.Find(pSkip[n]) < NOTHING ) {

			return TRUE;
			}
		}

	return FALSE;
	}

static	INT64	FindLatestRcc(CString Path)
{
	INT64 Latest = 0;

	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(Path + "*.*", &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			if( !(Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {

				CString Name  = Path + Data.cFileName;

				CString Match = ".rcc";

				Name.MakeLower();

				if( Name.Right(Match.GetLength()) == Match ) {

					if( !Skip(Name) ) {

						INT64 Time = GetFileTime(Name);

						if( Time > Latest ) {

							Latest = Time;
							}

						CString Loc;

						Loc = Name.Left(Name.GetLength() - Match.GetLength());

						Loc = Loc + ".loc";

						m_Locs.Append(Loc);
						}
					}
				}
			else {
				if( Data.cFileName[0] != '.' ) {

					CString Name = Path + Data.cFileName + '\\';

					INT64   Time = FindLatestRcc(Name);

					if( Time > Latest ) {

						Latest = Time;
						}
					}
				}

			} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
		}

	return Latest;
	}

static	INT64	FindLatestLoc(void)
{
	INT64 Latest = 0;

	for( UINT n = 0; n < m_Locs.GetCount(); n++ ) {

		INT64 Time = GetFileTime(m_Locs[n]);

		if( !Time ) {

			return 0;
			}

		if( Time > Latest ) {

			Latest = Time;
			}
		}

	return Latest;
	}

static	INT64	GetFileTime(CString File)
{
	HANDLE hFile = CreateFile( File,
				   GENERIC_READ,
				   FILE_SHARE_READ,
				   NULL,
				   OPEN_EXISTING,
				   0,
				   NULL
				   );

	if( hFile != INVALID_HANDLE_VALUE ) {

		FILETIME t;

		GetFileTime(hFile, NULL, NULL, &t);

		CloseHandle(hFile);

		return t.dwLowDateTime + (INT64(t.dwHighDateTime) << 32);
		}

	return 0;
	}

static	BOOL	ScanSource(CString Path)
{
	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(Path + "*.*", &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			if( !(Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {

				CString Name = Path + Data.cFileName;

				CString Match  = ".rcc";

				Name.MakeLower();

				if( Name.Right(Match.GetLength()) == Match ) {

					if( !Skip(Name) ) {

						m_File = Data.cFileName;

						m_Save = m_File.Left(m_File.GetLength() - Match.GetLength());

						m_Save = m_Save + ".loc";

						m_fDrv = FALSE;

						if( Path.Right(8) == "c3comms\\" ) {
								
							if( m_File.Left(7) != "c3comms" ) {

								m_fDrv = TRUE;
								}
							}

						if( (m_pSave = fopen(Path + m_Save, "wb")) ) {

							for( m_uLang = 0; m_uLang < 4; m_uLang++ ) {

								if( (m_pFile = fopen(Name, "rb")) ) {
		
									if( m_uLang == 0 ) {
										
										printf("Localizing %s...\n", Data.cFileName);
										}

									PutHead();

									ScanFile();

									fclose(m_pFile);
									}
								}

							PutLine("LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US");

							fclose(m_pSave);
							}
						}
					}
				}
			else {
				if( Data.cFileName[0] != '.' ) {

					CString Name = Path + Data.cFileName + '\\';

					if( !ScanSource(Name) ) {

						return FALSE;
						}
					}
				}

			} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
		}

	return TRUE;
	}

static	BOOL	ScanFile(void)
{
	CString Line   = "";

	int	nDepth = 0;

	while( GetLine(m_pFile, Line) ) {

		CString Text;

		Text = Line;

		Text.TrimBoth();

		if( Text.IsEmpty() ) {

			if( !nDepth ) {
				
				m_Type.Empty();
				}

			continue;
			}

		if( Text[0] == '/' && Text[1] == '/' ) {

			if( !nDepth ) {
				
				m_Type.Empty();
				}

			continue;
			}

		if( Text[0] == '#' ) {

			if( Text.Right(6) == "//KEEP" ) {

				PutLine(Line);
				}

			if( !nDepth ) {
				
				m_Type.Empty();
				}

			continue;
			}

		if( Text == "BEGIN" ) {

			if( !nDepth++ ) {

				m_fn = 0;

				m_sn = 1;
				}

			PutLine(Line);

			continue;
			}

		if( Text == "END" ) {

			if( !--nDepth ) {

				m_Type.Empty();

				PutLine(Line);

				PutLine("");
				}
			else
				PutLine(Line);

			continue;
			}

		if( Text.Left(8) == "LANGUAGE" ) {

			continue;
			}

		if( nDepth == 0 ) {

			if( m_Type.IsEmpty() ) {

				Text.TrimBoth();

				m_Name = Text.StripToken(" \t");

				Text.TrimBoth();

				m_Type = Text.StripToken(" \t");

				Text.TrimBoth();

				if( m_Type.IsEmpty() ) {

					m_Type = m_Name;

					m_Name.Empty();
					}

				m_Key.Empty();

				m_Key += m_File.ToLower();
				m_Key += '|';
				m_Key += m_Type;
				m_Key += '|';
				m_Key += m_Name;
				m_Key += '|';
				}

			if( !SkipType(m_Type) ) {

				m_sn = 0;

				ParseType(Line);

				PutLine(Line);
				}

			continue;
			}

		ParseLine(Line);

		PutLine(Line);
		}

	return TRUE;
	}

static	BOOL	SkipType(PCTXT pType)
{
	return !strcmp(pType, "RT_MANIFEST") ||
	       !strcmp(pType, "ICON"       ) ||
	       !strcmp(pType, "BITMAP"     ) ||
	       !strcmp(pType, "CURSOR"     ) ||
	       !strcmp(pType, "SFCAT"      ) ||
	       !strcmp(pType, "JPG"        ) ||
	       FALSE;
	}

static	BOOL	Strip(CString &Line, UINT &uPos, UINT &uEnd)
{
	BOOL fEdit;

	do {
		fEdit = FALSE;

		while( uEnd > uPos && isspace(Line[uPos+0]) ) {

			uPos += 1;

			fEdit = TRUE;
			}

		while( uEnd > uPos && isspace(Line[uEnd-1]) ) {

			uEnd -= 1;

			fEdit = TRUE;
			}

		if( uEnd > uPos ) {

			if( Line[uEnd-1] == ':' ) {

				uEnd -= 1;

				fEdit = TRUE;
				}
			}

		if( uEnd > uPos ) {

			if( Line[uEnd-3] == '.' && Line[uEnd-2] == '.' && Line[uEnd-1] == '.' ) {

				uEnd -= 3;

				fEdit = TRUE;
				}
			}

		if( uEnd > uPos ) {

			if( Line[uEnd-2] == '\\' && Line[uEnd-1] == 'r' ) {

				uEnd -= 2;

				fEdit = TRUE;
				}
			}

		if( uEnd > uPos ) {

			if( Line[uPos+0] == '*' && Line[uPos+1] == ' ' ) {

				uPos += 2;

				fEdit = TRUE;
				}
			}

		if( uEnd > uPos ) {

			if( Line[uPos+0] == '-' || Line[uPos+0] == '+' ) {

				uPos += 1;

				fEdit = TRUE;
				}
			}

		} while( fEdit );

	return uEnd > uPos;
	}

static	void	SaveMasterHeader(void)
{
	putc(0xFF, m_pMaster);

	putc(0xFE, m_pMaster);

	fwprintf( m_pMaster,
		  L"%S\t%S\t%S\t%S\t%S\t%S\t%S\t%S\t%S\t%S\t%S\r\n",
		  "File",
		  "Type",
		  "Name",
		  "Seq",
		  "Stat",
		  "Set",
		  "en-us",
		  "fr",
		  "de",
		  "es",
		  "zh-cn"
		  );
	}

static	void	SaveToMaster(CString Find, char cStatus, CTrans *pTrans)
{
	if( m_uLang == 0 ) {

		fwprintf( m_pMaster,
			  L"%S\t%S\t%S\t%u\t%c\t%c\t%S",
			  m_File,
			  m_Type,
			  m_Name,
			  m_sn,
			  cStatus,
			  m_fDrv ? 'D' : 'C',
			  Find
			  );

		if( pTrans ) {

			for( UINT n = 0; n < 4; n++ ) {

				putc(9, m_pMaster);

				putc(0, m_pMaster);

				PTXT p = PTXT(PCTXT(pTrans->m_To[n]));

				while( *p ) {

					if( p[0] == '\\' && p[1] == 'x' ) {

						p += 2;

						////////

						char t = p[4];

						p[4]   = 0;

						WORD w = WORD(strtoul(p, NULL, 16));

						p[4]   = t;

						////////

						p += 4;

						putc(LOBYTE(w), m_pMaster);

						putc(HIBYTE(w), m_pMaster);

						continue;
						}

					putc(*p++, m_pMaster);

					putc(0,    m_pMaster);
					}
				}
			}

		fwprintf( m_pMaster,
			  L"\r\n"
			  );

		m_sn++;
		}
	}

static	BOOL	Lookup(CString Find, CString &Repl)
{
	CString Key    = m_Key + Find;

	CTrans *pTrans = NULL;

	INDEX   ndx    = NULL;

	if( !m_Lex.Failed(ndx = m_Lex.FindName(Key)) ) {

		pTrans = m_Lex.GetData(ndx);

		Repl   = pTrans->m_To[m_uLang];

		if( Repl.GetLength() ) {

			pTrans->m_fUsed = TRUE;

			SaveToMaster(Find, 'F', pTrans);

			return TRUE;
			}

		return FALSE;
		}

	if( !m_Try.Failed(ndx = m_Try.FindName(Find)) ) {

		pTrans = m_Try.GetData(ndx);

		Repl   = pTrans->m_To[m_uLang];

		if( Repl.GetLength() ) {

			pTrans->m_fUsed = TRUE;

			SaveToMaster(Find, 'G', pTrans);

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

static	INT	Replace(CString &Line, UINT uPos, UINT uEnd)
{
	if( uEnd > uPos ) {

		if( Strip(Line, uPos, uEnd) ) {

			UINT    uLen = uEnd - uPos;

			CString Find = Line.Mid(uPos, uLen);

			CString Repl = "";

			if( Lookup(Find, Repl) ) {

				Line.Delete(uPos, uLen);

				Line.Insert(uPos, Repl);

				INT nAdj = Repl.GetLength() - uLen;

				return nAdj;
				}

			if( Find.Find(',') < NOTHING ) {

				UINT uSep = uPos + Find.Find(',');

				if( Strip(Line, uPos, uSep) ) {

					UINT    uLen = uSep - uPos;

					INT     nAdj = 0;

					CString Find = Line.Mid(uPos, uLen);

					CString Repl = "";

					if( Lookup(Find, Repl) ) {

						Line.Delete(uPos, uLen);

						Line.Insert(uPos, Repl);

						nAdj = Repl.GetLength() - uLen;

						uSep = uSep + nAdj;

						uEnd = uEnd + nAdj;

						uPos = uSep + 1;

						nAdj = nAdj + Replace(Line, uPos, uEnd);

						return nAdj;
						}
					}
				}

			if( m_uLang == 0 ) {

				if( uLen > 1 ) {

					for( UINT n = 0; Find[n]; n++ ) {

						if( isalpha(Find[n]) ) {

							if( Find.Find("/* NOT USED") < NOTHING ) {

								return 0;
								}

							if( !m_Skip.Failed(m_Skip.Find(Find)) ) {

								return 0;
								}

							if( Find.Left(7) == "http://" ) {

								return 0;
								}

							if( Find[0] == '<' && Find[uLen-1] == '>' ) {

								return 0;
								}

							SaveToMaster(Find, 'M', NULL);

							return 0;
							}
						}
					}
				}
			}
		}

	return 0;
	}

static	void	ParseType(CString &Line)
{
	if( m_Type == "DIALOG" ) {

		UINT uPos = Line.Find('"');

		if( uPos < NOTHING ) {

			UINT uEnd = Line.Find('"', uPos+1);

			if( uEnd < NOTHING ) {

				Line.Insert(uPos, 'L');

				uPos += 2;

				uEnd += 1;

				uEnd += Replace(Line, uPos, uEnd);
				}
			}
		}
	}

static	void	ParseLine(CString &Line)
{
	if( m_Type == "RCDATA" ) {

		UINT uPos = m_Name.Find('_');

		if( uPos < NOTHING ) {

			CString Suffix = m_Name.Mid(uPos + 1);

			if( Suffix == "Pages" ) {

				ParsePages(Line);

				return;
				}

			if( Suffix == "Schema" ) {

				ParseSchema(Line, TRUE);

				return;
				}

			if( Suffix.Left(4) == "Page" ) {

				ParsePage(Line, TRUE);

				return;
				}
			}
		else {
			if( m_Name.Right(6) == "UIList" ) {

				ParseSchema(Line, FALSE);

				return;
				}

			if( m_Name.Right(6) == "UIPage" ) {

				ParsePage(Line, FALSE);

				return;
				}
			}

		return;
		}

	if( m_Type == "DIALOG" ) {

		ParseDialog(Line);

		return;
		}

	if( m_Type == "STRINGTABLE" ) {

		ParseString(Line);

		return;
		}

	if( m_Type == "MENU" ) {

		ParseMenu(Line);
		
		return;
		}
	}

static	void	ParsePages(CString &Line)
{
	UINT uPos = 0;

	UINT uEnd = Line.GetLength();

	INT  nAdj = 0;

	if( Line[uEnd-1] == ',' ) {

		uEnd -= 1;
		}

	while( isspace(Line[uPos]) ) {

		uPos += 1;
		}

	if( Line[uPos+0] == 'L' && Line[uPos+1] == '"' ) {

		uPos += 2;

		uEnd  = Line.FindRev('"');

		if( Line[uEnd-2] == '\\' && Line[uEnd-1] == '0' ) {

			uEnd -= 2;
			}

		if( uPos < uEnd ) {

			for(;;) {

				UINT uSep = Line.Find(',', uPos);

				if( uSep == NOTHING ) {

					nAdj = Replace(Line, uPos, uEnd);

					uEnd = uEnd + nAdj;

					break;
					}

				nAdj = Replace(Line, uPos, uSep);

				uSep = uSep + nAdj;

				uEnd = uEnd + nAdj;

				uPos = uSep + 1;
				}
			}
		}
	}

static	void	ParseSchema(CString &Line, BOOL fWide)
{
	CString Keep = Line;

	UINT    uPos = 0;

	UINT    uEnd = Line.GetLength();

	INT     nAdj = 0;

	BOOL    fEnd = FALSE;

	if( Line[uEnd-1] == ',' ) {

		uEnd -= 1;
		}

	while( isspace(Line[uPos]) ) {

		uPos += 1;
		}

	if( fWide ) {

		if( Line[uPos] == 'L' ) {

			uPos += 1;
			}
		}

	if( Line[uPos] == '"' ) {

		uPos += 1;

		uEnd  = Line.FindRev('"');

		if( Line[uEnd-2] == '\\' && Line[uEnd-1] == '0' ) {

			uEnd -= 2;

			fEnd  = TRUE;
			}

		if( uPos < uEnd ) {

			while( m_fn < 5 ) {

				UINT uSep = Line.Find(',', uPos);

				if( uSep == NOTHING ) {

					nAdj = ParseField(Line, uPos, uEnd);

					uEnd = uEnd + nAdj;

					break;
					}

				nAdj = ParseField(Line, uPos, uSep);

				uSep = uSep + nAdj;

				uEnd = uEnd + nAdj;

				uPos = uSep + 1;

				m_fn = m_fn + 1;
				}

			if( m_fn == 5 ) {

				if( uPos < uEnd ) {

					nAdj = Replace(Line, uPos, uEnd);

					uEnd = uEnd + nAdj;
					}
				}
			}

		if( fEnd ) {

			m_fn = 0;
			}
		}

	if( !fWide && !MakeNarrow(Line) ) {

		Line = Keep;
		}
	}

static	INT	ParseField(CString &Line, UINT uPos, UINT uEnd)
{
	if( m_fn == 1 || m_fn == 2 ) {

		return Replace(Line, uPos, uEnd);
		}

	if( m_fn == 4 ) {

		INT nAdj = 0;

		INT nTot = 0;

		for(;;) {

			UINT uSep = Line.Find('|', uPos);

			if( uSep == NOTHING ) {

				if( Line.Mid(uPos,2) != "i=" && Line.Mid(uPos,5) != "Data=" ) {
	
					nAdj = Replace(Line, uPos, uEnd);

					nTot = nTot + nAdj;

					uEnd = uEnd + nAdj;
					}

				break;
				}

			if( Line.Mid(uPos,2) != "i=" && Line.Mid(uPos,5) != "Data=" ) {

				nAdj = Replace(Line, uPos, uSep);

				nTot = nTot + nAdj;

				uSep = uSep + nAdj;

				uEnd = uEnd + nAdj;
				}

			uPos = uSep + 1;
			}

		return nTot;
		}

	return 0;
	}

static	void	ParsePage(CString &Line, BOOL fWide)
{
	CString Keep = Line;

	UINT    uPos = 0;

	UINT    uEnd = Line.GetLength();

	INT     nAdj = 0;

	if( Line[uEnd-1] == ',' ) {

		uEnd -= 1;
		}

	while( isspace(Line[uPos]) ) {

		uPos += 1;
		}

	if( fWide ) {

		if( Line[uPos] == 'L' ) {

			uPos += 1;
			}
		}

	if( Line[uPos] == '"' ) {

		uPos += 1;

		uEnd  = Line.FindRev('"');

		if( Line[uEnd-2] == '\\' && Line[uEnd-1] == '0' ) {

			uEnd -= 2;
			}

		if( uPos < uEnd ) {

			if( Line[uPos+1] == ':' ) {

				if( Line[uPos+0] == 'G' ) {

					uPos += 2;

					for( UINT n = 0; n < 100; n++ ) {

						UINT uSep = Line.Find(',', uPos);

						if( uSep == NOTHING ) {

							if( n == 2 ) {

								nAdj = Replace(Line, uPos, uEnd);

								uEnd = uEnd + nAdj;
								}

							break;
							}

						if( n == 2 ) {

							nAdj = Replace(Line, uPos, uSep);

							uSep = uSep + nAdj;

							uEnd = uEnd + nAdj;
							}

						uPos = uSep + 1;
						}

					if( !fWide && !MakeNarrow(Line) ) {

						Line = Keep;
						}

					return;
					}

				if( Line[uPos+0] == 'T' ) {

					uPos += 2;

					for( UINT n = 0; n < 100; n++ ) {

						UINT uSep = Line.Find(',', uPos);

						if( uSep == NOTHING ) {

							if( n >= 1 ) {

								nAdj = Replace(Line, uPos, uEnd);

								uEnd = uEnd + nAdj;
								}

							break;
							}

						if( n >= 1 ) {

							nAdj = Replace(Line, uPos, uSep);

							uSep = uSep + nAdj;

							uEnd = uEnd + nAdj;
							}

						uPos = uSep + 1;
						}

					if( !fWide && !MakeNarrow(Line) ) {

						Line = Keep;
						}

					return;
					}
				}
			}
		}
	}

static	void	ParseDialog(CString &Line)
{
	UINT uPos = Line.Find('"');

	if( uPos < NOTHING ) {

		UINT uEnd = Line.Find('"', uPos+1);

		if( uEnd < NOTHING ) {

			Line.Insert(uPos, 'L');

			uPos += 2;

			uEnd += 1;

			if( Line[uPos] == '|' ) {

				uPos++;
				}

			Replace(Line, uPos, uEnd);

			ParseAccel(Line);
			}
		}
	}

static	void	ParseMenu(CString &Line)
{
	UINT    uPos = Line.FindNot(" \t");

	UINT    uEnd = Line.FindOne(" \t", uPos);

	INT     nAdj = 0;

	CString Word = Line.Mid(uPos, uEnd - uPos);

	if( Word == "POPUP" ) {

		UINT    uPos = Line.FindNot(" \t", uEnd);

		UINT    uEnd = Line.GetLength() - 1;

		CString Next = Line.Mid(uPos, uEnd - uPos);

		if( Next == "SEPARATOR" ) {

			return;
			}

		if( Next[0] == '"' ) {

			Line.Insert(uPos, 'L');

			uPos += 2;

			uEnd  = Line.Find('"', uPos);

			Next  = Line.Mid(uPos, uEnd - uPos);

			if( Next == "FCM" || Next == "CCM" ) {

				return;
				}

			if( IsHexEight(Next) ) { 

				return;
				}

			if( Line.Find('|', uPos) < NOTHING ) {

				for( UINT n = 0; n < 3; n++ ) {

					UINT uSep = Line.Find('|', uPos);

					if( uSep == NOTHING ) {

						if( n == 1 || n == 2 ) {

							nAdj = Replace(Line, uPos, uEnd);

							uEnd = uEnd + nAdj;
							}

						break;
						}

					if( n == 1 || n == 2 ) {

						nAdj = Replace(Line, uPos, uSep);

						uSep = uSep + nAdj;

						uEnd = uEnd + nAdj;
						}

					uPos = uSep + 1;
					}

				return;
				}

			Replace(Line, uPos, uEnd);

			ParseAccel(Line);
			}

		return;
		}

	if( Word == "MENUITEM" ) {

		UINT    uPos = Line.FindNot(" \t", uEnd);

		UINT    uEnd = Line.GetLength() - 1;

		CString Next = Line.Mid(uPos, uEnd - uPos);

		if( Next == "SEPARATOR" ) {

			return;
			}

		if( Next[0] == '"' ) {

			Line.Insert(uPos, 'L');

			uPos += 2;

			uEnd  = Line.Find('"', uPos);

			Next  = Line.Mid(uPos, uEnd - uPos);

			if( IsHexEight(Next) ) { 

				return;
				}

			if( Next.GetLength() == 5 ) {

				if( Next.Left(4) == "LANG" ) {

					return;
					}
				}

			if( Next.Right(3) == "\\t>" ) {

				uEnd -= 3;
				}

			Replace(Line, uPos, uEnd);

			ParseAccel(Line);
			}

		return;
		}
	}

static	void	ParseAccel(CString &Line)
{
	if( m_uLang == 3 ) {

		UINT uPos = Line.Find('(');

		UINT uEnd = Line.Find(')', uPos);

		if( uEnd == uPos + 2 ) {

			if( isupper(Line[uPos+1]) || isdigit(Line[uPos+1]) ) {

				Line.Insert(uPos+1, '&');
				}
			}
		}
	}

static	void	ParseString(CString &Line)
{
	UINT uPos = Line.Find('"');

	if( uPos < NOTHING ) {

		UINT uEnd = Line.FindRev('"');

		INT  nAdj = 0;

		if( uEnd < NOTHING ) {

			Line.Insert(uPos, 'L');

			uPos += 2;

			uEnd += 1;

			if( Line[uPos] == '[' ) {

				uPos += 1;

				uEnd  = Line.Find(']', uPos);

				nAdj  = Replace(Line, uPos, uEnd);

				uEnd  = uEnd + nAdj;
				
				uPos  = uEnd + 1;

				uEnd  = Line.FindRev('"');
				}

			Replace(Line, uPos, uEnd);

			ParseAccel(Line);
			}
		}
	}

static	BOOL	IsHexEight(CString const &Text)
{
	if( Text.GetLength() == 8) {

		for( UINT n = 0; n < 8; n++ ) {

			if( !isxdigit(Text[n]) ) {

				break;
				}
			}

		if( n == 8 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

static	void	DumpUnused(void)
{
	m_uLang = 0;

	INDEX n = m_Lex.GetHead();

	while( !m_Lex.Failed(n) ) {

		CTrans *pTrans = m_Lex.GetData(n);

		if( !pTrans->m_fUsed ) {

			CString Key = m_Lex.GetName(n);

			CStringArray Part;

			Key.Tokenize(Part, '|');

			m_File = Part[0];
			
			m_Type = Part[1];
			
			m_Name = Part[2];

			SaveToMaster(pTrans->m_From, 'U', pTrans);
			}

		m_Lex.GetNext(n);
		}
	}

// End of File
