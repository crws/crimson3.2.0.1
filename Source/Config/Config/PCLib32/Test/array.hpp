
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ARRAY_HPP
	
#define	INCLUDE_ARRAY_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE TEXT(__FILE__)

/////////////////////////////////////////////////////////////////////////
//
// Array Collection
//

template <typename CData> class CArray
{
	public:
		// Constructors
		CArray(void);
		CArray(CArray const &That);
		CArray(CData const &e1);
		CArray(CData const &e1, CData const &e2);
		CArray(CData const &e1, CData const &e2, CData const &e3);
		CArray(CData const &e1, CData const &e2, CData const &e3, CData const &e4);

		// Destructor
		~CArray(void);

		// Initialization
		void Init(PBYTE pData, UINT uCount, UINT uLimit);

		// Assignment
		CArray const & operator = (CArray const &That);

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetCount(void) const;
		UINT GetLimit(void) const;

		// Lookup Operator
		CData const & operator [] (UINT uIndex) const;

		// Data Read
		CData const & GetAt(UINT uIndex) const;

		// Data Write
		void SetAt(UINT uIndex, CData const &Data);
		void SetAt(UINT uIndex, CArray const &Array);
		void SetAt(UINT uIndex, CData const *pData, UINT uCount);

		// Data Pointer
		CData const * GetPointer(void) const;

		// Core Operations
		void Empty(void);
		UINT Append(CData const &Data);
		UINT Append(CArray const &Array);
		UINT Append(CData const *pData, UINT uCount);
		UINT Insert(UINT uIndex, CData const &Data);
		UINT Insert(UINT uIndex, CArray const &Array);
		UINT Insert(UINT uIndex, CData const *pData, UINT uCount);
		void Remove(UINT uIndex, UINT uCount);
		void Remove(UINT uIndex);

		// Other Operations
		void SetDelta(UINT uDelta);
		void SetCount(UINT uCount);
		void Expand(UINT uLimit);
		void Compress(void);
		void Shuffle(UINT uSeed);
		void UnShuffle(UINT uSeed);
		void Sort(void);

		// Simple Search
		UINT Find(CData  const &Data) const;
		UINT Find(CArray const &Array) const;

		// Binary Search
		UINT BSearch(CData const &Data) const;

		// Replacement
		UINT Replace(CData const &Srch, CArray const &Repl);
		UINT Replace(CArray const &Srch, CArray const &Repl);
		UINT Replace(CArray const &Srch, CData const &Repl);

		// Failure Checks
		BOOL Failed(UINT uIndex) const;
		UINT Failed(void) const;

	protected:
		// Data Members
		CData   m_Null;
		UINT    m_uCount;
		UINT    m_uLimit;
		UINT    m_uDelta;
		UINT    m_uScale;
		CData * m_pData;

		// Implementation
		void InitEmpty(void);
		void InitFrom(CArray const &That);

		// Sort Helper
		static int SortHelp(void const *p1, void const *p2);
	};

/////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CData>

#define	TP2 CArray <CData>

/////////////////////////////////////////////////////////////////////////
//
// Array Collection
//

// Constructors

TP1 TP2::CArray(void)
{
	InitEmpty();
	}

TP1 TP2::CArray(CArray const &That)
{
	InitFrom(That);
	}

TP1 TP2::CArray(CData const &e1)
{
	InitEmpty();

	Append(e1);
	}

TP1 TP2::CArray(CData const &e1, CData const &e2)
{
	InitEmpty();

	Append(e1);

	Append(e2);
	}

TP1 TP2::CArray(CData const &e1, CData const &e2, CData const &e3)
{
	InitEmpty();

	Append(e1);

	Append(e2);

	Append(e3);
	}

TP1 TP2::CArray(CData const &e1, CData const &e2, CData const &e3, CData const &e4)
{
	InitEmpty();

	Append(e1);

	Append(e2);

	Append(e3);

	Append(e4);
	}

// Destructor

TP1 TP2::~CArray(void)
{
	delete [] m_pData;
	}

// Initialization

TP1 void TP2::Init(PBYTE pData, UINT uCount, UINT uLimit)
{
	Empty();

	m_pData  = pData;

	m_uCount = uCount;

	m_uLimit = uLimit;
	}

// Assignment

TP1 TP2 const & TP2::operator = (CArray const &That)
{
	Empty();

	InitFrom(That);

	return ThisObject;
	}

// Attributes

TP1 BOOL TP2::IsEmpty(void) const
{
	return !m_uCount;
	}

TP1 BOOL TP2::operator ! (void) const
{
	return !m_uCount;
	}

TP1 UINT TP2::GetCount(void) const
{
	return m_uCount;
	}

TP1 UINT TP2::GetLimit(void) const
{
	return m_uLimit;
	}

// Lookup Operator

TP1 CData const & TP2::operator [] (UINT uIndex) const
{
	if( uIndex < m_uCount ) {

		return m_pData[uIndex];
		}

	return m_Null;
	}

// Data Read

TP1 CData const & TP2::GetAt(UINT uIndex) const
{
	if( uIndex < m_uCount ) {

		return m_pData[uIndex];
		}

	return m_Null;
	}

// Data Write

TP1 void TP2::SetAt(UINT uIndex, CData const &Data)
{
	if( uIndex < m_uCount ) {
		
		m_pData[uIndex] = Data;
		}
	}

TP1 void TP2::SetAt(UINT uIndex, CArray const &Array)
{
	UINT uCount = Array.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		SetAt(uIndex, Array[n]);

		uIndex++;
		}
	}

TP1 void TP2::SetAt(UINT uIndex, CData const *pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		SetAt(uIndex, *pData++);

		uIndex++;
		}
	}

// Data Pointer

TP1 CData const * TP2::GetPointer(void) const
{
	return m_pData;
	}

// Core Operations

TP1 void TP2::Empty(void)
{
	m_uCount  = 0;

	m_uLimit  = 0;

	m_uScale = m_uDelta;

	delete [] m_pData;

	m_pData  = NULL;
	}

TP1 UINT TP2::Append(CData const &Data)
{
	if( m_uCount == m_uLimit ) {

		UINT uLimit = m_uLimit + m_uScale;

		m_uScale    = m_uScale + m_uScale;

		Expand(uLimit);
		}

	m_pData[m_uCount] = Data;

	return m_uCount++;
	}

TP1 UINT TP2::Append(CArray const &Array)
{
	UINT uFirst = m_uCount;

	Expand(m_uCount + Array.m_uCount);

	for( UINT n = 0; n < Array.m_uCount; n++ ) {

		CData const &Data = Array.m_pData[n];

		m_pData[m_uCount++] = Data;
		}

	return uFirst;
	}

TP1 UINT TP2::Append(CData const *pData, UINT uCount)
{
	UINT uFirst = m_uCount;

	Expand(m_uCount + uCount);

	while( uCount-- ) {

		CData const &Data = *pData++;

		m_pData[m_uCount++] = Data;
		}

	return uFirst;
	}

TP1 UINT TP2::Insert(UINT uIndex, CData const &Data)
{
	if( m_uCount == m_uLimit ) {

		UINT uLimit = m_uLimit + 16;

		Expand(uLimit);
		}

	for( UINT n = m_uCount; n > uIndex; n-- ) {

		CData const &Move = m_pData[n - 1];

		m_pData[n] = Move;
		}

	m_pData[n] = Data;

	m_uCount++;

	return n;
	}

TP1 UINT TP2::Insert(UINT uIndex, CArray const &Array)
{
	UINT uCount = Array.GetCount();

	Expand(m_uCount + uCount);

	for( UINT n = m_uCount + uCount - 1; n > uIndex; n-- ) {

		CData const &Move = m_pData[n - uCount];

		m_pData[n] = Move;
		}

	for( UINT i = 0; i < uCount; i++ ) {

		CData const &Data = Array[i];

		m_pData[n + i] = Data;
		}

	m_uCount += uCount;

	return n;
	}

TP1 UINT TP2::Insert(UINT uIndex, CData const *pData, UINT uCount)
{
	Expand(m_uCount + uCount);

	for( UINT n = m_uCount + uCount - 1; n > uIndex; n-- ) {

		CData const &Move = m_pData[n - uCount];

		m_pData[n] = Move;
		}

	for( UINT i = 0; i < uCount; i++ ) {

		CData const &Data = *pData++;

		m_pData[n + i] = Data;
		}

	m_uCount += uCount;

	return n;
	}

TP1 void TP2::Remove(UINT uIndex, UINT uCount)
{
	for( UINT n = uIndex; n < m_uCount - uCount; n++ ) {

		CData const &Move = m_pData[n + uCount];

		m_pData[n] = Move;
		}

	m_uCount -= uCount;
	}

TP1 void TP2::Remove(UINT uIndex)
{
	Remove(uIndex, 1);
	}

// Other Operations

TP1 void TP2::SetDelta(UINT uDelta)
{
	AfxAssert(uDelta);

	m_uDelta = uDelta;

	m_uScale = uDelta;
	}

TP1 void TP2::SetCount(UINT uCount)
{
	Expand(uCount);

	m_uCount = uCount;
	}

TP1 void TP2::Expand(UINT uLimit)
{
	if( uLimit > m_uLimit ) {

		CData *pData = tp_new CData [ uLimit ];

		if( m_pData ) {

			for( UINT n = 0; n < m_uCount; n++ ) {

				pData[n] = m_pData[n];
				}
			
			delete [] m_pData;
			}

		m_uLimit = uLimit;

		m_pData  = pData;
		}
	}

TP1 void TP2::Compress(void)
{
	if( m_uCount < m_uLimit ) {

		if( m_uCount ) {

			CData *pData = tp_new CData [ m_uCount ];

			for( UINT n = 0; n < m_uCount; n++ ) {

				pData[n] = m_pData[n];
				}
			
			delete [] m_pData;

			m_pData = pData;
			}
		else {
			delete [] m_pData;

			m_pData = NULL;
			}

		m_uLimit = m_uCount;
		}
	}

TP1 void TP2::Shuffle(UINT uSeed)
{
	if( uSeed ) {
		
		srand(uSeed);
		}
	
	for( UINT n = 0; n < m_uCount - 1; n++ ) {

		UINT t = rand() % (m_uCount - n);

		if( n == t ) continue;

		Swap(m_pData[n], m_pData[t]);
		}
	}

TP1 void TP2::UnShuffle(UINT uSeed)
{
	srand(uSeed);
	
	INT *pRand = tp_new INT [ m_uCount ];
	
	for( UINT i = 0; i < m_uCount; i ++ ) {

		pRand[i] = rand();
		}

	for( INT n = m_uCount - 2; n >= 0; n-- ) {

		UINT t = pRand[n] % (m_uCount - n);

		if( UINT(n) == t ) continue;

		Swap(m_pData[n], m_pData[t]);
		}

	delete [] pRand;
	}

TP1 void TP2::Sort(void)
{
	qsort( m_pData,
	       m_uCount,
	       sizeof(CData),
	       SortHelp
	       );
	}

// Simple Search

TP1 UINT TP2::Find(CData const &Data) const
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_pData[n] == Data ) {

			return n;
			}
		}

	return NOTHING;
	}

TP1 UINT TP2::Find(CArray const &Array) const
{
	UINT  uPos = NOTHING;

	for( UINT n = 0; n < m_uCount; n ++ ) {

		UINT uCount = Array.GetCount();

		for( UINT m = 0; m < uCount; m ++ ) {

			if( Array[m] == m_pData[n + m] ) {

				if( m == 0 ) {

					uPos = n;
					}

				if( m == uCount - 1 ) {

					return uPos;
					}
				}
			else 
				break;
			}		
		}
	
	return uPos;
	}

// Binary Search

TP1 UINT TP2::BSearch(CData const &Data) const
{
	PVOID pFind = bsearch( &Data,
			       m_pData,
			       m_uCount,
			       sizeof(CData),
			       SortHelp
			       );

	if( pFind ) {

		CData *pData = (CData *) pFind;

		return pData - m_pData;
		}

	return NOTHING;
	}

// Replacement

TP1 UINT TP2::Replace(CData const &Srch, CArray const &Repl)
{
	UINT uCount = 0;

	for( ;; ) {

		UINT uPos = Find(Srch);

		if( uPos < NOTHING ) {
			
			Remove(uPos, 1);

			Insert(uPos, Repl);
			
			uCount ++;
			}
		else 
			break;
		}

	return uCount;	
	}

TP1 UINT TP2::Replace(CArray const &Srch, CArray const &Repl)
{
	UINT uCount = 0;

	for( ;; ) {

		UINT uPos = Find(Srch);

		if( uPos < NOTHING ) {
			
			Remove(uPos, Srch.GetCount());

			Insert(uPos, Repl);
			
			uCount ++;
			}
		else 
			break;
		}

	return uCount;	
	}

TP1 UINT TP2::Replace(CArray const &Srch, CData const &Repl)
{
	UINT uCount = 0;

	for( ;; ) {

		UINT uPos = Find(Srch);

		if( uPos < NOTHING ) {
			
			Remove(uPos, Srch.GetCount());

			Insert(uPos, Repl);
			
			uCount ++;
			}
		else 
			break;
		}

	return uCount;	
	}

// Failure Checks

TP1 BOOL TP2::Failed(UINT uIndex) const
{
	return uIndex == NOTHING;
	}

TP1 UINT TP2::Failed(void) const
{
	return NOTHING;
	}

// Implementation

TP1 void TP2::InitEmpty(void)
{
	m_uLimit = 0;

	m_uCount = 0;

	m_pData  = NULL;

	#if defined(_M_IX86)

	m_uDelta = max(16, 128 / sizeof(CData));

	m_uScale = m_uDelta;

	#else

	m_uDelta = 16;

	m_uScale = m_uDelta;

	#endif
	}

TP1 void TP2::InitFrom(CArray const &That)
{
	m_uLimit = That.m_uCount;

	m_uCount = That.m_uCount;

	m_uDelta = That.m_uDelta;

	if( m_uCount ) {

		m_pData = tp_new CData [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CData const &Data = That.m_pData[n];

			m_pData[n] = Data;
			}

		return;
		}

	m_pData = NULL;
	}

// Sort Helper

TP1 int TP2::SortHelp(void const *p1, void const *p2)
{
	CData const &d1 = ((CData const *) p1)[0];

	CData const &d2 = ((CData const *) p2)[0];

	return AfxCompare(d1, d2);
	}

// End of File

#endif
