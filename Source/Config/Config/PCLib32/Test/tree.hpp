
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TREE_HPP
	
#define	INCLUDE_TREE_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE TEXT(__FILE__)

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

template <typename CData> class CRootNode;

template <typename CData> class CTreeNode;

template <typename CData> class CTree;

//////////////////////////////////////////////////////////////////////////
//
// Tree Root Node
//

template <typename CData> class CRootNode
{
	public:
		// Constructor
		CRootNode(void);

		// Node Type
		typedef CTreeNode <CData> CNode;

		// Data Members
		CNode *m_pLink[2];
		CNode *m_pParent;
		int    m_nHeight;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tree Data Node
//

template <typename CData> class CTreeNode : public CRootNode <CData>
{
	public:
		// Constructor
		CTreeNode(CData const &Data);

		// Data Members
		CData m_Data;
	};

//////////////////////////////////////////////////////////////////////////
//
// Balanced Tree Collection
//

template <typename CData> class CTree
{
	public:
		// Constructors
		CTree(void);
		CTree(CTree const &That);
		
		// Destructor
		~CTree(void);

		// Assignment
		CTree const & operator = (CTree const &That);

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetCount(void) const;
		
		// Lookup Operator
		CData const & operator [] (INDEX Index) const;

		// Core Operations
		void Empty(void);
		BOOL Insert(CData const &Data);
		void Insert(CTree const &Tree);
		BOOL Remove(CData const &Data);
		void Remove(INDEX Index);

		// Enumeration
		INDEX GetHead(void) const;
		INDEX GetTail(void) const;
		BOOL  GetNext(INDEX &Index) const;
		BOOL  GetPrev(INDEX &Index) const;

		// Searching
		INDEX Find(CData const &Data) const;

		// Failure Checks
		BOOL  Failed(INDEX Index) const;
		INDEX Failed(void) const;

	protected:
		// Node Types
		typedef CRootNode <CData> CRoot;
		typedef CTreeNode <CData> CNode;

		// Link Types
		enum LinkType
		{
			LESS = 0,
			MORE = 1
			};
			
		// Data Members
		UINT    m_uCount;
		CNode * m_pRoot;
		
		// Insertion Helpers
		BOOL InsertData(CData const &Data, CNode *pSite, UINT Child);
		void InsertNode(CData const &Data, CNode *pSite, UINT Child);
		
		// Removal Helpers
		BOOL RemoveData(CData const &Data, CNode *pSite, UINT Child);
		void RemoveNode(CNode *pSite, UINT Child);
		
		// Balancing
		void Rotate1(CNode *pSite, UINT Child, UINT Side);
		void Rotate2(CNode *pSite, UINT Child, UINT Side);

		// General Support
		void MakeLink(CNode *pSite, UINT Child, CNode *pNode);
		UINT Compare(CNode const *pNode, CData const &Data) const;
		UINT GetSide(CNode const *pNode) const;
		void PurgeNode(CNode *pNode);

		// Height Calculation
		int  Height(CNode const *pNode);
		int  Height(CNode const *pNode, UINT Side);
		BOOL IsSkewed(CNode const *pNode, UINT Side);
		void CalcHeight(CNode *pNode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CData>

#define	TP2 CRootNode <CData>

//////////////////////////////////////////////////////////////////////////
//
// Tree Root Node
//

// Constructor

TP1 TP2::CRootNode(void)
{
	m_pLink[0] = NULL;
	
	m_pLink[1] = NULL;

	m_pParent  = NULL;

	m_nHeight  = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <class CData>

#define	TP2 CTreeNode <CData>

//////////////////////////////////////////////////////////////////////////
//
// Tree Data Node
//

// Constructor

TP1 TP2::CTreeNode(CData const &Data) : m_Data(Data)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <class CData>

#define	TP2 CTree <CData>

//////////////////////////////////////////////////////////////////////////
//
// Balanced Tree Collection
//

// Constructors

TP1 TP2::CTree(void)
{
	m_pRoot  = (CNode *) tp_new CRoot;

	m_uCount = 0;
	}

TP1 TP2::CTree(CTree const &That)
{
	m_pRoot  = (CNode *) tp_new CRoot;

	m_uCount = 0;

	Insert(That);
	}

// Destructor

TP1 TP2::~CTree(void)
{
	CNode *pNode = m_pRoot->m_pLink[LESS];

	PurgeNode(pNode);

	delete (CRoot *) m_pRoot;
	}

// Assignment

TP1 TP2 const & TP2::operator = (CTree const &That)
{
	Empty();

	Insert(That);

	return ThisObject;
	}

// Attributes

TP1 BOOL TP2::IsEmpty(void) const
{
	return !m_uCount;
	}

TP1 BOOL TP2::operator ! (void) const
{
	return !m_uCount;
	}

TP1 UINT TP2::GetCount(void) const
{
	return m_uCount;
	}
		
// Lookup Operator

TP1 CData const & TP2::operator [] (INDEX Index) const
{
	return ((CNode *) Index)->m_Data;
	}

// Core Operations

TP1 void TP2::Empty(void)
{
	CNode *pNode = m_pRoot->m_pLink[LESS];

	PurgeNode(pNode);

	m_pRoot->m_pLink[LESS] = NULL;

	m_uCount = 0;
	}

TP1 BOOL TP2::Insert(CData const &Data)
{
	return InsertData(Data, m_pRoot, LESS);
	}

TP1 void TP2::Insert(CTree const &Tree)
{
	for( INDEX i = Tree.GetHead(); !Tree.Failed(i); Tree.GetNext(i) ) {

		CData const &Data = Tree[i];

		Insert(Data);
		}
	}

TP1 BOOL TP2::Remove(CData const &Data)
{
	return RemoveData(Data, m_pRoot, LESS);
	}

TP1 void TP2::Remove(INDEX Index)
{
	CNode *pNode = (CNode *) Index;

	CNode *pSite = pNode->m_pParent;

	RemoveNode(pSite, GetSide(pNode));
	}
		
// Enumeration

TP1 INDEX TP2::GetHead(void) const
{
	CNode *pNode = m_pRoot->m_pLink[LESS];

	if( pNode ) {

		for(;;) {

			CNode *pNext = pNode->m_pLink[LESS];

			if( !pNext ) break;

			pNode = pNext;
			}
		}

	return INDEX(pNode);
	}

TP1 INDEX TP2::GetTail(void) const
{
	CNode *pNode = m_pRoot->m_pLink[LESS];

	if( pNode ) {

		for(;;) {

			CNode *pNext = pNode->m_pLink[MORE];

			if( !pNext ) break;

			pNode = pNext;
			}
		}

	return INDEX(pNode);
	}

TP1 BOOL TP2::GetNext(INDEX &Index) const
{
	if( Index ) {

		CNode *pNode = (CNode *) Index;

		if( pNode->m_pLink[MORE] ) {

			pNode = pNode->m_pLink[MORE];

			for(;;) {

				CNode *pNext = pNode->m_pLink[LESS];

				if( !pNext ) break;

				pNode = pNext;
				}
			}
		else {
			for(;;) {

				if( GetSide(pNode) == LESS ) {

					pNode = pNode->m_pParent;

					break;
					}

				pNode = pNode->m_pParent;
				}
			}

		if( pNode != m_pRoot ) {

			Index = INDEX(pNode);
		
			return TRUE;
			}

		Index = NULL;
		}

	return FALSE;
	}

TP1 BOOL TP2::GetPrev(INDEX &Index) const
{
	if( Index ) {

		CNode *pNode = (CNode *) Index;

		if( pNode->m_pLink[LESS] ) {

			pNode = pNode->m_pLink[LESS];

			for(;;) {

				CNode *pNext = pNode->m_pLink[MORE];

				if( !pNext ) break;

				pNode = pNext;
				}
			}
		else {
			for(;;) {

				if( GetSide(pNode) == MORE ) {

					pNode = pNode->m_pParent;

					break;
					}

				pNode = pNode->m_pParent;
				}
			}

		if( pNode ) {

			Index = INDEX(pNode);
		
			return TRUE;
			}

		Index = NULL;
		}

	return FALSE;
	}

// Searching

TP1 INDEX TP2::Find(CData const &Data) const
{
	CNode *pNode = m_pRoot->m_pLink[LESS];
	
	while( pNode ) {

		int Comp = AfxCompare(Data, pNode->m_Data);

		if( !Comp ) {
			
			break;
			}

		if( Comp < 0 ) {

			pNode = pNode->m_pLink[LESS];
			}
		else
			pNode = pNode->m_pLink[MORE];
		}
		
	return INDEX(pNode);
	}

// Failure Checks

TP1 BOOL TP2::Failed(INDEX Index) const
{
	return Index == NULL;
	}

TP1 INDEX TP2::Failed(void) const
{
	return NULL;
	}
		
// Insertion Helpers

TP1 BOOL TP2::InsertData(CData const &Data, CNode *pSite, UINT Child)
{
	CNode *pNode = pSite->m_pLink[Child];
	
	if( pNode ) {

		if( AfxCompare(pNode->m_Data, Data) ) {

			UINT Side = Compare(pNode, Data);
			
			UINT Flip = 1 - Side;
			
			if( InsertData(Data, pNode, Side) ) {

				if( Height(pNode, Side) - Height(pNode, Flip) > 1 ) {
				
					CNode *pTest = pNode->m_pLink[Side];
					
					if( Compare(pTest, Data) == Side )
						Rotate1(pSite, Child, Side);
					else
						Rotate2(pSite, Child, Side);

					return TRUE;
					}

				CalcHeight(pNode);

				return TRUE;
				}
			}

		return FALSE;
		}

	InsertNode(Data, pSite, Child);

	return TRUE;
	}

TP1 void TP2::InsertNode(CData const &Data, CNode *pSite, UINT Child)
{
	CNode *pNode = tp_new CNode(Data);
	
	MakeLink(pSite, Child, pNode);
	
	m_uCount++;
	}

// Removal Helpers

TP1 BOOL TP2::RemoveData(CData const &Data, CNode *pSite, UINT Child)
{
	CNode *pNode = pSite->m_pLink[Child];

	if( pNode ) {
	
		if( AfxCompare(pNode->m_Data, Data) ) {

			UINT Side = Compare(pNode, Data);
			
			UINT Flip = 1 - Side;
			
			if( RemoveData(Data, pNode, Side) ) {
				
				if( Height(pNode, Flip) - Height(pNode, Side) >= 2 ) {

					if( IsSkewed(pNode->m_pLink[Flip], Side) ) {

						Rotate2(pSite, Child, Flip);
						}
					else
						Rotate1(pSite, Child, Flip);

					return TRUE;
					}

				CalcHeight(pNode);

				return TRUE;
				}
			}
		else {
			RemoveNode(pSite, Child);
			
			return TRUE;
			}
		}

	return FALSE;
	}

TP1 void TP2::RemoveNode(CNode *pSite, UINT Child)
{
	CNode *pNode = pSite->m_pLink[Child];

	if( !pNode->m_pLink[LESS] || !pNode->m_pLink[MORE] ) {
	
		CNode *pChild = NULL;
		
		if( !pChild ) {

			pChild = pNode->m_pLink[LESS];
			}
		
		if( !pChild ) {

			pChild = pNode->m_pLink[MORE];
			}
			
		MakeLink(pSite, Child, pChild);
		}
	else {
		UINT Side = (m_uCount % 2) ? MORE : LESS;
		
		UINT Flip = 1 - Side;
		
		CNode *pScan = pNode->m_pLink[Flip];

		for(;;) {
		
			CNode *pNext = pScan->m_pLink[Side];
			 
			if( !pNext ) break;
			 	
			pScan = pNext;
			}
			
		if( pScan->m_pParent != pNode ) {

			MakeLink(pScan->m_pParent, Side, pScan->m_pLink[Flip]);
			
			MakeLink(pScan, Flip, pNode->m_pLink[Flip]);
			}
		
		MakeLink(pScan, Side, pNode->m_pLink[Side]);

		MakeLink(pSite, Child, pScan);

		CalcHeight(pScan);
		}

	delete pNode;
				
	m_uCount--;
	}

// Balancing

TP1 void TP2::Rotate1(CNode *pSite, UINT Child, UINT Side)
{
	CNode *pNode = pSite->m_pLink[Child];

	UINT Flip = 1 - Side;

	CNode *pWork = pNode->m_pLink[Side];

	MakeLink(pNode, Side, pWork->m_pLink[Flip]);
	
	MakeLink(pWork, Flip, pNode);
	
	pNode->m_nHeight = 1 + Max(Height(pNode, Side), Height(pNode, Flip));
	
	pWork->m_nHeight = 1 + Max(Height(pWork, Side), Height(pNode));
	
	MakeLink(pSite, Child, pWork);
	}

TP1 void TP2::Rotate2(CNode *pSite, UINT Child, UINT Side)
{
	CNode *pNode = pSite->m_pLink[Child];

	UINT Flip = 1 - Side;

	Rotate1(pNode, Side, Flip);
	
	Rotate1(pSite, Child, Side);
	}

// General Support

TP1 void TP2::MakeLink(CNode *pSite, UINT Child, CNode *pNode)
{
	if( pNode ) {
		
		pNode->m_pParent      = pSite;

		pSite->m_pLink[Child] = pNode;

		return;
		}

	pSite->m_pLink[Child] = NULL;
	}

TP1 UINT TP2::Compare(CNode const *pNode, CData const &Data) const
{
	return (AfxCompare(Data, pNode->m_Data) > 0) ? MORE : LESS;
	}
	
TP1 UINT TP2::GetSide(CNode const *pNode) const
{
	CNode *pParent = pNode->m_pParent;

	if( pParent ) {

		if( pParent->m_pLink[LESS] == pNode ) {

			return LESS;
			}
		else
			return MORE;
		}

	return MORE;
	}

TP1 void TP2::PurgeNode(CNode *pNode)
{
	if( pNode ) {
	
		PurgeNode(pNode->m_pLink[LESS]);
		
		PurgeNode(pNode->m_pLink[MORE]);
		
		delete pNode;
		}
	}
	
// Height Calculation

TP1 int TP2::Height(CNode const *pNode)
{
	return pNode ? pNode->m_nHeight : -1;
	}

TP1 int TP2::Height(CNode const *pNode, UINT Side)
{
	return Height(pNode->m_pLink[Side]);
	}

TP1 BOOL TP2::IsSkewed(CNode const *pNode, UINT Side)
{
	if( pNode->m_pLink[Side] ) {
	
		UINT Flip = 1 - Side;
	
		return Height(pNode, Side) > Height(pNode, Flip);
		}
		
	return FALSE;
	}

TP1 void TP2::CalcHeight(CNode *pNode)
{
	pNode->m_nHeight = 1 + Max(Height(pNode, LESS), Height(pNode, MORE));
	}

// End of File

#endif
