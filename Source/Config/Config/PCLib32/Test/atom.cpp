
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Atom
//

// Constructors

CLocalAtom::CLocalAtom(void)
{
	InitFrom(0, FALSE);
	}

CLocalAtom::CLocalAtom(CLocalAtom const &That)
{
	InitFrom(That.m_Atom, TRUE);
	}

CLocalAtom::CLocalAtom(ATOM Atom, BOOL fAdd)
{
	InitFrom(Atom, fAdd);
	}

CLocalAtom::CLocalAtom(PCTXT pString)
{
	AfxValidateStringPtr(pString);

	ATOM Atom = AddAtom(pString);

	InitFrom(Atom, FALSE);
	}

// Destructor

CLocalAtom::~CLocalAtom(void)
{
	Close();
	}

// Assignment Operators

CLocalAtom const & CLocalAtom::operator = (CLocalAtom const &That)
{
	Close();

	InitFrom(That.m_Atom, TRUE);

	return ThisObject;
	}

CLocalAtom const & CLocalAtom::operator = (ATOM Atom)
{
	Close();

	InitFrom(Atom, FALSE);

	return ThisObject;
	}

CLocalAtom const & CLocalAtom::operator = (PCTXT pString)
{
	Close();

	AfxValidateStringPtr(pString);

	ATOM Atom = AddAtom(pString);

	InitFrom(Atom, FALSE);

	return ThisObject;
	}

// Conversion

CLocalAtom::operator ATOM (void) const
{
	return m_Atom;
	}

CLocalAtom::operator PCTXT (void) const
{
	return PCTXT(DWORD(m_Atom));
	}

// Attributes

ATOM CLocalAtom::GetAtom(void) const
{
	return m_Atom;
	}

BOOL CLocalAtom::IsValid(void) const
{
	return m_Atom ? TRUE : FALSE;
	}

BOOL CLocalAtom::IsNull(void) const
{
	return m_Atom ? FALSE : TRUE;
	}

BOOL CLocalAtom::operator ! (void) const
{
	return m_Atom ? FALSE : TRUE;
	}

// Lookup

CLocalAtom CLocalAtom::Find(PCTXT pString)
{
	AfxValidateStringPtr(pString);

	ATOM Atom = FindAtom(pString);

	return CLocalAtom(Atom, TRUE);
	}

// Implementation

void CLocalAtom::InitFrom(ATOM Atom, BOOL fAdd)
{
	if( Atom && fAdd ) {

		TCHAR sBuff[256];
		
		m_Atom = 0;
		
		if( GetAtomName( Atom, 
				 sBuff, 
				 elements(sBuff)
				 ) ) {

			m_Atom = AddAtom(sBuff);
			
			return;
			}
			
		AfxAssert(FALSE);
		}

	m_Atom = Atom;
	}

void CLocalAtom::Close(void)
{
	if( m_Atom ) {

		DeleteAtom(m_Atom);

		m_Atom = 0;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Global Atom
//

// Constructors

CGlobalAtom::CGlobalAtom(void)
{
	InitFrom(0, FALSE);
	}

CGlobalAtom::CGlobalAtom(CGlobalAtom const &That)
{
	InitFrom(That.m_Atom, TRUE);
	}

CGlobalAtom::CGlobalAtom(ATOM Atom, BOOL fAdd)
{
	InitFrom(Atom, fAdd);
	}

CGlobalAtom::CGlobalAtom(PCTXT pString)
{
	AfxValidateStringPtr(pString);

	ATOM Atom = GlobalAddAtom(pString);

	InitFrom(Atom, FALSE);
	}

// Destructor

CGlobalAtom::~CGlobalAtom(void)
{
	Close();
	}

// Assignment Operators

CGlobalAtom const & CGlobalAtom::operator = (CGlobalAtom const &That)
{
	Close();

	InitFrom(That.m_Atom, TRUE);

	return ThisObject;
	}

CGlobalAtom const & CGlobalAtom::operator = (ATOM Atom)
{
	Close();

	InitFrom(Atom, FALSE);

	return ThisObject;
	}

CGlobalAtom const & CGlobalAtom::operator = (PCTXT pString)
{
	Close();

	AfxValidateStringPtr(pString);

	ATOM Atom = GlobalAddAtom(pString);

	InitFrom(Atom, FALSE);

	return ThisObject;
	}

// Conversion

CGlobalAtom::operator ATOM (void) const
{
	return m_Atom;
	}

CGlobalAtom::operator PCTXT (void) const
{
	return PCTXT(DWORD(m_Atom));
	}

// Attributes

ATOM CGlobalAtom::GetAtom(void) const
{
	return m_Atom;
	}

BOOL CGlobalAtom::IsValid(void) const
{
	return m_Atom ? TRUE : FALSE;
	}

BOOL CGlobalAtom::IsNull(void) const
{
	return m_Atom ? FALSE : TRUE;
	}

BOOL CGlobalAtom::operator ! (void) const
{
	return m_Atom ? FALSE : TRUE;
	}

// Lookup

CGlobalAtom CGlobalAtom::Find(PCTXT pString)
{
	AfxValidateStringPtr(pString);

	ATOM Atom = GlobalFindAtom(pString);

	return CGlobalAtom(Atom, TRUE);
	}

// Implementation

void CGlobalAtom::InitFrom(ATOM Atom, BOOL fAdd)
{
	if( Atom && fAdd ) {

		TCHAR sBuff[256];
		
		m_Atom = 0;
		
		if( GlobalGetAtomName( Atom, 
				       sBuff, 
				       elements(sBuff)
				       ) ) {

			m_Atom = GlobalAddAtom(sBuff);
	
			return;
			}
			
		AfxAssert(FALSE);
		}

	m_Atom = Atom;
	}

void CGlobalAtom::Close(void)
{
	if( m_Atom ) {

		GlobalDeleteAtom(m_Atom);

		m_Atom = 0;
		}
	}

// End of File
