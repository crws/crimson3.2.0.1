
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCCORE_HPP
	
#define	INCLUDE_PCCORE_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Warning Control
//

#include "level4.hpp"

//////////////////////////////////////////////////////////////////////////
//								
// Header Control
//

#define	UNICODE		TRUE

#define	NO_STRICT	TRUE

#define	_WIN32_WINNT	0x0501

#define	WINVER		0x0501

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <windows.h>
#include <olectl.h>
#include <ctype.h>
#include <eh.h>
#include <fcntl.h>
#include <io.h>
#include <malloc.h>
#include <shlobj.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <comcat.h>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pccore.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Library Files
//

#pragma	comment(lib, "kernel32.lib")
#pragma	comment(lib, "user32.lib")
#pragma	comment(lib, "gdi32.lib")
#pragma	comment(lib, "ole32.lib")
#pragma	comment(lib, "oleaut32.lib")
#pragma	comment(lib, "uuid.lib")
#pragma	comment(lib, "advapi32.lib")
#pragma comment(lib, "version.lib")
#pragma comment(lib, "shfolder.lib")

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCCORE

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pccore.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//								
// Legacy Support
//

#undef  ANSI_LEGACY

#if defined(PROJECT_PCCORE) || defined(PROJECT_C3COMM)

#define	ANSI_LEGACY

#endif

//////////////////////////////////////////////////////////////////////////
//								
// Warning Control
//

#include "level4.hpp"

//////////////////////////////////////////////////////////////////////////
//								
// Keep Stack Frames
//

#pragma optimize("y", off)

//////////////////////////////////////////////////////////////////////////
//								
// General Typedefs
//

typedef	TCHAR *		PTXT;

typedef TCHAR const *	PCTXT;

typedef	WCHAR *		PUTF;

typedef WCHAR const *	PCUTF;

typedef int		INT;

typedef	void *		PVOID;

typedef void const *	PCVOID;

typedef BYTE const *	PCBYTE;

typedef WORD const *	PCWORD;

typedef UINT const *	PCUINT;

typedef DWORD const *	PCDWORD;

//////////////////////////////////////////////////////////////////////////
//
// General Macros
//

#define	global		/**/

#define	NOTHING		UINT(-1)

#define	elements(x)	(sizeof(x) / sizeof((x)[0]))

#define	offset(t, x)	UINT(&(((t *) 0)->x))

#define	CTEXT		static TCHAR

#define	CLINK		extern "C"

#define	BEGIN		do

#define	END		while(0)

#define	BREAKPOINT	BEGIN { __asm int 3 } END

#define	AfxTouch(x)	((void) x)

#define	TLS		__declspec(thread)

#define	ThisObject	(*this)

#define	IDS(x)		L##x

//////////////////////////////////////////////////////////////////////////
//
// Dummy Link Specifiers
//

#define	SLOW		/**/

#define	FAST		/**/

#define	OPT1		/**/

#define	OPT2		/**/

#define	OPT3		/**/

//////////////////////////////////////////////////////////////////////////
//
// COM-OLE Macros
//

#define	METHOD	STDMETHODCALLTYPE

#define	HRM	HRESULT METHOD

#define	VHRM	virtual HRESULT METHOD

//////////////////////////////////////////////////////////////////////////
//
// Namespace Macros
//

#define	AfxNamespaceDefine(n)		namespace n { }

#define	AfxNamespaceBegin(n)		namespace n {

#define AfxNamespaceEnd(n)		}

#define AfxNamespaceUsing(n)		using namespace n

#define AfxNamespaceAlias(n, a)		namespace a = n

//////////////////////////////////////////////////////////////////////////
//
// COM Helper Macros
//

#define	 AfxRelease(p)	BEGIN { if( p ) (p)->Release(); } END

#define	 AfxAddRef(p)	BEGIN { if( p ) (p)->AddRef();  } END

//////////////////////////////////////////////////////////////////////////
//
// File Information Macros
//

#ifdef	_DEBUG

#define	AfxFileHeader()	CTEXT afxSourceFile[] = TEXT(__FILE__)

#define	afxLine		(__LINE__)

#define	afxFile		(afxSourceFile)

#else

#define	AfxFileHeader()	CTEXT afxSourceFile[] = L""

#define	afxLine		(__LINE__)

#define	afxFile		TEXT(__FILE__)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Min and Max Macros
//

#define	MakeMin(a, b)	BEGIN { if( (b) < (a) ) (a) = (b); } END

#define	MakeMax(a, b)	BEGIN { if( (b) > (a) ) (a) = (b); } END

#define	Min(a, b)	(((b) > (a)) ? (a) : (b))

#define	Max(a, b)	(((a) > (b)) ? (a) : (b))

//////////////////////////////////////////////////////////////////////////
//
// Utility Templates
//

template <typename type> inline void Swap(type &a, type &b)
{
	type c = a; a = b; b = c;
	}

template <typename type> inline int AfxCompare(type const &a, type const &b)
{
	return (a > b) ? +1 : ((b > a ) ? -1 : 0);
	}

template <typename type> inline void AfxSetZero(type &a)
{
	a = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// GUID Comparison
//

inline int AfxCompare(GUID const &a, GUID const &b)
{
	return memcmp(&a, &b, sizeof(GUID));
	}

//////////////////////////////////////////////////////////////////////////
//
// Linked List Macros
//

#define	AfxListAppend(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->p = t; o->n = NULL;			\
	t ? (t->n = o) : (h = o);		\
	t = o;					\
	} END					\

#define	AfxListInsert(h, t, o, n, p, b)		\
						\
	BEGIN {					\
	o->p = b ? b->p : t;			\
	o->n = b;				\
	o->p ? (o->p->n = o) : (h = o);		\
	o->n ? (o->n->p = o) : (t = o);		\
	} END					\

#define	AfxListRemove(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->n ? (o->n->p = o->p) : (t = o->p);	\
	o->p ? (o->p->n = o->n) : (h = o->n);	\
	} END					\

//////////////////////////////////////////////////////////////////////////
//
// Debugging Allocator
//

#ifdef _DEBUG

#define	New		new(afxFile, afxLine)

#define	tp_new		new(TP_FILE, afxLine)

#else

#define	New		new

#define	tp_new		new

#endif

//////////////////////////////////////////////////////////////////////////
//
// Memory Management Functions
//

DLLAPI void * AfxMalloc(UINT uSize);

DLLAPI void * AfxMalloc(UINT uSize, PCTXT pFile, UINT uLine);

DLLAPI void * AfxMalloc(UINT uSize, PCTXT pFile, UINT uLine, BOOL fObject);

DLLAPI void   AfxFree  (void *pData);

//////////////////////////////////////////////////////////////////////////
//
// Memory Management Operators
//

inline void * operator new(UINT uSize)
{
	return AfxMalloc(uSize);
	}

inline void * operator new(UINT uSize, PCTXT pFile, UINT uLine)
{
	return AfxMalloc(uSize, pFile, uLine);
	}

inline void * operator new(UINT uSize, void *pData)
{
	return pData;
	}

inline void operator delete(void *pData)
{
	AfxFree(pData);
	}

inline void operator delete(void *pData, PCTXT pFile, UINT uLine)
{
	AfxFree(pData);
	}

inline void operator delete(void *pData, void *pAddr)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Functions
//

#ifdef	_DEBUG

DLLAPI BOOL AfxCheckBlock(void *pData);
DLLAPI BOOL AfxCheckMemory(void);
DLLAPI void AfxDumpBlock(void *pData);
DLLAPI UINT AfxMarkMemory(void);
DLLAPI BOOL AfxDumpMemory(UINT uSince, PCTXT pFile);

#else

inline BOOL AfxCheckBlock(void *pData) { return TRUE; }
inline BOOL AfxCheckMemory(void) { return TRUE; }
inline void AfxDumpBlock(void *pData) { }
inline UINT AfxMarkMemory(void) { return 0; }
inline BOOL AfxDumpMemory(UINT uSince, PCTXT pFile) { return TRUE; }

#endif

//////////////////////////////////////////////////////////////////////////
//
// General Diagnostic Functions
//

#ifdef _DEBUG

DLLAPI BOOL AfxAssertFailed(PCTXT pFile, UINT uLine);
DLLAPI BOOL AfxFatalExit(PCTXT pText);
DLLAPI void AfxTraceArgs(PCTXT pText, va_list pArgs);
DLLAPI void AfxTrace(PCTXT pText, ...);
DLLAPI void AfxLog(PCTXT pText);
DLLAPI BOOL AfxAborting(void);
DLLAPI BOOL AfxAbort(void);

#else

inline void AfxTraceArgs(PCTXT pText, va_list pArgs) { }
inline void AfxTrace(PCTXT pText, ...) { }
inline void AfxLog(PCTXT pText) { }
inline BOOL AfxAborting(void) { return FALSE; }
inline BOOL AfxAbort(void) { FatalExit(0); return TRUE; }

#endif

//////////////////////////////////////////////////////////////////////////
//
// Assertion Macros
//

#ifdef _DEBUG

#define	AfxAssert(x)	BEGIN { if( !(x) )			\
			if( AfxAssertFailed(afxFile, afxLine) )	\
			BREAKPOINT;				\
			} END					\

#define	AfxVerify(x)	BEGIN { if( !(x) )			\
			if( AfxAssertFailed(afxFile, afxLine) )	\
			BREAKPOINT;				\
			} END					\
			
#else

#define AfxAssert(x)	BEGIN { __assume(x); ((void) (0)); } END

#define	AfxVerify(x)	BEGIN { __assume(x); ((void) (x)); } END

#endif

//////////////////////////////////////////////////////////////////////////
//
// Pointer Validation
//

#ifdef _DEBUG

DLLAPI void AfxValidateReadPtr(void const *pData, UINT uSize = 1);
DLLAPI void AfxValidateWritePtr(void *pData, UINT uSize = 1);
DLLAPI void AfxValidateStringPtr(PCTXT pText, UINT uSize = 1);
DLLAPI void AfxValidateStringPtr(PCSTR pText, UINT uSize = 1);
DLLAPI void AfxValidateResourceName(PCTXT pText, UINT uSize = 1);
DLLAPI void AfxValidateObject(class CObject const *pObject);
DLLAPI void AfxValidateObject(class CObject const &Object);

#else

inline void AfxValidateReadPtr(void const *pData, UINT uSize = 1) { }
inline void AfxValidateWritePtr(void *pData, UINT uSize = 1) { }
inline void AfxValidateStringPtr(PCTXT pText, UINT uSize = 1) { }
inline void AfxValidateStringPtr(PCSTR pText, UINT uSize = 1) { }
inline void AfxValidateResourceName(PCTXT pText, UINT uSize = 1) { }
inline void AfxValidateObject(class CObject const *pObject) { }
inline void AfxValidateObject(class CObject const &Object) { }

#endif

//////////////////////////////////////////////////////////////////////////
//								
// FAST-LZ Compression
//

CLINK DLLAPI int fastlz_compress  (PCVOID, int, PVOID);

CLINK DLLAPI int fastlz_decompress(PCVOID, int, PVOID, int);

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CEntity;
class CModule;
class CString;

//////////////////////////////////////////////////////////////////////////
//								
// PCCore Module
//

extern DLLAPI CModule * pccModule;

//////////////////////////////////////////////////////////////////////////
//								
// Current Module
//

extern CModule * afxModule;

//////////////////////////////////////////////////////////////////////////
//
// Entity Identifier
//

class CEntity
{
	public:
		// Constructors
		CEntity(void);
		CEntity(PCTXT pName, CModule *pModule = afxModule);
		CEntity(UINT  uID,   CModule *pModule = afxModule);

		// Attributes
		CModule * GetModule(void) const;
		PCTXT     GetName(void) const;
		UINT      GetID(void) const;
		BOOL	  IsStock(void) const;

	protected:
		// Data Mmebers
		CModule * m_pModule;
		PCTXT     m_pName;
	};

//////////////////////////////////////////////////////////////////////////
//
// Entity Reference
//

#define	ENTITY CEntity const &

//////////////////////////////////////////////////////////////////////////
//
// Entity Identifier Constructors
//

inline CEntity::CEntity(void)
{
	m_pName   = NULL;

	m_pModule = NULL;
	}

inline CEntity::CEntity(PCTXT pName, CModule *pModule)
{
	m_pName   = pName;

	m_pModule = pModule;
	}

inline CEntity::CEntity(UINT uID, CModule *pModule)
{
	m_pName   = PCTXT(uID);

	m_pModule = pModule;
	}

//////////////////////////////////////////////////////////////////////////
//
// Entity Identifier Attributes
//

inline CModule * CEntity::GetModule(void) const
{
	return m_pModule;
	}

inline PCTXT CEntity::GetName(void) const
{
	return m_pName;
	}

inline UINT CEntity::GetID(void) const
{
	return UINT(m_pName);
	}

inline BOOL CEntity::IsStock(void) const
{
	return HIWORD(m_pName) == 0x0000 && (LOWORD(m_pName) & 0xF000) == 0x7000;
	}

//////////////////////////////////////////////////////////////////////////
//								
// OEM Strings Hook
//

typedef void (*POEMHOOK)(PTXT pBuffer, UINT &uSize); 

//////////////////////////////////////////////////////////////////////////
//								
// General Index Pointer
//

typedef struct tagINDEX { } const * INDEX;

//////////////////////////////////////////////////////////////////////////
//
// Collection Templates
//

#include "named.hpp"

#include "zeroed.hpp"

#include "tree.hpp"

#include "map.hpp"

#include "zmap.hpp"

#include "linked.hpp"

#include "array.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Collections
//

typedef CTree  < class CString  > CStringTree;

typedef CList  < class CString  > CStringList;

typedef CArray < class CString  > CStringArray;

typedef CArray < BYTE           > CByteArray;

typedef CArray < WORD           > CWordArray;

typedef CArray < DWORD	        > CLongArray;

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CGuid;

//////////////////////////////////////////////////////////////////////////
//								
// GUID Wrapper Object
//

class DLLAPI CGuid : public GUID
{
	public:
		// Constructors
		CGuid(void);
		CGuid(CGuid const &That);
		CGuid(GUID const &Guid);
		CGuid(PCTXT pText);

		// Assignment
		CGuid const & operator = (CGuid const &That);
		CGuid const & operator = (GUID const &Guid);
		CGuid const & operator = (PCTXT pText);

		// Attributes
		BOOL    IsEmpty(void) const;
		BOOL    operator ! (void) const;
		CString GetAsText(void) const;

		// Operations
		void CreateUnique(void);

	protected:
		// Null GUID
		static GUID const * m_pNull;
	};

//////////////////////////////////////////////////////////////////////////
//
// Wide Strings
//

DLLAPI int    watoi    (PCUTF p);
DLLAPI double watof    (PCUTF p);
DLLAPI WCHAR  wtoupper (WCHAR c);
DLLAPI WCHAR  wtolower (WCHAR c);
DLLAPI BOOL   wisupper (WCHAR c);
DLLAPI BOOL   wislower (WCHAR c);
DLLAPI BOOL   wisalpha (WCHAR c);
DLLAPI BOOL   wisalnum (WCHAR c);
DLLAPI BOOL   wisspace (WCHAR c);
DLLAPI BOOL   wisdigit (WCHAR c);
DLLAPI int    wstrlen  (PCUTF p);
DLLAPI void   wstrcat  (PUTF d, PCUTF s);
DLLAPI void   wstrcpy  (PUTF d, PCUTF s);
DLLAPI void   wstrncpy (PUTF d, PCUTF s, INT n);
DLLAPI int    wstrcmp  (PCUTF p1, PCUTF p2);
DLLAPI int    wstricmp (PCUTF p1, PCUTF p2);
DLLAPI int    wstrncmp (PCUTF p1, PCUTF p2, INT n);
DLLAPI int    wstrnicmp(PCUTF p1, PCUTF p2, INT n);
DLLAPI PUTF   wstrchr  (PCUTF p, WORD f);
DLLAPI PUTF   wstrrchr (PCUTF p, WORD f);
DLLAPI PUTF   wstrstr  (PCUTF p, PCUTF f);
DLLAPI PUTF   wstristr (PCUTF p, PCUTF f);
DLLAPI UINT   wstrspn  (PCUTF p, PCUTF l);
DLLAPI UINT   wstrcspn (PCUTF p, PCUTF l);
DLLAPI PUTF   wstrdup  (PCUTF p);
DLLAPI PUTF   wstrdpi  (PCUTF p);
DLLAPI PUTF   wstrdup  (PCSTR p);
DLLAPI void   wstrupr  (PUTF p);
DLLAPI void   wstrlwr  (PUTF p);
DLLAPI void   wmemset  (PVOID d, WORD c, UINT n);
DLLAPI void   wmemcpy  (PVOID d, PCVOID s, UINT n);
DLLAPI void   wmemmove (PVOID d, PCVOID s, UINT n);

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CStrPtr;
class CString;
class CCaseString;
class CPrintf;
class CFormat;
class CDelete;
class CFilename;

//////////////////////////////////////////////////////////////////////////
//								
// Unicode Aliases
//

typedef CString CUnicode;

typedef CStrPtr CUniPtr;

//////////////////////////////////////////////////////////////////////////
//
// String Information
//

struct DLLAPI CStringData
{
	UINT	m_uAlloc;
	UINT	m_uLen;
	int	m_nRefs;
	UINT	m_uDummy;
	TCHAR	m_cData[];
	};

//////////////////////////////////////////////////////////////////////////
//
// String Searching
//

enum
{
	searchContains	 = 0x0001,
	searchIsEqualTo  = 0x0002,
	searchStartsWith = 0x0003,
	searchMatchCase  = 0x0100,
	searchWholeWords = 0x0200,
	};

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

class DLLAPI CStrPtr
{
	public:
		// Constructors
		CStrPtr(CStrPtr const &That);
		CStrPtr(CString const &That);
		CStrPtr(PCTXT pText);

		// Conversion
		operator PCTXT (void) const;
		
		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetLength(void) const;
		UINT GetHashValue(void) const;
		
		// Read-Only Access
		TCHAR GetAt(UINT uIndex) const;

		// Array Conversion
		UINT GetCharCodes(CWordArray &Code);
		UINT GetTypeInfo1(CWordArray &Type);
		UINT GetTypeInfo2(CWordArray &Type);
		UINT GetTypeInfo3(CWordArray &Type);
		UINT GetTypeInfo (CWordArray &Type, UINT uType);
					      
		// Comparison Functions
		int CompareC(PCTXT pText) const;
		int CompareN(PCTXT pText) const;
		int CompareC(PCTXT pText, UINT uCount) const;
		int CompareN(PCTXT pText, UINT uCount) const;
		int GetScore(PCTXT pText) const;

		// Comparison Helper
		friend DLLAPI int AfxCompare(CStrPtr const &a, CStrPtr const &b);

		// Comparison Operators
		BOOL operator == (PCTXT pText) const;
		BOOL operator != (PCTXT pText) const;
		BOOL operator <  (PCTXT pText) const;
		BOOL operator >  (PCTXT pText) const;
		BOOL operator <= (PCTXT pText) const;
		BOOL operator >= (PCTXT pText) const;

		// Legacy Comparison
		BOOL operator == (PCSTR pText) const;
		BOOL operator != (PCSTR pText) const;

		// Substring Extraction
		CString Left(UINT uCount) const;
		CString Right(UINT uCount) const;
		CString Mid(UINT uPos, UINT uCount) const;
		CString Mid(UINT uPos) const;

		// Case Conversion
		CString ToUpper(void) const;
		CString ToLower(void) const;

		// Searching
		UINT Find(TCHAR cChar) const;
		UINT Find(TCHAR cChar, TCHAR cQuote) const;
		UINT Find(TCHAR cChar, TCHAR cOpen, TCHAR cClose) const;
		UINT Find(TCHAR cChar, UINT uPos) const;
		UINT Find(TCHAR cChar, UINT uPos, TCHAR cQuote) const;
		UINT Find(TCHAR cChar, UINT uPos, TCHAR cOpen, TCHAR cClose) const;
		UINT FindRev(TCHAR cChar) const;
		UINT FindRev(TCHAR cChar, UINT uPos) const;
		UINT Find(PCTXT pText) const;
		UINT Find(PCTXT pText, UINT uPos) const;
		UINT FindNot(PCTXT pList) const;
		UINT FindNot(PCTXT pList, UINT uPos) const;
		UINT FindOne(PCTXT pList) const;
		UINT FindOne(PCTXT pList, UINT uPos) const;

		// Searching
		UINT Search(PCTXT pText, UINT uMethod) const;
		UINT Search(PCTXT pText, UINT uFrom, UINT uMethod) const;

		// Partials
		BOOL StartsWith(PCTXT pText) const;
		BOOL EndsWith(PCTXT pText) const;

		// Counting
		UINT Count(TCHAR cChar) const;

		// Diagnostics
		void AssertValid(void) const;
		
	protected:
		// Data Members
		PTXT m_pText;
		
		// Protected Constructors
		CStrPtr(void) { }
	};

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

class DLLAPI CString : public CStrPtr
{
 	public:
		// Constructors
		CString(void);
		CString(CString const &That);
		CString(PCTXT pText, UINT uCount);
		CString(TCHAR cData, UINT uCount);
		CString(GUID const &Guid);
		CString(PCTXT pText);
		CString(ENTITY ID);

#if defined(ANSI_LEGACY)
		// Legacy Constructors
		CString(PCSTR pText);
#else
		// Explicit Constructors
		explicit CString(PCSTR pText);
#endif

		// Destructor
		~CString(void);
		
		// Assignment Operators
		CString const & operator = (CString const &That);
		CString const & operator = (GUID const &Guid);
		CString const & operator = (PCTXT pText);
		CString const & operator = (ENTITY ID);

		// Legacy Assignment
		CString const & operator = (PCSTR pText);

		// Quick Init
		void QuickInit(PCTXT pText, UINT uSize);
		
		// Attributes
		UINT GetLength(void) const;
		
		// Read-Only Access
		TCHAR GetAt(UINT uIndex) const;
		
		// Substring Extraction
		CString Left(UINT uCount) const;
		CString Right(UINT uCount) const;
		CString Mid(UINT uPos, UINT uCount) const;
		CString Mid(UINT uPos) const;

		// Buffer Managment
		void Empty(void);
		void Expand(UINT uAlloc);
		void Compress(void);
		void CopyOnWrite(void);
		void FixLength(void);

		// Concatenation In-Place
		CString const & operator += (CString const &That);
		CString const & operator += (PCTXT pText);
		CString const & operator += (PCSTR pText);
		CString const & operator += (TCHAR cData);

		// Concatenation via Friends
		friend DLLAPI CString operator + (CString const &A, CString const &B);
		friend DLLAPI CString operator + (CString const &A, PCTXT pText);
		friend DLLAPI CString operator + (CString const &A, TCHAR cData);
		friend DLLAPI CString operator + (PCTXT pText, CString const &B);
		friend DLLAPI CString operator + (TCHAR cData, CString const &B);

		// Write Data Access
		void SetAt(UINT uIndex, TCHAR cData);

		// Comparison Helper
		friend DLLAPI int AfxCompare(CString const &a, CString const &b);

		// Insert and Remove
		void Insert(UINT uIndex, PCTXT pText);
		void Insert(UINT uIndex, CString const &That);
		void Insert(UINT uIndex, TCHAR cData);
		void Delete(UINT uIndex, UINT uCount);

		// Whitespace Trimming
		void TrimLeft(void);
		void TrimRight(void);
		void TrimBoth(void);
		void StripAll(void);

		// Case Switching
		void MakeUpper(void);
		void MakeLower(void);

		// Unicode Transforms
		BOOL FoldUnicode(UINT uFlags);
		BOOL RemoveUpperDiacriticals(void);
		BOOL RemoveDiacriticals(void);

		// Replacement
		UINT Replace(TCHAR cFind, TCHAR cNew);
		UINT Replace(PCTXT pFind, PCTXT pNew);

		// Removal
		void    Remove (TCHAR cFind);
		CString Without(TCHAR cFind);

		// Parsing
		UINT    Tokenize(CStringArray &Array, TCHAR cFind) const;
		UINT    Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cQuote) const;
		UINT    Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const;
		UINT    Tokenize(CStringArray &Array, PCTXT pFind) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, TCHAR pFind, TCHAR cQuote) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, PCTXT pFind) const;
		CString TokenLeft(TCHAR cFind) const;
		CString TokenFrom(TCHAR cFind) const;
		CString TokenLast(TCHAR cFind) const;
		CString StripToken(TCHAR cFind);
		CString StripToken(TCHAR cFind, TCHAR cQuote);
		CString StripToken(TCHAR cFind, TCHAR cOpen, TCHAR cClose);
		CString StripToken(PCTXT pFind);

		// Building
		void Build(CStringArray &Array, TCHAR cSep);
		void Build(CStringArray &Array, PCTXT pSep);

		// Printf Formatting
		void Printf(PCTXT pFormat, ...);
		void Printf(CEntity Format, ...);
		void VPrintf(PCTXT pFormat, va_list pArgs);
		void VPrintf(ENTITY Format, va_list pArgs);

#if defined(ANSI_LEGACY)
		// Legacy Printf
		void Printf(PCSTR pFormat, ...);
		void VPrintf(PCSTR pFormat, va_list pArgs);
#endif

		// Windows Formatting
		void Format(PCTXT pFormat, ...);
		void Format(CEntity Format, ...);
		void VFormat(PCTXT pFormat, va_list pArgs);
		void VFormat(ENTITY Format, va_list pArgs);

#if defined(ANSI_LEGACY)
		// Legacy Format
		void Format(PCSTR pFormat, ...);
		void VFormat(PCSTR pFormat, va_list pArgs);
#endif

		// Resource Loading
		void Load(ENTITY ID);

		// Resource Name Encoding
		void SetResourceName(PCTXT pName);

		// Diagnostics
		void AssertValid(void) const;
		
	protected:
		// Static Data
		static CStringData m_Empty;

		// Protected Constructor
		CString(PCTXT p1, UINT u1, PCTXT p2, UINT u2);

		// Initialisation
		void InitFrom(GUID const &Guid);
		
		// Interal Helpers
		PTXT IntPrintf(PCTXT pFormat, va_list pArgs);
		PTXT IntFormat(PCTXT pFormat, va_list pArgs);
		PTXT IntLoadString(ENTITY ID);

		// Clearing
		friend void DLLAPI AfxSetZero(CString &Text);

		// Implementation
		void Alloc(UINT uLen, UINT uAlloc);
		void Alloc(UINT uLen);
		BOOL GrowString(UINT uLen);
		void Release(void);
		UINT AdjustSize(UINT uAlloc);
		UINT FindAlloc(UINT uLen);
	};

//////////////////////////////////////////////////////////////////////////
//
// Case-Sensitive String
//

class DLLAPI CCaseString : public CString
{
	public:
		// Constructors
		CCaseString(void);
		CCaseString(CCaseString const &That);
		CCaseString(CString const &That);

		// Assignment
		CCaseString const & operator = (CCaseString const &That);
		CCaseString const & operator = (CString const &That);

		// Conversion
		operator CString (void) const;

		// Comparison
		friend int DLLAPI AfxCompare(CCaseString const &a, CCaseString const &b);
	};

//////////////////////////////////////////////////////////////////////////
//
// Printf Formatted String
//

class DLLAPI CPrintf : public CString
{
	public:
		// Constructors
		CPrintf(PCTXT   pFormat, ...);
		CPrintf(PCSTR   pFormat, ...);
		CPrintf(CEntity  Format, ...);
	};

//////////////////////////////////////////////////////////////////////////
//
// Windows Formatted String
//

class DLLAPI CFormat : public CString
{
	public:
		// Constructors
		CFormat(PCTXT   pFormat, ...);
		CFormat(PCSTR   pFormat, ...);
		CFormat(CEntity  Format, ...);
	};

//////////////////////////////////////////////////////////////////////////
//
// Deleting String
//

class DLLAPI CDelete : public CString
{
	public:
		// Constructors
		CDelete(PTXT pText, BOOL fOLE);
	};

//////////////////////////////////////////////////////////////////////////
//
// Filename String
//

class DLLAPI CFilename : public CString
{
	public:
		// Constructors
		CFilename(void);
		CFilename(int nFolder);
		CFilename(CFilename const &That);
		CFilename(CString const &That);
		CFilename(PCTXT pText);

		// Assignment Operator
		CFilename const & operator = (CFilename const &That);
		CFilename const & operator = (CString const &That);
		CFilename const & operator = (PCTXT pText);

		// Attributes
		BOOL IsUNC(void) const;
		BOOL HasDrive(void) const;
		BOOL HasPath(void) const;
		BOOL HasName(void) const;
		BOOL HasType(void) const;

		// Element Parsing
		CString GetDrive(void) const;
		CString GetDirectory(void) const;
		CString GetPath(void) const;
		CString GetBarePath(void) const;
		CString GetName(void) const;
		CString GetBareName(void) const;
		CString GetType(void) const;

		// Length Conversion
		void MakeLong(void);
		void MakeShort(void);

		// Strip Operations
		void StripDrive(void);
		void StripPath(void);
		void StripName(void);
		void StripType(void);

		// Edit Conversions
		CFilename WithName(PCTXT pName) const;
		CFilename WithType(PCTXT pType) const;

		// Edit Operations
		void ChangeName(PCTXT pName);
		void ChangeType(PCTXT pType);

		// Path Creation
		BOOL CreatePath(void);

		// Existance Check
		BOOL IsFile(void) const;
		BOOL IsDir(void) const;
		BOOL Exists(void) const;

		// File Access
		HANDLE OpenRead(void) const;
		HANDLE OpenReadSeq(void) const;
		HANDLE OpenWrite(void) const;

		// Temporary Files
		void MakeTemporary(void);
		void MakeTemporary(CString Path);
	};

//////////////////////////////////////////////////////////////////////////
//								
// String Inline Methods
//

#include "string.ipp"

//////////////////////////////////////////////////////////////////////////
//
// Unicode Order Conversion
//

DLLAPI CUnicode UniVisual(CUnicode s);

DLLAPI void     UniVisual(CUnicode s, CArray <UINT> &Map);

DLLAPI BOOL     UniIsComplex(CUnicode s);

DLLAPI BOOL     UniIsComplex(TCHAR c);

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStringNormData;
class CStringNormalizer;

//////////////////////////////////////////////////////////////////////////
//
// String Normalization Cases
//

enum
{
	caseNormal  = 0,
	caseUpper   = 1,
	caseInitial = 2,
	caseTitle   = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// String Normalization Levels
//

enum
{
	normNone  = 0,
	normStrip = 1,
	normCase  = 2
	};

//////////////////////////////////////////////////////////////////////////
//
// String Normalization Data
//

class DLLAPI CStringNormData
{
	public:
		// Data Members
		UINT	m_Norm;
		UINT    m_Case;
		CString m_Head;
		CString m_Tail;
	};

//////////////////////////////////////////////////////////////////////////
//
// String Normalization Object
//

class DLLAPI CStringNormalizer
{
	public:
		// Attributes
		static BOOL IsNormal(CString const &Text);

		// Operations
		static BOOL Normalize  (CString &Text, CStringNormData       &Data, UINT Norm);
		static void Denormalize(CString &Text, CStringNormData const &Data);
		static BOOL Conormalize(CString &Text, CStringNormData const &Data);

	protected:
		// Implementation
		static BOOL IsExtra(WCHAR c);
		static BOOL IsAllUpper(CString const &Text);
		static BOOL HasInitial(CString const &Text);
		static BOOL IsTitleCase(CString const &Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCriticalSection;

//////////////////////////////////////////////////////////////////////////
//
// Library Critical Section
//

extern DLLAPI CCriticalSection afxCritical;

//////////////////////////////////////////////////////////////////////////
//
// Critical Section Object
//

class DLLAPI CCriticalSection
{
	public:
		// Constructor
		CCriticalSection(void);

		// Destructor
		~CCriticalSection(void);

		// Operations
		void Enter(void);
		void Leave(void);

	protected:
		CRITICAL_SECTION m_Critical;
	};

//////////////////////////////////////////////////////////////////////////
//
// Critical Section Guard
//

class DLLAPI CCriticalGuard
{
	public:
		// Constructor
		CCriticalGuard(CCriticalSection &rSection = afxCritical);
		CCriticalGuard(CCriticalSection *pSection);

		// Destructor
		~CCriticalGuard(void);

		// Attach/Detach
		BOOL Attach(CCriticalSection &rSection = afxCritical);
		BOOL Attach(CCriticalSection *pSection);
		void Detach(void);

	protected:
		// Data Members
		CCriticalSection *m_pSection;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CKeyName;
class CRegKey;
class CLocalAtom;
class CGlobalAtom;

//////////////////////////////////////////////////////////////////////////
//
// Standard Collections
//

typedef CMap <CKeyName, CString> CKeyMap;

//////////////////////////////////////////////////////////////////////////
//
// Registry Key Name
//

class DLLAPI CKeyName : public CString
{
	public:
		// Constructors
		CKeyName(void);
		CKeyName(CKeyName const &That);
		CKeyName(CString const &That);
		CKeyName(PCTXT pText);

		// Assignment Operator
		CKeyName const & operator = (CKeyName const &That);
		CKeyName const & operator = (CString const &That);
		CKeyName const & operator = (PCTXT pText);

		// Attributes
		BOOL HasKey(void) const;
		BOOL HasName(void) const;

		// Element Parsing
		CString GetKey(void) const;
		CString GetName(void) const;
		
		// Strip Operations
		void StripKey(void);
		void StripName(void);

		// Edit Conversions
		CKeyName WithName(PCTXT pName) const;

		// Edit Operations
		void ChangeName(PCTXT pName);
		void ChangeKey(PCTXT pKey);
	};

//////////////////////////////////////////////////////////////////////////
//
// Registry Key Object
//

class DLLAPI CRegKey
{
	public:
		// Constructors
		CRegKey(void);
		CRegKey(CRegKey const &That);
		CRegKey(HKEY hKey, BOOL fAdd);
		CRegKey(HKEY hKey);

		// Destructor
		~CRegKey(void);

		// Assignment Operators
		CRegKey const & operator = (CRegKey const &That);
		CRegKey const & operator = (HKEY hKey);

		// Conversion
		operator HKEY (void) const;

		// Attributes
		HKEY GetHandle(void) const;
		BOOL IsValid(void) const;
		BOOL IsNull(void) const;
		BOOL IsReadOnly(void) const;
		BOOL operator ! (void) const;

		// Key Operations
		CRegKey Open(PCTXT pName);
		CRegKey Create(PCTXT pName);
		BOOL    MoveTo(PCTXT pName);
		BOOL    Delete(PCTXT pName);
		BOOL    Flush(void);

		// Map Operations
		BOOL	WriteMap(CKeyMap const &Map);

		// Value Attributes
		DWORD   GetValueType(PCTXT pName) const;
		DWORD   GetValueSize(PCTXT pName) const;
		CString GetValueAsString(PCTXT pName) const;
		UINT    GetValueAsBlob(PCTXT pName, PVOID pData, UINT nBytes) const;
		DWORD   GetValueAsDword(PCTXT pName) const;

		// Value Attributes with Defaults
		UINT    GetValue(PCTXT pName, UINT  uDefault) const;
		CString GetValue(PCTXT pName, PCTXT pDefault) const;

		// Value Operations
		BOOL DeleteValue(PCTXT pName);
		BOOL SetValue(PCTXT pName, PCTXT pText);
		BOOL SetValue(PCTXT pName, PCTXT pText, BOOL fExpand);
		BOOL SetValue(PCTXT pName, PVOID pBlob, UINT nBytes);
		BOOL SetValue(PCTXT pName, DWORD dwData);

		// Prorated Values
		UINT GetValue(PCTXT pName, UINT uData, UINT uLimit) const;
		BOOL SetValue(PCTXT pName, UINT uData, UINT uLimit);

		// Enumeration
		CString GetSubkeyName(UINT n) const;
		CString GetValueName(UINT n) const;
		UINT    GetSubkeyNames(CStringArray &List) const;
		UINT    GetValueNames(CStringArray &List) const;
		CString NullSubkeyName(void) const;
		CString NullValueName(void) const;

	protected:
		// Data Members
		HKEY m_hKey;

		// Implementation
		void InitFrom(HKEY hKey, BOOL fAdd);
		void CloseKey(void);
		
		// Helpers
		HKEY CreateKey(HKEY hKey, PCTXT pName);
		HKEY OpenKey(HKEY hKey, PCTXT pName);
	};

//////////////////////////////////////////////////////////////////////////
//
// Local Atom
//

class DLLAPI CLocalAtom
{
	public:
		// Constructors
		CLocalAtom(void);
		CLocalAtom(CLocalAtom const &That);
		CLocalAtom(ATOM Atom, BOOL fAdd = FALSE);
		CLocalAtom(PCTXT pString);

		// Destructor
		~CLocalAtom(void);

		// Assignment Operators
		CLocalAtom const & operator = (CLocalAtom const &That);
		CLocalAtom const & operator = (PCTXT pString);
		CLocalAtom const & operator = (ATOM Atom);

		// Conversion
		operator ATOM  (void) const;
		operator PCTXT (void) const;

		// Attributes
		ATOM GetAtom(void) const;
		BOOL IsValid(void) const;
		BOOL IsNull(void) const;
		BOOL operator ! (void) const;
		
		// Lookup
		static CLocalAtom Find(PCTXT pString);

	protected:
		// Data Members
		ATOM m_Atom;

		// Implementation
		void InitFrom(ATOM Atom, BOOL fAdd);
		void Close(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Global Atom 
//

class DLLAPI CGlobalAtom
{
	public:
		// Constructors
		CGlobalAtom(void);
		CGlobalAtom(CGlobalAtom const &That);
		CGlobalAtom(ATOM Atom, BOOL fAdd = FALSE);
		CGlobalAtom(PCTXT pString);

		// Destructor
		~CGlobalAtom(void);

		// Assignment Operators
		CGlobalAtom const & operator = (CGlobalAtom const &That);
		CGlobalAtom const & operator = (ATOM Atom);
		CGlobalAtom const & operator = (PCTXT pString);

		// Conversion
		operator ATOM  (void) const;
		operator PCTXT (void) const;

		// Attributes
		ATOM GetAtom(void) const;
		BOOL IsValid(void) const;
		BOOL IsNull(void) const;
		BOOL operator ! (void) const;
		
		// Lookup
		static CGlobalAtom Find(PCTXT pString);

	protected:
		// Data Members
		ATOM m_Atom;

		// Implementation
		void InitFrom(ATOM Atom, BOOL fAdd);
		void Close(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRuntimeClass;
class CObject;

//////////////////////////////////////////////////////////////////////////
//
// Class Information Types
//

typedef class CRuntimeClass * ICLASS;

typedef class CRuntimeClass const * CLASS;

typedef void (*CTOR) (void *);

typedef CList <ICLASS> CClassList;

typedef CMap <IID, UINT> CInterfaceMap;

typedef CMap <CString, UINT> CAppDataDict;

typedef CMap <UINT, CObject *> CAppDataList;

/////////////////////////////////////////////////////////////////////////
//
// Class Information Macros
//

#define	AfxNewObject(p, c)	((p *)((c)->NewObject(AfxRuntimeClass(p))))

#define	AfxObjectClass(o)	((o).AfxObjectClassInfo())

#define	AfxPointerClass(p)	((p) ? (p)->AfxObjectClassInfo() : NULL)

#define	AfxRuntimeClass(c)	(c::AfxStaticClassInfo())

#define	AfxNamedClass(n)	(CRuntimeClass::FindClass(CEntity(n)))

#define	AfxThisClass()		((ICLASS) AfxObjectClassInfo())

#define AfxAllocAppData(n)	(CRuntimeClass::AllocAppData(n))

//////////////////////////////////////////////////////////////////////////
//
// Runtime Class Declaration
//

#define	AfxDeclareRuntimeClass()			\
							\
	private:					\
							\
	static CRuntimeClass m_afxClass;		\
							\
	public:						\
							\
	static  CLASS AfxStaticClassInfo(void);		\
							\
	virtual CLASS AfxObjectClassInfo(void) const;	\

//////////////////////////////////////////////////////////////////////////
//
// Dynamic Class Declaration
//

#define	AfxDeclareDynamicClass()			\
							\
	protected:					\
							\
	static void AfxConstruct(void *pObject);	\
							\
	AfxDeclareRuntimeClass();			\
								
//////////////////////////////////////////////////////////////////////////
//
// Runtime Class Implementation (CObject)
//

#define	AfxImplementObjectClass(name)			\
							\
	CRuntimeClass name::m_afxClass			\
	(						\
		L#name,					\
		sizeof(name),				\
		NULL,					\
		AfxConstruct				\
		);					\
                                                       	\
	CLASS name::AfxStaticClassInfo(void)		\
	{						\
		return &m_afxClass;			\
		}					\
							\
	CLASS name::AfxObjectClassInfo(void) const	\
	{						\
		return &m_afxClass;			\
		}					\

//////////////////////////////////////////////////////////////////////////
//
// Runtime Class Implementation (Others)
//

#define	AfxImplementRuntimeClass(name, base)		\
							\
	CRuntimeClass name::m_afxClass			\
	(						\
		L#name,					\
		sizeof(name),				\
		base::AfxStaticClassInfo(),		\
		NULL					\
		);					\
                                                       	\
	CLASS name::AfxStaticClassInfo(void)		\
	{						\
		return &m_afxClass;			\
		}					\
							\
	CLASS name::AfxObjectClassInfo(void) const	\
	{						\
		return &m_afxClass;			\
		}					\

//////////////////////////////////////////////////////////////////////////
//
// Dynamic Class Implementation
//

#define	AfxImplementDynamicClass(name, base)		\
							\
	CRuntimeClass name::m_afxClass			\
	(						\
		L#name,					\
		sizeof(name),				\
		base::AfxStaticClassInfo(),		\
		AfxConstruct				\
		);					\
                                                       	\
	CLASS name::AfxStaticClassInfo(void)		\
	{						\
		return &m_afxClass;			\
		}					\
							\
	CLASS name::AfxObjectClassInfo(void) const	\
	{						\
		return &m_afxClass;			\
		}					\
							\
	void name::AfxConstruct(void *pObject)		\
	{						\
		new (pObject) name;			\
		}					\

//////////////////////////////////////////////////////////////////////////
//
// Runtime Class Record
//

class DLLAPI CRuntimeClass
{
	public:
		// Constructor
		CRuntimeClass( PCTXT pName,
			       UINT  uSize,
			       CLASS Base,
			       CTOR  Ctor
			       );

		// Destructor
		~CRuntimeClass(void);
		
		// Interface Map
		CInterfaceMap & GetInterfaceMap(void);

		// Type Checking
		BOOL IsKindOf(CLASS Class) const;

		// Owner Module
		CModule * GetModule(void) const;

		// Attributes
		PCTXT     GetClassName(void) const;
		UINT      GetObjectSize(void) const;
		CLASS     GetBaseClass(void) const;
		BOOL      IsComponent(void) const;
		REFGUID   GetClassGuid(void) const;
		LONG	  GetObjectCount(void) const;
		CObject * GetAppData(UINT n) const;

		// Operations
		void RegisterFactory(void);
		void RevokeFactory(void);
		void ObjectCreated(void);
		void ObjectDeleted(void);
		void DeleteInterfaceMap(void);
		void SetAppData(UINT n, CObject *pApp);

		// Construction
		void Construct(void *pObject) const;

		// Allocation
		CObject * NewObject(CLASS Class) const;
		
		// Location
		static CLASS FindClass(ENTITY ID);

		// Registration
		static void Register(CModule *pModule);

		// App Data Indices
		static UINT AllocAppData(PCTXT pName);
		static void ClearAppData(void);

	protected:
		// App Data Dictionary
		static CAppDataDict m_AppDict;
		                  
		// Class List
		static CClassList m_ClassList;

		// Data Members
		PCTXT		m_pName;
		UINT		m_uSize;
		CLASS		m_Base;
		CTOR		m_Ctor;
		CGuid		m_Guid;
		CModule	      * m_pModule;
		CInterfaceMap * m_pIntMap;
		LONG		m_nObjects;
		CAppDataList    m_AppData;

		// Overridables
		virtual void ManageFactory(BOOL fReg);
		
		// Implementation
		void CheckCircular(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// General Base Class
//

class DLLAPI CObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CObject(void);
		
		// Virtual Destructor
		virtual ~CObject(void);

		// Memory Management
		void * operator new (UINT uSize);
		void * operator new (UINT uSize, PCTXT pFile, UINT uLine);
		void * operator new (UINT uSize, void *pData);
		void   operator delete (void *pData);
		void   operator delete (void *pData, PCTXT pFile, UINT uLine);
		void   operator delete (void *pData, void *pAddr);
		
		// Attributes
		PCTXT   GetClassName(void) const;
		UINT    GetObjectSize(void) const;
		CLASS   GetBaseClass(void) const;
		BOOL    IsComponent(void) const;
		REFGUID GetClassGuid(void) const;
		
		// Type Checking
		BOOL IsKindOf(CLASS Class) const;

		// Object Validation
		virtual void AssertValid(void) const;

	private:
		// Hidden Copy Constructor
		CObject(CObject const &That);

		// Hidden Assignment Operator
		void operator = (CObject const &That);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CException;
class CMemoryException;
class CResourceException;
class CSupportException;
class CSystemException;
class CUserException;

//////////////////////////////////////////////////////////////////////////
//
// Exception Reference
//

typedef class CException const & EXCEPTION;

//////////////////////////////////////////////////////////////////////////
//
// Exception Throwing Functions
//

DLLAPI void ThrowException(PCTXT pFile, UINT uLine);
DLLAPI void ThrowMemoryException(PCTXT pFile, UINT uLine);
DLLAPI void ThrowResourceException(PCTXT pFile, UINT uLine);
DLLAPI void ThrowSupportException(PCTXT pFile, UINT uLine);
DLLAPI void ThrowSystemException(PCTXT pFile, UINT uLine, UINT uCode);
DLLAPI void ThrowUserException(PCTXT pFile, UINT uLine);

//////////////////////////////////////////////////////////////////////////
//
// Exception Throwing Macros
//

#ifdef  _DEBUG

#define AfxThrowException()	    ThrowException(afxFile, afxLine)
#define AfxThrowMemoryException()   ThrowMemoryException(afxFile, afxLine)
#define AfxThrowResourceException() ThrowResourceException(afxFile, afxLine)
#define AfxThrowSupportException()  ThrowSupportException(afxFile, afxLine)
#define AfxThrowSystemException(c)  ThrowSystemException(afxFile, afxLine, c)
#define AfxThrowUserException()	    ThrowUserException(afxFile, afxLine)

#else

#define AfxThrowException()	    ThrowException(NULL, 0)
#define AfxThrowMemoryException()   ThrowMemoryException(NULL, 0)
#define AfxThrowResourceException() ThrowResourceException(NULL, 0)
#define AfxThrowSupportException()  ThrowSupportException(NULL, 0)
#define AfxThrowSystemException(c)  ThrowSystemException(NULL, 0, c)
#define AfxThrowUserException()	    ThrowUserException(NULL, 0)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Standard Exception Catcher
//

#define	AfxStdCatch()					\
							\
	catch(CMemoryException const &Exception) {	\
							\
		AfxTouch(Exception);			\
							\
		return E_OUTOFMEMORY;			\
		}					\
							\
	catch(CException const &Exception) {		\
							\
		AfxTouch(Exception);			\
							\
		return E_UNEXPECTED;			\
		}					\

//////////////////////////////////////////////////////////////////////////
//
// Exception Base Class
//

class DLLAPI CException : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CException(void);
		CException(CException const &That);

		// Attributes
		PCTXT GetFile(void) const;
		UINT  GetLine(void) const;

		// Operations
		void SetContext(PCTXT pFile, UINT uLine);

	protected:
		// Data Memebrs
		PCTXT m_pFile;
		UINT  m_uLine;
	};

//////////////////////////////////////////////////////////////////////////
//
// Out-of-Memory Exception
//

class DLLAPI CMemoryException : public CException
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
	};

//////////////////////////////////////////////////////////////////////////
//
// Resource Failure Exception
//

class DLLAPI CResourceException : public CException
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
	};

//////////////////////////////////////////////////////////////////////////
//
// Unsupported Feature Exception
//

class DLLAPI CSupportException : public CException
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
	};

//////////////////////////////////////////////////////////////////////////
//
// System Exception
//

class DLLAPI CSystemException : public CException
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Data Members
		UINT m_uCode;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Exception
//

class DLLAPI CUserException : public CException
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CModule;
class CResTag;

//////////////////////////////////////////////////////////////////////////
//
// Module Types
//

enum ModuleType
{
	modCoreLibrary	= 1,
	modUserLibrary	= 2,
	modInProcServer	= 3,
	modApplication	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// Module Information
//

class DLLAPI CModule
{
	public:
		// Constructor
		CModule(UINT Type);

		// Destructor
		~CModule(void);

		// Initialisation
		BOOL InitApp(HANDLE hInstance, PCTXT pCmdLine, int nShowCode);
		BOOL InitLib(HANDLE hInstance);
		
		// Registry Setup
		void SetRegCompany(PCTXT pData);
		void SetRegName(PCTXT pData);
		void SetRegVersion(PCTXT pData);
	
		// Termination
		BOOL Terminate(void);

		// Process Management
		BOOL ProcessAttach(void);
		BOOL ProcessDetach(void);

		// Thread Management
		BOOL ThreadAttach(void);
		BOOL ThreadDetach(void);

		// Attributes
		BOOL      HasApp(void) const;
		CModule * GetApp(void) const;
		BOOL      IsApplication(void) const;
		BOOL	  IsServer(void) const;
		HANDLE    GetInstance(void) const;
		HANDLE    GetAppInstance(void) const;
		PCTXT     GetCommandLine(void) const;
		int       GetShowCommand(void) const;
		UINT      GetArgumentCount(void) const;
		PCTXT     GetArgument(UINT uIndex) const;
		CFilename GetFilename(void) const;
		CFilename GetHelpFilename(void) const;
		CFilename GetFolder(int nFolder, PCTXT pSuffix) const;
		PCTXT     GetRegCompany(void) const;
		PCTXT     GetRegName(void) const;
		PCTXT     GetRegVersion(void) const;
		CRegKey   GetMachineRegKey(void) const;
		CRegKey   GetUserRegKey(void) const;
		
		// Switch Location
		UINT FindSwitch(PCTXT pName, BOOL fRemove);

		// Resource Location
		BOOL HasResource(PCTXT pName, PCTXT pType);

		// Resource Location
		static HANDLE LocateResource(ENTITY ID, PCTXT pType);
		static HANDLE LocateImage(ENTITY ID, UINT uType);

		// Resource Loading
		static HANDLE LoadResource(ENTITY ID, PCTXT pType);
		static HANDLE LoadResource(ENTITY ID, PCTXT pType, UINT &uSize);
		static HANDLE LoadBitmap(ENTITY ID);
		static HANDLE LoadBitmap(ENTITY ID, int nSize);
		static HANDLE LoadCursor(ENTITY ID);
		static HANDLE LoadCursor(ENTITY ID, int nSize);
		static HANDLE LoadIcon(ENTITY ID);
		static HANDLE LoadIcon(ENTITY ID, int nSize);
		static HANDLE LoadMenu(ENTITY ID);
		static HANDLE LoadAccelerator(ENTITY ID);

		// Image Loading
		static HANDLE LoadImage(ENTITY ID, UINT uType, int nSize, UINT uFlags);

		// String Loading
		static UINT LoadString(ENTITY ID, PTXT pBuffer, UINT uSize);

		// Global Class Location
		static CLASS FindClass(ENTITY ID);

		// Local Class Location
		CLASS FindClass(CString const &Name);

		// Class Registration
		void RegisterClass(ICLASS Class);

		// Class Enumeration
		INDEX GetHeadClass(void);
		CLASS GetClassEntry(INDEX Index);
		BOOL  GetNextClass(INDEX &Index);

		// Object Management
		LONG GetObjectCount(void) const;
		void ObjectCreated(void);
		void ObjectDeleted(void);
		void LockServer(void);
		void UnlockServer(void);

		// Oem Support
		void SetOemHook(POEMHOOK pOemHook);
		
	protected:
		// Collection Types
		typedef CMap <CString, ICLASS> CNameMap;
		typedef CMap <GUID,    ICLASS> CGuidMap;
		typedef CMap <CResTag, HANDLE> CResMap;

		// Module Pointers
		static CModule * m_pApp;
		static CModule * m_pHead;
		static CModule * m_pTail;

		// OEM Translation
		static POEMHOOK m_pOemHook;

		// Resource Map
		static CResMap          m_ResMap;
		static CCriticalSection m_ResSect;

		// Class Cache
		static CString m_LastName;
		static CLASS   m_LastClass;

		// Data Members
		UINT	      m_Type;
		CModule *     m_pNext;
		CModule *     m_pPrev;
		HANDLE	      m_hInstance;
		PCTXT	      m_pCmdLine;
		int	      m_nShowCode;
		CStringArray  m_ArgList;
		CFilename     m_Filename;
		CString	      m_RegCompany;
		CString	      m_RegName;
		CString	      m_RegVersion;
		CNameMap      m_NameMap;
		CGuidMap      m_GuidMap;
		LONG	      m_nObjects;
		LONG	      m_nLocks;

		// Implementation
		BOOL CommonInit(void);
		void FindFileNames(void);
		void ParseCommandLine(void);
		void DeleteInterfaceMaps(void);
		void DeleteResources(void);

		// Factory Mananagement
		void RegisterAllFactories(void);
		void RevokeAllFactories(void);
		void RegisterFactories(void);
		void RevokeFactories(void);

		// Resource Mapping
		static HANDLE SearchResMap(PCTXT pName, PCTXT pType);
		static void   InsertResMap(PCTXT pName, PCTXT pType, HANDLE hFile);

		// Friend List
		friend class CComRegHelper;
	};

//////////////////////////////////////////////////////////////////////////
//
// Resource Tag
//

class DLLAPI CResTag
{
	public:
		// Constructor
		CResTag(PCTXT pName, PCTXT pType);

		// Comparison Function
		friend DLLAPI int AfxCompare(CResTag const &a, CResTag const &b);

	protected:
		// Data Members
		CString m_Name;
		CString m_Type;
	};

// End of File

#endif
