
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_STRING_IPP
	
#define	INCLUDE_STRING_IPP

//////////////////////////////////////////////////////////////////////////
//
// String Data Access
//

#define	CString_GetData(x) (((CStringData *) ((x)->m_pText)) - 1)

//////////////////////////////////////////////////////////////////////////
//
// String Inline Methods
//

inline CStrPtr::operator PCTXT (void) const
{
	return m_pText;
	}

inline CString::CString(CString const &That)
{
	That.AssertValid();
	
	if( !That.IsEmpty() ) {
		
		m_pText = That.m_pText;
		
		CString_GetData(this)->m_nRefs++;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

inline CString::CString(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( *pText ) {
		
		Alloc(wstrlen(pText));
		
		wstrcpy(m_pText, pText);

		return;
		}

	m_pText = m_Empty.m_cData;
	}

inline CString::CString(ENTITY ID)
{
	PTXT pWork = IntLoadString(ID);
	
	Alloc(wstrlen(pWork));
	
	wstrcpy(m_pText, pWork);
	
	delete pWork;
	}

// End of File

#endif
