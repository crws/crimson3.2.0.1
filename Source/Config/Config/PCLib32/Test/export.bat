@echo off

copy *.hpp	..\..\..\include	>nul

copy *.ipp	..\..\..\include	>nul

copy *.hxx	..\..\..\include	>nul

copy %1\*.lib   ..\..\..\lib\%1		>nul

copy %2\*.dll   ..\..\..\bin\%1		>nul

copy %2\*.dll   ..\..\..\opc\bin\%1  >nul

erase ..\..\..\include\intern.h*

if /i %1 == Release (

copy %1\*.pdb   ..\..\..\bin\release\sym >nul

erase ..\..\..\bin\release\sym\vc*.pdb 2>nul

)
