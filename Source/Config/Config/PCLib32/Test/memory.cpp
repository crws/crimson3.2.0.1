
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug Only Code
//

#ifdef _DEBUG

//////////////////////////////////////////////////////////////////////////
//
// Memory Debug Constants
//

static UINT const memSentinel    = 'MSMS';

static UINT const memMagic       = 'MBMB';

static UINT const memRelease     = 'MFMF';

static BYTE const memFillEmpty   = 0xEB;

static BYTE const memFillRelease = 0xFB;

static UINT const memSentSize    = 8;

//////////////////////////////////////////////////////////////////////////
//
// Memory Debug Structures
//

struct CHeader
{
	UINT	  uPad1;
	UINT	  uPad2;
	UINT	  uMagic;
	CHeader * pNext;
	CHeader * pPrev;
	UINT	  uSize;
	UINT      uBatch;
	PCTXT	  pFile;
	UINT	  uLine;
	BOOL	  fObject;
	UINT	  uSent[memSentSize];
	BYTE	  bData[];
	};
	
struct CTailer
{
	UINT	  uSent[memSentSize];
	};

//////////////////////////////////////////////////////////////////////////
//
// Memory Debug Information
//

static 	CHeader * m_pHead   = NULL;

static 	CHeader * m_pTail   = NULL;

static	UINT	  m_uBatch  = 0;

static	DWORD	  m_dwTotal = 0;

static	DWORD	  m_dwCount = 0;

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Helpers
//

static void LoadSentinel(UINT *pData, UINT uCount)
{
	while( uCount-- ) {

		*pData = memSentinel;

		pData++;
		}
	}

static BOOL TestSentinel(UINT *pData, UINT uCount)
{
	while( uCount-- ) {

		if( *pData != memSentinel ) {

			return FALSE;
			}

		pData++;
		}

	return TRUE;
	}

static BOOL IsFourBytes(void * &pData, BYTE bCheck)
{
	for( int n = 0; n < sizeof(pData); n++ ) {

		if( ((BYTE *) &pData)[n] == bCheck ) {

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

static BOOL IsValidPointer(void *pData)
{
	if( pData == NULL ) {

		AfxTrace(L"ERROR: Attempting to use NULL memory pointer\n");

		return FALSE;
		}

	if( IsFourBytes(pData, memFillEmpty) ) {

		AfxTrace(L"ERROR: Attempting to use pointer from empty block\n");

		return FALSE;
		}

	if( IsFourBytes(pData, memFillRelease) ) {

		AfxTrace(L"ERROR: Attempting to use pointer from released block\n");

		return FALSE;
		}

	if( IsBadWritePtr(pData, 1) ) {

		AfxTrace(L"ERROR: Attempting to use invalid memory pointer\n");

		return FALSE;
		}
	
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Memory Management Functions
//

void * AfxMalloc(UINT uSize)
{
	return AfxMalloc(uSize, NULL, 0, FALSE);
	}

void * AfxMalloc(UINT uSize, PCTXT pFile, UINT uLine)
{
	return AfxMalloc(uSize, pFile, uLine, FALSE);
	}

void * AfxMalloc(UINT uSize, PCTXT pFile, UINT uLine, BOOL fObject)
{
	UINT    uExtra = sizeof(CHeader) + sizeof(CTailer);
	
	CHeader *pHead = (CHeader *) malloc(uSize + uExtra);

	if( pHead ) {

		pHead->uMagic  = NOTHING;

		pHead->uSize   = uSize;

		pHead->uBatch  = m_uBatch;

		pHead->pFile   = pFile;

		pHead->uLine   = uLine;

		pHead->fObject = fObject;

		LoadSentinel(pHead->uSent, memSentSize);

		memset(pHead->bData, memFillEmpty, uSize);

		CTailer *pTail = (CTailer *) ((BYTE *) (pHead + 1) + uSize);

		LoadSentinel(pTail->uSent, memSentSize);

		if( TRUE ) {
			
			CCriticalGuard Guard;

			m_dwTotal += pHead->uSize;
			
			m_dwCount += 1;

			AfxListAppend(m_pHead, m_pTail, pHead, pNext, pPrev);

			pHead->uMagic = memMagic;
			}

		return pHead->bData;
		}
		
	AfxThrowMemoryException();
	
	return NULL;
	}

void AfxFree(void *pData)
{
	if( pData ) {

		if( AfxCheckBlock(pData) ) {

			CHeader *pHead = (CHeader *) pData - 1;

			if( TRUE ) {

				CCriticalGuard Guard;

				pHead->uMagic = memRelease;

				AfxListRemove(m_pHead, m_pTail, pHead, pNext, pPrev);
				
				m_dwCount -= 1;
				
				m_dwTotal -= pHead->uSize;
				}

			memset(pHead->bData, memFillRelease, pHead->uSize);

			free(pHead);

			return;
			}

		AfxAssert(FALSE);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Functions
//

BOOL AfxCheckBlock(void *pData)
{
	if( IsValidPointer(pData) ) {

		CHeader *pHead = (CHeader *) pData - 1;

		AfxValidateReadPtr(pHead, sizeof(CHeader));

		if( pHead->uMagic == memMagic ) {

			if( TestSentinel(pHead->uSent, memSentSize) ) {
			
				CTailer *pTail = (CTailer *) (pHead->bData + pHead->uSize);
				
				AfxValidateReadPtr(pTail, sizeof(CTailer));

				if( TestSentinel(pTail->uSent, memSentSize) ) {

					return TRUE;
					}

				AfxTrace(L"ERROR: Block at %p has invalid tail sentinel\n", pData);

				return FALSE;
				}
			
			AfxTrace(L"ERROR: Block at %p has invalid head sentinel\n", pData);

			return FALSE;
			}

		if( pHead->uMagic == memRelease ) {

			AfxTrace(L"ERROR: Block at %p has already been released\n", pData);

			return FALSE;
			}

		AfxTrace(L"ERROR: Block at %p has invalid magic number\n", pData);

		return FALSE;
		}

	return FALSE;
	}

BOOL AfxCheckMemory(void)
{
	CCriticalGuard Guard;

	BOOL fOkay = TRUE;

	for( CHeader *pHead = m_pHead; pHead; pHead = pHead->pNext ) {

		if( !AfxCheckBlock(pHead->bData) ) {

			fOkay = FALSE;

			if( pHead->uMagic == memMagic ) {
				
				if( pHead->uSent[0] == memSentinel ) {

					AfxDumpBlock(pHead->bData);

					continue;
					}
				}

			break;
			}
		}

	return fOkay;
	}

void AfxDumpBlock(void *pData)
{
	CHeader *pHead = (CHeader *) pData - 1;

	AfxTrace(L"%p  %-5u  ", pData, pHead->uSize);
	
	if( pHead->pFile ) {

		CString File;
		
		if( IsBadStringPtr(pHead->pFile, 10) ) {

			File = L"[unloaded]";
			}
		else
			File = pHead->pFile;

		UINT p1 = File.FindRev('\\');

		UINT p2 = File.Left(p1).FindRev('\\');
		
		File = File.Mid(p2 + 1);

		File.MakeLower();
		
		AfxTrace(L"Line %-4u of %-30.30s  ", pHead->uLine, PCTXT(File));
		}
	else {
		PCTXT p = L"Unknown File";

		AfxTrace(L"%-43.43s  ", p);
		}

	if( pHead->fObject ) {
	
		CObject *pObject = (CObject *) pHead->bData;
		
		AfxTrace(L"%s", pObject->GetClassName());
		
		AfxTrace(L"\n");
		}
	else {
		AfxTrace(L"Raw Data");
		
		AfxTrace(L"\n");
		}
	}

UINT AfxMarkMemory(void)
{
	return m_uBatch++;
	}

BOOL AfxDumpMemory(UINT uSince, PCTXT pFile)
{
	CCriticalGuard Guard;

	BOOL fFind = FALSE;
	
	for( CHeader *pHead = m_pHead; pHead; pHead = pHead->pNext ) {

		if( pHead->uBatch <= uSince ) {

			continue;
			}

		if( pFile && !wstrstr(pHead->pFile, pFile) ) {

			continue;
			}
		
		if( !fFind ) {
			
			AfxTrace(L"\nBLOCKS ALLOCATED SINCE MARKER %u\n\n", uSince);
			
			AfxTrace(L"ADDRESS   BYTES  ALLOCATED BY                                 ");
				
			AfxTrace(L"CONTENTS\n");
				
			AfxTrace(L"========  =====  ===========================================  ");
				
			AfxTrace(L"====================\n");
				
			fFind = TRUE;
			}
		
		AfxDumpBlock(pHead->bData);
		}
		
	if( fFind ) {
				
		AfxTrace(L"========  =====  ===========================================  ");
				
		AfxTrace(L"====================\n\n");
		}

	return fFind;
	}

// End of File

#endif
