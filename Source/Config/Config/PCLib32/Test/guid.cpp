
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GUID Wrapper Class
//

// Null GUID

GUID const * CGuid::m_pNull = & CLSID_NULL;

// Constructors

CGuid::CGuid(void)
{
	memcpy(this, m_pNull, sizeof(GUID));
	}

CGuid::CGuid(CGuid const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy(this, &That, sizeof(GUID));
	}

CGuid::CGuid(GUID const &Guid)
{
	AfxValidateReadPtr(&Guid, sizeof(Guid));

	memcpy(this, &Guid, sizeof(GUID));
	}

CGuid::CGuid(PCTXT pText)
{
	if( !pText ) {

		memcpy(this, m_pNull, sizeof(GUID));

		return;
		}

	AfxValidateStringPtr(pText);

	CLSIDFromString(PTXT(pText), this);
	}

// Assignment

CGuid const & CGuid::operator = (CGuid const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy(this, &That, sizeof(GUID));

	return ThisObject;
	}

CGuid const & CGuid::operator = (GUID const &Guid)
{
	AfxValidateReadPtr(&Guid, sizeof(Guid));

	memcpy(this, &Guid, sizeof(GUID));

	return ThisObject;
	}

CGuid const & CGuid::operator = (PCTXT pText)
{
	if( !pText ) {

		memcpy(this, m_pNull, sizeof(GUID));

		return ThisObject;
		}

	AfxValidateStringPtr(pText);

	CLSIDFromString(PTXT(pText), this);

	return ThisObject;
	}

// Attributes

BOOL CGuid::IsEmpty(void) const
{
	return !memcmp(this, m_pNull, sizeof(GUID));
	}

BOOL CGuid::operator ! (void) const
{
	return !memcmp(this, m_pNull, sizeof(GUID));
	}

CString CGuid::GetAsText(void) const
{
	LPOLESTR pText = NULL;

	StringFromCLSID(ThisObject, &pText);

	return CDelete(pText, TRUE);
	}

// Operations

void CGuid::CreateUnique(void)
{
	CoCreateGuid(this);
	}

// End of File
