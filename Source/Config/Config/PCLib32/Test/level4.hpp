
//////////////////////////////////////////////////////////////////////////
//
// Level 4 Compiler Warning Control for Visual C++
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#pragma warning(disable: 4065)	// Switch has no case statements
#pragma warning(disable: 4100)	// Unreference formal parameter
#pragma warning(disable: 4127)	// Constant conditional expression
#pragma warning(disable: 4200)	// Zero-sized array in structure
#pragma warning(disable: 4201)	// Nameless structure or union
#pragma warning(disable: 4231)	// Nonstandard extension used
#pragma warning(disable: 4239)	// Non-const temporary reference
#pragma warning(disable: 4251)	// Mismatched export on classes
#pragma warning(disable: 4355)	// This used in base class init
#pragma warning(disable: 4511)	// No default copy constructor
#pragma warning(disable: 4512)	// No default assignment operator
#pragma warning(disable: 4514)	// Inline function not expanded
#pragma warning(disable: 4702)	// Unreachable code in function
#pragma warning(disable: 4706)	// Assignment within conditional
#pragma warning(disable: 4710)	// Function skipped for expansion
#pragma warning(disable: 4711)	// Function selected for expansion

// End of File
