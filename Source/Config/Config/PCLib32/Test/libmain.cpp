
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * pccModule = NULL;

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

static UINT uMarker = 0;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		uMarker = AfxMarkMemory();

		try {
			afxModule = New CModule(modCoreLibrary);

			pccModule = afxModule;

			afxModule->ProcessAttach();

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load PCCore\n");

				delete afxModule;

				afxModule = NULL;

				pccModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading PCCore\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		afxModule->ProcessDetach();

		delete afxModule;

		afxModule = NULL;

		pccModule = NULL;

		if( AfxCheckMemory() ) {
			
			AfxDumpMemory(uMarker, NULL);
			}

		return TRUE;
		}

	if( uReason == DLL_THREAD_ATTACH ) {

		afxModule->ThreadAttach();

		return TRUE;
		}

	if( uReason == DLL_THREAD_DETACH ) {

		afxModule->ThreadDetach();

		return TRUE;
		}

	return TRUE;
	}

// End of File
