
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Release Only Code
//

#ifdef NDEBUG

//////////////////////////////////////////////////////////////////////////
//
// Memory Management Functions
//

void * AfxMalloc(UINT uSize)
{
	void *pData;
	
	if( (pData = malloc(uSize)) ) {

		return pData;
		}
	
	AfxThrowMemoryException();
	
	return NULL;
	}

void * AfxMalloc(UINT uSize, PCTXT pFile, UINT uLine)
{
	return AfxMalloc(uSize);
	}

void * AfxMalloc(UINT uSize, PCTXT pFile, UINT uLine, BOOL fObject)
{
	return AfxMalloc(uSize);
	}

void AfxFree(void *pData)
{
	free(pData);
	}

// End of File

#endif
