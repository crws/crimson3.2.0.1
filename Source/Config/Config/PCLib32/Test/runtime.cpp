
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Runtime Class Record
//

// Constructor

CRuntimeClass::CRuntimeClass(PCTXT pName, UINT uSize, CLASS Base, CTOR Ctor)
{
	m_pName    = pName;

	m_uSize    = uSize;
	
	m_Base     = Base;
	
	m_Ctor     = Ctor;

	m_pModule  = NULL;
	
	m_pIntMap  = NULL;
	
	m_nObjects = 0;

	CheckCircular();

	m_ClassList.Append(this);
	}
	        
// Destructor

CRuntimeClass::~CRuntimeClass(void)
{
	AfxAssert(!m_pIntMap);
	}

// Interface Map

CInterfaceMap & CRuntimeClass::GetInterfaceMap(void)
{
	if( !m_pIntMap ) {

		m_pIntMap = New CInterfaceMap;

		AfxAssert(m_pIntMap);
		}

	return *m_pIntMap;
	}

// Type Checking

BOOL CRuntimeClass::IsKindOf(CLASS Class) const
{
	CLASS pScan = this;
	
	while( pScan ) {

		if( pScan == Class ) {

			return TRUE;
			}
		
		pScan = pScan->m_Base;
		}

	return FALSE;
	}

// Owner Module

CModule * CRuntimeClass::GetModule(void) const
{
	return m_pModule;
	}

// Attributes

PCTXT CRuntimeClass::GetClassName(void) const
{
	return m_pName;
	}

UINT CRuntimeClass::GetObjectSize(void) const
{
	return m_uSize;
	}

CLASS CRuntimeClass::GetBaseClass(void) const
{
	return m_Base;
	}

BOOL CRuntimeClass::IsComponent(void) const
{
	return !m_Guid.IsEmpty();
	}

REFGUID CRuntimeClass::GetClassGuid(void) const
{
	return m_Guid;
	}

LONG CRuntimeClass::GetObjectCount(void) const
{
	return m_nObjects;
	}

CObject * CRuntimeClass::GetAppData(UINT n) const
{
	INDEX    Index = m_AppData.FindName(n);

	CObject *pData = NULL;

	if( !m_AppData.Failed(Index) ) {

		pData = m_AppData.GetData(Index);

		if( !pData ) {

			pData = (CObject *) 1;
			}
		}

	return pData;
	}

// Operations

void CRuntimeClass::RegisterFactory(void)
{
	AfxAssert(IsComponent());

	ManageFactory(TRUE);
	}

void CRuntimeClass::RevokeFactory(void)
{
	AfxAssert(IsComponent());

	ManageFactory(FALSE);
	}

void CRuntimeClass::ObjectCreated(void)
{
	if( IsComponent() ) {

		InterlockedIncrement(&m_nObjects);

		m_pModule->ObjectCreated();
		}
	}

void CRuntimeClass::ObjectDeleted(void)
{
	if( IsComponent() ) {

		InterlockedDecrement(&m_nObjects);

		m_pModule->ObjectDeleted();
		}
	}

void CRuntimeClass::DeleteInterfaceMap(void)
{
	delete m_pIntMap;

	m_pIntMap = NULL;

	for( INDEX n = m_AppData.GetHead(); !m_AppData.Failed(n); m_AppData.GetNext(n) ) {

		CObject *pData = m_AppData.GetData(n);

		if( HIWORD(pData) ) {

			delete pData;
			}
		}

	m_AppData.Empty();
	}

void CRuntimeClass::SetAppData(UINT n, CObject *pApp)
{
	m_AppData.Remove(n);

	m_AppData.Insert(n, pApp);
	}

// Construction

void CRuntimeClass::Construct(void *pData) const
{
	(m_Ctor)(pData);
	}

// Allocation

CObject * CRuntimeClass::NewObject(CLASS Class) const
{
	AfxAssert(IsKindOf(Class));

	void *pData = AfxMalloc(m_uSize, afxFile, afxLine, TRUE);

	Construct(pData);

	return (CObject *) pData;
	}

// Location

CLASS CRuntimeClass::FindClass(ENTITY ID)
{
	return CModule::FindClass(ID);
	}

// Registration

void CRuntimeClass::Register(CModule *pModule)
{
	CClassList &List = m_ClassList;

	for( INDEX i = List.GetHead(); !List.Failed(i); List.GetNext(i) ) {

		ICLASS p = m_ClassList[i];

		p->m_pModule = pModule;

		pModule->RegisterClass(p);
		}

	List.Empty();
	}

// App Data Indices

UINT CRuntimeClass::AllocAppData(PCTXT pName)
{
	INDEX i = m_AppDict.FindName(pName);

	if( m_AppDict.Failed(i) ) {

		UINT n = m_AppDict.GetCount();

		m_AppDict.Insert(pName, n);

		return n;
		}

	return m_AppDict.GetData(i);
	}

void CRuntimeClass::ClearAppData(void)
{
	m_AppDict.Empty();
	}

// Overridables

void CRuntimeClass::ManageFactory(BOOL fReg)
{
	}

// Implementation

void CRuntimeClass::CheckCircular(void)
{
	#ifdef _DEBUG

	if( m_Base->IsKindOf(this) ) {
		
		AfxTrace(L"ERROR: Circular base class on %s\n", m_pName);

		AfxAssert(FALSE);
		}

	#endif
	}

// End of File
