
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Module Information
//

// Module Pointers

CModule * CModule::m_pApp  = NULL;

CModule * CModule::m_pHead = NULL;

CModule * CModule::m_pTail = NULL;

// OEM Translation

POEMHOOK CModule::m_pOemHook = NULL;

// Resource Map

CModule::CResMap CModule::m_ResMap;

CCriticalSection CModule::m_ResSect;

// Class Cache

CString CModule::m_LastName  = L"";

CLASS   CModule::m_LastClass = NULL;

// Constructor

CModule::CModule(UINT Type)
{
	m_Type	     = Type;

	m_RegCompany = L"Red Lion Controls";

	m_RegName    = L"Test Application";

	m_RegVersion = L"1.0";

	m_pNext	     = NULL;
	
	m_pPrev	     = NULL;

	m_nObjects   = 0;

	m_nLocks     = 0;
	}

// Destructor

CModule::~CModule(void)
{
	}

// Initialisation

BOOL CModule::InitApp(HANDLE hInstance, PCTXT pCmdLine, int nShowCode)
{
	AfxAssert(IsApplication() == TRUE);

	AfxAssert(m_pApp == NULL);

	m_hInstance = hInstance;

	m_pCmdLine  = pCmdLine;

	m_nShowCode = nShowCode;

	if( CommonInit() ) {

		m_pApp = this;

		ParseCommandLine();

		ThreadAttach();

		RegisterAllFactories();

		return TRUE;
		}

	return FALSE;
	}

BOOL CModule::InitLib(HANDLE hInstance)
{
	AfxAssert(IsApplication() == FALSE);

	m_hInstance = hInstance;

	m_pCmdLine  = NULL;

	m_nShowCode = 0;

	if( CommonInit() ) {

		if( HasApp() && !IsServer() ) {

			RegisterFactories();
			}

		return TRUE;
		}

	return FALSE;
	}

// Registry Setup

void CModule::SetRegCompany(PCTXT pData)
{
	AfxValidateStringPtr(pData);

	m_RegCompany = pData;
	}

void CModule::SetRegName(PCTXT pData)
{
	AfxValidateStringPtr(pData);

	m_RegName = pData;
	}

void CModule::SetRegVersion(PCTXT pData)
{
	AfxValidateStringPtr(pData);

	m_RegVersion = pData;
	}

// Termination

BOOL CModule::Terminate(void)
{
	if( !IsApplication() ) {

		if( HasApp() && !IsServer() ) {

			RevokeFactories();
			}
		}
	else {
		RevokeAllFactories();

		ThreadDetach();

		m_pApp = NULL;
		}

	DeleteInterfaceMaps();

	DeleteResources();

	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);

	if( !m_pHead ) {

		CRuntimeClass::ClearAppData();

		m_ResMap.Empty();

		return TRUE;
		}

	return FALSE;
	}

// Process Management

BOOL CModule::ProcessAttach(void)
{
	return TRUE;
	}

BOOL CModule::ProcessDetach(void)
{
	m_LastName.Empty();

	return TRUE;
	}

// Thread Management

BOOL CModule::ThreadAttach(void)
{
	static LONG dwLock = 0;

	if( HasApp() ) {

		if( InterlockedIncrement(&dwLock) == 1 ) {

			OleInitialize(NULL);
			}

		InterlockedDecrement(&dwLock);

		return TRUE;
		}

	return FALSE;
	}

BOOL CModule::ThreadDetach(void)
{
	if( HasApp() ) {

		OleUninitialize();
		
		return TRUE;
		}

	return FALSE;
	}

// Attributes

BOOL CModule::HasApp(void) const
{
	return m_pApp ? TRUE : FALSE;
	}

CModule * CModule::GetApp(void) const
{
	return m_pApp;
	}

BOOL CModule::IsApplication(void) const
{
	return m_Type == modApplication;
	}

BOOL CModule::IsServer(void) const
{
	return m_Type == modInProcServer;
	}

HANDLE CModule::GetInstance(void) const
{
	return m_hInstance;
	}

HANDLE CModule::GetAppInstance(void) const
{
	AfxAssert(m_pApp);

	return m_pApp->m_hInstance;
	}

PCTXT CModule::GetCommandLine(void) const
{
	AfxAssert(m_pApp);

	return m_pApp->m_pCmdLine;
	}

int CModule::GetShowCommand(void) const
{
	AfxAssert(m_pApp);

	return m_pApp->m_nShowCode;
	}

UINT CModule::GetArgumentCount(void) const
{
	AfxAssert(m_pApp);

	return m_pApp->m_ArgList.GetCount();
	}

PCTXT CModule::GetArgument(UINT uIndex) const
{
	AfxAssert(m_pApp);

	return m_pApp->m_ArgList[uIndex];
	}

CFilename CModule::GetFilename(void) const
{
	return m_Filename;
	}

CFilename CModule::GetHelpFilename(void) const
{
	return m_Filename.WithType(L"hlp");
	}

CFilename CModule::GetFolder(int nFolder, PCTXT pSuffix) const
{
	CFilename Path(nFolder);

	Path += L"\\";

	CreateDirectory(Path, NULL);

	Path += m_RegCompany + L"\\";

	CreateDirectory(Path, NULL);

	Path += m_RegName    + L"\\";

	CreateDirectory(Path, NULL);

	Path += m_RegVersion + L"\\";

	CreateDirectory(Path, NULL);

	if( pSuffix ) {

		Path += pSuffix;

		Path += L"\\";

		CreateDirectory(Path, NULL);
		}

	return Path;
	}

PCTXT CModule::GetRegCompany(void) const
{
	return m_RegCompany;
	}

PCTXT CModule::GetRegName(void) const
{
	return m_RegName;
	}

PCTXT CModule::GetRegVersion(void) const
{
	return m_RegVersion;
	}

CRegKey CModule::GetMachineRegKey(void) const
{
	CRegKey Key(HKEY_LOCAL_MACHINE);

	Key.MoveTo(L"Software");

	Key.MoveTo(m_RegCompany);

	Key.MoveTo(m_RegName);

	Key.MoveTo(m_RegVersion);

	return Key;
	}

CRegKey CModule::GetUserRegKey(void) const
{
	CRegKey Key(HKEY_CURRENT_USER);

	Key.MoveTo(L"Software");

	Key.MoveTo(m_RegCompany);

	Key.MoveTo(m_RegName);

	Key.MoveTo(m_RegVersion);

	return Key;
	}

// Switch Location

UINT CModule::FindSwitch(PCTXT pName, BOOL fRemove)
{
	if( m_Type == modApplication ) {

		for( UINT uPos = 0; uPos < m_ArgList.GetCount(); uPos++ ) {

			CString const &Token = m_ArgList[uPos];

			if( Token[0] == '-' || Token[0] == '/' ) {

				CString Target = Token.Mid(1);

				if( Target == pName ) {
		
					if( fRemove ) {

						m_ArgList.Remove(uPos);
						}
					
					return uPos;
					}
				}
			}

		return NOTHING;
		}

	AfxAssert(m_pApp);
	
	return m_pApp->FindSwitch(pName, fRemove);
	}

// Resource Location

BOOL CModule::HasResource(PCTXT pName, PCTXT pType)
{
	UINT uID = 0;

	if( pType == RT_STRING ) {

		uID   = LOWORD(pName);

		pName = MAKEINTRESOURCE(1 + (uID >> 4));
		}

	if( FindResource(m_hInstance, pName, pType) ) {

		if( uID ) {

			TCHAR s[2];

			UINT nc = ::LoadString( m_hInstance,
						uID,
						s,
						elements(s)
						);

			return nc ? TRUE : FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Resource Location

HANDLE CModule::LocateResource(ENTITY ID, PCTXT pType)
{
	CModule *pCaller = ID.GetModule();

	PCTXT    pName   = ID.GetName();

	HANDLE   hFile   = NULL;

	if( ID.IsStock() ) {

		return NULL;
		}

	if( pCaller->HasResource(pName, pType) ) {

		hFile = pCaller->GetInstance();

		return hFile;
		}

	if( (hFile = SearchResMap(pName, pType)) ) {

		return hFile;
		}

	if( TRUE ) {

		for( CModule *pModule = m_pTail; pModule; pModule = pModule->m_pPrev ) {

			if( pModule == pCaller ) {

				continue;
				}

			if( pCaller->IsServer() ) {

				switch( pModule->m_Type ) {

					case modApplication:
					case modUserLibrary:
					case modInProcServer:

						continue;
					}
				}
			else {
				switch( pModule->m_Type ) {

					case modInProcServer:

						continue;
					}
				}

			if( pModule->HasResource(pName, pType) ) {

				hFile = pModule->GetInstance();
		
				InsertResMap(pName, pType, hFile);

				return hFile;
				}
			}
		}

	return NULL;
	}

HANDLE CModule::LocateImage(ENTITY ID, UINT uType)
{
	PCTXT pType;

	switch( uType ) {

		case IMAGE_BITMAP:

			pType = RT_BITMAP;
			
			break;

		case IMAGE_CURSOR:
			
			pType = RT_GROUP_CURSOR;
			
			break;

		case IMAGE_ICON:
			
			pType = RT_GROUP_ICON;
			
			break;

		default:

			pType = NULL;
			
			AfxAssert(FALSE);
			
			break;
		}

	return LocateResource(ID, pType);
	}

// Resource Loading

HANDLE CModule::LoadResource(ENTITY ID, PCTXT pType)
{
	HANDLE hFile = LocateResource(ID, pType);

	HANDLE hRes  = FindResource(hFile, ID.GetName(), pType);

	return ::LoadResource(hFile, hRes);
	}

HANDLE CModule::LoadResource(ENTITY ID, PCTXT pType, UINT &uSize)
{
	HANDLE hFile = LocateResource(ID, pType);

	HANDLE hRes  = FindResource(hFile, ID.GetName(), pType);

	uSize        = SizeofResource(hFile, hRes);

	return ::LoadResource(hFile, hRes);
	}

HANDLE CModule::LoadBitmap(ENTITY ID)
{
	HANDLE hFile = LocateResource(ID, RT_BITMAP);

	return ::LoadBitmap(hFile, ID.GetName());
	}

HANDLE CModule::LoadBitmap(ENTITY ID, int nSize)
{
	return LoadImage(ID, IMAGE_BITMAP, nSize, 0);
	}

HANDLE CModule::LoadCursor(ENTITY ID)
{
	HANDLE hFile = LocateResource(ID, RT_GROUP_CURSOR);

	return ::LoadCursor(hFile, ID.GetName());
	}

HANDLE CModule::LoadCursor(ENTITY ID, int nSize)
{
	return LoadImage(ID, IMAGE_CURSOR, nSize, 0);
	}

HANDLE CModule::LoadIcon(ENTITY ID)
{
	HANDLE hFile = LocateResource(ID, RT_GROUP_ICON);

	return ::LoadIcon(hFile, ID.GetName());
	}

HANDLE CModule::LoadIcon(ENTITY ID, int nSize)
{
	return LoadImage(ID, IMAGE_ICON, nSize, 0);
	}

HANDLE CModule::LoadMenu(ENTITY ID)
{
	HANDLE hFile = LocateResource(ID, RT_MENU);

	return ::LoadMenu(hFile, ID.GetName());
	}

HANDLE CModule::LoadAccelerator(ENTITY ID)
{
	HANDLE hFile = LocateResource(ID, RT_ACCELERATOR);

	return ::LoadAccelerators(hFile, ID.GetName());
	}

// Image Loading

HANDLE CModule::LoadImage(ENTITY ID, UINT uType, int nSize, UINT uFlags)
{
	HANDLE hFile = LocateImage(ID, uType);

	return ::LoadImage(hFile, ID.GetName(), uType, nSize, nSize, uFlags);
	}

// String Loading

UINT CModule::LoadString(ENTITY ID, PTXT pBuffer, UINT uSize)
{
	HANDLE hFile = LocateResource(ID, RT_STRING);

	UINT uResult = ::LoadString(hFile, ID.GetID(), pBuffer, uSize);

	if( m_pOemHook ) {

		if( uResult ) {

			(*m_pOemHook)(pBuffer, uSize);

			return uSize;
			}
		}

	return uResult;
	}

// Global Class Location

CLASS CModule::FindClass(ENTITY ID)
{
	CModule *pCaller = ID.GetModule();

	CString  Name    = ID.GetName();

	CLASS    Class   = NULL;

	if( m_LastName == Name ) {

		return m_LastClass;
		}

	if( Class = pCaller->FindClass(Name) ) {

		m_LastName  = Name;

		m_LastClass = Class;

		return Class;
		}

	if( TRUE ) {

		for( CModule *pModule = m_pTail; pModule; pModule = pModule->m_pPrev ) {

			if( pModule == pCaller ) {

				continue;
				}

			if( pCaller->IsServer() ) {

				switch( pModule->m_Type ) {

					case modApplication:
					case modUserLibrary:
					case modInProcServer:

						continue;
					}
				}
			else {
				switch( pModule->m_Type ) {

					case modInProcServer:

						continue;
					}
				}

			if( Class = pModule->FindClass(Name) ) {

				m_LastName  = Name;

				m_LastClass = Class;

				return Class;
				}
			}
		}

	return NULL;
	}

// Local Class Location

CLASS CModule::FindClass(CString const &Name)
{
	INDEX i = m_NameMap.FindName(Name);

	if( !m_NameMap.Failed(i) ) {

		CLASS Class = m_NameMap[i];

		return Class;
		}

	return NULL;
	}

// Class Registration

void CModule::RegisterClass(ICLASS Class)
{
	if( Class->IsComponent() ) {

		REFGUID Guid = Class->GetClassGuid();

		m_GuidMap.Insert(Guid, Class);
		}

	CString Name = Class->GetClassName();

	m_NameMap.Insert(Name, Class);
	}

// Class Enumeration

INDEX CModule::GetHeadClass(void)
{
	return m_NameMap.GetHead();
	}

CLASS CModule::GetClassEntry(INDEX Index)
{
	return m_NameMap.Failed(Index) ? NULL : m_NameMap.GetData(Index);
	}

BOOL CModule::GetNextClass(INDEX &Index)
{
	return m_NameMap.GetNext(Index);
	}

// Object Management

LONG CModule::GetObjectCount(void) const
{
	return m_Type == modInProcServer ? m_nObjects : m_pApp->m_nObjects;
	}

void CModule::ObjectCreated(void)
{
	if( m_Type == modInProcServer ) {

		InterlockedIncrement(&m_nObjects);

		return;
		}

	InterlockedIncrement(&m_pApp->m_nObjects);
	}

void CModule::ObjectDeleted(void)
{
	if( m_Type == modInProcServer ) {

		InterlockedDecrement(&m_nObjects);

		return;
		}
		
	InterlockedDecrement(&m_pApp->m_nObjects);
	}

void CModule::LockServer(void)
{
	AfxAssert(m_Type == modInProcServer || m_Type == modApplication);

	InterlockedIncrement(&m_nLocks);
	}

void CModule::UnlockServer(void)
{
	AfxAssert(m_Type == modInProcServer || m_Type == modApplication);

	InterlockedDecrement(&m_nLocks);
	}

// OEM Support

void CModule::SetOemHook(POEMHOOK pOemHook)
{
	m_pOemHook = pOemHook;
	}

// Implementation

BOOL CModule::CommonInit(void)
{
	FindFileNames();

	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);

	CRuntimeClass::Register(this);

	return TRUE;
	}

void CModule::FindFileNames(void)
{
	TCHAR sBuffer[MAX_PATH];
	
	GetModuleFileName(m_hInstance, sBuffer, elements(sBuffer));
	
	wstrlwr(sBuffer);

	m_Filename = sBuffer;
	}

void CModule::ParseCommandLine(void)
{
	CString Line = m_pCmdLine;

	UINT    uPos = 0;

	Line.TrimBoth();

	while( Line.GetLength() ) {

		TCHAR cFind = ' ';

		if( Line[0] == '\"' || Line[0] == '\'' ) {

			cFind = Line[0];

			Line  = Line.Mid(1);
			}

		if( (uPos = Line.Find(cFind)) == NOTHING ) {

			m_ArgList.Append(Line);

			break;
			}

		CString Text = Line.Left(uPos);

		Text.TrimBoth();

		if( !Text.IsEmpty() ) {

			m_ArgList.Append(Text);
			}

		while( Line[++uPos] == cFind );

		Line = Line.Mid(uPos);
		}

	m_ArgList.Remove(0);
	}

void CModule::DeleteInterfaceMaps(void)
{
	INDEX i = m_NameMap.GetHead();

	while( !m_NameMap.Failed(i) ) {

		m_NameMap[i]->DeleteInterfaceMap();

		m_NameMap.GetNext(i);
		}
	}

void CModule::DeleteResources(void)
{
	INDEX i = m_ResMap.GetHead();

	while( !m_ResMap.Failed(i) ) {

		if( m_ResMap[i] == m_hInstance ) {

			HANDLE hNull = NULL;

			m_ResMap.SetData(i, hNull);
			}

		m_ResMap.GetNext(i);
		}
	}

// Factory Mananagement

void CModule::RegisterAllFactories(void)
{
	CModule *pModule = m_pHead;

	while( pModule ) {

		switch( pModule->m_Type ) {
			
			case modApplication:
			case modCoreLibrary:
			case modUserLibrary:

				pModule->RegisterFactories();

				break;
			}

		pModule = pModule->m_pNext;
		}
	}

void CModule::RevokeAllFactories(void)
{
	CModule *pModule = m_pHead;

	while( pModule ) {

		switch( pModule->m_Type ) {
			
			case modApplication:
			case modCoreLibrary:
			case modUserLibrary:

				pModule->RevokeFactories();

				break;
			}

		pModule = pModule->m_pNext;
		}
	}

void CModule::RegisterFactories(void)
{
	INDEX i = m_GuidMap.GetHead();
	
	while( !m_GuidMap.Failed(i) ) {

		ICLASS Class = m_GuidMap[i];

		Class->RegisterFactory();

		m_GuidMap.GetNext(i);
		}
	}

void CModule::RevokeFactories(void)
{
	INDEX i = m_GuidMap.GetHead();
	
	while( !m_GuidMap.Failed(i) ) {

		ICLASS Class = m_GuidMap[i];

		Class->RevokeFactory();

		m_GuidMap.GetNext(i);
		}
	}

// Resource Mapping

HANDLE CModule::SearchResMap(PCTXT pName, PCTXT pType)
{
	CResTag Tag(pName, pType);

	for(;;) {

		CCriticalGuard Guard(m_ResSect);
		
		INDEX i = m_ResMap.FindName(Tag);

		if( !m_ResMap.Failed(i) ) {

			HANDLE hFile = m_ResMap[i];

			if( !hFile ) {

				m_ResMap.Remove(i);

				continue;
				}

			return hFile;
			}

		return NULL;
		}
	}

void CModule::InsertResMap(PCTXT pName, PCTXT pType, HANDLE hFile)
{
	CResTag Tag(pName, pType);

	CCriticalGuard Guard(m_ResSect);

	m_ResMap.Insert(Tag, hFile);
	}

// End of File
