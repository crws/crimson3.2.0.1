
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Critical Section Object
//

// Constructor

CCriticalSection::CCriticalSection(void)
{
	InitializeCriticalSection(&m_Critical);
	}

// Destructor

CCriticalSection::~CCriticalSection(void)
{
	DeleteCriticalSection(&m_Critical);
	}

// Operations

void CCriticalSection::Enter(void)
{
	EnterCriticalSection(&m_Critical);
	}

void CCriticalSection::Leave(void)
{
	LeaveCriticalSection(&m_Critical);
	}

//////////////////////////////////////////////////////////////////////////
//
// Critical Section Guard
//

// Constructor

CCriticalGuard::CCriticalGuard(CCriticalSection *pSection)
{
	m_pSection = NULL;
	
	Attach(pSection);
	}

CCriticalGuard::CCriticalGuard(CCriticalSection &Section)
{
	m_pSection = NULL;
	
	Attach(Section);
	}

// Destructor

CCriticalGuard::~CCriticalGuard(void)
{
	Detach();
	}

// Attach/Detach

BOOL CCriticalGuard::Attach(CCriticalSection &Section)
{
	Detach();

	m_pSection = &Section;

	m_pSection->Enter();

	return TRUE;
	}

BOOL CCriticalGuard::Attach(CCriticalSection *pSection)
{
	Detach();

	if( (m_pSection = pSection) ) {

		m_pSection->Enter();

		return TRUE;
		}

	return FALSE;
	}

void CCriticalGuard::Detach(void)
{
	if( m_pSection ) {
			
		m_pSection->Leave();
		}

	m_pSection = NULL;
	}

// End of File
