
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Framework Window Class
//

// Message Box Hook

MSGBOX CWnd::m_pMsgBox = DefMsgBox;

// Property Atom

CGlobalAtom CWnd::m_PropAtom(L"RED-LION-CWND");

// Dynamic Class

AfxImplementDynamicClass(CWnd, CCmdTarget);

// Constructor

CWnd::CWnd(void)
{
	m_hWnd     = NULL;

	m_pfnSuper = NULL;

	m_pfnWider = NULL;
	}

// Destructor

CWnd::~CWnd(void)
{
	Detach();
	}

// Normal Creation

BOOL CWnd::Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData)
{
	return Create(L"", dwExStyle, dwStyle, Rect, hParent, hMenu, pData);
	}

BOOL CWnd::Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData)
{
	return Create(L"", dwExStyle, dwStyle, Rect, Parent.GetHandle(), Menu, pData);
	}

BOOL CWnd::Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData)
{
	return Create(L"", dwExStyle, dwStyle, Rect, Parent.GetHandle(), HMENU(uID), pData);
	}

BOOL CWnd::Create(DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData)
{
	return Create(L"", 0, dwStyle, Rect, hParent, hMenu, pData);
	}

BOOL CWnd::Create(DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData)
{
	return Create(L"", 0, dwStyle, Rect, Parent.GetHandle(), Menu, pData);
	}

BOOL CWnd::Create(DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData)
{
	return Create(L"", 0, dwStyle, Rect, Parent.GetHandle(), HMENU(uID), pData);
	}

BOOL CWnd::Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData)
{
	RegisterClass();

	CPoint Pos(CW_USEDEFAULT, CW_USEDEFAULT);

	CSize Size(CW_USEDEFAULT, CW_USEDEFAULT);

	if( !Rect.IsEmpty() ) {

		Pos  = Rect.GetTopLeft();

		Size = Rect.GetSize();
		}

	if( AfxInstallHook(this) ) {
	
		HWND hWnd = CreateWindowEx( dwExStyle,
					    GetDefaultClassName(),
					    pName,
					    dwStyle,
					    Pos.x, Pos.y, Size.cx, Size.cy,
					    hParent,
					    hMenu,
	    				    afxModule->GetAppInstance(),
					    pData
					    );
	
		if( hWnd ) {
	
			AfxAssert(m_hWnd == hWnd);
			
			if( !hParent && afxThread ) {
				
				if( afxMainWnd == NULL ) {

					afxThread->SetMainWnd(ThisObject);
					}
				}
	
			SendMessage(WM_POSTCREATE);
	
			return TRUE;
			}
	
		AfxRemoveHook();
		}

	AfxTrace(L"WARNING: Failed to create %s window\n", GetDefaultClassName());

	AfxThrowResourceException();

	return FALSE;
	}

BOOL CWnd::Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData)
{
	AfxValidateObject(Parent);

	AfxValidateObject(Menu);

	Menu.SetExtern(TRUE);

	return Create(pName, dwExStyle, dwStyle, Rect, Parent.GetHandle(), Menu.GetHandle(), pData);
	}

BOOL CWnd::Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData)
{
	AfxValidateObject(Parent);

	AfxAssert(!uID || (dwStyle & WS_CHILD));

	return Create(pName, dwExStyle, dwStyle, Rect, Parent.GetHandle(), HMENU(uID), pData);
	}

BOOL CWnd::Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData)
{
	return Create(pName, 0, dwStyle, Rect, hParent, hMenu, pData);
	}

BOOL CWnd::Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData)
{
	return Create(pName, 0, dwStyle, Rect, Parent, Menu, pData);
	}

BOOL CWnd::Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData)
{
	return Create(pName, 0, dwStyle, Rect, Parent, uID, pData);
	}

// Sub-Classing

BOOL CWnd::SubClassWindow(void)
{
	AfxAssert(IsWindow());

	if( !m_pfnSuper ) {
	
		FARPROC pfnWndProc = FARPROC(GetWindowLong(GWL_WNDPROC));

		if( pfnWndProc == FARPROC(AfxWndProc) ) {

			return FALSE;
			}
		
		if( afxMap->FromHandle(m_hWnd, NS_HWND) ) {
			
			if( afxMap->IsTemporary(m_hWnd, NS_HWND) ) {
	
				afxMap->RemoveTemp(m_hWnd, NS_HWND, this);
	
				afxMap->InsertPerm(m_hWnd, NS_HWND, this);
				}
			}

		m_pfnWider = FARPROC(::GetWindowLong(m_hWnd, GWL_WNDPROC));

		m_pfnSuper = pfnWndProc;

		SetWindowLong(GWL_WNDPROC, (LONG) (FARPROC) AfxWndProc);
	
		SendMessage(WM_SUBCLASSED);

		return TRUE;
		}
		
	return FALSE;
	}

// Destruction

void CWnd::DestroyWindow(BOOL fImmediate)
{
	SendMessage(WM_PREDESTROY);

	if( fImmediate ) {

		::DestroyWindow(m_hWnd);

		return;
		}

	PostMessage(WM_PLEASEKILLME);
	}
	
// Attachment

BOOL CWnd::Attach(HANDLE hWnd)
{
	if( hWnd ) {

		AfxAssert(!m_hWnd);

		m_hWnd = hWnd;

		SetProp(m_PropAtom, HANDLE(this));
		
		afxMap->InsertPerm(hWnd, NS_HWND, this);

		return TRUE;
		}

	return FALSE;
	}

void CWnd::Detach(void)
{
	if( m_hWnd ) {

		afxMap->RemoveBoth(m_hWnd, NS_HWND, this);

		if( m_pfnSuper ) {
		
			SetWindowLong(GWL_WNDPROC, LONG(m_pfnSuper));

			m_pfnWider = NULL;

			m_pfnSuper = NULL;
			}

		RemoveProp(m_PropAtom);

		m_hWnd = NULL;
		}
	}

// Attributes

HWND CWnd::GetHandle(void) const
{
	return m_hWnd;
	}

BOOL CWnd::IsWindow(void) const
{
	return this && m_hWnd && ::IsWindow(m_hWnd);
	}

BOOL CWnd::IsValid(void) const
{
	return m_hWnd ? TRUE : FALSE;
	}

BOOL CWnd::IsNull(void) const
{
	return m_hWnd ? FALSE : TRUE;
	}

BOOL CWnd::operator ! (void) const
{
	return m_hWnd ? FALSE : TRUE;
	}

BOOL CWnd::IsEnabled(void) const
{
	return ::IsWindowEnabled(m_hWnd);
	}

BOOL CWnd::InPrintClient(void) const
{
	return m_MsgCtx.Msg.message == WM_PRINTCLIENT;
	}

HDC CWnd::GetPrintDC(void) const
{
	return HDC(m_MsgCtx.Msg.wParam);
	}
		
// Conversion

CWnd::operator HWND (void) const
{
	return m_hWnd;
	}

// Comparision Operators

BOOL CWnd::operator == (CWnd const &Wnd)
{
	return BOOL(m_hWnd == Wnd.m_hWnd);
	}

BOOL CWnd::operator == (HWND hWnd)
{
	return BOOL(m_hWnd == hWnd);
	}

BOOL CWnd::operator != (CWnd const &Wnd)
{
	return BOOL(m_hWnd != Wnd.m_hWnd);
	}

BOOL CWnd::operator != (HWND hWnd)
{
	return BOOL(m_hWnd != hWnd);
	}

// Message Handling (Standard)

LRESULT CWnd::SendMessage(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	return ::SendMessage(m_hWnd, uMessage, wParam, lParam);
	}
	
BOOL CWnd::PostMessage(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	return ::PostMessage(m_hWnd, uMessage, wParam, lParam);
	}

// Message Handling (Constant)

LRESULT CWnd::SendMessageConst(UINT uMessage, WPARAM wParam, LPARAM lParam) const
{
	return ::SendMessage(m_hWnd, uMessage, wParam, lParam);
	}
	
BOOL CWnd::PostMessageConst(UINT uMessage, WPARAM wParam, LPARAM lParam) const
{
	return ::PostMessage(m_hWnd, uMessage, wParam, lParam);
	}

// Message Broadcast

void CWnd::SendBroadcast(UINT uCode)
{
	MSG     Message = { NULL, WM_BROADCAST, uCode, 0, 0 };

	LRESULT lResult = 0;

	afxMainWnd->RouteMessage(Message, lResult);
	}

// Window Text Functions

CString CWnd::GetWindowText(void) const
{
	UINT uCount = ::GetWindowTextLength(m_hWnd);
	
	if( uCount ) {

		CString Result(' ', uCount);
	
		::GetWindowText(m_hWnd, PTXT(PCTXT(Result)), uCount + 1);
	
		return Result;
		}

	return L"";
	}

UINT CWnd::GetWindowTextLength(void) const
{
	return UINT(::GetWindowTextLength(m_hWnd));
	}

void CWnd::SetWindowText(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( GetWindowTextLength() == UINT(wstrlen(pText)) ) {
	
		if( !wstrcmp(GetWindowText(), pText) ) {
		
			return;
			}
		}
		
	::SetWindowText(m_hWnd, pText);
	}

void CWnd::SetWindowText(PCSTR pText)
{
	SetWindowText(CString(pText));
	}

// Window Tree Access

BOOL CWnd::HasParent(void) const
{
	return ::GetParent(m_hWnd) ? TRUE : FALSE;
	}

CWnd & CWnd::GetParent(void) const
{
	return FromHandle(::GetParent(m_hWnd));
	}
	
CWnd & CWnd::GetParent(UINT uLevel) const
{
	AfxAssert(IsWindow());

	HWND hWnd = m_hWnd;

	while( uLevel-- ) {
	
		HWND hNext = ::GetParent(hWnd);
		
		if( hNext ) {

			hWnd = hNext;

			continue;
			}

		break;
		}

	return FromHandle(hWnd);
	}

CWnd & CWnd::GetParent(CLASS Class) const
{
	AfxAssert(IsWindow());

	CWnd *pWnd = (CWnd *) this;

	while( pWnd && pWnd->IsWindow() ) {

		if( pWnd->IsKindOf(Class) ) {

			return *pWnd;
			}

		pWnd = &pWnd->GetParent();
		}

	return AfxNull(CWnd);
	}

CWnd & CWnd::GetTopParent(void) const
{
	AfxAssert(IsWindow());

	HWND hWnd = m_hWnd;
	
	for(;;) {
	
		HWND hNext = ::GetParent(hWnd);
		
		if( hNext ) {

			hWnd = hNext;

			continue;
			}

		break;
		}
	
	return FromHandle(hWnd);
	}

CWnd & CWnd::GetDlgParent(void) const
{
	AfxAssert(IsWindow());

	CWnd *pWnd = (CWnd *) this;

	// cppcheck-suppress nullPointerRedundantCheck

	if( pWnd ) {

		while( (pWnd = &pWnd->GetParent()) ) {
		
			if( !(pWnd->GetWindowStyle() & WS_CHILD) ) {

				break;
				}
			}
		}

	return *pWnd;
	}
	
UINT CWnd::GetControlID(void) const
{
	return UINT(::GetWindowLong(m_hWnd, GWL_ID));
	}
	
CWnd & CWnd::GetDlgItem(UINT uID) const
{
	return FromHandle(::GetDlgItem(m_hWnd, uID));
	}

CWnd & CWnd::GetDlgItem(UINT uID, CLASS Class) const
{
	return FromHandle(::GetDlgItem(m_hWnd, uID), Class);
	}

CWnd * CWnd::GetDlgItemPtr(UINT uID) const
{
	return & FromHandle(::GetDlgItem(m_hWnd, uID));
	}

CWnd * CWnd::GetDlgItemPtr(UINT uID, CLASS Class) const
{
	return & FromHandle(::GetDlgItem(m_hWnd, uID), Class);
	}

HWND CWnd::GetDlgItemHandle(UINT uID) const
{
	return ::GetDlgItem(m_hWnd, uID);
	}

CWnd & CWnd::GetWindow(UINT uRelation) const
{
	return FromHandle(::GetWindow(m_hWnd, uRelation));
	}
	
CWnd * CWnd::GetWindowPtr(UINT uRelation) const
{
	return & FromHandle(::GetWindow(m_hWnd, uRelation));
	}
	
HWND CWnd::GetWindowHandle(UINT uRelation) const
{
	return ::GetWindow(m_hWnd, uRelation);
	}
	
// Get Size and Position

BOOL CWnd::IsIconic(void) const
{
	return ::IsIconic(m_hWnd);
	}

BOOL CWnd::IsZoomed(void) const
{
	return ::IsZoomed(m_hWnd);
	}

CRect CWnd::GetWindowRect(void) const
{
	CRect Result;
	
	::GetWindowRect(m_hWnd, &Result);
	
	return Result;
	}

void CWnd::GetWindowRect(CRect &Rect) const
{
	::GetWindowRect(m_hWnd, &Rect);
	}

CRect CWnd::GetClientRect(void) const
{
	CRect Result;
	
	::GetClientRect(m_hWnd, &Result);
	
	return Result;
	}

void CWnd::GetClientRect(CRect &Rect) const
{
	::GetClientRect(m_hWnd, &Rect);
	}

// Change Size and Position

void CWnd::MoveWindow(CRect const &Rect, BOOL fPaint)
{
	::MoveWindow(m_hWnd, Rect.left, Rect.top, Rect.GetWidth(), Rect.GetHeight(), fPaint);
	}

void CWnd::MoveWindow(int x, int y, int cx, int cy, BOOL fPaint)
{
	::MoveWindow(m_hWnd, x, y, cx, cy, fPaint);
	}

void CWnd::SetWindowPos(HWND hWnd, int x, int y, int cx, int cy, UINT uFlags)
{
	::SetWindowPos(m_hWnd, hWnd, x, y, cx, cy, uFlags);
	}

void CWnd::SetWindowPos(CPoint const &Pos, CSize const &Size, UINT uFlags)
{
	::SetWindowPos(m_hWnd, NULL, Pos.x, Pos.y, Size.cx, Size.cy, uFlags | SWP_NOZORDER);
	}

void CWnd::SetWindowPos(CRect const &Rect, UINT uFlags)
{
	::SetWindowPos(m_hWnd, NULL, Rect.left, Rect.top, Rect.GetWidth(), Rect.GetHeight(), uFlags | SWP_NOZORDER);
	}

void CWnd::SetWindowOrg(CPoint const &Pos, BOOL fPaint)
{
	UINT uFlags = SWP_NOZORDER | SWP_NOSIZE | SWP_NOACTIVATE;
	
	if( !fPaint ) uFlags |= SWP_NOREDRAW;
	
	::SetWindowPos(m_hWnd, NULL, Pos.x, Pos.y, 0, 0, uFlags);
	}

void CWnd::SetWindowOrg(int xPos, int yPos, BOOL fPaint)
{
	UINT uFlags = SWP_NOZORDER | SWP_NOSIZE | SWP_NOACTIVATE;
	
	if( !fPaint ) uFlags |= SWP_NOREDRAW;
	
	::SetWindowPos(m_hWnd, NULL, xPos, yPos, 0, 0, uFlags);
	}

void CWnd::SetWindowSize(CSize const &Size, BOOL fPaint)
{
	UINT uFlags = SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE;
	
	if( !fPaint ) uFlags |= SWP_NOREDRAW;
	
	::SetWindowPos(m_hWnd, NULL, 0, 0, Size.cx, Size.cy, uFlags);
	}

void CWnd::SetWindowSize(int xSize, int ySize, BOOL fPaint)
{
	UINT uFlags = SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE;
	
	if( !fPaint ) uFlags |= SWP_NOREDRAW;
	
	::SetWindowPos(m_hWnd, NULL, 0, 0, xSize, ySize, uFlags);
	}

void CWnd::SetWindowOrder(HWND hWnd, BOOL fPaint)
{
	UINT uFlags = SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE;
	
	if( !fPaint ) uFlags |= SWP_NOREDRAW;
	
	::SetWindowPos(m_hWnd, hWnd, 0, 0, 0, 0, uFlags);
	}

void CWnd::BringWindowToTop(void)
{
	::BringWindowToTop(m_hWnd);
	}

void CWnd::CloseWindow(void)
{
	::CloseWindow(m_hWnd);
	}
	
BOOL CWnd::OpenIcon(void)
{
	return ::OpenIcon(m_hWnd);
	}

void CWnd::MakeTopmost(BOOL fState)
{
	UINT uFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER;

	HWND hWnd   = fState ? HWND_TOPMOST : HWND_NOTOPMOST;

	::SetWindowPos(m_hWnd, hWnd, 0, 0, 0, 0, uFlags);
	}

// Coordinate Mapping

void CWnd::ClientToScreen(POINT &Point) const
{
	::ClientToScreen(m_hWnd, &Point);
	}

void CWnd::ClientToScreen(SIZE &Size) const
{
	::ClientToScreen(m_hWnd, PPOINT(&Size));
	}

void CWnd::ClientToScreen(RECT &Rect) const
{
	::ClientToScreen(m_hWnd, &PPOINT(&Rect)[0]);
	
	::ClientToScreen(m_hWnd, &PPOINT(&Rect)[1]);
	}

void CWnd::ScreenToClient(POINT &Point) const
{
	::ScreenToClient(m_hWnd, &Point);
	}

void CWnd::ScreenToClient(SIZE &Size) const
{
	::ScreenToClient(m_hWnd, PPOINT(&Size));
	}

void CWnd::ScreenToClient(RECT &Rect) const
{
	::ScreenToClient(m_hWnd, &PPOINT(&Rect)[0]);
	
	::ScreenToClient(m_hWnd, &PPOINT(&Rect)[1]);
	}

void CWnd::ClientToWindow(POINT &Point) const
{
	CPoint Offset;

	ClientToScreen(Offset);

	Offset -= GetWindowRect().GetTopLeft();

	((CPoint &) Point) += Offset;
	}

void CWnd::ClientToWindow(SIZE &Size) const
{
	ClientToWindow((POINT &) Size);
	}

void CWnd::ClientToWindow(RECT &Rect) const
{
	ClientToWindow(PPOINT(&Rect)[0]);

	ClientToWindow(PPOINT(&Rect)[1]);
	}

void CWnd::WindowToClient(POINT &Point) const
{
	CPoint Offset;

	ClientToScreen(Offset);

	Offset -= GetWindowRect().GetTopLeft();

	((CPoint &) Point) -= Offset;
	}

void CWnd::WindowToClient(SIZE &Size) const
{
	WindowToClient((POINT &) Size);
	}

void CWnd::WindowToClient(RECT &Rect) const
{
	WindowToClient(PPOINT(&Rect)[0]);

	WindowToClient(PPOINT(&Rect)[1]);
	}

// Update and Painting

void CWnd::UpdateWindow(void)
{
	::UpdateWindow(m_hWnd);
	}

void CWnd::SetRedraw(BOOL fRedraw)
{
	::SendMessage(m_hWnd, WM_SETREDRAW, fRedraw, 0L);
	}

CRect CWnd::GetUpdateRect(void)
{
	CRect Result;
	
	::GetUpdateRect(m_hWnd, &Result, FALSE);
	
	return Result;
	}

void CWnd::GetUpdateRect(CRect &Rect)
{
	::GetUpdateRect(m_hWnd, &Rect, FALSE);
	}

void CWnd::Invalidate(BOOL fErase)
{
	::InvalidateRect(m_hWnd, NULL, fErase);
	}

void CWnd::Invalidate(CRect const &Rect, BOOL fErase)
{
	::InvalidateRect(m_hWnd, &Rect, fErase);
	}

void CWnd::InvalidateAll(BOOL fErase)
{
	CWnd *pWnd = GetWindowPtr(GW_CHILD);
	
	while( pWnd->GetHandle() ) {
	
		pWnd->InvalidateAll(fErase);
		
		pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
		}
		
	Invalidate(fErase);
	}

void CWnd::Validate(void)
{
	::ValidateRect(m_hWnd, NULL);
	}

void CWnd::Validate(CRect const &Rect)
{
	::ValidateRect(m_hWnd, &Rect);
	}

void CWnd::ValidateAll(void)
{
	CWnd *pWnd = GetWindowPtr(GW_CHILD);
	
	while( pWnd->GetHandle() ) {
	
		pWnd->ValidateAll();
		
		pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
		}
		
	Validate();
	}

BOOL CWnd::ShowWindow(int nCmdShow)
{
	if( IsWindow() ) {

		DWORD ID;

		GetWindowThreadProcessId(m_hWnd, &ID);

		if( ID == GetCurrentProcessId() ) {

			::ShowWindow(m_hWnd, nCmdShow);

			return TRUE;
			}

		AfxAssert(FALSE);
		}

	return FALSE;
	}

BOOL CWnd::ShowForeignWindow(int nCmdShow)
{
	if( m_hWnd ) {

		::ShowWindow(m_hWnd, nCmdShow);

		return TRUE;
		}

	return FALSE;
	}

BOOL CWnd::IsWindowVisible(void) const
{
	return ::IsWindowVisible(m_hWnd);
	}

void CWnd::ShowOwnedPopups(BOOL bShow)
{
	::ShowOwnedPopups(m_hWnd, bShow);
	}

// Get Window State

BOOL CWnd::IsWindowEnabled(void) const
{
	return ::IsWindowEnabled(m_hWnd);
	}

BOOL CWnd::HasFocus(void) const
{
	return BOOL(::GetFocus() == m_hWnd);
	}
	
BOOL CWnd::IsActive(void) const
{
	return BOOL(::GetActiveWindow() == m_hWnd);
	}

BOOL CWnd::HasCapture(void) const
{
	return BOOL(::GetCapture() == m_hWnd);
	}
	
// Get Given Window

CWnd & CWnd::GetFocus(void)
{
	return FromHandle(::GetFocus());
	}

CWnd & CWnd::GetActiveWindow(void)
{
	return FromHandle(::GetActiveWindow());
	}

CWnd & CWnd::GetCapture(void)
{
	return FromHandle(::GetCapture());
	}

// Modify Window State

void CWnd::EnableWindow(BOOL fEnable)
{
	::EnableWindow(m_hWnd, fEnable);
	}

void CWnd::EnableWindow(void)
{
	::EnableWindow(m_hWnd, TRUE);
	}

void CWnd::DisableWindow(void)
{
	::EnableWindow(m_hWnd, FALSE);
	}

void CWnd::SetFocus(void)
{
	::SetFocus(m_hWnd);
	}

void CWnd::SetActiveWindow(void)
{
	::SetActiveWindow(m_hWnd);
	}

void CWnd::SetCapture(void)
{
	::SetCapture(m_hWnd);
	}

void CWnd::ReleaseCapture(void)
{
	::ReleaseCapture();
	}

void CWnd::SetTopActive(void)
{
	BringWindowToTop();

	ShowWindow(SW_SHOWNA);

	SetActiveWindow();

	SetFocus();
	}
		
// Window Data Functions

WORD CWnd::GetWindowWord(int nOffset) const
{
	return ::GetWindowWord(m_hWnd, nOffset);
	}

void CWnd::SetWindowWord(int nOffset, WORD wData)
{
	::SetWindowWord(m_hWnd, nOffset, wData);
	}

LONG CWnd::GetWindowLong(int nOffset) const
{
	return ::GetWindowLong(m_hWnd, nOffset);
	}

void CWnd::SetWindowLong(int nOffset, LONG lData)
{
	::SetWindowLong(m_hWnd, nOffset, lData);
	}
		
// Window Property Functions

HANDLE CWnd::GetProp(PCTXT pText)
{
	return ::GetProp(m_hWnd, pText);
	}

BOOL CWnd::SetProp(PCTXT pText, HANDLE hData)
{
	return ::SetProp(m_hWnd, pText, hData);
	}

HANDLE CWnd::RemoveProp(PCTXT pText)
{
	return ::RemoveProp(m_hWnd, pText);
	}

// Rectangle Adjustment

void CWnd::AdjustWindowRect(CRect &Rect) const
{
	AdjustWindowRect(Rect, GetWindowStyle());
	}

void CWnd::AdjustWindowSize(CSize &Size) const
{
	AdjustWindowSize(Size, GetWindowStyle());
	}

void CWnd::AdjustWindowRect(CRect &Rect, DWORD dwStyle) const
{
	BOOL fMenu = FALSE;

	if( !((dwStyle & (WS_CHILD | WS_POPUP)) == WS_CHILD) ) {

		if( ::GetMenu(m_hWnd) ) {
			
			fMenu = TRUE;
			}
		}

	AdjustWindowRectEx(Rect, dwStyle, fMenu, GetWindowExStyle());
	}

void CWnd::AdjustWindowSize(CSize &Size, DWORD dwStyle) const
{
	CRect Rect = CRect(Size);
	
	AdjustWindowRect(Rect, dwStyle);
	
	Size = Rect.GetSize();
	}

// Style Functions

DWORD CWnd::GetWindowStyle(void) const
{
	return GetWindowLong(GWL_STYLE);
	}

void CWnd::SetWindowStyle(DWORD dwMask, DWORD dwData)
{
	SetWindowLong(GWL_STYLE, (GetWindowStyle() & ~dwMask) | (dwData & dwMask));
	}

DWORD CWnd::GetWindowExStyle(void) const
{
	return GetWindowLong(GWL_EXSTYLE);
	}

void CWnd::SetWindowExStyle(DWORD dwMask, DWORD dwData)
{
	SetWindowLong(GWL_EXSTYLE, (GetWindowExStyle() & ~dwMask) | (dwData & dwMask));
	}

// Class Functions

WORD CWnd::GetClassWord(int nOffset) const
{
	return ::GetClassWord(m_hWnd, nOffset);
	}

void CWnd::SetClassWord(int nOffset, WORD wData)
{
	::SetClassWord(m_hWnd, nOffset, wData);
	}

LONG CWnd::GetClassLong(int nOffset) const
{
	return ::GetClassLong(m_hWnd, nOffset);
	}

void CWnd::SetClassLong(int nOffset, LONG lData)
{
	::SetClassLong(m_hWnd, nOffset, lData);
	}
		
CString CWnd::GetWindowClassName(void) const
{
	TCHAR sBuffer[256];
	
	UINT uCount = ::GetClassName(m_hWnd, sBuffer, sizeof(sBuffer));
	
	AfxAssert(uCount < sizeof(sBuffer));
	
	return CString(sBuffer);
	}

BOOL CWnd::IsClass(PCTXT pName) const
{
	TCHAR sBuffer[256];
	
	UINT uCount = ::GetClassName(m_hWnd, sBuffer, sizeof(sBuffer));

	AfxAssert(uCount < sizeof(sBuffer));
	
	return BOOL(wstricmp(sBuffer, pName) == 0);
	}

BOOL CWnd::IsValidClass(PCTXT pName) const
{
	WNDCLASS Class;
	
	if( GetClassInfo(NULL, pName, &Class) ) {

		return TRUE;
		}

	if( GetClassInfo(afxModule->GetAppInstance(), pName, &Class) ) {

		return TRUE;
		}
		
	return FALSE;
	}

// Font Operations

HFONT CWnd::GetFont(void) const
{
	return HFONT(SendMessageConst(WM_GETFONT));
	}

void CWnd::SetFont(CFont const &Font)
{
	AfxValidateObject(Font);
	
	SendMessage(WM_SETFONT, WPARAM(Font.GetHandle()));
	}

// Icon Operations

CIcon & CWnd::GetIcon(void) const
{
	return CIcon::FromHandle(HICON(SendMessageConst(WM_GETICON, ICON_SMALL)));
	}

void CWnd::SetIcon(CIcon const &Icon)
{
	SendMessage(WM_SETICON, WPARAM(ICON_SMALL), LPARAM(Icon.GetHandle()));
	}

// Dialog and Controls

UINT CWnd::GetID(void) const
{
	return UINT(::GetWindowLong(m_hWnd, GWL_ID));
	}
	
// Menu Functions

void CWnd::SetMenu(CMenu &Menu)
{
	AfxValidateObject(Menu);

	CMenu &Prev = GetMenu();

	Prev.FreeOwnerDraw();

	Prev.SetExtern(FALSE);
	
	::SetMenu(m_hWnd, Menu.GetHandle());
	
	Menu.SetExtern(TRUE);
	}

CMenu & CWnd::GetMenu(void) const
{
	return CMenu::FromHandle(::GetMenu(m_hWnd));
	}
	
CMenu & CWnd::GetSystemMenu(void) const
{
	return CMenu::FromHandle(::GetSystemMenu(m_hWnd, FALSE));
	}

void CWnd::ResetSystemMenu(void)
{
	::GetSystemMenu(m_hWnd, TRUE);
	}

void CWnd::DrawMenuBar(void)
{
	::DrawMenuBar(m_hWnd);
	}

// Scroll Attributes

int CWnd::GetScrollPos(UINT uBar) const
{
	return ::GetScrollPos(m_hWnd, uBar);
	}

int CWnd::GetScrollRangeMin(UINT uBar) const
{
	int nMin, nMax;
	
	::GetScrollRange(m_hWnd, uBar, &nMin, &nMax);
	
	return nMin;
	}

int CWnd::GetScrollRangeMax(UINT uBar) const
{
	int nMin, nMax;
	
	::GetScrollRange(m_hWnd, uBar, &nMin, &nMax);
	
	return nMax;
	}

UINT CWnd::GetScrollPageSize(UINT uBar) const
{
	SCROLLINFO Info;

	Info.cbSize = sizeof(Info);
	
	Info.fMask  = SIF_PAGE;

	::GetScrollInfo(m_hWnd, uBar, &Info);

	return Info.nPage;
	}
		
// Scroll Operations

void CWnd::SetScrollPos(UINT uBar, int nPos, BOOL fPaint)
{
	::SetScrollPos(m_hWnd, uBar, nPos, fPaint);
	}

void CWnd::SetScrollRange(UINT uBar, int nMin, int nMax, BOOL fPaint)
{
	::SetScrollRange(m_hWnd, uBar, nMin, nMax, fPaint);
	}

void CWnd::SetScrollPageSize(UINT uBar, UINT uPage, BOOL fPaint)
{
	SCROLLINFO Info;

	Info.cbSize = sizeof(Info);
	
	Info.fMask  = SIF_PAGE;

	Info.nPage  = uPage;

	::SetScrollInfo(m_hWnd, uBar, &Info, fPaint);
	}

void CWnd::EnableScrollBar(UINT uBar, UINT uArrows)
{
	::EnableScrollBar(m_hWnd, uBar, uArrows);
	}

void CWnd::ShowScrollBar(UINT uBar, BOOL fShow)
{
	::ShowScrollBar(m_hWnd, uBar, fShow);
	}

// Parent Commands

void CWnd::SendParentCommand(UINT uID)
{
	GetParent().SendMessage(WM_AFX_COMMAND, WPARAM(uID));
	}

void CWnd::SendParentCommand(UINT uLevel, UINT uID)
{
	GetParent(uLevel).SendMessage(WM_AFX_COMMAND, WPARAM(uID));
	}

void CWnd::PostParentCommand(UINT uID)
{
	GetParent().PostMessage(WM_AFX_COMMAND, WPARAM(uID));
	}

void CWnd::PostParentCommand(UINT uLevel, UINT uID)
{
	GetParent(uLevel).PostMessage(WM_AFX_COMMAND, WPARAM(uID));
	}

// Timer Functions

void CWnd::SetTimer(UINT uID, UINT uPeriod)
{
	::SetTimer(m_hWnd, uID, uPeriod, NULL);
	}

void CWnd::KillTimer(UINT uID)
{
	::KillTimer(m_hWnd, uID);
	}

// Timer Allocation

UINT CWnd::AllocTimerID(void)
{
	static UINT uID = 100;
	
	return uID++;
	}

// WndProc Calling

LRESULT CWnd::CallWndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	AfxAssert(this);

	if( afxThread ) {
		
		if( uMessage == WM_SETCURSOR && afxThread->InWaitMode() ) {
	
			afxThread->SetWaitCursor();
		
			return 1L;
			}
		}

	if( uMessage == WM_PREDESTROY ) {

		CWnd *pWnd = GetWindowPtr(GW_CHILD);

		while( pWnd && pWnd->IsWindow() ) {

			pWnd->SendMessage(WM_PREDESTROY);

			pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
			}
		}

	if( uMessage == WM_NCDESTROY ) {

		HWND    hWnd   = m_hWnd;

		FARPROC pSuper = m_pfnSuper;

		Detach();

		if( afxThread ) {

			if( afxMainWnd == this ) {

				afxThread->ClearMainWnd();

				PostQuitMessage(0);
				}
			}

		OnNCDestroy();

		if( pSuper ) {

			return CallWindowProc(pSuper, hWnd, uMessage, wParam, lParam);
			}

		return DefWindowProc(hWnd, uMessage, wParam, lParam);
		}

	if( uMessage == WM_TAKEFOCUS ) {

		SetFocus();

		return 0;
		}
		
	if( uMessage == WM_SETFOCUS || uMessage == WM_FOCUSNOTIFY ) {

		LRESULT lResult = WndProc(uMessage, wParam, lParam);
		
		if( GetWindowStyle() & WS_CHILD ) {
		
			HWND hParent = ::GetParent(m_hWnd);
			
			if( hParent ) {
				
				WPARAM wParam = WPARAM(GetID());
				
				LPARAM lParam = LPARAM(m_hWnd);
					
				::SendMessage(hParent, WM_FOCUSNOTIFY, wParam, lParam);
				}
			}
			
		return lResult;
		}

	if( uMessage == WM_MEASUREITEM || uMessage == WM_DRAWITEM ) {

		if( !IsKindOf(AfxNamedClass(L"CCommonDialog")) ) {

			LRESULT lResult = WndProc(uMessage, wParam, lParam);

			if( !lResult ) {

				CWnd &Ctrl = GetDlgItem(UINT(wParam));

				if( Ctrl.IsWindow() ) {

					lResult = Ctrl.SendMessage(uMessage, wParam, lParam);
					}
				}

			return lResult;
			}
		}

	if( uMessage == WM_NOTIFY ) {

		NMHDR *pHeader = (NMHDR *) lParam;

		if( pHeader->code == NM_CUSTOMDRAW ) {

			if( !IsKindOf(AfxNamedClass(L"CCommonDialog")) ) {

				LRESULT lResult = WndProc(uMessage, wParam, lParam);

				if( !lResult ) {

					CWnd &Ctrl = GetDlgItem(UINT(wParam));

					if( Ctrl.IsWindow() ) {

						lResult = Ctrl.SendMessage(uMessage, wParam, lParam);
						}
					}

				return lResult;
				}
			}
		}

	return WndProc(uMessage, wParam, lParam);
	}

// Handle Lookup

CWnd & CWnd::FromHandle(HANDLE hWnd)
{
	HANDLE hProp = ::GetProp(hWnd, m_PropAtom);

	if( hProp ) {

		DWORD id1 = GetCurrentProcessId();

		DWORD id2 = NULL;

		GetWindowThreadProcessId(hWnd, &id2);

		if( id1 == id2 ) {

			CWnd *pWnd = (CWnd *) hProp;

			pWnd->AssertValid();

			return *pWnd;
			}
		}

	return FromHandle(hWnd, AfxRuntimeClass(CWnd));
	}

CWnd & CWnd::FromHandle(HANDLE hWnd, CLASS Class)
{
	if( !hWnd ) {
	
		static CWnd NullObject;
		
		return NullObject;
		}

	CWnd *pWnd = (CWnd *) afxMap->FromHandle(hWnd, NS_HWND);

	if( pWnd ) {
	
		if( pWnd->IsKindOf(Class) ) {

			pWnd->AssertValid();

			return *pWnd;
			}

		AfxAssert(Class->IsKindOf(AfxPointerClass(pWnd)));

		AfxAssert(afxMap->IsTemporary(hWnd, NS_HWND));

		AfxTrace(L"WARNING: Duplicate temporary object required\n");
		}

	pWnd = AfxNewObject(CWnd, Class);

	afxMap->InsertTemp(hWnd, NS_HWND, pWnd);

	pWnd->m_hWnd = hWnd;

	return *pWnd;
	}

// Message Box Hook

void CWnd::SetMessageBox(MSGBOX pMsgBox)
{
	m_pMsgBox = pMsgBox;
	}

// Routing Control

BOOL CWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( !CCmdTarget::OnRouteMessage(Message, lResult) ) {

		if( afxThread ) {

			if( afxMainWnd == this ) {

				return afxThread->RouteMessage(Message, lResult);
				}
			}

		return FALSE;
		}

	return TRUE;
	}

// Message Control

LRESULT CWnd::AfxCallDefProc(void)
{
	return DefProc(m_MsgCtx.Msg.message, m_MsgCtx.Msg.wParam, m_MsgCtx.Msg.lParam);
	}

void CWnd::AfxRouteToDefProc(void)
{
	m_MsgCtx.fDP = TRUE;
	}

// Message Forwarding

LRESULT CWnd::AfxForwardMsg(CCmdTarget *pTarg)
{
	if( pTarg ) {

		LRESULT lResult;

		pTarg->LocalMessage(m_MsgCtx.Msg, lResult);

		return lResult;
		}

	return 0;
	}

// Message Procedures

LRESULT CWnd::WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	void *pData = SaveMessageContext();

	m_MsgCtx.Msg.hwnd    = m_hWnd;

	m_MsgCtx.Msg.message = uMessage;

	m_MsgCtx.Msg.wParam  = wParam;
	
	m_MsgCtx.Msg.lParam  = lParam;
	
	m_MsgCtx.fDP = FALSE;
	
	LRESULT rc = 0;

	if( InPrintClient() ) {

		MSG Msg = { m_hWnd, WM_PAINT, 0, 0 };

		LocalMessage(Msg, rc);

		RestoreContext(pData);

		pData = NULL;

		rc    = DefProc(uMessage, wParam, lParam);
		}
	else {
		try {
			if( !LocalMessage(m_MsgCtx.Msg, rc) || m_MsgCtx.fDP ) {

				RestoreContext(pData);

				pData = NULL;

				rc = DefProc(uMessage, wParam, lParam);
				}
			else {
				RestoreContext(pData);

				pData = NULL;
				}
			}

		catch(CException &) {

			if( pData ) {

				RestoreContext(pData);

				pData = NULL;
				}

			throw;
			}
		}
	
	return rc;
	}

LRESULT CWnd::DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( m_pfnSuper ) {

		AfxAssert(IsWindow());

		return CallWindowProc(m_pfnSuper, m_hWnd, uMessage, wParam, lParam);
		}

	return DefWindowProc(m_hWnd, uMessage, wParam, lParam);
	}

// Destruction Cleanup

void CWnd::OnNCDestroy(void)
{
	delete this;
	}

// Default Class Name

PCTXT CWnd::GetDefaultClassName(void) const
{
	return L"AfxWndClass";
	}

// Default Class Definition

BOOL CWnd::GetClassDetails(WNDCLASSEX &Class) const
{
	Class.style         = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;

	Class.hIcon         = afxModule->LoadIcon(IDI_APPLICATION);

	Class.hCursor       = afxModule->LoadCursor(IDC_ARROW);

	Class.hbrBackground = NULL;

	return TRUE;
	}

// Implementation

BOOL CWnd::ForwardMouseWheel(CPoint Pos)
{
	if( !GetWindowRect().PtInRect(Pos) ) {

		HWND  hWnd   = WindowFromPoint(Pos);
		
		DWORD dwThis = GetCurrentProcessId();

		DWORD dwHit  = 0;

		GetWindowThreadProcessId(hWnd, &dwHit);

		if( dwHit == dwThis ) {

			::PostMessage( hWnd,
				       m_MsgCtx.Msg.message,
				       m_MsgCtx.Msg.wParam,
				       m_MsgCtx.Msg.lParam
				       );

			return TRUE;
			}
		}

	return FALSE;
	}

void CWnd::RegisterClass(void)
{
	if( !IsValidClass(GetDefaultClassName()) ) {

		WNDCLASSEX Class;

		memset(&Class, 0, sizeof(Class));

		Class.cbSize	    = sizeof(Class);

		Class.lpszClassName = GetDefaultClassName();

		Class.cbWndExtra    = DLGWINDOWEXTRA;
	
		Class.hInstance     = afxModule->GetAppInstance();

		Class.lpfnWndProc   = AfxWndProc;

		if( GetClassDetails(Class) ) {

			::RegisterClassEx(&Class);

			return;
			}

		AfxAssert(FALSE);
		}
	}
	
PVOID CWnd::SaveMessageContext(void)
{
	afxMap->Lock();
	
	CMsgCtx *pCon = New CMsgCtx;
	
	*pCon = m_MsgCtx;
	
	return pCon;
	}

void CWnd::RestoreContext(void *pData)
{
	CMsgCtx *pCon = (CMsgCtx *) pData;
	
	m_MsgCtx = *pCon;
	
	delete pCon;
	
	afxMap->Unlock();
	}

// End of File
