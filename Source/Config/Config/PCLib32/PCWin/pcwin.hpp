
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCWIN_HPP
	
#define	INCLUDE_PCWIN_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcobj.hpp>

#include <commctrl.h>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pcwin.hxx"

/////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCWIN

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pcwin.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Integer Helpers
//

inline int Sgn(int n)
{
	return n ? ((n < 0) ? -1 : +1) : 0;
	}

inline int Sgn1(int n)
{
	return (n < 0) ? -1 : +1;
	}

inline int Abs(int n)
{
	return (n < 0) ? -n : +n;
	}

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CWnd;
class CPoint;
class CSize;
class CRect;
class CColor;

//////////////////////////////////////////////////////////////////////////
//
// Point Object
//

class DLLAPI CPoint : public tagPOINT
{
	public:
		// Standard Constructors
		CPoint(void);
		CPoint(POINT const &That);
		CPoint(int sx, int sy);

		// Explicit Constructurs
		explicit CPoint(CSize const &That);
		explicit CPoint(DWORD dwPacked);

		// Assignment Operators
		CPoint & operator = (POINT const &That);
		CPoint & operator = (CSize const &That);

		// Conversions
		operator POINT * (void);
		operator DWORD   (void) const;

		// Clipping
		void ClipTo(CRect const &Rect);
		
		// Compraison Operators
		BOOL operator == (CPoint const &Arg) const;
		BOOL operator != (CPoint const &Arg) const;

		// Offset Operators
		CPoint const & operator ++ (void);
		CPoint         operator ++ (int nDummy);
		CPoint const & operator -- (void);
		CPoint         operator -- (int nDummy);
		
		// Offset in Place
		CPoint const & operator += (int nStep);
		CPoint const & operator += (CSize const &Step);
		CPoint const & operator += (CPoint const &Pos);
		CPoint const & operator -= (int nStep);
		CPoint const & operator -= (CSize const &Step);
		CPoint const & operator -= (CPoint const &Pos);
		
		// Offset via Friends
		friend DLLAPI CPoint operator + (CPoint const &a, int nStep);
		friend DLLAPI CPoint operator + (CPoint const &a, CSize const &b);
		friend DLLAPI CPoint operator + (CPoint const &a, CPoint const &b);
		friend DLLAPI CPoint operator - (CPoint const &a, int nStep);
		friend DLLAPI CPoint operator - (CPoint const &a, CSize const &b);
		friend DLLAPI CPoint operator - (CPoint const &a, CPoint const &b);
		
		// Distance Calculation
		friend DLLAPI CSize Distance(CPoint const &a, CPoint const &b);

		// Scaling in Place
		CPoint const & operator *= (int nScale);
		CPoint const & operator *= (CSize const &Scale);
		CPoint const & operator /= (int nScale);
		CPoint const & operator /= (CSize const &Scale);

		// Scaling via Friends
		friend DLLAPI CPoint operator * (CPoint const &a, int nScale);
		friend DLLAPI CPoint operator * (int nScale, CPoint const &b);
		friend DLLAPI CPoint operator * (CPoint const &a, CSize const &b);
		friend DLLAPI CPoint operator / (CPoint const &a, int nScale);
		friend DLLAPI CPoint operator / (CPoint const &a, CSize const &b);
		
		// Associated Functions
		friend DLLAPI CPoint GetCursorPos(void);
		friend DLLAPI CPoint GetCursorPos(CWnd const &Wnd);

		// Zeroing
		friend DLLAPI void AfxSetZero(CPoint &p);
	};

//////////////////////////////////////////////////////////////////////////
//
// Size Object
//

class DLLAPI CSize : public tagSIZE
{
	public:
		// Standard Constructors
		CSize(void);
		CSize(SIZE const &That);
		CSize(int sx, int sy);

		// Explicit Constructurs
		explicit CSize(CPoint const &That);
		explicit CSize(DWORD dwPacked);
		explicit CSize(int nMetric);

		// Assignment Operators
		CSize & operator = (SIZE   const &That);
		CSize & operator = (CPoint const &That);
		
		// Conversions
		operator SIZE * (void);
		operator DWORD  (void) const;

		// Attributes
		CSize GetSign(void) const;

		// Operations
		void  MakeSquare(void);
		CSize Normalize(void);
		void  Denormalize(CSize Sign);

		// Compraison Operators
		BOOL operator == (CSize const &Arg) const;
		BOOL operator != (CSize const &Arg) const;
		
		// Offset Operators
		CSize const & operator ++ (void);
		CSize         operator ++ (int nDummy);
		CSize const & operator -- (void);
		CSize         operator -- (int nDummy);
		
		// Offset in Place
		CSize const & operator += (int nStep);
		CSize const & operator += (CSize const &Step);
		CSize const & operator -= (int nStep);
		CSize const & operator -= (CSize const &Step);
		
		// Offset via Friends
		friend DLLAPI CSize operator + (CSize const &a, int nStep);
		friend DLLAPI CSize operator + (CSize const &a, CSize const &b);
		friend DLLAPI CSize operator - (CSize const &a, int nStep);
		friend DLLAPI CSize operator - (CSize const &a, CSize const &b);

		// Scaling in Place
		CSize const & operator *= (int nScale);
		CSize const & operator *= (CSize const &Scale);
		CSize const & operator /= (int nScale);
		CSize const & operator /= (CSize const &Scale);

		// Scaling via Friends
		friend DLLAPI CSize operator * (CSize const &a, int nScale);
		friend DLLAPI CSize operator * (int nScale, CSize const &b);
		friend DLLAPI CSize operator * (CSize const &a, CSize const &b);
		friend DLLAPI CSize operator / (CSize const &a, int nScale);
		friend DLLAPI CSize operator / (CSize const &a, CSize const &b);

		// Zeroing
		friend DLLAPI void AfxSetZero(CSize &s);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rectangle Object
//

class DLLAPI CRect : public tagRECT
{
	public:
		// Constructors
		CRect(void);
		CRect(RECT const &That);
		CRect(int x1, int y1, int x2, int y2);
		CRect(CPoint const &Org, CPoint const &End);
		CRect(CPoint const &Org, CSize const &Size);
		CRect(int xSize, int ySize);
		CRect(CSize const &Size);

		// Clearing
		void Empty(void);
		
		// Setting
		void Set(int x1, int y1, int x2, int y2);
		
		// Assignment Operators
		CRect & operator = (RECT const &That);

		// Conversions
		operator RECT * (void);

		// Compraison Operators
		BOOL operator == (CRect const &Arg) const;
		BOOL operator != (CRect const &Arg) const;

		// Basic Attributes
		BOOL   IsEmpty(void) const;
		int    GetWidth(void) const;
		int    GetHeight(void) const;
		CPoint GetOrg(void) const;
		CSize  GetSize(void) const;
		CSize  GetSign(void) const;

		// Simple Aliases
		int cx(void) const;
		int cy(void) const;

		// Derrived Positions
		CPoint GetTopLeft(void) const;
		CPoint GetTopRight(void) const;
		CPoint GetBottomLeft(void) const;
		CPoint GetBottomRight(void) const;
		CPoint GetCentre(void) const;
		CPoint GetCenter(void) const;
		
		// Clipping
		void ClipRectTo(CRect const &Clip);
		void ClipEndsTo(CRect const &Clip);

		// Normalization
		CSize Normalize(void);

		// Denormalization
		void Denormalize(CSize Sign);

		// Aspect Adjustment
		void MatchAspect(CSize Aspect);

		// Hit Testing
		BOOL PtInRect(CPoint const &Pos) const;
		BOOL Overlaps(CRect const &Rect) const;
		BOOL Encloses(CRect const &Rect) const;
		
		// Dragging Functions
		void DragTopLeft(CPoint const &Pos);
		void DragTopRight(CPoint const &Pos);
		void DragBottomLeft(CPoint const &Pos);
		void DragBottomRight(CPoint const &Pos);
		void DragCentre(CPoint const &Pos);

		// Offset in Place
		CRect const & operator += (CPoint const &Step);
		CRect const & operator -= (CPoint const &Step);

		// Offset via Friends
		friend DLLAPI CRect operator + (CRect const &a, CPoint const &b);
		friend DLLAPI CRect operator - (CRect const &a, CPoint const &b);

		// Inflation Functions
		void Inflate(int nStep);
		void Inflate(CSize const &Step);
		void Deflate(int nStep);
		void Deflate(CSize const &Step);

		// Inflation Operators
		CRect const & operator ++ (void);
		CRect         operator ++ (int nDummy);
		CRect const & operator -- (void);
		CRect         operator -- (int nDummy);

		// Inflation in Place
		CRect const & operator += (int nStep);
		CRect const & operator += (CSize const &Step);
		CRect const & operator -= (int nStep);
		CRect const & operator -= (CSize const &Step);

		// Inflation via Friends
		friend DLLAPI CRect operator + (CRect const &a, int nStep);
		friend DLLAPI CRect operator + (CRect const &a, CSize const &b);
		friend DLLAPI CRect operator - (CRect const &a, int nStep);
		friend DLLAPI CRect operator - (CRect const &a, CSize const &b);

		// Scaling in Place
		void operator *= (int nScale);
		void operator *= (CSize const &Scale);
		void operator /= (int nScale);
		void operator /= (CSize const &Scale);

		// Scaling via Friends
		friend DLLAPI CRect operator * (CRect const &a, int nScale);
		friend DLLAPI CRect operator * (int nScale, CRect const &b);
		friend DLLAPI CRect operator * (CRect const &a, CSize const &b);
		friend DLLAPI CRect operator / (CRect const &a, int nScale);
		friend DLLAPI CRect operator / (CRect const &a, CSize const &b);

		// Combination in Place
		CRect const & operator &= (CRect const &Rect);
		CRect const & operator |= (CRect const &Rect);

		// Combination via Friends
		friend DLLAPI CRect operator & (CRect const &a, CRect const &b);
		friend DLLAPI CRect operator | (CRect const &a, CRect const &b);

		// Zeroing
		friend DLLAPI void AfxSetZero(CRect &r);
	};

//////////////////////////////////////////////////////////////////////////
//
// Color Object
//

class DLLAPI CColor
{
	public:
		// Color Data
		union
		{
			COLORREF m_clrRef;
			BYTE     m_bColor[4];
			DWORD    m_dwColor;
			};

		// Standard Constructors
		CColor(void);
		CColor(CColor const &That);
		CColor(COLORREF clrRef);
		CColor(BYTE r, BYTE g, BYTE b);

		// Explicit Constructors
		explicit CColor(int nElement);
		explicit CColor(CColor const &c1, CColor const &c2, BYTE bMix);

		// Assignment Operators
		CColor & operator = (CColor const &That);
		CColor & operator = (COLORREF clrRef);

		// Conversion
		operator COLORREF (void) const;

		// Attributes
		BYTE  GetRed(void) const;
		BYTE  GetGreen(void) const;
		BYTE  GetBlue(void) const;
		DWORD Flip(void) const;

		// Comparison Operators
		BOOL operator == (CColor const &Arg) const;
		BOOL operator != (CColor const &Arg) const;

		// Bitwise Operators
		CColor   operator ~  (void);
		CColor & operator &= (CColor const &Arg);
		CColor & operator |= (CColor const &Arg);
		CColor & operator ^= (CColor const &Arg);
		CColor   operator &  (CColor const &Arg);
		CColor   operator |  (CColor const &Arg);
		CColor   operator ^  (CColor const &Arg);

		// Logarithmic Operators
		CColor   operator -  (void);
		CColor & operator *= (CColor const &Arg);
		CColor & operator += (CColor const &Arg);
		CColor   operator *  (CColor const &Arg);
		CColor   operator +  (CColor const &Arg);
		
	protected:
		// Implementation
		static BYTE Mul(BYTE b1, BYTE b2);
		static BYTE Add(BYTE b1, BYTE b2);
		static BYTE Neg(BYTE b1);
	};

//////////////////////////////////////////////////////////////////////////
//
// Handle Mapping Macros
//

#define	AfxNull(c)	(c::FromHandle(NULL))

#define	afxMap		(CMapManager::FindInstance())

//////////////////////////////////////////////////////////////////////////
//
// Handle Mapping Spaces
//

#define	NS_GENERIC	0
#define	NS_HGDIOBJ	1
#define	NS_HDC		2
#define	NS_HACCEL	3
#define	NS_HMENU	4
#define	NS_HCURSOR	5
#define	NS_HWND		6
#define NS_HIMAGELIST	7

//////////////////////////////////////////////////////////////////////////
//
// Handle Name Structure
//

struct CHandleName
{
	HANDLE	hObject;
	UINT	uSpace;
	};

//////////////////////////////////////////////////////////////////////////
//
// Handle Name Helper
//

inline int AfxCompare(CHandleName const &a, CHandleName const &b)
{
	return memcmp(&a, &b, sizeof(a));
	}

//////////////////////////////////////////////////////////////////////////
//
// Basic Handle Map
//

typedef CSyncMap <CHandleName, CObject *> CHandleMap;

//////////////////////////////////////////////////////////////////////////
//
// Handle Map Manager
//

class DLLAPI CMapManager
{
	public:
		// Object Location
		static CMapManager * FindInstance(void);

		// Constructor
		CMapManager(void);

		// Destructor
		~CMapManager(void);

		// Attributes
		BOOL IsEmpty(void) const;
		
		// Handle Lookup
		CObject * FromHandle (HANDLE hObj, UINT uSpace);
		BOOL      IsTemporary(HANDLE hObj, UINT uSpace);

		// Handle Operations
		void InsertPerm(HANDLE hObj, UINT uSpace, CObject *pObj);
		void InsertTemp(HANDLE hObj, UINT uSpace, CObject *pObj);
		BOOL RemoveBoth(HANDLE hObj, UINT uSpace, CObject *pObj);
		BOOL RemovePerm(HANDLE hObj, UINT uSpace, CObject *pObj);
		BOOL RemoveTemp(HANDLE hObj, UINT uSpace, CObject *pObj);

		// Global Operations
		void EmptyAll(void);
		void DeleteTemporaries(void);
		void Lock(void);
		BOOL Unlock(void);
		
	protected:
		// Static Data
		static CMapManager * m_pThis;

		// Type Definitions
		typedef CHandleName CName;

		// Data Members
		LONG       m_uLock;
		CHandleMap m_PermMap;
		CHandleMap m_TempMap;
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Handle Wrapper
//

class DLLAPI CHandle : public CObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CHandle(void);

		// Destructor
		~CHandle(void);

		// Attachment
		BOOL Attach(HANDLE hObject);
		void Detach(BOOL fDelete);

		// Attributes
		HANDLE GetHandle(void) const;
		BOOL   IsValid(void) const;
		BOOL   IsNull(void) const;
		BOOL   operator ! (void) const;

		// Conversion
		operator HANDLE (void) const;

		// Operations
		void SetExtern(BOOL fExtern);

		// Handle Lookup
		static CHandle & FromHandle(HANDLE hObject, UINT uSpace, CLASS Class);

	protected:
		// Data Members
		HANDLE m_hObject;
		BOOL   m_fExtern;

		// Handle Space
		virtual WORD GetHandleSpace(void) const;

		// Destruction
		virtual void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDC;
class CGdiObject;
class CBrush;
class CPen;
class CFont;
class CBitmap;

//////////////////////////////////////////////////////////////////////////
//
// Generic GDI Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CGdiObject : public CHandle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CGdiObject(void);
		CGdiObject(CGdiObject const &That);
		CGdiObject(int nStock);

		// Destructor
		~CGdiObject(void);

		// Creation
		BOOL CreateStockObject(int nStock);

		// Operations
		void Unrealize(void);

		// Handle Lookup
		static CGdiObject & FromHandle(HANDLE hObject, CLASS Class);

	protected:
		// Handle Space
		WORD GetHandleSpace(void) const;

		// Destruction
		void DestroyObject(void);

		// Exceptions
		void CheckException(BOOL fCheck);
	};

//////////////////////////////////////////////////////////////////////////
//
// GDI Brush Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CBrush : public CGdiObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CBrush(void);
		CBrush(CBrush const &That);
		CBrush(int nElement);
		CBrush(CColor const &Color);
		CBrush(int nStyle, CColor const &Color);
		CBrush(CBitmap const &Bitmap);
		CBrush(CColor const &C1, CColor const &C2, BYTE bMix);

		// Creation
		BOOL Create(int nElement);
		BOOL Create(CColor const &Color);
		BOOL Create(int nStyle, CColor const &Color);
		BOOL Create(CBitmap const &Bitmap);
		BOOL Create(CColor const &C1, CColor const &C2, BYTE bMix);

		// Handle Lookup
		static CBrush & FromHandle(HANDLE hObject);
	};

//////////////////////////////////////////////////////////////////////////
//
// GDI Pen Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CPen : public CGdiObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CPen(void);
		CPen(CPen const &That);
		CPen(int nStyle, int nWidth, CColor const &Color);
		CPen(CColor const &Color);

		// Creation
		BOOL Create(int nStyle, int nWidth, CColor const &Color);
		BOOL Create(CColor const &Color);

		// Handle Lookup
		static CPen & FromHandle(HANDLE hObject);
	};

//////////////////////////////////////////////////////////////////////////
//
// GDI Font Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CFont : public CGdiObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CFont(void);
		CFont(CFont const &That);
		CFont(LOGFONT const *pLogFont);
		CFont(LOGFONT const &LogFont);
		CFont(PCTXT pFace, CSize const &Size, UINT uType, int nRotate);
		CFont(PCTXT pFace, CSize const &Size, UINT uType);
		CFont(CDC &DC, PCTXT pFace, int nPoint, UINT uType, int nRotate);
		CFont(CDC &DC, PCTXT pFace, int nPoint, UINT uType);
		CFont(int nStock);

		// Creation
		BOOL Create(LOGFONT const *pLogFont);
		BOOL Create(LOGFONT const &LogFont);
		BOOL Create(PCTXT pFace, CSize const &Size, UINT uType, int nRotate);
		BOOL Create(PCTXT pFace, CSize const &Size, UINT uType);
		BOOL Create(CDC &DC, PCTXT pFace, int nPoint, UINT uType, int nRotate);
		BOOL Create(CDC &DC, PCTXT pFace, int nPoint, UINT uType);

		// Handle Lookup
		static CFont & FromHandle(HANDLE hObject);

		// Font Types
		enum
		{
			Bold     = 1,
			Italic   = 2,
			NoSmooth = 4,
			};
	};

//////////////////////////////////////////////////////////////////////////
//
// GDI Bitmap Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CBitmap : public CGdiObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CBitmap(void);
		CBitmap(CBitmap const &Bitmap);
		CBitmap(ENTITY ID);
		CBitmap(ENTITY ID, int xSize, int ySize);
		CBitmap(ENTITY ID, CSize const &Size);
		CBitmap(int xSize, int ySize, UINT uPlanes, UINT uBits);
		CBitmap(CSize const &Size, UINT uPlanes, UINT uBits);
		CBitmap(int xSize, int ySize, UINT uPlanes, UINT uBits, PBYTE &pBits);
		CBitmap(CSize const &Size, UINT uPlanes, UINT uBits, PBYTE &pBits);
		CBitmap(CDC &DC, int xSize, int ySize);
		CBitmap(CDC &DC, CSize const &Size);

		// Creation
		BOOL Create(ENTITY ID);
		BOOL Create(ENTITY ID, int xSize, int ySize);
		BOOL Create(ENTITY ID, CSize const &Size);
		BOOL Create(int xSize, int ySize, UINT uPlanes, UINT uBits);
		BOOL Create(CSize const &Size, UINT uPlanes, UINT uBits);
		BOOL Create(int xSize, int ySize, UINT uPlanes, UINT uBits, PBYTE &pBits);
		BOOL Create(CSize const &Size, UINT uPlanes, UINT uBits, PBYTE &pBits);
		BOOL Create(CDC &DC, int xSize, int ySize);
		BOOL Create(CDC &DC, CSize const &Size);

		// Attributes
		CSize GetSize(void) const;
		int GetWidth(void) const;
		int GetHeight(void) const;

		// Operations
		void SetAlpha(PBYTE pData);
		void SetAlpha(PBYTE pData, BYTE bAlpha);
		void SetAlpha(PBYTE pData, CColor const &Find, BYTE bAlpha);
		void SetAlpha(PBYTE pData, CColor const &Find, CColor const &Repl, BYTE bAlpha);

		// Handle Lookup
		static CBitmap & FromHandle(HANDLE hObject);

	protected:
		// Data Members
		CSize m_Size;
	};

//////////////////////////////////////////////////////////////////////////
//
// Toolbox Macros
//

#define	afxStdTools	(CStdTools::FindInstance())

#define	afxAdjustDPI(n)	(afxStdTools->AdjustDPI((n)))

#define	afxWin2K	(afxStdTools->Win2K())

#define	afxVista	(afxStdTools->Vista())

#define	afxCanBlend	(afxStdTools->CanBlend())

#define	afxAero		(afxStdTools->UsingAero())

#define	afxDlgFont	(afxStdTools->DlgFont())

#define	afxColor(n)	(afxStdTools->m_Color[CStdTools::col##n ])

#define	afxBrush(n)	(afxStdTools->m_Brush[CStdTools::col##n ])

#define	afxPen(n)	(afxStdTools->m_Pen  [CStdTools::col##n ])

#define	afxFont(n)	(afxStdTools->m_Font [CStdTools::font##n])

//////////////////////////////////////////////////////////////////////////
//
// Standard Toolbox
//

class DLLAPI CStdTools : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Object Location
		static CStdTools * FindInstance(void);

		// Constructor
		CStdTools(void);

		// Destructor
		~CStdTools(void);

		// Attributes
		int     AdjustDPI(int n) const;
		BOOL    Win2K(void) const;
		BOOL    Vista(void) const;
		BOOL    CanBlend(void) const;
		BOOL    UsingAero(void) const;
		CString DlgFont(void) const;

		// Operations
		void Update(void);
		BOOL SetColor(int nPos, CColor Color);
		void SetMainWnd(void);

		// Theme Access
		BOOL   HasTheme(void) const;
		HANDLE OpenTheme(CString const &Class);
		CColor GetThemeColor(CString const &Class, int nPart, int nState, int nProp);
		CColor GetThemeColor(int nPart, int nState, int nProp);
		void   CloseTheme(void);

		// Color Codes
		enum ColorCode
		{
			colBackground,
			colAppWorkspace,
			colWindowText,
			colWindowBack,
			colSelectText,
			colSelectBack,
			colButtonText,
			col3dFace,
			col3dShadow,
			col3dHighlight,
			col3dDkShadow,
			col3dLight,
			colInfoText,
			colInfoBack,
			colTabFace,
			colTabGroup,
			colTabLock,
			colMenuFace,
			colMenuText,
			colMenuBar,
			colDisabled,
			colEnabled,
			colOrange1,
			colOrange2,
			colOrange3,
			colBlue1,
			colBlue2,
			colBlue3,
			colBlue4,
			colNavBar1,
			colNavBar2,
			colNavBar3,
			colNavBar4,
			colMASK,
			colWHITE,
			colBLACK,
			colRED,
			colGREEN,
			colBLUE,
			colYELLOW,
			colCYAN,
			colMAGENTA,
			_colors
			};
			
		// Font Codes
		enum FontCode
		{
			fontDialog,
			fontDialogNA,
			fontStatus,
			fontLarge,
			fontToolTip,
			fontToolFix,
			fontFixed,
			fontRotate,
			fontBolder,
			fontItalic,
			fontMarlett1,
			fontMarlett2,
			_fonts
			};

		// Data Members
		CColor m_Color[_colors];
		BOOL   m_Force[_colors];
		CBrush m_Brush[_colors];
		CPen   m_Pen  [_colors];
		CFont  m_Font [_fonts ];
		HANDLE m_hTheme;

	protected:
		// Static Data
		static CStdTools * m_pThis;

		// Theme APIs
		typedef BOOL    (__stdcall *PCHECK )(void);
		typedef HANDLE  (__stdcall *POPEN  )(HWND,PCTXT);
		typedef HRESULT (__stdcall *PGETCOL)(HANDLE,int,int,int,COLORREF*);
		typedef HRESULT (__stdcall *PCLOSE )(HANDLE);
		typedef HRESULT (__stdcall *PISAERO)(BOOL*);

		// Data Members
		HWND    m_hWnd;
		int     m_dpi;
		BOOL    m_fWin2K;
		BOOL    m_fVista;
		BOOL    m_fBlend;
		BOOL	m_fAero;
		BOOL	m_fTheme;
		HANDLE  m_hLib;
		PCHECK  m_pfnCheck;
		POPEN   m_pfnOpen;
		PGETCOL m_pfnGetCol;
		PCLOSE  m_pfnClose;
		CString m_Class;

		// Implementation
		void   DoUpdate(BOOL fSafe);
		CColor FindColor(int nPos);
		void   MakeFonts(void);
		int    GetDPI(void);
		BOOL   GetWin2K(void);
		BOOL   GetVista(void);
		BOOL   GetCanBlend(void);
		BOOL   GetAero(void);
		BOOL   LoadThemeLib(void);
		BOOL   FreeThemeLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CWnd;
class CUserObject;
class CAccelerator;
class CCursor;
class CIcon;
class CMenu;
class CMenuInfo;

//////////////////////////////////////////////////////////////////////////
//
// Generic User Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CUserObject : public CHandle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUserObject(void);
		CUserObject(CUserObject const &That);

	protected:
		// Exceptions
		void CheckException(BOOL fCheck);
	};

//////////////////////////////////////////////////////////////////////////
//
// Accelerator Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CAccelerator : public CUserObject
{
 	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CAccelerator(void);
		CAccelerator(CAccelerator const &That);
		CAccelerator(ENTITY ID);

		// Destructor
		~CAccelerator(void);

		// Creation
		BOOL Create(ENTITY ID);
		
		// Operations
		BOOL Translate(HWND hWnd, MSG &Msg);
		BOOL Translate(MSG &Msg);

		// Handle Lookup
		static CAccelerator & FromHandle(HANDLE hObject);
		
	protected:
		// Handle Space
		WORD GetHandleSpace(void) const;

		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Cursor Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CCursor : public CUserObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructors
		CCursor(void);
		CCursor(CCursor const &That);
		CCursor(ENTITY ID, int xSize, int ySize);
		CCursor(ENTITY ID, CSize const &Size);
		CCursor(ENTITY ID);

		// Destructor
		~CCursor(void);

		// Creation
		BOOL Create(ENTITY ID, int xSize, int ySize);
		BOOL Create(ENTITY ID, CSize const &Size);
		BOOL Create(ENTITY ID);

		// Attributes
		CSize GetSize(void) const;
		
		// Object Matching
		static CCursor & FromHandle(HANDLE hObject);
		
	protected:
		// Data Members
		CSize m_Size;

		// Handle Space
		WORD GetHandleSpace(void) const;

		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Icon Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CIcon : public CUserObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CIcon(void);
		CIcon(CIcon const &That);
		CIcon(ENTITY ID, int xSize, int ySize);
		CIcon(ENTITY ID, CSize const &Size);
		CIcon(ENTITY ID);

		// Destructor
		~CIcon(void);

		// Creation
		BOOL Create(ENTITY ID, int xSize, int ySize);
		BOOL Create(ENTITY ID, CSize const &Size);
		BOOL Create(ENTITY ID);

		// Attributes
		CSize GetSize(void) const;

		// Handle Lookup
		static CIcon & FromHandle(HANDLE hObject);
		
	protected:
		// Data Members
		CSize m_Size;

		// Handle Space
		WORD GetHandleSpace(void) const;

		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Menu Object
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CMenu : public CUserObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CMenu(void);
		CMenu(CMenu const &That);
		CMenu(ENTITY ID);

		// Destructor
		~CMenu(void);

		// Creation
		BOOL Create(ENTITY ID);
		BOOL CreatePopupMenu(void);
		BOOL CreateMenu(void);

		// Attributes
		int     GetMenuItemCount(void) const;
		UINT    GetMenuItemID(int nPos) const;
		UINT    GetMenuUniqueID(int nPos) const;
		UINT    GetMenuState(UINT uID, UINT uFlags) const;
		CString GetMenuString(UINT uID, UINT uFlags) const;
		CMenu & GetSubMenu(int nPos) const;
		BYTE    GetMenuType(void) const;
		BOOL	IsMenuActive(void) const;

		// Operations
		void AppendMenu(UINT uFlags, UINT uNewID, PCTXT pText);
		void AppendMenu(UINT uFlags, UINT uNewID, HBITMAP hBitmap);
		void AppendMenu(UINT uFlags, UINT uNewID, DWORD dwData);
		void AppendMenu(UINT uFlags, CMenu &Menu, PCTXT pText);
		void AppendMenu(CMenu &Menu);
		void AppendSeparator(void);
		void CheckMenuItem(UINT uID, UINT uFlags);
		void DeleteDisabled(void);
		void DeleteMenu(UINT uID, UINT uFlags);
		void EnableMenuItem(UINT uID, UINT uFlags);
		void EnableMenuItem(UINT uID, BOOL fEnable, BOOL fGray);
		void EmptyMenu(void);
		void InsertMenu(UINT uID, UINT uFlags, UINT uNewID, PCTXT pText);
		void InsertMenu(UINT uID, UINT uFlags, UINT uNewID, HBITMAP hBitmap);
		void InsertMenu(UINT uID, UINT uFlags, UINT uNewID, DWORD dwData);
		void InsertMenu(UINT uID, UINT uFlags, CMenu &Menu, PCTXT pText);
		void InsertSeparator(UINT uID, UINT uFlags);
		void MergeMenu(CMenu &Menu);
		void ModifyMenu(UINT uID, UINT uFlags, UINT uNewID, PCTXT pText);
		void ModifyMenu(UINT uID, UINT uFlags, UINT uNewID, HBITMAP hBitmap);
		void ModifyMenu(UINT uID, UINT uFlags, UINT uNewID, DWORD dwData);
		void ModifyMenu(UINT uID, UINT uFlags, CMenu &Menu, PCTXT pText);
		void RemoveMenu(UINT uID, UINT uFlags);
		void RemoveMenu(CMenu const &SubMenu);
		void SendInitMessage(void);
		void SetMenuItemBitmaps(UINT uID, UINT uFlags, HBITMAP hMap0, HBITMAP hMap1);
		BOOL TrackPopupMenu(UINT uFlags, CPoint const &Pos, HWND hWnd);
		BOOL TrackPopupMenu(UINT uFlags, int xPos, int yPos, HWND hWnd);

		// Owner Draw Support
		void MakeOwnerDraw(BOOL fTop);
		void MakeOwnerDraw(int nPos, BOOL fTop);
		void FreeOwnerDraw(void);
		BOOL FreeOwnerDraw(int nPos);
		
		// Handle Lookup
		static CMenu & FromHandle(HANDLE hObject);

	protected:
		// Static Data
		static HANDLE m_hMenuPad;

		// Handle Space
		WORD GetHandleSpace(void) const;

		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Menu Info for Owner Draw Conversion
//

class DLLAPI CMenuInfo
{
	public:
		// Data Members
		BOOL    m_fTop;
		UINT    m_uID;
		UINT	m_uImage;
		CString m_Text;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDC;
class CWindowDC;
class CClientDC;
class CExtendedDC;
class CPaintDC;
class CMemoryDC;

//////////////////////////////////////////////////////////////////////////
//
// Basic Device Context
//

class DLLAPI CDC : public CObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructors
		CDC(void);
		CDC(HDC hDC);

		// Attachment
		BOOL Attach(HDC hDC);
		void Detach(BOOL fDelete);

		// Attributes
		HDC  GetHandle(void) const;
		BOOL IsValid(void) const;
		BOOL IsNull(void) const;
		BOOL operator ! (void) const;

		// Conversion
		operator HDC (void) const;

		// Object Selection
		HANDLE SelectObject(HANDLE hObject);

		// Object Selection
		void Select(CGdiObject const &Object);
		void Replace(CGdiObject const &Object);
		void Deselect(void);

		// Current Selections
		CBrush  & GetCurrentBrush(void) const;
		CPen    & GetCurrentPen(void) const;
		CFont   & GetCurrentFont(void) const;
		CBitmap & GetCurrentBitmap(void) const;

		// Get Attributes
		CColor GetBkColor(void) const;
		int    GetBkMode(void) const;
		CPoint GetBrushOrg(void) const;
		int    GetPolyFillMode(void) const;
		int    GetROP2(void) const;
		int    GetStretchBltMode(void) const;
		UINT   GetTextAlign(void) const;
		int    GetTextCharacterExtra(void) const;
		CColor GetTextColor(void) const;
		
		// Set Attributes
		void SetBkColor(CColor const &Color);
		void SetBkMode(int nMode);
		void SetBrushOrg(int xPos, int yPos);
		void SetBrushOrg(CPoint const &Pos);
		void SetPolyFillMode(int nMode);
		void SetROP2(int nMode);
		void SetStretchBltMode(int nMode);
		void SetTextAlign(UINT uAlign);
		void SetTextCharacterExtra(int nExtra);
		void SetTextJustification(int nExtra, int nCount);
		void SetTextColor(CColor const &Color);

		// Get Mapping Info
		int    GetMapMode(void) const;
		CSize  GetViewportExt(void) const;
		CPoint GetViewportOrg(void) const;
		CSize  GetWindowExt(void) const;
		CPoint GetWindowOrg(void) const;
		
		// Set Mapping Info
		void SetMapMode(int nMode);
		void SetViewportExt(int xExt, int yExt);
		void SetViewportExt(CSize const &Ext);
		void SetViewportOrg(int xPos, int yPos);
		void SetViewportOrg(CPoint const &Pos);
		void SetWindowExt(int xExt, int yExt);
		void SetWindowExt(CSize const &Ext);
		void SetWindowOrg(int xPos, int yPos);
		void SetWindowOrg(CPoint const &Pos);

		// DP->LP Coordinate Transforms
		void DPtoLP(CPoint &Point) const;
		void DPtoLP(CSize &Size) const;
		void DPtoLP(PPOINT pPoint, int nCount = 1) const;
		void DPtoLP(PSIZE pSize, int nCount = 1) const;
		void DPtoLP(RECT &Rect) const;

		// LP->DP Coordinate Transforms
		void LPtoDP(CPoint &Point) const;
		void LPtoDP(CSize &Size) const;
		void LPtoDP(PPOINT pPoint, int nCount = 1) const;
		void LPtoDP(PSIZE pSize, int nCount = 1) const;
		void LPtoDP(RECT &Rect) const;
		
		// Clipping Status
		CRect GetClipBox(void) const;
		int   GetClipBox(CRect &Rect) const;
		BOOL  PtVisible(CPoint const &Pos) const;
		BOOL  RectVisible(CRect const &Rect) const;
		
		// Clipping Control
		int ExcludeClipRect(int x1, int y1, int x2, int y2);
		int ExcludeClipRect(CRect const &Rect);
		int IntersectClipRect(int x1, int y1, int x2, int y2);
		int IntersectClipRect(CRect const &Rect);
		int OffsetClipRgn(int xStep, int yStep);
		int OffsetClipRgn(CSize const &Step);

		// Pixel Read
		CColor GetPixel(int xPos, int yPos) const;
		CColor GetPixel(CPoint const &Pos) const;

		// Pixel Write
		void SetPixel(int xPos, int yPos, CColor const &Color);
		void SetPixel(CPoint const &Pos, CColor const &Color);
		
		// Line Drawing
		void MoveTo(int xPos, int yPos);
		void MoveTo(CPoint const &Pos);
		void LineTo(int xPos, int yPos);
		void LineTo(CPoint const &Pos);
		void Arc(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
		void Arc(CRect const &Rect, CPoint const &p1, CPoint const &p2);
		void Polyline(PPOINT pPoint, UINT uCount);
		
		// Simple Drawing
		void FillRect(int x1, int y1, int x2, int y2, CBrush const &Brush);
		void FillRect(CRect const &Rect, CBrush const &Brush);
		void FrameRect(int x1, int y1, int x2, int y2, CBrush const &Brush);
		void FrameRect(CRect const &Rect, CBrush const &Brush);
		void InvertRect(int x1, int y1, int x2, int y2);
		void InvertRect(CRect const &Rect);

		// Figure Drawing
		void Rectangle(int x1, int y1, int x2, int y2);
		void Rectangle(CRect const &Rect);
		void Ellipse(int x1, int y1, int x2, int y2);
		void Ellipse(CRect const &Rect);
		void RoundRect(int x1, int y1, int x2, int y2, int cx, int cy);
		void RoundRect(CRect const &Rect, CSize const &Size);
		void RoundRect(CRect const &Rect, int nSize);
		void Diamond(int x1, int y1, int x2, int y2);
		void Diamond(CRect const &Rect);

		// Graduated Fills
		void GradHorz(CRect const &Rect, CColor const &c1, CColor const &c2);
		void GradVert(CRect const &Rect, CColor const &c1, CColor const &c2);
		
		// PatBlt Functions
		void PatBlt(int xDest, int yDest, int xSize, int ySize, DWORD dwRop);
		void PatBlt(CPoint const &Dest, CSize const &Size, DWORD dwRop);
		void PatBlt(CRect const &Rect, DWORD dwRop);

		// Bitmap Transfer from DC
		void BitBlt(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc, DWORD dwRop);
		void BitBlt(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src, DWORD dwRop);
		void BitBlt(CRect const &Rect, CDC &SrcDC, CPoint const &Src, DWORD dwRop);
		
		// Bitmap Transfer from Bitmap
		void BitBlt(int xDest, int yDest, int xSize, int ySize, CBitmap const &SrcBM, int xSrc, int ySrc, DWORD dwRop);
		void BitBlt(CPoint const &Dest, CSize const &Size, CBitmap const &SrcBM, CPoint const &Src, DWORD dwRop);
		void BitBlt(CRect const &Rect, CBitmap const &SrcBM, CPoint const &Src, DWORD dwRop);
		
		// Stretch Transfer from DC
		void StretchBlt(int xDest, int yDest, int cxDest, int cyDest, CDC &SrcDC, int xSrc, int ySrc, int cxSrc, int cySrc, DWORD dwRop);
		void StretchBlt(CPoint const &Dest, CSize const &DestSize, CDC &SrcDC, CPoint const &Src, CSize const &SrcSize, DWORD dwRop);
		void StretchBlt(CRect const &Rect, CDC &SrcDC, CRect const &Src, DWORD dwRop);
		
		// Stretch Transfer from Bitmap
		void StretchBlt(int xDest, int yDest, int cxDest, int cyDest, CBitmap const &SrcBM, int xSrc, int ySrc, int cxSrc, int cySrc, DWORD dwRop);
		void StretchBlt(CPoint const &Dest, CSize const &DestSize, CBitmap const &SrcBM, CPoint const &Src, CSize const &SrcSize, DWORD dwRop);
		void StretchBlt(CRect const &Rect, CBitmap const &SrcBM, CRect const &Dest, DWORD dwRop);
		
		// Transparent Transfer from DC
		void TransBlt(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc);
		void TransBlt(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src);
		void TransBlt(CRect const &Rect, CDC &SrcDC, CPoint const &Src);

		// Transparent Transfer from Bitmap
		void TransBlt(int xDest, int yDest, int xSize, int ySize, CBitmap const &SrcBM, int xSrc, int ySrc);
		void TransBlt(CPoint const &Dest, CSize const &Size, CBitmap const &SrcBM, CPoint const &Src);
		void TransBlt(CRect const &Rect, CBitmap const &SrcBM, CPoint const &Src);

		// Alpha Blended Transfer from DC
		void AlphaBlend(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc);
		void AlphaBlend(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src);
		void AlphaBlend(CRect const &Rect, CDC &SrcDC, CPoint const &Src);
		void AlphaBlend(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc, int nAlpha);
		void AlphaBlend(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src, int nAlpha);
		void AlphaBlend(CRect const &Rect, CDC &SrcDC, CPoint const &Src, int nAlpha);

		// Alpha Blended Transfer from DC
		void AlphaBlend(int xDest, int yDest, int xSize, int ySize, CBitmap &SrcBM, int xSrc, int ySrc);
		void AlphaBlend(CPoint const &Dest, CSize const &Size, CBitmap &SrcBM, CPoint const &Src);
		void AlphaBlend(CRect const &Rect, CBitmap &SrcBM, CPoint const &Src);
		void AlphaBlend(int xDest, int yDest, int xSize, int ySize, CBitmap &SrcBM, int xSrc, int ySrc, int nAlpha);
		void AlphaBlend(CPoint const &Dest, CSize const &Size, CBitmap &SrcBM, CPoint const &Src, int nAlpha);
		void AlphaBlend(CRect const &Rect, CBitmap &SrcBM, CPoint const &Src, int nAlpha);

		// Icon Drawing
		void DrawIcon(int xPos, int yPos, CIcon const &Icon);
		void DrawIcon(CPoint const &Pos, CIcon const &Icon);
		void DrawIcon(int xPos, int yPos, CIcon const &Icon, int xSize, int ySize);
		void DrawIcon(CPoint const &Pos, CIcon const &Icon, CSize const &Size);
		void DrawIcon(CRect const &Rect, CIcon const &Icon);
		void DrawIcon(int xPos, int yPos, CIcon const &Icon, CBrush const &Brush);
		void DrawIcon(CPoint const &Pos, CIcon const &Icon, CBrush const &Brush);
		void DrawIcon(int xPos, int yPos, CIcon const &Icon, int xSize, int ySize, CBrush const &Brush);
		void DrawIcon(CPoint const &Pos, CIcon const &Icon, CSize const &Size, CBrush const &Brush);
		void DrawIcon(CRect const &Rect, CIcon const &Icon, CBrush const &Brush);

		// Text Information
		void    GetTextMetrics(TEXTMETRIC &TextMetric) const;
		CSize   GetTextExtent(PCTXT pString, int nLength) const;
		CSize   GetTextExtent(CString const &String) const;
		CString GetTextFace(void) const;

		// Tabbed Text Information
		CSize GetTabbedTextExtent(PCTXT pString, int nLength, UINT uCount, int *pTable);
		CSize GetTabbedTextExtent(CString const &String, UINT uCount, int *pTable);

		// TextOut Functions
		void TextOut(int xPos, int yPos, PCTXT pString, int nLength);
		void TextOut(int xPos, int yPos, CString const &String);
		void TextOut(CPoint const &Pos, PCTXT pString, int nLength);
		void TextOut(CPoint const &Pos, CString const &String);
		
		// ExtTextOut Functions
		void ExtTextOut(int xPos, int yPos, UINT uOptions, CRect const &Rect, PCTXT pString, int nLength, int *pTabs = NULL);
		void ExtTextOut(int xPos, int yPos, UINT uOptions, CRect const &Rect, CString const &String, int *pTabs = NULL);
		void ExtTextOut(CPoint const &Pos, UINT uOptions, CRect const &Rect, PCTXT pString, int nLength, int *pTabs = NULL);
		void ExtTextOut(CPoint const &Pos, UINT uOptions, CRect const &Rect, CString const &String, int *pTabs = NULL);

		// Gray String Functions
		void GrayString(CBrush const &Brush, PCTXT pString, int nLength, int xPos, int yPos);
		void GrayString(CBrush const &Brush, CString const &String, int xPos, int yPos);
		void GrayString(CBrush const &Brush, PCTXT pString, int nLength, int xPos, int yPos, int xSize, int ySize);
		void GrayString(CBrush const &Brush, CString const &String, int xPos, int yPos, int xSize, int ySize);
		void GrayString(CBrush const &Brush, PCTXT pString, int nLength, CPoint const &Point);
		void GrayString(CBrush const &Brush, CString const &String, CPoint const &Point);
		void GrayString(CBrush const &Brush, PCTXT pString, int nLength, CPoint const &Point, CSize const &Size);
		void GrayString(CBrush const &Brush, CString const &String, CPoint const &Point, CSize const &Size);
		void GrayString(CBrush const &Brush, PCTXT pString, int nLength, CRect const &Rect);
		void GrayString(CBrush const &Brush, CString const &String, CRect const &Rect);

		// DrawText Functions
		void DrawText(PCTXT pString, int nLength, CRect const &Rect, UINT uFormat);
		void DrawText(CString const &String, CRect const &Rect, UINT uFormat);

		// TabbedTextOut Functions
		CSize TabbedTextOut(int xPos, int yPos, PCTXT pString, int nLength, UINT uCount, int *pTable, int xBase);
		CSize TabbedTextOut(int xPos, int yPos, CString const &String, UINT uCount, int *pTable, int xBase);
		CSize TabbedTextOut(CPoint const &Pos, PCTXT pString, int nLength, UINT uCount, int *pTable, int xBase);
		CSize TabbedTextOut(CPoint const &Pos, CString const &String, UINT uCount, int *pTable, int xBase);

		// 3D Drawing Routines
		void  DrawState(HANDLE hBrush, LPARAM lParam, WPARAM wParam, CRect const &Rect, UINT uFlags);
		void  DrawState(CString const &String, CRect const &Rect, BOOL fPrefix, UINT uFlags);
		void  DrawState(CIcon const &Icon, CRect const &Rect, UINT uFlags);
		void  DrawState(CBitmap const &Bitmap, CRect const &Rect, UINT uFlags);
		CRect DrawEdge(CRect const &Rect, UINT uEdge, UINT uFlags);
		
		// Rectangle Edges
		void DrawTopEdge(CRect &Rect, int n, CBrush const &Brush);
		void DrawBottomEdge(CRect &Rect, int n, CBrush const &Brush);
		void DrawLeftEdge(CRect &Rect, int n, CBrush const &Brush);
		void DrawRightEdge(CRect &Rect, int n, CBrush const &Brush);
		void DrawTopEdge(CRect const &Rect, int n, CBrush const &Brush);
		void DrawBottomEdge(CRect const &Rect, int n, CBrush const &Brush);
		void DrawLeftEdge(CRect const &Rect, int n, CBrush const &Brush);
		void DrawRightEdge(CRect const &Rect, int n, CBrush const &Brush);

		// Focus Rect Helper
		void DrawFocusRect(CRect const &Rect);

		// Frame Control Functions
		void DrawFrameControl(CRect const &Rect, UINT uType, UINT uState);
		void DrawButtonFrameControl(CRect const &Rect, UINT uState);
		void DrawCaptionFrameControl(CRect const &Rect, UINT uState);
		void DrawScrollFrameControl(CRect const &Rect, UINT uState);

		// Stacking
		void Save(void);
		void Restore(void);

		// Operations
		void SetExtern(BOOL fExtern);

		// Handle Lookup
		static CDC & FromHandle(HDC hDC);
		static CDC & FromHandle(HDC hDC, CLASS Class);

	protected:
		// Data Members
		HDC    m_hDC;
		BOOL   m_fExtern;
		HANDLE m_hList[8];
		UINT   m_uCount;

		// Destruction
		virtual void DestroyObject(void);

		// Exceptions
		void CheckException(void);

		// Implementation
		int Length(PCTXT pString, int nLength) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Window Device Context
//

class DLLAPI CWindowDC : public CDC
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CWindowDC(HWND hWnd);
		
		// Destructor
		~CWindowDC(void);

	protected:
		// Window Handle
		HWND m_hWnd;

		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Client Device Context
//

class DLLAPI CClientDC : public CDC
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CClientDC(HWND hWnd);

		// Destructor
		~CClientDC(void);
		
	protected:
		// Window Handle
		HWND m_hWnd;

		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Extended Device Context
//

class DLLAPI CExtendedDC : public CDC
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CExtendedDC(HWND hWnd, UINT uFlags);

		// Destructor
		~CExtendedDC(void);
		
	protected:
		// Window Handle
		HWND m_hWnd;

		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Painting Device Context
//

class DLLAPI CPaintDC : public CDC
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CPaintDC(CWnd &Wnd);
		CPaintDC(HWND hWnd);

		// Destructor
		~CPaintDC(void);
		
		// Attributes
		BOOL  GetEraseFlag(void) const;
		CRect GetPaintRect(void) const;
		
	protected:
		// Window Object
		CWnd * m_pWnd;

		// Paint Context
		PAINTSTRUCT m_PaintStruct;

		// Destruction
		void DestroyObject(void);
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Memory Device Context
//

class DLLAPI CMemoryDC : public CDC
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CMemoryDC(void);
		CMemoryDC(CDC const &That);
		
		// Destructor
		~CMemoryDC(void);

	protected:
		// Destruction
		void DestroyObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//                     

class CCmdInfo;
class CCmdSource;
class CMessageItem;
class CMessageMap;
class CCmdTarget;

//////////////////////////////////////////////////////////////////////////
//
// Command Information
//

class DLLAPI CCmdInfo
{
	public:
		// Constructor
		CCmdInfo(void);
		
		// Data Members
		CString m_Prompt;
		CString m_ToolTip;
		DWORD   m_Image;
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Abstract Command Source
//                     

class DLLAPI CCmdSource : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Overridables
		virtual void PrepareSource(void);
		virtual void EnableItem(BOOL fEnable);
		virtual void CheckItem(BOOL fCheck);
		virtual void SetItemText(PCTXT pText);
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Command Source with Data
//

class DLLAPI CCmdSourceData : public CCmdSource
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CCmdSourceData(void);
		
		// Attributes
		UINT  GetFlags(void) const;
		PCTXT GetText (void) const;

		// Overridables
		void PrepareSource(void);
		void EnableItem(BOOL fEnable);
		void CheckItem(BOOL fCheck);
		void SetItemText(PCTXT pText);
	
	protected:
		// Data Members
		UINT    m_uFlags;
		CString m_String;
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Command Source for Menus
//

class DLLAPI CCmdSourceMenu : public CCmdSource
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CCmdSourceMenu(CMenu &Menu, UINT uID);
		
		// Overridables
		void EnableItem(BOOL fEnable);
		void CheckItem(BOOL fCheck);
		void SetItemText(PCTXT pText);
		
	protected:
		// Data Members
		CMenu *m_pMenu;
		UINT   m_uID;
	};
		
//////////////////////////////////////////////////////////////////////////
//
// Message Map Typedefs
//                     

typedef LRESULT (CCmdTarget::*MSGP)(void);

typedef BOOL    (CCmdTarget::*MSGP1)(UINT);

typedef BOOL    (CCmdTarget::*MSGP2)(UINT, CCmdSource &);

typedef BOOL    (CCmdTarget::*MSGP3)(UINT, CCmdInfo &);

typedef BOOL    (CCmdTarget::*MSGP4)(MSG &Msg);

//////////////////////////////////////////////////////////////////////////
//
// Message Map Entry
//                     

class DLLAPI CMessageItem
{
	public:
		// Data Members
		UINT uMessage;
		UINT uMinID;
		UINT uMaxID;
		UINT uNotify;
		MSGP pfnProc;
		char Sig[16];
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Message Map Header
//                     

class DLLAPI CMessageMap
{
	public:
		// Data Members
		CMessageItem const *pItem;
		CMessageMap  const *pBase;
	};

//////////////////////////////////////////////////////////////////////////
//
// Message Map Declaration
//                     

#define	AfxDeclareMessageMap()						\
									\
	private: static CMessageItem m_afxMessageItem[];		\
									\
	protected: static CMessageMap m_afxMessageMap;			\
									\
	virtual CMessageMap const * AfxObjectMessageMap(void) const; 	\
									\
	static CMessageMap const * AfxStaticMessageMap(void)		\
	
//////////////////////////////////////////////////////////////////////////
//
// Message Map Implementation
//                     

#define	AfxMessageMap(name, base)					\
									\
	CMessageMap name::m_afxMessageMap =				\
	{ 								\
		m_afxMessageItem, base::AfxStaticMessageMap()		\
		};							\
									\
	CMessageMap const * name::AfxObjectMessageMap(void) const	\
	{								\
		return &m_afxMessageMap;				\
		}							\
									\
	CMessageMap const * name::AfxStaticMessageMap(void)		\
	{								\
		return &m_afxMessageMap;				\
		}							\
									\
	CMessageItem name::m_afxMessageItem[] = 			\

//////////////////////////////////////////////////////////////////////////
//
// Simple Message Map Entries
//                     

#define	AfxDispatchWinMsg(mess, fn, args)				\
									\
	{ mess, 0, 0, 0, (MSGP) fn, args },				\

#define	AfxDispatchWinNot(mess, i1, i2, nc, fn, args)			\
									\
	{ mess, i1, i2, nc, (MSGP) fn, args },				\
	
#define	AfxDispatchCommand(id, fn)					\
									\
	{ WM_AFX_COMMAND, id, id, 0, (MSGP)(MSGP1) &fn, "C-1L" },	\
                                                                
#define	AfxDispatchControl(id, fn)					\
									\
	{ WM_AFX_CONTROL, id, id, 0, (MSGP)(MSGP2) &fn, "C-1L2V" },	\

#define	AfxDispatchGetInfo(id, fn)					\
									\
	{ WM_AFX_GETINFO, id, id, 0, (MSGP)(MSGP3) &fn, "C-1L2V" },	\
	
#define	AfxDispatchAccelerator()					\
									\
	{ WM_AFX_ACCEL, 0,0,0, (MSGP)(MSGP4) &OnAccelerator, "C-2V" },	\
                                                                        
#define	AfxMessageEnd(name)						\
									\
	{ 0, 0, 0, NULL, 0 }						\

//////////////////////////////////////////////////////////////////////////
//
// Ranged Message Map Entries
//                     

#define	AfxDispatchCommandRange(i1, i2, fn)				\
									\
	{ WM_AFX_COMMAND, i1, i2, 0, (MSGP) (MSGP1) &fn, "C-1L" },	\

#define	AfxDispatchControlRange(i1, i2, fn)				\
									\
	{ WM_AFX_CONTROL, i1, i2, 0, (MSGP) (MSGP2) &fn, "C-1L2V" },	\

#define	AfxDispatchGetInfoRange(i1, i2, fn)				\
									\
	{ WM_AFX_GETINFO, i1, i2, 0, (MSGP) (MSGP3) &fn, "C-1L2V" },	\

//////////////////////////////////////////////////////////////////////////
//
// Typed Message Map Entries
//                     

#define	AfxDispatchCommandType(type, fn)				\
									\
	AfxDispatchCommandRange((type << 8), (type << 8) | 255, fn)	\

#define	AfxDispatchControlType(type, fn)				\
									\
	AfxDispatchControlRange((type << 8), (type << 8) | 255, fn)	\

#define	AfxDispatchGetInfoType(type, fn)				\
									\
	AfxDispatchGetInfoRange((type << 8), (type << 8) | 255, fn)	\

//////////////////////////////////////////////////////////////////////////
//
// Framework Messages
//

#define	WM_AFX_COMMAND		(WM_APP + 0x0200)
#define	WM_AFX_CONTROL		(WM_APP + 0x0201)
#define	WM_AFX_GETINFO		(WM_APP + 0x0202)
#define	WM_AFX_ACCEL		(WM_APP + 0x0203)

//////////////////////////////////////////////////////////////////////////
//
// Status Bar Control
//

#define	WM_GETSTATUSBAR		(WM_APP + 0x0204)
#define	WM_SETSTATUSBAR		(WM_APP + 0x0205)

//////////////////////////////////////////////////////////////////////////
//
// Command Target Base Class
//                     

class DLLAPI CCmdTarget : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Attributes
		BOOL HasMessageMap(void) const;
		
		// Routing Function
		BOOL RouteMessage(MSG const &Msg, LRESULT &lResult);
		BOOL LocalMessage(MSG const &Msg, LRESULT &lResult);

		// Routing Helpers
		BOOL RouteCommand(UINT uID, LPARAM lParam);
		BOOL RouteControl(UINT uID, CCmdSource &Source);
		BOOL RouteGetInfo(UINT uID, CCmdInfo &Info);
		BOOL RouteAccelerator(MSG &Msg);

	protected:
		// Data Members
		MSG const * m_pMsg;

		// Mapping Stubs
		virtual CMessageMap const * AfxObjectMessageMap(void) const;
		static  CMessageMap const * AfxStaticMessageMap(void);

		// Routing Control
		virtual BOOL OnRouteMessage(MSG const &Msg, LRESULT &lResult);

		// Message Location
		BOOL SearchMessageMap(MSG const &Msg, LRESULT &lResult);
		BOOL IsInternalMessage(MSG const &Msg);
		UINT ExtractTargetID(MSG const &Msg);
		UINT ExtractNotifyCode(MSG const &Msg);

		// Call Implementation
		LRESULT CallHandler(CMessageItem const &Item, MSG const &Msg);
		LRESULT MakeCall(MSGP pfnProc, UINT const *pStack, UINT uCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Messages
//

#define	WM_POSTCREATE		(WM_APP + 0x0300)
#define	WM_SUBCLASSED		(WM_APP + 0x0301)
#define	WM_PLEASEKILLME		(WM_APP + 0x0302)
#define	WM_GOINGIDLE		(WM_APP + 0x0303)
#define	WM_GETMETRIC		(WM_APP + 0x0304)
#define	WM_MOUSEREPEAT		(WM_APP + 0x0305)
#define	WM_FOCUSNOTIFY		(WM_APP + 0x0306)
#define	WM_PREDESTROY		(WM_APP + 0x0307)
#define WM_TAKEFOCUS		(WM_APP + 0x0308)
#define	WM_BROADCAST		(WM_APP + 0x0309)

//////////////////////////////////////////////////////////////////////////
//
// WM_GETMETRIC Codes
//

#define	MC_DIALOG_ORG		1
#define	MC_BEST_SIZE		2
#define	MC_MINIMUM_SIZE		3
#define MC_ACTIVE_POSITION	4
#define MC_ACTIVE_SIZE		5

//////////////////////////////////////////////////////////////////////////
//
// Message Dispatch
//

#include "winmsg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Window Creation Hooking
//

DLLAPI BOOL AfxInstallHook(CWnd *pWnd);

DLLAPI void AfxRemoveHook(void);

//////////////////////////////////////////////////////////////////////////
//
// Window and Hook Procedures
//

DLLAPI LRESULT CALLBACK AfxWndProc(HWND, UINT, WPARAM, LPARAM);

DLLAPI LRESULT CALLBACK AfxCbtHookProc(int, WPARAM, LPARAM);

DLLAPI LRESULT CALLBACK AfxMsgHookProc(int, WPARAM, LPARAM);

//////////////////////////////////////////////////////////////////////////
//
// Message Box Extensions
//

#define	MB_YESNOALLCANCEL	6

#define	MB_YESNOALL		7

//////////////////////////////////////////////////////////////////////////
//
// Message Box Procedure Type
//

typedef UINT (*MSGBOX)(HWND, PCTXT, PCTXT, UINT, PCTXT);

//////////////////////////////////////////////////////////////////////////
//
// Default Message Box Procedure
//

DLLAPI UINT DefMsgBox(HWND, PCTXT, PCTXT, UINT, PCTXT);

//////////////////////////////////////////////////////////////////////////
//
// Framework Window Object
//

class DLLAPI CWnd : public CCmdTarget
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWnd(void);

		// Destructor
		~CWnd(void);

		// Normal Creation
		BOOL Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData);
		BOOL Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData);
		BOOL Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData);
		BOOL Create(DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData);
		BOOL Create(DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData);
		BOOL Create(DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData);
		BOOL Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData);
		BOOL Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData);
		BOOL Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData);
		BOOL Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, HWND hParent, HMENU hMenu, void *pData);
		BOOL Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, CWnd &Parent, CMenu &Menu, void *pData);
		BOOL Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, CWnd &Parent, UINT uID, void *pData);

		// Sub-Classing
		BOOL SubClassWindow(void);

		// Destruction
		void DestroyWindow(BOOL fNow);

		// Attachment
		BOOL Attach(HWND hWnd);
		void Detach(void);

		// Attributes
		HWND GetHandle(void) const;
		BOOL IsWindow(void) const;
		BOOL IsValid(void) const;
		BOOL IsNull(void) const;
		BOOL operator ! (void) const;
		BOOL IsEnabled(void) const;
		BOOL InPrintClient(void) const;
		HDC  GetPrintDC(void) const;
		
		// Conversion
		operator HWND (void) const;
		
		// Comparision Operators
		BOOL operator == (CWnd const &Wnd);
		BOOL operator == (HWND hWnd);
		BOOL operator != (CWnd const &Wnd);
		BOOL operator != (HWND hWnd);
		
		// WndProc Calling
		LRESULT CallWndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// Handle Lookup
		static CWnd & FromHandle(HWND hWnd);
		static CWnd & FromHandle(HWND hWnd, CLASS Class);

		// Message Box Hook
		static void SetMessageBox(MSGBOX pMsgBox);

		// Message Handling (Normal)
		LRESULT SendMessage(UINT uMessage, WPARAM wParam = 0, LPARAM lParam = 0);
		BOOL    PostMessage(UINT uMessage, WPARAM wParam = 0, LPARAM lParam = 0);

		// Message Handling (Constant)
		LRESULT SendMessageConst(UINT uMessage, WPARAM wParam = 0, LPARAM lParam = 0) const;
		BOOL    PostMessageConst(UINT uMessage, WPARAM wParam = 0, LPARAM lParam = 0) const;

		// Message Broadcast
		void SendBroadcast(UINT uCode);
		
		// Window Text Functions
		CString GetWindowText(void) const;
		UINT    GetWindowTextLength(void) const;
		void    SetWindowText(PCTXT pText);
		void    SetWindowText(PCSTR pText);
		
		// Window Tree Access
		BOOL   HasParent(void) const;
		CWnd & GetParent(void) const;
		CWnd & GetParent(UINT uLevel) const;
		CWnd & GetParent(CLASS Class) const;
		CWnd & GetTopParent(void) const;
		CWnd & GetDlgParent(void) const;
		UINT   GetControlID(void) const;
		CWnd & GetDlgItem(UINT uID) const;
		CWnd & GetDlgItem(UINT uID, CLASS Class) const;
		CWnd * GetDlgItemPtr(UINT uID) const;
		CWnd * GetDlgItemPtr(UINT uID, CLASS Class) const;
		HWND   GetDlgItemHandle(UINT uID) const;
		CWnd & GetWindow(UINT uRelation) const;
		CWnd * GetWindowPtr(UINT uRelation) const;
		HWND   GetWindowHandle(UINT uRelation) const;
		
		// Get Size and Position
		BOOL  IsIconic(void) const;
		BOOL  IsZoomed(void) const;
		CRect GetWindowRect(void) const;
		void  GetWindowRect(CRect &Rect) const;
		CRect GetClientRect(void) const;
		void  GetClientRect(CRect &Rect) const;
		
		// Change Size and Position
		void MoveWindow(CRect const &Rect, BOOL fPaint);
		void MoveWindow(int x, int y, int cx, int cy, BOOL fPaint);
		void SetWindowPos(HWND hWnd, int x, int y, int cx, int cy, UINT uFlags);
		void SetWindowPos(CPoint const &Pos, CSize const &Size, UINT uFlags);
		void SetWindowPos(CRect const &Rect, UINT uFlags);
		void SetWindowOrg(CPoint const &Pos, BOOL fPaint);
		void SetWindowOrg(int xPos, int yPos, BOOL fPaint);
		void SetWindowSize(CSize const &Size, BOOL fPaint);
		void SetWindowSize(int xSize, int ySize, BOOL fPaint);
		void SetWindowOrder(HWND hWnd, BOOL fPaint);
		void BringWindowToTop(void);
		void CloseWindow(void);
		BOOL OpenIcon(void);
		void MakeTopmost(BOOL fState);
		
		// Coordinate Mapping
		void ClientToScreen(POINT &Point) const;
		void ClientToScreen(SIZE &Size) const;
		void ClientToScreen(RECT &Rect) const;
		void ScreenToClient(POINT &Point) const;
		void ScreenToClient(SIZE &Size) const;
		void ScreenToClient(RECT &Rect) const;
		void ClientToWindow(POINT &Point) const;
		void ClientToWindow(SIZE &Size) const;
		void ClientToWindow(RECT &Rect) const;
		void WindowToClient(POINT &Point) const;
		void WindowToClient(SIZE &Size) const;
		void WindowToClient(RECT &Rect) const;
		
		// Update and Painting
		void  UpdateWindow(void);
		void  SetRedraw(BOOL fRedraw);
		CRect GetUpdateRect(void);
		void  GetUpdateRect(CRect &Rect);
		void  Invalidate(BOOL fErase);
		void  Invalidate(CRect const &Rect, BOOL fErase);
		void  InvalidateAll(BOOL fErase);
		void  Validate(void);
		void  Validate(CRect const &Rect);
		void  ValidateAll(void);
		BOOL  ShowWindow(int nCmdShow);
		BOOL  ShowForeignWindow(int nCmdShow);
		BOOL  IsWindowVisible(void) const;
		void  ShowOwnedPopups(BOOL bShow);
		
		// Get Window State
		BOOL IsWindowEnabled(void) const;
		BOOL HasFocus(void) const;
		BOOL IsActive(void) const;
		BOOL HasCapture(void) const;
		
		// Get Given Window
		static CWnd & GetFocus(void);
		static CWnd & GetActiveWindow(void);
		static CWnd & GetCapture(void);
		
		// Modify Window State
		void EnableWindow(BOOL fEnable);
		void EnableWindow(void);
		void DisableWindow(void);
		void SetFocus(void);
		void SetActiveWindow(void);
		void SetCapture(void);
		void ReleaseCapture(void);
		void SetTopActive(void);
		
		// Window Data Functions
		WORD GetWindowWord(int nOffset) const;
		void SetWindowWord(int nOffset, WORD wData);
		LONG GetWindowLong(int nOffset) const;
		void SetWindowLong(int nOffset, LONG lData);

		// Window Property Functions
		HANDLE GetProp(PCTXT pText);
		BOOL   SetProp(PCTXT pText, HANDLE hData);
		HANDLE RemoveProp(PCTXT pText);

		// Rectangle Adjustment
		void AdjustWindowRect(CRect &Rect) const;
		void AdjustWindowSize(CSize &Size) const;
		void AdjustWindowRect(CRect &Rect, DWORD dwStyle) const;
		void AdjustWindowSize(CSize &Size, DWORD dwStyle) const;
		
		// Style Functions
		DWORD GetWindowStyle(void) const;
		void  SetWindowStyle(DWORD dwMask, DWORD dwData);
		DWORD GetWindowExStyle(void) const;
		void  SetWindowExStyle(DWORD dwMask, DWORD dwData);
		
		// Class Functions
		WORD    GetClassWord(int nOffset) const;
		void    SetClassWord(int nOffset, WORD wData);
		LONG    GetClassLong(int nOffset) const;
		void    SetClassLong(int nOffset, LONG lData);
		CString GetWindowClassName(void) const;
		BOOL    IsClass(PCTXT pName) const;
		BOOL    IsValidClass(PCTXT pName) const;
		
		// Message Box Functions
		UINT Error(PCTXT pText, PCTXT pDoNot = NULL);
		UINT Error(PCSTR pText, PCTXT pDoNot = NULL);
		UINT Error(ENTITY ID, PCTXT pDoNot = NULL);
		UINT Information(PCTXT pText, PCTXT pDoNot = NULL);
		UINT Information(PCSTR pText, PCTXT pDoNot = NULL);
		UINT Information(ENTITY ID, PCTXT pDoNot = NULL);
		UINT OkCancel(PCTXT pText);
		UINT OkCancel(PCSTR pText);
		UINT OkCancel(ENTITY ID);
		UINT YesNoCancel(PCTXT pText);
		UINT YesNoCancel(PCSTR pText);
		UINT YesNoCancel(ENTITY ID);
		UINT YesNoAllCancel(PCTXT pText);
		UINT YesNoAllCancel(PCSTR pText);
		UINT YesNoAllCancel(ENTITY ID);
		UINT YesNo(PCTXT pText);
		UINT YesNo(PCSTR pText);
		UINT YesNo(ENTITY ID);
		UINT YesNoAll(PCTXT pText);
		UINT YesNoAll(PCSTR pText);
		UINT YesNoAll(ENTITY ID);
		UINT NoYes(PCTXT pText);
		UINT NoYes(PCSTR pText);
		UINT NoYes(ENTITY ID);

		// File Overwrite Check
		UINT CanOverwrite(CFilename const &Name);

		// Font Operations
		HFONT GetFont(void) const;
		void  SetFont(CFont const &Font);
		
		// Icon Operations
		CIcon & GetIcon(void) const;
		void    SetIcon(CIcon const &Icon);

		// Dialog and Controls
		UINT GetID(void) const;

		// Menu Functions
		void    SetMenu(CMenu &NewMenu);
		CMenu & GetMenu(void) const;
		CMenu & GetSystemMenu(void) const;
		void    ResetSystemMenu(void);
		void    DrawMenuBar(void);

		// Scroll Attributes
		int  GetScrollPos(UINT uBar) const;
		int  GetScrollRangeMin(UINT uBar) const;
		int  GetScrollRangeMax(UINT uBar) const;
		UINT GetScrollPageSize(UINT uBar) const;
		
		// Scroll Operations
		void SetScrollPos(UINT uBar, int nPos, BOOL fPaint);
		void SetScrollRange(UINT uBar, int nMin, int nMax, BOOL fPaint);
		void SetScrollPageSize(UINT uBar, UINT uPage, BOOL fPaint);
		void EnableScrollBar(UINT uBar, UINT uArrows);
		void ShowScrollBar(UINT uBar, BOOL fShow);

		// Parent Commands
		void SendParentCommand(UINT uID);
		void SendParentCommand(UINT uLevel, UINT uID);
		void PostParentCommand(UINT uID);
		void PostParentCommand(UINT uLevel, UINT uID);

		// Timer Functions
		void SetTimer(UINT uID, UINT uPeriod);
		void KillTimer(UINT uID);
		
		// Timer Allocation
		static UINT AllocTimerID(void);

	protected:
		// Message Context
		struct CMsgCtx
		{
			MSG  Msg;
			BOOL fDP;
			};
	
		// Message Box Hook
		static MSGBOX m_pMsgBox;

		// Property Atom
		static CGlobalAtom m_PropAtom;
		
		// Data Members
		HWND    m_hWnd;
		FARPROC m_pfnSuper;
		FARPROC m_pfnWider;
		CMsgCtx m_MsgCtx;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Msg, LRESULT &lResult);

		// Message Control
		LRESULT AfxCallDefProc(void);
		void AfxRouteToDefProc(void);

		// Message Forwarding
		LRESULT AfxForwardMsg(CCmdTarget *pTarg);

		// Message Procedures
		virtual LRESULT WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);
		virtual LRESULT DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// Destruction Cleanup
		virtual void OnNCDestroy(void);

		// Default Class Name
		virtual PCTXT GetDefaultClassName(void) const;

		// Default Class Definition
		virtual BOOL GetClassDetails(WNDCLASSEX &Class) const;

		// Implementation
		BOOL  ForwardMouseWheel(CPoint Pos);
		void  RegisterClass(void);
		PVOID SaveMessageContext(void);
		void  RestoreContext(void *pData);
		BOOL  ShowError(PCTXT pText);

		// Message Handlers
		BOOL OnAccelerator(MSG &Msg);
		void OnActivate(UINT uCode, BOOL fMinimized, CWnd &Wnd);
		void OnActivateApp(BOOL fActive, HANDLE hTask);
		void OnCancelMode(void);
		void OnBroadcast(UINT uCode);
		void OnChar(UINT uCode, DWORD dwData);
		LONG OnCharToItem(UINT uCode, UINT uCaret, CWnd &ListBox);
		void OnClose(void);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);
		LONG OnCompareItem(UINT uID, COMPAREITEMSTRUCT &Compare);
		UINT OnCreate(CREATESTRUCT &Create);
		HOBJ OnCtlColorBtn(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorDlg(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorEdit(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorListBox(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorMsgBox(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorScrollBar(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorStatic(CDC &DC, CWnd &Wnd);
		void OnDeleteItem(UINT uID, DELETEITEMSTRUCT &Delete);
		void OnDestroy(void);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);
		void OnEnable(BOOL fEnable);
		void OnEnterIdle(UINT uSource, CWnd &Wnd);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		UINT OnGetDlgCode(MSG *pMsg);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnGetMinMaxInfo(MINMAXINFO &Info);
		void OnGetStatusBar(CString &Text);
		void OnGoingIdle(void);
		void OnHScroll(UINT uCode, int nPos, CWnd &Ctrl);
		void OnIconEraseBkGnd(CDC &DC);
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnInitMenu(CMenu &Menu);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnKeyDown(UINT uCode, DWORD dwData);
		void OnKeyUp(UINT uCode, DWORD dwData);
		void OnKillFocus(CWnd &Wnd);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnMdiActivate(CWnd &Wnd, CWnd &Prev);
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Measure);
		LONG OnMenuChar(char cData, UINT uFlags, CMenu &Menu);
		void OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnMouseRepeat(void);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
		void OnMove(CPoint Pos);
		BOOL OnNCActivate(BOOL fActive);
		UINT OnNCCalcSize(BOOL fCalcRects, NCCALCSIZE_PARAMS &Params);
		UINT OnNCHitTest(CPoint Pos);
		void OnNCLButtonDblClk(UINT uHitTest, CPoint Pos);
		void OnNCLButtonDown(UINT uHitTest, CPoint Pos);
		void OnNCLButtonUp(UINT uHitTest, CPoint Pos);
		void OnNCMouseMove(UINT uHitTest, CPoint Pos);
		void OnNCPaint(void);
		BOOL OnNotify(UINT uID, NMHDR &Info);
		void OnPaint(void);
		void OnParentNotify(UINT uFlags, UINT uArg1, UINT uArg2);
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnPrint(CDC &DC, UINT uFlags);
		void OnPrintClient(CDC &DC, UINT uFlags);
		HOBJ OnQueryDragIcon(void);
		BOOL OnQueryEndSession(void);
		BOOL OnQueryOpen(void);
		void OnRButtonDblClk(UINT uFlags, CPoint Pos);
		void OnRButtonDown(UINT uFlags, CPoint Pos);
		void OnRButtonUp(UINT uFlags, CPoint Pos);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnSetFocus(CWnd &Wnd);
		void OnSetStatusBar(PCTXT pText);
		void OnSetText(PCTXT pText);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnSize(UINT uType, CSize Size);
		void OnSubClassed(void);
		void OnSysChar(UINT uCode, DWORD dwData);
		void OnSysColorChange(void);
		void OnSysCommand(UINT uID, CPoint Pos);
		void OnSysKeyDown(UINT uCode, DWORD dwData);
		void OnSysKeyUp(UINT uCode, DWORD dwData);
		void OnTimeChange(void);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		LONG OnVKeyToItem(UINT uCode, UINT uCaret, CWnd &ListBox);
		void OnVScroll(UINT uCode, int nPos, CWnd &Ctrl);
		void OnWindowPosChanged(WINDOWPOS &Pos);
		void OnWindowPosChanging(WINDOWPOS &Pos);
		
		// Friends
		DLLAPI friend LRESULT CALLBACK AfxWndProc(HWND, UINT, WPARAM, LPARAM);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Decalarations
//

class CWaitMode;
class CThreadHelper;
class CThread;

//////////////////////////////////////////////////////////////////////////
//
// Thread Access Macros
//

#define	afxThread	(CThread::FindInstance())

#define	afxMainWnd	(afxThread->GetMainWnd())

//////////////////////////////////////////////////////////////////////////
//
// Wait Mode Object
//

class DLLAPI CWaitMode
{
	public:
		// Constructor
		CWaitMode(void);

		// Destructor
		~CWaitMode(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Thread Helper Object
//

class CThreadHelper : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CThreadHelper(CThread *pThread);

	protected:
		// Data Members
		CThread *m_pThread;

		// Overridables
		BOOL OnInit(void);
		UINT OnExec(void);
		void OnTerm(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Windows Thread Object
//

class DLLAPI CThread : public CCmdTarget
{
	public:
		// Object Location
		static CThread * FindInstance(void);

		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CThread(void);

		// Destructor
		~CThread(void);

		// Creation
		void Create(void);
		void CreateSuspended(void);

		// Execution
		UINT Execute(void);

		// Helper Entries
		BOOL HelperInit(void);
		UINT HelperExec(void);
		void HelperTerm(void);

		// Raw Thread Access
		CRawThread & GetRawThread(void) const;

		// Attributes
		CWnd *  GetMainWnd(void) const;
		CWnd *  GetAppWnd(void) const;
		CString GetStatusText(void);

		// Operations
		void ClearMainWnd(void);
		void SetMainWnd(CWnd &MainWnd);
		void SetStatusText(PCTXT pText);
		void LockUpdate(BOOL fLock);

		// Wait Cursor Management
		BOOL InWaitMode(void) const;
		int  GetWaitCount(void) const;
		void SetWaitMode(BOOL fWait);
		int  SuspendWaitMode(void);
		void RestoreWaitMode(int nCount);
		void SetWaitCursor(void);
		
	protected:
		// Data Memebrs
		CThreadHelper * m_pThread;
		CWnd          *	m_pMainWnd;
		UINT		m_uLockCount;
		CCursor		m_WaitCursor;
		int		m_nWaitCount;
		CPoint		m_LastPos;
		UINT		m_uLastMsg;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Core Overridables
		virtual BOOL OnInitialize(void);
		virtual UINT OnExecute(void);
		virtual BOOL OnIdle(UINT uCount);
		virtual BOOL OnTranslateMessage(MSG &Msg);
		virtual void OnException(EXCEPTION Ex);
		virtual void OnTerminate(void);

		// Implementation
		UINT MessageLoop(void);
		BOOL IsMessageReady(void);
		BOOL IsTrivialMessage(MSG &Msg);
		void PumpMessage(MSG &Msg);
		void SendGoingIdle(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Clipboard Object
//

class DLLAPI CClipboard : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CClipboard(HWND hWnd);

		// Destructor
		~CClipboard(void);
		
		// Attributes
		BOOL IsValid(void) const;
		BOOL IsFormatAvailable(UINT uFormat) const;
		UINT GetFormatCount(void) const;
		UINT EnumFormats(UINT uFormat) const;
		
		// Operations
		HGLOBAL GetData(UINT uFormat);
		CString GetText(UINT uFormat);
		BOOL    SetData(UINT uFormat, HGLOBAL hData);
		BOOL    SetText(PCTXT pText);
		BOOL    Empty(void); 
		
	protected:
		// Data Members
		BOOL m_fValid;
	};

// End of File

#endif
