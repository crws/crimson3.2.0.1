
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hooking Structure
//

struct CMsgInfo
{
	LPARAM lParam;
	WPARAM wParam;
	UINT   uMessage;
	WORD   hWnd;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hooking Context
//

static TLS HHOOK m_hCbtHook = NULL;

static TLS HHOOK m_hMsgHook = NULL;

static TLS CWnd *m_pWnd     = NULL;

static TLS HWND  m_hWnd     = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Window Creation Hooking
//

BOOL AfxInstallMsgHook(HWND hWnd)
{
	AfxAssert(hWnd);

	m_hMsgHook = SetWindowsHookEx( WH_CALLWNDPROC,
				       HOOKPROC(AfxMsgHookProc),
				       NULL,
				       GetCurrentThreadId()
				       );

	if( !m_hMsgHook ) {

		AfxThrowResourceException();
			
		return FALSE;
		}
		
	m_hWnd = hWnd;
	
	return TRUE;
	}

void AfxRemoveMsgHook(void)
{		
	if( m_hMsgHook ) {

		UnhookWindowsHookEx(m_hMsgHook);

		m_hMsgHook = NULL;
		}
	}

BOOL AfxInstallHook(CWnd *pWnd)
{
	AfxValidateObject(pWnd);

	m_hCbtHook = SetWindowsHookEx( WH_CBT,
				       HOOKPROC(AfxCbtHookProc),
				       NULL,
				       GetCurrentThreadId()
				       );

	if( !m_hCbtHook ) {

		AfxThrowResourceException();
			
		return FALSE;
		}
		
	m_pWnd = pWnd;
	
	return TRUE;
	}

void AfxRemoveHook(void)
{
	if( m_hCbtHook ) {

		UnhookWindowsHookEx(m_hCbtHook);

		m_hCbtHook = NULL;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// CBT Hook Procedure
//

LRESULT CALLBACK AfxCbtHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if( m_pWnd ) {
		
		if( nCode == HCBT_CREATEWND ) {
		
			HWND hWnd = HWND(wParam);
			
			AfxRemoveHook();
			
			AfxInstallMsgHook(hWnd);
			
			return 0;
			}

		return CallNextHookEx(m_hCbtHook, nCode, wParam, lParam);
		}

	AfxRemoveHook();

	return CallNextHookEx(m_hCbtHook, nCode, wParam, lParam);
	}

//////////////////////////////////////////////////////////////////////////
//
// Msg Hook Procedure
//

LRESULT CALLBACK AfxMsgHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if( m_hWnd ) {

		CMsgInfo *pInfo = (CMsgInfo *) lParam;
		
		if( pInfo->uMessage == WM_NCCREATE ) {
		
			AfxRemoveMsgHook();

			m_pWnd->Attach(m_hWnd);
			
			m_pWnd->SubClassWindow();
			
			m_pWnd = NULL;
			
			m_hWnd = NULL;
			
			return 0;
			}
	
		return 0;
		}

	AfxRemoveMsgHook();
		
	return CallNextHookEx(m_hMsgHook, nCode, wParam, lParam);
	}

// End of File
