
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Wait Mode Object
//

// Constructor

CWaitMode::CWaitMode(void)
{
	if( afxThread ) {

		afxThread->SetWaitMode(1);
		}
	}

// Destructor

CWaitMode::~CWaitMode(void)
{
	if( afxThread ) {

		afxThread->SetWaitMode(0);
		}
	}

// End of File
