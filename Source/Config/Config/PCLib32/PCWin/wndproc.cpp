
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Main Window Procedure
//

LRESULT CALLBACK AfxWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	CWnd *pWnd = (CWnd *) GetProp(hWnd, CWnd::m_PropAtom);
	
	if( !pWnd ) {

		pWnd = (CWnd *) afxMap->FromHandle(hWnd, NS_HWND);
		
		if( !pWnd ) {

			LPARAM lResult = DefWindowProc(hWnd, uMessage, wParam, lParam);

			return lResult;
			}

		SetProp(hWnd, CWnd::m_PropAtom, HANDLE(pWnd));
		}

	if( uMessage == WM_PLEASEKILLME ) {

		DestroyWindow(hWnd);

		return 0;
		}

	return pWnd->CallWndProc(uMessage, wParam, lParam);
	}

// End of File
