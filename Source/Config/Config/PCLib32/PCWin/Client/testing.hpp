
//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP
	
#define	INCLUDE_TESTING_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTestApp;
class CTestWnd;
class CWinManager;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CTestApp : public CThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTestApp(UINT n);

		// Destructor
		~CTestApp(void);

	protected:
		// Data Members
		UINT m_n;

		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

class CTestWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestWnd(void);

		// Destructor
		~CTestWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPaint(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnSetStatusBar(PCTXT pText);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
	};

// End of File

#endif
