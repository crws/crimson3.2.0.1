
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Window Device Context
//

// Runtime Class

AfxImplementRuntimeClass(CWindowDC, CDC);

// Constructor

CWindowDC::CWindowDC(HWND hWnd)
{
	m_hWnd = hWnd;
	
	Attach(GetWindowDC(m_hWnd));

	CheckException();
	}
		
// Destructor

CWindowDC::~CWindowDC(void)
{
	Detach(TRUE);
	}

// Destruction

void CWindowDC::DestroyObject(void)
{
	ReleaseDC(m_hWnd, m_hDC);
	}

//////////////////////////////////////////////////////////////////////////
//
// Client Device Context
//

// Runtime Class

AfxImplementRuntimeClass(CClientDC, CDC);

// Constructor

CClientDC::CClientDC(HWND hWnd)
{
	m_hWnd = hWnd;

	Attach(GetDC(m_hWnd));

	CheckException();
	}

// Destructor

CClientDC::~CClientDC(void)
{
	Detach(TRUE);
	}

// Destruction

void CClientDC::DestroyObject(void)
{
	ReleaseDC(m_hWnd, m_hDC);
	}

//////////////////////////////////////////////////////////////////////////
//
// Extended Device Context
//

// Runtime Class

AfxImplementRuntimeClass(CExtendedDC, CDC);

// Constructor

CExtendedDC::CExtendedDC(HWND hWnd, UINT uFlags)
{
	m_hWnd = hWnd;

	Attach(GetDCEx(m_hWnd, NULL, uFlags));

	CheckException();
	}

// Destructor

CExtendedDC::~CExtendedDC(void)
{
	Detach(TRUE);
	}

// Destruction

void CExtendedDC::DestroyObject(void)
{
	ReleaseDC(m_hWnd, m_hDC);
	}

//////////////////////////////////////////////////////////////////////////
//
// Painting Device Context
//

// Runtime Class

AfxImplementRuntimeClass(CPaintDC, CDC);

// Constructor

CPaintDC::CPaintDC(CWnd &Wnd)
{
	m_pWnd = &Wnd;

	if( m_pWnd->InPrintClient() ) {

		m_PaintStruct.rcPaint = m_pWnd->GetClientRect();

		Attach(m_pWnd->GetPrintDC());
		}
	else {
		HWND hWnd = m_pWnd->GetHandle();

		Attach(BeginPaint(hWnd, &m_PaintStruct));
		}

	CheckException();
	}

CPaintDC::CPaintDC(HWND hWnd)
{
	m_pWnd = &CWnd::FromHandle(hWnd);

	if( m_pWnd->InPrintClient() ) {

		m_PaintStruct.rcPaint = m_pWnd->GetClientRect();

		Attach(m_pWnd->GetPrintDC());
		}
	else {
		hWnd = m_pWnd->GetHandle();

		Attach(BeginPaint(hWnd, &m_PaintStruct));
		}

	CheckException();
	}

// Destructor

CPaintDC::~CPaintDC(void)
{
	Detach(TRUE);
	}

// Attributes

BOOL CPaintDC::GetEraseFlag(void) const
{
	return m_PaintStruct.fErase;
	}
	
CRect CPaintDC::GetPaintRect(void) const
{
	return m_PaintStruct.rcPaint;
	}

// Destruction

void CPaintDC::DestroyObject(void)
{
	if( !m_pWnd->InPrintClient() ) {

		HWND hWnd = m_pWnd->GetHandle();

		EndPaint(hWnd, &m_PaintStruct);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Memory Device Context
//

// Runtime Class

AfxImplementRuntimeClass(CMemoryDC, CDC);

// Constructor

CMemoryDC::CMemoryDC(void)
{
	Attach(CreateCompatibleDC(NULL));

	CheckException();
	}

CMemoryDC::CMemoryDC(CDC const &Src)
{
	Attach(CreateCompatibleDC(Src));

	CheckException();
	}

// Destructor

CMemoryDC::~CMemoryDC(void)
{
	Detach(TRUE);
	}

// Destruction

void CMemoryDC::DestroyObject(void)
{
	DeleteDC(m_hDC);
	}

// End of File
