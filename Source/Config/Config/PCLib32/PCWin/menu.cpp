
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Menu Object
//

// Dynamic Class

AfxImplementDynamicClass(CMenu, CUserObject);

// Static Data

HANDLE CMenu::m_hMenuPad = NULL;

// Constructors

CMenu::CMenu(void)
{
	}
	
CMenu::CMenu(CMenu const &That)
{
	AfxAssert(FALSE);
	}

CMenu::CMenu(ENTITY ID)
{
	CheckException(Create(ID));
	}

// Destructor

CMenu::~CMenu(void)
{       
	Detach(TRUE);
	}

// Creation

BOOL CMenu::Create(ENTITY ID)
{
	Detach(TRUE);

	HANDLE hObject = CModule::LoadMenu(ID);

	return Attach(hObject);
	}

BOOL CMenu::CreatePopupMenu(void)
{
	Detach(TRUE);

	HANDLE hObject = ::CreatePopupMenu();

	return Attach(hObject);
	}

BOOL CMenu::CreateMenu(void)
{
	Detach(TRUE);

	HANDLE hObject = ::CreateMenu();

	return Attach(hObject);
	}

// Attributes

int CMenu::GetMenuItemCount(void) const
{
	return ::GetMenuItemCount(m_hObject);
	}
	
UINT CMenu::GetMenuItemID(int nPos) const
{
	return ::GetMenuItemID(m_hObject, nPos);
	}

UINT CMenu::GetMenuUniqueID(int nPos) const
{
	INT id = GetMenuItemID(nPos);

	if( id < 0 ) {

		return UINT(GetSubMenu(nPos).GetHandle());
		}

	return id;
	}
	
UINT CMenu::GetMenuState(UINT uID, UINT uFlags) const
{
	return ::GetMenuState(m_hObject, uID, uFlags);
	}
	
CString CMenu::GetMenuString(UINT uID, UINT uFlags) const
{
	TCHAR sText[256];
	
	UINT uCount = ::GetMenuString(m_hObject, uID, sText, sizeof(sText), uFlags);
	
	AfxAssert(uCount < sizeof(sText));
	
	return CString(sText);
	}
	
CMenu & CMenu::GetSubMenu(int nPos) const
{
	return FromHandle(::GetSubMenu(m_hObject, nPos));
	}

BYTE CMenu::GetMenuType(void) const
{
	int nCount = GetMenuItemCount();

	for( int nItem = 0; nItem < nCount; nItem++ ) {

		int nID = GetMenuItemID(nItem);

		if( nID ) {

			if( nID == -1 ) {

				CMenu &Sub = GetSubMenu(nItem);

				return Sub.GetMenuType();
				}

			return HIBYTE(nID);
			}
		}

	return 0xFF;
	}

BOOL CMenu::IsMenuActive(void) const
{
	int nCount = GetMenuItemCount();

	for( int n = 0; n < nCount; n++ ) {
	
		if( GetMenuState(n, MF_BYPOSITION) & MF_POPUP ) {

			if( GetSubMenu(n).IsMenuActive() ) {

				return TRUE;
				}
			}
		else {
			UINT uID = GetMenuItemID(n);

			CCmdSourceData Src;
			
			Src.PrepareSource();
			
			afxMainWnd->RouteControl(uID, Src);
			
			if( !(Src.GetFlags() & MF_DISABLED) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Operations

void CMenu::AppendMenu(UINT uFlags, UINT uNewID, PCTXT pText)
{
	::AppendMenu(m_hObject, uFlags | MF_STRING, uNewID, pText);
	}
	
void CMenu::AppendMenu(UINT uFlags, UINT uNewID, HBITMAP hBitmap)
{
	::AppendMenu(m_hObject, uFlags | MF_BITMAP, uNewID, PCTXT(DWORD(hBitmap)));
	}
	
void CMenu::AppendMenu(UINT uFlags, UINT uNewID, DWORD dwData)
{
	::AppendMenu(m_hObject, uFlags | MF_OWNERDRAW, uNewID, PCTXT(dwData));
	}

void CMenu::AppendMenu(UINT uFlags, CMenu &Menu, PCTXT pText)
{
	AfxValidateObject(Menu);
	
	AfxValidateStringPtr(pText);
	
	::AppendMenu(m_hObject, uFlags | MF_POPUP, UINT(Menu.GetHandle()), pText);

	Menu.SetExtern(TRUE);
	}

void CMenu::AppendMenu(CMenu &Menu)
{
	AfxValidateObject(Menu);
	
	int nCount = Menu.GetMenuItemCount();
	
	for( int nItem = 0; nItem < nCount; nItem++ ) {
	
		int nID = Menu.GetMenuItemID(nItem);
		
		if( nID ) {

			CString Text = Menu.GetMenuString(nItem, MF_BYPOSITION);
			
			if( nID == -1 ) {
		
				CMenu SubMenu;
				
				SubMenu.CreatePopupMenu();
				
				SubMenu.AppendMenu(Menu.GetSubMenu(nItem));
				
				AppendMenu(0, SubMenu, Text);
				}
			else
				AppendMenu(0, nID, Text);
			}
		else
			AppendSeparator();
		}
	}

void CMenu::MergeMenu(CMenu &Menu)
{
	AfxValidateObject(Menu);
	
	int nCount = Menu.GetMenuItemCount();
	
	for( int nItem = 0; nItem < nCount; nItem++ ) {

		AfxAssert(Menu.GetMenuItemID(nItem) == -1);

		CString Text = Menu.GetMenuString(nItem, MF_BYPOSITION);

		CMenu & Item = Menu.GetSubMenu(nItem);

		int nFind;

		for( nFind = 0; nFind < GetMenuItemCount(); nFind++ ) {

			CMenu & Find = GetSubMenu(nFind);
			
			if( Find.GetMenuType() == Item.GetMenuType() ) {

				Find.AppendSeparator();

				Find.AppendMenu(Item);

				break;
				}
			}

		if( nFind == GetMenuItemCount() ) {

			CMenu SubMenu;
			
			SubMenu.CreatePopupMenu();
			
			SubMenu.AppendMenu(Menu.GetSubMenu(nItem));
			
			AppendMenu(0, SubMenu, Text);
			}
		}
	}

void CMenu::AppendSeparator(void)
{
	::AppendMenu(m_hObject, MF_SEPARATOR, 0, NULL);
	}

void CMenu::CheckMenuItem(UINT uID, UINT uFlags)
{
	::CheckMenuItem(m_hObject, uID, uFlags);
	}

void CMenu::DeleteDisabled(void)
{
	int nCount = GetMenuItemCount();

	int nItem;

	for( nItem = 0; nItem < nCount; nItem++ ) {

		int nID = GetMenuItemID(nItem);

		if( nID ) {

			BOOL fKill = FALSE;

			if( nID > 0 ) {

				if( GetMenuState(nItem, MF_BYPOSITION) & MF_DISABLED ) {

					fKill = TRUE;
					}
				}
			else {
				CMenu &Sub = GetSubMenu(nItem);

				Sub.DeleteDisabled();

				if( Sub.GetMenuItemCount() == 1 ) {

					if( Sub.GetMenuItemID(0) > 0 ) {

						UINT    uCmd = Sub.GetMenuItemID(0);

						CString Text = Sub.GetMenuString(0, MF_BYPOSITION);

						InsertMenu(nItem, MF_BYPOSITION, uCmd, Text);

						nItem = nItem + 1;

						fKill = TRUE;
						}
					}

				if( Sub.GetMenuItemCount() == 0 ) {

					fKill = TRUE;
					}
				}

			if( fKill ) {
					
				DeleteMenu(nItem, MF_BYPOSITION);

				if( !nItem || !GetMenuItemID(nItem-1) ) {
				
					while( !GetMenuItemID(nItem) ) {

						DeleteMenu(nItem, MF_BYPOSITION);
						}
					}
				
				nCount = GetMenuItemCount();

				nItem  = nItem - 1;
				}
			}
		}

	while( !GetMenuItemID(nItem - 1) ) {

		DeleteMenu(nItem - 1, MF_BYPOSITION);
		}
	}

void CMenu::DeleteMenu(UINT uID, UINT uFlags)
{
	::DeleteMenu(m_hObject, uID, uFlags);
	}

void CMenu::EnableMenuItem(UINT uID, UINT uFlags)
{
	::EnableMenuItem(m_hObject, uID, uFlags);
	}
	
void CMenu::EnableMenuItem(UINT uID, BOOL fEnable, BOOL fGray)
{
	UINT uFlags = MF_BYCOMMAND;
	
	if( !fEnable ) {
		
		if( fGray ) {
			
			uFlags |= MF_GRAYED;
			}
	
		uFlags |= MF_DISABLED;
		}
	
	::EnableMenuItem(m_hObject, uID, uFlags);
	}

void CMenu::EmptyMenu(void)
{
	int nCount = ::GetMenuItemCount(m_hObject);

	while( nCount-- ) {
		
		::DeleteMenu(m_hObject, 0, MF_BYPOSITION);
		}
		
	AfxAssert(::GetMenuItemCount(m_hObject) == 0);
	}

void CMenu::InsertMenu(UINT uID, UINT uFlags, UINT uNewID, PCTXT pText)
{
	::InsertMenu(m_hObject, uID, uFlags | MF_STRING, uNewID, pText);
	}
	
void CMenu::InsertMenu(UINT uID, UINT uFlags, UINT uNewID, HBITMAP hBitmap)
{
	::InsertMenu(m_hObject, uID, uFlags | MF_BITMAP, uNewID, PCTXT(DWORD(hBitmap)));
	}
	
void CMenu::InsertMenu(UINT uID, UINT uFlags, UINT uNewID, DWORD dwData)
{
	::InsertMenu(m_hObject, uID, uFlags | MF_OWNERDRAW, uNewID, PCTXT(dwData));
	}

void CMenu::InsertMenu(UINT uID, UINT uFlags, CMenu &Menu, PCTXT pText)
{
	AfxValidateObject(Menu);
	
	AfxValidateStringPtr(pText);
	
	::InsertMenu(m_hObject, uID, uFlags | MF_POPUP, UINT(Menu.GetHandle()), pText);

	Menu.SetExtern(TRUE);
	}

void CMenu::InsertSeparator(UINT uID, UINT uFlags)
{
	::InsertMenu(m_hObject, uID, uFlags | MF_SEPARATOR, 0, NULL);
	}

void CMenu::ModifyMenu(UINT uID, UINT uFlags, UINT uNewID, PCTXT pText)
{
	::ModifyMenu(m_hObject, uID, uFlags | MF_STRING, uNewID, pText);
	}
	
void CMenu::ModifyMenu(UINT uID, UINT uFlags, UINT uNewID, HBITMAP hBitmap)
{
	::ModifyMenu(m_hObject, uID, uFlags | MF_BITMAP, uNewID, PCTXT(DWORD(hBitmap)));
	}
	
void CMenu::ModifyMenu(UINT uID, UINT uFlags, UINT uNewID, DWORD dwData)
{
	::ModifyMenu(m_hObject, uID, uFlags | MF_OWNERDRAW, uNewID, PCTXT(dwData));
	}

void CMenu::ModifyMenu(UINT uID, UINT uFlags, CMenu &Menu, PCTXT pText)
{
	AfxValidateObject(Menu);
	
	AfxValidateStringPtr(pText);
	
	::ModifyMenu(m_hObject, uID, uFlags | MF_POPUP, UINT(Menu.GetHandle()), pText);

	Menu.SetExtern(TRUE);
	}

void CMenu::RemoveMenu(UINT uID, UINT uFlags)
{
	::RemoveMenu(m_hObject, uID, uFlags);
	}
	
void CMenu::RemoveMenu(CMenu const &SubMenu)
{
	AfxValidateObject(SubMenu);
	
	int nCount = ::GetMenuItemCount(m_hObject);
	
	for( int nPos = 0; nPos < nCount; nPos++ ) {
	
		if( ::GetSubMenu(m_hObject, nPos) == SubMenu.GetHandle() ) {
		
			::RemoveMenu(m_hObject, UINT(nPos), MF_BYPOSITION);
			
			return;
			}
		}
		
	AfxAssert(FALSE);
	}

void CMenu::SendInitMessage(void)
{
	int nCount = GetMenuItemCount();

	for( int nItem = 0; nItem < nCount; nItem++ ) {

		int nID = GetMenuItemID(nItem);

		if( nID ) {

			if( nID == -1 ) {

				CMenu &Sub = GetSubMenu(nItem);

				Sub.SendInitMessage();
				}
			}
		}

	afxMainWnd->SendMessage( WM_INITMENUPOPUP,
				 WPARAM(m_hObject),
				 LPARAM(0)
				 );
	}	

void CMenu::SetMenuItemBitmaps(UINT uID, UINT uFlags, HBITMAP hMap0, HBITMAP hMap1)
{
	::SetMenuItemBitmaps(m_hObject, uID, uFlags, hMap0, hMap1);
	}

BOOL CMenu::TrackPopupMenu(UINT uFlags, CPoint const &Pos, HWND hWnd)
{
	return ::TrackPopupMenuEx(m_hObject, uFlags, Pos.x, Pos.y, hWnd, NULL);
	}
	
BOOL CMenu::TrackPopupMenu(UINT uFlags, int xPos, int yPos, HWND hWnd)
{
	return ::TrackPopupMenuEx(m_hObject, uFlags, xPos, yPos, hWnd, NULL);
	}
	
// Owner Draw Support

void CMenu::MakeOwnerDraw(BOOL fTop)
{
	int nCount = GetMenuItemCount();
	
	if( fTop ) {

		if( !m_hMenuPad ) {

			CClientDC DC(NULL);

			m_hMenuPad = CreateCompatibleBitmap(DC, 3, 24);
			}

		InsertMenu( 0,
			    MF_BYPOSITION | MF_DISABLED,
			    0,
			    m_hMenuPad
			    );

		nCount++;
		}

	for( int nItem = 0; nItem < nCount; nItem++ ) {

		MakeOwnerDraw(nItem, fTop);
		}
	}

void CMenu::MakeOwnerDraw(int nPos, BOOL fTop)
{
	MENUITEMINFO Info;

	memset(&Info, 0, sizeof(Info));

	Info.cbSize = sizeof(Info);

	Info.fMask  = MIIM_SUBMENU | MIIM_FTYPE | MIIM_ID | MIIM_STATE;

	nPos += (nPos < 0) ? GetMenuItemCount() : 0;

	GetMenuItemInfo(m_hObject, nPos, TRUE, &Info);
	
	if( Info.fType == MFT_STRING || Info.fType == MFT_SEPARATOR || Info.fType == MFT_BITMAP ) {

		CMenuInfo *pInfo = New CMenuInfo;

		pInfo->m_uImage  = 0;

		pInfo->m_fTop    = fTop;

		if( Info.fType == MFT_STRING ) {

			if( Info.hSubMenu == NULL ) {

				CCmdInfo CmdInfo;
			
				afxMainWnd->RouteGetInfo(Info.wID, CmdInfo);

				pInfo->m_uImage = CmdInfo.m_Image;
				}

			pInfo->m_Text = GetMenuString(nPos, MF_BYPOSITION);
			}

		if( Info.fType == MFT_SEPARATOR ) {

			Info.wID    = nPos;

			Info.fState = MFS_DISABLED;
			}

		if( Info.fType == MFT_BITMAP ) {

			Info.wID = 0x80;
			}

		pInfo->m_uID    = Info.wID;

		Info.fMask      = Info.fMask | MIIM_DATA;

		Info.fType      = MFT_OWNERDRAW;

		Info.dwItemData = DWORD(pInfo);

		SetMenuItemInfo(m_hObject, nPos, TRUE, &Info);
		}

	if( Info.hSubMenu ) {

		CMenu &Menu = GetSubMenu(nPos);

		Menu.MakeOwnerDraw(FALSE);
		}
	}
	
void CMenu::FreeOwnerDraw(void)
{
	int nCount = GetMenuItemCount();
	
	for( int nItem = 0; nItem < nCount; nItem++ ) {

		FreeOwnerDraw(nItem);
		}
	}
	
BOOL CMenu::FreeOwnerDraw(int nPos)
{
	MENUITEMINFO Info;

	memset(&Info, 0, sizeof(Info));

	Info.cbSize = sizeof(Info);

	Info.fMask  = MIIM_SUBMENU | MIIM_FTYPE | MIIM_DATA;

	nPos += (nPos < 0) ? GetMenuItemCount() : 0;

	GetMenuItemInfo(m_hObject, nPos, TRUE, &Info);

	if( Info.hSubMenu ) {

		CMenu &Menu = GetSubMenu(nPos);

		Menu.FreeOwnerDraw();
		}

	if( Info.fType == MFT_OWNERDRAW ) {

		CMenuInfo *pInfo = (CMenuInfo *) Info.dwItemData;

		delete pInfo;

		return TRUE;
		}

	return FALSE;
	}
	
// Handle Lookup

CMenu & CMenu::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CMenu NullObject;

		return NullObject;
		}

	CLASS Class = AfxStaticClassInfo();

	return (CMenu &) CHandle::FromHandle(hObject, NS_HMENU, Class);
	}

// Handle Space

WORD CMenu::GetHandleSpace(void) const
{
	return NS_HMENU;
	}

// Destruction

void CMenu::DestroyObject(void)
{
	DestroyMenu(m_hObject);
	}

// End of File
