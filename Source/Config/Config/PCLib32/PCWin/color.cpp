
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Color Object
//

// Standard Constructors

CColor::CColor(void)
{
	m_clrRef = RGB(0, 0, 0);
	}
	
CColor::CColor(CColor const &That)
{
	m_clrRef = That.m_clrRef;
	}
	
CColor::CColor(COLORREF clrRef)
{
	m_clrRef = clrRef;
	}
	
CColor::CColor(BYTE r, BYTE g, BYTE b)
{
	m_clrRef = RGB(r, g, b);
	}

// Explicit Constructors

CColor::CColor(int nElement)
{
	m_clrRef = GetSysColor(nElement);
	}
	
CColor::CColor(CColor const &c1, CColor const &c2, BYTE bMix)
{
	BYTE bInv = BYTE(255 - bMix);

	BYTE r = BYTE((GetRValue(c2) * bMix / 255) + (GetRValue(c1) * bInv / 255));
	BYTE g = BYTE((GetGValue(c2) * bMix / 255) + (GetGValue(c1) * bInv / 255));
	BYTE b = BYTE((GetBValue(c2) * bMix / 255) + (GetBValue(c1) * bInv / 255));

	m_clrRef = RGB(r,g,b);
	}
	
// Assignment Operators

CColor & CColor::operator = (CColor const &That)
{
	m_clrRef = That.m_clrRef;
	
	return ThisObject;
	}

CColor & CColor::operator = (COLORREF clrRef)
{
	m_clrRef = clrRef;
	
	return ThisObject;
	}
	
// Conversion

CColor::operator COLORREF (void) const
{
	return m_clrRef;
	}

// Attributes

BYTE CColor::GetRed(void) const
{
	return GetRValue(m_clrRef);
	}
	
BYTE CColor::GetGreen(void) const
{
	return GetGValue(m_clrRef);
	}
	
BYTE CColor::GetBlue(void) const
{
	return GetBValue(m_clrRef);
	}

DWORD CColor::Flip(void) const
{
	BYTE r = GetRValue(m_clrRef);
	BYTE g = GetGValue(m_clrRef);
	BYTE b = GetBValue(m_clrRef);

	return RGB(b,g,r);
	}

// Comparison Operators

BOOL CColor::operator == (CColor const &Arg) const
{
	return m_clrRef == Arg.m_clrRef;
	}

BOOL CColor::operator != (CColor const &Arg) const
{
	return m_clrRef != Arg.m_clrRef;
	}	

// Bitwise Operators

CColor CColor::operator ~ (void)
{
	CColor Result = ThisObject;

	Result.m_clrRef ^= 0xFFFFFFL;

	return Result;
	}

CColor & CColor::operator &= (CColor const &Arg)
{
	m_clrRef &= Arg.m_clrRef;

	return ThisObject;
	}

CColor & CColor::operator |= (CColor const &Arg)
{
	m_clrRef |= Arg.m_clrRef;

	return ThisObject;
	}

CColor & CColor::operator ^= (CColor const &Arg)
{
	m_clrRef ^= Arg.m_clrRef;

	return ThisObject;
	}

CColor CColor::operator & (CColor const &Arg)
{
	CColor Result = ThisObject;

	Result.m_clrRef &= Arg.m_clrRef;

	return Result;
	}

CColor CColor::operator | (CColor const &Arg)
{
	CColor Result = ThisObject;

	Result.m_clrRef |= Arg.m_clrRef;

	return Result;
	}

CColor CColor::operator ^ (CColor const &Arg)
{
	CColor Result = ThisObject;

	Result.m_clrRef ^= Arg.m_clrRef;

	return Result;
	}

// Logarithmic Operators

CColor CColor::operator - (void)
{
	CColor Result;
	
	Result.m_bColor[0] = Neg(m_bColor[0]);
	Result.m_bColor[1] = Neg(m_bColor[1]);
	Result.m_bColor[2] = Neg(m_bColor[2]);

	return Result;
	}

CColor & CColor::operator *= (CColor const &Arg)
{
	m_bColor[0] = Mul(m_bColor[0], Arg.m_bColor[0]);
	m_bColor[1] = Mul(m_bColor[1], Arg.m_bColor[1]);
	m_bColor[2] = Mul(m_bColor[2], Arg.m_bColor[2]);

	return ThisObject;
	}

CColor & CColor::operator += (CColor const &Arg)
{
	m_bColor[0] = Add(m_bColor[0], Arg.m_bColor[0]);
	m_bColor[1] = Add(m_bColor[1], Arg.m_bColor[1]);
	m_bColor[2] = Add(m_bColor[2], Arg.m_bColor[2]);

	return ThisObject;
	}

CColor CColor::operator * (CColor const &Arg)
{
	CColor Result;

	Result.m_bColor[0] = Mul(m_bColor[0], Arg.m_bColor[0]);
	Result.m_bColor[1] = Mul(m_bColor[1], Arg.m_bColor[1]);
	Result.m_bColor[2] = Mul(m_bColor[2], Arg.m_bColor[2]);

	return Result;
	}

CColor CColor::operator + (CColor const &Arg)
{
	CColor Result;

	Result.m_bColor[0] = Add(m_bColor[0], Arg.m_bColor[0]);
	Result.m_bColor[1] = Add(m_bColor[1], Arg.m_bColor[1]);
	Result.m_bColor[2] = Add(m_bColor[2], Arg.m_bColor[2]);

	return Result;
	}

// Implementation

BYTE CColor::Mul(BYTE b1, BYTE b2)
{
	return BYTE((UINT(b1) * UINT(b2)) / 255);
	}

BYTE CColor::Add(BYTE b1, BYTE b2)
{
	return Neg(Mul(Neg(b1), Neg(b2)));
	}

BYTE CColor::Neg(BYTE b1)
{
	return BYTE(255 - b1);
	}

// End of File
