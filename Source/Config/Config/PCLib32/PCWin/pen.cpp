
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GDI Pen Object
//

// Dynamic Class

AfxImplementDynamicClass(CPen, CGdiObject);

// Constructors

CPen::CPen(void)
{
	}

CPen::CPen(CPen const &Src)
{
	AfxAssert(FALSE);
	}

CPen::CPen(int nStyle, int nWidth, CColor const &Color)
{
	CheckException(Create(nStyle, nWidth, Color));
	}

CPen::CPen(CColor const &Color)
{
	CheckException(Create(Color));
	}

// Creation

BOOL CPen::Create(int nStyle, int nWidth, CColor const &Color)
{
	Detach(TRUE);

	HANDLE hPen = CreatePen(nStyle, nWidth, Color);

	return Attach(hPen);
	}

BOOL CPen::Create(CColor const &Color)
{
	Detach(TRUE);

	HANDLE hPen = CreatePen(PS_SOLID, 0, Color);

	return Attach(hPen);
	}

// Handle Lookup

CPen & CPen::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CPen NullObject;

		return NullObject;
		}

	CLASS Class = AfxStaticClassInfo();

	return (CPen &) CGdiObject::FromHandle(hObject, Class);
	}

// End of File
