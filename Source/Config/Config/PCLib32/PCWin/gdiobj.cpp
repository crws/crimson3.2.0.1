
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic GDI Object
//

// Dynamic Class

AfxImplementDynamicClass(CGdiObject, CHandle);

// Constructor

CGdiObject::CGdiObject(void)
{
	}

CGdiObject::CGdiObject(CGdiObject const &That)
{
	AfxAssert(FALSE);
	}

CGdiObject::CGdiObject(int nStock)
{
	CheckException(CreateStockObject(nStock));
	}

// Destructor

CGdiObject::~CGdiObject(void)
{
	Detach(TRUE);
	}

// Creation

BOOL CGdiObject::CreateStockObject(int nStock)
{
	Detach(TRUE);

	HANDLE hObject = GetStockObject(nStock);

	return Attach(hObject);
	}

// Operations

void CGdiObject::Unrealize(void)
{
	UnrealizeObject(m_hObject);
	}

// Handle Lookup

CGdiObject & CGdiObject::FromHandle(HANDLE hObject, CLASS Class)
{
	if( hObject == NULL ) {

		static CGdiObject NullObject;

		return NullObject;
		}

	return (CGdiObject &) CHandle::FromHandle(hObject, NS_HGDIOBJ, Class);
	}

// Handle Space

WORD CGdiObject::GetHandleSpace(void) const
{
	return NS_HGDIOBJ;
	}

// Destruction

void CGdiObject::DestroyObject(void)
{
	DeleteObject(m_hObject);
	}

// Exceptions

void CGdiObject::CheckException(BOOL fCheck)
{
	if( !fCheck ) {
		
		AfxThrowResourceException();
		}
	}

// End of File
