
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Point Object
//

// Standard Constructors

CPoint::CPoint(void)
{
	x = 0;
	y = 0;
	}

CPoint::CPoint(POINT const &Src)
{
	x = Src.x;
	y = Src.y;
	}

CPoint::CPoint(int sx, int sy)
{
	x = sx;
	y = sy;
	}

// Explicit Constructors

CPoint::CPoint(CSize const &Src)
{
	x = Src.cx;
	y = Src.cy;
	}

CPoint::CPoint(DWORD dwPacked)
{
	x = LOWORD(dwPacked);
	y = HIWORD(dwPacked);
	}

// Assignment Operators

CPoint & CPoint::operator = (POINT const &Src)
{
	x = Src.x;
	y = Src.y;

	return ThisObject;
	}

CPoint & CPoint::operator = (CSize const &Src)
{
	x = Src.cx;
	y = Src.cy;

	return ThisObject;
	}

// Conversions

CPoint::operator POINT * (void)
{
	return this;
	}

CPoint::operator DWORD (void) const
{
	return MAKELONG(x, y);
	}

// Clipping

void CPoint::ClipTo(CRect const &Rect)
{
	MakeMax(x, Rect.left);
	
	MakeMin(x, Rect.right - 1);
	
	MakeMax(y, Rect.top);
	
	MakeMin(y, Rect.bottom - 1);
	}

// Compraison Operators

BOOL CPoint::operator == (CPoint const &Arg) const
{
	return BOOL(x == Arg.x && y == Arg.y);
	}

BOOL CPoint::operator != (CPoint const &Arg) const
{
	return BOOL(x != Arg.x || y != Arg.y);
	}

// Offset Operators

CPoint const & CPoint::operator ++ (void) 
{
	x++;
	y++;   
	
	return ThisObject;
	}

CPoint CPoint::operator ++ (int nDummy)
{
	CPoint Prev = ThisObject;

	x++;
	y++;
	
	return Prev;
	}

CPoint const & CPoint::operator -- (void)
{
	x--;
	y--;

	return ThisObject;
	}
		
CPoint CPoint::operator -- (int nDummy)
{
	CPoint Prev = ThisObject;

	x--;
	y--;

	return Prev;
	}
		
// Offset in Place

CPoint const & CPoint::operator += (int nStep)
{
	x += nStep;
	y += nStep;
	
	return ThisObject;
	}

CPoint const & CPoint::operator += (CSize const &Step)
{
	x += Step.cx;
	y += Step.cy;
	
	return ThisObject;
	}

CPoint const & CPoint::operator += (CPoint const &Pos)
{
	x += Pos.x;
	y += Pos.y;
	
	return ThisObject;
	}

CPoint const & CPoint::operator -= (int nStep)
{
	x -= nStep;
	y -= nStep;
	
	return ThisObject;
	}

CPoint const & CPoint::operator -= (CSize const &Step)
{
	x -= Step.cx;
	y -= Step.cy;
	
	return ThisObject;
	}

CPoint const & CPoint::operator -= (CPoint const &Pos)
{
	x -= Pos.x;
	y -= Pos.y;
	
	return ThisObject;
	}

// Offset via Friends

CPoint operator + (CPoint const &a, int nStep)
{
	CPoint Result;

	Result.x = a.x + nStep;
	Result.y = a.y + nStep;

	return Result;
	}

CPoint operator + (CPoint const &a, CSize const &b)
{
	CPoint Result;

	Result.x = a.x + b.cx;
	Result.y = a.y + b.cy;

	return Result;
	}

CPoint operator + (CPoint const &a, CPoint const &b)
{
	CPoint Result;

	Result.x = a.x + b.x;
	Result.y = a.y + b.y;

	return Result;
	}

CPoint operator - (CPoint const &a, int nStep)
{
	CPoint Result;

	Result.x = a.x - nStep;
	Result.y = a.y - nStep;

	return Result;
	}

CPoint operator - (CPoint const &a, CSize const &b)
{
	CPoint Result;

	Result.x = a.x - b.cx;
	Result.y = a.y - b.cy;

	return Result;
	}

CPoint operator - (CPoint const &a, CPoint const &b)
{
	CPoint Result;

	Result.x = a.x - b.x;
	Result.y = a.y - b.y;

	return Result;
	}

// Distance Calculation

CSize Distance(CPoint const &a, CPoint const &b)
{
	CSize Result;

	Result.cx = a.x - b.x;
	Result.cy = a.y - b.y;

	return Result;
	}

// Scaling in Place

CPoint const & CPoint::operator *= (int nScale)
{
	x *= nScale;
	y *= nScale;
	
	return ThisObject;
	}

CPoint const & CPoint::operator *= (CSize const &Scale)
{
	x *= Scale.cx;
	y *= Scale.cy;
	
	return ThisObject;
	}

CPoint const & CPoint::operator /= (int nScale)
{
	x /= nScale;
	y /= nScale;
	
	return ThisObject;
	}

CPoint const & CPoint::operator /= (CSize const &Scale)
{
	x /= Scale.cx;
	y /= Scale.cy;
	
	return ThisObject;
	}

// Scaling via Friends

CPoint operator * (CPoint const &a, int nScale)
{
	CPoint Result;

	Result.x = a.x * nScale;
	Result.y = a.y * nScale;

	return Result;
	}

CPoint operator * (int nScale, CPoint const &b)
{
	CPoint Result;

	Result.x = nScale * b.x;
	Result.y = nScale * b.y;

	return Result;
	}

CPoint operator * (CPoint const &a, CSize const &b)
{
	CPoint Result;

	Result.x = a.x * b.cx;
	Result.y = a.y * b.cy;

	return Result;
	}

CPoint operator / (CPoint const &a, int nScale)
{
	CPoint Result;

	Result.x = a.x / nScale;
	Result.y = a.y / nScale;

	return Result;
	}

CPoint operator / (CPoint const &a, CSize const &b)
{
	CPoint Result;

	Result.x = a.x / b.cx;
	Result.y = a.y / b.cy;

	return Result;
	}

// Associated Functions

CPoint GetCursorPos(void)
{
	CPoint Pos;
	
	GetCursorPos(&Pos);
	
	return Pos;
	}

CPoint GetCursorPos(CWnd const &Wnd)
{
	CPoint Pos;
	
	GetCursorPos(&Pos);

	Wnd.ScreenToClient(Pos);
	
	return Pos;
	}

// Zeroing

void AfxSetZero(CPoint &p)
{
	p.x = p.y = 0;
	}

// End of File
