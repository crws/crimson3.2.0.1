
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Basic Device Context
//

// Dynamic Class

AfxImplementDynamicClass(CDC, CObject);
		
// Constructors

CDC::CDC(void)
{
	m_fExtern = TRUE;

	m_hDC     = NULL;

	m_uCount  = 0;
	}

CDC::CDC(HDC hDC)
{
	m_fExtern = FALSE;

	m_hDC     = hDC;

	CheckException();

	m_uCount = 0;
	}

// Attachment

BOOL CDC::Attach(HANDLE hDC)
{
	if( hDC ) {
	
		Detach(TRUE);

		m_fExtern = FALSE;

		m_hDC     = hDC;

		afxMap->InsertPerm(hDC, NS_HDC, this);

		return TRUE;
		}

	return FALSE;
	}

void CDC::Detach(BOOL fDelete)
{
	if( m_hDC ) {

		if( fDelete && !m_fExtern ) {

			DestroyObject();
			}

		afxMap->RemoveBoth(m_hDC, NS_HDC, this);

		m_fExtern = TRUE;

		m_hDC     = NULL;
		}
	}

// Attributes

HDC CDC::GetHandle(void) const
{
	return m_hDC;
	}

BOOL CDC::IsValid(void) const
{
	return m_hDC ? TRUE : FALSE;
	}

BOOL CDC::IsNull(void) const
{
	return m_hDC ? FALSE : TRUE;
	}

BOOL CDC::operator ! (void) const
{
	return m_hDC ? FALSE : TRUE;
	}

// Conversion

CDC::operator HDC (void) const
{
	return m_hDC;
	}

// Object Selection

HANDLE CDC::SelectObject(HANDLE hObject)
{
	return ::SelectObject(m_hDC, hObject);
	}

// Object Selection

void CDC::Select(CGdiObject const &Object)
{
	AfxValidateObject(Object);

	HANDLE hLast = ::SelectObject(m_hDC, Object);

	AfxAssert(m_uCount < elements(m_hList));

	m_hList[m_uCount++] = hLast;
	}

void CDC::Replace(CGdiObject const &Object)
{
	AfxValidateObject(Object);

	AfxAssert(m_uCount > 0);

	::SelectObject(m_hDC, Object);
	}

void CDC::Deselect(void)
{
	AfxAssert(m_uCount > 0);

	HANDLE hLast = m_hList[--m_uCount];

	::SelectObject(m_hDC, hLast);
	}

// Current Selections

CBrush & CDC::GetCurrentBrush(void) const
{
	return CBrush::FromHandle(GetCurrentObject(m_hDC, OBJ_BRUSH));
	}

CPen & CDC::GetCurrentPen(void) const
{
	return CPen::FromHandle(GetCurrentObject(m_hDC, OBJ_PEN));
	}

CFont & CDC::GetCurrentFont(void) const
{
	return CFont::FromHandle(GetCurrentObject(m_hDC, OBJ_FONT));
	}

CBitmap & CDC::GetCurrentBitmap(void) const
{
	return CBitmap::FromHandle(GetCurrentObject(m_hDC, OBJ_BITMAP));
	}

// Get Attributes

CColor CDC::GetBkColor(void) const
{
	return ::GetBkColor(m_hDC);
	}

int CDC::GetBkMode(void) const
{
	return ::GetBkMode(m_hDC);
	}

CPoint CDC::GetBrushOrg(void) const
{
	CPoint Result;

	::GetBrushOrgEx(m_hDC, &Result);

	return Result;
	}

int CDC::GetPolyFillMode(void) const
{
	return ::GetPolyFillMode(m_hDC);
	}

int CDC::GetROP2(void) const
{
	return ::GetROP2(m_hDC);	
	}

int CDC::GetStretchBltMode(void) const
{
	return ::GetStretchBltMode(m_hDC);
	}

UINT CDC::GetTextAlign(void) const
{
	return ::GetTextAlign(m_hDC);
	}

int CDC::GetTextCharacterExtra(void) const
{
	return ::GetTextCharacterExtra(m_hDC);
	}

CColor CDC::GetTextColor(void) const
{
	return ::GetTextColor(m_hDC);
	}

// Set Attributes

void CDC::SetBkColor(CColor const &Color)
{
	::SetBkColor(m_hDC, Color);
	}

void CDC::SetBkMode(int nMode)
{
	::SetBkMode(m_hDC, nMode);
	}

void CDC::SetBrushOrg(int xPos, int yPos)
{
	::SetBrushOrgEx(m_hDC, xPos, yPos, NULL);
	}

void CDC::SetPolyFillMode(int nMode)
{
	::SetPolyFillMode(m_hDC, nMode);
	}
	
void CDC::SetROP2(int nMode)
{
	::SetROP2(m_hDC, nMode);
	}
	
void CDC::SetStretchBltMode(int nMode)
{	
	::SetStretchBltMode(m_hDC, nMode);
	}
	
void CDC::SetTextAlign(UINT uAlign)
{
	::SetTextAlign(m_hDC, uAlign);
	}

void CDC::SetTextCharacterExtra(int nExtra)
{
	::SetTextCharacterExtra(m_hDC, nExtra);
	}

void CDC::SetTextJustification(int nExtra, int nCount)
{
	::SetTextJustification(m_hDC, nExtra, nCount);
	}

void CDC::SetTextColor(CColor const &Color)
{
	::SetTextColor(m_hDC, Color);
	}
		
// Get Mapping Info

int CDC::GetMapMode(void) const
{
	return ::GetMapMode(m_hDC);
	}

CSize CDC::GetViewportExt(void) const
{
	CSize Result;
	
	GetViewportExtEx(m_hDC, &Result);
	
	return Result;
	}

CPoint CDC::GetViewportOrg(void) const
{
	CPoint Result;
	
	GetViewportOrgEx(m_hDC, &Result);
	
	return Result;
	}

CSize CDC::GetWindowExt(void) const
{
	CSize Result;
	
	GetWindowExtEx(m_hDC, &Result);
	
	return Result;
	}

CPoint CDC::GetWindowOrg(void) const
{
	CPoint Result;
	
	GetWindowOrgEx(m_hDC, &Result);
	
	return Result;
	}
		
// Set Mapping Info

void CDC::SetMapMode(int nMode)
{
	::SetMapMode(m_hDC, nMode);
	}

void CDC::SetViewportExt(CSize const &Ext)
{
	::SetViewportExtEx(m_hDC, Ext.cx, Ext.cy, NULL);
	}

void CDC::SetViewportExt(int xExt, int yExt)
{
	::SetViewportExtEx(m_hDC, xExt, yExt, NULL);
	}

void CDC::SetViewportOrg(CPoint const &Pos)
{
	::SetViewportOrgEx(m_hDC, Pos.x, Pos.y, NULL);
	}

void CDC::SetViewportOrg(int xPos, int yPos)
{
	::SetViewportOrgEx(m_hDC, xPos, yPos, NULL);
	}

void CDC::SetWindowExt(CSize const &Ext)
{
	::SetWindowExtEx(m_hDC, Ext.cx, Ext.cy, NULL);
	}

void CDC::SetWindowExt(int xExt, int yExt)
{
	::SetWindowExtEx(m_hDC, xExt, yExt, NULL);
	}

void CDC::SetWindowOrg(CPoint const &Pos)
{
	::SetWindowOrgEx(m_hDC, Pos.x, Pos.y, NULL);
	}

void CDC::SetWindowOrg(int xPos, int yPos)
{
	::SetWindowOrgEx(m_hDC, xPos, yPos, NULL);
	}

// DP->LP Coordinate Transforms

void CDC::DPtoLP(CPoint &Point) const
{
	::DPtoLP(m_hDC, PPOINT(&Point), 1);
	}

void CDC::DPtoLP(CSize &Size) const
{
	::DPtoLP(m_hDC, PPOINT(&Size), 1);
	}

void CDC::DPtoLP(PPOINT pPoint, int nCount) const
{
	::DPtoLP(m_hDC, PPOINT(pPoint), nCount);
	}

void CDC::DPtoLP(PSIZE pSize, int nCount) const
{
	::DPtoLP(m_hDC, PPOINT(pSize), nCount);
	}

void CDC::DPtoLP(RECT &Rect) const
{
	::DPtoLP(m_hDC, PPOINT(&Rect), 2);
	}

// LP->DP Coordinate Transforms

void CDC::LPtoDP(CPoint &Point) const
{
	::LPtoDP(m_hDC, PPOINT(&Point), 1);
	}

void CDC::LPtoDP(CSize &Size) const
{
	::LPtoDP(m_hDC, PPOINT(&Size), 1);
	}

void CDC::LPtoDP(PPOINT pPoint, int nCount) const
{
	::LPtoDP(m_hDC, PPOINT(pPoint), nCount);
	}

void CDC::LPtoDP(PSIZE pSize, int nCount) const
{
	::LPtoDP(m_hDC, PPOINT(pSize), nCount);
	}

void CDC::LPtoDP(RECT &Rect) const
{
	::LPtoDP(m_hDC, PPOINT(&Rect), 2);
	}

// Clipping Status

int CDC::GetClipBox(CRect &Rect) const
{
	AfxValidateWritePtr(&Rect, sizeof(CRect));
	
	return ::GetClipBox(m_hDC, &Rect);
	}

CRect CDC::GetClipBox(void) const
{
	CRect Result;
	
	::GetClipBox(m_hDC, &Result);
	
	return Result;
	}

BOOL CDC::PtVisible(CPoint const &Pos) const
{
	return ::PtVisible(m_hDC, Pos.x, Pos.y);
	}

BOOL CDC::RectVisible(CRect const &Rect) const
{
	return ::RectVisible(m_hDC, &Rect);
	}
		
// Clipping Control

int CDC::ExcludeClipRect(CRect const &Rect)
{
	return ::ExcludeClipRect(m_hDC, Rect.left, Rect.top, Rect.right, Rect.bottom);
	}

int CDC::ExcludeClipRect(int x1, int y1, int x2, int y2)
{
	return ::ExcludeClipRect(m_hDC, x1, y1, x2, y2);
	}

int CDC::IntersectClipRect(CRect const &Rect)
{
	return ::IntersectClipRect(m_hDC, Rect.left, Rect.top, Rect.right, Rect.bottom);
	}

int CDC::IntersectClipRect(int x1, int y1, int x2, int y2)
{
	return ::IntersectClipRect(m_hDC, x1, y1, x2, y2);
	}

int CDC::OffsetClipRgn(CSize const &Step)
{
	return ::OffsetClipRgn(m_hDC, Step.cx, Step.cy);
	}

int CDC::OffsetClipRgn(int xStep, int yStep)
{
	return ::OffsetClipRgn(m_hDC, xStep, yStep);
	}

// Pixel Read

CColor CDC::GetPixel(int xPos, int yPos) const
{
	return ::GetPixel(m_hDC, xPos, yPos);
	}
	
CColor CDC::GetPixel(CPoint const &Pos) const
{
	return ::GetPixel(m_hDC, Pos.x, Pos.y);
	}
	
// Pixel Write

void CDC::SetPixel(int xPos, int yPos, CColor const &Color)
{
	::SetPixel(m_hDC, xPos, yPos, Color);
	}
		
void CDC::SetPixel(CPoint const &Pos, CColor const &Color)
{	
	::SetPixel(m_hDC, Pos.x, Pos.y, Color);
	}
	
// Line Drawing

void CDC::MoveTo(int xPos, int yPos)
{
	::MoveToEx(m_hDC, xPos, yPos, NULL);
	}

void CDC::MoveTo(CPoint const &Pos)
{
	::MoveToEx(m_hDC, Pos.x, Pos.y, NULL);
	}

void CDC::LineTo(int xPos, int yPos)
{
	::LineTo(m_hDC, xPos, yPos);
	}

void CDC::LineTo(CPoint const &Pos)
{
	::LineTo(m_hDC, Pos.x, Pos.y);
	}

void CDC::Arc(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
{
	::Arc(m_hDC, x1, y1, x2, y2, x3, y3, x4, y4);
	}

void CDC::Arc(CRect const &Rect, CPoint const &p1, CPoint const &p2)
{
	::Arc(m_hDC, Rect.left, Rect.top, Rect.right, Rect.bottom, p1.x, p1.y, p2.x, p2.y);
	}

void CDC::Polyline(PPOINT pPoint, UINT uCount)
{
	AfxAssert(uCount > 0);

	AfxValidateReadPtr(pPoint, uCount * sizeof(POINT));
	
	::Polyline(m_hDC, pPoint, uCount);
	}

// Simple Drawing

void CDC::FillRect(int x1, int y1, int x2, int y2, CBrush const &Brush)
{
	AfxValidateObject(Brush);
	
	RECT Rect = { x1, y1, x2, y2 };
	
	::FillRect(m_hDC, &Rect, Brush.GetHandle());
	}

void CDC::FillRect(CRect const &Rect, CBrush const &Brush)
{
	AfxValidateObject(Brush);
	
	::FillRect(m_hDC, &Rect, Brush.GetHandle());
	}

void CDC::FrameRect(int x1, int y1, int x2, int y2, CBrush const &Brush)
{
	AfxValidateObject(Brush);
	
	RECT Rect = { x1, y1, x2, y2 };
	
	::FrameRect(m_hDC, &Rect, Brush.GetHandle());
	}

void CDC::FrameRect(CRect const &Rect, CBrush const &Brush)
{
	AfxValidateObject(Brush);
	
	::FrameRect(m_hDC, &Rect, Brush.GetHandle());
	}

void CDC::InvertRect(int x1, int y1, int x2, int y2)
{
	RECT Rect = { x1, y1, x2, y2 };
	
	::InvertRect(m_hDC, &Rect);
	}

void CDC::InvertRect(CRect const &Rect)
{
	::InvertRect(m_hDC, &Rect);
	}

// Figure Drawing

void CDC::Rectangle(int x1, int y1, int x2, int y2)
{
	::Rectangle(m_hDC, x1, y1, x2, y2);
	}

void CDC::Rectangle(CRect const &Rect)
{
	::Rectangle(m_hDC, Rect.left, Rect.top, Rect.right, Rect.bottom);
	}

void CDC::Ellipse(int x1, int y1, int x2, int y2)
{
	::Ellipse(m_hDC, x1, y1, x2, y2);
	}

void CDC::Ellipse(CRect const &Rect)
{
	::Ellipse(m_hDC, Rect.left, Rect.top, Rect.right, Rect.bottom);
	}

void CDC::RoundRect(int x1, int y1, int x2, int y2, int cx, int cy)
{
	::RoundRect(m_hDC, x1, y1, x2, y2, cx, cy);
	}

void CDC::RoundRect(CRect const &Rect, CSize const &Size)
{
	::RoundRect(m_hDC, Rect.left, Rect.top, Rect.right, Rect.bottom, Size.cx, Size.cy);
	}

void CDC::RoundRect(CRect const &Rect, int nSize)
{
	::RoundRect(m_hDC, Rect.left, Rect.top, Rect.right, Rect.bottom, nSize, nSize);
	}
	
void CDC::Diamond(int x1, int y1, int x2, int y2)
{
	POINT p[4];

	p[0].x = (x1 + x2) / 2;
	p[2].x = (x1 + x2) / 2;

	p[1].y = (y1 + y2) / 2;
	p[3].y = (y1 + y2) / 2;

	p[0].y = y1;
	p[1].x = x2 - 1;
	p[2].y = y2 - 1;
	p[3].x = x1;

	::Polygon(m_hDC, p, 4);
	}

void CDC::Diamond(CRect const &Rect)
{
	POINT p[4];

	p[0].x = (Rect.left + Rect.right) / 2;
	p[2].x = (Rect.left + Rect.right) / 2;

	p[1].y = (Rect.top + Rect.bottom) / 2;
	p[3].y = (Rect.top + Rect.bottom) / 2;

	p[0].y = Rect.top;
	p[1].x = Rect.right  - 1;
	p[2].y = Rect.bottom - 1;
	p[3].x = Rect.left;

	::Polygon(m_hDC, p, 4);
	}

// Graduated Fills

void CDC::GradHorz(CRect const &Rect, CColor const &c1, CColor const &c2)
{
	CRect Line = Rect;

	int cx = Rect.cx();

	for( int n = 0; n < cx; n++ ) {

		Line.left  = Rect.left + n;

		Line.right = Line.left + 1;

		CColor Color(c1, c2, BYTE(n * 255 / (cx - 1)));

		FillRect(Line, CBrush(Color));
		}
	}

void CDC::GradVert(CRect const &Rect, CColor const &c1, CColor const &c2)
{
	CRect Line = Rect;

	int cy = Rect.cy();

	for( int n = 0; n < cy; n++ ) {

		Line.top    = Rect.top + n;

		Line.bottom = Line.top + 1;

		CColor Color(c1, c2, BYTE(n * 255 / (cy - 1)));

		FillRect(Line, CBrush(Color));
		}
	}

// PatBlt Functions

void CDC::PatBlt(int xDest, int yDest, int xSize, int ySize, DWORD dwRop)
{
	::PatBlt(m_hDC, xDest, yDest, xSize, ySize, dwRop);
	}
		
void CDC::PatBlt(CPoint const &Dest, CSize const &Size, DWORD dwRop)
{
	::PatBlt(m_hDC, Dest.x, Dest.y, Size.cx, Size.cy, dwRop);
	}

void CDC::PatBlt(CRect const &Rect, DWORD dwRop)
{
	::PatBlt( m_hDC, Rect.left, Rect.top, Rect.GetWidth(), Rect.GetHeight(), dwRop);
	}

// Bitmap Transfer from DC

void CDC::BitBlt(CRect const &Rect, CDC &SrcDC, CPoint const &Src, DWORD dwRop)
{
	::BitBlt(m_hDC, Rect.left, Rect.top, Rect.cx(), Rect.cy(), SrcDC, Src.x, Src.y, dwRop);
	}

void CDC::BitBlt(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src, DWORD dwRop)
{
	::BitBlt(m_hDC, Dest.x, Dest.y, Size.cx, Size.cy, SrcDC, Src.x, Src.y, dwRop);
	}

void CDC::BitBlt(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc, DWORD dwRop)
{
	::BitBlt(m_hDC, xDest, yDest, xSize, ySize, SrcDC, xSrc, ySrc, dwRop);
	}

// Bitmap Transfer from Bitmap

void CDC::BitBlt(int xDest, int yDest, int xSize, int ySize, CBitmap const &SrcBM, int xSrc, int ySrc, DWORD dwRop)
{
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	BitBlt(xDest, yDest, xSize, ySize, SrcDC, xSrc, ySrc, dwRop);

	SrcDC.Deselect();
	}

void CDC::BitBlt(CPoint const &Dest, CSize const &Size, CBitmap const &SrcBM, CPoint const &Src, DWORD dwRop)
{
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	BitBlt(Dest, Size, SrcDC, Src, dwRop);

	SrcDC.Deselect();
	}

void CDC::BitBlt(CRect const &Rect, CBitmap const &SrcBM, CPoint const &Src, DWORD dwRop)
{	
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	BitBlt(Rect, SrcDC, Src, dwRop);

	SrcDC.Deselect();
	}

// Stretch Transfer from DC

void CDC::StretchBlt(int xDest, int yDest, int cxDest, int cyDest, CDC &SrcDC, int xSrc, int ySrc, int cxSrc, int cySrc, DWORD dwRop)
{
	::StretchBlt(m_hDC, xDest, yDest, cxDest, cyDest, SrcDC, xSrc, ySrc, cxSrc, cySrc, dwRop);
	}

void CDC::StretchBlt(CPoint const &Dest, CSize const &DestSize, CDC &SrcDC, CPoint const &Src, CSize const &SrcSize, DWORD dwRop)
{
	::StretchBlt(m_hDC, Dest.x, Dest.y, DestSize.cx, DestSize.cy, SrcDC, Src.x, Src.y, SrcSize.cx, SrcSize.cy, dwRop);
	}

void CDC::StretchBlt(CRect const &Rect, CDC &SrcDC, CRect const &Src, DWORD dwRop)
{
	::StretchBlt(m_hDC, Rect.left, Rect.top, Rect.cx(), Rect.cy(), SrcDC, Src.left, Src.top, Src.cx(), Src.cy(), dwRop);
	}

// Stretch Transfer from Bitmap

void CDC::StretchBlt(int xDest, int yDest, int cxDest, int cyDest, CBitmap const &SrcBM, int xSrc, int ySrc, int cxSrc, int cySrc, DWORD dwRop)
{
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	StretchBlt(xDest, yDest, cxDest, cyDest, SrcDC, xSrc, ySrc, cxSrc, cySrc, dwRop);

	SrcDC.Deselect();
	}

void CDC::StretchBlt(CPoint const &Dest, CSize const &DestSize, CBitmap const &SrcBM, CPoint const &Src, CSize const &SrcSize, DWORD dwRop)
{
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	StretchBlt(Dest, DestSize, SrcDC, Src, SrcSize, dwRop);

	SrcDC.Deselect();
	}

void CDC::StretchBlt(CRect const &Rect, CBitmap const &SrcBM, CRect const &Src, DWORD dwRop)
{
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	StretchBlt(Rect, SrcDC, Src, dwRop);

	SrcDC.Deselect();
	}

// Transparent Transfer from DC
		
void CDC::TransBlt(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc)
{
	TransBlt(CRect(xDest, yDest, xDest + xSize, yDest + ySize), SrcDC, CPoint(xSrc, ySrc));
	}

void CDC::TransBlt(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src)
{
	TransBlt(CRect(Dest, Size), SrcDC, Src);
	}

void CDC::TransBlt(CRect const &Rect, CDC &SrcDC, CPoint const &Src)
{
	int cx = Rect.cx();

	int cy = Rect.cy();

	TransparentBlt(m_hDC, Rect.left, Rect.top, cx, cy, SrcDC, Src.x, Src.y, cx, cy, GetBkColor());
	}

// Transparent Transfer from Bitmap

void CDC::TransBlt(int xDest, int yDest, int xSize, int ySize, CBitmap const &SrcBM, int xSrc, int ySrc)
{
	TransBlt(CRect(xDest, yDest, xDest + xSize, yDest + ySize), SrcBM, CPoint(xSrc, ySrc));
	}

void CDC::TransBlt(CPoint const &Dest, CSize const &Size, CBitmap const &SrcBM, CPoint const &Src)
{
	TransBlt(CRect(Dest, Size), SrcBM, Src);
	}

void CDC::TransBlt(CRect const &Rect, CBitmap const &SrcBM, CPoint const &Src)
{	
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	TransBlt(Rect, SrcDC, Src);

	SrcDC.Deselect();
	}

// Alpha Blended Transfer from DC

void CDC::AlphaBlend(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc)
{
	AlphaBlend(CRect(xDest, yDest, xSize, ySize), SrcDC, CPoint(xSrc, ySrc));
	}

void CDC::AlphaBlend(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src)
{
	AlphaBlend(CRect(Dest, Size), SrcDC, Src);
	}

void CDC::AlphaBlend(CRect const &Rect, CDC &SrcDC, CPoint const &Src)
{
	int cx = Rect.cx();

	int cy = Rect.cy();
	
	BLENDFUNCTION Blend;

	Blend.BlendOp             = AC_SRC_OVER;
	Blend.BlendFlags          = 0;
	Blend.SourceConstantAlpha = 255;
	Blend.AlphaFormat         = AC_SRC_ALPHA;

	::AlphaBlend(m_hDC, Rect.left, Rect.top, cx, cy, SrcDC, Src.x, Src.y, cx, cy, Blend);
	}

void CDC::AlphaBlend(int xDest, int yDest, int xSize, int ySize, CDC &SrcDC, int xSrc, int ySrc, int nAlpha)
{
	AlphaBlend(CRect(xDest, yDest, xSize, ySize), SrcDC, CPoint(xSrc, ySrc), nAlpha);
	}

void CDC::AlphaBlend(CPoint const &Dest, CSize const &Size, CDC &SrcDC, CPoint const &Src, int nAlpha)
{
	AlphaBlend(CRect(Dest, Size), SrcDC, Src, nAlpha);
	}

void CDC::AlphaBlend(CRect const &Rect, CDC &SrcDC, CPoint const &Src, int nAlpha)
{
	int cx = Rect.cx();

	int cy = Rect.cy();
	
	BLENDFUNCTION Blend;

	Blend.BlendOp             = AC_SRC_OVER;
	Blend.BlendFlags          = 0;
	Blend.SourceConstantAlpha = BYTE(nAlpha);
	Blend.AlphaFormat         = 0;

	::AlphaBlend(m_hDC, Rect.left, Rect.top, cx, cy, SrcDC, Src.x, Src.y, cx, cy, Blend);
	}

// Alpha Blended Transfer from Bitmap

void CDC::AlphaBlend(int xDest, int yDest, int xSize, int ySize, CBitmap &SrcBM, int xSrc, int ySrc)
{
	AlphaBlend(CRect(xDest, yDest, xSize, ySize), SrcBM, CPoint(xSrc, ySrc));
	}

void CDC::AlphaBlend(CPoint const &Dest, CSize const &Size, CBitmap &SrcBM, CPoint const &Src)
{
	AlphaBlend(CRect(Dest, Size), SrcBM, Src);
	}

void CDC::AlphaBlend(CRect const &Rect, CBitmap &SrcBM, CPoint const &Src)
{
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	int cx = Rect.cx();

	int cy = Rect.cy();
	
	BLENDFUNCTION Blend;

	Blend.BlendOp             = AC_SRC_OVER;
	Blend.BlendFlags          = 0;
	Blend.SourceConstantAlpha = 255;
	Blend.AlphaFormat         = AC_SRC_ALPHA;

	::AlphaBlend(m_hDC, Rect.left, Rect.top, cx, cy, SrcDC, Src.x, Src.y, cx, cy, Blend);

	SrcDC.Deselect();
	}

void CDC::AlphaBlend(int xDest, int yDest, int xSize, int ySize, CBitmap &SrcBM, int xSrc, int ySrc, int nAlpha)
{
	AlphaBlend(CRect(xDest, yDest, xSize, ySize), SrcBM, CPoint(xSrc, ySrc), nAlpha);
	}

void CDC::AlphaBlend(CPoint const &Dest, CSize const &Size, CBitmap &SrcBM, CPoint const &Src, int nAlpha)
{
	AlphaBlend(CRect(Dest, Size), SrcBM, Src, nAlpha);
	}

void CDC::AlphaBlend(CRect const &Rect, CBitmap &SrcBM, CPoint const &Src, int nAlpha)
{
	AfxValidateObject(SrcBM);

	CMemoryDC SrcDC(ThisObject);

	SrcDC.Select(SrcBM);

	int cx = Rect.cx();

	int cy = Rect.cy();
	
	BLENDFUNCTION Blend;

	Blend.BlendOp             = AC_SRC_OVER;
	Blend.BlendFlags          = 0;
	Blend.SourceConstantAlpha = BYTE(nAlpha);
	Blend.AlphaFormat         = 0;

	::AlphaBlend(m_hDC, Rect.left, Rect.top, cx, cy, SrcDC, Src.x, Src.y, cx, cy, Blend);

	SrcDC.Deselect();
	}

// Icon Drawing

void CDC::DrawIcon(int xPos, int yPos, CIcon const &Icon)
{
	::DrawIcon(m_hDC, xPos, yPos, Icon);
	}

void CDC::DrawIcon(CPoint const &Pos, CIcon const &Icon)
{
	::DrawIcon(m_hDC, Pos.x, Pos.y, Icon);
	}

void CDC::DrawIcon(int xPos, int yPos, CIcon const &Icon, int xSize, int ySize)
{
	::DrawIcon(m_hDC, xPos, yPos, Icon);
	}

void CDC::DrawIcon(CPoint const &Pos, CIcon const &Icon, CSize const &Size)
{
	::DrawIcon(m_hDC, Pos.x, Pos.y, Icon);
	}

void CDC::DrawIcon(CRect const &Rect, CIcon const &Icon)
{
	::DrawIcon(m_hDC, Rect.left, Rect.top, Icon);
	}

void CDC::DrawIcon(int xPos, int yPos, CIcon const &Icon, CBrush const &Brush)
{
	DrawIconEx(m_hDC, xPos, yPos, Icon, Icon.GetSize().cx, Icon.GetSize().cy, NULL, Brush, DI_NORMAL);
	}

void CDC::DrawIcon(CPoint const &Pos, CIcon const &Icon, CBrush const &Brush)
{
	DrawIconEx(m_hDC, Pos.x, Pos.y, Icon, Icon.GetSize().cx, Icon.GetSize().cy, NULL, Brush, DI_NORMAL);
	}

void CDC::DrawIcon(int xPos, int yPos, CIcon const &Icon, int xSize, int ySize, CBrush const &Brush)
{
	DrawIconEx(m_hDC, xPos, yPos, Icon, xSize, ySize, NULL, Brush, DI_NORMAL);
	}

void CDC::DrawIcon(CPoint const &Pos, CIcon const &Icon, CSize const &Size, CBrush const &Brush)
{
	DrawIconEx(m_hDC, Pos.x, Pos.y, Icon, Size.cx, Size.cy, NULL, Brush, DI_NORMAL);
	}

void CDC::DrawIcon(CRect const &Rect, CIcon const &Icon, CBrush const &Brush)
{
	DrawIconEx(m_hDC, Rect.left, Rect.top, Icon, Rect.cx(), Rect.cy(), NULL, Brush, DI_NORMAL);
	}

// Text Information

void CDC::GetTextMetrics(TEXTMETRIC &TextMetric) const
{
	::GetTextMetrics(m_hDC, &TextMetric);
	}

CSize CDC::GetTextExtent(PCTXT pString, int nLength) const
{
	CSize Size;

	::GetTextExtentPoint32(m_hDC, pString, Length(pString, nLength), &Size);

	return Size;
	}

CSize CDC::GetTextExtent(CString const &String) const
{
	CSize Size;

	::GetTextExtentPoint32(m_hDC, String, String.GetLength(), &Size);

	return Size;
	}

CString CDC::GetTextFace(void) const
{
	TCHAR sBuffer[256];

	::GetTextFace(m_hDC, sizeof(sBuffer), sBuffer);

	return sBuffer;
	}

// Tabbed Text Information

CSize CDC::GetTabbedTextExtent(PCTXT pString, int nLength, UINT uCount, int *pTable)
{
	DWORD dwResult = ::GetTabbedTextExtent(m_hDC, pString, Length(pString, nLength), uCount, pTable);
	
	return CSize(dwResult);
	}

CSize CDC::GetTabbedTextExtent(CString const &String, UINT uCount, int *pTable)
{
	DWORD dwResult = ::GetTabbedTextExtent(m_hDC, String, String.GetLength(), uCount, pTable);

	return CSize(dwResult);
	}

// TextOut Functions

void CDC::TextOut(int xPos, int yPos, PCTXT pString, int nLength)
{
	::TextOut(m_hDC, xPos, yPos, pString, Length(pString, nLength));
	}

void CDC::TextOut(int xPos, int yPos, CString const &String)
{
	::TextOut(m_hDC, xPos, yPos, String, String.GetLength());
	}

void CDC::TextOut(CPoint const &Pos, PCTXT pString, int nLength)
{
	::TextOut(m_hDC, Pos.x, Pos.y, pString, Length(pString, nLength));
	}

void CDC::TextOut(CPoint const &Pos, CString const &String)
{
	::TextOut(m_hDC, Pos.x, Pos.y, String, String.GetLength());
	}

// ExtTextOut Functions

void CDC::ExtTextOut(int xPos, int yPos, UINT uOptions, CRect const &Rect, PCTXT pString, int nLength, int *pTabs)
{
	::ExtTextOut(m_hDC, xPos, yPos, uOptions, &Rect, pString, Length(pString, nLength), pTabs);
	}

void CDC::ExtTextOut(int xPos, int yPos, UINT uOptions, CRect const &Rect, CString const &String, int *pTabs)
{
	::ExtTextOut(m_hDC, xPos, yPos, uOptions, &Rect, String, String.GetLength(), pTabs);
	}

void CDC::ExtTextOut(CPoint const &Pos, UINT uOptions, CRect const &Rect, PCTXT pString, int nLength, int *pTabs)
{
	::ExtTextOut(m_hDC, Pos.x, Pos.y, uOptions, &Rect, pString, Length(pString, nLength), pTabs);
	}

void CDC::ExtTextOut(CPoint const &Pos, UINT uOptions, CRect const &Rect, CString const &String, int *pTabs)
{
	::ExtTextOut(m_hDC, Pos.x, Pos.y, uOptions, &Rect, String, String.GetLength(), pTabs);
	}

// Gray String Functions

void CDC::GrayString(CBrush const &Brush, PCTXT pString, int nLength, int xPos, int yPos)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(pString), nLength, xPos, yPos, 0, 0);
	}

void CDC::GrayString(CBrush const &Brush, CString const &String, int xPos, int yPos)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(PCTXT(String)), String.GetLength(), xPos, yPos, 0, 0);
	}

void CDC::GrayString(CBrush const &Brush, PCTXT pString, int nLength, int xPos, int yPos, int xSize, int ySize)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(pString), nLength, xPos, yPos, xSize, ySize);
	}

void CDC::GrayString(CBrush const &Brush, CString const &String, int xPos, int yPos, int xSize, int ySize)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(PCTXT(String)), String.GetLength(), xPos, yPos, xSize, ySize);
	}

void CDC::GrayString(CBrush const &Brush, PCTXT pString, int nLength, CPoint const &Point)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(pString), nLength, Point.x, Point.y, 0, 0);
	}

void CDC::GrayString(CBrush const &Brush, CString const &String, CPoint const &Point)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(PCTXT(String)), String.GetLength(), Point.x, Point.y, 0, 0);
	}

void CDC::GrayString(CBrush const &Brush, PCTXT pString, int nLength, CPoint const &Point, CSize const &Size)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(pString), nLength, Point.x, Point.y, Size.cx, Size.cy);
	}

void CDC::GrayString(CBrush const &Brush, CString const &String, CPoint const &Point, CSize const &Size)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(PCTXT(String)), String.GetLength(), Point.x, Point.y, Size.cx, Size.cy);
	}

void CDC::GrayString(CBrush const &Brush, PCTXT pString, int nLength, CRect const &Rect)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(pString), nLength, Rect.left, Rect.top, Rect.cx(), Rect.cy());
	}

void CDC::GrayString(CBrush const &Brush, CString const &String, CRect const &Rect)
{
	::GrayString(m_hDC, Brush, NULL, LPARAM(PCTXT(String)), String.GetLength(), Rect.left, Rect.top, Rect.cx(), Rect.cy());
	}
		
// DrawText Functions

void CDC::DrawText(PCTXT pString, int nLength, CRect const &Rect, UINT uFormat)
{
	::DrawText(m_hDC, pString, Length(pString, nLength), (LPRECT) &Rect, uFormat);
	}

void CDC::DrawText(CString const &String, CRect const &Rect, UINT uFormat)
{
	::DrawText(m_hDC, String, String.GetLength(), (LPRECT) &Rect, uFormat);
	}

// TabbedTextOut Functions

CSize CDC::TabbedTextOut(int xPos, int yPos, PCTXT pString, int nLength, UINT uCount, int *pTable, int xBase)
{
	DWORD dwResult = ::TabbedTextOut(m_hDC, xPos, yPos, pString, Length(pString, nLength), uCount, pTable, xBase);

	return CSize(dwResult);
	}

CSize CDC::TabbedTextOut(int xPos, int yPos, CString const &String, UINT uCount, int *pTable, int xBase)
{
	DWORD dwResult = ::TabbedTextOut(m_hDC, xPos, yPos, String, String.GetLength(), uCount, pTable, xBase);

	return CSize(dwResult);
	}

CSize CDC::TabbedTextOut(CPoint const &Pos, PCTXT pString, int nLength, UINT uCount, int *pTable, int xBase)
{
	DWORD dwResult = ::TabbedTextOut(m_hDC, Pos.x, Pos.y, pString, Length(pString, nLength), uCount, pTable, xBase);

	return CSize(dwResult);
	}

CSize CDC::TabbedTextOut(CPoint const &Pos, CString const &String, UINT uCount, int *pTable, int xBase)
{
	DWORD dwResult = ::TabbedTextOut(m_hDC, Pos.x, Pos.y, String, String.GetLength(), uCount, pTable, xBase);

	return CSize(dwResult);
	}

// 3D Drawing Routines

void CDC::DrawState(HANDLE hBrush, LPARAM lParam, WPARAM wParam, CRect const &Rect, UINT uFlags)
{
	::DrawState(m_hDC, hBrush, NULL, lParam, wParam, Rect.left, Rect.top, Rect.cx(), Rect.cy(), uFlags); 
	}

void CDC::DrawState(CString const &String, CRect const &Rect, BOOL fPrefix, UINT uFlags)
{
	LPARAM lParam = LPARAM(PCTXT(String));

	WPARAM wParam = String.GetLength();

	uFlags |= (fPrefix ? DST_PREFIXTEXT : DST_TEXT);

	::DrawState(m_hDC, NULL, NULL, lParam, wParam, Rect.left, Rect.top, Rect.cx(), Rect.cy(), uFlags);
	}

void CDC::DrawState(CIcon const &Icon, CRect const &Rect, UINT uFlags)
{
	LPARAM lParam = LPARAM(HANDLE(Icon));

	WPARAM wParam = 0;
	
	uFlags |= DST_ICON;

	::DrawState(m_hDC, NULL, NULL, lParam, wParam, Rect.left, Rect.top, Rect.cx(), Rect.cy(), uFlags);
	}

void CDC::DrawState(CBitmap const &Bitmap, CRect const &Rect, UINT uFlags)
{
	LPARAM lParam = LPARAM(HANDLE(Bitmap));

	WPARAM wParam = 0;
	
	uFlags |= DST_BITMAP;

	::DrawState(m_hDC, NULL, NULL, lParam, wParam, Rect.left, Rect.top, Rect.cx(), Rect.cy(), uFlags);
	}

CRect CDC::DrawEdge(CRect const &Rect, UINT uEdge, UINT uFlags)
{
	CRect Work = Rect;

	::DrawEdge(m_hDC, Work, uEdge, uFlags | BF_ADJUST);

	return Work;
	}

// Rectangle Edges

void CDC::DrawTopEdge(CRect &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.left, Rect.top, Rect.right, Rect.top + n, Brush);
	
	Rect.top += n;
	}

void CDC::DrawBottomEdge(CRect &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.left, Rect.bottom - n, Rect.right, Rect.bottom, Brush);
	
	Rect.bottom -= n;
	}

void CDC::DrawLeftEdge(CRect &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.left, Rect.top, Rect.left + n, Rect.bottom, Brush);
	
	Rect.left += n;
	}

void CDC::DrawRightEdge(CRect &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.right - n, Rect.top, Rect.right, Rect.bottom, Brush);
	
	Rect.right -= n;
	}

void CDC::DrawTopEdge(CRect const &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.left, Rect.top, Rect.right, Rect.top + n, Brush);
	}

void CDC::DrawBottomEdge(CRect const &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.left, Rect.bottom - n, Rect.right, Rect.bottom, Brush);
	}

void CDC::DrawLeftEdge(CRect const &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.left, Rect.top, Rect.left + n, Rect.bottom, Brush);
	}

void CDC::DrawRightEdge(CRect const &Rect, int n, CBrush const &Brush)
{
	FillRect(Rect.right - n, Rect.top, Rect.right, Rect.bottom, Brush);
	}

// Focus Rect Helper

void CDC::DrawFocusRect(CRect const &Rect)
{
	::DrawFocusRect(m_hDC, &Rect);
	}

// Frame Control Functions

void CDC::DrawFrameControl(CRect const &Rect, UINT uType, UINT uState)
{
	::DrawFrameControl(m_hDC, PRECT(&Rect), uType, uState);
	}

void CDC::DrawButtonFrameControl(CRect const &Rect, UINT uState)
{
	::DrawFrameControl(m_hDC, PRECT(&Rect), DFC_BUTTON, uState);
	}

void CDC::DrawCaptionFrameControl(CRect const &Rect, UINT uState)
{
	::DrawFrameControl(m_hDC, PRECT(&Rect), DFC_CAPTION, uState);
	}

void CDC::DrawScrollFrameControl(CRect const &Rect, UINT uState)
{
	::DrawFrameControl(m_hDC, PRECT(&Rect), DFC_SCROLL, uState);
	}

// Stacking

void CDC::Save(void)
{
	SaveDC(m_hDC);
	}

void CDC::Restore(void)
{
	RestoreDC(m_hDC, -1);
	}

// Handle Lookup

CDC & CDC::FromHandle(HDC hDC)
{
	return FromHandle(hDC, AfxRuntimeClass(CDC));
	}

CDC & CDC::FromHandle(HDC hDC, CLASS Class)
{
	if( hDC == NULL ) {

		static CDC NullObject;

		return NullObject;
		}

	CDC *pDC = (CDC *) afxMap->FromHandle(hDC, NS_HDC);

	if( pDC ) {
	
		if( pDC->IsKindOf(Class) ) {

			pDC->AssertValid();

			return *pDC;
			}

		AfxAssert(Class->IsKindOf(AfxPointerClass(pDC)));

		AfxAssert(afxMap->IsTemporary(hDC, NS_HDC));

		AfxTrace(L"WARNING: Duplicate temporary object required\n");
		}

	pDC = AfxNewObject(CDC, Class);
	
	afxMap->InsertTemp(hDC, NS_HDC, pDC);

	pDC->m_hDC = hDC;
	
	return *pDC;
	}

// Overridables

void CDC::DestroyObject(void)
{
	}

// Exceptions

void CDC::CheckException(void)
{
	if( !m_hDC ) AfxThrowResourceException();
	}

// Implementation

int CDC::Length(PCTXT pString, int nLength) const
{
	int nActual = wstrlen(pString);

	return nLength < 0 ? nActual : Min(nActual, nLength);
	}

// End of File
