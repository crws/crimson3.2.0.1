
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Abstract Command Source
//

// Runtime Class

AfxImplementRuntimeClass(CCmdSource, CObject);
		
// Overridables

void CCmdSource::PrepareSource(void)
{
	EnableItem(FALSE);
	
	CheckItem (FALSE);
	}

void CCmdSource::EnableItem(BOOL fEnable)
{
	}

void CCmdSource::CheckItem(BOOL fCheck)
{
	}

void CCmdSource::SetItemText(PCTXT pText)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Command Source with Data
//

// Runtime Class

AfxImplementRuntimeClass(CCmdSourceData, CCmdSource);
		
// Constructor

CCmdSourceData::CCmdSourceData(void)
{
	m_uFlags = 0;

	PrepareSource();
	}
		
// Attributes

UINT CCmdSourceData::GetFlags(void) const
{
	return m_uFlags;
	}
	
PCTXT CCmdSourceData::GetText(void) const
{
	return m_String;
	}

// Overridables

void CCmdSourceData::PrepareSource(void)
{
	EnableItem(FALSE);
	
	CheckItem (FALSE);

	m_String = CString(IDS_SRC_NONE);
	}

void CCmdSourceData::EnableItem(BOOL fEnable)
{
	if( !fEnable ) {

		m_uFlags |=  (MF_GRAYED | MF_DISABLED);
		}
	else
		m_uFlags &= ~(MF_GRAYED | MF_DISABLED);
	}

void CCmdSourceData::CheckItem(BOOL fCheck)
{
	if( fCheck ) {

		m_uFlags |=  MF_CHECKED;
		}
	else
		m_uFlags &= ~MF_CHECKED;
	}

void CCmdSourceData::SetItemText(PCTXT pText)
{
	AfxValidateStringPtr(pText);

	m_String = pText;
	}

//////////////////////////////////////////////////////////////////////////
//
// Command Source for Menus
//

// Runtime Class

AfxImplementRuntimeClass(CCmdSourceMenu, CCmdSource);

// Constructor

CCmdSourceMenu::CCmdSourceMenu(CMenu &Menu, UINT uID)
{
	m_pMenu = &Menu;
	
	m_uID   = uID;
	}

// Overridables

void CCmdSourceMenu::EnableItem(BOOL fEnable)
{
	UINT uFlags = MF_BYCOMMAND;
	
	if( !fEnable ) {

		uFlags |=  (MF_GRAYED | MF_DISABLED);
		}
	else
		uFlags &= ~(MF_GRAYED | MF_DISABLED);
	
	m_pMenu->EnableMenuItem(m_uID, uFlags);
	}

void CCmdSourceMenu::CheckItem(BOOL fCheck)
{
	UINT uFlags = MF_BYCOMMAND;

	if( fCheck ) {

		uFlags |=  MF_CHECKED;
		}
	else
		uFlags &= ~MF_CHECKED;
	
	m_pMenu->CheckMenuItem(m_uID, uFlags);
	}

void CCmdSourceMenu::SetItemText(PCTXT pText)
{
	AfxValidateStringPtr(pText);

	UINT c = m_pMenu->GetMenuItemCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_pMenu->GetMenuItemID(n) == m_uID ) {

			if( m_pMenu->FreeOwnerDraw(n) ) {

				m_pMenu->ModifyMenu(n, MF_BYPOSITION, m_uID, pText);

				m_pMenu->MakeOwnerDraw(n, FALSE);

				break;
				}

			m_pMenu->ModifyMenu(n, MF_BYPOSITION, m_uID, pText);
			
			break;
			}
		}
	}

// End of File
