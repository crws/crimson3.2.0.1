
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WINMSG_HPP
	
#define	INCLUDE_WINMSG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Message Dispatch Helpers
//

#define	AfxDispatchMessage(n)		On_##n(n)
				
#define	DPM(a, b, c)			AfxDispatchWinMsg(a, b, c)

#define	CTP				CCmdTarget::*

#define	HOBJ				HANDLE

//////////////////////////////////////////////////////////////////////////
//
// Message Dispatch Macros
//

#define On_WM_ACTIVATE(n)		DPM(n,(void(CTP)(UINT,BOOL,CWnd&))&OnActivate,"V-5W6W2w")
#define On_WM_ACTIVATEAPP(n)		DPM(n,(void(CTP)(BOOL,DWORD))&OnActivateApp,"V-1L2L")
#define On_WM_BROADCAST(n)		DPM(n,(void(CTP)(UINT))&OnBroadcast,"V-1L")
#define On_WM_CANCELMODE(n)		DPM(n,(void(CTP)(void))&OnCancelMode,"V-")
#define On_WM_CHAR(n)			DPM(n,(void(CTP)(UINT,DWORD))&OnChar,"V-1L2L")
#define On_WM_CHARTOITEM(n)		DPM(n,(LONG(CTP)(UINT,UINT,CWnd&))&OnCharToItem,"L-5W6W2w")
#define On_WM_CLOSE(n)			DPM(n,(void(CTP)(void))&OnClose,"V-")
#define On_WM_COMMAND(n)		DPM(n,(BOOL(CTP)(UINT,UINT,CWnd&))&OnCommand,"C-5W6W2w")
#define On_WM_COMPAREITEM(n)		DPM(n,(LONG(CTP)(UINT,COMPAREITEMSTRUCT&))&OnCompareItem,"L-1L2V")
#define On_WM_CREATE(n)			DPM(n,(UINT(CTP)(CREATESTRUCT&))&OnCreate,"L-2V")
#define	On_WM_CTLCOLORBTN(n)		DPM(n,(HOBJ(CTP)(CDC&,CWnd&))&OnCtlColorBtn,"L-1d2w")
#define	On_WM_CTLCOLORDLG(n)		DPM(n,(HOBJ(CTP)(CDC&,CWnd&))&OnCtlColorDlg,"L-1d2w")
#define	On_WM_CTLCOLOREDIT(n)		DPM(n,(HOBJ(CTP)(CDC&,CWnd&))&OnCtlColorEdit,"L-1d2w")
#define	On_WM_CTLCOLORMSGBOX(n)		DPM(n,(HOBJ(CTP)(CDC&,CWnd&))&OnCtlColorMsgBox,"L-1d2w")
#define	On_WM_CTLCOLORLISTBOX(n)	DPM(n,(HOBJ(CTP)(CDC&,CWnd&))&OnCtlColorListBox,"L-1d2w")
#define	On_WM_CTLCOLORSCROLLBAR(n)	DPM(n,(HOBJ(CTP)(CDC&,CWnd&))&OnCtlColorScrollBar,"L-1d2w")
#define	On_WM_CTLCOLORSTATIC(n)		DPM(n,(HOBJ(CTP)(CDC&,CWnd&))&OnCtlColorStatic,"L-1d2w")
#define On_WM_DELETEITEM(n)		DPM(n,(void(CTP)(UINT,DELETEITEMSTRUCT&))&OnDeleteItem,"T-1L2V")
#define On_WM_DESTROY(n)		DPM(n,(void(CTP)(void))&OnDestroy,"V-")
#define On_WM_DRAWITEM(n)		DPM(n,(void(CTP)(UINT,DRAWITEMSTRUCT&))&OnDrawItem,"T-1L2V")
#define On_WM_ENABLE(n) 		DPM(n,(void(CTP)(BOOL))&OnEnable,"V-1L")
#define On_WM_ENTERIDLE(n)		DPM(n,(void(CTP)(UINT,CWnd&))&OnEnterIdle,"V-1L2w")
#define On_WM_ERASEBKGND(n)             DPM(n,(BOOL(CTP)(CDC&))&OnEraseBkGnd,"L-1d")
#define On_WM_FOCUSNOTIFY(n)		DPM(n,(void(CTP)(UINT,CWnd&))&OnFocusNotify,"V-1L2w")
#define On_WM_GETDLGCODE(n)		DPM(n,(UINT(CTP)(MSG*))&OnGetDlgCode,"L-2L")
#define On_WM_GETMETRIC(n)		DPM(n,(void(CTP)(UINT,CSize&))&OnGetMetric,"V-1L2V")
#define On_WM_GETMINMAXINFO(n)		DPM(n,(void(CTP)(MINMAXINFO&))&OnGetMinMaxInfo,"V-2V")
#define	On_WM_GETSTATUSBAR(n)		DPM(n,(void(CTP)(CString&))&OnGetStatusBar,"V-1V")
#define On_WM_GOINGIDLE(n)		DPM(n,(void(CTP)(void))&OnGoingIdle,"V-")
#define On_WM_HSCROLL(n)		DPM(n,(void(CTP)(UINT,int,CWnd&))&OnHScroll,"V-5W6W2w")
#define On_WM_ICONERASEBKGND(n) 	DPM(n,(void(CTP)(CDC&))&OnIconEraseBkGnd,"V-1d")
#define On_WM_INITDIALOG(n)		DPM(n,(BOOL(CTP)(CWnd&,DWORD))&OnInitDialog,"L-1w2L")
#define On_WM_INITMENU(n)		DPM(n,(void(CTP)(CMenu&))&OnInitMenu,"V-1m")
#define On_WM_INITMENUPOPUP(n)		DPM(n,(void(CTP)(CMenu&,int,BOOL))&OnInitPopup,"V-1m3W4W")
#define On_WM_KEYDOWN(n)		DPM(n,(void(CTP)(UINT,DWORD))&OnKeyDown,"V-1L2L")
#define On_WM_KEYUP(n)			DPM(n,(void(CTP)(UINT,DWORD))&OnKeyUp,"V-1L2L")
#define On_WM_KILLFOCUS(n)		DPM(n,(void(CTP)(CWnd&))&OnKillFocus,"V-1w")
#define On_WM_LBUTTONDBLCLK(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnLButtonDblClk,"V-1L2P")
#define On_WM_LBUTTONDOWN(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnLButtonDown,"V-1L2P")
#define On_WM_LBUTTONUP(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnLButtonUp,"V-1L2P")
#define On_WM_MBUTTONDBLCLK(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnMButtonDblClk,"V-1L2P")
#define On_WM_MBUTTONDOWN(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnMButtonDown,"V-1L2P")
#define On_WM_MBUTTONUP(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnMButtonUp,"V-1L2P")
#define On_WM_MDIACTIVATE(n)		DPM(n,(void(CTP)(CWnd&,CWnd&))&OnMdiActivate,"V-2w1w")
#define On_WM_MEASUREITEM(n)		DPM(n,(void(CTP)(UINT,MEASUREITEMSTRUCT&))&OnMeasureItem,"T-1L2V")
#define On_WM_MENUCHAR(n)		DPM(n,(LONG(CTP)(char,UINT,CMenu&))&OnMenuChar,"L-5W6W2m")
#define On_WM_MENUSELECT(n)		DPM(n,(void(CTP)(UINT,UINT,CMenu&))&OnMenuSelect,"V-5W6W2m")
#define On_WM_MOUSEACTIVATE(n)		DPM(n,(UINT(CTP)(CWnd&,UINT,UINT))&OnMouseActivate,"L-1w3W4W")
#define On_WM_MOUSEMOVE(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnMouseMove,"V-1W2P")
#define On_WM_MOUSEREPEAT(n)		DPM(n,(void(CTP)(void))&OnMouseRepeat,"V-")
#define	On_WM_MOUSEWHEEL(n)		DPM(n,(void(CTP)(UINT,short,CPoint))&OnMouseWheel,"V-5W6W2P")
#define On_WM_MOVE(n)			DPM(n,(void(CTP)(CPoint))&OnMove,"V-2P")
#define On_WM_NCACTIVATE(n)		DPM(n,(BOOL(CTP)(BOOL))&OnNCActivate,"L-1L")
#define On_WM_NCCALCSIZE(n)		DPM(n,(UINT(CTP)(BOOL,NCCALCSIZE_PARAMS&))&OnNCCalcSize,"L-1L2V")
#define On_WM_NCHITTEST(n)		DPM(n,(UINT(CTP)(CPoint))&OnNCHitTest,"L-2P")
#define On_WM_NCLBUTTONDBLCLK(n)	DPM(n,(void(CTP)(UINT,CPoint))&OnNCLButtonDblClk,"V-1L2P")
#define On_WM_NCLBUTTONDOWN(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnNCLButtonDown,"V-1L2P")
#define On_WM_NCLBUTTONUP(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnNCLButtonUp,"V-1L2P")
#define On_WM_NCMOUSEMOVE(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnNCMouseMove,"V-1L2P")
#define On_WM_NCPAINT(n)		DPM(n,(void(CTP)(void))&OnNCPaint,"V-")
#define	On_WM_NOTIFY(n)			DPM(n,(BOOL(CTP)(UINT,NMHDR&))&OnNotify,"C-5L2V")
#define On_WM_PAINT(n)			DPM(n,(void(CTP)(void))&OnPaint,"V-")
#define On_WM_PARENTNOTIFY(n)		DPM(n,(void(CTP)(UINT,UINT,UINT))&OnParentNotify,"V-5W6W2W")
#define On_WM_POSTCREATE(n)		DPM(n,(void(CTP)(void))&OnPostCreate,"V-")
#define	On_WM_PREDESTROY(n)		DPM(n,(void(CTP)(void))&OnPreDestroy,"V-")
#define	On_WM_PRINT(n)			DPM(n,(void(CTP)(CDC&,UINT))&OnPrint,"V-1d2L")
#define	On_WM_PRINTCLIENT(n)		DPM(n,(void(CTP)(CDC&,UINT))&OnPrintClient,"V-1d2L")
#define On_WM_QUERYDRAGICON(n)		DPM(n,(HOBJ(CTP)(void))&OnQueryDragIcon,"L-")
#define On_WM_QUERYENDSESSION(n)	DPM(n,(BOOL(CTP)(void))&OnQueryEndSession,"L-")
#define On_WM_QUERYOPEN(n)		DPM(n,(BOOL(CTP)(void))&OnQueryOpen,"L-")
#define On_WM_RBUTTONDBLCLK(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnRButtonDblClk,"V-1W2P")
#define On_WM_RBUTTONDOWN(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnRButtonDown,"V-1W2P")
#define On_WM_RBUTTONUP(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnRButtonUp,"V-1W2P")
#define On_WM_SETCURSOR(n)		DPM(n,(BOOL(CTP)(CWnd&,UINT,UINT))&OnSetCursor,"L-1w3W4W")
#define On_WM_SETFOCUS(n)		DPM(n,(void(CTP)(CWnd&))&OnSetFocus,"V-1w")
#define	On_WM_SETSTATUSBAR(n)		DPM(n,(void(CTP)(PCTXT))&OnSetStatusBar,"V-1V")
#define On_WM_SETTEXT(n)		DPM(n,(void(CTP)(PCTXT))&OnSetText,"V-2L")
#define On_WM_SHOWWINDOW(n)		DPM(n,(void(CTP)(BOOL,UINT))&OnShowWindow,"V-1L2L")
#define On_WM_SIZE(n)			DPM(n,(void(CTP)(UINT,CSize))&OnSize,"V-1L2P")
#define On_WM_SUBCLASSED(n)		DPM(n,(void(CTP)(void))&OnSubClassed,"V-")
#define On_WM_SYSCHAR(n)		DPM(n,(void(CTP)(UINT,DWORD))&OnSysChar,"V-1L2L")
#define On_WM_SYSCOLORCHANGE(n) 	DPM(n,(void(CTP)(void))&OnSysColorChange,"V-")
#define On_WM_SYSCOMMAND(n)		DPM(n,(void(CTP)(UINT,CPoint))&OnSysCommand,"V-1L2P")
#define On_WM_SYSKEYDOWN(n)		DPM(n,(void(CTP)(UINT,DWORD))&OnSysKeyDown,"V-1L2L")
#define On_WM_SYSKEYUP(n)		DPM(n,(void(CTP)(UINT,DWORD))&OnSysKeyUp,"V-1L2L")
#define On_WM_TIMECHANGE(n)		DPM(n,(void(CTP)(void))&OnTimeChange,"V-")
#define On_WM_TIMER(n)			DPM(n,(void(CTP)(UINT,TIMERPROC*))&OnTimer,"V-1L2V")
#define On_WM_VKEYTOITEM(n)		DPM(n,(LONG(CTP)(UINT,UINT,CWnd&))&OnVKeyToItem,"L-5W6W2w")
#define On_WM_VSCROLL(n)		DPM(n,(void(CTP)(UINT,int,CWnd&))&OnVScroll,"V-5W6W2w")
#define On_WM_WINDOWPOSCHANGED(n)	DPM(n,(void(CTP)(WINDOWPOS&))&OnWindowPosChanged,"V-2V")
#define On_WM_WINDOWPOSCHANGING(n)	DPM(n,(void(CTP)(WINDOWPOS&))&OnWindowPosChanging,"V-2V")
#define On_WM_DEVICECHANGE(n)		DPM(n,(void(CTP)(LONG,LONG))&OnDeviceChange,"T-1L2L")

// End of File

#endif
