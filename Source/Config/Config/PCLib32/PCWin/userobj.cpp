
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic User Object
//

// Dynamic Class

AfxImplementDynamicClass(CUserObject, CHandle);

// Constructor

CUserObject::CUserObject(void)
{
	}

CUserObject::CUserObject(CUserObject const &Src)
{
	AfxAssert(FALSE);
	}

// Exceptions

void CUserObject::CheckException(BOOL fCheck)
{
	if( !fCheck ) {
		
		AfxThrowResourceException();
		}
	}

// End of File
