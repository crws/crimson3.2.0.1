
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Command Target Base Class
//

// Runtime Class

AfxImplementRuntimeClass(CCmdTarget, CObject);

// Attributes

BOOL CCmdTarget::HasMessageMap(void) const
{
	return AfxObjectMessageMap() ? TRUE : FALSE;
	}

// Routing Functions

BOOL CCmdTarget::RouteMessage(MSG const &Msg, LRESULT &lResult)
{
	afxMap->Lock();
	
	BOOL fResult = OnRouteMessage(Msg, lResult);

	afxMap->Unlock();
	
	return fResult;
	}

BOOL CCmdTarget::LocalMessage(MSG const &Msg, LRESULT &lResult)
{
	afxMap->Lock();
	
	BOOL fResult = SearchMessageMap(Msg, lResult);
	
	afxMap->Unlock();

	return fResult;
	}

// Routing Helpers

BOOL CCmdTarget::RouteCommand(UINT uID, LPARAM lParam)
{
	MSG Message = { NULL, WM_AFX_COMMAND, uID, lParam };

	LRESULT lResult = 0;

	return RouteMessage(Message, lResult);
	}

BOOL CCmdTarget::RouteControl(UINT uID, CCmdSource &Source)
{
	MSG Message = { NULL, WM_AFX_CONTROL, uID, LPARAM(&Source) };

	LRESULT lResult = 0;

	return RouteMessage(Message, lResult);
	}

BOOL CCmdTarget::RouteGetInfo(UINT uID, CCmdInfo &Info)
{
	MSG Message = { NULL, WM_AFX_GETINFO, uID, LPARAM(&Info) };

	LRESULT lResult = 0;

	return RouteMessage(Message, lResult);
	}

BOOL CCmdTarget::RouteAccelerator(MSG &Msg)
{
	MSG Message = { NULL, WM_AFX_ACCEL, 0, LPARAM(&Msg) };

	LRESULT lResult = 0;

	return RouteMessage(Message, lResult);
	}

// Mapping Stubs

CMessageMap const * CCmdTarget::AfxObjectMessageMap(void) const
{
	return NULL;
	}

CMessageMap const * CCmdTarget::AfxStaticMessageMap(void)
{
	return NULL;
	}

// Routing Control

BOOL CCmdTarget::OnRouteMessage(MSG const &Msg, LRESULT &lResult)
{
	return SearchMessageMap(Msg, lResult);
	}

// Message Location

BOOL CCmdTarget::SearchMessageMap(MSG const &Msg, LRESULT &lResult)
{
	CMessageMap const *pMap = AfxObjectMessageMap();

	while( pMap ) {

		for( int nPos = 0; pMap->pItem[nPos].uMessage; nPos++ ) {

			CMessageItem const &Item = pMap->pItem[nPos];

			if( Item.uMessage == Msg.message ) {

				if( Item.uMinID ) {

					UINT uID = ExtractTargetID(Msg);

					if( uID < Item.uMinID ) {

						continue;
						}
				
					if( uID > Item.uMaxID ) {

						continue;
						}
					}

				if( Item.uNotify ) {

					UINT uCode = ExtractNotifyCode(Msg);

					if( Item.uNotify < uCode ) {

						continue;
						}

					if( Item.uNotify > uCode ) {

						continue;
						}
					}

				lResult = CallHandler(Item, Msg);

				if( Item.Sig[0] == 'Z' ) {

					return FALSE;
					}

				if( Item.Sig[0] == 'C' ) {

					if( !lResult ) {

						if( Item.uMinID ) {

							if( Item.uMinID == Item.uMaxID ) {

								break;
								}
							}

						continue;
						}
					}

				return TRUE;
				}
			}
			
		AfxAssert(pMap != pMap->pBase);

		pMap = pMap->pBase;
		}

	return FALSE;
	}

UINT CCmdTarget::ExtractTargetID(MSG const &Msg)
{
	switch( Msg.message ) {

		case WM_HSCROLL:
		case WM_VSCROLL:

			if( HIWORD(Msg.lParam) ) {

				HWND hWnd = HWND(Msg.lParam);

				return GetWindowLong(hWnd, GWL_ID);
				}

			return NOTHING;
		}

	return LOWORD(Msg.wParam);
	}

UINT CCmdTarget::ExtractNotifyCode(MSG const &Msg)
{
	switch( Msg.message ) {

		case WM_COMMAND:

			return HIWORD(Msg.wParam);

		case WM_AFX_COMMAND:
			
			return HIWORD(Msg.wParam);

		case WM_NOTIFY:
			
			return ((NMHDR *) Msg.lParam)->code;

		default:
			AfxAssert(FALSE);
		}

	return NOTHING;
	}

// Call Implementation

LRESULT CCmdTarget::CallHandler(CMessageItem const &Item, MSG const &Msg)
{
	MSG const *pSaved;
	
	UINT uStack[6] = { 0, 0, 0, 0, 0, 0 };
	
	UINT uPtr = 0;
	
	UINT uPos = 2;

	while( Item.Sig[uPos] ) {
	
		UINT Data = 0;
		
		switch( Item.Sig[uPos++] ) {
		
			case '1':	Data = Msg.wParam;		break;
			case '2':	Data = Msg.lParam;		break;
			case '3':	Data = LOWORD(Msg.lParam);	break;
			case '4':	Data = HIWORD(Msg.lParam);	break;
			case '5':	Data = LOWORD(Msg.wParam);	break;
			case '6':	Data = HIWORD(Msg.wParam);	break;
				
			default:	AfxAssert(FALSE);
			
			}
			
		if( Item.Sig[uPos] == 'P' ) {

			uStack[uPtr++] = DWORD(long(short(LOWORD(Data))));

			uStack[uPtr++] = DWORD(long(short(HIWORD(Data))));

			uPos++;
			}
		else {
			switch( Item.Sig[uPos] ) {
			
				case 'd':
					Data = DWORD(&CDC::FromHandle(HANDLE(Data)));
					break;

				case 'm':
					Data = DWORD(&CMenu::FromHandle(HANDLE(Data)));
					break;

				case 'w':
					Data = DWORD(&CWnd::FromHandle(HANDLE(Data)));
					break;
				}
		
			uStack[uPtr++] = Data;

			uPos++;
			}

		AfxAssert(uPtr <= elements(uStack));
		}

	pSaved = m_pMsg;

	m_pMsg = &Msg;

	LRESULT rc = MakeCall(Item.pfnProc, uStack, uPtr);

	m_pMsg = pSaved;
	
	switch( Item.Sig[0] ) {
	
		case 'T':	return 1L;
		case 'F':	return 0L;
		case 'V':	return 0L;
		case 'Z':	return 0L;
		case 'L':	return rc;
		case 'C':	return rc;
			
		default:	AfxAssert(FALSE);

		}
		
	return 0L;
	}

LRESULT CCmdTarget::MakeCall(MSGP pfnProc, UINT const *pStack, UINT uCount)
{
	LRESULT rc = 0;

	try {
		UINT S1 = 0;

		UINT S2 = 0;
		
		__asm mov S1, esp

		while( uCount-- ) {

			UINT uData = pStack[uCount];

			__asm {
				mov	eax, uData
				push	eax
				}
			}

		__asm {
			mov	ecx, this
			mov	edx, pfnProc
			call	edx
			mov	rc, eax
			mov	S2, esp
			mov	esp, S1
			}

		AfxAssert(S1 == S2);
		}

	catch(CException &) {

		AfxTrace(L"ERROR: Exception in message handler\n");

		throw;
		}

	return rc;
	}

// End of File
