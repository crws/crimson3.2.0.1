
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GDI Font Object
//

// Dynamic Class

AfxImplementDynamicClass(CFont, CGdiObject);

// Constructors

CFont::CFont(void)
{
	}

CFont::CFont(CFont const &That)
{
	AfxAssert(FALSE);
	}

CFont::CFont(LOGFONT const *pLogFont)
{
	CheckException(Create(pLogFont));
	}

CFont::CFont(LOGFONT const &LogFont)
{
	CheckException(Create(LogFont));
	}

CFont::CFont(PCTXT pFace, CSize const &Size, UINT uType, int nRotate)
{
	CheckException(Create(pFace, Size, uType, nRotate));
	}

CFont::CFont(PCTXT pFace, CSize const &Size, UINT uType)
{
	CheckException(Create(pFace, Size, uType));
	}

CFont::CFont(CDC &DC, PCTXT pFace, int nPoint, UINT uType, int nRotate)
{
	CheckException(Create(DC, pFace, nPoint, uType, nRotate));
	}

CFont::CFont(CDC &DC, PCTXT pFace, int nPoint, UINT uType)
{
	CheckException(Create(DC, pFace, nPoint, uType));
	}

CFont::CFont(int nStock)
{
	CheckException(CreateStockObject(nStock));
	}

// Creation

BOOL CFont::Create(LOGFONT const *pLogFont)
{
	Detach(TRUE);

	HANDLE hFont = CreateFontIndirect(pLogFont);

	return Attach(hFont);
	}

BOOL CFont::Create(LOGFONT const &LogFont)
{
	Detach(TRUE);

	HANDLE hFont = CreateFontIndirect(&LogFont);

	return Attach(hFont);
	}

BOOL CFont::Create(PCTXT pFace, CSize const &Size, UINT uType, int nRotate)
{
	AfxValidateStringPtr(pFace);

	LOGFONT LogFont;

	BOOL fBold = (uType & Bold);

	BOOL fJags = (uType & NoSmooth);

	BOOL fLean = (uType & Italic);

	LogFont.lfHeight	 = Size.cy;
	LogFont.lfWidth		 = Size.cx;
	LogFont.lfEscapement	 = nRotate;
	LogFont.lfOrientation	 = nRotate;
	LogFont.lfWeight	 = UINT(fBold ? FW_BOLD : FW_NORMAL);
	LogFont.lfItalic	 = BYTE(fLean ? TRUE : FALSE);
	LogFont.lfUnderline	 = FALSE;
	LogFont.lfStrikeOut	 = FALSE;
	LogFont.lfCharSet	 = DEFAULT_CHARSET;
	LogFont.lfOutPrecision	 = OUT_DEFAULT_PRECIS;
	LogFont.lfClipPrecision	 = CLIP_DEFAULT_PRECIS;
	LogFont.lfQuality	 = BYTE(fJags ? NONANTIALIASED_QUALITY : DEFAULT_QUALITY);
	LogFont.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	
	wstrcpy(LogFont.lfFaceName, pFace);

	return Attach(CreateFontIndirect(&LogFont));
	}

BOOL CFont::Create(PCTXT pFace, CSize const &Size, UINT uType)
{
	return Create(pFace, Size, uType, 0);
	}

BOOL CFont::Create(CDC &DC, PCTXT pFace, int nPoint, UINT uType, int nRotate)
{
	AfxValidateObject(DC);

	AfxValidateStringPtr(pFace);

	LOGFONT LogFont;

	BOOL fBold = (uType & Bold);

	BOOL fJags = (uType & NoSmooth);

	BOOL fLean = (uType & Italic);

	nPoint = MulDiv(nPoint, GetDeviceCaps(DC, LOGPIXELSY), 72);
	
	LogFont.lfHeight	 = -nPoint;
	LogFont.lfWidth		 = 0;
	LogFont.lfEscapement	 = nRotate;
	LogFont.lfOrientation	 = nRotate;
	LogFont.lfWeight	 = UINT(fBold ? FW_BOLD : FW_NORMAL);
	LogFont.lfItalic	 = BYTE(fLean ? TRUE : FALSE);
	LogFont.lfUnderline	 = FALSE;
	LogFont.lfStrikeOut	 = FALSE;
	LogFont.lfCharSet	 = DEFAULT_CHARSET;
	LogFont.lfOutPrecision	 = OUT_DEFAULT_PRECIS;
	LogFont.lfClipPrecision	 = CLIP_DEFAULT_PRECIS;
	LogFont.lfQuality	 = BYTE(fJags ? NONANTIALIASED_QUALITY : DEFAULT_QUALITY);
	LogFont.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	
	wstrcpy(LogFont.lfFaceName, pFace);

	return Attach(CreateFontIndirect(&LogFont));
	}

BOOL CFont::Create(CDC &DC, PCTXT pFace, int nPoint, UINT uType)
{
	return Create(DC, pFace, nPoint, uType, 0);
	}

// Handle Lookup

CFont & CFont::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CFont NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CFont &) CGdiObject::FromHandle(hObject, Class);
	}

// End of File
