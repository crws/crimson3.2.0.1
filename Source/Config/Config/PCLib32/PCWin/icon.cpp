
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Icon Object
//

// Dynamic Class

AfxImplementDynamicClass(CIcon, CUserObject);

// Constructors

CIcon::CIcon(void)
{
	}
	
CIcon::CIcon(CIcon const &That)
{
	AfxAssert(FALSE);
	}

CIcon::CIcon(ENTITY ID, int xSize, int ySize)
{
	CheckException(Create(ID, xSize, ySize));

	SetExtern(ID.IsStock());
	}

CIcon::CIcon(ENTITY ID, CSize const &Size)
{
	CheckException(Create(ID, Size));

	SetExtern(ID.IsStock());
	}

CIcon::CIcon(ENTITY ID)
{
	CheckException(Create(ID));

	SetExtern(ID.IsStock());
	}

// Destructor

CIcon::~CIcon(void)
{       
	Detach(TRUE);
	}

// Creation

BOOL CIcon::Create(ENTITY ID, int xSize, int ySize)
{
	AfxAssert(xSize == ySize);

	Detach(TRUE);

	m_Size = CSize(xSize, ySize);

	HANDLE hObject = CModule::LoadImage(ID, IMAGE_ICON, xSize, 0);

	return Attach(hObject);
	}

BOOL CIcon::Create(ENTITY ID, CSize const &Size)
{
	AfxAssert(Size.cx == Size.cy);

	Detach(TRUE);

	m_Size = Size;

	HANDLE hObject = CModule::LoadImage(ID, IMAGE_ICON, Size.cx, 0);

	return Attach(hObject);
	}

BOOL CIcon::Create(ENTITY ID)
{
	Detach(TRUE);

	HANDLE hObject = CModule::LoadIcon(ID);

	return Attach(hObject);
	}

// Attributes

CSize CIcon::GetSize(void) const
{
	return m_Size;
	}
	
// Handle Lookup

CIcon & CIcon::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CIcon NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CIcon &) CHandle::FromHandle(hObject, NS_HCURSOR, Class);
	}

// Handle Space

WORD CIcon::GetHandleSpace(void) const
{
	return NS_HCURSOR;
	}

// Destruction

void CIcon::DestroyObject(void)
{
	DestroyIcon(m_hObject);
	}

// End of File
