
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Default Message Box Procedure
//

UINT DefMsgBox(HWND hWnd, PCTXT pText, PCTXT pTitle, UINT uFlags, PCTXT pDoNot)
{
	return MessageBox(hWnd, pText, pTitle, uFlags);
	}

//////////////////////////////////////////////////////////////////////////
//
// Base Window Class
//

// Message Box Functions

UINT CWnd::Error(PCTXT pText, PCTXT pDoNot)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONEXCLAMATION | MB_OK;

	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, pDoNot);
	
	ShowError(pText);
	
	return uCode;
	}

UINT CWnd::Error(PCSTR pText, PCTXT pDoNot)
{
	return Error(CString(pText), pDoNot);
	}

UINT CWnd::Error(ENTITY ID, PCTXT pDoNot)
{
	return Error(CString(ID), pDoNot);
	}

UINT CWnd::Information(PCTXT pText, PCTXT pDoNot)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONINFORMATION | MB_OK;

	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, pDoNot);
	
	return uCode;
	}

UINT CWnd::Information(PCSTR pText, PCTXT pDoNot)
{
	return Information(CString(pText), pDoNot);
	}

UINT CWnd::Information(ENTITY ID, PCTXT pDoNot)
{
	return Information(CString(ID), pDoNot);
	}

UINT CWnd::OkCancel(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONEXCLAMATION | MB_OKCANCEL;
	
	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, NULL);
	
	return uCode;
	}

UINT CWnd::OkCancel(PCSTR pText)
{
	return OkCancel(CString(pText));
	}

UINT CWnd::OkCancel(ENTITY ID)
{
	return OkCancel(CString(ID));
	}

UINT CWnd::YesNoCancel(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONQUESTION | MB_YESNOCANCEL;
	
	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, NULL);
	
	return uCode;
	}

UINT CWnd::YesNoCancel(PCSTR pText)
{
	return YesNoCancel(CString(pText));
	}

UINT CWnd::YesNoCancel(ENTITY ID)
{
	return YesNoCancel(CString(ID));
	}

UINT CWnd::YesNoAllCancel(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONQUESTION | MB_YESNOALLCANCEL;
	
	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, NULL);
	
	return uCode;
	}

UINT CWnd::YesNoAllCancel(PCSTR pText)
{
	return YesNoAllCancel(CString(pText));
	}

UINT CWnd::YesNoAllCancel(ENTITY ID)
{
	return YesNoAllCancel(CString(ID));
	}

UINT CWnd::YesNo(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONQUESTION | MB_YESNO;
	
	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, NULL);
	
	return uCode;
	}

UINT CWnd::YesNo(PCSTR pText)
{
	return YesNo(CString(pText));
	}

UINT CWnd::YesNo(ENTITY ID)
{
	return YesNo(CString(ID));
	}

UINT CWnd::YesNoAll(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONQUESTION | MB_YESNOALL;
	
	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, NULL);
	
	return uCode;
	}

UINT CWnd::YesNoAll(PCSTR pText)
{
	return YesNoAll(CString(pText));
	}

UINT CWnd::YesNoAll(ENTITY ID)
{
	return YesNoAll(CString(ID));
	}

UINT CWnd::NoYes(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	UINT uFlags = MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2;
	
	UINT uCode  = (*m_pMsgBox)(m_hWnd, pText, NULL, uFlags, NULL);
	
	return uCode;
	}
	
UINT CWnd::NoYes(PCSTR pText)
{
	return YesNoAll(CString(pText));
	}

UINT CWnd::NoYes(ENTITY ID)
{
	return NoYes(CString(ID));
	}

// File Overwrite Check

UINT CWnd::CanOverwrite(CFilename const &Name)
{
	if( Name.Exists() ) {

		CFormat Text( CString(IDS_FILE_NAMED_FMT) +
			      CString(IDS_DO_YOU_WANT_TO) ,
			      Name.GetBareName()
			      );

		return YesNoCancel(Text);
		}

	return IDYES;
	}

// Implementation

BOOL CWnd::ShowError(PCTXT pText)
{
	if( FALSE ) {

		if( afxThread ) {

			CString Status;
			
			Status += CString(IDS_MSGBOX_LAST);
			
			Status += pText;
			
			for( UINT uPos; (uPos = Status.Find('\n')) < NOTHING; ) {
			
				CString T1 = Status.Left(uPos);
				
				CString T2 = Status.Mid(uPos + 1);
			
				Status = T1 + L" " + T2;
				}
			
			afxThread->SetStatusText(Status);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
