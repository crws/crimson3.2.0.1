
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Accelerator Object
//

// Dynamic Class

AfxImplementDynamicClass(CAccelerator, CUserObject);

// Constructors

CAccelerator::CAccelerator(void)
{
	}
	
CAccelerator::CAccelerator(CAccelerator const &That)
{
	AfxAssert(FALSE);
	}

CAccelerator::CAccelerator(ENTITY ID)
{
	CheckException(Create(ID));
	}

// Destructor

CAccelerator::~CAccelerator(void)
{       
	Detach(TRUE);
	}

// Creation

BOOL CAccelerator::Create(ENTITY ID)
{
	Detach(TRUE);

	HANDLE hObject = CModule::LoadAccelerator(ID);

	return Attach(hObject);
	}

// Operations

BOOL CAccelerator::Translate(HWND hWnd, MSG &Msg)
{
	if( TranslateAccelerator(hWnd, m_hObject, &Msg) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CAccelerator::Translate(MSG &Msg)
{
	HWND hWnd = GetActiveWindow();

	if( TranslateAccelerator(hWnd, m_hObject, &Msg) ) {

		return TRUE;
		}

	return FALSE;
	}

// Handle Lookup

CAccelerator & CAccelerator::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CAccelerator NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CAccelerator &) CHandle::FromHandle(hObject, NS_HACCEL, Class);
	}
	
// Handle Space

WORD CAccelerator::GetHandleSpace(void) const
{
	return NS_HACCEL;
	}

// Destruction

void CAccelerator::DestroyObject(void)
{
	}

// End of File
