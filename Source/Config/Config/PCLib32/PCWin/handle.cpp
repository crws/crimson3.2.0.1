
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Handle Wrapper
//

// Dynamic Class

AfxImplementDynamicClass(CHandle, CObject);

// Constructors

CHandle::CHandle(void)
{
	m_hObject = NULL;

	m_fExtern = TRUE;
	}

// Destructor

CHandle::~CHandle(void)
{
	}

// Attachment

BOOL CHandle::Attach(HANDLE hObject)
{
	if( hObject ) {
	
		Detach(TRUE);

		m_hObject = hObject;

		m_fExtern = FALSE;

		afxMap->InsertPerm(hObject, GetHandleSpace(), this);

		return TRUE;
		}

	return FALSE;
	}

void CHandle::Detach(BOOL fDelete)
{
	if( m_hObject ) {

		if( fDelete && !m_fExtern ) {

			DestroyObject();
			}

		afxMap->RemoveBoth(m_hObject, GetHandleSpace(), this);

		m_hObject = NULL;

		m_fExtern = TRUE;
		}
	}

// Attributes

HANDLE CHandle::GetHandle(void) const
{
	return m_hObject;
	}

BOOL CHandle::IsValid(void) const
{
	return m_hObject ? TRUE : FALSE;
	}

BOOL CHandle::IsNull(void) const
{
	return m_hObject ? FALSE : TRUE;
	}

BOOL CHandle::operator ! (void) const
{
	return m_hObject ? FALSE : TRUE;
	}

// Conversion

CHandle::operator HANDLE (void) const
{
	return m_hObject;
	}

// Operations

void CHandle::SetExtern(BOOL fExtern)
{
	if( m_hObject ) {
		
		m_fExtern = fExtern;
		}
	}

// Handle Lookup

CHandle & CHandle::FromHandle(HANDLE hObject, UINT uSpace, CLASS Class)
{
	if( !hObject ) {
	
		static CHandle NullObject;
		
		return NullObject;
		}

	CHandle *pObject = (CHandle *) afxMap->FromHandle(hObject, uSpace);

	if( pObject ) {
	
		if( pObject->IsKindOf(Class) ) {

			pObject->AssertValid();

			return *pObject;
			}

		AfxAssert(Class->IsKindOf(AfxPointerClass(pObject)));

		AfxAssert(afxMap->IsTemporary(hObject, uSpace));

		AfxTrace(L"WARNING: Duplicate temporary object required\n");
		}

	pObject = AfxNewObject(CHandle, Class);

	afxMap->InsertTemp(hObject, uSpace, pObject);

	pObject->m_hObject = hObject;

	return *pObject;
	}

// Handle Space

WORD CHandle::GetHandleSpace(void) const
{
	return NS_GENERIC;
	}

// Destruction

void CHandle::DestroyObject(void)
{
	}

// End of File
