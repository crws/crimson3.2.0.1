
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// MDI Child Window
//

// Dynamic Class

AfxImplementDynamicClass(CMdiChildWnd, CFrameWnd);

// Constructor

CMdiChildWnd::CMdiChildWnd(void)
{
	m_pView   = NULL;
	m_fZoomed = FALSE;
	m_fIconic = FALSE;
	m_fEatAct = TRUE;
	m_fEatNow = FALSE;
	}
		
// Creation

BOOL CMdiChildWnd::Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, CMdiFrameWnd &Parent)
{
	RegisterClass();

	CPoint Pos(CW_USEDEFAULT, CW_USEDEFAULT);

	CSize Size(CW_USEDEFAULT, CW_USEDEFAULT);

	if( !Rect.IsEmpty() ) {

		Pos  = Rect.GetTopLeft();

		Size = Rect.GetSize();
		}
	
	if( !(dwStyle & ~(WS_VISIBLE | WS_MAXIMIZE)) ) {
	
		dwStyle |= WS_THICKFRAME | WS_CAPTION | WS_SYSMENU;

		dwStyle |= WS_CHILD | WS_MAXIMIZEBOX | WS_MINIMIZEBOX;

		dwStyle |= WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
		}

	MDICREATESTRUCT Info;
	
	Info.szClass = GetDefaultClassName();
	Info.szTitle = pName;
	Info.hOwner  = afxModule->GetAppInstance();
	Info.x	     = Pos.x;
	Info.y	     = Pos.y;
	Info.cx	     = Size.cx;
	Info.cy	     = Size.cy;
	Info.style   = (dwStyle & ~WS_VISIBLE);
	Info.lParam  = NULL;

	if( AfxInstallHook(this) ) {
		
		m_pClient = &Parent.GetClientWnd();

		m_pFrame  = &Parent;

		if( m_pClient->SendMessage(WM_MDICREATE, 0, LPARAM(&Info)) ) {
			
			SendMessage(WM_POSTCREATE);
			
			if( dwStyle & WS_VISIBLE ) {

				m_pClient->ShowChild(ThisObject);
				}
		
			return TRUE;
			}
		}
		
	AfxTrace(L"WARNING: Failed to create MDI child window\n");

	AfxThrowResourceException();

	return FALSE;
	}

// Destruction

void CMdiChildWnd::DestroyWindow(void)
{
	m_pClient->DestroyChild(ThisObject);
	}

// Attributes

BOOL CMdiChildWnd::IsZoomed(void) const
{
	return m_fZoomed;
	}

BOOL CMdiChildWnd::IsIconic(void) const
{
	return m_fIconic;
	}

// Operations

void CMdiChildWnd::MaximizeWindow(void)
{
	m_pClient->MaximizeChild(ThisObject);
	}

void CMdiChildWnd::RestoreWindow(void)
{
	m_pClient->RestoreChild(ThisObject);
	}

void CMdiChildWnd::SelectNextWindow(void)
{
	m_pClient->NextChild(ThisObject);
	}
		
// Client Calculation

CRect CMdiChildWnd::GetClientRect(void) const
{
	return CFrameWnd::GetClientRect();
	}

// Message Procedures

LRESULT CMdiChildWnd::DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( m_pfnSuper ) {

		AfxAssert(IsWindow());

		return CallWindowProc(m_pfnSuper, m_hWnd, uMessage, wParam, lParam);
		}

	return DefMDIChildProc(m_hWnd, uMessage, wParam, lParam);
	}

// Message Map

AfxMessageMap(CMdiChildWnd, CFrameWnd)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_MDIACTIVATE)
	AfxDispatchMessage(WM_MENUSELECT)
	AfxDispatchMessage(WM_MOUSEACTIVATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SIZE)

	AfxMessageEnd(CMdiChildWnd)
	};

// Message Handlers

UINT CMdiChildWnd::OnCreate(CREATESTRUCT &Create)
{
	m_pClient->ChildNotify(childCreate, this);

	return 0;
	}

void CMdiChildWnd::OnDestroy(void)
{
	m_pClient->ChildNotify(childDestroy, this);
	}

void CMdiChildWnd::OnMdiActivate(CWnd &Wnd, CWnd &Prev)
{
	if( Wnd.GetHandle() == m_hWnd ) {
	        
		if( m_fEatAct ) {

			m_fEatNow = TRUE;
			}
		
		m_pClient->ChildNotify(childActivate, this);
		}
	else {
		if( m_pView ) {

			m_pView->SendMessage(WM_CANCELMODE);
			}

		m_pClient->ChildNotify(childDeactivate, this);
		}
	}

void CMdiChildWnd::OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu)
{
	if( uFlags < 0xFFFF ) {
		
		uFlags &= ~MF_SYSMENU;
		}
		
	LPARAM lParam = MAKELPARAM(uFlags, Menu.GetHandle());
		
	afxMainWnd->SendMessage(WM_MENUSELECT, uID, lParam);
	}

UINT CMdiChildWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	if( m_fEatNow ) {
	
		m_fEatNow = FALSE;
	
		if( uHitTest == HTCLIENT ) {
		
			if( m_pView ) {
			
				CPoint Pos = GetCursorPos();
				
				m_pView->ScreenToClient(Pos);
				
				m_pView->SendMessage(WM_CHILDCLICK, uMessage, Pos);
				}
		
			return MA_ACTIVATEANDEAT;
			}
		}
		
	return UINT(AfxCallDefProc());
	}

void CMdiChildWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	m_fEatNow = FALSE;
	}

void CMdiChildWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MINIMIZED ) {

		m_fIconic = TRUE;

		m_pClient->ChildNotify(childZoomed, this);
		
		m_pFrame->SetFocus();
		}		

	if( uCode == SIZE_MAXIMIZED ) {
	
		m_fZoomed = TRUE;
			
		m_fIconic = FALSE;
			
		m_pClient->ChildNotify(childZoomed, this);
		}
		
	if( uCode == SIZE_RESTORED ) {
	
		m_fZoomed = FALSE;
			
		m_fIconic = FALSE;
		
		m_pClient->ChildNotify(childZoomed, this);
		}		
		
	CFrameWnd::OnSize(uCode, Size);
	
	AfxCallDefProc();
	}

// End of File
