
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Local Button Flags
//

#define	MF_PRESSED1	0x00020000

#define	MF_PRESSED2	0x00040000

#define	MF_DISABLED2	0x00080000

//////////////////////////////////////////////////////////////////////////
//
// Menu Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CMenuGadget, CButtonGadget);
		
// Constructor

CMenuGadget::CMenuGadget(DWORD Image, CMenu &Menu, PCTXT pText, PCTXT pTip, BOOL fOnOff)
{
	m_uID     = Menu.GetMenuItemID(0);

	m_Image   = Image;
	
	m_Tip     = pTip;
	
	m_uProps  = (tbgShowTip | ((HIBYTE(m_uID) == IDM_HELP) ? tbgRight : 0));

	m_fOnOff  = fOnOff;

	m_Text    = CString(pText) + L"!";

	m_Menu.CreatePopupMenu();
	
	m_Menu.AppendMenu(Menu);

	m_Menu.MakeOwnerDraw(FALSE);
	}

// Destructor

CMenuGadget::~CMenuGadget(void)
{
	m_Menu.FreeOwnerDraw();
	}

// Overridlables

BOOL CMenuGadget::OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip)
{
	if( m_Tip.GetLength() ) {
		
		Tip = m_Tip;
		
		return TRUE;
		}
		
	return FALSE;
	}
		
void CMenuGadget::OnIdle(CWnd *pPrivate)
{
	UINT uFlags = m_uFlags;

	UINT uMask  = MF_DISABLED | MF_CHECKED;

	if( !m_fOnOff ) {

		AdjustFlag(MF_DISABLED, !m_Menu.IsMenuActive());
		}
	else {
		UINT uID = m_Menu.GetMenuItemID(0);

		CCmdSourceData Src;
		
		Src.PrepareSource();
		
		afxMainWnd->RouteControl(uID, Src);

		AdjustFlag(MF_CHECKED, !(Src.GetFlags() & MF_CHECKED));
		}

	if( (m_uFlags ^ uFlags) & uMask ) {

		Update();
		}
	}

void CMenuGadget::DoAction(void)
{
	if( m_uProps & tbgRight ) {

		CPoint Pos = m_Rect.GetBottomRight();
		
		m_pWnd->ClientToScreen(Pos);

		Pos.y -= 1;

		m_Menu.TrackPopupMenu(TPM_RIGHTALIGN | TPM_LEFTBUTTON, Pos, *afxMainWnd);
		}
	else {
		CPoint Pos = m_Rect.GetBottomLeft();
		
		m_pWnd->ClientToScreen(Pos);

		Pos.y -= 1;

		AdjustFlag(MF_GRAYED, TRUE);

		m_Menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, Pos, *afxMainWnd);

		AdjustFlag(MF_GRAYED, FALSE);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Any Menu Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CAnyMenuGadget, CMenuGadget);
		
// Constructor

CAnyMenuGadget::CAnyMenuGadget(DWORD Image, CMenu &Menu, PCTXT pText, PCTXT pTip) :

	CMenuGadget( Image, 
		     Menu, 
		     pText, 
		     pTip, 
		     FALSE
		     )
{
	m_uID = 0;
	}

// Overidables

void CAnyMenuGadget::OnIdle(CWnd *pPrivate)
{
	UINT c = m_Menu.GetMenuItemCount();

	for( UINT n = 0; n < c; n++ ) {

		UINT uID = m_Menu.GetMenuItemID(n);

		if( uID ) {

			CCmdSourceData Src;
			
			Src.PrepareSource();

			CWnd &Wnd = CWnd::GetActiveWindow();
			
			Wnd.RouteControl(uID, Src);

			if( Src.GetFlags() & MF_CHECKED ) {

				SetFlag(MF_CHECKED);

				return;
				}
			}
		}

	ClearFlag(MF_CHECKED);
	}
		
void CAnyMenuGadget::DoAction(void)
{
	CPoint Pos = m_Rect.GetBottomLeft();
	
	m_pWnd->ClientToScreen(Pos);

	Pos.y -= 1;

	AdjustFlag(MF_GRAYED, TRUE);

	UINT uID = m_Menu.TrackPopupMenu( TPM_LEFTALIGN  | 
					  TPM_LEFTBUTTON | 
					  TPM_RETURNCMD, 
					  Pos, 
					  *afxMainWnd
					  );

	AdjustFlag(MF_GRAYED, FALSE);

	if( uID ) {

		CWnd &Wnd = CWnd::GetActiveWindow();

		Wnd.PostMessage(WM_COMMAND, uID);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Select Menu Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CSelectMenuGadget, CMenuGadget);
		
// Constructor

CSelectMenuGadget::CSelectMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip) :

	CMenuGadget( 0, 
		     Menu, 
		     pText, 
		     pTip, 
		     FALSE
		     )
{
	m_uID = 0;
	}

// Overidables

void CSelectMenuGadget::OnIdle(CWnd *pPrivate)
{
	UINT c = m_Menu.GetMenuItemCount();

	for( UINT n = 0; n < c; n++ ) {

		UINT uID = m_Menu.GetMenuItemID(n);

		if( uID ) {

			CCmdSourceData Src;
			
			Src.PrepareSource();

			CWnd &Wnd = CWnd::GetActiveWindow();
			
			Wnd.RouteControl(uID, Src);

			if( Src.GetFlags() & MF_CHECKED ) {

				CopyID(uID, TRUE);

				return;
				}
			}
		}

	UINT uID = m_Menu.GetMenuItemID(c / 2);

	AfxAssert(uID);

	CopyID(uID, FALSE);
	}
		
void CSelectMenuGadget::DoAction(void)
{
	CPoint Pos = m_Rect.GetBottomLeft();
	
	m_pWnd->ClientToScreen(Pos);

	Pos.y -= 1;

	AdjustFlag(MF_GRAYED, TRUE);

	UINT uID = m_Menu.TrackPopupMenu( TPM_LEFTALIGN  | 
					  TPM_LEFTBUTTON | 
					  TPM_RETURNCMD, 
					  Pos, 
					  *afxMainWnd
					  );

	AdjustFlag(MF_GRAYED, FALSE);

	if( uID ) {

		CCmdInfo Info;
		
		afxMainWnd->RouteGetInfo(uID, Info);

		m_uID   = uID;

		m_Image = Info.m_Image;

		CWnd &Wnd = CWnd::GetActiveWindow();

		Wnd.PostMessage(WM_COMMAND, m_uID);
		}
	}

// Implementation

void CSelectMenuGadget::CopyID(UINT uID, BOOL fEnable)
{
	if( m_uID != uID ) {

		CCmdInfo Info;
		
		afxMainWnd->RouteGetInfo(uID, Info);

		m_uID   = uID;

		m_Image = Info.m_Image;

		Update();
		}

	if( fEnable ) {

		ClearFlag(MF_DISABLED);

		return;
		}

	SetFlag(MF_DISABLED);
	}

//////////////////////////////////////////////////////////////////////////
//
// Split Menu Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CSplitMenuGadget, CMenuGadget);
		
// Constructor

CSplitMenuGadget::CSplitMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip, BOOL fOnOff) :

	CMenuGadget( NOTHING, 
		     Menu, 
		     pText, 
		     pTip, 
		     fOnOff
		     )
{
	m_Text = CString(pText);
	}

// Overridables

void CSplitMenuGadget::OnCreate(CDC &DC)
{
	CCmdInfo Info;
	
	afxMainWnd->RouteGetInfo(m_uID, Info);

	m_Image     = Info.m_Image;

	m_LSize     = m_Size;

	m_RSize     = m_Size;

	m_LSize.cx += afxButton->Adjust(DC, m_Image, m_Text);
	
	m_RSize.cx  = afxButton->Adjust(DC, NOTHING, L"!");

	m_Size.cx   = m_LSize.cx + m_RSize.cx - 1;
	}

void CSplitMenuGadget::OnSetPosition(void)
{
	CPoint LPos = m_Rect.GetTopLeft();

	CPoint RPos = LPos;

	RPos.x += m_LSize.cx - 1;

	m_LRect = CRect(LPos, m_LSize);

	m_RRect = CRect(RPos, m_RSize);
	}

void CSplitMenuGadget::OnPaint(CDC &DC)
{
	UINT uFlags1 = m_uFlags;

	UINT uFlags2 = m_uFlags & ~(MF_DISABLED | MF_CHECKED);

	if( TestFlag(MF_PRESSED1) ) {

		uFlags1 |= MF_PRESSED;
		}

	if( TestFlag(MF_PRESSED2) ) {

		uFlags2 |= MF_PRESSED;
		}

	if( TestFlag(MF_DISABLED2) ) {

		uFlags2 |= MF_DISABLED;
		}

	afxButton->Draw( DC, 
			 m_LRect, 
			 m_Image, 
			 m_Text, 
			 uFlags1
			 );

	afxButton->Draw( DC, 
			 m_RRect, 
			 NOTHING, 
			 L"!", 
			 uFlags2
			 );
	}

BOOL CSplitMenuGadget::OnMouseDown(CPoint const &Pos)
{
	UINT uFlags = m_uFlags;
	
	if( m_LRect.PtInRect(Pos) ) {

		if( !TestFlag(MF_DISABLED) ) {
		
			SetFlag(MF_PRESSED1);
			}
		}
		
	if( m_RRect.PtInRect(Pos) ) {

		SetFlag(MF_PRESSED2);
		}		

	if( uFlags ^ m_uFlags ) {

		Update();
		
		return TRUE;
		}
		
	return FALSE;
	}

void CSplitMenuGadget::OnMouseMove(CPoint const &Pos, BOOL fHit)
{
	AdjustFlag(MF_PRESSED1, fHit && m_LRect.PtInRect(Pos));

	AdjustFlag(MF_PRESSED2, fHit && m_RRect.PtInRect(Pos));
	}

void CSplitMenuGadget::OnMouseUp(CPoint const &Pos, BOOL fHit)
{
	if( TestFlag(MF_PRESSED1) ) {
		
		CButtonGadget::DoAction();
		
		ClearFlag(MF_PRESSED | MF_PRESSED1);
		}
		
	if( TestFlag(MF_PRESSED2) ) {
		
		DoAction();
		
		ClearFlag(MF_PRESSED | MF_PRESSED2);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Sticky Menu Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CStickyMenuGadget, CSplitMenuGadget);
		
// Constructor

CStickyMenuGadget::CStickyMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip) :

	CSplitMenuGadget( Menu, 
			  pText, 
			  pTip, 
			  FALSE
			  )
{
	}

// Overidables
		
void CStickyMenuGadget::OnIdle(CWnd *pPrivate)
{
	if( !m_Menu.IsMenuActive() ) {

		m_uFlags |=  MF_DISABLED2;
		}
	else
		m_uFlags &= ~MF_DISABLED2;

	CButtonGadget::OnIdle(pPrivate);
	}

void CStickyMenuGadget::DoAction(void)
{
	CPoint Pos = m_Rect.GetBottomLeft();
	
	m_pWnd->ClientToScreen(Pos);

	Pos.y -= 1;

	AdjustFlag(MF_GRAYED, TRUE);

	UINT uID = m_Menu.TrackPopupMenu( TPM_LEFTALIGN  | 
					  TPM_LEFTBUTTON | 
					  TPM_RETURNCMD, 
					  Pos, 
					  *afxMainWnd
					  );

	AdjustFlag(MF_GRAYED, FALSE);

	if( uID ) {

		CCmdInfo Info;
		
		afxMainWnd->RouteGetInfo(uID, Info);
		
		m_uID   = uID;

		m_Image = Info.m_Image;

		CWnd &Wnd = CWnd::GetActiveWindow();

		Wnd.PostMessage(WM_COMMAND, m_uID);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Toggle Menu Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CToggleMenuGadget, CSplitMenuGadget);
		
// Constructor

CToggleMenuGadget::CToggleMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip) :

	CSplitMenuGadget( Menu, 
			  pText, 
			  pTip, 
			  TRUE
			  )
{
	}

// Overidables
		
void CToggleMenuGadget::OnIdle(CWnd *pPrivate)
{
	CButtonGadget::OnIdle(pPrivate);
	}

void CToggleMenuGadget::DoAction(void)
{
	CPoint Pos = m_Rect.GetBottomLeft();
	
	m_pWnd->ClientToScreen(Pos);

	Pos.y -= 1;

	AdjustFlag(MF_GRAYED, TRUE);

	m_Menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, Pos, *afxMainWnd);

	AdjustFlag(MF_GRAYED, FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Help Menu Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CHelpMenuGadget, CSplitMenuGadget);
		
// Constructor

CHelpMenuGadget::CHelpMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip) :

	CSplitMenuGadget( Menu, 
			  pText, 
			  pTip, 
			  TRUE
			  )
{
	}

// Overidables

void CHelpMenuGadget::OnPaint(CDC &DC)
{
	UINT uFlags = (m_uFlags & ~(MF_DISABLED | MF_CHECKED));

	afxButton->Draw( DC, 
			 m_LRect, 
			 m_Image, 
			 m_Text, 
			 TestFlag(MF_PRESSED1) ? (uFlags | MF_PRESSED) : uFlags
			 );

	afxButton->Draw( DC, 
			 m_RRect, 
			 NOTHING, 
			 L"!", 
			 TestFlag(MF_PRESSED2) ? (uFlags | MF_PRESSED) : uFlags
			 );
	}

BOOL CHelpMenuGadget::OnMouseDown(CPoint const &Pos)
{
	UINT uFlags = m_uFlags;
	
	if( m_LRect.PtInRect(Pos) ) {

		if( !TestFlag(MF_DISABLED) ) {
			
			SetFlag(MF_PRESSED1);
			}
		}
		
	if( m_RRect.PtInRect(Pos) ) {

		SetFlag(MF_PRESSED2);
		}

	if( uFlags ^ m_uFlags ) {

		Update();
		
		return TRUE;
		}
		
	return FALSE;
	}

// Overidables
		
void CHelpMenuGadget::OnIdle(CWnd *pPrivate)
{
	UINT uFlags = m_uFlags;

	UINT uMask  = (MF_DISABLED | MF_CHECKED);

	CCmdSourceData Src;
	
	Src.PrepareSource();
	
	afxMainWnd->RouteControl(m_uID, Src);

	AdjustFlag(MF_CHECKED,   !(Src.GetFlags() & MF_CHECKED));

	AdjustFlag(MF_DISABLED, !!(Src.GetFlags() & MF_DISABLED));

	if( (m_uFlags ^ uFlags) & uMask ) {

		Update();
		}
	}

void CHelpMenuGadget::DoAction(void)
{
	CPoint Pos = m_Rect.GetBottomRight();
	
	m_pWnd->ClientToScreen(Pos);

	Pos.y -= 1;

	AdjustFlag(MF_GRAYED, TRUE);

	m_Menu.TrackPopupMenu(TPM_RIGHTALIGN | TPM_LEFTBUTTON, Pos, *afxMainWnd);
	
	AdjustFlag(MF_GRAYED, FALSE);
	}

// End of File
