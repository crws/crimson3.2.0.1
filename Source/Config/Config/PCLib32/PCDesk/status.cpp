
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Status Bar Window
//

// Dynamic Class

AfxImplementDynamicClass(CStatusWnd, CGadgetBarWnd);

// Constructor

CStatusWnd::CStatusWnd(void)
{
	m_Border.Set(1, 2, 1, 0);
	
	m_fPrompt = FALSE;

	m_uLock   = 0;
	}

// Attributes

BOOL CStatusWnd::InPromptMode(void) const
{
	return m_fPrompt;
	}

// Operations

void CStatusWnd::LockPrompt(BOOL fLock)
{
	fLock ? m_uLock++ : m_uLock--;
	}

void CStatusWnd::SetPrompt(PCTXT pText)
{
	if( !m_uLock ) {

		if( pText ) {
		
			m_PromptText = pText;
			
			m_fPrompt = TRUE;
			
			Invalidate(FALSE);
			}
		else {
			m_fPrompt = FALSE;
			
			Invalidate(FALSE);
			}
		}
	}
		
// Message Map

AfxMessageMap(CStatusWnd, CGadgetBarWnd)
{
	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CStatusWnd)
	};

// Message Handlers

void CStatusWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();
		
	if( m_fPrompt ) {

		DC.GradVert( GetClientRect(),
			     afxColor(Blue3),
			     afxColor(Blue1)
			     );
	
		DC.Select(afxFont(Status));

		DC.SetBkMode(TRANSPARENT);
		
		CSize Char = DC.GetTextExtent(L"X");
		
		CSize Size = CSize(4, (Rect.GetHeight() - Char.cy) / 2);
		
		DC.ExtTextOut( Rect.GetTopLeft() + Size,
			       ETO_CLIPPED,
			       Rect,
			       m_PromptText
			       );
		
		DC.Deselect();
		}
	else {
		DC.GradVert( GetClientRect(),
			     afxColor(Blue3),
			     afxColor(Blue1)
			     );

		PaintGadgets(DC);
		}
	}

// End of File
