
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Horizontal Split View Window
//

// Dynamic Class

AfxImplementDynamicClass(CHorzSplitViewWnd, CSplitViewWnd);

// Constructor

CHorzSplitViewWnd::CHorzSplitViewWnd(void)
{
	m_Cursor.Create(L"HSplitAdjust");
	}

// Message Map

AfxMessageMap(CHorzSplitViewWnd, CSplitViewWnd)
{
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_GETMETRIC)
	AfxDispatchMessage(WM_SIZE)
	
	AfxDispatchControlType(IDM_WINDOW, OnWindowControl)
	AfxDispatchCommandType(IDM_WINDOW, OnWindowCommand)

	AfxMessageEnd(CHorzSplitViewWnd)
	};

// Message Handlers

void CHorzSplitViewWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_fFixed && !m_fCapture ) {
	
		if( m_Rect[2].PtInRect(Pos) ) {
		
			StartAdjust(Pos.x);
			
			return;
			}
		}
	}

void CHorzSplitViewWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( m_fCapture ) {
	
		TrackAdjust(Pos.x);
		
		return;
		}	
	}

void CHorzSplitViewWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_fCapture ) {
	
		EndAdjust();
		
		return;
		}
	}

void CHorzSplitViewWnd::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	if( m_fCapture ) {
	
		int nPos = m_nTrackPos;
	
		switch( uCode ) {
		
			case VK_ESCAPE:
				AbortAdjust();
				break;
				
			case VK_RETURN:
				EndAdjust();
				break;
				
			case VK_LEFT:
				nPos -= 4;
				break;
				
			case VK_RIGHT:
				nPos += 4;
				break;
				
			case VK_PRIOR:
				nPos -= 16;
				break;
				
			case VK_NEXT:
				nPos += 16;
				break;
			}
			
		if( TrackAdjust(nPos) ) {

			CPoint Pos = GetCursorPos();
				
			ScreenToClient(Pos);
		
			Pos.x = m_nTrackPos;

			ClientToScreen(Pos);
			
			SetCursorPos(Pos.x, Pos.y);
			}
		}
	}

void CHorzSplitViewWnd::OnGetMetric(UINT uCode, CSize &Size)
{
	if( uCode == MC_BEST_SIZE ) {

		if( m_fUse[1] ) {
	
			Size = m_pWnd[1]->GetWindowRect().GetSize();
				
			m_pWnd[1]->SendMessage(WM_GETMETRIC, uCode, LPARAM(&Size));
				
			AdjustWindowSize(Size);
				
			Size.cx = GetWindowRect().GetWidth();
			
			return;
			}
		}
	}

void CHorzSplitViewWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_RESTORED || uCode == SIZE_MAXIMIZED ) {
		
		if( m_nTotal != Size.cx ) {
			
			m_nTotal  = Size.cx;
		
			m_nBarPos = MulDiv(m_nTotal, m_nPercent, 1000);
			}
		
		if( m_pWnd[0] && m_pWnd[0]->IsWindow() ) {
		
			CalcWndRects();
		
			MoveWindows();
			}
		}
	}

// Command Handlers

BOOL CHorzSplitViewWnd::OnWindowControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_WINDOW_ADJUST:
			
			Src.EnableItem(!m_fFixed && m_fUse[0] && m_fUse[1]);
			
			break;
			
		default:
			return FALSE;
		}
		
	return TRUE;
	}

BOOL CHorzSplitViewWnd::OnWindowCommand(UINT uID)
{
	if( uID == IDM_WINDOW_ADJUST ) {

		CPoint Pos = GetCursorPos();
			
		ScreenToClient(Pos);
			
		Pos.x = (m_Rect[2].left + m_Rect[2].right) / 2;
			
		MakeMax(Pos.y, m_Rect[2].top);
	
		MakeMin(Pos.y, m_Rect[2].bottom);
			
		StartAdjust(Pos.x);
	
		ClientToScreen(Pos);
			
		SetCursorPos(Pos.x, Pos.y);
	
	        return TRUE;
		}
		
	return FALSE;
	}

// Overridables

void CHorzSplitViewWnd::FindBestSplit(void)
{
	if( m_fUse[1] ) {
		
		CSize Init = m_pWnd[1]->GetWindowRect().GetSize();
		
		CSize Size = Init;
		
		m_pWnd[1]->SendMessage(WM_GETMETRIC, MC_BEST_SIZE, (LPARAM) &Size);
		
		if( Size.cx < Init.cx ) {
			
			m_nBarPos += Init.cx - Size.cx;
			
			CalcWndRects();
			
			MoveWindows();
			}
		}
	}

void CHorzSplitViewWnd::CalcWndRects(void)
{
	if( m_fUse[0] && m_fUse[1] ) {
		
		m_Rect[0] = m_Rect[1] = m_Rect[2] = GetClientRect();
		
		m_Rect[0].right = m_Rect[2].left = m_nBarPos - 2;
		
		m_Rect[2].right = m_Rect[1].left = m_nBarPos + 2;
		}
	else {
		if( m_fUse[0] ) {

			m_Rect[0] = GetClientRect();
			
			m_Rect[1].Set(0, 0, 0, 0);
			
			m_Rect[2].Set(0, 0, 0, 0);
			}
		else {
			m_Rect[0].Set(0, 0, 0, 0);

			m_Rect[1] = GetClientRect();
			
			m_Rect[2].Set(0, 0, 0, 0);
			}
		}
	}

// Implementation

void CHorzSplitViewWnd::StartAdjust(int nPos)
{
	SetCapture();
		
	m_fCapture = TRUE;
			
	SetFocus();
			
	m_nTrackOrg = nPos;
			
	m_nTrackPos = nPos;
			
	ShowTracking();

	SetCursor(m_Cursor);
	}

BOOL CHorzSplitViewWnd::TrackAdjust(int nPos)
{
	MakeMin(nPos, m_Rect[1].right - 4);
		
	MakeMax(nPos, m_Rect[0].left  + 4);

	if( m_nTrackPos != nPos ) {
	
		ShowTracking();
		
		m_nTrackPos = nPos;
		
		ShowTracking();
		
		return TRUE;
		}
		
	return FALSE;
	}

void CHorzSplitViewWnd::EndAdjust(void)
{
	ReleaseCapture();
		
	m_fCapture = FALSE;
		
	ShowTracking();
		
	if( m_nTrackPos != m_nTrackOrg ) {
		
		m_nBarPos += m_nTrackPos - m_nTrackOrg;
		
		m_nPercent = MulDiv(1000, m_nBarPos, m_nTotal);

		CalcWndRects();
				
		MoveWindows();
			
		FindBestSplit();
				
		Invalidate(TRUE);
		}			
		
	m_pWnd[m_uFocus]->SetFocus();
	}
	
void CHorzSplitViewWnd::AbortAdjust(void)
{
	ReleaseCapture();
		
	m_fCapture = FALSE;
		
	ShowTracking();
		
	m_pWnd[m_uFocus]->SetFocus();
	}

void CHorzSplitViewWnd::ShowTracking(void)
{
	CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);
	
	CRect Rect  = m_Rect[2];
	
	Rect.left  += m_nTrackPos - m_nTrackOrg;
	
	Rect.right += m_nTrackPos - m_nTrackOrg;
	
	DC.PatBlt(Rect, DSTINVERT);
	}

// End of File
