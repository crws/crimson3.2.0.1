
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Tabbed View Window
//

// Static Data

UINT CMultiViewWnd::m_timerSelect = AllocTimerID();

// Dynamic Class

AfxImplementDynamicClass(CMultiViewWnd, CViewWnd);

// Constructor

CMultiViewWnd::CMultiViewWnd(void)
{
	m_dwStyle = TCS_BOTTOM;

	m_dwMade  = 0;

	m_nMargin = 2;

	m_pTab    = New CTabCtrl;

	m_uFocus  = 0;

	m_uDrop    = NOTHING;

	m_Accel.Create(L"MultiViewAccel");
	}

// Destructor

CMultiViewWnd::~CMultiViewWnd(void)
{
	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {

		if( !(m_dwMade & (1 << n)) ) {

			delete m_Views[n];
			}
		}
	}

// Attributes

UINT CMultiViewWnd::GetTabIndex(void) const
{
	return m_uFocus;
	}

CString CMultiViewWnd::GetTabLabel(void) const
{
	return m_Label[m_uFocus];
	}

// Operations

void CMultiViewWnd::AddView(PCTXT pLabel, CViewWnd *pView)
{
	m_Label.Append(pLabel);

	m_Views.Append(pView);
	}

void CMultiViewWnd::SetTabStyle(DWORD dwStyle)
{
	m_dwStyle = dwStyle;
	}

void CMultiViewWnd::SetMargin(int nMargin)
{
	m_nMargin = nMargin;
	}

BOOL CMultiViewWnd::SelectTab(UINT uTab)
{
	if( uTab < m_Label.GetCount() ) {

		Select(uTab);

		return TRUE;
		}

	return FALSE;
	}

BOOL CMultiViewWnd::SelectTab(CString Tab)
{
	for( UINT n = 0; n < m_Label.GetCount(); n++ ) {

		if( m_Label[n] == Tab ) {

			Select(n);

			return TRUE;
			}
		}

	return FALSE;
	}
	
// IUnknown

HRESULT CMultiViewWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CMultiViewWnd::AddRef(void)
{
	return 1;
	}

ULONG CMultiViewWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CMultiViewWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	CPoint Pos(pt.x, pt.y);

	SetDrop(Pos);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CMultiViewWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	CPoint Pos(pt.x, pt.y);

	SetDrop(Pos);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CMultiViewWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(NOTHING);

	return S_OK;
	}

HRESULT CMultiViewWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	SetDrop(NOTHING);

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Routing Control

BOOL CMultiViewWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	UINT uRC = GetRoutingCode(Message);

	BYTE bLo = LOBYTE(uRC);

	BYTE bHi = HIBYTE(uRC);

	////////
	
	if( bHi & HIBYTE(RC_LOCAL1) ) {
	
		if( CViewWnd::OnRouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}
		
	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {
	
		if( n == m_uFocus || bLo == RC_FIRST || bLo == RC_ALL ) {

			if( !m_Views[n]->IsWindow() ) {

				continue;
				}
		
			if( m_Views[n]->RouteMessage(Message, lResult) ) {
			
				if( bLo == RC_ALL ) {

					continue;
					}

				if( bHi & HIBYTE(RC_LOCAL2) ) {

					CViewWnd::OnRouteMessage(Message, lResult);
					}
					
				return TRUE;
				}
			}
		}

	return CViewWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CMultiViewWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_DRAWITEM)
	AfxDispatchMessage(WM_GETMINMAXINFO)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_GETMETRIC)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_PRINT)
	
	AfxDispatchNotify(IDTABS, TCN_SELCHANGE, OnSelChange)

	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)

	AfxMessageEnd(CMultiViewWnd)
	};

// Accelerator

BOOL CMultiViewWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CMultiViewWnd::OnPostCreate(void)
{
	if( m_Views.IsEmpty() ) {
		
		AfxTrace(L"WARNING: Default view windows created\n");

		AddView(L"One",   New CDummyViewWnd);

		AddView(L"Two",   New CDummyViewWnd);

		AddView(L"Three", New CDummyViewWnd);

		AddView(L"Four",  New CDummyViewWnd);
		}

	CreateTabCtrl();

	CalcLayout();

	CreateView(m_uFocus);

	m_Views[m_uFocus]->ShowWindow(SW_SHOW);

	m_Views[m_uFocus]->SetWindowOrder(HWND_TOP, TRUE);
	
	m_Views[m_uFocus]->SetCurrent(IsCurrent());
	}

void CMultiViewWnd::OnCancelMode(void)
{
	m_Views[m_uFocus]->SendMessage(WM_CANCELMODE);
	}
	
BOOL CMultiViewWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}
	
void CMultiViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);
	
	CRect Rect = GetClientRect();

	for( int n = 0; n < m_nMargin; n++ ) {
	
		DC.FrameRect(Rect, afxBrush(3dFace));

		Rect--;
		}
	}

void CMultiViewWnd::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw)
{
	if( uID == m_pTab->GetID() ) {

		CDC DC(Draw.hDC);

		DC.Select(afxFont(Dialog));

		CPoint  Pos;

		CString Text = m_pTab->GetItemText(Draw.itemID);

		CRect   Rect = Draw.rcItem;

		CSize   Size = DC.GetTextExtent(Text);

		Pos.x = Rect.left + 3;
		
		Pos.y = Rect.top  + 3;

		if( Draw.itemState & ODS_SELECTED ) {

			Pos.x += 2;
			}
		else
			Rect.bottom += 2;

		DC.SetTextColor(afxColor(ButtonText));

		DC.SetBkMode(TRANSPARENT);

		if( m_uDrop == Draw.itemID ) {

			CRect Rect1 = Rect;
			
			CRect Rect2 = Rect;

			Rect1.bottom = Rect2.top = Rect.top + (8 * Rect.cy() / 10);

			DC.GradVert(Rect1, afxColor(Orange2), afxColor(TabFace));

			DC.FillRect(Rect2, afxBrush(TabFace));
			}
		else
			DC.FillRect(Rect, afxBrush(TabFace));

		DC.TextOut(Pos, Text);

		DC.Deselect();

		return;
		}

	AfxRouteToDefProc();
	}

void CMultiViewWnd::OnGetMinMaxInfo(MINMAXINFO &Info)
{
	CSize MinSize;

	CSize MaxSize;

	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {
	
		CreateView(n);

		CViewWnd *pWnd = m_Views[n];

		MINMAXINFO Work;

		memset(&Work, 0, sizeof(Work));

		pWnd->SendMessage(WM_GETMINMAXINFO, 0, LPARAM(&Work));

		MakeMax(MinSize.cx, Work.ptMinTrackSize.x);
		MakeMax(MinSize.cy, Work.ptMinTrackSize.y);
		MakeMax(MaxSize.cx, Work.ptMaxTrackSize.x);
		MakeMax(MaxSize.cy, Work.ptMaxTrackSize.y);
		}

	AdjustWindowSize(MinSize);
	
	AdjustWindowSize(MaxSize);

	CRect MinRect = CRect(MinSize) + m_nMargin;

	CRect MaxRect = CRect(MaxSize) + m_nMargin;

	m_pTab->AdjustRect(TRUE, MinRect);

	m_pTab->AdjustRect(TRUE, MaxRect);

	Info.ptMinTrackSize = CPoint(MinRect.GetSize());
	
	Info.ptMaxTrackSize = CPoint(MaxRect.GetSize());
	}

void CMultiViewWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_RESTORED || uCode == SIZE_MAXIMIZED ) {
			
		if( m_pTab->IsWindow() ) {

			CalcLayout();

			MoveWindows();
			}
		}
	}

void CMultiViewWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( !m_Views.IsEmpty() ) {

		if( fShow ) {

			m_Views[m_uFocus]->ShowWindow(SW_SHOW);

			return;
			}

		m_Views[m_uFocus]->ShowWindow(SW_HIDE);
		}
	}

void CMultiViewWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_Views.GetCount() && m_Views[m_uFocus] ) {
	
		m_Views[m_uFocus]->SetFocus();
		
		return;
		}
	}

void CMultiViewWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID >= IDVIEW && uID < IDVIEW + m_Views.GetCount() ) {
	
		Select(uID - IDVIEW);

		return;
		}
	}

void CMultiViewWnd::OnGetMetric(UINT uCode, CSize &Size)
{
	if( uCode == MC_BEST_SIZE ) {
	
		CSize Init = Size;
	
		m_Views[0]->SendMessage(WM_GETMETRIC, uCode, LPARAM(&Size));

		CRect Rect = Size;

		m_pTab->AdjustRect(TRUE, Rect);

		Size = (Rect + 2).GetSize();
		
		AdjustWindowSize(Size);
		
		Size.cx = Init.cx;
	
		return;
		}
	}

void CMultiViewWnd::OnSetCurrent(BOOL fCurrent)
{
	m_Views[m_uFocus]->SetCurrent(fCurrent);
	}

// Notification Handlers

void CMultiViewWnd::OnSelChange(UINT uID, NMHDR &Info)
{
	if( m_pTab->HasFocus() ) {

		Select(m_pTab->GetCurSel());

		return;
		}

	SelectFocus(m_pTab->GetCurSel());
	}

void CMultiViewWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerSelect ) {

		if( m_uDrop < NOTHING ) {

			SelectFocus(m_uDrop);
			}

		KillTimer(uID);
		}
	}

void CMultiViewWnd::OnPrint(CDC &DC, UINT uFlags)
{
	// NOTE -- We have to do this as DefWindowProc's version is
	// broken and screws up the z-order such that the tab control
	// blocks the underlying window. We are using a bitmap for
	// each of our controls, which is crazy expensive. Is there
	// not some way of offseting the DC we're given? We can't just
	// use the viewpoint / window functions, though, as the child
	// window might use them and get confused. I guess it really
	// does not matter too much as we have only two children...

	if( !(uFlags & PRF_CHECKVISIBLE) || IsWindowVisible() ) {

		if( uFlags & PRF_ERASEBKGND ) {

			SendMessage( WM_ERASEBKGND,
				     WPARAM(DC.GetHandle())
				     );
			}

		SendMessage( WM_PRINTCLIENT,
			     WPARAM(DC.GetHandle()),
			     LPARAM(uFlags)
			     );
		}

	CWnd *pWnd = GetWindowPtr(GW_CHILD);

	if( pWnd->IsWindow() ) {

		pWnd = pWnd->GetWindowPtr(GW_HWNDLAST);

		CClientDC DispDC(ThisObject);

		while( pWnd->IsWindow() ) {

			if( pWnd->IsWindowVisible() ) {

				CRect Rect = pWnd->GetWindowRect();

				if( !Rect.IsEmpty() ) {

					CMemoryDC WorkDC(DC);

					CBitmap   Bitmap(DispDC, Rect.GetSize());

					WorkDC.Select(Bitmap);

					pWnd->SendMessage( WM_PRINT,
							   WPARAM(WorkDC.GetHandle()),
							   LPARAM(uFlags)
							   );

					ScreenToClient(Rect);

					DC.BitBlt(Rect, WorkDC, CPoint(0, 0), SRCCOPY);

					WorkDC.Deselect();
					}
				}

			pWnd = pWnd->GetWindowPtr(GW_HWNDPREV);
			}
		}
	}

// Command Handlers

BOOL CMultiViewWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_VIEW_TAB_1 && uID < IDM_VIEW_TAB_1 + m_Views.GetCount() ) {

		Src.EnableItem(TRUE);
	
		return TRUE;
		}

	if( uID == IDM_VIEW_TAB_NEXT ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}

	if( uID == IDM_VIEW_TAB_PREV ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CMultiViewWnd::OnViewCommand(UINT uID)
{
	if( uID >= IDM_VIEW_TAB_1 && uID < IDM_VIEW_TAB_1 + m_Views.GetCount() ) {

		SelectFocus(uID - IDM_VIEW_TAB_1);	
	
		return TRUE;
		}
		
	if( uID == IDM_VIEW_TAB_NEXT ) {

		UINT uCount = m_Views.GetCount();

		SelectFocus((m_uFocus + 1) % uCount);

		return TRUE;
		}

	if( uID == IDM_VIEW_TAB_PREV ) {

		UINT uCount = m_Views.GetCount();

		SelectFocus((m_uFocus + uCount - 1) % uCount);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CMultiViewWnd::CreateTabCtrl(void)
{
	m_TabRect = GetClientRect() - m_nMargin;

	DWORD dwTheme = afxStdTools->HasTheme() ? 0 : TCS_OWNERDRAWFIXED;

	DWORD dwStyle = m_dwStyle | WS_CLIPSIBLINGS;

	m_pTab->Create( dwStyle | dwTheme,
			m_TabRect,
			ThisObject,
			IDTABS
			);

	for( UINT n = 0; n < m_Label.GetCount(); n++ ) {

		CTabItem Item(m_Label[n]);

		m_pTab->AppendItem(Item);
		}

	m_pTab->SetFont(afxFont(Dialog));

	m_pTab->SetCurSel(m_uFocus);

	m_pTab->ShowWindow(SW_SHOW);
	}

void CMultiViewWnd::CreateView(UINT uView)
{
	if( !m_Views[uView]->IsWindow() ) {

		UINT  uID       = IDVIEW + uView;
		
		DWORD dwStyle   = WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

		DWORD dwExStyle = (GetWindowExStyle() & WS_EX_CONTROLPARENT);
	
		m_Views[uView]->Create(dwExStyle, dwStyle, m_ViewRect, ThisObject, uID, NULL);

		m_dwMade |= (1 << uView);
		}
	}

void CMultiViewWnd::CalcLayout(void)
{
	CRect Rect = GetClientRect() - m_nMargin;

	m_TabRect  = Rect;

	m_TabRect.right  += 2;

	m_TabRect.bottom += 2;

	m_pTab->AdjustRect(FALSE, Rect);

	m_ViewRect.left   = Rect.left   + 0;

	m_ViewRect.top    = Rect.top    + 1;

	m_ViewRect.right  = Rect.right  + 1;

	m_ViewRect.bottom = Rect.bottom + 1;
	}

void CMultiViewWnd::MoveWindows(void)
{
	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {
	
		CViewWnd *pWnd = m_Views[n];

		if( pWnd->IsWindow() ) {
	
			pWnd->MoveWindow(m_ViewRect, TRUE);
			}
		}

	m_pTab->MoveWindow(m_TabRect, TRUE);
	}

BOOL CMultiViewWnd::SelectFocus(UINT uFocus)
{	
	if( Select(uFocus) ) {
	
		m_Views[m_uFocus]->SetFocus();
		
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CMultiViewWnd::Select(UINT uFocus)
{
	if( m_uFocus != uFocus ) {

		CreateView(uFocus);
			
		m_Views[uFocus]->SetCurrent(TRUE);

		m_Views[uFocus]->ShowWindow(SW_SHOWNOACTIVATE);

		m_Views[uFocus]->SetWindowOrder(HWND_TOP, TRUE);
			
		m_Views[m_uFocus]->ShowWindow(SW_HIDE);

		m_Views[m_uFocus]->SetCurrent(FALSE);

		m_pTab->SetCurSel(m_uFocus = uFocus);
		
		return TRUE;
		}
		
	return FALSE;
	}

void CMultiViewWnd::SetDrop(CPoint Pos)
{
	ScreenToClient(Pos);

	UINT uDrop = NOTHING;

	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {

		CRect Rect = m_pTab->GetItemRect(n);

		if( Rect.PtInRect(Pos) ) {

			uDrop = n;

			break;
			}
		}

	SetDrop(uDrop);
	}

void CMultiViewWnd::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		if( (m_uDrop = uDrop) < NOTHING ) {

			SetTimer(m_timerSelect, 300);
			}
		else
			KillTimer(m_timerSelect);

		m_pTab->Invalidate(FALSE);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Tabbed View Window with Auto Attach
//

// Dynamic Class

AfxImplementDynamicClass(CAutoMultiViewWnd, CMultiViewWnd);

// Overribables

void CAutoMultiViewWnd::OnAttach(void)
{
	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {

		m_Views[n]->Attach(m_pItem);
		}
	}

// End of File
