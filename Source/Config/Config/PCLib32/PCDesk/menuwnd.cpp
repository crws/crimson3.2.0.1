
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Window with Menu
//

// Dynamic Class

AfxImplementDynamicClass(CMenuWnd, CWnd);

// Constructor

CMenuWnd::CMenuWnd(void)
{
	m_fMouse = FALSE;
	}

// Message Map

AfxMessageMap(CMenuWnd, CWnd)
{
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_SYSCOMMAND)
	AfxDispatchMessage(WM_MENUSELECT)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxMessageEnd(CMenuWnd)
	};

// Message Handlers

BOOL CMenuWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	LPARAM lParam = m_MsgCtx.Msg.lParam;

	if( uID >= 0xF000 ) {
		
		AfxCallDefProc();
		
		return TRUE;
		}
		
	if( uID >= 0x8000 ) {
	
		CCmdSourceData Source;
		
		if( uID < 0xF000 ) {

			Source.PrepareSource();
			}
	
		RouteControl(uID, Source);
		
		if( Source.GetFlags() & MF_DISABLED ) {
		
			MessageBeep(0);
			
			return TRUE;
			}

		RouteCommand(uID, lParam);
			
		return TRUE;
		}

	SendMessage(WM_AFX_COMMAND, uID, lParam);
			
	return TRUE;
	}

void CMenuWnd::OnSysCommand(UINT uID, CPoint Pos)
{
	switch( uID & 0xFFF0 ) {
	
		case SC_MOVE:
		case SC_SIZE:

			SendMessage(WM_CANCELMODE);
			
			break;
		}
		
	AfxRouteToDefProc();
	}

void CMenuWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	UINT uCount = Menu.GetMenuItemCount();

	for( UINT uPos = 0; uPos < uCount; uPos++ ) {

		UINT uID = Menu.GetMenuItemID(uPos);
		
		if( uID == 0xFFFF ) {

			CMenu &Sub = Menu.GetSubMenu(uPos);
			
			if( Sub.GetHandle() ) {

				OnInitPopup(Sub, 0, FALSE);
				}
			}
		else {
			if( HIBYTE(uID) ) {
	
				CCmdSourceMenu Source(Menu, uID);
	
				if( uID < 0xF000 ) {

					Source.PrepareSource();
					}
	
				RouteControl(uID, Source);
				}
			}
		}

	m_uLast = 0;
	}

void CMenuWnd::OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu)
{
	// NOTE -- This is a horrible hack to prevent separators from being
	// selected by the keyboard and to prevent the padding item at the
	// start of the menu bar from being selected. It also detects mouse
	// selection to allow the owner-draw logic to use the right colors.

	if( uFlags & MF_OWNERDRAW ) {

		if( uID == 0x80 ) {

			if( m_uLast ) {

				FakeKey(VK_LEFT);
				}
			else
				FakeKey(VK_RIGHT);
			}

		if( uFlags & MF_MOUSESELECT ) {

			m_fMouse = TRUE;
			}
		else {
			if( uFlags & MF_POPUP ) {

				uID = UINT(Menu.GetSubMenu(uID).GetHandle());
				}

			if( uID < 0x20 ) {

				for( int n = 0; n < Menu.GetMenuItemCount(); n++ ) {

					if( Menu.GetMenuItemID(n) == uID ) {

						if( Menu.GetMenuUniqueID(n-1) == m_uLast ) {

							FakeKey(VK_DOWN);
							}

						if( Menu.GetMenuUniqueID(n+1) == m_uLast ) {

							FakeKey(VK_UP);
							}
						
						break;
						}
					}
				}

			m_fMouse = FALSE;
			}
		}

	m_uLast = uID;
	}

void CMenuWnd::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	CMenuInfo *pInfo = (CMenuInfo *) Item.itemData;

	if( pInfo->m_Text.GetLength() ) {

		CClientDC DC(ThisObject);

		DC.Select(afxFont(Status));

		if( pInfo->m_fTop ) {

			CRect Calc;

			DC.DrawText( pInfo->m_Text,
				     Calc,
				     DT_CALCRECT | DT_SINGLELINE
				     );

			Item.itemWidth  = Calc.cx();
		
			Item.itemHeight = Calc.cy();
			}
		else {
			Item.itemWidth  = 42;
			
			Item.itemHeight = 22;

			CStringArray List;

			pInfo->m_Text.Tokenize(List, '\t');

			UINT n;

			for( n = 0; n < List.GetCount(); n++ ) {

				if( List[n].GetLength() > 1 ) {

					CRect Calc;

					DC.DrawText( List[n],
						     Calc,
						     DT_CALCRECT | DT_SINGLELINE
						     );

					UINT cx = Calc.cx();

					Item.itemWidth += cx;
					}
				}

			Item.itemWidth += 8 * (n - 1);
			}

		DC.Deselect();
		}
	else {
		Item.itemWidth  = 1;;
		
		Item.itemHeight = 3;
		}
	}

void CMenuWnd::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
        if( Item.CtlType == ODT_MENU ) {
		
		CRect Rect = Item.rcItem;
	
		CSize Size = Rect.GetSize();

		CDC       MenuDC(Item.hDC);

		CMemoryDC WorkDC(MenuDC);

		CBitmap   WorkBM(MenuDC, Size);

		WorkDC.Select(WorkBM);

		DRAWITEMSTRUCT Copy = Item;

		Copy.hDC      = WorkDC;
	
		Copy.rcItem   = CRect(Size);

		CPoint Corner = Rect.GetTopLeft();

		CColor Color  = MenuDC.GetPixel(Corner);

		WorkDC.FillRect(Size, CBrush(Color));

		DrawMenu(WorkDC, Copy);

		MenuDC.BitBlt(Rect, WorkDC, CPoint(0, 0), SRCCOPY);

		WorkDC.Deselect();
		}
	}

// Implementation

void CMenuWnd::DrawMenu(CDC &DC, DRAWITEMSTRUCT &Item)
{
	CMenuInfo *pInfo = (CMenuInfo *) Item.itemData;

	DC.Select(afxFont(Status));

	DC.SetBkMode(TRANSPARENT);

	if( pInfo->m_fTop ) {

		DrawTopMenu(DC, pInfo, Item.rcItem, Item.itemState);
		}
	else
		DrawStdMenu(DC, pInfo, Item.rcItem, Item.itemState);

	DC.Deselect();
	}

void CMenuWnd::DrawStdMenu(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState)
{
	CRect Work = Rect;

	Work.right = Work.left + 22;

	DC.GradHorz(Work, afxColor(Blue1), afxColor(Blue3));

	Work.left  = Work.right;

	Work.right = Rect.right;

	if( pInfo->m_Text.GetLength() ) {

		DrawStdBack(DC, Work, Rect, uState);

		Work.left  = Work.left  +  4;

		Work.right = Work.right - 16;

		CStringArray List;

		pInfo->m_Text.Tokenize(List, '\t');

		UINT n;

		for( n = 0; n < List.GetCount(); n++ ) {

			if( List[n].GetLength() > 1 ) {

				CRect Calc;

				DC.DrawText( List[n],
					     Calc,
					     DT_CALCRECT | DT_SINGLELINE
					     );

				if( n && n == List.GetCount() - 1 ) {

					CRect Draw(Work);

					Draw.left = Draw.right - Calc.cx();

					DC.DrawText( List[n],
						     Draw, 
						     DT_SINGLELINE | DT_VCENTER
						     );
					}
				else {
					CRect Draw(Work);

					Draw.right = Draw.left  + Calc.cx();

					Work.left  = Draw.right + 8;

					DC.DrawText( List[n],
						     Draw,
						     DT_SINGLELINE | DT_VCENTER
						     );
					}
				}
			}

		if( List[n-1] == L">" ) {

			CRect Draw(Rect);

			Draw.left = Draw.right - 18;

			Draw.top  = Draw.top   +  4;

			CBitmap Pick(L"MenuPickMark");

			DC.BitBlt(Draw, Pick, CPoint(0, 0), SRCAND);
			}

		if( (uState & ODS_CHECKED) || pInfo->m_uImage ) {

			CRect Draw(Rect);

			Draw.right = Draw.left + 22;

			if( uState & ODS_CHECKED ) {

				if( uState & ODS_SELECTED ) {

					DC.FrameRect(Draw-1, afxBrush(Orange3));

					if( !(uState & ODS_DISABLED) ) {
		
						DC.FillRect(Draw-2, afxBrush(Orange2));
						}
					}
				else {
					DC.FrameRect(Draw-1, afxBrush(3dDkShadow));

					if( !(uState & ODS_DISABLED) ) {
		
						DC.FillRect(Draw-2, afxBrush(Orange2));
						}
					}

				if( !pInfo->m_uImage ) {

					DC.Select(afxFont(Marlett2));

					DC.DrawText( L"\x061",
						     Draw,
						     DT_SINGLELINE | DT_VCENTER | DT_CENTER
						     );

					DC.Deselect();
					}
				}

			if( pInfo->m_uImage ) {

				DrawMenuImage( DC,
					       Draw-3,
					       pInfo->m_uImage,
					       uState
					       );
				}
			}
		}
	else {
		DC.FillRect(Work, afxBrush(MenuFace));

		Work.left   += 4;

		Work.top    += 1;

		Work.bottom -= 1;

		DC.FillRect(Work, afxBrush(3dShadow));
		}
	}

void CMenuWnd::DrawTopMenu(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState)
{
	if( pInfo->m_Text.GetLength() ) {

		CRect Work = Rect;

		CRect Calc;

		DC.DrawText( pInfo->m_Text,
			     Calc,
			     DT_CALCRECT | DT_SINGLELINE
			     );

		Work.top    = Rect.top + 2;

		Work.bottom = Work.top + Calc.cy() + 6;

		CRect Fill1 = Work;

		CRect Fill2 = Work;
		
		DrawTopBack(DC, Work, uState);

		DC.DrawText( pInfo->m_Text,
			     Work,
			     DT_SINGLELINE | DT_VCENTER | DT_CENTER
			     );

		Fill1.bottom = Fill1.top;

		Fill1.top    = Fill1.bottom - 2;

		Fill2.top    = Fill2.bottom;

		Fill2.bottom = Fill2.top + 3;

		DC.FillRect(Fill1, afxBrush(MenuBar));

		DC.FillRect(Fill2, afxBrush(MenuBar));
		}
	}

void CMenuWnd::DrawMenuImage(CDC &DC, CRect const &Rect, UINT uImage, UINT uState)
{
	if( uState & ODS_DISABLED ) {

		uState = MF_POPUP | MF_DISABLED;
		}
	else {
		if( uState & ODS_SELECTED ) {

			uState = MF_POPUP | MF_HILITE;
			}
		else
			uState = MF_POPUP;
		}

	afxButton->Draw( DC,
			 Rect,
			 uImage,
			 L"", 
			 uState
			 );
	}

void CMenuWnd::DrawStdBack(CDC &DC, CRect &Rect, CRect &Wide, UINT uState)
{
	if( uState & ODS_DISABLED ) {

		if( m_fMouse || !(uState & ODS_SELECTED) ) {

			DC.FillRect(Rect, afxBrush(MenuFace));

			DC.SetTextColor(afxColor(3dShadow));
			}
		else {
			DC.FrameRect(Wide-0, afxBrush(3dDkShadow));

			DC.FillRect (Wide-1, afxBrush(MenuFace));

			DC.SetTextColor(afxColor(3dShadow));
			}
		}
	else {
		if( uState & ODS_SELECTED ) {

			DC.FrameRect(Wide-0, afxBrush(3dDkShadow));

			DC.FillRect (Wide-1, afxBrush(Orange3));
			}
		else
			DC.FillRect(Rect, afxBrush(MenuFace));
		
		DC.SetTextColor(afxColor(MenuText));
		}
	}

void CMenuWnd::DrawTopBack(CDC &DC, CRect &Rect, UINT uState)
{
	if( uState & ODS_SELECTED ) {

		DC.FrameRect(Rect--, afxBrush(3dDkShadow));

		DC.GradVert (Rect--, afxColor(Blue1), afxColor(Blue3));

		DC.SetBkMode(TRANSPARENT);
		}
	else {
		if( uState & ODS_HOTLIGHT ) {

			DC.FrameRect(Rect--, afxBrush(3dDkShadow));

			DC.GradVert (Rect--, afxColor(Orange3), afxColor(Orange2));

			DC.SetBkMode(TRANSPARENT);
			}
		else
			DC.FillRect(Rect, afxBrush(MenuBar));
		}
	}

void CMenuWnd::FakeKey(UINT uCode)
{
	// TODO -- Is this correct??? Wrong window, surely???

	afxMainWnd->PostMessage(WM_KEYDOWN, uCode);

	afxMainWnd->PostMessage(WM_KEYUP,   uCode);
	}

// End of File
