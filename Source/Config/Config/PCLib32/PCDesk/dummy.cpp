
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Dummy View Window
//

// Dynamic Class

AfxImplementDynamicClass(CDummyViewWnd, CViewWnd);

// Constructor

CDummyViewWnd::CDummyViewWnd(void)
{
	}

// Message Map

AfxMessageMap(CDummyViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_RBUTTONDOWN)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)

	AfxMessageEnd(CDummyViewWnd)
	};

// Message Handlers

BOOL CDummyViewWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CDummyViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);
	
	DC.Select(afxFont(Dialog));
	
	CRect Rect = GetClientRect();
	
	DC.FillRect (Rect, afxBrush(WHITE));

	DC.FrameRect(Rect, afxBrush(3dShadow));
	
	DC.SetTextColor(afxColor(ButtonText));
	
	DC.SetBkColor(afxColor(WHITE));
	
	CPrintf Text(L"%p [%u] [%u] %u %u", m_pItem, HasFocus(), IsCurrent(), Rect.cx(), Rect.cy());
	
	DC.TextOut(10, 10, Text);
	
	DC.Deselect();
	}

void CDummyViewWnd::OnSetFocus(CWnd &Wnd)
{
	CPaintDC DC(ThisObject);
	
	DC.Select(afxFont(Dialog));
	
	CPrintf Text(L"%p [%u] [%u]", this, 1, 1);
	
	CSize Size = DC.GetTextExtent(Text) + CSize(10, 10);
	
	DC.Deselect();
	
//	CreateCaret(m_hWnd, NULL, Size.cx, Size.cy);
	
//	SetCaretPos(5, 5);
	
//	ShowCaret(m_hWnd);
	
	Invalidate(TRUE);
	}

void CDummyViewWnd::OnKillFocus(CWnd &Wnd)
{
//	HideCaret(m_hWnd);
	
//	DestroyCaret();
	
	Invalidate(TRUE);
	}

void CDummyViewWnd::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	}
	
void CDummyViewWnd::OnRButtonDown(UINT uCode, CPoint Pos)
{
	SetFocus();
	}
	
void CDummyViewWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	KillTimer(100);
	
	SetFocus();
	}

void CDummyViewWnd::OnLoadMenu(UINT uID, CMenu &Menu)
{
/*	if( uID == 0 ) {

		Menu.AppendMenu(CMenu(L"TestMenu"));
		}
*/	}

void CDummyViewWnd::OnLoadTool(UINT uID, CMenu &Tool)
{
/*	if( uID == 0 ) {

		Tool.AppendMenu(CMenu(L"TestTool"));
		}
*/	}

// End of File
