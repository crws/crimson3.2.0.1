
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Triple Split View Window
//

// Runtime Class

AfxImplementRuntimeClass(CTripleViewWnd, CViewWnd);

// Static Data

UINT CTripleViewWnd::m_timerCheck  = AllocTimerID();

UINT CTripleViewWnd::m_timerSelect = AllocTimerID();

// Constructor

CTripleViewWnd::CTripleViewWnd(void)
{
	m_pWnd [0] = NULL;

	m_pWnd [1] = NULL;

	m_pWnd [2] = NULL;

	m_uShow[0] = showLocked;
			   
	m_uShow[1] = showLocked;

	m_nBar[0]  = 180;

	m_nBar[1]  = 180;

	m_uFocus   = 2;

	m_uHover   = 0;

	m_fPress   = FALSE;

	m_fAnim    = FALSE;

	m_fSemi    = FALSE;

	m_uCapture = 0;

	m_uDrop    = NOTHING;

	m_uTwin    = 0;

	m_Accel. Create(L"TripleViewAccel");

	m_Cursor.Create(L"HSplitAdjust");
	}

// Attributes

BOOL CTripleViewWnd::IsSemiModal(void) const
{
	return m_fSemi;
	}

BOOL CTripleViewWnd::InTwinMode(void) const
{
	return m_uTwin > 0;
	}

BOOL CTripleViewWnd::InFloater(void) const
{
	INDEX i = m_Float.GetHead();

	while( !m_Float.Failed(i) ) {

		CWnd *pWnd = m_Float[i];

		if( pWnd->IsActive() ) {

			return TRUE;
			}

		m_Float.GetNext(i);
		}

	return FALSE;
	}

// Operations

void CTripleViewWnd::SetView(UINT uView, CViewWnd *pView)
{
	AfxAssert(!m_hWnd);

	m_pWnd[uView] = pView;
	}

void CTripleViewWnd::SetTwinMode(BOOL fTwin)
{
	if( fTwin ) {

		if( !m_uTwin++ ) {

			memcpy(m_uSaveShow, m_uShow, sizeof(m_uShow));

			memcpy(m_nSaveBar,  m_nBar,  sizeof(m_nBar));

			m_nBar [0] = GetThirdWidth();

			m_uShow[0] = showLocked;

			m_uShow[1] = showLocked;

			UpdateLayout(NOTHING);
			}
		}
	else {
		if( !--m_uTwin ) {

			memcpy(m_uShow, m_uSaveShow, sizeof(m_uShow));

			memcpy(m_nBar,  m_nSaveBar,  sizeof(m_nBar));

			if( m_uFocus ) {

				AfxNull(CWnd).SetFocus();

				UpdateLayout(NOTHING);

				SelectFocus(2);
				}
			else
				UpdateLayout(0);
			}
		}
	}

void CTripleViewWnd::AddFloater(CWnd *pWnd)
{
	m_Float.Append(pWnd);
	}

void CTripleViewWnd::RemFloater(CWnd *pWnd)
{
	INDEX i = m_Float.GetHead();

	while( !m_Float.Failed(i) ) {

		if( m_Float[i] == pWnd ) {

			m_Float.Remove(i);

			return;
			}

		m_Float.GetNext(i);
		}

	AfxAssert(FALSE);
	}

// IUnknown

HRESULT CTripleViewWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CTripleViewWnd::AddRef(void)
{
	return 1;
	}

ULONG CTripleViewWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CTripleViewWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( !m_fSemi ) {

		CPoint Pos(pt.x, pt.y);

		SetDrop(Pos);
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CTripleViewWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( !m_fSemi ) {

		CPoint Pos(pt.x, pt.y);

		SetDrop(Pos);
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CTripleViewWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( !m_fSemi ) {

		SetDrop(NOTHING);
		}

	return S_OK;
	}

HRESULT CTripleViewWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( !m_fSemi ) {

		SetDrop(NOTHING);
		}

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Routing Control

BOOL CTripleViewWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	UINT uRC = GetRoutingCode(Message);

	BOOL fRV = FALSE;

	BYTE bLo = LOBYTE(uRC);

	BYTE bHi = HIBYTE(uRC);

	////////
	
	if( bHi & HIBYTE(RC_LOCAL1) ) {
	
		if( CViewWnd::OnRouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	UINT uList[3] = { 2, 0, 1 };

	for( UINT i = 0; i < 3; i++ ) {

		UINT n = uList[i];

		if( !m_pWnd[n]->IsWindow() ) {

			continue;
			}

		if( UINT(n) == m_uFocus || bLo == RC_FIRST || bLo == RC_ALL ) {
		
			if( m_pWnd[n]->RouteMessage(Message, lResult) ) {
			
				if( bLo == RC_ALL ) {
					
					if( lResult ) {

						fRV = TRUE;
						}

					continue;
					}

				if( bHi & HIBYTE(RC_LOCAL2) ) {

					CViewWnd::OnRouteMessage(Message, lResult);
					}
					
				return TRUE;
				}
			}
		}

	if( !m_Float.IsEmpty() ) {

		INDEX i = m_Float.GetHead();

		while( !m_Float.Failed(i) ) {

			CWnd *pWnd = m_Float[i];

			if( bLo == RC_FIRST || bLo == RC_ALL ) {
		
				if( pWnd->RouteMessage(Message, lResult) ) {
				
					if( bLo == RC_ALL ) {
						
						if( lResult ) {

							fRV = TRUE;
							}

						m_Float.GetNext(i);

						continue;
						}

					if( bHi & HIBYTE(RC_LOCAL2) ) {

						CViewWnd::OnRouteMessage(Message, lResult);
						}
						
					return TRUE;
					}
				}

			m_Float.GetNext(i);
			}
		}

	if( !fRV ) {

		return CViewWnd::OnRouteMessage(Message, lResult);
		}

	return TRUE;
	}

// Message Map

AfxMessageMap(CTripleViewWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_ACTIVATEAPP)
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_TIMER)
	
	AfxDispatchControlType(IDM_WINDOW, OnWindowControl)
	AfxDispatchCommandType(IDM_WINDOW, OnWindowCommand)

	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)

	AfxMessageEnd(CTripleViewWnd)
	};

// Accelerator

BOOL CTripleViewWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CTripleViewWnd::OnPostCreate(void)
{
	LoadConfig();

	CalcWndRects();

	CreateView(2);
	
	CreateView(0);
	
	CreateView(1);

	OnUpdateLayout();

	MoveWindows(FALSE);

	m_DropHelp.Bind(m_hWnd, this);

	m_pWnd[0]->BringWindowToTop();

	m_pWnd[1]->BringWindowToTop();

	m_pWnd[m_uFocus]->SetFocus();
	}

void CTripleViewWnd::OnPreDestroy(void)
{
	INDEX i = m_Float.GetHead();

	while( !m_Float.Failed(i) ) {

		CWnd *pWnd = m_Float[i];

		pWnd->DestroyWindow(TRUE);

		m_Float.GetNext(i);
		}
	}

void CTripleViewWnd::OnActivateApp(BOOL fActive, DWORD dwThread)
{
	INDEX i = m_Float.GetHead();

	while( !m_Float.Failed(i) ) {

		CWnd *pWnd = m_Float[i];

		pWnd->SendMessage( m_MsgCtx.Msg.message,
				   m_MsgCtx.Msg.wParam,
				   m_MsgCtx.Msg.lParam
				   );

		m_Float.GetNext(i);
		}
	}

void CTripleViewWnd::OnCancelMode(void)
{
	if( !m_fSemi ) {

		CancelSlideOut();

		m_pWnd[m_uFocus]->SendMessage(WM_CANCELMODE);
		}
	}

void CTripleViewWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_RESTORED || uCode == SIZE_MAXIMIZED ) {

		if( m_pWnd[2] && m_pWnd[2]->IsWindow() ) {

			int tw = GetThirdWidth();

			MakeMin(m_nBar[0], tw);

			MakeMin(m_nBar[1], tw);
		
			CalcWndRects();
		
			MoveWindows(FALSE);
			}
		}
	}

BOOL CTripleViewWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}
	
void CTripleViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	for( UINT n = 0; n < 2; n++ ) {
	
		if( m_uShow[n] ) {

			if( n ) {

				DC.DrawRightEdge(Rect, 3, afxBrush(3dHighlight));
				}
			else
				DC.DrawLeftEdge (Rect, 3, afxBrush(3dHighlight));

			DC.FillRect(m_Line[n], afxBrush(3dHighlight));
			}
		else {
			if( m_fAnim ) {
				
				BOOL fNow = m_pWnd[n]->IsWindowVisible();
				
				BOOL fReq = (m_uShow[n] > 0);

				if( fNow == fReq ) {

					DrawSideBar(DC, n);
					}
				else {
					CRect Side = m_Line[n];

					DC.FillRect(Side, afxBrush(3dHighlight));
					}
				}
			else
				DrawSideBar(DC, n);

			if( n ) {

				Rect.right = m_Line[n].left;
				}
			else
				Rect.left  = m_Line[n].right;
			}
		}

	DC.DrawTopEdge   (Rect, 3, afxBrush(3dHighlight));

	DC.DrawBottomEdge(Rect, 3, afxBrush(3dHighlight));
	}

void CTripleViewWnd::OnSetFocus(CWnd &Wnd)
{
	if( !m_uCapture ) {
	
		if( m_pWnd[m_uFocus] ) {

			m_pWnd[m_uFocus]->SetFocus();
			}
		}
	}

void CTripleViewWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID >= IDVIEW && uID <= IDVIEW + 2 ) {

		UINT uFocus = uID - IDVIEW;

		if( !m_fSemi ) {

			if( uFocus < 2 ) {

				if( m_uShow[1 - uFocus] == showSlide ) {

					CancelSlideOut();
					}
				}
			else
				CancelSlideOut();
			}
			
		Select(uFocus);
		}
	}

BOOL CTripleViewWnd::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( &Wnd == this && uHitTest == HTCLIENT ) {

		if( !m_fSemi ) {

			CPoint Pos = GetCursorPos();
			
			ScreenToClient(Pos);

			UINT c = InTwinMode() ? 1 : 2;

			for( UINT n = 0; n < c; n++ ) {
					
				if( m_uShow[n] && m_Line[n].PtInRect(Pos) ) {
					
					SetCursor(m_Cursor);
					
					return TRUE;
					}
				}

			return BOOL(AfxCallDefProc());
			}

		SetCursor(CCursor(IDC_ARROW));

		return TRUE;
		}
	
	return BOOL(AfxCallDefProc());
	}

void CTripleViewWnd::OnSetCurrent(BOOL fCurrent)
{
	m_pWnd[m_uFocus]->SetCurrent(fCurrent);
	}

void CTripleViewWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_fSemi && !m_uCapture ) {

		UINT c = InTwinMode() ? 1 : 2;

		for( UINT n = 0; n < c; n++ ) {

			if( !m_Line[n].PtInRect(Pos) ) {

				continue;
				}
			
			if( m_uShow[n] == showHidden ) {
			
				Invalidate(m_Line[n], FALSE);

				m_uCapture = 100 + n;

				break;
				}

			if( m_uShow[n] == showLocked ) {

				m_uCapture  = 200 + n;

				m_nTrackOrg = Pos.x;

				m_nTrackPos = Pos.x;

				if( InTwinMode() ) {

					int tw = GetThirdWidth();

					m_nTrackMin = m_nTrackOrg - (m_nBar[n] - 1 * tw);

					m_nTrackMax = m_nTrackOrg + (2 * tw - m_nBar[n]);

					break;
					}

				if( n == 0 ) {

					int tw = GetThirdWidth();

					m_nTrackMin = m_nTrackOrg - (m_nBar[n]      - 64);

					m_nTrackMax = m_nTrackOrg + (m_Rect[2].cx() - tw);
					}
				else {
					int tw = GetThirdWidth();

					m_nTrackMin = m_nTrackOrg - (m_Rect[2].cx() - tw);

					m_nTrackMax = m_nTrackOrg + (m_nBar[n]      - 64);
					}

				break;
				}
			}

		if( m_uCapture ) {

			if( m_uCapture / 100 == 2 ) {

				CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

				ShowDragBar(DC);
				}

			CancelSlideOut();

			SetCapture();

			KillTimer(m_timerCheck);

			m_fPress = TRUE;
			}
		}
	}

void CTripleViewWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( !m_fSemi ) {
		
		if( m_uCapture ) {

			if( m_uCapture / 100 == 1 ) {

				UINT uSide  = m_uCapture % 100;

				BOOL fPress = m_Line[uSide].PtInRect(Pos);

				if( m_fPress != fPress ) {

					Invalidate(m_Line[uSide], FALSE);

					m_fPress = fPress;
					}
				}

			if( m_uCapture / 100 == 2 ) {

				CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

				ShowDragBar(DC);

				m_nTrackPos = Pos.x;

				MakeMin(m_nTrackPos, m_nTrackMax);

				MakeMax(m_nTrackPos, m_nTrackMin);

				ShowDragBar(DC);
				}
			}
		else {
			UINT uHover = CheckHover(Pos);

			if( m_uHover != uHover ) {

				if( m_uHover ) {

					Invalidate(m_Line[m_uHover % 100], FALSE);
					}

				if( (m_uHover = uHover) ) {
								
					SetTimer(m_timerCheck, 10);
					}
				
				if( m_uHover ) {

					Invalidate(m_Line[m_uHover % 100], FALSE);
					}

				return;
				}
			}
		}
	}

void CTripleViewWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( !m_fSemi ) {
		
		if( m_uCapture ) {

			ReleaseCapture();

			if( m_uCapture / 100 == 1 ) {

				if( m_fPress ) {

					UINT uSide = m_uCapture % 100;

					m_uShow[uSide] = showSlide;

					UpdateLayout(uSide);
					}
				}

			if( m_uCapture / 100 == 2 ) {

				CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

				ShowDragBar(DC);

				if( m_uCapture % 100 == 0 ) {

					m_nBar[0] += m_nTrackPos - m_nTrackOrg;
					}
				else
					m_nBar[1] -= m_nTrackPos - m_nTrackOrg;

				UpdateLayout(NOTHING);

				SaveConfig();
				}

			m_uCapture = 0;

			m_fPress   = FALSE;

			OnMouseMove(uFlags, Pos);
			}
		}
	}

void CTripleViewWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerCheck ) {

		CPoint Pos = GetCursorPos();

		ScreenToClient(Pos);

		if( !CheckHover(Pos) ) {

			Invalidate(m_Line[0], FALSE);

			Invalidate(m_Line[1], FALSE);

			m_uHover = 0;

			KillTimer(uID);
			}
		}
	
	if( uID == m_timerSelect ) {

		if( m_uDrop < NOTHING ) {

			m_uShow[m_uDrop] = showSlide;

			UpdateLayout(m_uDrop);

			m_uDrop = NOTHING;
			}

		KillTimer(uID);
		}
	}

// Command Handlers

BOOL CTripleViewWnd::OnWindowControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_WINDOW_NEXT_PANE:
		case IDM_WINDOW_PREV_PANE:

			Src.EnableItem(TRUE);
			
			break;

		default:
			return FALSE;
		}
		
	return TRUE;
	}

BOOL CTripleViewWnd::OnWindowCommand(UINT uID)
{
	if( uID == IDM_WINDOW_NEXT_PANE || uID == IDM_WINDOW_PREV_PANE ) {

		UINT uDelta  = (uID == IDM_WINDOW_NEXT_PANE) ? 1 : 2;

		UINT uFocus  = (m_uFocus + uDelta) % 3;

		BOOL fUpdate = FALSE;

		if( m_uFocus < 2 ) {

			if( m_uShow[m_uFocus] == showSlide ) {
	
				m_uShow[m_uFocus] = showHidden;

				fUpdate = TRUE;
				}
			}

		if( uFocus < 2 ) {

			if( m_uShow[uFocus] == showHidden ) {
	
				m_uShow[uFocus] = showSlide;

				fUpdate = TRUE;
				}
			}

		if( fUpdate ) {

			UpdateLayout(uFocus);
			}
		else
			SelectFocus(uFocus);
	
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTripleViewWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_VIEW_SHOW_LEFT:
			
			Src.EnableItem(!m_fSemi && !InTwinMode());
			
			Src.CheckItem (m_uShow[0] == showLocked);
			
			break;
			
		case IDM_VIEW_SHOW_RIGHT:
			
			Src.EnableItem(!m_fSemi && !InTwinMode());
			
			Src.CheckItem (m_uShow[1] == showLocked);
			
			break;
			
		default:
			return FALSE;
		}
		
	return TRUE;
	}

BOOL CTripleViewWnd::OnViewCommand(UINT uID)
{
	if( uID == IDM_VIEW_SHOW_LEFT || uID == IDM_VIEW_SHOW_RIGHT ) {

                if( !m_fSemi ) {

			UINT uSide = (uID == IDM_VIEW_SHOW_RIGHT);

			if( m_uShow[uSide] == showLocked ) {

				m_uShow[uSide] = showHidden;

				UpdateLayout(2);
				}
			else {
				m_uShow[uSide] = showLocked;

				UpdateLayout(uSide);
				}

			SaveConfig();
			
			return TRUE;
			}
		}
		
	return FALSE;
	}

// Drawing Helpers

void CTripleViewWnd::DrawSideBar(CDC &DC, UINT n)
{
	CRect  Side   = m_Line[n];

	PCTXT  pArrow = NULL;

	int    xArrow = 0;

	CColor Color1 = 0;
	
	CColor Color2 = 0;

	if( m_uHover == 100 + n || m_uDrop == n ) {

		if( m_fPress ) {

			Color1 = afxColor(Orange1);
			
			Color2 = afxColor(Orange2);
			}
		else {
			Color1 = afxColor(Orange3);
			
			Color2 = afxColor(Orange2);
			}
		}
	else {
		Color1 = afxColor(Blue1);
		
		Color2 = afxColor(Blue3);
		}

	if( n ) {

		pArrow = L"\x033";

		xArrow = 1;

		Swap(Color1, Color2);

		DC.DrawLeftEdge (Side, 3, afxBrush(3dHighlight));
		}
	else {
		pArrow = L"\x034";

		xArrow = 0;

		DC.DrawRightEdge(Side, 3, afxBrush(3dHighlight));
		}

	DC.GradHorz(Side, Color1, Color2);

	DC.Select(afxFont(Marlett2));

	CSize Size = DC.GetTextExtent(pArrow);

	int yp = (Side.top  + Side.bottom - Size.cy) / 2;

	int xp = (Side.left + Side.right  - Size.cx) / 2;

	DC.SetBkMode(TRANSPARENT);

	DC.TextOut(xp + xArrow, yp - 20, pArrow);

	DC.TextOut(xp + xArrow, yp     , pArrow);

	DC.TextOut(xp + xArrow, yp + 20, pArrow);
	}

void CTripleViewWnd::ShowDragBar(CDC &DC)
{
	UINT  uSide = m_uCapture % 100;

	CRect Rect  = m_Line[uSide];
	
	Rect.left  += m_nTrackPos - m_nTrackOrg;
	
	Rect.right += m_nTrackPos - m_nTrackOrg;
	
	DC.PatBlt(Rect, DSTINVERT);
	}

// Routing Codes

UINT CTripleViewWnd::GetRoutingCode(MSG const &Msg)
{
	switch( Msg.message ) {

		case WM_AFX_ACCEL:

			return RC_ALL;

		case WM_GOINGIDLE:
		case WM_INITMENUPOPUP:

			return RC_ALL;

		case WM_BROADCAST:

			return RC_BCAST | RC_ALL;
	
		case WM_AFX_COMMAND:
		case WM_AFX_CONTROL:
		case WM_AFX_GETINFO:
		
			switch( HIBYTE(Msg.wParam) ) {

				case IDM_VIEW:

					if( Msg.wParam == IDM_VIEW_REFRESH ) {

						return RC_ALL;
						}
					
					return RC_FIRST;

				case IDM_HELP:

					return RC_LOCAL1 | RC_ALL;

				case IDM_EDIT:

					return RC_CURRENT;
				}

			return RC_FIRST;
			
		case WM_LOADMENU:
		case WM_LOADTOOL:
		case WM_SHOWUI:

			return RC_CURRENT;
		}

	return RC_CURRENT;
	}

// Overridables

void CTripleViewWnd::OnUpdateLayout(void)
{
	}

// Implementation

void CTripleViewWnd::CreateView(UINT uView)
{
	if( !m_pWnd[uView] ) {
	
		AfxTrace(L"WARNING: Default view window created\n");
	
		m_pWnd[uView] = New CDummyViewWnd;
		}
		
	DWORD dwStyle = WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect Rect    = m_Rect[uView];

	UINT  uID     = IDVIEW + uView;
	
	m_pWnd[uView]->Create(dwStyle, Rect, ThisObject, uID, NULL);
	}

void CTripleViewWnd::CalcWndRects(void)
{
	CRect Client = GetClientRect();

	CRect Rect   = Client - 3;

	CRect Keep   = Rect;

	if( InTwinMode() ) {

		m_nBar[1] = Client.cx() - m_nBar[0] - 4;
		}

	if( TRUE ) {

		m_Rect[0] = Rect;

		m_Rect[1] = Rect;
		
		m_Rect[2] = Rect;
		}

	if( m_uShow[0] == showHidden ) {

		Rect.left  += 18;
		}

	if( m_uShow[1] == showHidden ) {

		Rect.right -= 18;
		}

	if( m_uShow[0] <= showSlide ) {

		Keep.left  += 18;
		}

	if( m_uShow[1] <= showSlide ) {

		Keep.right -= 18;
		}

	if( InTwinMode() ) {

		m_Rect[0].right = Rect.left  + m_nBar[0] - 2;

		m_Rect[2].left  = Keep.left  + (m_nBar    [0] + 1);

		m_Rect[2].right = Keep.right - (m_nSaveBar[1] + 1);

		m_Rect[1].left  = Rect.right - m_nBar[1] + 2;
		}
	else {
		m_Rect[0].right = Rect.left  + m_nBar[0] - 2;

		m_Rect[2].left  = Keep.left  + ((m_uShow[0] == showLocked) ? m_nBar[0] + 1 : 0);

		m_Rect[2].right = Keep.right - ((m_uShow[1] == showLocked) ? m_nBar[1] + 1 : 0);

		m_Rect[1].left  = Rect.right - m_nBar[1] + 2;
		}

	if( m_uShow[0] ) {

		m_Line[0]       = Rect;

		m_Line[0].left  = m_Rect[0].right;

		m_Line[0].right = m_Rect[2].left;
		}
	else {
		m_Line[0]       = Client;

		m_Line[0].right = m_Rect[2].left;
		}
	
	if( m_uShow[1] ) {

		m_Line[1]       = Rect;

		m_Line[1].left  = m_Rect[2].right;

		m_Line[1].right = m_Rect[1].left;
		}
	else {
		m_Line[1]      = Client;

		m_Line[1].left = m_Rect[2].right;
		}
	}

UINT CTripleViewWnd::CheckHover(CPoint Pos)
{
	if( m_uShow[0] == showHidden && m_Line[0].PtInRect(Pos) ) {

		return 100;
		}

	if( m_uShow[1] == showHidden && m_Line[1].PtInRect(Pos) ) {

		return 101;
		}

	return 0;
	}

BOOL CTripleViewWnd::CancelSlideOut(void)
{
	if( m_uShow[0] == showSlide || m_uShow[1] == showSlide ) {

		if( m_uShow[0] == showSlide ) {

			m_uShow[0] = showHidden;
			}
		
		if( m_uShow[1] == showSlide ) {

			m_uShow[1] = showHidden;
			}

		UpdateLayout(NOTHING);

		return TRUE;
		}

	return FALSE;
	}

void CTripleViewWnd::UpdateLayout(UINT uPane)
{
	CWnd &Focus = GetFocus();

	AfxNull(CWnd).SetFocus();

	CalcWndRects();
	
	MoveWindows(TRUE);

	if( uPane < NOTHING ) {

		if( SelectFocus(uPane) ) {

			return;	
			}
		}

	Focus.SetFocus();
	}

void CTripleViewWnd::MoveWindows(BOOL fAnimate)
{
	UINT uTotal = 3;

	BOOL fHit   = FALSE;

	if( fAnimate ) {

		m_fAnim = TRUE;

		Invalidate(FALSE);

		UpdateWindow();

		uTotal = 2;
		}

	for( UINT n = 0; n < uTotal; n++ ) {

		BOOL fUse = TRUE;

		if( n < 2 ) {

			if( m_uShow[n] == showHidden ) {

				fUse = FALSE;
				}
			}

		if( fUse ) {

			m_pWnd[n]->MoveWindow(m_Rect[n], TRUE);

			m_pWnd[n]->EnableWindow(TRUE);

			if( !m_pWnd[n]->IsWindowVisible() ) {

				if( fAnimate ) {

					Animate(n, TRUE);

					fHit = TRUE;
					}
				else
					m_pWnd[n]->ShowWindow(SW_SHOWNA);
				}
			}
		else {
			if( m_pWnd[n]->IsWindowVisible() ) {

				if( fAnimate ) {

					Animate(n, FALSE);

					fHit = TRUE;
					}
				else {
					m_pWnd[n]->SendMessage(WM_CANCELMODE);

					m_pWnd[n]->ShowWindow(SW_HIDE);
					}
				}

			m_pWnd[n]->EnableWindow(FALSE);

			m_pWnd[n]->MoveWindow(m_Rect[n], FALSE);
			}
		}

	if( fAnimate ) {

		if( !fHit ) {

			OnUpdateLayout();

			m_pWnd[2]->MoveWindow(m_Rect[2], TRUE);
			}

		m_fAnim = FALSE;

		Invalidate(FALSE);

		UpdateWindow();
		}
	}

void CTripleViewWnd::Animate(UINT uWnd, BOOL fShow)
{
	if( fShow ) {

		UINT uVerb = uWnd ? AW_HOR_NEGATIVE : AW_HOR_POSITIVE;

		AnimateWindow( m_pWnd[uWnd]->GetHandle(),
			       100,
			       AW_SLIDE | uVerb
			       );

		OnUpdateLayout();

		m_pWnd[2]->MoveWindow(m_Rect[2], TRUE);
		}
	else {
		OnUpdateLayout();

		m_pWnd[2]->MoveWindow(m_Rect[2], TRUE);

		if( !IsAnimateBroken() ) {

			UINT uVerb = uWnd ? AW_HOR_POSITIVE : AW_HOR_NEGATIVE;

			AnimateWindow( m_pWnd[uWnd]->GetHandle(),
				       100,
				       AW_HIDE | AW_SLIDE | uVerb
				       );

			m_pWnd[uWnd]->SendMessage(WM_CANCELMODE);
			}
		else {
			m_pWnd[uWnd]->SendMessage(WM_CANCELMODE);

			m_pWnd[uWnd]->ShowWindow(SW_HIDE);
			}
		}
	}

BOOL CTripleViewWnd::IsAnimateBroken(void)
{
	OSVERSIONINFO Ver;

	Ver.dwOSVersionInfoSize = sizeof(Ver);

	GetVersionEx(&Ver);

	if( Ver.dwMajorVersion == 5 ) {
		
		if( Ver.dwMinorVersion == 1 ) {
		
			return FALSE;
			}

		return TRUE;
		}

	if( Ver.dwMajorVersion >= 6 ) {

		if( afxAero ) {

			// LATER -- Test on Aero!

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CTripleViewWnd::SelectFocus(UINT uFocus)
{	
	if( Select(uFocus) ) {

		m_pWnd[m_uFocus]->SetFocus();
		
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTripleViewWnd::Select(UINT uFocus)
{
	if( m_uFocus != uFocus ) {
			
		m_pWnd[m_uFocus]->SetCurrent(FALSE);
			
		m_uFocus = uFocus;
			
		m_pWnd[m_uFocus]->SetCurrent(TRUE);
			
		afxMainWnd->SendMessage(WM_UPDATEUI);
		
		return TRUE;
		}
		
	return FALSE;
	}

void CTripleViewWnd::FindConfig(CRegKey &Key)
{
	Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"C3Core");

	Key.MoveTo(m_KeyName);
	}

void CTripleViewWnd::LoadConfig(void)
{
	if( m_KeyName.GetLength() ) {

		CRegKey Key;

		FindConfig(Key);
		
		m_uShow[0] = Key.GetValue(L"Show0", m_uShow[0]);

		m_uShow[1] = Key.GetValue(L"Show1", m_uShow[1]);

		m_nBar [0] = Key.GetValue(L"Bar0",  m_nBar [0]);

		m_nBar [1] = Key.GetValue(L"Bar1",  m_nBar [1]);

		SaveConfig();
		}
	}

void CTripleViewWnd::SaveConfig(void)
{
	if( !InTwinMode() ) {

		if( m_KeyName.GetLength() ) {

			CRegKey Key;

			FindConfig(Key);
			
			Key.SetValue(L"Show0", m_uShow[0]);

			Key.SetValue(L"Show1", m_uShow[1]);
			
			Key.SetValue(L"Bar0",  m_nBar [0]);

			Key.SetValue(L"Bar1",  m_nBar [1]);
			}
		}
	}

void CTripleViewWnd::SetDrop(CPoint Pos)
{
	ScreenToClient(Pos);

	UINT uDrop = NOTHING;

	for( UINT n = 0; n < 2; n++ ) {

		if( m_uShow[n] == showHidden ) {

			if( m_Line[n].PtInRect(Pos) ) {

				uDrop = n;

				break;
				}
			}
		}

	SetDrop(uDrop);
	}

void CTripleViewWnd::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		if( m_uDrop < NOTHING ) {

			Invalidate(m_Line[m_uDrop], FALSE);
			}

		if( (m_uDrop = uDrop) < NOTHING ) {

			SetTimer(m_timerSelect, 300);
			}
		else
			KillTimer(m_timerSelect);

		if( m_uDrop < NOTHING ) {

			Invalidate(m_Line[m_uDrop], FALSE);
			}
		}
	}

int CTripleViewWnd::GetThirdWidth(void)
{
	return (GetClientRect() - 3).cx() / 3;
	}

// End of File
