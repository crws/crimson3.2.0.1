
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Generic Bar Window
//

// Dynamic Class

AfxImplementDynamicClass(CGadgetBarWnd, CGadgetHolderWnd);

// Constructor

CGadgetBarWnd::CGadgetBarWnd(void)
{
	m_Border.Set(4, 4, 4, 4);

	m_nHeight = 0;
	}

// Destructor

CGadgetBarWnd::~CGadgetBarWnd(void)
{
	}
		
// Creation

BOOL CGadgetBarWnd::Create(CRect const &Rect, CWnd &Parent)
{
	DWORD dwStyle = WS_CHILD | WS_CLIPCHILDREN;
	
	return CWnd::Create(dwStyle, Rect, Parent, 0, NULL);
	}
		
// Attributes

int CGadgetBarWnd::GetHeight(void)
{
	CalcHeight();
	
	return m_nHeight;
	}

// Message Map

AfxMessageMap(CGadgetBarWnd, CGadgetHolderWnd)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SIZE)

	AfxMessageEnd(CGadgetBarWnd)
	};

// Message Handlers

UINT CGadgetBarWnd::OnCreate(CREATESTRUCT &Create)
{
	CGadgetHolderWnd::OnCreate(Create);

	CalcPositions();

	AddToolTips();

	return 0;
	}

BOOL CGadgetBarWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CGadgetBarWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.FillRect(GetClientRect(), afxBrush(3dFace));

	CGadget *pScan = m_pHead;
	
	while( pScan ) {
	
		pScan->OnPaint(DC);
	
		pScan = pScan->m_pNext;
		}
	}

void CGadgetBarWnd::OnSize(UINT uCode, CSize Size)
{
	for( CGadget *pScan = m_pHead; pScan; pScan = pScan->m_pNext ) {
			
		if( pScan->GetProps() & (tbgSizeToFit | tbgRight) ) {
		
			CalcPositions();

			UpdateToolTips();
			
			Invalidate(TRUE);
			
			break;
			}
		}
	}

// Implementation

void CGadgetBarWnd::CalcHeight(void)
{
	if( !m_nHeight ) {

		for( CGadget *p1 = m_pHead; p1; p1 = p1->m_pNext ) {

			CSize Size = p1->GetSize();
			
			MakeMax(m_nHeight, Size.cy);
			}

		for( CGadget *p2 = m_pHead; p2; p2 = p2->m_pNext ) {
		
			p2->SetHeight(m_nHeight);
			
			p2->SetWindow(ThisObject);
			}

		m_nHeight += m_Border.top;
			
		m_nHeight += m_Border.bottom;
		}
	}

void CGadgetBarWnd::CalcPositions(void)
{
	CPoint Pos1 = m_Border.GetTopLeft();

	CPoint Pos2 = GetClientRect().GetTopRight();

	Pos2.x -= Pos1.x;

	Pos2.y += Pos1.y;
	
	for( CGadget *pScan = m_pHead; pScan; pScan = pScan->m_pNext ) {

		if( pScan->GetProps() & tbgRight ) {

			CSize Size = pScan->GetSize();

			Pos2.x -= Size.cx;

			pScan->SetPosition(Pos2);
			}
		else {
			pScan->SetPosition(Pos1);
			
			if( pScan->GetProps() & tbgSizeToFit ) {
			
				int nWidth = SizeToFit(pScan);
				
				pScan->SetWidth(nWidth);
				}

			CSize Size = pScan->GetSize();
			
			Pos1.x += Size.cx;
			}
		}
	}
	
int CGadgetBarWnd::SizeToFit(CGadget *pScan)
{
	int nTotal = m_Border.right + pScan->GetRect().left;
	
	while( pScan = pScan->m_pNext ) {
	
		AfxAssert(!(pScan->GetProps() & tbgSizeToFit));
	
		nTotal += pScan->GetSize().cx;
		}

	return Max(GetClientRect().GetWidth() - nTotal, 0);
	}

// End of File
