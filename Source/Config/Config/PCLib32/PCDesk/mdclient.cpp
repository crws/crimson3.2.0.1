
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// MDI Client Window
//

// Dynamic Class

AfxImplementDynamicClass(CMdiClientWnd, CViewWnd);

// Constructor

CMdiClientWnd::CMdiClientWnd(void)
{
	m_pActive = NULL;
	
	m_uCount  = 0;
	}

// Creation

BOOL CMdiClientWnd::Create(CMdiFrameWnd &Parent)
{
	CLIENTCREATESTRUCT Info;
	
	Info.hWindowMenu  = FindWindowMenu(Parent.GetMenu());
	
	Info.idFirstChild = IDM_WINDOW_CHILD;

	DWORD dwStyle     = WS_CHILD | MDIS_ALLCHILDSTYLES | WS_CLIPCHILDREN;
	
	CRect Rect        = Parent.GetClientRect();
	
	return CWnd::Create(dwStyle, Rect, Parent, 0, &Info);
	}
		
// Active Child

CMdiChildWnd * CMdiClientWnd::GetActiveChild(void) const
{
	return m_pActive;
	}

// List Access

CMdiList const & CMdiClientWnd::GetMdiList(void) const
{
	return m_MdiList;
	}
		
// Attributes

HMENU CMdiClientWnd::FindWindowMenu(CMenu &MenuBar) const
{
	if( MenuBar.GetHandle() ) {
		
		int nCount = MenuBar.GetMenuItemCount();
		
		for( int nPos = 0; nPos < nCount; nPos++ ) {
		
			CMenu &Popup = MenuBar.GetSubMenu(nPos);
			
			UINT uID = Popup.GetMenuItemID(0);
			
			if( HIBYTE(uID) == IDM_WINDOW ) {

				return Popup;
				}
			}
		}
	
	return NULL;
	}

// Child Operations

void CMdiClientWnd::ShowChild(CMdiChildWnd &Child)
{
	Child.ShowWindow(SW_SHOW);
	
	ActivateChild(Child);
	
	RefreshParentMenu();
	}

void CMdiClientWnd::ActivateChild(CMdiChildWnd &Child)
{
	SendMessage(WM_MDIACTIVATE, WPARAM(Child.GetHandle()), 0L);
	}

void CMdiClientWnd::DestroyChild(CMdiChildWnd &Child)
{
	SendMessage(WM_MDIDESTROY, WPARAM(Child.GetHandle()));
	}

void CMdiClientWnd::MaximizeChild(CMdiChildWnd &Child)
{
	SendMessage(WM_MDIMAXIMIZE, WPARAM(Child.GetHandle()));
	}

void CMdiClientWnd::RestoreChild(CMdiChildWnd &Child)
{
	SendMessage(WM_MDIRESTORE, WPARAM(Child.GetHandle()));
	}

void CMdiClientWnd::NextChild(CMdiChildWnd &Child)
{
	SendMessage(WM_MDINEXT, WPARAM(Child.GetHandle()), LPARAM(0));
	}

void CMdiClientWnd::PrevChild(CMdiChildWnd &Child)
{
	SendMessage(WM_MDINEXT, WPARAM(Child.GetHandle()), LPARAM(1));
	}

// Frame Operations

void CMdiClientWnd::SetParentMenu(CMenu &Menu)
{
	CWnd &Parent = GetParent();

	Parent.GetMenu().SetExtern(FALSE);
	
	HMENU hMenu = Menu.GetHandle();
		
	HMENU hWnd  = FindWindowMenu(Menu);
	
	SendMessage(WM_MDISETMENU, WPARAM(hMenu), WPARAM(hWnd));
		
	Menu.SetExtern(TRUE);
		
	Parent.DrawMenuBar();
	}

void CMdiClientWnd::RefreshParentMenu(void)
{
	SendMessage(WM_MDISETMENU, 1, 0L);
	
	GetParent().DrawMenuBar();
	}

void CMdiClientWnd::ShowParentEdge(BOOL fEdge)
{
	CMdiFrameWnd &Frame = (CMdiFrameWnd &) GetParent();

	if( !(fEdge == Frame.IsEdgeVisible()) ) {
	
		if( !fEdge ) {
		
			Frame.ShowEdge(fEdge);
			
			Frame.UpdateWindow();
			}
			
		afxThread->LockUpdate(TRUE);
	
		int nDelta = fEdge ? -4 : +4;
		
		CWnd *pWnd = GetWindowPtr(GW_CHILD);
		
		while( pWnd->IsWindow() ) {
		
			if( pWnd->IsKindOf(AfxRuntimeClass(CMdiChildWnd)) ) {
			
				if( pWnd->IsIconic() && pWnd->IsWindowVisible() ) {
				
					CRect Rect = pWnd->GetWindowRect();
					
					ScreenToClient(Rect);
					
					Rect += CPoint(0, nDelta);

					pWnd->MoveWindow(Rect, TRUE);
					}
				}
				
			pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
			}

		afxThread->LockUpdate(FALSE);

		if( fEdge ) {
		
			Frame.ShowEdge(fEdge);
			
			Frame.UpdateWindow();
			}
		}
	}
	
void CMdiClientWnd::ShowParentEdge(void)
{
	if( m_pActive ) {
	
		BOOL fEdge = m_pActive->IsIconic() || !m_pActive->IsZoomed();
		
		ShowParentEdge(fEdge);
	
		return;
		}
		
	ShowParentEdge(TRUE);
	}

// Child Callback

void CMdiClientWnd::ChildNotify(UINT uCode, CMdiChildWnd *pChild)
{
	if( uCode == childActivate ) {
	
		Select(pChild);
	
		ShowParentEdge();
		}
		
	if( uCode == childDeactivate ) {

		if( m_pActive == pChild ) {
		
			Select(NULL);
			}
		}
		
	if( uCode == childCreate ) {
	
		m_uCount++;

		m_MdiList.Append(pChild);
		}

	if( uCode == childDestroy ) {
	
		if( !--m_uCount ) {
		
			Select(NULL);
				
			ShowParentEdge();
			}

		INDEX n = m_MdiList.Find(pChild);

		AfxAssert(!m_MdiList.Failed(n));

		m_MdiList.Remove(n);
		}

	if( uCode == childZoomed ) {
	
		if( m_pActive == pChild ) {
				
			ShowParentEdge();
			}
		}
	}

// Default Class Name

PCTXT CMdiClientWnd::GetDefaultClassName(void) const
{
	return L"MDICLIENT";
	}
		
// Routing Control

BOOL CMdiClientWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pActive ) {

		if( m_pActive->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CViewWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CMdiClientWnd, CViewWnd)
{
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_GETMETRIC)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_CANCELMODE)

	AfxDispatchCommandType(IDM_WINDOW, OnWindowCommand)
	AfxDispatchControlType(IDM_WINDOW, OnWindowControl)

	AfxDispatchGetInfo(0, OnGetInfo)

	AfxMessageEnd(CMdiClientWnd)
	};
		
// Message Handlers

void CMdiClientWnd::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	if( uCode == VK_RETURN ) {
		
		HWND hWnd = HWND(SendMessage(WM_MDIGETACTIVE));

		if( !hWnd ) return;

		CMdiChildWnd &Child = (CMdiChildWnd &) CWnd::FromHandle(hWnd);
			
		if( Child.IsIconic() ) {
		
			if( Child.IsZoomed() )
				Child.ShowWindow(SW_SHOWMAXIMIZED);
			else
				Child.ShowWindow(SW_SHOWNORMAL);
			}
		
		SendMessage(WM_MDIACTIVATE, WPARAM(hWnd));
		}
	}
	
void CMdiClientWnd::OnFocusNotify(UINT uID, CWnd &Child)
{
	SelectActive((CMdiChildWnd *) &Child);
	}

void CMdiClientWnd::OnGetMetric(UINT uCode, CSize &Size)
{
	if( m_pActive && m_pActive->IsZoomed() ) {
	
		LPARAM lParam = LPARAM(&Size);
	
		m_pActive->SendMessage(WM_GETMETRIC, uCode, lParam);
		}
	}

void CMdiClientWnd::OnSetCurrent(BOOL fCurrent)
{
	SetChildCurrent(fCurrent);
	}

void CMdiClientWnd::OnCancelMode(void)
{
	if( m_pActive ) {
	
		m_pActive->SendMessage(WM_CANCELMODE);
		
		return;
		}
	}

// Window Menu Routing

BOOL CMdiClientWnd::OnWindowCommand(UINT uID)
{
	if( uID >= IDM_WINDOW_CHILD ) {
	
		AfxCallDefProc();
		
		return TRUE;
		}

	switch( uID ) {

		case IDM_WINDOW_CASCADE:
			
			OnCmdWindowCascade();
			
			break;
			
		case IDM_WINDOW_TILE:
			
			OnCmdWindowTile();
			
			break;
			
		case IDM_WINDOW_ARRANGE:
			
			OnCmdWindowArrange();
			
			break;
			
		case IDM_WINDOW_CLOSE_ALL:
			
			OnCmdWindowCloseAll();
			
			break;
			
		default:
			return FALSE;
		}
		
	return TRUE;
	}

BOOL CMdiClientWnd::OnWindowControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_WINDOW_CHILD ) {
	
		Src.EnableItem(TRUE);
		
		Src.CheckItem(m_pActive->GetID() == uID);
		
		return TRUE;
		}
	
	switch( uID ) {

		case IDM_WINDOW_CASCADE:
			
			Src.EnableItem(CanWindowCascade());
			
			break;
			
		case IDM_WINDOW_TILE:
			
			Src.EnableItem(CanWindowTile());
			
			break;
			
		case IDM_WINDOW_ARRANGE:
			
			Src.EnableItem(CanWindowArrange());
			
			break;
			
		case IDM_WINDOW_CLOSE_ALL:
			
			Src.EnableItem(CanWindowCloseAll());
			
			break;
			
		default:
			return FALSE;
		}
		
	return TRUE;
	}

// Window Menu Handlers

void CMdiClientWnd::OnCmdWindowCascade(void)
{
	SendMessage(WM_MDICASCADE);
	}

void CMdiClientWnd::OnCmdWindowTile(void)
{
	SendMessage(WM_MDITILE);
	}

void CMdiClientWnd::OnCmdWindowArrange(void)
{
	SendMessage(WM_MDIICONARRANGE);
	}

void CMdiClientWnd::OnCmdWindowCloseAll(void)
{
	for(;;) {
	
		UINT uLastCount = m_uCount;
		
		CWnd *pWnd = GetWindowPtr(GW_CHILD);

		for(;;) {
					
			if( pWnd == NULL || pWnd->IsWindow() == FALSE ) {

				return;
				}
			
			if( pWnd->IsKindOf(AfxRuntimeClass(CMdiChildWnd)) ) {

				break;
				}
		
			pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
			}
		
		pWnd->SendMessage(WM_SYSCOMMAND, SC_CLOSE);

		if( m_uCount == uLastCount ) {

			break;
			}
		}
	}

// Window Menu Permissions

BOOL CMdiClientWnd::CanWindowCascade(void) const
{
	if( m_uCount ) {
	
		CWnd *pWnd = GetWindowPtr(GW_CHILD);
		
		while( pWnd && pWnd->IsWindow() ) {
		
			if( pWnd->IsKindOf(AfxRuntimeClass(CMdiChildWnd)) ) {

				if( !pWnd->IsIconic() ) {

					return TRUE;
					}
				}
				
			pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
			}
		}
	
	return FALSE;
	}

BOOL CMdiClientWnd::CanWindowTile(void) const
{
	return CanWindowCascade();
	}

BOOL CMdiClientWnd::CanWindowArrange(void) const
{
	if( m_uCount ) {
	
		CWnd *pWnd = GetWindowPtr(GW_CHILD);
		
		while( pWnd && pWnd->IsWindow() ) {
		
			if( pWnd->IsKindOf(AfxRuntimeClass(CMdiChildWnd)) ) {

				if( pWnd->IsIconic() ) {

					return TRUE;
					}
				}
				
			pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
			}
		}
	
	return FALSE;
	}

BOOL CMdiClientWnd::CanWindowCloseAll(void) const
{
	return m_uCount > 0;
	}

// Command Information

BOOL CMdiClientWnd::OnGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= IDM_WINDOW_CHILD && uID < IDM_WINDOW_MORE ) {
	
		CWnd    &Wnd = GetDlgItem(uID);
		
		CString Type = Wnd.GetWindowText();
	
		Info.m_Prompt.Printf(L"Activate the %s window.", Type);
		
		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

BOOL CMdiClientWnd::SelectActive(CMdiChildWnd *pChild)
{
	if( Select(pChild) ) {
	
		ActivateChild(*pChild);
		
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CMdiClientWnd::Select(CMdiChildWnd *pChild)
{
	if( !(m_pActive == pChild) ) {
	
		SetChildCurrent(FALSE);
	
		m_pActive = pChild;

		SetChildCurrent(TRUE);
			
		afxMainWnd->SendMessage(WM_UPDATEUI);
		
		return TRUE;
		}
		
	return FALSE;
	}

void CMdiClientWnd::SetChildCurrent(BOOL fCurrent)
{
	if( m_pActive ) {
	
		CViewWnd *pView = m_pActive->GetView();
		
		if( pView ) {

			pView->SetCurrent(fCurrent);
			}
		}
	}

// End of File
