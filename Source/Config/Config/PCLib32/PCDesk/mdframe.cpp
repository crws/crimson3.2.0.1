
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// MDI Frame Window
//

// Dynamic Class

AfxImplementDynamicClass(CMdiFrameWnd, CMainWnd);

// Constructor

CMdiFrameWnd::CMdiFrameWnd(void)
{
	m_pClient = New CMdiClientWnd;
	
	m_pView   = m_pClient;
	}

// Active Child

CMdiChildWnd * CMdiFrameWnd::GetActiveChild(void) const
{
	return m_pClient->GetActiveChild();
	}

// List Access

CMdiList const & CMdiFrameWnd::GetMdiList(void) const
{
	return m_pClient->GetMdiList();
	}
		
// Attributes

CMdiClientWnd & CMdiFrameWnd::GetClientWnd(void) const
{
	return *m_pClient;
	}

// Child Operations

void CMdiFrameWnd::ShowChild(CMdiChildWnd &Child)
{
	m_pClient->ShowChild(Child);
	}

void CMdiFrameWnd::ActivateChild(CMdiChildWnd &Child)
{
	m_pClient->ActivateChild(Child);
	}

void CMdiFrameWnd::DestroyChild(CMdiChildWnd &Child)
{
	m_pClient->DestroyChild(Child);
	}

void CMdiFrameWnd::MaximizeChild(CMdiChildWnd &Child)
{
	m_pClient->MaximizeChild(Child);
	}

void CMdiFrameWnd::RestoreChild(CMdiChildWnd &Child)
{
	m_pClient->RestoreChild(Child);
	}

void CMdiFrameWnd::NextChild(CMdiChildWnd &Child)
{
	m_pClient->NextChild(Child);
	}

void CMdiFrameWnd::PrevChild(CMdiChildWnd &Child)
{
	m_pClient->PrevChild(Child);
	}

// Frame Operations
		
void CMdiFrameWnd::SetMenu(CMenu &Menu)
{
	if( m_pClient->GetHandle() ) {
	
		m_pClient->SetParentMenu(Menu);
		
		return;
		}

	CWnd::SetMenu(Menu);
	}

void CMdiFrameWnd::RefreshMenu(void)
{
	m_pClient->RefreshParentMenu();
	}
		
// Message Procedures

LRESULT CMdiFrameWnd::DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( m_pfnSuper ) {

		AfxAssert(IsWindow());

		return CallWindowProc(m_pfnSuper, m_hWnd, uMessage, wParam, lParam);
		}
		
	return DefFrameProc(m_hWnd, m_pClient->GetHandle(), uMessage, wParam, lParam);
	}

// View Creation

void CMdiFrameWnd::OnCreateView(void)
{
	m_pClient->Create(ThisObject);
	}
	
// Message Map

AfxMessageMap(CMdiFrameWnd, CMainWnd)
{
	AfxDispatchMessage(WM_COMMAND)

	AfxMessageEnd(CMdiFrameWnd)
	};

// Message Handlers

BOOL CMdiFrameWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( HIBYTE(uID) == IDM_WINDOW && uID >= IDM_WINDOW_CHILD ) {
		
		AfxCallDefProc();
		
		return TRUE;
		}
		
	return FALSE;
	}

// End of File
