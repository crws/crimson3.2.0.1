
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Standard Dialog with Toolbar
//

// Runtime Class

AfxImplementRuntimeClass(CStdToolbarDialog, CStdDialog);

// Constructor

CStdToolbarDialog::CStdToolbarDialog(void)
{
	m_pTB = New CToolbarWnd(0);
	}

// Destructor

CStdToolbarDialog::~CStdToolbarDialog(void)
{
	}

// Message Map

AfxMessageMap(CStdToolbarDialog, CStdDialog)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxMessageEnd(CStdDialog)
	};

// Message Handlers

UINT CStdToolbarDialog::OnCreate(CREATESTRUCT &Create)
{
	SendMessage(WM_UPDATEUI);

	m_nPadTop = m_pTB->GetHeight();

	m_Loader.SetOrigin(CPoint(0, m_nPadTop));

	CStdDialog::OnCreate(Create);

	CRect Rect  = GetClientRect();

	Rect.bottom = Rect.top + m_nPadTop;

	m_pTB->Create(Rect, ThisObject);
	
	m_pTB->PollGadgets();

	m_pTB->ShowWindow(SW_SHOW);

	return 0;
	}

void CStdToolbarDialog::OnGoingIdle(void)
{
	if( m_pTB->IsWindow() ) {

		m_pTB->PollGadgets();
		}
	}

// End of File
