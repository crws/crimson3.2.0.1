
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Toolbar Window
//

// Runtime Class

AfxImplementRuntimeClass(CToolbarWnd, CGadgetBarWnd);

// Static Data

UINT CToolbarWnd::timerSelect = AllocTimerID();

// Constructor

CToolbarWnd::CToolbarWnd(UINT uFlags)
{
	m_uFlags = uFlags;

	m_pDrop  = NULL;

	int nGap = (m_uFlags & barTight) ? 1 : 2;

	SetRect(m_Border, nGap, nGap, nGap, nGap);
	}

// Attributes

int CToolbarWnd::GetWidth(void) const
{
	int xPos = 0;

	xPos += m_Border.left;
	
	for( CGadget *pScan = m_pHead; pScan; pScan = pScan->m_pNext ) {

		CSize Size = pScan->GetSize();
		
		xPos += Size.cx;
		}

	xPos += m_Border.right;

	return xPos;
	}

// IUnknown

HRESULT CToolbarWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CToolbarWnd::AddRef(void)
{
	return 1;
	}

ULONG CToolbarWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CToolbarWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( CanTakeDrop() ) {

		CPoint Pos(pt.x, pt.y);

		SetDrop(Pos);
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CToolbarWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( CanTakeDrop() ) {

		CPoint Pos(pt.x, pt.y);

		SetDrop(Pos);
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CToolbarWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( CanTakeDrop() ) {

		SetDrop(NULL);
		}

	return S_OK;
	}

HRESULT CToolbarWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( CanTakeDrop() ) {

		SetDrop(NULL);
		}

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Message Map

AfxMessageMap(CToolbarWnd, CGadgetBarWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_TIMER)

	AfxMessageEnd(CToolbarWnd)
	};

// Message Handlers

void CToolbarWnd::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
	}

void CToolbarWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_uFlags & barFlat ) {

		if( m_uFlags & barDark ) {

			DC.FillRect( GetClientRect(),
				     afxBrush(3dFace)
				     );
			}
		else {
			DC.FillRect( GetClientRect(),
				     afxBrush(Blue2)
				     );
			}
		}
	else {
		if( m_uFlags & barDark ) {

			DC.GradVert( GetClientRect(),
				     afxColor(Blue2),
				     afxColor(Blue4)
				     );
			}
		else {
			DC.GradVert( GetClientRect(),
				     afxColor(Blue1),
				     afxColor(Blue3)
				     );
			}
		}

	if( !PaintGadgets(DC) ) {

		DC.Select(afxFont(Marlett1));

		DC.SetBkMode(TRANSPARENT);

		CRect Rect  = GetClientRect();

		Rect.right += 2;

		DC.DrawText(L"\x034", Rect, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

		DC.Deselect();
		}
	}

void CToolbarWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( m_uFlags & barDouble ) {

		afxMainWnd->SendMessage(WM_CANCELMODE);

		return;
		}

	CGadgetBarWnd::OnLButtonDblClk(uFlags, Pos);
	}

void CToolbarWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == timerSelect ) {

		if( m_pDrop ) {

			CPoint Pos = GetCursorPos();

			ScreenToClient(Pos);

			if( m_pDrop->OnMouseDown(Pos) ) {

				m_pDrop->OnMouseUp(Pos, TRUE);
				}
			}

		KillTimer(uID);
		}

	CGadgetBarWnd::OnTimer(uID, pfnProc);
	}

// Implementation

BOOL CToolbarWnd::CanTakeDrop(void)
{
	if( m_uFlags & barDrop ) {

		return !IsSemiModal();
		}

	return FALSE;
	}

void CToolbarWnd::SetDrop(CPoint Pos)
{
	ScreenToClient(Pos);

	for( CGadget *pScan = m_pHead; pScan; pScan = pScan->m_pNext ) {
			
		if( pScan->GetRect().PtInRect(Pos) ) {

			SetDrop(pScan);

			return;
			}
		}

	SetDrop(NULL);
	}

void CToolbarWnd::SetDrop(CGadget *pDrop)
{
	if( m_pDrop != pDrop ) {

		if( m_pDrop ) {

			m_pDrop->ClearFlag(MF_HILITE);
			}

		if( (m_pDrop = pDrop) ) {

			SetTimer (timerSelect, 300);
			}
		else
			KillTimer(timerSelect);

		if( m_pDrop ) {

			m_pDrop->SetFlag(MF_HILITE);
			}

		}
	}

// End of File
