
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Dynamic Class

AfxImplementDynamicClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestFrameWnd;

	pWnd->Create( CString(IDS_APP_CAPTION),
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      CRect(240, 200, 1100, 900),
		      AfxNull(CWnd),
		      CMenu(L"HeadMenu"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());
	
	afxThread->SetStatusText(L"Hello World");

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:
			Src.EnableItem(TRUE);
			break;
		
		default:
			return FALSE;
		}
		
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestFrameWnd, CMainWnd);

// Constructor

CTestFrameWnd::CTestFrameWnd(void)
{
	m_Accel.Create(L"TestMenu");

	m_pView = New CTestViewWnd;

	ShowEdge(FALSE);
	}

// Destructor

CTestFrameWnd::~CTestFrameWnd(void)
{
	}

// Interface Control

void CTestFrameWnd::OnUpdateInterface(void)
{
	CMainWnd::OnUpdateInterface();
	}

// Message Map

AfxMessageMap(CTestFrameWnd, CMainWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SHOWUI)

	AfxDispatchControlType(IDM_FILE, OnCommandControl)
	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)

	AfxMessageEnd(CTestFrameWnd)
	};

// Accelerators

BOOL CTestFrameWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CTestFrameWnd::OnPostCreate(void)
{
	CMainWnd::OnPostCreate();

	afxMainWnd->PostMessage(WM_UPDATEUI);
	}

void CTestFrameWnd::OnShowUI(BOOL fShow)
{
	}

// Command Handlers

BOOL CTestFrameWnd::OnCommandExecute(UINT uID)
{
	return FALSE;
	}

BOOL CTestFrameWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test View Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestViewWnd, CViewWnd);

// Constructor

CTestViewWnd::CTestViewWnd(void)
{
	}

// Destructor

CTestViewWnd::~CTestViewWnd(void)
{
	}

// Message Map

AfxMessageMap(CTestViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_RBUTTONDOWN)

	AfxMessageEnd(CTestViewWnd)
	};

// Message Handlers

void CTestViewWnd::OnPaint(void)
{
	CPaintDC(ThisObject).FillRect(GetClientRect(), afxBrush(WHITE));
	}

void CTestViewWnd::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	CColorPickerHost *pHost = New CColorPickerHost;

	pHost->Create( WS_CHILD,
		       CRect(0, 0, 1, 1),
		       ThisObject,
		       1111,
		       NULL
		       );

	ClientToScreen(Pos);

	pHost->Select(Pos);

	pHost->DestroyWindow(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Color Picker Host
//

// Dynamic Class

AfxImplementDynamicClass(CColorPickerHost, CMenuWnd);

// Static Data

CColorPickerHost * CColorPickerHost::m_pThis = NULL;

// Constructor

CColorPickerHost::CColorPickerHost(void)
{
	m_pThis = this;
	}

// Destructor

CColorPickerHost::~CColorPickerHost(void)
{
	m_pThis = NULL;
	}

// Operations

void CColorPickerHost::Select(CPoint Pos)
{
	CMenu Menu(L"ColorMenu");

	Menu.MakeOwnerDraw(FALSE);

	m_pMenu = &Menu.GetSubMenu(0);

	m_fSpec = FALSE;

	m_hHook = SetWindowsHookEx( WH_MSGFILTER,
			            FARPROC(MessageProc),
				    afxModule->GetAppInstance(),
				    GetCurrentThreadId()
				    );

	m_pMenu->TrackPopupMenu(TPM_LEFTALIGN, Pos, ThisObject);

	UnhookWindowsHookEx(m_hHook);

	Menu.FreeOwnerDraw();
	}

// Callback Hook

void CColorPickerHost::TrackPos(CPoint Pos)
{
	CWnd  &Wnd = CWnd::FromHandle(FindWindow(L"#32768", NULL));

	CRect Rect = Wnd.GetWindowRect();

	m_Pos      = Pos - Rect.GetTopLeft();

	Wnd.Invalidate(FALSE);
	}

// Message Map

AfxMessageMap(CColorPickerHost, CMenuWnd)
{
	AfxDispatchMessage(WM_MENUSELECT)
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxDispatchGetInfoType(0xCC, OnColorGetInfo)
	AfxDispatchControlType(0xCC, OnColorControl)
	AfxDispatchCommandType(0xCC, OnColorExecute)

	AfxMessageEnd(CColorPickerHost)
	};

// Message Handlers

void CColorPickerHost::OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu)
{
	if( uFlags & MF_OWNERDRAW ) {

		if( uID == 0xCCF0 ) {

			m_fSpec = TRUE;

			TrackPos(GetCursorPos());
			}
		else
			m_fSpec = FALSE;
		}

	CMenuWnd::OnMenuSelect(uID, uFlags, Menu);
	}

void CColorPickerHost::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	if( Item.itemID == 0xCCF0 ) {

		Item.itemWidth  = 200;
		
		Item.itemHeight = 22;

		return;
		}

	CMenuWnd::OnMeasureItem(uID, Item);
	}

void CColorPickerHost::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
        if( Item.CtlType == ODT_MENU ) {
		
		CRect Rect = Item.rcItem;
	
		CSize Size = Rect.GetSize();

		CDC       MenuDC(Item.hDC);

		CMemoryDC WorkDC(MenuDC);

		CBitmap   WorkBM(MenuDC, Size);

		WorkDC.Select(WorkBM);

		DRAWITEMSTRUCT Copy = Item;

		Copy.hDC      = WorkDC;
	
		Copy.rcItem   = CRect(Size);

		CPoint Corner = Rect.GetTopLeft();

		CColor Color  = MenuDC.GetPixel(Corner);

		WorkDC.FillRect(Size, CBrush(Color));

		if( Item.itemID == 0xCCF0 ) {

			DrawSpecial(WorkDC, Copy);
			}
		else
			DrawMenu(WorkDC, Copy);

		MenuDC.BitBlt(Rect, WorkDC, CPoint(0, 0), SRCCOPY);

		WorkDC.Deselect();
		}
	}

// Command Handlers

BOOL CColorPickerHost::OnColorGetInfo(UINT uID, CCmdInfo &Info)
{
	return TRUE;
	}

BOOL CColorPickerHost::OnColorControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

BOOL CColorPickerHost::OnColorExecute(UINT uID)
{
	return TRUE;
	}

// Implementation

void CColorPickerHost::DrawSpecial(CDC &DC, DRAWITEMSTRUCT &Item)
{
	CMenuInfo *pInfo = (CMenuInfo *) Item.itemData;

	DC.Select(afxFont(Status));

	DC.SetBkMode(TRANSPARENT);

	DrawColors(DC, pInfo, Item.rcItem, Item.itemState);

	DC.Deselect();
	}

void CColorPickerHost::DrawColors(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState)
{
	CRect Work = Rect;

	Work.right = Work.left + 22;

	DC.GradHorz(Work, afxColor(Blue1), afxColor(Blue3));

	Work.left  = Work.right;

	Work.right = Rect.right;

	DrawStdBack(DC, Work, Rect, uState);

	DC.TextOut(Work.left + 4, Work.top + 4, CPrintf(L"%u %u", m_Pos));
	}

// Hook Procedure

LRESULT CALLBACK CColorPickerHost::MessageProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if( m_pThis->m_fSpec ) {

		MSG *pMsg = (MSG *) lParam;

		if( pMsg->message == WM_MOUSEMOVE ) {

			CPoint Pos = CPoint(pMsg->lParam);

			m_pThis->TrackPos(Pos);
			}
		}

	return CallNextHookEx(m_pThis->m_hHook, nCode, wParam, lParam);
	}

// End of File
