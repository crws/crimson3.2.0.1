
//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP
	
#define	INCLUDE_TESTING_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTestApp;
class CTestFrameWnd;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CTestApp : public CThread
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestApp(void);

		// Destructor
		~CTestApp(void);

	protected:
		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

class CTestFrameWnd : public CMainWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestFrameWnd(void);

		// Destructor
		~CTestFrameWnd(void);

	protected:
		// Data Members
		CAccelerator m_Accel;

           	// Interface Control
		void OnUpdateInterface(void);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnShowUI(BOOL fShow);
           
		// Command Handlers
		BOOL OnCommandGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);

		// Debug
		PCTXT MakeTestText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test View Window
//

class CTestViewWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestViewWnd(void);

		// Destructor
		~CTestViewWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPaint(void);
		void OnRButtonDown(UINT uFlags, CPoint Pos);
	};

//////////////////////////////////////////////////////////////////////////
//
// Color Picker Host
//

class CColorPickerHost : public CMenuWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CColorPickerHost(void);

		// Destructor
		~CColorPickerHost(void);

		// Operations
		void Select(CPoint Pos);

		// Callback Hook
		void TrackPos(CPoint Pos);

	protected:
		// Static Data
		static CColorPickerHost *m_pThis;

		// Data Members
		CMenu * m_pMenu;
		BOOL    m_fSpec;
		HHOOK   m_hHook;
		CPoint  m_Pos;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu);
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item);

		// Command Handlers
		BOOL OnColorGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnColorControl(UINT uID, CCmdSource &Src);
		BOOL OnColorExecute(UINT uID);

		// Implementation
		void DrawSpecial(CDC &DC, DRAWITEMSTRUCT &Item);
		void DrawColors(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState);

		// Hook Procedure
		static LRESULT CALLBACK MessageProc(int nCode, WPARAM wParam, LPARAM lParam);
	};

// End of File

#endif
