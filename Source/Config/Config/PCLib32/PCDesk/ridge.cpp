
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Ridge Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CRidgeGadget, CGadget);
		
// Constructor

CRidgeGadget::CRidgeGadget(int nWidth)
{
	m_Size = CSize(nWidth ? nWidth : 4, 0);
	}

// Overridables

void CRidgeGadget::OnPaint(CDC &DC)
{
	CRect Rect = m_Rect;

	Rect.top    += 4;

	Rect.bottom -= 4;

	Rect.left   += 1;
	
	Rect.right  -= 1;
	
	DC.DrawLeftEdge (Rect, 1, afxBrush(3dShadow));
	
	DC.DrawRightEdge(Rect, 1, afxBrush(3dHighlight));
	}

// End of File
