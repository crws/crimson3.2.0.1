
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Drag Helper
//

// Runtime Class

AfxImplementRuntimeClass(CDragHelper, CObject);

// Constructor

CDragHelper::CDragHelper(void)
{
	CoCreateInstance( CLSID_DragDropHelper,
			  NULL,
			  CLSCTX_INPROC_SERVER,
			  IID_IDragSourceHelper,
			  (void **) &m_pHelp
			  );
	}

// Destructor

CDragHelper::~CDragHelper(void)
{
	m_pHelp->Release();
	}

// Operations

void CDragHelper::AddImage(IDataObject *pData, CBitmap &Bitmap, CSize Size, CSize Offset)
{
	SHDRAGIMAGE Image;

	Image.sizeDragImage.cx = Size.cx;

	Image.sizeDragImage.cy = Size.cy;

	Image.ptOffset.x       = Offset.cx;

	Image.ptOffset.y       = Offset.cy;

	Image.hbmpDragImage    = Bitmap.GetHandle();

	Image.crColorKey       = afxColor(MAGENTA);

	Bitmap.Detach(FALSE);

	m_pHelp->InitializeFromBitmap(&Image, pData);
	}

void CDragHelper::AddEmpty(IDataObject *pData)
{
	// NOTE -- Empty image works around a Windows bug whereby
	// we sometimes get the image from the last drag operation.

	AddImage(pData, CBitmap(1,1,1,1), CSize(0,0), CSize(0,0));
	}

//////////////////////////////////////////////////////////////////////////
//
// Drop Helper
//

// Runtime Class

AfxImplementRuntimeClass(CDropHelper, CObject);

// Static Data

CDropHelper * CDropHelper::m_pHead = NULL;

CDropHelper * CDropHelper::m_pTail = NULL;

// Constructor

CDropHelper::CDropHelper(void)
{
	CoCreateInstance( CLSID_DragDropHelper,
			  NULL,
			  CLSCTX_INPROC_SERVER,
			  IID_IDropTargetHelper,
			  (void **) &m_pHelp
			  );

	m_hWnd   = NULL;

	m_pDrop  = NULL;

	m_fProxy = FALSE;
	}

// Destructor

CDropHelper::~CDropHelper(void)
{
	if( m_hWnd ) {

		AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);

		m_pHelp->Release();

		RevokeDragDrop(m_hWnd);
		}
	}

// Binding

void CDropHelper::Bind(HWND hWnd, IDropTarget *pDrop)
{
	RegisterDragDrop(m_hWnd = hWnd, m_pDrop = pDrop);

	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Attributes

BOOL CDropHelper::IsProxyDrop(void) const
{
	return m_fProxy;
	}

// Operations

void CDropHelper::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	if( pt.x == -1 && pt.y == -1 ) {

		m_fProxy = TRUE;

		return;
		}

	m_fProxy = FALSE;

	POINT rp = { pt.x, pt.y };

	m_pHelp->DragEnter(m_hWnd, pData, &rp, *pEffect);
	}

void CDropHelper::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	if( !m_fProxy ) {

		POINT rp = { pt.x, pt.y };

		m_pHelp->DragOver(&rp, *pEffect);
		}
	}

void CDropHelper::DragLeave(void)
{
	if( !m_fProxy ) {

		m_pHelp->DragLeave();
		}

	m_fProxy = FALSE;
	}

void CDropHelper::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	if( !m_fProxy ) {

		POINT rp = { pt.x, pt.y };

		m_pHelp->Drop(pData, &rp, *pEffect);
		}

	m_fProxy = FALSE;
	}

void CDropHelper::Show(BOOL fShow)
{
	m_pHelp->Show(fShow);
	}

// Location

IDropTarget * CDropHelper::Find(CPoint Pos)
{
	HWND         hFind = WindowFromPoint(Pos);

	CDropHelper *pScan = m_pHead;

	while( pScan ) {

		if( pScan->m_hWnd == hFind ) {

			return pScan->m_pDrop;
			}

		pScan = pScan->m_pNext;
		}

	return NULL;
	}

// End of File
