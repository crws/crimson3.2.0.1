
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Recent File List
//

// Dynamic Class

AfxImplementDynamicClass(CRecentList, CObject);

// Constructor

CRecentList::CRecentList(void)
{
	m_BaseID = IDM_FILE_RECENT;

	m_uLimit = 8;
	}

// Attributes

UINT CRecentList::GetBaseID(void) const
{
	return m_BaseID;
	}

UINT CRecentList::GetCount(void) const
{
	return m_List.GetCount();
	}

CFilename CRecentList::GetFile(UINT uIndex) const
{
	return m_List[uIndex];
	}

CFilename CRecentList::GetFileFromID(UINT ID) const
{
	return GetFile(ID - m_BaseID);
	}

UINT CRecentList::GetLimit(void) const
{
	return m_uLimit;
	}

// Operations

void CRecentList::SetBaseID(UINT BaseID)
{
	m_BaseID = BaseID;
	}

void CRecentList::SetLimit(UINT uLimit)
{
	m_uLimit = uLimit;

	if( m_uLimit > m_List.GetCount() ) {

		UINT uCount = m_List.GetCount() - m_uLimit;

		m_List.Remove(m_uLimit - 1, uCount);
		}
	}

void CRecentList::AddFile(PCTXT pFile)
{
	UINT uCount = m_List.GetCount();
	
	for( UINT uIndex = 0; uIndex < uCount; uIndex++ ) {
	
		if( m_List[uIndex] == pFile ) {
		
			m_List.Remove(uIndex);
			
			m_List.Insert(0, pFile);
			
			return;
			}
		}
	
	if( uCount == m_uLimit ) {

		m_List.Remove(uCount - 1);
		}
		
	m_List.Insert(0, pFile);
	}

void CRecentList::RemoveFile(PCTXT pFile)
{
	UINT uCount = m_List.GetCount();
	
	for( UINT uIndex = 0; uIndex < uCount; uIndex++ ) {
	
		if( m_List[uIndex] == pFile ) {
		
			m_List.Remove(uIndex);
			
			return;
			}
		}
	}

// Persistance

void CRecentList::LoadList(PCTXT pName)
{
	CRegKey Key = FindKey(pName);
	
	UINT uLimit = Key.GetValue(L"Limit", 8u);
	
	UINT uCount = Key.GetValue(L"Count", 0u);
	
	m_List.Empty();
	
	for( UINT uIndex = 0; uIndex < Min(uLimit, uCount); uIndex++ ) {
	
		CString File = CPrintf(L"File %u", 1 + uIndex);
	
		CString Name = Key.GetValue(File, L"");
		
		if( !Name.IsEmpty() ) {
		
			m_List.Append(Name);
			}
		}
		
	m_uLimit = uLimit;
	}

void CRecentList::SaveList(PCTXT pName)
{
	CRegKey Key = FindKey(pName);
	
	Key.SetValue(L"Count", m_List.GetCount());
		
	Key.SetValue(L"Limit", m_uLimit);

	for( UINT uIndex = 0; uIndex < m_List.GetCount(); uIndex++ ) {
	
		CString File = CPrintf(L"File %u", 1 + uIndex);

		CString Name = m_List[uIndex];
		
		Key.SetValue(File, Name);
		}
	}

// Menu Operations

void CRecentList::UpdateMenu(CMenu &Menu, UINT After)
{
	if( Remove(Menu, After) ) {

		Append(Menu, After);
		}
	}

// Implementation

BOOL CRecentList::Remove(CMenu &Menu, UINT After)
{
	UINT uCount = Menu.GetMenuItemCount();

	UINT uIndex = 0;

	if( After ) {

		for( uIndex = uCount - 1; uIndex; uIndex-- ) {

			if( Menu.GetMenuItemID(uIndex) == After ) {

				break;
				}
			}

		if( !uIndex ) {

			return FALSE;
			}

		uIndex++;
		}

	for( UINT uDelete = uIndex; uDelete < uCount; uDelete++ ) {

		Menu.FreeOwnerDraw(uIndex);

		Menu.DeleteMenu(uIndex, MF_BYPOSITION);
		}

	return TRUE;
	}

void CRecentList::Append(CMenu &Menu, UINT After)
{
	UINT uCount = m_List.GetCount();

	if( After ) {
		
		if( uCount ) {

			Menu.AppendSeparator();

			Menu.MakeOwnerDraw(-1, FALSE);
			}
		}

	for( UINT uIndex = 0; uIndex < uCount; uIndex++ ) {
	
		CString Text = m_List[uIndex];
		
		if( Text.GetLength() > 40 ) {
		
			CString Left = Text.Left(3) + L"...";
			
			UINT uPos = Text.Right(35).Find('\\');
			
			Text = Left + Text.Right(35 - uPos);
			}
	
		Text = CPrintf(L"&%u ", 1 + uIndex) + Text;
		
		Menu.AppendMenu(0, IDM_FILE_RECENT + uIndex, Text);

		Menu.MakeOwnerDraw(-1, FALSE);
		}
	}

CRegKey CRecentList::FindKey(PCTXT pName)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"PCDesk");
	
	Key.MoveTo(L"Recent Files");

	if( pName ) {
		
		Key.MoveTo(pName);
		}

	return Key;
	}

// End of File
