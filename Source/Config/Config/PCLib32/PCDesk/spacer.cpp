
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Spacer Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CSpacerGadget, CGadget);
		
// Constructor

CSpacerGadget::CSpacerGadget(void)
{
	m_Size = CSize(4, 0);
	}

CSpacerGadget::CSpacerGadget(int xSize)
{
	m_Size = CSize(xSize, 0);
	}

CSpacerGadget::CSpacerGadget(int xSize, int ySize)
{
	m_Size = CSize(xSize, ySize);
	}

// Overridables

void CSpacerGadget::OnPaint(CDC &DC)
{
	}

// End of File
