
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCDESK_HXX
	
#define	INCLUDE_PCDESK_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdialog.hxx>

//////////////////////////////////////////////////////////////////////////
//
// Menu Commands
//

#define	IDM_VIEW_TAB_1		0x8380
#define	IDM_VIEW_TAB_2		0x8381
#define	IDM_VIEW_TAB_3		0x8382
#define	IDM_VIEW_TAB_4		0x8383
#define	IDM_VIEW_TAB_5		0x8384
#define	IDM_VIEW_TAB_6		0x8385
#define	IDM_VIEW_TAB_7		0x8386
#define	IDM_VIEW_TAB_8		0x8387
#define	IDM_VIEW_TAB_9		0x8388
#define	IDM_VIEW_TAB_NEXT	0x8390
#define	IDM_VIEW_TAB_PREV	0x8391
#define	IDM_VIEW_SHOW_LEFT	0x8392
#define	IDM_VIEW_SHOW_RIGHT	0x8393

#define	IDM_HELP		0x86
#define	IDM_HELP_MENU_TITLE	0x8600
#define	IDM_HELP_BUTTON		0x8601
#define	IDM_HELP_CONTENTS	0x8602
#define	IDM_HELP_SEARCH		0x8603
#define	IDM_HELP_SUPPORT	0x8604
#define	IDM_HELP_ON_HELP	0x8605
#define	IDM_HELP_ABOUT		0x8606
#define	IDM_HELP_BALLOON_OFF	0x8607
#define	IDM_HELP_BALLOON_SOFT	0x8608
#define	IDM_HELP_BALLOON_HARD	0x8609
#define	IDM_HELP_BALLOON_MOD	0x860A
#define	IDM_HELP_BALLOON_NOW	0x860B

#define	IDM_ITEM		0x87
#define	IDM_ITEM_TITLE		0x8700
#define	IDM_ITEM_RENAME		0x8701
#define	IDM_ITEM_DELETE		0x8702
#define	IDM_ITEM_MOVE_UP	0x8703
#define	IDM_ITEM_MOVE_DOWN	0x8704
#define	IDM_ITEM_NEW_FOLDER	0x8705
#define	IDM_ITEM_EXPAND		0x8706
#define	IDM_ITEM_FIND		0x8707
#define	IDM_ITEM_LOCK		0x8708
#define	IDM_ITEM_NEW		0x8709
#define	IDM_ITEM_NEW_NAMED	0x870A
#define	IDM_ITEM_SYNC		0x870B
#define	IDM_ITEM_SORT_UP	0x870C
#define	IDM_ITEM_SORT_DOWN	0x870D
#define	IDM_ITEM_USAGE		0x870E
#define	IDM_ITEM_WATCH		0x870F
#define	IDM_ITEM_PRIVATE	0x8710

#define	IDM_GO			0x88
#define	IDM_GO_TITLE		0x8800
#define	IDM_GO_NEXT_1		0x8801
#define	IDM_GO_PREV_1		0x8802
#define	IDM_GO_BACK		0x8803
#define	IDM_GO_FORWARD		0x8804
#define	IDM_GO_NEXT_CAT		0x8805
#define	IDM_GO_PREV_CAT		0x8806
#define	IDM_GO_NEXT_2		0x8807
#define	IDM_GO_PREV_2		0x8808
#define	IDM_GO_SELECT		0x8840
#define	IDM_GO_REFERENCE	0x8880

//////////////////////////////////////////////////////////////////////////
//
// Status Pane Identifiers
//

#define	IDP_PRIMARY		100
#define	IDP_CAPITAL		101
#define	IDP_NUMLOCK		102
#define	IDP_OVERWRITE		103
#define	IDP_USERNAME		104

//////////////////////////////////////////////////////////////////////////
//
// View Window Identifiers
//

#define	IDVIEW			100
#define	IDTABS			200

// End of File

#endif
