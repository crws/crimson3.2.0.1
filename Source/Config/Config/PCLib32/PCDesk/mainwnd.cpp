
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Main Application Window
//

// Dynamic Class

AfxImplementDynamicClass(CMainWnd, CFrameWnd);

// Static Data

UINT CMainWnd::timerLocks  = AllocTimerID();

UINT CMainWnd::timerPrompt = AllocTimerID();

UINT CMainWnd::timerUpdate = AllocTimerID();

// Constructor

CMainWnd::CMainWnd(void)
{
	m_fSemi    = FALSE;

	m_pSemi    = NULL;

	m_uBalloon = 0;

	m_pTB      = New CToolbarWnd(0);

	m_pSB      = New CStatusWnd;

	m_fMenu    = FALSE;
	
	m_fEdge    = TRUE;
	
	m_fUpdate  = FALSE;

	m_fIconic  = FALSE;
	
	m_Recent.LoadList();
	
	GetConfig();
	}

// Destructor

CMainWnd::~CMainWnd(void)
{
	m_Recent.SaveList();
	}

// Attributes

BOOL CMainWnd::IsMenuActive(void) const
{
	return m_fMenu;
	}
	
BOOL CMainWnd::IsEdgeVisible(void) const
{
	return m_fEdge;
	}

BOOL CMainWnd::IsSemiModal(void) const
{
	return m_fSemi;
	}

CDialog * CMainWnd::GetSemiModal(void) const
{
	return m_pSemi;
	}

CString CMainWnd::GetStatus(void) const
{
	return m_pSB->GetGadget(IDP_PRIMARY)->GetText();
	}	

UINT CMainWnd::GetBalloonMode(void) const
{
	return m_uBalloon;
	}

BOOL CMainWnd::AllowGhostBar(void) const
{
	return m_fShowGB;
	}

// Operations

void CMainWnd::SetSemiModal(CDialog *pSemi)
{
	BOOL fSemi = pSemi ? TRUE : FALSE;

	if( m_fSemi - fSemi ) {

		if( m_fSemi = fSemi ) {

			m_pTB->SetSemiModal();

			m_pSB->SetSemiModal();

			SetTimer(timerLocks, 200);
			}
		else
			KillTimer(timerLocks);
		}

	m_pSemi = pSemi;
	}

void CMainWnd::LockPrompt(BOOL fLock)
{
	if( m_fShowSB ) {
		
		m_pSB->LockPrompt(fLock);
		}
	}

void CMainWnd::SetPrompt(PCTXT pPrompt)
{
	if( m_fShowSB ) {
		
		if( !m_pSB->InPromptMode() ) {
	
			KillTimer(timerPrompt);
			
			m_Prompt = pPrompt;
		
			SetTimer(timerPrompt, 200);
			
			return;
			}
		
		m_pSB->SetPrompt(pPrompt);
		}
	}

void CMainWnd::SetPrompt(UINT uID)
{
	if( m_fShowSB ) {
		
		if( uID < NOTHING ) {
		
			CCmdInfo Info;
		
			if( uID ) {

				RouteGetInfo(uID, Info);
				}
				
			SetPrompt(Info.m_Prompt);
			}
		else {
			KillTimer(timerPrompt);
			
			m_pSB->SetPrompt(NULL);
			}
		}
	}
	
void CMainWnd::SetStatus(PCTXT pStatus)
{
	m_pSB->GetGadget(IDP_PRIMARY)->SetText(pStatus);
	}

void CMainWnd::SetOverwrite(BOOL fOver)
{
/*	m_pSB->GetGadget(IDP_OVERWRITE)->AdjustFlag(MF_DISABLED, !fOver);
*/	}

void CMainWnd::ShowToolbar(BOOL fShow)
{
	if( m_fShowTB != fShow ) {
	
		m_fShowTB = fShow;
		
		m_pTB->ShowWindow(m_fShowTB ? SW_SHOW : SW_HIDE);
		
		PutConfig();
			
		SendReSize();
		}
	}

void CMainWnd::ShowStatusBar(BOOL fShow)
{
	if( m_fShowSB != fShow ) {
	
		m_fShowSB = fShow;
		
		m_pSB->ShowWindow(m_fShowSB ? SW_SHOW : SW_HIDE);
		
		PutConfig();
			
		SendReSize();
		}
	}

void CMainWnd::ShowEdge(BOOL fShow)
{
	if( m_fEdge != fShow ) {
	
		m_fEdge = fShow;
		
		if( !m_fEdge ) {

			Invalidate(TRUE);
			}
		
		SendReSize();
		}
	}

BOOL CMainWnd::ForceUI(void)
{
	if( m_fUpdate ) {

		m_fUpdate = FALSE;
		
		KillTimer(timerUpdate);
		
		DoUpdateUI();

		return TRUE;
		}

	return FALSE;
	}

void CMainWnd::AddStickyPopup(CWnd *pWnd)
{
	m_Sticky.Append(pWnd);
	}

void CMainWnd::RemoveStickyPopup(CWnd *pWnd)
{
	for( UINT n = 0; n < m_Sticky.GetCount(); n++ ) {

		if( m_Sticky[n] == pWnd ) {

			m_Sticky.Remove(n);

			break;
			}
		}
	}

// Client Calculation

CRect CMainWnd::GetClientRect(void) const
{	
	CRect Rect = CFrameWnd::GetClientRect();

	if( m_fShowTB )	{

		Rect.top = m_TBRect.bottom;
		}
	
	if( m_fShowSB )	{

		Rect.bottom = m_SBRect.top;
		}

	if( m_fEdge ) {

		Rect -= 2;
		}

	return Rect;
	}

// Routing Control

BOOL CMainWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( Message.message == WM_AFX_COMMAND ) {

		if( Message.wParam == IDM_VIEW_REFRESH ) {

			if( CWnd::OnRouteMessage(Message, lResult) ) {

				PostMessage(WM_UPDATEUI);

				return TRUE;
				}

			PostMessage(WM_UPDATEUI);
			}
		}

	if( m_pTB && m_pTB->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	if( m_pSB && m_pSB->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	return CFrameWnd::OnRouteMessage(Message, lResult);
	}
	
// Interface Control

void CMainWnd::OnUpdateInterface(void)
{
	LRESULT lResult = 0;

	CMenu Tool;

	CMenu Menu;

	Tool.CreateMenu();

	Menu.CreateMenu();

	Tool.AppendMenu(CMenu(L"HeadTool"));

	Menu.AppendMenu(CMenu(L"HeadMenu"));

	MSG MsgTool = { NULL, WM_LOADTOOL, 0, LPARAM(&Tool) };

	MSG MsgMenu = { NULL, WM_LOADMENU, 0, LPARAM(&Menu) };

	MSG MsgShow = { NULL, WM_SHOWUI,   1, 0 };

	RouteMessage(MsgTool, lResult);

	RouteMessage(MsgMenu, lResult);

	RouteMessage(MsgShow, lResult);
	
	Tool.AppendMenu(CMenu(L"TailTool"));

	Menu.AppendMenu(CMenu(L"TailMenu"));

	m_pTB->AddFromMenu(Tool, 0);

	Menu.MakeOwnerDraw(TRUE);

	SetMenu(Menu);
	}

void CMainWnd::OnCreateStatusBar(void)
{
	}

void CMainWnd::OnUpdateStatusBar(void)
{
	}
		
// Message Map

AfxMessageMap(CMainWnd, CFrameWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_ACTIVATEAPP)
	AfxDispatchMessage(WM_GETSTATUSBAR)
	AfxDispatchMessage(WM_GOINGIDLE)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_MENUSELECT)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETSTATUSBAR)
	AfxDispatchMessage(WM_WINDOWPOSCHANGING)
	AfxDispatchMessage(WM_WINDOWPOSCHANGED)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_UPDATEUI)
	AfxDispatchMessage(WM_NCHITTEST)
	AfxDispatchMessage(WM_SYSCOLORCHANGE)

	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)
	
	AfxDispatchControlType(IDM_HELP, OnHelpControl)
	AfxDispatchCommandType(IDM_HELP, OnHelpCommand)
	
	AfxDispatchGetInfo(0, OnGetInfo)

	AfxMessageEnd(CMainWnd)
	};

// Message Handlers

void CMainWnd::OnPostCreate(void)
{
	CreateStatusBar();
	
	OnUpdateInterface();

	CalcPositions();

	CFrameWnd::OnPostCreate();

	m_pTB->Create(m_TBRect, ThisObject);

	m_pSB->Create(m_SBRect, ThisObject);
	
	m_pTB->PollGadgets();
	
	m_pSB->PollGadgets();

	m_pTB->ShowWindow(m_fShowTB ? SW_SHOW : SW_HIDE);
	
	m_pSB->ShowWindow(m_fShowSB ? SW_SHOW : SW_HIDE);
	
	UpdateStatusBar();
	
	SetOverwrite(FALSE);
	}

void CMainWnd::OnDestroy(void)
{
	SetMenu(AfxNull(CMenu));
	}

void CMainWnd::OnActivateApp(BOOL fActive, DWORD dwThread)
{
	if( fActive && IsIconic() ) {

		m_fIconic = TRUE;
		}
	else {
		m_pView->SendMessage( m_MsgCtx.Msg.message,
				      m_MsgCtx.Msg.wParam,
				      0x5555AAAA
				      );

		m_fIconic = FALSE;
		}

	if( !fActive ) {

		m_pView->SendMessage(WM_CANCELMODE);
	
		SetTimer(timerLocks, 200);

		m_pView->SetCurrent(FALSE);
		}
	else {
		m_pView->SetCurrent(TRUE);
		
		KillTimer(timerLocks);
		}

	AfxCallDefProc();
	}

void CMainWnd::OnGetStatusBar(CString &Text)
{
	Text = m_pSB->GetGadget(IDP_PRIMARY)->GetText();
	}

void CMainWnd::OnGoingIdle(void)
{
	m_pTB->PollGadgets();

	m_pSB->PollGadgets();

	UpdateStatusBar();
	}

void CMainWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_FILE ) {

		UINT After = IDM_FILE_EXIT;

		m_Recent.UpdateMenu(Menu, After);
		}

	CFrameWnd::OnInitPopup(Menu, nIndex, fSystem);
	}

void CMainWnd::OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu)
{
	if( uFlags == 0xFFFF || LOWORD(Menu.GetHandle()) == 0xFFFF ) {
		
		SetPrompt(NOTHING);
	
		m_fMenu = FALSE;
		}
	else {
		if( (uFlags & MF_SYSMENU) || uID >= 0xF000 ) {
		
			if( !(uFlags & MF_POPUP) ) {

				if( uID ) {

					CString Name;
					
					if( uFlags & MF_SYSMENU ) {

						Name.Load(IDS_WND_APPLICATION);
						}
					else
						Name.Load(IDS_WND_DOCUMENT);
					
					SetPrompt(CPrintf(uID, Name));
					}
				else
					SetPrompt(0xF200);
				}
			else
				SetPrompt(0xF200);
			}
		else {
			if( uFlags & MF_POPUP ) {
			
				HMENU hMenu = Menu.GetSubMenu(uID);
				
				while( hMenu ) {

					UINT uTop = GetMenuItemID(hMenu, 0);
					
					if( LOWORD(uTop) != 0xFFFF ) {
					
						SetPrompt(uTop & 0xFF00);
					
						break;
						}
						
					hMenu = GetSubMenu(hMenu, 0);
					}
				}
			else {
				if( uID ) {

					SetPrompt(uID);
					}
				else
					SetPrompt(L"");
				}
			}

		m_fMenu = TRUE;
		}

	CMenuWnd::OnMenuSelect(uID, uFlags, Menu);
	}

void CMainWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_fEdge ) {
		
		CRect Rect = GetClientRect() + 2;
			
		DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);
		}
	}

void CMainWnd::OnSetStatusBar(PCTXT pText)
{
	m_pSB->GetGadget(IDP_PRIMARY)->SetText(pText);
	}

void CMainWnd::OnWindowPosChanging(WINDOWPOS &Pos)
{
	m_OldRect = GetWindowRect();

	AfxCallDefProc();
	}

void CMainWnd::OnWindowPosChanged(WINDOWPOS &Pos)
{
	if( !(Pos.flags & SWP_NOMOVE) ) {

		if( m_Sticky.GetCount() ) {

			CRect r[2];

			r[0] = m_OldRect;

			r[1] = GetWindowRect();

			for( UINT n = 0; n < m_Sticky.GetCount(); n++ ) {

				m_Sticky[n]->SendMessage(WM_MAINMOVED, WPARAM(r));
				}
			}
		}

	AfxCallDefProc();
	}

void CMainWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode != SIZE_MINIMIZED ) {

		if( m_fIconic ) {

			m_pView->SendMessage( WM_ACTIVATEAPP,
					      1,
					      0x12345678
					      );
			}

		m_fIconic = FALSE;
		}

	if( m_pTB->IsWindow() ) {

		CalcPositions();

		CFrameWnd::OnSize(uCode, Size);
		
		if( m_fEdge ) {
			
			Invalidate(TRUE);
			}
		
		m_pTB->MoveWindow(m_TBRect, TRUE);
		
		m_pSB->MoveWindow(m_SBRect, TRUE);

		return;
		}
		
	CFrameWnd::OnSize(uCode, Size);
	}

void CMainWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( m_pSB->IsWindow() ) {

		if( uID == timerLocks ) {
		
			UpdateStatusBar();
			
			return;
			}
			
		if( uID == timerPrompt ) {
		
			m_pSB->SetPrompt(m_Prompt);
			
			KillTimer(timerPrompt);
			
			return;
			}
			
		if( uID == timerUpdate ) {

			if( m_fUpdate ) {

				if( m_fSemi ) {

					m_fUpdate = FALSE;

					KillTimer(timerUpdate);
					}
				else {
					MENUBARINFO Info;

					Info.cbSize = sizeof(Info);

					GetMenuBarInfo(m_hWnd, OBJID_MENU, 0, &Info);

					if( Info.fFocused || Info.fBarFocused ) {

						// NOTE -- Can't update the interface if
						// the menu is active so delay this for
						// another 100ms and then try again.

						SetTimer(timerUpdate, 100);
						}
					else {
						m_fUpdate = FALSE;
						
						KillTimer(timerUpdate);
						
						DoUpdateUI();
						}
					}
				}
		
			return;
			}
		}
	}

void CMainWnd::OnFocusNotify(UINT uID, CWnd &Child)
{
	if( !IsActive() ) {
		
		BringWindowToTop();
		
		SetActiveWindow();
		}
	}
	
void CMainWnd::OnUpdateUI(void)
{
	if( !m_fSemi ) {

		if( !m_fUpdate ) {
		
			SetTimer(timerUpdate, 0);
		
			m_fUpdate = TRUE;
			}
		}
	}

UINT CMainWnd::OnNCHitTest(CPoint Pos)
{
	if( m_fSemi ) {

		return UINT(HTERROR);
		}

	return AfxCallDefProc();
	}

void CMainWnd::OnSysColorChange(void)
{
	afxStdTools->Update();
	}

// Command Handlers

BOOL CMainWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_VIEW_TOOLBAR:

			Src.EnableItem(TRUE);
			
			Src.CheckItem (m_fShowTB);
			
			break;
	
		case IDM_VIEW_GHOSTBAR:

			Src.EnableItem(TRUE);
			
			Src.CheckItem (m_fShowGB);
			
			break;
	
		case IDM_VIEW_STATUS:
			
			Src.EnableItem(TRUE);
			
			Src.CheckItem (m_fShowSB);
			
			break;

		case IDM_VIEW_REFRESH:

			Src.EnableItem(TRUE);

			break;
			
		case IDM_VIEW_CYCLE_LANG:

			Src.EnableItem(TRUE);

			break;
			
		default:
			return FALSE;
		}
	
	return TRUE;
	}

BOOL CMainWnd::OnViewCommand(UINT uID)
{
	switch( uID ) {
	
		case IDM_VIEW_TOOLBAR:

			ShowToolbar(!m_fShowTB);
			
			break;

		case IDM_VIEW_GHOSTBAR:

			m_fShowGB = !m_fShowGB;

			PutConfig();

			break;
	
		case IDM_VIEW_STATUS:
			
			ShowStatusBar(!m_fShowSB);
			
			break;
			
		case IDM_VIEW_CYCLE_LANG:

			OnViewCycleLang();

			break;
			
		case IDM_VIEW_REFRESH:

			OnViewRefresh();

			return FALSE;
			
		default:
			return FALSE;
		}
	
	return TRUE;
	}

void CMainWnd::OnViewRefresh(void)
{
	}

void CMainWnd::OnViewCycleLang(void)
{
	#if defined(_DEBUG)

	static UINT n = 1;

	switch( n++ % 5 ) {

		case 0:
			SetThreadLocale(MAKELCID(MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US),SORT_DEFAULT));
			break;
		
/*		case 1:
			SetThreadLocale(MAKELCID(MAKELANGID(LANG_FRENCH,SUBLANG_FRENCH),SORT_DEFAULT));
			break;
*/		
		case 2:
			SetThreadLocale(MAKELCID(MAKELANGID(LANG_GERMAN,SUBLANG_GERMAN),SORT_DEFAULT));
			break;
/*		
		case 3:
			SetThreadLocale(MAKELCID(MAKELANGID(LANG_SPANISH,SUBLANG_SPANISH),SORT_DEFAULT));
			break;
		
		case 4:
			SetThreadLocale(MAKELCID(MAKELANGID(LANG_CHINESE,SUBLANG_CHINESE_SIMPLIFIED),SORT_DEFAULT));
			break;
*/		}

	if( ::GetActiveWindow() == m_hWnd ) {

		PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);
		}

	#endif
	}
		
// Help Menu
		
BOOL CMainWnd::OnHelpControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_HELP_BALLOON_OFF && uID <= IDM_HELP_BALLOON_HARD ) {

		Src.EnableItem(CToolTip::AreBalloonsPossible());
		
		Src.CheckItem (uID - IDM_HELP_BALLOON_OFF == m_uBalloon);
	
		return TRUE;
		}

	return FALSE;
	}

BOOL CMainWnd::OnHelpCommand(UINT uID)
{
	if( uID >= IDM_HELP_BALLOON_OFF && uID <= IDM_HELP_BALLOON_HARD ) {

		UINT uBalloon = uID - IDM_HELP_BALLOON_OFF;

		if( m_uBalloon != uBalloon ) {

			if( uBalloon ) {

				if( !CToolTip::AreBalloonsEnabled() ) {

					if( !CToolTip::EnableBalloons(TRUE) ) {

						return TRUE;
						}
					}
				}
			
			m_uBalloon = uBalloon;

			RouteCommand(IDM_HELP_BALLOON_MOD, 0);

			PutConfig();
			}
	
		return TRUE;
		}

	return FALSE;
	}

// Command Information

BOOL CMainWnd::OnGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_FILE_NEW,		MAKELONG(0x0000, 0x1000),
		IDM_FILE_OPEN,		MAKELONG(0x0001, 0x1000),
		IDM_FILE_SAVE,		MAKELONG(0x0002, 0x1000),
		IDM_FILE_EXIT,		MAKELONG(0x0010, 0x1000),
		IDM_EDIT_UNDO,		MAKELONG(0x0003, 0x1000),
		IDM_EDIT_REDO,		MAKELONG(0x0004, 0x1000),
		IDM_EDIT_CUT,		MAKELONG(0x0005, 0x1000),
		IDM_EDIT_COPY,		MAKELONG(0x0006, 0x1000),
		IDM_EDIT_PASTE,		MAKELONG(0x0007, 0x1000),
		IDM_EDIT_PASTE_SPECIAL, MAKELONG(0x0032, 0x1000),
		IDM_EDIT_DELETE,	MAKELONG(0x0008, 0x1000),
		IDM_EDIT_PROPERTIES,	MAKELONG(0x0012, 0x1000),
		IDM_EDIT_SMART_DUP,	MAKELONG(0x0033, 0x1000),
		IDM_EDIT_SELECT_ALL,	MAKELONG(0x0013, 0x1000),
		IDM_EDIT_FIND,		MAKELONG(0x000C, 0x1000),
		IDM_EDIT_FIND_NEXT,	MAKELONG(0x002F, 0x1000),
		IDM_EDIT_FIND_PREV,	MAKELONG(0x0030, 0x1000),
		IDM_EDIT_FIND_ALL,	MAKELONG(0x002E, 0x1000),
		IDM_EDIT_FIND_ALL_NEXT,	MAKELONG(0x002F, 0x1000),
		IDM_EDIT_FIND_ALL_PREV,	MAKELONG(0x0030, 0x1000),
		IDM_VIEW_FIND_RESULTS,	MAKELONG(0x0031, 0x1000),
		IDM_VIEW_WATCH,		MAKELONG(0x0036, 0x1000),
		IDM_GO_BACK,		MAKELONG(0x0009, 0x1000),
		IDM_GO_FORWARD,		MAKELONG(0x000A, 0x1000),
		IDM_GO_NEXT_1,		MAKELONG(0x0015, 0x1000),
		IDM_GO_PREV_1,		MAKELONG(0x0014, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	if( uID >= IDM_FILE_RECENT && uID < IDM_FILE_RECENT + 10 ) {
	
		CString Name = m_Recent.GetFileFromID(uID);

		Info.m_Prompt.Printf(IDM_FILE_RECENT, Name);
		
		Info.m_ToolTip = L"";
		}
	else {
		if( HIBYTE(uID) ) {

			CString Text(uID);
			
			if( Text[0] == '[' ) {
			
				UINT uPos = Text.Find(']');
				
				Info.m_ToolTip = Text.Mid(1, uPos - 1);
				
				Info.m_Prompt  = Text.Mid(uPos + 1);
				}
			else
				Info.m_Prompt  = Text;
			}
		}
	
	return TRUE;
	}

// Implementation

void CMainWnd::CalcPositions(void)
{
	m_TBRect = CWnd::GetClientRect();

	m_TBRect.bottom = m_TBRect.top + m_pTB->GetHeight();

	m_SBRect = CWnd::GetClientRect();

	m_SBRect.top = m_SBRect.bottom - m_pSB->GetHeight();
	}

void CMainWnd::CreateStatusBar(void)
{
/*	CString T1(IDS_PANE_OVERWRITE);

*/	CString T2(IDS_PANE_CAPITAL);

	CString T3(IDS_PANE_NUMLOCK);

	m_pSB->AddGadget(New CTextGadget(IDP_PRIMARY,   0,  textNormal));

	OnCreateStatusBar();

/*	m_pSB->AddGadget(New CTextGadget(IDP_OVERWRITE, T1, textNormal));
	
*/	m_pSB->AddGadget(New CTextGadget(IDP_CAPITAL,   T2, textNormal));
	
	m_pSB->AddGadget(New CTextGadget(IDP_NUMLOCK,   T3, textNormal));
	}

void CMainWnd::UpdateStatusBar(void)
{
	BOOL fCaps = !(GetKeyState(VK_CAPITAL) & 1);

	BOOL fNum  = !(GetKeyState(VK_NUMLOCK) & 1);

	m_pSB->GetGadget(IDP_CAPITAL)->AdjustFlag(MF_DISABLED, fCaps);

	m_pSB->GetGadget(IDP_NUMLOCK)->AdjustFlag(MF_DISABLED, fNum);

	OnUpdateStatusBar();
	}
		
void CMainWnd::SendReSize(void)
{
	LPARAM lParam = LPARAM(CWnd::GetClientRect().GetSize());
	
	SendMessage(WM_SIZE, SIZE_RESTORED, lParam);
	}

void CMainWnd::ClearToolbar(void)
{
	m_pTB->DestroyWindow(TRUE);
			
	m_pTB = New CToolbarWnd(0);
	}

void CMainWnd::UpdateToolbar(void)
{
	m_TBRect = CWnd::GetClientRect();

	m_TBRect.bottom = m_TBRect.top + m_pTB->GetHeight();
		
	m_pTB->Create(m_TBRect, ThisObject);

	m_pTB->PollGadgets();
	
	m_pTB->ShowWindow(m_fShowTB ? SW_SHOW : SW_HIDE);
	}

void CMainWnd::GetConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();
	
	Key.MoveTo(L"PCDesk");
	
	Key.MoveTo(L"Main Window");

	m_fShowTB  = Key.GetValue(L"ShowTB",  UINT(1));

	m_fShowGB  = Key.GetValue(L"ShowGB",  UINT(1));

	m_fShowSB  = Key.GetValue(L"ShowSB",  UINT(1));

	m_uBalloon = Key.GetValue(L"Balloon", UINT(0));
	
	PutConfig();
	}

void CMainWnd::PutConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();
	
	Key.MoveTo(L"PCDesk");
	
	Key.MoveTo(L"Main Window");

	Key.SetValue(L"ShowTB",  m_fShowTB);

	Key.SetValue(L"ShowGB",  m_fShowGB);

	Key.SetValue(L"ShowSB",  m_fShowSB);

	Key.SetValue(L"Balloon", m_uBalloon);
	}

void CMainWnd::DoUpdateUI(void)
{
	try {
		ClearToolbar();

		EndMenu();
	
		OnUpdateInterface();
		}
			
	catch(CException &) {
		
		UpdateToolbar();
		
		throw;
		}

	UpdateToolbar();
	}

// End of File
