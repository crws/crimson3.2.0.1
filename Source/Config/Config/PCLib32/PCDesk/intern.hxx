
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_PANE_CAPITAL        0x4000
#define IDS_PANE_NUMLOCK        0x4001
#define IDS_PANE_OVERWRITE      0x4002
#define IDS_WND_APPLICATION     0x4003
#define IDS_WND_DOCUMENT        0x4004

// End of File

#endif
