
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Semaphore Object Class
//

// Runtime Class

AfxImplementRuntimeClass(CSemaphore, CWaitable);

// Constructors

CSemaphore::CSemaphore(LONG lCount)
{
	m_hObject = CreateSemaphore(NULL, lCount, lCount, NULL);
	}

CSemaphore::CSemaphore(LONG lCount, LONG lMax)
{
	m_hObject = CreateSemaphore(NULL, lCount, lMax, NULL);
	}

// Operations

BOOL CSemaphore::Release(UINT uCount)
{
	AfxAssert(uCount);
	
	return ReleaseSemaphore(m_hObject, uCount, NULL);
	}

// End of File
