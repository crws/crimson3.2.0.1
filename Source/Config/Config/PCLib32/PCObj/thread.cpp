
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Thread Object
//

// Dyanamic Class

AfxImplementDynamicClass(CRawThread, CWaitable);

// Static Data

DWORD CRawThread::m_dwTlsAlloc = NOTHING;

// TLS Initialization

BOOL CRawThread::AllocThreadData(void)
{
	if( m_dwTlsAlloc == NOTHING ) {

		m_dwTlsAlloc = TlsAlloc();

		TlsSetValue(m_dwTlsAlloc, NULL);

		return TRUE;
		}

	return FALSE;
	}

// Object Location

CRawThread * CRawThread::FindInstance(void)
{
	return (CRawThread *) TlsGetValue(m_dwTlsAlloc);
	}

// Constructor

CRawThread::CRawThread(void)
{
	m_uState = threadInitial;

	m_ID     = 0;
	}

// Destructor

CRawThread::~CRawThread(void)
{
	AfxAssert(!IsRunning());
	}

// Attributes

DWORD CRawThread::GetThreadID(void) const
{
	return m_ID;
	}

UINT CRawThread::GetState(void) const
{
	return m_uState;
	}

BOOL CRawThread::IsRunning(void) const
{
	return m_uState == threadRunning;
	}

BOOL CRawThread::IsDone(void) const
{
	return m_uState == threadDone;
	}

BOOL CRawThread::GetTermRequest(void)
{
	return m_TermEvent.WaitForObject(0) == waitSignal;
	}

// Operations

void CRawThread::Create(void)
{
	AfxAssert(m_uState == threadInitial || m_uState == threadDone);

	Close();
	
	CModule::SetCreatingThread();
	
	m_uState      = threadInitial;

	DWORD dwFlags = 0;

	m_hObject     = CreateThread(NULL, 0, ThreadProc, this, dwFlags, &m_ID);
	}

void CRawThread::CreateSuspended(void)
{
	AfxAssert(m_uState == threadInitial || m_uState == threadDone);

	Close();

	CModule::SetCreatingThread();
	
	m_uState      = threadInitial;

	DWORD dwFlags = CREATE_SUSPENDED;

	m_hObject     = CreateThread(NULL, 0, ThreadProc, this, dwFlags, &m_ID);
	}

void CRawThread::Suspend(void)
{
	SuspendThread(m_hObject);
	}

void CRawThread::Resume(void)
{
	ResumeThread(m_hObject);
	}

BOOL CRawThread::Terminate(DWORD Timeout)
{
	m_TermEvent.Set();

	if( Timeout ) {

		CWaitable Object = ThisObject;

		if( Object.WaitForObject(Timeout) == waitTimeout ) {

			TerminateThread(m_hObject, 0);

			m_uState = threadDone;

			m_TermEvent.Reset();

			return FALSE;
			}

		m_TermEvent.Reset();
		}

	return TRUE;
	}

void CRawThread::AttachInput(CRawThread &Thread)
{
	AfxValidateObject(Thread);

	AttachThreadInput(m_ID, Thread.m_ID, TRUE);
	}

void CRawThread::DetachInput(CRawThread &Thread)
{
	AfxValidateObject(Thread);

	AttachThreadInput(m_ID, Thread.m_ID, FALSE);
	}

void CRawThread::SetPriority(int nPriority)
{
	SetThreadPriority(m_hObject, nPriority);
	}
		
// Validation

void CRawThread::AssertValid(void) const
{
	AfxAssert(m_ID); 

	CWinObject::AssertValid();
	}

// Overridables

BOOL CRawThread::OnInit(void)
{
	return TRUE;
	}

UINT CRawThread::OnExec(void)
{
	m_TermEvent.WaitForObject();

	return 0;
	}

void CRawThread::OnTerm(void)
{
	}

// Entry Point

DWORD __stdcall ThreadProc(PVOID pData)
{
	UINT ExitCode = 0;

	CRawThread *pThread = (CRawThread *) pData;

	TlsSetValue(CRawThread::m_dwTlsAlloc, pThread);

	if( pThread->OnInit() ) {

		pThread->m_uState = threadRunning;
		
		ExitCode = pThread->OnExec();

		pThread->m_uState = threadDone;

		pThread->OnTerm();
		}

	TlsSetValue(CRawThread::m_dwTlsAlloc, NULL);

	return ExitCode;
	}

// End of File
