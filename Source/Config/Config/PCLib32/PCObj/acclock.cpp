
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Read Write Access Lock Class
//

// Runtime Class

AfxImplementRuntimeClass(CAccessLock, CObject);

// Constructor

CAccessLock::CAccessLock(void)
{
	}

// Destructor

CAccessLock::~CAccessLock(void)
{
	}

// Methods

void CAccessLock::LockRead(void)
{
	m_ReadMutex.WaitForObject();

	if( m_nReaders++ == 0 ) {

		m_DataMutex.WaitForObject();
		}

	m_ReadMutex.Release();
 	}

void CAccessLock::FreeRead(void)
{
	m_ReadMutex.WaitForObject();

	if( --m_nReaders == 0 ) {

		m_DataMutex.Release();
		}

	m_ReadMutex.Release();
	}

void CAccessLock::LockWrite(void)
{
	m_DataMutex.WaitForObject();
	}

void CAccessLock::FreeWrite(void)
{
	m_DataMutex.Release();
	}

//////////////////////////////////////////////////////////////////////////
//
// Write Lock Guard
//

// Runtime Class

AfxImplementRuntimeClass(CWrGuard, CObject);

// Constructors

CWrGuard::CWrGuard(CAccessLock &Lock)
{
	m_pLock = &Lock;

	m_pLock->LockWrite();
	}

CWrGuard::CWrGuard(CWrGuard const &That)
{
	m_pLock = That.m_pLock;

	m_pLock->LockWrite();
	}

// Destructor

CWrGuard::~CWrGuard(void)
{
	m_pLock->FreeWrite();
	}

//////////////////////////////////////////////////////////////////////////
//
// Read Lock Guard
//

// Runtime Class

AfxImplementRuntimeClass(CRdGuard, CObject);

// Constructors

CRdGuard::CRdGuard(CRdGuard const &That)
{
	m_pLock = That.m_pLock;

	m_pLock->LockRead();
	}

CRdGuard::CRdGuard(CAccessLock &Lock)
{
	m_pLock = &Lock;

	m_pLock->LockRead();
	}

// Destructor

CRdGuard::~CRdGuard(void)
{
	m_pLock->FreeRead();
	}

// End of File
