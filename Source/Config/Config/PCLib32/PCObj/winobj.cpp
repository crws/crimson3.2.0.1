
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Win32 Object Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CWinObject, CObject);

// Constructors

CWinObject::CWinObject(void)
{
	m_hObject = NULL;
	}

CWinObject::CWinObject(CWinObject const &That)
{
	if( That.IsValid() ) {

		HANDLE hProcess = GetCurrentProcess();

		DuplicateHandle( hProcess, 
				 That.m_hObject,
				 hProcess, 
				 &m_hObject,
				 0,
				 FALSE,
				 DUPLICATE_SAME_ACCESS
				 );

		return;
		}

	m_hObject = NULL;
	}

CWinObject::CWinObject(HANDLE hObject)
{
	m_hObject = hObject;
	}

// Destructor

CWinObject::~CWinObject(void)
{
	Close();
	}

// Assignment Operators

CWinObject const & CWinObject::operator = (CWinObject const &That)
{
	Close();

	HANDLE hProcess = GetCurrentProcess();

	DuplicateHandle( hProcess, 
			 That.m_hObject,
			 hProcess, 
			 &m_hObject,
			 0,
			 FALSE,
			 DUPLICATE_SAME_ACCESS
			 );

	return ThisObject;
	}

CWinObject const & CWinObject::operator = (HANDLE hObject)
{
	Close();

	m_hObject = hObject;

	return ThisObject;
	}

// Conversion

CWinObject::operator HANDLE (void) const
{
	return m_hObject;
	}

// Attributes

HANDLE CWinObject::GetHandle(void) const
{
	return m_hObject;
	}

BOOL CWinObject::IsValid(void) const
{
	return (m_hObject == NULL || m_hObject == INVALID_HANDLE_VALUE) ? FALSE : TRUE;
	}

BOOL CWinObject::IsNull(void) const
{
	return (m_hObject == NULL || m_hObject == INVALID_HANDLE_VALUE) ? FALSE : TRUE;
	}

BOOL CWinObject::operator ! (void) const
{
	return IsNull();
	}

// Operations

void CWinObject::Close(void)
{
	if( IsValid() ) {

		CloseHandle(m_hObject);

		m_hObject = NULL;
		}
	}

void CWinObject::Detach(void)
{
	m_hObject = NULL;
	}

// Validation

void CWinObject::AssertValid(void) const
{
	CObject::AssertValid();
	
	AfxAssert(IsValid());
	}

// End of File
