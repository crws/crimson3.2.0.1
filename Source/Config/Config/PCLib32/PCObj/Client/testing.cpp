
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Data
//

static CSyncList <int> m_Array;

static UINT m_uWrite    = 0;

static UINT m_uRead     = 0;

static UINT m_uMaxWrite = 0;

static UINT m_uMaxRead  = 0;

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	int n;

	CRawThread *t[10];

	for( n = 0; n < elements(t); n ++ ) {
	
		t[n] = New CTestThread();

		t[n]->Create();
		}

	Sleep(500);
	
	for( n = 0; n < elements(t); n ++ ) {

		t[n]->Terminate(1000);
		}

	AfxTrace(L"\n");

	AfxTrace(L"Max Write = %u\n", m_uMaxWrite);

	AfxTrace(L"Max Read  = %u\n", m_uMaxRead);

	AfxTrace(L"Count     = %u\n", m_Array.GetCount());
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Thread
//

// Runtime Class

AfxImplementRuntimeClass(CTestThread, CRawThread);

// Constructor

CTestThread::CTestThread(void)
{
	}

// Overridables

BOOL CTestThread::OnInit(void)
{
	AfxAssert(afxRawThread == this);

	return TRUE;
	}

UINT CTestThread::OnExec(void)
{
	while( !GetTermRequest() ) {

		if( rand() % 4 ) {

			CRdGuard Guard = m_Array.GetRdGuard();

			InterlockedIncrement(LPLONG(&m_uRead));

			AfxAssert(m_uWrite == 0);

			BEGIN
			{
				CCriticalGuard Guard;

				MakeMax(m_uMaxRead, m_uRead);
				
				} END;

			m_Array.Find(rand() % 100);

			InterlockedDecrement(LPLONG(&m_uRead));
			}
		else {
			CWrGuard Guard = m_Array.GetWrGuard();

			InterlockedIncrement(LPLONG(&m_uWrite));

			AfxAssert(m_uRead == 0);

			BEGIN
			{
				CCriticalGuard Guard;

				MakeMax(m_uMaxWrite, m_uWrite);
				
				} END;

			m_Array.Append(rand() % 100);

			InterlockedDecrement(LPLONG(&m_uWrite));
			}
		}

	return 0;
	}

void CTestThread::OnTerm(void)
{
	delete this;
	}

// End of File
