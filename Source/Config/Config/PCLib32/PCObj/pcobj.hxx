
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCOBJ_HXX
	
#define	INCLUDE_PCOBJ_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pccore.hxx>

// End of File

#endif
