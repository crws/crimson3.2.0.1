
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Timer Object Class
//

// Runtime Class

AfxImplementRuntimeClass(CTimer, CWaitable);

// Constructor

CTimer::CTimer(BOOL fManual)
{
	m_hObject = CreateWaitableTimer(NULL, fManual, NULL);
	}

// Operations

void CTimer::SetDelay(DWORD dwTime)
{
	LARGE_INTEGER lRel;
	
	lRel.QuadPart = (INT64) dwTime * -10000;
		
	SetWaitableTimer(m_hObject, &lRel, 0, NULL, 0, FALSE);
	}

void CTimer::SetPeriod(DWORD dwTime)
{
	LARGE_INTEGER lRel;
	
	lRel.QuadPart = 0;
		
	SetWaitableTimer(m_hObject, &lRel, dwTime, NULL, 0, FALSE);
	}

void CTimer::Cancel(void)
{
	CancelWaitableTimer(m_hObject);
	}

// End of File
