
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCOBJ_HPP
	
#define	INCLUDE_PCOBJ_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pccore.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pcobj.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCOBJ

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pcobj.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CWinObject;
class CWaitable;
class CWaitableList;
class CEvent;
class CSemaphore;
class CMutex;
class CTimer;
class CRawThread;
class CAccessLock;
class CRdGuard;
class CWrGuard;

//////////////////////////////////////////////////////////////////////////
//
// Waitable Reference
//

typedef CWaitable const &WAITREF;

//////////////////////////////////////////////////////////////////////////
//
// Thread Access Macro
//

#define	afxRawThread (CRawThread::FindInstance())

//////////////////////////////////////////////////////////////////////////
//
// Wait Return Codes
//

enum WaitCode
{
	waitTimeout = 0,
	waitSignal  = 1,
	waitMessage = 2,
	waitFailed  = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Thread State Codes
//

enum ThreadState
{
	threadInitial = 0,
	threadRunning = 1,
	threadDone    = 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Win32 Object Base Class
//

class DLLAPI CWinObject : public CObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructors
		CWinObject(void);
		CWinObject(CWinObject const &That);
		CWinObject(HANDLE hObject);
		
		// Destructor
		~CWinObject(void);

		// Assignment Operators
		CWinObject const & operator = (CWinObject const &That);
		CWinObject const & operator = (HANDLE hObject);

		// Conversion
		operator HANDLE (void) const;

		// Attributes
		HANDLE GetHandle(void) const;
		BOOL IsValid(void) const;
		BOOL IsNull(void) const;
		BOOL operator ! (void) const;

		// Operations
		void Close(void);
		void Detach(void);

		// Validation
		void AssertValid(void) const;

	protected:
		// Data
		HANDLE m_hObject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Waitable Object
//

class DLLAPI CWaitable : public CWinObject
{
	public:
		// Dyanmic Class
		AfxDeclareDynamicClass();
		
		// Constructors
		CWaitable(void);
		CWaitable(CWinObject const &That);
		CWaitable(CWaitable  const &That);
		CWaitable(HANDLE hObject);

		// Destructor
		~CWaitable(void);

		// Assignment Operators
		CWaitable const & operator = (CWaitable const &That);
		CWaitable const & operator = (HANDLE hObject);
		
		// Operations
		UINT WaitForObject(DWORD Timeout = INFINITE) const;
		UINT WaitForObjectAndMsg(DWORD Timeout = INFINITE) const;
		UINT WaitForObjectOrMsg(DWORD Timeout = INFINITE) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Waitable Object List
//

class DLLAPI CWaitableList : public CObject
{
	public:
		// Constructors
		CWaitableList(void);
		CWaitableList(WAITREF o1);
		CWaitableList(WAITREF o1, WAITREF o2);
		CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3);
		CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3, WAITREF o4);
		CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3, WAITREF o4, WAITREF o5);
		CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3, WAITREF o4, WAITREF o5, WAITREF o6);

		// Destructor
		~CWaitableList(void);

		// List Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetCount(void) const;
		UINT GetLimit(void) const;

		// List Lookup Operator
		CWaitable const & operator [] (UINT uIndex) const;

		// List Data Read
		CWaitable const & GetAt(UINT uIndex) const;

		// List Operations
		void Empty(void);
		UINT Append(CWaitable const &Object);
		void Append(CWaitableList const &List);
		UINT Insert(UINT uIndex, CWaitable const &Object);
		void Insert(UINT uIndex, CWaitableList const &List);
		void Remove(UINT uIndex, UINT uCount);
		void Remove(UINT uIndex);

		// Wait Operations
		UINT WaitForAllObjects(DWORD Timeout = INFINITE);
		UINT WaitForAnyObject(DWORD Timeout = INFINITE);
		UINT WaitForAllObjectsAndMsg(DWORD Timeout = INFINITE);
		UINT WaitForAnyObjectOrMsg(DWORD Timeout = INFINITE);

		// Wait Attributes
		UINT GetObjectIndex(void) const;

	protected:
		// List Typedef
		typedef CArray <CWaitable const *> CObjList;

		// Data Members
		CObjList m_List;
		UINT     m_uIndex;
		HANDLE * m_pHandles;
		UINT     m_uCount;

		// Implementation
		void MakeHandleList(void);
		void FreeHandleList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Object
//

class DLLAPI CEvent : public CWaitable
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEvent(BOOL fManual = TRUE);
		
		// Operations
		void Set(void);
		void Reset(void);
		void Pulse(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mutex Object
//

class DLLAPI CMutex : public CWaitable
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CMutex(void);
		
		// Operations
		void Release(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Semaphore Object
//

class DLLAPI CSemaphore : public CWaitable
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSemaphore(LONG lCount = 1);
		CSemaphore(LONG lCount, LONG lMax);
		
		// Operations
		BOOL Release(UINT uCount = 1);
	};

//////////////////////////////////////////////////////////////////////////
//
// Timer Object
//

class DLLAPI CTimer : public CWaitable
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTimer(BOOL fManual = TRUE);
		
		// Operations
		void SetDelay(DWORD dwTime);
		void SetPeriod(DWORD dwTime);
		void Cancel(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Thread Object
//

class DLLAPI CRawThread : public CWaitable
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// TLS Initialization
		static BOOL AllocThreadData(void);

		// Object Location
		static CRawThread * FindInstance(void);
		
		// Constructors
		CRawThread(void);

		// Destructor
		~CRawThread(void);

		// Attributes
		DWORD GetThreadID(void) const;
		UINT  GetState(void) const;
		BOOL  IsRunning(void) const;
		BOOL  IsDone(void) const;
		BOOL  GetTermRequest(void);

		// Operations
		void Create(void);
		void CreateSuspended(void);
		void Suspend(void);
		void Resume(void);
		BOOL Terminate(DWORD Timeout);
		void AttachInput(CRawThread &Thread);
		void DetachInput(CRawThread &Thread);
		void SetPriority(int nPriority);
		
		// Validation
		void AssertValid(void) const;

	protected:
		// Static Data
		static DWORD m_dwTlsAlloc;

		// Data Members
		UINT   m_uState;
		DWORD  m_ID;
		CEvent m_TermEvent;

		// Overridables
		virtual BOOL OnInit(void);
		virtual UINT OnExec(void);
		virtual void OnTerm(void);

		// Entry Point
		friend DWORD __stdcall ThreadProc(PVOID pData); 
	};

//////////////////////////////////////////////////////////////////////////
//
// Access Lock Class
//

class DLLAPI CAccessLock : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
	
		// Constructor
		CAccessLock(void);

		// Destructor
		~CAccessLock(void);

		// Methods
		void LockRead(void);
		void FreeRead(void);
		void LockWrite(void);
		void FreeWrite(void);

	protected:
		// Data Members
		CMutex m_DataMutex;
		CMutex m_ReadMutex;
		INT    m_nReaders;
	};

//////////////////////////////////////////////////////////////////////////
//
// Write Lock Guard
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CWrGuard : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
	
		// Constructors
		CWrGuard(CAccessLock &Lock);
		CWrGuard(CWrGuard const &That);

		// Destructor
		~CWrGuard(void);

	protected:
		// Data
		CAccessLock *m_pLock;
	};

//////////////////////////////////////////////////////////////////////////
//
// Read Lock Guard
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CRdGuard : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
	
		// Constructors
		CRdGuard(CAccessLock &Lock);
		CRdGuard(CRdGuard const &That);

		// Destructor
		~CRdGuard(void);

	protected:
		// Data
		CAccessLock *m_pLock;
	};

//////////////////////////////////////////////////////////////////////////
//
// Synchronised Collections
//

#include "syncol.hpp"

// End of File

#endif
