
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2004 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Socket Event Wrapper Class
//

// Constructor

CNetEvent::CNetEvent(void)
{
	Reset();
	}

// Operations

void CNetEvent::Reset(void)
{
	ZeroMemory(this, sizeof(WSANETWORKEVENTS));
	}

BOOL CNetEvent::Check(UINT uBit) const
{
	return (lNetworkEvents & (1 << uBit)) && !iErrorCode[ uBit ];
	}

BOOL CNetEvent::CheckRead(void) const
{
	return Check(FD_READ_BIT);
	}

BOOL CNetEvent::CheckWrite(void) const
{
	return Check(FD_WRITE_BIT);
	}

BOOL CNetEvent::CheckAccept(void) const
{
	return Check(FD_ACCEPT_BIT);
	}

BOOL CNetEvent::CheckClose(void) const
{
	return Check(FD_CLOSE_BIT);
	}

BOOL CNetEvent::CheckConnect(void) const
{
	return Check(FD_CONNECT_BIT);
	}

// End of File
