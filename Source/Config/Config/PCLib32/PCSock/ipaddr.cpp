
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2004 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IP Address
//

// Constructors

CIPAddr::CIPAddr(void)
{
	CreateEmpty();
	}

CIPAddr::CIPAddr(CIPAddr const &That)
{
	Create(That);
	}

CIPAddr::CIPAddr(sockaddr const &addr)
{
	Create(addr);
	}

CIPAddr::CIPAddr(PCTXT pHost)
{
	Create(pHost);
	}

CIPAddr::CIPAddr(BYTE a, BYTE b, BYTE c, BYTE d)
{
	Create(a, b, c, d);
	}

CIPAddr::CIPAddr(BYTE bAddr[4])
{
	Create(bAddr[0], bAddr[1], bAddr[2], bAddr[3]);
	}

CIPAddr::CIPAddr(DWORD ip)
{
	Create(ip);
	}

// Assignment Operators

CIPAddr const & CIPAddr::operator = (CIPAddr const &That)
{
	Create(That);

	return ThisObject;
	}

CIPAddr const & CIPAddr::operator = (sockaddr const &addr)
{
	Create(addr);

	return ThisObject;
	}

CIPAddr const & CIPAddr::operator = (PCTXT pHost)
{
	Create(pHost);

	return ThisObject;
	}

CIPAddr const & CIPAddr::operator = (DWORD ip)
{
	Create(ip);

	return ThisObject;
	}

// Initialisation

void CIPAddr::CreateLoopback(void)
{
	m_Addr.S_un.S_addr = INADDR_LOOPBACK;
	}

void CIPAddr::CreateAny(void)
{
	m_Addr.S_un.S_addr = INADDR_ANY;
	}

void CIPAddr::CreateBroadcast(void)
{
	m_Addr.S_un.S_addr = INADDR_BROADCAST;
	}

void CIPAddr::CreateEmpty(void)
{
	m_Addr.S_un.S_addr = INADDR_NONE;
	}

BOOL CIPAddr::Create(CIPAddr const &That)
{
	m_Addr.S_un.S_addr = That.m_Addr.S_un.S_addr;

	return TRUE;
	}

BOOL CIPAddr::Create(sockaddr const &addr)
{
	sockaddr_in &in = (sockaddr_in &) addr;

	m_Addr.S_un.S_addr = in.sin_addr.S_un.S_addr;

	return TRUE;
	}

BOOL CIPAddr::Create(PCTXT pHost)
{
	DWORD ip = inet_addr(pHost);

	if( !(ip == INADDR_NONE) ) {

		m_Addr.S_un.S_addr = ip;

		return TRUE;
		}
	else {
		hostent *p = gethostbyname(pHost);

		if( p ) {

			for( UINT n = 0; p->h_addr_list[n]; n++ );

			if( n ) {

				n = (rand() % n);

				BYTE *pData = (BYTE *) p->h_addr_list[n];

				memcpy(&m_Addr, pData, sizeof(DWORD));

				return TRUE;
				}
			}

		m_Addr.S_un.S_addr = INADDR_NONE;

		return FALSE;
		}
	}

BOOL CIPAddr::Create(BYTE a, BYTE b, BYTE c, BYTE d)
{
	m_Addr.S_un.S_un_b.s_b1 = a;
	m_Addr.S_un.S_un_b.s_b2 = b;
	m_Addr.S_un.S_un_b.s_b3 = c;
	m_Addr.S_un.S_un_b.s_b4 = d;

	return TRUE;
	}

BOOL CIPAddr::Create(DWORD ip)
{
	m_Addr.S_un.S_addr = ip;

	return TRUE;
	}

BOOL CIPAddr::CreateLocal(void)
{
	char sName[128] = { 0 };

	gethostname(sName, sizeof(sName));

	return Create(sName);
	}

// Conversions

CIPAddr::operator CString (void) const
{
	return GetHost();
	}

CIPAddr::operator DWORD (void) const
{
	return m_Addr.S_un.S_addr;
	}

// Comparison Operators

int CIPAddr::operator == (CIPAddr const &That) const
{
	return m_Addr.S_un.S_addr == That.m_Addr.S_un.S_addr;
	}

int CIPAddr::operator != (CIPAddr const &That) const
{
	return m_Addr.S_un.S_addr != That.m_Addr.S_un.S_addr;
	}

// Attributes

BOOL CIPAddr::IsEmpty(void) const
{
	return m_Addr.S_un.S_addr == INADDR_NONE;
	}

BOOL CIPAddr::IsAny(void) const
{
	return m_Addr.S_un.S_addr == INADDR_ANY;
	}

BOOL CIPAddr::IsBroadcast(void) const
{
	return m_Addr.S_un.S_addr == INADDR_BROADCAST;
	}

BOOL CIPAddr::IsLoopback(void) const
{
	return m_Addr.S_un.S_addr == INADDR_LOOPBACK;
	}

PCBYTE CIPAddr::GetBinary(void) const
{
	return PCBYTE(&m_Addr.S_un.S_un_b);
	}

BYTE CIPAddr::GetByte(UINT n) const
{
	switch( n ) {

		case 0: return m_Addr.S_un.S_un_b.s_b1;
		case 1: return m_Addr.S_un.S_un_b.s_b2;
		case 2: return m_Addr.S_un.S_un_b.s_b3;
		case 3: return m_Addr.S_un.S_un_b.s_b4;

		}
	
	AfxAssert(FALSE);

	return 0;
	}

CString CIPAddr::GetDotted(void) const
{
	return CPrintf( "%u.%u.%u.%u",
			m_Addr.S_un.S_un_b.s_b1,
			m_Addr.S_un.S_un_b.s_b2,
			m_Addr.S_un.S_un_b.s_b3,
			m_Addr.S_un.S_un_b.s_b4
			);
	}

CString CIPAddr::GetHost(void) const
{
	hostent *p = gethostbyaddr( PCTXT(&m_Addr.S_un.S_addr),
				    sizeof(m_Addr.S_un.S_addr),
				    AF_INET
				    );

	if( p ) {

		CString Name = p->h_name;

		Name.MakeLower();

		return Name;
		}

	return GetDotted();
	}

// Socket Helpers

void CIPAddr::LoadSockAddr(sockaddr &s, WORD Port) const
{
	sockaddr_in &in = (sockaddr_in &) s;

	in.sin_family           = AF_INET;

	in.sin_port             = MAKEWORD(HIBYTE(Port), LOBYTE(Port));

	in.sin_addr.S_un.S_addr = m_Addr.S_un.S_addr;
	}

// End of File
