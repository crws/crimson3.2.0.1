
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2004 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCSOCK_HPP
	
#define	INCLUDE_PCSOCK_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcobj.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pcsock.hxx"

/////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCSOCK

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pcsock.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//								
// Library Files
//

#pragma	comment(lib, "ws2_32.lib")

//////////////////////////////////////////////////////////////////////////
//
// Byte Ordering Helpers
//

#define	NetToHost(x)	HostToNet(x)

//////////////////////////////////////////////////////////////////////////
//
// Network Byte Ordering
//

inline	WORD	HostToNet(WORD Data)
{
	return htons(Data);
	}

inline	DWORD	HostToNet(DWORD Data)
{
	return htonl(Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CIPAddr;
class CIPPort;
class CNetEvent;
class CSocket;

//////////////////////////////////////////////////////////////////////////
//
// IP Address
//

class DLLAPI CIPAddr
{
	public:
		// Constructors
		CIPAddr(void);
		CIPAddr(CIPAddr const &That);
		CIPAddr(sockaddr const &addr);
		CIPAddr(PCTXT pHost);
		CIPAddr(BYTE Addr[4]);
		CIPAddr(BYTE a, BYTE b, BYTE c, BYTE d);
		CIPAddr(DWORD ip);

		// Assignment Operators
		CIPAddr const & operator = (CIPAddr const &That);
		CIPAddr const & operator = (sockaddr const &addr);
		CIPAddr const & operator = (PCTXT pHost);
		CIPAddr const & operator = (DWORD ip);

		// Initialisation
		BOOL Create(CIPAddr const &That);
		BOOL Create(sockaddr const &addr);
		BOOL Create(PCTXT pHost);
		BOOL Create(BYTE a, BYTE b, BYTE c, BYTE d);
		BOOL Create(DWORD ip);
		BOOL CreateLocal(void);
		void CreateLoopback(void);
		void CreateAny(void);
		void CreateBroadcast(void);
		void CreateEmpty(void);

		// Conversions
		operator CString(void) const;
		operator DWORD  (void) const;
		
		// Comparison Operators
		int operator == (CIPAddr const &That) const;
		int operator != (CIPAddr const &That) const;

		// Attributes
		BOOL    IsEmpty(void) const;
		BOOL    IsAny(void) const;
		BOOL	IsBroadcast(void) const;
		BOOL    IsLoopback(void) const;
		BYTE    GetByte(UINT num) const;
		PCBYTE  GetBinary(void) const;
		DWORD   GetAddress(void) const;
		CString GetDotted(void) const;
		CString GetHost(void) const;

		// Socket Helpers
		void LoadSockAddr(sockaddr &addr, WORD Port) const;

	protected:
		// Data Members
		IN_ADDR m_Addr;
	};

//////////////////////////////////////////////////////////////////////////
//
// IP Port Address
//

class DLLAPI CIPPort : public CIPAddr
{
	public:
		// Constructors
		CIPPort(void);
		CIPPort(CIPPort const &That);
		CIPPort(sockaddr const &addr);
		CIPPort(PCTXT pHost);
		CIPPort(PCTXT pHost, WORD Port);
		CIPPort(CIPAddr const &Addr, WORD Port);
		CIPPort(WORD Port);

		// Assignment Operators
		CIPPort const & operator = (CIPPort const &That);
		CIPPort const & operator = (sockaddr const &addr);
		CIPPort const & operator = (PCTXT pHost);
		CIPPort const & operator = (WORD Port);

		// Initialisation
		BOOL Create(CIPPort const &That);
		BOOL Create(sockaddr const &addr);
		BOOL Create(PCTXT pHost);
		BOOL Create(PCTXT pHost, WORD Port);
		BOOL Create(CIPAddr const &Addr, WORD Port);
		BOOL Create(WORD Port);

		// Conversions
		operator CString(void) const;

		// Comparison Operators
		int operator == (CIPPort const &That) const;
		int operator != (CIPPort const &That) const;

		// Attributes
		WORD    GetPort(void) const;
		CString GetDottedWithPort(void) const;
		CString GetHostWithPort(void) const;

		// Operations
		void SetPort(WORD Port);

		// Socket Helpers
		void LoadSockAddr(sockaddr &addr) const;

	protected:
		// Data Members
		WORD m_Port;
	};

//////////////////////////////////////////////////////////////////////////
//
// Socket Event Wrapper Class
//

class DLLAPI CNetEvent : public WSANETWORKEVENTS
{
	public:
		// Constructor
		CNetEvent(void);

		// Operations
		void Reset(void);
		BOOL Check(UINT uBit) const;
		BOOL CheckRead(void) const;
		BOOL CheckWrite(void) const;
		BOOL CheckConnect(void) const;
		BOOL CheckAccept(void) const;
		BOOL CheckClose(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Socket 
//

class DLLAPI CSocket : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CSocket(void);
		CSocket(SOCKET s);
		CSocket(CSocket const &That);

		// Destructor
		~CSocket(void);

		// Attachment
		void   Attach(SOCKET s);
		void   Detach(void);
		SOCKET TakeOver(void);

		// Assignment Operators
		CSocket const & operator = (CSocket const &That);
		CSocket const & operator = (SOCKET s);

		// Conversion
		operator SOCKET (void) const;

		// Attributes
		SOCKET  GetHandle(void) const;
		BOOL    IsValid(void) const;
		BOOL    IsNull(void) const;
		CIPPort GetSocketName(void) const;
		CIPPort GetRemoteName(void) const;
		int     GetLastError(void) const;
		ULONG   GetRxCount(void) const;
	
		// Validation
		void AssertValid(void) const;

		// Creation
		void CreateTCP(void);
		void CreateUDP(void);
		void CreateICMP(void);

		// Operations
		BOOL Bind(CIPPort const &Local);
		BOOL Listen(int Backlog);
		BOOL Accept(CSocket &Session);
		BOOL Connect(CIPPort const &Remote);
		BOOL CanRead(void);
		BOOL CanWrite(void);
		BOOL Shutdown(BOOL fBoth);
		void Abort(void);
		BOOL Unlisten(void);
		BOOL SetNonBlocking(BOOL fEnable);
		INT  Recv(void *pData, UINT uLimit);
		INT  Send(void const *pData, UINT uCount);
		INT  SendTo(void const *pData, UINT uCount, CIPPort const &Dest);
		INT  RecvFrom(void *pData, UINT uLimit, CIPPort &Port);
		BOOL SetOption(INT nOpt, PVOID pData, UINT  uLen);
		BOOL GetOption(INT nOpt, PVOID pData, UINT &uLen) const;
		void SetLinger(WORD wPeriod);
		
		// Events
		BOOL EventClear(void);
		BOOL EventSelect(DWORD Mask, CEvent &Event);
		BOOL EventEnum(CNetEvent &NetEvent);
		BOOL EventEnum(CNetEvent &NetEvent, CEvent &Event);

	protected:
		// Data
		SOCKET m_s;
	};

//////////////////////////////////////////////////////////////////////////
//
// ICMP Pinger Object
//

class DLLAPI CPinger : public CObject
{
	public:
		// Constructor
		CPinger(void);

		// Destructor
		~CPinger(void);

		// Attributes
		UINT GetDelay(void) const;

		// Operations
		void SetTimeout(UINT uTimeout);
		BOOL Ping(CIPAddr const &Addr);
	
	protected:
		// Headers
		struct IPHeader;
		struct ICMPHeader;
		
		// Constants
		static WORD const ICMP_ECHO;
		static WORD const ICMP_ECHOREPLY;
		static WORD const ICMP_MIN;
		static UINT const MAX_PACKET;
		static UINT const DEF_PACKET;
			
		// Data
		UINT     m_uTimeout;
		UINT     m_uDelay;
		CSocket  m_Sock;
		BYTE   * m_pTxBuf;
		BYTE   * m_pRxBuf;

		// Implementation
		void OpenSocket(void);
		void CloseSocket(void);
		BOOL Decode(IPHeader *pRecv, int nSize);
		WORD Checksum(ICMPHeader *pSend, int nSize);

		// Buffers
		void AllocBuffers(UINT uTx, UINT uRx);
		void FreeBuffers(void);
	};

// End of File

#endif
