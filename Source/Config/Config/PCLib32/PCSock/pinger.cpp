
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2004 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ICMP Pinger Object
//

// Constants

WORD const CPinger::ICMP_ECHO	   = 8;

WORD const CPinger::ICMP_ECHOREPLY = 0;

WORD const CPinger::ICMP_MIN	   = 8;

UINT const CPinger::MAX_PACKET	   = 1024;

UINT const CPinger::DEF_PACKET	   = 32;

// Data Packing

#pragma pack(push, 4)

// Layout of IP Header

struct CPinger::IPHeader
{
	DWORD	h_len:4;
	DWORD	version:4;
	BYTE	tos;
	WORD	total_len;
	WORD	ident;
	WORD	frag_and_flags;
	BYTE	ttl;
	BYTE	proto;
	WORD	checksum;
	DWORD	sourceIP;
	DWORD	destIP;
	};

// Layout of ICMP Header

#pragma pack(push, 4)

struct CPinger::ICMPHeader
{
	BYTE	i_type;
	BYTE	i_code;
	WORD	i_cksum;
	WORD	i_id;
	WORD	i_seq;
	DWORD	timestamp;
	BYTE	data[];
	};

// Constructor

CPinger::CPinger(void)
{
	m_uTimeout = 0;

	m_uDelay   = NOTHING;

	m_pTxBuf   = NULL;

	m_pRxBuf   = NULL;

	OpenSocket();
	}

// Destructor

CPinger::~CPinger(void)
{
	FreeBuffers();

	CloseSocket();
	}

// Operations

void CPinger::SetTimeout(UINT uTimeout)
{
	m_uTimeout = uTimeout;
	}

BOOL CPinger::Ping(CIPAddr const &Addr)
{
	m_uDelay = NOTHING;

	if( !Addr.IsEmpty() ) {

		BOOL fOkay = FALSE;

		int  nData = DEF_PACKET;

		int  nSize = nData + sizeof(ICMPHeader);

		AllocBuffers(nSize, MAX_PACKET);

		ICMPHeader *pSend = (ICMPHeader *) m_pTxBuf;

		IPHeader   *pRecv = (IPHeader   *) m_pRxBuf;

		pSend->i_type	= ICMP_ECHO;
		pSend->i_code	= 0;
		pSend->i_id	= (WORD) GetCurrentProcessId();
		pSend->i_cksum	= 0;
		pSend->i_seq	= 0;
		
		memset(pSend->data, '*', nData);

		for( int i = 0; i < 4; i++ ) {

			DWORD t1 = GetTickCount();

			pSend->i_cksum	 = 0;
			pSend->timestamp = GetTickCount();
			pSend->i_seq	 = (WORD) i;
			pSend->i_cksum	 = Checksum(pSend, nSize);

			INT wr = m_Sock.SendTo(pSend, nSize, CIPPort(Addr, 0));

			if( wr == SOCKET_ERROR ) {

				if( m_Sock.GetLastError() == WSAEWOULDBLOCK ) {

					Sleep(20);

					continue;
					}
				
				FreeBuffers();

				return FALSE;
				}
			
			INT rd = 0;

			for(;;) {
			
				CIPPort From;
			
				rd = m_Sock.RecvFrom(pRecv, MAX_PACKET, From);

				if( rd == SOCKET_ERROR ) {

					if( m_Sock.GetLastError() == WSAEWOULDBLOCK ) {

						if( (GetTickCount() - t1) > m_uTimeout ) {

							break;
							}

						Sleep(10);

						continue;
						}

					FreeBuffers();

					return FALSE;
					}
				break;
				}

			if( Decode(pRecv, rd) ) {

				DWORD t2 = GetTickCount();

				m_uDelay = (t2 - t1);

				fOkay = TRUE;

				break;
				}
			}

		FreeBuffers();
			
		return fOkay;
		}

	return FALSE;
	}

// Attributes

UINT CPinger::GetDelay(void) const
{
	return m_uDelay;
	}

// Implementation

void CPinger::OpenSocket(void)
{
	m_Sock.CreateICMP();

	m_Sock.SetNonBlocking(TRUE);
	}

void CPinger::CloseSocket(void)
{
	m_Sock.Detach();
	}

BOOL CPinger::Decode(IPHeader *pRecv, int nSize)
{
	WORD wSize = (WORD) (pRecv->h_len * 4);

	if( nSize >= wSize + ICMP_MIN ) {

		ICMPHeader *pICMP = (ICMPHeader *) (PBYTE(pRecv) + wSize);

		if( pICMP->i_type == ICMP_ECHOREPLY ) {

			if( pICMP->i_id == (WORD) GetCurrentProcessId() ) {

				// NOTE: By here, the packet is the right
				// size, it contains an echo reply opcode,
				// and it was sent in response to our query.
		
				return TRUE;
				}
			}
		}

	return FALSE;
	}

WORD CPinger::Checksum(ICMPHeader *pSend, int nSize)
{
	PWORD pData = PWORD(pSend);

	DWORD dwSum = 0;

	while( nSize > 1 ) {

		dwSum += *(pData++);

		nSize -= sizeof(WORD);
		}

	if( nSize )
		dwSum += PBYTE(pData)[0];

	dwSum = (dwSum >> 16) + (dwSum & 0xFFFF);

	dwSum = dwSum + (dwSum >> 16);

	return (WORD) (~dwSum);
	}

// Buffers

void CPinger::AllocBuffers(UINT uSend, UINT uRecv)
{
	FreeBuffers();
	
	if( !m_pTxBuf ) {
	
		m_pTxBuf = New BYTE [ uSend ];
		}

	if( !m_pRxBuf ) {

		m_pRxBuf = New BYTE [ uRecv ];
		}
	
	memset(m_pTxBuf, 0, uSend);

	memset(m_pRxBuf, 0, uRecv);
	}

void CPinger::FreeBuffers(void)
{
	if( m_pTxBuf ) {

		delete [] m_pTxBuf;

		m_pTxBuf = NULL;
		}

	if( m_pRxBuf ) {

		delete [] m_pRxBuf;

		m_pRxBuf = NULL;
		}
	}

// End of File
