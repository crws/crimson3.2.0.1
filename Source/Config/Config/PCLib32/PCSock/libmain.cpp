
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2004 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		afxModule = New CModule(modCoreLibrary);

		if( !afxModule->InitLib(hThis) ) {

			AfxTrace("ERROR: Failed to load PCSock\n");

			delete afxModule;

			afxModule = NULL;

			return FALSE;
			}

		WORD Version = MAKEWORD(2, 0);

		WSADATA Data;
 
		WSAStartup(Version, &Data);

		return TRUE;
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		WSACleanup();

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
