
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// List View Control
//

// Dynamic Class

AfxImplementDynamicClass(CListView, CCtrlWnd);

// Constructor

CListView::CListView(void)
{
	LoadControlClass(ICC_LISTVIEW_CLASSES);
	}

// Attributes

CSize CListView::ApproximateViewRect(UINT uCount, CSize Size) const
{
	return CSize(DWORD(SendMessageConst(LVM_APPROXIMATEVIEWRECT, WPARAM(uCount), LPARAM(Size))));
	}

CSize CListView::ApproximateViewRect(UINT uCount) const
{
	return ApproximateViewRect(uCount, CSize(-1, -1));
	}

UINT CListView::FindItem(UINT uStart, LVFINDINFO &FindInfo) const
{
	return UINT(SendMessageConst(LVM_FINDITEM, uStart, LPARAM(&FindInfo)));
	}

CColor CListView::GetBkColor(void) const
{
	return CColor(SendMessageConst(LVM_GETBKCOLOR));
	}

BOOL CListView::GetBkImage(LVBKIMAGE &Image) const
{
	return BOOL(SendMessageConst(LVM_GETBKIMAGE, 0, LPARAM(&Image)));
	}

UINT CListView::GetCallbackMask(void) const
{
	return UINT(SendMessageConst(LVM_GETCALLBACKMASK));
	}

BOOL CListView::GetColumn(UINT uIndex, LVCOLUMN &Item) const
{
	return BOOL(SendMessageConst(LVM_GETCOLUMN, WPARAM(uIndex), LPARAM(&Item)));
	}

CListViewColumn CListView::GetColumn(UINT uIndex) const
{
	CListViewColumn Column;

	Column.SetMask( LVCF_FMT
		      | LVCF_WIDTH
		      | LVCF_TEXT
		      | LVCF_SUBITEM
		      | LVCF_IMAGE
		      | LVCF_ORDER
		      );
	
	Column.SetTextBuffer(256);

	GetColumn(uIndex, Column);

	return Column;
	}

UINT CListView::GetColumnFormat(UINT uIndex) const
{
	CListViewColumn Column;
	
	Column.SetMask(LVCF_FMT);

	GetColumn(uIndex, Column);

	return Column.GetFormat();
	}

UINT CListView::GetColumnImage(UINT uIndex) const
{
	CListViewColumn Column;
	
	Column.SetMask(LVCF_IMAGE);

	GetColumn(uIndex, Column);

	return Column.GetImage();
	}

CString CListView::GetColumnText(UINT uIndex) const
{
	CListViewColumn Column;
	
	Column.SetTextBuffer(256);

	GetColumn(uIndex, Column);

	return Column.GetText();
	}

int CListView::GetColumnWidth(UINT uColumn) const
{
	return int(SendMessageConst(LVM_GETCOLUMNWIDTH, uColumn));
	}

BOOL CListView::GetColumnOrderArray(UINT uCount, UINT *pArray) const
{
	return BOOL(SendMessageConst(LVM_GETCOLUMNORDERARRAY, WPARAM(uCount), LPARAM(pArray)));
	}

BOOL CListView::GetColumnOrderArray(CArray <UINT> &Array) const
{
	Array.Empty();

	UINT uCount = GetItemCount();

	Array.Expand(uCount);

	return GetColumnOrderArray(uCount, (UINT *) Array.GetPointer());
	}

UINT CListView::GetCountPerPage(void) const
{
	return UINT(SendMessageConst(LVM_GETCOUNTPERPAGE));
	}

CEditCtrl & CListView::GetEditControl(void) const
{
	return CEditCtrl::FromHandle(HWND(SendMessageConst(LVM_GETEDITCONTROL)));
	}

DWORD CListView::GetExtendedListViewStyle(void) const
{
	return UINT(SendMessageConst(LVM_GETEXTENDEDLISTVIEWSTYLE));
	}

CHeaderCtrl & CListView::GetHeader(void) const
{
	return CHeaderCtrl::FromHandle(HWND(SendMessageConst(LVM_GETHEADER)));
	}

HCURSOR CListView::GetHotCursor(void) const
{
	return HCURSOR(SendMessageConst(LVM_GETHOTCURSOR));
	}

UINT CListView::GetHotItem(void) const
{
	return UINT(SendMessageConst(LVM_GETHOTITEM));
	}

UINT CListView::GetHoverTime(void) const
{
	return UINT(SendMessageConst(LVM_GETHOVERTIME));
	}

CImageList & CListView::GetImageList(UINT uList) const
{
	return CImageList::FromHandle(HIMAGELIST(SendMessageConst(LVM_GETIMAGELIST, uList)));
	}

CString CListView::GetSearchString(void) const
{
	TCHAR sBuffer[256] = { 0 };

	UINT uCount = SendMessageConst(LVM_GETISEARCHSTRING, 0, LPARAM(sBuffer));

	AfxAssert(uCount < sizeof(sBuffer));

	return sBuffer;
	}

BOOL CListView::GetItem(LVITEM &Item) const
{
	return BOOL(SendMessageConst(LVM_GETITEM, 0, LPARAM(&Item)));
	}

BOOL CListView::GetItem(UINT uItem, UINT uSub, LVITEM &Item) const
{
	Item.iItem    = uItem;

	Item.iSubItem = uSub;

	return BOOL(SendMessageConst(LVM_GETITEM, 0, LPARAM(&Item)));
	}

BOOL CListView::GetItem(UINT uItem, LVITEM &Item) const
{
	Item.iItem    = uItem;

	return BOOL(SendMessageConst(LVM_GETITEM, 0, LPARAM(&Item)));
	}

CListViewItem CListView::GetItem(UINT uItem, UINT uSub) const
{
	CListViewItem Item;

	Item.SetItem(uItem, uSub);

	Item.SetMask( LVIF_PARAM
		    | LVIF_STATE
		    | LVIF_TEXT
		    | LVIF_IMAGE
		    | LVIF_INDENT
		    );
	
	Item.SetTextBuffer(256);

	GetItem(Item);

	return Item;
	}

CListViewItem CListView::GetItem(UINT uItem) const
{
	CListViewItem Item;

	Item.SetItem(uItem);

	Item.SetMask( LVIF_PARAM
		    | LVIF_STATE
		    | LVIF_TEXT
		    | LVIF_IMAGE
		    | LVIF_INDENT
		    );
	
	Item.SetTextBuffer(256);

	GetItem(Item);

	return Item;
	}

UINT CListView::GetItemImage(UINT uItem, UINT uSub) const
{
	CListViewItem Item;

	Item.SetItem(uItem, uSub);

	Item.SetMask(LVIF_IMAGE);

	GetItem(Item);

	return Item.GetImage();
	}

UINT CListView::GetItemImage(UINT uItem) const
{
	CListViewItem Item;

	Item.SetItem(uItem);

	Item.SetMask(LVIF_IMAGE);

	GetItem(Item);

	return Item.GetImage();
	}

LPARAM CListView::GetItemParam(UINT uItem, UINT uSub) const
{
	CListViewItem Item;

	Item.SetItem(uItem, uSub);

	Item.SetMask(LVIF_PARAM);

	GetItem(Item);

	return Item.GetParam();
	}

LPARAM CListView::GetItemParam(UINT uItem) const
{
	CListViewItem Item;

	Item.SetItem(uItem);

	Item.SetMask(LVIF_PARAM);

	GetItem(Item);

	return Item.GetParam();
	}

CString CListView::GetItemText(UINT uItem, UINT uSub) const
{
	CListViewItem Item;

	Item.SetItem(uItem, uSub);

	Item.SetTextBuffer(256);

	GetItem(Item);

	return Item.GetText();
	}

CString CListView::GetItemText(UINT uItem) const
{
	CListViewItem Item;

	Item.SetItem(uItem);

	Item.SetTextBuffer(256);

	GetItem(Item);

	return Item.GetText();
	}

UINT CListView::GetItemCount(void) const
{
	return UINT(SendMessageConst(LVM_GETITEMCOUNT));
	}

CPoint CListView::GetItemPosition(UINT uItem) const
{
	CPoint Result;

	SendMessageConst(LVM_GETITEMPOSITION, uItem, LPARAM(&Result));

	return Result;
	}

CRect CListView::GetItemRect(UINT uItem, UINT uCode) const
{
	CRect Result;

	Result.left = int(uCode);

	SendMessageConst(LVM_GETITEMRECT, uItem, LPARAM(&Result));

	return Result;
	}

int CListView::GetItemSpacing(BOOL fSmall) const
{
	return int(SendMessageConst(LVM_GETITEMSPACING, fSmall));
	}

UINT CListView::GetItemState(UINT uItem, UINT uMask) const
{
	return UINT(SendMessageConst(LVM_GETITEMSTATE, uItem, uMask));
	}

UINT CListView::GetNextItem(UINT uStart, UINT uFlags) const
{
	return UINT(SendMessageConst(LVM_GETNEXTITEM, uStart, uFlags));
	}

UINT CListView::GetNumberOfWorkAreas(void) const
{
	UINT n = 0;

	SendMessageConst(LVM_GETNUMBEROFWORKAREAS, 0, LPARAM(&n));

	return n;
	}

CPoint CListView::GetOrigin(void) const
{
	CPoint Result;

	SendMessageConst(LVM_GETORIGIN, 0, LPARAM(&Result));

	return Result;
	}

UINT CListView::GetSelection(void) const
{
	UINT uCount = GetSelectedCount();

	AfxAssert(uCount <= 1);

	if( uCount ) {

		UINT uItems = GetItemCount();

		for( UINT uIndex = 0; uIndex < uItems; uIndex++ ) {

			if( GetItemState(uIndex, LVIS_SELECTED) ) {

				return uIndex;
				}
			}

		AfxAssert(FALSE);
		}

	return NOTHING;
	}

UINT CListView::GetSelectionList(CArray <UINT> &Array) const
{
	UINT uCount = GetSelectedCount();

	UINT uItems = GetItemCount();

	for( UINT uIndex = 0; uIndex < uItems; uIndex++ ) {

		if( GetItemState(uIndex, LVIS_SELECTED) ) {

			Array.Append(uIndex);

			if( --uCount == 0 )
				break;
			}
		}

	return Array.GetCount();
	}

UINT CListView::GetSelectedCount(void) const
{
	return UINT(SendMessageConst(LVM_GETSELECTEDCOUNT));
	}

UINT CListView::GetSelectionMark(void) const
{
	return UINT(SendMessageConst(LVM_GETSELECTIONMARK));
	}

int CListView::GetStringWidth(PCTXT pString) const
{
	return int(SendMessageConst(LVM_GETSTRINGWIDTH, 0, LPARAM(pString)));
	}

CRect CListView::GetSubItemRect(UINT uItem, UINT uSub, UINT uCode) const
{
	CRect Result;

	Result.top  = int(uSub);

	Result.left = int(uCode);

	SendMessageConst(LVM_GETSUBITEMRECT, uItem, LPARAM(&Result));

	return Result;
	}

CColor CListView::GetTextBkColor(void) const
{
	return CColor(SendMessageConst(LVM_GETTEXTBKCOLOR));
	}

CColor CListView::GetTextColor(void) const
{
	return CColor(SendMessageConst(LVM_GETTEXTCOLOR));
	}

CToolTip & CListView::GetToolTips(void) const
{
	return CToolTip::FromHandle(HWND(SendMessageConst(LVM_GETTOOLTIPS)));
	}

UINT CListView::GetTopIndex(void) const
{
	return UINT(SendMessageConst(LVM_GETTOPINDEX));
	}

CRect CListView::GetViewRect(void) const
{
	CRect Result;

	SendMessageConst(LVM_GETVIEWRECT, 0, LPARAM(&Result));

	return Result;
	}

void CListView::GetWorkAreas(UINT uCount, RECT *pRects) const
{
	SendMessageConst(LVM_GETWORKAREAS, WPARAM(uCount), LPARAM(pRects));
	}

void CListView::GetWorkAreas(CArray <CRect> &Array) const
{
	Array.Empty();

	UINT uCount = GetNumberOfWorkAreas();

	Array.Expand(uCount);

	GetWorkAreas(uCount, (RECT *) Array.GetPointer());
	}

UINT CListView::HitTest(BOOL fSub, LVHITTESTINFO &Info) const
{
	return UINT(SendMessageConst(fSub ? LVM_SUBITEMHITTEST : LVM_HITTEST, 0, LPARAM(&Info)));
	}

UINT CListView::HitTestItem(BOOL fSub, CPoint const &Pos) const
{
	LVHITTESTINFO Info;

	Info.pt = Pos;

	HitTest(fSub, Info);

	return Info.iItem;
	}

UINT CListView::HitTestSubItem(BOOL fSub, CPoint const &Pos) const
{
	LVHITTESTINFO Info;

	Info.pt = Pos;

	HitTest(fSub, Info);

	return Info.iSubItem;
	}

UINT CListView::HitTestFlags(BOOL fSub, CPoint const &Pos) const
{
	LVHITTESTINFO Info;

	Info.pt = Pos;

	HitTest(fSub, Info);

	return Info.flags;
	}

UINT CListView::HitTestItem(BOOL fSub, int xPos, int yPos) const
{
	return HitTestItem(fSub, CPoint(xPos, yPos));
	}

UINT CListView::HitTestSubItem(BOOL fSub, int xPos, int yPos) const
{
	return HitTestSubItem(fSub, CPoint(xPos, yPos));
	}

UINT CListView::HitTestFlags(BOOL fSub, int xPos, int yPos) const
{
	return HitTestFlags(fSub, CPoint(xPos, yPos));
	}

// Operations

BOOL CListView::Arrange(UINT uCode)
{
	return BOOL(SendMessage(LVM_ARRANGE, uCode));
	}

CImageList & CListView::CreateDragImage(UINT uItem)
{
	return CImageList::FromHandle(HIMAGELIST(SendMessage(LVM_CREATEDRAGIMAGE, uItem)));
	}

BOOL CListView::DeleteAllItems(void)
{
	return BOOL(SendMessage(LVM_DELETEALLITEMS));
	}

BOOL CListView::DeleteColumn(UINT uColumn)
{
	return BOOL(SendMessage(LVM_DELETECOLUMN, uColumn));
	}

BOOL CListView::DeleteItem(UINT uItem)
{
	return BOOL(SendMessage(LVM_DELETEITEM, uItem));
	}

BOOL CListView::EditLabel(UINT uItem)
{
	return SendMessage(LVM_EDITLABEL, uItem) ? TRUE : FALSE;
	}

BOOL CListView::EnsureVisible(UINT uItem, BOOL fPartial)
{
	return BOOL(SendMessage(LVM_ENSUREVISIBLE, uItem, fPartial));
	}

UINT CListView::InsertColumn(UINT uIndex, LVCOLUMN const &Column)
{
	return UINT(SendMessage(LVM_INSERTCOLUMN, WPARAM(uIndex), LPARAM(&Column)));
	}

UINT CListView::InsertItem(LVITEM const &Item)
{
	return UINT(SendMessage(LVM_INSERTITEM, 0, LPARAM(&Item)));
	}

BOOL CListView::RedrawItems(UINT uFirst, UINT uLast)
{
	return BOOL(SendMessage(LVM_REDRAWITEMS, uFirst, uLast));
	}

BOOL CListView::Scroll(CSize const &Size)
{
	return BOOL(SendMessage(LVM_SCROLL, Size.cx, Size.cy));
	}

BOOL CListView::Scroll(int xSize, int ySize)
{
	return BOOL(SendMessage(LVM_SCROLL, xSize, ySize));
	}

BOOL CListView::SetBkColor(CColor const &Color)
{
	return BOOL(SendMessage(LVM_SETBKCOLOR, 0, LPARAM(Color)));
	}

BOOL CListView::SetBkImage(LVBKIMAGE &Image)
{
	return BOOL(SendMessage(LVM_SETBKIMAGE, 0, LPARAM(&Image)));
	}

BOOL CListView::SetCallbackMask(UINT uMask)
{
	return BOOL(SendMessage(LVM_SETCALLBACKMASK, uMask));
	}

BOOL CListView::SetColumn(UINT uIndex, LVCOLUMN const &Column)
{
	return BOOL(SendMessage(LVM_SETCOLUMN, WPARAM(uIndex), LPARAM(&Column)));
	}

BOOL CListView::SetColumnOrderArray(UINT uCount, UINT const *pArray)
{
	return BOOL(SendMessage(LVM_SETCOLUMNORDERARRAY, WPARAM(uCount), LPARAM(pArray)));
	}

BOOL CListView::SetColumnOrderArray(CArray <UINT> const &Array)
{
	return SetColumnOrderArray(Array.GetCount(), Array.GetPointer());
	}

BOOL CListView::SetColumnWidth(UINT uIndex, int xSize)
{
	return BOOL(SendMessage(LVM_SETCOLUMNWIDTH, uIndex, xSize));
	}

void CListView::SetExtendedListViewStyle(DWORD dwMask, DWORD dwStyle)
{
	SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, WPARAM(dwMask), LPARAM(dwStyle));
	}

void CListView::SetHotCursor(HCURSOR hCursor)
{
	SendMessage(LVM_SETHOTCURSOR, 0, LPARAM(hCursor));
	}

UINT CListView::SetHotItem(UINT uItem)
{
	return UINT(SendMessage(LVM_SETHOTITEM, WPARAM(uItem)));
	}

UINT CListView::SetHoverTime(UINT uTime)
{
	return UINT(SendMessage(LVM_SETHOVERTIME, WPARAM(uTime)));
	}

CSize CListView::SetIconSpacing(CSize Size)
{
	return CSize(SendMessage(LVM_SETICONSPACING, 0, LPARAM(Size)));
	}

void CListView::SetImageList(UINT uList, HIMAGELIST hList)
{
	SendMessage(LVM_SETIMAGELIST, uList, LPARAM(hList));
	}

BOOL CListView::SetItem(LVITEM &Item)
{
	return BOOL(SendMessage(LVM_SETITEM, 0, LPARAM(&Item)));
	}

void CListView::SetItemCount(UINT uCount)
{
	SendMessage(LVM_SETITEMCOUNT, uCount);
	}

BOOL CListView::SetItemPosition(UINT uItem, CPoint const &Pos)
{
	return BOOL(SendMessage(LVM_SETITEMPOSITION, uItem, LPARAM(Pos)));
	}

BOOL CListView::SetItemPosition(UINT uItem, int xPos, int yPos)
{
	return BOOL(SendMessage(LVM_SETITEMPOSITION, uItem, MAKELPARAM(xPos, yPos)));
	}

BOOL CListView::SetItemState(UINT uItem, UINT uMask, UINT uState)
{
	CListViewItem Item;

	Item.SetStateMask(uMask);

	Item.SetState(uState);

	return BOOL(SendMessage(LVM_SETITEMSTATE, uItem, LPARAM(&Item)));
	}

BOOL CListView::SetItemText(UINT uItem, UINT uSub, PCTXT pText)
{
	CListViewItem Item;

	Item.SetItem(uItem, uSub);

	Item.SetText(pText);

	return BOOL(SendMessage(LVM_SETITEMTEXT, uItem, LPARAM(&Item)));
	}

BOOL CListView::SetItemText(UINT uItem, PCTXT pText)
{
	CListViewItem Item;

	Item.SetItem(uItem);

	Item.SetText(pText);

	return BOOL(SendMessage(LVM_SETITEMTEXT, uItem, LPARAM(&Item)));
	}

BOOL CListView::SetItemParam(UINT uItem, LPARAM lParam)
{
	CListViewItem Item;

	Item.SetItem(uItem);

	Item.SetParam(lParam);

	return SetItem(Item);
}

BOOL CListView::SetItemParam(UINT uItem, UINT uSub, LPARAM lParam)
{
	CListViewItem Item;

	Item.SetItem(uItem);

	Item.SetSubItem(uSub);

	Item.SetParam(lParam);

	return SetItem(Item);
}

UINT CListView::SetSelectionMark(UINT uItem)
{
	return UINT(SendMessage(LVM_SETSELECTIONMARK, WPARAM(uItem)));
	}

BOOL CListView::SetTextBkColor(CColor const &Color)
{
	return BOOL(SendMessage(LVM_SETTEXTBKCOLOR, 0, LPARAM(Color)));
	}

BOOL CListView::SetTextColor(CColor const &Color)
{
	return BOOL(SendMessage(LVM_SETTEXTCOLOR, 0, LPARAM(Color)));
	}

void CListView::SetToolTips(CToolTip &Tips)
{
	SendMessage(LVM_SETTOOLTIPS, WPARAM(HWND(Tips)));
	}

void CListView::SetWorkAreas(UINT uCount, RECT const *pRects)
{
	SendMessageConst(LVM_SETWORKAREAS, WPARAM(uCount), LPARAM(pRects));
	}

void CListView::SetWorkAreas(CArray <CRect> const &Array)
{
	SetWorkAreas(Array.GetCount(), (RECT *) Array.GetPointer());
	}

void CListView::SetWorkArea(CRect const &Rect)
{
	SetWorkAreas(1, &Rect);
	}

BOOL CListView::SortItems(FARPROC pfnCompare, LPARAM lParam)
{
	return BOOL(SendMessage(LVM_SORTITEMS, WPARAM(lParam), LPARAM(pfnCompare)));
	}

BOOL CListView::SortItemsEx(FARPROC pfnCompare, LPARAM lParam)
{
	return BOOL(SendMessage(LVM_SORTITEMSEX, WPARAM(lParam), LPARAM(pfnCompare)));
	}

BOOL CListView::Update(void)
{
	return BOOL(SendMessage(LVM_UPDATE));
	}

// Handle Lookup

CListView & CListView::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CListView NullObject;

		return NullObject;
		}

	return (CListView &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Routing Control

BOOL CListView::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	CEditCtrl &Edit = GetEditControl();

	if( Edit.GetHandle() ) {

		if( Edit.RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CCtrlWnd::OnRouteMessage(Message, lResult);
	}

// Default Class Name

PCTXT CListView::GetDefaultClassName(void) const
{
	return WC_LISTVIEW;
	}

// End of File
