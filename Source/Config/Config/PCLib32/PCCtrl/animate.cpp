
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Animation Control
//

// Dynamic Class

AfxImplementDynamicClass(CAnimation, CCtrlWnd);

// Constructor

CAnimation::CAnimation(void)
{
	LoadControlClass(ICC_ANIMATE_CLASS);
	}

// Operations

BOOL CAnimation::Open(PCTXT pFilename)
{
	return BOOL(SendMessage(ACM_OPEN, 0, LPARAM(pFilename)));
	}

BOOL CAnimation::Close(void)
{
	return BOOL(SendMessage(ACM_OPEN, 0, 0));
	}

BOOL CAnimation::Play(UINT uRepeat, UINT uFrom, UINT uTo)
{
	return BOOL(SendMessage(ACM_PLAY, uRepeat, MAKELPARAM(uFrom, uTo)));
	}

BOOL CAnimation::Play(UINT uRepeat)
{
	return BOOL(SendMessage(ACM_PLAY, uRepeat, MAKELPARAM(-1, -1)));
	}

BOOL CAnimation::Stop(void)
{
	return BOOL(SendMessage(ACM_STOP));
	}

// Handle Lookup

CAnimation & CAnimation::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CAnimation NullObject;

		return NullObject;
		}

	return (CAnimation &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CAnimation::GetDefaultClassName(void) const
{
	return ANIMATE_CLASS;
	}

// End of File
