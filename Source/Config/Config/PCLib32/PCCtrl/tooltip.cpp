

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// ToolTip Control
//

// Static Data

BOOL CToolTip::m_fAsked = FALSE;

// Dynamic Class

AfxImplementDynamicClass(CToolTip, CCtrlWnd);

// Constructor

CToolTip::CToolTip(void)
{
	LoadControlClass(ICC_BAR_CLASSES);
	}

// Creation

BOOL CToolTip::Create(HWND hParent, DWORD dwStyle)
{
	DWORD dwExStyle = WS_EX_TOPMOST | WS_EX_TOOLWINDOW;

	if( !afxWin2K ) { 

		dwExStyle |= WS_EX_TRANSPARENT;
		}

	if( CWnd::Create(dwExStyle, dwStyle | WS_POPUP, CRect(), hParent, NULL, NULL) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CToolTip::Create(HWND hParent, DWORD dwExStyle, DWORD dwStyle)
{
	if( CWnd::Create(dwExStyle, dwStyle | WS_POPUP, CRect(), hParent, NULL, NULL) ) {

		return TRUE;
		}

	return FALSE;
	}

// Enable Status

BOOL CToolTip::AreBalloonsEnabled(void)
{
	CRegKey Key(HKEY_CURRENT_USER);

	Key.MoveTo(L"Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced");

	return Key.GetValue(L"EnableBalloonTips", DWORD(1));
	}

BOOL CToolTip::AreBalloonsPossible(void)
{
	return !m_fAsked || AreBalloonsEnabled();
	}

// Enable Control

BOOL CToolTip::EnableBalloons(BOOL fPrompt)
{
	if( fPrompt ) {

		if( m_fAsked ) {

			return FALSE;
			}
		else {
			CString Text = CString(IDS_BALLOON_TIPS_1) +
				       CString(IDS_BALLOON_TIPS_2) ;

			if( afxMainWnd->NoYes(Text) == IDNO ) {

				m_fAsked = TRUE;

				return FALSE;
				}

			CString Warn = CString(IDS_BALLOON_TIPS_3) +
				       CString(IDS_BALLOON_TIPS_4) ;

			afxMainWnd->Information(Warn);

			m_fAsked = TRUE;
			}
		}

	CRegKey Key(HKEY_CURRENT_USER);

	Key.MoveTo(L"Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced");

	Key.SetValue(L"EnableBalloonTips", DWORD(1));

	return TRUE;
	}

// Attributes

CRect CToolTip::AdjustRect(BOOL fGrow, RECT const &Rect) const
{
	CRect Work = Rect;

	SendMessageConst(TTM_ADJUSTRECT, fGrow, LPARAM(&Work));

	return Work;
	}

BOOL CToolTip::EnumTools(UINT uIndex, TOOLINFO &Info) const
{
	return BOOL(SendMessageConst(TTM_ENUMTOOLS, uIndex, LPARAM(&Info)));
	}

CToolInfo CToolTip::EnumTools(UINT uIndex) const
{
	CToolInfo Info;

	Info.SetTextBuffer(256);

	EnumTools(uIndex, Info);

	return Info;
	}

CSize CToolTip::GetBubbleSize(TOOLINFO &Info) const
{
	return CSize(DWORD(SendMessageConst(TTM_GETBUBBLESIZE, 0, LPARAM(&Info))));
	}

CSize CToolTip::GetBubbleSize(HWND hWnd, UINT uID) const
{
	return GetBubbleSize(CToolInfo(hWnd, uID));
	}

CSize CToolTip::GetBubbleSize(HWND hWnd, HWND hCtrl) const
{
	return GetBubbleSize(CToolInfo(hWnd, hCtrl));
	}

BOOL CToolTip::HasCurrentTool(void) const
{
	return BOOL(SendMessageConst(TTM_GETCURRENTTOOL));
	}

BOOL CToolTip::GetCurrentTool(TOOLINFO &Info) const
{
	return BOOL(SendMessageConst(TTM_GETCURRENTTOOL, 0, LPARAM(&Info)));
	}

CToolInfo CToolTip::GetCurrentTool(void) const
{
	CToolInfo Info;

	Info.SetTextBuffer(256);

	GetCurrentTool(Info);

	return Info;
	}

INT CToolTip::GetDelayTime(UINT uCode) const
{
	return INT(SendMessageConst(TTM_GETDELAYTIME, WPARAM(uCode)));
	}

void CToolTip::GetMargin(RECT &Rect) const
{
	SendMessageConst(TTM_GETMARGIN, 0, LPARAM(&Rect));
	}

CRect CToolTip::GetMargin(void) const
{
	CRect Rect;

	GetMargin(Rect);

	return Rect;
	}

INT CToolTip::GetMaxTipWidth(void) const
{
	return INT(SendMessageConst(TTM_GETMAXTIPWIDTH));
	}

BOOL CToolTip::GetText(TOOLINFO &Info) const
{
	return BOOL(SendMessageConst(TTM_GETTEXT, 0, LPARAM(&Info)));
	}

CString CToolTip::GetText(HWND hWnd, UINT uID) const
{
	CToolInfo Info(hWnd, uID);

	Info.SetTextBuffer(256);

	GetText(Info);

	return Info.GetText();
	}

CString	CToolTip::GetText(HWND hWnd, HWND hCtrl) const
{
	CToolInfo Info(hWnd, hCtrl);

	Info.SetTextBuffer(256);

	GetText(Info);

	return Info.GetText();
	}

CColor CToolTip::GetTipBkColor(void) const
{
	return CColor(COLORREF(SendMessageConst(TTM_GETTIPBKCOLOR)));
	}

CColor CToolTip::GetTipTextColor(void) const
{
	return CColor(COLORREF(SendMessageConst(TTM_GETTIPTEXTCOLOR)));
	}

UINT CToolTip::GetToolCount(void) const
{
	return UINT(SendMessageConst(TTM_GETTOOLCOUNT));
	}

BOOL CToolTip::GetToolInfo(TOOLINFO &Info) const
{
	return BOOL(SendMessageConst(TTM_GETTOOLINFO, 0, LPARAM(&Info)));
	}

CToolInfo CToolTip::GetToolInfo(HWND hWnd, UINT uID) const
{
	CToolInfo Info(hWnd, uID);

	GetToolInfo(Info);

	return Info;
	}

CToolInfo CToolTip::GetToolInfo(HWND hWnd, HWND hCtrl) const
{
	CToolInfo Info(hWnd, hCtrl);

	GetToolInfo(Info);

	return Info;
	}

BOOL CToolTip::HitTest(TTHITTESTINFO &Test) const
{
	return BOOL(SendMessageConst(TTM_HITTEST, 0, LPARAM(&Test)));
	}

BOOL CToolTip::HitTest(CToolInfo &Info, HWND hWnd, CPoint const &Pos) const
{
	TTHITTESTINFO Test;

	Test.hwnd      = hWnd;

	Test.pt        = Pos;

	Test.ti.cbSize = sizeof(Test.ti.cbSize);

	if( HitTest(Test) ) {

		Info = Test.ti;

		return TRUE;
		}

	return FALSE;
	}

CToolInfo CToolTip::HitTest(HWND hWnd, CPoint const &Pos) const
{
	CToolInfo Info;

	HitTest(Info, hWnd, Pos);

	return Info;
	}

CWnd & CToolTip::WindowFromPoint(CPoint const &Pos) const
{
	return CWnd::FromHandle(HWND(SendMessageConst(TTM_WINDOWFROMPOINT, 0, LPARAM(&Pos))));
	}

// Operations

void CToolTip::Activate(BOOL fActivate)
{
	SendMessage(TTM_ACTIVATE, fActivate);
	}

BOOL CToolTip::AddTool(TOOLINFO const &Info)
{
	return BOOL(SendMessage(TTM_ADDTOOL, 0, LPARAM(&Info)));
	}

void CToolTip::DelTool(TOOLINFO const &Info)
{
	SendMessage(TTM_DELTOOL, 0, LPARAM(&Info));
	}

void CToolTip::DelTool(HWND hWnd, UINT uID)
{
	DelTool(CToolInfo(hWnd, uID));
	}

void CToolTip::DelTool(HWND hWnd, HWND hCtrl)
{
	DelTool(CToolInfo(hWnd, hCtrl));
	}

void CToolTip::NewToolRect(TOOLINFO const &Info)
{
	SendMessage(TTM_NEWTOOLRECT, 0, LPARAM(&Info));
	}

void CToolTip::NewToolRect(HWND hWnd, UINT uID, CRect const &Rect)
{
	CToolInfo Info(hWnd, uID);

	Info.SetRect(Rect);

	NewToolRect(Info);
	}

void CToolTip::NewToolRect(HWND hWnd, HWND hCtrl, CRect const &Rect)
{
	CToolInfo Info(hWnd, hCtrl);

	Info.SetRect(Rect);

	NewToolRect(Info);
	}

void CToolTip::Pop(void)
{
	SendMessage(TTM_POP);
	}

void CToolTip::RelayEvent(MSG &Message)
{
	SendMessage(TTM_RELAYEVENT, 0, LPARAM(&Message));
	}

void CToolTip::SetDelayTime(UINT uCode, UINT uDelay)
{
	SendMessage(TTM_SETDELAYTIME, uCode, uDelay);
	}

void CToolTip::SetMargin(RECT const &Rect)
{
	SendMessage(TTM_SETMARGIN, 0, LPARAM(&Rect));
	}

void CToolTip::SetMaxTipWidth(INT nMax)
{
	SendMessage(TTM_SETMAXTIPWIDTH, 0, LPARAM(nMax));
	}

void CToolTip::SetTipBkColor(CColor const &Color)
{
	SendMessage(TTM_SETTIPBKCOLOR, WPARAM(COLORREF(Color)));
	}

void CToolTip::SetTipTextColor(CColor const &Color)
{
	SendMessage(TTM_SETTIPTEXTCOLOR, WPARAM(COLORREF(Color)));
	}

void CToolTip::SetTitle(UINT Icon, PCTXT pText)
{
	SendMessage(TTM_SETTITLE, WPARAM(Icon), LPARAM(pText));
	}

void CToolTip::SetToolInfo(TOOLINFO const &Info)
{
	SendMessage(TTM_SETTOOLINFO, 0, LPARAM(&Info));
	}

void CToolTip::TrackActivate(BOOL fActive, TOOLINFO const &Info)
{
	SendMessage(TTM_TRACKACTIVATE, fActive, LPARAM(&Info));
	}

void CToolTip::TrackActivate(BOOL fActive, HWND hWnd, UINT uID)
{
	TrackActivate(fActive, CToolInfo(hWnd, uID));
	}

void CToolTip::TrackActivate(BOOL fActive, HWND hWnd, HWND hCtrl)
{
	TrackActivate(fActive, CToolInfo(hWnd, hCtrl));
	}

void CToolTip::TrackPosition(CPoint const &Pos)
{
	SendMessage(TTM_TRACKPOSITION, 0, LPARAM(DWORD(Pos)));
	}

void CToolTip::Update(void)
{
	SendMessage(TTM_UPDATE);
	}

void CToolTip::UpdateTipText(TOOLINFO const &Info)
{
	SendMessage(TTM_UPDATETIPTEXT, 0, LPARAM(&Info));
	}

void CToolTip::UpdateTipText(HWND hWnd, UINT uID, PCTXT pText)
{
	CToolInfo Info(hWnd, uID);

	Info.SetText(pText);

	UpdateTipText(Info);
	}

void CToolTip::UpdateTipText(HWND hWnd, HWND hCtrl, PCTXT pText)
{
	CToolInfo Info(hWnd, hCtrl);

	Info.SetText(pText);

	UpdateTipText(Info);
	}

// Handle Lookup

CToolTip & CToolTip::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CToolTip NullObject;

		return NullObject;
		}

	return (CToolTip &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CToolTip::GetDefaultClassName(void) const
{
	return TOOLTIPS_CLASS;
	}

// End of File
