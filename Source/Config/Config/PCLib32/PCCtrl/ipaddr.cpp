
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IP Address Control
//

// Dynamic Class

AfxImplementDynamicClass(CIPAddrCtrl, CCtrlWnd);

// Constructor

CIPAddrCtrl::CIPAddrCtrl(void)
{
	LoadControlClass(ICC_INTERNET_CLASSES);
	}

// Attributes

DWORD CIPAddrCtrl::GetAddress(void) const
{
	DWORD dwAddr = 0;

	SendMessageConst(IPM_GETADDRESS, 0, LPARAM(&dwAddr));

	return dwAddr;
	}

BOOL CIPAddrCtrl::IsBlank(void) const
{
	return BOOL(SendMessageConst(IPM_ISBLANK));
	}

// Operations

void CIPAddrCtrl::ClearAddress(void)
{
	SendMessageConst(IPM_CLEARADDRESS);
	}

void CIPAddrCtrl::SetAddress(DWORD dwAddr)
{
	SendMessageConst(IPM_SETADDRESS, 0, LPARAM(dwAddr));
	}

void CIPAddrCtrl::SetRange(UINT uIndex, CRange const &Range)
{
	AfxValidateReadPtr(&Range, sizeof(Range));

	SendMessageConst(IPM_SETRANGE, uIndex, LPARAM(WORD(Range)));
	}

void CIPAddrCtrl::SetFocus(UINT uIndex)
{
	SendMessageConst(IPM_SETFOCUS, uIndex);
	}

void CIPAddrCtrl::SetFocus(void)
{
	CWnd::SetFocus();
	}

// Clipboard Support

void CIPAddrCtrl::Cut(void)
{
	if( Copy() ) {

		Clear();
		}
	}

BOOL CIPAddrCtrl::Copy(void)
{
	CClipboard Clip(ThisObject);

	if( Clip.IsValid() ) {

		DWORD Addr = GetAddress();

		CPrintf Text( L"%u.%u.%u.%u",
			      HIBYTE(HIWORD(Addr)),
			      LOBYTE(HIWORD(Addr)),
			      HIBYTE(LOWORD(Addr)),
			      LOBYTE(LOWORD(Addr))
			      );

		Clip.SetText(Text);

		return TRUE;
		}

	return FALSE;
	}

void CIPAddrCtrl::Paste(void)
{
	CClipboard Clip(ThisObject);

	if( Clip.IsValid() ) {

		CString Text = Clip.GetText(CF_UNICODETEXT);

		if( !Text.IsEmpty() ) {

			CStringArray List;

			if( Text.Tokenize(List, '.') == 4 ) {

				BYTE b0 = BYTE(watoi(List[0]));
				BYTE b1 = BYTE(watoi(List[1]));
				BYTE b2 = BYTE(watoi(List[2]));
				BYTE b3 = BYTE(watoi(List[3]));

				SetAddress(MAKELONG(MAKEWORD(b3,b2),MAKEWORD(b1,b0)));

				SetFocus(0);
				
				return;
				}
			}

		MessageBeep(0);
		}
	}

void CIPAddrCtrl::Clear(void)
{
	SetAddress(0);

	SetFocus(0);
	}

// Handle Lookup

CIPAddrCtrl & CIPAddrCtrl::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CIPAddrCtrl NullObject;

		return NullObject;
		}

	return (CIPAddrCtrl &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CIPAddrCtrl::GetDefaultClassName(void) const
{
	return WC_IPADDRESS;
	}

// Message Map

AfxMessageMap(CIPAddrCtrl, CCtrlWnd)
{
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxMessageEnd(CIPAddrCtrl)
	};

// Command Handlers

BOOL CIPAddrCtrl::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_CUT:
		case IDM_EDIT_COPY:
			Src.EnableItem(TRUE);
			break;

		case IDM_EDIT_DELETE:
			Src.EnableItem(TRUE);
			break;

		case IDM_EDIT_PASTE:
			Src.EnableItem(CanPasteText());
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CIPAddrCtrl::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_CUT:
			Cut();
			break;

		case IDM_EDIT_COPY:
			Copy();
			break;

		case IDM_EDIT_PASTE:
			Paste();
			break;

		case IDM_EDIT_DELETE:
			Clear();
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Implementation

BOOL CIPAddrCtrl::CanPasteText(void) const
{
	CClipboard Clip(ThisObject);

	if( Clip.IsFormatAvailable(CF_UNICODETEXT) ) {

		return TRUE;
		}

	if( Clip.IsFormatAvailable(CF_TEXT) ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File
