
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Multiple Select Tree View
//

// Dynamic Class

AfxImplementDynamicClass(CMulTreeView, CTreeView);

// Static Data

static UINT m_timerWait = CWnd::AllocTimerID();

// Constructor

CMulTreeView::CMulTreeView(void)
{
	m_uCount   = 0;

	m_fMulti   = FALSE;

	m_fDefer   = FALSE;

	m_fCapture = FALSE;

	m_fWaiting = FALSE;

	m_fPick    = FALSE;

	m_dwStyle  = 0;

	m_PickCursor.Create(L"CursorPick");

	m_MissCursor.Create(L"CursorMiss");
	}

// Attributes

BOOL CMulTreeView::GetMultiple(void) const
{
	return m_fMulti;
	}

BOOL CMulTreeView::HasMultiple(void) const
{
	return m_uCount > 1;
	}

BOOL CMulTreeView::IsSelected(HTREEITEM hItem) const
{
	if( GetItemState(hItem, TVIS_SELECTED) ) {

		return TRUE;
		}

	return FALSE;
	}

HTREEITEM CMulTreeView::GetFirstSelect(void) const
{
	HTREEITEM hScan = GetRoot();

	while( hScan ) {

		if( GetItemState(hScan, TVIS_SELECTED) ) {

			return hScan;
			}

		hScan = GetNextNode(hScan, TRUE);
		}

	return NULL;
	}

HTREEITEM CMulTreeView::GetLastSelect(void) const
{
	HTREEITEM hScan = GetRoot();

	HTREEITEM hLast = NULL;

	while( hScan ) {

		if( GetItemState(hScan, TVIS_SELECTED) ) {

			hLast = hScan;
			}

		hScan = GetNextNode(hScan, TRUE);
		}

	return hLast;
	}

HTREEITEM CMulTreeView::GetNextSelect(HTREEITEM hItem) const
{
	for(;;) {

		if( !(hItem = GetNextNode(hItem, TRUE)) ) {

			break;
			}

		if( GetItemState(hItem, TVIS_SELECTED) ) {

			return hItem;
			}
		}

	return NULL;
	}

HTREEITEM CMulTreeView::GetPrevSelect(HTREEITEM hItem) const
{
	for(;;) {

		if( !(hItem = GetPrevNode(hItem, TRUE)) ) {

			break;
			}

		if( GetItemState(hItem, TVIS_SELECTED) ) {

			return hItem;
			}
		}

	return NULL;
	}

BOOL CMulTreeView::InEditMode(void) const
{
	return SendMessageConst(TVM_GETEDITCONTROL) ? TRUE : FALSE;
	}

// Operations

void CMulTreeView::SetMultiple(BOOL fMulti)
{
	m_fMulti = fMulti;
	}

void CMulTreeView::SetDeferred(BOOL fDefer)
{
	m_fDefer = fDefer;
	}

void CMulTreeView::ClearSelection(void)
{
	HTREEITEM hScan = GetRoot();

	while( hScan ) {

		if( GetItemState(hScan, TVIS_SELECTED) ) {

			ClearSelected(hScan);
			}

		hScan = GetNextNode(hScan, TRUE);
		}

	SetCount(0);

	PickItem(NULL);
	}

void CMulTreeView::ClearExcept(void)
{
	if( m_uCount > 1 ) {

		ClearExcept(GetSelection());
		}
	}

void CMulTreeView::ClearExcept(HTREEITEM hSkip)
{
	HTREEITEM hScan = GetRoot();

	while( hScan ) {

		if( hScan != hSkip ) {

			if( GetItemState(hScan, TVIS_SELECTED) ) {

				ClearSelected(hScan);
				}
			}

		hScan = GetNextNode(hScan, TRUE);
		}

	SetCount(1);

	PickItem(NULL);
	}

BOOL CMulTreeView::SelectItem(HTREEITEM hItem)
{
	NMTREEVIEW Notify;

	PrepNotify(Notify, TVN_SELCHANGING);

	Notify.itemNew.hItem = hItem;

	if( !SendNotify(Notify) ) {

		ClearSelection();

		CTreeView::SelectItem(hItem);

		SetSelected(hItem);

		SetCount(1);

		PickItem(NULL);

		return TRUE;
		}

	return FALSE;
	}

void CMulTreeView::SelectRange(HTREEITEM h1, HTREEITEM h2)
{
	UINT      uMark = 0;

	BOOL      fMark = FALSE;

	HTREEITEM hScan = GetRoot();

	while( hScan ) {

		if( !fMark ) {
			
			if( hScan == h1 || hScan == h2 ) {

				SetSelected(hScan);

				uMark = uMark + 1;

				fMark = TRUE;

				if( h1 == h2 ) {

					break;
					}
				}
			else
				ClearSelected(hScan);
			}
		else {
			if( hScan == h1 || hScan == h2 ) {

				fMark = FALSE;
				}

			SetSelected(hScan);

			uMark = uMark + 1;
			}

		hScan = GetNextNode(hScan, TRUE);
		}

	SetCount(uMark);

	PickItem(NULL);
	}

BOOL CMulTreeView::SyncFirstSelection(void)
{
	HTREEITEM hScan = GetRoot();

	while( hScan ) {

		if( GetItemState(hScan, TVIS_SELECTED) ) {

			CTreeView::SelectItem(hScan);

			return TRUE;
			}

		hScan = GetNextNode(hScan, TRUE);
		}

	return FALSE;
	}

void CMulTreeView::SetPickMode(BOOL fPick)
{
	if( m_fPick != fPick ) {

		if( !(m_fPick = fPick) ) {

			CPoint Pos  = GetCursorPos();

			CRect  Rect = GetWindowRect();

			if( Rect.PtInRect(Pos) ) {

				SendMessage( WM_SETCURSOR, 
					     WPARAM    (m_hWnd),
					     MAKELPARAM(HTCLIENT, WM_MOUSEMOVE)
					     );
				}
			}
		}
	}

// Message Map

AfxMessageMap(CMulTreeView, CTreeView)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_RBUTTONDOWN)
	AfxDispatchMessage(WM_RBUTTONDBLCLK)
	AfxDispatchMessage(WM_RBUTTONUP)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)
	AfxDispatchMessage(WM_CHAR)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_SETCURSOR)

	AfxMessageEnd(CMulTreeView)
	};

// Message Handlers

void CMulTreeView::OnPostCreate(void)
{
	m_dwStyle = GetWindowStyle();
	}

void CMulTreeView::OnLButtonDown(UINT uCode, CPoint Pos)
{
	BOOL fFocus = HasFocus();

	if( m_fDefer ) {

		m_fDouble = FALSE;

		KillDefer();
		}
	else
		OfferFocus();

	if( m_fPick ) {

		m_Info.pt    = Pos;
		m_Info.flags = 0;
		m_Info.hItem = 0;

		if( HitTest(m_Info) ) {

			if( !IsValidPick(m_Info.hItem) ) {

				m_Info.hItem = NULL;

				return;
				}

			PickItem(m_Info.hItem);
			}

		return;
		}

	if( m_fMulti ) {

		m_Info.pt    = Pos;
		m_Info.flags = 0;
		m_Info.hItem = 0;

		if( HitTest(m_Info) ) {

			if( m_Info.flags & TVHT_ONITEMBUTTON ) {

				AfxCallDefProc();

				return;
				}

			if( InEditMode() ) {
		
				EndEditLabelNow(TRUE);
				}

			if( uCode & MK_SHIFT ) {

				SelectRange(GetSelection(), m_Info.hItem);

				return;
				}

			if( uCode & MK_CONTROL ) {

				if( GetItemState(m_Info.hItem, TVIS_SELECTED) ) {

					if( m_uCount > 1 ) {

						ClearSelected(m_Info.hItem);

						if( m_Info.hItem == GetSelection() ) {

							SyncFirstSelection();
							}

						SetCount(m_uCount - 1);
						}
					}
				else {
					SetSelected(m_Info.hItem);

					SetCount(m_uCount + 1);
					}

				return;
				}

			if( !GetItemState(m_Info.hItem, TVIS_SELECTED) ) {

				if( !SelectItem(m_Info.hItem) ) {

					return;
					}

				m_Info.flags = 0;
				}

			m_fCapture = TRUE;

			SetCapture();

			return;
			}
		}
	else {
		if( !fFocus ) {

			m_Info.pt    = Pos;
			m_Info.flags = 0;
			m_Info.hItem = 0;

			if( HitTest(m_Info) ) {

				if( GetItemState(m_Info.hItem, TVIS_SELECTED) ) {

					return;
					}
				}
			}
		}

	AfxCallDefProc();
	}

void CMulTreeView::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	m_fDouble = TRUE;

	KillDefer();

	NMTREEVIEW Notify;

	PrepNotify(Notify, NM_DBLCLK);

	SendNotify(Notify);
	}

void CMulTreeView::OnMouseMove(UINT uCode, CPoint Pos)
{
	if( m_fPick ) {

		m_Info.pt    = Pos;
		m_Info.flags = 0;
		m_Info.hItem = 0;

		HitTest(m_Info);

		if( !IsValidPick(m_Info.hItem) ) {

			m_Info.hItem = NULL;
			}

		if( m_Info.hItem ) {

			SetCursor(m_PickCursor);
			}
		else
			SetCursor(m_MissCursor);

		AfxCallDefProc();

		return;
		}

	if( m_fCapture ) {

		int dx = abs(Pos.x - m_Info.pt.x);

		int dy = abs(Pos.y - m_Info.pt.y);

		int cx = afxAdjustDPI(2);

		int cy = afxAdjustDPI(2);

		if( dx > cx || dy > cy ) {

			m_fCapture = FALSE;

			ReleaseCapture();

			NMTREEVIEW Notify;

			PrepNotify(Notify, TVN_BEGINDRAG);

			Notify.itemNew.hItem = GetSelection();

			SendNotify(Notify);
			}
		}
	else {
		if( m_fWaiting ) {

			int dx = abs(Pos.x - m_Info.pt.x);

			int dy = abs(Pos.y - m_Info.pt.y);

			int cx = GetSystemMetrics(SM_CXDOUBLECLK);

			int cy = GetSystemMetrics(SM_CYDOUBLECLK);

			if( dx > cx || dy > cy ) {

				KillDefer();

				OfferFocus();
				}
			}

		AfxCallDefProc();
		}
	}

void CMulTreeView::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
		}

	AfxCallDefProc();
	}

void CMulTreeView::OnLButtonUp(UINT uCode, CPoint Pos)
{
	if( m_fCapture ) {

		if( m_Info.hItem == GetSelection() ) {

			if( m_uCount == 1 ) {

				if( m_Info.flags & TVHT_ONITEMLABEL ) {

					EditLabel(m_Info.hItem);
					}
				}
			else
				ClearExcept();
			}
		else
			SelectItem(m_Info.hItem);

		if( !InEditMode() ) {

			StartDefer(Pos);
			}

		m_fCapture = FALSE;

		ReleaseCapture();

		return;
		}

	StartDefer(Pos);

	AfxCallDefProc();
	}

void CMulTreeView::OnRButtonDown(UINT uCode, CPoint Pos)
{
	TV_HITTESTINFO Info;

	Info.pt    = Pos;
	Info.flags = 0;
	Info.hItem = 0;

	if( HitTest(Info) ) {

		if( !GetItemState(Info.hItem, TVIS_SELECTED) ) {

			SelectItem(Info.hItem);
			}

		if( FALSE ) {

			// TODO -- If we enable this, the popup menu is
			// immediately removed as soon as it appears. We
			// need to figure out why this is happening...

			OfferFocus();
			}

		if( HasFocus() ) {

			NMTREEVIEW Notify;

			PrepNotify(Notify, TVN_CONTEXTMENU);

			Notify.itemNew.hItem = Info.hItem;

			Notify.ptDrag        = Pos;

			SendNotify(Notify);
			}
		else
			OfferFocus();

		return;
		}
	}

void CMulTreeView::OnRButtonDblClk(UINT uCode, CPoint Pos)
{
	OnRButtonDown(uCode, Pos);
	}

void CMulTreeView::OnRButtonUp(UINT uCode, CPoint Pos)
{
	}

void CMulTreeView::OnKeyDown(UINT uCode, DWORD dwData)
{
	if( m_fPick ) {

		if( uCode == VK_ESCAPE ) {

			PickItem(NULL);
			}

		return;
		}

	if( m_fMulti ) {

		if( uCode == VK_SHIFT ) {

			if( m_uCount > 1 ) {

				m_hAnchor = GetFirstSelect();
				}
			else 
				m_hAnchor = GetSelection();
			}

		switch( uCode ) {

			case VK_DOWN:
			case VK_UP:
			case VK_HOME:
			case VK_END:
			case VK_NEXT:
			case VK_PRIOR:

				if( GetKeyState(VK_SHIFT) & 0x8000 ) {

					if( uCode == VK_UP || uCode == VK_DOWN ) {

						AfxCallDefProc();

						SelectRange(m_hAnchor, GetSelection());
						}

					if( uCode == VK_NEXT || uCode == VK_PRIOR ) {

						AfxCallDefProc();

						SelectRange(m_hAnchor, GetSelection());
						}

					return;
					}
				else {
					if( m_uCount > 1 ) {

						ClearExcept();

						return;
						}
					}
				break;
			}

		if( uCode == VK_ESCAPE ) {

			ClearExcept();
			}

		if( uCode >= VK_SPACE ) {

			ClearExcept();
			}
		}

	AfxCallDefProc();
	}

void CMulTreeView::OnKeyUp(UINT uCode, DWORD dwData)
{
	if( m_fPick ) {

		return;
		}

	AfxCallDefProc();
	}

void CMulTreeView::OnChar(UINT uCode, DWORD dwData)
{
	if( m_fPick ) {

		return;
		}

	AfxCallDefProc();
	}

void CMulTreeView::OnSetFocus(CWnd &Wnd)
{
	KillDefer();

	if( m_uCount > 1 ) {

		Invalidate(FALSE);
		}

	AfxCallDefProc();
	}

void CMulTreeView::OnKillFocus(CWnd &Wnd)
{
	KillDefer();

	PickItem(NULL);

	if( m_uCount > 1 ) {

		Invalidate(FALSE);
		}

	AfxCallDefProc();
	}

void CMulTreeView::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerWait ) {

		KillDefer();

		OfferFocus();
		}

	AfxCallDefProc();
	}

BOOL CMulTreeView::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( &Wnd == this ) {

		if( m_fPick ) {

			if( m_Info.hItem ) {

				SetCursor(m_PickCursor);
				}
			else
				SetCursor(m_MissCursor);

			return TRUE;
			}
		}

	return AfxCallDefProc();
	}

// Implementation

void CMulTreeView::SetCount(UINT uCount)
{
	if( uCount <= 1 && m_uCount > 1 ) {

		m_uCount = uCount;

		NotifyMultiple(FALSE);

		return;
		}

	if( uCount > 1 && m_uCount <= 1 ) {

		m_uCount = uCount;

		NotifyMultiple(TRUE);

		return;
		}

	m_uCount = uCount;
	}

void CMulTreeView::NotifyMultiple(BOOL fMulti)
{
	if( fMulti ) {
		
		OfferFocus();
		}

	NMTREEVIEW Notify;

	PrepNotify(Notify, TVN_CHANGEMULTI);

	Notify.action = fMulti;

	SendNotify(Notify);
	}

void CMulTreeView::ClearSelected(HTREEITEM hItem)
{
	SetItemState(hItem, 0, TVIS_SELECTED);
	}

void CMulTreeView::SetSelected(HTREEITEM hItem)
{
	HTREEITEM hParent = GetParent(hItem);

	if( hParent ) {

		Expand(hParent, TVE_EXPAND);
		}

	SetItemState(hItem, TVIS_SELECTED, TVIS_SELECTED);
	}

BOOL CMulTreeView::StartDefer(CPoint Pos)
{
	if( m_fDefer ) {
		
		if( !m_fDouble ) {

			UINT uTime = GetDoubleClickTime();

			SetTimer(m_timerWait, uTime);

			m_Info.pt  = Pos;

			m_fWaiting = TRUE;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMulTreeView::KillDefer(void)
{
	if( m_fDefer ) {

		KillTimer(m_timerWait);

		m_fWaiting = FALSE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CMulTreeView::OfferFocus(void)
{
	if( !HasFocus() ) {
		
		if( !InEditMode() ) {

			NMTREEVIEW Notify;

			PrepNotify(Notify, TVN_OFFERFOCUS);

			SendNotify(Notify);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMulTreeView::IsValidPick(HTREEITEM hItem)
{
	if( !GetParent(hItem) ) {

		return FALSE;
		}

	if( m_uCount == 1 ) {

		if( IsSelected(hItem) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CMulTreeView::PickItem(HTREEITEM hItem)
{
	if( m_fPick ) {

		SetPickMode(FALSE);

		NMTREEVIEW Notify;

		PrepNotify(Notify, TVN_PICKITEM);

		Notify.itemNew.hItem = hItem;

		if( SendNotify(Notify) ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

void CMulTreeView::PrepNotify(NMTREEVIEW &Notify, UINT uCode)
{
	memset(&Notify, 0, sizeof(Notify));

	Notify.hdr.hwndFrom = m_hWnd;

	Notify.hdr.idFrom   = GetID();

	Notify.hdr.code	    = uCode;
	}

BOOL CMulTreeView::SendNotify(NMTREEVIEW &Notify)
{
	CWnd &Wnd = CWnd::GetParent();

	return BOOL(Wnd.SendMessage( WM_NOTIFY,
				     WPARAM(GetID()),
				     LPARAM(&Notify)
				     ));
	}

// End of File
