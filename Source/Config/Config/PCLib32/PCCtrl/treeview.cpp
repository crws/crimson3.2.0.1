
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Tree View Control
//

// Dynamic Class

AfxImplementDynamicClass(CTreeView, CCtrlWnd);

// Constructor

CTreeView::CTreeView(void)
{
	LoadControlClass(ICC_TREEVIEW_CLASSES);
	}

// Attributes

CColor CTreeView::GetBkColor(void) const
{
	return CColor(SendMessageConst(TVM_GETBKCOLOR));
	}

UINT CTreeView::GetCount(void) const
{
	return UINT(SendMessageConst(TVM_GETCOUNT));
	}

CEditCtrl & CTreeView::GetEditControl(void) const
{
	return CEditCtrl::FromHandle(HWND(SendMessageConst(TVM_GETEDITCONTROL)));
	}

CImageList & CTreeView::GetImageList(void) const
{
	return CImageList::FromHandle(HIMAGELIST(SendMessageConst(TVM_GETIMAGELIST)));
	}

int CTreeView::GetIndent(void) const
{
	return int(SendMessageConst(TVM_GETINDENT));
	}

CColor CTreeView::GetInsertMarkColor(void) const
{
	return CColor(SendMessageConst(TVM_GETINSERTMARKCOLOR));
	}

CString CTreeView::GetSearchString(void) const
{
	TCHAR sBuffer[256] = { 0 };

	SendMessageConst(TVM_GETISEARCHSTRING, 0, LPARAM(&sBuffer));

	return CString(sBuffer);
	}

BOOL CTreeView::GetItem(TVITEMEX &Item) const
{
	return BOOL(SendMessageConst(TVM_GETITEM, 0, LPARAM(&Item)));
	}

CTreeViewItem CTreeView::GetItem(HTREEITEM hItem) const
{
	CTreeViewItem Item;

	Item.SetHandle(hItem);

	Item.SetMask( TVIF_HANDLE
		    | TVIF_STATE
		    | TVIF_IMAGE
		    | TVIF_SELECTEDIMAGE
		    | TVIF_TEXT
		    | TVIF_CHILDREN
		    | TVIF_PARAM
		    | TVIF_INTEGRAL
		    );
	
	Item.SetTextBuffer(256);

	GetItem(Item);

	return Item;
	}

CString CTreeView::GetItemText(HTREEITEM hItem) const
{
	CTreeViewItem Item;

	Item.SetHandle(hItem);
	
	Item.SetTextBuffer(256);

	GetItem(Item);

	return Item.GetText();
	}

UINT CTreeView::GetItemImage(HTREEITEM hItem) const
{
	CTreeViewItem Item;
	
	Item.SetHandle(hItem);

	Item.SetMask(TVIF_IMAGE);

	GetItem(Item);

	return Item.GetImage();
	}

LPARAM CTreeView::GetItemParam(HTREEITEM hItem) const
{
	CTreeViewItem Item;
	
	Item.SetHandle(hItem);

	Item.SetMask(TVIF_PARAM);

	GetItem(Item);

	return Item.GetParam();
	}

int CTreeView::GetItemHeight(void) const
{
	return int(SendMessageConst(TVM_GETITEMHEIGHT));
	}

CRect CTreeView::GetItemRect(HTREEITEM hItem, BOOL fRect) const
{
	CRect Result;

	Result.left = int(hItem);

	SendMessageConst(TVM_GETITEMRECT, WPARAM(fRect), LPARAM(&Result));

	return Result;
	}

UINT CTreeView::GetItemState(HTREEITEM hItem, UINT uMask) const
{
	return UINT(SendMessageConst(TVM_GETITEMSTATE, WPARAM(hItem), LPARAM(uMask)) & uMask);
	}

HTREEITEM CTreeView::GetLastChild(HTREEITEM hItem) const
{
	HTREEITEM hScan = GetNextItem(hItem, TVGN_CHILD);

	HTREEITEM hNext;

	while( (hNext = GetNextItem(hScan, TVGN_NEXT)) ) {
		
		hScan = hNext;
		}

	return hScan;
	}

CColor CTreeView::GetLineColor(void) const
{
	return CColor(SendMessageConst(TVM_GETLINECOLOR));
	}

HTREEITEM CTreeView::GetNextItem(HTREEITEM hItem, UINT uCode) const
{
	return HTREEITEM(SendMessageConst(TVM_GETNEXTITEM, uCode, LPARAM(hItem)));
	}

HTREEITEM CTreeView::GetNextNode(HTREEITEM hItem, BOOL fSiblings) const
{
	HTREEITEM hNext;

	if( hNext = GetNextItem(hItem, TVGN_CHILD) ) {

		return hNext;
		}

	if( hNext = GetNextItem(hItem, TVGN_NEXT) ) {

		return hNext;
		}

	if( fSiblings ) {

		while( hItem = GetNextItem(hItem, TVGN_PARENT) ) {

			if( hNext = GetNextItem(hItem, TVGN_NEXT) ) {

				return hNext;
				}
			}
		}

	return NULL;
	}

HTREEITEM CTreeView::GetPrevNode(HTREEITEM hItem, BOOL fSiblings) const
{
	HTREEITEM hPrev;

	if( hPrev = GetNextItem(hItem, TVGN_PREVIOUS) ) {

		if( fSiblings ) {
			
			while( GetChild(hPrev) ) {
			
				hPrev = GetLastChild(hPrev);
				}
			}
		
		return hPrev;
		}

	if( hPrev = GetNextItem(hItem, TVGN_PARENT) ) {

		return hPrev;
		}

	return NULL;
	}

UINT CTreeView::GetScrollTime(void) const
{
	return UINT(SendMessageConst(TVM_GETSCROLLTIME));
	}

CColor CTreeView::GetTextColor(void) const
{
	return CColor(SendMessageConst(TVM_GETTEXTCOLOR));
	}

CToolTip & CTreeView::GetToolTips(void) const
{
	return CToolTip::FromHandle(HWND(SendMessageConst(TVM_GETTOOLTIPS)));
	}

UINT CTreeView::GetVisibleCount(void) const
{
	return UINT(SendMessageConst(TVM_GETVISIBLECOUNT));
	}

HTREEITEM CTreeView::HitTest(TV_HITTESTINFO &Info) const
{
	return HTREEITEM(SendMessageConst(TVM_HITTEST, 0, LPARAM(&Info)));
	}

HTREEITEM CTreeView::HitTestItem(CPoint const &Pos) const
{
	TVHITTESTINFO Info;

	Info.pt = Pos;

	HitTest(Info);

	return Info.hItem;
	}

UINT CTreeView::HitTestFlags(CPoint const &Pos) const
{
	TVHITTESTINFO Info;

	Info.pt = Pos;

	HitTest(Info);

	return Info.flags;
	}

BOOL CTreeView::IsExpanded(HTREEITEM hItem) const
{
	return GetItemState(hItem, TVIS_EXPANDED) ? TRUE : FALSE;
	}

BOOL CTreeView::IsVisible(HTREEITEM hItem) const
{
	CRect Result;

	Result.left = int(hItem);

	return BOOL(SendMessageConst(TVM_GETITEMRECT, WPARAM(FALSE), LPARAM(&Result)));
	}

// Notification Helper

BOOL CTreeView::IsMouseInSelection(void) const
{
	HTREEITEM hItem = GetSelection();

	if( hItem ) {

		CPoint Pos = GetCursorPos(ThisObject);

		if( GetItemRect(hItem, TRUE).PtInRect(Pos) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Item Access Helpers

HTREEITEM CTreeView::GetSelection(void) const
{
	return GetNextItem(NULL, TVGN_CARET);
	}

HTREEITEM CTreeView::GetChild(HTREEITEM hItem) const
{
	return GetNextItem(hItem, TVGN_CHILD);
	}

HTREEITEM CTreeView::GetDropHilite(void) const
{
	return GetNextItem(NULL, TVGN_DROPHILITE);
	}

HTREEITEM CTreeView::GetFirstVisible(void) const
{
	return GetNextItem(NULL, TVGN_FIRSTVISIBLE);
	}

HTREEITEM CTreeView::GetNext(HTREEITEM hItem) const
{
	return GetNextItem(hItem, TVGN_NEXT);
	}

HTREEITEM CTreeView::GetNextVisible(HTREEITEM hItem) const
{
	return GetNextItem(hItem, TVGN_NEXTVISIBLE);
	}

HTREEITEM CTreeView::GetParent(HTREEITEM hItem) const
{
	return GetNextItem(hItem, TVGN_PARENT);
	}

HTREEITEM CTreeView::GetPrevious(HTREEITEM hItem) const
{
	return GetNextItem(hItem, TVGN_PREVIOUS);
	}

HTREEITEM CTreeView::GetPreviousVisible(HTREEITEM hItem) const
{
	return GetNextItem(hItem, TVGN_PREVIOUSVISIBLE);
	}

HTREEITEM CTreeView::GetRoot(void) const
{
	return GetNextItem(NULL, TVGN_ROOT);
	}

// Operations

CImageList & CTreeView::CreateDragImage(HTREEITEM hItem)
{
	return CImageList::FromHandle(HIMAGELIST(SendMessage(TVM_CREATEDRAGIMAGE, 0, LPARAM(hItem))));
	}

BOOL CTreeView::DeleteItem(HTREEITEM hItem)
{
	return BOOL(SendMessage(TVM_DELETEITEM, 0, LPARAM(hItem)));
	}

void CTreeView::DeleteChildren(HTREEITEM hItem)
{
	HTREEITEM hChild;

	while( hChild = GetNextItem(hItem, TVGN_CHILD) ) {

		DeleteItem(hChild);
		}
	}

BOOL CTreeView::EditLabel(HTREEITEM hItem)
{
	return SendMessage(TVM_EDITLABEL, 0, LPARAM(hItem)) ? TRUE : FALSE;
	}

BOOL CTreeView::EndEditLabelNow(BOOL fCancel)
{
	return BOOL(SendMessage(TVM_ENDEDITLABELNOW, WPARAM(fCancel)));
	}

BOOL CTreeView::EnsureVisible(HTREEITEM hItem)
{
	return BOOL(SendMessage(TVM_ENSUREVISIBLE, 0, LPARAM(hItem)));
	}

BOOL CTreeView::Expand(HTREEITEM hItem, UINT uAction)
{
	return BOOL(SendMessage(TVM_EXPAND, uAction, LPARAM(hItem)));
	}

HTREEITEM CTreeView::InsertItem(HTREEITEM hParent, HTREEITEM hAfter, CTreeViewItem const &Item)
{
	TVINSERTSTRUCT Insert;

	Insert.itemex       = Item;

	Insert.hParent      = hParent;

	Insert.hInsertAfter = hAfter;

	return HTREEITEM(SendMessage(TVM_INSERTITEM, 0, LPARAM(&Insert)));
	}

void CTreeView::MoveSelection(void)
{
	HTREEITEM hSelect  = GetSelection();

	HTREEITEM hItem    = GetNextItem(hSelect, TVGN_NEXT);

	if( !hItem ) hItem = GetNextItem(hSelect, TVGN_PREVIOUS);

	if( !hItem ) hItem = GetNextItem(hSelect, TVGN_PARENT);

	SelectItem(hItem, TVGN_CARET);
	}

BOOL CTreeView::SelectItem(HTREEITEM hItem)
{
	return SelectItem(hItem, TVGN_CARET);
	}

BOOL CTreeView::SelectItem(HTREEITEM hItem, UINT uAction)
{
	return BOOL(SendMessage(TVM_SELECTITEM, WPARAM(uAction), LPARAM(hItem)));
	}

void CTreeView::SetBkColor(CColor const &Color)
{
	SendMessage(TVM_SETBKCOLOR, 0, LPARAM(Color));
	}

void CTreeView::SetImageList(UINT uList, HIMAGELIST hList)
{
	SendMessage(TVM_SETIMAGELIST, uList, LPARAM(hList));
	}

void CTreeView::SetIndent(int nIndent)
{
	SendMessage(TVM_SETINDENT, WPARAM(nIndent));
	}

void CTreeView::SetInsertMark(UINT uMark)
{
	SendMessage(TVM_SETINSERTMARK, WPARAM(uMark));
	}

void CTreeView::SetInsertMarkColor(CColor const &Color)
{
	SendMessage(TVM_SETINSERTMARKCOLOR, 0, LPARAM(Color));
	}

BOOL CTreeView::SetItem(TVITEMEX const &Item)
{
	return BOOL(SendMessage(TVM_SETITEM, 0, LPARAM(&Item)));
	}

void CTreeView::SetItemHeight(int nHeight)
{
	SendMessage(TVM_SETINSERTMARK, WPARAM(nHeight));
	}

void CTreeView::SetItemText(HTREEITEM hItem, CString Text)
{
	CTreeViewItem Item;

	Item.SetHandle(hItem);

	Item.SetText(Text);

	SetItem(Item);
	}

void CTreeView::SetItemState(HTREEITEM hItem, UINT uState, UINT uMask)
{
	CTreeViewItem Item;

	Item.SetHandle(hItem);

	Item.SetState(uState, uMask);

	SetItem(Item);
	}

void CTreeView::SetLineColor(CColor const &Color)
{
	SendMessage(TVM_SETLINECOLOR, 0, LPARAM(Color));
	}

void CTreeView::SetScrollTime(UINT uTime)
{
	SendMessage(TVM_SETSCROLLTIME, WPARAM(uTime));
	}

void CTreeView::SetTextColor(CColor const &Color)
{
	SendMessage(TVM_SETTEXTCOLOR, 0, LPARAM(Color));
	}

void CTreeView::SetToolTips(CToolTip &Tips)
{
	SendMessage(TVM_SETTOOLTIPS, WPARAM(HWND(Tips)));
	}

BOOL CTreeView::SortChildren(HTREEITEM hItem)
{
	return BOOL(SendMessage(TVM_SORTCHILDREN, 0, LPARAM(hItem)));
	}

BOOL CTreeView::SortChildren(HTREEITEM hItem, PFNTVCOMPARE pfnCompare, LPARAM lParam)
{
	TVSORTCB Sort;

	Sort.hParent     = hItem;

	Sort.lpfnCompare = pfnCompare;

	Sort.lParam	 = lParam;

	return BOOL(SendMessage(TVM_SORTCHILDRENCB, 0, LPARAM(&Sort)));
	}

// Handle Lookup

CTreeView & CTreeView::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CTreeView NullObject;

		return NullObject;
		}

	return (CTreeView &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Routing Control

BOOL CTreeView::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	CEditCtrl &Edit = GetEditControl();

	if( Edit.GetHandle() ) {

		if( Edit.RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CCtrlWnd::OnRouteMessage(Message, lResult);
	}

// Default Class Name

PCTXT CTreeView::GetDefaultClassName(void) const
{
	return WC_TREEVIEW;
	}

// End of File
