
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Rich Edit Control
//

// Dynamic Class

AfxImplementDynamicClass(CRichEditCtrl, CEditCtrl);

// Constructor

CRichEditCtrl::CRichEditCtrl(void)
{
	LoadLibrary(L"riched20.dll");
	}

// Handle Lookup

CRichEditCtrl & CRichEditCtrl::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CRichEditCtrl NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CRichEditCtrl &) CWnd::FromHandle(hWnd, Class);
	}

// Default Class Name

PCTXT CRichEditCtrl::GetDefaultClassName(void) const
{
	return RICHEDIT_CLASS;
	}

// End of File
