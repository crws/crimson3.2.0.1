 
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Dynamic Class

AfxImplementDynamicClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestWnd;

	pWnd->Create( L"Test Application",
		      WS_OVERLAPPEDWINDOW,
		      CRect(300, 200, 600, 450),
		      AfxNull(CWnd), CMenu(L"TestMenu"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:
			
			Src.EnableItem(TRUE);
			
			break;
		
		default:
			return FALSE;
		}
		
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestWnd, CWnd);

// Constructor

CTestWnd::CTestWnd(void)
{
	m_Accel.Create(L"TestMenu");

	m_pCtrl = New CHotLinkCtrl;
	}

// Destructor

CTestWnd::~CTestWnd(void)
{
	}

// Routing Control

BOOL CTestWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pCtrl ) {

		if( m_pCtrl->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CTestWnd, CWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	
	AfxMessageEnd(CTestWnd)
	};

// Accelerator

BOOL CTestWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

UINT CTestWnd::OnCreate(CREATESTRUCT &Create)
{
	m_pCtrl->Create( L"Clear Port Settings...",
			 WS_CHILD,
			 CRect(15, 15, 130, 32),
			 ThisObject,
			 100
			 );

	m_pCtrl->SetFont(afxFont(Dialog));

	m_pCtrl->ShowWindow(SW_SHOW);

	return 0;
	}

BOOL CTestWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID >= 0xF000 ) {
		
		AfxCallDefProc();
		
		return TRUE;
		}
		
	if( uID >= 0x8000 ) {
	
		CCmdSourceData Source;
		
		if( uID < 0xF000 ) {

			Source.PrepareSource();
			}
	
		RouteControl(uID, Source);
		
		if( Source.GetFlags() & MF_DISABLED ) {
		
			MessageBeep(0);
			
			return TRUE;
			}

		RouteCommand(uID, 0);
			
		return TRUE;
		}

	return FALSE;
	}

void CTestWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	UINT uCount = Menu.GetMenuItemCount();

	for( UINT uPos = 0; uPos < uCount; uPos++ ) {

		UINT uID = Menu.GetMenuItemID(uPos);
		
		if( uID == 0xFFFF ) {

			CMenu &Sub = Menu.GetSubMenu(uPos);
			
			if( Sub.GetHandle() ) {

				OnInitPopup(Sub, 0, FALSE);
				}
			}
		else {
			if( uID ) {
	
				CCmdSourceMenu Source(Menu, uID);
	
				if( uID < 0xF000 ) {

					Source.PrepareSource();
					}
	
				RouteControl(uID, Source);
				}
			}
		}
	}

void CTestWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);
	}

BOOL CTestWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	DC.FillRect(Rect, afxBrush(TabFace));

	return TRUE;
	}

void CTestWnd::OnSetFocus(CWnd &Wnd)
{
	m_pCtrl->SetFocus();
	}

void CTestWnd::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	}

// End of File
