
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// TimeDate Control
//

// Dynamic Class

AfxImplementDynamicClass(CTimeDateCtrl, CCtrlWnd);

// Constructor

CTimeDateCtrl::CTimeDateCtrl(void)
{
	LoadControlClass(ICC_DATE_CLASSES);
	}

// Attributes

CColor CTimeDateCtrl::GetMonthColor(UINT uIndex) const
{
	COLORREF clr = COLORREF(SendMessageConst(DTM_GETMCCOLOR, uIndex));

	return CColor(clr);
	}

CFont CTimeDateCtrl::GetMonthFont(void) const
{
	HFONT hFont = HFONT(SendMessageConst(DTM_GETMCFONT));
	
	return CFont::FromHandle(hFont);
	}

CMonthCtrl & CTimeDateCtrl::GetMonthCtrl(void) const
{
	HWND hwnd = HWND(SendMessageConst(DTM_GETMONTHCAL));
	
	return CMonthCtrl::FromHandle(hwnd);
	}

DWORD CTimeDateCtrl::GetRange(SYSTEMTIME *pTimeArray) const
{
	AfxValidateWritePtr(pTimeArray, sizeof(SYSTEMTIME) * 2);

	return SendMessageConst(DTM_GETRANGE, 0, LPARAM(pTimeArray)) == GDT_VALID;
	}

BOOL CTimeDateCtrl::GetSystemTime(SYSTEMTIME &Time) const
{
	AfxValidateWritePtr(&Time, sizeof(Time));

	return SendMessageConst(DTM_GETSYSTEMTIME, 0, LPARAM(&Time)) == GDT_VALID;
	}

SYSTEMTIME CTimeDateCtrl::GetSystemTime(void) const
{
	SYSTEMTIME Time;

	GetSystemTime(Time);

	return Time;
	}

// Operations

BOOL CTimeDateCtrl::SetFormat(PCTXT pText)
{
	return BOOL(SendMessageConst(DTM_SETFORMAT, 0, LPARAM(pText)));
	}

CColor CTimeDateCtrl::SetMonthColor(UINT uIndex, CColor const &Color)
{
	AfxValidateReadPtr(&Color, sizeof(Color));

	COLORREF clr = COLORREF(SendMessageConst(DTM_SETMCCOLOR, uIndex, LPARAM(COLORREF(Color))));

	return CColor(clr);
	}

void CTimeDateCtrl::SetMonthFont(CFont const &Font, BOOL fRedraw)
{
	AfxValidateObject(Font);

	SendMessageConst(DTM_SETMCFONT, WPARAM(HFONT(Font)), MAKELONG(fRedraw, 0));
	}

BOOL CTimeDateCtrl::SetRange(UINT uFlags, SYSTEMTIME const *pTimeArray)
{
	AfxValidateReadPtr(pTimeArray, sizeof(SYSTEMTIME) * 2);

	return BOOL(SendMessageConst(DTM_SETRANGE, uFlags, LPARAM(pTimeArray)));
	}

BOOL CTimeDateCtrl::SetSystemTime(SYSTEMTIME const &Time)
{
	AfxValidateReadPtr(&Time, sizeof(Time));

	return BOOL(SendMessageConst(DTM_SETSYSTEMTIME, GDT_VALID, LPARAM(&Time)));
	}

BOOL CTimeDateCtrl::ClearSystemTime(void)
{
	return BOOL(SendMessageConst(DTM_SETSYSTEMTIME, GDT_NONE));
	}

// Handle Lookup

CTimeDateCtrl & CTimeDateCtrl::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CTimeDateCtrl NullObject;

		return NullObject;
		}

	return (CTimeDateCtrl &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CTimeDateCtrl::GetDefaultClassName(void) const
{
	return DATETIMEPICK_CLASS;
	}

// End of File
