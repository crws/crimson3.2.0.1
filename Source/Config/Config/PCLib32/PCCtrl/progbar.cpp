
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Progress Bar Control
//

// Dynamic Class

AfxImplementDynamicClass(CProgressBar, CCtrlWnd);

// Constructor

CProgressBar::CProgressBar(void)
{
	LoadControlClass(ICC_PROGRESS_CLASS);
	}

// Attributes

UINT CProgressBar::GetPos(void) const
{
	return UINT(SendMessageConst(PBM_GETPOS));
	}

UINT CProgressBar::GetRange(BOOL fMin) const
{
	return UINT(SendMessageConst(PBM_GETRANGE, WPARAM(fMin)));
	}

CRange CProgressBar::GetRange(void) const
{
	return CRange(GetRangeMin(), GetRangeMax());
	}

UINT CProgressBar::GetRangeMin(void) const
{
	return GetRange(TRUE);
	}

UINT CProgressBar::GetRangeMax(void) const
{
	return GetRange(FALSE);
	}

// Operations

void CProgressBar::DeltaPos(int nDelta)
{
	SendMessage(PBM_DELTAPOS, WPARAM(nDelta));
	}

void CProgressBar::SetBarColor(CColor const &Color)
{
	SendMessage(PBM_SETBARCOLOR, 0, LPARAM(Color));
	}

void CProgressBar::SetBkColor(CColor const &Color)
{
	SendMessage(PBM_SETBKCOLOR, 0, LPARAM(Color));
	}

void CProgressBar::SetPos(UINT uPos)
{
	SendMessage(PBM_SETPOS, WPARAM(uPos));
	}

void CProgressBar::SetRange(UINT uMin, UINT uMax)
{
	SendMessage(PBM_SETRANGE, 0, MAKELPARAM(uMin, uMax));
	}

void CProgressBar::SetRange(CRange const &Range)
{
	SendMessage(PBM_SETRANGE, 0, LPARAM(DWORD(Range)));
	}

void CProgressBar::SetStep(UINT uStep)
{
	SendMessage(PBM_SETSTEP, uStep);
	}

void CProgressBar::StepIt(void)
{
	SendMessage(PBM_STEPIT);
	}

// Handle Lookup

CProgressBar & CProgressBar::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CProgressBar NullObject;

		return NullObject;
		}

	return (CProgressBar &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CProgressBar::GetDefaultClassName(void) const
{
	return PROGRESS_CLASS;
	}

// End of File
