
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Linear Range
//

// Standard Constructors

CRange::CRange(void)
{
	m_nFrom = 0;
	
	m_nTo   = 0;
	}

CRange::CRange(INT nFrom, INT nTo)
{
	m_nFrom = nFrom;

	m_nTo   = nTo;
	}

// Explicit Constructors

CRange::CRange(BOOL fAll)
{
	m_nFrom = 0;

	m_nTo   = fAll ? -1 : 0;
	}

CRange::CRange(DWORD dwRange)
{
	m_nFrom = INT(LOWORD(dwRange));

	m_nTo   = INT(HIWORD(dwRange));
	}

// Conversion

CRange::operator DWORD(void) const
{
	return MAKELONG(LOWORD(m_nFrom), LOWORD(m_nTo));
	}

CRange::operator WORD(void) const
{
	return MAKEWORD(LOBYTE(m_nFrom), LOBYTE(m_nTo));
	}

// Attributes

UINT CRange::GetCount(void) const
{
	return IsEmpty() ? 0 : (m_nTo - m_nFrom);
	}

BOOL CRange::IsEmpty(void) const
{
	return m_nTo <= m_nFrom;
	}

// Operations

void CRange::Empty(void)
{
	m_nFrom = 0;
	
	m_nTo   = 0;
	}

void CRange::Set(BOOL fAll)
{
	m_nFrom = 0;

	m_nTo   = fAll ? -1 : 0;
	}

void CRange::Set(INT nFrom, INT nTo)
{
	m_nFrom = nFrom;

	m_nTo   = nTo;
	}

void CRange::Reverse(void)
{
	Swap(m_nFrom, m_nTo);
	}

// End of File
