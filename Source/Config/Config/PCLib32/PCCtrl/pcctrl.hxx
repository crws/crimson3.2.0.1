
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCCTRL_HXX
	
#define	INCLUDE_PCCTRL_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcwin.hxx>

//////////////////////////////////////////////////////////////////////////
//								
// Multiple Selection Tree View Notifications
//

#define	TVN_CONTEXTMENU		(TVN_LAST+1)
#define	TVN_CHANGEMULTI		(TVN_LAST+2)
#define	TVN_OFFERFOCUS		(TVN_LAST+3)
#define	TVN_PICKITEM		(TVN_LAST+4)

// End of File

#endif
