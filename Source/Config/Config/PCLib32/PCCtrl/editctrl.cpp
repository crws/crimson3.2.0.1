
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Edit Control
//

// Dynamic Class

AfxImplementDynamicClass(CEditCtrl, CCtrlWnd);

// Constructor

CEditCtrl::CEditCtrl(void)
{
	}

// Attributes

BOOL CEditCtrl::CanUndo(void) const
{
	return BOOL(SendMessageConst(EM_CANUNDO));
	}

UINT CEditCtrl::CharFromPos(CPoint const &Pos) const
{
	return UINT(SendMessageConst(EM_CHARFROMPOS, 0, DWORD(Pos)));
	}

UINT CEditCtrl::GetFirstVisibleLine(void) const
{
	return UINT(SendMessageConst(EM_GETFIRSTVISIBLELINE));
	}

HANDLE CEditCtrl::GetEditHandle(void) const
{
	return HANDLE(SendMessageConst(EM_GETHANDLE));
	}

CString CEditCtrl::GetLine(UINT uLine) const
{
	UINT uIndex = LineIndex(uLine);

	if( uIndex < NOTHING ) {

		UINT uCount = LineLength(uIndex) + 8;

		CString Text(' ', uCount);

		((UINT *) (PTXT) (PCTXT) Text)[0] = uCount;

		SendMessageConst(EM_GETLINE, WPARAM(uLine), LPARAM(PTXT(PCTXT(Text))));
	
		return Text;
		}

	return L"";
	}

UINT CEditCtrl::GetLineCount(void) const
{
	return UINT(SendMessageConst(EM_GETLINECOUNT));
	}

DWORD CEditCtrl::GetMargins(void) const
{
	return DWORD(SendMessageConst(EM_GETMARGINS));
	}

UINT CEditCtrl::GetMarginLeft(void) const
{
	return LOWORD(GetMargins());
	}

UINT CEditCtrl::GetMarginRight(void) const
{
	return HIWORD(GetMargins());
	}

BOOL CEditCtrl::GetModify(void) const
{
	return BOOL(SendMessageConst(EM_GETMODIFY));
	}

TCHAR CEditCtrl::GetPasswordChar(void) const
{
	return TCHAR(SendMessageConst(EM_GETPASSWORDCHAR));
	}

CRect CEditCtrl::GetRect(void) const
{
	CRect Result;

	SendMessageConst(EM_GETRECT, 0, LPARAM(PRECT(&Result)));
	
	return Result;
	}

void CEditCtrl::GetRect(CRect &Rect) const
{
	SendMessageConst(EM_GETRECT, 0, LPARAM(PRECT(&Rect)));
	}

CRange CEditCtrl::GetSel(void) const
{
	INT nFrom = 0;

	INT nTo   = 0;

	SendMessageConst(EM_GETSEL, WPARAM(&nFrom), LPARAM(&nTo));

	return CRange(nFrom, nTo);
	}

UINT CEditCtrl::GetThumb(void) const
{
	return UINT(SendMessageConst(EM_GETTHUMB));
	}

FARPROC CEditCtrl::GetWordBreakProc(void) const
{
	return FARPROC(SendMessageConst(EM_GETWORDBREAKPROC));
	}

BOOL CEditCtrl::IsReadOnly(void) const
{
	return (GetWindowStyle() & ES_READONLY) ? TRUE : FALSE;
	}

UINT CEditCtrl::LineFromChar(UINT uChar) const
{
	return UINT(SendMessageConst(EM_LINEFROMCHAR, WPARAM(uChar)));
	}

UINT CEditCtrl::LineIndex(UINT uLine) const
{
	return UINT(SendMessageConst(EM_LINEINDEX, WPARAM(uLine)));
	}

UINT CEditCtrl::LineLength(UINT uChar) const
{
	return UINT(SendMessageConst(EM_LINELENGTH, WPARAM(uChar)));
	}

CPoint CEditCtrl::PosFromChar(UINT uChar) const
{
	return CPoint(DWORD(SendMessageConst(EM_POSFROMCHAR, WPARAM(uChar))));
	}

// General Operations

void CEditCtrl::EmptyUndoBuffer(void)
{
	SendMessage(EM_EMPTYUNDOBUFFER);
	}
	
void CEditCtrl::FmtLines(BOOL fAddEOL)
{
	SendMessage(EM_FMTLINES, WPARAM(fAddEOL));
	}
	
void CEditCtrl::LimitText(UINT uLimit)
{
	SendMessage(EM_LIMITTEXT, WPARAM(uLimit));
	}

void CEditCtrl::LineScroll(int dx, int dy)
{
	SendMessage(EM_LINESCROLL, WPARAM(dx), LPARAM(dy));
	}

void CEditCtrl::ReplaceSel(PCTXT pString)
{
	SendMessage(EM_REPLACESEL, 0, LPARAM(pString));
	}

void CEditCtrl::SetEditHandle(HANDLE hHandle)
{
	SendMessage(EM_SETHANDLE, WPARAM(hHandle));
	}

void CEditCtrl::SetMargins(UINT uFlags, DWORD dwMargin)
{
	SendMessage(EM_SETMARGINS, WPARAM(uFlags), LPARAM(dwMargin));
	}

void CEditCtrl::SetMarginLeft(UINT uMargin)
{
	SetMargins(EC_LEFTMARGIN, MAKELONG(uMargin, 0));
	}

void CEditCtrl::SetMarginRight(UINT uMargin)
{
	SetMargins(EC_RIGHTMARGIN, MAKELONG(0, uMargin));
	}

void CEditCtrl::SetModify(BOOL fModify)
{
	SendMessage(EM_SETMODIFY, WPARAM(fModify));
	}

void CEditCtrl::SetPasswordChar(TCHAR cChar)
{
	SendMessage(EM_SETPASSWORDCHAR, WPARAM(cChar));
	}

void CEditCtrl::SetReadOnly(BOOL fState)
{
	SendMessage(EM_SETREADONLY, WPARAM(fState));
	}

void CEditCtrl::SetRect(CRect const &Rect)
{
	SendMessage(EM_SETRECT, 0, LPARAM(PRECT(&Rect)));
	}

void CEditCtrl::SetRectNP(CRect const &Rect)
{
	SendMessage(EM_SETRECTNP, 0, LPARAM(PRECT(&Rect)));
	}

void CEditCtrl::SetSel(CRange const &Sel)
{
	SendMessage(EM_SETSEL, WPARAM(Sel.m_nFrom), LPARAM(Sel.m_nTo));
	}

void CEditCtrl::SetTabStops(UINT uTabs, int const *pTabs)
{
	SendMessage(EM_SETTABSTOPS, WPARAM(uTabs), LPARAM(pTabs));
	}

void CEditCtrl::SetTabStops(CArray <int> const &Array)
{
	SetTabStops(Array.GetCount(), Array.GetPointer());
	}

void CEditCtrl::SetWordBreakProc(FARPROC lpfnProc)
{
	SendMessage(EM_SETWORDBREAKPROC, 0, LPARAM(lpfnProc));
	}

void CEditCtrl::Scroll(UINT uCode)
{
	SendMessage(EM_SCROLL, WPARAM(uCode), 0);
	}

void CEditCtrl::ScrollCaret(void)
{
	SendMessage(EM_SCROLLCARET, 0, 0);
	}

// Clipboard Functions

void CEditCtrl::Undo(void)
{
	SendMessage(EM_UNDO);
	}

void CEditCtrl::Clear(void)
{
	SendMessage(WM_CLEAR);
	}

void CEditCtrl::Copy(void)
{
	SendMessage(WM_COPY);
	}

void CEditCtrl::Cut(void)
{
	SendMessage(WM_CUT);
	}

void CEditCtrl::Paste(void)
{
	if( !(GetWindowStyle() & ES_READONLY) ) {

		if( CanPasteText() ) {

			SendMessage(WM_PASTE);

			SetModify(TRUE);
			}
		}
	}

// Handle Lookup

CEditCtrl & CEditCtrl::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CEditCtrl NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CEditCtrl &) CWnd::FromHandle(hWnd, Class);
	}

// Default Class Name

PCTXT CEditCtrl::GetDefaultClassName(void) const
{
	return L"EDIT";
	}

// Message Map

AfxMessageMap(CEditCtrl, CCtrlWnd)
{
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxMessageEnd(CEditCtrl)
	};

// Command Handlers

BOOL CEditCtrl::OnEditControl(UINT uID, CCmdSource &Src)
{
	BOOL fReadOnly = FALSE;

	if( GetWindowStyle() & ES_READONLY ) {

		fReadOnly = TRUE;
		}

	switch( uID ) {

		case IDM_EDIT_CUT:
		case IDM_EDIT_COPY:
			Src.EnableItem(!fReadOnly && !GetSel().IsEmpty());
			break;

		case IDM_EDIT_DELETE:
			Src.EnableItem(!GetSel().IsEmpty());
			break;

		case IDM_EDIT_PASTE:
			Src.EnableItem(!fReadOnly && CanPasteText());
			break;

		case IDM_EDIT_SELECT_ALL:
			Src.EnableItem(CanSelectAll());
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditCtrl::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_CUT:
			Cut();
			break;

		case IDM_EDIT_COPY:
			Copy();
			break;

		case IDM_EDIT_PASTE:
			Paste();
			break;

		case IDM_EDIT_DELETE:
			Clear();
			break;

		case IDM_EDIT_SELECT_ALL:
			SetSel(CRange(TRUE));
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Implementation

BOOL CEditCtrl::CanPasteText(void) const
{
	CClipboard Clip(ThisObject);

	if( Clip.IsFormatAvailable(CF_UNICODETEXT) ) {

		return TRUE;
		}

	if( Clip.IsFormatAvailable(CF_TEXT) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CEditCtrl::CanSelectAll(void) const
{
	if( GetWindowLong(GWL_STYLE) & ES_MULTILINE ) {

		if( GetLineCount() ) {

			if( GetSel().IsEmpty() ) {

				return TRUE;
				}
			
			return LineLength(NOTHING) > 0;
			}

		return FALSE;
		}

	UINT   uSize     = GetWindowTextLength();

	CRange Selection = GetSel();

	return uSize && Selection.GetCount() < uSize;
	}

// End of File
