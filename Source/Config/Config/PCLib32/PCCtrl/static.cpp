
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Static Control
//

// Dynamic Class

AfxImplementDynamicClass(CStatic, CCtrlWnd);

// Constructor

CStatic::CStatic(void)
{
	}

// Attributes

CIcon & CStatic::GetIcon(void) const
{
	return CIcon::FromHandle(HANDLE(SendMessageConst(STM_GETICON)));
	}

HANDLE CStatic::GetImage(UINT uType) const
{
	return HANDLE(SendMessageConst(STM_GETIMAGE, WPARAM(uType)));
	}

// Operations

void CStatic::SetIcon(CIcon const &Icon)
{
	SendMessage(STM_SETICON, WPARAM(Icon.GetHandle()));
	}

void CStatic::SetImage(UINT uType, HANDLE hImage)
{
	SendMessage(STM_SETIMAGE, WPARAM(uType), LPARAM(hImage));
	}

void CStatic::SetImage(CBitmap &Bitmap)
{
	SetImage(IMAGE_BITMAP, Bitmap.GetHandle());
	}

void CStatic::SetImage(CIcon &Icon)
{
	SetImage(IMAGE_ICON, Icon.GetHandle());
	}

// Handle Lookup

CStatic & CStatic::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CStatic NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CStatic &) CWnd::FromHandle(hWnd, Class);
	}

// Default Class Name

PCTXT CStatic::GetDefaultClassName(void) const
{
	return L"STATIC";
	}

// End of File
