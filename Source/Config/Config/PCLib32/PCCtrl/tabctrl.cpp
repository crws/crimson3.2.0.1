
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Tab Control
//

// Dynamic Class

AfxImplementDynamicClass(CTabCtrl, CCtrlWnd);

// Constructor

CTabCtrl::CTabCtrl(void)
{
	LoadControlClass(ICC_TAB_CLASSES);
	}

// Attributes

void CTabCtrl::AdjustRect(BOOL fGrow, CRect &Rect) const
{
	SendMessageConst(TCM_ADJUSTRECT, fGrow, LPARAM(&Rect));
	}

UINT CTabCtrl::GetCurFocus(void) const
{
	return UINT(SendMessageConst(TCM_GETCURFOCUS));
	}

UINT CTabCtrl::GetCurSel(void) const
{
	return UINT(SendMessageConst(TCM_GETCURSEL));
	}

DWORD CTabCtrl::GetExtendedSyle(void) const
{
	return DWORD(SendMessageConst(TCM_GETEXTENDEDSTYLE));
	}

CImageList & CTabCtrl::GetImageList(void) const
{
	return CImageList::FromHandle(HIMAGELIST(SendMessageConst(TCM_GETIMAGELIST)));
	}

BOOL CTabCtrl::GetItem(UINT uItem, TCITEM &Item) const
{
	return BOOL(SendMessageConst(TCM_GETITEM, uItem, LPARAM(&Item)));
	}

CTabItem CTabCtrl::GetItem(UINT uItem) const
{
	CTabItem Item;

	Item.SetMask( TCIF_IMAGE
		    | TCIF_PARAM
		    | TCIF_STATE
		    | TCIF_TEXT
		    );
	
	Item.SetTextBuffer(256);

	GetItem(uItem, Item);

	return Item;
	}

CString CTabCtrl::GetItemText(UINT uItem) const
{
	CTabItem Item;

	Item.SetTextBuffer(256);

	GetItem(uItem, Item);

	return Item.GetText();
	}

INT CTabCtrl::GetItemImage(UINT uItem) const
{
	CTabItem Item(TCIF_IMAGE);

	GetItem(uItem, Item);

	return Item.GetImage();
	}

DWORD CTabCtrl::GetItemState(UINT uItem) const
{
	CTabItem Item(TCIF_STATE);

	GetItem(uItem, Item);

	return Item.GetState();
	}

UINT CTabCtrl::GetItemCount(void) const
{
	return UINT(SendMessageConst(TCM_GETITEMCOUNT));
	}

CRect CTabCtrl::GetItemRect(UINT uItem) const
{
	CRect Rect;

	SendMessageConst(TCM_GETITEMRECT, uItem, LPARAM(&Rect));

	return Rect;
	}

UINT CTabCtrl::GetRowCount(void) const
{
	return UINT(SendMessageConst(TCM_GETROWCOUNT));
	}

CToolTip & CTabCtrl::GetToolTips(void) const
{
	return CToolTip::FromHandle(HWND(SendMessageConst(TCM_GETTOOLTIPS)));
	}

// Operations

void CTabCtrl::AddToolTip(UINT uItem, PCTXT pText)
{
	GetToolTips().AddTool(CToolInfo(m_hWnd, uItem, GetItemRect(uItem), pText));
	}

UINT CTabCtrl::AppendItem(TCITEM const &Item)
{
	return InsertItem(10000, Item);
	}

void CTabCtrl::DeleteAllItems(void)
{
	SendMessage(TCM_DELETEALLITEMS);
	}

void CTabCtrl::DeleteItem(UINT uItem)
{
	SendMessage(TCM_DELETEITEM, uItem);
	}

void CTabCtrl::DeselectAll(BOOL fExFocus)
{
	SendMessage(TCM_DESELECTALL, fExFocus);
	}

void CTabCtrl::HighlightItem(UINT uItem, BOOL fShow)
{
	SendMessage(TCM_HIGHLIGHTITEM, uItem, fShow);
	}

UINT CTabCtrl::InsertItem(UINT uItem, TCITEM const &Item)
{
	return INT(SendMessage(TCM_INSERTITEM, uItem, LPARAM(&Item)));
	}

void CTabCtrl::RemoveImage(UINT uImage)
{
	SendMessage(TCM_REMOVEIMAGE, uImage);
	}

void CTabCtrl::SetCurFocus(UINT uItem)
{
	SendMessage(TCM_SETCURFOCUS, uItem);
	}

void CTabCtrl::SetCurSel(UINT uItem)
{
	SendMessage(TCM_SETCURSEL, uItem);
	}

void CTabCtrl::SetExtendedStyle(DWORD dwMask, DWORD dwStyle)
{
	SendMessage(TCM_SETEXTENDEDSTYLE, dwMask, dwStyle);
	}

void CTabCtrl::SetImageList(HIMAGELIST hList)
{
	SendMessage(TCM_SETIMAGELIST, 0, LPARAM(hList));
	}

void CTabCtrl::SetItem(UINT uItem, TCITEM const &Item)
{
	SendMessage(TCM_SETITEM, uItem, LPARAM(&Item));
	}

void CTabCtrl::SetItemText(UINT uItem, PCTXT pText)
{
	CTabItem Item;

	Item.SetText(pText);

	SetItem(uItem, Item);
	}

void CTabCtrl::SetItemImage(UINT uItem, UINT uImage)
{
	CTabItem Item;

	Item.SetImage(uImage);

	SetItem(uItem, Item);
	}

void CTabCtrl::SetItemState(UINT uItem, DWORD dwState)
{
	CTabItem Item;

	Item.SetState(dwState);

	SetItem(uItem, Item);
	}

void CTabCtrl::SetItemSize(CSize const &Size)
{
	SendMessage(TCM_SETITEMSIZE, 0, LPARAM(Size));
	}

void CTabCtrl::SetMinTabWidth(int cx)
{
	SendMessage(TCM_SETMINTABWIDTH, cx);
	}

void CTabCtrl::SetPadding(CSize const &Size)
{
	SendMessage(TCM_SETPADDING, 0, LPARAM(Size));
	}

void CTabCtrl::SetToolTips(CToolTip &Tips)
{
	SendMessage(TCM_SETTOOLTIPS, WPARAM(HWND(Tips)));
	}

// Handle Lookup

CTabCtrl & CTabCtrl::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CTabCtrl NullObject;

		return NullObject;
		}

	return (CTabCtrl &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CTabCtrl::GetDefaultClassName(void) const
{
	return WC_TABCONTROL;
	}

// Message Map

AfxMessageMap(CTabCtrl, CCtrlWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)

	AfxMessageEnd(CTabCtrl)
	};

// Message Handlers

BOOL CTabCtrl::OnEraseBkGnd(CDC &DC)
{
	// LATER -- This can cause the tabs to flicker! The real
	// solution here is to draw the whole thing using visual
	// styles from within our own WM_PAINT handler. That is
	// going to be tricky, but it will work the best.

	CRect Rect = GetItemRect(0);

	CRect Fill = GetClientRect();

	Fill.top    = Rect.bottom - 0;

	Fill.left   = Fill.left   + 2;

	Fill.right  = Fill.right  - 2;

	Fill.bottom = Fill.bottom - 2;

	DC.FillRect(Fill, afxBrush(TabFace));

	Fill        = GetClientRect();
	
	Fill.bottom = Rect.bottom;

	DC.FillRect(Fill, afxBrush(3dFace));

	return TRUE;
	}

// End of File
