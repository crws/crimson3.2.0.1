
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Spinner Control
//

// Dynamic Class

AfxImplementDynamicClass(CSpinner, CCtrlWnd);

// Constructor

CSpinner::CSpinner(void)
{
	LoadControlClass(ICC_UPDOWN_CLASS);
	}

// Attributes

UINT CSpinner::GetAccel(UINT uCount, UDACCEL *pAccel) const
{
	return UINT(SendMessageConst(UDM_GETACCEL, uCount, LPARAM(pAccel)));
	}

UINT CSpinner::GetAccel(CArray <UDACCEL> &Array) const
{
	Array.Empty();

	UINT uCount = GetAccel(0, NULL);

	Array.Expand(uCount);

	GetAccel(uCount, (UDACCEL *) Array.GetPointer());

	return uCount;
	}

UINT CSpinner::GetBase(void) const
{
	return UINT(SendMessageConst(UDM_GETBASE));
	}

CWnd & CSpinner::GetBuddy(void) const
{
	return CWnd::FromHandle(HWND(SendMessageConst(UDM_GETBUDDY)));
	}

INT CSpinner::GetPos(void) const
{
	return INT(SendMessageConst(UDM_GETPOS));
	}

CRange CSpinner::GetRange(void) const
{
	return CRange(SendMessageConst(UDM_GETRANGE), TRUE);
	}

INT CSpinner::GetRangeMin(void) const
{
	return HIWORD(SendMessageConst(UDM_GETRANGE));
	}

INT CSpinner::GetRangeMax(void) const
{
	return LOWORD(SendMessageConst(UDM_GETRANGE));
	}

// Operations

BOOL CSpinner::SetAccel(UINT uCount, UDACCEL const *pAccel)
{
	return BOOL(SendMessage(UDM_SETACCEL, uCount, LPARAM(pAccel)));
	}

BOOL CSpinner::SetAccel(CArray <UDACCEL> const &Array)
{
	return BOOL(SendMessage(UDM_SETACCEL, Array.GetCount(), LPARAM(Array.GetPointer())));
	}

UINT CSpinner::SetBase(UINT uBase)
{
	return UINT(SendMessage(UDM_SETBASE, uBase));
	}

void CSpinner::SetBuddy(HWND hWnd)
{
	SendMessage(UDM_SETBUDDY, WPARAM(hWnd));
	}

INT CSpinner::SetPos(INT nPos)
{
	return INT(SendMessage(UDM_SETPOS, 0, LPARAM(nPos)));
	}

void CSpinner::SetRange(INT nMin, INT nMax)
{
	SendMessage(UDM_SETRANGE, 0, MAKELPARAM(nMax, nMin));
	}

void CSpinner::SetRange(CRange const &Range)
{
	SendMessage(UDM_SETRANGE, 0, MAKELPARAM(Range.m_nTo, Range.m_nFrom));
	}

// Handle Lookup

CSpinner & CSpinner::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CSpinner NullObject;

		return NullObject;
		}

	return (CSpinner &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CSpinner::GetDefaultClassName(void) const
{
	return UPDOWN_CLASS;
	}

// Message Map

AfxMessageMap(CSpinner, CCtrlWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)

	AfxMessageEnd(CSpinner)
	};

// Message Handlers

BOOL CSpinner::OnEraseBkGnd(CDC &DC)
{
	DC.FillRect(GetClientRect(), afxBrush(WindowBack));

	return TRUE;
	}

// End of File
