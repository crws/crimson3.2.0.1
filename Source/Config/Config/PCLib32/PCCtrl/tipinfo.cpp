 
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Tool Tip Info
//

// Warning Control

#pragma warning(disable: 4458)

// Constructors

CToolInfo::CToolInfo(void)
{
	memset((TOOLINFO *) this, 0, TTTOOLINFOW_V2_SIZE);

	cbSize = TTTOOLINFOW_V2_SIZE;
	}

CToolInfo::CToolInfo(CToolInfo const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	memcpy((TOOLINFO *) this, &That, That.cbSize);

	cbSize = TTTOOLINFOW_V2_SIZE;

	m_Text = That.m_Text;
	}

CToolInfo::CToolInfo(TOOLINFO const &Info)
{
	AfxValidateReadPtr(&Info, sizeof(Info));

	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	memcpy((TOOLINFO *) this, &Info, Info.cbSize);

	cbSize = TTTOOLINFOW_V2_SIZE;
	}

CToolInfo::CToolInfo(HWND hWnd, UINT uID, CRect const &Rect, PCTXT pText, UINT uFlags)
{
	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	cbSize = TTTOOLINFOW_V2_SIZE;

	SetFlags(uFlags | TTF_TRANSPARENT);

	SetID(hWnd, uID);

	SetRect(Rect);

	SetText(pText);
	}

CToolInfo::CToolInfo(HWND hWnd, HWND hCtrl, CRect const &Rect, PCTXT pText, UINT uFlags)
{
	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	cbSize = TTTOOLINFOW_V2_SIZE;

	SetFlags(uFlags | TTF_TRANSPARENT);

	SetID(hWnd, hCtrl);

	SetRect(Rect);

	SetText(pText);
	}

CToolInfo::CToolInfo(HWND hWnd, UINT uID, CRect const &Rect, PCTXT pText)
{
	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	cbSize = TTTOOLINFOW_V2_SIZE;

	SetFlags(TTF_TRANSPARENT);

	SetID(hWnd, uID);

	SetRect(Rect);

	SetText(pText);
	}

CToolInfo::CToolInfo(HWND hWnd, HWND hCtrl, CRect const &Rect, PCTXT pText)
{
	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	cbSize = TTTOOLINFOW_V2_SIZE;

	SetFlags(TTF_TRANSPARENT);

	SetID(hWnd, hCtrl);

	SetRect(Rect);

	SetText(pText);
	}

CToolInfo::CToolInfo(HWND hWnd, UINT uID)
{
	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	cbSize = TTTOOLINFOW_V2_SIZE;

	SetID(hWnd, uID);
	}

CToolInfo::CToolInfo(HWND hWnd, HWND hCtrl)
{
	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	cbSize = TTTOOLINFOW_V2_SIZE;

	SetID(hWnd, hCtrl);
	}

// Assignment Operators

CToolInfo const & CToolInfo::operator = (CToolInfo const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	memcpy((TOOLINFO *) this, &That, That.cbSize);

	cbSize = TTTOOLINFOW_V2_SIZE;

	m_Text = That.m_Text;

	return ThisObject;
	}

CToolInfo const & CToolInfo::operator = (TOOLINFO const &Info)
{
	AfxValidateReadPtr(&Info, sizeof(Info));

	memset((TOOLINFO *) this, 0, sizeof(TOOLINFO));

	memcpy((TOOLINFO *) this, &Info, Info.cbSize);

	cbSize = TTTOOLINFOW_V2_SIZE;

	return ThisObject;
	}

// Attributes

UINT CToolInfo::GetFlags(void) const
{
	return uFlags;
	}

BOOL CToolInfo::TestFlag(UINT uFlags) const
{
	return (this->uFlags & uFlags) ? TRUE : FALSE;
	}

CWnd & CToolInfo::GetWnd(void) const
{
	return CWnd::FromHandle(hwnd);
	}

WPARAM CToolInfo::GetID(void) const
{
	return uId;
	}

CRect CToolInfo::GetRect(void) const
{
	return rect;
	}

HINSTANCE CToolInfo::GetInstance(void) const
{
	return hinst;
	}

PCTXT CToolInfo::GetText(void) const
{
	return lpszText;
	}

LPARAM CToolInfo::GetParam(void) const
{
	return lParam;
	}

// Operations

void CToolInfo::SetFlags(UINT uSet)
{
	this->uFlags = uSet;
	}

void CToolInfo::SetWnd(HWND hWnd)
{
	this->uFlags = this->uFlags | TTF_SUBCLASS;

	this->hwnd   = hWnd;
	}

void CToolInfo::SetID(WPARAM uID)
{
	this->uId = uID;
	}

void CToolInfo::SetID(HWND hCtrl)
{
	this->uFlags = this->uFlags | TTF_IDISHWND;

	this->uId    = WPARAM(hCtrl);
	}

void CToolInfo::SetID(HWND hWnd, WPARAM uID)
{
	SetWnd(hWnd);

	SetID(uID);
	}

void CToolInfo::SetID(HWND hWnd, HWND hCtrl)
{
	SetWnd(hWnd);

	SetID(hCtrl);
	}

void CToolInfo::SetRect(CRect const &Rect)
{
	this->rect = Rect;
	}

void CToolInfo::SetInstance(HINSTANCE hInstance)
{
	this->hinst = hInstance;
	}

void CToolInfo::SetText(CString const &Text)
{
	m_Text     = Text;

	lpszText   = PTXT(PCTXT(m_Text));
	}

void CToolInfo::SetText(PCTXT pText)
{
	m_Text   = pText;

	lpszText = PTXT(PCTXT(m_Text));
	}

void CToolInfo::SetTextBuffer(UINT uCount)
{
	m_Text   = CString(' ', uCount);

	lpszText = PTXT(PCTXT(m_Text));
	}

void CToolInfo::SetParam(LPARAM lParam)
{
	this->lParam = lParam;
	}

// End of File
