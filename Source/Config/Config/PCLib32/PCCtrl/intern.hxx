
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//
//

#define IDS_BASE                0x4000
#define IDS_BALLOON_TIPS_1      0x4000
#define IDS_BALLOON_TIPS_2      0x4001
#define IDS_BALLOON_TIPS_3      0x4002
#define IDS_BALLOON_TIPS_4      0x4003

// End of File

#endif
