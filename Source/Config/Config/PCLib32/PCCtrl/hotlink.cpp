
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hot Link Control
//

// Dynamic Class

AfxImplementDynamicClass(CHotLinkCtrl, CCtrlWnd);

// Static Data

UINT CHotLinkCtrl::m_timerCheck = CWnd::AllocTimerID();

// Constructor

CHotLinkCtrl::CHotLinkCtrl(void)
{
	m_fHover = FALSE;

	m_fSpace = FALSE;

	m_fPress = FALSE;
	}

// Destructor

CHotLinkCtrl::~CHotLinkCtrl(void)
{
	}

// Message Map

AfxMessageMap(CHotLinkCtrl, CCtrlWnd)
{
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_ENABLE)
	AfxDispatchMessage(WM_GETDLGCODE)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)
	
	AfxMessageEnd(CTestWnd)
	};

// Message Handlers

BOOL CHotLinkCtrl::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	DC.FillRect(Rect, afxBrush(TabFace));

	return TRUE;
	}

void CHotLinkCtrl::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	LOGFONT LogFont;

	GetObject(afxFont(Dialog), sizeof(LogFont), &LogFont);

	LogFont.lfUnderline = BYTE(m_fHover ? 1 : 0);

	CFont  Font = CFont(LogFont);

	CRect  Rect = GetClientRect();

	CColor Col  = RGB(128,128,128);

	if( IsEnabled() ) {

		if( m_fSpace || (m_fHover && m_fPress) ) {

			Col = RGB(0,0,160);
			}
		else
			Col = RGB(0,0,255);
		}

	DC.Select(Font);

	DC.SetTextColor(Col);

	DC.SetBkMode(TRANSPARENT);

	CString Text = GetLinkText();

	CSize   Size = DC.GetTextExtent(Text);

	CPoint  Pos  = CPoint(3, (Rect.cy() - Size.cy) / 2);

	DC.TextOut(Pos, Text);

	if( HasFocus() ) {

		CRect Rect = CRect(Pos, Size) + CSize(3, 2);

		DC.SetTextColor(afxColor(WindowText));

		DC.DrawFocusRect(Rect);
		}

	DC.Deselect();
	}

void CHotLinkCtrl::OnSetFocus(CWnd &Wnd)
{
	Invalidate(TRUE);
	}

void CHotLinkCtrl::OnKillFocus(CWnd &Wnd)
{
	Invalidate(TRUE);
	}

void CHotLinkCtrl::OnLButtonDown(UINT uCode, CPoint Pos)
{
	if( IsEnabled() ) {

		if( InText(Pos) ) {

			if( !m_fPress ) {
			
				m_fPress = TRUE;

				SetCapture();

				Invalidate(TRUE);
				}

			SetFocus();
			}
		}
	}

void CHotLinkCtrl::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	OnLButtonDown(uCode, Pos);
	}

void CHotLinkCtrl::OnLButtonUp(UINT uCode, CPoint Pos)
{
	if( m_fPress ) {

		if( m_fHover ) {

			CWnd &Wnd = GetParent();

			Wnd.PostMessage(WM_COMMAND, GetID(), LPARAM(m_hWnd));
			}

		ReleaseCapture();

		m_fPress = FALSE;

		m_fHover = FALSE;

		Invalidate(TRUE);
		}
	}

void CHotLinkCtrl::OnMouseMove(UINT uCode, CPoint Pos)
{
	if( IsEnabled() ) {

		if( InText(Pos) ) {

			if( !m_fHover ) {

				CPoint Pos  = GetCursorPos();

				CRect  Rect = GetWindowRect();

				if( Rect.PtInRect(Pos) ) {

					m_fHover = TRUE;

					SetTimer(m_timerCheck, 50);

					Invalidate(TRUE);
					}
				}
			}
		}
	}

BOOL CHotLinkCtrl::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( InText() ) {

		SetCursor(CCursor(IDC_HAND));

		return 1;
		}

	return 0;
	}

void CHotLinkCtrl::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerCheck ) {

		if( m_fHover ) {

			if( !InText() ) {

				m_fHover = FALSE;

				KillTimer(m_timerCheck);

				Invalidate(TRUE);
				}

			return;
			}

		KillTimer(m_timerCheck);
		}
	}

void CHotLinkCtrl::OnEnable(BOOL fEnable)
{
	Invalidate(TRUE);
	}

UINT CHotLinkCtrl::OnGetDlgCode(MSG *pMsg)
{
	UINT uCode = DLGC_WANTCHARS;

	if( pMsg ) {

		if( pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP ) {

			if( pMsg->wParam == VK_RETURN ) {

				uCode |= DLGC_WANTMESSAGE;
				}

			if( pMsg->wParam == VK_SPACE ) {

				uCode |= DLGC_WANTMESSAGE;
				}
			}
		}

	return uCode;
	}

void CHotLinkCtrl::OnKeyDown(UINT uCode, DWORD dwData)
{
	if( uCode == VK_RETURN || uCode == VK_SPACE ) {

		if( !m_fSpace ) {

			m_fSpace = TRUE;

			Invalidate(TRUE);
			}
		}
	}

void CHotLinkCtrl::OnKeyUp(UINT uCode, DWORD dwData)
{
	if( uCode == VK_RETURN || uCode == VK_SPACE ) {

		if( m_fSpace ) {

			CWnd &Wnd = GetParent();

			Wnd.PostMessage(WM_COMMAND, GetID(), LPARAM(m_hWnd));

			m_fSpace = FALSE;

			Invalidate(TRUE);
			}
		}
	}

// Implementation

CString CHotLinkCtrl::GetLinkText(void)
{
	CString Text = GetWindowText();

	UINT    uLen = Text.GetLength();

	if( uLen > 3 ) {

		if( Text.Mid(uLen-3, 3) == L"..." ) {

			return Text.Left(uLen-3);
			}
		}

	return Text;
	}

BOOL CHotLinkCtrl::InText(CPoint Pos)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	CRect   Rect = GetClientRect();

	CString Text = GetLinkText();

	CSize   Size = DC.GetTextExtent(Text);

	CPoint  Loc  = CPoint(3, (Rect.cy() - Size.cy) / 2);

	CRect   Draw = CRect(Loc, Size) + CSize(3, 2);

	DC.Deselect();

	return Draw.PtInRect(Pos);
	}

BOOL CHotLinkCtrl::InText(void)
{
	CPoint Pos = GetCursorPos();

	ScreenToClient(Pos);

	return InText(Pos);
	}

// End of File
