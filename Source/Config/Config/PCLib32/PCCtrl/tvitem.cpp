
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Tree View Item
//

// Warning Control

#pragma warning(disable: 4458)

// Constructors

CTreeViewItem::CTreeViewItem(void)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));
	}

CTreeViewItem::CTreeViewItem(CTreeViewItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((TVITEMEX *) this, &That, sizeof(TVITEMEX));

	m_Text = That.m_Text;
	}

CTreeViewItem::CTreeViewItem(TVITEMEX const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((TVITEMEX *) this, &Item, sizeof(TVITEMEX));
	}

CTreeViewItem::CTreeViewItem(CString const &Text, UINT uImage)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetText(Text);

	SetImages(uImage, uImage);
	}

CTreeViewItem::CTreeViewItem(PCTXT pText, UINT uImage)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetText(pText);

	SetImages(uImage, uImage);
	}

CTreeViewItem::CTreeViewItem(CString const &Text, UINT uImage, UINT uState)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetText(Text);

	SetImages(uImage, uImage);

	SetState(uState);
	}

CTreeViewItem::CTreeViewItem(PCTXT pText, UINT uImage, UINT uState)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetText(pText);

	SetImages(uImage, uImage);

	SetState(uState);
	}

CTreeViewItem::CTreeViewItem(HTREEITEM hItem)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetHandle(hItem);
	}

CTreeViewItem::CTreeViewItem(HTREEITEM hItem, CString const &Text)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetHandle(hItem);

	SetText(Text);
	}

CTreeViewItem::CTreeViewItem(HTREEITEM hItem, PCTXT pText)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetHandle(hItem);

	SetText(pText);
	}

CTreeViewItem::CTreeViewItem(HTREEITEM hItem, UINT uMask)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetHandle(hItem);

	SetMask(uMask);
	}

CTreeViewItem::CTreeViewItem(UINT uMask)
{
	memset((TVITEMEX *) this, 0, sizeof(TVITEMEX));

	SetMask(uMask);
	}

// Assignment Operators

CTreeViewItem const & CTreeViewItem::operator = (CTreeViewItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((TVITEMEX *) this, &That, sizeof(TVITEMEX));

	m_Text = That.m_Text;

	return ThisObject;
	}

CTreeViewItem const & CTreeViewItem::operator = (TVITEMEX const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((TVITEMEX *) this, &Item, sizeof(TVITEMEX));

	return ThisObject;
	}

// Attributes

UINT CTreeViewItem::GetMask(void) const
{
	return mask;
	}

BOOL CTreeViewItem::TestMask(UINT uMask) const
{
	return (mask & uMask) ? TRUE : FALSE;
	}

HTREEITEM CTreeViewItem::GetHandle(void) const
{
	AfxAssert(mask & TVIF_HANDLE);

	return hItem;
	}

UINT CTreeViewItem::GetState(void) const
{
	AfxAssert(mask & TVIF_STATE);

	return state;
	}

UINT CTreeViewItem::GetStateMask(void) const
{
	AfxAssert(mask & TVIF_STATE);

	return stateMask;
	}

PCTXT CTreeViewItem::GetText(void) const
{
	AfxAssert(mask & TVIF_TEXT);

	return pszText;
	}

UINT CTreeViewItem::GetImage(void) const
{
	AfxAssert(mask & TVIF_IMAGE);

	return iImage;
	}

UINT CTreeViewItem::GetSelectedImage(void) const
{
	AfxAssert(mask & TVIF_SELECTEDIMAGE);

	return iSelectedImage;
	}

UINT CTreeViewItem::GetChildren(void) const
{
	AfxAssert(mask & TVIF_CHILDREN);

	return cChildren;
	}

LPARAM CTreeViewItem::GetParam(void) const
{
	AfxAssert(mask & TVIF_PARAM);

	return lParam;
	}

int CTreeViewItem::GetHeight(void) const
{
	AfxAssert(mask & TVIF_INTEGRAL);

	return iIntegral;
	}

// Operations

void CTreeViewItem::SetMask(UINT uMask)
{
	mask = uMask;
	}

void CTreeViewItem::SetHandle(HTREEITEM hHandle)
{
	mask |= TVIF_HANDLE;

	hItem = hHandle;
	}

void CTreeViewItem::SetState(UINT uState)
{
	mask |= TVIF_STATE;

	state = uState;
	}

void CTreeViewItem::SetState(UINT uState, UINT uMask)
{
	mask |= TVIF_STATE;

	stateMask = uMask;

	state     = uState;
	}

void CTreeViewItem::SetStateMask(UINT uMask)
{
	mask |= TVIF_STATE;

	stateMask = uMask;
	}

void CTreeViewItem::SetStateImage(UINT uImage)
{
	mask      |= TVIF_STATE;

	state     |= (uImage << 12);

	stateMask |= TVIS_STATEIMAGEMASK;
	}

void CTreeViewItem::SetImage(UINT uImage)
{
	mask |= TVIF_IMAGE;

	iImage = uImage;
	}

void CTreeViewItem::SetSelectedImage(UINT uSelect)
{
	mask |= TVIF_SELECTEDIMAGE;

	iSelectedImage = uSelect;
	}

void CTreeViewItem::SetImages(UINT uImage, UINT uSelect)
{
	mask |= TVIF_IMAGE | TVIF_SELECTEDIMAGE;

	iImage         = uImage;

	iSelectedImage = uSelect;
	}

void CTreeViewItem::SetImages(UINT uImage)
{
	mask |= TVIF_IMAGE | TVIF_SELECTEDIMAGE;

	iImage         = uImage;

	iSelectedImage = uImage;
	}

void CTreeViewItem::SetText(PCTXT pText)
{
	mask |= TVIF_TEXT;

	m_Text     = pText;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CTreeViewItem::SetText(CString const &Text)
{
	mask |= TVIF_TEXT;

	m_Text     = Text;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CTreeViewItem::SetTextBuffer(UINT uCount)
{
	mask |= TVIF_TEXT;

	m_Text     = CString(' ', uCount);

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CTreeViewItem::SetChildren(UINT uChildren)
{
	mask |= TVIF_CHILDREN;

	cChildren = uChildren;
	}

void CTreeViewItem::SetParam(LPARAM lParam)
{
	mask |= TVIF_PARAM;

	this->lParam = lParam;
	}

void CTreeViewItem::SetHeight(int nHeight)
{
	mask |= TVIF_INTEGRAL;

	iIntegral = nHeight;
	}

// End of File
