
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Month Calendar Control
//

// Dynamic Class

AfxImplementDynamicClass(CMonthCtrl, CCtrlWnd);

// Constructor

CMonthCtrl::CMonthCtrl(void)
{
	LoadControlClass(ICC_DATE_CLASSES);
	}

// Attributes

CColor CMonthCtrl::GetColor(UINT uIndex) const
{
	COLORREF clr = COLORREF(SendMessageConst(MCM_GETCOLOR, uIndex));

	return CColor(clr);
	}

BOOL CMonthCtrl::GetCurSel(SYSTEMTIME &Time) const
{
	AfxValidateWritePtr(&Time, sizeof(Time));

	return BOOL(SendMessageConst(MCM_GETCURSEL, 0, LPARAM(&Time)));
	}

SYSTEMTIME CMonthCtrl::GetCurSel(void) const
{
	SYSTEMTIME Time;

	GetCurSel(Time);

	return Time;
	}

UINT CMonthCtrl::GetFirstDayOfWeek(void) const
{
	return LOWORD(SendMessageConst(MCM_GETFIRSTDAYOFWEEK));
	}

UINT CMonthCtrl::GetMaxSelCount(void) const
{
	return UINT(SendMessageConst(MCM_GETMAXSELCOUNT));
	}

int CMonthCtrl::GetMaxTodayWidth(void) const
{
	return int(SendMessageConst(MCM_GETMAXTODAYWIDTH));
	}

CRect CMonthCtrl::GetMinReqRect(void) const
{
	CRect Result;

	SendMessageConst(MCM_GETMINREQRECT, 0, LPARAM(PRECT(&Result)));
	
	return Result;
	}
	
int CMonthCtrl::GetMonthDelta(void) const
{
	return int(SendMessageConst(MCM_GETMONTHDELTA));
	}

UINT CMonthCtrl::GetMonthRange(DWORD dwFlags, SYSTEMTIME *pTimeArray) const
{
	AfxValidateWritePtr(pTimeArray, 2 * sizeof(SYSTEMTIME));

	return UINT(SendMessageConst(MCM_GETMONTHRANGE, dwFlags, LPARAM(pTimeArray)));
	}

DWORD CMonthCtrl::GetRange(SYSTEMTIME *pTimeArray) const
{
	AfxValidateWritePtr(pTimeArray, 2 * sizeof(SYSTEMTIME));

	return DWORD(SendMessageConst(MCM_GETRANGE, 0, LPARAM(pTimeArray)));
	}

BOOL CMonthCtrl::GetSelRange(SYSTEMTIME *pTimeArray) const
{
	AfxValidateWritePtr(pTimeArray, 2 * sizeof(SYSTEMTIME));

	return BOOL(SendMessageConst(MCM_GETSELRANGE, 0, LPARAM(pTimeArray)));
	}

BOOL CMonthCtrl::GetToday(SYSTEMTIME &Time) const
{
	return BOOL(SendMessageConst(MCM_GETTODAY, 0, LPARAM(&Time)));
	}

SYSTEMTIME CMonthCtrl::GetToday(void) const
{
	SYSTEMTIME Time;

	GetToday(Time);

	return Time;
	}

DWORD CMonthCtrl::HitTest(MCHITTESTINFO &Info) const
{
	AfxValidateWritePtr(&Info, sizeof(Info));

	return DWORD(SendMessageConst(MCM_HITTEST, 0, LPARAM(&Info)));
	}

UINT CMonthCtrl::HitTestCode(CPoint const &Pos) const
{
	MCHITTESTINFO Info;

	Info.cbSize = sizeof(Info);
	
	Info.pt     = Pos;

	HitTest(Info);

	return Info.uHit;
	}

UINT CMonthCtrl::HitTestTime(CPoint const &Pos, SYSTEMTIME &Time) const
{
	MCHITTESTINFO Info;

	Info.cbSize = sizeof(Info);
	
	Info.pt     = Pos;

	HitTest(Info);

	Time = Info.st;

	return Info.uHit;
	}

SYSTEMTIME CMonthCtrl::HitTestTime(CPoint const &Pos) const
{
	MCHITTESTINFO Info;

	Info.cbSize = sizeof(Info);
	
	Info.pt     = Pos;

	HitTest(Info);

	return Info.st;
	}

// Operations

CColor CMonthCtrl::SetColor(UINT uIndex, CColor const &Color)
{
	AfxValidateReadPtr(&Color, sizeof(Color));

	COLORREF clr = COLORREF(SendMessageConst(MCM_SETCOLOR, uIndex, LPARAM(COLORREF(Color))));

	return CColor(clr);
	}

BOOL CMonthCtrl::SetCurSel(SYSTEMTIME const &Time)
{
	AfxValidateReadPtr(&Time, sizeof(Time));

	return BOOL(SendMessageConst(MCM_SETCURSEL, 0, LPARAM(&Time)));
	}

BOOL CMonthCtrl::SetDayState(UINT uCount, MONTHDAYSTATE const *pState)
{
	AfxValidateReadPtr(pState, uCount * sizeof(MONTHDAYSTATE));

	return BOOL(SendMessageConst(MCM_SETDAYSTATE, uCount, LPARAM(pState)));
	}

BOOL CMonthCtrl::SetDayState(CArray <MONTHDAYSTATE> const &Array)
{
	return SetDayState(Array.GetCount(), Array.GetPointer());
	}
	
DWORD CMonthCtrl::SetFirstDayOfWeek(UINT uDay)
{
	return DWORD(SendMessageConst(MCM_SETFIRSTDAYOFWEEK, 0, LPARAM(uDay)));
	}

BOOL CMonthCtrl::SetMaxSelCount(UINT uCount)
{
	return BOOL(SendMessageConst(MCM_SETMAXSELCOUNT, WPARAM(uCount)));
	}

BOOL CMonthCtrl::SetMonthDelta(UINT uDelta)
{
	return BOOL(SendMessageConst(MCM_SETMONTHDELTA, WPARAM(uDelta)));
	}
	
BOOL CMonthCtrl::SetRange(UINT uFlags, SYSTEMTIME const *pTimeArray)
{
	AfxValidateReadPtr(pTimeArray, 2 * sizeof(SYSTEMTIME));

	return BOOL(SendMessageConst(MCM_SETRANGE, uFlags, LPARAM(pTimeArray)));
	}

BOOL CMonthCtrl::SetSelRange(SYSTEMTIME const *pTimeArray)
{
	AfxValidateReadPtr(pTimeArray, 2 * sizeof(SYSTEMTIME));

	return BOOL(SendMessageConst(MCM_SETSELRANGE, 0, LPARAM(pTimeArray)));
	}

BOOL CMonthCtrl::SetToday(SYSTEMTIME const &Time)
{
	AfxValidateReadPtr(&Time, sizeof(Time));

	return BOOL(SendMessageConst(MCM_SETTODAY, 0, LPARAM(&Time)));
	}

// Handle Lookup

CMonthCtrl & CMonthCtrl::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CMonthCtrl NullObject;

		return NullObject;
		}

	return (CMonthCtrl &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CMonthCtrl::GetDefaultClassName(void) const
{
	return MONTHCAL_CLASS;
	}

// End of File
