
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// List View Column
//

// Warning Control

#pragma warning(disable: 4458)

// Constructors

CListViewColumn::CListViewColumn(void)
{
	memset((LVCOLUMN *) this, 0, sizeof(LVCOLUMN));
	}

CListViewColumn::CListViewColumn(CListViewColumn const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((LVCOLUMN *) this, &That, sizeof(LVCOLUMN));

	m_Text = That.m_Text;
	}

CListViewColumn::CListViewColumn(LVCOLUMN const &Column)
{
	AfxValidateReadPtr(&Column, sizeof(Column));

	memcpy((LVCOLUMN *) this, &Column, sizeof(LVCOLUMN));
	}

CListViewColumn::CListViewColumn(UINT uSub, UINT uFormat, int cx, CString const &Text, UINT uImage)
{
	memset((LVCOLUMN *) this, 0, sizeof(LVCOLUMN));

	SetSubItem(uSub);

	SetFormat(uFormat);

	SetWidth(cx);

	SetText(Text);

	SetImage(uImage);
	}

CListViewColumn::CListViewColumn(UINT uSub, UINT uFormat, int cx, PCTXT pText, UINT uImage)
{
	memset((LVCOLUMN *) this, 0, sizeof(LVCOLUMN));

	SetSubItem(uSub);

	SetFormat(uFormat);

	SetWidth(cx);

	SetText(pText);

	SetImage(uImage);
	}

CListViewColumn::CListViewColumn(UINT uSub, UINT uFormat, int cx, CString const &Text)
{
	memset((LVCOLUMN *) this, 0, sizeof(LVCOLUMN));

	SetSubItem(uSub);

	SetFormat(uFormat);

	SetWidth(cx);

	SetText(Text);
	}

CListViewColumn::CListViewColumn(UINT uSub, UINT uFormat, int cx, PCTXT pText)
{
	memset((LVCOLUMN *) this, 0, sizeof(LVCOLUMN));

	SetSubItem(uSub);

	SetFormat(uFormat);

	SetWidth(cx);

	SetText(pText);
	}

CListViewColumn::CListViewColumn(UINT uMask)
{
	memset((LVCOLUMN *) this, 0, sizeof(LVCOLUMN));

	SetMask(uMask);
	}

// Assignment Operators

CListViewColumn const & CListViewColumn::operator = (CListViewColumn const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((LVCOLUMN *) this, &That, sizeof(LVCOLUMN));

	m_Text = That.m_Text;

	return ThisObject;
	}

CListViewColumn const & CListViewColumn::operator = (LVCOLUMN const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((LVCOLUMN *) this, &Item, sizeof(LVCOLUMN));

	return ThisObject;
	}

// Attributes

UINT CListViewColumn::GetMask(void) const
{
	return mask;
	}

BOOL CListViewColumn::TestMask(UINT uMask) const
{
	return (mask & uMask) ? TRUE : FALSE;
	}

UINT CListViewColumn::GetFormat(void) const
{
	AfxAssert(mask & LVCF_FMT);

	return fmt;
	}

int CListViewColumn::GetWidth(void) const
{
	AfxAssert(mask & LVCF_WIDTH);

	return cx;
	}

PCTXT CListViewColumn::GetText(void) const
{
	AfxAssert(mask & LVCF_TEXT);

	return pszText;
	}

UINT CListViewColumn::GetSubItem(void) const
{
	AfxAssert(mask & LVCF_SUBITEM);

	return iSubItem;
	}

UINT CListViewColumn::GetImage(void) const
{
	AfxAssert(mask & LVCF_IMAGE);

	return iImage;
	}

UINT CListViewColumn::GetOrder(void) const
{
	AfxAssert(mask & LVCF_ORDER);

	return iOrder;
	}

// Operations

void CListViewColumn::SetMask(UINT uMask)
{
	mask = uMask;
	}

void CListViewColumn::SetFormat(UINT uFormat)
{
	mask |= LVCF_FMT;

	fmt = uFormat;
	}

void CListViewColumn::SetWidth(int cx)
{
	mask |= LVCF_WIDTH;

	this->cx = cx;
	}

void CListViewColumn::SetText(PCTXT pText)
{
	mask |= LVCF_TEXT;

	m_Text     = pText;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CListViewColumn::SetText(CString const &Text)
{
	mask |= LVCF_TEXT;

	m_Text     = Text;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CListViewColumn::SetTextBuffer(UINT uCount)
{
	mask |= LVCF_TEXT;

	m_Text     = CString(' ', uCount);

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CListViewColumn::SetSubItem(UINT uSub)
{
	mask |= LVCF_SUBITEM;

	iSubItem = uSub;
	}

void CListViewColumn::SetImage(UINT uImage)
{
	mask |= LVCF_IMAGE;

	iImage = uImage;
	}

void CListViewColumn::SetOrder(UINT uOrder)
{
	mask |= LVCF_ORDER;

	iOrder = uOrder;
	}

// End of File
