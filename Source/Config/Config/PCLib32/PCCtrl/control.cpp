
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Abstract Control Window
//

// Dynamic Class

AfxImplementDynamicClass(CCtrlWnd, CWnd);

// Static Data

DWORD CCtrlWnd::m_dwICC	= 0;

// Constructor

CCtrlWnd::CCtrlWnd(void)
{
	}

// Normal Creation

BOOL CCtrlWnd::Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID)
{
	return CWnd::Create(L"", dwExStyle, dwStyle | WS_CHILD, Rect, hParent, HMENU(uID), NULL);
	}

BOOL CCtrlWnd::Create(DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID)
{
	return CWnd::Create(L"", dwStyle | WS_CHILD, Rect, hParent, HMENU(uID), NULL);
	}

BOOL CCtrlWnd::Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID)
{
	return CWnd::Create(pName, dwExStyle, dwStyle | WS_CHILD, Rect, hParent, HMENU(uID), NULL);
	}

BOOL CCtrlWnd::Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID)
{
	return CWnd::Create(pName, dwStyle | WS_CHILD, Rect, hParent, HMENU(uID), NULL);
	}

// Common Control Management

BOOL CCtrlWnd::IsControlClassLoaded(DWORD dwICC)
{
	return (m_dwICC & dwICC) == dwICC;
	}

BOOL CCtrlWnd::LoadControlClass(DWORD dwICC)
{
	CCriticalGuard Guard;

	if( !IsControlClassLoaded(dwICC) ) {

		INITCOMMONCONTROLSEX icex;

		icex.dwSize = sizeof(icex);
		
		icex.dwICC  = dwICC;
		
		InitCommonControlsEx(&icex);

		m_dwICC |= dwICC;

		return TRUE;
		}

	return FALSE;
	}

// Handle Lookup

CCtrlWnd & CCtrlWnd::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CCtrlWnd NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CCtrlWnd &) CWnd::FromHandle(hWnd, Class);
	}

// Default Class Name

PCTXT CCtrlWnd::GetDefaultClassName(void) const
{
	return CWnd::GetDefaultClassName();
	}

// Default Class Definition

BOOL CCtrlWnd::GetClassDetails(WNDCLASSEX &Class) const
{
	return CWnd::GetClassDetails(Class);
	}

// Implementation

LRESULT CCtrlWnd::SendNotify(UINT uCode)
{
	return GetParent().SendMessage( WM_COMMAND,
					WPARAM(MAKELONG(GetID(), uCode)),
					LPARAM(m_hWnd)
					);
	}

// End of File
