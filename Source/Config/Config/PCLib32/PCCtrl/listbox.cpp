
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// List Box Control
//

// Dynamic Class

AfxImplementDynamicClass(CListBox, CCtrlWnd);

// Constructor

CListBox::CListBox(void)
{
	}

// Attributes

UINT CListBox::GetAnchorIndex(void) const
{
	return UINT(SendMessageConst(LB_GETANCHORINDEX));
	}

UINT CListBox::GetCaretIndex(void) const
{
	return UINT(SendMessageConst(LB_GETCARETINDEX));
	}

UINT CListBox::GetCount(void) const
{
	return UINT(SendMessageConst(LB_GETCOUNT));	
	}

UINT CListBox::GetCurSel(void) const
{
	return UINT(SendMessageConst(LB_GETCURSEL));	
	}

DWORD CListBox::GetCurSelData(void) const
{
	UINT uIndex = UINT(SendMessageConst(LB_GETCURSEL));
	
	if( uIndex == LB_ERR ) return DWORD(-1L);
		
	return SendMessageConst(LB_GETITEMDATA, WPARAM(uIndex));
	}

int CListBox::GetHorizontalExtent(void) const
{
	return int(SendMessageConst(LB_GETHORIZONTALEXTENT));
	}

DWORD CListBox::GetItemData(UINT uIndex) const
{
	return SendMessageConst(LB_GETITEMDATA, WPARAM(uIndex));
	}

int CListBox::GetItemHeight(UINT uIndex) const
{
	return int(SendMessageConst(LB_GETITEMHEIGHT, WPARAM(uIndex)));
	}

CRect CListBox::GetItemRect(UINT uIndex) const
{
	CRect Result;

	SendMessageConst(LB_GETITEMRECT, WPARAM(uIndex), LPARAM(PRECT(&Result)));
	
	return Result;
	}

void CListBox::GetItemRect(UINT uIndex, CRect &Rect) const
{
	SendMessageConst(LB_GETITEMRECT, WPARAM(uIndex), LPARAM(PRECT(&Rect)));
	}

BOOL CListBox::GetSel(UINT uIndex) const
{
	return BOOL(SendMessageConst(LB_GETSEL, WPARAM(uIndex)));
	}

UINT CListBox::GetSelCount(void) const
{
	return UINT(SendMessageConst(LB_GETSELCOUNT));	
	}

UINT CListBox::GetSelItems(UINT uMax, UINT *pList) const
{
	return UINT(SendMessageConst(LB_GETSELITEMS, WPARAM(uMax), LPARAM(pList)));
	}

UINT CListBox::GetSelItems(CArray <UINT> &Array) const
{
	UINT n = GetSelCount();

	Array.Empty();

	Array.Expand(n);

	GetSelItems(n, (UINT *) Array.GetPointer());

	return n;
	}

CString CListBox::GetText(UINT uIndex) const
{
	if( uIndex < NOTHING ) {

		UINT uLength = GetTextLen(uIndex);

		if( uLength < NOTHING ) {
			
			CString Text = CString(' ', uLength);
			
			SendMessageConst(LB_GETTEXT, WPARAM(uIndex), LPARAM(PTXT(PCTXT(Text))));
	
			return Text;
			}
		}

	return L"";
	}

UINT CListBox::GetTextLen(UINT uIndex) const
{
	return UINT(SendMessageConst(LB_GETTEXTLEN, WPARAM(uIndex)));	
	}

UINT CListBox::GetTopIndex(void) const
{
	return UINT(SendMessageConst(LB_GETTOPINDEX));	
	}

UINT CListBox::ItemFromPoint(CPoint const &Pos) const
{
	return UINT(SendMessageConst(LB_ITEMFROMPOINT, 0, LPARAM(Pos)));
	}

// General Operations

UINT CListBox::AddFile(PCTXT pFile)
{
	return UINT(SendMessage(LB_ADDSTRING, 0, LPARAM(pFile)));
	}

UINT CListBox::AddString(PCTXT pString)
{
	return UINT(SendMessage(LB_ADDSTRING, 0, LPARAM(pString)));
	}

UINT CListBox::AddString(PCSTR pString)
{
	return AddString(CString(pString));
	}

UINT CListBox::AddString(PCTXT pString, DWORD dwData)
{
	UINT uIndex = UINT(SendMessage(LB_ADDSTRING, 0, LPARAM(pString)));
	
	SendMessage(LB_SETITEMDATA, WPARAM(uIndex), LPARAM(dwData));
	
	return uIndex;
	}

UINT CListBox::AddString(PCSTR pString, DWORD dwData)
{
	return AddString(CString(pString), dwData);
	}

void CListBox::AddStrings(CStringArray const &Array)
{
	SetRedraw(FALSE);

	for( UINT n = 0; n < Array.GetCount(); n++ ) {

		CString const &Text = Array[n];

		AddString(Text);
		}

	SetRedraw(TRUE);

	Invalidate(FALSE);
	}

void CListBox::DeleteString(UINT uIndex)
{
	SendMessage(LB_DELETESTRING, WPARAM(uIndex));
	}

UINT CListBox::Dir(UINT uAttr, PCTXT pFileSpec)
{
	return UINT(SendMessage(LB_DIR, WPARAM(uAttr), LPARAM(pFileSpec)));
	}

UINT CListBox::FindString(UINT uStart, PCTXT pString)
{
	return UINT(SendMessage(LB_FINDSTRING, WPARAM(uStart), LPARAM(pString)));
	}

UINT CListBox::FindStringExact(UINT uStart, PCTXT pString)
{
	return UINT(SendMessage(LB_FINDSTRINGEXACT, WPARAM(uStart), LPARAM(pString)));
	}

void CListBox::InitStorage(UINT uCount, DWORD dwMemory)
{
	SendMessage(LB_INITSTORAGE, WPARAM(uCount), LPARAM(dwMemory));
	}

UINT CListBox::InsertString(UINT uIndex, PCTXT pString)
{
	return UINT(SendMessage(LB_INSERTSTRING, WPARAM(uIndex), LPARAM(pString)));
	}

UINT CListBox::InsertString(UINT uIndex, PCTXT pString, DWORD dwData)
{
	UINT uInsert = UINT(SendMessage(LB_INSERTSTRING, WPARAM(uIndex), LPARAM(pString)));
	
	SendMessage(LB_SETITEMDATA, WPARAM(uInsert), LPARAM(dwData));
	
	return uInsert;
	}

void CListBox::InsertStrings(UINT uIndex, CStringArray const &Array)
{
	SetRedraw(FALSE);

	for( UINT n = 0; n < Array.GetCount(); n++ ) {

		CString const &Text = Array[n];

		InsertString(uIndex, Text);
		}

	SetRedraw(TRUE);

	Invalidate(FALSE);
	}

void CListBox::ResetContent(void)
{
	SendMessage(LB_RESETCONTENT);	
	}

void CListBox::SelectString(UINT uIndex, PCTXT pString)
{
	SendMessage(LB_SELECTSTRING, WPARAM(uIndex), LPARAM(pString));
	}

void CListBox::SelItemRange(UINT uFirst, UINT uLast, BOOL fSelect)
{
	SendMessage(LB_SELITEMRANGE, WPARAM(fSelect), MAKELONG(uFirst, uLast));
	}

void CListBox::SelItemRange(CRange const &Range, BOOL fSelect)
{
	SendMessage(LB_SELITEMRANGE, WPARAM(fSelect), LPARAM(DWORD(Range)));
	}

void CListBox::SetAnchorIndex(UINT uIndex)
{
	SendMessage(LB_SETANCHORINDEX, WPARAM(uIndex));
	}
	
void CListBox::SetCaretIndex(UINT uIndex, BOOL fScroll)
{
	SendMessage(LB_SETCARETINDEX, WPARAM(uIndex), LPARAM(fScroll));
	}
	
void CListBox::SetColumnWidth(int nWidth)
{
	SendMessage(LB_SETCOLUMNWIDTH, WPARAM(nWidth));
	}

void CListBox::SetCount(UINT uCount)
{
	SendMessage(LB_SETCOUNT, WPARAM(uCount));
	}
	
void CListBox::SetCurSel(UINT uIndex)
{
	SendMessage(LB_SETCURSEL, WPARAM(uIndex));
	}
	
BOOL CListBox::SelectData(DWORD dwData)
{
	UINT uCount = UINT(SendMessageConst(LB_GETCOUNT));
	
	for( UINT uIndex = 0; uIndex < uCount; uIndex++ ) {
	
		DWORD dwTest = SendMessageConst(LB_GETITEMDATA, WPARAM(uIndex));
		
		if( dwTest == dwData ) {
		
			SendMessage(LB_SETCURSEL, WPARAM(uIndex));
			
			return TRUE;
			}
		}
	
	return FALSE;
	}

void CListBox::SetHorizontalExtent(int nExtent)
{
	SendMessage(LB_SETHORIZONTALEXTENT, WPARAM(nExtent));
	}

void CListBox::SetItemData(UINT uIndex, DWORD dwData)
{
	SendMessage(LB_SETITEMDATA, WPARAM(uIndex), LPARAM(dwData));
	}

void CListBox::SetItemHeight(UINT uIndex, int nHeight)
{
	SendMessage(LB_SETITEMHEIGHT, WPARAM(uIndex), WPARAM(nHeight));	
	}

void CListBox::SetSel(UINT uIndex, BOOL fSelect)
{
	SendMessage(LB_SETSEL, WPARAM(fSelect), LPARAM(uIndex));
	}

void CListBox::SetTabStops(UINT uTabs, int const *pTabs)
{
	SendMessage(LB_SETTABSTOPS, WPARAM(uTabs), LPARAM(pTabs));
	}

void CListBox::SetTabStops(CArray <int> const &Array)
{
	SetTabStops(Array.GetCount(), Array.GetPointer());
	}

void CListBox::SetTopIndex(UINT uIndex)
{
	SendMessage(LB_SETTOPINDEX, WPARAM(uIndex));	
	}

// Handle Lookup

CListBox & CListBox::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CListBox NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CListBox &) CWnd::FromHandle(hWnd, Class);
	}

// Default Class Name

PCTXT CListBox::GetDefaultClassName(void) const
{
	return L"LISTBOX";
	}

// End of File
