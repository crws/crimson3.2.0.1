
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// List View Item
//

// Warning Control

#pragma warning(disable: 4458)

// Constructors

CListViewItem::CListViewItem(void)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));
	}

CListViewItem::CListViewItem(CListViewItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((LVITEM *) this, &That, sizeof(LVITEM));

	m_Text = That.m_Text;
	}

CListViewItem::CListViewItem(LVITEM const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((LVITEM *) this, &Item, sizeof(LVITEM));
	}

CListViewItem::CListViewItem(UINT uItem, UINT uSub, CString const &Text, UINT uImage, LPARAM lParam)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem, uSub);

	SetText(Text);

	SetImage(uImage);

	SetParam(lParam);
	}

CListViewItem::CListViewItem(UINT uItem, UINT uSub, PCTXT pText, UINT uImage, LPARAM lParam)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem, uSub);

	SetText(pText);

	SetImage(uImage);

	SetParam(lParam);
	}

CListViewItem::CListViewItem(UINT uItem, CString const &Text, UINT uImage, LPARAM lParam)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem);

	SetText(Text);

	SetImage(uImage);

	SetParam(lParam);
	}

CListViewItem::CListViewItem(UINT uItem, PCTXT pText, UINT uImage, LPARAM lParam)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem);

	SetText(pText);

	SetImage(uImage);

	SetParam(lParam);
	}

CListViewItem::CListViewItem(UINT uItem, UINT uSub, CString const &Text, UINT uImage)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem, uSub);

	SetText(Text);

	SetImage(uImage);
	}

CListViewItem::CListViewItem(UINT uItem, UINT uSub, PCTXT pText, UINT uImage)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem, uSub);

	SetText(pText);

	SetImage(uImage);
	}

CListViewItem::CListViewItem(UINT uItem, CString const &Text, UINT uImage)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem);

	SetText(Text);

	SetImage(uImage);
	}

CListViewItem::CListViewItem(UINT uItem, PCTXT pText, UINT uImage)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetItem(uItem);

	SetText(pText);

	SetImage(uImage);
	}

CListViewItem::CListViewItem(UINT uMask)
{
	memset((LVITEM *) this, 0, sizeof(LVITEM));

	SetMask(uMask);
	}

// Assignment Operators

CListViewItem const & CListViewItem::operator = (CListViewItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((LVITEM *) this, &That, sizeof(LVITEM));

	m_Text = That.m_Text;

	return ThisObject;
	}

CListViewItem const & CListViewItem::operator = (LVITEM const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((LVITEM *) this, &Item, sizeof(LVITEM));

	return ThisObject;
	}

// Attributes

UINT CListViewItem::GetMask(void) const
{
	return mask;
	}

BOOL CListViewItem::TestMask(UINT uMask) const
{
	return (mask & uMask) ? TRUE : FALSE;
	}

UINT CListViewItem::GetItem(void) const
{
	return iItem;
	}

UINT CListViewItem::GetSubItem(void) const
{
	return iSubItem;
	}

LPARAM CListViewItem::GetParam(void) const
{
	AfxAssert(mask & LVIF_PARAM);

	return lParam;
	}

UINT CListViewItem::GetState(void) const
{
	AfxAssert(mask & LVIF_STATE);

	return state;
	}

UINT CListViewItem::GetStateMask(void) const
{
	AfxAssert(mask & LVIF_STATE);

	return stateMask;
	}

PCTXT CListViewItem::GetText(void) const
{
	AfxAssert(mask & LVIF_TEXT);

	return pszText;
	}

UINT CListViewItem::GetImage(void) const
{
	AfxAssert(mask & LVIF_IMAGE);

	return iImage;
	}

UINT CListViewItem::GetIndent(void) const
{
	AfxAssert(mask & LVIF_INDENT);

	return iIndent;
	}

// Operations

void CListViewItem::SetMask(UINT uMask)
{
	mask = uMask;
	}

void CListViewItem::SetItem(UINT uItem)
{
	iItem = uItem;
	}

void CListViewItem::SetItem(UINT uItem, UINT uSub)
{
	iItem    = uItem;

	iSubItem = uSub;
	}

void CListViewItem::SetSubItem(UINT uSub)
{
	iSubItem = uSub;
	}

void CListViewItem::SetParam(LPARAM lParam)
{
	mask |= LVIF_PARAM;

	this->lParam = lParam;
	}

void CListViewItem::SetState(UINT uState)
{
	mask |= LVIF_STATE;

	state = uState;
	}

void CListViewItem::SetStateMask(UINT uMask)
{
	mask |= LVIF_STATE;

	stateMask = uMask;
	}

void CListViewItem::SetText(PCTXT pText)
{
	mask |= LVIF_TEXT;

	m_Text     = pText;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CListViewItem::SetText(CString const &Text)
{
	mask |= LVIF_TEXT;

	m_Text     = Text;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CListViewItem::SetTextBuffer(UINT uCount)
{
	mask |= LVIF_TEXT;

	m_Text     = CString(' ', uCount);

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CListViewItem::SetImage(UINT uImage)
{
	mask |= LVIF_IMAGE;

	iImage = uImage;
	}

void CListViewItem::SetIndent(UINT uIndent)
{
	mask |= LVIF_INDENT;

	iIndent = uIndent;
	}

// End of File
