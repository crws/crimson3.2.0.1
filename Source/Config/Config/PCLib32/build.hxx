
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Class Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Build Version
//

#ifdef _DEBUG

#include "..\..\..\..\Build\Bin\Version\Debug\version.hxx"

#else

#include "..\..\..\..\Build\Bin\Version\Release\version.hxx"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Package Version
//

#define PCLIB_VERSION   	"8.0.0.3"

#define	PCLIB_REG_VER		"8.0"

#define PCLIB_MAJOR     	8

#define PCLIB_MINOR     	0

#define PCLIB_LEVEL     	0

#define PCLIB_BUILD     	C3_BUILD

#define PCLIB_HOTFIX		C3_HOTFIX

#define PCLIB_COMMENT   	C3_COMMENT

#define	PCLIB_COMPANYNAME	C3_COMPANYNAME

#define	PCLIB_COPYRIGHT		C3_COPYRIGHT

// End of File
