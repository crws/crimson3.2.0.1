
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GUID Wrapper Class
//

// Null GUID

GUID const * CGuid::m_pNull = & CLSID_NULL;

// Constructors

CGuid::CGuid(void)
{
	memcpy(this, m_pNull, sizeof(GUID));
	}

CGuid::CGuid(CGuid const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy(this, &That, sizeof(GUID));
	}

CGuid::CGuid(GUID const &Guid)
{
	AfxValidateReadPtr(&Guid, sizeof(Guid));

	memcpy(this, &Guid, sizeof(GUID));
	}

CGuid::CGuid(UINT t, UINT n)
{
	Data1 = (unsigned long) t;

	Data2 = (unsigned short) n;

	Data3 = (unsigned short) 0;

	((DWORD &) Data4[0]) = DWORD('noeA');

	((DWORD &) Data4[4]) = DWORD('DII');
}

CGuid::CGuid(PCTXT pText)
{
	if( !pText ) {

		memcpy(this, m_pNull, sizeof(GUID));

		return;
		}

	AfxValidateStringPtr(pText);

	CLSIDFromString(PTXT(pText), this);
	}

// Assignment

CGuid const & CGuid::operator = (CGuid const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy(this, &That, sizeof(GUID));

	return ThisObject;
	}

CGuid const & CGuid::operator = (GUID const &Guid)
{
	AfxValidateReadPtr(&Guid, sizeof(Guid));

	memcpy(this, &Guid, sizeof(GUID));

	return ThisObject;
	}

CGuid const & CGuid::operator = (PCTXT pText)
{
	if( !pText ) {

		memcpy(this, m_pNull, sizeof(GUID));

		return ThisObject;
		}

	AfxValidateStringPtr(pText);

	CLSIDFromString(PTXT(pText), this);

	return ThisObject;
	}

// Attributes

BOOL CGuid::IsEmpty(void) const
{
	return !memcmp(this, m_pNull, sizeof(GUID));
	}

BOOL CGuid::IsUnknown(void) const
{
	if( !Data1 && !Data2 && !Data3 ) {

		if( ((DWORD &) Data4[0]) == 0x0000000C0 ) {

			if( ((DWORD &) Data4[4]) == 0x000000046 ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CGuid::IsAeon(UINT t, UINT n) const
{
	if( Data1 == t && Data2 == n && Data3 == 0 ) {

		if( ((DWORD &) Data4[0]) == DWORD('noeA') ) {

			if( ((DWORD &) Data4[4]) == DWORD('DII') ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CGuid::operator ! (void) const
{
	return !memcmp(this, m_pNull, sizeof(GUID));
	}

CString CGuid::GetAsText(void) const
{
	LPOLESTR pText = NULL;

	StringFromCLSID(ThisObject, &pText);

	return CDelete(pText, TRUE);
	}

// Operations

void CGuid::CreateUnique(void)
{
	CoCreateGuid(this);
	}

// End of File
