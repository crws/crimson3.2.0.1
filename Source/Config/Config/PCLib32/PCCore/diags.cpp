
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug Only Code
//

#ifdef _DEBUG

//////////////////////////////////////////////////////////////////////////
//
// Diagnostic Status
//

static BOOL fAbort = FALSE;

//////////////////////////////////////////////////////////////////////////
//
// Diagnostic Functions
//

BOOL AfxAssertFailed(PCSTR pFile, UINT uLine)
{
	CCriticalGuard Guard;

	if( !AfxAborting() ) {

		CString Text;

		Text.Printf(L"Assertion failed at line %u of %hs", uLine, pFile);

		AfxTrace(L"ERROR: ");

		AfxTrace(Text);

		AfxTrace(L"\n");

		return AfxFatalExit(Text);
		}

	return FALSE;
	}

BOOL AfxFatalExit(PCTXT pText)
{
	CCriticalGuard Guard;

	if( !AfxAborting() ) {

		CString Text;

		Text += pText;

		Text += L"\n\n";

		Text += L"Do you want to execute a breakpoint and enter the debugger?";

		UINT uFlag = MB_ICONSTOP | MB_YESNOCANCEL | MB_TASKMODAL | MB_SETFOREGROUND;
	
		UINT uCode = MessageBox(NULL, Text, L"APPLICATION ERROR", uFlag);

		switch( uCode ) {
		
			case IDNO:
				return FALSE;

			case IDYES:
				return TRUE;
			}

		AfxAbort();
		}

	return FALSE;
	}

void AfxTraceArgs(PCTXT pText, va_list pArgs)
{
	if( !AfxAborting() ) {

		CCriticalGuard Guard;

		static TCHAR sText[1024];

		vswprintf(sText, pText, pArgs);

		OutputDebugString(sText);

		/*AfxLog(sText);*/
		}
	}

void AfxTrace(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTraceArgs(pText, pArgs);

	va_end(pArgs);
	}

void AfxDump(PCVOID pData, UINT uSize)
{
	PCBYTE p = PCBYTE(pData);

	UINT   s = 0;

	for( UINT n = 0; n < uSize; n++ ) {

		if( n % 0x10 == 0x0 ) {

			AfxTrace(L" %8.8X : %4.4X : ", p + n, n);

			s = n;
			}

		if( true ) {

			AfxTrace(L"%2.2X ", p[n]);
			}

		if( n % 0x10 == 0xF || n == uSize - 1 ) {

			AfxTrace(L" ");

			while( n % 0x10 < 0xF ) {

				AfxTrace(L"   ");

				n++;
				}

			for( UINT i = 0; i <= n - s; i++ ) {

				BYTE b = p[s+i];

				if( b >= 32 && b < 127 ) {

					AfxTrace(L"%c", b);
					}
				else
					AfxTrace(L".");
				}

			AfxTrace(L"\n");
			}
		}
	}

void AfxLog(PCTXT pText)
{
	if( !AfxAborting() ) {

		PCTXT pName = L"pclib32.log";

		TCHAR szBuff[MAX_PATH];
	
		DWORD dw = GetTempPath(elements(szBuff), szBuff);

		if( dw + wstrlen(pName) < MAX_PATH ) {

			wstrcpy(szBuff + dw, pName);
		
			CCriticalGuard Guard;

			int hFile = _wopen( szBuff,
					    O_APPEND | O_CREAT | O_WRONLY | O_BINARY, 
					    0600
					    );

			if( hFile >= 0 ) {

				for( int n = 0; pText[n]; n++ ) {

					if( pText[n] == '\n' ) {

						char sLine[] = { 13, 10 };

						write(hFile, sLine, sizeof(sLine));
						}
					else {
						char sChar[] = { char(pText[n]) };

						write(hFile, sChar, sizeof(sChar));
						}
					}

				close(hFile);
				}
			}
		}

	// cppcheck-suppress resourceLeak
	}

BOOL AfxAborting(void)
{
	return fAbort;
	}

BOOL AfxAbort(void)
{
	CCriticalGuard Guard;

	if( fAbort ) {

		fAbort = TRUE;

		PCTXT pText = L"THE APPLICATION HAS BEEN TERMINATED.";

		FatalAppExit(0, pText);

		return TRUE;
		}

	return FALSE;
	}

// End of File

#endif
