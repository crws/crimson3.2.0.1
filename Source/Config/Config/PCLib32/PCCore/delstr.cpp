
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Deleting String
//

// Constructors

CDelete::CDelete(PTXT pText, BOOL fOLE) : CString(pText)
{
	if( fOLE ) {

		CoTaskMemFree(pText);
		}
	else
		delete pText;
	}

// End of File
