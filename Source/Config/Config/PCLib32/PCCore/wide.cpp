
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Wide String Support
//

INT64 watoi64(PCUTF p)
{
	return _wcstoi64(p, NULL, 10);
	}

int watoi(PCUTF p)
{
	return wcstol(p, NULL, 10);
	}

double watof(PCUTF p)
{
	return wcstod(p, NULL);
	}

WCHAR wtoupper(WCHAR c)
{
	return LOWORD(CharUpper(PTXT(MAKELONG(c, 0))));
	}

WCHAR wtolower(WCHAR c)
{
	return LOWORD(CharLower(PTXT(MAKELONG(c, 0))));
	}

BOOL wisupper(WCHAR c)
{
	return IsCharUpper(c);
	}

BOOL wislower(WCHAR c)
{
	return IsCharLower(c);
	}

BOOL wisalpha(WCHAR c)
{
	return IsCharAlpha(c);
	}

BOOL wisalnum(WCHAR c)
{
	return IsCharAlphaNumeric(c);
	}

BOOL wisspace(WCHAR c)
{
	if( c < 256 && isspace(c) ) {

		return TRUE;
		}
	else {
		WORD t;

		GetStringTypeEx( LOCALE_USER_DEFAULT,
				 CT_CTYPE2,
				 &c,
				 1,
				 &t
				 );

		return t == C2_WHITESPACE;
		}
	}

BOOL wisdigit(WCHAR c)
{
	if( c < 256 && isdigit(c) ) {

		return TRUE;
		}
	else {
		WORD t;

		GetStringTypeEx( LOCALE_USER_DEFAULT,
				 CT_CTYPE2,
				 &c,
				 1,
				 &t
				 );

		return t == C2_EUROPENUMBER;
		}
	}

int wstrlen(PCUTF p)
{
	return wcslen(p);
	}

void wstrcat(PUTF d, PCUTF s)
{
	wcscat(d, s);
	}

void wstrcpy(PUTF d, PCUTF s)
{
	wcscpy(d, s);
	}

void wstrncpy(PUTF d, PCUTF s, INT n)
{
	wcsncpy(d, s, n+1);
	}

int wstrcmp(PCUTF p1, PCUTF p2)
{
	return wcscmp(p1, p2);
	}

int wstricmp(PCUTF p1, PCUTF p2)
{
	return wcsicmp(p1, p2);
	}

int wstrncmp(PCUTF p1, PCUTF p2, INT n)
{
	return wcsncmp(p1, p2, n);
	}

int wstrnicmp(PCUTF p1, PCUTF p2, INT n)
{
	return wcsnicmp(p1, p2, n);
	}

PUTF wstrchr(PCUTF p, WCHAR f)
{
	return PUTF(wcschr(p, f));
	}

PUTF wstrrchr(PCUTF p, WCHAR f)
{
	return PUTF(wcsrchr(p, f));
	}

PUTF wstrstr(PCUTF p, PCUTF f)
{
	return PUTF(wcsstr(p, f));
	}

PUTF wstristr(PCUTF p, PCUTF f)
{
	int n = wstrlen(f);

	while( *p ) {

		if( !wstrnicmp(p, f, n) ) {

			return PUTF(p);
			}

		p++;
		}

	return NULL;
	}

UINT wstrspn(PCUTF p, PCUTF l)
{
	return wcsspn(p, l);
	}

UINT wstrcspn(PCUTF p, PCUTF l)
{
	return wcscspn(p, l);
	}

PUTF wstrdup(PCUTF p)
{
	return wcsdup(p);
	}

PUTF wstrdpi(PCUTF p)
{
	UINT n = wstrlen(p) + 1;

	PUTF c = PUTF(malloc(n * sizeof(TCHAR)));

	AfxAssume(c);

	wstrcpy(c, p);

	while( n-- ) {

		BYTE hi = HIBYTE(c[n]);
		BYTE lo = LOBYTE(c[n]);

		c[n] = MAKEWORD(hi,lo);
		}

	return c;
	}

PUTF wstrdup(PCSTR p)
{
	UINT n = strlen(p) + 1;

	PUTF c = PUTF(malloc(n * sizeof(TCHAR)));

	AfxAssume(c);

	PUTF w = c;

	while( *w++ = BYTE(*p++) );

	return c;
	}

void wstrupr(PUTF p)
{
	CharUpper(p);
	}

void wstrlwr(PUTF p)
{
	CharLower(p);
	}

void wmemset(PVOID d, WCHAR c, UINT n)
{
	if( sizeof(c) == 2 ) {

		BYTE c1 = LOBYTE(c);

		BYTE c2 = HIBYTE(c);

		if( c1 == c2 ) {

			memset(d, c1, n * sizeof(WCHAR));

			return;
			}
		}

	for( PUTF d1 = PUTF(d); n--; *d1++ = c );
	}

void wmemcpy(PVOID d, PCVOID s, UINT n)
{
	memcpy(d, s, n * sizeof(WCHAR));
	}

void wmemmove(PVOID d, PCVOID s, UINT n)
{
	memmove(d, s, n * sizeof(WCHAR));
	}

// End of File
