
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Resource Tag
//

// Constructor

CResTag::CResTag(PCTXT pName, PCTXT pType)
{
	m_Name.SetResourceName(pName);

	m_Type.SetResourceName(pType);
	}

// Comparison Function

int AfxCompare(CResTag const &a, CResTag const &b)
{
	int c1 = AfxCompare(a.m_Name, b.m_Name);

	if( c1 ) return c1;

	int c2 = AfxCompare(a.m_Name, b.m_Name);

	if( c2 ) return c2;

	return 0;
	}

// End of File
