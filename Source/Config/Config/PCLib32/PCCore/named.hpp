
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_NAMED_HPP
	
#define	INCLUDE_NAMED_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE __FILE__

/////////////////////////////////////////////////////////////////////////
//
// Named Pair
//

// cppcheck-suppress copyCtorAndEqOperator

template <typename CName, typename CData> class CNamedPair
{
	public:
		// Constructors
		CNamedPair(void);
		CNamedPair(CName const &Name, CData const &Data);
		CNamedPair(CNamedPair const &That);
		CNamedPair(CName const &Name);

		// Attributes
		CName const & GetName(void) const;
		CData const & GetData(void) const;

		// Operations
		void SetName(CName const &Name);
		void SetData(CData const &Data);

		// Comparison Operators
		int operator == (CNamedPair const &That) const;
		int operator >  (CNamedPair const &That) const;

		// Comparison Function
		friend int AfxCompare(CNamedPair const &a, CNamedPair const &b)
		{
		return AfxCompare(a.m_Name, b.m_Name);
		}

	protected:
		// Data Members
		CName m_Name;
		CData m_Data;
	};

/////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CName, typename CData>

#define	TP2 CNamedPair <CName, CData>

/////////////////////////////////////////////////////////////////////////
//
// Named Pair
//

// Constructors

TP1 TP2::CNamedPair(void)
{
	}

TP1 TP2::CNamedPair(CName const &Name, CData const &Data) : m_Name(Name), m_Data(Data)
{
	}

TP1 TP2::CNamedPair(CNamedPair const &That) : m_Name(That.m_Name), m_Data(That.m_Data)
{
	}

TP1 TP2::CNamedPair(CName const &Name) : m_Name(Name)
{
	}

// Attributes

TP1 CName const & TP2::GetName(void) const
{
	return m_Name;
	}

TP1 CData const & TP2::GetData(void) const
{
	return m_Data;
	}

// Operations

TP1 void TP2::SetName(CName const &Name)
{
	m_Name = Name;
	}

TP1 void TP2::SetData(CData const &Data)
{
	m_Data = Data;
	}

// Comparison Operators

TP1 int TP2::operator == (CNamedPair const &That) const
{
	return m_Name == That.m_Name;
	}

TP1 int TP2::operator > (CNamedPair const &That) const
{
	return m_Name > That.m_Name;
	}

// End of File

#endif
