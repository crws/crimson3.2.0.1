
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LINKED_HPP
	
#define	INCLUDE_LINKED_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE __FILE__

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

template <typename CData> class CListNode;

template <typename CData> class CList;

//////////////////////////////////////////////////////////////////////////
//
// Linked List Node
//

template <typename CData> class CListNode
{
	public:
		// Constructor
		CListNode(CData const &Data);

		// Data Members
		CData	    m_Data;
		CListNode * m_pPrev;
		CListNode * m_pNext;
	};

//////////////////////////////////////////////////////////////////////////
//
// Linked List Collection
//

template <typename CData> class CList
{
	public:
		// Constructors
		CList(void);
		CList(CList const &That);
		
		// Destructor
		~CList(void);

		// Assignment
		CList const & operator = (CList const &That);

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetCount(void) const;
		
		// Lookup Operator
		CData const & operator [] (INDEX Index) const;

		// Data Read
		CData const & GetAt(INDEX Index) const;

		// Core Operations
		void  Empty(void);
		INDEX Append(CData const &Data);
		INDEX Append(CList const &List);
		INDEX Append(CData const *pData, UINT uCount);
		INDEX Insert(INDEX Index, CData const &Data);
		INDEX Insert(INDEX Index, CList const &List);
		INDEX Insert(INDEX Index, CData const *pData, UINT uCount);
		void  Remove(INDEX Index, UINT uCount);
		void  Remove(INDEX Index);

		// Enumeration
		INDEX GetHead(void) const;
		INDEX GetTail(void) const;
		BOOL  GetNext(INDEX &Index) const;
		BOOL  GetPrev(INDEX &Index) const;

		// Searching
		INDEX Find(CData const &Data) const;

		// Failure Checks
		BOOL  Failed(INDEX Index) const;
		INDEX Failed(void) const;

	protected:
		// Type Definitions
		typedef CListNode <CData> CNode;
			
		// Data Members
		UINT    m_uCount;
		CNode * m_pHead;
		CNode * m_pTail;
	};

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CData>

#define	TP2 CListNode <CData>

//////////////////////////////////////////////////////////////////////////
//
// Linked List Node
//

// Constructor

TP1 TP2::CListNode(CData const &Data) : m_Data(Data)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CData>

#define	TP2 CList <CData>

/////////////////////////////////////////////////////////////////////////
//
// Linked List Collection
//

// Constructors

TP1 TP2::CList(void)
{
	m_uCount = 0;

	m_pHead  = NULL;

	m_pTail  = NULL;
	}

TP1 TP2::CList(CList const &That)
{
	m_uCount = 0;

	m_pHead  = NULL;

	m_pTail  = NULL;

	Append(That);
	}

// Destructor

TP1 TP2::~CList(void)
{
	Empty();
	}

// Assignment

// cppcheck-suppress operatorEqVarError

TP1 TP2 const & TP2::operator = (CList const &That)
{
	Empty();
	
	Append(That);

	return ThisObject;
	}

// Attributes

TP1 BOOL TP2::IsEmpty(void) const
{
	return !m_uCount;
	}

TP1 BOOL TP2::operator ! (void) const
{
	return !m_uCount;
	}

TP1 UINT TP2::GetCount(void) const
{
	return m_uCount;
	}

// Lookup Operator

TP1 CData const & TP2::operator [] (INDEX Index) const
{
	CNode *pNode = (CNode *) Index;

	AfxValidateReadPtr(pNode, sizeof(CNode));

	return pNode->m_Data;
	}

// Read Read

TP1 CData const & TP2::GetAt(INDEX Index) const
{
	CNode *pNode = (CNode *) Index;

	AfxValidateReadPtr(pNode, sizeof(CNode));

	return pNode->m_Data;
	}

// Core Operations

TP1 void TP2::Empty(void)
{
	while( m_pHead ) {

		CNode *pNode = m_pHead;

		m_pHead = m_pHead->m_pNext;

		delete pNode;
		}

	m_uCount = 0;

	m_pHead  = NULL;

	m_pTail  = NULL;
	}

TP1 INDEX TP2::Append(CData const &Data)
{
	CNode *pNode = tp_new CNode(Data);

	AfxListAppend(m_pHead, m_pTail, pNode, m_pNext, m_pPrev);

	m_uCount++;

	return INDEX(pNode);
	}

TP1 INDEX TP2::Append(CList const &List)
{
	CNode *pInit = NULL;

	CNode *pNode = List.m_pHead;

	while( pNode ) {

		CNode *pCopy = tp_new CNode(pNode->m_Data);

		AfxListAppend(m_pHead, m_pTail, pCopy, m_pNext, m_pPrev);

		if( !pInit ) {

			pInit = pCopy;
			}

		pNode = pNode->m_pNext;
		}

	m_uCount += List.m_uCount;

	return INDEX(pInit);
	}

TP1 INDEX TP2::Append(CData const *pData, UINT uCount)
{
	CNode *pInit = NULL;

	while( uCount-- ) {

		CNode *pCopy = tp_new CNode(pData->m_Data);

		AfxListAppend(m_pHead, m_pTail, pCopy, m_pNext, m_pPrev);

		if( !pInit ) {

			pInit = pCopy;
			}

		m_uCount++;

		pData++;
		}

	return INDEX(pInit);
	}

TP1 INDEX TP2::Insert(INDEX Index, CData const &Data)
{
	CNode *pSlot = (CNode *) Index;

	CNode *pNode = tp_new CNode(Data);

	AfxListInsert(m_pHead, m_pTail, pNode, m_pNext, m_pPrev, pSlot);

	m_uCount++;

	return INDEX(pNode);
	}

TP1 INDEX TP2::Insert(INDEX Index, CList const &List)
{
	CNode *pInit = NULL;

	CNode *pSlot = (CNode *) Index;

	CNode *pNode = List.m_pTail;

	while( pNode ) {

		CNode *pCopy = tp_new CNode(pNode->m_Data);

		AfxListInsert(m_pHead, m_pTail, pCopy, m_pNext, m_pPrev, pSlot);

		if( !pInit ) {

			pInit = pCopy;
			}

		pNode = pNode->m_pPrev;
		}

	m_uCount += List.m_uCount;

	return pInit;
	}

TP1 INDEX TP2::Insert(INDEX Index, CData const *pData, UINT uCount)
{
	CNode *pInit = NULL;

	CNode *pSlot = (CNode *) Index;

	while( uCount-- ) {

		CNode *pCopy = tp_new CNode(pData->m_Data);

		AfxListInsert(m_pHead, m_pTail, pCopy, m_pNext, m_pPrev, pSlot);

		if( !pInit ) {

			pInit = pCopy;
			}

		m_uCount++;

		pData++;
		}

	return pInit;
	}

TP1 void TP2::Remove(INDEX Index, UINT uCount)
{
	CNode *pNode = (CNode *) Index;

	for( UINT n = 0; n < uCount; n++ ) {

		CNode *pNext = pNode->m_pNext;

		AfxValidateReadPtr(pNode, sizeof(CNode));

		AfxListRemove(m_pHead, m_pTail, pNode, m_pNext, m_pPrev);

		delete pNode;

		pNode = pNext;
		}

	m_uCount -= uCount;
	}

TP1 void TP2::Remove(INDEX Index)
{
	CNode *pNode = (CNode *) Index;

	AfxValidateReadPtr(pNode, sizeof(CNode));

	AfxListRemove(m_pHead, m_pTail, pNode, m_pNext, m_pPrev);

	delete pNode;

	m_uCount--;
	}

// Enumeration

TP1 INDEX TP2::GetHead(void) const
{
	return INDEX(m_pHead);
	}

TP1 INDEX TP2::GetTail(void) const
{
	return INDEX(m_pTail);
	}

TP1 BOOL TP2::GetNext(INDEX &Index) const
{
	CNode * &pNode = (CNode * &) Index;

	if( pNode ) pNode = pNode->m_pNext;

	return pNode ? TRUE : FALSE;
	}

TP1 BOOL TP2::GetPrev(INDEX &Index) const
{
	CNode * &pNode = (CNode * &) Index;

	if( pNode ) pNode = pNode->m_pPrev;

	return pNode ? TRUE : FALSE;
	}

// Searching

TP1 INDEX TP2::Find(CData const &Data) const
{
	CNode *pNode = m_pHead;

	while( pNode ) {

		if( pNode->m_Data == Data ) {

			return INDEX(pNode);
			}

		pNode = pNode->m_pNext;
		}

	return Failed();
	}

// Failure Checks

TP1 BOOL TP2::Failed(INDEX Index) const
{
	return Index == Failed();
	}

TP1 INDEX TP2::Failed(void) const
{
	return NULL;
	}

// End of File

#endif
