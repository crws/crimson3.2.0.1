
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// String Normalization Object
//

// Attributes

BOOL CStringNormalizer::IsNormal(CString const &Text)
{
	UINT n;

	for( n = 0; Text[n]; n++ ) {

		if( wisalpha(Text[n]) || wisspace(Text[n]) ) {

			continue;
			}

		return FALSE;
		}

	return n > 0;
	}

// Operations

BOOL CStringNormalizer::Normalize(CString &Text, CStringNormData &Data, UINT Norm)
{
	if( Text.GetLength() ) {

		if( Norm >= normStrip ) {

			if( Data.m_Norm < normStrip ) {

				UINT n = Text.GetLength();

				UINT i = 0;

				UINT j = n - 1;

				while( i <= j && IsExtra(Text[i]) ) {

					i++;
					}

				while( i <= j && IsExtra(Text[j]) ) {

					j--;
					}

				if( i < j ) {

					Data.m_Head = Text.Left(i);

					Data.m_Tail = Text.Mid (j+1);

					Text.Delete(j+1, n-1-j);

					Text.Delete(0, i);
					}

				Data.m_Norm = normStrip;
				}
			}
		}

	if( Text.GetLength() ) {

		if( Norm >= normCase ) {

			if( Data.m_Norm < normCase ) {

				Data.m_Case = caseNormal;

				if( !Data.m_Case && IsAllUpper(Text) ) {

					Data.m_Case = caseUpper;

					Text.MakeLower();
					}

				if( !Data.m_Case && HasInitial(Text) ) {

					Data.m_Case = caseInitial;

					Text.MakeLower();
					}

				if( !Data.m_Case && IsTitleCase(Text) ) {

					Data.m_Case = caseTitle;

					Text.MakeLower();
					}

				Data.m_Norm = normCase;
				}
			}
		}

	return Text.GetLength();
	}

void CStringNormalizer::Denormalize(CString &Text, CStringNormData const &Data)
{
	if( !Text.IsEmpty() ) {

		if( Data.m_Norm >= normCase ) {

			if( Data.m_Case == caseTitle ) {

				BOOL fInit = TRUE;

				for( UINT n = 0; Text[n]; n++ ) {

					if( fInit ) {

						Text.SetAt(n, wtoupper(Text[n]));

						fInit = FALSE;

						continue;
						}

					if( wisspace(Text[n]) || Text[n] == '\'' ) {

						fInit = TRUE;
						}
					}
				}

			if( Data.m_Case == caseUpper ) {

				Text.MakeUpper();
				}

			if( Data.m_Case == caseInitial ) {

				Text.SetAt(0, wtoupper(Text[0]));
				}
			}
		}

	if( Data.m_Norm >= normStrip ) {

		Text = Text + Data.m_Tail;

		Text = Data.m_Head + Text;
		}
	}

BOOL CStringNormalizer::Conormalize(CString &Text, CStringNormData const &Data)
{
	if( Data.m_Norm >= normStrip ) {

		CString         Text2 = Text;

		CStringNormData Data2;

		if( Normalize(Text2, Data2, normStrip) ) {

			if( Data2.m_Head == Data.m_Head ) {

				if( Data2.m_Tail == Data.m_Tail ) {

					Text = Text2;

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

BOOL CStringNormalizer::IsExtra(WCHAR c)
{
	return !wisalpha(c);
	}

BOOL CStringNormalizer::IsAllUpper(CString const &Text)
{
	for( UINT n = 0; Text[n]; n++ ) {

		if( wisalpha(Text[n]) ) {

			if( wislower(Text[n]) ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CStringNormalizer::HasInitial(CString const &Text)
{
	if( Text.GetLength() > 1 ) {

		if( wisupper(Text[0]) ) {

			for( UINT n = 1; Text[n]; n++ ) {

				if( wisalpha(Text[n]) ) {

					if( wisupper(Text[n]) ) {

						return FALSE;
						}
					}
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CStringNormalizer::IsTitleCase(CString const &Text)
{
	BOOL fInit = TRUE;

	for( UINT n = 0; Text[n]; n++ ) {

		if( wisalpha(Text[n]) ) {

			if( fInit ) {

				if( wislower(Text[n]) ) {

					return FALSE;
					}
				}
			else {
				if( wisupper(Text[n]) ) {

					return FALSE;
					}
				}

			fInit = FALSE;

			continue;
			}

		if( wisspace(Text[n]) ) {

			fInit = TRUE;
			}
		}

	return TRUE;
	}

// End of File
