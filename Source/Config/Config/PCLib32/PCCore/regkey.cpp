
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Registry Key
//

// Constructors

CRegKey::CRegKey(void)
{
	m_hKey = NULL;
}

CRegKey::CRegKey(CRegKey const &That)
{
	InitFrom(That.m_hKey, TRUE);
}

CRegKey::CRegKey(HKEY hKey, BOOL fAdd)
{
	InitFrom(hKey, fAdd);
}

CRegKey::CRegKey(HKEY hKey)
{
	InitFrom(hKey, TRUE);
}

CRegKey::CRegKey(HKEY hKey, UINT uType)
{
	if( uType == 64 )
		InitFrom64(hKey, TRUE);
	else
		InitFrom(hKey, TRUE);
}

// Destructor

CRegKey::~CRegKey(void)
{
	CloseKey();
}

// Assignment Operators

CRegKey const & CRegKey::operator = (CRegKey const &That)
{
	CloseKey();

	InitFrom(That.m_hKey, TRUE);

	return ThisObject;
}

CRegKey const & CRegKey::operator = (HKEY hKey)
{
	CloseKey();

	InitFrom(hKey, FALSE);

	return ThisObject;
}

// Conversion

CRegKey::operator HKEY (void) const
{
	return m_hKey;
}

// Attributes

HKEY CRegKey::GetHandle(void) const
{
	return m_hKey;
}

BOOL CRegKey::IsValid(void) const
{
	return m_hKey ? TRUE : FALSE;
}

BOOL CRegKey::IsNull(void) const
{
	return m_hKey ? FALSE : TRUE;
}

BOOL CRegKey::operator ! (void) const
{
	return m_hKey ? FALSE : TRUE;
}

BOOL CRegKey::IsReadOnly(void) const
{
	if( m_hKey ) {

		HKEY hKey = NULL;

		LONG r = RegOpenKeyEx(m_hKey,
				      NULL,
				      0,
				      KEY_ALL_ACCESS,
				      &hKey
		);

		if( r == ERROR_SUCCESS ) {

			RegCloseKey(hKey);

			return FALSE;
		}

		return r == ERROR_ACCESS_DENIED;
	}

	return TRUE;
}

// Key Operations

CRegKey CRegKey::Open(PCTXT pName)
{
	if( m_hKey ) {

		AfxValidateStringPtr(pName);

		HKEY hKey = OpenKey(m_hKey, pName);

		return CRegKey(hKey, FALSE);
	}

	return CRegKey();
}

CRegKey CRegKey::Create(PCTXT pName)
{
	if( m_hKey ) {

		AfxValidateStringPtr(pName);

		HKEY hKey = CreateKey(m_hKey, pName);

		return CRegKey(hKey, FALSE);
	}

	return CRegKey();
}

BOOL CRegKey::MoveTo(PCTXT pName)
{
	if( m_hKey ) {

		AfxValidateStringPtr(pName);

		HKEY hKey = CreateKey(m_hKey, pName);

		if( hKey ) {

			CloseKey();

			m_hKey = hKey;

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CRegKey::Delete(PCTXT pName)
{
	if( m_hKey ) {

		AfxValidateStringPtr(pName);

		CRegKey SubKey = Open(pName);

		CStringArray List;

		UINT n = SubKey.GetSubkeyNames(List);

		while( n-- ) {

			if( !SubKey.Delete(List[n]) ) {

				return FALSE;
			}
		}

		SubKey.CloseKey();

		LONG r = RegDeleteKey(m_hKey,
				      pName
		);

		return r == ERROR_SUCCESS;
	}

	return FALSE;
}

BOOL CRegKey::Flush(void)
{
	if( m_hKey ) {

		LONG r = RegFlushKey(m_hKey);

		return r == ERROR_SUCCESS;
	}

	return FALSE;
}

// Map Operations

BOOL CRegKey::WriteMap(CKeyMap const &Map)
{
	for( INDEX i = Map.GetHead(); !Map.Failed(i); Map.GetNext(i) ) {

		CKeyName const &Name  = Map.GetName(i);

		CString  const &Value = Map.GetData(i);

		if( Name.HasKey() ) {

			CRegKey Key = ThisObject;

			if( !Key.MoveTo(Name.GetKey()) ) {

				return FALSE;
			}

			if( !Key.SetValue(Name.GetName(), Value) ) {

				return FALSE;
			}

			continue;
		}

		if( !SetValue(Name, Value) ) {

			return FALSE;
		}
	}

	return TRUE;
}

// Value Attributes

DWORD CRegKey::GetValueType(PCTXT pName) const
{
	if( m_hKey ) {

		DWORD dwType = 0;

		RegQueryValueEx(m_hKey,
				pName,
				NULL,
				&dwType,
				NULL,
				NULL
		);

		return dwType;
	}

	return 0;
}

DWORD CRegKey::GetValueSize(PCTXT pName) const
{
	if( m_hKey ) {

		DWORD dwSize = 0;

		RegQueryValueEx(m_hKey,
				pName,
				NULL,
				NULL,
				NULL,
				&dwSize
		);

		return dwSize;
	}

	return 0;
}

CString CRegKey::GetValueAsString(PCTXT pName) const
{
	if( m_hKey ) {

		DWORD dwType = GetValueType(pName);

		DWORD dwSize = GetValueSize(pName);

		if( dwType == REG_SZ || dwType == REG_EXPAND_SZ ) {

			CString d(' ', dwSize++);

			PBYTE pData = PBYTE(PTXT(PCTXT(d)));

			RegQueryValueEx(m_hKey,
					pName,
					NULL,
					NULL,
					pData,
					&dwSize
			);

			if( dwType == REG_EXPAND_SZ ) {

				UINT n = dwSize + d.Count('%') * 256;

				PTXT p = New TCHAR[n];

				ExpandEnvironmentStrings(d, p, n);

				return CDelete(p, FALSE);
			}

			d.FixLength();

			return d;
		}

		return L"";
	}

	return L"";
}

UINT CRegKey::GetValueAsBlob(PCTXT pName, PVOID pBlob, UINT nBytes) const
{
	if( m_hKey ) {

		AfxValidateWritePtr(pBlob, nBytes);

		DWORD dwType = GetValueType(pName);

		DWORD dwSize = GetValueSize(pName);

		if( dwType == REG_BINARY && dwSize <= nBytes ) {

			PBYTE pData = PBYTE(pBlob);

			RegQueryValueEx(m_hKey,
					pName,
					NULL,
					NULL,
					pData,
					&dwSize
			);

			return UINT(dwSize);
		}

		return 0;
	}

	return 0;
}

BOOL CRegKey::GetValueAsBlob(PCTXT pName, CByteArray &Data) const
{
	if( m_hKey ) {

		DWORD dwType = GetValueType(pName);

		DWORD dwSize = GetValueSize(pName);

		Data.Empty();

		Data.SetCount(dwSize);

		if( dwType == REG_BINARY ) {

			PBYTE pData = PBYTE(Data.GetPointer());

			RegQueryValueEx(m_hKey,
					pName,
					NULL,
					NULL,
					pData,
					&dwSize
			);

			return TRUE;
		}

		return 0;
	}

	return 0;
}

DWORD CRegKey::GetValueAsDword(PCTXT pName) const
{
	if( m_hKey ) {

		DWORD dwType = GetValueType(pName);

		DWORD dwSize = GetValueSize(pName);

		if( dwType == REG_DWORD && dwSize == sizeof(DWORD) ) {

			DWORD d = 0;

			PBYTE pData = PBYTE(&d);

			RegQueryValueEx(m_hKey,
					pName,
					NULL,
					NULL,
					pData,
					&dwSize
			);

			return d;
		}

		return 0;
	}

	return 0;
}

// Value Attributes with Defaults

UINT CRegKey::GetValue(PCTXT pName, UINT uDefault) const
{
	DWORD dwType = GetValueType(pName);

	if( dwType == REG_DWORD ) {

		UINT uValue = GetValueAsDword(pName);

		return uValue;
	}

	return uDefault;
}

CString CRegKey::GetValue(PCTXT pName, PCTXT pDefault) const
{
	DWORD dwType = GetValueType(pName);

	if( dwType == REG_SZ || dwType == REG_EXPAND_SZ ) {

		CString Value = GetValueAsString(pName);

		return Value;
	}

	return pDefault;
}

// Value Operations

BOOL CRegKey::DeleteValue(PCTXT pName)
{
	if( m_hKey ) {

		LONG r = RegDeleteValue(m_hKey,
					pName
		);

		return r == ERROR_SUCCESS;
	}

	return FALSE;
}

BOOL CRegKey::SetValue(PCTXT pName, PCTXT pText)
{
	if( m_hKey ) {

		AfxValidateStringPtr(pText);

		PCBYTE pData  = (PCBYTE) pText;

		UINT   uSize  = sizeof(TCHAR) * (wstrlen(pText) + 1);

		DWORD  dwType = REG_SZ;

		LONG r = RegSetValueEx(m_hKey,
				       pName,
				       NULL,
				       dwType,
				       pData,
				       uSize
		);

		return r == ERROR_SUCCESS;
	}

	return FALSE;
}

BOOL CRegKey::SetValue(PCTXT pName, PCTXT pText, BOOL fExpand)
{
	if( m_hKey ) {

		AfxValidateStringPtr(pText);

		PCBYTE pData  = (PCBYTE) pText;

		UINT   uSize  = sizeof(TCHAR) * (wstrlen(pText) + 1);

		DWORD  dwType = fExpand ? REG_EXPAND_SZ : REG_SZ;

		LONG r = RegSetValueEx(m_hKey,
				       pName,
				       NULL,
				       dwType,
				       pData,
				       uSize
		);

		return r == ERROR_SUCCESS;
	}

	return FALSE;
}

BOOL CRegKey::SetValue(PCTXT pName, PVOID pBlob, UINT nBytes)
{
	if( m_hKey ) {

		AfxValidateReadPtr(pBlob, nBytes);

		PCBYTE pData  = (PCBYTE) pBlob;

		UINT   uSize  = nBytes;

		DWORD  dwType = REG_BINARY;

		LONG r = RegSetValueEx(m_hKey,
				       pName,
				       NULL,
				       dwType,
				       pData,
				       uSize
		);

		return r == ERROR_SUCCESS;
	}

	return FALSE;
}

BOOL CRegKey::SetValue(PCTXT pName, DWORD dwData)
{
	if( m_hKey ) {

		PCBYTE pData  = (PCBYTE) &dwData;

		UINT   uSize  = sizeof(dwData);

		DWORD  dwType = REG_DWORD;

		LONG r = RegSetValueEx(m_hKey,
				       pName,
				       NULL,
				       dwType,
				       pData,
				       uSize
		);

		return r == ERROR_SUCCESS;
	}

	return FALSE;
}


// Prorated Values

UINT CRegKey::GetValue(PCTXT pName, UINT uData, UINT uLimit) const
{
	uData  = GetValue(pName, uData * 1000 / uLimit);

	uData *= uLimit;

	uData /= 1000;

	return uData;
}

BOOL CRegKey::SetValue(PCTXT pName, UINT uData, UINT uLimit)
{
	return SetValue(pName, uData * 1000 / uLimit);
}

// Enumeration

CString CRegKey::GetSubkeyName(UINT n) const
{
	if( m_hKey ) {

		DWORD dwSize = 512;

		PTXT  pData  = New TCHAR[dwSize];

		pData[0] = 0;

		LONG r = RegEnumKeyEx(m_hKey,
				      n,
				      pData,
				      &dwSize,
				      NULL,
				      NULL,
				      NULL,
				      NULL
		);

		if( r == ERROR_SUCCESS ) {

			return CDelete(pData, FALSE);
		}

		delete[] pData;
	}

	return NullSubkeyName();
}

CString CRegKey::GetValueName(UINT n) const
{
	if( m_hKey ) {

		DWORD dwSize = 512;

		PTXT  pData  = New TCHAR[dwSize];

		pData[0] = 0;

		LONG r = RegEnumValue(m_hKey,
				      n,
				      pData,
				      &dwSize,
				      NULL,
				      NULL,
				      NULL,
				      NULL
		);

		if( r == ERROR_SUCCESS ) {

			return CDelete(pData, FALSE);
		}

		delete[] pData;
	}

	return NullValueName();
}

UINT CRegKey::GetSubkeyNames(CStringArray &List) const
{
	UINT n;

	for( n = 0;; n++ ) {

		CString s = GetSubkeyName(n);

		if( !(s == NullSubkeyName()) ) {

			List.Append(s);

			continue;
		}

		break;
	}

	return n;
}

UINT CRegKey::GetValueNames(CStringArray &List) const
{
	UINT n;

	for( n = 0;; n++ ) {

		CString s = GetValueName(n);

		if( !(s == NullValueName()) ) {

			List.Append(s);

			continue;
		}

		break;
	}

	return n;
}

CString CRegKey::NullSubkeyName(void) const
{
	return L"";
}

CString CRegKey::NullValueName(void) const
{
	return L"!!!!";
}

// Implementation

void CRegKey::InitFrom(HKEY hKey, BOOL fAdd)
{
	if( fAdd && hKey ) {

		m_hKey = OpenKey(hKey, NULL);

		return;
	}

	m_hKey = hKey;
}

void CRegKey::InitFrom64(HKEY hKey, BOOL fAdd)
{
	if( fAdd && hKey ) {

		m_hKey = OpenKey64(hKey, NULL);

		return;
	}

	m_hKey = hKey;
}

void CRegKey::CloseKey(void)
{
	if( m_hKey ) {

		RegCloseKey(m_hKey);

		m_hKey = NULL;
	}
}

// Helpers

HKEY CRegKey::CreateKey(HKEY hKey, PCTXT pName)
{
	for( int n = 0; n < 2; n++ ) {

		HKEY hNew = NULL;

		LONG r = RegCreateKeyEx(hKey,
					pName,
					NULL,
					NULL,
					REG_OPTION_NON_VOLATILE,
					n ? KEY_READ : KEY_ALL_ACCESS,
					NULL,
					&hNew,
					NULL
		);

		if( r == ERROR_SUCCESS ) {

			return hNew;
		}
		else {
			if( r == ERROR_ACCESS_DENIED ) {

				if( n ) {

					return NULL;
				}

				continue;
			}
		}

		return NULL;
	}

	return NULL;
}

HKEY CRegKey::OpenKey(HKEY hKey, PCTXT pName)
{
	for( int n = 0; n < 2; n++ ) {

		HKEY hNew = NULL;

		LONG r = RegOpenKeyEx(hKey,
				      pName,
				      0,
				      n ? KEY_READ : KEY_ALL_ACCESS,
				      &hNew
		);

		if( r == ERROR_SUCCESS ) {

			return hNew;
		}
		else {
			if( r == ERROR_ACCESS_DENIED ) {

				if( n ) {

					return NULL;
				}

				continue;
			}
		}

		return NULL;
	}

	return NULL;
}

HKEY CRegKey::OpenKey64(HKEY hKey, PCTXT pName)
{
	for( int n = 0; n < 2; n++ ) {

		HKEY hNew = NULL;

		LONG r = RegOpenKeyEx(hKey,
				      pName,
				      0,
				      n ? KEY_READ | KEY_WOW64_64KEY : KEY_ALL_ACCESS | KEY_WOW64_64KEY,
				      &hNew
		);

		if( r == ERROR_SUCCESS ) {

			return hNew;
		}
		else {
			if( r == ERROR_ACCESS_DENIED ) {

				if( n ) {

					return NULL;
				}

				continue;
			}
		}

		return NULL;
	}

	return NULL;
}

// End of File
