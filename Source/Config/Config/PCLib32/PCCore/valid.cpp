
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug Only Code
//

#ifdef _DEBUG

//////////////////////////////////////////////////////////////////////////
//
// Pointer Validation
//

void AfxValidateReadPtr(void const *pData, UINT uSize)
{
	if( IsBadReadPtr(pData, uSize) ) {

		CString Text;

		Text.Printf(L"Invalid Read Pointer %p", pData);

		AfxTrace(L"ERROR: ");

		AfxTrace(Text);

		AfxTrace(L"\n");

		if( AfxFatalExit(Text) ) {

			BREAKPOINT;
			}
		}
	}
	
void AfxValidateWritePtr(void *pData, UINT uSize)
{
	if( IsBadWritePtr(pData, uSize) ) {

		CString Text;

		Text.Printf(L"Invalid Write Pointer %p", pData);

		AfxTrace(L"ERROR: ");

		AfxTrace(Text);

		AfxTrace(L"\n");

		if( AfxFatalExit(Text) ) {

			BREAKPOINT;
			}
		}
	}

void AfxValidateStringPtr(PCTXT pText, UINT uSize)
{
	if( IsBadStringPtrW(pText, uSize) ) {

		CString Text;

		Text.Printf(L"Invalid String Pointer %p", pText);

		AfxTrace(L"ERROR: ");

		AfxTrace(Text);

		AfxTrace(L"\n");

		if( AfxFatalExit(Text) ) {

			BREAKPOINT;
			}
		}
	}

void AfxValidateStringPtr(PCSTR pText, UINT uSize)
{
	if( IsBadStringPtrA(pText, uSize) ) {

		CString Text;

		Text.Printf(L"Invalid String Pointer %p", pText);

		AfxTrace(L"ERROR: ");

		AfxTrace(Text);

		AfxTrace(L"\n");

		if( AfxFatalExit(Text) ) {

			BREAKPOINT;
			}
		}
	}

void AfxValidateResourceName(PCTXT pText, UINT uSize)
{
	if( (HIWORD(pText) || !pText) && IsBadStringPtr(pText, uSize) ) {

		CString Text;

		Text.Printf(L"Invalid Resource Name %p", pText);

		AfxTrace(L"ERROR: ");

		AfxTrace(Text);

		AfxTrace(L"\n");

		if( AfxFatalExit(Text) ) {

			BREAKPOINT;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Object Validation
//

void AfxValidateObject(CObject const *pObject)
{
	pObject->AssertValid();
	}

void AfxValidateObject(CObject const &Object)
{
	Object.AssertValid();
	}

// End of File

#endif
