
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MAP_HPP
	
#define	INCLUDE_MAP_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE __FILE__

/////////////////////////////////////////////////////////////////////////
//
// Map Collection
//

template <typename CName, typename CData> class CMap
{
	public:
		// Constructors
		CMap(void);
		CMap(CMap const &That);

		// Destructor
		~CMap(void);

		// Assignment
		CMap const & operator = (CMap const &That);

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetCount(void) const;

		// Lookup Operators
		CData const & operator [] (CName const &Name) const;
		CData const & operator [] (INDEX Index) const;

		// Data Read
		CName const & GetName(INDEX Index) const;
		CData const & GetData(INDEX Index) const;

		// Data Write
		void SetData(INDEX Index, CData const &Data);

		// Core Operations
		void Empty(void);
		BOOL Insert(CName const &Name, CData const &Data);
		void Insert(CMap const &Map);
		BOOL Remove(CName const &Name);
		void Remove(INDEX Index);

		// Enumeration
		INDEX GetHead(void) const;
		INDEX GetTail(void) const;
		BOOL  GetNext(INDEX &Index) const;
		BOOL  GetPrev(INDEX &Index) const;

		// Searching
		INDEX FindName(CName const &Name) const;
		INDEX FindData(CData const &Data) const;

		// Failure Checks
		BOOL  Failed(INDEX Index) const;
		INDEX Failed(void) const;

	protected:
		// Type Definitions
		typedef CNamedPair <CName, CData> CPair;
		typedef CTree      <CPair>        COurTree;

		// Data Members
		COurTree m_Tree;
	};

/////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CName, typename CData>

#define	TP2 CMap <CName, CData>

/////////////////////////////////////////////////////////////////////////
//
// Map Collection
//

// Constructors

TP1 TP2::CMap(void)
{
	}

TP1 TP2::CMap(CMap const &That) : m_Tree(That.m_Tree)
{
	}

// Destructor

TP1 TP2::~CMap(void)
{
	}

// Assignment

TP1 TP2 const & TP2::operator = (CMap const &That)
{
	m_Tree = That.m_Tree;

	return ThisObject;
	}

// Attributes

TP1 BOOL TP2::IsEmpty(void) const
{
	return m_Tree.IsEmpty();
	}

TP1 BOOL TP2::operator ! (void) const
{
	return !m_Tree;
	}

TP1 UINT TP2::GetCount(void) const
{
	return m_Tree.GetCount();
	}

// Lookup Operator

TP1 CData const & TP2::operator [] (CName const &Name) const
{
	INDEX Index = m_Tree.Find(CPair(Name));

	AfxAssert(!Failed(Index));

	return m_Tree[Index].GetData();
	}

TP1 CData const & TP2::operator [] (INDEX Index) const
{
	return m_Tree[Index].GetData();
	}

// Data Read

TP1 CName const & TP2::GetName(INDEX Index) const
{
	return m_Tree[Index].GetName();
	}

TP1 CData const & TP2::GetData(INDEX Index) const
{
	return m_Tree[Index].GetData();
	}

// Data Write

TP1 void TP2::SetData(INDEX Index, CData const &Data)
{
	((CPair &) m_Tree[Index]).SetData(Data);
	}

// Core Operations

TP1 void TP2::Empty(void)
{
	m_Tree.Empty();
	}

TP1 BOOL TP2::Insert(CName const &Name, CData const &Data)
{
	return m_Tree.Insert(CPair(Name, Data));
	}

TP1 void TP2::Insert(CMap const &Map)
{
	m_Tree.Insert(Map.m_Tree);
	}

TP1 BOOL TP2::Remove(CName const &Name)
{
	return m_Tree.Remove(CPair(Name));
	}

TP1 void TP2::Remove(INDEX Index)
{
	m_Tree.Remove(Index);
	}

// Enumeration

TP1 INDEX TP2::GetHead(void) const
{
	return m_Tree.GetHead();
	}

TP1 INDEX TP2::GetTail(void) const
{
	return m_Tree.GetTail();
	}

TP1 BOOL TP2::GetNext(INDEX &Index) const
{
	return m_Tree.GetNext(Index);
	}

TP1 BOOL TP2::GetPrev(INDEX &Index) const
{
	return m_Tree.GetPrev(Index);
	}

// Searching

TP1 INDEX TP2::FindName(CName const &Name) const
{
	return m_Tree.Find(CPair(Name));
	}

TP1 INDEX TP2::FindData(CData const &Data) const
{
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		if( !(m_Tree[i].GetData() == Data) ) {

			continue;
			}

		return i;
		}
	
	return Failed();
	}

// Failure Checks

TP1 BOOL TP2::Failed(INDEX Index) const
{
	return m_Tree.Failed(Index);
	}

TP1 INDEX TP2::Failed(void) const
{
	return m_Tree.Failed();
	}

// End of File

#endif
