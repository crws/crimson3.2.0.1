
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Key Name
//

// Constructors

CKeyName::CKeyName(void)
{
	}

CKeyName::CKeyName(CKeyName const &That) : CString(That)
{
	}

CKeyName::CKeyName(CString const &That) : CString(That)
{
	}

CKeyName::CKeyName(PCTXT pText) : CString(pText)
{
	}

// Assignment Operator

CKeyName const & CKeyName::operator = (CKeyName const &That)
{
	CString::operator = (That);

	return ThisObject;
	}

CKeyName const & CKeyName::operator = (CString const &That)
{
	CString::operator = (That);

	return ThisObject;
	}

CKeyName const & CKeyName::operator = (PCTXT pText)
{
	CString::operator = (pText);

	return ThisObject;
	}

// Attributes

BOOL CKeyName::HasKey(void) const
{
	return !GetKey().IsEmpty();
	}

BOOL CKeyName::HasName(void) const
{
	CString s = GetName();
	
	if( s.IsEmpty() ) {
		
		return FALSE;
		}
	
	if( s.CompareN(L"default") ) {

		return TRUE;
		}

	return FALSE;
	}

// Element Parsing

CString CKeyName::GetKey(void) const
{
	UINT p = FindRev('\\');

	return (p < NOTHING) ? Left(p) : L"";
	}

CString CKeyName::GetName(void) const
{
	UINT p = FindRev('\\');

	return (p < NOTHING) ? Mid(p + 1) : ThisObject;
	}

// Strip Operations

void CKeyName::StripKey(void)
{
	CString::operator = (GetName());
	}

void CKeyName::StripName(void)
{
	CString::operator = (GetKey());
	}

// Edit Conversions

CKeyName CKeyName::WithName(PCTXT pName) const
{
	CKeyName Result = ThisObject;

	Result.ChangeName(pName);

	return Result;
	}

// Edit Operations

void CKeyName::ChangeName(PCTXT pName)
{
	CString::operator = (GetKey() + L'\\' + CString(pName));
	}

void CKeyName::ChangeKey(PCTXT pKey)
{
	CString::operator = (CString(pKey) + L'\\' + GetName());
	}

// End of File
