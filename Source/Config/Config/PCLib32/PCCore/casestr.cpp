
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Case-Sensitive String
//

// Constructors

CCaseString::CCaseString(void)
{
	}

CCaseString::CCaseString(CCaseString const &That) : CString(That)
{
	}

CCaseString::CCaseString(CString const &That) : CString(That)
{
	}

// Assignment

CCaseString const & CCaseString::operator = (CCaseString const &That)
{
	CString::operator = (That);

	return ThisObject;
	}

CCaseString const & CCaseString::operator = (CString const &That)
{
	CString::operator = (That);

	return ThisObject;
	}

// Conversion

CCaseString::operator CString (void) const
{
	return ThisObject;
	}

// Comparison

int AfxCompare(CCaseString const &a, CCaseString const &b)
{
	return wstrcmp(a.m_pText, b.m_pText);
	}

// End of File
