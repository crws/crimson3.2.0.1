
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Filename String
//

// Constructors

CFilename::CFilename(void)
{
	}

CFilename::CFilename(int nFolder)
{
	WCHAR sPath[MAX_PATH] = { 0 };

	SHGetFolderPath(NULL, nFolder, NULL, SHGFP_TYPE_CURRENT, sPath);

	if( sPath[0] ) {

		Alloc(wstrlen(sPath));
		
		wstrcpy(m_pText, sPath);

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CFilename::CFilename(CFilename const &That) : CString(That)
{
	}

CFilename::CFilename(CString const &That) : CString(That)
{
	}

CFilename::CFilename(PCTXT pText) : CString(pText)
{
	}

// Assignment Operator

CFilename const & CFilename::operator = (CFilename const &That)
{
	CString::operator = (That);

	return ThisObject;
	}

CFilename const & CFilename::operator = (CString const &That)
{
	CString::operator = (That);

	return ThisObject;
	}

CFilename const & CFilename::operator = (PCTXT pText)
{
	CString::operator = (pText);

	return ThisObject;
	}

// Attributes

BOOL CFilename::IsUNC(void) const
{
	return Left(2) == L"\\\\";
	}

BOOL CFilename::HasDrive(void) const
{
	return !GetDrive().IsEmpty();
	}

BOOL CFilename::HasPath(void) const
{
	return !GetBarePath().IsEmpty();
	}

BOOL CFilename::HasName(void) const
{
	return !GetName().IsEmpty();
	}

BOOL CFilename::HasType(void) const
{
	return !GetType().IsEmpty();
	}

// Element Parsing

CString CFilename::GetDrive(void) const
{
	if( IsUNC() ) {

		UINT p = Find('\\', 2U);

		if( p < NOTHING ) {

			p = Find('\\', p + 1);

			if( p < NOTHING ) {

				return Left(p);
				}
			}

		return L"";
		}
	else {
		UINT p = Find(':');

		if( p < NOTHING ) {

			return Left(p + 1).ToUpper();
			}

		return L"";
		}
	}

CString CFilename::GetDirectory(void) const
{
	return Left(GetLength() - GetName().GetLength());
	}

CString CFilename::GetPath(void) const
{
	if( IsUNC() ) {

		UINT p = Find('\\', 2U);

		if( p < NOTHING ) {

			p = Find('\\', p + 1);

			if( p < NOTHING ) {

				return Mid(p);
				}
			}

		return L"";
		}
	else {
		UINT p = Find(':');

		if( p < NOTHING ) {

			return Mid(p + 1);
			}

		return ThisObject;
		}
	}

CString CFilename::GetBarePath(void) const
{
	CString s = GetPath();

	UINT    p = s.GetLength() - GetName().GetLength();

	return s.Left(p);
	}

CString CFilename::GetName(void) const
{
	UINT p;
	
	p = FindRev('\\');

	p = (p == NOTHING ) ? FindRev(':') : p;

	return (p < NOTHING) ? Mid(p + 1) : ThisObject;
	}

CString CFilename::GetBareName(void) const
{
	CString s = GetName();

	UINT    p = s.FindRev('.');

	return s.Left(p);
	}

CString CFilename::GetType(void) const
{
	CString s = GetName();

	UINT    p = s.FindRev('.');

	return (p < NOTHING) ? s.Mid(p + 1) : L"";
	}

// Length Conversion

void CFilename::MakeLong(void)
{
	UINT uSize = GetLongPathName(m_pText, NULL, 0);

	if( uSize ) {

		GrowString(uSize);

		GetLongPathName(m_pText, m_pText, uSize);

		Compress();

		return;
		}

	Empty();
	}

void CFilename::MakeShort(void)
{
	UINT uSize = GetShortPathName(m_pText, NULL, 0);

	if( uSize ) {

		GrowString(uSize);

		GetShortPathName(m_pText, m_pText, uSize);

		Compress();

		return;
		}

	Empty();
	}

// Strip Operations

void CFilename::StripDrive(void)
{
	CString::operator = (GetPath());
	}

void CFilename::StripPath(void)
{
	CString::operator = (GetName());
	}

void CFilename::StripName(void)
{
	CString::operator = (GetDrive() + GetBarePath());
	}

void CFilename::StripType(void)
{
	CString::operator = (GetDrive() + GetBarePath() + GetBareName());
	}

// Edit Conversions

CFilename CFilename::WithName(PCTXT pName) const
{
	CFilename Result = ThisObject;

	Result.ChangeName(pName);

	return Result;
	}

CFilename CFilename::WithType(PCTXT pType) const
{
	CFilename Result = ThisObject;

	Result.ChangeType(pType);

	return Result;
	}

// Edit Operations

void CFilename::ChangeName(PCTXT pName)
{
	StripName();

	operator += (pName);
	}

void CFilename::ChangeType(PCTXT pType)
{
	StripType();

	operator += ('.');
	
	operator += (pType);
	}

// Path Creation

BOOL CFilename::CreatePath(void)
{
	CFilename Path = GetDirectory();

	if( !Path.IsDir() ) {

		CFilename Walk = Path.GetDrive();

		CFilename Rest = Path.GetPath().Mid(1);

		CStringArray List;

		Rest.Tokenize(List, '\\');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			Walk += L'\\';

			Walk += List[n];

			if( !Walk.IsDir() ) {

				if( !CreateDirectory(Walk, NULL) ) {

					return FALSE;
					}
				}
			}

		return TRUE;
		}

	return TRUE;
	}

// Existance Checks

BOOL CFilename::IsFile(void) const
{
	DWORD Attr = GetFileAttributes(PCTXT(ThisObject));

	if( Attr == INVALID_FILE_ATTRIBUTES ) {

		return FALSE;
		}

	if( Attr & FILE_ATTRIBUTE_DEVICE ) {

		return FALSE;
		}

	if( Attr & FILE_ATTRIBUTE_DIRECTORY ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CFilename::IsDir(void) const
{
	DWORD Attr = GetFileAttributes(PCTXT(ThisObject));

	if( Attr == INVALID_FILE_ATTRIBUTES ) {

		return FALSE;
		}

	if( Attr & FILE_ATTRIBUTE_DEVICE ) {

		return FALSE;
		}

	if( Attr & FILE_ATTRIBUTE_DIRECTORY ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CFilename::Exists(void) const
{
	return IsFile();
	}

// File Access

HANDLE CFilename::OpenRead(void) const
{
	return CreateFile( PCTXT(ThisObject),
			   GENERIC_READ,
			   FILE_SHARE_READ,
			   NULL,
			   OPEN_EXISTING,
			   0,
			   NULL
			   );
	}

HANDLE CFilename::OpenReadSeq(void) const
{
	return CreateFile( PCTXT(ThisObject),
			   GENERIC_READ,
			   FILE_SHARE_READ,
			   NULL,
			   OPEN_EXISTING,
			   FILE_FLAG_SEQUENTIAL_SCAN,
			   NULL
			   );
	}

HANDLE CFilename::OpenWrite(void) const
{
	return CreateFile( PCTXT(ThisObject),
			   GENERIC_WRITE,
			   FILE_SHARE_WRITE,
			   NULL,
			   CREATE_ALWAYS,
			   FILE_ATTRIBUTE_NORMAL,
			   NULL
			   );
	}

// Temporary Files

void CFilename::MakeTemporary(void)
{
	TCHAR sPath[MAX_PATH];

	TCHAR sFile[MAX_PATH];

	GetTempPath(MAX_PATH, sPath);

	GetTempFileName(sPath, L"RLC", 0, sFile);

	CString::operator = (sFile);

	DeleteFile(sFile);
	}

void CFilename::MakeTemporary(CString Path)
{
	TCHAR sFile[MAX_PATH] = { 0 };

	GetTempFileName(Path, L"RLC", 0, sFile);

	CString::operator = (sFile);

	DeleteFile(sFile);
	}

// End of File
