
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCCOM_HPP
	
#define	INCLUDE_PCCOM_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pccore.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pccom.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCCOM

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pccom.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// General Macros
//

#define	METHOD	STDMETHODCALLTYPE

#define	HRM	HRESULT METHOD

#define	VHRM	virtual HRESULT METHOD

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBSTR;

//////////////////////////////////////////////////////////////////////////
//
// Deleting BSTR
//

class DLLAPI CBSTR: public CString
{
	public:
		// Constructors
		CBSTR(BSTR bstr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class COleResult;
class COleLookup;
class CHasResult;

//////////////////////////////////////////////////////////////////////////
//
// OLE Result Code Collection
//

typedef CMap <HRESULT, CString> COleResultMap;

//////////////////////////////////////////////////////////////////////////
//
// OLE Result Code Macros
//

#define	AfxResultCodeTable(n)	static COleLookup n[] = 

#define	AfxResultCodeEntry(h)	COleLookup(h, L#h),

//////////////////////////////////////////////////////////////////////////
//
// OLE Result Code Wrapper
//

class DLLAPI COleResult
{
	public:
		// Constructors
		COleResult(void);
		COleResult(COleResult const &That);
		COleResult(HRESULT h);

		// Assignment Operators
		COleResult const & operator = (COleResult const &That);
		COleResult const & operator = (HRESULT h);

		// Conversions
		operator HRESULT (void) const;

		// Attributes
		BOOL    Succeeded(void) const;
		BOOL    Failed(void) const;
		DWORD   GetSeverity(void) const;
		DWORD   GetFacility(void) const;
		CString GetName(void) const;

		// Description
		CString Describe(void) const;
		CString DescribeSeverity(void) const;
		CString DescribeFacility(void) const;
		CString DescribeName(void) const;

	protected:
		// Data Members
		HRESULT m_h;

		// Static Data
		static CArray <COleResultMap *> m_Maps;

		// Code Registration
		static void InsertCode(HRESULT h, PCTXT p);
		static void DeleteCode(HRESULT h);

		// Friend Classes
		friend class COleLookup;
	};

//////////////////////////////////////////////////////////////////////////
//
// OLE Result Code Lookup
//

class DLLAPI COleLookup
{
	public:
		// Constructor
		COleLookup(HRESULT h, PCTXT p);

		// Destructor
		~COleLookup(void);

	protected:
		// Data Members
		HRESULT m_h;
	};

//////////////////////////////////////////////////////////////////////////
//
// Object with OLE Result
//

class DLLAPI CHasResult
{
	public:
		// Constructor
		CHasResult(void);

		// Status Check
		BOOL IsOkay(void) const;
		
		// Result Access
		COleResult const & GetResult(void) const;

	protected:
		// Data Members
		COleResult mutable m_Result;

		// Implementation
		BOOL Check(HRESULT h) const;
		void CheckAndThrow(HRESULT h) const;
		void ThrowResult(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CComRuntimeClass;
class CComObject;
class CComFactory;
class CConnectableObject;
class CEnumConnectionPoints;
class CConnectionPoint;
class CEnumConnections;

//////////////////////////////////////////////////////////////////////////
//
// Class Information Types
//

typedef	CMap  <IID, CConnectionPoint *> CConnectionPointMap;

typedef CList <IUnknown *>              CConnectionList;

/////////////////////////////////////////////////////////////////////////
//
// Class Information Macros
//

#define	AfxClassGuid(c)		(AfxRuntimeClass(c)->GetClassGuid())

//////////////////////////////////////////////////////////////////////////
//
// Interface Map Entry Macro
//

#define	AfxInterface(iid)	AddInterface(IID_##iid, (iid *) this);

#define	AfxBasedUpon(c)		c::AddInterfaces();

//////////////////////////////////////////////////////////////////////////
//
// Componment Class Declaration
//

#define	AfxDeclareComponentClass()					\
									\
	protected:							\
									\
	static void AfxConstruct(void *pObject);			\
									\
	private:							\
									\
	static CComRuntimeClass m_afxClass;				\
									\
	public:								\
									\
	static  CLASS AfxStaticClassInfo(void);				\
									\
	virtual CLASS AfxObjectClassInfo(void) const;			\

//////////////////////////////////////////////////////////////////////////
//
// Single Threaded Component Class Declaration
//

#define	AfxDeclareComponentClassSTA()					\
									\
	AfxDeclareComponentClass();					\

//////////////////////////////////////////////////////////////////////////
//
// Multi Threaded Component Class Declaration
//

#define	AfxDeclareComponentClassMTA()					\
									\
	protected:							\
									\
	CCriticalSection m_cs;						\
									\
	public:								\
									\
	virtual CCriticalSection * GetGuard(void);			\
									\
	AfxDeclareComponentClass();					\
								
//////////////////////////////////////////////////////////////////////////
//
// Component Class Implementation
//

#define	AfxImplementComponentClass(guid, name, base)			\
									\
	CComRuntimeClass name::m_afxClass				\
	(								\
		L#name,							\
		sizeof(name),						\
		base::AfxStaticClassInfo(),				\
		AfxConstruct,						\
		guid							\
		);							\
                                                       			\
	CLASS name::AfxStaticClassInfo(void)				\
	{								\
		return &m_afxClass;					\
		}							\
									\
	CLASS name::AfxObjectClassInfo(void) const			\
	{								\
		return &m_afxClass;					\
		}							\
									\
	void name::AfxConstruct(void *pObject)				\
	{								\
		new (pObject) name;					\
		}							\

//////////////////////////////////////////////////////////////////////////
//
// Component Class Implementation STA
//

#define	AfxImplementComponentClassSTA(guid, name, base)			\
									\
	AfxImplementComponentClass(guid, name, base)			\

//////////////////////////////////////////////////////////////////////////
//
// Component Class Implementation MTA
//

#define	AfxImplementComponentClassMTA(guid, name, base)			\
									\
	AfxImplementComponentClass(guid, name, base)			\
									\
	CCriticalSection * name::GetGuard(void)				\
	{								\
		return &m_cs;						\
		}							\

//////////////////////////////////////////////////////////////////////////
//
// Interface Map Declaration
//

#define	AfxDeclareInterfaceMap()					\
									\
	public:								\
									\
	HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);	\
	ULONG   METHOD AddRef(void);					\
	ULONG   METHOD Release(void);					\
									\
	protected:							\
									\
	virtual void AddInterfaces(void);				\
									\
	public:								\

//////////////////////////////////////////////////////////////////////////
//
// Interface Map Implementation
//

#define	AfxImplementInterfaceMap(c)					\
									\
	HRESULT c::QueryInterface(REFIID iid, void **ppObject)		\
	{								\
		return CComObject::QueryInterface(iid, ppObject);	\
		}							\
									\
	ULONG c::AddRef(void)						\
	{								\
		return CComObject::AddRef();				\
		}							\
									\
	ULONG c::Release(void)						\
	{								\
		return CComObject::Release();				\
		}							\
									\
	void c::AddInterfaces(void)					\

//////////////////////////////////////////////////////////////////////////
//
// Implicit Unknown Interface
//

interface IImplicit
{
	virtual HRESULT METHOD ImplicitQuery(	REFIID iid,
						void **ppObject
						) = 0;

	virtual ULONG   METHOD ImplicitAddRef(	void
						) = 0;

	virtual ULONG   METHOD ImplicitRelease(	void
						) = 0;

	};

//////////////////////////////////////////////////////////////////////////
//
// Component Class Record
//

class DLLAPI CComRuntimeClass : public CRuntimeClass
{
	public:
		// Constructor
		CComRuntimeClass( PCTXT pName,
				  UINT  uSize,
				  CLASS Base,
				  CTOR  Ctor,
				  CGuid Guid
				  );
		
	protected:
		// Data Members
		DWORD m_Factory;

		// Overridables
		void ManageFactory(BOOL fReg);
	};

//////////////////////////////////////////////////////////////////////////
//
// Component Registration Info
//

class DLLAPI CComRegInfo
{
	public:
		// Constructor
		CComRegInfo(void);

		// Data Members
		UINT    m_Version;
		CString m_Readable;
		CString m_VerInd;
		CString m_ProgID;
	};

//////////////////////////////////////////////////////////////////////////
//
// Component Categories Info
//

struct CComCategory
{
	CGuid   m_cid;
	CString m_Desc;
	BOOL    m_fOwner;
	};

//////////////////////////////////////////////////////////////////////////
//
// Component Object
//

class DLLAPI CComObject : public CObject,
			  public IUnknown,
			  public IImplicit
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CComObject(void);

		// Unknown Access
		IUnknown * GetUnknown(void);
		IUnknown * GetImplicit(void);

		// IUnknown Methods
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IImplicit Methods
		HRESULT METHOD ImplicitQuery(REFIID iid, void **ppObject);
		ULONG   METHOD ImplicitAddRef(void);
		ULONG   METHOD ImplicitRelease(void);

		// Operations
		void SetOuter(IUnknown *punkOuter);

		// Creation
		virtual BOOL CheckLicense(void);
		virtual BOOL Create(void);

		// Threading
		virtual CCriticalSection * GetGuard(void);

		// Registration
		virtual void BuildRegInfo(CComRegInfo &Info);
		virtual void BuildRegKeys(CKeyMap  &Map);
		virtual BOOL EnumCategories(UINT i, CComCategory &Cat);
		virtual BOOL CustomRegistration(void);

	protected:
		// Aggregate Maps
		typedef CMap <IID,   IUnknown *> CAggregateIntMap;
		typedef CMap <CLSID, IUnknown *> CAggregateObjMap;

		// Data Members
		LONG		   m_uRefCount;
		IUnknown	 * m_punkOuter;
		CAggregateIntMap * m_pAggIntMap;
		CAggregateObjMap * m_pAggObjMap;

		// Destructor
		~CComObject(void);

		// Interface Map Creation
		virtual void AddInterfaces(void);

		// Threading
		virtual void EnterGuarded(void);
		virtual void LeaveGuarded(void);

		// Connection Support
		DWORD AttachConnection(REFIID iid, IUnknown *pSource, IUnknown *pSink);
		BOOL  DetachConnection(REFIID iid, IUnknown *pSource, DWORD Cookie);

		// Interface and Aggregation
		void AddInterface(REFIID iid, IUnknown *pObject);
		BOOL AddAggregate(REFIID iid, IUnknown *pObject);
		BOOL AddAggregate(REFIID iid, CGuid const &Guid);

		// Implementation
		BOOL BuildInterfaceMap(void);
		BOOL AllocAggregateMap(void);
		void EmptyAggregateMap(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Class Factory Object
//

class DLLAPI CComFactory : public CObject,
			   public IClassFactory
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CComFactory(CLASS Class);

		// IUnknown Methods
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IClassFactory Methods
		HRM CreateInstance(IUnknown *punkOuter, REFIID iid, void **ppObject);
		HRM LockServer(BOOL fLock);

	protected:
		// Data Members
		CLASS m_Class;
		LONG  m_uRefCount;

		// Destructor
		~CComFactory(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Connection Point Container
//

class DLLAPI CConnectableObject : public CComObject,
			          public IConnectionPointContainer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CConnectableObject(void);

		// Interface Map
		AfxDeclareInterfaceMap();

		// IConnectionPointContainer Methods
		HRM EnumConnectionPoints(IEnumConnectionPoints **ppEnum);
		HRM FindConnectionPoint(REFIID iid, IConnectionPoint **ppConnect);

		// Connection Location
		CConnectionPoint * FindConnectionPoint(REFIID iid);

	protected:
		// Data Members
		CConnectionPointMap *m_pConMap;

		// Destructor
		~CConnectableObject(void);

		// Implementation
		void AddConnectionPoint(REFIID iid);
		BOOL AllocConnectionMap(void);
		void EmptyConnectionMap(void);

		// Friend Classes
		friend class CEnumConnectionPoints;
	};

//////////////////////////////////////////////////////////////////////////
//
// Connection Point Enumerator
//

class DLLAPI CEnumConnectionPoints : public CComObject,
				     public IEnumConnectionPoints
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CEnumConnectionPoints(CEnumConnectionPoints const &That);
		CEnumConnectionPoints(CConnectableObject *pObject);

		// Destructor
		~CEnumConnectionPoints(void);

		// Interface Map
		AfxDeclareInterfaceMap();

		// IEnumConnectionPoints Methods
		HRM Next(ULONG uCount, IConnectionPoint **ppList, ULONG *pFetch);
		HRM Skip(ULONG uCount);
		HRM Reset(void);
		HRM Clone(IEnumConnectionPoints **ppEnum);

	protected:
		// Data Members
		CConnectableObject * m_pObject;
		INDEX                m_i;
	};

//////////////////////////////////////////////////////////////////////////
//
// Connection Point Object
//

class DLLAPI CConnectionPoint : public CComObject,
				public IConnectionPoint 
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CConnectionPoint(CConnectableObject *pObject, REFIID iid);

		// Destructor
		~CConnectionPoint(void);

		// Interface Map
		AfxDeclareInterfaceMap();

		// IConnectionPoint Methods
		HRM GetConnectionInterface(IID *pIID);
		HRM GetConnectionPointContainer(IConnectionPointContainer **ppCont);
		HRM Advise(IUnknown *pUnknown, DWORD *pCookie);
		HRM Unadvise(DWORD Cookie);
		HRM EnumConnections(IEnumConnections **ppEnum);

		// Connection Access
		IUnknown * GetConnection(INDEX i) const;

		// Connection Enumeration
		INDEX GetHeadConnection(void) const;
		INDEX GetTailConnection(void) const;
		BOOL  GetNextConnection(INDEX &i) const;
		BOOL  GetPrevConnection(INDEX &i) const;

		// Enumerator Failure Check
		BOOL  Failed(INDEX i) const;
		INDEX Failed(void) const;

		// Threading
		CCriticalSection * GetGuard(void);

	protected:
		// Data Members
		CConnectableObject * m_pObject;
		IID		     m_iid;
		CConnectionList    * m_pConList;

		// Friend Classes
		friend class CEnumConnections;
	};

//////////////////////////////////////////////////////////////////////////
//
// Connection Enumerator
//

class DLLAPI CEnumConnections : public CComObject,
				public IEnumConnections
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CEnumConnections(CEnumConnections const &That);
		CEnumConnections(CConnectionPoint *pObject);

		// Destructor
		~CEnumConnections(void);

		// Interface Map
		AfxDeclareInterfaceMap();

		// IEnumConnectionPoints Methods
		HRM Next(ULONG uCount, CONNECTDATA *pList, ULONG *pFetch);
		HRM Skip(ULONG uCount);
		HRM Reset(void);
		HRM Clone(IEnumConnections **ppEnum);

		// Threading
		CCriticalSection * GetGuard(void);

	protected:
		// Data Members
		CConnectionPoint * m_pObject;
		INDEX              m_i;
	};

//////////////////////////////////////////////////////////////////////////
//
// Connection Caller Macro
//

#define	AfxFireEvent(ii, fn)					\
								\
	BEGIN {							\
	CConnectionPoint *p = FindConnectionPoint(IID_##ii);	\
	if( p ) {						\
	CCriticalGuard g(p->GetGuard());			\
	INDEX i = p->GetHeadConnection();			\
	while( !p->Failed(i) ) {				\
	ii *s = (ii *) p->GetConnection(i);			\
	s->fn;							\
	p->GetNextConnection(i);				\
	} } } END						\

//////////////////////////////////////////////////////////////////////////
//
// Connection Poller Macro
//

#define	AfxPollEvent(hr, ii, fn)				\
								\
	BEGIN {							\
	CConnectionPoint *p = FindConnectionPoint(IID_##ii);	\
	if( p ) {						\
	CCriticalGuard g(p->GetGuard());			\
	INDEX i = p->GetHeadConnection();			\
	while( !p->Failed(i) ) {				\
	ii *s = (ii *) p->GetConnection(i);			\
	if( (hr = s->fn) != S_OK ) break;			\
	p->GetNextConnection(i);				\
	} } } END						\

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class COleException;

//////////////////////////////////////////////////////////////////////////
//
// Exception Throwing Functions
//

DLLAPI void ThrowOleException(PCTXT pFile, UINT uLine, HRESULT hResult);

//////////////////////////////////////////////////////////////////////////
//
// Exception Throwing Macros
//

#ifdef  _DEBUG

#define AfxThrowOleException(h)	    ThrowOleException(afxFile, afxLine, h)

#else

#define AfxThrowOleException(h)	    ThrowOleException(NULL, 0, h)

#endif

//////////////////////////////////////////////////////////////////////////
//
// OLE Exception
//

class DLLAPI COleException : public CException
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Data Members
		HRESULT m_hResult;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CComModule;

//////////////////////////////////////////////////////////////////////////
//
// Component Module
//

class DLLAPI CComModule : public CModule
{
	public:
		// Constructor
		CComModule(UINT uType);

		// DLL Server Calls
		HRESULT DllCanUnloadNow(void);
		HRESULT DllGetClassObject(REFCLSID Class, REFIID iid, void **ppObject);
		HRESULT DllRegisterServer(void);
		HRESULT DllUnregisterServer(void);

		// EXE Server Calls
		HRESULT AppRegisterServer(void);
		HRESULT AppUnregisterServer(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Definitions
//

class CTypeLib;
class CTypeInfo;
class CRegHelper;

//////////////////////////////////////////////////////////////////////////
//
// Type Library
//

class DLLAPI CTypeLib : public CHasResult
{
	public:
		// Constructor
		CTypeLib(void);
		CTypeLib(CTypeLib const &That);
		CTypeLib(ITypeLib *pTypeLib);
		CTypeLib(ITypeLib *pTypeLib, BOOL fAdd);

		// Destructor
		~CTypeLib(void);

		// Assignment Operators
		CTypeLib const & operator = (CTypeLib const &That);
		CTypeLib const & operator = (ITypeLib *pTypeLib);

		// Conversion
		operator ITypeLib * (void) const;
		operator BOOL (void) const;
		operator ! (void) const;

		// Attributes
		CGuid     GetGuid(void);
		LCID      GetLcid(void);
		WORD      GetFlags(void);
		CString   GetVersion(void);
		CString   GetName(void);
		CString   GetDesc(void);
		CFilename GetFilename(void);
		
		// Type Info
		UINT      GetTypeInfoCount(void);
		TYPEKIND  GetTypeInfoType(UINT uIndex);
		CTypeInfo GetTypeInfo(UINT uIndex);

		// Enumerators
		CTypeInfo EnumComponents(UINT n);
		CTypeInfo EnumInterfaces(UINT n);
		CTypeInfo EnumDispatchs(UINT n);

		// Loading
		void Load(CFilename const &File); 
		void Load(CFilename const &File, UINT uResourceID);
		void Load(CGuid const &LibID, DWORD dwVersion);
	
	protected:
		// Data
		TLIBATTR  * m_pAttr;
		ITypeLib  * m_pTypeLib;
		CFilename   m_Filename;

		// Library Attributes
		void GetLibAttr(void);
		void FreeLibAttr(void);

		// Enumerator
		CTypeInfo Enum(UINT n, TYPEKIND Type);

		// Implementation
		void InitFrom(ITypeLib *pTypeLib, BOOL fAdd);
		void CloseLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Info
//

class DLLAPI CTypeInfo : public CHasResult
{
	public:
		// Constructor
		CTypeInfo(void);
		CTypeInfo(CTypeInfo const &That);
		CTypeInfo(ITypeInfo *pITypeInfo);
		CTypeInfo(ITypeInfo *pITypeInfo, BOOL fAdd);

		// Destructor
		~CTypeInfo(void);

		// Assignment Operators
		CTypeInfo const & operator = (CTypeInfo const &That);
		CTypeInfo const & operator = (ITypeInfo *pITypeInfo);

		// Conversion
		operator ITypeInfo * (void) const;
		operator BOOL (void) const;
		operator ! (void) const;

		// Attributes
		CGuid    GetGuid(void);
		LCID     GetLcid(void);
		WORD	 GetFlags(void);
		TYPEKIND GetType(void);
		WORD	 GetFuncs(void);
		WORD	 GetVars(void);
		CString  GetVersion(void);
		WORD	 GetInterfaces(void);
		CString  GetName(void);
		CString  GetDesc(void);

		// Type Library
		CTypeLib GetTypeLib(void);
		
		// Dispatch Helpers
		HRESULT GetIDsOfNames( OLECHAR ** szNames, 
				       UINT       cbNames, 
				       MEMBERID * pMemId
				       );

		HRESULT Invoke( void       * pvInstance,     
				MEMBERID     memid,           
				WORD	     wFlags,    
				DISPPARAMS * pDispParams,  
				VARIANT    * pVarResult,  
				EXCEPINFO  * pExcepInfo,  
				UINT       * puArgErr
				);		
	protected:
		// Data
		TYPEATTR  * m_pAttr;
		ITypeInfo * m_pITypeInfo;

		// Type Attributes
		void GetTypeAttr(void);
		void FreeTypeAttr(void);

		// Implementation
		void InitFrom(ITypeInfo *pITypeInfo, BOOL fAdd);
		void Close(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Module Registration Helper
//

class DLLAPI CComRegHelper
{
	public:
		// Constructor
		CComRegHelper(CModule *pModule);

		// Destructor
		~CComRegHelper(void);

		// Components
		BOOL RegComponents(void);
		BOOL UnregComponents(void);

		// Interfaces
		BOOL RegInterface(REFIID iid, CString Name, REFGUID Proxy); 
		BOOL RegDispInterface(REFIID iid, CString Name, REFGUID LibID, CString Ver); 
		BOOL RegDispInterface(REFIID iid, CString Name, REFGUID LibID, CString Ver, REFGUID Proxy);
		BOOL UnregInterface(REFIID iid);

		// Type Libs
		BOOL RegTypeLibs(void);
		BOOL UnregTypeLibs(void);

		// Proxy Servers
		BOOL RegProxy(CGuid &ClsID, CFilename &Filename, CString Name);
		BOOL UnregProxy(CGuid &ClsID);
		
	protected:
		// Data Members
		CModule * m_pModule;

		// Component Helpers
		BOOL RegComponent(CComObject &Object);
		BOOL UnregComponent(CComObject &Object);

		// Component Categories
		BOOL RegCategories(CComObject &Object);
		BOOL UnregCategories(CComObject &Object);

		// Interface Helpers
		BOOL RegInterface(CGuid IID, CString Name, CGuid LibID, CString Ver, CGuid Proxy);
		UINT IncrementRef(CRegKey &Key);
		UINT DecrementRef(CRegKey &Key);

		// Type Lib Helpers
		BOOL RegTypeLibFromFile(void);
		BOOL RegTypeLibFromResource(void);
		BOOL RegTypeLib(CTypeLib &Lib);
		BOOL RegTypeLibComponents(CTypeLib &Lib);
		BOOL RegTypeLibInterfaces(CTypeLib &Lib);
		BOOL UnregTypeLibFromFile(void);
		BOOL UnregTypeLibFromResource(void);
		BOOL UnregTypeLib(CTypeLib &Lib);
		BOOL UnregTypeLibComponents(CTypeLib &Lib);
		BOOL UnregTypeLibInterfaces(CTypeLib &Lib);
	};

// End of File

#endif
