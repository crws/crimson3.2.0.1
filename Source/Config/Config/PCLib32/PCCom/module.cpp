
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Component Module
//

// Constructor

CComModule::CComModule(UINT uType) : CModule(uType)
{
	}

// DLL Server Calls

HRESULT CComModule::DllCanUnloadNow(void)
{
	AfxAssert(m_Type == modInProcServer);

	if( m_nLocks ) {

		return S_FALSE;
		}
		
	if( m_nObjects ) {

		return S_FALSE;
		}

	return S_OK;
	}

HRESULT CComModule::DllGetClassObject(REFCLSID Class, REFIID iid, void **ppObject)
{
	AfxAssert(m_Type == modInProcServer);

	if( ppObject ) {

		CComFactory *pFactory = NULL;

		try {
			*ppObject = NULL;

			INDEX i = m_GuidMap.FindName(Class);

			if( !m_GuidMap.Failed(i) ) {

				CLASS Class = m_GuidMap.GetData(i);

				pFactory    = New CComFactory(Class);
				}

			if( pFactory ) {

				HRESULT h = pFactory->QueryInterface(iid, ppObject);

				pFactory->Release();

				return h;
				}

			return CLASS_E_CLASSNOTAVAILABLE;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CComModule::DllRegisterServer(void)
{
	AfxAssert(m_Type == modInProcServer);

	CComRegHelper Helper(this);

	if( !Helper.RegComponents() ) {

		return E_FAIL;
		}
	
	if( !Helper.RegTypeLibs() ) {

		return E_FAIL;
		}
	
	return S_OK;
	}

HRESULT CComModule::DllUnregisterServer(void)
{
	AfxAssert(m_Type == modInProcServer);

	CComRegHelper Helper(this);

	if( !Helper.UnregComponents() ) {

		return E_FAIL;
		}
		
	if( !Helper.UnregTypeLibs() ) {

		return E_FAIL;
		}
	
	return S_OK;
	}

// EXE Server Calls

HRESULT CComModule::AppRegisterServer(void)
{
	AfxAssert(m_Type == modApplication);

	CComRegHelper Helper(this);

	if( !Helper.RegComponents() ) {

		return E_FAIL;
		}
	
	if( !Helper.RegTypeLibs() ) {

		return E_FAIL;
		}
	
	return S_OK;
	}

HRESULT CComModule::AppUnregisterServer(void)
{
	AfxAssert(m_Type == modApplication);

	CComRegHelper Helper(this);

	if( !Helper.UnregComponents() ) {

		return E_FAIL;
		}
		
	if( !Helper.UnregTypeLibs() ) {

		return E_FAIL;
		}
	
	return S_OK;
	}

// End of File
