
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Warning Control
//

#pragma warning(disable: 4671)

#pragma warning(disable: 4673)

//////////////////////////////////////////////////////////////////////////
//
// Exception Throwing Functions
//

void ThrowOleException(PCTXT pFile, UINT uLine, HRESULT hResult)
{
	AfxTrace(L"THROW: COleException at line %u of %s\n", uLine, pFile);

	AfxTrace(L"THROW: %s\n", COleResult(hResult).Describe());

	COleException Ex;

	Ex.SetContext(pFile, uLine);

	Ex.m_hResult = hResult;

	throw Ex;
	}

//////////////////////////////////////////////////////////////////////////
//
// Exception Type Records
//

AfxImplementRuntimeClass(COleException, CException);

// End of File
