
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load PCCom\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading PCCom\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	if( uReason == DLL_THREAD_ATTACH ) {

		afxModule->ThreadAttach();

		return TRUE;
		}

	if( uReason == DLL_THREAD_DETACH ) {

		afxModule->ThreadDetach();

		return TRUE;
		}

	return TRUE;
	}

global void DLLAPI PCCom(void)
{
	}

// End of File
