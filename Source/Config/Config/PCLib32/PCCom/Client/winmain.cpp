
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

extern void TestFunc(void);

//////////////////////////////////////////////////////////////////////////
//
// Test Harness
//

int WINAPI WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow)
{
	afxModule = New CComModule(modApplication);

	if( !afxModule->InitApp(hThis, GetCommandLine(), nCmdShow) ) {

		AfxTrace(L"ERROR: Failed to initialize application\n");

		delete afxModule;

		afxModule = NULL;

		return 1;
		}

	TestFunc();

	afxModule->Terminate();

	delete afxModule;

	afxModule = NULL;

	return 0;
	}

// End of File
