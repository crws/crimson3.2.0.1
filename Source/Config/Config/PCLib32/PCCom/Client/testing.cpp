
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	CGuid guid = L"{ADA1668A-D4C2-43A4-8E91-8A5877026885}";

	IUnknown *pObject = NULL;

	CoCreateInstance(guid, NULL, CLSCTX_INPROC_SERVER, IID_IUnknown, (void**) &pObject);

	IConnectionPointContainer *pCont1 = NULL;

	IConnectionPointContainer *pCont2 = NULL;

	pObject->QueryInterface(IID_IConnectionPointContainer, (void**) &pCont1);

	pObject->QueryInterface(IID_IConnectionPointContainer, (void**) &pCont2);

	pCont1->Release();

	pCont2->Release();

	pObject->Release();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Class
//

// Component Class

AfxImplementComponentClass( L"{ADA1668A-D4C2-43A4-8E91-8A5877026885}",
			    CTestClass,
			    CConnectableObject
			    );

// Constructor

CTestClass::CTestClass(void)
{
	}

// Destructor

CTestClass::~CTestClass(void)
{
	}

// End of File
