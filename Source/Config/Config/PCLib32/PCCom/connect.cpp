
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Connection Point Container
//

// Dynamic Class

AfxImplementDynamicClass(CConnectableObject, CComObject);

// Constructor

CConnectableObject::CConnectableObject(void)
{
	m_pConMap = NULL;
	}

// Destructor

CConnectableObject::~CConnectableObject(void)
{
	EmptyConnectionMap();
	}

// Interface Map

AfxImplementInterfaceMap(CConnectableObject)
{
	AfxInterface(IConnectionPointContainer)

	AfxBasedUpon(CComObject)
	}

// IConnectionPointContainer Methods

HRESULT CConnectableObject::EnumConnectionPoints(IEnumConnectionPoints **ppEnum)
{
	if( ppEnum ) {

		try {
			*ppEnum = NULL;

			*ppEnum = New CEnumConnectionPoints(this);

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CConnectableObject::FindConnectionPoint(REFIID iid, IConnectionPoint **ppConnect)
{
	if( ppConnect ) {

		try {
			*ppConnect = NULL;

			if( m_pConMap ) {

				CConnectionPointMap &ConMap = *m_pConMap;

				INDEX i = ConMap.FindName(iid);

				if( !ConMap.Failed(i) ) {

					CConnectionPoint *pConnect = ConMap[i];

					pConnect->QueryInterface( IID_IConnectionPoint,
								  (void **) ppConnect
								  );

					return S_OK;
					}
				}

			return CONNECT_E_NOCONNECTION;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

// Connection Location

CConnectionPoint * CConnectableObject::FindConnectionPoint(REFIID iid)
{
	BuildInterfaceMap();

	if( m_pConMap ) {

		CConnectionPointMap &ConMap = *m_pConMap;

		INDEX i = ConMap.FindName(iid);

		if( ConMap.Failed(i) ) {

			return NULL;
			}

		return ConMap[i];
		}

	return NULL;
	}

// Implementation

void CConnectableObject::AddConnectionPoint(REFIID iid)
{
	AllocConnectionMap();

	CConnectionPoint *pConnect = New CConnectionPoint(this, iid);

	m_pConMap->Insert(iid, pConnect);
	}

BOOL CConnectableObject::AllocConnectionMap(void)
{
	if( !m_pConMap ) {

		m_pConMap = New CConnectionPointMap;

		return TRUE;
		}

	return FALSE;
	}

void CConnectableObject::EmptyConnectionMap(void)
{
	if( m_pConMap ) {

		CConnectionPointMap &ConMap = *m_pConMap;

		INDEX i = ConMap.GetHead();

		while( !ConMap.Failed(i) ) {

			CConnectionPoint *pConnect = ConMap[i];

			pConnect->Release();
			
			ConMap.GetNext(i);
			}

		ConMap.Empty();

		delete m_pConMap;

		m_pConMap = NULL;
		}
	}

// End of File
