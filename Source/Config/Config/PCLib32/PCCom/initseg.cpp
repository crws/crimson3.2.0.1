
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Initialization Pragma
//

#pragma warning(disable: 4073)

#pragma init_seg(lib)

//////////////////////////////////////////////////////////////////////////
//
// OLE Result Code Maps
//

CArray <COleResultMap *> COleResult::m_Maps;

// End of File
