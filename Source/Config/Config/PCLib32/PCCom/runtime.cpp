
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Component Class Record
//

// Constructor

CComRuntimeClass::CComRuntimeClass( PCTXT pName,
				    UINT  uSize,
				    CLASS Base,
				    CTOR  Ctor,
				    CGuid Guid
				    ) :
	CRuntimeClass( pName,
		       uSize,
		       Base,
		       Ctor
		       )
{
	m_Guid    = Guid;

	m_Factory = 0;
	}
	        
// Overridables

void CComRuntimeClass::ManageFactory(BOOL fReg)
{
	if( fReg ) {

		AfxAssert(!m_Factory);

		CComFactory *pFactory = New CComFactory(this);

		CoRegisterClassObject( GetClassGuid(),
				       pFactory,
				       CLSCTX_LOCAL_SERVER,
				       REGCLS_MULTIPLEUSE,
				       &m_Factory
				       );

		pFactory->Release();

		return;
		}

	AfxAssert(m_Factory);

	CoRevokeClassObject(m_Factory);

	m_Factory = 0;
	}

// End of File
