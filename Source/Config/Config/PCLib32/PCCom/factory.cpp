
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Class Factory Object
//

// Runtime Class

AfxImplementRuntimeClass(CComFactory, CObject);

// Constructor

CComFactory::CComFactory(CLASS Class)
{
	m_Class     = Class;

	m_uRefCount = 1;
	}

// Destructor

CComFactory::~CComFactory(void)
{
	AfxAssert(!m_uRefCount);
	}

// IUnknown Methods

HRESULT CComFactory::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		try {
			if( iid == IID_IUnknown ) {

				*ppObject = (IUnknown *) this;

				InterlockedIncrement(&m_uRefCount);

				return S_OK;
				}

			if( iid == IID_IClassFactory ) {

				*ppObject = (IClassFactory *) this;

				InterlockedIncrement(&m_uRefCount);

				return S_OK;
				}

			*ppObject = NULL;
			
			return E_NOINTERFACE;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

ULONG CComFactory::AddRef(void)
{
	InterlockedIncrement(&m_uRefCount);

	return m_uRefCount;
	}

ULONG CComFactory::Release(void)
{
	if( !InterlockedDecrement(&m_uRefCount) ) {

		delete this;

		return 0;
		}

	return m_uRefCount;
	}

// IClassFactory Methods

HRESULT CComFactory::CreateInstance(IUnknown *punkOuter, REFIID iid, void **ppObject)
{
	if( ppObject ) {

		try {
			*ppObject = NULL;
		
			if( !punkOuter || iid == IID_IUnknown ) {

				CComObject *pObject = AfxNewObject(CComObject, m_Class);

				((ICLASS) AfxPointerClass(pObject))->ObjectCreated();

				pObject->SetOuter(punkOuter);

				if( !pObject->CheckLicense() ) {

					pObject->ImplicitRelease();

					return CLASS_E_NOTLICENSED;
					}

				if( !pObject->Create() ) {

					pObject->ImplicitRelease();

					return E_UNEXPECTED;
					}

				HRESULT h = pObject->ImplicitQuery(iid, ppObject);

				pObject->ImplicitRelease();

				return h;
				}

			AfxTrace(L"ERROR: Aggregation allowed only with IUnknown\n");

			return CLASS_E_NOAGGREGATION;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CComFactory::LockServer(BOOL fLock)
{
	try {
		if( fLock ) {

			m_Class->GetModule()->LockServer();

			return S_OK;
			}

		m_Class->GetModule()->UnlockServer();

		return S_OK;
		}

	AfxStdCatch();
	}

// End of File
