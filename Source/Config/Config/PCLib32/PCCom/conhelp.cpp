
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Connection Point Enumerator
//

// Runtime Class

AfxImplementRuntimeClass(CEnumConnectionPoints, CComObject);

// Constructors

CEnumConnectionPoints::CEnumConnectionPoints(CEnumConnectionPoints const &That)
{
	m_pObject = That.m_pObject;

	m_i       = That.m_i;

	m_pObject->AddRef();
	}

CEnumConnectionPoints::CEnumConnectionPoints(CConnectableObject *pObject)
{
	AfxAssert(pObject);

	m_pObject = pObject;

	m_pObject->AddRef();

	Reset();
	}

// Destructor

CEnumConnectionPoints::~CEnumConnectionPoints(void)
{
	AfxRelease(m_pObject);
	}

// Interface Map

AfxImplementInterfaceMap(CEnumConnectionPoints)
{
	AfxInterface(IEnumConnectionPoints)

	AfxBasedUpon(CComObject)
	}

// IEnumConnectionPoints Methods

HRESULT CEnumConnectionPoints::Next(ULONG uCount, IConnectionPoint **ppList, ULONG *pFetch)
{
	if( ppList ) {

		try {
			UINT uFetch = 0;

			BOOL fBreak = FALSE;

			if( m_pObject->m_pConMap ) {

				CConnectionPointMap &ConMap = *(m_pObject->m_pConMap);

				while( uCount-- ) {

					if( ConMap.Failed(m_i) ) {

						fBreak = TRUE;

						break;
						}

					IConnectionPoint *pConnect = NULL;

					ConMap[m_i]->QueryInterface( IID_IConnectionPoint,
								     (void **) &pConnect
								     );

					ppList[uFetch] = pConnect;

					uFetch++;

					ConMap.GetNext(m_i);
					}
				}
			else
				fBreak = TRUE;

			if( pFetch )
				*pFetch = uFetch;

			if( fBreak )
				return S_FALSE;

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CEnumConnectionPoints::Skip(ULONG uCount)
{
	try {
		if( m_pObject->m_pConMap ) {

			CConnectionPointMap &ConMap = *(m_pObject->m_pConMap);

			while( uCount-- ) {

				if( ConMap.Failed(m_i) ) {

					return S_FALSE;
					}

				ConMap.GetNext(m_i);
				}

			return S_OK;
			}

		return S_FALSE;
		}

	AfxStdCatch();
	}

HRESULT CEnumConnectionPoints::Reset(void)
{
	try {
		if( m_pObject->m_pConMap ) {

			CConnectionPointMap &ConMap = *(m_pObject->m_pConMap);

			m_i = ConMap.GetHead();

			return S_OK;
			}

		return S_OK;
		}

	AfxStdCatch();
	}

HRESULT CEnumConnectionPoints::Clone(IEnumConnectionPoints **ppEnum)
{
	if( ppEnum ) {

		try {
			*ppEnum = NULL;

			*ppEnum = New CEnumConnectionPoints(*this);

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

//////////////////////////////////////////////////////////////////////////
//
// Connection Point Object
//

// Runtime Class

AfxImplementRuntimeClass(CConnectionPoint, CComObject);

// Constructors

CConnectionPoint::CConnectionPoint(CConnectableObject *pObject, REFIID iid)
{
	AfxAssert(pObject);

	m_pObject  = pObject;

	m_iid      = iid;

	m_pConList = NULL;
	}

// Destructor

CConnectionPoint::~CConnectionPoint(void)
{
	if( m_pConList ) {

		CConnectionList &ConList = *m_pConList;

		INDEX i = ConList.GetHead();
		
		while( !ConList.Failed(i) ) {

			IUnknown *pSink = ConList[i];

			pSink->Release();

			ConList.GetNext(i);
			}

		ConList.Empty();

		delete m_pConList;
		}
	}

// Interface Map

AfxImplementInterfaceMap(CConnectionPoint)
{
	AfxInterface(IConnectionPoint)

	AfxBasedUpon(CComObject)
	}

// IConnectionPoint Methods

HRESULT CConnectionPoint::GetConnectionInterface(IID *pIID)
{
	if( pIID ) {

		try {
			*pIID = m_iid;

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CConnectionPoint::GetConnectionPointContainer(IConnectionPointContainer **ppCont)
{
	if( ppCont ) {

		try {
			*ppCont = m_pObject;

			m_pObject->AddRef();

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CConnectionPoint::Advise(IUnknown *pUnknown, DWORD *pCookie)
{
	if( pUnknown && pCookie ) {

		try {
			*pCookie = 0;
			
			IUnknown *pSink = NULL;

			if( SUCCEEDED(pUnknown->QueryInterface(m_iid, (void **) &pSink)) ) {

				CCriticalGuard Guard(GetGuard());
				
				if( !m_pConList ) {

					m_pConList = New CConnectionList;

					AfxAssert(m_pConList);
					}

				INDEX i = m_pConList->Append(pSink);

				*pCookie = DWORD(i);

				m_pObject->AddRef();

				return S_OK;
				}

			return CONNECT_E_CANNOTCONNECT;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CConnectionPoint::Unadvise(DWORD Cookie)
{
	try {
		INDEX i = INDEX(Cookie);

		if( m_pConList ) {

			CConnectionList &ConList = *m_pConList;

			if( !ConList.Failed(i) ) {

				m_pObject->Release();

				IUnknown *pSink = ConList[i];

				pSink->Release();

				CCriticalGuard Guard(GetGuard());

				ConList.Remove(i);

				return S_OK;
				}
			}

		return CONNECT_E_NOCONNECTION;
		}

	AfxStdCatch();
	}

HRESULT CConnectionPoint::EnumConnections(IEnumConnections **ppEnum)
{
	if( ppEnum ) {

		try {
			*ppEnum = NULL;

			*ppEnum = New CEnumConnections(this);

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

// Connection Access

IUnknown * CConnectionPoint::GetConnection(INDEX i) const
{
	if( m_pConList ) {

		CConnectionList &ConList = *m_pConList;

		return ConList[i];
		}

	return NULL;
	}

// Connection Enumeration

INDEX CConnectionPoint::GetHeadConnection(void) const
{
	if( m_pConList ) {

		CConnectionList &ConList = *m_pConList;
	
		return ConList.GetHead();
		}

	return NULL;
	}

INDEX CConnectionPoint::GetTailConnection(void) const
{
	if( m_pConList ) {

		CConnectionList &ConList = *m_pConList;
	
		return ConList.GetTail();
		}

	return NULL;
	}

BOOL CConnectionPoint::GetNextConnection(INDEX &i) const
{
	if( m_pConList ) {

		CConnectionList &ConList = *m_pConList;
	
		return ConList.GetNext(i);
		}

	return FALSE;
	}

BOOL CConnectionPoint::GetPrevConnection(INDEX &i) const
{
	if( m_pConList ) {

		CConnectionList &ConList = *m_pConList;
	
		return ConList.GetPrev(i);
		}

	return FALSE;
	}

// Enumerator Failure Check

BOOL CConnectionPoint::Failed(INDEX i) const
{
	return i == NULL;
	}

INDEX CConnectionPoint::Failed(void) const
{
	return NULL;
	}

// Threading

CCriticalSection * CConnectionPoint::GetGuard(void)
{
	return m_pObject->GetGuard();
	}

//////////////////////////////////////////////////////////////////////////
//
// Connection Enumerator
//

// Runtime Class

AfxImplementRuntimeClass(CEnumConnections, CComObject);

// Constructors

CEnumConnections::CEnumConnections(CEnumConnections const &That)
{
	m_pObject = That.m_pObject;

	m_i       = That.m_i;

	m_pObject->AddRef();
	}

CEnumConnections::CEnumConnections(CConnectionPoint *pObject)
{
	AfxAssert(pObject);

	m_pObject = pObject;

	m_pObject->AddRef();

	Reset();
	}

// Destructor

CEnumConnections::~CEnumConnections(void)
{
	AfxRelease(m_pObject);
	}

// Interface Map

AfxImplementInterfaceMap(CEnumConnections)
{
	AfxInterface(IEnumConnections)

	AfxBasedUpon(CComObject)
	}

// IEnumConnectionPoints Methods

HRESULT CEnumConnections::Next(ULONG uCount, CONNECTDATA *pList, ULONG *pFetch)
{
	if( pList ) {

		try {
			UINT uFetch = 0;

			BOOL fBreak = FALSE;

			CCriticalGuard Guard(GetGuard());

			if( m_pObject->m_pConList ) {

				CConnectionList &ConList = *m_pObject->m_pConList;

				while( uCount-- ) {

					if( ConList.Failed(m_i) ) {

						fBreak = TRUE;

						break;
						}

					pList[uFetch].pUnk     = ConList[m_i];

					pList[uFetch].dwCookie = DWORD(m_i);

					uFetch++;

					ConList.GetNext(m_i);
					}
				}
			else
				fBreak = TRUE;

			if( pFetch )
				*pFetch = uFetch;

			if( fBreak )
				return S_FALSE;

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

HRESULT CEnumConnections::Skip(ULONG uCount)
{
	try {
		if( m_pObject->m_pConList ) {

			CConnectionList &ConList = *m_pObject->m_pConList;

			while( uCount-- ) {

				if( ConList.Failed(m_i) )
					return S_FALSE;

				ConList.GetNext(m_i);
				}

			return S_OK;
			}

		return S_FALSE;
		}

	AfxStdCatch();
	}

HRESULT CEnumConnections::Reset(void)
{
	try {
		if( m_pObject->m_pConList ) {

			CConnectionList &ConList = *m_pObject->m_pConList;

			m_i = ConList.GetHead();

			return S_OK;
			}

		return S_OK;
		}

	AfxStdCatch();
	}

HRESULT CEnumConnections::Clone(IEnumConnections **ppEnum)
{
	if( ppEnum ) {

		try {
			*ppEnum = NULL;

			*ppEnum = New CEnumConnections(*this);

			return S_OK;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

// Threading

CCriticalSection * CEnumConnections::GetGuard(void)
{
	return m_pObject->GetGuard();
	}

// End of File
