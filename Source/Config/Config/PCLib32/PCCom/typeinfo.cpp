
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Type Info
//

// Constructors

CTypeInfo::CTypeInfo(void)
{
	InitFrom(NULL, FALSE);
	}

CTypeInfo::CTypeInfo(CTypeInfo const &That)
{
	InitFrom(That.m_pITypeInfo, TRUE);
	}

CTypeInfo::CTypeInfo(ITypeInfo *pITypeInfo)
{
	InitFrom(pITypeInfo, TRUE);
	}

CTypeInfo::CTypeInfo(ITypeInfo *pITypeInfo, BOOL fAdd)
{
	InitFrom(pITypeInfo, fAdd);
	}

// Destructors

CTypeInfo::~CTypeInfo(void)
{
	Close();
	}

// Assignment Operators

CTypeInfo const & CTypeInfo::operator = (CTypeInfo const &That)
{
	Close();

	InitFrom(That.m_pITypeInfo, TRUE);

	return *this;
	}

CTypeInfo const & CTypeInfo::operator = (ITypeInfo *pITypeInfo)
{
	Close();

	InitFrom(pITypeInfo, FALSE);

	return *this;
	}

// Conversion

CTypeInfo::operator ITypeInfo * (void) const
{
	return m_pITypeInfo;
	}

CTypeInfo::operator BOOL (void) const
{
	return m_pITypeInfo != NULL;
	}

CTypeInfo::operator ! (void) const
{
	return m_pITypeInfo == NULL;
	}

// Attributes

CGuid CTypeInfo::GetGuid(void)
{
	GetTypeAttr();

	return m_pAttr->guid;
	}

LCID CTypeInfo::GetLcid(void)
{
	GetTypeAttr();

	return m_pAttr->lcid;
	}

WORD CTypeInfo::GetFlags(void)
{
	GetTypeAttr();

	return m_pAttr->wTypeFlags;
	}

TYPEKIND CTypeInfo::GetType(void)
{
	GetTypeAttr();

	return m_pAttr->typekind;
	}

WORD CTypeInfo::GetFuncs(void)
{
	GetTypeAttr();

	return m_pAttr->cFuncs;
	}

WORD CTypeInfo::GetVars(void)
{
	GetTypeAttr();

	return m_pAttr->cVars;
	}

CString CTypeInfo::GetVersion(void)
{
	GetTypeAttr();

	WORD wMaj = m_pAttr->wMajorVerNum;

	WORD wMin = m_pAttr->wMinorVerNum;

	return CPrintf(L"%u.%u", Max(1u, wMaj), wMin);
	}

WORD CTypeInfo::GetInterfaces(void)
{
	GetTypeAttr();

	return m_pAttr->cImplTypes;
	}

CString CTypeInfo::GetName(void)
{
	AfxAssert(m_pITypeInfo);

	BSTR bstr;

	CheckAndThrow(m_pITypeInfo->GetDocumentation( MEMBERID_NIL,
						      &bstr,                
						      NULL,
						      NULL,  
						      NULL
						      ));

	return CBSTR(bstr);
	}

CString CTypeInfo::GetDesc(void)
{
	AfxAssert(m_pITypeInfo);

	BSTR bstr;

	CheckAndThrow(m_pITypeInfo->GetDocumentation( MEMBERID_NIL,
						      NULL,                
						      &bstr,
						      NULL,  
						      NULL
						      ));

	return CBSTR(bstr);
	}

// Type Library

CTypeLib CTypeInfo::GetTypeLib(void)
{
	AfxAssert(m_pITypeInfo);

	ITypeLib *pITypeLib = NULL;

	UINT uIndex = 0;

	CheckAndThrow(m_pITypeInfo->GetContainingTypeLib(&pITypeLib, &uIndex));
	
	AfxTouch(uIndex);

	return CTypeLib(pITypeLib, FALSE);
	}

// Dispatch Helpers

HRESULT CTypeInfo::GetIDsOfNames( OLECHAR ** szNames, 
				  UINT       cbNames, 
				  MEMBERID * pMemId
				  )
{
	AfxAssert(m_pITypeInfo);

	return m_pITypeInfo->GetIDsOfNames( szNames, 
					    cbNames, 
					    pMemId
					    );
	}

HRESULT CTypeInfo::Invoke( void       *	pvInstance,     
			   MEMBERID	memid,           
			   WORD		wFlags,    
			   DISPPARAMS *	pDispParams,  
			   VARIANT    *	pVarResult,  
			   EXCEPINFO  *	pExcepInfo,  
			   UINT       *	puArgErr
			   )
{
	AfxAssert(m_pITypeInfo);

	AfxAssert(pvInstance);

	return m_pITypeInfo->Invoke( pvInstance,
				     memid,
				     wFlags,
				     pDispParams,
				     pVarResult,
				     pExcepInfo,
				     puArgErr
				     );
	}

// Attributes

void CTypeInfo::GetTypeAttr(void)
{
	if( !m_pAttr ) {
	
		AfxAssert(m_pITypeInfo);

		CheckAndThrow(m_pITypeInfo->GetTypeAttr(&m_pAttr));
		}
	}

void CTypeInfo::FreeTypeAttr(void)
{
	if( m_pAttr ) {

		AfxAssert(m_pITypeInfo);
		
		m_pITypeInfo->ReleaseTypeAttr(m_pAttr);

		m_pAttr = NULL;
		}
	}

// Implementation

void CTypeInfo::InitFrom(ITypeInfo *pITypeInfo, BOOL fAdd)
{
	m_pAttr = NULL;

	if( fAdd && pITypeInfo ) {

		m_pITypeInfo = pITypeInfo;

		m_pITypeInfo->AddRef();
		
		return;
		}

	m_pITypeInfo = pITypeInfo;
	}

void CTypeInfo::Close(void)
{
	FreeTypeAttr();

	AfxRelease(m_pITypeInfo);

	m_pITypeInfo = NULL;
	}

// End of File
