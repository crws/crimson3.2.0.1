
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Type Library Enumerator
//

class CTypeLibEnum
{
	public:
		// Constructor
		CTypeLibEnum(CModule *pModule);

		// Attributes
		CTypeLib GetLib(UINT n);

	protected:
		// Data Members
		CModule       *m_pModule;
		CArray <UINT>  m_LibArray;

		// Callback
		static BOOL CallBack(HMODULE, LPCTSTR, LPTSTR, LONG_PTR);
	};

//////////////////////////////////////////////////////////////////////////
//
// Module Registration Helper
//

// Constructor

CComRegHelper::CComRegHelper(CModule *pModule)
{
	m_pModule = pModule;
	}

// Destructor

CComRegHelper::~CComRegHelper(void)
{
	}

// Components

BOOL CComRegHelper::RegComponents(void)
{
	CModule::CGuidMap const &Map = m_pModule->m_GuidMap;
	
	INDEX i = Map.GetHead();
	
	while( !Map.Failed(i) ) {

		ICLASS const &Class = Map[i];

		AfxAssert(Class->IsKindOf(AfxRuntimeClass(CComObject)));

		CComObject *pObject = AfxNewObject(CComObject, Class);
				
		BOOL        fResult = RegComponent(*pObject);
		
		pObject->Release();

		if( fResult ) {

			Map.GetNext(i);

			continue;
			}

		return FALSE;
		}
	
	return TRUE;
	}

BOOL CComRegHelper::UnregComponents(void)
{
	CModule::CGuidMap const &Map = m_pModule->m_GuidMap;
	
	INDEX i = Map.GetHead();
	
	while( !Map.Failed(i) ) {

		ICLASS const &Class = Map[i];

		AfxAssert(Class->IsKindOf(AfxRuntimeClass(CComObject)));

		CComObject *pObject = AfxNewObject(CComObject, Class);
				
		BOOL        fResult = UnregComponent(*pObject);
		
		pObject->Release();
		
		if( fResult ) {

			Map.GetNext(i);

			continue;
			}

		return FALSE;
		}
	
	return TRUE;
	}

// Interfaces

BOOL CComRegHelper::RegInterface(REFIID iid, CString Name, REFGUID Proxy)
{
	return RegInterface(iid, Name, CGuid(), CString(), Proxy);
	}

BOOL CComRegHelper::RegDispInterface(REFIID iid, CString Name, REFGUID LibID, CString Ver)
{
	static CGuid Proxy = L"{00020424-0000-0000-C000-000000000046}";

	return RegInterface(iid, Name, LibID, Ver, Proxy);
	}

BOOL CComRegHelper::RegDispInterface(REFIID iid, CString Name, REFGUID LibID, CString Ver, REFGUID Proxy)
{
	return RegInterface(iid, Name, LibID, Ver, Proxy);
	}

BOOL CComRegHelper::UnregInterface(REFIID iid)
{
	CString Text = CGuid(iid).GetAsText();

	CRegKey Key  = CRegKey(HKEY_CLASSES_ROOT);

	if( Key.MoveTo(L"Interface") ) {

		CRegKey SubKey = Key.Open(Text);
		
		UINT    uCount = DecrementRef(SubKey);

		if( uCount < NOTHING ) {
		
			if( uCount ) {

				return TRUE;
				}
			
			return Key.Delete(Text);
			}

		return FALSE;
		}

	return FALSE;
	}

// Type Libs

BOOL CComRegHelper::RegTypeLibs(void)
{
	return RegTypeLibFromFile() && RegTypeLibFromResource();
	}

BOOL CComRegHelper::UnregTypeLibs(void)
{
	return UnregTypeLibFromFile() && UnregTypeLibFromResource();
	}

// Proxy Servers

BOOL CComRegHelper::RegProxy(CGuid &Clsid, CFilename &Filename, CString Name)
{
	CRegKey Key = CRegKey(HKEY_CLASSES_ROOT);

	if( Key.MoveTo(L"CLSID") && Key.MoveTo(Clsid.GetAsText()) ) {

		CKeyMap Map;

		Map.Insert(L"",                 Name);

		Map.Insert(L"InprocServer32\\", Filename);

		return Key.WriteMap(Map);
		}

	return FALSE;
	}

BOOL CComRegHelper::UnregProxy(CGuid &Clsid)
{
	CRegKey Key = CRegKey(HKEY_CLASSES_ROOT);

	Key.MoveTo(L"CLSID");
	
	Key.Delete(Clsid.GetAsText());

	return TRUE;
	}

// Component Helpers

BOOL CComRegHelper::RegComponent(CComObject &Object)
{
	CComRegInfo Info;

	Object.BuildRegInfo(Info);

	CString ClassID = AfxObjectClass(Object)->GetClassGuid();

	CRegKey Key     = CRegKey(HKEY_CLASSES_ROOT);

	CKeyMap Map;

	Map.Insert(Info.m_ProgID + L"\\",         Info.m_Readable);

	Map.Insert(Info.m_ProgID + L"\\CLSID\\",  ClassID);
	
	Map.Insert(Info.m_VerInd + L"\\",         Info.m_Readable);
	
	Map.Insert(Info.m_VerInd + L"\\CLSID\\",  ClassID);

	Map.Insert(Info.m_VerInd + L"\\CurVer\\", Info.m_ProgID);

	if( Key.WriteMap(Map) ) {

		if( Key.MoveTo(L"CLSID") && Key.MoveTo(ClassID) ) {
			
			if( Key.SetValue(L"", Info.m_Readable) ) {

				Map.Empty();

				Object.BuildRegKeys(Map);

				if( Key.WriteMap(Map) ) {

					if( !RegCategories(Object) ) {
						
						return FALSE;
						}
					
					if( !Object.CustomRegistration() ) {

						return FALSE;
						}

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CComRegHelper::UnregComponent(CComObject &Object)
{
	UnregCategories(Object);
	
	CComRegInfo Info;

	Object.BuildRegInfo(Info);

	CString ClassID = AfxObjectClass(Object)->GetClassGuid();

	CRegKey Key     = CRegKey(HKEY_CLASSES_ROOT);

	Key.Delete(Info.m_ProgID);

	Key.Delete(Info.m_VerInd);

	Key.MoveTo(L"CLSID");

	Key.Delete(ClassID);

	return TRUE;
	}

// Component Categories

BOOL CComRegHelper::RegCategories(CComObject &Object)
{
	ICatRegister *pIReg = NULL;
	
	for( UINT i = 0; ; i ++ ) {

		CComCategory Category;
		
		if( Object.EnumCategories(i, Category) ) {

			if( !i ) {

				HRESULT hr = CoCreateInstance( CLSID_StdComponentCategoriesMgr,
							       NULL,
							       CLSCTX_INPROC_SERVER, 
							       IID_ICatRegister,
							       (void **) &pIReg
							       );

				if( FAILED(hr) ) {

					return FALSE;
					}
				}

			CATEGORYINFO Info;

			Info.catid = Category.m_cid; 
			
			Info.lcid  = MAKELCID( MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), 
					       SORT_DEFAULT
					       );
	
			wcscpy(Info.szDescription, Category.m_Desc);

			HRESULT hr = pIReg->RegisterCategories(1, &Info);
			
			if( SUCCEEDED(hr) ) {

				CATID Ref[1];
			
				Ref[0] = CATID(Category.m_cid);

				hr = pIReg->RegisterClassImplCategories( AfxObjectClass(Object)->GetClassGuid(),
									 1,
									 Ref
									 );
			
				if( SUCCEEDED(hr) ) {

					continue;
					}
				}			
				
			pIReg->Release();

			return FALSE;
			}

		break;
		}

	if( pIReg ) {

		pIReg->Release();
		}
	
	return TRUE;
	}

BOOL CComRegHelper::UnregCategories(CComObject &Object)
{
	ICatRegister *pIReg = NULL;
	
	for( UINT i = 0; ; i ++ ) {

		GUID Guid = AfxObjectClass(Object)->GetClassGuid();

		CComCategory Category;
		
		if( Object.EnumCategories(i, Category) ) {

			if( !i ) {

				HRESULT hr = CoCreateInstance( CLSID_StdComponentCategoriesMgr,
							       NULL,
							       CLSCTX_INPROC_SERVER, 
							       IID_ICatRegister,
							       (void **) &pIReg
							       );

				if( FAILED(hr) ) {

					return FALSE;
					}
				}

			CATID Ref[1];
		
			Ref[0] = CATID(Category.m_cid);

			HRESULT hr = pIReg->UnRegisterClassImplCategories( Guid,
									   1,
									   Ref
									   );

			if( true || SUCCEEDED(hr) ) {

				if( Category.m_fOwner ) {
					
					hr = pIReg->UnRegisterCategories(1, Ref);

					if( SUCCEEDED(hr) ) {

						continue;
						}

					break;
					}

				continue;
				}
				
			pIReg->Release();

			return FALSE;
			}

		break;
		}

	if( pIReg ) {

		pIReg->Release();
		}
	
	return TRUE;
	}

// Interface Helpers

BOOL CComRegHelper::RegInterface(CGuid IID, CString Name, CGuid LibID, CString Ver, CGuid Proxy)
{
	CRegKey Key = CRegKey(HKEY_CLASSES_ROOT);

	if( Key.MoveTo(L"Interface") && Key.MoveTo(IID.GetAsText()) ) {

		UINT uCount = IncrementRef(Key);
		
		if( uCount == 1 ) {

			CKeyMap Map;

			Map.Insert(L"", Name);

			if( !!Proxy ) {
			
				Map.Insert(L"ProxyStubClsid\\",   Proxy.GetAsText());
			
				Map.Insert(L"ProxyStubClsid32\\", Proxy.GetAsText());
				}

			if( !!LibID ) {

				Map.Insert(L"TypeLib\\",        LibID.GetAsText());
			
				Map.Insert(L"TypeLib\\Version", Ver);
				}
			
			return Key.WriteMap(Map);
			}
			
		return uCount < NOTHING;
		}

	return FALSE;
	}

UINT CComRegHelper::IncrementRef(CRegKey &Key)
{
	UINT uCount = watoi(Key.GetValueAsString(L"Ref"));

	uCount++;

	if( Key.SetValue(L"Ref", CPrintf(L"%d", uCount)) ) {

		return uCount;
		}
	
	return NOTHING;
	}	

UINT CComRegHelper::DecrementRef(CRegKey &Key)
{
	UINT uCount = watoi(Key.GetValueAsString(L"Ref"));

	uCount--;
	
	if( Key.SetValue(L"Ref", CPrintf(L"%d", uCount)) ) {

		return uCount + 1;
		}

	return NOTHING;
	}	

// Type Lib Helpers

BOOL CComRegHelper::RegTypeLibFromFile(void)
{
	CFilename Name  = m_pModule->GetFilename().WithType(L"tlb");

	HANDLE    hFile = CreateFile( Name,
				      GENERIC_READ,
				      FILE_SHARE_READ,
				      NULL,
				      OPEN_EXISTING,
				      0,
				      NULL
				      );

	if( hFile != INVALID_HANDLE_VALUE ) {

		CloseHandle(hFile);
		
		CTypeLib Lib;

		Lib.Load(Name);
		
		return RegTypeLib(Lib);
		}
	
	return TRUE;
	}

BOOL CComRegHelper::RegTypeLibFromResource(void)
{
	CTypeLibEnum Enum(m_pModule);

	for( UINT i = 0;; i ++ ) {
	
		CTypeLib Lib = Enum.GetLib(i);

		if( Lib ) {

			if( !RegTypeLib(Lib) ) {

				return FALSE;
				}

			continue;
			}
		
		break;
		}

	return TRUE;
	}

BOOL CComRegHelper::RegTypeLib(CTypeLib &Lib)
{
	CRegKey Key = CRegKey(HKEY_CLASSES_ROOT);

	if( !Key.MoveTo(L"TypeLib") ) {

		return FALSE;
		}

	if( !Key.MoveTo(Lib.GetGuid().GetAsText()) ) {

		return FALSE;
		}

	if( !Key.MoveTo(Lib.GetVersion()) ) {

		return FALSE;
		}
	
	CKeyMap Map;

	CFilename File = Lib.GetFilename();

	CFilename Help = File.GetDrive() + File.GetBarePath();
	
	Map.Insert(L"",           Lib.GetDesc());

	Map.Insert(L"0\\win32\\", Lib.GetFilename());

	Map.Insert(L"Flags\\",    L"0");

	Map.Insert(L"HelpDir\\",  Help);
	
	if( Key.WriteMap(Map) ) {

		if( !RegTypeLibComponents(Lib) ) {

			return FALSE;
			}

		if( !RegTypeLibInterfaces(Lib) ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CComRegHelper::RegTypeLibComponents(CTypeLib &Lib)
{
	CString Company = m_pModule->GetRegCompany();

	Company.StripAll();
	
	for( UINT n = 0;; n ++ ) {
	
		CTypeInfo Info = Lib.EnumComponents(n);

		if( Info ) {
			
			CRegKey Key = CRegKey(HKEY_CLASSES_ROOT);
			
			CString VerInd = Company + L'.' + Info.GetName();

			CString ProgID = VerInd  + L'.' + Info.GetVersion();

			CKeyMap Map;

			Map.Insert(ProgID + L"\\",         Info.GetDesc());
			
			Map.Insert(ProgID + L"\\CLSID\\",  Info.GetGuid().GetAsText());
			
			Map.Insert(VerInd + L"\\",         Info.GetDesc());

			Map.Insert(VerInd + L"\\CurVer\\", ProgID);
			
			if( Key.WriteMap(Map) ) {

				if( !Key.MoveTo(L"CLSID") ) {

					return FALSE;
					}

				if( !Key.MoveTo(Info.GetGuid().GetAsText()) ) {

					return FALSE;
					}

				Map.Empty();

				Map.Insert(L"",                               Info.GetDesc());

				Map.Insert(L"ProgID\\",                       ProgID);

				Map.Insert(L"VersionIndependentProgID\\",     VerInd);
		
				Map.Insert(L"InprocServer32\\",               m_pModule->GetFilename());
		
				Map.Insert(L"InprocServer32\\ThreadingModel", L"Both");

				Map.Insert(L"TypeLib\\",		      Lib.GetGuid().GetAsText());
				
				if( Key.WriteMap(Map) ) {

					continue;
					}

				return FALSE;
				}
			
			return FALSE;
			}
		
		return TRUE;
		}
	}

BOOL CComRegHelper::RegTypeLibInterfaces(CTypeLib &Lib)
{
	for( UINT n = 0;; n ++ ) {
	
		CTypeInfo Info = Lib.EnumInterfaces(n);

		if( Info ) {
			
			static CGuid NullProxy;
	
			if( !RegInterface( Info.GetGuid(),
					   Info.GetName(),
   					   NullProxy
					   ) ) {
				
				return FALSE;
				}

			continue;
			}
		
		break;
		}

	for( n = 0;; n ++ ) {
	
		CTypeInfo Info = Lib.EnumDispatchs(n);

		if( Info ) {
			
			if( !RegDispInterface( Info.GetGuid(),
					       Info.GetName(),
					       Lib.GetGuid(),
					       Lib.GetVersion()
					       ) ) {
				
				return FALSE;
				}

			continue;
			}
		
		break;
		}

	return TRUE;
	}

BOOL CComRegHelper::UnregTypeLibFromFile(void)
{
	CFilename Name  = m_pModule->GetFilename().WithType(L"tlb");

	HANDLE    hFile = CreateFile( Name,
				      GENERIC_READ,
				      FILE_SHARE_READ,
				      NULL,
				      OPEN_EXISTING,
				      0,
				      NULL
				      );

	if( hFile != INVALID_HANDLE_VALUE ) {

		CloseHandle(hFile);
		
		CTypeLib Lib;

		Lib.Load(Name);
		
		return UnregTypeLib(Lib);
		}
	
	return TRUE;
	}

BOOL CComRegHelper::UnregTypeLibFromResource(void)
{
	CTypeLibEnum Enum(m_pModule);

	for( UINT i = 0;; i ++ ) {
	
		CTypeLib Lib = Enum.GetLib(i);

		if( Lib ) {

			UnregTypeLib(Lib);

			continue;
			}
		
		break;
		}

	return TRUE;
	}

BOOL CComRegHelper::UnregTypeLib(CTypeLib &Lib)
{
	CRegKey Key = CRegKey(HKEY_CLASSES_ROOT);

	if( Key.MoveTo(L"TypeLib") ) {

		Key.Delete(Lib.GetGuid().GetAsText());

		UnregTypeLibComponents(Lib);
	
		UnregTypeLibInterfaces(Lib);

		return TRUE;
		}

	return FALSE;
	}

BOOL CComRegHelper::UnregTypeLibComponents(CTypeLib &Lib)
{
	CString Company = m_pModule->GetRegCompany();

	Company.StripAll();
	
	for( UINT n = 0;; n ++ ) {
	
		CTypeInfo Info = Lib.EnumComponents(n);

		if( Info ) {
			
			CRegKey Key = CRegKey(HKEY_CLASSES_ROOT);
			
			Key.Delete(Company +  L'.' + Info.GetName() + L'.' + Info.GetVersion());
			
			Key.Delete(Company +  L'.' + Info.GetName());
			
			Key.MoveTo(L"CLSID");

			Key.Delete(Info.GetGuid().GetAsText());
			}
		else
			break;
		}
		
	return TRUE;
	}

BOOL CComRegHelper::UnregTypeLibInterfaces(CTypeLib &Lib)
{
	for( UINT n = 0;; n ++ ) {
	
		CTypeInfo Info = Lib.EnumInterfaces(n);

		if( Info ) {
			
			UnregInterface(Info.GetGuid());
			
			continue;
			}
		
		break;
		}

	for( n = 0;; n ++ ) {
	
		CTypeInfo Info = Lib.EnumDispatchs(n);

		if( Info ) {
			
			UnregInterface(Info.GetGuid());

			continue;
			}
		
		break;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Type Library Enumerator
//

// Constructor

CTypeLibEnum::CTypeLibEnum(CModule *pModule)
{
	m_pModule = pModule;

	m_LibArray.Expand(32);

	EnumResourceNames( m_pModule->GetInstance(), 
			   L"TYPELIB",
			   (ENUMRESNAMEPROC) CallBack,
			   LONG_PTR(this)
			   );

	m_LibArray.Compress();
	}

// Attributes

CTypeLib CTypeLibEnum::GetLib(UINT n)
{
	CTypeLib Lib;
	
	if( n < m_LibArray.GetCount() ) {

		UINT uID = m_LibArray[n];
		
		Lib.Load(m_pModule->GetFilename(), uID);
		}
	
	return Lib;
	}

// Callback

BOOL CTypeLibEnum::CallBack(HMODULE hModule, LPCTSTR szType, LPTSTR szName, LONG_PTR pData)
{
	AfxAssert(!HIWORD(szName));

	CTypeLibEnum *pEnum = (CTypeLibEnum *) pData;

	pEnum->m_LibArray.Append(LOWORD(szName));
	
	return TRUE;
	}

// End of File
