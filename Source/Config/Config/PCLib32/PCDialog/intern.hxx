
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//								
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_BUTTON_ABORT        0x4000
#define IDS_BUTTON_ALL          0x4001
#define IDS_BUTTON_CANCEL       0x4002
#define IDS_BUTTON_HELP         0x4003
#define IDS_BUTTON_IGNORE       0x4004
#define IDS_BUTTON_NO           0x4005
#define IDS_BUTTON_OK           0x4006
#define IDS_BUTTON_RETRY        0x4007
#define IDS_BUTTON_YES          0x4008
#define IDS_DIALOG_CLOSE        0x4009
#define IDS_DIALOG_MOVE         0x400A
#define IDS_DO_NOT_SHOW_THIS    0x400B
#define IDS_FILE_ALL            0x400C
#define IDS_FILE_OPEN           0x400D
#define IDS_FILE_SAVE           0x400E
#define IDS_PAGE_DISABLED       0x400F

//////////////////////////////////////////////////////////////////////////
//
// Tabbed Dialog Identifiers
//

#define	IDC_TAB_CTRL		30000
#define	IDC_TAB_PLACE		30001
#define	IDC_TAB_NEXT		30002
#define	IDC_TAB_PREV		30003

//////////////////////////////////////////////////////////////////////////
//
// Additional Button IDs
//

#define	IDALL			10

// End of File

#endif
