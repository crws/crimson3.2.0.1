
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// File Dialog Base Class
//

// Runtime Class

AfxImplementRuntimeClass(CFileDialog, CCommonDialog);

// Static Data

WCHAR CFileDialog::m_sLastPath[_MAX_PATH] = { 0 };
	
// Constructor

CFileDialog::CFileDialog(void)
{
	memset(&m_Data, 0, sizeof(m_Data));

	m_Data.lStructSize 	 = sizeof(m_Data);

	m_Data.hwndOwner   	 = NULL;
	
	m_Data.hInstance	 = NULL;
	
	m_Data.lpstrFilter	 = m_sMask;
	
	m_Data.lpstrCustomFilter = NULL;
	
	m_Data.nMaxCustFilter	 = 0;
	
	m_Data.nFilterIndex	 = 1;
	
	m_Data.lpstrFile	 = m_sFile;
	
	m_Data.lpstrFileTitle	 = NULL;
	
	m_Data.nMaxFile		 = sizeof(m_sFile);
	
	m_Data.nMaxFileTitle	 = 0;
	
	m_Data.lpstrInitialDir	 = m_sLastPath;
	
	m_Data.lpstrTitle	 = NULL;
	
	m_Data.Flags		 = OFN_EXPLORER | OFN_SHAREAWARE;
	
	m_Data.nFileOffset	 = 0;
	
	m_Data.nFileExtension	 = 0;
	
	m_Data.lpstrDefExt	 = NULL;
	
	m_Data.lCustData	 = NULL;
	
	m_Data.lpfnHook		 = NULL;
	
	m_Data.lpTemplateName	 = NULL;

	m_Data.pvReserved        = NULL;

	m_Data.dwReserved        = NULL;

	m_Data.FlagsEx		 = 0;
	
	m_Filter = CString(IDS_FILE_ALL);

	m_sFile[0] = 0;
	}
	
// Attributes

CFilename CFileDialog::GetFilename(void) const
{
	return m_sFile;
	}

// Operations

void CFileDialog::SetFilename(PCTXT pFile)
{
	AfxValidateStringPtr(pFile);
	
	wstrcpy(m_sFile, pFile);
	}

void CFileDialog::SetFilter(PCTXT pFilter)
{
	AfxValidateStringPtr(pFilter);
	
	m_Filter = pFilter;
	}
		
// Last Path Management

void CFileDialog::SetLastPath(PCTXT pPath)
{
	wstrcpy(m_sLastPath, pPath);
	}

void CFileDialog::LoadLastPath(PCTXT pType)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"PCDialog");

	Reg.MoveTo(L"Last Path");

	wstrcpy(m_sLastPath, Reg.GetValueAsString(pType));
	}

void CFileDialog::LoadLastPath(PCTXT pType, PCTXT pDefault)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"PCDialog");

	Reg.MoveTo(L"Last Path");

	wstrcpy(m_sLastPath, Reg.GetValue(pType, pDefault));
	}

void CFileDialog::SaveLastPath(PCTXT pType)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"PCDialog");

	Reg.MoveTo(L"Last Path");

	Reg.SetValue(pType, m_sLastPath);
	}

// Implementation

void CFileDialog::UpdateData(CWnd &Parent)
{
	m_Data.hwndOwner = Parent.GetHandle();

	wstrcpy(m_sMask, m_Filter + L'|');
	
	BOOL fFound = FALSE;
	
	for( int nPos = 0; m_sMask[nPos]; nPos++ ) {
	
		TCHAR &cData = m_sMask[nPos];
		
		if( cData == '|' ) {
			
			if( !fFound ) {
			
				m_Data.lpstrDefExt = wstrchr(&cData, '.') + 1;
				
				fFound = TRUE;
				}
		
			cData = 0;
			}
		}
		
	m_Data.lpstrTitle = m_Caption;
	}

void CFileDialog::UpdatePath(void)
{
	CFilename File = m_sFile;

	wstrcpy(m_sLastPath, File.GetDrive() + File.GetBarePath());
	}
	
//////////////////////////////////////////////////////////////////////////
//
// Open File Dialog
//

// Runtime Class

AfxImplementRuntimeClass(COpenFileDialog, CFileDialog);
		
// Constructor

COpenFileDialog::COpenFileDialog(void)
{
	m_Caption    = CString(IDS_FILE_OPEN);

	m_Data.Flags = m_Data.Flags | OFN_HIDEREADONLY;
	}
		
// Dialog Operations

UINT COpenFileDialog::Execute(CWnd &Parent)
{
	UpdateData(Parent);
	
	AfxInstallHook(this);
	
	if( !GetOpenFileName(&m_Data) ) {
	
		AfxRemoveHook();
		
		return FALSE;
		}

	afxMainWnd->UpdateWindow();

	UpdatePath();

	return TRUE;
	}

UINT COpenFileDialog::Execute(void)
{
	return CDialog::Execute();
	}

// Operations

void COpenFileDialog::AllowReadOnly(void)
{
	m_Data.Flags &= ~OFN_HIDEREADONLY;
	}

// Attributes

BOOL COpenFileDialog::IsReadOnly(void) const
{
	return (m_Data.Flags & OFN_READONLY) ? TRUE : FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Save File Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSaveFileDialog, CFileDialog);
		
// Constructor

CSaveFileDialog::CSaveFileDialog(void)
{
	m_Caption    = CString(IDS_FILE_SAVE);

	m_Data.Flags = m_Data.Flags | OFN_HIDEREADONLY;
	}
		
// Dialog Operations

UINT CSaveFileDialog::Execute(CWnd &Parent)
{
	UpdateData(Parent);
	
	AfxInstallHook(this);
	
	if( !GetSaveFileName(&m_Data) ) {
	
		AfxRemoveHook();
		
		return FALSE;
		}

	afxMainWnd->UpdateWindow();

	UpdatePath();

	return TRUE;
	}

UINT CSaveFileDialog::Execute(void)
{
	return CDialog::Execute();
	}

UINT CSaveFileDialog::ExecAndCheck(CWnd &Parent)
{
	for(;;) {

		if( Execute(Parent) ) {

			switch( Parent.CanOverwrite(m_sFile) ) {

				case IDNO:

					continue;

				case IDCANCEL:

					return FALSE;
				}

			Parent.UpdateWindow();

			return TRUE;
			}

		return FALSE;
		}
	}

UINT CSaveFileDialog::ExecAndCheck(void)
{
	CWnd &Wnd  = CWnd::GetActiveWindow();

	UINT uCode = ExecAndCheck(Wnd);

	return uCode;
	}

// End of File
