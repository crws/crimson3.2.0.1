
#include "intern.hpp"

// Rewrite for new style.

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Button Artist
//

// Static Data

CButtonArtist * CButtonArtist::m_pThis = NULL;
		
// Object Location

CButtonArtist * CButtonArtist::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructor

CButtonArtist::CButtonArtist(void)
{
	AfxAssert(m_pThis == NULL);
	
	m_pHead = m_pTail = NULL;
	
	m_pThis = this;
	}
		
// Destructor

CButtonArtist::~CButtonArtist(void)
{
	AfxAssert(m_pThis == this);

	CBitmapInfo *pScan = m_pHead;
	
	while( pScan ) {
	
		CBitmapInfo *pNext = pScan->m_pNext;
	
		delete pScan->m_pBitmap;
		
		delete pScan;
		
		pScan = pNext;
		}
		
	m_pThis = NULL;
	}
		
// Bitmap Management

void CButtonArtist::AppendBitmap(UINT uID, CButtonBitmap *pBitmap)
{
	CBitmapInfo *pInfo = New CBitmapInfo;
	
	pInfo->m_uID     = uID;
	
	pInfo->m_pBitmap = pBitmap;
	
	AfxListAppend(m_pHead, m_pTail, pInfo, m_pNext, m_pPrev);
	}

void CButtonArtist::RemoveBitmap(UINT uID)
{
	CBitmapInfo *pScan = m_pHead;
	
	while( pScan ) {
	
		if( pScan->m_uID == uID ) {
	
			AfxListRemove(m_pHead, m_pTail, pScan, m_pNext, m_pPrev);

			delete pScan->m_pBitmap;

			delete pScan;

			break;
			}

		pScan = pScan->m_pNext;
		}
	}
		
// Size Adjustment

int CButtonArtist::Adjust(CDC &DC, DWORD Image, CString Text)
{
	if( !Text.IsEmpty() ) {

		int cx = 4;

		if( Text.Right(1) == L"!" ) {

			Text = Text.Left(Text.GetLength() - 1);

			DC.Select(afxFont(Marlett1));

			CSize Size = DC.GetTextExtent(L"\x036");
			
			cx += Size.cx;

			DC.Deselect();

			if( Image < NOTHING && Text.IsEmpty() ) {

				cx -= 8;
				}
			}

		if( !Text.IsEmpty() ) {

			cx += DC.GetTextExtent(Text).cx;
			}

		return cx;
		}

	return 0;
	}
		
// Button Drawing

void CButtonArtist::Draw(CDC &DC, CRect Rect, DWORD Image, CString Text, UINT uState)
{
	CBitmapInfo *pInfo = NULL;

	if( Image < NOTHING ) {

		pInfo = FindInfo(HIWORD(Image));

		if( !pInfo ) {
		
			DC.FillRect(Rect, afxBrush(RED));
			
			return;
			}
		}

	if( uState & MF_DISABLED ) {

		if( !(uState & MF_POPUP) ) {

			if( uState & MF_CHECKED ) {

				DC.FrameRect(Rect--, afxBrush(3dDkShadow));

				DC.GradVert (Rect--, afxColor(Blue1), afxColor(Blue2));
				}
			else
				Rect -= 2;
			}

		uState = 1;
		}
	else {
		if( uState & MF_GRAYED ) {

			if( !(uState & MF_POPUP) ) {

				DC.FrameRect(Rect--, afxBrush(3dDkShadow));

				DC.GradVert (Rect--, afxColor(Blue3), afxColor(Blue1));
				}

			uState = 0;
			}
		else {
			if( uState & MF_HILITE ) {
			
				if( !(uState & MF_POPUP) ) {

					if( uState & MF_PRESSED ) {

						DC.FrameRect(Rect--, afxBrush(3dDkShadow));

						DC.GradVert (Rect--, afxColor(Orange1), afxColor(Orange2));
						}
					else {
						DC.FrameRect(Rect--, afxBrush(3dDkShadow));
						
						DC.GradVert (Rect--, afxColor(Orange3), afxColor(Orange2));
						}
					}
					
				uState = 2;
				}
			else {
				if( !(uState & MF_POPUP) ) {

					if( uState & MF_CHECKED ) {

						DC.FrameRect(Rect--, afxBrush(3dDkShadow));

						DC.GradVert (Rect--, afxColor(Orange2), afxColor(Orange1));
						}
					else
						Rect -= 2;
					}
						
				uState = 0;
				}
			}
		}
			
	if( !Text.IsEmpty() ) {

		DC.SetBkMode(TRANSPARENT);

		if( uState == 1 ) {

			DC.SetTextColor(afxColor(3dShadow));
			}
		else
			DC.SetTextColor(afxColor(BLACK));

		if( Text.Right(1) == L"!" ) {

			Text = Text.Left(Text.GetLength() - 1);

			DC.Select(afxFont(Marlett1));

			CSize Size = DC.GetTextExtent(L"\x036");

			int xp;
			
			int yp = Rect.top + (Rect.cy() - Size.cy) / 2;

			if( !pInfo && Text.IsEmpty() ) {

				xp = Rect.right - 6 - Size.cx / 2;
				}
			else {
				xp = Rect.right - 6 - Size.cx / 2;

				Rect.right -= 9;
				}

			DC.TextOut(xp, yp, L"\x036");

			DC.Deselect();
			}

		if( !Text.IsEmpty() ) {

			CSize Size = DC.GetTextExtent(Text);

			int xp = Rect.right - Size.cx - 2;
			
			int yp = Rect.top   + (Rect.cy() - Size.cy) / 2;

			DC.TextOut(xp, yp, Text);

			Rect.right -= Size.cx;

			Rect.right -= 2;
			}
		}

	if( pInfo ) {
	
		pInfo->m_pBitmap->Draw(DC, Rect, LOWORD(Image), uState);
		}
	}

// Info Location

CButtonArtist::CBitmapInfo * CButtonArtist::FindInfo(UINT uID)
{
	CBitmapInfo *pScan = m_pTail;
	
	while( pScan ) {
	
		if( pScan->m_uID == uID ) {

			return pScan;
			}

		pScan = pScan->m_pPrev;
		}
		
	return NULL;
	}

// End of File
