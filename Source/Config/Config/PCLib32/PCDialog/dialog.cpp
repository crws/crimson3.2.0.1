
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dialog Base Class
//

// Runtime Class

AfxImplementRuntimeClass(CDialog, CWnd);

// Static Data

CDialog * CDialog::m_pHead = NULL;

CDialog * CDialog::m_pTail = NULL;
		
// Constructor

CDialog::CDialog(void)
{
	AfxInstallDlgHook();

	m_nPadTop     = 0;

	m_fTranslate  = TRUE;

	m_fModeless   = FALSE;

	m_fAutoDelete = FALSE;

	m_fLightBack  = afxWin2K ? FALSE : TRUE;
	
	m_fDone       = FALSE;

	m_Accel.Create(L"DialogAccel");

	CalculateFontInfo();
	}

// Destructor

CDialog::~CDialog(void)
{
	AfxRemoveDlgHook();

	if( m_fModeless ) {

		ModelessListRemove();
		
		return;
		}
	}

// Dialog Operations

BOOL CDialog::Create(CWnd &Parent)
{
	AfxAssert(FALSE);

	return FALSE;
	}

UINT CDialog::Execute(CWnd &Parent)
{
	CWnd &Top   = GetActiveWindow();
	
	CWnd &Focus = GetFocus();

	if( Create(Parent) ) {
	
		SetWindowStyle(DS_MODALFRAME, DS_MODALFRAME);

		UINT uCode = ModalLoop(Top);
		
		Focus.SetFocus();

		DestroyWindow(TRUE);

		return uCode;
		}

	return 0;
	}

UINT CDialog::Execute(void)
{
	CWnd &Wnd  = CWnd::GetActiveWindow();

	UINT uCode = Execute(Wnd);

	Wnd.UpdateWindow();

	return uCode;
	}

// Modal Operation

UINT CDialog::ModalLoop(CWnd &Parent)
{
	AfxAssert(IsWindow());
	
	if( !m_fDone ) {
			
		int nWait = afxThread->SuspendWaitMode();

		if( !IsWindowVisible() ) {
		
			ShowWindow(SW_SHOW);

			SetFocus();
			}
	
		Parent.EnableWindow(FALSE);

		while( !m_fDone ) {
	
			MSG Message;
	
			GetMessage(&Message, NULL, NULL, NULL);
			
			try {
				switch( Message.message ) {

					case WM_SYSKEYDOWN:
					case WM_SYSKEYUP:
					case WM_KEYDOWN:
					case WM_KEYUP:
						
						if( RouteAccelerator(Message) ) {

							continue;
							}
						break;
					}

				if( !Translate(Message) ) {
				
					if( !m_fTranslate || !IsDialogMessage(Message) ) {
				
						TranslateMessage(&Message);
			
						DispatchMessage(&Message);
						}
					}
				}
				
			catch(CException &) {
			
				AfxTrace(L"ERROR: Exception in dialog ModalLoop\n");
				
				m_fDone = TRUE;
				}
			}
	
		AfxNull(CWnd).SetFocus();

		Parent.EnableWindow(TRUE);
	
		ShowWindow(SW_HIDE);
	
		afxThread->RestoreWaitMode(nWait);
		}

	return m_uCode;
	}

UINT CDialog::SemiModal(CWnd *pBefore, CWnd *pUpdate)
{
	AfxAssert(IsWindow());
	
	if( !m_fDone ) {
			
		int nWait = afxThread->SuspendWaitMode();

		if( !IsWindowVisible() ) {
		
			ShowWindow(SW_SHOW);

			SetFocus();
			}

		if( pBefore ) {

			pBefore->EnableWindow(FALSE);
			}

		while( !m_fDone ) {
	
			MSG Message;

			if( pUpdate ) {

				UINT uFlags = PM_NOREMOVE;

				if( !PeekMessage(&Message, NULL, NULL, NULL, uFlags) ) {

					MSG     Idle    = { pUpdate->GetHandle(), WM_GOINGIDLE, 0, 0 };

					LRESULT lResult = 0;

					pUpdate->RouteMessage(Idle, lResult);
					}
				}
	
			GetMessage(&Message, NULL, NULL, NULL);
			
			try {
				switch( Message.message ) {

					case WM_SYSKEYDOWN:
					case WM_SYSKEYUP:

						if( !IsActive() ) {

							continue;
							}
						
						if( RouteAccelerator(Message) ) {

							continue;
							}
						
						break;

					case WM_KEYDOWN:
					case WM_KEYUP:
						
						if( RouteAccelerator(Message) ) {

							continue;
							}
						
						break;
					}

				if( !Translate(Message) ) {
				
					if( !m_fTranslate || !IsDialogMessage(Message) ) {
				
						TranslateMessage(&Message);
			
						DispatchMessage(&Message);
						}
					}
				}
				
			catch(CException &) {
			
				AfxTrace(L"ERROR: Exception in dialog ModalLoop\n");
				
				m_fDone = TRUE;
				}
			}
	
		AfxNull(CWnd).SetFocus();

		if( pBefore ) {

			pBefore->EnableWindow(TRUE);

			pBefore->SetActiveWindow();

			pBefore->SetFocus();
			}

		ShowWindow(SW_HIDE);
	
		afxThread->RestoreWaitMode(nWait);
		}

	return m_uCode;
	}

void CDialog::EndDialog(UINT uCode)
{
	if( m_fModeless ) {
	
		DestroyWindow(FALSE);
		
		return;
		}
		
	if( !m_fDone ) {
		
		m_fDone = TRUE;
		
		m_uCode = uCode;
		}
	}

// Attributes

BOOL CDialog::UseTranslate(void) const
{
	return m_fTranslate;
	}

CString CDialog::GetCaption(void) const
{
	return m_Caption;
	}

// Operations

void CDialog::SetTranslate(BOOL fTranslate)
{
	m_fTranslate = fTranslate;
	}

void CDialog::SetCaption(CString Caption)
{
	m_Caption = Caption;

	if( IsWindow() ) {

		SetWindowText(m_Caption);
		}
	}

void CDialog::SetCenterRect(CRect const &Center)
{
	m_Center = Center;
	}

BOOL CDialog::IsDialogMessage(MSG &Msg)
{
	return ::IsDialogMessage(m_hWnd, &Msg);
	}

void CDialog::SetRadioGroup(UINT uFrom, UINT uTo, UINT uData)
{
	for( UINT uID = uFrom; uID <= uTo; uID++ ) {
	
		BOOL fCheck = (uID - uFrom == uData);

		CButton &Button = (CButton &) GetDlgItem(uID);
		
		Button.SetCheck(fCheck);
		}
	}

UINT CDialog::GetRadioGroup(UINT uFrom, UINT uTo) const
{
	for( UINT uID = uFrom; uID <= uTo; uID++ ) {
	
		CButton &Button = (CButton &) GetDlgItem(uID);
		
		if( Button.IsChecked() ) {
		
			UINT uIndex = uID - uFrom;
			
			return uIndex;
			}
		}
	
	return 0;
	}

void CDialog::SetDlgFocus(CWnd &Wnd)
{
	AfxValidateObject(Wnd);
	
	if( Wnd.IsClass(L"BUTTON") ) {
		
		DWORD dwStyle = Wnd.GetWindowLong(GWL_STYLE);
		
		if( (dwStyle & BS_TYPEMASK) == BS_AUTORADIOBUTTON ) {
		
			for( UINT uID = Wnd.GetID();; uID++ ) {
			
				CButton &Button = (CButton &) GetDlgItem(uID);
				
				if( !Button ) {
				
					Wnd.SetFocus();
					
					return;
					}
				
				if( Button.IsChecked() ) {
					
					Button.SetFocus();
					
					return;
					}
				}
			}
		}
		
	if( Wnd.IsClass(L"COMBOBOX") ) {
	
		CComboBox &Combo = (CComboBox &) Wnd;
		
		Combo.SetEditSel(CRange(TRUE));
		}
		
	if( Wnd.IsClass(L"EDIT") ) {
	
		CEditCtrl &Edit = (CEditCtrl &) Wnd;
		
		Edit.SetSel(CRange(TRUE));
		}
		
	Wnd.SetFocus();
	}

void CDialog::SetDlgFocus(UINT uID)
{
	SetDlgFocus(GetDlgItem(uID));
	}

void CDialog::SendInitDialog(void)
{
	CWnd *pCtrl = FindFocus();

	MSG Message = { m_hWnd, WM_INITDIALOG, WPARAM(pCtrl->GetHandle()), 0L };

	LRESULT res = 0;

	if( !LocalMessage(Message, res) || res ) {
	
		CWnd &Wnd = *pCtrl;
		
		SetDlgFocus(Wnd);
		}
	}

void CDialog::MakeAutoDelete(void)
{
	m_fAutoDelete = TRUE;
	}

void CDialog::ForceActive(void)
{
	if( IsActive() ) {

		FLASHWINFO Info;

		Info.cbSize    = sizeof(Info);
		Info.hwnd      = m_hWnd;
		Info.dwFlags   = FLASHW_CAPTION;
		Info.uCount    = 3;
		Info.dwTimeout = 75;
		
		FlashWindowEx(&Info);

		MessageBeep(0);

		return;
		}

	SetActiveWindow();
	}

// Dialog Placement

void CDialog::AutoSizeDialog(BOOL fForce)
{
	if( fForce || !GetClientRect().GetWidth() ) {

		CRect Rect = GetBorderSize();

		Rect.bottom += m_nPadTop;

		AdjustWindowRect(Rect);

		SetWindowSize(Rect.GetSize(), FALSE);
		}
	}

void CDialog::PlaceDialog(void)
{
	PlaceDialogCentral();
	}

void CDialog::PlaceDialogStacked(void)
{
	CWnd &Parent = GetParent();
	
	CRect Rect = Parent.GetClientRect();
	
	CSize Step = CSize(SM_CXSIZE);
	
	Parent.SendMessage(WM_GETMETRIC, MC_DIALOG_ORG, LPARAM(&Step));
			
	CPoint Org = Rect.GetTopLeft() + Step;
	
	Parent.ClientToScreen(Org);
	
	KeepOnScreen(Org);
	
	SetWindowOrg(Org, FALSE);
	}
	
void CDialog::PlaceDialogCentral(void)
{
	CRect Rect = m_Center;

	if( Rect.IsEmpty() ) {
	
		Rect = afxMainWnd->GetClientRect();
	
		afxMainWnd->ClientToScreen(Rect);
		}

	CSize  Size = GetWindowRect().GetSize();
	
	CSize  Step = (Rect.GetSize() - Size) * CSize(2, 2) / CSize(4, 5);

	CPoint Org  = Rect.GetTopLeft() + Step;
	
	KeepOnScreen(Org, Rect.GetCenter());
	
	SetWindowOrg(Org, TRUE);
	}

void CDialog::KeepOnScreen(CPoint &Org)
{
	CPoint Pos;

	Pos.x = (1<<30);
	
	Pos.y = (1<<30);

	HMONITOR hMonitor = MonitorFromPoint(Pos, MONITOR_DEFAULTTOPRIMARY);

	KeepOnScreen(Org, hMonitor);
	}
	
void CDialog::KeepOnScreen(CPoint &Org, CPoint const &Ref)
{
	HMONITOR hMonitor = MonitorFromPoint(Ref, MONITOR_DEFAULTTONEAREST);

	KeepOnScreen(Org, hMonitor);
	}

void CDialog::KeepOnScreen(CPoint &Org, HMONITOR hMonitor)
{
	MONITORINFO Info;

	Info.cbSize = sizeof(Info);

	GetMonitorInfo(hMonitor, &Info);

	CRect Dialog = GetWindowRect();

	MakeMin(Org.x, Info.rcWork.right  - Dialog.GetWidth());

	MakeMin(Org.y, Info.rcWork.bottom - Dialog.GetHeight());

	MakeMax(Org.x, Info.rcWork.left);
		
	MakeMax(Org.y, Info.rcWork.top);
	}

// Modeless Translation

BOOL CDialog::IsModelessMessage(MSG &Msg)
{
	CDialog *pDialog = m_pHead;
	
	while( pDialog ) {
	
		if( pDialog->IsDialogMessage(Msg) ) {

			return TRUE;
			}
			
		pDialog = pDialog->m_pNext;
		}
	
	return FALSE;
	}

// Dialog Unit Scaling

CPoint CDialog::FromDlgUnits(CPoint const &Pos)
{
	CPoint Result = Pos;

	Result *= m_FontSize;

	Result += CSize(2, 4);

	Result /= CSize(4, 8);
	
	return Result;
	}

CSize CDialog::FromDlgUnits(CSize const &Size)
{
	CSize Result = Size;

	Result *= m_FontSize;

	Result += CSize(2, 4);

	Result /= CSize(4, 8);
	
	return Result;
	}

// Translation Hook

BOOL CDialog::Translate(MSG &Msg)
{
	return FALSE;
	}

// Message Procedures

LRESULT CDialog::DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( m_pfnSuper ) {

		AfxAssert(IsWindow());

		return CallWindowProc(m_pfnSuper, m_hWnd, uMessage, wParam, lParam);
		}

	return DefDlgProc(m_hWnd, uMessage, wParam, lParam);
	}

// Destruction Cleanup

void CDialog::OnNCDestroy(void)
{
	if( m_fAutoDelete && m_fModeless ) {
		
		delete this;
		
		return;
		}
	}

// Default Class Name

PCTXT CDialog::GetDefaultClassName(void) const
{
	return L"AfxDialogClass";
	}

// Default Class Definition

BOOL CDialog::GetClassDetails(WNDCLASSEX &Class) const
{
	Class.style         = CS_DBLCLKS | CS_SAVEBITS;

	Class.hCursor       = afxModule->LoadCursor(IDC_ARROW);

	Class.hbrBackground = NULL;

	Class.hIcon         = NULL;

	Class.hIconSm       = NULL;

	Class.cbWndExtra    = DLGWINDOWEXTRA;

	return TRUE;
	}

// Routing Control

BOOL CDialog::OnRouteMessage(MSG const &Msg, LRESULT &lResult)
{
	if( !HasFocus() ) {

		CWnd &Focus = GetFocus();
		
		if( Focus.IsWindow() ) {
		
			if( Focus.RouteMessage(Msg, lResult) ) {

				return TRUE;
				}
			}
		}
		
	return CCmdTarget::OnRouteMessage(Msg, lResult);
	}

// Message Map

AfxMessageMap(CDialog, CWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_CTLCOLORSTATIC)
	AfxDispatchMessage(WM_CTLCOLORBTN)

	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)

	AfxMessageEnd(CDialog)
	};

// Accelerators

BOOL CDialog::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CDialog::OnDestroy(void)
{
	}

BOOL CDialog::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	LPARAM lParam = m_MsgCtx.Msg.lParam;

	if( uID >= 0x8000 && uID <= 0xEFFF ) {

		RouteCommand(uID, lParam);

		return TRUE;
		}
	
	SendMessage(WM_AFX_COMMAND, uID, lParam);

	return TRUE;
	}

void CDialog::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	UINT uCount = Menu.GetMenuItemCount();

	for( UINT uPos = 0; uPos < uCount; uPos++ ) {

		UINT uID = Menu.GetMenuItemID(uPos);
		
		if( uID == 0xFFFF ) {

			CMenu &Sub = Menu.GetSubMenu(uPos);
			
			if( Sub.GetHandle() ) {

				OnInitPopup(Sub, 0, FALSE);
				}
			}
		else {
			if( HIBYTE(uID) ) {
	
				CCmdSourceMenu Source(Menu, uID);
	
				if( uID < 0xF000 ) {

					Source.PrepareSource();
					}
	
				RouteControl(uID, Source);
				}
			}
		}	
	}

BOOL CDialog::OnEraseBkGnd(CDC &DC)
{
	if( m_fLightBack ) {

		DC.FillRect(GetClientRect(), afxBrush(TabFace));

		return TRUE;
		}

	DC.FillRect(GetClientRect(), afxBrush(3dFace));
	
	return TRUE;
	}

HOBJ CDialog::OnCtlColorStatic(CDC &DC, CWnd &Wnd)
{
	if( m_fLightBack ) {

		DC.SetBkColor(afxColor(TabFace));

		return afxBrush(TabFace);
		}

	DC.SetBkColor(afxColor(3dFace));

	return afxBrush(3dFace);
	}

HOBJ CDialog::OnCtlColorBtn(CDC &DC, CWnd &Wnd)
{
	if( m_fLightBack ) {

		DC.SetBkColor(afxColor(TabFace));

		return afxBrush(TabFace);
		}

	DC.SetBkColor(afxColor(3dFace));

	return afxBrush(3dFace);
	}

// Command Handlers

BOOL CDialog::OnViewControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_VIEW_REFRESH:

			Src.EnableItem(TRUE);

			break;
			
		case IDM_VIEW_CYCLE_LANG:

			Src.EnableItem(TRUE);

			break;
			
		default:
			return FALSE;
		}
	
	return TRUE;
	}

BOOL CDialog::OnViewCommand(UINT uID)
{
	if( uID == IDM_VIEW_CYCLE_LANG ) {

		afxMainWnd->SendMessage(WM_AFX_COMMAND, uID);

		PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CDialog::CalculateFontInfo(void)
{
	CClientDC DC(GetDesktopWindow());
	
	DC.Select(afxFont(Dialog));

	TEXTMETRIC TextMetric;
	
	DC.GetTextMetrics(TextMetric);

	m_FontSize.cx = TextMetric.tmAveCharWidth + TextMetric.tmOverhang;

	m_FontSize.cy = TextMetric.tmHeight;
	
	m_FontSize.cx = m_FontSize.cx + 2;

	m_FontSize.cy = m_FontSize.cy + 0;
	
	m_FullSize.cx = m_FontSize.cx;

	m_FullSize.cy = m_FontSize.cy + TextMetric.tmExternalLeading + 2;
	
	DC.Deselect();
	}

void CDialog::ModelessListAppend(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

void CDialog::ModelessListRemove(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

void CDialog::BuildSystemMenu(void)
{
	if( GetWindowStyle() & WS_SYSMENU ) {
		
		CMenu &Menu = GetSystemMenu();
		
		Menu.EmptyMenu();
		
		Menu.AppendMenu(0, SC_MOVE,  CString(IDS_DIALOG_MOVE));
		
		Menu.AppendMenu(0, SC_CLOSE, CString(IDS_DIALOG_CLOSE));
		}
	}

CRect CDialog::GetActiveRect(void)
{
	CPoint Min = CPoint(10000, 10000);

	CPoint Max = CPoint(0, 0);

	CWnd *pWnd = GetWindowPtr(GW_CHILD);
	
	while( pWnd->GetHandle() ) {
	
		CRect Rect = pWnd->GetWindowRect();

		if( !Rect.IsEmpty() ) {

			ScreenToClient(Rect);

			MakeMin(Min.x, Rect.left  );
			MakeMin(Min.y, Rect.top   );
			MakeMax(Max.x, Rect.right );
			MakeMax(Max.y, Rect.bottom);
			}

		pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
		}
			
	return CRect(Min, Max);
	}

CSize CDialog::GetBorderSize(CRect const &Rect)
{
	CPoint Min = Rect.GetTopLeft();

	CPoint Max = Rect.GetBottomRight();

	return CSize(Min + Max);
	}

CSize CDialog::GetBorderSize(void)
{
	CRect Rect = GetActiveRect();

	Rect.top    -= m_nPadTop;

	Rect.bottom -= m_nPadTop;

	return GetBorderSize(Rect);
	}

CWnd * CDialog::FindFocus(void)
{
	CWnd *pCtrl = GetWindowPtr(GW_CHILD);

	while( pCtrl->IsWindow() ) {
	
		if( pCtrl->GetWindowStyle() & WS_TABSTOP ) {

			if( pCtrl->IsWindowEnabled() ) {

				break;
				}
			}
			
		pCtrl = pCtrl->GetWindowPtr(GW_HWNDNEXT);
		}

	return pCtrl;
	}

// End of File
