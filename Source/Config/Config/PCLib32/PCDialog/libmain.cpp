
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		afxModule = New CModule(modCoreLibrary);

		if( !afxModule->InitLib(hThis) ) {

			AfxTrace(L"ERROR: Failed to load pcdialog\n");

			delete afxModule;

			afxModule = NULL;

			return FALSE;
			}

		New CButtonArtist;

		CWnd::SetMessageBox(ExtMsgBox);

		return TRUE;
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		CWnd::SetMessageBox(DefMsgBox);

		delete afxButton;

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
