
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Error Context
//

// Constructors

CError::CError(void)
{
	m_fAllowUI = TRUE;
	
	m_uType    = ET_OKAY;
	}

CError::CError(CError const &That)
{
	m_fAllowUI = That.m_fAllowUI;

	m_uType    = That.m_uType;
	
	m_Message  = That.m_Message;
	
	m_Range    = That.m_Range;
	
	m_Flags.Append(That.m_Flags);
	}

CError::CError(BOOL fAllowUI)
{
	m_fAllowUI = fAllowUI;
	
	m_uType    = ET_OKAY;
	}

// Assignment

CError const & CError::operator = (CError const &That)
{
	m_fAllowUI = That.m_fAllowUI;
	
	m_uType    = That.m_uType;
	
	m_Message  = That.m_Message;
	
	m_Range    = That.m_Range;
	
	m_Flags.Append(That.m_Flags);
	
	return ThisObject;
	}
		
// Attributes

BOOL CError::AllowUI(void) const
{
	return m_fAllowUI;
	}
		
BOOL CError::IsOkay(void) const
{
	return m_uType ? FALSE : TRUE;
	}
		
UINT CError::GetType(void) const
{
	return m_uType;
	}
		
PCTXT CError::GetMessage(void) const
{
	return m_Message;
	}
		
CRange CError::GetRange(void) const
{
	return m_Range;
	}
		
BOOL CError::GetFlag(PCTXT pFlag) const
{
	return !m_Flags.Failed(m_Flags.Find(pFlag));
	}

// Rendering

BOOL CError::Show(CWnd &Wnd) const
{
	if( !IsOkay() ) {
	
		if( m_uType == ET_CANCEL ) {

			return TRUE;
			}

		if( !GetFlag(L"Shown") ) {
			
			Wnd.Error(m_Message);
	
			return TRUE;
			}
		}

	return FALSE;
	}
		
// Core Operations

void CError::Clear(void)
{
	m_uType = ET_OKAY;
	
	m_Message.Empty();
	}
		
void CError::SetUI(BOOL fAllowUI)
{
	m_fAllowUI = fAllowUI;
	}

void CError::SetRange(CRange const &Range)
{
	m_Range = Range;
	}

void CError::SetCancel(void)
{
	m_uType = ET_CANCEL;
	
	m_Message.Empty();
	}
		
void CError::SetWarning(PCTXT pText)
{
	m_uType   = ET_WARNING;
	
	m_Message = pText;
	}
		
void CError::SetWarning(ENTITY ID)
{
	m_uType   = ET_WARNING;
	
	m_Message = CString(ID);
	}
		
void CError::SetError(PCTXT pText)
{
	m_uType   = ET_ERROR;
	
	m_Message = pText;
	}

void CError::SetError(ENTITY ID)
{
	m_uType   = ET_ERROR;
	
	m_Message = CString(ID);
	}
		
void CError::Set(PCTXT pText)
{
	m_uType   = ET_ERROR;
	
	m_Message = pText;
	}

void CError::Set(ENTITY ID)
{
	m_uType   = ET_ERROR;
	
	m_Message = CString(ID);
	}

void CError::Format(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	m_uType = ET_ERROR;

	m_Message.VFormat(pText, pArgs);

	va_end(pArgs);
	}

void CError::Format(CEntity ID, ...)
{
	va_list pArgs;

	va_start(pArgs, ID);

	m_uType = ET_ERROR;

	m_Message.VFormat(ID, pArgs);

	va_end(pArgs);
	}

void CError::Printf(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	m_uType = ET_ERROR;

	m_Message.VPrintf(pText, pArgs);

	va_end(pArgs);
	}

void CError::Printf(CEntity ID, ...)
{
	va_list pArgs;

	va_start(pArgs, ID);

	m_uType = ET_ERROR;

	m_Message.VPrintf(ID, pArgs);

	va_end(pArgs);
	}

// Legacy Operations

void CError::Set(PCSTR pText, UINT uHelp)
{
	Set(CString(pText));
	}

void CError::Set(PCTXT pText, UINT uHelp)
{
	Set(pText);
	}

// Message Modification
		
void CError::AddPrefix(PCTXT pText)
{
	m_Message = pText + m_Message;
	}

void CError::AddPrefix(ENTITY ID)
{
	m_Message = CString(ID) + m_Message;
	}

void CError::AddSuffix(PCTXT pText)
{
	m_Message = m_Message + pText;
	}
	
void CError::AddSuffix(ENTITY ID)
{
	m_Message = m_Message + CString(ID);
	}

// Flag Operations
	
BOOL CError::SetFlag(PCTXT pFlag)
{
	if( !GetFlag(pFlag) ) {
	
		m_Flags.Append(pFlag);
	
		return TRUE;
		}

	return FALSE;
	}

BOOL CError::ClearFlag(PCTXT pFlag)
{
	if( !GetFlag(pFlag) ) {

		m_Flags.Remove(m_Flags.Find(pFlag));
	
		return TRUE;
		}

	return FALSE;
	}

// End of File
