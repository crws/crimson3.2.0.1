
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "pcdialog.hpp"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Library Files
//

#pragma	comment(lib, "comdlg32.lib")

#pragma	comment(lib, "winmm.lib")

#pragma	comment(lib, "msimg32.lib")

// End of File
