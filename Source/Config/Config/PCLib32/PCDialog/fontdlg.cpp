
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Font Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CFontDialog, CCommonDialog);

// Static Data

LOGFONT CFontDialog::m_LogFont = { 0 };

BOOL    CFontDialog::m_fInit   = FALSE;

// Constructor

CFontDialog::CFontDialog(void)
{
	DWORD dwFlags = CF_EFFECTS	  |
			CF_FORCEFONTEXIST |
			CF_SCREENFONTS    |
			CF_LIMITSIZE	  |
			CF_NOVECTORFONTS  |
			CF_NOVERTFONTS	  |
			CF_NOSCRIPTSEL    ;

	if( m_fInit ) {

		dwFlags |= CF_INITTOLOGFONTSTRUCT;
		}

	m_Data.lStructSize    = sizeof(CHOOSEFONT);
	m_Data.hwndOwner      = NULL;
	m_Data.hDC	      = NULL;
	m_Data.lpLogFont      = &m_LogFont;
	m_Data.iPointSize     = 0;
	m_Data.Flags	      = dwFlags;
	m_Data.rgbColors      = 0;
	m_Data.lCustData      = 0;
	m_Data.lpfnHook       = NULL;
	m_Data.lpTemplateName = NULL;
	m_Data.hInstance      = NULL;
	m_Data.lpszStyle      = NULL;
	m_Data.nFontType      = 0;
	m_Data.nSizeMin       = 10;
	m_Data.nSizeMax       = 200;
	}
	
// Dialog Operations

UINT CFontDialog::Execute(CWnd &Parent)
{
	m_Data.hwndOwner = Parent.GetHandle();
	
	AfxInstallHook(this);
	
	if( !ChooseFont(&m_Data) ) {
	
		AfxRemoveHook();
		
		return FALSE;
		}

	m_fInit = TRUE;

	return TRUE;
	}

UINT CFontDialog::Execute(void)
{
	return CDialog::Execute();
	}

// Attributes

CString CFontDialog::GetFaceName(void) const
{
	return m_LogFont.lfFaceName;
	}

UINT CFontDialog::GetPointSize(void) const
{
	return m_Data.iPointSize / 10;
	}

BOOL CFontDialog::IsFontBold(void) const
{
	return m_LogFont.lfWeight >= FW_BOLD;
	}

BOOL CFontDialog::IsFontItalic(void) const
{
	return m_LogFont.lfItalic >= 1;
	}

// Operations

void CFontDialog::TrueTypeOnly(void)
{
	m_Data.Flags |= CF_TTONLY;
	}

void CFontDialog::SetMaxSize(INT nMax)
{
	m_Data.nSizeMax = nMax;
	}

// Message Map

AfxMessageMap(CFontDialog, CCommonDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxMessageEnd(CFontDialog)
	};

// Message Handlers

BOOL CFontDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CCommonDialog::OnInitDialog(Focus, dwData);

	GetDlgItem(0x410).EnableWindow(FALSE);
	
	GetDlgItem(0x411).EnableWindow(FALSE);
	
	GetDlgItem(0x443).ShowWindow(SW_HIDE);
	
	GetDlgItem(0x473).ShowWindow(SW_HIDE);

	SetWindowText(L"Select Font");

	return FALSE;
	}

// End of File
