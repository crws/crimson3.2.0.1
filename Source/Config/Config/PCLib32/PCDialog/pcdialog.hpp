
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCDIALOG_HPP
	
#define	INCLUDE_PCDIALOG_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcctrl.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pcdialog.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCDIALOG

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pcdialog.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Message Hook Procedure
//

DLLAPI LRESULT CALLBACK AfxDialogFilter(int, WPARAM, LPARAM);

//////////////////////////////////////////////////////////////////////////
//
// Message Hook Control
//

DLLAPI void AfxInstallDlgHook(void);
	
DLLAPI void AfxRemoveDlgHook(void);

//////////////////////////////////////////////////////////////////////////
//
// Message Box Procedure
//

DLLAPI UINT ExtMsgBox(HWND, PCTXT, PCTXT, UINT, PCTXT);

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CZoom;
class CDialog;
class CDlgLoader;
class CStdDialog;
class CTabDialog;
class CMessageBox;

//////////////////////////////////////////////////////////////////////////
//
// Zoom Helper
//

class DLLAPI CZoom : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CZoom(CWnd &Wnd);

		// Operations
		void DrawZoom(CRect const &From, CRect const &To);
		void DrawZoom(CRect const &From, CRect const &To, DWORD tZoom);

	protected:
		// Data Members
		HWND m_hWnd;

		// Implementation
		void DrawAnimRect(CDC &DC, CRect const &Rect);
		void PlaySound(CRect const &From, CRect const &To);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dialog Base Class
//

class DLLAPI CDialog : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDialog(void);

		// Destructor
		~CDialog(void);

		// Dialog Operations
		virtual BOOL Create(CWnd &Parent);
		virtual UINT Execute(CWnd &Parent);
		virtual UINT Execute(void);

		// Modal Operation
		UINT ModalLoop(CWnd &Parent);
		UINT SemiModal(CWnd *pBefore, CWnd *pUpdate);
		void EndDialog(UINT uCode);

		// Attributes
		BOOL    UseTranslate(void) const;
		CString GetCaption(void) const;

		// Operations
		void SetTranslate(BOOL fTranslate);
		void SetCaption(CString Caption);
		void SetCenterRect(CRect const &Center);
		BOOL IsDialogMessage(MSG &Msg);
		void SetRadioGroup(UINT uFrom, UINT uTo, UINT uData);
		UINT GetRadioGroup(UINT uFrom, UINT uTo) const;
		void SetDlgFocus(CWnd &Wnd);
		void SetDlgFocus(UINT uID);
		void SendInitDialog(void);
		void MakeAutoDelete(void);
		void ForceActive(void);

		// Dialog Placement
		void AutoSizeDialog(BOOL fForce);
		void PlaceDialog(void);
		void PlaceDialogStacked(void);
		void PlaceDialogCentral(void);
		void KeepOnScreen(CPoint &Org);
		void KeepOnScreen(CPoint &Org, CPoint const &Ref);
		void KeepOnScreen(CPoint &Org, HMONITOR hMonitor);
		
		// Modeless Translation
		static BOOL IsModelessMessage(MSG &Msg);

		// Dialog Unit Scaling
		CPoint FromDlgUnits(CPoint const &Pos);
		CSize  FromDlgUnits(CSize const &Size);

	protected:
		// Static Data
		static CDialog * m_pHead;
		static CDialog * m_pTail;
		
		// Data Members
		CAccelerator    m_Accel;
		CDialog       * m_pNext;
		CDialog       * m_pPrev;
		CString		m_Caption;
		int		m_nPadTop;
		CRect		m_Center;
		BOOL		m_fTranslate;
		BOOL		m_fLightBack;
		BOOL		m_fModeless;
		BOOL		m_fAutoDelete;
		BOOL		m_fDone;
		UINT		m_uCode;
		CSize		m_FontSize;
		CSize		m_FullSize;
		
		// Translation Hook
		virtual BOOL Translate(MSG &Msg);

		// Message Procedures
		LRESULT DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// Destruction Cleanup
		void OnNCDestroy(void);

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Default Class Definition
		BOOL GetClassDetails(WNDCLASSEX &Class) const;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Msg, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnDestroy(void);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		BOOL OnEraseBkGnd(CDC &DC);
		HOBJ OnCtlColorStatic(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorBtn(CDC &DC, CWnd &Wnd);

		// Command Handlers
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		// Implementation
		void   ModelessListAppend(void);
		void   ModelessListRemove(void);
		void   CalculateFontInfo(void);
		void   BuildSystemMenu(void);
		CRect  GetActiveRect(void);
		CSize  GetBorderSize(CRect const &Rect);
		CSize  GetBorderSize(void);
		CWnd * FindFocus(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dialog Resource Loader
//

class DLLAPI CDlgLoader : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDlgLoader(void);

		// Operations
		void SetOrigin(CPoint Org);
		void SetName(ENTITY ID);

		// Resource Loading
		BOOL Load(void);
		BOOL Process(CDialog &Dlg, BOOL fCaption);
		void Free(void);

	protected:
		// Data Members
		CPoint  m_Org;
		CEntity m_Name;
		HGLOBAL m_hData;
		BYTE *  m_pData;

		// Implementation
		BOOL LoadTemplate(void);
		void FreeTemplate(void);
		UINT ReadHeader(CDialog &Dlg, BOOL fCaption);
		void MakeControl(CDialog &Dlg);

		// String Access
		CString ReadText(WCHAR * &pText, UINT uFactor);

		// Pointer Alignment
		BYTE  * Align(BYTE  *pData, UINT uFactor);
		WCHAR * Align(WCHAR *pData, UINT uFactor);

		// Control Location
		CLASS FindControlClass(PCTXT pName);
	};

//////////////////////////////////////////////////////////////////////////
//
// Resource File Dialog
//

class DLLAPI CStdDialog : public CDialog
{
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CStdDialog(void);

		// Dialog Operations
		BOOL Create(CWnd &Parent);

		// Modeless Operation
		BOOL CreateModeless(CWnd &Parent);
		
		// Operations
		void SetName(ENTITY ID);

	protected:
		// Data Members
		CDlgLoader m_Loader;
		BOOL       m_fAutoClose;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnClose(void);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);
	};

//////////////////////////////////////////////////////////////////////////
//
// Tabbed Dialog
//

class DLLAPI CTabDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTabDialog(void);

		// Destructor
		~CTabDialog(void);

		// Attributes
		UINT GetPage(void) const;

		// Operations
		BOOL SetPage(UINT uPage);
		void SetDisabledText(PCTXT pText);

	protected:
		// Data Members
		CTabCtrl *m_pTab;
		CRect     m_TabRect;
		CCtrlWnd *m_pDisText;
		UINT	  m_uPage;

		// Overridables
		virtual void OnShowPage(void);
		virtual BOOL IsPageEnabled(void);
		virtual BOOL OnSaveData(void);

		// Translation Hook
		BOOL Translate(MSG &Msg);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);

		// Notification Handlers
		BOOL OnSelChanging(UINT uID, NMHDR &Info);
		void OnSelChange(UINT uID, NMHDR &Info);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnTabNext(UINT uID);
		BOOL OnTabPrev(UINT uID);

		// Implementation
		void  LayoutTabControl(void);
		void  CreateTabControl(void);
		void  CreateDisabledText(void);
		CSize GetDisabledTextSize(void);
		void  AddTabItems(void);
		BOOL  IncludeControl(BOOL fShared, UINT ID);
		void  MoveControls(BOOL fShared, CPoint Step);
		CRect GetActiveRect(BOOL fShared);
		void  ShowControls(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Message Box Dialog
//

class DLLAPI CMessageBox : public CDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CMessageBox(PCTXT pText, PCTXT pTitle, UINT uStyle, PCTXT pDoNot = NULL);

		// Dialog Operations
		BOOL Create(CWnd &Parent);
		UINT Execute(CWnd &Parent);
		UINT Execute(void);
		
		// Operations
		void ChangeIcon(ENTITY ID);

	protected:
		// Button Data
		struct CButtonInfo
		{
			TCHAR   cTag;
			CString Label;
			UINT    uID;
			};

		// Data Members
		CString  m_Text;
		CString  m_Title;
		CString  m_DoNot;
		PCTXT    m_pPattern;
		PCTXT    m_pIconName;
		UINT     m_uAlert;
		UINT     m_uDefault;
		UINT	 m_uDefID;
		CIcon    m_Icon;
		CString  m_Line[20];
		UINT     m_uLines;
		UINT     m_uButtons;
		CSize    m_BarSize;
		CSize    m_BorderSize;
		CSize    m_DialogSize;
		CSize    m_IconSize;
		CSize    m_TextSize;
		CSize    m_ButtonSize;
		CPoint   m_IconOrg;
		CPoint   m_TextOrg;
		
		// Protected Constructor
		CMessageBox(void) { }

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		void OnDestroy(void);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnClose(void);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		
		// Tooltip Control
		BOOL EnableTips(void);

		// Paint Helper
		void Paint(CDC &DC);

		// Implementation
		PCTXT   FindPattern(UINT uStyle);
		PCTXT   FindIconName(UINT uStyle);
		CString FindDefTitle(UINT uStyle);
		void    FormatText(void);
		void    FindSizeInfo(void);
		void    MakeButtons(void);
		void    MakeDoNot(void);
		void    AdjustSize(void);
		void    SaveDoNot(UINT uCode);
		
		// Button Location
		virtual BOOL FindButton(TCHAR cTag, CButtonInfo &Info);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCommonDialog;
class CFileDialog;
class COpenFileDialog;
class CSaveFileDialog;
class CColorDialog;
class CFontDialog;

//////////////////////////////////////////////////////////////////////////
//
// Common Dialog Base Class
//

class DLLAPI CCommonDialog : public CDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
	protected:
		// Data Members
		BOOL m_fHide;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnWindowPosChanging(WINDOWPOS &Pos);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Dialog Base Class
//

class DLLAPI CFileDialog : public CCommonDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CFileDialog(void);
		
		// Attributes
		CFilename GetFilename(void) const;
		
		// Operations
		void SetFilename(PCTXT pFile);
		void SetFilter(PCTXT pFilter);

		// Last Path Management
		static void SetLastPath(PCTXT pPath);
		static void LoadLastPath(PCTXT pType);
		static void LoadLastPath(PCTXT pType, PCTXT pDefault);
		static void SaveLastPath(PCTXT pType);
		
	protected:
		// Static Data
		static TCHAR m_sLastPath[_MAX_PATH];

		// Data Members
		TCHAR	     m_sFile[_MAX_PATH];
		TCHAR	     m_sMask[_MAX_PATH];
		OPENFILENAME m_Data;
		CString      m_Filter;

		// Implementation
		void UpdateData(CWnd &Parent);
		void UpdatePath(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Open File Dialog
//

class DLLAPI COpenFileDialog : public CFileDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		COpenFileDialog(void);
		
		// Dialog Operations
		UINT Execute(CWnd &Parent);
		UINT Execute(void);

		// Operations
		void AllowReadOnly(void);
		
		// Attributes
		BOOL IsReadOnly(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Save File Dialog
//

class DLLAPI CSaveFileDialog : public CFileDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CSaveFileDialog(void);
		
		// Dialog Operations
		UINT Execute(CWnd &Parent);
		UINT Execute(void);
		UINT ExecAndCheck(CWnd &Parent);
		UINT ExecAndCheck(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Color Dialog
//

class DLLAPI CColorDialog : public CCommonDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CColorDialog(void);
		
		// Dialog Operations
		UINT Execute(CWnd &Parent);
		UINT Execute(void);

		// Attributes
		CColor GetColor(void) const;
		
		// Operations
		void SetColor(CColor Color);
		void PreventFullOpen(void);
		void ShowRainbowOnly(void);

		// Custom Color Management
		static void   LoadCustom(CRegKey &Key);
		static void   SaveCustom(CRegKey &Key);
		static void   LoadCustom(void);
		static void   SaveCustom(void);
		static void   SetCustom(UINT n, CColor Color);
		static CColor GetCustom(UINT n);
		
	protected:
		// Static Data
		static CColor m_Custom[16];

		// Data Members
		CHOOSECOLOR m_Data;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
	};

//////////////////////////////////////////////////////////////////////////
//
// Font Dialog
//

class DLLAPI CFontDialog : public CCommonDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CFontDialog(void);
		
		// Dialog Operations
		UINT Execute(CWnd &Parent);
		UINT Execute(void);
		
		// Attributes
		CString GetFaceName(void) const;
		UINT    GetPointSize(void) const;
		BOOL    IsFontBold(void) const;
		BOOL    IsFontItalic(void) const;

		// Operations
		void TrueTypeOnly(void);
		void SetMaxSize(INT nMax);
		
	protected:
		// Static Data
		static LOGFONT m_LogFont;
		static BOOL    m_fInit;		

		// Data Members
		CHOOSEFONT m_Data;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStringDialog;

//////////////////////////////////////////////////////////////////////////
//
// Single String Dialog
//

class DLLAPI CStringDialog : public CStdDialog
{
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CStringDialog(void);
		CStringDialog(PCTXT pCaption, PCTXT pGroup, PCTXT pData);
		
		// Attributes
		CString GetData(void) const;

		// Operations
		void SetGroup(PCTXT pGroup);
		void SetData(PCTXT pData);
		void SetLimit(UINT uLimit);
		
	protected:
		// Data Members
		CString m_Group;
		CString m_Data;
		UINT    m_uLimit;

		// Overridables
		virtual BOOL OnCheckText(CString Text);
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		void OnEditChange(UINT uID, CWnd &Wnd);
	};	

//////////////////////////////////////////////////////////////////////////
//
// Button Flags
//

#define	MF_PRESSED	0x00010000

//////////////////////////////////////////////////////////////////////////
//
// Button Artist
//

#define	afxButton	(CButtonArtist::FindInstance())

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CButtonArtist;
class CButtonBitmap;

//////////////////////////////////////////////////////////////////////////
//
// Button Artist
//

class DLLAPI CButtonArtist
{
	public:
		// Object Location
		static CButtonArtist * FindInstance(void);

		// Constructor
		CButtonArtist(void);
		
		// Destructor
		~CButtonArtist(void);
		
		// Bitmap Management
		void AppendBitmap(UINT uID, CButtonBitmap *pBitmap);
		void RemoveBitmap(UINT uID);

		// Size Adjustment
		int Adjust(CDC &DC, DWORD Image, CString Text);

		// Button Drawing
		void Draw(CDC &DC, CRect Rect, DWORD Image, CString Text, UINT uState);
		
	protected:
		// Static Data
		static CButtonArtist * m_pThis;
		
		// Bitmap Structure
		struct CBitmapInfo {
			UINT           m_uID;
			CButtonBitmap *m_pBitmap;
			CBitmapInfo   *m_pPrev;
			CBitmapInfo   *m_pNext;
			};
			
		// Data Members
		CBitmapInfo *m_pHead;
		CBitmapInfo *m_pTail;
		
		// Info Location
		CBitmapInfo * FindInfo(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Button Bitmap
//

class DLLAPI CButtonBitmap
{
	public:
		// Constructor
		CButtonBitmap(CBitmap &Src, CSize Size, UINT uCount);
		
		// Operations
		void Draw(CDC &DC, CRect Rect, UINT uImage, UINT uState);
		
	protected:
		// Data Members
		CBitmap m_Bitmap;
		CSize   m_Size;
		CRect   m_Rect;
		UINT    m_uCount;
		
		// Implementation
		void CopyImage(CBitmap &Src);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CError;

//////////////////////////////////////////////////////////////////////////
//
// Error Types
//

#define	ET_OKAY		0
#define	ET_CANCEL	1
#define	ET_WARNING	2
#define	ET_ERROR	3

//////////////////////////////////////////////////////////////////////////
//
// Error Context
//

class DLLAPI CError
{
	public:
		// Constructors
		CError(void);
		CError(CError const &That);
		CError(BOOL fAllowUI);
		
		// Assignment
		CError const & operator = (CError const &That);
		
		// Attributes
		BOOL   AllowUI(void) const;
		BOOL   IsOkay(void) const;
		UINT   GetType(void) const;
		PCTXT  GetMessage(void) const;
		CRange GetRange(void) const;
		BOOL   GetFlag(PCTXT pFlag) const;
		
		// Rendering
		BOOL Show(CWnd &Wnd) const;
		
		// Core Operations
		void Clear(void);
		void SetUI(BOOL fAllowUI);
		void SetRange(CRange const &Range);
		void SetCancel(void);
		void SetWarning(PCTXT pText);
		void SetWarning(ENTITY ID);
		void SetError(PCTXT pText);
		void SetError(ENTITY ID);
		void Set(PCTXT pText);
		void Set(ENTITY ID);
		void Format(PCTXT pText, ...);
		void Format(CEntity ID, ...);
		void Printf(PCTXT pText, ...);
		void Printf(CEntity ID, ...);

		// Legacy Operations
		void Set(PCSTR pText, UINT uHelp);
		void Set(PCTXT pText, UINT uHelp);

		// Message Modification
		void AddPrefix(PCTXT pText);
		void AddPrefix(ENTITY ID);
		void AddSuffix(PCTXT pText);
		void AddSuffix(ENTITY ID);

		// Flag Operations
		BOOL SetFlag(PCTXT pFlag);
		BOOL ClearFlag(PCTXT pFlag);

	protected:
		// Data Members
		BOOL         m_fAllowUI;
		UINT         m_uType;
		CString      m_Message;
		CRange       m_Range;
		CStringArray m_Flags;
	};

// End of File

#endif
