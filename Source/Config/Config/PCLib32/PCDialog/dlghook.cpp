
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dialog Message Hook Data
//

static UINT  m_uCount = 0;

static HHOOK m_hHook  = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Dialog Message Hook Control
//

void AfxInstallDlgHook(void)
{
	if( !m_uCount++ ) {

		AfxAssert(m_hHook == NULL);
		
		m_hHook = SetWindowsHookEx( WH_MSGFILTER,
					    FARPROC(AfxDialogFilter),
					    afxModule->GetAppInstance(),
					    GetCurrentThreadId()
					    );
					    

		AfxAssert(m_hHook);
		}
	}
	
void AfxRemoveDlgHook(void)
{
	if( !--m_uCount ) {

		AfxAssert(m_hHook);
		
		UnhookWindowsHookEx(m_hHook);
		
		m_hHook = NULL;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Dialog Message Processing
//

static void ProcessF1(UINT &Message)
{
	HWND hWnd  = GetActiveWindow();
			
	HWND hCtrl = GetDlgItem(hWnd, IDHELP);
			
	if( hCtrl == NULL || IsWindowEnabled(hCtrl) ) {
	
	        if( Message == WM_KEYDOWN ) {
	        
	        	UINT uID = 0x8601;
	        	
			PostMessage(hWnd, WM_COMMAND, uID, 0L);
			}
		
		Message = WM_DEADCHAR;

		return;
		}

	MessageBeep(0);
	}
	
//////////////////////////////////////////////////////////////////////////
//
// Dialog Message Hook
//

LRESULT CALLBACK AfxDialogFilter(int nCode, WPARAM wParam, LPARAM lParam)
{
	MSG *pMsg = (MSG *) lParam;
	
	if( nCode == MSGF_DIALOGBOX ) {

		if( pMsg->message == WM_KEYDOWN ) {
		
			switch( pMsg->wParam ) {
			
				case VK_F1:

					ProcessF1(pMsg->message);
					
					break;
				}
			}

		if( pMsg->message == WM_KEYUP ) {
		
			switch( pMsg->wParam ) {
			
				case VK_F1:

					ProcessF1(pMsg->message);

					break;
				}
			}
		}

	return CallNextHookEx(m_hHook, nCode, wParam, lParam);
	}

// End of File
