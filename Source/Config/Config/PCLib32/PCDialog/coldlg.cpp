
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Color Dialog
//

// Static Data

CColor CColorDialog::m_Custom[] = { 0 };

// Runtime Class

AfxImplementRuntimeClass(CColorDialog, CCommonDialog);

// Constructor

CColorDialog::CColorDialog(void)
{
	m_Data.lStructSize    = sizeof(CHOOSECOLOR);
	m_Data.hwndOwner      = NULL;
	m_Data.hInstance      = NULL;
	m_Data.rgbResult      = 0;
	m_Data.lpCustColors   = PDWORD(m_Custom);
	m_Data.Flags          = CC_SOLIDCOLOR | CC_FULLOPEN | CC_RGBINIT;
	m_Data.lCustData      = 0;
	m_Data.lpfnHook       = NULL;
	m_Data.lpTemplateName = NULL;

	m_fLightBack = FALSE;
	}
	
// Dialog Operations

UINT CColorDialog::Execute(CWnd &Parent)
{
	m_Data.hwndOwner = Parent.GetHandle();
	
	AfxInstallHook(this);
	
	if( !ChooseColor(&m_Data) ) {
	
		AfxRemoveHook();
		
		return FALSE;
		}

	return TRUE;
	}

UINT CColorDialog::Execute(void)
{
	return CDialog::Execute();
	}

// Attributes

CColor CColorDialog::GetColor(void) const
{
	return m_Data.rgbResult;
	}

// Operations

// cppcheck-suppress passedByValue

void CColorDialog::SetColor(CColor Color)
{
	m_Data.rgbResult = Color;
	}

void CColorDialog::PreventFullOpen(void)
{
	m_Data.Flags &= ~CC_FULLOPEN;

	m_Data.Flags |= CC_PREVENTFULLOPEN;
	}

void CColorDialog::ShowRainbowOnly(void)
{
	m_Data.Flags         |= CC_ENABLETEMPLATE;

	m_Data.hInstance      = afxModule->GetInstance();

	m_Data.lpTemplateName = L"ChooseColor";
	}

// Custom Color Management

void CColorDialog::LoadCustom(CRegKey &Key)
{
	for( UINT n = 0; n < elements(m_Custom); n++ ) {

		CPrintf Name(L"Col%2.2X", n);

		m_Custom[n] = Key.GetValue(Name, UINT(0));
		}
	}

void CColorDialog::SaveCustom(CRegKey &Key)
{
	for( UINT n = 0; n < elements(m_Custom); n++ ) {

		CPrintf Name(L"Col%2.2X", n);

		Key.SetValue(Name, DWORD(m_Custom[n]));
		}
	}

void CColorDialog::LoadCustom(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();
	
	Key.MoveTo(L"PCDialog");
	
	Key.MoveTo(L"Custom Colors");

	LoadCustom(Key);
	}

void CColorDialog::SaveCustom(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();
	
	Key.MoveTo(L"PCDialog");
	
	Key.MoveTo(L"Custom Colors");

	SaveCustom(Key);
	}

// cppcheck-suppress passedByValue

void CColorDialog::SetCustom(UINT n, CColor Color)
{
	m_Custom[n] = Color;
	}
		
CColor CColorDialog::GetCustom(UINT n)
{
	return m_Custom[n];
	}

// Message Map

AfxMessageMap(CColorDialog, CCommonDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	
	AfxMessageEnd(CColorDialog)
	};

// Message Handlers

BOOL CColorDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CCommonDialog::OnInitDialog(Focus, dwData);

	SetWindowText(IDS("Select Color"));

	return FALSE;
	}

void CColorDialog::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	PostMessage(WM_COMMAND, IDOK, 0);
	}
	
// End of File
