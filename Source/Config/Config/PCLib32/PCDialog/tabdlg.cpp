
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tabbed Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CTabDialog, CStdDialog);

// Constructor

CTabDialog::CTabDialog(void)
{
	m_uPage = 0;
	}

// Destructor

CTabDialog::~CTabDialog(void)
{
	}

// Attributes

UINT CTabDialog::GetPage(void) const
{
	return m_uPage;
	}

// Operations

BOOL CTabDialog::SetPage(UINT uPage)
{
	if( m_uPage != uPage ) {

		if( !IsPageEnabled() || OnSaveData() ) {

			m_uPage = uPage;

			m_pTab->SetCurSel(m_uPage);

			ShowControls();

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

void CTabDialog::SetDisabledText(PCTXT pText)
{
	m_pDisText->SetWindowText(pText);
	}

// Overridables

void CTabDialog::OnShowPage(void)
{
	}

BOOL CTabDialog::IsPageEnabled(void)
{
	return TRUE;
	}

BOOL CTabDialog::OnSaveData(void)
{
	return TRUE;
	}

// Translation Hook

BOOL CTabDialog::Translate(MSG &Msg)
{
	if( Msg.message == WM_KEYDOWN || Msg.message == WM_KEYUP ) {

		if( GetKeyState(VK_CONTROL) & 0x8000 ) {
	
			if( Msg.wParam == VK_TAB ) {

				if( Msg.message == WM_KEYDOWN ) {

					if( GetKeyState(VK_SHIFT) & 0x8000 ) {

						SendMessage(WM_COMMAND, IDC_TAB_PREV, 0L);
						}
					else
						SendMessage(WM_COMMAND, IDC_TAB_NEXT, 0L);
					}
				}
		
			Msg.message = WM_DEADCHAR;
			}
		}

	return CStdDialog::Translate(Msg);
	}

// Message Map

AfxMessageMap(CTabDialog, CStdDialog)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxDispatchNotify (IDC_TAB_CTRL, TCN_SELCHANGING, OnSelChanging)
	AfxDispatchNotify (IDC_TAB_CTRL, TCN_SELCHANGE,   OnSelChange  )

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchCommand(IDC_TAB_NEXT, OnTabNext)
	AfxDispatchCommand(IDC_TAB_PREV, OnTabPrev)

	AfxMessageEnd(CTabDialog)
	};

// Message Handlers

UINT CTabDialog::OnCreate(CREATESTRUCT &Create)
{
	BuildSystemMenu();
	
	m_Loader.Process(ThisObject, TRUE);

	SendInitDialog();

	CreateTabControl();

	LayoutTabControl();

	ShowControls();

	PlaceDialog();

	return 0;
	}

BOOL CTabDialog::OnEraseBkGnd(CDC &DC)
{
	DC.Save();

	DC.ExcludeClipRect(m_TabRect);

	DC.FillRect(GetClientRect(), afxBrush(3dFace));

	DC.Restore();

	return TRUE;
	}

void CTabDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	m_pTab->Invalidate(FALSE);
	}

void CTabDialog::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw)
{
	if( uID == m_pTab->GetID() ) {

		CDC DC(Draw.hDC);

		DC.Select(afxFont(Dialog));

		CPoint  Pos;

		CString Text = m_pTab->GetItemText(Draw.itemID);

		CRect   Rect = Draw.rcItem;

		CSize   Size = DC.GetTextExtent(Text);

		Pos.x = Rect.left + 3;
		
		Pos.y = Rect.top  + 3;

		if( Draw.itemState & ODS_SELECTED ) {

			Pos.x += 2;
			}
		else
			Rect.bottom += 2;

		DC.SetTextColor(afxColor(ButtonText));

		DC.SetBkColor(afxColor(TabFace));

		DC.FillRect(Rect, afxBrush(TabFace));

		DC.TextOut (Pos, Text);

		DC.Deselect();

		return;
		}

	AfxRouteToDefProc();
	}

// Notification Handlers

BOOL CTabDialog::OnSelChanging(UINT uID, NMHDR &Info)
{
	return !OnSaveData();
	}

void CTabDialog::OnSelChange(UINT uID, NMHDR &Info)
{
	m_uPage = m_pTab->GetCurSel();

	ShowControls();
	}

// Command Handlers

BOOL CTabDialog::OnOkay(UINT uID)
{
	if( !IsPageEnabled() || OnSaveData() ) {

		EndDialog(TRUE);

		return TRUE;
		}

	return TRUE;
	}

BOOL CTabDialog::OnTabNext(UINT uID)
{
	UINT uCount = m_pTab->GetItemCount();

	SetPage((m_uPage + 1) % uCount);

	return TRUE;
	}

BOOL CTabDialog::OnTabPrev(UINT uID)
{
	UINT uCount = m_pTab->GetItemCount();

	SetPage((m_uPage + uCount - 1) % uCount);

	return TRUE;
	}

// Implementation

void CTabDialog::LayoutTabControl(void)
{
	CRect Active = GetActiveRect(FALSE);

	CRect Shared = GetActiveRect(TRUE);

	CRect Border = GetBorderSize(Active);

	CRect Work   = Border;

	m_pTab->AdjustRect(TRUE, Work);

	MoveControls(FALSE, Active.GetTopLeft() - Work.GetTopLeft());

	m_TabRect = CRect(Active.GetTopLeft(), Work.GetSize());

	m_pTab->MoveWindow(m_TabRect, FALSE);

	CreateDisabledText();

	CPoint Step = m_TabRect.GetBottomLeft() - Shared.GetTopLeft();

	MoveControls(TRUE, Step + CSize(0, m_TabRect.top));

	AutoSizeDialog(TRUE);
	}

void CTabDialog::CreateTabControl(void)
{
	m_pTab = New CTabCtrl;

	DWORD dwStyle = WS_VISIBLE | WS_GROUP | WS_TABSTOP;
	
	if( !afxStdTools->HasTheme() ) {
		
		dwStyle |= TCS_OWNERDRAWFIXED;
		}

	m_pTab->Create(dwStyle, CRect(), m_hWnd, IDC_TAB_CTRL);

	m_pTab->SetFont(afxFont(Dialog));

	AddTabItems();
	}

void CTabDialog::CreateDisabledText(void)
{
	m_pDisText = New CStatic;

	CRect Rect = m_pTab->GetWindowRect();

	CSize Size = GetDisabledTextSize();

	ScreenToClient(Rect);

	m_pTab->AdjustRect(FALSE, Rect);

	Rect.top    = Rect.top + (Rect.cy() - Size.cy) / 2;

	Rect.bottom = Rect.top + Size.cy;

	m_pDisText->Create(WS_VISIBLE | SS_CENTER, Rect, m_hWnd, IDC_TAB_PLACE);

	m_pDisText->SetFont(afxFont(Dialog));

	m_pDisText->SetWindowText(CString(IDS_PAGE_DISABLED));
	}

CSize CTabDialog::GetDisabledTextSize(void)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	CSize Size = DC.GetTextExtent(L"X");

	DC.Deselect();

	return Size;
	}

void CTabDialog::AddTabItems(void)
{
	CString Text = GetWindowText();

	UINT    uPos = Text.Find('|');

	SetWindowText(Text.Left(uPos));

	do {
		Text = Text.Mid(uPos + 1);

		uPos = Text.Find('|');

		m_pTab->AppendItem(CTabItem(Text.Left(uPos)));

		} while( uPos < NOTHING );
	}

BOOL CTabDialog::IncludeControl(BOOL fShared, UINT ID)
{
	if( ID == IDC_TAB_CTRL || ID == IDC_TAB_PLACE ) {

		return FALSE;
		}

	if( ID < 1000 ) {

		return fShared ? TRUE : FALSE;
		}

	return fShared ? FALSE : TRUE;
	}

void CTabDialog::MoveControls(BOOL fShared, CPoint Step)
{
	CWnd *pWnd = GetWindowPtr(GW_CHILD);
	
	while( pWnd->GetHandle() ) {

		if( IncludeControl(fShared, pWnd->GetID()) ) {
		
			CRect Rect = pWnd->GetWindowRect();

			ScreenToClient(Rect);

			pWnd->MoveWindow(Rect + Step, FALSE);
			}

		pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
		}
	}

CRect CTabDialog::GetActiveRect(BOOL fShared)
{
	CPoint Min = CPoint(10000, 10000);

	CPoint Max = CPoint(0, 0);

	CWnd *pWnd = GetWindowPtr(GW_CHILD);
	
	while( pWnd->GetHandle() ) {
	
		CRect Rect = pWnd->GetWindowRect();

		if( !Rect.IsEmpty() ) {
			
			if( IncludeControl(fShared, pWnd->GetID()) ) {

				ScreenToClient(Rect);

				MakeMin(Min.x, Rect.left  );
				MakeMin(Min.y, Rect.top   );
				MakeMax(Max.x, Rect.right );
				MakeMax(Max.y, Rect.bottom);
				}
			}

		pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
		}
			
	return CRect(Min, Max);
	}

void CTabDialog::ShowControls(void)
{
	BOOL fShow = IsPageEnabled();

	if( fShow ) {

		m_pDisText->ShowWindow(SW_HIDE);

		OnShowPage();
		}

	for( UINT nPass = 0; nPass < 2; nPass++ ) {

		CWnd *pWnd = GetWindowPtr(GW_CHILD);
		
		while( pWnd->GetHandle() ) {

			UINT ID = pWnd->GetID();

			if( IncludeControl(FALSE, ID) ) {

				if( fShow && ID / 1000 == m_uPage + 1 ) {

					if( nPass == 1 ) {

						pWnd->EnableWindow(TRUE);

						pWnd->ShowWindow(SW_SHOW);
						}
					}
				else {
					if( nPass == 0 ) {

						if( pWnd->HasFocus() ) {

							AfxNull(CWnd).SetFocus();
							}

						pWnd->ShowWindow(SW_HIDE);

						pWnd->EnableWindow(FALSE);
						}
					}
				}

			pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
			}
		}

	if( !fShow ) {

		m_pDisText->ShowWindow(SW_SHOW);
		}

	if( !m_pTab->HasFocus() ) {

		CWnd *pWnd = FindFocus();

		SetDlgFocus(*pWnd);
		}
	}

// End of File
