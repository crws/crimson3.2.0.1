 
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	CButtonBitmap *pBitmap = New CButtonBitmap( CBitmap(L"Tools"),
						    CSize(16, 16),
						    13
						    );

	afxButton->AppendBitmap(1, pBitmap);

	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Dynamic Class

AfxImplementDynamicClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestWnd;

	pWnd->Create( CString(IDS_APP_CAPTION),
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      CRect(200, 150, 850, 700),
		      AfxNull(CWnd), CMenu(L"TestMenu"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:
			Src.EnableItem(TRUE);
			break;
		
		default:
			return FALSE;
		}
		
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestWnd, CWnd);

// Constructor

CTestWnd::CTestWnd(void)
{
	m_Color = afxColor(3dFace);
	}

// Destructor

CTestWnd::~CTestWnd(void)
{
	}

// Routing Control

BOOL CTestWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	return CWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CTestWnd, CWnd)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)

	AfxMessageEnd(CTestWnd)
	};

// Message Handlers

UINT CTestWnd::OnCreate(CREATESTRUCT &Create)
{
	return 0;
	}

BOOL CTestWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID >= 0xF000 ) {
		
		AfxCallDefProc();
		
		return TRUE;
		}
		
	if( uID >= 0x8000 ) {
	
		CCmdSourceData Source;
		
		if( uID < 0xF000 ) {

			Source.PrepareSource();
			}
	
		RouteControl(uID, Source);
		
		if( Source.GetFlags() & MF_DISABLED ) {
		
			MessageBeep(0);
			
			return TRUE;
			}

		RouteCommand(uID, 0);
			
		return TRUE;
		}

	return FALSE;
	}

void CTestWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	UINT uCount = Menu.GetMenuItemCount();

	for( UINT uPos = 0; uPos < uCount; uPos++ ) {

		UINT uID = Menu.GetMenuItemID(uPos);
		
		if( uID == 0xFFFF ) {

			CMenu &Sub = Menu.GetSubMenu(uPos);
			
			if( Sub.GetHandle() ) {

				OnInitPopup(Sub, 0, FALSE);
				}
			}
		else {
			if( uID ) {
	
				CCmdSourceMenu Source(Menu, uID);
	
				if( uID < 0xF000 ) {

					Source.PrepareSource();
					}
	
				RouteControl(uID, Source);
				}
			}
		}
	}

void CTestWnd::OnPaint(void)
{
	CPaintDC DC(*this);

	for( UINT i = 0; i < 13; i++ ) {

		CRect Rect;
		
		Rect.left   = 10 + 30 * i;
		Rect.top    = 10;
		Rect.right  = Rect.left + 22;
		Rect.bottom = Rect.top  + 22;

		UINT s[] = {	0,
				MF_DISABLED,
				MF_HILITE,
				MF_HILITE | MF_PRESSED,
				MF_CHECKED
				};	

		for( UINT j = 0; j < elements(s); j++ ) {

			afxButton->Draw(DC, Rect, MAKELONG(i,1), L"", s[j]);

			Rect.top    = Rect.bottom + 8;

			Rect.bottom = Rect.top    + 22;
			}
		}
	}

BOOL CTestWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	DC.FillRect(Rect, CBrush(m_Color));

	return TRUE;
	}

void CTestWnd::OnSetFocus(CWnd &Wnd)
{
	}

void CTestWnd::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	CColorDialog Dlg;

	Dlg.ShowRainbowOnly();

	Dlg.Execute(ThisObject);
	}

// End of File
