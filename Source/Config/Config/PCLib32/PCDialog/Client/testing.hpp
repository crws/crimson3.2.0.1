
//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP
	
#define	INCLUDE_TESTING_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTestApp;
class CTestWnd;
class CTestDlg;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CTestApp : public CThread 
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestApp(void);

		// Destructor
		~CTestApp(void);

	protected:
		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

class CTestWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestWnd(void);

		// Destructor
		~CTestWnd(void);

	protected:
		// Data Members
		CColor m_Color;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnPaint(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnSetFocus(CWnd &Wnd);
		void OnLButtonDblClk(UINT uCode, CPoint Pos);
	};

// End of File

#endif
