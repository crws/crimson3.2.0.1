
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simple String Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CStringDialog, CStdDialog);
		
// Constructors

CStringDialog::CStringDialog(void)
{
	m_uLimit = 0;

	SetName(L"StringDialog");
	}

CStringDialog::CStringDialog(PCTXT pCaption, PCTXT pGroup, PCTXT pData)
{
	m_Caption = pCaption;
	
	m_Group   = pGroup;
	
	m_Data    = pData;

	m_uLimit  = 0;
	
	SetName(L"StringDialog");
	}

// Attributes

CString CStringDialog::GetData(void) const
{
	return m_Data;
	}
		
// Operations

void CStringDialog::SetGroup(PCTXT pGroup)
{
	m_Group = pGroup;
	}

void CStringDialog::SetData(PCTXT pData)
{
	m_Data = pData;
	}

void CStringDialog::SetLimit(UINT uLimit)
{
	m_uLimit = uLimit;
	}

// Overridables

BOOL CStringDialog::OnCheckText(CString Text)
{
	return TRUE;
	}

// Message Map

AfxMessageMap(CStringDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchCommand(IDOK, OnCommandOK)

	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxDispatchNotify(100, EN_CHANGE, OnEditChange)

	AfxMessageEnd(CStringDialog)
	};

// Message Handlers

BOOL CStringDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(m_Caption);
	
	GetDlgItem(200).SetWindowText(m_Group);
	
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(100);

	if( m_uLimit ) Edit.LimitText(m_uLimit);
	
	Edit.SetWindowText(m_Data);
	
	Edit.SetSel(CRange(TRUE));
	
	OnEditChange(100, Edit);

	return TRUE;
	}
	
// Command Handlers

BOOL CStringDialog::OnCommandOK(UINT uID)
{
	m_Data = GetDlgItem(100).GetWindowText();

	if( OnCheckText(m_Data) ) {
	
		EndDialog(TRUE);

		return TRUE;
		}

	SetDlgFocus(100);

	return TRUE;
	}

BOOL CStringDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

void CStringDialog::OnEditChange(UINT uID, CWnd &Wnd)
{
	GetDlgItem(IDOK).EnableWindow(Wnd.GetWindowTextLength() > 0);
	}

// End of File
