
#include "intern.hpp"

// Rewrite for new style.

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Button Bitmap
//

// Constructor

CButtonBitmap::CButtonBitmap(CBitmap &Src, CSize Size, UINT uCount)
{
	m_Size = Size;

	m_Rect = CRect(0, 0, uCount * Size.cx, 3 * Size.cy);

	m_Bitmap.Create(CClientDC(NULL), m_Rect.GetSize());

	CopyImage(Src);
	}

// Operations

void CButtonBitmap::Draw(CDC &DC, CRect Rect, UINT uImage, UINT uState)
{
	CPoint Pos = CPoint(uImage * m_Size.cx, uState * m_Size.cy);

	int xExtra = Rect.cx() - m_Size.cx;

	int yExtra = Rect.cy() - m_Size.cy;

	if( xExtra >= 0 ) {

		Rect.left   += xExtra / 2;
		
		Rect.right  -= xExtra / 2;
		}
	else
		Pos.x       -= xExtra / 2;

	if( yExtra >= 0 ) {

		Rect.top    += yExtra / 2;
		
		Rect.bottom -= yExtra / 2;
		}
	else
		Pos.y       -= yExtra / 2;

	DC.SetBkColor(afxColor(MAGENTA));

	DC.TransBlt(Rect, m_Bitmap, Pos);
	}

// Implementation

void CButtonBitmap::CopyImage(CBitmap &Src)
{
	CMemoryDC ImgDC;
	
	ImgDC.Select(m_Bitmap);
	
	ImgDC.BitBlt(m_Rect, Src, CPoint(0, 0), SRCCOPY);

	ImgDC.Deselect();
	}

// End of File
