
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCDIALOG_HXX
	
#define	INCLUDE_PCDIALOG_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcctrl.hxx>

//////////////////////////////////////////////////////////////////////////
//								
// Standard Control Styles
//

#define XS_BUTTONFIRST		(WS_GROUP)

#define XS_BUTTONREST		(NOT WS_GROUP)

#define XS_RADIOFIRST		(NOT BS_RADIOBUTTON|(BS_AUTORADIOBUTTON|WS_TABSTOP|WS_GROUP))

#define XS_RADIOREST		(NOT BS_RADIOBUTTON|(BS_AUTORADIOBUTTON))

#define XS_CHECKFIRST		(BS_AUTOCHECKBOX|WS_TABSTOP|WS_GROUP|WS_VISIBLE)

#define XS_CHECKREST		(BS_AUTOCHECKBOX|WS_VISIBLE)

#define	XS_DROPDOWNLIST		(CBS_DROPDOWNLIST | CBS_DISABLENOSCROLL | WS_TABSTOP | WS_VSCROLL)

#define	XS_LISTBOX		(LBS_DISABLENOSCROLL | WS_TABSTOP | WS_VSCROLL)

#define	XS_SIMPLE		(CBS_SIMPLE | WS_TABSTOP | WS_VSCROLL | CBS_DISABLENOSCROLL)

#define	XS_TREEVIEW		(WS_TABSTOP | TVS_LINESATROOT | TVS_HASLINES | TVS_HASBUTTONS | TVS_DISABLEDRAGDROP | TVS_SHOWSELALWAYS)

// End of File

#endif
