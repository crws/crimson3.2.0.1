
#include "intern.hpp"

#include <SoftPub.h>

#include <WinTrust.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma comment(lib, "Crypt32.lib")

#pragma comment(lib, "Wintrust.lib")

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CString (*PGETSTRING  )(void);

typedef BOOL    (*PGETPAIR    )(UINT, CString &, CString &);

typedef BOOL    (*PGETFEATURE )(CString const &, BOOL);

typedef CString (*PGETOPTION  )(CString const &, CString const &);

typedef BOOL    (*PALLOWDRIVER)(UINT, BOOL);

typedef DWORD	(*PGETCOLOR   )(CString const &, DWORD);

typedef	WORD	(*PGETUSBBASE )(void);

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

static COemStrings  m_OemStrings;

static HMODULE      m_hOem           = NULL;

static PGETSTRING   m_pfnGetCompany  = NULL;

static PGETSTRING   m_pfnGetModels   = NULL;

static PGETPAIR     m_pfnGetPair     = NULL;

static PGETFEATURE  m_pfnGetFeature  = NULL;

static PGETOPTION   m_pfnGetOption   = NULL;

static PALLOWDRIVER m_pfnAllowDriver = NULL;

static PGETCOLOR    m_pfnGetColor    = NULL;

static PGETUSBBASE  m_pfnGetUsbBase  = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Signature Checking
//

static BOOL CheckSig(PCTXT pName)
{
	WINTRUST_FILE_INFO FileInfo;
	
	WINTRUST_DATA      Data;
	
	memset(&FileInfo, 0, sizeof(WINTRUST_FILE_INFO));
	
	memset(&Data,     0, sizeof(WINTRUST_DATA));

	////////
	
	FileInfo.cbStruct      = sizeof(WINTRUST_FILE_INFO);
	
	FileInfo.pcwszFilePath = pName;

	FileInfo.hFile         = NULL;

	////////
	
	Data.cbStruct            = sizeof(WINTRUST_DATA);

	Data.dwUIChoice          = WTD_UI_NONE;
	
	Data.fdwRevocationChecks = WTD_REVOKE_NONE;
	
	Data.dwUnionChoice       = WTD_CHOICE_FILE;
	
	Data.pFile		 = &FileInfo;

	Data.dwProvFlags	 = WTD_REVOCATION_CHECK_NONE;

	Data.dwStateAction	 = WTD_STATEACTION_VERIFY;

	////////

	GUID    op = WINTRUST_ACTION_GENERIC_VERIFY_V2;
	
	HRESULT hr = WinVerifyTrust( INVALID_HANDLE_VALUE,
				     &op,
				     &Data
				     );

	if( SUCCEEDED(hr) ) {

		CRYPT_PROVIDER_DATA const *psProvData = WTHelperProvDataFromStateData( Data.hWVTStateData
										       );
		
		if( psProvData ) {
			
			CRYPT_PROVIDER_SGNR *psProvSigner = WTHelperGetProvSignerFromChain( PCRYPT_PROVIDER_DATA(psProvData),
											    0 ,
											    FALSE,
											    0
											    );
			
			if( psProvSigner ) {
		
				CRYPT_PROVIDER_CERT *psProvCert = WTHelperGetProvCertFromChain( psProvSigner,
												0
												);
				
				if( psProvCert ) {
					
					DWORD dwType  = CERT_X500_NAME_STR;
					
					DWORD dwCount = CertGetNameString( psProvCert->pCert,
									   CERT_NAME_RDN_TYPE,
									   0,
									   &dwType,
									   NULL,
									   0
									   );
					
					if( dwCount ) {
						
						PTXT pn = PTXT(LocalAlloc(0, dwCount * sizeof(TCHAR)));
						
						dwCount = CertGetNameString( psProvCert->pCert,
									     CERT_NAME_RDN_TYPE,
									     0,
									     &dwType,
									     pn,
									     dwCount
									     );
						
						if( dwCount ) {

							PTXT cn = wstrstr(pn, L"CN=");

							if( cn ) {

								if( !wstrcmp(cn + 3, L"Red Lion Controls Inc") ) {

									Data.dwStateAction = WTD_STATEACTION_CLOSE;
									
									WinVerifyTrust( INVALID_HANDLE_VALUE,
											&op,
											&Data
											);

									AfxTrace(L"VERIFY(%ls) = OKAY\n", pName);

									return TRUE;
									}
								}
							}
						
						LocalFree(pn);
						}
					}
				}
			}
	
		Data.dwStateAction = WTD_STATEACTION_CLOSE;
		
		WinVerifyTrust( INVALID_HANDLE_VALUE,
				&op,
				&Data
				);
		}

	AfxTrace(L"VERIFY(%ls) = FAIL\n", pName);

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// OEM Conversion APIs
//

void C3OemInit(void)
{
	m_hOem = LoadLibrary(L"oem.dll");

	m_pfnGetCompany  = PGETSTRING  (GetProcAddress(m_hOem, "?OemGetCompany@@YA?AVCString@@XZ"));

	m_pfnGetModels   = PGETSTRING  (GetProcAddress(m_hOem, "?OemGetModels@@YA?AVCString@@XZ"));

	m_pfnGetPair     = PGETPAIR    (GetProcAddress(m_hOem, "?OemGetPair@@YAHIAAVCString@@0@Z"));

	m_pfnGetFeature  = PGETFEATURE (GetProcAddress(m_hOem, "?OemGetFeature@@YAHAAVCString@@H@Z"));

	m_pfnGetOption   = PGETOPTION  (GetProcAddress(m_hOem, "?OemGetOption@@YA?AVCString@@ABV1@0@Z"));

	m_pfnAllowDriver = PALLOWDRIVER(GetProcAddress(m_hOem, "?OemAllowDriver@@YAHIH@Z"));

	m_pfnGetColor    = PGETCOLOR   (GetProcAddress(m_hOem, "?OemGetColor@@YAKAAVCString@@K@Z"));

	m_pfnGetUsbBase  = PGETUSBBASE (GetProcAddress(m_hOem, "?OemGetUsbBase@@YAGXZ"));
	}

void C3OemTerm(void)
{
	FreeLibrary(m_hOem);
	}

BOOL C3OemCheckSigs(void)
{
	if( CheckSig(afxModule->GetFilename()) ) {

		if( CheckSig(afxModule->GetApp()->GetFilename()) ) {

			TCHAR sPath[MAX_PATH];

			GetModuleFileName( m_hOem,
					   sPath,
					   elements(sPath)
					   );

			if( !CheckSig(sPath) ) {

				return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

CString C3OemCompany(void)
{
	if( m_pfnGetCompany ) {

		return (*m_pfnGetCompany)();
		}

	return L"Red Lion Controls";
	}

CString C3OemModels(void)
{
	if( m_pfnGetModels ) {

		return (*m_pfnGetModels)();
		}

	return L"g310v2";
	}

CString C3OemGetManual(UINT uFile)
{
	CString   Name;

	switch( uFile ) {
		
		default:
		case 0:	Name = L"c3.pdf";	break;
		case 1:	Name = L"rm.pdf";	break;
		case 2:	Name = L"e3.pdf";	break;
		case 3:	Name = L"fb.pdf";	break;
		}

	C3OemStrings(Name);

	if( TRUE ) {

		CString Root = Name.Left(2);

		PCTXT  pLang = L"en";

		switch( PRIMARYLANGID(GetThreadLocale()) ) {

/*			case LANG_FRENCH:

				pLang = L"fr";

				break;
*/
			case LANG_GERMAN:

				pLang = L"de";

				break;
/*
			case LANG_SPANISH:

				pLang = L"es";

				break;

			case LANG_CHINESE:

				pLang = L"zh";

				break;
*/			}

		Name.Printf(L"%s_%s.pdf", Root, pLang);
		}

	for( UINT p = 0; p < 2; p++ ) {

		CFilename Folder;

		if( p == 0 ) {

			Folder = afxModule->GetFilename().WithName(L"Manual\\");
		}
		else {
			CString    Oem = L"RedLion";

			Folder = afxModule->GetFilename().WithName(L"..\\..\\..\\..\\..\\Source\\Setup\\OEM\\" + Oem + L"\\Main\\Manual\\");
		}

		CFilename File = Folder + Name;

		if( File.Exists() ) {

			return Folder + Name;
		}

	}	

	return L"";
	}

void C3OemStrings(PTXT pBuffer, UINT &uSize)
{
	CCriticalGuard cg;

	m_OemStrings.OpenLoad();
	
	m_OemStrings.Replace(pBuffer, uSize);
	}

void C3OemStrings(CString &String)
{
	CCriticalGuard cg;

	m_OemStrings.OpenLoad();

	m_OemStrings.Replace(String);
	}

void C3OemAdjustMenu(CMenu &Menu)
{
	int nCount = Menu.GetMenuItemCount();
	
	for( int nItem = 0; nItem < nCount; nItem++ ) {
	
		int nID = Menu.GetMenuItemID(nItem);
		
		if( nID ) {

			CString Text = Menu.GetMenuString(nItem, MF_BYPOSITION);

			CString Work = Text;
			
			if( nID == -1 ) {
		
				C3OemAdjustMenu(Menu.GetSubMenu(nItem));
				}

			C3OemStrings(Work);

			if( Work.CompareC(Text) ) {

				MENUITEMINFO Info;

				memset(&Info, 0, sizeof(Info));

				Info.cbSize     = sizeof(Info);
				Info.fMask      = MIIM_TYPE;
				Info.fType      = MFT_STRING;
				Info.dwTypeData = PTXT(PCTXT(Work));
				Info.cch        = Work.GetLength();

				SetMenuItemInfo(Menu, nItem, TRUE, &Info);
				}
			}
		}
	}

BOOL C3OemFeature(PCTXT pName, BOOL fDefault)
{
	if( m_pfnGetFeature ) {

		return (*m_pfnGetFeature)(pName, fDefault);
		}

	return fDefault;
	}

CString C3OemOption(PCTXT pName, PCTXT pDefault)
{
	if( m_pfnGetOption ) {

		return (*m_pfnGetOption)(pName, pDefault);
		}

	return pDefault;
	}

BOOL C3OemAllowDriver(UINT uIdent, BOOL fDefault)
{
	if( m_pfnAllowDriver ) {

		return (*m_pfnAllowDriver)(uIdent, fDefault);
		}

	return fDefault;
	}

CColor C3OemGetColor(PCTXT pName, CColor Default)
{
	if( m_pfnGetColor ) {

		return (*m_pfnGetColor)(pName, Default);
		}

	return Default;
	}

WORD C3OemGetUsbBase(void)
{
	if( m_pfnGetUsbBase ) {

		return (*m_pfnGetUsbBase)();
		}

	return 0x0000;
	}

//////////////////////////////////////////////////////////////////////////
//
// Oem String Conversion
//

// Constructor

COemStrings::COemStrings(void)
{
	m_fFile  = FALSE;

	m_uCount = 0;
	}

// Management

void COemStrings::OpenLoad(void)
{
	if( !m_fFile ) {

		if( m_hOem ) {

			if( m_pfnGetPair ) {

				for( UINT n = 0;; n++ ) {

					CString a, b;

					if( (*m_pfnGetPair)(n, a, b) ) {

						LoadPair(a, b);

						continue;
						}

					break;
					}
				}
			
			m_fFile = TRUE;
			}
		}
	}

// Operations

void COemStrings::Replace(PTXT pBuffer, UINT &uSize)
{
	if( m_fFile ) {
		
		if( pBuffer ) {

			CString String(pBuffer);

			Replace(String);

			UINT uLen = String.GetLength();

			if( uLen >= uSize - 1 ) {

				uSize = uLen;

				return;
				}

			uSize = uLen;

			wmemcpy(pBuffer, String, uSize);

			pBuffer[uSize] = 0;

			return;
			}
		}

	uSize = wstrlen(pBuffer);
	}

void COemStrings::Replace(CString &String)
{
	// NOTE -- We use a two-level caching mechanism here. First, we store the
	// last input and output strings, as there are many repeated calls for the
	// same input. Second, we build a map of input strings to output strings,
	// and use that to translate first-level misses. If we miss the second cache,
	// we perform the translation manually and store the result.

	if( m_fFile ) {
		
		if( !String.IsEmpty() ) {

			if( String.CompareC(m_CacheIn) ) {

				m_CacheIn = String;

				INDEX i   = m_Cache.FindName(String);

				if( m_Cache.Failed(i) ) {
				
					CString Prev = String;

					BOOL    fHit = FALSE;

					for( UINT n = 0; n < m_uCount; n ++ ) {

						CString const &Search  = m_String1[n];

						CString const &Replace = m_String2[n];

						if( Search[0] == '{' ) {

							CString Extract = Search.Mid(1, Search.GetLength() - 2);

							if( !String.CompareC(Extract) ) {

								String = Replace;

								fHit = TRUE;

								break;
								}

							continue;
							}

						if( String.CompareC(Replace) ) {
							
							UINT uFind = 0;

							while( (uFind = String.Find(Search, uFind)) < NOTHING ) {

								String.Delete(uFind, Search.GetLength());

								String.Insert(uFind, Replace);

								uFind += Replace.GetLength();

								fHit = TRUE;
								}
							}
						}

					if( fHit ) {

						m_Cache.Insert(Prev, String);
						}
					else
						m_Cache.Insert(Prev, L"");
					}
				else {
					CString Data = m_Cache.GetData(i);

					if( !Data.IsEmpty() ) {

						String = Data;
						}
					}

				m_CacheOut = String;
				}
			else
				String = m_CacheOut;
			}

		return;
		}
	}

// Implementation

void COemStrings::LoadPair(CString String1, CString String2)
{
	m_String1.Append(String1);

	m_String2.Append(String2);

	m_uCount++;
	}

// End of File
