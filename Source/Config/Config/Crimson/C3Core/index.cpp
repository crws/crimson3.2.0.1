
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Indexed List of Items
//

// Runtime Class

AfxImplementRuntimeClass(CItemIndexList, CItemList);

// Constructor

CItemIndexList::CItemIndexList(void)
{
	}

CItemIndexList::CItemIndexList(CLASS Class) : CItemList(Class)
{
	}

// Destructor

CItemIndexList::~CItemIndexList(void)
{
	}

// Item Access

CItem * CItemIndexList::GetItem(INDEX Index) const
{
	return m_List[Index];
	}

CItem * CItemIndexList::GetItem(UINT uPos) const
{
	if( uPos < m_Index.GetCount() ) {

		INDEX Index = m_Index[uPos];

		if( !m_List.Failed(Index) ) {

			return m_List[Index];
			}
		}

	return NULL;
	}

// List Lookup

CItem * CItemIndexList::operator [] (INDEX Index) const
{
	return GetItem(Index);
	}

CItem * CItemIndexList::operator [] (UINT uPos) const
{
	return GetItem(uPos);
	}

// Persistance

void CItemIndexList::Load(CTreeFile &File)
{
	if( File.IsName(L"Fixed") ) {

		File.GetName();

		m_Fixed = File.GetValueAsInteger();
		}

	while( !File.IsEndOfData() ) {

		CString Name = File.GetName();

		CItem *pItem = CreateItem(Name);

		if( pItem ) {

			INDEX Index = m_List.Append(pItem);

			File.GetObject();

			if( File.IsName(L"NDX") ) {

				File.GetName();

				UINT uPos = File.GetValueAsInteger();

				pItem->SetIndex(uPos);

				while( uPos + 1 > m_Index.GetCount() ) {
					
					m_Index.Append(NULL);
					}

				m_Index.SetAt(uPos, Index);
				}
			else
				AllocIndex(pItem, Index);

			pItem->Load(File);

			File.EndObject();
			}
		}
	}

void CItemIndexList::Save(CTreeFile &File)
{
	File.PutValue(L"Fixed", m_Fixed);

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		if( !m_Class ) {

			CString Name = pItem->GetClassName();

			File.PutObject(Name.Mid(1));
			}
		else
			File.PutElement();

		File.SetPadding(pItem->GetPadding());

		File.PutValue(L"NDX", pItem->GetIndex());

		pItem->Save(File);

		File.EndObject();

		m_List.GetNext(Index);
		}
	}

// Download Support

BOOL CItemIndexList::MakeInitData(CInitData &Init)
{
	CItem::MakeInitData(Init);

	UINT uCount = m_Index.GetCount();

	Init.AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n++ ) {

		INDEX Index = m_Index[n];

		if( Index ) {

			Init.AddByte(1);

			m_List[Index]->MakeInitData(Init);
			}
		else
			Init.AddByte(0);
		}

	return TRUE;
	}

// Attributes

UINT CItemIndexList::GetIndexCount(void) const
{
	return m_Index.GetCount();
	}

// Operations

BOOL CItemIndexList::DeleteItem(INDEX Index)
{
	return CItemList::DeleteItem(Index);
	}

BOOL CItemIndexList::DeleteItem(UINT uPos)
{
	return (uPos < m_Index.GetCount()) ? DeleteItem(m_Index[uPos]) : FALSE;
	}

BOOL CItemIndexList::DeleteItem(CItem *pItem)
{
	return CItemList::DeleteItem(pItem);
	}

BOOL CItemIndexList::GetOrder(CArray <UINT> &List)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		List.Append(m_List[Index]->GetIndex());

		GetNext(Index);
		}

	return TRUE;
	}

BOOL CItemIndexList::SetOrder(CArray <UINT> const &List)
{
	UINT uCount = List.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CItem *pItem = GetItem(List[n]);

		MoveItem(pItem, INDEX(NULL));
		}

	return TRUE;
	}

// Index Management

void CItemIndexList::AllocIndex(CItem *pItem, INDEX Index)
{
	UINT uCount = m_Index.GetCount();

	UINT uPos   = pItem->GetIndex();

	if( uPos < NOTHING ) {

		if( uPos < uCount ) {

			if( !m_Index[uPos] ) {

				m_List[Index]->SetIndex(uPos);

				m_Index.SetAt(uPos, Index);

				return;
				}
			}
		else {
			pItem->SetIndex(uPos);

			while( uPos + 1 > uCount ) {
				
				m_Index.Append(NULL);

				uCount++;
				}

			m_Index.SetAt(uPos, Index);

			return;
			}
		}

	if( uCount + 1 == m_List.GetCount() ) {

		pItem->SetIndex(uCount);

		m_Index.Append(Index);

		return;
		}

	for( uPos = 0; uPos < uCount; uPos++ ) {

		if( !m_Index[uPos] ) {

			pItem->SetIndex(uPos);

			m_Index.SetAt(uPos, Index);

			return;
			}
		}

	AfxAssert(FALSE);
	}

void CItemIndexList::UpdateIndex(CItem *pItem, INDEX Index)
{
	UINT uPos = pItem->GetIndex();

	AfxAssert(uPos < m_Index.GetCount());

	m_Index.SetAt(uPos, Index);
	}

void CItemIndexList::FreeIndex(CItem *pItem, INDEX Index)
{
	UINT uPos = m_List[Index]->GetIndex();

	AfxAssert(uPos < m_Index.GetCount());

	m_Index.SetAt(uPos, NULL);
	}

// End of File
