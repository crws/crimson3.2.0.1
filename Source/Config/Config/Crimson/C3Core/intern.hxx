
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_ADOBE_ACROBAT       0x4000
#define IDS_CREATING_CRIMSON    0x4001 /* NOT USED */
#define IDS_DOWNLOAD_ONLY       0x4002
#define IDS_DOWN_ONLY           0x4003
#define IDS_DO_YOU_WANT_TO      0x4004
#define IDS_EDITING_IT_WITH     0x4005
#define IDS_FILE_READ_ONLY_1    0x4006
#define IDS_FILE_READ_ONLY_2    0x4007
#define IDS_FILE_WILL_BE_OPEN   0x4008
#define IDS_FOLDER              0x4009
#define IDS_FULL_ACCESS         0x400A
#define IDS_ITEMS_SKIPPED       0x400B /* NOT USED */
#define IDS_NAME_EMPTY          0x400C
#define IDS_NAME_INVALID        0x400D
#define IDS_NAME_LONG           0x400E
#define IDS_NEW_IDENT_1         0x400F
#define IDS_NEW_IDENT_2         0x4010
#define IDS_NEW_IDENT_3         0x4011
#define IDS_NO_ACCESS           0x4012
#define IDS_PARSING_CRIMSON     0x4013
#define IDS_PASSWORDS_DO_NOT    0x4014
#define IDS_PLEASE_CHECK_FOR    0x4015
#define IDS_PRIVATE             0x4016
#define IDS_PRIV_ACCESS         0x4017
#define IDS_READONLY_ACCESS     0x4018
#define IDS_READ_ONLY           0x4019
#define IDS_RECOVERED           0x401A
#define IDS_RECOVERED_COPY_OF   0x401B
#define IDS_SAVING_FILE_WILL    0x401C
#define IDS_SOME_ITEMS_IN       0x401D
#define IDS_THIS_DATABASE_WAS   0x401E
#define IDS_UNABLE_TO_OPEN      0x401F
#define IDS_UNABLE_TO_SWITCH    0x4020
#define IDS_YOU_MUST_ENTER      0x4021
#define IDS_YOU_MUST_ENTER_2    0x4022

// End of File

#endif
