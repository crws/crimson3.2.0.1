
#include "intern.hpp"

#include "rc4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Uploadable File Image
//

// Dynamic Class

AfxImplementDynamicClass(CFileImage, CItem);

// Constructor

CFileImage::CFileImage(void)
{
	}

// Destructor

CFileImage::~CFileImage(void)
{
	}

// Download Support

BOOL CFileImage::MakeInitData(CInitData &Init)
{
	if( m_pDbase->HasImage() ) {

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			CTreeFile Tree;

			if( Tree.OpenSave(Stream) ) {

				Tree.PutObject(L"C3Data");

				m_pDbase->Save(Tree);

				Tree.EndObject();

				Tree.Close();

				HGLOBAL hData = Stream.TakeOver();

				UINT    uSize = GlobalSize(hData);

				HGLOBAL	hCopy = GlobalAlloc(GHND, uSize);

				if( hCopy ) {

					PBYTE pData = PBYTE(GlobalLock(hData));

					PBYTE pCopy = PBYTE(GlobalLock(hCopy));

					UINT  uCopy = fastlz_compress(pData, uSize, pCopy);

					if( FALSE ) {

						FILE *p = fopen("c:\\temp\\Crimson\\work\\out.cd31", "wb");

						if( p ) {

							putc('L', p);

							putc('Z', p);

							fwrite(pCopy, 1, uCopy, p);

							fclose(p);
							}
						}

					GlobalUnlock(hData);

					Crypto(pCopy, uCopy, 1);

					Init.AddByte('E');

					Init.AddByte('1');

					Init.AddData(pCopy, uCopy);

					GlobalUnlock(hCopy);
	
					GlobalFree(hCopy);
					}

				GlobalFree(hData);
				}
			}
		}

	return TRUE;
	}

// Implementation

BOOL CFileImage::Crypto(PBYTE pData, UINT uSize, UINT uMode)
{
	switch( uMode ) {

		case 0:
			return TRUE;
		
		case 1:
			return Crypto(pData, uSize, "PineappleHead");
		}

	return FALSE;
	}

BOOL CFileImage::Crypto(PBYTE pData, UINT uSize, PCSTR pPass)
{
	rc4_key key;

	prepare_key(PBYTE(pPass), strlen(pPass), &key);

	rc4(pData, uSize, &key);

	return TRUE;
	}

// End of File
