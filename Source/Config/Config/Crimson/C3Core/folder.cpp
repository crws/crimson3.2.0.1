
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Folder Item
//

// Dynamic Class

AfxImplementDynamicClass(CFolderItem, CMetaItem);

// Constructor

CFolderItem::CFolderItem(void)
{
	m_Private = 0;

	m_fLegacy = TRUE;
	}

// Item Naming

CString CFolderItem::GetHumanName(void) const
{
	return m_Name;
	}

// Persistance

void CFolderItem::Init(void)
{
	CMetaItem::Init();
	
	m_fLegacy = FALSE;
	}

void CFolderItem::PostLoad(void)
{
	CMetaItem::PostLoad();

	if( m_fLegacy ) {

		if( !GetHumanName().CompareN(L"Private") ) {
			
			m_Private = 1;
			
			m_fLegacy = FALSE;
			}		
		}
	}

// Meta Data Creation

void CFolderItem::AddMetaData(void)
{
	Meta_AddString(Name);

	CMetaItem::AddMetaData();

	Meta_AddInteger(Private);
	Meta_AddBoolean(Legacy);

	Meta_SetName((IDS_FOLDER));
	}

// End of File
