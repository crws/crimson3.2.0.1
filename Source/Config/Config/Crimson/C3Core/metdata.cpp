
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Meta Data Entry
//

// Constructors

CMetaData::CMetaData(void)
{
	m_Type = 0;

	m_Pos  = 0;
	}

CMetaData::CMetaData(PCTXT pTag, UINT Type, UINT Pos)
{
	m_Tag  = pTag;

	m_Type = Type;

	m_Pos  = Pos;
	}

// Attributes

CString CMetaData::GetTag(void) const
{
	return m_Tag;
	}

UINT CMetaData::GetType(void) const
{
	return m_Type;
	}

UINT CMetaData::GetPos(void) const
{
	return m_Pos;
	}

// Data Read

PCTXT CMetaData::ReadString(PCVOID pItem) const
{
	AfxAssert(m_Type == metaString);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CString *) pData)[0];
	}

UINT CMetaData::ReadInteger(PCVOID pItem) const
{
	AfxAssert(m_Type == metaInteger);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((UINT *) pData)[0];
	}

number CMetaData::ReadNumber(PCVOID pItem) const
{
	AfxAssert(m_Type == metaNumber);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	// cppcheck-suppress invalidPointerCast

	return ((number *) pData)[0];
	}

CGuid CMetaData::ReadGuid(PCVOID pItem) const
{
	AfxAssert(m_Type == metaGUID);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CGuid *) pData)[0];
	}

// Data Write

void CMetaData::WriteString(PVOID pItem, PCTXT pText) const
{
	AfxAssert(m_Type == metaString);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	((CString *) pData)[0] = pText;
	}

void CMetaData::WriteString(PVOID pItem, CString const &Text) const
{
	AfxAssert(m_Type == metaString);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	((CString *) pData)[0] = Text;
	}

void CMetaData::WriteInteger(PVOID pItem, UINT uValue) const
{
	AfxAssert(m_Type == metaInteger);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	((UINT *) pData)[0] = uValue;
	}

void CMetaData::WriteNumber(PVOID pItem, number value) const
{
	AfxAssert(m_Type == metaInteger);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	// cppcheck-suppress invalidPointerCast

	((number *) pData)[0] = value;
	}

void CMetaData::WriteGuid(PVOID pItem, CGuid const &Guid) const
{
	AfxAssert(m_Type == metaGUID);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	((CGuid *) pData)[0] = Guid;
	}

// Referencing

CPoint & CMetaData::GetPoint(PVOID pItem) const
{
	AfxAssert(m_Type == metaPoint);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CPoint *) pData)[0];
	}

CRect & CMetaData::GetRect(PVOID pItem) const
{
	AfxAssert(m_Type == metaRect);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CRect *) pData)[0];
	}

R2R & CMetaData::GetR2R(PVOID pItem) const
{
	AfxAssert(m_Type == metaR2R);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((R2R *) pData)[0];
	}

CByteArray & CMetaData::GetBlob(PVOID pItem) const
{
	AfxAssert(m_Type == metaBlob);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CByteArray *) pData)[0];
	}

CWordArray & CMetaData::GetWide(PVOID pItem) const
{
	AfxAssert(m_Type == metaWide);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CWordArray *) pData)[0];
	}

CLongArray & CMetaData::GetHuge(PVOID pItem) const
{
	AfxAssert(m_Type == metaHuge);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CLongArray *) pData)[0];
	}

CItem * & CMetaData::GetObject(PVOID pItem) const
{
	AfxAssert(m_Type >= metaObject);

	BYTE *pData = ((BYTE *) pItem) + m_Pos;

	return ((CItem **) pData)[0];
	}

// Data Control

void CMetaData::SetDirty(PVOID pItem) const
{
	((CItem *) pItem)->SetDirty();
	}

// End of File
