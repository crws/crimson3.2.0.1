
#include "intern.hpp"

#include "rc4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Memory Text Stream
//

// Constructor

CTextStreamMemory::CTextStreamMemory(void)
{
	m_hBlock = NULL;
	
	m_fPrev  = FALSE;

	m_fSpec  = FALSE;

	m_fFail  = FALSE;
	}

// Destructor

CTextStreamMemory::~CTextStreamMemory(void)
{
	Close();
	}

// Management

BOOL CTextStreamMemory::OpenLoad(HGLOBAL hBlock)
{
	return OpenLoad(hBlock, FALSE);
	}

BOOL CTextStreamMemory::OpenLoad(HGLOBAL hBlock, BOOL fFree)
{
	if( !m_hBlock ) {

		m_hBlock = hBlock;

		m_pData  = PBYTE(GlobalLock(m_hBlock));

		m_uSize  = GlobalSize(m_hBlock);

		m_uPos   = 0;

		m_fFree  = fFree;

		m_fSave  = FALSE;

		m_fANSI  = IsANSI();

		return TRUE;
		}

	return FALSE;
	}

BOOL CTextStreamMemory::OpenSave(void)
{
	if( !m_hBlock ) {

		m_uSize  = 64 * 1024;

		m_uPos   = 0;

		m_hBlock = GlobalAlloc(GHND, m_uSize);

		AfxAssume(m_hBlock);

		m_pData  = PBYTE(GlobalLock(m_hBlock));

		m_fFree  = TRUE;

		m_fSave  = TRUE;

		m_fANSI  = TRUE;

		return m_pData ? TRUE : FALSE;
		}

	return FALSE;
	}

HGLOBAL CTextStreamMemory::TakeOver(void)
{
	if( m_hBlock ) {

		HGLOBAL hBlock;
		
		hBlock = m_hBlock;

		GlobalUnlock(hBlock);

		if( m_fSave ) {

			m_uSize = m_uPos + (m_fANSI ? 1 : 2);

			hBlock  = GlobalReAlloc( hBlock,
						 m_uSize,
						 0
						 );
			}

		m_hBlock = NULL;

		m_pData  = NULL;

		m_fFree  = FALSE;

		return hBlock;
		}

	return NULL;
	}

void CTextStreamMemory::Close(void)
{
	if( m_hBlock ) {

		if( m_pData ) {

			GlobalUnlock(m_hBlock);

			m_pData = NULL;
			}

		if( m_fFree ) {

			GlobalFree(m_hBlock);

			m_fFree = FALSE;
			}

		m_hBlock = NULL;
		}
	}

// File Transfer

BOOL CTextStreamMemory::LoadFromFile(CFilename const &Name)
{
	HANDLE hFile = Name.OpenReadSeq();

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT uData = GetFileSize(hFile, NULL);

		if( uData ) {

			HANDLE hData = GlobalAlloc(GHND, uData);

			if( hData ) {

				PBYTE  pData = PBYTE(GlobalLock(hData));

				DWORD  uRead = 0;

				ReadFile   (hFile, pData, uData, &uRead, NULL);

				CloseHandle(hFile);

				if( uRead == uData ) {

					if( m_fSpec ) {

						GlobalUnlock(hData);

						hData = GlobalReAlloc( hData,
								       uData + 2,
								       GMEM_MOVEABLE
								       );

						pData = PBYTE(GlobalLock(hData));

						pData[uData+0] = 0;
						
						pData[uData+1] = 0;

						GlobalUnlock(hData);

						return OpenLoad(hData, TRUE);						
						}

					if( pData[0] == 'E' ) {

						if( isdigit(pData[1]) ) {

							UINT uMode = pData[1] - '0';

							Crypto(pData + 2, uData - 2, uMode);

							pData[0] = 'L';

							pData[1] = 'Z';
							}
						}

					if( pData[0] == 'L' && pData[1] == 'Z' ) {
				
						for( UINT uGrow = 10;; uGrow <<= 1 ) {

							UINT   uCopy = uData * uGrow;

							HANDLE hCopy = GlobalAlloc(GHND, uCopy);

							if( hCopy ) {

								PBYTE pCopy = PBYTE(GlobalLock(hCopy));

								UINT  uUsed = fastlz_decompress( pData+2,
												 uData-2,
												 pCopy,
												 uCopy
												 );

								if( uUsed == 0 || uUsed == uCopy ) {

									GlobalUnlock(hCopy);

									GlobalFree  (hCopy);

									continue;
									}

								GlobalUnlock(hCopy);

								GlobalUnlock(hData);
				
								GlobalFree  (hData);

								hCopy = GlobalReAlloc( hCopy,
										       uUsed + 2,
										       GMEM_MOVEABLE
										       );

								AfxAssume(hCopy);

								pCopy = PBYTE(GlobalLock(hCopy));

								AfxAssume(pCopy);

								pCopy[uUsed+0] = 0;
								
								pCopy[uUsed+1] = 0;

								GlobalUnlock(pCopy);

								return OpenLoad(hCopy, TRUE);
								}

							break;
							}
						}
					else {
						GlobalUnlock(hData);

						hData = GlobalReAlloc( hData,
								       uData + 2,
								       GMEM_MOVEABLE
								       );

						AfxAssume(hData);

						pData = PBYTE(GlobalLock(hData));

						AfxAssume(pData);

						pData[uData+0] = 0;
						
						pData[uData+1] = 0;

						GlobalUnlock(hData);

						return OpenLoad(hData, TRUE);
						}
					}

				GlobalUnlock(hData);

				GlobalFree  (hData);
				}
			}
		}

	return FALSE;
	}

BOOL CTextStreamMemory::SaveToFile(CFilename const &Name, UINT uMode)
{
	UINT    uSize = m_uPos;

	BOOL    fCopy = FALSE;

	HGLOBAL	hCopy = NULL;

	PBYTE   pCopy = NULL;

	UINT    uCopy = 0;

	UINT    uCode = 0;

	if( uMode == saveRaw ) {

		hCopy = m_hBlock;

		pCopy = PBYTE(GlobalLock(hCopy));

		uCopy = uSize;
		}
	else {
		if( (hCopy = GlobalAlloc(GHND, uSize)) ) {

			PBYTE pData = PBYTE(GlobalLock(m_hBlock));

			if( pData ) {

				pCopy = PBYTE(GlobalLock(hCopy));

				uCopy = fastlz_compress(pData, uSize, pCopy);

				GlobalUnlock(m_hBlock);

				if( uMode == saveSecure ) {

					uCode = 1;

					Crypto(pCopy, uCopy, uCode);
					}

				fCopy = TRUE;
				}
			else {
				GlobalFree(hCopy);

				return FALSE;
				}
			}
		else
			return FALSE;
		}

	if( Name.Exists() ) {

		CFilename Path = Name.GetDirectory();

		CFilename Work = L"";

		CFilename Park = L"";

		Work.MakeTemporary(Path);

		Park.MakeTemporary(Path);

		HANDLE hFile = Work.OpenWrite();

		if( hFile != INVALID_HANDLE_VALUE ) {

			DWORD uDone = 0;

			if( fCopy ) {

				WriteSig(hFile, uCode);
				}

			WriteFile(hFile, pCopy, uCopy, &uDone, NULL);

			if( uDone == uCopy ) {

				CloseHandle (hFile);

				if( MoveFile(Name, Park) ) {

					if( MoveFile(Work, Name) ) {

						if( DeleteFile(Park) ) {

							GlobalUnlock(hCopy);

							if( fCopy ) {

								GlobalFree(hCopy);
								}

							return TRUE;
							}

						DeleteFile(Name);
						}

					MoveFile(Park, Name);
					}
				}
			}
		}
	else {
		HANDLE hFile = Name.OpenWrite();

		if( hFile != INVALID_HANDLE_VALUE ) {

			DWORD uDone = 0;

			if( fCopy ) {

				WriteSig(hFile, uCode);
				}

			WriteFile(hFile, pCopy, uCopy, &uDone, NULL);

			if( uDone == uCopy ) {

				CloseHandle (hFile);

				GlobalUnlock(hCopy);

				if( fCopy ) {

					GlobalFree(hCopy);
					}

				return TRUE;
				}
			}
		}

	GlobalUnlock(hCopy);

	if( fCopy ) {

		GlobalFree(hCopy);
		}

	return FALSE;
	}

BOOL CTextStreamMemory::SaveToFile(CFilename const &Name)
{
	return SaveToFile(Name, saveCompress);
	}

// Attributes

BOOL CTextStreamMemory::IsWide(void) const
{
	return !m_fANSI;
	}

UINT CTextStreamMemory::GetSize(void) const
{
	AfxAssert(m_fSave == 0);

	return m_uSize;
	}

UINT CTextStreamMemory::GetRead(void) const
{
	AfxAssert(m_fSave == 0);

	return m_uPos;
	}

BOOL CTextStreamMemory::SaveSuccessful(void) const
{
	return !m_fFail;
	}

// Operations

void CTextStreamMemory::SetWide(void)
{
	AfxAssert(m_uPos  == 0);

	AfxAssert(m_fSave == 1);

	m_fANSI = FALSE;

	PutLine(L"\xFEFF");
	}

void CTextStreamMemory::SetPrev(void)
{
	m_fPrev = TRUE;
	}

void CTextStreamMemory::SetSpecial(void)
{
	m_fSpec = TRUE;
	}

BOOL CTextStreamMemory::GetLine(PTXT pText, UINT uSize)
{
	if( m_fANSI ) {

		PBYTE pData = PBYTE(m_pData + m_uPos);

		PBYTE pEnd  = PBYTE(m_pData + m_uSize);

		if( pData < pEnd && *pData ) {

			UINT uPos = 0;

			while( uPos < uSize  - 1 ) {

				BYTE bData = *pData++;

				if( !m_fPrev ) {
					
					if( bData == '#' ) {

						if( *pData == '#' ) {

							pText[uPos++] = *pData++;
							}
						else {
							BYTE b1 = FromHex(*pData++);
							BYTE b2 = FromHex(*pData++);
							BYTE b3 = FromHex(*pData++);
							BYTE b4 = FromHex(*pData++);

							pText[uPos++] = WCHAR((b1<<12)|(b2<<8)|(b3<<4)|b4);
							}

						continue;
						}
					}

				pText[uPos++] = bData;

				if( bData == 0x0A || bData == 0x0D ) {

					while( uPos < uSize - 1 ) {

						BYTE bData = *pData++;

						if( bData == 0x0A || bData == 0x0D ) {

							pText[uPos++] = bData;

							continue;
							}

						pData--;
						
						break;
						}

					break;
					}

				if( pData == pEnd ) {

					// NOTE -- Drop junk at end of file.

					return FALSE;
					}
				}

			pText[uPos] = 0;

			m_uPos      = pData - m_pData;

			return TRUE;
			}
		}
	else {
		PTXT pData = PTXT(m_pData + m_uPos);

		PTXT pEnd  = PTXT(m_pData + m_uSize);

		if( pData < pEnd && *pData ) {

			UINT uPos = 0;

			while( uPos < uSize - 1 ) {

				TCHAR cData   = *pData++;

				pText[uPos++] = cData;

				if( cData == 0x0A || cData == 0x0D ) {

					while( uPos < uSize - 1 ) {

						TCHAR cData = *pData++;

						if( cData == 0x0A || cData == 0x0D ) {

							pText[uPos++] = cData;

							continue;
							}

						pData--;
						
						break;
						}

					break;
					}

				if( pData == pEnd ) {

					// NOTE -- Drop junk at end of file.

					return FALSE;
					}
				}

			pText[uPos] = 0;

			m_uPos      = PBYTE(pData) - m_pData;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTextStreamMemory::PutLine(PCTXT pText)
{
	if( m_fFail ) {

		return FALSE;
		}

	UINT uLen = wstrlen(pText);

	UINT uEnd = m_uPos + 5 * uLen + 1;

	if( uEnd > m_uSize  ) {

		GlobalUnlock(m_hBlock);

		HGLOBAL tempBlock = GlobalReAlloc(m_hBlock, 2 * m_uSize, GMEM_MOVEABLE);

		if( tempBlock != NULL ) {

			m_hBlock = tempBlock;
			}
		else {
			m_fFail = TRUE;

			#pragma warning(suppress: 6001)

			m_pData  = PBYTE(GlobalLock(m_hBlock));

			return FALSE;
			}

		m_pData  = PBYTE(GlobalLock(m_hBlock));

		m_uSize  = GlobalSize(m_hBlock);
		}

	if( m_fANSI ) {

		AfxAssume(m_pData);

		PBYTE pData = m_pData + m_uPos;

		PCSTR pHex  = "0123456789ABCDEF";

		while( *pText ) {

			if( m_fSpec ) {
				
				*pData++ = BYTE(*pText);

				pText++;

				continue;
				}

			if( *pText == '#' ) {

				*pData++ = '#';

				*pData++ = '#';

				pText++;

				continue;
				}

			if( HIBYTE(*pText) || *pText == 0x7F ) {

				*pData++ = '#';
				
				*pData++ = pHex[(*pText >> 12) & 15];
				*pData++ = pHex[(*pText >>  8) & 15];
				*pData++ = pHex[(*pText >>  4) & 15];
				*pData++ = pHex[(*pText >>  0) & 15];
				}
			else
				*pData++ = BYTE(*pText);

			pText++;
			}

		*pData = 0;

		m_uPos = pData - m_pData;
		}
	else {
		AfxAssume(m_pData);

		PTXT pData = PTXT(m_pData + m_uPos);

		UINT uSize = uLen * sizeof(WCHAR);

		memcpy(pData, pText, uSize);

		pData[uLen] = 0;

		m_uPos = m_uPos + uSize;
		}

	return TRUE;
	}

void CTextStreamMemory::Rewind(void)
{
	m_uPos = 0;
	}

// Implementation

BOOL CTextStreamMemory::IsANSI(void)
{
	if( m_pData[0] == 0xFF ) {
		
		if( m_pData[1] == 0xFE ) {

			m_pData += 2;

			return FALSE;
			}
		}

	for( UINT n = 0; n < m_uSize; n++ ) {

		if( m_pData[n+0] == 0x0D ) {

			if( m_pData[n+1] == 0x0A ) {

				return TRUE;
				}

			if( m_pData[n+1] == 0x00 ) {

				return FALSE;
				}
			}
		}

	return FALSE;
	}

BYTE CTextStreamMemory::FromHex(TCHAR cData)
{
	if( cData >= '0' && cData <= '9' ) {
		
		return BYTE(cData - '0' + 0x00);
		}

	if( cData >= 'A' && cData <= 'F' ) {
		
		return BYTE(cData - 'A' + 0x0A);
		}
	
	if( cData >= 'a' && cData <= 'f' ) {
		
		return BYTE(cData - 'a' + 0x0A);
		}

	return 0;
	}

BOOL CTextStreamMemory::Crypto(PBYTE pData, UINT uSize, UINT uCode)
{
	switch( uCode ) {

		case 0:
			return TRUE;
		
		case 1:
			return Crypto(pData, uSize, "PineappleHead");
		}

	return FALSE;
	}

BOOL CTextStreamMemory::Crypto(PBYTE pData, UINT uSize, PCSTR pPass)
{
	rc4_key key;

	prepare_key(PBYTE(pPass), strlen(pPass), &key);

	rc4(pData, uSize, &key);

	return TRUE;
	}

BOOL CTextStreamMemory::WriteSig(HANDLE hFile, UINT uCode)
{
	DWORD uDone;

	if( !uCode ) {

		BYTE bSig[] = { 'L', 'Z' };

		WriteFile(hFile, bSig, sizeof(bSig), &uDone, NULL);

		return TRUE;
		}

	BYTE bSig[] = { 'E', '0' };

	bSig[1] = BYTE('0' + uCode);

	WriteFile(hFile, bSig, sizeof(bSig), &uDone, NULL);

	return TRUE;
	}

// End of File
