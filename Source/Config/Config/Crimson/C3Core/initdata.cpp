
#include "intern.hpp"

#include "rc4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Initialization Data
//

// Constructor

CInitData::CInitData(void)
{
	m_fBig    = TRUE;

	m_fAlign  = FALSE;

	m_uOffset = 0;

	m_Data.SetDelta(1024);
	}

// Array Access

CInitData::operator CByteArray & (void)
{
	return m_Data;
	}

// Attributes

UINT CInitData::GetCount(void) const
{
	return m_Data.GetCount();
	}

PCBYTE CInitData::GetPointer(void) const
{
	return m_Data.GetPointer();
	}

DWORD CInitData::GetCRC(void) const
{
	return CRC32(GetPointer(), GetCount());
	}

BOOL CInitData::GetOrder(void) const
{
	return m_fBig;
	}

BOOL CInitData::GetAlign(void) const
{
	return m_fAlign;
	}

// Operations

void CInitData::Empty(void)
{
	m_Data.Empty();
	}

void CInitData::SetCount(UINT uCount)
{
	m_uOffset = uCount;

	m_Data.SetCount(uCount);
	}

void CInitData::SetOffset(UINT uOffset)
{
	m_uOffset = uOffset;
	}

BOOL CInitData::Compress(void)
{
	UINT uSize = GetCount();

	if( uSize > 128 ) {

		PCBYTE pData = GetPointer();

		PBYTE  pCopy = New BYTE [ uSize + 1024 ];

		UINT   uCopy = fastlz_compress(pData, uSize, pCopy);

		if( uCopy < uSize ) {

			m_Data.Init(pCopy, uCopy, uSize);

			return TRUE;
			}

		delete [] pCopy;
		}

	return FALSE;
	}

// Mode Control

void CInitData::SetOrder(BOOL fBig)
{
	m_fBig = fBig;
	}

void CInitData::SetAlign(BOOL fAlign)
{
	m_fAlign = fAlign;
	}

// Item Addition

void CInitData::AddItem(UINT uMethod, CItem *pItem)
{
	if( uMethod == itemSimple ) {

		if( pItem ) {

			pItem->MakeInitData(ThisObject);
			}

		return;
		}

	if( uMethod == itemSized ) {

		if( pItem ) {

			AddLong(0);

			UINT  p = m_Data.GetCount();

			pItem->MakeInitData(ThisObject);

			DWORD s = m_Data.GetCount() - p;

			m_Data.SetAt(p-4, m_fBig ? HIBYTE(HIWORD(s)) : LOBYTE(LOWORD(s)));

			m_Data.SetAt(p-3, m_fBig ? LOBYTE(HIWORD(s)) : HIBYTE(LOWORD(s)));

			m_Data.SetAt(p-2, m_fBig ? HIBYTE(LOWORD(s)) : LOBYTE(HIWORD(s)));

			m_Data.SetAt(p-1, m_fBig ? LOBYTE(LOWORD(s)) : HIBYTE(HIWORD(s)));
			}
		else
			AddLong(0);

		return;
		}

	if( uMethod == itemVirtual ) {

		if( pItem ) {

			AddByte(1);

			pItem->MakeInitData(ThisObject);
			}
		else
			AddByte(0);

		return;
		}

	if( uMethod == itemHandle ) {

		if( pItem ) {

			WORD hItem = 0xAAAA;

			AddWord(hItem);
			}
		else
			AddWord(0);

		return;
		}

	if( uMethod == itemEncrypt ) {

		if( pItem ) {

			CInitData Temp;

			Temp.SetAlign(m_fAlign);

			Temp.SetOrder(m_fBig);

			pItem->MakeInitData(Temp);

			AddByte(2);

			AddCryp(Temp);
			}
		else
			AddByte(0);

		return;
		}
	}

// Bulk Addition

void CInitData::AddData(PCBYTE pData, UINT uCount)
{
	m_Data.Append(PCBYTE(pData), uCount * sizeof(BYTE));
	}

void CInitData::AddData(PCWORD pData, UINT uCount)
{
	m_Data.Append(PCBYTE(pData), uCount * sizeof(WORD));
	}

void CInitData::AddData(CString const &Data, UINT uCount)
{
	PCWORD pData = PCWORD(PCTXT(Data));

	UINT   uSize = min(uCount, Data.GetLength());

	UINT n, p;

	for( n = 0; n < uSize; n++ ) {

		AddByte(BYTE(pData[n]));
		}

	for( p = n; p < uCount; p++ ) {

		AddByte(0);
		}
	}

void CInitData::AddGuid(CGuid const &Guid)
{
	AddData(PCBYTE(&Guid), sizeof(Guid));
	}

void CInitData::AddCryp(PCTXT pText)
{
	CInitData Temp;

	Temp.SetAlign(m_fAlign);

	Temp.SetOrder(m_fBig);

	Temp.AddText(pText);

	AddCryp(Temp);
	}

void CInitData::AddCryp(CInitData const &Data)
{
	PCBYTE pData = Data.GetPointer();

	UINT   uSize = Data.GetCount();
	
	AddCryp(pData, uSize);
	}

void CInitData::AddCryp(PCBYTE pData, UINT uCount)
{
	UINT  uCopy = uCount;

	PBYTE pCopy = New BYTE [ uCopy ];

	memcpy(pCopy, pData, uCopy);

	Crypto(pCopy, uCopy, 1);

	AddWord(WORD(uCopy));

	AddData(pCopy, uCopy);

	delete [] pCopy;
	}

// Logic

void CInitData::OrByte(UINT uOffset, BYTE bData)
{
	BYTE bTemp = BYTE(m_Data.GetAt(uOffset) | bData);

	m_Data.SetAt(uOffset, bTemp);
	}

// Data Addition

void CInitData::AddByte(BYTE bData)
{
	m_Data.Append(bData);
	}

void CInitData::AddWord(WORD wData)
{
	if( m_fAlign ) {

		while( m_Data.GetCount() & 1 ) {
			
			AddByte(0);
			}
		}

	AddByte(m_fBig ? HIBYTE(wData) : LOBYTE(wData));

	AddByte(m_fBig ? LOBYTE(wData) : HIBYTE(wData));
	}

void CInitData::AddLong(DWORD dwData)
{
	if( m_fAlign ) {

		while( m_Data.GetCount() & 3 ) {
			
			AddByte(0);
			}
		}

	AddWord(m_fBig ? HIWORD(dwData) : LOWORD(dwData));

	AddWord(m_fBig ? LOWORD(dwData) : HIWORD(dwData));
	}

void CInitData::AddText(PCTXT pData)
{
	UINT uLength = wstrlen(pData);

	AddWord(WORD(uLength + 1));

	for( UINT n = 0; n <= uLength; n++ ) {

		AddWord(pData[n]);
		}
	}

void CInitData::AddIntl(PCTXT pData, PCTXT pDefault)
{
	CString Text;

	UINT    uLen;

	CString Data(pData);

	CString Norm(pDefault);

	if( TRUE ) {

		CStringArray List;

		Data.Tokenize(List, '\\');

		UINT uCount = List.GetCount();
		
		for( UINT n = 0; n < uCount; n++ ) {

			if( List[n].IsEmpty() ) {

				if( n && !List[0].IsEmpty() ) {

					Text += List[0];
					}
				else
					Text += Norm;
				}
			else
				Text += List[n];

			Text += '\\';
			}

		if( Text.IsEmpty() ) {

			Text  = Norm;

			Text += '\\';
			}
		}

	if( (uLen = Text.GetLength()) ) {

		AddWord(WORD(uLen + 1));

		for( UINT n = 0; n < uLen; n++ ) {

			if( Text[n] == '\\' ) {

				AddWord(0);

				continue;
				}

			AddWord(Text[n]);
			}

		AddWord(0xFFFF);
		
		return;
		}

	AddWord(0x0000);

	AddWord(0xFFFF);
	}

void CInitData::AddIntl(PCTXT pData)
{
	AddIntl(pData, L"");
	}

// Data Adjustment

void CInitData::SetWord(UINT uPos, WORD wData)
{
	m_Data.SetAt(uPos + 0, m_fBig ? HIBYTE(wData) : LOBYTE(wData));

	m_Data.SetAt(uPos + 1, m_fBig ? LOBYTE(wData) : HIBYTE(wData));
	}

// Data Retrieval

BYTE CInitData::GetByte(UINT uOffset)
{
	m_uOffset = uOffset + 1;

	return m_Data.GetAt(uOffset);
	}

WORD CInitData::GetWord(UINT uOffset)
{
	if( m_fAlign ) {

		while( uOffset & 1 ) {

			uOffset++;
			}
		}

	m_uOffset = uOffset + 2;

	BYTE lo = m_Data.GetAt(uOffset);

	BYTE hi = m_Data.GetAt(uOffset+1);

	return MAKEWORD(m_fBig ? hi : lo, m_fBig ? lo : hi);
	}

DWORD CInitData::GetLong(UINT uOffset)
{
	if( m_fAlign ) {

		while( uOffset & 3 ) {

			uOffset++;
			}
		}

	m_uOffset = uOffset + 4;

	WORD lo = GetWord(uOffset);

	WORD hi = GetWord(uOffset+2);
	
	return MAKELONG(m_fBig ? hi : lo, m_fBig ? lo : hi);
	}

BYTE CInitData::GetByte(void)
{
	return GetByte(m_uOffset);
	}

WORD CInitData::GetWord(void)
{
	return GetWord(m_uOffset);
	}

DWORD CInitData::GetLong(void)
{
	return GetLong(m_uOffset);
	}

// Implementation

BOOL CInitData::Crypto(PBYTE pData, UINT uSize, UINT uMode)
{
	switch( uMode ) {

		case 0:
			return TRUE;
		
		case 1:
			return Crypto(pData, uSize, "PineappleHead");
		}

	return FALSE;
	}

BOOL CInitData::Crypto(PBYTE pData, UINT uSize, PCSTR pPass)
{
	rc4_key key;

	prepare_key(PBYTE(pPass), strlen(pPass), &key);

	rc4(pData, uSize, &key);

	return TRUE;
	}
// End of File
