
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linked List of Items
//

// Runtime Class

AfxImplementRuntimeClass(CItemList, CItem);

// Constructor

CItemList::CItemList(void)
{
	m_Class  = NULL;

	m_Assume = NULL;
	
	m_Fixed  = 0;
	}

CItemList::CItemList(CLASS Class)
{
	m_Class  = Class;

	m_Assume = NULL;
	
	m_Fixed  = 0;
	}

// Destructor

CItemList::~CItemList(void)
{
	DeleteAllItems(FALSE);
	}

// Item Access

CItem * CItemList::GetItem(INDEX Index) const
{
	return m_List[Index];
	}

CItem * CItemList::GetItem(UINT uPos) const
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		if( !uPos-- ) {

			return m_List[Index];
			}
		
		m_List.GetNext(Index);
		}

	return NULL;
	}

// List Lookup

CItem * CItemList::operator [] (INDEX Index) const
{
	return GetItem(Index);
	}

CItem * CItemList::operator [] (UINT uPos) const
{
	return GetItem(uPos);
	}

// List Enumeration

INDEX CItemList::GetHead(void) const
{
	return m_List.GetHead();
	}

INDEX CItemList::GetTail(void) const
{
	return m_List.GetTail();
	}

BOOL CItemList::GetNext(INDEX &Index) const
{
	return m_List.GetNext(Index);
	}

BOOL CItemList::GetPrev(INDEX &Index) const
{
	return m_List.GetPrev(Index);
	}

BOOL CItemList::Failed(INDEX Index) const
{
	return m_List.Failed(Index);
	}

// Item Naming

CString CItemList::GetFixedName(void) const
{
	return CPrintf(L"%8.8X", m_Fixed);
	}

void CItemList::SetFixedName(CString Name)
{
	m_Fixed = wcstoul(Name, NULL, 16);
	}

// Persistance

void CItemList::Init(void)
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->Init();

		m_List.GetNext(Index);
		}

	m_Fixed = m_pDbase->AllocFixedInt();
	}

void CItemList::Kill(void)
{
	DeleteAllItems(TRUE);
	}

void CItemList::Load(CTreeFile &File)
{
	if( File.IsName(L"Fixed") ) {

		File.GetName();

		m_Fixed = File.GetValueAsInteger();
		}

	while( !File.IsEndOfData() ) {

		CString Name = File.GetName();

		CItem *pItem = CreateItem(Name);

		if( pItem ) {

			m_List.Append(pItem);

			File.GetObject();

			pItem->Load(File);

			File.EndObject();
			}
		}
	}

void CItemList::PostLoad(void)
{
	if( m_pParent ) {

		m_pDbase->CheckFixed(m_Fixed);
		}

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->PostLoad();

		m_List.GetNext(Index);
		}
	}

void CItemList::PostSave(void)
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->PostSave();

		m_List.GetNext(Index);
		}
	}

void CItemList::PreSnapshot(void)
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->PreSnapshot();

		m_List.GetNext(Index);
		}
	}

void CItemList::PreCopy(void)
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->PreCopy();

		m_List.GetNext(Index);
		}
	}

void CItemList::PostPaste(void)
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->PostPaste();

		m_List.GetNext(Index);
		}

	m_Fixed = m_pDbase->AllocFixedInt();
	}

void CItemList::Save(CTreeFile &File)
{
	File.PutValue(L"Fixed", m_Fixed);

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		if( !m_Class ) {

			CString Name = pItem->GetClassName();

			File.PutObject(Name.Mid(1));
			}
		else
			File.PutElement();

		pItem->Save(File);

		File.EndObject();

		m_List.GetNext(Index);
		}
	}

// Download Support

void CItemList::PrepareData(void)
{
	CItem::PrepareData();

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->PrepareData();

		m_List.GetNext(Index);
		}
	}

BOOL CItemList::MakeInitData(CInitData &Init)
{
	CItem::MakeInitData(Init);

	Init.AddWord(WORD(m_List.GetCount()));

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		m_List[Index]->MakeInitData(Init);

		m_List.GetNext(Index);
		}

	return TRUE;
	}

// Attributes

UINT CItemList::GetItemCount(void) const
{
	return m_List.GetCount();
	}

BOOL CItemList::IsHeadItem(CItem const *pItem) const
{
	return m_List.GetCount() && m_List[m_List.GetHead()] == pItem;
	}

BOOL CItemList::IsTailItem(CItem const *pItem) const
{
	return m_List.GetCount() && m_List[m_List.GetTail()] == pItem;
	}

INDEX CItemList::FindItemIndex(CItem const *pItem) const
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		if( m_List[Index] == pItem ) {

			return Index;
			}

		m_List.GetNext(Index);
		}

	return m_List.Failed();
	}

UINT CItemList::FindItemPos(CItem const *pItem) const
{
	UINT  i = 0;

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		if( m_List[Index] == pItem ) {

			return i;
			}

		m_List.GetNext(Index);

		i++;
		}

	return NOTHING;
	}

// Operations

BOOL CItemList::SetItemCount(UINT uCount)
{
	AfxAssert(m_Class);

	if( uCount < m_List.GetCount() ) {

		if( uCount ) {

			UINT uExtra = m_List.GetCount() - uCount;

			while( uExtra-- ) {

				INDEX  Index = m_List.GetTail();

				CItem *pItem = m_List[Index];

				FreeIndex(pItem, Index);

				pItem->Kill();

				delete pItem;

				m_List.Remove(Index);
				}
			}
		else
			DeleteAllItems(TRUE);

		SetDirty();

		return TRUE;
		}

	if( uCount > m_List.GetCount() ) {

		UINT uExtra = uCount - m_List.GetCount();

		while( uExtra-- ) {

			CItem *pItem = CreateItem(NULL);

			pItem->Init();

			INDEX Item = m_List.Append(pItem);

			AllocIndex(pItem, Item);
			}

		SetDirty();

		return TRUE;
		}

	return FALSE;
	}

void CItemList::SetItemClass(CLASS Class)
{
	AfxAssert(m_List.IsEmpty());

	m_Class = Class;
	}

BOOL CItemList::AppendItem(CItem *pItem)
{
	if( !pItem->GetParent() ) {

		pItem->SetParent(this);

		pItem->Init();
		}
	else
		pItem->SetParent(this);

	INDEX Item = m_List.Append(pItem);

	AllocIndex(pItem, Item);

	SetDirty();

	return TRUE;
	}

INDEX CItemList::InsertItem(CItem *pItem, INDEX Index)
{
	if( !pItem->GetParent() ) {

		pItem->SetParent(this);

		pItem->Init();
		}
	else
		pItem->SetParent(this);

	INDEX Item = m_List.Insert(Index, pItem);

	AllocIndex(pItem, Item);

	SetDirty();

	return Item;
	}

INDEX CItemList::InsertItem(CItem *pItem, CItem *pBefore)
{
	return InsertItem(pItem, FindItemIndex(pBefore));
	}

INDEX CItemList::MoveItem(INDEX Index, INDEX Before)
{
	CItem *pItem = m_List[Index];

	m_List.Remove(Index);

	Index = m_List.Insert(Before, pItem);

	UpdateIndex(pItem, Index);

	SetDirty();

	return Index;
	}

INDEX CItemList::MoveItem(CItem *pItem, INDEX Before)
{
	m_List.Remove(FindItemIndex(pItem));

	INDEX Index = m_List.Insert(Before, pItem);

	UpdateIndex(pItem, Index);

	SetDirty();

	return Index;
	}

INDEX CItemList::MoveItem(CItem *pItem, CItem *pBefore)
{
	return MoveItem(pItem, FindItemIndex(pBefore));
	}

INDEX CItemList::MoveHeadward(INDEX Index)
{
	if( !m_List.Failed(Index) ) {

		INDEX b = Index;

		if( m_List.GetPrev(b) ) {

			CItem *pItem = m_List[Index];

			m_List.Remove(Index);

			Index = m_List.Insert(b, pItem);

			UpdateIndex(pItem, Index);

			SetDirty();

			return Index;
			}
		}

	return NULL;
	}

INDEX CItemList::MoveHeadward(CItem *pItem)
{
	return MoveHeadward(FindItemIndex(pItem));
	}

INDEX CItemList::MoveTailward(INDEX Index)
{
	if( !m_List.Failed(Index) ) {

		INDEX b = Index;

		if( m_List.GetNext(b) ) {

			CItem *pItem = m_List[Index];

			m_List.GetNext(b);

			m_List.Remove(Index);

			Index = m_List.Insert(b, pItem);

			UpdateIndex(pItem, Index);

			SetDirty();

			return Index;
			}
		}

	return NULL;
	}

INDEX CItemList::MoveTailward(CItem *pItem)
{
	return MoveTailward(FindItemIndex(pItem));
	}

BOOL CItemList::RemoveItem(INDEX Index)
{
	if( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		FreeIndex(pItem, Index);

		m_List.Remove(Index);

		SetDirty();

		return TRUE;
		}

	return FALSE;
	}

BOOL CItemList::RemoveItem(CItem *pItem)
{
	return RemoveItem(FindItemIndex(pItem));
	}

BOOL CItemList::DeleteItem(INDEX Index)
{
	if( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		FreeIndex(pItem, Index);

		pItem->Kill();

		delete pItem;

		m_List.Remove(Index);

		SetDirty();

		return TRUE;
		}

	return FALSE;
	}

BOOL CItemList::DeleteItem(CItem *pItem)
{
	return DeleteItem(FindItemIndex(pItem));
	}

BOOL CItemList::DeleteAllItems(BOOL fKill)
{
	ZdiCheckItem(sizeof(*this));

	if( m_List.GetCount() ) {

		INDEX Index = m_List.GetHead();

		while( !m_List.Failed(Index) ) {

			CItem *pItem = m_List[Index];

			if( fKill ) {

				FreeIndex(pItem, Index);

				pItem->Kill();
				}

			delete pItem;

			m_List.GetNext(Index);
			}

		m_List.Empty();

		return TRUE;
		}

	return FALSE;
	}

// Item Creation

CItem * CItemList::CreateItem(PCTXT pName)
{
	CLASS Class = m_Class;

	if( !Class ) {

		if( pName && *pName ) {

			Class = m_pDbase->AliasClass(pName);
			}
		else
			Class = m_Assume;
		}

	if( Class ) {

		CItem *pItem = AfxNewObject(CItem, Class);

		if( pItem ) {

			pItem->SetParent(this);

			return pItem;
			}
		}

	return NULL;
	}

// Index Management

void CItemList::AllocIndex(CItem *pItem, INDEX Index)
{
	}

void CItemList::UpdateIndex(CItem *pItem, INDEX Index)
{
	}

void CItemList::FreeIndex(CItem *pItem, INDEX Index)
{
	}

// End of File
