
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Disk Text Stream
//

// Constructor

CTextStreamDisk::CTextStreamDisk(void)
{
	m_pFile = NULL;

	m_fWide = FALSE;
	}

// Destructor

CTextStreamDisk::~CTextStreamDisk(void)
{
	Close();
	}

// Management

BOOL CTextStreamDisk::OpenLoad(PCTXT pFile)
{
	if( !m_pFile ) {

		if( (m_pFile = _wfopen(pFile, L"rb")) ) {

			BYTE b[2];

			fread(b, sizeof(BYTE), elements(b), m_pFile);

			if( b[0] == 0xFF && b[1] == 0xFE ) {

				m_fWide = TRUE;

				return TRUE;
				}

			if( b[0] == 0xFE && b[1] == 0xFF ) {

				Close();

				return FALSE;
				}

			fseek(m_pFile, 0, SEEK_SET);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTextStreamDisk::OpenSave(PCTXT pFile)
{
	if( !m_pFile ) {

		if( (m_pFile = _wfopen(pFile, L"wb")) ) {

			fputc(0xFF, m_pFile);

			fputc(0xFE, m_pFile);

			m_fWide = TRUE;

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CTextStreamDisk::Close(void)
{
	if( m_pFile ) {

		fclose(m_pFile);

		m_pFile = NULL;

		m_fWide = FALSE;
		}
	}

// Attributes

BOOL CTextStreamDisk::IsWide(void) const
{
	return m_fWide;
	}

BOOL CTextStreamDisk::HasError(void) const
{
	return ferror(m_pFile) ? TRUE : FALSE;
	}

// Operations

BOOL CTextStreamDisk::GetLine(PTXT pText, UINT uSize)
{
	if( !feof(m_pFile) ) {

		if( !m_fWide ) {

			PSTR pTemp = PSTR(_alloca(uSize));

			pTemp[0] = 0;
			
			fgets(pTemp, uSize, m_pFile);

			for( int n = 0; pText[n] = BYTE(pTemp[n]); n++ );
			}
		else {
			pText[0] = 0;

			fgetws(pText, uSize, m_pFile);
			}

		return pText[0] ? TRUE : FALSE;
		}

	return FALSE;
	}

BOOL CTextStreamDisk::PutLine(PCTXT pText)
{
	AfxAssert(m_fWide);

	fputws(pText, m_pFile);

	return TRUE;
	}

// End of File
