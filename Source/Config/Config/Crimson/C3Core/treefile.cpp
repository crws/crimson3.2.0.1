
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tree File Object
//

// File Modes

enum {
	modeIdle  = 0,
	modeRead  = 1,
	modeWrite = 2,
	};

// Lines Types

enum {
	lineEmpty   = 0,
	lineValue   = 1,
	lineObject  = 2,
	lineCollect = 3,
	};

// Constructor

CTreeFile::CTreeFile(void)
{
	m_pStream = NULL;

	m_pDivert = NULL;

	m_uMode   = modeIdle;

	m_uLine   = lineEmpty;

	m_uPad    = 16;

	m_fSkip   = FALSE;
	}

// Destructor

CTreeFile::~CTreeFile(void)
{
	if( m_uMode == modeWrite ) {

		CloseFile();
		}
	else {
		#ifdef _DEBUG

		m_uMode ? Close() : CloseFile();

		#endif

		CloseFile();
		}
	}

// Attributes

BOOL CTreeFile::GetSkipFlag(void) const
{
	return m_fSkip;
	}

UINT CTreeFile::GetPadding(void) const
{
	return m_uPad;
	}

BOOL CTreeFile::HasFile(void) const
{
	return !m_File.IsEmpty();
	}

CFilename CTreeFile::GetFile(void) const
{
	return m_File;
	}

// Management

BOOL CTreeFile::OpenLoad(ITextStream &Stream)
{
	if( !m_pStream ) {

		m_pStream = &Stream;

		ClearState();

		m_uMode = modeRead;

		ReadLine();

		if( GetName() == L"TreeFile" ) {

			GetObject();

			return TRUE;
			}

		CloseFile();

		return FALSE;
		}

	return FALSE;
	}

BOOL CTreeFile::OpenSave(ITextStream &Stream)
{
	if( !m_pStream ) {

		m_pStream = &Stream;

		ClearState();

		m_uMode = modeWrite;

		PutObject(L"TreeFile");

		return TRUE;
		}

	return FALSE;
	}

void CTreeFile::SetFile(CFilename const &File)
{
	m_File = File;
	}

void CTreeFile::Abort(void)
{
	if( m_uMode ) {

		CloseFile();
		}
	}

void CTreeFile::Close(void)
{
	AfxAssert(m_uMode);

	EndObject();

	AfxAssert(!m_uObject);

	AfxAssert(!m_uCollect);

	CloseFile();
	}

// Control

void CTreeFile::SetDivert(ITextStream *pDivert)
{
	m_pDivert = pDivert;
	}

void CTreeFile::SetPadding(UINT uPad)
{
	m_uPad = uPad;
	}

void CTreeFile::SetCompressed(void)
{
	}

// Writing

BOOL CTreeFile::PutDivert(ITextStream *pDivert)
{
	TCHAR sLine[512];

	while( pDivert->GetLine(sLine, elements(sLine)) ) {

		m_pStream->PutLine(sLine);
		}

	return TRUE;
	}

void CTreeFile::PutValue(PCTXT pName, PCTXT pValue)
{
	AfxAssert(m_uMode == modeWrite);

	UINT n = wstrlen(pValue);

	if( n ) {

		UINT c = 80;

		if( n <= c ) {

			PutName(pName);

			PutText(L"\"");

			PutText(pValue);

			PutText(L"\"\r\n");
			}
		else {
			PTXT p = PTXT(alloca(2 * c + 2));

			UINT i = 0;

			while( i < n ) {

				UINT s = min(n - i, c);

				wmemcpy(p, pValue + i, s);

				p[s] = 0;

				PutName(i ? L"." : pName);

				PutText(L"\"");

				PutText(p);

				PutText(L"\"\r\n");

				i = i + s;
				}
			}
		}
	else {
		PutName(pName);

		PutText(L"\"\"\r\n");
		}
	}

void CTreeFile::PutValue(PCTXT pName, UINT uValue)
{
	AfxAssert(m_uMode == modeWrite);

	WCHAR p[32];

	UINT  n = 0;

	n += wstrlen(_ultow(uValue, p + n, 10));

	p[n++] = '\r';
	p[n++] = '\n';
	p[n++] = '\0';

	PutName(pName);

	PutText(p);
	}

void CTreeFile::PutValue(PCTXT pName, BOOL fValue)
{
	// This version just avoids a lot of extra casts
	// to resolve ambiguity between UINT and number.

	PutValue(pName, UINT(fValue));
	}

void CTreeFile::PutValue(PCTXT pName, DWORD dwValue)
{
	// This version just avoids a lot of extra casts
	// to resolve ambiguity between UINT and number.

	PutValue(pName, UINT(dwValue));
	}

void CTreeFile::PutValue(PCTXT pName, number value)
{
	AfxAssert(m_uMode == modeWrite);

	WCHAR p[32];

	UINT  n = 0;

	n += wstrlen(RealToWide(value, p + n));

	p[n++] = '\r';
	p[n++] = '\n';
	p[n++] = '\0';

	PutName(pName);

	PutText(p);
	}

void CTreeFile::PutValue(PCTXT pName, CGuid const &Guid)
{
	AfxAssert(m_uMode == modeWrite);

	LPOLESTR p = NULL;

	StringFromCLSID(Guid, &p);

	PutName(pName);

	PutText(p);

	PutText(L"\r\n");
	}

void CTreeFile::PutValue(PCTXT pName, CPoint const &Pos)
{
	AfxAssert(m_uMode == modeWrite);

	WCHAR p[40];

	UINT  n = 0;

	n += wstrlen(_itow(Pos.x, p + n, 10)); p[n++] = ',';
	n += wstrlen(_itow(Pos.y, p + n, 10));
	
	p[n++] = '\r';
	p[n++] = '\n';
	p[n++] = '\0';

	PutName(pName);

	PutText(p);
	}

void CTreeFile::PutValue(PCTXT pName, CRect const &Rect)
{
	AfxAssert(m_uMode == modeWrite);

	WCHAR p[80];

	UINT  n = 0;

	n += wstrlen(_itow(Rect.left,   p + n, 10)); p[n++] = ',';
	n += wstrlen(_itow(Rect.top,    p + n, 10)); p[n++] = ',';
	n += wstrlen(_itow(Rect.right,  p + n, 10)); p[n++] = ',';
	n += wstrlen(_itow(Rect.bottom, p + n, 10));

	p[n++] = '\r';
	p[n++] = '\n';
	p[n++] = '\0';

	PutName(pName);

	PutText(p);
	}

void CTreeFile::PutValue(PCTXT pName, R2R const &Rect)
{
	AfxAssert(m_uMode == modeWrite);

	WCHAR p[80];

	UINT  n = 0;

	n += wstrlen(RealToWide(Rect.m_x1, p + n)); p[n++] = ',';
	n += wstrlen(RealToWide(Rect.m_y1, p + n)); p[n++] = ',';
	n += wstrlen(RealToWide(Rect.m_x2, p + n)); p[n++] = ',';
	n += wstrlen(RealToWide(Rect.m_y2, p + n));

	p[n++] = '\r';
	p[n++] = '\n';
	p[n++] = '\0';

	PutName(pName);

	PutText(p);
	}

void CTreeFile::PutValue(PCTXT pName, CByteArray const &Data)
{
	AfxAssert(m_uMode == modeWrite);

	PCBYTE pData  = Data.GetPointer();

	UINT   uCount = Data.GetCount();

	PutValue(pName, pData, uCount);
	}

void CTreeFile::PutValue(PCTXT pName, CWordArray const &Data)
{
	AfxAssert(m_uMode == modeWrite);

	PCBYTE pData  = PCBYTE(Data.GetPointer());

	UINT   uCount = 2 * Data.GetCount();

	PutValue(pName, pData, uCount);
	}

void CTreeFile::PutValue(PCTXT pName, CLongArray const &Data)
{
	AfxAssert(m_uMode == modeWrite);

	PCBYTE pData  = PCBYTE(Data.GetPointer());

	UINT   uCount = 4 * Data.GetCount();

	PutValue(pName, pData, uCount);
	}

//////////////////////////
#pragma optimize("g", off)
//////////////////////////

void CTreeFile::PutValue(PCTXT pName, PCBYTE pData, UINT uCount)
{
	AfxAssert(m_uMode == modeWrite);

	if( TRUE ) {

		WCHAR p[32] = { L"0x" };

		UINT  n     = wstrlen(p);

		n += wstrlen(_itow(uCount, p + n, 16));

		p[n++] = '\r';
		p[n++] = '\n';
		p[n++] = '\0';

		PutName(pName);

		PutText(p);
		}

	if( TRUE ) {

		PutName(L".");

		PutText(L"0x");

		UINT  uChop = 40;

		UINT  uPos  = 0;

		TCHAR sHex[200];

		for( UINT n = 0; n < uCount; n++ ) {

			sHex[uPos++] = MakeHex(*pData/16);

			sHex[uPos++] = MakeHex(*pData%16);

			if( n < uCount - 1 ) {

				if( n % uChop == uChop - 1 ) {

					sHex[uPos++] = 13;
					sHex[uPos++] = 10;
					sHex[uPos++] = 0;

					PutText(sHex);

					PutName(L".");

					PutText(L"0x");

					// cppcheck-suppress redundantAssignment
					
					uPos = 0;
					}
				}

			pData++;
			}

		sHex[uPos++] = 13;
		sHex[uPos++] = 10;
		sHex[uPos++] = 0;

		PutText(sHex);
		}
	}

//////////////////////////
#pragma optimize("",  on )
//////////////////////////
#pragma optimize("y", off)
//////////////////////////

void CTreeFile::PutCollect(PCTXT pName)
{
	AfxAssert(m_uMode == modeWrite);

	UINT n = wstrlen(pName);

	UINT a = n + 4;

	PTXT p = PTXT(alloca(2 * a + 2));

	wmemcpy(p, pName, n);

	wstrcpy(p + n, L" [\r\n");

	PutIndent();

	PutText(p);

	m_uCollect++;

	m_uIndent++;
	}

void CTreeFile::PutElement(void)
{
	PutObject(NULL);
	}

void CTreeFile::PutObject(PCTXT pName)
{
	AfxAssert(m_uMode == modeWrite);

	PutIndent();

	if( pName ) {

		UINT n = wstrlen(pName);

		UINT a = n + 4;

		PTXT p = PTXT(alloca(2 * a + 2));

		wmemcpy(p, pName, n);

		wstrcpy(p + n, L" {\r\n");

		PutText(p);
		}
	else {
		AfxAssert(m_uCollect);

		PutText(L"{\r\n");
		}

	m_uObject++;

	m_uIndent++;
	}

// Name Reading

CString const & CTreeFile::GetName(void)
{
	AfxAssert(m_uMode == modeRead);

	switch( m_uLine ) {

		case lineObject:
			
			SkipObject();

			m_fSkip = TRUE;
			
			break;

		case lineCollect:

			SkipCollect();

			m_fSkip = TRUE;

			break;

		case lineValue:

			if( SilentSkip() ) {

				SkipValue();
				}
			else {
				SkipValue();

				m_fSkip = TRUE;
				}

			break;
		}

	ParseLine();

	return m_Name;
	}

// Data Reading

BOOL CTreeFile::IsEndOfData(void)
{
	AfxAssert(m_uMode == modeRead);

	if( m_Name.IsEmpty() ) {

		if( !m_Sep.IsEmpty() ) {

			if( m_Sep == L"]" ) {
				
				return TRUE;
				}

			if( m_Sep == L"}" ) {
				
				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTreeFile::IsName(CString const &Name)
{
	return m_Name == Name;
	}

BOOL CTreeFile::IsSimpleValue(void)
{
	return m_uLine == lineValue;
	}

CString CTreeFile::GetValueAsString(void)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	CString Value;
	
	do {
		UINT n = m_Data.GetLength();

		PTXT p = PTXT(PCTXT(m_Data));

		p[n-1] = 0;

		Value  += p + 1;

		m_uLine = lineEmpty;

		ReadLine();

		} while( m_Name == L"." );

	return Value;
	}

UINT CTreeFile::GetValueAsInteger(void)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	UINT uValue = wcstoul(m_Data, NULL, 10);

	m_uLine     = lineEmpty;

	ReadLine();

	return uValue;
	}

number CTreeFile::GetValueAsNumber(void)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	number value = wcstod(m_Data, NULL);

	m_uLine      = lineEmpty;

	ReadLine();

	return value;
	}

CGuid CTreeFile::GetValueAsGuid(void)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	CGuid Value = m_Data;

	m_uLine     = lineEmpty;

	ReadLine();

	return Value;
	}

void CTreeFile::GetValue(CPoint &Pos)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	PTXT pData = PTXT(PCTXT(m_Data));

	Pos.x      = wcstoul(pData,   &pData, 10);

	Pos.y      = wcstoul(pData+1, &pData, 10);

	m_uLine    = lineEmpty;

	ReadLine();
	}

void CTreeFile::GetValue(CRect &Rect)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	PTXT pData  = PTXT(PCTXT(m_Data));

	Rect.left   = wcstoul(pData,   &pData, 10);

	Rect.top    = wcstoul(pData+1, &pData, 10);

	Rect.right  = wcstoul(pData+1, &pData, 10);

	Rect.bottom = wcstoul(pData+1, &pData, 10);

	m_uLine     = lineEmpty;

	ReadLine();
	}

void CTreeFile::GetValue(R2R &Rect)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	PTXT pData = PTXT(PCTXT(m_Data));

	Rect.m_x1  = wcstod(pData,   &pData);

	Rect.m_y1  = wcstod(pData+1, &pData);

	Rect.m_x2  = wcstod(pData+1, &pData);

	Rect.m_y2  = wcstod(pData+1, &pData);

	m_uLine     = lineEmpty;

	ReadLine();
	}

UINT CTreeFile::GetValue(CByteArray &Data)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	UINT uCount = wcstoul(PCTXT(m_Data) + 2, NULL, 16);

	if( uCount ) {

		Data.SetCount(uCount);

		Data.Compress();

		PBYTE pData = PBYTE(Data.GetPointer());

		GetValue(pData, uCount);
		}
	else {
		Data.Empty();

		GetValue(NULL, 0);
		}

	return uCount;
	}

UINT CTreeFile::GetValue(CWordArray &Data)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	UINT uCount = wcstoul(PCTXT(m_Data) + 2, NULL, 16);

	if( uCount ) {

		Data.SetCount((uCount + 1) / 2);

		Data.Compress();

		PBYTE pData = PBYTE(Data.GetPointer());

		GetValue(pData, uCount);
		}
	else {
		Data.Empty();

		GetValue(NULL, 0);
		}

	return uCount;
	}

UINT CTreeFile::GetValue(CLongArray &Data)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	UINT uCount = wcstoul(PCTXT(m_Data) + 2, NULL, 16);

	if( uCount ) {

		Data.SetCount((uCount + 3) / 4);

		Data.Compress();

		PBYTE pData = PBYTE(Data.GetPointer());

		GetValue(pData, uCount);
		}
	else {
		Data.Empty();

		GetValue(NULL, 0);
		}

	return uCount;
	}

UINT CTreeFile::GetValue(PBYTE pData, UINT uLimit)
{
	AfxAssert(m_uMode == modeRead);

	ZdiCheckLine(lineValue);

	UINT uCount = 0;

	for(;;) {

		m_uLine = lineEmpty;

		ReadLine();

		if( m_Name == L"." ) {

			PCTXT d = m_Data;

			UINT  p = 2;

			while( d[p] ) {

				if( uCount < uLimit ) {

					BYTE hi = FromHex(d[p+0]);

					BYTE lo = FromHex(d[p+1]);

					*pData++ = BYTE(hi*16 + lo);

					uCount++;
					}

				p += 2;
				}

			continue;
			}

		break;
		}

	return uCount;
	}

void CTreeFile::GetCollect(void)
{
	AfxAssert(m_uMode == modeRead);

	m_uCollect++;

	m_uLine = lineEmpty;

	ReadLine();
	}

void CTreeFile::GetElement(void)
{
	GetName();

	GetObject();
	}

void CTreeFile::GetObject(void)
{
	AfxAssert(m_uMode == modeRead);
	
	m_uObject++;

	m_uLine = lineEmpty;

	ReadLine();
	}

// Shared

void CTreeFile::EndObject(void)
{
	AfxAssert(m_uMode);

	AfxAssert(m_uObject);

	if( m_uMode == modeWrite ) {

		PutIndent();

		PutText(L"}\r\n");

		m_uIndent--;
		}
	else
		ReadLine();

	m_uObject--;
	}

void CTreeFile::EndCollect(void)
{
	AfxAssert(m_uMode);

	AfxAssert(m_uCollect);

	if( m_uMode == modeWrite ) {

		PutIndent();

		PutText(L"]\r\n");

		m_uIndent--;
		}
	else
		ReadLine();

	m_uCollect--;
	}

// Hex Conversion

BYTE CTreeFile::FromHex(TCHAR cData)
{
	if( cData >= '0' && cData <= '9' ) {
		
		return BYTE(cData - '0' + 0x00);
		}

	if( cData >= 'A' && cData <= 'F' ) {
		
		return BYTE(cData - 'A' + 0x0A);
		}
	
	if( cData >= 'a' && cData <= 'f' ) {
		
		return BYTE(cData - 'a' + 0x0A);
		}

	return 0;
	}

TCHAR CTreeFile::MakeHex(UINT uData)
{
	static PCTXT const pHex = L"0123456789ABCDEF";

	return pHex[uData];
	}

// Real Conversion

WCHAR * CTreeFile::RealToWide(number r, WCHAR *p)
{
	int    dec, sgn;

	char * s = _fcvt(r, 10, &dec, &sgn); // _fcvt sucks!

	int    i = 0;

	int    j = 0;

	AfxAssume(s);

	if( sgn ) {

		p[j++] = '-';
		}

	if( dec <= 0 ) {

		p[j++] = '0';

		p[j++] = '.';

		while( dec++ ) {

			p[j++] = '0';
			}

		while( s[i] ) {

			p[j++] = s[i++];
			}
		}
	else {
		while( s[i] ) {

			if( i == dec ) {

				p[j++] = '.';
				}

			p[j++] = s[i++];
			}
		}

	while( p[j-1] == '0' ) {

		j--;
		}

	while( p[j-1] == '.' ) {

		j--;
		}

	p[j++] = 0;

	return p;
	}

// Implementation

void CTreeFile::CloseFile(void)
{
	m_pStream = NULL;

	m_uMode = modeIdle;
	}

void CTreeFile::ClearState(void)
{
	m_uIndent  = 0;

	m_uObject  = 0;

	m_uCollect = 0;
	}

void CTreeFile::PutIndent(void)
{
	UINT a = m_uIndent;

	PTXT p = PTXT(alloca(2 * a + 2));

	UINT i;

	for( i = 0; i < a; i++ ) {

		p[i] = 0x20;
		}

	p[i] = 0;

	PutText(p);
	}

void CTreeFile::PutName(PCTXT pName)
{
	PutIndent();

	UINT n = wstrlen(pName);

	UINT s = max(n, m_uPad);

	UINT a = s + 3;

	PTXT p = PTXT(alloca(2 * a + 2));

	wmemcpy(p, pName, n);

	while( n < s ) {

		p[n++] = 0x20;
		}

	wstrcpy(p + n, L" = ");

	PutText(p);
	}

void CTreeFile::PutText(PCTXT pText)
{
	m_pStream->PutLine(pText);
	}

void CTreeFile::ReadLine(void)
{
	if( m_pStream->GetLine(m_sLine, elements(m_sLine)) ) {

		UINT  uState  = 0;

		int   nPos[6] = { 0, 0, 0, 0, 0, 0 };

		TCHAR cData   = 0;

		int n;

		for( n = 0; cData = m_sLine[n]; n++ ) {

			switch( uState ) {

				case 0:
				case 2:
				case 4:
					if( !isspace(cData) ) {

						nPos[uState++] = n;
						}
					break;

				case 1:
				case 3:
					if( isspace(cData) ) {

						nPos[uState++] = n;
						}
					break;
				}
			}

		if( nPos[1] ) {

			if( nPos[2] ) {

				if( nPos[4] ) {

					while( isspace(m_sLine[--n]) );

					nPos[5] = n + 1;
					}

				m_Name.QuickInit(m_sLine + nPos[0], nPos[1] - nPos[0]);
			
				m_Sep .QuickInit(m_sLine + nPos[2], nPos[3] - nPos[2]);

				m_Data.QuickInit(m_sLine + nPos[4], nPos[5] - nPos[4]);
				}
			else {
				m_Name.QuickInit(L"", 0);

				m_Sep .QuickInit(m_sLine + nPos[0], nPos[1] - nPos[0]);

				m_Data.QuickInit(L"", 0);
				}
			}
		else {
			throw (int *) 0;
			}
		}
	else {
		m_Name.QuickInit(L"", 0);

		m_Sep .QuickInit(L"", 0);
		
		m_Data.QuickInit(L"", 0);
		}
	}

void CTreeFile::ParseLine(void)
{
	if( m_Sep == L"=" ) {
		
		m_uLine = lineValue;

		return;
		}

	if( m_Sep == L"{" ) {
		
		m_uLine = lineObject;

		return;
		}
	
	if( m_Sep == L"[" ) {
		
		m_uLine = lineCollect;

		return;
		}

	m_uLine = lineEmpty;
	}

void CTreeFile::SkipValue(void)
{
	if( m_Data.Left(2) == L"0x" || m_Data.Left(1) == L"\"" ) {

		do {
			SkipLine();

			ReadLine();

			} while( m_Name == L"." );

		SkipDone();

		return;
		}

	SkipLine();

	ReadLine();

	SkipDone();
	}

void CTreeFile::SkipObject(void)
{
	UINT uObject = 1;

	do {
		SkipLine();

		ReadLine();

		if( m_Sep == L"{" ) uObject++;

		if( m_Sep == L"}" ) uObject--;

		} while( uObject );

	SkipLine();

	ReadLine();

	SkipDone();
	}

void CTreeFile::SkipCollect(void)
{
	UINT uCollect = 1;

	do {
		SkipLine();

		ReadLine();

		if( m_Sep == L"[" ) uCollect++;

		if( m_Sep == L"]" ) uCollect--;

		} while( uCollect );

	SkipLine();

	ReadLine();

	SkipDone();
	}

void CTreeFile::SkipLine(void)
{
	if( m_pDivert ) {

		m_pDivert->PutLine(m_sLine);
		}
	}

void CTreeFile::SkipDone(void)
{
	if( m_pDivert ) {

		m_pDivert = NULL;
		}
	}

BOOL CTreeFile::SilentSkip(void)
{
	if( m_Name == L"*"      ) return TRUE;
	
	if( m_Name == L"Handle" ) return TRUE;
	
	if( m_Name == L"Name"   ) return TRUE;

	return FALSE;
	}

void CTreeFile::ZdiCheckLine(UINT uLine)
{
	if( m_uLine != uLine ) {

		throw (int *) 0;
		}
	}

// End of File
