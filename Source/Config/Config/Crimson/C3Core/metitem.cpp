
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Meta Item
//

// Dynamic Class

AfxImplementDynamicClass(CMetaItem, CItem);

// Static Data

UINT  CMetaItem::m_uAppData = AfxAllocAppData(L"MetaData");

PCTXT CMetaItem::m_pHandle  = L"Handle";

// Constructor

CMetaItem::CMetaItem(void)
{
	m_pList   = NULL;

	m_pDivert = NULL;

	m_Fixed   = 0;
}

// Destructor

CMetaItem::~CMetaItem(void)
{
	CleanUp();

	delete m_pDivert;
}

// Meta Data Access

CMetaList * CMetaItem::FindMetaList(void) const
{
	return m_pList;
}

CMetaData const * CMetaItem::FindMetaData(PCTXT pName) const
{
	return m_pList->FindData(pName);
}

CItemIndexList * CMetaItem::FindIndexList(PCTXT pName) const
{
	CMetaData const *pMeta = FindMetaData(pName);

	if( pMeta ) {

		CItem *pItem = pMeta->GetObject(PVOID(this));

		if( pItem->IsKindOf(AfxRuntimeClass(CItemIndexList)) ) {

			CItemIndexList *pList = (CItemIndexList *) pItem;

			return pList;
		}
	}

	AfxAssert(FALSE);

	return NULL;
}

// Name Padding

UINT CMetaItem::GetPadding(void) const
{
	return m_pList->GetPadding();
}

// Item Naming

CString CMetaItem::GetHumanName(void) const
{
	CMetaData const * pMeta = FindMetaData(L"Name");

	if( pMeta ) {

		return pMeta->ReadString(this);
	}

	return m_pList->GetName();
}

CString CMetaItem::GetFixedName(void) const
{
	return CPrintf(L"%8.8X", m_Fixed);
}

void CMetaItem::SetFixedName(CString Name)
{
	m_Fixed = wcstoul(Name, NULL, 16);
}

// Item Privacy

BOOL CMetaItem::HasPrivate(void)
{
	return GetDataAccess(L"Private") != NULL;
}

UINT CMetaItem::GetPrivate(void)
{
	CString Name = GetName();

	UINT    uPos = Name.FindRev(L'.');

	if( uPos != NOTHING ) {

		CNamedList *pList = (CNamedList *) HasParent(AfxRuntimeClass(CNamedList));

		if( pList ) {

			CString Root = Name.Left(uPos);

			CItem *pRoot = pList->FindByName(Root);

			if( pRoot ) {

				if( pRoot->GetPrivate() ) {

					return 1;
				}
			}
		}
	}

	IDataAccess *pData = GetDataAccess(L"Private");

	if( pData ) {

		UINT uPrivate = pData->ReadInteger(this);

		return uPrivate;
	}

	return 0;
}

// Named Item Support

BOOL CMetaItem::HasName(void)
{
	AddMeta();

	if( FindMetaList() ) {

		return FindMetaData(L"Name") != NULL;
	}

	return FALSE;
}

CString CMetaItem::GetName(void)
{
	AddMeta();

	if( FindMetaList() ) {

		CMetaData const *pMeta = FindMetaData(L"Name");

		if( pMeta ) {

			return pMeta->ReadString(this);
		}
	}

	return L"";
}

BOOL CMetaItem::SetName(CString Name)
{
	AddMeta();

	if( FindMetaList() ) {

		CMetaData const *pMeta = FindMetaData(L"Name");

		if( pMeta ) {

			pMeta->WriteString(this, Name);

			CItem *pParent = GetParent();

			if( pParent->IsKindOf(AfxRuntimeClass(CNamedList)) ) {

				CNamedList *pList = (CNamedList *) pParent;

				pList->ItemRenamed(this);
			}

			return TRUE;
		}
	}

	return FALSE;
}

// Persistance

void CMetaItem::Init(void)
{
	AddMeta();

	m_Fixed = m_pDbase->AllocFixedInt();

	CItem *pItem = NULL;

	UINT  uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pItem = pMeta->GetObject(this);

				pItem->SetParent(this);

				pItem->Init();

				break;

			case metaVirtual:

				InitVirtual(pMeta);

				break;
		}
	}

	InitHandle();
}

void CMetaItem::Kill(void)
{
	AfxAssert(m_pList);

	UINT uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pMeta->GetObject(this)->Kill();

				break;

			case metaVirtual:

				KillVirtual(pMeta);

				break;
		}
	}

	FreeHandle();
}

void CMetaItem::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
		}

/*		if( !SkipProp(Name) ) {

			if( !m_pDivert ) {

				m_pDivert = New CTextStreamMemory;

				m_pDivert->OpenSave();
				}

			Tree.SetDivert(m_pDivert);
			}
*/
	}

	RegisterHandle();
}

void CMetaItem::PreSnapshot(void)
{
	AfxAssert(m_pList);

	CItem *pItem = NULL;

	UINT  uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pItem = pMeta->GetObject(this);

				pItem->PreSnapshot();

				break;

			case metaVirtual:

				SnapVirtual(pMeta);

				break;
		}
	}
}

void CMetaItem::PreCopy(void)
{
	AfxAssert(m_pList);

	CItem *pItem = NULL;

	UINT  uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pItem = pMeta->GetObject(this);

				pItem->PreCopy();

				break;

			case metaVirtual:

				CopyVirtual(pMeta);

				break;
		}
	}
}

void CMetaItem::PostPaste(void)
{
	AfxAssert(m_pList);

	CItem *pItem = NULL;

	UINT  uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pItem = pMeta->GetObject(this);

				if( pItem->GetParent() ) {

					pItem->PostPaste();
				}

				break;

			case metaVirtual:

				PasteVirtual(pMeta);

				break;
		}
	}

	m_Fixed = m_pDbase->AllocFixedInt();
}

void CMetaItem::PostLoad(void)
{
	AfxAssert(m_pList);

	if( m_pParent ) {

		m_pDbase->CheckFixed(m_Fixed);
	}

	CItem *pItem = NULL;

	UINT  uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pItem = pMeta->GetObject(this);

				if( pItem->GetParent() ) {

					pItem->PostLoad();
				}
				else {
					pItem->SetParent(this);

					pItem->Init();
				}

				break;

			case metaVirtual:

				PostLoadVirtual(pMeta);

				break;
		}
	}

	CheckHandle();
}

void CMetaItem::PostSave(void)
{
	AfxAssert(m_pList);

	CItem *pItem = NULL;

	UINT  uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pItem = pMeta->GetObject(this);

				if( pItem->GetParent() ) {

					pItem->PostSave();
				}

				break;

			case metaVirtual:

				PostSaveVirtual(pMeta);

				break;
		}
	}

	CheckHandle();
}

void CMetaItem::Save(CTreeFile &Tree)
{
	AfxAssert(m_pList);

	Tree.SetPadding(m_pList->GetPadding());

	UINT uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		if( !SaveProp(pMeta->GetTag()) ) {

			continue;
		}

		SaveProp(Tree, pMeta);
	}

	if( m_pDivert ) {

		m_pDivert->Rewind();

		Tree.PutDivert(m_pDivert);
	}
}

// Property Upgrade

void CMetaItem::Upgrade(CTreeFile &Tree, CMetaData const *pMeta)
{
}

// Data Access

IDataAccess * CMetaItem::GetDataAccess(PCTXT pTag)
{
	AddMeta();

	return (IDataAccess *) m_pList->FindData(pTag);
}

// Download Support

void CMetaItem::PrepareData(void)
{
	AfxAssert(m_pList);

	CItem *pItem = NULL;

	UINT  uCount = m_pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = m_pList->FindData(n);

		switch( pMeta->GetType() ) {

			case metaObject:
			case metaCollect:

				pItem = pMeta->GetObject(this);

				pItem->PrepareData();

				break;

			case metaVirtual:

				PrepVirtual(pMeta);

				break;
		}
	}
}

BOOL CMetaItem::MakeInitData(CInitData &Init)
{
	CItem::MakeInitData(Init);

	return TRUE;
}

// Virtual Helpers

BOOL CMetaItem::InitVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->SetParent(this);

		pItem->Init();

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::KillVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->Kill();

		delete pItem;

		pMeta->GetObject(this) = NULL;

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::LoadVirtual(CMetaData const *pMeta, CTreeFile &Tree)
{
	Tree.GetName();

	CString Class = Tree.GetValueAsString();

	if( Class == L"NULL" ) {

		pMeta->GetObject(this) = NULL;

		return FALSE;
	}
	else {
		CLASS Named = m_pDbase->AliasClass(Class);

		if( Named ) {

			CItem *pItem = AfxNewObject(CItem, Named);

			if( pItem ) {

				pItem->SetParent(this);

				pItem->Load(Tree);

				pMeta->GetObject(this) = pItem;

				return TRUE;
			}
		}
	}

	while( !Tree.IsEndOfData() ) {

		Tree.GetName();
	}

	pMeta->GetObject(this) = NULL;

	return FALSE;
}

BOOL CMetaItem::PostLoadVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->PostLoad();

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::PostSaveVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->PostSave();

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::SnapVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->PreSnapshot();

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::CopyVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->PreCopy();

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::PasteVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->PostPaste();

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::SaveVirtual(CMetaData const *pMeta, CTreeFile &Tree)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		Tree.PutObject(pMeta->GetTag());

		Tree.SetPadding(pItem->GetPadding());

		Tree.PutValue(L"*", pItem->GetClassName() + 1);

		pItem->Save(Tree);

		Tree.EndObject();

		return TRUE;
	}

	return FALSE;
}

BOOL CMetaItem::PrepVirtual(CMetaData const *pMeta)
{
	CItem *pItem = pMeta->GetObject(this);

	if( pItem ) {

		pItem->PrepareData();

		return TRUE;
	}

	return FALSE;
}

// Property Filters

BOOL CMetaItem::SaveProp(CString const &Tag) const
{
	if( Tag[0] == '/' ) {

		return FALSE;
	}

	return TRUE;
}

BOOL CMetaItem::SkipProp(CString const &Tag) const
{
	return FALSE;
}

// Meta Data Creation

void CMetaItem::AddMetaData(void)
{
	Meta_Add(L"Fixed", m_Fixed, metaInteger);
}

// Implementation

BOOL CMetaItem::AddMeta(void)
{
	if( !m_pList ) {

		m_pList = (CMetaList *) AfxThisClass()->GetAppData(m_uAppData);

		if( !m_pList ) {

			m_pList = New CMetaList;

			AfxThisClass()->SetAppData(m_uAppData, m_pList);

			AddMetaData();

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

UINT CMetaItem::AddMeta(PCTXT pTag, UINT Type, UINT Pos)
{
	return m_pList->Add(pTag, Type, Pos);
}

void CMetaItem::LoadProp(CTreeFile &Tree, CMetaData const *pMeta)
{
	CItem *pItem = NULL;

	//AfxTrace(L"load [%s], type %d\n", pMeta->GetTag(), pMeta->GetType());

	switch( pMeta->GetType() ) {

		case metaString:

			if( Tree.IsSimpleValue() ) {

				pMeta->WriteString(this,
						   Tree.GetValueAsString()
				);

				return;
			}

			Upgrade(Tree, pMeta);

			break;

		case metaInteger:

			if( Tree.IsSimpleValue() ) {

				pMeta->WriteInteger(this,
						    Tree.GetValueAsInteger()
				);

				return;
			}

			Upgrade(Tree, pMeta);

			break;

		case metaNumber:

			if( Tree.IsSimpleValue() ) {

				pMeta->WriteNumber(this,
						   Tree.GetValueAsNumber()
				);

				return;
			}

			Upgrade(Tree, pMeta);

			break;

		case metaGUID:

			pMeta->WriteGuid(this,
					 Tree.GetValueAsGuid()
			);
			break;

		case metaPoint:

			Tree.GetValue(pMeta->GetPoint(this));

			break;

		case metaRect:

			Tree.GetValue(pMeta->GetRect(this));

			break;

		case metaR2R:

			Tree.GetValue(pMeta->GetR2R(this));

			break;

		case metaBlob:

			Tree.GetValue(pMeta->GetBlob(this));

			break;

		case metaWide:

			Tree.GetValue(pMeta->GetWide(this));

			break;

		case metaHuge:

			Tree.GetValue(pMeta->GetHuge(this));

			break;

		case metaObject:

			pItem = pMeta->GetObject(this);

			if( Tree.IsSimpleValue() ) {

				pItem->SetParent(this);

				pItem->Init();

				Upgrade(Tree, pMeta);
			}
			else {
				Tree.GetObject();

				pItem->SetParent(this);

				pItem->Load(Tree);

				Tree.EndObject();
			}

			break;

		case metaCollect:

			pItem = pMeta->GetObject(this);

			if( Tree.IsSimpleValue() ) {

				pItem->SetParent(this);

				pItem->Init();

				Upgrade(Tree, pMeta);
			}
			else {
				Tree.GetCollect();

				pItem->SetParent(this);

				pItem->Load(Tree);

				Tree.EndCollect();
			}

			break;

		case metaVirtual:

			if( Tree.IsSimpleValue() ) {

				Upgrade(Tree, pMeta);
			}
			else {
				Tree.GetObject();

				LoadVirtual(pMeta, Tree);

				Tree.EndObject();
			}

			break;
	}
}

void CMetaItem::SaveProp(CTreeFile &Tree, CMetaData const *pMeta)
{
	UINT uPad;

	switch( pMeta->GetType() ) {

		case metaString:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->ReadString(this)
			);
			break;

		case metaInteger:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->ReadInteger(this)
			);
			break;

		case metaNumber:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->ReadNumber(this)
			);
			break;

		case metaGUID:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->ReadGuid(this)
			);
			break;

		case metaPoint:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->GetPoint(this)
			);
			break;

		case metaRect:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->GetRect(this)
			);
			break;

		case metaR2R:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->GetR2R(this)
			);
			break;

		case metaBlob:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->GetBlob(this)
			);
			break;

		case metaWide:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->GetWide(this)
			);
			break;

		case metaHuge:

			Tree.PutValue(pMeta->GetTag(),
				      pMeta->GetHuge(this)
			);
			break;

		case metaObject:

			uPad = Tree.GetPadding();

			Tree.PutObject(pMeta->GetTag());

			pMeta->GetObject(this)->Save(Tree);

			Tree.EndObject();

			Tree.SetPadding(uPad);

			break;

		case metaCollect:

			uPad = Tree.GetPadding();

			Tree.PutCollect(pMeta->GetTag());

			pMeta->GetObject(this)->Save(Tree);

			Tree.EndCollect();

			Tree.SetPadding(uPad);

			break;

		case metaVirtual:

			uPad = Tree.GetPadding();

			SaveVirtual(pMeta, Tree);

			Tree.SetPadding(uPad);

			break;
	}
}

void CMetaItem::InitHandle(void)
{
	IDataAccess *pData = GetDataAccess(m_pHandle);

	if( pData ) {

		if( pData->ReadInteger(this) == HANDLE_NONE ) {

			UINT uItem = m_pDbase->AllocHandle(this);

			pData->WriteInteger(this, uItem);

			return;
		}

		RegisterHandle();
	}
}

void CMetaItem::RegisterHandle(void)
{
	IDataAccess *pData = GetDataAccess(m_pHandle);

	if( pData ) {

		UINT uItem = pData->ReadInteger(this);

		if( uItem != HANDLE_NONE && uItem != HANDLE_BROKE ) {

			if( !m_pDbase->RegisterHandle(uItem, this) ) {

				UINT uItem = m_pDbase->AllocHandle(this);

				pData->WriteInteger(this, uItem);
			}
		}
	}
}

void CMetaItem::CheckHandle(void)
{
	IDataAccess *pData = GetDataAccess(m_pHandle);

	if( pData ) {

		UINT uItem = pData->ReadInteger(this);

		if( uItem == HANDLE_NONE ) {

			UINT uItem = m_pDbase->AllocHandle(this);

			pData->WriteInteger(this, uItem);
		}
	}
}

void CMetaItem::FreeHandle(void)
{
	IDataAccess *pData = GetDataAccess(m_pHandle);

	if( pData ) {

		UINT uItem = pData->ReadInteger(this);

		m_pDbase->RegisterHandle(uItem, NULL);
	}
}

void CMetaItem::CleanUp(void)
{
	if( m_pList ) {

		UINT uCount = m_pList->GetCount();

		for( UINT n = 0; n < uCount; n++ ) {

			CMetaData const *pMeta = m_pList->FindData(n);

			switch( pMeta->GetType() ) {

				case metaObject:
				case metaVirtual:
				case metaCollect:

					delete pMeta->GetObject(this);

					break;
			}
		}

		m_pList = NULL;
	}
}

// Import Helpers

BOOL CMetaItem::ImportString(CPropValue *pRoot, CString Tag, CString Prop)
{
	if( pRoot ) {

		CPropValue *pValue = pRoot->GetChild(Prop);

		if( pValue ) {

			CMetaList       *pList = FindMetaList();

			CMetaData const *pData = pList->FindData(Tag);

			UINT             uType = pData->GetType();

			if( uType == metaString ) {

				CString Text = pValue->GetString();

				pData->WriteString(this, Text);

				return TRUE;
			}

			AfxAssert(FALSE);
		}
	}

	return FALSE;
}

BOOL CMetaItem::ImportString(CPropValue *pRoot, CString Tag)
{
	return ImportString(pRoot, Tag, Tag);
}

BOOL CMetaItem::ImportNumber(CPropValue *pRoot, CString Tag, CString Prop)
{
	if( pRoot ) {

		CPropValue *pValue = pRoot->GetChild(Prop);

		if( pValue ) {

			CMetaList       *pList = FindMetaList();

			CMetaData const *pData = pList->FindData(Tag);

			UINT             uType = pData->GetType();

			if( uType == metaInteger ) {

				UINT uData = pValue->GetNumber();

				pData->WriteInteger(this, uData);

				return TRUE;
			}

			AfxAssert(FALSE);
		}
	}

	return FALSE;
}

BOOL CMetaItem::ImportNumber(CPropValue *pRoot, CString Tag)
{
	return ImportNumber(pRoot, Tag, Tag);
}

// End of File
