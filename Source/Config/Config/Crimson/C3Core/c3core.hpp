
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3CORE_HPP
	
#define	INCLUDE_C3CORE_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdesk.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3core.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_C3CORE

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "c3core.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Numeric Type
//

#ifndef DEFINED_number

#define DEFINED_number

typedef double number;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Real Rectangle
//

#ifndef DEFINED_R2R

#define DEFINED_R2R

struct R2R
{
	union {
		number m_x1;
		number m_left;
		};
	union {
		number m_y1;
		number m_top;
		};
	union {
		number m_x2;
		number m_right;
		};
	union {
		number m_y2;
		number m_bottom;
		};
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Time-Date Framework
//

DLLAPI DWORD Time(UINT h, UINT m, UINT s);
DLLAPI DWORD Date(UINT y, UINT m, UINT d);
DLLAPI void  GetWholeDate(DWORD t, UINT *p);
DLLAPI void  GetWholeTime(DWORD t, UINT *p);
DLLAPI UINT  GetYear(DWORD t);
DLLAPI UINT  GetMonth(DWORD t);
DLLAPI UINT  GetDate(DWORD t);
DLLAPI UINT  GetDays(DWORD t);
DLLAPI UINT  GetWeeks(DWORD t);
DLLAPI UINT  GetDay(DWORD t);
DLLAPI UINT  GetWeek(DWORD t);
DLLAPI UINT  GetWeekYear(DWORD t);
DLLAPI UINT  GetHours(DWORD t);
DLLAPI UINT  GetHour(DWORD t);
DLLAPI UINT  GetMin(DWORD t);
DLLAPI UINT  GetSec(DWORD t);
DLLAPI UINT  GetMonthDays(UINT y, UINT m);

//////////////////////////////////////////////////////////////////////////
//
// OEM Conversion APIs
//

DLLAPI void    C3OemInit(void);
DLLAPI void    C3OemTerm(void);
DLLAPI BOOL    C3OemCheckSigs(void);
DLLAPI CString C3OemCompany(void);
DLLAPI CString C3OemModels(void);
DLLAPI CString C3OemGetManual(UINT uFile);
DLLAPI void    C3OemStrings(PTXT pBuffer, UINT &uSize);
DLLAPI void    C3OemStrings(CString &String);
DLLAPI void    C3OemAdjustMenu(CMenu &Menu);
DLLAPI BOOL    C3OemFeature(PCTXT pName, BOOL fDefault);
DLLAPI CString C3OemOption(PCTXT pName, PCTXT pDefault);
DLLAPI BOOL    C3OemAllowDriver(UINT uIdent, BOOL fDefault);
DLLAPI CColor  C3OemGetColor(PCTXT pName, CColor Default);
DLLAPI WORD    C3OemGetUsbBase(void);

//////////////////////////////////////////////////////////////////////////
//
// API for Name Validation
//

DLLAPI BOOL C3ValidateName(CError &Error, CString Name);

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CArray <BYTE> CByteArray;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

interface ITextStream;

//////////////////////////////////////////////////////////////////////////
//
// Text Stream Interface
//

interface ITextStream
{
	// Operations
	virtual BOOL GetLine(PTXT  pText, UINT uSize) = 0;
	virtual BOOL PutLine(PCTXT pText)	      = 0;
	virtual void Rewind (void)                    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTextStreamMemory;
class CTreeFile;

//////////////////////////////////////////////////////////////////////////
//
// Stream Save Modes
//

enum
{
	saveCompress = 0,
	saveSecure   = 1,
	saveRaw      = 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Memory Text Stream
//

class DLLAPI CTextStreamMemory : public ITextStream
{
	public:
		// Constructor
		CTextStreamMemory(void);

		// Destructor
		~CTextStreamMemory(void);

		// Management
		BOOL    OpenLoad(HGLOBAL hBlock);
		BOOL    OpenLoad(HGLOBAL hBlock, BOOL fFree);
		BOOL    OpenSave(void);
		HGLOBAL TakeOver(void);
		void    Close(void);

		// File Transfer
		BOOL LoadFromFile(CFilename const &Name);
		BOOL SaveToFile(CFilename const &Name, UINT uMode);
		BOOL SaveToFile(CFilename const &Name);

		// Attributes
		BOOL IsWide(void) const;
		UINT GetSize(void) const;
		UINT GetRead(void) const;
		BOOL SaveSuccessful(void) const;

		// Operations
		void SetWide(void);
		void SetPrev(void);
		void SetSpecial(void);
		BOOL GetLine(PTXT  pText, UINT uSize);
		BOOL PutLine(PCTXT pText);
		void Rewind(void);

	protected:
		// Data Members
		HGLOBAL m_hBlock;
		PBYTE   m_pData;
		BOOL    m_fFree;
		BOOL    m_fSave;
		BOOL	m_fANSI;
		BOOL	m_fPrev;
		UINT    m_uSize;
		UINT    m_uPos;
		BOOL    m_fSpec;
		BOOL	m_fFail;

		// Implementation
		BOOL IsANSI(void);
		BYTE FromHex(TCHAR cData);
		BOOL Crypto(PBYTE pData, UINT uSize, UINT uCode);
		BOOL Crypto(PBYTE pData, UINT uSize, PCSTR pPass);
		BOOL WriteSig(HANDLE hFile, UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Tree File Object
//

class DLLAPI CTreeFile
{
	public:
		// Constructor
		CTreeFile(void);

		// Destructor
		~CTreeFile(void);

		// Attributes
		BOOL GetSkipFlag(void) const;
		UINT GetPadding(void) const;
		BOOL HasFile(void) const;
		CFilename GetFile(void) const;

		// Management
		BOOL OpenLoad(ITextStream &Stream);
		BOOL OpenSave(ITextStream &Stream);
		void SetFile(CFilename const &File);
		void Abort(void);
		void Close(void);

		// Control
		void SetDivert(ITextStream *pDivert);
		void SetPadding(UINT uPad);
		void SetCompressed(void);

		// Writing
		BOOL PutDivert(ITextStream *pDivert);
		void PutValue(PCTXT pName, PCTXT pValue);
		void PutValue(PCTXT pName, UINT uValue);
		void PutValue(PCTXT pName, BOOL fValue);
		void PutValue(PCTXT pName, DWORD dwValue);
		void PutValue(PCTXT pName, number value);
		void PutValue(PCTXT pName, CGuid const &Guid);
		void PutValue(PCTXT pName, CPoint const &Pos);
		void PutValue(PCTXT pName, CRect const &Rect);
		void PutValue(PCTXT pName, R2R const &Rect);
		void PutValue(PCTXT pName, CByteArray const &Data);
		void PutValue(PCTXT pName, CWordArray const &Data);
		void PutValue(PCTXT pName, CLongArray const &Data);
		void PutValue(PCTXT pName, PCBYTE pData, UINT uCount);
		void PutCollect(PCTXT pName);
		void PutElement(void);
		void PutObject(PCTXT pName);

		// Name Reading
		CString const & GetName(void);

		// Data Reading
		BOOL	IsEndOfData(void);
		BOOL    IsName(CString const &Name);
		BOOL    IsSimpleValue(void);
		CString GetValueAsString(void);
		UINT    GetValueAsInteger(void);
		number  GetValueAsNumber(void);
		CGuid   GetValueAsGuid(void);
		void    GetValue(CPoint &Pos);
		void    GetValue(CRect &Rect);
		void    GetValue(R2R &Rect);
		UINT    GetValue(CByteArray &Data);
		UINT    GetValue(CWordArray &Data);
		UINT    GetValue(CLongArray &Data);
		UINT    GetValue(PBYTE pData, UINT uLimit);
		void    GetCollect(void);
		void	GetElement(void);
		void	GetObject(void);

		// Shared
		void EndObject(void);
		void EndCollect(void);

	protected:
		// Data Members
		ITextStream   * m_pStream;
		ITextStream   * m_pDivert;
		TCHAR		m_sLine[512];
		UINT		m_uMode;
		UINT		m_uIndent;
		UINT		m_uObject;
		UINT		m_uCollect;
		CString		m_Name;
		CString		m_Sep;
		CString		m_Data;
		UINT		m_uLine;
		BOOL		m_fSkip;
		UINT		m_uPad;
		CFilename       m_File;

		// Hex Conversion
		BYTE  FromHex(TCHAR cData);
		TCHAR MakeHex(UINT uData);

		// Real Conversion
		WCHAR * RealToWide(number r, WCHAR *p);

		// Implementation
		void CloseFile(void);
		void ClearState(void);
		void PutIndent(void);
		void PutName(PCTXT pName);
		void PutText(PCTXT pText);
		void ReadLine(void);
		void GetLine(void);
		void ParseLine(void);
		void SkipValue(void);
		void SkipObject(void);
		void SkipCollect(void);
		void SkipLine(void);
		void SkipDone(void);
		BOOL SilentSkip(void);
		void ZdiCheckLine(UINT uLine);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPropValue;
class CPropLoader;

//////////////////////////////////////////////////////////////////////////
//
// Property Map
//

typedef CMap <CString, CPropValue *> CPropMap;

//////////////////////////////////////////////////////////////////////////
//
// Property Value
//

class DLLAPI CPropValue
{
	public:
		// Constructor
		CPropValue(void);

		// Destructor
		~CPropValue(void);

		// Child Access
		CPropValue * GetChild(CString Name) const;

		// Attributes
		BOOL	IsList(void) const;
		BOOL    IsComplex(void) const;
		BOOL    IsString(void) const;
		BOOL    IsNumber(void) const;
		BOOL    IsBlob(void) const;
		CString GetString(void) const;
		UINT    GetNumber(void) const;
		UINT    GetBlob(CByteArray &Blob) const;

		// Operations
		BOOL Rename(CString From, CString Name);
		void Print(CString Name, UINT uDepth);

		// Data Members
		CPropValue *  m_pLink;
		BOOL	      m_fList;
		BOOL	      m_fUsed;
		CString       m_Value;
		CPropMap      m_Props;

	protected:
		// Static Data
		static UINT m_uDepth;

		// Implementation
		BYTE FromHex(TCHAR cData) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Property Loader
//

class DLLAPI CPropLoader
{
	public:
		// Constructor
		CPropLoader(void);

		// Destructor
		~CPropLoader(void);

		// Operations
		BOOL LoadFromFile(CFilename const &Name);

		// Data Members
		CPropValue m_Root;

	protected:
		// Static Data
		static WCHAR m_sLine[8192];

		// Data Members
		CString m_Name;
		CString m_Sep;
		CString m_Data;
		CString m_Last;

		// Implementation
		BOOL ReadLine(ITextStream *pStm);
		BOOL IsValue(void);
		BOOL IsOpen(void);
		BOOL IsClose(void);
		BOOL IsList(void);
		BOOL ForceList(CString const &Path, CString const &Name);
		BOOL ForceItem(CString const &Path, CString const &Name);
		BOOL ForceEnum(CString const &Path, CString const &Name);
		BOOL ForceSkip(CString const &Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDataObject;

//////////////////////////////////////////////////////////////////////////
//
// Standard Data Object
//

class DLLAPI CDataObject : public CObject, public IDataObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CDataObject(void);

		// Destructor
		~CDataObject(void);

		// Attributes
		BOOL IsEmpty(void) const;

		// Operations
		void AddStream(UINT cfData, CTextStreamMemory &Stream);
		void AddText(UINT cfData, PCTXT pText);
		void AddText(UINT cfData, LPCSTR pText);
		void AddText(PCTXT pText);
		void AddText(LPCSTR pText);
		void AddEnhMetaFile(UINT cfData, HENHMETAFILE hMeta);
		void AddEnhMetaFile(HENHMETAFILE hMeta);
		void AddGlobalData(UINT cfData, PCBYTE pBits, UINT uSize);
		void AddGlobalData(UINT cfData, HGLOBAL hData);
		void AddBitmapData(PCBYTE pBits, UINT uSize);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDataObject
		HRESULT METHOD GetData(FORMATETC *pFmt, STGMEDIUM *pMed);
		HRESULT METHOD GetDataHere(FORMATETC *pFmt, STGMEDIUM *pMed);
		HRESULT METHOD QueryGetData(FORMATETC *pFmt);
		HRESULT METHOD GetCanonicalFormatEtc(FORMATETC *pFmtIn, FORMATETC *pFmtOut);
		HRESULT METHOD SetData(FORMATETC *pFmt, STGMEDIUM *pMed, BOOL fRelease);
		HRESULT METHOD EnumFormatEtc(DWORD dwDirection, IEnumFORMATETC **ppEnum);
		HRESULT METHOD DAdvise(FORMATETC *pFmt, DWORD advf, IAdviseSink *pSink, DWORD *pdwConn);
		HRESULT METHOD DUnadvise(DWORD dwConn);
		HRESULT METHOD EnumDAdvise(IEnumSTATDATA **ppEnum);

	protected:
		// Data Entry
		struct CEntry
		{
			FORMATETC m_Fmt;
			STGMEDIUM m_Med;
			};

		// Data List
		typedef CList <CEntry *> CDataList;

		// Data Members
		ULONG       m_uRefs;
		CDataList   m_List;
		BOOL        m_fFmt;
		UINT        m_uFmt;
		FORMATETC * m_pFmt;

		// Implementation
		void     InitData(void);
		CEntry * FindEntry(FORMATETC *pFmt, BOOL fAdd);
		BOOL	 BuildFmtList(void);
		BOOL     AddMediumRef(STGMEDIUM *pRef);
		BOOL     SameObject(IUnknown *punkOne, IUnknown *punkTwo);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

interface IDataAccess;

//////////////////////////////////////////////////////////////////////////
//
// Data Access Interface
//

interface IDataAccess
{
	// Data Read
	virtual PCTXT   ReadString(PCVOID pItem)		const = 0;
	virtual UINT    ReadInteger(PCVOID pItem)		const = 0;
	virtual number  ReadNumber(PCVOID pItem)		const = 0;
	virtual CGuid   ReadGuid(PCVOID pItem)			const = 0;

	// Data Write
	virtual void WriteString(PVOID pItem, PCTXT pText)	const = 0;
	virtual void WriteInteger(PVOID pItem, UINT uValue)	const = 0;
	virtual void WriteNumber(PVOID pItem, number value)	const = 0;
	virtual void WriteGuid(PVOID pItem, CGuid const &Guid)	const = 0;

	// Referencing
	virtual CPoint     & GetPoint(PVOID pItem)		const = 0;
	virtual CRect      & GetRect(PVOID pItem)		const = 0;
	virtual R2R        & GetR2R(PVOID pItem)		const = 0;
	virtual CByteArray & GetBlob(PVOID pItem)		const = 0;
	virtual CWordArray & GetWide(PVOID pItem)		const = 0;
	virtual CItem    * & GetObject(PVOID pItem)		const = 0;

	// Dirty Control
	virtual void SetDirty(PVOID pItem)			const = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CInitData;
class CItem;
class CItemList;
class CItemIndexList;
class CNamedList;
class CDatabase;

//////////////////////////////////////////////////////////////////////////
//
// Item Addition Flags
//

enum
{
	itemSimple,
	itemSized,
	itemVirtual,
	itemHandle,
	itemEncrypt,
	};

//////////////////////////////////////////////////////////////////////////
//
// Item View Types
//

enum
{
	viewDefault,
	viewSystem,
	viewCategory,
	viewNavigation,
	viewResource,
	viewItem,
	viewDetail
	};

//////////////////////////////////////////////////////////////////////////
//
// Initialization Data
//

class DLLAPI CInitData
{
	public:
		// Constructor
		CInitData(void);

		// Array Access
		operator CByteArray & (void);

		// Attributes
		UINT   GetCount(void) const;
		PCBYTE GetPointer(void) const;
		DWORD  GetCRC(void) const;
		BOOL   GetOrder(void) const;
		BOOL   GetAlign(void) const;

		// Operations
		void Empty(void);
		void SetCount(UINT uCount );
		void SetOffset(UINT uOffset);
		BOOL Compress(void);

		// Mode Control
		void SetOrder(BOOL fBig);
		void SetAlign(BOOL fAlign);

		// Item Addition
		void AddItem(UINT uMethod, CItem *pItem);

		// Bulk Addition
		void AddData(PCBYTE pData, UINT uCount);
		void AddData(PCWORD pData, UINT uCount);
		void AddData(CString const &Data, UINT uCount);
		void AddGuid(CGuid const &Guid);
		void AddCryp(PCTXT pText);
		void AddCryp(CInitData const &Data);
		void AddCryp(PCBYTE pData, UINT uCount);

		// Logical Operations
		void OrByte(UINT uOffset, BYTE bData);
		
		// Data Addition
		void AddByte(BYTE  bData);
		void AddWord(WORD  wData);
		void AddLong(DWORD dwData);
		void AddText(PCTXT pData);
		void AddIntl(PCTXT pData, PCTXT pDefault);
		void AddIntl(PCTXT pData);

		// Data Adjustment
		void SetWord(UINT uPos, WORD wData);

		// Data Retrieval
		BYTE  GetByte(UINT uOffset);
		WORD  GetWord(UINT uOffset);
		DWORD GetLong(UINT uOffset);
		BYTE  GetByte(void);
		WORD  GetWord(void);
		DWORD GetLong(void);

	protected:
		// Data Members
		CByteArray m_Data;
		BOOL	   m_fBig;
		BOOL	   m_fAlign;
		UINT	   m_uOffset;

		// Implementation
		BOOL Crypto(PBYTE pData, UINT uSize, UINT uMode);
		BOOL Crypto(PBYTE pData, UINT uSize, PCSTR pPass);
	};

//////////////////////////////////////////////////////////////////////////
//
// Abstract Data Item
//

class DLLAPI CItem : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CItem(void);

		// Destructor
		~CItem(void);

		// Attributes
		CDatabase * GetDatabase(void) const;
		CItem     * GetParent(void) const;
		CItem     * GetParent(UINT uLevel) const;
		CItem	  * GetParent(CLASS Class) const;
		CItem	  * HasParent(CLASS Class) const;
		UINT        GetIndex(void) const;
		CString     GetHumanPath(void) const;
		CString     GetFixedPath(void) const;
		CString     GetFindInfo(void) const;

		// Operations
		void SetDatabase(CDatabase *pDbase);
		void SetParent(CItem *pParent);
		void SetDirty(void);
		void SetIndex(UINT uPos);

		// Snapshot Imaging
		HGLOBAL TakeSnapshot(void);
		HGLOBAL TakeSnapshot(PCTXT pClass);
		BOOL    LoadSnapshot(HGLOBAL hBuffer);

		// Snapshot Imaging
		static CItem * MakeFromSnapshot(CItem *pParent, HGLOBAL hBuffer);
		static CItem * MakeFromItem    (CItem *pParent, CItem * pSource);

		// Item Naming
		virtual BOOL    IsHumanRoot(void) const;
		virtual CString GetHumanName(void) const;
		virtual CString GetItemOrdinal(void) const;
		virtual CString GetFixedName(void) const;
		virtual void    SetFixedName(CString Name);

		// Item Privacy
		virtual BOOL HasPrivate(void);
		virtual UINT GetPrivate(void);

		// File Padding
		virtual UINT GetPadding(void) const;

		// Persistance
		virtual void Init(void);
		virtual void Kill(void);
		virtual void Load(CTreeFile &Tree);
		virtual void PreSnapshot(void);
		virtual void PreCopy(void);
		virtual void PostPaste(void);
		virtual void PostLoad(void);
		virtual void PostSave(void);
		virtual void Save(CTreeFile &Tree);

		// Data Access
		virtual IDataAccess * GetDataAccess(PCTXT pTag);

		// UI Management
		virtual CViewWnd * CreateView(UINT uType);

		// Download Support
		virtual void PrepareData(void);
		virtual BOOL MakeInitData(CInitData &Init);
		virtual void CompressData(void);

		// Legacy Thunks
		IDataAccess * GetDataAccess(PCSTR pTag);

	protected:
		// Data Members
		DWORD	    m_zdi;
		DWORD	    m_seq;
		CDatabase * m_pDbase;
		CItem     * m_pParent;
		UINT	    m_uPos;

		// Dirty Control
		virtual void OnSetDirty(void);

		// Validation
		void ZdiCheckItem(size_t s) const;
		void ZdiCheckRead(void const *p, size_t s) const;
		void ZdiCheckWrite(void *p, size_t s) const;
		void ZdiAssert(BOOL predicate) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Linked List of Items
//

class DLLAPI CItemList : public CItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CItemList(void);
		CItemList(CLASS Class);

		// Destructor
		~CItemList(void);

		// Item Access
		CItem * GetItem(INDEX Index) const;
		CItem * GetItem(UINT  uPos)  const;

		// List Lookup
		CItem * operator [] (INDEX Index) const;
		CItem * operator [] (UINT  uPos ) const;

		// List Enumeration
		INDEX GetHead(void) const;
		INDEX GetTail(void) const;
		BOOL  GetNext(INDEX &Index) const;
		BOOL  GetPrev(INDEX &Index) const;
		BOOL  Failed(INDEX Index) const;

		// Item Naming
		CString GetFixedName(void) const;
		void    SetFixedName(CString Name);

		// Persistance
		void Init(void);
		void Kill(void);
		void Load(CTreeFile &File);
		void PreSnapshot(void);
		void PreCopy(void);
		void PostLoad(void);
		void PostSave(void);
		void PostPaste(void);
		void Save(CTreeFile &File);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Attributes
		UINT  GetItemCount(void) const;
		BOOL  IsHeadItem(CItem const *pItem) const;
		BOOL  IsTailItem(CItem const *pItem) const;
		INDEX FindItemIndex(CItem const *pItem) const;
		UINT  FindItemPos(CItem const *pItem) const;

		// Operations
		BOOL  SetItemCount(UINT uCount);
		void  SetItemClass(CLASS Class);
		BOOL  AppendItem(CItem *pItem);
		INDEX InsertItem(CItem *pItem, INDEX Index);
		INDEX InsertItem(CItem *pItem, CItem *pBefore);
		INDEX MoveItem(INDEX Index, INDEX Before);
		INDEX MoveItem(CItem *pItem, INDEX Before);
		INDEX MoveItem(CItem *pItem, CItem *pBefore);
		INDEX MoveHeadward(INDEX Index);
		INDEX MoveHeadward(CItem *pItem);
		INDEX MoveTailward(INDEX Index);
		INDEX MoveTailward(CItem *pItem);
		BOOL  RemoveItem(INDEX Index);
		BOOL  RemoveItem(CItem *pItem);
		BOOL  DeleteItem(INDEX Index);
		BOOL  DeleteItem(CItem *pItem);
		BOOL  DeleteAllItems(BOOL fKill);

		// Item Creation
		virtual CItem * CreateItem(PCTXT pName);

	protected:
		// Data Members
		DWORD		m_zdi;
		CLASS		m_Class;
		CLASS	        m_Assume;
		CList <CItem *> m_List;
		UINT            m_Fixed;

		// Index Management
		virtual void AllocIndex(CItem *pItem, INDEX Index);
		virtual void UpdateIndex(CItem *pItem, INDEX Index);
		virtual void FreeIndex(CItem *pItem, INDEX Index);
	};

//////////////////////////////////////////////////////////////////////////
//
// Indexed List of Items
//

class DLLAPI CItemIndexList : public CItemList
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CItemIndexList(void);
		CItemIndexList(CLASS Class);

		// Destructor
		~CItemIndexList(void);

		// Item Access
		CItem * GetItem(INDEX Index) const;
		CItem * GetItem(UINT  uPos ) const;

		// List Lookup
		CItem * operator [] (INDEX Index) const;
		CItem * operator [] (UINT  uPos ) const;

		// Persistance
		void Load(CTreeFile &File);
		void Save(CTreeFile &File);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Attributes
		UINT GetIndexCount(void) const;

		// Operations
		BOOL DeleteItem(INDEX Index);
		BOOL DeleteItem(UINT  uPos);
		BOOL DeleteItem(CItem *pItem);
		BOOL GetOrder(CArray <UINT> &List);
		BOOL SetOrder(CArray <UINT> const &List);

	protected:
		// Data Members
		CArray <INDEX> m_Index;

		// Index Management
		void AllocIndex(CItem *pItem, INDEX Index);
		void UpdateIndex(CItem *pItem, INDEX Index);
		void FreeIndex(CItem *pItem, INDEX Index);
	};

//////////////////////////////////////////////////////////////////////////
//
// Indexed List of Items with Names
//

class DLLAPI CNamedList : public CItemIndexList
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CNamedList(void);
		CNamedList(CLASS Class);

		// Destructor
		~CNamedList(void);

		// Attributes
		INDEX   IndexFromName(CString Name) const;
		CItem * FindByName(CString Name) const;

		// Operations
		void MakeUnique(CString &Name);
		BOOL ItemRenamed(CItem *pItem);
		void ItemRenamed(INDEX Index);
		void RemakeIndex(void);

		// Persistance
		void PostLoad(void);

		// Static Data
		static CString m_Disambig;

	protected:
		// Data Members
		CMap <CString, INDEX> m_Names;

		// Index Management
		void AllocIndex(CItem *pItem, INDEX Index);
		void FreeIndex(CItem *pItem, INDEX Index);
		BOOL MakeIndex(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMetaData;
class CMetaItem;

//////////////////////////////////////////////////////////////////////////
//
// Fixed Handle Values
//

#define HANDLE_OFFSET	UINT(16)

#define HANDLE_HCON	UINT(-1)
#define HANDLE_PCON	UINT(-2)
#define HANDLE_SCON	UINT(-3)
#define HANDLE_UCON	UINT(-4)

#define HANDLE_IMAGE	UINT(0)
#define	HANDLE_SYSTEM	UINT(1)

#define HANDLE_NONE	UINT(0xFFFF0000)

#define HANDLE_BROKE	UINT(0xCDCDCDCD)

//////////////////////////////////////////////////////////////////////////
//
// Meta Data Types
//

enum MetaType
{
	metaNull,
	metaString,
	metaInteger,
	metaNumber,
	metaGUID,
	metaPoint,
	metaRect,
	metaR2R,
	metaBlob,
	metaWide,
	metaHuge,
	metaObject,
	metaVirtual,
	metaCollect
	};

//////////////////////////////////////////////////////////////////////////
//
// Meta Data Macros
//

#define	Meta_Add(n, d, t)	AddMeta(n, t, PBYTE(&(d)) - PBYTE(this))

#define	Meta_AddString(n)	Meta_Add(L#n, m_##n,  metaString)
#define	Meta_AddInteger(n)	Meta_Add(L#n, m_##n,  metaInteger)
#define	Meta_AddNumber(n)	Meta_Add(L#n, m_##n,  metaNumber)
#define	Meta_AddBoolean(n)	Meta_Add(L#n, m_f##n, metaInteger)
#define	Meta_AddGuid(n)		Meta_Add(L#n, m_##n,  metaGUID)
#define	Meta_AddBlob(n)		Meta_Add(L#n, m_##n,  metaBlob)
#define	Meta_AddWide(n)		Meta_Add(L#n, m_##n,  metaWide)
#define	Meta_AddHuge(n)		Meta_Add(L#n, m_##n,  metaHuge)
#define	Meta_AddPoint(n)	Meta_Add(L#n, m_##n,  metaPoint)
#define	Meta_AddRect(n)		Meta_Add(L#n, m_##n,  metaRect)
#define	Meta_AddR2R(n)		Meta_Add(L#n, m_##n,  metaR2R)
#define	Meta_AddObject(n)	Meta_Add(L#n, m_p##n, metaObject)
#define	Meta_AddVirtual(n)	Meta_Add(L#n, m_p##n, metaVirtual)
#define	Meta_AddCollect(n)	Meta_Add(L#n, m_p##n, metaCollect)

#define	Meta_SetName(n)		CMetaItem::m_pList->SetName(n)

//////////////////////////////////////////////////////////////////////////
//
// Meta Data Entry
//

class DLLAPI CMetaData : public IDataAccess
{
	public:
		// Constructors
		CMetaData(void);
		CMetaData(PCTXT pTag, UINT Type, UINT Pos);

		// Attributes
		CString GetTag(void) const;
		UINT    GetType(void) const;
		UINT    GetPos(void) const;

		// Data Read
		PCTXT   ReadString(PCVOID pItem) const;
		UINT    ReadInteger(PCVOID pItem) const;
		number  ReadNumber(PCVOID pItem) const;
		CGuid   ReadGuid(PCVOID pItem) const;

		// Data Write
		void WriteString(PVOID pItem, PCTXT pText) const;
		void WriteString(PVOID pItem, CString const &Text) const;
		void WriteInteger(PVOID pItem, UINT uValue) const;
		void WriteNumber(PVOID pItem, number value) const;
		void WriteGuid(PVOID pItem, CGuid const &Guid) const;

		// Referencing
		CPoint     & GetPoint(PVOID pItem) const;
		CRect      & GetRect(PVOID pItem) const;
		R2R        & GetR2R(PVOID pItem) const;
		CByteArray & GetBlob(PVOID pItem) const;
		CWordArray & GetWide(PVOID pItem) const;
		CLongArray & GetHuge(PVOID pItem) const;
		CItem    * & GetObject(PVOID pItem) const;

		// Dirty Control
		void SetDirty(PVOID pItem) const;

	protected:
		// Data Members
		CString m_Tag;
		UINT	m_Type;
		UINT	m_Pos;

		// Friend Classes
		friend class CMetaList;
	};

//////////////////////////////////////////////////////////////////////////
//
// Meta Data List
//

class DLLAPI CMetaList : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CMetaList(void);

		// Operations
		UINT Add(PCTXT pTag, UINT Type, UINT Pos);
		UINT Add(CMetaData const &Meta);
		void SetName(ENTITY Name);
		void SetName(PCTXT pName);

		// Attributes
		UINT    GetCount(void) const;
		UINT    GetPadding(void) const;
		CString GetName(void) const;

		// Data Access
		CMetaData const * FindData(UINT          uSlot) const;
		CMetaData const * FindData(CString const &Name) const;

	protected:
		// Type Definitions
		typedef CArray <class CMetaData> CList;
		typedef CMap   <CString, UINT>	 CDict;
		
		// Data Members
		CList   m_List;
		CDict   m_Dict;
		UINT    m_uPad;
		PCTXT   m_pName;
		CEntity m_Name;
	};

//////////////////////////////////////////////////////////////////////////
//
// Meta Item
//

class DLLAPI CMetaItem : public CItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMetaItem(void);

		// Destructor
		~CMetaItem(void);

		// Meta Data Access
		CMetaList       * FindMetaList (void) const;
		CMetaData const * FindMetaData (PCTXT pName) const;
		CItemIndexList  * FindIndexList(PCTXT pName) const;

		// Name Padding
		UINT GetPadding(void) const;

		// Item Naming
		CString GetHumanName(void) const;
		CString GetFixedName(void) const;
		void    SetFixedName(CString Name);

		// Item Privacy
		BOOL HasPrivate(void);
		UINT GetPrivate(void);

		// Named Item Support
		BOOL    HasName(void);
		CString GetName(void);
		BOOL    SetName(CString Name);

		// Persistance
		void Init(void);
		void Kill(void);
		void Load(CTreeFile &File);
		void PreSnapshot(void);
		void PreCopy(void);
		void PostPaste(void);
		void PostLoad(void);
		void PostSave(void);
		void Save(CTreeFile &File);

		// Property Upgrade
		virtual void Upgrade(CTreeFile &Tree, CMetaData const *pMeta);

		// Data Access
		IDataAccess * GetDataAccess(PCTXT pTag);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Static Data
		static UINT  m_uAppData;
		static PCTXT m_pHandle;

		// Data Members
		CMetaList         * m_pList;
		CTextStreamMemory * m_pDivert;
		UINT		    m_Fixed;
		
		// Virtual Helpers
		BOOL InitVirtual(CMetaData const *pMeta);
		BOOL KillVirtual(CMetaData const *pMeta);
		BOOL LoadVirtual(CMetaData const *pMeta, CTreeFile &Tree);
		BOOL PostLoadVirtual(CMetaData const *pMeta);
		BOOL PostSaveVirtual(CMetaData const *pMeta);
		BOOL SnapVirtual(CMetaData const *pMeta);
		BOOL CopyVirtual(CMetaData const *pMeta);
		BOOL PasteVirtual(CMetaData const *pMeta);
		BOOL SaveVirtual(CMetaData const *pMeta, CTreeFile &Tree);
		BOOL PrepVirtual(CMetaData const *pMeta);

		// Property Filters
		virtual BOOL SaveProp(CString const &Tag) const;
		virtual BOOL SkipProp(CString const &Tag) const;

		// Meta Data Creation
		virtual void AddMetaData(void);

		// Import Helpers
		BOOL ImportString(CPropValue *pRoot, CString Tag, CString Prop);
		BOOL ImportString(CPropValue *pRoot, CString Tag);
		BOOL ImportNumber(CPropValue *pRoot, CString Tag, CString Prop);
		BOOL ImportNumber(CPropValue *pRoot, CString Tag);

		// Implementation
		BOOL AddMeta(void);
		UINT AddMeta(PCTXT pTag, UINT Type, UINT Pos);
		void SaveProp(CTreeFile &Tree, CMetaData const *pMeta);
		void LoadProp(CTreeFile &Tree, CMetaData const *pMeta);
		void InitHandle(void);
		void RegisterHandle(void);
		void CheckHandle(void);
		void FreeHandle(void);
		void CleanUp(void);

		// Friends
		friend class CTagImport;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CFolderItem;

//////////////////////////////////////////////////////////////////////////
//
// Folder Item
//

class DLLAPI CFolderItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFolderItem(void);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

	protected:
		// Data Members
		CString m_Name;
		UINT    m_Private;
		BOOL    m_fLegacy;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCategory;
class CDatabase;
class CSystemItem;
class CFileImage;

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CArray <CCategory> CCatList;

typedef CCatList  const &  CATLIST;

typedef CCategory const &  RCAT;

//////////////////////////////////////////////////////////////////////////
//
// Datatabse Category
//

class DLLAPI CCategory
{
	public:
		// Data Members
		CDatabase * m_pDbase;
		CEntity     m_Name;
		CString     m_Tag;
		CString	    m_Res;
		DWORD       m_Image1;
		DWORD	    m_Image2;
	};

//////////////////////////////////////////////////////////////////////////
//
// Datatabse Object
//

class DLLAPI CDatabase : public CMetaItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CDatabase(void);
		CDatabase(CString Model);

		// Destructor
		~CDatabase(void);

		// System Item
		CSystemItem * GetSystemItem(void) const;

		// Attributes
		CFilename GetFilename(void) const;
		CString   GetModelName(void) const;
		BOOL	  HasImage(void) const;
		BOOL	  IsReadOnly(void) const;
		BOOL	  IsPrivate(void) const;
		BOOL	  IsDownloadOnly(void) const;
		BOOL	  CanSaveCopy(void) const;
		BOOL	  IsDirty(void) const;
		BOOL	  IsMucky(void) const;
		REFGUID	  GetGuid(void) const;
		CString   GetUniqueSig(void) const;
		CString   GetConfig(void) const;
		BOOL	  HasFlag(CString Name) const;
		CATLIST	  GetCatList(void) const;
		BOOL	  GetRecomp(void) const;
		BOOL	  GetCircle(void) const;
		BOOL	  GetPending(void) const;
		CString   GetLockedName(void) const;
		UINT      GetSoftwareGroup(void) const;

		// Operations
		BOOL LoadFile(CFilename const &Name, BOOL fRead);
		BOOL LoadFile(CFilename const &Name, CString Force, BOOL fRead);
		BOOL LoadFile(CFilename const &Name, CString Force, CString Model, BOOL fRead);
		BOOL SaveFile(CFilename const &Name);
		BOOL SaveCopy(CFilename const &Name);
		BOOL SaveAuto(CFilename const &Name);
		BOOL SaveImage(CFilename const &Name);
		void SetConfig(CString Config);
		void ClearFilename(void);
		BOOL RecoverFilename(void);
		void SetImage(BOOL fImage);
		void SetImageDefault(void);
		void ClearDirty(void);
		void SetDirty(void);
		void ClearReadOnly(void);
		void SetHumanName(CString Name);
		void AddFlag(CString Name);
		void RemFlag(CString Name);
		void ResetCategories(void);
		void AddNavCategory(ENTITY Name, CString Tag, DWORD Image1, DWORD Image2, CString Res);
		void AddResCategory(ENTITY Name, CString Tag, DWORD Image1, DWORD Image2);
		void AddResCategory(ENTITY Name, CString Tag, DWORD Image1, DWORD Image2, CDatabase *pDbase);
		void AddCategory(CCategory const &Cat);
		void SetRecomp(BOOL fRecomp = TRUE);
		void SetCircle(BOOL fCircle = TRUE);
		void SetPending(BOOL fPending = TRUE);
		void BlockErrors(BOOL fBlock);
		BOOL EditSecurity(void);

		// Class Aliasing
		BOOL  AliasModel(CString &Model, CString &Value);
		CLASS AliasClass(CString const &Name);

		// Item Handles
		UINT    AllocHandle(CItem *pItem);
		BOOL    RegisterHandle(UINT uItem, CItem *pItem);
		INDEX   GetHeadHandle(void);
		BOOL    GetNextHandle(INDEX &Index);
		UINT    GetHandleValue(INDEX Index);
		CItem * GetHandleItem(INDEX Index);
		CItem * GetHandleItem(UINT uItem);
		BOOL    HasNewHandles(void);

		// Fixed Names
		UINT    AllocFixedInt(void);
		CString AllocFixedString(void);
		void    CheckFixed(UINT &uFixed);

		// Item Naming
		CString GetTitleName(void) const;
		CString GetHumanName(void) const;
		CString GetFixedName(void) const;

		// Persistance
		void Init(void);
		void Load(CTreeFile &Tree, CString Force);
		void Save(CTreeFile &Tree);

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Download Config
		CString GetDownloadConfig(void);

	protected:
		// Static Data
		static CGuid m_InstGuid;

		// Collection Types
		typedef CMap  <INT, CItem *> CItemMap;
		typedef CTree <CString>      CFlagMap;

		// Data Members
		CGuid	      m_Guid;
		UINT	      m_Version;
		CString       m_Config;
		CString       m_Build;
		CString	      m_Source;
		UINT	      m_Image;
		UINT	      m_Recomp;
		UINT	      m_Circle;
		UINT	      m_Pending;
		UINT	      m_Locked;
		CString	      m_Password;
		CString	      m_Email;
		CSystemItem * m_pSystem;

		// Model Data
		CString    m_Model;
		CString    m_Human;
		CCatList   m_Cats;
		CFlagMap   m_Flags;

		// File Context
		CFileImage * m_pImage;
		CFilename    m_Filename;
		BOOL	     m_fReadOnly;
		BOOL	     m_fDirty;
		BOOL	     m_fMucky;
		BOOL         m_fBlock;

		// Fixed Names
		UINT m_uFixAlloc;

		// Handle Mapping
		CItemMap     m_Map;
		BOOL	     m_fNew;
		INT	     m_nAlloc;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void Construct(void);
		BOOL CheckReadOnly(CFilename const &Name);
		BOOL LoadDLL(CString Model);
		void FreeDLL(void);
		BOOL HideSplash(void);
		BOOL CheckPassword(BOOL &fRead);

	};

//////////////////////////////////////////////////////////////////////////
//
// Software Groups
//

#define SW_GROUP_1	0
#define SW_GROUP_2	1
#define SW_GROUP_3A	2
#define SW_GROUP_3B	3
#define SW_GROUP_3C	4
#define SW_GROUP_4	5

//////////////////////////////////////////////////////////////////////////
//
// System Item Base Class
//

class DLLAPI CSystemItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSystemItem(void);

		// Destructor
		~CSystemItem(void);

		// Version Test
		virtual BOOL NeedNewSoftware(void) const;

		// Model Control
		virtual BOOL    SetModel(CString const &Model);
		virtual CString GetModel(void) const;
		virtual CString GetEmulatorModel(void) const;
		virtual CString GetDisplayName(void) const;

		// Model Editing
		virtual BOOL    SetModelSpec(CString const &Model);
		virtual CString GetModelSpec(void);

		// Model Mapping
		virtual CString GetProcessor(void) const;
		virtual CString GetModelList(void) const;
		virtual CString GetModelInfo(CString Model) const;

		// Conversion
		virtual CString GetConvList(void) const;
		virtual void    PostConvert(void);
		virtual CString GetSpecies(void) const;

		// Download Config
		virtual CString GetDownloadConfig(void) const;
		virtual void    SetDownloadTarget(CString Target);

		// Device Config
		virtual UINT GetSoftwareGroup(void) const;
		virtual BOOL HasExpansion(void) const;
		virtual BOOL GetDeviceConfig(CByteArray &Data, UINT uItem) const;
		virtual BOOL DisableDeviceConfig(UINT uItem);

		// Extra Commands
		virtual UINT    GetExtraCount(void);
		virtual CString GetExtraText (UINT uCmd);
		virtual BOOL    GetExtraState(UINT uCmd);
		virtual BOOL    RunExtraCmd  (CWnd &Wnd, UINT uCmd);

		// Validation
		virtual BOOL HasBroken(void) const;
		virtual BOOL HasCircular(void) const;
		virtual BOOL HasHardware(void) const;
		virtual BOOL FindBroken(void);
		virtual BOOL FindCircular(void);
		virtual void Rebuild(UINT uAction);
		virtual void ShowHardware(void);
		virtual void UpdateHardware(void);

		// Control Project
		virtual BOOL NeedBuild(void);
		virtual BOOL PerformBuild(void);
		virtual BOOL HasControl(void);

		// SQL Queries
		virtual BOOL CheckSqlQueries(void);

		// Watch Window
		virtual CWnd * GetWatchWindow(void);

		// Searching
		virtual BOOL GlobalFind(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Uploadable File Images
//

class DLLAPI CFileImage : public CItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFileImage(void);

		// Destructor
		~CFileImage(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	
	protected:
		// Implementation
		BOOL Crypto(PBYTE pData, UINT uSize, UINT uMode);
		BOOL Crypto(PBYTE pData, UINT uSize, PCSTR pPass);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class COemStrings;

//////////////////////////////////////////////////////////////////////////
//
// Oem String Conversion
//

class DLLAPI COemStrings
{
	public:
		// Constructor
		COemStrings(void);

		// Management
		void OpenLoad(void);

		// Operations
		void Replace(PTXT pBuffer, UINT &uSize);
		void Replace(CString &String);

	protected:
		// Type Definitions
		typedef CMap <CString, CString> CCache;

		// Data Members
		BOOL		  m_fFile;
		CCache		  m_Cache;
		CString		  m_CacheIn;
		CString		  m_CacheOut;
		CStringArray	  m_String1;
		CStringArray	  m_String2;
		UINT		  m_uCount;

		// Implementation
		void LoadPair(CString String1, CString String2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPdfHelpSystem;

//////////////////////////////////////////////////////////////////////////
//
// PDF Help System
//

class DLLAPI CPdfHelpSystem
{
	public:
		// Constructor
		CPdfHelpSystem(UINT uManual);

		// Operations
		UINT FindTopic(CString Topic);
		BOOL ShowHelp (void);
		BOOL ShowHelp (CString Topic);
		BOOL ShowHelp (UINT uPage);

	protected:
		// Data Members
		CFilename m_Manual;
	};

// End of File

#endif
