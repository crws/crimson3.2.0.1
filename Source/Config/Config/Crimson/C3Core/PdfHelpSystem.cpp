
#include "intern.hpp"

#include "DdeCommander.hpp"

#include <Shlwapi.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PDF Help System
//

// Libraries

#pragma comment(lib, "Shlwapi.lib")

// Constructor

CPdfHelpSystem::CPdfHelpSystem(UINT uManual)
{
	m_Manual = C3OemGetManual(uManual);
	}

// Operations

UINT CPdfHelpSystem::FindTopic(CString Topic)
{
	CString Index = m_Manual.WithName(CPrintf("%2.2s_ndx.txt", m_Manual.GetName()));
	
	FILE *  pFile = _wfopen(Index, L"rb");

	if( pFile ) {

		UINT uBase = NOTHING;

		for(;;) {

			char sLine[256] = { 0 };

			fgets(sLine, sizeof(sLine), pFile);

			if( sLine[0] ) {

				if( uBase == NOTHING ) {

					uBase = atoi(sLine);
					}
				else {
					char *pArg = strchr(sLine, '(');

					if( pArg ) {

						*pArg = 0;

						if( CString(sLine) == Topic ) {

							char *pTab = strchr(pArg + 1, '\t');

							if( pTab ) {

								fclose(pFile);

								return uBase + atoi(pTab + 1);
								}

							break;
							}
						}
					else {
						TCHAR wTok = CString(sLine).Find('\t') < NOTHING ? '\t' : ' ';

						CStringArray List;

						CString(sLine).Tokenize(List, wTok);

						UINT c = List.GetCount();

						if( c > 1 ) {

							for( UINT n = 0; n < c - 1; n++ ) {

								if( List[n] == Topic ) {

									fclose(pFile);

									return uBase + watoi(List[c-1]);
									}
								}
							}
						}
					}

				continue;
				}

			break;
			}

		fclose(pFile);
		}

	return NOTHING;
	}

BOOL CPdfHelpSystem::ShowHelp(void)
{
	return ShowHelp(0);
	}

BOOL CPdfHelpSystem::ShowHelp(CString Topic)
{
	return ShowHelp(FindTopic(Topic));
	}

BOOL CPdfHelpSystem::ShowHelp(UINT uPage)
{
	if( uPage < NOTHING ) {

		WCHAR sApp[MAX_PATH] = { 0 };

		DWORD uApp = elements(sApp);

		if( sApp[0] == 0 ) {

			AssocQueryString( ASSOCF_OPEN_BYEXENAME,
					  ASSOCSTR_EXECUTABLE,
					  L"Acrobat.exe",
					  NULL,
					  sApp,
					  &uApp
					  );
			}

		if( sApp[0] == 0 ) {

			AssocQueryString( ASSOCF_OPEN_BYEXENAME,
					  ASSOCSTR_EXECUTABLE,
					  L"AcroRd32.exe",
					  NULL,
					  sApp,
					  &uApp
					  );
			}

		if( sApp[0] ) {

			afxThread->SetWaitMode(TRUE);

			CString Line;

			Line += L"/A ";

			Line += L"toolbar=0";
			
			Line += L"&statusbar=0";

			Line += L"&navpanes=0";

			Line += L"&messages=0";

			if( TRUE || !uPage ) {

				// We always open the file on the command line as
				// it avoids an empty instance of the reader popping
				// up and confusing the user. We could skip this step
				// if using DDE commands, but it's prettier this way.

				if( uPage ) {

					Line += CPrintf(L"&page=%u", uPage);
					}

				Line += L' ';

				Line += L'"' + m_Manual + L'"';
				}
			
			HINSTANCE hAdobe = ShellExecute( afxMainWnd->GetHandle(),
							 NULL,
							 sApp,
							 Line,
							 NULL, 
							 SW_SHOWNORMAL
							 );

			if( INT(hAdobe) > 32 ) {

				if( uPage > 0 ) {

					CDdeCommander *pWnd = New CDdeCommander;

					if( pWnd->Create(WS_POPUP, CRect(0, 0, 1, 1), NULL, NULL, NULL) ) {

						CRegKey Key(HKEY_CLASSES_ROOT);

						CRegKey AppKey = Key.Open(L"acrobat\\shell\\open\\ddeexec\\application");

						CRegKey TopKey = Key.Open(L"acrobat\\shell\\open\\ddeexec\\topic");

						if( AppKey.IsValid() && TopKey.IsValid() ) {

							CString Server = AppKey.GetValueAsString(L"");

							CString Topic  = TopKey.GetValueAsString(L"");

							BOOL    fScan  = FALSE;

							CString Base;

							if( Server == L"AcroViewR10" || Server == L"AcroViewA10" ) {

								// Some versions of Acrobat do not install correctly and leave the
								// wrong Server key in the registry. If we find one of those broken
								// versions, we first look to see if we've found the right server
								// before, otherwise we have to scan through several options.

								CRegKey User = afxModule->GetApp()->GetUserRegKey();

								CRegKey Prog = User.Open(L"Acrobat");

								if( Prog.IsValid() ) {

									Server = Prog.GetValueAsString(L"");
									}
								else {
									Base  = Server.Left(9);

									fScan = TRUE;
									}
								}

							for( UINT p = 0;; p++ ) {

								if( fScan ) {

									// These are the versions we try if we find R10 or A10
									// in the registry. They ought to cover all the possible
									// options that install this broken value!

									static UINT s[] = { 11, 15, 16, 10 };

									if( p == elements(s) ) {

										break;
										}

									Server.Printf(L"%s%2.2u", PCTXT(Base), s[p]);
									}

								if( pWnd->Initiate(Server, Topic) ) {

									if( fScan ) {

										// We've found a valid server name, so store it back
										// in the registry under our own user key so that we
										// can avoid the search later.

										CRegKey User = afxModule->GetApp()->GetUserRegKey();

										CRegKey Prog = User.Create(L"Acrobat");

										Prog.SetValue(L"", Server);
										}

									if( TRUE ) {

										CString Cmd;

										// Have to issue DocOpen even though we already
										// opened the file using the command line...
					
										Cmd += CPrintf(L"[DocOpen(\"%s\")]",     m_Manual);

										Cmd += CPrintf(L"[DocGoTo(\"%s\", %u)]", m_Manual, uPage-1);

										pWnd->Execute(Cmd);
										}

									pWnd->Terminate();

									pWnd->DestroyWindow(TRUE);

									afxThread->SetWaitMode(FALSE);

									return TRUE;
									}

								if( !fScan ) {

									// We have failed so delete any cached server topic in case
									// someone has installed a new broken version over the top
									// of the old broken one! This won't happen in real life but
									// it may well in testing scenarios.

									CRegKey User = afxModule->GetApp()->GetUserRegKey();

									User.Delete(L"Acrobat");

									break;
									}
								}
							}

						pWnd->DestroyWindow(TRUE);
						}
					else
						delete pWnd;
					
					afxThread->SetWaitMode(FALSE);

					afxMainWnd->Error(CString(IDS_UNABLE_TO_SWITCH));

					return TRUE;
					}

				afxThread->SetWaitMode(FALSE);

				return TRUE;
				}

			afxThread->SetWaitMode(FALSE);

			afxMainWnd->Error(CString(IDS_UNABLE_TO_OPEN));

			return TRUE;
			}

		if( !uPage ) {

			afxThread->SetWaitMode(TRUE);

			HINSTANCE hAdobe= ShellExecute( afxMainWnd->GetHandle(),
							L"open",
							m_Manual,
							NULL,
							NULL,
							SW_SHOWNORMAL
							);

			afxThread->SetWaitMode(FALSE);

			if( INT(hAdobe) > 32 ) {

				return TRUE;
				}
			}

		if( afxMainWnd->YesNo(CString(IDS_ADOBE_ACROBAT)) == IDYES ) {

			ShellExecute( NULL,
				      L"open",
				      L"https://get.adobe.com/reader/",
				      NULL,
				      NULL,
				      SW_SHOWNORMAL
				      );
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
