
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Standard Data Object
//

// Runtime Class

AfxImplementRuntimeClass(CDataObject, CObject);

// Constructor

CDataObject::CDataObject(void)
{
	InitData();
	}

// Destructor

CDataObject::~CDataObject(void)
{
	INDEX n = m_List.GetHead();

	while( !m_List.Failed(n) ) {

		STGMEDIUM *pMed = &m_List[n]->m_Med;

		ReleaseStgMedium(pMed);

		delete m_List[n];

		m_List.GetNext(n);
		}

	delete [] m_pFmt;
	}

// Attributes

BOOL CDataObject::IsEmpty(void) const
{
	return m_List.IsEmpty();
	}

// Operations

void CDataObject::AddStream(UINT cfData, CTextStreamMemory &Stream)
{
	FORMATETC Fmt = { WORD(cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { TYMED_HGLOBAL, Stream.TakeOver(), NULL };

	SetData(&Fmt, &Med, TRUE);
	}

void CDataObject::AddText(UINT cfData, PCTXT pText)
{
	UINT    uSize = sizeof(TCHAR) * (wstrlen(pText) + 1);
	
	HGLOBAL hData = GlobalAlloc(GHND, uSize);

	AfxAssume(hData);

	PBYTE   pData = PBYTE(GlobalLock(hData));

	memcpy(pData, pText, uSize);

	GlobalUnlock(hData);

	FORMATETC Fmt = { WORD(cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { TYMED_HGLOBAL, hData, NULL };

	SetData(&Fmt, &Med, TRUE);
	}

void CDataObject::AddText(UINT cfData, LPCSTR pText)
{
	UINT    uSize = sizeof(CHAR) * (strlen(pText) + 1);
	
	HGLOBAL hData = GlobalAlloc(GHND, uSize);

	AfxAssume(hData);

	PBYTE   pData = PBYTE(GlobalLock(hData));

	memcpy(pData, pText, uSize);

	GlobalUnlock(hData);

	FORMATETC Fmt = { WORD(cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { TYMED_HGLOBAL, hData, NULL };

	SetData(&Fmt, &Med, TRUE);
	}

void CDataObject::AddText(PCTXT pText)
{
	AddText(CF_UNICODETEXT, pText);
	}

void CDataObject::AddText(LPCSTR pText)
{
	AddText(CF_TEXT, pText);
	}

void CDataObject::AddEnhMetaFile(UINT cfData, HENHMETAFILE hMeta)
{
	hMeta = CopyEnhMetaFile(hMeta, NULL);

	FORMATETC Fmt = { WORD(cfData), NULL, 1, -1, TYMED_ENHMF };

	STGMEDIUM Med = { TYMED_ENHMF, hMeta, NULL };

	SetData(&Fmt, &Med, TRUE);
	}

void CDataObject::AddEnhMetaFile(HENHMETAFILE hMeta)
{
	AddEnhMetaFile(CF_ENHMETAFILE, hMeta);
	}

void CDataObject::AddGlobalData(UINT cfData, PCBYTE pBits, UINT uSize)
{
	HGLOBAL hData = GlobalAlloc(GHND, uSize);

	AfxAssume(hData);

	PBYTE   pData = PBYTE(GlobalLock(hData));

	memcpy(pData, pBits, uSize);

	GlobalUnlock(hData);

	AddGlobalData(cfData, hData);
	}

void CDataObject::AddGlobalData(UINT cfData, HGLOBAL hData)
{
	FORMATETC Fmt = { WORD(cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { TYMED_HGLOBAL, hData, NULL };

	SetData(&Fmt, &Med, TRUE);
	}

void CDataObject::AddBitmapData(PCBYTE pBits, UINT uSize)
{
	AddGlobalData(CF_DIB, pBits, uSize);
	}

// IUnknown

HRESULT CDataObject::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDataObject ) {

			*ppObject = (IDataObject *) this;

			m_uRefs++;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CDataObject::AddRef(void)
{
	return ++m_uRefs;
	}

ULONG CDataObject::Release(void)
{
	if( !--m_uRefs ) {

		delete this;

		return 0;
		}

	return m_uRefs;
	}

// IDataObject

HRESULT CDataObject::GetData(FORMATETC *pFmt, STGMEDIUM *pMed)
{
	if( !pFmt->ptd ) {

		CEntry *pEntry = FindEntry(pFmt, FALSE);

		if( pEntry ) {

			*pMed = pEntry->m_Med;

			AddMediumRef(pMed);

			return S_OK;
			}
		}

	return DV_E_FORMATETC;
	}

HRESULT CDataObject::GetDataHere(FORMATETC *pFmt, STGMEDIUM *pMed)
{
	CEntry *pEntry = FindEntry(pFmt, TRUE);

	if( pEntry ) {

		if( pFmt->tymed == TYMED_ISTREAM ) {

			STATSTG Stat;

			LARGE_INTEGER  Seek  = { 0, 0 };

			pEntry->m_Med.pstm->Seek(Seek, 0, NULL);

			pEntry->m_Med.pstm->Stat(&Stat, 0);

			pEntry->m_Med.pstm->CopyTo(pMed->pstm, Stat.cbSize, NULL, NULL);

			pEntry->m_Med.pstm->Seek(Seek, 0, NULL);

			return S_OK;
			}

		if( pFmt->tymed == TYMED_ISTORAGE ) {

			pEntry->m_Med.pstg->CopyTo(0, 0, 0, pMed->pstg);

			return S_OK;
			}

		AfxAssert(FALSE);
		}

	return DV_E_FORMATETC;
	}

HRESULT CDataObject::QueryGetData(FORMATETC *pFmt)
{
	if( !pFmt->ptd ) {

		if( FindEntry(pFmt, FALSE) ) {

			return S_OK;
			}
		}

	return DV_E_FORMATETC;
	}

HRESULT CDataObject::GetCanonicalFormatEtc(FORMATETC *pFmtIn, FORMATETC *pFmtOut)
{
	memcpy(pFmtOut, pFmtIn, sizeof(FORMATETC));
	
	pFmtOut->ptd = NULL;

	return DATA_S_SAMEFORMATETC;
	}

HRESULT CDataObject::SetData(FORMATETC *pFmt, STGMEDIUM *pMed, BOOL fRelease)
{
	if( fRelease ) {

		CEntry *pEntry = FindEntry(pFmt, TRUE);

		if( pEntry) {

			if( pEntry->m_Med.tymed ) {

				STGMEDIUM *pOld = &pEntry->m_Med;

				ReleaseStgMedium(pOld);
				}

			pEntry->m_Med = *pMed;

			if( pEntry->m_Med.pUnkForRelease ) {

				if( SameObject(pEntry->m_Med.pUnkForRelease, this) ) {

					pEntry->m_Med.pUnkForRelease->Release();

					pEntry->m_Med.pUnkForRelease = NULL;
					}
				}

			pEntry->m_Fmt.tymed = pEntry->m_Med.tymed;

			m_fFmt = TRUE;

			return S_OK;
			}

		return DV_E_FORMATETC;
		}

	AfxAssert(FALSE);

	return E_NOTIMPL;
	}

HRESULT CDataObject::EnumFormatEtc(DWORD dwDirection, IEnumFORMATETC **ppEnum)
{
	BuildFmtList();

	SHCreateStdEnumFmtEtc(m_uFmt, m_pFmt, ppEnum);

	return S_OK;
	}

HRESULT CDataObject::DAdvise(FORMATETC *pFmt, DWORD advf, IAdviseSink *pAdvSink, DWORD *pdwConnection)
{
	return OLE_E_ADVISENOTSUPPORTED;
	}

HRESULT CDataObject::DUnadvise(DWORD dwConnection)
{
	return OLE_E_ADVISENOTSUPPORTED;
	}

HRESULT CDataObject::EnumDAdvise(IEnumSTATDATA **ppenumAdvise)
{
	return OLE_E_ADVISENOTSUPPORTED;
	}

// Implementation

void CDataObject::InitData(void)
{
	m_uRefs = 1;

	m_fFmt = FALSE;

	m_pFmt = NULL;
	}

CDataObject::CEntry * CDataObject::FindEntry(FORMATETC *pFmt, BOOL fAdd)
{
	INDEX n = m_List.GetHead();

	while( !m_List.Failed(n) ) {

		CEntry *pEntry = m_List[n];

		if( pEntry->m_Fmt.cfFormat == pFmt->cfFormat ) {

			if( pEntry->m_Fmt.dwAspect == pFmt->dwAspect ) {

				if( pEntry->m_Fmt.lindex == pFmt->lindex ) {

					if( fAdd ) {

						return pEntry;
						}

					if( pEntry->m_Fmt.tymed & pFmt->tymed ) {

						return pEntry;
						}
					}
				}
			}

		m_List.GetNext(n);
		}

	if( fAdd ) {

		CEntry *pEntry = New CEntry;
		
		memset(pEntry, 0, sizeof(CEntry));

		pEntry->m_Fmt = *pFmt;

		m_List.Append(pEntry);

		return pEntry;
		}
	
	return NULL;
	}

BOOL CDataObject::AddMediumRef(STGMEDIUM *pMed)
{
	if( !pMed->pUnkForRelease ) {

		if( !(pMed->tymed & (TYMED_ISTREAM | TYMED_ISTORAGE)) ) {

			pMed->pUnkForRelease = this;
			}
		}

	if( pMed->tymed == TYMED_ISTREAM ) {

		pMed->pstm->AddRef();
		}

	if( pMed->tymed == TYMED_ISTORAGE ) {

		pMed->pstg->AddRef();
		}

        if( pMed->pUnkForRelease ) {
            
		pMed->pUnkForRelease->AddRef();
	        }

	return TRUE;
	}

BOOL CDataObject::BuildFmtList(void)
{
	if( m_fFmt ) {

		delete [] m_pFmt;
		
		m_fFmt = FALSE;

		m_uFmt = m_List.GetCount();

		if( m_uFmt ) {

			m_pFmt = New FORMATETC [ m_uFmt ];

			INDEX n = m_List.GetHead();

			UINT  i = 0;

			while( !m_List.Failed(n) ) {

				m_pFmt[i++] = m_List[n]->m_Fmt;

				m_List.GetNext(n);
				}

			return TRUE;
			}

		m_pFmt = NULL;

		return FALSE;
		}

	return TRUE;
	}

BOOL CDataObject::SameObject(IUnknown *punkOne, IUnknown *punkTwo)
{
	IUnknown *punkCanonOne;
	
	IUnknown *punkCanonTwo;

	punkOne->QueryInterface(IID_IUnknown, (void **) &punkCanonOne);

	punkTwo->QueryInterface(IID_IUnknown, (void **) &punkCanonTwo);

	BOOL fSame = (punkCanonOne == punkCanonTwo);

	punkCanonOne->Release();

	punkCanonTwo->Release();

	return fSame;
	}

// End of File
