
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2001 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window
//

// Static Data

UINT CNavTreeWnd::m_timerDouble = AllocTimerID();

UINT CNavTreeWnd::m_timerExpand = AllocTimerID();

UINT CNavTreeWnd::m_timerScroll = AllocTimerID();

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd, CViewWnd);

// Constructor

CNavTreeWnd::CNavTreeWnd(CString List, CLASS Folder, CLASS Class)
{
	m_List	  = List;

	m_Folder  = Folder;

	m_Class   = Class;

	m_cfData  = RegisterClipboardFormat(L"C3 Tree File");
		 
	m_pTree   = New CTreeView;

	m_fLock   = FALSE;

	m_fDblClk = FALSE;

	m_hDrag   = NULL;

	m_fDrag   = FALSE;

	m_Accel.Create(L"NavTreeAccel");
	}

// Overridables

void CNavTreeWnd::OnAttach(void)
{
	m_pItem = (CMetaItem *) CViewWnd::m_pItem;

	CMetaData const *pMeta = m_pItem->FindMetaData(m_List);

	if( pMeta ) {

		CItem *pList = pMeta->GetObject(m_pItem);

		if( pList->IsKindOf(AfxRuntimeClass(CNamedIndexList)) ) {

			m_pList = (CNamedIndexList *) pList;

			return;
			}
		}

	AfxAssert(FALSE);
	}

BOOL CNavTreeWnd::OnNavigate(CString Nav)
{
	if( Nav.IsEmpty() ) {

		m_pTree->SelectItem(m_hRoot, TVGN_CARET);

		return TRUE;
		}
	else {
		CString   Prefix = m_pList->GetFixedName();

		UINT      uStrip = Prefix.GetLength() + 1;

		HTREEITEM hFind  = m_MapFixed[Nav.Mid(uStrip)];

		if( hFind ) {
				
			m_pTree->SelectItem(hFind, TVGN_CARET);

			SetViewedItem();

			return TRUE;
			}

		return FALSE;
		}
	}

void CNavTreeWnd::OnExec(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnExecCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnExecRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnExecDelete((CCmdDelete *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnExecPaste((CCmdPaste *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		LockUpdate(!m_fLock);
		}
	}

void CNavTreeWnd::OnUndo(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnUndoCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnUndoRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnUndoDelete((CCmdDelete *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnUndoPaste((CCmdPaste *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		LockUpdate(!m_fLock);
		}
	}

// Message Map

AfxMessageMap(CNavTreeWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchNotify(100, TVN_SELCHANGING,    OnTreeSelChanging)
	AfxDispatchNotify(100, TVN_SELCHANGED,     OnTreeSelChanged )
	AfxDispatchNotify(100, TVN_KEYDOWN,        OnTreeKeyDown    )
	AfxDispatchNotify(100, NM_CUSTOMDRAW,      OnTreeCustomDraw )
	AfxDispatchNotify(100, NM_RETURN,          OnTreeReturn     )
	AfxDispatchNotify(100, NM_DBLCLK,          OnTreeDblClk     )
	AfxDispatchNotify(100, NM_RCLICK,          OnTreeRClick     )
	AfxDispatchNotify(100, TVN_BEGINDRAG,      OnTreeBeginDrag  )
	AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit  )
	AfxDispatchNotify(100, TVN_ENDLABELEDIT,   OnTreeEndEdit    )
	AfxDispatchNotify(100, TVN_ITEMEXPANDING,  OnTreeExpanding  )

	AfxDispatchControlType(IDM_GO,   OnGoControl  )
	AfxDispatchCommandType(IDM_GO,   OnGoCommand  )
	AfxDispatchControlType(IDM_ITEM, OnItemControl)
	AfxDispatchCommandType(IDM_ITEM, OnItemCommand)
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxMessageEnd(CNavTreeWnd)
	};

// Accelerator

BOOL CNavTreeWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CNavTreeWnd::OnPostCreate(void)
{
	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_LINESATROOT
		      | TVS_HASLINES 
		      | TVS_EDITLABELS 
		      | TVS_NOTOOLTIPS
		      |	TVS_TRACKSELECT
		      | TVS_SHOWSELALWAYS;

	m_pTree->Create(dwStyle, GetClientRect() - 1, ThisObject, 100);

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TreeIcon16"), afxColor(MAGENTA));

	m_pTree->SetImageList(TVSIL_NORMAL, m_Images);

	m_pTree->SetImageList(TVSIL_STATE,  m_Images);

	LoadRoot();

	LoadTree();

	m_pTree->ShowWindow(SW_SHOW);
	}

void CNavTreeWnd::OnSize(UINT uCode, CSize Size)
{
	m_pTree->MoveWindow(GetClientRect() - 1, TRUE);
	}

void CNavTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( fShow ) {

		SetNavCheckpoint();

		SetViewedItem();
		}
	}

void CNavTreeWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.FrameRect(Rect, afxBrush(WHITE));
	}

void CNavTreeWnd::OnSetCurrent(BOOL fCurrent)
{
	}

void CNavTreeWnd::OnSetFocus(CWnd &Wnd)
{
	if( !m_fDrag ) {

		m_pTree->SetFocus();
		}
	}

void CNavTreeWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	TrackDrag();
	}

void CNavTreeWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	EndDrag(FALSE);
	}

void CNavTreeWnd::OnKeyDown(UINT uCode, DWORD dwData)
{
	if( m_fDrag ) {

		switch( uCode ) {

			case VK_ESCAPE:

				EndDrag(TRUE);

				break;

			case VK_CONTROL:

				UpdateDragImage();

				break;
			}
		}
	}

void CNavTreeWnd::OnKeyUp(UINT uCode, DWORD dwData)
{
	if( m_fDrag ) {

		switch( uCode ) {

			case VK_CONTROL:

				UpdateDragImage();

				break;
			}
		}
	}

void CNavTreeWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerDouble ) {

		afxMainWnd->SendMessage(WM_CANCELMODE);

		CLASS    Class = AfxRuntimeClass(CSystemWnd);

		CViewWnd *pWnd = (CViewWnd *) &GetParent(Class).GetDlgItem(IDVIEW+2);

		if( !pWnd->IsCurrent() ) {

			AfxNull(CWnd).SetFocus();

			pWnd->SetFocus();
			}

		KillTimer(uID);
		}

	if( uID == m_timerExpand ) {

		if( m_fDrag && IsExpandDrop() ) {

			DrawDragImage();
			
			ShowDrop(FALSE);

			m_pTree->Expand(m_hDropRoot, TVE_EXPAND);

			m_pTree->UpdateWindow();
			
			ShowDrop(TRUE);

			DrawDragImage();
			}

		KillTimer(uID);
		}

	if( uID == m_timerScroll ) {

		if( m_fDrag ) {

			UINT  uCmd = NOTHING;

			CRect View = m_pTree->GetClientRect();

			ClientToScreen(View);

			if( View.PtInRect(m_DragPos) ) {

				View -= 32;

				if( m_DragPos.y < View.top ) {

					int nLimit = m_pTree->GetScrollRangeMin(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) > nLimit ) {

						uCmd = SB_LINEUP;
						}
					}

				if( m_DragPos.y > View.bottom ) {

					int nLimit = m_pTree->GetScrollRangeMax(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) < nLimit ) {

						uCmd = SB_LINEDOWN;
						}
					}
				
				if( uCmd < NOTHING ) {

					DrawDragImage();
					
					ShowDrop(FALSE);

					m_pTree->SendMessage(WM_VSCROLL, uCmd);

					m_pTree->UpdateWindow();
					
					ShowDrop(TRUE);

					DrawDragImage();

					return;
					}
				}
			}
		
		KillTimer(uID);
		}
	}

// Notification Handlers

BOOL CNavTreeWnd::OnTreeSelChanging(UINT uID, NMTREEVIEW &Info)
{
	return FALSE;
	}

void CNavTreeWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( m_hSelect != Info.itemNew.hItem ) {

		if( Info.action == TVC_BYMOUSE ) {
			
			SetFocus();
			}

		m_hSelect = Info.itemNew.hItem;

		m_pSelect = (CItem *) Info.itemNew.lParam;

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CNamedItem)) ) {

			m_pNamed = (CNamedItem *) m_pSelect;
			}
		else
			m_pNamed = NULL;

		SetViewedItem();
		}
	}

BOOL CNavTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {
		
		case VK_F2:
			
			m_pTree->EditLabel(m_hSelect);
			
			return TRUE;

		case VK_ESCAPE:

			afxMainWnd->SendMessage(WM_CANCELMODE);

			EndDrag(TRUE);

			return TRUE;
		}

	return FALSE;
	}

UINT CNavTreeWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
		}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		if( Info.uItemState & CDIS_HOT ) {

			if( Info.uItemState & CDIS_SELECTED ) {

				SetBkColor(Info.hdc, afxColor(Orange2));
				}
			else
				SetBkColor(Info.hdc, afxColor(Orange3));

			SetTextColor(Info.hdc, afxColor(BLACK));

			SelectObject(Info.hdc, m_pTree->GetFont());

			return CDRF_NEWFONT;
			}
		}

	return 0;
	}

void CNavTreeWnd::OnTreeReturn(UINT uID, NMHDR &Info)
{
	if( m_pTree->GetChild(m_hSelect) ) {

		m_pTree->Expand(m_hSelect, TVE_TOGGLE);
		}
	else {
		AfxNull(CWnd).SetFocus();

		CLASS Class = AfxRuntimeClass(CSystemWnd);

		CWnd *pWnd  = &GetParent(Class).GetDlgItem(IDVIEW+2);

		pWnd->SetFocus();
		}
	}

void CNavTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->GetChild(m_hSelect) ) {

		m_pTree->Expand(m_hSelect, TVE_TOGGLE);

		return;
		}

	SetTimer(m_timerDouble, 1);
	}

BOOL CNavTreeWnd::OnTreeRClick(UINT uID, NMHDR &Info)
{
	CPoint Pos = GetCursorPos();

	CPoint Hit = Pos;

	m_pTree->ScreenToClient(Hit);

	HTREEITEM hItem = m_pTree->HitTestItem(Hit);

	if( hItem ) {

		m_pTree->SelectItem(hItem, TVGN_CARET);

		CMenu Menu(L"NavTreeCtxMenu");

		Menu.MakeOwnerDraw(FALSE);

		Menu.GetSubMenu(0).TrackPopupMenu( TPM_LEFTALIGN,
						   Pos,
						   afxMainWnd->GetHandle()
						   );

		Menu.FreeOwnerDraw();
		}

	return TRUE;
	}

void CNavTreeWnd::OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info)
{
	HTREEITEM hItem = Info.itemNew.hItem;

	m_pTree->SelectItem(hItem, TVGN_CARET);

	StartDrag();
	}

BOOL CNavTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{
	return !m_pNamed;
	}

BOOL CNavTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
		}
	
	return FALSE;
	}

BOOL CNavTreeWnd::OnTreeExpanding(UINT uID, NMTREEVIEW &Info)
{
	HTREEITEM hItem = Info.itemNew.hItem;

	CTreeViewItem Node(hItem, TVIF_IMAGE);

	m_pTree->GetItem(Node);

	UINT uImage = Node.GetImage();

	uImage &= ~1;

	uImage |= (Info.action == TVE_EXPAND) ? 1 : 0;

	Node.SetImages(uImage);

	m_pTree->SetItem(Node);

	return FALSE;
	}

// Command Handlers

BOOL CNavTreeWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_GO_NEXT:

			Src.EnableItem(!!m_pTree->GetNextItem(m_hSelect));
			
			return TRUE;
		
		case IDM_GO_PREV:

			Src.EnableItem(!!m_pTree->GetPrevItem(m_hSelect));

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnGoCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GO_NEXT:

			OnGoNext();

			return TRUE;

		case IDM_GO_PREV:

			OnGoPrev();

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_ITEM_DELETE:
		case IDM_ITEM_RENAME:

			Src.EnableItem(m_hSelect != m_hRoot);

			return TRUE;

		case IDM_ITEM_NEW_FOLDER:

			Src.EnableItem(TRUE);

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_DELETE:

			OnItemDelete(IDS_DELETE_1, TRUE, TRUE);

			return TRUE;

		case IDM_ITEM_RENAME:

			m_pTree->EditLabel(m_hSelect);

			return TRUE;

		case IDM_ITEM_NEW_FOLDER:

			OnItemCreateFolder();

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:
		case IDM_EDIT_CUT:
		case IDM_EDIT_COPY:

			Src.EnableItem(m_hSelect != m_hRoot);

			return TRUE;

		case IDM_EDIT_PASTE:

			Src.EnableItem(CanEditPaste());

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			OnItemDelete(IDS_DELETE_1, TRUE, TRUE);

			return TRUE;

		case IDM_EDIT_CUT:

			OnEditCut();

			return TRUE;

		case IDM_EDIT_COPY:

			OnEditCopy();

			return TRUE;

		case IDM_EDIT_PASTE:

			OnEditPaste();

			return TRUE;
		}

	return FALSE;
	}

// Go Menu

BOOL CNavTreeWnd::OnGoNext(void)
{
	HTREEITEM hItem = m_pTree->GetNextItem(m_hSelect);

	if( hItem ) {

		m_pTree->SelectItem(hItem, TVGN_CARET);

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnGoPrev(void)
{
	HTREEITEM hItem = m_pTree->GetPrevItem(m_hSelect);

	if( hItem ) {

		m_pTree->SelectItem(hItem, TVGN_CARET);

		return TRUE;
		}

	return FALSE;
	}

// Item Menu : Create

void CNavTreeWnd::OnItemCreateFolder(void)
{
	CCmd *pCmd = New CCmdCreate(m_pSelect, IDS_GROUP, m_Folder);

	SaveCmd(pCmd);
	}

void CNavTreeWnd::OnExecCreate(CCmdCreate *pCmd)
{
	CNamedItem *pItem = AfxNewObject(CNamedItem, pCmd->m_Class);

	pItem->Init();

	pItem->m_Fixed = pCmd->m_Fixed;

	pItem->m_Name  = GetRoot(TRUE) + pCmd->m_Name;

	MakeUnique(pItem->m_Name);

	PasteIntoList(m_hSelect, pItem);

	PasteIntoTree(m_hSelect, pItem);

	pCmd->m_Item = pItem->GetFixedPath();

	pCmd->m_Menu = CFormat(pCmd->m_Menu, pItem->m_Name);

	m_pTree->Expand(m_hSelect, TVE_EXPAND);
	}

void CNavTreeWnd::OnUndoCreate(CCmdCreate *pCmd)
{
	HTREEITEM    hItem = m_hSelect;

	CNamedItem * pItem = m_pNamed;
	
	HTREEITEM    hPrev = m_MapFixed[pCmd->m_Prev];

	m_pTree->SelectItem(hPrev, TVGN_CARET);

	RemoveFromMaps(pItem);

	m_pList->DeleteItem(pItem);

	m_pTree->DeleteItem(hItem);
	}

// Item Menu : Rename

BOOL CNavTreeWnd::OnItemRename(CString Name)
{
	UINT    uPos = m_pNamed->m_Name.FindRev(L'.');

	CString Prev = m_pNamed->m_Name.Mid(uPos + 1);

	if( wstrcmp(Name, Prev) ) {

		if( uPos < NOTHING ) {

			Name = m_pNamed->m_Name.Left(uPos + 1) + Name;
			}

		if( m_MapNames[Name] ) {
		
			CString Text = CFormat(IDS_THE_NAME, Name);
		
			Error(Text);
			}
		else {
			CError Error(TRUE);

			if( C3ValidateName(Error, GetName(Name)) ) {

				CCmd *pCmd = New CCmdRename(m_pNamed, Name);

				SaveCmd(pCmd);

				return TRUE;
				}
			
			Error.Show(ThisObject);
			}
		}

	return FALSE;
	}

void CNavTreeWnd::OnExecRename(CCmdRename *pCmd)
{
	CString Prev = pCmd->m_Prev;
	
	CString Name = pCmd->m_Name;

	m_pNamed->m_Name = Name;

	m_pNamed->SetDirty();

	m_pTree->SetItemText(m_hSelect, GetName(Name));

	m_MapNames.Remove(Prev);

	m_MapNames.Insert(Name, m_hSelect);

	RenameFrom(m_hSelect, Prev.GetLength(), Name);
	}

void CNavTreeWnd::OnUndoRename(CCmdRename *pCmd)
{
	CString Prev = pCmd->m_Name;
	
	CString Name = pCmd->m_Prev;

	m_pNamed->m_Name = Name;

	m_pNamed->SetDirty();

	m_pTree->SetItemText(m_hSelect, GetName(Name));

	m_MapNames.Remove(Prev);

	m_MapNames.Insert(Name, m_hSelect);

	RenameFrom(m_hSelect, Prev.GetLength(), Name);
	}

BOOL CNavTreeWnd::RenameFrom(HTREEITEM hRoot, UINT uStrip, CString const &Name)
{
	HTREEITEM hItem = m_pTree->GetChild(hRoot);

	if( hItem ) {

		while( hItem ) {

			CNamedItem *pItem = (CNamedItem *) m_pTree->GetItemParam(hItem);

			pItem->m_Name = Name + L"." + pItem->m_Name.Mid(uStrip + 1);

			pItem->SetDirty();

			RenameFrom(hItem, uStrip, Name);

			hItem = m_pTree->GetNext(hItem);
			}
		
		return TRUE;
		}

	return FALSE;
	}

// Item Menu : Delete

void CNavTreeWnd::OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop)
{
	if( IsFolder() ) {

		if( !fWarn || WarnMultiple(IDS_DELETE_2) ) {

			HTREEITEM hInit = m_hSelect;

			CString   Item  = m_pSelect->GetFixedPath();

			CString   Verb  = uVerb;

			CString   Menu  = CFormat(IDS_FORMAT, Verb, m_pNamed->m_Name);

			MarkMulti(Item, Menu, fTop);

			OnGoNext();

			while( !(m_hSelect == hInit) ) {

				OnItemDelete(uVerb, FALSE, FALSE);
				}

			OnItemDelete(uVerb, FALSE, FALSE);

			MarkMulti(Item, Menu, fTop);
			}
		}
	else {
		HTREEITEM hRoot = m_pTree->GetParent  (m_hSelect);

		HTREEITEM hPrev = m_pTree->GetPrevious(m_hSelect);

		CItem *   pRoot = hRoot ? (CItem *) m_pTree->GetItemParam(hRoot) : NULL;

		CItem *   pPrev = hPrev ? (CItem *) m_pTree->GetItemParam(hPrev) : NULL;

		CCmd  *   pCmd  = New CCmdDelete(m_pNamed, pRoot, pPrev);

		SaveCmd(pCmd);
		}
	}

void CNavTreeWnd::OnExecDelete(CCmdDelete *pCmd)
{
	HTREEITEM    hItem = m_hSelect;

	CNamedItem * pItem = m_pNamed;

	OnGoNext() || OnGoPrev();

	RemoveFromMaps(pItem);

	m_pList->DeleteItem(pItem);

	m_pTree->DeleteItem(hItem);
	}

void CNavTreeWnd::OnUndoDelete(CCmdDelete *pCmd)
{
	CNamedItem *pNamed = (CNamedItem *) CItem::MakeFromMemory(pCmd->m_hData);

	m_pList->AppendItem(pNamed);

	CTreeViewItem Node;

	LoadNodeItem(Node, pNamed);

	HTREEITEM hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM hPrev = m_MapFixed[pCmd->m_Prev];

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, hPrev ? hPrev : TVI_FIRST, Node);

	AddToMaps(pNamed, hItem);

	m_pTree->SelectItem(hItem, TVGN_CARET);

	SetNavCheckpoint();
	}

// Edit Menu : Cut and Copy

void CNavTreeWnd::OnEditCut(void)
{
	if( !IsFolder() || WarnMultiple(IDS_CUT_1) ) {

		OnEditCopy();
		
		OnItemDelete(IDS_CUT_2, FALSE, TRUE);
		}
	}

void CNavTreeWnd::OnEditCopy(void)
{
	if( OpenClipboard(ThisObject) ) {

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			CTreeFile Tree;

			if( Tree.OpenSave(Stream) ) {

				OnEditCopy(Tree, m_hSelect);

				Tree.Close();

				EmptyClipboard();

				SetClipboardData(m_cfData, Stream.TakeOver());
				}

			CloseClipboard();
			}
		}
	}

void CNavTreeWnd::OnEditCopy(CTreeFile &Tree, HTREEITEM hItem)
{
	CItem   *pItem = (CItem *) m_pTree->GetItemParam(hItem);

	CString  Class = pItem->GetClassName();

	Tree.PutObject(Class.Mid(1));

	pItem->PreCopy();

	pItem->Save(Tree);

	Tree.EndObject();

	if( (hItem = m_pTree->GetChild(hItem)) ) {

		while( hItem ) {

			OnEditCopy(Tree, hItem);

			hItem = m_pTree->GetNext(hItem);
			}
		}
	}

// Edit Menu : Paste

BOOL CNavTreeWnd::CanEditPaste(void)
{
	if( IsClipboardFormatAvailable(m_cfData) ) {

		BOOL fPaste = FALSE;

		if( OpenClipboard(ThisObject) ) {

			CTextStreamMemory Stream;

			if( Stream.OpenLoad(GetClipboardData(m_cfData)) ) {

				CTreeFile Tree;

				if( Tree.OpenLoad(Stream) ) {

					while( !Tree.IsEndOfData() ) {

						CString Name = Tree.GetName();

						if( !Name.IsEmpty() ) {

							CLASS Class = AfxNamedClass(PCTXT(L"C" + Name));

							if( !Class->IsKindOf(m_Class) ) {

								if( !Class->IsKindOf(m_Folder) ) {

									fPaste = FALSE;

									break;
									}
								}

							Tree.GetObject();

							while( !Tree.IsEndOfData() ) {

								Tree.GetName();
								}

							Tree.EndObject();

							fPaste = TRUE;
							}
						}
					}
				}

			CloseClipboard();
			}

		return fPaste;
		}

	return FALSE;
	}

void CNavTreeWnd::OnEditPaste(void)
{
	if( IsClipboardFormatAvailable(m_cfData) ) {

		if( OpenClipboard(ThisObject) ) {

			OnEditPaste( m_pSelect,
				     m_hSelect,
				     m_pTree->GetParent(m_hSelect),
				     FALSE,
				     GetClipboardData(m_cfData)
				     );

			CloseClipboard();
			}
		}
	}

void CNavTreeWnd::OnEditPaste(CItem *pItem, HTREEITEM hPrev, HTREEITEM hRoot, BOOL fMove, HGLOBAL hData)
{
	CItem * pPrev = (CItem *) (hPrev ? m_pTree->GetItemParam(hPrev) : NULL);
	
	CItem * pRoot = (CItem *) (hRoot ? m_pTree->GetItemParam(hRoot) : NULL);

	CCmd  * pCmd  = New CCmdPaste(pItem, pPrev, pRoot, fMove, hData);

	SaveCmd(pCmd);
	}

void CNavTreeWnd::OnExecPaste(CCmdPaste *pCmd)
{
	CTextStreamMemory Stream;

	if( Stream.OpenLoad(pCmd->m_hData) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			HTREEITEM    hPrev = m_MapFixed[pCmd->m_Prev];

			HTREEITEM    hBase = NULL;

			CNamedItem * pBase = NULL;
			
			UINT         uBase = 0;

			UINT         uSize = pCmd->m_Names.GetCount();

			UINT         uPos  = 0;

			CString      Root  = L"";

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				if( !Name.IsEmpty() ) {

					CLASS       Class = AfxNamedClass(PCTXT(L"C" + Name));

					CNamedItem *pItem = AfxNewObject(CNamedItem, Class);

					Tree.GetObject();

					pItem->SetParent(m_pList);

					pItem->Load(Tree);

					Tree.EndObject();

					if( pBase ) {

						pItem->m_Name = pBase->m_Name + pItem->m_Name.Mid(uBase);
						}
					else {
						HTREEITEM hRoot = m_MapFixed[pCmd->m_Root];

						Root  = GetRoot(hRoot, TRUE);

						uBase = pItem->m_Name.GetLength();

						pItem->m_Name = Root + GetName(pItem->m_Name);

						pBase = pItem;
						}

					MakeUnique(pItem->m_Name);

					PasteIntoList(hPrev, pItem);

					if( !uSize ) {
						
						pItem->PostPaste();
						
						pItem->PostLoad();

						pCmd->m_Names.Append(pItem->GetFixedName());
						}
					else {
						pItem->m_Fixed = wcstol(pCmd->m_Names[uPos], NULL, 16);
						
						pItem->PostLoad();

						uPos++;
						}

					PasteIntoTree(hPrev, hPrev, pItem);

					if( !hBase ) {

						hBase = hPrev;
						}
					}
				}
			
			if( !pCmd->m_fMove && pBase->IsKindOf(m_Folder) ) {

				m_pTree->Expand(hBase, TVE_EXPAND);
				}
			else {
				m_pTree->SelectItem(hBase, TVGN_CARET);

				m_pTree->Expand(m_hSelect, TVE_EXPAND);
				}

			pCmd->m_Menu.Format(pCmd->m_Menu, pBase->m_Name);
			}

		m_pTree->EnsureVisible(m_hSelect);
		}
	}

void CNavTreeWnd::OnUndoPaste(CCmdPaste *pCmd)
{
	UINT n = pCmd->m_Names.GetCount();

	while( n-- ) {

		CString      Fixed = pCmd->m_Names[n];

		HTREEITEM    hItem = m_MapFixed[Fixed];

		CNamedItem * pItem = (CNamedItem *) m_pTree->GetItemParam(hItem);

		RemoveFromMaps(pItem);

		m_pList->DeleteItem(pItem);

		m_pTree->DeleteItem(hItem);
		}
	}

void CNavTreeWnd::PasteIntoList(HTREEITEM hPrev, CNamedItem *pItem)
{
	INDEX n = m_pList->FindItemIndex(m_pSelect);

	m_pList->GetNext(n);

	m_pList->InsertItem(pItem, n);
	}

void CNavTreeWnd::PasteIntoTree(HTREEITEM &hItem, HTREEITEM hPrev, CNamedItem *pItem)
{
	CString   Root  = GetRoot(pItem->m_Name, FALSE);

	HTREEITEM hRoot = m_MapNames[Root];

	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	if( hPrev ) {

		if( m_pTree->GetParent(hPrev) != hRoot ) {

			hPrev = TVI_LAST;
			}
		}
	else
		hPrev = TVI_FIRST;

	m_pTree->Expand(hRoot, TVE_EXPAND);

	hItem = m_pTree->InsertItem(hRoot, hPrev, Node);

	AddToMaps(pItem, hItem);
	}

void CNavTreeWnd::PasteIntoTree(HTREEITEM hPrev, CNamedItem *pItem)
{
	HTREEITEM hItem;

	PasteIntoTree(hItem, hPrev, pItem);
	}

// Name Parsing

CString CNavTreeWnd::GetName(CString Path)
{
	UINT uPos = Path.FindRev('.');

	return Path.Mid(uPos + 1);
	}

CString CNavTreeWnd::GetRoot(CString Path, BOOL fDot)
{
	UINT    uPos = Path.FindRev('.');

	CString Root = Path.Left(uPos + 1);

	if( !fDot ) {

		Root = Root.Left(Root.GetLength() - 1);
		}
	
	return Root;
	}

CString CNavTreeWnd::GetRoot(CItem *pItem, BOOL fDot)
{
	if( pItem->IsKindOf(m_Folder) ) {

		CNamedItem *pNamed = (CNamedItem *) pItem;

		if( fDot ) {

			return pNamed->m_Name + L'.';
			}

		return pNamed->m_Name;
		}

	if( pItem->IsKindOf(m_Class) ) {

		CNamedItem *pNamed = (CNamedItem *) pItem;

		return GetRoot(pNamed->m_Name, fDot);
		}

	return L"";
	}

CString CNavTreeWnd::GetRoot(HTREEITEM hItem, BOOL fDot)
{
	return GetRoot((CItem *) m_pTree->GetItemParam(hItem), fDot);
	}

CString CNavTreeWnd::GetRoot(BOOL fDot)
{
	return GetRoot(m_pSelect, fDot);
	}

// Implementation

void CNavTreeWnd::LoadRoot(void)
{
	CTreeViewItem Root;
	
	LoadRootItem(Root);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Root);

	m_MapNames.Insert(L"", m_hRoot);

	m_MapFixed.Insert(m_pItem->GetFixedName(), m_hRoot);
	}

void CNavTreeWnd::LoadTree(void)
{
	UINT uTotal = m_pList->GetItemCount();

	UINT uCount = 0;

	for( UINT uPass = 0; uCount < uTotal; uPass++ ) {

		INDEX Index = m_pList->GetHead();

		while( !m_pList->Failed(Index) ) {

			CNamedItem *pItem = m_pList->GetItem(Index);
			
			if( pItem->m_Name.Count('.') == uPass ) {

				UINT uPos = 0;

				if( uPass ) {

					for( UINT c = 0; pItem->m_Name[uPos]; uPos++ ) {

						if( pItem->m_Name[uPos] == L'.' ) {

							if( ++c == uPass ) {

								break;
								}
							}
						}
					}

				CString Path = pItem->m_Name.Left(uPos+0);

				CString Name = pItem->m_Name.Mid (uPos+1);

				CTreeViewItem Node;

				LoadNodeItem(Node, pItem);

				HTREEITEM hRoot = m_MapNames[Path];

				HTREEITEM hItem = m_pTree->InsertItem(hRoot, NULL, Node);

				AddToMaps(pItem, hItem);

				uCount++;
				}

			m_pList->GetNext(Index);
			}
		}

	m_pTree->SelectItem(m_hRoot, TVGN_CARET);

	m_pTree->Expand(m_hRoot, TVE_EXPAND);

	m_hSelect = m_hRoot;

	m_pSelect = m_pItem;
	}

UINT CNavTreeWnd::FindStep(HTREEITEM hOld, HTREEITEM hNew)
{
	while( hOld ) {

		if( hOld == hNew ) {

			return TVGN_NEXT;
			}

		hOld = m_pTree->GetNext(hOld);
		}

	return TVGN_PREVIOUS;
	}

void CNavTreeWnd::LoadRootItem(CTreeViewItem &Root)
{
	Root.SetText (m_pItem->GetHumanName());

	Root.SetParam(LPARAM(m_pItem));

	UINT uImage = 0;

	Root.SetImages(uImage);
	}

void CNavTreeWnd::LoadNodeItem(CTreeViewItem &Node, CNamedItem *pItem)
{
	Node.SetText (GetName(pItem->m_Name));

	Node.SetParam(LPARAM(pItem));

	UINT uImage = pItem->IsKindOf(m_Folder) ? 2 : 4;

	Node.SetImages(uImage);
	}

void CNavTreeWnd::AddToMaps(CNamedItem *pItem, HTREEITEM hItem)
{
	m_MapNames.Insert(pItem->m_Name, hItem);

	m_MapFixed.Insert(pItem->GetFixedName(), hItem);
	}

void CNavTreeWnd::RemoveFromMaps(CNamedItem *pItem)
{
	m_MapNames.Remove(pItem->m_Name);

	m_MapFixed.Remove(pItem->GetFixedName());
	}

BOOL CNavTreeWnd::IsFolder(void)
{
	return m_pTree->GetChild(m_hSelect) ? TRUE : FALSE;
	}

BOOL CNavTreeWnd::WarnMultiple(UINT uVerb)
{
	m_pTree->Expand(m_hSelect, TVE_EXPAND);

	CString Text = CFormat(IDS_DO_YOU_WANT, CString(uVerb));

	return NoYes(Text) == IDYES;
	}

BOOL CNavTreeWnd::MarkMulti(CString const &Item, CString const &Menu, BOOL fMark)
{
	if( fMark) {

		CCmd *pCmd = New CCmdMulti(Item, Menu);

		SaveCmd(pCmd);

		return TRUE;
		}

	return FALSE;
	}

void CNavTreeWnd::MakeUnique(CString &Name)
{
	if( m_MapNames[Name] ) {

		UINT c = Name.GetLength();

		UINT n = 0;

		while( isdigit(Name[c - 1 - n]) ) {

			n++;
			}

		UINT    v = wcstol(PCTXT(Name) + c - n, NULL, 10);

		CString r = Name.Left(c - n);

		for(;;) { 

			Name = r + CPrintf( L"%*.*u",
					    n,
					    n, 
					    ++v
					    );
			
			if( !m_MapNames[Name] ) {

				break;
				}
			}
		}
	}

void CNavTreeWnd::SaveCmd(CCmd *pCmd)
{
	CLASS       Class  = AfxRuntimeClass(CSystemWnd);

	CSystemWnd &Parent = (CSystemWnd &) GetParent(Class);

	Parent.SaveCmd(this, pCmd);
	}

void CNavTreeWnd::SetViewedItem(void)
{
	CLASS       Class  = AfxRuntimeClass(CSystemWnd);

	CSystemWnd &Parent = (CSystemWnd &) GetParent(Class);

	Parent.SetViewedItem(m_pSelect);
	}

void CNavTreeWnd::SetNavCheckpoint(void)
{
	CLASS       Class  = AfxRuntimeClass(CSystemWnd);

	CSystemWnd &Parent = (CSystemWnd &) GetParent(Class);

	Parent.SetNavCheckpoint();
	}

void CNavTreeWnd::LockUpdate(BOOL fLock)
{
	if( fLock && !m_fLock ) {

		m_pTree->SetRedraw(FALSE);
		}

	if( !fLock && m_fLock ) {

		m_pTree->SetRedraw(TRUE);

		m_pTree->Invalidate(FALSE);
		}

	m_fLock = fLock;
	}

//////////////////////////////////////////////////////////////////////////
//
// Create Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdCreate, CCmd);

// Constructor

CNavTreeWnd::CCmdCreate::CCmdCreate(CItem *pPrev, UINT uName, CLASS Class)
{
	m_Menu  = IDS_CREATE;

	m_Prev  = pPrev->GetFixedName();

	m_Name  = uName;

	m_Class = Class;

	m_Fixed = CMetaItem::AllocFixed();
	}

//////////////////////////////////////////////////////////////////////////
//
// Rename Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdRename, CCmd);

// Constructor

CNavTreeWnd::CCmdRename::CCmdRename(CNamedItem *pItem, CString Name)
{
	m_Menu = CFormat(IDS_RENAME, pItem->m_Name);

	m_Item = pItem->GetFixedPath();

	m_Prev = pItem->m_Name;

	m_Name = Name;
	}

//////////////////////////////////////////////////////////////////////////
//
// Delete Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdDelete, CCmd);

// Constructor

CNavTreeWnd::CCmdDelete::CCmdDelete(CNamedItem *pItem, CItem *pRoot, CItem *pPrev)
{
	m_Menu  = CFormat(IDS_DELETE_3, pItem->m_Name);

	m_Item  = pItem->GetFixedPath();

	m_Root  = pRoot ? pRoot->GetFixedName() : L"";

	m_Prev  = pPrev ? pPrev->GetFixedName() : L"";

	m_hData = pItem->SaveToMemory();
	}

// Destructor

CNavTreeWnd::CCmdDelete::~CCmdDelete(void)
{
	GlobalFree(m_hData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Paste Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdPaste, CCmd);

// Constructor

CNavTreeWnd::CCmdPaste::CCmdPaste(CItem *pItem, CItem *pRoot, CItem *pPrev, BOOL fMove, HGLOBAL hData)
{
	m_Menu  = IDS_PASTE;

	m_Item  = pItem->GetFixedPath();

	m_Root  = pRoot ? pRoot->GetFixedName() : L"";

	m_Prev  = pPrev ? pPrev->GetFixedName() : L"";

	m_fMove = fMove;

	m_hData = GlobalAlloc(GHND, GlobalSize(hData));

	PVOID pFrom = GlobalLock(hData);

	PVOID pDest = GlobalLock(m_hData);

	memcpy(pDest, pFrom, GlobalSize(hData));

	GlobalUnlock(hData);

	GlobalUnlock(m_hData);
	}

// Destructor

CNavTreeWnd::CCmdPaste::~CCmdPaste(void)
{
	GlobalFree(m_hData);
	}









// Drag Support

void CNavTreeWnd::StartDrag(void)
{
	if( m_pNamed ) {

		CRect Rect = m_pTree->GetItemRect(m_hSelect, TRUE);

		m_pTree->ClientToScreen(Rect);

		Rect.left    = Rect.left - 16;

		m_DragPos    = GetCursorPos();

		m_DragSize   = Rect.GetSize();

		m_DragOffset = m_DragPos - Rect.GetTopLeft();

		MakeMin(m_DragOffset.cx, Rect.cx());

		MakeMin(m_DragOffset.cy, Rect.cy());

		MakeMax(m_DragOffset.cx, 0);

		MakeMax(m_DragOffset.cy, 0);

		m_fDrag     = TRUE;

		m_fCopy     = IsDragCopy();

		m_hDrag     = m_hSelect;

		m_hDropRoot = m_pTree->GetParent(m_hSelect);

		m_hDropPrev = m_hSelect;

		SetFocus();

		SetCapture();

		ShowCursor(FALSE);

		SetCursor(LoadCursor(NULL, IDC_NO));

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, 0);

		m_pTree->UpdateWindow();

		ShowDrop(TRUE);

		MakeDragImage();

		DrawDragImage();
		}
	}

void CNavTreeWnd::TrackDrag(void)
{
	if( m_fDrag ) {

		HCURSOR hCursor = LoadCursor(NULL, IDC_NO);

		DrawDragImage();

		m_DragPos  = GetCursorPos();

		CPoint Pos = m_DragPos - m_DragOffset + CSize(0, m_DragSize.cy / 2);

		m_pTree->ScreenToClient(Pos);

		if( m_pTree->GetClientRect().PtInRect(Pos) ) {

			HTREEITEM hItem = m_pTree->HitTestItem(Pos);

			HTREEITEM hRoot = NULL;

			HTREEITEM hPrev = NULL;

			if( !hRoot && hItem == m_hRoot ) {

				hRoot = m_hRoot;

				hPrev = NULL;
				}

			if( !hRoot && hItem ) {

				CItem *pItem = (CItem *) m_pTree->GetItemParam(hItem);

				if( pItem->IsKindOf(m_Folder) ) {

					if( !m_pTree->GetChild(hItem) || !m_pTree->IsExpanded(hItem) ) {
							
						CRect Rect = m_pTree->GetItemRect(hItem, TRUE);

						if( Pos.y > Rect.top + 8 ) {

							hRoot = hItem;

							hPrev = NULL;
							}
						}
					}
				}

			if( !hRoot ) {

				HTREEITEM hScan, hBack;

				if( hItem ) {
				
					hScan = m_pTree->GetPreviousVisible(hItem);

					hBack = m_pTree->GetParent(hItem);
					}
				else {
					hScan = hItem;

					hBack = NULL;

					WalkToLast(hScan);
					}

				if( hScan != hBack ) {

					while( !m_pTree->GetNext(hScan) ) {

						HTREEITEM hNext = m_pTree->GetParent(hScan);

						CRect     Rect  = m_pTree->GetItemRect(hScan, TRUE);
					
						if( hNext == m_hRoot || Pos.x >= Rect.left - 24 ) {

							hPrev = hScan;

							hRoot = hNext;

							break;
							}

						hScan = hNext;
						}
					}
				}

			if( !hRoot && hItem ) {

				hRoot = m_pTree->GetParent  (hItem);
	
				hPrev = m_pTree->GetPrevious(hItem);
				}

			if( m_hDropRoot != hRoot || m_hDropPrev != hPrev ) {

				ShowDrop(FALSE);

				m_hDropRoot = hRoot;

				m_hDropPrev = hPrev;

				if( IsExpandDrop() ) {

					SetTimer (m_timerExpand, 500);
					}
				else
					KillTimer(m_timerExpand);

				ShowDrop(TRUE);

				DragDebug();
				}

			hCursor = LoadCursor(NULL, IDC_ARROW);
			}

		SetCursor(hCursor);

		DrawDragImage();

		SetTimer(m_timerScroll, 10);
		}
	}

void CNavTreeWnd::DragDebug(void)
{
	CNamedItem *pRoot = m_hDropRoot ? (CNamedItem *) m_pTree->GetItemParam(m_hDropRoot) : NULL;

	CNamedItem *pPrev = m_hDropPrev ? (CNamedItem *) m_pTree->GetItemParam(m_hDropPrev) : NULL;

	CString Root = pRoot ? (pRoot->IsKindOf(AfxRuntimeClass(CNamedItem)) ? pRoot->m_Name : L"Top") : L"Null";

	CString Prev = pPrev ? (pPrev->IsKindOf(AfxRuntimeClass(CNamedItem)) ? pPrev->m_Name : L"Top") : L"Null";

	CFormat Text(L"Root=%1 Prev=%2", Root, Prev);

	afxMainWnd->SendMessage(WM_SETSTATUSBAR, WPARAM(PCTXT(Text)));
	}

void CNavTreeWnd::EndDrag(BOOL fAbort)
{
	if( m_fDrag ) {

		DrawDragImage();

		ShowDrop(FALSE);

		m_DragImage.Detach(TRUE);
		
		ShowCursor(TRUE);

		ReleaseCapture();

		m_fDrag = FALSE;

		if( !fAbort && IsValidDrop() ) {

			CTextStreamMemory Stream;

			if( Stream.OpenSave() ) {

				CTreeFile Tree;

				if( Tree.OpenSave(Stream) ) {

					CString Item = m_pSelect->GetFixedPath();

					CString Verb = m_fCopy ? IDS_COPY : IDS_MOVE;

					CString Menu = CFormat(IDS_FORMAT, Verb, m_pNamed->m_Name);

					MarkMulti(Item, Menu, TRUE);

					OnEditCopy(Tree, m_hSelect);

					Tree.Close();

					HGLOBAL hData = Stream.TakeOver();

					if( !m_fCopy ) {

						OnItemDelete(IDS_MOVE, FALSE, FALSE);
						}

					OnEditPaste( m_pSelect,
						     m_hDropRoot,
						     m_hDropPrev,
						     TRUE,
						     hData
						     );

					MarkMulti(Item, Menu, TRUE);
					}
				}
			}

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, TVS_TRACKSELECT);

		m_pTree->SetFocus();
		}
	}

void CNavTreeWnd::MakeDragImage(void)
{
	CRect Rect = m_DragSize;

	m_DragImage.Create(CClientDC(NULL), m_DragSize);

	CMemoryDC DC;

	DC.Select(m_DragImage);

	DC.FillRect(Rect, afxBrush(WHITE));

	DC.SetTextColor(afxColor(BLACK));

	DC.Select(afxFont(Dialog));

	DC.SetBkMode(TRANSPARENT);

	CString Text   = m_pTree->GetItemText (m_hSelect);

	UINT    uImage = m_pTree->GetItemImage(m_hSelect);

	CSize   Size   = DC.GetTextExtent(Text);

	CPoint  Origin = CPoint(16, 0);

	CSize   Adjust = Rect.GetSize() - Size - CSize(16, 0);

	DC.TextOut(Origin + Adjust / 2, Text);

	m_Images.Draw(uImage, DC, 0, 0, ILD_NORMAL);

	DC.PatBlt(Rect, DSTINVERT);

	DC.Deselect();
	
	DC.Deselect();
	}

void CNavTreeWnd::DrawDragImage(void)
{
	CClientDC DC(NULL);

	CPoint Pos = m_DragPos - m_DragOffset;

	CPoint Org = CPoint(0, 0);

	DC.SetBkColor(afxColor(MAGENTA));

	DC.BitBlt(Pos, m_DragSize,  m_DragImage, Org, SRCINVERT);
	}

BOOL CNavTreeWnd::UpdateDragImage(void)
{
	BOOL fCopy = IsDragCopy();

	if( m_fCopy != fCopy ) {

		DrawDragImage();

		ShowDrop(FALSE);

		m_DragImage.Detach(TRUE);

		m_fCopy = fCopy;

		MakeDragImage();

		ShowDrop(TRUE);

		DrawDragImage();

		return TRUE;
		}

	return FALSE;
	}

void CNavTreeWnd::ShowDrop(BOOL fShow)
{
	if( m_hDropRoot ) {

		CRect Horz, Vert;

		HTREEITEM hItem;

		if( !m_hDropPrev ) {

			if( IsFolderDrop() ) {

				CTreeViewItem Node(m_hDropRoot);

				Node.SetStateMask(TVIS_DROPHILITED);

				Node.SetState(fShow ? TVIS_DROPHILITED : 0);

				m_pTree->SetItem(Node);

				m_pTree->UpdateWindow();

				return;
				}

			hItem = m_pTree->GetChild(m_hDropRoot);
			}
		else
			hItem = m_pTree->GetNext(m_hDropPrev);

		if( !hItem ) {

			HTREEITEM hDraw = m_hDropPrev;

			CRect     Root  = m_pTree->GetItemRect(hDraw, TRUE);

			WalkToLast(hDraw);

			Horz = m_pTree->GetItemRect(hDraw, TRUE);

			Horz.top    = Horz.bottom -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Root.left   - 30;

			Horz.right  = Root.right  +  8;
			}
		else {
			Horz = m_pTree->GetItemRect(hItem, TRUE);
		
			Horz.top    = Horz.top    -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Horz.left   - 30;

			Horz.right  = Horz.right  +  8;
			}

		if( m_hDropPrev ) {
			
			if( !hItem || hItem != m_pTree->GetNextVisible(m_hDropPrev) ) {

				if( ShowDropVert(m_hDropPrev) ) {

					Vert.left   = Horz.left   -  0;

					Vert.right  = Vert.left   +  2;

					Vert.bottom = Horz.top    -  0;

					Vert.top    = Vert.bottom - 16;
					}
				}
			}

		CClientDC DC(*m_pTree);

		CBrush Brush(afxColor(WHITE), afxColor(BLACK), 128);

		DC.Select(IsValidDrop() ? afxBrush(WHITE) : Brush);
		
		DC.PatBlt(Horz, PATINVERT);
		
		DC.PatBlt(Vert, PATINVERT);

		DC.Deselect();
		}
	}

BOOL CNavTreeWnd::IsDragCopy(void)
{
	return (GetKeyState(VK_CONTROL) & 0x8000) ? TRUE : FALSE;
	}

BOOL CNavTreeWnd::IsValidDrop(void)
{
	if( !IsExpandDrop() ) {

		if( !m_fCopy ) {

			if( m_pSelect->IsKindOf(m_Folder) ) {

				HTREEITEM hScan = m_hDropRoot;

				while( hScan ) {

					if( hScan == m_hSelect ) {

						return FALSE;
						}

					hScan = m_pTree->GetParent(hScan);
					}
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::IsExpandDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
				}

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::IsFolderDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::ShowDropVert(HTREEITEM hItem)
{
	if( m_pTree->GetChild(hItem) ) {
		
		if( !m_pTree->GetItemState(hItem, TVIS_EXPANDED) ) {

			return FALSE;
			}
		}
	
	return TRUE;
	}

void CNavTreeWnd::WalkToLast(HTREEITEM &hItem)
{
	HTREEITEM hNext;

	while( (hNext = m_pTree->GetNext (hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;
		}

	while( (hNext = m_pTree->GetChild(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;

		WalkToLast(hItem);

		break;
		}
	}

// End of File
