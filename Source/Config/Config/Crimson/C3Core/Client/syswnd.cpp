
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2001 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// System View Window
//

// Runtime Class

AfxImplementRuntimeClass(CSystemWnd, CTripleViewWnd);

// Constructor

CSystemWnd::CSystemWnd(void)
{
	m_KeyName = L"SystemView";

	m_fUsed   = FALSE;

	m_fBusy   = FALSE;

	m_Accel.Create(L"SystemAccel");
	}

// Destructor

CSystemWnd::~CSystemWnd(void)
{
	FreeList(m_StackUndo);

	FreeList(m_StackRedo);
	}

// Operations

void CSystemWnd::SetNavCheckpoint(void)
{
	if( !m_fUsed ) {

		if( !m_fBusy ) {

			m_StackFore.Empty();
			}

		m_fUsed = TRUE;
		}
	}

void CSystemWnd::SetViewedItem(CItem *pItem)
{
	if( !m_pViewer->IsItem(pItem) ) {

		CString Nav = pItem->GetFixedPath();

		if( m_Nav != Nav ) {

			if( !m_fBusy && m_fUsed ) {

				if( StoreHistory(m_StackBack, m_Nav) ) {

					m_fUsed = FALSE;
					}
				}

			m_Nav = Nav;
			}

		if( !m_pViewer->IsCurrent() ) {

			m_pViewer->Attach(pItem);
			}
		else {
			AfxNull(CWnd).SetFocus();

			m_pViewer->Attach(pItem);

			m_pViewer->SetFocus();
			}
		}
	}

void CSystemWnd::SaveCmd(CViewWnd *pView, CCmd *pCmd)
{
	pCmd->m_View = FindView(pView);

	pCmd->m_Side = FindView(&GetFocus());

	m_StackUndo.Append(pCmd);

	pView->ExecCmd(pCmd);

	FreeList(m_StackRedo);

	m_fUsed = TRUE;
	}

// Overribables

void CSystemWnd::OnAttach(void)
{
	m_pDbase  = (CDatabase *) m_pItem;

	m_pViewer = New CItemViewWnd(viewItem, viewEdge);

	m_pWnd[0] = New CNavPaneWnd;
	
	m_pWnd[1] = NULL;

	m_pWnd[2] = m_pViewer;

	m_pWnd[0]->Attach(m_pItem);
	}

BOOL CSystemWnd::OnNavigate(CString Nav)
{
	if( m_pWnd[0]->Navigate(Nav) ) {

		return m_pViewer->Navigate(Nav);
		}

	return FALSE;
	}

// Message Map

AfxMessageMap(CSystemWnd, CTripleViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_FOCUSNOTIFY)

	AfxDispatchControlType(IDM_GO, OnGoControl)
	AfxDispatchCommandType(IDM_GO, OnGoCommand)

	AfxDispatchControl(IDM_EDIT_UNDO, OnEditUndoControl)
	AfxDispatchCommand(IDM_EDIT_UNDO, OnEditUndoCommand)
	AfxDispatchControl(IDM_EDIT_REDO, OnEditRedoControl)
	AfxDispatchCommand(IDM_EDIT_REDO, OnEditRedoCommand)

	AfxMessageEnd(CSystemWnd)
	};

// Accelerator

BOOL CSystemWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CSystemWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		m_pWnd[0]->SendMessage( m_MsgCtx.Msg.message,
					m_MsgCtx.Msg.wParam,
					m_MsgCtx.Msg.lParam
					);
		}
	}

void CSystemWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID == m_pViewer->GetID() ) {

		SetNavCheckpoint();
		}

	CTripleViewWnd::OnFocusNotify(uID, Wnd);
	}

// Go Commands

BOOL CSystemWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_GO_BACK:

			Src.EnableItem(!m_StackBack.IsEmpty());

			return TRUE;

		case IDM_GO_FORWARD:

			Src.EnableItem(!m_StackFore.IsEmpty());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSystemWnd::OnGoCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GO_BACK:

			OnGoBack();

			return TRUE;

		case IDM_GO_FORWARD:

			OnGoForward();

			return TRUE;
		}

	return FALSE;
	}

void CSystemWnd::OnGoBack(void)
{
	StepBack();
	}

void CSystemWnd::OnGoForward(void)
{
	StepForward();
	}

BOOL CSystemWnd::StepBack(void)
{
	return StepHistory(m_StackBack, m_StackFore);
	}

BOOL CSystemWnd::StepForward(void)
{
	return StepHistory(m_StackFore, m_StackBack);
	}

// Edit Commands

BOOL CSystemWnd::OnEditUndoControl(UINT uID, CCmdSource &Src)
{
	Enable(Src, m_StackUndo, IDS_UNDO, L"Z");
	
	return TRUE;
	}

BOOL CSystemWnd::OnEditRedoControl(UINT uID, CCmdSource &Src)
{
	Enable(Src, m_StackRedo, IDS_REDO, L"A");
	
	return TRUE;
	}

BOOL CSystemWnd::OnEditUndoCommand(UINT uID)
{
	Action(m_StackUndo, m_StackRedo, FALSE);

	return TRUE;
	}

BOOL CSystemWnd::OnEditRedoCommand(UINT uID)
{
	Action(m_StackRedo, m_StackUndo, TRUE);

	return TRUE;
	}

void CSystemWnd::Enable(CCmdSource &Src, CCmdList &List, UINT uVerb, CString Key)
{
	CString Verb  = uVerb;

	CString Text  = CFormat(IDS_CANT, Verb);

	INDEX   Index = List.GetTail();

	if( !List.Failed(Index) ) {

		CCmd *pCmd = List[Index];

		Text = CFormat(IDS_FORMAT, Verb, pCmd->m_Menu);
		}

	Src.SetItemText(CFormat(IDS_TCTRL, Text, Key));

	Src.EnableItem(!List.Failed(Index));
	}

BOOL CSystemWnd::Action(CCmdList &List1, CCmdList &List2, BOOL fRedo)
{
	BOOL  fMul = FALSE;

	INDEX Tail = List1.GetTail();

	while( !List1.Failed(Tail) ) {

		CCmd *pCmd = List1[Tail];

		BOOL fMark = pCmd->IsKindOf(AfxRuntimeClass(CCmdMulti));

		if( !fMark || !fMul ) {

			CStringList &List = fRedo ? m_StackBack : m_StackFore;

			StoreHistory(List, m_Nav);

			NavigateBusy(pCmd->m_Item);
			}

		Action(pCmd, fRedo);

		List1.Remove(Tail);

		List2.Append(pCmd);

		if( fMark ) {

			fMul = !fMul;
			}

		if( !fMul ) {

			break;
			}

		Tail = List1.GetTail();
		}

	return TRUE;
	}

void CSystemWnd::Action(CCmd *pCmd, BOOL fRedo)
{
	UINT uView = pCmd->m_View - IDVIEW;

	UINT uSide = pCmd->m_Side - IDVIEW;

	CViewWnd *pView = m_pWnd[uView];

	CViewWnd *pSide = m_pWnd[uSide];

	AfxNull(CWnd).SetFocus();

	Action(pView, pCmd, fRedo);

	if( uSide < 2 ) {
		
		if( m_uShow[uSide] == showHidden ) {

			m_uShow[uSide] = showSlide;

			UpdateLayout(uSide);
			}
		}

	pSide->SetFocus();
	}

void CSystemWnd::Action(CViewWnd *pView, CCmd *pCmd, BOOL fRedo)
{
	m_fBusy = TRUE;

	if( fRedo ) {
		
		pView->ExecCmd(pCmd);
		}
	else
		pView->UndoCmd(pCmd);

	m_fUsed = TRUE;

	m_fBusy = FALSE;
	}

// Implementation

UINT CSystemWnd::FindView(CWnd *pWnd)
{
	while( pWnd->IsWindow() ) {

		if( pWnd->GetParent().GetHandle() == m_hWnd ) {

			return pWnd->GetID();
			}

		pWnd = &pWnd->GetParent();
		}

	return IDVIEW + 2;
	}

BOOL CSystemWnd::StepHistory(CStringList &List1, CStringList &List2)
{
	CString Init = m_Nav;

	for(;;) { 

		INDEX Index = List1.GetTail();

		if( !List1.Failed(Index) ) {

			CString Prev = m_Nav;

			CString Step = List1[Index];
			
			List1.Remove(Index);

			StoreHistory(List2, Prev);

			if( Step != Prev ) {

				if( NavigateBusy(Step) ) {

					return TRUE;
					}
				}

			continue;
			}
	
		return FALSE;
		}
	}

BOOL CSystemWnd::StoreHistory(CStringList &List, CString Nav)
{
	INDEX Index = List.GetTail();

	while( !List.Failed(Index) ) {

		if( List[Index] == Nav ) {

			if( Index == List.GetTail() ) {

				return FALSE;
				}

			List.Remove(Index);

			break;
			}

		List.GetPrev(Index);
		}

	List.Append(Nav);

	return TRUE;
	}

BOOL CSystemWnd::NavigateBusy(CString Nav)
{
	BOOL fOkay;

	m_fBusy = TRUE;

	fOkay   = Navigate(Nav);

	m_Nav   = Nav;

	m_fBusy = FALSE;

	return fOkay;
	}

void CSystemWnd::FreeList(CCmdList &List)
{
	INDEX i;

	while( !List.Failed(i = List.GetHead()) ) {

		delete List[i];

		List.Remove(i);
		}
	}

// End of File
