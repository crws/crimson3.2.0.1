
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2001 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Pane Window
//

// Runtime Class

AfxImplementRuntimeClass(CPaneWnd, CViewWnd);

// Constructor

CPaneWnd::CPaneWnd(void)
{
	m_pView  = NULL;

	m_pTB1   = New CToolbarWnd(barTight | barDark);

	m_pTB2   = New CToolbarWnd(barTight);

	m_pTB3   = New CToolbarWnd(barTight | barDouble);

	m_fShow3 = FALSE;

	m_fValid = FALSE;
	}

// Routing Control

BOOL CPaneWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pTB1 && m_pTB1->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	if( m_pTB2 && m_pTB2->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	if( m_pTB3 && m_pTB3->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	if( m_pView && m_pView->RouteMessage(Message, lResult) ) {

		return TRUE;
		}
	
	return CViewWnd::OnRouteMessage(Message, lResult);
	}

// Interface Control

void CPaneWnd::OnUpdateInterface(void)
{
	LRESULT lResult = 0;

	CMenu Tool1, Tool2, Tool3;

	Tool1.CreateMenu();

	Tool2.CreateMenu();

	Tool3.CreateMenu();
	
	////////

	Tool2.AppendMenu(m_ToolMenu);

	////////

	MSG MsgTool1 = { NULL, WM_LOADTOOL, 1, LPARAM(&Tool1) };

	MSG MsgTool2 = { NULL, WM_LOADTOOL, 2, LPARAM(&Tool2) };

	MSG MsgTool3 = { NULL, WM_LOADTOOL, 3, LPARAM(&Tool3) };

	RouteMessage(MsgTool1, lResult);

	RouteMessage(MsgTool2, lResult);

	RouteMessage(MsgTool3, lResult);

	////////

	m_pTB1->AddGadget(New CTextGadget  (IDOK, 0, TRUE));

	m_pTB1->AddGadget(New CToggleGadget(m_ShowID, 0x1000000F, L""));

	////////

	m_pTB1->AddFromMenu(Tool1, 0);

	m_pTB2->AddFromMenu(Tool2, 0);

	m_pTB3->AddFromMenu(Tool3, 0);
	}
		
// Message Map

AfxMessageMap(CPaneWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_GOINGIDLE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_UPDATEUI)
	AfxDispatchMessage(WM_KEYDOWN)

	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)

	AfxMessageEnd(CPaneWnd)
	};

// Accelerator

BOOL CPaneWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CPaneWnd::OnPostCreate(void)
{
	OnUpdateInterface();

	if( !m_pView ) {

		m_pView = New CDummyViewWnd;
		}

	m_pView->Create( WS_CHILD | WS_CLIPCHILDREN,
			 GetClientRect(),
			 ThisObject, IDVIEW, NULL
			 );

	m_pView->ShowWindow(SW_SHOW);

	UpdateToolbars();

	m_fValid = TRUE;
	}

void CPaneWnd::OnSize(UINT uCode, CSize Size)
{
	if( m_fValid ) {

		CalcPositions();

		if( m_pView ) {

			CRect View = GetClientRect();

			m_pView->MoveWindow(View, TRUE);
			}
		
		if( m_fShow3 ) {

			m_pTB3->MoveWindow(m_TBRect3, TRUE);

			m_pTB3->ShowWindow(SW_SHOW);
			}
		else
			m_pTB3->ShowWindow(SW_HIDE);

		m_pTB1->MoveWindow(m_TBRect1, TRUE);

		m_pTB2->MoveWindow(m_TBRect2, TRUE);
		}
	}

BOOL CPaneWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}
	
void CPaneWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = CWnd::GetClientRect();

	CRect Work = Rect - 1;

	DC.FrameRect(Rect, afxBrush(3dShadow));

	DC.FillRect (Work, afxBrush(WHITE));
	}

void CPaneWnd::OnGoingIdle(void)
{
	m_pTB1->PollGadgets();

	m_pTB2->PollGadgets();

	m_pTB3->PollGadgets();
	}

void CPaneWnd::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
	}

void CPaneWnd::OnSetCurrent(BOOL fCurrent)
{
	m_pView->SetCurrent(IsCurrent());
	}

void CPaneWnd::OnUpdateUI(void)
{
	try {
		ClearToolbars();

		OnUpdateInterface();
		}
			
	catch(CException &) {
		
		UpdateToolbars();
		
		throw;
		}

	UpdateToolbars();
	}

void CPaneWnd::OnKeyDown(UINT uCode, DWORD dwData)
{
	if( uCode == VK_ESCAPE ) {

		afxMainWnd->SendMessage(WM_CANCELMODE);
		}
	}

// Command Handlers

BOOL CPaneWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	return FALSE;
	}

BOOL CPaneWnd::OnViewCommand(UINT uID)
{
	return FALSE;
	}

// Overridable

CRect CPaneWnd::GetClientRect(void)
{
	CRect Rect = CWnd::GetClientRect() - 1;
		   
	Rect.top   = m_TBRect2.bottom;

	if( m_fShow3 ) {

		Rect.bottom = m_TBRect3.top;
		}

	return Rect;
	}

// Implementation

void CPaneWnd::CalcPositions(void)
{
	CRect Rect = CWnd::GetClientRect() - 1;

	m_TBRect1  = Rect;

	m_TBRect2  = Rect;

	m_TBRect3  = Rect;

	m_TBRect1.bottom = m_TBRect1.top    + m_pTB1->GetHeight();

	m_TBRect2.top    = m_TBRect1.bottom;

	m_TBRect2.bottom = m_TBRect2.top    + m_pTB2->GetHeight();

	m_TBRect3.top    = m_TBRect3.bottom - m_pTB3->GetHeight();
	}

void CPaneWnd::ClearToolbars(void)
{
	m_pTB1->DestroyWindow(TRUE);

	m_pTB2->DestroyWindow(TRUE);

	m_pTB3->DestroyWindow(TRUE);
			
	m_pTB1 = New CToolbarWnd(barTight | barDark);

	m_pTB2 = New CToolbarWnd(barTight);

	m_pTB3 = New CToolbarWnd(barTight | barDouble);
	}

void CPaneWnd::UpdateToolbars(void)
{
	CalcPositions();
		
	m_pTB1->Create(m_TBRect1, ThisObject);

	m_pTB2->Create(m_TBRect2, ThisObject);

	m_pTB3->Create(m_TBRect3, ThisObject);

	m_pTB1->GetGadget(IDOK)->SetText(m_Title);

	m_pTB1->PollGadgets();
	
	m_pTB2->PollGadgets();
	
	m_pTB3->PollGadgets();
	
	m_pTB1->ShowWindow(SW_SHOW);
	
	m_pTB2->ShowWindow(SW_SHOW);
	
	m_pTB3->ShowWindow(SW_SHOW);
	}

// End of File
