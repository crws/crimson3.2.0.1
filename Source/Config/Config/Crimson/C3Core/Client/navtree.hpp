
//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window
//

class /*DLLAPI*/ CNavTreeWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CNavTreeWnd(CString List, CLASS Folder, CLASS Class);

	protected:
		// Editing Actions
		class CCmdCreate;
		class CCmdRename;
		class CCmdDelete;
		class CCmdPaste;

		// Object Maps
		typedef CMap <CString, HTREEITEM> CNameMap;

		// Static Data
		static UINT m_timerDouble;
		static UINT m_timerExpand;
		static UINT m_timerScroll;

		// Data Members
		CString		  m_List;
		CLASS		  m_Folder;
		CLASS		  m_Class;
		CImageList	  m_Images;
		UINT		  m_cfData;
		CAccelerator      m_Accel;
		CMetaItem	* m_pItem;
		CNamedIndexList * m_pList;
		CTreeView       * m_pTree;
		BOOL		  m_fLock;
		HTREEITEM	  m_hRoot;
		CNameMap	  m_MapNames;
		CNameMap	  m_MapFixed;
		HTREEITEM	  m_hSelect;
		CItem           * m_pSelect;
		CNamedItem      * m_pNamed;
		BOOL		  m_fDblClk;
		BOOL		  m_fDrag;
		BOOL		  m_fCopy;
		HTREEITEM	  m_hDrag;
		HTREEITEM	  m_hDropRoot;
		HTREEITEM	  m_hDropPrev;
		BOOL		  m_fFold;
		CPoint		  m_DragPos;
		CSize		  m_DragOffset;
		CSize		  m_DragSize;
		CBitmap		  m_DragImage;

		// Overridables
		void OnAttach(void);
		BOOL OnNavigate(CString Nav);
		void OnExec(CCmd *pCmd);
		void OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerator
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnPaint(void);
		void OnSetCurrent(BOOL fCurrent);
		void OnSetFocus(CWnd &Wnd);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnKeyDown(UINT uCode, DWORD dwData);
		void OnKeyUp(UINT uCode, DWORD dwData);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Notification Handlers
		void OnCloseButton(UINT uID, CWnd &Wnd);
		BOOL OnTreeSelChanging(UINT uID, NMTREEVIEW &Info);
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);
		void OnTreeReturn(UINT uID, NMHDR &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);
		BOOL OnTreeRClick(UINT uID, NMHDR &Info);
		void OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeExpanding(UINT uID, NMTREEVIEW &Info);

		// Command Handlers
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);

		// Go Menu
		BOOL OnGoNext(void);
		BOOL OnGoPrev(void);

		// Item Menu : Create
		void OnItemCreateFolder(void);
		void OnExecCreate(CCmdCreate *pCmd);
		void OnUndoCreate(CCmdCreate *pCmd);

		// Item Menu : Rename
		BOOL OnItemRename(CString Name);
		void OnExecRename(CCmdRename *pCmd);
		void OnUndoRename(CCmdRename *pCmd);
		BOOL RenameFrom(HTREEITEM hRoot, UINT uStrip, CString const &Name);
		
		// Item Menu : Delete
		void OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop);
		void OnExecDelete(CCmdDelete *pCmd);
		void OnUndoDelete(CCmdDelete *pCmd);

		// Edit Menu : Cut and Copy
		void OnEditCut(void);
		void OnEditCopy(void);
		void OnEditCopy(CTreeFile &Tree, HTREEITEM hItem);

		// Edit Menu : Paste
		BOOL CanEditPaste(void);
		void OnEditPaste(void);
		void OnEditPaste(CItem *pItem, HTREEITEM hPrev, HTREEITEM hRoot, BOOL fMove, HGLOBAL hData);
		void OnExecPaste(CCmdPaste *pCmd);
		void OnUndoPaste(CCmdPaste *pCmd);
		void PasteFixName(CNamedItem * &pBase, UINT &uBase, CNamedItem *pRoot, CNamedItem *pItem);
		void PasteIntoList(HTREEITEM hPrev, CNamedItem *pItem);
		void PasteIntoTree(HTREEITEM &hItem, HTREEITEM hPrev, CNamedItem *pItem);
		void PasteIntoTree(HTREEITEM hPrev, CNamedItem *pItem);

		// Name Parsing
		CString GetName(CString Path);
		CString GetRoot(CString Path, BOOL fDot);
		CString GetRoot(CItem *pItem, BOOL fDot);
		CString GetRoot(HTREEITEM hItem, BOOL fDot);
		CString GetRoot(BOOL fDot);

		// Implementation
		void LoadRoot(void);
		void LoadTree(void);
		UINT FindStep(HTREEITEM hOld, HTREEITEM hNew);
		void LoadRootItem(CTreeViewItem &Root);
		void LoadNodeItem(CTreeViewItem &Node, CNamedItem *pItem);
		void AddToMaps(CNamedItem *pItem, HTREEITEM hItem);
		void RemoveFromMaps(CNamedItem *pItem);
		BOOL IsFolder(void);
		BOOL WarnMultiple(UINT uVerb);
		BOOL MarkMulti(CString const &Item, CString const &Menu, BOOL fMark);
		void MakeUnique(CString &Name);
		void SaveCmd(CCmd *pCmd);
		void SetViewedItem(void);
		void SetNavCheckpoint(void);
		void LockUpdate(BOOL fLock);

		// Drag Support
		void StartDrag(void);
		void TrackDrag(void);
		void DragDebug(void);
		void EndDrag(BOOL fAbort);
		void MakeDragImage(void);
		void DrawDragImage(void);
		BOOL UpdateDragImage(void);
		void ShowDrop(BOOL fShow);
		BOOL IsDragCopy(void);
		BOOL IsValidDrop(void);
		BOOL IsExpandDrop(void);
		BOOL IsFolderDrop(void);
		BOOL ShowDropVert(HTREEITEM hItem);
		void WalkToLast(HTREEITEM &hItem);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Create Command
//

class CNavTreeWnd::CCmdCreate : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdCreate(CItem *pPrev, UINT uName, CLASS Class);

		// Data Members
		CString m_Prev;
		CString m_Name;
		CLASS   m_Class;
		UINT    m_Fixed;
	};

//////////////////////////////////////////////////////////////////////////
//
// Rename Command
//

class CNavTreeWnd::CCmdRename : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdRename(CNamedItem *pItem, CString Name);

		// Data Members
		CString m_Prev;
		CString m_Name;
	};

//////////////////////////////////////////////////////////////////////////
//
// Delete Command
//

class CNavTreeWnd::CCmdDelete : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdDelete(CNamedItem *pItem, CItem *pRoot, CItem *pPrev);

		// Destructor
		~CCmdDelete(void);

		// Data Members
		CString m_Root;
		CString m_Prev;
		HGLOBAL m_hData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Paste Command
//

class CNavTreeWnd::CCmdPaste : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdPaste(CItem *pItem, CItem *pRoot, CItem *pPrev, BOOL fMove, HGLOBAL hData);

		// Destructor
		~CCmdPaste(void);

		// Data Members
		CString      m_Root;
		CString      m_Prev;
		BOOL	     m_fMove;
		HGLOBAL      m_hData;
		CStringArray m_Names;
	};

// End of File
