
//////////////////////////////////////////////////////////////////////////
//
// System View Window
//

class /*DLLAPI*/ CSystemWnd : public CTripleViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CSystemWnd(void);

		// Destructor
		~CSystemWnd(void);

		// Operations
		void SetNavCheckpoint(void);
		void SetViewedItem(CItem *pItem);
		void SaveCmd(CViewWnd *pView, CCmd *pCmd);

	protected:
		// Typedefs
		typedef CList <CCmd * > CCmdList;
		typedef CList <CString> CStringList;

		// Commands
		class CCmdNav;

		// Data Members
		CAccelerator    m_Accel;
		CDatabase     * m_pDbase;
		CItemViewWnd  * m_pViewer;
		CStringList	m_StackBack;
		CStringList	m_StackFore;
		CCmdList	m_StackUndo;
		CCmdList	m_StackRedo;
		CString         m_Nav;
		BOOL	        m_fUsed;
		BOOL	        m_fBusy;

		// Overribables
		void OnAttach(void);
		BOOL OnNavigate(CString Nav);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerator
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnFocusNotify(UINT uID, CWnd &Wnd);

		// Go Commands
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		void OnGoBack(void);
		void OnGoForward(void);
		BOOL StepBack(void);
		BOOL StepForward(void);

		// Edit Commands
		BOOL OnEditUndoControl(UINT uID, CCmdSource &Src);
		BOOL OnEditRedoControl(UINT uID, CCmdSource &Src);
		BOOL OnEditUndoCommand(UINT uID);
		BOOL OnEditRedoCommand(UINT uID);
		void Enable(CCmdSource &Src, CCmdList &List, UINT uVerb, CString Key);
		BOOL Action(CCmdList &List1, CCmdList &List2, BOOL fRedo);
		void Action(CCmd *pCmd, BOOL fRedo);
		void Action(CViewWnd *pView, CCmd *pCmd, BOOL fRedo);

		// Implementation
		UINT FindView(CWnd *pWnd);
		BOOL StepHistory(CStringList &List1, CStringList &List2);
		BOOL StoreHistory(CStringList &List, CString Nav);
		BOOL NavigateBusy(CString Nav);
		void FreeList(CCmdList &List);
	};		

// End of File
