
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DdeCommander_HPP

#define INCLUDE_DdeCommander_HPP

//////////////////////////////////////////////////////////////////////////
//
// DDE Commander Window
//

class CDdeCommander : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDdeCommander(void);

		// Operations
		BOOL Initiate(CString Server, CString Topic);
		BOOL Execute(CString Cmd);
		BOOL Terminate(void);

	protected:
		// Data Members
		CEvent m_Event;
		HWND   m_hServer;
		WPARAM m_wParam;
		LPARAM m_lParam;

		// Message Procedures
		LRESULT WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// Implementation
		BOOL Wait(UINT uTimeout);
		void FreeInitiateAck(void);
	};

// End of File

#endif
