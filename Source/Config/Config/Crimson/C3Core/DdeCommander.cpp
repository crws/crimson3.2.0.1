
#include "intern.hpp"

#include "DdeCommander.hpp"

#include <Dde.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DDE Commander Window
//

// Runtime Class

AfxImplementRuntimeClass(CDdeCommander, CWnd);

// Constructor

CDdeCommander::CDdeCommander(void) : m_Event(FALSE)
{
	}

// Operations

BOOL CDdeCommander::Initiate(CString Server, CString Topic)
{
	CGlobalAtom as(Server);

	CGlobalAtom at(Topic);

	for( UINT n = 0; n < 20; n++ ) {

		::SendMessageTimeout( HWND(HWND_BROADCAST),
				      WM_DDE_INITIATE,
				      WPARAM(m_hWnd),
				      MAKELONG( as.GetAtom(),
				      		at.GetAtom()
				      		),
				      SMTO_BLOCK,
				      10,
				      NULL
				      );

		if( Wait(250) ) {

			FreeInitiateAck();

			m_hServer = HWND(m_wParam);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CDdeCommander::Execute(CString Cmd)
{
	UINT    uGlobal = sizeof(WCHAR) * (Cmd.GetLength() + 1);

	HGLOBAL hGlobal = GlobalAlloc(GHND, uGlobal);

	AfxAssume(hGlobal);

	PBYTE   pGlobal = PBYTE(GlobalLock(hGlobal));

	memcpy(pGlobal, PCTXT(Cmd), uGlobal);

	GlobalUnlock(hGlobal);

	::PostMessage( m_hServer,
		       WM_DDE_EXECUTE,
		       WPARAM(m_hWnd),
		       LPARAM(hGlobal)
		       );

	if( Wait(1000) ) {

		GlobalFree(hGlobal);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDdeCommander::Terminate(void)
{
	::PostMessage( m_hServer,
		       WM_DDE_TERMINATE,
		       WPARAM(m_hWnd),
		       LPARAM(0)
		       );

	return Wait(1000);
	}

// Implementation

BOOL CDdeCommander::Wait(UINT uTimeout)
{
	while( uTimeout ) {

		UINT uTime = GetTickCount();

		UINT uWait = m_Event.WaitForObjectOrMsg(uTimeout);

		if( uWait == waitSignal ) {

			return TRUE;
			}

		if( uWait == waitMessage ) {

			MSG Msg;

			while( PeekMessage(&Msg, m_hWnd, 0, 0, PM_REMOVE) ) {

				DispatchMessage(&Msg);
				}

			UINT uDone = GetTickCount();

			UINT uGone = uDone - uTime;

			if( uGone < uTimeout ) {

				uTimeout -= uGone;

				continue;
				}
			}

		break;
		}

	return FALSE;
	}

void CDdeCommander::FreeInitiateAck(void)
{
	GlobalDeleteAtom(ATOM(LOWORD(m_lParam)));

	GlobalDeleteAtom(ATOM(HIWORD(m_lParam)));
	}

// Message Procedures

LRESULT CDdeCommander::WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_DDE_TERMINATE ) {

		m_Event.Set();
		}

	if( uMessage == WM_DDE_ACK ) {

		m_wParam = wParam;

		m_lParam = lParam;

		m_Event.Set();
		}

	return CWnd::WndProc(uMessage, wParam, lParam);
	}

// End of File
