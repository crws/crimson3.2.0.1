
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3UI_HPP
	
#define	INCLUDE_C3UI_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdialog.hpp>

#include <c3look.hpp>

#include <c3layout.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3ui.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_C3UI

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "c3ui.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//								
// UI Change Command
//

#define	IDM_UI_CHANGE	0x1234
#define	IDM_UI_ABORT    0x4321
#define	IDM_UI_SPECIAL	0x4444

//////////////////////////////////////////////////////////////////////////
//								
// Hot Link Usage
//

#if 0

#define	useHotLinks	TRUE

#define	NewHotLink()	New CHotLinkCtrl

#else

#define	useHotLinks	FALSE

#define	NewHotLink()	New CDropButton(this)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Indeterminate Values
//

#define	multiInteger	UINT(0x7FFFAAAA)

#define	multiObject	((CItem *) multiInteger)

#define	multiString	L"<{[INDEP]}>"

//////////////////////////////////////////////////////////////////////////
//
// Indeterminate Testing
//

inline BOOL IsMulti(UINT uData)
{
	return uData == multiInteger;
	}

inline BOOL IsMulti(CItem *pItem)
{
	return pItem == multiObject;
	}

inline BOOL IsMulti(PCTXT pText)
{
	return !wstrcmp(pText, multiString);
	}

inline BOOL IsMulti(CString const &Text)
{
	return Text == multiString;
	}

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CUIData;
class CUISchema;
class CUIBaseElement;
class CUITextElement;
class CUIElement;
class CUIControl;

//////////////////////////////////////////////////////////////////////////
//								
// Type Definitions
//

typedef CUIBaseElement * PUIBASE;

typedef CUITextElement * PUITEXT;

typedef CUIElement     * PUI;

//////////////////////////////////////////////////////////////////////////
//
// Element Save Results
//

enum
{
	saveSame   = 0,
	saveChange = 1,
	saveError  = 2,
	};

///////////////////////////////////////////////////////////////////////////
//
// Textual UI Element Flags
//

enum
{
	textEdit     = 0x00001,
	textExpand   = 0x00002,
	textEnum     = 0x00004,
	textScroll   = 0x00008,
	textHide     = 0x00010,
	textUnits    = 0x00020,
	textNumber   = 0x00040,
	textRefresh  = 0x00080,
	textDefault  = 0x00100,
	textNoMerge  = 0x00200,
	textReload   = 0x00400,
	textStatus   = 0x00800,
	textRead     = 0x01000,
	textLocking  = 0x02000,
	textDummy    = 0x04000,
	textAllowWAS = 0x08000,
	textSigned   = 0x10000,
	textPlaces   = 0x20000,
	};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Data
//

class DLLAPI CUIData
{
	public:
		// Data Members
		CString m_Tag;
		CString m_Label;
		CString	m_Local;
		CLASS   m_ClassText;
		CLASS   m_ClassUI;
		CString	m_Format;
		CString m_Tip;
		CString m_Status;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Schema
//

class DLLAPI CUISchema : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUISchema(CItem *pItem);

		// Destructor
		~CUISchema(void);

		// Attributes
		BOOL CheckType(CItem *pItem) const;

		// UI Data Access
		CUIData const * GetUIData(PCTXT pTag) const;

		// Operations
		BOOL LoadFromFile(PCTXT pFile);

	protected:
		// Type Definitions
		typedef CArray <CUIData>       CUIDataList;
		typedef CMap   <CString, UINT> CUIDataDict;

		// Data Members
		CLASS       m_Class;
		CUIDataList m_List;
		CUIDataDict m_Dict;

		// Implementation
		void LoadData(CLASS Class);
		BOOL FindClasses(CUIData &Data, CString Name, BOOL fLegacy);
		BOOL FindLegacy(CUIData &Data, CString Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Basic UI Element
//

class DLLAPI CUIBaseElement : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIBaseElement(void);

		// Binding
		BOOL Bind(CItem *pItem, CUIData const *pUIData);

		// Attributes
		CItem	      * GetItem(void) const;
		IDataAccess   * GetDataAccess(void) const;
		CUIData const & GetUIData(void) const;
		CString		GetTag(void) const;
		CString		GetLabel(void) const;
		CString		GetLocal(void) const;
		CString		GetFormat(void) const;
		CString		GetStatus(void) const;

		// Operations
		void RebindUI(CItem *pItem);

	protected:
		// Data Members
		CItem	      * m_pItem;
		IDataAccess   * m_pData;
		CUIData		m_UIData;

		// Overidables
		virtual void OnBind(void);
		virtual void OnRebind(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element
//

class DLLAPI CUITextElement : public CUIBaseElement
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUITextElement(void);

		// Attributes
		UINT    GetFlags(void) const;
		BOOL    HasFlag(UINT uFlag) const;
		UINT    GetWidth(void) const;
		UINT    GetLimit(void) const;
		CString GetVerb(void) const;
		CString GetUnits(void) const;
		CString GetDefault(void) const;

		// Operations
		void SetReadOnly(void);

		// Text Mode Editing
		CString GetAsText(void);
		UINT    SetAsText(CError &Error, CString Text);
		CString ScrollData(CString Text, UINT uCode);
		BOOL    EnumValues(CStringArray &List);
		BOOL    ExpandData(CWnd &Wnd);
		
	protected:
		// Data Members
		UINT    m_uFlags;
		UINT    m_uWidth;
		UINT    m_uLimit;
		CString m_Verb;
		CString m_Units;
		CString m_Default;

		// Overridable
		virtual CString OnGetAsText(void);
		virtual UINT    OnSetAsText(CError &Error, CString Text);
		virtual CString OnScrollData(CString Text, UINT uCode);
		virtual BOOL    OnEnumValues(CStringArray &List);
		virtual BOOL    OnExpand(CWnd &Wnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Grapical UI Element
//

class DLLAPI CUIElement : public CUIBaseElement
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIElement(void);

		// Destructor
		~CUIElement(void);

		// Binding
		BOOL Bind(CItem *pItem, CUIData const *pUIData, BOOL fTable);

		// Attributes
		CRect   GetRect(void) const;
		BOOL    IsVisible(void) const;
		CString GetFixed(void) const;

		// Text Access
		CUITextElement * GetText(void) const;
		
		// Core Operations
		void LayoutUI(CLayFormation *pForm);
		void CreateUI(CWnd &Wnd, UINT &uID);
		void PositionUI(void);
		void ShowUI(BOOL fShow);
		void EnableUI(BOOL fEnable);
		void UpdateUI(void);
		void OffsetUI(CSize const &Offset);
		void ScrollData(UINT uCode);
		void PaintUI(CRect const &Work, CDC &DC);
		BOOL FindFocus(CWnd * &pWnd);
		void AllowMulti(BOOL fMulti);

		// Data Operations
		void LoadUI(void);
		UINT SaveUI(BOOL fUI);
		BOOL CanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL AcceptData(IDataObject *pData);

		// Notifications
		BOOL NotifyUI(UINT uID, UINT uCode);
		BOOL NotifyUI(UINT uID, NMHDR &Info);

	protected:
		// Data Members
		CString          m_Fixed;
		BOOL             m_fTable;
		CUITextElement * m_pText;
		CLayFormation  * m_pMainLayout;
		BOOL		 m_fMulti;
		BOOL		 m_fShow;
		BOOL		 m_fBusy;

		// Core Overidables
		virtual void OnBind(void);
		virtual void OnRebind(void);
		virtual void OnLayout(CLayFormation *pForm);
		virtual void OnCreate(CWnd &Wnd, UINT &uID);
		virtual void OnPosition(void);
		virtual void OnShow(BOOL fShow);
		virtual void OnEnable(BOOL fEnable);
		virtual void OnScrollData(UINT uCode);
		virtual void OnPaint(CRect const &Work, CDC &DC);
		virtual BOOL OnFindFocus(CWnd * &pWnd);

		// Data Overridables
		virtual void OnLoad(void);
		virtual UINT OnSave(BOOL fUI);
		virtual BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		virtual BOOL OnAcceptData(IDataObject *pData);

		// Notification Handlers
		virtual BOOL OnNotify(UINT uID, UINT uCode);
		virtual BOOL OnNotify(UINT uID, NMHDR &Info);

		// Implementation
		UINT StdSave(BOOL fUI, CString Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element with Controls
//

class DLLAPI CUIControl : public CUIElement
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIControl(void);

		// Attributes
		UINT GetID(void);

		// Core Operations
		void AddControl(CCtrlWnd *pCtrl);
		void AddControl(CCtrlWnd *pCtrl, UINT uCode);

	protected:
		// Type Definitions
		typedef CArray <CCtrlWnd *> CCtrlList;

		// Data Members
		CCtrlList m_List;
		UINT	  m_uID;
		UINT      m_uCode;

		// Core Overridables
		void OnShow(BOOL fShow);
		void OnEnable(BOOL fEnable);
		BOOL OnFindFocus(CWnd * &pWnd);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		BOOL CanAcceptText(IDataObject *pData, UINT cfText);
		BOOL AcceptText(IDataObject *pData, UINT cfText, CString Prefix);
		BOOL AcceptText(IDataObject *pData, UINT cfText);
		void ForceUpdate(void);
		BOOL EnsureFocus(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

interface IUICreate;
interface IUIHost;
interface IUICore;

//////////////////////////////////////////////////////////////////////////
//
// User Interface Creation
//

interface IUICreate
{
	virtual void StartPage(UINT uCols)					 = 0;
	virtual void ResetPage(UINT uCols)					 = 0;
	virtual void StartOverlay(void)						 = 0;
	virtual void StartGroup(UINT uCols)					 = 0;
	virtual void StartGroup(PCTXT pName, UINT uCols)			 = 0;
	virtual void StartGroup(PCTXT pName, UINT uCols, BOOL fEqual)		 = 0;
	virtual void StartTable(PCTXT pName, UINT uCols)			 = 0;
	virtual void AddColHeadExtraRow(void)					 = 0;
	virtual void AddColHead(PCTXT pName)					 = 0;
	virtual void AddRowHead(PCTXT pName)					 = 0;
	virtual BOOL AddUI(CItem *pItem, CString Object, CString Tag)		 = 0;
	virtual BOOL AddUI(CItem *pItem, CString Object, CUIData const *pUIData) = 0;
	virtual void AddButton(PCTXT pName, PCTXT pTip, UINT uID)		 = 0;
	virtual void AddButton(PCTXT pName, UINT uID)				 = 0;
	virtual void AddButton(PCTXT pName, PCTXT pTip, PCTXT pTag)		 = 0;
	virtual void AddButton(PCTXT pName, PCTXT pTag)				 = 0;
	virtual void AddNarrative(PCTXT pText)					 = 0;
	virtual void AddElement(CUIElement *pUI)				 = 0;
	virtual void EndTable(void)						 = 0;
	virtual void EndGroup(BOOL fExpand)					 = 0;
	virtual void EndOverlay(void)						 = 0;
	virtual void EndPage(BOOL fExpand)					 = 0;
	virtual void SetBorder(int nBorder)					 = 0;
	virtual void NoRecycle(void)						 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Core
//

interface DLLAPI IUICore
{
	virtual PUITEXT OnFindUI(CLASS Class, CItem *pItem, PCTXT pTag)	   = 0;
	virtual BOOL	OnShowUI  (CItem *pItem, PCTXT pTag, BOOL fShow)   = 0;
	virtual BOOL	OnEnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable) = 0;
	virtual BOOL	OnUpdateUI(CItem *pItem, PCTXT pTag)		   = 0;
	virtual void	OnRemakeUI(void)				   = 0;
	virtual BOOL	OnHasUndo(void)					   = 0;
	virtual BOOL	OnInReplay(void)				   = 0;
	virtual void	OnExecExtraCmd(CCmd *pCmd)			   = 0;
	virtual void	OnSaveExtraCmd(CCmd *pCmd)			   = 0;
	virtual void	OnSendUpdate(UINT uType)			   = 0;
	virtual BOOL	KillUndoList(void)				   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Host
//

interface DLLAPI IUIHost
{
	virtual BOOL       HasWindow(void)				    = 0;
	virtual CViewWnd & GetWindow(void)				    = 0;
	virtual PUITEXT    FindUI(CLASS Class,  CItem *pItem, PCTXT pTag)   = 0;
	virtual PUITEXT    FindUI(CItem *pItem, PCTXT pTag)		    = 0;
	virtual PUITEXT    FindUI(CLASS Class,  PCTXT pTag)		    = 0;
	virtual PUITEXT    FindUI(PCTXT pTag)				    = 0;
	virtual void       ShowUI(BOOL fShow)				    = 0;
	virtual void       ShowUI(CItem *pItem, BOOL fShow)		    = 0;
	virtual BOOL	   ShowUI(CItem *pItem, PCTXT pTag, BOOL fShow)     = 0;
	virtual BOOL	   ShowUI(CItem *pItem, PCSTR pTag, BOOL fShow)     = 0;
	virtual BOOL	   ShowUI(PCTXT pTag, BOOL fShow)		    = 0;
	virtual BOOL	   ShowUI(PCSTR pTag, BOOL fShow)		    = 0;
	virtual void	   EnableUI(BOOL fEnable)			    = 0;
	virtual void	   EnableUI(CItem *pItem, BOOL fEnable)		    = 0;
	virtual BOOL	   EnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable) = 0;
	virtual BOOL	   EnableUI(CItem *pItem, PCSTR pTag, BOOL fEnable) = 0;
	virtual BOOL	   EnableUI(PCTXT pTag, BOOL fEnable)		    = 0;
	virtual BOOL	   EnableUI(PCSTR pTag, BOOL fEnable)		    = 0;
	virtual void	   UpdateUI(void)				    = 0;
	virtual void	   UpdateUI(CItem *pItem)			    = 0;
	virtual BOOL	   UpdateUI(CItem *pItem, PCTXT pTag)		    = 0;
	virtual BOOL	   UpdateUI(CItem *pItem, PCSTR pTag)		    = 0;
	virtual BOOL	   UpdateUI(PCTXT pTag)				    = 0;
	virtual BOOL	   UpdateUI(PCSTR pTag)				    = 0;
	virtual void	   RemakeUI(void)				    = 0;
	virtual BOOL	   HasUndo (void)				    = 0;
	virtual BOOL	   InReplay(void)				    = 0;
	virtual void	   ExecExtraCmd(CCmd *pCmd)			    = 0;
	virtual void	   SaveExtraCmd(CCmd *pCmd)			    = 0;
	virtual void	   SendUpdate(UINT uType)			    = 0;
	virtual BOOL	   KillUndoList(void)				    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUIStdHost;

//////////////////////////////////////////////////////////////////////////
//
// User Interface Standard Host
//

class DLLAPI CUIStdHost : public IUIHost
{
	public:
		// Constructor
		CUIStdHost(IUICore *pCore, CViewWnd *pWnd);

		// UI Window
		BOOL       HasWindow(void);
		CViewWnd & GetWindow(void);

		// UI Location
		CUITextElement * FindUI(CLASS Class,  CItem *pItem, PCTXT pTag);
		CUITextElement * FindUI(CItem *pItem, PCTXT pTag);
		CUITextElement * FindUI(CLASS Class,  PCTXT pTag);
		CUITextElement * FindUI(PCTXT pTag);

		// UI Creation
		void RemakeUI(void);

		// UI Showing
		void ShowUI(BOOL fShow);
		void ShowUI(CItem *pItem, BOOL fShow);
		BOOL ShowUI(CItem *pItem, PCTXT pTag, BOOL fShow);
		BOOL ShowUI(CItem *pItem, PCSTR pTag, BOOL fShow);
		BOOL ShowUI(PCTXT pTag, BOOL fShow);
		BOOL ShowUI(PCSTR pTag, BOOL fShow);

		// UI Enabling
		void EnableUI(BOOL fEnable);
		void EnableUI(CItem *pItem, BOOL fEnable);
		BOOL EnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable);
		BOOL EnableUI(CItem *pItem, PCSTR pTag, BOOL fEnable);
		BOOL EnableUI(PCTXT pTag, BOOL fEnable);
		BOOL EnableUI(PCSTR pTag, BOOL fEnable);

		// UI Update
		void UpdateUI(void);
		void UpdateUI(CItem *pItem);
		BOOL UpdateUI(CItem *pItem, PCTXT pTag);
		BOOL UpdateUI(CItem *pItem, PCSTR pTag);
		BOOL UpdateUI(PCTXT pTag);
		BOOL UpdateUI(PCSTR pTag);

		// UI Mode
		BOOL HasUndo (void);
		BOOL InReplay(void);
		
		// Extra Commands
		void ExecExtraCmd(CCmd *pCmd);
		void SaveExtraCmd(CCmd *pCmd);

		// Item Updates
		void SendUpdate(UINT uType);

		// Undo Kill
		BOOL KillUndoList(void);

	protected:
		// Data Members
		IUICore  * m_pCore;
		CViewWnd * m_pWnd;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUIPageList;
class CUIPage;
class CUIStdPage;

//////////////////////////////////////////////////////////////////////////
//								
// User Interface Page List
//

class DLLAPI CUIPageList : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CUIPageList(void);

		// Destructor
		~CUIPageList(void);

		// Attributes
		UINT      GetCount(void) const;
		CUIPage * GetEntry(UINT uIndex) const;

		// Operations
		void Append (CUIPage *pPage);
		void Replace(UINT uIndex, CUIPage *pPage);

		// Indexing
		CUIPage * operator [] (UINT uIndex) const;

	protected:
		// Data Members
		CArray <CUIPage *> m_List;
	};

//////////////////////////////////////////////////////////////////////////
//								
// User Interface Page Base Class
//

class DLLAPI CUIPage : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIPage(void);
		
		// Attributes
		CString GetTitle(void) const;

		// Operations
		virtual BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CString m_Title;

		// Implementation
		void UIError(PCTXT pText, ...);
	};

//////////////////////////////////////////////////////////////////////////
//								
// User Interface Standard Page
//

class DLLAPI CUIStdPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIStdPage(void);
		CUIStdPage(CString Title, CLASS Class, UINT uPage);
		CUIStdPage(CLASS Class, UINT uPage);
		CUIStdPage(CLASS Class);
		
		// Creation
		void Create(CLASS Class, UINT &uPage, CString Text);

		// Operations
		void SetSubstitute(CString Obj1, CString Obj2);
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CLASS   m_Class;
		UINT    m_uPage;
		CString m_Group;
		CString m_Obj1;
		CString m_Obj2;

		// Implementation
		CString GetNameW(CLASS Class);
		CString GetNameA(CLASS Class);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUIViewWnd;
class CUIToolTip;
class CUIItem;
class CUIItemViewWnd;
class CUIItemMultiWnd;

//////////////////////////////////////////////////////////////////////////
//								
// User Interface View Window
//

class DLLAPI CUIViewWnd : public CDialogViewWnd,
			  public CUIStdHost,
			  public IUICreate,
			  public IUICore,
			  public IDropTarget
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIViewWnd(void);

		// Destructor
		~CUIViewWnd(void);

		// Attributes
		CRect GetBorder(void) const;
		CRect GetUIRect(void) const;

		// UI Attributes
		CSize GetMinSize(void) const;
		CSize GetMaxSize(void) const;

		// UI Operations
		void CreateUI(void);
		void LayoutUI(void);
		void PositionUI(void);
		void OffsetUI(void);

		// IUICreate
		void StartPage(UINT uCols);
		void ResetPage(UINT uCols);
		void StartOverlay(void);
		void StartGroup(UINT uCols);
		void StartGroup(PCTXT pName, UINT uCols);
		void StartGroup(PCTXT pName, UINT uCols, BOOL fEqual);
		void StartTable(PCTXT pName, UINT uCols);
		void AddColHeadExtraRow(void);
		void AddColHead(PCTXT pName);
		void AddRowHead(PCTXT pName);
		BOOL AddUI(CItem *pItem, CString Object, CString Tag);
		BOOL AddUI(CItem *pItem, CString Object, CUIData const *pUIData);
		void AddButton(PCTXT pName, PCTXT pTip, UINT uID);
		void AddButton(PCTXT pName, UINT uID);
		void AddButton(PCTXT pName, PCTXT pTip, PCTXT pTag);
		void AddButton(PCTXT pName, PCTXT pTag);
		void AddNarrative(PCTXT pText);
		void AddElement(CUIElement *pUI);
		void EndTable(void);
		void EndGroup(BOOL fExpand);
		void EndOverlay(void);
		void EndPage(BOOL fExpand);
		void SetBorder(int nBorder);
		void SetBorder(CRect const &Border);
		void NoRecycle(void);
		void NoScroll(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Commands
		class CCmdEdit;

		// Type Definitions
		typedef CArray <CUIElement *>	       CUIList;
		typedef CMap   <CString, CUIElement *> CUIDict;
		typedef CMap   <CString, CString>      CUIHist;
		typedef CMap   <UINT,    UINT>	       CUICtrl;

		// Static Data
		static BOOL m_fStatus;
		static BOOL m_fFirst;

		// Bound Item
		CMetaItem * m_pItem;
		CUISchema * m_pSchema;
		BOOL	    m_fRead;
		BOOL	    m_fHasUndo;
		CString	    m_HelpText;
		CSysProxy   m_System;

		// Layout Data
		CRect		m_Border;
		int		m_nScroll;
		CScrollBar    * m_pScroll;
		CRect		m_Active;
		CRect	        m_UIRect;
		BOOL		m_fValid;
		CLayFormation * m_pForm;
		CLayFormation * m_pGrid;
		CLayFormation * m_pSave;
		BOOL		m_fTable;
		UINT		m_uFormCols;
		UINT		m_uFormCount;
		UINT		m_uGridCols;
		UINT		m_uGridCount;
		CString		m_GridName;
		UINT		m_uMinID;
		UINT		m_uMaxID;
		BOOL		m_fScroll;

		// Current Status
		CWnd	* m_pFocus;
		UINT	  m_uFocus;
		CString   m_NavTag;
		BOOL      m_fEditCmd;
		BOOL      m_fEditMul;
		BOOL      m_fExecCmd;
		BOOL      m_fExecMul;
		BOOL      m_fRemake;
		CString	  m_EditItem;
		CString	  m_EditMenu;
		BOOL	  m_fNoUndo;

		// Field Data
		CUIList      m_UIList;
		CStringArray m_UIItem;
		CUIDict	     m_UIDict;
		CUIHist	     m_UIHist;
		CUICtrl      m_UICtrl;

		// Tool Tip Data
		CToolTip * m_pToolTip;
		UINT	   m_uToolMode;
		UINT	   m_uToolShow;

		// Drop Context
		CDropHelper m_DropHelp;

		// IUICore
		PUITEXT OnFindUI(CLASS Class, CItem *pItem, PCTXT pTag);
		BOOL    OnShowUI  (CItem *pItem, PCTXT pTag, BOOL fShow);
		BOOL    OnEnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable);
		BOOL    OnUpdateUI(CItem *pItem, PCTXT pTag);
		void    OnRemakeUI(void);
		BOOL    OnHasUndo(void);
		BOOL    OnInReplay(void);
		void    OnExecExtraCmd(CCmd *pCmd);
		void    OnSaveExtraCmd(CCmd *pCmd);
		void    OnSendUpdate(UINT uType);
		BOOL	KillUndoList(void);

		// UI Update
		virtual void OnUICreate(void);
		virtual void OnUIChange(CItem *pItem, CString Tag);

		// Overribables
		void    OnAttach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnDestroy(void);
		void OnCancelMode(void);
		void OnSetCurrent(BOOL fCurrent);
		void OnPaint(void);
		void OnPaint(CDC &DC);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnSize(UINT uCode, CSize Size);
		void OnGetMinMaxInfo(MINMAXINFO &Info);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		BOOL OnNotify(UINT uID, NMHDR &Info);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		void OnSetFocus(CWnd &Wnd);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
		void OnHScroll(UINT uCode, int nPos, CWnd &Ctrl);
		void OnVScroll(UINT uCode, int nPos, CWnd &Ctrl);
		HOBJ OnCtlColorStatic(CDC &DC, CWnd &Wnd);
		HOBJ OnCtlColorBtn(CDC &DC, CWnd &Wnd);

		// View Menu
		BOOL OnViewRefresh(UINT uID);

		// Help Menu
		BOOL OnBalloonControl(UINT uID, CCmdSource &Src);
		BOOL OnBalloonCommand(UINT uID);

		// Notification Handlers
		UINT OnToolShow(UINT uID, NMHDR &Info);
		void OnScroll(UINT uCode, int nPos, CWnd &Ctrl);

		// Schema Management
		void LoadSchema(CItem *pItem);
		void FreeSchema(void);

		// Tag Naming
		CString GetLongName(CUIElement *pUI) const;
		CString GetLongName(CItem *pItem, CString Tag) const;

		// Sub-Item Location
		CItem * GetObject(CItem *pItem, CString Object);

		// Tool Tips
		void CreateToolTip(void);
		void DestroyToolTip(void);
		void AddToolTips(BOOL fTrack);
		void PlaceToolRect(CRect &Rect, CRect const &Item, BOOL fRight, BOOL fBottom);
		void TrackToolTip(BOOL fForce);
		void MoveToolTips(void);
		void HideToolTip(void);

		// Editing Command
		BOOL OnExecEdit(CCmdEdit *pCmd, CString Data);

		// Implementation
		void MakeUI(void);
		void KillUI(void);
		BOOL SetNavFocus(CString Nav);
		BOOL StartMulti(void);
		BOOL CloseMulti(void);
		BOOL CheckForChange(CUIElement *pUI, BOOL fUpdate);
		void SendChange(CCmdSubItem *pSub);
		BOOL SendChange(CUIElement *pUI);
		void SendChange(CItem *pItem, CString Tag);
		void SendEnable(CItem *pItem);
		void SendEnable(void);
		BOOL CheckScroll(void);
		BOOL Scroll(int nScroll);
		void AddToDict(CUIElement *pUI);
		void BuildHistory(void);
		void BuildDict(void);
		void UIError(PCTXT pText, ...);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Editing Command
//

class DLLAPI CUIViewWnd::CCmdEdit : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdEdit(CString Menu, CString Item, CString Tag, CString Prev, CString Data);

		// Data Members
		CString m_Tag;
		CString m_Prev;
		CString m_Data;
	};

//////////////////////////////////////////////////////////////////////////
//
// Sub-Classed Tool Tip Control
//

class DLLAPI CUIToolTip : public CToolTip
{
	public:
		// Constructor
		CUIToolTip(void);

	protected:
		// Window Procedure
		LRESULT WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);
	};

//////////////////////////////////////////////////////////////////////////
//								
// Data Item with User Interface Support
//

class DLLAPI CUIItem : public CMetaItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIItem(void);

		// Attributes
		CString GetPropAsText(CString Tag) const;
		CString GetSubItemLabel(CItem const *pItem) const;
		CLASS   GetSubItemClass(CItem const *pItem) const;

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Overridables
		virtual void OnUIChange (IUIHost    *pHost, CItem *pItem, CString Tag);
		virtual void OnUIChange (CUIViewWnd *pView, CItem *pItem, CString Tag);
		virtual BOOL OnLoadPages(CUIPageList *pList);
		virtual BOOL OnSummarize(CString &Text);

	protected:
		// Implementation
		CViewWnd * CreateItemView(BOOL fScroll);
	};

//////////////////////////////////////////////////////////////////////////
//								
// Default View for UI Item
//

class DLLAPI CUIItemViewWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CUIItemViewWnd(CUIPageList *pList, BOOL fScroll = TRUE);
		CUIItemViewWnd(CUIPage     *pPage, BOOL fScroll = TRUE);

		// Destructor
		~CUIItemViewWnd(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// Operations
		void SetPage(CUIPage *pPage);

	protected:
		// Data Members
		CUIPageList * m_pList;
		CUIPage     * m_pPage;
		CUIItem     * m_pItem;

		// Overridables
		void OnAttach(void);
		BOOL OnNavigate(CString const &Nav);

		// Implementation
		void SendToAll(CString Tag);
	};

//////////////////////////////////////////////////////////////////////////
//								
// Multi Page View for UI Item
//

class DLLAPI CUIItemMultiWnd : public CMultiViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIItemMultiWnd(CUIPageList *pList, BOOL fScroll = TRUE);

		// Destructor
		~CUIItemMultiWnd(void);

		// Operations
		BOOL RemakeRest(CWnd *pSkip);

	protected:
		// Static Data
		static UINT m_timerSelect;

		// Data Members
		CUIPageList * m_pList;
		BOOL          m_fScroll;
		CUIItem     * m_pItem;
		BOOL          m_fBusy;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);

		// Overridables
		void    OnAttach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// View Creation
		virtual CViewWnd * CreateViewObject(UINT n, CUIPage *pPage);
	
		// Implementation
		void MakeTabs(void);
		void AttachViews(void);
		void FindItem(void);
		void FreeList(void);
	};

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CUIGetSet;
class CUIGetSetDialog;

//////////////////////////////////////////////////////////////////////////
//								
// Property Get-and-Set Object
//

class DLLAPI CUIGetSet : public IUICreate, public IUICore
{
	public:
		// Constructor
		CUIGetSet(void);

		// Destructor
		~CUIGetSet(void);

		// Attributes
		BOOL HadFailures(void) const;

		// Operations
		BOOL GetProps(CStringArray &Props, CUIItem *pRoot);
		BOOL SetProps(CUIItem *pRoot, CStringArray &Props);

	protected:
		// UI Entry
		struct CEntry
		{
			UINT	m_uPage;
			CUIData m_UIData;
			CItem * m_pItem;
			PUITEXT m_pText;
			BOOL	m_fShow;
			BOOL	m_fEnable;
			};

		// Typedefs
		typedef CArray <CEntry *>          CEntryList;
		typedef CMap   <CItem  *, CString> CChildNameMap;
		typedef CMap   <CString,  CItem *> CChildItemMap;

		// Data Members
		BOOL	      m_fFail;
		CUIItem     * m_pRoot;
		UINT	      m_uPage;
		UINT	      m_uPages;
		CUISchema   * m_pSchema;
		CUIStdHost  * m_pHost;
		CItem       * m_pItem;
		CEntryList    m_List;
		CChildNameMap m_NameMap;
		CChildItemMap m_ItemMap;

		// IUICreate
		void StartPage(UINT uCols);
		void ResetPage(UINT uCols);
		void StartOverlay(void);
		void StartGroup(UINT uCols);
		void StartGroup(PCTXT pName, UINT uCols);
		void StartGroup(PCTXT pName, UINT uCols, BOOL fEqual);
		void StartTable(PCTXT pName, UINT uCols);
		void AddColHeadExtraRow(void);
		void AddColHead(PCTXT pName);
		void AddRowHead(PCTXT pName);
		BOOL AddUI(CItem *pItem, CString Object, CString Tag);
		BOOL AddUI(CItem *pItem, CString Object, CUIData const *pUIData);
		void AddButton(PCTXT pName, PCTXT pTip, UINT uID);
		void AddButton(PCTXT pName, UINT uID);
		void AddButton(PCTXT pName, PCTXT pTip, PCTXT pTag);
		void AddButton(PCTXT pName, PCTXT pTag);
		void AddNarrative(PCTXT pText);
		void AddElement(CUIElement *pUI);
		void EndTable(void);
		void EndGroup(BOOL fExpand);
		void EndOverlay(void);
		void EndPage(BOOL fExpand);
		void SetBorder(int nBorder);
		void SetBorder(CRect const &Border);
		void NoRecycle(void);
		void NoScroll(void);

		// IUICore
		PUITEXT OnFindUI(CLASS Class, CItem *pItem, PCTXT pTag);
		BOOL    OnShowUI  (CItem *pItem, PCTXT pTag, BOOL fShow);
		BOOL    OnEnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable);
		BOOL    OnUpdateUI(CItem *pItem, PCTXT pTag);
		void    OnRemakeUI(void);
		BOOL    OnHasUndo(void);
		BOOL    OnInReplay(void);
		void    OnExecExtraCmd(CCmd *pCmd);
		void    OnSaveExtraCmd(CCmd *pCmd);
		void    OnSendUpdate(UINT uType);
		BOOL    KillUndoList(void);

		// Schema Management
		void LoadSchema(CItem *pItem);
		void FreeSchema(void);

		// Sub-Item Location
		CItem * GetObject(CItem *pItem, CString Object);

		// Implementation
		BOOL MakeUI(void);
		void CleanUp(void);
		BOOL LoadPages(void);
		BOOL SendEnables(void);
		BOOL SendEnable(CItem *pItem);
		BOOL SendChange(CUIItem *pItem, CString const &Tag);
		BOOL AddRootToMap(void);
		BOOL BuildChildMap(CString Root, CUIItem *pItem);
		BOOL GetAsText(CStringArray &Props);
		BOOL SetAsText(CStringArray &Props);
		BOOL SetValue(CEntry *pEntry, CString const &Value);
		void UIError(PCTXT pText, ...);
		void Encode(CString &Value);
		void Decode(CString &Value);

	};

//////////////////////////////////////////////////////////////////////////
//								
// Property Selection Dialog
//

class DLLAPI CUIGetSetDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIGetSetDialog(CString Name, CStringArray &Props);

		// Destructor
		~CUIGetSetDialog(void);

		// Operations
		void SetLastBuffer(CStringTree &Last);

	public:
		// Static Data
		static CStringTree m_Last;

		// Data Members
		CString        m_Name;
		CStringArray * m_pProps;
		CListView    * m_pView;
		BOOL	       m_fBusy;
		CStringTree  * m_pLast;
			 
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnAll(UINT uID);
		BOOL OnNone(UINT uID);

		// Notification Handlers
		void OnItemChanged(UINT uID, NMLISTVIEW &Info);
		UINT OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

		// Implementation
		void MakeList(void);
		void LoadList(void);
		void ApplyLast(void);
		void SetParents(BOOL fAll);
		void ShowList(void);
		void CheckItem(INT n, BOOL fCheck);
		BOOL IsChecked(INT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUITextString;
class CUITextDisplay;
class CUITextName;
class CUITextFile;
class CUITextPath;
class CUITextCert;
class CUITextInteger;
class CUITextHexInt;
class CUITextASCII;
class CUITextIPAddress;
class CUITextOnOff;
class CUITextEnum;
class CUITextEnumPick;
class CUITextEnumInt;
class CUITextBitMask;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- String
//

class DLLAPI CUITextString : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextString(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Display
//

class DLLAPI CUITextDisplay : public CUITextString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextDisplay(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Name
//

class DLLAPI CUITextName : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextName(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Filename
//

class DLLAPI CUITextFile : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextFile(void);

	protected:
		// Data Members
		CString	     m_Filter;
		CString      m_LastPath;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL	OnExpand(CWnd &Wnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Pathname
//

class DLLAPI CUITextPath : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextPath(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL	OnExpand(CWnd &Wnd);

		// Callback
		static INT CALLBACK Hook(HWND, UINT, LPARAM, LPARAM);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Certificate
//

class DLLAPI CUITextCert : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextCert(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL	OnExpand(CWnd &Wnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Integer
//

class DLLAPI CUITextInteger : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextInteger(void);

	protected:
		// Data Members
		UINT m_uPlaces;
		INT  m_nMin;
		INT  m_nMax;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		CString OnScrollData(CString Text, UINT uCode);

		// Scaling
		virtual INT StoreToDisp(INT nData);
		virtual INT DispToStore(INT nData);

		// Implementation
		BOOL    Check (CError &Error, INT &nData);
		CString Format(INT nData);
		INT     Parse (PCTXT pText);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Hex Integer
//

class DLLAPI CUITextHexInt : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextHexInt(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		CString Format(UINT uData);
		UINT    Parse (PCTXT pText);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- ASCII Character
//

class DLLAPI CUITextASCII : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextASCII(void);

	protected:
		// Data Members
		UINT m_uMin;
		UINT m_uMax;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		CString OnScrollData(CString Text, UINT uCode);

		// Implementation
		CString Format(UINT uData);
		UINT    Parse (PCTXT pText);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- IP Address
//

class DLLAPI CUITextIPAddress : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextIPAddress(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- On/Off
//

class DLLAPI CUITextOnOff : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextOnOff(void);

	protected:
		// Data Members
		CString m_On;
		CString m_Off;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnEnumValues(CStringArray &List);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Enumerated
//

class DLLAPI CUITextEnum : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnum(void);

		// Attributes
		BOOL IsDataValid(UINT uData) const;

		// Operations
		void SetMask(UINT uMask);
		void RebindUI(CStringArray List);

	protected:
		// Data Option
		struct CEntry
		{
			// Data Members
			CString m_Text;
			UINT    m_Data;
			};

		// Data Members
		CArray <CEntry> m_Enum;
		UINT            m_uMask;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		CString OnScrollData(CString Text, UINT uCode);
		BOOL    OnEnumValues(CStringArray &List);
		
		// Implementation
		void AddData(UINT Data, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Enumerated with Pick
//

class DLLAPI CUITextEnumPick : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnumPick(void);

	protected:
		// Overridables
		BOOL OnExpand(CWnd &Wnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Pick Dialog
//

class DLLAPI CPickDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CPickDialog(CUITextElement *pText, CString Data);

		// Attributes
		CString GetData(void) const;
		
	protected:
		// Data Members
		CUITextElement * m_pText;
		CString          m_Data;
		CStringArray     m_DataText;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
	};	

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Enumerated Integer
//

class DLLAPI CUITextEnumInt : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnumInt(void);

	protected:
		// Data Members
		UINT m_uMin;
		UINT m_uMax;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Indexed Integer
//

class DLLAPI CUITextEnumIndexed : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnumIndexed(void);

	protected:
		// Overridables
		void    OnBind(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Bit Mask
//

class DLLAPI CUITextBitMask : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextBitMask(void);

	protected:
		// Data Members
		UINT	      m_uCols;
		BOOL	      m_fRev;
		CStringArray  m_DataText;
		
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnExpand(CWnd &Wnd);

		// Implementation
		CString Format(UINT uData);
		UINT    Parse(CString Text);
		UINT    FindMask(UINT b);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUIGroup;
class CUIHeading;
class CUIButton;
class CUIMoreButton;

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Group Box
//

class DLLAPI CUIGroup : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIGroup(PCTXT pLabel, CLayItem *pItem);

	protected:
		// Data Members
		CString	      m_Label;
		CLayItem    * m_pItem;
		CLayFormPad * m_pCtrlLayout;
		CCtrlWnd    * m_pGroupCtrl;
		CSize	      m_FontSize;

		// Core Overridables
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Implementation
		void FindMetrics(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Table Heading
//

class DLLAPI CUIHeading : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIHeading(PCTXT pTag, PCTXT pLabel);

	protected:
		// Data Members
		CString	       m_Label;
		CLayItemText * m_pCtrlLayout;
		CStatic	     * m_pStatic;

		// Core Overridables
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Action Button
//

class DLLAPI CUIButton : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIButton(PCTXT pLabel, PCTXT pTip, UINT ID);
		CUIButton(PCTXT pLabel, PCTXT pTip, PCTXT pTag);

	protected:
		// Data Members
		CString	       m_Label;
		UINT	       m_ID;
		CLayItemText * m_pCtrlLayout;
		CHotLinkCtrl * m_pButton;

		// Core Overridables
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- More Button
//

class DLLAPI CUIMoreButton : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIMoreButton(void);

	protected:
		// Data Members
		CString	       m_Label;
		UINT           m_uPad;
		CLayItemText * m_pCtrlLayout;
		CHotLinkCtrl * m_pHotLink;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect GetRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDropEditCtrl;
class CDropComboBox;
class CDropButton;
class CModeButton;
class CLayDropDown;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUIEditBox;
class CUIDropDown;
class CUIDropPick;
class CUIDropEdit;
class CUIRadioBase;
class CUIRadioRow;
class CUIRadioCol;
class CUIIPAddress;
class CUICheck;
class CUIPick;
class CUIPushBase;
class CUIPushCol;
class CUIPushRow;
class CUIScrollBase;
class CUIScrollCol;
class CUIScrollRow;
class CUICategorizer;

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Edit Box
//

class DLLAPI CUIEditBox : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIEditBox(void);

	protected:
		// Data Members
		CString		m_Label;
		CString		m_Units;
		DWORD		m_dwStyle;
		BOOL		m_fFlexUnits;
		CLayItemText  *	m_pTextLayout;
		CLayItemText  *	m_pDataLayout;
		CLayItem      *	m_pUnitLayout;
		CStatic	      *	m_pTextCtrl;
		CSpinner      * m_pSpinCtrl;
		CDropEditCtrl *	m_pDataCtrl;
		CStatic	      *	m_pUnitCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);
		void OnScrollData(UINT uCode);
		BOOL OnFindFocus(CWnd * &pWnd);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Nofication Overridbles
		BOOL OnNotify(UINT uID, NMHDR &Info);

		// Implementation
		CRect   GetDataCtrlRect(void);
		CString GetDataCtrlText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Drop Down List
//

class DLLAPI CUIDropDown : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIDropDown(void);

	protected:
		// Data Members
		BOOL	        m_fEdit;
		CString	        m_Label;
		CLayItemText  * m_pTextLayout;
		CLayItem      * m_pDataLayout;
		CStatic	      * m_pTextCtrl;
		CDropComboBox * m_pDataCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Overridables
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect FindComboRect(void);
		UINT  LoadList(void);
		BOOL  Reload(void);
		void  LockList(void);
		void  UnlockList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Drop Down List with Pick
//

class DLLAPI CUIDropPick : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIDropPick(void);

	protected:
		// Data Members
		BOOL	       m_fEdit;
		CString	       m_Label;
		CString        m_Verb;
		CLayItemText * m_pTextLayout;
		CLayItem     * m_pDataLayout;
		CLayItem     * m_pPickLayout;
		CStatic	     * m_pTextCtrl;
		CComboBox    * m_pDataCtrl;
		CCtrlWnd     * m_pPickCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect FindComboRect(void);
		UINT  LoadList(void);
		BOOL  Reload(void);
		void  LockList(void);
		void  UnlockList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Drop Down Edit
//

class DLLAPI CUIDropEdit : public CUIDropDown
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIDropEdit(void);

	protected:
		// Data Overridables
		void OnLoad(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Radio Button Group
//

class DLLAPI CUIRadioBase : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIRadioBase(void);

		// Destructor
		~CUIRadioBase(void);

	protected:
		// Type Definitions
		typedef CArray <CButton      *> CButtonCtrl;
		typedef CArray <CLayItemText *>	CButtonData;

		// Data Members
		CString	        m_Label;
		CLayItemText  *	m_pTextLayout;
		CStatic	      *	m_pTextCtrl;
		CStringArray	m_DataText;
		CButtonCtrl     m_DataCtrl;
		CButtonData     m_DataLayout;
		UINT            m_uFrom;
		UINT            m_uTo;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);
		
		// Implementation
		void AddData(UINT uData, CString Text);
		void AddCtrl(CButton *pCtrl);
		void AddData(CLayItemText *pData);
		void SetRadioGroup(UINT uData);
		UINT GetRadioGroup(void);

		// Layout Creation
		virtual CLayFormation * MakeLayout(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Radio Button Column
//

class DLLAPI CUIRadioCol : public CUIRadioBase
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIRadioCol(void);

	protected:
		// Layout Creation
		CLayFormation * MakeLayout(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Radio Button Row
//

class DLLAPI CUIRadioRow : public CUIRadioBase
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIRadioRow(void);

	protected:
		// Layout Creation
		CLayFormation * MakeLayout(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- IP Address
//

class DLLAPI CUIIPAddress : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIIPAddress(void);

	protected:
		// Data Members
		CString		m_Label;
		DWORD		m_dwStyle;
		CLayItemText  *	m_pTextLayout;
		CLayItemText  *	m_pDataLayout;
		CStatic	      *	m_pTextCtrl;
		CIPAddrCtrl   *	m_pDataCtrl;
		CFont		m_Font;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Implementation
		UINT    Parse(CString Text);
		CString Format(UINT uData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Check Box
//

class DLLAPI CUICheck : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUICheck(void);

	protected:
		// Data Members
		CString	        m_Label;
		CLayItemText  * m_pDataLayout;
		CButton	      * m_pDataCtrl;
		CStringArray	m_DataText;
		UINT	        m_uBit;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		UINT    Parse(PCTXT pText);
		CString Format(UINT uData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Pick Control
//

class DLLAPI CUIPick : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIPick(void);

	protected:
		// Data Members
		CString		m_Label;
		CString         m_Verb;
		BOOL		m_fFlexData;
		CLayItemText  *	m_pTextLayout;
		CLayItemText  *	m_pDataLayout;
		CLayItem      *	m_pPickLayout;
		CStatic	      *	m_pTextCtrl;
		CEditCtrl     *	m_pDataCtrl;
		CCtrlWnd      * m_pPickCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect GetDataCtrlRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Push Button Group
//

class DLLAPI CUIPushBase : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIPushBase(void);

		// Destructor
		~CUIPushBase(void);

	protected:
		// Type Definitions
		typedef CArray <CButton      *> CButtonCtrl;
		typedef CArray <CLayItemText *>	CButtonData;

		// Data Members
		CString	        m_Label;
		CStringArray    m_DataText;
		CLayItem      *	m_pTextLayout;
		CStatic	      *	m_pTextCtrl;
		CButtonCtrl     m_DataCtrl;
		CButtonData     m_DataLayout;
		UINT            m_uFrom;
		UINT            m_uTo;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		void AddCtrl(CButton *pCtrl);
		void AddData(CLayItemText *pData);

		// Layout Creation
		virtual CLayFormation * MakeLayout(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Push Button Column
//

class DLLAPI CUIPushCol : public CUIPushBase
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIPushCol(void);

	protected:
		// Layout Creation
		CLayFormation * MakeLayout(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Push Button Row
//

class DLLAPI CUIPushRow : public CUIPushBase
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIPushRow(void);

	protected:
		// Layout Creation
		CLayFormation * MakeLayout(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Scroll Bar Control
//

class CUIScrollBase : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIScrollBase(void);

	protected:
		// Data Members
		CString		m_Label;
		INT		m_nPage;
		INT		m_nMin;
		INT		m_nMax;
		CStringArray	m_DataText;
		CLayItemText  *	m_pTextLayout;
		CLayItem      *	m_pCtrlLayout;
		CStatic	      *	m_pTextCtrl;
		CScrollBar    *	m_pScrollCtrl;
		DWORD		m_dwStyle;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Implementation
		INT     Parse(PCTXT pText);
		CString Format(INT nData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Horizontal Scroll Bar Control
//

class CUIScrollHorz : public CUIScrollBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIScrollHorz(void);

	protected:
		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Vertical Scroll Bar Control
//

class CUIScrollVert : public CUIScrollBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIScrollVert(void);

	protected:
		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Categorizer
//

class DLLAPI CUICategorizer : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUICategorizer(void);

	protected:
		// Data Members
		CString		m_Label;
		DWORD		m_dwShowStyle;
		DWORD		m_dwEditStyle;
		DWORD		m_dwListStyle;
		UINT		m_uShowCode;
		UINT		m_uEditCode;
		UINT		m_uListCode;
		CLayItem      *	m_pTextLayout;
		CLayItem      *	m_pModeLayout;
		CLayItem      *	m_pDataLayout;
		CLayItem      * m_pPickLayout;
		CStatic       * m_pTextCtrl;
		CModeButton   *	m_pModeCtrl;
		CDropEditCtrl *	m_pShowCtrl;
		CDropEditCtrl *	m_pEditCtrl;
		CCtrlWnd      * m_pListCtrl;
		CCtrlWnd      * m_pPickCtrl;
		UINT            m_uMode;
		UINT            m_uType;
		BOOL            m_fShow;
		
		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);
		void OnShow(BOOL fShow);
		void OnScrollData(UINT uCode);
		BOOL OnFindFocus(CWnd * &pWnd);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);
		BOOL OnAcceptData(IDataObject *pData);

		// Nofication Overridbles
		BOOL OnNotify(UINT uID, UINT uCode);
		BOOL OnNotify(UINT uID, NMHDR &Info);

		// Category Overridables
		virtual BOOL    LoadModeButton(void);
		virtual void    LoadShowControl(CString Data);
		virtual void    LoadEditControl(CString Data);
		virtual void    LoadListControl(CString Data);
		virtual BOOL    SwitchMode(UINT uMode);
		virtual UINT    FindDispMode(CString Text);
		virtual CString FindDispText(CString Text, UINT uMode);
		virtual CString FindDataText(CString Text, UINT uMode);
		virtual UINT    FindDispType(UINT uMode);
		virtual CString FindDispVerb(UINT uMode);

		// Implementation
		void    LoadData(CString Data, BOOL fModify);
		CRect   GetDataCtrlRect(void);
		CRect   GetListCtrlRect(void);
		CString GetDataCtrlText(void);
		void    SetBestFocus(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDropEditCtrl;
class CDropComboBox;
class CDropButton;
class CModeButton;

/////////////////////////////////////////////////////////////////////////
//
// Capturing Edit Control
//

class DLLAPI CDropEditCtrl : public CEditCtrl, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDropEditCtrl(CUIElement *pUI);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Operations
		void SetScroll(BOOL fScroll);
		void SetNumber(BOOL fNumber);
		void SetDefault(PCTXT pDefault);

	protected:
		// Static Data
		static UINT m_timerQuick;

		// Data Members
		CUIElement * m_pUI;
		CString      m_Default;
		BOOL	     m_fScroll;
		BOOL	     m_fNumber;
		BOOL	     m_fFirst;
		BOOL	     m_fQuick;
		BOOL	     m_fSpecial;
		BOOL	     m_fError;
		BOOL	     m_fDying;
		CDropHelper  m_DropHelp;
		DWORD	     m_dwEffect;
		UINT         m_uDrop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnSetText(PCTXT pText);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnRButtonDown(UINT uFlags, CPoint Pos);
		void OnRButtonUp(UINT uFlags, CPoint Pos);
		void OnRButtonDblClk(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		UINT OnGetDlgCode(MSG *pMsg);
		void OnKeyDown(UINT uCode, DWORD dwData);
		void OnKeyUp(UINT uCode, DWORD dwData);
		void OnChar(UINT uCode, DWORD dwData);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Command Handlers
		BOOL OnPasteControl(UINT uID, CCmdSource &Src);
		BOOL OnPasteCommand(UINT uID);

		// Implementation
		void   Construct(void);
		void   ForceUpdate(void);
		void   HandleTab(void);
		CWnd & FindParent(void);
		BOOL   SetDrop(UINT uDrop);
		BOOL   InEditMode(void);
		BOOL   IsItemReadOnly(void);
		void   DrawSpecial(CDC &DC);
		void   DrawError(CDC &DC);
		DWORD  FindColor(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Drop Target Combo Box
//

class DLLAPI CDropComboBox : public CComboBox , public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDropComboBox(CUIElement *pUI);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Data Member
		CUIElement  * m_pUI;
		CDropHelper   m_DropHelp;
		DWORD	      m_dwEffect;
		UINT          m_uDrop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPaint(void);

		// Command Handlers
		BOOL OnPasteControl(UINT uID, CCmdSource &Src);
		BOOL OnPasteCommand(UINT uID);

		// Implementation
		BOOL SetDrop(UINT uDrop);
		BOOL IsItemReadOnly(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Drop Target Button
//

class DLLAPI CDropButton: public CButton, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDropButton(CUIElement *pUI);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Data Member
		CUIElement  * m_pUI;
		CDropHelper   m_DropHelp;
		DWORD	      m_dwEffect;
		UINT          m_uDrop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);

		// Notification Handlers
		UINT OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

		// Command Handlers
		BOOL OnPasteControl(UINT uID, CCmdSource &Src);
		BOOL OnPasteCommand(UINT uID);

		// Implementation
		BOOL SetDrop(UINT uDrop);
		BOOL IsItemReadOnly(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mode Selection Button
//

class DLLAPI CModeButton : public CDropButton
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CModeButton(CUIElement *pUI);

		// Option Data
		struct COption
		{
			UINT	m_uID;
			CString	m_Text;
			BOOL	m_fEnable;
			BOOL    m_fHidden;
			DWORD	m_Image;
			};

		// Creation
		BOOL Create(CRect const &Rect, HWND hParent, UINT uID);

		// Attributes
		CSize GetSize(void);
		CSize GetSize(CDC &DC);
		UINT  GetData(void) const;

		// Operations
		void ClearOptions(void);
		BOOL AddOption(COption const &Opt);
		void SetData(UINT uData);
		UINT ShowMenu(void);

	protected:
		// Data Member
		UINT               m_uData;
		UINT               m_uSlot;
		CArray <COption>   m_Opt;
		CMap   <UINT,UINT> m_Map;
		CSize              m_Size;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnGetDlgCode(MSG *pMsg);
		void OnChar(UINT uCode, DWORD dwFlags);
		void OnEnable(BOOL fEnable);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);

		// Notification Handlers
		UINT OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

		// Menu Control
		BOOL OnControl(UINT uID, CCmdSource &Src);
		BOOL OnGetInfo(UINT uID, CCmdInfo &Info);

		// Implementation
		void CalcSize(CDC &DC);
		BOOL Update(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CLayDropDown;

//////////////////////////////////////////////////////////////////////////
//
// Layout Item for Drop Down
//

class DLLAPI CLayDropDown : public CLayItem
{
	public:
		// Constructors
		CLayDropDown(CUITextElement *pText, BOOL fGrow);
		CLayDropDown(CUITextElement *pText, BOOL fGrow, int xPad);
		CLayDropDown(CUITextElement *pText, BOOL fGrow, int xPad, int yPad);

	protected:
		// Data Mebers
		CUITextElement * m_pText;
		BOOL             m_fGrow;
		int              m_xPad;
		int              m_yPad;

		// Overidables
		void OnPrepare(CDC &DC);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBitMaskData;

//////////////////////////////////////////////////////////////////////////
//
// Bit Mask Data
//

class DLLAPI CBitMaskData : public CUIItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CBitMaskData(void);

		// Destructor
		~CBitMaskData(void);

		// Operations
		void SetColumns(UINT uCols);
		void SetReverse(BOOL fRev);
		void LoadNames(CStringArray const &List);

		// Public Data
		UINT m_Data;

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

	protected:
		// Data Members
		UINT	     m_uCols;
		BOOL	     m_fRev;
		CStringArray m_List;

		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
