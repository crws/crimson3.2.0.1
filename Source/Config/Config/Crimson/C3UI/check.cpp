
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Checkbox Control
//

// Dynamic Class

AfxImplementDynamicClass(CUICheck, CUIControl);

// Constructor

CUICheck::CUICheck(void)
{
	m_pDataLayout = NULL;

	m_pDataCtrl   = New CButton;

	m_uBit	      = NOTHING;
	}

// Core Overridables

void CUICheck::OnBind(void)
{
	CUIElement::OnBind();

	if( m_pText->HasFlag(textEnum) ) {

		m_pText->EnumValues(m_DataText);
		}
	else {
		if( m_pText->GetFormat()[0] == 'B' ) {
			
			m_uBit = watoi(m_pText->GetFormat().Mid(1));
			}
		}
		
	m_Label = GetLabel();
	}

void CUICheck::OnLayout(CLayFormation *pForm)
{
	m_pDataLayout = New CLayItemText(m_Label + L" XXX ", 2, 2);

	m_pMainLayout = New CLayFormPad(m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem(New CLayItem());

	pForm->AddItem(m_pMainLayout);
	}

void CUICheck::OnCreate(CWnd &Wnd, UINT &uID)
{
	CString Label = m_Label;

	if( !Label.IsEmpty() ) {

		Label = L' ' + Label + L' ';
		}

	m_pDataCtrl->Create( Label,
			     WS_TABSTOP | BS_AUTOCHECKBOX | BS_NOTIFY,
			     m_pDataLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pDataCtrl, BN_CLICKED);
	}

void CUICheck::OnPosition(void)
{
	m_pDataCtrl->MoveWindow(m_pDataLayout->GetRect(), TRUE);
	}

// Data Overridables

void CUICheck::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	if( m_pText->HasFlag(textEnum) ) {
		
		BOOL  fCheck = (Data == m_DataText[1]);

		m_pDataCtrl->SetCheck(fCheck);
		}
	else {
		if( m_uBit == NOTHING ) {

			BOOL fCheck = (Parse(Data) ? TRUE : FALSE);
			
			m_pDataCtrl->SetCheck(fCheck);
			}
		else {
			BOOL fCheck = (Parse(Data) & (1 << m_uBit)) ? TRUE : FALSE;
			
			m_pDataCtrl->SetCheck(fCheck);
			}
		}
	}

UINT CUICheck::OnSave(BOOL fUI)
{
	BOOL fCheck = m_pDataCtrl->IsChecked();

	if( m_pText->HasFlag(textEnum) ) {

		return StdSave(fUI, m_DataText[fCheck ? 1 : 0]);
		}
	else {
		if( m_uBit == NOTHING ) {
			
			return StdSave(fUI, Format(fCheck));
			}

		UINT uData = Parse(m_pText->GetAsText());

		if( fCheck ) {

			uData |=  (1 << m_uBit);
			}
		else
			uData &= ~(1 << m_uBit);

		return StdSave(fUI, Format(uData));
		}
	}

// Notification Handlers

BOOL CUICheck::OnNotify(UINT uID, UINT uCode)
{
	if( m_Label.IsEmpty() ) {

		if( uCode == BN_SETFOCUS || uCode == BN_KILLFOCUS ) {

			CWnd  &Wnd = m_pDataCtrl->GetParent();

			CRect Rect = m_pDataCtrl->GetWindowRect() + 2;

			Rect.left -= 2;

			Rect.right = Rect.left + Rect.cy();

			Wnd.ScreenToClient(Rect);

			if( m_pDataCtrl->HasFocus() ) {

				CBrush  Brush = CBrush(afxColor(3dFace), afxColor(BLACK), 128);

				CClientDC(Wnd).FrameRect(Rect, Brush);
				}
			else {
				CBrush &Brush = afxBrush(3dFace);

				CClientDC(Wnd).FrameRect(Rect, Brush);
				}
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Implementation

UINT CUICheck::Parse(PCTXT pText)
{
	return watoi(pText);
	}

CString CUICheck::Format(UINT uData)
{
	return CPrintf(L"%u", uData);
	}

// End of File
