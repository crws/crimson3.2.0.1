
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- ASCII Character
//

// Dynamic Class

AfxImplementDynamicClass(CUITextASCII, CUITextElement);

// Lookup Table

static const struct { UINT c; TCHAR n[8]; } m_Lookup[] =

	{	{	0x00, L"NUL"	},
		{	0x01, L"SOH"	},
		{	0x02, L"STX"	},
		{	0x03, L"ETX"	},
		{	0x04, L"EOT"	},
		{	0x05, L"ENQ"	},
		{	0x06, L"ACK"	},
		{	0x07, L"BEL"	},
		{	0x08, L"BS"	},
		{	0x09, L"HT"	},
		{	0x0A, L"LF"	},
		{	0x0B, L"VT"	},
		{	0x0C, L"FF"	},
		{	0x0D, L"CR"	},
		{	0x0E, L"SO"	},
		{	0x0F, L"SI"	},
		{	0x10, L"DLE"	},
		{	0x11, L"DC1"	},
		{	0x12, L"DC2"	},
		{	0x13, L"DC3"	},
		{	0x14, L"DC4"	},
		{	0x15, L"NAK"	},
		{	0x16, L"SYN"	},
		{	0x17, L"ETB"	},
		{	0x18, L"CAN"	},
		{	0x19, L"EM"	},
		{	0x1A, L"SUB"	},
		{	0x1B, L"ESC"	},
		{	0x1C, L"FS"	},
		{	0x1D, L"GS"	},
		{	0x1E, L"RS"	},
		{	0x1F, L"US"	},
		{	0x20, L"SPACE"	},
		{	0x7F, L"DEL"	}

		};

// Constructor

CUITextASCII::CUITextASCII(void)
{
	m_uFlags = textEdit | textScroll;

	m_uMin   = 0x00;

	m_uMax   = 0x1F;

	m_uWidth = 8;

	m_uLimit = 6;
	}

// Overridables

void CUITextASCII::OnBind(void)
{
	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( List.GetCount() == 3 ) {

		if( List[2] == L"N" ) {
			
			m_uMin = 0x01;

			m_uMax = 0xFF;
			}
		else {
			m_uMin = 0x00;

			m_uMax = 0xFF;
			}
		}

	if( List.GetCount() == 2 ) {

		m_uMin   = 0x00;

		m_uMax   = 0xFF;

		m_Units  = List[1];

		m_uFlags = m_uFlags | textUnits;
		}

	CUITextElement::OnBind();
	}

CString CUITextASCII::OnGetAsText(void)
{
	UINT uData = m_pData->ReadInteger(m_pItem);

	if( !IsMulti(uData) ) {

		return Format(uData);
		}

	return multiString;
	}

UINT CUITextASCII::OnSetAsText(CError &Error, CString Text)
{
	if( !IsMulti(Text) ) {

		UINT uData = Parse(Text);

		if( uData >= m_uMin && uData <= m_uMax ) {

			UINT uPrev = m_pData->ReadInteger(m_pItem);

			if( uData - uPrev ) {

				m_pData->WriteInteger(m_pItem, uData);

				return saveChange;
				}

			return saveSame;
			}

		Error.Set(IDS_THAT_IS_NOT);

		return saveError;
		}
	
	return saveSame;
	}

CString CUITextASCII::OnScrollData(CString Text, UINT uCode)
{
	if( !IsMulti(Text) ) {

		UINT uData = Parse(Text);

		if( uData == NOTHING ) {

			uData = m_uMin;
			}

		if( uCode == SB_TOP ) {

			uData = m_uMin;
			}

		if( uCode == SB_BOTTOM ) {

			uData = m_uMax;
			}

		if( uCode == SB_LINEDOWN || uCode == SB_PAGEDOWN ) {

			uData -= (uData > m_uMin);
			}

		if( uCode == SB_LINEUP || uCode == SB_PAGEUP ) {

			uData += (uData < m_uMax);
			}

		return Format(uData);
		}

	return Format(m_uMin);
	}

// Implementation

CString CUITextASCII::Format(UINT uData)
{
	CString Text;

	if( uData == 0x20 ) {

		return L"SPACE";
		}

	if( uData > 0x20 && uData < 0x7F ) {

		Text.Printf(L"0x%2.2X (%c)", uData, uData);

		return Text;
		}

	if( uData > 0x00 && uData < 0x1B ) {

		Text.Printf(L"^%c", uData + 64);
		}
	else
		Text.Printf(L"0x%2.2X", uData);

	for( UINT n = 0; n < elements(m_Lookup); n++ ) {

		if( m_Lookup[n].c == uData ) {

			Text += CFormat(L" (%1)", m_Lookup[n].n);
			}
		}

	return Text;
	}

UINT CUITextASCII::Parse(PCTXT pText)
{
	CString Text = pText;

	CString Word = Text.StripToken(' ');

	if( Word.GetLength() == 1 ) {

		return Word[0];
		}

	if( Word.GetLength() == 2 && Word[0] == L'^' ) {

		UINT c = wtoupper(Word[1]);

		if( c >= 'A' && c <= 'Z' ) {

			return c - 64;
			}

		return NOTHING;
		}

	if( Word.StartsWith(L"0x") ) {

		return wcstoul(Word.Mid(2), NULL, 16);
		}

	for( UINT n = 0; n < elements(m_Lookup); n++ ) {

		if( Word == m_Lookup[n].n ) {

			return m_Lookup[n].c;
			}
		}

	return NOTHING;
	}

// End of File
