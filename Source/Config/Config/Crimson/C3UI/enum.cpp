
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Enumerated
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnum, CUITextElement);

// Constructor

CUITextEnum::CUITextEnum(void)
{
	m_uFlags = textEnum | textEdit | textScroll;

	m_uMask  = NOTHING;
	}

// Attributes

BOOL CUITextEnum::IsDataValid(UINT uData) const
{
	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_Enum[n].m_Data == uData ) {

			if( m_uMask & (1 << n) ) {

				return TRUE;
				}

			return FALSE;
			}
		}

	return FALSE;
	}

// Operations

void CUITextEnum::SetMask(UINT uMask)
{
	m_uMask  = uMask;
	
	m_uFlags = m_uFlags | textRefresh;
	}

void CUITextEnum::RebindUI(CStringArray List)
{
	m_Enum.Empty();

	if( List.IsEmpty() ) {

		OnBind();

		return;
		}

	CUITextElement::OnBind();

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		if( !List[n].IsEmpty() ) {

			CEntry Enum;

			Enum.m_Data = n;

			Enum.m_Text = List[n];

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}
		}
	}
	
// Overridables

void CUITextEnum::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	UINT c = GetFormat().Tokenize(List, '|');

	for( UINT n = 0; n < c; n++ ) {

		if( !List[n].IsEmpty() ) {

			BOOL fRaw = (List[n][0] == L'`');

			CEntry Enum;

			if( !fRaw && List[n].Count('=') ) {

				CString Tmp = List[n];

				Enum.m_Text = Tmp.StripToken('=');

				Enum.m_Data = watoi(Tmp);
				}
			else {
				CString Tmp = List[n];

				if( fRaw ) {

					Tmp.StripToken('`');
					}

				Enum.m_Text = Tmp;

				Enum.m_Data = n;
				}

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}
		}
	}

CString CUITextEnum::OnGetAsText(void)
{
	UINT d = m_pData->ReadInteger(m_pItem);

	if( !IsMulti(d) ) {

		UINT c = m_Enum.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			if( m_Enum[n].m_Data == d ) {

				return m_Enum[n].m_Text;
				}
			}

		return L"";
		}

	return multiString;
	}

UINT CUITextEnum::OnSetAsText(CError &Error, CString Text)
{
	if( !IsMulti(Text) ) {

		UINT c = m_Enum.GetCount();

		if( m_uFlags & textAllowWAS ) {

			if( Text.StartsWith(L"WAS ") ) {

				Text = Text.Mid(4);
				}
			}

		for( UINT n = 0; n < c; n++ ) {

			if( m_Enum[n].m_Text == Text ) {

				UINT uPrev = m_pData->ReadInteger(m_pItem);

				UINT uData = m_Enum[n].m_Data;

				if( uData - uPrev ) {

					m_pData->WriteInteger(m_pItem, uData);

					return saveChange;
					}

				return saveSame;
				}
			}

		Error.Printf(IDS_IS_NOT, Text);

		return saveError;
		}

	return saveSame;
	}

CString CUITextEnum::OnScrollData(CString Text, UINT uCode)
{
	if( !IsMulti(Text) ) {

		UINT c = m_Enum.GetCount();

		if( uCode == SB_TOP ) {

			return m_Enum[0].m_Text;
			}

		if( uCode == SB_BOTTOM ) {

			return m_Enum[c-1].m_Text;
			}

		for( UINT n = 0; n < c; n++ ) {

			if( m_Enum[n].m_Text == Text ) {

				if( uCode == SB_LINEDOWN || uCode == SB_PAGEDOWN ) {

					UINT i = n;

					while( i++ < c - 1 ) {

						if( m_uMask & (1 << i) ) {

							return m_Enum[i].m_Text;
							}
						}
					
					return m_Enum[n].m_Text;
					}

				if( uCode == SB_LINEUP || uCode == SB_PAGEUP ) {

					UINT i = n;

					while( i-- > 0 ) {

						if( m_uMask & (1 << i) ) {

							return m_Enum[i].m_Text;
							}
						}
					
					return m_Enum[n].m_Text;
					}
				}
			}	

		return Text;
		}

	return m_Enum[0].m_Text;
	}

BOOL CUITextEnum::OnEnumValues(CStringArray &List)
{
	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_uMask & (1 << n) ) {

			List.Append(m_Enum[n].m_Text);
			}
		}

	return TRUE;
	}

// Implementation

void CUITextEnum::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Enumerated with Pick
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnumPick, CUITextEnum);

// Constructor

CUITextEnumPick::CUITextEnumPick(void)
{
	m_uFlags = textEnum | textExpand;
	}

// Overridables

BOOL CUITextEnumPick::OnExpand(CWnd &Wnd)
{
	CPickDialog Dialog(this, GetAsText());

	if( Dialog.Execute(Wnd) ) {
		
		SetAsText(CError(FALSE), Dialog.GetData());
		
		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Pick Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CPickDialog, CStdDialog);
		
// Constructors

CPickDialog::CPickDialog(CUITextElement *pText, CString Data)
{
	m_pText = pText;

	m_Data  = Data;

	SetName(L"PickDialog");
	}

// Attributes

CString CPickDialog::GetData(void) const
{
	return m_Data;
	}
		
// Message Map

AfxMessageMap(CPickDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(100, LBN_DBLCLK, OnDblClk)
	
	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CPickDialog)
	};

// Message Handlers

BOOL CPickDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(CPrintf(GetWindowText(), m_pText->GetLabel()));

	m_pText->EnumValues(m_DataText);

	CListBox &List = (CListBox &) GetDlgItem(100);

	List.AddStrings(m_DataText);

	List.SelectString(NOTHING, m_Data);

	return TRUE;
	}
	
// Notification Handlers

void CPickDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

// Command Handlers

BOOL CPickDialog::OnCommandOK(UINT uID)
{
	CListBox &List = (CListBox &) GetDlgItem(100);

	m_Data = m_DataText[List.GetCurSel()];

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CPickDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Enumerated Integer
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnumInt, CUITextEnum);

// Constructor

CUITextEnumInt::CUITextEnumInt(void)
{
	m_uFlags = textEnum | textEdit;

	m_uMin   = 0;

	m_uMax   = 60000;
	}

// Overridables

void CUITextEnumInt::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	UINT c = GetFormat().Tokenize(List, '|');

	if( c >= 2 ) {

		m_uMin = watoi(List[0]);

		m_uMax = watoi(List[1]);

		for( UINT n = 2; n < c; n++ ) {

			CEntry Enum;

			Enum.m_Data = watoi(List[n]);

			Enum.m_Text = List[n];

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}
		}
	}

CString CUITextEnumInt::OnGetAsText(void)
{
	return CPrintf(L"%u", m_pData->ReadInteger(m_pItem));
	}

UINT CUITextEnumInt::OnSetAsText(CError &Error, CString Text)
{
	UINT uPrev = m_pData->ReadInteger(m_pItem);

	UINT uData = watoi(Text);

	if( uData - uPrev ) {

		if( uData < m_uMin || uData > m_uMax ) {

			Error.Format(IDS_THIS_PROPERTY_1, m_uMin, m_uMax);

			return saveError;
			}

		m_pData->WriteInteger(m_pItem, uData);

		return saveChange;
		}

	return saveSame;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Indexed Integer
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnumIndexed, CUITextEnum);

// Constructor

CUITextEnumIndexed::CUITextEnumIndexed(void)
{
	m_uFlags = textEnum;
	}

// Overridables

void CUITextEnumIndexed::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	UINT c = GetFormat().Tokenize(List, '|');

	for( UINT n = 0; n < c; n+=2 ) {

		CEntry Enum;

		Enum.m_Data = watoi(List[n]);

		Enum.m_Text = List[n+1];

		// cppcheck-suppress uninitStructMember

		m_Enum.Append(Enum);
		}
	}

// End of File
