
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// User Interface View Windows
//

// Dynamic Class

AfxImplementDynamicClass(CUIViewWnd, CDialogViewWnd);

// Static Data

BOOL CUIViewWnd::m_fStatus = FALSE;

BOOL CUIViewWnd::m_fFirst  = TRUE;

// Constructor

CUIViewWnd::CUIViewWnd(void) : CUIStdHost(this, this)
{
	m_pForm      = NULL;

	m_pSchema    = NULL;

	m_pToolTip   = NULL;

	m_uToolMode  = 0;

	m_uToolShow  = 0;

	m_uMinID     = 1000;

	m_uMaxID     = 1000;

	m_fValid     = TRUE;

	m_pFocus     = NULL;

	m_uFocus     = NOTHING;

	m_fEditCmd   = FALSE;

	m_fEditMul   = FALSE;

	m_fExecCmd   = FALSE;

	m_fExecMul   = FALSE;

	m_fRemake    = FALSE;

	m_fRead      = FALSE;

	m_fHasUndo   = TRUE;

	m_fNoUndo    = FALSE;

	m_fRecycle   = TRUE;

	m_fScroll    = TRUE;
	}

// Destructor

CUIViewWnd::~CUIViewWnd(void)
{
	KillUI();
	}

// Attributes

CRect CUIViewWnd::GetBorder(void) const
{
	return m_Border;
	}

CRect CUIViewWnd::GetUIRect(void) const
{
	CRect Rect = GetClientRect();

	Rect.left   += m_Border.left;
	Rect.right  -= m_Border.right;
	Rect.top    += m_Border.top;
	Rect.bottom -= m_Border.bottom;

	Rect.right  -= 20; /* Scroll Bar!!!! */

	return Rect;
	}

// UI Attributes

CSize CUIViewWnd::GetMinSize(void) const
{
	return m_pForm->GetMinSize();
	}

CSize CUIViewWnd::GetMaxSize(void) const
{
	return m_pForm->GetMaxSize();
	}

// UI Operations

void CUIViewWnd::CreateUI(void)
{
	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CUIElement *pUI = m_UIList[n];

		UINT        uID = m_uMaxID;

		pUI->CreateUI(ThisObject, m_uMaxID);

		while( uID < m_uMaxID ) {

			m_UICtrl.Insert(uID, n);

			uID++;
			}

		if( m_fRead ) {

			CUITextElement *pText = pUI->GetText();

			if( pText ) {
				
				pText->SetReadOnly();
				}
			}

		pUI->ShowUI(m_fValid);
		}
	}

void CUIViewWnd::LayoutUI(void)
{
	int  nExtra  = m_fScroll ? 60000 : 0;

	BOOL fScroll = m_fScroll;

	for(;;) {

		try {
			CClientDC DC(ThisObject);

			m_pForm->Prepare(DC);

			if( fScroll ) {

				m_UIRect      = GetUIRect();

				CRect Shift   = m_UIRect;

				Shift.top    -= m_nScroll;
				
				Shift.bottom += nExtra;

				m_pForm->SetRect(Shift);

				int xSize = m_UIRect.cx();

				int ySize = m_pForm->GetMinSize().cy;

				m_Active  = CRect(Shift.GetTopLeft(), CSize(xSize, ySize));

				if( ySize > m_UIRect.cy() ) {

					m_pScroll->SetScrollRange(0, ySize, FALSE);

					m_pScroll->SetPageSize(m_UIRect.cy(), FALSE);

					m_pScroll->SetScrollPos(m_nScroll, TRUE);

					m_pScroll->ShowWindow(SW_SHOW);
					}
				else
					m_pScroll->ShowWindow(SW_HIDE);
				}
			else {
				m_UIRect    = GetUIRect();

				m_pForm->SetRect(m_UIRect);

				m_pScroll->ShowWindow(SW_HIDE);
				}

			if( !m_fValid ) {

				ShowUI(TRUE);

				m_fValid = TRUE;
				}

			return;
			}

		catch(CUserException const &) {

			if( !fScroll ) {

				int ySize = m_UIRect.cy() - m_nScroll;

				int yNeed = m_pForm->GetMinSize().cy;

				fScroll = TRUE;

				nExtra  = yNeed - ySize;

				continue;
				}

			if( m_fValid ) {

				ShowUI(FALSE);
				
				m_fValid = FALSE;
				}

			m_pScroll->ShowWindow(SW_HIDE);

			return;
			}
		}
	}

void CUIViewWnd::PositionUI(void)
{
	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		m_UIList[n]->PositionUI();
		}
	}

void CUIViewWnd::OffsetUI(void)
{
	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		m_UIList[n]->OffsetUI(CSize(0, -m_nScroll));
		}
	}

// IUICreate

void CUIViewWnd::StartPage(UINT uCols)
{
	m_pForm	     = New CLayFormGrid(uCols);

	m_pGrid	     = NULL;

	m_pSave	     = NULL;

	m_uFormCols  = uCols;

	m_uFormCount = 0;
	}

void CUIViewWnd::ResetPage(UINT uCols)
{
	}

void CUIViewWnd::StartOverlay(void)
{
	if( !m_pSave ) {

		m_pSave = m_pForm;

		m_pForm = New CLayFormOverlay;

		return;
		}

	UIError(L"cannot nest overlays");
	}

void CUIViewWnd::StartGroup(UINT uCols)
{
	StartGroup(L"", uCols, FALSE);
	}

void CUIViewWnd::StartGroup(PCTXT pName, UINT uCols)
{
	StartGroup(pName, uCols, FALSE);
	}

void CUIViewWnd::StartGroup(PCTXT pName, UINT uCols, BOOL fEqual)
{
	if( !m_pGrid ) {

		m_GridName   = pName;

		m_pGrid	     = fEqual					      ?
			       (CLayFormation *) New CLayFormEqual(2 * uCols) :
			       (CLayFormation *) New CLayFormGrid (2 * uCols) ;

		m_uGridCols  = uCols;

		m_uGridCount = 0;

		m_fTable     = FALSE;

		return;
		}

	UIError(L"cannot nest groups");
	}

void CUIViewWnd::StartTable(PCTXT pName, UINT uCols)
{
	if( !m_pGrid ) {

		m_GridName   = pName;

		m_pGrid	     = New CLayFormGrid(2 * ++uCols);

		m_uGridCols  = uCols;

		m_uGridCount = 0;

		m_fTable     = TRUE;

		AddColHead(L"");

		return;
		}

	UIError(L"cannot nest tables");
	}

void CUIViewWnd::AddColHeadExtraRow(void)
{
	m_uGridCount = 0;

	AddColHead(L"");
	}

void CUIViewWnd::AddColHead(PCTXT pName)
{
	if( m_pGrid && m_fTable ) {

		if( m_uGridCount < m_uGridCols ) {

			CPrintf Tag  = CPrintf(L"ColLabel%u", m_uGridCols);

			CPrintf Name = CPrintf(L"%s:", pName);

			CUIElement *pUI = New CUIHeading(Tag, pName);

			pUI->LayoutUI(m_pGrid);

			m_UIList.Append(pUI);

			m_UIItem.Append(L"root");

			AddToDict(pUI);

			m_uGridCount++;

			return;
			}

		UIError(L"too many column headings");

		return;
		}

	UIError(L"column heading outside table");
	}

void CUIViewWnd::AddRowHead(PCTXT pName)
{
	if( m_pGrid && m_fTable ) {

		if( m_uGridCount % m_uGridCols == 0 ) {

			UINT    uRow = m_uGridCount / m_uGridCols - 1;

			CPrintf Tag  = CPrintf(L"RowLabel%u", uRow);

			CPrintf Name = CPrintf(L"%s:", pName);

			CUIElement *pUI = New CUIHeading(Tag, Name);

			pUI->LayoutUI(m_pGrid);

			m_UIList.Append(pUI);

			m_UIItem.Append(L"root");

			AddToDict(pUI);

			m_uGridCount++;

			return;
			}

		UIError(L"row heading must be at start of row");

		return;
		}

	UIError(L"row heading outside table");
	}

BOOL CUIViewWnd::AddUI(CItem *pItem, CString Object, CString Tag)
{
	if( !Tag.IsEmpty() ) {

		if( pItem = GetObject(pItem, Object) ) {

			LoadSchema(pItem);

			CUIData const *pUIData = m_pSchema->GetUIData(Tag);

			if( !pUIData ) {

				UIError(L"cannot find tag %s in ui schema", Tag);

				return FALSE;
				}

			return AddUI(pItem, Object, pUIData);
			}

		return FALSE;
		}

	m_pGrid->AddItem(New CLayItem(TRUE));

	m_pGrid->AddItem(New CLayItem(TRUE));

	m_uGridCount++;

	return TRUE;
	}

BOOL CUIViewWnd::AddUI(CItem *pItem, CString Object, CUIData const *pUIData)
{
	if( m_pGrid ) {

		CLASS Class;

		if( (Class = pUIData->m_ClassUI) ) {

			CUIElement *pUI = AfxNewObject(CUIElement, Class);

			if( pUI ) {

				if( pUI->Bind(pItem, pUIData, m_fTable) ) {

					pUI->LayoutUI(m_pGrid);

					m_UIList.Append(pUI);

					m_UIItem.Append(Object);

					AddToDict(pUI);

					m_uGridCount++;

					return TRUE;
					}

				UIError(L"cannot bind UI element to %s", pUIData->m_Tag);

				return FALSE;
				}

			UIError(L"cannot create UI element of class %s", Class->GetClassName());

			return FALSE;
			}

		UIError(L"null UI element class for %s", pUIData->m_Label);

		return FALSE;
		}

	UIError(L"no table or group for UI element");

	return FALSE;
	}

void CUIViewWnd::AddButton(PCTXT pName, UINT uID)
{
	AddButton(pName, L"", uID);
	}

void CUIViewWnd::AddButton(PCTXT pName, PCTXT pTip, UINT uID)
{
	if( *pName ) {

		AddElement(New CUIButton(pName, pTip, uID));
		}
	else {
		m_pGrid->AddItem(New CLayItem(TRUE));

		m_pGrid->AddItem(New CLayItem(TRUE));

		m_uGridCount += 2;
		}
	}

void CUIViewWnd::AddButton(PCTXT pName, PCTXT pTag)
{
	AddButton(pName, L"", pTag);
	}

void CUIViewWnd::AddButton(PCTXT pName, PCTXT pTip, PCTXT pTag)
{
	if( *pName ) {

		CUIElement *pUI = New CUIButton(pName, pTip, pTag);

		AddElement(pUI);

		AddToDict(pUI);
		}
	else {
		m_pGrid->AddItem(New CLayItem(TRUE));

		m_pGrid->AddItem(New CLayItem(TRUE));

		m_uGridCount += 2;
		}
	}

void CUIViewWnd::AddNarrative(PCTXT pText)
{
	AddElement(New CUIHeading(L"", pText));

	AddElement(New CUIHeading(L"", L""));
	}

void CUIViewWnd::AddElement(CUIElement *pUI)
{
	if( m_pGrid ) {

		pUI->LayoutUI(m_pGrid);

		m_UIList.Append(pUI);

		m_UIItem.Append(L"");

		m_pGrid->AddItem(New CLayItem(TRUE));

		m_uGridCount += 2;
		}
	else {
		pUI->LayoutUI(m_pForm);

		m_UIList.Append(pUI);

		m_UIItem.Append(L"");

		m_uFormCount += 1;
		}
	}

void CUIViewWnd::EndTable(void)
{
	if( m_fTable ) {

		EndGroup(FALSE);

		return;
		}

	UIError(L"mismatched end of table");
	}

void CUIViewWnd::EndGroup(BOOL fExpand)
{
	if( m_pGrid ) {

		while( m_uGridCount % m_uGridCols ) {

			m_pGrid->AddItem(New CLayItem(fExpand));

			m_pGrid->AddItem(New CLayItem(fExpand));

			m_uGridCount++;
			}

		if( !m_GridName.IsEmpty() ) {

			CUIElement *pUI = New CUIGroup(m_GridName, m_pGrid);

			pUI->LayoutUI(m_pForm);

			m_UIList.Append(pUI);

			m_UIItem.Append(L"");
			}
		else
			m_pForm->AddItem(m_pGrid);

		m_pGrid = NULL;

		m_uFormCount++;

		return;
		}

	UIError(L"mismatched end of group");
	}

void CUIViewWnd::EndOverlay(void)
{
	if( m_pSave ) {

		m_pSave->AddItem(m_pForm);

		m_pForm = m_pSave;

		m_pSave = NULL;

		return;
		}

	UIError(L"mismatched end of overlay");
	}

void CUIViewWnd::EndPage(BOOL fExpand)
{
	if( !m_pGrid ) {

		while( m_uFormCount % m_uFormCols ) {

			m_pForm->AddItem(New CLayItem(fExpand));

			m_uFormCount++;
			}

		FreeSchema();

		return;
		}

	UIError(L"cannot end page with group active");
	}

void CUIViewWnd::SetBorder(CRect const &Border)
{
	m_Border = Border;
	}

void CUIViewWnd::SetBorder(int nBorder)
{
	m_Border.Set(nBorder, nBorder, nBorder, nBorder);
	}

void CUIViewWnd::NoRecycle(void)
{
	m_fRecycle = FALSE;
	}

void CUIViewWnd::NoScroll(void)
{
	m_fScroll = FALSE;
	}

// IUnknown

HRESULT CUIViewWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CUIViewWnd::AddRef(void)
{
	return 1;
	}

ULONG CUIViewWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CUIViewWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CUIViewWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CUIViewWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	return S_OK;
	}

HRESULT CUIViewWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// IUICore

CUITextElement * CUIViewWnd::OnFindUI(CLASS Class, CItem *pItem, PCTXT pTag)
{
	INDEX n = m_UIDict.FindName(GetLongName(pItem, pTag));

	if( !m_UIDict.Failed(n) ) {

		CUIElement *pUI = m_UIDict.GetData(n);

		if( pUI ) {

			CUITextElement *pText = pUI->GetText();

			if( !Class || pText->IsKindOf(Class) ) {

				return pText;
				}
			}

		return NULL;
		}

	return NULL;
	}

BOOL CUIViewWnd::OnShowUI(CItem *pItem, PCTXT pTag, BOOL fShow)
{
	if( pTag ) {

		INDEX n = m_UIDict.FindName(GetLongName(pItem, pTag));

		if( !m_UIDict.Failed(n) ) {

			CUIElement *pUI = m_UIDict.GetData(n);

			pUI->ShowUI(fShow);

			return TRUE;
			}

		return FALSE;
		}
	else {
		UINT c = m_UIList.GetCount();

		UINT h = 0;

		for( UINT n = 0; n < c; n++ ) {

			CUIElement *pUI = m_UIList[n];

			if( !pItem || pUI->GetItem() == pItem ) {

				pUI->ShowUI(fShow);

				h++;
				}
			}

		return h ? TRUE : FALSE;
		}
	}

BOOL CUIViewWnd::OnEnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable)
{
	if( pTag ) {
		
		INDEX n = m_UIDict.FindName(GetLongName(pItem, pTag));

		if( !m_UIDict.Failed(n) ) {

			CUIElement *pUI = m_UIDict.GetData(n);

			pUI->EnableUI(fEnable);

			return TRUE;
			}

		return FALSE;
		}
	else {
		UINT c = m_UIList.GetCount();

		UINT h = 0;

		for( UINT n = 0; n < c; n++ ) {

			CUIElement *pUI = m_UIList[n];

			if( !pItem || pUI->GetItem() == pItem ) {

				pUI->EnableUI(fEnable);

				h++;
				}
			}

		return h ? TRUE : FALSE;
		}
	}

BOOL CUIViewWnd::OnUpdateUI(CItem *pItem, PCTXT pTag)
{
	if( pTag ) {

		INDEX n = m_UIDict.FindName(GetLongName(pItem, pTag));

		if( !m_UIDict.Failed(n) ) {

			CUIElement *pUI = m_UIDict.GetData(n);

			if( m_fEditCmd ) {

				CheckForChange(pUI, TRUE);
				}

			pUI->UpdateUI();

			return TRUE;
			}

		return FALSE;
		}
	else {
		UINT c = m_UIList.GetCount();

		UINT h = 0;

		for( UINT n = 0; n < c; n++ ) {

			CUIElement *pUI = m_UIList[n];

			if( !pItem || pUI->GetItem() == pItem ) {

				CString Object = m_UIItem[n];
				
				CItem * pTest  = GetObject(m_pItem, Object);

				AfxAssert(!pItem || pTest == pItem);

				if( m_fEditCmd ) {

					CheckForChange(pUI, TRUE);
					}

				pUI->UpdateUI();

				h++;
				}
			}

		return h ? TRUE : FALSE;
		}
	}

void CUIViewWnd::OnRemakeUI(void)
{
	if( !m_fExecMul && !m_fEditMul  ) {

		m_uMinID = m_uMaxID + 1;
		
		m_uMaxID = m_uMinID + 0;

		KillUI();

		CWnd *pWnd = GetWindowPtr(GW_CHILD);

		while( pWnd->IsWindow() ) {

			// This can cause problems as the window
			// still exists and can try to send messages
			// to a UI element that had already died...

			pWnd->DestroyWindow(FALSE);

			pWnd = pWnd->GetWindowPtr(GW_HWNDNEXT);
			}

		MakeUI();

		UINT n;

		for( n = 1; n <= 2; n++ ) {

			CWnd &Wnd = GetParent(n);

			if( Wnd.IsKindOf(AfxRuntimeClass(CItemDialog)) ) {

				if( n > 1 ) {

					CWnd &Wnd = GetParent(n - 1);

					if( Wnd.IsKindOf(AfxRuntimeClass(CUIItemMultiWnd)) ) {

						CUIItemMultiWnd &View = (CUIItemMultiWnd &) Wnd;

						View.RemakeRest(this);
						}
					}

				CItemDialog &Dlg = (CItemDialog &) Wnd;

				Dlg.Resize();

				break;
				}
			}

		if( n == 3 ) {

			Invalidate(FALSE);

			UpdateWindow();
			}

		SetNavFocus(m_NavTag);

		m_fRemake = FALSE;

		return;
		}

	m_fRemake = TRUE;
	}

BOOL CUIViewWnd::OnHasUndo(void)
{
	return m_fHasUndo;
	}

BOOL CUIViewWnd::OnInReplay(void)
{
	return m_fExecCmd || m_fExecMul;
	}

void CUIViewWnd::OnExecExtraCmd(CCmd *pCmd)
{
	if( !InReplay() ) {

		StartMulti();

		pCmd->m_Menu = m_EditMenu;

		pCmd->m_Item = m_EditItem;

		m_System.ExecCmd(pCmd);

		return;
		}

	OnExec(pCmd);

	delete pCmd;
	}

void CUIViewWnd::OnSaveExtraCmd(CCmd *pCmd)
{
	if( !InReplay() ) {

		StartMulti();

		pCmd->m_Menu = m_EditMenu;

		pCmd->m_Item = m_EditItem;

		m_System.SaveCmd(pCmd);

		return;
		}

	AfxAssert(FALSE);
	}

void CUIViewWnd::OnSendUpdate(UINT uType)
{
	m_System.ItemUpdated(m_pItem, uType);
	}

BOOL CUIViewWnd::KillUndoList(void)
{
	if( m_fHasUndo ) {

		if( m_System.KillUndoList() ) {

			m_fNoUndo = TRUE;

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

// UI Update

void CUIViewWnd::OnUICreate(void)
{
	}

void CUIViewWnd::OnUIChange(CItem *pItem, CString Tag)
{
	}

// Overribables

void CUIViewWnd::OnAttach(void)
{
	m_pItem = (CMetaItem *) CViewWnd::m_pItem;

	m_fRead = m_pItem->GetDatabase()->IsReadOnly();

	if( m_hWnd ) {

		AfxAssert(m_fRecycle);

		m_UIDict.Empty();

		m_UIHist.Empty();

		BuildDict();

		BuildHistory();

		SendEnable();
		}
	}

CString CUIViewWnd::OnGetNavPos(void)
{
	CString Nav = m_pItem->GetFixedPath();

	if( m_pFocus ) {

		if( m_uFocus < NOTHING ) {

			CUIElement *pUI = m_UIList[m_uFocus];

			if( pUI->GetItem() ) {

				Nav = pUI->GetFixed();
				}

			Nav += L":";

			Nav += pUI->GetTag();
			}
		}

	return Nav;
	}

BOOL CUIViewWnd::OnNavigate(CString const &Nav)
{
	if( !Nav.IsEmpty() ) {

		return SetNavFocus(Nav);
		}

	return FALSE;
	}

void CUIViewWnd::OnExec(CCmd *pCmd)
{
	if( m_fHasUndo ) {

		m_fExecCmd = TRUE;

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMulti)) ) {

			if( m_fExecMul ) {
				
				m_fExecMul = FALSE;

				if( m_fRemake ) {

					RemakeUI();
					}
				}
			else
				m_fExecMul = TRUE;
			}

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdSubItem)) ) {

			CCmdSubItem *pSub = (CCmdSubItem *) pCmd;

			pSub->Exec(m_pItem);

			SendChange(pSub);
			}

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdGlobalItem)) ) {

			CCmdGlobalItem *pGlobal = (CCmdGlobalItem *) pCmd;

			pGlobal->Exec(m_pItem);
			}

		if( pCmd->IsKindOf(AfxRuntimeClass(CUIViewWnd::CCmdEdit)) ) {

			CCmdEdit *pEdit = (CCmdEdit *) pCmd;

			OnExecEdit(pEdit, pEdit->m_Data);
			}

		m_fExecCmd = FALSE;

		return;
		}

	UIError(L"unexpected redo command");
	}

void CUIViewWnd::OnUndo(CCmd *pCmd)
{
	if( m_fHasUndo ) {
		
		m_fExecCmd = TRUE;

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMulti)) ) {

			if( m_fExecMul ) {
				
				m_fExecMul = FALSE;

				if( m_fRemake ) {

					RemakeUI();
					}
				}
			else
				m_fExecMul = TRUE;
			}

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdSubItem)) ) {

			CCmdSubItem *pSub = (CCmdSubItem *) pCmd;

			pSub->Undo(m_pItem);

			SendChange(pSub);
			}

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdGlobalItem)) ) {

			CCmdGlobalItem *pGlobal = (CCmdGlobalItem *) pCmd;

			pGlobal->Undo(m_pItem);
			}

		if( pCmd->IsKindOf(AfxRuntimeClass(CUIViewWnd::CCmdEdit)) ) {

			CCmdEdit *pEdit = (CCmdEdit *) pCmd;

			OnExecEdit(pEdit, pEdit->m_Prev);
			}

		m_fExecCmd = FALSE;

		return;
		}

	UIError(L"unexpected undo command");
	}

// Message Map

AfxMessageMap(CUIViewWnd, CDialogViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
 	AfxDispatchMessage(WM_CANCELMODE)
 	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_GETMINMAXINFO)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_NOTIFY)
 	AfxDispatchMessage(WM_FOCUSNOTIFY)
 	AfxDispatchMessage(WM_SETFOCUS)
 	AfxDispatchMessage(WM_SHOWWINDOW)
 	AfxDispatchMessage(WM_MOUSEWHEEL)
 	AfxDispatchMessage(WM_HSCROLL)
 	AfxDispatchMessage(WM_VSCROLL)
	AfxDispatchMessage(WM_CTLCOLORSTATIC)
	AfxDispatchMessage(WM_CTLCOLORBTN)

	AfxDispatchCommand(IDM_VIEW_REFRESH, OnViewRefresh)

	AfxDispatchControlType(IDM_HELP, OnBalloonControl)
	AfxDispatchCommandType(IDM_HELP, OnBalloonCommand)

	AfxDispatchNotify(0, TTN_SHOW, OnToolShow)

	AfxMessageEnd(CUIViewWnd)
	};

// Message Handlers

void CUIViewWnd::OnPostCreate(void)
{
	m_System.Bind(this);

	if( GetParent(AfxRuntimeClass(CDialog)).IsWindow() ) {
		
		m_fHasUndo = FALSE;
		}

	for( UINT n = 1; n <= 2; n++ ) {

		CWnd &Wnd = GetParent(n);

		if( Wnd.IsKindOf(AfxRuntimeClass(CItemDialog)) ) {

			if( ((CItemDialog &) Wnd).IsReadOnly() ) {

				m_fRead = TRUE;
				}

			break;
			}
		}
	
	m_DropHelp.Bind(m_hWnd, this);

	MakeUI();
	}

void CUIViewWnd::OnCancelMode(void)
{
	HideToolTip();
	}

void CUIViewWnd::OnSetCurrent(BOOL fCurrent)
{
	if( !fCurrent ) {

		if( m_fStatus ) {

			afxThread->SetStatusText(L"");

			m_fStatus = FALSE;
			}
		
		HideToolTip();
		}
	}

void CUIViewWnd::OnDestroy(void)
{
	DestroyToolTip();
	}

void CUIViewWnd::OnPaint(void)
{
	OnPaint(CPaintDC(ThisObject));
	}

void CUIViewWnd::OnPaint(CDC &DC)
{
	if( !m_fValid ) {

		DC.SetBkMode(TRANSPARENT);

		DC.SetTextColor(afxColor(BLACK));

		DC.Select(afxFont(Large));

		CString Text = IDS_UI_WIN_SMALL;

		CSize   Size = DC.GetTextExtent(Text);

		DC.TextOut(m_UIRect.GetTopLeft() + (m_UIRect.GetSize() - Size) / 2, Text);

		DC.Deselect();
		}
	else {
		if( !m_HelpText.IsEmpty() ) {

			DC.Select(afxFont(Large));

			DC.SetTextColor(afxColor(ButtonText));

			DC.SetBkColor(afxColor(TabFace));

			CSize Size = m_pForm->GetMaxSize();

			CRect Rect = m_UIRect;

			Rect.top   += Size.cy + 8;

			Rect.left  += 8;

			Rect.right -= 8;

			CRect Calc = Rect;

			DC.DrawText( m_HelpText,
				     Calc,
				     DT_WORDBREAK | DT_EXTERNALLEADING | DT_CALCRECT
				     );

			int n = Calc.GetHeight();

			Calc.bottom = Rect.bottom - 8;

			Calc.top    = Calc.bottom - n;

			if( Calc.top >= Rect.top ) {

				DC.DrawText( m_HelpText,
					     Calc,
					     DT_WORDBREAK | DT_EXTERNALLEADING
					     );

				DC.FrameRect(Calc + 4, afxBrush(BLACK));
				}

			DC.Deselect();
			}

		if( TRUE ) {

			CRect Rect = GetClientRect();

			UINT c = m_UIList.GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CUIElement *pUI = m_UIList[n];

				pUI->PaintUI(Rect, DC);
				}
			}
		}
	}

BOOL CUIViewWnd::OnEraseBkGnd(CDC &DC)
{
	if( m_pForm ) {

		CRect Rect = GetClientRect();

		DC.FillRect(Rect, afxBrush(TabFace));

		return TRUE;
		}

	return FALSE;
	}

void CUIViewWnd::OnSize(UINT uCode, CSize Size)
{
	if( m_pForm ) {

		CRect Rect = GetClientRect();

		Rect.left  = Rect.right - 18;

		m_pScroll->MoveWindow(Rect, FALSE);

		LayoutUI();

		PositionUI();

		HideToolTip();

		MoveToolTips();

		if( !CheckScroll() ) {

			Scroll(0);
			}

		return;
		}
	}

void CUIViewWnd::OnGetMinMaxInfo(MINMAXINFO &Info)
{
	if( m_pForm ) {

		CSize Size1 = GetMinSize();
		CSize Size2 = GetMaxSize();

		Size1.cx += m_Border.left + m_Border.right;
		Size2.cx += m_Border.left + m_Border.right;

		Size1.cx += 20; /* Scroll Bar!!!! */
		Size2.cx += 20;

		Size1.cy += m_Border.top + m_Border.bottom;
		Size2.cy += m_Border.top + m_Border.bottom;

		Info.ptMinTrackSize = CPoint(Size1);
		Info.ptMaxTrackSize = CPoint(Size2);

		return;
		}
	}

BOOL CUIViewWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID >= 0x8000 ) {
	
		afxMainWnd->PostMessage(WM_COMMAND, uID);
			
		return TRUE;
		}

	if( uID == IDOK ) {

		if( m_pFocus ) {
		
			AfxNull(CWnd).SetFocus();

			SetDlgFocus(*m_pFocus);
			}

		return TRUE;
		}

	if( uID >= m_uMinID && uID < m_uMaxID ) {

		INDEX n = m_UICtrl.FindName(uID);

		if( !m_UICtrl.Failed(n) ) {

			CUIElement *pUI = m_UIList[m_UICtrl[n]];

			if( pUI->NotifyUI(uID, uNotify) ) {

				SendChange(pUI);

				return TRUE;
				}

			return FALSE;
			}
	
		return FALSE;
		}

	return FALSE;
	}

BOOL CUIViewWnd::OnNotify(UINT uID, NMHDR &Info)
{
	if( uID >= m_uMinID && uID < m_uMaxID ) {

		INDEX n = m_UICtrl.FindName(uID);

		if( !m_UICtrl.Failed(n) ) {

			CUIElement *pUI = m_UIList[m_UICtrl[n]];

			if( pUI->NotifyUI(uID, Info) ) {

				SendChange(pUI);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CUIViewWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	SetCurrent(TRUE);

	m_pFocus = &Wnd;

	UINT  id = Wnd.GetID();

	INDEX np = m_UICtrl.FindName(id);

	if( m_UICtrl.Failed(np) ) {

		if( m_uFocus < NOTHING ) {

			if( m_fStatus ) {

				afxThread->SetStatusText(L"");

				m_fStatus = FALSE;
				}

			m_uFocus = NOTHING;

			m_NavTag.Empty();

			TrackToolTip(FALSE);
			}
		}
	else {
		if( m_uFocus != m_UICtrl[np] ) {

			m_uFocus	= m_UICtrl[np];

			CUIElement *pUI = m_UIList[m_uFocus];

			CUITextElement *pText = pUI->GetText();

			if( pText && pText->HasFlag(textStatus) ) {

				CString Status = pText->GetStatus();

				afxThread->SetStatusText(Status);

				m_fStatus = TRUE;
				}
			else {
				if( m_fStatus ) {

					afxThread->SetStatusText(L"");

					m_fStatus = FALSE;
					}
				}

			m_NavTag = pUI->GetTag();

			if( !m_fFirst ) {

				// NOTE -- We don't do this on the first field that
				// the app displays, as otherwise we can get in knots
				// with respect to the splash screen.

				TrackToolTip(FALSE);
				}

			m_fFirst = FALSE;
			}
		}

	CheckScroll();
	}

void CUIViewWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_pFocus ) {

		if( m_pFocus->IsWindowVisible() ) {

			SetDlgFocus(*m_pFocus);
			}

		return;
		}

	AfxCallDefProc();
	}

void CUIViewWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( !fShow ) {

		HideToolTip();

		return;
		}

	SendEnable();
	}

void CUIViewWnd::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
		}

	Scroll(nDelta / 5);
	}

void CUIViewWnd::OnHScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	OnScroll(uCode, nPos, Ctrl);
	}

void CUIViewWnd::OnVScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( m_pScroll == &Ctrl ) {

		switch( uCode ) {

			case SB_LINEUP:

				Scroll(+12);

				break;

			case SB_LINEDOWN:

				Scroll(-12);

				break;

			case SB_PAGEUP:

				Scroll(+m_UIRect.cy());

				break;

			case SB_PAGEDOWN:

				Scroll(-m_UIRect.cy());

				break;

			case SB_THUMBTRACK:

				Scroll(m_nScroll - nPos);

				break;
			}
		}

	OnScroll(uCode, nPos, Ctrl);
	}

HOBJ CUIViewWnd::OnCtlColorStatic(CDC &DC, CWnd &Wnd)
{
	if( Wnd.IsKindOf(AfxRuntimeClass(CEditCtrl)) ) {

		if( Wnd.GetWindowStyle() & ES_READONLY ) {

			DC.SetBkColor(afxColor(TabLock));

			return afxBrush(TabLock);
			}
		}

	DC.SetBkColor(afxColor(TabFace));

	return afxBrush(TabFace);
	}

HOBJ CUIViewWnd::OnCtlColorBtn(CDC &DC, CWnd &Wnd)
{
	DC.SetBkColor(afxColor(TabFace));

	return afxBrush(TabFace);
	}

// View Menu

BOOL CUIViewWnd::OnViewRefresh(UINT uID)
{
	RemakeUI();

	return TRUE;
	}

// Help Menu

BOOL CUIViewWnd::OnBalloonControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_HELP_BUTTON ) {

		Src.EnableItem(m_uToolMode == 0 && CToolTip::AreBalloonsPossible());

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIViewWnd::OnBalloonCommand(UINT uID)
{
	if( uID == IDM_HELP_BUTTON ) {

		if( m_uToolMode == 0 ) {

			if( !CToolTip::AreBalloonsEnabled() ) {

				if( !CToolTip::EnableBalloons(TRUE) ) {

					return TRUE;
					}
				}

			if( m_uToolShow ) {

				HideToolTip();
				}
			else
				TrackToolTip(TRUE);
			}

		return TRUE;
		}

	if( uID == IDM_HELP_BALLOON_MOD ) {

		DestroyToolTip();

		CreateToolTip();

		TrackToolTip(FALSE);

		return FALSE;
		}

	return FALSE;
	}

// Notification Handlers

UINT CUIViewWnd::OnToolShow(UINT uID, NMHDR &Info)
{
	CUIElement *pUI = m_UIList[uID - 1];

	if( pUI->IsVisible() ) {

		if( m_uToolMode == 1 ) {

			CRect  Rect = m_pToolTip->GetWindowRect();

			CRect  Item = pUI->GetRect();

			CPoint Pos  = GetCursorPos();

			BOOL fRight  = (Rect.left > Pos.x - 40);

			BOOL fBottom = (Rect.top  > Pos.y - 40);

			PlaceToolRect(Rect, Item, fRight, fBottom);

			m_pToolTip->MoveWindow(Rect, FALSE);
			}

		m_pToolTip->SetTitle(1, pUI->GetLabel());

		m_pToolTip->SetTipBkColor(afxColor(WHITE));

		return 1;
		}

	CRect Null;

	m_pToolTip->MoveWindow(Null, FALSE);

	return 1;
	}

void CUIViewWnd::OnScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	UINT uID = Ctrl.GetID();

	if( uID >= m_uMinID && uID < m_uMaxID ) {

		UINT c = m_UIList.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CUIElement *pUI = m_UIList[n];

			DWORD dwParam   = MAKELONG(nPos, uCode);

			if( pUI->NotifyUI(uID, dwParam) ) {

				SendChange(pUI);
				}
			}
		}
	}

// Schema Management

void CUIViewWnd::LoadSchema(CItem *pItem)
{
	if( m_pSchema ) {

		if( m_pSchema->CheckType(pItem) ) {

			return;
			}

		delete m_pSchema;
		}

	m_pSchema = New CUISchema(pItem);
	}

void CUIViewWnd::FreeSchema(void)
{
	if( m_pSchema ) {

		delete m_pSchema;

		m_pSchema = NULL;
		}
	}

// Sub-Item Location

CItem * CUIViewWnd::GetObject(CItem *pItem, CString Object)
{
	if( Object.GetLength() ) {
		
		if( Object.CompareN(L"root") ) {

			IDataAccess *pData = pItem->GetDataAccess(Object);

			if( pData ) {

				pItem = pData->GetObject(pItem);
				
				return pItem;
				}

			UIError(L"cannot find child object %s", Object);

			return NULL;
			}
		}

	return pItem;
	}

// Tag Naming

CString CUIViewWnd::GetLongName(CUIElement *pUI) const
{
	CString Name;

	if( !pUI->GetItem() ) {

		Name = m_pItem->GetFixedPath();
		}
	else
		Name = pUI->GetFixed();

	Name += L':';

	Name += pUI->GetTag();

	return Name;
	}

CString CUIViewWnd::GetLongName(CItem *pItem, CString Tag) const
{
	CString Name;

	if( pItem ) {

		Name += pItem->GetFixedPath();

		Name += L':';
		}

	Name += Tag;

	return Name;
	}

// Tool Tips

void CUIViewWnd::CreateToolTip(void)
{
	m_pToolTip    = New CUIToolTip;

	m_uToolMode   = ((CMainWnd *) afxMainWnd)->GetBalloonMode();

	HWND hWnd     = afxMainWnd->GetHandle();

	DWORD dwStyle = TTS_NOFADE    |
			TTS_NOANIMATE |
			TTS_NOPREFIX  |
			TTS_ALWAYSTIP |
			TTS_BALLOON;

	m_pToolTip->Create(hWnd, dwStyle);

	m_pToolTip->SetMaxTipWidth(320);

	m_pToolTip->SetTitle(1, L"XXX");

	m_pToolTip->SetDelayTime(TTDT_AUTOPOP, 30000);

	AddToolTips(m_uToolMode != 1);
	}

void CUIViewWnd::DestroyToolTip(void)
{
	if( m_pToolTip ) {

		HideToolTip();

		m_pToolTip->DestroyWindow(TRUE);

		m_pToolTip = NULL;
		}
	}

void CUIViewWnd::AddToolTips(BOOL fTrack)
{
	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CUIElement *pUI = m_UIList[n];

		CString     Tip = pUI->GetUIData().m_Tip;

		Tip.TrimBoth();

		if( !Tip.IsEmpty() ) {

			CToolInfo Info(m_hWnd, 1 + n, pUI->GetRect(), Tip, fTrack ? TTF_TRACK : TTF_SUBCLASS);

			m_pToolTip->AddTool(Info);
			}
		}
	}

void CUIViewWnd::PlaceToolRect(CRect &Rect, CRect const &Item, BOOL fRight, BOOL fBottom)
{
	if( fRight ) {

		if( fBottom ) {

			Rect.DragTopLeft   (Item.GetBottomLeft() + CSize(16, 0));
			}
		else
			Rect.DragBottomLeft(Item.GetTopLeft()    + CSize(16, 0));
		}
	else {
		if( fBottom ) {

			Rect.DragTopRight   (Item.GetBottomLeft() + CSize(32, 0));
			}
		else
			Rect.DragBottomRight(Item.GetTopLeft()    + CSize(32, 0));
		}

	ClientToScreen(Rect);
	}

void CUIViewWnd::TrackToolTip(BOOL fForce)
{
	if( m_pToolTip && IsCurrent() ) {
		
		if( fForce || m_uToolMode == 2 ) {

			if( m_uFocus < NOTHING ) {

				CUIElement *pUI = m_UIList[m_uFocus];

				if( !pUI->GetUIData().m_Tip.IsEmpty() ) {

					CRect  Rect = m_pFocus->GetWindowRect();

					CPoint Pos  = Rect.GetBottomLeft() + CSize(8, 0);

					HideToolTip();

					m_uToolShow = 1 + m_uFocus;

					m_pToolTip->TrackPosition(Pos);

					m_pToolTip->TrackActivate(TRUE, m_hWnd, m_uToolShow);

					return;
					}
				}
			}

		HideToolTip();
		}
	}

void CUIViewWnd::MoveToolTips(void)
{
	if( m_pToolTip ) {

		UINT c = m_UIList.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CUIElement *pUI = m_UIList[n];

			CString     Tip = pUI->GetUIData().m_Tip;

			if( !Tip.IsEmpty() ) {

				m_pToolTip->NewToolRect( m_hWnd,
							 1 + n,
							 pUI->GetRect()
							 );
				}
			}
		}
	}

void CUIViewWnd::HideToolTip(void)
{
	if( m_pToolTip ) {

		if( m_uToolMode == 1 ) {

			m_pToolTip->Pop();
			}
		else {
			if( m_uToolShow ) {

				m_pToolTip->TrackActivate(FALSE, m_hWnd, m_uToolShow);

				m_uToolShow = 0;
				}
			}
		}
	}

// Editing Command

BOOL CUIViewWnd::OnExecEdit(CCmdEdit *pEdit, CString Data)
{
	CString Base = pEdit->m_Item;

	CString Item = Base.StripToken(':');

	UINT    c    = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CUIElement * pUI   = m_UIList[n];

		CItem      * pItem = pUI->GetItem();

		CString      Tag   = pUI->GetTag();

		if( Tag == pEdit->m_Tag ) {

			if( pUI->GetFixed() == Item ) {

				CUITextElement *pText = pUI->GetText();

				if( pText ) {

					CError Error(FALSE);

					pText->SetAsText(Error, Data);

					if( Error.IsOkay() ) {

						CString Name = GetLongName(pUI);

						INDEX   Find = m_UIHist.FindName(Name);

						if( !m_UIHist.Failed(Find) ) {

							m_UIHist.SetData(Find, Data);

							pUI->UpdateUI();

							SendChange(pItem, Tag);

							return TRUE;
							}
		
						UIError(L"can't find property in history");

						return FALSE;
						}

					UIError(L"undo or redo failed to set property");

					return FALSE;
					}

				UIError(L"undo or redo could not get text object");

				return FALSE;
				}
			}
		}

	return FALSE;
	}

// Implementation

void CUIViewWnd::MakeUI(void)
{
	OnUICreate();

	m_nScroll = 0;

	m_pScroll = New CScrollBar;

	CRect Rect = GetClientRect();

	Rect.left  = Rect.right - 18;

	m_pScroll->Create(SBS_VERT, Rect, ThisObject, 0);

	if( m_pForm ) {

		BuildHistory();

		LayoutUI();

		CreateUI();

		CreateToolTip();

		SendEnable();

		return;
		}

	UIError(L"no form defined");
	}

void CUIViewWnd::KillUI(void)
{
	FreeSchema();

	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		delete m_UIList[n];
		}

	m_UIList.Empty();

	m_UIItem.Empty();

	m_UIDict.Empty();

	m_UIHist.Empty();

	m_UICtrl.Empty();
	
	DestroyToolTip();

	delete m_pForm;

	m_pFocus = NULL;

	m_uFocus = NOTHING;
	}

BOOL CUIViewWnd::SetNavFocus(CString Nav)
{
	UINT  uFind = Nav.Find(L'!');

	CString Src = Nav.Left(uFind);

	CString Loc = Nav.Mid(uFind+1);

	INDEX Index = m_UIDict.FindName(Src);

	if( !m_UIDict.Failed(Index) ) {

		CWnd *pWnd = NULL;

		if( m_UIDict.GetData(Index)->FindFocus(pWnd) ) {

			if( pWnd ) {

				if( IsCurrent() && afxMainWnd->IsActive() ) {

					pWnd->SetFocus();
					}
				else
					m_pFocus = pWnd;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUIViewWnd::StartMulti(void)
{
	if( !m_fEditMul ) {

		CCmd *pCmd = New CCmdMulti(m_EditItem, m_EditMenu, navOne);

		m_System.SaveCmd(pCmd);

		m_fEditMul = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIViewWnd::CloseMulti(void)
{
	if( m_fEditMul ) {

		CCmd *pCmd = New CCmdMulti(m_EditItem, m_EditMenu, navOne);

		m_System.SaveCmd(pCmd);

		m_fEditMul = FALSE;

		if( m_fRemake ) {

			RemakeUI();
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIViewWnd::CheckForChange(CUIElement *pUI, BOOL fUpdate)
{
	if( m_fNoUndo ) {

		return TRUE;
		}

	if( pUI->GetText() ) {

		CString Name = GetLongName(pUI);

		INDEX   Find = m_UIHist.FindName(Name);

		if( !m_UIHist.Failed(Find) ) {

			CString Prev = m_UIHist.GetData(Find);

			CString Data = pUI->GetText()->GetAsText();

			if( wstrcmp(Prev, Data) ) {

				m_UIHist.SetData(Find, Data);

				CCmd  * pCmd = m_System.GetLastCmd();

				CString Tag  = pUI->GetTag();
				
				CString Item = m_EditItem;

				if( fUpdate ) {

					StartMulti();

					Item  = Item.Left(Item.FindRev(':') + 1);

					Item += Tag;
					}
				else {
					if( !pUI->GetText()->HasFlag(textNoMerge) ) {

						if( pCmd ) {
							
							CCmdEdit *pEdit = (CCmdEdit *) pCmd;

							if( pEdit->IsKindOf(AfxRuntimeClass(CCmdEdit)) ) {

								if( pCmd->m_Item == m_EditItem ) {

									if( wstrcmp(pEdit->m_Prev, Data) ) {

										pEdit->m_Data = Data;

										return TRUE;
										}

									m_System.KillLastCmd();

									return TRUE;
									}
								}
							}
						}
					}

				pCmd = New CCmdEdit( m_EditMenu,
						     Item,
						     Tag,
						     Prev,
						     Data
						     );

				m_System.SaveCmd(pCmd);

				return TRUE;
				}

			return FALSE;
			}

		UIError(L"can't find property in history");

		return FALSE;
		}

	return FALSE;
	}

void CUIViewWnd::SendChange(CCmdSubItem *pSub)
{
	CItem *pItem = pSub->GetItem(m_pItem);

	if( pItem ) {

		while( pItem != m_pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

				SendChange(pItem, L"SUBITEM");
				}

			pItem = pItem->GetParent();
			}
		}

	if( pSub->m_fPrev ) {

		m_System.ItemUpdated(m_pItem, updateChildren);
		}
	}

BOOL CUIViewWnd::SendChange(CUIElement *pUI)
{
	CItem * pItem = pUI->GetItem();

	CString Tag   = pUI->GetTag();

	if( m_fHasUndo ) {

		if( TRUE ) {

			m_fEditCmd = TRUE;
			
			m_EditItem = m_System.GetNavPos();

			m_EditMenu = pUI->GetLabel();

			if( pUI->GetText() ) {

				m_EditMenu.Printf(IDS_CHANGE_TO_S, m_EditMenu);
				}
			else {
				if( m_EditMenu.Right(3) == L"..." ) {

					UINT uPos = m_EditMenu.GetLength() - 3;

					m_EditMenu.Delete(uPos, 3);
					}
				}

			SendChange(pItem, Tag);
			
			m_fEditCmd = FALSE;
			}

		if( CheckForChange(pUI, FALSE) ) {

			m_pItem->SetDirty();

			CloseMulti();

			m_fNoUndo = FALSE;

			return TRUE;
			}

		if( CloseMulti() ) {

			m_pItem->SetDirty();
			}

		return FALSE;
		}

	SendChange(pItem, Tag);

	return TRUE;
	}

void CUIViewWnd::SendChange(CItem *pItem, CString Tag)
{
	OnUIChange(pItem, Tag);
	}

void CUIViewWnd::SendEnable(CItem *pItem)
{
	SendChange(pItem, L"");
	}

void CUIViewWnd::SendEnable(void)
{
	CTree <CItem *> Tree;

	Tree.Insert(m_pItem);

	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CUIElement *pUI   = m_UIList[n];

		CItem      *pItem = pUI->GetItem();

		if( pItem ) {
			
			if( Tree.Failed(Tree.Find(pItem)) ) {

				SendEnable(pItem);

				Tree.Insert(pItem);
				}
			}
		}

	SendEnable(m_pItem);
	}

void CUIViewWnd::AddToDict(CUIElement *pUI)
{
	m_UIDict.Insert(pUI->GetTag(),    pUI);

	m_UIDict.Insert(GetLongName(pUI), pUI);
	}

void CUIViewWnd::BuildHistory(void)
{
	if( m_fHasUndo ) {

		UINT c = m_UIList.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CUIElement     *pUI   = m_UIList[n];

			CUITextElement *pText = pUI->GetText();

			if( pText ) {

				CString Name = GetLongName(pUI);

				CString Data = pText->GetAsText();

				m_UIHist.Insert(Name, Data);
				}
			}
		}
	}

void CUIViewWnd::BuildDict(void)
{
	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CUIElement *pUI = m_UIList[n];

		CString  Object = m_UIItem[n];

		if( !Object.IsEmpty() ) {

			CItem *pItem = GetObject(m_pItem, Object);

			pUI->RebindUI(pItem);

			AddToDict(pUI);
			}
		}
	}

BOOL CUIViewWnd::CheckScroll(void)
{
	if( IsCurrent() && m_pFocus ) {

		CRect Ctrl = m_pFocus->GetWindowRect();

		ScreenToClient(Ctrl);

		int nMargin = 32;

		int nScroll = 0;

		if( Ctrl.bottom > m_UIRect.bottom - nMargin ) {

			nScroll = m_UIRect.bottom - nMargin - Ctrl.bottom;
			}

		if( Ctrl.top < m_UIRect.top + nMargin ) {

			nScroll = m_UIRect.top + nMargin - Ctrl.top;
			}

		return Scroll(nScroll);
		}

	return FALSE;
	}

BOOL CUIViewWnd::Scroll(int nScroll)
{
	int nLimit = m_Active.cy() - m_UIRect.cy();

	MakeMax(nLimit,  0);

	MakeMin(nScroll, m_nScroll);

	MakeMax(nScroll, m_nScroll - nLimit);

	if( nScroll ) {

		HideToolTip();

		UpdateWindow();

		ScrollWindowEx( m_hWnd,
				0,
				nScroll,
				m_Active,
				NULL,
				NULL,
				NULL,
				SW_SCROLLCHILDREN | SW_INVALIDATE | SW_ERASE
				);

		m_Active.top    += nScroll;

		m_Active.bottom += nScroll;

		m_nScroll -= nScroll;

		m_pScroll->SetScrollPos(m_nScroll, TRUE);

		OffsetUI();

		PositionUI();

		MoveToolTips();

		return TRUE;
		}
	
	return FALSE;
	}

void CUIViewWnd::UIError(PCTXT pText, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pText);

	CString Text;

	Text.VPrintf(pText, pArgs);

	va_end(pArgs);

	AfxTrace(L"ERROR: %s\n", Text);

	AfxAssert(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Data Editing Command
//

// Runtime Class

AfxImplementRuntimeClass(CUIViewWnd::CCmdEdit, CStdCmd);

// Constructor

CUIViewWnd::CCmdEdit::CCmdEdit(CString Menu, CString Item, CString Tag, CString Prev, CString Data)
{
	m_Menu = Menu;

	m_Item = Item;

	m_Tag  = Tag;

	m_Prev = Prev;

	m_Data = Data;
	}

//////////////////////////////////////////////////////////////////////////
//
// Sub-Classed Tool Tip Control
//

// Constructor

CUIToolTip::CUIToolTip(void)
{
	}

// Window Procedure

LRESULT CUIToolTip::WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == TTM_WINDOWFROMPOINT ) {

		HWND hWnd = HWND(CToolTip::WndProc(uMessage, wParam, lParam));

		while( hWnd ) {

			if( CWnd::FromHandle(hWnd).IsKindOf(AfxRuntimeClass(CUIViewWnd)) ) {

				break;
				}

			hWnd = ::GetParent(hWnd);
			}

		return LPARAM(hWnd);
		}

	return CToolTip::WndProc(uMessage, wParam, lParam);
	}

// End of File
