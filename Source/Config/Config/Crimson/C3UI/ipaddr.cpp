
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- IP Address
//

// Dynamic Class

AfxImplementDynamicClass(CUITextIPAddress, CUITextElement);

// Constructor

CUITextIPAddress::CUITextIPAddress(void)
{
	m_uFlags = textEdit;
	}

// Overridables

void CUITextIPAddress::OnBind(void)
{
	m_uWidth = 20;

	m_uLimit = 20;

	CUITextElement::OnBind();
	}

CString CUITextIPAddress::OnGetAsText(void)
{
	UINT uData = m_pData->ReadInteger(m_pItem);

	BYTE b1 = PBYTE(&uData)[0];
	BYTE b2 = PBYTE(&uData)[1];
	BYTE b3 = PBYTE(&uData)[2];
	BYTE b4 = PBYTE(&uData)[3];

	return CPrintf(L"%u.%u.%u.%u", b4, b3, b2, b1);
	}

UINT CUITextIPAddress::OnSetAsText(CError &Error, CString Text)
{
	// LATER -- Better error processing?

	CStringArray List;

	Text.Tokenize(List, '.');

	BYTE b1 = BYTE(watoi(List[3]));
	BYTE b2 = BYTE(watoi(List[2]));
	BYTE b3 = BYTE(watoi(List[1]));
	BYTE b4 = BYTE(watoi(List[0]));

	UINT uPrev = m_pData->ReadInteger(m_pItem);

	UINT uData = 0;

	((PBYTE) &uData)[0] = b1;
	((PBYTE) &uData)[1] = b2;
	((PBYTE) &uData)[2] = b3;
	((PBYTE) &uData)[3] = b4;

	if( uData - uPrev ) {

		m_pData->WriteInteger(m_pItem, uData);

		return saveChange;
		}

	return saveSame;
	}

// End of File
