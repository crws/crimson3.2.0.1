
#include "intern.hpp"

#include <cryptuiapi.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Certificate
//

// Libraries

#pragma comment(lib, "cryptui.lib")

#pragma comment(lib, "crypt32.lib")

// Dynamic Class

AfxImplementDynamicClass(CUITextCert, CUITextElement);

// Constructor

CUITextCert::CUITextCert(void)
{
	m_uFlags = textExpand;

	m_Verb   = CString(IDS_SELECT);

	m_uWidth = 20;
	}

// Overridables

void CUITextCert::OnBind(void)
{
	CUITextElement::OnBind();
	}

CString CUITextCert::OnGetAsText(void)
{
	return m_pData->ReadString(m_pItem);
	}

UINT CUITextCert::OnSetAsText(CError &Error, CString Text)
{
	m_pData->WriteString(m_pItem, Text);
	
	return saveChange;
	}

BOOL CUITextCert::OnExpand(CWnd &Wnd)
{
	HCERTSTORE hStore = CertOpenStore( CERT_STORE_PROV_SYSTEM,
					   X509_ASN_ENCODING,
					   NULL,
					   CERT_SYSTEM_STORE_CURRENT_USER,
					   L"MY"
					   );

	if( hStore ) {

		PCCERT_CONTEXT pPick = CryptUIDlgSelectCertificateFromStore( hStore,
									     Wnd.GetHandle(),
									     NULL,
									     NULL,
									     0,
									     0,
									     0
									     );

		if( pPick ) {

			WCHAR sName[128] = {0};

			CertGetNameString( pPick,
					   CERT_NAME_FRIENDLY_DISPLAY_TYPE,
					   0,
					   NULL,
					   sName,
					   elements(sName)
					   );

			DWORD Serial = CRC32( pPick->pCertInfo->SerialNumber.pbData,
					      pPick->pCertInfo->SerialNumber.cbData
					      );

			CPrintf Name(L"%s (%8.8X)", sName, Serial);

			SetAsText(CError(FALSE), Name);

			CertFreeCertificateContext(pPick);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
