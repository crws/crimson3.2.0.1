
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Capturing Edit Control
//

// Runtime Class

AfxImplementRuntimeClass(CDropEditCtrl, CEditCtrl);

// Static Data

UINT CDropEditCtrl::m_timerQuick = CWnd::AllocTimerID();

// Constants

static TCHAR const cSep = 0xB6;

// Constructor

CDropEditCtrl::CDropEditCtrl(CUIElement *pUI)
{
	m_pUI      = pUI;

	m_uDrop    = 0;

	m_fSpecial = FALSE;

	m_fError   = FALSE;

	m_fDying   = FALSE;

	Construct();
	}

// IUnknown

HRESULT CDropEditCtrl::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CDropEditCtrl::AddRef(void)
{
	return 1;
	}

ULONG CDropEditCtrl::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CDropEditCtrl::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( !IsItemReadOnly() ) {

		if( !IsReadOnly() ) {
		
			FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

			if( pData->QueryGetData(&Fmt) == S_OK ) {

				SetFocus();

				if( *pEffect & DROPEFFECT_LINK ) {

					*pEffect = DROPEFFECT_LINK;
					}
				else
					*pEffect = DROPEFFECT_COPY;

				m_dwEffect = *pEffect;

				SetDrop(1);

				return S_OK;
				}
			}

		if( m_pUI ) {

			if( m_pUI->CanAcceptData(pData, *pEffect) ) {

				m_dwEffect = *pEffect;

				SetDrop(2);

				return S_OK;
				}
			}
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CDropEditCtrl::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CDropEditCtrl::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(0);

	return S_OK;
	}

HRESULT CDropEditCtrl::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

		STGMEDIUM Med = { 0, NULL, NULL };

		if( pData->GetData(&Fmt, &Med) == S_OK ) {

			CString Text  = PCTXT(GlobalLock(Med.hGlobal));

			CString Prev  = GetWindowText();

			int     nLen  = Prev.GetLength();

			CRange  Range = GetSel();

			GlobalUnlock(Med.hGlobal);

			ReleaseStgMedium(&Med);

			if( Range.m_nFrom == 0 && Range.m_nTo == nLen ) {

				if( Text.CompareC(Prev) ) {

					SetWindowText(Text);

					SetModify(TRUE);

					m_pUI->SaveUI(TRUE);

					ForceUpdate();
					}
				}
			else {
				Prev.Delete(Range.m_nFrom, Range.m_nTo - Range.m_nFrom);

				Prev.Insert(Range.m_nFrom, Text);

				SetWindowText(Prev);

				SetModify(TRUE);

				int nPos = Range.m_nFrom + Text.GetLength();

				SetSel(CRange(nPos, nPos));
				}

			*pEffect = m_dwEffect;

			SetDrop(0);

			return S_OK;
			}
		}

	if( m_uDrop == 2 ) {

		if( m_pUI->AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);
	
	return S_OK;
	}

// Operations

void CDropEditCtrl::SetScroll(BOOL fScroll)
{
	m_fScroll = fScroll;
	}

void CDropEditCtrl::SetNumber(BOOL fNumber)
{
	m_fNumber = fNumber;
	}

void CDropEditCtrl::SetDefault(PCTXT pDefault)
{
	m_Default = pDefault;

	if( m_hWnd ) {

		Invalidate(TRUE);
		}
	}

// Message Map

AfxMessageMap(CDropEditCtrl, CEditCtrl)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_SETTEXT)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_RBUTTONDOWN)
	AfxDispatchMessage(WM_RBUTTONUP)
	AfxDispatchMessage(WM_RBUTTONDBLCLK)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_GETDLGCODE)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)
	AfxDispatchMessage(WM_CHAR)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CDropEditCtrl)
	};

// Message Handlers

void CDropEditCtrl::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
	}

void CDropEditCtrl::OnPreDestroy(void)
{
	m_fDying = TRUE;
	}

void CDropEditCtrl::OnSetText(PCTXT pText)
{
	m_fSpecial = FALSE;

	m_fError   = FALSE;

	if( IsReadOnly() ) {
		
		if( CString(pText).Find(L"{{") < NOTHING ) {

			if( HasFocus() ) {

				AfxTrace(L"");
				}

			m_fSpecial = TRUE;
			}
		}

	if( CString(pText).StartsWith(L"WAS ") ) {

		m_fError   = TRUE;
		}

	AfxCallDefProc();
	}

void CDropEditCtrl::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_fSpecial ) {
		
		DrawSpecial(DC);

		return;
		}

	if( m_fError ) {

		DrawError(DC);

		return;
		}

	if( m_uDrop ) {

		DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

		DC.FrameRect(GetClientRect(), afxBrush(Orange1));

		return;
		}

	if( !IsEnabled() ) {

		DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

		DC.FrameRect(GetClientRect(), afxColor(Disabled));

		return;
		}

	if( !m_Default.IsEmpty() ) {

		if( !GetWindowTextLength() && !HasFocus() && IsEnabled() ) {

			CRect Rect = GetClientRect();
			
			DC.FillRect(Rect, afxBrush(WindowBack));

			DC.FrameRect(Rect, afxBrush(Enabled));

			DC.SetTextColor(afxColor(3dShadow));

			DC.SetBkMode(TRANSPARENT);

			DC.Select(afxFont(Dialog));

			DC.TextOut(5, 2, m_Default);

			DC.Deselect();

			return;
			}
		}

	DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

	DC.FrameRect(GetClientRect(), afxColor(Enabled));
	}

void CDropEditCtrl::OnSetFocus(CWnd &Wnd)
{
	if( m_fSpecial || m_fError ) {

		return;
		}

	AfxCallDefProc();
	}

void CDropEditCtrl::OnKillFocus(CWnd &Wnd)
{
	if( m_fSpecial || m_fError ) {

		return;
		}

	if( !m_Default.IsEmpty() ) {

		if( !GetWindowTextLength() && IsEnabled() ) {

			Invalidate(TRUE);
			}
		}

	AfxCallDefProc();
	}

void CDropEditCtrl::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_fSpecial ) {

		AfxCallDefProc();
		}
	}

void CDropEditCtrl::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( !m_fSpecial ) {

		AfxCallDefProc();
		}
	}

void CDropEditCtrl::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( GetWindowStyle() & ES_READONLY ) {
	
		if( !m_fSpecial ) {

			AfxCallDefProc();
			}
		
		SetFocus();

		SendNotify(IDM_UI_SPECIAL);

		return;
		}

	if( !m_fSpecial ) {

		AfxCallDefProc();
		}
	}

void CDropEditCtrl::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_fSpecial ) {

		AfxCallDefProc();
		}
	}

void CDropEditCtrl::OnRButtonUp(UINT uFlags, CPoint Pos)
{
	if( !m_fSpecial ) {

		AfxCallDefProc();
		}
	}

void CDropEditCtrl::OnRButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( !m_fSpecial ) {

		AfxCallDefProc();
		}
	}

void CDropEditCtrl::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( !m_fSpecial ) {

		AfxCallDefProc();
		}
	}

void CDropEditCtrl::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
		}

	if( m_fScroll ) {

		if( nDelta > 0 ) {

			m_pUI->ScrollData(SB_LINEUP);
			}
		else
			m_pUI->ScrollData(SB_LINEDOWN);

		return;
		}

	AfxCallDefProc();
	}

BOOL CDropEditCtrl::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( !m_fSpecial ) {

		return AfxCallDefProc();
		}

	SetCursor(LoadCursor(NULL, IDC_ARROW));

	return TRUE;
	}

UINT CDropEditCtrl::OnGetDlgCode(MSG *pMsg)
{
	if( !m_fSpecial ) {

		UINT uCode = DLGC_WANTCHARS | DLGC_HASSETSEL | DLGC_WANTARROWS;

		if( pMsg ) {

			if( pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP ) {

				if( pMsg->wParam == VK_TAB ) {

					uCode |= DLGC_WANTMESSAGE;
					}

				if( pMsg->wParam == VK_RETURN ) {

					uCode |= DLGC_WANTMESSAGE;
					}

				if( pMsg->wParam == VK_ESCAPE ) {

					if( GetModify() ) {

						uCode |= DLGC_WANTMESSAGE;
						}
					}
				}
			}

		return uCode;
		}

	return 0;
	}

void CDropEditCtrl::OnKeyDown(UINT uCode, DWORD dwData)
{
	if( m_fDying || m_fSpecial ) {

		return;
		}

	if( !IsItemReadOnly() ) {

		if( m_fScroll ) {

			switch( uCode ) {

				case VK_UP:

					if( m_fFirst ) {

						SetTimer(m_timerQuick, 1000);

						m_fFirst = FALSE;
						}
					
					m_pUI->ScrollData(m_fQuick ? SB_PAGEUP : SB_LINEUP);

					return;

				case VK_DOWN:
					
					if( m_fFirst ) {

						SetTimer(m_timerQuick, 1000);

						m_fFirst = FALSE;
						}
					
					m_pUI->ScrollData(m_fQuick ? SB_PAGEDOWN : SB_LINEDOWN);

					return;

				case VK_HOME:

					if( GetKeyState(VK_CONTROL) & 0x8000 ) {

						m_pUI->ScrollData(SB_TOP);
					
						return;
						}

					break;

				case VK_END:

					if( GetKeyState(VK_CONTROL) & 0x8000 ) {

						m_pUI->ScrollData(SB_BOTTOM);
					
						return;
						}

					break;

				case VK_PRIOR:

					m_pUI->ScrollData(SB_PAGEUP);
					
					return;

				case VK_NEXT:
					
					m_pUI->ScrollData(SB_PAGEDOWN);
					
					return;
				}
			}
		}

	switch( uCode ) {

		case VK_TAB:

			if( GetModify() ) {

				switch( m_pUI->SaveUI(TRUE) ) {

					case saveChange:

						ForceUpdate();

						HandleTab();

						break;

					case saveSame:

						HandleTab();

						break;
					
					case saveError:

						SetSel(CRange(TRUE));

						break;
					}
				}
			else
				HandleTab();

			return;

		case VK_RETURN:

			if( GetModify() ) {

				switch( m_pUI->SaveUI(TRUE) ) {
					
					case saveChange:

						ForceUpdate();

						break;
					
					case saveSame:

						SetSel(CRange(TRUE));

						break;

					case saveError:

						SetSel(CRange(TRUE));

						break;
					}

				return;
				}
			
			SetSel(CRange(TRUE));

			return;

		case VK_ESCAPE:

			m_pUI->LoadUI();

			return;
		}

	AfxCallDefProc();
	}

void CDropEditCtrl::OnKeyUp(UINT uCode, DWORD dwData)
{
	if( m_fDying || m_fSpecial ) {

		return;
		}

	if( !IsItemReadOnly() ) {

		if( m_fScroll ) {

			switch( uCode ) {

				case VK_UP:
				case VK_DOWN:

					m_fFirst = TRUE;

					m_fQuick = FALSE;

					KillTimer(m_timerQuick);

					return;

				case VK_HOME:
				case VK_END:

					if( GetKeyState(VK_CONTROL) & 0x8000 ) {

						return;
						}

					break;

				case VK_PRIOR:
				case VK_NEXT:

					return;
				}
			}
		}

	switch( uCode ) {

		case VK_TAB:
		case VK_RETURN:
		case VK_ESCAPE:

			return;
		}

	AfxCallDefProc();
	}

void CDropEditCtrl::OnChar(UINT uCode, DWORD dwData)
{
	if( m_fDying || m_fSpecial ) {

		return;
		}

	if( IsItemReadOnly() ) {

		return;
		}

	if( m_fNumber ) {

		if( isprint(uCode) ) {

			if( isdigit(uCode) ) {

				AfxCallDefProc();
				
				return;
				}

			if( uCode == '-' || uCode == '+' ) {

				if( m_pUI->GetText()->HasFlag(textSigned) ) {
				
					AfxCallDefProc();

					return;
					}
				}

			if( uCode == '.' ) {

				if( m_pUI->GetText()->HasFlag(textPlaces) ) {
				
					AfxCallDefProc();

					return;
					}				
				}

			return;
			}
		}

	switch( uCode ) {

		case VK_TAB:
		case VK_RETURN:
		case VK_ESCAPE:

			return;
		}

	AfxCallDefProc();
	}

void CDropEditCtrl::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerQuick ) {

		m_fQuick = TRUE;
		}

	AfxCallDefProc();
	}

// Command Handlers

BOOL CDropEditCtrl::OnPasteControl(UINT uID, CCmdSource &Src)
{
	if( IsItemReadOnly() ) {

		Src.EnableItem(FALSE);

		return TRUE;
		}

	if( InEditMode() ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

			if( pData->QueryGetData(&Fmt) == S_OK ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
				}

			pData->Release();
			}

		return TRUE;
		}

	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			DWORD dwEffect;

			if( m_pUI->CanAcceptData(pData, dwEffect) ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

BOOL CDropEditCtrl::OnPasteCommand(UINT uID)
{
	if( InEditMode() ) {

		Paste();

		return TRUE;
		}

	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			if( m_pUI->AcceptData(pData) ) {

				pData->Release();

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

// Implementation

void CDropEditCtrl::Construct(void)
{
	m_fScroll = FALSE;

	m_fNumber = FALSE;

	m_fFirst  = TRUE;

	m_fQuick  = FALSE;

	m_uDrop   = 0;
	}

void CDropEditCtrl::ForceUpdate(void)
{
	SendNotify(IDM_UI_CHANGE);
	}

void CDropEditCtrl::HandleTab(void)
{
	BOOL fPrev = (GetKeyState(VK_SHIFT) & 0x8000) ? TRUE : FALSE;

	CWnd  &Dlg = FindParent();

	HWND  hWnd = GetNextDlgTabItem(Dlg, m_hWnd, fPrev);

	CWnd  &Wnd = CWnd::FromHandle(hWnd);

	((CDialog &) Dlg).SetDlgFocus(Wnd);
	}

CWnd & CDropEditCtrl::FindParent(void)
{
	HWND hWnd = m_hWnd;

	for(;;) {

		hWnd = ::GetParent(hWnd);

		if( ::GetWindowLong(hWnd, GWL_EXSTYLE) & WS_EX_CONTROLPARENT ) {

			continue;
			}

		break;
		}

	return CWnd::FromHandle(hWnd);
	}

BOOL CDropEditCtrl::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDropEditCtrl::InEditMode(void)
{
	if( HasFocus() ) {

		CRange Edit = GetSel();

		int    nLen = GetWindowTextLength();

		if( Edit.m_nFrom == 0 && Edit.m_nTo == nLen ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CDropEditCtrl::IsItemReadOnly(void)
{
	return m_pUI->GetText()->HasFlag(textRead);
	}

void CDropEditCtrl::DrawSpecial(CDC &DC)
{
	CColor const &Back = afxColor(TabLock);

	CBrush const &Fill = afxBrush(TabLock);

	CRect         Rect = GetClientRect();
	
	CString       Text = GetWindowText();

	if( m_uDrop ) {

		CBrush const &Edge = afxBrush(Orange1);

		DC.FrameRect(Rect--, Edge);
		}
	else {
		CBrush const &Edge = IsEnabled() ? afxBrush(Enabled) : afxBrush(Disabled);

		DC.FrameRect(Rect--, Edge);
		}

	CString      Code;

	CStringArray List;

	Text.Tokenize(List, cSep);

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		if( List[n].StartsWith(L"{{") ) {

			CRect Draw = Rect;

			Rect.left  = Rect.left + Rect.cy();

			Draw.right = Rect.left;
			
			DC.FillRect(Draw--, Fill);

			if( !IsEnabled() ) {
				
				DC.FillRect(--Draw, afxBrush(3dShadow));
				}
			else {
				DWORD Color = DWORD(wcstol(List[n].Mid(2), NULL, 16));
				
				if( Color == Back ) {
					
					DC.FrameRect(--Draw, afxBrush(BLACK));
					}
				
				DC.FillRect(--Draw, CColor(Color));
				}
			}
		else {
			if( !Code.IsEmpty() ) {

				Code += L',';
				}

			Code += List[n];
			}
		}

	DC.FillRect(Rect, afxBrush(TabLock));

	DC.Select(afxFont(Dialog));

	DC.SetTextColor(afxColor(BLACK));

	DC.SetBkMode(TRANSPARENT);

	CPoint Pos = Rect.GetTopLeft();

	int    cy  = DC.GetTextExtent(L"X").cy;

	Pos.x += 3;

	Pos.y += (Rect.cy() - cy) / 2;

	DC.ExtTextOut( Pos,
		       ETO_CLIPPED,
		       Rect,
		       Code
		       );

	DC.Deselect();
	}

void CDropEditCtrl::DrawError(CDC &DC) {

	CRect         Rect = GetClientRect();
	
	CString       Text = GetWindowText();

	CBrush const &Edge = IsEnabled() ? afxBrush(Enabled) : afxBrush(Disabled);

	DC.FillRect(Rect, afxBrush(TabLock));

	DC.FrameRect(Rect, Edge);

	DC.SetTextColor(afxColor(RED));

	DC.SetBkMode(TRANSPARENT);

	DC.Select(afxFont(Dialog));

	CPoint Pos = Rect.GetTopLeft();

	int    cy  = DC.GetTextExtent(L"X").cy;

	Pos.x += 3;

	Pos.y += (Rect.cy() - cy) / 2;

	DC.ExtTextOut(Pos, ETO_CLIPPED, Rect, Text);

	DC.Deselect();
	}

// End of File
