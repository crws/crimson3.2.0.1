
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- On/Off
//

// Dynamic Class

AfxImplementDynamicClass(CUITextOnOff, CUITextElement);

// Constructor

CUITextOnOff::CUITextOnOff(void)
{
	m_uFlags = textEnum;

	m_On     = IDS_ON;

	m_Off    = IDS_OFF;
	}

// Overridables

void CUITextOnOff::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	if( GetFormat().Tokenize(List, '|') ) {

		m_Off = List[0];

		m_On  = List[1];
		}
	}

CString CUITextOnOff::OnGetAsText(void)
{
	return m_pData->ReadInteger(m_pItem) ? m_On : m_Off;
	}

UINT CUITextOnOff::OnSetAsText(CError &Error, CString Text)
{
	UINT uPrev = m_pData->ReadInteger(m_pItem);

	UINT uData = (Text == m_On);

	if( uData - uPrev ) {

		m_pData->WriteInteger(m_pItem, uData);

		return saveChange;
		}

	return saveSame;
	}

BOOL CUITextOnOff::OnEnumValues(CStringArray &List)
{
	List.Append(m_Off);

	List.Append(m_On);

	return TRUE;
	}

// End of File
