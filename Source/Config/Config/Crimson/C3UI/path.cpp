
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Pathname
//

// Dynamic Class

AfxImplementDynamicClass(CUITextPath, CUITextElement);

// Constructor

CUITextPath::CUITextPath(void)
{
	m_uFlags = textExpand;

	m_Verb   = IDS_BROWSE;

	m_uWidth = 20;
	}

// Overridables

void CUITextPath::OnBind(void)
{
	CUITextElement::OnBind();
	}

CString CUITextPath::OnGetAsText(void)
{
	return m_pData->ReadString(m_pItem);
	}

UINT CUITextPath::OnSetAsText(CError &Error, CString Text)
{
	m_pData->WriteString(m_pItem, Text);
	
	return saveChange;
	}

BOOL CUITextPath::OnExpand(CWnd &Wnd)
{
	TCHAR      sWork[MAX_PATH];

	CString    Title = IDS_SELECT_THE;

	BROWSEINFO Info  = { 0 };

	Info.hwndOwner      = Wnd;
	Info.pidlRoot       = NULL;
	Info.pszDisplayName = sWork;
	Info.lpszTitle	    = Title;
	Info.ulFlags	    = BIF_NEWDIALOGSTYLE | BIF_RETURNONLYFSDIRS;
	Info.lpfn	    = Hook;
	Info.lParam	    = LPARAM(sWork);
	Info.iImage	    = 0;
	
	wstrcpy(sWork, m_pData->ReadString(m_pItem));

	LPITEMIDLIST pidl = SHBrowseForFolder(&Info);

	if( pidl ) {

		IMalloc *pMalloc = NULL;

		SHGetMalloc(&pMalloc);

		if( SHGetPathFromIDList(pidl, sWork) ) {

			SetAsText(CError(FALSE), sWork);

			pMalloc->Free(pidl);

			return TRUE;
			}
		else {
			Wnd.Error(IDS_THE_SELECTION_IS);

			pMalloc->Free(pidl);
			}
		}

	return FALSE;
	}

// Callback

INT CALLBACK CUITextPath::Hook(HWND hWnd, UINT uMessage, LPARAM lParam, LPARAM lpData)
{
	if( uMessage == BFFM_INITIALIZED ) {

		SendMessage(hWnd, BFFM_SETSELECTION, TRUE, lpData);
		}

	return 0;
	}

// End of File
