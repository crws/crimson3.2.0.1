
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Edit Box
//

// Dynamic Class

AfxImplementDynamicClass(CUIEditBox, CUIControl);

// Constructor

CUIEditBox::CUIEditBox(void)
{
	m_dwStyle     = ES_AUTOHSCROLL;

	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pUnitLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pSpinCtrl   = NULL;

	m_pDataCtrl   = New CDropEditCtrl(this);

	m_pUnitCtrl   = New CStatic;
	}

// Core Overridables

void CUIEditBox::OnBind(void)
{
	CUIElement::OnBind();

	if( m_pText->HasFlag(textHide) ) {

		m_dwStyle |= ES_PASSWORD;
		}

	if( !m_pText->HasFlag(textEdit) ) {

		m_dwStyle |= ES_READONLY;
		}

	if( m_pText->HasFlag(textScroll) ) {

		m_pDataCtrl->SetScroll(TRUE);
		}

	if( m_pText->HasFlag(textNumber) ) {

		m_pDataCtrl->SetNumber(TRUE);
		}

	if( m_pText->HasFlag(textUnits) ) {

		m_Units = m_pText->GetUnits();
		}

	if( m_pText->HasFlag(textDefault) ) {

		m_pDataCtrl->SetDefault(m_pText->GetDefault());
		}

	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}
	}

void CUIEditBox::OnLayout(CLayFormation *pForm)
{
	UINT uSize = m_pText->GetWidth();

	INT  nPad  = 2;

	if( m_pText->HasFlag(textScroll) ) {

		// LATER -- If we make the box taller via nPad to better
		// show the spin buttons, the editable text isn't in the
		// middle any more. There is no trivial way I can find to
		// fix this, so figure out some other more complex way!

		MakeMax(uSize, m_pText->GetLimit());

		uSize = uSize + 5;

		nPad  = nPad  + 1;
		}

	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayItemText(uSize, CSize(5, nPad), CSize(1, 1));

	m_pUnitLayout = New CLayItemText(m_Units, 1, m_Units.IsEmpty() ? 1 : 4);

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pDataLayout, horzLeft | vertCenter));
	
	m_pMainLayout->AddItem(New CLayFormPad(m_pUnitLayout, horzNone | vertCenter));

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIEditBox::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	if( m_pText->HasFlag(textScroll) ) {

		// LATER -- Can we stop this flickering so
		// that we can keep using the spin buttons?

		CRect Spin  = GetDataCtrlRect();

		Spin.left   = Spin.right  - 18;
		
		Spin.right  = Spin.left   + 20;

		Spin.top    = Spin.top    +  1;

		Spin.bottom = Spin.bottom -  1;

		m_pSpinCtrl = New CSpinner;

		m_pSpinCtrl->Create( UDS_HOTTRACK | WS_CLIPSIBLINGS,
				     Spin,
				     Wnd,
				     uID++
				     );

		m_pSpinCtrl->SetRange(0, 100);

		m_pSpinCtrl->SetPos(50);

		AddControl(m_pSpinCtrl);

		m_dwStyle |= WS_CLIPSIBLINGS;
		}

	m_pDataCtrl->Create( WS_TABSTOP | WS_BORDER | m_dwStyle,
			     GetDataCtrlRect(),
			     Wnd,
			     uID++
			     );

	m_pUnitCtrl->Create( m_Units,
			     SS_WORDELLIPSIS,
			     m_pUnitLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	m_pUnitCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, EN_KILLFOCUS);

	AddControl(m_pUnitCtrl);

	m_pDataCtrl->LimitText(m_pText->GetLimit());
	}

void CUIEditBox::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(GetDataCtrlRect(), TRUE);

	m_pUnitCtrl->MoveWindow(m_pUnitLayout->GetRect(), TRUE);

	if( m_pSpinCtrl ) {

		// LATER -- Why are these different from above???

		CRect Spin  = GetDataCtrlRect();

		Spin.left   = Spin.right  - 21;
		
		Spin.right  = Spin.left   + 20;

		Spin.top    = Spin.top    +  1;

		Spin.bottom = Spin.bottom -  1;

		m_pSpinCtrl->MoveWindow(Spin, TRUE);
		}
	}

void CUIEditBox::OnScrollData(UINT uCode)
{
	CString Prev = GetDataCtrlText();

	CString Data = m_pText->ScrollData(Prev, uCode);

	m_pDataCtrl->SetFocus();

	m_pDataCtrl->SetWindowText(Data);

	m_pDataCtrl->SetModify(TRUE);

	CRange Range(TRUE);

	m_pDataCtrl->SetSel(Range);

	OnSave(FALSE);

	ForceUpdate();
	}

BOOL CUIEditBox::OnFindFocus(CWnd * &pWnd)
{
	CRange Range(TRUE);

	m_pDataCtrl->SetSel(Range);

	pWnd = m_pDataCtrl;
	
	return TRUE;
	}

// Data Overridables

void CUIEditBox::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	if( !IsMulti(Data) ) {

		m_pDataCtrl->SetWindowText(Data);

		if( m_pDataCtrl->HasFocus() ) {

			CRange Range(TRUE);

			m_pDataCtrl->SetSel(Range);
			}
		}
	else
		m_pDataCtrl->SetWindowText(L"");

	m_pDataCtrl->SetModify(FALSE);
	}

UINT CUIEditBox::OnSave(BOOL fUI)
{
	if( m_pDataCtrl->GetModify() ) {

		CString Text = GetDataCtrlText();

		return StdSave(fUI, Text);
		}
	
	return saveSame;
	}

// Notification Overridables

BOOL CUIEditBox::OnNotify(UINT uID, NMHDR &Info)
{
	if( m_pSpinCtrl ) {
		
		if( uID == m_pSpinCtrl->GetID() ) {

			if( Info.code == UDN_DELTAPOS ) {

				NMUPDOWN &Spin = (NMUPDOWN &) Info;

				if( Spin.iDelta < 0 ) {

					if( Spin.iDelta < -1 ) {

						ScrollData(SB_PAGEDOWN);
						}
					else
						ScrollData(SB_LINEDOWN);
					}

				if( Spin.iDelta > 0 ) {
					
					if( Spin.iDelta > +1 ) {

						ScrollData(SB_PAGEUP);
						}
					else
						ScrollData(SB_LINEUP);
					}

				return FALSE;
				}
			}
		}

	return FALSE;
	}

// Implementation

CRect CUIEditBox::GetDataCtrlRect(void)
{
	CRect Rect;
	
	Rect = m_pDataLayout->GetRect();

	Rect.top    -= 1;

	Rect.bottom += 1;

	return Rect;
	}

CString CUIEditBox::GetDataCtrlText(void)
{
	CString Text = m_pDataCtrl->GetWindowText();

	if( m_fMulti && Text.IsEmpty() ) {

		return multiString;
		}

	Text.FoldUnicode(MAP_FOLDDIGITS | MAP_PRECOMPOSED);

	return Text;
	}

// End of File
