
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Hex Integer
//

// Dynamic Class

AfxImplementDynamicClass(CUITextHexInt, CUITextElement);

// Constructor

CUITextHexInt::CUITextHexInt(void)
{
	m_uFlags = textEdit;

	m_uWidth = 4;

	m_uLimit = 4;
	}

// Overridables

void CUITextHexInt::OnBind(void)
{
	CUITextElement::OnBind();

	CString Size = GetFormat().StripToken('|');

	if( !Size.IsEmpty() ) {

		if( Size[0] == L'*' ) {

			m_uFlags |= textHide;

			Size = Size.Mid(1);
			}

		m_uLimit = watoi(Size);

		MakeMax(m_uLimit, 1);
		}

	m_uWidth = max(4, m_uLimit);
	}

CString CUITextHexInt::OnGetAsText(void)
{
	UINT uData = m_pData->ReadInteger(m_pItem);

	return Format(uData);
	}

UINT CUITextHexInt::OnSetAsText(CError &Error, CString Text)
{
	UINT uPrev = m_pData->ReadInteger(m_pItem);

	UINT uData = Parse(Text);

	if( uData - uPrev ) {

		m_pData->WriteInteger(m_pItem, uData);

		return saveChange;
		}

	return saveSame;
	}

// Implementation

CString CUITextHexInt::Format(UINT uData)
{
	return CPrintf(L"%*.*X", m_uLimit, m_uLimit, uData);
	}

UINT CUITextHexInt::Parse(PCTXT pText)
{
	return (wcstoul(pText, NULL, 16) & ((1ULL << (4 * m_uLimit)) - 1));
	}

// End of File
