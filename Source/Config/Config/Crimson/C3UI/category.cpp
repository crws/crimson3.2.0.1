
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Categorizer
//

// Dynamic Class

AfxImplementDynamicClass(CUICategorizer, CUIControl);

// Constructor

CUICategorizer::CUICategorizer(void)
{
	m_dwShowStyle = WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL | ES_READONLY;

	m_dwEditStyle = WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL;

	m_dwListStyle = WS_TABSTOP | WS_BORDER;

	m_uShowCode   = NOTHING;

	m_uEditCode   = EN_KILLFOCUS;

	m_uListCode   = NOTHING;

	m_pTextLayout = NULL;

	m_pModeLayout = NULL;

	m_pDataLayout = NULL;

	m_pPickLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pModeCtrl   = New CModeButton(this);

	m_pShowCtrl   = New CDropEditCtrl(this);

	m_pEditCtrl   = New CDropEditCtrl(this);

	m_pListCtrl   = NULL;

	m_pPickCtrl   = NewHotLink();
	}

// Core Overridables

void CUICategorizer::OnBind(void)
{
	CUIElement::OnBind();

	if( m_pText->HasFlag(textHide) ) {

		m_dwEditStyle |= ES_PASSWORD;

		m_dwShowStyle |= ES_PASSWORD;
		}

	if( !m_pText->HasFlag(textEdit) ) {

		m_dwEditStyle |= ES_READONLY;
		}

	if( m_pText->HasFlag(textScroll) ) {

		m_pEditCtrl->SetScroll(TRUE);
		}

	if( m_pText->HasFlag(textNumber) ) {

		m_pEditCtrl->SetNumber(TRUE);
		}

	if( m_pText->HasFlag(textDefault) ) {

		m_pEditCtrl->SetDefault(m_pText->GetDefault());
		}

	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}

	LoadModeButton();
	}

void CUICategorizer::OnLayout(CLayFormation *pForm)
{
	UINT uSize = m_pText->GetWidth();

	INT  nPad  = 2;

	m_pTextLayout = New CLayItemText(m_Label, 0, 1);

	m_pModeLayout = New CLayItemSize(m_pModeCtrl->GetSize(), 0, 1);

	m_pDataLayout = New CLayItemText(uSize, CSize(0, nPad), CSize(1, 1));

	m_pPickLayout = New CLayItemText(CString(IDS_PICK_1), CSize(6, 4), CSize(1, 1));

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pModeLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pDataLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pPickLayout, horzLeft | vertCenter));

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUICategorizer::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pModeCtrl->Create( m_pModeLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	if( m_pShowCtrl ) {

		m_pShowCtrl->Create( WS_TABSTOP | m_dwShowStyle,
				     GetDataCtrlRect(),
				     Wnd,
				     uID++
				     );
		}

	if( m_pEditCtrl ) {

		m_pEditCtrl->Create( WS_TABSTOP | m_dwEditStyle,
				     GetDataCtrlRect(),
				     Wnd,
				     uID++
				     );
		}

	if( m_pListCtrl ) {

		m_pListCtrl->Create( WS_TABSTOP | m_dwListStyle,
				     GetListCtrlRect(),
				     Wnd,
				     uID++
				     );
		}

	m_pPickCtrl->Create( WS_TABSTOP | BS_PUSHBUTTON,
			     m_pPickLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pModeCtrl->SetFont(afxFont(Dialog));

	m_pPickCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pModeCtrl);

	AddControl(m_pPickCtrl);

	if( m_pShowCtrl ) {

		m_pShowCtrl->SetFont(afxFont(Dialog));
		}

	if( m_pEditCtrl ) {
	
		m_pEditCtrl->SetFont(afxFont(Dialog));
		}

	if( m_pListCtrl ) {
	
		m_pListCtrl->SetFont(afxFont(Dialog));
		}

	if( m_pShowCtrl ) {

		AddControl(m_pShowCtrl, m_uShowCode);
		}

	if( m_pEditCtrl ) {

		AddControl(m_pEditCtrl, m_uEditCode);
		}

	if( m_pListCtrl ) {

		AddControl(m_pListCtrl, m_uListCode);
		}
	}

void CUICategorizer::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pModeCtrl->MoveWindow(m_pModeLayout->GetRect(), TRUE);

	m_pPickCtrl->MoveWindow(m_pPickLayout->GetRect(), TRUE);

	if( m_pEditCtrl ) {

		m_pEditCtrl->MoveWindow(GetDataCtrlRect(), TRUE);
		}

	if( m_pShowCtrl ) {

		m_pShowCtrl->MoveWindow(GetDataCtrlRect(), TRUE);
		}

	if( m_pListCtrl ) {

		m_pListCtrl->MoveWindow(GetDataCtrlRect(), TRUE);
		}
	}

void CUICategorizer::OnShow(BOOL fShow)
{
	UINT uCode = fShow ? SW_SHOW : SW_HIDE;

	UINT uHide = SW_HIDE;

	UINT uPick = m_pPickCtrl->GetWindowTextLength();

	if( fShow ) {

		if( !uPick || !m_pPickCtrl->HasFocus() ) {

			UINT uCount = m_List.GetCount();

			for( UINT n = 0; n < uCount; n++ ) {

				CCtrlWnd *pCtrl = m_List[n];

				if( pCtrl->HasFocus() ) {

					SetBestFocus();

					break;
					}
				}
			}
		}

	m_pTextCtrl->ShowWindow(uCode);

	m_pModeCtrl->ShowWindow(uCode);

	m_pPickCtrl->ShowWindow(uPick ? uCode : uHide);

	if( m_pShowCtrl ) {

		m_pShowCtrl->ShowWindow((m_uType == 1) ? uCode : uHide);
		}

	if( m_pEditCtrl ) {

		m_pEditCtrl->ShowWindow((m_uType == 2) ? uCode : uHide);
		}

	if( m_pListCtrl ) {
	
		m_pListCtrl->ShowWindow((m_uType == 3) ? uCode : uHide);
		}

	m_fShow = fShow;
	}

void CUICategorizer::OnScrollData(UINT uCode)
{
	CString Prev = GetDataCtrlText();

	CString Data = m_pText->ScrollData(Prev, uCode);

	m_pEditCtrl->SetWindowText(Data);

	m_pEditCtrl->SetModify(TRUE);

	CRange Range(TRUE);

	m_pEditCtrl->SetSel(Range);

	OnSave(FALSE);

	ForceUpdate();
	}

BOOL CUICategorizer::OnFindFocus(CWnd * &pWnd)
{
	pWnd = m_pModeCtrl;

	return TRUE;
	}

// Data Overridables

void CUICategorizer::OnLoad(void)
{
	LoadModeButton();

	CString Data = m_pText->GetAsText();

	m_uMode = FindDispMode(Data);

	LoadData(Data, FALSE);
	}

UINT CUICategorizer::OnSave(BOOL fUI)
{
	if( m_pEditCtrl->GetModify() ) {

		CString Text = GetDataCtrlText();

		CString Data = FindDataText(Text, m_uMode);

		return StdSave(fUI, Data);
		}
	
	return saveSame;
	}

BOOL CUICategorizer::OnAcceptData(IDataObject *pData)
{
	if( AcceptText(pData, CF_UNICODETEXT) ) {

		SetBestFocus();

		return TRUE;
		}

	return FALSE;
	}

// Notification Overridables

BOOL CUICategorizer::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pShowCtrl->GetID() ) {

		if( !m_pItem->GetDatabase()->IsReadOnly() ) {

			if( uCode == IDM_UI_SPECIAL ) {

				SwitchMode(NOTHING);
				}
			}

		return FALSE;
		}

	if( uID == m_pModeCtrl->GetID() ) {

		if( !m_pText->HasFlag(textRead) ) {

			if( !uCode ) {

				uCode = m_pModeCtrl->ShowMenu();
				}

			if( uCode < NOTHING ) {

				SwitchMode(uCode);
				}
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

BOOL CUICategorizer::OnNotify(UINT uID, NMHDR &Info)
{
	return FALSE;
	}

// Category Overridables

BOOL CUICategorizer::LoadModeButton(void)
{
	CModeButton::COption Opt;

	m_pModeCtrl->ClearOptions();

	m_pModeCtrl->AddOption(Opt);

	Opt.m_uID     = 1;
	Opt.m_Text    = IDS_DEFAULT;
	Opt.m_fEnable = TRUE;
	Opt.m_fHidden = FALSE;
	Opt.m_Image   = 0;

	m_pModeCtrl->AddOption(Opt);

	return TRUE;
	}

void CUICategorizer::LoadShowControl(CString Data)
{
	m_pShowCtrl->SetWindowText(Data);
	}

void CUICategorizer::LoadEditControl(CString Data)
{
	m_pEditCtrl->SetWindowText(Data);
	}

void CUICategorizer::LoadListControl(CString Data)
{
	}

BOOL CUICategorizer::SwitchMode(UINT uMode)
{
	return TRUE;
	}

UINT CUICategorizer::FindDispMode(CString Text)
{
	return 1;
	}

CString CUICategorizer::FindDispText(CString Text, UINT uMode)
{
	return Text;
	}

CString CUICategorizer::FindDataText(CString Text, UINT uMode)
{
	return Text;
	}

UINT CUICategorizer::FindDispType(UINT uMode)
{
	return 2;
	}

CString CUICategorizer::FindDispVerb(UINT uMode)
{
	return L"";
	}

// Implementation

void CUICategorizer::LoadData(CString Data, BOOL fModify)
{
	m_uType = FindDispType(m_uMode);

	CString Verb = FindDispVerb(m_uMode);

	CString Text = FindDispText(Data, m_uMode);

	LoadShowControl(Text);

	LoadEditControl(Text);

	LoadListControl(Text);

	m_pEditCtrl->SetSel(CRange(TRUE));

	m_pEditCtrl->SetModify(fModify);

	m_pPickCtrl->SetWindowText(Verb);

	m_pModeCtrl->SetData(m_uMode);

	OnShow(m_fShow);
	}

CRect CUICategorizer::GetDataCtrlRect(void)
{
	CRect Rect;
	
	Rect = m_pDataLayout->GetRect();

	Rect.top    -= 1;

	Rect.bottom += 1;

	return Rect;
	}

CRect CUICategorizer::GetListCtrlRect(void)
{
	CRect Rect  = m_pDataLayout->GetRect();

	Rect.top    = Rect.top   - 1;

	Rect.bottom = Rect.top   + 8 * Rect.cy();

	return Rect;
	}

CString CUICategorizer::GetDataCtrlText(void)
{
	CString Text = m_pEditCtrl->GetWindowText();

	if( m_fMulti && Text.IsEmpty() ) {

		return multiString;
		}

	Text.FoldUnicode(MAP_FOLDDIGITS | MAP_PRECOMPOSED);

	return Text;
	}

void CUICategorizer::SetBestFocus(void)
{
	switch( m_uType ) {

		case 0:
			m_pModeCtrl->SetFocus();
			break;

		case 1:
			m_pModeCtrl->SetFocus();
			break;

		case 2:
			m_pEditCtrl->SetFocus();
			break;

		case 3:
			m_pListCtrl->SetFocus();
			break;
		}
	}

// End of File
