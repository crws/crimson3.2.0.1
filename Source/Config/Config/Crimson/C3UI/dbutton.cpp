
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Drop-Target Button
//

// Runtime Class

AfxImplementRuntimeClass(CDropButton, CButton);

// Constructor

CDropButton::CDropButton(CUIElement *pUI)
{
	m_pUI   = pUI;
	
	m_uDrop = 0;
	}

// IUnknown

HRESULT CDropButton::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CDropButton::AddRef(void)
{
	return 1;
	}

ULONG CDropButton::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CDropButton::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( m_pUI ) {

		if( !IsItemReadOnly() ) {

			if( m_pUI->CanAcceptData(pData, *pEffect) ) {

				m_dwEffect = *pEffect;

				SetDrop(1);

				return S_OK;
				}
			}
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CDropButton::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CDropButton::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(0);

	return S_OK;
	}

HRESULT CDropButton::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		if( m_pUI->AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);
	
	return S_OK;
	}

// Message Map

AfxMessageMap(CDropButton, CButton)
{
	AfxDispatchMessage(WM_POSTCREATE)

	AfxDispatchNotify(0, NM_CUSTOMDRAW, OnCustomDraw)
	
	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CDropButton)
	};

// Message Handlers

void CDropButton::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
	}

// Notification Handlers

UINT CDropButton::OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYPOSTPAINT;
		}

	if( Info.dwDrawStage == CDDS_POSTPAINT ) {

		if( m_uDrop ) {

			CDC DC(Info.hdc);
	
			CRect Rect = Info.rc;

			DC.FrameRect(Rect, afxBrush(Orange1));

			DC.Detach(FALSE);
			}
		}

	return 0;
	}

// Command Handlers

BOOL CDropButton::OnPasteControl(UINT uID, CCmdSource &Src)
{
	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			DWORD dwEffect;

			if( m_pUI->CanAcceptData(pData, dwEffect) ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

BOOL CDropButton::OnPasteCommand(UINT uID)
{
	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			if( m_pUI->AcceptData(pData) ) {

				pData->Release();

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

// Implementation

BOOL CDropButton::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDropButton::IsItemReadOnly(void)
{
	return m_pUI->GetText()->HasFlag(textRead);
	}

// End of File
