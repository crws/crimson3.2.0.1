
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- String
//

// Dynamic Class

AfxImplementDynamicClass(CUITextString, CUITextElement);

// Constructor

CUITextString::CUITextString(void)
{
	m_uFlags = textEdit;
	}

// Overridables

void CUITextString::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( !List[0].IsEmpty() ) {

		CString Size = List[0];

		if( Size[0] == '*' ) {

			m_uFlags |= textHide;

			Size = Size.Mid(1);
			}

		m_uLimit = watoi(Size);

		MakeMax(m_uLimit, 1);
		}

	if( !List[1].IsEmpty() ) {
		
		m_uFlags |= textDefault;

		m_Default = List[1];

		C3OemStrings(m_Default);
		}

	if( !List[2].IsEmpty() ) {

		m_uWidth = watoi(List[2]);
		}
	else
		m_uWidth = Min(m_uLimit, 40);

	if( !List[3].IsEmpty() ) {

		m_Units   = List[3];

		m_uFlags |= textUnits;
		}
	}

CString CUITextString::OnGetAsText(void)
{
	return m_pData->ReadString(m_pItem);
	}

UINT CUITextString::OnSetAsText(CError &Error, CString Text)
{
	m_pData->WriteString(m_pItem, Text);

	return saveChange;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Display
//

// Dynamic Class

AfxImplementDynamicClass(CUITextDisplay, CUITextString);

// Constructor

CUITextDisplay::CUITextDisplay(void)
{
	m_uFlags = 0;
	}

// End of File
