
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Group Control Spacer
//

class CRulerWnd : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRulerWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSetFocus(CWnd &Prev);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Group Control
//

// Runtime Class

AfxImplementRuntimeClass(CUIGroup, CUIControl);

// Constructor

CUIGroup::CUIGroup(PCTXT pLabel, CLayItem *pItem)
{
	m_Label       = pLabel;

	m_pItem	      = pItem;

	m_pCtrlLayout = NULL;

	m_pGroupCtrl  = New CRulerWnd;
	}

// Core Overridables

void CUIGroup::OnLayout(CLayFormation *pForm)
{
	FindMetrics();

	CRect Rect1 = CRect(16, m_FontSize.cy + 12, 0, 2);

	CRect Rect2 = CRect(4, 4, 4, 4);

	m_pCtrlLayout = New CLayFormPad(m_pItem,       Rect1, horzNone | vertTop);

	m_pMainLayout = New CLayFormPad(m_pCtrlLayout, Rect2, horzNone | vertNone | horzGrow);

	pForm->AddItem(m_pMainLayout);
	}

void CUIGroup::OnCreate(CWnd &Wnd, UINT &uID)
{
	CRect Rect  = m_pCtrlLayout->GetRect();

	Rect.bottom = Rect.top + m_FontSize.cy + 8;

	m_pGroupCtrl->Create( m_Label,
			      WS_CHILD,
			      Rect,
			      Wnd,
			      uID++
			      );

	m_pGroupCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pGroupCtrl);
	}

void CUIGroup::OnPosition(void)
{
	CRect Rect  = m_pCtrlLayout->GetRect();

	Rect.bottom = Rect.top + m_FontSize.cy + 8;

	m_pGroupCtrl->MoveWindow(Rect, TRUE);
	}
	
// Implementation

void CUIGroup::FindMetrics(void)
{
	CClientDC DC(NULL);

	DC.Select(afxFont(Dialog));

	m_FontSize = DC.GetTextExtent(L"X");

	DC.Deselect();
	}

//////////////////////////////////////////////////////////////////////////
//
// Group Control Spacer
//

// Dynamic Class

AfxImplementDynamicClass(CRulerWnd, CCtrlWnd);

// Constructor

CRulerWnd::CRulerWnd(void)
{
	}

// Message Map

AfxMessageMap(CRulerWnd, CCtrlWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_LBUTTONDOWN)

	AfxMessageEnd(CRulerWnd)
	};

// Message Handlers

BOOL CRulerWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CRulerWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.Select(afxFont(Bolder));

	DC.SetTextColor(afxColor(TabGroup));

	DC.SetBkColor(afxColor(TabFace));

	CRect   Rect = GetClientRect();

	CString Text = GetWindowText();

	CSize   Size = DC.GetTextExtent(Text);

	DC.FillRect(Rect, afxBrush(TabFace));

	Rect.left += 0;

	Rect.top  += 4;

	DC.TextOut(Rect.left, Rect.top, Text);

	Rect.left += Size.cx + 8;

	Rect.top  += Size.cy / 2;

	DC.DrawTopEdge(Rect, 1, afxBrush(3dShadow));

	DC.Deselect();
	}

void CRulerWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	CWnd *pCtrl = this;

	for(;;) {

		if( (pCtrl = pCtrl->GetWindowPtr(GW_HWNDPREV))->IsWindow() ) {

			if( pCtrl->IsKindOf(AfxRuntimeClass(CRulerWnd)) ) {

				break;
				}

			continue;
			}

		pCtrl = GetWindowPtr(GW_HWNDFIRST);

		break;
		}

	while( pCtrl->IsWindow() ) {
		
		if( pCtrl->GetWindowStyle() & WS_TABSTOP ) {

			if( pCtrl->IsWindowEnabled() ) {

				pCtrl->SetFocus();

				break;
				}
			}

		pCtrl = pCtrl->GetWindowPtr(GW_HWNDNEXT);
		}
	}

// End of File
