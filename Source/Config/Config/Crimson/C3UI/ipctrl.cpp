
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- IP Address Control
//

// Dynamic Class

AfxImplementDynamicClass(CUIIPAddress, CUIControl);

// Constructor

CUIIPAddress::CUIIPAddress(void)
{
	m_dwStyle     = 0;

	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CIPAddrCtrl;
	}

// Core Overridables

void CUIIPAddress::OnBind(void)
{
	CUIElement::OnBind();

	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}
	}

void CUIIPAddress::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayItemText(L" 8888.8888.8888.8888 ", 4, 1);

	m_pMainLayout = New CLayFormPad(m_pDataLayout, horzNone | vertCenter);

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIIPAddress::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_Font.Create(CClientDC(Wnd), afxDlgFont, 8, 0);

	m_pTextCtrl->Create(	m_Label,
				0,
				m_pTextLayout->GetRect(),
				Wnd,
				0
				);

	m_pDataCtrl->Create(	L"",
				WS_TABSTOP | m_dwStyle,
				m_pDataLayout->GetRect(),
				Wnd,
				uID++
				);

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(m_Font);

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, EN_KILLFOCUS);
	}

void CUIIPAddress::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(m_pDataLayout->GetRect(), TRUE);
	}

// Data Overridables

void CUIIPAddress::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	m_pDataCtrl->SetAddress(Parse(Data));
	}

UINT CUIIPAddress::OnSave(BOOL fUI)
{
	UINT uData = m_pDataCtrl->GetAddress();

	return StdSave(fUI, Format(uData));
	}

// Implementation

UINT CUIIPAddress::Parse(CString Text)
{
	CStringArray List;

	Text.Tokenize(List, '.');

	BYTE b1 = BYTE(watoi(List[3]));
	BYTE b2 = BYTE(watoi(List[2]));
	BYTE b3 = BYTE(watoi(List[1]));
	BYTE b4 = BYTE(watoi(List[0]));

	UINT uData = 0;

	((PBYTE) &uData)[0] = b1;
	((PBYTE) &uData)[1] = b2;
	((PBYTE) &uData)[2] = b3;
	((PBYTE) &uData)[3] = b4;

	return uData;
	}

CString CUIIPAddress::Format(UINT uData)
{
	BYTE b1 = PBYTE(&uData)[0];
	BYTE b2 = PBYTE(&uData)[1];
	BYTE b3 = PBYTE(&uData)[2];
	BYTE b4 = PBYTE(&uData)[3];

	return CPrintf(L"%u.%u.%u.%u", b4, b3, b2, b1);
	}

// End of File
