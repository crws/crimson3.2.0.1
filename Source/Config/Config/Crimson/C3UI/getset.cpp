
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Property Get-and-Set Object
//

// Constructor

CUIGetSet::CUIGetSet(void)
{
	m_pItem   = NULL;

	m_pSchema = NULL;

	m_pHost   = New CUIStdHost(this, NULL);
	}

// Destructor

CUIGetSet::~CUIGetSet(void)
{
	CleanUp();

	delete m_pHost;
	}

// Attributes

BOOL CUIGetSet::HadFailures(void) const
{
	return m_fFail;
	}

// Operations

BOOL CUIGetSet::GetProps(CStringArray &Props, CUIItem *pRoot)
{
	m_pRoot = pRoot;

	if( MakeUI() ) {

		if( GetAsText(Props) ) {

			CleanUp();

			return TRUE;
			}
		}

	CleanUp();

	return FALSE;
	}

BOOL CUIGetSet::SetProps(CUIItem *pRoot, CStringArray &Props)
{
	m_fFail = FALSE;

	m_pRoot = pRoot;

	if( MakeUI() ) {

		if( SetAsText(Props) ) {

			CleanUp();

			return TRUE;
			}
		}

	CleanUp();

	return FALSE;
	}

// IUICreate

void CUIGetSet::StartPage(UINT uCols)
{
	}

void CUIGetSet::ResetPage(UINT uCols)
{
	}

void CUIGetSet::StartOverlay(void)
{
	}

void CUIGetSet::StartGroup(UINT uCols)
{
	}

void CUIGetSet::StartGroup(PCTXT pName, UINT uCols)
{
	}

void CUIGetSet::StartGroup(PCTXT pName, UINT uCols, BOOL fEqual)
{
	}

void CUIGetSet::StartTable(PCTXT pName, UINT uCols)
{
	}

void CUIGetSet::AddColHeadExtraRow(void)
{
	}

void CUIGetSet::AddColHead(PCTXT pName)
{
	}

void CUIGetSet::AddRowHead(PCTXT pName)
{
	}

BOOL CUIGetSet::AddUI(CItem *pItem, CString Object, CString Tag)
{
	if( !Tag.IsEmpty() ) {

		if( pItem = GetObject(pItem, Object) ) {

			LoadSchema(pItem);

			CUIData const *pUIData = m_pSchema->GetUIData(Tag);

			if( !pUIData ) {

				UIError(L"cannot find tag %s in ui schema", Tag);

				return FALSE;
				}

			return AddUI(pItem, Object, pUIData);
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CUIGetSet::AddUI(CItem *pItem, CString Object, CUIData const *pUIData)
{
	CEntry * pEntry   = New CEntry;

	pEntry->m_uPage   = m_uPage;

	pEntry->m_UIData  = *pUIData;

	pEntry->m_pItem   = pItem;

	pEntry->m_pText   = AfxNewObject(CUITextElement, pUIData->m_ClassText);

	pEntry->m_fShow   = TRUE;

	pEntry->m_fEnable = TRUE;

	pEntry->m_pText->Bind(pItem, pUIData);

	m_List.Append(pEntry);

	return TRUE;
	}

void CUIGetSet::AddButton(PCTXT pName, PCTXT pTip, UINT uID)
{
	}

void CUIGetSet::AddButton(PCTXT pName, UINT uID)
{
	}

void CUIGetSet::AddButton(PCTXT pName, PCTXT pTip, PCTXT pTag)
{
	}

void CUIGetSet::AddButton(PCTXT pName, PCTXT pTag)
{
	}

void CUIGetSet::AddNarrative(PCTXT pText)
{
	}

void CUIGetSet::AddElement(CUIElement *pUI)
{
	}

void CUIGetSet::EndTable(void)
{
	}

void CUIGetSet::EndGroup(BOOL fExpand)
{
	}

void CUIGetSet::EndOverlay(void)
{
	}

void CUIGetSet::EndPage(BOOL fExpand)
{
	}

void CUIGetSet::SetBorder(int nBorder)
{
	}

void CUIGetSet::SetBorder(CRect const &Border)
{
	}

void CUIGetSet::NoRecycle(void)
{
	}

void CUIGetSet::NoScroll(void)
{
	}

// IUICore

PUITEXT CUIGetSet::OnFindUI(CLASS Class, CItem *pItem, PCTXT pTag)
{
	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CEntry *pEntry = m_List[n];

		if( pEntry->m_uPage == m_uPage ) {

			if( pEntry->m_pItem == (pItem ? pItem : m_pItem) ) {

				if( !pTag || pEntry->m_UIData.m_Tag == pTag ) {

					return pEntry->m_pText;
					}
				}
			}
		}

	return NULL;
	}

BOOL CUIGetSet::OnShowUI(CItem *pItem, PCTXT pTag, BOOL fShow)
{
	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CEntry *pEntry = m_List[n];

		if( pEntry->m_uPage == m_uPage ) {

			if( pEntry->m_pItem == (pItem ? pItem : m_pItem) ) {

				if( !pTag || pEntry->m_UIData.m_Tag == pTag ) {

					pEntry->m_fShow = fShow;

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CUIGetSet::OnEnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable)
{
	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CEntry *pEntry = m_List[n];

		if( pEntry->m_uPage == m_uPage ) {

			if( pEntry->m_pItem == (pItem ? pItem : m_pItem) ) {

				if( !pTag || pEntry->m_UIData.m_Tag == pTag ) {

					pEntry->m_fEnable = fEnable;

					return TRUE;
					}
				}
			}
		}

	return TRUE;
	}

BOOL CUIGetSet::OnUpdateUI(CItem *pItem, PCTXT pTag)
{
	return TRUE;
	}

void CUIGetSet::OnRemakeUI(void)
{
	CleanUp();

	MakeUI();
	}

BOOL CUIGetSet::OnHasUndo(void)
{
	return FALSE;
	}

BOOL CUIGetSet::OnInReplay(void)
{
	return FALSE;
	}

void CUIGetSet::OnExecExtraCmd(CCmd *pCmd)
{
	delete pCmd;
	}

void CUIGetSet::OnSaveExtraCmd(CCmd *pCmd)
{
	delete pCmd;
	}

void CUIGetSet::OnSendUpdate(UINT uType)
{
	}

BOOL CUIGetSet::KillUndoList(void)
{
	return !OnHasUndo();
	}

// Schema Management

void CUIGetSet::LoadSchema(CItem *pItem)
{
	if( m_pSchema ) {

		if( m_pSchema->CheckType(pItem) ) {

			return;
			}

		delete m_pSchema;
		}

	m_pSchema = New CUISchema(pItem);
	}

void CUIGetSet::FreeSchema(void)
{
	if( m_pSchema ) {

		delete m_pSchema;

		m_pSchema = NULL;
		}
	}

// Sub-Item Location

CItem * CUIGetSet::GetObject(CItem *pItem, CString Object)
{
	if( Object.GetLength() ) {
		
		if( Object.CompareN(L"root") ) {

			IDataAccess *pData = pItem->GetDataAccess(Object);

			if( pData ) {

				pItem = pData->GetObject(pItem);
				
				return pItem;
				}

			UIError(L"cannot find child object %s", Object);

			return NULL;
			}
		}

	return pItem;
	}

// Implementation

BOOL CUIGetSet::MakeUI(void)
{
	if( LoadPages() ) {

		if( BuildChildMap(L"", m_pRoot) ) {

			if( AddRootToMap() ) {
	
				if( SendEnables() ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

void CUIGetSet::CleanUp(void)
{
	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CEntry *pEntry = m_List[n];

		delete pEntry->m_pText;

		delete pEntry;
		}

	m_List.Empty();

	m_NameMap.Empty();

	m_ItemMap.Empty();

	FreeSchema();
	}

BOOL CUIGetSet::LoadPages(void)
{
	CUIPageList List;

	m_pRoot->OnLoadPages(&List);

	m_uPages = List.GetCount();

	for( m_uPage = 0; m_uPage < m_uPages; m_uPage++ ) {

		CUIPage *pPage = List[m_uPage];

		pPage->LoadIntoView(this, m_pRoot);
		}

	return TRUE;
	}

BOOL CUIGetSet::SendEnables(void)
{
	for( m_uPage = 0; m_uPage < m_uPages; m_uPage++ ) {

		INDEX Index = m_NameMap.GetHead();

		while( !m_NameMap.Failed(Index) ) {

			CItem *pItem = m_NameMap.GetName(Index);

			if( pItem != m_pRoot ) {

				SendEnable(pItem);
				}

			m_NameMap.GetNext(Index);
			}

		SendEnable(m_pRoot);
		}

	return TRUE;
	}

BOOL CUIGetSet::SendEnable(CItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

		CUIItem *pUI = (CUIItem *) pItem;

		SendChange(pUI, L"");

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIGetSet::SendChange(CUIItem *pItem, CString const &Tag)
{
	CItem *pSave = m_pItem;

	m_pItem = pItem;
	
	pItem->OnUIChange(m_pHost, pItem, Tag);

	m_pItem = pSave;

	return TRUE;
	}

BOOL CUIGetSet::AddRootToMap(void)
{
	m_NameMap.Insert(m_pRoot, L"Core\nCore");

	m_ItemMap.Insert(L"Core", m_pRoot);

	return TRUE;
	}

BOOL CUIGetSet::BuildChildMap(CString Root, CUIItem *pItem)
{
	LoadSchema(pItem);

	CMetaList *pList = pItem->FindMetaList();

	UINT      uCount = pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = pList->FindData(n);

		UINT             uType = pMeta->GetType();

		if( uType == metaObject || uType == metaVirtual ) {

			CItem *pChild = pMeta->GetObject(pItem);

			if( pChild ) {

				if( pChild->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

					CUIItem *pUI = (CUIItem *) pChild;

					CString  Tag = pMeta->GetTag();

					BuildChildMap(Tag + L' ', pUI);

					LoadSchema(pItem);

					CString        Child   = Root + Tag;

					CUIData const *pUIData = m_pSchema->GetUIData(Child);

					CString        Label   = pUIData ? pUIData->m_Label : Child;

					CString        Build   = Child + L'\n' + Label;

					AfxVerify(m_NameMap.Insert(pChild, Build));

					AfxVerify(m_ItemMap.Insert(Child,  pChild));
					}
				}
			}

		if( uType == metaCollect ) {

			CItem *pChild = pMeta->GetObject(pItem);

			if( pChild ) {

				CItemList *pList = (CItemList *) pChild;

				INDEX Index = pList->GetHead();

				UINT  uPos  = 1;

				while( !pList->Failed(Index) ) {

					CItem *pSlot = pList->GetItem(Index);

					if( pSlot->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

						LoadSchema(pItem);

						CString        Child   = Root + pMeta->GetTag();

						CUIData const *pUIData = m_pSchema->GetUIData(Child);

						CString        Label   = pUIData ? pUIData->m_Label : Child;

						Child = CPrintf(L"%s %u", Child, uPos);

						CString        Build   = Child + L'\n' + Label;

						AfxVerify(m_NameMap.Insert(pSlot, Build));

						AfxVerify(m_ItemMap.Insert(Child, pSlot));

						uPos++;
						}

					pList->GetNext(Index);
					}
				}
			}
		}

	return TRUE;
	}

BOOL CUIGetSet::GetAsText(CStringArray &Props)
{
	// LATER -- Do we need these in order?

	UINT c = m_List.GetCount();

	for( INT p = 0; p < 1; p++ ) {

		for( UINT n = 0; n < c; n++ ) {

			CEntry *pEntry = m_List[n];

			BOOL fForce = FALSE;

			if( pEntry->m_UIData.m_Tag == "HasSP" ) {

				fForce = TRUE;
				}

			if( (pEntry->m_fEnable || fForce) && pEntry->m_fShow ) {

				if( !pEntry->m_pText->HasFlag(textDummy) ) {

					CString Child;

					CString Group;

					INDEX   Find = m_NameMap.FindName(pEntry->m_pItem);

					if( !m_NameMap.Failed(Find) ) {

						CString Text = m_NameMap.GetData(Find);

						Child = Text.StripToken(L'\n');

						Group = Text;
						}

					if( Child.IsEmpty() ) {

						continue;
						}

					CString Tag   = pEntry->m_UIData.m_Tag;

					CString Label = pEntry->m_UIData.m_Label;

					CString Value = pEntry->m_pText->GetAsText();

					Encode(Value);

					CString Text;

					Text += Child;	Text += '\n';

					Text += Tag;	Text += '\n';

					Text += Value;	Text += '\n';

					Text += Group;	Text += '\n';

					Text += Label;

					Props.Append(Text);
					}
				}
			}
		}

	return TRUE;
	}

BOOL CUIGetSet::SetAsText(CStringArray &Props)
{
	UINT c = Props.GetCount();

	for( UINT p = 0; p < c; p++ ) {

		CStringArray Parts;

		if( Props[p].GetLength() ) {

			Props[p].Tokenize(Parts, '\n');

			CString const &Child = Parts[0];

			CString const &Tag   = Parts[1];

			CString        Value = Parts[2];

			INDEX   Find         = m_ItemMap.FindName(Child);

			if( !m_ItemMap.Failed(Find) ) {

				CItem *pItem = m_ItemMap.GetData(Find);

				UINT   e     = m_List.GetCount();

				for( UINT n = 0; n < e; n++ ) {

					CEntry *pEntry = m_List[n];

					if( pEntry->m_UIData.m_Tag == Tag ) {

						if( pEntry->m_pItem == pItem ) {

							if( pEntry->m_fEnable && pEntry->m_fShow ) {

								Decode(Value);
								
								SetValue(pEntry, Value);
								}
							else
								m_fFail = TRUE;

							break;
							}
						}
					}
				}
			}
		}

	return TRUE;
	}

BOOL CUIGetSet::SetValue(CEntry *pEntry, CString const &Value)
{
	if( !pEntry->m_pText->HasFlag(textDummy) ) {

		CError Error(FALSE);

		UINT uCode = pEntry->m_pText->SetAsText(Error, Value);

		if( !Error.IsOkay() ) {
			
			uCode = pEntry->m_pText->SetAsText(Error, L"WAS " + Value);
			}

		if( uCode == saveChange ) {

			CUIItem *pUI = m_pRoot;

			if( pEntry->m_pItem->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

				pUI = (CUIItem *) pEntry->m_pItem;
				}

			m_uPage = pEntry->m_uPage;

			SendChange(pUI, pEntry->m_UIData.m_Tag);

			return TRUE;
			}

		if( uCode == saveError ) {

			m_fFail = TRUE;
			}
		
		return FALSE;
		}

	return TRUE;
	}

void CUIGetSet::UIError(PCTXT pText, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pText);

	CString Text;

	Text.VPrintf(pText, pArgs);

	va_end(pArgs);

	AfxTrace(L"ERROR: %s\n", Text);

	AfxAssert(FALSE);
	}

void CUIGetSet::Encode(CString &Value)
{
	Value.Replace(L"\r\n", L"\x00B2");

	Value.Replace(L"\r",   L"\x00B3");

	Value.Replace(L"\t",   L"\x00B9");

	Value.Replace(L",",    L"\x00AC");
	}

void CUIGetSet::Decode(CString &Value)
{
	Value.Replace(L"\x00B2", L"\r\n");
	
	Value.Replace(L"\x00B3", L"\r");

	Value.Replace(L"\x00B9", L"\t");

	Value.Replace(L"\x00AC", L",");
	}

// End of File
