
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimsm_pCore->On 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// User Interface Standard Host
//

// Constructor

CUIStdHost::CUIStdHost(IUICore *pCore, CViewWnd *pWnd)
{
	m_pCore = pCore;

	m_pWnd  = pWnd;
	}

// Bound Window

BOOL CUIStdHost::HasWindow(void)
{
	return m_pWnd ? TRUE : FALSE;
	}

CViewWnd & CUIStdHost::GetWindow(void)
{
	return *m_pWnd;
	}

// Location

CUITextElement * CUIStdHost::FindUI(CLASS Class, CItem *pItem, PCTXT pTag)
{
	return m_pCore->OnFindUI(Class, pItem, pTag);
	}

CUITextElement * CUIStdHost::FindUI(CItem *pItem, PCTXT pTag)
{
	return m_pCore->OnFindUI(NULL, pItem, pTag);
	}

CUITextElement * CUIStdHost::FindUI(CLASS Class, PCTXT pTag)
{
	return m_pCore->OnFindUI(Class, NULL, pTag);
	}

CUITextElement * CUIStdHost::FindUI(PCTXT pTag)
{
	return m_pCore->OnFindUI(NULL, NULL, pTag);
	}

// Creation

void CUIStdHost::RemakeUI(void)
{
	m_pCore->OnRemakeUI();
	}

// Showing

void CUIStdHost::ShowUI(BOOL fShow)
{
	m_pCore->OnShowUI(NULL, NULL, fShow);
	}

void CUIStdHost::ShowUI(CItem *pItem, BOOL fShow)
{
	m_pCore->OnShowUI(pItem, NULL, fShow);
	}

BOOL CUIStdHost::ShowUI(CItem *pItem, PCTXT pTag, BOOL fShow)
{
	return m_pCore->OnShowUI(pItem, pTag, fShow);
	}

BOOL CUIStdHost::ShowUI(CItem *pItem, PCSTR pTag, BOOL fShow)
{
	return m_pCore->OnShowUI(pItem, CString(pTag), fShow);
	}

BOOL CUIStdHost::ShowUI(PCTXT pTag, BOOL fShow)
{
	return m_pCore->OnShowUI(NULL, pTag, fShow);
	}

BOOL CUIStdHost::ShowUI(PCSTR pTag, BOOL fShow)
{
	return m_pCore->OnShowUI(NULL, CString(pTag), fShow);
	}

// Enabling

void CUIStdHost::EnableUI(BOOL fEnable)
{
	m_pCore->OnEnableUI(NULL, NULL, fEnable);
	}

void CUIStdHost::EnableUI(CItem *pItem, BOOL fEnable)
{
	m_pCore->OnEnableUI(pItem, NULL, fEnable);
	}

BOOL CUIStdHost::EnableUI(CItem *pItem, PCTXT pTag, BOOL fEnable)
{
	return m_pCore->OnEnableUI(pItem, pTag, fEnable);
	}

BOOL CUIStdHost::EnableUI(CItem *pItem, PCSTR pTag, BOOL fEnable)
{
	return m_pCore->OnEnableUI(pItem, CString(pTag), fEnable);
	}

BOOL CUIStdHost::EnableUI(PCTXT pTag, BOOL fEnable)
{
	return m_pCore->OnEnableUI(NULL, pTag, fEnable);
	}

BOOL CUIStdHost::EnableUI(PCSTR pTag, BOOL fEnable)
{
	return m_pCore->OnEnableUI(NULL, CString(pTag), fEnable);
	}

// Updating

void CUIStdHost::UpdateUI(void)
{
	m_pCore->OnUpdateUI(NULL, NULL);
	}

void CUIStdHost::UpdateUI(CItem *pItem)
{
	m_pCore->OnUpdateUI(pItem, NULL);
	}

BOOL CUIStdHost::UpdateUI(CItem *pItem, PCTXT pTag)
{
	return m_pCore->OnUpdateUI(pItem, pTag);
	}

BOOL CUIStdHost::UpdateUI(CItem *pItem, PCSTR pTag)
{
	return m_pCore->OnUpdateUI(pItem, CString(pTag));
	}

BOOL CUIStdHost::UpdateUI(PCTXT pTag)
{
	return m_pCore->OnUpdateUI(NULL, pTag);
	}

BOOL CUIStdHost::UpdateUI(PCSTR pTag)
{
	return m_pCore->OnUpdateUI(NULL, CString(pTag));
	}

// Undo Mode

BOOL CUIStdHost::HasUndo(void)
{
	return m_pCore->OnHasUndo();
	}

BOOL CUIStdHost::InReplay(void)
{
	return m_pCore->OnInReplay();
	}

// Commands

void CUIStdHost::ExecExtraCmd(CCmd *pCmd)
{
	m_pCore->OnExecExtraCmd(pCmd);
	}

void CUIStdHost::SaveExtraCmd(CCmd *pCmd)
{
	m_pCore->OnSaveExtraCmd(pCmd);
	}

// Updates

void CUIStdHost::SendUpdate(UINT uType)
{
	m_pCore->OnSendUpdate(uType);
	}

// Undo Kill

BOOL CUIStdHost::KillUndoList(void)
{
	return m_pCore->KillUndoList();
	}

// End of File
