
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Pick Control
//

// Dynamic Class

AfxImplementDynamicClass(CUIPick, CUIControl);

// Constructor

CUIPick::CUIPick(void)
{
	m_fFlexData   = TRUE;

	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pPickLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CDropEditCtrl(this);

	m_pPickCtrl   = NewHotLink();
	}

// Core Overridables

void CUIPick::OnBind(void)
{
	CUIElement::OnBind();

	if( !m_fTable ) {
	
		m_Label = GetLabel() + L":";
		}

	if( m_pText->HasFlag(textExpand) ) {

		m_Verb = m_pText->GetVerb() + L"...";

		return;
		}

	m_Verb = IDS_UI_EDIT;
	}

void CUIPick::OnLayout(CLayFormation *pForm)
{
	UINT uSize = m_pText->GetWidth();

	m_pDataLayout = New CLayItemText(uSize, 1, 2);

	m_pPickLayout = New CLayItemText(m_Verb, CSize(6, 4), CSize(1, 1));

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pDataLayout, horzLeft | vertCenter));
	
	m_pMainLayout->AddItem(New CLayFormPad(m_pPickLayout, horzNone | vertCenter));

	if( m_Label.IsEmpty() ) {

		m_pTextLayout = New CLayItemText(m_Label, 0, 1);

		pForm->AddItem(m_pTextLayout);

		pForm->AddItem(m_pMainLayout);

		return;
		}

	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIPick::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( ES_READONLY | ES_AUTOHSCROLL | WS_BORDER,
			     GetDataCtrlRect(),
			     Wnd,
			     uID++
			     );

	m_pPickCtrl->Create( m_Verb,
			     BS_PUSHBUTTON | WS_TABSTOP,
			     m_pPickLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	m_pPickCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl);

	AddControl(m_pPickCtrl);
	}

void CUIPick::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(GetDataCtrlRect(), TRUE);

	m_pPickCtrl->MoveWindow(m_pPickLayout->GetRect(), TRUE);
	}

// Data Overridables

void CUIPick::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	m_pDataCtrl->SetWindowText(Data);
	}

UINT CUIPick::OnSave(BOOL fUI)
{
	return saveSame;
	}

// Notification Handlers

BOOL CUIPick::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pPickCtrl->GetID() ) {

		if( m_pText->HasFlag(textExpand) ) {

			if( m_pText->ExpandData(*m_pPickCtrl) ) {

				LoadUI();
				
				return TRUE;
				}			
			}
		else {
			CString Title  = IDS_EDIT_CAPTION;

			CString Prompt = m_UIData.m_Label;

			CString Text   = m_pText->GetAsText();

			CStringDialog Dlg(Title, Prompt, Text);

			if( Dlg.Execute() ) {

				CError Error(TRUE);

				UINT uCode = m_pText->SetAsText(Error, Dlg.GetData());

				if( uCode == saveError ) {

					Error.Show(*afxMainWnd);

					return FALSE;
					}

				if( uCode == saveChange ) {
					
					LoadUI();

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

CRect CUIPick::GetDataCtrlRect(void)
{
	CRect Rect;
	
	Rect = m_pDataLayout->GetRect() + 2;

	Rect.left += 1;
	
	return Rect;
	}

// End of File
