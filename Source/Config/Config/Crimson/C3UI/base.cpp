
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Base User Interface Element
//

// Runtime Class

AfxImplementRuntimeClass(CUIBaseElement, CObject);

// Constructor

CUIBaseElement::CUIBaseElement(void)
{
	m_pItem	= NULL;

	m_pData = NULL;
	}

// Binding

BOOL CUIBaseElement::Bind(CItem *pItem, CUIData const *pUIData)
{
	if( !m_pItem ) {

		m_pItem  = pItem;

		m_pData  = pItem->GetDataAccess(pUIData->m_Tag);

		m_UIData = *pUIData;

		if( m_pData ) {

			OnBind();

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// Attributes

CItem * CUIBaseElement::GetItem(void) const
{
	return m_pItem;
	}

IDataAccess * CUIBaseElement::GetDataAccess(void) const
{
	return m_pData;
	}

CUIData const & CUIBaseElement::GetUIData(void) const
{
	return m_UIData;
	}

CString	CUIBaseElement::GetTag(void) const
{
	return m_UIData.m_Tag;
	}

CString CUIBaseElement::GetLabel(void) const
{
	return m_UIData.m_Label;
	}

CString CUIBaseElement::GetLocal(void) const
{
	return m_UIData.m_Local;
	}

CString CUIBaseElement::GetFormat(void) const
{
	return m_UIData.m_Format;
	}

CString CUIBaseElement::GetStatus(void) const
{
	return m_UIData.m_Status;
	}

// Operations

void CUIBaseElement::RebindUI(CItem *pItem)
{
	m_pItem = pItem;

	m_pData = pItem->GetDataAccess(m_UIData.m_Tag);

	OnRebind();
	}

// Overidables

void CUIBaseElement::OnBind(void)
{
	}

void CUIBaseElement::OnRebind(void)
{
	}

// End of File
