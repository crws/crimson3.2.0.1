
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// User Interface Schema
//

// Runtime Class

AfxImplementRuntimeClass(CUISchema, CObject);

// Constructor

CUISchema::CUISchema(CItem *pItem)
{
	LoadData(m_Class = AfxPointerClass(pItem));
	}

// Destructor

CUISchema::~CUISchema(void)
{
	}

// Attributes

BOOL CUISchema::CheckType(CItem *pItem) const
{
	return AfxPointerClass(pItem) == m_Class;
	}

// UI Data Access

CUIData const * CUISchema::GetUIData(PCTXT pTag) const
{
	INDEX n = m_Dict.FindName(pTag);

	if( !m_Dict.Failed(n) ) {

		UINT uPos = m_Dict[n];

		return &m_List[uPos];
		}

	return NULL;
	}

// Operations

BOOL CUISchema::LoadFromFile(PCTXT pFile)
{
	if( m_List.IsEmpty() ) {

		CFilename Name;
		
		Name    = afxModule->GetFilename();
			
		Name    = Name.GetDirectory() + L"schema\\" + pFile + L".csv";

		FILE *p = _wfopen(Name, L"rb");

		if( p ) {

			LCID    Lang  = GetThreadLocale();

			DWORD   Prim  = PRIMARYLANGID(Lang);

			BOOL    fLang = TRUE;

			UINT    uLine = 0;

			CString Mark  = L"";

			switch( Prim ) {

			/*	case LANG_FRENCH:

					Mark = L"-FRENCH\r\n";

					break;
			*/
				default:
					fLang = FALSE;

					break;
				}

			while( !feof(p) ) {

				TCHAR sLine[256];

				char  sTemp[256];

				sTemp[0] = 0;
				
				fgets(sTemp, elements(sTemp), p);

				if( uLine++ ) {

					for( int n = 0; sLine[n] = BYTE(sTemp[n]); n++ );

					if( fLang ) {

						CString Line = sLine;

						if( Line == Mark ) {

							fLang = FALSE;

							uLine = 0;
							}

						continue;
						}
					
					if( wstrlen(sLine) ) {

						if( sLine[0] == '-' ) {

							break;
							}

						CStringArray Parts;

						CString      Line;
						
						Line = sLine;

						Line.TrimBoth();

						Line.Tokenize(Parts, ',');

						CUIData Data;

						if( FindClasses(Data, Parts[3], TRUE) ) {

							Data.m_Tag     = Parts[0];

							Data.m_Label   = Parts[1];

							Data.m_Local   = Parts[2];

							Data.m_Format  = Parts[4];

							Data.m_Tip     = Parts[5];

							C3OemStrings(Data.m_Format);

							C3OemStrings(Data.m_Tip);

							if( !Data.m_Local.IsEmpty() ) {

								Data.m_Label = Data.m_Local;
								}

							Data.m_Label.TrimBoth();

							Data.m_Local.TrimBoth();

							UINT uPos = m_List.Append(Data);

							m_Dict.Insert(Data.m_Tag, uPos);
							}

						continue;
						}

					break;
					}
				}

			fclose(p);

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

void CUISchema::LoadData(CLASS Class)
{
	for(;;) {

		if( Class == AfxRuntimeClass(CMetaItem) ) {

			break;
			}

		if( Class == AfxRuntimeClass(CUIItem) ) {

			break;
			}

		CString NameW = CString(Class->GetClassName()) + L"_Schema";

		CString NameA = CString(Class->GetClassName()) + L"UIList";

		CEntity EntityW(NameW, Class->GetModule());

		CEntity EntityA(NameA, Class->GetModule());

		LCID Local = GetThreadLocale();

		LCID Basic = MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT);

		UINT uPass = (Local == Basic ? 1 : 2);
		
		BOOL fAnsi = FALSE;

		for( UINT n = 0; n < uPass; n++ ) {

			HANDLE hSchema = NULL;
			
			// cppcheck-suppress knownConditionTrueFalse

			if( hSchema == NULL ) {
				
				if( hSchema = afxModule->LoadResource(EntityW, RT_RCDATA) ) {

					fAnsi = FALSE;
					}
				}

			if( hSchema == NULL ) {
				
				if( hSchema = afxModule->LoadResource(EntityA, RT_RCDATA) ) {

					fAnsi = TRUE;
					}
				}

			if( hSchema ) {

				PCTXT pSchema = PCTXT(LockResource(hSchema));

				PTXT  pDelete = NULL;

				UINT  uLength = 0;

				if( fAnsi ) {

					PCSTR pAnsi = PCSTR(pSchema);

					UINT  uSize = 1;

					while( pAnsi[uSize] || pAnsi[uSize-1] ) {

						uSize++;
						}

					pDelete = New TCHAR [ ++uSize ];

					for( UINT n = 0; n < uSize; n++ ) {

						pDelete[n] = BYTE(pAnsi[n]);
						}

					UnlockResource(hSchema);

					FreeResource  (hSchema);

					pSchema = pDelete;
					}
					
				while( uLength = wstrlen(pSchema) ) {

					CStringArray Parts;

					CString(pSchema).Tokenize(Parts, 6U, ',');

					CUIData Data;

					if( FindClasses(Data, Parts[3], fAnsi) ) {

						Data.m_Tag       = Parts[0];

						Data.m_Label     = Parts[1];

						Data.m_Local     = Parts[2];

						Data.m_Format    = Parts[4];

						Data.m_Tip       = Parts[5];

						C3OemStrings(Data.m_Format);
						
						C3OemStrings(Data.m_Tip);

						if( Data.m_Local.IsEmpty() ) {

							Data.m_Local = Data.m_Label;
							}

						if( !m_Dict.FindName(Data.m_Tag) ) {

							UINT uPos = m_List.Append(Data);

							m_Dict.Insert(Data.m_Tag, uPos);
							}
						}

					pSchema += uLength + 1;
					}

				if( fAnsi ) {

					delete [] pDelete;
					}
				else {
					UnlockResource(hSchema);

					FreeResource  (hSchema);
					}
				}

			SetThreadLocale(n ? Local : Basic);
			}

		Class = Class->GetBaseClass();
		}
	}

BOOL CUISchema::FindClasses(CUIData &Data, CString Name, BOOL fLegacy)
{
	if( Name.GetLength() ) {

		if( Name.Find('/') == NOTHING ) {

			if( fLegacy ) {

				// REV3 -- Should the legacy list be customizable
				// by the client, rather than having all the MC
				// stuff hard-coded in this file?
				
				if( FindLegacy(Data, Name) ) {

					return TRUE;
					}
				}

			CLASS Class = AfxNamedClass(Name);

			if( Class ) {

				if( Class->IsKindOf(AfxRuntimeClass(CUIElement)) ) {

					Data.m_ClassText = NULL;

					Data.m_ClassUI   = Class;

					return TRUE;
					}

				if( Class->IsKindOf(AfxRuntimeClass(CUITextElement)) ) {

					Data.m_ClassText = Class;

					Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

					return TRUE;
					}
				}

			if( FindLegacy(Data, Name) ) {

				return TRUE;
				}
			}
		else {
			CString Work = Name;

			CString Text = L"CUIText" + Work.StripToken('/');

			CString UI   = L"CUI"     + Work.StripToken('/');

			if( Text == L"CUITextIPAddress" ) {

				Data.m_ClassText = AfxRuntimeClass(CUITextIPAddress);

				Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);
				}
			else {
				Data.m_ClassText = AfxNamedClass(Text);

				Data.m_ClassUI   = AfxNamedClass(UI);
				}

			if( Data.m_ClassText && Data.m_ClassUI ) {

				return TRUE;
				}

			AfxTrace(L"ERROR: unable to create classes for %s\n", Name);

			AfxAssert(FALSE);

			return FALSE;
			}

		AfxTrace(L"ERROR: unable to find classes for %s\n", Name);

		AfxAssert(FALSE);

		return FALSE;
		}

	Data.m_ClassText = NULL;

	Data.m_ClassUI   = NULL;

	return TRUE;
	}

BOOL CUISchema::FindLegacy(CUIData &Data, CString Name)
{
	if( Name == L"CUIAction" ) {

		// REV3 -- Should this use a categorizer?

		Data.m_ClassText = AfxNamedClass  (L"CUITextAction");

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIBitMask" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextBitMask);

		Data.m_ClassUI   = AfxRuntimeClass(CUIPick);

		return TRUE;
		}

	if( Name == L"CUICheck" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextOnOff);

		Data.m_ClassUI   = AfxRuntimeClass(CUICheck);

		return TRUE;
		}

	if( Name == L"CUIDropDown" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextEnum);

		Data.m_ClassUI   = AfxRuntimeClass(CUIDropDown);

		return TRUE;
		}

	if( Name == L"CUIDropEdit" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextEnumInt);

		Data.m_ClassUI   = AfxRuntimeClass(CUIDropEdit);

		return TRUE;
		}

	if( Name == L"CUIDropIndex" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextEnumIndexed);

		Data.m_ClassUI   = AfxRuntimeClass(CUIDropDown);

		return TRUE;
		}

	if( Name == L"CUIEditASCII" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextASCII);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIEditInteger" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextInteger);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIEditIntHex" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextHexInt);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIEditString" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextString);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIExpression" ) {

		// REV3 -- Should this use a categorizer?

		Data.m_ClassText = AfxNamedClass  (L"CUITextCoded");

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIIPAddress" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextIPAddress);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIPickInteger" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextInteger);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIPickList" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextEnumPick);

		Data.m_ClassUI   = AfxRuntimeClass(CUIPick);

		return TRUE;
		}

	if( Name == L"CUIPortName" ) {

		// REV3 -- This ought to be a drop down of names.

		Data.m_ClassText = AfxRuntimeClass(CUITextInteger);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIScrollHorz" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextInteger);

		Data.m_ClassUI   = AfxRuntimeClass(CUIEditBox);

		return TRUE;
		}

	if( Name == L"CUIPushRow" ) {

		Data.m_ClassText = AfxRuntimeClass(CUITextEnum);

		Data.m_ClassUI   = AfxRuntimeClass(CUIPushRow);

		return TRUE;
		}

	if( Name == L"CUIPIDProcess" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextPIDProcess");

		Data.m_ClassUI   = AfxNamedClass(L"CUIPIDProcess");

		return TRUE;
		}

	if( Name == L"CUIIVIProcess" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextIVIProcess");

		Data.m_ClassUI   = AfxNamedClass(L"CUIIVIProcess");

		return TRUE;
		}

	if( Name == L"CUITC8Process" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextTC8Process");

		Data.m_ClassUI   = AfxNamedClass(L"CUITC8Process");

		return TRUE;
		}

	if( Name == L"CUISGProcess" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextSGProcess");

		Data.m_ClassUI   = AfxNamedClass(L"CUISGProcess");

		return TRUE;
		}

	if( Name == L"CUIOut4Dynamic" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextOut4Dynamic");

		Data.m_ClassUI   = AfxNamedClass(L"CUIOut4Dynamic");

		return TRUE;
		}

	if( Name == L"CUISLCMap" ) {

		Data.m_ClassText = AfxNamedClass  (L"CUITextSLCMap");

		Data.m_ClassUI   = AfxRuntimeClass(CUIDropDown);

		return TRUE;
		}

	if( Name == L"CUIDLCMap" ) {

		Data.m_ClassText = AfxNamedClass  (L"CUITextDLCMap");

		Data.m_ClassUI   = AfxRuntimeClass(CUIDropDown);

		return TRUE;
		}

	if( Name == L"CUIInputProcess" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextInputProcess");

		Data.m_ClassUI   = AfxNamedClass(L"CUIInputProcess");

		return TRUE;
		}

	if( Name == L"CUIPickFile" ) {

		Data.m_ClassText = AfxNamedClass  (L"CUITextFile");

		Data.m_ClassUI   = AfxRuntimeClass(CUIPick);

		return TRUE;
		}

	if( Name == L"CUISGMap" ) {

		Data.m_ClassText = AfxNamedClass  (L"CUISGMap");

		Data.m_ClassUI   = AfxRuntimeClass(CUIDropDown);

		return TRUE;
		}

	if( Name == L"CUIAO8Dynamic" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextAO8Dynamic");

		Data.m_ClassUI   = AfxNamedClass(L"CUIAO8Dynamic");

		return TRUE;
		}

	if( Name == L"CUIMix4AODynamic" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextMix4AODynamic");

		Data.m_ClassUI   = AfxNamedClass(L"CUIMix4AODynamic");

		return TRUE;
	}

	if( Name == L"CUIMix2AODynamic" ) {

		Data.m_ClassText = AfxNamedClass(L"CUITextMix2AODynamic");

		Data.m_ClassUI   = AfxNamedClass(L"CUIMix2AODynamic");

		return TRUE;
	}

	return FALSE;
	}

// End of File
