
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//								
// Bit Mask Page
//

class CBitMaskDataPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CBitMaskDataPage(UINT uCols, BOOL fRev, CStringArray const &List);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		UINT	     m_uCols;
		BOOL	     m_fRev;
		CStringArray m_List;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Bit Mask Page
//

// Runtime Class

AfxImplementRuntimeClass(CBitMaskDataPage, CUIPage);

// Constructor

CBitMaskDataPage::CBitMaskDataPage(UINT uCols, BOOL fRev, CStringArray const &List)
{
	m_uCols = uCols;

	m_fRev  = fRev;

	m_List  = List;
	}

// Operations

BOOL CBitMaskDataPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Options", m_uCols, TRUE);

	UINT c = m_List.GetCount();

	UINT p = 0;
	
	for( UINT n = 0; n < c; n ++ ) {

		CString Name = m_List[n];

		CUIData Data;
		
		Name.TrimBoth();

		if( Name.GetLength() ) {

			Data.m_Tag       = L"Data";

			Data.m_Label     = Name;

			Data.m_ClassText = AfxNamedClass(L"CUITextInteger");

			Data.m_ClassUI   = AfxNamedClass(L"CUICheck");

			Data.m_Format    = CPrintf(L"B%d", m_fRev ? (c - 1 - n) : n);

			pView->AddUI(pItem, L"root", &Data);

			p++;

			if( m_List[n].Right(1) == L"\n" ) {

				while( p % m_uCols ) {

					pView->AddUI(NULL, L"", L"");

					p++;
					}
				}
			}
		}

	pView->EndGroup(TRUE);

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bit Mask
//

// Runtime Class

AfxImplementRuntimeClass(CBitMaskData, CUIItem);

// Constructor

CBitMaskData::CBitMaskData(void)
{
	m_fRev = FALSE;
	}

// Destructor

CBitMaskData::~CBitMaskData(void)
{
	}

// Operations

void CBitMaskData::SetColumns(UINT uCols)
{
	m_uCols = uCols;
	}

void CBitMaskData::SetReverse(BOOL fRev)
{
	m_fRev = fRev;
	}

void CBitMaskData::LoadNames(CStringArray const &List)
{
	m_List = List;
	}

// UI Overridables

BOOL CBitMaskData::OnLoadPages(CUIPageList *pList)
{
	CUIPage *pPage = New CBitMaskDataPage(m_uCols, m_fRev, m_List);

	pList->Append(pPage);

	return FALSE;
	}

// Meta Data

void CBitMaskData::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Bit Mask
//

// Dynamic Class

AfxImplementDynamicClass(CUITextBitMask, CUITextElement);

// Constructor

CUITextBitMask::CUITextBitMask(void)
{
	m_uFlags = textExpand;

	m_uCols  = 1;

	m_fRev   = FALSE;

	m_Verb   = IDS_EDIT;
	}

// Overridables

void CUITextBitMask::OnBind(void)
{
	CUITextElement::OnBind();

	GetFormat().Tokenize(m_DataText, '|');

	if( m_DataText[0] == L"r" ) {

		m_DataText.Remove(0, 1);

		m_fRev = TRUE;
		}

	if( TRUE ) {

		m_uCols  = watoi(m_DataText[0]);

		m_uWidth = watoi(m_DataText[1]);

		m_DataText.Remove(0, 2);
		}

	AfxAssert(m_DataText.GetCount());
	}

CString CUITextBitMask::OnGetAsText(void)
{
	return Format(m_pData->ReadInteger(m_pItem));
	}

UINT CUITextBitMask::OnSetAsText(CError &Error, CString Text)
{
	UINT uPrev = m_pData->ReadInteger(m_pItem);

	UINT uData = Parse(Text);

	if( uData ^ uPrev ) {
		
		m_pData->WriteInteger(m_pItem, uData);

		return saveChange;
		}
	
	return saveSame;
	}

BOOL CUITextBitMask::OnExpand(CWnd &Wnd)
{
	CBitMaskData BitMask;

	BitMask.SetParent(m_pItem);

	BitMask.Init();

	BitMask.m_Data = Parse(GetAsText());

	BitMask.SetColumns(m_uCols);

	BitMask.SetReverse(m_fRev);
	
	BitMask.LoadNames(m_DataText);

	CItemDialog Dialog(&BitMask, CString(IDS_SELECT_OPTIONS));

	if( Dialog.Execute(Wnd) ) {

		SetAsText(CError(FALSE), Format(BitMask.m_Data));
		
		return TRUE;
		}

	return FALSE;	
	}

// Implementation

CString CUITextBitMask::Format(UINT uData)
{
	CString Data;

	UINT c = m_DataText.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		UINT m = FindMask(n);

		if( uData & m ) {

			CString Text = m_DataText[n];
			
			if( !Data.IsEmpty() ) {
				
				Data += L", ";
				}

			Text.TrimBoth();

			Data += Text;
			}
		}

	if( Data.IsEmpty() ) {

		return IDS_NONE;
		}

	return Data;
	}

UINT CUITextBitMask::Parse(CString Text)
{
	UINT uData = 0;

	if( Text != CString(IDS_NONE) ) {

		CStringArray List;

		Text.Tokenize(List, ',');

		for( UINT n = 0; n < List.GetCount(); n ++ ) {

			CString Find = List[n];

			Find.TrimBoth();

			UINT c = m_DataText.GetCount();

			for( UINT b = 0; b < c; b++ ) {

				CString Test = m_DataText[b];

				Test.TrimBoth();

				if( Find == Test ) {

					UINT m = FindMask(b);

					uData |= m;

					break;
					}
				}
			}
		}

	return uData;
	}

// Implementation

UINT CUITextBitMask::FindMask(UINT b)
{
	return 1 << (m_fRev ? (m_DataText.GetCount() - 1 - b) : b);
	}

// End of File
