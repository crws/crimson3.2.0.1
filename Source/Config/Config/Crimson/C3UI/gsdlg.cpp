
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Property Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CUIGetSetDialog, CStdDialog);

// Static Data

CStringTree CUIGetSetDialog::m_Last;

// Constructor

CUIGetSetDialog::CUIGetSetDialog(CString Name, CStringArray &Props)
{
	m_Name   = Name;

	m_pProps = &Props;

	m_pView  = New CListView;

	m_pLast  = &m_Last;

	m_fBusy  = FALSE;

	SetName(L"UIGetSetDialog");
	}

// Destructor

CUIGetSetDialog::~CUIGetSetDialog(void)
{
	}

// Operations

void CUIGetSetDialog::SetLastBuffer(CStringTree &Last)
{
	m_pLast = &Last;
	}

// Message Map

AfxMessageMap(CUIGetSetDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(300,  OnAll)
	AfxDispatchCommand(301,  OnNone)

	AfxDispatchNotify (200, LVN_ITEMCHANGED, OnItemChanged)
	AfxDispatchNotify (200, NM_CUSTOMDRAW,   OnCustomDraw )

	AfxMessageEnd(CUIGetSetDialog)
	};

// Message Handlers

BOOL CUIGetSetDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	MakeList();

	LoadList();

	ApplyLast();

	ShowList();

	return FALSE;
	}

// Command Handlers

BOOL CUIGetSetDialog::OnOkay(UINT uID)
{
	m_Last.Empty();

	int nCount = m_pView->GetItemCount();

	for( int n = 0; n < nCount; n++ ) {

		UINT p = m_pView->GetItemParam(n, 0);

		if( p < NOTHING ) {

			if( !IsChecked(n) ) {

				m_pProps->SetAt(p, L"");
				}
			else {
				CStringArray Parts;

				m_pProps->GetAt(p).Tokenize(Parts, '\n');

				CString const &Child = Parts[3];

				CString const &Tag   = Parts[4];

				CString        Pair  = Child + L"\n" + Tag;

				m_Last.Insert(Pair);
				}
			}
		}

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CUIGetSetDialog::OnAll(UINT uID)
{
	int nCount = m_pView->GetItemCount();

	for( int n = 0; n < nCount; n++ ) {

		CheckItem(n, TRUE);
		}

	return TRUE;
	}

BOOL CUIGetSetDialog::OnNone(UINT uID)
{
	int nCount = m_pView->GetItemCount();

	for( int n = 0; n < nCount; n++ ) {

		CheckItem(n, FALSE);
		}

	return TRUE;
	}

// Notification Handlers

void CUIGetSetDialog::OnItemChanged(UINT uID, NMLISTVIEW &Info)
{
	if( !m_fBusy ) {

		m_fBusy = TRUE;

		if( (Info.uNewState ^ Info.uOldState) & LVIS_STATEIMAGEMASK ) {

			if( Info.lParam == NOTHING ) {

				if( Info.iItem ) {

					int nCount = m_pView->GetItemCount();

					for( int n = Info.iItem + 1; n < nCount; n++ ) {

						CListViewItem Item(LVIF_PARAM);
			
						m_pView->GetItem(n, 0, Item);

						if( Item.GetParam() < NOTHING ) {

							m_pView->SetItemState( n,
									       LVIS_STATEIMAGEMASK,
									       Info.uNewState
									       );

							continue;
							}

						break;
						}

					SetParents(FALSE);
					}
				else {
					if( (Info.uNewState & LVIS_STATEIMAGEMASK) == 0x1000 ) {

						OnNone(0);
						}
					else
						OnAll (0);
					}
				}
			else
				SetParents(TRUE);
			}

		m_fBusy = FALSE;
		}
	}

UINT CUIGetSetDialog::OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
		}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMLVCUSTOMDRAW  &List  = (NMLVCUSTOMDRAW &) Info;

		LPARAM          lParam = Info.lItemlParam;

		if( lParam == NOTHING ) {

			BYTE b = BYTE(Info.dwItemSpec ? 220 : 200);

			List.clrTextBk = RGB(b,b,b);

			SelectObject(Info.hdc, afxFont(Bolder));

			return CDRF_NEWFONT;
			}
		else {
			BYTE b = BYTE((Info.dwItemSpec % 2) ? 250 : 245);

			List.clrTextBk = RGB(b,b,b);

			return 0;
			}
		}

	return 0;
	}

// Implementation

void CUIGetSetDialog::MakeList(void)
{
	CWnd  &Wnd = GetDlgItem(100);

	CRect Rect = Wnd.GetWindowRect();
	
	ScreenToClient(Rect);

	Rect.left   += 8;
	Rect.top    += 20;
	Rect.right  -= 8;
	Rect.bottom -= 8;

	DWORD dwStyle   = LVS_REPORT        |
			  LVS_SHOWSELALWAYS |
			  LVS_SINGLESEL     |
			  LVS_NOSORTHEADER  |
			  WS_BORDER         |
			  WS_TABSTOP        ;

	DWORD dwExStyle = LVS_EX_FULLROWSELECT |
			  LVS_EX_CHECKBOXES    ;

	m_pView->Create( dwStyle,
			 Rect,
			 ThisObject,
			 200
			 );

	m_pView->SetExtendedListViewStyle( dwExStyle,
					   dwExStyle
					   );

	int cx = Rect.cx() / 32;

	int c0 = 16 * cx;
	
	int c1 = Rect.cx() - c0;

	CListViewColumn Col0(0, LVCFMT_LEFT, c0, CString(IDS_NAME));

	CListViewColumn Col1(1, LVCFMT_LEFT, c1, CString(IDS_VALUE));

	m_pView->InsertColumn(0, Col0);
	
	m_pView->InsertColumn(1, Col1);
	}

void CUIGetSetDialog::LoadList(void)
{
	m_fBusy = TRUE;

	CMap <CString, int> Map;

	UINT uCount = m_pProps->GetCount();

	CListViewItem Item( 9999,
			    0,
			    CString(' ', 1) + m_Name,
			    0,
			    NOTHING
			    );
	
	m_pView->InsertItem(Item);

	for( UINT n = 0; n < uCount; n++ ) {

		CStringArray Parts;

		m_pProps->GetAt(n).Tokenize(Parts, '\n');

		CString const &Child = Parts[3];

		CString const &Tag   = Parts[4];

		CString        Value = Parts[2];

		INDEX          Index = Map.FindName(Child);

		INT	       nPos  = 0;

                if( Value.IsEmpty() ) {

			Value = CString(IDS_DEFAULT);
			}

		if( Map.Failed(Index) ) {

			CListViewItem Item( 9999,
					    0,
					    CString(' ', 3) + Child,
					    0,
					    NOTHING
					    );

			nPos = m_pView->InsertItem(Item);

			Map.Insert(Child, ++nPos);
			}
		else {
			nPos = Map.GetData(Index);

			Map.SetData(Index, ++nPos);
			}

		CListViewItem Item( nPos,
				    0,
				    CString(' ', 5) + Tag,
				    1,
				    n
				    );

		nPos = m_pView->InsertItem(Item);

		m_pView->SetItem(CListViewItem(nPos, 1, Value, 0));
		}

	m_fBusy = FALSE;
	}

void CUIGetSetDialog::ApplyLast(void)
{
	int nCount = m_pView->GetItemCount();

	int nTotal = 0;

	int nChild = 0;

	int nPos   = 0;

	int n;

	for( n = 1; n < nCount; n++ ) {

		UINT p = m_pView->GetItemParam(n, 0);

		if( p == NOTHING ) {

			if( nPos ) {
				
				if( nChild + 1 == n - nPos ) {

					CheckItem(nPos, TRUE);

					nTotal += 1;
					}
				}

			nPos   = n;

			nChild = 0;
			}
		else {
			CStringArray Parts;

			m_pProps->GetAt(p).Tokenize(Parts, '\n');

			CString const &Child = Parts[3];

			CString const &Tag   = Parts[4];

			CString        Pair  = Child + L"\n" + Tag;

			if( !m_Last.Failed(m_Last.Find(Pair)) ) {

				CheckItem(n, TRUE);

				nTotal += 1;

				nChild += 1;
				}
			}
		}

	if( nChild + 1 == n - nPos ) {

		CheckItem(nPos, TRUE);

		nTotal += 1;
		}

	if( nTotal + 1 == nCount ) {

		CheckItem(0, TRUE);
		}
	}

void CUIGetSetDialog::SetParents(BOOL fAll)
{
	int nCount = m_pView->GetItemCount();

	int nTotal = 0;

	int nChild = 0;

	int nPos   = 0;

	int n;

	for( n = 1; n < nCount; n++ ) {

		if( fAll ) {

			UINT p = m_pView->GetItemParam(n, 0);

			if( p == NOTHING ) {

				if( nPos ) {
					
					if( nChild + 1 == n - nPos ) {

						CheckItem(nPos, TRUE);

						nTotal += 1;
						}
					else
						CheckItem(nPos, FALSE);
					}

				nPos   = n;

				nChild = 0;

				continue;
				}
			}

		if( IsChecked(n) ) {

			nTotal += 1;

			nChild += 1;
			}
		}

	if( fAll ) {

		if( nChild + 1 == n - nPos ) {

			CheckItem(nPos, TRUE);

			nTotal += 1;
			}
		else
			CheckItem(nPos, FALSE);
		}

	if( nTotal + 1 == nCount ) {

		CheckItem(0, TRUE);
		}
	else
		CheckItem(0, FALSE);
	}

void CUIGetSetDialog::ShowList(void)
{
	m_pView->ShowWindow(SW_SHOW);

	SCROLLBARINFO Info;

	Info.cbSize = sizeof(Info);

	GetScrollBarInfo(m_pView->GetHandle(), OBJID_VSCROLL, &Info);

	if( Info.rgstate[0] != STATE_SYSTEM_INVISIBLE ) {

		int cx = m_pView->GetColumnWidth(1);

		m_pView->SetColumnWidth(1, cx - 17);
		}

	m_pView->SetFocus();
	}

void CUIGetSetDialog::CheckItem(INT n, BOOL fCheck)
{
	m_pView->SetItemState( n,
			       LVIS_STATEIMAGEMASK,
			       fCheck ? 0x2000 : 0x1000
			       );
	}

BOOL CUIGetSetDialog::IsChecked(INT n)
{
	return m_pView->GetItemState(n, LVIS_STATEIMAGEMASK) == 0x2000;
	}

// End of File
