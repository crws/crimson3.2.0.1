
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "c3ui.hpp"

#include "intern.hxx"

#include <math.h>

#include <shlobj.h>

//////////////////////////////////////////////////////////////////////////
//
// Library Imports
//

#pragma	comment(lib, "shell32.lib")

// End of File

#endif
