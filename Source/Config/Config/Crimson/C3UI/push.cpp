
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Basic Push Button Group
//

// Runtime Class

AfxImplementRuntimeClass(CUIPushBase, CUIElement);

// Constructor

CUIPushBase::CUIPushBase(void)
{
	m_uFrom       = 0;
	
	m_uTo         = m_uFrom;

	m_pTextLayout = NULL;

	m_pTextCtrl   = New CStatic;
	}

// Destructor

CUIPushBase::~CUIPushBase(void)
{
	}

// Core Overridables

void CUIPushBase::OnBind(void)
{
	CUIElement::OnBind();

	m_pText->EnumValues(m_DataText);

	m_Label = GetLabel();
	}

void CUIPushBase::OnLayout(CLayFormation *pForm)
{
	m_pMainLayout = MakeLayout();

	m_pTextLayout = New CLayItem(FALSE);

	UINT   uCount = m_DataText.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CLayItemText *pDataLayout = New CLayItemText(m_DataText[n], 8, 2);

		AddData(pDataLayout);

		m_pMainLayout->AddItem(New CLayFormPad(pDataLayout, horzLeft | vertCenter));
		}

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIPushBase::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_uFrom     = uID;
	
	m_uTo       = m_uFrom;	
	
	UINT uCount = m_DataLayout.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CLayItemText *pDataLayout = m_DataLayout.GetAt(n);

		CButton *pCtrl   = New CButton;

		pCtrl->Create(	m_DataText[n],
				BS_PUSHBUTTON | WS_TABSTOP,
				pDataLayout->GetRect(),
				Wnd,
				uID++
				);

		pCtrl->SetFont(afxFont(Dialog));

		AddControl(pCtrl);

		AddCtrl(pCtrl);

		m_uTo++;
		}
	
	m_pTextCtrl->Create(	m_Label,
				WS_GROUP,
				m_pTextLayout->GetRect(),
				Wnd,
				0
				);

	m_pTextCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);
	}

void CUIPushBase::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	UINT uCount = m_DataLayout.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {
	
		CButton *pDataCtrl = m_DataCtrl.GetAt(n);

		CLayItemText *pDataLayout = m_DataLayout.GetAt(n);

		pDataCtrl->MoveWindow(pDataLayout->GetRect(), TRUE);
		}
	}

// Notification Handlers

BOOL CUIPushBase::OnNotify(UINT uID, UINT uCode)
{
	if( uID >= m_uFrom && uID < m_uTo ) {

		UINT uIndex = uID - m_uFrom;

		StdSave(FALSE, m_DataText[uIndex]);

		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CUIPushBase::AddCtrl(CButton *pCtrl)
{
	m_DataCtrl.Append(pCtrl);
	}

void CUIPushBase::AddData(CLayItemText *pData)
{
	m_DataLayout.Append(pData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Push Button Column
//

// Dynamic Class

AfxImplementDynamicClass(CUIPushCol, CUIPushBase);

// Constructor

CUIPushCol::CUIPushCol(void)
{
	}

// Layout Creation

CLayFormation * CUIPushCol::MakeLayout(void)
{
	return New CLayFormCol;
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Push Button Row
//

// Dynamic Class

AfxImplementDynamicClass(CUIPushRow, CUIPushBase);

// Constructor

CUIPushRow::CUIPushRow(void)
{
	}

// Layout Creation

CLayFormation * CUIPushRow::MakeLayout(void)
{
	return New CLayFormRow;
	}

// End of File
