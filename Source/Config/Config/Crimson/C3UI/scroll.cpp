
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Basic Scroll Bar Control
//

// Dynamic Class

AfxImplementDynamicClass(CUIScrollBase, CUIControl);

// Constructor

CUIScrollBase::CUIScrollBase(void)
{
	m_nPage = 8;
	
	m_nMin  = 0;
	
	m_nMax  = 16;

	m_pCtrlLayout = NULL;

	m_pTextLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pScrollCtrl = New CScrollBar;

	m_dwStyle     = 0;
	}

// Core Overridables

void CUIScrollBase::OnBind(void)
{
	CUIElement::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( !List[0].IsEmpty() ) {

		m_nPage = watoi(List[0]);
		}

	if( !List[1].IsEmpty() ) {

		m_nMin = watoi(List[1]);
		}

	if( !List[2].IsEmpty() ) {

		m_nMax = watoi(List[2]);
		}

	m_Label = GetLabel();
	}

void CUIScrollBase::OnLayout(CLayFormation *pForm)
{
	m_pMainLayout = New CLayFormRow;

	m_pCtrlLayout = New CLayItemText(CString('X', 16), CSize(4, 4), CSize(4, 1));

	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pMainLayout->AddItem(New CLayFormPad(m_pCtrlLayout, horzLeft | vertCenter));

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIScrollBase::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create(   m_Label,
			       0,
			       m_pTextLayout->GetRect(),
			       Wnd,
			       0
			       );

	m_pScrollCtrl->Create( L"",
			       WS_TABSTOP | m_dwStyle,
			       m_pCtrlLayout->GetRect(),
			       Wnd,
			       uID++
			       );

	m_pScrollCtrl->SetFont(afxFont(Dialog));

	m_pTextCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pScrollCtrl);
	}

void CUIScrollBase::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);
	
	m_pScrollCtrl->MoveWindow(m_pCtrlLayout->GetRect(), TRUE);
	}

// Data Overridables

void CUIScrollBase::OnLoad(void)
{
	INT nData = Parse(m_pText->GetAsText());

	m_pScrollCtrl->SetScrollRange(m_nMin, m_nMax - m_nPage, FALSE);

	m_pScrollCtrl->SetScrollPos(nData, TRUE);
	}

UINT CUIScrollBase::OnSave(BOOL fUI)
{
	INT nData = m_pScrollCtrl->GetScrollPos();

	return StdSave(fUI, Format(nData));
	}

// Implementation

INT CUIScrollBase::Parse(PCTXT pText)
{
	return watoi(pText);
	}

CString CUIScrollBase::Format(INT nData)
{
	return CPrintf(L"%d", nData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Horizontal Scroll Bar Control
//

// Dynamic Class

AfxImplementDynamicClass(CUIScrollHorz, CUIScrollBase);

// Constructor

CUIScrollHorz::CUIScrollHorz(void)
{
	m_dwStyle = SBS_HORZ;
	}

// Scroll Overridable

BOOL CUIScrollHorz::OnNotify(UINT uID, UINT uCode)
{
	if( m_pScrollCtrl->GetID() == uID ) {

		INT nData = Parse(m_pText->GetAsText());

		INT  nPos = LOWORD(uCode);

		switch( HIWORD(uCode) ) {

			case SB_LEFT:
				nData = m_nMin;
				break;

			case SB_RIGHT:
				nData = m_nMax;
				break;

			case SB_LINELEFT:
				nData -= 1;
				break;

			case SB_LINERIGHT:
				nData += 1;
				break;

			case SB_PAGELEFT:
				nData -= m_nPage;
				break;

			case SB_PAGERIGHT:
				nData += m_nPage;
				break;

			case SB_THUMBPOSITION:
				nData = nPos;
				break;

			case SB_THUMBTRACK:
				nData = nPos;
				break;

			default:
				return FALSE;
			}

		MakeMin(nData, m_nMax - m_nPage);

		MakeMax(nData, m_nMin);

		m_pScrollCtrl->SetScrollPos(nData, FALSE);

		switch( SaveUI(FALSE) ) {
			
			case saveChange:
			
				return TRUE;
			
			case saveSame:
			
				return FALSE;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Vertical Scroll Bar Control
//

// Dynamic Class

AfxImplementDynamicClass(CUIScrollVert, CUIScrollBase);

// Constructor

CUIScrollVert::CUIScrollVert(void)
{
	m_dwStyle = SBS_VERT;
	}

// Scroll Overridable

BOOL CUIScrollVert::OnNotify(UINT uID, UINT uCode)
{
	if( m_pScrollCtrl->GetID() == uID ) {

		switch( uCode ) {

			case SB_BOTTOM:
				break;

			case SB_TOP:
				break;

			case SB_LINEDOWN:
				break;

			case SB_LINEUP:
				break;

			case SB_PAGEDOWN:
				break;

			case SB_PAGEUP:
				break;

			default:
				return FALSE;
			}
		
		return TRUE;
		}

	return FALSE;
	}

// End of File
