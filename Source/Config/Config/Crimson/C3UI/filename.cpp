
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Filename
//

// Dynamic Class

AfxImplementDynamicClass(CUITextFile, CUITextElement);

// Constructor

CUITextFile::CUITextFile(void)
{
	m_uFlags = textExpand;

	m_Verb   = IDS_BROWSE;

	m_uWidth = 20;
	}

// Overridables

void CUITextFile::OnBind(void)
{
	CUITextElement::OnBind();

	UINT  uPos = GetFormat().Find('|');

	m_Filter   = GetFormat().Mid(uPos+1);	

	m_LastPath = GetFormat().Left(uPos);
	}

CString CUITextFile::OnGetAsText(void)
{
	return m_pData->ReadString(m_pItem);
	}

UINT CUITextFile::OnSetAsText(CError &Error, CString Text)
{
	m_pData->WriteString(m_pItem, Text);
	
	return saveChange;
	}

BOOL CUITextFile::OnExpand(CWnd &Wnd)
{
	COpenFileDialog Dlg;

	Dlg.SetCaption(CString(IDS_UI_FILE_PICK));

	CString Data = GetAsText();

	if( !m_LastPath.IsEmpty() ) {

		Dlg.LoadLastPath(m_LastPath);
		}

	if( !m_Filter.IsEmpty() ) {

		Dlg.SetFilter(m_Filter);
		}

	if( Data.GetLength() ) {

		Dlg.SetFilename(Data);
		}

	if( Dlg.Execute(Wnd) ) {

		SetAsText(CError(FALSE), Dlg.GetFilename());

		Dlg.SaveLastPath(m_LastPath);

		return TRUE;
		}

	return FALSE;
	}

// End of File
