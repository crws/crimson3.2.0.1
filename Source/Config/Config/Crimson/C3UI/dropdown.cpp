
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Drop Down List
//

// Dynamic Class

AfxImplementDynamicClass(CUIDropDown, CUIControl);

// Constructor

CUIDropDown::CUIDropDown(void)
{
	m_fEdit       = FALSE;

	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CDropComboBox(this);
	}

// Core Overridables

void CUIDropDown::OnBind(void)
{
	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}

	CUIElement::OnBind();
	}

void CUIDropDown::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayDropDown(m_pText, TRUE);

	m_pMainLayout = New CLayFormPad(m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem(New CLayFormPad (m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIDropDown::OnCreate(CWnd &Wnd, UINT &uID)
{
	DWORD dwStyle = m_fEdit ? CBS_DROPDOWN  : CBS_DROPDOWNLIST;

	UINT  uEvent  = m_fEdit ? CBN_KILLFOCUS : CBN_SELCHANGE;

	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( WS_TABSTOP | WS_VSCROLL | dwStyle,
			     FindComboRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, uEvent);

	LockList();

	LoadList();

	UnlockList();
	}

void CUIDropDown::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(FindComboRect(), TRUE);
	}

// Data Overridables

void CUIDropDown::OnLoad(void)
{
	if( m_pText->HasFlag(textRefresh) ) {

		LockList();

		m_pDataCtrl->ResetContent();

		LoadList();

		m_pDataCtrl->SelectStringExact(m_pText->GetAsText());

		UnlockList();

		return;
		}

	m_pDataCtrl->SelectStringExact(m_pText->GetAsText());
	}

UINT CUIDropDown::OnSave(BOOL fUI)
{
	CString Text = m_pDataCtrl->GetWindowText();

	Text.TrimLeft();

	return StdSave(fUI, Text);
	}

// Notification Overridables

BOOL CUIDropDown::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pDataCtrl->GetID() ) {

		if( uCode == CBN_DROPDOWN ) {

			if( m_pText->HasFlag(textReload) ) {

				LockList();

				m_pDataCtrl->ResetContent();

				LoadList();

				m_pDataCtrl->SelectStringExact(m_pText->GetAsText());

				UnlockList();
				}
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Implementation

CRect CUIDropDown::FindComboRect(void)
{
	CRect Rect  = m_pDataLayout->GetRect();

	Rect.right  = Rect.right - 16;

	Rect.top    = Rect.top   - 1;

	Rect.bottom = Rect.top   + 8 * Rect.cy();

	return Rect;
	}

UINT CUIDropDown::LoadList(void)
{
	CStringArray List;

	m_pText->EnumValues(List);

	UINT uCount = List.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		m_pDataCtrl->AddString(List[n]);
		}

	return uCount;
	}

BOOL CUIDropDown::Reload(void)
{
	if( m_pDataCtrl->IsWindow() ) {

		LockList();

		m_pDataCtrl->ResetContent();

		LoadList();

		LoadUI();

		UnlockList();

		return TRUE;
		}

	return FALSE;
	}

void CUIDropDown::LockList(void)
{
	m_pDataCtrl->SetRedraw(FALSE);

	m_pDataCtrl->SendMessage(CB_SETMINVISIBLE, 1);
	}

void CUIDropDown::UnlockList(void)
{
	m_pDataCtrl->SendMessage(CB_SETMINVISIBLE, 8);

	m_pDataCtrl->SetRedraw(TRUE);

	m_pDataCtrl->Invalidate(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Drop Down Edit
//

// Dynamic Class

AfxImplementDynamicClass(CUIDropEdit, CUIDropDown);

// Constructor

CUIDropEdit::CUIDropEdit(void)
{
	m_fEdit = TRUE;
	}

// Data Overridables

void CUIDropEdit::OnLoad(void)
{
	CString Text = m_pText->GetAsText();

	UINT    uPos = m_pDataCtrl->FindStringExact(Text);

	if( uPos == NOTHING ) {

		m_pDataCtrl->SetWindowText(Text);

		if( m_pDataCtrl->HasFocus() ) {

			CRange Range(TRUE);

			m_pDataCtrl->SetEditSel(Range);
			}

		return;
		}

	m_pDataCtrl->SetCurSel(uPos);

	// LATER -- Don't like this, but it fixes a problem
	// with the control library which highlights the fields
	// when it shouldn't...

	m_pDataCtrl->SetWindowText(Text + WCHAR(0x20));

	m_pDataCtrl->SetEditSel(CRange(0,0));
	}

//////////////////////////////////////////////////////////////////////////
//
// Layout Item for Drop Down
//

// Constructor

CLayDropDown::CLayDropDown(CUITextElement *pText, BOOL fGrow)
{
	m_pText = pText;

	m_fGrow = fGrow;

	m_xPad  = 0;

	m_yPad  = 6;
	}

CLayDropDown::CLayDropDown(CUITextElement *pText, BOOL fGrow, int xPad)
{
	m_pText = pText;

	m_fGrow = fGrow;

	m_xPad  = xPad;

	m_yPad  = 6;
	}

CLayDropDown::CLayDropDown(CUITextElement *pText, BOOL fGrow, int xPad, int yPad)
{
	m_pText = pText;

	m_fGrow = fGrow;

	m_xPad  = xPad;

	m_yPad  = yPad;
	}

// Overidables

void CLayDropDown::OnPrepare(CDC &DC)
{
	CStringArray List;

	m_MinSize = CSize();

	DC.Select(afxFont(Dialog));

	m_pText->EnumValues(List);

	UINT uCount = List.GetCount();

	BOOL fSpace = FALSE;

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CSize Size = DC.GetTextExtent(List[n]);

			MakeMax(m_MinSize.cx, Size.cx);

			MakeMax(m_MinSize.cy, Size.cy);

			if( !fSpace ) {

				if( List[n].Count(' ') ) {

					fSpace = TRUE;
					}
				}
			}
		}

	CSize Size = DC.GetTextExtent(L"012345678901");

	MakeMax(m_MinSize.cx, Size.cx);

	MakeMax(m_MinSize.cy, Size.cy);

	m_MinSize.cx += 48;

	m_MinSize.cx += m_xPad;

	m_MinSize.cy += m_yPad;

	m_MaxSize.cx  = Max(m_MinSize.cx, (m_fGrow && fSpace) ? 200 : 100);

	m_MaxSize.cy  = 0;

	DC.Deselect();
	}

// End of File
