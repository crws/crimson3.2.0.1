
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

BOOL TestPage(CUIItem *pItem, CString Page, CEntity Name)
{
	CUISchema *m_pSchema = New CUISchema(pItem);

	LCID Local = GetThreadLocale();

	LCID Basic = MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT);

	UINT uPass = (Local == Basic ? 1 : 2);

	for( UINT n = 0; n < uPass; n++ ) {

		HANDLE hLayout = afxModule->LoadResource(Name, RT_RCDATA);

		if( hLayout ) {

			BOOL   fDone  = FALSE;

			PCTXT pLayout = PCTXT(LockResource(hLayout));

			UINT  uLength = 0;

			if( pLayout ) {

				while( uLength = wstrlen(pLayout) ) {

					if( pLayout[1] == ':' ) {

						CStringArray Parts;

						CString(pLayout + 2).Tokenize(Parts, ',');

						if( pLayout[0] == 'G' ) {

							CString Group = Parts[2];

							C3OemStrings(Group);

							for( UINT n = 3; n < Parts.GetCount(); n++ ) {

								CUIData const *pUIData = m_pSchema->GetUIData(Parts[n]);

								AfxTrace(L"%s.%s.%s\n", Page, Group, pUIData->m_Label);
								}
							}
						}

					pLayout += uLength + 1;
					}

				UnlockResource(hLayout);

				fDone = TRUE;
				}

			FreeResource(hLayout);

			SetThreadLocale(Local);

			return fDone;
			}

		SetThreadLocale(n ? Local : Basic);
		}

	/*UIError(L"cannot load page schema %s", Name.GetName());*/

	return FALSE;
	}

void TestPage(CUIItem *pItem, CString Page, UINT uPage)
{
	CLASS   Class = AfxPointerClass(pItem);

	CString Base  = Class->GetClassName();

	CString Name  = Base + L"_Page";

	if( uPage ) {

		Name += CPrintf(L"%u", uPage);

		TestPage(pItem, Page, PCTXT(Name));

		return;
		}

	TestPage(pItem, Page, PCTXT(Name));
	}

void TestWork(CItemList *pList)
{
	CString   Pages = L"";

	CUIItem * pItem = (CUIItem *) pList->CreateItem(NULL);

	pItem->Init();

	if( pItem->LoadUIPages(Pages) ) {

		CStringArray List;

		Pages.Tokenize(List, ',');

		UINT uLast = 0;

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CString Rest  = List[n];

			CString Name  = Rest.StripToken('|');

			UINT    uPage = Rest.IsEmpty() ? (uLast+1) : watoi(Rest);

			TestPage(pItem, Name, uPage);

			uLast = uPage;
			}
		}

	pItem->Kill();

	delete pItem;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Dynamic Class

AfxImplementDynamicClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	CButtonBitmap *pCats16 = New CButtonBitmap( CBitmap(L"ToolCats16"),
						    CSize(16, 16),
						    7
						    );

	CButtonBitmap *pCats24 = New CButtonBitmap( CBitmap(L"ToolCats24"),
						    CSize(24, 24),
						    7
						    );

	afxButton->AppendBitmap(0x2000, pCats16);
	
	afxButton->AppendBitmap(0x2100, pCats24);
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestFrameWnd;
	
	CRect Rect = CRect(230, 200, 1100, 900);

	if( GetSystemMetrics(SM_CXSCREEN) == 1024 ) {

		Rect = CRect(100, 50, 970, 750);
		}

	pWnd->Create( L"Crimson 3.0 UI Prototype",
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      Rect,
		      AfxNull(CWnd),
		      CMenu(L"HeadMenu"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());
	
	afxThread->SetStatusText(L"Hello World");

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	if( Msg.message == WM_MOUSEWHEEL ) {

		CPoint Pos = CPoint(Msg.lParam);

		Msg.hwnd = WindowFromPoint(Pos);
		}

	return CDialogViewWnd::IsModelessMessage(Msg);
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:
			Src.EnableItem(TRUE);
			break;
		
		default:
			return FALSE;
		}
		
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestFrameWnd, CMainWnd);

// Constructor

CTestFrameWnd::CTestFrameWnd(void)
{
	m_Accel.Create(L"TestMenu");

	ShowEdge(FALSE);

	m_pDbase = New CDatabase(L"Test");

	m_pDbase->Init();
	}

// Destructor

CTestFrameWnd::~CTestFrameWnd(void)
{
	m_pDbase->Kill();

	delete m_pDbase;
	}

// Interface Control

void CTestFrameWnd::OnUpdateInterface(void)
{
	CMainWnd::OnUpdateInterface();
	}

// Message Map

AfxMessageMap(CTestFrameWnd, CMainWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_SHOWUI)

	AfxDispatchControlType(IDM_FILE, OnCommandControl)
	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)

	AfxMessageEnd(CTestFrameWnd)
	};

// Accelerators

BOOL CTestFrameWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CTestFrameWnd::OnPostCreate(void)
{
	m_pView = m_pDbase->CreateView(viewSystem);

	m_pView->Attach(m_pDbase);

	afxMainWnd->PostMessage(WM_UPDATEUI);

	CMainWnd::OnPostCreate();
	}

void CTestFrameWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		m_pView->SendMessage( m_MsgCtx.Msg.message,
				      m_MsgCtx.Msg.wParam,
				      m_MsgCtx.Msg.lParam
				      );
		}

	CMainWnd::OnInitPopup(Menu, nIndex, fSystem);
	}

void CTestFrameWnd::OnShowUI(BOOL fShow)
{
	}

// Command Handlers

BOOL CTestFrameWnd::OnCommandExecute(UINT uID)
{
	return FALSE;
	}

BOOL CTestFrameWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager
//

// Dynamic Class

AfxImplementDynamicClass(CTagManager, CMetaItem);

// Constructor

CTagManager::CTagManager(void)
{
	m_pTags = New CTagList;
	}

// UI Creation

CViewWnd * CTagManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		return New CNavTreeWnd( L"Tags",
					AfxRuntimeClass(CFolderItem),
					AfxRuntimeClass(CTagItem)
					);
		}						    

	return CMetaItem::CreateView(uType);
	}

// Item Naming

CString CTagManager::GetHumanName(void) const
{
	return L"Tags";
	}

// Meta Data

void CTagManager::AddMetaData(void)
{
	Meta_AddCollect(Tags);

	CMetaItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag List
//

// Dynamic Class

AfxImplementDynamicClass(CTagList, CItemIndexList);

// Constructor

CTagList::CTagList(void)
{
	m_Class = AfxRuntimeClass(CTagItem);
	}

// Persistance

void CTagList::Init(void)
{
	for( UINT n = 0; n < 6; n++ ) {

		CMetaItem *pItem = New CTagItem;

		pItem->Init();

		pItem->SetName(CPrintf(L"Tag%u", n));

		AppendItem(pItem);
		}

	CItemIndexList::Init();
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag Item
//

// Dynamic Class

AfxImplementDynamicClass(CTagItem, CUIItem);

// Constructor

CTagItem::CTagItem(void)
{
	m_Data1 = 1;
	m_Data2 = 2;
	m_Data3 = 3;
	m_Data4 = 4;
	}

// Meta Data

void CTagItem::AddMetaData(void)
{
	Meta_AddString(Name);
	Meta_AddInteger(Data1);
	Meta_AddInteger(Data2);
	Meta_AddInteger(Data3);
	Meta_AddInteger(Data4);

	CMetaItem::AddMetaData();

	Meta_SetName(L"Tag");
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Item
//

// Dynamic Class

AfxImplementDynamicClass(CTestItem, CMetaItem);

// Constructor

CTestItem::CTestItem(void)
{
	m_pTags = New CTagManager;
	}

// UI Creation

CViewWnd * CTestItem::CreateView(UINT uType)
{
	if( uType == viewSystem ) {

		return New CSystemWnd;
		}

	return NULL;
	}

// Item Naming

CString CTestItem::GetHumanName(void) const
{
	return CString();
	}

CString CTestItem::GetFixedName(void) const
{
	return CString();
	}

// Persistance

void CTestItem::Init(void)
{
	CMetaItem::Init();

	AddData();

	TestWork(m_pTags->m_pTags);
	}

void CTestItem::PostLoad(void)
{
	CMetaItem::PostLoad();

	AddData();
	}

// System Data

void CTestItem::AddData(void)
{
	CDatabase *pDbase = GetDatabase();

	pDbase->AddCategory(L"Data Tags", L"Tags", 0x21000001, 0x20000001);
	}

// Meta Data

void CTestItem::AddMetaData(void)
{
	Meta_AddObject(Tags);

	CMetaItem::AddMetaData();
	}

// End of File
