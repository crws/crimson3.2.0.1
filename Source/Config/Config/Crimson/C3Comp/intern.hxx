
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//								
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_CANNOT_TAKE_1       0x4000
#define IDS_CANNOT_TAKE_2       0x4001
#define IDS_CANNOT_USE_NULL     0x4002
#define IDS_CODE_BAD_BIT        0x4003
#define IDS_CODE_BAD_ENODE      0x4004
#define IDS_CODE_BAD_SNODE      0x4005
#define IDS_CODE_DIV_ZERO       0x4006
#define IDS_COMPLEX             0x4007
#define IDS_COMP_EXPECT_1       0x4008
#define IDS_COMP_EXPECT_2       0x4009
#define IDS_COMP_TOO_COMPLEX    0x400A
#define IDS_CONST_INTEGER       0x400B
#define IDS_CONST_REAL          0x400C
#define IDS_CONST_STRING        0x400D
#define IDS_EXPR_ARGS_LESS      0x400E
#define IDS_EXPR_ARGS_MORE      0x400F
#define IDS_EXPR_ARG_ARRAY      0x4010
#define IDS_EXPR_ARG_MISMATCH   0x4011
#define IDS_EXPR_ARG_NUMERIC    0x4012
#define IDS_EXPR_ARG_OBJECT     0x4013
#define IDS_EXPR_ARG_RVALUE     0x4014
#define IDS_EXPR_ARG_STRING     0x4015
#define IDS_EXPR_ARG_VOID       0x4016
#define IDS_EXPR_BAD_CAST       0x4017
#define IDS_EXPR_BAD_INDEX      0x4018
#define IDS_EXPR_DEF_TYPE       0x4019
#define IDS_EXPR_EMPTY_SUBEX    0x401A
#define IDS_EXPR_FUNC_ARG       0x401B
#define IDS_EXPR_MISS_OPERAND   0x401C
#define IDS_EXPR_MISS_PLCREF    0x401D
#define IDS_EXPR_MISS_SUBEX     0x401E
#define IDS_EXPR_NEED_CONSTANT  0x401F
#define IDS_EXPR_NEED_ELEMENT   0x4020
#define IDS_EXPR_NEED_INDEX     0x4021
#define IDS_EXPR_NEED_INTEGER   0x4022
#define IDS_EXPR_NEED_LVALUE    0x4023
#define IDS_EXPR_NEED_NUMERIC   0x4024
#define IDS_EXPR_NEED_OBJECT    0x4025
#define IDS_EXPR_NEED_SCALAR    0x4026
#define IDS_EXPR_NEED_STRING    0x4027
#define IDS_EXPR_NOT_BITREF     0x4028
#define IDS_EXPR_NOT_REAL       0x4029
#define IDS_EXPR_NOT_STRING     0x402A
#define IDS_EXPR_OPERAND        0x402B
#define IDS_EXPR_OPERAND_1      0x402C
#define IDS_EXPR_OPERAND_2      0x402D
#define IDS_EXPR_OPERAND_3      0x402E
#define IDS_EXPR_OPERAND_LHS    0x402F
#define IDS_EXPR_OPERAND_RHS    0x4030
#define IDS_EXPR_PAR_REDEFINE   0x4031
#define IDS_EXPR_RUN_ARG        0x4032
#define IDS_EXPR_RUN_RETURN     0x4033
#define IDS_EXPR_SIDE_EFFECT    0x4034
#define IDS_EXPR_THIS_EXPR      0x4035
#define IDS_EXPR_UNKNOWN_FUNC   0x4036
#define IDS_EXPR_UNKNOWN_REF    0x4037
#define IDS_EXPR_UNKNOWN_VAR    0x4038
#define IDS_EXPR_ZERO_EFFECT    0x4039
#define IDS_FOLDER_NAME         0x403A
#define IDS_FUNC_DUP_USING      0x403B
#define IDS_FUNC_INITIAL        0x403C
#define IDS_FUNC_IS_VOID        0x403D
#define IDS_FUNC_LATE_CASE      0x403E
#define IDS_FUNC_LOC_REDEFINE   0x403F
#define IDS_FUNC_LOC_STRING     0x4040
#define IDS_FUNC_LOC_TOO_MANY   0x4041
#define IDS_FUNC_MISS_CASE      0x4042
#define IDS_FUNC_MISS_CTRL      0x4043
#define IDS_FUNC_MISS_EXPR      0x4044
#define IDS_FUNC_MISS_INIT      0x4045
#define IDS_FUNC_REPEAT_CASE    0x4046
#define IDS_FUNC_REPEAT_DEF     0x4047
#define IDS_FUNC_RETURN         0x4048
#define IDS_FUNC_ZERO_EFFECT    0x4049
#define IDS_INTERNAL_ERROR_IN   0x404A
#define IDS_INVALID_REFERENCE   0x404B
#define IDS_LEX_BAD_DIGIT       0x404C
#define IDS_LEX_BAD_DP          0x404D
#define IDS_LEX_BAD_ESCAPE      0x404E
#define IDS_LEX_BAD_SUFFIX      0x404F
#define IDS_LEX_CR_IN_CHAR      0x4050
#define IDS_LEX_CR_IN_ESC       0x4051
#define IDS_LEX_CR_IN_STRING    0x4052
#define IDS_LEX_EMPTY_CHAR      0x4053
#define IDS_LEX_ESC_DIGIT       0x4054
#define IDS_LEX_ESC_OVERFLOW    0x4055
#define IDS_LEX_HANGING_DP      0x4056
#define IDS_LEX_INT_RANGE       0x4057
#define IDS_LEX_LONG_CHAR       0x4058
#define IDS_LEX_LONG_STRING     0x4059
#define IDS_LEX_OPEN_COMMENT    0x405A
#define IDS_LEX_UNEXPECTED      0x405B
#define IDS_LEX_UNICODE         0x405C /* NOT USED */
#define IDS_LEX_WAS             0x405D
#define IDS_MISSING_EXPONENT    0x405E
#define IDS_MUST_BE_1           0x405F
#define IDS_MUST_BE_2           0x4060
#define IDS_NODE_DIRECT         0x4061
#define IDS_NODE_FULL           0x4062
#define IDS_NODE_FUNCTION       0x4063
#define IDS_NODE_LOCAL          0x4064
#define IDS_NODE_PROP           0x4065
#define IDS_NODE_VARIABLE       0x4066
#define IDS_PROGRAM_NAME        0x4067
#define IDS_TOK_BRACE_CLOSE     0x4068
#define IDS_TOK_BRACE_OPEN      0x4069
#define IDS_TOK_BRACKET_CLOSE   0x406A
#define IDS_TOK_BRACKET_OPEN    0x406B
#define IDS_TOK_COLON           0x406C
#define IDS_TOK_END_OF_TEXT     0x406D
#define IDS_TOK_IDENTIFIER      0x406E
#define IDS_TOK_INDEX_CLOSE     0x406F
#define IDS_TOK_INDEX_OPEN      0x4070
#define IDS_TOK_KEYWORD         0x4071
#define IDS_TOK_OPERATOR        0x4072
#define IDS_TOK_SEMICOLON       0x4073
#define IDS_TOK_SEPARATOR       0x4074
#define IDS_TOK_WHITESPACE      0x4075

// End of File

#endif
