
#include "intern.hpp"

#include "bcode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Peephole Code Optimizer
//

// Constructor

CCodeOptimizer::CCodeOptimizer(CByteArray &Code, UINT uInit) : m_Code(Code)
{
	m_uInit = uInit;
	}

// Operations

BOOL CCodeOptimizer::Optimize(CError &Error)
{
	m_pError = &Error;

	try {
		static BOOL (CCodeOptimizer::*List[])(BYTE,UINT) = {
	
			&CCodeOptimizer::CheckJumpOverJump,
			&CCodeOptimizer::CheckInvertAndJump,
			&CCodeOptimizer::CheckTestAndJump,
			&CCodeOptimizer::CheckDoubleInvert1,
			&CCodeOptimizer::CheckDoubleInvert2,
			&CCodeOptimizer::CheckNegativeAdd,
			&CCodeOptimizer::CheckInvertCompare,
			&CCodeOptimizer::CheckDoubleLoad,
			&CCodeOptimizer::CheckSaveAndLoad,
			&CCodeOptimizer::CheckDeadPutLocal,
			&CCodeOptimizer::CheckDeadValue,
			&CCodeOptimizer::CheckDeadLoad,
			&CCodeOptimizer::CheckDeadCodeAfterReturn,
			&CCodeOptimizer::CheckDeadCodeAfterBranch,
			&CCodeOptimizer::CheckNullJump,
			&CCodeOptimizer::CheckDupPop,
			
			};

		for(;;) {

			UINT uSize = m_Code.GetCount();

			BOOL fAny   = FALSE;

			for( UINT uPos = m_uInit; uPos < uSize; ) {

				BYTE bCode = m_Code[uPos];

				BOOL fHit  = FALSE;

				for( UINT n = 0; n < elements(List); n++ ) {

					if( (this->*List[n])(bCode, uPos) ) {

						bCode = m_Code[uPos];

						fHit  = TRUE;

						fAny  = TRUE;
						}
					}

				if( fHit ) {

					uSize = m_Code.GetCount();

					continue;
					}

				uPos = GetNextOpcode(uPos);
				}

			if( !fAny ) {

				return TRUE;
				}
			}
		}

	catch(CUserException const &) {
	
		return FALSE;
		}
	}

// Optimizations

BOOL CCodeOptimizer::CheckJumpOverJump(BYTE bCode, UINT uPos)
{
	if( IsCondition(bCode) ) {

		if( m_Code[uPos+3] == bcBranch ) {

			if( m_Code[uPos+6] == bcLabel ) {

				if( m_Code[uPos+7] == m_Code[uPos+1] ) {
					
					if( m_Code[uPos+8] == m_Code[uPos+2] ) {

						m_Code.Remove(uPos + 1, 3);

						if( bCode == bcBranchZero ) {

							m_Code.SetAt(uPos, bcBranchNonZero);
							}
						else
							m_Code.SetAt(uPos, bcBranchZero);

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckInvertAndJump(BYTE bCode, UINT uPos)
{
	if( bCode == bcLogicalNot ) {

		if( m_Code[uPos+1] == bcBranchZero ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcBranchNonZero);

			return TRUE;
			}

		if( m_Code[uPos+1] == bcBranchNonZero ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcBranchZero);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckTestAndJump(BYTE bCode, UINT uPos)
{
	if( bCode == bcTestInteger ) {

		if( m_Code[uPos+1] == bcBranchZero ) {

			m_Code.Remove(uPos, 1);

			return TRUE;
			}

		if( m_Code[uPos+1] == bcBranchNonZero ) {

			m_Code.Remove(uPos, 1);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDoubleInvert1(BYTE bCode, UINT uPos)
{
	if( bCode == bcLogicalNot ) {

		if( m_Code[uPos+1] == bcLogicalNot ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcTestInteger);

			return TRUE;
			}

		if( m_Code[uPos+1] == bcTestInteger ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcLogicalNot);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDoubleInvert2(BYTE bCode, UINT uPos)
{
	if( bCode == bcTestInteger ) {

		if( m_Code[uPos+1] == bcLogicalNot ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcLogicalNot);

			return TRUE;
			}
		}

	if( bCode == bcTestReal ) {

		if( m_Code[uPos+1] == bcLogicalNot ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcRealLogicalNot);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckNegativeAdd(BYTE bCode, UINT uPos)
{
	if( bCode == bcUnaryMinus ) {

		if( m_Code[uPos+1] == bcAdd ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcSubtract);

			return TRUE;
			}

		if( m_Code[uPos+1] == bcSubtract ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcAdd);

			return TRUE;
			}
		}

	if( bCode == bcRealUnaryMinus ) {

		if( m_Code[uPos+1] == bcRealAdd ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcRealSubtract);

			return TRUE;
			}

		if( m_Code[uPos+1] == bcRealSubtract ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcRealAdd);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckInvertCompare(BYTE bCode, UINT uPos)
{
	static BYTE const bPair[][2] = {

		{ bcEqual,             bcNotEqual       },
		{ bcGreaterThan,       bcLessOr         },
		{ bcGreaterOr,         bcLessThan       },
		{ bcRealEqual,         bcRealNotEqual   },
		{ bcRealGreaterThan,   bcRealLessOr     },
		{ bcRealGreaterOr,     bcRealLessThan   },
		{ bcStringEqual,       bcStringNotEqual },
		{ bcStringGreaterThan, bcStringLessOr   },
		{ bcStringGreaterOr,   bcStringLessThan }

		};

	for( UINT n = 0; n < 9; n++ ) {

		if( bCode == bPair[n][0] ) {

			if( m_Code[uPos+1] == bcLogicalNot ) {

				m_Code.Remove(uPos, 1);

				m_Code.SetAt (uPos, bPair[n][1]);

				return TRUE;
				}
			}

		if( bCode == bPair[n][1] ) {

			if( m_Code[uPos+1] == bcLogicalNot ) {

				m_Code.Remove(uPos, 1);

				m_Code.SetAt (uPos, bPair[n][0]);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDoubleLoad(BYTE bCode, UINT uPos)
{
	if( bCode >= bcGetInteger && bCode <= bcGetString ) {

		if( m_Code[uPos+3] == bCode ) {

			if( m_Code[uPos+1] == m_Code[uPos+4] ) {

				if( m_Code[uPos+2] == m_Code[uPos+5] ) {

					m_Code.Remove(uPos + 3, 2);

					if( bCode == bcGetString ) {

						m_Code.SetAt(uPos + 3, bcDupTopString);
						}
					else
						m_Code.SetAt(uPos + 3, bcDupTop);

					return TRUE;
					}
				}
			}
		}

	if( bCode == bcGetLocal || bCode == bcGetLocalString ) {

		if( m_Code[uPos+2] == bCode ) {

			if( m_Code[uPos+3] == m_Code[uPos+1] ) {

				m_Code.Remove(uPos + 3, 1);

				if( bCode == bcGetLocalString ) {

					m_Code.SetAt(uPos + 2, bcDupTopString);
					}
				else
					m_Code.SetAt(uPos + 2, bcDupTop);

				return TRUE;
				}
			}
		}

	if( bCode >= bcGetLocal0 && bCode <= bcGetLocal5 ) {

		if( m_Code[uPos+1] == bCode ) {

			m_Code.SetAt(uPos + 1, bcDupTop);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckSaveAndLoad(BYTE bCode, UINT uPos)
{
	if( bCode >= bcPutInteger && bCode <= bcPutString ) {

		BYTE bLoad = bCode;
		
		bLoad -= bcPutInteger;

		bLoad += bcGetInteger;

		if( m_Code[uPos+3] == bLoad ) {

			if( m_Code[uPos+1] == m_Code[uPos+4] ) {

				if( m_Code[uPos+2] == m_Code[uPos+5] ) {

					m_Code.Remove(uPos + 1, 2);

					if( bCode == bcPutString ) {

						m_Code.SetAt(uPos + 0, bcDupTopString);

						m_Code.SetAt(uPos + 1, bCode);
						}
					else {
						m_Code.SetAt(uPos + 0, bcDupTop);

						m_Code.SetAt(uPos + 1, bCode);
						}

					return TRUE;
					}
				}
			}
		}

	if( bCode == bcPutLocal || bCode == bcPutLocalString ) {

		BYTE bLoad = bCode;
		
		bLoad -= bcPutLocal;

		bLoad += bcGetLocal;

		if( m_Code[uPos+2] == bLoad ) {

			if( m_Code[uPos+3] == m_Code[uPos+1] ) {

				m_Code.Remove(uPos + 1, 1);

				if( bCode == bcPutLocalString ) {

					m_Code.SetAt(uPos + 0, bcDupTopString);

					m_Code.SetAt(uPos + 1, bCode);
					}
				else {
					m_Code.SetAt(uPos + 0, bcDupTop);

					m_Code.SetAt(uPos + 1, bCode);
					}

				return TRUE;
				}
			}
		}

	if( bCode >= bcPutLocal0 && bCode <= bcPutLocal5 ) {

		BYTE bLoad = bCode;

		bLoad -= bcPutLocal0;
		
		bLoad += bcGetLocal0;

		if( m_Code[uPos+1] == bLoad ) {

			m_Code.SetAt(uPos + 0, bcDupTop);

			m_Code.SetAt(uPos + 1, bCode);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDeadPutLocal(BYTE bCode, UINT uPos)
{
	if( bCode >= bcPutLocal0 && bCode <= bcPutLocal5 ) {

		UINT uSlot = bCode - bcPutLocal0;

		BYTE bFind = BYTE(bcGetLocal0 + uSlot);

		UINT uScan = uPos + 1;

		UINT uSize = m_Code.GetCount();

		while( uScan < uSize ) {

			BYTE bScan = m_Code[uScan];

			if( bScan == bFind ) {

				return FALSE;
				}

			if( IsBranch(bScan) ) {

				return FALSE;
				}

			uScan = GetNextOpcode(uScan);
			}

		m_Code.SetAt(uPos, bcPop);

		return TRUE;
		}

	if( bCode == bcPutLocal || bCode == bcPutLocalString ) {

		UINT uSlot = m_Code[uPos+1];

		BYTE bFind = BYTE(bCode - bcPutLocal + bcGetLocal);

		UINT uScan = uPos + 2;

		UINT uSize = m_Code.GetCount();

		while( uScan < uSize ) {

			BYTE bScan = m_Code[uScan];

			if( bScan == bFind ) {

				if( m_Code[uScan+1] == uSlot ) {

					return FALSE;
					}
				}

			if( IsBranch(bScan) ) {

				return FALSE;
				}

			uScan = GetNextOpcode(uScan);
			}

		if( bCode == bcPutLocalString ) {

			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcPopString);
			}
		else {
			m_Code.Remove(uPos, 1);

			m_Code.SetAt (uPos, bcPop);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDeadValue(BYTE bCode, UINT uPos)
{
	if( IsNumericValue(bCode) ) {

		UINT uNext = GetNextOpcode(uPos);

		UINT uSize = uNext - uPos;

		if( m_Code[uNext] == bcPop ) {

			m_Code.Remove(uPos, uSize + 1);

			return TRUE;
			}
		}

	if( IsStringValue(bCode) ) {

		UINT uNext = GetNextOpcode(uPos);

		UINT uSize = uNext - uPos;

		if( m_Code[uNext] == bcPopString ) {

			m_Code.Remove(uPos, uSize + 1);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDeadLoad(BYTE bCode, UINT uPos)
{
	if( bCode == bcLoadInteger || bCode == bcLoadReal ) {

		UINT uNext = GetNextOpcode(uPos);

		UINT uSize = uNext - uPos;

		if( m_Code[uNext] == bcPop ) {

			m_Code.SetAt (uNext, bcPop);

			m_Code.Remove(uPos,  uSize);

			return TRUE;
			}
		}

	if( bCode == bcLoadString ) {

		UINT uNext = GetNextOpcode(uPos);

		UINT uSize = uNext - uPos;

		if( m_Code[uNext] == bcPopString ) {

			m_Code.SetAt (uNext, bcPop);

			m_Code.Remove(uPos,  uSize);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDeadCodeAfterReturn(BYTE bCode, UINT uPos)
{
	if( bCode == bcReturn ) {

		UINT uScan = uPos + 1;

		UINT uInit = uScan;

		UINT uSize = m_Code.GetCount();

		while( uScan < uSize ) {

			if( m_Code[uScan] == bcLabel ) {

				break;
				}

			uScan = GetNextOpcode(uScan);
			}

		if( uScan > uInit ) {

			m_Code.Remove(uInit, uScan - uInit);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDeadCodeAfterBranch(BYTE bCode, UINT uPos)
{
	if( bCode == bcBranch ) {

		UINT uScan = uPos + 3;

		UINT uInit = uScan;

		UINT uSize = m_Code.GetCount();

		while( uScan < uSize ) {

			if( m_Code[uScan] == bcLabel ) {

				break;
				}

			uScan = GetNextOpcode(uScan);
			}

		if( uScan > uInit ) {

			m_Code.Remove(uInit, uScan - uInit);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckNullJump(BYTE bCode, UINT uPos)
{
	if( IsBranch(bCode) ) {

		UINT uScan = uPos + 3;

		UINT uSize = m_Code.GetCount();

		while( uScan < uSize ) {

			if( m_Code[uScan] == bcLabel ) {

				if( m_Code[uPos+1] == m_Code[uScan+1] ) {

					if( m_Code[uPos+2] == m_Code[uScan+2] ) {

						m_Code.Remove(uPos, 3);

						return TRUE;
						}
					}

				uScan += 3;

				continue;
				}

			break;
			}
		}

	return FALSE;
	}

BOOL CCodeOptimizer::CheckDupPop(BYTE bCode, UINT uPos)
{
	if( bCode == bcDupTop ) {

		if( m_Code[uPos + 1] == bcPop ) {

			m_Code.Remove(uPos, 2);

			return TRUE;
			}
		}

	if( bCode == bcDupTopString ) {

		if( m_Code[uPos + 1] == bcPopString ) {

			m_Code.Remove(uPos, 2);

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

BOOL CCodeOptimizer::IsNumericValue(BYTE bCode)
{
	switch( bCode ) {

		case bcPushInteger1:
		case bcPushInteger2:
		case bcPushInteger4:
		case bcPushReal:
		case bcGetInteger:
		case bcGetReal:
		case bcGetLocal:
		case bcGetLocal0:
		case bcGetLocal1:
		case bcGetLocal2:
		case bcGetLocal3:
		case bcGetLocal4:
		case bcGetLocal5:

			return TRUE;
		}

	return bCode < 0x80;
	}

BOOL CCodeOptimizer::IsStringValue(BYTE bCode)
{
	switch( bCode ) {

		case bcPushString:
		case bcPushWideString:
		case bcGetString:
		case bcGetLocalString:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCodeOptimizer::IsBranch(BYTE bCode)
{
	switch( bCode ) {

		case bcBranchZero:
		case bcBranchNonZero:
		case bcBranch:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCodeOptimizer::IsCondition(BYTE bCode)
{
	switch( bCode ) {

		case bcBranchZero:
		case bcBranchNonZero:

			return TRUE;
		}

	return FALSE;
	}

BYTE CCodeOptimizer::GetOpcodeSize(BYTE bCode)
{
	switch( bCode ) {

		case bcPutLocal:
		case bcPutLocalString:
		case bcGetLocal:
		case bcGetLocalString:
		case bcPushInteger1:

			return 1;
	
		case bcBranchZero:
		case bcBranchNonZero:
		case bcBranch:
		case bcLabel:

			return 2;

		case bcLoadAddr:
		case bcPutInteger:
		case bcPutReal:
		case bcPutString:
		case bcGetInteger:
		case bcGetReal:
		case bcGetString:
		case bcPushInteger2:

			return 2;

		case bcFunction:

			return 3;

		case bcPushInteger4:
		case bcPushReal:

			return 4;
		}

	return 0;
	}

UINT CCodeOptimizer::GetNextOpcode(UINT uPos)
{
	BYTE bCode = m_Code[uPos];

	if( bCode == bcPushString ) {

		UINT uLength = MAKEWORD(m_Code[uPos+2], m_Code[uPos+1]);

		uPos += 3;

		uPos += 1 * uLength;

		return uPos;
		}

	if( bCode == bcPushWideString ) {

		UINT uLength = MAKEWORD(m_Code[uPos+2], m_Code[uPos+1]);

		uPos += 3;

		uPos += 2 * uLength;

		return uPos;
		}

	if( bCode == bcDebugFunc ) {

		UINT uArgs = m_Code[uPos+1];

		uPos += 1 + uArgs;

		uPos += 2;

		return uPos;
		}

	uPos += 1;

	uPos += GetOpcodeSize(bCode);

	return uPos;
	}

// End of File
