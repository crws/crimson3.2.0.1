
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Constant Accelerator
//

static void PutConst(CByteArray *pObject, UINT n)
{
	if( HIWORD(n) ) {

		pObject->Append(0xB4);

		pObject->Append(HIBYTE(HIWORD(n)));
		pObject->Append(LOBYTE(HIWORD(n)));
		pObject->Append(HIBYTE(LOWORD(n)));
		pObject->Append(LOBYTE(LOWORD(n)));
		}
	else {
		pObject->Append(0xB3);

		pObject->Append(HIBYTE(n));
		pObject->Append(LOBYTE(n));
		}

	pObject->Append(0x8D);
	}

static BOOL HexConst(PCTXT pText, CByteArray * pSource, CByteArray *pObject)
{
	if( pText[0] == '0' && pText[1] == 'x' ) {

		if( wstrlen(pText) <= 10 ) {

			for( UINT n = 2; pText[n]; n++ ) {

				if( !isxdigit(pText[n]) ) {

					return FALSE;
					}
				}

			if( pSource ) {

				for( int n = 0; pText[n]; n++ ) {

					pSource->Append(BYTE(pText[n]));
					}

				pSource->Append(0);
				}

			if( pObject ) {

				pObject->Append(BYTE(typeInteger));

				pObject->Append(1);

				pObject->Append(0);

				pObject->Append(0);

				DWORD n = wcstoul(pText + 2, NULL, 16);

				PutConst(pObject, n);
				}
			
			return TRUE;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Expression Compiler
//

DLLAPI BOOL C3CompileExpr(CCompileIn const &In, CCompileOut &Out)
{
	if( In.m_Type.m_Type == typeInteger ) {
		
		if( In.m_Type.m_Flags == flagNone ) {

			if( HexConst(In.m_pText, Out.m_pSource, Out.m_pObject) ) {

				Out.m_Type.m_Type  = typeInteger;

				Out.m_Type.m_Flags = flagConstant;

				return TRUE;
				}
			}
		}

	return C3CompileCode(In, Out, FALSE);
	}
			  
//////////////////////////////////////////////////////////////////////////
//
// Function Compiler
//

DLLAPI BOOL C3CompileFunc(CCompileIn const &In, CCompileOut &Out)
{
	return C3CompileCode(In, Out, TRUE);
	}
			  
//////////////////////////////////////////////////////////////////////////
//
// Common Compiler
//

DLLAPI BOOL C3CompileCode(CCompileIn const &In, CCompileOut &Out, BOOL fFunc)
{
	CFuncParser *pParse = New CFuncParser;

	if( !pParse->ParseCode(In, Out, fFunc) ) {

		delete pParse;

		return FALSE;
		}

	if( Out.m_pObject ) {

		CParseTree &Tree = pParse->GetTree();

		if( Tree.GetCount() ) {

			if( In.m_Optim >= 1 ) {

				CTreeOptimizer *pOptim = New CTreeOptimizer(Tree);

				if( !pOptim->Optimize(Out.m_Error) ) {

					delete pOptim;

					delete pParse;

					return FALSE;
					}

				delete pOptim;
				}

			CCodeGenerator *pCoder = New CCodeGenerator(Tree);

			UINT uDepth = pParse->GetDepth() + 1;

			UINT uLocal = pParse->GetLocal();

			pCoder->Prepare(uDepth, uLocal, In);

			if( !(In.m_Type.m_Flags & flagSoftWrite) ) {

				if( !(In.m_Type.m_Flags & flagWritable) ) {

					Out.m_Type.m_Flags &= ~flagWritable;
					}
				}

			if( !(In.m_Type.m_Flags & flagInherent) ) {

				if( !(In.m_Type.m_Flags & flagWritable) ) {

					Out.m_Type.m_Type = In.m_Type.m_Type;
					}
				}

			if( Out.m_Type.m_Type == typeLogical ) {

				Out.m_Type.m_Type   = typeInteger;

				Out.m_Type.m_Flags |= flagLogical;
				}

			if( !pCoder->GenerateCode(Out, fFunc) ) {

				delete pCoder;

				delete pParse;

				return FALSE;
				}

			Out.m_pObject->Empty();

			Out.m_pObject->Append(pCoder->GetCode());

			delete pCoder;
			}
		}

	if( Out.m_pSource ) {

		Out.m_pSource->Empty();

		Out.m_pSource->Append(pParse->GetSource());
		}

	if( Out.m_pRefList ) {

		Out.m_pRefList->Empty();

		Out.m_pRefList->Append(pParse->GetRefList());
		}

	delete pParse;

	return TRUE;
	}

// End of File
