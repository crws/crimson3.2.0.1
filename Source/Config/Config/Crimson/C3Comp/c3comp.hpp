
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3COMP_HPP
	
#define	INCLUDE_C3COMP_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdialog.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3comp.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_C3COMP

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "c3comp.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CArray <BYTE>	CByteArray;

typedef CArray <DWORD>  CLongArray;

typedef CArray <UINT>	CUIntArray;

typedef signed int	C3INT;

typedef float		C3REAL;

//////////////////////////////////////////////////////////////////////////
//
// Type Conversion
//

inline C3REAL I2R(DWORD d)
{
	// cppcheck-suppress invalidPointerCast

	return *((C3REAL *) &d);
	}

inline DWORD R2I(C3REAL r)
{
	// cppcheck-suppress invalidPointerCast

	return *((DWORD *) &r);
	}

//////////////////////////////////////////////////////////////////////////
//
// Data Types
//

enum DataType
{
	typeVoid	= 0x000,
	typeInteger	= 0x001,
	typeReal	= 0x002,
	typeString	= 0x003,
	typeLogical	= 0x004,
	typeNumeric	= 0x005,
	typeLValue	= 0x010,
	typeObject	= 0x040,
	typeFolder	= 0x040,
	typeIndex	= 0x060,

	};

//////////////////////////////////////////////////////////////////////////
//
// Type Flags
//

enum TypeFlag
{
	flagNone      = 0x0000,
	flagConstant  = 0x0001,
	flagWritable  = 0x0002,
	flagArray     = 0x0004,
	flagExtended  = 0x0008,
	flagElement   = 0x0010,
	flagBitRef    = 0x0020,
	flagLocal     = 0x0040,
	flagActive    = 0x0080,
	flagSoftWrite = 0x0100,
	flagInherent  = 0x0200,
	flagCommsRef  = 0x0400,
	flagCommsTab  = 0x0800,
	flagTagRef    = 0x1000,
	flagLogical   = 0x2000,
	flagDispatch  = 0x4000,
	flagTagProp   = 0x8000,

	};

//////////////////////////////////////////////////////////////////////////
//
// GetData Flags
//

enum
{
	getNone		= 0x0000,
	getMaskSource	= 0x000F,
	getScaled	= 0x0000,
	getManipulated	= 0x0001,
	getCommsData	= 0x0002,
	};

//////////////////////////////////////////////////////////////////////////
//
// SetData Flags
//

enum
{
	setNone		= 0x0000,
	setMaskSource	= 0x000F,
	setScaled	= 0x0000,
	setManipulated	= 0x0001,
	setCommsData	= 0x0002,
	setForce	= 0x0010,
	setDirect	= 0x0020,
	};

//////////////////////////////////////////////////////////////////////////
//
// Compiler Options
//

enum
{
	optAutoCast	= 0x0001,
	optPrecFix	= 0x0002,
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Definition
//

struct CTypeDef
{
	UINT	m_Type;
	UINT	m_Flags;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Parameter
//

struct CFuncParam
{
	CString	m_Name;
	UINT	m_Type;
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Reference
//

struct CDataRef
{
	union {
		struct {
			INT	m_Offset : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Block  : 10;
			UINT    m_BitRef : 5;
			UINT	m_HasBit : 1;
			} b;

		struct {
			UINT	m_Index  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Array  : 10;
			UINT	m_BitRef : 5;
			UINT    m_HasBit : 1;
			} t;

		struct {
			UINT	m_Index  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Array  : 15;
			UINT    m_HasBit : 1;
			} x;

		DWORD m_Ref;
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// Color Types
//

enum ColorSpanType
{
	spanDefault,
	spanWhiteSpace,
	spanComment,
	spanKeyword,
	spanNumber,
	spanString,
	spanIdentifier,
	spanOperator,
	spanSeparator,
	spanWas,

	};

//////////////////////////////////////////////////////////////////////////
//
// Syntax Color Span
//

struct CColorSpan
{
	UINT	m_Type;
	UINT	m_uCount;
	};

//////////////////////////////////////////////////////////////////////////
//
// Syntax Token Span
//

struct CTokenSpan
{
	UINT	m_Type;
	UINT	m_uCount;
	UINT	m_Code;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

interface IIdentServer;
interface IDirectServer;
interface IFuncServer;
interface IClassServer;
interface IFuncLibrary;
interface INameServer;
interface IDataServer;

//////////////////////////////////////////////////////////////////////////
//
// Base Interface
//

#if !defined(IBASE_DEFINED)

#define IBASE_DEFINED

interface IBase
{
	virtual UINT Release(void) = 0;
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Identifier Resolution Interface
//

interface IIdentServer : public IBase
{
	virtual BOOL	FindIdent(	CError        *	pError,
					CString		Name,
					WORD	      & ID,
					CTypeDef      & Type
					) = 0;

	virtual BOOL	NameIdent(	WORD		ID,
					CString	       & Name
					) = 0;

	virtual BOOL	FindProp(	CError	       * pError,
					CString		 Name,
					WORD		 ID,
					WORD	       & PropID,
					CTypeDef       & Type
					) = 0;

	virtual BOOL	NameProp(	WORD		 ID,
					WORD		 PropID,
					CString        & Name
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Reference Resolution Interface
//

interface IDirectServer : public IBase
{
	virtual BOOL	FindDirect(	CError        *	pError,
					CString		Name,
					DWORD	      & ID,
					CTypeDef      & Type
					) = 0;

	virtual BOOL	NameDirect(	DWORD		ID,
					CString       & Name
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Resolution Interface
//

interface IFuncServer : public IBase
{
	virtual BOOL	FindFunction(	CError        *	pError,
					CString		Name,
					WORD	      & ID,
					CTypeDef      & Type,
					UINT	      & uCount
					) = 0;

	virtual BOOL	NameFunction(	WORD		ID,
					CString	      & Name
					) = 0;

	virtual BOOL	EditFunction(	WORD	      & ID,
					CTypeDef      & Type
					) = 0;

	virtual BOOL	GetFuncParam(	WORD		ID,
					UINT		uParam,
					CString	      & Name,
					CTypeDef      & Type
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Class Resolution Interface
//

interface IClassServer : public IBase
{
	virtual BOOL	FindClass(	CError        *	pError,
					CString		Name,
					UINT	      & Type
					) = 0;
	
	virtual	BOOL	NameClass(	UINT		Type,
					CString	      & Name
					) = 0;

	virtual	BOOL	SaveClass(	UINT		Type
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Library Interface
//

interface IFuncLibrary : public IFuncServer
{
	virtual BOOL	AddFunc(	CError	      & Error,
					IClassServer  * pClass,
					WORD		ID,
					CString		Func
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Identifier Library Interface
//

interface IIdentLibrary : public IIdentServer
{
	virtual BOOL	AddIdent(	CError	      & Error,
					IClassServer  * pClass,
					WORD		ID,
					CString		Func
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Name Resolution Interface
//

interface INameServer : public IIdentServer,
			public IDirectServer,
			public IFuncServer,
			public IClassServer
{
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Server Interface
//

interface IDataServer
{
	virtual void	SetData(	DWORD		ID,
					UINT		Type,
					UINT		Flags,
					DWORD		Data
					) = 0;

	virtual DWORD	GetData(	DWORD		ID,
					UINT		Type,
					UINT		Flags
					) = 0;
	
	virtual	DWORD	GetProp(	DWORD		ID,
					WORD		Prop,
					UINT		Type
					) = 0;

	virtual	PVOID	GetItem(	DWORD		ID
					) = 0;
	
	virtual DWORD	RunFunc(	WORD		ID,
					UINT		uParam,
					PDWORD		pParam
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Compiler Inputs
//

struct CCompileIn
{
	CString		m_Scope;
	PCTXT		m_pText;
	CTypeDef	m_Type;
	UINT		m_Optim;
	UINT		m_Debug;
	UINT		m_Switch;
	CStringArray    m_Using;
	UINT            m_uParam;
	CFuncParam    * m_pParam;
	INameServer   * m_pName;
	};

//////////////////////////////////////////////////////////////////////////
//
// Compiler Outputs
//

struct CCompileOut
{
	CTypeDef	m_Type;
	CError	        m_Error;
	CByteArray    * m_pSource;
	CLongArray    * m_pRefList;
	CByteArray    * m_pObject;
	};

//////////////////////////////////////////////////////////////////////////
//
// API for Function Library
//

DLLAPI BOOL  C3MakeFuncLib(	IFuncLibrary *& pLib
				);

//////////////////////////////////////////////////////////////////////////
//
// API for Identifier Library
//

DLLAPI BOOL  C3MakeIdentLib(	IIdentLibrary *	& pLib
				);

//////////////////////////////////////////////////////////////////////////
//
// API for Expression Compiler
//

DLLAPI BOOL  C3CompileExpr(	CCompileIn  const & In,
				CCompileOut       & Out
				);


//////////////////////////////////////////////////////////////////////////
//
// API for Function Compiler
//

DLLAPI BOOL  C3CompileFunc(	CCompileIn  const & In,
				CCompileOut       & Out
				);

//////////////////////////////////////////////////////////////////////////
//
// API for General Compiler
//

DLLAPI BOOL  C3CompileCode(	CCompileIn  const & In,
				CCompileOut       & Out,
				BOOL		    fFunc
				);

//////////////////////////////////////////////////////////////////////////
//
// API for Source Expansion
//

DLLAPI BOOL  C3ExpandSource(	PCBYTE		     pSource,
				INameServer        * pName,
				CStringArray const & Using, 
				CString	           & Text
				);

//////////////////////////////////////////////////////////////////////////
//
// API for Source Validation
//

DLLAPI BOOL  C3CheckSource(	PCBYTE		pSource
				);

//////////////////////////////////////////////////////////////////////////
//
// API for Code Execution
//

DLLAPI DWORD C3ExecuteCode(	PCBYTE		pCode,
				IDataServer *	pData,
				PDWORD		pParam
				);

//////////////////////////////////////////////////////////////////////////
//
// API for Syntax Color Analysis
//

DLLAPI BOOL C3SyntaxColors(	CArray <CColorSpan> & List,
				BOOL                & fComment,
				PCTXT		      pLine,
				UINT		      uSize
				);

//////////////////////////////////////////////////////////////////////////
//
// API for Syntax Token Analysis
//

DLLAPI BOOL C3SyntaxTokens(	CArray <CTokenSpan> & List,
				BOOL                & fComment,
				PCTXT		      pLine,
				UINT		      uSize
				);

// End of File

#endif
