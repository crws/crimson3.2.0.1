
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Function Library
//

class CFuncLibrary : public CBasicParser, public IFuncLibrary
{
	public:
		// Constructor
		CFuncLibrary(void);

		// Destructor
		~CFuncLibrary(void);

		// IBase Methods
		UINT Release(void);

		// IFuncLibrary Methods
		BOOL AddFunc(CError &Error, IClassServer *pClass, WORD ID, CString Func);

		// IFuncServer Methods
		BOOL FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount);
		BOOL NameFunction(WORD ID, CString &Name);
		BOOL EditFunction(WORD &ID, CTypeDef &Type);
		BOOL GetFuncParam(WORD ID, UINT uParam, CString &Name, CTypeDef &Type);

	protected:
		// Argument List
		typedef CArray <CTypeDef> CTypeArray;

		// Function Definition
		struct CFuncDef
		{
			// Data Members
			WORD		m_ID;
			CString		m_Name;
			CTypeDef	m_Type;
			CStringArray	m_ArgName;
			CTypeArray	m_ArgType;
			};

		// Function List
		CArray <CFuncDef> m_List;

		// Lookup Indexes
		CMap <CString, UINT> m_IndexName;
		CMap <WORD,    UINT> m_IndexID;

		// Data Members
		BOOL m_fPair;

		// Implementation
		void ParseFunc(IClassServer *pClass, CFuncDef &Func);
		void ParseType(IClassServer *pClass, CTypeDef &Type);
		BOOL ParseName(CString &Name);
		void ChangeType(CFuncDef &Func, UINT Target);
		BOOL ChangeType(CTypeDef &Type, UINT Target);
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Library
//

// Instantiator

DLLAPI BOOL C3MakeFuncLib(IFuncLibrary * &pLib)
{
	pLib = New CFuncLibrary;

	return TRUE;
	}

// Constructor

CFuncLibrary::CFuncLibrary(void)
{
	}

// Destructor

CFuncLibrary::~CFuncLibrary(void)
{
	}

// IBase Methods

UINT CFuncLibrary::Release(void)
{
	delete this;

	return 0;
	}

// IFuncLibrary Methods

BOOL CFuncLibrary::AddFunc(CError &Error, IClassServer *pClass, WORD ID, CString Func)
{
	m_pError = &Error;

	m_fPair  = FALSE;

	m_Lex.Attach(Func);

	try {
		GetToken();

		CFuncDef Func;

		Func.m_ID = ID;

		ParseFunc(pClass, Func);

		Func.m_ArgName.Compress();

		Func.m_ArgType.Compress();

		if( m_fPair ) {

			CFuncDef Func1, Func2;

			Func1 = Func;

			Func2 = Func;

			Func2.m_ID += 1;

			ChangeType(Func1, typeInteger);

			ChangeType(Func2, typeReal);

			UINT uIndex1 = m_List.Append(Func1);

			UINT uIndex2 = m_List.Append(Func2);

			m_IndexName.Insert(Func.m_Name, uIndex1);

			m_IndexID.Insert(Func1.m_ID, uIndex1);

			m_IndexID.Insert(Func2.m_ID, uIndex2);
			}
		else {
			UINT uIndex = m_List.Append(Func);

			m_IndexName.Insert(Func.m_Name, uIndex);

			m_IndexID.Insert(Func.m_ID, uIndex);
			}

		return TRUE;
		}

	catch(CUserException const &) {

		return FALSE;
		}
	}

// IFuncServer Methods

BOOL CFuncLibrary::FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount)
{
	INDEX n = m_IndexName.FindName(Name);

	if( !m_IndexName.Failed(n) ) {

		CFuncDef const &Func = m_List[m_IndexName.GetData(n)];

		ID     = Func.m_ID;

		Type   = Func.m_Type;

		uCount = Func.m_ArgType.GetCount();

		return TRUE;
		}

	return FALSE;
	}

BOOL CFuncLibrary::NameFunction(WORD ID, CString &Name)
{
	INDEX n = m_IndexID.FindName(ID);

	if( !m_IndexName.Failed(n) ) {

		CFuncDef const &Func = m_List[m_IndexID.GetData(n)];

		Name = Func.m_Name;

		return TRUE;
		}

	return FALSE;
	}

BOOL CFuncLibrary::EditFunction(WORD &ID, CTypeDef &Type)
{
	CString Name;

	for( WORD i = ID;; i++ ) {

		INDEX n = m_IndexID.FindName(i);

		if( !m_IndexName.Failed(n) ) {

			CFuncDef const &Func = m_List[m_IndexID.GetData(n)];

			if( i == ID ) {

				Name = Func.m_Name;
				}
			else {
				if( Func.m_Name != Name ) {

					continue;
					}
				}

			if( Func.m_ArgType[0].m_Type != Type.m_Type ) {

				continue;
				}

			if( i > ID ) {

				ID   = i;

				Type = Func.m_Type;

				return TRUE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CFuncLibrary::GetFuncParam(WORD ID, UINT uParam, CString &Name, CTypeDef &Type)
{
	INDEX n = m_IndexID.FindName(ID);

	if( !m_IndexName.Failed(n) ) {

		CFuncDef const &Func = m_List[m_IndexID.GetData(n)];

		Name = Func.m_ArgName[uParam];

		Type = Func.m_ArgType[uParam];

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CFuncLibrary::ParseFunc(IClassServer *pClass, CFuncDef &Func)
{
	ParseType(pClass, Func.m_Type);

	if( ParseName(Func.m_Name) ) {

		MatchCode(tokenBracketOpen);

		if( m_Token.m_Code == tokenVoid ) {

			GetToken();
			}
		else {
			CTypeDef Type;

			CString  Name;

			for( UINT n = 0;; n++ ) {

				ParseType(pClass, Type);

				ParseName(Name);

				Name.Printf(L"%u", 1 + n);

				Func.m_ArgType.Append(Type);

				Func.m_ArgName.Append(Name);

				if( m_Token.m_Code == tokenComma ) {

					GetToken();

					continue;
					}

				break;
				}
			}

		MatchCode(tokenBracketClose);

		if( m_Token.m_Code == tokenConst ) {

			GetToken();

			return;
			}

		Func.m_Type.m_Flags |= flagActive;

		return;
		}

	Expected(L"function name");
	}

void CFuncLibrary::ParseType(IClassServer *pClass, CTypeDef &Type)
{
	if( m_Token.m_Group == groupKeyword ) {

		if( m_Token.m_Code == tokenClass ) {

			CString Name;

			GetToken();

			if( ParseName(Name) ) {

				UINT Code;

				if( !pClass->FindClass(m_pError, Name, Code) ) {

					ThrowError();
					}

				Type.m_Type = typeObject | Code;
				}
			else
				Expected(L"class name");
			}
		else {
			switch( m_Token.m_Code ) {

				case tokenVoid:		Type.m_Type = typeVoid;		break;
				case tokenInt:		Type.m_Type = typeInteger;	break;
				case tokenFloat:	Type.m_Type = typeReal;		break;
				case tokenString:	Type.m_Type = typeString;	break;
				case tokenNumeric:	Type.m_Type = typeNumeric;	break;

				default:		Expected(L"type name");

				}

			if( Type.m_Type == typeNumeric ) {

				m_fPair = TRUE;
				}

			GetToken();
			}

		Type.m_Flags = 0;

		if( Type.m_Type ) {

			for(;;) {

				if( m_Token.m_Code == tokenBitwiseAnd ) {

					GetToken();

					Type.m_Flags |= flagWritable;

					continue;
					}

				if( m_Token.m_Code == tokenIndexOpen ) {

					GetToken();

					if( m_Token.m_Code == tokenConst ) {

						GetToken();

						MatchCode(tokenIndexClose);

						Type.m_Flags |= flagElement;
						}
					else {
						MatchCode(tokenIndexClose);

						Type.m_Flags |= flagElement | flagWritable;
						}

					continue;
					}

				if( m_Token.m_Code == tokenConst ) {

					GetToken();

					Type.m_Flags |= flagConstant;

					continue;
					}

				break;
				}
			}

		return;
		}

	Expected(L"type name");
	}

BOOL CFuncLibrary::ParseName(CString &Name)
{
	if( m_Token.m_Group == groupIdentifier ) {

		Name = m_Token.m_Const.GetStringValue();

		GetToken();

		return TRUE;
		}

	return FALSE;
	}

void CFuncLibrary::ChangeType(CFuncDef &Func, UINT Target)
{
	for( UINT n = 0; n < Func.m_ArgType.GetCount(); n++ ) {

		CTypeDef &Type = (CTypeDef &) Func.m_ArgType[n];

		ChangeType(Type, Target);
		}

	ChangeType(Func.m_Type, Target);
	}

BOOL CFuncLibrary::ChangeType(CTypeDef &Type, UINT Target)
{
	if( Type.m_Type == typeNumeric ) {

		Type.m_Type = Target;

		return TRUE;
		}

	return FALSE;
	}

// End of File
