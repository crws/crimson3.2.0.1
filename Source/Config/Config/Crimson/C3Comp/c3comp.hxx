
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3COMP_HXX
	
#define	INCLUDE_C3COMP_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdialog.hxx>

// End of File

#endif
