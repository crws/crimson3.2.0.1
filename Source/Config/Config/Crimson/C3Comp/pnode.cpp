
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Parse Tree Node
//

// Constructor

CParseNode::CParseNode(void) : CLexToken()
{
	m_uOrder = 0;

	m_Type   = 0;
	}

CParseNode::CParseNode(CParseNode const &That)
{
	m_Code   = That.m_Code;

	m_Group  = That.m_Group;

	m_Const  = That.m_Const;

	m_Range  = That.m_Range;

	m_uLine  = That.m_uLine;

	m_uOrder = That.m_uOrder;

	m_Type   = That.m_Type;
	}

CParseNode::CParseNode(UINT uOrder, CLexToken const &Token, UINT Type) : CLexToken(Token)
{
	m_uOrder = uOrder;

	m_Type   = (Type == typeLogical) ? typeInteger : Type;
	}

// Attributes

UINT CParseNode::GetOrder(void) const
{
	return m_uOrder;
	}

UINT CParseNode::GetType(void) const
{
	return m_Type;
	}

CString CParseNode::Describe(void) const
{
	CString Text;

	if( m_Group == groupIdentifier ) {

		CString Format;

		switch( m_Code ) {

			case usageVariable:
				
				Format = IDS_NODE_VARIABLE;
				
				break;
			
			case usageFunction:
				
				Format = IDS_NODE_FUNCTION;
				
				break;
			
			case usageDirectRef:
				
				Format = IDS_NODE_DIRECT;
				
				break;
			
			case usageFullRef:
				
				Format = IDS_NODE_FULL;
				
				break;

			case usageLocal:
				
				Format = IDS_NODE_LOCAL;
				
				break;

			case usageProperty:

				Format = IDS_NODE_PROP;

				break;

			default:
				
				AfxAssert(FALSE);
				
				break;
			}
			
		if( !m_Const.IsType(typeString) ) {

			UINT n = 0;

			switch( m_Code ) {

				case usageVariable:
				case usageFunction:
				case usageLocal:
				case usageProperty:
					
					n = 4;
					
					break;

				case usageDirectRef:
				case usageFullRef:
					
					n = 8;
					
					break;
				}

			Text.Format( Format,
				     CPrintf( L"%*.*X",
					      n, n,
					      m_Const.GetIntegerValue()
					      )
				     );
			}
		else {
			Text.Format( Format,
				     CPrintf( L"'%s'",
					      m_Const.GetStringValue()
					      )
				     );
			}

		return Text;
		}

	return CLexToken::Describe();
	}

// End of File
