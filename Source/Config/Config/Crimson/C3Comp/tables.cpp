
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Lexical Tables
//

// Static Data

CLexTables * CLexTables::m_pThis = NULL;

// Object Location

CLexTables * CLexTables::FindInstance(void)
{
	return m_pThis;
	}

// Constructor

CLexTables::CLexTables(void)
{
	AfxAssert(!m_pThis);

	MakeKeywordMap();

	MakeSeparatorMap();

	MakeOperatorMap();

	m_pThis = this;
	}

// Destructor

CLexTables::~CLexTables(void)
{
	m_pThis = NULL;
	}

// Lookup

UINT CLexTables::Lookup(UINT Group, CString const &Text)
{
	CForeMap const *pMap = NULL;

	switch( Group ) {

		case groupKeyword:
			
			pMap = &m_KeywordMap;
			
			break;

		case groupSeparator:
			
			pMap = &m_SeparatorMap;

			break;

		case groupOperator:
			
			pMap = &m_OperatorMap;

			break;
		}

	if( pMap ) {

		INDEX n = pMap->FindName(Text);

		if( !pMap->Failed(n) ) {

			return pMap->GetData(n);
			}
		}

	return tokenNull;
	}

// Expansion

CString CLexTables::Expand(UINT Code)
{
	if( Code == tokenMakeInteger ) {

		return L"";
		}

	if( Code == tokenNull ) {

		return L"NULL";
		}

	CString Text = m_ReverseMap[Code];

	if( Text.IsEmpty() ) {

		Text.Printf(L"0x%2.2X", Code);
		}

	return Text;
	}

// Implementation

void CLexTables::MakeKeywordMap(void)
{
	CForeMap &Map = m_KeywordMap;

	Map.Insert(	L"break",	tokenBreak	);
	Map.Insert(	L"bool",	tokenBool	);
	Map.Insert(	L"case",	tokenCase	);
	Map.Insert(	L"class",	tokenClass	);
	Map.Insert(	L"const",	tokenConst	);
	Map.Insert(	L"continue",	tokenContinue	);
	Map.Insert(	L"cstring",	tokenString	);
	Map.Insert(	L"default",	tokenDefault	);
	Map.Insert(	L"dispatch",	tokenDispatch   );
	Map.Insert(	L"do",		tokenDo		);
	Map.Insert(	L"else",	tokenElse	);
	Map.Insert(	L"false",	tokenFalse	);
	Map.Insert(	L"float",	tokenFloat	);
	Map.Insert(	L"continue",	tokenContinue	);
	Map.Insert(	L"for",		tokenFor	);
	Map.Insert(	L"if",		tokenIf		);
	Map.Insert(	L"int",		tokenInt	);
	Map.Insert(	L"numeric",	tokenNumeric	);
	Map.Insert(	L"return",	tokenReturn	);
	Map.Insert(	L"Run",		tokenRun	);
	Map.Insert(	L"switch",	tokenSwitch	);
	Map.Insert(	L"true",	tokenTrue	);
	Map.Insert(	L"using",	tokenUsing	);
	Map.Insert(	L"void",	tokenVoid	);
	Map.Insert(	L"WAS",		tokenWasComment	);
	Map.Insert(	L"while",	tokenWhile	);

	AddReverseEntries(Map);
	}

void CLexTables::MakeSeparatorMap(void)
{
	CForeMap &Map = m_SeparatorMap;

	Map.Insert(	L"(",	tokenBracketOpen	);
	Map.Insert(	L")",	tokenBracketClose	);
	Map.Insert(	L"[",	tokenIndexOpen		);
	Map.Insert(	L"]",	tokenIndexClose		);
	Map.Insert(	L"{",	tokenBraceOpen		);
	Map.Insert(	L"}",	tokenBraceClose		);
	Map.Insert(	L":",	tokenColon		);
	Map.Insert(	L";",	tokenSemicolon		);

	AddReverseEntries(Map);
	}

void CLexTables::MakeOperatorMap(void)
{
	CForeMap &Map = m_OperatorMap;

	Map.Insert(	L"++",	tokenPostIncrement	);
	Map.Insert(	L"--",	tokenPostDecrement	);
	Map.Insert(	L".",	tokenBitSelect		);
	Map.Insert(	L"!",	tokenLogicalNot		);
	Map.Insert(	L"~",	tokenBitwiseNot		);
	Map.Insert(	L"*",	tokenMultiply		);
	Map.Insert(	L"/",	tokenDivide		);
	Map.Insert(	L"%",	tokenRemainder		);
	Map.Insert(	L"+",	tokenAdd		);
	Map.Insert(	L"-",	tokenSubtract		);
	Map.Insert(	L"<<",	tokenLeftShift		);
	Map.Insert(	L">>",	tokenRightShift		);
	Map.Insert(	L"<",	tokenLessThan		);
	Map.Insert(	L"<=",	tokenLessOr		);
	Map.Insert(	L">",	tokenGreaterThan	);
	Map.Insert(	L">=",	tokenGreaterOr		);
	Map.Insert(	L"==",	tokenEqual		);
	Map.Insert(	L"!=",	tokenNotEqual		);
	Map.Insert(	L"&",	tokenBitwiseAnd		);
	Map.Insert(	L"^",	tokenBitwiseXor		);
	Map.Insert(	L"|",	tokenBitwiseOr		);
	Map.Insert(	L"&&",	tokenLogicalAnd		);
	Map.Insert(	L"||",	tokenLogicalOr		);
	Map.Insert(	L"=",	tokenOldAssignment	);
	Map.Insert(	L":=",	tokenNewAssignment	);
	Map.Insert(	L"*=",	tokenMultiplyAssign	);
	Map.Insert(	L"/=",	tokenDivideAssign	);
	Map.Insert(	L"%=",	tokenRemainderAssign	);
	Map.Insert(	L"+=",	tokenAddAssign		);
	Map.Insert(	L"-=",	tokenSubtractAssign	);
	Map.Insert(	L"<<=",	tokenLeftShiftAssign	);
	Map.Insert(	L">>=",	tokenRightShiftAssign	);
	Map.Insert(	L"&=",	tokenAndAssign		);
	Map.Insert(	L"^=",	tokenXorAssign		);
	Map.Insert(	L"|=",	tokenOrAssign		);
	Map.Insert(	L",",	tokenComma		);
	Map.Insert(	L"?",	tokenQuestion		);
	Map.Insert(	L"??",	tokenCondition		);
	Map.Insert(	L"/*",	tokenBlockComment	);
	Map.Insert(	L"//",	tokenLineComment	);

	AddReverseEntries(Map);

	AddReverseOperators();
	};

void CLexTables::AddReverseOperators(void)
{
	CForeMap Map;

	Map.Insert(	L"++",	tokenPreIncrement	);
	Map.Insert(	L"--",	tokenPreDecrement	);
	Map.Insert(	L"-",	tokenUnaryMinus		);
	Map.Insert(	L"+",	tokenUnaryPlus		);

	Map.Insert(	L"?",	tokenLogicalTest	);
	Map.Insert(	L"arg",	tokenArgument		);
	Map.Insert(     L"[$",  tokenIndexString        );
	Map.Insert(     L"[+",  tokenIndexExtended      );
	Map.Insert(     L"..",  tokenBitSelectLong	);
	Map.Insert(     L".?",  tokenGetProperty	);

	AddReverseEntries(Map);
	};

void CLexTables::AddReverseEntries(CForeMap const &Map)
{
	INDEX n = Map.GetHead();
	
	while( !Map.Failed(n) ) {

		m_ReverseMap.Insert( Map.GetData(n),
				     Map.GetName(n)
				     );

		Map.GetNext(n);
		}
	}

// End of File
