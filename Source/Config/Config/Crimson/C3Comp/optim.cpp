
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tree Optimizer
//

// Constructor

CTreeOptimizer::CTreeOptimizer(CParseTree &Tree) : m_Tree(Tree)
{
	}

// Operations

BOOL CTreeOptimizer::Optimize(CError &Error)
{
	m_pError = &Error;

	try {
		m_uPos = m_Tree.GetCount() - 1;

		ShowTree(L"INPUT");

		OptimizeNode();

		ShowTree(L"OUTPUT");

		return TRUE;
		}

	catch(CUserException const &) {

		return FALSE;
		}
	}

// Optimization

UINT CTreeOptimizer::OptimizeNode(void)
{
	UINT uPos   = m_uPos--;

	UINT uTotal = 0;

	for( UINT n = 0; n < m_Tree[uPos].m_uOrder; n++ ) {

		UINT uDelta = OptimizeNode();

		uTotal += uDelta;

		uPos   -= uDelta;
		}

	return uTotal + OptimizeNode(uPos);
	}

UINT CTreeOptimizer::OptimizeNode(UINT uPos)
{
	m_uError = uPos;

	static UINT (CTreeOptimizer::*List[])(UINT) = {
	
		&CTreeOptimizer::FoldConstants,
		&CTreeOptimizer::CheckIdentities,
		&CTreeOptimizer::CheckConstIndex,
		&CTreeOptimizer::CheckConstBit,
		&CTreeOptimizer::CheckCompare,
		&CTreeOptimizer::CheckSequence,
		&CTreeOptimizer::CheckLogical,
		&CTreeOptimizer::CheckTernary,
		&CTreeOptimizer::CheckConstStatement,

		};

	UINT uTotal = 0;

	UINT uDelta = 0;

	for( UINT n = 0; n < elements(List); n++ ) {

		if( uDelta = (this->*List[n])(uPos) ) {

			uTotal += uDelta;

			uPos   -= uDelta;
			}
		}

	return uTotal;
	}

// Constant Statements

UINT CTreeOptimizer::CheckConstStatement(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupKeyword ) {

		if( Node.m_uOrder >= 2 ) {

			UINT uTest = uPos - 1;

			if( Node.m_Code == tokenDo ) {

				UINT uDepth = GetNodeDepth(uTest);

				uTest = uTest - uDepth;
				}

			CParseNode const &Test = m_Tree[uTest];

			if( Test.m_Group == groupConstant ) {

				BOOL fTest = Test.m_Const.ForceLogical();

				switch( Node.m_Code ) {

					case tokenIf:
						
						return CheckConstIf(uPos, fTest);

					case tokenWhile:
						
						return CheckConstWhile(uPos, fTest);

					case tokenDo:
						
						return CheckConstDo(uPos, fTest);
					}
				}
			}
		}
		
	return 0;
	}

UINT CTreeOptimizer::CheckConstIf(UINT uPos, BOOL fTest)
{
	UINT pc = uPos - 1;

	UINT dc = 1;

	UINT pb = pc - dc;

	if( m_Tree[uPos].m_uOrder == 2 ) {

		if( fTest ) {

			DeleteNode(uPos, 1);

			DeleteNode(pc, dc);

			return dc + 1;
			}
		else {
			UINT db = GetNodeDepth(pb);

			ReplaceWithNull(uPos);

			DeleteNode(pc, dc);

			DeleteNode(pb, db);

			return dc + db;
			}
		}
	else {
		UINT db = GetNodeDepth(pb);

		UINT pe = pb - db;

		if( fTest ) {

			UINT de = GetNodeDepth(pe);

			DeleteNode(uPos, 1);

			DeleteNode(pc, dc);

			DeleteNode(pe, de);

			return dc + de + 1;
			}
		else {
			DeleteNode(uPos, 1);

			DeleteNode(pc, dc);

			DeleteNode(pb, db);

			return dc + db + 1;
			}
		}

	return 0;
	}

UINT CTreeOptimizer::CheckConstWhile(UINT uPos, BOOL fTest)
{
	if( !fTest ) {

		UINT pc = uPos - 1;

		UINT dc = 1;

		UINT pb = pc - dc;

		UINT db = GetNodeDepth(pb);

		ReplaceWithNull(uPos);

		DeleteNode(pc, dc);

		DeleteNode(pb, db);

		return dc + db;
		}

	return 0;
	}

UINT CTreeOptimizer::CheckConstDo(UINT uPos, BOOL fTest)
{
	if( !fTest ) {

		UINT px = uPos;

		UINT dx = 1;

		UINT pb = px - dx;
		
		UINT db = GetNodeDepth(pb);

		UINT pc = pb - db;

		UINT dc = GetNodeDepth(pc);

		DeleteNode(px, dx);

		DeleteNode(pc, dc);

		return dc + 1;
		}

	return 0;
	}

// Constant Index

UINT CTreeOptimizer::CheckConstIndex(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupSeparator && Node.m_Code == tokenIndexOpen ) {

		UINT pl = uPos - 1;

		UINT dl = GetNodeDepth(pl);

		UINT pr = pl - dl;

		CParseNode const &ArgL = m_Tree[pl];

		CParseNode const &ArgR = m_Tree[pr];

		if( ArgR.m_Group == groupConstant ) {

			UINT Index = ArgR.m_Const.GetIntegerValue();

			if( ArgL.m_Group == groupIdentifier ) {

				if( ArgL.m_Code == usageVariable ) {

					DWORD ID;
					
					ID = ArgL.m_Const.GetIntegerValue();

					((CDataRef &) ID).t.m_Array = Index;

					ReplaceWithFullRef(pl, ID, Node.m_Type);

					UINT dr = GetNodeDepth(pr);

					DeleteNode(uPos, 1);

					DeleteNode(pr, dr);

					return dr + 1;
					}
				}
			}
		}

	if( Node.m_Group == groupSeparator && Node.m_Code == tokenIndexExtended ) {

		UINT pl = uPos - 1;

		UINT dl = GetNodeDepth(pl);

		UINT pr = pl - dl;

		CParseNode const &ArgL = m_Tree[pl];

		CParseNode const &ArgR = m_Tree[pr];

		if( ArgR.m_Group == groupConstant ) {

			UINT Index = ArgR.m_Const.GetIntegerValue();

			if( ArgL.m_Group == groupIdentifier ) {
				
				if( ArgL.m_Code == usageVariable ) {

					DWORD ID;
					
					ID = ArgL.m_Const.GetIntegerValue();

					((CDataRef &) ID).x.m_Array = Index;

					ReplaceWithFullRef(pl, ID, Node.m_Type);

					UINT dr = GetNodeDepth(pr);

					DeleteNode(uPos, 1);

					DeleteNode(pr, dr);

					return dr + 1;
					}
				}
			}
		}

	return 0;
	}

// Constant Bit

UINT CTreeOptimizer::CheckConstBit(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupOperator ) {
		
		if( Node.m_Code == tokenBitSelect || Node.m_Code == tokenBitSelectLong ) {

			UINT pl = uPos - 1;

			UINT dl = GetNodeDepth(pl);

			UINT pr = pl - dl;

			CParseNode const &ArgL = m_Tree[pl];

			CParseNode const &ArgR = m_Tree[pr];

			if( ArgR.m_Group == groupConstant ) {

				C3INT nBit = ArgR.m_Const.GetIntegerValue();

				CheckBit(nBit);

				if( Node.m_Code == tokenBitSelectLong ) {

					return 0;
					}

				if( ArgL.m_Group == groupIdentifier ) {
					
					if( ArgL.m_Code == usageFullRef ) {

						DWORD ID = ArgL.m_Const.GetIntegerValue();

						((CDataRef &) ID).t.m_HasBit = 1;

						((CDataRef &) ID).t.m_BitRef = nBit;

						ReplaceWithFullRef(pl, ID, Node.m_Type);

						UINT dr = GetNodeDepth(pr);

						DeleteNode(uPos, 1);

						DeleteNode(pr, dr);

						return dr + 1;
						}

					if( ArgL.m_Code == usageVariable ) {

						DWORD Var = ArgL.m_Const.GetIntegerValue();

						DWORD ID  = 0;

						((CDataRef &) ID).t.m_IsTag  = ((CDataRef &) Var).t.m_IsTag;

						((CDataRef &) ID).t.m_Index  = ((CDataRef &) Var).t.m_Index;

						((CDataRef &) ID).t.m_HasBit = 1;

						((CDataRef &) ID).t.m_BitRef = nBit;

						ReplaceWithFullRef(pl, ID, Node.m_Type);

						UINT dr = GetNodeDepth(pr);

						DeleteNode(uPos, 1);

						DeleteNode(pr, dr);

						return dr + 1;
						}
					}
				}
			}
		}

	return 0;
	}

// Ternary Operator

UINT CTreeOptimizer::CheckTernary(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupOperator && Node.m_Code == tokenQuestion ) {

		UINT px = uPos - 1;

		UINT dx = GetNodeDepth(px);

		UINT pl = px - dx;

		UINT dl = GetNodeDepth(pl);

		CParseNode const &ArgX = m_Tree[px];

		if( ArgX.m_Group == groupConstant ) {

			DeleteNode(uPos, 1);

			if( ArgX.m_Const.ForceLogical() ) {

				UINT pr = pl - dl;

				UINT dr = GetNodeDepth(pr);

				DeleteNode(px, dx);

				DeleteNode(pr, dr);

				return dr + dx + 1;
				}
			else {
				DeleteNode(px, dx);

				DeleteNode(pl, dl);

				return dl + dx + 1;
				}
			}
		}

	return 0;
	}

// Compares to Zero

UINT CTreeOptimizer::CheckCompare(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupOperator ) {

		UINT Code;

		switch( Node.m_Code ) {

			case tokenEqual:
				
				Code = tokenLogicalNot;
				
				break;

			case tokenNotEqual:
				
				Code = tokenLogicalTest;
				
				break;

			default:
				return 0;
			}

		if( Node.m_Type == typeInteger || Node.m_Type == typeReal ) {

			UINT pl = uPos - 1;

			UINT dl = GetNodeDepth(pl);

			UINT pr = pl - dl;

			CParseNode const &ArgL = m_Tree[pl];

			CParseNode const &ArgR = m_Tree[pr];

			if( ArgL.m_Group == groupConstant ) {

				if( IsConst0(ArgL.m_Const) ) {

					ReplaceWithUnary(uPos, Code, Node.m_Type);

					DeleteNode(pl, 1);

					return 1;
					}
				}

			if( ArgR.m_Group == groupConstant ) {

				if( IsConst0(ArgR.m_Const) ) {

					ReplaceWithUnary(uPos, Code, Node.m_Type);

					DeleteNode(pr, 1);

					return 1;
					}
				}
			}
		}
	
	return 0;
	}

// Sequence Operator

UINT CTreeOptimizer::CheckSequence(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupOperator && Node.m_Code == tokenComma ) {

		UINT pl = uPos - 1;

		UINT dl = GetNodeDepth(pl);

		if( !IsActiveNode(pl) ) {

			DeleteNode(uPos, 1);

			DeleteNode(pl, dl);

			return dl + 1;
			}
		}

	return 0;
	}

// Logical Sequences

UINT CTreeOptimizer::CheckLogical(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupOperator ) {

		if( Node.m_Code == tokenLogicalAnd ) {

			return CheckLogical(uPos, TRUE);
			}

		if( Node.m_Code == tokenLogicalOr ) {

			return CheckLogical(uPos, FALSE);
			}
		}

	return 0;
	}

UINT CTreeOptimizer::CheckLogical(UINT uPos, BOOL fTest)
{
	UINT pl = uPos - 1;

	UINT dl = GetNodeDepth(pl);

	UINT pr = pl - dl;

	CParseNode const &ArgL = m_Tree[pl];
		
	CParseNode const &ArgR = m_Tree[pr];

	if( ArgL.m_Group == groupConstant ) {

		if( ArgL.m_Const.ForceLogical() == fTest ) {

			ReplaceWithUnary(uPos, tokenLogicalTest, 0);

			DeleteNode(pl, dl);

			return dl;
			}
		else {
			UINT dr = GetNodeDepth(pr);

			ReplaceWithConst(pl, C3INT(!fTest));

			DeleteNode(uPos, 1);

			DeleteNode(pr, dr);

			return dr + 1;
			}
		}

	if( ArgR.m_Group == groupConstant ) {

		if( ArgR.m_Const.ForceLogical() == fTest ) {

			UINT dr = GetNodeDepth(pr);

			ReplaceWithUnary(uPos, tokenLogicalTest, 0);

			DeleteNode(pr, dr);

			return dr;
			}
		else {
			if( IsActiveNode(pl) ) {

				ReplaceWithBinary(uPos, tokenComma, 0);

				ReplaceWithConst(pr, C3INT(!fTest));

				return 0;
				}
			else {
				ReplaceWithConst(pr, C3INT(!fTest));

				DeleteNode(uPos, 1);

				DeleteNode(pl, dl);

				return dl + 1;
				}
			}
		}

	return 0;
	}

// Identity Operations

UINT CTreeOptimizer::CheckIdentities(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupOperator ) {

		if( Node.m_uOrder == 2 ) {

			UINT pl = uPos - 1;

			UINT dl = GetNodeDepth(pl);

			UINT pr = pl - dl;

			CParseNode const &ArgL = m_Tree[pl];

			CParseNode const &ArgR = m_Tree[pr];
					
			if( ArgR.m_Group == groupConstant ) {

				if( IsConst1(ArgR.m_Const) ) {

					if( Node.m_Code == tokenAddAssign ) {

						UINT dr   = GetNodeDepth(pr);

						UINT Code = tokenPreIncrement;

						ReplaceWithUnary(uPos, Code, Node.m_Type);

						DeleteNode(pr, dr);

						return dr;
						}

					if( Node.m_Code == tokenSubtractAssign ) {

						UINT dr   = GetNodeDepth(pr);

						UINT Code = tokenPreDecrement;

						ReplaceWithUnary(uPos, Code, Node.m_Type);

						DeleteNode(pr, dr);

						return dr;
						}

					if( HasIdentity1(Node.m_Code) ) {

						UINT dr = GetNodeDepth(pr);

						DeleteNode(uPos, 1);

						DeleteNode(pr, dr);

						return dr + 1;
						}
					}

				if( IsConst0(ArgR.m_Const) ) {

					if( ZeroFromRight(Node.m_Code) ) {

						DeleteNode(uPos, 1);

						DeleteNode(pl, dl);

						return 1 + dl;
						}

					if( HasIdentity0(Node.m_Code) ) {

						UINT dr = GetNodeDepth(pr);

						DeleteNode(uPos, 1);

						DeleteNode(pr, dr);

						return dr + 1;
						}
					}
				}

			if( ArgL.m_Group == groupConstant ) {

				if( IsConst1(ArgL.m_Const) ) {

					if( DoesCommute(Node.m_Code) ) {

						if( HasIdentity1(Node.m_Code) ) {

							DeleteNode(uPos, 1);

							DeleteNode(pl, dl);

							return dl + 1;
							}
						}
					}

				if( IsConst0(ArgL.m_Const) ) {

					if( Node.m_Code == tokenSubtract ) {

						UINT Code = tokenUnaryMinus;

						ReplaceWithUnary(uPos, Code, Node.m_Type);

						DeleteNode(pl, dl);

						return dl;
						}

					if( ZeroFromLeft(Node.m_Code) ) {

						UINT dr = GetNodeDepth(pr);

						DeleteNode(uPos, 1);

						DeleteNode(pr, dr);

						return 1 + dr;
						}

					if( DoesCommute(Node.m_Code) ) {

						if( HasIdentity0(Node.m_Code) ) {

							DeleteNode(uPos, 1);

							DeleteNode(pl, dl);

							return dl + 1;
							}
						}
					}
				}
			}
		}

	return 0;
	}

// Constant Folding

UINT CTreeOptimizer::FoldConstants(UINT uPos)
{
	CParseNode const &Node = m_Tree[uPos];

	if( Node.m_Group == groupOperator ) {

		CLexConst Const;

		UINT uOrder = 0;

		if( Node.m_uOrder == 1 ) {

			CParseNode const &ArgA = m_Tree[uPos-1];

			if( ArgA.m_Group == groupConstant ) {

				if( Node.m_Type == typeInteger ) {

					C3INT a = ArgA.m_Const.ForceInteger();
					
					C3INT r = 0;

					if( FoldOperator(Node.m_Code, a, r) ) {

						Const.SetValue(r);

						uOrder = 1;
						}
					}

				if( Node.m_Type == typeReal ) {

					C3REAL a = ArgA.m_Const.ForceReal();

					C3REAL r = 0;

					if( FoldOperator(Node.m_Code, a, r) ) {

						Const.SetValue(r);

						uOrder = 1;
						}
					}
				}
			}

		if( Node.m_uOrder == 2 ) {

			CParseNode const &ArgL = m_Tree[uPos-1];
		
			CParseNode const &ArgR = m_Tree[uPos-2];

			if( ArgR.m_Group == groupConstant ) {

				UINT Code = Node.m_Code;

				if( Code == tokenDivide || Code == tokenRemainder ) {

					if( Node.m_Type == typeInteger ) {
					
						C3INT a2 = ArgR.m_Const.ForceInteger();

						if( !a2 ) {

							m_pError->Set(IDS_CODE_DIV_ZERO);

							ThrowError();
							}
						}

					if( Node.m_Type == typeReal ) {

						C3REAL a2 = ArgR.m_Const.ForceReal();

						if( !a2 ) {

							m_pError->Set(IDS_CODE_DIV_ZERO);

							ThrowError();
							}
						}
					}

				if( ArgL.m_Group == groupConstant ) {

					if( Node.m_Type == typeInteger ) {

						C3INT a1 = ArgL.m_Const.ForceInteger();
						
						C3INT a2 = ArgR.m_Const.ForceInteger();

						C3INT r  = 0;

						if( FoldOperator(Node.m_Code, a1, a2, r) ) {

							Const.SetValue(r);

							uOrder = 2;
							}
						}

					if( Node.m_Type == typeReal ) {

						C3REAL a1 = ArgL.m_Const.ForceReal();
						
						C3REAL a2 = ArgR.m_Const.ForceReal();

						C3REAL r  = 0;

						if( FoldOperator(Node.m_Code, a1, a2, r) ) {

							Const.SetValue(r);

							uOrder = 2;
							}
						}
					}
				}
			}

		if( uOrder ) {

			ReplaceWithConst(uPos, Const);

			DeleteNode(uPos - 1, uOrder);

			return uOrder;
			}
		}

	return 0;
	}

BOOL CTreeOptimizer::FoldOperator(UINT Code, C3INT a1, C3INT a2, C3INT &r)
{
	if( Code == tokenBitSelect ) {

		CheckBit(a2);
		
		r = !!(a1&(1<<a2));

		return TRUE;
		}

	switch( Code ) {

		case tokenMultiply:		r = (a1* a2);	break;
		case tokenDivide:		r = (a1/ a2);	break;
		case tokenRemainder:		r = (a1% a2);	break;
		case tokenAdd:			r = (a1+ a2);	break;
		case tokenSubtract:		r = (a1- a2);	break;
		case tokenLeftShift:		r = (a1<<a2);	break;
		case tokenRightShift:		r = (a1>>a2);	break;
		case tokenLessThan:		r = (a1< a2);	break;
		case tokenLessOr:		r = (a1<=a2);	break;
		case tokenGreaterThan:		r = (a1> a2);	break;
		case tokenGreaterOr:		r = (a1>=a2);	break;
		case tokenEqual:		r = (a1==a2);	break;
		case tokenNotEqual:		r = (a1!=a2);	break;
		case tokenBitwiseAnd:		r = (a1& a2);	break;
		case tokenBitwiseXor:		r = (a1^ a2);	break;
		case tokenBitwiseOr:		r = (a1| a2);	break;
		case tokenLogicalAnd:		r = (a1&&a2);	break;
		case tokenLogicalOr:		r = (a1||a2);	break;

		default: return FALSE;

		}

	return TRUE;
	}

BOOL CTreeOptimizer::FoldOperator(UINT Code, C3REAL a1, C3REAL a2, C3REAL &r)
{
	switch( Code ) {

		case tokenMultiply:		r = C3REAL(a1* a2);	break;
		case tokenDivide:		r = C3REAL(a1/ a2);	break;
		case tokenAdd:			r = C3REAL(a1+ a2);	break;
		case tokenSubtract:		r = C3REAL(a1- a2);	break;
		case tokenLessThan:		r = C3REAL(a1< a2);	break;
		case tokenLessOr:		r = C3REAL(a1<=a2);	break;
		case tokenGreaterThan:		r = C3REAL(a1> a2);	break;
		case tokenGreaterOr:		r = C3REAL(a1>=a2);	break;
		case tokenEqual:		r = C3REAL(a1==a2);	break;
		case tokenNotEqual:		r = C3REAL(a1!=a2);	break;
		case tokenLogicalAnd:		r = C3REAL(a1&&a2);	break;
		case tokenLogicalOr:		r = C3REAL(a1||a2);	break;

		default: return FALSE;

		}

	return TRUE;
	}

BOOL CTreeOptimizer::FoldOperator(UINT Code, C3INT a, C3INT &r)
{
	switch( Code ) {

		case tokenLogicalTest:		r = !!a;	break;
		case tokenLogicalNot:		r =  !a;	break;
		case tokenBitwiseNot:		r =  ~a;	break;
		case tokenUnaryPlus:		r =   a;	break;
		case tokenUnaryMinus:		r =  -a;	break;

		default: return FALSE;

		}

	return TRUE;
	}

BOOL CTreeOptimizer::FoldOperator(UINT Code, C3REAL a, C3REAL &r)
{
	switch( Code ) {

		case tokenLogicalTest:		r = C3REAL(!!a);	break;
		case tokenLogicalNot:		r = C3REAL(!a );	break;
		case tokenUnaryPlus:		r = C3REAL(  a);	break;
		case tokenUnaryMinus:		r = C3REAL( -a);	break;

		default: return FALSE;

		}

	return TRUE;
	}

// Node Characterisation

BOOL CTreeOptimizer::IsActiveNode(UINT uPos)
{
	return IsActiveFrom(uPos);
	}

BOOL CTreeOptimizer::IsActiveFrom(UINT &uPos)
{
	CParseNode const &Node = m_Tree[uPos--];

	switch( Node.m_Group ) {
		
		case groupOperator:
			
			if( IsActiveOperator(Node.m_Code) ) {

				return TRUE;
				}
			break;

		case groupIdentifier:

			if( Node.m_Code == usageFunction ) {

				return TRUE;
				}
			break;
		}

	for( UINT n = 0; n < Node.m_uOrder; n++ ) {

		if( IsActiveFrom(uPos) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CTreeOptimizer::GetNodeDepth(UINT uPos)
{
	return GetDepthFrom(uPos);
	}

UINT CTreeOptimizer::GetDepthFrom(UINT &uPos)
{
	UINT uDepth = 1;

	CParseNode const &Node = m_Tree[uPos--];

	for( UINT n = 0; n < Node.m_uOrder; n++ ) {

		uDepth += GetDepthFrom(uPos);
		}

	return uDepth;
	}

// Token Characterisation

BOOL CTreeOptimizer::IsActiveOperator(UINT Code)
{
	switch( Code ) {

		case tokenPreIncrement:
		case tokenPreDecrement:
		case tokenPostIncrement:
		case tokenPostDecrement:
		case tokenOldAssignment:
		case tokenNewAssignment:
		case tokenMultiplyAssign:
		case tokenDivideAssign:
		case tokenRemainderAssign:
		case tokenAddAssign:
		case tokenSubtractAssign:
		case tokenLeftShiftAssign:
		case tokenRightShiftAssign:
		case tokenAndAssign:
		case tokenXorAssign:
		case tokenOrAssign:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CTreeOptimizer::HasIdentity0(UINT Code)
{
	switch( Code ) {

		case tokenAdd:
		case tokenSubtract:
		case tokenLeftShift:
		case tokenRightShift:
		case tokenBitwiseXor:
		case tokenBitwiseOr:
		case tokenAddAssign:
		case tokenSubtractAssign:
		case tokenLeftShiftAssign:
		case tokenRightShiftAssign:
		case tokenXorAssign:
		case tokenOrAssign:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CTreeOptimizer::HasIdentity1(UINT Code)
{
	switch( Code ) {

		case tokenMultiply:
		case tokenDivide:
		case tokenMultiplyAssign:
		case tokenDivideAssign:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CTreeOptimizer::DoesCommute(UINT Code)
{
	switch( Code ) {

		case tokenAdd:
		case tokenBitwiseXor:
		case tokenBitwiseOr:
		case tokenBitwiseAnd:
		case tokenMultiply:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CTreeOptimizer::ZeroFromLeft(UINT Code)
{
	switch( Code ) {

		case tokenMultiply:
		case tokenDivide:
		case tokenBitwiseAnd:
		case tokenLeftShift:
		case tokenRightShift:
			
			return TRUE;
		
		default:
			return FALSE;
		}
	}

BOOL CTreeOptimizer::ZeroFromRight(UINT Code)
{
	switch( Code ) {

		case tokenMultiply:
		case tokenBitwiseAnd:
			
			return TRUE;
		
		default:
			return FALSE;
		}
	}

// Tree Editing

void CTreeOptimizer::ReplaceWithNull(UINT uPos)
{
	CLexToken Token = m_Tree[uPos];

	Token.Set(groupOperator, tokenNull);

	m_Tree.SetAt(uPos, CParseNode(0, Token, typeVoid));
	}

void CTreeOptimizer::ReplaceWithUnary(UINT uPos, UINT Code, UINT Type)
{
	CLexToken Token = m_Tree[uPos];

	Token.Set(groupOperator, Code);

	m_Tree.SetAt(uPos, CParseNode(1, Token, Type));
	}

void CTreeOptimizer::ReplaceWithBinary(UINT uPos, UINT Code, UINT Type)
{
	CLexToken Token = m_Tree[uPos];

	Token.Set(groupOperator, Code);

	m_Tree.SetAt(uPos, CParseNode(2, Token, Type));
	}

void CTreeOptimizer::ReplaceWithConst(UINT uPos, CLexConst const &Const)
{
	m_Tree.SetAt(uPos, CParseNode(0, Const, Const.GetType()));
	}

void CTreeOptimizer::ReplaceWithFullRef(UINT uPos, DWORD ID, UINT Type)
{
	CParseNode Node;

	Node.m_uOrder = 0;

	Node.m_Group  = groupIdentifier;

	Node.m_Code   = usageFullRef;

	Node.m_Const  = C3INT(ID);

	Node.m_Type   = Type;

	m_Tree.SetAt(uPos, Node);
	}

void CTreeOptimizer::DeleteNode(UINT uPos, UINT uDepth)
{
	m_Tree.Remove(uPos + 1 - uDepth, uDepth);
	}

// Implementation

BOOL CTreeOptimizer::IsConst0(CLexConst const &Const)
{
	switch( Const.GetType() ) {

		case typeInteger:
		
			return Const.GetIntegerValue() == 0;

		case typeReal:
			
			return Const.GetRealValue() == 0.00;
		}

	return FALSE;
	}

BOOL CTreeOptimizer::IsConst1(CLexConst const &Const)
{
	switch( Const.GetType() ) {

		case typeInteger:
			
			return Const.GetIntegerValue() == 1;

		case typeReal:
			
			return Const.GetRealValue() == 1.00;
		}

	return FALSE;
	}

void CTreeOptimizer::CheckBit(C3INT nBit)
{
	if( nBit < 0 || nBit > 31 ) {

		m_pError->Set(IDS_CODE_BAD_BIT);

		ThrowError();
		}
	}

void CTreeOptimizer::ThrowError(void)
{
	CRange Pos = m_Tree[m_uError].GetPos();

	m_pError->SetRange(Pos);

	AfxThrowUserException();
	}

void CTreeOptimizer::ShowTree(PCTXT pName)
{
/*	AfxTrace(L"%s:\n", pName);

	for( UINT n = 0; n < m_Tree.GetCount(); n++ ) {

		CParseNode const &Node = m_Tree[n];

		AfxTrace( L"  %2u) %u-NODE: %s\n",
			  n,
			  Node.m_uOrder,
			  Node.Describe()
			  );
		}
	
	AfxTrace(L"\n");
*/	}

// End of File
