
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Lexical Token
//

// Static Data

BOOL CLexToken::m_fPrecFix = TRUE;

// Constructor

CLexToken::CLexToken(void)
{
	Empty();
	}

CLexToken::CLexToken(UINT Code)
{
	m_Code = Code;
	
	FindGroup();
	}

CLexToken::CLexToken(CLexToken const &That)
{
	m_Code  = That.m_Code;

	m_Group = That.m_Group;

	m_Const = That.m_Const;

	m_Range = That.m_Range;

	m_uLine = That.m_uLine;
	}

CLexToken::CLexToken(CLexConst const &Const)
{
	m_Group = groupConstant;

	m_Const = Const;

	m_Code  = 0;
	}

// Assignment

CLexToken const & CLexToken::operator = (CLexToken const &That)
{
	m_Code  = That.m_Code;

	m_Group = That.m_Group;

	m_Const = That.m_Const;

	m_Range = That.m_Range;

	m_uLine = That.m_uLine;

	return ThisObject;
	}

// Core Attributes

UINT CLexToken::GetGroup(void) const
{
	return m_Group;
	}

UINT CLexToken::GetCode(void) const
{
	return m_Code;
	}

CRange CLexToken::GetPos(void) const
{
	return m_Range;
	}

UINT CLexToken::GetStart(void) const
{
	return m_Range.m_nFrom;
	}

UINT CLexToken::GetEnd(void) const
{
	return m_Range.m_nTo;
	}

UINT CLexToken::GetLine(void) const
{
	return m_uLine;
	}

// Attribute Tests

BOOL CLexToken::IsEndOfText(void) const
{
	return m_Group == groupEndOfText;
	}

BOOL CLexToken::IsGroup(UINT Group) const
{
	return m_Group == Group;
	}
	
BOOL CLexToken::IsCode(UINT Code) const
{
	return m_Code == Code;
	}

// Characterization

BOOL CLexToken::IsBitSelect(void) const
{
	if( m_Group == groupOperator ) {
	
		if( m_Code == tokenBitSelect ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CLexToken::IsCastOperator(void) const
{
	if( m_Group == groupKeyword ) {
		
		switch( m_Code ) {
		
			case tokenInt:
			case tokenFloat:
			case tokenString:
				
				return TRUE;
			
			default:
				return FALSE;
			}
		}

	return FALSE;
	}

BOOL CLexToken::IsUnaryOperator(void) const
{
	switch( m_Code ) {

		case tokenPreIncrement:
		case tokenPreDecrement:
		case tokenLogicalTest:
		case tokenLogicalNot:
		case tokenBitwiseNot:
		case tokenUnaryMinus:
		case tokenUnaryPlus:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsPrefixOperator(void) const
{
	switch( m_Code ) {

		case tokenPreIncrement:
		case tokenPreDecrement:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsPostfixOperator(void) const
{
	switch( m_Code ) {
	
		case tokenPostIncrement:
		case tokenPostDecrement:
			
			return TRUE;
				
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsBinaryOperator(void) const
{
	switch( m_Code ) {

		case tokenBitSelect:
		case tokenMultiply:
		case tokenDivide:
		case tokenRemainder:
		case tokenAdd:
		case tokenSubtract:
		case tokenLeftShift:
		case tokenRightShift:
		case tokenLessThan:
		case tokenLessOr:
		case tokenGreaterThan:
		case tokenGreaterOr:
		case tokenEqual:
		case tokenNotEqual:
		case tokenBitwiseAnd:
		case tokenBitwiseXor:
		case tokenBitwiseOr:
		case tokenLogicalAnd:
		case tokenLogicalOr:
		case tokenOldAssignment:
		case tokenNewAssignment:
		case tokenMultiplyAssign:
		case tokenDivideAssign:
		case tokenRemainderAssign:
		case tokenAddAssign:
		case tokenSubtractAssign:
		case tokenLeftShiftAssign:
		case tokenRightShiftAssign:
		case tokenAndAssign:
		case tokenXorAssign:
		case tokenOrAssign:
		case tokenComma:
		case tokenCondition:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsIncDecOperator(void) const
{
	switch( m_Code ) {

		case tokenPreIncrement:
		case tokenPreDecrement:
		case tokenPostIncrement:
		case tokenPostDecrement:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsAssignOperator(void) const
{
	switch( m_Code ) {

		case tokenOldAssignment:
		case tokenNewAssignment:
		case tokenMultiplyAssign:
		case tokenDivideAssign:
		case tokenRemainderAssign:
		case tokenAddAssign:
		case tokenSubtractAssign:
		case tokenLeftShiftAssign:
		case tokenRightShiftAssign:
		case tokenAndAssign:
		case tokenXorAssign:
		case tokenOrAssign:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsSimpleAssign(void) const
{
	switch( m_Code ) {

		case tokenOldAssignment:
		case tokenNewAssignment:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsComplexAssign(void) const
{
	switch( m_Code ) {

		case tokenMultiplyAssign:
		case tokenDivideAssign:
		case tokenRemainderAssign:
		case tokenAddAssign:
		case tokenSubtractAssign:
		case tokenLeftShiftAssign:
		case tokenRightShiftAssign:
		case tokenAndAssign:
		case tokenXorAssign:
		case tokenOrAssign:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsLogicalOperator(void) const
{
	switch( m_Code ) {

		case tokenLogicalAnd:
		case tokenLogicalOr:
		case tokenLogicalTest:
		case tokenLogicalNot:
		case tokenLessThan:
		case tokenLessOr:
		case tokenGreaterThan:
		case tokenGreaterOr:
		case tokenEqual:
		case tokenNotEqual:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsLogicalSequence(void) const
{
	switch( m_Code ) {

		case tokenLogicalAnd:
		case tokenLogicalOr:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsStringAssign(void) const
{
	switch( m_Code ) {

		case tokenOldAssignment:
		case tokenNewAssignment:
		case tokenAddAssign:
		case tokenAndAssign:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsStringBinary(void) const
{
	switch( m_Code ) {

		case tokenAdd:
		case tokenLessThan:
		case tokenLessOr:
		case tokenGreaterThan:
		case tokenGreaterOr:
		case tokenEqual:
		case tokenNotEqual:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsFloatAssign(void) const
{
	switch( m_Code ) {

		case tokenOldAssignment:
		case tokenNewAssignment:
		case tokenMultiplyAssign:
		case tokenDivideAssign:
		case tokenAddAssign:
		case tokenSubtractAssign:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

BOOL CLexToken::IsFloatBinary(void) const
{
	switch( m_Code ) {

		case tokenLogicalAnd:
		case tokenLogicalOr:
		case tokenMultiply:
		case tokenDivide:
		case tokenAdd:
		case tokenSubtract:
		case tokenLessThan:
		case tokenLessOr:
		case tokenGreaterThan:
		case tokenGreaterOr:
		case tokenEqual:
		case tokenNotEqual:
			
			return TRUE;
			
		default:
			return FALSE;
		}
	}

UINT CLexToken::GetPriority(void) const
{
	switch( m_Code ) {

		case tokenIndexOpen:

			return 0;

		case tokenPreIncrement:
		case tokenPreDecrement:

			return 1;

		case tokenBitSelect:
			
			return m_fPrecFix ? 2 : 3;
			
		case tokenVoid:
		case tokenInt:
		case tokenFloat:
		case tokenString:
		case tokenPostIncrement:
		case tokenPostDecrement:
		case tokenLogicalTest:
		case tokenLogicalNot:
		case tokenBitwiseNot:
		case tokenUnaryMinus:
		case tokenUnaryPlus:
			
			return m_fPrecFix ? 3 : 2;

		case tokenMultiply:
		case tokenDivide:
		case tokenRemainder:
			
			return 4;
			
		case tokenAdd:
		case tokenSubtract:
			
			return 5;
			
		case tokenLeftShift:
		case tokenRightShift:
			
			return 6;
			
		case tokenLessThan:
		case tokenLessOr:
		case tokenGreaterThan:
		case tokenGreaterOr:
			
			return 7;
			
		case tokenEqual:
		case tokenNotEqual:
			
			return 8;
			
		case tokenBitwiseAnd:
			
			return 9;
			
		case tokenBitwiseXor:
			
			return 10;
			
		case tokenBitwiseOr:
			
			return 11;
			
		case tokenLogicalAnd:
			
			return 12;
			
		case tokenLogicalOr:
			
			return 13;
			
		case tokenQuestion:
			
			return 14;

		case tokenOldAssignment:
		case tokenNewAssignment:
		case tokenMultiplyAssign:
		case tokenDivideAssign:
		case tokenRemainderAssign:
		case tokenAddAssign:
		case tokenSubtractAssign:
		case tokenLeftShiftAssign:
		case tokenRightShiftAssign:
		case tokenAndAssign:
		case tokenXorAssign:
		case tokenOrAssign:
			
			return 15;
			
		case tokenComma:
			
			return 16;

		case tokenCondition:
			
			return 17;
		}
	
	return 18;
	}

// Constant Access

CLexConst const & CLexToken::GetConst(void) const
{
	return m_Const;
	}

CLexConst & CLexToken::GetConst(void)
{
	return m_Const;
	}

// Token Expansion

CString CLexToken::Expand(void) const
{
	return afxLexTabs->Expand(m_Code);
	}

// Token Descripion
		
CString CLexToken::Describe(void) const
{
	switch( m_Group ) {
	
		case groupConstant:
			
			return m_Const.Describe();
		
		case groupKeyword:
			
			return Describe(IDS_TOK_KEYWORD);
		
		case groupOperator:
			
			return Describe(IDS_TOK_OPERATOR);
		
		case groupIdentifier:
			
			return DescribeIdentifier();
		
		case groupSeparator:
			
			return DescribeSeparator();
		
		case groupEndOfText:
			
			return IDS_TOK_END_OF_TEXT;
		
		case groupWhiteSpace:
			
			return IDS_TOK_WHITESPACE;
		}

	AfxAssert(FALSE);		

	return L"<token>";
	}

// Core Operations

void CLexToken::Empty(void)
{
	m_Group = groupEndOfText;

	m_Code  = 0;

	m_Const.Empty();

	m_Range = CRange(FALSE);

	m_uLine = 0;
	}

void CLexToken::Set(UINT Group, UINT Code)
{
	m_Group = Group;

	m_Code  = Code;
	}

void CLexToken::SetGroup(UINT Group)
{
	m_Group = Group;
	}

void CLexToken::SetCode(UINT Code)
{
	m_Code = Code;

	FindGroup();
	}

// Code Adjustments

void CLexToken::AdjustPostfix(void)
{
	switch( m_Code ) {

		case tokenPreIncrement:
			
			m_Code = tokenPostIncrement;
			
			break;

		case tokenPreDecrement:
			
			m_Code = tokenPostDecrement;
			
			break;
		}
	}

void CLexToken::AdjustPrefix(void)
{
	switch( m_Code ) {

		case tokenPostIncrement:
			
			m_Code = tokenPreIncrement;
			
			break;

		case tokenPostDecrement:
			
			m_Code = tokenPreDecrement;
			
			break;

		case tokenAdd:
			
			m_Code = tokenUnaryPlus;
			
			break;

		case tokenSubtract:
			
			m_Code = tokenUnaryMinus;
			
			break;

		case tokenQuestion:
			
			m_Code = tokenLogicalTest;
			
			break;
		}
	}

void CLexToken::AdjustBinary(void)
{
	switch( m_Code ) {

		case tokenUnaryPlus:
			
			m_Code = tokenAdd;
			
			break;

		case tokenUnaryMinus:
			
			m_Code = tokenSubtract;
			
			break;
		}
	}

// Implementation

void CLexToken::FindGroup(void)
{
	if( m_Code == tokenIndexClose ) {

		m_Group = groupOperator;
		
		return;
		}

	if( m_Code == tokenRun ) {
	
		m_Group = groupKeyword;
		
		return;
		}

	if( m_Code >= tokenBreak && m_Code <= tokenClass ) {
	
		m_Group = groupKeyword;
		
		return;
		}

	if( m_Code >= tokenBracketOpen && m_Code <= tokenSemicolon ) {
	
		m_Group = groupSeparator;
		
		return;
		}
		
	m_Group = groupOperator;
	}

CString CLexToken::DescribeIdentifier(void) const
{
	return CFormat(IDS_TOK_IDENTIFIER, m_Const.GetStringValue());
	}

CString CLexToken::DescribeSeparator(void) const
{
	switch( m_Code ) {
	
		case tokenBracketOpen:
			
			return IDS_TOK_BRACKET_OPEN;
		
		case tokenBracketClose:
			
			return IDS_TOK_BRACKET_CLOSE;
		
		case tokenIndexOpen:
			
			return IDS_TOK_INDEX_OPEN;
		
		case tokenIndexClose:
			
			return IDS_TOK_INDEX_CLOSE;
		
		case tokenBraceOpen: 
			
			return IDS_TOK_BRACE_OPEN;
		
		case tokenBraceClose:
			
			return IDS_TOK_BRACE_CLOSE;
		
		case tokenColon:   
			
			return IDS_TOK_COLON;
		
		case tokenSemicolon: 
			
			return IDS_TOK_SEMICOLON;
		}	
		
	return Describe(IDS_TOK_SEPARATOR);
	}

CString CLexToken::Describe(ENTITY ID) const
{
	return CFormat(ID, Expand());
	}

// End of File
