
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Function Parser
//

// Constructor

CFuncParser::CFuncParser(void)
{
	}

// Destructor

CFuncParser::~CFuncParser(void)
{
	}

// Operations

BOOL CFuncParser::ParseCode(CCompileIn const &In, CCompileOut &Out, BOOL fFunc)
{
	m_Lex.Attach(In.m_pText);

	m_pName  = In.m_pName;

	m_Type   = In.m_Type;

	m_Switch = In.m_Switch;

	m_pError = &Out.m_Error;

	InitParser();

	ReadParam(In.m_pParam, In.m_uParam);

	ReadUsing(In.m_Using);

	try {
		GetToken();

		if( m_Token.IsEndOfText() ) {

			if( In.m_Type.m_Type >= typeObject ) {

				Out.m_Type.m_Type  = typeVoid;

				Out.m_Type.m_Flags = 0;

				m_Source.Append(0);

				return TRUE;
				}
			}

		if( m_Token.IsCode(tokenWasComment) ) {

			m_Source.Append(0);

			return TRUE;
			}

		if( fFunc ) {

			return ParseFunc(In.m_Type, Out.m_Type);
			}
		
		return ParseExpr(In.m_Type, Out.m_Type);
		}
		
	catch(CUserException const &) {

		m_Tree.Empty();

		m_Source.Empty();

		return FALSE;
		}
	}

// Operations

BOOL CFuncParser::ParseFunc(CTypeDef const &In, CTypeDef &Out)
{
	if( m_Token.IsCode(tokenDispatch) ) {

		if( In.m_Flags & flagDispatch ) {

			GetToken();

			if( m_Token.IsCode(tokenBraceOpen) ) {

				m_fDispatch  = TRUE;

				ParseCompoundStatement(m_Token);

				Out.m_Type  = m_Type.m_Type;

				Out.m_Flags = flagActive | flagDispatch;

				return TRUE;
				}
			}

		Expected(NULL);
		}
	else {
		UINT uCount = 0;

		for(;;) {

			if( m_Token.IsEndOfText() ) {

				if( uCount > 1 ) {

					ProcessCompound(uCount);
					}
			
				Out.m_Type  = m_Type.m_Type;

				Out.m_Flags = flagActive;

				return TRUE;
				}

			ParseStatement();

			uCount++;
			}
		}

	return FALSE;
	}

// Initialization

void CFuncParser::InitParser(void)
{
	CLexToken::m_fPrecFix = (m_Switch & optPrecFix) ? TRUE : FALSE;

	m_uLoop = 0;

	m_uCase = 0;

	CExprParser::InitParser();
	}

// Statement Parser

void CFuncParser::ParseStatement(void)
{
	switch( m_Token.m_Group ) {
	
		case groupKeyword:
			
			ParseKeywordStatement();
			
			break;
			
		case groupSeparator:
			
			ParseSeparatorStatement();
			
			break;

		default:
		
			ParseExpressionStatement(m_Token);
			
			break;
		}
	}

void CFuncParser::ParseKeywordStatement(void)
{
	switch( m_Token.GetCode() ) {

		case tokenInt:
			
			ParseLocalVar(typeInteger);
			
			break;

		case tokenFloat:
			
			ParseLocalVar(typeReal);
			
			break;

		case tokenString:
			
			ParseLocalVar(typeString);
			
			break;

		case tokenBreak:
			
			ParseBreakStatement(m_Token);
			
			break;
			
		case tokenContinue:
			
			ParseContinueStatement(m_Token);
			
			break;
			
		case tokenDo:
			
			ParseDoStatement(m_Token);
			
			break;
			
		case tokenFor:
			
			ParseForStatement(m_Token);
			
			break;
			
		case tokenIf:
			
			ParseIfStatement(m_Token);
			
			break;
			
		case tokenReturn:
			
			ParseReturnStatement(m_Token);
			
			break;
			
		case tokenSwitch:
			
			ParseSwitchStatement(m_Token);
			
			break;

		case tokenUsing:

			ParseUsingStatement(m_Token);

			break;
			
		case tokenWhile:
			
			ParseWhileStatement(m_Token);
			
			break;

		default:
			
			ParseExpressionStatement(m_Token);
			
			break;
		}
	}

void CFuncParser::ParseLocalVar(UINT Type)
{
	CheckLocalVar(Type);

	CLexToken Token = m_Token;

	UINT      nVars = 0;

	GetToken();

	for(;;) {

		if( m_Token.m_Group == groupIdentifier ) {

			CString Name = m_Token.m_Const.GetStringValue();

			INDEX   nPos = m_LocalMap.FindName(Name);

			UINT	Slot = m_uLocal++;

			UINT	Hack = typeObject + Slot;

			nVars++;

			if( !m_LocalMap.Failed(nPos) ) {

				m_pError->Set(CFormat(IDS_FUNC_LOC_REDEFINE, Name));

				ThrowError();
				}

			if( m_uLocal == 256 ) {

				m_pError->Set(IDS_FUNC_LOC_TOO_MANY);

				ThrowError();
				}

			m_LocalMap.Insert(Name, MAKELONG(Slot, Type));

			m_LocalCtx.Append(m_LocalMap.FindName(Name));

			AppendSource(Name, 0);

			Token.m_Const = Name;
			
			GetToken();

			if( m_Token.m_Group == groupOperator ) {

				if( m_Token.IsSimpleAssign() ) {

					GetToken();

					ParseInitialData(Name, Type);

					ProcessNode(CParseNode(1, Token, Hack));
					}
				else
					ProcessNode(CParseNode(0, Token, Hack));
				}
			else
				ProcessNode(CParseNode(0, Token, Hack));

			if( m_Token.m_Group == groupSeparator ) {

				if( m_Token.m_Code == tokenSemicolon ) {

					if( nVars > 1 ) {

						ProcessNode(CParseNode(nVars, Token, Hack));
						}

					GetToken();

					return;
					}
				}

			MatchCode(tokenComma);

			continue;
			}

		Expected(NULL);
		}
	}

void CFuncParser::CheckLocalVar(UINT Type)
{
	if( m_uLoop && Type == typeString ) {

		// REV3 -- This restriction is a result of the failure of the
		// coder to handle the destruction of strings when we leave a
		// particular scope. Instead, it deletes all strings at the end
		// of the function, which causes problems if the same slot is
		// initialized more than once. The coder should be modified to
		// avoid this limitation at some point, but there's no rush.

		m_pError->Set(IDS_FUNC_LOC_STRING);

		ThrowError();
		}
	}

void CFuncParser::ParseBreakStatement(CLexToken Token)
{
	if( m_uLoop || m_uCase ) {

		GetToken();

		MatchCode(tokenSemicolon);

		ProcessNode(CParseNode(0, Token, typeVoid));

		return;
		}

	Expected(NULL);
	}

void CFuncParser::ParseContinueStatement(CLexToken Token)
{
	if( m_uLoop ) {

		GetToken();

		MatchCode(tokenSemicolon);

		ProcessNode(CParseNode(0, Token, typeVoid));

		return;
		}

	Expected(NULL);
	}

void CFuncParser::ParseDoStatement(CLexToken Token)
{
	GetToken();

	m_uLoop++;

	ParseStatement();

	m_uLoop--;

	MatchCode(tokenWhile);

	MatchCode(tokenBracketOpen);

	ParseControlExpression();

	MatchCode(tokenBracketClose);

	MatchCode(tokenSemicolon);

	ProcessNode(CParseNode(2, Token, typeVoid));
	}

void CFuncParser::ParseForStatement(CLexToken Token)
{
	GetToken();

	MatchCode(tokenBracketOpen);

	for( UINT n = 0; n < 3; n++ ) {

		if( !ParseOptionalExpression(n==1) ) {

			ProcessNode(CParseNode(0, tokenNull, typeVoid));
			}
		
		MatchCode(n == 2 ? tokenBracketClose : tokenSemicolon);
		}

	m_uLoop++;

	ParseStatement();

	m_uLoop--;

	ProcessNode(CParseNode(4, Token, typeVoid));
	}

void CFuncParser::ParseIfStatement(CLexToken Token)
{
	GetToken();

	MatchCode(tokenBracketOpen);

	ParseControlExpression();

	MatchCode(tokenBracketClose);

	ParseStatement();

	UINT uOrder = 2;

	if( m_Token.m_Code == tokenElse ) {

		GetToken();
		
		ParseStatement();

		uOrder++;
		}

	ProcessNode(CParseNode(uOrder, Token, typeVoid));
	}

void CFuncParser::ParseReturnStatement(CLexToken Token)
{
	GetToken();

	CLexToken Value;

	if( ParseExpression(Value) ) {

		if( m_Type.m_Type == typeVoid ) {
			
			if( !(m_Type.m_Flags & flagInherent) ) {

				m_pError->Set(IDS_FUNC_IS_VOID);

				ThrowError(Token);
				}
			}

		CheckConvert( CString(IDS_FUNC_RETURN),
			      ReadType(1),
			      m_Type,
			      Value
			      );

		m_Type.m_Type = PullType().m_Type;

		MatchCode(tokenSemicolon);

		ProcessNode(CParseNode(1, Token, m_Type.m_Type));
		}
	else {
		if( m_Type.m_Type == typeVoid ) {
			
			if( !(m_Type.m_Flags & flagInherent) ) {

				MatchCode(tokenSemicolon);

				ProcessNode(CParseNode(0, Token, typeVoid));

				return;
				}
			}

		Expected(NULL);
		}
	}

void CFuncParser::ParseSwitchStatement(CLexToken Token)
{
	m_uCase++;

	GetToken();

	MatchCode(tokenBracketOpen);

	ParseControlExpression();

	MatchCode(tokenBracketClose);

	MatchCode(tokenBraceOpen);

	CTree <UINT> Check;

	UINT uCount = 0;

	UINT uBlock = 0;

	UINT uLast  = 0;

	for(;;) {

		if( m_Token.m_Group == groupSeparator ) {
			
			if( m_Token.m_Code == tokenBraceClose ) {

				uCount += ProcessSwitchBlock(uBlock);

				GetToken();

				break;
				}
			}

		if( m_Token.m_Group == groupKeyword ) {

			if( m_Token.m_Code == tokenCase ) {

				CLexToken First = m_Token;

				CLexToken Token = First;

				if( uLast ) {

					CaseAfterDefault();
					}

				uCount += ProcessSwitchBlock(uBlock);

				UINT uConst;

				for( uConst = 0;; ) {

					GetToken();

					UINT uCase = ParseConstantExpression();

					if( uCase < NOTHING ) {

						if( !Check.Failed(Check.Find(uCase)) ) {

							RepeatedCase(Token, uCase);
							}

						Check.Insert(uCase);
						}
					
					uConst++;
					
					MatchCode(tokenColon);

					if( m_Token.m_Group == groupKeyword ) {

						if( m_Token.m_Code == tokenCase ) {

							Token = m_Token;

							continue;
							}
						}

					break;
					}

				ProcessNode(CParseNode(uConst, First, typeVoid));

				uCount++;

				continue;
				}

			if( m_Token.m_Code == tokenDefault ) {

				CLexToken Token = m_Token;

				if( uLast++ ) {

					MultipleDefaults();
					}

				uCount += ProcessSwitchBlock(uBlock);
				
				GetToken();

				MatchCode(tokenColon);

				ProcessNode(CParseNode(0, Token, typeVoid));

				uCount++;

				continue;
				}
			}

		if( uCount ) {

			ParseStatement();

			uBlock++;

			continue;
			}

		Expected(IDS_FUNC_MISS_CASE);
		}

	ProcessNode(CParseNode(uCount+1, Token, typeVoid));

	m_uCase--;
	}

void CFuncParser::ParseUsingStatement(CLexToken Token)
{
	GetToken();

	if( m_Token.m_Group == groupIdentifier ) {

		CString   Name = m_Token.m_Const.GetStringValue();

		CLexToken Full = m_Token;

		UINT      uPos = m_Source.GetCount();

		GetToken();

		while( m_Token.IsBitSelect() ) {

			GetToken();

			if( m_Token.m_Group == groupIdentifier ) {

				m_Source.Remove(uPos, 1);

				Name += L'.';

				Name += m_Token.m_Const.GetStringValue();

				Full.m_Range.m_nTo = m_Token.m_Range.m_nTo;

				GetToken();

				continue;
				}

			Expected(IDS_FOLDER_NAME);
			}

		MatchCode(tokenSemicolon);

		CTypeDef Type = { typeVoid, 0 };

		WORD     ID   = 0;

		if( m_pName->FindIdent(m_pError, Name, ID, Type) ) {

			if( Type.m_Type == typeFolder ) {

				m_pName->NameIdent(ID, Name);

				BYTE Src[3] = {	srcIdent,
						LOBYTE(ID),
						HIBYTE(ID)
						};

				InsertSource(uPos, Name, 1);

				InsertSource(uPos, Src,  elements(Src));

				UINT uCount = m_Using.GetCount();

				for( UINT n = 0; n < uCount; n++ ) {

					if( m_Using[n] == Name ) {

						m_pError->Set(IDS_FUNC_DUP_USING);

						ThrowError(Full);
						}
					}

				m_Using.Append(Name);

				return;
				}
			}

		m_Token = Full;
		}

	Expected(IDS_FOLDER_NAME);
	}

void CFuncParser::ParseWhileStatement(CLexToken Token)
{
	GetToken();

	MatchCode(tokenBracketOpen);

	ParseControlExpression();

	MatchCode(tokenBracketClose);

	m_uLoop++;

	ParseStatement();
	
	m_uLoop--;

	ProcessNode(CParseNode(2, Token, typeVoid));
	}

void CFuncParser::ParseSeparatorStatement(void)
{
	switch( m_Token.GetCode() ) {
	
		case tokenBraceOpen:
			
			ParseCompoundStatement(m_Token);
			
			break;
			
		case tokenSemicolon:
			
			ParseNullStatement(m_Token);
			
			break;
			
		default:
			
			ParseExpressionStatement(m_Token);
			
			break;
		}
	}

void CFuncParser::ParseCompoundStatement(CLexToken Token)
{
	GetToken();

	UINT uStart = m_LocalCtx.GetCount();

	UINT uLocal = 0;
	
	UINT uCount = 0;

	for(;;) {
	
		if( m_Token.m_Code == tokenBraceClose ) {

			if( uCount < 1 ) {

				CLexToken Other = Token;

				Other.SetCode(tokenNull);

				ProcessNode(CParseNode(0, Other, typeVoid));
				}

			if( uCount > 1 ) {
				
				ProcessCompound(uCount);
				}

			MatchCode(tokenBraceClose);

			break;
			}
			
		ParseStatement();

		uCount++;
		}

	if( (uLocal = m_LocalCtx.GetCount()) > uStart ) {

		for( UINT n = uStart; n < uLocal; n++ ) {

			INDEX nPos = m_LocalCtx[n];

			m_LocalMap.Remove(nPos);
			}

		m_LocalCtx.Remove(uStart, uLocal - uStart);
		}
	}

void CFuncParser::ParseNullStatement(CLexToken Token)
{
	CLexToken Other = Token;

	Other.SetCode(tokenNull);

	ProcessNode(CParseNode(0, Other, typeVoid));

	GetToken();
	}

void CFuncParser::ParseExpressionStatement(CLexToken Token)
{
	m_fActive = FALSE;

	CLexToken Value;

	if( ParseExpression(Value) ) {

		MatchCode(tokenSemicolon);

		if( !m_fActive ) {

			m_pError->Set(IDS_FUNC_ZERO_EFFECT);

			ThrowError(Value);
			}

		CLexToken Other = Token;

		Other.SetCode(tokenVoid);

		ProcessNode(CParseNode(1, Other, typeVoid));
	
		return;
		}
		
	Expected(NULL);
	}

UINT CFuncParser::ParseConstantExpression(void)
{
	CLexToken Value;

	if( ParseExpression(Value) ) {

		CTypeDef Test;

		Test.m_Type  = typeInteger;

		Test.m_Flags = flagConstant;

		CheckConvert( CString(IDS_EXPR_THIS_EXPR),
			      PullType(),
			      Test,
			      Value
			      );

		CParseNode const &Node = m_Tree[m_Tree.GetCount()-1];

		if( Node.m_Group == groupConstant ) {

			return Node.m_Const.GetIntegerValue();
			}

		return NOTHING;
		}

	Expected(IDS_FUNC_MISS_EXPR);

	return NOTHING;
	}

void CFuncParser::ParseControlExpression(void)
{
	CLexToken Value;

	if( ParseExpression(Value) ) {

		CTypeDef Test;

		Test.m_Type  = typeInteger;

		Test.m_Flags = 0;

		CheckConvert( CString(IDS_EXPR_THIS_EXPR),
			      PullType(),
			      Test,
			      Value
			      );

		return;
		}

	Expected(IDS_FUNC_MISS_CTRL);
	}

BOOL CFuncParser::ParseOptionalExpression(BOOL fCheck)
{
	m_fActive = FALSE;

	CLexToken Value;

	if( ParseExpression(Value) ) {

		if( fCheck ) {

			CTypeDef Test;

			Test.m_Type  = typeInteger;

			Test.m_Flags = 0;

			CheckConvert( CString(IDS_EXPR_THIS_EXPR),
				      PullType(),
				      Test,
				      Value
				      );
			}
		else {
			if( !m_fActive ) {

				m_pError->Set(IDS_FUNC_ZERO_EFFECT);

				ThrowError();
				}
			}

		return TRUE;
		}

	return FALSE;
	}

void CFuncParser::ParseInitialData(PCTXT pName, UINT Type)
{
	ClearCodeStack();

	ClearTypeStack();

	CLexToken Value;

	if( ParseSection(TRUE, Value) ) {

		CTypeDef Test;

		Test.m_Type  = Type;

		Test.m_Flags = 0;

		CheckConvert( CFormat(IDS_FUNC_INITIAL, pName),
			      PullType(),
			      Test,
			      Value
			      );

		return;
		}

	Expected(IDS_FUNC_MISS_INIT);
	}

// Output Processing

void CFuncParser::ProcessCompound(UINT uCount)
{
	ProcessNode(CParseNode(uCount, tokenBraceOpen, typeVoid));
	}

UINT CFuncParser::ProcessSwitchBlock(UINT &uBlock)
{
	if( uBlock ) {

		if( uBlock > 1 ) {
			
			ProcessCompound(uBlock);
			}

		uBlock = 0;

		return 1;
		}

	return 0;
	}

// Switch Errors

void CFuncParser::RepeatedCase(CLexToken const &Token, UINT n)
{
	m_pError->Set(CFormat(IDS_FUNC_REPEAT_CASE, CPrintf(L"%d", n)));

	ThrowError(Token);
	}

void CFuncParser::MultipleDefaults(void)
{
	m_pError->Set(IDS_FUNC_REPEAT_DEF);

	ThrowError();
	}

void CFuncParser::CaseAfterDefault(void)
{
	m_pError->Set(IDS_FUNC_LATE_CASE);

	ThrowError();
	}

// End of File
