
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EXECUTE_HPP
	
#define	INCLUDE_EXECUTE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "bcode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constant Access
//

#if defined(_M_M68K)

#define	ReadByte(p)	((p)[0])

#define	ReadWord(p)	(((WORD  *)(p))[0])

#define	ReadLong(p)	(((DWORD *)(p))[0])

#define	ReadText(p)	wstrdup(p)

#endif

#if defined(_M_IX86)

#define	ReadByte(p)	((p)[0])

#define	ReadWord(p)	MAKEWORD(ReadByte((p)+1),ReadByte((p)+0))

#define	ReadLong(p)	MAKELONG(ReadWord((p)+2),ReadWord((p)+0))

#define	ReadText(p)	wstrdpi(p)

#endif

#if defined(_M_ARM)

#define	ReadByte(p)	((p)[0])

#define	ReadWord(p)	MAKEWORD(ReadByte((p)+1),ReadByte((p)+0))

#define	ReadLong(p)	MAKELONG(ReadWord((p)+2),ReadWord((p)+0))

#define	ReadText(p)	wstrdpi(p)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Stack Access
//

#define	PUSH()		pStack--

#define	PULL()		pStack++

#define	SX_N(n)		(pStack[n])

#define	SX0		SX_N(0)
#define	SX1		SX_N(1)
#define	SX2		SX_N(2)
#define	SX3		SX_N(3)

#define	SI_N(n)		((C3INT  &) SX_N(n))

#define	SI0		SI_N(0)
#define	SI1		SI_N(1)
#define	SI2		SI_N(2)
#define	SI3		SI_N(3)

#define	SR_N(n)		((C3REAL &) SX_N(n))

#define	SR0		SR_N(0)
#define	SR1		SR_N(1)
#define	SR2		SR_N(2)
#define	SR3		SR_N(3)

#define	SS_N(n)		((WCHAR * &) SX_N(n))

#define	SS0		SS_N(0)
#define	SS1		SS_N(1)
#define	SS2		SS_N(2)
#define	SS3		SS_N(3)

// End of File

#endif
