
#include "intern.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Lexical Analyser
//

// Constants

#define	cSingleQuote	'\''

#define	cDoubleQuote	'\"'

#define	cBackslash	'\\'

#define MAX_STR_CONST	20000

// Constructor

CLexical::CLexical(void)
{
	m_pText    = NULL;
	
	m_fPLC     = FALSE;

	m_fPartial = FALSE;

	m_fComment = FALSE;
	}
		
// Attributes

BOOL CLexical::GetCommentState(void) const
{
	AfxAssert(m_fPartial);

	return m_fComment;
	}

// Operations

void CLexical::Attach(PCTXT pText)
{
	Attach(pText, NOTHING);
	}

void CLexical::Attach(PCTXT pText, UINT uSize)
{
	AfxValidateStringPtr(pText);

	m_pText     = pText;

	m_uSize     = uSize;
	
	m_uPos      = 0;

	m_uPrevPos  = 0;

	m_uLine     = 1;

	m_uThisLine = 1;

	m_uPrevLine = 0;

	m_pSource   = NULL;
	
	ReadChar(FALSE);
	}

void CLexical::SetSource(CByteArray &Source)
{
	m_pSource = &Source;
	}

BOOL CLexical::GetToken(CError &Error, CLexToken &Token)
{
	m_pError    = &Error;

	UINT uStart = 0;

	UINT uLine  = 0;

	BOOL fOkay  = TRUE;

	try {
		for(;;) {

			uStart = m_uThisPos;

			uLine  = m_uThisLine;

			if( m_fComment ) {

				m_Token.Empty();

				if( !ParseEndOfText() ) {

					m_Token.SetCode(tokenBlockComment);

					m_fComment = FALSE;
					}
				}
			else {
				m_Token.Empty();
			
				ParseToken();
				}

			if( m_Token.m_Code == tokenBlockComment ) {
			
				if( SkipBlockComment() ) {
				
					continue;
					}
				}

			if( m_Token.m_Code == tokenLineComment ) {
			
				if( SkipLineComment() ) {
				
					continue;
					}
				}

			if( m_Token.m_Code == tokenWasComment ) {

				// LATER -- How about in partial mode? How do
				// we handle WAS comments as far as the editor
				// is concerned? I suspect we just ignore them
				// so they don't screw up the editing, but
				// return the WAS token in such a way that we
				// highlight it on the display...

				if( uStart ) {

					m_pError->Set(IDS_LEX_WAS);
				
					ThrowError();
					}
			
				SkipWasComment();
				}

			Token = m_Token;

			Token.m_Range.m_nFrom = uStart;

			Token.m_Range.m_nTo   = m_uPrevPos + 1;

			Token.m_uLine         = uLine;
	
			m_fPLC = FALSE;

			break;
			}
		}

	catch(CUserException const &) {

		if( !m_fPartial ) {

			Token.Empty();

			Token.m_Range.m_nFrom = m_uThisPos;

			Token.m_Range.m_nTo   = m_uThisPos + 1;

			Token.m_uLine         = uLine;
		
			m_fPLC = FALSE;

			return FALSE;
			}

		while( m_cData ) {
			
			ReadChar(TRUE);
			}
	
		Token.m_Group = groupWhiteSpace;

		Token.m_Code  = tokenUnknown;

		Token.m_Range.m_nFrom = uStart;

		Token.m_Range.m_nTo   = m_uPos;

		Token.m_uLine         = uLine;

		m_fPLC = FALSE;
		}
			
	return fOkay;
	}

void CLexical::PartialMode(BOOL fComment)
{
	m_fPartial = TRUE;

	m_fComment = fComment;
	}

void CLexical::AcceptPlcRef(void)
{
	m_fPLC = TRUE;

	m_uPLC = 0;
	}

// Main Analyser

void CLexical::ParseToken(void)
{
	ParseEndOfText()	||
	ParseSpace()		||
	ParseConstant()		||
	ParseCharConst()	||
	ParseStringConst()	||
	ParseIdentifier()	||
	ParseSeparator()	||
	ParseOperator()		||
	ParseGarbage()		;;
	}

BOOL CLexical::ParseEndOfText(void)
{
	if( !m_cData ) {

		if( m_pSource ) {

			m_pSource->Append(0);
			}
		
		m_Token.m_Group = groupEndOfText;
			
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CLexical::ParseSpace(void)
{
	if( IsSpace() ) {

		do {
			ReadChar(TRUE);
				
			} while( m_cData && IsSpace() );
				
		m_Token.m_Group = groupWhiteSpace;
			
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CLexical::ParseConstant(void)
{
	if( !m_fPLC && IsNumeric() ) {
	        
		INT  nRadix = 10;
		
		BOOL fFirst = TRUE;
		
		if( m_cData == '0' ) {

			ReadChar(TRUE);
			
			if( Find(L"XB") ) {
			
				switch( ToUpper() ) {
				
					case 'X':
						nRadix = 16;
						break;
						
					case 'B':
						nRadix = 2;
						break;
					}
				
				ReadChar(TRUE);
				}
			else {
				if( IsNumeric() ) {

					nRadix = 8;
					}
				
				fFirst = FALSE;
				}
			}
			
		INT64 Value = 0;
	
		for(;;) {

			if( ToUpper() == 'E' ) {

				if( nRadix == 10 ) {

					return ParseReal(Value);
					}
				}

			if( m_cData == '.' ) {

				if( nRadix == 10 ) {
				
					ReadChar(TRUE);
	
					return ParseReal(Value);
					}

				m_pError->Set(IDS_LEX_BAD_DP);
					
				ThrowError();
				}
	
			PCTXT pHex = L"0123456789ABCDEF";
	
			PCTXT pPos = Find(pHex);
			
			if( !pPos || INT(pPos - pHex) >= nRadix ) {
			
				if( pPos || fFirst ) {
		
					m_pError->Set(IDS_LEX_BAD_DIGIT);
							
					ThrowError();
					}
					
				break;
				}
		
			Value *= nRadix;
			
			Value += (pPos - pHex);

			ReadChar(TRUE);

			fFirst = FALSE;
			}
	
		while( IsAlpha() ) {
		
			if( ToUpper() == 'L' ) {
				
				ReadChar(TRUE);

				nRadix = 0;
					
				continue;
				}
					
			if( ToUpper() == 'U' ) {
				
				ReadChar(TRUE);

				nRadix = 0;
				
				continue;
				}

			if( nRadix == 16 ) {

				m_pError->Set(IDS_LEX_BAD_DIGIT);
				}
			else
				m_pError->Set(IDS_LEX_BAD_SUFFIX);
			
			ThrowError();
			}

		if( C3INT(Value) != Value ) {

			m_pError->Set(IDS_LEX_INT_RANGE);
			}
			
		m_Token.m_Group = groupConstant;

		m_Token.m_Const = C3INT(Value);
		
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CLexical::ParseReal(INT64 Integer)
{
	double Radix = 10;

	double Whole = double(Integer);

	double Fract = 0;
	
	double Denom = 1;

	if( ToUpper() != 'E' ) {
		
		if( !IsNumeric() ) {

			m_pError->Set(IDS_LEX_HANGING_DP);
			
			ThrowError();
			}
		
		while( IsNumeric() ) {
		
			Fract = Fract * Radix + double(m_cData - '0');
				
			Denom = Denom * Radix;

			ReadChar(TRUE);
			}
		}

	if( ToUpper() == 'E' ) {

		int Exp  = 0;

		int Sign = 1;

		ReadChar(TRUE);

		if( m_cData == '-' || m_cData == '+' ) {

			if( m_cData == '-' ) {

				Sign = -1;
				}

			ReadChar(TRUE);
			}

		if( IsNumeric() ) {

			while( IsNumeric() ) {
			
				Exp *= 10;
				
				Exp += int(m_cData - '0');

				ReadChar(TRUE);
				}

			}
		else {
			m_pError->Set(IDS_MISSING_EXPONENT);

			ThrowError();
			}

		Fract *= pow(Radix, Sign * Exp);

		Whole *= pow(Radix, Sign * Exp);
		}

	m_Token.m_Group = groupConstant;
	
	m_Token.m_Const = C3REAL(Whole + Fract / Denom);

	return TRUE;
	}

BOOL CLexical::ParseCharConst(void)
{
	if( m_cData == cSingleQuote ) {
		
		ReadChar(TRUE);
	
		TCHAR cValue = ParseCharacter(TRUE);
		
		if( m_cData == cSingleQuote ) {
		
			ReadChar(TRUE);
	
			m_Token.m_Group = groupConstant;
			
			m_Token.m_Const = cValue;
			
			return TRUE;
			}

		m_pError->Set(IDS_LEX_LONG_CHAR);
		
		ThrowError();
		}
	
	return FALSE;
	}

BOOL CLexical::ParseStringConst(void)
{
	if( m_cData == cDoubleQuote ) {
		
		ReadChar(TRUE);

		CString Work;

		Work.Expand(256);

                for(;;) {
                
			if( m_cData == cDoubleQuote ) {				

				if( Work.GetLength() > MAX_STR_CONST ) {

					m_pError->Set(IDS_LEX_LONG_STRING);
				
					ThrowError();
					}

				ReadChar(TRUE);

                		m_Token.m_Group = groupConstant;

				m_Token.m_Const = Work;
                		
                		return TRUE;
                		}
                	
                	Work += TCHAR(ParseCharacter(FALSE));
			}
		}
	
	return FALSE;
	}

BOOL CLexical::ParseIdentifier(void)
{
	if( IsIdentFirst() ) {

		UINT Code;
		
		CString Work;

		Work.Expand(64);

		m_uPLC = 1;
		
		do {
			Work += m_cData;

			ReadChar(FALSE);

			if( m_fPLC ) {

				if( m_cData == '[' ) {
					
					m_uPLC++;
					}
				
				if( m_cData == ']' ) {
					
					m_uPLC--;
					}
				}

			} while( IsIdentRest() );

		if( (Code = afxLexTabs->Lookup(groupKeyword, Work)) ) {
		
			if( m_pSource ) {

				BYTE bCode = BYTE(Code | 0x80);

				m_pSource->Append(bCode);
				}
		
			m_Token.m_Group = groupKeyword;
			
			m_Token.m_Code  = Code;
				
			return TRUE;
			}
			
		m_Token.m_Group = groupIdentifier;
		
		m_Token.m_Const = Work;
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CLexical::ParseSeparator(void)
{
	UINT Code;

	CString Work;

	Work += m_cData;

	if( Code = afxLexTabs->Lookup(groupSeparator, Work) ) {

		ReadChar(FALSE);
		
		if( Code == tokenColon && m_cData == '=' ) {

			ReadChar(FALSE);

			Code = tokenNewAssignment;

			if( m_pSource ) {

				BYTE bCode = BYTE(Code | 0x80);

				m_pSource->Append(bCode);
				}
		
			m_Token.m_Group = groupOperator;
			
			m_Token.m_Code  = Code;
			}
		else {
			if( m_pSource ) {

				BYTE bCode = BYTE(Code | 0x80);

				m_pSource->Append(bCode);
				}
		
			m_Token.m_Group = groupSeparator;
			
			m_Token.m_Code  = Code;
			}
			
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CLexical::ParseOperator(void)
{
	UINT Code;

	UINT Last = tokenNull;

	CString Work;

	for(;;) {

		if( m_cData ) {

			Work += m_cData;

			if( Code = afxLexTabs->Lookup(groupOperator, Work) ) {

				ReadChar(FALSE);

				Last = Code;

				continue;
				}
			}

		if( Last ) {

			if( m_pSource ) {

				BYTE bCode = BYTE(Last | 0x80);

				m_pSource->Append(bCode);
				}

			m_Token.m_Group = groupOperator;
			
			m_Token.m_Code  = Last;

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CLexical::ParseGarbage(void)
{
	PCTXT p;
	
	if( m_cData < 32 || m_cData > 126 ) {

		if( HIBYTE(m_cData) ) {

			p = L"0x%4.4X";
			}
		else
			p = L"0x%2.2X";
		}
	else
		p = L"'%c'";

	m_pError->Set(CFormat(IDS_LEX_UNEXPECTED, CPrintf(p, m_cData)));

	ThrowError();

	return FALSE;
	}

// Comment Handling

BOOL CLexical::SkipBlockComment(void)
{
	int nState = 0;
	
	for(;;) {

		if( m_cData == 0 ) {

			if( m_fPartial ) {

				m_fComment = TRUE;

				return FALSE;
				}

			m_pError->Set(IDS_LEX_OPEN_COMMENT);
			
			ThrowError();

			break;
			}
		
		switch( nState ) {
		
			case 0:
				if( m_cData == '*' ) {

					nState = 1;
					}
				
				break;
				
			case 1:
				if( m_cData == '/' ) {

					ReadChar(TRUE);

					if( m_fPartial ) {

						return FALSE;
						}

					return TRUE;
					}

				nState = 0;

				break;
			}
		
		ReadChar(TRUE);
		}

	if( m_fPartial ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CLexical::SkipLineComment(void)
{
	while( m_cData ) {
			
		if( IsBreak() ) {
			
			ReadChar(TRUE);
			
			break;
			}
		
		ReadChar(TRUE);
		}

	if( m_fPartial ) {

		return FALSE;
		}

	return TRUE;
	}

void CLexical::SkipWasComment(void)
{
	while( m_cData ) {
			
		ReadChar(TRUE);
		}
	}

// Extended Characters

TCHAR CLexical::ParseCharacter(BOOL fChar)
{
	if( fChar ) {

		if( m_cData == cSingleQuote ) {

			m_pError->Set(IDS_LEX_EMPTY_CHAR);
			
			ThrowError();
			}

		if( IsBreak() ) {
		
			m_pError->Set(IDS_LEX_CR_IN_CHAR);
			
			ThrowError();
			}
		}
	else {
		if( IsBreak() ) {
		
			m_pError->Set(IDS_LEX_CR_IN_STRING);
			
			ThrowError();
			}
		}
		
	if( m_cData == cBackslash ) {
	
		ReadChar(TRUE);
		
		if( IsBreak() ) {
		
			m_pError->Set(IDS_LEX_CR_IN_ESC);
			
			ThrowError();
			}
		
		TCHAR cUpper = ToUpper();

		TCHAR cValue = 0;

		if( IsNumeric() || cUpper == 'X' || cUpper == 'U' ) {

			if( cUpper == 'X' || cUpper == 'U' ) {
			
				ReadChar(TRUE);

				if( cUpper == 'X' ) {
				
					cValue = ParseEscape(16, FALSE);
					}
				else
					cValue = ParseEscape(16, TRUE);
				}
			else
				cValue = ParseEscape(8, FALSE);

			if( !fChar ) {
				
				if( !cValue ) {

					m_pError->Set(CString(IDS_CANNOT_USE_NULL));
					
					ThrowError();
					}
				}
			}
		else {
			switch( cUpper ) {
			
				case cSingleQuote:
				case cDoubleQuote:
				case cBackslash:
					cValue = BYTE(m_cData);
					break;
				
				case 'A':
					cValue = 0x07;
					break;
					
				case 'T':
					cValue = 0x09;
					break;
				
				case 'N':
					cValue = 0x0A;
					break;
				
				case 'F':
					cValue = 0x0C;
					break;
				
				case 'R':
					cValue = 0x0D;
					break;
				
				case 'E':
					cValue = 0x1B;
					break;
				}
				
			if( !cValue ) {
			
				m_pError->Set(CFormat(IDS_LEX_BAD_ESCAPE, CPrintf(L"%c", m_cData)));
				
				ThrowError();
				}

			ReadChar(TRUE);
			}
		
		return cValue;
		}

	TCHAR cValue = m_cData;
	
	ReadChar(TRUE);
		
	return cValue;
	}

TCHAR CLexical::ParseEscape(INT nRadix, BOOL fWide)
{
	UINT uCount = (nRadix == 16) ? (fWide ? 4 : 2) : 3;

	BOOL fFirst = TRUE;

	INT  nValue = 0;

	while( uCount-- ) {
	
		PCTXT pHex = L"0123456789ABCDEF";
		
		PCTXT pPos = Find(pHex);
		
		if( !pPos || WORD(pPos - pHex) >= nRadix ) {
		
			if( fFirst ) {
			
				m_pError->Set(IDS_LEX_ESC_DIGIT);
				
				ThrowError();
				}
			
			break;
			}
			
		nValue = (nValue * nRadix) + (pPos - pHex);
		
		fFirst = FALSE;
		
		ReadChar(TRUE);
		}
		
	if( nValue > 65535 ) {
	
		m_pError->Set(IDS_LEX_ESC_OVERFLOW);
		
		ThrowError();
		}
	
	return TCHAR(nValue);
	}

// General Support

void CLexical::ReadChar(BOOL fStore)
{
	if( fStore && m_pSource ) {

		if( HIBYTE(m_cData) ) {

			m_pSource->Append(srcUnicode);

			m_pSource->Append(LOBYTE(m_cData));

			m_pSource->Append(HIBYTE(m_cData));
			}
		else {
			if( m_cData & 0x80 ) {

				m_pSource->Append(srcUpper);

				m_pSource->Append(BYTE(m_cData & 0x7F));
				}
			else
				m_pSource->Append(BYTE(m_cData));
			}
		}
			
	if( *m_pText && m_uSize ) {

		m_uPrevLine = m_uThisLine;

		m_uThisLine = m_uLine;
	
		if( (m_cData = *m_pText) == '\r' ) {
			
			m_uLine++;
			}

		m_pText++;

		m_uSize--;

		m_uPrevPos = m_uThisPos;

		m_uThisPos = m_uPos++;
		
		return;
		}

	if( m_cData ) {
		
		m_uPrevPos = m_uThisPos;

		m_uThisPos = m_uPos;
		}
		
	m_cData = 0;
	}

PCTXT CLexical::Find(PCTXT pList) const
{
	return m_cData ? wstrchr(pList, ToUpper()) : NULL;
	}
	
void CLexical::ThrowError(void)
{
	AfxThrowUserException();
	}

// Characterisers

BOOL CLexical::IsBreak(void) const
{
	return m_cData == 0x00 || m_cData == 0x0A || m_cData == 0x0D;
	}

BOOL CLexical::IsSpace(void) const
{
	return m_cData == 0x09 || m_cData == 0x20 || IsBreak();
	}

BOOL CLexical::IsNumeric(void) const
{
	return wisdigit(m_cData);
	}

BOOL CLexical::IsAlpha(void) const
{
	return wisalpha(m_cData);
	}

BOOL CLexical::IsAlphaNum(void) const
{
	return IsNumeric() || IsAlpha();
	}

BOOL CLexical::IsIdentFirst(void) const
{
	return m_fPLC ? IsPlcRef(TRUE)  : (m_cData == '_' || IsAlpha());
	}

BOOL CLexical::IsIdentRest(void) const
{
	return m_fPLC ? IsPlcRef(FALSE) : (m_cData == '_' || IsAlphaNum());
	}

BOOL CLexical::IsPlcRef(BOOL fFirst) const
{
	return (!fFirst || !IsSpace()) && !(m_cData == ']' && !m_uPLC);
	}

// Conversion

TCHAR CLexical::ToUpper(void) const
{
	return wtoupper(m_cData);
	}

// End of File
