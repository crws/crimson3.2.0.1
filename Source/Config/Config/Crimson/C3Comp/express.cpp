
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Expression Parser
//

// Constructor

CExprParser::CExprParser(void)
{
	m_Switch = 0;
	}

// Destructor

CExprParser::~CExprParser(void)
{
	}

// Attributes

UINT CExprParser::GetDepth(void) const
{
	return m_uDepth;
	}

UINT CExprParser::GetLocal(void) const
{
	return m_uLocal;
	}

// Result Access

CByteArray & CExprParser::GetSource(void)
{
	return m_Source;
	}

CParseTree & CExprParser::GetTree(void)
{
	return m_Tree;
	}

CLongArray & CExprParser::GetRefList(void)
{
	return m_Refs;
	}

// Operations

BOOL CExprParser::ParseExpr(CTypeDef const &In, CTypeDef &Out)
{
	m_Hint = In.m_Type;

	CLexToken Value;

	if( ParseExpression(Value) ) {
	
		if( !m_Token.IsEndOfText() ) {
		
			Expected(NULL);
			}

		if( m_fActive ) {
			
			if( !(In.m_Flags & flagActive) ) {

				m_pError->Set(IDS_EXPR_SIDE_EFFECT);

				ThrowError(Value);
				}
			}
		else {
			if( In.m_Flags & flagActive ) {

				m_pError->Set(IDS_EXPR_ZERO_EFFECT);

				ThrowError(Value);
				}
			}

		CheckConvert( CString(IDS_EXPR_THIS_EXPR),
			      ReadType(1),
			      In,
			      Value
			      );

		Out = PullType();
			
		return TRUE;
		}
		
	Expected(NULL);

	return FALSE;
	}

// Initialization

void CExprParser::InitParser(void)
{
	m_Tree.Empty();

	m_Source.Empty();

	m_Refs.Empty();

	m_fActive   = FALSE;

	m_fDispatch = FALSE;

	m_uDepth    = 0;

	m_uLocal    = 0;

	m_Hint      = typeVoid;

	m_Lex.SetSource(m_Source);
	}

void CExprParser::ReadParam(CFuncParam const *pParam, UINT uParam)
{
	while( uParam-- ) {

		CString Name = pParam->m_Name;

		INDEX   nPos = m_LocalMap.FindName(Name);

		if( !m_LocalMap.Failed(nPos) ) {

			m_pError->Set(CFormat(IDS_EXPR_PAR_REDEFINE, Name));

			ThrowError();
			}

		UINT Type = pParam->m_Type;

		UINT Slot = m_uLocal++;

		m_LocalMap.Insert(Name, MAKELONG(Slot, Type));

		pParam++;
		}
	}

void CExprParser::ReadUsing(CStringArray const &Using)
{
	m_Using.Append(Using);
	}

// Expression Parser

BOOL CExprParser::ParseExpression(CLexToken &Value)
{
	ClearCodeStack();

	ClearTypeStack();
	
	return ParseSection(FALSE, Value);
	}

BOOL CExprParser::ParseSection(BOOL fArg, CLexToken &Value)
{
	Value.m_Range.m_nFrom = m_Token.m_Range.m_nFrom;

	PushCode(tokenNull);
	
	BOOL fNeed = FALSE;
	
	BOOL fNull = TRUE;
	
	for(;;) {
	
		if( ParseOperandSeq(fNeed) ) {

			Value.m_Range.m_nTo = m_Before.m_Range.m_nTo;

			fNull               = FALSE;

			if( fArg ) {
				
				if( m_Token.m_Code == tokenComma ) {

					break;
					}
				}

			if( ParseBinaryOperator() || ParseTernaryOperator() ) {

				Value.m_Range.m_nTo = m_Before.m_Range.m_nTo;

				fNeed               = TRUE;

				continue;
				}
			}

		break;
		}
	
	for(;;) {

		CLexToken const &Token = PullCode();
		
		if( Token.GetCode() == tokenNull ) {

			break;
			}

		ProcessOperator(Token);
		}

	return !fNull;
	}

void CExprParser::ParseNested(UINT End)
{
	CLexToken Token;

	if( !ParseSection(FALSE, Token) ) {
	
		if( m_Token.m_Code == End ) {

			m_pError->Set(IDS_EXPR_EMPTY_SUBEX);
			
			ThrowError();
			}
			
		Expected(IDS_EXPR_MISS_SUBEX);
		}
	
	MatchCode(End);
	}

BOOL CExprParser::ParseOperandSeq(BOOL fNeed)
{
	for(;;) {
	
		if( ParsePrefixOperator() ) {
		
			fNeed = TRUE;
			
			continue;
			}

		if( m_Token.m_Code == tokenBracketOpen ) {
		
			GetToken();
			
			if( m_Token.IsCastOperator() ) {
			
				ParseCastOperator();
				
				fNeed = TRUE;
				
				continue;
				}
				
			ParseNested(tokenBracketClose);
				
			ParsePostfixSeq();
			
			return TRUE;
			}
		
		break;
		}
		
	if( !ParseOperand() ) {
	
		if( fNeed ) {

			Expected(IDS_EXPR_MISS_OPERAND);
			}
			
		return FALSE;
		}

	ParsePostfixSeq();
		
	return TRUE;
	}

BOOL CExprParser::ParseOperand(void)
{
	if( m_Token.m_Code == tokenTrue ) {

		ProcessTrue();

		GetToken();

		return TRUE;
		}

	if( m_Token.m_Code == tokenFalse ) {

		ProcessFalse();

		GetToken();

		return TRUE;
		}

	if( m_Token.m_Code == tokenRun ) {

		GetToken();

		ParseRun();

		return TRUE;
		}

	if( m_Token.m_Group == groupConstant ) {
	
		ProcessConstant();
		
		GetToken();

		return TRUE;
		}
		
	if( m_Token.m_Code == tokenIndexOpen ) {
	
		m_Lex.AcceptPlcRef();
		
		GetToken();
		
		ParseDirectRef();
		
		return TRUE;
		}

	if( m_Token.m_Group == groupIdentifier ) {
	
		ParseIdentifier();
		
		return TRUE;
		}

	if( m_Token.IsCastOperator() ) {
	
		ParseCastFunction();
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CExprParser::ParseBinaryOperator(void)
{
	if( IsBinaryOperator() ) {

		if( m_Token.m_Code == tokenBitSelect ) {

			CLexToken const &Code = ReadCode(1);

			if( Code.m_Code == tokenIndexOpen ) {

				if( Code.m_Const.IsType(typeInteger) ) {

					CLexToken Save = m_Token;

					WORD      ID   = WORD(Code.m_Const.GetIntegerValue());

					UINT      uPos = m_Source.GetCount();

					PurgeCode(Save);

					GetToken();

					if( m_Token.m_Group == groupIdentifier ) {

						CString  Prop = m_Token.m_Const.GetStringValue();

						CTypeDef Type;

						WORD     PropID;

						if( m_pName->FindProp(m_pError, Prop, ID, PropID, Type) ) {

							BYTE Src[5] = {	srcProp2,
									LOBYTE(ID),
									LOBYTE(ID),
									HIBYTE(PropID),
									HIBYTE(PropID)
									};
								
							CString Prop;

							m_pName->NameProp(ID, PropID, Prop);

							InsertSource(uPos, Prop, 1);

							InsertSource(uPos, Src,  elements(Src));

							PullType();

							ProcessProp(PropID, Type);

							GetToken();

							return ParseBinaryOperator();
							}
						}

					PushCode(Save);

					return TRUE;
					}
				}
			}
		
		PurgeAndPush(m_Token);
		
		GetToken();
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CExprParser::ParseTernaryOperator(void)
{
	if( m_Token.m_Group == groupOperator ) {
		
		if( m_Token.m_Code == tokenQuestion ) {

			PurgeAndPush(m_Token);

			GetToken();

			CLexToken Token;

			ParseSection(FALSE, Token);

			MatchCode(tokenColon);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CExprParser::ParsePrefixOperator(void)
{
	if( IsPrefixOperator() ) {
	
		PurgeAndPush(m_Token);
		
		GetToken();
		
		return TRUE;
		}
		
	return FALSE;
	}

void CExprParser::ParseCastOperator(void)
{
	PurgeAndPush(m_Token);

	GetToken();
	
	MatchCode(tokenBracketClose);
	}
	
void CExprParser::ParseCastFunction(void)
{
	CLexToken Token = m_Token;
	
	GetToken();
	
	MatchCode(tokenBracketOpen);
	
	ParseNested(tokenBracketClose);
	
	ProcessCast(Token);
	}
	
void CExprParser::ParsePostfixSeq(void)
{
	for(;;) {
	
		if( ParsePostfixOperator() ) {

			continue;
			}
			
		if( ParseIndexOperator() ) {

			continue;
			}
		
		break;
		}
	}
	
BOOL CExprParser::ParsePostfixOperator(void)
{
	if( IsPostfixOperator() ) {
	
		PurgeAndPush(m_Token);
		
		GetToken();
		
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CExprParser::ParseIndexOperator(void)
{
	if( m_Token.m_Code == tokenIndexOpen ) {

		CParseNode const &Node = m_Tree[m_Tree.GetCount()-1];

		m_Token.m_Const = Node.m_Const;

		PurgeAndPush(m_Token);
		
		GetToken();

		ParseNested(tokenIndexClose);

		return TRUE;
		}

	return FALSE;
	}

void CExprParser::PurgeCode(CLexToken const &ThisToken)
{
	UINT ThisLevel = ThisToken.GetPriority();
	
	for(;;) {
	
		CLexToken const & ThatToken = ReadCode(1);

		if( ThatToken.GetCode() ) {
		
			UINT ThatLevel = ThatToken.GetPriority();
			
			if( ThatLevel > ThisLevel ) {

				break;
				}

			if( ThatLevel == ThisLevel ) { 
			
				if( IsRightToLeft(ThisLevel) ) {

					break;
					}
				}
				
			ProcessOperator(ThatToken);
			
			PullCode();
			}
		else
			break;
		}
	}

void CExprParser::PurgeAndPush(CLexToken const &ThisToken)
{
	PurgeCode(ThisToken);
	
	PushCode (ThisToken);
	}

void CExprParser::ParseIdentifier(void)
{
	CString   Name = m_Token.m_Const.GetStringValue();

	CLexToken Full = m_Token;

	CLexToken Last;

	CLexToken BSel;

	UINT      uPos = m_Source.GetCount();

	BOOL      fBit = FALSE;

	BOOL      fDot = FALSE;

	GetToken();

	while( m_Token.IsBitSelect() ) {

		BSel = m_Token;

		GetToken();

		if( m_Token.m_Group == groupIdentifier ) {

			m_Source.Remove(uPos, 1);

			Name += L'.';

			Name += m_Token.m_Const.GetStringValue();

			Last  = m_Token;

			Full.m_Range.m_nTo = Last.m_Range.m_nTo;

			GetToken();

			fDot = TRUE;

			continue;
			}

		fBit = TRUE;

		break;
		}

	if( !fBit ) {

		if( m_Token.m_Code == tokenBracketOpen ) {
		
			GetToken();

			ParseFunction(Full, uPos, Name, typeVoid);

			return;
			}
		}

	if( fDot ) {

		if( !ParseVariable(uPos, Name, NULL, FALSE) ) {

			UINT    uDot = Name.FindRev(L'.');

			CString Root = Name.Left(uDot);

			CString Prop = Name.Mid (uDot+1);

			WORD    ID1  = 0;

			if( ParseVariable(0, Root, &ID1, FALSE) ) {

				CTypeDef Type;

				WORD     PropID;

				if( m_pName->FindProp(m_pError, Prop, ID1, PropID, Type) ) {

					BYTE Src[5] = {	srcProp1,
							LOBYTE(ID1),
							HIBYTE(ID1),
							LOBYTE(PropID),
							HIBYTE(PropID)
							};
						
					CString Prop;

					m_pName->NameProp(ID1, PropID, Prop);

					CString Full = Root + L"." + Prop;

					InsertSource(uPos, Full, 1);

					InsertSource(uPos, Src,  elements(Src));

					PullType();

					ProcessProp(PropID, Type);
					}
				else {
					if( ReadType(1).m_Type == typeFolder ) {

						if( !ParseVariable(uPos, Name, NULL, TRUE) ) {

							m_Token = Full;

							m_pError->Set(CFormat(IDS_EXPR_UNKNOWN_VAR, Name));

							ThrowError();
							}
						}
					else {
						PurgeCode(BSel);

						WORD ID2 = 0;

						if( ParseVariable(0, Prop, &ID2, TRUE) ) {

							BYTE Src1[3] = { srcIdent,
									 LOBYTE(ID1),
									 HIBYTE(ID1)
									 };

							BYTE Src2[3] = { srcIdent,
									 LOBYTE(ID2),
									 HIBYTE(ID2)
									 };

							InsertSource(uPos, Prop, 1);

							InsertSource(uPos, Src2, elements(Src2));

							m_Source.Insert(uPos, tokenBitSelect | 0x80);

							InsertSource(uPos, Root, 1);

							InsertSource(uPos, Src1, elements(Src1));

							PushCode(BSel);

							return;
							}

						m_Token = Last;

						m_pError->Set(CFormat(IDS_EXPR_UNKNOWN_VAR, Name));

						ThrowError();
						}
					}
				}
			else {
				if( !ParseVariable(uPos, Name, NULL, TRUE) ) {

					m_Token = Full;

					m_pError->Set(CFormat(IDS_EXPR_UNKNOWN_VAR, Name));

					ThrowError();
					}
				}
			}
		}
	else {		     
		if( !ParseLocal(uPos, Name) ) {

			if( !ParseVariable(uPos, Name, NULL, TRUE) ) {

				m_Token = Full;

				m_pError->Set(CFormat(IDS_EXPR_UNKNOWN_VAR, Name));

				ThrowError();
				}
			}
		}

	if( fBit ) {

		PurgeAndPush(BSel);

		ParseOperandSeq(TRUE);
		}
	}

BOOL CExprParser::ParseLocal(UINT uPos, PCTXT pName)
{
	INDEX nPos = m_LocalMap.FindName(pName);

	if( !m_LocalMap.Failed(nPos) ) {

		DWORD ID   = m_LocalMap.GetData(nPos);

		UINT  Slot = LOWORD(ID);

		UINT  Type = HIWORD(ID);

		ProcessLocal(Slot, Type);

		CString const &Name = m_LocalMap.GetName(nPos);

		InsertSource(uPos, Name, 0);

		return TRUE;
		}

	return FALSE;
	}

BOOL CExprParser::ParseVariable(UINT uPos, CString &Name, WORD *pID, BOOL fMake)
{
	UINT uCount = m_Using.GetCount();

	for( UINT uScan = 0; uScan <= uCount; uScan++ ) {

		BOOL     fEnd   = (uScan == uCount);

		CString  Find   = fEnd ? Name : (m_Using[uScan] + L'.' + Name);

		CTypeDef Type   = { m_Hint, 0 };

		CError   NoUI   = CError(FALSE);

		CError * pError = fMake ? m_pError : &NoUI;

		WORD     ID     = 0;

		if( !pID && fMake ) {

			if( m_Token.IsCode(tokenIndexOpen) ) {

				Type.m_Flags |= flagArray;
				}
			}

		if( m_pName->FindIdent(pError, Find, ID, Type) ) {

			if( m_pName->SaveClass(Type.m_Type) ) {

				if( m_Refs.Failed(m_Refs.Find(ID)) ) {

					if( m_fDispatch ) {

						if( ID >= 0xFF80 ) {

							m_pError->Set(CString(IDS_INVALID_REFERENCE));

							ThrowError(m_Before);
							}
						}

					m_Refs.Append(ID);
					}
				}

			m_pName->NameIdent(ID, Name);

			if( uScan < uCount ) {

				UINT uLen = m_Using[uScan].GetLength();

				Name.Delete(0, uLen);

				Name.Delete(0, 1);
				}

			if( pID ) {

				*pID = ID;
				}
			else {
				BYTE Src[3] = {	srcIdent,
						LOBYTE(ID),
						HIBYTE(ID)
						};

				InsertSource(uPos, Name, 1);

				InsertSource(uPos, Src,  elements(Src));
				}

			ProcessVariable(ID, Type);

			return TRUE;
			}
		}

	return FALSE;
	}

void CExprParser::ParseRun(void)
{
	MatchCode(tokenBracketOpen);

	if( m_Token.m_Group == groupIdentifier ) {

		CString  Name = m_Token.m_Const.GetStringValue();

		UINT     uPos = m_Source.GetCount();

		CTypeDef Type = { typeVoid, 0 };

		WORD	 ID;

		UINT	 uCount;

		if( m_pName->FindFunction(m_pError, Name, ID, Type, uCount) ) {

			if( uCount ) {

				m_pError->Set(IDS_EXPR_RUN_ARG);

				ThrowError();
				}

			if( Type.m_Type ) {

				m_pError->Set(IDS_EXPR_RUN_RETURN);

				ThrowError();
				}

			ProcessFunction(ID, Type, uCount);

			if( Type.m_Flags & flagActive ) {

				m_fActive = TRUE;
				}

			if( ID & 0x8000 ) {

				WORD id = WORD(ID & 0x7FFF);

				if( m_Refs.Failed(m_Refs.Find(id)) ) {

					m_Refs.Append(id);
					}
				}

			BYTE Src[3] = {	srcFunction,
					LOBYTE(ID),
					HIBYTE(ID)
					};

			CString Name;

			m_pName->NameFunction(ID, Name);

			InsertSource(uPos, Name, 1);

			InsertSource(uPos, Src,  elements(Src));

			GetToken();

			MatchCode(tokenBracketClose);

			return;
			}

		m_pError->Set(CFormat(IDS_EXPR_UNKNOWN_FUNC, Name));

		ThrowError();
		}

	Expected(IDS_PROGRAM_NAME);
	}

void CExprParser::ParseFunction(CLexToken const &Save, UINT uPos, PCTXT pName, UINT Type)
{
	if( m_pName ) {
		
		CTypeDef FuncType = { Type, 0 };

		WORD	 ID;

		UINT	 uCount;

		if( m_pName->FindFunction(m_pError, pName, ID, FuncType, uCount) ) {
			
			CString  Name;

			CString  ArgName;
		
			CTypeDef ArgType;

			m_pName->NameFunction(ID, Name);

			UINT uParam = 1;

			UINT uIndex = Type ? 1 : 0;

			for(;;) {

				if( uIndex >= uCount ) {

					if( m_Token.m_Code == tokenComma ) {

						m_pError->Set(IDS_EXPR_ARGS_MORE);

						ThrowError();
						}

					MatchCode(tokenBracketClose);

					break;
					}
				else {
					CLexToken Token;

					m_pName->GetFuncParam(ID, uIndex, ArgName, ArgType);

					m_Hint = ArgType.m_Type;

					if( !ParseSection(TRUE, Token) ) {

						m_pError->Set(IDS_EXPR_ARGS_LESS);

						ThrowError();
						}

					m_Hint = typeVoid;

					if( uIndex == 0 ) {

						CTypeDef Type = ReadType(1);

						if( m_pName->EditFunction(ID, Type) ) {

							FuncType = Type;

							m_pName->GetFuncParam(ID, uIndex, ArgName, ArgType);
							}
						}

					if( uIndex < uCount - 1 ) {

						if( m_Token.m_Code == tokenBracketClose ) {

							m_pError->Set(IDS_EXPR_ARGS_LESS);

							ThrowError();
							}

						MatchCode(tokenComma);
						}

					ProcessParam(Name, ArgName, ArgType, Token);

					uIndex++;

					uParam++;
					}
				}

			ProcessFunction(ID, FuncType, uCount);

			if( FuncType.m_Flags & flagActive ) {

				m_fActive = TRUE;
				}

			if( ID & 0x8000 ) {

				WORD id = WORD(ID & 0x7FFF);

				if( m_Refs.Failed(m_Refs.Find(id)) ) {

					m_Refs.Append(id);
					}
				}

			BYTE Src[3] = {	srcFunction,
					LOBYTE(ID),
					HIBYTE(ID)
					};

			InsertSource(uPos, Name, 1);

			InsertSource(uPos, Src,  elements(Src));

			return;
			}
		}

	m_Token = Save;

	m_pError->Set(CFormat(IDS_EXPR_UNKNOWN_FUNC, pName));

	ThrowError();
	}

void CExprParser::ParseDirectRef(void)
{
	if( m_Token.m_Group == groupIdentifier ) {
	
		CString Name = m_Token.m_Const.GetStringValue();

		if( m_pName ) {

			CTypeDef Type;

			DWORD	 ID;

			if( m_pName->FindDirect(m_pError, Name, ID, Type) ) {

				if( m_Refs.Failed(m_Refs.Find(ID)) ) {

					m_Refs.Append(ID);
					}

				m_Source.Append(srcDirect);

				m_Source.Append(PCBYTE(&ID), sizeof(ID));

				m_pName->NameDirect(ID, Name);

				AppendSource(Name, 1);

				GetToken();
					
				MatchCode(tokenIndexClose);

				Type.m_Flags |= flagCommsRef;

				ProcessDirectRef(ID, Type);

				return;
				}
			}

		if( m_pError->IsOkay() ) {
			
			m_pError->Set(CFormat(IDS_EXPR_UNKNOWN_REF, Name));
			}

		ThrowError();
		}			
		
	Expected(IDS_EXPR_MISS_PLCREF);
	}

// Output Processing

void CExprParser::ProcessTrue(void)
{
	C3INT Value = 1;

	PushType(typeLogical, flagConstant);

	ProcessNode(CParseNode(0, CLexToken(CLexConst(Value)), typeInteger));
	}

void CExprParser::ProcessFalse(void)
{
	C3INT Value = 0;

	PushType(typeLogical, flagConstant);

	ProcessNode(CParseNode(0, CLexToken(CLexConst(Value)), typeInteger));
	}

void CExprParser::ProcessConstant(void)
{
	UINT Type = m_Token.m_Const.GetType();

	PushType(Type, flagConstant);

	ProcessNode(CParseNode(0, m_Token, Type));
	}

void CExprParser::ProcessLocal(UINT Slot, UINT Type)
{
	PushType(Type, flagWritable | flagLocal);

	CLexToken Token;

	Token.m_Group = groupIdentifier;

	Token.m_Code  = usageLocal;

	Token.m_Const = C3INT(Slot);

	ProcessNode(CParseNode(0, Token, Type));
	}

void CExprParser::ProcessVariable(WORD ID, CTypeDef const &Type)
{
	PushType(Type);

	CLexToken Token;

	Token.m_Group = groupIdentifier;

	Token.m_Code  = usageVariable;

	Token.m_Const = C3INT(ID);

	ProcessNode(CParseNode(0, Token, Type.m_Type));
	}

void CExprParser::ProcessProp(WORD PropID, CTypeDef const &Type)
{
	PushType(Type);

	CLexToken Token;

	Token.m_Group = groupIdentifier;

	Token.m_Code  = usageProperty;

	Token.m_Const = C3INT(PropID);

	ProcessNode(CParseNode(0, Token, typeInteger));

	Token.m_Group = groupOperator;

	Token.m_Code  = tokenGetProperty;

	Token.m_Const.Empty();

	ProcessNode(CParseNode(2, Token, Type.m_Type));
	}

void CExprParser::ProcessFunction(WORD ID, CTypeDef const &Type, UINT uCount)
{
	CLexToken Token;

	Token.m_Group = groupIdentifier;

	Token.m_Code  = usageFunction;

	Token.m_Const = C3INT(ID) | ((Type.m_Flags & flagActive) ? 0x10000 : 0x00000);

	PushType(Type);

	MakeMax(m_uDepth, m_uTypePtr + uCount);

	ProcessNode(CParseNode(uCount, Token, Type.m_Type));
	}

void CExprParser::ProcessParam(PCTXT pFunc, PCTXT pName, CTypeDef const &Type, CLexToken const &Token)
{
	if( Type.m_Flags & (flagWritable | flagElement) ) {

		ProcessNode( CParseNode( 1,
					 tokenArgument,
					 typeLValue
					 ));
		}
	else {
		ProcessNode( CParseNode( 1,
					 tokenArgument,
					 Type.m_Type
					 ));
		}

	CheckConvert( CFormat(IDS_EXPR_FUNC_ARG, pName, pFunc),
		      PullType(),
		      Type,
		      Token
		      );
	}

void CExprParser::ProcessDirectRef(DWORD ID, CTypeDef const &Type)
{
	CLexToken Token;

	Token.m_Group = groupIdentifier;

	Token.m_Code  = usageDirectRef;

	Token.m_Const = C3INT(ID);

	PushType(Type);

	ProcessNode(CParseNode(0, Token, Type.m_Type));
	}

void CExprParser::ProcessOperator(CLexToken const &Token)
{
	UINT Code = Token.GetCode();

	if( Code == tokenQuestion ) {

		CheckScalar(1, Token, IDS_EXPR_OPERAND_3, TRUE);

		CheckScalar(2, Token, IDS_EXPR_OPERAND_2, TRUE);

		CheckScalar(3, Token, IDS_EXPR_OPERAND_1, FALSE);

		ProcessTernary(Token);

		return;
		}

	if( Code == tokenIndexOpen ) {

		ProcessIndex(Token);

		return;
		}

	if( Token.IsCastOperator() ) {

		ProcessCast(Token);

		return;
		}

	if( Token.IsBinaryOperator() ) {

		if( Code == tokenComma ) {

			ProcessSequence(Token);

			return;
			}
		else {
			CheckScalar(1, Token, IDS_EXPR_OPERAND_RHS, FALSE);

			CheckScalar(2, Token, IDS_EXPR_OPERAND_LHS, FALSE);

			if( Code == tokenBitSelect ) {

				ProcessBitSelect(Token);

				return;
				}

			if( Code == tokenCondition ) {

				ProcessCondition(Token);

				return;
				}

			if( Token.IsAssignOperator() ) {

				ProcessAssign(Token);

				return;
				}

			ProcessBinary(Token);
			}
		}
	else {
		CheckScalar(1, Token, IDS_EXPR_OPERAND, FALSE);

		if( Token.IsIncDecOperator() ) {

			ProcessIncDec(Token);

			return;
			}

		ProcessUnary(Token);
		}
	}

void CExprParser::ProcessAssign(CLexToken const &Token)
{
	CTypeDef const & TypeR = PullType();

	CTypeDef const & TypeL = PullType();

	CheckWriteable(TypeL, Token, IDS_EXPR_OPERAND_LHS);

	if( Token.IsComplexAssign() ) {

		if( TypeL.m_Flags & flagBitRef ) {

			if( TypeL.m_Flags & (flagExtended | flagLocal) ) {

				NoComplexAssign(Token);
				}
			}
		}

	switch( TypeL.m_Type ) {

		case typeInteger:
		case typeLogical:

			if( m_Switch & optAutoCast ) {

				CheckNumeric(TypeR, Token);
				}
			else
				CheckInteger(TypeR, Token);

			break;

		case typeReal:

			if( !Token.IsFloatAssign() ) {

				NotWithReal(Token);
				}

			CheckNumeric(TypeR, Token);

			break;

		case typeString:

			if( Token.GetCode() == tokenAddAssign ) {

				if( TypeR.m_Type == typeInteger ) {

					CLexToken Other = Token;

					Other.SetCode(tokenAndAssign);

					UINT Type  = TypeL.m_Type;

					UINT Flags = 0;

					PushType(Type, Flags);

					ProcessNode(CParseNode(2, Other, Type));

					m_fActive = TRUE;

					return;
					}

				CheckString(TypeR, Token);
				}
			else {
				if( !Token.IsStringAssign() ) {

					NotWithString(Token);
					}

				CheckString(TypeR, Token);
				}

			break;
		}

	UINT Type  = TypeL.m_Type;

	UINT Keep =  Token.IsComplexAssign() ? 0 : flagConstant;

	UINT Flags = TypeR.m_Flags & Keep;

	PushType(Type, Flags);

	ProcessNode(CParseNode(2, Token, Type));

	m_fActive = TRUE;
	}

void CExprParser::ProcessBinary(CLexToken const &Token)
{
	CTypeDef const & TypeR = PullType();

	CTypeDef const & TypeL = PullType();

	UINT             Type1 = GetResultType(TypeL.m_Type, TypeR.m_Type);

	UINT		 Type2 = Token.IsLogicalOperator() ? typeLogical : Type1;

	UINT		 Flags = TypeR.m_Flags & TypeL.m_Flags & flagConstant;

	switch( TypeL.m_Type ) {

		case typeInteger:
		case typeLogical:
		case typeReal:

			CheckNumeric(TypeR, Token);

			break;

		case typeString:

			CheckString(TypeR, Token);

			break;
		}

	switch( Type1 ) {

		case typeReal:
			
			if( !Token.IsFloatBinary() ) {

				NotWithReal(Token);
				}
			break;

		case typeString:
			
			if( !Token.IsStringBinary() ) {

				NotWithString(Token);
				}
			break;
		}

	PushType(Type2, Flags);

	ProcessNode(CParseNode(2, Token, Type1));
	}

void CExprParser::ProcessTernary(CLexToken const &Token)
{
	CTypeDef const & TypeR = PullType();

	CTypeDef const & TypeL = PullType();

	CTypeDef const & TypeX = PullType();

	CheckNumeric(TypeX, Token, IDS_EXPR_OPERAND_1);

	switch( TypeL.m_Type ) {

		case typeReal:
		case typeInteger:
		case typeLogical:

			CheckNumeric(TypeR, Token);

			break;

		case typeString:

			CheckString(TypeR, Token);

			break;

		case typeVoid:

			CheckVoid(TypeR, Token);

			break;
		}

	UINT Type  = GetResultType(TypeL.m_Type, TypeR.m_Type);

	UINT Flags = TypeX.m_Flags & TypeR.m_Flags & TypeL.m_Flags & flagConstant;

	PushType(Type, Flags);

	ProcessNode(CParseNode(3, Token, Type));
	}

void CExprParser::ProcessUnary(CLexToken const &Token)
{
	CTypeDef const & TypeA = PullType();

	switch( TypeA.m_Type ) {

		case typeReal:

			if( Token.GetCode() == tokenBitwiseNot ) {

				NotWithReal(Token);
				}
			break;

		case typeString:

			NotWithString(Token);

			break;
		}

	UINT Type1 = TypeA.m_Type;

	UINT Type2 = Token.IsLogicalOperator() ? typeLogical : Type1;

	UINT Flags = TypeA.m_Flags & flagConstant;

	PushType(Type2, Flags);

	ProcessNode(CParseNode(1, Token, Type1));
	}

void CExprParser::ProcessIncDec(CLexToken const &Token)
{
	CTypeDef const &TypeA = PullType();

	CheckWriteable(TypeA, Token, IDS_EXPR_OPERAND);

	if( TypeA.m_Flags & flagBitRef ) {

		NotWithBitRef(Token);
		}

	switch( TypeA.m_Type ) {
		
		case typeString:

			NotWithString(Token);

			break;
		}

	UINT Type  = TypeA.m_Type;

	UINT Flags = 0;

	PushType(Type, Flags);

	ProcessNode(CParseNode(1, Token, Type));

	m_fActive = TRUE;
	}

void CExprParser::ProcessCast(CLexToken const &Token)
{
	CTypeDef const & TypeA = PullType();

	UINT Type  = TypeA.m_Type;

	UINT Flags = 0;

	switch( TypeA.m_Type ) {

		case typeInteger:
		case typeLogical:

			if( Token.GetCode() == tokenFloat ) {

				Type = typeReal;

				ProcessNode(CParseNode(1, Token, Type));
				}

			if( Token.GetCode() == tokenString ) {

				NotGoodCast(Token);
				}
			break;

		case typeReal:

			if( Token.GetCode() == tokenInt ) {

				Type = typeInteger;

				ProcessNode(CParseNode(1, Token, Type));
				}

			if( Token.GetCode() == tokenString ) {

				NotGoodCast(Token);
				}
			break;

		default:

			NotGoodCast(Token);

			break;
		}

	PushType(Type, Flags);
	}

void CExprParser::ProcessBitSelect(CLexToken const &Token)
{
	CTypeDef const & TypeR = PullType();

	CTypeDef const & TypeL = PullType();

	if( TypeL.m_Flags & flagBitRef ) {

		NotWithBitRef(Token);
		}

	if( TypeL.m_Type == typeString ) {

		NotWithString(Token);
		}
		
	if( TypeR.m_Type == typeString ) {

		NotWithString(Token);
		}

	UINT Type  = typeLogical;

	UINT Keep  = flagWritable | flagExtended | flagLocal;

	UINT Flags = TypeL.m_Flags & Keep | flagBitRef;

	PushType(Type, Flags);

	if( Flags & (flagLocal | flagExtended) ) {

		CLexToken Other = Token;

		Other.SetCode(tokenBitSelectLong);

		ProcessNode(CParseNode(2, Other, Type));
		}
	else
		ProcessNode(CParseNode(2, Token, Type));
	}

void CExprParser::ProcessSequence(CLexToken const &Token)
{
	CTypeDef const & TypeR = PullType();

	CTypeDef const & TypeL = PullType();

	AfxTouch(TypeL);

	UINT Type  = TypeR.m_Type;

	UINT Flags = TypeR.m_Flags & flagConstant;

	PushType(Type, Flags);

	ProcessNode(CParseNode(2, Token, 0));
	}

void CExprParser::ProcessCondition(CLexToken const &Token)
{
	CTypeDef const & TypeR = PullType();

	CTypeDef const & TypeL = PullType();

	CheckNumeric(TypeL, Token, IDS_EXPR_OPERAND_1);

	AfxTouch(TypeR);

	PushType(typeLogical, 0);

	ProcessNode(CParseNode(2, Token, typeInteger));
	}

void CExprParser::ProcessIndex(CLexToken const &Token)
{
	CTypeDef const & TypeR = PullType();

	CTypeDef const & TypeL = PullType();

	if( !(TypeL.m_Flags & flagArray) ) {

		if( TypeL.m_Type == typeString ) {

			CheckScalar (TypeL, Token, IDS_EXPR_OPERAND_LHS, FALSE);

			CheckScalar (TypeR, Token, IDS_EXPR_OPERAND_RHS, FALSE);

			CheckNumeric(TypeR, Token, IDS_EXPR_OPERAND_RHS);

			CLexToken Other = Token;

			UINT Type  = typeInteger;

			UINT Flags = 0;

			Other.SetCode(tokenIndexString);

			PushType(Type, Flags);

			ProcessNode(CParseNode(2, Other, Type));
			}
		else {
			m_Token = Token;

			m_pError->Set(IDS_EXPR_BAD_INDEX);

			ThrowError();
			}
		}
	else {
		CheckScalar (TypeR, Token, IDS_EXPR_OPERAND_RHS, FALSE);

		CheckNumeric(TypeR, Token, IDS_EXPR_OPERAND_RHS);

		UINT Type  = TypeL.m_Type;

		UINT Keep  = flagWritable | flagExtended | flagTagRef;

		UINT Flags = TypeL.m_Flags & Keep | flagElement;

		PushType(Type, Flags);

		if( Flags & flagExtended ) {

			CLexToken Other = Token;

			Other.SetCode(tokenIndexExtended);

			ProcessNode(CParseNode(2, Other, Type));
			}
		else {
			CLexToken Other = Token;

			Other.SetGroup(groupOperator);

			ProcessNode(CParseNode(2, Token, Type));
			}
		}
	}

void CExprParser::ProcessNode(CParseNode const &Node)
{
	if( Node.m_uOrder > 1 ) {

		m_Tree.Append(Node);

		if( Node.m_Group == groupIdentifier ) {
			
			if( Node.m_Code == usageFunction ) {

				return;
				}
			}

		ReverseNode();

		return;
		}

	m_Tree.Append(Node);
	}

// Node Flipping

void CExprParser::ReverseNode(void)
{
	UINT uPos   = m_Tree.GetCount() - 1;

	UINT uOrder = m_Tree[uPos--].m_uOrder;;

	if( uOrder > 1 ) {

		CParseTree Work;

		for( UINT n = 0; n < uOrder; n++ ) {

			UINT uCount = GetNodeDepth(uPos);

			uPos -= uCount;

			CParseNode const *pData = m_Tree.GetPointer();

			Work.Append(pData + uPos + 1, uCount);
			}

		m_Tree.SetAt(uPos + 1, Work);
		}
	}

UINT CExprParser::GetNodeDepth(UINT uPos)
{
	return GetDepthFrom(uPos);
	}

UINT CExprParser::GetDepthFrom(UINT &uPos)
{
	UINT uDepth = 1;

	CParseNode const &Node = m_Tree[uPos--];

	for( UINT n = 0; n < Node.m_uOrder; n++ ) {

		uDepth += GetDepthFrom(uPos);
		}

	return uDepth;
	}

// Code Stack

void CExprParser::ClearCodeStack(void)
{
	m_uCodePtr = 0;
	}

void CExprParser::CheckCodeStack(void)
{
	if( m_uCodePtr == elements(m_CodeStack) ) {

		m_pError->Set(IDS_COMP_TOO_COMPLEX);
		
		ThrowError();
		}
	}

void CExprParser::PushCode(CLexToken const &Token)
{
	CheckCodeStack();

	m_CodeStack[m_uCodePtr] = Token;

	m_uCodePtr++;
	}

CLexToken const & CExprParser::ReadCode(UINT uBack) const
{
	AfxAssert(m_uCodePtr >= uBack);

	return m_CodeStack[m_uCodePtr - uBack];
	}

CLexToken const & CExprParser::PullCode(void)
{
	AfxAssert(m_uCodePtr);

	return m_CodeStack[--m_uCodePtr];
	}

// Type Stack

void CExprParser::ClearTypeStack(void)
{
	m_uTypePtr = 0;
	}

void CExprParser::CheckTypeStack(void)
{
	if( m_uTypePtr == elements(m_TypeStack) ) {

		m_pError->Set(IDS_COMP_TOO_COMPLEX);
		
		ThrowError();
		}
	}

void CExprParser::PushType(CTypeDef const &Type)
{
	CheckTypeStack();

	m_TypeStack[m_uTypePtr] = Type;

	m_uTypePtr++;

	MakeMax(m_uDepth, m_uTypePtr);
	}

void CExprParser::PushType(UINT Type, UINT Flags)
{
	CheckTypeStack();

	m_TypeStack[m_uTypePtr].m_Type  = Type;

	m_TypeStack[m_uTypePtr].m_Flags = Flags;

	m_uTypePtr++;

	MakeMax(m_uDepth, m_uTypePtr);
	}

CTypeDef const & CExprParser::ReadType(UINT uBack)
{
	AfxAssert(m_uTypePtr);

	return m_TypeStack[m_uTypePtr - uBack];
	}

CTypeDef const & CExprParser::PullType(void)
{
	AfxAssert(m_uTypePtr);

	return m_TypeStack[--m_uTypePtr];
	}

// Type Combination

UINT CExprParser::GetBasicType(UINT Type)
{
	return (Type == typeLogical) ? typeInteger : Type;
	}

UINT CExprParser::GetResultType(UINT Type1, UINT Type2)
{
	Type1 = GetBasicType(Type1);

	Type2 = GetBasicType(Type2);

	return max(Type1, Type2);
	}

// Token Characterisation

BOOL CExprParser::IsPrefixOperator(void)
{
	if( m_Token.m_Group == groupOperator ) {

		m_Token.AdjustPrefix();
		
		return m_Token.IsUnaryOperator();
		}
		
	return FALSE;
	}

BOOL CExprParser::IsPostfixOperator(void)
{
	if( m_Token.m_Group == groupOperator ) {

		m_Token.AdjustPostfix();

		return m_Token.IsPostfixOperator();
		}
		
	return FALSE;
	}

BOOL CExprParser::IsBinaryOperator(void)
{
	if( m_Token.m_Group == groupOperator ) {

		m_Token.AdjustBinary();

		return m_Token.IsBinaryOperator();
		}
		
	return FALSE;
	}

BOOL CExprParser::IsRightToLeft(UINT Level)
{
	switch( Level ) {

		case 2:
		case 3:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
			return TRUE;
		}
		
	return FALSE;
	}

// Error Checking

BOOL CExprParser::CheckScalar(UINT n, CLexToken const &Token, ENTITY Side, BOOL fVoid)
{
	return CheckScalar(ReadType(n), Token, Side, fVoid);
	}

BOOL CExprParser::CheckScalar(CTypeDef const &Type, CLexToken const &Token, ENTITY Side, BOOL fVoid)
{
	if( Type.m_Type == typeVoid ) {

		if( !fVoid ) {

			CFormat Error( IDS_EXPR_ARG_VOID,
				       CString(Side),
				       Token.Describe()
				       );

			m_pError->Set(Error);

			ThrowError(Token);
			}
		}

	if( Type.m_Type >= typeObject ) {

		CFormat Error( IDS_EXPR_ARG_OBJECT,
			       CString(Side),
			       Token.Describe(),
			       GetObjectName(Type.m_Type)
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	if( (Type.m_Flags & flagArray) ) {

		CFormat Error(	IDS_EXPR_ARG_ARRAY,
				CString(Side),
				Token.Describe()
				);

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

BOOL CExprParser::CheckWriteable(CTypeDef const &Type, CLexToken const &Token, ENTITY Side)
{
	if( (Type.m_Flags & flagWritable) == 0 ) {

		CFormat Error( IDS_EXPR_ARG_RVALUE,
			       CString(Side),
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

BOOL CExprParser::CheckNumeric(CTypeDef const &Type, CLexToken const &Token, ENTITY Side)
{
	if( Type.m_Type != typeLogical && Type.m_Type != typeInteger && Type.m_Type != typeReal ) {

		CFormat Error( IDS_EXPR_ARG_NUMERIC,
			       CString(Side),
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

BOOL CExprParser::CheckInteger(CTypeDef const &Type, CLexToken const &Token, ENTITY Side)
{
	if( Type.m_Type != typeLogical && Type.m_Type != typeInteger ) {

		CFormat Error( IDS_EXPR_ARG_NUMERIC,
			       CString(Side),
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

BOOL CExprParser::CheckString(CTypeDef const &Type, CLexToken const &Token, ENTITY Side)
{
	if( Type.m_Type != typeString ) {

		CFormat Error( IDS_EXPR_ARG_STRING,
			       CString(Side),
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}


BOOL CExprParser::CheckNumeric(CTypeDef const &Type, CLexToken const &Token)
{
	if( Type.m_Type != typeLogical && Type.m_Type != typeInteger && Type.m_Type != typeReal ) {

		CFormat Error( IDS_EXPR_ARG_MISMATCH,
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

BOOL CExprParser::CheckInteger(CTypeDef const &Type, CLexToken const &Token)
{
	if( Type.m_Type != typeLogical && Type.m_Type != typeInteger ) {

		CFormat Error( IDS_EXPR_ARG_MISMATCH,
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

BOOL CExprParser::CheckString(CTypeDef const &Type, CLexToken const &Token)
{
	if( Type.m_Type != typeString ) {

		CFormat Error( IDS_EXPR_ARG_MISMATCH,
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

BOOL CExprParser::CheckVoid(CTypeDef const &Type, CLexToken const &Token)
{
	if( Type.m_Type != typeVoid ) {

		CFormat Error( IDS_EXPR_ARG_MISMATCH,
			       Token.Describe()
			       );

		m_pError->Set(Error);

		ThrowError(Token);
		}

	return TRUE;
	}

// Conversion Check

void CExprParser::CheckConvert(PCTXT pWhat, CTypeDef const &From, CTypeDef const &Type, CLexToken const &Token)
{
	if( Type.m_Type >= typeObject ) {

		if( Type.m_Type >= typeIndex ) {

			if( From.m_Type != Type.m_Type && From.m_Type != typeInteger ) {

				CFormat Error( IDS_EXPR_NEED_INDEX,
					       pWhat,
					       GetObjectName(Type.m_Type)
					       );
			
				m_pError->Set(Error);

				ThrowError(Token);
				}
			}
		else {
			if( From.m_Type != Type.m_Type ) {

				CFormat Error( IDS_EXPR_NEED_OBJECT,
					       pWhat,
					       GetObjectName(Type.m_Type)
					       );
			
				m_pError->Set(Error);

				ThrowError(Token);
				}
			}
		}

	if( From.m_Flags & flagArray ) {

		if( !(Type.m_Flags & flagArray) ) {

			m_pError->Set(CFormat(IDS_EXPR_NEED_SCALAR, pWhat));

			ThrowError(Token);
			}
		}

	if( Type.m_Flags & flagWritable ) {

		if( From.m_Flags & flagBitRef ) {

			if( From.m_Flags & flagExtended ) {

				m_pError->Set(IDS_CANNOT_TAKE_1);

				ThrowError(Token);
				}

			if( From.m_Flags & flagLocal ) {
		
				m_pError->Set(IDS_CANNOT_TAKE_2);

				ThrowError(Token);
				}
			}

		if( !(From.m_Flags & flagWritable) ) {
		
			m_pError->Set(CFormat(IDS_EXPR_NEED_LVALUE, pWhat));

			ThrowError(Token);
			}

		if( From.m_Flags & flagLocal ) {

			m_pError->Set(CFormat(IDS_EXPR_NEED_LVALUE, pWhat));

			ThrowError(Token);
			}
		}

	if( Type.m_Flags & flagCommsTab ) {

		if( From.m_Flags & flagCommsRef ) {

			if( !(From.m_Flags & flagCommsTab) ) {

				m_pError->Set(CFormat(IDS_MUST_BE_1, pWhat));

				ThrowError(Token);
				}
			}
		}
	
	if( Type.m_Flags & flagTagRef ) {

		if( !(From.m_Flags & flagTagRef) ) {

			m_pError->Set(CFormat(IDS_MUST_BE_2, pWhat));

			ThrowError(Token);
			}
		}
	
	if( Type.m_Flags & flagElement ) {

		if( !(From.m_Flags & flagElement) ) {
		
			m_pError->Set(CFormat(IDS_EXPR_NEED_ELEMENT, pWhat));

			ThrowError(Token);
			}
		}
	
	if( Type.m_Flags & flagConstant ) {

		if( !(From.m_Flags & flagConstant) ) {

			m_pError->Set(CFormat(IDS_EXPR_NEED_CONSTANT, pWhat));

			ThrowError(Token);
			}
		}

	if( Type.m_Type == typeLogical || Type.m_Type == typeInteger ) {

		if( From.m_Type != typeLogical && From.m_Type != typeInteger ) {

			m_pError->Set(CFormat(IDS_EXPR_NEED_INTEGER, pWhat));

			ThrowError(Token);
			}
		}

	if( Type.m_Type == typeReal || Type.m_Type == typeNumeric ) {

		if( From.m_Type != typeLogical && From.m_Type != typeInteger ) {
			
			if( From.m_Type != typeReal ) {

				m_pError->Set(CFormat(IDS_EXPR_NEED_NUMERIC, pWhat));

				ThrowError(Token);
				}
			}
		}

	if( Type.m_Type == typeString ) {

		if( Type.m_Flags & flagInherent ) {

			if( From.m_Flags & flagCommsRef ) {

				if( From.m_Type == typeInteger ) {

					return;
					}
				}
			}

		if( From.m_Type != typeString ) {

			m_pError->Set(CFormat(IDS_EXPR_NEED_STRING, pWhat));

			ThrowError(Token);
			}
		}
	}

// Common Errors

void CExprParser::NotWithString(CLexToken const &Token)
{
	CFormat Error(IDS_EXPR_NOT_STRING, Token.Describe());

	m_pError->Set(Error);

	ThrowError(Token);
	}

void CExprParser::NotWithReal(CLexToken const &Token)
{
	CFormat Error(IDS_EXPR_NOT_REAL, Token.Describe());

	m_pError->Set(Error);

	ThrowError(Token);
	}

void CExprParser::NotWithBitRef(CLexToken const &Token)
{
	CFormat Error(IDS_EXPR_NOT_BITREF, Token.Describe());

	m_pError->Set(Error);

	ThrowError(Token);
	}

void CExprParser::NotGoodCast(CLexToken const &Token)
{
	m_pError->Set(IDS_EXPR_BAD_CAST);

	ThrowError(Token);
	}

void CExprParser::NoComplexAssign(CLexToken const &Token)
{
	m_pError->Set(IDS_COMPLEX);

	ThrowError(Token);
	}

// Source Building

void CExprParser::AppendSource(CString const &Data, UINT uAdd)
{
	UINT  uSize = Data.GetLength() + uAdd;

	PCTXT pText = PCTXT(Data);

	for( UINT n = 0; n < uSize; n++ ) {

		if( HIWORD(pText[n]) ) {

			m_Source.Append(srcUnicode);

			m_Source.Append(LOBYTE(pText[n]));

			m_Source.Append(HIBYTE(pText[n]));

			continue;
			}

		if( pText[n] & 0x80 ) {

			m_Source.Append(srcUpper);

			m_Source.Append(BYTE(pText[n] & 0x7F));

			continue;
			}

		m_Source.Append(BYTE(pText[n]));
		}
	}

void CExprParser::InsertSource(UINT uPos, CString const &Data, UINT uAdd)
{
	UINT  uSize  = Data.GetLength() + uAdd;

	PCTXT pText = PCTXT(Data);

	for( UINT n = 0; n < uSize; n++ ) {

		if( HIBYTE(pText[n]) ) {

			m_Source.Insert(uPos++, srcUnicode);

			m_Source.Insert(uPos++, LOBYTE(pText[n]));

			m_Source.Insert(uPos++, HIBYTE(pText[n]));

			continue;
			}

		if( pText[n] & 0x80 ) {

			m_Source.Insert(uPos++, srcUpper);

			m_Source.Insert(uPos++, BYTE(pText[n] & 0x7F));

			continue;
			}

		m_Source.Insert(uPos++, BYTE(pText[n]));
		}
	}

void CExprParser::InsertSource(UINT uPos, PCBYTE pData, UINT uSize)
{
	m_Source.Insert(uPos, pData, uSize);
	}

// Object Naming

CString CExprParser::GetObjectName(UINT Type)
{
	CString Name = IDS_EXPR_DEF_TYPE;

	if( m_pName ) {
		
		m_pName->NameClass(Type, Name);

		return Name;
		}

	return Name;
	}

// End of File
