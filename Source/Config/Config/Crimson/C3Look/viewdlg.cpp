
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// View Dialog Window
//

// Runtime Class

AfxImplementRuntimeClass(CViewDialog, CDialog);

// Constructor

CViewDialog::CViewDialog(void)
{
	m_pView      = NULL;

	m_fTranslate = FALSE;

	m_pButton1   = New CButton;
	}

CViewDialog::CViewDialog(CString Caption, CViewWnd *pView)
{
	m_Caption  = Caption;
	
	m_pView    = pView;

	m_fTranslate = FALSE;

	m_pButton1 = New CButton;
	}

// Dialog Operations

BOOL CViewDialog::Create(CWnd &Parent)
{
	DWORD dwStyle   = WS_POPUP | WS_SYSMENU | WS_CAPTION | WS_DLGFRAME | WS_CLIPCHILDREN;

	DWORD dwExStyle = WS_EX_DLGMODALFRAME;

	CWnd::Create( m_Caption,
		      dwExStyle, dwStyle,
		      CRect(0, 0, 1000, 1000),
		      Parent,
		      0,
		      NULL
		      );

	return TRUE;
	}

// Class Definition

PCTXT CViewDialog::GetDefaultClassName(void) const
{
	return L"CrimsonDlg";
	}

BOOL CViewDialog::GetClassDetails(WNDCLASSEX &Class) const
{
	if( CWnd::GetClassDetails(Class) ) {

		Class.hIcon   = HICON(afxMainWnd->GetClassLong(GCL_HICON));

		Class.hIconSm = HICON(afxMainWnd->GetClassLong(GCL_HICONSM));

		return TRUE;
		}

	return FALSE;
	}

// Routing Control

BOOL CViewDialog::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView ) {

		if( m_pView->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CDialog::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CViewDialog, CDialog)
{
	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_GETMINMAXINFO)

	AfxMessageEnd(CViewDialog)
	};

// Command Handlers

BOOL CViewDialog::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

// Message Handlers

void CViewDialog::OnPostCreate(void)
{
	FindLayout();

	CreateButtons();

	CreateView();
	
	FindBestSize();

	PlaceDialogCentral();
	}

void CViewDialog::OnClose(void)
{
	OnOkay(IDOK);
	}

BOOL CViewDialog::OnEraseBkGnd(CDC &DC)
{
	DC.FillRect(GetClientRect(), afxBrush(3dFace));

	return TRUE;
	}

void CViewDialog::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
	}

void CViewDialog::OnSize(UINT uCode, CSize Size)
{
	m_pView->MoveWindow(GetViewRect(), TRUE);

	MoveButtons();
	}

void CViewDialog::OnGetMinMaxInfo(MINMAXINFO &Info)
{
	if( m_pView ) {

		MINMAXINFO Work;

		memset(&Work, 0, sizeof(Work));

		m_pView->SendMessage(WM_GETMINMAXINFO, 0, LPARAM(&Work));

		CSize MinSize = CSize(Work.ptMinTrackSize);

		CSize MaxSize = CSize(Work.ptMaxTrackSize);

		AdjustWindowSize(MinSize);
		
		AdjustWindowSize(MaxSize);

		MinSize.cy += m_Button.cy + 2 * m_Border.cy;

		MaxSize.cy += m_Button.cy + 2 * m_Border.cy;

		Info.ptMinTrackSize = CPoint(MinSize);
		
		Info.ptMaxTrackSize = CPoint(MaxSize);
		}
	}

// Implementation

void CViewDialog::FindLayout(void)
{
	m_Button.cx = 64;

	m_Button.cy = 24;

	m_Border.cx = 4;
	
	m_Border.cy = 4;
	}

void CViewDialog::CreateView(void)
{
	UINT  uID       = IDVIEW;
	
	DWORD dwStyle   = WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE;

	DWORD dwExStyle = WS_EX_CONTROLPARENT;

	m_pView->Create( dwExStyle, dwStyle, 
			 GetViewRect(), 
			 ThisObject, 
			 uID, 
			 NULL
			 );
	}

void CViewDialog::CreateButtons(void)
{
	m_pButton1->Create( L"Close",
			    WS_TABSTOP | WS_VISIBLE | BS_DEFPUSHBUTTON,
			    CRect(),
			    ThisObject,
			    IDOK
			    );

	m_pButton1->SetFont(afxFont(Dialog));
	}

void CViewDialog::FindBestSize(void)
{
	MINMAXINFO Work;

	memset(&Work, 0, sizeof(Work));

	m_pView->SendMessage(WM_GETMINMAXINFO, 0, LPARAM(&Work));

	CSize MinSize = CSize(Work.ptMinTrackSize);

	CSize MaxSize = CSize(Work.ptMaxTrackSize);

	MakeMin(MaxSize.cx, 3 * MinSize.cx / 2);

	MakeMin(MaxSize.cy, 3 * MinSize.cy / 2);

	MakeMin(MaxSize.cx, 600);

	MakeMin(MaxSize.cy, 400);

	CSize Size = (MinSize + MaxSize) / 2;

	AdjustWindowSize(Size);

	Size.cy += 64;

	SetWindowSize(Size, TRUE);
	}

CRect CViewDialog::GetViewRect(void)
{
	CRect Rect   = GetClientRect();

	Rect.bottom -= m_Button.cy + 2 * m_Border.cy;

	return Rect;
	}

void CViewDialog::MoveButtons(void)
{
	CRect Rect = GetClientRect();

	Rect.bottom = Rect.bottom - m_Border.cy;

	Rect.top    = Rect.bottom - m_Button.cy;

	Rect.left   = Rect.left  + m_Border.cx;

	Rect.right  = Rect.left  + m_Button.cx;

	m_pButton1->MoveWindow(Rect, TRUE);
	}

// End of File
