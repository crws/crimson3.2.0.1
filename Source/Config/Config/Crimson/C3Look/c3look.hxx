
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3LOOK_HXX
	
#define	INCLUDE_C3LOOK_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3core.hxx>

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_RES			0xB8
#define	IDM_RES_TITLE		0xB800
#define	IDM_RES_JUMP		0xB801
#define	IDM_RES_HELP		0xB802
#define	IDM_RES_EXPAND		0xB803
#define	IDM_RES_COLLAPSE	0xB804
#define	IDM_RES_TOGGLE		0xB805

#define	IDM_GLOBAL		0xE0
#define	IDM_GLOBAL_COMMIT	0xE001
#define IDM_GLOBAL_VALIDATE	0xE002
#define	IDM_GLOBAL_DROP_LINK	0xE003
#define	IDM_GLOBAL_STOP_DEBUG	0xE004
#define IDM_GLOBAL_SEMI_START	0xE005
#define IDM_GLOBAL_SEMI_END	0xE006
#define IDM_GLOBAL_ENABLE	0xE007
#define IDM_GLOBAL_DISABLE	0xE008

// End of File

#endif
