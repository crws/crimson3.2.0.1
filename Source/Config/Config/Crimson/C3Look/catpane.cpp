
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Category Pane Window
//

// Runtime Class

AfxImplementRuntimeClass(CCatPaneWnd, CPaneWnd);

// Static Data

UINT CCatPaneWnd::timerCheck = AllocTimerID();

// Layout Constants

int const CCatPaneWnd::sizeCategory = 33;

int const CCatPaneWnd::sizeToolbar  = 26;

int const CCatPaneWnd::sizeSplit    = 5;

// Constructor

CCatPaneWnd::CCatPaneWnd(void)
{
	m_uCount  = 0;

	m_uSelect = 0;

	m_uHover  = 0;

	m_fPress  = FALSE;

	m_Cursor1.Create(L"VSplitAdjust");

	m_Cursor2.Create(IDC_HAND);
	}

// Attributes

CString CCatPaneWnd::GetCat(void) const
{
	return m_Cats[m_uSelect].m_Tag;
	}

// Operations

void CCatPaneWnd::ShowCat(CString Cat)
{
	if( !Cat.IsEmpty() ) {
		
		for( UINT n = 0; n < m_uCount; n++ ) {

			if( m_Cats[n].m_Tag == Cat ) {

				Select(n);

				break;
				}
			}

		OnGoingIdle();
		}
	}

void CCatPaneWnd::SwapCat(CString &Cat)
{
	if( !Cat.IsEmpty() ) {

		CString Was = m_Cats[m_uSelect].m_Tag;
		
		for( UINT n = 0; n < m_uCount; n++ ) {

			if( m_Cats[n].m_Tag == Cat ) {

				Select(n);

				break;
				}
			}

		Cat = Was;

		OnGoingIdle();
		}
	}

// Message Map

AfxMessageMap(CCatPaneWnd, CPaneWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchGetInfoType(IDM_GO, OnGoGetInfo)
	AfxDispatchControlType(IDM_GO, OnGoControl)
	AfxDispatchCommandType(IDM_GO, OnGoCommand)

	AfxMessageEnd(CCatPaneWnd)
	};

// Accelerator

BOOL CCatPaneWnd::OnAccelerator(MSG &Msg)
{
	if( afxMainWnd->IsActive() ) {

		if( IsCurrent() ) {
			
			if( m_Accel2.Translate(Msg) ) {

				return TRUE;
				}
			}
		}

	if( m_Accel1.Translate(Msg) ) {
		
		return TRUE;
		}

	return FALSE;
	}

// Message Handlers

void CCatPaneWnd::OnPostCreate(void)
{
	AttachItemView();

	CPaneWnd::OnPostCreate();

	FindSplit();
	}

void CCatPaneWnd::OnSize(UINT uCode, CSize Size)
{
	CPaneWnd::OnSize(uCode, Size);

	if( CheckSplit() ) {

		OnUpdateUI();

		CPaneWnd::OnSize(uCode, Size);

		FindSplit();
		}
	}

void CCatPaneWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = CWnd::GetClientRect();

	DC.FrameRect(Rect, afxBrush(3dShadow));

	CRect Work = CPaneWnd::GetClientRect();

	DC.SetBkMode(TRANSPARENT);

	DC.Select(afxFont(Bolder));

	for( UINT n = 0; n < m_uShow; n++ ) {

		UINT  uCat  = m_uShow - 1 - n;

		CRect Draw  = Work;

		CRect Line  = Work;

		Draw.top    = Draw.bottom - sizeCategory + 1;

		Line.bottom = Draw.top;

		Line.top    = Draw.top    - 1;

		if( DC.GetPaintRect().Overlaps(Draw) ) {

			DrawCategory(DC, Draw, uCat);
			}

		if( DC.GetPaintRect().Overlaps(Line) ) {

			DC.FillRect(Line, afxBrush(3dShadow));
			}

		Work.bottom = Line.top;
		}

	DrawSplit(DC);

	DC.Deselect();
	}

void CCatPaneWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 3 ) {
		
		for( UINT n = m_uShow; n < m_uCount; n++ ) {

			CCategory const &Cat = m_Cats[n];

			Menu.AppendMenu( 0,
					 m_uBase + n,
					 CPrintf(L"%8.8X", Cat.m_Image2)
					 );
			}
		}
	}

void CCatPaneWnd::OnSetCurrent(BOOL fCurrent)
{
	m_pView->SetCurrent(fCurrent);
	}

BOOL CCatPaneWnd::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( &Wnd == this && uHitTest == HTCLIENT ) {

		CRect  Rect = CWnd::GetClientRect() - 1;
	
		CPoint Pos  = GetCursorPos();
		
		ScreenToClient(Pos);
		
		if( m_Split.PtInRect(Pos) ) {
		
			SetCursor(m_Cursor1);
			
			return TRUE;
			}

		if( Rect.PtInRect(Pos) ) {
		
			SetCursor(m_Cursor2);
		
			return TRUE;
			}
		}
	
	return BOOL(AfxCallDefProc());
	}

void CCatPaneWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	afxMainWnd->SendMessage(WM_CANCELMODE);
	}

void CCatPaneWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_uCapture ) {

		UINT uHover = CheckHover(Pos);

		if( uHover ) {
				
			InvalidateCat(uHover);

			m_uCapture = uHover;
			}
		else {
			if( m_Split.PtInRect(Pos) ) {

				m_uCapture  = 200;

				m_nTrackOrg = Pos.y;

				m_nTrackPos = Pos.y;

				int nBottom = CWnd::GetClientRect().bottom - 1 - (m_Split.bottom - Pos.y);

				m_nTrackMin = nBottom - sizeCategory * m_uCount;

				m_nTrackMax = nBottom - sizeToolbar;
				}
			}

		if( m_uCapture ) {

			if( GetCaptureMode() == 2 ) {

				CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

				ShowDragBar(DC);
				}

			SetCapture();

			KillTimer(timerCheck);

			m_fPress = TRUE;
			}
		}

	SetFocus();
	}

void CCatPaneWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( m_uCapture ) {

		if( GetCaptureMode() == 1 ) {

			BOOL fPress = (CheckHover(Pos) == m_uCapture);

			if( m_fPress != fPress ) {

				InvalidateCat(m_uCapture);

				m_fPress = fPress;
				}
			}

		if( GetCaptureMode() == 2 ) {

			CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

			ShowDragBar(DC);

			m_nTrackPos = Pos.y;

			MakeMin(m_nTrackPos, m_nTrackMax);

			MakeMax(m_nTrackPos, m_nTrackMin);

			ShowDragBar(DC);
			}
		}
	else {
		UINT uHover = CheckHover(Pos);

		if( m_uHover != uHover ) {

			InvalidateCat(m_uHover);

			if( (m_uHover = uHover) ) {
							
				SetTimer(timerCheck, 10);
				}
			
			InvalidateCat(m_uHover);

			return;
			}
		}
	}

void CCatPaneWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_uCapture ) {

		ReleaseCapture();

		if( GetCaptureMode() == 1 ) {

			if( m_fPress ) {

				Select(GetCaptureItem());
				}
			}

		if( GetCaptureMode() == 2 ) {

			CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

			ShowDragBar(DC);

			m_uShow  = CalcShowCount();

			m_fShow3 = (m_uShow < m_uCount);

			UpdateAll();

			SaveConfig();
			}

		m_uCapture = 0;

		m_uHover   = 0;

		m_fPress   = FALSE;

		m_pView->SetFocus();

		OnMouseMove(uFlags, Pos);
		}
	}

void CCatPaneWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	CPoint Pos = GetCursorPos();

	ScreenToClient(Pos);

	if( !CheckHover(Pos) ) {

		InvalidateCat(m_uHover);

		m_uHover = 0;

		KillTimer(timerCheck);
		}
	}

// Command Handlers

BOOL CCatPaneWnd::OnGoGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= m_uBase && uID < m_uBase + m_uCount ) {

		UINT uCat = uID - m_uBase;

		RCAT Cat  = m_Cats[uCat];

		Info.m_Prompt  = CFormat(m_Prompt, CString(Cat.m_Name));

		Info.m_ToolTip = Cat.m_Name;

		Info.m_Image   = Cat.m_Image2;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatPaneWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	if( uID >= m_uBase && uID < m_uBase + m_uCount ) {

		UINT uCat = uID - m_uBase;

		if( !Src.IsKindOf(AfxRuntimeClass(CCmdSourceMenu)) ) {

			Src.CheckItem(m_uSelect == uCat);
			}

		Src.EnableItem(TRUE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatPaneWnd::OnGoCommand(UINT uID)
{
	if( uID >= m_uBase && uID < m_uBase + m_uCount ) {

		UINT uCat = uID - m_uBase;

		Select(uCat);

		return TRUE;
		}

	return FALSE;
	}

// Overridable

CRect CCatPaneWnd::GetClientRect(void)
{
	CRect Rect = CPaneWnd::GetClientRect();

	Rect.bottom -= m_uShow * sizeCategory;

	Rect.bottom -= sizeSplit;

	return Rect;
	}

// Drawing Helpers

void CCatPaneWnd::DrawSplit(CDC &DC)
{
	DC.GradVert(m_Split, afxColor(Blue4), afxColor(Blue2));

	for( UINT n = 0; n < 8; n++ ) {

		int xp = (m_Split.left + m_Split.right - 40) / 2 + 5 * n;

		int yp = (m_Split.top + m_Split.bottom -  1) / 2;

		CRect Rect(xp-1, yp-1, xp+1, yp+1);

		DC.FillRect(Rect+CPoint(1,1), afxBrush(3dFace));

		DC.FillRect(Rect+CPoint(0,0), afxBrush(3dDkShadow));
		}
	}

void CCatPaneWnd::DrawCategory(CDC &DC, CRect Rect, UINT uCat)
{
	RCAT  Cat    = m_Cats[uCat];

	CRect Draw   = Rect;

	UINT  uState = 0;

	if( m_uSelect == uCat ) {

		if( m_uHover == 100 + uCat ) {

			DC.GradVert( Draw,
				     afxColor(NavBar1),
				     afxColor(NavBar2)
				     );

			uState = MF_HILITE | MF_POPUP;
			}
		else {
			DC.GradVert( Draw,
				     afxColor(NavBar2),
				     afxColor(NavBar1)
				     );
			}
		}
	else {
		if( m_uHover == 100 + uCat ) {

			if( m_fPress ) {

				DC.GradVert( Draw,
					     afxColor(NavBar1),
					     afxColor(NavBar2)
					     );
				}
			else {
				DC.GradVert( Draw,
					     afxColor(NavBar4),
					     afxColor(NavBar3)
					     );
				}

			uState = MF_HILITE | MF_POPUP;
			}
		else {
			DC.GradVert( Draw,
				     afxColor(Blue2),
				     afxColor(Blue3)
				     );
			}
		}

	Draw.left  = Draw.left + 2;

	Draw.right = Draw.left + Draw.cy();

	afxButton->Draw(DC, Draw, Cat.m_Image1, L"", uState);

	Draw.left  = Draw.right + 6;

	Draw.right = Rect.right;

	DC.DrawText(Cat.m_Name, Draw, DT_SINGLELINE | DT_VCENTER);
	}

void CCatPaneWnd::ShowDragBar(CDC &DC)
{
	CRect Split = CWnd::GetClientRect() - 1;

	UINT  uShow = CalcShowCount();

	if( uShow < m_uCount ) {

		Split.bottom -= sizeToolbar;
		}

	Split.bottom = Split.bottom - sizeCategory * uShow;

	Split.top    = Split.bottom - sizeSplit;
	
	DC.PatBlt(Split, DSTINVERT);
	}

// Item Location

CMetaItem * CCatPaneWnd::FindCatItem(UINT uCat)
{
	RCAT       Cat   = m_Cats[uCat];

	CString    Name  = Cat.m_Tag;

	CMetaItem *pItem = Cat.m_pDbase->GetSystemItem();

	while( !Name.IsEmpty() ) {

		CMetaData const *pMeta = pItem->FindMetaData(Name.StripToken('/'));

		if( pMeta ) {

			CItem *pFind = pMeta->GetObject(pItem);

			if( pFind->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

				pItem = (CMetaItem *) pFind;

				continue;
				}
			}

		AfxAssert(FALSE);

		return NULL;
		}

	return pItem;
	}

// Category Selection

void CCatPaneWnd::OnSetCategory(CItem *pItem)
{
	}

// Implementation

UINT CCatPaneWnd::GetCaptureMode(void)
{
	return m_uCapture / 100;
	}

UINT CCatPaneWnd::GetCaptureItem(void)
{
	return m_uCapture % 100;
	}

BOOL CCatPaneWnd::Select(CString Nav)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CItem *pItem = FindCatItem(n);

		if( pItem->GetFixedPath() == Nav ) {

			Select(n);

			return TRUE;
			}
		}

	return FALSE;
	}

void CCatPaneWnd::Select(UINT uSelect)
{
	if( m_uSelect != uSelect ) {

		InvalidateCat(100 + m_uSelect);

		m_uSelect = uSelect;

		InvalidateCat(100 + m_uSelect);

		UpdateWindow();

		AttachItemView();

		OnUpdateUI();
		}
	}

BOOL CCatPaneWnd::AttachItemView(void)
{
	CItem *pItem = FindCatItem(m_uSelect);

	if( !m_pView->IsItem(pItem) ) {

		if( !IsCurrent() ) {

			OnSetCategory(pItem);
			
			m_pView->Attach(pItem);
			}
		else {
			AfxNull(CWnd).SetFocus();

			OnSetCategory(pItem);

			m_pView->Attach(pItem);

			SetFocus();
			}

		return TRUE;
		}

	return FALSE;
	}

void CCatPaneWnd::InvalidateCat(UINT uCat)
{
	if( uCat ) {

		if( (uCat %= 100) < m_uShow ) {
		
			CRect Rect  = CPaneWnd::GetClientRect();

			Rect.top    = Rect.bottom - sizeCategory * (m_uShow - uCat);

			Rect.bottom = Rect.top    + sizeCategory;
			
			Invalidate(Rect, FALSE);
			}
		}
	}

UINT CCatPaneWnd::CheckHover(CPoint Pos)
{
	CRect Rect = GetClientRect();

	if( Pos.x >= Rect.left && Pos.x < Rect.right ) {

		Rect.bottom += sizeSplit;

		if( Pos.y >= Rect.bottom ) {

			UINT uHover = (Pos.y - Rect.bottom) / sizeCategory;

			if( uHover < m_uShow ) {

				return 100 + uHover;
				}
			}
		}

	return 0;
	}

UINT CCatPaneWnd::CalcShowCount(void)
{
	UINT uAdj = sizeCategory / 2;

	int  nMin = GetSplitLimit();

	UINT uPos = m_Split.bottom + max(nMin, m_nTrackPos) - m_nTrackOrg;
	
	UINT uGap = CWnd::GetClientRect().bottom - 1 - uPos + uAdj;

	if( uGap / sizeCategory >= m_uCount ) {

		return m_uCount;
		}
		
	uGap = uGap - sizeToolbar;

	return uGap / sizeCategory;
	}

void CCatPaneWnd::FindConfig(CRegKey &Key)
{
	Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"C3Look");

	Key.MoveTo(m_KeyName);
	}

void CCatPaneWnd::LoadConfig(void)
{
	CRegKey Key;

	FindConfig(Key);

	m_uShow  = Key.GetValue(L"Count", 100);

	m_fShow3 = (m_uShow < m_uCount);

	MakeMin(m_uShow, m_uCount);

	SaveConfig();
	}

void CCatPaneWnd::SaveConfig(void)
{
	CRegKey Key;

	FindConfig(Key);

	UINT uShow = (m_uShow == m_uCount) ? 100 : m_uShow;

	Key.SetValue(L"Count", uShow);
	}

BOOL CCatPaneWnd::UpdateAll(void)
{
	if( m_fValid ) {

		OnUpdateUI();

		UpdateLayout();

		FindSplit();

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatPaneWnd::FindSplit(void)
{
	if( m_fValid ) {

		m_Split = CPaneWnd::GetClientRect();

		m_Split.bottom = m_Split.bottom - m_uShow * sizeCategory;

		m_Split.top    = m_Split.bottom - sizeSplit;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatPaneWnd::CheckSplit(void)
{
	if( FindSplit() ) {

		BOOL fMove = FALSE;

		int  nTest = GetSplitLimit();

		while( m_uShow && m_Split.top < nTest ) {

			m_uShow -= 1;

			m_fShow3 = TRUE;

			fMove    = TRUE;

			FindSplit();
			}

		if( fMove ) {

			SaveConfig();

			return TRUE;
			}
		}

	return FALSE;
	}

int CCatPaneWnd::GetSplitLimit(void)
{
	return CPaneWnd::GetClientRect().top + 64;
	}

// End of File
