
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd, CDotTreeWnd);

// Static Data

UINT    CNavTreeWnd::m_timerSelect = AllocTimerID();

UINT    CNavTreeWnd::m_timerScroll = AllocTimerID();

CItem * CNavTreeWnd::m_pKilled     = NULL;

// Sort Context

CNavTreeWnd * CNavTreeWnd::m_pSort = NULL;

int           CNavTreeWnd::m_nSort = 1;

// Constructors

CNavTreeWnd::CNavTreeWnd(CString List, CLASS Folder, CLASS Class)
{
	m_List	    = List;

	m_Folder    = Folder;

	m_Class     = Class;

	m_pEmpty    = NULL;

	m_dwStyle   = m_dwStyle | TVS_EDITLABELS | TVS_SHOWSELALWAYS;

	m_cfData    = RegisterClipboardFormat(L"C3.1 Nav Tree");

	m_fLocked   = FALSE;
	
	m_fInitSel  = TRUE;

	m_uDrop     = dropNone;

	m_uMode     = modeSelect;

	m_Accel1.Create(L"NavTreeAccel1");

	m_Accel2.Create(L"NavTreeAccel2");

	m_pTree->SetMultiple(FALSE);
	}

// IUpdate

HRESULT CNavTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{	
	if( uType == updateChildren ) {

		if( pItem == m_pList ) {

			if( IsWindowVisible() ) {
	
				RefreshTree();
				}
			else {
				m_fLocked = TRUE;
	
				RefreshTree();
	
				m_fLocked = FALSE;
				}
			}
		}

	return CDotTreeWnd::ItemUpdated(pItem, uType);
	}

// IDropTarget

HRESULT CNavTreeWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( CanAcceptDataObject(pData, m_fDropLocal) ) {

		m_uDrop = dropAccept;
		}
	else
		m_uDrop = dropOther;

	m_hDropRoot = NULL;

	m_hDropPrev = NULL;

	m_fDropMove = FALSE;

	BOOL fMove  = IsDropMove(dwKeys, pEffect);

	DropTrack(CPoint(pt.x, pt.y), fMove);

	return S_OK;
	}

HRESULT CNavTreeWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		BOOL fMove = IsDropMove(dwKeys, pEffect);
		
		DropTrack(CPoint(pt.x, pt.y), fMove);
		
		return S_OK;
		}
	
	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CNavTreeWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( m_uDrop ) {

		ShowDrop(FALSE);

		m_System.HidePaneOnDrop(0);

		m_uDrop = dropNone;

		return S_OK;
		}

	return S_OK;
	}

HRESULT CNavTreeWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop ) {

		ShowDrop(FALSE);

		if( m_uDrop == dropOther ) {

			// LATER -- Forward to view?
			}

		if( m_uDrop == dropAccept ) {
			
			DropDone(pData);
			
			SetFocus();
			}

		m_uDrop = dropNone;
	
		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Overridables

CString CNavTreeWnd::OnGetNavPos(void)
{	
	// TODO -- Multiple selections???

	if( m_pSelect ) {

		return m_pSelect->GetFixedPath();
		}

	return m_pItem->GetFixedPath();
	}

BOOL CNavTreeWnd::OnNavigate(CString const &Nav)
{	
	// TODO -- Multiple selections???

	if( Nav.IsEmpty() ) {

		m_pTree->SelectItem(m_hRoot);

		return TRUE;
		}
	else {
		CString Find = Nav;

		while( !Find.IsEmpty() ) {

			HTREEITEM hFind = m_MapFixed[Find];

			if( hFind ) {

				if( hFind == m_hRoot ) {

					if( Find != Nav ) {

						return FALSE;
						}
					}
					
				m_pTree->SelectItem(hFind);

				m_System.SetViewedItem(m_pSelect);

				return TRUE;
				}

			Find = Find.Left(Find.FindRev('/'));
			}

		return FALSE;
		}
	}

void CNavTreeWnd::OnExec(CCmd *pCmd)
{	
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		LockUpdate(!m_fLocked);
		}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnExecCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnExecRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnExecDelete((CCmdDelete *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnExecPaste((CCmdPaste *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdOrder) ) {

		OnExecOrder((CCmdOrder *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdLock) ) {

		CCmdLock *pLock = (CCmdLock *) pCmd;

		LockItem(pLock->m_fLock);

		m_System.ItemUpdated(m_pSelect, updateContents);

		ItemUpdated(m_pSelect, updateLocked);
		}

	if( Class == AfxRuntimeClass(CCmdPrivate) ) {

		CCmdPrivate *pHide = (CCmdPrivate *) pCmd;

		HideItem(pHide->m_fHide);

		m_System.ItemUpdated(m_pSelect, updateContents);
		}
	}

void CNavTreeWnd::OnUndo(CCmd *pCmd)
{
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		LockUpdate(!m_fLocked);
		}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnUndoCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnUndoRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnUndoDelete((CCmdDelete *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnUndoPaste((CCmdPaste *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdOrder) ) {

		OnUndoOrder((CCmdOrder *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdLock) ) {

		CCmdLock *pLock = (CCmdLock *) pCmd;

		LockItem(pLock->m_fPrev);

		m_System.ItemUpdated(m_pSelect, updateContents);

		ItemUpdated(m_pSelect, updateLocked);
		
		return;
		}

	if( Class == AfxRuntimeClass(CCmdPrivate) ) {

		CCmdPrivate *pHide = (CCmdPrivate *) pCmd;

		HideItem(pHide->m_fPrev);

		m_System.ItemUpdated(m_pSelect, updateContents);
	
		return;
		}
	}

// Message Map

AfxMessageMap(CNavTreeWnd, CDotTreeWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_SIZE)

	AfxDispatchNotify(100, TVN_PICKITEM,       OnTreePickItem   )
	AfxDispatchNotify(100, TVN_KEYDOWN,        OnTreeKeyDown    )
	AfxDispatchNotify(100, NM_RETURN,          OnTreeReturn     )
	AfxDispatchNotify(100, NM_DBLCLK,          OnTreeDblClk     )
	AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit  )
	AfxDispatchNotify(100, TVN_ENDLABELEDIT,   OnTreeEndEdit    )
	
	AfxDispatchControlType(IDM_GO,   OnGoControl  )
	AfxDispatchCommandType(IDM_GO,   OnGoCommand  )
	AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM, OnItemControl)
	AfxDispatchCommandType(IDM_ITEM, OnItemCommand)
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxMessageEnd(CNavTreeWnd)
	};

// Message Handlers

void CNavTreeWnd::OnPostCreate(void)
{
	CDotTreeWnd::OnPostCreate();

	NormalizeList();

	if( !m_Empty.IsEmpty() ) {

		if( m_pList->GetItemCount() == 0 ) {

			m_pEmpty = New CEmptyPromptWnd;

			CRect Rect;
		
			GetEmptyRect(Rect);

			m_pEmpty->Create(m_Empty, WS_CHILD | WS_VISIBLE | SS_GRAYRECT, Rect, *m_pTree, 100, NULL);
			}
		}

	if( m_fInitSel ) {

		if( !m_System.IsNavBusy() ) {

			CheckInitSel();
			}
		}

	m_System.RegisterForUpdates(this);
	}

void CNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"NavTreeTool"));
		}
	}

void CNavTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{	
	if( fShow ) {

		if( !m_System.IsNavBusy() ) {

			SetViewedItem(TRUE);
			}

		SetMode(modeSelect);
		}
	}

void CNavTreeWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerDouble ) {

		m_System.FlipToItemView(TRUE);

		KillTimer(uID);
		}

	if( uID == m_timerSelect ) {

		if( m_uDrop ) {

			if( m_hDropRoot ) {

				if( m_uDrop == dropAccept ) {

					ShowDrop(FALSE);
					}
				
				if( IsExpandDrop() ) {

					ExpandItem(m_hDropRoot);

					m_pTree->UpdateWindow();
					}
				else {
					m_pTree->SelectItem(m_hDropRoot);

					m_pTree->UpdateWindow();
					}

				if( m_uDrop == dropAccept ) {

					ShowDrop(TRUE);
					}
				}
			}

		KillTimer(uID);
		}

	if( uID == m_timerScroll ) {

		if( m_uDrop ) {

			UINT  uCmd = NOTHING;

			CRect View = m_pTree->GetClientRect();

			ClientToScreen(View);

			if( View.PtInRect(m_DragPos) ) {

				View -= 40;

				if( m_DragPos.y < View.top ) {

					int nLimit = m_pTree->GetScrollRangeMin(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) > nLimit ) {

						uCmd = SB_LINEUP;
						}
					}

				if( m_DragPos.y > View.bottom ) {

					int nLimit = m_pTree->GetScrollRangeMax(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) < nLimit ) {

						uCmd = SB_LINEDOWN;
						}
					}
				
				if( uCmd < NOTHING ) {

					ShowDrop(FALSE);

					m_pTree->SendMessage(WM_VSCROLL, uCmd);

					m_pTree->UpdateWindow();
					
					ShowDrop(TRUE);

					return;
					}
				}
			}
		
		KillTimer(uID);
		}
	}

void CNavTreeWnd::OnSize(UINT uCode, CSize Size)
{
	CStdTreeWnd::OnSize(uCode, Size);

	if( m_pEmpty->IsWindow() ) {

		CRect Rect;

		GetEmptyRect(Rect);

		m_pEmpty->MoveWindow(Rect, TRUE);
		}
	}

// Notification Handlers

void CNavTreeWnd::OnTreePickItem(UINT uID, NMTREEVIEW &Info)
{	
	afxThread->SetStatusText(L"");

	SetMode(modeSelect);
	}

BOOL CNavTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{	
	switch( Info.wVKey ) {

		case VK_TAB:

			m_System.FlipToItemView(TRUE);

			return TRUE;			
		}

	return CDotTreeWnd::OnTreeKeyDown(uID, Info);
	}

void CNavTreeWnd::OnTreeReturn(UINT uID, NMHDR &Info)
{	
	if( m_pTree->GetChild(m_hSelect) ) {

		ToggleItem(m_hSelect);

		return;
		}

	m_System.FlipToItemView(TRUE);
	}

void CNavTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{	
	if( m_pTree->IsMouseInSelection() ) {

		if( m_pTree->GetChild(m_hSelect) ) {

			ToggleItem(m_hSelect);

			return;
			}

		SetTimer(m_timerDouble, 5);
		}
	}

BOOL CNavTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{	
	if( IsItemLocked(Info.item.hItem) ) {

		return TRUE;
		}
	
	return m_pNamed ? FALSE : TRUE;
	}

BOOL CNavTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{	
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
		}
	
	return FALSE;
	}

// Command Handlers

BOOL CNavTreeWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_GO_NEXT_1:
		case IDM_GO_NEXT_2:

			Src.EnableItem(m_pTree->GetNextNode(m_hSelect, TRUE) ? TRUE : FALSE);
			
			return TRUE;
		
		case IDM_GO_PREV_1:
		case IDM_GO_PREV_2:

			Src.EnableItem(m_pTree->GetPrevNode(m_hSelect, TRUE) ? TRUE : FALSE);

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnGoCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GO_NEXT_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_NEXT_1:

			OnGoNext(TRUE);

			return TRUE;

		case IDM_GO_PREV_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_PREV_1:

			OnGoPrev(TRUE);

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID == IDM_ITEM_RENAME ) {

		Info.m_Image = MAKELONG(0x0021, 0x1000);
		}

	if( uID == IDM_ITEM_NEW_FOLDER ) {

		Info.m_Image = MAKELONG(0x0016, 0x1000);
		}

	if( uID == IDM_ITEM_SYNC ) {

		Info.m_Image = MAKELONG(0x0024, 0x1000);
		}

	if( uID == IDM_ITEM_SORT_UP ) {

		Info.m_Image = MAKELONG(0x002C, 0x1000);
		}

	if( uID == IDM_ITEM_SORT_DOWN ) {

		Info.m_Image = MAKELONG(0x002D, 0x1000);
		}

	if( uID == IDM_ITEM_SYNC ) {

		Info.m_Image = MAKELONG(0x0024, 0x1000);
		}

	if( uID == IDM_ITEM_FIND ) {

		Info.m_Image = MAKELONG(0x000C, 0x1000);
		}

	if( uID == IDM_ITEM_USAGE ) {

		Info.m_Image = MAKELONG(0x000C, 0x1000);
		}

	if( uID == IDM_ITEM_EXPAND ) {

		if( m_pTree->IsExpanded(m_hSelect) ) {

			Info.m_Image = MAKELONG(0x0020, 0x1000);
			}
		else
			Info.m_Image = MAKELONG(0x001F, 0x1000);
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	CString Verb;

	switch( uID ) {

		case IDM_ITEM_DELETE:

			Src.EnableItem(!IsSelLocked() && !IsRootSelected());

			return TRUE;

		case IDM_ITEM_RENAME:

			Src.EnableItem(!m_fMulti && !IsSelLocked() && !IsRootSelected());

			return TRUE;

		case IDM_ITEM_NEW_FOLDER:

			Src.EnableItem(!m_fMulti && m_Folder && !IsSelLocked());

			return TRUE;

		case IDM_ITEM_EXPAND:

			if( m_fMulti ) {

				Src.EnableItem(FALSE);
				}
			else {
				Verb = m_pTree->IsExpanded(m_hSelect) ? IDS_COLLAPSE : IDS_EXPAND;

				Src.SetItemText(Verb);

				Src.EnableItem (m_hSelect != m_hRoot && IsParent());
				}

			return TRUE;

		case IDM_ITEM_SYNC:

			Src.EnableItem(!m_fMulti && m_System.CanSyncPanes());

			return TRUE;

		case IDM_ITEM_SORT_UP:
		case IDM_ITEM_SORT_DOWN:

			if( !m_fMulti ) {

				Src.EnableItem(!IsReadOnly() && IsParent());
				}

			return TRUE;

		case IDM_ITEM_FIND:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_ITEM_PRIVATE:

			Src.EnableItem(!IsReadOnly() && !IsPrivate() && m_pSelect->HasPrivate());

			Src.CheckItem (m_pSelect->GetPrivate());

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_DELETE:

			OnItemDelete(IDS_DELETE_1, FALSE, TRUE);

			return TRUE;

		case IDM_ITEM_RENAME:

			m_pTree->EditLabel(m_hSelect);

			return TRUE;

		case IDM_ITEM_NEW_FOLDER:

			OnItemCreateFolder();

			return TRUE;

		case IDM_ITEM_EXPAND:

			ToggleItem(m_hSelect);

			return TRUE;

		case IDM_ITEM_SYNC:

			m_System.SyncPanes();

			return TRUE;
		
		case IDM_ITEM_SORT_UP:

			m_pSort = this;

			m_nSort = +1;

			OnItemSort(SortByName);

			return TRUE;

		case IDM_ITEM_SORT_DOWN:

			m_pSort = this;

			m_nSort = -1;

			OnItemSort(SortByName);

			return TRUE;

		case IDM_ITEM_FIND:

			OnItemFind();

			return TRUE;

		case IDM_ITEM_PRIVATE:

			OnItemPrivate();

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			Src.EnableItem(!IsSelLocked() && !IsRootSelected() && !IsItemLocked(m_hSelect));

			return TRUE;

		case IDM_EDIT_CUT:

			Src.EnableItem(!m_fMulti && !IsSelLocked() && !IsRootSelected() && !IsItemLocked(m_hSelect));

			return TRUE;

		case IDM_EDIT_COPY:

			Src.EnableItem(!m_fMulti && !IsRootSelected() && !IsItemPrivate(m_hSelect));

			return TRUE;

		case IDM_EDIT_PASTE:

			Src.EnableItem(!m_fMulti && !IsSelLocked() && CanEditPaste());

			return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			OnItemDelete(IDS_DELETE_1, FALSE, TRUE);

			return TRUE;

		case IDM_EDIT_CUT:

			OnEditCut();

			return TRUE;

		case IDM_EDIT_COPY:

			OnEditCopy();

			return TRUE;

		case IDM_EDIT_PASTE:

			OnEditPaste();

			return TRUE;
		}

	return FALSE;
	}

// Go Menu

BOOL CNavTreeWnd::OnGoNext(BOOL fSiblings)
{
	HTREEITEM hItem = m_pTree->GetNextNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::OnGoPrev(BOOL fSiblings)
{
	HTREEITEM hItem	  = m_pTree->GetPrevNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::StepAway(void)
{
	HTREEITEM hItem;
	
	if( (hItem = m_pTree->GetNext(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	if( (hItem = m_pTree->GetPrevious(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	if( (hItem = m_pTree->GetParent(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	return FALSE;
	}

// Item Menu : Create

void CNavTreeWnd::OnItemCreateFolder(void)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(IDS_GROUP, n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Folder);

			m_System.ExecCmd(pCmd);

			break;
			}
		}
	}

void CNavTreeWnd::OnExecCreate(CCmdCreate *pCmd)
{
	HTREEITEM hItem;

	if( pCmd->m_Class ) {

		CMetaItem *pItem = AfxNewObject(CMetaItem, pCmd->m_Class);

		CString    Root1 = GetRoot(FALSE);

		CString    Root2 = GetRoot(TRUE);

		CString    Name  = Root2 + pCmd->m_Name;

		HTREEITEM  hRoot = m_MapNames[Root1];

		MakeUnique(Name);

		AddToList(hRoot, m_hSelect, pItem);

		pItem->SetFixedName(pCmd->m_Fixed);

		pItem->SetName(Name);

		hItem        = AddToTree(hRoot, m_hSelect, pItem);

		pCmd->m_Make = pItem->GetFixedPath();

		pCmd->m_Menu = CFormat(pCmd->m_Menu, pItem->GetName());

		ExpandItem(m_hSelect);
		}
	else {
		CItem     *pBase = CItem::MakeFromSnapshot(m_pList, pCmd->m_hData);

		CMetaItem *pItem = (CMetaItem *) pBase;

		if( m_hSelect ) {
			
			CString    Root1 = GetRoot(FALSE);

			HTREEITEM  hRoot = m_MapNames[Root1];

			AddToList(hRoot, m_hSelect, pItem);

			hItem = AddToTree(hRoot, m_hSelect, pItem);

			ExpandItem(m_hSelect);
			}
		else {
			AddToList(m_hRoot, NULL, pItem);

			hItem = AddToTree(m_hRoot, NULL, pItem);			
			}
		}

	m_pTree->SelectItem(hItem);

	ListUpdated();
	}

void CNavTreeWnd::OnUndoCreate(CCmdCreate *pCmd)
{
	HTREEITEM  hItem = m_MapFixed[pCmd->m_Make];

	CMetaItem *pItem = GetItemPtr(hItem);

	m_pList->DeleteItem(pItem);

	KillTree(hItem, TRUE);

	m_pKilled = pItem;

	ListUpdated();
	}

// Item Menu : Rename

BOOL CNavTreeWnd::OnItemRename(CString Name)
{
	UINT    uPos = m_pNamed->GetName().FindRev(L'.');

	CString Prev = m_pNamed->GetName().Mid(uPos + 1);

	if( wstrcmp(Name, Prev) ) {

		if( uPos < NOTHING ) {

			Name = m_pNamed->GetName().Left(uPos + 1) + Name;
			}

		HTREEITEM hFind = m_MapNames[Name];

		if( hFind && hFind != m_hSelect ) {
		
			CString Text = CFormat(IDS_THE_NAME, Name);
		
			Error(Text);
			}
		else {
			CError Error(TRUE);

			if( C3ValidateName(Error, GetName(Name)) ) {

				CCmd *pCmd = New CCmdRename(m_pNamed, Name);

				m_System.ExecCmd(pCmd);

				return TRUE;
				}
			
			Error.Show(ThisObject);
			}
		}

	return FALSE;
	}

void CNavTreeWnd::OnExecRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Name, pCmd->m_Prev);
	}

void CNavTreeWnd::OnUndoRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Prev, pCmd->m_Name);
	}

void CNavTreeWnd::RenameItem(CString Name, CString Prev)
{
	m_pNamed->SetName(Name);

	m_pNamed->SetDirty();

	m_pTree->SetItemText(m_hSelect, GetName(Name));

	m_MapNames.Remove(Prev);

	m_MapNames.Insert(Name, m_hSelect);

	RenameFrom(m_hSelect, Name, Prev);

	OnItemRenamed(m_pNamed);
	}

BOOL CNavTreeWnd::RenameFrom(HTREEITEM hRoot, CString const &Name, CString const &Prev)
{
	HTREEITEM hItem = m_pTree->GetChild(hRoot);

	if( hItem ) {

		UINT  uStrip = Prev.GetLength();

		while( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			CString     Item = pItem->GetName().Mid(uStrip + 1);
			
			pItem->SetName(Name + L"." + Item);

			pItem->SetDirty();

			OnItemRenamed(pItem);

			m_MapNames.Remove(Prev + L"." + Item);

			m_MapNames.Insert(Name + L"." + Item, hItem);

			RenameFrom(hItem, Name, Prev);

			hItem = m_pTree->GetNext(hItem);
			}
		
		return TRUE;
		}

	return FALSE;
	}

// Item Menu : Delete

void CNavTreeWnd::OnItemDelete(UINT uVerb, BOOL fMove, BOOL fWarn)
{
	if( m_fMulti ) {

		CSysProxy Proxy;

		Proxy.Bind();

		if( Proxy.KillUndoList() ) {

			CStringArray List;

			HTREEITEM    hItem = m_pTree->GetFirstSelect();

			while( hItem ) {

				CMetaItem *pItem = GetItemPtr(hItem);

				CString    Fixed = pItem->GetFixedPath();

				List.Append(Fixed);

				hItem = m_pTree->GetNextSelect(hItem);
				}

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				hItem = m_MapFixed[List[n]];

				if( hItem ) {

					m_hSelect = hItem;

					PerformDelete(FALSE);
					}
				}
			}
		}
	else {
		if( !fWarn || WarnMultiple(uVerb) ) {

			afxThread->SetWaitMode(TRUE);

			IDataObject *pData = NULL;

			if( MakeDataObject(pData, FALSE) ) {

				CString   Item  = m_pSelect->GetFixedPath();

				CString   Menu  = CFormat(IDS_FORMAT, CString(uVerb), m_pNamed->GetName());

				HTREEITEM hRoot = m_pTree->GetParent  (m_hSelect);

				HTREEITEM hPrev = m_pTree->GetPrevious(m_hSelect);

				CItem *   pRoot = GetItemPtr(hRoot);

				CItem *   pPrev = GetItemOpt(hPrev);

				PerformDelete(fMove);

				CCmd *pCmd = New CCmdDelete(Menu, Item, pRoot, pPrev, pData, fMove);
				
				m_System.SaveCmd(pCmd);
				}

			afxThread->SetWaitMode(FALSE);
			}
		}
	}

void CNavTreeWnd::OnExecDelete(CCmdDelete *pCmd)
{
	afxThread->SetWaitMode(TRUE);

	PerformDelete(pCmd->m_fMove);

	afxThread->SetWaitMode(FALSE);
	}

void CNavTreeWnd::OnUndoDelete(CCmdDelete *pCmd)
{
	afxThread->SetWaitMode(TRUE);

	CAcceptCtx Ctx;

	Ctx.m_Root   = pCmd->m_Root;

	Ctx.m_Prev   = pCmd->m_Prev;

	Ctx.m_fPaste = FALSE;

	Ctx.m_pNames = NULL;

	Ctx.m_fCheck = FALSE;

	if( pCmd->m_fMove ) {

		Ctx.m_fUndel = FALSE;

		AcceptDataObject(pCmd->m_pData, Ctx);
		}
	else {
		LockUpdate(TRUE);
		
		Ctx.m_fUndel = TRUE;

		AcceptDataObject(pCmd->m_pData, Ctx);

		LockUpdate(FALSE);
		}

	m_System.SetNavCheckpoint();

	afxThread->SetWaitMode(FALSE);
	}

void CNavTreeWnd::PerformDelete(BOOL fMove)
{
	if( fMove ) {

		HTREEITEM hItem = m_hSelect;

		StepAway();

		DeleteFromHere(hItem, TRUE);

		OnItemDeleted(NULL, TRUE);

		KillTree(hItem, TRUE);
		}
	else {
		LockUpdate(TRUE);
		
		HTREEITEM hItem = m_hSelect;

		StepAway();

		DeleteFromHere(hItem, FALSE);

		OnItemDeleted(NULL, TRUE);

		KillTree(hItem, TRUE);

		LockUpdate(FALSE);
		}
	}

void CNavTreeWnd::DeleteFromHere(HTREEITEM hItem, BOOL fMove)
{
	HTREEITEM hChild;

	if( hChild = m_pTree->GetChild(hItem) ) {

		for(;;) {

			HTREEITEM hNext = m_pTree->GetNext(hChild);

			DeleteFromHere(hChild, fMove);

			if( hNext == NULL ) {

				break;
				}

			hChild = hNext;
			}
		}

	CItem *pItem = GetItemPtr(hItem);

	if( fMove ) {

		m_pList->DeleteItem(pItem);
		}
	else {
		m_pList->RemoveItem(pItem);
		
		OnItemDeleted(pItem, TRUE);

		pItem->Kill();

		delete pItem;
		}

	#pragma warning(suppress: 6001)

	m_pKilled = pItem;
	}

// Item Menu : Sort

void CNavTreeWnd::OnItemSort(int (*pfnSort)(PCVOID, PCVOID))
{
	UpdateWindow();

	afxThread->SetWaitMode(TRUE);

	CArray <INDEX> List;

	INDEX   Index = m_pList->GetHead();

	CString Match = L"";

	if( m_hSelect != m_hRoot ) {

		Match = m_pNamed->GetName();

		Match = Match + L'.';
		}

	while( !m_pList->Failed(Index) ) {

		if( Match.IsEmpty() ) {

			List.Append(Index);
			}
		else {
			CMetaItem *pItem = (CMetaItem *) m_pList->GetItem(Index);

			if( !wstrncmp(pItem->GetName(), Match, Match.GetLength()) ) {

				List.Append(Index);
				}
			}

		m_pList->GetNext(Index);
		}

	if( !List.IsEmpty() ) {

		CCmdOrder *pCmd = New CCmdOrder(m_pSelect);

		m_pList->GetOrder(pCmd->m_Old);

		UINT uCount = List.GetCount();

		qsort( PVOID(List.GetPointer()),
		       uCount,
		       sizeof(INDEX),
		       pfnSort
		       );

		for( UINT n = 0; n < uCount; n++ ) {

			m_pList->MoveItem(List[n], NULL);
			}

		if( m_pList->IsKindOf(AfxRuntimeClass(CNamedList)) ) {

			CNamedList *pNamed = (CNamedList *) m_pList;

			pNamed->RemakeIndex();
			}

		m_pList->GetOrder(pCmd->m_New);

		m_System.SaveCmd(pCmd);

		RefreshTree();

		ExpandItem(m_hSelect);

		m_System.ItemUpdated(m_pList, updateChildren);
		}

	afxThread->SetWaitMode(FALSE);
	}

void CNavTreeWnd::OnExecOrder(CCmdOrder *pCmd)
{
	UpdateWindow();

	afxThread->SetWaitMode(TRUE);

	m_pList->SetOrder(pCmd->m_New);

	RefreshTree();

	ExpandItem(m_hSelect);

	m_System.ItemUpdated(m_pList, updateChildren);

	afxThread->SetWaitMode(FALSE);
	}

void CNavTreeWnd::OnUndoOrder(CCmdOrder *pCmd)
{
	UpdateWindow();

	afxThread->SetWaitMode(TRUE);

	m_pList->SetOrder(pCmd->m_Old);

	RefreshTree();

	ExpandItem(m_hSelect);

	m_System.ItemUpdated(m_pList, updateChildren);

	afxThread->SetWaitMode(FALSE);
	}

// Item Menu : Find

void CNavTreeWnd::OnItemFind(void)
{
	// REV3 -- Use a "Find" dialog to gather options for CString::Search().

	CStringDialog Dlg;

	Dlg.SetCaption(CString(IDS_FIND_ITEMS_IN));

	Dlg.SetGroup  (CString(IDS_FIND));

	Dlg.SetData   (m_LastFind);

	if( Dlg.Execute() ) {

		CStringArray List;

		INDEX   Index = m_pList->GetHead();

		CString Find  = Dlg.GetData();

		while( !m_pList->Failed(Index) ) {

			CItem *pItem = m_pList->GetItem(Index);

			if( pItem->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

				CMetaItem *pMeta = (CMetaItem *) pItem;

				CString    Name  = pMeta->GetName();

				if( Name.Search(Find, searchContains) < NOTHING ) {

					CString Path = pItem->GetFixedPath();

					List.Append(Name + L"\n" + Path);
					}
				}

			m_pList->GetNext(Index);
			}

		if( List.IsEmpty() ) {

			Error(CString(IDS_TEXT_NOT_FOUND));
			}
		else {
			CSysProxy Proxy;

			Proxy.Bind();

			Proxy.SetFindList(CString(IDS_FIND_ITEMS), List, TRUE);

			m_LastFind = Find;
			}
		}
	}

// Item Menu : Private

void CNavTreeWnd::OnItemPrivate(void)
{
	UINT uPrivate = m_pSelect->GetPrivate();

	CCmd *pCmd = New CCmdPrivate(m_pSelect, uPrivate ? 0 : 1);

	m_System.ExecCmd(pCmd);
	}

// Edit Menu : Cut and Copy

void CNavTreeWnd::OnEditCut(void)
{
	if( WarnMultiple(IDS_CUT_1) ) {

		OnEditCopy();
	
		OnItemDelete(IDS_CUT_2, FALSE, FALSE);
		}
	}

void CNavTreeWnd::OnEditCopy(void)
{
	UpdateWindow();

	IDataObject *pData;

	if( MakeDataObject(pData, TRUE) ) {

		OleSetClipboard(pData);
		
		OleFlushClipboard();

		pData->Release();
		}
	}

// Edit Menu : Paste

BOOL CNavTreeWnd::CanEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		BOOL fLocal;

		if( CanAcceptDataObject(pData, fLocal) ) {

			pData->Release();

			return TRUE;
			}

		pData->Release();
		}

	return FALSE;
	}

void CNavTreeWnd::OnEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		BOOL fLocal;

		if( CanAcceptDataObject(pData, fLocal) ) {

			OnEditPaste(pData);
			}

		pData->Release();
		}
	}

void CNavTreeWnd::OnEditPaste(IDataObject *pData)
{
	HTREEITEM hRoot = m_pTree->GetParent(m_hSelect);

	HTREEITEM hPrev = m_hSelect;

	if( !hRoot ) {

		hRoot = m_hSelect;

		hPrev = m_hSelect;
		}
	else {
		if( m_pSelect->IsKindOf(m_Folder) ) {

			if( !m_pTree->GetChild(m_hSelect) ) {

				hRoot = m_hSelect;

				hPrev = m_hSelect;
				}
			}
		}

	OnEditPaste( m_pSelect,
		     hRoot,
		     hPrev,
		     pData
		     );
	}

void CNavTreeWnd::OnEditPaste(CItem *pItem, HTREEITEM hRoot, HTREEITEM hPrev, IDataObject *pData)
{
	CItem * pRoot = GetItemPtr(hRoot);

	CItem * pPrev = GetItemOpt(hPrev);

	CCmd  * pCmd  = New CCmdPaste(pItem, pRoot, pPrev, pData, FALSE);

	m_System.ExecCmd(pCmd);
	}

void CNavTreeWnd::OnExecPaste(CCmdPaste *pCmd)
{
	CAcceptCtx Ctx;

	Ctx.m_Root   = pCmd->m_Root;

	Ctx.m_Prev   = pCmd->m_Prev;

	Ctx.m_fUndel = FALSE;

	if( pCmd->m_fInit ) {

		Ctx.m_fPaste = TRUE;

		Ctx.m_fCheck = TRUE;

		Ctx.m_pNames = &pCmd->m_Names;

		AcceptDataObject(pCmd->m_pData, Ctx);

		pCmd->m_fInit = FALSE;

		pCmd->m_Menu  = CFormat(pCmd->m_Menu, m_pNamed->GetName());

		pCmd->m_pData->Release();

		MakeDataObject(pCmd->m_pData, FALSE);

		return;
		}

	Ctx.m_fPaste = FALSE;
		
	Ctx.m_fCheck = pCmd->m_fMove;

	Ctx.m_pNames = NULL;

	AcceptDataObject(pCmd->m_pData, Ctx);
	}

void CNavTreeWnd::OnUndoPaste(CCmdPaste *pCmd)
{
	UINT n = pCmd->m_Names.GetCount();

	while( n-- ) {

		CString    Base  = m_pList->GetFixedPath();

		CString    Name  = pCmd->m_Names[n];

		CString    Fixed = Base + L'/' + Name;

		HTREEITEM  hItem = m_MapFixed[Fixed];

		CMetaItem *pItem = GetItemPtr(hItem);

		if( pItem == m_pSelect ) {

			StepAway();
			}

		m_pList->DeleteItem(pItem);

		m_pKilled = pItem;

		KillTree(hItem);
		}

	ListUpdated();
	}

// Data Object Construction

BOOL CNavTreeWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	if( !m_fMulti ) {

		CDataObject *pMake = New CDataObject;

		if( m_pNamed ) {

			CTextStreamMemory Stream;

			if( Stream.OpenSave() ) {

				if( AddItemToStream(Stream, m_hSelect) ) {
		
					pMake->AddStream(m_cfData, Stream);
					}
				}
			}

		if( fRich ) {

			if( m_pNamed ) {

				pMake->AddText(m_pNamed->GetName());
				}
			}

		if( pMake->IsEmpty() ) {

			delete pMake;

			pData = NULL;

			return FALSE;
			}

		pData = pMake;

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::AddItemToStream(ITextStream &Stream, HTREEITEM hItem)
{
	CString F1   = m_pItem->GetDatabase()->GetUniqueSig();

	CString F2   = m_pList->GetFixedPath();

	CString F3   = CPrintf(L"%8.8X", m_hWnd);

	CString Head = CPrintf(L"%s|%s|%s\r\n", F1, F2, F3);

	Stream.PutLine(Head);

	CTreeFile Tree;

	if( Tree.OpenSave(Stream) ) {

		AddItemToTreeFile(Tree, hItem);

		Tree.Close();

		return TRUE;
		}

	return FALSE;
	}

void CNavTreeWnd::AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem)
{
	CItem * pItem = GetItemPtr(hItem);

	if( !(IsPrivate() && pItem->GetPrivate()) ) {

		CString Class = pItem->GetClassName();

		Tree.PutObject(Class.Mid(1));

		Tree.PutValue (L"NDX", pItem->GetIndex());

		pItem->PreCopy();

		pItem->Save(Tree);

		Tree.EndObject();

		if( (hItem = m_pTree->GetChild(hItem)) ) {

			while( hItem ) {

				AddItemToTreeFile(Tree, hItem);

				hItem = m_pTree->GetNext(hItem);
				}
			}
		}
	}

// Data Object Acceptance

BOOL CNavTreeWnd::CanAcceptDataObject(IDataObject *pData, BOOL &fLocal)
{
	if( CanAcceptStream(pData, fLocal) ) {
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::CanAcceptStream(IDataObject *pData, BOOL &fLocal)
{
	FORMATETC Fmt = { WORD(m_cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(Med.hGlobal) ) {

			if( CanAcceptStream(Stream, fLocal) ) {

				Stream.Close();

				ReleaseStgMedium(&Med);

				return TRUE;
				}
			}

		Stream.Close();

		ReleaseStgMedium(&Med);
		}

	return FALSE;
	}

BOOL CNavTreeWnd::CanAcceptStream(ITextStream &Stream, BOOL &fLocal)
{
	BOOL fAccept = FALSE;

	if( GetStreamInfo(Stream, fLocal) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				if( !Name.IsEmpty() ) {

					if( OnAcceptName(Tree, TRUE, Name) ) {

						CLASS Class = AfxNamedClass(PCTXT(L"C" + Name));

						if( !Class->IsKindOf(m_Class) ) {

							if( !Class->IsKindOf(m_Folder) ) {

								fAccept = FALSE;

								break;
								}
							}

						Tree.GetObject();

						while( !Tree.IsEndOfData() ) {

							Tree.GetName();
							}

						Tree.EndObject();

						fAccept = TRUE;
						}
					}
				}
			}
		}

	return fAccept;
	}

BOOL CNavTreeWnd::AcceptDataObject(IDataObject *pData, CAcceptCtx const &Ctx)
{
	if( AcceptStream(pData, Ctx) ) {

		ListUpdated();

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::AcceptStream(IDataObject *pData, CAcceptCtx const &Ctx)
{
	FORMATETC Fmt = { WORD(m_cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(Med.hGlobal) ) {

			BOOL fLocal = FALSE;

			if( GetStreamInfo(Stream, fLocal) ) {

				if( AcceptStream(Stream, Ctx, fLocal) ) {

					ReleaseStgMedium(&Med);

					return TRUE;
					}
				}
			}

		ReleaseStgMedium(&Med);
		}

	return FALSE;
	}

BOOL CNavTreeWnd::AcceptStream(ITextStream &Stream, CAcceptCtx const &Ctx, BOOL fLocal)
{
	CTreeFile Tree;

	if( Tree.OpenLoad(Stream) ) {

		OnAcceptInit(Tree);

		CArray <CItem *> Rename;

		HTREEITEM        hPrev = m_MapFixed[Ctx.m_Prev];

		HTREEITEM        hBase = NULL;

		CMetaItem  *     pBase = NULL;
		
		UINT             uBase = 0;

		CString          Root  = L"";

		HTREEITEM        hRoot = NULL;
		
		while( !Tree.IsEndOfData() ) {

			CString Name = Tree.GetName();

			if( !Name.IsEmpty() ) {

				if( OnAcceptName(Tree, fLocal, Name) ) {

					CLASS Class = AfxNamedClass(PCTXT(L"C" + Name));

					UINT  uSize = Class->GetObjectSize();

					PVOID pData = AfxMalloc(uSize, afxFile, afxLine, TRUE);

					if( pData == m_pKilled ) {

						// URGH -- Various things get confused during a move
						// if the new item has the same address as the one
						// we have just deleted, so this horrible hack avoids
						// the problem until I can fix it properly. Note we
						// do this with a block of memory rather than the
						// object itself as that is painful to construct and
						// tear-down without breaking other things.

						PVOID   pMore = AfxMalloc(uSize, afxFile, afxLine, TRUE);

						AfxFree(pData, TRUE);

						pData = pMore;
						}

					CMetaItem *pItem = AfxNewObjectAt( CMetaItem,
									   Class,
									   pData
									   );

					Tree.GetObject();

					Tree.GetName();

					UINT uIndex = Tree.GetValueAsInteger();

					pItem->SetIndex (uIndex);

					pItem->SetParent(m_pList);

					pItem->Load(Tree);

					Tree.EndObject();

					if( !pBase ) {

						hRoot = m_MapFixed[Ctx.m_Root];

						Name  = pItem->GetName();

						uBase = Name.GetLength();

						pBase = pItem;

						Root  = GetRoot(hRoot, TRUE);

						Name  = Root + GetName(Name);

						if( Ctx.m_fCheck ) {

							MakeUnique(Name);
							}

						pItem->SetName(Name);

						ExpandItem(hRoot);
						}
					else {
						Name  = pBase->GetName();

						Name += pItem->GetName().Mid(uBase);

						pItem->SetName(Name);

						Root  = GetRoot(Name, FALSE);

						hRoot = m_MapNames[Root];
						}

					AddToList( hRoot,
						   hPrev,
						   pItem
						   );

					if( !Ctx.m_fPaste ) {

						pItem->PostLoad();
						}
					else {
						pItem->PostPaste();

						pItem->PostLoad();
						}

					if( Ctx.m_pNames ) {

						AfxAssume(pItem);

						CString Fixed = pItem->GetFixedName();

						Ctx.m_pNames->Append(Fixed);
						}

					hPrev = AddToTree( hRoot,
							   hPrev,
							   pItem
							   );

					if( Ctx.m_fUndel ) {

						OnItemDeleted(pItem, FALSE);
						}
					else {
						// REV3 -- We say the item is renamed to
						// ensure that moves recompile the code
						// to reflect the new name. I guess we
						// should really just do this for moves.

						Rename.Append(pItem);
						}

					if( !hBase ) {

						hBase = hPrev;
						}
					}
				}
			}

		if( Ctx.m_fUndel ) {

			OnItemDeleted(NULL, FALSE);
			}

		for( UINT n = 0; n < Rename.GetCount(); n++ ) {

			OnItemRenamed(Rename[n]);
			}

		OnAcceptDone(Tree, hBase);
		
		m_pTree->SelectItem(hBase);

		return TRUE;
		}

	m_pTree->EnsureVisible(m_hSelect);		       

	return FALSE;
	}

BOOL CNavTreeWnd::GetStreamInfo(ITextStream &Stream, BOOL &fLocal)
{
	TCHAR sText[512] = { 0 };

	if( Stream.GetLine(sText, elements(sText)) ) {

		CStringArray List;

		CString(sText).Tokenize(List, '|');

		if( List[0] == m_pItem->GetDatabase()->GetUniqueSig() ) { 

			fLocal = TRUE;

			return TRUE;
			}

		fLocal = FALSE;

		return TRUE;
		}

	return FALSE;
	}

// Data Object Hooks

void CNavTreeWnd::OnAcceptInit(CTreeFile &Tree)
{
	}

BOOL CNavTreeWnd::OnAcceptName(CTreeFile &Tree, BOOL fLocal, CString Name)
{
	return TRUE;
	}

void CNavTreeWnd::OnAcceptDone(CTreeFile &Tree, HTREEITEM hItem)
{
	}

// Drag Hooks

DWORD CNavTreeWnd::FindDragAllowed(void)
{
	if( IsItemLocked(m_hSelect) ) {
		
		return DROPEFFECT_COPY;
		}

	return DROPEFFECT_COPY | DROPEFFECT_MOVE;
	}

// Drop Support

void CNavTreeWnd::DropTrack(CPoint Pos, BOOL fMove)
{
	m_DragPos = Pos;

	Pos   -= m_DragOffset;

	Pos.y += m_DragSize.cy / 2;

	m_pTree->ScreenToClient(Pos);

	if( m_pTree->GetClientRect().PtInRect(Pos) ) {

		HTREEITEM hItem = m_pTree->HitTestItem(Pos);

		HTREEITEM hRoot = NULL;

		HTREEITEM hPrev = NULL;

		if( m_uDrop == dropAccept ) {

			if( !hRoot && hItem == m_hRoot ) {

				hRoot = m_hRoot;

				hPrev = NULL;
				}

			if( !hRoot && hItem ) {

				CItem *pItem = GetItemPtr(hItem);

				if( pItem->IsKindOf(m_Folder) ) {

					if( !m_pTree->GetChild(hItem) || !m_pTree->IsExpanded(hItem) ) {
							
						CRect Rect = m_pTree->GetItemRect(hItem, TRUE);

						if( Pos.y > Rect.top + 8 ) {

							hRoot = hItem;

							hPrev = NULL;
							}
						}
					}
				}

			if( !hRoot ) {

				HTREEITEM hScan, hBack;

				if( hItem ) {
				
					hScan = m_pTree->GetPreviousVisible(hItem);

					hBack = m_pTree->GetParent(hItem);
					}
				else {
					hScan = hItem;

					hBack = NULL;

					WalkToLast(hScan);
					}

				if( hScan != hBack ) {

					while( !m_pTree->GetNext(hScan) ) {

						HTREEITEM hNext = m_pTree->GetParent(hScan);

						CRect     Rect  = m_pTree->GetItemRect(hScan, TRUE);
					
						if( hNext == m_hRoot || Pos.x >= Rect.left - 24 ) {

							hPrev = hScan;

							hRoot = hNext;

							break;
							}

						hScan = hNext;
						}
					}
				}

			if( !hRoot && hItem ) {

				hRoot = m_pTree->GetParent  (hItem);

				hPrev = m_pTree->GetPrevious(hItem);
				}
			}
		else {
			hRoot = hItem;
			
			hPrev = NULL;
			}

		if( m_hDropRoot != hRoot || m_hDropPrev != hPrev || m_fDropMove != fMove ) {

			ShowDrop(FALSE);

			m_hDropRoot = hRoot;

			m_hDropPrev = hPrev;

			m_fDropMove = fMove;

			if( IsExpandDrop() || m_uDrop == dropOther ) {

				SetTimer (m_timerSelect, 300);
				}
			else
				KillTimer(m_timerSelect);

			ShowDrop(TRUE);

			DropDebug();
			}
		}

	SetTimer(m_timerScroll, 10);
	}

BOOL CNavTreeWnd::DropDone(IDataObject *pData)
{
	if( !m_hDropRoot ) {

		m_hDropRoot = m_hRoot;
		}

	if( m_fDropLocal ) {

		if( IsValidDrop() ) {

			CString Menu;
				
			if( m_fDropMove ) {

				Menu.Format(IDS_MOVE_2, m_pNamed->GetName());

				MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

				OnItemDelete(IDS_MOVE_1, TRUE, FALSE);

				CItem     * pRoot = GetItemPtr(m_hDropRoot);

				CItem     * pPrev = GetItemOpt(m_hDropPrev);

				CCmdPaste * pCmd  = New CCmdPaste(m_pSelect, pRoot, pPrev, pData, TRUE);

				CAcceptCtx  Ctx;

				Ctx.m_Root   = pCmd->m_Root;

				Ctx.m_Prev   = pCmd->m_Prev;

				Ctx.m_fPaste = FALSE;

				Ctx.m_fCheck = TRUE;

				Ctx.m_fUndel = FALSE;

				Ctx.m_pNames = &pCmd->m_Names;

				AcceptDataObject(pCmd->m_pData, Ctx);

				pCmd->m_fInit = FALSE;

				m_System.SaveCmd(pCmd);
				}
			else {
				Menu.Format(IDS_COPY_2, m_pNamed->GetName());

				MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

				OnEditPaste( m_pSelect,
					     m_hDropRoot,
					     m_hDropPrev,
					     pData
					     );
				}

			MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

			return TRUE;
			}

		return FALSE;
		}

	OnEditPaste( m_pSelect,
		     m_hDropRoot,
		     m_hDropPrev,
		     pData
		     );

	return TRUE;
	}

void CNavTreeWnd::DropDebug(void)
{
/*	CMetaItem *pRoot = GetItemOpt(m_hDropRoot);

	CMetaItem *pPrev = GetItemOpt(m_hDropPrev);

	CString Root = pRoot ? (pRoot->IsKindOf(AfxRuntimeClass(CMetaItem)) ? pRoot->GetName() : L"Top") : L"Null";

	CString Prev = pPrev ? (pPrev->IsKindOf(AfxRuntimeClass(CMetaItem)) ? pPrev->GetName() : L"Top") : L"Null";

	CPrintf Text(L"Root=%s (%p)  Prev=%s (%p)", Root, pRoot, Prev, pPrev);

	afxMainWnd->SendMessage(WM_SETSTATUSBAR, WPARAM(PCTXT(Text)));
*/	}

void CNavTreeWnd::ShowDrop(BOOL fShow)
{
	if( m_hDropRoot ) {

		CRect Horz, Vert;

		HTREEITEM hItem;

		if( !m_hDropPrev ) {

			if( IsFolderDrop() || m_uDrop == dropOther ) {

				CTreeViewItem Node(m_hDropRoot);

				Node.SetStateMask(TVIS_DROPHILITED);

				Node.SetState(fShow ? TVIS_DROPHILITED : 0);

				m_pTree->SetItem(Node);

				m_pTree->UpdateWindow();

				return;
				}

			hItem = m_pTree->GetChild(m_hDropRoot);
			}
		else
			hItem = m_pTree->GetNext(m_hDropPrev);

		if( !hItem ) {

			HTREEITEM hDraw = m_hDropPrev;

			CRect     Root  = m_pTree->GetItemRect(hDraw, TRUE);

			WalkToLast(hDraw);

			Horz = m_pTree->GetItemRect(hDraw, TRUE);

			Horz.top    = Horz.bottom -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Root.left   - 20;

			Horz.right  = Root.right  +  8;
			}
		else {
			Horz = m_pTree->GetItemRect(hItem, TRUE);
		
			Horz.top    = Horz.top    -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Horz.left   - 20;

			Horz.right  = Horz.right  +  8;
			}

		if( m_hDropPrev ) {
			
			if( !hItem || hItem != m_pTree->GetNextVisible(m_hDropPrev) ) {

				if( ShowDropVert(m_hDropPrev) ) {

					Vert.left   = Horz.left   -  0;

					Vert.right  = Vert.left   +  2;

					Vert.bottom = Horz.top    -  0;

					Vert.top    = Vert.bottom - 16;
					}
				}
			}

		CClientDC DC(*m_pTree);

		CBrush Brush(afxColor(WHITE), afxColor(BLACK), 128);

		DC.Select(IsValidDrop() ? afxBrush(WHITE) : Brush);
		
		DC.PatBlt(Horz, PATINVERT);
		
		DC.PatBlt(Vert, PATINVERT);

		DC.Deselect();
		}
	}

BOOL CNavTreeWnd::ShowDropVert(HTREEITEM hItem)
{
	if( m_pTree->GetChild(hItem) ) {
		
		if( !m_pTree->GetItemState(hItem, TVIS_EXPANDED) ) {

			return FALSE;
			}
		}
	
	return TRUE;
	}

BOOL CNavTreeWnd::IsExpandDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
				}

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::IsFolderDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::IsDropMove(DWORD dwKeys, DWORD *pEffect)
{
	if( m_uDrop == dropAccept ) {

		if( !IsItemLocked(m_hDropRoot) ) {

			if( m_fDropLocal && !(dwKeys & MK_CONTROL) ) {

				*pEffect = DROPEFFECT_MOVE;

				return TRUE;
				}

			*pEffect = DROPEFFECT_COPY;

			return FALSE;
			}
		}

	if( m_uDrop == dropOther ) {

		// LATER -- Forward to view?

		*pEffect = DROPEFFECT_NONE;

		return FALSE;
		}

	*pEffect = DROPEFFECT_NONE;

	return FALSE;
	}

BOOL CNavTreeWnd::IsValidDrop(void)
{
	if( !IsExpandDrop() ) {

		if( m_fDropMove ) {

			if( m_pSelect->IsKindOf(m_Folder) ) {

				HTREEITEM hScan = m_hDropRoot;

				while( hScan ) {

					if( hScan == m_hSelect ) {

						return FALSE;
						}

					hScan = m_pTree->GetParent(hScan);
					}
				}

			if( m_fDropLocal ) {

				if( m_hDropRoot == m_pTree->GetParent(m_hSelect) ) {

					if( m_hDropPrev == m_pTree->GetPrevious(m_hSelect) ) {

						return FALSE;
						}

					if( m_hDropPrev == m_hSelect ) {

						return FALSE;
						}
					}
				}
			}
	
		return TRUE;
		}

	return TRUE;
	}

// Item Addition

void CNavTreeWnd::AddToList(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem)
{
	INDEX After  = NULL;

	INDEX Before = NULL;

	if( hPrev ) {

		if( m_pTree->GetParent(hPrev) != hRoot ) {

			hPrev = hRoot;
			}

		CMetaItem * pPrev = GetItemPtr(hPrev);

		INDEX       Index = m_pList->FindItemIndex(pPrev);

		CString     Name  = pPrev->GetName() + L'.';

		while( m_pList->GetNext(Index) ) {

			CMetaItem *pScan = (CMetaItem *) m_pList->GetItem(Index);

			CString    Basis = pScan->GetName().Left(Name.GetLength());

			if( Name == Basis ) {

				continue;
				}

			m_pList->GetPrev(Index);

			break;
			}

		After = Index;
		}
	else {
		if( hRoot == m_hRoot ) {

			HTREEITEM hNext = m_pTree->GetChild(hRoot);
				
			if( hNext ) {
				
				CMetaItem * pNext = GetItemPtr(hNext);

				INDEX       Index = m_pList->FindItemIndex(pNext);

				Before = Index;
				}
			else
				Before = m_pList->GetHead();
			}
		else {
			CMetaItem * pRoot = GetItemPtr(hRoot);

			INDEX       Index = m_pList->FindItemIndex(pRoot);

			After = Index;
			}
		}

	if( After ) {

		Before = After;

		m_pList->GetNext(Before);
		}
	
	m_pList->InsertItem(pItem, Before);
	}

HTREEITEM CNavTreeWnd::AddToTree(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	if( hPrev ) {

		if( m_pTree->GetParent(hPrev) != hRoot ) {

			hPrev = TVI_LAST;
			}
		}
	else
		hPrev = TVI_FIRST;

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, hPrev, Node);

	AddToMaps(pItem, hItem);

	return hItem;
	}

// Item Hooks

void CNavTreeWnd::NewItemSelected(void)
{
	if( m_pEmpty ) {

		if( m_pList->GetItemCount() ) {

			m_pEmpty->DestroyWindow(TRUE);

			m_pEmpty = NULL;
			}
		}

	if( !m_fLocked ) {

		SetViewedItem(FALSE);
		}
	}

BOOL CNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {
	
		Name = L"NavTreeCtxMenu";

		return TRUE;
		}

	return FALSE;
	}

// Item Locking

BOOL CNavTreeWnd::IsItemLocked(HTREEITEM hItem)
{
	return IsReadOnly() || IsItemPrivate(hItem);
	}

// Update Hook

void CNavTreeWnd::OnListUpdated(void)
{
	}

// Implementation

void CNavTreeWnd::NormalizeList(void)
{
	BOOL fDirty = m_pItem->GetDatabase()->IsDirty();
	
	HTREEITEM hItem = m_pTree->GetNextItem(m_hRoot, TVGN_CHILD);

	while( hItem ) {

		CMetaItem * pItem = GetItemPtr(hItem);

		m_pList->MoveItem(pItem, INDEX(NULL));

		hItem = m_pTree->GetNextNode(hItem, TRUE);
		}

	if( m_pList->IsKindOf(AfxRuntimeClass(CNamedList)) ) {

		CNamedList *pNamed = (CNamedList *) m_pList;

		pNamed->RemakeIndex();
		}

	if( !fDirty ) {

		m_pItem->GetDatabase()->ClearDirty();
		}
	}

BOOL CNavTreeWnd::SetMode(UINT uMode)
{
	if( m_uMode - uMode ) {

		switch( (m_uMode = uMode) ) {

			case modeSelect:

				m_pTree->SetPickMode(FALSE);

				break;

			case modePick:

				afxThread->SetStatusText(CString(IDS_SELECT_ITEM_FROM));

				m_pTree->SetPickMode(TRUE);

				break;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavTreeWnd::WarnMultiple(UINT uVerb)
{
	if( IsParent() ) {

		ExpandItem(m_hSelect);

		CString Verb = CString(uVerb);

		Verb.MakeLower();

		CString Text = CFormat(IDS_DO_YOU_WANT, Verb);

		return NoYes(Text) == IDYES;
		}

	return TRUE;
	}

BOOL CNavTreeWnd::MarkMulti(CString Item, CString Menu, BOOL fMark)
{
	if( fMark) {

		CCmd *pCmd = New CCmdMulti(Item, Menu, navOne);

		m_System.ExecCmd(pCmd);

		return TRUE;
		}

	return FALSE;
	}

void CNavTreeWnd::MakeUnique(CString &Name)
{
	if( m_MapNames[Name] ) {

		UINT c = Name.GetLength();

		UINT d = CNamedList::m_Disambig.GetLength();

		UINT n = 0;

		while( isdigit(Name[c - 1 - n]) ) {

			n++;
			}

		if( Name.Mid(c - n - d, d) == CNamedList::m_Disambig ) {
			
			Name = Name.Left(c - n - d);
			}

		CString r = Name + CNamedList::m_Disambig;

		UINT    v = 0;

		for(;;) { 

			Name = r + CPrintf(L"%u", ++v);
			
			if( !m_MapNames[Name] ) {

				break;
				}
			}
		}
	}

void CNavTreeWnd::WalkToLast(HTREEITEM &hItem)
{
	HTREEITEM hNext;

	while( (hNext = m_pTree->GetNext (hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;
		}

	while( (hNext = m_pTree->GetChild(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;

		WalkToLast(hItem);

		break;
		}
	}

void CNavTreeWnd::LockUpdate(BOOL fLock)
{
	if( fLock != m_fLocked ) {

		m_fLocked = fLock;

		if( fLock ) {

			m_pTree->SetRedraw(FALSE);
			}
		else {
			m_pTree->SetRedraw(TRUE);

			m_pTree->Invalidate(FALSE);

			SetViewedItem(FALSE);

			ListUpdated();

			m_pTree->UpdateWindow();
			}
		}
	}

void CNavTreeWnd::ListUpdated(void)
{
	if( !m_fLocked ) {

		m_System.ItemUpdated(m_pList, updateChildren);

		OnListUpdated();
		}
	}

void CNavTreeWnd::LockItem(BOOL fLock)
{
	CMetaData const *pData = m_pSelect->FindMetaData(L"Locked");

	if( pData ) {

		pData->WriteInteger(m_pSelect, fLock);

		m_pSelect->SetDirty();
		}
	}

void CNavTreeWnd::HideItem(BOOL fHide)
{
	CMetaData const *pData = m_pSelect->FindMetaData(L"Private");

	if( pData ) {

		pData->WriteInteger(m_pSelect, fHide);

		m_pSelect->SetDirty();
		}
	}

void CNavTreeWnd::SetViewedItem(BOOL fCheck)
{
	if( m_fMulti ) {

		m_System.SetViewedItem(NULL);
		}
	else {
		if( fCheck ) {

			m_System.SetNavCheckpoint();
			}

		m_System.SetViewedItem(m_pSelect);
		}
	}

BOOL CNavTreeWnd::IsSelLocked(void)
{
	if( m_fMulti ) {

		HTREEITEM hItem = m_pTree->GetFirstSelect();

		while( hItem ) {

			if( IsItemLocked(hItem) ) {

				return TRUE;
				}

			hItem = m_pTree->GetNextSelect(hItem);
			}
		
		return FALSE;
		}

	return IsItemLocked(m_hSelect);
	}

BOOL CNavTreeWnd::IsRootSelected(void)
{
	if( m_fMulti ) {

		if( m_pTree->IsSelected(m_hRoot) ) {

			return TRUE;
			}

		return FALSE;
		}

	return m_hSelect == m_hRoot;
	}

void CNavTreeWnd::CheckInitSel(void)
{
	if( m_pSelect->IsKindOf(m_Folder) ) {

		HTREEITEM hItem = m_hSelect;

		for(;;) {

			hItem = m_pTree->GetNextNode(hItem, TRUE);

			if( hItem ) {

				CMetaItem *pItem = GetItemPtr(hItem);

				if( pItem->IsKindOf(m_Class) ) {

					m_pTree->SelectItem(hItem);

					break;
					}

				continue;
				}

			break;
			}
		}
	}

void CNavTreeWnd::GetEmptyRect(CRect &Rect)
{
	Rect = m_pTree->GetClientRect();

	Rect.top    = Rect.top   + 64;

	Rect.bottom = Rect.top   + 256;

	int xMargin = 32;

	if( Rect.cx() < 208 ) {

		xMargin = 8;
		}
	else {
		if( Rect.cx() < 256 ) {

			xMargin = (Rect.cx() - 192) / 2;
			}
		}

	Rect.left   = Rect.left  + xMargin;

	Rect.right  = Rect.right - xMargin;
	}

BOOL CNavTreeWnd::IsItemPrivate(HTREEITEM hItem)
{
	if( IsPrivate() ) {

		CMetaItem *pItem = GetItemOpt(hItem);

		if( pItem ) {

			return pItem->GetPrivate();
			}
		}

	return FALSE;
	}

// Sort Function

int CNavTreeWnd::SortByName(PCVOID p1, PCVOID p2)
{
	INDEX i1 = *((INDEX *) p1);

	INDEX i2 = *((INDEX *) p2);

	CMetaItem *m1 = (CMetaItem *) m_pSort->m_pList->GetItem(i1);

	CMetaItem *m2 = (CMetaItem *) m_pSort->m_pList->GetItem(i2);

	int f1 = m1->IsKindOf(m_pSort->m_Folder);

	int f2 = m2->IsKindOf(m_pSort->m_Folder);

	if( f1 == f2 ) {

		CString n1 = m1->GetName();

		CString n2 = m2->GetName();

		UINT    l1 = n1.GetLength();

		UINT    l2 = n2.GetLength();

		UINT d1, d2;

		for( d1 = 0;; d1++ ) {

			if( !isdigit(n1[l1-1-d1]) ) {

				break;
				}
			}

		for( d2 = 0;; d2++ ) {

			if( !isdigit(n2[l2-1-d2]) ) {

				break;
				}
			}

		if( l1 - d1 == l2 - d2 ) {
			
			int q1 = l1 - d1;

			int q2 = l2 - d2;

			if( n1.Left(q1) == n2.Left(q2) ) {

				int v1 = watoi(PCTXT(n1) + q1);

				int v2 = watoi(PCTXT(n2) + q2);

				if( v1 < v2 ) return -m_nSort;

				if( v1 > v2 ) return +m_nSort;

				return 0;
				}
			}

		return m_nSort * lstrcmp(n1, n2);
		}

	return (f1 < f2 ) ? +1 : -1;
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Create Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdCreate, CStdCmd);

// Constructor

CNavTreeWnd::CCmdCreate::CCmdCreate(CItem *pParent, CString Name, CLASS Class)
{
	m_Menu  = IDS_CREATE;

	m_Item  = pParent->GetFixedPath();

	m_Name  = Name;

	m_Class = Class;

	m_hData = NULL;

	m_Fixed = pParent->GetDatabase()->AllocFixedString();
	}

CNavTreeWnd::CCmdCreate::CCmdCreate(CItem *pParent, CMetaItem *pItem)
{
	m_Menu  = CFormat(IDS_CREATE, pItem->GetName());

	m_Item  = pParent->GetFixedPath();

	m_Class = NULL;

	m_hData = pItem->TakeSnapshot();

	m_Make  = pItem->GetFixedPath();
	}

// Destructor

CNavTreeWnd::CCmdCreate::~CCmdCreate(void)
{
	if( m_hData ) {

		GlobalFree(m_hData);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdRename, CStdCmd);

// Constructor

CNavTreeWnd::CCmdRename::CCmdRename(CMetaItem *pItem, CString Name)
{
	m_Menu = CFormat(IDS_RENAME, pItem->GetName());

	m_Item = pItem->GetFixedPath();

	m_Prev = pItem->GetName();

	m_Name = Name;
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Delete Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdDelete, CStdCmd);

// Constructor

CNavTreeWnd::CCmdDelete::CCmdDelete(CString Menu, CString Item, CItem *pRoot, CItem *pPrev, IDataObject *pData, BOOL fMove)
{
	m_Menu  = Menu;

	m_Item  = Item;

	m_Root  = GetFixedPath(pRoot);

	m_Prev  = GetFixedPath(pPrev);

	m_pData = pData;

	m_fMove = fMove;
	}

// Destructor

CNavTreeWnd::CCmdDelete::~CCmdDelete(void)
{
	m_pData->Release();
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Paste Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdPaste, CStdCmd);

// Constructor

CNavTreeWnd::CCmdPaste::CCmdPaste(CItem *pItem, CItem *pRoot, CItem *pPrev, IDataObject *pData, BOOL fMove)
{
	m_Menu  = IDS_PASTE;

	m_Item  = pItem->GetFixedPath();

	m_Root  = GetFixedPath(pRoot);

	m_Prev  = GetFixedPath(pPrev);

	m_pData = pData;

	m_pData->AddRef();

	m_fMove = fMove;

	m_fInit = TRUE;
	}

// Destructor

CNavTreeWnd::CCmdPaste::~CCmdPaste(void)
{
	m_pData->Release();
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Lock Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdLock, CStdCmd);

// Constructor

CNavTreeWnd::CCmdLock::CCmdLock(CMetaItem *pItem, BOOL fLock)
{
	m_Menu  = CFormat(fLock ? IDS_LOCK : IDS_UNLOCK, pItem->GetName());

	m_Item  = pItem->GetFixedPath();

	CMetaData const *pData = pItem->FindMetaData(L"Locked");

	m_fPrev = pData ? pData->ReadInteger(pItem) : FALSE;

	m_fLock = fLock;
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Order Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdOrder, CStdCmd);

// Constructor

CNavTreeWnd::CCmdOrder::CCmdOrder(CMetaItem *pItem)
{
	m_Menu = CString(IDS_SORT_LIST);

	m_Item = pItem->GetFixedPath();
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Private Command
//

// Runtime Class

AfxImplementRuntimeClass(CNavTreeWnd::CCmdPrivate, CStdCmd);

// Constructor

CNavTreeWnd::CCmdPrivate::CCmdPrivate(CMetaItem *pItem, BOOL fHide)
{
	m_Menu = CFormat(CString(IDS_MAKE_FMT_PRIVATE), pItem->GetName());

	m_Item = pItem->GetFixedPath();

	CMetaData const *pData = pItem->FindMetaData(L"Private");

	m_fPrev = pData ? pData->ReadInteger(pItem) : FALSE;

	m_fHide = fHide;
	}

// End of File
