
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Navigation Pane Window
//

// Runtime Class

AfxImplementRuntimeClass(CNavPaneWnd, CCatPaneWnd);

// Static Data

UINT CNavPaneWnd::m_timerSelect = AllocTimerID();

// Constructor

CNavPaneWnd::CNavPaneWnd(void)
{
	m_Title   = IDS_NAVIGATION_PANE;

	m_Prompt  = IDS_SELECT_THE_1;

	m_ShowID  = IDM_VIEW_SHOW_LEFT;

	m_KeyName = L"NavPane";

	m_uBase   = IDM_GO_SELECT + 0x00;

	m_pView   = New CItemViewWnd(viewNavigation, viewCache);

	m_uDrop   = NOTHING;

	m_Accel1.Create(L"NavPaneAccel1");

	m_Accel2.Create(L"NavPaneAccel2");
	}

// Attributes

CItem * CNavPaneWnd::GetNavItem(void) const
{
	return m_pView->GetItem();
	}

CString CNavPaneWnd::GetResList(void) const
{
	return m_Cats[m_uSelect].m_Res;
	}

// IUnknown

HRESULT CNavPaneWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CNavPaneWnd::AddRef(void)
{
	return 1;
	}

ULONG CNavPaneWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CNavPaneWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	CPoint Pos(pt.x, pt.y);

	SetDrop(Pos);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CNavPaneWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	CPoint Pos(pt.x, pt.y);

	SetDrop(Pos);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CNavPaneWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	m_System.HidePaneOnDrop(0);

	SetDrop(NOTHING);

	return S_OK;
	}

HRESULT CNavPaneWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	SetDrop(NOTHING);

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Overridables

void CNavPaneWnd::OnAttach(void)
{
	CDatabase *pDbase = (CDatabase *) m_pItem;

	CATLIST    List   = pDbase->GetCatList();

	UINT       uCount = List.GetCount();

	for( UINT c = 0; c < uCount; c++ ) {

		RCAT Cat = List[c];

		if( Cat.m_Res.GetLength() ) {

			m_Cats.Append(Cat);

			m_uCount++;
			}
		}

	LoadConfig();

	CPaneWnd::OnAttach();
	}

CString CNavPaneWnd::OnGetNavPos(void)
{
	return m_pView->GetNavPos();
	}

BOOL CNavPaneWnd::OnNavigate(CString const &Nav)
{
	CString Path = Nav.TokenLeft(':');

	CString Find = Path;

	for(;;) {

		if( !Select(Find) ) {

			UINT uPos = Find.FindRev('/');

			if( uPos < NOTHING ) {

				Find = Find.Left(uPos);

				continue;
				}

			break;
			}

		return m_pView->Navigate(Path);
		}

	return FALSE;
	}

void CNavPaneWnd::OnExec(CCmd *pCmd)
{
	m_pView->ExecCmd(pCmd);
	}

void CNavPaneWnd::OnUndo(CCmd *pCmd)
{
	m_pView->UndoCmd(pCmd);
	}

// Message Map

AfxMessageMap(CNavPaneWnd, CCatPaneWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchGetInfoType(IDM_GO, OnGoGetInfo)
	AfxDispatchControlType(IDM_GO, OnGoControl)
	AfxDispatchCommandType(IDM_GO, OnGoCommand)

	AfxDispatchCommand(IDM_VIEW_REFRESH, OnViewRefresh)

	AfxMessageEnd(CNavPaneWnd)
	};

// Message Handlers

void CNavPaneWnd::OnPostCreate(void)
{
	m_System.Bind(this);

	// Force early creation of any category windows.

	for( UINT n = 0; n < m_uCount; n++ ) {

		CMetaItem *pItem = FindCatItem(n);

		OnSetCategory(pItem);
		}

	CCatPaneWnd::OnPostCreate();

	m_DropHelp.Bind(m_hWnd, this);
	}

void CNavPaneWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		UINT uLast = Menu.GetMenuItemID(Menu.GetMenuItemCount() - 1);

		if( uLast < 0x80 ) {

			for( UINT n = 0; n < m_uCount; n++ ) {

				CFormat Text(IDS_TCTRLU, CString(m_Cats[n].m_Name), n);
				
				Menu.AppendMenu(0, m_uBase + n, Text);
				}

			Menu.AppendSeparator();

			Menu.AppendMenu(0, IDM_GO_PREV_CAT, CString(IDS_PREVIOUS));

			Menu.AppendMenu(0, IDM_GO_NEXT_CAT, CString(IDS_NEXT));

			Menu.MakeOwnerDraw(FALSE);
			}
		}
	}

void CNavPaneWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerSelect ) {

		if( m_uDrop < NOTHING ) {

			Select(m_uDrop);
			}

		KillTimer(uID);
		}

	CCatPaneWnd::OnTimer(uID, pfnProc);
	}

// Command Handlers

BOOL CNavPaneWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_GO_PREV_CAT ) {

		Src.EnableItem(m_uSelect > 0);

		return TRUE;
		}

	if( uID == IDM_GO_NEXT_CAT ) {

		Src.EnableItem(m_uSelect < m_uCount - 1);

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavPaneWnd::OnGoCommand(UINT uID)
{
	if( uID == IDM_GO_PREV_CAT ) {

		Select(m_uSelect-1);

		return TRUE;
		}

	if( uID == IDM_GO_NEXT_CAT ) {

		Select(m_uSelect+1);

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavPaneWnd::OnViewRefresh(UINT uID)
{
	m_Title  = IDS_NAVIGATION_PANE;

	m_Prompt = IDS_SELECT_THE_1;

	OnUpdateUI();

	return FALSE;
	}

// Category Selection

void CNavPaneWnd::OnSetCategory(CItem *pItem)
{
	CSysProxy Proxy;
	
	Proxy.Bind();

	Proxy.SetNavCategory(pItem);
	}

// Implementation

void CNavPaneWnd::SetDrop(CPoint Pos)
{
	ScreenToClient(Pos);

	UINT uDrop = CheckHover(Pos);

	if( uDrop ) {

		SetDrop(uDrop - 100);

		return;
		}

	SetDrop(NOTHING);
	}

void CNavPaneWnd::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		if( m_uDrop < NOTHING ) {

			InvalidateCat(m_uHover);
			}

		if( (m_uDrop = uDrop) < NOTHING ) {
			
			m_uHover = 100 + uDrop;

			SetTimer(m_timerSelect, 300);
			}
		else {
			m_uHover = 0;

			KillTimer(m_timerSelect);
			}	

		if( m_uDrop < NOTHING ) {

			InvalidateCat(m_uHover);
			}
		}
	}

// End of File
