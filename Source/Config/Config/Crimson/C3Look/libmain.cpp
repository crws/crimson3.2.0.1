
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Font Helpers
//

static void AddFonts(void)
{
	CFilename Name = afxModule->GetFilename();

	Name.ChangeName(L"rlc31.ttf");

	AddFontResourceEx(Name, FR_PRIVATE, NULL);
	}

static void DelFonts(void)
{
	CFilename Name = afxModule->GetFilename();

	Name.ChangeName(L"rlc31.ttf");

	RemoveFontResourceEx(Name, FR_PRIVATE, NULL);
	}

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load C3Look\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			AddFonts();

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading C3Look\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		DelFonts();

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
