
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Resource Tree Window
//

// Runtime Class

AfxImplementRuntimeClass(CResTreeWnd, CDotTreeWnd);

// Constructor

CResTreeWnd::CResTreeWnd(CString List, CLASS Folder, CLASS Class)
{
	m_List	  = List;

	m_Folder  = Folder;

	m_Class   = Class;

	m_fJumpTo = TRUE;

	m_cfIdent = RegisterClipboardFormat(L"C3.1 Identifier");

	m_pTree->SetDeferred(TRUE);
	}

// IUpdate

HRESULT CResTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		if( pItem == m_pList ) {

			// OPTIM -- This is really slow for Honeywell
			// as they have so many bloody tags. We need
			// to figure out how to avoid refreshing too
			// much of the tree...

			RefreshTree();
			}
		}

	return CDotTreeWnd::ItemUpdated(pItem, uType);
	}

// Overridables

BOOL CResTreeWnd::OnNavigate(CString const &Nav)
{
	if( !Nav.IsEmpty() ) {

		HTREEITEM hFind = m_MapFixed[Nav];

		if( hFind ) {

			m_pTree->SelectItem(hFind);

			return TRUE;
			}
		}

	return FALSE;
	}

// Message Map

AfxMessageMap(CResTreeWnd, CDotTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_TIMER)
	
	AfxDispatchNotify(100, TVN_KEYDOWN, OnTreeKeyDown )
	AfxDispatchNotify(100, NM_DBLCLK,   OnTreeDblClk  )

	AfxDispatchGetInfoType(IDM_RES, OnResGetInfo)
	AfxDispatchControlType(IDM_RES, OnResControl)
	AfxDispatchCommandType(IDM_RES, OnResCommand)

	AfxMessageEnd(CResTreeWnd)
	};

// Message Handlers

void CResTreeWnd::OnPostCreate(void)
{
	CDotTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
	}

void CResTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ResTreeTool"));
		}
	}

void CResTreeWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerDouble ) {
			
		m_System.SendPaneCommand(IDM_EDIT_PASTE);

		KillTimer(uID);
		}
	}

// Notification Handlers

BOOL CResTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_TAB:

			m_System.FlipToNavPane(TRUE);

			return TRUE;			
		}

	return CDotTreeWnd::OnTreeKeyDown(uID, Info);
	}

void CResTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( !m_pTree->GetChild(m_hSelect) ) {

			// TODO -- Multiple?

			IDataObject *pData = NULL;

			if( MakeDataObject(pData) ) {

				OleSetClipboard(pData);
			
				OleFlushClipboard();

				pData->Release();

				SetTimer(m_timerDouble, 5);
				}

			return;
			}

		ToggleItem(m_hSelect);
		}
	}

// Command Handlers

BOOL CResTreeWnd::OnResGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_RES_JUMP,   MAKELONG(0x0023, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CResTreeWnd::OnResControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_RES_JUMP:

			Src.EnableItem(m_fJumpTo && m_hSelect != m_hRoot);

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CResTreeWnd::OnResCommand(UINT uID)
{
	switch( uID ) {

		case IDM_RES_JUMP:

			m_System.Navigate(m_pSelect->GetFixedPath());

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Data Object Construction

BOOL CResTreeWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddIdentifier(pMake);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CResTreeWnd::AddIdentifier(CDataObject *pData)
{
	if( !m_fMulti ) {

		if( m_pNamed ) {

			CString F1   = m_pItem->GetDatabase()->GetUniqueSig();

			CString F2   = m_Class->GetClassName();

			CString F3   = m_pNamed->GetName();

			CString F4   = CPrintf(L"%8.8X", m_pNamed);

			CString F5   = CPrintf(L"%8.8X", PDWORD(m_pNamed)[0]);

			CString Head = CPrintf(L"%s|%s|%s|%s|%s", F1, F2, F3, F4, F5);

			pData->AddText(m_cfIdent, Head);

			return TRUE;
			}
		}

	return FALSE;
	}

// Drag Hooks

DWORD CResTreeWnd::FindDragAllowed(void)
{
	return DROPEFFECT_COPY | DROPEFFECT_LINK;
	}

// Item Hooks

void CResTreeWnd::NewItemSelected(void)
{
	}

BOOL CResTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {
	
		Name = L"ResTreeCtxMenu";

		return TRUE;
		}

	return FALSE;
	}

// End of File
