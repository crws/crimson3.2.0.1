
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Item Dialog Window
//

// Runtime Class

AfxImplementRuntimeClass(CItemDialog, CDialog);

// Constructors

CItemDialog::CItemDialog(CItem *pItem)
{
	m_pItem	  = pItem;

	m_uView   = viewItem;

	m_fRead   = m_pItem->GetDatabase()->IsReadOnly();

	Construct();
	}

CItemDialog::CItemDialog(CItem *pItem, CString Caption)
{
	m_pItem	  = pItem;

	m_Caption = Caption;

	m_uView   = viewItem;

	m_fRead   = m_pItem->GetDatabase()->IsReadOnly();

	Construct();
	}

CItemDialog::CItemDialog(CItem *pItem, CString Caption, UINT uView)
{
	m_pItem	  = pItem;

	m_Caption = Caption;

	m_uView   = uView;

	m_fRead   = m_pItem->GetDatabase()->IsReadOnly();

	Construct();
	}

// Creation

BOOL CItemDialog::Create(CWnd &Parent)
{
	DWORD dwStyle   = WS_POPUP | WS_BORDER | WS_DLGFRAME | WS_SYSMENU | WS_CLIPCHILDREN;

	DWORD dwExStyle = WS_EX_DLGMODALFRAME;

	CWnd::Create( m_Caption,
		      dwExStyle, dwStyle,
		      CRect(0, 0, 1000, 1000),
		      Parent,
		      0,
		      NULL
		      );

	return TRUE;
	}

// Attributes

BOOL CItemDialog::IsReadOnly(void) const
{
	return m_fRead;
	}

// Operations

void CItemDialog::Resize(void)
{
	FindBestSize();

	PlaceDialogCentral();
	}

void CItemDialog::ForceReadOnly(void)
{
	if( !m_fRead ) {

		delete m_pButton2;

		m_pButton2 = NULL;

		m_Caption += CString(IDS_LOCKED);

		m_fRead    = TRUE;
		}
	}

// Class Definition

PCTXT CItemDialog::GetDefaultClassName(void) const
{
	return L"CrimsonDlg";
	}

BOOL CItemDialog::GetClassDetails(WNDCLASSEX &Class) const
{
	if( CDialog::GetClassDetails(Class) ) {

		if( FALSE ) {

			Class.hIcon   = HICON(afxMainWnd->GetClassLong(GCL_HICON));

			Class.hIconSm = HICON(afxMainWnd->GetClassLong(GCL_HICONSM));
			}

		return TRUE;
		}

	return FALSE;
	}

// Translation Hook

BOOL CItemDialog::Translate(MSG &Msg)
{
	if( Msg.message == WM_KEYDOWN || Msg.message == WM_KEYUP ) {

		if( Msg.wParam == VK_RETURN ) {

			if( GetKeyState(VK_CONTROL) & 0x8000 ) {

				if( Msg.message == WM_KEYDOWN ) {

					PostMessage(WM_COMMAND, IDOK);
					}

				return TRUE;
				}

			CWnd &Wnd = GetFocus();

			if( Wnd.IsKindOf(AfxRuntimeClass(CButton)) ) {

				if( Wnd.GetID() < 10 ) {

					return IsDialogMessage(Msg);
					}
				}

			return FALSE;
			}
		}

	return IsDialogMessage(Msg);
	}

// Routing Control

BOOL CItemDialog::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView ) {

		if( m_pView->RouteMessage(Message, lResult) ) {

			return TRUE;
			}

		#if defined(_DEBUG)

		if( Message.message == WM_AFX_COMMAND ) {

			if( Message.wParam == IDM_VIEW_REFRESH ) {

				CWnd *  pPrev = m_pView;

				CString Field = m_pView->GetNavPos();

				m_pView       = m_pItem ? m_pItem->CreateView(m_uView) : NULL;

				CreateView();

				FindBestSize();

				PlaceDialogCentral();

				m_pView->Navigate(Field);

				pPrev->DestroyWindow(TRUE);
				}
			}

		#endif
		}

	return CDialog::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CItemDialog, CDialog)
{
	AfxDispatchCommand(IDOK,     OnOkay  )
	AfxDispatchCommand(IDCANCEL, OnCancel)

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_GETMINMAXINFO)

	AfxDispatchCommand(IDM_VIEW_REFRESH, OnViewRefresh)

	AfxMessageEnd(CItemDialog)
	};

// Command Handlers

BOOL CItemDialog::OnOkay(UINT uID)
{
	SetDlgFocus(uID);

	FreeData();

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CItemDialog::OnCancel(UINT uID)
{
	SetDlgFocus(uID);

	LoadData();

	FreeData();

	EndDialog(FALSE);

	return TRUE;
	}

// Message Handlers

void CItemDialog::OnPostCreate(void)
{
	EditSystemMenu();

	SaveData();

	FindLayout();

	CreateButtons();

	CreateView();

	FindBestSize();

	PlaceDialogCentral();
	}

void CItemDialog::OnClose(void)
{
	OnCancel(IDCANCEL);
	}

void CItemDialog::OnCancelMode(void)
{
	m_pView->SendMessage(WM_CANCELMODE);
	}

void CItemDialog::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
	}

void CItemDialog::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID == IDOK || uID == IDCANCEL ) {

		m_pView->SendMessage(WM_CANCELMODE);
		}
	}

void CItemDialog::OnSize(UINT uCode, CSize Size)
{
	m_pView->MoveWindow(GetViewRect(), TRUE);

	MoveButtons();
	}

void CItemDialog::OnGetMinMaxInfo(MINMAXINFO &Info)
{
/*	if( m_pView ) {

		MINMAXINFO Work;

		memset(&Work, 0, sizeof(Work));

		m_pView->SendMessage(WM_GETMINMAXINFO, 0, LPARAM(&Work));

		CSize MinSize = CSize(Work.ptMinTrackSize);

		CSize MaxSize = CSize(Work.ptMaxTrackSize);

		AdjustWindowSize(MinSize);
		
		AdjustWindowSize(MaxSize);

		MakeMin(MinSize.cy, 550);

		MakeMin(MaxSize.cy, 550);

		MinSize.cy += m_Button.cy + m_Border.top + m_Border.bottom;

		MinSize.cx += m_xExtra;

		MaxSize.cx += m_xExtra;

		Info.ptMinTrackSize = CPoint(MinSize);
		
		Info.ptMaxTrackSize = CPoint(MaxSize);
		}
*/	}

// View Menu

BOOL CItemDialog::OnViewRefresh(UINT uID)
{
	if( m_fRead ) {

		m_pButton1->SetWindowText(CString(IDS_CLOSE));
		}
	else {
		m_pButton1->SetWindowText(CString(IDS_OK));

		m_pButton2->SetWindowText(CString(IDS_CANCEL));
		}

	return FALSE;
	}

// Implementation

void CItemDialog::Construct(void)
{
	m_fTranslate = FALSE;

	m_pView      = m_pItem ? m_pItem->CreateView(m_uView) : NULL;

	m_fLightBack = UseLightBack();

	m_pButton1   = New CButton;

	m_pButton2   = m_fRead ? NULL : New CButton;

	m_hData      = NULL;

	m_xExtra     = 0;

	m_yLimit     = 0;
	}

BOOL CItemDialog::UseLightBack(void)
{
	if( m_pView ) {
		
		if( !m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CItemDialog::EditSystemMenu(void)
{
	CMenu &Menu = GetSystemMenu();

	Menu.DeleteMenu(SC_MAXIMIZE, MF_BYCOMMAND);
	
	Menu.DeleteMenu(SC_MINIMIZE, MF_BYCOMMAND);
	
	Menu.DeleteMenu(SC_RESTORE,  MF_BYCOMMAND);
	
	Menu.DeleteMenu(SC_SIZE,     MF_BYCOMMAND);
	}

void CItemDialog::FindLayout(void)
{
	if( m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

		CMultiViewWnd *pView = (CMultiViewWnd *) m_pView;

		pView->SetMargin(8);

		m_Border.left   = 8;

		m_Border.top    = 2;

		m_Border.right  = 8;

		m_Border.bottom = 8;

		m_Button.cx     = 64;

		m_Button.cy     = 24;
		}
	else {
		m_Border.left   = 8;

		m_Border.top    = 0;

		m_Border.right  = 0;

		m_Border.bottom = 16;

		m_Button.cx     = 64;

		m_Button.cy     = 24;
		}
	}

void CItemDialog::CreateView(void)
{
	UINT  uID       = IDVIEW;
	
	DWORD dwStyle   = WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE;

	DWORD dwExStyle = WS_EX_CONTROLPARENT;

	m_pView->Attach(m_pItem);

	m_pView->Create(dwExStyle, dwStyle, GetViewRect(), ThisObject, uID, NULL);
	}

void CItemDialog::CreateButtons(void)
{
	if( m_fRead ) {

		m_pButton1->Create( CString(IDS_CLOSE),
				    WS_TABSTOP | WS_VISIBLE | BS_DEFPUSHBUTTON,
				    CRect(),
				    ThisObject,
				    IDOK
				    );

		m_pButton1->SetFont(afxFont(Dialog));
		}
	else {
		m_pButton1->Create( CString(IDS_OK),
				    WS_TABSTOP | WS_VISIBLE | BS_DEFPUSHBUTTON,
				    CRect(),
				    ThisObject,
				    IDOK
				    );

		m_pButton2->Create( CString(IDS_CANCEL),
				    WS_TABSTOP | WS_VISIBLE | BS_PUSHBUTTON,
				    CRect(),
				    ThisObject,
				    IDCANCEL
				    );

		m_pButton1->SetFont(afxFont(Dialog));

		m_pButton2->SetFont(afxFont(Dialog));
		}
	}

void CItemDialog::FindBestSize(void)
{
	MINMAXINFO Work;

	memset(&Work, 0, sizeof(Work));

	m_pView->SendMessage(WM_GETMINMAXINFO, 0, LPARAM(&Work));

	CSize MinSize = CSize(Work.ptMinTrackSize);

	CSize MaxSize = CSize(Work.ptMaxTrackSize);

	CSize Size    = CSize(MinSize.cx, MaxSize.cy);

	MakeMin(Size.cy, 620);

	MakeMax(Size.cy, m_yLimit);

	MakeMax(Size.cx, m_Button.cx * 3);

	Size.cy += m_Button.cy + m_Border.top + m_Border.bottom;

	Size.cx += m_xExtra;

	AdjustWindowSize(Size);

	CRect Screen;

	SystemParametersInfo( SPI_GETWORKAREA,
			      0,
			      &Screen,
			      0
			      );

	MakeMin(Size.cy, Screen.cy());

	SetWindowSize(Size, FALSE);
	}

CRect CItemDialog::GetViewRect(void)
{
	CRect Rect   = GetClientRect();

	Rect.right  -= m_xExtra;

	Rect.bottom -= m_Button.cy + m_Border.top + m_Border.bottom;

	return Rect;
	}

void CItemDialog::MoveButtons(void)
{
	CRect Rect = GetClientRect();

	if( m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

		Rect.bottom = Rect.bottom - m_Border.bottom;
		}
	else
		Rect.bottom = Rect.bottom - m_Border.bottom / 2;

	Rect.top    = Rect.bottom - m_Button.cy;

	Rect.left   = Rect.left   + m_Border.left;

	Rect.right  = Rect.left   + m_Button.cx;

	m_pButton1->MoveWindow(Rect, TRUE);

	if( m_pButton2 ) {

		Rect.left   = Rect.right  + 2 * m_Border.left / 3;

		Rect.right  = Rect.left   + m_Button.cx;

		m_pButton2->MoveWindow(Rect, TRUE);
		}
	}

void CItemDialog::SaveData(void)
{
	if( !m_fRead ) {

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			if( TRUE ) {

				CTreeFile Tree;

				if( Tree.OpenSave(Stream) ) {

					Tree.PutObject(L"Item");

					m_pItem->Save(Tree);

					Tree.EndObject();
					}
				}

			m_hData = Stream.TakeOver();
			}
		}
	}

void CItemDialog::LoadData(void)
{
	if( !m_fRead ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(m_hData) ) {

			CTreeFile Tree;

			if( Tree.OpenLoad(Stream) ) {

				if( Tree.GetName() == L"Item" ) {

					Tree.GetObject();

					m_pItem->Kill();

					m_pItem->Load(Tree);

					m_pItem->PostLoad();

					Tree.EndObject();
					}

				Tree.Close();
				}
			}
		}
	}

void CItemDialog::FreeData(void)
{
	if( !m_fRead ) {

		GlobalFree(m_hData);
		}
	}

// End of File
