
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Empty Tree Prompt Window
//

// Runtime Class

AfxImplementRuntimeClass(CEmptyPromptWnd, CWnd);

// Constructor

CEmptyPromptWnd::CEmptyPromptWnd(void)
{
	}

// Message Map

AfxMessageMap(CEmptyPromptWnd, CWnd)
{
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_SETFOCUS)

	AfxMessageEnd(CEmptyPromptWnd)
	};

// Message Handlers

void CEmptyPromptWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.Select(afxFont(Large));

	CString Text = GetWindowText();

	CRect   Rect = GetClientRect();
	
	CRect   Calc = Rect;

	DC.DrawText(Text, Calc,  DT_CENTER | DT_CALCRECT | DT_WORDBREAK);

	if( Calc.cx() <= Rect.cx() ) {
		
		// Loop around narrowing the rectangle as long as we end
		// up with the same output height. This squares up the
		// presentation and avoids orphaned words on last line.

		int   nHeight = Calc.cy();

		CRect NewRect = Rect;

		for(;;) {

			NewRect.left  += 8;

			NewRect.right -= 8;

			CRect NewCalc = NewRect;

			DC.DrawText(Text, NewCalc,  DT_CENTER | DT_CALCRECT | DT_WORDBREAK);

			if( NewCalc.cx() <= NewRect.cx() ) {

				if( NewCalc.cy() == nHeight ) {

					Rect = NewRect;

					Calc = NewCalc;

					continue;
					}
				}

			break;
			}

		CRect Draw = Calc;
		
		int   xPos = (Rect.cx() - Calc.cx()) / 2;

		Draw.left  += xPos;

		Draw.right += xPos;

		DC.SetBkMode(TRANSPARENT);

		DC.DrawText(Text, Draw,  DT_CENTER | DT_WORDBREAK);
		}

	DC.Deselect();
	}

void CEmptyPromptWnd::OnSize(UINT uCode, CSize Size)
{
	Invalidate(FALSE);
	}

void CEmptyPromptWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	GetParent().SetFocus();
	}

void CEmptyPromptWnd::OnSetFocus(CWnd &Wnd)
{
	GetParent().SetFocus();
	}

// End of File
