
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP

#define	INCLUDE_TESTING_HPP
	
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTestApp;
class CTestFrameWnd;
class CTagItem;
class CTagList;
class CTagManager;
class CTestItem;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CTestApp : public CThread
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestApp(void);

		// Destructor
		~CTestApp(void);

	protected:
		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

class CTestFrameWnd : public CMainWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestFrameWnd(void);

		// Destructor
		~CTestFrameWnd(void);

	protected:
		// Data Members
		CDatabase *  m_pDbase;
		CAccelerator m_Accel;

           	// Interface Control
		void OnUpdateInterface(void);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnShowUI(BOOL fShow);
           
		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Data Objects
//

class CTagItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagItem(void);

		// Data Members
		CString m_Name;
		UINT   m_Data1;
		UINT   m_Data2;
		UINT   m_Data3;
		UINT   m_Data4;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Data Lists
//

class CTagList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagList(void);

		// Persistance
		void Init(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Managers
//

class CTagManager : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagManager(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;

	protected:
		// Data Members
		CTagList *m_pTags;
		
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Item
//

class CTestItem : public CSystemItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestItem(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;
		CString GetFixedName(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

	protected:
		// Data Members
		CTagManager *m_pTags;

		// System Data
		void AddData(void);

		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
