
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Resource Pane Window
//

// Runtime Class

AfxImplementRuntimeClass(CResPaneWnd, CCatPaneWnd);

// Constructor

CResPaneWnd::CResPaneWnd(void)
{
	m_Title   = IDS_RESOURCE_PANE;

	m_Prompt  = IDS_SELECT_THE_2;

	m_ShowID  = IDM_VIEW_SHOW_RIGHT;

	m_KeyName = L"ResPane";

	m_uBase   = IDM_GO_SELECT + 0x20;

	m_pView   = New CItemViewWnd(viewResource, viewCache);

	m_uSelect = NOTHING;

	m_Accel1.Create(L"ResPaneAccel1");

	m_Accel2.Create(L"ResPaneAccel2");
	}

// Attributes

BOOL CResPaneWnd::HasCat(CString Tag) const
{
	return !m_Res.Failed(m_Res.Find(Tag));
	}

// Operations

BOOL CResPaneWnd::ShowNavCats(CNavPaneWnd *pNav)
{
	CItem *pItem = pNav->GetNavItem();

	if( m_pItem != pItem ) {

		CStringArray Res;

		pNav->GetResList().Tokenize(Res, '|');

		m_Res.Empty();

		UINT c = Res.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			m_Res.Insert(Res[n]);
			}

		Attach(pItem);

		return TRUE;
		}

	return FALSE;
	}

void CResPaneWnd::SetItem(CString Nav)
{
	m_pView->Navigate(Nav);
	}

// Overridables

void CResPaneWnd::OnAttach(void)
{
	CDatabase *pDbase = m_pItem->GetDatabase();

	CATLIST    List   = pDbase->GetCatList();

	UINT       uCount = List.GetCount();

	CString    Last   = FindLastRes();

	for( UINT c = 0; c < uCount; c++ ) {

		RCAT Cat = List[c];

		if( Cat.m_Res.IsEmpty() ) {

			CString Tag = Cat.m_Tag;

			if( !m_Res.Failed(m_Res.Find(Tag)) ) {

				m_Cats.Append(Cat);

				if( Tag == Last ) {

					m_uSelect = m_uCount;
					}

				m_uCount++;
				}
			}
		}

	if( m_uSelect == NOTHING ) {

		m_uSelect = 0;
		}

	LoadConfig();

	AttachItemView();

	UpdateAll();
	}

BOOL CResPaneWnd::OnPreDetach(void)
{
	SaveLastRes();

	return CPaneWnd::OnPreDetach();
	}

void CResPaneWnd::OnDetach(void)
{
	m_Cats.Empty();

	m_uCount  = 0;

	m_uSelect = NOTHING;

	CPaneWnd::OnDetach();
	}

// Message Map

AfxMessageMap(CResPaneWnd, CCatPaneWnd)
{
	AfxDispatchCommand(IDM_VIEW_REFRESH, OnViewRefresh)

	AfxMessageEnd(CResPaneWnd)
	};

// Command Handlers

BOOL CResPaneWnd::OnViewRefresh(UINT uID)
{
	m_Title   = IDS_RESOURCE_PANE;

	m_Prompt  = IDS_SELECT_THE_2;

	OnUpdateUI();

	return FALSE;
	}

// Category Selection

void CResPaneWnd::OnSetCategory(CItem *pItem)
{
	CSysProxy Proxy;
	
	Proxy.Bind();

	Proxy.SetResCategory(pItem);
	}

// Implementation

CString CResPaneWnd::FindLastRes(void)
{
	CLASS Class = AfxPointerClass(m_pItem);

	INDEX Index = m_Last.FindName(Class);

	if( !m_Last.Failed(Index) ) {

		return m_Last.GetData(Index);
		}

	return L"";
	}

void CResPaneWnd::SaveLastRes(void)
{
	CLASS   Class = AfxPointerClass(m_pItem);

	CString Last  = m_Cats[m_uSelect].m_Tag;

	m_Last.Remove(Class);

	m_Last.Insert(Class, Last);
	}

// End of File
