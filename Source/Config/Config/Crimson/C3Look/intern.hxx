
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4100
#define IDS_CANCEL              0x4100
#define IDS_CANT                0x4101
#define IDS_CLOSE               0x4102
#define IDS_COLLAPSE            0x4103
#define IDS_COPY_2              0x4104
#define IDS_CREATE              0x4105
#define IDS_CUT_1               0x4106
#define IDS_CUT_2               0x4107
#define IDS_DELETE_1            0x4108
#define IDS_DO_YOU_WANT         0x4109
#define IDS_DO_YOU_WANT_TO      0x410A
#define IDS_EXPAND              0x410B
#define IDS_FIND                0x410C
#define IDS_FIND_ITEMS          0x410D
#define IDS_FIND_ITEMS_IN       0x410E
#define IDS_FMT_END_OF_LIST     0x410F
#define IDS_FMT_START_OF_LIST   0x4110
#define IDS_FORMAT              0x4111
#define IDS_GROUP               0x4112
#define IDS_LOADING_VIEW        0x4113
#define IDS_LOCK                0x4114
#define IDS_LOCKED              0x4115
#define IDS_MAKE_FMT_PRIVATE    0x4116
#define IDS_MOVE_1              0x4117
#define IDS_MOVE_2              0x4118
#define IDS_MULTIPLE_ITEMS      0x4119
#define IDS_MULTIPLE_ITEMS_2    0x411A
#define IDS_NAVIGATION_PANE     0x411B
#define IDS_NEXT                0x411C
#define IDS_NOT_FOUND           0x411D /* NOT USED */
#define IDS_OK                  0x411E
#define IDS_PASTE               0x411F
#define IDS_PREVIOUS            0x4120
#define IDS_PRIVATE_ACCESS      0x4121
#define IDS_PRIVATE_ACCESS_2    0x4122
#define IDS_REDO                0x4123
#define IDS_RENAME              0x4124
#define IDS_RESOURCE_PANE       0x4125
#define IDS_SEARCH_RESULTS_1    0x4126
#define IDS_SEARCH_RESULTS_2    0x4127
#define IDS_SELECT_ITEM_FROM    0x4128
#define IDS_SELECT_THE_1        0x4129
#define IDS_SELECT_THE_2        0x412A
#define IDS_SORT_LIST           0x412B
#define IDS_TCTRL               0x412C
#define IDS_TCTRLU              0x412D
#define IDS_TEXT_NOT_FOUND      0x412E
#define IDS_THE_NAME            0x412F
#define IDS_THIS_ITEM_HAS       0x4130
#define IDS_THIS_OPERATION      0x4131
#define IDS_UNDO                0x4132
#define IDS_UNLOCK              0x4133
#define IDS_YOU_MAY_APPLY_1     0x4134
#define IDS_YOU_MAY_APPLY_2     0x4135

// End of File

#endif
