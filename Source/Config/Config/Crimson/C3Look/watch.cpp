
#include "intern.hpp"

#include "watch.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Watch Window
//

// Runtime Class

AfxImplementRuntimeClass(CWatchWnd, CWnd);

// Constructor

CWatchWnd::CWatchWnd(CSystemWnd *pSystem, CWnd *pView)
{
	m_pSystem = pSystem;

	m_pView   = pView;

	m_fActive = TRUE;

	m_fShow   = FALSE;
	}

// Attributes

BOOL CWatchWnd::IsShown(void) const
{
	return m_fShow;
	}

// Operations

void CWatchWnd::Create(CRect Rect)
{
	LoadConfig(Rect);

	DWORD dwStyle = WS_BORDER	|
			WS_POPUP	|
			WS_CAPTION	|
			WS_SYSMENU	|
			WS_THICKFRAME   |
			WS_CLIPCHILDREN |
			WS_CLIPSIBLINGS ;

	CWnd::Create( IDS("Watch Window"),
		      WS_EX_TOOLWINDOW,
		      dwStyle,
		      Rect,
		      afxMainWnd->GetHandle(),
		      AfxNull(CMenu),
		      NULL
		      );
	}

void CWatchWnd::Show(BOOL fShow)
{
	m_fShow = fShow;

	if( m_fShow && m_fActive ) {

		ShowWindow(SW_SHOW);
	
		return;
		}

	m_pView->PostMessage(WM_COMMAND, IDKILL);
	
	ShowWindow(SW_HIDE);
	}

// Routing Control

BOOL CWatchWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView && m_pView->RouteMessage(Message, lResult) ) {

		return TRUE;
		}
	
	return CWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CWatchWnd, CWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_ACTIVATE)
	AfxDispatchMessage(WM_MOUSEACTIVATE)
	AfxDispatchMessage(WM_ACTIVATEAPP)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_KEYDOWN)

	AfxMessageEnd(CWatchWnd)
	};

// Message Handlers

void CWatchWnd::OnPostCreate(void)
{
	SetWindowPos( HWND_TOP,
		      0, 0, 0, 0,
		      SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE
		      );

	m_pView->Create( WS_CHILD | WS_CLIPCHILDREN,
			 GetClientRect(),
			 ThisObject, IDVIEW, NULL
			 );

	m_pView->ShowWindow(SW_SHOW);
	}

void CWatchWnd::OnPreDestroy(void)
{
	SaveConfig();
	}

void CWatchWnd::OnClose(void)
{
	Show(FALSE);
	}

void CWatchWnd::OnActivate(UINT uMethod, BOOL fMinimize, CWnd &Wnd)
{
	}

UINT CWatchWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	CDialog *pDlg;

	if( (pDlg = m_pSystem->GetSemiModal()) ) {

		pDlg->ForceActive();
		
		return MA_NOACTIVATEANDEAT;
		}
		
	if( !afxMainWnd->IsWindowEnabled() ) {

		MessageBeep(0);
		
		return MA_NOACTIVATEANDEAT;
		}

	return MA_ACTIVATE;
	}

void CWatchWnd::OnActivateApp(BOOL fActive, DWORD dwThread)
{
	if( dwThread == 0x12345678 ) {

		m_fActive = fActive;

		ShowWindow((m_fShow && m_fActive) ? SW_SHOWNA : SW_HIDE);
		}
	}

void CWatchWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		CRect Rect = GetClientRect();

		m_pView->MoveWindow(Rect, TRUE);
		}
	}

void CWatchWnd::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
	}

BOOL CWatchWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	LPARAM lParam = m_MsgCtx.Msg.lParam;

	if( uID >= 0xF000 ) {
		
		AfxCallDefProc();
		
		return TRUE;
		}
		
	if( uID >= 0x8000 ) {
	
		CCmdSourceData Source;
		
		if( uID < 0xF000 ) {

			Source.PrepareSource();
			}
	
		RouteControl(uID, Source);
		
		if( Source.GetFlags() & MF_DISABLED ) {
		
			MessageBeep(0);
			
			return TRUE;
			}

		RouteCommand(uID, lParam);
			
		return TRUE;
		}

	SendMessage(WM_AFX_COMMAND, uID, lParam);
			
	return TRUE;
	}

void CWatchWnd::OnKeyDown(UINT uCode, DWORD Flags)
{
	if( uCode == VK_ESCAPE || uCode == VK_F7 ) {

		Show(FALSE);
		}
	}

// Implementation

BOOL CWatchWnd::LoadConfig(CRect &Rect)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	CRect   Read = Rect;

	Reg.MoveTo(L"C3Look");

	Reg.MoveTo(L"Watch");

	Read.left   = Reg.GetValue(L"left",   UINT(Read.left));

	Read.top    = Reg.GetValue(L"top",    UINT(Read.top));

	Read.right  = Reg.GetValue(L"right",  UINT(Read.right));

	Read.bottom = Reg.GetValue(L"bottom", UINT(Read.bottom));

	if( CRect(CSize(SM_CXVIRTUALSCREEN)).Encloses(Read) ) {

		Rect = Read;

		return TRUE;
		}

	return FALSE;
	}

void CWatchWnd::SaveConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	CRect   Rect = GetWindowRect();

	Reg.MoveTo(L"C3Look");

	Reg.MoveTo(L"Watch");

	Reg.SetValue(L"left",   UINT(Rect.left));

	Reg.SetValue(L"top",    UINT(Rect.top));
	
	Reg.SetValue(L"right",  UINT(Rect.right));
	
	Reg.SetValue(L"bottom", UINT(Rect.bottom));
	}

// End of File
