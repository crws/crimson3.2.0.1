
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Results Window
//

// Runtime Class

AfxImplementRuntimeClass(CResultsWnd, CWnd);

// REV3 -- Add a toolbar to this view, with buttons for next item, previous
// item, and copy list to clipboard. Add ability to view as a tree. Add simple
// context menu on selection to allow selection of Jump To. Add a pane to
// allow the optional context associated with an item to be displayed.

// Constructor

CResultsWnd::CResultsWnd(CSystemWnd *pSystem)
{
	m_pSystem = pSystem;

	m_pTree   = New CTreeView;

	m_fValid  = FALSE;
	
	m_fActive = TRUE;

	m_fShow   = FALSE;

	m_uPos    = NOTHING;
	}

// Attributes

BOOL CResultsWnd::IsShown(void) const
{
	return m_fShow;
	}

// Operations

void CResultsWnd::Create(CRect Rect)
{
	LoadConfig(Rect);

	DWORD dwStyle = WS_BORDER	|
			WS_POPUP	|
			WS_CAPTION	|
			WS_SYSMENU	|
			WS_THICKFRAME   |
			WS_CLIPCHILDREN |
			WS_CLIPSIBLINGS ;

	CWnd::Create( CString(IDS_SEARCH_RESULTS_1),
		      WS_EX_TOOLWINDOW,
		      dwStyle,
		      Rect,
		      afxMainWnd->GetHandle(),
		      AfxNull(CMenu),
		      NULL
		      );
	}

void CResultsWnd::Show(BOOL fShow)
{
	m_fShow = fShow;

	ShowWindow((m_fShow && m_fActive) ? SW_SHOW : SW_HIDE);
	}

void CResultsWnd::Load(CString const &Head, CStringArray const &List)
{
	DoLoad(List);

	if( !m_fValid ) {

		m_pTree->ShowWindow(SW_SHOW);

		if( m_pTree->HasFocus() ) {

			m_pTree->SetFocus();
			}

		m_fValid = TRUE;
		}

	SetWindowText(Head);
	}

BOOL CResultsWnd::Select(UINT uPos)
{
	if( uPos >= m_Index.GetCount() ) {

		m_pTree->SelectItem(NULL, TVGN_CARET);

		return FALSE;
		}

	m_pTree->SelectItem(m_Index[uPos], TVGN_CARET);

	return TRUE;
	}

BOOL CResultsWnd::Deselect(void)
{
	m_pTree->SelectItem(NULL, TVGN_CARET);

	return TRUE;
	}

void CResultsWnd::Empty(void)
{
	if( m_fValid ) {

		if( m_pTree->HasFocus() ) {

			SetFocus();
			}

		m_pTree->ShowWindow(SW_HIDE);

		m_fValid = FALSE;
		}

	SetWindowText(CString(IDS_SEARCH_RESULTS_1));

	Invalidate(TRUE);

	m_pTree->DeleteChildren(NULL);

	m_Index.Empty();
	}

// Message Map

AfxMessageMap(CResultsWnd, CWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_ACTIVATE)
	AfxDispatchMessage(WM_MOUSEACTIVATE)
	AfxDispatchMessage(WM_ACTIVATEAPP)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KEYDOWN)

	AfxDispatchNotify(100, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(100, NM_CUSTOMDRAW,  OnTreeCustomDraw)
	AfxDispatchNotify(100, TVN_KEYDOWN,    OnTreeKeyDown   )
	AfxDispatchNotify(100, NM_RETURN,      OnTreeReturn    )
	AfxDispatchNotify(100, NM_DBLCLK,      OnTreeDblClk    )

	AfxMessageEnd(CResultsWnd)
	};

// Message Handlers

void CResultsWnd::OnPostCreate(void)
{
	SetWindowPos( HWND_TOP,
		      0, 0, 0, 0,
		      SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE
		      );

	CRect Rect    = GetClientRect() - 2;

	DWORD dwStyle = TVS_NOTOOLTIPS
		      | TVS_NOHSCROLL
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_TRACKSELECT
		      | TVS_FULLROWSELECT;

	m_pTree->Create( WS_CLIPCHILDREN | dwStyle,
			 Rect,
			 ThisObject,
			 IDVIEW
			 );

	m_pTree->SetFont(afxFont(Dialog));
	}

void CResultsWnd::OnPreDestroy(void)
{
	SaveConfig();
	}

void CResultsWnd::OnClose(void)
{
	Show(FALSE);
	}

void CResultsWnd::OnActivate(UINT uMethod, BOOL fMinimize, CWnd &Wnd)
{
	}

UINT CResultsWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	CDialog *pDlg;

	if( (pDlg = m_pSystem->GetSemiModal()) ) {

		pDlg->ForceActive();
		
		return MA_NOACTIVATEANDEAT;
		}
		
	if( !afxMainWnd->IsWindowEnabled() ) {

		MessageBeep(0);
		
		return MA_NOACTIVATEANDEAT;
		}

	return MA_ACTIVATE;
	}

void CResultsWnd::OnActivateApp(BOOL fActive, DWORD dwThread)
{
	if( dwThread == 0x12345678 ) {

		m_fActive = fActive;

		ShowWindow((m_fShow && m_fActive) ? SW_SHOWNA : SW_HIDE);
		}
	}

BOOL CResultsWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	if( !m_fValid ) {

		DC.FillRect(Rect, afxBrush(TabFace));
		}
	else {
		DC.FrameRect(Rect--, afxBrush(WHITE));

		DC.FrameRect(Rect--, afxBrush(WHITE));
		}

	return TRUE;
	}
	
void CResultsWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( !m_fValid ) {

		DC.Select(afxFont(Bolder));

		CString Text = IDS_SEARCH_RESULTS_2;

		CSize   Size = DC.GetTextExtent(Text);

		CPoint  Pos  = CPoint((GetClientRect().GetSize() - Size) / 2);

		DC.SetTextColor(afxColor(BLACK));

		DC.SetBkMode(TRANSPARENT);

		DC.TextOut(Pos, Text);

		DC.Deselect();
		}
	}

void CResultsWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		CRect Rect = GetClientRect() - 2;

		m_pTree->MoveWindow(Rect, TRUE);
		}
	}

void CResultsWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_fValid ) {

		m_pTree->SetFocus();
		}
	}

void CResultsWnd::OnKeyDown(UINT uCode, DWORD Flags)
{
	if( uCode == VK_ESCAPE || uCode == VK_F8 ) {

		Show(FALSE);
		}
	}

// Notification Handlers

void CResultsWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( Info.itemNew.hItem ) {

		m_uPos = Info.itemNew.lParam;
		}
	else
		m_uPos = NOTHING;
	}

UINT CResultsWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
		}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMTVCUSTOMDRAW &Extra = (NMTVCUSTOMDRAW &) Info;

		HTREEITEM       hItem = HTREEITEM(Info.dwItemSpec);

		if( hItem ) {
		
			UINT uTest = m_pTree->GetItemState(hItem, NOTHING);

			if( uTest & TVIS_DROPHILITED ) {

				Extra.clrTextBk = afxColor(Orange2);

				SelectObject(Info.hdc, m_pTree->GetFont());

				return CDRF_NEWFONT;
				}
			}

		if( Info.uItemState & CDIS_HOT ) {

			if( Info.uItemState & CDIS_SELECTED ) {

				Extra.clrTextBk = afxColor(Orange2);
				}
			else
				Extra.clrTextBk = afxColor(Orange3);

			Extra.clrText = afxColor(BLACK);

			SelectObject(Info.hdc, m_pTree->GetFont());

			return CDRF_NEWFONT;
			}
		}

	return 0;
	}

BOOL CResultsWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	if( Info.wVKey == VK_ESCAPE || Info.wVKey == VK_F8 ) {

		Show(FALSE);

		return TRUE;
		}

	if( Info.wVKey == VK_F4 ) {

		HTREEITEM hTree = m_pTree->GetSelection();

		if( GetKeyState(VK_SHIFT) & 0x8000 ) {

			if( hTree ) {

				hTree = m_pTree->GetPrevNode(hTree, TRUE);
				}
			else
				hTree = GetLast();
			}
		else {
			if( hTree ) {

				hTree = m_pTree->GetNextNode(hTree, TRUE);
				}
			else
				hTree = GetFirst();
			}

		if( !hTree ) {

			m_pTree->SelectItem(hTree, TVGN_CARET);

			MessageBeep(0);
			}
		else {
			m_pTree->SelectItem(hTree, TVGN_CARET);

			m_pSystem->SetFindPos(m_uPos);
			}

		return TRUE;
		}

	return FALSE;
	}

void CResultsWnd::OnTreeReturn(UINT uID, NMHDR &Info)
{
	if( m_uPos < NOTHING ) {

		Show(FALSE);

		m_pSystem->SetFindPos(m_uPos);
		}
	}

void CResultsWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( m_uPos < NOTHING ) {

			m_pSystem->SetFindPos(m_uPos);
			}
		}
	}

// Implementation

HTREEITEM CResultsWnd::GetFirst(void)
{
	return m_pTree->GetChild(NULL);
	}

HTREEITEM CResultsWnd::GetLast(void)
{
	HTREEITEM hItem = m_pTree->GetChild(NULL);

	for(;;) {

		HTREEITEM hNext = m_pTree->GetNextNode(hItem, TRUE);

		if( !hNext ) {

			break;
			}

		hItem = hNext;
		}

	return hItem;
	}

void CResultsWnd::DoLoad(CStringArray const &List)
{
	m_pTree->SetRedraw(FALSE);

	m_pTree->DeleteChildren(NULL);

	m_Index.Empty();

	UINT c = List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CString t = List[n].TokenLeft('\n');

		CTreeViewItem Node;

		Node.SetText(t);

		Node.SetParam(n);

		HTREEITEM hItem = m_pTree->InsertItem(NULL, NULL, Node);

		m_Index.Append(hItem);
		}

	m_pTree->SetRedraw(TRUE);

	m_pTree->Invalidate(FALSE);
	}

BOOL CResultsWnd::LoadConfig(CRect &Rect)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	CRect   Read = Rect;

	Reg.MoveTo(L"C3Look");

	Reg.MoveTo(L"Results");

	Read.left   = Reg.GetValue(L"left",   UINT(Read.left));

	Read.top    = Reg.GetValue(L"top",    UINT(Read.top));

	Read.right  = Reg.GetValue(L"right",  UINT(Read.right));

	Read.bottom = Reg.GetValue(L"bottom", UINT(Read.bottom));

	if( CRect(CSize(SM_CXVIRTUALSCREEN)).Encloses(Read) ) {

		Rect = Read;

		return TRUE;
		}

	return FALSE;
	}

void CResultsWnd::SaveConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	CRect   Rect = GetWindowRect();

	Reg.MoveTo(L"C3Look");

	Reg.MoveTo(L"Results");

	Reg.SetValue(L"left",   UINT(Rect.left));

	Reg.SetValue(L"top",    UINT(Rect.top));
	
	Reg.SetValue(L"right",  UINT(Rect.right));
	
	Reg.SetValue(L"bottom", UINT(Rect.bottom));
	}

// End of File
