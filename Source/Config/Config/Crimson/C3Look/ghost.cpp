
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ghost Bar Window
//

// Dynamic Class

AfxImplementDynamicClass(CGhostBar, CWnd);

// Constructor

CGhostBar::CGhostBar(void)
{
	BOOL fShadow = TRUE;

	SystemParametersInfo( SPI_GETDROPSHADOW,
			      0,
			      &fShadow,
			      0
			      );

	m_pShadow = fShadow ? New CGhostShadowWnd : NULL;

	m_nMax    = 99999;
	
	m_nHit    = afxAdjustDPI(50);
	}

// Destructor

CGhostBar::~CGhostBar(void)
{
	}

// Operations

BOOL CGhostBar::AddBar(CMenu &Menu)
{
	if( Menu.GetMenuItemCount() ) {

		if( Menu.IsMenuActive() ) {

			CToolbarWnd *pBar = New CToolbarWnd(barFlat);

			pBar->AddFromMenu(Menu, FALSE);

			m_Bars.Append(pBar);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CGhostBar::Create(CWnd &Wnd, CPoint const &Pos)
{
	m_pWnd = &Wnd;

	if( CWnd::Create( L"",
			  WS_EX_LAYERED,
			  WS_CLIPSIBLINGS | WS_POPUP,
			  CRect(Pos, CSize(1, 1)),
			  afxMainWnd->GetHandle(),
			  0,
			  NULL
			  ) ) {

		SetAlpha(255);

		return TRUE;
		}

	return FALSE;
	}

void CGhostBar::PollGadgets(void)
{
	UINT c = m_Bars.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CToolbarWnd *pBar = m_Bars[n];

		pBar->PollGadgets();
		}
	}

BOOL CGhostBar::SetAlpha(CPoint const &Pos)
{
	CRect Rect = GetWindowRect();

	if( Rect.PtInRect(Pos) ) {

		SetAlpha(255);

		m_nHit = afxAdjustDPI(180);

		return TRUE;
		}
	else {
		int cx = 0;

		int cy = 0;

		if( Pos.x < Rect.left   ) cx = Rect.left   - Pos.x;

		if( Pos.x > Rect.right  ) cx = Rect.right  - Pos.x;

		if( Pos.y < Rect.top    ) cy = Rect.top    - Pos.y;
				     
		if( Pos.y > Rect.bottom ) cy = Rect.bottom - Pos.y;

		int cd = int(sqrt(double(cx*cx + cy*cy)));

		MakeMin(m_nMax, cd + 10);

		int cm = max(m_nMax, m_nHit);

		if( cd < cm ) {

			SetAlpha(BYTE(255 - (192 * cd / cm)));

			return TRUE;
			}

		return FALSE;
		}
	}

void CGhostBar::SetAlpha(BYTE bAlpha)
{
	SetLayeredWindowAttributes( m_hWnd,
				    afxColor(MAGENTA),
				    bAlpha,
				    LWA_ALPHA | LWA_COLORKEY
				    );

	if( m_pShadow ) {

		m_pShadow->SetAlpha(bAlpha);
		}
	}

// Message Map

AfxMessageMap(CGhostBar, CWnd)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_ACTIVATE)
	AfxDispatchMessage(WM_MOUSEACTIVATE)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CGhostBar)
	};

// Message Handlers

UINT CGhostBar::OnCreate(CREATESTRUCT &Create)
{
	UINT  n = 0;

	UINT  c = m_Bars.GetCount();

	int  cx = 0;

	int  cy = 0;

	int  cm = 2;

	for( n = 0; n < c; n++ ) {

		CToolbarWnd *pBar = m_Bars[n];

		MakeMax(cx, pBar->GetWidth ());

		MakeMax(cy, pBar->GetHeight());
		}

	CSize  Size(cx, cy);

	CPoint Pos (cm, cm);

	for( n = 0; n < c; n++ ) {

		CToolbarWnd *pBar = m_Bars[n];

		pBar->Create(CRect(Pos, Size), ThisObject);

		pBar->PollGadgets();

		pBar->ShowWindow(SW_SHOW);

		Pos.y += Size.cy;
		}

	CPoint Org = GetWindowRect().GetTopLeft();

	SetWindowSize(Pos.x + cx + cm, Pos.y + cm, FALSE);

	SetWindowOrg (Org - CSize(0, (Pos.y + cm) / 2), FALSE);

	if( m_pShadow ) {

		m_pShadow->Create(ThisObject);
		}

	return 0;
	}

void CGhostBar::OnDestroy(void)
{
	if( m_pShadow ) {

		m_pShadow->DestroyWindow(TRUE);
		}
	}

void CGhostBar::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( m_pShadow ) {

		m_pShadow->ShowWindow(fShow ? SW_SHOWNA : SW_HIDE);
		}
	}

void CGhostBar::OnActivate(UINT uMethod, BOOL fMinimize, CWnd &Wnd)
{
	if( uMethod == WA_ACTIVE || uMethod == WA_CLICKACTIVE ) {
		
		afxMainWnd->SetActiveWindow();
		}
	}

UINT CGhostBar::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	if( !afxMainWnd->IsEnabled() ) {

		MessageBeep(0);
		
		return MA_NOACTIVATEANDEAT;
		}
	
	return MA_NOACTIVATE;
	}

BOOL CGhostBar::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	SetAlpha(255);

	m_nHit = afxAdjustDPI(180);

	return BOOL(AfxCallDefProc());
	}

void CGhostBar::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.FillRect(GetClientRect(), afxBrush(MAGENTA));

	CRect Rect = GetClientRect();

	DC.Select(afxBrush(Blue2));

	DC.Select(afxPen(3dDkShadow));

	DC.RoundRect(Rect, 9);

	DC.Deselect();

	DC.Deselect();
	}

//////////////////////////////////////////////////////////////////////////
//
// Ghost Bar Shadow
//

// Dynamic Class

AfxImplementDynamicClass(CGhostShadowWnd, CWnd);

// Constructor

CGhostShadowWnd::CGhostShadowWnd(void)
{
	m_nUmbra    = 0;

	m_nPenumbra = afxAdjustDPI(4);

	m_nTotal    = m_nUmbra + m_nPenumbra;

	m_pDC       = NULL;

	m_pBitmap   = NULL;

	m_pData     = NULL;
	}

// Destructor

CGhostShadowWnd::~CGhostShadowWnd(void)
{
	DeleteImage();
	}

// Operations

void CGhostShadowWnd::Create(CWnd &Wnd)
{
	CRect Rect = Wnd.GetWindowRect();

	Rect.left   += m_nTotal;

	Rect.top    += m_nTotal;

	Rect.right  += m_nTotal;

	Rect.bottom += m_nTotal;

	CWnd::Create( L"",
		      WS_EX_LAYERED,
		      WS_CLIPSIBLINGS | WS_POPUP,
		      Rect,
		      afxMainWnd->GetHandle(),
		      0,
		      NULL
		      );

	CreateImage();
	}

void CGhostShadowWnd::SetAlpha(BYTE bAlpha)
{
	if( IsWindow() ) {
		
		if( m_pDC ) {

			CPoint Org;

			BLENDFUNCTION Blend;

			Blend.BlendOp             = AC_SRC_OVER;
			Blend.BlendFlags          = 0;
			Blend.SourceConstantAlpha = BYTE(bAlpha / 2);
			Blend.AlphaFormat         = AC_SRC_ALPHA;

			UpdateLayeredWindow( m_hWnd,
					     NULL,
					     m_Pos,
					     m_Size,
					     m_pDC->GetHandle(),
					     Org,
					     0,
					     &Blend,
					     ULW_ALPHA
					     );
			}
		}
	}

// Implementation

void CGhostShadowWnd::CreateImage(void)
{
	m_Rect    = GetWindowRect();

	m_Pos     = m_Rect.GetTopLeft();

	m_Size    = m_Rect.GetSize();

	m_pBitmap = New CBitmap(m_Size, 1, 32, m_pData);

	m_pDC     = New CMemoryDC(CClientDC(NULL));

	m_pDC->Select(*m_pBitmap);

	Draw();
	}

void CGhostShadowWnd::DeleteImage(void)
{
	m_pDC->Deselect();

	delete m_pBitmap;

	delete m_pDC;
	}

void CGhostShadowWnd::Draw(void)
{
	CRect Draw = m_Size;

	CRect Clip = m_Size;

	m_pDC->FillRect(Draw, afxBrush(MAGENTA));

	Clip.right  -= m_nTotal;

	Clip.bottom -= m_nTotal;

	m_pDC->Select(afxBrush(RED));

	m_pDC->Select(afxPen  (RED));

	for( int n = 0; n <= m_nPenumbra; n++ ) {

		m_pDC->RoundRect(Draw, 9);

		BYTE bAlpha = BYTE(255 * (n + 1) / (m_nPenumbra + 1));

		m_pBitmap->SetAlpha( m_pData,
				     afxColor(RED),
				     afxColor(BLACK),
				     bAlpha
				     );

		Draw--;
		}

	m_pDC->Replace(afxBrush(MAGENTA));

	m_pDC->Replace(afxPen  (MAGENTA));

	m_pDC->RoundRect(Clip, 9);

	m_pBitmap->SetAlpha( m_pData,
			     afxColor(MAGENTA),
			     0
			     );

	m_pDC->Deselect();

	m_pDC->Deselect();
	}

// End of File
