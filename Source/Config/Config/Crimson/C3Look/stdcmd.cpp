
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Standard Command
//

// Runtime Class

AfxImplementRuntimeClass(CStdCmd, CCmd);

// Encoding

CString CStdCmd::GetFixedName(CItem *pItem)
{
	return pItem ? pItem->GetFixedName() : L"";
	}

CString CStdCmd::GetFixedPath(CItem *pItem)
{
	return pItem ? pItem->GetFixedPath() : L"";
	}

//////////////////////////////////////////////////////////////////////////
//
// Root Item Change Command
//

// Runtime Class

AfxImplementRuntimeClass(CCmdItemData, CStdCmd);

// Constructor

CCmdItemData::CCmdItemData(void)
{
	m_hData = NULL;

	m_hPrev = NULL;
	}

// Destructor

CCmdItemData::~CCmdItemData(void)
{
	GlobalFree(m_hData);

	GlobalFree(m_hPrev);
	}

// Attributes

BOOL CCmdItemData::IsNull(void) const
{
	UINT uData = GlobalSize(m_hData);

	UINT uPrev = GlobalSize(m_hPrev);

	if( uData == uPrev ) {

		PCBYTE pData = PCBYTE(GlobalLock(m_hData));

		PCBYTE pPrev = PCBYTE(GlobalLock(m_hPrev));

		if( memcmp(pData, pPrev, uData) ) {

			GlobalUnlock(m_hData);

			GlobalUnlock(m_hPrev);

			return FALSE;
			}

		GlobalUnlock(m_hData);

		GlobalUnlock(m_hPrev);

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Item Change Command
//

// Runtime Class

AfxImplementRuntimeClass(CCmdItem, CCmdItemData);

// Constructor

CCmdItem::CCmdItem(CString Menu, CItem *pItem, HGLOBAL hPrev)
{
	m_Menu  = Menu;

	m_Item  = pItem ? pItem->GetFixedPath() : L"";

	m_hData = pItem ? pItem->TakeSnapshot() : NULL;

	m_hPrev = hPrev;
	}

// Operations

void CCmdItem::Exec(CItem *pItem)
{
	pItem->LoadSnapshot(m_hData);

	pItem->SetDirty();
	}

void CCmdItem::Undo(CItem *pItem)
{
	pItem->LoadSnapshot(m_hPrev);

	pItem->SetDirty();
	}

//////////////////////////////////////////////////////////////////////////
//
// Global Item Change Command
//

// Runtime Class

AfxImplementRuntimeClass(CCmdGlobalItem, CCmdItemData);

// Constructors

CCmdGlobalItem::CCmdGlobalItem(CItem *pItem, HGLOBAL hPrev)
{
	m_Edit  = pItem->GetFixedPath();

	m_hData = pItem->TakeSnapshot();

	m_hPrev = hPrev;
	}

// Operations

void CCmdGlobalItem::Exec(CItem *pItem)
{
	pItem = FindEditItem(pItem->GetDatabase()->GetSystemItem());

	AfxAssert(pItem);

	pItem->LoadSnapshot(m_hData);

	pItem->SetDirty();
	}

void CCmdGlobalItem::Undo(CItem *pItem)
{
	pItem = FindEditItem(pItem->GetDatabase()->GetSystemItem());

	AfxAssert(pItem);

	pItem->LoadSnapshot(m_hPrev);

	pItem->SetDirty();
	}

// Implementation

CItem * CCmdGlobalItem::FindEditItem(CMetaItem *pItem)
{
	CString Path = m_Edit;

	while( !Path.IsEmpty() ) {

		CString    Step   = Path.StripToken(L'/');

		CMetaList *pList  = pItem->FindMetaList();

		UINT       uCount = pList->GetCount();

		UINT n;

		for( n = 0; n < uCount; n++ ) {

			CMetaData const *pData = pList->FindData(n);

			UINT             uType = pData->GetType();

			if( uType == metaObject || uType == metaVirtual ) {

				CItem *pChild = pData->GetObject(pItem);

				if( pChild ) {

					if( pChild->GetFixedName() == Step ) {

						if( pChild->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

							pItem = (CMetaItem *) pChild;

							break;
							}

						return NULL;
						}
					}
				}
			}

		if( n == uCount ) {

			return NULL;
			}
		}

	return pItem;
	}

//////////////////////////////////////////////////////////////////////////
//
// Sub-Item Change Command
//

// Runtime Class

AfxImplementRuntimeClass(CCmdSubItem, CCmdItemData);

// Constructors

CCmdSubItem::CCmdSubItem(CString Tag, CLASS Class)
{
	// LATER -- This is really two different commands and
	// it gets a bit confusing as to which one does what!!!

	m_Tag   = Tag;

	m_hPrev = NULL;

	m_hData = NULL;

	m_Class = Class;

	m_fPend = TRUE;

	m_fPrev = FALSE;
	}

CCmdSubItem::CCmdSubItem(CString Tag, HGLOBAL hPrev, HGLOBAL hData)
{
	// LATER -- This is really two different commands and
	// it gets a bit confusing as to which one does what!!!

	m_Tag   = Tag;

	m_hPrev = hPrev;

	m_hData = hData;

	m_Class = NULL;

	m_fPend = FALSE;

	m_fPrev = TRUE;
	}

// Attributes

CItem * CCmdSubItem::GetItem(CMetaItem *pRoot) const
{
	return FindObject(pRoot);
	}

// Operations

BOOL CCmdSubItem::Exec(CMetaItem *pRoot)
{
	CItem * & pItem   = FindObject(pRoot);

	CItem *   pParent = FindParent(pRoot);

	if( m_fPend ) {

		if( pItem ) {

			m_hPrev = pItem->TakeSnapshot();

			pItem->Kill();

			delete pItem;

			pItem = NULL;
			}

		if( m_Class ) {

			pItem = (CMetaItem *) AfxNewObject(CItem, m_Class);

			pItem->SetParent(pParent);

			pItem->Init();

			m_hData = pItem->TakeSnapshot();
			}

		pRoot->SetDirty();

		return TRUE;
		}

	if( pItem ) {

		pItem->Kill();

		delete pItem;
		}

	if( m_hData ) {

		pItem = (CMetaItem *) CItem::MakeFromSnapshot(pParent, m_hData);
		}

	pRoot->SetDirty();

	return TRUE;
	}

BOOL CCmdSubItem::Undo(CMetaItem *pRoot)
{
	CItem * & pItem   = FindObject(pRoot);

	CItem *   pParent = FindParent(pRoot);

	if( pItem ) {

		pItem->Kill();

		delete pItem;

		pItem = NULL;
		}

	if( m_hPrev ) {

		pItem = CItem::MakeFromSnapshot(pParent, m_hPrev);
		}

	pRoot->SetDirty();

	return TRUE;
	}

// Implementation

CItem * & CCmdSubItem::FindObject(CMetaItem *pRoot, CString Name) const
{
	CString Part = Name.StripToken('/');

	while( !Name.IsEmpty() ) {

		CMetaData const *pMeta = pRoot->FindMetaData(Part);

		pRoot = (CMetaItem *) pMeta->GetObject(pRoot);
		
		Part  = Name.StripToken('/');
		}

	CMetaData const *pMeta = pRoot->FindMetaData(Part);

	return pMeta->GetObject(pRoot);
	}

CItem * & CCmdSubItem::FindObject(CMetaItem *pRoot) const
{
	return FindObject(pRoot, m_Tag);
	}

CItem * CCmdSubItem::FindParent(CMetaItem *pRoot) const
{
	UINT uPos = m_Tag.FindRev('/');

	if( uPos < NOTHING ) {
	
		CString Name = m_Tag.Left(uPos);

		return FindObject(pRoot, Name);
		}

	return pRoot;
	}

// End of File
