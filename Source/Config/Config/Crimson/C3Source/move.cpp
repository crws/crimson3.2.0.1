
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Moving

BOOL CSourceEditorWnd::IsMoveKey(UINT uCode)
{
	switch( uCode ) {

		case VK_UP:
		case VK_DOWN:
		case VK_LEFT:
		case VK_RIGHT:
		case VK_HOME:
		case VK_END:
		case VK_PRIOR:
		case VK_NEXT:
			return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::PerformMove(CPoint &Pos, UINT uCode)
{
	CRange Line  = m_Lines[Pos.y];

	int    nFrom = Line.m_nFrom;

	int    nTo   = Line.m_nTo;

	int    nSize = nTo - nFrom;

	PCTXT  pText = m_Text;
	
	switch( uCode ) {

		case VK_UP:

			MovePrevLine(Pos);

			break;			

		case VK_DOWN:

			MoveNextLine(Pos);

			break;			

		case VK_LEFT:

			if( m_fSelect && !IsDown(VK_SHIFT) ) {
				
				Pos.x = m_Select.m_nFrom - nFrom;
				}
			else {
				if( IsDown(VK_CONTROL) ) {

					MovePrevToken(Pos);
					}
				else 
					MovePrevChar(Pos);
				}

			break;			

		case VK_RIGHT:
			
			if( m_fSelect && !IsDown(VK_SHIFT) ) {

				Pos.x = m_Select.m_nTo - nFrom;
				}
			else {
				if( IsDown(VK_CONTROL) ) {

					MoveNextToken(Pos);
					}
				else {
					int xCaret = FindPosFromChar(Pos.y, Pos.x+1);

					if( xCaret < m_TextExt.cx ) {
					
						Pos.x++;
						}
					}
				}
			
			break;			

		case VK_HOME:

			if( IsDown(VK_CONTROL) ) {

				Pos.x = 0;

				Pos.y = 0;
				}
			else {
				int n;

				for( n = nFrom; n < nTo; n++ ) {

					if( !wisspace(pText[n]) ) {

						break;
						}
					}

				if( n < nTo ) {

					if( Pos.x == (n -= nFrom) ) {

						Pos.x = 0;
						}
					else
						Pos.x = n;
					}
				else
					Pos.x = 0;
				}
			
			break;

		case VK_END:

			if( IsDown(VK_CONTROL) ) {

				Pos.x = 0;

				Pos.y = m_nLines - 1;
				}
			else
				Pos.x = nSize;
			
			break;

		case VK_PRIOR:

			Pos.y -= m_ViewExt.cy / m_FontSize.cy;

			MakeMax(Pos.y, 0);
			
			break;

		case VK_NEXT:

			Pos.y += m_ViewExt.cy / m_FontSize.cy;

			MakeMin(Pos.y, m_nLines - 1);
			
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Moving Help

BOOL CSourceEditorWnd::MovePrevChar(CPoint &Pos)
{
	if( Pos.x ) {
		
		Pos.x --;

		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::MovePrevLine(CPoint &Pos)
{
	if( Pos.y ) {
		
		Pos.y --;

		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::MoveNextLine(CPoint &Pos)
{
	if( Pos.y + 1 < m_nLines ) {

		Pos.y ++;

		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::MoveFirstChar(CPoint &Pos)
{
	if( Pos.x ) {
		
		Pos.x = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::MoveLastChar(CPoint &Pos)
{
	CPoint Old = Pos;

	CRange Line = m_Lines[Pos.y];

	int   nFrom = Line.m_nFrom;

	int     nTo = Line.m_nTo;

	int   nSize = nTo - nFrom;

	Pos.x = nSize;

	return Old.x != Pos.x;
	}

void CSourceEditorWnd::MovePrevWord(CPoint &Pos)
{
	CRange Line = m_Lines[Pos.y];

	int   nFrom = Line.m_nFrom;

	PCTXT pText = PCTXT(m_Text) + nFrom;

	while( Pos.x && wisspace(pText[Pos.x - 1]) ) {
			
		Pos.x --;			
		}

	while( Pos.x && !wisspace(pText[Pos.x - 1]) ) {
			
		Pos.x --;			
		}
	}

void CSourceEditorWnd::MoveNextWord(CPoint &Pos)
{
	CRange Line = m_Lines[Pos.y];

	int   nFrom = Line.m_nFrom;

	int     nTo = Line.m_nTo;

	int   nSize = nTo - nFrom;

	PCTXT pText = PCTXT(m_Text) + nFrom;

	while( Pos.x < nSize && !wisspace(pText[Pos.x]) ) {
		
		Pos.x ++;
		}

	while( Pos.x < nSize && wisspace(pText[Pos.x]) ) {
		
		Pos.x ++;
		}
	}

void CSourceEditorWnd::MovePrevToken(CPoint &Pos)
{
	if( Pos.x ) {

		CRange Line = m_Lines[Pos.y];

		int   nFrom = Line.m_nFrom;

		int     nTo = Line.m_nTo;

		int   nSize = nTo - nFrom;

		CArray <CTokenSpan> Spans;

		BOOL fComment = FALSE;

		C3SyntaxTokens(Spans, fComment, PCTXT(m_Text) + nFrom, nSize);

		CRange Token(nSize, nSize);

		for( UINT n = Spans.GetCount(); n; n -- ) {

			CTokenSpan Span = Spans[n - 1];

			Token.m_nFrom   = Token.m_nTo - Span.m_uCount;
			
			if( Pos.x >= Token.m_nFrom && Pos.x <= Token.m_nTo ) {

				if( Span.m_Type == spanComment ) {

					MovePrevWord(Pos);
					
					break;
					}

				if( Span.m_Type == spanWhiteSpace ) {

					Pos.x = Token.m_nFrom;
					}
				else {
					if( Pos.x > Token.m_nFrom ) {
						
						Pos.x = Token.m_nFrom;

						break;
						}
					}
				}

			Token.m_nTo = Token.m_nFrom;
			}
		}
	else {
		if( MovePrevLine(Pos) && MoveLastChar(Pos) ) {

			MovePrevToken(Pos);
			}
		}
	}

void CSourceEditorWnd::MoveNextToken(CPoint &Pos)
{
	CRange Line = m_Lines[Pos.y];

	int   nFrom = Line.m_nFrom;

	int     nTo = Line.m_nTo;

	int   nSize = nTo - nFrom;

	PCTXT pText = PCTXT(m_Text) + nFrom;

	if( Pos.x < nSize ) {

		CArray <CTokenSpan> Spans;

		BOOL fComment = FALSE;

		C3SyntaxTokens(Spans, fComment, pText, nSize);

		CRange Token(0, 0);

		for( UINT n = 0; n < Spans.GetCount(); n ++ ) {

			CTokenSpan Span = Spans[n];

			Token.m_nTo = Token.m_nFrom + Span.m_uCount;

			if( Pos.x >= Token.m_nFrom && Pos.x < Token.m_nTo ) {

				if( Span.m_Type == spanComment ) {

					MoveNextWord(Pos);
					}
				else
					Pos.x = Token.m_nTo;
				
				break;
				}

			Token.m_nFrom = Token.m_nTo;
			}

		while( Pos.x < nSize && wisspace(pText[Pos.x]) ) {
			
			Pos.x ++;
			}
		}
	else {
		if( MoveNextLine(Pos) ) {

			MoveFirstChar(Pos);

			CRange Line = m_Lines[Pos.y];

			int   nFrom = Line.m_nFrom;

			int     nTo = Line.m_nTo;

			int   nSize = nTo - nFrom;

			if( nSize ) {

				MoveNextToken(Pos);
				}
			}
		}
	}

// End of File
