
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// IUnknown

HRESULT CSourceEditorWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CSourceEditorWnd::AddRef(void)
{
	return 1;
	}

ULONG CSourceEditorWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CSourceEditorWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	UINT uType;

	if( CanAcceptDataObject(pData, uType) ) {

		*pEffect = DROPEFFECT_LINK;

		m_dwEffect = *pEffect;

		SetDrop(2);

		SetFocus();
		
		DropTrack(CPoint(pt.x, pt.y), FALSE);

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CSourceEditorWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		DropTrack(CPoint(pt.x, pt.y), FALSE);
		
		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CSourceEditorWnd::DragLeave(void)
{
	KillTimer(m_timerScroll);
	
	m_DropHelp.DragLeave();

	if( m_uDrop ) {
	
		SetDrop(0);

		return S_OK;
		}

	return S_OK;
	}

HRESULT CSourceEditorWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 2 ) {

		if( AcceptDataObject(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);
	
	return S_OK;
	}

// Drop Support

void CSourceEditorWnd::DropTrack(CPoint Pos, BOOL fMove)
{
	ScreenToClient(Pos);

	ScrollIntoView(Pos, TRUE);

	if( MouseToText(Pos) ) {
		
		m_Pos = Pos;

		MoveCaret();
		}
	}

BOOL CSourceEditorWnd::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

// End of File