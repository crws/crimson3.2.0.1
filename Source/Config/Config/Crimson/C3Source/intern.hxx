
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4300
#define IDS_FIND                0x4300
#define IDS_FIND_TEXT_IN        0x4301
#define IDS_PROTECTED_ACCESS    0x4302
#define IDS_TEXT_NOT_FOUND      0x4303

// End of File

#endif
