
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Selection

BOOL CSourceEditorWnd::StartSelect(CPoint Pos, BOOL fSelect)
{
	if( !m_fSelect ) {
		
		if( fSelect ) {
			
			m_SelectStart  = Pos;
			
			m_SelectEnd    = Pos;
			
			m_SelectAnchor = Pos;
			
			m_fSelect      = TRUE;
			}
		}
	else {
		if( !fSelect ) {
			
			m_fSelect = FALSE;
			
			Invalidate(GetSelectRect(), FALSE);
			}
		}
	
	return FALSE;
	}

void CSourceEditorWnd::CheckSelect(CPoint Pos)
{
	if( m_fSelect ) {
		
		if( Pos.y <= m_SelectAnchor.y ) {
			
			m_SelectStart = Pos;
			
			m_SelectEnd   = m_SelectAnchor;
			}
		else
			m_SelectEnd   = Pos;

		if( m_SelectEnd.y == m_SelectStart.y ) {

			if( m_SelectEnd.x < m_SelectStart.x ) {

				int nPos = m_SelectEnd.x;

				m_SelectEnd.x   = m_SelectStart.x;

				m_SelectStart.x = nPos;				
				}			
			}

		SelectSyncFrom2D();

		Invalidate(GetSelectRect(), FALSE);
		}
	}

CRect CSourceEditorWnd::GetSelectRect(void)
{
	CRange Select;

	Select.m_nFrom = min(m_SelectOld.m_nFrom, m_Select.m_nFrom);

	Select.m_nTo   = max(m_SelectOld.m_nTo, m_Select.m_nTo);

	CRect   Rect = GetClientRect();

	Rect.top     = m_Inits[CalcFrom1D(Select.m_nFrom).y].y;

	Rect.top    += m_ViewOrg.y + m_WndOrg.y;

	Rect.bottom  = m_Inits[CalcFrom1D(Select.m_nTo).y].y;

	Rect.bottom += m_ViewOrg.y + m_WndOrg.y;

	Rect.bottom +=  m_FontSize.cy;

	return Rect;
	}

BOOL CSourceEditorWnd::IsBlockSelect(void)
{
	if( m_fSelect ) {

		int   nFrom = m_Select.m_nFrom;

		int     nTo = m_Select.m_nTo;

		int   nSize = nTo - nFrom;

		PCTXT pText = PCTXT(m_Text) + nFrom;

		if( CString(pText, nSize).Find(EOL) < NOTHING ) {
			
			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CSourceEditorWnd::SelectBlock(void)
{
	if( m_fSelect ) {
		
		SyncFrom2D();

		CRange const &Start = m_Lines[m_SelectStart.y];

		CRange const   &End = m_Lines[m_SelectEnd.y];

		if( m_nPos == m_Select.m_nFrom ) {

			m_Select.m_nFrom = Start.m_nFrom;

			m_Select.m_nTo   = End.m_nTo;
			
			m_nPos           = Start.m_nFrom;
			}

		else if( m_nPos == m_Select.m_nTo ) {

			m_Select.m_nFrom = Start.m_nFrom;

			if( m_Select.m_nTo > End.m_nFrom ) {

				m_Select.m_nTo = End.m_nTo + 1;
				
				MakeMin(m_Select.m_nTo, int(m_Text.GetLength()));
				}

			m_nPos = m_Select.m_nTo;
			}
		else {
			return;
			}

		SyncFrom1D();

		MoveCaret();
		}
	}

void CSourceEditorWnd::SelectToken(CPoint Pos)
{
	CRange Line = m_Lines[Pos.y];

	int   nFrom = Line.m_nFrom;

	int     nTo = Line.m_nTo;

	int   nSize = nTo - nFrom;

	PCTXT pText = PCTXT(m_Text) + nFrom;

	CArray <CTokenSpan> Spans;

	BOOL fComment = FALSE;

	C3SyntaxTokens(Spans, fComment, PCTXT(m_Text) + nFrom, nSize);

	CRange Token(nSize, nSize);

	for( UINT n = Spans.GetCount(); n; n -- ) {

		CTokenSpan Span = Spans[n - 1];

		Token.m_nFrom   = Token.m_nTo - Span.m_uCount;
		
		if( Pos.x >= Token.m_nFrom && Pos.x <= Token.m_nTo ) {

			if( Span.m_Type == spanComment ) {

				while( Pos.x && !wisspace(pText[Pos.x - 1]) ) {
						
					Pos.x --;			
					}

				StartSelect(Pos, TRUE);

				while( Pos.x < nSize && !wisspace(pText[Pos.x]) ) {
					
					Pos.x ++;
					}

				}
			else {
				Pos.x = Token.m_nFrom;

				StartSelect(Pos, TRUE);

				Pos.x = Token.m_nTo;				
				}

			break;
			}

		Token.m_nTo = Token.m_nFrom;
		}

	if( Pos != m_Pos ) {

		m_Pos = Pos;

		CheckSelect(Pos);

		MoveCaret();
		}
	}

void CSourceEditorWnd::SelectSyncFrom2D(void)
{
	m_SelectOld = m_Select;

	m_Select.m_nFrom = CalcFrom2D(m_SelectStart);

	m_Select.m_nTo   = CalcFrom2D(m_SelectEnd);
	}

void CSourceEditorWnd::SelectSyncFrom1D(void)
{
	m_SelectStart = CalcFrom1D(m_Select.m_nFrom);

	m_SelectEnd   = CalcFrom1D(m_Select.m_nTo);
	}

// End of File