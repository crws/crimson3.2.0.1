
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3SOURCE_HXX
	
#define	INCLUDE_C3SOURCE_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3comp.hxx>

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_EDIT_BLOCK_COMMENT		0x8230
#define	IDM_EDIT_LINE_COMMENT		0x8231
#define	IDM_EDIT_INDENT			0x8232
#define	IDM_EDIT_OUTDENT		0x8233
#define	IDM_EDIT_SHOW_INFO		0x8234
#define	IDM_EDIT_JUMP_TO		0x8235

#define	IDM_VIEW_PROPERTIES		0x8340

// End of File

#endif
