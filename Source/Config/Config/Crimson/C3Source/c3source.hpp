
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3SOURCE_HPP
	
#define	INCLUDE_C3SOURCE_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdesk.hpp>

#include <c3comp.hpp>

#include <c3ui.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3source.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_C3SOURCE

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "c3source.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

class DLLAPI CSourceEditorWnd : public CCtrlWnd, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor		
		CSourceEditorWnd(UINT uMode = 0);

		// Destructor
		~CSourceEditorWnd(void);

		// Operations
		void Attach(CItem *pItem);
		void SetText(CString Data);
		void SetModify(BOOL fModify);
		void SetSel(CRange Range);

		// Attributes
		CString GetText(void);
		BOOL    GetModify(void);
		CRange  GetSel(void);

		// Editor Options
		struct COptions
		{
			UINT	m_uFontSize;
			BOOL	m_fSyntax;
			BOOL	m_fVirtual;
			BOOL	m_fUseTabs;
			UINT	m_uIndent;
			int	m_nTabSize;
			int	m_nIndentSize;
			};

	protected:
		// Timer IDs
		static UINT m_timerScroll;

		// Bound Items
		CDatabase   * m_pDbase;
		CString	      m_Text;
		BOOL	      m_fRead;
		BOOL	      m_fHide;
		UINT          m_uMode;

		// Editor Options
		COptions m_Opts;

		// 1D Syntax Spans
		CArray <CColorSpan> m_Spans;

		// Accelerator
		CAccelerator m_Accel;

		// Fonts
		CFont m_FontCode;

		// Metrics
		CSize m_FontSize;
		CSize m_TextExt;

		// States
		BOOL m_fComment;
		BOOL m_fModify;
		UINT m_uCapture;
		UINT m_uHelp;

		// Line Data
		CArray <CRange>	m_Lines;
		CArray <CPoint>	m_Inits;
		CArray <CSize>	m_Sizes;
		int             m_nLines;

		// Drop Context
		CDropHelper   m_DropHelp;
		UINT          m_uDrop;
		UINT	      m_cfCode;
		UINT	      m_cfFunc;
		DWORD	      m_dwEffect;

		// 1D Position
		int    m_nFrom;
		int    m_nTo;
		int    m_nSize;
		int    m_nPos;

		// 2D Position
		CPoint m_Pos;
		CPoint m_CaretPos;

		// 2D Selection
		BOOL   m_fSelect;
		CPoint m_SelectStart;
		CPoint m_SelectEnd;
		CPoint m_SelectAnchor;

		// 1D Selection
		CRange m_Select;
		CRange m_SelectOld;

		// Scroll
		CStatic    * m_pScrollC;
		CScrollBar * m_pScrollV;
		CScrollBar * m_pScrollH;
		BOOL         m_fScrollV;
		BOOL         m_fScrollH;
		int          m_nSpace;
		CPoint	     m_WndOrg;
		CPoint       m_ViewOrg;
		CSize	     m_ViewExt;

		// Find Data
		CString m_LastFind;

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		UINT OnGetDlgCode(MSG *pMsg);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnChar(UINT uCode, DWORD dwFlags);
		void OnSize(UINT uCode, CSize Size);
		void OnPaint(void);

	// dataobj.cpp

		// Data Object Testing
		BOOL CanAcceptDataObject(IDataObject *pData, UINT &uType);
		BOOL CanAcceptCode(IDataObject *pData, UINT &uType);
		BOOL CanAcceptFunc(IDataObject *pData, UINT &uType);
		BOOL CanAcceptText(IDataObject *pData, UINT &uType);

		// Data Object Acceptance
		BOOL AcceptDataObject(IDataObject *pData);
		BOOL AcceptCode(IDataObject *pData);
		BOOL AcceptFunc(IDataObject *pData);
		BOOL AcceptCode(IDataObject *pData, UINT cfData);
		BOOL AcceptText(IDataObject *pData);

		// Data Object Creation
		BOOL MakeDataObject(IDataObject * &pData);

	// draw.cpp

		// Drawing
		void DrawText(CDC &DC, CRect const &Clip);
		void DrawLine(CDC &DC, CRect const &Clip, CPoint &Pos, CRange Line, BOOL fSyntax);
		void DrawFrag(CDC &DC, CPoint &Pos, int &nFrom, int nSize, UINT uFlag);
		void SetSyntaxColor(CDC &DC, UINT uType);		

		// Tabbed Text Helpers
		CSize TabbedTextOut(CDC &DC, CPoint Pos, PCTXT pText, UINT uLen);
		CSize GetTabbedTextExtent(PCTXT pText, UINT uLen);
		CSize GetTabbedTextExtent(CString Text);

	// drag.cpp

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Drop Support
		void DropTrack(CPoint Pos, BOOL fMove);
		BOOL SetDrop(UINT uDrop);

	// edit.cpp

		// Edit Command Handlers
		BOOL OnEditGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL CanEditCut(void);
		BOOL CanEditCopy(void);
		BOOL CanEditPaste(void);
		BOOL CanEditShowInfo(void);
		BOOL CanEditJumpTo(void);
		BOOL FindSelectedWord(CString &Text);
		void OnEditCut(void);
		void OnEditCopy(void);
		void OnEditPaste(void);
		void OnEditDelete(void);
		void OnEditSelectAll(void);
		void OnEditLineComment(void);
		void OnEditBlockComment(void);
		void OnEditIndent(BOOL fIndent);
		void OnEditShowInfo(void);
		BOOL OnEditFind(void);
		BOOL OnEditFindNext(void);
		BOOL OnEditFindPrev(void);
		
		// Help
		BOOL FindLineComment(PCTXT pText, UINT uSize, UINT &uPos);

	// editor.cpp
		
		// Initialisation
		void LoadFonts(void);
		void FindMetrics(void);
		void FindLayout(void);
		void LoadConfig(void);
		void SaveConfig(void);
		void CalcTextExt(void);

		// Implementation
		void   ReflowFromHere(void);
		void   ReflowAll(void);
		void   UpdateAll(void);
		void   GetLineInfo(void);
		void   GetLineInfo(int nLine);		
		CPoint CalcFrom1D(int nPos);
		int    CalcFrom2D(CPoint Pos);
		void   SyncFrom1D(void);
		void   SyncFrom2D(void);
		void   SyncRange(void);
		void   SyncRange(int Line);
		void   SyncSize(int nLine);
		void   AdjustFromHere(int nDelta);
		void   AdjustFromHere(int nLine, int nDelta);
		void   MoveCaret(void);
		void   SetCaretPos(void);
		BOOL   IsDown(UINT uCode);
		int    FindCharFromPos(int nLine, int xPos);
		int    FindPosFromChar(int nLine, int xChar);
		void   DecodeEOL(CString &Text);
		void   EncodeEOL(CString &Text);
		
		// Status Display
		void ShowDefaultStatus(void);

		// Editing
		BOOL Indent(void);
		BOOL Outdent(void);
		BOOL EnterText(PCTXT pText);
		BOOL EnterChar(TCHAR cCode);
		BOOL DeleteChar(void);
		BOOL Backspace(void);
		BOOL DeleteSelect(void);

	// move.cpp

		// Moving
		BOOL IsMoveKey(UINT uCode);
		BOOL PerformMove(CPoint &Pos, UINT uCode);

		// Moving Help
		BOOL MovePrevChar(CPoint &Pos);		
		BOOL MovePrevLine(CPoint &Pos);
		BOOL MoveNextLine(CPoint &Pos);		
		BOOL MoveFirstChar(CPoint &Pos);
		BOOL MoveLastChar(CPoint &Pos);		
		void MovePrevWord(CPoint &Pos);
		void MoveNextWord(CPoint &Pos);		
		void MovePrevToken(CPoint &Pos);
		void MoveNextToken(CPoint &Pos);

	// mouse.cpp

		// Mouse Messages
		void OnRButtonDown(UINT uFlags, CPoint Pos);
		void OnRButtonUp(UINT uFlags, CPoint Pos);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Mouse Position
		CPoint GetClientPos(void);

		// Mouse Support
		BOOL MouseToText(CPoint &Pos);
		BOOL HitTestSelect(void);
		BOOL HitTestSelect(CPoint Pos);

	// scroll.cpp

		// Scroll Messages
		void OnVScroll(UINT uCode, int nPos, CWnd &Ctrl);
		void OnHScroll(UINT uCode, int nPos, CWnd &Ctrl);

		// Scroll Support
		void ScrollBy(CSize Step);
		void JumpIntoView(void);
		void ScrollIntoView(CPoint Pos, BOOL fDrop);
		void CreateScrollBars(void);
		void PlaceScrollBars(void);
		void ShowScrollBar(CWnd *pWnd);
		BOOL OnScroll(UINT uCode, int nPos, CScrollBar *pBar, int nDelta, long &nValue);

	// select.cpp

		// Selection
		BOOL  StartSelect(CPoint Pos, BOOL fSelect);
		void  CheckSelect(CPoint Pos);
		CRect GetSelectRect(void);
		BOOL  IsBlockSelect(void);
		void  SelectBlock(void);
		void  SelectToken(CPoint Pos);
		void  SelectSyncFrom2D(void);
		void  SelectSyncFrom1D(void);

	// view.cpp

		// View Command Handlers
		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		void OnViewProperties(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Source Options Dialog
//

class CSourceOptionsDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CSourceOptionsDialog(CSourceEditorWnd::COptions &Opts);

		// Attributes
		void GetOptions(CSourceEditorWnd::COptions &Opts) const;
		
	protected:
		// Data Members
		CSourceEditorWnd::COptions m_Opts;
				 
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		BOOL OnButton(UINT uID);

		// Implementation
		void DoEnables(void);
	};

// End of File

#endif
