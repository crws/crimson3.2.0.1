
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Mouse Messages

void CSourceEditorWnd::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	SetFocus();
	
	if( MouseToText(Pos) ) {

		if( !HitTestSelect() ) {

			if( m_Pos != Pos ) {

				m_Pos = Pos;

				MoveCaret();

				return;
				}
			}
		}
	}

void CSourceEditorWnd::OnRButtonUp(UINT uFlags, CPoint Pos)
{
	CMenu Load = CMenu(L"SourceEditorCtx");

	if( Load.IsValid() ) {

		CMenu &Menu = Load.GetSubMenu(0);

		Menu.SendInitMessage();

		Menu.DeleteDisabled();

		Menu.MakeOwnerDraw(FALSE);
		
		Menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
				     GetCursorPos(),
				     afxMainWnd->GetHandle()
				     );

		Menu.FreeOwnerDraw();
		}
	}

void CSourceEditorWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	SetFocus();

	if( MouseToText(Pos) ) {

		if( m_Pos != Pos ) {

			m_Pos = Pos;

			MoveCaret();
			}

		StartSelect(CPoint(0, 0), FALSE);

		m_uCapture = captSelect;

		StartSelect(m_Pos, TRUE);
		
		CheckSelect(m_Pos);

		SetCapture();
		}
	}

void CSourceEditorWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	m_uCapture = captNone;

	KillTimer(m_timerScroll);
	
	ReleaseCapture();
	}

void CSourceEditorWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( MouseToText(Pos) ) {

		StartSelect(CPoint(), FALSE);

		SelectToken(Pos);
		}
	}

void CSourceEditorWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( m_uCapture ) {

		ScrollIntoView(Pos, FALSE);

		if( MouseToText(Pos) ) {

			if( m_Pos != Pos ) {

				m_Pos = Pos;

				MoveCaret();

				if( m_uCapture == captSelect ) {

					CheckSelect(m_Pos);
					
					return;
					}
				}
			}
		}
	}

void CSourceEditorWnd::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
		}

	if( m_uCapture == captNone || m_uCapture == captSelect ) {

		nDelta /= -40;

		if( IsDown(VK_CONTROL) ) {
			
			ScrollBy(CSize(nDelta, 0));
			
			return;
			}

		ScrollBy(CSize(0, nDelta));
		}
	}

void CSourceEditorWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerScroll ) {

		if( m_uDrop ) {

			return;
			}

		if( m_uCapture ) {
		
			ScrollIntoView(GetClientPos(), FALSE);
			}
		else 
			ScrollIntoView(m_CaretPos, FALSE);

		return;
		}
	}

BOOL CSourceEditorWnd::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	CPoint Pos  = GetClientPos();

	CRect  Rect = CRect(m_WndOrg, m_ViewExt);

	if( Rect.PtInRect(Pos) ) {

		if( HitTestSelect(Pos) ) {
			
			SetCursor(CCursor(IDC_ARROW));
			}
		else
			SetCursor(CCursor(IDC_IBEAM));
		}
	else
		SetCursor(CCursor(IDC_ARROW));

	return TRUE;
	}

// Mouse Position

CPoint CSourceEditorWnd::GetClientPos(void)
{
	CPoint Pos = GetCursorPos();

	ScreenToClient(Pos);

	return Pos;
	}

// Mouse Support

BOOL CSourceEditorWnd::MouseToText(CPoint &Pos)
{
	CRect Rect = CRect(m_WndOrg, m_ViewExt);

	if( Rect.PtInRect(Pos)  ) {

		Pos -= m_WndOrg;

		Pos -= m_ViewOrg;

		int r;

		for( r = 0; r < m_nLines; r ++ ) {

			CPoint Init = m_Inits[r];

			if( Pos.y >= Init.y && Pos.y < Init.y + m_FontSize.cy ) {

				Pos.y = r;
				
				Pos.x = FindCharFromPos(Pos.y, Pos.x);
				
				CRange Line = m_Lines[Pos.y];

				int   nSize = Line.m_nTo - Line.m_nFrom;

				MakeMin(Pos.x, nSize);
				
				return TRUE;
				}
			}

		Pos.y = r - 1;
		
		Pos.x = FindCharFromPos(Pos.y, Pos.x);		
				
		CRange Line = m_Lines[Pos.y];

		int   nSize = Line.m_nTo - Line.m_nFrom;

		MakeMin(Pos.x, nSize);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::HitTestSelect(void)
{
	CPoint Pos = GetCursorPos();

	ScreenToClient(Pos);

	return HitTestSelect(Pos);
	}

BOOL CSourceEditorWnd::HitTestSelect(CPoint Pos)
{
	if( m_fSelect ) {

		Pos -= m_ViewOrg;

		for( int r = m_SelectStart.y; r <= m_SelectEnd.y; r++ ) {

			int x1, x2, y1, y2;

			if( r == m_SelectStart.y ) {

				x1 = FindPosFromChar(r, m_SelectStart.x);
				}
			else
				x1 = FindPosFromChar(r, 0);

			if( r == m_SelectEnd.y ) {

				x2 = FindPosFromChar(r, m_SelectEnd.x);
				}
			else
				x2 = GetClientRect().right;

			y1 = m_Inits[r].y;

			y2 = y1 + m_FontSize.cy;

			CRect Rect = CRect(x1, y1, x2, y2);

			if( Rect.PtInRect(Pos) ) {
				
				return TRUE;
				}
			}		
		}

	return FALSE;
	}

// End of File