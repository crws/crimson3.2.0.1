
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Data Object Testing

BOOL CSourceEditorWnd::CanAcceptDataObject(IDataObject *pData, UINT &uType)
{
	if( !m_fRead ) {

		if( CanAcceptCode(pData, uType) ) {
			
			return TRUE;
			}

		if( CanAcceptFunc(pData, uType) ) {
			
			return TRUE;
			}

		if( CanAcceptText(pData, uType) ) {
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::CanAcceptCode(IDataObject *pData, UINT &uType)
{
	FORMATETC Fmt = { WORD(m_cfCode), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text  = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);
		
		ReleaseStgMedium(&Med);

		return !UniIsComplex(Text);
		}
	
	return FALSE;
	}

BOOL CSourceEditorWnd::CanAcceptFunc(IDataObject *pData, UINT &uType)
{
	FORMATETC Fmt = { WORD(m_cfFunc), NULL, 1, -1, TYMED_HGLOBAL };
	
	return pData->QueryGetData(&Fmt) == S_OK;
	}

BOOL CSourceEditorWnd::CanAcceptText(IDataObject *pData, UINT &uType)
{
	FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text  = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);
		
		ReleaseStgMedium(&Med);

		return !UniIsComplex(Text);
		}
	
	return FALSE;
	}

// Data Object Acceptance

BOOL CSourceEditorWnd::AcceptDataObject(IDataObject *pData)
{
	if( AcceptCode(pData) ) {
		
		return TRUE;
		}

	if( AcceptFunc(pData) ) {
		
		return TRUE;
		}

	if( AcceptText(pData) ) {
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::AcceptCode(IDataObject *pData)
{
	return AcceptCode(pData, m_cfCode);
	}

BOOL CSourceEditorWnd::AcceptFunc(IDataObject *pData)
{
	return AcceptCode(pData, m_cfFunc);
	}

BOOL CSourceEditorWnd::AcceptCode(IDataObject *pData, UINT cfData)
{
	FORMATETC Fmt = { WORD(cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		HideCaret(m_hWnd);

		DeleteSelect();	

		UINT uPos = Text.Find('(');

		if( uPos < NOTHING ) {

			Text = Text.Left(uPos);

			Text = Text + L"()";
			}

		if( EnterText(Text) ) {

			if( uPos < NOTHING ) {
				
				m_nPos -= 1;

				SyncFrom1D();
				}

			UpdateAll();
			}

		ShowCaret(m_hWnd);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::AcceptText(IDataObject *pData)
{
	FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		HGLOBAL hText = Med.hGlobal;

		CString Text  = PCTXT(GlobalLock(hText));

		if( m_uMode == 1 ) {
			
			Text = Text.StripToken(L"\r\n");
			}

		EncodeEOL(Text);

		HideCaret(m_hWnd);

		DeleteSelect();	

		if( EnterText(Text) ) {
			
			UpdateAll();
			}

		ShowCaret(m_hWnd);

		GlobalUnlock(hText);
		
		ReleaseStgMedium(&Med);
		
		return TRUE;
		}
	
	return FALSE;
	}

// Data Object Creation

BOOL CSourceEditorWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	int     nFrom = m_Select.m_nFrom;

	int     nTo   = m_Select.m_nTo;

	int     nSize = nTo - nFrom;

	CString Text  = m_Text.Mid(nFrom, nSize);

	DecodeEOL(Text);

	pMake->AddText(Text);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

// End of File
