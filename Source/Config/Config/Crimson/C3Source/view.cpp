
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// View Command Handlers

BOOL CSourceEditorWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =
	
	{	IDM_VIEW_PROPERTIES, MAKELONG(0x0002, 0x5000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_VIEW_PROPERTIES:

			Src.EnableItem(TRUE);

			break;

		default:
			return FALSE;
		}

	return TRUE;	
	}

BOOL CSourceEditorWnd::OnViewCommand(UINT uID)
{
	switch( uID ) {

		case IDM_VIEW_PROPERTIES:

			OnViewProperties();

			break;

		default:

			return FALSE;
		}

	return TRUE;
	}

void CSourceEditorWnd::OnViewProperties(void)
{
	COptions Saved = m_Opts;

	CSourceOptionsDialog Dlg(Saved);

	if( Dlg.Execute(ThisObject) ) {

		Dlg.GetOptions(m_Opts);

		if( memcmp(&Saved, &m_Opts, sizeof(COptions)) ) {

			if( Saved.m_uFontSize != m_Opts.m_uFontSize ) {

				AfxNull(CWnd).SetFocus();

				LoadFonts();

				FindMetrics();

				ReflowAll();

				FindLayout();

				SetFocus();
				}

			if( Saved.m_fVirtual != m_Opts.m_fVirtual ) {

				if( !m_Opts.m_fVirtual ) {

					CRange Line = m_Lines[m_Pos.y];

					int   nSize = Line.m_nTo - Line.m_nFrom;

					MakeMin(m_Pos.x, nSize);
					
					UpdateAll();
					}
				}

			Invalidate(TRUE);
			}
		}	
	}

//////////////////////////////////////////////////////////////////////////
//
// Editor Properties Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSourceOptionsDialog, CStdDialog);
		
// Constructors

CSourceOptionsDialog::CSourceOptionsDialog(CSourceEditorWnd::COptions &Opts)
{
	m_Opts = Opts;

	SetName(L"EditorPropertiesDlg");
	}

// Attributes

void CSourceOptionsDialog::GetOptions(CSourceEditorWnd::COptions &Opts) const
{
	Opts = m_Opts;
	}

// Message Map

AfxMessageMap(CSourceOptionsDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnCommandOK)
	
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxDispatchCommand(300, OnButton)
	AfxDispatchCommand(200, OnButton)
	AfxDispatchCommand(201, OnButton)
	AfxDispatchCommand(202, OnButton)

	AfxMessageEnd(CSourceOptionsDialog)
	};

// Message Handlers

BOOL CSourceOptionsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	((CButton &) GetDlgItem(100)).SetCheck(m_Opts.m_fSyntax);

	GetDlgItem(101).SetWindowText(CPrintf("%d", m_Opts.m_uFontSize));
	
	SetRadioGroup(200, 202, m_Opts.m_uIndent);
	
	GetDlgItem(202).EnableWindow(FALSE);
	
	((CButton &) GetDlgItem(300)).SetCheck(m_Opts.m_fVirtual);

	SetRadioGroup(301, 302, m_Opts.m_fUseTabs ? 1 : 0);

	GetDlgItem(303).SetWindowText(CPrintf("%d", m_Opts.m_nTabSize));

	DoEnables();
	
	return TRUE;
	}

BOOL CSourceOptionsDialog::OnCommandOK(UINT uID)
{
	m_Opts.m_fSyntax   = ((CButton &) GetDlgItem(100)).IsChecked();

	m_Opts.m_uFontSize = max(watoi(GetDlgItem(101).GetWindowText()), 8);

	m_Opts.m_uIndent   = GetRadioGroup(200, 202);

	m_Opts.m_fVirtual  = ((CButton &) GetDlgItem(300)).IsChecked();

	m_Opts.m_fUseTabs  = GetRadioGroup(301, 302);

	m_Opts.m_nTabSize  = watoi(GetDlgItem(303).GetWindowText());

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CSourceOptionsDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

BOOL CSourceOptionsDialog::OnButton(UINT uID)
{
	DoEnables();

	return TRUE;
	}

// Implementation

void CSourceOptionsDialog::DoEnables(void)
{
	BOOL fVirtual = ((CButton &) GetDlgItem(300)).IsChecked();

	GetDlgItem(301).EnableWindow(fVirtual);

	GetDlgItem(302).EnableWindow(fVirtual);
	}

// End of File
