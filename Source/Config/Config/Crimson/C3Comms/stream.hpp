
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_STREAM_HPP
	
#define	INCLUDE_STREAM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedItem : public CItem { };

//////////////////////////////////////////////////////////////////////////
//
// Test Streamer Driver Options
//

class CTestStreamDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestStreamDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		CCodedItem *m_pEnable;
		UINT	    m_Files;
		UINT	    m_Fields;
		UINT	    m_Saved;
		UINT	    m_Size;
		UINT	    m_Start;
		UINT	    m_End;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Streamer Driver #1
//

class CTestStream1Driver : public CBasicCommsDriver
{
	public:
		// Constructor
		CTestStream1Driver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding (void);
		void GetBindInfo(CBindInfo &Info);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Streamer Driver #2
//

class CTestStream2Driver : public CBasicCommsDriver
{
	public:
		// Constructor
		CTestStream2Driver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding (void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
