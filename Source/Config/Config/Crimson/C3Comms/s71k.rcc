
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// UI for CS71KDriverOptions
//

CS71KDriverOptionsUIList RCDATA
BEGIN
		"\0"
END

CS71KDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CS71KDeviceOptions
//

CS71KDeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the primary IP address of the S7 device."
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the TCP port number on which the S7 protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Type,Type,,CUIDropDown,CPU|,"
	""
	"\0"

	"Rack,Rack,,CUIEditInteger,|0||0|6,"
	"Specify the number representing the rack of the ethernet module."
	"\0"

	"Slot,Slot,,CUIEditInteger,|0||0|255,"
	"Specify the position of the ethernet module."
	"\0"

	"Addr2,IP Address,,CUIIPAddress,,"
	"Indicate the secondary IP address of the S7 device. This will be used "
	"if the primary address does not respond, and can be used to implement "
	"redundant communications. Leave the field at 0.0.0.0 to disable fallback."
	"\0"

	"Type2,Type,,CUIDropDown,CPU|,"
	""
	"\0"

	"Rack2,Rack,,CUIEditInteger,|0||0|6,"
	"Specify the number representing the rack of the secondary ethernet module."
	"\0"

	"Slot2,Slot,,CUIEditInteger,|0||0|255,"
	"Specify the position of the secondary ethernet module."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the S7 driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the S7 server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a S7 request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"TSAP,Use TSAP Configuration,,CUIDropDown,No|Yes,"
	"Select to configure this device according to the TSAP definition as defined in the "
	"Siemen's configuration software. "
	"\0"

	"STsap,S7 Server TSAP (Hex),,CUIEditIntHex,4|,"
	"Enter the S7's Server or Local TSAP with the '.' omitted."
	"\0"

	"CTsap,S7 Client TSAP (Hex),,CUIEditIntHex,4|,"
	"Enter the S7's Client or Partner TSAP with the '.' omitted."
	"\0"

	"STsap2,S7 Server TSAP (Hex),,CUIEditIntHex,4|,"
	"Enter the S7's Server or Local TSAP with the '.' omitted.  Set this field to a value of 0 to use "
	"the primary device server configuration."
	"\0"

	"CTsap2,S7 Client TSAP (Hex),,CUIEditIntHex,4||.,"
	"Enter the S7's Client or Partner TSAP with the '.' omitted.  Set this field to a value of 0 to use "
	"the primary device client configuration."
	"\0"

	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// S7 1K Element Dialog
//
//

S71KElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT		2002,	 0, 0, 22, 12, 
	CTEXT		":",	2004,	25, 1, 14, 12
	EDITTEXT		2005,	42, 0, 24, 12, 
END


//////////////////////////////////////////////////////////////////////////
//
// Import Error List
//
//

S7ImportErrorDlg DIALOG 0, 0, 0, 0
CAPTION "S7 Import Error Dialog"
BEGIN
	LTEXT	 "<Error>"			1002,	  4,   4,  292,  14
	GROUPBOX "&Error List",			1000,	  4,   20, 292, 312
	LISTBOX					1001,	 10,   34, 280, 302, XS_LISTBOX | LBS_USETABSTOPS

	////////
	DEFPUSHBUTTON	"OK",			IDOK,	 193,  340,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Copy To File",		1003,	 237,  340,  60,  14, XS_BUTTONREST
	
END

// End of File
