
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Driver Data
//

struct CDriverInfo
{
	// Data Members
	UINT	 m_uIdent;
	UINT	 m_uFlags;
	CString	 m_Group;
	CString	 m_Name;
	CString	 m_Version;
};

//////////////////////////////////////////////////////////////////////////
//
// Driver Picker Dialog
//

class CDriverPickerDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDriverPickerDialog(UINT uIdent, UINT uBinding, UINT uBindMask);

	// Destructor
	~CDriverPickerDialog(void);

	// Attributes
	UINT GetIdent(void) const;

protected:
	// Type Definitions
	typedef CList <CDriverInfo *> CDriverList;

	// Static Data
	static BOOL    m_fAllowBeta;
	static UINT    m_uAllowGroup;
	static CString m_AllowModel;

	// Data Members
	UINT		m_uIdent;
	UINT		m_uBinding;
	UINT		m_uBindMask;
	BOOL		m_fInit;
	CString		m_Group;
	CDriverList	m_List;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
	void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Measure);
	LONG OnCompareItem(UINT uID, COMPAREITEMSTRUCT &Comp);
	void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);

	// Notification Handlers
	void OnDblClk(UINT uID, CWnd &Wnd);
	void OnNewGroup(UINT uID, CWnd &Wnd);

	// Command Handlers
	BOOL OnOkay(UINT uID);

	// Implementation
	void SetCaption(void);
	void LoadGroupList(void);
	void LoadDriverList(void);
	void InitList(CListBox &List, CString Name);

	// Driver List Support
	void MakeDriverList(void);
	void KillDriverList(void);
	void AddDriver(ICommsDriver *pDriver);
	void GetInfo(UINT uIdent, CDriverInfo &Info);
	void ShowCount(void);

	// Binding Filter
	BOOL CheckBinding(UINT uBinding, ICommsDriver *pDriver);

	// Text Helper
	CString	GetBindText(UINT uBinding);

	// Default Allow
	BOOL AllowDriver(UINT uIdent);
	BOOL IsBetaDriver(UINT uIdent);

	// Friends
	friend DLLAPI void C3AllowBetaDrivers(void);
	friend DLLAPI void C3AllowDriverGroup(UINT uGroup, CString const &Model);
	friend DLLAPI BOOL C3IsDriverAllowed(UINT ID);
};

//////////////////////////////////////////////////////////////////////////
//
// Driver Picker Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CDriverPickerDialog, CStdDialog);

// Static Data

BOOL    CDriverPickerDialog::m_fAllowBeta  = FALSE;

UINT    CDriverPickerDialog::m_uAllowGroup = 0;

CString CDriverPickerDialog::m_AllowModel;

// Block Override

DLLAPI void C3AllowBetaDrivers(void)
{
	CDriverPickerDialog::m_fAllowBeta = TRUE;
}

DLLAPI void C3AllowDriverGroup(UINT uGroup, CString const &Model)
{
	CDriverPickerDialog::m_uAllowGroup = uGroup;

	CDriverPickerDialog::m_AllowModel  = Model;
}

// Constructor

CDriverPickerDialog::CDriverPickerDialog(UINT uIdent, UINT uBinding, UINT uBindMask)
{
	m_uIdent    = uIdent;

	m_uBinding  = uBinding;

	m_uBindMask = uBindMask;

	m_fInit     = TRUE;

	SetName(L"DriverPickerDlg");
}

// Destructor

CDriverPickerDialog::~CDriverPickerDialog(void)
{
	KillDriverList();
}

// Attributes

UINT CDriverPickerDialog::GetIdent(void) const
{
	return m_uIdent;
}

// Message Map

AfxMessageMap(CDriverPickerDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
		AfxDispatchMessage(WM_MEASUREITEM)
		AfxDispatchMessage(WM_COMPAREITEM)
		AfxDispatchMessage(WM_DRAWITEM)

		AfxDispatchCommand(IDOK, OnOkay)

		AfxDispatchNotify(1101, LBN_DBLCLK, OnDblClk)
		AfxDispatchNotify(1001, LBN_SELCHANGE, OnNewGroup)

		AfxMessageEnd(CDriverPickerDialog)
};

// Message Handlers

BOOL CDriverPickerDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetCaption();

	afxThread->SetWaitMode(TRUE);

	MakeDriverList();

	ShowCount();

	LoadGroupList();

	afxThread->SetWaitMode(FALSE);

	return FALSE;
}

void CDriverPickerDialog::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Measure)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	Measure.itemHeight = DC.GetTextExtent(L"X").cy;

	DC.Deselect();
}

LONG CDriverPickerDialog::OnCompareItem(UINT uID, COMPAREITEMSTRUCT &Comp)
{
	CDriverInfo *pInfo1 = (CDriverInfo *) Comp.itemData1;

	CDriverInfo *pInfo2 = (CDriverInfo *) Comp.itemData2;

	CString      Name1  = pInfo1 ? pInfo1->m_Name : L"@";

	CString      Name2  = pInfo2 ? pInfo2->m_Name : L"@";

	LONG         Code   = wstricmp(Name1, Name2);

	if( !Code ) {

		AfxAssume(pInfo1);

		AfxAssume(pInfo2);

		Name1 = pInfo1->m_Version;

		Name2 = pInfo2->m_Version;

		Code  = wstricmp(Name1, Name2);
	}

	return Code;
}

void CDriverPickerDialog::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw)
{
	CDC DC(Draw.hDC);

	CDriverInfo *pInfo = (CDriverInfo *) Draw.itemData;

	CRect        Rect  = CRect(Draw.rcItem);

	if( Draw.itemState & ODS_SELECTED ) {

		DC.SetBkColor(afxColor(SelectBack));

		DC.SetTextColor(afxColor(SelectText));
	}
	else {
		DC.SetBkColor(afxColor(WindowBack));

		DC.SetTextColor(afxColor(WindowText));
	}

	if( TRUE ) {

		Rect.right -= 1;
	}

	if( pInfo ) {

		CString Driver  = pInfo->m_Name;

		CString Version = CString(IDS_VERSION) + pInfo->m_Version;

		int     xSplit  = DC.GetTextExtent(Version).cx + 4;

		CRect   One     = Rect;

		CRect   Two     = Rect;

		Two.left        = Two.right - xSplit;

		One.right       = Two.left;

		DC.ExtTextOut(One.GetTopLeft()+CPoint(2, 0),
			      ETO_CLIPPED | ETO_OPAQUE,
			      One,
			      Driver
		);

		DC.ExtTextOut(Two.GetTopLeft()+CPoint(2, 0),
			      ETO_CLIPPED | ETO_OPAQUE,
			      Two,
			      Version
		);
	}
	else {
		CString Text = IDS_DRIVER_NONESELECTED;

		DC.ExtTextOut(Rect.GetTopLeft()+CPoint(2, 0),
			      ETO_CLIPPED | ETO_OPAQUE,
			      Rect,
			      Text
		);
	}

	if( Draw.itemState & ODS_FOCUS ) {

		DC.DrawFocusRect(Rect);
	}
}

// Command Handlers

BOOL CDriverPickerDialog::OnOkay(UINT uID)
{
	CListBox    &List  = (CListBox    &) GetDlgItem(1101);

	CDriverInfo *pInfo = (CDriverInfo *) List.GetCurSelData();

	m_uIdent           = pInfo ? pInfo->m_uIdent : 0;

	EndDialog(TRUE);

	return TRUE;
}

// Notification Handlers

void CDriverPickerDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
}

void CDriverPickerDialog::OnNewGroup(UINT uID, CWnd &Wnd)
{
	CListBox &List = (CListBox &) Wnd;

	m_Group        = List.GetText(List.GetCurSel());

	LoadDriverList();
}

// Implementation

void CDriverPickerDialog::SetCaption(void)
{
	CString Text;

	Text.Printf(IDS_DRIVER_PORTCAPTION,
		    GetWindowText(),
		    GetBindText(m_uBinding)
	);

	C3OemStrings(Text);

	SetWindowText(Text);
}

void CDriverPickerDialog::LoadGroupList(void)
{
	CString  Priority = C3OemOption(L"Priority", L"");

	BOOL     fMatched = FALSE;

	CListBox &List = (CListBox &) GetDlgItem(1001);

	List.SetRedraw(FALSE);

	List.ResetContent();

	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			CDriverInfo &Info = *m_List[n];

			if( !Priority.IsEmpty() ) {

				if( Info.m_Group == Priority ) {

					// allows SSD Drives Firewire Driver to be loaded as a driver for the option card

					if( Info.m_uIdent != 0x4020 ) {

						fMatched = TRUE;

						m_List.GetNext(n);

						continue;
					}
				}
			}

			if( List.FindStringExact(0, Info.m_Group) == NOTHING ) {

				List.AddString(Info.m_Group);
			}

			m_List.GetNext(n);
		}
	}

	if( fMatched ) {

		List.InsertString(1, Priority);
	}

	UINT uFind = List.FindStringExact(0, m_Group);

	if( uFind == NOTHING ) {

		List.SetCurSel(0);
	}
	else
		List.SetCurSel(uFind);

	List.SetRedraw(TRUE);

	List.Invalidate(TRUE);

	OnNewGroup(1001, List);
}

void CDriverPickerDialog::LoadDriverList(void)
{
	CListBox &List = (CListBox &) GetDlgItem(1101);

	DWORD    dwSelect = 0;

	List.SetRedraw(FALSE);

	List.ResetContent();

	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			CDriverInfo &Info = m_List[n][0];

			if( m_Group == Info.m_Group ) {

				BOOL fInclude = TRUE;

				if( m_Group[0] == '<' ) {

					if( m_uBinding == bindStdSerial ) {

						fInclude = FALSE;
					}
				}

				if( fInclude ) {

					DWORD dwData = DWORD(&Info);

					if( m_fInit ) {

						if( Info.m_uIdent == m_uIdent ) {

							dwSelect = dwData;
						}
					}

					List.AddString(PCTXT(dwData), dwData);
				}
			}

			m_List.GetNext(n);
		}
	}

	List.InsertString(0, PCTXT(NULL), NULL);

	if( m_fInit ) {

		List.SelectData(dwSelect);

		m_fInit = FALSE;
	}
	else
		List.SetCurSel(1);

	List.SetRedraw(TRUE);

	List.Invalidate(TRUE);
}

// Driver List Support

void CDriverPickerDialog::MakeDriverList(void)
{
	for( UINT n = 0;; n++ ) {

		ICommsDriver *p = C3EnumDrivers(n);

		if( p ) {

			AddDriver(p);

			p->Release();
		}
		else
			break;
	}
}

void CDriverPickerDialog::KillDriverList(void)
{
	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			delete m_List[n];

			m_List.GetNext(n);
		}

		m_List.Empty();
	}
}

void CDriverPickerDialog::AddDriver(ICommsDriver *pDriver)
{
	UINT uIdent = pDriver->GetID();

	BOOL fAllow = AllowDriver(uIdent);

	BOOL fBlock = IsBetaDriver(uIdent);

	if( !fBlock && C3OemAllowDriver(uIdent, fAllow) ) {

		UINT uBinding = pDriver->GetBinding();

		if( CheckBinding(uBinding, pDriver) ) {

			CDriverInfo *pInfo = New CDriverInfo;

			pInfo->m_uIdent    = uIdent;

			pInfo->m_uFlags    = pDriver->GetFlags();

			pInfo->m_Group     = pDriver->GetString(stringManufacturer);

			pInfo->m_Name      = pDriver->GetString(stringDriverName);

			pInfo->m_Version   = pDriver->GetString(stringVersion);

			if( uIdent == m_uIdent ) {

				m_Group = pInfo->m_Group;
			}

			m_List.Append(pInfo);
		}
	}
}

void CDriverPickerDialog::GetInfo(UINT uIdent, CDriverInfo &Info)
{
	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			CDriverInfo *pEntry = m_List[n];

			if( pEntry->m_uIdent == uIdent ) {

				Info = *pEntry;

				break;
			}

			m_List.GetNext(n);
		}
	}
}

void CDriverPickerDialog::ShowCount(void)
{
	UINT uCount = m_List.GetCount();

	if( uCount ) {

		if( uCount == 1 ) {

			CString Text(IDS_TOTAL_OF_DRIVER);

			GetDlgItem(1200).SetWindowText(Text);
		}
		else {
			CPrintf Text(IDS_TOTAL_OF_FMT, uCount);

			GetDlgItem(1200).SetWindowText(Text);
		}
	}
	else {
		CString Text(CString(IDS_NO_DRIVERS));

		GetDlgItem(1200).SetWindowText(Text);
	}
}

// Binding Filter

BOOL CDriverPickerDialog::CheckBinding(UINT uBinding, ICommsDriver *pDriver)
{
	if( m_uBinding == uBinding && m_uBindMask & (1 << bindRemote) ) {

		return pDriver->GetFlags() & dflagRemotable;
	}

	if( m_uBinding == uBinding || m_uBindMask & (1 << uBinding) ) {

		return TRUE;
	}

	if( m_uBinding == bindRawSerial ) {

		if( uBinding == bindStdSerial ) {

			return TRUE;
		}
	}

	return FALSE;
}

// Text Helper

CString	CDriverPickerDialog::GetBindText(UINT uBinding)
{
	switch( uBinding ) {

		case bindRawSerial: return IDS_DRIVER_RAW;
		case bindStdSerial: return IDS_DRIVER_STD;
		case bindEthernet:  return IDS_DRIVER_ETHERNET;
		case bindCAN:	    return IDS_DRIVER_CAN;
		case bindProfibus:  return IDS_DRIVER_PROFIBUS;
		case bindFireWire:  return IDS_DRIVER_FIREWIRE;
		case bindDeviceNet: return IDS_DRIVER_DEVICENET;
		case bindCatLink:   return IDS_DRIVER_CATLINK;
		case bindMPI:       return IDS_DRIVER_MPI;
		case bindJ1939:	    return IDS_DRIVER_J1939;
	}

	return L"";
}

// Default Allow

BOOL CDriverPickerDialog::AllowDriver(UINT uIdent)
{
	switch( uIdent ) {

		// Obsolete Modems
		case 0xFF01: return FALSE;
		case 0xFF02: return FALSE;
		case 0xFF03: return FALSE;

		// Honeywell Specific
		case 0x9998: return FALSE;
		case 0x9999: return FALSE;

		// SSD Drives RTNX
		case 0x400C: return FALSE;
		case 0x4020: return FALSE;

		// CDL Drivers
		case 0x4039: return FALSE;
		case 0x403D: return FALSE;
		case 0x4041: return FALSE;
		case 0x404F: return FALSE;
		case 0x407E: return FALSE;

		// Monico Specific
		case 0x3527: return FALSE;
		case 0x406B: return FALSE;
		case 0x4090: return FALSE;
		case 0x408E: return FALSE;
		case 0x40A3: return FALSE;
		case 0x40AD: return FALSE;
		case 0x40BE: return FALSE;
		case 0x40BF: return FALSE;
		case 0x40C6: return FALSE;
		case 0x40C7: return FALSE;

		// Eurotherm-Invensys
		case 0x353D: return FALSE;
		case 0x353E: return FALSE;
		case 0x353F: return FALSE;
		case 0x3540: return FALSE;
		case 0x4087: return FALSE;
		case 0x4088: return FALSE;
		case 0x4089: return FALSE;
		case 0x408F: return FALSE;
		case 0x409F: return FALSE;
		case 0x3542: return FALSE;

		// Microscan
		case 0x402D: return FALSE;
	}

	return C3IsDriverAllowed(uIdent);
}

BOOL CDriverPickerDialog::IsBetaDriver(UINT uIdent)
{
	// NOTE -- Drivers should be placed in this section unless they are ready
	// to be included in various branched released. Drivers that return TRUE
	// in this function will only show up in Development builds, whether or not
	// the source has already been included in the shipping branches. This
	// can be overidden on the command line with the -beta switch.

	#if C3_LEVEL > 0

	if( !m_fAllowBeta ) {

		switch( uIdent ) {

			// Unreleased Drivers
			case 0x4086: return TRUE;	// Siemens S7 Extended DB TCP/IP
			case 0x4092: return TRUE;	// Toshiba EX40+
		}
	}

	#endif

	return FALSE;
}

// Selection API

DLLAPI UINT C3SelectDriver(HWND hWnd, UINT &ID, UINT uBinding, UINT uBindMask)
{
	CDriverPickerDialog Dlg(ID, uBinding, uBindMask);

	if( Dlg.Execute(CWnd::FromHandle(hWnd)) ) {

		ID = Dlg.GetIdent();

		return TRUE;
	}

	return FALSE;
}

// Selection Filter

DLLAPI BOOL C3IsDriverAllowed(UINT ID)
{
	switch( ID ) {

		// Program Thru
		case 0x3705:
		case 0x3706:
		case 0x4072:
			return CDriverPickerDialog::m_uAllowGroup >= SW_GROUP_3B;

		// BacNet
		case 0x402A:
		case 0x4031:
			return CDriverPickerDialog::m_uAllowGroup >= SW_GROUP_3B;

		// BFD
		case 0x3703:
		case 0x3708:
			return FALSE;

		// DNP3
		case 0x40C0:
		case 0x40C1:
		case 0x40C2:
		case 0x40C3:
		case 0x40E6:
		case 0x40E7:
		{
			BOOL fFlexEdge = (CDriverPickerDialog::m_AllowModel.StartsWith(L"DA50|") || CDriverPickerDialog::m_AllowModel.StartsWith(L"DA70|"));

			UINT uTarget   = fFlexEdge ? SW_GROUP_2 : SW_GROUP_3C;

			return CDriverPickerDialog::m_uAllowGroup >= uTarget;
		}

		// Other Cameras
		case 0x3F01:
		case 0x40A9:
		case 0x3F05:
		case 0x3F04:
		case 0x40A4:
			return CDriverPickerDialog::m_uAllowGroup >= SW_GROUP_3C;

		// Generic Camera
		case 0x40D3:
			return CDriverPickerDialog::m_uAllowGroup >= SW_GROUP_3A;

		// OPC UA Client
		case 0x40E2:
			return CDriverPickerDialog::m_uAllowGroup == SW_GROUP_2 || CDriverPickerDialog::m_uAllowGroup >= SW_GROUP_3B;
	}

	return TRUE;
}

// End of File
