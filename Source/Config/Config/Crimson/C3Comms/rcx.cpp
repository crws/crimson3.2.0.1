
#include "intern.hpp"

#include "rcx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYamahaRcxDeviceOptions, CUIItem);

// Constructor

CYamahaRcxDeviceOptions::CYamahaRcxDeviceOptions(void)
{

	}


//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYamahaRcxTCPDeviceOptions, CUIItem);       

// Constructor

CYamahaRcxTCPDeviceOptions::CYamahaRcxTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 2, 1), MAKEWORD( 168, 192)));;

	m_Socket = 2000;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_Login  = 1;

	m_User   = "USER";

	m_PWord  = "PASSWORD"; 

	m_Echo   = 0;
	}


// UI Managament

void CYamahaRcxTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "Login" ) {

			BOOL fEnable = pItem->GetDataAccess("Login")->ReadInteger(pItem) ? FALSE : TRUE;

			pWnd->EnableUI("User",  fEnable);

			pWnd->EnableUI("PWord", fEnable);
			}
		}
	}

// Download Support

BOOL CYamahaRcxTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_Login));
	Init.AddText(m_User);
	Init.AddText(m_PWord);
	Init.AddByte(BYTE(m_Echo));

	return TRUE;
	}

// Meta Data Creation

void CYamahaRcxTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Login);
	Meta_AddString(User);
	Meta_AddString(PWord);
	Meta_AddInteger(Echo);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master Driver
//

// Instantiator

ICommsDriver *	Create_YamahaRcxDriver(void)
{
	return New CYamahaRcxDriver;
	}

// Constructor

CYamahaRcxDriver::CYamahaRcxDriver(void)
{
	m_wID		= 0x4078;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yamaha";
	
	m_DriverName	= "RCX Series";
	
	m_Version	= "1.05";
	
	m_ShortName	= "RCX";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Binding Control

UINT	CYamahaRcxDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CYamahaRcxDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
 	}

// Configuration

CLASS CYamahaRcxDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYamahaRcxDeviceOptions);
	}

// Address Management

BOOL CYamahaRcxDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CYamahaRcxDialog Dlg(ThisObject, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CYamahaRcxDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uFind  = Text.Find(':');

	BOOL fRobot = FALSE;

	BOOL fDir   = FALSE;

	UINT uAxis  = 0;

	if( uFind < NOTHING ) {

		if( HasRobot(pSpace) ) {

			if( IsAxis(pSpace) ) {

				switch( Text[uFind - 1] ) {

					case 'X':	Text.SetAt(uFind - 1, '1');	break;
					case 'Y':	Text.SetAt(uFind - 1, '2');	break;
					case 'Z':	Text.SetAt(uFind - 1, '3');	break;
					case 'R':	Text.SetAt(uFind - 1, '4');	break;
					case 'A':	Text.SetAt(uFind - 1, '5');	break;
					case 'B':	Text.SetAt(uFind - 1, '6');	break;
					}
				}

			if( IsMove(pSpace) ) {

				switch( Text[uFind - 1] ) {

					case 'P' :	Text.SetAt(uFind - 1, '1');			break;
					case 'L' :	Text.SetAt(uFind - 1, '2');	fDir = TRUE;	break;
					}
				}

			if( uFind + 1 < Text.GetLength() ) {

				switch( Text[uFind + 1] ) {

					case 'S':	fRobot = TRUE;	break;
					case 'M':			break;
					
					default: 
						
						Error.Set("Invalid robot identifier", 0);
						return FALSE;
					
					}

				if( HasDirection(pSpace) && uFind + 2 < Text.GetLength() ) {

					switch( Text[uFind + 2] ) {

						case 'M':	fDir = TRUE;	break;
						case 'P':			break;

						default: 

							Error.Set("Invalid direction identifier", 0);
							return FALSE; 
						}
					}
				}
			}

		else if( IsSpecial(pSpace) ) {
			
			switch( Text[uFind + 1] ) {

				case 'X':	uAxis = 0;	break;
				case 'Y':	uAxis = 1;	break;
				case 'Z':	uAxis = 2;	break;
				case 'R':	uAxis = 3;	break;
				case 'A':	uAxis = 4;	break;
				case 'B':	uAxis = 5;	break;
				}
			}

		Text = Text.Left(uFind);
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		if( IsSpecial(pSpace) ) {

			Addr.a.m_Extra  = (Addr.a.m_Offset & 0x7FFF) >> 12;

			Addr.a.m_Offset = (Addr.a.m_Offset << 3) + uAxis;
			}
		else {
			Addr.a.m_Extra = (fRobot | (fDir << 1));
			}		

		return TRUE;
		}

	return FALSE;
	}

BOOL CYamahaRcxDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);
	
	if( pSpace ) {

		Text = pSpace->m_Prefix;

		if( pSpace->m_uTable < addrNamed ) {

			if( IsSpecial(pSpace) ) {

				UINT uOffset = (Addr.a.m_Extra << 12) | (Addr.a.m_Offset >> 3);

				Text += pSpace->GetValueAsText(uOffset);

				Text += ":";

				Text += sAxis[(Addr.a.m_Offset & 0x7) % elements(sAxis)];

				return TRUE;
				}

			if( IsAxis(pSpace) ) {

				Text += sAxis[(Addr.a.m_Offset - 1) % elements(sAxis)];
				}
			
			else if( IsMove(pSpace) ) {

				Text += sMove[(Addr.a.m_Offset - 1) % elements(sMove)];
				}
			else {
				Text += pSpace->GetValueAsText(Addr.a.m_Offset);
				}
			}
		
		if( HasRobot(pSpace) ) {

			Text += ":";

			Text += ((Addr.a.m_Extra & 0x1) ? "S" : "M");
			
			if( HasDirection(pSpace) ) {

				Text +=	((Addr.a.m_Extra & 0x2) ? "M" : "P");
				}
			} 

		return TRUE;
		}

	return FALSE;
	}

// Implementation	

void CYamahaRcxDriver::AddSpaces(void)
{
	AddSpace(New CSpace(	    8,  "DI",		"Parallel Inputs",			 8, 0,127,addrByteAsByte));
	AddSpace(New CSpace(	    9,  "DO",		"Parallel Outputs",			 8, 0,87, addrByteAsByte));
	AddSpace(New CSpace(	   10,  "MO",		"Internal Auxiliary Outputs",		 8, 0,23, addrByteAsByte));
	AddSpace(New CSpace(	   11,  "TO",		"Timer Outputs",			 8, 0, 0, addrByteAsByte));
	AddSpace(New CSpace(	   12,  "LO",		"Arm Lock Outputs",			 8, 0, 0, addrByteAsByte));
	AddSpace(New CSpace(	   13,  "SI",		"Serial Inputs",			 8, 0,23, addrByteAsByte));
	AddSpace(New CSpace(	   14,  "SO",		"Serial Outputs",			 8, 0,23, addrByteAsByte));
	AddSpace(New CSpace(	   15,  "SIW",		"Serial Word Input",			10, 2,15, addrWordAsWord));
	AddSpace(New CSpace(	   16,  "SOW",		"Serial Word Output",			10, 2,15, addrWordAsWord));
	AddSpace(New CSpace(	   17,	"SGI",		"SG Integer Variables",			10, 0, 7, addrLongAsLong));
	AddSpace(New CSpace(	   18,  "SGR",		"SG Real Variables",			10, 0, 7, addrRealAsReal));
	/////////////////////////////////////////////////////////////////////////////
	AddSpace(New CSpace(addrNamed,  "AUTO",		"Set AUTO Mode",			10, 1, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"PROG",		"Set PROGRAM Mode",			10, 2, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"MAN",		"Set MANUAL Mode",			10, 3, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "SYS",		"Set SYSTEM Mode",			10, 4, 0, addrBitAsBit ));

	AddSpace(New CSpace(addrNamed,  "RESET",	"Reset Program",			10, 5, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "RUN",		"Execute Program",			10, 6, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "STEP",		"Execute Single Program Line",		10, 7, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"SKIP",		"Skip Single Program Line",		10, 8, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "NEXT",		"Execute to Next Program Line",		10, 9, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "STOP",		"Stop Program",				10,10, 0, addrBitAsBit ));
	////////////////////////////////////////////////////////////////////////////
	AddSpace(New CSpace(addrNamed,  "BRKPT",	"Break Point Number",			10,14, 0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed,  "BRKLN",	"Break Line Number",			10,15, 0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed,  "BREAK",	"Set Break Point",			10,16, 4, addrBitAsBit ));
	
//	AddSpace(New CSpace(addrNamed,	"LANG",		"Set Display Language",			10,18, 0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,	"UNIT",		"Set Coordinates and Units",		10,19, 0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,	"ACCESS",	"Set Access Level",			10,20, 0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,	"EXELVL",	"Set Execution Level",			10,21, 0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,	"SEQUENCE",	"Set Sequence Program Execution Flag",	10,22, 0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "EMGRST",	"Reset Emergency Stop Flag",		10,23, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "CHGTSK",	"Switch Execution Task",		10,11, 0, addrBitAsBit ));
	
	AddSpace(New CSpace(addrNamed,  "MSPEED",	"Manual Speed",				10,12, 0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "ASPEED",	"Automatic Speed",			10,26, 0, addrByteAsByte)); 
	AddSpace(New CSpace(        2,  "ABSADJ",	"Move To Absolute Reset Position",	10, 1, 6, addrBitAsBit));
	AddSpace(New CSpace(	    3,  "ABSRST",	"Absolute Reset",			10, 1, 6, addrBitAsBit));
	AddSpace(New CSpace(addrNamed,	"ABSRST_A",	"Absolute Reset All Axis",		10,30, 0, addrBitAsBit));
	AddSpace(New CSpace(	    4,  "ORGRTN",	"Return To Origin",			10, 1, 6, addrBitAsBit));
	AddSpace(New CSpace(	    5,  "INCH",		"Manual Inching",			10, 1, 6, addrBitAsBit));
	AddSpace(New CSpace(	    6,  "JOG",		"Manual Jog",				10, 1, 6, addrBitAsBit));
	AddSpace(New CSpace(	   21,	"WHERE",	"Current Position (pulse units)",	10, 1, 6, addrLongAsLong));
	AddSpace(New CSpace(	   22,  "WHRXY",	"Current Position (XY coords)",		10, 1, 6, addrRealAsReal));
	AddSpace(New CSpace(	   23,  "LANGS",	"Display Language",			10, 0,80, addrByteAsByte)); 
	AddSpace(New CSpace(	   24,	"ACCESSL",	"Access Level",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   25,	"ARM",		"Arm Status",				10, 0,80, addrByteAsByte)); 
	AddSpace(New CSpace(	   26,  "BREAKS",	"Break Point Status",			10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   27,	"CONFIG",	"Configuration Status",			10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   28,	"EXELVLL",	"Execution Level",			10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   29,  "MODE",		"Mode Status",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   30,  "ORIGIN",	"Return-To-Origin Status",		10, 0,80, addrByteAsByte)); 
	AddSpace(New CSpace(	   31,  "ABSRSTS",	"Absolute Reset Status",		10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   32,  "SERVO",	"Servo Status",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   33,  "SEQS",		"Sequence Program Execution Status",	10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   34,	"SPEED",	"Speed Status",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   35,  "UNITS",	"Coordinates and Units",		10, 0,80, addrByteAsByte)); 
	AddSpace(New CSpace(	   36,  "VER",		"Version Info",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   37,  "TASKS",	"Tasks in RUN or SUSPEND Status",	10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   38,	"TSKMON",	"Tasks Operation Status",		10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   39,	"SHIFT",	"Shift Status",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   40,	"HAND",		"Hand Status",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   41,  "MEM",		"Remaing Memory Capacity",		10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   42,	"PADDR",	"Program Execution Status",		10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,	"EMG",		"Emergency Stop Status",		10,17, 0, addrByteAsByte)); 
	AddSpace(New CSpace(addrNamed,  "TEACH",	"Point Data Teaching",			10,13, 0, addrWordAsWord));
	AddSpace(New CSpace(	   45,	"SERVOC",	"Servo Command",			10, 1, 6, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,	"SERVOC_A",	"Servo Command All Axis",		10,31, 0, addrBitAsBit));
	AddSpace(New CSpace(	   46,	"MOVE",		"Absolute Movement Command",	        10, 1, 2, addrWordAsWord));				
	AddSpace(New CSpace(       43,	"DRIVEI",	"Robot Relative Movement",		10, 1, 6, addrRealAsReal));
	AddSpace(New CSpace(       44,  "PMOVE",	"Pallet Movement",			10, 0,19, addrWordAsWord)); 
	AddSpace(New CSpace(	   47,	"Pn",		"Current Point Table Position",		10,0,9999,addrRealAsReal));	
	AddSpace(New CSpace(	   48,  "PnSet",	"Set Point Table Position",		10,0,9999,addrRealAsReal)); 
	AddSpace(New CSpace(	   50,  "PnHStat",	"Point Table Hand Status",		10,0,9999,addrByteAsByte));
	AddSpace(New CSpace(	   51,	"PnHSet",	"Set Point Table Hand Status Flag",	10,0,9999,addrByteAsByte));
	AddSpace(New CSpace(       49,  "PnCmd",	"Send Pn Command",			10,0,9999,addrBitAsBit));
	////////////////////////////////////////////////////////////////////////////
	AddSpace(New CSpace(addrNamed,  "DATE",		"Sync Date",				10,24, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "TIME",		"Sync Time",				10,25, 0, addrBitAsBit ));
		////////////////////////////////////////////////////////////////////////////
	AddSpace(New CSpace(	    7,  "SWI",		"Program Switching",			10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	    1,  "ERROR",	"Latest Error Message",			10, 0,64, addrLongAsLong));
	////////////////////////////////////////////////////////////////////////////

	AddSpace(New CSpace(	   19,  "USER",		"User Command",				10, 0,80, addrByteAsByte));
	AddSpace(New CSpace(	   20,  "RESP",		"User Response",			10, 0,80, addrByteAsByte)); 	
	AddSpace(New CSpace(addrNamed,	"CTRLC",	"Execution Language Interruption",	10,29, 0, addrBitAsBit)); 
	}

// Access

BOOL CYamahaRcxDriver::HasRobot(CSpace * pSpace)
{
	if( pSpace ) {	  

		if( pSpace->m_uTable < addrNamed ) {

			switch( pSpace->m_uTable ) {

				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 21:
				case 22:
				case 43:
				case 44:
				case 45:
				case 46:
								
					return TRUE;
				}
			
			return FALSE;
			}

		switch( pSpace->m_uMinimum ) {

			case 12:
			case 13:
			case 26:
			

				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYamahaRcxDriver::HasDirection(CSpace * pSpace)
{
	if( pSpace ) {

		if( pSpace->m_uTable < addrNamed ) {

			switch( pSpace->m_uTable ) {

				case 2:
				case 5:
				case 6:			
					return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CYamahaRcxDriver::IsString(UINT uTable)
{
	switch( uTable ) {

		case  1:
		case  7:
		case 19:
		case 20:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:
		case 32:
		case 33:
		case 34:
		case 35:
		case 36:
		case 37:
		case 38:
		case 39:
		case 40:
		case 41:
		case 42:
	
			return TRUE;
		}

	return FALSE;
	}

BOOL CYamahaRcxDriver::IsAxis(CSpace * pSpace)
{
	if( pSpace ) {

		if( pSpace->m_uTable < addrNamed ) {

			switch( pSpace->m_uTable ) {

				case 5:
				case 6:
				case 21:
				case 22:
				case 47:
				case 48:
							
					return TRUE;
				}
			}
		}

	return FALSE;
  	}

BOOL CYamahaRcxDriver::IsMove(CSpace * pSpace)
{
	if( pSpace ) {

		if( pSpace->m_uTable < addrNamed ) {

			switch( pSpace->m_uTable ) {

				case 46:
					return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CYamahaRcxDriver::IsSpecial(CSpace * pSpace)
{
	if( pSpace ) {

		if( pSpace->m_uTable < addrNamed ) {

			switch( pSpace->m_uTable ) {

				case 47:
				case 48:
					return TRUE;
				}
			}
		}

	return FALSE;
     	}

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master TCP/IP Driver
//

// Instantiator

ICommsDriver * Create_YamahaRcxTcpDriver(void)
{
	return New CYamahaRcxTcpDriver;
	}

// Constructor

CYamahaRcxTcpDriver::CYamahaRcxTcpDriver(void)
{
	m_wID		= 0x407A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yamaha";
	
	m_DriverName	= "RCX Series TCP/IP Master";
	
	m_Version	= "1.05";
	
	m_ShortName	= "RCX";

	AddSpaces();
	}

// Binding Control

UINT CYamahaRcxTcpDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYamahaRcxTcpDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYamahaRcxTcpDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYamahaRcxTCPDeviceOptions);
	}


// Implementation

void CYamahaRcxTcpDriver::AddSpaces(void)
{
	AddSpace(New CSpace(addrNamed,  "LOGIN",	"Login to Server",		10,27, 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "LOGOFF",	"Logoff of Server",		10,28, 0, addrBitAsBit ));
	}

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CYamahaRcxDialog, CStdAddrDialog);
		
// Constructor

CYamahaRcxDialog::CYamahaRcxDialog(CYamahaRcxDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "YamahaRcxElementDlg";
	}

// Overridables

void CYamahaRcxDialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CYamahaRcxDialog::SetAddressText(CString Text)
{
	EnableRobot();

	EnableDirection();

	GetDlgItem(2002).ShowWindow(FALSE);

	GetDlgItem(2003).ShowWindow(FALSE);

	GetDlgItem(2011).ShowWindow(FALSE);

	if( m_pSpace ) {

		CYamahaRcxDriver * pDriver = (CYamahaRcxDriver *)m_pDriver;

		BOOL fEnable = (m_pSpace->m_uTable < addrNamed);

		if( pDriver ) {

			if( fEnable ) {

				fEnable = !pDriver->IsString(m_pSpace->m_uTable);

				if( fEnable && (pDriver->IsAxis(m_pSpace) || pDriver->IsMove(m_pSpace)) ) {

					BOOL fAxis = pDriver->IsAxis(m_pSpace);

					UINT uID   = pDriver->IsSpecial(m_pSpace) ? 2011 : 2003;
			  
					CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

					Combo.ResetContent();

					UINT uFind = Text.Find(':');

					UINT uElements = fAxis ? elements(sAxis) : elements(sMove);

					for( UINT  u = 0; u < uElements; u++ ) {

						Combo.AddString(fAxis ? sAxis[u] : sMove[u]);
						}

					for( UINT  i = 0; i < uElements; i++ ) {

						CString Format = "%s";

						if( uFind < NOTHING ) {

							if( pDriver->IsSpecial(m_pSpace) ) {

								Format.Printf(Format, Text.Mid(uFind + 1, 1));
								}
							else {
								Format.Printf(Format, Text.Mid(0, uFind));
								}

							if( CString(fAxis ? sAxis[i] : sMove[i]) == Format ) {

								Combo.SetCurSel(i);

								break;								
								}
							}
						}

					GetDlgItem(2002).ShowWindow(pDriver->IsSpecial(m_pSpace));

					Combo.ShowWindow(fEnable);

					fEnable = pDriver->IsSpecial(m_pSpace);
					}
				else {
					GetDlgItem(2003).ShowWindow(FALSE);

					GetDlgItem(2002).ShowWindow(fEnable);

					GetDlgItem(2011).ShowWindow(FALSE);
					}
				}
			}

		GetDlgItem(2002).EnableWindow(fEnable);

		fEnable = !Text.IsEmpty();

		if( fEnable ) {

			UINT uFind = Text.Find(':');

			if( uFind < NOTHING ) {

				GetDlgItem(2002).SetWindowText(Text.Left(uFind));

				uFind++;

				if( uFind < Text.GetLength() ) {

					SetRadioGroup(2005, 2006, Text[uFind] == 'S');

					uFind++;

					if( uFind < Text.GetLength() ) {

						SetRadioGroup(2009, 2010, Text[uFind] == 'M');
						}
					}
				
				return;
				}

			GetDlgItem(2002).SetWindowText(Text);
		       
			return;
			}
		}
	}

CString CYamahaRcxDialog::GetAddressText(void)
{
	CString Text;
	
	CYamahaRcxDriver * pDriver = (CYamahaRcxDriver *)m_pDriver;

	if( pDriver ) {	

		if( (!pDriver->IsAxis(m_pSpace) && !pDriver->IsMove(m_pSpace)) || pDriver->IsSpecial(m_pSpace) ) {

			Text += GetDlgItem(2002).GetWindowText();

			CComboBox &Combo = (CComboBox &) GetDlgItem(2011);

			if( pDriver->IsSpecial(m_pSpace) ) {

				Text += ":";

				Text += sAxis[Combo.GetCurSel()];

				return Text;
				}
			}
		else {
			BOOL fAxis = pDriver->IsAxis(m_pSpace);

			CComboBox &Combo = (CComboBox &) GetDlgItem(2003);

			UINT uSel = Combo.GetCurSel();

			Text += (fAxis ? sAxis[uSel] : sMove[uSel]);  
			}		

		if( GetDlgItem(2005).IsEnabled() ) {

			Text += ":";

			Text += (GetRadioGroup(2005, 2006) ? "S" : "M");

			if( GetDlgItem(2009).IsEnabled() ) {

				Text += (GetRadioGroup(2009, 2010) ? "M" : "P");
				}
			}
		}

	return Text;
	}

void CYamahaRcxDialog::EnableRobot(void)
{
	BOOL fEnable = HasRobot();

	EnableGroup(fEnable, 2004);
	}

void CYamahaRcxDialog::EnableDirection(void)
{
	BOOL fEnable = HasDirection();

	EnableGroup(fEnable, 2008);
	}

void CYamahaRcxDialog::EnableGroup(BOOL fEnable, UINT uID)
{
	for( UINT u = uID; u < uID + 3; u++ ) {

		GetDlgItem(u).ShowWindow(fEnable);
		}
	}

BOOL CYamahaRcxDialog::HasRobot(void)
{
	CYamahaRcxDriver * pDriver = (CYamahaRcxDriver *)m_pDriver;

	if( pDriver ) {

		return pDriver->HasRobot(m_pSpace);
		}

	return FALSE;
	}


BOOL CYamahaRcxDialog::HasDirection(void)
{
	CYamahaRcxDriver * pDriver = (CYamahaRcxDriver *)m_pDriver;

	if( pDriver ) {

		return pDriver->HasDirection(m_pSpace);
		}

	return FALSE;
	}

// End of File