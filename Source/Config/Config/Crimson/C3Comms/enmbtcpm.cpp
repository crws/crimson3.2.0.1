
#include "intern.hpp"

#include "enmbtcpm.hpp"

#include "mbtcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Encapsulated Modbus Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEnModbusMasterTCPDeviceOptions, CUIItem);       

// Constructor

CEnModbusMasterTCPDeviceOptions::CEnModbusMasterTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_fDisable16	= FALSE;
	
	m_fDisable15	= FALSE;

	m_fDisable5	= FALSE;

	m_fDisable6	= FALSE;

	m_fMode		= FALSE;

	m_PingReg	= 1;

	m_Max01		= 512;
	
	m_Max02		= 512;
	
	m_Max03		= 32;
	
	m_Max04		= 32;
	
	m_Max15		= 512;
	
	m_Max16		= 32;

	SetPages(2);
       	}

// UI Managament

void CEnModbusMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		
		if( Tag.IsEmpty() || Tag == "Disable5" ) {

			pWnd->EnableUI("Disable15", !m_fDisable5);
			}

		if( Tag.IsEmpty() || Tag == "Disable6" ) {

			pWnd->EnableUI("Disable16", !m_fDisable6);
			}
	
		if( Tag.IsEmpty() || Tag == "Disable15" ) {

			pWnd->EnableUI("Max15", !m_fDisable15 && !m_fDisable5);

			pWnd->EnableUI("Disable5", !m_fDisable15);
			}

		if( Tag.IsEmpty() || Tag == "Disable16" ) {

			pWnd->EnableUI("Max16", !m_fDisable16 && !m_fDisable6);

			pWnd->EnableUI("Disable6", !m_fDisable16);
	      		}
		}
	}

// Download Support

BOOL CEnModbusMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_fDisable15));
	Init.AddByte(BYTE(m_fDisable16));
	Init.AddByte(BYTE(m_fDisable5));
	Init.AddByte(BYTE(m_fDisable6)); 
	Init.AddByte(BYTE(m_fMode));
	Init.AddWord(WORD(m_PingReg));
	Init.AddWord(WORD(m_Max01));
	Init.AddWord(WORD(m_Max02));
	Init.AddWord(WORD(m_Max03));
	Init.AddWord(WORD(m_Max04));
	Init.AddWord(WORD(m_Max15));
	Init.AddWord(WORD(m_Max16));
 	
	return TRUE;
	}

// Meta Data Creation

void CEnModbusMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddBoolean(Disable15);
	Meta_AddBoolean(Disable16);
	Meta_AddBoolean(Disable5);
	Meta_AddBoolean(Disable6);
	Meta_AddBoolean(Mode);
	Meta_AddInteger(PingReg);
	Meta_AddInteger(Max01);
	Meta_AddInteger(Max02);
	Meta_AddInteger(Max03);
	Meta_AddInteger(Max04);
	Meta_AddInteger(Max15);
	Meta_AddInteger(Max16);

       	}
 
//////////////////////////////////////////////////////////////////////////
//
// Encapsulated Modbus Master TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_EnModbusMasterTCPDriver(void)
{
	return New CEnModbusMasterTCPDriver;
	}

// Constructor

CEnModbusMasterTCPDriver::CEnModbusMasterTCPDriver(void)
{
	m_wID		= 0x350A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "Encapsulated Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Encapsulated Master";

	AddSpaces();
	}

// Destructor

CEnModbusMasterTCPDriver::~CEnModbusMasterTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CEnModbusMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEnModbusMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CEnModbusMasterTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CEnModbusMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEnModbusMasterTCPDeviceOptions);
	}

// Implementation

void CEnModbusMasterTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "4",  "Holding Registers",	   10, 1, 65535, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(2, "3",  "Analog Inputs",		   10, 1, 65535, addrWordAsWord, addrWordAsReal));
					  
	AddSpace(New CSpace(3, "0",  "Digital Coils",		   10, 1, 65535, addrBitAsBit));
	
	AddSpace(New CSpace(4, "1",  "Digital Inputs",		   10, 1, 65535, addrBitAsBit));

	}

BOOL CEnModbusMasterTCPDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
		case 2:
		case 5:
		case 6:

			return FALSE;
		}

	return TRUE;
	}


// End of File
