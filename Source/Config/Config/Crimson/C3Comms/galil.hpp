
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GALIL_HPP
	
#define	INCLUDE_GALIL_HPP

#include "ylegend.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Galil Serial Driver : Yaskawa Universal SMC Driver
//

class CGalilSerialDriver : public CYaskawaUnivSMCDriver
{
	public:
		// Constructor
		CGalilSerialDriver(void);
		
	protected:
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Galil TCP Driver : Yaskawa Universal SMC TCP/IP Master Driver
//

class CGalilTCPDriver : public CYaskawaSMCTCPDriver
{
	public:
		// Constructor
		CGalilTCPDriver(void);
	};

// End of File

#endif
