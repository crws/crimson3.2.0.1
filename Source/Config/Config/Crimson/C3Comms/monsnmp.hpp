
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MONSNMP_HPP
	
#define	INCLUDE_MONSNMP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver Options
//

class CMonicoSNMPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMonicoSNMPDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT    m_Port1;
		UINT    m_Port2;
		CString m_Comm;
		UINT	m_AclAddr1;
		UINT	m_AclMask1;
		UINT	m_AclAddr2;
		UINT	m_AclMask2;
		UINT	m_TrapFrom;
		UINT	m_TrapAddr1;
		UINT	m_TrapMode1;
		UINT	m_TrapAddr2;
		UINT	m_TrapMode2;
		UINT	m_SysDefault;
		CString	m_SysDescr;
		CString	m_SysContact;
		CString	m_SysName;
		CString	m_SysLocation;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(CUIViewWnd *pWnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver
//

class CMonicoSNMPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMonicoSNMPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver v2.0
//

class CMonicoSNMPDriver2 : public CMonicoSNMPDriver
{
	public:
		// Constructor
		CMonicoSNMPDriver2(void);

		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
