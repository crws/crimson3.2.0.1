
#include "intern.hpp"

#include "appliedm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Applied Motion Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CAppliedMotionDeviceOptions, CUIItem);

// Constructor

CAppliedMotionDeviceOptions::CAppliedMotionDeviceOptions(void)
{
	m_Drop		= TEXT("1");

	m_fPM2Disable	= FALSE;
	}

// UI Management

void CAppliedMotionDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == TEXT("Drop") ) {

		if( InvalidDropChar(m_Drop[0]) ) {
			
			m_Drop = TEXT("1");
			}

		pWnd->UpdateUI(TEXT("Drop"));
		}
	}

// Download Support

BOOL CAppliedMotionDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop[0]));
	Init.AddByte(BYTE(m_fPM2Disable));

	return TRUE;
	}

// Meta Data Creation

void CAppliedMotionDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Drop);
	Meta_AddBoolean(PM2Disable);
	}

// Helper

BOOL CAppliedMotionDeviceOptions::InvalidDropChar(TCHAR c)
{
	if( c < '!' || c > '@' ) {
		
		return TRUE;
		}

	return c == '%' || c == '*' || c == '?';
	}

//////////////////////////////////////////////////////////////////////////
//
// Applied Motion Comms Driver
//

// Instantiator

ICommsDriver *	Create_AppliedMotionDriver(void)
{
	return New CAppliedMotionDriver;
	}

// Constructor

CAppliedMotionDriver::CAppliedMotionDriver(void)
{
	m_wID		= 0x3399;

	m_uType		= driverMaster;
	
	m_Manufacturer	= TEXT("Applied Motion");
	
	m_DriverName	= TEXT("SCL");
	
	m_Version	= TEXT("1.10");
	
	m_ShortName	= TEXT("Applied Motion SCL");

	AddSpaces();
	}

// Binding Control

UINT  CAppliedMotionDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void  CAppliedMotionDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CAppliedMotionDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CAppliedMotionDeviceOptions);
	}

// Address Management

BOOL CAppliedMotionDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	if( Addr.a.m_Table >= 200 && Addr.a.m_Table < AN ) return FALSE;

	CAppliedMotionAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL  CAppliedMotionDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Type   = pSpace->m_uType;

	ValidateListSelect(pSpace->m_uTable, pSpace->m_uMinimum);

	Addr.a.m_Extra  = m_uListSelect;

	UINT uOffset    = pSpace->m_uMinimum;

	if( pSpace->m_uTable != AN && pSpace->m_uTable != SGL ) {

		UINT uLetter = IsLetterSelect(pSpace);

		CString sE;

		switch( uLetter ) {

			case ISREGLETTER:

				if( isdigit(Text[0]) && Text.GetLength() > 1 ) {

					uOffset = tatoi(Text);
					}

				else uOffset = (UINT)Text[0];
				break;

			case ISINPLETTER:

				Text.MakeUpper();

				sE = TEXT("FHLR");

				if( sE.Find(Text[0]) == NOTHING ) {

					Error.Set(TEXT("F H L R"), 0);

					return FALSE;
					}

				uOffset = (UINT)Text[0];
				break;

			case ISNOTLETTER:
			default:
				uOffset = tatoi(Text);
				break;
			}

		if( uOffset < pSpace->m_uMinimum || uOffset > pSpace->m_uMaximum ) {

			sE.Printf( TEXT("%s%d - %s%d"),

				pSpace->m_Prefix,
				pSpace->m_uMinimum,
				pSpace->m_Prefix,
				pSpace->m_uMaximum
				);

			Error.Set( sE, 0 );

			return FALSE;
			}
		}

	Addr.a.m_Offset = uOffset;

	return TRUE;
	}

BOOL  CAppliedMotionDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( pSpace->m_uTable == AN || pSpace->m_uTable == SGL ) {

		Text.Printf( TEXT("%s"),
			pSpace->m_Prefix
			);
		}

	else {
		CString sO	= TEXT("");
		CString sD	= TEXT("");

		UINT uOff	= Addr.a.m_Offset;

		UINT u		= LOBYTE(uOff);

		switch( IsLetterSelect(pSpace) ) {

			case ISNOTLETTER: sO.Printf( TEXT("%d"), uOff );	break;

			case ISREGLETTER:

				switch( uOff ) {

					case '.': sD.Printf( TEXT("_Period") );		break;
					case '[': sD.Printf( TEXT("_Open_Sqr_Bkt") );	break;
					case ']': sD.Printf( TEXT("_Close_Sqr_Bkt") );	break;
					case '(': sD.Printf( TEXT("_Open_Rnd_Bkt") );	break;
					case ')': sD.Printf( TEXT("_Close_Rnd_Bkt") );	break;
					case '{': sD.Printf( TEXT("_Open_Brace") );	break;
					case '}': sD.Printf( TEXT("_Close_Brace") );	break;
					case '|': sD.Printf( TEXT("_Vert_Bar") );	break;
					case '~': sD.Printf( TEXT("_Tilde") );		break;
					default:
						if( uOff >= '!' && uOff <= 'z' ) sD.Printf( TEXT("_%c"), u );
						break;
					}

				sO.Printf( TEXT("%3.3d%s"), uOff, sD );

				break;

			case ISINPLETTER:

				switch( u ) {

					case 'F': sO = TEXT("Falling");	break;
					case 'H': sO = TEXT("High");		break;
					case 'L': sO = TEXT("Low");		break;
					case 'R': sO = TEXT("Rising");	break;
					}
				break;
			}

		Text.Printf( TEXT("%s%s"),

			pSpace->m_Prefix,
			sO
			);
		}

	return TRUE;
	}

// Dialog Support
void  CAppliedMotionDriver::AddHeaderSpaces(UINT uTop)
{
	if( uTop & LST01 )
		AddSpace(New CSpace(FNCMOT, TEXT(""), TEXT("Motion Commands..."),		10,   0,   0, LL));
	if( uTop & LST02 )
		AddSpace(New CSpace(FNCCON, TEXT(""), TEXT("Configuration Commands..."),	10,   0,   0, LL));
	if( uTop & LST04 )
		AddSpace(New CSpace(FNCIO,  TEXT(""), TEXT("I/O Commands..."),			10,   0,   0, LL));
	if( uTop & LST08 )
		AddSpace(New CSpace(FNCCOM, TEXT(""), TEXT("Communication Commands..."),	10,   0,   0, LL));
	if( uTop & LST10 )
		AddSpace(New CSpace(FNCQ,   TEXT(""), TEXT("Q Program Commands..."),		10,   0,   0, LL));
	if( uTop & LST20 )
		AddSpace(New CSpace(FNCDRV, TEXT(""), TEXT("Drive Commands..."),		10,   0,   0, LL));
	if( uTop & LST40 )
		AddSpace(New CSpace(FNCREG, TEXT(""), TEXT("Register Commands..."),		10,   0,   0, LL));
	if( uTop & LST80 )
		AddSpace(New CSpace(FNCIMM, TEXT(""), TEXT("Immediate Commands..."),		10,   0,   0, LL));
	}

void  CAppliedMotionDriver::AddDialogSpaces(BOOL fPart)
{
	DeleteAllSpaces();

	if( fPart ) {

		AddSpaces();
		return;
		}

	AddHeaderSpaces(LST01);
	AddMotionSpaces();

	AddHeaderSpaces(LST02);
	AddConfigSpaces();

	AddHeaderSpaces(LST04);
	AddIOSpaces();

	AddHeaderSpaces(LST08);
	AddCommSpaces();

	AddHeaderSpaces(LST10);
	AddQSpaces();

	AddHeaderSpaces(LST20);
	AddDriveSpaces();

	AddHeaderSpaces(LST40);
	AddRegisterSpaces();

	AddHeaderSpaces(LST80);
	AddImmediateSpaces();
	}

void  CAppliedMotionDriver::SetListSelect(UINT *pListSelect)
{
	switch( *pListSelect ) {

		case LSTCON:	m_uListSelect = EXTCON;	return;
		case LSTIO:	m_uListSelect = EXTIO;	return;
		case LSTCOM:	m_uListSelect = EXTCOM;	return;
		case LSTQ:	m_uListSelect = EXTQ;	return;
		case LSTDRV:	m_uListSelect = EXTDRV;	return;
		case LSTREG:	m_uListSelect = EXTREG;	return;
		case LSTIMM:	m_uListSelect = EXTIMM;	return;
		}

	m_uListSelect = EXTMOT;
	}

UINT  CAppliedMotionDriver::GetListFromTable(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case RCB:
		case RCX:
		case RGD:
		case RGI:
		case RGL:
		case RGM:
		case RGR:
		case RGW:
		case RGX:	return LSTREG;

		case FDA:
		case FDB:
		case FDC:
		case FDD:
		case FEB:
		case FEX:
		case FMB:
		case FMX:
		case FSB:
		case FSX:
		case FYB:
		case FYX:
		case HWB:
		case HWX:
		case SHB:
		case SHX:
		case FOL:
		case FOY:	return LSTMOT;

		case FIR:
		case SOU:
		case SOY:	return LSTIO;

		case SGL:	return LSTCOM;

		case RST:
		case IAV:	return LSTIMM;

		case AN:	return uOffset & LSTALL;
		}

	return 0;
	}

// Implementation

void  CAppliedMotionDriver::AddMotionSpaces(void)
{
	AddSpace(New CSpace(AN, "AC", "   Acceleration",			  10, 0x0101, 000, RR));
	AddSpace(New CSpace(AN, "AM", "   Max Acceleration",			  10, 0x0102, 000, RR));
	AddSpace(New CSpace(AN, "CJ", "   Commence Jogging",			  10, 0x0103, 000, LL));
	AddSpace(New CSpace(AN, "DC", "   Change Distance",			  10, 0x0104, 000, LL));
	AddSpace(New CSpace(AN, "DE", "   Deceleration",			  10, 0x0105, 000, RR));
	AddSpace(New CSpace(AN, "DI", "   Distance/Position",			  10, 0x0106, 000, LL));
	AddSpace(New CSpace(AN, "EG", "   Electronic Gearing",			  10, 0x0107, 000, LL));
	AddSpace(New CSpace(AN, "EP", "   Encoder Position",			  10, 0x2108, 000, LL));
	AddSpace(New CSpace(AN, "FC", "   Feed to Length, Spd Change",		  10, 0x0109, 000, LL));
	AddSpace(New CSpace(AN, "FD", "   Feed Double Sensor (1=FD, 2=FDX)",	  10, 0x019A, 000, LL));
	AddSpace(New CSpace(FDA,"FDA","     FD(X) Input Number 1",		  10,      1,   9, LL));
	AddSpace(New CSpace(FDB,"FDB","     FD(X) Condition 1",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(FDC,"FDC","     FD(X) Input Number 2",		  10,      1,   9, LL));
	AddSpace(New CSpace(FDD,"FDD","     FD(X) Condition 2",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(FEB,"FE", "   Follow Encoder",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(FEX,"FEX","   Follow Encoder, X",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(AN, "FLC","   Feed/Length Command",			  10, 0x010C, 000, LL));
	AddSpace(New CSpace(AN, "FLV","   Feed/Length with Value",		  10, 0x010D, 000, LL));
	AddSpace(New CSpace(FMB,"FM", "   Feed/Sensor, Mask Dist.",		  10,    'F', 'R', LL));
	AddSpace(New CSpace(FMX,"FMX","   Feed/Sensor, Mask Dist. X",		  10,    'F', 'R', LL));
	AddSpace(New CSpace(FOL,"FO", "   Feed/Length + Set Out (1=L, 2=H)",	  10,      1,   4, LL));
	AddSpace(New CSpace(FOY,"FOY","   Feed/Length + Set Out Y (1=L, 2=H)",	  10,      1,   4, LL));
	AddSpace(New CSpace(AN, "FPC","   Feed/Position Command",		  10, 0x0112, 000, LL));
	AddSpace(New CSpace(AN, "FPV","   Feed/Position with Value",		  10, 0x0113, 000, LL));
	AddSpace(New CSpace(FSB,"FS", "   Feed/Sensor",				  10,    'F', 'R', LL));
	AddSpace(New CSpace(FSX,"FSX","   Feed/Sensor, X",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(FYB,"FY", "   Feed/Sensor, Safety Dist.",		  10,    'F', 'R', LL));
	AddSpace(New CSpace(FYX,"FYX","   Feed/Sensor, Safety Dist.X",		  10,    'F', 'R', LL));
	AddSpace(New CSpace(HWB,"HW", "   Hand Wheel",				  10,    'F', 'R', LL));
	AddSpace(New CSpace(HWX,"HWX","   Hand Wheel X",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(AN, "JA", "   Jog Acceleration",			  10, 0x011A, 000, RR));
	AddSpace(New CSpace(AN, "JC", "   Velocity mode second speed",		  10, 0x011B, 000, RR));
	AddSpace(New CSpace(AN, "JD", "   Jog Disable",				  10, 0x011C, 000, LL));
	AddSpace(New CSpace(AN, "JE", "   Jog Enable",				  10, 0x011D, 000, LL));
	AddSpace(New CSpace(AN, "JL", "   Jog Deceleration",			  10, 0x011E, 000, RR));
	AddSpace(New CSpace(AN, "JM", "   Jog Mode",				  10, 0x011F, 000, LL));
	AddSpace(New CSpace(AN, "JS", "   Jog Speed",				  10, 0x0120, 000, RR));
	AddSpace(New CSpace(SHB,"SH", "   Seek Home",				  10,    'F', 'R', LL));
	AddSpace(New CSpace(SHX,"SHX","   Seek Home, X",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(AN, "SJ", "   Stop Jogging",			  10, 0x8123, 000, LL));
	AddSpace(New CSpace(AN, "SM", "   Stop Move (1=SMD, 2=SMM)",		  10, 0x1124, 000, LL));
	AddSpace(New CSpace(AN, "SP", "   Set Position",			  10, 0x0125, 000, LL));
	AddSpace(New CSpace(AN, "ST", "   Stop (1=ST, 2=STD)",			  10, 0x8126, 000, LL));
	AddSpace(New CSpace(AN, "VC", "   Velocity Change",			  10, 0x0127, 000, RR));
	AddSpace(New CSpace(AN, "VE", "   Velocity",				  10, 0x0128, 000, RR));
	AddSpace(New CSpace(AN, "VM", "   Maximum Velocity",			  10, 0x0129, 000, RR));
	}

void  CAppliedMotionDriver::AddConfigSpaces(void)
{
	AddSpace(New CSpace(AN, "BD", "   Brake Disengage Delay",		  10, 0x062A, 000, RR));
	AddSpace(New CSpace(AN, "BE", "   Brake Engage Delay",			  10, 0x062B, 000, RR));
	AddSpace(New CSpace(AN, "CC", "   Change Current",			  10, 0x022C, 000, RR));
	AddSpace(New CSpace(AN, "CD", "   Idle Current Delay Time",		  10, 0x022D, 000, RR));
	AddSpace(New CSpace(AN, "CF", "   Anti-resonance Filter Freq.",		  10, 0x022E, 000, LL));
	AddSpace(New CSpace(AN, "CG", "   Anti-resonance Filter Gain",		  10, 0x022F, 000, LL));
	AddSpace(New CSpace(AN, "CI", "   Change Idle Current",			  10, 0x0230, 000, RR));
	AddSpace(New CSpace(AN, "CM", "   Control Mode (Command Mode)",		  10, 0x0231, 000, LL));
	AddSpace(New CSpace(AN, "CP", "   Change Peak Current",			  10, 0x0232, 000, RR));
	AddSpace(New CSpace(AN, "DA", "   Define Address",			  10, 0x0233, 000, LL));
	AddSpace(New CSpace(AN, "DL", "   Define Limits (X6, X7)",		  10, 0x0634, 000, LL));
	AddSpace(New CSpace(AN, "DR", "   Data Register (Capture)",		  10, 0x4235, 000, LL));
	AddSpace(New CSpace(AN, "ER", "   Encoder Resolution",			  10, 0x0236, 000, LL));
	AddSpace(New CSpace(AN, "HG", "   4th Harmonic Filter Gain",		  10, 0x0237, 000, LL));
	AddSpace(New CSpace(AN, "HP", "   4th Harmonic Filter Phase",		  10, 0x0238, 000, LL));
	AddSpace(New CSpace(AN, "KC", "   Overall Servo Filter",		  10, 0x0239, 000, LL));
	AddSpace(New CSpace(AN, "KD", "   Differential Constant",		  10, 0x023A, 000, LL));
	AddSpace(New CSpace(AN, "KE", "   Differential Filter",			  10, 0x023B, 000, LL));
	AddSpace(New CSpace(AN, "KF", "   Velocity FeedFwd Constant",		  10, 0x023C, 000, LL));
	AddSpace(New CSpace(AN, "KI", "   Integrator Constant",			  10, 0x023D, 000, LL));
	AddSpace(New CSpace(AN, "KK", "   Inertia FeedFwd Constant",		  10, 0x023E, 000, LL));
	AddSpace(New CSpace(AN, "KP", "   Proportional Constant",		  10, 0x023F, 000, LL));
	AddSpace(New CSpace(AN, "KV", "   Velocity Feedback Constant",		  10, 0x0240, 000, LL));
	AddSpace(New CSpace(AN, "KW", "   Velocity Filter",			  10, 0x0241, 000, LL));
	AddSpace(New CSpace(AN, "MO", "   Motion Output (Y2)",			  10, 0x0642, 000, LL));
	AddSpace(New CSpace(AN, "PC", "   Power-up Current",			  10, 0x0243, 000, RR));
	AddSpace(New CSpace(AN, "PF", "   Position Fault/Torque %",		  10, 0x0244, 000, LL));
	AddSpace(New CSpace(AN, "PI", "   Power-up Idle Current",		  10, 0x0245, 000, RR));
	AddSpace(New CSpace(AN, "PL", "   Position Limit",			  10, 0x0246, 000, LL));
	AddSpace(New CSpace(AN, "PM", "   Power-up Mode",			  10, 0x0247, 000, LL));
	AddSpace(New CSpace(AN, "PP", "   Power-up Peak current",		  10, 0x0248, 000, RR));
	AddSpace(New CSpace(AN, "SA", "   Save Parameters",			  10, 0x2249, 000, LL));
	AddSpace(New CSpace(AN, "SF", "   Step Filter Frequency",		  10, 0x024A, 000, LL));
	AddSpace(New CSpace(AN, "SI", "   Enable Input Usage",			  10, 0x064B, 000, LL));
	AddSpace(New CSpace(AN, "VI", "   Velocity Integrator Constant",	  10, 0x024C, 000, LL));
	AddSpace(New CSpace(AN, "VP", "   Velocity Mode Prop. Constant",	  10, 0x024D, 000, LL));
	AddSpace(New CSpace(AN, "ZC", "   Regen Resistor Cont. Wattage",	  10, 0x024E, 000, LL));
	AddSpace(New CSpace(AN, "ZR", "   Regen Resistor Value",		  10, 0x024F, 000, LL));
	AddSpace(New CSpace(AN, "ZT", "   Regen Resistor Peak Time",		  10, 0x0250, 000, LL));
	}

void  CAppliedMotionDriver::AddIOSpaces(void)
{
	AddSpace(New CSpace(AN, "AD", "   Analog DeadBand",			  10, 0x0451, 000, LL));
	AddSpace(New CSpace(AN, "AF", "   Analog Filter",			  10, 0x0452, 000, LL));
	AddSpace(New CSpace(AN, "AG", "   Analog Velocity Gain",		  10, 0x0453, 000, LL));
	AddSpace(New CSpace(AN, "AI", "   Alarm Input Usage",			  10, 0x0454, 000, LL));
	AddSpace(New CSpace(AN, "AO", "   Alarm Output (Y3)",			  10, 0x0455, 000, LL));
	AddSpace(New CSpace(AN, "AP", "   Analog Position Gain",		  10, 0x0456, 000, LL));
	AddSpace(New CSpace(AN, "AS", "   Analog Scaling",			  10, 0x0457, 000, LL));
	AddSpace(New CSpace(AN, "AT", "   Analog Threshold",			  10, 0x0458, 000, RR));
	AddSpace(New CSpace(AN, "AV", "   Analog Offset Value",			  10, 0x0459, 000, RR));
	AddSpace(New CSpace(AN, "AZ", "   Analog Zero",				  10, 0x045A, 000, LL));
//	AddSpace(New CSpace(AN, "BD", "   Brake Disengage Delay",		  10, 0x062A, 000, LL));
//	AddSpace(New CSpace(AN, "BE", "   Brake Engage Delay",			  10, 0x062B, 000, LL));
	AddSpace(New CSpace(AN, "BO", "   Brake Output (Y1)",			  10, 0x045B, 000, LL));
//	AddSpace(New CSpace(AN, "DL", "   Define Limits (X6, X7)",		  10, 0x0634, 000, LL));
	AddSpace(New CSpace(FIR,"FI", "   Filter Input",			  10,      3,   7, LL));
	AddSpace(New CSpace(AN, "FX", "   Filter select inputs",		  10, 0x045D, 000, LL));
	AddSpace(New CSpace(AN, "IH", "   Immediate High Output",		  10, 0x845E, 000, LL));
	AddSpace(New CSpace(AN, "IHY","   Immediate High Output, Y",		  10, 0x845F, 000, LL));
	AddSpace(New CSpace(AN, "IL", "   Immediate Low Output",		  10, 0x8460, 000, LL));
	AddSpace(New CSpace(AN, "ILY","   Immediate Low Output, Y",		  10, 0x8461, 000, LL));
	AddSpace(New CSpace(AN, "IS", "   Input Status",			  10, 0x8462, 000, LL));
	AddSpace(New CSpace(AN, "ISX","   Input Status, X",			  10, 0x8463, 000, LL));
//	AddSpace(New CSpace(AN, "MO", "   Motion Output (Y2)",			  10, 0x0642, 000, LL));
	AddSpace(New CSpace(SOU,"SO", "   Set Output (1=L, 2=H)",		  10,      1,   4, LL));
	AddSpace(New CSpace(SOY,"SOY","   Set Output, Y (1=L, 2=H)",		  10,      1,   4, LL));
//	AddSpace(New CSpace(AN, "SI", "   Enable Input Usage",			  10, 0x064B, 000, LL));
	}

void  CAppliedMotionDriver::AddCommSpaces(void)
{
	AddSpace(New CSpace(AN, "TD",  "   Transmit Delay",			  10, 0x0866, 000, LL));
	AddSpace(New CSpace(AN, "ZERR","   Error Code Received",		  10, 0x08FF, 000, LL));
	AddSpace(New CSpace(AN, "PC2D","   PC to Drive (Enable=1, Disable=0)",	  10, 0x08FE, 000, LL));
	AddSpace(New CSpace(AN, "G32D","   Restart Comms after PC Disable",	  10, 0x08FD, 000, LL));

	if( ALLOWSGLANDQRSP ) {
		AddSpace(New CSpace(SGL,"SGLD","   Send user defined string",		  10,      0,   4, LL));
		AddSpace(New CSpace(AN, "QRSP","   SGLD Response",			  10, 0x08FC, 000, LL));
		}
	}

void  CAppliedMotionDriver::AddQSpaces(void)
{
	AddSpace(New CSpace(AN, "CT", "   Continue",				  10, 0x9067, 000, LL));
	AddSpace(New CSpace(AN, "MT", "   Multi-Tasking",			  10, 0x1068, 000, LL));
	AddSpace(New CSpace(AN, "PS", "   Pause",				  10, 0x1069, 000, LL));
	AddSpace(New CSpace(AN, "QC", "   Queue Call (QC)",			  10, 0x106A, 000, LL));
	AddSpace(New CSpace(AN, "QCN","   Queue Call, (QC<segment #>)",		  10, 0x106B, 000, LL));
	AddSpace(New CSpace(AN, "QD", "   Queue Delete",			  10, 0x106C, 000, LL));
	AddSpace(New CSpace(AN, "QE", "   Queue Execute",			  10, 0x906D, 000, LL));
	AddSpace(New CSpace(AN, "QK", "   Queue Kill (1=QKD,2=QKM)",		  10, 0x106E, 000, LL));
	AddSpace(New CSpace(AN, "QL", "   Queue Load",				  10, 0x906F, 000, LL));
	AddSpace(New CSpace(AN, "QS", "   Queue Save",				  10, 0x9070, 000, LL));
	AddSpace(New CSpace(AN, "QX", "   Queue Load & Execute",		  10, 0x1071, 000, LL));
//	AddSpace(New CSpace(AN, "SM", "   Stop Move",				  10, 0x1124, 000, LL));
	AddSpace(New CSpace(AN, "WD", "   Wait Delay",				  10, 0x1072, 000, LL));
	AddSpace(New CSpace(AN, "WT", "   Wait Time",				  10, 0x1073, 000, RR));
	}

void  CAppliedMotionDriver::AddDriveSpaces(void)
{
	AddSpace(New CSpace(AN, "AL", "   Alarm Code",				  10, 0xA074, 000, LL));
	AddSpace(New CSpace(AN, "AX", "   Alarm Reset Buffered",		  10, 0x2075, 000, LL));
//	AddSpace(New CSpace(AN, "EP", "   Encoder Position",			  10, 0x2108, 000, LL));
	AddSpace(New CSpace(AN, "MD", "   Motor Disable",			  10, 0x2076, 000, LL));
	AddSpace(New CSpace(AN, "ME", "   Motor Enable",			  10, 0x2077, 000, LL));
	AddSpace(New CSpace(AN, "MN", "   Model Number",			  10, 0xA078, 000, LL));
//	AddSpace(New CSpace(AN, "SA", "   Save Parameters",			  10, 0x2249, 000, LL));
	AddSpace(New CSpace(AN, "SC", "   Status Code",				  10, 0x2079, 000, LL));
	}

void  CAppliedMotionDriver::AddRegisterSpaces(void)
{
//	AddSpace(New CSpace(AN, "DR", "   Data Register (Capture)",		  10, 0x4235, 000, LL));
	AddSpace(New CSpace(RCB,"RC", "   Reg. Counter",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(RCX,"RCX","   Reg. Counter, X",			  10,    'F', 'R', LL));
	AddSpace(New CSpace(AN, "RD", "   Reg. Decrement",			  10, 0x4001, 000, LL));
	AddSpace(New CSpace(AN, "RI", "   Reg. Increment",			  10, 0x4002, 000, LL));
	AddSpace(New CSpace(RGL,"RL", "   Reg. Load Immediate",			  10,      0, 255, LL));
	AddSpace(New CSpace(RGM,"RM", "   Reg. Move",				  10,      0, 255, LL));
	AddSpace(New CSpace(RGR,"RR", "   Reg. Read (<data> = NV memory)",	  10,      0, 255, LL));
	AddSpace(New CSpace(RGW,"RW", "   Reg. Write (<data> = NV memory)",	  10,      0, 255, LL));
	AddSpace(New CSpace(RGX,"RX", "   Reg. Load Buffered",			  10,      0, 255, LL));
	AddSpace(New CSpace(AN, "TS", "   Time Stamp",				  10, 0x4083, 000, LL));
	}

void  CAppliedMotionDriver::AddImmediateSpaces(void)
{
//	AddSpace(New CSpace(AN, "AL", "   Alarm Code",				  10, 0xA074, 000, LL));
	AddSpace(New CSpace(AN, "AR", "   Alarm Reset immediate",		  10, 0x8084, 000, LL));
	AddSpace(New CSpace(AN, "BS", "   Buffer Status",			  10, 0x8085, 000, LL));
	AddSpace(New CSpace(AN, "CS", "   Change Speed",			  10, 0x8086, 000, RR));
//  	AddSpace(New CSpace(AN, "CT", "   Continue",				  10, 0x9067, 000, LL));
	AddSpace(New CSpace(AN, "GC", "   Current Command",			  10, 0x8087, 000, LL));
	AddSpace(New CSpace(IAV,"IA", "   Immediate Analog (IA, IA1...IA3)",	  10,      0,   3, RR));
	AddSpace(New CSpace(AN, "IC", "   Immediate Current (Commanded)",	  10, 0x8089, 000, LL));
	AddSpace(New CSpace(AN, "ID", "   Immediate Distance",			  10, 0x808A, 000, LL));
	AddSpace(New CSpace(AN, "IE", "   Immediate Encoder",			  10, 0x808B, 000, LL));
//	AddSpace(New CSpace(AN, "IH", "   Immediate High Output",		  10, 0x845E, 000, LL));
//	AddSpace(New CSpace(AN, "IHY","   Immediate High Output, Y",		  10, 0x845F, 000, LL));
//	AddSpace(New CSpace(AN, "IL", "   Immediate Low Output",		  10, 0x8460, 000, LL));
//	AddSpace(New CSpace(AN, "ILY","   Immediate Low Output, Y",		  10, 0x8461, 000, LL));
	AddSpace(New CSpace(AN, "IO", "   Output Status",			  10, 0x808C, 000, LL));
	AddSpace(New CSpace(AN, "IOY","   Output Status, Y",			  10, 0x808D, 000, LL));
	AddSpace(New CSpace(AN, "IP", "   Immediate Position",			  10, 0x808E, 000, LL));
	AddSpace(New CSpace(AN, "IQ", "   Immediate Current (Actual)",		  10, 0x808F, 000, LL));
//	AddSpace(New CSpace(AN, "IS", "   Input Status",			  10, 0x8462, 000, LL));
	AddSpace(New CSpace(AN, "IT", "   Immediate Temperature",		  10, 0x8090, 000, LL));
	AddSpace(New CSpace(AN, "IU", "   Immediate Voltage",			  10, 0x8091, 000, LL));
	AddSpace(New CSpace(AN, "IVA","   Immediate Actual Velocity",		  10, 0x8092, 000, LL));
	AddSpace(New CSpace(AN, "IVT","   Immediate Target Velocity",		  10, 0x8093, 000, LL));
	AddSpace(New CSpace(AN, "IX", "   Immediate Position Error",		  10, 0x8094, 000, LL));
//	AddSpace(New CSpace(AN, "MN", "   Model Number",			  10, 0xA078, 000, LL));
//	AddSpace(New CSpace(AN, "QE", "   Queue Execute",			  10, 0x906D, 000, LL));
//	AddSpace(New CSpace(AN, "QL", "   Queue Load",				  10, 0x906F, 000, LL));
//	AddSpace(New CSpace(AN, "QS", "   Queue Save",				  10, 0x9070, 000, LL));
	AddSpace(New CSpace(AN, "RE", "   Restart or Reset",			  10, 0x8095, 000, LL));
//	AddSpace(New CSpace(RGL,"RL", "   Register Load Immediate",		  16,    '0', 'z', RR));
	AddSpace(New CSpace(RST,"RS", "   Request Status (4 chars/item)",	  10,      0,   2, LL));
	AddSpace(New CSpace(AN, "RV", "   Revision Level",			  10, 0x8098, 000, LL));
//	AddSpace(New CSpace(AN, "SJ", "   Stop Jogging",			  10, 0x8123, 000, LL));
	AddSpace(New CSpace(AN, "SK", "   Stop & Kill (1=SK, 2=SKD)",		  10, 0x8099, 000, LL));
//	AddSpace(New CSpace(AN, "ST", "   Stop (1=ST, 2=STD)",			  10, 0x8126, 000, LL));
	}

void  CAppliedMotionDriver::AddSpaces(void)
{
	AddMotionSpaces();
	AddConfigSpaces();
	AddIOSpaces();
	AddCommSpaces();
	AddQSpaces();
	AddDriveSpaces();
	AddRegisterSpaces();
	AddImmediateSpaces();
	}

// Helpers
void  CAppliedMotionDriver::ValidateListSelect(UINT uTable, UINT uOffset)
{
	UINT uItemSel = GetListFromTable(uTable, uOffset);

	if( !uItemSel || (uTable == AN) ) { // named item, or unknown list value

		if( m_uListSelect >= EXTMOT && m_uListSelect <= EXTIMM ) {

			if( uItemSel & (0x100 << (m_uListSelect-1)) ) return; // valid list selected
			}

		uItemSel = SetListForNamed(uOffset); // use default list for named item
		}

	SetListSelect(&uItemSel);
	}

UINT  CAppliedMotionDriver::IsLetterSelect(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case FDB:
		case FDD:
		case FEB:
		case FEX:
		case FMB:
		case FMX:
		case FSB:
		case FSX:
		case FYB:
		case FYX:
		case HWB:
		case HWX:
		case SHB:
		case SHX:
		case RCB:
		case RCX:
			return ISINPLETTER;

		case RGL:
		case RGM:
		case RGR:
		case RGW:
		case RGX:
			return ISREGLETTER;
		}

	return ISNOTLETTER;
	}

UINT  CAppliedMotionDriver::SetListForNamed(UINT uOffset)
{
	UINT u = uOffset;
	UINT uItemSel;	

	if(       u & LSTMOT ) uItemSel = LSTMOT;
	else if ( u & LSTCON ) uItemSel = LSTCON;
	else if ( u & LSTIO  ) uItemSel = LSTIO;
	else if ( u & LSTCOM ) uItemSel = LSTCOM;
	else if ( u & LSTQ   ) uItemSel = LSTQ;
	else if ( u & LSTDRV ) uItemSel = LSTDRV;
	else if ( u & LSTREG ) uItemSel = LSTREG;
	else if ( u & LSTIMM ) uItemSel = LSTIMM;

	else uItemSel = LSTMOT;

	return uItemSel;
	}

//////////////////////////////////////////////////////////////////////////
//
// Applied Motion Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CAppliedMotionAddrDialog, CStdAddrDialog);
		
// Constructor

CAppliedMotionAddrDialog::CAppliedMotionAddrDialog(CAppliedMotionDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pSpace  = NULL;

	m_pDriverData = &Driver;

	DoCurrentList(LSTMOT);
	}

// Destructor
CAppliedMotionAddrDialog::~CAppliedMotionAddrDialog(void)
{
	m_pDriverData->AddDialogSpaces(TRUE);
	}

// Message Map

AfxMessageMap(CAppliedMotionAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CAppliedMotionAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;

	if( Addr.m_Ref ) {

		UINT uList = GetListFromTable(Addr.a.m_Table, Addr.a.m_Offset);

		if( Addr.a.m_Extra ) uList = GetListFromExtra(Addr.a.m_Extra);

		DoCurrentList(uList);
		}

	PickSpaces();

	if( !m_fPart ) {

		FindSpace();

		if( m_pSpace ) {

			if( !Addr.m_Ref ) SetAllowSpace(m_pSpace->m_uTable);
			}

		else DoCurrentList(LSTMOT);

		LoadList();

		if( m_pSpace ) {

			if( !IsHeader(m_pSpace->m_uTable) ) {

				SCLShowAddress(Addr);
				}

			return FALSE;
			}

		return TRUE;
		}
	else {
		FindSpace();

		if( IsHeader(m_pSpace->m_uTable) ) {

			Addr.a.m_Table  = addrNamed;
			Addr.a.m_Offset = 101;
			Addr.a.m_Type   = addrLongAsLong;
			Addr.a.m_Extra  = 0;

			*m_pAddr = Addr;

			FindSpace();
			}

		SetAllowSpace(m_pSpace->m_uTable);

		LoadType();

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		CError   Error(FALSE);
			
		if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			m_pSpace->GetMinimum(Addr);

			SCLShowAddress(Addr);
			}

		return FALSE;
		}
	}

// Notification Handlers

void CAppliedMotionAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CAppliedMotionAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			UINT uTable = m_pSpace->m_uTable;

			if( IsFuncHeader(uTable) ) {

				UINT uList  = m_CurrentList;

				SetAllowSpace(uTable);

				if( uList != m_CurrentList ) {

					PickSpaces();

					LoadList();

					INDEX I  = INDEX(ListBox.GetItemData(2));

					CSpace *p = m_pDriver->GetSpace(I);

					if( p ) {

						m_pSpace = p;
						uTable   = p->m_uTable;
						}

					LoadList();
					}
				}

			if( !IsHeader(uTable) ) {

				CAddress Addr;

				Addr.a.m_Type = m_pSpace->m_uType;

				m_pSpace->GetMinimum(Addr);

				SCLShowAddress(Addr);
				}
			}
		}
	else {
		DoCurrentList(LSTMOT);

		LoadList();

		m_pSpace = NULL;

		ClearType();

		ClearAddress();

		ClearDetails();
		}
	}

// Command Handlers

BOOL CAppliedMotionAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( IsHeader(m_pSpace->m_uTable) ) return FALSE;

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			Addr.a.m_Extra = SetExtra();

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// List Selection/Display

void CAppliedMotionAddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 40, 100, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf(TEXT("<%s>\t%s"), CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING);

	INDEX Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	INDEX n = List.GetHead();

	if( List.GetCount() ) {

		DoDesignatedHeader(&ListBox, List, n);

		Find = DoSelection(&ListBox, List, n);

		DoRemainingHeaders(&ListBox, List, n);
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSpaceChange(1001, ListBox);
	}

void CAppliedMotionAddrDialog::LoadEntry(CListBox *pBox, CString Prefix, CString Caption, DWORD n)
{
	CString Entry;

	Entry.Printf(TEXT("%s\t%s"), Prefix, Caption);

	pBox->AddString(Entry, n);
	}

void CAppliedMotionAddrDialog::DoDesignatedHeader(CListBox *pBox, CSpaceList &List, INDEX n)
{
	while( !(List.Failed(n)) ) {

		CSpace *p;

		p = List[n];

		UINT t = p->m_uTable;

		if( IsHeader(t) && GetListFromHeader(t) == m_CurrentList ) {

			m_fListTypeChg = p == m_pSpace;

			LoadEntry( pBox, p->m_Prefix, p->m_Caption, DWORD(n) );

			return;
			}

		List.GetNext(n);
		}
	}

INDEX CAppliedMotionAddrDialog::DoSelection(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *p;

	INDEX Find        = INDEX(NOTHING);
	INDEX DefaultFind = INDEX(NOTHING);

	INDEX   Sort[100] = {INDEX(0)};
	CString   st[100] = {TEXT("")};

	UINT i = 0;

	while( !List.Failed(n) ) {

		p          = List[n];

		CString sP = p->m_Prefix;

		if( !IsHeader(p->m_uTable) ) {

			i = 0;

			if( (GetListFromTable(p->m_uTable, p->m_uMinimum) & (m_CurrentList)) && !sP.IsEmpty() ) {

				while( i < 100 ) {

					UINT uP = 100;

					if( st[i].IsEmpty() ) uP = i;

					else {
						int wC1  = tstrncmp(sP, CString(st[i]  ), 2);
						
						int wC2  = tstrncmp(sP, CString(st[i+1]), 2);

						UINT k;

						if( wC1 >= 0 ) {							

							if( st[i+1].IsEmpty() ) uP = i + 1;

							else {
								if( wC2 < 0 ) {

									for( k = 99; k > i+1; k-- ) {

										st[k] = st[k-1];
										Sort[k] = Sort[k-1];
										}

									uP = i+1;
									}
								}
							}

						else {
							for( k = 99; k > i; k-- ) {

								st[k] = st[k-1];
								Sort[k] = Sort[k-1];
								}

							uP = i;
							}
						}

					if( uP < 100 ) {

						st[uP]   = sP;
						Sort[uP] = n;

						i = 100;
						}
					i++;
					}
				}
			}

		List.GetNext(n);
		}

	n = Sort[0];

	p = List[n];

	if( (p->m_uMinimum >> 8) == LST08 ) { // Comms area, re-sort to logical order

		if( !ALLOWSGLANDQRSP ) {

			Sort[4] = Sort[0]; // save G32D
			Sort[0] = Sort[2]; // move TD to 0
			Sort[2] = Sort[4]; // move G32D to 2
			Sort[4] = Sort[1]; // save PC2D
			Sort[1] = Sort[3]; // move ZERR to 1
			Sort[3] = Sort[4]; // move PC2D to 3
			Sort[4] = INDEX(0);
			}

		else {
			Sort[6] = Sort[2]; // save QRSP
			Sort[2] = Sort[0]; // move G32D to 2
			Sort[0] = Sort[4]; // move TD   to 0
			Sort[4] = Sort[3]; // move SGL  to 4
			Sort[3] = Sort[1]; // move PC2D to 3
			Sort[1] = Sort[5]; // move ZERR to 1
			Sort[5] = Sort[6]; // move QRSP to 5
			Sort[6] = INDEX(0);
			}
		}

	DefaultFind = Sort[0];

	for( i = 0; i < 100; i++ ) {

		n = Sort[i];

		if( n != INDEX(0) ) {

			p = List[n];

			LoadEntry(pBox, p->m_Prefix, p->m_Caption, DWORD(n) );

			if( p == m_pSpace ) Find = n;
			}

		else i = 100;
		}

	return Find == INDEX(NOTHING) ? DefaultFind : Find;
	}

void CAppliedMotionAddrDialog::DoRemainingHeaders(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *pSpace;

	while( !List.Failed(n) ) {

		pSpace = List[n];

		UINT t = pSpace->m_uTable;

		if( IsHeader(t) && ((GetListFromHeader(t) != m_CurrentList) || IsInfo(t) )) {

			LoadEntry( pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n) );
			}

		List.GetNext(n);
		}
	}

BOOL  CAppliedMotionAddrDialog::MatchList(CSpace *pSpace)
{
	return m_CurrentList & GetListFromTable(pSpace->m_uTable, pSpace->m_uMinimum);
	}

BOOL  CAppliedMotionAddrDialog::IsInfo(UINT uTable)
{
	return uTable >= INFO0 && uTable <= ENDINFO;
	}

// Other Overrideables
BOOL  CAppliedMotionAddrDialog::AllowSpace(CSpace * pSpace)
{
	UINT uTable = pSpace->m_uTable;

	if( m_fPart ) return !IsHeader(uTable);

	return IsHeader(uTable) ? TRUE : GetListFromTable(uTable, pSpace->m_uMinimum) == m_CurrentList;
	}

// Helpers
BOOL  CAppliedMotionAddrDialog::IsHeader(UINT uTable)
{
	return uTable >= 200 && uTable < AN;
	}

BOOL CAppliedMotionAddrDialog::IsFuncHeader(UINT uTable)
{
	return uTable >= FNCMOT && uTable <= FNCIMM;
	}

void  CAppliedMotionAddrDialog::SetAllowSpace(UINT uTable)
{
	if( IsHeader(uTable) ) DoCurrentList( GetListFromHeader(uTable) );

	else {
		if( m_pSpace ) DoCurrentList( GetListFromTable(m_pSpace->m_uTable, m_pSpace->m_uMinimum) );
		}
	}

UINT  CAppliedMotionAddrDialog::GetListFromTable(UINT uTable, UINT uOffset)
{
	return m_pDriverData->GetListFromTable(uTable, uOffset);
	}

UINT  CAppliedMotionAddrDialog::GetListFromHeader(UINT uTable)
{
	switch( uTable ) {

		case FNCCON:	return LSTCON;
		case FNCIO:	return LSTIO;
		case FNCCOM:	return LSTCOM;
		case FNCQ:	return LSTQ;
		case FNCDRV:	return LSTDRV;
		case FNCREG:	return LSTREG;
		case FNCIMM:	return LSTIMM;
		}

	return LSTMOT;
	}

UINT  CAppliedMotionAddrDialog::GetListFromExtra(UINT uExtra)
{
	switch( uExtra ) {

		case EXTCON:	return LSTCON;
		case EXTIO:	return LSTIO;
		case EXTCOM:	return LSTCOM;
		case EXTQ:	return LSTQ;
		case EXTDRV:	return LSTDRV;
		case EXTREG:	return LSTREG;
		case EXTIMM:	return LSTIMM;
		}

	return LSTMOT;
	}

void  CAppliedMotionAddrDialog::PickSpaces(void)
{
	CAppliedMotionDriver * p = m_pDriverData;

	p->AddDialogSpaces(m_fPart);
	}

void  CAppliedMotionAddrDialog::DoCurrentList(UINT uList)
{
	m_CurrentList = uList;

	m_pDriverData->SetListSelect(&m_CurrentList);
	}

UINT  CAppliedMotionAddrDialog::SetExtra(void)
{
	switch( m_CurrentList ) {

		case LSTCON:	return EXTCON;
		case LSTIO:	return EXTIO;
		case LSTCOM:	return EXTCOM;
		case LSTQ:	return EXTQ;
		case LSTDRV:	return EXTDRV;
		case LSTREG:	return EXTREG;
		case LSTIMM:	return EXTIMM;
		}

	return EXTMOT;
	}

void  CAppliedMotionAddrDialog::SCLShowAddress(CAddress Addr)
{
	ShowAddress(Addr);

	if( Addr.a.m_Table == SGL ) {

		GetDlgItem(2002).SetWindowText(TEXT(""));
		GetDlgItem(2002).EnableWindow(FALSE);
		ClearDetails();
		}

	else {
		ShowDetails();
		}
	}

// End of File
