
#include "intern.hpp"

#include "s5base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Space Wrapper
//

// Constructor

CSpaceS5::CSpaceS5(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s)
{
	m_uTable	= uTable;
	
	m_Prefix	= p;

	m_Caption	= c;

	m_uRadix	= r;

	m_uMinimum	= n;
	
	m_uMaximum	= x;
	
	m_uType		= t;

	m_uSpan         = s;

	FindWidth();

	FindAlignment();
	}

// Matching

BOOL CSpaceS5::MatchSpace(CAddress const &Addr)
{
	return m_uTable == (Addr.a.m_Table & 0x0F);
	}

// Limits

void CSpaceS5::GetMinimum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);

	if( m_uTable == 0x06 || m_uTable == 0x07 ) {

		UINT uBlock = m_uTable == 0x06 ? 2 : 0;

		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= uBlock & 0xF0;
		}
	}

void CSpaceS5::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	if( m_uTable == 0x06 || m_uTable == 0x07 ) {

		UINT uBlock = 255;

		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= uBlock & 0xF0;
		}

	if( m_uTable == 0x09 ) {

		Addr.a.m_Extra = 0x0F;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Base Driver
//

// Constructor

CS5AS511BaseDriver::CS5AS511BaseDriver(BOOL fV2)
{
	m_uType	       = driverMaster;
	
	m_Manufacturer = "Siemens";
	
	m_ShortName    = "S5 Master";

	m_DevRoot      = "DEV";

	m_fSingle      = TRUE;

	AddSpaces(fV2);
	}

// Implementation     

void CS5AS511BaseDriver::AddSpaces(BOOL fV2)
{
	AddSpace(New CSpaceS5(0x01, "PIQ", "Process Output Image",  10, 0,    128, addrByteAsByte, addrByteAsWord));

	AddSpace(New CSpaceS5(0x02, "PII", "Process Input Image",   10, 0,    128, addrByteAsByte, addrByteAsWord));

	if( fV2 ) {
	
		AddSpace(New CSpaceS5(0x03, "F", "Flags",           10, 0,    255, addrByteAsByte,  addrByteAsLong));
		}
	else {
		AddSpace(New CSpaceS5(0x03, "F", "Flags",           10, 0,   2048, addrBitAsByte,  addrBitAsLong));
		}

	AddSpace(New CSpaceS5(0x04, "T",   "Timer Value",           10, 0,    255, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS5(0x05, "C",   "Counter Value",         10, 0,    255, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS5(0x06, "DB",  "Data Blocks",           10, 0,    255, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpaceS5(0x07, "DX",  "Extended Data Blocks",  10, 0,    255, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpaceS5(0x08, "S",   "System Data",	    10, 0,    512, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpaceS5(0x09, "M",   "Memory Location",	    16, 0, 0xFFFFF, addrWordAsWord, addrWordAsLong));
	}

// Address Management

BOOL CS5AS511BaseDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CS5AS511Dialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CS5AS511BaseDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uBlock = 0;

	if( pSpace->m_uTable == 0x06 || pSpace->m_uTable == 0x07 ) {

		UINT uFind = Text.Find(':');

		if( uFind == NOTHING ) {

			uBlock = 1;
			}
		else {
			uBlock = tatoi(Text);

			Text   = Text.Mid(uFind + 1);

			if( uBlock < 1 || uBlock > 255 ) {

				Error.Set( "Invalid data block number",
					   0
					   );

				return FALSE;
				}
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {
		
		if( pSpace->m_uTable == 0x09 ) {

			PTXT pError  = NULL;
	
			UINT uOffset = tstrtoul(Text, &pError, pSpace->m_uRadix);

			Addr.a.m_Offset	= uOffset;

			Addr.a.m_Extra	= (uOffset >> 16) & 0x0F;

			return TRUE;
			}
		
		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= uBlock & 0xF0;

		return TRUE; 
		}

	return FALSE;
	}

BOOL CS5AS511BaseDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uOffset = Addr.a.m_Offset;

		UINT uType   = AddrType(Addr.a.m_Type);

		if( pSpace->m_uTable == 0x06 || pSpace->m_uTable == 0x07 ) {

			UINT uBlock  = Addr.a.m_Extra | (Addr.a.m_Table & 0xF0);

			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%4.4u:%s", 
					     pSpace->m_Prefix, 
					     uBlock,
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			else {
				Text.Printf( "%s%4.4u:%s.%s", 
					     pSpace->m_Prefix, 
					     uBlock,
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}
		else {
			if( pSpace->m_uTable == 0x09 ) {

				uOffset |= DWORD(Addr.a.m_Extra) << 16;
				}

			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			else {
				Text.Printf( "%s%s.%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}

		return TRUE;
		}
	
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CS5AS511Dialog, CStdAddrDialog);
		
// Constructor

CS5AS511Dialog::CS5AS511Dialog(CS5AS511BaseDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "S5AS511ElementDlg";
	}

// Overridables

void CS5AS511Dialog::SetAddressFocus(void)
{
	if( m_pSpace && (m_pSpace->m_uTable == 0x06 || m_pSpace->m_uTable == 0x07) ) {

		SetDlgFocus(2005);

		return;
		}
	
	SetDlgFocus(2002);
	}

void CS5AS511Dialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	if( m_pSpace && (m_pSpace->m_uTable == 0x06 || m_pSpace->m_uTable == 0x07) ) {

		if( fEnable ) {

			UINT uFind = Text.Find(':');

			GetDlgItem(2002).SetWindowText(Text.Left(uFind));

			GetDlgItem(2004).SetWindowText(":");

			GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fEnable);
		
		GetDlgItem(2005).EnableWindow(fEnable);
		}
	else {
		if( fEnable ) {

			GetDlgItem(2002).SetWindowText(Text);

			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2005).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		}
	}

CString CS5AS511Dialog::GetAddressText(void)
{
	if( m_pSpace && (m_pSpace->m_uTable == 0x06 || m_pSpace->m_uTable == 0x07) ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += ":";
		
		Text += GetDlgItem(2005).GetWindowText();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

// End of File
