
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ABULTRA3000_HPP

#define	INCLUDE_ABULTRA3000_HPP

#define	AN	addrNamed

#define	LL	addrLongAsLong

// Valid Function Code Definitions
#define	FA		0x7000 // F0
#define	FB		0x7001 // F1
#define	FC		0x7002 // F0 + F1
#define	FD		0x7003 // F0 + F1 + F8 + F9
#define	FE		0x7004 // F0 + F2 + F3 + F6 + F7 + F8 + F9
#define	FF		0x7005 // F0 + F1 + F2 + F3 + F6 + F7 + F8 + F9
#define	FG		0x7006 // F0 + F1 + F2 + F3 + F4 + F5 + F6 + F7 + FA + FB
#define	FH		0x7007 // F0 + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9
#define	FI		0x7008 // F0 + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9 + FA + FB
#define	FJ		0x7009 // Internal Value
#define	FW		0x7010 // F1 with "Write Stored Value"
#define	FX		0x7011 // Drive Name, F0 + F1 + F2 + F3 + F4 + F5 + F6 + F7
#define FY		0x7012 // Motor Model F0 + F1 + F2 + F3 + F6 + F7
#define	FZ		0x7013 // Section Title

#define	SPGENERAL	0x1000
#define	SPCOMMS		0x1001
#define	SPANALOGMODE	0x1002
#define	SPPRESETMODE	0x1003
#define	SPFOLLOWMODE	0x1004
#define	SPINDEXMODE	0x1005
#define	SPOVERTRAVEL	0x1006
#define	SPHOMING	0x1007
#define	SPMOTOR		0x1008
#define	SPTUNING	0x1009
#define	SPENCODER	0x1010
#define	SPDIGITALIO	0x1011
#define	SPANALOGIO	0x1012
#define	SPMONITORING	0x1013
#define	SPFAULT		0x1014
#define	SPDRIVENAME	0x1015
#define	SPMOTORMODEL	0x1016

#define	SPNONE		0x2000

//////////////////////////////////////////////////////////////////////////
//
// ABUltra3000 Device Options
//

class CABUltra3DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABUltra3DeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Broadcast;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ABUltra3000 Driver
//

class CABUltra3Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CABUltra3Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		CSpace * GetSpace(CAddress const &Addr);
		BOOL MatchSpace(CSpace *pSpace, CAddress const &Addr);

	protected:
		// Data

		// Implementation
		void AddSpaces(void);

		// Helpers
		BOOL Validate(CSpace *pSpace, UINT uFnc, CString * pErr);

		// Friend
		friend class CABUltra3AddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// ABUltra3000 Address Selection
//

class CABUltra3AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CABUltra3AddrDialog(CABUltra3Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		UINT m_uAllowSpace;
		CABUltra3Driver * m_pABUDriver;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Overridables
		void ShowAddress(CAddress Addr);

		// Helpers
		UINT GetRadioButton(void);
		void SetRadioButton(UINT uFunction);
		void PutRadioButton(UINT uID, BOOL fYes);
		void ClearRadioButtons(void);
		void SetRadioOptions(void);
		void EnableWindow(UINT uMask);
		void ValidateSetting(UINT * pFunction);
		BOOL AllowSpace(CSpace *pSpace);
		BOOL SelectList(UINT uCommand, UINT uAllow);
		void SetAllow(void);
		void SetDescription2016(UINT uCommand);
		UINT IsHeader(UINT uCommand);
		UINT GetFunctionCodeList(void);
		BOOL EnableIndexInput(void);
	};

// End of File

#endif
