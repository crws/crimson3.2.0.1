
#include "intern.hpp"

#include "garmin.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Garmin GPS Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CGarminDriverOptions, CUIItem);

// Constructor

CGarminDriverOptions::CGarminDriverOptions(void)
{
	m_DegForm   = 0;

	m_UnitSpeed = 0;

	m_UnitAlt   = 0;
	}

// Download Support

BOOL CGarminDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_DegForm));
	Init.AddByte(BYTE(m_UnitSpeed));
	Init.AddByte(BYTE(m_UnitAlt));

	return TRUE;
	}

// Meta Data Creation

void CGarminDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(DegForm);
	Meta_AddInteger(UnitSpeed);
	Meta_AddInteger(UnitAlt);
	}

//////////////////////////////////////////////////////////////////////////
//
// Garmin GPS Master
//

// Instantiator

ICommsDriver * Create_GarminDriver(void)
{
	return New CGarminDriver;
	}

// Constructor

CGarminDriver::CGarminDriver(void)
{
	m_wID		= 0x4004;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Garmin";
	
	m_DriverName	= "NMEA-0183";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Garmin NMEA-0183";

	m_DevRoot	= "GPS";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Configuration

CLASS CGarminDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CGarminDriverOptions);
	}

CLASS CGarminDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Binding Control

UINT CGarminDriver::GetBinding(void)
{
	return bindRawSerial;
	}

void CGarminDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CGarminDriver::AddSpaces(void)
{
	AddSpace(New CSpace("Valid",	 "Fix Valid",			10, addrLongAsLong));
	AddSpace(New CSpace("Lat",	 "Latitude",			 2, addrRealAsReal));
	AddSpace(New CSpace("Long",	 "Longitude",			 3, addrRealAsReal));
	AddSpace(New CSpace("Alt",	 "Altitude",			 4, addrRealAsReal));
	AddSpace(New CSpace("Speed",	 "Ground Speed",		 5, addrRealAsReal));
	AddSpace(New CSpace("Course",	 "True Course",			 6, addrRealAsReal));
	AddSpace(New CSpace("Var",	 "Magnetic Variation",		 9, addrRealAsReal));
	AddSpace(New CSpace("Time",	 "Fix Time",			 1, addrLongAsLong));
	AddSpace(New CSpace("Date",	 "Fix Date",			 7, addrLongAsLong));
	AddSpace(New CSpace("Both",	 "Fix Time & Date",		 8, addrLongAsLong));
	AddSpace(New CSpace("WindValid", "Wind Valid",			14, addrLongAsLong));
	AddSpace(New CSpace("WindSpeed", "Wind Speed",			11, addrRealAsReal));
	AddSpace(New CSpace("WindAngle", "Wind Angle",			12, addrRealAsReal));
	AddSpace(New CSpace("WindRef",   "Wind Reference",		13, addrLongAsLong));
	AddSpace(New CSpace("Parallel",  "Speed Parallel to Wind",	15, addrRealAsReal));
	}

// End of File
