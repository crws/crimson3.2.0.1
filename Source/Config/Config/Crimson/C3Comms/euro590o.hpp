
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EUROTHERM590OLD_HPP
	
#define	INCLUDE_EUROTHERM590OLD_HPP

#include "euro590.hpp"

// Space Defines
#define	TAGB	1
#define	TAGI	2
#define	TAGR	3
#define	PARB	4
#define	PARI	5
#define	PARR	6

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Old 590 Ascii Master Driver
//

class CEuroOld590Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuroOld590Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
