
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BACNET_HPP
	
#define	INCLUDE_BACNET_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "bacmap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Device Options
//

class CBACNetDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBACNetDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Object Allocation
		UINT  ObjectIndex(DWORD ObjectID);
		UINT  GetObjectCount(void);
		DWORD GetObjectID(UINT uObject);

		// Config Data
		UINT       m_Device;
		CByteArray m_ObjID;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Device Options
//

class CBACNet8023DeviceOptions : public CBACNetDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBACNet8023DeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT    m_Time1;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Device Options
//

class CBACNetIPDeviceOptions : public CBACNetDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBACNetIPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT    m_Time1;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Device Options
//

class CBACNetMSTPDeviceOptions : public CBACNetDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBACNetMSTPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT	m_Broke;
		UINT    m_Type;
		UINT	m_TimeMode;
		UINT	m_Time1;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Driver Options
//

class CBACNetMSTPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBACNetMSTPDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_ThisDrop;
		UINT m_LastDrop;
		UINT m_Optim1;
		UINT m_Optim2;
		UINT m_TxFast;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Master
//

class CBACNetDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CBACNetDriver(void);

		// Destructor
		~CBACNetDriver(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

	protected:
		// Data Members
		CBACNetMapping *m_pMap;

		// Implementation
		BOOL Encode(CAddress       &Addr, CItem *pConfig, DWORD  ObjectID, BYTE  PropID, BYTE  TypeID);
		BOOL Decode(CAddress const &Addr, CItem *pConfig, DWORD &ObjectID, BYTE &PropID, BYTE &TypeID);

		// Priority Helpers
		BOOL DecodePriority(BYTE ObjectType, BYTE &PropID, BYTE &Priority);
		BOOL EncodePriority(BYTE ObjectType, BYTE &PropID, BYTE &Priority);
		BOOL HasPriority   (BYTE ObjectType);
		BYTE GetRelinquish (void); 
		BOOL SetRelinquish (BYTE &TypeID);
		BOOL IsRelinquish  (BYTE  TypeID);

		// Friends
		friend class CBACNetDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Master
//

class CBACNet8023 : public CBACNetDriver
{
	public:
		// Constructor
		CBACNet8023(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Master
//

class CBACNetIP : public CBACNetDriver
{
	public:
		// Constructor
		CBACNetIP(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Master
//

class CBACNetMSTP : public CBACNetDriver
{
	public:
		// Constructor
		CBACNetMSTP(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
		CLASS GetDriverConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class CBACNetDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBACNetDialog(CBACNetDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		CBACNetDriver *	m_pDriver;
		CAddress      *	m_pAddr;
		CItem	      * m_pConfig;
		BOOL		m_fPart;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay (UINT uID);
		BOOL OnClick(UINT uID);
		
		// Implementation
		void LoadObjectList(void);
		void LoadPropertyList(BYTE ObjectType);
		void LoadPriority(void);
		void ShowPropType(CComboBox &Box);
		void ShowPropType(BYTE TypeID);
		void ShowPriority(CComboBox &Box);
		void ShowPriority(BYTE Priority, BYTE TypeID);
		void ShowRelinquish(CComboBox &Box);
		void ShowRelinquish(BYTE Priority, BYTE TypeID);

		// Helpers
		BYTE FindTypeID(CComboBox &Box);
	};

// End of File

#endif
