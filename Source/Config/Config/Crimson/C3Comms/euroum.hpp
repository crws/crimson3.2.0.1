
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EUROTHERMUM_HPP
	
#define	INCLUDE_EUROTHERMUM_HPP


//////////////////////////////////////////////////////////////////////////
//
// EurothermUM Universal Master Device Options
//

class CEurothermUMDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEurothermUMDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Group;
		UINT m_Unit;
		UINT m_fEnableChannelID;
		UINT m_ChannelID;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// EurothermUM Universal Master ASCII Driver
//

class CEurothermUMDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEurothermUMDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);

	};

// End of File

#endif
