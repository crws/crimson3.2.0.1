#include "intern.hpp"

#include "ml1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ML1 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CML1DriverOptions, CUIItem);

// Constructor

CML1DriverOptions::CML1DriverOptions(void)
{
	m_AuthInt = 60;
	}

// Download Support

BOOL CML1DriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_AuthInt));

	return TRUE;
	}

// Meta Data Creation

void CML1DriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(AuthInt);
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CML1SerialDriverOptions, CML1DriverOptions);

// Constructor

CML1SerialDriverOptions::CML1SerialDriverOptions(void)
{
	m_Track = 1;
	}

// Download Support

BOOL CML1SerialDriverOptions::MakeInitData(CInitData &Init)
{
	CML1DriverOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_Track));

	return TRUE;
	}

// Meta Data Creation

void CML1SerialDriverOptions::AddMetaData(void)
{
	CML1DriverOptions::AddMetaData();

	Meta_AddInteger(Track);
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CML1DeviceOptions, CUIItem);

// Constructor

CML1DeviceOptions::CML1DeviceOptions(void)
{
	m_PingReg = 0;

	m_TO	  = 300;

	m_Retry	  = 0;

	m_pBlocks = New CML1BlockList;
	}

// Persistance

void CML1DeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "List" ) {

			CMetaData const *pMeta = m_pList->FindData("Blocks");

			if( pMeta ) {

				LoadProp(Tree, pMeta);
				}

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}


// UI Loading

BOOL CML1DeviceOptions::OnLoadPages(CUIPageList * pList)
{
	CML1DeviceOptionsUIPage * pPage = New CML1DeviceOptionsUIPage(this);

	pList->Append(pPage);
		
	return TRUE;			   
	}

// UI Managament

void CML1DeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{	
	if( pItem == this ) {

		/*if( Tag.IsEmpty() || Tag == "Server" ) {

			UINT uServer = pItem->GetDataAccess("Server")->ReadInteger(pItem);

			BOOL fBroadcast = uServer == 255;

			pHost->EnableUI("PingReg", !fBroadcast);
			}*/

		if( Tag == "ViewBlocks" ) {

			CML1BlockDialog Dlg(this);
	
			Dlg.Execute();
			}
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CML1DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_PingReg));

	Init.AddWord(WORD(m_TO));

	Init.AddByte(BYTE(m_Retry));

	if( m_pBlocks ) {

		UINT uCount = m_pBlocks->GetItemCount();

		Init.AddWord(WORD(uCount));

		for( UINT u = 0; u < uCount; u++ ) {

			m_pBlocks->GetItem(u)->MakeInitData(Init);
			}
		}
	else {
		Init.AddWord(0);
		} 

	return TRUE;
	}

// Meta Data Creation

void CML1DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(PingReg);

	Meta_AddInteger(TO);

	Meta_AddInteger(Retry);

	Meta_AddCollect(Blocks);
	}

// Block Item Access

CML1Block * CML1DeviceOptions::GetBlock(CAddress Addr)
{
	UINT uRef  = Addr.m_Ref;

	for( INDEX i = m_pBlocks->GetHead(); !m_pBlocks->Failed(i); m_pBlocks->GetNext(i) ) {

		CML1Block * pItem = m_pBlocks->GetItem(i);

		if( pItem ) {
				
			if( uRef == pItem->GetRef() ) {

				return pItem;
				}
			}
		}

	return NULL;
	}

BOOL CML1DeviceOptions::AppendBlock(CML1Block * pBlock)
{
	UINT uRef  = pBlock->GetRef();

	for( INDEX i = m_pBlocks->GetHead(); !m_pBlocks->Failed(i); m_pBlocks->GetNext(i) ) {

		CML1Block * pItem = m_pBlocks->GetItem(i);
		
		if( uRef == pItem->GetRef() ) {

			return FALSE;
			}
		} 

	m_pBlocks->AppendItem(pBlock); 

	return TRUE;
	}

BOOL CML1DeviceOptions::RemoveBlock(CML1Block * pBlock)
{
	UINT uRef  = pBlock->GetRef();

	for( INDEX i = m_pBlocks->GetHead(); !m_pBlocks->Failed(i); m_pBlocks->GetNext(i) ) {

		CML1Block * pItem = m_pBlocks->GetItem(i);
		
		if( uRef == pItem->GetRef() ) {

			m_pBlocks->RemoveItem(pItem);

			delete pItem;

			return TRUE;
			}
		} 

	return FALSE;
	}

BOOL CML1DeviceOptions::ExpandBlock(CAddress Addr, UINT uSpan)
{
	UINT uRef  = Addr.m_Ref;

	for( INDEX i = m_pBlocks->GetHead(); !m_pBlocks->Failed(i); m_pBlocks->GetNext(i) ) {

		CML1Block * pItem = m_pBlocks->GetItem(i);
		
		if( uRef >= pItem->GetRef() ) {

			UINT uExp = pItem->GetRef() + pItem->GetSize() * uSpan;

			if( uRef < uExp ) {

				return TRUE;
				}

			if( uRef < uExp + uSpan && !IsBlockBegin(uRef) ) {
				
				pItem->SetSize(pItem->GetSize() + 1);

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

CML1BlockList * CML1DeviceOptions::GetBlockList(void)
{
	return m_pBlocks;
	}

BOOL CML1DeviceOptions::IsBlockBegin(UINT uRef)
{
	for( INDEX i = m_pBlocks->GetHead(); !m_pBlocks->Failed(i); m_pBlocks->GetNext(i) ) {

		CML1Block * pItem = m_pBlocks->GetItem(i);
		
		if( uRef == pItem->GetRef() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CML1DeviceOptions::Rebuild(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		pSystem->Rebuild(1);

		if( m_pBlocks ) {

			for( INDEX i = m_pBlocks->GetHead(); !m_pBlocks->Failed(i); m_pBlocks->GetNext(i)) {

				CML1Block * pItem = m_pBlocks->GetItem(i);

				if( pItem ) {

					if( !pItem->m_fMark ) {

						m_pBlocks->GetPrev(i);

						m_pBlocks->RemoveItem(pItem);
						}
					else {
						pItem->m_fMark = FALSE;
						}
					}
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Device Options UI Page
//


// Runtime Class

AfxImplementRuntimeClass(CML1DeviceOptionsUIPage , CUIPage);

// Constructor

CML1DeviceOptionsUIPage::CML1DeviceOptionsUIPage(CML1DeviceOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CML1DeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	//pView->StartGroup(CString(IDS_DEVICE_2), 1);

	//pView->AddUI(pItem, L"root", L"Server");

	//pView->AddUI(pItem, L"root", L"Unit");

	//pView->AddUI(pItem, L"root", L"PingReg");

	//pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_ML_OPTIONS), 1);

	pView->AddUI(pItem, L"root", L"TO");
	
	pView->AddUI(pItem, L"root", L"Retry");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_ML_BLOCKS), 1);

	pView->AddButton(L"View", L"ViewBlocks");

	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CML1SerialDeviceOptions, CML1DeviceOptions);

// Constructor

CML1SerialDeviceOptions::CML1SerialDeviceOptions(void)
{
	
	}

// Download Support

BOOL CML1SerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CML1DeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CML1SerialDeviceOptions::AddMetaData(void)
{
	CML1DeviceOptions::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CML1TCPDeviceOptions, CML1DeviceOptions);

// Constructor

CML1TCPDeviceOptions::CML1TCPDeviceOptions(void)
{
	m_Addr1  = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Addr2  = DWORD(0);

	m_Socket = 996;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 10000;

	m_Time2  = 2500;

	m_Time3  = 200;	

	m_IpSel1 = 0;

	m_Text1  = "live.monicoinc.com";
	}

// UI Loading

BOOL CML1TCPDeviceOptions::OnLoadPages(CUIPageList * pList)
{
	CML1TCPDeviceOptionsUIPage * pPage = New CML1TCPDeviceOptionsUIPage(this);

	pList->Append(pPage);
		
	return TRUE;			   
	}

// UI Managament

void CML1TCPDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{	
	if( pItem == this ) {

		/*if( Tag.IsEmpty() || Tag == "Keep" ) {

			pHost->EnableUI("Time3", !m_Keep);
			}*/

		if( Tag.IsEmpty() || Tag == "IpSel1" ) {

			pHost->EnableUI("Addr1", m_IpSel1 == 0);

			pHost->EnableUI("Text1", m_IpSel1 != 0);
			}
		}

	CML1DeviceOptions::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CML1TCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CML1DeviceOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_IpSel1));
	Init.AddLong(LONG(m_Addr1));
	Init.AddText(m_Text1);
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddLong(LONG(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddLong(LONG(m_Addr2));
			
	return TRUE;
	}

// Meta Data Creation

void CML1TCPDeviceOptions::AddMetaData(void)
{
	CML1DeviceOptions::AddMetaData();

	Meta_AddInteger(IpSel1);
	Meta_AddInteger(Addr1);
	Meta_AddString (Text1);
	Meta_AddInteger(Addr2);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 TCP Device Options UI Page
//


// Runtime Class

AfxImplementRuntimeClass(CML1TCPDeviceOptionsUIPage, CML1DeviceOptionsUIPage);

// Constructor

CML1TCPDeviceOptionsUIPage::CML1TCPDeviceOptionsUIPage(CML1DeviceOptions * pOption) : CML1DeviceOptionsUIPage(pOption)
{
		
	}

// Operations

BOOL CML1TCPDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_DEVICE_2), 1);

	pView->AddUI(pItem, L"root", L"IpSel1");

	pView->AddUI(pItem, L"root", L"Addr1");

	pView->AddUI(pItem, L"root", L"Text1");

	//pView->AddUI(pItem, L"root", L"Addr2");

	//pView->AddUI(pItem, L"root", L"Unit");

	pView->AddUI(pItem, L"root", L"Socket");

	//pView->AddUI(pItem, L"root", L"Server");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_ML_OPTIONS), 1);

	pView->AddUI(pItem, L"root", L"TO");

	pView->AddUI(pItem, L"root", L"Retry");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_PROTOCOL_OPTIONS), 1);

	//pView->AddUI(pItem, L"root", L"PingReg");

	//pView->AddUI(pItem, L"root", L"Keep");

	pView->AddUI(pItem, L"root", L"Ping");

	pView->AddUI(pItem, L"root", L"Time1");

	//pView->AddUI(pItem, L"root", L"Time3");

	pView->AddUI(pItem, L"root", L"Time2");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_ML_BLOCKS), 1);

	pView->AddButton(L"View", L"ViewBlocks");

	pView->EndGroup(TRUE);

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// ML1 Base Driver
//

// Instantiator

ICommsDriver *Create_ML1BaseDriver(void)
{
	return New CML1BaseDriver;
	}

// Constructor

CML1BaseDriver::CML1BaseDriver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "Monico";
	
	m_DriverName	= "ML1";
	
	m_Version	= "1.00";
	
	m_ShortName	= "ML1";

	m_fSingle	= TRUE;

	m_fRebuild	= FALSE;

	m_fSelect	= FALSE;

	AddSpaces();

	C3_PASSED();
	}

// Address Management

BOOL CML1BaseDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CML1DeviceOptions * pOpt = (CML1DeviceOptions *) pConfig;

	if( pOpt ) {

		CML1Block * pBlock = pOpt->GetBlock(Addr);

		BOOL fNew = pBlock ? FALSE : TRUE;

		if( fNew ) {

			pBlock = New CML1Block();
			}

		if( pBlock ) {

			pBlock->SetDatabase(pOpt->GetDatabase());

			pBlock->SetDriver(this);

			CItemDialog Dlg(pBlock, CString(IDS_ML_ADDRESS));
			
			if( Dlg.Execute(*afxMainWnd) ) {

				CSpace * pSpace = pBlock->GetSpace();

				if( pSpace ) {

					CString Sel = pBlock->GetSelection();
					
					CError Error;

					m_fSelect = TRUE;

					if( DoParseAddress(Error, Addr, pConfig, pSpace, Sel) ) {

						m_fSelect = FALSE;

						pBlock->SetAddress(Addr);

						if( fNew ) {

							if( pOpt->AppendBlock(pBlock) ) {

								pBlock->SetSize(1);

								return TRUE;
								}

							Addr.m_Ref = 0;

							delete pBlock;

							Error.Set(CString(IDS_THIS_MAPPING));

							Error.Show(Dlg);

							return FALSE;
							}

						return TRUE;
						}

					m_fSelect = FALSE;

					Error.Show(Dlg);

					return FALSE;
					}

				Addr.m_Ref = 0;

				pOpt->RemoveBlock(pBlock);

				return TRUE;
				}
			
			if( fNew ) {

				delete pBlock;
				}
			}
		}

	Addr.m_Ref = 0;

	return FALSE;
	}

BOOL CML1BaseDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	switch( Addr.a.m_Table ) {

		case spAI:
		case spDI:
		case spSR:
			return TRUE;
		}

	return FALSE;
	}

// Address Helpers

BOOL CML1BaseDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{	
	CSpaceML1 * pML1 = (CSpaceML1 *)pSpace;

	UINT uFile = 0;

	if( pML1 ) {

		BOOL fFile = pML1->IsFileRecord();

		if( fFile ) {

			UINT uFind = Text.Find(':');

			if( uFind < NOTHING ) {

				uFile = tstrtol(Text.Left(uFind), NULL, 10);

				Text  = Text.Mid(uFind + 1);
				}
			}
		
		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			if( fFile ) {

				pML1->SetFile(Addr, uFile);
				}

			CML1DeviceOptions * pOpt = (CML1DeviceOptions *) pConfig;

			if( pOpt ) {

				CML1Block * pBlock = pOpt->GetBlock(Addr);

				if( m_fRebuild ) {

					if( pBlock ) {

						pBlock->m_fMark = TRUE;
						}
					}

				else if( !m_fSelect ) {

					if( !pBlock ) {

						UINT uSpan = GetSpan(Addr);

						CAddress Prev;

						Prev.m_Ref = Addr.m_Ref;

						Prev.a.m_Offset -= uSpan;

						pBlock = pOpt->GetBlock(Prev);

						if( pBlock ) {

							CML1Block * pNew = new CML1Block(*pBlock, Addr, uSpan);

							if( pNew ) {

								if( pOpt->AppendBlock(pNew) ) {

									return TRUE;
									}

								delete pNew;
								}
							}

						Addr.m_Ref = 0;

						return FALSE;
						}

					return TRUE;
					}
				}
			
			pConfig->SetDirty();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CML1BaseDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr) )  {

		CML1DeviceOptions * pOpt = (CML1DeviceOptions *) pConfig;

		if( pOpt ) {

			pOpt->ExpandBlock(Addr, GetSpan(Addr));

			CSpaceML1 * pML1 = (CSpaceML1 *)GetSpace(Addr);

			if( pML1 ) { 

				if( pML1->IsFileRecord() ) {

					UINT uFile   = pML1->GetFile(Addr);

					UINT uRecord = pML1->GetRecord(Addr);

					Text.Printf( "%s%2.2u:%5.5u", pML1->m_Prefix,
								      uFile,
								      uRecord);

					if( pML1->m_uType != Addr.a.m_Type ) {

						Text += "." + pML1->GetTypeModifier(Addr.a.m_Type);
						}
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Notifications

void CML1BaseDriver::NotifyInit(CItem * pConfig)
{
	CML1DeviceOptions * pOpt = (CML1DeviceOptions *) pConfig;

	if( pOpt ) {

		m_fRebuild = TRUE;

		pOpt->Rebuild();

		m_fRebuild = FALSE;
		}
	}

void CML1BaseDriver::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	CML1DeviceOptions * pOpt = (CML1DeviceOptions *) pConfig;

	if( pOpt ) {

		CML1Block * pBlock = pOpt->GetBlock(Addr);

		if( pBlock ) {

			pBlock->SetSize(nSize);
			}
		}
	}
 
// Overridables

BOOL CML1BaseDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
		case 2:
		case 5:
		
			return FALSE;
		}

	return FALSE;
	}

// Implementation

void CML1BaseDriver::AddSpaces(void)
{
	AddSpace(New CSpaceML1(spHR,	"HR",	"Holding Registers",	10, 1,	   64999, addrWordAsWord, addrWordAsReal));
	
	//AddSpace(New CSpaceML1(spAI,	"AI",	"Analog Inputs",	10, 1,	   65535, addrWordAsWord, addrWordAsReal));
					  
	AddSpace(New CSpaceML1(spDC,	"DC",	"Digital Coils",	10, 1,	   65535, addrBitAsBit));
	
	//AddSpace(New CSpaceML1(spDI,	"DI",	"Digital Inputs",	10, 1,	   65535, addrBitAsBit));

	AddSpace(New CSpaceML1(spFR,	"FR",	"File Records",		10, 1,	   10000, addrWordAsWord, addrWordAsReal, 1, 1, 64));

	AddSpace(New CSpaceML1(spSR,	"SR",	"Special Registers",	10, 65000, 65535, addrWordAsWord, addrWordAsLong));
	}

UINT CML1BaseDriver::GetSpan(CAddress Addr)
{
	UINT uSpan = 1;
	
	switch( Addr.a.m_Type ) {
	
		case addrWordAsLong:
		case addrWordAsReal:
			
			uSpan = 2;
			break;
		}

	return uSpan;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Driver
//

// Instantiator

ICommsDriver *Create_ML1SerialDriver(void)
{
	return New CML1SerialDriver;
	}

// Constructor

CML1SerialDriver::CML1SerialDriver(void)
{
	m_wID		= 0x40BE;

	m_DriverName	= "ML1 Serial";
	
	C3_PASSED();
	}

// Binding Control

UINT CML1SerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CML1SerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CML1SerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CML1SerialDriverOptions);
	}

// Configuration

CLASS CML1SerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CML1SerialDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Tester Driver
//

// Instantiator

ICommsDriver *Create_ML1SerialTesterDriver(void)
{
	return New CML1SerialTesterDriver;
	}

// Constructor

CML1SerialTesterDriver::CML1SerialTesterDriver(void)
{
	m_wID		= 0x40C6;

	m_DriverName	= "ML1 Serial Tester";
	
	C3_PASSED();
	}

BOOL CML1SerialTesterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Ethernet Driver
//

// Instantiator

ICommsDriver *Create_ML1TCPDriver(void)
{
	return New CML1TCPDriver;
	}

// Constructor

CML1TCPDriver::CML1TCPDriver(void)
{
	m_wID		= 0x40BF;

	m_DriverName	= "ML1 TCP/IP";
	
	C3_PASSED();
	}

// Binding Control

UINT CML1TCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CML1TCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CML1TCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CML1DriverOptions);
	}

// Configuration

CLASS CML1TCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CML1TCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Ethernet Tester Driver
//

// Instantiator

ICommsDriver *Create_ML1TCPTesterDriver(void)
{
	return New CML1TCPTesterDriver;
	}

// Constructor

CML1TCPTesterDriver::CML1TCPTesterDriver(void)
{
	m_wID		= 0x40C7;

	m_DriverName	= "ML1 TCP/IP Tester";
	
	C3_PASSED();
	}

BOOL CML1TCPTesterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block List
//

// Dynamic Class

AfxImplementDynamicClass(CML1BlockList, CItemList);

// Constructor

CML1BlockList::CML1BlockList(void)
{
	}

// Item Access

CML1Block * CML1BlockList::GetItem(INDEX Index) const
{
	return (CML1Block *) CItemList::GetItem(Index);
	}

CML1Block * CML1BlockList::GetItem(UINT uPos) const
{
	return (CML1Block *) CItemList::GetItem(uPos);
	}

// Operations

CML1Block * CML1BlockList::AppendItem(CML1Block * pBlock)
{
	pBlock->SetParent(this);

	if( CItemList::AppendItem(pBlock) ) {

		return pBlock;
		}  

	return NULL;
	}

INDEX CML1BlockList::InsertItem(CML1Block * pID, CML1Block * pPre)
{
	pID->SetParent(this);
	
	return CItemList::InsertItem(pID, pPre);
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block UI Item 
//

// Dynamic Class

AfxImplementDynamicClass(CML1Block, CUIItem);

// Constructors

CML1Block::CML1Block(void)
{
	m_pDriver	= NULL;

	m_Addr.m_Ref	= 0;

	m_Size		= 0;

	m_TypeRef	= 0;

	m_SelText	= "";

	m_fMark		= FALSE;

	m_AddrRef	= 0;

	SetDefaults();

	AddMeta();
	}

CML1Block::CML1Block(CML1Block const &Block, CAddress Addr, UINT uSpan)
{
	m_pDriver	= Block.m_pDriver;

	m_Addr.m_Ref	= Addr.m_Ref;

	m_Size		= Block.m_Size;

	m_TypeRef	= Block.m_TypeRef;

	m_SelText	= Block.m_SelText;

	m_fMark		= FALSE;

	m_AddrRef	= Addr.m_Ref;

	m_Space		= Block.m_Space; 

	m_Reg		= Block.m_Reg + uSpan;

	m_Type		= Block.m_Type;

	m_File		= Block.m_File;

	m_Record	= Block.m_Record + uSpan;

	m_pRBE		= NULL;

	m_COV		= Block.m_COV;

	m_TO		= Block.m_TO;

	m_Retry		= Block.m_Retry;

	AddMeta();
	}

// UI Loading

BOOL CML1Block::OnLoadPages(CUIPageList * pList)
{
	CML1BlockUIPage * pPage = New CML1BlockUIPage(this);

	pList->Append(pPage);
		
	return FALSE;			   
	}
	
// UI Managament

void CML1Block::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Space" || Tag == "Reg" || Tag == "Type" || Tag == "File" || Tag == "Record" ) {

		UINT uSpace = SelectionToSpace(m_Space);

		BOOL fFile  = FALSE;

		LoadFields(pHost, pItem, uSpace);

		CSpaceML1 * pML1 = (CSpaceML1 *)GetSpace(uSpace);

		if( pML1 ) {

			fFile = pML1->IsFileRecord();

			if( !Tag.IsEmpty() ) {

				TypeMetaToRef(m_Type, uSpace);

				if( pML1->IsTypeOutOfRange(m_TypeRef) ) {

					m_Type = 0;
					}

				if( fFile ) {

					UpdateSelText(uSpace, m_Record, m_Type, m_File);
					}
				else {
					if( pML1->IsOutOfRange(m_Reg) ) { 

						MakeMax(m_Reg, pML1->m_uMinimum);

						if( pML1->IsOutOfRange(m_Reg) ) {

							MakeMin(m_Reg, pML1->m_uMinimum);
							}
						}

					UpdateSelText(uSpace, m_Reg, m_Type);
					}

				pHost->RemakeUI();
				}
			}

		DoEnables(pHost, uSpace, fFile);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Data Access

CString CML1Block::GetSelection(void)
{
	CSpace * pSpace = GetSpace();

	CString Sel;

	if( pSpace ) {

		CSpaceML1 * pML1 = (CSpaceML1 *)pSpace;

		if( pML1->IsFileRecord() ) {

			Sel = "%2.2u:%5.5u.";
	
			Sel.Printf(Sel, m_File, m_Record);
			}
		else {
			Sel = "%5.5u.";
	
			Sel.Printf(Sel, m_Reg);
			}

		Sel += pSpace->GetTypeModifier(pSpace->m_uType + m_Type);
		}

	return Sel;
	}

CSpace * CML1Block::GetSpace(void)
{
	CSpaceList Spaces = GetSpaceList();

	UINT	   uSpace = SelectionToSpace(m_Space);

	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i) ) {

		CSpace * pSpace = Spaces.GetAt(i);

		if( pSpace->m_uTable == uSpace ) {

			return pSpace;				
			}
		}

	return NULL;
	}

CSpaceList CML1Block::GetSpaceList(void)
{
	if( m_pDriver ) {

		return m_pDriver->GetSpaceList();
		}

	CSpaceList Empty;

	return Empty;
	}

BOOL CML1Block::SetAddress(CAddress Addr)
{
	m_Addr.m_Ref = Addr.m_Ref;

	m_AddrRef    = Addr.m_Ref;

	return TRUE;
	}

BOOL CML1Block::SetSize(UINT uSize)
{
	m_Size = uSize;

	return TRUE;
	}

UINT CML1Block::GetSize(void)
{
	return m_Size;
	}

UINT CML1Block::GetRef(void)
{
	return m_Addr.m_Ref;
	}

void CML1Block::SetDriver(CML1BaseDriver * pDriver)
{
	if( !m_pDriver ) {

		m_pDriver = pDriver;

		if( m_AddrRef == 0 ) {

			m_Space = SpaceToSelection(m_Space);

			if( m_Space ) {

				m_AddrRef = m_Addr.m_Ref;
				}
			}
		}
	}

CString CML1Block::GetSelText(void)
{
	return m_SelText;
	}

UINT CML1Block::SpaceToSelection(UINT uSpace)
{
	CSpaceList Spaces = GetSpaceList();

	UINT uIndex	  = 1;

	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i), uIndex++ ) {

		if( uSpace == Spaces.GetAt(i)->m_uTable ) {

			return uIndex;
			}
		}

	return 0;
	}

// Download Support

BOOL CML1Block::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr.m_Ref));

	Init.AddWord(WORD(m_Size));

	Init.AddByte(BYTE(m_COV));

	Init.AddWord(WORD(m_TO));

	Init.AddByte(BYTE(m_Retry));

	Init.AddItem(itemVirtual, m_pRBE);

	return TRUE;
	}

// Persistance

void CML1Block::PostLoad(void)
{
	CUIItem::PostLoad();

	if(m_AddrRef == 0 ) {

		// Pre-release version support

		m_Addr.a.m_Table = m_Space;

		m_Addr.a.m_Type  = m_TypeRef;

		if( m_Space == 5 ) {

			UINT uFile = m_File - 1;

			m_Addr.a.m_Offset  = m_Record;

			m_Addr.a.m_Extra   = uFile & 0xF;

			m_Addr.a.m_Offset |= ((uFile & 0x30) << 10);

			m_Reg = 0;

			return;
			}

		m_Addr.a.m_Offset = m_Reg;

		m_File = 0;

		m_Record = 0;

		return;
		}

	m_Addr.m_Ref = m_AddrRef;
	}

// Meta Data Creation

void CML1Block::AddMetaData(void)	
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Space);
	
	Meta_AddInteger(Reg);

	Meta_AddInteger(File);

	Meta_AddInteger(Record);

	Meta_AddInteger(Type);

	Meta_AddInteger(Size);

	Meta_AddInteger(COV);

	Meta_AddInteger(TO);

	Meta_AddInteger(Retry);

	Meta_AddVirtual(RBE);

	Meta_AddInteger(TypeRef);

	Meta_AddString(SelText);

	Meta_AddInteger(AddrRef);

	Meta_SetName(L"ML1_Block_Item");
	} 

// Implementation

void CML1Block::DoEnables(IUIHost *pHost, UINT uSpace, BOOL fFile)
{
	BOOL fEnable = uSpace != 0;

	BOOL fSR     = uSpace == spSR;

	pHost->EnableUI("Type",	fEnable);

	pHost->EnableUI("Reg",	(fEnable && !fFile));

	pHost->EnableUI("File",	fEnable && fFile);

	pHost->EnableUI("Record", fEnable && fFile);

	pHost->EnableUI("RBE",	fEnable && !fSR);
	
	pHost->EnableUI("COV",	fEnable && !fSR);

	//pHost->EnableUI("TO",	fEnable);	

	//pHost->EnableUI("Retry",fEnable);
	}

void CML1Block::SetDefaults(void)
{
	m_Space	 = 0; 

	m_Reg	 = 1;

	m_Type	 = 0;

	m_File   = 1;

	m_Record = 1;

	m_pRBE	 = NULL;

	m_COV	 = 1;

	m_TO	 = 300;

	m_Retry	 = 0;
	}

void CML1Block::LoadFields(IUIHost *pHost, CItem *pItem, UINT uSpace)
{	
	CSpaceList Spaces = GetSpaceList();

	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i) ) {

		CSpace * pSpace = Spaces.GetAt(i); 

		if( pSpace->m_uTable == uSpace || uSpace == 0 )  {

			CUITextEnum * pEnum = (CUITextEnum *) pHost->FindUI(L"Type");

			if( pEnum ) {

				CStringArray List;

				List.Append(pSpace->GetTypeText());

				if( pSpace->m_uSpan ) {

					for( UINT t = pSpace->m_uType + 1; t <= pSpace->m_uSpan; t++ ) {

						List.Append(pSpace->GetTypeAsText(t));
						}
					}

				pEnum->SetMask(NOTHING);
					
				pEnum->RebindUI(List);
				
				return;
				}
			}
		}
	}

void CML1Block::TypeMetaToRef(UINT uType, UINT uSpace)
{
	CSpaceList Spaces = GetSpaceList();

	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i) ) {

		CSpace * pSpace = Spaces.GetAt(i); 

		if( pSpace && pSpace->m_uTable == uSpace )  {

			m_TypeRef = pSpace->m_uSpan ? pSpace->m_uType + uType : pSpace->m_uType;

			return;
			}
		}
	}

void CML1Block::UpdateSelText(UINT uSpace, UINT uReg, UINT uType, UINT uFile)
{
	CSpaceList Spaces = GetSpaceList();

	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i) ) {

		CSpaceML1 * pSpace = (CSpaceML1 *)Spaces.GetAt(i); 

		if( pSpace && pSpace->m_uTable == uSpace )  {

			CString Sel = pSpace->m_Prefix;
			
			if( uFile ) {

				Sel += CString("%2.2u:%5.5u.");

				m_SelText.Printf(Sel, uFile, uReg);
				}
			else {
				Sel += CString("%5.5u.");
	
				m_SelText.Printf(Sel, uReg);
				}

			m_SelText += pSpace->GetTypeAsText(pSpace->m_uType + uType);
			}
		}
	}

// Helpers

CSpace * CML1Block::GetSpace(UINT uSpace)
{
	CSpaceList Spaces = GetSpaceList();

	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i) ) {

		CSpace * pSpace = Spaces.GetAt(i);

		if( pSpace->m_uTable == uSpace ) {

			return pSpace;				
			}
		}

	return NULL;
	}

UINT CML1Block::SelectionToSpace(UINT uSel)
{
	CSpaceList Spaces = GetSpaceList();

	UINT uIndex	  = 1;

	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i), uIndex++ ) {

		if( uSel == uIndex ) {

			return Spaces.GetAt(i)->m_uTable;
			}
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CML1BlockUIPage, CUIPage);

// Constructor

CML1BlockUIPage::CML1BlockUIPage(CML1Block * pBlock) 
{
	m_pBlock = pBlock;
	}

// Operations

BOOL CML1BlockUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_ITEM), 2);

	CString Item, Help, Type; 

	Item += CString(IDS_DRIVER_NONE);

	Type += Item;
			
	CSpaceList Spaces = m_pBlock->GetSpaceList();
		
	for( INDEX i = Spaces.GetHead(); !Spaces.Failed(i); Spaces.GetNext(i) ) {

		CSpace * pSpace = Spaces.GetAt(i);

		if( pSpace )  {
					
			Item += "|" + pSpace->m_Caption; 
			}
		}

	Help += CString(L"");

	CUIData Data;

	Data.m_Tag       = L"Space";
		
	Data.m_Label     = CString(IDS_ITEM);

	Data.m_ClassText = AfxNamedClass(L"CUITextEnum");
		
	Data.m_ClassUI   = AfxNamedClass(L"CUIDropDown");
		
	Data.m_Format    = Item;

	Data.m_Tip       = Help;

	pView->AddUI(pItem, L"root", &Data);

	pView->AddUI(pItem, L"root", L"");

	pView->AddUI(pItem, L"root", L"Type");

	pView->AddUI(pItem, L"root", L"");

	if( m_pBlock->m_Space != m_pBlock->SpaceToSelection(spFR) ) {
	
		pView->AddUI(pItem, L"root", L"Reg");

		pView->AddUI(pItem, L"root", L"");
		}
	else {
		pView->AddUI(pItem, L"root", L"File");
		
		pView->AddUI(pItem, L"root", L"Record");
		}

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_EXCHANGE), 1);

	pView->AddUI(pItem, L"root", L"RBE");

	pView->AddUI(pItem, L"root", L"COV");

	pView->EndGroup(TRUE);

/*	pView->StartGroup(CString(IDS_ADDITIONAL), 1);

	pView->AddUI(pItem, L"root", L"TO");

	pView->AddUI(pItem, L"root", L"Retry");

	pView->EndGroup(TRUE);

*/	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Space Wrapper Class
//

// Constructors

CSpaceML1::CSpaceML1(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT fd, UINT fn, UINT fx)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= x;
	m_uType		= t;
	m_uSpan         = s > t ? s : t;
	m_FileDef	= fd;
	m_FileMin	= fn;
	m_FileMax	= fx;
	
	FindAlignment();

	FindWidth();
	}

// Data Access

BOOL CSpaceML1::IsFileRecord(void)
{
	return m_FileMax > 0;
	}

// Data Encoding

UINT CSpaceML1::GetFile(CAddress Addr)
{
	UINT uFile = Addr.a.m_Extra | ((Addr.a.m_Offset & 0xC000) >> 10);

	return uFile + 1;
	}

UINT CSpaceML1::GetRecord(CAddress Addr)
{
	return Addr.a.m_Offset & 0x3FFF;
	}

BOOL CSpaceML1::SetFile(CAddress &Addr, UINT uFile)
{
	uFile -= 1;

	Addr.a.m_Extra = uFile & 0xF;

	Addr.a.m_Offset &= ~0xC000;

	Addr.a.m_Offset |= ((uFile & 0x30) << 10);

	return TRUE;
	}

BOOL CSpaceML1::SetRecord(CAddress &Addr, UINT uRecord)
{
	Addr.a.m_Offset &= ~0x3FFF;

	Addr.a.m_Offset |= uRecord & 0x3FFF;

	return TRUE;
	}

// Type Checking

BOOL CSpaceML1::IsTypeOutOfRange(UINT uRef)
{
	return uRef < m_uType || uRef > m_uSpan || uRef == 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block View Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CML1BlockDialog, CStdDialog);
		
// Constructor

CML1BlockDialog::CML1BlockDialog(CML1DeviceOptions * pOptions)
{
	m_pOptions = pOptions;

	m_Blocks.Empty();

	SetName(L"ML1BlockDlg");
	}

// Message Map

AfxMessageMap(CML1BlockDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(1003, OnCopy)
	
	AfxMessageEnd(CML1BlockDialog)
	};
 
// Message Handlers

BOOL CML1BlockDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadBlockList();
		
	GetDlgItem(1002).SetWindowText(TEXT("The following block(s) are configured for this device."));

	return TRUE;
	}

// Command Handlers

BOOL CML1BlockDialog::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CML1BlockDialog::OnCopy(UINT uID)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("ML1 Files"));

	Dlg.SetCaption(CPrintf("ML1 Blocks"));

	Dlg.SetFilter(TEXT("TEXT Files (*.txt)|*.txt|CSV Files (*.csv)|*.csv"));

	if( Dlg.Execute(*afxMainWnd) ) {

		FILE *pFile;

		CString Text = "";
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			Text = CString(IDS_UNABLE_TO_OPEN_2);

			afxMainWnd->Error(Text);
			}
		else {
			CString Path = Dlg.GetFilename();
			
			CString Error = "";

			BOOL fCSV = Dlg.GetFilename().EndsWith(TEXT(".csv"));
			
			UINT uCount = m_Blocks.GetCount();

			AfxAssume(pFile);

			for( UINT u = 0; u < uCount; u++ ) {

				CString Text = m_Blocks.GetAt(u);  

				if( fCSV ) {

					Text.Replace('\t', ',');

					Text.Remove('\r');
					}

				fprintf(pFile, TEXT("%s"), PCTXT(Text));
				}

			Information(CString(IDS_COPY_IS_COMPLETE));

			fclose(pFile);
			}
		}

	return TRUE;
	}

// Implementation

void CML1BlockDialog::LoadBlockList(void)
{
	if( m_pOptions ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		Box.ResetContent();

		int nTab[] = { 80, 150, };

		Box.SetTabStops(elements(nTab), nTab);

		CML1BlockList * pList = m_pOptions->GetBlockList();

		if( pList ) {

			for( INDEX i = pList->GetHead(); !pList->Failed(i); pList->GetNext(i) ) {

				CML1Block * pBlock = pList->GetItem(i);

				CString BLOCK = pBlock->GetSelText();

				UINT uFind = BLOCK.Find('.');

				if( uFind < NOTHING ) {

					BLOCK.Printf("%s\t%s\t%u", BLOCK.Mid(0, uFind), 
								   BLOCK.Mid(uFind + 1), 
								   pBlock->GetSize());
					Box.AddString(BLOCK);

					BLOCK += L"\n";

					m_Blocks.Append(BLOCK);
					}
				}
			}
		}
	}


// End of File
