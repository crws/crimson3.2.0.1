
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DRVTABLE_HPP
	
#define	INCLUDE_DRVTABLE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Test Table Comms Driver
//

class CTestTableCommsDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CTestTableCommsDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
