
#include "intern.hpp"

#include "3psnet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader(); 

//////////////////////////////////////////////////////////////////////////
//
// 3PSNet Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(C3psNetDriverOptions, CUIItem);

// Constructor

C3psNetDriverOptions::C3psNetDriverOptions(void)
{
	m_Source = 0;
	}

// UI Managament

void C3psNetDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Source")  {

		pWnd->EnableUI("Source", FALSE);
		}
	}

// Download Support

BOOL C3psNetDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Source));

	return TRUE;
	}

// Meta Data Creation

void C3psNetDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Source);
	}


//////////////////////////////////////////////////////////////////////////
//
// 3PSNet Device Options
//

// Dynamic Class

AfxImplementDynamicClass(C3psNetDeviceOptions, CUIItem);

// Constructor

C3psNetDeviceOptions::C3psNetDeviceOptions(void)
{
	m_ID		= 1;

	m_DeviceType	= 0;

	m_Custom	= 0;

	m_Wireless	= 0;

	m_Interval	= 150;

	m_Transact	= 2500;
	}

// UI Managament

void C3psNetDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "DeviceType" )  {

		BOOL fCustom = IsCustom(); 

		if( !fCustom ) {

			pItem->GetDataAccess("Wireless")->WriteInteger(pItem, IsWireless());
			}

		pWnd->UpdateUI("Wireless");

		pWnd->EnableUI("Custom",   fCustom);

		pWnd->EnableUI("Wireless", fCustom);
		}

	if( Tag == "DeviceType"  )  {

		UINT uPoll = IsWireless() ? 1500 : 150;

		pItem->GetDataAccess("Interval")->WriteInteger(pItem, uPoll);

		pWnd->UpdateUI("Interval");
		}
	}

// Data Access

BYTE C3psNetDeviceOptions::GetDeviceType(void)
{
	switch( m_DeviceType ) {

		case posLoadSensHard:		return devLoadSensHard;
		case posLoadSensWireless:	return devLoadSensWireless;
		case posA2BSensorHard:		return devA2BSensorHard;
		case posA2BSensorWireless:	return devA2BSensorWireless;
		case posAngleSensorHardBoom:	return devAngleSensorHardBoom;
		case posRelayOutputHard4:	return devRelayOutputHard4;
		case posWindSpeedWireless:	return devWindSpeedWireless;
		case posPayoutHard:		return devPayoutHard;
		case posRemoteAntenna:		return devRemoteAntenna;
		case posSpeedHard:		return devSpeedHard;
		case posAnalogOuputHard1:	return devAnalogOuputHard1;
		case posAnalogInput:		return devAnalogInput;
		case posSwitchInputsHard8:	return devSwitchInputsHard8;
		case posSlewSensorHard:		return devSlewSensorHard;
		case posDualAngleSensorHard:	return devDualAngleSensorHard;
		case posPayoutWireless:		return devPayoutWireless;
		case posPayoutSpeedWireless:	return devPayoutSpeedWireless;
		case posTorqueWireless:		return devTorqueWireless;
		case posTorqueHookWireless:	return devTorqueHookWireless;
		case posPressureSensor:		return devPressureSensor;
		case posDualAngleWireless:	return devDualAngleWireless;
		case posBoomLengthHard:		return devBoomLengthHard;
		}

	return devCustom;
	}

BOOL C3psNetDeviceOptions::IsCustom(void)
{
	return GetDeviceType() == devCustom;
	}

// Download Support

BOOL C3psNetDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_ID));

	Init.AddByte(GetDeviceType());

	Init.AddByte(BYTE(m_Custom));

	Init.AddByte(BYTE(m_Wireless));

	Init.AddLong(LONG(m_Interval));

	Init.AddWord(WORD(m_Transact));

	return TRUE;
	}

// Meta Data Creation

void C3psNetDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(ID);

	Meta_AddInteger(DeviceType);

	Meta_AddInteger(Custom);

	Meta_AddInteger(Wireless);

	Meta_AddInteger(Interval);

	Meta_AddInteger(Transact);
	}

// Implementation

BOOL C3psNetDeviceOptions::IsWireless(void)
{
	switch( m_DeviceType ) {

		case posLoadSensWireless:
		case posA2BSensorWireless:
		case posWindSpeedWireless:
		case posPayoutWireless:
		case posPayoutSpeedWireless:
		case posTorqueWireless:
		case posTorqueHookWireless:
		case posDualAngleWireless:

			return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// 3PSNet CAN Bus Driver
//

// Instantiator

ICommsDriver *	Create_3psNetDriver(void)
{
	return New C3psNetDriver;
	}


// Constructor

C3psNetDriver::C3psNetDriver(void)
{
	m_wID		= 0x40BB;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "3 Point Solutions";
	
	m_DriverName	= "3PS Network Protocol";
	
	m_Version	= "1.20";
	
	m_ShortName	= "3PSNet";

	m_uMsgs		= 0;

	AddSpaces();
	}

// Binding Control

UINT C3psNetDriver::GetBinding(void)
{
	return bindCAN;
	}

void C3psNetDriver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 125000;

	CAN.m_Terminate = FALSE;
	}

// Configuration

CLASS C3psNetDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(C3psNetDriverOptions);
	}


CLASS C3psNetDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(C3psNetDeviceOptions);
	}

// Address Management

BOOL C3psNetDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case messGetConfigRevision:

			case messBroadcastStatus:
			case messBroadcastBattery:
			case messBroadcastTemp:
			case messBroadcastValue:

				return TRUE;
			}
		}

	return FALSE;
	}

BOOL C3psNetDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	C3psNetDeviceOptions * pOpt = (C3psNetDeviceOptions *) pConfig;

	if( pOpt ) {

		DeleteAllSpaces();

		AddSpaces(pOpt->GetDeviceType(), pOpt->IsCustom());
		}

	CStdAddrDialog Dlg(ThisObject, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void C3psNetDriver::AddSpaces(void)     
{
	AddSpaces(devLoadSensHard, FALSE);
	}

void C3psNetDriver::AddSpaces(UINT uDeviceType, BOOL fCustom)     
{
	// Broadcast messages

	AddMsg(messBroadcastStatus);
	AddMsg(messBroadcastBattery);	
	//AddMsg(messBroadcastTemp);	// reserved for factory use
	AddMsg(messBroadcastValue);

	// Explicit messages

	AddMsg(messGetConfigRevision);

	UINT uDevice = uDeviceType;

	if( uDevice == devLoadSensHard || uDevice == devLoadSensWireless || fCustom ) {

		AddMsg(messSetShuntCal);
		AddMsg(messSetRawData);
		AddMsg(messGetCapacity);
		AddMsg(messGetSpan0);
		AddMsg(messGetZero0);
		AddMsg(messGetTare0);
		AddMsg(messGetCold0);
		AddMsg(messGetRoom0);
		AddMsg(messGetHot0);
		AddMsg(messGetTempPoint0);
		AddMsg(messGetFilterThreshold);
		AddMsg(messGetFilterConstant);
		}

	if( uDevice == devRelayOutputHard4 || fCustom ) {

		AddMsg(messIntRelayValue);
		AddMsg(messSetRelayValue);
		}
			
	if( uDevice == devRemoteAntenna || fCustom ) {

		AddMsg(messGetTXID);
		AddMsg(messSetPowerDown);
		}

	if( uDevice == devAngleSensorHardBoom || fCustom ) {

		AddMsg(messZeroAngle);
		}

	if( uDevice == devBoomLengthHard || fCustom ) {

		AddMsg(messResetLength);
		}
	}

void C3psNetDriver::AddMsg(UINT uSpace)     
{
	BYTE bTable = uSpace & 0xFF;

	switch( uSpace ) {

		// Broadcast messages

		case messBroadcastStatus:	AddSpace(New CSpace(addrNamed,	"STAT", "Sensor Status",	10,  uSpace,	0, addrByteAsByte));	break;
		case messBroadcastBattery:	AddSpace(New CSpace(addrNamed,	"BAT",	"Current Battery Level",10,  uSpace,	0, addrByteAsByte));	break;
		case messBroadcastTemp:		AddSpace(New CSpace(addrNamed,	"TEMP",	"Sensor Temperature",	10,  uSpace,	0, addrByteAsByte));	break;
		case messBroadcastValue:	AddSpace(New CSpace(addrNamed,	"DATA",	"Device Data",		10,  uSpace,	0, addrLongAsLong));	break;

		// Explicit messages

		case messGetConfigRevision:	AddSpace(New CSpace(addrNamed,	"REV",	"Config Revision",	10,  uSpace,	0, addrLongAsLong));	break;
		case messSetShuntCal:		AddSpace(New CSpace(addrNamed,	"SCAL",	"Set Shunt Cal",	10,  uSpace,	0, addrBitAsBit));	break;
		case messSetRawData:		AddSpace(New CSpace(addrNamed,	"SRAW",	"Set Raw Data",		10,  uSpace,	0, addrLongAsLong));	break;
		case messGetCapacity:		AddSpace(New CSpace(addrNamed,	"CAP",	"Capacity",		10,  uSpace, 	0, addrRealAsReal));	break;
		case messGetSpan0:		AddSpace(New CSpace(bTable,	"SPAN",	"Span",			10,	  0,	1, addrRealAsReal));	break;
		case messGetZero0:		AddSpace(New CSpace(bTable,	"ZERO",	"Zero",			10,       0,	1, addrRealAsReal));	break;
		case messGetTare0:		AddSpace(New CSpace(bTable,	"TARE", "Tare",			10,       0,	1, addrRealAsReal));	break;
		case messGetCold0:		AddSpace(New CSpace(bTable,	"COLD",	"Cold",			10,	  0,	1, addrRealAsReal));	break;
		case messGetRoom0:		AddSpace(New CSpace(bTable,	"ROOM", "Room",			10,	  0,	1, addrRealAsReal));	break;
		case messGetHot0:		AddSpace(New CSpace(bTable,	"HOT",	"Hot",			10,	  0,	1, addrRealAsReal));	break;
		case messGetTempPoint0:		AddSpace(New CSpace(bTable,	"TP",	"Temp Point",		10,	  0,	1, addrLongAsLong));	break;
		case messGetFilterThreshold:	AddSpace(New CSpace(addrNamed,	"FILT", "Filter Threshold",	10,  uSpace,	0, addrRealAsReal));	break;
		case messGetFilterConstant:	AddSpace(New CSpace(addrNamed,	"FILC",	"Filter Constant",	10,  uSpace,	0, addrRealAsReal));	break;
		case messIntRelayValue:		AddSpace(New CSpace(bTable,	"IRLY",	"Relay Value",		10,	  1,	4, addrBitAsBit));	break;
		case messSetRelayValue:		AddSpace(New CSpace(addrNamed,	"SRLY",	"Set Relay Values",	10,  uSpace,	0, addrBitAsBit));	break;
		case messGetTXID:		AddSpace(New CSpace(addrNamed,	"TXID",	"TXID",			10,  uSpace,	0, addrLongAsLong));	break;
		case messSetPowerDown:		AddSpace(New CSpace(addrNamed,	"SPWR",	"Set Power Down",	10,  uSpace,	0, addrBitAsBit));	break;
		case messZeroAngle:		AddSpace(New CSpace(addrNamed,	"ZANG",	"Zero Angle",		10,  uSpace,	0, addrBitAsBit));	break;  
		case messResetLength:		AddSpace(New CSpace(addrNamed,	"RLEN",	"Reset Length",		10,  uSpace,	0, addrRealAsReal));	break;
		}
	}


// End of File
