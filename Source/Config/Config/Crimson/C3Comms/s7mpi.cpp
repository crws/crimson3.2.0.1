
#include "intern.hpp"

#include "s7mpi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CS7MPIDriverOptions, CUIItem);

// Constructor

CS7MPIDriverOptions::CS7MPIDriverOptions(void)
{
	m_LastDrop = 31;
	}

// Download Support

BOOL CS7MPIDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_LastDrop));

	return TRUE;
	}

// Meta Data Creation

void CS7MPIDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(LastDrop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS7MPIDeviceOptions, CUIItem);

// Constructor

CS7MPIDeviceOptions::CS7MPIDeviceOptions(void)
{
	m_MPIDrop  = 1;
	
	m_Drop	   = 2;

	m_Timeout  = 800;
	}

// Download Support

BOOL CS7MPIDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_MPIDrop));

	Init.AddByte(BYTE(m_Drop));

	Init.AddLong(LONG(m_Timeout));

	return TRUE;
	}

// Meta Data Creation

void CS7MPIDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(MPIDrop);

	Meta_AddInteger(Drop);

	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Space Wrapper
//

// Constructor

CSpaceS7::CSpaceS7(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s)
{
	m_uTable	= uTable;
	
	m_Prefix	= p;

	m_Caption	= c;

	m_uRadix	= r;

	m_uMinimum	= n;
	
	m_uMaximum	= x;
	
	m_uType		= t;

	m_uSpan         = s;

	FindWidth();

	FindAlignment();
	}

// Matching

BOOL CSpaceS7::MatchSpace(CAddress const &Addr)
{
	return m_uTable == (Addr.a.m_Table & 0x0F);
	}

// Limits

void CSpaceS7::GetMinimum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);

	if( m_uTable == 0x0A ) {

		UINT uBlock = 1;

		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= uBlock & 0xF0;

		Addr.a.m_Offset|= ((uBlock & 0x700) << 5);
		}
	}

void CSpaceS7::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	if( m_uTable == 0x0A ) {

		UINT uBlock = 2047; 

		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= uBlock & 0xF0;

		Addr.a.m_Offset|= ((uBlock & 0x700) << 5);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Driver
//

// Instantiator

ICommsDriver *	Create_S7MPIDriver(void)
{
	return New CS7MPIDriver;
	}

// Constructor

CS7MPIDriver::CS7MPIDriver(void)
{
	m_wID		= 0x3367;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 via MPI Adapter";
	
	m_Version	= "1.01";
	
	m_ShortName	= "S7 MPI";

	AddSpaces();
	}

// Configuration

CLASS CS7MPIDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CS7MPIDriverOptions);
	}

// Configuration

CLASS CS7MPIDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CS7MPIDeviceOptions);
	}

// Binding Control

UINT CS7MPIDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CS7MPIDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}

// Implementation

void CS7MPIDriver::AddSpaces(void)
{
	AddSpace(New CSpaceS7(0x01, "QB", "Output Bytes",  10, 0,  8191, addrByteAsByte, addrByteAsLong));
   
	AddSpace(New CSpaceS7(0x02, "IB", "Input Bytes",   10, 0,  8191, addrByteAsByte, addrByteAsLong));
	
	AddSpace(New CSpaceS7(0x03, "MB", "Flag Bytes",	   10, 0,  8191, addrByteAsByte, addrByteAsLong));

	AddSpace(New CSpaceS7(0x04, "T",  "Timer Value",   10, 1,  8191, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS7(0x05, "C",  "Counter Value", 10, 1,  8191, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS7(0x0A, "DB", "Data Blocks",   10, 0,  8191, addrByteAsByte, addrByteAsLong));
	}

// Address Management

BOOL CS7MPIDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CS7MPIDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CS7MPIDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uBlock = 0;
				  
	if( pSpace->m_uTable == 0x0A ) {

		UINT uFind = Text.Find(':');

		if( uFind == NOTHING ) {

			uBlock = 1;
			}
		else {
			uBlock = tatoi(Text);

			Text   = Text.Mid(uFind + 1);

			if( uBlock < 1 || uBlock > 2047 ) {

				Error.Set( "Invalid data block number",
					   0
					   );

				return FALSE;
				}
			}
		}

	Addr.a.m_Offset &= 0x1FFF;

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {
		
		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= uBlock & 0xF0;

		Addr.a.m_Offset|= ((uBlock & 0x700) << 5);

		return TRUE; 
		}

	return FALSE;
	}

BOOL CS7MPIDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uOffset = Addr.a.m_Offset & 0x1FFF;

		UINT uType   = AddrType(Addr.a.m_Type);

		UINT uBlock  = Addr.a.m_Extra;

		uBlock      |= Addr.a.m_Table & 0xF0;

		uBlock	    |= ((Addr.a.m_Offset & 0xE000) >> 5);

		if( pSpace->m_uTable == 0x0A ) {

			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%4.4u:%s", 
					     pSpace->m_Prefix, 
					     uBlock,
					     pSpace->GetValueAsText(uOffset)
					     );
				}
	
			else {
				Text.Printf( "%s%4.4u:%s.%s", 
					     pSpace->m_Prefix, 
					     uBlock,
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}
		else {
			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			else {
				Text.Printf( "%s%s.%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CS7MPIDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 0x01:
		case 0x02:
		case 0x03:
		case 0x0A:

			return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CS7MPIDialog, CStdAddrDialog);
		
// Constructor

CS7MPIDialog::CS7MPIDialog(CS7MPIDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "S7MPIElementDlg";
	}

// Overridables

void CS7MPIDialog::SetAddressFocus(void)
{
	if( m_pSpace && m_pSpace->m_uTable == 0x0A ) {

		SetDlgFocus(2005);

		return;
		}
	
	SetDlgFocus(2002);
	}

void CS7MPIDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	if( m_pSpace && m_pSpace->m_uTable == 0x0A ) {

		if( fEnable ) {

			UINT uFind = Text.Find(':');

			GetDlgItem(2002).SetWindowText(Text.Left(uFind));

			GetDlgItem(2004).SetWindowText(":");

			GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fEnable);
		
		GetDlgItem(2005).EnableWindow(fEnable);
		}
	else {
		if( fEnable ) {

			GetDlgItem(2002).SetWindowText(Text);

			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2005).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		}
	}

CString CS7MPIDialog::GetAddressText(void)
{
	if( m_pSpace && m_pSpace->m_uTable == 0x0A ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += ":";
		
		Text += GetDlgItem(2005).GetWindowText();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS7MPITCPDeviceOptions, CS7IsoTCPDeviceOptions);

// Constructor

CS7MPITCPDeviceOptions::CS7MPITCPDeviceOptions(void)
{
	m_Type = 0;

	m_Slot = 2;
	}

// UI Managament

void CS7MPITCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "Type" )  {

			BOOL fEnable = pItem->GetDataAccess("Type")->ReadInteger(pItem);

			BOOL fTSAP = pItem->GetDataAccess("TSAP")->ReadInteger(pItem);

			pWnd->EnableUI("Conn", fEnable && !fTSAP);

			pWnd->EnableUI("Rack", !fEnable && !fTSAP);

		//	pWnd->EnableUI("Client", fEnable);
			}  

		if( Tag.IsEmpty() || Tag == "TSAP" ) {

			BOOL fEnable = pItem->GetDataAccess("TSAP")->ReadInteger(pItem);

			BOOL fType   = pItem->GetDataAccess("Type")->ReadInteger(pItem);

			pWnd->EnableUI("Type",   !fEnable);
					         
			pWnd->EnableUI("Conn",   !fEnable && fType);
					         
			pWnd->EnableUI("Client", !fEnable);
					         
			pWnd->EnableUI("Rack",   !fEnable && !fType);
					         
			pWnd->EnableUI("Slot",   !fEnable);
					         
			pWnd->EnableUI("STsap",  fEnable);
					         
			pWnd->EnableUI("CTsap",  fEnable);

			pWnd->EnableUI("STsap2", fEnable && m_Addr2);

			pWnd->EnableUI("CTsap2", fEnable && m_Addr2);
			}

		if( Tag.IsEmpty() || Tag == "Addr2" ) {

			BOOL fEnable = pItem->GetDataAccess("Addr2")->ReadInteger(pItem) != 0;

			BOOL fTSAP   = pItem->GetDataAccess("TSAP")->ReadInteger(pItem);

			pWnd->EnableUI("STsap2", fTSAP && fEnable);

			pWnd->EnableUI("CTsap2", fTSAP && fEnable);
			}
		
		if( Tag == "Conn" ) {

			SetHexadecimal(pWnd, Tag, m_Conn, m_uConn);

			pWnd->UpdateUI("Conn");
			}

		if( Tag == "Client" ) {

			SetHexadecimal(pWnd, Tag, m_Client, m_uClient);

			pWnd->UpdateUI("Client");
			}
	       	}    
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI over TCP/IP driver
//

// Instantiator

ICommsDriver * Create_CS7MPITCPDriver(void)
{
	return New CS7MPITCPDriver;
	}

// Constructor

CS7MPITCPDriver::CS7MPITCPDriver(void)
{
	m_wID		= 0x3517;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 300/400 TCP/IP Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "S7 300/400 TCP/IP";

	C3_PASSED();
	}

// Binding Control

UINT CS7MPITCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CS7MPITCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CS7MPITCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CS7MPITCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CS7MPITCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Option Card - Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CS7DirectDriverOptions, CUIItem);

// Constructor

CS7DirectDriverOptions::CS7DirectDriverOptions(void)
{
	m_BaudRate = 6;

	m_ThisDrop = 7;

	m_LastDrop = 1;
	}

// Download Support

BOOL CS7DirectDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_BaudRate));

	Init.AddByte(BYTE(m_ThisDrop));

	Init.AddByte(BYTE(m_LastDrop));

	return FALSE;
	}

// Meta Data Creation

void CS7DirectDriverOptions::AddMetaData(void)
{
	Meta_AddInteger(BaudRate);

	Meta_AddInteger(ThisDrop);

	Meta_AddInteger(LastDrop);	
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Option Card - Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS7DirectDeviceOptions, CUIItem);

// Constructor

CS7DirectDeviceOptions::CS7DirectDeviceOptions(void)
{
	m_ThatDrop = 2;
	}

// Download Support

BOOL CS7DirectDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);
	
	Init.AddByte(BYTE(m_ThatDrop));

	return FALSE;
	}

// Meta Data Creation

void CS7DirectDeviceOptions::AddMetaData(void)
{
	Meta_AddInteger(ThatDrop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Option Card
//

// Instantiator

ICommsDriver *	Create_S7DirectDriver(void)
{
	return New CS7DirectDriver;
	}

// Constructor

CS7DirectDriver::CS7DirectDriver(void)
{
	m_wID		= 0x404D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 300/400 Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "S7 300/400";

	AddSpaces();
	}

// Binding Control

UINT CS7DirectDriver::GetBinding(void)
{
	return bindMPI;
	}

void CS7DirectDriver::GetBindInfo(CBindInfo &Info)
{
	CBindMPI &MPI = (CBindMPI &) Info;

	AfxTouch(MPI);
	}

// Configuration

CLASS CS7DirectDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CS7DirectDriverOptions);
	}

CLASS CS7DirectDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CS7DirectDeviceOptions);
	}

// Implementation

void CS7DirectDriver::AddSpaces(void)
{
	DeleteAllSpaces();
	
	AddSpace(New CSpaceS7(0x01, "OB", "Output",	10, 0,  8191, addrByteAsByte, addrByteAsLong));
   
	AddSpace(New CSpaceS7(0x02, "IB", "Inputs",	10, 0,  8191, addrByteAsByte, addrByteAsLong));
	
	AddSpace(New CSpaceS7(0x03, "MB", "Memory",	10, 0,  8191, addrByteAsByte, addrByteAsLong));

	AddSpace(New CSpaceS7(0x04, "T",  "Timer",	10, 0,  8191, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS7(0x05, "C",  "Counter",	10, 0,  8191, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS7(0x0A, "DB", "Data Block",	10, 0,  8191, addrByteAsByte, addrByteAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Space Wrapper
//

// Constructor

CSpaceS7Ext::CSpaceS7Ext(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s)
{
	m_uTable	= uTable;
	
	m_Prefix	= p;

	m_Caption	= c;

	m_uRadix	= r;

	m_uMinimum	= n;
	
	m_uMaximum	= x;
	
	m_uType		= t;

	m_uSpan         = s;

	FindWidth();

	FindAlignment();
	}

// Matching

BOOL CSpaceS7Ext::MatchSpace(CAddress const &Addr)
{
	return m_uTable == (Addr.a.m_Table & 0x07);
	}

// Limits

void CSpaceS7Ext::GetMinimum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);

	if( ((m_uTable & 0x7) == 0x06) ) {

		UINT uBlock = 1;

		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= ((uBlock & 0xF0) >> 1);
		}
	}

void CSpaceS7Ext::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	if( ((m_uTable & 0x7) == 0x06) ) {

		UINT uBlock = 255; 

		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= ((uBlock & 0xF0) >> 1);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 300 Extended DB TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS7MPIExtTCPDeviceOptions, CS7MPITCPDeviceOptions);

// Constructor

CS7MPIExtTCPDeviceOptions::CS7MPIExtTCPDeviceOptions(void)
{
	
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI Extended over TCP/IP driver
//

// Instantiator

ICommsDriver * Create_S7MpiExtTcpDriver(void)
{
	return New CS7MpiExtTcpDriver;
	}

// Constructor

CS7MpiExtTcpDriver::CS7MpiExtTcpDriver(void)
{
	m_wID		= 0x4086;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 300/400 Extd DB TCP/IP Master";
	
	m_Version	= "BETA";
	
	m_ShortName	= "S7 300/400 Extd DB";

	AddSpaces();
	}

// Binding Control

UINT CS7MpiExtTcpDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CS7MpiExtTcpDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CS7MpiExtTcpDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CS7MpiExtTcpDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CS7MPIExtTCPDeviceOptions);
	}

// Implementation

void CS7MpiExtTcpDriver::AddSpaces(void)
{
	AddSpace(New CSpaceS7Ext(0x01, "QB", "Output Bytes",  10, 0,   8191, addrByteAsByte, addrByteAsLong));
   
	AddSpace(New CSpaceS7Ext(0x02, "IB", "Input Bytes",   10, 0,   8191, addrByteAsByte, addrByteAsLong));
	
	AddSpace(New CSpaceS7Ext(0x03, "MB", "Flag Bytes",    10, 0,   8191, addrByteAsByte, addrByteAsLong));

	AddSpace(New CSpaceS7Ext(0x04, "T",  "Timer Value",   10, 1,    512, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS7Ext(0x05, "C",  "Counter Value", 10, 1,    512, addrWordAsWord, addrWordAsWord));

	AddSpace(New CSpaceS7Ext(0x06, "DB", "Data Blocks",   10, 0,  65535, addrByteAsByte, addrByteAsLong));
	}

// Address Management

BOOL CS7MpiExtTcpDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CS7MPIXDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CS7MpiExtTcpDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uBlock = 0;
				  
	if( IsDB(pSpace->m_uTable) ) {

		UINT uFind = Text.Find(':');

		if( uFind == NOTHING ) {

			uBlock = 1;
			}
		else {
			uBlock = tatoi(Text);

			Text   = Text.Mid(uFind + 1);

			if( uBlock < 1 || uBlock > 255 ) {

				Error.Set( "Invalid data block number",
					   0
					   );

				return FALSE;
				}
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {
		
		Addr.a.m_Extra	= uBlock & 0x0F;

		Addr.a.m_Table |= ((uBlock & 0xF0) >> 1);

		return TRUE; 
		}

	return FALSE;
	}

BOOL CS7MpiExtTcpDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uOffset = Addr.a.m_Offset;

		UINT uType   = AddrType(Addr.a.m_Type);

		UINT uBlock  = Addr.a.m_Extra;

		uBlock      |= ((Addr.a.m_Table & 0x78) << 1);

		if( IsDB(Addr.a.m_Table) ) {

			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%3.3u:%s", 
					     pSpace->m_Prefix, 
					     uBlock,
					     pSpace->GetValueAsText(uOffset)
					     );
				}
	
			else {
				Text.Printf( "%s%3.3u:%s.%s", 
					     pSpace->m_Prefix, 
					     uBlock,
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}
		else {
			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			else {
				Text.Printf( "%s%s.%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CS7MpiExtTcpDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

BOOL CS7MpiExtTcpDriver::IsDB(UINT uTable)
{
	return ((uTable & 0x7) == 0x6);
	}


//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Driver Extended Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CS7MPIXDialog, CStdAddrDialog);
		
// Constructor

CS7MPIXDialog::CS7MPIXDialog(CS7MpiExtTcpDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "S7MPIElementDlg";
	}

// Overridables

void CS7MPIXDialog::SetAddressFocus(void)
{
	CS7MpiExtTcpDriver * pDriver = (CS7MpiExtTcpDriver *)m_pDriver;
	
	if( m_pSpace && pDriver->IsDB(m_pSpace->m_uTable) ) {

		SetDlgFocus(2005);

		return;
		}
	
	SetDlgFocus(2002);
	}

void CS7MPIXDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	CS7MpiExtTcpDriver * pDriver = (CS7MpiExtTcpDriver *)m_pDriver;
	
	if( m_pSpace && pDriver->IsDB(m_pSpace->m_uTable) ) {

		if( fEnable ) {

			UINT uFind = Text.Find(':');

			GetDlgItem(2002).SetWindowText(Text.Left(uFind));

			GetDlgItem(2004).SetWindowText(":");

			GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fEnable);
		
		GetDlgItem(2005).EnableWindow(fEnable);
		}
	else {
		if( fEnable ) {

			GetDlgItem(2002).SetWindowText(Text);

			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2005).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		}
	}

CString CS7MPIXDialog::GetAddressText(void)
{
	CS7MpiExtTcpDriver * pDriver = (CS7MpiExtTcpDriver *)m_pDriver;
	
	if( m_pSpace && pDriver->IsDB(m_pSpace->m_uTable) ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += ":";
		
		Text += GetDlgItem(2005).GetWindowText();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

// End of File
