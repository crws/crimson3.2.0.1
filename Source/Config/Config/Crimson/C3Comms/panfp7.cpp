#include "intern.hpp"

#include "panfp7.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
//  Panasonic FP7 Map Definition
//

bool CFpMap::operator==(const CFpMap &That) const
{	
	return ((m_Map == That.m_Map) && (m_Addr.a.m_Type == That.m_Addr.a.m_Type));
	}

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CPanFp7DriverOptions, CUIItem);

// Constructor

CPanFp7DriverOptions::CPanFp7DriverOptions(void)
{
	}

// Download Support

BOOL CPanFp7DriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);
	
	return TRUE;
	}

// Meta Data Creation

void CPanFp7DriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CPanFp7DeviceOptions, CUIItem);

// Constructor

CPanFp7DeviceOptions::CPanFp7DeviceOptions(void)
{
	m_fUpdate = FALSE;

	EmptyMap();
	}

// Download Support

void CPanFp7DeviceOptions::PrepareData(void)
{
	m_MapArray.Sort();
	}

BOOL CPanFp7DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	MakeInitDataMap(Init);

	return TRUE;
	}

// Persistance

void CPanFp7DeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"FpMapList" ) {

			Tree.GetCollect();
			
			LoadMap(Tree);

			Tree.EndCollect();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CPanFp7DeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutCollect(L"FpMapList");

	SaveMap(Tree);

	Tree.EndCollect();
	}

// Access

void CPanFp7DeviceOptions::FindMap(CAddress &Addr, DWORD dwMap)
{
	CFpMap Map;

	Map.m_Addr.m_Ref = Addr.m_Ref;

	Map.m_Map	 = dwMap;

	Map.m_Extent	 = 1;

	UINT uCount      = m_MapArray.GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CFpMap Index = m_MapArray.GetAt(u);

		if( Map == Index ) {

			if( m_fUpdate ) {

				if( Map.m_Extent != Index.m_Extent ) {

					Map.m_Extent = Index.m_Extent;

					m_MapArray.Remove(u);

					break;
					}
				}
			
			Addr.m_Ref = Index.m_Addr.m_Ref;

			return;
			}
		}

	GetNextRef(Map.m_Addr, Addr.a.m_Table, Addr.a.m_Type, Map.m_Extent);

	m_MapArray.Append(Map);

	Addr.m_Ref = Map.m_Addr.m_Ref;
	}

DWORD CPanFp7DeviceOptions::GetMap(CAddress Addr)
{
	UINT uCount  = m_MapArray.GetCount();

	for( UINT u  = 0; u < uCount; u++ ) {

		if( Addr.m_Ref == m_MapArray.GetAt(u).m_Addr.m_Ref ) {

			return m_MapArray.GetAt(u).m_Map;
			}
		}

	return NOTHING;
	}

void CPanFp7DeviceOptions::EmptyMap(void)
{
	if( !m_fUpdate ) {

		m_MapArray.Empty();

		m_UM  = 0;

		m_IN  = 0;

		m_OT  = 0;

		m_WI  = 0;

		m_WO  = 0;
		}
	}

void CPanFp7DeviceOptions::UpdateMap(CAddress Addr, INT nSize)
{
	if( !m_fUpdate ) {

		CSystemItem *pSystem = GetDatabase()->GetSystemItem();

		if( pSystem ) {

			UINT uCount  = m_MapArray.GetCount();

			for( UINT u  = 0; u < uCount; u++ ) {

				CFpMap Map = m_MapArray.GetAt(u);

				if( Addr.m_Ref == Map.m_Addr.m_Ref ) {

					if( Map.m_Extent != WORD(nSize) ) {

						m_fUpdate = TRUE;

						if( IsLastRef(Map.m_Addr, Map.m_Extent) ) {

							m_fUpdate = FALSE;

							IncrementRef(Map.m_Addr.a.m_Table, Map.m_Addr.a.m_Type, nSize);
							}

						Map.m_Extent = WORD(nSize);

						m_MapArray.Remove(u);

						m_MapArray.Append(Map);

						if( m_fUpdate ) {

							pSystem->Rebuild(1);

							m_fUpdate = FALSE;
							}
						}

					return;
					}
				}
			}
		}
	}

// Meta Data Creation

void CPanFp7DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(UM);

	Meta_AddInteger(IN);

	Meta_AddInteger(WI);

	Meta_AddInteger(OT);

	Meta_AddInteger(WO);	
	}

// Implementation
		
void CPanFp7DeviceOptions::MakeInitDataMap(CInitData &Init)
{
	UINT uCount = m_MapArray.GetCount();

	Init.AddLong(uCount);

	for( UINT u = 0; u < uCount; u++ ) {

		CFpMap Map = m_MapArray.GetAt(u);

		Init.AddLong(Map.m_Addr.m_Ref);

		Init.AddLong(Map.m_Map);

		Init.AddWord(Map.m_Extent);
		}
	}

void CPanFp7DeviceOptions::SaveMap(CTreeFile &Tree)
{
	m_MapArray.Sort();

	UINT uCount = m_MapArray.GetCount();

	for( UINT u = 0; u < uCount; u ++ ) {

		CFpMap Map = m_MapArray.GetAt(u);

		Tree.PutValue(CPrintf(L"Ref%u", u), Map.m_Addr.m_Ref);

		Tree.PutValue(CPrintf(L"Map%u", u), Map.m_Map);

		Tree.PutValue(CPrintf(L"Ext%u", u), Map.m_Extent);
		}
	}

void CPanFp7DeviceOptions::LoadMap(CTreeFile &Tree)
{
	CFpMap Entry;

	while( !Tree.IsEndOfData() ) {
		
		CString	Name = Tree.GetName();

		if( Name.Left(3) == L"Ref" ) {

			Entry.m_Addr.m_Ref = Tree.GetValueAsInteger();
			}

		if( Name.Left(3) == L"Map" ) {

			Entry.m_Map = Tree.GetValueAsInteger();
			}

		if( Name.Left(3) == L"Ext" ) {

			Entry.m_Extent = WORD(Tree.GetValueAsInteger());

			m_MapArray.Append(Entry);
			}
		}
	}

void CPanFp7DeviceOptions::GetNextRef(CAddress &Ref, UINT uTable, UINT uType, INT nSize)
{
	CAddress Addr;

	Addr.a.m_Table = uTable;

	Addr.a.m_Type  = uType;

	UINT uIndex    = 0;

	switch( uTable ) {

		case spaceUM:	uIndex = m_UM;	Increment(m_UM, uType, nSize);	break;
		case spaceIN:	uIndex = m_IN;	Increment(m_IN, uType, nSize);	break;
		case spaceOT:	uIndex = m_OT;	Increment(m_OT, uType, nSize);	break;
		case spaceWI:	uIndex = m_WI;	Increment(m_WI, uType, nSize);	break;
		case spaceWO:	uIndex = m_WO;	Increment(m_WO, uType, nSize);	break;
		}

	Addr.a.m_Offset = uIndex & 0xFFFF;

	Addr.a.m_Extra  = (uIndex & 0xF0000) >> 16;

	Ref.m_Ref       = Addr.m_Ref;
	}

void  CPanFp7DeviceOptions::Increment(UINT &uIndex, UINT uType, INT nSize)
{
	if( nSize > 1 ) {

		nSize--;
		}

	if( uType == addrWordAsLong || uType == addrWordAsReal ) {

		uIndex += (nSize * 2);

		return;
		}

	uIndex += nSize;
	}

void  CPanFp7DeviceOptions::IncrementRef(UINT uTable, UINT uType, INT nSize)
{
	switch( uTable ) {

		case spaceUM:	Increment(m_UM, uType, nSize);	break;
		case spaceIN:	Increment(m_IN, uType, nSize);	break;
		case spaceOT:	Increment(m_OT, uType, nSize);	break;
		case spaceWI:	Increment(m_WI, uType, nSize);	break;
		case spaceWO:	Increment(m_WO, uType, nSize);	break;
		}
	}

BOOL CPanFp7DeviceOptions::IsLastRef(CAddress Addr, INT nSize)
{
	UINT uIndex = 0;

	switch( Addr.a.m_Table ) {

		case spaceUM:	uIndex = m_UM;	break;
		case spaceIN:	uIndex = m_IN;	break;
		case spaceOT:	uIndex = m_OT;	break;
		case spaceWI:	uIndex = m_WI;	break;
		case spaceWO:	uIndex = m_WO;	break;
		}

	UINT uLast = Addr.a.m_Offset | (Addr.a.m_Extra << 16);

	Increment(uLast, Addr.a.m_Type, nSize);

	return (uLast == uIndex);
	}

// Helpers

void CPanFp7DeviceOptions::PrintMap(void)
{
	UINT uCount = m_MapArray.GetCount();
	
	for( UINT u = 0; u < uCount; u++ ) {

		CFpMap Map = m_MapArray[u];

		AfxTrace(L"\nMap %u ref %8.8x map %8.8x extent %u", u, Map.m_Addr.m_Ref,
								       Map.m_Map,
								       Map.m_Extent);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Serial Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CPanFp7SerialMasterDeviceOptions, CPanFp7DeviceOptions);

// Constructor

CPanFp7SerialMasterDeviceOptions::CPanFp7SerialMasterDeviceOptions(void) 
{
	m_Station = 1;
	}

// Download Support

BOOL CPanFp7SerialMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CPanFp7DeviceOptions::MakeInitData(Init);

	Init.AddWord(WORD(m_Station));

	return TRUE;
	}

// Meta Data Creation

void CPanFp7SerialMasterDeviceOptions::AddMetaData(void)
{
	CPanFp7DeviceOptions::AddMetaData();

	Meta_AddInteger(Station);
	}


//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM TCP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CPanFp7TcpMasterDeviceOptions, CPanFp7DeviceOptions);

// Constructor

CPanFp7TcpMasterDeviceOptions::CPanFp7TcpMasterDeviceOptions(void) 
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD(5, 1), MAKEWORD(168, 192)));

	m_Addr2   = 0;

	m_Socket  = 32769;

	m_Keep    = 1;

	m_Ping    = 0;

	m_Time1	  = 5000;

	m_Time2   = 2500;

	m_Time3	  = 200;
	}

// UI Managament

void CPanFp7TcpMasterDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pHost->EnableUI("Time3", !m_Keep);
			}
		}

	CPanFp7DeviceOptions::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CPanFp7TcpMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CPanFp7DeviceOptions::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));

	Init.AddLong(LONG(m_Addr2));

	Init.AddWord(WORD(m_Socket));

	Init.AddByte(BYTE(m_Keep));

	Init.AddByte(BYTE(m_Ping));

	Init.AddWord(WORD(m_Time1));

	Init.AddWord(WORD(m_Time2));

	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CPanFp7TcpMasterDeviceOptions::AddMetaData(void)
{
	CPanFp7DeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);

	Meta_AddInteger(Addr2);

	Meta_AddInteger(Socket);

	Meta_AddInteger(Keep);

	Meta_AddInteger(Ping);

	Meta_AddInteger(Time1);

	Meta_AddInteger(Time2);

	Meta_AddInteger(Time3);
	}

/////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM TCP/IP Driver
//

// Constructor

CPanFp7MasterDriver::CPanFp7MasterDriver(void)
{
	m_uType		= driverMaster;

	m_Manufacturer	= "Panasonic - Matsushita";

	AddSpaces();

	C3_PASSED();
	}

// Configuration

CLASS CPanFp7MasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CPanFp7DriverOptions);
	}

CLASS CPanFp7MasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CPanFp7DeviceOptions);
	}

// Notifications

void CPanFp7MasterDriver::NotifyInit(CItem * pConfig)
{
	CPanFp7DeviceOptions * pOpt = (CPanFp7DeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->EmptyMap();
		}
	}

void CPanFp7MasterDriver::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	if( HasSlot(Addr.a.m_Table) ) {

		CPanFp7DeviceOptions * pOpt = (CPanFp7DeviceOptions *)pConfig;

		if( pOpt ) {

			pOpt->UpdateMap(Addr, nSize);
			}
		}
	}

// Address Management

BOOL CPanFp7MasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CPanFp7AddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}
 

BOOL CPanFp7MasterDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		WORD wSlot   = FindSlot(Text, pSpace);
		
		UINT uOffset = FindOffset(Text, pSpace);
	
		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			SetMixed(uOffset, Addr);

			SetExtended(uOffset, Addr);
			
			if( VerifyOffset(Error, Addr, pSpace) ) {

				CPanFp7DeviceOptions * pOpts = (CPanFp7DeviceOptions *) pConfig;

				if( pOpts ) {

					if( wSlot ) {

						DWORD dwMap = 0;

						SetSlot(wSlot, dwMap, Addr.a.m_Table);

						SetExtended(uOffset, dwMap, Addr.a.m_Table);

						SetMixed(uOffset, Addr, dwMap);
												
						pOpts->FindMap(Addr, dwMap);

						return TRUE;
						}
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPanFp7MasterDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace * pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uOffset = GetOffset(Addr, pConfig);

		UINT uType   = Addr.a.m_Type;

		return DoExpandAddress(Text, uOffset, uType, pSpace);
		}
	
	return FALSE;
	}

BOOL CPanFp7MasterDriver::DoExpandAddress(CString &Text, UINT uValue, UINT uType, CSpace * pSpace)
{
	if( pSpace ) {

		CString Full;

		if( HasSlot(pSpace->m_uTable) ) {

			UINT uSlot = GetSlot(uValue);

			Full.Printf(L"%3.3u:", uSlot);
			}

		if( IsMixed(pSpace->m_uTable) ) {

			UINT uBit = GetMixedBit(uValue);

			UINT uDec = GetMixedDec(uValue);
		
			Full += pSpace->GetValueAsText(uDec);

			CString Hex;

			Hex.Printf(L"%1.1X", uBit);

			Full += Hex;
			}
		else {
			Full += pSpace->GetValueAsText(uValue & 0xFFFFF);
			}
				
		if( uType == pSpace->m_uType ) {

			Text.Printf( L"%s%s", 
				     pSpace->m_Prefix, 
				     Full
				     );
			}
		else {
			Text.Printf( L"%s%s.%s",
				     pSpace->m_Prefix, 
				     Full,
				     pSpace->GetTypeModifier(uType)
				     );
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPanFp7MasterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	switch( Addr.a.m_Table ) {

		case spaceS:
		case spaceT:
		case spaceC:
		case spaceP:
		case spaceE:
		case spaceIN:
		case spaceWI:
		case spaceSD:

			return TRUE;
		}

	return FALSE;
	}

// Access

BOOL CPanFp7MasterDriver::IsMixed(UINT uTable)
{
	switch( uTable ) {

		case spaceR:
		case spaceL:
		case spaceX:
		case spaceY:
		case spaceP:
		case spaceIN:
		case spaceOT:
		case spaceS:

			return TRUE;
		}
	
	return FALSE;
	}

BOOL CPanFp7MasterDriver::IsExtended(UINT uTable)
{
	switch( uTable ) {

		case spaceDT:
		case spaceUM:
		case spaceIN:
		case spaceOT:
		case spaceWI:
		case spaceWO:
				
			return TRUE;
		}

	return FALSE;
	}

BOOL CPanFp7MasterDriver::HasSlot(UINT uTable)
{
	switch( uTable ) {

		case spaceUM:
		case spaceIN:
		case spaceOT:
		case spaceWI:
		case spaceWO:
			
			return TRUE;
		}

	return FALSE;
	}

WORD CPanFp7MasterDriver::GetSlot(DWORD dwMap)
{
	return (dwMap >> 22) & 0x3FF;
	}

void CPanFp7MasterDriver::SetSlot(UINT uValue, DWORD &dwMap, UINT uTable)
{
	if( HasSlot(uTable) ) {

		DWORD dwMask = DWORD(0x3FF << 22);

		dwMap &= ~dwMask;

		dwMap |= ( uValue & 0x3FF ) << 22;
		}
	}

DWORD CPanFp7MasterDriver::GetExtended(DWORD dwMap)
{
	return (dwMap & 0xFFFFF);
	}

DWORD CPanFp7MasterDriver::GetExtended(CAddress Addr)
{
	return (Addr.a.m_Extra << 16) | Addr.a.m_Offset;
	}

void CPanFp7MasterDriver::SetExtended(UINT uValue, DWORD &dwMap, UINT uTable)
{
	if( IsExtended(uTable) ) {

		DWORD dwMask = 0xFFFFF;

		dwMap &= ~dwMask;

		dwMap |= uValue & dwMask;
		}
	}

BOOL  CPanFp7MasterDriver::SetExtended(UINT uValue, CAddress &Addr)
{
	if( IsExtended(Addr.a.m_Table) && !HasSlot(Addr.a.m_Table) ) {

		Addr.a.m_Extra = (uValue & 0xF0000) >> 16;

		return TRUE;
		}

	return FALSE;
	}

DWORD CPanFp7MasterDriver::GetMixedBit(DWORD dwMap)
{
	return (dwMap & 0xF);
	}

DWORD CPanFp7MasterDriver::GetMixedDec(DWORD dwMap)
{
	return (dwMap >> 4) & 0xFFFF;
	}

void CPanFp7MasterDriver::SetMixed(UINT uBit, CAddress Addr, DWORD &dwMap)
{
	if( IsMixed(Addr.a.m_Table) ) {

		DWORD dwMask = 0xFFFFF;

		dwMap &= ~dwMask;

		dwMap |= (Addr.a.m_Offset << 4) & 0xFFFF0;

		dwMap |= (uBit & 0xF);
		}
	}

BOOL CPanFp7MasterDriver::SetMixed(UINT uValue, CAddress &Addr)
{
	if( IsMixed(Addr.a.m_Table) && !HasSlot(Addr.a.m_Table) ) {

		Addr.a.m_Offset = (Addr.a.m_Offset << 4) | uValue;

		return TRUE;
		}

	return FALSE;
	}

UINT  CPanFp7MasterDriver::GetMinimum(CSpace * pSpace)
{
	if( pSpace ) {

		return pSpace->m_uMinimum;
		}

	return 0;
	}


UINT  CPanFp7MasterDriver::GetMaximum(CSpace * pSpace)
{
	if( pSpace ) {

		UINT uMax = pSpace->m_uMaximum;

		if( HasSlot(pSpace->m_uTable) ) {

			MakeSlotMax(uMax);
			}

		if( IsMixed(pSpace->m_uTable) ) {

			MakeMixedMax(uMax);
			}

		return uMax;
		}

	return 0;
	}

CString CPanFp7MasterDriver::GetFormat(CSpace * pSpace)
{
	if( pSpace ) {

		BOOL fSlot = HasSlot(pSpace->m_uTable);

		if( IsMixed(pSpace->m_uTable) ) {

			UINT uMax = pSpace->m_uWidth;

			CString Format = L"Mixed ";

			if( fSlot ) {

				Format += L"DDD:";
				}

			while( uMax ) {

				Format += L"D";

				uMax--;
				}

			Format += L"H";

			return Format;
			}

		return pSpace->GetRadixAsText();
		}

	return CString(L"");
	}

// Implementation

void CPanFp7MasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(spaceDT,  "DT",  "Data Register",			10, 0,  589823, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(spaceLD,  "LD",  "Link Data Register",		10, 0,   16383, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(spaceSD,  "SD",  "System Data Register",		10, 0,     255, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(spaceWX,  "WX",  "External Input Words",		10, 0,     511,	addrWordAsWord, addrWordAsReal));			
	AddSpace(New CSpace(spaceWY,  "WY",  "External Output Words",		10, 0,     511,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(spaceWR,  "WR",  "Internal Relay Words",		10, 0,    2047,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(spaceWL,  "WL",  "Link Relay Words",		10, 0,    1023,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(spaceWS,  "WS",  "System Relay Words",		10, 0,     223,	addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(spaceX,   "X",   "External Inputs",			10, 0,     511, addrBitAsBit));						
	AddSpace(New CSpace(spaceY,   "Y",   "External Outputs",		10, 0,     511, addrBitAsBit));				
	AddSpace(New CSpace(spaceR,   "R",   "Internal Relays",			10, 0,    2047, addrBitAsBit));				 
	AddSpace(New CSpace(spaceL,   "L",   "Link Relays",			10, 0,    1023, addrBitAsBit));	
	AddSpace(New CSpace(spaceS,   "S",   "System Relays",			10, 0,     223, addrBitAsBit)); 
	//AddSpace(New CSpace(spaceP,   "P",   "Pulse Relays",			10, 0,     255, addrBitAsBit));				
	
	AddSpace(New CSpace(spaceT,   "T",   "Timer Flags",		        10, 0,    4095, addrBitAsBit));				
	AddSpace(New CSpace(spaceC,   "C",   "Counter Flags",			10, 0,    1023, addrBitAsBit));				 
	AddSpace(New CSpace(spaceE,   "E",   "Error Alarm Relays",		10, 0,    4095, addrBitAsBit));	
	AddSpace(New CSpace(spaceI,   "I",   "Index Register",			16, 0,     0xE, addrLongAsLong, addrLongAsReal));	
	
	AddSpace(New CSpace(spaceTS,  "TSV", "Timer Set Value",			10, 0,    4095, addrLongAsLong, addrLongAsReal));
	AddSpace(New CSpace(spaceTE,  "TEV", "Timer Elapsed Value",		10, 0,    4095, addrLongAsLong, addrLongAsReal));
	AddSpace(New CSpace(spaceCS,  "CSV", "Counter Set Value",		10, 0,    1023, addrLongAsLong, addrLongAsReal));
	AddSpace(New CSpace(spaceCE,  "CEV", "Counter Elapsed Value",		10, 0,    1023, addrLongAsLong, addrLongAsReal));

	AddSpace(New CSpace(spaceUM,  "UM",  "Unit Memory Words",		16, 0, 0x7FFFF, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(spaceWI,  "WI",  "Direct Input Words",		10, 0,      62, addrWordAsWord));
	AddSpace(New CSpace(spaceWO,  "WO",  "Direct Output Words",		10, 0,      62, addrWordAsWord));
	//AddSpace(New CSpace(spaceIN,  "IN",  "Direct Inputs",			10, 0,      62, addrBitAsBit));										 
	//AddSpace(New CSpace(spaceOT,  "OT",  "Direct Outputs",		10, 0,      62, addrBitAsBit));		

	AddSpace(New CSpace(spaceLER, "LER", "Lastest Error Request",		10, 0,      60, addrByteAsByte));
	AddSpace(New CSpace(spaceLEC, "LEC", "Latest Error Code",		10, 0,       0, addrLongAsLong));
	}

UINT CPanFp7MasterDriver::GetOffset(CAddress  Addr, CItem *pConfig)
{
	if( HasSlot(Addr.a.m_Table) ) {
			
		CPanFp7DeviceOptions * pOpt = (CPanFp7DeviceOptions *) pConfig;

		if( pOpt ) {

			UINT uMap = pOpt->GetMap(Addr);

			if( uMap < NOTHING ) {

				return uMap;
				}
			}
		}

	if( IsExtended(Addr.a.m_Table) ) {

		return GetExtended(Addr);
		}
	
	return Addr.a.m_Offset;
	}

BOOL CPanFp7MasterDriver::VerifyOffset(CError &Error, CAddress Addr, CSpace * pSpace)
{
	if( pSpace && IsMixed(Addr.a.m_Table) ) {

		CString Value = pSpace->GetValueAsText(Addr.a.m_Offset);

		UINT uLength  = Value.GetLength();

		for( UINT u = 0; u < uLength - 1; u++ ) {

			TCHAR c = Value.GetAt(u);

			if( c >= '0' && c <= '9' || c == ':' ) {

				continue;
				}

			CString Text = CString(IDS_SELECTED_ADDRESS);

			Error.Set(Text);

			return FALSE;
			}		
		}

	return TRUE;
	}

WORD CPanFp7MasterDriver::FindSlot(CString &Text, CSpace * pSpace)
{
	WORD wSlot = 0;

	if( pSpace ) {

		UINT uFind = Text.Find(':');

		if( uFind < NOTHING ) {

			CString Slot = Text.Mid(0, uFind);
		
			if( HasSlot(pSpace->m_uTable) ) {

				wSlot = WORD(tstrtoul(Text.Mid(0, uFind), NULL, 10));
				}

			Text = Text.Mid(uFind + 1);
			}
		}
	
	return wSlot;
	}

UINT CPanFp7MasterDriver::FindOffset(CString &Text, CSpace * pSpace)
{
	if( pSpace ) {

		if( IsMixed(pSpace->m_uTable) ) {
			
			UINT  uFind = Text.Find('.');

			UINT  uChar = uFind < NOTHING ? uFind - 1 : Text.GetLength() - 1;

			TCHAR cChar = Text.GetAt(uChar);

			Text.Delete(uChar, 1);

			CString Text;

			Text.Printf("%c", cChar);
	
			return tstrtol(Text, NULL, 16);			
			}
		
		return tstrtoul(Text, NULL, pSpace->m_uRadix);
		}

	return NOTHING;
	}

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Serial Master Driver
//

// Instantiator

ICommsDriver * Create_PanFp7SerialMasterDriver(void)
{
	return New CPanFp7SerialMasterDriver;
	}

// Constructor

CPanFp7SerialMasterDriver::CPanFp7SerialMasterDriver(void)
{
	m_wID		= 0x40DB;

	m_DriverName	= "FP7 MEWTOCOL7-COM Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MEWTOCOL7-COM";
	}

// Configuration

CLASS CPanFp7SerialMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CPanFp7SerialMasterDeviceOptions);
	}

// Binding Control

UINT CPanFp7SerialMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CPanFp7SerialMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_PanFp7TcpMasterDriver(void)
{
	return New CPanFp7TcpMasterDriver;
	}

// Constructor

CPanFp7TcpMasterDriver::CPanFp7TcpMasterDriver(void)
{
	m_wID		= 0x40DA;

	m_DriverName	= "FP7 MEWTOCOL7-COM TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MEWTOCOL7-COM";
	}

// Configuration

CLASS CPanFp7TcpMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CPanFp7TcpMasterDeviceOptions);
	}

// Binding Control

UINT CPanFp7TcpMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CPanFp7TcpMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

/////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CPanFp7AddrDialog, CStdAddrDialog);
		
// Constructor

CPanFp7AddrDialog::CPanFp7AddrDialog(CPanFp7MasterDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "PanFp7ElementDlg";
	}

// Message Map

AfxMessageMap(CPanFp7AddrDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	
	AfxMessageEnd(CPanFp7AddrDialog)
	};

// Message Handlers

BOOL CPanFp7AddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	BOOL fReturn = CStdAddrDialog::OnInitDialog(Focus, dwData);

	OnSpaceChange(1001, Focus);

	return fReturn;
	}

// Notification Handlers

void CPanFp7AddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	CPanFp7MasterDriver * pDriver = (CPanFp7MasterDriver *) m_pDriver;

	BOOL fSlot = pDriver && m_pSpace ? pDriver->HasSlot(m_pSpace->m_uTable) : FALSE;

	GetDlgItem(2005).ShowWindow(fSlot);

	GetDlgItem(2006).ShowWindow(fSlot);

	GetDlgItem(2007).ShowWindow(fSlot);
	}

// Overridables

void CPanFp7AddrDialog::SetAddressText(CString Text)
{
	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		CString Slot = Text.Mid(0, uFind);

		GetDlgItem(2006).SetWindowText(Slot);

		Text = Text.Mid(uFind + 1);
		}

	CStdAddrDialog::SetAddressText(Text);
	}

CString CPanFp7AddrDialog::GetAddressText(void)
{
	if( GetDlgItem(2006).IsWindowVisible() ) {

		CString Text;
		
		Text += GetDlgItem(2006).GetWindowText();

		Text += ":";

		Text += CStdAddrDialog::GetAddressText();

		return Text;
		}

	return CStdAddrDialog::GetAddressText();
	}

void CPanFp7AddrDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CPanFp7MasterDriver * pDriver = (CPanFp7MasterDriver *) m_pDriver;
	
	if( pDriver && m_pSpace ) {

		UINT uType  = GetTypeCode();

		UINT uMin   = pDriver->GetMinimum(m_pSpace);

		UINT uMax   = pDriver->GetMaximum(m_pSpace);

		CString Min;
		
		CString Max;
		
		pDriver->DoExpandAddress(Min, uMin, uType, m_pSpace);

		pDriver->DoExpandAddress(Max, uMax, uType, m_pSpace);

		GetDlgItem(3004).SetWindowText(Min);

		GetDlgItem(3006).SetWindowText(Max);

		GetDlgItem(3008).SetWindowTextW(pDriver->GetFormat(m_pSpace));
		}
	}

// End of File
