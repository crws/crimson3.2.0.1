
#include "intern.hpp"

#include "YaskawS7.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 Inverters Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaSeries7Driver(void)
{
	return New CYaskawaSeries7Driver;
	}

// Constructor

CYaskawaSeries7Driver::CYaskawaSeries7Driver(void)
{
	m_wID		= 0x4026;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "Series 7 Inverters";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Yaskawa Series 7";

	AddSpaces();  
	}

// Binding Control

UINT CYaskawaSeries7Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CYaskawaSeries7Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Helpers

BOOL CYaskawaSeries7Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type  = StripType(pSpace, Text);

	UINT uAddress = tatoi(Text);

	UINT uFind    = Text.Find('-');

	if( uFind < NOTHING ) {

		uAddress  = tatoi(Text.Left(uFind)) * 100;

		uAddress += tatoi(Text.Mid(uFind + 1));
		}
	
	CString s;

	if( uAddress < pSpace->m_uMinimum || uAddress > pSpace->m_uMaximum ) {

		s.Printf( "%s%1.1d-%2.2d...%s%1.1d-%2.2d",
			pSpace->m_Prefix,
			pSpace->m_uMinimum / 100,
			pSpace->m_uMinimum % 100,
			pSpace->m_Prefix,
			pSpace->m_uMaximum / 100,
			pSpace->m_uMaximum % 100
			);

		Error.Set( s,
			0
			);

		return FALSE;
		}

	s = ValidateSelection(pSpace->m_uTable, uAddress);

	if( s.GetLength() > 1 ) {

		Error.Set( s,
			0
			);

		return FALSE;
		}

	Addr.a.m_Type	= pSpace->TypeFromModifier(Type);

	Addr.a.m_Table	= pSpace->m_uTable;
	
	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uAddress;

	return TRUE;
	}

BOOL CYaskawaSeries7Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		switch( Addr.a.m_Table ) {

			case SPDA:
			case SPEN:
			case SPAC:
			case SPEC:
			case SPEV:
				return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		CString Type = Addr.a.m_Type == pSpace->m_uType ? "" : "." + pSpace->GetTypeModifier(Addr.a.m_Type);

		Text.Printf( "%s%1.1d-%2.2d%s",
			pSpace->m_Prefix,
			Addr.a.m_Offset / 100,
			Addr.a.m_Offset % 100,
			Type
			);
		
		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CYaskawaSeries7Driver::AddSpaces(void)
{
	AddSpace(New CSpace(SPA, "A",  "A Parameters",		10, 100, 232,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPB, "B",  "B Parameters",		10, 101, 902,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPC, "C",  "C Parameters",		10, 101, 605,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPD, "D",  "D Parameters",		10, 101, 606,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPE, "E",  "E Parameters",		10, 101, 407,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPF, "F",  "F Parameters",		10, 101, 606,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPH, "H",  "H Parameters",		10, 101, 607,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPL, "L",  "L Parameters",		10, 101, 818,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPN, "N",  "N Parameters",		10, 101, 304,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPO, "O",  "O Parameters",		10, 101, 302,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPP, "P",  "P Parameters",		10, 101, 310,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPT, "T",  "T Parameters",		10, 100, 108,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPU, "U",  "U Parameters",		10, 101, 320,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPEN,"EN", "ENTER Command",		10, 0,  0,	addrBitAsBit));
	AddSpace(New CSpace(SPAC,"AC", "ACCEPT Command",	10, 0,  0,	addrBitAsBit));
	AddSpace(New CSpace(SPDA,"DA", "Direct Address",	10, 0, 65535,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPEC,"EC", "Latest Error Command",	10, 0, 0,	addrWordAsWord));
	AddSpace(New CSpace(SPEV,"EV", "Latest Error Value",	10, 0, 0,	addrWordAsWord));
	}

CString CYaskawaSeries7Driver::ValidateSelection(UINT uTable, UINT uAddress )
{
	UINT l[10];
	
	UINT    c = 1;

	char    p = 'A';

	CString s = "";

	switch( uTable ) {

		case SPA:
			l[c++]	= 105;
			l[c++]	= 232;
			break;

		case SPB:
			p	= 'B';
			l[c++]	= 108;
			l[c++]	= 208;
			l[c++]	= 314;
			l[c++]	= 402;
			l[c++]	= 519;
			l[c++]	= 604;
			l[c++]	= 702;
			l[c++]	= 806;
			l[c++]	= 902;
			break;

		case SPC:
			p	= 'C';
			l[c++]	= 111;
			l[c++]	= 204;
			l[c++]	= 305;
			l[c++]	= 405;
			l[c++]	= 508;
			l[c++]	= 605;
			break;

		case SPD:
			p	= 'D';
			l[c++]	= 117;
			l[c++]	= 203;
			l[c++]	= 304;
			l[c++]	= 402;
			l[c++]	= 506;
			l[c++]	= 606;
			break;

		case SPE:
			p	= 'E';
			l[c++]	= 113;
			l[c++]	= 212;
			l[c++]	= 308;
			break;

		case SPF:
			p	= 'F';
			l[c++]	= 114;
			l[c++]	= 201;
			l[c++]	= 301;
			l[c++]	= 408;
			l[c++]	= 509;
			l[c++]	= 606;
			break;

		case SPH:
			p	= 'H';
			l[c++]	= 106;
			l[c++]	= 205;
			l[c++]	= 312;
			l[c++]	= 408;
			l[c++]	= 507;
			l[c++]	= 607;
			break;

		case SPL:
			p	= 'L';
			l[c++]	= 105;
			l[c++]	= 208;
			l[c++]	= 312;
			l[c++]	= 406;
			l[c++]	= 502;
			l[c++]	= 606;
			l[c++]	= 707;
			l[c++]	= 818;
			break;

		case SPN:
			p	= 'N';
			l[c++]	= 102;
			l[c++]	= 203;
			l[c++]	= 304;
			break;

		case SPO:
			p	= 'O';
			l[c++]	= 105;
			l[c++]	= 214;
			l[c++]	= 302;
			break;

		case SPP:
			p	= 'P';
			l[c++]	= 110;
			l[c++]	= 210;
			l[c++]	= 310;
			break;

		case SPT:
			p	= 'T';
			l[c++]	= 108;
			break;

		case SPU:
			if( uAddress > 145 && uAddress < 190 ) return "U101...U145\nU190...U199";
			p	= 'U';
			l[c++]	= 199;
			l[c++]	= 214;
			l[c++]	= 320;
			break;

		default:
			return "";
		}

	for( UINT i = 1; i < c; i++ ) {

		UINT uPrevLowerLimit = 100 * i;

		UINT uNextLowerLimit = uPrevLowerLimit + 101;

		if( uTable != SPA && uTable != SPT ) {
		
			uPrevLowerLimit++;
			}

		if( uAddress > l[i] && uAddress < uNextLowerLimit ) {

			s.Printf( "%c%1.1d-%2.2d...%c%1.1d-%2.2d\n%c%1.1d-%2.2d...%c%1.1d-%2.2d",
				  p,
				  uPrevLowerLimit / 100,
				  uPrevLowerLimit % 100,
				  p,
				  l[i] / 100,
				  l[i] % 100,
				  p,
				  uNextLowerLimit / 100,
				  uNextLowerLimit % 100,
				  p,
				  l[i+1] / 100,
				  l[i+1] % 100
				  );

			return s;
			}
		}

	return s + TCHAR(p);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaSeries7TCPDeviceOptions, CUIItem);

// Constructor

CYaskawaSeries7TCPDeviceOptions::CYaskawaSeries7TCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(20, 1), MAKEWORD(168, 192) ));

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CYaskawaSeries7TCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CYaskawaSeries7TCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddWord(WORD(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaSeries7TCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_YaskawaSeries7TCPDriver(void)
{
	return New CYaskawaSeries7TCPDriver;
	}

// Constructor

CYaskawaSeries7TCPDriver::CYaskawaSeries7TCPDriver(void)
{
	m_wID		= 0x351C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "TCP/IP Series 7";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Yaskawa Series 7 TCP/IP";
	}

// Binding Control

UINT CYaskawaSeries7TCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYaskawaSeries7TCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYaskawaSeries7TCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CYaskawaSeries7TCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaSeries7TCPDeviceOptions);
	}

// End of File
