
#include "intern.hpp"

#include "eiepower.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 2009-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm/Invensys EPower TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEuroInvensysEPowerTCPDeviceOptions, CUIItem);       

// Constructor

CEuroInvensysEPowerTCPDeviceOptions::CEuroInvensysEPowerTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CEuroInvensysEPowerTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CEuroInvensysEPowerTCPDeviceOptions::MakeInitData(CInitData &Init)
{	
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CEuroInvensysEPowerTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Comms Driver
//

// Constructor

CEuroInvensysEPowerDriver::CEuroInvensysEPowerDriver(void)
{
	AddSpaces();

	InitEIE();
	}

// Destructor

CEuroInvensysEPowerDriver::~CEuroInvensysEPowerDriver(void)
{
	DeleteAllSpaces();
	}

// Address Management

BOOL CEuroInvensysEPowerDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CEuroInvensysEPowerAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL  CEuroInvensysEPowerDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	if( !(IsNotStdModbus(pSpace->m_uTable)) ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	Addr.a.m_Table	= pSpace->m_uTable;
	Addr.a.m_Extra	= 0;
	Addr.a.m_Type	= pSpace->m_uType;

	UINT uOffset	= tatoi(Text);

	if( uOffset > pSpace->m_uMaximum ) {

		uOffset = 0;
		}

	LoadItemStrings(Addr.a.m_Table, TRUE, uOffset);

	CString s	= Get_sName();

	// cppcheck-suppress redundantAssignment

	s		= Text.Right(1);

	Addr.a.m_Type	= s[0] == 'R' ? RR : LL;

	Addr.a.m_Offset	= uOffset;

	return TRUE;
	}

BOOL  CEuroInvensysEPowerDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		if( !IsNotStdModbus(Addr.a.m_Table ) ) {

			return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		UINT uTable	= Addr.a.m_Table;
		UINT uOffset	= Addr.a.m_Offset;

		m_pEIE->uTable	= uTable;

		LoadItemInfo(uTable);

		m_pEIE->uNPos	= uOffset % m_pEIE->uQty;

		m_pEIE->uBlock	= (uOffset / m_pEIE->uQty) + 1;

		LoadItemStrings(uTable, TRUE, uOffset);

		CString sName	= Get_sName();

		sName		= sName.Left(sName.GetLength() - 1);

		Text.Printf(TEXT("%s%d_%s"),

			pSpace->m_Prefix,
			uOffset,
			sName
			);

		return TRUE;
		}

	return FALSE;
	}

EIESTRINGS * CEuroInvensysEPowerDriver::GetEIEPtr(void)
{
	return m_pEIE;
	}

BOOL CEuroInvensysEPowerDriver::IsNotStdModbus(UINT uTable)
{
	return uTable >= SP_ACC;
	}

void CEuroInvensysEPowerDriver::InitItemInfo(UINT uTable)
{
	LoadItemInfo(uTable);

	LoadItemStrings(uTable, TRUE, m_pEIE->uNPos);
	}

void CEuroInvensysEPowerDriver::LoadItemInfo(UINT uTable)
{
	switch( uTable ) {

		case SP_ACC: 	LoadACCInfo();	break;
		case SP_COM: 	LoadCOMInfo();	break;
		case SP_CON: 	LoadCONInfo();	break;
		case SP_COU: 	LoadCOUInfo();	break;
		case SP_CUS: 	LoadCUSInfo();	break;
		case SP_CST: 	LoadCSTInfo();	break;
		case SP_ENE: 	LoadENEInfo();	break;
		case SP_EVE: 	LoadEVEInfo();	break;
		case SP_EVS: 	LoadEVSInfo();	break;
		case SP_FAU: 	LoadFAUInfo();	break;
		case SP_FIR: 	LoadFIRInfo();	break;
		case SP_INS: 	LoadINSInfo();	break;
		case SP_IOI: 	LoadIOIInfo();	break;
		case SP_IOO: 	LoadIOOInfo();	break;
		case SP_IOD: 	LoadIODInfo();	break;
		case SP_IOR: 	LoadIORInfo();	break;
		case SP_IPM: 	LoadIPMInfo();	break;
		case SP_LG2: 	LoadLG2Info();	break;
		case SP_LG8: 	LoadLG8Info();	break;
		case SP_LTC: 	LoadLTCInfo();	break;
		case SP_MAT: 	LoadMATInfo();	break;
		case SP_MOD: 	LoadMODInfo();	break;
		case SP_AAC: 	LoadAACInfo();	break;
		case SP_ADE: 	LoadADEInfo();	break;
		case SP_ADI: 	LoadADIInfo();	break;
		case SP_ALA: 	LoadALAInfo();	break;
		case SP_ASI: 	LoadASIInfo();	break;
		case SP_AST: 	LoadASTInfo();	break;
		case SP_MEA: 	LoadMEAInfo();	break;
		case SP_NSU: 	LoadNSUInfo();	break;
		case SP_PLM: 	LoadPLMInfo();	break;
		case SP_PLC: 	LoadPLCInfo();	break;
		case SP_QST: 	LoadQSTInfo();	break;
		case SP_SET: 	LoadSETInfo();	break;
		case SP_TIM: 	LoadTIMInfo();	break;
		case SP_TOT: 	LoadTOTInfo();	break;
		case SP_USR: 	LoadUSRInfo();	break;
		}
	}

CString CEuroInvensysEPowerDriver::LoadItemStrings(UINT uTable, BOOL fStore, UINT uSelect)
{
	CString s = SNOTU;

	switch( uTable ) {

		case SP_ACC: 	s = LoadACC(uSelect);	break;
		case SP_COM: 	s = LoadCOM(uSelect);	break;
		case SP_CON: 	s = LoadCON(uSelect);	break;
		case SP_COU: 	s = LoadCOU(uSelect);	break;
		case SP_CUS: 	s = LoadCUS(uSelect);	break;
		case SP_CST: 	s = LoadCST(uSelect);	break;
		case SP_ENE: 	s = LoadENE(uSelect);	break;
		case SP_EVE: 	s = LoadEVE(uSelect);	break;
		case SP_EVS: 	s = LoadEVS(uSelect);	break;
		case SP_FAU: 	s = LoadFAU(uSelect);	break;
		case SP_FIR: 	s = LoadFIR(uSelect);	break;
		case SP_INS: 	s = LoadINS(uSelect);	break;
		case SP_IOI: 	s = LoadIOI(uSelect);	break;
		case SP_IOO: 	s = LoadIOO(uSelect);	break;
		case SP_IOD: 	s = LoadIOD(uSelect);	break;
		case SP_IOR: 	s = LoadIOR(uSelect);	break;
		case SP_IPM: 	s = LoadIPM(uSelect);	break;
		case SP_LG2: 	s = LoadLG2(uSelect);	break;
		case SP_LG8: 	s = LoadLG8(uSelect);	break;
		case SP_LTC: 	s = LoadLTC(uSelect);	break;
		case SP_MAT: 	s = LoadMAT(uSelect);	break;
		case SP_MOD: 	s = LoadMOD(uSelect);	break;
		case SP_AAC: 	s = LoadAAC(uSelect);	break;
		case SP_ADE: 	s = LoadADE(uSelect);	break;
		case SP_ADI: 	s = LoadADI(uSelect);	break;
		case SP_ALA: 	s = LoadALA(uSelect);	break;
		case SP_ASI: 	s = LoadASI(uSelect);	break;
		case SP_AST: 	s = LoadAST(uSelect);	break;
		case SP_MEA: 	s = LoadMEA(uSelect);	break;
		case SP_NSU: 	s = LoadNSU(uSelect);	break;
		case SP_PLM: 	s = LoadPLM(uSelect);	break;
		case SP_PLC: 	s = LoadPLC(uSelect);	break;
		case SP_QST: 	s = LoadQST(uSelect);	break;
		case SP_SET: 	s = LoadSET(uSelect);	break;
		case SP_TIM: 	s = LoadTIM(uSelect);	break;
		case SP_TOT: 	s = LoadTOT(uSelect);	break;
		case SP_USR: 	s = LoadUSR(uSelect);	break;
		}

	return ParseParamString(s, fStore);
	}

BOOL CEuroInvensysEPowerDriver::HasBlocks(void)
{
	return m_pEIE->uBlkCt > 1;
	}

CString CEuroInvensysEPowerDriver::Get_sName(void)
{
	return m_sName.Left(m_sName.Find('@'));
	}

void CEuroInvensysEPowerDriver::Set_sName(CString sName)
{
	m_sName = sName;
	}

UINT CEuroInvensysEPowerDriver::GetAddrFromName(void)
{
	return tatoi(m_sName.Mid(m_sName.Find('@') + 1));
	}

// Implementation

void  CEuroInvensysEPowerDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SP_ACC,	"ACC",	"Access",			10,     0,     6, LL));
	AddSpace(New CSpace(SP_COM,	"COM",	"Comms",			10,     0,    39, LL));
	AddSpace(New CSpace(SP_CON,	"CON",	"Control",			10,     0,   159, LL, RR));
	AddSpace(New CSpace(SP_COU,	"COU",	"Counter",			10,     0,    35, LL));
	AddSpace(New CSpace(SP_CUS,	"CUS",	"Customer Page",		10,     0,    31, LL));
	AddSpace(New CSpace(SP_CST,	"CST",	"Customer Page Strings",	10,     0,    15, LL));
	AddSpace(New CSpace(SP_ENE,	"ENE",	"Energy",			10,     0,    69, LL, RR));
	AddSpace(New CSpace(SP_EVE,	"EVE",	"Event Log",			10,     0,    79, LL));
	AddSpace(New CSpace(SP_EVS,	"EVS",	"Event Log Status",		10,     0,     0, LL));
	AddSpace(New CSpace(SP_FAU,	"FAU",	"Fault Detection",		10,     0,     7, LL));
	AddSpace(New CSpace(SP_FIR,	"FIR",	"Firing O/P",			10,     0,    43, LL, RR));
	AddSpace(New CSpace(SP_INS,	"INS",	"Instrument",			10,     0,    16, LL, RR));
	AddSpace(New CSpace(SP_IOI,	"IOI",	"Analog IP IO",			10,     0,    24, LL, RR));
	AddSpace(New CSpace(SP_IOO,	"IOO",	"Analog OP IO",			10,     0,    43, LL, RR));
	AddSpace(New CSpace(SP_IOD,	"IOD",	"Digital IO",			10,     0,    31, LL));
	AddSpace(New CSpace(SP_IOR,	"IOR",	"Relay IO",			10,     0,     7, LL));
	AddSpace(New CSpace(SP_IPM,	"IPM",	"IP Monitor",			10,     0,    43, LL, RR));
	AddSpace(New CSpace(SP_LG2,	"L2G",	"LGC2",				10,     0,    31, LL, RR));
	AddSpace(New CSpace(SP_LG8,	"L8G",	"LGC8",				10,     0,    51, LL));
	AddSpace(New CSpace(SP_LTC,	"LTC",	"LTC",				10,     0,    25, LL, RR));
	AddSpace(New CSpace(SP_MAT,	"MAT",	"Maths2",			10,     0,    55, LL, RR));
	AddSpace(New CSpace(SP_MOD,	"MOD",	"Modulator",			10,     0,    31, LL, RR));
	AddSpace(New CSpace(SP_AAC,	"AAC",	"Network AlmAck",		10,     0,    59, LL));
	AddSpace(New CSpace(SP_ADE,	"ADE",	"Network AlmDet",		10,     0,    59, LL));
	AddSpace(New CSpace(SP_ADI,	"ADI",	"Network AlmDis",		10,     0,    59, LL));
	AddSpace(New CSpace(SP_ALA,	"ALA",	"Network AlmLat",		10,     0,    59, LL));
	AddSpace(New CSpace(SP_ASI,	"ASI",	"Network AlmSig",		10,     0,    59, LL));
	AddSpace(New CSpace(SP_AST,	"AST",	"Network AlmStop",		10,     0,    59, LL));
	AddSpace(New CSpace(SP_MEA,	"MEA",	"Network Meas",			10,     0,   123, RR));
	AddSpace(New CSpace(SP_NSU,	"NSU",	"Network Setup",		10,     0,   107, LL, RR));
	AddSpace(New CSpace(SP_PLM,	"PLM",	"Predictive Load Manager",	10,     0,    22, LL, RR));
	AddSpace(New CSpace(SP_PLC,	"PLC",	"PLM Channels",			10,     0,    19, LL, RR));
	AddSpace(New CSpace(SP_QST,	"QST",	"Quick Start",			10,     0,    13, LL));
	AddSpace(New CSpace(SP_SET,	"SET",	"SetProv",			10,     0,    51, LL, RR));
	AddSpace(New CSpace(SP_TIM,	"TIM",	"Timer",			10,     0,    23, LL));
	AddSpace(New CSpace(SP_TOT,	"TOT",	"Totaliser",			10,     0,    35, LL, RR));
	AddSpace(New CSpace(SP_USR,	"USR",	"User Value",			10,     0,    23, LL, RR));
	AddSpace(New CSpace(SP_4,	"4",	"Holding Registers",		10,     0, 39999, WW, WR));
	}

CString CEuroInvensysEPowerDriver::ParseParamString(CString sParamString, BOOL fStore)
{
	UINT uFind    = sParamString.Find('@');

	CString sName = sParamString.Left(uFind);	// Name portion
	CString sAddr = sParamString.Mid(uFind + 1);	// Initial Address portion

	if( HasBlocks() ) {

		UINT uFind1 = sName.Find('_') + 1;	// loop # within function name

		UINT uFind2 = sName.FindRev('_');	// format is <func1>[func2]_1_<par>...

		UINT uBlock = m_pEIE->uBlock;		// desired loop number

		CString Lt  = sName.Left(uFind1);	// keep "<basename>" and "."
		CString Rt  = sName.Mid(uFind2);	// keep "_" and "<param>"

		sName.Printf(L"%s%d%s", Lt, uBlock, Rt);
		}

	sName.Printf(L"%s@%s", sName, ParseAddr(sAddr, fStore));

	if( fStore ) {

		m_sName = sName;
		}

	return sName;
	}

CString CEuroInvensysEPowerDriver::ParseAddr(CString sAddr, BOOL fStore)
{
	UINT uAddr = tatoi(sAddr);

	if( HasBlocks() ) {

		UINT uInc = 0;

		ParseModifiers(sAddr, &uInc);

		UINT uBlock = m_pEIE->uBlock - 1;

		if( uInc ) {

			uAddr += uBlock * uInc;
			}
		}

	if( fStore ) {

		m_pEIE->uAddr = uAddr;
		}

	sAddr.Printf("%d", uAddr);

	return sAddr;
	}

void CEuroInvensysEPowerDriver::ParseModifiers(CString sParamString, UINT *pInc)
{
	UINT uFind = sParamString.Find('#');

	if( uFind < NOTHING ) {

		*pInc = tatoi(sParamString.Mid(uFind + 1));

		return;
		}

	*pInc = 0;
	}

// Helpers

// Other Help
void CEuroInvensysEPowerDriver::InitEIE(void)
{
	m_pEIE	= &EIESTR;

	m_pEIE->uBlock	= 1;
	m_pEIE->uNPos	= 0;
	m_pEIE->uBPos	= 0;
	m_pEIE->uOffset	= 0;
	m_pEIE->uAddr	= SACC1A;
	m_sName		= SACC1N;

	LoadACCInfo();
	}

// String Loading
void CEuroInvensysEPowerDriver::LoadACCInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 7;
	pE->uTable	= SP_ACC;
	}

void CEuroInvensysEPowerDriver::LoadCOMInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 40;
	pE->uTable	= SP_COM;
	}

void CEuroInvensysEPowerDriver::LoadCONInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 40;
	pE->uTable	= SP_CON;
	}

void CEuroInvensysEPowerDriver::LoadCOUInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 9;
	pE->uTable	= SP_COU;
	}

void CEuroInvensysEPowerDriver::LoadCUSInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 8;
	pE->uTable	= SP_CUS;
	}

void CEuroInvensysEPowerDriver::LoadCSTInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 4;
	pE->uTable	= SP_CST;
	}

void CEuroInvensysEPowerDriver::LoadENEInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 5;
	pE->uQty	= 14;
	pE->uTable	= SP_ENE;
	}

void CEuroInvensysEPowerDriver::LoadEVEInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 40;
	pE->uQty	= 2;
	pE->uTable	= SP_EVE;
	}

void CEuroInvensysEPowerDriver::LoadEVSInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 1;
	pE->uTable	= SP_EVS;
	}

void CEuroInvensysEPowerDriver::LoadFAUInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 8;
	pE->uTable	= SP_FAU;
	}

void CEuroInvensysEPowerDriver::LoadFIRInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 11;
	pE->uTable	= SP_FIR;
	}

void CEuroInvensysEPowerDriver::LoadINSInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 17;
	pE->uTable	= SP_INS;
	}

void CEuroInvensysEPowerDriver::LoadIOIInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 5;
	pE->uQty	= 5;
	pE->uTable	= SP_IOI;
	}

void CEuroInvensysEPowerDriver::LoadIOOInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 11;
	pE->uTable	= SP_IOO;
	}

void CEuroInvensysEPowerDriver::LoadIODInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 8;
	pE->uQty	= 4;
	pE->uTable	= SP_IOD;
	}

void CEuroInvensysEPowerDriver::LoadIORInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 2;
	pE->uTable	= SP_IOR;
	}

void CEuroInvensysEPowerDriver::LoadIPMInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 11;
	pE->uTable	= SP_IPM;
	}

void CEuroInvensysEPowerDriver::LoadLG2Info(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 8;
	pE->uTable	= SP_LG2;
	}

void CEuroInvensysEPowerDriver::LoadLG8Info(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 13;
	pE->uTable	= SP_LG8;
	}

void CEuroInvensysEPowerDriver::LoadLTCInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 26;
	pE->uTable	= SP_LTC;
	}

void CEuroInvensysEPowerDriver::LoadMATInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 14;
	pE->uTable	= SP_MAT;
	}

void CEuroInvensysEPowerDriver::LoadMODInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 8;
	pE->uTable	= SP_MOD;
	}

void CEuroInvensysEPowerDriver::LoadAACInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 15;
	pE->uTable	= SP_AAC;
	}

void CEuroInvensysEPowerDriver::LoadADEInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 15;
	pE->uTable	= SP_ADE;
	}

void CEuroInvensysEPowerDriver::LoadADIInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 15;
	pE->uTable	= SP_ADI;
	}

void CEuroInvensysEPowerDriver::LoadALAInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 15;
	pE->uTable	= SP_ALA;
	}

void CEuroInvensysEPowerDriver::LoadASIInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 15;
	pE->uTable	= SP_ASI;
	}

void CEuroInvensysEPowerDriver::LoadASTInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 15;
	pE->uTable	= SP_AST;
	}

void CEuroInvensysEPowerDriver::LoadMEAInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 31;
	pE->uTable	= SP_MEA;
	}

void CEuroInvensysEPowerDriver::LoadNSUInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 27;
	pE->uTable	= SP_NSU;
	}

void CEuroInvensysEPowerDriver::LoadPLMInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 23;
	pE->uTable	= SP_PLM;
	}

void CEuroInvensysEPowerDriver::LoadPLCInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 5;
	pE->uTable	= SP_PLC;
	}

void CEuroInvensysEPowerDriver::LoadQSTInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 14;
	pE->uTable	= SP_QST;
	}

void CEuroInvensysEPowerDriver::LoadSETInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 13;
	pE->uTable	= SP_SET;
	}

void CEuroInvensysEPowerDriver::LoadTIMInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 6;
	pE->uTable	= SP_TIM;
	}

void CEuroInvensysEPowerDriver::LoadTOTInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 9;
	pE->uTable	= SP_TOT;
	}

void CEuroInvensysEPowerDriver::LoadUSRInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 6;
	pE->uTable	= SP_USR;
	}

// String Loading
CString CEuroInvensysEPowerDriver::LoadACC(UINT uSel)
{
	switch( uSel ) {

		case 0: return SACC0;
		case 1: return SACC1;
		case 2: return SACC2;
		case 3: return SACC3;
		case 4: return SACC4;
		case 5: return SACC5;
		case 6: return SACC6;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadCOM(UINT uSel)
{
	switch( uSel ) {

		case  0:	return SCOM0;
		case  1:	return SCOM1;
		case  2:	return SCOM2;
		case  3:	return SCOM3;
		case  4:	return SCOM4;
		case  5:	return SCOM5;
		case  6:	return SCOM6;
		case  7:	return SCOM7;
		case  8:	return SCOM8;
		case  9:	return SCOM9;
		case 10:	return SCOM10;
		case 11:	return SCOM11;
		case 12:	return SCOM12;
		case 13:	return SCOM13;
		case 14:	return SCOM14;
		case 15:	return SCOM15;
		case 16:	return SCOM16;
		case 17:	return SCOM17;
		case 18:	return SCOM18;
		case 19:	return SCOM19;
		case 20:	return SCOM20;
		case 21:	return SCOM21;
		case 22:	return SCOM22;
		case 23:	return SCOM23;
		case 24:	return SCOM24;
		case 25:	return SCOM25;
		case 26:	return SCOM26;
		case 27:	return SCOM27;
		case 28:	return SCOM28;
		case 29:	return SCOM29;
		case 30:	return SCOM30;
		case 31:	return SCOM31;
		case 32:	return SCOM32;
		case 33:	return SCOM33;
		case 34:	return SCOM34;
		case 35:	return SCOM35;
		case 36:	return SCOM36;
		case 37:	return SCOM37;
		case 38:	return SCOM38;
		case 39:	return SCOM39;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadCON(UINT uSel)
{
	switch( uSel % 40 ) {

		case  0:	return SCON0;
		case  1:	return SCON1;
		case  2:	return SCON2;
		case  3:	return SCON3;
		case  4:	return SCON4;
		case  5:	return SCON5;
		case  6:	return SCON6;
		case  7:	return SCON7;
		case  8:	return SCON8;
		case  9:	return SCON9;
		case 10:	return SCON10;
		case 11:	return SCON11;
		case 12:	return SCON12;
		case 13:	return SCON13;
		case 14:	return SCON14;
		case 15:	return SCON15;
		case 16:	return SCON16;
		case 17:	return SCON17;
		case 18:	return SCON18;
		case 19:	return SCON19;
		case 20:	return SCON20;
		case 21:	return SCON21;
		case 22:	return SCON22;
		case 23:	return SCON23;
		case 24:	return SCON24;
		case 25:	return SCON25;
		case 26:	return SCON26;
		case 27:	return SCON27;
		case 28:	return SCON28;
		case 29:	return SCON29;
		case 30:	return SCON30;
		case 31:	return SCON31;
		case 32:	return SCON32;
		case 33:	return SCON33;
		case 34:	return SCON34;
		case 35:	return SCON35;
		case 36:	return SCON36;
		case 37:	return SCON37;
		case 38:	return SCON38;
		case 39:	return SCON39;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadCOU(UINT uSel)
{
	switch( uSel % 9 ) {

		case 0:	return SCOU0;
		case 1:	return SCOU1;
		case 2:	return SCOU2;
		case 3:	return SCOU3;
		case 4:	return SCOU4;
		case 5:	return SCOU5;
		case 6:	return SCOU6;
		case 7:	return SCOU7;
		case 8:	return SCOU8;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadCUS(UINT uSel)
{
	switch( uSel % 8 ) {

		case 0:	return SCUS0;
		case 1:	return SCUS1;
		case 2:	return SCUS2;
		case 3:	return SCUS3;
		case 4:	return SCUS4;
		case 5:	return SCUS5;
		case 6:	return SCUS6;
		case 7:	return SCUS7;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadCST(UINT uSel)
{
	switch( uSel % 4 ) {

		case 0:	return SCST0;
		case 1:	return SCST1;
		case 2:	return SCST2;
		case 3:	return SCST3;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadENE(UINT uSel)
{
	switch( uSel % 14 ) {

		case  0:	return SENE0;
		case  1:	return SENE1;
		case  2:	return SENE2;
		case  3:	return SENE3;
		case  4:	return SENE4;
		case  5:	return SENE5;
		case  6:	return SENE6;
		case  7:	return SENE7;
		case  8:	return SENE8;
		case  9:	return SENE9;
		case 10:	return SENE10;
		case 11:	return SENE11;
		case 12:	return SENE12;
		case 13:	return SENE13;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadEVE(UINT uSel)
{
	switch( uSel % 2 ) {

		case 0:	return SEVE0;
		case 1:	return SEVE1;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadEVS(UINT uSel)
{
	return SEVS0;
	}

CString CEuroInvensysEPowerDriver::LoadFAU(UINT uSel)
{
	switch( uSel ) {

		case 0:	return SFAU0;
		case 1:	return SFAU1;
		case 2:	return SFAU2;
		case 3:	return SFAU3;
		case 4:	return SFAU4;
		case 5:	return SFAU5;
		case 6:	return SFAU6;
		case 7:	return SFAU7;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadFIR(UINT uSel)
{
	switch( uSel % 11 ) {

		case  0:	return SFIR0;
		case  1:	return SFIR1;
		case  2:	return SFIR2;
		case  3:	return SFIR3;
		case  4:	return SFIR4;
		case  5:	return SFIR5;
		case  6:	return SFIR6;
		case  7:	return SFIR7;
		case  8:	return SFIR8;
		case  9:	return SFIR9;
		case 10:	return SFIR10;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadINS(UINT uSel)
{
	switch( uSel ) {

		case  0:	return SINS0;
		case  1:	return SINS1;
		case  2:	return SINS2;
		case  3:	return SINS3;
		case  4:	return SINS4;
		case  5:	return SINS5;
		case  6:	return SINS6;
		case  7:	return SINS7;
		case  8:	return SINS8;
		case  9:	return SINS9;
		case 10:	return SINS10;
		case 11:	return SINS11;
		case 12:	return SINS12;
		case 13:	return SINS13;
		case 14:	return SINS14;
		case 15:	return SINS15;
		case 16:	return SINS16;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadIOI(UINT uSel)
{
	switch( uSel % 5 ) {

		case 0:	return SIOI0;
		case 1:	return SIOI1;
		case 2:	return SIOI2;
		case 3:	return SIOI3;
		case 4:	return SIOI4;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadIOO(UINT uSel)
{
	switch( uSel % 11 ) {

		case  0:	return SIOO0;
		case  1:	return SIOO1;
		case  2:	return SIOO2;
		case  3:	return SIOO3;
		case  4:	return SIOO4;
		case  5:	return SIOO5;
		case  6:	return SIOO6;
		case  7:	return SIOO7;
		case  8:	return SIOO8;
		case  9:	return SIOO9;
		case 10:	return SIOO10;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadIOD(UINT uSel)
{
	switch( uSel % 4 ) {

		case  0:	return SIOD0;
		case  1:	return SIOD1;
		case  2:	return SIOD2;
		case  3:	return SIOD3;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadIOR(UINT uSel)
{
	return (uSel & 1) ? SIOR1 :SIOR0;
	}

CString CEuroInvensysEPowerDriver::LoadIPM(UINT uSel)
{
	switch( uSel % 11) {

		case  0:	return SIPM0;
		case  1:	return SIPM1;
		case  2:	return SIPM2;
		case  3:	return SIPM3;
		case  4:	return SIPM4;
		case  5:	return SIPM5;
		case  6:	return SIPM6;
		case  7:	return SIPM7;
		case  8:	return SIPM8;
		case  9:	return SIPM9;
		case 10:	return SIPM10;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadLG2(UINT uSel)
{
	switch( uSel % 8) {

		case 0:	return SL2G0;
		case 1:	return SL2G1;
		case 2:	return SL2G2;
		case 3:	return SL2G3;
		case 4:	return SL2G4;
		case 5:	return SL2G5;
		case 6:	return SL2G6;
		case 7:	return SL2G7;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadLG8(UINT uSel)
{
	switch( uSel % 13 ) {

		case  0:	return SL8G0;
		case  1:	return SL8G1;
		case  2:	return SL8G2;
		case  3:	return SL8G3;
		case  4:	return SL8G4;
		case  5:	return SL8G5;
		case  6:	return SL8G6;
		case  7:	return SL8G7;
		case  8:	return SL8G8;
		case  9:	return SL8G9;
		case 10:	return SL8G10;
		case 11:	return SL8G11;
		case 12:	return SL8G12;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadLTC(UINT uSel)
{
	switch( uSel ) {

		case  0:	return SLTC0;
		case  1:	return SLTC1;
		case  2:	return SLTC2;
		case  3:	return SLTC3;
		case  4:	return SLTC4;
		case  5:	return SLTC5;
		case  6:	return SLTC6;
		case  7:	return SLTC7;
		case  8:	return SLTC8;
		case  9:	return SLTC9;
		case 10:	return SLTC10;
		case 11:	return SLTC11;
		case 12:	return SLTC12;
		case 13:	return SLTC13;
		case 14:	return SLTC14;
		case 15:	return SLTC15;
		case 16:	return SLTC16;
		case 17:	return SLTC17;
		case 18:	return SLTC18;
		case 19:	return SLTC19;
		case 20:	return SLTC20;
		case 21:	return SLTC21;
		case 22:	return SLTC22;
		case 23:	return SLTC23;
		case 24:	return SLTC24;
		case 25:	return SLTC25;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadMAT(UINT uSel)
{
	switch( uSel % 14 ) {

		case  0:	return SMAT0;
		case  1:	return SMAT1;
		case  2:	return SMAT2;
		case  3:	return SMAT3;
		case  4:	return SMAT4;
		case  5:	return SMAT5;
		case  6:	return SMAT6;
		case  7:	return SMAT7;
		case  8:	return SMAT8;
		case  9:	return SMAT9;
		case 10:	return SMAT10;
		case 11:	return SMAT11;
		case 12:	return SMAT12;
		case 13:	return SMAT13;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadMOD(UINT uSel)
{
	switch( uSel % 8 ) {

		case 0:	return SMOD0;
		case 1:	return SMOD1;
		case 2:	return SMOD2;
		case 3:	return SMOD3;
		case 4:	return SMOD4;
		case 5:	return SMOD5;
		case 6:	return SMOD6;
		case 7:	return SMOD7;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadAAC(UINT uSel)
{
	switch( uSel % 15 ) {

		case  0:	return SAAC0;
		case  1:	return SAAC1;
		case  2:	return SAAC2;
		case  3:	return SAAC3;
		case  4:	return SAAC4;
		case  5:	return SAAC5;
		case  6:	return SAAC6;
		case  7:	return SAAC7;
		case  8:	return SAAC8;
		case  9:	return SAAC9;
		case 10:	return SAAC10;
		case 11:	return SAAC11;
		case 12:	return SAAC12;
		case 13:	return SAAC13;
		case 14:	return SAAC14;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadADE(UINT uSel)
{
	switch( uSel % 15 ) {

		case  0:	return SADE0;
		case  1:	return SADE1;
		case  2:	return SADE2;
		case  3:	return SADE3;
		case  4:	return SADE4;
		case  5:	return SADE5;
		case  6:	return SADE6;
		case  7:	return SADE7;
		case  8:	return SADE8;
		case  9:	return SADE9;
		case 10:	return SADE10;
		case 11:	return SADE11;
		case 12:	return SADE12;
		case 13:	return SADE13;
		case 14:	return SADE14;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadADI(UINT uSel)
{
	switch( uSel % 15 ) {

		case  0:	return SADI0;
		case  1:	return SADI1;
		case  2:	return SADI2;
		case  3:	return SADI3;
		case  4:	return SADI4;
		case  5:	return SADI5;
		case  6:	return SADI6;
		case  7:	return SADI7;
		case  8:	return SADI8;
		case  9:	return SADI9;
		case 10:	return SADI10;
		case 11:	return SADI11;
		case 12:	return SADI12;
		case 13:	return SADI13;
		case 14:	return SADI14;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadALA(UINT uSel)
{
	switch( uSel % 15 ) {

		case  0:	return SALA0;
		case  1:	return SALA1;
		case  2:	return SALA2;
		case  3:	return SALA3;
		case  4:	return SALA4;
		case  5:	return SALA5;
		case  6:	return SALA6;
		case  7:	return SALA7;
		case  8:	return SALA8;
		case  9:	return SALA9;
		case 10:	return SALA10;
		case 11:	return SALA11;
		case 12:	return SALA12;
		case 13:	return SALA13;
		case 14:	return SALA14;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadASI(UINT uSel)
{
	switch( uSel % 15 ) {

		case  0:	return SASI0;
		case  1:	return SASI1;
		case  2:	return SASI2;
		case  3:	return SASI3;
		case  4:	return SASI4;
		case  5:	return SASI5;
		case  6:	return SASI6;
		case  7:	return SASI7;
		case  8:	return SASI8;
		case  9:	return SASI9;
		case 10:	return SASI10;
		case 11:	return SASI11;
		case 12:	return SASI12;
		case 13:	return SASI13;
		case 14:	return SASI14;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadAST(UINT uSel)
{
	switch( uSel % 15 ) {

		case  0:	return SAST0;
		case  1:	return SAST1;
		case  2:	return SAST2;
		case  3:	return SAST3;
		case  4:	return SAST4;
		case  5:	return SAST5;
		case  6:	return SAST6;
		case  7:	return SAST7;
		case  8:	return SAST8;
		case  9:	return SAST9;
		case 10:	return SAST10;
		case 11:	return SAST11;
		case 12:	return SAST12;
		case 13:	return SAST13;
		case 14:	return SAST14;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadMEA(UINT uSel)
{
	switch( uSel % 31 ) {

		case  0:	return SMEA0;
		case  1:	return SMEA1;
		case  2:	return SMEA2;
		case  3:	return SMEA3;
		case  4:	return SMEA4;
		case  5:	return SMEA5;
		case  6:	return SMEA6;
		case  7:	return SMEA7;
		case  8:	return SMEA8;
		case  9:	return SMEA9;
		case 10:	return SMEA10;
		case 11:	return SMEA11;
		case 12:	return SMEA12;
		case 13:	return SMEA13;
		case 14:	return SMEA14;
		case 15:	return SMEA15;
		case 16:	return SMEA16;
		case 17:	return SMEA17;
		case 18:	return SMEA18;
		case 19:	return SMEA19;
		case 20:	return SMEA20;
		case 21:	return SMEA21;
		case 22:	return SMEA22;
		case 23:	return SMEA23;
		case 24:	return SMEA24;
		case 25:	return SMEA25;
		case 26:	return SMEA26;
		case 27:	return SMEA27;
		case 28:	return SMEA28;
		case 29:	return SMEA29;
		case 30:	return SMEA30;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadNSU(UINT uSel)
{
	switch( uSel % 27 ) {

		case  0:	return SNSU0;
		case  1:	return SNSU1;
		case  2:	return SNSU2;
		case  3:	return SNSU3;
		case  4:	return SNSU4;
		case  5:	return SNSU5;
		case  6:	return SNSU6;
		case  7:	return SNSU7;
		case  8:	return SNSU8;
		case  9:	return SNSU9;
		case 10:	return SNSU10;
		case 11:	return SNSU11;
		case 12:	return SNSU12;
		case 13:	return SNSU13;
		case 14:	return SNSU14;
		case 15:	return SNSU15;
		case 16:	return SNSU16;
		case 17:	return SNSU17;
		case 18:	return SNSU18;
		case 19:	return SNSU19;
		case 20:	return SNSU20;
		case 21:	return SNSU21;
		case 22:	return SNSU22;
		case 23:	return SNSU23;
		case 24:	return SNSU24;
		case 25:	return SNSU25;
		case 26:	return SNSU26;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadPLM(UINT uSel)
{
	switch( uSel ) {

		case  0:	return SPLM0;
		case  1:	return SPLM1;
		case  2:	return SPLM2;
		case  3:	return SPLM3;
		case  4:	return SPLM4;
		case  5:	return SPLM5;
		case  6:	return SPLM6;
		case  7:	return SPLM7;
		case  8:	return SPLM8;
		case  9:	return SPLM9;
		case 10:	return SPLM10;
		case 11:	return SPLM11;
		case 12:	return SPLM12;
		case 13:	return SPLM13;
		case 14:	return SPLM14;
		case 15:	return SPLM15;
		case 16:	return SPLM16;
		case 17:	return SPLM17;
		case 18:	return SPLM18;
		case 19:	return SPLM19;
		case 20:	return SPLM20;
		case 21:	return SPLM21;
		case 22:	return SPLM22;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadPLC(UINT uSel)
{
	switch( uSel % 5 ) {

		case 0:	return SPLC0;
		case 1:	return SPLC1;
		case 2:	return SPLC2;
		case 3:	return SPLC3;
		case 4:	return SPLC4;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadQST(UINT uSel)
{
	switch( uSel ) {

		case  0:	return SQST0;
		case  1:	return SQST1;
		case  2:	return SQST2;
		case  3:	return SQST3;
		case  4:	return SQST4;
		case  5:	return SQST5;
		case  6:	return SQST6;
		case  7:	return SQST7;
		case  8:	return SQST8;
		case  9:	return SQST9;
		case 10:	return SQST10;
		case 11:	return SQST11;
		case 12:	return SQST12;
		case 13:	return SQST13;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadSET(UINT uSel)
{
	switch( uSel % 13 ) {

		case  0:	return SSET0;
		case  1:	return SSET1;
		case  2:	return SSET2;
		case  3:	return SSET3;
		case  4:	return SSET4;
		case  5:	return SSET5;
		case  6:	return SSET6;
		case  7:	return SSET7;
		case  8:	return SSET8;
		case  9:	return SSET9;
		case 10:	return SSET10;
		case 11:	return SSET11;
		case 12:	return SSET12;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadTIM(UINT uSel)
{
	switch( uSel % 6 ) {

		case 0:	return STIM0;
		case 1:	return STIM1;
		case 2:	return STIM2;
		case 3:	return STIM3;
		case 4:	return STIM4;
		case 5:	return STIM5;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadTOT(UINT uSel)
{
	switch( uSel % 9 ) {

		case 0:	return STOT0;
		case 1:	return STOT1;
		case 2:	return STOT2;
		case 3:	return STOT3;
		case 4:	return STOT4;
		case 5:	return STOT5;
		case 6:	return STOT6;
		case 7:	return STOT7;
		case 8:	return STOT8;
		}

	return SNOTU;
	}

CString CEuroInvensysEPowerDriver::LoadUSR(UINT uSel)
{
	switch( uSel % 6 ) {

		case 0:	return SUSR0;
		case 1:	return SUSR1;
		case 2:	return SUSR2;
		case 3:	return SUSR3;
		case 4:	return SUSR4;
		case 5:	return SUSR5;
		}

	return SNOTU;
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Serial Comms Driver
//

// Instantiator

ICommsDriver *	Create_EuroInvensysEPowerSerialDriver(void)
{
	return New CEuroInvensysEPowerSerialDriver;
	}

// Destructor

CEuroInvensysEPowerSerialDriver::~CEuroInvensysEPowerSerialDriver(void)
{
	}

CEuroInvensysEPowerSerialDriver::CEuroInvensysEPowerSerialDriver(void)
{
	m_wID		= 0x4088;

	m_uType		= driverMaster;

	m_Manufacturer	= "Eurotherm";

	m_DriverName	= "EPower Serial";

	m_Version	= "1.00";

	m_ShortName	= "Eurotherm EPower";
	}

// Binding Control

UINT  CEuroInvensysEPowerSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void  CEuroInvensysEPowerSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys TCP Comms Driver
//

// Instantiator

ICommsDriver *	Create_EuroInvensysEPowerTCPDriver(void)
{
	return New CEuroInvensysEPowerTCPDriver;
	}

CEuroInvensysEPowerTCPDriver::CEuroInvensysEPowerTCPDriver(void)
{
	m_wID		= 0x353E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Eurotherm";
	
	m_DriverName	= "EPower TCP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Eurotherm EPower";
	}

// Destructor

CEuroInvensysEPowerTCPDriver::~CEuroInvensysEPowerTCPDriver(void)
{
	}

// Binding Control

UINT CEuroInvensysEPowerTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEuroInvensysEPowerTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CEuroInvensysEPowerTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CEuroInvensysEPowerTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEuroInvensysEPowerTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEuroInvensysEPowerAddrDialog, CStdAddrDialog);
		
// Constructor

CEuroInvensysEPowerAddrDialog::CEuroInvensysEPowerAddrDialog(CEuroInvensysEPowerDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pSpace  = NULL;

	m_pGDrv = &Driver;

	m_pEIE	= m_pGDrv->GetEIEPtr();

	SetName(TEXT("CEuroInvensysEPowerAddressDlg"));
	}

// Destructor
CEuroInvensysEPowerAddrDialog::~CEuroInvensysEPowerAddrDialog(void)
{
	}

// Message Map

AfxMessageMap(CEuroInvensysEPowerAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001,  LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(DNAME, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(DNUMB, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(4001,  LBN_SELCHANGE,	OnTypeChange )

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CEuroInvensysEPowerAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	LoadList();

	if( m_pSpace ) {

		UINT uTable = Addr.m_Ref ? Addr.a.m_Table : m_pSpace->m_uTable;

		BOOL f = m_pGDrv->IsNotStdModbus(uTable);

		GetDlgItem(DNAME).SetWindowText(TEXT(""));
		GetDlgItem(DNUMB).SetWindowText(TEXT(""));

		GetDlgItem(DNAME).EnableWindow(f);
		GetDlgItem(DNUMB).EnableWindow(f);

		ClearSelData();
		InitSelects();

		if( f ) {
			if( Addr.m_Ref ) {

				CString Text;

				m_pGDrv->InitItemInfo(uTable);

				LoadType();

				if( m_pGDrv->DoExpandAddress(Text, NULL, Addr) ) {

					GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

					InitItemInfo(Addr.a.m_Table, FALSE);

					LoadType();

					return FALSE;
					}
				}

			OnSpaceChange(1001, Focus);

			return FALSE;
			}
	
		if( !m_fPart ) {

			if( m_pSpace ) {

				ShowAddress(Addr);

				ShowType(Addr.a.m_Type);

				SetAddressFocus();

				return FALSE;
				}

			return TRUE;
			}

		else {
			LoadType();

			ShowAddress(Addr);

			ShowType(Addr.a.m_Type);

			if( TRUE ) {

				CString Text = m_pSpace->m_Prefix;

				Text += GetAddressText();

				Text += GetTypeText();

				CError   Error(FALSE);

				CAddress Addr;
			
				if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

					CAddress Addr;

					m_pSpace->GetMinimum(Addr);

					ShowAddress(Addr);
					}
				}

			SetAddressFocus();

			return FALSE;
			}
		}

	ClearSelData();

	return FALSE;
	}

// Notification Handlers

void CEuroInvensysEPowerAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CEuroInvensysEPowerAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		BOOL f = FALSE;

		LoadType();

		if( m_pSpace != pSpace ) {

			f = m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable);

			if( !f ) {

				LoadType();

				GetDlgItem(DMODB).SetWindowText(TEXT(""));

				m_pSpace      = NULL;

				CStdAddrDialog::OnSpaceChange(uID, Wnd);

				GetDlgItem(3001).SetWindowText(L"Type:");

				CStdAddrDialog::ShowDetails();
				}
			}

		if( f ) {

			ClearAddress();
			ClearDetails();
			ClearSelData();
			InitSelects();

			InitItemInfo(m_pSpace->m_uTable, TRUE);

			GetParamData(GetBoxPosition(DNAME));

			LoadType();
			}

		GetDlgItem(DNAME).EnableWindow(f);
		GetDlgItem(DNUMB).EnableWindow(f);
		}

	else {
		ClearAddress();
		ClearDetails();
		ClearSelData();
		InitSelects();

		m_pSpace = NULL;
		}
	}

void CEuroInvensysEPowerAddrDialog::OnComboChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		if( m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable) ) {

			m_pEIE->uNPos	= GetBoxPosition(DNAME);

			m_pEIE->uBPos	= GetBoxPosition(DNUMB);

			m_pEIE->uBlock	= m_pEIE->uBPos + 1;

			m_pGDrv->InitItemInfo(m_pEIE->uTable);

			InitItemInfo(m_pEIE->uTable, FALSE);

			LoadType();

			SetDlgFocus(uID);
			}

		else SetDlgFocus(DPADD);
		}

	else SetDlgFocus(1001);
	}

void CEuroInvensysEPowerAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		if( !(m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable)) ) {

			CStdAddrDialog::OnTypeChange(uID, Wnd);

			return;
			}

		ShowEPowerDetails();

		ShowType(GetTypeCode());
		}
	}

void CEuroInvensysEPowerAddrDialog::LoadType(void)
{
	if( !m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable) ) {

		CStdAddrDialog::LoadType();

		return;
		}

	CListBox &ListBox = (CListBox &) GetDlgItem(4001);
	
	ListBox.SetRedraw(FALSE);

	ListBox.ResetContent();

	UINT uFind = m_pSpace->m_uType;

	char c = GetItemType();

	switch( c ) {

		case 'R':
			ListBox.AddString(m_pSpace->GetTypeAsText(RR), RR);
			uFind = RR;
			break;

		default:
			ListBox.AddString(m_pSpace->GetTypeAsText(LL), LL);
			uFind = LL;
			break;
		}

	m_pSpace->m_uType = uFind;

	ListBox.SelectData(DWORD(uFind));

	ListBox.EnableWindow(TRUE);

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnTypeChange(4001, ListBox);
	}

// Command Handlers
BOOL CEuroInvensysEPowerAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		BOOL f = m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable);

		if( f ) {

			CString s;

			s.Printf(TEXT("%d_%s"),

				m_pEIE->uOffset,
				m_pGDrv->Get_sName()
				);

			Text += s;
 			}

		else {
			Text += GetAddressText();

			Text += GetTypeText();
			}

		CError   Error(TRUE);

		CAddress Addr;

		if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			if( f ) {

				Addr.a.m_Table	= m_pSpace->m_uTable;
				Addr.a.m_Offset	= m_pEIE->uAddr;
				Addr.a.m_Type	= m_pSpace->m_uType;
				Addr.a.m_Extra	= 1;
				}

			else {
				Addr.a.m_Table	= SP_4;
				Addr.a.m_Offset	= 1;
				Addr.a.m_Type	= m_pSpace->m_uType;
				Addr.a.m_Extra	= 0;
				}
			}

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overrides
BOOL CEuroInvensysEPowerAddrDialog::AllowType(UINT uType)
{
	if( m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable) ) {

		char c = GetItemType();

		switch( c ) {

			case 'B':
			case 'Y':
			case 'W':
			case 'L':
				return uType == LL;
			}

		return uType == RR;
		}

	return uType >= WW && uType <= WR;
	}

// Selecton Handling
void CEuroInvensysEPowerAddrDialog::InitItemInfo(UINT uTable, BOOL fNew)
{
	ClearSelData();

	if( fNew ) {

		m_pEIE->uTable	= uTable;

		m_pEIE->uBlock	= 1;

		m_pEIE->uNPos	= 0;

		m_pEIE->uBPos	= 0;

		m_pGDrv->InitItemInfo(uTable);
		}

	m_pEIE->uNPos %= m_pEIE->uQty;

	LoadNames();

	SetBlockNumbers();

	GetParamData(m_pEIE->uNPos); 

	GetDlgItem(DNUMB).EnableWindow(TRUE);

	SetItemData();

	SetBoxPosition(DNAME, m_pEIE->uNPos);

	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
	}

void CEuroInvensysEPowerAddrDialog::LoadNames(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DNAME);

	ClearBox(DNAME);

	for( UINT i = 0; i < m_pEIE->uQty; i++ ) {

		CString s = m_pGDrv->LoadItemStrings(m_pEIE->uTable, FALSE, i);

		Box.AddString(StripAddr(s));
		}
	}

void CEuroInvensysEPowerAddrDialog::SetItemData(void)
{
	SetBoxPosition(DNUMB, m_pEIE->uBlock - 1);

	SetItemAddr();

	ShowEPowerDetails();
	}

void CEuroInvensysEPowerAddrDialog::SetBoxPosition(UINT uID, UINT uPos)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CEuroInvensysEPowerAddrDialog::GetBoxPosition(UINT uID)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	return Box.GetCurSel();
	}

void CEuroInvensysEPowerAddrDialog::SetItemAddr(void)
{
	m_pEIE->uAddr	= m_pGDrv->GetAddrFromName();

	CString s;

	s.Printf( TEXT("%d"), m_pEIE->uAddr);

	if( m_pGDrv->IsNotStdModbus(m_pEIE->uTable) && m_pEIE->uAddr < 16384 ) {

		char c = GetItemType();

		if( c == 'R' || c == 'L' ) {

			s.Printf( TEXT("(%d * 2) + 0x8000"), m_pEIE->uAddr );
			}
		}

	GetDlgItem(DMODB).SetWindowText(s);

	m_pEIE->uOffset = m_pEIE->uNPos + (m_pEIE->uQty * (m_pEIE->uBlock - 1));

	s.Printf( "%d", m_pEIE->uOffset);

	GetDlgItem(DPADD).SetWindowText(s);
	}

void CEuroInvensysEPowerAddrDialog::SetBlockNumbers(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DNUMB);

	ClearBox(DNUMB);

	UINT uBlkCt = m_pEIE->uBlkCt;

	for( UINT i = 1; i <= uBlkCt; i++ ) {

		CString sCount;

		sCount.Printf(TEXT("%d"), i);

		Box.AddString(sCount);
		}

	GetDlgItem(DNUMB).EnableWindow(TRUE);

	Box.SetCurSel(m_pEIE->uBlock - 1);
	}

void CEuroInvensysEPowerAddrDialog::GetParamData(UINT uPos)
{
	UINT uBlock = 1;

	UINT uSelect = GetBoxPosition(DNUMB);

	if( m_pEIE->uBlkCt > 1 ) {

		uBlock = uSelect + 1;
		}

	m_pEIE->uBlock	= uBlock;
	m_pEIE->uAddr	= m_pGDrv->GetAddrFromName();
	m_pEIE->uBPos	= uSelect;
	m_pEIE->uOffset	= uPos;
	m_pEIE->uNPos	= uPos;
	}

// Helpers
void CEuroInvensysEPowerAddrDialog::InitSelects(void)
{
	m_pEIE->uNPos = 0;
	m_pEIE->uBPos = 0;
	}

void CEuroInvensysEPowerAddrDialog::ClearSelData(void)
{
	GetDlgItem(DPADD).SetWindowText(SNONE);
	GetDlgItem(DMODB).SetWindowText(SNONE);

	ClearBox(DNAME);
	ClearBox(DNUMB);
	}

void CEuroInvensysEPowerAddrDialog::ClearBox(CComboBox & ccb)
{
	UINT uCount = ccb.GetCount();

	for( UINT i = 0; i < uCount; i++ ) {

		ccb.DeleteString(0);
		}
	}

void CEuroInvensysEPowerAddrDialog::ClearBox(UINT uID)
{
	ClearBox((CComboBox &)GetDlgItem(uID));
	}

void CEuroInvensysEPowerAddrDialog::ShowEPowerDetails(void)
{
	UINT uCurrBlk	= m_pEIE->uBlock;
	UINT uCurrOff	= m_pEIE->uOffset;
	UINT uTable	= m_pEIE->uTable;

	m_pEIE->uBlock	= 1;

	CString s = m_pGDrv->LoadItemStrings(uTable, FALSE, m_pSpace->m_uMinimum);

	GetDlgItem(3004).SetWindowText(StripAddr(s));

	m_pEIE->uBlock = m_pEIE->uBlkCt;
	
	s = m_pGDrv->LoadItemStrings(uTable, FALSE, m_pSpace->m_uMaximum);

	GetDlgItem(3006).SetWindowText(StripAddr(s));

	GetDlgItem(3008).SetWindowText(L"Decimal");

	GetDlgItem(3001).SetWindowText(L"EPower Type:");

	char c = GetItemType();

	s = L"Long";

	switch( c ) {

		case 'B': s = L"Boolean";	break;
		case 'Y': s = L"Byte";		break;
		case 'W': s = L"Word";		break;
		case 'R': s = L"Real";		break;
		}

	s.Printf(L"%s", s);

	GetDlgItem(3002).SetWindowText(s);

	m_pEIE->uBlock = uCurrBlk;

	m_pGDrv->LoadItemInfo(uTable);
	m_pGDrv->LoadItemStrings(uTable, TRUE, uCurrOff);
	}

CString CEuroInvensysEPowerAddrDialog::StripAddr(CString sSelect)
{
	return sSelect.Left(sSelect.Find('@') - 1);
	}

char CEuroInvensysEPowerAddrDialog::GetItemType(void)
{
	CString s = m_pGDrv->Get_sName();

	s = s.Right(1);

	return (char)s[0];
	}

// End of File
