
#include "intern.hpp"

#include "kollm600.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen600 Driver
//

// Instantiator

ICommsDriver *	Create_Kollmorgen600Driver(void)
{
	return New CKollmorgen600Driver;
	}

// Constructor

CKollmorgen600Driver::CKollmorgen600Driver(void)
{
	m_wID		= 0x3359;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Kollmorgen";
	
	m_DriverName	= "AC Drives";
	
	m_Version	= "1.3";
	
	m_ShortName	= "Kollmorgen AC Drives";

	AddSpaces();
	}

// Binding Control

UINT	CKollmorgen600Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CKollmorgen600Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

// Address Management

BOOL CKollmorgen600Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	if( fPart && Addr.a.m_Table >= HTMIN && Addr.a.m_Offset == 0 ) return FALSE;
	
	CKollmorgen600AddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CKollmorgen600Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) {

		return FALSE;
		}

	if( pSpace->m_uTable == addrNamed ) {

		Addr.a.m_Offset = pSpace->m_uMinimum;
		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = pSpace->m_uType;

		return TRUE;
		}

	return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
	}

BOOL CKollmorgen600Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( IsStringItem(Addr.a.m_Table ) || Addr.a.m_Table == addrNamed ){

		Text.Printf( "%s", pSpace->m_Prefix );
		return TRUE;
		}

	return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
	}

// Implementation	

void CKollmorgen600Driver::AddSpaces(void)
{
	AddSpace(New CSpace(HAL,	"",	"A COMMANDS...",			10,	0,	ABA, LL));
	AddSpace(New CSpace(AN,		"ACC",	"   Acceleration",			10, ACC,	NVE, WW));
	AddSpace(New CSpace(AN,		"ACCR",	"   Acceleration - Home/Jog",		10, ACCR,	NPO, WW));
	AddSpace(New CSpace(AN,		"ACCU",	"   Type of acceleration command",	10, ACCUNIT,	NAM, LL));
	AddSpace(New CSpace(AN,		"ACTF",	"   Active Fault Mode",			10, ACTFAULT,	NAM, BB));
	AddSpace(New CSpace(AN,		"ACTI",	"   Output stage active/inhibited",	10, ACTIVE,	NDS, BB));
	AddSpace(New CSpace(AN,		"ACTR",	"   Activate RS232 Watchdog",		10, ACTRS232,	NCM, BB));
	AddSpace(New CSpace(AN,		"ADDR",	"   Multidrop Address",			10, ADDR,	NBS, BB));
//FB	AddSpace(New CSpace(AN,		"ADDF",	"   Fieldbus Addr - Drive 400 Slave",	10, ADDRFB,	NCM, WW));
	AddSpace(New CSpace(AN,		"AENA",	"   Auto-Enable",			10, AENA,	NBS, BB));
	AddSpace(New CSpace(ALIAS,	"ALIA",	"   Drive Name String",			10, 0,		1,   LL));
	AddSpace(New CSpace(AN,		"A10X",	"   Additional Torque Tuning Value",	10, AN10TX,	NAI, WW));
	AddSpace(New CSpace(AN,		"A11N",	"   Max. # of changeable INxTRIG",	10, AN11NR,	NAI, BB));
	AddSpace(New CSpace(AN,		"A11R",	"   Range - analog change of INxTRIG",	10, AN11RANGE,	NAI, LL));
	AddSpace(New CSpace(ANTRIG,	"ANTR",	"   Analog Output Scaling",		10,	1,	8,   LL));
	AddSpace(New CSpace(AN,		"ANCN",	"   Analog Input Configuration",	10, ANCNFG,	NAI, BB));
	AddSpace(New CSpace(AN,		"ANDB",	"   Dead Band - Analog Velocity Input",	10, ANDB,	NAI, RR));
	AddSpace(New CSpace(ANIN,	"ANIN",	"   Analog Input Voltage",		10,	1,	8,   LL));
	AddSpace(New CSpace(ANOFF,	"AOFF",	"   Analog Input Offset",		10,	1,	8,   WW));
	AddSpace(New CSpace(ANOUT,	"AOUT",	"   Analog Output Configuration",	10,	1,	8,   BB));
	AddSpace(New CSpace(ANZERO,	"AZER",	"   Analog Input Zero",			10,	1,	8,   BT));
	AddSpace(New CSpace(AN,		"AHOM",	"   Automatic Homing",			10, AUTOHOME,	NPO, BB));
	AddSpace(New CSpace(AVZ,	"AVZ",	"   Analog Out Filter Time Constant",	10,	1,	8,   RR));

	AddSpace(New CSpace(HAL,	"",	"C COMMANDS...",			10,	0,	ABC, LL));
//	AddSpace(New CSpace(AN,		"CALC",	"   Calculate Resolver Parameters",	10, CALCRK,	NFB, BT));
//FB	AddSpace(New CSpace(AN,		"CBAU",	"   CAN Bus Baud Rate",			10, CBAUD,	NBS, WW));
	AddSpace(New CSpace(AN,		"CLRF",	"   Clear Drive Fault",			10, CLRFAULT,	NAM, BT));
	AddSpace(New CSpace(AN,		"CLRH",	"   Bit 5 of STAT is cleared",		10, CLRHR,	NDS, BT));
	AddSpace(New CSpace(AN,		"CLRO",	"   Delete a Motion Task",		10, CLRORDER,	NPO, WW));
	AddSpace(New CSpace(AN,		"CLRW",	"   Warning Mode",			10, CLRWARN,	NDS, BB));
	AddSpace(New CSpace(AN,		"COLD",	"   Drive Reset",			10, COLDSTART,	NDS, BT));

	AddSpace(New CSpace(HAL,	"",	"D COMMANDS...",			10,	0,	ABD, LL));
	AddSpace(New CSpace(DAOFFSET,	"DOFF",	"   Analog Output Offset",		10,	1,	8,   WW));
	AddSpace(New CSpace(AN,		"DEC",	"   Deceleration",			10, DEC,	NVE, WW));
	AddSpace(New CSpace(AN,		"DECD",	"   Deceleration - Disable Output",	10, DECDIS,	NVE, WW));
	AddSpace(New CSpace(AN,		"DECR",	"   Deceleration Ramp - Home/Jog",	10, DECR,	NPO, WW));
	AddSpace(New CSpace(AN,		"DECS",	"   Fast Stop Ramp",			10, DECSTOP,	NVE, WW));
	AddSpace(New CSpace(DEVICE,	"DVCE",	"   Device ID Numeric Data String",	10,	0,	12,  LL));
	AddSpace(New CSpace(AN,		"DICO",	"   Continuous Current",		10, DICONT,	NAM, RR));
	AddSpace(New CSpace(AN,		"DIPE",	"   Peak Rated Current",		10, DIPEAK,	NAM, RR));
	AddSpace(New CSpace(AN,		"DIR",	"   Count Direction",			10, DIR,	NVE, BB));
	AddSpace(New CSpace(AN,		"DIS",	"   Disable",				10, DIS,	NAM, BT));
	AddSpace(New CSpace(AN,		"DREF",	"   Homing Direction",			10, DREF,	NPO, BB));
	AddSpace(New CSpace(AN,		"DRVS",	"   Internal Status",			10, DRVSTAT,	NDS, LL));

	AddSpace(New CSpace(HAL,	"",	"E COMMANDS...",			10,	0,	ABE, LL));
	AddSpace(New CSpace(AN,		"EN",	"   Enable",				10, EN,		NAM, BT));
	AddSpace(New CSpace(AN,		"ENCI",	"   Encoder Pulse Input",		10, ENCIN,	NGR, LL));
	AddSpace(New CSpace(AN,		"ENCL",	"   SinCos Encoder Resolution",		10, ENCLINES,	NMT, WW));
	AddSpace(New CSpace(AN,		"ENCM",	"   Encoder Emulation",			10, ENCMODE,	NPO, BB));
	AddSpace(New CSpace(AN,		"ENCO",	"   Encoder Emulation Resolution",	10, ENCOUT,	NPO, LL));
	AddSpace(New CSpace(AN,		"ENZE",	"   Zero Pulse Offset",			10, ENZERO,	NPO, WW));
	AddSpace(New CSpace(ERRCODE,	"ECOD",	"   Fault Message String (79 Chars.)",	10,	0,	19,  LL));
	AddSpace(New CSpace(AN,		"ECOA",	"   Output Error Register",		10, ERRCODEA,	NDS, LL));
	AddSpace(New CSpace(AN,		"EXTM",	"   External Encoder Multiplier",	10, EXTMUL,	NPO, WW));
	AddSpace(New CSpace(AN,		"EXTW",	"   External Fieldbus Watchdog",	10, EXTWD,	NCM, LL));
	AddSpace(New CSpace(ERSP,	"ERSP",	"   Response Error String (79 Chars.)",	10,	0,	19,  LL));

	AddSpace(New CSpace(HAL,	"",	"F COMMANDS...",			10,	0,	ABF, LL));
	AddSpace(New CSpace(AN,		"FBTY",	"   Encoder/Resolver Selection",	10, FBTYPE,	NFB, BB));
	AddSpace(New CSpace(AN,		"FLSH",	"   Send Data to External Flash",	10, FLASH,	NCM, BB));
	AddSpace(New CSpace(FLTCNT,	"FLTC",	"   Fault Frequency",			10,	0,	32,  LL));
	AddSpace(New CSpace(FLTHIST,	"FLTH",	"   Fault History",			10,	0,	19,  LL));
	AddSpace(New CSpace(AN,		"FLUX",	"   Rated Flux",			10, FLUXM,	NMT, RR));

	AddSpace(New CSpace(HAL,	"",	"G COMMANDS...",			10,	0,	ABG, LL));
	AddSpace(New CSpace(AN,		"GRI",	"   Gearing Input Factor",		10, GEARI,	NGR, WW));
	AddSpace(New CSpace(AN,		"GRMO",	"   Secondary Position Source",		10, GEARMODE,	NGR, BB));
	AddSpace(New CSpace(AN,		"GRO",	"   Gearing Output Factor",		10, GEARO,	NGR, WW));
	AddSpace(New CSpace(AN,		"GP",	"   Proportional Gain - Position Loop",	10, GP,		NPO, RR));
	AddSpace(New CSpace(AN,		"GPFB",	"   Feed Forward - Actual Current",	10, GPFBT,	NPO, RR));
	AddSpace(New CSpace(AN,		"GPFF",	"   Feed Forward - Velocity",		10, GPFFV,	NPO, RR));
	AddSpace(New CSpace(AN,		"GPTN",	"   Integral - Position Loop",		10, GPTN,	NPO, RR));
	AddSpace(New CSpace(AN,		"GPV",	"   Prop Gain - Velocity Controller",	10, GPV,	NPO, RR));
	AddSpace(New CSpace(AN,		"GV",	"   Prop Gain - Velocity Loop",		10, GV,		NVE, RR));
	AddSpace(New CSpace(AN,		"GVFB",	"   First Order TC - Velocity Loop",	10, GVFBT,	NVE, RR));
	AddSpace(New CSpace(AN,		"GVFI",	"   % Output Filtered - Velocity Loop",	10, GVFILT,	NVE, BB));
	AddSpace(New CSpace(AN,		"GVFR",	"   Feed Forward - Actual Velocity",	10, GVFR,	NVE, RR));
	AddSpace(New CSpace(AN,		"GVT2",	"   Second TC - Velocity Loop",		10, GVT2,	NVE, RR));
	AddSpace(New CSpace(AN,		"GVTN",	"   Integral - Velocity Loop",		10, GVTN,	NVE, RR));

	AddSpace(New CSpace(HAL,	"",	"H COMMANDS...",			10,	0,	ABH, LL));
	AddSpace(New CSpace(HVER,	"HVER",	"   Hardware Version String",		10,	0,	12,  LL));

	AddSpace(New CSpace(HAL,	"",	"I COMMANDS...",			10,	0,	ABI, LL));
	AddSpace(New CSpace(AN,		"I",	"   Current",				10, ICURR,	NAV, RR));
	AddSpace(New CSpace(AN,		"I2T",	"   RMS Current Loading %",		10, I2T,	NAV, LL));
	AddSpace(New CSpace(AN,		"I2TL",	"   I2T Warning %",			10, I2TLIM,	NCU, BB));
	AddSpace(New CSpace(AN,		"ICMD",	"   Current Command",			10, ICMD,	NCU, RR));
	AddSpace(New CSpace(AN,		"ICNT",	"   Rated Current",			10, ICONT,	NCU, RR));
	AddSpace(New CSpace(AN,		"ID",	"   D-Component of Current Monitor",	10, ID,		NAV, RR));
	AddSpace(New CSpace(AN,		"IMAX",	"   Current Limit for Drive/Motor",	10, IMAX,	NCU, RR));
	AddSpace(New CSpace(INAD,	"INAD",	"   A/D Channels Input counts",		10,	0,	7,   LL));
	AddSpace(New CSpace(INS,	"IN",	"   Digital Input Status",		10,	1,	20,  BB));
	AddSpace(New CSpace(INMODE,	"INMO",	"   Digital Input Function",		10,	1,	20,  BB));
	AddSpace(New CSpace(INTRIG,	"INTR",	"   INMODE Trigger Data",		10,	1,	20,  LL));
	AddSpace(New CSpace(AN,		"INPO",	"   In-Position Status",		10, INPOS,	NPO, BB));
	AddSpace(New CSpace(AN,		"INPT",	"   In-Position Delay",			10, INPT,	NPO, WW));
	AddSpace(New CSpace(AN,		"IPEK",	"   Peak Current - Application",	10, IPEAK,	NCU, RR));
	AddSpace(New CSpace(AN,		"IQ",	"   Q-Component of Current Monitor",	10, IQ,		NAV, RR));
	AddSpace(New CSpace(ISCALE,	"ISCA",	"   Analog Current Scaling",		10,	1,	8,   RR));

	AddSpace(New CSpace(HAL,	"",	"K COMMANDS...",			10,	0,	ABK, LL));
	AddSpace(New CSpace(AN,		"K",	"   Kill",				10, KILL,	NAM, BT));
	AddSpace(New CSpace(AN,		"KC",	"   I-Controller Prediction Current",	10, KC,		NCU, RR));
	AddSpace(New CSpace(AN,		"KEYL",	"   Lock the Push Buttons",		10, KEYLOCK,	NBS, BB));
	AddSpace(New CSpace(AN,		"KTN",	"   Integral - Current Controller",	10, KTN,	NCU, RR));

	AddSpace(New CSpace(HAL,	"",	"L COMMANDS...",			10,	0,	ABL, LL));
	AddSpace(New CSpace(AN,		"L",	"   Stator Inductance",			10, LIND,	NMT, RR));
	AddSpace(New CSpace(LATCH,	"LTCH",	"   Latched 32/16 Position(DRVSTAT)",	10,	0,	1,   LL));
	AddSpace(New CSpace(LATCHX,	"LTCX",	"   Latched 32/16 Position (TRJSTAT)",	10,	0,	1,   LL));
	AddSpace(New CSpace(LTCH16,	"L16P",	"   16 bit Position @ INx rising",	10,	1,	2,   LL)); // NOV 08
	AddSpace(New CSpace(LTCH32,	"L32P",	"   32 bit Position @ INx rising",	10,	1,	2,   LL)); // NOV 08
	AddSpace(New CSpace(LED,	"LED",	"   LED Display",			10,	1,	3,   BB));
	AddSpace(New CSpace(AN,		"LEDS",	"   Display Page",			10, LEDSTAT,	NDS, WW));
	AddSpace(New CSpace(AN,		"LOAD",	"   Load Parameters from EEPROM",	10, LOAD,	NAM, BT));

	AddSpace(New CSpace(HAL,	"",	"M COMMANDS...",			10,	0,	ABM, LL));
	AddSpace(New CSpace(AN,		"MXTE",	"   Switch off - Ambient �C",		10, MAXTEMPE,	NBS, WW));
	AddSpace(New CSpace(AN,		"MXTH",	"   Switch off - Heat Sink �C",		10, MAXTEMPH,	NBS, WW));
	AddSpace(New CSpace(AN,		"MXTM",	"   Switch off - Motor (Ohms)",		10, MAXTEMPM,	NBS, RR));
	AddSpace(New CSpace(AN,		"MBRA",	"   Motor Holding Brake Select",	10, MBRAKE,	NMT, BB));
	AddSpace(New CSpace(AN,		"MDBC",	"   Number of Motor Data Sets",		10, MDBCNT,	NMT, BB));
//	AddSpace(New CSpace(AN,		"MDBG",	"   Get Actual Motor Data Set String",	10, MDBGET,	NMT, LL));
//	AddSpace(New CSpace(AN,		"MDBS",	"   Set Actual Motor Data Set",		10, MDBSET,	NMT, WW));
	AddSpace(New CSpace(AN,		"MH",	"   Start Homing",			10, MH,		NPO, BT));
	AddSpace(New CSpace(AN,		"MICO",	"   Motor Continuous Current Rating",	10, MICONT,	NMT, RR));
	AddSpace(New CSpace(AN,		"MIPK",	"   Motor Peak Current Rating",		10, MIPEAK,	NMT, RR));
	AddSpace(New CSpace(AN,		"MJOG",	"   Start Jog Mode",			10, MJOG,	NPO, BT));
	AddSpace(New CSpace(AN,		"MKT",	"   Motor KT",				10, MKT,	NMT, RR));
	AddSpace(New CSpace(AN,		"MLGC",	"   Adapt Gain Q-rated - Current Loop",	10, MLGC,	NCU, RR));
	AddSpace(New CSpace(AN,		"MLGD",	"   Adapt Gain D - Current Loop",	10, MLGD,	NCU, RR));
	AddSpace(New CSpace(AN,		"MLGP",	"   Adapt Gain Q-peak - Current Loop",	10, MLGP,	NCU, RR));
	AddSpace(New CSpace(AN,		"MLGQ",	"   Adapt Gain Abs - Current Loop",	10, MLGQ,	NCU, RR));
	AddSpace(New CSpace(MNAME,	"MNAM",	"   Motor Name String",			10,	0,	3,   LL));
	AddSpace(New CSpace(AN,		"MNUM",	"   Motor Number",			10, MNUMBER,	NMT, WW));
	AddSpace(New CSpace(MONITOR,	"MNTR",	"   Monitor Output Voltage",		10,	1,	8,   WW));
	AddSpace(New CSpace(AN,		"MOVE",	"   Start Motion Task",			10, MOVE,	NPO, WW));
	AddSpace(New CSpace(AN,		"MPHA",	"   Motor Phase, Feedback Offset",	10, MPHASE,	NFB, WW));
	AddSpace(New CSpace(AN,		"MPOL",	"   Number of Motor Poles",		10, MPOLES,	NMT, BB));
	AddSpace(New CSpace(AN,		"MRD",	"   Homing to Resolver Zero, Mode 5",	10, MRD,	NPO, BT));
	AddSpace(New CSpace(AN,		"MRBW",	"   Resolver Bandwidth",		10, MRESBW,	NFB, WW));
	AddSpace(New CSpace(AN,		"MRPO",	"   Number of Resolver Poles",		10, MRESPOLES,	NFB, BB));
	AddSpace(New CSpace(AN,		"MSPD",	"   Maximum Rated Motor Velocity",	10, MSPEED,	NMT, RR));
	AddSpace(New CSpace(AN,		"MTAP",	"   Current Lead",			10, MTANGLP,	NMT, WW));
	AddSpace(New CSpace(AN,		"MTMUX","   Presetting for Motion Task",	10, MTMUX,	NPO, WW));
	AddSpace(New CSpace(MTMUXR,	"MTMXR","   MTMUX Write Response String",	10,     0,	12,  LL));
	AddSpace(New CSpace(AN,		"MUNI",	"   Units for Velocity Parameters",	10, MUNIT,	NPO, BB));
	AddSpace(New CSpace(AN,		"MVAB",	"   Velocity Lead (Start Phi)",		10, MVANGLB,	NMT, LL));
	AddSpace(New CSpace(AN,		"MVAF",	"   Velocity Lead (Limit Phi)",		10, MVANGLF,	NMT, WW));
	AddSpace(New CSpace(AN,		"MVAP",	"   Velocity Lead (Commutation Angle)",	10, MVANGLP,	NMT, WW));

	AddSpace(New CSpace(HAL,	"",	"N COMMANDS...",			10,	0,	ABN, LL));
	AddSpace(New CSpace(AN,		"NONB",	"   Mains-BTB Check On/Off",		10, NONBTB,	NDS, BB));
	AddSpace(New CSpace(AN,		"NREF",	"   Homing Mode",			10, NREF,	NPO, BB));

	AddSpace(New CSpace(HAL,	"",	"O COMMANDS...",			10,	0,	ABO, LL));
	AddSpace(New CSpace(O_ACC,	"OACC",	"   Acceleration Time - Motion Task 0",	10,	1,	2,   LL));
	AddSpace(New CSpace(AN,		"OC",	"   Control Variable - Motion Task 0",	10, O_C,	NPO, WW));
	AddSpace(New CSpace(O_DEC,	"ODEC",	"   Deceleration Time - Motion Task 0",	10,	1,	2,   WW));
	AddSpace(New CSpace(AN,		"OFN",	"   Next Task Number - Motion Task 0",	10, O_FN,	NPO, WW));
	AddSpace(New CSpace(AN,		"OFT",	"   Delay before Next Motion Task",	10, O_FT,	NPO, WW));
	AddSpace(New CSpace(AN,		"OP",	"   Target Position - Motion Task 0",	10, O_P,	NPO, LL));
	AddSpace(New CSpace(AN,		"OV",	"   Target Speed - Motion Task 0",	10, O_V,	NPO, LL));
	AddSpace(New CSpace(OUTS,	"O",	"   Digital Output Status",		10,	1,	20,  BB));
	AddSpace(New CSpace(AN,		"OCOPY","   Execute OCOPY. <data> = Quantity",	10, OCOPYQ,	NPO, BB));
	AddSpace(New CSpace(AN,		"OCPS",	"   ...OCOPY Source Task Number",	10, OCOPYS,	NPO, BB));
	AddSpace(New CSpace(AN,		"OCPD",	"   ...OCOPY Destination Task Number",	10, OCOPYD,	NPO, BB));
	AddSpace(New CSpace(OCOPYR,	"OCPR",	"   OCOPY Write Response String",	10,     0,	12,  LL));
	AddSpace(New CSpace(OMODE,	"OMOD",	"   Digital Output Function",		10,	1,	20,  BB));
	AddSpace(New CSpace(OTRIG,	"OTRI",	"   OMODE Trigger Data",		10,	1,	20,  LL));
	AddSpace(New CSpace(AN,		"OPMO",	"   Operating Mode",			10, OPMODE,	NAM, BB));
	AddSpace(New CSpace(AN,		"OPTI",	"   Option Slot ID",			10, OPTION,	NDS, WW));

	AddSpace(New CSpace(HAL,	"",	"P COMMANDS...",			10,	0,	ABP, LL));
	AddSpace(New CSpace(AN,		"PASS",	"   Parameter Change Password",		10, PASSCM,	NBS, LL));
	AddSpace(New CSpace(AN,		"PBAL",	"   Regen Power - Actual",		10, PBAL,	NAV, LL));
	AddSpace(New CSpace(AN,		"PBAX",	"   Regen Power - Maximum",		10, PBALMAX,	NBS, LL));
	AddSpace(New CSpace(AN,		"PBAR",	"   Regen Resistor - Select",		10, PBALRES,	NBS, BB));
//FB	AddSpace(New CSpace(AN,		"PBAU",	"   Profibus Baud Rate",		10, PBAUD,	NCM, RR));
	AddSpace(New CSpace(AN,		"PE",	"   Following Error - Actual",		10, PE,		NAV, LL));
	AddSpace(New CSpace(AN,		"PEIN",	"   In-Position Window",		10, PEINPOS,	NPO, LL));
	AddSpace(New CSpace(AN,		"PEMX",	"   Following Error - Maximum",		10, PEMAX,	NPO, LL));
	AddSpace(New CSpace(AN,		"PFB",	"   Actual Position from Feedback",	10, PFB,	NAV, LL));
	AddSpace(New CSpace(AN,		"PFB0",	"   Position from External Encoder",	10, PFB0,	NAV, LL));
	AddSpace(New CSpace(AN,		"PGRI",	"   Position Resolution - Numerator",	10, PGEARI,	NPO, LL));
	AddSpace(New CSpace(AN,		"PGRO",	"   Position Resolution - Denominator",	10, PGEARO,	NPO, LL));
	AddSpace(New CSpace(AN,		"PMOD",	"   Line Phase Error Mode",		10, PMODE,	NBS, BB));
	AddSpace(New CSpace(AN,		"POSC",	"   Axes Type",				10, POSCNFG,	NPO, BB));
	AddSpace(New CSpace(AN,		"PRD",	"   20-bit Position Feedback",		10, PRD,	NAV, LL));
//FB	AddSpace(New CSpace(AN,		"PSTA",	"   Profibus State String",		10, PSTATE,	NCM, LL));
	AddSpace(New CSpace(AN,		"PTMN",	"   Min. Accel for Motion Tasks",	10, PTMIN,	NPO, WW));
	AddSpace(New CSpace(AN,		"PUNI",	"   Position Resolution",		10, PUNIT,	NPO, LL));
	AddSpace(New CSpace(AN,		"PV",	"   Actual Velocity - Position Loop",	10, PV,		NAV, LL));
	AddSpace(New CSpace(AN,		"PVX",	"   Max. Velocity - Position Loop",	10, PVMAX,	NPO, LL));
	AddSpace(New CSpace(AN,		"PVXN",	"   Max. Neg Velocity - Position Loop",	10, PVMAXN,	NPO, LL));
	AddSpace(New CSpace(AN,		"PVXP",	"   Maximum Positive Velocity",		10, PVMAXP,	NPO, LL));

	AddSpace(New CSpace(HAL,	"",	"R COMMANDS...",			10,	0,	ABR, LL));
	AddSpace(New CSpace(AN,		"RDY",	"   Software Enable Status",		10, READY,	NDS, BB));
	AddSpace(New CSpace(AN,		"REFI",	"   Peak Rated Current for Homing 7",	10, REFIP,	NCU, RR));
	AddSpace(New CSpace(AN,		"REFP",	"   Reference Switch Position",		10, REFPOS,	NPO, LL));
	AddSpace(New CSpace(AN,		"REMO",	"   Hardware Enable Status",		10, REMOTE,	NDS, BB));
	AddSpace(New CSpace(AN,		"RESP",	"   Resolver Phase",			10, RESPHASE,	NFB, WW));
	AddSpace(New CSpace(AN,		"RK",	"   Resolver Sine Gain Adjust",		10, RK,		NFB, WW));
	AddSpace(New CSpace(AN,		"ROFF",	"   Reference Offset",			10, ROFFS,	NPO, LL));
	AddSpace(New CSpace(AN,		"RABS",	"   Offset to Encoder Position",	10, ROFFSA,	NPO, LL));
	AddSpace(New CSpace(AN,		"R232",	"   RS232 Watchdog Time",		10, RS232T,	NCM, WW));
	AddSpace(New CSpace(AN,		"RSTV",	"   Restore Variables to Default",	10, RSTVAR,	NBS, BT));

	AddSpace(New CSpace(HAL,	"",	"S COMMANDS...",			10,	0,	ABS, LL));
	AddSpace(New CSpace(AN,		"S",	"   Stop Motor and Disable Drive",	10, S_STOP,	NOS, BT));
	AddSpace(New CSpace(AN,		"SAVE",	"   Save Data in EEPROM",		10, SAVE,	NAM, BT));
	AddSpace(New CSpace(AN,		"SCAN",	"   Restart Communications",		10, SCANX,	NCM, BT));
	AddSpace(New CSpace(AN,		"SNO",	"   Drive Serial Number",		10, SERIALNO,	NBS, LL));
	AddSpace(New CSpace(AN,		"SETR",	"   Set Reference Point",		10, SETREF,	NPO, BT));
	AddSpace(New CSpace(AN,		"SLOT",	"   I/O States - Expansion Card",	10, SLOTIO,	NDI, LL));
	AddSpace(New CSpace(AN,		"SSIG",	"   SSI Code Select",			10, SSIGRAY,	NFB, BB));
	AddSpace(New CSpace(AN,		"SSII",	"   SSI Clock",				10, SSIINV,	NFB, BB));
	AddSpace(New CSpace(AN,		"SSIM",	"   SSI Mode",				10, SSIMODE,	NPO, BB));
	AddSpace(New CSpace(AN,		"SSIO",	"   SSI Baud Rate",			10, SSIOUT,	NFB, BB));
	AddSpace(New CSpace(AN,		"STAT",	"   Drive Status Word",			10, STAT,	NDS, WW));
	AddSpace(New CSpace(AN,		"STCO",	"   Status Variable Warnings",		10, STATCODE,	NDS, LL));
	AddSpace(New CSpace(STATIO,	"STIO",	"   I/O Status",			10,	1,	8,   BB));
	AddSpace(New CSpace(STATUS,	"STUS",	"   Detailed Amplifier Status",		10,	1,	5,   LL));
	AddSpace(New CSpace(AN,		"STOP",	"   Stop Motion Task",			10, STOP,	NPO, BT));
	AddSpace(New CSpace(AN,		"SCF",	"   Position Reg 1...4 Configuration",	10, SWCNFG,	NPO, WW));
	AddSpace(New CSpace(AN,		"SCF2",	"   Position Reg 0 & 5 Configuration",	10, SWCNFG2,	NPO, WW));
	AddSpace(New CSpace(SWE,	"SWE",	"   Position Register Data",		10,	0,	8,   LL));
	AddSpace(New CSpace(SWEN,	"SWEN",	"   Cam Position Register Data",	10,	0,	8,   LL));

	AddSpace(New CSpace(HAL,	"",	"T COMMANDS...",			10,	0,	ABT, LL));
	AddSpace(New CSpace(AN,		"T",	"   Digital Current Command",		10, TCURR,	NOS, RR));
	AddSpace(New CSpace(AN,		"TMPE",	"   Ambient Temperature",		10, TEMPE,	NAV, LL));
	AddSpace(New CSpace(AN,		"TMPH",	"   Heat Sink Temperature",		10, TEMPH,	NAV, LL));
	AddSpace(New CSpace(AN,		"TMPM",	"   Motor Temperature",			10, TEMPM,	NAV, LL));
	AddSpace(New CSpace(AN,		"TRJS",	"   Status 2",				10, TRJSTAT,	NDS, LL));
	AddSpace(New CSpace(AN,		"TRUN",	"   Run-Time Counter Value - seconds",	10, TRUN,	NBS, LL));

	AddSpace(New CSpace(HAL,	"",	"U COMMANDS...",			10,	0,	ABU, LL));
	AddSpace(New CSpace(AN,		"UID",	"   User ID",				10, UID,	NAM, WW));
	AddSpace(New CSpace(USRDEF,	"USER",	"   User Defined Command String",	10,	0,	19,  LL));

	AddSpace(New CSpace(HAL,	"",	"V COMMANDS...",			10,	0,	ABV, LL));
	AddSpace(New CSpace(AN,		"V",	"   Actual Velocity",			10, VEL,	NAV, RR));
	AddSpace(New CSpace(AN,		"VBUS",	"   DC-bus Voltage",			10, VBUS,	NAV, LL));
	AddSpace(New CSpace(AN,		"VBSB",	"   Maximum Line Voltage",		10, VBUSBAL,	NBS, WW));
	AddSpace(New CSpace(AN,		"VBSX",	"   Maximum DC-bus Voltage",		10, VBUSMAX,	NAM, LL));
	AddSpace(New CSpace(AN,		"VBSN",	"   Minimum DC-bus Voltage",		10, VBUSMIN,	NAM, LL));
	AddSpace(New CSpace(AN,		"VCMD",	"   Internal Velocity RPM",		10, VCMD,	NAV, RR));
	AddSpace(New CSpace(VER,	"VER",	"   Firmware Version String",		10,	0,	12,  LL));
	AddSpace(New CSpace(AN,		"VJOG",	"   Jog Mode Speed",			10, VJOG,	NPO, LL));
	AddSpace(New CSpace(AN,		"VLIM",	"   Maximum Velocity",			10, VLIM,	NVE, RR));
	AddSpace(New CSpace(AN,		"VMAX",	"   Maximum System Speed",		10, VMAX,	NVE, RR));
	AddSpace(New CSpace(AN,		"VMIX",	"   Velocity Mix",			10, VMIX,	NVE, RR));
//FB	AddSpace(New CSpace(AN,		"VMUL",	"   Velocity Scale Factor",		10, VMUL,	NCU, LL));
	AddSpace(New CSpace(AN,		"VOSP",	"   Overspeed",				10, VOSPD,	NVE, RR));
	AddSpace(New CSpace(AN,		"VREF",	"   Homing Speed",			10, VREF,	NPO, LL));
	AddSpace(New CSpace(VSCALE,	"VSCA",	"   Velocity Scaling - Analog Input",	10,	1,	8,   WW));

	AddSpace(New CSpace(HAL,	"",	"W COMMANDS...",			10,	0,	ABW, LL));
	AddSpace(New CSpace(AN,		"WMSK",	"   Warning as Fault Mask",		10, WMASK,	NAM, LL));

	// List by Function Headers
	AddSpace(New CSpace(HFN,	"",	"ACTUAL VALUES...",			10,	0,	HAV, LL));
	AddSpace(New CSpace(HFN,	"",	"AMPLIFIER...",				10,	0,	HAM, LL));
	AddSpace(New CSpace(HFN,	"",	"ANALOG I/O...",			10,	0,	HAI, LL));
	AddSpace(New CSpace(HFN,	"",	"BASIC SETUP...",			10,	0,	HBS, LL));
	AddSpace(New CSpace(HFN,	"",	"COMMUNICATION...",			10,	0,	HCM, LL));
	AddSpace(New CSpace(HFN,	"",	"CURRENT...",				10,	0,	HCU, LL));
	AddSpace(New CSpace(HFN,	"",	"DIGITAL I/O...",			10,	0,	HDI, LL));
	AddSpace(New CSpace(HFN,	"",	"DRIVE STATUS...",			10,	0,	HDS, LL));
	AddSpace(New CSpace(HFN,	"",	"FEEDBACK...",				10,	0,	HFB, LL));
	AddSpace(New CSpace(HFN,	"",	"GEARING...",				10,	0,	HGR, LL));
	AddSpace(New CSpace(HFN,	"",	"MOTOR...",				10,	0,	HMT, LL));
	AddSpace(New CSpace(HFN,	"",	"OSCILLOSCOPE...",			10,	0,	HOS, LL));
	AddSpace(New CSpace(HFN,	"",	"POSITION...",				10,	0,	HPO, LL));
	AddSpace(New CSpace(HFN,	"",	"VELOCITY...",				10,	0,	HVE, LL));

	AddSpace(New CSpace(LSA,	"",	"LIST - ALPHABETICAL",			10,	0,	LAL, LL));
	AddSpace(New CSpace(LSF,	"",	"LIST - FUNCTION",			10,	0,	LFN, LL));
	AddSpace(New CSpace(KEYW,	"",	"LIST - KEYWORD",			16,	0xA,	0xF, LL));
	}

// Helpers
BOOL CKollmorgen600Driver::IsStringItem(UINT uTable)
{
	switch( uTable ) {

		case ALIAS:
		case DEVICE:
		case ERRCODE:
		case ERSP:
		case HVER:
		case MDBGET:
		case MDBSET:
		case MNAME:
		case MTMUXR:
		case OCOPYR:
		case PSTATE:
		case VER:
		case USRDEF:
			return TRUE;
		}

	return FALSE;
	}

CSpace * CKollmorgen600Driver::GetSpace(CAddress const &Addr)
{
	CSpaceList & List = CStdCommsDriver::GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( MatchSpace(pSpace, Addr) ) {

				return pSpace;
				}

			List.GetNext(n);
			}
		}

	return NULL;
	}

BOOL CKollmorgen600Driver::MatchSpace(CSpace * pSpace, CAddress const &Addr)
{
	UINT uTable = pSpace->m_uTable;

	if( uTable == Addr.a.m_Table ) {

		if( uTable == addrNamed ) {

			if( Addr.a.m_Offset == pSpace->m_uMinimum ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen600 Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CKollmorgen600AddrDialog, CStdAddrDialog);
		
// Constructor

CKollmorgen600AddrDialog::CKollmorgen600AddrDialog(CKollmorgen600Driver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fPart = fPart;

	m_pKMDriver = &Driver;

	SelectDefaultSpace();

	if( m_uListSel ) {

		if( m_uAllowSpace < ABA || m_uAllowSpace > ABZ ) m_uAllowSpace = ABA;
		}

	else {
		if( m_uAllowSpace < HAV || m_uAllowSpace > HVE ) m_uAllowSpace = HAV;
		}

	m_KWText = "";
	}

// Message Map

AfxMessageMap(CKollmorgen600AddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CKollmorgen600AddrDialog)
	};

// Message Handlers

BOOL CKollmorgen600AddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;

	m_pSpace   = m_pKMDriver->GetSpace(*m_pAddr);

	m_uListSel = Addr.a.m_Extra ? 1 : 0;

	SetAllow();
	
	if( !m_fPart ) {

		m_fListTypeChange = TRUE;

		LoadList();

		UINT uClear = 2;

		if( m_pSpace ) {

			UINT uT = m_pSpace->m_uTable;

			if( uT < HTMIN || uT == AN) {

				ShowDetails();

				uClear = 0;
				}

			else uClear = 1;

			if( Addr.a.m_Table ) {

				if( Addr.a.m_Table < HTMIN ) {

					if( DoShowAddress(Addr) ) {

						SetDlgFocus(2002);
						SetAddressText(m_pSpace->GetValueAsText(Addr.a.m_Offset));
						GetDlgItem(2002).EnableWindow(TRUE);
						}
					}

				else {
					SetDlgFocus(1001);
					GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
					GetDlgItem(2002).SetWindowText("");
					GetDlgItem(2002).EnableWindow(FALSE);
					GetDlgItem(2003).SetWindowText("");
					GetDlgItem(2003).EnableWindow(FALSE);
					}
				}
			}

		ClearInfo(uClear);

		return !(BOOL(m_pSpace));
		}
	else {
		if( m_pSpace && !IsHeader(m_pSpace->m_uMaximum) ) {

			LoadType();

			DoShowAddress(Addr);

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				DoShowAddress(Addr);
				}
			}

		SetAddressFocus();

		return FALSE;
		}
	}

// Notification Handlers

void CKollmorgen600AddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CKollmorgen600AddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		CString sOffset = GetAddressText();

		if( m_pSpace != pSpace ) {

			UINT uMax = m_pSpace->m_uMaximum;

			m_fListTypeChange = FALSE;

			if( IsHeader(uMax) ) {

				m_uAllowSpace = uMax;

				if( ListSelectMade(m_pSpace) ) {

					m_uListSel = uMax & 3;

					if( NeedDefaultSpace(uMax) ) SelectDefaultSpace();
					}

				else {
					if( IsAlphaHeader(uMax) ) m_uListSel = SELAL;

					else {
						if( IsFunctHeader(uMax) ) m_uListSel = SELFN;
						}
					}

				m_fListTypeChange = TRUE;

				LoadList();
				}

			CAddress Addr;

			m_pSpace->GetMinimum(Addr);

			Addr.a.m_Type = m_pSpace->m_uType;

			BOOL fKW = m_pSpace->m_uTable == KEYW;

			BOOL fHd = HeaderSpaceType(m_pSpace) ^ fKW;

			GetDlgItem(2001).SetWindowText( !fHd ? m_pSpace->m_Prefix : "");

			sOffset = fKW ? "<Text>" : m_pSpace->GetValueAsText(m_pSpace->m_uMinimum);

			UINT uFocus = 1001;

			if( m_pSpace->m_uTable < HTMIN || fKW ) {

				GetDlgItem(2002).SetWindowText(sOffset);
				GetDlgItem(2002).EnableWindow(TRUE);
				GetDlgItem(2003).SetWindowText("");
				GetDlgItem(2003).EnableWindow(FALSE);
				uFocus = 2002;
				if( !fKW ) {
					ShowDetails();
					DoShowAddress(Addr);
					}

				else ClearDetails();
				}

			else {
				if( !fHd ) ShowDetails();

				ClearInfo(fHd ? 2 : 1);
				}

			SetDlgFocus(uFocus);

			return;
			}
		}
	else {
		SelectDefaultSpace();

		ClearDetails();

		GetDlgItem(2002).EnableWindow(FALSE);
		GetDlgItem(2003).EnableWindow(FALSE);

		m_pSpace = NULL;
		}
	}

BOOL CKollmorgen600AddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( IsHeader(m_pSpace->m_uMaximum) ) {

			if( m_fPart ) return FALSE;

			SetAllow();
			LoadList();

			return TRUE;
			}

		if( m_pSpace->m_uTable == KEYW ) {

			m_fListTypeChange = TRUE;
			m_KWText          = GetDlgItem(2002).GetWindowText();
			m_uListSel        = SELKW;
			m_KWText.MakeLower();
			LoadList();
			return TRUE;
			}

		CString Text = m_pSpace->m_Prefix;

		if( m_pSpace->m_uTable < HTMIN ) {

			Text += GetDlgItem(2002).GetWindowText();
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pKMDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			Addr.a.m_Extra = m_uListSel ? 1 : 0;

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// List Selection

void CKollmorgen600AddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 40, 100, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf("<%s>\t%s", CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING);

	INDEX	         Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	if( List.GetCount() ) {

		DoDesignatedHeader(&ListBox);

		BOOL fKeyDone = m_uListSel == SELKW;

		Find = DoSelections(&ListBox);

		DoRemainingHeaders(&ListBox, fKeyDone);
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSpaceChange(1001, ListBox);
	}

void CKollmorgen600AddrDialog::LoadEntry(CListBox * pBox, CString p, CString c, DWORD n)
{
	CString Entry;

	Entry.Printf("%s\t%s", p, c);

	pBox->AddString(Entry, n);
	}

void CKollmorgen600AddrDialog::DoDesignatedHeader(CListBox *pBox)
{
	if( m_fListTypeChange && m_KWText && m_KWText[0] ) return;

	CSpaceList &List = m_pDriver->GetSpaceList();

	INDEX n = List.GetHead();

	while( !List.Failed(n) ) { // do designated Header first

		CSpace *p = List[n];

		if( p->m_uMaximum == m_uAllowSpace ) {

			if( p == m_pSpace ) m_fListTypeChange = TRUE;

			LoadEntry( pBox, p->m_Prefix, p->m_Caption, DWORD(n) );

			return;
			}

		List.GetNext(n);
		}
	}

INDEX CKollmorgen600AddrDialog::DoSelections(CListBox *pBox)
{
	INDEX n = INDEX(NOTHING);

	switch( m_uListSel & 0xF ) {

		case SELAL:
		case SELFN:	n = DoItems(pBox);	break;

		case SELKW:	n = DoKeyWd(pBox);	break;
		}

	return n;
	}

INDEX CKollmorgen600AddrDialog::DoItems(CListBox *pBox)
{
	CSpaceList &List = m_pDriver->GetSpaceList();

	CSpace * p;

	INDEX Find = INDEX(NOTHING);

	INDEX n = List.GetHead();

	while( !List.Failed(n) ) { // do Selections

		p = List[n];

		BOOL fLoad = FALSE;

		if( !IsHeader(p->m_uMaximum) ) {

			if( p->m_uTable == AN ) fLoad = SelectList( p->m_uMinimum, m_uAllowSpace );

			else {
				fLoad = SelectTableItemList( p->m_uTable ) == m_uAllowSpace;
				}

			if( fLoad ) {

				LoadEntry( pBox, p->m_Prefix, p->m_Caption, DWORD(n) );

				if( m_fListTypeChange ) {

					Find = n;
					m_fListTypeChange = FALSE;
					}

				else {
					if( p == m_pSpace ) Find = n;
					}
				}
			}

		List.GetNext(n);
		}

	return Find;
	}

INDEX CKollmorgen600AddrDialog::DoKeyWd(CListBox *pBox)
{
	CSpaceList &List = m_pDriver->GetSpaceList();

	CSpace * pSpace;

	INDEX Find = INDEX(NOTHING);

	INDEX n    = List.GetHead();

	BOOL fDone = FALSE;

	while( !fDone ) {

		pSpace = List[n];

		if( pSpace->m_uTable == KEYW ) {

			LoadEntry(pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n));

			fDone = TRUE;
			}

		else List.GetNext(n);
		}

	n = List.GetHead();

	while( !List.Failed(n) ) {

		pSpace = List[n];

		if( !IsHeader(pSpace->m_uMaximum) ) {

			CString p = pSpace->m_Prefix;
			CString c = pSpace->m_Caption;

			c.MakeLower();

			if( c.Find(m_KWText) != NOTHING ) {

				LoadEntry( pBox, p, pSpace->m_Caption, DWORD(n) ); // restore case

				if( m_fListTypeChange ) {

					m_fListTypeChange = FALSE;

					Find = n;

					m_uListSel = SELAL;

					if( !(m_uAllowSpace = SelectTableItemList(pSpace->m_uTable)) ) {

						m_uAllowSpace = ListMember(pSpace->m_uMinimum);
						}
					}
				}
			}

		List.GetNext(n);
		}

	m_KWText = "";

	return Find;
	}

void CKollmorgen600AddrDialog::DoRemainingHeaders(CListBox *pBox, BOOL fKeyDone)
{
	CSpaceList &List = m_pDriver->GetSpaceList();

	CSpace *p;

	INDEX n = List.GetHead();

	BOOL fKeyList = m_KWText && m_KWText[0];
	
	while( !List.Failed(n) ) { // do the rest of the Headers

		p = List[n];

		UINT uMax = p->m_uMaximum;

		if( IsHeader(uMax) && (fKeyDone || (uMax != m_uAllowSpace)) ) {

			if( (IsAlphaHeader(uMax) && (m_uListSel == SELAL)) ||
			    (IsFunctHeader(uMax) && (m_uListSel == SELFN)) ||
			    ListSelectMade(p)
			    ) {
				LoadEntry( pBox, p->m_Prefix, p->m_Caption, DWORD(n) );
				}
			}

		List.GetNext(n);
		}

	if( fKeyDone ) return;

	if( fKeyList ) m_KWText = "";

	n = List.GetHead();

	while( !List.Failed(n) ) {

		p = List[n];

		if( p->m_uTable == KEYW ) {

			LoadEntry(pBox, p->m_Prefix, p->m_Caption, DWORD(n));

			return;
			}

		List.GetNext(n);
		}
	}

BOOL CKollmorgen600AddrDialog::AllowSpace(CSpace *pSpace)
{
	if( IsHeader(pSpace->m_uMaximum) ) return FALSE;

	UINT uTable = pSpace->m_uTable;

	UINT uAllow = m_uAllowSpace;

	if( uAllow == LAL ) uAllow = ABA;

	if( uAllow == LFN ) uAllow = NAV;

	if( uTable == AN ) return SelectList(pSpace->m_uMinimum, uAllow);

	UINT uL = SelectTableItemList(uTable);

	return uL == uAllow;
	}

// Helpers

void CKollmorgen600AddrDialog::SetAllow()
{
	if( !m_pSpace ) {

		SelectDefaultSpace();
		return;
		}

	UINT uMax   = m_pSpace->m_uMaximum;
	UINT uTable = m_pSpace->m_uTable;

	if( uTable < HTMIN ) {

		m_uAllowSpace = SelectTableItemList(uTable);
		return;
		}

	UINT uSpace = GetHeaderSpace(uTable, uMax);

	switch( uSpace ) {

		case SPAT:
		case SPFT:
			m_uAllowSpace = uMax;
			return;

		case SPLT:
			m_uListSel  = uMax & 3;
			SelectDefaultSpace();
			return;
		}

	UINT uLo = m_uListSel ? ABA : HAV;
	UINT uHi = m_uListSel ? ABZ : HVE;

	for( UINT i = uLo; i <= uHi; i++ ) {

		if( SelectList(m_pSpace->m_uMinimum, i) ) {

			m_uAllowSpace = i;

			return;
			}
		}
	}

BOOL CKollmorgen600AddrDialog::SelectList(UINT uCommand, UINT uAllow)
{
	UINT uItem = SelectTableItemList(uCommand);

	if( uItem ) return LOBYTE(uItem) == LOBYTE(uAllow);

	UINT uL = ListMember(uCommand);

	if( LOBYTE(uL) == LOBYTE(uAllow) ) {

		if( m_uListSel ) return uAllow >= ABA && uAllow <= ABZ;

		return uAllow >= HAV && uAllow <= HVE;
		}

	return FALSE;
	}

UINT CKollmorgen600AddrDialog::ListMember(UINT uCommand)
{
	switch( uCommand ) {

		// Actual Values
		case ICURR:
		case I2T:
		case ID:
		case IQ:	return m_uListSel ? ABI : NAV;

		case PBAL:
		case PE:
		case PFB:
		case PFB0:
		case PRD:
		case PV:	return m_uListSel ? ABP : NAV;

		case TEMPE:
		case TEMPH:
		case TEMPM:	return m_uListSel ? ABT : NAV;

		case VEL:
		case VBUS:
		case VCMD:	return m_uListSel ? ABV : NAV;

		// Amplifier
		case ACCUNIT:
		case ACTFAULT:	return m_uListSel ? ABA : NAM;

		case CLRFAULT:	return m_uListSel ? ABC : NAM;

		case DICONT:
		case DIPEAK:
		case DIS:	return m_uListSel ? ABD : NAM;

		case EN:	return m_uListSel ? ABE : NAM;

		case KILL:	return m_uListSel ? ABK : NAM;

		case LOAD:	return m_uListSel ? ABL : NAM;

		case OPMODE:	return m_uListSel ? ABO : NAM;

		case SAVE:	return m_uListSel ? ABS : NAM;

		case UID:	return m_uListSel ? ABU : NAM;

		case VBUSMAX:
		case VBUSMIN:	return m_uListSel ? ABV : NAM;

		case WMASK:	return m_uListSel ? ABW : NAM;

		// Analog I/O
		case AN10TX:
		case AN11NR:
		case AN11RANGE:
		case ANCNFG:
		case ANDB:	return m_uListSel ? ABA : NAI;

		// Basic Setup
		case ADDR:
		case AENA:	return m_uListSel ? ABA : NBS;

		case CBAUD:	return m_uListSel ? ABC : NBS;

		case KEYLOCK:	return m_uListSel ? ABK : NBS;

		case MAXTEMPE:
		case MAXTEMPH:
		case MAXTEMPM:	return m_uListSel ? ABM : NBS;

		case PASSCM:
		case PBALMAX:
		case PBALRES:
		case PMODE:	return m_uListSel ? ABP : NBS;

		case RSTVAR:	return m_uListSel ? ABR : NBS;

		case SERIALNO:	return m_uListSel ? ABS : NBS;

		case TRUN:	return m_uListSel ? ABT : NBS;

		case VBUSBAL:	return m_uListSel ? ABV : NBS;

		// Communication
		case ACTRS232:
		case ADDRFB:	return m_uListSel ? ABA : NCM;

		case EXTWD:	return m_uListSel ? ABE : NCM;

		case FLASH:	return m_uListSel ? ABF : NCM;

		case PBAUD:
		case PSTATE:	return m_uListSel ? ABP : NCM;

		case RS232T:	return m_uListSel ? ABR : NCM;

		case SCANX:	return m_uListSel ? ABS : NCM;

		case VMUL:	return m_uListSel ? ABV : NCM;

		// Current
		case I2TLIM:
		case ICMD:
		case ICONT:
		case IMAX:
		case IPEAK:	return m_uListSel ? ABI : NCU;

		case KC:
		case KTN:	return m_uListSel ? ABK : NCU;

		case MLGC:
		case MLGD:
		case MLGP:
		case MLGQ:	return m_uListSel ? ABM : NCU;

		case REFIP:	return m_uListSel ? ABR : NCU;

		// Digital I/O
		case SLOTIO:	return m_uListSel ? ABS : NDI;

		// Drive Status
		case ACTIVE:	return m_uListSel ? ABA : NDS;

		case CLRHR:
		case CLRWARN:
		case COLDSTART:	return m_uListSel ? ABC : NDS;

		case DRVSTAT:	return m_uListSel ? ABD : NDS;

		case ERRCODEA:	return m_uListSel ? ABE : NDS;

		case LEDSTAT:	return m_uListSel ? ABL : NDS;

		case NONBTB:	return m_uListSel ? ABN : NDS;

		case OPTION:	return m_uListSel ? ABO : NDS;

		case READY:
		case REMOTE:	return m_uListSel ? ABR : NDS;

		case STAT:
		case STATCODE:	return m_uListSel ? ABS : NDS;

		case TRJSTAT:	return m_uListSel ? ABT : NDS;

		// Feedback
		case CALCRK:	return m_uListSel ? ABC : NFB;

		case FBTYPE:	return m_uListSel ? ABF : NFB;

		case MPHASE:
		case MRESBW:
		case MRESPOLES:	return m_uListSel ? ABM : NFB;

		case RESPHASE:
		case RK:	return m_uListSel ? ABR : NFB;

		case SSIGRAY:
		case SSIINV:
		case SSIOUT:	return m_uListSel ? ABS : NFB;

		// Gearing
		case ENCIN:	return m_uListSel ? ABE : NGR;

		case GEARI:
		case GEARMODE:
		case GEARO:	return m_uListSel ? ABG : NGR;

		// Motor
		case ENCLINES:	return m_uListSel ? ABE : NMT;

		case FLUXM:	return m_uListSel ? ABF : NMT;

		case LIND:	return m_uListSel ? ABL : NMT;

		case MBRAKE:
		case MDBCNT:
		case MDBGET:
		case MDBSET:
		case MICONT:
		case MIPEAK:
		case MKT:
		case MNUMBER:
		case MPOLES:
		case MSPEED:
		case MTANGLP:
		case MVANGLB:
		case MVANGLF:
		case MVANGLP:	return m_uListSel ? ABM : NMT;

		// Oscilloscope
		case S_STOP:	return m_uListSel ? ABS : NOS;

		case TCURR:	return m_uListSel ? ABT : NOS;

		// Position
		case ACCR:
		case AUTOHOME:	return m_uListSel ? ABA : NPO;
		
		case CLRORDER:	return m_uListSel ? ABC : NPO;
		
		case DECR:
		case DREF:	return m_uListSel ? ABD : NPO;
		
		case ENCMODE:
		case ENCOUT:
		case ENZERO:
		case EXTMUL:	return m_uListSel ? ABE : NPO;

		case GP:
		case GPFBT:
		case GPFFV:
		case GPTN:
		case GPV:	return m_uListSel ? ABG : NPO;

		case INPOS:
		case INPT:	return m_uListSel ? ABI : NPO;

		case MH:
		case MJOG:
		case MOVE:
		case MRD:
		case MTMUX:
		case MUNIT:	return m_uListSel ? ABM : NPO;
		
		case NREF:	return m_uListSel ? ABN : NPO;

		case O_C:
		case O_FN:
		case O_FT:
		case O_P:
		case O_V:
		case OCOPYQ:
		case OCOPYS:
		case OCOPYD:	return m_uListSel ? ABO : NPO;
		
		case PEINPOS:
		case PEMAX:
		case PGEARI:
		case PGEARO:
		case POSCNFG:
		case PTMIN:
		case PUNIT:
		case PVMAX:
		case PVMAXN:
		case PVMAXP:	return m_uListSel ? ABP : NPO;

		case REFPOS:
		case ROFFS:
		case ROFFSA:	return m_uListSel ? ABR : NPO;

		case SETREF:
		case SSIMODE:
		case STOP:
		case SWCNFG:
		case SWCNFG2:	return m_uListSel ? ABS : NPO;

		case VJOG:
		case VREF:	return m_uListSel ? ABV : NPO;

		// Velocity
		case ACC:	return m_uListSel ? ABA : NVE;

		case DEC:
		case DECDIS:
		case DECSTOP:
		case DIR:	return m_uListSel ? ABD : NVE;

		case GV:
		case GVFBT:
		case GVFILT:
		case GVFR:
		case GVT2:
		case GVTN:	return m_uListSel ? ABG : NVE;

		case VLIM:
		case VMAX:
		case VMIX:
		case VOSPD:	return m_uListSel ? ABV : NVE;
		}

	return m_uListSel ? ABA : NAV;
	}

UINT CKollmorgen600AddrDialog::SelectTableItemList(UINT uTable)
{
	switch( uTable ) {

		case ALIAS:	return m_uListSel ? ABA : HBS;
		case ANTRIG:	return m_uListSel ? ABA : HAI;
		case ANIN:	return m_uListSel ? ABA : HAV;
		case ANOFF:	return m_uListSel ? ABA : HAI;
		case ANOUT:	return m_uListSel ? ABA : HAI;
		case ANZERO:	return m_uListSel ? ABA : HAI;
		case AVZ:	return m_uListSel ? ABA : HAI;
		case DAOFFSET:	return m_uListSel ? ABD : HVE;
		case DEVICE:	return m_uListSel ? ABD : HAM;
		case ERRCODE:	return m_uListSel ? ABE : HBS;
		case ERSP:	return m_uListSel ? ABE : HBS;
		case FLTCNT:	return m_uListSel ? ABF : HBS;
		case FLTHIST:	return m_uListSel ? ABF : HBS;
		case HVER:	return m_uListSel ? ABH : HBS;
		case INS:	return m_uListSel ? ABI : HDI;
		case INAD:	return m_uListSel ? ABI : HAI;
		case INMODE:	return m_uListSel ? ABI : HDI;
		case INTRIG:	return m_uListSel ? ABI : HDI;
		case ISCALE:	return m_uListSel ? ABI : HAI;
		case LATCH:	return m_uListSel ? ABL : HPO;
		case LATCHX:	return m_uListSel ? ABL : HPO;
		case LTCH16:	return m_uListSel ? ABL : HPO;
		case LTCH32:	return m_uListSel ? ABL : HPO;
		case LED:	return m_uListSel ? ABL : HAV;
		case MNAME:	return m_uListSel ? ABM : HMT;
		case MONITOR:	return m_uListSel ? ABM : HAI;
		case MTMUXR:	return m_uListSel ? ABM : HPO;
		case O_ACC:	return m_uListSel ? ABO : HPO;
		case O_DEC:	return m_uListSel ? ABO : HPO;
		case OUTS:	return m_uListSel ? ABO : HDI;
		case OMODE:	return m_uListSel ? ABO : HDI;
		case OTRIG:	return m_uListSel ? ABO : HDI;
		case OCOPYR:	return m_uListSel ? ABO : HPO;
		case STATIO:	return m_uListSel ? ABS : HDI;
		case STATUS:	return m_uListSel ? ABS : HDS;
		case SWE:	return m_uListSel ? ABS : HPO;
		case SWEN:	return m_uListSel ? ABS : HPO;
		case USRDEF:	return m_uListSel ? ABU : HAV;
		case VSCALE:	return m_uListSel ? ABV : HAI;
		case VER:	return m_uListSel ? ABV : HBS;
		}

	return 0;
	}

BOOL CKollmorgen600AddrDialog::NeedDefaultSpace(UINT uHead)
{
	if( IsAlphaHeader(m_uAllowSpace) ) return m_uListSel != SELAL;

	if( IsFunctHeader(m_uAllowSpace) ) return m_uListSel == SELAL;

	return m_uListSel != SELKW;
	}

UINT CKollmorgen600AddrDialog::SelectDefaultSpace(void)
{
	return m_uAllowSpace = (m_uListSel ? ABA : HAV);
	}

BOOL CKollmorgen600AddrDialog::IsAlphaHeader(UINT uSel)
{
	return uSel >= ABA && uSel <= ABZ;
	}

BOOL CKollmorgen600AddrDialog::IsFunctHeader(UINT uSel)
{
	return uSel >= HAV && uSel <= HVE;
	}

BOOL CKollmorgen600AddrDialog::IsHeader(UINT uSel)
{
	return uSel >= HMN;
	}

UINT CKollmorgen600AddrDialog::HeaderSpaceType(CSpace * pSpace)
{
	return GetHeaderSpace(pSpace->m_uTable, pSpace->m_uMaximum);
	}

UINT CKollmorgen600AddrDialog::GetHeaderSpace(UINT uTable, UINT uMax)
{
	if( uTable == AN ) {

		if( uMax >= ABA && uMax <= ABZ ) return	SPAT; // Alpha Header
		if( uMax >= HAV && uMax <= HVE ) return SPFT; // Funct Header
		if( uMax >= LAL && uMax <= LFN ) return SPLT; // List Select Header
		}

	return SPIT; // Not Header
	}

BOOL CKollmorgen600AddrDialog::ListSelectMade(CSpace *pSpace)
{
	UINT uMax = pSpace->m_uMaximum;

	return uMax == LAL || uMax == LFN;
	}

void CKollmorgen600AddrDialog::ClearInfo(UINT uClear)
{
	switch( uClear ) {

		case 2:
			ClearAddress();
			ClearDetails();

		case 1: break;

		default: return;
		}

	GetDlgItem(2002).SetWindowText("");
	GetDlgItem(2002).EnableWindow(FALSE);
	GetDlgItem(2003).SetWindowText("");
	GetDlgItem(2003).EnableWindow(FALSE);
	}

BOOL CKollmorgen600AddrDialog::DoShowAddress(CAddress Addr)
{
	ShowAddress(Addr);

	switch( Addr.a.m_Table ) {

		case ALIAS:
		case DEVICE:
		case ERRCODE:
		case HVER:
		case MDBGET:
		case MDBSET:
		case MNAME:
		case MTMUXR:
		case OCOPYR:
		case PSTATE:
		case VER:
		case USRDEF:
			ClearDetails();
			SetDlgFocus(1001);
			GetDlgItem(3002).SetWindowText("String");
			GetDlgItem(3002).EnableWindow(TRUE);
			return FALSE;
		}

	return TRUE;
	}

// End of File
