
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RCX_HPP

#define	INCLUDE_RCX_HPP

const char * sAxis [] = { "X", "Y", "Z", "R", "A", "B" };

const char * sMove [] = { "P", "L" };

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Device Options
//

class CYamahaRcxDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYamahaRcxDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series TCP/IP Device Options
//

class CYamahaRcxTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYamahaRcxTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_Login;
		UINT m_Echo;

		CString m_User;
		CString m_PWord;
	
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master Driver
//

class CYamahaRcxDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYamahaRcxDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		
		// Access
		BOOL HasRobot(CSpace * pSpace);
		BOOL HasDirection(CSpace * pSpace);
		BOOL IsString(UINT uTable);
		BOOL IsAxis(CSpace * pSpace);
		BOOL IsMove(CSpace * pSpace);
		BOOL IsSpecial(CSpace * pSpace);

	protected:
		

		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master TCP/IP Driver
//

class CYamahaRcxTcpDriver : public CYamahaRcxDriver
{
	public:
		// Constructor
		CYamahaRcxTcpDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:

		// Implementation
		void AddSpaces(void);

	};


//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master Driver Address Selection
//

class CYamahaRcxDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CYamahaRcxDialog(CYamahaRcxDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Implementation

		void	EnableRobot(void);
		void	EnableDirection(void);
		void	EnableGroup(BOOL fEnable, UINT uID);

		// Helpers

		BOOL	HasRobot(void);
		BOOL	HasDirection(void);

	};


#endif

// End of File

