
#include "intern.hpp"

#include "gednc2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GE DNC2 Master
//

// Instantiator

ICommsDriver *	Create_GEDNC2MasterDriver(void)
{
	return New CGEDNC2MasterDriver;
	}

// Constructor

CGEDNC2MasterDriver::CGEDNC2MasterDriver(void)
{
	m_wID		= 0x403E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Fanuc";
	
	m_DriverName	= "DNC2 Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Fanuc DNC2 Master";

	AddSpaces();
	}

// Binding Control

UINT CGEDNC2MasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CGEDNC2MasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Address Management

BOOL CGEDNC2MasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CGEDNC2AddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CGEDNC2MasterDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	if( (pSpace->m_uTable != RPAP) && (pSpace->m_uTable != RPAA) ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	UINT uFind1 = Text.Find('(');

	UINT uFind2 = Text.FindRev(')');

	if( (uFind1 < uFind2 - 1) && (uFind2 < NOTHING) ) {

		UINT uAxis = tatoi(Text.Mid(uFind1 + 1));

		if( uAxis < 16 ) {

			Addr.a.m_Offset = tatoi(Text);
			Addr.a.m_Extra  = uAxis;
			Addr.a.m_Type   = pSpace->m_uType;
			Addr.a.m_Table  = pSpace->m_uTable;

			return TRUE;
			}
		}

	Error.Set( "RPA0(0) - RPA9999(15)",
		0
		);

	return FALSE;
	}

BOOL CGEDNC2MasterDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		switch( Addr.a.m_Table ) {

			case RID:
			case WDIS:
				Text.Printf( "%s", pSpace->m_Prefix );

				return TRUE;

			case RPAP:
			case RPAA:
				Text.Printf( "%s%4.4d(%2.2d)",
					pSpace->m_Prefix,
					Addr.a.m_Offset,
					Addr.a.m_Extra
					);

				return TRUE;

			default:
				return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}
		}

	return FALSE;
	}

// Implementation

void CGEDNC2MasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(RMP,  "RMP",  "Tool Position - Machine",			10,	1,   15,    RR));
	AddSpace(New CSpace(RWP,  "RWP",  "Tool Position - Absolute",			10,	1,   15,    RR));
	AddSpace(New CSpace(RSP,  "RSP",  "Tool Position - Skip Signal Detect",		10,	1,   15,    RR));
	AddSpace(New CSpace(RSE,  "RSE",  "Servo Delay for Axis",			10,	1,   15,    RR));
	AddSpace(New CSpace(RAE,  "RAE",  "Accel / Decel Delay for Axis",		10,	1,   15,    RR));
	AddSpace(New CSpace(RMI,  "RMI",  "Machine Interface Signals",			10,	0,  999,    LL));
	AddSpace(New CSpace(RPN,  "RPN",  "Current Program Number",			10,	0,    0,    LL));
	AddSpace(New CSpace(RSN,  "RSN",  "Current Sequence Number",			10,	0,    0,    LL));
	AddSpace(New CSpace(RPAP, "RPAP", "Read CNC Parameter Value",			10,	0, 9999,    LL));
	AddSpace(New CSpace(RPAA, "RPAA", "CNC Parameter Axis Exponent",		10,	0, 9999,    LL));
	AddSpace(New CSpace(WPA,  "WPA",  "Execute CNC Parameter Write...",		10,	0,    0,    BB));
	AddSpace(New CSpace(WPAP, "WPAP", "  CNC Parameter Value to Write",		10,	0, 9999,    LL));
	AddSpace(New CSpace(WPAA, "WPAA", "  CNC Axis Exponent to Write",		10,	0, 9999,    LL));
	AddSpace(New CSpace(RWPE, "RWPE", "Pitch Error Compensation Value",		10,	0, 9999,    LL));
	AddSpace(New CSpace(RTOD, "RTOD", "Read Tool Offset Cutter Wear",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOK, "RTOK", "Read Tool Offset Cutter Geometry",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOH, "RTOH", "Read Tool Offset Length Wear",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOL, "RTOL", "Read Tool Offset Length Geometry",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOX, "RTOX", "Read Tool Offset X Axis Wear",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOU, "RTOU", "Read Tool Offset X Axis Geometry",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOY, "RTOY", "Read Tool Offset Y Axis Wear",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOV, "RTOV", "Read Tool Offset Y Axis Geometry",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOZ, "RTOZ", "Read Tool Offset Z Axis Wear",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOW, "RTOW", "Read Tool Offset Z Axis Geometry",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOR, "RTOR", "Read Tool Offset Tip Radius Wear",		10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOP, "RTOP", "Read Tool Offset Tip Radius Geometry",	10,	1, 9999,    RR));
	AddSpace(New CSpace(RTOQ, "RTOQ", "Read Tool Offset Virtual Direction",		10,	1, 9999,    RR));
	AddSpace(New CSpace(WTON, "WTON", "Write Tool Offset Number...",		10,	1, 9999,    LL));
	AddSpace(New CSpace(WTOB, "WTOB", "  Select Bit Pattern VPWUQYRZXLHKD(Bit 0)",	10,	0,    0,    WW));
	AddSpace(New CSpace(WTOD, "WTOD", "  Cutter Wear to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOK, "WTOK", "  Cutter Geometry to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOH, "WTOH", "  Length Wear to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOL, "WTOL", "  Length Geometry to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOX, "WTOX", "  X Axis Wear to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOU, "WTOU", "  X Axis Geometry to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOY, "WTOY", "  Y Axis Wear to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOV, "WTOV", "  Y Axis Geometry to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOZ, "WTOZ", "  Z Axis Wear to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOW, "WTOW", "  Z Axis Geometry to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOR, "WTOR", "  Tip Radius Wear to write",			10,	0,    0,    RR));
	AddSpace(New CSpace(WTOP, "WTOP", "  Tip Radius Geometry to write",		10,	0,    0,    RR));
	AddSpace(New CSpace(WTOQ, "WTOQ", "  Virtual Direction to write",		10,	0,    0,    YY));
	AddSpace(New CSpace(RWMV, "RWMV", "Custom Macro Variable Value",		10,	0,65535,    RR));
	AddSpace(New CSpace(RTLL, "RTLL", "Tool Life Value for Group",			10,	0, 9999,    LL));
	AddSpace(New CSpace(RTLQ, "RTLQ", "Tool Life Count for Group",			10,	0, 9999,    LL));
	AddSpace(New CSpace(RTLT, "RTLT", "Tool Number for Group",			10,	0, 9999,    LL));
	AddSpace(New CSpace(RTLH, "RTLH", "Tool Life H Code for Group",			10,	0, 9999,    LL));
	AddSpace(New CSpace(RTLD, "RTLD", "Tool Life D Code for Group",			10,	0, 9999,    LL));
	AddSpace(New CSpace(RTLC, "RTLC", "Tool Information for Group",			10,	0, 9999,    LL));
	AddSpace(New CSpace(RMDG, "RMDG", "Modal Information G for Block",		10,	0,    2,    RR));
	AddSpace(New CSpace(RMDD, "RMDD", "Modal Information D for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDE, "RMDE", "Modal Information E for Block",		10,	0,    2,    RR));
	AddSpace(New CSpace(RMDH, "RMDH", "Modal Information H for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDL, "RMDL", "Modal Information L for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDM, "RMDM", "Modal Information M for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDN, "RMDN", "Modal Information N for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDO, "RMDO", "Modal Information O for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDS, "RMDS", "Modal Information S for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDT, "RMDT", "Modal Information T for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RMDF, "RMDF", "Modal Information F for Block",		10,	0,    2,    LL));
	AddSpace(New CSpace(RAF,  "RAF",  "Feed Rate for Axis",				10,	1,   15,    RR));
	AddSpace(New CSpace(RADI, "RADI", "A/D - General Analog Input",			10,	0,    3,    LL));
	AddSpace(New CSpace(RADS, "RADS", "A/D Load Voltage for Spindle",		10,	1,    2,    LL));
	AddSpace(New CSpace(RADN, "RADN", "A/D Load Voltage for Control Axis",		10,	1,   15,    LL));
	AddSpace(New CSpace(RAL,  "RAL",  "Alarm Information",				10,	0,    0,    WW));
	AddSpace(New CSpace(RST,  "RST",  "Status Information",				10,	0,    0,    LL));
	AddSpace(New CSpace(WSL,  "WSL",  "Select Part Program",			10,	1, 9999,    BB));
	AddSpace(New CSpace(WCS,  "WCS",  "Execute a Program",				10,	0, 9999,    BB));
	AddSpace(New CSpace(WCC,  "WCC",  "Reset",					10,	0,    0,    BB));
	AddSpace(New CSpace(WDI,  "WDI",  "Send WDIS (select 6-10 to clear 1-5 first)",	10,	0,   10,    WW));
	AddSpace(New CSpace(WDIS, "WDIS", "Set Up Operator Message String",		10,	0,    8,    LL));
	AddSpace(New CSpace(RID,  "RID",  "System ID String",				10,	0,    5,    LL));
	AddSpace(New CSpace(ERR,  "NAK",  "Latest NAK (Internal)",			10,	0,    0,    LL));
	}

/////////////////////////////////////////////////////////////////////
//
// GE DNC2 Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CGEDNC2AddrDialog, CStdAddrDialog);
		
// Constructor

CGEDNC2AddrDialog::CGEDNC2AddrDialog(CGEDNC2MasterDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "GEDNC2ElementDlg";
	}

// Message Map

AfxMessageMap(CGEDNC2AddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CGEDNC2AddrDialog)
	};

// Message Handlers

BOOL CGEDNC2AddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	UINT uHasAddr = HasAddr(m_pSpace);

	EnableWindows(uHasAddr);
	
	if( !m_fPart ) {

		SetCaption();

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			ShowDetails();

			SetDlgFocus(uHasAddr ? 2002 : 1001);

			return FALSE;
			}

		return TRUE;
		}
	else {
		FindSpace();

		if( uHasAddr == 2 ) {

			GetDlgItem(2004).SetWindowText("0");
			}

		if( TRUE ) {

			CString Text = m_pSpace->m_Prefix;

			if( m_pSpace->m_uMinimum < m_pSpace->m_uMaximum ) {

				Text += GetAddressText();
				}

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				ShowAddress(Addr);
				}
			}

		SetAddressFocus();

		return FALSE;
		}
	}

// Notification Handlers

void CGEDNC2AddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CGEDNC2AddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		UINT uHasAddr = HasAddr(m_pSpace);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			}

		GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

		ShowDetails();

		SetDlgFocus( uHasAddr ? 2002 : 1001 );

		return;
		}

	m_pSpace = NULL;

	ClearAddress();

	EnableWindows(0);
	}

BOOL CGEDNC2AddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text  = m_pSpace->m_Prefix;

		UINT uHasAddr = HasAddr(m_pSpace);

		switch( uHasAddr ) {

			case 0:
				break;

			case 1:
				Text += GetDlgItem(2002).GetWindowText();
				break;

			case 2:
				Text += GetDlgItem(2002).GetWindowText();
				Text += "(";
				Text += GetDlgItem(2004).GetWindowText();
				Text += ")";
				break;
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( uHasAddr == 2 ? 2002 : 1001 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables

void CGEDNC2AddrDialog::ShowAddress(CAddress Addr)
{
	SetDescText(m_pSpace);

	UINT uHasAddr = HasAddr(m_pSpace);

	if( !uHasAddr ) {

		ClearDetails();

		if( m_pSpace->m_uTable == RID || m_pSpace->m_uTable == WDIS ) {

			GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());
			}

		return;
		}

	CString Text;

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	Text = Text.Mid(m_pSpace->m_Prefix.GetLength());

	UINT uFind  = Text.Find('(');
	
	UINT uFind2 = Text.Find('.');

	SetAddressText(Text.Left(min(uFind, uFind2)));

	if( uHasAddr == 2 ) {

		CString c = Text.Mid(uFind + 1);

		c = c.Left( min(2, c.Find(')')) );

		GetDlgItem(2004).SetWindowText(c);
		}
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);
	}

void CGEDNC2AddrDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	UINT uHasAddr = HasAddr(m_pSpace);

	if( uHasAddr ) {

		CAddress Addr;

		m_pSpace->GetMinimum(Addr);

		m_pDriver->DoExpandAddress(Min, m_pConfig, Addr);

		if( Min[Min.GetLength()-1] == '.' ) Min = Min.Left(Min.GetLength()-1);

		m_pSpace->GetMaximum(Addr);

		if( uHasAddr == 2 ) {

			Addr.a.m_Extra = 15;
			}

		m_pDriver->DoExpandAddress(Max, m_pConfig, Addr);

		if( Max[Max.GetLength()-1] == '.' ) Max = Max.Left(Max.GetLength()-1);

		Rad = m_pSpace->GetRadixAsText();
		}

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

void CGEDNC2AddrDialog::ClearAddress(void)
{
	GetDlgItem(2001).SetWindowText(CString(IDS_DRIVER_NONE));

	GetDlgItem(IDCLEAR).EnableWindow(FALSE);

	EnableWindows(0);
	}

// Helpers

UINT CGEDNC2AddrDialog::HasAddr(CSpace *pSpace)
{
	if( pSpace ) {

		if( pSpace->m_uTable == RPAP || pSpace->m_uTable == RPAA ) {

			return 2;
			}

		if( pSpace->m_uTable == RID || pSpace->m_uTable == WDIS ) {

			return 0;
			}

		return pSpace->m_uMinimum < pSpace->m_uMaximum ? 1 : 0;
		}

	return 0;
	}

void CGEDNC2AddrDialog::EnableWindows(UINT uHasAddr)
{
	if( m_pSpace ) {

		if( uHasAddr ) {

			if( uHasAddr < 2 ) {

				GetDlgItem(2004).SetWindowText("");
				GetDlgItem(2005).SetWindowText("");
				}
	
			GetDlgItem(2002).EnableWindow(TRUE);
			GetDlgItem(2003).EnableWindow(TRUE);
			GetDlgItem(2004).EnableWindow(uHasAddr == 2);
			GetDlgItem(2005).EnableWindow(uHasAddr == 2);

			return;
			}
		}

	GetDlgItem(2002).EnableWindow(FALSE);
	GetDlgItem(2003).EnableWindow(FALSE);
	GetDlgItem(2004).EnableWindow(FALSE);
	GetDlgItem(2005).EnableWindow(FALSE);
	GetDlgItem(2002).SetWindowText("");
	GetDlgItem(2003).SetWindowText("");
	GetDlgItem(2004).SetWindowText("");
	GetDlgItem(2005).SetWindowText("");
	}

void CGEDNC2AddrDialog::SetDescText(CSpace *pSpace)
{
	UINT uA = HasAddr(pSpace);

	UINT uT = pSpace->m_uTable;

	EnableWindows(uA);

	CString Text03 = "";
	CString Text05 = "";

	switch( uA ) {

		case 0:
			return;

		case 1:
			switch( pSpace->m_uMaximum ) {

				case 65535:
					Text03 = "Variable";
					break;

				case 999:
					Text03 = "Diag Number";
					break;

				case 15:
					Text03 = "Axis";
					break;

				case 3:
					Text03 = "Input";
					break;

				case 2:
					Text03 = uT == RADS ? "Spindle" : "Block";
					break;

				case 9999:
					if( (uT >= RTLL) && (uT <= RTLC) ) {

						Text03 = "Group";
						}

					else {
						Text03 = uT == WSL || uT == WCS ? "Program" : "Parameter";
						}
					break;

				case 10:
					Text03 = "Msg #";
					break;

				default:
					break;
				}
			break;

		case 2:
			Text03 = "Parameter";
			Text05 = "Axis";
			break;
		}

	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
	GetDlgItem(2003).SetWindowText(Text03);
	GetDlgItem(2005).SetWindowText(Text05);
	}

/*/////////////////////////////////////////////////////////////////////////
//
// GE DNC2 TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CGEDNC2TCPDeviceOptions, CUIItem);

// Constructor

CGEDNC2TCPDeviceOptions::CGEDNC2TCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 42, 1), MAKEWORD(  168, 192)));

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Management

void CGEDNC2TCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CGEDNC2TCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CGEDNC2TCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
       	}

//////////////////////////////////////////////////////////////////////////
//
// GE DNC2 TCP/IP Driver
//

// Instantiator

ICommsDriver *Create_GEDNC2TCPDriver(void)
{
	return New CGEDNC2TCPDriver;
	}

// Constructor

CGEDNC2TCPDriver::CGEDNC2TCPDriver(void)
{
	m_wID		= 0x3523;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "GE";
	
	m_DriverName	= "DNC2 TCP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "GE DNC2 TCP Master";
	}

// Destructor

CGEDNC2TCPDriver::~CGEDNC2TCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CGEDNC2TCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CGEDNC2TCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CGEDNC2TCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CGEDNC2TCPDeviceOptions);
	}
*/
// End of File
