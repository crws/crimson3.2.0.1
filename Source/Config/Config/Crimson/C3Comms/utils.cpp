
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Utility Functions
//

DLLAPI void C3GetTypeInfo(UINT uType, CTypeDef &Type)
{
	switch( uType ) {

		case addrBitAsBit:
			
			Type.m_Type  = typeLogical;

			Type.m_Flags = flagWritable;

			return;

		case addrBitAsByte:
		case addrByteAsByte:
		case addrBitAsWord:
		case addrByteAsWord:
		case addrWordAsWord:
		case addrBitAsLong:
		case addrByteAsLong:
		case addrWordAsLong:
		case addrLongAsLong:
			
			Type.m_Type  = typeInteger;

			Type.m_Flags = flagWritable;

			return;
		
		case addrBitAsReal:
		case addrByteAsReal:
		case addrWordAsReal:
		case addrLongAsReal:
		case addrRealAsReal:
			
			Type.m_Type  = typeReal;

			Type.m_Flags = flagWritable;

			return;
		}

	AfxAssert(FALSE);
	}

DLLAPI UINT C3GetTypeScale(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrByteAsByte:
		case addrWordAsWord:
		case addrLongAsLong:
		case addrRealAsReal:
		case addrLongAsReal:

			return 1;

		case addrByteAsWord:
		case addrWordAsLong:
		case addrWordAsReal:

			return 2;
		
		case addrByteAsLong:
		case addrByteAsReal:

			return 4;

		case addrBitAsByte:

			return 8;

		case addrBitAsWord:

			return 16;

		case addrBitAsLong:
		case addrBitAsReal:

			return 32;
		}

	AfxAssert(FALSE);

	return 0;
	}

DLLAPI UINT C3GetTypeTableBits(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrBitAsByte:
		case addrBitAsWord:
		case addrBitAsLong:
		case addrBitAsReal:

			return 1;

		case addrByteAsByte:
		case addrByteAsWord:
		case addrByteAsLong:
		case addrByteAsReal:

			return 8;

		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:

			return 16;

		case addrLongAsLong:
		case addrLongAsReal:
		case addrRealAsReal:
			
			return 32;
		}

	AfxAssert(FALSE);

	return 0;
	}

DLLAPI UINT C3GetTypeLocalBits(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:

			return 1;

		case addrBitAsByte:
		case addrByteAsByte:

			return 8;

		case addrBitAsWord:
		case addrByteAsWord:
		case addrWordAsWord:

			return 16;

		case addrBitAsLong:
		case addrByteAsLong:
		case addrWordAsLong:
		case addrLongAsLong:
		case addrBitAsReal:
		case addrByteAsReal:
		case addrWordAsReal:
		case addrLongAsReal:
		case addrRealAsReal:

			return 32;
		}

	AfxAssert(FALSE);

	return 0;
	}

// End of File
