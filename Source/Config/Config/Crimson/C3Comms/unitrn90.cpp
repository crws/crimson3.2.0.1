
#include "intern.hpp"

#include "unitrn90.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unitronics M90 Driver
//

// Instantiator

ICommsDriver *	Create_UnitronicsM90Driver(void)
{
	return New CUnitronicsM90Driver;
	}

// Constructor

CUnitronicsM90Driver::CUnitronicsM90Driver(void)
{
	m_wID		= 0x3388;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Unitronics";
	
	m_DriverName	= "Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Unitronics Master";

	AddSpaces();
	}

// Binding Control

UINT	CUnitronicsM90Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CUnitronicsM90Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

// Address Management

// Address Helpers

// Implementation	

void CUnitronicsM90Driver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "A",  "Output Bit",				10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(2,  "B",  "Memory Bit",				10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(3,  "E",  "Input Bit",				10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(4,  "G",  "System Bit",				10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(5,  "M",  "Counter Scan Bit",			10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(6,  "T",  "Timer Scan Bit",				10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(7,  "MI", "Memory Integer",				10, 0, 9999, addrWordAsWord ));
	AddSpace(New CSpace(8,  "ML", "Memory Long",				10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(9,  "MD", "Memory Double Word",			10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(10, "MR", "Memory Real",				10, 0, 9999, addrRealAsReal ));
	AddSpace(New CSpace(11, "SI", "System Integer",				10, 0, 9999, addrWordAsWord ));
	AddSpace(New CSpace(12, "SL", "System Long",				10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(13, "SD", "System Double Word",			10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(14, "TP", "Timer Preset",				10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(15, "TV", "Timer Value",				10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(16, "CP", "Counter Preset",				10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(17, "CV", "Counter Value",				10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(18, "RT",  "Real Time Clock (0=sec,...,6=year)",	10, 0,    6, addrByteAsByte ));
	}

// End of File