
#include "intern.hpp"

#include "kdn2tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// KEB DIN 2 Space Wrapper
//

// Constructor

CSpaceKEBDN2::CSpaceKEBDN2(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s)
{
	m_uTable	= uTable;
	
	m_Prefix	= p;

	m_Caption	= c;

	m_uRadix	= r;

	m_uMinimum	= n;
	
	m_uMaximum	= x;
	
	m_uType		= t;

	m_uSpan         = s;

	FindWidth();

	FindAlignment();
	}

// Limits

void CSpaceKEBDN2::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	if( m_uTable <= ETD ) {		// Enhanced - increment by set

		Addr.a.m_Extra	= 0x7;

		Addr.a.m_Offset = 0xFFFF;
		}
	
	else if( m_uTable <= STD ) {	// Standard - increment by parameter

		Addr.a.m_Extra  = 1;

		Addr.a.m_Offset = 0xFFFF;
		}
	}


//////////////////////////////////////////////////////////////////////////
//
// KEB DIN2 Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKEBDin2MasterTCPDeviceOptions, CUIItem);       

// Constructor

CKEBDin2MasterTCPDeviceOptions::CKEBDin2MasterTCPDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD( 52, 1), MAKEWORD(  168, 192)));

	m_Port   = 8000;

	m_Unit   = 0;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CKEBDin2MasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CKEBDin2MasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CKEBDin2MasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}
//////////////////////////////////////////////////////////////////////////
//
// KEB DIN 2 TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_KEBDin2MasterTCPDriver(void)
{
	return New CKEBDin2MasterTCPDriver;
	}

// Constructor

CKEBDin2MasterTCPDriver::CKEBDin2MasterTCPDriver(void)
{
	m_wID		= 0x4073;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "KEB";
	
	m_DriverName	= "DIN66019II TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "DIN II Master";

	AddSpaces();
	}

// Destructor

CKEBDin2MasterTCPDriver::~CKEBDin2MasterTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CKEBDin2MasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CKEBDin2MasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CKEBDin2MasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKEBDin2MasterTCPDeviceOptions);
	}

// Implementation

void CKEBDin2MasterTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpaceKEBDN2( ETD,		"EP", "Enhanced Parameter : Set",	16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
   	AddSpace(New CSpaceKEBDN2( STD,		"SP", "Standard Parameter : Set",	16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
   	AddSpace(New CSpaceKEBDN2( LMT | 0,	"LL", "Parameter Lower Limit",		16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( LMT | 1,	"UL", "Parameter Upper Limit",		16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( DEF,		"PD", "Parameter Default",		16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( CHR | 0,	"C1", "Characteristics 1",		16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( CHR | 1,	"C2", "Characteristics 2",		16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( DSP | 0,	"DD", "Display Divisor",		16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( DSP | 1,	"DM", "Display Multiplier",		16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( DSP | 2,	"DO", "Display Offset ",		16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( DSP | 3,	"DF", "Display Flags",			16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
//	AddSpace(New CSpaceKEBDN2( NAM,		"PN", "Parameter Name",			16, 0,  0xFFFF, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( PLI | 0,	"ID", "Index Display",			16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( PLI | 1,	"ND", "Number Display",			16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( PLI | 2,	"IC", "Index Combivis",			16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( PLI | 3,	"NC", "Number Combivis",		16, 0,  0xFFFF, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( PD1 | 0,	"LI", "Process Data In (32-bit)",	16, 1,       2, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( PD1 | 1,	"LO", "Process Data Out (32-bit)",	16, 1,       2, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceKEBDN2( PD1 | 2,	"SL", "Send Process Data Out (32-bit)", 16, 0,       0,   addrBitAsBit, addrBitAsBit  ));
	AddSpace(New CSpaceKEBDN2( PD2 | 0,	"WI", "Process Data In (16-bit)",	16, 1,	     4, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( PD2 | 1,	"WO", "Process Data Out (16-bit)",	16, 1,	     4, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceKEBDN2( PD2 | 2,	"SW", "Send Process Data Out (16-bit)", 16, 0,       0,   addrBitAsBit, addrBitAsBit  ));
	AddSpace(New CSpaceKEBDN2( ERR,		"LE", "Latest Error Code",		16, 0, 	     0, addrLongAsLong, addrLongAsLong));
	}

// Address Management

BOOL CKEBDin2MasterTCPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CKEBDn2Dialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CKEBDin2MasterTCPDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uSet = 0;
				  
	if( IsSet(pSpace->m_uTable) ) {

		UINT uFind = Text.Find(':');

		if( uFind < NOTHING ) {

			uSet = tatoi(Text.Mid(uFind + 1));

			Text   = Text.Mid(0, uFind);
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		if( IsSet(Addr.a.m_Table) ) {

			if( Addr.a.m_Table == ETD ) {

				Addr.a.m_Extra = (Addr.a.m_Offset & 0xF000) >> 13;

				Addr.a.m_Offset = (Addr.a.m_Offset << 3);

				Addr.a.m_Offset |= uSet;
				}

			else if( Addr.a.m_Table == STD ) {

				Addr.a.m_Extra	= uSet;
				}
			}

		return TRUE; 
		}

	return FALSE;
	}

BOOL CKEBDin2MasterTCPDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uType   = Addr.a.m_Type;

		UINT uOffset = Addr.a.m_Offset;

		UINT uExtra  = Addr.a.m_Extra;

		if( IsSet(pSpace->m_uTable) ) {

			if( pSpace->m_uTable == ETD ) {

				uOffset = (Addr.a.m_Extra << 13) | (Addr.a.m_Offset >> 3);

				uExtra  = Addr.a.m_Offset & 0x7;
				}

			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%4.4X:%1.1X", 
					     pSpace->m_Prefix, 
					     uOffset,
					     uExtra
					     );
				}
	
			else {
				Text.Printf( "%s%4.4X:%1.1X.%s", 
					     pSpace->m_Prefix, 
					     uOffset,
					     uExtra,
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}
		else {
			if( uType == pSpace->m_uType ) {

				Text.Printf( "%s%4.4X", 
					     pSpace->m_Prefix, 
					     uOffset
					     );
				}
			else {
				Text.Printf( "%s%4.4X.%s", 
					     pSpace->m_Prefix, 
					     uOffset,
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}

		return TRUE;
		}
	
	return FALSE;
	}

// Access

BOOL CKEBDin2MasterTCPDriver::IsSet(UINT uTable)
{
	return (uTable <= STD);
	}

BOOL CKEBDin2MasterTCPDriver::IsCmd(UINT uTable)
{
	switch( uTable ) {

		case PD1 | 2:
		case PD2 | 2:
		case ERR:

			return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// KEB DIN 2 Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CKEBDn2Dialog, CStdAddrDialog);
		
// Constructor

CKEBDn2Dialog::CKEBDn2Dialog(CKEBDin2MasterTCPDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "KEBDN2ElementDlg";
	}

// Overridables

void CKEBDn2Dialog::SetAddressFocus(void)
{
	CKEBDin2MasterTCPDriver * pDriver = (CKEBDin2MasterTCPDriver *) m_pDriver;
	
	if( m_pSpace && pDriver->IsSet(m_pSpace->m_uTable) ) {

		SetDlgFocus(2005);

		return;
		}
	
	SetDlgFocus(2002);
	}

void CKEBDn2Dialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	CKEBDin2MasterTCPDriver * pDriver = (CKEBDin2MasterTCPDriver *) m_pDriver;
	
	if( m_pSpace && pDriver->IsSet(m_pSpace->m_uTable) ) {

		if( fEnable ) {

			UINT uFind = Text.Find(':');

			GetDlgItem(2002).SetWindowText(Text.Left(uFind));

			GetDlgItem(2004).SetWindowText(":");

			GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fEnable);
		
		GetDlgItem(2005).EnableWindow(fEnable);
		}
	else {
		if( m_pSpace ) {

			if( pDriver->IsCmd(m_pSpace->m_uTable) ) {

				fEnable = FALSE;

				Text = "";
				}
			}
		
		GetDlgItem(2002).SetWindowText(Text);

		GetDlgItem(2004).SetWindowText("");

		GetDlgItem(2005).SetWindowText("");
	
		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		}
	}

CString CKEBDn2Dialog::GetAddressText(void)
{
	CKEBDin2MasterTCPDriver * pDriver = (CKEBDin2MasterTCPDriver *) m_pDriver;
	
	if( m_pSpace && pDriver->IsSet(m_pSpace->m_uTable) ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += ":";
		
		Text += GetDlgItem(2005).GetWindowText();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}
 	  
// End of File
