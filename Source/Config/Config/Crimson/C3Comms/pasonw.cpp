
#include "intern.hpp"

#include "pasonw.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Pason WITS
//

// Instantiator

ICommsDriver * Create_PasonWITSDriver(void)
{
	return New CPasonWITSDriver;
	}

// Constructor

CPasonWITSDriver::CPasonWITSDriver(void)
{
	m_wID		= 0x405C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= TEXT("Pason");
	
	m_DriverName	= TEXT("WITS");
	
	m_Version	= TEXT("1.00");
	
	m_ShortName	= TEXT("Pason WITS");

	m_DevRoot	= TEXT("Dev");

	AddSpaces();
	}

// Binding Control

UINT	CPasonWITSDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CPasonWITSDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS	CPasonWITSDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Address Helpers

// Implementation

void	CPasonWITSDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "CR", "WITS Codes, Real Input",		10, 100,  9999, addrRealAsReal));
	AddSpace(New CSpace(2, "CI", "WITS Codes, Integer Input",	10, 100,  9999, addrLongAsLong));
	AddSpace(New CSpace(3, "DT", "Delay Time in ms.",		10,   0,     0, addrWordAsWord));
	AddSpace(New CSpace(4, "PR", "Prefix PASON1984/EDR",		10,   0,     0, addrBitAsBit  ));
	AddSpace(New CSpace(5, "SR", "String Data Received",		10,   0,    39, addrLongAsLong));
	}

// End of File
