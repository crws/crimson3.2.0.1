
#include "intern.hpp"

#include "yask3iec.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaMp3000IecDeviceOptions, CYaskawaMPIECDeviceOptions);

// Constructor

CYaskawaMp3000IecDeviceOptions::CYaskawaMp3000IecDeviceOptions(void)
{
	m_IXHead	= 73728;

	m_QXHead	= 73728;

	m_IBHead	= 483328;

	m_QBHead	= 73792;

	m_QHead		= 483328;

	m_MemWordSwap	= 0;
	}

// Device Access

CString CYaskawaMp3000IecDeviceOptions::GetDeviceName(void)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pData = pItem->FindMetaData(TEXT("Name"));

	return pData ? pData->ReadString(pItem) : TEXT("Device");
	}

// Import Operations

BOOL CYaskawaMp3000IecDeviceOptions::ImportTags(FILE *pFile, CString Path, IMakeTags *pTags, CString &Error)
{
	if( pTags ) {

		CString File = FindFilename(Path);

		if( pTags->AddRootFolder(L"", GetDeviceName(), L"") ) {

			afxThread->SetWaitMode(TRUE);

			BOOL fFlag	= FALSE;

			char sLine[512] = {0};

			while ( !feof(pFile) ) {

				if( fgets(sLine, sizeof(sLine), pFile) ) {

					CString Line = sLine;

					if( Line.StartsWith(PCTXT(L"[Flag")) ) {

						fFlag = TRUE;
						}

					else if( Line.StartsWith(PCTXT(L"[Numeric")) ) {

						fFlag = FALSE;
						}

					else if( Line.Find(PCTXT(L",[")) < NOTHING ) {

						fFlag ? MakeFlag(sLine, pTags, Error) : MakeNumeric(sLine, pTags, Error);
						}
					}
				}

			pTags->EndFolder();

			afxThread->SetWaitMode(FALSE);
			}
		}

	return TRUE;
	}

CString CYaskawaMp3000IecDeviceOptions::FindFilename(CString Path)
{
	CString File = Path;

	UINT uBegin = 0;

	while( File.Find('\\') < NOTHING ) {

		uBegin = File.Find('\\');

		File = File.Mid(uBegin + 1);
		}

	uBegin = 0;
			
	while( File.Find('/') < NOTHING ) {

		uBegin = File.Find('/');

		File = File.Mid(uBegin + 1);
		}

	UINT uEnd   = File.Find('.');

	if( uEnd < NOTHING ) {

		File = File.Mid(0, uEnd);
		}

	Validate(File);

	return File;
	}

// Download Support

BOOL CYaskawaMp3000IecDeviceOptions::MakeInitData(CInitData &Init)
{
	CYaskawaMPIECDeviceOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_MemWordSwap));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaMp3000IecDeviceOptions::AddMetaData(void)
{
	CYaskawaMPIECDeviceOptions::AddMetaData();

	Meta_AddInteger(MemWordSwap);
	}


// Overridables

void CYaskawaMp3000IecDeviceOptions::SetIECMax(void)
{
	m_IXHigh = m_IXHead + 512 / 8; 

	m_QXHigh = m_QXHead + 512 / 8;

	m_QBHigh = m_QBHead + 20000;

	m_IBHigh = m_IBHead + 20000;

	m_QHigh  = m_QHead  + 20000;
	}

// Tag Creation

void CYaskawaMp3000IecDeviceOptions::MakeFlag(CString Text, IMakeTags * pTags, CString &Error)
{
	CStringArray Array;

	Text.Tokenize(Array, ',');

	CString Name = Array[0];

	CString Coil = GetMappingText(Array);

	UINT    uMap = tstrtol(PCTXT(Coil), NULL, 10);

	Validate(Name);

	CString Tag  = L'[' + ModbusToIEC(Coil, Array[4]) + L']';

	if( !pTags->AddFlag(Name, Name, Tag, 0, 3 | ((uMap %8) << 8), !(Tag.GetAt(1) == 'Q'), L"", L"") ) {

		Error += GetErrorText(Name, Array[0]);
		}
	}

void CYaskawaMp3000IecDeviceOptions::MakeNumeric(CString Text, IMakeTags * pTags, CString &Error)
{
	CStringArray Array;

	Text.Tokenize(Array, ',');

	CString Name	= Array[0];

	CString Numeric = GetMappingText(Array);

	Validate(Name);

	CString Tag  = L'[' + ModbusToIEC(Numeric, Array[4]) + L']';

	BOOL fWrite = !(Tag.GetAt(1) == 'Q');

	if( Numeric.EndsWith(L"REAL") || Array[4].EndsWith(L"REAL") ) {

		if( !pTags->AddReal(Name, Name, Tag, 0, fWrite, L"", L"", 0) ) { 

			Error += GetErrorText(Name, Array[0]);
			}

		return;
		}

	if( !pTags->AddInt(Name, Name, Tag, 0, fWrite, L"", L"", 0, 3) ) { 

		Error += GetErrorText(Name, Array[0]);
		}
	}

// Helpers

CString CYaskawaMp3000IecDeviceOptions::GetMappingText(CStringArray Array)
{
	CString Text = Array[1];

	if( !Text.IsEmpty() ) {

		UINT uFind = Text.Find('.');

		if( uFind < NOTHING ) {

			Text = Text.Mid(uFind + 1);

			uFind = Text.Find(']');

			if( uFind < NOTHING ) {

				return Text.Mid(0, uFind);
				}
			}
		}
	
	return CString("");
	}

CString	CYaskawaMp3000IecDeviceOptions::GetErrorText(CString Name, CString Tag)
{
	CString Error = Tag;

	if( Error.GetLength() > 35 ) {

		Error = Error.Mid(0, 32);

		Error += CString("...");
		}

	Error += CString("\t");

	Error += Name.GetLength() > 32 ? CPrintf(IDS_ERR_TOO_LONG, 32) : CPrintf(IDS_ERR_EXIST_AS, Name);

	Error += CString("\r\n%c");
	
	Error.Printf(Error, ',');

	return Error;
	}

void CYaskawaMp3000IecDeviceOptions::Validate(CString &String)
{
	UINT uCount = String.GetLength();

	for( UINT u = 0; u < uCount; u++ ) {

		TCHAR c = String.GetAt(u);

		if( u == 0 && isdigit(c) ) {

			String.Replace(c, '_');
			}

		else if( !isalnum(c) || isspace(c) ) {

			String.Replace(c, '_');
			}
		}
	}

CString CYaskawaMp3000IecDeviceOptions::ModbusToIEC(CString Modbus, CString Desc)
{
	UINT uModbus = tstrtol(PCTXT(Modbus), NULL, 10);

	UINT uOffset = uModbus % 100000 - 1;

	CString Map;

	if( uModbus < 100000 && uOffset / 8 + m_IXHead <= m_IXHigh ) {	

		Map.Printf("IX%u_%1.1u", uOffset / 8 + m_IXHead, uOffset % 8);

		return Map;
		}
	
	if( uModbus >= 100000 && uModbus < 200000 && uOffset / 8 + m_QXHead <= m_QXHigh ) {	

		Map.Printf("QX%u_%1.1u", uOffset / 8 + m_QXHead, uOffset % 8);

		return Map;
		}

	if( uModbus >= 300000 && uModbus < 400000 && uOffset + m_QBHead <= m_QBHigh ) {	

		Map	= "Q";

		uOffset = uOffset * 2 + m_QBHead;
		}
		
	else if( uModbus >= 400000 && uModbus < 500000 && uOffset + m_IBHead <= m_IBHigh ) {

		Map	= "M";

		uOffset = uOffset * 2 + m_IBHead;
		}

	if( Map.IsEmpty() ) {

		return Map;
		}

	CString Numeric;

	Numeric.Printf("%u", uOffset);

	CString Type;

	UINT uFind = Modbus.Find('.');

	if( uFind < NOTHING ) {

		Type = Modbus.Mid(uFind + 1);
		}

	if( Type.IsEmpty() ) {

		if( Desc == L"LREAL" ) {

			Map += "L";

			Map += Numeric;

			return Map;
			}
		}
		
	if( Type == L"REAL" ) {

		Map += "D";

		Map += Numeric;

		return Map;
		}

	Map += "W";

	Map += Numeric;

	if( Type.StartsWith(L"L") ) {

		Map += ".LONG";
		}

	return Map;
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaMp3000IecDriver(void)
{
	return New CYaskawaMp3000IecDriver;
	}

// Constructor

CYaskawaMp3000IecDriver::CYaskawaMp3000IecDriver(void)
{
	m_wID		= 0x40D7;

	m_uType		= driverMaster;
	
	m_Manufacturer	= TEXT("Yaskawa");
	
	m_DriverName	= TEXT("TCP/IP MP3000iec Series");
	
	m_Version	= TEXT("1.00");
	
	m_ShortName	= TEXT("Yaskawa MP3000iec");

	m_DevRoot	= TEXT("MP");

	m_fIsQ		= FALSE;

	DeleteAllSpaces();

	AddSpaces();  
	}

// Configuration

CLASS CYaskawaMp3000IecDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaMp3000IecDeviceOptions);
	}

// Driver Data

UINT CYaskawaMp3000IecDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagImport;
	}

// Tag Import

BOOL CYaskawaMp3000IecDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	CYaskawaMp3000IecDeviceOptions * pOpt = (CYaskawaMp3000IecDeviceOptions *)pConfig;

	if( pOpt ) {

		COpenFileDialog Dlg;

		Dlg.LoadLastPath(TEXT("MP3xxx Files"));

		Dlg.SetCaption(CPrintf("Yaskawa Import for %s", pOpt->GetDeviceName()));

		Dlg.SetFilter(TEXT("CSV Files (*.csv)|*.csv"));

		if( Dlg.Execute(*afxMainWnd) ) {

			FILE *pFile;

			CString Text = "";
		
			if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

				Text = CString(IDS_UNABLE_TO_OPEN);

				afxMainWnd->Error(Text);
				}
			else {
				CString Path = Dlg.GetFilename();

				CString File = pOpt->FindFilename(Path);

				File.ToUpper();

				CString Error = "";
				
				if( Dlg.GetFilename().EndsWith(TEXT(".csv")) ) {
			
					pOpt->ImportTags(pFile, Path, pTags, Error);
					}
				else {
					Text = CString(IDS_UNSUPPORTED_FILE);
					}

				Dlg.SaveLastPath(TEXT("MP3xxx Files"));
			
				fclose(pFile);

				if( !Text.IsEmpty() ) {

					afxMainWnd->Error(Text);
					}

				if( !Error.IsEmpty() ) {

					CStringArray List;

					Error.Tokenize(List, ',');

					CYaskawaMpImportErrorDialog Dlg(List);

					Dlg.Execute();
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Address Management

BOOL CYaskawaMp3000IecDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CYaskawaMp3000IecAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CYaskawaMp3000IecDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Overridables

BOOL CYaskawaMp3000IecDriver::Is64Bit(UINT uTable)
{
	switch( uTable ) {

		case SP_10:
		case SP_M64:
		
			return TRUE;
		}

	return CYaskawaMPIECDriver::Is64Bit(uTable);
	}

UINT CYaskawaMp3000IecDriver::IECToModbus(CAddress Addr, UINT uMin, BOOL fIsBit)
{
	UINT uOffset = Addr.a.m_Offset - (uMin & 0xFFFF);

	UINT uExtra  = Addr.a.m_Extra & 7;

	if( fIsBit ) {

		return (uOffset * 8) + uExtra;
		}

	return uOffset / 2;
	}

UINT CYaskawaMp3000IecDriver::GetIECMinByteNumber(UINT uTable)
{
	if( !IsIEC(uTable) ) return 0;

	switch( uTable ) {

		case SP_MB:	
		case SP_M32:	
		case SP_M64:	return m_uIB;
		}

	return CYaskawaMPIECDriver::GetIECMinByteNumber(uTable);;
	}

UINT CYaskawaMp3000IecDriver::GetIECMaxByteNumber(CAddress Addr)
{
	switch( Addr.a.m_Table ) {

		case SP_Q32:	
		case SP_Q64:	return m_uQBHigh;	
		
		case SP_MB:	
		case SP_M32:	
		case SP_M64:	return GetIBMax();	

		case SP_9:	return 0xFFFE;

		case SP_10:	return 0xFFFC;
		}	

	return CYaskawaMPIECDriver::GetIECMaxByteNumber(Addr);
	}

UINT CYaskawaMp3000IecDriver::GetIBMax(void)
{
	return m_uIB + 10000 * 2;
	}

// Address Helpers

UINT CYaskawaMp3000IecDriver::GetOffset(UINT uAddress, UINT uTable, BOOL fNative)
{
	UINT uOffset = NOTHING;

	UINT uModbus = NOTHING;

	UINT uNative = NOTHING;

	UINT uLimit  = 0;

	switch( uTable ) {

		case SP_IX:	uModbus = modIX;	
				uNative = m_uIX * 8;	
				uLimit  = 512;
				break;

		case SP_QX:	uModbus = modQX;
				uNative = m_uQX * 8;
				uLimit  = 512;
				break;

		case SP_QB:
		case SP_Q32:
		case SP_Q64:	uModbus = modQ;	
				uNative = m_uQB;	
				uLimit  = 10000 * 2;
				break;

		case SP_MB:
		case SP_M32:
		case SP_M64:	uModbus = modM;	
				uNative = m_uIB;
				uLimit  = 10000 * 2;
				break;
			
		}

	if( uModbus < NOTHING ) {

		UINT uBase  = fNative ? uModbus : uNative;

		UINT uBegin = fNative ? uNative : uModbus;

		if( uAddress >= uBase && uAddress < uBase + uLimit ) {

			uOffset = uAddress - uBase;

			if( !IsBit(uTable) ) {

				uOffset = fNative ? uOffset * 2 : uOffset / 2;
				}
			
			uOffset += uBegin;
			}
		}

	return uOffset;
	}

// Implementation

void CYaskawaMp3000IecDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SP_IX,	"IX",	"Input  Bit/Memory Location",		10,	0,   512, BB));
	AddSpace(New CSpace(SP_QX,	"QX",	"Output Bit/Memory Location",		10,	0,   512, BB));
	AddSpace(New CSpace(SP_QB,	"QW",	"Output Word",				10,	0, 10000, WW, LL));
	AddSpace(New CSpace(SP_MB,	"MW",	"Memory Word",				10,	0, 10000, WW, LL));
	AddSpace(New CSpace(SP_Q32,	"QD",	"Real:  Output Double Word",		10,	0, 10000, RR));
	AddSpace(New CSpace(SP_Q64,	"QL",	"LReal: Output Long Word",		10,	0, 10000, RR));
	AddSpace(New CSpace(SP_M32,	"MD",	"Real:  Memory Double Word",		10,	0, 10000, RR));
	AddSpace(New CSpace(SP_M64,	"ML",	"LReal: Memory Long Word",		10,	0, 10000, RR));

	AddSpace(New CSpace(SP_0,	"0",	"MB Coils",				10,     1,  0xFFFF, BB));
	AddSpace(New CSpace(SP_1,	"1",	"MB Inputs",				10,     1,  0xFFFF, BB));
	AddSpace(New CSpace(SP_3,	"3",	"MB Input Registers",			10,     1,  0xFFFF, WW, LL));
	AddSpace(New CSpace(SP_4,	"4",	"MB Holding Registers",			10,     1,  0xFFFF, WW, LL));
	AddSpace(New CSpace(SP_5,	"QDM",	"MB Real: Input Register 32 Bit",	10,     1,  0xFFFE, RR));
	AddSpace(New CSpace(SP_6,	"QLM",	"MB Real: Input Register 64 Bit",	10,     1,  0xFFFC, RR));
	AddSpace(New CSpace(SP_9,	"MDM",	"MB Real: Holding Register 32 Bit",	10,     1,  0xFFFE, RR));
	AddSpace(New CSpace(SP_10,	"MLM",	"MB Real: Holding Register 64 Bit",	10,     1,  0xFFFC, RR));
	}

// Overridables

UINT CYaskawaMp3000IecDriver::GetBitMax(UINT uMin)
{
	return uMin + ( 512 / 8 ) - 1;
	}

UINT CYaskawaMp3000IecDriver::GetWordMax(UINT uMin)
{
	return uMin + 10000 * 2 - 1;
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP3000 Series IEC Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CYaskawaMp3000IecAddrDialog, CYaskawaMPIECAddrDialog);
		
// Constructor

CYaskawaMp3000IecAddrDialog::CYaskawaMp3000IecAddrDialog(CYaskawaMp3000IecDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CYaskawaMPIECAddrDialog(Driver, Addr, pConfig, fPart)
{
	
	}

// Message Map

AfxMessageMap(CYaskawaMp3000IecAddrDialog, CYaskawaMPIECAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(2008, OnButtonClicked)
	AfxDispatchCommand(2009, OnButtonClicked)

	AfxMessageEnd(CYaskawaMp3000IecAddrDialog)
	};
	
// Command Handlers

BOOL CYaskawaMp3000IecAddrDialog::OnButtonClicked(UINT uID)
{
	if( !m_pSpace ) {

		return TRUE;
		}

	if( !IsIEC(m_pSpace->m_uTable) ) {

		return TRUE;
		}

	if( uID != 2008 && uID != 2009 ) {

		return TRUE;
		}

	UpdateAddressField(uID == 2008 ? 2002 : 2007);
	
	return TRUE;
	}

// Implementation

void CYaskawaMp3000IecAddrDialog::UpdateAddressField(UINT uID)
{
	if( uID != 2007 && uID != 2002 ) {

		return;
		}

	CYaskawaMp3000IecDriver * pDriver = (CYaskawaMp3000IecDriver *) m_pDriver;

	if( pDriver ) {

		CString Native = GetDlgItem(2002).GetWindowText();

		UINT uNative   = tstrtol(Native, NULL, 10);

		UINT uFind     = Native.Find('_');

		if( uFind < NOTHING ) {

			uNative *= 8;

			uNative += tstrtol(Native.Mid(uFind + 1), NULL, 10);
			}

		UINT uModbus = tstrtol(GetDlgItem(2007).GetWindowText(), NULL, 10);

		BOOL fNative = uID == 2007;

		UINT uAddr   = fNative ? uModbus : uNative;

		UINT uOffset = pDriver->GetOffset(uAddr, m_pSpace->m_uTable, fNative);

		if( uOffset < NOTHING ) {

			fNative ? ShowNativeOffset(uOffset, m_pSpace->m_uTable) : ShowModbusOffset(uOffset);
			}
		else {
			uAddr   = fNative ? uNative : uModbus;

			uOffset = pDriver->GetOffset(uAddr, m_pSpace->m_uTable, !fNative);

			if( uOffset < NOTHING ) {

				fNative ? ShowModbusOffset(uOffset) : ShowNativeOffset(uOffset, m_pSpace->m_uTable);
				}
			}
		}
	}

void CYaskawaMp3000IecAddrDialog::ShowNativeOffset(UINT uOffset, UINT uTable)
{
	CYaskawaMp3000IecDriver * pDriver = (CYaskawaMp3000IecDriver *) m_pDriver;

	if( pDriver ) {

		CString s;

		if( pDriver->IsBit(uTable) ) {

			s.Printf(TEXT("%u_%1.1u"), uOffset / 8, uOffset % 8);
			}
		else {
			s.Printf(TEXT("%u"), uOffset);
			}	

		GetDlgItem(2002).SetWindowText(s);
		}
	}

void CYaskawaMp3000IecAddrDialog::ShowModbusOffset(UINT uOffset)
{
	CString s;

	s.Printf(TEXT("%6.6u"), uOffset);
	
	GetDlgItem(2007).SetWindowText(s);
	}

// Overridables

void CYaskawaMp3000IecAddrDialog::ShowModbus(CAddress Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	if( IsIEC(Addr.a.m_Table) ) {

		Address.m_Ref += 1;
		}

	CYaskawaMPIECAddrDialog::ShowModbus(Address);
	}

void CYaskawaMp3000IecAddrDialog::ShowInfo(UINT uTable)
{
	GetDlgItem(5000).SetWindowText(TEXT(""));
	}
	

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Import Error List
//

// Runtime Class

AfxImplementRuntimeClass(CYaskawaMpImportErrorDialog, CStdDialog);
		
// Constructor

CYaskawaMpImportErrorDialog::CYaskawaMpImportErrorDialog(CStringArray List)
{
	m_Errors = List;

	SetName(L"MpImportErrorDlg");
	}

// Message Map

AfxMessageMap(CYaskawaMpImportErrorDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(1003, OnCopy)
	
	AfxMessageEnd(CYaskawaMpImportErrorDialog)
	};
 
// Message Handlers

BOOL CYaskawaMpImportErrorDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadErrorList();
		
	GetDlgItem(1002).SetWindowText(TEXT("The following Import error(s) have occurred."));

	return TRUE;
	}

// Command Handlers

BOOL CYaskawaMpImportErrorDialog::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CYaskawaMpImportErrorDialog::OnCopy(UINT uID)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("MP3xxx Files"));

	Dlg.SetCaption(CPrintf("MP Import Errors"));

	Dlg.SetFilter(TEXT("TEXT Files (*.txt)|*.txt|CSV Files (*.csv)|*.csv"));

	if( Dlg.Execute(*afxMainWnd) ) {

		FILE *pFile;

		CString Text = "";
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			Text = CString(IDS_UNABLE_TO_OPEN_2);

			afxMainWnd->Error(Text);
			}
		else {
			AfxAssume(pFile);

			CString Path = Dlg.GetFilename();
			
			CString Error = "";

			BOOL fCSV = Dlg.GetFilename().EndsWith(TEXT(".csv"));
			
			UINT uCount = m_Errors.GetCount();

			for( UINT u = 0; u < uCount; u++ ) {

				CString Text = m_Errors.GetAt(u);  

				if( fCSV ) {

					Text.Replace('\t', ',');

					Text.Remove('\r');
					}

				fprintf(pFile, TEXT("%s"), PCTXT(Text));
				}

			Information(CString(IDS_COPY_IS_COMPLETE));
			
			fclose(pFile);
			}
		}

	return TRUE;
	}

// Implementation

void CYaskawaMpImportErrorDialog::LoadErrorList(void)
{
	if( !m_Errors.IsEmpty() ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		Box.ResetContent();

		int nTab[] = { 150 };

		Box.SetTabStops(elements(nTab), nTab);

		Box.AddStrings(m_Errors);
		}
	}

// End of File
