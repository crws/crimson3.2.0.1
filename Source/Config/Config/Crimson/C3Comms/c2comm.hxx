
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C2COMM_HXX
	
#define	INCLUDE_C2COMM_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include "c3comms.hxx"

// End of File

#endif
