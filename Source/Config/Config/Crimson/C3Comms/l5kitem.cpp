
#include "intern.hpp"

#include "l5keip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Base Item List
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kList, CNamedList);

// Constructor

CABL5kList::CABL5kList(void)
{
	m_uDepth = 0;
	}

// Destructor

CABL5kList::~CABL5kList(void)
{
	Kill();
	}

// Item Access

CABL5kItem * CABL5kList::GetItem(INDEX Index) const
{
	return (CABL5kItem *) CNamedList::GetItem(Index);
	}

CString CABL5kList::GetName(INDEX Index) const
{
	return GetItem(Index)->GetName();
	}

CString CABL5kList::GetType(INDEX Index) const
{
	return GetItem(Index)->GetType();
	}

CABL5kItem * CABL5kList::GetItem(UINT uPos) const
{
	return (CABL5kItem *) CItemList::GetItem(uPos);
	}

// Item Location

CABL5kItem * CABL5kList::FindItem(CString const &Name) const
{
	UINT p1;

	if( (p1 = Name.Find('.')) < NOTHING ) {

		CString Root = Name.Left(p1);

		BOOL fArray = FALSE;

		UINT p2;

		if( (p2 = Root.Find('[')) < NOTHING ) {

			fArray = TRUE;
			}
		else {
			fArray = FALSE;
			}		
		
		INDEX Index = FindName(fArray ? Root.Left(p2) : Root);

		if( !Failed(Index) ) {
			
			CABL5kItem *pItem = GetItem(Index);

			if( pItem && pItem->IsStruct() ) {

				CABL5kList *pList = ((CABL5kStructItem *) pItem)->m_pElements;
				
				if( fArray ) {

					UINT uPos = tatoi(Root.Mid(p2 + 1, p1 - p2));
				
					CABL5kItem *pItem = pList->GetItem(uPos);

					if( pItem && pItem->IsStruct() ) {

						pList = ((CABL5kStructItem *) pItem)->m_pElements;
						}
					else
						AfxAssert(FALSE);
					}

				return pList->FindItem(Name.Mid(p1 + 1));
				}

			return pItem;
			}
		}
	else {
		INDEX Index;
		
		UINT p2;

		if( (p2 = Name.Find('[')) < NOTHING ) {
			
			Index = FindName(Name.Left(p2));
			}
		else
			Index = FindName(Name);

		if( !Failed(Index) ) {

			return GetItem(Index);
			}
		}

	return NULL;
	}

// Debug

void CABL5kList::ShowList(CABL5kList *pList)
{
	INDEX Index = pList->GetHead();

	while( !pList->Failed(Index) ) {

		CABL5kItem *pItem = pList->GetItem(Index);

		if( pItem->IsStruct() ) {

			for( UINT n = 0; n <= m_uDepth; n ++ )
				AfxTrace(TEXT("-"));
			
			m_uDepth ++;

			AfxTrace(TEXT("[%s]<%s>\n"), pItem->GetName(), pItem->GetType());

			ShowList(pItem->GetElements());

			m_uDepth --;
			}

		if( pItem->IsAtomic() ) {

			AfxTrace(TEXT("[%s] atomic\n"), pItem->GetName());
			}

		pList->GetNext(Index);
		}
	}

// Overridable

BOOL CABL5kList::LoadConfig(CError &Error, CABL5kController &Config)
{
	return FALSE;
	}

// Implementation

void CABL5kList::ThrowError(void)
{
	AfxThrowUserException();
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Base Item
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kItem, CNamedItem);

// Constructor

CABL5kItem::CABL5kItem(void)
{
	}

// Attributes

BOOL CABL5kItem::IsAtomic(void)
{
	return !IsStruct();
	}

BOOL CABL5kItem::IsStruct(void)
{
	return FindMetaData(TEXT("Elements")) != NULL;
	}

BOOL CABL5kItem::IsArray(void)
{
	CString Type = GetType();

	UINT p1;

	if( (p1 = Type.FindRev('.')) == NOTHING) {
		
		p1 = 0;
		}

	return Type.Find('[', p1) < NOTHING;
	}

// Overridables

CString CABL5kItem::GetName(void)
{
	return m_Name;
	}

CString CABL5kItem::GetType(void)
{
	return m_Type;
	}

CABL5kList * CABL5kItem::GetElements(void)
{
	return NULL;
	}

// Meta Data Creation

void CABL5kItem::AddMetaData(void)
{
	CNamedItem::AddMetaData();

	Meta_AddString(Type);
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Item
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kAtomicItem, CABL5kItem);

// Constructor

CABL5kAtomicItem::CABL5kAtomicItem(void)
{
	}

// Persistance

void CABL5kAtomicItem::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Addr" ) {

			Tree.GetValueAsInteger();
			
			SetDirty();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Structure Item
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kStructItem, CABL5kItem);

// Constructor

CABL5kStructItem::CABL5kStructItem(void)
{
	}

// Overridables

CABL5kList * CABL5kStructItem::GetElements(void)
{
	return m_pElements;
	}

// Operations

CABL5kItem * CABL5kStructItem::AddMember(CABL5kItem *pItem)
{
	m_pElements->AppendItem(pItem);

	return pItem;
	}

// Meta Data Creation

void CABL5kStructItem::AddMetaData(void)
{
	CABL5kItem::AddMetaData();

	Meta_AddCollect(Elements);
	}

// End of File
