
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KEBDIN2_HPP
	
#define	INCLUDE_KEBDIN2_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CKEBDIN2Driver;
class CKEBDIN2Dialog;

#define	AN	0xF0

#define KNOP	0
#define KARR	1
#define KPAR	2
#define KSET	3

//////////////////////////////////////////////////////////////////////////
//
// KEB DIN 2 Device Options
//

class CKEBDIN2DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKEBDIN2DeviceOptions(void);
				
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// KEBDIN2 Driver
//

class CKEBDIN2Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CKEBDIN2Driver(void);

		// Destructor
		~CKEBDIN2Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	
	protected:
		// Implementation
		void AddSpaces(void);

		// Helper
		UINT ToHex(CString Text, UINT uMaxCt);
		BOOL NeedSet(UINT uTable);
	};

//////////////////////////////////////////////////////////////////////////
//
// KEBDIN2 Address Selection
//

class CKEBDIN2Dialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CKEBDIN2Dialog(CKEBDIN2Driver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

		// Message Map
		AfxDeclareMessageMap();
		                
	protected:
		// Overridables
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowAddress(CAddress Addr);
		void	ShowDetails(void);

		// Helper
		UINT	ParameterType(UINT uTable);
		BOOL	NeedSet(UINT uTable);
	};

// End of File

#endif
