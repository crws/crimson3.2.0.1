
#include "intern.hpp"

#include "beckplus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus Tags TCP Driver
//

// Constructor

CBeckhoffPlusTagsTCPDriver::CBeckhoffPlusTagsTCPDriver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "Beckhoff";
	
	m_DriverName	= "Beckhoff Plus";
	
	m_Version	= "1.00";

	m_DevRoot	= "MAIN";
	
	m_ShortName	= "Beckhoff Plus";
	}

// Address Management

BOOL CBeckhoffPlusTagsTCPDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CBeckhoffPlusTagsTCPDeviceOptions *) pConfig)->ParseAddress(Error, Addr, Text);
	}

BOOL CBeckhoffPlusTagsTCPDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CBeckhoffPlusTagsTCPDeviceOptions *) pConfig)->ExpandAddress(Text, Addr);
	}

BOOL CBeckhoffPlusTagsTCPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CBeckhoffPlusTagsTCPDeviceOptions *) pConfig)->SelectAddress(hWnd, Addr, fPart);
	}

BOOL CBeckhoffPlusTagsTCPDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CBeckhoffPlusTagsTCPDeviceOptions *) pConfig)->ListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus TCP Driver
//

// Instantiator

ICommsDriver *	Create_BeckhoffPlusTCPDriver(void)
{
	return New CBeckhoffPlusTCPDriver;
	}

// Constructor

CBeckhoffPlusTCPDriver::CBeckhoffPlusTCPDriver(void)
{
	m_wID		= 0x3524;
	}

// Binding Control

UINT CBeckhoffPlusTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBeckhoffPlusTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CBeckhoffPlusTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CBeckhoffPlusTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBeckhoffPlusTCPDeviceOptions);
	}

// End of File
