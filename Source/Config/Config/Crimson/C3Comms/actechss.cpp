
#include "intern.hpp"

#include "actechss.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo Driver
//

// Instantiator

ICommsDriver *	Create_ACTechSSDriver(void)
{
	return New CACTechSSDriver;
	}

// Constructor

CACTechSSDriver::CACTechSSDriver(void)
{
	m_wID		= 0x401A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "AC Tech";
	
	m_DriverName	= "Simple Servo";
	
	m_Version	= "1.00";
	
	m_ShortName	= "AC Tech Simple Servo";

	AddSpaces();
	}

// Binding Control

UINT	CACTechSSDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CACTechSSDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

// Address Management

BOOL CACTechSSDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CACTechSSAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CACTechSSDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) {

		return FALSE;
		}

	if( pSpace->m_uTable == addrNamed ) {

		Addr.a.m_Offset = pSpace->m_uMinimum;
		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = pSpace->m_uType;

		return TRUE;
		}

	CString sErr;

	UINT uPar1 = tatoi(Text);

	if( uPar1 <= pSpace->m_uMaximum && uPar1 >= pSpace->m_uMinimum ) {

		Addr.a.m_Offset = uPar1;
		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = pSpace->m_uType;

		return TRUE;
		}

	sErr.Printf( "%s%d - %s%d",
		pSpace->m_Prefix,
		pSpace->m_uMinimum,
		pSpace->m_Prefix,
		pSpace->m_uMaximum
		);

	Error.Set( sErr,
		0
		);

	return FALSE;
	}

BOOL CACTechSSDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) {

		return FALSE;
		}

	switch( Addr.a.m_Table ) {

		case addrNamed:

			Text = pSpace->m_Prefix;

			return TRUE;

		default:
			Text.Printf( "%s%d",
				pSpace->m_Prefix,
				Addr.a.m_Offset
				);

			return TRUE;
		}			
	}

// Implementation	

void CACTechSSDriver::AddSpaces(void)
{
	AddSpace(New CSpace(AN, " ",	"DRIVE PARAMETERS...",			10, SPDRIVE,  FZ, LL));
	AddSpace(New CSpace(AN, "CL",	"   Current limit\t\tW30 | R43",			10,  30, FA, RR));
	AddSpace(New CSpace(AN, "PC",	"   Peak current\t\tW31 | R44",				10,  31, FA, RR));
	AddSpace(New CSpace(AN, "PFPG",	"   Position filter P-gain\tW32 | R45",			10,  32, FA, WW));
	AddSpace(New CSpace(AN, "PFIG",	"   Position filter I-gain\tW33 | R46",			10,  33, FA, RR));
	AddSpace(New CSpace(AN, "PFDG",	"   Position filter D-gain\tW34 | R47",			10,  34, FA, WW));
	AddSpace(New CSpace(AN, "VFFG",	"   Position filter VFF-gain\tW35 | R48",		10,  35, FA, RR));
	AddSpace(New CSpace(AN, "PFIL",	"   Position filter I-limit\tW36 | R49",		10,  36, FA, WW));
	AddSpace(New CSpace(AN, "VFPG",	"   Velocity filter P-gain\tW37 | R50",			10,  37, FA, WW));
	AddSpace(New CSpace(AN, "VFIG",	"   Velocity filter I-gain\tW38 | R51",			10,  38, FA, WW));
	AddSpace(New CSpace(AN, "MPE",	"   Maximum position error\tW39 | R52",			10,  39, FA, WW));
	AddSpace(New CSpace(AN, "MPET",	"   Maximum position error time\tW32 | R53",		10,  40, FA, WW));
//	AddSpace(New CSpace(1,  "DIA",	"   Drive identification - 4 char block",		10,   0,  4, LL));
	AddSpace(New CSpace(AN, "DCC",	"   Drive current capabilities\tR only-42",		10,  42, FA, RR));

	AddSpace(New CSpace(AN, " ",	"IO CONTROL...",			10, SPIO,  FZ, LL));
	AddSpace(New CSpace(2,  "OC",	"   Output configuration\tW54 | R58",			10,  0,   7, BB));
	AddSpace(New CSpace(AN, "EIC",	"   Enable input configuration\tW55 | R59",		10,  55, FB, BB));
	AddSpace(New CSpace(AN, "O",	"   Set/Get Output states\tW56 | R60",			10,  56, FB, BB));
	AddSpace(New CSpace(AN, "AOV",	"   Set/Get Analog output voltage\tW57 | R98",		10,  57, FB, RR));
	AddSpace(New CSpace(AN, "DIS",	"   Get digital input states\tR only-61",		10,  61, FB, WW));
	AddSpace(New CSpace(AN, "AIV",	"   Get analog input voltage\tR only-62",		10,  62, FB, RR));

	AddSpace(New CSpace(AN, " ",	"LIMITS HANDLING...",			10, SPLIMITS,  FZ, LL));
	AddSpace(New CSpace(AN, "SL",	"   Configure soft limits\tW63 | R67",			10,  63, FC, BB));
	AddSpace(New CSpace(AN, "HL",	"   Configure hard limits\tW64 | R68",			10,  64, FC, BB));
	AddSpace(New CSpace(AN, "NSL",	"   Negative soft limit\tW65 | R69",			10,  65, FC, RR));
	AddSpace(New CSpace(AN, "PSL",	"   Positive soft limit\t\tW66 | R70",			10,  66, FC, RR));

	AddSpace(New CSpace(AN, " ",	"MONITORING FUNCTION...",		10, SPMONITOR,  FZ, LL));
	AddSpace(New CSpace(AN, "MPF",	"   Last program fault\tR only-71",			10,  71, FD, BB));
	AddSpace(New CSpace(AN, "MS",	"   Status\t\tR only-72",				10,  72, FD, LL));
	AddSpace(New CSpace(AN, "MRMS",	"   RMS current\t\tR only-73",				10,  73, FD, RR));
/* Appear not to be in the device
	AddSpace(New CSpace(AN, "MAV",	"   Actual velocity\t\tR only-74",			10,  74, FD, RR));
	AddSpace(New CSpace(AN, "MTV",	"   Target velocity\t\tR only-75",			10,  75, FD, RR));
	AddSpace(New CSpace(AN, "MMPC",	"   Maximum motor phase current\tR only-77",		10,  77, FD, RR));
*/
	AddSpace(New CSpace(AN, "MMER",	"   Motor encoder resolution\tR only-76",		10,  76, FD, LL));
	AddSpace(New CSpace(AN, "MERR",	"   Command Execution Fault\tInternal",			10, 100, FD, LL));

// EEPR writes certain data directly to the EEPROM.  Will install only if demanded.
// Writing data using the other opcodes only stores to working area.
// EEPROM data is loaded into the working area on servo power up.
// Not all EEPROM opcodes have been determined.
// Programmer must arrange to reload the working area on servo power cycle.
// DLD code for EEPR is debugged, only additions need be made there.
//	AddSpace(New CSpace( 4,	"EEPR",	"   Store to EEPROM - See Application Note",		10,  30, 64, BB));
//	AddSpace(New CSpace( 5,	"UNOP",	"   Search for unknown Opcodes",			10,   1,65535, LL));
//	AddSpace(New CSpace(AN, "OPV",	"   Unknown Opcode Number",				10, 101, FD, LL));


	AddSpace(New CSpace(AN, " ",	"INDEXER VARIABLES...",			10, SPINDEXER,  FZ, LL));
	AddSpace(New CSpace(AN, "U",	"   Units\t\tW 1  | R12",				10,   1, FE, RR));
	AddSpace(New CSpace(AN, "A",	"   Acceleration\t\tW 2  | R13",			10,   2, FE, RR));
	AddSpace(New CSpace(AN, "D",	"   Deceleration\t\tW 3  | R14",			10,   3, FE, RR));
	AddSpace(New CSpace(AN, "QD",	"   Quick deceleration\tW 4  | R15",			10,   4, FE, RR));
	AddSpace(New CSpace(AN, "MV",	"   Maximum velocity\tW 5  | R16",			10,   5, FE, RR));
	AddSpace(New CSpace(AN, "SP",	"   Set/Get position\t\tW 6  | R17",			10,   6, FE, RR));
	AddSpace(New CSpace(AN, "IPL",	"   In-position limit\t\tW 7  | R20",			10,   7, FE, RR));
	AddSpace(New CSpace(AN, "V",	"   Velocity\t\tW 8  | R21",				10,   8, FE, RR));
	AddSpace(New CSpace(AN, "MER",	"   Master encoder resolution\tW 9  | R22",		10,   9, FE, LL));
	AddSpace(New CSpace(AN, "GC",	"   Gearing coefficient\tW10 | R23",			10,  10, FE, RR));
	AddSpace(New CSpace(3,  "VAR",	"   Variable\t\tW11 | R24",				10,   0, 31, RR));
	AddSpace(New CSpace(AN, "GAP",	"   Get actual position\tR only-17",			10,  17, FE, RR));
	AddSpace(New CSpace(AN, "GTP",	"   Get target position\tR only-18",			10,  18, FE, RR));
/* Appears to not conform to specification
	AddSpace(New CSpace(AN, "GPE",	"   Get position error\tR only-19",			10,  19, FE, RR));
*/
	AddSpace(New CSpace(AN, "GLR",	"   Get last registration\tR only-25",			10,  25, FE, RR));

	AddSpace(New CSpace(AN, " ",	"MOTION COMMANDS...",			10, SPMOTION,  FZ, LL));
	AddSpace(New CSpace(AN, "RI",	"   Reset indexer\t\t78-W only",			10,  78, FF, BB));
	AddSpace(New CSpace(AN, "MCE",	"   Motion controller enable\t79-W only",		10,  79, FF, BB));
	AddSpace(New CSpace(AN, "MCD",	"   Motion controller disable\t80-W only",		10,  80, FF, BB));
	AddSpace(New CSpace(AN, "SUSP",	"   Suspend motion\t\t81-W only",			10,  81, FF, BB));
	AddSpace(New CSpace(AN, "RM",	"   Resume motion\t\t82-W only",			10,  82, FF, BB));
	AddSpace(New CSpace(AN, "MP",	"   Move to position\t\t83-W only",			10,  83, FF, RR));
	AddSpace(New CSpace(AN, "MD",	"   Move distance\t\t84-W only",			10,  84, FF, RR));
	AddSpace(New CSpace(AN, "MDV",	"   Send MDVD + MDVV...\t85-W only",			10,  85, FF, BB));
	AddSpace(New CSpace(AN, "MDVD",	"   ...Move distance to velocity - Distance",		10, 185, FF, RR));
	AddSpace(New CSpace(AN, "MDVV",	"   ...Move distance to velocity - Velocity",		10, 285, FF, RR));
	AddSpace(New CSpace(AN, "MPI1",	"   Move positive while input = 1\t86-W only",		10,  86, FF, BB));
	AddSpace(New CSpace(AN, "MPI0",	"   Move positive while input = 0\t87-W only",		10,  87, FF, BB));
	AddSpace(New CSpace(AN, "MNI1",	"   Move negative while input = 1\t88-W only",		10,  88, FF, BB));
	AddSpace(New CSpace(AN, "MNI0",	"   Move negative while input = 0\t89-W only",		10,  89, FF, BB));
	AddSpace(New CSpace(AN, "MDUR",	"   Send MDUD + MDUS...\t90-W only",			10,  90, FF, BB));
	AddSpace(New CSpace(AN, "MDUD",	"   ...Move distance until registration - Distance",	10, 190, FF, RR));
	AddSpace(New CSpace(AN, "MDUS",	"   ...Move distance until registration - Displacement",10, 290, FF, RR));
	AddSpace(New CSpace(AN, "VME",	"   Velocity mode enable\t91-W only",			10,  91, FF, BB));
	AddSpace(New CSpace(AN, "VMD",	"   Velocity mode disable\t92-W only",			10,  92, FF, BB));
	AddSpace(New CSpace(AN, "GME",	"   Gearing mode enable\t93-W only",			10,  93, FF, BB));
	AddSpace(New CSpace(AN, "GMD",	"   Gearing mode disable\t94-W only",			10,  94, FF, BB));
	AddSpace(New CSpace(AN, "S",	"   Stop motion\t\t95-W only",				10,  95, FF, BB));
	AddSpace(New CSpace(AN, "SQ",	"   Stop motion quick\t96-W only",			10,  96, FF, BB));
	AddSpace(New CSpace(AN, "MPUR",	"   Send MPUD + MPUS...\t97-W only",			10,  97, FF, BB));
	AddSpace(New CSpace(AN, "MPUD",	"   ...Move to position until registration - Position",	10, 197, FF, RR));
	AddSpace(New CSpace(AN, "MPUS",	"   ...Move to position until registration - Displacement",10, 297, FF, RR));
	AddSpace(New CSpace(AN, "SIP",	"   Send SIPP + SIPD...\t99-W only",			10,  99, FF, BB));
	AddSpace(New CSpace(AN, "SIPP",	"   ...Start indexing program - Position",		10, 199, FF, RR));
	AddSpace(New CSpace(AN, "SIPD",	"   ...Start indexing program - Distance",		10, 299, FF, RR));
	}

// Helpers

CSpace * CACTechSSDriver::GetSpace(CAddress const &Addr)
{ // In Driver
	CSpaceList & List = CStdCommsDriver::GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( MatchSpace(pSpace, Addr) ) {

				return pSpace;
				}

			List.GetNext(n);
			}
		}

	return NULL;
	}

BOOL CACTechSSDriver::MatchSpace(CSpace * pSpace, CAddress const &Addr)
{
	UINT uTable = pSpace->m_uTable;

	if( uTable == Addr.a.m_Table ) {

		if( uTable == addrNamed ) {

			if( Addr.a.m_Offset == pSpace->m_uMinimum ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo UDP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CACTechSSUDPDeviceOptions, CUIItem);

// Constructor

CACTechSSUDPDeviceOptions::CACTechSSUDPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 8995;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Management

void CACTechSSUDPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CACTechSSUDPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CACTechSSUDPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo UDP Master Driver
//

// Instantiator

ICommsDriver * Create_ACTechSSUDPDriver(void)
{
	return New CACTechSSUDPDriver;
	}

// Constructor

CACTechSSUDPDriver::CACTechSSUDPDriver(void)
{
	m_wID		= 0x3516;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "AC Tech";
	
	m_DriverName	= "Simple Servo UDP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "AC Tech Simple Servo UDP";
	}

// Binding Control

UINT CACTechSSUDPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CACTechSSUDPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CACTechSSUDPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CACTechSSUDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CACTechSSUDPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CACTechSSAddrDialog, CStdAddrDialog);
		
// Constructor

CACTechSSAddrDialog::CACTechSSAddrDialog(CACTechSSDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	if( m_uAllowSpace < SPDRIVE || m_uAllowSpace > SPMOTION ) {

		m_uAllowSpace = SPMOTION;
		}

	m_pACDriver = &Driver;

	SetName(TEXT("ACTechAddressDlg"));
	}

// Message Map

AfxMessageMap(CACTechSSAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CACTechSSAddrDialog)
	};

// Message Handlers

BOOL CACTechSSAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	m_pSpace = m_pACDriver->GetSpace(*m_pAddr);

	if( !m_fPart ) {

		SetCaption();

		if( m_pSpace ) {

			SetAllow();
			}

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			return FALSE;
			}

		return TRUE;
		}
	else {
		FindSpace();

		if( m_pSpace ) {

			SetAllow();

			LoadList();

			OnSpaceChange(1000, ListBox);

			ShowAddress(Addr);

			return TRUE;
			}

		return FALSE;
		}
	}

// Notification Handlers

void CACTechSSAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CACTechSSAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			SetAllow();

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = m_pSpace->m_uMinimum;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			m_pSpace->m_uMinimum < SPDRIVE ? ShowDetails() : ClearDetails();

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(2001).SetWindowText("None");
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2003).SetWindowText("");
		GetDlgItem(2010).SetWindowText("");

		ClearDetails();

		GetDlgItem(2001).EnableWindow(TRUE);
		GetDlgItem(2002).EnableWindow(FALSE);
		}
	}

BOOL CACTechSSAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		switch( m_pSpace->m_uTable ) {

			case addrNamed:

				if( IsHeader(m_pSpace->m_uMinimum) ) {

					m_uAllowSpace = m_pSpace->m_uMinimum;

					LoadList();

					GetDlgItem(3002).SetWindowText("");

					return TRUE;
					}

				break;

			default:
				Text += GetDlgItem(2002).GetWindowText();

				break;
			}		

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables
void CACTechSSAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	GetDlgItem(2001).EnableWindow(TRUE);
	GetDlgItem(2002).EnableWindow(Addr.a.m_Table != addrNamed);
	GetDlgItem(2003).EnableWindow(Addr.a.m_Table != addrNamed);

	GetDlgItem(2010).SetWindowText("");
	GetDlgItem(2010).EnableWindow(FALSE);

	switch( Addr.a.m_Table ) {

		case addrNamed:

			GetDlgItem(2002).SetWindowText("");
			GetDlgItem(2003).SetWindowText("");

			if( Addr.a.m_Offset >= SPDRIVE ) {

				GetDlgItem(3002).SetWindowText("");
				}

			GetDlgItem(3004).SetWindowText("");
			GetDlgItem(3006).SetWindowText("");
			GetDlgItem(3008).SetWindowText("");

			return;

		case 1:	// Not available in protocol
			GetDlgItem(2002).SetWindowText(m_pSpace->GetValueAsText(Addr.a.m_Offset));
			GetDlgItem(2003).SetWindowText("4 Char. Block");
			GetDlgItem(2010).SetWindowText("\"A04108...\" A041->0, 08xx->1,...");

			GetDlgItem(2010).EnableWindow(TRUE);

			break;

		case 2:
			GetDlgItem(2002).SetWindowText(m_pSpace->GetValueAsText(Addr.a.m_Offset));
			GetDlgItem(2003).SetWindowText("Output Number");

			break;

		case 3:
			GetDlgItem(2002).SetWindowText(m_pSpace->GetValueAsText(Addr.a.m_Offset));
			GetDlgItem(2003).SetWindowText("Variable Number");
			break;

		case 4:
			GetDlgItem(2002).SetWindowText(m_pSpace->GetValueAsText(Addr.a.m_Offset));
			GetDlgItem(2003).SetWindowText("Servo failure possible!");
			break;

		case 5:
			GetDlgItem(2002).SetWindowText(m_pSpace->GetValueAsText(Addr.a.m_Offset));
			GetDlgItem(2003).SetWindowText("Unassigned");
			break;
		}

	SetDlgFocus(2002);
	}

// Helpers

BOOL CACTechSSAddrDialog::AllowSpace(CSpace *pSpace)
{
	return	pSpace->m_uTable == addrNamed

			? SelectList( pSpace, pSpace->m_uMinimum, m_uAllowSpace )

			: m_uAllowSpace == GetTableSpace(pSpace);
	}

BOOL CACTechSSAddrDialog::SelectList(CSpace *pSpace, UINT uCommand, UINT uAllow)
{
	if( IsHeader(uCommand) ) {

		return TRUE;
		}

	switch( uAllow ) {

		case SPDRIVE:	return pSpace->m_uMaximum == FA;

		case SPIO:	return pSpace->m_uMaximum == FB;

		case SPLIMITS:	return pSpace->m_uMaximum == FC;

		case SPMONITOR:	return pSpace->m_uMaximum == FD;

		case SPINDEXER:	return pSpace->m_uMaximum == FE;

		case SPMOTION:	return pSpace->m_uMaximum == FF;
		}

	return FALSE;
	}

void CACTechSSAddrDialog::SetAllow()
{
	if( !m_pSpace ) {

		m_uAllowSpace = SPMOTION;

		return;
		}

	if( m_pSpace->m_uTable != addrNamed ) {

		m_uAllowSpace = GetTableSpace(m_pSpace);

		return;
		}

	UINT uCommand = m_pSpace->m_uMinimum;

	for( UINT i = SPDRIVE; i <= SPMOTION; i++ ) {

		if( SelectList(m_pSpace, uCommand, i) ) {

			m_uAllowSpace = i;

			return;
			}
		}
	}

UINT CACTechSSAddrDialog::IsHeader(UINT uCommand)
{
	return uCommand >= SPDRIVE && uCommand <= SPMOTION ? 1 : 0;
	}

UINT CACTechSSAddrDialog::GetTableSpace(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
			return SPDRIVE;

		case 2:
			return SPIO;

		case 3:
			return SPINDEXER;

		case 4:
		case 5:
			return SPMONITOR;
		}

	return 0;
	}

// End of File
