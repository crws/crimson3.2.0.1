
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_FAM3_HPP
	
#define	INCLUDE_FAM3_HPP

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC
//

class CYokoFam3Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CYokoFam3Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC TCP/IP Master
//

class CYokoFam3TCPDriver : public CYokoFam3Driver
{
	public:
		// Constructor
		CYokoFam3TCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC Device Options
//

class CYokoFam3DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYokoFam3DeviceOptions(void);

		// Public Data
		UINT m_Mode;
		UINT m_Station;
		UINT m_Cpu;
		UINT m_Wait;
		BOOL m_fCheck;
		BOOL m_fTerm;
		
	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};   



//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC TCP/IP Device Options
//

class CYokoFam3TCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYokoFam3TCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_Addr;
		UINT m_Port;
		UINT m_Cpu;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_Mode;
		
	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};   

// End of File

#endif
