
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_OMNIFLOW_HPP
	
#define	INCLUDE_OMNIFLOW_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Spaces
//

#define	SPACE_CS	3
#define	SPACE_IS	4
#define	SPACE_HR	1
#define	SPACE_IR	2
#define SPACE_EC	5
#define SPACE_CDP	6
#define SPACE_CPB	7
#define SPACE_CDC	8
#define SPACE_CPD	9
#define SPACE_BRP	10
#define SPACE_BRR	11
#define SPACE_RAB	12
#define SPACE_BWP	13
#define SPACE_BWQ	14
#define SPACE_BWR	15
#define SPACE_WAB	16
#define SPACE_HRL	17
#define SPACE_HRD	18
#define SPACE_STR8	19
#define SPACE_STR16	20
#define SPACE_STR32	21
#define SPACE_CDR	22

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Serial Driver Options
//

class COmniFlowDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmniFlowDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);


		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Protocol;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Serial Device Options
//

class COmniFlowDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmniFlowDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Ping;
		BOOL m_fDisable5;
		BOOL m_fDisable6;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Serial Driver
//

class COmniFlowMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		COmniFlowMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Data Access
		BOOL IsLogical(void);

	protected:
		// Implementation
		void AddSpaces(void);
		

	};

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow TCP/IP Master Driver Options
//

class COmniFlowMasterTCPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmniFlowMasterTCPDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Omni Flow TCP/IP Master Device Options
//

class COmniFlowMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmniFlowMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Port;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_PingReg;
		BOOL m_fDisable5;
		BOOL m_fDisable6;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow TCP/IP Master Driver
//

class COmniFlowMasterTCPDriver : public COmniFlowMasterDriver
{
	public:
		// Constructor
		COmniFlowMasterTCPDriver(void);

		//Destructor
		~COmniFlowMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Space
//

class COmniSpace : public CSpace
{
	public:
		
		// Constructors
		COmniSpace(void);
		COmniSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s);
		                
	protected:
		// Overridables
		void	GetMaximum(CAddress &Addr);
	};



//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Modicon Support
//
/////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Modicon Serial Driver
//

class COmniFlowModiconMasterDriver : public  COmniFlowMasterDriver
{
	public:
		// Constructor
		COmniFlowModiconMasterDriver(void);

		//Destructor
		~COmniFlowModiconMasterDriver(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Modicon TCP/IP Master Driver
//

class COmniFlowModiconMasterTCPDriver : public COmniFlowModiconMasterDriver
{
	public:
		// Constructor
		COmniFlowModiconMasterTCPDriver(void);

		//Destructor
		~COmniFlowModiconMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

	};

// End of File

#endif
