//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GEDNC2_HPP

#define	INCLUDE_GEDNC2_HPP

//////////////////////////////////////////////////////////////////////////
//
// GE DNC2 Master Driver
//

class CGEDNC2MasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CGEDNC2MasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// GEDNC2 Address Selection
//

class CGEDNC2AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CGEDNC2AddrDialog(CGEDNC2MasterDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overridables
		void ShowAddress(CAddress Addr);
		void ShowDetails(void);
		void ClearAddress(void);

		// Helpers
		UINT	HasAddr(CSpace *pSpace);
		void	EnableWindows(UINT uHasAddr);
		void	SetDescText(CSpace *pSpace);
	};

/*/////////////////////////////////////////////////////////////////////////
//
// GE DNC2 TCP/IP Device Options
//

class CGEDNC2TCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGEDNC2TCPDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// GE DNC2 TCP/IP Driver
//

class CGEDNC2TCPDriver : public CGEDNC2MasterDriver
{
	public:
		// Constructor
		CGEDNC2TCPDriver(void);

		// Destructor
		~CGEDNC2TCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};
*/
#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Space ID's
#define	RMP	 1 // Tool Position - Machine
#define	RWP	 2 // Tool Position - Absolute
#define	RSP	 3 // Tool Position - Skip Signal Detect
#define	RSE	 4 // Servo Delay for Axis
#define	RAE	 5 // Accel / Decel Delay for Axis
#define	RMI	 6 // Machine Interface Signals
#define	RPN	 7 // Current Program Number
#define	RSN	 8 // Current Sequence Number
#define	RPAP	 9 // Read CNC Parameter Value
#define	RPAA	10 // CNC Parameter Axis Exponent
#define	WPA	11 // Execute CNC Parameter Write
#define	WPAP	12 // CNC Parameter Value to Write
#define	WPAA	13 // CNC Axis Exponent to Write
#define	RWPE	14 // Pitch Error Compensation Value
#define	RTOD	15 // Tool Offset Cutter Wear
#define	RTOK	16 // Tool Offset Cutter Geometry
#define	RTOH	17 // Tool Offset Length Wear
#define	RTOL	18 // Tool Offset Length Geometry
#define	RTOX	19 // Tool Offset X Axis Wear
#define	RTOU	20 // Tool Offset X Axis Geometry
#define	RTOY	21 // Tool Offset Y Axis Wear
#define	RTOV	22 // Tool Offset Y Axis Geometry
#define	RTOZ	23 // Tool Offset Z Axis Wear
#define	RTOW	24 // Tool Offset Z Axis Geometry
#define	RTOR	25 // Tool Offset Tip Radius Wear
#define	RTOP	26 // Tool Offset Tip Radius Geometry
#define	RTOQ	27 // Tool Offset Virtual Direction
#define	WTON	28 // Tool Offset Number
#define	WTOB	29 // Select Bit Pattern VPWUQYRZXLHKD
#define	WTOD	30 // Tool Offset Cutter Wear
#define	WTOK	31 // Tool Offset Cutter Geometry
#define	WTOH	32 // Tool Offset Length Wear
#define	WTOL	33 // Tool Offset Length Geometry
#define	WTOX	34 // Tool Offset X Axis Wear
#define	WTOU	35 // Tool Offset X Axis Geometry
#define	WTOY	36 // Tool Offset Y Axis Wear
#define	WTOV	37 // Tool Offset Y Axis Geometry
#define	WTOZ	38 // Tool Offset Z Axis Wear
#define	WTOW	39 // Tool Offset Z Axis Geometry
#define	WTOR	40 // Tool Offset Tip Radius Wear
#define	WTOP	41 // Tool Offset Tip Radius Geometry
#define	WTOQ	42 // Tool Offset Virtual Direction
#define	RWMV	43 // Custom Macro Variable Value
#define	RTLL	44 // Tool Life Value
#define	RTLQ	45 // Tool Life Count
#define	RTLT	46 // Tool Number
#define	RTLH	47 // Tool Life H Code
#define	RTLD	48 // Tool Life D Code
#define	RTLC	49 // Tool Information
#define	RMDG	50 // Modal Information G
#define	RMDD	51 // Modal Information D
#define	RMDE	52 // Modal Information E
#define	RMDH	53 // Modal Information H
#define	RMDL	54 // Modal Information L
#define	RMDM	55 // Modal Information M
#define	RMDN	56 // Modal Information N
#define	RMDO	57 // Modal Information O
#define	RMDS	58 // Modal Information S
#define	RMDT	59 // Modal Information T
#define	RMDF	60 // Modal Information F
#define	RAF	61 // Feed Rate for Axis
#define	RADI	62 // Analog Input - General
#define	RADS	63 // Spindle Voltage for Axis
#define	RADN	64 // NC Control Voltage for Axis
#define	RAL	65 // Alarm Information
#define	RST	66 // Status Information
#define	WDI	67 // Send Operator Message
#define	WDIS	68 // Set Up Operator Message
#define	WSL	69 // Select Part Program
#define	WCS	70 // Execute a Program
#define	WCC	71 // Reset
#define	RID	72 // System ID
#define	ERR	99 // Latest NAK

// End of File

#endif
