
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EURORTNX_HPP
	
#define	INCLUDE_EURORTNX_HPP

class CEurortnxDeviceOptions;
class CEurortnxDriver;

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX Device Options
//

class CEurortnxDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEurortnxDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX Driver
//

class CEurortnxDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEurortnxDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		
	protected:
		// Implementation
		void AddSpaces(void);

		// Helpers
	};

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX ID Selection
//

class CEurortnxAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CEurortnxAddrDialog(CEurortnxDriver &Driver, CAddress &Addr, BOOL fPart);

		// Message Map
		AfxDeclareMessageMap();
		                
	protected:
		// Overridables
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		BOOL	OnOkay(UINT uID);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowAddress(CAddress Addr);
		void	ShowDetails(void);
	};

// End of File

#endif
