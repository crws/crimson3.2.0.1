
#include "intern.hpp"

#include "DoMore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Serial Driver
//

// Instantiator

ICommsDriver * Create_DoMoreDriver(void)
{
	return New CDoMoreDriver();
	}

// Constructor

CDoMoreDriver::CDoMoreDriver(void)
{
	m_wID		= 0x40BC;

	m_uType		= driverMaster;

	m_Manufacturer	= "Automation Direct - Koyo";

	m_DriverName	= "Do-More PLC";

	m_Version	= "1.00";

	m_ShortName	= "Do-More";

	AddSpaces();
	}

// Destructor

CDoMoreDriver::~CDoMoreDriver(void)
{
	}

// Configuration

CLASS CDoMoreDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CDoMoreDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDoMoreSerialDeviceOptions);
	}

// Binding

UINT CDoMoreDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CDoMoreDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CDoMoreDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( IsStructuredType(pSpace->m_uTable) ) {

		CString TypeName = GetTypeName(pSpace->m_uTable);

		UINT uPos = Text.Find(L'.');

		if( uPos < NOTHING ) {

			CString FieldName = Text.Mid(uPos + 1);

			CString Offset = Text.Left(uPos);

			UINT uOffset = wcstol(Offset, NULL, pSpace->m_uRadix);

			if( uOffset < pSpace->m_uMinimum || uOffset > pSpace->m_uMaximum ) {

				Error.Set(IDS_ERROR_OFFSETRANGE);

				return FALSE;
				}

			CDoMoreField Field;

			if( FindField(TypeName, FieldName, Field) ) {

				UINT uDWORD = Field.m_uMajOffset;

				UINT uBitOff = Field.m_uMinOffset;

				UINT uPacked = (uDWORD << 12) | (uBitOff << 7) | uOffset;

				Addr.a.m_Table = pSpace->m_uTable;

				Addr.a.m_Extra = uPacked >> 16;

				Addr.a.m_Offset = uPacked & 0xFFFF;

				Addr.a.m_Type = Field.m_uType;

				return TRUE;
				}
			}

		Error.Set(L"Unable to parse structured type.");

		return FALSE;			
		}

	else if( IsString(pSpace->m_uTable) ) {

		UINT uPos = wcstoul(Text, NULL, pSpace->m_uRadix) * MAX_STRING;

		if( uPos >= pSpace->m_uMinimum && uPos <= pSpace->m_uMaximum ) {

			Addr.a.m_Offset = uPos;

			Addr.a.m_Table = pSpace->m_uTable;

			Addr.a.m_Type = pSpace->m_uType;

			Addr.a.m_Extra = 0;

			return TRUE;
			}
		else {
			Error.Set(IDS_ERROR_OFFSETRANGE);

			return FALSE;
			}
		}
	else {
		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}
	}

BOOL CDoMoreDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( IsStructuredType(Addr.a.m_Table) ) {

		CString TypeName = GetTypeName(Addr.a.m_Table);

		UINT uPacked = Addr.a.m_Extra << 16 | Addr.a.m_Offset;

		UINT uOffset = uPacked & 0x7F;

		UINT uBit = (uPacked & 0x0F80) >> 7;

		UINT uDWORD = uPacked >> 12;

		CString FieldName = GetFieldName(TypeName, uBit, uDWORD, Addr.a.m_Type);

		CSpace *pSpace = GetSpace(Addr);

		if( pSpace ) {

			Text = CPrintf(	"%s%3.3d.%s",
					pSpace->m_Prefix,
					uOffset,
					FieldName
					);

			return TRUE;
			}

		return FALSE;
		}

	else if( IsString(Addr.a.m_Table) ) {

		UINT uPos = Addr.a.m_Offset / MAX_STRING;

		CSpace *pSpace = GetSpace(Addr);

		if( pSpace ) {

			Text = CPrintf(	"%s%3.3d",
					pSpace->m_Prefix,
					uPos
					);

			return TRUE;
			}

		return FALSE;
		}
	else {
		return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
		}
	}


BOOL CDoMoreDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CDoMoreAddrDialog Dlg(ThisObject, Addr, pConfig, fPart);

	return Dlg.Execute();
	}

// Implementation

void CDoMoreDriver::AddSpaces(void)
{
	AddSpace(New CSpace(TBL_ST,	"ST",	"System Status Bits",				10, 0, 1023,	addrBitAsByte));
	AddSpace(New CSpace(TBL_DST,	"DST",	"System Status Longs",				10, 0, 511,	addrLongAsLong));
	AddSpace(New CSpace(TBL_SDT,	"SDT",	"System Time and Date Structures",		10, 0, 7,	addrWordAsWord));
	AddSpace(New CSpace(TBL_X,	"X",	"Discrete Input Bits",				10, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_Y,	"Y",	"Discrete Output Bits",				10, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_WX,	"WX",	"Analog Input Words",				10, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(TBL_WY,	"WY",	"Analog Output Words",				10, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(TBL_C,	"C",	"Program Control Bits",				10, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_V,	"V",	"Unsigned 16-bit Integer Variables",		10, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(TBL_N,	"N",	"Signed 16-bit Integer Variables",		10, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(TBL_D,	"D",	"Signed 32-bit Integer Variables",		10, 0, 65535,	addrLongAsLong));
	AddSpace(New CSpace(TBL_R,	"R",	"Floating Point Variables",			10, 0, 65535,	addrRealAsReal));
	AddSpace(New CSpace(TBL_T,	"T",	"Timer Structures",				10, 0, 127,	addrWordAsWord));
	AddSpace(New CSpace(TBL_CT,	"CT",	"Counter Structures",				10, 0, 127,	addrWordAsWord));
	AddSpace(New CSpace(TBL_SS,	"SS",	"Short Strings (64 characters)",		10, 0, 65535,	addrByteAsByte));
	AddSpace(New CSpace(TBL_SL,	"SL",	"Long Strings (256 characters)",		10, 0, 65535,	addrByteAsByte));
	AddSpace(New CSpace(TBL_UDT,	"UDT",	"User Time and Date Structures",		10, 0, 127,	addrWordAsWord));
	AddSpace(New CSpace(TBL_PL,	"PL",	"Peerlink Shared Memory",			10, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(TBL_DLX,	"DLX",	"Legacy DirectLogic X Bits (Octal Addressing)",	 8, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_DLY,	"DLY",	"Legacy DirectLogic Y Bits (Octal Addressing)",  8, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_DLC,	"DLC",	"Legacy DirectLogic C Bits (Octal Addressing)",	 8, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_DLV,	"DLV",	"Legacy DirectLogic V Words (Octal Addressing)", 8, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(TBL_MI,	"MI",	"Modbus Input Bits",				10, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_MC,	"MC",	"Modbus Coil Bits",				10, 0, 65535,	addrBitAsByte));
	AddSpace(New CSpace(TBL_MIR,	"MIR",	"Modbus Input Register Words",			10, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(TBL_MHR,	"MHR",	"Modbus Holding Register Words",		10, 0, 65535,	addrWordAsWord));

	AddSpace(New CSpace(TBL_LASTMSG,"LastMSG", "Previous System Messages",			10, 0, 1791,	addrByteAsByte));
	AddSpace(New CSpace(TBL_LASTERR,"LastERR", "Previous System Errors",			10, 0, 1791,	addrByteAsByte));
	}


BOOL CDoMoreDriver::IsStructuredType(UINT uTable)
{
	switch( uTable ) {

		case TBL_SDT:
		case TBL_T:
		case TBL_CT:
		case TBL_UDT:

			return TRUE;
		}

	return FALSE;
	}

BOOL CDoMoreDriver::IsString(UINT uTable)
{
	switch( uTable ) {

		case TBL_SS:
		case TBL_SL:
		case TBL_LASTERR:
		case TBL_LASTMSG:

			return TRUE;
		}

	return FALSE;
	}

CString CDoMoreDriver::GetTypeName(UINT uTable)
{
	switch( uTable ) {

		case TBL_SDT:			
		case TBL_UDT:

			return "DATETIME";

		case TBL_T:

			return "TIMER";

		case TBL_CT:

			return "COUNTER";
		}

	return "";
	}

BOOL CDoMoreDriver::FindField(CString const &TypeName, CString const &FieldName, CDoMoreField &Field)
{
	CDoMoreStruct *pStruct = &m_PredefinedStructs[0];

	UINT uCount = elements(m_PredefinedStructs);

	while( TypeName.CompareC(pStruct->m_pName) && uCount-- ) {

		pStruct++;
		}

	if( uCount && TypeName == pStruct->m_pName ) {
		
		for( UINT n = 0; n < pStruct->m_uCount; n++ ) {

			CDoMoreField Search = pStruct->m_Fields[n];

			if( FieldName == Search.m_pName ) {

				Field = Search;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

CString CDoMoreDriver::GetFieldName(CString const &TypeName, UINT uBitOffset, UINT uDWORD, UINT uType)
{
	CDoMoreStruct *pStruct = &m_PredefinedStructs[0];

	UINT uCount = elements(m_PredefinedStructs);

	while( TypeName.CompareC(pStruct->m_pName) && uCount-- ) {

		pStruct++;
		}

	if( uCount && TypeName == pStruct->m_pName ) {

		for( UINT n = 0; n < pStruct->m_uCount; n++ ) {

			CDoMoreField Field = pStruct->m_Fields[n];

			BOOL fMatch = (Field.m_uMinOffset == uBitOffset)
					&& (Field.m_uMajOffset == uDWORD)
					&& (Field.m_uType == uType);

			if( fMatch ) {

				return Field.m_pName;
				}
			}
		}

	return "";
	}


//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Device Options
//

AfxImplementDynamicClass(CDoMoreSerialDeviceOptions, CUIItem);

// Constructor

CDoMoreSerialDeviceOptions::CDoMoreSerialDeviceOptions(void)
{
	m_Ping		= 0;

	m_PingEnable	= 1;

	m_Drop		= 1;

	m_Timeout	= 1000;
	}

// Destructor

CDoMoreSerialDeviceOptions::~CDoMoreSerialDeviceOptions(void)
{
	}

// Download Support

BOOL CDoMoreSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddLong(m_Ping);
	Init.AddByte(BYTE(m_PingEnable));
	Init.AddLong(m_Timeout);
	Init.AddText(m_Password);

	return TRUE;
	}

// UI Managament

void CDoMoreSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "PingEnable" ) {

			pWnd->EnableUI("Ping", m_PingEnable);
			}
		}
	}

// Meta Data Creation

void CDoMoreSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Ping);
	Meta_AddInteger(PingEnable);
	Meta_AddInteger(Timeout);
	Meta_AddString (Password);
	}

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Driver
//

// Instantiator

ICommsDriver * Create_DoMoreUDPDriver(void)
{
	return New CDoMoreUDPDriver();
	}

// Constructor

CDoMoreUDPDriver::CDoMoreUDPDriver(void)
{
	m_wID		= 0x40BD;

	m_uType		= driverMaster;

	m_Manufacturer	= "Automation Direct - Koyo";

	m_DriverName	= "Do-More PLC";

	m_Version	= "1.00";

	m_ShortName	= "Do-More";
	}

// Destructor

CDoMoreUDPDriver::~CDoMoreUDPDriver(void)
{
	}

// Configuration

CLASS CDoMoreUDPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CDoMoreUDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDoMoreUDPDeviceOptions);
	}

// Binding

UINT CDoMoreUDPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CDoMoreUDPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	Ether.m_UDPCount = 1;
	}

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDoMoreUDPDeviceOptions, CUIItem);

// Constructor

CDoMoreUDPDeviceOptions::CDoMoreUDPDeviceOptions(void)
{
	m_Port		= 28784;

	m_IP		= DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Ping		= 0;

	m_PingEnable	= 1;

	m_Keep		= TRUE;

	m_Time1		= 5000;

	m_Time2		= 2500;

	m_Time3		= 200;
	}

// Destructor

CDoMoreUDPDeviceOptions::~CDoMoreUDPDeviceOptions(void)
{
	}

// Download Support

BOOL CDoMoreUDPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(m_IP);
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));	
	Init.AddWord(WORD(m_Time2));	
	Init.AddWord(WORD(m_Time3));
	Init.AddLong(m_Ping);
	Init.AddByte(BYTE(m_PingEnable));
	Init.AddText(m_Password);

	return TRUE;
	}

// UI Managament

void  CDoMoreUDPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag == "PingEnable" ) {

			pWnd->EnableUI("Ping", m_PingEnable);
			}
		}
	}

// Meta Data Creation

void CDoMoreUDPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Ping);
	Meta_AddInteger(PingEnable);
	Meta_AddString(Password);
	}

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Driver
//

// Runtime Class

AfxImplementRuntimeClass(CDoMoreAddrDialog, CStdAddrDialog);

// Message Map

AfxMessageMap(CDoMoreAddrDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange)
	
	AfxMessageEnd(CStdAddrDialog)
	};

// Constructor

CDoMoreAddrDialog::CDoMoreAddrDialog(CDoMoreDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "DoMoreElementDlg";

	m_pDoMoreDrv = &Driver;
	}

// Message Handlers

BOOL CDoMoreAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CStdAddrDialog::OnInitDialog(Focus, dwData);

	LoadCombo();

	DoEnables();

	return TRUE;
	}

void CDoMoreAddrDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	DoEnables();

	LoadCombo();
	}

BOOL CDoMoreAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( m_pDoMoreDrv->IsStructuredType(m_pSpace->m_uTable) ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += "." + GetDlgItem(2004).GetWindowText();

			CError   Error(TRUE);

			CAddress Addr;
		
			if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				*m_pAddr = Addr;

				EndDialog(TRUE);

				return TRUE;
				}			

			Error.Show(ThisObject);

			SetAddressFocus();

			return TRUE;
			}
		else {
			return CStdAddrDialog::OnOkay(uID);
			}
		}

	return TRUE;
	}

// Implementation

void CDoMoreAddrDialog::DoEnables(void)
{
	BOOL fEnable = FALSE;

	if( m_pSpace ) {

		fEnable = m_pDoMoreDrv->IsStructuredType(m_pSpace->m_uTable);
		}

	CComboBox &Box = (CComboBox &) GetDlgItem(2004);

	Box.EnableWindow(fEnable);
	}

void CDoMoreAddrDialog::LoadCombo(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(2004);

	Box.ResetContent();

	if( m_pSpace ) {

		switch( m_pSpace->m_uTable ) {

			case TBL_T:

				LoadType(Box, "TIMER");
				
				break;

			case TBL_CT:
				
				LoadType(Box, "COUNTER");

				break;
				
			case TBL_SDT:
			case TBL_UDT:
				
				LoadType(Box, "DATETIME");

				break;
			}
		}

	if( Box.GetCount() > 0 ) {		

		Box.SetCurSel(0);

		BOOL fIsStruct = m_pDoMoreDrv->IsStructuredType(m_pAddr->a.m_Table);

		if( fIsStruct ) {

			UINT uPacked = (m_pAddr->a.m_Extra << 16) | m_pAddr->a.m_Offset;

			UINT uBit = (uPacked & 0x0F80) >> 7;

			UINT uDWORD = uPacked >> 12;

			CString TypeName = m_pDoMoreDrv->GetTypeName(m_pAddr->a.m_Table);

			CString FieldName = m_pDoMoreDrv->GetFieldName(TypeName, uBit, uDWORD, m_pAddr->a.m_Type);

			UINT uPos = Box.FindString(FieldName);

			if( uPos < NOTHING ) {

				Box.SetCurSel(uPos);
				}
			}
		}
	}

void CDoMoreAddrDialog::LoadType(CComboBox &Combo, CString TypeName)
{
	CDoMoreStruct *pStruct = &CDoMoreDriver::m_PredefinedStructs[0];

	UINT uCount = elements(CDoMoreDriver::m_PredefinedStructs);

	while( TypeName.CompareC(pStruct->m_pName) && uCount-- ) {

		pStruct++;
		}

	if( uCount && TypeName == pStruct->m_pName ) {
		
		for( UINT n = 0; n < pStruct->m_uCount; n++ ) {

			CDoMoreField Field = pStruct->m_Fields[n];

			Combo.AddString(Field.m_pName, Field.m_uType);
			}
		}
	}

void CDoMoreAddrDialog::ShowAddress(CAddress Addr)
{
	if( m_pDoMoreDrv->IsStructuredType(Addr.a.m_Table) ) {

		GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

		CString Text;

		m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

		Text = Text.Mid(m_pSpace->m_Prefix.GetLength());

		SetAddressText(Text.Left(Text.Find('.')));

		LoadCombo();
		}
	else {
		CStdAddrDialog::ShowAddress(Addr);
		}
	}


void CDoMoreAddrDialog::ShowDetails(void)
{
	if( m_pDoMoreDrv->IsStructuredType(m_pSpace->m_uTable) ) {
		
		GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

		CString  Min;

		CString  Max;

		CString  Rad;

		if( !m_pSpace->IsNamed() ) {

			CAddress Addr;

			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			m_pDriver->ExpandAddress(Min, m_pConfig, Addr);

			Addr.a.m_Offset = m_pSpace->m_uMaximum & 0x07F;

			m_pDriver->ExpandAddress(Max, m_pConfig, Addr);

			Rad = m_pSpace->GetRadixAsText();

			Min = Min.Left(Min.Find('.'));

			Max = Max.Left(Max.Find('.'));
			}

		GetDlgItem(3004).SetWindowText(Min);
	
		GetDlgItem(3006).SetWindowText(Max);

		GetDlgItem(3008).SetWindowText(Rad);
		}
	else {
		CStdAddrDialog::ShowDetails();
		}
	}

// End of file
