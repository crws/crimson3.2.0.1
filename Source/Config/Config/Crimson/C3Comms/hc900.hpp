
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HC900_HPP
	
#define	INCLUDE_HC900_HPP

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Base Device Options
//

class CHoneywell900BaseDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHoneywell900BaseDeviceOptions(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CArray <BYTE>        m_Blob;
		CArray <WORD>        m_List;
		CMap   <WORD, UINT>  m_Index;
		UINT		     m_Alarms;
		CString		     m_Path;
		CString		     m_File;
		CString		     m_Time;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Network Device Options
//

class CHoneywell900DeviceOptions : public CHoneywell900BaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHoneywell900DeviceOptions(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		DWORD m_Addr;
		DWORD m_Fail;
		DWORD m_Back;
		UINT  m_HMI;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Serial Device Options
//

class CHoneywell900SerialDeviceOptions : public CHoneywell900BaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHoneywell900SerialDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Driver
//

class CHoneywell900Driver : public CBasicCommsDriver
{
	public:
		// Constructor
		CHoneywell900Driver(BOOL fNet);

		// Binding Control
		UINT GetBinding(void);

		// Driver Data
		UINT GetFlags(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Tag Import
		BOOL MakeTags(IMakeTags * pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
		BOOL IsAddrNamed(CItem *pConfig, CAddress const &Addr);

	protected:
		// Data Members
		BOOL m_fNet;
	};

// End of File

#endif
