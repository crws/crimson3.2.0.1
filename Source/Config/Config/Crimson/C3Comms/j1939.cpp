

#include "intern.hpp"

#include "j1939.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// J1939 Parameter List
//

static PARAM m_SPNList[] = {	

	1, // PGN"0 -TSC1   01/2008 - 8
	{
		
	{  "695", "Engine Override Control Mode",				type2bits	},
	{  "696", "Engine Requested Speed Control Conditions",			type2bits	},
	{  "897", "Override Control Mode Priority",				type2bits	},
	{  "9999","DEAD",							type2bits	},
	{  "898", "Engine Requested Speed/Speed Limit",				type2bytes	},
	{  "518", "Engine Requested Torque/Torque Limit",			type1byte	},
	{  "3349","TSC1 Transmission Rate",					type3bits	},
	{  "3350","TSC1 Control Purpose",					type5bits	},
	{  "4191","Engine Requested Torque - High Resolution",			type4bits	},
	{  "9999","DEAD",							type12bits	},
	{  "4206","Message Counter",						type4bits	},
	{  "4207","Message Checksum",						type4bits	},
	{  "0",   "END!!",							0		},
	},
    
	2, // PGN 256 -TC1  01/2008 - 8 less SPNs 4242, 4255
	{

	{  "681", "Transmission Gear Shift Inhibit Request",			type2bits	},
	{  "682", "Transmission Torque Converter Lockup Disable Request",	type2bits	},
	{  "683", "Disengage Driveline Request",				type2bits	},
	{ "9999", "DEAD",							type2bits	},
	{  "684", "Requested Percent Clutch Slip",				type1byte	},
	{  "525", "Transmission Requested Gear",				type1byte	},
	{  "685", "Disengage Differential Lock Request - Front Axle 1",		type2bits	},
	{  "686", "Disengage Differential Lock Request - Front Axle 2",		type2bits	},
	{  "687", "Disengage Differential Lock Request - Rear  Axle 1",		type2bits	},
	{  "688", "Disengage Differential Lock Request - Rear  Axle 2",		type2bits	},
	{  "689", "Disengage Differential Lock Request - Central",		type2bits	},
	{  "690", "Disengage Differential Lock Request - Central Front",	type2bits	},
	{  "691", "Disengage Differential Lock Request - Central Rear",		type2bits	},
	{ "9999", "DEAD",							type2bits	},
	{ "1852", "Transmission Mode 1",					type2bits	},
	{ "1853", "Transmission Mode 2",					type2bits	},
	{ "1854", "Transmission Mode 3",					type2bits	},
	{ "1855", "Transmission Mode 4",					type2bits	},
	{ "9999", "DEAD",							type6bits	},
	{ "2985", "Transmission Shift Selector Display Mode Switch",		type2bits	},
	{ "4246", "Transmission Mode 5",					type2bits	},
	{ "4247", "Transmission Mode 6",					type2bits	},
	{ "4248", "Transmission Mode 7",					type2bits	},
	{ "4248", "Transmission Mode 8",					type2bits	},
	{    "0", "END!!",							0		},
	},
     
	3, // PGN 1024 -XBR    01/2008 - 8
	{
		
	{ "2920", "External Acceleration Demand",				type2bytes	},
	{ "2914", "XBR EBI Mode",						type2bits	},
	{ "2915", "XBR Priority",						type2bits	},
	{ "2916", "XBR Control Mode",						type2bits	},
	{ "9999", "DEAD",							type2bits	},
	{ "4099", "XBR urgency",						type1byte	},
	{ "9999", "DEAD",							type3bytes	},					
	{ "3189", "XBR Message Counter",					type4bits	},
	{ "3188", "XBR Message Checksum",					type4bits	},
	{   "0",  "END!!",							0		},
	},

      
	4, // PGN 1792 -GPV4   01/2008 - 8
	{
		
	{ "4086", "Valve Load Sense Pressure",				type2bytes	},
	{ "4087", "Valve Pilot Pressure",				type1byte	},
	{ "4088", "Valve Assembly Load sense Pressure",			type2bytes	},
	{ "4089", "Valve Assembly Supply Pressure",			type2bytes	},
//	{ "9999", "DEAD",						type1byte	},
	{ "0",	  "END!!",							0		},
	},
       
	5, // PGN 42240 -AUXIO4	   01/2008 - 8
	{

	{ "3907", "Auxiliary I/O #84",					type2bits	},
	{ "3906", "Auxiliary I/O #83",					type2bits	},
	{ "3905", "Auxiliary I/O #82",					type2bits	},
	{ "3904", "Auxiliary I/O #81",					type2bits	},
	{ "3911", "Auxiliary I/O #88",					type2bits	},
	{ "3910", "Auxiliary I/O #87",					type2bits	},
	{ "3909", "Auxiliary I/O #86",					type2bits	},
	{ "3908", "Auxiliary I/O #85",					type2bits	},
	{ "3915", "Auxiliary I/O #92",					type2bits	},
	{ "3914", "Auxiliary I/O #91",					type2bits	},
	{ "3913", "Auxiliary I/O #90",					type2bits	},
	{ "3912", "Auxiliary I/O #89",					type2bits	},
	{ "3919", "Auxiliary I/O #96",					type2bits	},
	{ "3918", "Auxiliary I/O #95",					type2bits	},
	{ "3917", "Auxiliary I/O #94",					type2bits	},
	{ "3916", "Auxiliary I/O #93",					type2bits	},
	{ "3923", "Auxiliary I/O #100",					type2bits	},
	{ "3922", "Auxiliary I/O #99",					type2bits	},
	{ "3921", "Auxiliary I/O #98",					type2bits	},
	{ "3920", "Auxiliary I/O #97",					type2bits	},
	{ "3927", "Auxiliary I/O #104",					type2bits	},
	{ "3926", "Auxiliary I/O #103",					type2bits	},
	{ "3925", "Auxiliary I/O #102",					type2bits	},
	{ "3924", "Auxiliary I/O #101",					type2bits	},
	{ "3931", "Auxiliary I/O #108",					type2bits	},
	{ "3930", "Auxiliary I/O #107",					type2bits	},
	{ "3929", "Auxiliary I/O #106",					type2bits	},
	{ "3928", "Auxiliary I/O #105",					type2bits	},
	{ "3935", "Auxiliary I/O #112",					type2bits	},
	{ "3934", "Auxiliary I/O #111",					type2bits	},
	{ "3933", "Auxiliary I/O #110",					type2bits	},
	{ "3932", "Auxiliary I/O #109",					type2bits	},
	{    "0", "END!!",						0		},				
	},
	
	6, // PGN 42496 -AUXIO3	   01/2008 - 8
	{

	{ "3875", "Auxiliary I/O #52",					type2bits	},
	{ "3874", "Auxiliary I/O #51",					type2bits	},
	{ "3873", "Auxiliary I/O #50",					type2bits	},
	{ "3872", "Auxiliary I/O #49",					type2bits	},
	{ "3879", "Auxiliary I/O #56",					type2bits	},
	{ "3878", "Auxiliary I/O #55",					type2bits	},
	{ "3877", "Auxiliary I/O #54",					type2bits	},
	{ "3876", "Auxiliary I/O #53",					type2bits	},
	{ "3883", "Auxiliary I/O #60",					type2bits	},
	{ "3882", "Auxiliary I/O #59",					type2bits	},
	{ "3881", "Auxiliary I/O #58",					type2bits	},
	{ "3880", "Auxiliary I/O #57",					type2bits	},
	{ "3887", "Auxiliary I/O #64",					type2bits	},
	{ "3886", "Auxiliary I/O #63",					type2bits	},
	{ "3885", "Auxiliary I/O #62",					type2bits	},
	{ "3884", "Auxiliary I/O #61",					type2bits	},
	{ "3891", "Auxiliary I/O #68",					type2bits	},
	{ "3890", "Auxiliary I/O #67",					type2bits	},
	{ "3889", "Auxiliary I/O #66",					type2bits	},
	{ "3888", "Auxiliary I/O #65",					type2bits	},
	{ "3895", "Auxiliary I/O #72",					type2bits	},
	{ "3894", "Auxiliary I/O #71",					type2bits	},
	{ "3893", "Auxiliary I/O #70",					type2bits	},
	{ "3892", "Auxiliary I/O #69",					type2bits	},
	{ "3899", "Auxiliary I/O #76",					type2bits	},
	{ "3898", "Auxiliary I/O #75",					type2bits	},
	{ "3897", "Auxiliary I/O #74",					type2bits	},
	{ "3896", "Auxiliary I/O #72",					type2bits	},
	{ "3903", "Auxiliary I/O #80",					type2bits	},
	{ "3902", "Auxiliary I/O #79",					type2bits	},
	{ "3901", "Auxiliary I/O #78",					type2bits	},
	{ "3900", "Auxiliary I/O #77",					type2bits	},
	{   "0",  "END!!",						0		},				
	},
      
	7, // PGN 42752 -AUXIO2	   01/2008 - 8
	{

	{ "3843", "Auxiliary I/O #20",					type2bits	},
	{ "3842", "Auxiliary I/O #19",					type2bits	},
	{ "3841", "Auxiliary I/O #18",					type2bits	},
	{ "3840", "Auxiliary I/O #17",					type2bits	},
	{ "3847", "Auxiliary I/O #24",					type2bits	},
	{ "3846", "Auxiliary I/O #23",					type2bits	},
	{ "3845", "Auxiliary I/O #22",					type2bits	},
	{ "3844", "Auxiliary I/O #21",					type2bits	},
	{ "3851", "Auxiliary I/O #28",					type2bits	},
	{ "3850", "Auxiliary I/O #27",					type2bits	},
	{ "3849", "Auxiliary I/O #26",					type2bits	},
	{ "3848", "Auxiliary I/O #25",					type2bits	},
	{ "3855", "Auxiliary I/O #32",					type2bits	},
	{ "3854", "Auxiliary I/O #31",					type2bits	},
	{ "3853", "Auxiliary I/O #30",					type2bits	},
	{ "3852", "Auxiliary I/O #29",					type2bits	},
	{ "3859", "Auxiliary I/O #36",					type2bits	},
	{ "3858", "Auxiliary I/O #35",					type2bits	},
	{ "3857", "Auxiliary I/O #34",					type2bits	},
	{ "3856", "Auxiliary I/O #33",					type2bits	},
	{ "3863", "Auxiliary I/O #40",					type2bits	},
	{ "3862", "Auxiliary I/O #39",					type2bits	},
	{ "3861", "Auxiliary I/O #38",					type2bits	},
	{ "3860", "Auxiliary I/O #37",					type2bits	},
	{ "3867", "Auxiliary I/O #44",					type2bits	},
	{ "3866", "Auxiliary I/O #43",					type2bits	},
	{ "3865", "Auxiliary I/O #42",					type2bits	},
	{ "3864", "Auxiliary I/O #41",					type2bits	},
	{ "3871", "Auxiliary I/O #48",					type2bits	},
	{ "3870", "Auxiliary I/O #47",					type2bits	},
	{ "3869", "Auxiliary I/O #46",					type2bits	},
	{ "3868", "Auxiliary I/O #45",					type2bits	},
	{    "0", "END!!",						0		},				
	},
	
	8, // PGN 43008 -DISP1	  01/2008 - OK
	{

	{"3613", "Text Display Instructions",				type4bits	},
	{"9999", "DEAD",						type12bits	},
	{"3614","Text Display Index",					type1byte	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{"3615","Text Display",						type4bytes	},
	{   "0","END!!",						0		},					
	},
	
	9,// PGn 43264 -FLIC	 01/2008 - 8
	{
		
	{"3564","Lane Departure Warning Enable Command",		type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	("9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	10,// PGn 44544 -TPRS	01/2008	 - 8
	{
		
	{"3192", "Tire Location",					type8bits	},
	{"3193", "Reference Tire Pressure Setting",			type1byte	},
//	{"9999", "DEAD",						type4bytes	},
//	{"9999", "DEAD",						type2bytes	},
	{   "0","END!!",						0		},
	},
	
	11, // PGn 52992 -CTL	01/2008	- 8
	{

	{"1784","Engine Speed Limit Request - Minimum Continuous",	type1byte	},
	{"1785","Engine Speed Limit Request - Maximum Continuous",	type1byte	},
	{"1786","Engine Torque Limit Request - Minimum Continuous",	type1byte	},
	{"1787","Engine Torque Limit Request - Maximum Continuous",	type1byte	},
	{"1788","Minimum Continuous Retarder Speed Limit Request",	type1byte	},
	{"1789","Maximum Continuous Retarder Speed Limit Request",	type1byte	},
	{"1790","Minimum Continuous Retarder Torque Limit Request",	type1byte	},
	{"1791","Maximum Continuous Retarder Torque Limit Request",	type1byte	},
	{   "0","END!!",						0		},
	},
      
	12, // PGn 53248 -CL	01/2008	- 8
	{
		
	{"1487","Illumination Brightness Percent",			type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	}
	{   "0","END!!",						0		},
	},
	
	13, // PGn 53504 -ASC6	01/2008	- 8
	{

	{"1732","Level Preset Front Axle Left",				type2bytes	},
	{"1757","Level Preset Front Axle Right",			type2bytes	},
	{"1758","Level Preset Rear Axle Left",				type2bytes	},
	{"1735","Level Preset Rear Axle Right",				type2bytes	},
	{   "0","END!!",						0		},
	},
	
	14,// PGn 53760 -ASC2	01/2008 - 8
	{

	{"2984","Automatic traction help (load transfer)",		type2bits	},
	{"1749","Kneeling Request Left Side",				type2bits	},
	{"1748","Kneeling Request Right Side",				type2bits	},
	{"1747","Kneeling Control Mode Request",			type2bits	},
	{"1751","Nominal Level Request Front Axle",			type4bits	},
	{"1750","Nominal Level Request Rear Axle",			type4bits	},
	{"1753","Level Control Mode Request",				type4bits	},
	{"1752","Lift Axle 1 Position Command",				type2bits	},
	{"1828","Lift Axle 2 Position Command",				type2bits	},
	{"1718","Damper Stiffness Request Front Axle",			type1byte	},
	{"1719","Damper Stiffness Request Rear Axle",			type1byte	},
	{"1720","Damper Stiffness Request Lift / Tag Axle",		type1byte	},
	{"1830","Kneeling Command - Front Axle",			type2bits	},
	{"1829","Kneeling Command - Rear Axle",				type2bits	},
	{"3215","Prohibit air suspension control",			type2bits	},
//	{"9999","DEAD",							type2bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	15,// PGn 54528 -TDA	01/2008 - 8
	{

	{"1603","Adjust seconds",					type1byte	},
  	{"1604","Adjust minutes",					type1byte	},
	{"1605","Adjust hours",						type1byte	},
	{"1606","Adjust month",						type1byte	},
	{"1607","Adjust day",						type1byte	},
	{"1608","Adjust year",						type1byte	},
	{"1609","Adjust local minute offset",				type1byte	},
	{"1610","Adjust local hour offset",				type1byte	},
	{   "0","END!!",						0		},
	},
	
	16,// PGn 56320 -ATS	01/2008	- 8
	{

	{"1194","Anti-theft Encryption Seed Present Indicator",		type2bits	},
	{"1195","Anti-theft Password Valid Indicator",			type2bits	},
	{"1196","Anti-theft Component Status States",			type2bits	},
	{"1197","Anti-theft Modify Password States",			type2bits	},
	{"1198","Anti-theft Random Number",				type3bytes	},
	{"1198","Anti-theft Random Number",				type4bytes	},
	{   "0","END!!",						0		},	
	},
	
	17,// PGn 56576 -ATR	01/2008	- 8
	{

	{"9999","DEAD",							type1bit	},
	{"1199","Anti-theft Encryption Indicator States",		type2bits	},
	{"1200","Anti-theft Desired Exit Mode States",			type2bits	},
	{"1201","Anti-theft Command States",				type3bits	},
	{"1202","Anti-theft Password Representation",			type3bytes	},
       	{"1202","Anti-theft Password Representation",			type4bytes	},
	{   "0","END!!",						0		},			
	},
	
	18,// PGn 56832 -RESET	01/2008 - 8
	{

	{ "988","Trip Group 1",						type2bits	},
	{ "989","Trip Group 2 - Proprietary",				type2bits	},
	{"9999","DEAD",							type4bits	},
	{"1584","Service Component Identification",			type1byte	},
	{"1211","Engine Build Hours Reset",				type2bits	},
	{"3600","Steering Straight Ahead Position Reset",		type2bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},	
	},
	
	19,// PGn 57344 -CM1	01/2008	- 8
	{

	{ "986","Requested Percent Fan Speed",				type1byte	},
	{"1691", "Cab Interior Temperature Command",			type2bytes	},
	{"1684","Auxiliary Heater Coolant Pump Request",		type2bits	},
	{"1682", "Battery Main Switch Hold Request",			type2bits	},
	{"1714","Operator Seat Direction Switch",			type2bits	},
	{"1856","Seat Belt Switch",					type2bits	},
	{"9999","DEAD",							type2bits	},
	{"1655","Vehicle Limiting Speed Governor Decrement Switch",	type2bits	},
	{"1654","Vehicle Limiting Speed Governor Increment Switch.",	type2bits	},
	{"1653", "Vehicle Limiting Speed Governor Enable Switch",	type2bits	},
	{"3695","Particulate Trap Regeneration Inhibit Switch",		type2bits	},
	{"3696","Particulate Trap Regeneration Force Switch",		type2bits	},
	{"1666","Automatic Gear Shifting Enable Switch",		type2bits	},
	{"1656","Engine Automatic Start Enable Switch",			type2bits	},
	{"1683", "Auxiliary Heater Mode Request",			type4bits	},
	{"1685","Request Engine Zone Heating",				type2bits	},
	{"1686","Request Cab Zone Heating",				type2bits	},
	{"2596","Selected Maximum Vehicle Speed Limit",			type1byte	},
	{   "0","END!!",						0		},
	},
	
	20,// PGn 61440 -ERC1	01/2008 - 8 less SPNs 4233, 4234
	{

	{ "900","Retarder Torque Mode",					type4bits	},
	{ "571", "Retarder Enable - Brake Assist Switch",		type2bits	},
	{ "572", "Retarder Enable - Shift Assist Switch",		type2bits	},
	{ "520","Actual Retarder - Percent Torque",			type1byte	},
	{"1085","Intended Retarder Percent Torque",			type1byte	},
	{"1082", "Engine Coolant Load Increase",			type2bits	},
	{"1667","Retarder Requesting Brake Light",			type2bits	},
	{"9999","DEAD",							type4bits	},
	{"1480","Controlling Device Source Addr for Retarder Control",	type1byte	},
	{"1715","Drivers Demand Retarder - Percent Torque",		type1byte	},
	{"1716","Retarder Selection, non-engine",			type1byte	},
	{"1717","Actual Maximum Available Retarder - Percent Torque",	type1byte	},
	{   "0","END!!",						0		},	
	},
	
	21, // PGn 61441 -EBC1	01/2008 - 8 less SPN 1238
	{

	{ "561", "ASR Engine Control Active",				type2bits	},
	{ "562", "ASR Brake Control Active",				type2bits	},
	{ "563", "Anti-Lock Braking (ABS) Active",			type2bits	},
	{"1121", "EBS Brake Switch",					type2bits	},
	{ "521", "Brake Pedal Position",				type1byte	},
	{ "575","ABS Off-road Switch",					type2bits	},
	{ "576","ASR Off-road Switch",					type2bits	},
	{ "577","ASR 'Hill Holder' Switch",				type2bits	},
	{"9999","DEAD",							type2bits	},
	{ "972", "Accelerator Interlock Switch",			type2bits	},
	{ "971", "Engine Derate Switch",				type2bits	},
	{ "970","Engine Auxiliary Shutdown Switch",			type2bits	},
	{ "969","Remote Accelerator Enable Switch",			type2bits	},
	{ "973", "Engine Retarder Selection",				type1byte	},
	{"1243", "ABS Fully Operational",				type2bits	},
	{"1439","EBS Red Warning Signal",				type2bits	},
	{"1438","ABS/EBS Amber Warning Signal (Powered Vehicle)",	type2bits	},
	{"1793", "ATC/ASR Information Signal",				type2bits	},
	{"1481", "Controlling Device Source Addr for Brake Control",	type1byte	},
	{"9999","DEAD",							type2bits	},
	{"2911", "Halt brake switch",					type2bits	},
	{"1836","Trailer ABS Status",					type2bits	},
	{"1792", "Tractor-Mounted Trailer ABS Warning Signal",		type2bits	},
	{   "0","END!!",						0		}, 		
	},
	
	22, // PGn 61442 -ETC1	01/2008	- 8
	{

	{ "560","Transmission Driveline Engaged",			type2bits	},
	{ "573", "Transmission Torque Converter Lockup Engaged",	type2bits	},
	{ "574","Transmission Shift In Process",			type2bits	},
	{"9999","DEAD",							type2bits	},
	{ "191", "Transmission Output Shaft Speed",			type2bytes	},
	{ "522", "Percent Clutch Slip",					type1byte	},
	{ "606","Engine Momentary Overspeed Enable",			type2bits	},
	{ "607","Progressive Shift Disable",				type2bits	},
	{"9999","DEAD",							type4bits	},
	{ "161", "Transmission Input Shaft Speed",			type2bytes	},
	{"1482", "Controlling Device Source Addr for Transmission Ctrl",type1byte	},
	{   "0","END!!",						0		},	 		
	},
	
	23, // PGn 61443 -EEC2	01/2008	- 8
	{

	{ "558","Accelerator Pedal 1 Low Idle Switch",			type2bits	},
	{ "559","Accelerator Pedal Kickdown Switch",			type2bits	},
	{"1437","Road Speed Limit Status",				type2bits	},
	{"2970","Accelerator Pedal 2 Low Idle Switch",			type2bits	},
	{  "91","Accelerator Pedal Position 1",				type1byte	},
	{  "92","Engine Percent Load At Current Speed",			type1byte	},
	{ "974","Remote Accelerator Pedal Position",			type1byte	},
	{  "29","Accelerator Pedal Position 2",				type1byte	},
	{"2979","Vehicle Acceleration Rate Limit Status",		type2bits	},
	{"9999","DEAD",							type6bits	},
	{"3357","Actual Maximum Available Engine - Percent Torque",	type1byte	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},	 		
	},
	
	24,// PGn 61444 -EEC1	01/2008 - 8
	{

	{ "899","Engine Torque Mode",					type4bits	},
	{"4154","Actual Engine - Percent Torque High Resolution",	type4bits	},
	{ "512","Driver's Demand Engine - Percent Torque",		type1byte	},
	{ "513","Actual Engine - Percent Torque",			type1byte	},
	{ "190","Engine Speed",						type2bytes	},
	{"1483","Controlling Device Source Addr for Engine Control",	type1byte	},
	{"1675","Engine Starter Mode",					type4bits	},
	{"9999","DEAD",							type4bits	},
	{"2432","Engine Demand - Percent Torque",			type1byte	},
	{   "0","END!!",						0		},
	},
	
	25,// PGn 61445 -ETC2	01/2008 - 8
	{

	{ "524","Transmission Selected Gear",				type1byte	},
	{ "526","Transmission Actual Gear Ratio",			type2bytes	},
	{ "523", "Transmission Current Gear",				type1byte	},
	{ "162", "Transmission Requested Range",			type2bytes	},
	{ "163", "Transmission Current Range",				type2bytes	},
	{   "0","END!!",						0		},
	},
	
	26,// PGn 61446 -EAC1	01/2008	- 8
	{

	{ "927","Location",						type8bits	},
	{ "567","Differential Lock State - Front Axle 1",		type2bits	},
	{ "568","Differential Lock State - Front Axle 2",		type2bits	},
	{ "569","Differential Lock State - Rear Axle 1",		type2bits	},
	{ "570","Differential Lock State - Rear Axle 2",		type2bits	},
	{ "564","Differential Lock State - Central",			type2bits	},
	{ "565","Differential Lock State - Central Front",		type2bits	},
	{ "566","Differential Lock State - Central Rear",		type2bits	},
	{"9999","DEAD",							type2bits	},
	{"3819","Front axle group engagement status",			type2bits	},
	{"3820","Rear axle group engagement status",			type2bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},	
	},
	
	27,// PGn 61447 -FLI1	01/2008	 - 8
	{

	{"3565","Lane Departure Left",					type2bits	},
	{"3566","Lane Departure Right",					type2bits	},
	{"1701","Lane Departure Imminent Right Side",			type2bits	},
	{"1700","Lane Departure Imminent Left Side",			type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},	
	},
	
	28,// PGn 61448 -HPG	01/2008 - 8
	{

	{"1762","Hydraulic Pressure",					type2bytes	},
	{"1763","Engine Hydraulic Pressure Governor Mode Indicator",	type2bits	},
	{"1764","Engine Hydraulic Pressure Governor Switch",		type2bits	},
	{"2599","Fire Apparatus Pump Engagement",			type2bits	},
//	{"9999","DEAD",							type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},		
	},
	

	29,// PGn 61449 -VDC2	01/2008	- 8
	{

	{"1807","Steering Wheel Angle",					type2bytes	},
	{"1811", "Steering Wheel Turn Counter",				type6bits	},
	{"1812", "Steering Wheel Angle Sensor Type",			type2bits	},
	{"1808","Yaw Rate",						type2bytes	},
	{"1809","Lateral Acceleration",					type2bytes	},			
	{"1810","Longitudinal Acceleration",				type1byte	},
	{   "0","END!!",						0		},	
	},
	
	30,// PGn 61450 -EGF1	01/2008	- 8
	{

	{"2659","Engine Exhaust Gas Recirculation (EGR) Mass Flow Rate",type2bytes	},
	{ "132","Engine Inlet Air Mass Flow Rate",			type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},	
	},
	
	31, // PGn 61451 -ESC1	01/2008	- 8
	{

	{"2927","Actual Inner wheel steering angle",			type2bytes	},
	{"2928","Axle Location",					type8bits	},
	{"2923","Status of Steering Axle",				type4bits	},
	{"2922","Steerable Lift Axle Lowering Inhibit",			type2bits	},
	{"9999","DEAD",							type2bits	},
	{"2924","Steering Type",					type4bits	},
	{"2925","Type of Steering Forces",				type4bits	},
	{"2926","Type of Steering Transmission",			type4bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},	
	},
	
	32, // PGn 61452 -ETC8	01/2008	- 8
	{

	{"3030","Transmission Torque Converter Ratio",			type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	33, // PGn 61453 -LOI	01/2008	- 8
	{

	{"3156","Blade Control Mode Switch",				type4bits	},
	{"3157","Desired Grade Offset Switch",				type4bits	},
	{"3158","Blade Auto Mode Command",				type4bits	},
	{"3334","Left Blade Control Mode Operator Control",		type4bits	},
	{"3335","Right Blade Control Mode Operator Control",		type4bits	},
	{"3336","Left Desired Blade Offset Operator Control",		type4bits	},
	{"3337","Right Desired Blade Offset Operator Control",		type4bits	},
	{"3338","Side-shift Blade Control Mode Operator Control",	type4bits	},
	{"3339","Side-shift Desired Blade Offset Operator Control",	type4bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},	
	},
	
	34,// PGn 61454 -AT1IG1	01/2008	- 8
	{

	{"3216","Aftertreatment 1 Intake NOx",				type2bytes	},
	{"3217","Aftertreatment 1 Intake %O2",				type2bytes	},
	{"3218","Aftertreatment 1 Intake Gas Sensor Power In Range",	type2bits	},
	{"3219","Aftertreatment 1 Intake Gas Sensor at Temperature",	type2bits	},
	{"3220","Aftertreatment 1 Intake NOx Reading Stable",		type2bits	},
	{"3221","Aftertreatment 1 Intake Wide-Range % O2 Read Stable",	type2bits	},
	{"3222","Aftertreatment 1 Intake Gas Sensor Heater Prelim FMI",	type5bits	},
	{"3223","Aftertreatment 1 Intake Gas Sensor Heater Control",	type2bits	},
	{"9999","DEAD",							type1bit	},
	{"3224","Aftertreatment 1 Intake NOx Sensor Preliminary FMI",	type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3225","Aftertreatment 1 Intake Oxygen Sensor Preliminary FMI",type5bits	},
//	{"9999","DEAD",							type3bits	},
	{   "0","END!!",						0		},	
	},
	
	35,// PGn 61455 -AT1OG1	01/2008	- 8
	{

	{"3226","Aftertreatment 1 Outlet NOx",				type2bytes	},
	{"3227","Aftertreatment 1 Outlet %O2",				type2bytes	},
	{"3228","Aftertreatment 1 Outlet Gas Sensor Power In Range",	type2bits	},
	{"3229","Aftertreatment 1 Outlet Gas Sensor at Temperature",	type2bits	},
	{"3230","Aftertreatment 1 Outlet NOx Reading Stable",		type2bits	},
	{"3231","Aftertreatment 1 Outlet Wide-Range % O2 Read Stable",	type2bits	},
	{"3232","Aftertreatment 1 Outlet Gas Sensor Heater Prelim FMI",	type5bits	},
	{"3233","Aftertreatment 1 Outlet Gas Sensor Heater Control",	type2bits	},
	{"9999","DEAD",							type1bit	},
	{"3234","Aftertreatment 1 Outlet NOx Sensor Preliminary FMI",	type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3235","Aftertreatment 1 Outlet Oxygen Sensor Preliminary FMI",type5bits	},
//	{"9999","DEAD",							type3bits	},
	{   "0","END!!",						0		},	
	},
	

	36,// PGn 61456 -AT2IG1	01/2008 - 8
	{

	{"3255","Aftertreatment 2 Intake NOx",				type2bytes	},
	{"3256","Aftertreatment 2 Intake %O2",				type2bytes	},
	{"3257","Aftertreatment 2 Intake Gas Sensor Power In Range",	type2bits	},
	{"3258","Aftertreatment 2 Intake Gas Sensor at Temperature",	type2bits	},
	{"3259","Aftertreatment 2 Intake NOx Reading Stable",		type2bits	},
	{"3260","Aftertreatment 2 Intake Wide-Range % O2 Read Stable",	type2bits	},
	{"3261", "Aftertreatment 2 Intake Gas Sensor Heater Prelim FMI",type5bits	},
	{"3262", "Aftertreatment 2 Intake Gas Sensor Heater Control",	type2bits	},
	{"9999","DEAD",							type1bit	},
	{"3263", "Aftertreatment 2 Intake NOx Sensor Preliminary FMI",	type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3264","Aftertreatment 2 Intake Oxygen Sensor Preliminary FMI",type5bits	},
//	{"9999","DEAD",							type3bits	},
	{   "0","END!!",						0		},	
	},
	
	37,// PGn 61457 -AT2OG1	01/2008	- 8
	{

	{"3265","Aftertreatment 2 Outlet NOx",				type2bytes	},
	{"3266","Aftertreatment 2 Outlet %O2",				type2bytes	},
	{"3267","Aftertreatment 2 Outlet Gas Sensor Power In Range",	type2bits	},
	{"3268","Aftertreatment 2 Outlet Gas Sensor at Temperature",	type2bits	},
	{"3269","Aftertreatment 2 Outlet NOx Reading Stable",		type2bits	},
	{"3270","Aftertreatment 2 Outlet Wide-Range % O2 Read Stable",	type2bits	},
	{"3271", "Aftertreatment 2 Outlet Gas Sensor Heater Prelim FMI",type5bits	},
	{"3272", "Aftertreatment 2 Outlet Gas Sensor Heater Control",	type2bits	},
	{"9999","DEAD",							type1bit	},
	{"3273", "Aftertreatment 2 Outlet NOx Sensor Preliminary FMI",	type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3274","Aftertreatment 2 Outlet Oxygen Sensor Preliminary FMI",type5bits	},
//	{"9999","DEAD",							type3bits	},
	{   "0","END!!",						0		},	
	},
	
	38,// PGn 61458 -FWSS1	01/2008	- 8
	{

	{"3308","Fifth Wheel Vertical Force",				type2bytes	},
	{"3309","Fifth Wheel Drawbar Force",				type2bytes	},
	{"3310","Fifth Wheel Roll Moment",				type2bytes	},
	{"3317","Fifth Wheel Roll Warning Indicator",			type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},		
	},

	39,// PGn 61459 -SSI	01/2008	- 8
	{

	{"3318","Pitch Angle",						type2bytes	},
       	{"3319","Roll Angle",						type2bytes	},
	{"3322","Pitch Rate",						type2bytes	},
	{"3323","Pitch Angle Figure of Merit",				type2bits	},
	{"3324","Roll Angle Figure of Merit",				type2bits	},
	{"3325","Pitch Rate Figure of Merit",				type2bits	},
	{"3326","Pitch and Roll Compensated",				type2bits	},
	{"3327","Roll and Pitch Measurement Latency",			type1byte	},
	{   "0","END!!",						0		},	
	},

	40,// PGn 61460 -BI	01/2008 - 8
	{

	{"3365","Relative Blade Height",				type2bytes	},
	{"3331","Blade Rotation Angle",					type2bytes	},
	{"3366","Relative Blade Height & Blade Rotation Angle Meas Lat",type1byte	},
	{"3367","Relative Blade Height Figure of Merit",		type2bits	},
	{"3332","Blade Rotation Angle Figure of Merit",			type2bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},	
	},


	41, // PGn 61462 -CCS	01/2008	- 8
	{

	{"3387","Engine Cylinder 1 Combustion Status",			type2bits	},
	{"3388","Engine Cylinder 2 Combustion Status",			type2bits	},
	{"3389","Engine Cylinder 3 Combustion Status",			type2bits	},
	{"3390","Engine Cylinder 4 Combustion Status",			type2bits	},
	{"3391","Engine Cylinder 5 Combustion Status",			type2bits	},
	{"3392","Engine Cylinder 6 Combustion Status",			type2bits	},
	{"3393","Engine Cylinder 7 Combustion Status",			type2bits	},
	{"3394","Engine Cylinder 8 Combustion Status",			type2bits	},
	{"3395","Engine Cylinder 9 Combustion Status",			type2bits	},
	{"3396","Engine Cylinder 10 Combustion Status",			type2bits	},
	{"3397","Engine Cylinder 11 Combustion Status",			type2bits	},
	{"3398","Engine Cylinder 12 Combustion Status",			type2bits	},
	{"3399","Engine Cylinder 13 Combustion Status",			type2bits	},
	{"3400","Engine Cylinder 14 Combustion Status",			type2bits	},
	{"3401","Engine Cylinder 15 Combustion Status",			type2bits	},
	{"3402","Engine Cylinder 16 Combustion Status",			type2bits	},
	{"3403","Engine Cylinder 17 Combustion Status",			type2bits	},
	{"3404","Engine Cylinder 18 Combustion Status",			type2bits	},
	{"3405","Engine Cylinder 19 Combustion Status",			type2bits	},
	{"3406","Engine Cylinder 20 Combustion Status",			type2bits	},
	{"3407","Engine Cylinder 21 Combustion Status",			type2bits	},
	{"3408","Engine Cylinder 22 Combustion Status",			type2bits	},
	{"3409","Engine Cylinder 23 Combustion Status",			type2bits	},
	{"3410","Engine Cylinder 24 Combustion Status",			type2bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},					
	},

	42, // PGn 61463 -KL1	01/2008	- 8
	{

	{"1352","Engine Cylinder 1 Knock Level",			type1byte	},
	{"1353","Engine Cylinder 2 Knock Level",			type1byte	},
	{"1354","Engine Cylinder 3 Knock Level",			type1byte	},
	{"1355","Engine Cylinder 4 Knock Level",			type1byte	},
	{"1356","Engine Cylinder 5 Knock Level",			type1byte	},
	{"1357","Engine Cylinder 6 Knock Level",			type1byte	},
	{"1358","Engine Cylinder 7 Knock Level",			type1byte	},
	{"1359","Engine Cylinder 8 Knock Level",			type1byte	},
	{   "0","END!!",						0		},						
	},

	43, // PGn 61464 -KL2	01/2008	- 8
	{

	{"1360","Engine Cylinder 9 Knock Level",			type1byte	},
	{"1361","Engine Cylinder 10 Knock Level",			type1byte	},
	{"1362","Engine Cylinder 11 Knock Level",			type1byte	},
	{"1363","Engine Cylinder 12 Knock Level",			type1byte	},
	{"1364","Engine Cylinder 13 Knock Level",			type1byte	},
	{"1365","Engine Cylinder 14 Knock Level",			type1byte	},
	{"1366","Engine Cylinder 15 Knock Level",			type1byte	},
	{"1367","Engine Cylinder 16 Knock Level",			type1byte	},
	{   "0","END!!",						0		},						
	},

	44,// PGn 61465 -KL3	01/2008 - 8
	{

	{"1368","Engine Cylinder 17 Knock Level",			type1byte	},
	{"1369","Engine Cylinder 18 Knock Level",			type1byte	},
	{"1370","Engine Cylinder 19 Knock Level",			type1byte	},
	{"1371","Engine Cylinder 20 Knock Level",			type1byte	},
	{"1372","Engine Cylinder 21 Knock Level",			type1byte	},
	{"1373","Engine Cylinder 22 Knock Level",			type1byte	},
	{"1374","Engine Cylinder 23 Knock Level",			type1byte	},
	{"1375","Engine Cylinder 24 Knock Level",			type1byte	},
	{   "0","END!!",						0		},						
	},


	45,// PGn 61466 -TFAC	01/2008 - 8
	{

	{"3464","Engine Throttle Actuator 1 Control Command",		type2bytes	},
	{"3465","Engine Throttle Actuator 2 Control Command",		type2bytes	},
	{ "633","Engine Fuel Actuator 1 Control Command",		type2bytes	},
	{"1244","Engine Fuel Actuator 2 Control Command",		type2bytes	},
	{   "0","END!!",						0		},		
	},

	46,// PGn 61469 -SAS	01/2008	- 8
	{

	{"3683","Steering Wheel Angle",					type2bytes	},
	{"3684","Steering Wheel Angle Range Counter",			type6bits	},
	{"3685","Steering Wheel Angle Range Counter Type",		type2bits	},
	{"9999","DEAD",							type1byte	},
	{"3686","Steering Wheel Angle Range",				type2bytes	},
	{"3687","Steering Angle Sensor Active Mode",			type2bits	},
	{"3688","Steering Angle Sensor Calibrated",			type2bits	},
	{"9999","DEAD",							type4bits	},
	{"3689","Message Counter",					type4bits	},
	{"3690","Message Checksum",					type4bits	},
	{   "0","END!!",						0		},
	},
	
	47,// PGn 64869 -AT1FC2	01/2008	 - 8
	{

	{"4077","Aftertreatment 1 Fuel Pressure 2",			type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	48,// PGn 64870 -ET4	01/2008	- 8
	{

	{"4076","Engine Coolant Temperature 2",				type1byte	},
	{"4193","Engine Coolant Pump Outlet Temperature",		type1byte	},
	{"4194","Engine Coolant Thermostat Opening",			type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},

	49,// PGn 64871 -ZNVW	01/2008	- 8
	{

	{"4075","Zero Net Vehicle Weight Change",			type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
				
	50,// PGn 64872 -GCVW	01/2008 - 8
	{

	{ "417","Gross Combination Weight",				type3bytes	},
	{ "413","Net Vehicle Weight Change",				type3bytes	},
//	{"9999","DEAD",							type2bytes	},
   	{   "0","END!!",						0		},
	},

	51, // PGn 64873 -AGCW	01/2008	 - 8
	{

	{"4074","Axle Group Location",					type4bits	},
	{"9999","DEAD",							type4bits	},
	{ "408","Axle Group Empty Weight Calibration",			type2bytes	},
	{ "407","Axle Group Full Weight Calibration",			type2bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	52, // PGn 64874 -AGW	01/2008	 - 8
	{

	{"4073","Axle Group Location",					type4bits	},
	{"9999","DEAD",							type4bits	},
	{ "409","Axle Group Weight",					type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},	
	},

	53, // PGn 64875 -AAGW	01/2008	- 8
	{

	{"4059","Steer Axle Group Weight Available",			type2bits	},
	{"4060","Lift Axle Group Weight Available",			type2bits	},
	{"4061","Drive Axle Group Weight Available",			type2bits	},
	{"4062","Tag Axle Group Weight Available",			type2bits	},
	{"4063","Additional Tractor Axle Group Weight Available",	type2bits	},
	{"4064","Trailer A Axle Group Weight Available",		type2bits	},
	{"4065","Trailer B Axle Group Weight Available",		type2bits	},
	{"4066","Trailer C Axle Group Weight Available",		type2bits	},
	{"4067","Trailer D Axle Group Weight Available",		type2bits	},
	{"4068","Trailer E Axle Group Weight Available",		type2bits	},
	{"4069","Trailer F Axle Group Weight Available",		type2bits	},
	{"4070","Trailer G Axle Group Weight Available",		type2bits	},
	{"4071","Trailer H Axle Group Weight Available",		type2bits	},
	{"4072","Additional Trailer Axle Group Weight Available",	type2bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},				
	},

	54,// PGn 64876 -AT2AC2	01/2008 - 8
	{

	{"3833","Aftertreatment 2 Secondary Air Differential Pressure",	type2bytes	},
	{"3834","Aftertreatment 2 Secondary Air Temperature",		type2bytes	},
	{"3835","Aftertreatment 2 Secondary Air Mass Flow",		type2bytes	},
	{"3838","Aftertreatment 2 Secondary Air Pressure",		type2bytes	},
	{   "0","END!!",						0		},		
	},

	55,// PGn 64877 -AT1AC2	01/2008	 - 8
	{

	{"3830","Aftertreatment 1 Secondary Air Differential Pressure",	type2bytes	},
	{"3831", "Aftertreatment 1 Secondary Air Temperature",		type2bytes	},
	{"3832", "Aftertreatment 1 Secondary Air Mass Flow",		type2bytes	},
	{"3837","Aftertreatment 1 Secondary Air Pressure",		type2bytes	},
	{   "0","END!!",						0		},		
	},

	56,// PGn 64878 -SCR1	01/2008 - 8
	{

	{"3826","Average Catalyst Reagent Consumption",			type2bytes	},
	{"3828","Commanded Catalyst Reagent Consumption",		type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},			
	},

	57,// PGn 64879 -EEC8	01/2008	- 8
	{
		
	{"3821","Engine Exhaust Gas Recirculation (EGR) Valve 2 Ctrl",	type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	58,// PGn 64880 -DRC	01/2008 - 8
	{

	{"3810","Retract Status of ramp 1",				type2bits	},
	{"3811","Enable status of ramp 1",				type2bits	},
	{"3812","Movement status of ramp 1",				type2bits	},
	{"9999","DEAD",							type2bits	},
	{"3813","Retract Status of ramp 2",				type2bits	},
	{"3814","Enable status of ramp 2",				type2bits	},
	{"3815","Movement status of ramp 2",				type2bits	},
	{"9999","DEAD",							type2bits	},
	{"3816","Retract Status of ramp 3",				type2bits	},
	{"3817","Enable status of ramp 3",				type2bits	},
	{"3818","Movement status of ramp 3",				type2bits	},
//	{"9999","DEAD",							type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},			
	},

	59,// PGn 64881 -BSA	01/2008 - 8
	{

	{"3785","Tractor Brake Stroke Axle 1 Left",			type3bits	},
	{"3786","Tractor Brake Stroke Axle 1 Right",			type3bits	},
	{"3787","Tractor Brake Stroke Axle 2 Left",			type3bits	},
	{"3788","Tractor Brake Stroke Axle 2 Right",			type3bits	},
	{"3789","Tractor Brake Stroke Axle 3 Left",			type3bits	},
	{"3790","Tractor Brake Stroke Axle 3 Right",			type3bits	},
	{"3791","Tractor Brake Stroke Axle 4 Left",			type3bits	},
	{"3792","Tractor Brake Stroke Axle 4 Right",			type3bits	},
	{"3793","Tractor Brake Stroke Axle 5 Left",			type3bits	},
	{"3794","Tractor Brake Stroke Axle 5 Right",			type3bits	},
	{"3795","Trailer Brake Stroke Axle 1 Left",			type3bits	},
	{"3796","Trailer Brake Stroke Axle 1 Right",			type3bits	},
	{"3797","Trailer Brake Stroke Axle 2 Left",			type3bits	},
	{"3798","Trailer Brake Stroke Axle 2 Right",			type3bits	},
	{"3799","Trailer Brake Stroke Axle 3 Left",			type3bits	},
	{"3800","Trailer Brake Stroke Axle 3 Right",			type3bits	},
	{"3801","Trailer Brake Stroke Axle 4 Left",			type3bits	},
	{"3802","Trailer Brake Stroke Axle 4 Right",			type3bits	},
	{"3803","Trailer Brake Stroke Axle 5 Left",			type3bits	},
	{"3804","Trailer Brake Stroke Axle 5 Right",			type3bits	},
//	{"9999","DEAD",							type4bits	},
	{   "0","END!!",						0		},						
	},
	
	60,// PGn 64882 -ESV6	01/2008	- 8
	{

	{"1314","Engine Spark Plug 21",					type2bytes	},
	{"1315","Engine Spark Plug 22",					type2bytes	},
	{"1316","Engine Spark Plug 23",					type2bytes	},
	{"1317","Engine Spark Plug 24",					type2bytes	},
	{   "0","END!!",						0		},		
	},

	61, // PGn 64883 -ESV5	01/2008 - 8
	{

	{"1310","Engine Spark Plug 17",					type2bytes	},
	{"1311","Engine Spark Plug 18",					type2bytes	},
	{"1312","Engine Spark Plug 19",					type2bytes	},
	{"1313","Engine Spark Plug 20",					type2bytes	},
	{   "0","END!!",						0		},		
	},


	62, // PGn 64884 -ESV4	01/2008	- 8
	{

	{"1306","Engine Spark Plug 13",					type2bytes	},
	{"1307","Engine Spark Plug 14",					type2bytes	},
	{"1308","Engine Spark Plug 15",					type2bytes	},
	{"1309","Engine Spark Plug 16",					type2bytes	},
	{   "0","END!!",						0		},		
	},

	63, // PGn 64885 -ESV3	01/2008	- 8
	{

	{"1302","Engine Spark Plug 9",					type2bytes	},
	{"1303","Engine Spark Plug 10",					type2bytes	},
	{"1304","Engine Spark Plug 11",					type2bytes	},
	{"1305","Engine Spark Plug 12",					type2bytes	},
	{   "0","END!!",						0		},		
	},

	64,// PGn 64886 -ESV2	01/2008	- 8
	{

	{"1298","Engine Spark Plug 5",					type2bytes	},
	{"1299","Engine Spark Plug 6",					type2bytes	},
	{"1300","Engine Spark Plug 7",					type2bytes	},
	{"1301","Engine Spark Plug 8",					type2bytes	},
	{   "0","END!!",						0		},		
	},

	65,// PGn 64887 -ESV1	01/2008	- 8
	{

	{"1294","Engine Spark Plug 1",					type2bytes	},
	{"1295","Engine Spark Plug 2",					type2bytes	},
	{"1296","Engine Spark Plug 3",					type2bytes	},
	{"1297","Engine Spark Plug 4",					type2bytes	},
	{   "0","END!!",						0		},		
	},


	66,// PGn 64888 -AT2TI	01/2008	- 32
	{

	{"3741","Aftertreatment 2 Trip Fuel Used",			type4bytes	},
	{"3742","Aftertreatment 2 Trip Active Regeneration Time",	type4bytes	},
	{"3743","Aftertreatment 2 Trip Disabled Time",			type4bytes	},
	{"3744","Aftertreatment 2 Trip Active Regenerations",		type4bytes	},
	{"3745","Aftertreatment 2 Trip Passive Regeneration Time",	type4bytes	},
	{"3746","Aftertreatment 2 Trip Passive Regenerations",		type4bytes	},
	{"3747","Aftertreatment 2 Trip Active Regeneration Inhibit Req",type4bytes	},
	{"3748","Aftertreatment 2 Trip Active Regeneration Manual Req",	type4bytes	},
	{   "0","END!!",						0		},		
	},

	67,// PGn 64889 -AT1TI	01/2008 - 32
	{

	{"3733","Aftertreatment 1 Trip Fuel Used",			type4bytes	},
	{"3734","Aftertreatment 1 Trip Active Regeneration Time",	type4bytes	},
	{"3735","Aftertreatment 1 Trip Disabled Time",			type4bytes	},
	{"3736","Aftertreatment 1 Trip Active Regenerations",		type4bytes	},
	{"3737","Aftertreatment 1 Trip Passive Regeneration Time",	type4bytes	},
	{"3738","Aftertreatment 1 Trip Passive Regenerations",		type4bytes	},
	{"3739","Aftertreatment 1 Trip Active Regeneration Inhibit Req",type4bytes	},
	{"3740","Aftertreatment 1 Trip Active Regeneration Manual Req",	type4bytes	},
	{   "0","END!!",						0		},		
	},

	68,// PGn 64890 -AT2S	01/2008	 - 8
	{
									      
	{"3722","Particulate Trap 2 Soot Load Percent",			type1byte	},
	{"3723","Particulate Trap 2 Ash Load Percent",			type1byte	},
	{"3724","Particulate Trap 2 Time Since Last Act Regeneration",	type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},

	69,// PGn 64891 -AT1S	01/2008	- 8
	{
									      
	{"3719","Particulate Trap 1 Soot Load Percent",			type1byte	},
	{"3720","Particulate Trap 1 Ash Load Percent",			type1byte	},
	{"3721", "Particulate Trap 1 Time Since Last Act Regeneration",	type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},
	
	70,// PGn 64892 -PTC1	01/2008 - 8
	{

	{"3697","Particulate Trap Lamp Command",			type3bits	},
	{"9999","DEAD",							type5bits	},
	{"3699","Particulate Trap Passive Regeneration Status",		type2bits	},
	{"3700","Particulate Trap Active Regeneration Status",		type2bits	},
	{"3701", "Particulate Trap Status",				type3bits	},
	{"9999","DEAD",							type1bit	},
	{"3702", "Part Trap Act Regen Inhib Status",			type2bits	},
	{"3703", "Part Trap Act Regen Inhib - Inhibit Switch",		type2bits	},
	{"3704","Part Trap Act Regen Inhib - Clutch Disengaged",	type2bits	},
	{"3705","Part Trap Act Regen Inhib - Service Brake Active",	type2bits	},
	{"3706","Part Trap Act Regen Inhib - PTO Active",		type2bits	},
	{"3707","Part Trap Act Regen Inhib - Acc Pedal Off Idle",	type2bits	},
	{"3708","Part Trap Act Regen Inhib - Out of Neutral",		type2bits	},
	{"3709","Part Trap Act Regen Inhib - Speed Above Allowed Speed",type2bits	},
	{"3710","Part Trap Act Regen Inhib - Parking Brake Not Set",	type2bits	},
	{"3711", "Part Trap Act Regen Inhib - Low Exhaust Gas Temp",	type2bits	},
	{"3712", "Part Trap Act Regen Inhib - System Fault Active",	type2bits	},
	{"3713", "Part Trap Act Regen Inhib - System Timeout",		type2bits	},
	{"3714","Part Trap Act Regen Inhib - Temporary System Lockout",	type2bits	},
	{"3715","Part Trap Act Regen Inhib - Permanent System Lockout",	type2bits	},
	{"3716","Part Trap Act Regen Inhib - Engine Not Warmed Up",	type2bits	},
	{"3717","Part Trap Act Regen Inhib - Speed Below Allowed Speed",type2bits	},
	{"3718","Part Trap Auto Active Regen Initiation Configuration",	type2bits	},
	{"3698","Exhaust System High Temperature Lamp Command",		type3bits	},
	{"4175","Diesel Particulate Filter Active Regen Forced Status",	type3bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},

	71, // PGn 64894 -AFSS	01/2008	- 8
	{

	{"3691","Left Headlamp Dynamic Bending Light",			type3bits	},
	{"3692","Right Headlamp Dynamic Bending Light",			type3bits	},
	{"9999","DEAD",							type2bits	},
	{"3693","Left Headlamp Light Distribution",			type4bits	},
	{"3694","Right Headlamp Light Distribution",			type4bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},		
	},

	72, // PGn 64895 -EC2	01/2008	- 8
	{
		
	{"3670","Maximum Crank Attempts per Start Attempt",		type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	73, // PGn 64897 -EGRBV	01/2008	 - 8
	{
		
	{"3672", "EGR Cooler Bypass Actuator Postion",			type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
       
	74,// PGn 64899 -TCI	01/2008	 - 8
	{
		
	{"3645","Transfer case status",					type3bits	},
//	{"9999","DEAD",							type5bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	75,// PGn 64900 -EFL/P9		01/2008	- 8
	{

	{"3640","Eng Intake Valve Act Oil Pressure for Cylinder #17",	type2bytes	},
	{"3641","Eng Intake Valve Act Oil Pressure for Cylinder #18",	type2bytes	},
	{"3642","Eng Intake Valve Act Oil Pressure for Cylinder #19",	type2bytes	},
	{"3643","Eng Intake Valve Act Oil Pressure for Cylinder #20",	type2bytes	},
	{   "0","END!!",						0		},		
	},

	76,// PGn 64901 -EFL/P8		01/2008	- 8
	{

	{"3636","Eng Intake Valve Act Oil Pressure for Cylinder #13",	type2bytes	},
	{"3637","Eng Intake Valve Act Oil Pressure for Cylinder #14",	type2bytes	},
	{"3638","Eng Intake Valve Act Oil Pressure for Cylinder #15",	type2bytes	},
	{"3639","Eng Intake Valve Act Oil Pressure for Cylinder #16",	type2bytes	},
	{   "0","END!!",						0		},		
	},

	77,// PGn 64902 -EFL/P7		01/2008 - 8
	{

	{"3632","Eng Intake Valve Act Oil Pressure for Cylinder #9",	type2bytes	},
	{"3633","Eng Intake Valve Act Oil Pressure for Cylinder #10",	type2bytes	},
	{"3634","Eng Intake Valve Act Oil Pressure for Cylinder #11",	type2bytes	},
	{"3635","Eng Intake Valve Act Oil Pressure for Cylinder #12",	type2bytes	},
	{   "0","END!!",						0		},		
	},

	78,// PGn 64903 -EFL/P6		01/2008	- 8
	{

	{"3628","Eng Intake Valve Act Oil Pressure for Cylinder #5",	type2bytes	},
	{"3629","Eng Intake Valve Act Oil Pressure for Cylinder #6",	type2bytes	},
	{"3630","Eng Intake Valve Act Oil Pressure for Cylinder #7",	type2bytes	},
	{"3631","Eng Intake Valve Act Oil Pressure for Cylinder #8",	type2bytes	},
	{   "0","END!!",						0		},		
	},

	79,// PGn 64904 -EFL/P5		01/2008 - 8
	{

	{"3624","Eng Intake Valve Act Oil Pressure for Cylinder #5",	type2bytes	},
	{"3625","Eng Intake Valve Act Oil Pressure for Cylinder #6",	type2bytes	},
	{"3626","Eng Intake Valve Act Oil Pressure for Cylinder #7",	type2bytes	},
	{"3627","Eng Intake Valve Act Oil Pressure for Cylinder #8",	type2bytes	},
	{   "0","END!!",						0		},		
	},

	80,// PGn 64905 -VDS2		01/2008	- 8
	{
		
	{"3623","Vehicle Roll",						type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	81, // PGn 64906 -J2012		01/2008	- VAR
	{

	{"3619","Number of J2012 DTCs",					type1byte	},
	{"3620","J2012 DTC - 1",					type1byte	},
	{"3620","J2012 DTC - 1",					type4bytes	},
	{"3621","J2012 DTC Status - 1",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 1",			type7bits	},
	{"3620","J2012 DTC - 2",					type1byte	},
	{"3620","J2012 DTC - 2",					type4bytes	},
	{"3621","J2012 DTC Status - 2",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 2",			type7bits	},
	{"3620","J2012 DTC - 3",					type1byte	},
	{"3620","J2012 DTC - 3",					type4bytes	},
	{"3621","J2012 DTC Status - 3",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 3",			type7bits	},
	{"3620","J2012 DTC - 4",					type1byte	},
	{"3620","J2012 DTC - 4",					type4bytes	},
	{"3621","J2012 DTC Status - 4",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 4",			type7bits	},
	{"3620","J2012 DTC - 5",					type1byte	},
	{"3620","J2012 DTC - 5",					type4bytes	},
	{"3621","J2012 DTC Status - 5",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 5",			type7bits	},
	{"3620","J2012 DTC - 6",					type1byte	},
	{"3620","J2012 DTC - 6",					type4bytes	},
	{"3621","J2012 DTC Status - 6",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 6",			type7bits	},
	{"3620","J2012 DTC - 7",					type1byte	},
	{"3620","J2012 DTC - 7",					type4bytes	},
	{"3621","J2012 DTC Status - 7",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 7",			type7bits	},
	{"3620","J2012 DTC - 8",					type1byte	},
	{"3620","J2012 DTC - 8",					type4bytes	},
	{"3621","J2012 DTC Status - 8",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 8",			type7bits	},
	{"3620","J2012 DTC - 9",					type1byte	},
	{"3620","J2012 DTC - 9",					type4bytes	},
	{"3621","J2012 DTC Status - 9",					type1bit	},
	{"3622","J2012 DTC Occurrence Count - 9",			type7bits	},
	{"3620","J2012 DTC - 10",					type1byte	},
	{"3620","J2012 DTC - 10",					type4bytes	},
	{"3621","J2012 DTC Status - 10",				type1bit	},
	{"3622","J2012 DTC Occurrence Count - 10",			type7bits	},
	{   "0","END!!",						0		},		
	},

	82, // PGn 64907 -AT2GP		01/2008 - 8
	{

	{"3611","Particulate Trap Intake Pressure 2",			type2bytes	},
	{"3612","Particulate Trap Outlet Pressure 2",			type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},			
	},

	83, // PGn 64908 -AT1GP		01/2008	- 8
	{

	{"3609","Particulate Trap Intake Pressure 1",			type2bytes	},
	{"3610","Particulate Trap Outlet Pressure 1",			type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},			
	},
	
	84,// PGn 64912 -AETC		01/2008	- VAR
	{

	{"3558","AETC Data Collection Standard",			type4bits	},
	{"3559","Number of AETC data points",				type4bits	},
	{"3560","AETC Speed Value 1",					type2bytes	},
	{"3561","AETC Torque Value 1",					type2bytes	},
	{"3560","AETC Speed Value 2",					type2bytes	},
	{"3561","AETC Torque Value 2",					type2bytes	},
	{"3560","AETC Speed Value 3",					type2bytes	},
	{"3561","AETC Torque Value 3",					type2bytes	},
	{"3560","AETC Speed Value 4",					type2bytes	},
	{"3561","AETC Torque Value 4",					type2bytes	},
	{"3560","AETC Speed Value 5",					type2bytes	},
	{"3561","AETC Torque Value 5",					type2bytes	},
	{"3560","AETC Speed Value 6",					type2bytes	},
	{"3561","AETC Torque Value 6",					type2bytes	},
	{"3560","AETC Speed Value 7",					type2bytes	},
	{"3561","AETC Torque Value 7",					type2bytes	},
	{"3560","AETC Speed Value 8",					type2bytes	},
	{"3561","AETC Torque Value 8",					type2bytes	},
	{"3560","AETC Speed Value 9",					type2bytes	},
	{"3561","AETC Torque Value 9",					type2bytes	},
	{"3560","AETC Speed Value 10",					type2bytes	},
	{"3561","AETC Torque Value 10",					type2bytes	},
	{"3560","AETC Speed Value 11",					type2bytes	},
	{"3561","AETC Torque Value 11",					type2bytes	},
	{"3560","AETC Speed Value 12",					type2bytes	},
	{"3561","AETC Torque Value 12",					type2bytes	},
	{"3560","AETC Speed Value 13",					type2bytes	},
	{"3561","AETC Torque Value 13",					type2bytes	},
	{"3560","AETC Speed Value 14",					type2bytes	},
	{"3561","AETC Torque Value 14",					type2bytes	},
	{"3560","AETC Speed Value 15",					type2bytes	},
	{"3561","AETC Torque Value 15",					type2bytes	},
	{   "0","END!!",						0		},		
	},

	85,// PGn 64914 -EOI		01/2008	- 8
	{

	{"3543","Engine Operating State",				type4bits	},
	{"4082","Fuel Pump Primer Control",				type2bits	},
	{"9999","DEAD",							type2bits	},
	{"3544","Time Remaining in Engine Operating State",		type2bytes	},
       	{"3608","Engine Fuel Shutoff Vent Control",			type2bits	},
	{ "632","Engine Fuel Shutoff 1 Control",			type2bits	},
	{"2807","Engine Fuel Shutoff 2 Control",			type2bits	},
	{"3601","Engine Fuel Shutoff Valve Leak Test Control",		type2bits	},
	{"3589","Engine Oil Priming Pump Control",			type2bits	},
	{"3602","Engine Oil Pre-heater Control",			type2bits	},
	{"3603","Engine Electrical System Power Conservation Control",	type2bits	},
	{"3604","Engine Block / Coolant Pre-heater Control",		type2bits	},
	{"3605","Engine Coolant Circulating Pump Control",		type2bits	},
	{"3606","Engine Controlled Shutdown Request",			type2bits	},
	{"3607","Engine Emergency (Immediate) Shutdown Indication",	type2bits	},
	{"9999","DEAD",							type10bits	},
	{"3644","Engine Derate Request",				type1byte	},
	{   "0","END!!",						0		},
	},
	
	86,// PGn 64916 -EEC7	01/2008	- 8
	{

	{  "27","Engine Exhaust Gas Recirculation Valve Position",	type2bytes	},
	{"3822","Engine Exhaust Gas Recirculation Valve 2 Position",	type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},			
	},

	87,// PGn 64917 -TRF2	01/2008	- 8
	{

	{"3359","Transmission Oil Filter Restriction Switch",		type2bits	},
	{"3533","Transmission Oil Level Switch",			type2bits	},
	{"9999","DEAD",							type4bits	},
	{"3823","Transmission Torque Converter Oil Outlet Temperature",	type2bytes	},
	{"4177","Transmission Oil Life Remaining",			type1byte	},
//	{"9999","DEAD",							type4bytes	}, 
	{   "0","END!!",						0		},			
	},

	88,// PGn 64920 -AT1HI	01/2008	- 32
	{

	{"3522","Aftertreatment 1 Total Fuel Used",			type4bytes	},
	{"3523","Aftertreatment 1 Total Active Regeneration Time",	type4bytes	},
	{"3524","Aftertreatment 1 Total Disabled Time",			type4bytes	},
	{"3525","Aftertreatment 1 Total Active Regenerations",		type4bytes	},
	{"3725","Aftertreatment 1 Total Passive Regeneration Time",	type4bytes	},
	{"3726","Aftertreatment 1 Total Passive Regenerations",		type4bytes	},
	{"3727","Aftertreatment 1 Total Act Regeneration Inhibit Req",	type4bytes	},
	{"3728","Aftertreatment 1 Total Active Regeneration Manual Req",type4bytes	},
	{   "0","END!!",						0		},		
	},

	89,// PGn 64921 -AT2HI	01/2008 - 32
	{

	{"3526","Aftertreatment 2 Total Fuel Used",			type4bytes	},
	{"3527","Aftertreatment 2 Total Active Regeneration Time.",	type4bytes	},
	{"3528","Aftertreatment 2 Total Disabled Time",			type4bytes	},
	{"3529","Aftertreatment 2 Total Active Regenerations",		type4bytes	},
	{"3729","Aftertreatment 2 Total Passive Regeneration Time",	type4bytes	},
	{"3730","Aftertreatment 2 Total Passive Regenerations",		type4bytes	},
	{"3731","Aftertreatment 2 Total Act Regeneration Inhibit Req",	type4bytes	},
	{"3732","Aftertreatment 2 Total Active Regeneration Manual Req",type4bytes	},
	{   "0","END!!",						0		},		
	},
	
	90,// PGn 64923 -CRI1	01/2008 - 8
	{

	{"3515","Catalyst Reagent Temperature 2",			type1byte	},
	{"3516","Catalyst Reagent Concentration",			type1byte	},
	{"3518","Catalyst Reagent Conductivity",			type1byte	},
	{"3519","Catalyst Reagent Temperature 2 Preliminary FMI",	type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3520","Catalyst Reagent Properties Preliminary FMI",		type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3521","Catalyst Reagent Type",				type4bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},		
	},

	91, // PGn 64924 -SEP2	01/2008	- 8
	{

	{"3513","Sensor supply voltage 5",				type2bytes	},
	{"3514","Sensor supply voltage 6",				type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},			
	},

	92, // PGn 64925 -SEP1	01/2008	 - 8
	{

	{"3509","Sensor supply voltage 1",				type2bytes	},
	{"3510","Sensor supply voltage 2",				type2bytes	},
	{"3511","Sensor supply voltage 3",				type2bytes	},
	{"3512","Sensor supply voltage 4",				type2bytes	},
	{   "0","END!!",						0		},		
	},

	93, // PGn 64926 -AT2AC1	01/2008	 - 8
	{

	{"3499","Aftertreatment 2 Supply Air Pressure",			type2bytes	},
	{"3500","Aftertreatment 2 Purge Air Pressure",			type2bytes	},
	{"3501","Aftertreatment 2 Air Pressure Control",		type2bytes	},
	{"3502","Aftertreatment 2 Air Pressure Actuator Position",	type1byte	},
	{"3506","Aftertreatment 2 Air System Relay",			type2bits	},
	{"3505","Aftertreatment 2 Atomization Air Actuator",		type2bits	},
	{"3504","Aftertreatment 2 Purge Air Actuator",			type2bits	},
	{"3503","Aftertreatment 2 Air Enable Actuator",			type2bits	},
	{   "0","END!!",						0		},		
	},

	94,// PGn 64927 -AT1AC1		01/2008	- 8
	{

	{"3485","Aftertreatment 1 Supply Air Pressure",			type2bytes	},
	{"3486","Aftertreatment 1 Purge Air Pressure",			type2bytes	},
	{"3487","Aftertreatment 1 Air Pressure Control",		type2bytes	},
	{"3488","Aftertreatment 1 Air Pressure Actuator Position",	type1byte	},
	{"3492","Aftertreatment 1 Air System Relay",			type2bits	},
	{"3491","Aftertreatment 1 Atomization Air Actuator",		type2bits	},
	{"3490","Aftertreatment 1 Purge Air Actuator",			type2bits	},
	{"3489","Aftertreatment 1 Air Enable Actuator",			type2bits	},
	{   "0","END!!",						0		},		
	},

	95,// PGn 64928 -AT2FC		01/2008 - 8
	{

	{"3494","Aftertreatment 2 Fuel Pressure",			type2bytes	},
	{"3495","Aftertreatment 2 Fuel Rate",				type2bytes	},
	{"3493","Aftertreatment 2 Fuel Pressure Control",		type2bytes	},
	{"4098","Aftertreatment 2 Fuel Drain Actuator",			type2bits	},
	{"3498","Aftertreatment 2 Ignition",				type2bits	},
	{"3497","Aftertreatment 2 Regeneration Status",			type2bits	},
	{"3496","Aftertreatment 2 Fuel Enable Actuator",		type2bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},		
	},
	
	96,// PGn 64929 -AT1FC1		01/2008	- 8
	{

	{"3480","Aftertreatment 1 Fuel Pressure 1",			type2bytes	},
	{"3481","Aftertreatment 1 Fuel Rate",				type2bytes	},
	{"3479","Aftertreatment 1 Fuel Pressure Control",		type2bytes	},
	{"4097","Aftertreatment 1 Fuel Drain Actuator",			type2bits	},
	{"3484","Aftertreatment 1 Ignition",				type2bits	},
	{"3483","Aftertreatment 1 Regeneration Status",			type2bits	},
	{"3482","Aftertreatment 1 Fuel Enable Actuator",		type2bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},		
	},

	97,// PGn 64930 -GFI3		01/2008	- 8
	{

	{"3466","Engine Fuel Valve 2 Inlet Absolute Pressure",		type2bytes	},
	{"3467","Engine Gas 2 Mass Flow Rate",				type2bytes	},
	{"3468","Engine Fuel Temperature 2",				type1byte	},
	{"9999","DEAD",							type1byte	},
	{"3469","Engine Fuel Valve 2 Outlet Absolute Pressure",		type2bytes	},
	{   "0","END!!",						0		},
	},

	98,// PGn 64931 -EEC6		01/2008	- 8
	{

	{"3470","Engine Turbocharger Compressor Control",		type2bytes	},
	{ "641","Engine Variable Geometry Turbocharger Actuator #1",	type1byte	},
	{"3675","Engine Turbocharger Compressor Bypass Act Position",	type1byte	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},		
	},

	99,// PGn 64932 -PTODE		01/2008	- 8
	{

	{"3455","Enable Switch - Transfer case output shaft PTO",	type2bits	},
	{"3454","Enable Switch - Transmission output shaft PTO",	type2bits	},
	{"3453","Enable Switch - Transmission input shaft PTO 2",	type2bits	},
	{"3452","Enable Switch - Transmission input shaft PTO 1",	type2bits	},
	{"3939","Enable Switch - PTO Engine Flywheel",			type2bits	},
	{"3942","Enable Switch - PTO Engine Accessory Drive 1",		type2bits	},
	{"3945","Enable Switch - PTO Engine Accessory Drive 2",		type2bits	},
	{"9999","DEAD",							type2bits	},
	{"3459","Engagement Consent - Transfer case output shaft PTO",	type2bits	},
	{"3458","Engagement Consent - Transmission output shaft PTO",	type2bits	},
	{"3457","Engagement Consent - Transmission input shaft PTO 2",	type2bits	},
	{"3456","Engagement Consent - Transmission input shaft PTO 1",	type2bits	},
	{"3940","Engagement Consent - PTO Engine Flywheel",		type2bits	},
	{"3943","Engagement Consent - PTO Engine Accessory Drive 1",	type2bits	},
	{"3946","Engagement Consent - PTO Engine Accessory Drive 2",	type2bits	},
	{"9999","DEAD",							type2bits	},
	{"3463","Engagement Status - Transfer case output shaft PTO",	type2bits	},
	{"3462","Engagement Status - Transmission output shaft PTO",	type2bits	},
	{"3461","Engagement Status - Transmission input shaft PTO 2",	type2bits	},
	{"3460","Engagement Status - Transmission input shaft PTO 1",	type2bits	},
	{"3941","Engagement Status - PTO Engine Flywheel",		type2bits	},
	{"3944","Engagement Status - PTO Engine Accessory Drive 1",	type2bits	},
	{"3947","Engagement Status - PTO Engine Accessory Drive 2",	type2bits	},
	{"9999","DEAD",							type2bits	},
	{"3948","At least one PTO engaged",				type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},			
	},

	100,// PGn 64933 -DC2	01/2008	- 8
	{

	{"3412","Lock Status of Door 1",				type2bits	},
	{"3413","Open Status of Door 1",				type2bits	},
	{"3414","Enable Status of Door 1",				type2bits	},
	{"3415","Lock Status of Door 2",				type2bits	},
	{"3416","Open Status of Door 2",				type2bits	},
	{"3417","Enable Status of Door 2",				type2bits	},
	{"3418","Lock Status of Door 3",				type2bits	},
	{"3419","Open Status of Door 3",				type2bits	},
	{"3420","Enable Status of Door 3",				type2bits	},
	{"3421","Lock Status of Door 4",				type2bits	},
	{"3422","Open Status of Door 4",				type2bits	},
	{"3423","Enable Status of Door 4",				type2bits	},
	{"3424","Lock Status of Door 5",				type2bits	},
	{"3425","Open Status of Door 5",				type2bits	},
	{"3426","Enable Status of Door 5",				type2bits	},
	{"3427","Lock Status of Door 6",				type2bits	},
	{"3428","Open Status of Door 6",				type2bits	},
	{"3429","Enable Status of Door 6",				type2bits	},
	{"3430","Lock Status of Door 7",				type2bits	},
	{"3431","Open Status of Door 7",				type2bits	},
	{"3432","Enable Status of Door 7",				type2bits	},
	{"3433","Lock Status of Door 8",				type2bits	},
	{"3434","Open Status of Door 8",				type2bits	},
	{"3435","Enable Status of Door 8",				type2bits	},
	{"3436","Lock Status of Door 9",				type2bits	},
	{"3437","Open Status of Door 9",				type2bits	},
	{"3438","Enable Status of Door 9",				type2bits	},
	{"3439","Lock Status of Door 10",				type2bits	},
	{"3440","Open Status of Door 10",				type2bits	},
	{"3441","Enable Status of Door 10",				type2bits	},
//	{"9999","DEAD",							type4bits	},
	{   "0","END!!",						0		},			
	},

	101, // PGn 64936 -WCM2		01/2008	- 8
	{

	{"3442","Network Transceiver Status 2",				type8bits	},
	{"3443","Network Service Status 2",				type8bits	},
	{"3444","Network Antenna Status 2",				type8bits	},
	{"3445","Network Signal Strength 2",				type1byte	},
	{"3446","Wireless Communication Network Type 2",		type8bits	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
	
	102, // PGn 64937 -WCM1		01/2008 - 8
	{

	{"3368","Network Transceiver Status 1",				type8bits	},
	{"3369","Network Service Status 1",				type8bits	},
	{"3370","Network Antenna Status 1",				type8bits	},
	{"3371","Network Signal Strength 1",				type1byte	},
	{"3372","Wireless Communication Network Type 1",		type8bits	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
  	},
	
	103, // PGn 64938 -EFL/P4	01/2008 - 8
	{

	{"3340","Engine Charge Air Cooler 1 Inlet Pressure",		type1byte	},
	{"3341","Engine Charge Air Cooler 2 Inlet Pressure",		type1byte	},
	{"3342","Engine Coolant Pump Differential Pressure",		type1byte	},
	{"3343","Engine Centrifugal Oil Filter speed",			type2bytes	},
	{"3668","Engine Intercooler Coolant Level",			type1byte	},
	{"3676","Engine Aftercooler Coolant Level",			type1byte	},
	{"2631","Engine Charge Air Cooler Outlet Pressure",		type1byte	},
	{   "0","END!!",						0		},		
	},

	104,// PGn 64942 -FWSS2		01/2008	- 8
	{

	{"3307","Fifth Wheel Error Status",				type4bits	},
	{"3312","Fifth Wheel Lock Ready to Couple Indicator",		type2bits	},
	{"3313","Fifth Wheel Lock Couple Status Indicator",		type2bits	},
	{"3311","Fifth Wheel Slider Position",				type1byte	},
	{"3316","Fifth Wheel Slider Lock Indicator",			type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
										
	105,// PGn 64943 -AT2IMG	01/2008	- 8
	{

	{"3283","Aftertreatment 2 Exhaust Gas Temperature 2",		type2bytes	},
	{"3284","Aftertreatment 2 Part Trap Intermediate Gas Temp",	type2bytes	},
	{"3285","Aftertreatment 2 Particulate Trap Dif Pressure",	type2bytes	},
	{"3286","Aftertreatment 2 Exhaust Gas Temp 2 Preliminary FMI",	type5bits	},
	{"3287","Aftertreatment 2 Part Trap Delta Pressure Prelim FMI",	type5bits	},
	{"3288","Aftertreatment 2 Part Trap Inter Gas Temp Prelim FMI",	type5bits	},
//	{"9999","DEAD",							type1bit	},
	{   "0","END!!",						0		},		
	},
	
	106,// PGn 64944 -AT2OG2	01/2008 - 8
	{

	{"3279","Aftertreatment 2 Exhaust Gas Temperature 3",		type2bytes	},
	{"3280","Aftertreatment 2 Particulate Trap Outlet Gas Temp",	type2bytes	},
	{"3281","Aftertreatment 2 Exhaust Gas Temperature 3 Prelim FMI",type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3282","Aftertreatment 2 Part Trap Exhaust Gas Temp Prel FMI",	type5bits	},
//	{"9999","DEAD",							type3bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},
	
	107,// PGn 64945 -AT2IG2	01/2008	- 8
	{

	{"3275","Aftertreatment 2 Exhaust Gas Temperature 1",		type2bytes	},
	{"3276","Aftertreatment 2 Particulate Trap Intake Gas Temp",	type2bytes	},
	{"3277","Aftertreatment 2 Exhaust Gas Temperature 1 Prelim FMI",type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3278","Aftertreatment 2 Part Trap Intake Gas Temp Prelim FMI",type5bits	},
//	{"9999","DEAD",							type3bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},
	
	108,// PGn 64946 -AT1IMG	01/2008	- 8
	{

	{"3249","Aftertreatment 1 Exhaust Gas Temperature 2",		type2bytes	},
	{"3250","Aftertreatment 1 Part Trap Intermediate Gas Temp",	type2bytes	},
	{"3251","Aftertreatment 1 Particulate Trap Dif Pressure",	type2bytes	},
	{"3252","Aftertreatment 1 Exhaust Gas Temperature 2 Prelim FMI",type5bits	},
	{"3253","Aftertreatment 1 Part Trap Delta Pressure Prelim FMI",	type5bits	},
	{"3254","Aftertreatment 1 Part Trap Inter Gas Temp Prelim FMI",	type5bits	},
//	{"9999","DEAD",							type1bit	},
	{   "0","END!!",						0		},		
	},
	
	109,// PGn 64947 -AT1OG2	01/2008	- 8
	{

	{"3245","Aftertreatment 1 Exhaust Gas Temperature 3",		type2bytes	},
	{"3246","Aftertreatment 1 Particulate Trap Outlet Gas Temp",	type2bytes	},
	{"3247","Aftertreatment 1 Exhaust Gas Temperature 3 Prelim FMI",type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3248","Aftertreatment 1 Part Trap Out Ex Gas Temp Prelim FMI",type5bits	},
//	{"9999","DEAD",							type3bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},

	110,// PGn 64948 -AT1IG2	01/2008	- 8
	{

	{"3241","Aftertreatment 1 Exhaust Gas Temperature 1",		type2bytes	},
	{"3242","Aftertreatment 1 Particulate Trap Intake Gas Temp",	type2bytes	},
	{"3243","Aftertreatment 1 Exhaust Gas Temperature 1 Prelim FMI",type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3244","Aftertreatment 1 Part Trap Intake Gas Temp Prelim FMI",type5bits	},
//	{"9999","DEAD",							type3bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},
	
	111, // PGn 64953 -TPRI		01/2008 - 8
	{

	{"3190","Tire Location",					type8bits	},
	{"3191","Reference Tire Pressure",				type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},

	112, // PGn 64954 -TR6		01/2008	- 8
	{

	{"3179","Farebox Emergency Status",				type2bits	},
	{"9999","DEAD",							type6bits	},
	{"3181","Farebox Alarm Identifier",				type7bits	},
//	{"9999","DEAD",							type1bit	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},

	113, // PGn 64955 -TR5		01/2008	- 8
	{

	{"3170","Transaction Type",					type4bits	},
	{"3171", "Passenger Type",					type4bits	},
	{"3176","Type of Fare",						type4bits	},
	{"3177","Payment Details",					type4bits	},
	{"3165","Fare Validity",					type4bits	},
	{"3166","Pass Category",					type4bits	},
	{"3167","Initial Fare Agency",					type5bits	},
	{"9999","DEAD",							type3bits	},
	{"3172", "Type of Service",					type3bits	},
	{"3173", "Transfer Type",					type5bits	},
	{"3169","Route Number",						type12bits	},
	{"9999","DEAD",							type4bits	},
	{"3168","Transfer Sold",					type1byte	},
	{   "0","END!!",						0		},		
	},
	
	114,// PGn 64956 -TR4		01/2008	- 15
	{

	{"3178","Farebox Service Status",				type2bits	},
	{"3180","Trip Status",						type3bits	},
	{"9999","DEAD",							type3bits	},
	{"3174","Trip Direction",					type4bits	},
	{"9999","DEAD",							type4bits	},
	{"3175","Fare Presets",						type8bits	},
	{"3159","Trip Number",						type2bytes	},
	{"3161", "Pattern Number",					type2bytes	},
	{"3160","Assigned Route",					type2bytes	},
	{"3162", "Assigned Run",					type2bytes	},
	{"3163", "Assigned Block",					type2bytes	},
	{"3164","Driver's farebox security code",			type2bytes	},
	{   "0","END!!",						0		},		
	},

	115,// PGn 64957 -TR3		01/2008	- 8
	{

	{"9999","DEAD",							type2bits	},
	{"3081","Range Code Enable",					type2bits	},
	{"3080","Transit Route ID Usage",				type2bits	},
	{"3079","Intersection Preemption Request/Response",		type2bits	},
	{"3084","Priority of Response Sent by Emitter",			type4bits	},
	{"3083","Transit Door Enable",					type2bits	},
	{"3082","Strobe Activation Control Status",			type2bits	},
	{"3085","Vehicle ID",						type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},		
	},
	
	116,// PGn 64958 -TR1	01/2008 - VAR
	{

	{"3078","Agency",						type1byte	},
      	{"3071", "Number of bytes in the Transit Assigned Route Id",	type1byte	},
	{"3072", "Number of bytes in the Transit Assigned Run Id",	type1byte	},
	{"3073", "Number of bytes in the Transit Assigned Block Id",	type1byte	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3074","Transit Assigned Route Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3075","Transit Assigned Run Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{"3076","Transit Assigned Block Identity",			type4bytes	},
	{   "0","END!!",						0		},
	},
	
	117,// PGn 64959 -TR2	01/2008	- VAR
	{

	{"3070","Number of bytes in the Milepost Identification",	type1byte	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{ "509","Milepost Identification",				type4bytes	},
	{   "0","END!!",						0		}, 
	},
	
	118,// PGn 64960 -TR7		01/2008	 - 8
	{

	{"3043", "Type of Passenger Count",				type8bits	},
	{"3047","Patron Count",						type1byte	},
	{"3044","Silent Alarm Status",					type2bits	},
       	{"3045","Vehicle Use Status",					type2bits	},
	{"3046","Transit Run Status",					type2bits	},
//	{"9999","DEAD",							type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},			
	},
	
	119,// PGn 64961 -EFL/P3	01/2008	- 8
	{

	{"2948","Engine Intake Valve Actuation System Oil Pressure",	type2bytes	},
	{"3358","Engine Exhaust Gas Recirculation Inlet Pressure",	type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},			
	},
	
	120,// PGn 64964 -EBC5		01/2008 - 8
	{

	{"3829","Brake Temperature Warning",				type2bits	},
	{"2913", "Halt brake mode",					type3bits	},
	{"2912", "Hill holder mode",					type3bits	},
	{"2919","Foundation Brake Use",					type2bits	},
	{"2917","XBR System State",					type2bits	},
	{"2918","XBR Active Control Mode",				type4bits	},
	{"2921", "XBR Acceleration Limit",				type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},			
	},
	
	121, // PGn 64965 -ECUID	01/2008	- VAR
	{

	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2901", "ECU Part Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2902", "ECU Serial Number",					type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
/*	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
      	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
      	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
      	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
      	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2903", "ECU Location",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
	{"2904","ECU Type",						type4bytes	},
 */	{   "0","END!!",						0		},			
	},
	
	122, // PGn 64966 -		01/2008	- 8
	{

	{ "626","Engine Start Enable Device 1",				type2bits	},
	{"1804","Engine Start Enable Device 2",				type2bits	},
	{"9999","DEAD",							type4bits	},
	{"2899","Engine Start Enable Device 1 Configuration",		type4bits	},
	{"2898","Engine Start Enable Device 2 Configuration",		type4bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2byte	},
	{   "0","END!!",						0		},				
	},
	
	123, // PGn 64967 -OHCSS	01/2008 - 8
	{

	{"2896","Engine Auxiliary Governor State",			type2bits	},
	{"2890","Engine Multi-Unit Sync State",				type2bits	},
	{"2891","Engine Alternate Low Idle Select State",		type2bits	},
	{"9999","DEAD",							type2bits	},
	{"2888","Engine Alternate Rating Select State",			type8bits	},
	{"2889","Engine Alternate Droop Accelerator 1 Select State",	type4bits	},
	{"2893","Engine Alternate Droop Accelerator 2 Select State",	type4bits	},
	{"2894","Engine Alternate Droop Remote Acc Select State",	type4bits	},
	{"2895","Engine Alternate Droop Auxiliary Input Select State",	type4bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},			
	},
	
	124,// PGn 64968 -ISCS		01/2008 - 8
	{
		
	{"2892","Engine Operator Primary Inter Speed Select State",	type4bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
	
	125,// PGn 64969 -CMI		01/2008	- 8
	{
		
	{"2887","Total Count of Configuration Changes Made",		type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},
	
	126,// PGn 64970 -ISC		01/2008	- 8
	{
		
	{"2880","Engine Operator Primary Intermediate Speed Select",	type4bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
	
	127,// PGn 64971 -OHECS		01/2008 - 8
	{

	{"2884","Engine Auxiliary Governor Switch",			type2bits	},
	{"1377","Engine Synchronization Switch",			type2bits	},
	{"2883","Engine Alternate Low Idle Switch",			type2bits	},
	{"9999","DEAD",							type2bits	},
	{"2882","Engine Alternate Rating Select",			type1byte	},
	{"2881","Engine Alternate Droop Accelerator 1 Select",		type4bits	},
	{"2879","Engine Alternate Droop Accelerator 2 Select",		type4bits	},
	{"2886","Engine Alternate Droop Remote Accelerator Select",	type4bits	},
	{"2885","Engine Alternate Droop Auxiliary Input Select",	type4bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},				
	},

	128,// PGn 64972 -OEL		01/2008	- 8
	{

	{"2873","Work Light Switch",					type4bits	},
	{"2872","Main Light Switch",					type4bits	},
	{"2876","Turn Signal Switch",					type4bits	},
	{"2875","Hazard Light Switch",					type2bits	},
	{"2874","High-Low Beam Switch",					type2bits	},
	{"2878","Operators Desired Back-light",				type1byte	},
	{"2877","Operators Desired - Delayed Lamp Off Time",		type2bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},				
	},

	129,// PGn 64973 -OWW		01/2008	- 8
	{

	{"2864","Front Non-operator Wiper Switch",			type4bits	},
	{"2863","Front Operator Wiper Switch",				type4bits	},
	{"9999","DEAD",							type4bits	},
	{"2865","Rear Wiper Switch",					type4bits	},
	{"2869","Front Operator Wiper Delay Control",			type1byte	},
	{"2870","Front Non-operator Wiper Delay Control",		type1byte	},
	{"2871","Rear Wiper Delay Control",				type1byte	},
	{"9999","DEAD",							type2bits	},
	{"2867","Front Non-operator Washer Switch",			type3bits	},
	{"2866","Front Operator Washer Switch",				type3bits	},
	{"9999","DEAD",							type5bits	},
	{"2868","Rear Washer Function",					type3bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},					
	},

	130,// PGn 64976 -IC2		01/2008	- 8
	{

	{"2809","Engine Air Filter 2 Differential Pressure",		type1byte	},
	{"2810","Engine Air Filter 3 Differential Pressure",		type1byte	},
	{"2811","Engine Air Filter 4 Differential Pressure",		type1byte	},
	{"3562","Engine Intake Manifold #2 Pressure",			type1byte	},
	{"3563","Engine Intake Manifold #1 Absolute Pressure",		type1byte	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},					
	},
	
	131, // PGn 64977 -FMS		01/2008 - 8
	{

	{"2804","FMS-standard Diagnostics Supported",			type2bits	},
	{"2805","FMS-standard Requests Supported",			type2bits	},
	{"9999","DEAD",							type4bits	},
	{"2806","FMS-standard SW-version supported",			type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},					
	},

	132, // PGn 64978 -EP		01/2008	- 8
	{

	{"2803", "Keep-Alive Battery Consumption",			type2bytes	},
	{"2802", "Data Memory Usage",					type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},					
	},

	133, // PGn 64979 -TCI6		01/2008	- 8
	{

	{"2629","Engine Turbocharger 1 Compressor Outlet Temperature",	type2bytes	},
	{"2799","Engine Turbocharger 2 Compressor Outlet Temperature",	type2bytes	},
	{"2800","Engine Turbocharger 3 Compressor Outlet Temperature",	type2bytes	},
	{"2801","Engine Turbocharger 4 Compressor Outlet Temperature",	type2bytes	},
	{   "0","END!!",						0		},				
	},

	134,// PGn 64980 -CM3	01/2008	- 8
	{

	{"2796","Transfer Case Selector Switch",			type3bits	},
	{"9999","DEAD",							type5bits	},
	{"3314","Fifth Wheel Release Control",				type2bits	},
	{"3315","Fifth Wheel Release Control Security Lockout",		type2bits	},
	{"9999","DEAD",							type4bits	},
	{"3809","Transmission Oil Level Request",			type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},				
	},

	135,// PGn 64981 -EEC5	01/2008	- 8
	{

	{"2789","Engine Turbocharger 1 Calculated Turbine Inlet Temp",	type2bytes	},
	{"2790","Engine Turbocharger 1 Calculated Turbine Outlet Temp",	type2bytes	},
	{"2791","Engine Exhaust Gas Recirculation(EGR) Valve Control",	type2bytes	},
	{"2792","Eng Var Geom Turbocharger(VGT) Air Ctrl Shutoff Valve",type2bits	},
	{"9999","DEAD",							type6bits	},
	{"2795","Eng Var Geom Turbocharger(VGT) 1 Actuator Position",	type1byte	},
	{   "0","END!!",						0		},				
	},

	136,// PGn 64982 -BJM1		01/2008	- 8
	{

	{"2675","Joystick 1 X-Axis Neutral Position Status",		type2bits	},
	{"2670","Joystick 1 X-Axis Lever Left Negative Pos Status",	type2bits	},
	{"2665","Joystick 1 X-Axis Lever Right Positive Pos Status",	type2bits	},
	{"2660","Joystick 1 X-Axis Position",				type10bits	},
	{"2676","Joystick 1 Y-Axis Neutral Position Status",		type2bits	},
	{"2671","Joystick 1 Y-Axis Lever Back Negative Pos Status",	type2bits	},
	{"2666","Joystick 1 Y-Axis Lever Forward Positive Pos Stat",	type2bits	},
	{"2661","Joystick 1 Y-Axis Position",				type10bits	},
	{"9999","DEAD",							type4bits	},
	{"2681","Joystick 1 Y-Axis Detent Position Status",		type2bits	},
	{"2680","Joystick 1 X-Axis Detent Position Status",		type2bits	},
	{"2688","Joystick 1 Button 4 Pressed Status",			type2bits	},
	{"2687","Joystick 1 Button 3 Pressed Status",			type2bits	},
	{"2686","Joystick 1 Button 2 Pressed Status",			type2bits	},
	{"2685","Joystick 1 Button 1 Pressed Status",			type2bits	},
	{"2692","Joystick 1 Button 8 Pressed Status",			type2bits	},
	{"2691","Joystick 1 Button 7 Pressed Status",			type2bits	},
	{"2690","Joystick 1 Button 6 Pressed Status",			type2bits	},
	{"2689","Joystick 1 Button 5 Pressed Status",			type2bits	},
	{"2696","Joystick 1 Button 12 Pressed Status",			type2bits	},
	{"2695","Joystick 1 Button 11 Pressed Status",			type2bits	},
	{"2694","Joystick 1 Button 10 Pressed Status",			type2bits	},
	{"2693","Joystick 1 Button 9 Pressed Status",			type2bits	},
	{   "0","END!!",						0		},				
	},

	137,// PGn 64983 -EJM1		01/2008	- 8
	{

	{"2677","Joystick 1 Grip X-Axis Neutral Position Status",	type2bits	},
	{"2672","Joystick 1 Grip X-Axis Lever Left Negative Pos Stat",	type2bits	},
	{"2667","Joystick 1 Grip X-Axis Lever Right Positive Pos Stat",	type2bits	},
	{"2662","Joystick 1 Grip X-Axis Position",			type10bits	},
	{"2678","Joystick 1 Grip Y-Axis Neutral Position Status",	type2bits	},
	{"2673","Joystick 1 Grip Y-Axis Lever Back Negative Pos Stat",	type2bits	},
	{"2668","Joystick 1 Grip Y-Axis Lever Fwrd Positive Pos Stat",	type2bits	},
	{"2663","Joystick 1 Grip Y-Axis Position",			type10bits	},
	{"2679","Joystick 1 Theta-Axis Neutral Position Status",	type2bits	},
	{"2674","Joystick 1 Theta-Axis Cntr Clockwise Neg Pos Stat",	type2bits	},
	{"2669","Joystick 1 Theta-Axis Clockwise Positive Pos Stat",	type2bits	},
	{"2664","Joystick 1 Theta-Axis Position",			type10bits	},
	{"9999","DEAD",							type2bits	},
	{"2684","Joystick 1 Theta-Axis Detent Position Status",		type2bits	},
	{"2683","Joystick 1 Grip Y-Axis Detent Position Status",	type2bits	},
	{"2682","Joystick 1 Grip X-Axis Detent Position Status",	type2bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},					
	},

	138,// PGn 64984 -BJM2		01/2008 - 8
	{

	{"2712","Joystick 2 X-Axis Neutral Position Status",		type2bits	},
	{"2707","Joystick 2 X-Axis Lever Left Negative Pos Status",	type2bits	},
	{"2702","Joystick 2 X-Axis Lever Right Positive Pos Status",	type2bits	},
	{"2697","Joystick 2 X-Axis Position",				type10bits	},
	{"2713","Joystick 2 Y-Axis Neutral Position Status",		type2bits	},
	{"2708","Joystick 2 Y-Axis Lever Back Negative Pos Status",	type2bits	},
	{"2703","Joystick 2 Y-Axis Lever Forward Positive Pos Stat",	type2bits	},
	{"2698","Joystick 2 Y-Axis Position",				type10bits	},
	{"9999","DEAD",							type4bits	},
	{"2718","Joystick 2 Y-Axis Detent Position Status",		type2bits	},
	{"2717","Joystick 2 X-Axis Detent Position Status",		type2bits	},
	{"2725","Joystick 2 Button 4 Pressed Status",			type2bits	},
	{"2724","Joystick 2 Button 3 Pressed Status",			type2bits	},
	{"2723","Joystick 2 Button 2 Pressed Status",			type2bits	},
	{"2722","Joystick 2 Button 1 Pressed Status",			type2bits	},
	{"2729","Joystick 2 Button 8 Pressed Status",			type2bits	},
	{"2728","Joystick 2 Button 7 Pressed Status",			type2bits	},
	{"2727","Joystick 2 Button 6 Pressed Status",			type2bits	},
	{"2726","Joystick 2 Button 5 Pressed Status",			type2bits	},
	{"2733","Joystick 2 Button 12 Pressed Status",			type2bits	},
	{"2732","Joystick 2 Button 11 Pressed Status",			type2bits	},
	{"2731","Joystick 2 Button 10 Pressed Status",			type2bits	},
	{"2730","Joystick 2 Button 9 Pressed Status",			type2bits	},
	{   "0","END!!",						0		},				
	},
	
	139,// PGn 64985 -EJM2		01/2008	- 8
	{

	{"2714","Joystick 2 Grip X-Axis Neutral Position Status",	type2bits	},
	{"2709","Joystick 2 Grip X-Axis Lever Left Negative Pos Stat",	type2bits	},
	{"2704","Joystick 2 Grip X-Axis Lever Right Positive Pos Stat",	type2bits	},
	{"2699","Joystick 2 Grip X-Axis Position",			type10bits	},
	{"2715","Joystick 2 Grip Y-Axis Neutral Position Status",	type2bits	},
	{"2710","Joystick 2 Grip Y-Axis Lever Back Negative Pos Stat",	type2bits	},
	{"2705","Joystick 2 Grip Y-Axis Lever Fwrd Positive Pos Stat",	type2bits	},
	{"2700","Joystick 2 Grip Y-Axis Position",			type10bits	},
	{"2716","Joystick 2 Theta-Axis Neutral Position Status",	type2bits	},
	{"2711","Joystick 2 Theta-Axis Cntr Clockwise Neg Pos Stat",	type2bits	},
	{"2706","Joystick 2 Theta-Axis Clockwise Positive Pos Stat",	type2bits	},
	{"2701","Joystick 2 Theta-Axis Position",			type10bits	},
	{"9999","DEAD",							type2bits	},
	{"2721","Joystick 2 Theta-Axis Detent Position Status",		type2bits	},
	{"2720","Joystick 2 Grip Y-Axis Detent Position Status",	type2bits	},
	{"2719","Joystick 2 Grip X-Axis Detent Position Status",	type2bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},					
	},

	140,// PGn 64986 -BJM3		01/2008	- 8
	{

	{"2749","Joystick 3 X-Axis Neutral Position Status",		type2bits	},
	{"2744","Joystick 3 X-Axis Lever Left Negative Pos Status",	type2bits	},
	{"2739","Joystick 3 X-Axis Lever Right Positive Pos Status",	type2bits	},
	{"2734","Joystick 3 X-Axis Position",				type10bits	},
	{"2750","Joystick 3 Y-Axis Neutral Position Status",		type2bits	},
	{"2745","Joystick 3 Y-Axis Lever Back Negative Pos Status",	type2bits	},
	{"2740","Joystick 3 Y-Axis Lever Forward Positive Pos Stat",	type2bits	},
	{"2735","Joystick 3 Y-Axis Position",				type10bits	},
	{"9999","DEAD",							type4bits	},
	{"2755","Joystick 3 Y-Axis Detent Position Status",		type2bits	},
	{"2754","Joystick 3 X-Axis Detent Position Status",		type2bits	},
	{"2762","Joystick 3 Button 4 Pressed Status",			type2bits	},
	{"2761","Joystick 3 Button 3 Pressed Status",			type2bits	},
	{"2760","Joystick 3 Button 2 Pressed Status",			type2bits	},
	{"2759","Joystick 3 Button 1 Pressed Status",			type2bits	},
	{"2766","Joystick 3 Button 8 Pressed Status",			type2bits	},
	{"2765","Joystick 3 Button 7 Pressed Status",			type2bits	},
	{"2764","Joystick 3 Button 6 Pressed Status",			type2bits	},
	{"2763","Joystick 3 Button 5 Pressed Status",			type2bits	},
	{"2770","Joystick 3 Button 12 Pressed Status",			type2bits	},
	{"2769","Joystick 3 Button 11 Pressed Status",			type2bits	},
	{"2768","Joystick 3 Button 10 Pressed Status",			type2bits	},
	{"2767","Joystick 3 Button 9 Pressed Status",			type2bits	},
	{   "0","END!!",						0		},				
	},
	
	141, // PGn 64987 -EJM3	   01/2008 - 8
	{

	{"2751","Joystick 3 Grip X-Axis Neutral Position Status",	type2bits	},
	{"2746","Joystick 3 Grip X-Axis Lever Left Negative Pos Stat",	type2bits	},
	{"2741","Joystick 3 Grip X-Axis Lever Right Positive Pos Stat",	type2bits	},
	{"2736","Joystick 3 Grip X-Axis Position",			type10bits	},
	{"2752","Joystick 3 Grip Y-Axis Neutral Position Status",	type2bits	},
	{"2747","Joystick 3 Grip Y-Axis Lever Back Negative Pos Stat",	type2bits	},
	{"2742","Joystick 3 Grip Y-Axis Lever Fwrd Positive Pos Stat",	type2bits	},
	{"2737","Joystick 3 Grip Y-Axis Position",			type10bits	},
	{"2753","Joystick 3 Theta-Axis Neutral Position Status",	type2bits	},
	{"2748","Joystick 3 Theta-Axis Cntr Clockwise Neg Pos Stat",	type2bits	},
	{"2743","Joystick 3 Theta-Axis Clockwise Positive Pos Stat",	type2bits	},
	{"2738","Joystick 3 Theta-Axis Position",			type10bits	},
	{"9999","DEAD",							type2bits	},
	{"2758","Joystick 3 Theta-Axis Detent Position Status",		type2bits	},
	{"2757","Joystick 3 Grip Y-Axis Detent Position Status",	type2bits	},
	{"2756","Joystick 3 Grip X-Axis Detent Position Status",	type2bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},					
	},
	
	142, // PGn 64988 -MCI		01/2008	- 8
	{

	{"2615","Engine Throttle Synchronization Mode Status",		type4bits	},
	{"2616","Trolling Mode Status",					type2bits	},
	{"2617","Slow Vessel Mode Status",				type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},					
	},

	143, // PGn 64991 -FWD		01/2008 - 8
	{

	{"2612","Front Wheel Drive Actuator Status",			type2bits	},
	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},						
	},

	144,// PGn 64992 -AMB2		01/2008	- 8
	{

	{"2610","Solar Intensity Percent",				type1byte	},
	{"2611", "Solar Sensor Maximum",				type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},						
	},

	145,// PGn 64993 -CACI		01/2008 - 8
	{

	{"2609","Cab A/C Refrigerant Compressor Outlet Pressure",	type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},						
	},

	146,// PGn 64994 -SPR		01/2008 - 8
	{

	{"2603", "Pneumatic Supply Pressure Request",			type1byte	},
	{"2604","Parking and/or Trailer Air Pressure Request",		type1byte	},
	{"2605","Service Brake Air Pressure Request, Circuit #1",	type1byte	},
	{"2606","Service Brake Air Pressure Request, Circuit #2",	type1byte	},
	{"2607","Auxiliary Equipment Supply Pressure Request",		type1byte	},
	{"2608","Air Suspension Supply Pressure Request",		type1byte	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},				
	},

	147,// PGn 64995 -EOAC		01/2008	- 8
	{

	{"2601", "Travel Velocity Control Position",			type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},						
	},

	148,// PGn 64996 -EPD		01/2008	- 8
	{

	{"2600","Payload Percentage",					type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},						
	},

	149,// PGn 64997 -MVS		01/2008 - 8
	{

	{"2588","Maximum Vehicle Speed Limit 1",			type1byte	},
	{"2589","Maximum Vehicle Speed Limit 2",			type1byte	},
	{"2590","Maximum Vehicle Speed Limit 3",			type1byte	},
	{"2591","Maximum Vehicle Speed Limit 4",			type1byte	},
	{"2592","Maximum Vehicle Speed Limit 5",			type1byte	},
	{"2593","Maximum Vehicle Speed Limit 6",			type1byte	},
	{"2594","Maximum Vehicle Speed Limit 7",			type1byte	},
	{"2595","Applied Vehicle Speed Limit",				type1byte	},
	{   "0","END!!",						0		},				
	},

	150,// PGn 64998 -HBS		01/2008	 - 8
	{

	{"2580","Hydraulic Brake Pressure Circuit 1",			type1byte	},
	{"2581","Hydraulic Brake Pressure Circuit 2",			type1byte	},
	{"2584","Hydraulic Brake Pressure Warning State Circuit 1",	type2bits	},
	{"2585","Hydraulic Brake Pressure Warning State Circuit 2",	type2bits	},
	{"2582","Hydraulic Brake Pressure Supply State Circuit 1",	type2bits	},
	{"2583","Hydraulic Brake Pressure Supply State Circuit 2",	type2bits	},
	{"2930","Hydraulic Brake System Audible Warning Command",	type2bits	},
	{"2931","Hydraulic Brake Fluid Level Switch",			type2bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},				
	},

	151, // PGn 65031 -ET		01/2008	- 8
	{

	{"2433", "Engine Exhaust Gas Temperature - Right Manifold",	type2bytes	},
	{"2434","Engine Exhaust Gas Temperature - Left Manifold",	type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},					
	},

	152, // PGn 65088 -LD		01/2008 - 8
	{

	{"2404","Running Light",					type2bits	},
	{"2352", "Alternate Beam Head Light Data",			type2bits	},
	{"2350","Low Beam Head Light Data",				type2bits	},
	{"2348","High Beam Head Light Data",				type2bits	},
	{"2388","Tractor Front Fog Lights",				type2bits	},
	{"2386","Rotating Beacon Light",				type2bits	},
	{"2370","Right Turn Signal Lights",				type2bits	},
	{"2368","Left Turn Signal Lights",				type2bits	},
	{"2392", "Back Up Light and Alarm Horn",			type2bits	},
	{"2376","Center Stop Light",					type2bits	},
	{"2374","Right Stop Light",					type2bits	},
	{"2372", "Left Stop Light",					type2bits	},
	{"2384","Implement Clearance Light",				type2bits	},
	{"2382", "Tractor Clearance Light",				type2bits	},
	{"2380","Implement Marker Light",				type2bits	},
	{"2378","Tractor Marker Light",					type2bits	},
	{"2390","Rear Fog Lights",					type2bits	},
	{"2358","Tractor Underside Mounted Work Lights",		type2bits	},
	{"2360","Tractor Rear Low Mounted Work Lights",			type2bits	},
	{"2362", "Tractor Rear High Mounted Work Lights",		type2bits	},
	{"2364","Tractor Side Low Mounted Work Lights",			type2bits	},
	{"2366","Tractor Side High Mounted Work Lights",		type2bits	},
	{"2354","Tractor Front Low Mounted Work Lights",		type2bits	},
	{"2356","Tractor Front High Mounted Work Lights",		type2bits	},
	{"2398","Implement OEM Option 2 Light",				type2bits	},
	{"2396","Implement OEM Option 1 Light",				type2bits	},
	{"2407","Implement Right Facing Work Light",			type2bits	},
	{"2598","Implement Left Facing Work Light",			type2bits	},
	{"9999","DEAD",							type2bits	},
	{"2402", "Implement Right Forward Work Light",			type2bits	},
	{"2400","Implement Left Forward Work Light",			type2bits	},
	{"2394","Implement Rear Work Light",				type2bits	},
	{   "0","END!!",						0		},			
	},

	153, // PGn 65089 -LC	01/2008 less SPN 2393 - 8
	{

	{"2403", "Running Light Command",				type2bits	},
	{"2351", "Alternate Beam Head Light Data Command",		type2bits	},
	{"2349","Low Beam Head Light Data Command",			type2bits	},
	{"2347","High Beam Head Light Data Command",			type2bits	},
	{"2387","Tractor Front Fog Lights Command",			type2bits	},
	{"2385","Rotating Beacon Light Command",			type2bits	},
	{"2369","Right Turn Signal Lights Command",			type2bits	},
	{"2367","Left Turn Signal Lights Command",			type2bits	},
	{"2391", "Back Up Light and Alarm Horn Command",		type2bits	},
	{"2375","Center Stop Light Command",				type2bits	},
	{"2373", "Right Stop Light Command",				type2bits	},
	{"2371", "Left Stop Light Command",				type2bits	},
	{"2383", "Implement Clearance Light Command",			type2bits	},
	{"2381", "Tractor Clearance Light Command",			type2bits	},
	{"2379","Implement Marker Light Command",			type2bits	},
	{"2377","Tractor Marker Light Command",				type2bits	},
	{"2389","Rear Fog Lights Command",				type2bits	},
	{"2357","Tractor Underside Mounted Work Lights Command",	type2bits	},
	{"2359","Tractor Rear Low Mounted Work Lights Command",		type2bits	},
	{"2361", "Tractor Rear High Mounted Work Lights Command",	type2bits	},
	{"2363", "Tractor Side Low Mounted Work Lights Command",	type2bits	},
	{"2365","Tractor Side High Mounted Work Lights Command",	type2bits	},
	{"2353", "Tractor Front Low Mounted Work Lights Command",	type2bits	},
	{"2355","Tractor Front High Mounted Work Lights Command",	type2bits	},
	{"2397","Implement OEM Option 2 Light Command",			type2bits	},
	{"2395","Implement OEM Option 1 Light Command",			type2bits	},
	{"2406","Implement Right Facing Work Light Command",		type2bits	},
	{"2597","Implement Left Facing Work Light Command",		type2bits	},
	{"9999","DEAD",							type2bits	},
	{"2401", "Implement Right Forward Work Light Command",		type2bits	},
	{"2399","Implement Left Forward Work Light Command",		type2bits	},
	{"2405","Implement Rear Work Light Command",			type2bits	},
	{   "0","END!!",						0		},			
	},

	154,// PGn 65098 -ETC7		01/2008 - 8 les SPNs 4176, 4178
	{

	{"9999","DEAD",							type4bits	},
	{"1850","Transmission Requested Range Display Blank State",	type2bits	},
	{"1849","Transmission Requested Range Display Flash State",	type2bits	},
	{"3086","Transmission Ready for Brake Release",			type2bits	},
	{"2945","Active Shift Console Indicator",			type2bits	},
	{"2900","Transmission Engine Crank Enable",			type2bits	},
	{"1851","Transmission Shift Inhibit Indicator",			type2bits	},
	{"2539","Transmission Mode 4 Indicator",			type2bits	},
	{"2538","Transmission Mode 3 Indicator",			type2bits	},
	{"2537","Transmission Mode 2 Indicator",			type2bits	},
	{"2536","Transmission Mode 1 Indicator",			type2bits	},
	{"3289","Transmission Requested Gear Feedback",			type1byte	},
	{"4250","Transmission Mode 5 Indicator",			type2bits	},
	{"4251","Transmission Mode 6 Indicator",			type2bits	},
	{"4252","Transmission Mode 7 Indicator",			type2bits	},
	{"4253","Transmission Mode 8 Indicator",			type2bits	},
	{"4261","Transmission Reverse Gear Shift Inhibit Status",	type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},

	155,// PGn 65099 -TCFG2		01/2008	- VAR
	{
		
	{"1845","Transmission Torque Limit",				type2bytes	},
	{   "0","END!!",						0		},
	},

	156,// PGn 65100 -ML		01/2008	 - 8
	{

	{"1840","Rear Black Out Marker Select",				type2bits	},
	{"1839","Front Black Out Marker Lamp Select",			type2bits	},
	{"1838","Convoy Lamp Select.2 bits",				type2bits	},
	{"1837","Convoy Driving Lamp Select",				type2bits	},
	{"9999","DEAD",							type1byte	},
	{"9999","DEAD",							type6bits	},
	{"1841","Black Out Brake/Stop Lamp Select",			type2bits	},
	{"1843","Night Vision Illuminator Select",			type2bits	},
	{"9999","DEAD",							type4bits	},
	{"1842","Black Out Work Lamp Select",				type2bits	},
	{"9999","DEAD",							type3bytes	},
	{"1844","Operators Black Out Intensity Selection",		type1byte	},
	{   "0","END!!",						0		},		
	},

	157,// PGn 65101 -TAVG		01/2008 - 8
	{

	{"1834","Engine Total Average Fuel Rate",			type2bytes	},
	{"1835","Engine Total Average Fuel Economy",			type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},					
	},

	158,// PGn 65102 -DC1		01/2008	- 8
	{

	{"1821","Position of doors",					type4bits	},
	{"1820","Ramp / Wheel Chair Lift Position",			type2bits	},
	{"3411","Status 2 of doors",					type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},					
	},

	159,// PGn 65103 -VDC1		01/2008	- 8
	{

	{"1813", "VDC Information Signal",				type2bits	},
	{"1814","VDC Fully Operational",				type2bits	},
	{"1815","VDC brake light request",				type2bits	},
	{"9999","DEAD",							type2bits	},
	{"1816","ROP Engine Control active",				type2bits	},
	{"1818","ROP Brake Control active",				type2bits	},
	{"1817","YC Engine Control active",				type2bits	},
	{"1819","YC Brake Control active",				type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},			
	},

	160,// PGn 65104 -BT1		01/2008	 - 8
	{

	{"1800","Battery 1 Temperature",				type1byte	},
	{"1801", "Battery 2 Temperature",				type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	161, // PGn 65105 -ACC2		01/2008 - 8
	{
	{"9999","DEAD",							type5bits	},
	{"1799","Requested ACC Distance Mode",				type3bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	162, // PGn 65106 -VEP3		01/2008 - 8
	{

	{"1795","Alternator Current (High Range/Resolution)",		type2bytes	},
	{"2579","Net Battery Current (High Range/Resolution)",		type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},
	},

	163, // PGn 65107 -RTC1		01/2008 - 8
	{

	{"1776","Low Limit Threshold for Maximum RPM from Retarder",	type1byte	},
	{"1777","High Limit Threshold for Min Cont RPM from Retarder",	type1byte	},
	{"1778","Low Limit Threshold for Maximum Torque from Retarder",	type1byte	},
	{"1779","High Limit Thresh for Min Cont Torque from Retarder",	type1byte	},
	{"1780","Maximum Continuous Retarder Speed",			type1byte	},
	{"1781","Minimum Continuous Retarder Speed",			type1byte	},
	{"1782","Maximum Continuous Retarder Torque",			type1byte	},
	{"1783","Minimum Continuous Retarder Torque",			type1byte	},
	{   "0","END!!",						0		},				
	},

	164,// PGn 65108 -ECT1		01/2008 - 8
	{

	{"1768","Engine Low Limit Threshold for Max RPM from Engine",	type1byte	},
	{"1769","Engine High Limit Threshold for Min Cont Engine RPM",	type1byte	},
	{"1770","Engine Low Limit Thresh for Max Torque from Engine",	type1byte	},
	{"1771", "Engine High Limit Thresh for Min Cont Torq from Eng",	type1byte	},
	{"1772", "Engine Maximum Continuous RPM",			type1byte	},
	{"1773", "Engine Minimum Continuous RPM",			type1byte	},
	{"1774","Engine Maximum Continuous Torque",			type1byte	},
	{"1775","Engine Minimum Continuous Torque",			type1byte	},
	{   "0","END!!",						0		},				
	},

	165,// PGn 65109 -GFD		01/2008	- 8
	{
		
	{"1767","Specific Heat Ratio",					type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	166,// PGn 65110 -TI1		01/2008 - 8
	{

	{"1761", "Catalyst Tank Level",					type1byte	},
	{"3031", "Catalyst Tank Temperature",				type1byte	},
	{"3517","Catalyst Tank Level 2",				type2bytes	},
	{"3532", "Catalyst Tank Level Preliminary FMI",			type5bits	},
//	{"9999","DEAD",							type3bits	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
	
	167,// PGn 65111 -ASC5		01/2008 - 8
	{

	{"1729","Damper Stiffness Front Axle",				type1byte	},
	{"1730","Damper Stiffness Rear Axle",				type1byte	},
	{"1731","Damper Stiffness Lift / Tag Axle",			type1byte	},
	{"1833","Electronic Shock Absorber Control Mode - Front Axle",	type2bits	},
	{"1832","Electronic Shock Absorber Control Mode - Rear Axle",	type2bits	},
	{"1831","Electronic Shock Absorber Ctrl Mode - Lift/Tag Axle",	type2bits	},
//	{"9999","DEAD",							type2bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},
	},

	168,// PGn 65112 -ASC4		01/2008 - 8
	{

	{"1725","Bellow Pressure Front Axle Left",			type2bytes	},
	{"1726","Bellow Pressure Front Axle Right",			type2bytes	},
	{"1727","Bellow Pressure Rear Axle Left",			type2bytes	},
	{"1728","Bellow Pressure Rear Axle Right",			type2bytes	},
	{   "0","END!!",						0		},
	},

	169,// PGn 65113 -ASC3		01/2008	- 8
	{

	{"1721","Relative Level Front Axle Left",			type2bytes	},
	{"1722","Relative Level Front Axle Right",			type2bytes	},
	{"1724","Relative Level Rear Axle Left",			type2bytes	},
	{"1723","Relative Level Rear Axle Right",			type2bytes	},
	{   "0","END!!",						0		},
	},

	170,// PGn 65114 -ASC1		01/2008 - 8
	{

	{"1734","Nominal Level Front Axle",				type4bits	},
	{"1733","Nominal Level Rear Axle",				type4bits	},
	{"1738","Below Nominal Level Front Axle",			type2bits	},
	{"1754","Below Nominal Level Rear Axle",			type2bits	},
	{"1737","Above Nominal Level Front Axle",			type2bits	},
	{"1736","Above Nominal Level Rear Axle",			type2bits	},
	{"1740","Lowering Control Mode Front Axle",			type2bits	},
	{"1755","Lowering Control Mode Rear Axle",			type2bits	},
	{"1739","Lifting Control Mode Front Axle",			type2bits	},
	{"1756","Lifting Control Mode Rear Axle",			type2bits	},
	{"1742","Kneeling Information",					type4bits	},
	{"1741","Level Control Mode",					type4bits	},
	{"1746","Security Device",					type2bits	},
	{"1745","Vehicle Motion Inhibit",				type2bits	},
	{"1744","Door Release",						type2bits	},
	{"1743","Lift Axle 1 Position",					type2bits	},
	{"1824","Front Axle in Bumper Range",				type2bits	},
	{"1823","Rear Axle in Bumper Range",				type2bits	},
	{"9999","DEAD",							type2bits	},
	{"1822","Lift Axle 2 Position",					type2bits	},
	{"1826","Suspension Remote Control 1",				type2bits	},
	{"1825","Suspension Remote Control 2",				type2bits	},
	{"9999","DEAD",							type4bits	},
	{"1827","TSuspension Control Refusal Information",		type4bits	},
//	{"9999","DEAD",							type4bits	},
	{   "0","END!!",						0		},		
	},

	171, // PGn 65115 -FLI2		01/2008	- 8
	{
	
	{"9999","DEAD",							type2bits	},
	{"1711","Lane Tracking Status Right Side",			type2bits	},
	{"1710","Lane Tracking Status Left Side",			type2bits	},
	{"1702","Lane Departure Indication Enable Status",		type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	172, // PGn 65126 -BM		01/2008 - 8
	{
		
	{"1681", "Battery Main Switch Hold State",			type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	173, // PGn 65127 -BM		01/2008 - 8
	{
		
	{"1690","Auxiliary Heater Maximum Output Power",		type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	174,// PGn 65128 -VF		01/2008	- 8
	{

	{"1638","Hydraulic Temperature",				type1byte	},
	{"1713","Hydraulic Oil Filter Restriction Switch",		type2bits	},
	{"1857","Winch Oil Pressure Switch",				type2bits	},
	{"9999","DEAD",							type4bits	},
	{"2602","Hydraulic Oil Level",					type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	175,// PGn 65129 -ET3		01/2008	- 8
	{

	{"1636","Engine Intake Manifold 1 Air Temp (High Resolution)",	type2bytes	},
	{"1637","Engine Coolant Temperature (High Resolution)",		type2bytes	},
	{"2986","Engine Intake Valve Actuation System Oil Temperature",	type2bytes	},
	{"2630","Engine Charge Air Cooler Outlet Temperature",		type2bytes	},
	{   "0","END!!",						0		},
	},
	
	176,// PGn 65130 -EFS		01/2008 - 8
	{

	{"1380","Engine Oil Level Remote Reservoir",			type1byte	},
	{"1381","Engine Fuel Supply Pump Inlet Pressure",		type1byte	},
	{"1382","Engine Fuel Filter (suction side) Diff Pressure",	type1byte	},
	{"3548","Engine Waste Oil Reservoir Level",			type1byte	},
	{"3549","Engine Oil-Filter Outlet Pressure",			type1byte	},
	{"3550","Engine Oil Priming Pump Switch",			type2bits	},
	{"3551","Engine Oil Priming State",				type2bits	},
	{"3552","Engine Oil Pre-Heated State",				type2bits	},
	{"3553","Engine Coolant Pre-heated State",			type2bits	},
	{"3554","Engine Ventilation Status",				type3bits	},
	{"4083","Fuel Pump Primer Status",				type2bits	},
//	{"9999","DEAD",							type3bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	177,// PGn 65131 -DI		01/2008	- VAR
	{

	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1625","Driver 1 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{"1626","Driver 2 identification",				type4bytes	},
	{   "0","END!!",						0		},	 
	},
	
	178,// PGn 65132 -TCO1		01/2008	- 8
	{

	{"1612","Driver 1 working state",				type3bits	},
	{"1613","Driver 2 working state",				type3bits	},
	{"1611","Vehicle motion",					type2bits	},
	{"1617","Driver 1 Time Related States",				type4bits	},
	{"1615","Driver card, driver 1",				type2bits	},
	{"1614","Vehicle Overspeed",					type2bits	},
	{"1618","Driver 2 Time Related States",				type4bits	},
	{"1616","Driver card, driver 2",				type2bits	},
	{"9999","DEAD",							type2bits	},
	{"1622","System event",						type2bits	},
	{"1621","Handling information",					type2bits	},
	{"1620","Tachograph performance",				type2bits	},
	{"1619","Direction indicator",					type2bits	},
	{"1623","Tachograph output shaft speed",			type2bytes	},
	{"1624","Tachograph vehicle speed",				type2bytes	},
	{   "0","END!!",						0		},
	},
	
	179,// PGn 65133 -HTR		01/2008	- 8
	{

	{"1687","Auxiliary Heater Output Coolant Temperature",		type1byte	},
	{"1688","Auxiliary Heater Input Air Temperature",		type1byte	},
	{"1689","Auxiliary Heater Output Power Percent",		type1byte	},
	{"1677","Auxiliary Heater Mode",				type4bits	},
	{"9999","DEAD",							type4bits	},
	{"1676","Auxiliary Heater Water Pump Status",			type2bits	},
	{"1678","Cab Ventilation",					type2bits	},
	{"1679","Engine Heating Zone",					type2bits	},
	{"1680","Cab Heating Zone",					type2bits	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	180,// PGn 65134 -HRW		01/2008	- 8
	{

	{"1592","Front Axle, Left Wheel Speed",				type2bytes	},
	{"1593","Front axle, right wheel speed",			type2bytes	},
	{"1594","Rear axle, left wheel speed",				type2bytes	},
	{"1595","Rear axle, right wheel speed",				type2bytes	},
	{   "0","END!!",						0		},
	},
	
	181, // PGn 65135 -ACC1		01/2008 - 8
	{

	{"1586","Speed of forward vehicle",				type1byte	},
	{"1587","Distance to forward vehicle",				type1byte	},
	{"1588","Adaptive Cruise Control Set Speed",			type1byte	},
	{"1590","Adaptive Cruise Control Mode",				type3bits	},
	{"1589","Adaptive cruise control set distance mode",		type3bits	},
	{"9999","DEAD",							type2bits	},
	{"1591","Road curvature",					type2bytes	},
	{"1798","ACC Target Detected",					type2bits	},
	{"1797","ACC System Shutoff Warning",				type2bits	},
	{"1796","ACC Distance Alert Signal",				type2bits	},
//	{"9999","DEAD",							type2bits	},
//	{"9999","DEAD",							type1byte	},
      	{   "0","END!!",						0		},
	},

	182, // PGn 65136 -CVW		01/2008	- VAR
	{

	{"1585","Powered Vehicle Weight",				type2bytes	},
	{"1760","Gross Combination Vehicle Weight",			type2bytes	},
	{   "0","END!!",						0		},
	},
	
	183, // PGn 65137 -LTP		01/2008	- 8
	{

	{"1579","Speed of forward vehicle",				type2bytes	},
	{"1580","Distance to forward vehicle",				type2bytes	},
	{"1581","Adaptive Cruise Control Set Speed",			type1byte	},
	{"1582","Adaptive Cruise Control Mode",			type8bits	},
	{"1583","Adaptive cruise control set distance mode",		type8bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	184,// PGn 65138 -LBC		01/2008	- 8
	{

	{"1577","Blade Duration and Direction",				type2bytes	},
	{"1578","Blade Control Mode",					type8bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	185,// PGn 65139 -LMP		01/2008	- 8
	{

	{"1576","Mast Position",					type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},
	
	186,// PGn 65140 -LSP		01/2008	- 8
	{

	{"1575","Modify Leveling System Set Point",			type2bytes	},
	{"1759","Blade Height Set Point - High Resolution",		type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},
	
	187,// PGn 65141 -LVD		01/2008	- 8
	{

	{"1574","Laser Strike Vertical Deviation",			type2bytes	},
	{"2576","Laser Receiver Type",					type1byte	},
	{"2793", "Laser Strike Data Latency",				type2bytes	},
	{"2794","Absolute Laser Strike Position",			type2bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	188,// PGn 65142 -LVDD		01/2008	- 8
	{

	{"1573","LED Display Data #1",					type8bits	},
	{"1805","LED Display Mode Control",				type4bits	},
	{"1806","LED Display Deadband Control",				type4bits	},
	{"2578","LED Pattern Control",					type4bits	},
	{"2577","Display Deadbands",					type4bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	189,// PGn 65143 -AP		01/2008	- 8
	{

	{ "136","Auxiliary Vacuum Pressure Reading",			type2bytes	},
	{ "137","Auxiliary Gage Pressure Reading 1",			type2bytes	},
	{ "138","Auxiliary Absolute Pressure Reading",			type2bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	190,// PGn 65144 -TP1		01/2008	- 8
	{

	{  "39","Tire Pressure Check Interval",				type1byte	},
	{"1466","Steer Channel Mode",					type4bits	},
	{"9999","DEAD",							type4bits	},
	{"1467","Trailer/tag Channel Mode",				type4bits	},
	{"1468","Drive Channel Mode",					type4bits	},
	{"1469","PCU Drive Solenoid Status",				type2bits	},
	{"1470","PCU Steer Solenoid Status",				type2bits	},
	{"1471","Tire Pressure Supply Switch Status",			type2bits	},
	{"9999","DEAD",							type2bits	},
	{"1472","PCU Deflate Solenoid Status",				type2bits	},
	{"1473","PCU Control Solenoid Status",				type2bits	},
	{"1474","PCU Supply Solenoid Status",				type2bits	},
	{"1475","PCU Trailer, Tag or Push Solenoid Status",		type2bits	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	191, // PGn 65145 -TP2		01/2008	- 8
	{

	{ "141", "Trailer, Tag Or Push Channel Tire Pressure Target",	type2bytes	},
	{ "142", "Drive Channel Tire Pressure Target",			type2bytes	},
	{ "143", "Steer Channel Tire Pressure Target",			type2bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},	
	},

	192, // PGn 65146 -TP3		01/2008 - 8
	{

	{ "144","Trailer, Tag Or Push Channel Tire Pressure",		type2bytes	},
	{ "145","Drive Channel Tire Pressure",				type2bytes	},
	{ "146","Steer Channel Tire Pressure",				type2bytes	},
	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},	
	},

	193, // PGn 65147 -CT1		01/2008	- 8
	{

	{"1444","Engine Cylinder #1 Combustion Time",			type2bytes	},
	{"1445","Engine Cylinder #2 Combustion Time",			type2bytes	},
	{"1446","Engine Cylinder #3 Combustion Time",			type2bytes	},
	{"1447","Engine Cylinder #4 Combustion Time",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	194,// PGn 65148 -CT2		01/2008	- 8
	{

	{"1448","Engine Cylinder #5 Combustion Time",			type2bytes	},
	{"1449","Engine Cylinder #6 Combustion Time",			type2bytes	},
	{"1450","Engine Cylinder #7 Combustion Time",			type2bytes	},
	{"1451", "Engine Cylinder #8 Combustion Time",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	195,// PGn 65149 -CT3		01/2008 - 8
	{

	{"1452", "Engine Cylinder #9 Combustion Time",			type2bytes	},
	{"1453", "Engine Cylinder #10 Combustion Time",			type2bytes	},
	{"1454","Engine Cylinder #11 Combustion Time",			type2bytes	},
	{"1455","Engine Cylinder #12 Combustion Time",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	196,// PGn 65150 -CT4		01/2008 - 8
	{

	{"1456","Engine Cylinder #13 Combustion Time",			type2bytes	},
	{"1457","Engine Cylinder #14 Combustion Time",			type2bytes	},
	{"1458","Engine Cylinder #15 Combustion Time",			type2bytes	},
	{"1459","Engine Cylinder #16 Combustion Time",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	197,// PGn 65151 -CT5		01/2008	- 8
	{

	{"1460","Engine Cylinder #17 Combustion Time",			type2bytes	},
	{"1461","Engine Cylinder #18 Combustion Time",			type2bytes	},
	{"1462","Engine Cylinder #19 Combustion Time",			type2bytes	},
	{"1463","Engine Cylinder #20 Combustion Time",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	198,// PGn 65152 -CT6		01/2008	- 8
	{

	{"1464","Engine Desired Combustion Time",			type2bytes	},
	{"1465","Engine Average Combustion Time",			type2bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},	
	},

	199,// PGn 65153 -GFI2		01/2008 - 8
	{

	{"1440","Engine Fuel Flow Rate 1",				type2bytes	},
	{"1441","Engine Fuel Flow Rate 2",				type2bytes	},
	{"1442","Engine Fuel Valve 1 Position",				type1byte	},
	{"1443","Engine Fuel Valve 2 Position",				type1byte	},
	{"1765","Engine Requested Fuel Valve 1 Position",		type1byte	},
	{"1766","Engine Requested Fuel Valve 2 Position",		type1byte	},
	{   "0","END!!",						0		},		
	},

	200,// PGn 65154 -IT1		01/2008	- 8
	{

	{"1413","Engine Cylinder #1 Ignition Timing",			type2bytes	},
	{"1414","Engine Cylinder #2 Ignition Timing",			type2bytes	},
	{"1415","Engine Cylinder #3 Ignition Timing",			type2bytes	},
	{"1416","Engine Cylinder #4 Ignition Timing",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	201, // PGn 65155 -IT2		01/2008	- 8
	{

	{"1417","Engine Cylinder #5 Ignition Timing",			type2bytes	},
	{"1418","Engine Cylinder #6 Ignition Timing",			type2bytes	},
	{"1419","Engine Cylinder #7 Ignition Timing",			type2bytes	},
	{"1420","Engine Cylinder #8 Ignition Timing",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	202, // PGn 65156 -IT3		01/2008 - 8
	{

	{"1421","Engine Cylinder #9 Ignition Timing",			type2bytes	},
	{"1422","Engine Cylinder #10 Ignition Timing",			type2bytes	},
	{"1423","Engine Cylinder #11 Ignition Timing",			type2bytes	},
	{"1424","Engine Cylinder #12 Ignition Timing",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	203, // PGn 65157 -IT4		01/2008 - 8
	{

	{"1425","Engine Cylinder #13 Ignition Timing",			type2bytes	},
	{"1426","Engine Cylinder #14 Ignition Timing",			type2bytes	},
	{"1427","Engine Cylinder #15 Ignition Timing",			type2bytes	},
	{"1428","Engine Cylinder #16 Ignition Timing",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	204,// PGn 65158 -IT5		01/2008 - 8
	{

	{"1429","Engine Cylinder #17 Ignition Timing",			type2bytes	},
	{"1430","Engine Cylinder #18 Ignition Timing",			type2bytes	},
	{"1431","Engine Cylinder #19 Ignition Timing",			type2bytes	},
	{"1432","Engine Cylinder #20 Ignition Timing",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	205,// PGn 65159 -IT6		01/2008	 - 8
	{

	{"1433","Engine Desired Ignition Timing #1",			type2bytes	},
	{"1434","Engine Desired Ignition Timing #2",			type2bytes	},
	{"1435","Engine Desired Ignition Timing #3",			type2bytes	},
	{"1436","Engine Actual Ignition Timing",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	206,// PGn 65160 -ISO1		01/2008 - 8
	{

	{"1393","Engine Cylinder #1 Ignition Transformer Secondary Out",type1byte	},
	{"1394","Engine Cylinder #2 Ignition Transformer Secondary Out",type1byte	},
	{"1395","Engine Cylinder #3 Ignition Transformer Secondary Out",type1byte	},
	{"1396","Engine Cylinder #4 Ignition Transformer Secondary Out",type1byte	},
	{"1397","Engine Cylinder #5 Ignition Transformer Secondary Out",type1byte	},
	{"1398","Engine Cylinder #6 Ignition Transformer Secondary Out",type1byte	},
	{"1399","Engine Cylinder #7 Ignition Transformer Secondary Out",type1byte	},
	{"1400","Engine Cylinder #8 Ignition Transformer Secondary Out",type1byte	},
	{   "0","END!!",						0		},
	},

	207,// PGn 65161 -ISO2		01/2008 - 8
	{

	{"1401","Engine Cylinder #9 Ignition Transformer Secondary Out",type1byte	},
	{"1402","Engine Cylinder #10 Ignition Transformer Secondary Out",type1byte	},
	{"1403","Engine Cylinder #11 Ignition Transformer Secondary Out",type1byte	},
	{"1404","Engine Cylinder #12 Ignition Transformer Secondary Out",type1byte	},
	{"1405","Engine Cylinder #13 Ignition Transformer Secondary Out",type1byte	},
	{"1406","Engine Cylinder #14 Ignition Transformer Secondary Out",type1byte	},
	{"1407","Engine Cylinder #15 Ignition Transformer Secondary Out",type1byte	},
	{"1408","Engine Cylinder #16 Ignition Transformer Secondary Out",type1byte	},
	{   "0","END!!",						0		},
	},

	208,// PGn 65162 -ISO2		01/2008	- 8
	{				        

	{"1409","Engine Cylinder #17 Ignition Transformer Secondary Out",type1byte	},
	{"1410","Engine Cylinder #18 Ignition Transformer Secondary Out",type1byte	},
	{"1411","Engine Cylinder #19 Ignition Transformer Secondary Out",type1byte	},
	{"1412","Engine Cylinder #20 Ignition Transformer Secondary Out",type1byte	},
//	{"9999","DEAD",							 type4bytes	},
	{   "0","END!!",						0		},
	},

	209,// PGn 65163 -GFP		01/2008	- 8
	{

	{"1390","Engine Fuel Valve 1 Inlet Absolute Pressure",		type2bytes	},
	{"1391","Engine Fuel Valve Differential Pressure",		type2bytes	},
	{"1392","Engine Air to Fuel Differential Pressure",		type2bytes	},
	{"2980","Engine Fuel Valve 1 Outlet Absolute Pressure",		type2bytes	},
	{   "0","END!!",						0		},	
	},

	210,// PGn 65164 -AAI		01/2008	- 8
	{

	{ "441","Auxiliary Temperature 1",				type1byte	},
	{ "442","Auxiliary Temperature 2",				type1byte	},
	{"1387","Auxiliary Pressure #1",				type1byte	},
	{"1388","Auxiliary Pressure #2",				type1byte	},
	{"3087","Auxiliary Level",					type2bytes	},
	{ "354","Relative Humidity",					type1byte	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	211, // PGn 65165 -VEP2		01/2008 - 8 
	{

	{ "444","Battery Potential / Power Input 2",			type2bytes	},
	{"3597","ECU Power Output Supply Voltage #1",			type2bytes	},
	{"3598","ECU Power Output Supply Voltage #2",			type2bytes	},
	{"3599","ECU Power Output Supply Voltage #3",			type2bytes	},
	{   "0","END!!",						0		},	
	},

	212, // PGn 65166 -S2		01/2008 - 8
	{

	{"1379","Service Component Identification",			type1byte	},
	{"1350","Time Since Last Service",				type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},	
	},

	213, // PGn 65167 -SP2		01/2008 - 8
	{

	{"1320","Engine External Shutdown Air Supply Pressure",		type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},	
	},

	214,// PGn 65168 -ETH		01/2008 - VAR
	{

	{"1246","Number of Engine Torque History Records",		type1byte	},
	{"1247","Engine Power",						type2bytes	},
	{"1248","Engine Peak Torque 1",					type2bytes	},
	{"1249","Engine Peak Torque 2",					type2bytes	},
	{"1250","Calibration Record Start Month",			type1byte	},
	{"1251","Calibration Record Start Day",				type1byte	},
	{"1252","Calibration Record Start Year",			type1byte	},
	{"1253","Calibration Record Duration Time",			type4bytes	},
	{"1254","Torque Limiting Feature Status",			type2bits	},
	{"1632","Engine Torque Limit Feature",				type3bits	},
	{"9999","DEAD",							type3bits	},
	{"1255","Transmission Gear RatiO 1",				type2bytes	},
	{"1256","Engine Torque Limit 1 Transmission",			type2bytes	},
	{"1257","Transmission Gear RatiO 2",				type2bytes	},
	{"1258","Engine Torque Limit 2 Transmission",			type2bytes	},
	{"1259","Transmission Gear Ratio 3",				type2bytes	},
	{"1260","Engine Torque Limit 3 Transmission",			type2bytes	},
	{"1261","Engine Torque Limit 4 Transmission",			type2bytes	},
	{"1262","Engine Torque Limit 5 Switch",				type2bytes	},
	{"1263","Engine Torque Limit 6 Axle Input",			type2bytes	},
	{   "0","END!!",						0		},
	},

	215,// PGn 65169 -FL		01/2008 - 8
	{

	{"1239","Engine Fuel Leakage 1",				type2bits	},
	{"1240","Engine Fuel Leakage 2",				type2bits	},
//	{"9999","DEAD",							type4bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},	
	},
	
	216,// PGn 65170 -EI		01/2008	- 8
	{

	{"1208","Engine Pre-filter Oil Pressure",			type1byte	},
	{"1209","Engine Exhaust Gas Pressure",				type2bytes	},
	{"1210","Engine Fuel Rack Position",				type1byte	},
	{"1241","Engine Gas Mass Flow Rate 1",				type2bytes	},
	{"1242","Instantaneous Estimated Brake Power",			type2bytes	},
	{   "0","END!!",						0		},
	},
	
	217,// PGn 65171 -EES			01/2008	- 8
	{

	{"1204","Electrical Load",					type2bytes	},
	{"1205","Safety Wire Status",					type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},


	218,// PGn 65172 -EAC			01/2008 - 8
	{

	{"1203","Engine Auxiliary Coolant Pressure",			type1byte	},
	{"1212","Engine Auxiliary Coolant Temperature",			type1byte	},
	{"2435","Sea Water Pump Outlet Pressure",			type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},

	219,// PGn 65173 -RBI			01/2008 - 8
	{
		
	{"1193", "Engine Operation Time Since Rebuild",			type4bytes	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},
	},

	220,// PGn 65174 -TCW			01/2008	- 8
	{

	{"1188","Engine Turbocharger 1 Wastegate Drive",		type1byte	},
	{"1189","Engine Turbocharger 2 Wastegate Drive",		type1byte	},
	{"1190","Engine Turbocharger 3 Wastegate Drive",		type1byte	},
	{"1191","Engine Turbocharger 4 Wastegate Drive",		type1byte	},
	{"1192","Engine Turbocharger Wastegate Actuator Ctrl Air Pres",	type1byte	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	221, // PGn 65175 -TCI5			01/2008	- 8
	{

	{"1184","Engine Turbocharger 1 Turbine Outlet Temperature",	type2bytes	},
	{"1185","Engine Turbocharger 2 Turbine Outlet Temperature",	type2bytes	},
	{"1186","Engine Turbocharger 3 Turbine Outlet Temperature",	type2bytes	},
	{"1187","Engine Turbocharger 4 Turbine Outlet Temperature",	type2bytes	},
	{   "0","END!!",						0		},
	},

	222, // PGn 65176 -TCI4			01/2008 - 8
	{

	{"1180","Engine Turbocharger 1 Turbine Inlet Temperature",	type2bytes	},
	{"1181","Engine Turbocharger 2 Turbine Inlet Temperature",	type2bytes	},
	{"1182","Engine Turbocharger 3 Turbine Inlet Temperature",	type2bytes	},
	{"1183","Engine Turbocharger 4 Turbine Inlet Temperature",	type2bytes	},
	{   "0","END!!",						0		},
	},

	223, // PGn 65177 -TCI3			01/2008 - 8
	{

	{"1176","Engine Turbocharger 1 Compressor Inlet Pressure",	type2bytes	},
	{"1177","Engine Turbocharger 2 Compressor Inlet Pressure",	type2bytes	},
	{"1178","Engine Turbocharger 3 Compressor Inlet Pressure",	type2bytes	},
	{"1179","Engine Turbocharger 4 Compressor Inlet Pressure",	type2bytes	},
	{   "0","END!!",						0		},
	},

	224,// PGn 65178 -TCI2			01/2008	- 8
	{

	{"1172","Engine Turbocharger 1 Compressor Inlet Temperature",	type2bytes	},
	{"1173","Engine Turbocharger 2 Compressor Inlet Temperature",	type2bytes	},
	{"1174","Engine Turbocharger 3 Compressor Inlet Temperature",	type2bytes	},
	{"1175","Engine Turbocharger 4 Compressor Inlet Temperature",	type2bytes	},
	{   "0","END!!",						0		},
	},

	225,// PGn 65179 -TCI1		01/2008- 8
	{

	{"1168","Engine Turbocharger Lube Oil Pressure 2",		type1byte	},
	{"1169","Engine Turbocharger 2 Speed",				type2bytes	},
	{"1170","Engine Turbocharger 3 Speed",				type2bytes	},
	{"1171","Engine Turbocharger 4 Speed",				type2bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},

	226,// PGn 65180 -MBT3		01/2008	- 8
	{

	{"1165","Engine Main Bearing 9 Temperature",			type2bytes	},
	{"1166","Engine Main Bearing 10 Temperature",			type2bytes	},
	{"1167","Engine Main Bearing 11 Temperature",			type2bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	227,// PGn 65181 -MBT2		01/2008	- 8
	{

	{"1161", "Engine Main Bearing 5 Temperature",			type2bytes	},
	{"1162", "Engine Main Bearing 6 Temperature",			type2bytes	},
	{"1163", "Engine Main Bearing 7 Temperature",			type2bytes	},
	{"1164","Engine Main Bearing 8 Temperature",			type2bytes	},
	{   "0","END!!",						0		},
	},

	228,// PGn 65182 -MBT1		01/2008	- 8
	{

	{"1157","Engine Main Bearing 1 Temperature",			type2bytes	},
	{"1158","Engine Main Bearing 2 Temperature",			type2bytes	},
	{"1159","Engine Main Bearing 3 Temperature",			type2bytes	},
	{"1160","Engine Main Bearing 4 Temperature",			type2bytes	},
	{   "0","END!!",						0		},
	},

	229,// PGn 65183 -EPT5		01/2008 - 8
	{

	{"1153","Engine Exhaust Gas Port 17 Temperature",		type2bytes	},
	{"1154","Engine Exhaust Gas Port 18 Temperature",		type2bytes	},
	{"1155","Engine Exhaust Gas Port 19 Temperature",		type2bytes	},
	{"1156","Engine Exhaust Gas Port 20 Temperature",		type2bytes	},
	{   "0","END!!",						0		},
	},

	230,// PGn 65184 -EPT4		01/2008 - 8
	{

	{"1149","Engine Exhaust Gas Port 13 Temperature",		type2bytes	},
	{"1150","Engine Exhaust Gas Port 14 Temperature",		type2bytes	},
	{"1151","Engine Exhaust Gas Port 15 Temperature",		type2bytes	},
	{"1152","Engine Exhaust Gas Port 16 Temperature",		type2bytes	},
	{   "0","END!!",						0		},
	},

	231, // PGn 65185 -EPT3		01/2008	- 8
	{

	{"1145","Engine Exhaust Gas Port 9 Temperature",		type2bytes	},
	{"1146","Engine Exhaust Gas Port 10 Temperature",		type2bytes	},
	{"1147","Engine Exhaust Gas Port 11 Temperature",		type2bytes	},
	{"1148","Engine Exhaust Gas Port 12 Temperature",		type2bytes	},
	{   "0","END!!",						0		},
	},

	232, // PGn 65186 -EPT2		01/2008	- 8
	{

	{"1141","Engine Exhaust Gas Port 5 Temperature",		type2bytes	},
	{"1142","Engine Exhaust Gas Port 6 Temperature",		type2bytes	},
	{"1143","Engine Exhaust Gas Port 7 Temperature",		type2bytes	},
	{"1144","Engine Exhaust Gas Port 8 Temperature",		type2bytes	},
	{   "0","END!!",						0		},
	},

	233, // PGn 65187 -EPT1		01/2008	- 8
	{

	{"1137","Engine Exhaust Gas Port 1 Temperature",		type2bytes	},
	{"1138","Engine Exhaust Gas Port 2 Temperature",		type2bytes	},
	{"1139","Engine Exhaust Gas Port 3 Temperature",		type2bytes	},
	{"1140","Engine Exhaust Gas Port 4 Temperature",		type2bytes	},
	{   "0","END!!",						0		},
	},

	234,// PGn 65188 -ET2		01/2008	- 8
	{

	{"1135","Engine Oil Temperature 2",				type2bytes	},
	{"1136","Engine ECU Temperature",				type2bytes	},
	{ "411","Engine Exhaust Gas Recirculation Diff Pressure",	type2bytes	},
	{ "412","Engine Exhaust Gas Recirculation Temperature",		type2bytes	},
	{   "0","END!!",						0		},
	},

	235,// PGn 65189 -IMT2		01/2008 - 8
	{

	{"1131","Engine Intake Manifold 2 Temperature",			type1byte	},
	{"1132","Engine Intake Manifold 3 Temperature",			type1byte	},
	{"1133","Engine Intake Manifold 4 Temperature",			type1byte	},
	{"1802","Engine Intake Manifold 5 Temperature",			type1byte	},
	{"1803","Engine Intake Manifold 6 Temperature",			type1byte	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	236,// PGn 65190 -IMT1		01/2008	- 8
	{

	{"1127","Engine Turbocharger 1 Boost Pressure",			type2bytes	},
	{"1128","Engine Turbocharger 2 Boost Pressure",			type2bytes	},
	{"1129","Engine Turbocharger 3 Boost Pressure",			type2bytes	},
	{"1130","Engine Turbocharger 4 Boost Pressure",			type2bytes	},
	{   "0","END!!",						0		},
	},

	237,// PGn 65191 -AT		01/2008 - 8
	{

	{"1122","Engine Alternator Bearing 1 Temperature",		type1byte	},
	{"1123","Engine Alternator Bearing 2 Temperature",		type1byte	},
	{"1124","Engine Alternator Winding 1 Temperature",		type1byte	},
	{"1125","Engine Alternator Winding 2 Temperature",		type1byte	},
	{"1126","Engine Alternator Winding 3 Temperature",		type1byte	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	238,// PGn 65192 -AC		01/2008	- 8
	{
		
	{"1120","Articulation Angle",					type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
	
	239,// PGn 65193 -EO1		01/2008	- 8
	{

	{"1117","Engine Desired Rated Exhaust Oxygen",			type2bytes	},
	{"1118","Engine Desired Exhaust Oxygen",			type2bytes	},
	{"1119","Engine Actual Exhaust Oxygen",				type2bytes	},
	{"1695","Engine Exhaust Gas Oxygen Sensor Fueling Correction",	type1byte	},
	{"9999","DEAD",							type6bits	},
	{"1696","Engine Exhaust Gas Oxygen Sensor Closed Loop Op",	type2bits	},
	{   "0","END!!",						0		},
	},
	
	240,// PGn 65194 -AF2		01/2008 - 8
	{

	{"1116","Engine Gaseous Fuel Correction Factor",		type1byte	},
	{"1692","Eng Desired Absolute Intake Manifold Pressure (TBL)",	type2bytes	},
	{"1693","Engine Turbocharger Wastegate Valve Position",		type1byte	},
	{"1694","Engine Gas Mass Flow Sensor Fueling Correction",	type1byte	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},
	
	241, // PGn 65195 -ETC6		01/2008	- 8
	{

	{"1113","Recommended Gear",					type1byte	},
	{"1115","Highest Possible Gear",				type1byte	},
	{"1114","Lowest Possible Gear",					type1byte	},
	{"2983","Clutch Life Remaining",				type1byte	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},
	},
	
	242, // PGn 65196 -EBC4		01/2008	- 8
	{

	{"1099","Brake Lining Remaining Front Axle Left Wheel",		type1byte	},
	{"1100","Brake Lining Remaining Front Axle Right Wheel",	type1byte	},
	{"1101","Brake Lining Remaining Rear Axle #1 Left Wheel",	type1byte	},
	{"1102","Brake Lining Remaining Rear Axle #1 Right Wheel",	type1byte	},
	{"1103","Brake Lining Remaining Rear Axle #2 Left Wheel",	type1byte	},
	{"1104","Brake Lining Remaining Rear Axle #2 Right Wheel",	type1byte	},
	{"1105","Brake Lining Remaining Rear Axle #3 Left Wheel",	type1byte	},
	{"1106","Brake Lining Remaining Rear Axle #3 Right Wheel",	type1byte	},
	{   "0","END!!",						0		},
	},

	243, // PGn 65197 -EBC3		01/2008	- 8
	{

	{"1091","Brake App Pres High Range Front Axle Left Wheel",	type1byte	},
	{"1092","Brake App Pres High Range Front Axle Right Wheel",	type1byte	},
	{"1093","Brake App Pres High Range Rear Axle #1 Left Wheel",	type1byte	},
	{"1094","Brake App Pres High Range Rear Axle #1 Right Wheel",	type1byte	},
	{"1095","Brake App Pres High Range Rear Axle #2 Left Wheel",	type1byte	},
	{"1096","Brake App Pres High Range Rear Axle #2 Right Wheel",	type1byte	},
	{"1097","Brake App Pres High Range Rear Axle #3 Left Wheel",	type1byte	},
	{"1098","Brake App Pres High Range Rear Axle #3 Right Wheel",	type1byte	},
	{   "0","END!!",						0		},
	},
	
	244,// PGn 65198 -AIR1		01/2008	- 8
	{

	{  "46","Pneumatic Supply Pressure",				type1byte	},
	{"1086","Parking and/or Trailer Air Pressure",			type1byte	},
	{"1087","Service Brake Circuit 1 Air Pressure",			type1byte	},
	{"1088","Service Brake Circuit 2 Air Pressure",			type1byte	},
	{"1089","Auxiliary Equipment Supply Pressure",			type1byte	},
	{"1090","Air Suspension Supply Pressure",			type1byte	},
	{"1351","Air Compressor Status",				type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},

	245,// PGn 65199 -GFC		01/2008	- 8
	{

	{"1039","Trip Fuel (Gaseous)",					type4bytes	},
	{"1040","Total Fuel Used (Gaseous)",				type4bytes	},
	{   "0","END!!",						0		},
	},

	246,// PGn 65200 -TTI2		01/2008 - 20
	{

	{"1034","Trip Cruise Time",					type4bytes	},
	{"1035","Trip PTO Time",					type4bytes	},
	{"1036","Trip Engine Running Time",				type4bytes	},
	{"1037","Trip Idle Time",					type4bytes	},
	{"1038","Trip Air Compressor On Time",				type4bytes	},
	{   "0","END!!",						0		},
	},

	247,// PGn 65201 -EH		01/2008	- 8
	{

	{"1032","Total ECU Distance",					type4bytes	},
	{"1033","Total ECU Run Time",					type4bytes	},
	{   "0","END!!",						0		},
	},

	248,// PGn 65202 -GFI1		01/2008 - 8		
	{

	{"1030","Total Engine PTO Fuel Used (Gaseous)",			type4bytes	},
	{"1031","Trip Average Fuel Rate (Gaseous)",			type2bytes	},
	{"1389","Engine Fuel Specific Gravity",				type2bytes	},
	{   "0","END!!",						0		},
	},

	249,// PGn 65203 -LFI		01/2008	- 8
	{

	{"1028","Total Engine PTO Fuel Used",				type4bytes	},
	{"1029","TTrip Average Fuel Rate",				type2bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	250,// PGn 65204 -TTI1		01/2008 - 16
	{

	{"1024","Trip Time in VSL",					type4bytes	},
	{"1025","Trip Time in Top Gear",				type4bytes	},
	{"1026","Trip Time in Gear Down",				type4bytes	},
	{"1027","Trip Time in Derate by Engine",			type4bytes	},
	{   "0","END!!",						0		},
	},

	251, // PGn 65205 -TSI		01/2008	- 8
	{

	{"1020","Trip Number of Hot Shutdowns",				type2bytes	},
	{"1021","Trip Number of Idle Shutdowns",			type2bytes	},
	{"1022","Trip Number of Idle Shutdown Overrides",		type2bytes	},
	{"1023","Trip Sudden Decelerations",				type2bytes	},
	{   "0","END!!",						0		},
	},

	252, // PGn 65206 -TVI		01/2008	- 8
	{

	{"1018","Trip Maximum Vehicle Speed",				type2bytes	},
	{"1019","Trip Cruise Distance",					type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},

	253, // PGn 65207 -LF		01/2008	- 10
	{

	{"1013","Trip Maximum Engine Speed",				type2bytes	},
	{"1014","Trip Average Engine Speed",				type2bytes	},
	{"1015","Trip Drive Average Load Factor",			type1byte	},
	{"1016","Total Drive Average Load Factor",			type1byte	},
	{"1017","Total Engine Cruise Time",				type4bytes	},
	{   "0","END!!",						0		},
	},

	254,// PGn 65208 -GTFI		01/2008	- 22
	{

	{"1007","Trip Drive Fuel Used (Gaseous)",			type4bytes	},
	{"1008","Trip PTO Moving Fuel Used (Gaseous)",			type4bytes	},
	{"1009","Trip PTO Non-moving Fuel Used (Gaseous)",		type4bytes	},
	{"1010","Trip Vehicle Idle Fuel Used (Gaseous)",		type4bytes	},
	{"1011","Trip Cruise Fuel Used (Gaseous)",			type4bytes	},
	{"1012","Trip Drive Fuel Economy (Gaseous)",			type2bytes	},
       	{   "0","END!!",						0		},
	},

	255,// PGn 65209 -LTFI		01/2008	- 22
	{

	{"1001","Trip Drive Fuel Used",					type4bytes	},
	{"1002","Trip PTO Moving Fuel Used",				type4bytes	},
	{"1003","Trip PTO Non-moving Fuel Used",			type4bytes	},
	{"1004","Trip Vehicle Idle Fuel Used",				type4bytes	},
	{"1005","Trip Cruise Fuel Used",				type4bytes	},
	{"1006","Trip Drive Fuel Economy",				type2bytes	},
       	{   "0","END!!",						0		},
	},

	256,// PGn 65210 -TDI		01/2008	- 12
	{

	{ "998","Trip Distance on VSL",					type4bytes	},
	{ "999","Trip Gear Down Distance",				type4bytes	},
	{"1000","Trip Distance in Top Gear",				type4bytes	},
	{   "0","END!!",						0		},
	},

	257,// PGn 65211 -TFI		01/2008 - 16
	{

	{ "994","Trip Fan On Time",					type4bytes	},
	{ "995","Trip Fan On Time Due to the Engine System",		type4bytes	},
	{ "996","Trip Fan On Time Due to a Manual Switch",		type4bytes	},
	{ "997","Trip Fan On Time Due to the A/C System",		type4bytes	},
	{   "0","END!!",						0		},
	},

	258,// PGn 65212 -CBI		01/2008	- 16
	{

	{ "990","Total Compression Brake Distance",			type4bytes	},
	{ "991","Trip Compression Brake Distance",			type4bytes	},
	{ "992","Trip Service Brake Distance",				type4bytes	},
	{ "993","Trip Service Brake Applications",			type4bytes	},
	{   "0","END!!",						0		},
	},
	
	259,// PGn 65213 -FD		01/2008 - 8
	{

	{ "975","Estimated Percent Fan Speed",				type1byte	},
	{ "977","Fan Drive State",					type4bits	},
	{"9999","DEAD",							type4bits	},
	{"1639","Fan Speed",						type2bytes	},
	{"4211","Hydraulic Fan Motor Pressure",				type2bytes	},
	{"4212","Fan Drive Bypass Command Status",			type1byte	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},

	260,// PGn 65214 -EEC4		01/2008 - 8
	{

	{ "166","Engine Rated Power",					type2bytes	},
	{ "189","Engine Rated Speed",					type2bytes	},
	{"3669","Engine Rotation Direction",				type2bits	},
	{"9999","DEAD",							type6bits	},
	{"3671","Crank Attempt Count on Present Start Attempt",		type1byte	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},	
	},
	
	261, // PGn 65215 -EBC2		01/2008	- 8
	{

	{ "904","Front Axle Speed",					type2bytes	},
	{ "905","Relative Speed Front Axle Left Wheel",			type1byte	},
	{ "906","Relative Speed Front Axle Right Wheel",		type1byte	},
	{ "907","Relative Speed Rear Axle #1 Left Wheel",		type1byte	},
	{ "908","Relative Speed Rear Axle #1 Right Wheel",		type1byte	},
	{ "909","Relative Speed Rear Axle #2 Left Wheel",		type1byte	},
       	{ "910","Relative Speed Rear Axle #2 Right Wheel",		type1byte	},
       	{   "0","END!!",						0		},
	},

	262, // PGn 65216 -SERV		01/2008 - 8 or VAR
	{

	{ "911","Service Component Identification",			type1byte	},
	{ "914","Service Distance",					type2bytes	},
	{ "912","Service Component Identification",			type1byte	},
	{ "915","Service Delay/Calendar Time Based",			type1byte	},
	{ "913","Service Component Identification",			type1byte	},
	{ "916","Service Delay/Operational Time Based",			type2bytes	},
       	{   "0","END!!",						0		},       
	},

	263, // PGn 65217 -VDHR		01/2008	- 8
	{

	{ "917","High Resolution Total Vehicle Distance",		type4bytes	},
	{ "918","High Resolution Trip Distance",			type4bytes	},
	{   "0","END!!",						0		},   
	},
	
	264,// PGn 65218 -ERC2		01/2008	- 8
	{

	{ "748","Transmission Output Retarder",				type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		}, 
	},

	265,// PGn 65219 -ETC5		01/2008	- 8
	{

	{ "778","Transmission High Range Sense Switch",			type2bits	},
	{ "779","Transmission Low Range Sense Switch",			type2bits	},
	{"9999","DEAD",							type4bits	},
	{ "767","Transmission Reverse Direction Switch",		type2bits	},
	{ "604","Transmission Neutral Switch",				type2bits	},
	{ "903","Transmission Forward Direction Switch",		type2bits	},
//	{"9999","DEAD",							type2bits	},
  //	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		}, 
	},

	266,// PGn 65221 -ETC4		01/2008	- 8
	{

	{  "53","Transmission Synchronizer Clutch Value",		type1byte	},
	{  "54","Transmission Synchronizer Brake Value",		type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},   
	},

	267,// PGn 65223 -ETC3		01/2008	- 8
	{

	{  "59","Transmission Shift Finger Gear Position",		type1byte	},
	{  "60","Transmission Shift Finger Rail Position",		type1byte	},
	{ "780","Transmission Shift Finger Neutral Indicator",		type2bits	},
	{ "781","Transmission Shift Finger Engagement Indicator",	type2bits	},
	{ "782","Transmission Shift Finger Center Rail Indicator",	type2bits	},
	{"9999","DEAD",							type2bits	},
	{ "772","Transmission Shift Finger Rail Actuator 1",		type2bits	},
       	{ "773","Transmission Shift Finger Gear Actuator 1",		type2bits	},
	{ "783","Transmission Shift Finger Rail Actuator 2",		type2bits	},
	{ "784","Transmission Shift Finger Gear Actuator 2",		type2bits	},
	{ "768","Transmission Range High Actuator",			type2bits	},
	{ "769","Transmission Range Low Actuator",			type2bits	},
       	{ "770","Transmission Splitter Direct Actuator",		type2bits	},
	{ "771","Transmission Splitter Indirect Actuator",		type2bits	},
	{ "788","Transmission Clutch Actuator",				type2bits	},
	{ "740","Transmission Lockup Clutch Actuator",			type2bits	},
	{ "786","Transmission Defuel Actuator",				type2bits	},
       	{ "787","Transmission Inertia Brake Actuator",			type2bits	},
//	{"9999","DEAD",							type2bytes	},
        {   "0","END!!",						0		},      
	},

	268,// PGn 65237 -AS		01/2008	- 8
	{

	{ "589","Alternator Speed",					type2bytes	},
	{"3353","Alternator 1 Status",					type2bits	},
	{"3354","Alternator 2 Status",					type2bits	},
	{"3355","Alternator 3 Status",					type2bits	},
	{"3356","Alternator 4 Status",					type2bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	269,// PGn 65241 -AUXIO1	01/2008	- 8
	{

	{ "704","Auxiliary I/O #04",					type2bits	},
	{ "703","Auxiliary I/O #03",					type2bits	},
	{ "702","Auxiliary I/O #02",					type2bits	},
	{ "701","Auxiliary I/O #01",					type2bits	},
	{ "708","Auxiliary I/O #08",					type2bits	},
	{ "707","Auxiliary I/O #07",					type2bits	},
	{ "706","Auxiliary I/O #06",					type2bits	},
       	{ "705","Auxiliary I/O #05",					type2bits	},
	{ "712","Auxiliary I/O #12",					type2bits	},
	{ "711","Auxiliary I/O #11",					type2bits	},
	{ "710","Auxiliary I/O #10",					type2bits	},
	{ "709","Auxiliary I/O #09",					type2bits	},
       	{ "716","Auxiliary I/O #16",					type2bits	},
	{ "715","Auxiliary I/O #15",					type2bits	},
	{ "714","Auxiliary I/O #14",					type2bits	},
	{ "713","Auxiliary I/O #13",					type2bits	},
	{"1083","Auxiliary I/O Channel #1",				type2bytes	},
       	{"1084","Auxiliary I/O Channel #2",				type2bytes	},
        {   "0","END!!",						0		},      
	},
	
	270,// PGn 65242 -SOFT		01/2008	- VAR
	{

	{ "965","Number of Software Identification Fields",		type1byte	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{ "234","Software Identification",				type4bytes	},
	{   "0","END!!",						0		},
	},

	271, // PGn 65243 -EFL/P2	01/2008 - 8
	{

	{ "164","Engine Injection Control Pressure",			type2bytes	},
	{ "157","Engine Injector Metering Rail 1 Pressure",		type2bytes	},
	{ "156","Engine Injector Timing Rail 1 Pressure",		type2bytes	},
	{"1349","Engine Injector Metering Rail 2 Pressure",		type2bytes	},
	{   "0","END!!",						0		},
	},

	272, // PGn 65244 -IO		01/2008	- VAR
	{

	{ "236","Engine Total Idle Fuel Used",				type4bytes	},
	{ "235","Engine Total Idle Hours",				type4bytes	},
	{   "0","END!!",						0		},
	},
	
	273, // PGn 65245 -TC		01/2008 - 8
	{

	{ "104","Engine Turbocharger Lube Oil Pressure 1",		type1byte	},
	{ "103","Engine Turbocharger 1 Speed",				type2bytes	},
	{"9999","DEAD",							type6bits	},
	{"1665","Engine Turbocharger Oil Level Switch",			type2bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},
	},

	274,// PGn 65246 -AIR2		01/2008 - 8
	{
		
	{  "82","Engine Air Start Pressure",				type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

	275,// PGn 65247 -EEC3		01/2008	- 8
	{

	{ "514","Nominal Friction - Percent Torque",			type1byte	},
	{ "515","Engine's Desired Operating Speed",			type2bytes	},
	{ "519","Engine's Desired Operating Speed Asymmetry Adjustment",type1byte	},
	{"2978","Estimated Engine Parasitic Losses - Percent Torque",	type1byte	},
	{"3236","Aftertreatment 1 Exhaust Gas Mass Flow",		type2bytes	},
	{"3237","Aftertreatment 1 Intake Dew Point",			type2bits	},
	{"3238","Aftertreatment 1 Exhaust Dew Point",			type2bits	},
       	{"3239","Aftertreatment 2 Intake Dew Point",			type2bits	},
	{"3240","Aftertreatment 2 Exhaust Dew Point",			type2bits	},
      	{   "0","END!!",						0		},
	},

	276,// PGn 65248 -VD		01/2008	 - 8
	{

	{ "244","Trip Distance",					type4bytes	},
	{ "245","Total Vehicle Distance",				type4bytes	},
	{   "0","END!!",						0		},
	},
	
	277,// PGn 65249 -RC		01/2008 - 19
	{

	{ "901","Retarder Type",					type4bits	},
	{ "902","Retarder Location",					type4bits	},
	{ "557","Retarder Control Method (Retarder Configuration)",	type1byte	},
	{ "546","Retarder Speed At Idle Point 1 (Retarder Config)",	type2bytes	},
	{ "551","Percent Torque At Idle Point 1 (Retarder Config)",	type1byte	},
	{ "548","Maximum Retarder Speed Point 2 (Retarder Config)",	type2bytes	},
	{ "552","Percent Torque At Max Speed Point 2 (Retarder Config)",type1byte	},
       	{ "549","Retarder Speed At Point 3 (Retarder Configuration)",	type2bytes	},
	{ "553","Percent Torque At Point 3 (Retarder Configuration)",	type1byte	},
      	{ "550","Retarder Speed At Point 4 (Retarder Configuration)",	type2bytes	},
	{ "554","Percent Torque At Point 4 (Retarder Configuration)",	type1byte	},
       	{ "547","Retarder Speed At Peak Torq Point 5 (Retarder Config)",type2bytes	},
	{ "556","Reference Retarder Torque (Retarder Configuration)",	type2bytes	},
	{ "555","Percent Torque At Peak Torque Pt 5 (Retarder Config)",	type1byte	},
	{   "0","END!!",						0		},
	},

	278,// PGn 65250 -TCFG		01/2008	- VAR
	{

	{ "958","Number of Reverse Gear Ratios",			type1byte	},
	{ "957","Number of Forward Gear Ratios",			type1byte	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{ "581","Transmission Gear Ratio",				type2bytes	},
	{   "0","END!!",						0		},
	},
      
	279,// PGn 65251 -EC1		01/2008	- 39
	{

	{ "188","Engine Speed At Idle, Point 1 (Engine Configuration)",	type2bytes	},
	{ "539","Engine Percent Torque At Idle, Point 1 (Eng Config)",	type1byte	},
	{ "528","Engine Speed At Point 2 (Engine Configuration)",	type2bytes	},
	{ "540","Engine Percent Torque At Point 2 (Engine Config)",	type1byte	},
	{ "529","Engine Speed At Point 3 (Engine Configuration)",	type2bytes	},
	{ "541","Engine Percent Torque At Point 3 (Engine Config)",	type1byte	},
	{ "530","Engine Speed At Point 4 (Engine Configuration)",	type2bytes	},
       	{ "542","Engine Percent Torque At Point 4 (Engine Config)",	type1byte	},
	{ "531","Engine Speed At Point 5 (Engine Configuration)",	type2bytes	},
      	{ "543","Engine Percent Torque At Point 5 (Engine Config)",	type1byte	},
	{ "532","Engine Speed At High Idle, Point 6 (Engine Config)",	type2bytes	},
       	{ "545","Engine Gain (Kp) Of Endspeed Governor (Eng Config)",	type2bytes	},
	{ "544","Engine Reference Torque (Engine Configuration)",	type2bytes	},
	{ "533","Engine Max Momentary Override Speed Pt 7 (Eng Config)",type2bytes	},
	{ "534","Engine Max Momentary Override Time Limit (Eng Config)",type1byte	},
       	{ "535","Engine Req Speed Ctrl Range Lower Limit (Eng Config)",	type1byte	},
	{ "536","Engine Req Speed Ctrl Range Upper Limit (Eng Config)",	type1byte	},
      	{ "537","Engine Req Torque Ctrl Range Lower Limit (Eng Config)",type1byte	},
	{ "538","Engine Req Torque Ctrl Range Upper Limit (Eng Config)",type1byte	},
       	{"1712","Eng Extd Req Speed Ctrl Range Upper Limit (Eng Config)",type2bytes	},
	{"1794","Engine Moment of Inertia",				type2bytes	},
	{"1846","Engine Default Torque Limit",				type2bytes	},
	{"3344","Support Variable Rate TSC1 Message",			type8bits	},
	{"3345","Support TSC1 Control Purpose Group 1",			type8bits	},
       	{"3346","Support TSC1 Control Purpose Group 2",			type8bits	},
	{"3347","Support TSC1 Control Purpose Group 3",			type8bits	},
	{"3348","Support TSC1 Control Purpose Group 4",			type8bits	},
	{   "0","END!!",						0		},
	},
      
	280,// PGn 65252 -SHUTDN	01/2008 - 8
	{

	{ "593","Engine Idle Shutdown has Shutdown Engine",		type2bits	},
	{ "594","Engine Idle Shutdown Driver Alert Mode",		type2bits	},
	{ "592","Engine Idle Shutdown Timer Override",			type2bits	},
	{ "590","Engine Idle Shutdown Timer State",			type2bits	},
	{"9999","DEAD",							type6bits	},
	{ "591","Engine Idle Shutdown Timer Function",			type2bits	},
	{ "985","A/C High Pressure Fan Switch",				type2bits	},
	{ "875","Refrigerant Low Pressure Switch",			type2bits	},
       	{ "605","Refrigerant High Pressure Switch",			type2bits	},
	{"9999","DEAD",							type2bits	},
	{"1081","Engine Wait to Start Lamp",				type2bits	},
	{"9999","DEAD",							type6bits	},
	{"1110","Engine Protection System has Shutdown Engine",		type2bits	},
	{"1109","Engine Protection System Approaching Shutdown",	type2bits	},
       	{"1108","Engine Protection System Timer Override",		type2bits	},
	{"1107","Engine Protection System Timer State",			type2bits	},
	{"9999","DEAD",							type6bits	},
	{"1111","Engine Protection System Configuration",		type2bits	},
	{"2815","Engine Alarm Acknowledge",				type2bits	},
       	{"2814","Engine Alarm Output Command Status",			type2bits	},
	{"2813","Engine Air Shutoff Command Status",			type2bits	},
      	{"2812","Engine Overspeed Test",				type2bits	},
	{"3667","Engine Air Shutoff Status",				type2bits	},
//	{"9999","DEAD",							type6bits	},
       	{   "0","END!!",						0		},
	},

	281, // PGn 65253 -HOURS	01/2008	- 8
	{

	{ "247","Engine Total Hours of Operation",			type4bytes	},
	{ "249","Engine Total Revolutions",				type4bytes	},
	{   "0","END!!",						0		},
	},
	

	282, // PGn 65254 -TD		01/2008 - 8
	{

	{ "959","Seconds",						type1byte	},
	{ "960","Minutes",						type1byte	},
	{ "961","Hours",						type1byte	},
	{ "962","Month",						type1byte	},
	{ "963","Day",							type1byte	},
	{ "964","Year",							type1byte	},
	{"1601","Local minute offset",					type1byte	},
       	{"1602","Local hour offset",					type1byte	},
	{   "0","END!!",						0		},
	},

	283, // PGn 65255 -VH		01/2008	- 8
	{

	{ "246","Total Vehicle Hours",					type4bytes	},
	{ "248","Total Power Takeoff Hours",				type4bytes	},
	{   "0","END!!",						0		},
	},
	
	284,// PGn 65256 -VDS		01/2008	- 8
	{

	{ "165","Compass Bearing",					type2bytes	},
	{ "517","Navigation-Based Vehicle Speed",			type2bytes	},
	{ "583","Pitch",						type2bytes	},
	{ "580","Altitude",						type2bytes	},
	{   "0","END!!",						0		},
	},

	285,// PGn 65257 -LFC		01/2008	- 8
	{

	{ "182","Engine Trip Fuel",					type4bytes	},
	{ "250","Engine Total Fuel Used",				type4bytes	},
	{   "0","END!!",						0		},
	},
	
	286,// PGn 65258 -VW		01/2008 - 8
	{

	{ "928","Axle Location",					type8bits	},
	{ "582","Axle Weight",						type2bytes	},
	{ "180","Trailer Weight",					type2bytes	},
	{ "181","Cargo Weight",						type2bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	287,// PGn 65259 -CI		01/2008	 - VAR
	{

	{ "586","Make",							type1byte	},
	{ "586","Make",							type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "587","Model",						type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
/*	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "588","Serial Number",					type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
	{ "233", "Unit Number (Power Unit)",				type4bytes	},
*/	{   "0","END!!",						0		},	
	},


	288,// PGn 65260 -VI		01/2008	 - VAR
	{

	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{ "237","Vehicle Identification Number",			type4bytes	},
	{   "0","END!!",						0		},
	},

	
	289,// PGn 65261 -CCSS		01/2008 - 8
	{

	{  "74","Maximum Vehicle Speed Limit",				type1byte	},
	{  "87","Cruise Control High Set Limit Speed",			type1byte	},
	{  "88","Cruise Control Low Set Limit Speed",			type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},

	290,// PGn 65262 -ET1		01/2008 - 8
	{

	{ "110","Engine Coolant Temperature",				type1byte	},
	{ "174","Engine Fuel Temperature 1",				type1byte	},
	{ "175","Engine Oil Temperature 1",				type2bytes	},
	{ "176","Engine Turbocharger Oil Temperature",			type2bytes	},
	{  "52","Engine Intercooler Temperature",			type1byte	},
	{"1134","Engine Intercooler Thermostat Opening",		type1byte	},
	{   "0","END!!",						0		},
	},

	291, // PGn 65263 -EFL/P1	01/2008 - 8
	{

	{  "94","Engine Fuel Delivery Pressure",			type1byte	},
	{  "22","Engine Extended Crankcase Blow-by Pressure",		type1byte	},
	{  "98","Engine Oil Level",					type1byte	},
	{ "100","Engine Oil Pressure",					type1byte	},
	{ "101","Engine Crankcase Pressure",				type2bytes	},
	{ "109","Engine Coolant Pressure",				type1byte	},
	{ "111","Engine Coolant Level",					type1byte	},
	{   "0","END!!",						0		},
	},
	
	292, // PGn 65264 -PTO		01/2008 - 8
	{

	{  "90","Power Takeoff Oil Temperature",			type1byte	},
	{ "186","Power Takeoff Speed",					type2bytes	},
	{ "187","Power Takeoff Set Speed",				type2bytes	},
	{ "980","Engine PTO Enable Switch",				type2bits	},
	{ "979","Engine Remote PTO Preprogrammed Speed Control Switch",	type2bits	},
	{ "978","Engine Remote PTO Variable Speed Control Switch",	type2bits	},
	{"9999","DEAD",							type2bits	},
	{ "984","Engine PTO Set Switch",				type2bits	},
       	{ "983","Engine PTO Coast/Decelerate Switch",			type2bits	},
	{ "982","Engine PTO Resume Switch",				type2bits	},
	{ "981","Engine PTO Accelerate Switch",				type2bits	},
	{"2897","Operator PTO Memory Select Switch",			type2bits	},
       	{"3447","Remote PTO preprogrammed speed control switch #2",	type2bits	},
	{"3448","Auxiliary Input Ignore Switch",			type2bits	},
//	{"9999","DEAD",							type2bits	},
	{   "0","END!!",						0		},
	},
	
	293, // PGn 65265 -CCVS		01/2008	- 8
	{

	{  "69","Two Speed Axle Switch",				type2bits	},
	{  "70","Parking Brake Switch",					type2bits	},
	{"1633","Cruise Control Pause Switch",				type2bits	},
	{"3807","Park Brake Release Inhibit Request",			type2bits	},
	{  "84","Wheel-Based Vehicle Speed",				type2bytes	},
	{ "595","Cruise Control Active",				type2bits	},
	{ "596","Cruise Control Enable Switch",				type2bits	},
       	{ "597","Brake Switch",						type2bits	},
	{ "598","Clutch Switch",					type2bits	},
	{ "599","Cruise Control Set Switch",				type2bits	},
	{ "600","Cruise Control Coast (Decelerate) Switch",		type2bits	},
       	{ "601","Cruise Control Resume Switch",				type2bits	},
	{ "602","Cruise Control Accelerate Switch",			type2bits	},
	{  "86","Cruise Control Set Speed",				type1byte	},
	{ "976","PTO State",						type5bits	},
	{ "527","Cruise Control States",				type3bits	},
	{ "968","Engine Idle Increment Switch",				type2bits	},
	{ "967","Engine Idle Decrement Switch",				type2bits	},
	{ "966","Engine Test Mode Switch",				type2bits	},
	{"1237","Engine Shutdown Override Switch",			type2bits	},
	{   "0","END!!",						0		},
	},
	
	294,// PGn 65266 -LFE		01/2008	- 8
	{

	{ "183","Engine Fuel Rate",					type2bytes	},
	{ "184","Engine Instantaneous Fuel Economy",			type2bytes	},
	{ "185","Engine Average Fuel Economy",				type2bytes	},
	{  "51","Engine Throttle Position",				type1byte	},
	{"3673","Engine Throttle 2 Position",				type1byte	},
	{   "0","END!!",						0		},	
	},

	295,// PGn 65267 -VP		01/2008	- 8
	{

	{ "584","Latitude",						type4bytes	},
	{ "585","Longitude",						type4bytes	},
	{   "0","END!!",						0		},
	},
	
	296,// PGn 65268 -TIRE		01/2008	- 8
	{

	{ "929","Tire Location",					type8bits	},
	{ "241","Tire Pressure",					type1byte	},
	{ "242","Tire Temperature",					type2bytes	},
	{"1699","CTI Wheel Sensor Status",				type2bits	},
	{"1698","CTI Tire Status",					type2bits	},
	{"1697","CTI Wheel End Electrical Fault",			type2bits	},
	{"9999","DEAD",							type2bits	},
	{"2586","Tire Air Leakage Rate",				type2bytes	},
	{"9999","DEAD",							type5bits	},
	{"2587","Tire Pressure Threshold Detection",			type3bits	},
	{   "0","END!!",						0		},	
	},
	
	297,// PGn 65269 -AMB		01/2008	- 8
	{

	{ "108","Barometric Pressure",					type1byte	},
	{ "170","Cab Interior Temperature",				type2bytes	},
	{ "171","Ambient Air Temperature",				type2bytes	},
	{ "172","Engine Air Inlet Temperature",				type1byte	},
	{  "79","Road Surface Temperature",				type2bytes	},
	{   "0","END!!",						0		},	
	},
	
	298,// PGn 65270 -IC1		01/2008	- 8
	{

	{  "81","Engine Particulate Trap Inlet Pressure",		type1byte	},
	{ "102","Engine Intake Manifold #1 Pressure",			type1byte	},
	{ "105","Engine Intake Manifold 1 Temperature",			type1byte	},
	{ "106","Engine Air Inlet Pressure",				type1byte	},
	{ "107","Engine Air Filter 1 Differential Pressure",		type1byte	},
	{ "173","Engine Exhaust Gas Temperature",			type2bytes	},
	{ "112","Engine Coolant Filter Differential Pressure",		type1byte	},
	{   "0","END!!",						0		},
	},

	299,// PGn 65271 -VEP1		01/2008	- 8
	{

	{ "114","Net Battery Current",					type1byte	},
	{ "115","Alternator Current",					type1byte	},
	{ "167","Charging System Potential (Voltage)",			type2bytes	},
	{ "168","Battery Potential / Power Input 1",			type2bytes	},
	{ "158","Keyswitch Battery Potential",				type2bytes	},
	{   "0","END!!",						0		},
	},
	
	300,// PGn 65272 -TRF1		01/2008	- 8
	{

	{ "123", "Clutch Pressure",					type1byte	},
	{ "124","Transmission Oil Level",				type1byte	},
	{ "126","Transmission Filter Differential Pressure",		type1byte	},
	{ "127","Transmission Oil Pressure",				type1byte	},
	{ "177","Transmission Oil Temperature",				type2bytes	},
	{"3027","Transmission Oil Level High / Low",			type1byte	},
	{"3028","Transmission Oil Level Countdown Timer",		type4bits	},
	{"3026","Transmission Oil Level Measurement Status",		type4bits	},
	{   "0","END!!",						0		},
	},
	
	301, // PGn 65273 -AI		01/2008	 - 8
	{

	{  "75","Steering Axle Temperature",				type1byte	},
	{ "930","Drive Axle Location",					type8bits	},
	{ "579","Drive Axle Lift Air Pressure",				type1byte	},
	{ "578","Drive Axle Temperature",				type1byte	},
	{"2613","Drive Axle Lube Pressure",				type1byte	},
	{"9999","DEAD",							type2bytes	},
	{"2614","Steering Axle Lube Pressure",				type1byte	},
	{   "0","END!!",						0		},
	},
	
	302, // PGn 65274 -B		01/2008	- 8
	{

	{ "116","Brake Application Pressure",				type1byte	},
	{ "117","Brake Primary Pressure",				type1byte	},
	{ "118","Brake Secondary Pressure",				type1byte	},
	{ "619","Parking Brake Actuator",				type2bits	},
	{"3557","Parking Brake Red Warning Signal",			type2bits	},
	{"3808","Park Brake Release Inhibit Status",			type2bits	},
//	{"9999","DEAD",							type2bits	},
//	{"9999","DEAD",							type4bytes	},
	{   "0","END!!",						0		},
	},


	303, // PGn 65275 -RF		01/2008	- 8
	{

	{ "119","Hydraulic Retarder Pressure",				type1byte	},
	{ "120","Hydraulic Retarder Oil Temperature",			type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type2bytes	},
	{   "0","END!!",						0		},
	},
	
	304,// PGn 65276 -DD		01/2008 - 8
	{

	{  "80","Washer Fluid Level",					type1byte	},
	{  "96","Fuel Level",						type1byte	},
	{  "95","Engine Fuel Filter Differential Pressure",		type1byte	},
	{  "99","Engine Oil Filter Differential Pressure",		type1byte	},
	{ "169","Cargo Ambient Temperature",				type2bytes	},
	{  "38","Fuel Level 2",						type1byte	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	305,// PGn 65277 -A1		01/2008 - 8
	{

	{  "72","Engine Blower Bypass Valve Position",			type1byte	},
	{ "159","Engine Gas Supply Pressure",				type2bytes	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type1byte	},
	{   "0","END!!",						0		},
	},
	
	306,// PGn 65278 -AWPP		01/2008 - 8
	{
		
	{  "73","Auxiliary Pump Pressure",				type1byte	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{   "0","END!!",						0		},
	},

       	307, // PGN 65279 -WFI		01/2008	- 8
	{
		
	{   "97", "Water In Fuel Indicator",				type2bits	},
//	{"9999","DEAD",							type6bits	},
//	{"9999","DEAD",							type4bytes	},
//	{"9999","DEAD",							type3bytes	},
	{    "0", "END!!",						0		},
	}, 
	    

	308, // PGN 65226 -DM1		09/2006	- VAR
	{
	{   "MIL", "Malfunction Indicator Lamp Status",			type2bits	},
	{   "RSL", "Red Stop Lamp Status",				type2bits	},
	{   "AWL", "Amber Warning Lamp Status",				type2bits	},
	{    "PL", "Protect Lamp Status",				type2bits	},
	{  "FMIL", "Flash Malfunction Indicator Lamp",			type2bits	},
	{  "FRSL", "Flash Red Stop Lamp",				type2bits	},
	{  "FAWL", "Flash Amber Waring Lamp",				type2bits	},
	{   "FPL", "Flash Protect Lamp",				type2bits	},
	{  "SPN1", "Suspect Parameter Number 1",			19 | BITS	},
	{  "FMI1", "FMI 1",						type5bits	},
	{   "CM1", "SPN Conversion Method 1",				type1bit	},
	{   "OC1", "Occurrence Count 1",				type7bits	},
	{  "SPN2", "Suspect Parameter Number 2",			19 | BITS	},
	{  "FMI2", "FMI 2",						type5bits	},
	{   "CM2", "SPN Conversion Method 2",				type1bit	},
	{   "OC2", "Occurrence Count 2",				type7bits	},
	{  "SPN3", "Suspect Parameter Number 3",			19 | BITS	},
	{  "FMI3", "FMI 3",						type5bits	},
	{   "CM3", "SPN Conversion Method 3",				type1bit	},
	{   "OC3", "Occurrence Count 3",				type7bits	},
	{  "SPN4", "Suspect Parameter Number 4",			19 | BITS	},
	{  "FMI4", "FMI 4",						type5bits	},
	{   "CM4", "SPN Conversion Method 4",				type1bit	},
	{   "OC4", "Occurrence Count 4",				type7bits	},
	{  "SPN5", "Suspect Parameter Number 5",			19 | BITS	},
	{  "FMI5", "FMI 5",						type5bits	},
	{   "CM5", "SPN Conversion Method 5",				type1bit	},
	{   "OC5", "Occurrence Count 5",				type7bits	},
	{  "SPN6", "Suspect Parameter Number 6",			19 | BITS	},
	{  "FMI6", "FMI 6",						type5bits	},
	{   "CM6", "SPN Conversion Method 6",				type1bit	},
	{   "OC6", "Occurrence Count 6",				type7bits	},
	{  "SPN7", "Suspect Parameter Number 7",			19 | BITS	},
	{  "FMI7", "FMI 7",						type5bits	},
	{   "CM7", "SPN Conversion Method 7",				type1bit	},
	{   "OC7", "Occurrence Count 7",				type7bits	},
	{  "SPN8", "Suspect Parameter Number 8",			19 | BITS	},
	{  "FMI8", "FMI 8",						type5bits	},
	{   "CM8", "SPN Conversion Method 8",				type1bit	},
	{   "OC8", "Occurrence Count 8",				type7bits	},
	{  "SPN9", "Suspect Parameter Number 9",			19 | BITS	},
	{  "FMI9", "FMI 9",						type5bits	},
	{   "CM9", "SPN Conversion Method 9",				type1bit	},
	{   "OC9", "Occurrence Count 9",				type7bits	},
	{  "SPN10", "Suspect Parameter Number 10",			19 | BITS	},
	{  "FMI10", "FMI 10",						type5bits	},
	{   "CM10", "SPN Conversion Method 10",				type1bit	},
	{   "OC10", "Occurrence Count 10",				type7bits	},
	{  "SPN11", "Suspect Parameter Number 11",			19 | BITS	},
	{  "FMI11", "FMI 11",						type5bits	},
	{   "CM11", "SPN Conversion Method 11",				type1bit	},
	{   "OC11", "Occurrence Count 11",				type7bits	},
	{  "SPN12", "Suspect Parameter Number 12",			19 | BITS	},
	{  "FMI12", "FMI 12",						type5bits	},
	{   "CM12", "SPN Conversion Method 12",				type1bit	},
	{   "OC12", "Occurrence Count 12",				type7bits	},
	{  "SPN13", "Suspect Parameter Number 13",			19 | BITS	},
	{  "FMI13", "FMI 13",						type5bits	},
	{   "CM13", "SPN Conversion Method 13",				type1bit	},
	{   "OC13", "Occurrence Count 13",				type7bits	},
	{  "SPN14", "Suspect Parameter Number 14",			19 | BITS	},
	{  "FMI14", "FMI 14",						type5bits	},
	{   "CM14", "SPN Conversion Method 14",				type1bit	},
	{   "OC14", "Occurrence Count 14",				type7bits	},
	{  "SPN15", "Suspect Parameter Number 15",			19 | BITS	},
	{  "FMI15", "FMI 15",						type5bits	},
	{   "CM15", "SPN Conversion Method 15",				type1bit	},
	{   "OC15", "Occurrence Count 15",				type7bits	},
	{  "SPN16", "Suspect Parameter Number 16",			19 | BITS	},
	{  "FMI16", "FMI 16",						type5bits	},
	{   "CM16", "SPN Conversion Method 16",				type1bit	},
	{   "OC16", "Occurrence Count 16",				type7bits	},
	{  "SPN17", "Suspect Parameter Number 17",			19 | BITS	},
	{  "FMI17", "FMI 17",						type5bits	},
	{   "CM17", "SPN Conversion Method 17",				type1bit	},
	{   "OC17", "Occurrence Count 17",				type7bits	},
	{  "SPN18", "Suspect Parameter Number 18",			19 | BITS	},
	{  "FMI18", "FMI 18",						type5bits	},
	{   "CM18", "SPN Conversion Method 18",				type1bit	},
	{   "OC18", "Occurrence Count 18",				type7bits	},
	{  "SPN19", "Suspect Parameter Number 19",			19 | BITS	},
	{  "FMI19", "FMI 19",						type5bits	},
	{   "CM19", "SPN Conversion Method 19",				type1bit	},
	{   "OC19", "Occurrence Count 19",				type7bits	},
/*	{  "SPN20", "Suspect Parameter Number 20",			19 | BITS	},
	{  "FMI20", "FMI 20",						type5bits	},
	{   "CM20", "SPN Conversion Method 20",				type1bit	},
	{   "OC20", "Occurrence Count 20",				type7bits	},
	{  "SPN21", "Suspect Parameter Number 21",			19 | BITS	},
	{  "FMI21", "FMI 21",						type5bits	},
	{   "CM21", "SPN Conversion Method 21",				type1bit	},
	{   "OC21", "Occurrence Count 21",				type7bits	},
	{  "SPN22", "Suspect Parameter Number 22",			19 | BITS	},
	{  "FMI22", "FMI 22",						type5bits	},
	{   "CM22", "SPN Conversion Method 22",				type1bit	},
	{   "OC22", "Occurrence Count 22",				type7bits	},
	{  "SPN23", "Suspect Parameter Number 23",			19 | BITS	},
	{  "FMI23", "FMI 23",						type5bits	},
	{   "CM23", "SPN Conversion Method 23",				type1bit	},
	{   "OC23", "Occurrence Count 23",				type7bits	},
	{  "SPN24", "Suspect Parameter Number 24",			19 | BITS	},
	{  "FMI24", "FMI 24",						type5bits	},
	{   "CM24", "SPN Conversion Method 24",				type1bit	},
	{   "OC24", "Occurrence Count 24",				type7bits	},
	{  "SPN25", "Suspect Parameter Number 25",			19 | BITS	},
	{  "FMI25", "FMI 25",						type5bits	},
	{   "CM25", "SPN Conversion Method 25",				type1bit	},
	{   "OC25", "Occurrence Count 25",				type7bits	},
	{  "SPN26", "Suspect Parameter Number 26",			19 | BITS	},
	{  "FMI26", "FMI 26",						type5bits	},
	{   "CM26", "SPN Conversion Method 26",				type1bit	},
	{   "OC26", "Occurrence Count 26",				type7bits	},
	{  "SPN27", "Suspect Parameter Number 27",			19 | BITS	},
	{  "FMI27", "FMI 27",						type5bits	},
	{   "CM27", "SPN Conversion Method 27",				type1bit	},
	{   "OC27", "Occurrence Count 27",				type7bits	},
	{  "SPN28", "Suspect Parameter Number 28",			19 | BITS	},
	{  "FMI28", "FMI 28",						type5bits	},
	{   "CM28", "SPN Conversion Method 28",				type1bit	},
	{   "OC28", "Occurrence Count 28",				type7bits	},
	{  "SPN29", "Suspect Parameter Number 29",			19 | BITS	},
	{  "FMI29", "FMI 29",						type5bits	},
	{   "CM29", "SPN Conversion Method 29",				type1bit	},
	{   "OC29", "Occurrence Count 29",				type7bits	},
	{  "SPN30", "Suspect Parameter Number 30",			19 | BITS	},
	{  "FMI30", "FMI 30",						type5bits	},
	{   "CM30", "SPN Conversion Method 30",				type1bit	},
	{   "OC30", "Occurrence Count 30",				type7bits	},
*/	{     "0", "END!!",						0		},
	},

	309, // PGN 65227 -DM2		09/2006	- VAR
	{
	{   "MIL", "Malfunction Indicator Lamp Status",			type2bits	},
	{   "RSL", "Red Stop Lamp Status",				type2bits	},
	{   "AWL", "Amber Warning Lamp Status",				type2bits	},
	{    "PL", "Protect Lamp Status",				type2bits	},
	{  "FMIL", "Flash Malfunction Indicator Lamp",			type2bits	},
	{  "FRSL", "Flash Red Stop Lamp",				type2bits	},
	{  "FAWL", "Flash Amber Waring Lamp",				type2bits	},
	{   "FPL", "Flash Protect Lamp",				type2bits	},
	{  "SPN1", "Suspect Parameter Number 1",			19 | BITS	},
	{  "FMI1", "FMI 1",						type5bits	},
	{   "CM1", "SPN Conversion Method 1",				type1bit	},
	{   "OC1", "Occurrence Count 1",				type7bits	},
	{  "SPN2", "Suspect Parameter Number 2",			19 | BITS	},
	{  "FMI2", "FMI 2",						type5bits	},
	{   "CM2", "SPN Conversion Method 2",				type1bit	},
	{   "OC2", "Occurrence Count 2",				type7bits	},
	{  "SPN3", "Suspect Parameter Number 3",			19 | BITS	},
	{  "FMI3", "FMI 3",						type5bits	},
	{   "CM3", "SPN Conversion Method 3",				type1bit	},
	{   "OC3", "Occurrence Count 3",				type7bits	},
	{  "SPN4", "Suspect Parameter Number 4",			19 | BITS	},
	{  "FMI4", "FMI 4",						type5bits	},
	{   "CM4", "SPN Conversion Method 4",				type1bit	},
	{   "OC4", "Occurrence Count 4",				type7bits	},
	{  "SPN5", "Suspect Parameter Number 5",			19 | BITS	},
	{  "FMI5", "FMI 5",						type5bits	},
	{   "CM5", "SPN Conversion Method 5",				type1bit	},
	{   "OC5", "Occurrence Count 5",				type7bits	},
	{  "SPN6", "Suspect Parameter Number 6",			19 | BITS	},
	{  "FMI6", "FMI 6",						type5bits	},
	{   "CM6", "SPN Conversion Method 6",				type1bit	},
	{   "OC6", "Occurrence Count 6",				type7bits	},
	{  "SPN7", "Suspect Parameter Number 7",			19 | BITS	},
	{  "FMI7", "FMI 7",						type5bits	},
	{   "CM7", "SPN Conversion Method 7",				type1bit	},
	{   "OC7", "Occurrence Count 7",				type7bits	},
	{  "SPN8", "Suspect Parameter Number 8",			19 | BITS	},
	{  "FMI8", "FMI 8",						type5bits	},
	{   "CM8", "SPN Conversion Method 8",				type1bit	},
	{   "OC8", "Occurrence Count 8",				type7bits	},
	{  "SPN9", "Suspect Parameter Number 9",			19 | BITS	},
	{  "FMI9", "FMI 9",						type5bits	},
	{   "CM9", "SPN Conversion Method 9",				type1bit	},
	{   "OC9", "Occurrence Count 9",				type7bits	},
	{  "SPN10", "Suspect Parameter Number 10",			19 | BITS	},
	{  "FMI10", "FMI 10",						type5bits	},
	{   "CM10", "SPN Conversion Method 10",				type1bit	},
	{   "OC10", "Occurrence Count 10",				type7bits	},
	{  "SPN11", "Suspect Parameter Number 11",			19 | BITS	},
	{  "FMI11", "FMI 11",						type5bits	},
	{   "CM11", "SPN Conversion Method 11",				type1bit	},
	{   "OC11", "Occurrence Count 11",				type7bits	},
	{  "SPN12", "Suspect Parameter Number 12",			19 | BITS	},
	{  "FMI12", "FMI 12",						type5bits	},
	{   "CM12", "SPN Conversion Method 12",				type1bit	},
	{   "OC12", "Occurrence Count 12",				type7bits	},
	{  "SPN13", "Suspect Parameter Number 13",			19 | BITS	},
	{  "FMI13", "FMI 13",						type5bits	},
	{   "CM13", "SPN Conversion Method 13",				type1bit	},
	{   "OC13", "Occurrence Count 13",				type7bits	},
	{  "SPN14", "Suspect Parameter Number 14",			19 | BITS	},
	{  "FMI14", "FMI 14",						type5bits	},
	{   "CM14", "SPN Conversion Method 14",				type1bit	},
	{   "OC14", "Occurrence Count 14",				type7bits	},
	{  "SPN15", "Suspect Parameter Number 15",			19 | BITS	},
	{  "FMI15", "FMI 15",						type5bits	},
	{   "CM15", "SPN Conversion Method 15",				type1bit	},
	{   "OC15", "Occurrence Count 15",				type7bits	},
	{  "SPN16", "Suspect Parameter Number 16",			19 | BITS	},
	{  "FMI16", "FMI 16",						type5bits	},
	{   "CM16", "SPN Conversion Method 16",				type1bit	},
	{   "OC16", "Occurrence Count 16",				type7bits	},
	{  "SPN17", "Suspect Parameter Number 17",			19 | BITS	},
	{  "FMI17", "FMI 17",						type5bits	},
	{   "CM17", "SPN Conversion Method 17",				type1bit	},
	{   "OC17", "Occurrence Count 17",				type7bits	},
	{  "SPN18", "Suspect Parameter Number 18",			19 | BITS	},
	{  "FMI18", "FMI 18",						type5bits	},
	{   "CM18", "SPN Conversion Method 18",				type1bit	},
	{   "OC18", "Occurrence Count 18",				type7bits	},
	{  "SPN19", "Suspect Parameter Number 19",			19 | BITS	},
	{  "FMI19", "FMI 19",						type5bits	},
	{   "CM19", "SPN Conversion Method 19",				type1bit	},
	{   "OC19", "Occurrence Count 19",				type7bits	},
/*	{  "SPN20", "Suspect Parameter Number 20",			19 | BITS	},
	{  "FMI20", "FMI 20",						type5bits	},
	{   "CM20", "SPN Conversion Method 20",				type1bit	},
	{   "OC20", "Occurrence Count 20",				type7bits	},
	{  "SPN21", "Suspect Parameter Number 21",			19 | BITS	},
	{  "FMI21", "FMI 21",						type5bits	},
	{   "CM21", "SPN Conversion Method 21",				type1bit	},
	{   "OC21", "Occurrence Count 21",				type7bits	},
	{  "SPN22", "Suspect Parameter Number 22",			19 | BITS	},
	{  "FMI22", "FMI 22",						type5bits	},
	{   "CM22", "SPN Conversion Method 22",				type1bit	},
	{   "OC22", "Occurrence Count 22",				type7bits	},
	{  "SPN23", "Suspect Parameter Number 23",			19 | BITS	},
	{  "FMI23", "FMI 23",						type5bits	},
	{   "CM23", "SPN Conversion Method 23",				type1bit	},
	{   "OC23", "Occurrence Count 23",				type7bits	},
	{  "SPN24", "Suspect Parameter Number 24",			19 | BITS	},
	{  "FMI24", "FMI 24",						type5bits	},
	{   "CM24", "SPN Conversion Method 24",				type1bit	},
	{   "OC24", "Occurrence Count 24",				type7bits	},
	{  "SPN25", "Suspect Parameter Number 25",			19 | BITS	},
	{  "FMI25", "FMI 25",						type5bits	},
	{   "CM25", "SPN Conversion Method 25",				type1bit	},
	{   "OC25", "Occurrence Count 25",				type7bits	},
	{  "SPN26", "Suspect Parameter Number 26",			19 | BITS	},
	{  "FMI26", "FMI 26",						type5bits	},
	{   "CM26", "SPN Conversion Method 26",				type1bit	},
	{   "OC26", "Occurrence Count 26",				type7bits	},
	{  "SPN27", "Suspect Parameter Number 27",			19 | BITS	},
	{  "FMI27", "FMI 27",						type5bits	},
	{   "CM27", "SPN Conversion Method 27",				type1bit	},
	{   "OC27", "Occurrence Count 27",				type7bits	},
	{  "SPN28", "Suspect Parameter Number 28",			19 | BITS	},
	{  "FMI28", "FMI 28",						type5bits	},
	{   "CM28", "SPN Conversion Method 28",				type1bit	},
	{   "OC28", "Occurrence Count 28",				type7bits	},
	{  "SPN29", "Suspect Parameter Number 29",			19 | BITS	},
	{  "FMI29", "FMI 29",						type5bits	},
	{   "CM29", "SPN Conversion Method 29",				type1bit	},
	{   "OC29", "Occurrence Count 29",				type7bits	},
	{  "SPN30", "Suspect Parameter Number 30",			19 | BITS	},
	{  "FMI30", "FMI 30",						type5bits	},
	{   "CM30", "SPN Conversion Method 30",				type1bit	},
	{   "OC30", "Occurrence Count 30",				type7bits	},
*/	{     "0", "END!!",						0		},
	},

	310,// PGN 64999 -BUSC		06/2007 - 8
	{

	{ "2535", "Bus #1/Utility Dead Bus",				type2bits	},
	{ "2531", "Bus #1/Utility Phase Match",				type2bits	},
	{ "2533", "Bus #1/Utility Frequency Match",			type2bits	},
	{ "2532", "Bus #1/Utility Voltage Match",			type2bits	},
	{ "2534", "Bus #1/Utility In Sync",				type2bits	},
	{ "9999", "DEAD",						type6bits	},
	{ "2517", "Bus #1/Utility AC Phase Difference",			type2bytes	},
//	{ "9999", "DEAD",						type4bytes	},
	{    "0", "END!!",						0		},
	},

	311,// PGN 65000 -BGSC		06/2007	- 8	
	{

	{ "2530", "Bus #1/Generator Dead Bus",				type2bits	},
	{ "2526", "Bus #1/Generator Phase Match",			type2bits	},
	{ "2528", "Bus #1/Generator Frequency Match",			type2bits	},
	{ "2527", "Bus #1/Generator Voltage Match",			type2bits	},
	{ "2529", "Bus #1/Generator In Sync",				type2bits	},
	{ "9999", "DEAD",						type6bits	},
	{ "2516", "Bus #1/Generator AC Phase Difference",		type2bytes	},
//	{ "9999", "DEAD",						type4bytes	},
	{    "0", "END!!",						0		},
	},

	312,// PGN 65001 -BPCAC		06/2007 - 8
	{

	{ "2511", "Bus #1/Phase CA Line-Line AC RMS Voltage",		type2bytes	},
	{ "2515", "Bus #1/Phase C Line-Neutral AC RMS Voltage",		type2bytes	},
	{ "2507", "Bus #1/Phase C AC Frequency",			type2bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	313,// PGN 65002 -BPBAC		06/2007 - 8
	{

	{ "2510", "Bus #1/Phase BA Line-Line AC RMS Voltage",		type2bytes	},
	{ "2514", "Bus #1/Phase B Line-Neutral AC RMS Voltage",		type2bytes	},
	{ "2506", "Bus #1/Phase B AC Frequency",			type2bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	314,// PGN 65003 -BPAAC		06/2007	- 8
	{

	{ "2509", "Bus #1/Phase AB Line-Line AC RMS Voltage",		type2bytes	},
	{ "2513", "Bus #1/Phase A Line-Neutral AC RMS Voltage",		type2bytes	},
	{ "2505", "Bus #1/Phase A AC Frequency",			type2bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	315,// PGN 65004 -BAAC		06/2007	- 8
	{

	{ "2508", "Bus #1/Average Line-Line AC RMS Voltage",		type2bytes	},
	{ "2512", "Bus #1/Average Line-Neutral AC RMS Voltage",		type2bytes	},
	{ "2504", "Bus #1/Average AC Frequency",			type2bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	316,// PGN 65005 -UTACE		06/2007	 - 8
	{

	{ "2502", "Utility Total kW Hours Export",			type4bytes	},
	{ "2503", "Utility Total kW Hours Import",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	317,// PGN 65006 -UPCACR	06/2007	- 8
	{

	{ "2493", "Utility Phase C Reactive Power",			type4bytes	},
	{ "2501", "Utility Phase C Power Factor",			type2bytes	},
	{ "2525", "Utility Phase C Reactive Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
     	{    "0", "END!!",						0		},
	},

	318,// PGN 65007 -UPCACP	06/2007	- 8
	{

	{ "2489", "Utility Phase C Real Power",				type4bytes	},
	{ "2497", "Utility Phase C Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	319,// PGN 65008 -UPCAC		06/2007	-  8
	{

	{ "2477", "Utility Phase CA Line-Line AC RMS Voltage",		type2bytes	},
	{ "2481", "Utility Phase C Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2473", "Utility Phase C AC Frequency",			type2bytes	},
	{ "2485", "Utility Phase C AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	320,// PGN 65009 -UPBACR	06/2007 - 8
	{

	{ "2492", "Utility Phase B Reactive Power",			type4bytes	},
	{ "2500", "Utility Phase B Power Factor",			type2bytes	},
	{ "2524", "Utility Phase B Reactive Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	321,// PGN 65010 -UPBACP	06/2007 - 8
	{

	{ "2488", "Utility Phase B Real Power",				type4bytes	},
	{ "2496", "Utility Phase B Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	322,// PGN 65011 -UPBAC		06/2007 - 8
	{

	{ "2476", "Utility Phase BC Line-Line AC RMS Voltage",		type2bytes	},
	{ "2480", "Utility Phase B Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2472", "Utility Phase B AC Frequency",			type2bytes	},
	{ "2484", "Utility Phase B AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	323,// PGN 65012 -UPACCR	06/2007	- 8
	{

	{ "2491", "Utility Phase A Reactive Power",			type4bytes	},
	{ "2499", "Utility Phase A Power Factor",			type2bytes	},
	{ "2523", "Utility Phase A Reactive Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	324,// PGN 65013 -UPAACP	06/2007	- 8
	{

	{ "2487", "Utility Phase A Real Power",				type4bytes	},
	{ "2495", "Utility Phase A Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	325,// PGN 65014 -UPAAC		06/2007	- 8
	{

	{ "2475", "Utility Phase AB Line-Line AC RMS Voltage",		type2bytes	},
	{ "2479", "Utility Phase A Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2471", "Utility Phase A AC Frequency",			type2bytes	},
	{ "2483", "Utility Phase A AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	326,// PGN 65015 -UTACR		06/2007	- 8
	{

	{ "2490", "Utility Total Reactive Power",			type4bytes	},
	{ "2498", "Utility Overall Power Factor",			type2bytes	},
	{ "2522", "Utility Overall Power Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	327,// PGN 65016 -UTACP		06/2007 - 8
	{

	{ "2486", "Utility Total Real Power",				type4bytes	},
	{ "2494", "Utility Total Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	328,// PGN 65017 -UAAC		06/2007	 - 8
	{

	{ "2474", "Utility Average Line-Line AC RMS Voltage",		type2bytes	},
	{ "2478", "Utility Average Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2470", "Utility Average AC Frequency",			type2bytes	},
	{ "2482", "Utility Average AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	329,// PGN 65018 -GTACE		06/2007	- 8
	{

	{ "2468", "Generator Total kW Hours Export",			type4bytes	},
	{ "2469", "Generator Total kW Hours Import",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	330,// PGN 65019 -GPCACR	06/2007 - 8
	{

	{ "2459", "Generator Phase C Reactive Power",			type4bytes	},
	{ "2467", "Generator Phase C Power Factor",			type2bytes	},
	{ "2521", "Generator Phase C Power Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	331,// PGN 65020 -GPCACP	06/2007	- 8
	{

	{ "2455", "Generator Phase C Real Power",			type4bytes	},
	{ "2463", "Generator Phase C Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	332,// PGN 65021 -GPCAC		06/2007	- 8
	{

	{ "2443", "Generator Phase CA Line-Line AC RMS Voltage",	type2bytes	},
	{ "2447", "Generator Phase C Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2439", "Generator Phase C AC Frequency",			type2bytes	},
	{ "2451", "Generator Phase C AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	333,// PGN 65022 -GPBACRP	06/2007 - 8
	{

	{ "2458", "Generator Phase B Reactive Power",			type4bytes	},
	{ "2466", "Generator Phase B Power Factor",			type2bytes	},
	{ "2520", "Generator Phase B Power Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	334,// PGN 65023 -GPBACP	06/2007	 - 8
	{

	{ "2454", "Generator Phase B Real Power",			type4bytes	},
	{ "2462", "Generator Phase B Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	335,// PGN 65024 -GPBAC		06/2007 - 8
	{

	{ "2442", "Generator Phase BC Line-Line AC RMS Voltage",	type2bytes	},
	{ "2446", "Generator Phase B Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2438", "Generator Phase B AC Frequency",			type2bytes	},
	{ "2450", "Generator Phase B AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	336,// PGN 65025 -GPBAACR	06/2007	- 8
	{

	{ "2457", "Generator Phase A Reactive Power",			type4bytes	},
	{ "2465", "Generator Phase A Power Factor",			type2bytes	},
	{ "2519", "Generator Phase A Power Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	337,// PGN 65026 -GPAACP	06/2007	- 8
	{

	{ "2453", "Generator Phase A Real Power",			type4bytes	},
	{ "2461", "Generator Phase A Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	338,// PGN 65027 -GPAAC		06/2007	- 8
	{

	{ "2441", "Generator Phase AB Line-Line AC RMS Voltage",	type2bytes	},
	{ "2445", "Generator Phase A Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2437", "Generator Phase A AC Frequency",			type2bytes	},
	{ "2449", "Generator Phase A AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	339,// PGN 65028 -GPTACR	06/2007 - 8
	{

	{ "2456", "Generator Total Reactive Power",			type4bytes	},
	{ "2464", "Generator Overall Power Factor",			type2bytes	},
	{ "2518", "Generator Overall Power Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	340,// PGN 65029 -GTACP		06/2007	- 8
	{

	{ "2452", "Generator Total Real Power",				type4bytes	},
	{ "2460", "Generator Total Apparent Power",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	341,// PGN 65030 -GAAC		06/2007	- 8
	{

	{ "2440", "Generator Average Line-Line AC RMS Voltage",		type2bytes	},
	{ "2444", "Generator Average Line-Neutral AC RMS Voltage",	type2bytes	},
	{ "2436", "Generator Average AC Frequency",			type2bytes	},
	{ "2448", "Generator Average AC RMS Current",			type2bytes	},
	{    "0", "END!!",						0		},
	},

	342,// PGN 61461 -RGPTACRP	06/2007	- 8
	{

	{ "3383", "Req Generator Total AC Reactive Power",		type4bytes	},
	{ "3384", "Req Generator Overall Power Factor",			type2bytes	},
	{ "3385", "Req Generator Overall Power Factor Lagging",		type2bits	},
//	{ "9999", "DEAD",						type6bits	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	343,// PGN 61468 -RGAAC		06/2007	- 8
	{

	{ "3386", "Req Generator Average Line-Line AC RMS Voltage",	type4bytes	},
//	{ "9999", "DEAD",						type4bytes	},
	{    "0", "END!!",						0		},
	},

	344,// PGN 64934 -VREP		06/2007	- 8
	{

	{ "3380", "Generator Excitation Field Voltage",			type2bytes	},
	{ "3381", "Generator Excitation Field Current",			type2bytes	},
	{ "3382", "Generator Output Voltage Bias Percentage",		type2bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	345,// PGN 64935 -VROM		06/2007	- 8
	{

	{ "3375", "Voltage Regulator Load Compensation Mode",		type3bits	},
	{ "3376", "Voltage Regulator VAr/Power Factor Operating Mode",	type3bits	},
	{ "3377", "Voltage Regulator Underfreq Compensation Enabled",	type2bits	},
	{ "3378", "Voltage Regulator Soft Start State",			type2bits	},
	{ "3379", "Voltage Regulator Enabled",				type2bits	},
//	{ "9999", "DEAD",						type4bits	},
//	{ "9999", "DEAD",						type4bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},
	
	346,// PGN 61470 -GC2		06/2007	- 8
	{

	{ "3938", "Generator Governing Bias",				type2bytes	},
//	{ "9999", "DEAD",						type4bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	347,// PGN 64909 -UTACER	06/2007 - 8
	{

	{ "3595", "Utility Total kVAr Hours Export",			type4bytes	},
	{ "3596", "Utility Total kVAr Hours Import",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	348,// PGN 64910 -GTACER	06/2007	- 8
	{

	{ "3593", "Generator Total kVAr Hours Export",			type4bytes	},
	{ "3594", "Generator Total kVAr Hours Import",			type4bytes	},
	{    "0", "END!!",						0		},
	},

	349,// PGN 64911 -GTACPP	06/2007	- 8
	{

	{ "3590", "Generator Total Percent kW",				type2bytes	},
	{ "3591", "Generator Total Percent kVA",			type2bytes	},
	{ "3592", "Generator Total Percent kVAr",			type2bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	350,// PGN 64913 -ACS		06/2007	- 8
	{

	{ "3545", "Generator Circuit Breaker Status",			type3bits	},
	{ "3546", "Utility Circuit Breaker Status",			type3bits	},
	{ "9999", "DEAD",						type2bits	},
	{ "3547", "Automatic Transfer Switch Status",			type3bits	},
//	{ "9999", "DEAD",						type5bits	},
//	{ "9999", "DEAD",						type4bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	351,// PGN 64915 -GC1		06/2007- 8
	{

	{ "3542", "Requested Engine Control Mode",			type4bits	},
	{ "3567", "Generator Control Not In Automatic Start State",	type2bits	},
	{ "3568", "Generator Not Ready to Automatically Parallel State",type2bits	},
	{ "4078", "Generator Alternator Efficiency",			type2bytes	},
	{ "4079", "Generator Governing Speed Command",			type2bits	},
	{ "4080", "Generator Frequency Selection",			type4bits	},
//	{ "9999", "DEAD",						type2bits	},
//	{ "9999", "DEAD",						type4bytes	},
	{    "0", "END!!",						0		},
	},

	352,// PGN 2048 -AUXIO5		01/2008 - 8
	{

	{ "4155", "Auxiliary I/O Channel #6",				type2bytes	},
	{ "4156", "Auxiliary I/O Channel #5",				type2bytes	},
	{ "4157", "Auxiliary I/O Channel #4",				type2bytes	},
	{ "4158", "Auxiliary I/O Channel #3",				type2bytes	},
	{    "0", "END!!",						0		},
	},

	353,// PGN 39936 -AUXIO7	01/2008	- 8
	{

	{ "4167", "Auxiliary I/O Channel #18",				type1byte	},
	{ "4168", "Auxiliary I/O Channel #17",				type1byte	},
	{ "4169", "Auxiliary I/O Channel #16",				type1byte	},
	{ "4170", "Auxiliary I/O Channel #15",				type1byte	},
	{ "4171", "Auxiliary I/O Channel #22",				type1byte	},
	{ "4172", "Auxiliary I/O Channel #21",				type1byte	},
	{ "4173", "Auxiliary I/O Channel #20",				type1byte	},
	{ "4174", "Auxiliary I/O Channel #19",				type1byte	},
	{    "0", "END!!",						0		},
	},

	354,// PGN 40192 -AUXIO6	01/2008	 - 8
	{

	{ "4159", "Auxiliary I/O Channel #10",				type1byte	},
	{ "4160", "Auxiliary I/O Channel #9",				type1byte	},
	{ "4161", "Auxiliary I/O Channel #8",				type1byte	},
	{ "4162", "Auxiliary I/O Channel #7",				type1byte	},
	{ "4163", "Auxiliary I/O Channel #14",				type1byte	},
	{ "4164", "Auxiliary I/O Channel #13",				type1byte	},
	{ "4165", "Auxiliary I/O Channel #12",				type1byte	},
	{ "4166", "Auxiliary I/O Channel #11",				type1byte	},
	{    "0", "END!!",						0		},
	},

	355,// PGN 64840 -O2FT2		01/2008	- 8
	{

	{ "4239", "Long-term Fuel Trim - Bank 2",			type2bytes	},
	{ "4238", "Short-term Fuel Trim - Bank 2",			type2bytes	},
	{ "4241", "Eng Exhaust Gas Oxy Sensor Closed Loop Op, Bank 2",	type4bits	},
//	{ "9999", "DEAD",						type4bits	},
//	{ "9999", "DEAD",						type3bytes	},
	{    "0", "END!!",						0		},
	},

	356,// PGN 64841 -O2FT1		01/2008	- 8
	{

	{ "4237", "Long-term Fuel Trim - Bank 1",			type2bytes	},
	{ "4236", "Short-term Fuel Trim - Bank 1",			type2bytes	},
	{ "4240", "Eng Exhaust Gas Oxy Sensor Closed Loop Op, Bank 1",	type4bits	},
//	{ "9999", "DEAD",						type4bits	},
//	{ "9999", "DEAD",						type3bytes	},
	{    "0", "END!!",						0		},
	},

	357,// PGN 64849 -ACCVC		01/2008 - 8
	{

	{ "4198", "Aftercooler Coolant Thermostat Mode",		type2bits	},
	{ "9999", "DEAD",						type6bits	},
	{ "4199", "Desired Aftercooler Coolant Inlet Temperature",	type1byte	},
	{ "4200", "Desired Aftercooler Coolant Thermostat Opening",	type1byte	},
//	{ "9999", "DEAD",						type4bytes	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	358,// PGN 64850 -ECCVC		01/2008	- 8
	{

	{ "4195", "Engine Coolant Thermostat Mode",			type2bits	},
	{ "9999", "DEAD",						type6bits	},
	{ "4196", "Desired Engine Coolant Pump Outlet Temperature",	type1byte	},
	{ "4197", "Desired Engine Coolant Thermostat Opening",		type1byte	},
//	{ "9999", "DEAD",						type4bytes	},
//	{ "9999", "DEAD",						type1byte	},
	{    "0", "END!!",						0		},
	},

	359,// PGN 64851 -EAI		01/2008	- 8
	{

	{ "4151", "Engine Exhaust Gas Temperature Average",		type2bytes	},
	{ "4153", "Engine Exhaust Gas Temperature Average - Bank 1",	type2bytes	},
	{ "4152", "Engine Exhaust Gas Temperature Average - Bank 2",	type2bytes	},
//	{ "9999", "DEAD",						type2bytes	},
	{    "0", "END!!",						0		},
	},

	360,// PGN 65230 -DM5		09/2006	- VAR
	{
	{ "1218", "Active Trouble Codes",				type1byte	},
	{ "1219", "Previously Active Trouble Codes",			type1byte	},
	{ "1220", "OBD Compliance",					type1byte	},
	{ "1221", "Continuously Monitored Systems Support/Status",	type1byte	},
	{ "1222", "Non-continuously Monitored Systems Support",		type2bytes	},
	{ "1223", "Non-continuously Monitored Systems Support",		type2bytes	},
	{    "0", "END!!",						0		},
	},

	361,// PGN 65228 -DM3
	{
	{ "Clear", "Clear/Reset Previously Active DTCs",		type1bit	},
	{   "0", "END!!",						0		},
	},

	362,// PGN 65235 -DM11
	{
	{ "ClrAct", "Clear/Reset For Active DTCs",			type1bit	},
	{   "0", "END!!",						0		},
	},

	363, // PGN 65229 -DM4		
	{
	{   "FFL", "Freeze Frame Length",				type8bits	},
	{   "SPN", "Suspect Parameter Number",				19 | BITS	},
	{   "FMI", "FMI",						type5bits	},
	{    "CM", "SPN Conversion Method",				type1bit	},
	{    "OC", "Occurrence Count",					type7bits	},
	{   "ETM", "Engine Torque Mode",				type8bits	},
	{   "BST", "Boost",						type8bits	},
	{    "ES", "Engine Speed",					type2bytes	},
	{    "EL", "Engine % Load",					type8bits	},
	{   "ECT", "Engine Coolant Temperature",			type8bits	},
	{    "VS", "Vehicle Speed",					type2bytes	},
	{   "0", "END!!",						0		},
	},
	
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Suspect Parameter
//

// Dynamic Class

AfxImplementDynamicClass(CSPN, CMetaItem);

// Constructor

CSPN::CSPN(void)
{
	m_Number = 0;

	m_Size   = 0;

	m_DM	 = "";

	AddMeta();
   	}

CSPN::CSPN(SPN Param)
{
	CString Num   = CString(Param.m_Number);

	TCHAR   Digit = Num.GetAt(0);

	m_Number = 0;

	m_DM = "";

	if( Digit >= '1' && Digit <= '9' ) {

		m_Number = strtol(Param.m_Number, NULL, 10);
		}

	else if ( Digit != '0' ) {

		m_DM = Param.m_Number;
		}

	m_Size = GetSizeAsBits(Param.m_Size);

	AddMeta();
     	}

CSPN::CSPN(UINT uNumber, UINT uSize, CString DM)
{
	m_Number = uNumber;

	m_Size   = GetSizeAsBits(uSize);

	m_DM	 = DM;

	AddMeta();
	}

// Destructor

CSPN::~CSPN(void)
{
	
	} 

// Download Support

BOOL CSPN::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_Size));

	Init.AddWord(WORD(m_Number));

	return TRUE;
	}

// Meta Data Creation

void CSPN::AddMetaData(void)	
{
	CMetaItem::AddMetaData();
  
	Meta_AddInteger(Size);

	Meta_AddInteger(Number);

	Meta_SetName(IDS_J_SPN);
	}

// Data Access

UINT CSPN::GetSize(void)
{
	m_Size = GetSizeAsBits(m_Size);
	
	return m_Size;
	}

void CSPN::SetSize(UINT uSize)
{	
	m_Size = GetSizeAsBits(uSize);
	} 

BOOL CSPN::IsDM(void)
{
	if( m_Number == 0 ) {

		return TRUE;
		}
	
	if( m_DM == "" ) {

		return FALSE;
		}

	TCHAR Digit = m_DM.GetAt(0);

	if( Digit >= '1' && Digit <= '9' ) {

		return FALSE;
		}

	return TRUE;
	}

// Help

UINT CSPN::GetSizeAsBits(UINT uSize)
{
	UINT Size = uSize;

	BOOL fDM = IsDM();
	
	if( Size < BITS ) {

		switch( Size ) {

			case type1byte:

				Size = 8;
				break;

			case type2bytes:

				Size = 16;
				break;

			case type3bytes:

				Size = fDM ? 19 : 24;
				break;

			case type4bytes:

				Size = 32;
				break;
			}

		Size |= BITS;
		}
	else {
		if( fDM ) {

			UINT uBits = Size & 0x7F;

			if( uBits == type3bytes ) {

				Size = 19 | BITS;
				}
			}
		} 

	return Size;
	}


//////////////////////////////////////////////////////////////////////////
//
// SPN List Class
//

// Dynamic Class

AfxImplementDynamicClass(CSPNList, CItemList);

// Constructor

CSPNList::CSPNList(void)
{
	}

// Destructor

CSPNList::~CSPNList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CSPN * CSPNList::GetItem(INDEX Index) const
{
	return (CSPN *) CItemList::GetItem(Index);
	}

CSPN * CSPNList::GetItem(UINT uPos) const
{
	return (CSPN *) CItemList::GetItem(uPos);
	}

// Operations

CSPN * CSPNList::AppendItem(CSPN * pSPN)
{
	pSPN->SetParent(this);

	if( CItemList::AppendItem(pSPN) ) {

		return pSPN;
		}  

	return NULL;
	}


//////////////////////////////////////////////////////////////////////////
//
// J1939 Parameter Group
//

// Dynamic Class

AfxImplementDynamicClass(CPGN, CMetaItem);

// Constructor

CPGN::CPGN(void)
{
	m_Number    = 0;
		    
	m_Priority  = 7;	
		    
	m_SendReq   = 0;
		    
	m_Prefix    = "";
		    
	m_Title     = "";
		    
	m_pSPNs     = New CSPNList;
		    
	m_RepRate   = 1000;
		    
	m_Table     = 0;
		    
	m_Diag	    = 0;
		    
	m_Enhanced  = 0;

	m_Direction = 0;
		
	m_Ref       = 0;

	m_fMark	    = FALSE;

	AddMeta();
	}

CPGN::CPGN(CPGN * pPGN)
{
	if( pPGN ) {

		m_Number    = pPGN->m_Number;

		m_Priority  = pPGN->m_Priority;

		m_SendReq   = pPGN->m_SendReq;

		m_Prefix    = pPGN->m_Prefix;  

		m_Title     = pPGN->m_Title;

		m_RepRate   = pPGN->m_RepRate;

		m_Table     = pPGN->m_Table;

		m_pSPNs     = New CSPNList;

		m_Diag      = pPGN->m_Diag;

		m_Enhanced  = pPGN->m_Enhanced;

		m_Direction = pPGN->m_Direction;

		m_fMark	    = pPGN->m_fMark;

		m_Ref	    = 0;

		UINT uCount = pPGN->GetSPNs()->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {
			
			CSPN * pNew = pPGN->GetSPNs()->GetItem(u);

			UINT uSize = pNew->GetSize();

			CString DM = pNew->m_DM;

			m_pSPNs->AppendItem(New CSPN(pNew->m_Number, uSize, DM));
			}

		AddMeta();
		} 
	}
 
CPGN::CPGN(UINT Number, CString Prefix, CString Title, UINT Priority, BOOL SendReq, UINT RepRate) : CMetaItem()
{
	m_Number    = Number;
		    
	m_Priority  = Priority;
		    
	m_SendReq   = SendReq;
		    
	m_Prefix    = Prefix;
		    
	m_Title     = Title;
		    
	m_pSPNs     = New CSPNList;
		    
	m_RepRate   = RepRate;
		    
	m_Table     = 0;
		    
	m_Diag      = 0;
		    
	m_Enhanced  = 0;

	m_Direction = 0;
		
	m_Ref       = 0;

	AddMeta();
	}


CPGN::CPGN(UINT Number, CString Prefix, CString Title, PARAM Params, UINT Priority, BOOL SendReq, UINT RepRate) : CMetaItem()
{
	m_Number    = Number;
		    
	m_Priority  = Priority;
		    
	m_SendReq   = SendReq;
		    
	m_Prefix    = Prefix;
		    
	m_Title     = Title;
		    
	m_Params    = Params;
		    
	m_pSPNs     = New CSPNList;
		    
	m_RepRate   = RepRate;
		    
	m_Table     = 0;
		    
	m_Diag      = 0;
		    
	m_Enhanced  = 0;

	m_Direction = 0;

	m_Ref       = 0;

	CreateSPNList(this);

	AddMeta();
	}

// Destructor
 
CPGN::~CPGN(void)
{
	m_pSPNs->DeleteAllItems(TRUE);
	} 

// Download Support

BOOL CPGN::MakeInitData(CInitData &Init)
{
	if( m_pSPNs ) {

		Init.AddLong(LONG(m_Number));

		Init.AddByte(BYTE(m_Diag));

		Init.AddByte(BYTE(m_Priority));

		Init.AddByte(BYTE(m_SendReq));

		UINT uCount = m_pSPNs->GetItemCount();

		Init.AddWord(WORD(uCount));

		for( UINT u = 0; u < uCount; u++ ) {

			m_pSPNs->GetItem(u)->MakeInitData(Init);
			}

		Init.AddLong(LONG(m_RepRate));

		Init.AddByte(BYTE(m_Enhanced));

		Init.AddByte(BYTE(m_Direction));
		}
	else {
		Init.AddWord(0);
		}   
		
	return TRUE;
	}

// Meta Data Creation

void CPGN::AddMetaData(void)	
{
	CMetaItem::AddMetaData();
  
	Meta_AddInteger(Number);

	Meta_AddInteger(Priority);
	
	Meta_AddInteger(SendReq);

	Meta_AddCollect(SPNs);

	Meta_AddInteger(RepRate);

	Meta_AddString(Prefix);

	Meta_AddString(Title);

	Meta_AddInteger(Diag);

	Meta_AddInteger(Enhanced);

	Meta_AddInteger(Direction);

	Meta_AddInteger(Ref);

	Meta_SetName((IDS_J_PGN));
	}

// Attributes

BOOL CPGN::IsDirty(BOOL fReq, UINT uPriority, UINT uRepRate)
{
	return fReq != m_SendReq || uPriority != m_Priority || uRepRate != m_RepRate;
	}

BOOL CPGN::IsDM(void)
{	
	if( m_pSPNs ) {

		UINT uCount = m_pSPNs->GetItemCount();

		for( UINT u = 0; u < uCount - 1; u++ ) {

			CSPN * pSPN = m_pSPNs->GetItem(u);

			if( !pSPN->m_Number && !pSPN->m_DM.IsEmpty() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPGN::IsThisPGN(UINT uNum, UINT uDir)
{
	return m_Number == uNum && m_Direction == uDir;
	}

// SPN List Management

BOOL CPGN::CreateSPNList(CPGN * pPGN)
{
	UINT uIndex = 0;

	CSPN * pSPN = New CSPN(pPGN->m_Params.m_SPN[uIndex]);

	while( pSPN->m_Number != 0 || !pSPN->m_DM.IsEmpty() ) {
   
		m_pSPNs->AppendItem(pSPN);

		uIndex++;

		pSPN = New CSPN(pPGN->m_Params.m_SPN[uIndex]);
		}

	delete pSPN;
	
	return TRUE;
	}

BOOL CPGN::GrowSPNList(UINT uCount)
{
	if( !m_pSPNs ) {

		m_pSPNs = New CSPNList;
		}

	if( m_pSPNs ) {

		UINT uIndex = m_pSPNs->GetItemCount();

		if( uIndex > uCount ) {

			INDEX i = m_pSPNs->GetTail();

			for( UINT u = uIndex; u > uCount; i = m_pSPNs->GetTail(), u-- ) {

				CSPN * pSPN = m_pSPNs->GetItem(i);

				m_pSPNs->RemoveItem(pSPN);

				delete pSPN;
				}
			}

		else {
	       		while( uIndex < uCount ) {

				UINT uSize = type1bit;

				CSPN * pSPN = New CSPN(uIndex + 1, uSize, "");

				m_pSPNs->AppendItem(pSPN);

				uIndex++;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

CSPNList * CPGN::GetSPNs(void)
{
	return m_pSPNs;
	}

//////////////////////////////////////////////////////////////////////////
//
// PGN List Class
//

// Dynamic Class

AfxImplementDynamicClass(CPGNList, CItemList);

// Constructor

CPGNList::CPGNList(void)
{
	
	}

// Destructor

CPGNList::~CPGNList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CPGN * CPGNList::GetItem(INDEX Index) const
{
	return (CPGN *) CItemList::GetItem(Index);
	}

CPGN * CPGNList::GetItem(UINT uPos) const
{
	return (CPGN *) CItemList::GetItem(uPos);
	}

// Operations

CPGN * CPGNList::AppendItem(CPGN * pPGN)
{
	pPGN->SetParent(this);

	if( CItemList::AppendItem(pPGN) ) {

		return pPGN;
		}

	return NULL;
	}

INDEX CPGNList::InsertItem(CPGN * pPGN, CPGN * pBefore)
{
	pPGN->SetParent(this);
	
	return CItemList::InsertItem(pPGN, pBefore);
	}

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver
//

// Instantiator

ICommsDriver * Create_CANJ1939Driver(void)
{
	return New CCANJ1939Driver;
	}

// Constructor			        

CCANJ1939Driver::CCANJ1939Driver(void)
{
	m_wID		= 0x4036;

	m_uType		= driverSlave;

	m_Manufacturer	= "SAE";
	
	m_DriverName	= "J1939";
	
	m_Version	= "1.09";
	
	m_ShortName	= "J1939";

	m_DevRoot	= "ECU";

	m_pPGNs		= New CPGNList;

	m_fSet		= FALSE;

	m_fRebuild	= FALSE;

	LoadPGNs(NULL);
	}

// Destructor

CCANJ1939Driver::~CCANJ1939Driver(void)
{
	delete m_pPGNs;
	}   
	
// Driver Data

UINT CCANJ1939Driver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagRemotable | dflagRpcMaster;
	}

// Binding Control

UINT CCANJ1939Driver::GetBinding(void)
{
	return bindJ1939;
	}

void CCANJ1939Driver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 250000;

	CAN.m_Terminate = FALSE;
	}

// Configuration

CLASS CCANJ1939Driver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CCANJ1939DriverOptions);
	}
 
CLASS CCANJ1939Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCANJ1939DeviceOptions);
	}

// Notifications

void CCANJ1939Driver::NotifyInit(CItem * pConfig)
{
	CCANJ1939DeviceOptions * pOptions = ( CCANJ1939DeviceOptions * ) pConfig;

	if( pOptions ) {

		m_fRebuild = TRUE;

		pOptions->Rebuild();

		m_fRebuild = FALSE;
		}
	}

// Address Management

BOOL CCANJ1939Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem *pDrvCfg)
{
	LoadPGNs(pDrvCfg);

	m_fSet = TRUE;
	
	CCANJ1939Dialog Dlg(*this, Addr, pConfig, fPart);  
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CCANJ1939Driver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

BOOL CCANJ1939Driver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}
 
// Data Access

CPGN * CCANJ1939Driver::GetPGN(UINT uIndex)
{
	if( uIndex < GetPGNCount() ) {
	
		return m_pPGNs->GetItem(uIndex);
		}

	return NULL;
	}

CPGN * CCANJ1939Driver::GetPGN(CString Text)
{
	UINT uFind  = Text.Find('\t');
	
	if( uFind < NOTHING ) {
		
		CString Prefix = Text.Mid(0, uFind);

		UINT uCount = m_pPGNs->GetItemCount();
     	   
		for( UINT u = 0; u < uCount; u++ ) {

			if( Prefix == m_pPGNs->GetItem(u)->m_Prefix ) { 

				return m_pPGNs->GetItem(u);
				}
			}
		}
	
	return NULL;
	}

UINT  CCANJ1939Driver::GetPGNCount(void)
{
	return m_pPGNs->GetItemCount();
	}

CString CCANJ1939Driver::GetFullPGString(CPGN * pPGN, BOOL fNum)
{
	CString Format = "";

	if( pPGN ) { 

		if( fNum ) {
		
			Format = "%u\t";     
					        
			Format.Printf(Format, pPGN->m_Number);

			Format += pPGN->m_Title;

			Format += CString("-") + pPGN->m_Prefix;
			}
		else {
			Format = pPGN->m_Prefix;
			}		
		}

	return Format;
	}

CString CCANJ1939Driver::GetPGString(CPGN * pPGN, UINT uOffset)
{
	CString Format = "ERROR";

	if( pPGN && pPGN->GetSPNs() ) {

		if( uOffset < SPN_MAX && uOffset < pPGN->GetSPNs()->GetItemCount() ) {

			if( pPGN->GetSPNs()->GetItem(uOffset)->GetSize() == 0 ) {

				return Format;
				}

			Format = pPGN->m_Prefix + CString("-");

			CString Number = "%u";

			CSPN * pSPN = pPGN->GetSPNs()->GetItem(uOffset);

			if( !pSPN->m_DM.IsEmpty() ) {

				Number = pSPN->m_DM;
				}
			else { 

				Number.Printf(Number, pSPN->m_Number); 
				} 

			Format += Number;
		
			Format += ".";

			Format += GetTypeString(pPGN, uOffset);
			}
		} 

	return Format;
	}

CString CCANJ1939Driver::GetTypeString(CPGN * pPGN, UINT uOffset)
{
	if( pPGN && pPGN->GetSPNs() && uOffset < pPGN->GetSPNs()->GetItemCount() ) {

		UINT uSize = pPGN->GetSPNs()->GetItem(uOffset)->GetSize() & 0x7F;
		
		CString Format = "%u bit";

		CString Text;

		Text.Printf(Format, uSize);
	
		return Text;
		}
	
	return "";
	}


CString CCANJ1939Driver::RemovePGFormat(CString Format)
{
	CString Text = Format;
	
	UINT uFind = Text.Find('\t');

	UINT u = 1;

	while( Text.Find('\t', uFind + u) < NOTHING ) {

		u++;
		}

      	Text.Delete(uFind, u);

	return Text;
       	}

UINT CCANJ1939Driver::FindPGNIndex(CAddress Addr, CItem * pConfig)
{
	CPGN * pPGN = GetDevicePGN(Addr, pConfig);

	if( pPGN ) {

		UINT uNum   = pPGN->m_Number;

		UINT uDir   = pPGN->m_Direction;

		UINT uCount = m_pPGNs->GetItemCount();
     	   
		for( UINT u = 0; u < uCount; u++ ) {

			if( m_pPGNs->GetItem(u)->IsThisPGN(uNum, uDir) ) {

				return u;
				}
			}
		}
	
	return NOTHING;
	}

CPGN * CCANJ1939Driver::GetDevicePGN(CAddress Addr, CItem * pConfig)
{
	CCANJ1939DeviceOptions * pOptions = (CCANJ1939DeviceOptions *) pConfig;

	CPGNList * pPGNs = pOptions->GetPGNList();

	if( pPGNs ) {

		UINT uCount = pPGNs->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CPGN * pPGN = pPGNs->GetItem(u);

			CAddress Ref;

			Ref.m_Ref = pPGN->m_Ref;

			if( Addr.a.m_Table == Ref.a.m_Table ) {

				return pPGN;
				}
			}
		}
	
	return NULL;
	}

BOOL CCANJ1939Driver::DeleteDevicePGN(CAddress Addr, CItem *pConfig)
{
	CPGN * pPGN = GetDevicePGN(Addr, pConfig);

	if( pPGN ) {

		CCANJ1939DeviceOptions * pOptions = ( CCANJ1939DeviceOptions * ) pConfig;

		if( pOptions ) {

			if( pOptions->GetPGNList()->RemoveItem(pPGN) ) {

				delete pPGN;

				return TRUE;
				}
			}
		}

	return FALSE;						     
	}

BOOL CCANJ1939Driver::IsTextValid(CString Text, CPGN * pPGN)
{
	if( Text.IsEmpty() ) {

		return TRUE;
		}
	
	UINT uFind = Text.Find('*');
		
	CString Pre = Text;

	CString Part = Text;

	if( uFind < NOTHING ) {

		Pre  = Text.Mid(0, uFind);

		Part = Text.Mid(0, uFind);  
		} 

	uFind = Pre.Find('.');

	if( uFind < NOTHING ) {

		uFind = Pre.Find('-');

		if( uFind < NOTHING ) {

			if( !isdigit(Pre.GetAt(uFind + 1)) ) {

				uFind = Pre.Find('-', uFind + 1);

				if( uFind < NOTHING ) {

					Pre = Pre.Mid(0, uFind);

					Pre.Remove('-');
					}
				}
			else {
				Pre = Pre.Mid(0, uFind);

				Pre.Remove('-');

				Pre.Remove('/');
				}
			}
		}
	else {

	      uFind = Pre.Find('-');

		if( uFind < NOTHING ) {
			
			Pre = Pre.Mid(uFind + 1);

			Pre.Remove('-');
			}
		}

	UINT uNum = tstrtoul(Part, NULL, 10);

	if( Pre == pPGN->m_Prefix && uNum == pPGN->m_Number ) {

		return TRUE;
		}

	// NOTE:  Do not include size in string check

	CString PGString = GetPGString(pPGN, 0);

	if( PGString.GetLength() < Part.GetLength() ) {
		
		uFind  = PGString.Find('.');

		if( uFind < NOTHING ) {

			PGString = PGString.Left(uFind);
			}

		uFind = Part.Find('.');

		if( uFind < NOTHING ) {

			Part = Part.Left(uFind);
			}
		}
		
	if( Part == PGString ) {
		
		return TRUE;
		}

	return FALSE;
	}

CPGN * CCANJ1939Driver::FindPGN(CString Text)
{
	UINT uCount = m_pPGNs->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CPGN * pPGN = GetPGN(u);
		
		if( pPGN ) {

			if( IsTextValid(Text, pPGN) ) {

				return pPGN;
				}
			}
		}

	return NULL;
	}

CPGN * CCANJ1939Driver::FindPGN(UINT uNumber)
{
	UINT uCount = m_pPGNs->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CPGN * pPGN = m_pPGNs->GetItem(u);

		if( uNumber == pPGN->m_Number ) {

			return pPGN;
			}
		}
	
	return NULL;
	}

UINT CCANJ1939Driver::FindPGNNumber(CAddress Addr)
{
	UINT uNumber = NOTHING;
	
	if( Addr.m_Ref ) {

		UINT uPF = Addr.a.m_Extra;

		uNumber = Addr.a.m_Offset >> 8;

		if( uPF ) {

			uNumber |= (0xEF + uPF) << 8;
			}

		else if ( Addr.a.m_Offset & 0x80 ) {

			uNumber |= 0xFF00;
			}
		else {
			uNumber = Addr.a.m_Offset & 0xFF00;
			}

		// TODO:  Enable for possible future DP support ( PGNs > 65535 ) - NOT TESTED !
		/* 
	      	if( Addr.a.m_Offset & 0x40 ) {

			uNumber |= 0x1 << 16;
			}
		*/
		}

	return uNumber;	
	}

CPGN * CCANJ1939Driver::UpdateUserList(CPGNList * pList, CPGN * pPGN)
{
	if( pList && pPGN ) {

		UINT uCount = pList->GetItemCount();
	
		for( UINT u = 0; u < uCount; u++ ) {

			if( pList->GetItem(u)->m_Number == pPGN->m_Number ) {

				CPGN * pOpt = pList->GetItem(u);

				pPGN->m_Priority  = pOpt->m_Priority;
						  
				pPGN->m_RepRate   = pOpt->m_RepRate;
						  
				pPGN->m_SendReq   = pOpt->m_SendReq;
						  
				pPGN->m_Prefix    = pOpt->m_Prefix;
						  
				pPGN->m_Diag      = pOpt->m_Diag;
						  
				pPGN->m_Enhanced  = pOpt->m_Enhanced;

				pPGN->m_Direction = pOpt->m_Direction;

				pList->InsertItem(New CPGN(pPGN), pOpt);
			   
				pList->RemoveItem(pOpt);

				pPGN = pList->GetItem(u);

				delete pOpt;

				return pPGN;
				}
			}
		}

	return NULL;
	}


void  CCANJ1939Driver::SetList(CPGNList * pList)
{
	if( !m_fSet && pList && m_pPGNs ) {

		UINT uCount = pList->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {

			m_pPGNs->AppendItem(New CPGN(pList->GetItem(u)));

			m_fSet = TRUE;
			}
		}
	}

CString CCANJ1939Driver::FindTextFromPrefix(CString Prefix)
{
	if( m_pPGNs ) {

		UINT uCount = m_pPGNs->GetItemCount();
	
		for( UINT u = 0; u < uCount; u++ ) {

			CPGN * pPGN = m_pPGNs->GetItem(u);

			if( pPGN->m_Prefix == Prefix ) {

				return CCANJ1939Driver::GetFullPGString(pPGN, TRUE);
				}
			}
		}

	return "";	
	}

// Address Helpers

BOOL CCANJ1939Driver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem *pDrvCfg)
{
	CCANJ1939DriverOptions * pOpt = (CCANJ1939DriverOptions *) pDrvCfg;

	if( pOpt ) {
		
		SetList(pOpt->m_pPGNList);
		}

	return ParseAddress(Error, Addr, pConfig, Text);
	}

BOOL CCANJ1939Driver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	if( Addr.m_Ref && Text.IsEmpty() ) {

		return TRUE;
		}

	CCANJ1939DeviceOptions * pOptions = ( CCANJ1939DeviceOptions * ) pConfig;

	CPGN * pPGN = FindPGN(Text);

	if( !pPGN && pOptions ) {

		pPGN = pOptions->GetPGN(Text);

		if( !pPGN ) {

			UINT uPGN = Addr.a.m_Offset;
			
			if( Addr.a.m_Extra ) {
				
				uPGN = ((Addr.a.m_Offset >> 8) + ((0xF0 + Addr.a.m_Extra - 1) << 8));
				}

			pPGN = pOptions->GetPGN(uPGN);
			}
		}

	if( pPGN && pOptions && CheckRebuild(pOptions, pPGN) ) {

		UINT uNum = pPGN->m_Number;

		UINT uPF  = uNum >> 8;

		Addr.a.m_Extra = 0;

		Addr.a.m_Offset = uNum & 0xFF00;

		if( uPF >= 0xF0 ) {

			Addr.a.m_Extra = uPF - 0xEF;

			Addr.a.m_Offset = uNum << 8;

			if( uPF == 0xFF ) {

				Addr.a.m_Offset |= 0x80;
				}

			// TODO:  Enable for possible future DP support ( PGNs > 65535 ) - NOT TESTED !
			/* 
			else if( uPF > 0xFF ) {
	
				Addr.a.m_Offset |= 0x40;
				}
			*/
			}

		UINT uTable = pPGN->m_Table;

		if( uTable > 0 ) {

			Addr.a.m_Table  = uTable;
			}
		else {
			Addr.a.m_Table = pOptions->GetNextTable();
	
			pPGN->m_Table  = Addr.a.m_Table;
			}

		Addr.a.m_Type = addrLongAsLong;

		CString PGNText = Text;

		UINT uFind = Text.Find('*');
		       
		if( uFind < NOTHING ) {

			CPGN * pNew = New CPGN(pPGN);

			PGNText = Text.Right(Text.GetLength() - uFind);
			
			BOOL fReq = pPGN->m_SendReq;

			UINT uPriority = pPGN->m_Priority;

			UINT uRepRate = pPGN->m_RepRate;

			UINT i = 1;

			if( PGNText.GetAt(i) == 'R' ) {

				pNew->m_SendReq = TRUE;	       

				i++;
				}
			else {
				pNew->m_SendReq = FALSE;
				}
			
			pNew->m_Priority = PGNText.GetAt(i) - 0x30;

			i++;
				
			if( i + 5 <= PGNText.GetLength() ) {

				pNew->m_RepRate = tstrtol(PGNText.Mid(i, 5), NULL, 10);
				}
				
			if( pNew->IsDirty(fReq, uPriority, uRepRate) ) {

				pConfig->SetDirty();
				}

			if( pOptions ) {

				pNew->m_Ref = Addr.m_Ref;

				pOptions->MakePGN(pNew);

				return TRUE;
				} 

			delete pNew; 
			}

		if( m_fRebuild ) {

			pPGN->m_Ref = Addr.m_Ref;
			}
		
		return TRUE;
		}	
	
	Error.Set("Invalid Parameter Group", 0);

	return FALSE; 
	}

BOOL CCANJ1939Driver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem *pDrvCfg)
{
	CCANJ1939DriverOptions * pOpt = (CCANJ1939DriverOptions *) pDrvCfg;

	if( pOpt ) {
		
		SetList(pOpt->m_pPGNList);
		}

	return ExpandAddress(Text, pConfig, Addr);
	}

BOOL CCANJ1939Driver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
 	if( Addr.m_Ref ) {

		UINT uNumber = FindPGNNumber(Addr);

		CCANJ1939DeviceOptions * pOptions = ( CCANJ1939DeviceOptions * ) pConfig;

		CPGN * pPGN  = GetDevicePGN(Addr, pConfig);

		if( !pPGN && pOptions ) {

			pPGN = pOptions->GetPGN(uNumber);
			}

		if( !pPGN ) {

			pPGN = FindPGN(uNumber);

			if( pPGN && pOptions ) {

				pOptions->GetPGNList()->AppendItem(new CPGN(pPGN));
				}
			}

		if( pPGN ) {

			if( pPGN->m_Ref == 0 ) {

				pPGN->m_Ref = Addr.m_Ref;
				}

			if( Text.IsEmpty() ) {

				UINT uOffset = FindOffset(pPGN, Addr.a.m_Offset & 0x3F);
				
				Text = GetPGString(pPGN, uOffset);

				if( Text == CString(pPGN->m_Prefix + CString("-0.")) ||
				    Text == CString("ERROR") ) {
		
					Text = pPGN->m_Prefix + CString("-?.?");
					} 
				}
			else {	
				Text = RemovePGFormat(pPGN->m_Title);
					
				Text += CString("-") + pPGN->m_Prefix;
				}

			return TRUE;
			}
		} 
	
	return FALSE;
	}

// Implementation

void CCANJ1939Driver::LoadPGNs(CItem *pConfig)
{
	if( !pConfig ) {
	
	m_pPGNs->AppendItem(New CPGN(0,		"TSC1",		"Torque/Speed Control 1\t",			m_SPNList[0],  3,0,    10));
     	m_pPGNs->AppendItem(New CPGN(256, 	"TC1",		"Transmission Control 1\t",			m_SPNList[1],  3,0,    50));
     	m_pPGNs->AppendItem(New CPGN(1024,	"XBR",		"External Brake Request\t",			m_SPNList[2],  3,0,   200));
	m_pPGNs->AppendItem(New CPGN(1792, 	"GPV4",		"General Purpose Valve Pressure\t",		m_SPNList[3],  6,0,   100));
	m_pPGNs->AppendItem(New CPGN(2048,	"AUXIO5",	"Auxiliary I/O Status 5\t",			m_SPNList[351],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(39936,	"AUXIO7",	"Auxiliary I/O Status 7\t",			m_SPNList[352],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(40192,	"AUXIO6",	"Auxiliary I/O Status 6\t",			m_SPNList[353],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(42240,	"AUXIO4",	"Auxilary I/O Status 4\t",			m_SPNList[4],  6,0,   100));
    	m_pPGNs->AppendItem(New CPGN(42496,	"AUXIO3",	"Auxilary I/O Status 3\t",			m_SPNList[5],  6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(42752,	"AUXIO2",	"Auxilary I/O Status 2\t",			m_SPNList[6],  6,0,   100));
     	m_pPGNs->AppendItem(New CPGN(43008,	"DISP1",	"Text Display\t",				m_SPNList[7],  6,0,  2000));	
     	m_pPGNs->AppendItem(New CPGN(43264,	"FLIC",		"Forward Lane Image Command\t",			m_SPNList[8],  6,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(44544,	"TPRS",		"Tire Pressure Reference Setting\t",		m_SPNList[9],  6,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(52992,	"CTL",		"Continuous Torque & Speed Limit Request\t",	m_SPNList[10], 6,0,  5000));
     	m_pPGNs->AppendItem(New CPGN(53248,	"CL",		"Cab Illumination Message\t",			m_SPNList[11], 6,0,  5000));
       	m_pPGNs->AppendItem(New CPGN(53504,	"ASC6",		"Air Suspension Control 6\t",			m_SPNList[12], 3,0,   100));	 
       	m_pPGNs->AppendItem(New CPGN(53760,	"ASC2",		"Air Suspension Control 2\t",			m_SPNList[13], 3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(54528,	"TDA",		"Time/Date Adjust\t",				m_SPNList[14], 6,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(56320,	"ATS",		"Anti Theft Status\t",				m_SPNList[15], 7,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(56576,	"ATR",		"Anti Theft Request\t",				m_SPNList[16], 7,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(56832,	"RESET",	"Reset\t",					m_SPNList[17], 7,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(57344,	"CM1",		"Cab Message 1\t",				m_SPNList[18], 6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(61440,	"ERC1",		"Electronic Retarder Controller 1\t",		m_SPNList[19], 6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(61441,	"EBC1",		"Electronic Brake Controller 1\t",		m_SPNList[20], 6,0,   100));
	m_pPGNs->AppendItem(New CPGN(61442,	"ETC1",		"Electronic Transmission Controller 1\t",	m_SPNList[21], 3,0,    10));
	m_pPGNs->AppendItem(New CPGN(61443,	"EEC2",		"Electronic Engine Controller 2\t",		m_SPNList[22], 3,0,    50));
	m_pPGNs->AppendItem(New CPGN(61444,	"EEC1",		"Electronic Engine Controller 1\t",		m_SPNList[23], 3,0,  2000));
	m_pPGNs->AppendItem(New CPGN(61445,	"ETC2",		"Electronic Transmission Controller 2\t",	m_SPNList[24], 6,0,   100));
	m_pPGNs->AppendItem(New CPGN(61446,	"EAC1",		"Electronic Axle Controller 1\t",		m_SPNList[25], 6,0,   500));
	m_pPGNs->AppendItem(New CPGN(61447,	"FLI1",		"Forward Lane Image urgent msg\t",		m_SPNList[26], 4,0,    50));
	m_pPGNs->AppendItem(New CPGN(61448,	"HPG",		"Hydraulic Pressure Governor Info\t",		m_SPNList[27], 6,0,    50));
	m_pPGNs->AppendItem(New CPGN(61449,	"VDC2",		"Vehicle Dynamic Stability Control 2\t",	m_SPNList[28], 6,0,    10));
	m_pPGNs->AppendItem(New CPGN(61450,	"EGF1",		"Engine Gas Flow Rate\t",			m_SPNList[29], 3,0,    50));
	m_pPGNs->AppendItem(New CPGN(61451,	"ESC1",		"Electronic Steering Control\t",		m_SPNList[30], 6,0,    20));
	m_pPGNs->AppendItem(New CPGN(61452,	"ETC8",		"Electronic Transmission Controller #8\t",	m_SPNList[31], 3,0,    20));
	m_pPGNs->AppendItem(New CPGN(61453,	"LOI",		"Land Leveling System Operational Info\t",	m_SPNList[32], 3,0,   100));
	m_pPGNs->AppendItem(New CPGN(61454,	"AT1IG1",	"Aftertreatment 1 Intake Gas 1\t",		m_SPNList[33], 6,0,    50));
	m_pPGNs->AppendItem(New CPGN(61455,	"AT1OG1",	"Aftertreatment 1 Outlet Gas 1\t",		m_SPNList[34], 6,0,    50));
	m_pPGNs->AppendItem(New CPGN(61456,	"AT2IG1",	"Aftertreatment 2 Intake Gas 1\t",		m_SPNList[35], 6,0,    50));
	m_pPGNs->AppendItem(New CPGN(61457,	"AT2OG1",	"Aftertreatment 2 Outlet Gas 1\t",		m_SPNList[36], 6,0,    50));
	m_pPGNs->AppendItem(New CPGN(61458,	"FWSS1",	"Fifth Wheel Smart Systems 1\t",		m_SPNList[37], 6,0,    50));
	m_pPGNs->AppendItem(New CPGN(61459,	"SSI",		"Slope Sensor Information\t",			m_SPNList[38], 3,0,    10));
	m_pPGNs->AppendItem(New CPGN(61460,	"BI",		"Blade Information\t",				m_SPNList[39], 3,0,    50));
	m_pPGNs->AppendItem(New CPGN(61461,	"RGTACRP",	"Req Generator Total AC Reactive Power\t",	m_SPNList[341],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(61462,	"CCS",		"Cylinder Combustion Status\t",			m_SPNList[40], 3,0,  5000));
	m_pPGNs->AppendItem(New CPGN(61463,	"KL1",		"Engine Knock Level #1\t",			m_SPNList[41], 3,0,  5000));
	m_pPGNs->AppendItem(New CPGN(61464,	"KL2",		"Engine Knock Level #2\t",			m_SPNList[42], 3,0,  5000));
	m_pPGNs->AppendItem(New CPGN(61465,	"KL3",		"Engine Knock Level #3\t",			m_SPNList[43], 3,0,  5000));
	m_pPGNs->AppendItem(New CPGN(61466,	"TFAC",		"Engine Throttle/Fuel Actuator Control Cmd\t",	m_SPNList[44], 4,0,    50));
	m_pPGNs->AppendItem(New CPGN(61468,	"RGAAC",	"Req Generator Average Basic AC Quantities\t",	m_SPNList[342],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(61469,	"SAS",		"Steering Angle Sensor Information\t",		m_SPNList[45], 6,0,    10));
	m_pPGNs->AppendItem(New CPGN(61470,	"GC2",		"Generator Control 2\t",			m_SPNList[345],3,0,    20));
	m_pPGNs->AppendItem(New CPGN(64840,	"O2FT2",	"Engine Exhaust Bank 2 O2 Fuel Trim\t",		m_SPNList[354],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64841,	"O2FT1",	"Engine Exhaust Bank 1 O2 Fuel Trim\t",		m_SPNList[355],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64849,	"ACCVC",	"Aftercooler Coolant Ctrl Valve Cmd\t",		m_SPNList[356],4,0,  1000));
	m_pPGNs->AppendItem(New CPGN(64850,	"ECCVC",	"Engine Coolant Ctrl Valve Cmd\t",		m_SPNList[357],4,0,  1000));
	m_pPGNs->AppendItem(New CPGN(64851,	"EAI",		"Engine Average Information\t",			m_SPNList[358],5,0,   500));
	m_pPGNs->AppendItem(New CPGN(64869,	"AT1FC2",	"Aftertreatment 1 Fuel Control 2\t",		m_SPNList[46], 6,0,   500));
	m_pPGNs->AppendItem(New CPGN(64870,	"ET4",		"Engine Temperature 4\t",			m_SPNList[47], 6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(64871,	"ZNVW",		"Zero Net Vehicle Weight Change\t",		m_SPNList[48], 6,0,  2000));
	m_pPGNs->AppendItem(New CPGN(64872,	"GCVW",		"Gross Combination Vehicle Weight\t",		m_SPNList[49], 5,0,     0));
	m_pPGNs->AppendItem(New CPGN(64873,	"AGCW",		"Axle Group Calibration Weights\t",		m_SPNList[50], 7,0,     0));
	m_pPGNs->AppendItem(New CPGN(64874,	"AGW",		"Axle Group Weight\t",				m_SPNList[51], 5,0,     0));
	m_pPGNs->AppendItem(New CPGN(64875,	"AAGW",		"Available Axle Group Weights\t",		m_SPNList[52], 6,0,  2000));
	m_pPGNs->AppendItem(New CPGN(64876,	"AT2AC2",	"Aftertreatment 2 Air Control 2\t",		m_SPNList[53], 6,0,   500));
	m_pPGNs->AppendItem(New CPGN(64877,     "AT1AC2",	"Aftertreatment 1 Air Control 2\t",		m_SPNList[54], 6,0,   500));
	m_pPGNs->AppendItem(New CPGN(64878,	"SCR1",		"Catalyst Use Information\t",			m_SPNList[55], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64879,	"EEC8",		"Electronic Engine Controller 8\t",		m_SPNList[56], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64880,	"DRC",		"Door ramp control\t",				m_SPNList[57], 6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(64881,	"BSA",		"Brake actuator stroke status\t",		m_SPNList[58], 6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(64882,	"ESV6",		"Engine Spark Voltage 6\t",			m_SPNList[59], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64883,	"ESV5",		"Engine Spark Voltage 5\t",			m_SPNList[60], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64884,	"ESV4",		"Engine Spark Voltage 4\t",			m_SPNList[61], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64885,	"ESV3",		"Engine Spark Voltage 3\t",			m_SPNList[62], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64886,	"ESV2",		"Engine Spark Voltage 2\t",			m_SPNList[63], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64887,	"ESV1",		"Engine Spark Voltage 1\t",			m_SPNList[64], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64888,	"AT2TI",	"Aftertreatment 2 Trip Information\t",		m_SPNList[65], 6,0,     0));	
       	m_pPGNs->AppendItem(New CPGN(64889,	"AT1TI",	"Aftertreatment 1 Trip Information\t",		m_SPNList[66], 6,0,     0));	 
       	m_pPGNs->AppendItem(New CPGN(64890,	"AT2S",		"Aftertreatment 2 Service\t",			m_SPNList[67], 6,0,     0));
    	m_pPGNs->AppendItem(New CPGN(64891,	"AT1S",		"Aftertreatment 1 Service\t",			m_SPNList[68], 6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64892,	"PTC1",		"Particulate Trap Control 1\t",			m_SPNList[69], 6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64894,	"AFSS",		"Adaptive Front Lighting System Status\t",	m_SPNList[70], 6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64895,	"EC2",		"Engine Configuration 2\t",			m_SPNList[71], 6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64897,	"EGRBV",	"EGR Cooler Bypass\t",				m_SPNList[72], 5,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64899,	"TCI",		"Transfer Case Information\t",			m_SPNList[73], 6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64900,	"EFLP9",	"Engine Fluid Level/Pressure 9\t",		m_SPNList[74], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64901,	"EFLP8",	"Engine Fluid Level/Pressure 8\t",		m_SPNList[75], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64902,	"EFLP7",	"Engine Fluid Level/Pressure 7\t",		m_SPNList[76], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64903,	"EFLP6",	"Engine Fluid Level/Pressure 6\t",		m_SPNList[77], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64904,	"EFLP5",	"Engine Fluid Level/Pressure 5\t",		m_SPNList[78], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64905,	"VDS2",		"Vehicle Direction/Speed 2\t",			m_SPNList[79], 6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64906,	"J2012",	"SAE J2012 DTC Display\t",			m_SPNList[80], 7,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64907,	"AT2GP",	"Aftertreatment 2 Gas Parameters\t",		m_SPNList[81], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64908,	"AT1GP",	"Aftertreatment 1 Gas Parameters\t",		m_SPNList[82], 6,0,   500));
	m_pPGNs->AppendItem(New CPGN(64909,	"UTACER",	"Utility Total AC Reactive Energy\t",		m_SPNList[346],6,0,   250));
	m_pPGNs->AppendItem(New CPGN(64910,	"GTACER",	"Generator Total AC Reactive Energy\t",		m_SPNList[347],6,0,   250));
	m_pPGNs->AppendItem(New CPGN(64911,	"GTACPP",	"Generator Total AC Percent Power\t",		m_SPNList[348],6,0,   250));
	m_pPGNs->AppendItem(New CPGN(64912,	"AETC",		"Advertised Engine Torque Curve\t",		m_SPNList[83], 6,0,     0));
	m_pPGNs->AppendItem(New CPGN(64913,	"ACS",		"AC Switching Device Status\t",			m_SPNList[349],6,0,   250));
	m_pPGNs->AppendItem(New CPGN(64914,	"EOI",		"Engine Operating Information\t",		m_SPNList[84], 3,0,   250));
 	m_pPGNs->AppendItem(New CPGN(64915,	"GC1",		"Generator Control 1\t",			m_SPNList[350],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(64916,	"EEC7",		"Electronic Engine Controller 7\t",		m_SPNList[85], 6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64917,	"TRF2",		"Transmission Fluids 2\t",			m_SPNList[86], 6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64920,	"AT1HI",	"Aftertreatment 1 Historical Information\t",	m_SPNList[87], 6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64921,	"AT2HI",	"Aftertreatment 2 Historical Information\t",	m_SPNList[88], 6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64923,	"CRI1",		"Catalyst Reagent Information\t",		m_SPNList[89], 6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64924,	"SEP2",		"Sensor Electrical Power #2\t",			m_SPNList[90], 6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64925,	"SEP1",		"Sensor Electrical Power #1\t",			m_SPNList[91], 6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64926,	"AT2AC1",	"Aftertreatment 2 Air Control 1\t",		m_SPNList[92], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64927,	"AT1AC1",	"Aftertreatment 1 Air Control 1\t",		m_SPNList[93], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64928,	"AT2FC",	"Aftertreatment 2 Fuel Control 1\t",		m_SPNList[94], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64929,	"AT1FC",	"Aftertreatment 1 Fuel Control 1\t",		m_SPNList[95], 6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64930,	"GFI3",		"Fuel Information 3 (Gaseous)\t",		m_SPNList[96], 4,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64931,	"EEC6",		"Electronic Engine Controller 6\t",		m_SPNList[97], 4,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64932,	"PTODE",	"PTO Drive Engagement\t",			m_SPNList[98], 6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64933,	"DC2",		"Door Control 2\t",				m_SPNList[99], 6,0,   100));
	m_pPGNs->AppendItem(New CPGN(64934,	"VREP",		"Voltage Regulator Excitation Status\t",	m_SPNList[343],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(64935,	"VROM",		"Voltage Regulator Operating Mode\t",		m_SPNList[344],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(64936,	"WCM2",		"Wireless Communications Message 2\t",		m_SPNList[100],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64937,	"WCM1",		"Wireless Communications Message 1\t",		m_SPNList[101],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64938,	"EFLP4",	"Engine Fluid Level/Pressure 4\t",		m_SPNList[102],6,0,   500));
     	m_pPGNs->AppendItem(New CPGN(64942,	"FWSS2",	"Fifth Wheel Smart Systems 2\t",		m_SPNList[103],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64943,	"AT2IMG",	"Aftertreatment 2 Intermediate Gas\t",		m_SPNList[104],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64944,	"AT2OG2",	"Aftertreatment 2 Outlet Gas 2\t",		m_SPNList[105],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64945,	"AT2IG2",	"Aftertreatment 2 Intake Gas 2\t",		m_SPNList[106],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64946,	"AT1IMG",	"Aftertreatment 1 Intermediate Gas\t",		m_SPNList[107],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64947,	"AT1OG2",	"Aftertreatment 1 Outlet Gas 2\t",		m_SPNList[108],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64948,	"AT1IG2",	"Aftertreatment 1 Intake Gas 2\t",		m_SPNList[109],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64953,	"TPRI",		"Tire Pressure Reference Information\t",	m_SPNList[110],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64954,	"TR6",		"Farebox Status\t",				m_SPNList[111],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64955,	"TR5",		"Farebox Point of Sale\t",			m_SPNList[112],6,0,     0));
     	m_pPGNs->AppendItem(New CPGN(64956,	"TR4",		"Farebox Service Detail\t",			m_SPNList[113],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64957,	"TR3",		"Signal Preemption\t",				m_SPNList[114],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64958,	"TR1",		"Transit Route\t",				m_SPNList[115],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64959,	"TR2",		"Transit Milepost\t",				m_SPNList[116],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64960,	"TR7",		"Passenger Counter\t",				m_SPNList[117],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64961,	"EFLP3",	"Engine Fluid Level/Pressure 3\t",		m_SPNList[118],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64964,	"EBC5",		"Electronic Brake Controller 5\t",		m_SPNList[119],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64965,	"ECUID",	"ECU Identification Information\t",		m_SPNList[120],6,0,     0));
   	m_pPGNs->AppendItem(New CPGN(64966,	"CSA",		"Cold Start Aids\t",				m_SPNList[121],6,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(64967,	"OHCSS",	"Off Highway Engine Ctrl Selection States\t",	m_SPNList[122],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64968,	"ISCS",		"Operator Primary Inter Speed Ctrl state\t",	m_SPNList[123],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64969,	"CMI",		"Electronic Control Module Information\t",	m_SPNList[124],7,0,  2000));
       	m_pPGNs->AppendItem(New CPGN(64970,	"ISC",		"Intermediate Speed Control\t",			m_SPNList[125],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64971,	"OHECS",	"Off Highway Engine Control Selection\t",	m_SPNList[126],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64972,	"OEL",		"Operators External Light Ctrls Message\t",	m_SPNList[127],3,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64973,	"OWW",		"Operator Wiper & Washer Ctrls Message\t",	m_SPNList[128],6,0,   200));
       	m_pPGNs->AppendItem(New CPGN(64976,	"IC2",		"Inlet/Exhaust Conditions 2\t",			m_SPNList[129],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64977,	"FMS",		"FMS standard Interface Id/Capabilities\t",	m_SPNList[130],7,0, 10000));
       	m_pPGNs->AppendItem(New CPGN(64978,	"EP",		"ECU Performance\t",				m_SPNList[131],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64979,	"TCI6",		"Turbocharger Information 6\t",			m_SPNList[132],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64980,	"CM3",		"Cab Message 3\t",				m_SPNList[133],6,0, 10000));
       	m_pPGNs->AppendItem(New CPGN(64981,	"EEC5",		"Electronic Engine Controller 5\t",		m_SPNList[134],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(64982,	"BJM1",		"Basic Joystick Message 1\t",			m_SPNList[135],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64983,	"EJM1",		"Extended Joystick Message 1\t",		m_SPNList[136],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64984,	"BJM2",		"Basic Joystick Message 2\t",			m_SPNList[137],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64985,	"EJM2",		"Extended Joystick Message 2\t",		m_SPNList[138],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64986,	"BJM3",		"Basic Joystick Message 3\t",			m_SPNList[139],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64987,	"EJM3",		"Extended Joystick Message 3\t",		m_SPNList[140],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(64988,	"MCI",		"Marine Control Information\t",			m_SPNList[141],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64991,	"FWD",		"Front Wheel Drive Status\t",			m_SPNList[142],7,0,   500));
     	m_pPGNs->AppendItem(New CPGN(64992,	"AMB2",		"Ambient Conditions 2\t",			m_SPNList[143],6,0,  1000));
     	m_pPGNs->AppendItem(New CPGN(64993,	"CACI",		"Cab A/C Climate System Information\t",		m_SPNList[144],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64994,	"SPR",		"Supply Pressure Demand\t",			m_SPNList[145],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64995,	"EOAC",		"Equipment Operation and Control\t",		m_SPNList[146],6,0,   250));
       	m_pPGNs->AppendItem(New CPGN(64996,	"EPD",		"Equipment Performance Data\t",			m_SPNList[147],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(64997,	"MVS",		"Maximum Vehicle Speed Limit Status\t",		m_SPNList[148],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(64998,	"HBS",		"Hydraulic Braking System\t",			m_SPNList[149],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(64999,	"BUSC",		"Bus #1/Utility Sync Check Status\t",		m_SPNList[309],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65000,	"BGSC",		"Bus #1/Generator Sync Check Status\t",		m_SPNList[310],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65001,	"BPCAC",	"Bus #1 Phase C Basic AC Quantities\t",		m_SPNList[311],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65002,	"BPBAC",	"Bus #1 Phase B Basic AC Quantities\t",		m_SPNList[312],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65003,	"BPAAC",	"Bus #1 Phase A Basic AC Quantities\t",		m_SPNList[313],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65004,	"BAAC",		"Bus #1 Average Basic AC Quantities\t",		m_SPNList[314],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65005,	"UTACE",	"Utility Total AC Energy\t",			m_SPNList[315],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65006,	"UPCACR",	"Utility Phase C AC Reactive Power\t",		m_SPNList[316],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65007,	"UPCACP",	"Utility Phase C AC Power\t",			m_SPNList[317],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65008,	"UPCAC",	"Utility Phase C AC Basic Quantities\t",	m_SPNList[318],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65009,	"UPBACR",	"Utility Phase B AC Reactive Power\t",		m_SPNList[319],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65010,	"UPBACP",	"Utility Phase B AC Power\t",			m_SPNList[320],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65011,	"UPBAC",	"Utility Phase B AC Basic Quantities\t",	m_SPNList[321],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65012,	"UPACCR",	"Utility Phase A AC Reactive Power\t",		m_SPNList[322],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65013,	"UPAACP",	"Utility Phase A AC Power\t",			m_SPNList[323],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65014,	"UPAAC",	"Utility Phase A Basic AC Quantities\t",	m_SPNList[324],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65015,	"UTACR",	"Utility Total AC Reactive Power\t",		m_SPNList[325],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65016,	"UTACP",	"Utility Total AC Power\t",			m_SPNList[326],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65017,	"UAAC",		"Utility Average Basic AC Quantities\t",	m_SPNList[327],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65018,	"GTACE",	"Generator Total AC Energy\t",			m_SPNList[328],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65019,	"GPCACR",	"Generator Phase C AC Reactive Power\t",	m_SPNList[329],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65020,	"GPCACP",	"Generator Phase C AC Power\t",			m_SPNList[330],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65021,	"GPCAC",	"Generator Phase C Basic AC Quantities\t",	m_SPNList[331],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65022,	"GPBACRP",	"Generator Phase B AC Reactive Power\t",	m_SPNList[332],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65023,	"GPBACP",	"Generator Phase B AC Power\t",			m_SPNList[333],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65024,	"GPBAC",	"Generator Phase B Basic AC Quantities\t",	m_SPNList[334],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65025,	"GPAACR",	"Generator Phase A AC Reactive Power\t",	m_SPNList[335],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65026,	"GPAACP",	"Generator Phase A AC Power\t",			m_SPNList[336],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65027,	"GPAAC",	"Generator Phase A Basic AC Quantities\t",	m_SPNList[337],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65028,	"GTACR",	"Generator Total AC Reactive Power\t",		m_SPNList[338],3,0,   100));	
	m_pPGNs->AppendItem(New CPGN(65029,	"GTACP",	"Generator Total AC Power\t",			m_SPNList[339],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65030,	"GAAC",		"Generator Average Basic AC Quantities\t",	m_SPNList[340],3,0,   100));
	m_pPGNs->AppendItem(New CPGN(65031,	"ET",		"Exhaust Temperature\t",			m_SPNList[150],6,0,   500));
     	m_pPGNs->AppendItem(New CPGN(65088,	"LD",		"Lighting Data\t",				m_SPNList[151],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65089,	"LC",		"Lighting Command\t",				m_SPNList[152],3,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65098,	"ETC7",		"Electronic Transmission Controller 7\t",	m_SPNList[153],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65099,	"TCFG2",	"Transmission Configuration 2\t",		m_SPNList[154],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65100,	"ML",		"Military Lighting Command\t",			m_SPNList[155],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(65101,	"TAVG",		"Total Averaged Information\t",			m_SPNList[156],7,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65102,	"DC1",		"Door Control 1\t",				m_SPNList[157],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65103,	"VDC1",		"Vehicle Dynamic Stability Control 1\t",	m_SPNList[158],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65104,	"BT1",		"Battery Temperature\t",			m_SPNList[159],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(65105,	"ACC2",		"Adaptive Cruise Control Operator Input\t",	m_SPNList[160],6,0,   250));
       	m_pPGNs->AppendItem(New CPGN(65106,	"VEP3",		"Vehicle Electrical Power #3\t",		m_SPNList[161],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(65107,	"RTC1",		"Retarder Continuous Torque & Speed Limit\t",	m_SPNList[162],6,0,  5000));
       	m_pPGNs->AppendItem(New CPGN(65108,	"ECT1",		"Engine Continuous Torque & Speed Limit\t",	m_SPNList[163],6,0,  5000));
       	m_pPGNs->AppendItem(New CPGN(65109,	"GFD",		"Gaseous Fuel Properties\t",			m_SPNList[164],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65110,	"TI1",		"Tank Information 1\t",				m_SPNList[165],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(65111,	"ASC5",		"Air Suspension Control 5\t",			m_SPNList[166],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65112,	"ASC4",		"Air Suspension Control 4\t",			m_SPNList[167],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65113,	"ASC3",		"Air Suspension Control 3\t",			m_SPNList[168],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65114,	"ASC1",		"Air Suspension Control 1\t",			m_SPNList[169],3,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65115,	"FLI2",		"Forward Lane Image\t",				m_SPNList[170],6,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65126,	"BM",		"Battery Main Switch Information\t",		m_SPNList[171],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(65127,	"CCC",		"Climate Control Configuration\t",		m_SPNList[172],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65128,	"VF",		"Vehicle Fluids\t",				m_SPNList[173],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(65129,	"ET3",		"Engine Temperature 3\t",			m_SPNList[174],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(65130,	"EFS",		"Engine Fuel/lube systems\t",			m_SPNList[175],6,0,   500));
       	m_pPGNs->AppendItem(New CPGN(65131,	"DI",		"Driver's Identification\t",			m_SPNList[176],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65132,	"TCO1",		"Tachograph\t",					m_SPNList[177],3,0,    50));
       	m_pPGNs->AppendItem(New CPGN(65133,	"HTR",		"Heater Information\t",				m_SPNList[178],6,0,  1000));
       	m_pPGNs->AppendItem(New CPGN(65134,	"HRW",		"High Resolution Wheel Speed\t",		m_SPNList[179],2,0,    20));
	m_pPGNs->AppendItem(New CPGN(65135,	"ACC1",		"Adaptive Cruise Control\t",			m_SPNList[180],4,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65136,	"CVW",		"Combination Vehicle Weight\t",			m_SPNList[181],6,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65137,	"LTP",		"Laser Tracer Position\t",			m_SPNList[182],3,0,    50));
       	m_pPGNs->AppendItem(New CPGN(65138,	"LBC",		"Laser Leveling System Blade Control\t",	m_SPNList[183],3,0,    50));
       	m_pPGNs->AppendItem(New CPGN(65139,	"LMP",		"Laser Receiver Mast Position\t",		m_SPNList[184],3,0,    50));
       	m_pPGNs->AppendItem(New CPGN(65140,	"LSP",		"Modify Leveling System Control Set Point\t",	m_SPNList[185],3,0,    50));
       	m_pPGNs->AppendItem(New CPGN(65141,	"LVD",		"Laser Leveling System Vertical Deviation\t",	m_SPNList[186],3,0,    50));
       	m_pPGNs->AppendItem(New CPGN(65142,	"LVDD",		"Laser Leveling System Vertical Pos Dsp Data\t",m_SPNList[187],4,0,   100));
       	m_pPGNs->AppendItem(New CPGN(65143,	"AP",		"Auxiliary Pressures\t",			m_SPNList[188],7,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65144,	"TP1",		"Tire Pressure Control Unit Mode & Status\t",	m_SPNList[189],7,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65145,	"TP2",		"Tire Pressure Control Unit Target Pressures\t",m_SPNList[190],7,0,     0));
       	m_pPGNs->AppendItem(New CPGN(65146,	"TP3",		"Tire Pressure Ctrl Unit Current Pressures\t",	m_SPNList[191],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65147,	"CT1",		"Combustion Time 1\t",				m_SPNList[192],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65148,	"CT2",		"Combustion Time 2\t",				m_SPNList[193],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65149,	"CT3",		"Combustion Time 3\t",				m_SPNList[194],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65150,	"CT4",		"Combustion Time 4\t",				m_SPNList[195],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65151,	"CT5",		"Combustion Time 5\t",				m_SPNList[196],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65152,	"CT6",		"Combustion Time 6\t",				m_SPNList[197],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65153,	"GFI2",		"Fuel Information 2 (Gaseous)\t",		m_SPNList[198],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65154,	"IT1",		"Ignition Timing 1\t",				m_SPNList[199],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65155,	"IT2",		"Ignition Timing 2\t",				m_SPNList[200],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65156,	"IT3",		"Ignition Timing 3\t",				m_SPNList[201],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65157,	"IT4",		"Ignition Timing 4\t",				m_SPNList[202],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65158,	"IT5",		"Ignition Timing 5\t",				m_SPNList[203],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65159,	"IT6",		"Ignition Timing 6\t",				m_SPNList[204],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65160,	"ISO1",		"Ignition Transformer Secondary Output 1\t",	m_SPNList[205],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65161,	"ISO2",		"Ignition Transformer Secondary Output 2\t",	m_SPNList[206],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65162,	"ISO3",		"Ignition Transformer Secondary Output 3\t",	m_SPNList[207],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65163,	"GFP",		"Gaseous Fuel Pressure\t",			m_SPNList[208],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65164,	"AAI",		"Auxiliary Analog Information\t",		m_SPNList[209],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65165,	"VEP2",		"Vehicle Electrical Power #2\t",		m_SPNList[210],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65166,	"S2",		"Service 2\t",					m_SPNList[211],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65167,	"SP2",		"Supply Pressure 2\t",				m_SPNList[212],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65168,	"ETH",		"Engine Torque History\t",			m_SPNList[213],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65169,	"FL",		"Fuel Leakage\t",				m_SPNList[214],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65170,	"EI",		"Engine Information\t",				m_SPNList[215],7,0,   100));
	m_pPGNs->AppendItem(New CPGN(65171,	"EES",		"Engine Electrical System/Module Info\t",	m_SPNList[216],7,0,   100));
	m_pPGNs->AppendItem(New CPGN(65172,	"EAC",		"Engine Auxiliary Coolant\t",			m_SPNList[217],6,0,   500));
	m_pPGNs->AppendItem(New CPGN(65173,	"RBI",		"Rebuild Information\t",			m_SPNList[218],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65174,	"TCW",		"Turbocharger Wastegate\t",			m_SPNList[219],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(65175,	"TCI5",		"Turbocharger Information 5\t",			m_SPNList[220],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65176,	"TCI4",		"Turbocharger Information 4\t",			m_SPNList[221],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65177,	"TCI3",		"Turbocharger Information 3\t",			m_SPNList[222],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65178,	"TCI2",		"Turbocharger Information 2\t",			m_SPNList[223],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65179,	"TCI1",		"Turbocharger Information 1\t",			m_SPNList[224],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65180,	"MBT3",		"Main Bearing Temperature 3\t",			m_SPNList[225],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65181,	"MBT2",		"Main Bearing Temperature 2\t",			m_SPNList[226],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65182,	"MBT1",		"Main Bearing Temperature 1\t",			m_SPNList[227],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65183,	"EPT5",		"Exhaust Port Temperature 5\t",			m_SPNList[228],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65184,	"EPT4",		"Exhaust Port Temperature 4\t",			m_SPNList[229],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65185,	"EPT3",		"Exhaust Port Temperature 3\t",			m_SPNList[230],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65186,	"EPT2",		"Exhaust Port Temperature 2\t",			m_SPNList[231],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65187,	"EPT1",		"Exhaust Port Temperature 1\t",			m_SPNList[232],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65188,	"ET2",		"Engine Temperature 2\t",			m_SPNList[233],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65189,	"IMT2",		"Intake Manifold Information 2\t",		m_SPNList[234],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65190,	"IMT1",		"Intake Manifold Information 1\t",		m_SPNList[235],6,0,   500));
	m_pPGNs->AppendItem(New CPGN(65191,	"AT",		"Alternator Temperature\t",			m_SPNList[236],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65192,	"AC",		"Articulation Control\t",			m_SPNList[237],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65193,	"EO1",		"Exhaust Oxygen 1\t",				m_SPNList[238],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65194,	"AF2",		"Alternate Fuel 2\t",				m_SPNList[239],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65195,	"ETC6",		"Electronic Transmission Controller 6\t",	m_SPNList[240],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65196,	"EBC4",		"Wheel Brake Lining Remaining Info\t",		m_SPNList[241],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65197,	"EBC3",		"Wheel Application Pressure High Range Info\t",	m_SPNList[242],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(65198,	"AIR1",		"Air Supply Pressure\t",			m_SPNList[243],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65199,	"GFC",		"Fuel Consumption (Gaseous)\t",			m_SPNList[244],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65200,	"TTI2",		"Trip Time Information 2\t",			m_SPNList[245],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65201,	"EH",		"ECU History\t",				m_SPNList[246],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65202,	"GFI1",		"Fuel Information 1 (Gaseous)\t",		m_SPNList[247],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65203,	"LFI",		"Fuel Information (Liquid)\t",			m_SPNList[248],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65204,	"TTI1",		"Trip Time Information 1\t",			m_SPNList[249],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65205,	"TSI",		"Trip Shutdown Information\t",			m_SPNList[250],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65206,	"TVI",		"Trip Vehicle Speed/Cruise Distance Info\t",	m_SPNList[251],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65207,	"LF",		"Engine Speed/Load Factor Information\t",	m_SPNList[252],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65208,	"GTFI",		"Trip Fuel Information (Gaseous)\t",		m_SPNList[253],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65209,	"LTFI",		"Trip Fuel Information (Liquid)\t",		m_SPNList[254],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65210,	"TDI",		"Trip Distance Information\t",			m_SPNList[255],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65211,	"TFI",		"Trip Fan Information\t",			m_SPNList[256],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65212,	"CBI",		"Compression/Service Brake Information\t",	m_SPNList[257],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65213,	"FD",		"Fan Drive\t",					m_SPNList[258],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65214,	"EEC4",		"Electronic Engine Controller 4\t",		m_SPNList[259],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65215,	"EBC2",		"Wheel Speed Information\t",			m_SPNList[260],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(65216,	"SERV",		"Service Information\t",			m_SPNList[261],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65217,	"VDHR",		"High Resolution Vehicle Distance\t",		m_SPNList[262],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65218,	"ERC2",		"Electronic Retarder Controller 2\t",		m_SPNList[263],7,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65219,	"ETC5",		"Electronic Transmission Controller 5\t",	m_SPNList[264],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65221,	"ETC4",		"Electronic Transmission Controller 4\t",	m_SPNList[265],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65223,	"ETC3",		"Electronic Transmission Controller 3\t",	m_SPNList[266],7,0,     0));
	m_pPGNs->AppendItem(New CPGN(65226,	"DM1",		"Active Diagnostic Trouble Codes\t",		m_SPNList[307],6,0,  1000));	
	m_pPGNs->AppendItem(New CPGN(65227,	"DM2",		"Previous Diagnostic Trouble Codes\t",		m_SPNList[308],6,0,     0));	
	m_pPGNs->AppendItem(New CPGN(65228,	"DM3",		"Clear/Reset Previously Active DTCs\t",		m_SPNList[360],6,0,     0)); 
	m_pPGNs->AppendItem(New CPGN(65229,	"DM4",		"Freeze Frame Parameters\t",			m_SPNList[362],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65230,	"DM5",		"Diagnostic Readiness 1\t",			m_SPNList[359],6,1,  1000));		
	m_pPGNs->AppendItem(New CPGN(65235,	"DM11",		"Clear/Reset For Active DTCs\t",		m_SPNList[361],6,0,     0)); 
	m_pPGNs->AppendItem(New CPGN(65237,	"AS",		"Alternator Information\t",			m_SPNList[267],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65241,	"AUXIO1",	"Auxiliary Input/Output Status 1\t",		m_SPNList[268],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65242,	"SOFT",		"Software Identification\t",			m_SPNList[269],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65243,	"EFLP2",	"Engine Fluid Level/Pressure 2\t",		m_SPNList[270],6,0,   500));
	m_pPGNs->AppendItem(New CPGN(65244,	"IO",		"Idle Operation\t",				m_SPNList[271],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65245,	"TC",		"Turbocharger\t",				m_SPNList[272],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65246,	"AIR2",		"Air Start Pressure\t",				m_SPNList[273],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65247,	"EEC3",		"Electronic Engine Controller 3\t",		m_SPNList[274],6,0,   250));
	m_pPGNs->AppendItem(New CPGN(65248,	"VD",		"Vehicle Distance\t",				m_SPNList[275],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(65249,	"RC",		"Retarder Configuration\t",			m_SPNList[276],6,0,  5000));
	m_pPGNs->AppendItem(New CPGN(65250,	"TCFG",		"Transmission Configuration\t",			m_SPNList[277],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65251,	"EC1",		"Engine Configuration 1\t",			m_SPNList[278],6,0,  5000));
	m_pPGNs->AppendItem(New CPGN(65252,	"SHUTDN",	"Shutdown\t",					m_SPNList[279],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65253,	"HOURS",	"Engine Hours, Revolutions\t",			m_SPNList[280],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65254,	"TD",		"Time/Date\t",					m_SPNList[281],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65255,	"VH",		"Vehicle Hours\t",				m_SPNList[282],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65256,	"VDS",		"Vehicle Direction/Speed\t",			m_SPNList[283],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65257,	"LFC",		"Fuel Consumption (Liquid)\t",			m_SPNList[284],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65258,	"VW",		"Vehicle Weight\t",				m_SPNList[285],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65259,	"CI",		"Component Identification\t",			m_SPNList[286],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65260,	"VI",		"Vehicle Identification\t",			m_SPNList[287],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65261,	"CCSS",		"Cruise Control/Vehicle Speed Setup\t",		m_SPNList[288],6,0,     0));
	m_pPGNs->AppendItem(New CPGN(65262,	"ET1",		"Engine Temperature 1\t",			m_SPNList[289],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65263,	"EFLP1",	"Engine Fluid Level/Pressure 1\t",		m_SPNList[290],6,0,   500));
	m_pPGNs->AppendItem(New CPGN(65264,	"PTO",		"Power Takeoff Information\t",			m_SPNList[291],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(65265,	"CCVS",		"Cruise Control/Vehicle Speed\t",		m_SPNList[292],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(65266,	"LFE",		"Fuel Economy (Liquid)\t",			m_SPNList[293],6,0,   100));
	m_pPGNs->AppendItem(New CPGN(65267,	"VP",		"Vehicle Position\t",				m_SPNList[294],6,0,  5000));
	m_pPGNs->AppendItem(New CPGN(65268,	"TIRE",		"Tire Condition\t",				m_SPNList[295],6,0, 10000));
	m_pPGNs->AppendItem(New CPGN(65269,	"AMB",		"Ambient Conditions\t",				m_SPNList[296],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65270,	"IC1",		"Inlet/Exhaust Conditions 1\t",			m_SPNList[297],6,0,   500));
	m_pPGNs->AppendItem(New CPGN(65271,	"VEP1",		"Vehicle Electrical Power 1\t",			m_SPNList[298],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65272,	"TRF1",		"Transmission Fluids 1\t",			m_SPNList[299],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65273,	"AI",		"Axle Information\t",				m_SPNList[300],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65274,	"B",		"Brakes\t",					m_SPNList[301],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65275,	"RF",		"Retarder fluids\t",				m_SPNList[302],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65276,	"DD",		"Dash Display\t",				m_SPNList[303],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65277,	"A1",		"Alternate Fuel 1\t",				m_SPNList[304],6,0,   500));
	m_pPGNs->AppendItem(New CPGN(65278,	"AWPP",		"Auxiliary Water Pump Pressure\t",		m_SPNList[305],6,0,  1000));
	m_pPGNs->AppendItem(New CPGN(65279,	"WFI",		"Water In Fuel Indicator\t",			m_SPNList[306],6,0, 10000));
	}

	CCANJ1939DriverOptions * pOptions = (CCANJ1939DriverOptions *) pConfig;
  	
	if( pOptions ) {

		INDEX Index = pOptions->m_pPGNList->GetHead();

		while( !pOptions->m_pPGNList->Failed(Index) ) {

			UINT uNum = pOptions->m_pPGNList->GetItem(Index)->m_Number;

			UINT uDir = pOptions->m_pPGNList->GetItem(Index)->m_Direction;

			BOOL fNew = TRUE;

			for( INDEX i = m_pPGNs->GetHead(); !m_pPGNs->Failed(i); m_pPGNs->GetNext(i) ) {

				if( m_pPGNs->GetItem(i)->IsThisPGN(uNum, uDir) || (uDir && m_pPGNs->GetItem(i)->IsThisPGN(uNum, 0))) {

					CPGN * pOpt = pOptions->m_pPGNList->GetItem(Index);

					CPGN * pPGN = m_pPGNs->GetItem(i);

					m_pPGNs->InsertItem(New CPGN(pOpt), pPGN);

					m_pPGNs->RemoveItem(pPGN);

					delete pPGN;

					fNew = FALSE;
					
					break;
					}
				}  

			if( fNew ) {

				m_pPGNs->AppendItem(New CPGN(pOptions->m_pPGNList->GetItem(Index)));
				}

			pOptions->m_pPGNList->GetNext(Index);
			}
		} 
	}

// Helpers

BOOL  CCANJ1939Driver::IsDead(CPGN * pPGN, UINT uOffset)
{
	if( uOffset < SPN_MAX && uOffset < pPGN->GetSPNs()->GetItemCount() ) {
	
		return ( pPGN->GetSPNs()->GetItem(uOffset)->m_Number == UNSPECIFIED );
		}

	return FALSE;
	}

UINT  CCANJ1939Driver::FindOffset(CPGN * pPGN, UINT uOffset)
{
	UINT Offset = uOffset;
	
	for( UINT u = 0, i = 0; u <= uOffset + i; u++ ) {
			       
		if ( IsDead(pPGN, u) ) {

			Offset++;
	
			i++;
			}
		}

	return Offset;
	}

BOOL CCANJ1939Driver::CheckRebuild(CCANJ1939DeviceOptions * pOptions, CPGN * pPGN)
{
	if( m_fRebuild && pOptions && pPGN ) {

		pPGN->m_Ref	 = 0;

		CPGNList * pList = pOptions->GetPGNList();

		if( pList ) {

			UINT uPos = pList->FindItemPos(pPGN);

			if( uPos < NOTHING ) {

				CPGN * pDev = pList->GetItem(uPos);

				for( INDEX i = m_pPGNs->GetHead(); !m_pPGNs->Failed(i); m_pPGNs->GetNext(i) ) {

					CPGN * pDrv = m_pPGNs->GetItem(i);

					if( pDrv->IsThisPGN(pDev->m_Number, pDev->m_Direction) ) {

						pList->GetItem(uPos)->m_fMark = TRUE;

						return TRUE;
						}
					}

				pList->RemoveItem(pPGN);

				delete pPGN;

				return FALSE;
				}
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver Options	UI Page
//


// Runtime Class

AfxImplementRuntimeClass(CCANJ1939DriverOptionsUIPage , CUIPage);

// Constructor

CCANJ1939DriverOptionsUIPage::CCANJ1939DriverOptionsUIPage(CCANJ1939DriverOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CCANJ1939DriverOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Driver Settings", 1);

	pView->AddUI(pItem, L"root", L"Source");

	pView->AddUI(pItem, L"root", L"Source2");

	pView->EndGroup(TRUE);

	pView->StartGroup(L"Manage User Defined PGNs", 1);

	pView->AddButton(L"Manage", L"Manage");

	pView->EndGroup(TRUE);

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANJ1939DriverOptions, CUIItem);

// Constructor

CCANJ1939DriverOptions::CCANJ1939DriverOptions(void)
{
	m_Source	= 2;	 
					    
	m_pPGNList	= New CPGNList;		  

	m_List		= 0;

	m_Priority	= 6;

	m_RepRate	= 1000;
			
	m_SPNs		= 0;

	m_SPN		= 0;

	m_Size1		= 1 | BITS;

	m_Size2		= 1 | BITS;

	m_Size3		= 1 | BITS;

	m_Size4		= 1 | BITS;

	m_Size5		= 1 | BITS;

	m_Size6		= 1 | BITS;

	m_Size7		= 1 | BITS;

	m_Size8		= 1 | BITS;

	m_Diag		= 0;	

	m_Enhanced      = 0;

	m_Direction     = 0;

	m_Source2       = 0;
	}

BOOL CCANJ1939DriverOptions::Import(CPropValue *pValue)
{
	if( pValue ) {

		ImportNumber(pValue, L"Source");
		
		ImportList(pValue->GetChild(L"PGNList"));

		ImportNumber(pValue, L"List");

		ImportNumber(pValue, L"Priority");

		ImportNumber(pValue, L"RepRate");

		ImportNumber(pValue, L"SPNs");

		ImportNumber(pValue, L"SPN");

		ImportNumber(pValue, L"Size1");

		ImportNumber(pValue, L"Size2");

		ImportNumber(pValue, L"Size3");

		ImportNumber(pValue, L"Size4");

		ImportNumber(pValue, L"Size5");

		ImportNumber(pValue, L"Size6");

		ImportNumber(pValue, L"Size7");

		ImportNumber(pValue, L"Size8");	

		ImportNumber(pValue, L"Diag");

		ImportNumber(pValue, L"Enhanced");

		ImportNumber(pValue, L"Direction");
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANJ1939DriverOptions::ImportList(CPropValue *pValue)
{
	if( pValue ) {

		for( INDEX x = pValue->m_Props.GetHead(); !pValue->m_Props.Failed(x); pValue->m_Props.GetNext(x) ) {

			CPropValue *pEntry = pValue->m_Props.GetData(x);

			if( pEntry ) {

				CPGN * pPGN = New CPGN();

				pPGN->m_Number   = pEntry->GetChild(L"Number")->GetNumber();

				pPGN->m_Priority = pEntry->GetChild(L"Priority")->GetNumber();

				pPGN->m_SendReq  = pEntry->GetChild(L"SendReq")->GetNumber();

				CPropValue * pSub = pEntry->GetChild(L"SPNs");

				if( pSub ) {

					for( INDEX y = pSub->m_Props.GetHead(); !pSub->m_Props.Failed(y); pSub->m_Props.GetNext(y) ) {

						CPropValue *pSub1 = pSub->m_Props.GetData(y);

						if( pSub1 ) {

							CSPN * pSPN = New CSPN();

							pSPN->SetSize(pSub1->GetChild(L"Size")->GetNumber());

							pSPN->m_Number = pSub1->GetChild(L"Number")->GetNumber();

							pPGN->GetSPNs()->AppendItem(pSPN);
							}
						}
					  }

				pPGN->m_RepRate   = pEntry->GetChild(L"RepRate")->GetNumber();
						  
				pPGN->m_Prefix    = pEntry->GetChild(L"Prefix")->GetString();
						  
				pPGN->m_Title     = pEntry->GetChild(L"Title")->GetString();
						  
				pPGN->m_Diag      = pEntry->GetChild(L"Diag")->GetNumber();
						  
				pPGN->m_Enhanced  = pEntry->GetChild(L"Enhanced")->GetNumber();

				pPGN->m_Direction = pEntry->GetChild(L"Direction")->GetNumber();
								
				m_pPGNList->AppendItem(pPGN);
				}
			}    

		return TRUE;
		}

	return FALSE;
	}


// UI Loading

BOOL CCANJ1939DriverOptions::OnLoadPages(CUIPageList * pList)
{ 
	CCANJ1939DriverOptionsUIPage * pPage = New CCANJ1939DriverOptionsUIPage(this);

	pList->Append(pPage);
	
	return TRUE;			   
	}

// UI Managament

void CCANJ1939DriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Manage" ) {

			CCustomPGNDialog Dlg(this);
	
			Dlg.Execute(*pWnd);
			}
 		}
	}

// Download Support

BOOL CCANJ1939DriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Source));

	Init.AddWord(WORD(m_Source2));

	return TRUE;
	}

// Meta Data Creation

void CCANJ1939DriverOptions::AddMetaData(void)
{ 	
	CUIItem::AddMetaData();

	Meta_AddInteger(Source);

	Meta_AddCollect(PGNList);

	Meta_AddInteger(List);

	Meta_AddInteger(Priority);

	Meta_AddInteger(RepRate);

	Meta_AddInteger(SPNs);

	Meta_AddInteger(SPN);

	Meta_AddInteger(Size1);

	Meta_AddInteger(Size2);

	Meta_AddInteger(Size3);

	Meta_AddInteger(Size4);

	Meta_AddInteger(Size5);

	Meta_AddInteger(Size6);

	Meta_AddInteger(Size7);

	Meta_AddInteger(Size8);	

	Meta_AddInteger(Diag);

	Meta_AddInteger(Enhanced);

	Meta_AddInteger(Direction);

	Meta_AddInteger(Source2);

	Meta_SetName((IDS_J_DRIVER_OPTIONS));
	}

// Implementation

BOOL CCANJ1939DriverOptions::MakePGN(CPGN * pNew, CPGN * pFrom)
{		
	UINT uNum  = pFrom ? pFrom->m_Number    : pNew->m_Number;

	UINT uDir  = pFrom ? pFrom->m_Direction : pNew->m_Direction;

	for( INDEX i = m_pPGNList->GetHead(); !m_pPGNList->Failed(i); m_pPGNList->GetNext(i) ) {

		CPGN * pPGN  = m_pPGNList->GetItem(i);

		if( pPGN->IsThisPGN(uNum, uDir) ) {

			m_pPGNList->InsertItem(pNew, pPGN);

			m_pPGNList->RemoveItem(pPGN);

			delete pPGN;

			return FALSE;
			}

		if( pPGN->m_Number == uNum && uDir == 0 ) {

			delete pNew;

			return FALSE;
			}
		} 

	m_pPGNList->AppendItem(pNew); 

	return TRUE;

	}

BOOL CCANJ1939DriverOptions::RemovePGN(CPGN * PGN)
{
	UINT uNum  = PGN->m_Number;

	UINT uDir = PGN->m_Direction;

	for( INDEX i = m_pPGNList->GetHead(); !m_pPGNList->Failed(i); m_pPGNList->GetNext(i) ) {

		CPGN * pPGN = m_pPGNList->GetItem(i);
		
		if( pPGN->IsThisPGN(uNum, uDir) ) {

			m_pPGNList->RemoveItem(pPGN);

			delete pPGN;

			return TRUE;
			}
		} 

	return FALSE;
	}

CPGN * CCANJ1939DriverOptions::GetPGN(CString Text)
{
	if( !Text.IsEmpty() ) {

		UINT uFind = Text.Find('-');

		if( uFind < NOTHING ) {

			UINT uCount = m_pPGNList->GetItemCount();

			for( UINT u = 0; u < uCount; u++ ) {

				if( !tstrncmp(Text.Mid(0, uFind), m_pPGNList->GetItem(u)->m_Prefix, uFind)) {

					return m_pPGNList->GetItem(u);
					}

				if( !tstrncmp(Text.Mid(uFind + 1), m_pPGNList->GetItem(u)->m_Prefix, uFind)) {

					return m_pPGNList->GetItem(u);
					}
				}
			}
		}

	return NULL;
	}

BOOL CCANJ1939DriverOptions::IsMfrAssigned(UINT uPGN)
{
	UINT uPF = uPGN >> 8;

	return uPF == 0xEF || uPF == 0xFF || uPGN == 65229;
	}

//////////////////////////////////////////////////////////////////////////
//
// J1939 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANJ1939DeviceOptions, CUIItem);

// Constructor

CCANJ1939DeviceOptions::CCANJ1939DeviceOptions(void)
{
	m_pPGNs   = New CPGNList;   

	m_Drop    = 1;

	m_BackOff = 200;

	m_ClrDTC  = 0;

	m_uLast   = 0;

	m_Source  = 0;
	}

// Persistance

void CCANJ1939DeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();

	for( INDEX i = m_pPGNs->GetHead(); !m_pPGNs->Failed(i); m_pPGNs->GetNext(i) ) {

		CPGN * pLast = m_pPGNs->GetItem(i);

		if( pLast ) {

			CAddress Ref;
			
			Ref.m_Ref = pLast->m_Ref;

			MakeMax(m_uLast, Ref.a.m_Table);
			}
		}
	}

// Data Access

UINT CCANJ1939DeviceOptions::GetNextTable(void)
{
	m_uLast++;

	return m_uLast;
	}

CPGNList * CCANJ1939DeviceOptions::GetPGNList(void)
{
	return m_pPGNs;
	}

CPGN * CCANJ1939DeviceOptions::GetPGN(UINT uNumber)
{     
	UINT uCount = m_pPGNs->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		if( uNumber == m_pPGNs->GetItem(u)->m_Number ) {

			return m_pPGNs->GetItem(u);
			}
		}
	
	return NULL;
	}

CPGN * CCANJ1939DeviceOptions::GetPGN(CString Text)
{
	if( !Text.IsEmpty() ) {

		UINT uFind = Text.Find('-');

		if( uFind < NOTHING ) {

			UINT uCount = m_pPGNs->GetItemCount();

			for( UINT u = 0; u < uCount; u++ ) {

				if( !tstrncmp(Text.Mid(0, uFind), m_pPGNs->GetItem(u)->m_Prefix, uFind)) {

					return m_pPGNs->GetItem(u);
					}

				if( !tstrncmp(Text.Mid(uFind + 1), m_pPGNs->GetItem(u)->m_Prefix, uFind)) {

					return m_pPGNs->GetItem(u);
					}
				}
			}
		}

	return NULL;
	}

void CCANJ1939DeviceOptions::Rebuild(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		ClearTables();		

		pSystem->Rebuild(1);

		if( m_pPGNs ) {

			for( INDEX i = m_pPGNs->GetHead(); !m_pPGNs->Failed(i); m_pPGNs->GetNext(i)) {

				CPGN * pItem = m_pPGNs->GetItem(i);

				if( pItem ) {

					if( !pItem->m_fMark ) {

						m_pPGNs->GetPrev(i);
						
						m_pPGNs->RemoveItem(pItem);
						}
					else {
						pItem->m_fMark = FALSE;
						}
					}
				}
			}
		}
	}

// Implementation

void CCANJ1939DeviceOptions::ClearTables(void)
{
	m_uLast = 0;

	for( INDEX i = m_pPGNs->GetHead(); !m_pPGNs->Failed(i); m_pPGNs->GetNext(i)) {

		CPGN * pItem = m_pPGNs->GetItem(i);

		if( pItem ) {

			pItem->m_Table = 0;
			}
		}
	}

// Download Support

BOOL CCANJ1939DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddLong(LONG(m_BackOff));

	Init.AddByte(BYTE(m_ClrDTC));

	if( m_pPGNs ) {

		UINT uCount = m_pPGNs->GetItemCount();

		Init.AddWord(WORD(uCount));

		for( UINT u = 0; u < uCount; u++ ) {

			m_pPGNs->GetItem(u)->MakeInitData(Init);
			}
		}
	else {
		Init.AddWord(0);
		}  

	Init.AddByte(BYTE(m_Source));

	return TRUE;
	}

// Meta Data Creation

void CCANJ1939DeviceOptions::AddMetaData(void)	
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(BackOff);

	Meta_AddInteger(ClrDTC);
		
	Meta_AddCollect(PGNs);

	Meta_AddInteger(Source);
	
	Meta_SetName((IDS_J_DEVICE_OPTIONS));
	}

// Implementation

BOOL CCANJ1939DeviceOptions::MakePGN(CPGN * PGN)
{		
	UINT uNum = PGN->m_Number;

	UINT uDir = PGN->m_Direction;

	for( INDEX i = m_pPGNs->GetHead(); !m_pPGNs->Failed(i); m_pPGNs->GetNext(i) ) {

		CPGN * pPGN = m_pPGNs->GetItem(i);
		
		if( pPGN->IsThisPGN(uNum, uDir) ) {

			m_pPGNs->InsertItem(PGN, pPGN);

			m_pPGNs->RemoveItem(pPGN);

			delete pPGN;

			return FALSE;
			}
		} 

	m_pPGNs->AppendItem(PGN); 

	return TRUE;
	}


/////////////////////////////////////////////////////////////////////////
//
// J1939 Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CCANJ1939Dialog, CStdDialog);
		
// Constructor

CCANJ1939Dialog::CCANJ1939Dialog(CCANJ1939Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;
	
	SetName(L"CANJ1939Dlg");
 	}

// Message Map

AfxMessageMap(CCANJ1939Dialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange) 
	
	AfxMessageEnd(CCANJ1939Dialog)
	};

// Message Handlers

BOOL CCANJ1939Dialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadPGNs();

	LoadOptions();

	LoadPriority();

	LoadRepRate();

	return TRUE;
	}

// Notification Handlers

void CCANJ1939Dialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 1001 ) {

		SetOptions();

		SetPriority();

		SetRepRate();
		}
	}


// Command Handlers

BOOL CCANJ1939Dialog::OnOkay(UINT uID)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uSel = Box.GetCurSel();

	if( uSel > 0 ) {

		CError   Error(TRUE);
		
		CAddress Addr;

		CString Text = Box.GetText(uSel);

		BOOL fMonico = m_pDriver->GetID() == 0x4090;

		if( fMonico ) {

			UINT uFind = Text.Find('\t');

			if( uFind < NOTHING ) {

				Text = m_pDriver->FindTextFromPrefix(Text.Mid(0, uFind));
				}
			}

		CButton &Button = (CButton &) GetDlgItem(2002);

		Text += "*";

		if( Button.IsChecked() ) {

			Text += "R";
			}

		CComboBox &Priority = (CComboBox &) GetDlgItem(2001);

		CEditCtrl &RepRate = (CEditCtrl &) GetDlgItem(2003);

		Text += CPrintf("%u", Priority.GetCurSel());

		Text += CPrintf("%5.5u", tstrtol(RepRate.GetWindowText(), NULL, 10));

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			if( m_pAddr && m_pAddr->m_Ref ) {
				
				UINT uMask = 0xFF00FFFF;
				
				if( (m_pAddr->m_Ref & uMask) != (Addr.m_Ref & uMask) ) { 

					m_pDriver->DeleteDevicePGN(*m_pAddr, m_pConfig);
					}
				}
			}

		AfxAssume(m_pAddr);

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	m_pAddr->m_Ref  = 0;
	
	EndDialog(TRUE);

	return TRUE;				    
	}

// Implementation

void CCANJ1939Dialog::LoadPGNs(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	BOOL fMonico = m_pDriver->GetID() == 0x4090;

	if( fMonico ) {

		int nTab[] = { 50 };

		Box.SetTabStops(elements(nTab), nTab);

		CString Name = "<ABBR>\tNo Selection";

		Box.AddString(Name);

		CStringArray List;

		for( UINT u = 0; u < m_pDriver->GetPGNCount(); u++ ) {

			CPGN * pPGN = m_pDriver->GetPGN(u);

			List.Append(m_pDriver->GetFullPGString(pPGN, TRUE));
			}

		List.Sort();

		UINT uPos = 0;
	
		if( m_pAddr->m_Ref ) {

			uPos = m_pDriver->FindPGNIndex(*m_pAddr, m_pConfig);

			Name = m_pDriver->GetFullPGString(m_pDriver->GetPGN(uPos), TRUE);
			}

		for( UINT i = 0; i < List.GetCount(); i++ ) {

			Box.AddString(List[i]);

			if( m_pAddr->m_Ref && Name == List[i] ) {

				uPos = i + 1;
				}
			}

		Box.SetCurSel(uPos);

		return;
		}

	int nTab[] = { 30, 200 };

	Box.SetTabStops(elements(nTab), nTab);
	
	Box.AddString("<PGN>\tNo Selection\t<ABBR>");

	for( UINT u = 0; u < m_pDriver->GetPGNCount(); u++ ) {

		CPGN * pPGN = m_pDriver->GetPGN(u);

		Box.AddString(m_pDriver->GetFullPGString(pPGN, TRUE));
		}
	
	Box.SetCurSel(m_pAddr->m_Ref ? m_pDriver->FindPGNIndex(*m_pAddr, m_pConfig) + 1 : 0);
	}

void CCANJ1939Dialog::LoadOptions(void)
{
	SetOptions();
	}

void CCANJ1939Dialog::LoadPriority(void)
{
	CComboBox &Priority = (CComboBox &) GetDlgItem(2001);

	CString Format;

	CString Text;

	for( UINT u = 0; u <= 7; u++ ) {

		Format = "%u ";

		if( u == 0 ) {

			Format += "- highest";
			}
		
		else if ( u == 7 ) {

			Format += "- lowest";
			}

		Text.Printf(Format, u);

		Priority.AddString(PCTXT(Text));
		}
	
	SetPriority(); 
	}

void CCANJ1939Dialog::LoadRepRate(void)
{
	SetRepRate();
	}	

void CCANJ1939Dialog::SetOptions(void)
{
	if( m_pDriver ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);
	
		CButton &Button = (CButton &) GetDlgItem(2002);

		Button.SetCheck(FALSE);

		CPGN * pPGN = m_pDriver->GetPGN(Box.GetCurSel() - 1);

		BOOL fMonico = m_pDriver->GetID() == 0x4090;

		if( fMonico ) {

			pPGN = m_pDriver->GetPGN(Box.GetText(Box.GetCurSel()));
			}

		CCANJ1939DeviceOptions * pOptions = (CCANJ1939DeviceOptions *) m_pConfig;

		if( pPGN ) {

			if( pOptions && pOptions->GetPGN(pPGN->m_Number) ) {

				pPGN = pOptions->GetPGN(pPGN->m_Number);
				}

			if( pPGN ) {

				Button.SetCheck(pPGN->m_SendReq);
				}
			}

		Button.EnableWindow(pPGN && !pPGN->m_Diag ? TRUE : FALSE);
		}
	}

void CCANJ1939Dialog::SetPriority(void)
{
	if( m_pDriver ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		CPGN * pPGN = m_pDriver->GetPGN(Box.GetCurSel() - 1);

		BOOL fMonico = m_pDriver->GetID() == 0x4090;

		if( fMonico ) {

			pPGN = m_pDriver->GetPGN(Box.GetText(Box.GetCurSel()));
			}

		CComboBox &Priority = (CComboBox &) GetDlgItem(2001);

		CCANJ1939DeviceOptions * pOptions = (CCANJ1939DeviceOptions *) m_pConfig;

		if( pPGN ) {

			if( pOptions && pOptions->GetPGN(pPGN->m_Number) ) {

				pPGN = pOptions->GetPGN(pPGN->m_Number);
				}

			if( pPGN ) {

				UINT uSel = pPGN->m_Priority;
				
				Priority.SetCurSel(uSel);
		       		}
			}

		Priority.EnableWindow(pPGN && !pPGN->m_Diag ? TRUE : FALSE);
		}
	}


void CCANJ1939Dialog::SetRepRate(void)
{
	if( m_pDriver ) {

		CEditCtrl &Ctrl = (CEditCtrl &) GetDlgItem(2003);

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		CPGN * pPGN = m_pDriver->GetPGN(Box.GetCurSel() - 1);

		BOOL fMonico = m_pDriver->GetID() == 0x4090;

		if( fMonico ) {

			pPGN = m_pDriver->GetPGN(Box.GetText(Box.GetCurSel()));
			}

		CCANJ1939DeviceOptions * pOptions = (CCANJ1939DeviceOptions *) m_pConfig;

		if( pPGN ) {

			if( pOptions && pOptions->GetPGN(pPGN->m_Number) ) {

				pPGN = pOptions->GetPGN(pPGN->m_Number);
				}

			if( pPGN ) {

				UINT uRepRate = pPGN->m_RepRate;

				CString RepRate = "%u";

				RepRate.Printf(RepRate, uRepRate);
		
				Ctrl.SetWindowText(RepRate);
				}
			}

		Ctrl.EnableWindow(pPGN && !pPGN->m_Diag ? TRUE : FALSE);
		}
	}  

//////////////////////////////////////////////////////////////////////////
//
// J1939 User Defined PGN Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCustomPGNDialog, CStdDialog);
		
// Constructor

CCustomPGNDialog::CCustomPGNDialog(CCANJ1939DriverOptions * pOptions)
{
	memset(m_pSPNs, 0, sizeof(m_pSPNs));

	m_pOptions = pOptions;

	SetName(L"CustomPGNDlg");
	}

// Message Map

AfxMessageMap(CCustomPGNDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchCommand(CP_ADD, OnCreateEdit)
	AfxDispatchCommand(CP_DEL, OnRemove)
	
	AfxDispatchNotify(1001,		LBN_SELCHANGE,	OnPGNChange )
	AfxDispatchNotify(CP_DIAG,	CBN_SELCHANGE,  OnDiagChange)
	AfxDispatchNotify(CP_NUM,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_REP,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPNs,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN1,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN2,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN3,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN4,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN5,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN6,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN7,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN8,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_PAGE,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_ENH,	CBN_SELCHANGE,	OnEditChange)

	AfxMessageEnd(CCustomPGNDialog)
	};
 
// Message Handlers

BOOL CCustomPGNDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadPGNList();

	LoadCombos();

	InitAll();

	return TRUE;
	}

// Notification Handlers

void CCustomPGNDialog::OnPGNChange(UINT uID, CWnd &Wnd)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uSel = Box.GetCurSel();

	if( uSel == 0  ) {

		InitAll();

		return;
		}

	if( m_pOptions ) {

		CPGN * pPGN = m_pOptions->m_pPGNList->GetItem(uSel - 1);

		if( pPGN ) {

			GetDlgItem(CP_NUM).SetWindowText(CPrintf("%u", pPGN->m_Number));

			GetDlgItem(CP_TITLE).SetWindowText(GetTitle(pPGN));

			GetDlgItem(CP_PRE).SetWindowText(pPGN->m_Prefix);

			CComboBox &Diag = (CComboBox &)GetDlgItem(CP_DIAG);

			Diag.SetCurSel(pPGN->m_Diag);

			CComboBox &Enh = (CComboBox &)GetDlgItem(CP_ENH);

			Enh.SetCurSel(pPGN->m_Enhanced);

			CComboBox &Dir = (CComboBox &)GetDlgItem(CP_DIR);

			Dir.SetCurSel(pPGN->m_Direction);

			CComboBox &Priority = (CComboBox &)GetDlgItem(CP_PRIOR);

			Priority.SetCurSel(pPGN->m_Priority);

			GetDlgItem(CP_REP).SetWindowText(CPrintf("%u", pPGN->m_RepRate));

			UINT uSPNs = pPGN->GetSPNs()->GetItemCount();

			GetDlgItem(CP_SPNs).SetWindowText(CPrintf("%u", uSPNs));

			for( UINT u = 0; u < uSPNs; u++ ) {

				m_pSPNs[u] = BYTE(pPGN->GetSPNs()->GetItem(u)->GetSize());
				}
			}

		GetDlgItem(CP_PAGE).SetWindowText("1");

		SetSPNs();

		DoEnables();
		}
	}

void CCustomPGNDialog::OnDiagChange(UINT uID, CWnd &Wnd)
{
	CComboBox &Box = (CComboBox &)GetDlgItem(CP_DIAG);

	UINT uSel = Box.GetCurSel();

	if( uSel ) {

		GetDlgItem(CP_SPNs).SetWindowText("8");

		CString Text = uSel == 2 ? "32" : "8";

		for( UINT u = CP_SPN1; u <= CP_SPN8; u+=2 ) {

			GetDlgItem(u).SetWindowText(Text);

			OnEditChange(u, Wnd);
			}
		}

	DoEnables();
	}

void CCustomPGNDialog::OnEditChange(UINT uID, CWnd &Wnd)
{
	UINT uHi = 0;

	UINT uLo = 0;

	BOOL fSPNs = FALSE;

	BOOL fPage = FALSE;

	if( uID == CP_ENH ) {

		DoEnables();

		return;
		}

	if( uID == CP_NUM ) {

		uHi = 65535;

		UINT uPGN  = tstrtol(GetDlgItem(CP_NUM).GetWindowText(), NULL, 10);

		if( !m_pOptions->IsMfrAssigned(uPGN) ) {

			CComboBox &Dir = (CComboBox &) GetDlgItem(CP_DIR);

			Dir.SetCurSel(0);
			}
		}
	
	if( uID == CP_REP ) {

		uHi = 65535;
		}

	if( uID == CP_SPNs ) {

		uHi = 64;
		}

	if( (uID >= CP_SPN1) && (uID <= CP_SPN8) && (uID % 2 == 0) ) {

		uHi = 32;

		fSPNs = TRUE;
		}

	if( uID == CP_PAGE ) {

		uHi = 8;

		uLo = 1;

		fPage = TRUE;
		}

	if( uHi > 0 ) {

 		if( !IsInteger(uID) ) {

			CWnd w;

			w.Error(CPrintf("Valid values are %u - %u", uLo, uHi));

			GetDlgItem(uID).SetFocus();

			return;
			}

		UINT uCheck = tstrtol(GetDlgItem(uID).GetWindowText(), NULL, 10);

		if( uCheck > uHi ) {

			GetDlgItem(uID).SetWindowText(CPrintf("%u", uHi));
			}

		if( uCheck < uLo ) {

			GetDlgItem(uID).SetWindowText(CPrintf("%u", uLo));
			}

		if( fSPNs ) {

			UINT uPage  = tstrtol(GetDlgItem(CP_PAGE).GetWindowText(), NULL, 10);

			if( uPage > 0 ) {

				uPage -= 1;
				}

			UINT uIndex = uPage * 8 + (uID - CP_SPN1) / 2;

			m_pSPNs[uIndex] = BYTE(tstrtol(GetDlgItem(uID).GetWindowText(), NULL, 10)) | BITS;
			}
		
		if( fPage ) {
		
			SetSPNs(FALSE);
			}

		DoEnables();
		}
	}

BOOL CCustomPGNDialog::OnCreateEdit(UINT uID)
{
	if( m_pOptions ) {

		CComboBox &Diag = (CComboBox &) GetDlgItem(CP_DIAG);

		CComboBox &Enh  = (CComboBox &) GetDlgItem(CP_ENH);

		CComboBox &Dir  = (CComboBox &) GetDlgItem(CP_DIR);

		CComboBox &Priority = (CComboBox &) GetDlgItem(CP_PRIOR);

		UINT uPGN     = tstrtol(GetDlgItem(CP_NUM).GetWindowText(), NULL, 10);

		if( uPGN > 0xFFFF || (((uPGN >> 8) < 0xF0) && ((uPGN & 0xFF) != 0)) ) {

			CWnd w;	  

			w.Error(CPrintf("PGN %u is not currently supported.\n\n"
				"Please contact technical support for more details.", uPGN));
				
			return FALSE;
			}

		CString Title     = GetDlgItem(CP_TITLE).GetWindowText();
			          
		CString Pre       = GetDlgItem(CP_PRE).GetWindowText();
			          
		UINT uDiag        = Diag.GetCurSel();
			          
		UINT uEnh         = Enh.GetCurSel();
			          
		UINT uDir         = Dir.GetCurSel();
			          
		UINT uPrior       = Priority.GetCurSel();
			          
		UINT uRep         = tstrtol(GetDlgItem(CP_REP).GetWindowText(), NULL, 10);
			          
		UINT uSPNs        = tstrtol(GetDlgItem(CP_SPNs).GetWindowText(), NULL, 10);

		CPGN * pPGN       = New CPGN();	

		pPGN->m_Number    = uPGN;
			          
		pPGN->m_Title     = Title;
			          
		pPGN->m_Prefix    = Pre;
			          
		pPGN->m_Diag      = uDiag;
				  
		pPGN->m_Priority  = uPrior;
				  
		pPGN->m_Enhanced  = uEnh;

		pPGN->m_Direction = uDir;

		pPGN->m_RepRate   = uRep;
			
		pPGN->GrowSPNList(uSPNs);

		for( UINT u = 0; u < uSPNs; u++ ) {

			pPGN->GetSPNs()->GetItem(u)->SetSize(m_pSPNs[u]);
			
			pPGN->GetSPNs()->GetItem(u)->m_Number = (u + 1);
			}

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		UINT uSel = Box.GetCurSel();

		CPGN * pFrom = uSel ? m_pOptions->m_pPGNList->GetItem(uSel - 1) : NULL;

		m_pOptions->MakePGN(pPGN, pFrom);

		LoadPGNList();

		InitAll();
				
		return TRUE;
		}

	return FALSE;
	}

BOOL CCustomPGNDialog::OnRemove(UINT uID)
{
	if( m_pOptions ) {

		CPGN  PGN;

		CPGN * pPGN       = &PGN;
			          
		UINT uPGN         = tstrtol(GetDlgItem(CP_NUM).GetWindowText(), NULL, 10);
			          
		pPGN->m_Number    = uPGN;
			          
		CComboBox &Dir    = (CComboBox &) GetDlgItem(CP_DIR);

		pPGN->m_Direction = Dir.GetCurSel();
		
		m_pOptions->RemovePGN(pPGN);
		
		LoadPGNList();

		InitAll();
		}

	return TRUE;
	}

// Implementation

void CCustomPGNDialog::LoadPGNList(void)
{
	if( m_pOptions ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		Box.ResetContent();

		int nTab[] = { 40, 110 };

		Box.SetTabStops(elements(nTab), nTab);

		CPGNList * pList = m_pOptions->m_pPGNList;

		Box.AddString("<none>\t<PGN List>\t<prefix>");

		for( UINT u = 0; u < pList->GetItemCount(); u++ ) {

			CPGN * pPGN = pList->GetItem(u);

			if( pPGN ) {

				CString PGN;

				PGN.Printf("%u\t", pPGN->m_Number);

				PGN += GetTitle(pPGN);

				PGN += pPGN->m_Prefix;

				Box.AddString(PGN);

				continue;
				}

			break;
			}

		Box.SetCurSel(0);
		}
	}

void CCustomPGNDialog::LoadCombos(void)
{
	CComboBox &Diag = (CComboBox &) GetDlgItem(CP_DIAG);

	Diag.AddString(CString(IDS_DRIVER_NONE));

	Diag.AddString("Report Network Nodes");

	Diag.AddString("Report Network PGNs");

	Diag.SetCurSel(0);

	CComboBox &Priority = (CComboBox &) GetDlgItem(CP_PRIOR);

	CString Num;

	for( UINT u = 0; u <= 7; u++ ) {

		Num.Printf("%u ", u);

		if( u == 0 ) {

			Num += "- highest";
			}

		if( u == 7 ) {

			Num += "- lowest";
			}

		Priority.AddString(Num);
		}

	Priority.SetCurSel(6);

	CComboBox &Enh = (CComboBox &) GetDlgItem(CP_ENH);

	Enh.AddString(CString(IDS_DRIVER_NONE));

	Enh.AddString("Rapid Fire Message");

	Enh.SetCurSel(0);

	CComboBox &Dir = (CComboBox &) GetDlgItem(CP_DIR);

	Dir.AddString("Bidirectional");

	Dir.AddString("Transmit");

	Dir.AddString("Receive");

	Dir.SetCurSel(0);
	}

void CCustomPGNDialog::InitAll(void)
{
	GetDlgItem(CP_NUM).SetWindowText("");

	GetDlgItem(CP_TITLE).SetWindowText("");

	GetDlgItem(CP_PRE).SetWindowText("");

	GetDlgItem(CP_REP).SetWindowText("1000");

	GetDlgItem(CP_SPNs).SetWindowText("1");

	GetDlgItem(CP_PAGE).SetWindowText("1");

	memset(m_pSPNs, 0, sizeof(m_pSPNs));

	SetSPNs();

	GetDlgItem(CP_SPN1).SetWindowText("0");

	GetDlgItem(CP_SPN2).SetWindowText("0");

	GetDlgItem(CP_SPN3).SetWindowText("0");

	GetDlgItem(CP_SPN4).SetWindowText("0");

	GetDlgItem(CP_SPN5).SetWindowText("0");

	GetDlgItem(CP_SPN6).SetWindowText("0");

	GetDlgItem(CP_SPN7).SetWindowText("0");

	GetDlgItem(CP_SPN8).SetWindowText("0");

	CComboBox& Diag = (CComboBox &)GetDlgItem(CP_DIAG);

	Diag.SetCurSel(0);

	CComboBox& Enh = (CComboBox &)GetDlgItem(CP_ENH);

	Enh.SetCurSel(0);

	CComboBox& Priority = (CComboBox &)GetDlgItem(CP_PRIOR);

	Priority.SetCurSel(6);

	CComboBox& Dir = (CComboBox &)GetDlgItem(CP_DIR);

	Dir.SetCurSel(0);

	DoEnables();
	}

void CCustomPGNDialog::DoEnables(void)
{	
	BOOL fEnable   = !GetDlgItem(CP_NUM).GetWindowText().IsEmpty();
		       
	UINT uPGN      = tstrtol(GetDlgItem(CP_NUM ).GetWindowText(), NULL, 10);
		       
	UINT uSPNs     = tstrtol(GetDlgItem(CP_SPNs).GetWindowText(), NULL, 10);
		       
	UINT uPage     = tstrtol(GetDlgItem(CP_PAGE).GetWindowText(), NULL, 10) - 1;

	CComboBox &Box = (CComboBox &)GetDlgItem(CP_DIAG);

	CComboBox &Enh = (CComboBox &)GetDlgItem(CP_ENH);

	GetDlgItem(CP_DIAG).EnableWindow(fEnable && !Enh.GetCurSel());

	GetDlgItem(CP_ENH).EnableWindow(fEnable && !Box.GetCurSel());

	GetDlgItem(CP_DIR).EnableWindow(fEnable && m_pOptions->IsMfrAssigned(uPGN));	

	GetDlgItem(CP_PRIOR).EnableWindow(fEnable && !Box.GetCurSel());

	GetDlgItem(CP_REP).EnableWindow(fEnable && !Box.GetCurSel());

	GetDlgItem(CP_SPNs).EnableWindow(fEnable);

	UINT uSize = 8;

	for( UINT x = CP_SPN1, y = 1; x <= CP_SPN8; x+=2, y++ ) {

		GetDlgItem(x).EnableWindow(fEnable ? uSPNs >= y + uSize * uPage : FALSE);
		} 

	GetDlgItem(CP_PAGE).EnableWindow(fEnable ? uSPNs > uSize : FALSE);

	CListBox &List = (CListBox &) GetDlgItem(1001);

	GetDlgItem(CP_DEL).EnableWindow(List.GetCurSel());
	}

void CCustomPGNDialog::SetSPNs(BOOL fUpdate)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	CPGN * pPGN = NULL;

	UINT uSel = Box.GetCurSel();

	if( uSel > 0 ) {
		
		pPGN = m_pOptions->m_pPGNList->GetItem(uSel - 1);
		}
	
	UINT uPage = tstrtol(GetDlgItem(CP_PAGE).GetWindowText(), NULL, 10) - 1;

	if( uPage == NOTHING ) {

		uPage = 0;
		}

	CString Text;

	for( UINT a = 3001, b = 0; a < 3017; a+=2, b++ ) {

		CString String = GetDlgItem(a).GetWindowText();

		UINT uLen = String.GetLength();

		String = String.Mid(0, uLen - 3);
		
		Text = CPrintf(L"%2.2u:", uPage * 8 + b + 1 );

		Text = String + Text;

		GetDlgItem(a).SetWindowText(Text);
		}

	for( UINT x = CP_SPN1, y = 0; x <= CP_SPN8; x+=2, y++ ) {

		Text.Printf("%u", m_pSPNs[y + uPage * 8] & 0x7F);
		
		if( fUpdate && pPGN ) {

			CSPN * pSPN = pPGN->GetSPNs()->GetItem(y + uPage * 8);

			if( pSPN ) {
						
				Text.Printf("%u", pSPN->GetSize() & 0x7F);
				}
			else {
				Text = "0";
				}
			}

		GetDlgItem(x).SetWindowText(Text);
		}  
	 }

// Helpers

BOOL CCustomPGNDialog::IsInteger(UINT uID)
{
	CString Text = GetDlgItem(uID).GetWindowText();

	for( UINT u = 0; u < Text.GetLength(); u++ ) {

		TCHAR Char = Text.GetAt(u);

		if( Char < '0' || Char > '9' ) {

			return FALSE;
			}
		}
	
	return TRUE;
	}

CString CCustomPGNDialog::GetTitle(CPGN * pPGN)
{
	CString Title = pPGN->m_Title;

	UINT uFind = Title.Find('\t');

	while( uFind < NOTHING ) {

		Title.Remove('\t');

		uFind = Title.Find('\t');
		}

	return Title + "\t";
	}

// End of File
