
#include "intern.hpp"

#include "elmo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Elmo EP Driver
//

// Instantiator

ICommsDriver *	Create_ElmoDriver(void)
{
	return New CElmoDriver;
	}

// Constructor

CElmoDriver::CElmoDriver(void)
{
	m_wID		= 0x403B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Elmo";
	
	m_DriverName	= "Elmo Drives";
	
	m_Version	= "1.0";
	
	m_ShortName	= "Elmo Drives";

	m_uListSel	= TMF;

	AddSpaces();
	}

// Binding Control

UINT	CElmoDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CElmoDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CElmoDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	if( fPart && Addr.a.m_Table >= HMO ) return FALSE;
	
	CElmoAddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL CElmoDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Type   = pSpace->m_uType;
	Addr.a.m_Extra  = m_uListSel == TMF ? 0 : 1;

	UINT uOffset = uOffset = tatoi(Text);

	if( uOffset < pSpace->m_uMinimum || uOffset > pSpace->m_uMaximum ) {

		CString sE;

		sE.Printf( "%s%d - %s%d",

			pSpace->m_Prefix,
			pSpace->m_uMinimum,
			pSpace->m_Prefix,
			pSpace->m_uMaximum
			);

		Error.Set( sE, 0 );

		return FALSE;
		}

	Addr.a.m_Offset = uOffset;

	return TRUE;
	}

BOOL CElmoDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( pSpace->m_uTable != TKW ) {

		if( NeedsParam(pSpace->m_uTable) ) {

			Text.Printf( "%s%d",

				pSpace->m_Prefix,

				Addr.a.m_Offset
				);
			}

		else {
			Text.Printf( "%s",

				pSpace->m_Prefix
				);
			}
		}

	else {
		if( Addr.a.m_Offset == 0xA ) Text.Printf("N/A");

		else {
			if( Addr.a.m_Offset == 0xF ) Text.Printf("N/A");

			else {
				Text.Printf( "%X",
					Addr.a.m_Offset
					);
				}
			}
		}

	return TRUE;
	}

BOOL CElmoDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Implementation	

void CElmoDriver::AddSpaces(void)
{
	AddSpace(New CSpace(HMO,"",	"Motion Commands...",				10,   0,    0, LL));
	AddSpace(New CSpace(CAC,"AC",	"   Acceleration",				10,   0,    0, LL));
	AddSpace(New CSpace(CBG,"BG",	"   Begin motion",				10,   0,    0, BB));
	AddSpace(New CSpace(CBT,"BT",	"   Begin motion at defined time",		10,   0,    0, LL));
	AddSpace(New CSpace(CDC,"DC",	"   Deceleration",				10,   0,    0, LL));
	AddSpace(New CSpace(CIL,"IL",	"   Input logic",				10,   1,   10, LL));
	AddSpace(New CSpace(CJV,"JV",	"   Speed of jogging motion",			10,   0,    0, LL));
	AddSpace(New CSpace(CMO,"MO",	"   Motor on/off",				10,   0,    0, LL));
	AddSpace(New CSpace(CPA,"PA",	"   Absolute position",				10,   0,    0, LL));
	AddSpace(New CSpace(CPR,"PR",	"   Relative position",				10,   0,    0, LL));
	AddSpace(New CSpace(CSD,"SD",	"   Stop deceleration",				10,   0,    0, LL));
	AddSpace(New CSpace(CSF,"SF",	"   Smooth factor for motion command",		10,   0,    0, LL));
	AddSpace(New CSpace(CSP,"SP",	"   Speed for point-to-point motion",		10,   0,    0, LL));
	AddSpace(New CSpace(CST,"ST",	"   Stop motion using deceleration value",	10,   0,    0, BB));
	AddSpace(New CSpace(CTC,"TC",	"   Torque command",				10,   0,    0, RR));

	AddSpace(New CSpace(HIO,"",	"I/O Commands...",				10,   0,    0, LL));
	AddSpace(New CSpace(CAN,"AN",	"   Read analog inputs",			10,   1,   10, RR));
	AddSpace(New CSpace(CIB,"IB",	"   Bit-wise digital input",			10,   0,   31, LL));
	AddSpace(New CSpace(CIF,"IF",	"   Digital input filter",			10,   1,   10, RR));
	AddSpace(New CSpace(CIP,"IP",	"   Read all digital inputs",			10,   0,    0, LL));
	AddSpace(New CSpace(COB,"OB",	"   Bit-wise digital output",			10,   1,   15, LL));
	AddSpace(New CSpace(COC,"OC",	"   Output Compare",				10,   1,    6, LL));
	AddSpace(New CSpace(COL,"OL",	"   Output Logic",				10,   1,    6, LL));
	AddSpace(New CSpace(COP,"OP",	"   Set all digital outputs",			10,   0,    0, LL));

	AddSpace(New CSpace(HST,"",	"Status Commands...",				10,   0,    0, LL));
	AddSpace(New CSpace(CBV,"BV",	"   Maximum motor DC voltage",			10,   0,    0, LL));
//	AddSpace(New CSpace(CDD,"DD",	"   CAN controller status",			10,   0,    0, LL));
	AddSpace(New CSpace(CDV,"DV",	"   Reference desired value",			10,   1,    7, RR));
	AddSpace(New CSpace(CEC,"EC",	"   Error code",				10,   0,    0, LL));
	AddSpace(New CSpace(CLC,"LC",	"   Current limitation",			10,   0,    0, LL));
	AddSpace(New CSpace(CMF,"MF",	"   Motor fault",				10,   0,    0, LL));
	AddSpace(New CSpace(CMS,"MS",	"   Motion status reporting",			10,   0,    0, LL));
// 	AddSpace(New CSpace(CPK,"PK",	"   Peak memory (Can't handle)",		10,   0,    0, LL));
	AddSpace(New CSpace(CSN,"SN",	"   Serial number",				10,   1,    4, LL));
	AddSpace(New CSpace(CSR,"SR",	"   Numerical",					10,   0,    0, LL));
	AddSpace(New CSpace(CTI,"TI",	"   Temperature indication",			10,   0,    0, LL));
	AddSpace(New CSpace(CVR,"VR",	"   Software (firmware) version",		10,   0,    0, LL));

	AddSpace(New CSpace(HFB,"",	"Feedback Commands...",				10,   0,    0, LL));
	AddSpace(New CSpace(CAB,"AB",	"   Absolute encoder setting parameters",	10,   1,    6, LL));
	AddSpace(New CSpace(CID,"ID",	"   Read active current",			10,   0,    0, RR));
	AddSpace(New CSpace(CIQ,"IQ",	"   Read reactive current",			10,   0,    0, RR));
	AddSpace(New CSpace(CPE,"PE",	"   Position error",				10,   0,    0, LL));
	AddSpace(New CSpace(CPX,"PX",	"   Main encoder position",			10,   0,    0, LL));
	AddSpace(New CSpace(CPY,"PY",	"   Auxiliary position",			10,   0,    0, LL));
	AddSpace(New CSpace(CVE,"VE",	"   Velocity error",				10,   0,    0, LL));
	AddSpace(New CSpace(CVX,"VX",	"   Main encoder velocity",			10,   0,    0, LL));
	AddSpace(New CSpace(CVY,"VY",	"   Velocity of auxiliary feedback",		10,   0,    0, LL));
	AddSpace(New CSpace(CYA,"YA",	"   Auxiliary position sensor parameters",	10,   1,    5, LL));

	AddSpace(New CSpace(HCG,"",	"Configuration Commands...",			10,   0,    0, LL));
	AddSpace(New CSpace(CAG,"AG",	"   Analog gains array",			10,   1,    2, RR));
	AddSpace(New CSpace(CAS,"AS",	"   Analog input offsets array",		10,   1,   10, RR));
	AddSpace(New CSpace(CBP,"BP",	"   Brake parameter",				10,   1,    2, LL));
	AddSpace(New CSpace(CCA,"CA",	"   Commutation parameters array",		10,   1,   40, LL));
	AddSpace(New CSpace(CCL,"CL",	"   Current continuous limitations array",	10,   1,    3, RR));
	AddSpace(New CSpace(CEF,"EF",	"   Encoder filter frequency",			10,   1,    2, LL));
	AddSpace(New CSpace(CEM,"EM",	"   ECAM parameters",				10,   1,    8, LL));
	AddSpace(New CSpace(CET,"ET",	"   Entries for ECAM table",			10,   1, 1024, LL));
	AddSpace(New CSpace(CFF,"FF",	"   Feed forward",				10,   1,    2, RR));
	AddSpace(New CSpace(CFR,"FR",	"   Follower ratio",				10,   1,    3, RR));
	AddSpace(New CSpace(CHM,"HM",	"   Homing and capture mode",			10,   1,    8, LL));
	AddSpace(New CSpace(CHY,"HY",	"   Auxiliary home and capture mode",		10,   1,    8, LL));
	AddSpace(New CSpace(CMC,"MC",	"   Maximum peak current",			10,   0,    0, RR));
	AddSpace(New CSpace(CMP,"MP",	"   Motion (PT/PVT) parameters",		10,   1,    6, LL));
	AddSpace(New CSpace(CPL,"PL",	"   Peak duration and limit",			10,   1,    2, RR));
	AddSpace(New CSpace(CPM,"PM",	"   Profiler mode",				10,   0,    0, LL));
	AddSpace(New CSpace(CPT,"PT",	"   Position time command",			10,   0,    0, LL));
	AddSpace(New CSpace(CPV,"PV",	"   Position velocity time command",		10,   0,    0, LL));
	AddSpace(New CSpace(CPW,"PW",	"   PWM signal parameters",			10,   1,    2, RR));
	AddSpace(New CSpace(CQP,"QP",	"   Position",					10,   1, 1024, LL));
	AddSpace(New CSpace(CQT,"QT",	"   Time",					10,   1,   64, LL));
	AddSpace(New CSpace(CQV,"QV",	"   Velocity",					10,   1,   64, LL));
 	AddSpace(New CSpace(CRM,"RM",	"   Reference mode (analog en./dis.",		10,   0,    0, LL));
	AddSpace(New CSpace(CUM,"UM",	"   Unit mode stepper",				10,   0,    0, LL));
	AddSpace(New CSpace(CTR,"TR",	"   Target radius",				10,   1,    2, LL));
	AddSpace(New CSpace(CVH,"VH",	"   High reference limit",			10,   2,    3, LL));
	AddSpace(New CSpace(CVL,"VL",	"   Low reference limit",			10,   2,    3, LL));
	AddSpace(New CSpace(CXM,"XM",	"   X Modulo",					10,   1,    2, LL));
 	AddSpace(New CSpace(CYM,"YM",	"   Y Modulo",					10,   1,    2, LL));

	AddSpace(New CSpace(HCF,"",	"Control Filter Commands...",			10,   0,    0, LL));
	AddSpace(New CSpace(CGS,"GS",	"   Gain scheduling",				10,   0,   15, LL));
	AddSpace(New CSpace(CKR,"KGR",	"   Gain scheduled controller Reals",		10,   1,  189, RR));
	AddSpace(New CSpace(CKG,"KGI",	"   Gain scheduled controller Integers",	10, 190,  504, LL));
	AddSpace(New CSpace(CKI,"KI",	"   PID integral terms array",			10,   1,    3, RR));
	AddSpace(New CSpace(CKP,"KP",	"   PID proportional terms array",		10,   1,    3, RR));
	AddSpace(New CSpace(CKV,"KV",	"   Advanced filter for speed loop",		10,   0,   99, LL));
	AddSpace(New CSpace(CXA,"XA",	"   Extra parameters (more)",			10,   1,    4, LL));
	AddSpace(New CSpace(CXP,"XP",	"   Extra parameters",				10,   0,    9, LL));

	AddSpace(New CSpace(HPR,"",	"Protection Commands...",			10,   0,    0, LL));
	AddSpace(New CSpace(CER,"ER",	"   Maximum tracking errors",			10,   2,    3, LL));
	AddSpace(New CSpace(CHL,"HL",	"   Over-speed and position range limit",	10,   2,    3, LL));
	AddSpace(New CSpace(CLL,"LL",	"   Low actual feedback limit",			10,   2,    3, LL));

	AddSpace(New CSpace(HDR,"",	"Data Recording Commands...",			10,   0,    0, LL));
	AddSpace(New CSpace(CBH,"BH",	"   Get a sample signal as hexadecimal",	10,   1,    8, LL));
	AddSpace(New CSpace(CRC,"RC",	"   Variables to record",			10,   0,    0, LL));
	AddSpace(New CSpace(CRG,"RG",	"   Recording gap",				10,   0,    0, LL));
	AddSpace(New CSpace(CRL,"RL",	"   Record length",				10,   0,    0, LL));
	AddSpace(New CSpace(CRP,"RP",	"   Recorder parameters",			10,   1,    9, LL));
	AddSpace(New CSpace(CRR,"RR",	"   Recording on/off",				10,   0,    0, LL));
	AddSpace(New CSpace(CRV,"RV",	"   Recorded variables",			10,   1,   16, LL));

	AddSpace(New CSpace(HUP,"",	"User Parameters...",				10,   0,    0, LL));
	AddSpace(New CSpace(CHP,"HP",	"   Halt program execution",			10,   0,    0, BB));
	AddSpace(New CSpace(CKL,"KL",	"   Kill motion and stop program",		10,   0,    0, BB));
	AddSpace(New CSpace(CMI,"MI",	"   Mask interrupt",				10,   0,    0, LL));
	AddSpace(New CSpace(CPS,"PS",	"   Program status",				10,   0,    0, LL));
	AddSpace(New CSpace(CXC,"XC",	"   Continue program execution",		10,   0,    0, BB));
	AddSpace(New CSpace(CXQ,"XQ",	"   Execute program",				10,   0,    0, LL));

	AddSpace(New CSpace(HGC,"",	"General Commands...",				10,   0,    0, LL));
	AddSpace(New CSpace(CLD,"LD",	"   Load parameters from flash memory",		10,   0,    0, BB));
	AddSpace(New CSpace(CRS,"RS",	"   Reset Metronome",				10,   0,    0, BB));
	AddSpace(New CSpace(CSV,"SV",	"   Save parameters to flash memory",		10,   0,    0, BB));
	AddSpace(New CSpace(CTM,"TM",	"   System time",				10,   0,    0, LL));
	AddSpace(New CSpace(CTS,"TS",	"   Sampling time",				10,   0,    0, LL));
	AddSpace(New CSpace(CUF,"UF",	"   User float array",				10,   1,   24, RR));
	AddSpace(New CSpace(CUI,"UI",	"   User integer",				10,   1,   24, LL));
	AddSpace(New CSpace(CWI,"WI",	"   Metronome data",				10,   1,   23, LL));
	AddSpace(New CSpace(CWS,"WS",	"   Metronome data",				10,   3,   56, LL));
	AddSpace(New CSpace(CZX,"ZX",	"   Program and auto-tune storage",		10,   0, 1023, LL));

	// List Selections
	AddSpace(New CSpace(HAA,"",	"A...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAB,"",	"B...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAC,"",	"C...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAD,"",	"D...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAE,"",	"E...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAF,"",	"F...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAG,"",	"G...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAH,"",	"H...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAI,"",	"I...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAJ,"",	"J...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAK,"",	"K...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAL,"",	"L...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAM,"",	"M...",						10,   0,    0,	LL));
//	AddSpace(New CSpace(HAN,"",	"N...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAO,"",	"O...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAP,"",	"P...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAQ,"",	"Q...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAR,"",	"R...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAS,"",	"S...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAT,"",	"T...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAU,"",	"U...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAV,"",	"V...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAW,"",	"W...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAX,"",	"X...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAY,"",	"Y...",						10,   0,    0,	LL));
	AddSpace(New CSpace(HAZ,"",	"Z...",						10,   0,    0,	LL));

	// List Type Change
	AddSpace(New CSpace(TMF,"",	"Machine Functions",				10,   0,    0,	LL));
	AddSpace(New CSpace(TAZ,"",	"A - Z",					10,   0,    0,	LL));
	AddSpace(New CSpace(TKW, "",	"List By Keyword",				16, 0xA,  0xF,	LL));
	}

// Helpers

BOOL CElmoDriver::NeedsParam(UINT uTable)
{
	switch( uTable ) {

		case CIL:
		case CAN:
		case CIB:
		case CIF:
		case COB:
		case COC:
		case COL:
		case CDV:
		case CSN:
		case CAB:
		case CYA:
		case CAG:
		case CAS:
		case CBP:
		case CCA:
		case CCL:
		case CEF:
		case CEM:
		case CET:
		case CFF:
		case CFR:
		case CHM:
		case CHY:
		case CMP:
		case CPL:
		case CPW:
		case CQP:
		case CQT:
		case CQV:
		case CTR:
		case CVH:
		case CVL:
		case CXM:
		case CYM:
		case CGS:
		case CKG:
		case CKR:
		case CKI:
		case CKP:
		case CKV:
		case CXA:
		case CXP:
		case CER:
		case CHL:
		case CLL:
		case CBH:
		case CRP:
		case CRV:
		case CUF:
		case CUI:
		case CWI:
		case CWS:
		case CZX:
			return TRUE;
		}

	return FALSE;
	}

void CElmoDriver::SetListSelect(UINT * pSelect)
{
	m_uListSel = *pSelect;
	}



//////////////////////////////////////////////////////////////////////////
//
// Elmo Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CElmoAddrDialog, CStdAddrDialog);
		
// Constructor

CElmoAddrDialog::CElmoAddrDialog(CElmoDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fPart = fPart;

	if(	 m_uAllowSpace < HMO  ||
		(m_uAllowSpace > HAZ) ||
		(m_uAllowSpace > HGC && m_uAllowSpace < HAA)
		) {

			m_uAllowSpace = HMO;
			m_uListSel    = TMF;
		}

	else {
		m_uListSel = TAZ;
		}

	m_pDriverData = &Driver;

	m_KWText  = "";
	}

// Message Map

AfxMessageMap(CElmoAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CElmoAddrDialog)
	};

// Message Handlers

BOOL CElmoAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	m_uListSel = Addr.a.m_Extra ? TAZ : TMF;

	SetAllow();

	if( m_pSpace ) m_fListTypeChange = IsHeaderSpace(m_pSpace->m_uTable);

	else m_fListTypeChange = TRUE;

	BOOL fShow = FALSE;
	
	if( !m_fPart ) {

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			if( !Addr.m_Ref ) m_pSpace->GetMinimum(Addr);

			ShowDetails();

			ShowAddress(Addr);

			if( Addr.m_Ref ) fShow = ShowOK(Addr.a.m_Table);
			}

		GetDlgItem(2002).EnableWindow(fShow);

		SetDlgFocus(fShow ? 2002 : 1001);

		return !(BOOL(m_pSpace));
		}
	else {
		if( m_pSpace && !IsHeaderSpace(m_pSpace->m_uTable) ) {

			LoadType();

			ShowAddress(Addr);

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				Addr.a.m_Extra = m_uListSel == TMF ? 0 : 1;

				ShowAddress(Addr);

				if( NeedsParam(m_pSpace->m_uTable) ) ShowDetails();

				fShow = ShowOK(Addr.a.m_Table);
				}
			}

		GetDlgItem(2002).EnableWindow(fShow);

		return FALSE;
		}
	}

// Notification Handlers

void CElmoAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CElmoAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			UINT uHead = m_pSpace->m_uTable;

			m_fListTypeChange = FALSE;

			if( IsHeaderSpace(uHead) ) {

				m_uListSel = GetListSelFromHeader(uHead);

				m_pDriverData->SetListSelect(&m_uListSel);

				if( IsListTypeChange(uHead) ) {

					if( NeedDefaultSpace(uHead) ) SelectDefaultSpace();

					m_fListTypeChange = TRUE;
					}

				else m_uAllowSpace = uHead;

				LoadList();
				}

			CString sOffset = GetAddressText();

			CAddress Addr;

			m_pSpace->GetMinimum(Addr);

			Addr.a.m_Type = m_pSpace->m_uType;

			GetDlgItem(2001).SetWindowText( m_pSpace->m_Prefix );

			// cppcheck-suppress redundantAssignment

			sOffset = m_pSpace->GetValueAsText(m_pSpace->m_uMinimum);

			UINT t = m_pSpace->m_uTable;

			if( NeedsParam(t) || t == TKW ) GetDlgItem(2002).SetWindowText(sOffset);

			ShowDetails();

			ShowAddress(Addr);

			GetDlgItem(2002).EnableWindow(ShowOK(m_pSpace->m_uTable));

			SetDlgFocus(1001);

			return;
			}

		CAddress Addr;

		Addr.a.m_Table  = m_pSpace->m_uTable;
		Addr.a.m_Extra  = m_uListSel == TMF ? 0 : 1;
		Addr.a.m_Type   = m_pSpace->m_uType;
		Addr.a.m_Offset = tatoi(GetAddressText());

		if( NeedsParam(m_pSpace->m_uTable) ) {

			GetDlgItem(2002).EnableWindow(TRUE);
			SetDlgFocus(2002);

			ShowDetails();
			return;
			}
		}
	else {
		SelectDefaultSpace();

		ClearDetails();

		ClearAddress();

		m_pSpace = NULL;
		}
	}

BOOL CElmoAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( IsHeaderSpace(m_pSpace->m_uTable) ) {

			if( m_fPart ) return FALSE;

			SetAllow();
			LoadList();

			return TRUE;
			}

		if( m_pSpace->m_uTable == TKW ) {

			m_fListTypeChange = TRUE;

			m_KWPrev = m_uAllowSpace;

			m_KWText = GetDlgItem(2002).GetWindowText();

			if( m_KWText.IsEmpty() ) m_KWText = "a";

			m_KWText.MakeLower();

			m_uListSel = TKW;

			LoadList();

			return TRUE;
			}

		CString Text = m_pSpace->m_Prefix;

		Text += GetDlgItem(2002).GetWindowText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			Addr.a.m_Extra = m_uListSel == TMF ? 0 : 1;

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Load List Functions

void CElmoAddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 40, 100, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf("<%s>\t%s", CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING);

	INDEX Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	if( List.GetCount() ) {

		DoDesignatedHeader(&ListBox);

		Find = DoSelections(&ListBox);

		DoRemainingHeaders(&ListBox);
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSpaceChange(1001, ListBox);
	}

void CElmoAddrDialog::LoadEntry(CListBox * pBox, CString Prefix, CString Caption, DWORD n)
{
	CString Entry;

	Entry.Printf("%s\t%s", Prefix, Caption);

	pBox->AddString(Entry, n);
	}

void CElmoAddrDialog::DoDesignatedHeader(CListBox * pBox )
{
	if( m_fListTypeChange && m_KWText && m_KWText[0] ) return;

	CSpaceList &List = m_pDriver->GetSpaceList();

	INDEX n = List.GetHead();

	BOOL fListTypeChange = FALSE;

	if( m_pSpace ) {

		fListTypeChange = m_pSpace->m_uTable == TAZ || m_pSpace->m_uTable == TMF;
		}

	while( !(List.Failed(n)) ) {

		CSpace * p;

		if( (p = List[n]) ) {

			if( p->m_uTable == m_uAllowSpace ) {

				if( p == m_pSpace || fListTypeChange ) {

					m_fListTypeChange = TRUE;
				}

				LoadEntry(pBox, p->m_Prefix, p->m_Caption, DWORD(n));

				return;
				}
			}

		List.GetNext(n);
		}
	}

INDEX CElmoAddrDialog::DoSelections(CListBox * pBox)
{
	INDEX n1 = INDEX(NOTHING);

	switch( m_uListSel ) {

		case TMF:	n1 = DoFunct(pBox);	break;
		case TAZ:	n1 = DoAlpha(pBox);	break;
		case TKW:	n1 = DoKeyWd(pBox);	break;
		}

	return n1;
	}

INDEX CElmoAddrDialog::DoFunct(CListBox * pBox)
{
	CSpaceList &List = m_pDriver->GetSpaceList();

	CSpace * pSpace;

	INDEX Find = INDEX(NOTHING);

	INDEX n = List.GetHead();

	while( !List.Failed(n) ) { // do Selections

		pSpace = List[n];

		UINT u    = pSpace->m_uTable;

		BOOL f    = FALSE;

		switch( m_uAllowSpace ) {

			case HMO:	f = IsHMO(u);	break;
			case HIO:	f = IsHIO(u);	break;
			case HST:	f = IsHST(u);	break;
			case HFB:	f = IsHFB(u);	break;
			case HCG:	f = IsHCG(u);	break;
			case HCF:	f = IsHCF(u);	break;
			case HPR:	f = IsHPR(u);	break;
			case HDR:	f = IsHDR(u);	break;
			case HUP:	f = IsHUP(u);	break;
			case HGC:	f = IsHGC(u);	break;
			}


		if( f ) {

			LoadEntry( pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n) );

			if( pSpace == m_pSpace ) Find = n;

			else {
				if( m_fListTypeChange ) {

				     	Find = n;

					m_fListTypeChange = FALSE;
					}
				}
			}

		List.GetNext(n);
		}

	return Find;
	}

INDEX CElmoAddrDialog::DoAlpha(CListBox * pBox)
{
	CSpaceList &List = m_pDriver->GetSpaceList();

	CSpace * pSpace;

	INDEX Find = INDEX(NOTHING);

	INDEX n = List.GetHead();

	BYTE  b = LOBYTE(m_uAllowSpace - UINT(AOFF));

	while( !List.Failed(n) ) {

		pSpace = List[n];

		CString p = pSpace->m_Prefix;

		if( BYTE(p[0]) == b ) {

			LoadEntry( pBox, p, pSpace->m_Caption, DWORD(n) );

			if( pSpace == m_pSpace ) Find = n;

			else {
				if( m_fListTypeChange ) {

					Find = n;

					m_fListTypeChange = FALSE;
					}
				}
			}

		List.GetNext(n);
		}

	return Find;
	}

INDEX CElmoAddrDialog::DoKeyWd(CListBox * pBox)
{
	CSpaceList &List = m_pDriver->GetSpaceList();

	CSpace * pSpace;

	INDEX Find = INDEX(NOTHING);

	INDEX n = List.GetHead();

	while( !List.Failed(n) ) {

		pSpace = List[n];

		if( !IsHeaderSpace(pSpace->m_uTable) ) {

			CString p = pSpace->m_Prefix;
			CString c = pSpace->m_Caption;

			c.MakeLower();

			if( c.Find(m_KWText) != NOTHING ) {

				LoadEntry( pBox, p, pSpace->m_Caption, DWORD(n) ); // restore case

				if( m_fListTypeChange ) {

					Find = n;

					if( m_KWPrev >= HAA && m_KWPrev <= HAZ ) {

						m_uListSel    = TAZ;
						m_uAllowSpace = HAA + AOFF;
						}

					else {
						m_uListSel    = TMF;

						CSpace * pS;

						pS = m_pSpace;

						m_pSpace = pSpace;

						SetAllow();

						m_pSpace = pS;
						}

					m_fListTypeChange = FALSE;
					}
				}
			}

		List.GetNext(n);
		}

	return Find;
	}

void CElmoAddrDialog::DoRemainingHeaders(CListBox * pBox)
{
	CSpaceList &List = m_pDriver->GetSpaceList();

	CSpace * pSpace;

	INDEX n = List.GetHead();

	CString p = "";

	BOOL fKeyList = m_KWText && m_KWText[0];

	while( !List.Failed(n) ) {

		pSpace = List[n];

		UINT t = pSpace->m_uTable;

		if( IsHeaderSpace(t) && (t != m_uAllowSpace || fKeyList) ) {

			BOOL fShow = FALSE;

			BOOL fType = IsListTypeChange(t) && t != TKW;

			switch( m_uListSel ) {

				case TAZ:

					fShow = IsAlphaHeader(t) || ((t != TAZ) && fType);
					break;

				case TMF:
					fShow = IsFunctHeader(t) || ((t != TMF) && fType);
					break;
				}

			if( fShow ) LoadEntry( pBox, p, pSpace->m_Caption, DWORD(n) );
			}

		List.GetNext(n);
		}

	if( fKeyList ) m_KWText = "";

	n = List.GetHead();

	while( !List.Failed(n) ) {

		pSpace = List[n];

		if( pSpace->m_uTable == TKW ) {

			LoadEntry( pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n) );
			}

		List.GetNext(n);
		}
	}

// Helpers

void CElmoAddrDialog::SetAllow(void)
{
	if( !m_pSpace ) {

		SelectDefaultSpace();
		return;
		}

	m_uAllowSpace = HMO;

	switch( m_uListSel ) {

		case TAZ:
			m_uAllowSpace = m_pSpace->m_Prefix[0] + AOFF;
			return;

		case TMF:
			switch( m_pSpace->m_uTable/10 ) {

				case 0:
				case 1:		m_uAllowSpace = HMO;	return;

				case 2:		m_uAllowSpace = HIO;	return;

				case 3:
				case 4:		m_uAllowSpace = HST;	return;

				case 5:
				case 6:		m_uAllowSpace = HFB;	return;

				case 7:
				case 8:
				case 9:		m_uAllowSpace = HCG;	return;

				case 11:	m_uAllowSpace = HCF;	return;

				case 12:	m_uAllowSpace = HPR;	return;

				case 13:	m_uAllowSpace = HDR;	return;

				case 14:	m_uAllowSpace = HUP;	return;

				case 15:	m_uAllowSpace = HGC;	return;
				}
		}
	}

BOOL CElmoAddrDialog::NeedDefaultSpace(UINT uHead)
{
	if( IsAlphaHeader(m_uAllowSpace) ) return !IsListChoice(TAZ);

	if( IsFunctHeader(m_uAllowSpace) ) return !IsListChoice(TMF);

	return TRUE;
	}

void CElmoAddrDialog::SelectDefaultSpace(void)
{
	m_uAllowSpace = m_uListSel == TAZ ? HAA : HMO;
	}

BOOL CElmoAddrDialog::IsHeaderSpace(UINT uHead)
{
	return uHead >= HMO && uHead < TKW;
	}

BOOL CElmoAddrDialog::IsAlphaHeader(UINT uHead)
{
	return uHead >= HAA && uHead <= HAZ;
	}

BOOL CElmoAddrDialog::IsFunctHeader(UINT uHead)
{
	return uHead >= HMO && uHead <= HGC;
	}

BOOL CElmoAddrDialog::IsListChoice(UINT uSel)
{
	return m_uListSel == uSel;
	}

BOOL CElmoAddrDialog::IsListTypeChange(UINT uSel)
{
	return uSel == TMF || uSel == TAZ;
	}

BOOL CElmoAddrDialog::ShowOK(UINT uTable)
{
	return NeedsParam(uTable) || uTable == TKW;
	}

UINT CElmoAddrDialog::GetListSelFromHeader(UINT uHeader)
{
	if( uHeader == TMF || (uHeader >= HMO && uHeader <= HGC) ) return TMF;

	if( uHeader == TAZ || (uHeader >= HAA && uHeader <= HAZ) ) return TAZ;

	return TKW;
	}

BOOL CElmoAddrDialog::IsHMO(UINT uTable)
{
	return uTable < 20;
	}

BOOL CElmoAddrDialog::IsHIO(UINT uTable)
{
	return uTable > 20 && uTable < 30;
	}

BOOL CElmoAddrDialog::IsHST(UINT uTable)
{
	return uTable > 30 && uTable < 50;
	}

BOOL CElmoAddrDialog::IsHFB(UINT uTable)
{
	return uTable > 50 && uTable < 70;
	}

BOOL CElmoAddrDialog::IsHCG(UINT uTable)
{
	return uTable > 70 && uTable < 110;
	}

BOOL CElmoAddrDialog::IsHCF(UINT uTable)
{
	return uTable > 110 && uTable < 120;
	}

BOOL CElmoAddrDialog::IsHPR(UINT uTable)
{
	return uTable > 120 && uTable < 130;
	}

BOOL CElmoAddrDialog::IsHDR(UINT uTable)
{
	return uTable > 130 && uTable < 140;
	}

BOOL CElmoAddrDialog::IsHUP(UINT uTable)
{
	return uTable > 140 && uTable < 150;
	}

BOOL CElmoAddrDialog::IsHGC(UINT uTable)
{
	return uTable > 150 && uTable < 170;
	}

BOOL CElmoAddrDialog::NeedsParam(UINT uTable)
{
	return m_pDriverData->NeedsParam(uTable);
	}

// End of File
