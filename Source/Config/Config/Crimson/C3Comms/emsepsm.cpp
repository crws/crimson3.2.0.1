#include "intern.hpp"

#include "emsonep.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

// Pre-mapped
void CEmersonEPDriver::AddSpacesSM15(void)
{
	if( m_fM15Loaded ) return;

	AddSpace(New CSpace(M15, "15_",    "   SOL_MODULE_MENU_15",		10,     2,    60, WW,LL));
	AddSpace(New CSpace(AN, "15_50_",  "   SOL_MODULE_ERROR_STATUS",	10, 51550, 51550, WW   ));
	AddSpace(New CSpace(AN, "15_01_",  "   SOL_MODULE_ID_CODE",		10, 51501, 51501, WW   ));

	m_fM15Loaded = TRUE;
	}

void CEmersonEPDriver::AddSpacesSM16(void)
{
	if( m_fM16Loaded ) return;

	AddSpace(New CSpace(M16, "16_",    "   SOL_MODULE_MENU_16",		10,     2,    60, WW,LL));
 	AddSpace(New CSpace(AN,  "16_50_", "   SOL_MODULE_ERROR_STATUS",	10, 51650, 51650, WW   ));
	AddSpace(New CSpace(AN,  "16_01_", "   SOL_MODULE_ID_CODE",		10, 51601, 51601, WW   ));

	m_fM16Loaded = TRUE;
	}

void CEmersonEPDriver::AddSpacesSM17(void)
{
	if( m_fM17Loaded ) return;

	AddSpace(New CSpace(M17, "17_",    "   SOL_MODULE_MENU_17",		10,     2,    60, WW,LL));
 	AddSpace(New CSpace(AN,  "17_50_", "   SOL_MODULE_ERROR_STATUS",	10, 51750, 51750, WW   ));
	AddSpace(New CSpace(AN,  "17_01_", "   SOL_MODULE_ID_CODE",		10, 51701, 51701, WW   ));

	m_fM17Loaded = TRUE;
	}	

void CEmersonEPDriver::AddSpacesMC(void)
{
	if( m_fMCLoaded ) return;

	AddSpace(New CSpace(TMCH, "",     "Motion Coordinator Selections",	10,     0,     0,    LL));
	AddSpace(New CSpace( MCO, "MCOP", "   Motion Coordinator Output Bit",	10,     0,  9998,    BB));
	AddSpace(New CSpace( MCI, "MCIN", "   Motion Coordinator Input Bit",	10,     0,  9998,    BB));
	AddSpace(New CSpace( MCH, "MCVR", "   Motion Coordinator Register",	10,     0,  9998,    WW));

	m_fMCLoaded = TRUE;
	}

// End of File
