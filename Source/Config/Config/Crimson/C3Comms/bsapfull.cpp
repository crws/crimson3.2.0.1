
#include "intern.hpp"

#include "bsapfull.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLUDPDriverOptions, CUIItem);

// Constructor

CBSAPFULLUDPDriverOptions::CBSAPFULLUDPDriverOptions(void)
{
	m_Local  = 1;
	m_Global = 100;
	m_PollH  = 2;
	m_Level  = 0;

	m_Level1 = 7;
	m_Level2 = 7;
	m_Level3 = 1;
	m_Level4 = 0;
	m_Level5 = 0;
	m_Level6 = 0;

	m_IPAddr = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Socket = 1234;

	m_Count  = 2;
	}

// UI Management

void CBSAPFULLUDPDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Level" || Tag == "Global" ) {

		if( (BOOL)m_Level ) {

			m_Local  = max(m_Local, 1);
			m_Global = max(m_Global, m_Level * 100);
			}

		else {
			m_Local  = 0;
			m_Global = 0;
			}

		pWnd->UpdateUI("Local");
		pWnd->UpdateUI("Global");

		pWnd->EnableUI("Local", (BOOL)m_Level);
		pWnd->EnableUI("Global", (BOOL)m_Level);

		CheckLevelCounts();

		UpdateLevels(pWnd);
		}

	if(     Tag.IsEmpty() ||
		Tag == "Level1" ||
		Tag == "Level2" ||
		Tag == "Level3" ||
		Tag == "Level4" ||
		Tag == "Level5" ||
		Tag == "Level6"
		) {

		if( !m_Level ) m_Level1 = max(1, m_Level1);

		CheckLevelCounts();

		UpdateLevels(pWnd);

		pWnd->UpdateUI("Level");
		}

	if( Tag.IsEmpty() || Tag == "Local" || Tag == "PollH" ) {

		m_PollH = max(1, m_PollH);

		if( m_Local == m_PollH ) {

			m_PollH++;
			}

		pWnd->UpdateUI("PollH");
		}
	}

// Download Support

BOOL CBSAPFULLUDPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Level));
	Init.AddByte(BYTE(m_Local));
	Init.AddWord(WORD(m_Global));
	Init.AddByte(BYTE(m_PollH));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level1))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level2))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level3))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level4))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level5))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level6))));
	Init.AddLong(LONG(m_IPAddr));
	Init.AddWord(WORD(m_Socket));
	Init.AddWord(WORD(m_Count));

	return TRUE;
	}

// Meta Data Creation

void CBSAPFULLUDPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	
	Meta_AddInteger(Level);
	Meta_AddInteger(Local);
	Meta_AddInteger(Global);
	Meta_AddInteger(PollH);
	Meta_AddInteger(Level1);
	Meta_AddInteger(Level2);
	Meta_AddInteger(Level3);
	Meta_AddInteger(Level4);
	Meta_AddInteger(Level5);
	Meta_AddInteger(Level6);
	Meta_AddInteger(IPAddr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Count);
	}

BOOL CBSAPFULLUDPDriverOptions::CheckLevelCounts(void) {

	UINT uMin = 6;

	if( !m_Level5 ) { m_Level = min(m_Level, 3); uMin = 5; }
	if( !m_Level4 ) { m_Level = min(m_Level, 2); uMin = 4; }
	if( !m_Level3 ) { m_Level = min(m_Level, 1); uMin = 3; }
	if( !m_Level2 ) { m_Level = min(m_Level, 0); uMin = 2; }
	if( !m_Level1 ) { m_Level1 = 1; m_Level = 0; uMin = 1; }

	ClearLowerLevels(uMin);

	if( !m_Level ) m_Level1 = max( 1, m_Level1 );

	UINT uCurrent = m_Level1 + m_Level2;

	UINT k = 3;

	while( k < 7 ) {

		UINT *pLvl = &m_Level3;

		switch( k ) {

			case 4: pLvl = &m_Level4; break;
			case 5: pLvl = &m_Level5; break;
			case 6: pLvl = &m_Level6; break;
			}

		if( !(BOOL)*pLvl || CheckBitCount(uCurrent, pLvl) ) {	// reached maximum

			ClearLowerLevels(k);

			return TRUE;
			}

		else {
			uCurrent += *pLvl;
			}

		k++;
		}

	return FALSE;
	}

void CBSAPFULLUDPDriverOptions::UpdateLevels(CUIViewWnd *pWnd) {

	pWnd->UpdateUI("Level1");
	pWnd->UpdateUI("Level2");
	pWnd->UpdateUI("Level3");
	pWnd->UpdateUI("Level4");
	pWnd->UpdateUI("Level5");
	pWnd->UpdateUI("Level6");

	m_Local = m_Level ? max(1, m_Local) : 0;

	pWnd->UpdateUI("Local");
	}

BOOL CBSAPFULLUDPDriverOptions::CheckBitCount(UINT uCurrent, UINT *pNew) {

	UINT uNew  = *pNew;

	while( uNew + uCurrent > 15 ) {

		uNew--;
		}

	*pNew = uNew;

	return uNew + uCurrent == 15;
	}

BOOL CBSAPFULLUDPDriverOptions::CheckClearLower(UINT uThis) {

	switch( uThis ) {

		case 2:	if( !m_Level2 ) ClearLowerLevels(2); return TRUE;
		case 3:	if( !m_Level3 ) ClearLowerLevels(3); return TRUE;
		case 4:	if( !m_Level4 ) ClearLowerLevels(4); return TRUE;
		case 5:	if( !m_Level5 ) ClearLowerLevels(5); return TRUE;
		}

	return FALSE;
	}

void CBSAPFULLUDPDriverOptions::ClearLowerLevels(UINT uThis) {

	switch( uThis ) {

		case 1:	m_Level2 = 0;
		case 2:	m_Level3 = 0;
		case 3: m_Level4 = 0;
		case 4: m_Level5 = 0;
		case 5: m_Level6 = 0;
		}
	}

BYTE CBSAPFULLUDPDriverOptions::MakeLevelMask(BYTE bLevel) {

	switch( bLevel ) {	// make actual runtime mask

		case 1: return 1;
		case 2: return 3;
		case 3: return 7;
		case 4: return 15;
		case 5: return 31;
		case 6: return 63;
		case 7: return 127;
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLSerialDriverOptions, CUIItem);

// Constructor

CBSAPFULLSerialDriverOptions::CBSAPFULLSerialDriverOptions(void)
{
	m_Local  = 1;
	m_Global = 100;
	m_PollH  = 2;
	m_Level  = 0;

	m_Level1 = 7;
	m_Level2 = 7;
	m_Level3 = 1;
	m_Level4 = 0;
	m_Level5 = 0;
	m_Level6 = 0;
	}

// UI Management

void CBSAPFULLSerialDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Level" || Tag == "Global" ) {

		if( (BOOL)m_Level ) {

			m_Local  = max(m_Local, 1);
			m_Global = max(m_Global, m_Level * 100);
			}

		else {
			m_Local  = 0;
			m_Global = 0;
			}

		pWnd->UpdateUI("Local");
		pWnd->UpdateUI("Global");

		pWnd->EnableUI("Local", (BOOL)m_Level);
		pWnd->EnableUI("Global", (BOOL)m_Level);

		CheckLevelCounts();

		UpdateLevels(pWnd);
		}

	if(     Tag.IsEmpty() ||
		Tag == "Level1" ||
		Tag == "Level2" ||
		Tag == "Level3" ||
		Tag == "Level4" ||
		Tag == "Level5" ||
		Tag == "Level6"
		) {

		if( !m_Level ) m_Level1 = max(1, m_Level1);

		CheckLevelCounts();

		UpdateLevels(pWnd);

		pWnd->UpdateUI("Level");
		}

	if( Tag.IsEmpty() || Tag == "Local" || Tag == "PollH" ) {

		m_PollH = max(1, m_PollH);

		if( m_Local == m_PollH ) {

			m_PollH++;
			}

		pWnd->UpdateUI("PollH");
		}
	}

// Download Support

BOOL CBSAPFULLSerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Level));
	Init.AddByte(BYTE(m_Local));
	Init.AddWord(WORD(m_Global));
	Init.AddByte(BYTE(m_PollH));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level1))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level2))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level3))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level4))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level5))));
	Init.AddByte(BYTE(MakeLevelMask(LOBYTE(m_Level6))));

	return TRUE;
	}

// Meta Data Creation

void CBSAPFULLSerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Level);
	Meta_AddInteger(Local);
	Meta_AddInteger(Global);
	Meta_AddInteger(PollH);
	Meta_AddInteger(Level1);
	Meta_AddInteger(Level2);
	Meta_AddInteger(Level3);
	Meta_AddInteger(Level4);
	Meta_AddInteger(Level5);
	Meta_AddInteger(Level6);
	}

BOOL CBSAPFULLSerialDriverOptions::CheckLevelCounts(void) {

	UINT uMin = 6;

	if( !m_Level5 ) { m_Level = min(m_Level, 3); uMin = 5; }
	if( !m_Level4 ) { m_Level = min(m_Level, 2); uMin = 4; }
	if( !m_Level3 ) { m_Level = min(m_Level, 1); uMin = 3; }
	if( !m_Level2 ) { m_Level = min(m_Level, 0); uMin = 2; }
	if( !m_Level1 ) { m_Level1 = 1; m_Level = 0; uMin = 1; }

	ClearLowerLevels(uMin);

	m_Level1 = max( 1, m_Level1 );

	UINT uCurrent = m_Level1 + m_Level2;

	UINT k = 3;

	while( k < 7 ) {

		UINT *pLvl = &m_Level3;

		switch( k ) {

			case 4: pLvl = &m_Level4; break;
			case 5: pLvl = &m_Level5; break;
			case 6: pLvl = &m_Level6; break;
			}

		if( !(BOOL)*pLvl || CheckBitCount(uCurrent, pLvl) ) {	// reached maximum

			ClearLowerLevels(k);

			return TRUE;
			}

		else {
			uCurrent += *pLvl;
			}

		k++;
		}

	return FALSE;
	}


void CBSAPFULLSerialDriverOptions::UpdateLevels(CUIViewWnd *pWnd) {

	pWnd->UpdateUI("Level1");
	pWnd->UpdateUI("Level2");
	pWnd->UpdateUI("Level3");
	pWnd->UpdateUI("Level4");
	pWnd->UpdateUI("Level5");
	pWnd->UpdateUI("Level6");

	m_Local = m_Level ? max(1, m_Local) : 0;

	pWnd->UpdateUI("Local");
	}

BOOL CBSAPFULLSerialDriverOptions::CheckBitCount(UINT uCurrent, UINT *pNew) {

	UINT uNew  = *pNew;

	while( uNew > 15 - uCurrent ) {

		uNew--;
		}

	*pNew = uNew;

	return uNew + uCurrent == 15;
	}

BOOL CBSAPFULLSerialDriverOptions::CheckClearLower(UINT uThis) {

	switch( uThis ) {

		case 2:	if( !m_Level2 ) ClearLowerLevels(2); return TRUE;
		case 3:	if( !m_Level3 ) ClearLowerLevels(3); return TRUE;
		case 4:	if( !m_Level4 ) ClearLowerLevels(4); return TRUE;
		case 5:	if( !m_Level5 ) ClearLowerLevels(5); return TRUE;
		}

	return FALSE;
	}

void CBSAPFULLSerialDriverOptions::ClearLowerLevels(UINT uThis) {

	switch( uThis ) {

		case 1: m_Level2 = 0;
		case 2:	m_Level3 = 0;
		case 3: m_Level4 = 0;
		case 4: m_Level5 = 0;
		case 5: m_Level6 = 0;
		}
	}

BYTE CBSAPFULLSerialDriverOptions::MakeLevelMask(BYTE bLevel) {

	switch( bLevel ) {	// make actual runtime mask

		case 1: return 1;
		case 2: return 3;
		case 3: return 7;
		case 4: return 15;
		case 5: return 31;
		case 6: return 63;
		case 7: return 127;
		}

	return 0;
	} 

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLUDPSDriverOptions, CUIItem);

// Constructor

CBSAPFULLUDPSDriverOptions::CBSAPFULLUDPSDriverOptions(void)
{
	m_Level  = 1;
	m_Local  = 1;
	m_Global = 100;

	m_IPAddr = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Socket = 1234;

	m_Count  = 2;
	}

// UI Management

void CBSAPFULLUDPSDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CBSAPFULLUDPSDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Level));
	Init.AddByte(BYTE(m_Local));
	Init.AddWord(WORD(m_Global));
	Init.AddLong(LONG(m_IPAddr));
	Init.AddWord(WORD(m_Socket));
	Init.AddWord(WORD(m_Count));

	return TRUE;
	}

// Meta Data Creation

void CBSAPFULLUDPSDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Level);
	Meta_AddInteger(Local);
	Meta_AddInteger(Global);
	Meta_AddInteger(IPAddr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Count);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLSerialSDriverOptions, CUIItem);

// Constructor

CBSAPFULLSerialSDriverOptions::CBSAPFULLSerialSDriverOptions(void)
{
	m_Level  = 1;
	m_Local  = 1;
	m_Global = 100;
	}

// UI Management

void CBSAPFULLSerialSDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CBSAPFULLSerialSDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Local));
	Init.AddWord(WORD(m_Global));
	Init.AddByte(BYTE(m_Level));

	return TRUE;
	}

// Meta Data Creation

void CBSAPFULLSerialSDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Level);
	Meta_AddInteger(Local);
	Meta_AddInteger(Global);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags UDP Master Driver
//

// Constructor

CBSAPFULLTagsUDPDriver::CBSAPFULLTagsUDPDriver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "Bristol Babcock BSAP";
	
	m_DriverName	= "Extended UDP Master";
	
	m_Version	= "1.00";

	m_DevRoot	= "CW";
	
	m_ShortName	= "Extended UDP Master";
	}

// Address Management

BOOL CBSAPFULLTagsUDPDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CBSAPFULLTagsUDPDeviceOptions *) pConfig)->ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsUDPDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CBSAPFULLTagsUDPDeviceOptions *) pConfig)->ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsUDPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CBSAPFULLTagsUDPDeviceOptions *) pConfig)->SelectAddress(hWnd, Addr, fPart);
	}

BOOL CBSAPFULLTagsUDPDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CBSAPFULLTagsUDPDeviceOptions *) pConfig)->ListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Master Driver
//

// Instantiator

ICommsDriver *	Create_BSAPFULLUDPDriver(void)
{
	return New CBSAPFULLUDPDriver;
	}

// Constructor

CBSAPFULLUDPDriver::CBSAPFULLUDPDriver(void)
{
	m_wID		= 0x3543;
	}

// Binding Control

UINT CBSAPFULLUDPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBSAPFULLUDPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CBSAPFULLUDPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLUDPDriverOptions);
	}

// Configuration

CLASS CBSAPFULLUDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLUDPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags Serial Master Driver
//

// Constructor

CBSAPFULLTagsSerialDriver::CBSAPFULLTagsSerialDriver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "Bristol Babcock BSAP";
	
	m_DriverName	= "Extended Serial Master";
	
	m_Version	= "1.01";

	m_DevRoot	= "CW";
	
	m_ShortName	= "BSAP Ext Master";
	}

// Address Management

BOOL CBSAPFULLTagsSerialDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CBSAPFULLTagsSerialDeviceOptions *) pConfig)->ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsSerialDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CBSAPFULLTagsSerialDeviceOptions *) pConfig)->ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CBSAPFULLTagsSerialDeviceOptions *) pConfig)->SelectAddress(hWnd, Addr, fPart);
	}

BOOL CBSAPFULLTagsSerialDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CBSAPFULLTagsSerialDeviceOptions *) pConfig)->ListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Master Driver
//

// Instantiator

ICommsDriver *	Create_BSAPFULLSerialDriver(void)
{
	return New CBSAPFULLSerialDriver;
	}

// Constructor

CBSAPFULLSerialDriver::CBSAPFULLSerialDriver(void)
{
	m_wID		= 0x40A2;
	}

// Binding Control

UINT	CBSAPFULLSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CBSAPFULLSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CBSAPFULLSerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLSerialDriverOptions);
	}

// Configuration

CLASS CBSAPFULLSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLSerialDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags UDP Slave Driver
//

// Constructor

CBSAPFULLTagsUDPSDriver::CBSAPFULLTagsUDPSDriver(void)
{
	m_uType		= driverSlave;
	
	m_Manufacturer	= "Bristol Babcock BSAP";
	
	m_DriverName	= "Extended UDP Slave";
	
	m_Version	= "1.00";

	m_DevRoot	= "CW";
	
	m_ShortName	= "Extended UDP Slave";
	}

// Address Management

BOOL CBSAPFULLTagsUDPSDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CBSAPFULLTagsUDPSDeviceOptions *) pConfig)->ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsUDPSDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CBSAPFULLTagsUDPSDeviceOptions *) pConfig)->ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsUDPSDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CBSAPFULLTagsUDPSDeviceOptions *) pConfig)->SelectAddress(hWnd, Addr, fPart);
	}

BOOL CBSAPFULLTagsUDPSDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CBSAPFULLTagsUDPSDeviceOptions *) pConfig)->ListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Slave Driver
//

// Instantiator

ICommsDriver *	Create_BSAPFULLUDPSDriver(void)
{
	return New CBSAPFULLUDPSDriver;
	}

// Constructor

CBSAPFULLUDPSDriver::CBSAPFULLUDPSDriver(void)
{
	m_wID		= 0x3544;
	}

// Binding Control

UINT CBSAPFULLUDPSDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBSAPFULLUDPSDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CBSAPFULLUDPSDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLUDPSDriverOptions);
	}

// Configuration

CLASS CBSAPFULLUDPSDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLUDPSDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags Serial Slave Driver
//

// Constructor

CBSAPFULLTagsSerialSDriver::CBSAPFULLTagsSerialSDriver(void)
{
	m_uType		= driverSlave;
	
	m_Manufacturer	= "Bristol Babcock BSAP";
	
	m_DriverName	= "Extended Serial Slave";
	
	m_Version	= "1.01";

	m_DevRoot	= "CW";
	
	m_ShortName	= "BSAP Ext Slave";
	}

// Address Management

BOOL CBSAPFULLTagsSerialSDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CBSAPFULLTagsSerialSDeviceOptions *) pConfig)->ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsSerialSDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CBSAPFULLTagsSerialSDeviceOptions *) pConfig)->ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsSerialSDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CBSAPFULLTagsSerialSDeviceOptions *) pConfig)->SelectAddress(hWnd, Addr, fPart);
	}

BOOL CBSAPFULLTagsSerialSDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CBSAPFULLTagsSerialSDeviceOptions *) pConfig)->ListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Driver
//

// Instantiator

ICommsDriver *	Create_BSAPFULLSerialSDriver(void)
{
	return New CBSAPFULLSerialSDriver;
	}

// Constructor

CBSAPFULLSerialSDriver::CBSAPFULLSerialSDriver(void)
{
	m_wID		= 0x3410;
	}

// Binding Control

UINT	CBSAPFULLSerialSDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CBSAPFULLSerialSDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CBSAPFULLSerialSDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLSerialSDriverOptions);
	}

// Configuration

CLASS CBSAPFULLSerialSDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBSAPFULLSerialSDeviceOptions);
	}

// End of File
