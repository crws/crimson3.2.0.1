//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_J1939_HPP
	
#define	INCLUDE_J1939_HPP

/////////////////////////////////////////////////////////////////////////
//
// J1939 Constants
//

#define SPN_MAX		85

#define UNSPECIFIED	9999

#define BITS		0x80

#define CP_NUM		2002
#define CP_TITLE	2004
#define CP_PRE		2006
#define CP_DIAG		2008
#define CP_PRIOR	2010
#define CP_REP		2012
#define CP_SPNs		2014
#define CP_SPN1		3002
#define CP_SPN2		3004
#define CP_SPN3		3006
#define CP_SPN4		3008
#define CP_SPN5		3010
#define CP_SPN6		3012
#define CP_SPN7		3014
#define CP_SPN8		3016
#define CP_PAGE		3018
#define CP_ADD		3020
#define CP_DEL		3022
#define CP_ENH		2016
#define CP_DIR		2018

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCANJ1939DriverOptions;
class CCANJ1939DeviceOptions;

/////////////////////////////////////////////////////////////////////////
//
// J1939 Types
//

enum J1939Types {

	type1bit	= 1,
	type2bits	= 2,
	type3bits	= 3,
	type4bits	= 4,
	type5bits	= 5,
	type6bits	= 6,
	type7bits	= 7,
	type8bits	= 8,
	type9bits	= 9,
	type10bits	= 10,
	type11bits	= 11,
	type12bits	= 12,
	type1byte	= 13,
	type2bytes	= 14,
	type3bytes	= 15,
	type4bytes	= 16,
	};


/////////////////////////////////////////////////////////////////////////
//
// J1939 Structures
//

struct SPN
{	
	char	m_Number[10];
	char	m_Title[55];
	BYTE    m_Size;
	};

struct PARAM
{
	UINT	m_Index;
	SPN	m_SPN[SPN_MAX];
	}; 

//////////////////////////////////////////////////////////////////////////
//
// J1939 Suspect Parameter
//

class CSPN : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CSPN(void);
		CSPN(SPN Param);
		CSPN(UINT uNumber, UINT uSize, CString DM);

		// Destructor
		~CSPN(void);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Data Access
		UINT    GetSize(void);
		void	SetSize(UINT uSize);
		BOOL	IsDM(void);

		// Public Members
		UINT	  m_Number;
		CString   m_DM;
				
	protected:
		// Data Members
		UINT	  m_Size;
		
		// Meta Data Creation
		void AddMetaData(void);

		// Help
		UINT GetSizeAsBits(UINT uSize);


	};


/////////////////////////////////////////////////////////////////////////
//
// J1939 SPN Storage Class 
//

class CSPNList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSPNList(void);

		// Destructor
		~CSPNList(void);

		// Item Access
		CSPN * GetItem(INDEX Index) const;
		CSPN * GetItem(UINT  uPos ) const;

		// Operations
		CSPN * AppendItem(CSPN * pSPN);

	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Parameter Group
//

class CPGN : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPGN(void);
		CPGN(CPGN * pPGN);
		CPGN(UINT Number, CString Prefix, CString Title, UINT Priority, BOOL SendReq, UINT RepRate);
		CPGN(UINT Number, CString Prefix, CString Title, PARAM Param, UINT Priority, BOOL SendReq, UINT RepRate);

		// Destructor
		~CPGN(void);
	       
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Attributes
		BOOL IsDirty(BOOL fReq, UINT uPriority, UINT uRepRate);
		BOOL IsDM(void);
		BOOL IsThisPGN(UINT uNum, UINT uDir);
		
		
		// SPN List Management
		BOOL	   CreateSPNList(CPGN * pPGN);
		BOOL	   GrowSPNList(UINT uCount);
		CSPNList * GetSPNs(void);

		// Public Data 
		UINT	  m_Number;
		CString	  m_Prefix;
		CString	  m_Title;
		PARAM	  m_Params;
		UINT      m_Priority;
		BOOL	  m_SendReq;
		UINT	  m_RepRate;
		UINT	  m_Table;
		UINT      m_Diag;
		UINT      m_Enhanced;
		UINT	  m_Direction;
		BOOL	  m_fMark;
		UINT	  m_Ref;
		       	
	protected:
		// Data Members
		CSPNList *m_pSPNs;
							
		// Meta Data Creation
		void AddMetaData(void);

		
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Parameter Group List Class
//

class CPGNList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPGNList(void);

		// Destructor
		~CPGNList(void);

		// Item Access
		CPGN * GetItem(INDEX Index) const;
		CPGN * GetItem(UINT  uPos ) const;

		// Operations
		CPGN * AppendItem(CPGN * pPGN);
		INDEX  InsertItem(CPGN * pPGN, CPGN * pBefore);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN J1939 Driver
//

class CCANJ1939Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CCANJ1939Driver(void);

		// Destructor
		~CCANJ1939Driver(void);
		
		// Driver Data
		UINT GetFlags(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Notifications
		void NotifyInit(CItem * pConfig);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem *pDrvCfg);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Data Access
		CPGN*	GetPGN(UINT uIndex);
		CPGN*	GetPGN(CString Text);
		UINT	GetPGNCount(void);
		CString	GetTypeString(CPGN * pPGN, UINT uOffset);
		CString	RemovePGFormat(CString Format);
		UINT    FindPGNIndex(CAddress Addr, CItem * pConfig);
		CPGN *  GetDevicePGN(CAddress Addr, CItem * pConfig);
		BOOL    DeleteDevicePGN(CAddress Addr, CItem *pConfig);
		CPGN *  FindPGN(CString Text);
		CPGN *  FindPGN(UINT uNumber);
		void	SetList(CPGNList * pList);
		CString FindTextFromPrefix(CString Prefix);

		// Overridables
		virtual	CString	GetFullPGString(CPGN * pPGN, BOOL fNum);
		virtual	CString	GetPGString(CPGN * pPGN, UINT uOffset);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem *pDrvCfg);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig,CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem *pDrvCfg);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
				       		
	protected:

		// Data members
		CPGNList * m_pPGNs;
		BOOL	   m_fSet;
		BOOL	   m_fRebuild;
											
		// Implementation
		void  LoadPGNs(CItem *pConfig);
		BOOL  IsTextValid(CString Text, CPGN * pPGN);
		UINT  FindPGNNumber(CAddress Addr);
		CPGN *UpdateUserList(CPGNList * pList, CPGN * pPGN);
		BOOL  IsDead(CPGN * pPGN, UINT uOffset);
		UINT  FindOffset(CPGN * pPGN, UINT uOffset);
		BOOL  CheckRebuild(CCANJ1939DeviceOptions * pOptions, CPGN * pPGN);
       	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver Options	Custom PGN Page
//

class CCANJ1939DriverOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCANJ1939DriverOptionsUIPage(CCANJ1939DriverOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CCANJ1939DriverOptions * m_pOption;
	}; 
 
//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver Options
//

class CCANJ1939DriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANJ1939DriverOptions(void);

		// Persistance
		BOOL Import(CPropValue *pValue);
		BOOL ImportList(CPropValue *pValue);
		
		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);
		
		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// View Creation
		CViewWnd * CreateItemView(void);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Ops
		BOOL	MakePGN(CPGN * pNew, CPGN * pFrom);
		BOOL	RemovePGN(CPGN * PGN);
		CPGN *	GetPGN(CString Text);
		BOOL    IsMfrAssigned(UINT uPGN);
				
		// Config Data
		UINT	   m_Source;
		UINT       m_Source2;
		CPGNList * m_pPGNList;
		UINT	   m_List;
		UINT	   m_Priority;
		UINT	   m_RepRate;
		UINT	   m_SPNs;
		UINT	   m_SPN;
		UINT	   m_Size1;
		UINT	   m_Size2;
		UINT	   m_Size3;
		UINT	   m_Size4;
		UINT	   m_Size5;
		UINT	   m_Size6;
		UINT	   m_Size7;
		UINT	   m_Size8;
		UINT	   m_Diag;
		UINT	   m_Enhanced;
		UINT	   m_Direction;
					
	protected:

		
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Device Options
//

class CCANJ1939DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANJ1939DeviceOptions(void);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		CPGNList * GetPGNList(void);
		CPGN	 * GetPGN(UINT uNumber);
		CPGN	 * GetPGN(CString Text);
		UINT	   GetNextTable(void);
		BOOL	   MakePGN(CPGN * pPGN);
		void	   Rebuild(void);
				     
		// Config Data
		UINT	   m_Drop;
		UINT	   m_BackOff;
		UINT	   m_ClrDTC;
		UINT       m_Source;
			
	protected:

		CPGNList * m_pPGNs;
		UINT	   m_uLast;

		// Implementation
		void ClearTables(void);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Address Selection
//

class CCANJ1939Dialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CCANJ1939Dialog(CCANJ1939Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:

		// Data
		CCANJ1939Driver *	m_pDriver;
		CAddress	*	m_pAddr;
		CItem		*	m_pConfig;
		BOOL			m_fPart;

	
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
	       
		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadPGNs(void);
		void LoadOptions(void);
		void LoadPriority(void);
		void LoadRepRate(void);
		void SetOptions(void);
		void SetPriority(void);
		void SetRepRate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 User Defined PGN Dialog
//

class CCustomPGNDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCustomPGNDialog(CCANJ1939DriverOptions * pOptions);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		BOOL OnCreateEdit(UINT uID);
		BOOL OnRemove(UINT uID);
		void OnPGNChange(UINT uID, CWnd &Wnd);
		void OnDiagChange(UINT uID, CWnd &Wnd);
		void OnEditChange(UINT uID, CWnd &Ctrl);
			
	protected:

		CCANJ1939DriverOptions * m_pOptions;
		BYTE			 m_pSPNs[85];	// Limited to 64

		// Implementation

		void LoadPGNList(void);
		void LoadCombos(void);
		void InitAll(void);
		void DoEnables(void);
		void SetSPNs(BOOL fUpdate = TRUE);
		
		// Helpers
		BOOL	IsInteger(UINT uID);
		CString GetTitle(CPGN * pPGN);


	};


// End of File

#endif
