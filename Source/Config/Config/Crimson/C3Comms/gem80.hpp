
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GEM80_HPP

#define	INCLUDE_GEM80_HPP

#include "srtpmtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Driver Options
//

class CGem80SerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGem80SerialDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Device Options
//

class CGem80SerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGem80SerialDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
									

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Exchange Device Options
//

class CGem80SerialExchangeDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGem80SerialExchangeDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Exchange;
		UINT m_Ping;
		UINT m_Timeout;
		UINT m_Monitor;
									

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Base Driver
//

class CGem80Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CGem80Driver(void);

	protected:
					
		// Implementation
		void	AddSpaces(void); 


	};


//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Base Serial Driver
//

class CGem80SerialDriver : public CGem80Driver
{
	public:
		// Constructor
		CGem80SerialDriver(void);

		// Binding Control

		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial Master Program Port Driver
//

class CGem80SerialMasterDriver : public CGem80SerialDriver
{
	public:
		// Constructor
		CGem80SerialMasterDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial Master J/K Driver
//

class CGem80SerialJKMasterDriver : public CGem80SerialDriver
{
	public:
		// Constructor
		CGem80SerialJKMasterDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Implementation
		void	AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial Slave J/K Driver
//

class CGem80SerialSlaveDriver : public CGem80SerialDriver
{
	public:
		// Constructor
		CGem80SerialSlaveDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 TCP/IP Master Driver Options
//

class CGem80TCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGem80TCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Port;
		//UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 TCP/IP Master Driver
//

class CGem80TCPMasterDriver : public CGem80Driver
{
	public:
		// Constructor
		CGem80TCPMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
						
	protected:
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom Alspa via SNP Master Driver
//

class CAlstomSNPMasterDriver : public CSNPDriver
{
	public:
		// Constructor
		CAlstomSNPMasterDriver(void);
			
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom Alspa via SRTP Master TCP Driver
//

class CAlstomSRTPMasterTCPDriver : public CGeSrtpMasterTCPDriver
{
	public:
		// Constructor
		CAlstomSRTPMasterTCPDriver(void);
			
	};
 

 
// End of File	

#endif
