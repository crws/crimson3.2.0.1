
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LMB_HPP
	
#define	INCLUDE_LMB_HPP

//////////////////////////////////////////////////////////////////////////
//
// LMB Driver
//

class CLMBDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CLMBDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
