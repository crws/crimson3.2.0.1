
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Standard Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CStdDeviceOptions, CUIItem);

// Constructor

CStdDeviceOptions::CStdDeviceOptions(void)
{
	m_Drop = 0;
	}

// Download Support

BOOL CStdDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CStdDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Table Device Options (Legacy Only)
//

// Dynamic Class

AfxImplementDynamicClass(CTableDeviceOptions, CStdDeviceOptions);

//////////////////////////////////////////////////////////////////////////
//
// Named Device Options (Legacy Only)
//

// Dynamic Class

AfxImplementDynamicClass(CNamedDeviceOptions, CStdDeviceOptions);

// End of File
