
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HONEYWELLUDC9000_HPP
	
#define	INCLUDE_HONEYWELLUDC9000_HPP

// Space Identifiers
#define	SPR	1	// Registers
#define	SPIW	2	// I/O Words
#define	SPIB	3	// I/O Bits - needed for Gateway Blocks
#define	SPM	4	// Mode Select
#define	SPS	5	// Status Request
#define	SPT	6	// Device/Comms Status
#define	SPB	7	// Control Block I/O
#define	SPC	8	// Control Block Config
 

//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC9000 Device Options
//

class CHoneywellUDC9000DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHoneywellUDC9000DeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC9000 Driver
//

class CHoneywellUDC9000Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CHoneywellUDC9000Driver(void);

		//Destructor
		~CHoneywellUDC9000Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
						
	protected:
		// Implementation
		void	AddSpaces(void);

		// Helpers
		BOOL	IsControlBlock(UINT uTable);
	};

//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC9000 Address Selection
//

class CHoneywellUDC9000AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CHoneywellUDC9000AddrDialog(CHoneywellUDC9000Driver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		BOOL	AllowType(UINT uType);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Helpers
		BOOL	IsControlBlock(UINT uTable);
	};

// End of File

#endif
