#include "intern.hpp"

#include "UnidriveM.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Base Device Options
//

CUnidriveMDeviceOptions::CUnidriveMDeviceOptions(void)
{
	m_RegisterMode	= regModeMod;

	m_DriveMode	= drvModeRFCS;

	m_uLastDriveMode = m_DriveMode;

	InitMapRFCS(m_DevMenus);
}

CUnidriveMDeviceOptions::~CUnidriveMDeviceOptions(void)
{
	CleanupMenus();
}

// Persistence

void CUnidriveMDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	// Empty out anything already loaded.
	CleanupMenus();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Menu" ) {

			Tree.GetObject();

			LoadMenus(Tree);

			Tree.EndObject();
		}

		if( Name == "ItemList" ) {

			Tree.GetObject();

			LoadItems(Tree);

			Tree.EndObject();

			continue;
		}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
		}
	}
}

void CUnidriveMDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutObject(TEXT("Menu"));

	SaveMenus(Tree);

	Tree.EndObject();

	Tree.PutObject(TEXT("ItemList"));

	SaveItems(Tree);

	Tree.EndObject();
}

void CUnidriveMDeviceOptions::AddItem(CUnidriveMItem const &Item)
{
	INDEX i = m_DevMenus.FindName(Item.m_uMenu);

	if( i && i != INDEX(NOTHING) ) {

		CUnidriveMMenu *pMenu = m_DevMenus.GetData(i);

		pMenu->AddItem(Item);
	}
}

BOOL CUnidriveMDeviceOptions::AddMenu(CUnidriveMMenu *pMenu)
{
	INDEX i = m_DevMenus.FindName(pMenu->m_uMenu);

	// Check that this menu isn't a duplicate

	if( i == 0 || i == INDEX(NOTHING) ) {

		m_DevMenus.Insert(pMenu->m_uMenu, pMenu);

		return TRUE;
	}

	return FALSE;
}

BOOL CUnidriveMDeviceOptions::FindItem(UINT uRef, CUnidriveMItem &Item)
{
	// Lookup items using the encoded menu/item number as a reference

	UINT uMenu = uRef / 1000;

	UINT uItem = uRef % 1000;

	CMenuMap MenuMap = m_DevMenus;

	INDEX i = MenuMap.FindName(uMenu);

	if( i && i != INDEX(NOTHING) ) {

		CUnidriveMMenu *pMenu = MenuMap[i];

		for( UINT n = 0; n < pMenu->m_Items.GetCount(); n++ ) {

			if( pMenu->m_Items[n].m_uItem == uItem ) {

				Item = pMenu->m_Items[n];

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Persistence Helpers

void CUnidriveMDeviceOptions::SaveMenus(CTreeFile &Tree)
{
	for( INDEX i = m_DevMenus.GetHead(); !m_DevMenus.Failed(i); m_DevMenus.GetNext(i) ) {

		CUnidriveMMenu *pMenu = m_DevMenus[i];

		Tree.PutValue(TEXT("MenuNum"), pMenu->m_uMenu);

		Tree.PutValue(TEXT("Name"), pMenu->m_Name);

		Tree.PutValue(TEXT("Desc"), pMenu->m_Desc);
	}
}

void CUnidriveMDeviceOptions::LoadMenus(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "MenuNum" ) {

			UINT uMenuNum = Tree.GetValueAsInteger();

			Tree.GetName();

			CString Name = Tree.GetValueAsString();

			Tree.GetName();

			CString Desc = Tree.GetValueAsString();

			CUnidriveMMenu *pMenu = New CUnidriveMMenu(Name, Desc, uMenuNum);

			if( !AddMenu(pMenu) ) {

				delete pMenu;
			}

			continue;
		}
	}
}

void CUnidriveMDeviceOptions::LoadItems(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		CUnidriveMItem Item;

		if( Name == "Menu" ) {

			Item.m_uMenu = Tree.GetValueAsInteger();

			Tree.GetName();

			Item.m_uItem = Tree.GetValueAsInteger();

			Tree.GetName();

			Item.m_uType = Tree.GetValueAsInteger();

			Tree.GetName();

			CString Desc = Tree.GetValueAsString();

			wstrcpy(Item.m_pDesc, Desc);

			AddItem(Item);

			continue;
		}
	}
}

void CUnidriveMDeviceOptions::SaveItems(CTreeFile &Tree)
{
	for( INDEX i = m_DevMenus.GetHead(); !m_DevMenus.Failed(i); m_DevMenus.GetNext(i) ) {

		CUnidriveMMenu *pMenu = m_DevMenus[i];

		for( UINT n = 0; n < pMenu->m_Items.GetCount(); n++ ) {

			CUnidriveMItem Item = pMenu->m_Items[n];

			Tree.PutValue(TEXT("Menu"), Item.m_uMenu);

			Tree.PutValue(TEXT("Item"), Item.m_uItem);

			Tree.PutValue(TEXT("Type"), Item.m_uType);

			Tree.PutValue(TEXT("Desc"), CString(Item.m_pDesc));
		}
	}
}

void CUnidriveMDeviceOptions::OnImport(CWnd *pWnd)
{
	if( m_DevMenus.GetCount() ) {

		UINT uRes = pWnd->YesNo(CString(IDS_CT_UDRV_ONIMPORT));

		if( uRes != IDYES ) {

			return;
		}
	}

	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Unidrive M"));

	Dlg.SetCaption(CString(IDS_CT_UDRV_IMPORT));

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

			CString Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
		}
		else {
			CleanupMenus();

			if( ImportItems(pFile) ) {

				SetDirty();

				SendRefreshRebuildMsg(pWnd);
			}

			fclose(pFile);
		}

		Dlg.SaveLastPath(TEXT("Unidrive M"));
	}
}

// Import / Export for custom parameters

void CUnidriveMDeviceOptions::OnExport(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Unidrive M"));

	Dlg.SetCaption(CString(IDS_CT_UDRV_EXPORT));

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;

		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {

			fclose(pFile);

			CString Text = TEXT("The selected file already exists.\n\n")
				TEXT("Do you want to overwrite it?");

			if( pWnd->YesNo(Text) == IDNO ) {

				return;
			}
		}

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			CString Text = TEXT("Unable to open file for writing.");

			pWnd->Error(Text);

			return;
		}

		ExportItems(pFile);

		fclose(pFile);

		Dlg.SaveLastPath(TEXT("Unidrive M"));
	}
}

CMenuMap CUnidriveMDeviceOptions::GetMenus(void)
{
	return m_DevMenus;
}

BOOL CUnidriveMDeviceOptions::ImportItems(FILE *pFile)
{
	DWORD dwPos = ftell(pFile);

	for( UINT n = 0; n < 1; n++ ) {

		UINT uLine = 0;

		BOOL fInit = TRUE;

		fseek(pFile, dwPos, SEEK_SET);

		while( !feof(pFile) ) {

			char sLine[256] = { 0 };

			fgets(sLine, sizeof(sLine), pFile);

			if( sLine[0] ) {

				uLine++;

				CStringArray List;

				sLine[strlen(sLine)-1] = 0;

				TCHAR Sep = GetSeparator();

				CString(sLine).Tokenize(List, Sep);

				if( fInit ) {

					if( List.GetCount() ) {

						if( List[0] == "TYPE" ) {

							fInit = FALSE;
						}
					}

					continue;
				}

				if( List.GetCount() == 4 ) {

					if( List[0].CompareN(TEXT("MENU")) == 0 ) {

						CString Name = List[1];

						CString Desc = List[2];

						UINT uMenu = wcstoul(List[3], NULL, 10);

						CUnidriveMMenu *pMenu = New CUnidriveMMenu(Name, Desc, uMenu);

						AddMenu(pMenu);
					}
					else {
						CUnidriveMItem Item;

						Item.m_uType = CUnidriveMDriver::GetType(List[0]);

						Item.m_uItem = wcstoul(List[1], NULL, 10);

						wstrcpy(Item.m_pDesc, List[2]);

						Item.m_uMenu = wcstoul(List[3], NULL, 10);

						AddItem(Item);
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CUnidriveMDeviceOptions::ExportItems(FILE *pFile)
{
	fprintf(pFile, TEXT("TYPE,ID,DESCRIPTION,MENUNUMBER\n"));

	TCHAR Sep = GetSeparator();

	for( INDEX i = m_DevMenus.GetHead(); !m_DevMenus.Failed(i); m_DevMenus.GetNext(i) ) {

		CUnidriveMMenu *pMenu = m_DevMenus[i];

		// Print the line using the separator for this locale.

		fprintf(pFile,
			TEXT("MENU%c%s%c%s%c%d\n"),
			Sep,
			PCTXT(pMenu->m_Name),
			Sep,
			PCTXT(pMenu->m_Desc),
			Sep,
			pMenu->m_uMenu
			);

		for( UINT n = 0; n < pMenu->m_Items.GetCount(); n++ ) {

			CUnidriveMItem Item = pMenu->m_Items[n];

			fprintf(pFile,
				TEXT("%s%c%d%c%s%c%d\n"),
				PCTXT(CUnidriveMDriver::GetTypeText(Item.m_uType)),
				Sep,
				Item.m_uItem,
				Sep,
				Item.m_pDesc,
				Sep,
				Item.m_uMenu
			);
		}
	}

	return TRUE;
}

TCHAR CUnidriveMDeviceOptions::GetSeparator(void)
{
	TCHAR Sep = L',';

	TCHAR Buf[8] = { 0 };

	int ret = GetLocaleInfo(GetThreadLocale(),
				LOCALE_SLIST,
				Buf,
				sizeof(Buf) / sizeof(Buf[0]));

	if( ret ) {

		Sep = Buf[0];
	}

	return Sep;
}

// Comms Rebuild / View Refresh

void CUnidriveMDeviceOptions::SendRefreshRebuildMsg(CWnd *pWnd)
{
	pWnd->PostMessage(WM_COMMAND, IDM_COMMS_BUILD_BLOCK);

	pWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);
}

// Menu Map Management

void CUnidriveMDeviceOptions::InitMapRFCA(CMenuMap &MenuMap)
{
	AddHeadersRFCA(MenuMap);
	AddHeadersCommon(MenuMap);
	AddSpacesRFCA(MenuMap);
}

void CUnidriveMDeviceOptions::InitMapRFCS(CMenuMap &MenuMap)
{
	AddHeadersRFCS(MenuMap);
	AddHeadersCommon(MenuMap);
	AddSpacesRFCS(MenuMap);
}

void CUnidriveMDeviceOptions::InitMapRegen(CMenuMap &MenuMap)
{
	AddHeadersRegen(MenuMap);
	AddHeadersCommon(MenuMap);
	AddSpacesRegen(MenuMap);
}

void CUnidriveMDeviceOptions::InitMapOpenLoop(CMenuMap &MenuMap)
{
	AddHeadersOpenLoop(MenuMap);
	AddHeadersCommon(MenuMap);
	AddSpacesOpenLoop(MenuMap);
}

void CUnidriveMDeviceOptions::CleanupMenus(void)
{
	for( INDEX i = m_DevMenus.GetHead(); !m_DevMenus.Failed(i); m_DevMenus.GetNext(i) ) {

		CUnidriveMMenu *pMenu = m_DevMenus[i];

		delete pMenu;
	}

	m_DevMenus.Empty();
}

BOOL CUnidriveMDeviceOptions::OnChangeMode(CWnd *pWnd, UINT uMode)
{
	if( pWnd->YesNo(CString(IDS_CT_UDRV_CHNG_MODE)) == IDYES ) {

		// Empty out anything already loaded
		CleanupMenus();

		switch( uMode ) {

			case drvModeOpenLoop:
				InitMapOpenLoop(m_DevMenus);
				break;

			case drvModeRegen:
				InitMapRegen(m_DevMenus);
				break;

			case drvModeRFCA:
				InitMapRFCA(m_DevMenus);
				break;

			case drvModeRFCS:
				InitMapRFCS(m_DevMenus);
				break;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CUnidriveMDeviceOptions::DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {

			if( !m_DevMenus.IsEmpty() ) {

				m_ListIndex = m_DevMenus.GetHead();

				Data.m_Addr.m_Ref = 0;
				Data.m_fPart	  = FALSE;
				Data.m_Name	  = m_DevMenus.GetData(m_ListIndex)->m_Name;
				Data.m_uData	  = m_DevMenus.GetData(m_ListIndex)->m_uMenu;

				return TRUE;
			}

			return FALSE;
		}
		else {
			m_DevMenus.GetNext(m_ListIndex);

			if( m_DevMenus.Failed(m_ListIndex) ) {

				return FALSE;
			}

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart	  = FALSE;
			Data.m_Name	  = m_DevMenus.GetData(m_ListIndex)->m_Name;
			Data.m_uData	  = m_DevMenus.GetData(m_ListIndex)->m_uMenu;

			return TRUE;
		}
	}
	else {
		if( pRoot->m_uData ) {

			CUnidriveMMenu *pMenu = m_DevMenus.GetData(m_ListIndex);

			CItemArray Items = pMenu->m_Items;

			if( uItem < Items.GetCount() ) {

				CUnidriveMItem Item = Items[uItem];

				UINT uTable = 0;

				UINT uOffset = 0;

				BOOL fPart = FALSE;

				if( Item.m_uMenu == MENU_MODBUS ) {

					uTable = Item.m_uItem;

					fPart = TRUE;

					uOffset = 0;
				}
				else {
					uTable = addrNamed;

					if( m_RegisterMode == regModeStd ) {

						uOffset = Item.m_uMenu * 100 + uItem;
					}
					else {
						uOffset = Item.m_uMenu * 256 + uItem;
					}
				}

				Data.m_Addr.a.m_Extra	= 0;
				Data.m_Addr.a.m_Offset	= uOffset;
				Data.m_Addr.a.m_Table	= uTable;
				Data.m_Addr.a.m_Type	= Item.m_uType;

				Data.m_fPart      = fPart;
				Data.m_Name       = Item.m_pDesc;
				Data.m_uData	  = 0;

				return TRUE;
			}
		}
	}

	return FALSE;
}

// UI Management

void CUnidriveMDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Push" ) {

			if( m_Push == 0 ) {

				OnImport(pWnd);
			}

			if( m_Push == 1 ) {

				OnExport(pWnd);
			}
		}

		if( Tag == "DriveMode" ) {

			if( OnChangeMode(pWnd, m_DriveMode) ) {

				m_uLastDriveMode = m_DriveMode;

				SendRefreshRebuildMsg(pWnd);
			}
			else {
				// If the user cancels the drive mode change,
				// restore the previous mode setting and refresh
				// the UI.

				m_DriveMode = m_uLastDriveMode;

				pWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);
			}
		}

		if( Tag == "RegisterMode" ) {

			SendRefreshRebuildMsg(pWnd);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CUnidriveMSerialDeviceOptions, CUIItem);

// Constructor

CUnidriveMSerialDeviceOptions::CUnidriveMSerialDeviceOptions(void)
{
	m_Drop		= 1;
	m_Ping		= 1;
}

// Download Support

BOOL CUnidriveMSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddWord(WORD(m_Ping));
	Init.AddLong(m_RegisterMode);
	Init.AddLong(m_DriveMode);

	return TRUE;
}

// Meta Data Creation

void CUnidriveMSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Ping);
	Meta_AddInteger(RegisterMode);
	Meta_AddInteger(DriveMode);

	Meta_AddInteger(Push);
}

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CUnidriveMTCPDeviceOptions, CUIItem);

// Constructor

CUnidriveMTCPDeviceOptions::CUnidriveMTCPDeviceOptions(void)
{
	m_Addr     = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Socket   = 502;

	m_Unit     = 1;

	m_Keep     = TRUE;

	m_Time1    = 5000;

	m_Time2    = 2500;

	m_Time3    = 200;
}

// UI Managament

void CUnidriveMTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
		}

		CUnidriveMDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}
}

// Download Support

BOOL CUnidriveMTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_Unit));
	Init.AddLong(m_RegisterMode);
	Init.AddLong(m_DriveMode);

	return TRUE;
}

// Meta Data Creation

void CUnidriveMTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(RegisterMode);
	Meta_AddInteger(DriveMode);

	Meta_AddInteger(Push);
}

// End of File
