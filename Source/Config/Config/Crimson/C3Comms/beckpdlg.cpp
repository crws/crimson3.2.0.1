
#include "intern.hpp"

#include "beckplus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBeckhoffPlusTagsTCPDialog, CStdAddrDialog);
		
// Constructor

CBeckhoffPlusTagsTCPDialog::CBeckhoffPlusTagsTCPDialog(CBeckhoffPlusTagsTCPDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_pDriver  = &Driver;
	
	m_pAddr    = &Addr;

	m_pConfig  = (CBeckhoffPlusTagsTCPDeviceOptions *) pConfig;

	m_fPart    = fPart;

	m_fSelect  = TRUE;
	
	m_Images.Create("CommsManager", 16, 0, IMAGE_BITMAP, 0);

	SetName(fPart ? "PartBeckhoffPlusDlg" : "FullBeckhoffPlusDlg");
	}

// Destructor

CBeckhoffPlusTagsTCPDialog::~CBeckhoffPlusTagsTCPDialog(void)
{
	afxThread->SetStatusText("");
	}

// Initialisation

void CBeckhoffPlusTagsTCPDialog::SetCaption(CString const &Text)
{
	m_Caption = Text;
	}

void CBeckhoffPlusTagsTCPDialog::SetSelect(BOOL fSelect)
{
	m_fSelect = fSelect;
	}

// Message Map

AfxMessageMap(CBeckhoffPlusTagsTCPDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown   )
	AfxDispatchNotify(4001, LBN_SELCHANGE, OnTypeChange)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(4002, OnCreateTag)

	AfxMessageEnd(CBeckhoffPlusTagsTCPDialog)
	};

// Message Handlers

BOOL CBeckhoffPlusTagsTCPDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(m_Caption);

	GetDlgItem(IDCANCEL).EnableWindow(m_fSelect);

	GetDlgItem(IDOK).SetWindowText(m_fSelect ? CString("OK") : CString("Close"));

	SetRadioButton(addrByteAsByte);

	GetDlgItem(4007).SetWindowText("");
	GetDlgItem(4007).EnableWindow(FALSE);

	if( m_fPart ) {

		ShowAddress(*m_pAddr);
		}
	else {
		LoadNames();
		LoadTypes();
		}

	CAddress &Addr = *m_pAddr;

	UpdateCombos(Addr);

	return FALSE;
	}

// Notification Handlers

void CBeckhoffPlusTagsTCPDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_hSelect  = Info.itemNew.hItem;

	m_dwSelect = Info.itemNew.lParam;

	CAddress Addr = *m_pAddr;

	Addr.a.m_Offset = 0;

	if( m_dwSelect == Addr.m_Ref ) {
		
		Addr = *m_pAddr;
		}
	else
		Addr = (CAddress &) m_dwSelect;

	ShowAddress(Addr);

	ShowDetails(Addr);

	UpdateCombos(Addr);
	}

BOOL CBeckhoffPlusTagsTCPDialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{

	if( Info.wVKey == VK_DELETE ) {
		
		if( IsDown(VK_SHIFT) ) {

			return DeleteTag();
			}
		}

	if( Info.wVKey == VK_F2 ) {		
		
		return RenameTag();
		}

	return FALSE;
	}

// Command Handlers

BOOL CBeckhoffPlusTagsTCPDialog::OnOkay(UINT uID)
{
	if( m_fSelect ) {
	
		CAddress Addr;
		
		if( !m_fPart ) {

			if( !m_dwSelect ) {

				m_pAddr->m_Ref = 0;

				EndDialog(TRUE);

				return TRUE;
				}

			Addr = (CAddress &) m_dwSelect;
			}
		else 
			Addr = *m_pAddr;

		CString Name;

		m_pConfig->ExpandAddress(Name, Addr);

		CString Text;

		Text += m_pConfig->GetTagName(Name);

		CError Error(TRUE);

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {
			
			*m_pAddr = Addr;

			m_pConfig->SetDirty();
			
			EndDialog(TRUE);
			
			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus(2002);

		return TRUE;
		}

	EndDialog(TRUE);
	
	return TRUE;
	}

BOOL CBeckhoffPlusTagsTCPDialog::OnCreateTag(UINT uID)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(4001);

	DWORD       Data = Combo.GetCurSelData();

	CAddress    Addr = (CAddress &) Data;

	UINT      uTable = Addr.a.m_Table;

	if( m_pConfig->IsFixed(uTable) ) {

		CString Offset  = GetDlgItem(4007).GetWindowText();

		char * Endptr;

		Addr.a.m_Offset = strtoul((const char *)Offset, &Endptr, 16);
		}

	Addr.a.m_Type = m_pConfig->IsNotIndexed(uTable) ? GetDefaultType(uTable) : ButtonToType(GetRadioButton());

	CString  Name = m_pConfig->CreateName(Addr);

	CStringDialog Dlg(CString("Create Tag"), CString("Tag Name"), Name);

	if( !IsDown(VK_SHIFT) ) {
		
		if( Dlg.Execute(ThisObject) ) {

			Name = Dlg.GetData();
			}
		else
			return TRUE;
		}

	CError Error(TRUE);

	if( m_pConfig->CreateTag(Error, Name, Addr, TRUE) ) {

		CError Error(FALSE);

		if( m_pConfig->ParseAddress(Error, Addr, Name) ) {
			
			CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);
			
			LoadTag(Tree, Name, Addr);
			
			Tree.Expand(m_hRoot, TVE_EXPAND);
			
			Tree.SetFocus();
			
			m_pConfig->SetDirty();
			}
		else
			AfxAssert(FALSE);
		}
	else {
		UpdateCombos(Addr);

		Error.Show(ThisObject);
		}

	return TRUE;
	}

void CBeckhoffPlusTagsTCPDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	CAddress Addr;

	Addr.m_Ref = GetTypeRef();

	BOOL fOff  = m_pConfig->IsFixed(Addr.a.m_Table);

	SetDlgFocus(fOff ? 4007 : 4002);

	UpdateCombos(Addr);
	}

DWORD CBeckhoffPlusTagsTCPDialog::GetTypeRef(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(4001);

	return Combo.GetItemData(Combo.GetCurSel());
	}

// Tree Loading

void CBeckhoffPlusTagsTCPDialog::LoadNames(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_NOHSCROLL
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_EDITLABELS
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

	Tree.SendMessage(TVM_SETUNICODEFORMAT);
	
	LoadNames(Tree);

	Tree.SetFocus();
	}

void CBeckhoffPlusTagsTCPDialog::LoadNames(CTreeView &Tree)
{
	LoadRoot(Tree);
	
	LoadTags(Tree);

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CBeckhoffPlusTagsTCPDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	Node.SetText(m_pConfig->GetDeviceName());

	Node.SetParam(FALSE);

	Node.SetImages(3);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CBeckhoffPlusTagsTCPDialog::LoadTags(CTreeView &Tree)
{
	CTagDataArray List;
	
	m_pConfig->ListTags(List);

//	List.Sort();

	m_fLoad = TRUE;

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CTagData const &Data = List[n];

		LoadTag(Tree, Data.m_Name, (CAddress &) Data.m_Addr);
		}

	m_fLoad = FALSE;
	}

void CBeckhoffPlusTagsTCPDialog::LoadTag(CTreeView &Tree, CString Name, CAddress Addr)
{
	CTreeViewItem Node;

	Node.SetText(Name);

	Node.SetParam(Addr.m_Ref);

	Node.SetImages(0x34);

	HTREEITEM hNode = Tree.InsertItem(m_hRoot, NULL, Node);

	if( m_fLoad ) {

		CAddress Test = *m_pAddr;

		if( Addr.m_Ref == Test.m_Ref ) {	

			Tree.SelectItem(hNode, TVGN_CARET);
			}
		}
	else
		Tree.SelectItem(hNode, TVGN_CARET);

	SetRadioButton(Addr.a.m_Type);
	}

// Data Type Loading

void CBeckhoffPlusTagsTCPDialog::LoadTypes(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(4001);

	Combo.SetRedraw(FALSE);

	AddType(SPMEM,	Combo, STRMEM);
	AddType(SPINP,	Combo, STRINP);
	AddType(SPOUT,	Combo, STROUT);
	AddType(SPFMEM,	Combo, STRFM);
	AddType(SPFI,	Combo, STRFI);
	AddType(SPFO,	Combo, STRFO);
	AddType(SPRAS,	Combo, STRRAS);
	AddType(SPRDS,	Combo, STRRDS);
	AddType(SPWC,	Combo, STRWC);
	AddType(SPWCA,	Combo, STRWCA);
	AddType(SPWCD,	Combo, STRWCD);

	Combo.SetCurSel(0);

	Combo.SetRedraw(TRUE);
	}

void CBeckhoffPlusTagsTCPDialog::AddType(UINT uTable, CComboBox &Combo, CString Text)
{
	CAddress Addr;

	Addr.a.m_Table  = uTable;
	Addr.a.m_Offset = 0;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = m_pConfig->IsNotIndexed(uTable) ? GetDefaultType(uTable) : ButtonToType(GetRadioButton());

	Combo.AddString(Text, LPARAM(Addr.m_Ref));
	}

UINT CBeckhoffPlusTagsTCPDialog::GetDefaultType(UINT uTable)
{
	switch( uTable ) {

		case SPRAS:
		case SPRDS:
		case SPWCA:
		case SPWCD:	return addrWordAsWord;
		}

	return addrBitAsBit;
	}

// Implementation

void CBeckhoffPlusTagsTCPDialog::ShowDetails(CAddress Addr)
{
	if( Addr.m_Ref ) {

		CString Type = CSpace("", "", 0).GetTypeAsText(Addr.a.m_Type);
			
		GetDlgItem(3002).SetWindowText(Type);
		
		return;
		}

	GetDlgItem(3002).SetWindowText("");
	}

void CBeckhoffPlusTagsTCPDialog::ShowAddress(CAddress Addr)
{
	if( Addr.m_Ref ) {

		CString  Tag = FindNameFromAddr(Addr);

		GetDlgItem(2001).SetWindowText(Tag);

		if( !m_pConfig->IsNotIndexed(Addr.a.m_Table) ) {

			EnableRadioButtons();
			}

		SetRadioButton(TypeToButton(Addr.a.m_Type));

		return;
		}

	GetDlgItem(2001).SetWindowText("None");

	GetDlgItem(2002).ShowWindow(FALSE);

	GetDlgItem(2003).ShowWindow(FALSE);
	}

BOOL CBeckhoffPlusTagsTCPDialog::DeleteTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString  Name;

		m_pConfig->ExpandAddress(Name, Addr);

		CString Text  = CPrintf("Do you really want to delete %s ?", m_pConfig->GetTagName(Name));

		if( NoYes(Text) == IDYES ) {

			CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

			Tree.SetRedraw(FALSE);

			HTREEITEM hPrev = m_hSelect;

			Tree.MoveSelection();

			m_pConfig->DeleteTag(Addr, TRUE);

			Tree.DeleteItem(hPrev);

			Tree.SetRedraw(TRUE);

			m_pConfig->SetDirty();

			return TRUE;
			}		
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTagsTCPDialog::RenameTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString Name;

		if( m_pConfig->ExpandAddress(Name, Addr) ) {

			CString Title   = "Rename Variable";
			
			CString Group   = "Variable Name";

			CString TagName = m_pConfig->GetTagName(Name);

			CStringDialog Dlg(Title, Group, TagName);

			if( Dlg.Execute(ThisObject) ) {

				CTreeView &Tree = (CTreeView &) GetDlgItem(1001);
				
				CString    Name = Dlg.GetData();			

				if( m_pConfig->RenameTag(Name, Addr) ) {

					Tree.SetRedraw(FALSE);

					HTREEITEM hPrev = m_hSelect;

					Tree.MoveSelection();

					Tree.DeleteItem(hPrev);

					LoadTag(Tree, Name, Addr);

					Tree.SetRedraw(TRUE);

					m_pConfig->SetDirty();
					
					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

BOOL CBeckhoffPlusTagsTCPDialog::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
	}

CString CBeckhoffPlusTagsTCPDialog::FindNameFromAddr(CAddress Addr)
{
	CString Name;

	if( Addr.m_Ref ) {

		m_pConfig->ExpandAddress(Name, Addr);
		}

	return Name;
	}

void CBeckhoffPlusTagsTCPDialog::UpdateCombos(CAddress Addr)
{
	if( Addr.m_Ref ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(4001);

		UINT uSel = 0;
		UINT uTab = Addr.a.m_Table;

		if( m_pConfig->IsFixed(uTab) ) uSel = uTab + 2;

		else if( m_pConfig->IsNotIndexed(uTab) ) uSel = uTab - 4;

		else uSel = uTab - SPMEM;

		Combo.SetCurSel(uSel);

		SetRadioButton(Addr.a.m_Type);

		BOOL fOff = m_pConfig->IsFixed(Addr.a.m_Table);

		CString Offset;

		Offset.Printf("%4.4X", Addr.a.m_Offset);

		GetDlgItem(4007).SetWindowText(fOff ? Offset : "");
		GetDlgItem(4007).EnableWindow (fOff);
		GetDlgItem(4008).EnableWindow (fOff);
		}
	}

// Radio button handling

UINT CBeckhoffPlusTagsTCPDialog::GetRadioButton(void)
{
	UINT uResult = 0;

	for( UINT i = 4003; i <= 4006; i++ ) {

		CButton &Button = (CButton &) GetDlgItem(i);

		if( Button.IsChecked() ) {

			ClearRadioButtons();

			uResult = i - 4003;

			SetRadioButton(ButtonToType(uResult));
			}
		}

	return uResult;
	}

void CBeckhoffPlusTagsTCPDialog::SetRadioButton(UINT uType)
{
	PutRadioButton(4003, uType == addrByteAsByte);
	PutRadioButton(4004, uType == addrWordAsWord);
	PutRadioButton(4005, uType == addrLongAsLong);
	PutRadioButton(4006, uType == addrRealAsReal);
	}

void CBeckhoffPlusTagsTCPDialog::PutRadioButton(UINT uID, BOOL fYes)
{
	CButton &Check = (CButton &) GetDlgItem(uID);

	Check.SetCheck( fYes );
	}

void CBeckhoffPlusTagsTCPDialog::ClearRadioButtons(void)
{
	for( UINT i = 4003; i <= 4006; i++ ) {

		CButton &Check = (CButton &) GetDlgItem(i);

		Check.SetCheck(FALSE);
		}
	}

void CBeckhoffPlusTagsTCPDialog::EnableRadioButtons(void)
{
	for( UINT i = 4003; i <= 4006; i++ ) {

		CButton &Check = (CButton &) GetDlgItem(i);

		Check.EnableWindow(TRUE);
		}
	}

void CBeckhoffPlusTagsTCPDialog::DisableRadioButtons(void)
{
	for( UINT i = 4003; i <= 4006; i++ ) {

		CButton &Check = (CButton &) GetDlgItem(i);

		Check.EnableWindow(FALSE);
		}
	}

UINT CBeckhoffPlusTagsTCPDialog::TypeToButton(UINT uType)
{
	UINT uSet = 0;

	switch( uType ) {

		case addrWordAsWord: uSet = 1;	break;
		case addrLongAsLong: uSet = 2;	break;
		case addrRealAsReal: uSet = 3;	break;
		}

	return uSet;
	}

UINT CBeckhoffPlusTagsTCPDialog::ButtonToType(UINT uButton)
{
	switch( uButton ) {

		case 1:	return addrWordAsWord;
		case 2: return addrLongAsLong;
		case 3: return addrRealAsReal;
		}

	return addrByteAsByte;
	}

// End of File
