
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCOM_HPP

#define	INCLUDE_PCOM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBinaryPCOMSpace;

//////////////////////////////////////////////////////////////////////////
//
// Column Item
//

class CColumn : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CColumn(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void Save(CTreeFile &Tree);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Data Access
		UINT	GetType(void);
		UINT    GetTypeAs(void);
		UINT	GetVisual(void); 
		void	SetType(UINT uType);
		CString GetLabel(void);
		void	SetLabel(CString Label);
		void	SetByte(UINT uBytes);
		void    SetBytes(UINT uType);

		

	protected:
		// Data Members
		UINT	  m_Type;
		CString   m_Label;
		UINT	  m_Bytes;
				
		// Meta Data Creation
		void AddMetaData(void);
	};

/////////////////////////////////////////////////////////////////////////
//
//  Column List Class
//
//

class CColumnList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CColumnList(void);

		// Item Access
		CColumn * GetItem(INDEX Index) const;
		CColumn * GetItem(UINT  uPos ) const;

		// Operations
		CColumn * AppendItem(CColumn * pColumn);
		INDEX	  InsertItem(CColumn * pColumn, CColumn * pBefore);


	};

//////////////////////////////////////////////////////////////////////////
//
// Data Table
//

class CDataTable : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDataTable(void);
		
		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void Save(CTreeFile &Tree);
						
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		CColumn * GetColumn(void);
		CColumn * GetColumn(UINT uPos);
		void	  DeleteColumn(CItem * pItem);
		CColumn * FindColumn(CString Label);
		CString   GetLabel(void);
		void	  SetLabel(CString Label);
		CColumnList * GetColumnList(void);
		
		
		// Public Data
		UINT	      m_Rows;
		UINT	      m_Cols;
		

	protected:

		CString	      m_Label;
		CColumnList * m_pColumns;
						
		// Meta Data Creation
		void AddMetaData(void);
	};

/////////////////////////////////////////////////////////////////////////
//
//  Data Table List Class
//
//

class CDataTableList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDataTableList(void);

		// Item Access
		CDataTable * GetItem(INDEX Index) const;
		CDataTable * GetItem(UINT  uPos ) const;

		// Operations
		CDataTable * AppendItem(CDataTable * pTable);
		INDEX	     InsertItem(CDataTable * pTable, CDataTable * pBefore);

	};


///////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Serial Device Options
//

class CUniPCOMDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUniPCOMDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Unit;
		
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Serial Device Options	UI Page
//

class CUniPCOMBinaryDeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUniPCOMBinaryDeviceOptionsUIPage(void);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

	}; 

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Serial Device Options
//

class CUniPCOMBinaryDeviceOptions : public CUniPCOMDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUniPCOMBinaryDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		CDataTable     * GetTable(void);
		CDataTable     * GetTable(UINT uPos);
		void	         DeleteTable(CItem * pItem);
		CDataTable     * FindTable(CString Label);
		CDataTableList * GetTableList(void);
			
	protected:
		// Data Members
		CDataTableList * m_pTables;

		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM ASCII Master Driver
//

class CUniPCOMAsciiDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CUniPCOMAsciiDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
		
	protected:
		

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Master Driver
//

class CUniPCOMBinaryDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CUniPCOMBinaryDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		
    
	protected:
	
		// Implementation
		void AddSpaces(CItem * pItem);
	};


//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM TCP/IP Device Options
//

class CUniPCOMTcpDeviceOptions : public CUniPCOMDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUniPCOMTcpDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
   		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary TCP/IP Device Options
//

class CUniPCOMBinaryTcpDeviceOptions : public CUniPCOMBinaryDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUniPCOMBinaryTcpDeviceOptions(void);

		// UI Loading
		void LoadUI(CUIViewWnd *pWnd, UINT uPage);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM TCP/IP ASCII Master Driver
//


class CUniPCOMTcpADriver : public CUniPCOMAsciiDriver
{
	public:
		// Constructor
		CUniPCOMTcpADriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM TCP/IP Binary Master Driver
//


class CUniPCOMTcpBDriver : public CUniPCOMBinaryDriver
{
	public:
		// Constructor
		CUniPCOMTcpBDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	};

///////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Data Table Management Dialog
//

class CPCOMDataTableDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CPCOMDataTableDialog(CItem *pConfig);
		                
	protected:

		// Data
		
		CUniPCOMBinaryDeviceOptions *	m_pConfig;
		HTREEITEM			m_hRoot;
		HTREEITEM			m_hSelect;
			
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCancel(UINT uID);
		BOOL OnCreate(UINT uID);
		BOOL OnEdit(UINT uID);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
	
		// Implementation
		void LoadDataTables(void);
		void LoadRoot(CTreeView &Tree);
		void LoadTables(CTreeView &Tree);
		void DoEnables(void);
		void ShowTable(CTreeView &Tree, HTREEITEM hNode, CDataTable * pTable);
		void ShowColumn(CTreeView &Tree, HTREEITEM hNode, CColumn * pColumn);
		void UpdateTable(CTreeView &Tree, HTREEITEM hNode, CDataTable * pTable);
		void UpdateColumn(CTreeView &Tree, HTREEITEM hNode, CColumn * pColumn);


       	};

//////////////////////////////////////////////////////////////////////////
//
// Unitronics Binary PCOM Address Selection
//

class CUniPCOMBinaryDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CUniPCOMBinaryDialog(CUniPCOMBinaryDriver &Driver, CAddress &Addr, CItem * pItem, BOOL fPart);
		
                // Message Map
		AfxDeclareMessageMap();

		// Initialization
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSpaceChange(UINT uID, CWnd &Wnd);
		void OnColumnChange(UINT uID, CWnd &Wnd);

	protected:

		// Overridables
		BOOL    AllowType(UINT uType);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PCOM Binary Space Wrapper Class
//

class CBinaryPCOMSpace : public CSpace
{
	public:
		// Constructors
		CBinaryPCOMSpace(UINT uTable, CString p,  CDataTable * pTable);

		// Data Access
		BOOL SetType(UINT uCol);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);


	protected:

		// Data members
		CDataTable * m_pTable;
	};

#endif


// End of File

