
#include "intern.hpp"

#include "CognexCameraDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CognexCameraDeviceOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cognex Camera Driver
//

// Instantiator

ICommsDriver *	Create_CognexCameraDriver(void)
{
	return New CCognexCameraDriver;
	}

// Constructor

CCognexCameraDriver::CCognexCameraDriver(void)
{
	m_wID		= 0x3F04;

	m_uType		= driverCamera;
	
	m_Manufacturer	= "Cognex";
	
	m_DriverName	= "In-Sight Camera Image";
	
	m_Version	= "1.00";

	m_DevRoot	= "Cam";
	
	m_ShortName	= "In-Sight Camera";
	}

// Configuration

CLASS CCognexCameraDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CCognexCameraDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCognexCameraDeviceOptions);
	}

// Binding Control

UINT CCognexCameraDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
