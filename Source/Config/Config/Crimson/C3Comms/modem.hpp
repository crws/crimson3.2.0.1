
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODEM_HPP
	
#define	INCLUDE_MODEM_HPP

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver (External)
//

#if defined(PROJECT_C3COMMS)

class CModemClientDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CModemClientDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Data Members
		UINT m_Binding;
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver (Option Card)
//

#if defined(PROJECT_C3COMMS)

class CModemClientDriverOptionCard : public CBasicCommsDriver
{
	public:
		// Constructor
		CModemClientDriverOptionCard(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		UINT m_Binding;
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver Options (External)
//

class CModemClientDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModemClientDriverOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Load(CTreeFile &Tree);				

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT	m_Dummy;
		UINT    m_Connect;
		UINT    m_ModemBrand;
		UINT	m_Safe;
		UINT	m_Demand;
		UINT	m_Timeout;
		UINT	m_SMS;		
		CString m_Init;
		CString m_Dial;
		CString m_User;
		CString m_Pass;		
		UINT	m_Route;
		UINT	m_Addr;
		UINT	m_Mask;
		UINT	m_NewMask;
		UINT    m_Location;
		UINT    m_LogFile;
		UINT	m_DynDNS;
		CString	m_DynUser;
		CString	m_DynPass;
		CString	m_DynHost;
		UINT    m_DNSMode;
		UINT    m_DNS1;
		UINT    m_DNS2;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		UINT GetDefInit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver Options (Options Card)
//

class CModemClientDriverOptionsOptionCard : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModemClientDriverOptionsOptionCard(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT    m_Connect;
		UINT	m_Safe;
		UINT	m_Demand;
		UINT	m_Timeout;
		UINT	m_SMS;
		UINT	m_Roaming;
		UINT	m_SIMSlot;
		CString m_PIN;
		UINT    m_Auth;
		UINT	m_CellRadio;
		CString m_Init;
		CString m_Dial;
		CString m_User;
		CString m_Pass;
		UINT    m_Location;
		UINT	m_Route;
		UINT	m_Addr;
		UINT	m_Mask;
		UINT    m_LogFile;
		UINT	m_DynDNS;
		CString	m_DynUser;
		CString	m_DynPass;
		CString	m_DynHost;
		UINT    m_DNSMode;
		UINT    m_DNS1;
		UINT    m_DNS2;

	protected:
		// Property Save Filter
		BOOL SaveProp(CString Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void CheckConnect(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver (External)
//

#if defined(PROJECT_C3COMMS)

class CModemServerDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CModemServerDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Data Members
		UINT m_Binding;	
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver (Option Card)
//

#if defined(PROJECT_C3COMMS)

class CModemServerDriverOptionCard : public CBasicCommsDriver
{
	public:
		// Constructor
		CModemServerDriverOptionCard(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Data Members
		UINT m_Binding;	
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver Options (External)
//

class CModemServerDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModemServerDriverOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Load(CTreeFile &Tree);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT    m_Connect;
		UINT    m_ModemBrand;
		UINT	m_Timeout;
		UINT    m_Location;		
		UINT	m_SMS;		
		CString m_Init;
		CString m_User;
		CString m_Pass;
		UINT	m_LocAddr;
		UINT	m_RemAddr;
		UINT	m_RemMask;
		UINT	m_NewMask;
		UINT    m_LogFile;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		UINT GetDefInit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver Options (Option Card)
//

class CModemServerDriverOptionsOptionCard : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModemServerDriverOptionsOptionCard(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT    m_Connect;
		UINT	m_Timeout;
		UINT    m_Location;
		UINT	m_SMS;
		CString m_Init;
		CString m_User;
		CString m_Pass;
		UINT	m_LocAddr;
		UINT	m_RemAddr;
		UINT	m_RemMask;
		UINT    m_LogFile;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver (External)
//

#if defined(PROJECT_C3COMMS)

class CModemSMSDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CModemSMSDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Data Members
		UINT m_Binding;		
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver (Option Card)
//

#if defined(PROJECT_C3COMMS)

class CModemSMSDriverOptionCard : public CBasicCommsDriver
{
	public:
		// Constructor
		CModemSMSDriverOptionCard(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Data Members
		UINT m_Binding;		
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver Options (External)
//

class CModemSMSDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModemSMSDriverOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Load(CTreeFile &Tree);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT    m_Connect;
		CString m_Init;
		UINT	m_NewMask;
		UINT    m_Location;
		UINT    m_ModemBrand;		

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		UINT GetDefInit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver Options (Option Card)
//

class CModemSMSDriverOptionsOptionCard : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModemSMSDriverOptionsOptionCard(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT    m_Connect;
		CString m_Init;
		UINT    m_Location;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
