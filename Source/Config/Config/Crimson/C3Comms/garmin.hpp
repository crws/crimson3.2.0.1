
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GARMIN_HPP
	
#define	INCLUDE_GARMIN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Garmin GPS Options
//

class CGarminDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGarminDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_DegForm;
		UINT m_UnitSpeed;
		UINT m_UnitAlt;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Garmin GPS Master
//

class CGarminDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CGarminDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
