
#include "modbus.hpp"

#include "mbtcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SCHMOD_HPP
	
#define	INCLUDE_SCHMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via MODBUS Device Options
//

class CSchneiderModbusDeviceOptions : public CModbusDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSchneiderModbusDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_ByteR;
		UINT m_ByteL;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via MODBUS Driver
//

class CSchneiderModbusSerialDriver : public CModbusDriver
{
	public:
		// Constructor
		CSchneiderModbusSerialDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Schneider via Modbus TCP/IP Master Device Options
//

class CSchneiderModbusTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSchneiderModbusTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		BOOL m_fDisable16;
		BOOL m_fDisable15;
		BOOL m_fDisable5;
		BOOL m_fDisable6;
		UINT m_PingReg;
		UINT m_Max01;
		UINT m_Max02;
		UINT m_Max03;
		UINT m_Max04;
		UINT m_Max15;
		UINT m_Max16;
		UINT m_ByteR;
		UINT m_ByteL;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);

	};


//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via MODBUS TCP/IP Master Driver
//

class CSchneiderModbusTCPDriver : public CModbusMasterTCPDriver
{
	public:
		// Constructor
		CSchneiderModbusTCPDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
						
	protected:
		// Implementation
		void AddSpaces(void);
	};



// End of File

#endif
