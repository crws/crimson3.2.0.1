
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BALDOR_HPP
	
#define	INCLUDE_BALDOR_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Baldor Mint Host Serial Device Options
//

class CBaldorSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBaldorSerialDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Protocol;
	

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Baldor Mint Host Serial Master Driver
//
//

class CBaldorSerialMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBaldorSerialMasterDriver(void);

		//Destructor
		~CBaldorSerialMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Implementation
		void AddSpaces(CItem * pConfig);
	};

#endif

// End of File