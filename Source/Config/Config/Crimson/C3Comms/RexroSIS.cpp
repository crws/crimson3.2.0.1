
#include "intern.hpp"

#include "Rexrosis.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CRexrothSISDeviceOptions, CUIItem);

// Constructor

CRexrothSISDeviceOptions::CRexrothSISDeviceOptions(void)
{
	m_SDrop		= 0;

	m_RDrop		= 1;

//	m_Model         = 0;
	}

// Download Support

BOOL CRexrothSISDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_SDrop));
	Init.AddByte(BYTE(m_RDrop));
//	Init.AddByte(BYTE(m_Model));

	return TRUE;
	}

// Meta Data Creation

void CRexrothSISDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(SDrop);
	Meta_AddInteger(RDrop);
//	Meta_AddInteger(Model);
	}

//////////////////////////////////////////////////////////////////////////
//
// Rexroth Indramat SIS Driver
//

// Instantiator

ICommsDriver *	Create_RexrothSISDriver(void)
{
	return New CRexrothSISDriver;
	}

// Constructor

CRexrothSISDriver::CRexrothSISDriver(void)
{
	m_wID		= 0x4011;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Rexroth Indramat";
	
	m_DriverName	= "SIS";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Rexroth Indramat SIS";

	AddSpaces();
	}

// Binding Control

UINT	CRexrothSISDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CRexrothSISDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CRexrothSISDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CRexrothSISDeviceOptions);
	}

// Address Management

BOOL CRexrothSISDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CRexrothSISAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CRexrothSISDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	UINT uTable = pSpace->m_uTable;

	if( uTable == SPCERR ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	UINT uOffset;

	UINT uItem = 256;

//	UINT uModel = pConfig->GetDataAccess("Model")->ReadInteger(pConfig);

	CString Lft;

	CString Rgt;

	CString s = "";

	CString s1 = "[SP]";

//	CString s2 = "[SPAC]";

	Text.MakeUpper();

	UINT uFind1  = Text.Find('(');

	UINT uFind2  = Text.FindRev(')');

	if( (uFind1 < uFind2 - 1) && (uFind2 < NOTHING) ) {

		Lft = Text.Left(uFind1);

		Rgt = Text.Mid(uFind1+1, uFind2 - uFind1 - 1);

		BOOL fBad = TRUE;

		if( uTable == SPCLIST ) {

			uItem = tatoi( Rgt );

			fBad = uItem > 64 || uItem == 0;
			}

		else {
			uItem = tatoi( Rgt.Mid(1) );

			s = Rgt.Left(1);

			switch( s[0] ) {

				case 'S':
					fBad = (uItem > 7);
					break;
					
				case 'P':
					if( uItem < 8 ) {

						uItem |= 8;
						fBad = FALSE;
						}
					break;

//				case 'A':
//					if( uModel == MODELECO ) s = s1;

//					else {					
//						if( uItem < 8 ) {

//							uItem |= 0x10;
//							fBad = FALSE;
//							}
//						}
//					break;

//				case 'C':
//					if( uModel == MODELECO ) s = s1;

//					else {					
//						if( uItem < 8 ) {

//							uItem |= 0x20;
//							fBad = FALSE;
//							}
//						}
//					break;

				default:
//					s = uModel == MODELECO ? s1 : s2;
					s = s1;
					break;
				}
			}

		if( fBad ) {

			CString t;

			if( uTable == SPCLIST ) {

				t.Printf("aaaa(1) - aaaa(64)");
				}

			else {
				t.Printf("aaaa(%s0) - aaaa(%s7)", s, s);
				}

			Error.Set( t,
				0
				);

			return FALSE;
			}
		}

	else {
		if( uTable == SPCLIST ) {

			Error.Set( "aaaa(nn)",
				0
				);
			}

		else {
			CString t;

//			t.Printf("aaaa(%sn)", (uModel==MODELECO) ? s1 : s2 );
			t.Printf("aaaa(%sn)", s1 );

			Error.Set( t,
				0
				);
			}

		return FALSE;
		}

	uOffset = tatoi( Lft );

	if( uOffset > 4095 ) {

		Error.Set( "0() - 4095()",
			0
			);

		return FALSE;
		}

	switch( uTable ) {

		case 1:
		case 2:
		case 3:
		case SPCLIST:
			Addr.a.m_Type = addrRealAsReal;
			break;

		case 4:
		case 5:
		case SPCERR:
			Addr.a.m_Type = addrLongAsLong;
			break;

		default:
			return FALSE;
		}

	Addr.a.m_Extra  = uOffset >> 8;

	uOffset &= 0xFF;

	Addr.a.m_Table  = uTable;

	Addr.a.m_Offset = uTable == SPCLIST ? (uOffset << 8) + uItem : (uItem << 8) + uOffset;

	return TRUE;
	}

BOOL CRexrothSISDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( Addr.a.m_Table == SPCERR ) {

		Text.Printf( "%s",
		     pSpace->m_Prefix
		     );

		return TRUE;
		}

	UINT uExtra = Addr.a.m_Extra << 8;
	UINT uF1    = Addr.a.m_Offset >> 8;
	UINT uF2    = Addr.a.m_Offset & 0xFF;

	if( Addr.a.m_Table == SPCLIST ) {

		if( !uF2 ) uF2 = 1;
				
		Text.Printf( "%s%4.4d(%d)",
			pSpace->m_Prefix,
			uExtra + uF1,
			uF2
			);

		return TRUE;
		}

	if( Addr.a.m_Table < SPCLIST ) {

		char c = 'S';

		switch( uF1 >> 3 ) {

			case 1: c = 'P'; break;

//			case 2: c = 'A'; break;
			
//			case 4: c = 'C'; break;
			}
				
		Text.Printf( "%s%4.4d(%c%d)",
		     pSpace->m_Prefix,
		     uExtra + uF2,
		     c,
		     uF1 & 0x7
		     );

		return TRUE;
		}

	return FALSE;
	}

// Implementation	

void CRexrothSISDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"PC",	"Current Value",			10,  0, 65535,	addrRealAsReal));

	AddSpace(New CSpace(2,	"PN",	"Minimum Value",			10,  0, 65535,	addrRealAsReal));

	AddSpace(New CSpace(3,	"PX",	"Maximum Value",			10,  0, 65535,	addrRealAsReal));

	AddSpace(New CSpace(4,	"PA",	"Attribute",				10,  0, 65535,	addrLongAsLong));

	AddSpace(New CSpace(5,	"PU",	"Unit",					10,  0, 65535,	addrLongAsLong));

	AddSpace(New CSpace(SPCLIST, "L0-",	"P-0- Parameter List",		10,  0, 65535,	addrRealAsReal));

	AddSpace(New CSpace(SPCERR,  "ERR",	"Latest Error",			10,  0, 0,	addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CRexrothSISAddrDialog, CStdAddrDialog);
		
// Constructor

CRexrothSISAddrDialog::CRexrothSISAddrDialog(CRexrothSISDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "RexrothElementDlg";

//	m_uModel = pConfig->GetDataAccess("Model")->ReadInteger(pConfig);
	}

// Overridables


void CRexrothSISAddrDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	UINT uTable = SPCERR;

	if( m_pSpace ) {

		uTable = m_pSpace->m_uTable;

		if( fEnable ) {

			if( uTable < SPCERR ) {

				CString L = "0000";
				CString R = uTable == SPCLIST ? "00" : "P0";

				Text.MakeUpper();

				UINT uFind1 = Text.Find('(');

				UINT uFind2 = Text.Find(')');

				if( uFind1 < NOTHING ) {

					L = Text.Left(uFind1);

					if( uFind2 < NOTHING && uFind2 > uFind1 + 1 ) {

						R = Text.Mid( uFind1+1, uFind2 - uFind1 - 1 );
						}
					}

				if( uTable < SPCLIST ) {

					switch( R[0] ) {

						case 'S':
						case 'P':
							break;

//						case 'A':
//						case 'C':
//							if( m_uModel == MODELECO ) {

//								if( R.GetLength() >= 2 ) R = "P" + R.Mid(1);

//								else R = "P0";
//								}
//							break;

						default:
							R = "P0";
							break;
						}

					GetDlgItem(2008).SetWindowText("Set      ");
					}

				else GetDlgItem(2008).SetWindowText("List Item");

				GetDlgItem(2002).SetWindowText(L);

				GetDlgItem(2005).SetWindowText(R);

				GetDlgItem(2007).SetWindowText( "IDN" );
				}

			ShowDetails();
			}
		}

	if( !m_pSpace || uTable >= SPCERR ) ClearAllWindowText();

	BOOL fEW  = m_pSpace && fEnable && (uTable < SPCERR);

	GetDlgItem(2002).EnableWindow(fEW);
		
	GetDlgItem(2004).EnableWindow(fEW);
		
	GetDlgItem(2005).EnableWindow(fEW);
		
	GetDlgItem(2006).EnableWindow(fEW);
		
	GetDlgItem(2007).EnableWindow(fEW);

	GetDlgItem(2008).EnableWindow(fEW);
	}

CString CRexrothSISAddrDialog::GetAddressText(void)
{
	if( m_pSpace && (m_pSpace->m_uTable < SPCERR) ) {

		CString Text;

		Text = GetDlgItem(2002).GetWindowText();

		Text += "(";
		
		Text += GetDlgItem(2005).GetWindowText();

		Text += ")";

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

void CRexrothSISAddrDialog::ShowDetails(void)
{
	if( !m_pSpace ) return;

	CString  Min;

	CString  Max;

	if( m_pSpace->m_uTable >= SPCERR ) {

		CStdAddrDialog::ShowDetails();

		return;
		}

	UINT uTable = m_pSpace->m_uTable;

	CAddress Addr;

	Addr.a.m_Table = uTable;

	Addr.a.m_Offset = uTable < SPCLIST ? 0 : 1;

	if( uTable < SPCLIST ) {

		CString sSet = GetDlgItem(2005).GetWindowText();

		switch( sSet[0] ) {

			case 'P': Addr.a.m_Offset = 0x800; break;
//			case 'A': if( m_uModel == MODELSYNAX ) Addr.a.m_Offset = 0x1000; break;
//			case 'C': if( m_uModel == MODELSYNAX ) Addr.a.m_Offset = 0x2000; break;
			}
		}

	Addr.a.m_Extra = 0;

	Addr.a.m_Type = GetTypeCode();

	m_pDriver->DoExpandAddress(Min, m_pConfig, Addr);

	Addr.a.m_Offset += (uTable < SPCLIST) ? 0x7FF : 0xFF3F;

	Addr.a.m_Extra = 0xF;

	m_pDriver->DoExpandAddress(Max, m_pConfig, Addr);

	GetDlgItem(3004).SetWindowText(Min);

	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText("Decimal");
	}

// Helpers

void CRexrothSISAddrDialog::ClearAllWindowText(void)
{
	GetDlgItem(2002).SetWindowText("");
	GetDlgItem(2005).SetWindowText("");
	GetDlgItem(2007).SetWindowText("");
	GetDlgItem(2008).SetWindowText("");
	}

// End of File
