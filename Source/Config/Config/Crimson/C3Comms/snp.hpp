
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SNP_HPP
	
#define	INCLUDE_SNP_HPP

/////////////////////////////////////////////////////////////////////////
//
// SNP Device Options
//

class CSNPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSNPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		CString m_Drop;
		BOOL m_fBreakFree;
		UINT m_Slot;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SNP Master Serial Driver
//

class CSNPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSNPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);  

		// Address Helpers
		BOOL CheckAlignment(CSpace *pSpace);


	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SNP-X Master Serial Driver
//

class CSNPXDriver : public CSNPDriver
{
	public:
		// Constructor
		CSNPXDriver(void);
	};

// End of File

#endif
