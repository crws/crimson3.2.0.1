
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IAIXSEL_HPP

#define	INCLUDE_IAIXSEL_HPP

#define	BB	addrBitAsBit
#define	LL	addrLongAsLong
#define	YY	addrByteAsByte
#define	RR	addrRealAsReal

#define	AN	addrNamed

// Table Addresses
#define	C09A	1
#define	C09B	2
#define	C09C	3
#define	C09D	4
#define	CD4E	5
#define	C09E	6
#define	C0B	7
#define	C0C	8
#define	C0D	9
#define	C0E	10
#define	C12A	11
#define	C12B	12
#define	C12C	13
#define	C12D	14
#define	C12E	15
#define	CCB	16
#define	CA1	17
#define	C0DB	18
#define	C0BB	19
#define	C13A	20
#define	C13B	21
#define	C13C	22
#define	C13D	23
#define	C38	24
#define	C46	25
#define	C62	26
#define	C34	27
#define	CD5E	28
#define	C35	29
#define	CXXP	30
#define	C0F	31
#define	C01A	32
#define	C01B	33
#define	C01C	34
#define	C01D	35
#define	C01E	36
#define	C44D	37
#define	C44E	38
#define	CSND	39
#define	CRCV	40
// Added Apr 07
#define	CRQAT	41
#define	CRSPST	42
#define	CRSPIN	43
#define	CRSPEC	44
#define	CRSPEN	45
#define	CRSPPL	46
// Added Feb 09
#define	C0FA	47

// Parameter Types
#define	NOPARAM		0
#define	PNTNUM		1 // Point Number
#define	PNTNUMAXIS	2 // Point Number & Axis
#define	PRTNUM		3 // Port Number
#define	FLGPRGNUM	4 // Flag Number & Program Number
#define	VARPRGNUM	5 // Variable Number & Program Number
#define	AXISPATTERN	6 // Axis Pattern
#define	AXISSCALAR	7 // Axis Number & Scalar Location
#define AXISPATNUM	8 // Axis Patter & Axis Number
#define	AXISNUM		9 // Axis Number
#define	CLASSDEV	10 // Unit Classification& Device
#define	ADDCOMMBYTE	11 // Additional Command Byte
#define	PRGNUM		12 // Program Number
#define	USERSTRING	13 // User String


// Forward Declarations
class CIaixselDeviceOptions;
class CIaixselDriver;

//////////////////////////////////////////////////////////////////////////
//
// IAI XSEL Device Options
//

class CIaixselDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIaixselDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// IAI XSEL Comms Driver
//

class CIaixselDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIaixselDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);

		// Helpers
		UINT	CheckParameters(CSpace * pSpace );

		friend class CIaixselAddrDialog;
	};

class CIaixselAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CIaixselAddrDialog(CIaixselDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowDetails(void);

		// Helpers
		UINT	GetParameters(void);
	};

// End of File

#endif
