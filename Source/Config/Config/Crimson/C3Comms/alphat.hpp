
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ALPHATERNARY_HPP
	
#define	INCLUDE_ALPHATERNARY_HPP

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Special Data Space
#define	DC	17

// Allowable Direct Commands
#define	VS	"a d g i j l m o p q r s v x z"
#define	VS1	"\nn, t do not have parameters"

//////////////////////////////////////////////////////////////////////////
//
// Alpha Gear Ternary Comms Driver
//

class CAlphaTernaryDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CAlphaTernaryDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Information Text Space for Dialog
		void	AddRequiredSpaces(BOOL fData);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Alpha Gear Ternary Address Selection
//

class CAlphaTernaryAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CAlphaTernaryAddrDialog(CAlphaTernaryDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Destructor
		~CAlphaTernaryAddrDialog(void);
		                
	protected:
		// Data Members

		// Initialization members
		CAlphaTernaryDriver * m_pDriverData;

		// Message Map
		AfxDeclareMessageMap();

		// Notification Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnDblClk(UINT uID, CWnd &Wnd);
		BOOL	OnOkay(UINT uID);
	};

// End of File

#endif
