#include "p6k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PALOOP_HPP
	
#define	INCLUDE_PALOOP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop Motion Controller Device Options
//

class CParkerAcroloopDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CParkerAcroloopDeviceOptions(void);
				
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Device;
		
	
	protected:
		
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop Motion Controller
//

class CParkerAcroloopDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CParkerAcroloopDriver(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
		
		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	
	
	protected:
		
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop TCP/IP Master Device Options
//
 
class CPAMasterTCPDeviceOptions : public CParker6KTCPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPAMasterTCPDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

	}; 

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop TCP/IP Master Driver
//

class CPAMasterTCPDriver : public CParkerAcroloopDriver
{
	public:
		// Constructor
		CPAMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Acroloop Space Wrapper
//

class CSpaceAcro : public CSpace
{
	public:
		// Constructors
		CSpaceAcro(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);
		
	};

// End of File

#endif
