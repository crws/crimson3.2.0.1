
#include "intern.hpp"

#include "emsonep.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

// Load List Functions

void CEmersonEPAddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 40, 100, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf("<%s>\t%s", CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING);

	INDEX Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	INDEX n = List.GetHead();

	if( List.GetCount() ) {

		DoDesignatedHeader(&ListBox, List, n);

		switch( m_uTopLevel ) {

			case TLMB:	Find = DoModbus(&ListBox, List, n);	break;

			case TLKY:	Find = DoKeyWord(&ListBox, List, n);	break;

			case TLMC:
				Find = DoMC(&ListBox, List, n);
				break;

			case TLMF:
				if( IsCommanderP() ) Find = DoSPFnc(&ListBox, List, n);

				else Find = DoIBFnc(&ListBox, List, n);
				break;
			}

		if( !(m_uTopLevel == TMCH) ) DoRemainingHeaders(&ListBox, List, n);
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSpaceChange(1001, ListBox);
	}

INDEX CEmersonEPAddrDialog::SetEntry(CListBox *pBox, CSpace *pSpace, INDEX n, BOOL *pfDef)
{
	INDEX Find = INDEX(NOTHING);

	LoadEntry( pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n) );

	if( pSpace == m_pSpace ) Find = n;

	else {
		if( m_fListTypeChg ) {

			Find = n;

			*pfDef = TRUE;

			m_fListTypeChg = FALSE;
			}
		}

	return Find;
	}

void CEmersonEPAddrDialog::LoadEntry(CListBox *pBox, CString Prefix, CString Caption, DWORD n)
{
	if( Caption[0] == '*' ) return; // list entry is only for Validate Tags

	CString Entry;

	Entry.Printf("%s\t%s", Prefix, Caption);

	pBox->AddString(Entry, n);
	}

void CEmersonEPAddrDialog::DoDesignatedHeader(CListBox *pBox, CSpaceList &List, INDEX n)
{
	if( m_fListTypeChg && m_KWText && m_KWText[0] ) return;

	while( !(List.Failed(n)) ) {

		CSpace *p;

		p = List[n];

		UINT t = p->m_uTable;

		BOOL fShow = m_uTopLevel == TLMB ? t == TLMB : t == m_uAllowList;

		if( IsHeaderSpace(t) && fShow ) {

			m_fListTypeChg = p == m_pSpace;

			LoadEntry( pBox, p->m_Prefix, p->m_Caption, DWORD(n) );

			return;
			}

		List.GetNext(n);
		}
	}

INDEX CEmersonEPAddrDialog::DoModbus(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *pSpace;

	INDEX Find        = INDEX(NOTHING);
	INDEX DefaultFind = INDEX(NOTHING);

	UINT Start = GMBO;
	UINT End   = GMBF;

	switch( m_uAllowList ) {

		case THDO:
			End   = GMBO;
			break;

		case THDI:
			Start = GMBI;
			End   = GMBI;
			break;

		case THAI:
			Start = GMBA;
			End   = GMBC;
			break;

		case THHR:
			Start = GMBD;
			End   = GMBF;
			break;
		}

	for( UINT k = Start; k <= End; k++ ) {

		INDEX n = List.GetHead();

		BOOL fDone = FALSE;

		while( !fDone ) {

			pSpace = List[n];

			if( k == pSpace->m_uTable ) {

				fDone = TRUE;

				LoadEntry(pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n));

				if( pSpace == m_pSpace ) Find = n;

				if( k == Start ) DefaultFind = n;
				}

			List.GetNext(n);
			}
		}

	return Find != INDEX(NOTHING) ? Find : DefaultFind;
	}

INDEX CEmersonEPAddrDialog::DoKeyWord(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *pSpace;

	INDEX Find        = INDEX(NOTHING);
	INDEX DefaultFind = INDEX(NOTHING);

	while( !List.Failed(n) ) {

		pSpace = List[n];

		if( !m_KWText[0] ) {

			if( pSpace->m_uTable == TLKY ) return n;

			List.GetNext(n);

			continue;
			}

		if( !IsHeaderSpace(pSpace->m_uTable) && ValidPrefix(UINT(pSpace->m_Prefix[0])) ) {

			CString p = pSpace->m_Prefix;
			CString c = pSpace->m_Caption;

			if( DefaultFind == INDEX(NOTHING) ) {

				if( IsCommanderP() ) DefaultFind = SetSPDefaultFind(pSpace, n);

				else DefaultFind = SetIBDefaultFind(pSpace, n);
				}

			if( IsGenericMB(pSpace->m_uTable) ) {

				return Find != INDEX(NOTHING) ? Find : DefaultFind;
				}

			c.MakeLower();

			UINT c0  = p[0];

			BOOL fList = FALSE;

			if( c.Find(m_KWText) != NOTHING ) {

				if( IsCommanderP() ) {

					if( isdigit(c0) ) fList = TRUE;
					}

				else {
					fList = GetIBItemType(pSpace->m_uTable, pSpace->m_uMinimum);
					}
						
				if( fList ) {

					LoadEntry( pBox, p, pSpace->m_Caption, DWORD(n) ); // restore case

					if( m_fListTypeChg ) {

						Find = n;

						m_fListTypeChg = FALSE;
						}
					}
				}
			}

		List.GetNext(n);
		}

	return Find != INDEX(NOTHING) ? Find : DefaultFind;
	}

INDEX CEmersonEPAddrDialog::DoMC(CListBox *pBox, CSpaceList &List, INDEX n)
{
	UINT uLoaded = 0;

	m_pDriverData->GetLoaded(&uLoaded);

	if( !(uLoaded & 8) ) {

		m_uAllowList = THHR;
		m_uTopLevel  = TLMB;
		m_uListSel   = SELNM;

		return DoModbus(pBox, List, n);
		}

	CSpace *pSpace;

	INDEX Find = INDEX(NOTHING);

	BOOL fDone = FALSE;

	while( !fDone && !List.Failed(n) ) {

		pSpace    = List[n];

		CString p = pSpace->m_Prefix;

		if( p.GetLength() == 4 && p.Left(2) == "MC" ) {

			BOOL fDef = FALSE;

			INDEX i   = SetEntry(pBox, pSpace, n, &fDef);

			if( Find == INDEX(NOTHING) ) Find = i;

			if( p == "MCH" ) fDone = TRUE;
			}

		List.GetNext(n);
		}

	return Find;
	}

INDEX CEmersonEPAddrDialog::DoIBFnc(CListBox *pBox, CSpaceList &List, INDEX n)
{
	return IsValidAlpha0() ? SortIBAlpha(pBox, List, n) : SortIBNumer(pBox, List, n);
	}

INDEX CEmersonEPAddrDialog::SortIBAlpha(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *pSpace;

	INDEX Find        = INDEX(NOTHING);
	INDEX DefaultFind = INDEX(NOTHING);

	while( !List.Failed(n) ) {

		pSpace    = List[n];

		CString p = pSpace->m_Prefix;

		if( p[0] == '_' || pSpace->m_Caption[0] == '*' ) {

			return Find != INDEX(NOTHING) ? Find : DefaultFind;
			}

		if( MatchIBAllow(pSpace) ) {

			BOOL fDef = FALSE;

			INDEX i   = SetEntry(pBox, pSpace, n, &fDef);
			
			if( pSpace == m_pSortSpace ) Find = n;

			else {
				if( fDef ) DefaultFind = n;

				else {
					if( i != INDEX(NOTHING) ) Find = i;
					}
				}
			}

		List.GetNext(n);
		}

	return Find != INDEX(NOTHING) ? Find : DefaultFind;
	}

INDEX CEmersonEPAddrDialog::SortIBNumer(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *p;

	UINT uSize;

	UINT * pA = GetIBArray(&uSize);

	if( !uSize ) return INDEX(NOTHING);

	uSize /= sizeof(UINT);

	INDEX ix[sizeof(m_uINX)/sizeof(UINT)];

	UINT i;

	for( i = 0; i < uSize; i++ ) {

		ix[i] = INDEX(NOTHING);
		}

	for( i = 0; i < uSize; i++ ) {

		n = List.GetHead();

		while( !List.Failed(n) ) {

			p = List[n];

			if( p->m_Caption[0] != '*' ) {

				UINT t = p->m_uTable;
				UINT m = p->m_uMinimum;

				if( GetIBItemType(t, m) == m_uAllowList && !isdigit(p->m_Prefix[0]) ) {

					UINT m1;

					if( GetAddressFromItem(t, m, &m1) ) {

						m = m1;
						}

					if( pA[i] == m ) {

						ix[i] = n;

						break;
						}
					}
				}

			List.GetNext(n);
			}
		}

	INDEX DefaultFind = INDEX(NOTHING);

	INDEX Find = INDEX(NOTHING);

	for( i = 0; i < uSize; i++ ) {

		n = ix[i];

		if( n != INDEX(NOTHING) ) {

			if( DefaultFind == INDEX(NOTHING) ) DefaultFind = n;

			CSpace * pSpace = List[n];

			BOOL fDef = FALSE;

			INDEX i   = SetEntry(pBox, pSpace, n, &fDef);

			if( pSpace == m_pSortSpace ) Find = n;

			else {
				if( i != INDEX(NOTHING) ) Find = i;
				}
			}
		}

	return Find != INDEX(NOTHING) ? Find : DefaultFind;
	}

INDEX CEmersonEPAddrDialog::DoSPFnc(CListBox *pBox, CSpaceList &List, INDEX n)
{
	if( IsValidAlpha0() || (m_uMenuSel >= 15 && m_uMenuSel <= 17 ) ) {

		return SortSPAlpha(pBox, List, n);
		}

	return SortSPNumer(pBox, List, n);
	}

INDEX CEmersonEPAddrDialog::SortSPAlpha(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *pSpace;

	INDEX Find        = INDEX(NOTHING);
	INDEX DefaultFind = INDEX(NOTHING);

	while( !List.Failed(n) ) {

		pSpace = List[n];

		CString Prefix = pSpace->m_Prefix;
		
		UINT uMenu;

		if( Prefix[0] == '_' || pSpace->m_Caption[0] == '*' ) {

			return Find != INDEX(NOTHING) ? Find : DefaultFind;
			}

		if( GetMenuNumber(Prefix, pSpace->m_uMinimum, &uMenu) ) {

			if( uMenu == m_uMenuSel ) {

				BOOL fDef      = FALSE;

				m_fListTypeChg = DefaultFind == INDEX(NOTHING);

				INDEX i        = SetEntry(pBox, pSpace, n, &fDef);

				if( pSpace == m_pSortSpace ) Find = n;

				else {
					if( fDef ) DefaultFind = n;

					else {
						if( i != INDEX(NOTHING) ) Find = i;
						}
					}
				}
			}

		List.GetNext(n);
		}

	return Find != INDEX(NOTHING) ? Find : DefaultFind;
	}

INDEX CEmersonEPAddrDialog::SortSPNumer(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *p;

	INDEX ix[100];

	UINT i;

	for( i = 0; i < 100; i++ ) {

		ix[i] = INDEX(NOTHING);
		}

	INDEX Find = INDEX(NOTHING);

	while( !List.Failed(n) ) {

		p = List[n];

		if( p->m_Caption[0] != '*' ) {

			UINT uMenu;

			if( GetMenuNumber(p->m_Prefix, p->m_uMinimum, &uMenu) ) {

				if( uMenu == m_uMenuSel ) {

					ix[p->m_uMinimum % 100] = n;
					}
				}
			}

		List.GetNext(n);
		}

	INDEX DefaultFind = INDEX(NOTHING);

	for( i = 0; i < 100; i++ ) {

		n = ix[i];

		if( n != INDEX(NOTHING) ) {

			if( DefaultFind == INDEX(NOTHING) ) DefaultFind = n;

			CSpace * pSpace = List[n];

			BOOL fDef = FALSE;

			INDEX i   = SetEntry(pBox, pSpace, n, &fDef);

			if( pSpace == m_pSortSpace ) Find = n;

			else {
				if( i != INDEX(NOTHING) ) Find = i;
				}
			}
		}

	return Find != INDEX(NOTHING) ? Find : DefaultFind;
	}

void CEmersonEPAddrDialog::DoRemainingHeaders(CListBox *pBox, CSpaceList &List, INDEX n)
{
	CSpace *pSpace;

	BOOL fKeyList = m_KWText && m_KWText[0];

	while( !List.Failed(n) ) {

		pSpace = List[n];

		UINT t = pSpace->m_uTable;

		if( IsHeaderSpace(t) && (t != m_uAllowList || fKeyList) ) {

			BOOL fShow = FALSE;

			if( IsPreM() ) {

				if( m_uTopLevel == TLMB ) {

					fShow  = t == TLMF;
					fShow |= IsModbusHeader(t);
					}

				else {
					fShow  = t == TLMB;
					fShow |= (!fKeyList && IsSortHeader(t));
					fShow |= IsComHeader(t);
					fShow |= IsHeaderForIBDevice(t);
					}
				}

			else {
				fShow = IsModbusHeader(t);
				}

			if( fShow ) LoadEntry( pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n) );
			}

		List.GetNext(n);
		}

	if( fKeyList ) m_KWText = "";

	n = List.GetHead();

	while( !List.Failed(n) ) {

		pSpace = List[n];

		if( pSpace->m_uTable == TLKY ) {

			LoadEntry( pBox, pSpace->m_Prefix, pSpace->m_Caption, DWORD(n) );
			}

		List.GetNext(n);
		}
	}

INDEX CEmersonEPAddrDialog::SetSPDefaultFind(CSpace *pSpace, INDEX n)
{
	UINT u;

	GetMenuNumber(pSpace->m_Prefix, pSpace->m_uMinimum, &u);

	return  u == m_uMenuSel ? n : INDEX(NOTHING);
	}

INDEX CEmersonEPAddrDialog::SetIBDefaultFind(CSpace *pSpace, INDEX n)
{
	return GetIBItemType(pSpace->m_uTable, pSpace->m_uMinimum) ? n : INDEX(NOTHING);
	}

// End of File
