
#include "intern.hpp"

#include "simovert.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert via USS Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSimovertSerialDeviceOptions, CUIItem);
				   
// Constructor

CSimovertSerialDeviceOptions::CSimovertSerialDeviceOptions(void)
{
	m_Drop     = 1;

	m_PZD      = 2;

	m_PKW      = 2;

	m_ModelUSS = MODELVERT;
	}

// UI Managament

void CSimovertSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{

	}

// Download Support

BOOL CSimovertSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_PZD));

	Init.AddByte(BYTE(m_PKW));

	Init.AddByte(BYTE(m_ModelUSS));
	
	return TRUE;
	}

// Meta Data Creation

void CSimovertSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(PZD);

	Meta_AddInteger(PKW);

	Meta_AddInteger(ModelUSS);
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert via USS Driver
//

// Instantiator

ICommsDriver *	Create_SimovertSerialDriver(void)
{
	return New CSimovertSerialDriver;
	}

// Constructor

CSimovertSerialDriver::CSimovertSerialDriver(void)
{
	m_wID		= 0x3376;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
			
	m_DriverName	= "USS Drives";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Siemens USS Drives";	

	m_DevRoot	= "USS";

	m_uPZD		= 10;

	AddSpaces();
	}

// Binding Control

UINT CSimovertSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSimovertSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CSimovertSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSimovertSerialDeviceOptions);
	}

// Implementation     

void CSimovertSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(        1, "P",    CString(IDS_SIMO_PARAM),			10,  0,  9999, addrWordAsWord));

	AddSpace(New CSpace(	    2, "LP",   CString(IDS_SIMO_PARAM) + " (32-bit)",	10,  0,  9999, addrLongAsLong));

	AddSpace(New CSpace(addrNamed, "STW",  CString(IDS_SIMO_CTRL),			10,  1,     0, addrWordAsWord));

	AddSpace(New CSpace(addrNamed, "HSW",  CString(IDS_SIMO_FREQ),			10,  2,     0, addrWordAsWord));

	AddSpace(New CSpace(	   10, "SWx",  "Process Data Out (Write Only)",		10,  1,    13, addrWordAsWord, addrWordAsLong)); 
	
	AddSpace(New CSpace(addrNamed, "SEND", CString(IDS_SIMO_SEND),			10,  3,     0, addrWordAsWord));

	AddSpace(New CSpace(addrNamed, "ZSW",  CString(IDS_SIMO_STAT),			10,  4,     0, addrWordAsWord));

	AddSpace(New CSpace(addrNamed, "HIW",  CString(IDS_SIMO_REAL),			10,  5,     0, addrWordAsWord));

	AddSpace(New CSpace(       11, "IWx",  "Process Data In (Read Only)",		10,  1,    13, addrWordAsWord, addrWordAsLong)); 
	
	AddSpace(New CSpace(        3, "IW0",  "Indexed P-16 0000-0999 (SimoReg only)",	10,  0, 63999, addrWordAsWord));

	AddSpace(New CSpace(        4, "IW1",  "Indexed P-16 1000-1999 (SimoReg only)",	10,  0, 63999, addrWordAsWord));

	AddSpace(New CSpace(        5, "IW2",  "Indexed P-16 2000-2999 (SimoReg only)",	10,  0, 63999, addrWordAsWord));

	AddSpace(New CSpace(        6, "IL0",  "Indexed P-32 0000-0999 (SimoReg only)",	10,  0, 63999, addrLongAsLong));

	AddSpace(New CSpace(        7, "IL1",  "Indexed P-32 1000-1999 (SimoReg only)",	10,  0, 63999, addrLongAsLong));

	AddSpace(New CSpace(        8, "IL2",  "Indexed P-32 2000-2999 (SimoReg only)",	10,  0, 63999, addrLongAsLong));

	AddSpace(New CSpace(	    9, "PZD",  "Additional Process Data PZD3...PZD10",	10,  3,    10, addrWordAsWord));
	}

// Address Management

BOOL CSimovertSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	m_uPZD = pConfig->GetDataAccess("PZD")->ReadInteger(pConfig);

	CSimovertAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CSimovertSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uExtra = 0;

	CString Type = StripType(pSpace, Text);

	if( Type == "WORD" || Type == "LONG" ) {

		if( pSpace->m_uTable < addrNamed ) {

			Addr.a.m_Table  = pSpace->m_uTable;
			Addr.a.m_Type   = pSpace->m_uType;

			if( Text.Right(1) == "E" ) {

				Text   = Text.Left(Text.GetLength() - 1);

				uExtra = 1;
				}

			UINT uPar = tatoi(Text);

			if( pSpace->m_uTable >= 3 && pSpace->m_uTable <= 8 ) {

				uPar       = min(uPar, 999);

				if( uPar > 499 ) {

					uExtra |= 2;
					uPar   -= 500;
					}

				Addr.a.m_Offset = uPar << 7;

				Addr.a.m_Extra  = uExtra;

				UINT uInx       = 0;

				UINT uFind      = Text.Find('(');

				if( uFind < NOTHING ) {

					uInx = min(tatoi(Text.Mid(uFind+1)), 127);

					Addr.a.m_Offset += uInx ;
					}

				return TRUE;
				}

			if( pSpace->m_uTable == 9 ) {

				if( m_uPZD > 2 ) {

					if( uPar <  3 ) uPar = 3;

					if( uPar > m_uPZD ) uPar = m_uPZD;
					}

				else {
					uPar = 0;
					}

				Addr.a.m_Offset = uPar;

				Addr.a.m_Extra  = uExtra;

				return TRUE;
				}
			}

		Text += "." + Type;

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			Addr.a.m_Extra = uExtra;

			return TRUE;
			}

		return FALSE;
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CSimovertSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.a.m_Table >= 3 && Addr.a.m_Table <= 8 ) {

		UINT uPar      = Addr.a.m_Offset >> 7;
		UINT uInx      = Addr.a.m_Offset &  0x7F;

		if( Addr.a.m_Extra & 2 ) {

			uPar += 500;
			}

		CString s;

		CSpace *pSpace = GetSpace(Addr);

		if( !pSpace ) {

			return FALSE;
			}

		Text.Printf( "%s%3.3d(%d)",

			pSpace->m_Prefix,

			uPar,

			uInx
			);
		}

	else {
		if( !(CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr)) ) {

			return FALSE;
			}
		}

	if( Addr.a.m_Table < addrNamed  ) {

		if( Addr.a.m_Extra & 1 ) {

			Text += 'E';
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert via USS Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSimovertAddrDialog, CStdAddrDialog);
		
// Constructor

CSimovertAddrDialog::CSimovertAddrDialog(CSimovertSerialDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element     = "SimovertElementDlg";
	}

// Message Map
AfxMessageMap(CSimovertAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(CStdAddrDialog)
	};

BOOL CSimovertAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	BOOL f = CStdAddrDialog::OnInitDialog(Focus, dwData);

	UpdateMax();

	return f;
	}

void CSimovertAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	UpdateMax();
	}

// Overridables

void CSimovertAddrDialog::SetAddressText(CString Text)
{
	CEditCtrl &Index = (CEditCtrl &) GetDlgItem(2002);

	CButton   &Check = (CButton   &) GetDlgItem(2004);

	if( Text.IsEmpty() ) {

		Check.SetCheck(FALSE);

		Index.SetWindowText("");

		Check.EnableWindow(FALSE);

		Index.EnableWindow(FALSE);
		}
	else {
		if( Text.Right(1) == "E" ) {

			Text = Text.Left(Text.GetLength() - 1);

			Check.SetCheck(TRUE);
			}
		else
			Check.SetCheck(FALSE);

		Index.SetWindowText(Text);

		Check.EnableWindow(TRUE);

		Index.EnableWindow(TRUE);
		}
	}

CString CSimovertAddrDialog::GetAddressText(void)
{
	CEditCtrl &Index = (CEditCtrl &) GetDlgItem(2002);

	CButton   &Check = (CButton   &) GetDlgItem(2004);

	if( Index.IsEnabled() ) {

		CString Text = Index.GetWindowText();

		if( Check.IsChecked() ) {

			Text += "E";
			}

		return Text;
		}

	return "";
	}

// Helpers
void CSimovertAddrDialog::UpdateMax(void)
{
	if( m_pSpace ) {

		if( m_pSpace->m_uTable >= 3 && m_pSpace->m_uTable <= 8 ) {

			CString Max;

			Max.Printf( "%s999(127)", m_pSpace->m_Prefix );

			GetDlgItem(3006).SetWindowText(Max);
			}
		}
	}

// End of File
