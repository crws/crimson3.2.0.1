
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DUALISDATADRIVER_HPP
	
#define	INCLUDE_DUALISDATADRIVER_HPP

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Data Driver
//

class CIfmDualisData : public CStdCommsDriver
{
	public:
		// Constructor
		CIfmDualisData(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);

	protected:

		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
