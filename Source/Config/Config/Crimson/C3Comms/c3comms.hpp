
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_C3COMMS_HPP
	
#define INCLUDE_C3COMMS_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hpp>

#include <c3comp.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3comms.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef	DLLAPI

#ifdef	PROJECT_C3COMMS

#define DLLAPI __declspec(dllexport)

#else

#define DLLAPI __declspec(dllimport)

#pragma comment(lib, "c3comms.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Physical Layer Codes
//

enum Physical
{
	physicalNone	= 0,
	physicalRS232	= 1,
	physicalRS422	= 2,
	physicalRS485	= 3,
	physical20mA	= 4,
	physicalProgram = 5,
	};

//////////////////////////////////////////////////////////////////////////
//
// Parity Codes
//

enum Parity
{
	parityNone	= 0,
	parityOdd	= 1,
	parityEven	= 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Port Modes
//

enum Mode
{
	modeTwoWire  = 0x00,
	modeFourWire = 0x01,
	};

//////////////////////////////////////////////////////////////////////////
//								
// Driver Types
//

enum DriverType
{
	driverMaster	= 1,
	driverSlave	= 2,
	driverRawPort	= 3,
	driverModem	= 4,
	driverRack	= 5,
	driverCamera	= 6,
	driverStreamer	= 7,
	driverHoneywell = 8,
	driverUtility   = 9,
	driverWifi	= 10,
	};

//////////////////////////////////////////////////////////////////////////
//								
// Comms Driver Flags
//

enum
{
	dflagNone	= 0x00,
	dflagNoMapping	= 0x01,
	dflagOneDevice	= 0x02,
	dflagImport	= 0x04,
	dflagBigBlocks  = 0x08,
	dflagCrimson3   = 0x10,
	dflagRemotable	= 0x20,
	dflagRpcMaster	= 0x40,
	dflagRpcSlave	= 0x80,
	};

//////////////////////////////////////////////////////////////////////////
//								
// Driver String IDs
//

enum DriverString
{
	stringManufacturer = 1,
	stringDriverName   = 2,
	stringVersion	   = 3,
	stringShortName    = 4,
	stringDevRoot	   = 5
	};

//////////////////////////////////////////////////////////////////////////
//								
// Driver Bindings
//

enum DriverBinding
{
	bindEthernet	 = 1,
	bindStdSerial	 = 2,
	bindRawSerial	 = 3,
	bindCAN 	 = 4,
	bindRack	 = 5,
	bindProfibus	 = 6,
	bindFireWire	 = 7,
	bindDeviceNet	 = 8,
	bindCatLink	 = 9,
	bindModem	 = 10,
	bindMPI 	 = 11,
	bindJ1939	 = 12,
	bindDnp3	 = 13,
	bindDnp3Ip	 = 14,
	bindTest	 = 15,
	bindTestIp	 = 16,
	bindRemote	 = 17,
	bindBluetooth    = 18,
	bindWifi	 = 19,
	};

//////////////////////////////////////////////////////////////////////////
//								
// Binding Information
//

struct CBindInfo
{
	UINT	m_Binding;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Binding Information for Ethernet
//

struct CBindEther : public CBindInfo
{
	UINT	m_UDPCount;
	UINT	m_TCPCount;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Binding Information for Serial Ports
//

struct CBindSerial : public CBindInfo
{
	UINT	m_Physical;
	UINT	m_BaudRate;
	UINT	m_DataBits;
	UINT	m_StopBits;
	UINT	m_Parity;
	UINT	m_Mode;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Binding Information for CAN Ports
//

struct CBindCAN : public CBindInfo
{
	UINT	m_BaudRate;
	UINT	m_Terminate;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Binding Information for Profibus Ports
//

struct CBindProfibus : public CBindInfo
{
	UINT	m_StationAddress;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Binding Information for DeviceNet Ports
//

struct CBindDevNet : public CBindInfo
{
	UINT	m_BaudRate;
	UINT	m_Mac;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Binding Information for MPI Card Ports
//

struct CBindMPI : public CBindInfo
{
	UINT	m_StationAddress;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Comms Item Address
//

struct CAddress
{
	union {
		struct {
			DWORD	m_Offset : 16;
			DWORD	m_Table  :  8;
			DWORD	m_Extra  :  4;
			DWORD	m_Type	 :  4;
			} a;
		
		DWORD m_Ref;
		};
	};

//////////////////////////////////////////////////////////////////////////
//								
// Address Table Codes
//

enum AddrTable
{
	addrEmpty	= 0x00,
	addrNamed	= 0xF0,
	};

//////////////////////////////////////////////////////////////////////////
//								
// Address Type Codes
//

enum AddrType
{
	addrBitAsBit	= 0x00,
	addrBitAsByte	= 0x01,
	addrBitAsWord	= 0x02,
	addrBitAsLong	= 0x03,
	addrBitAsReal	= 0x04,
	addrByteAsByte	= 0x05,
	addrByteAsWord	= 0x06,
	addrByteAsLong	= 0x07,
	addrByteAsReal	= 0x08,
	addrWordAsWord	= 0x09,
	addrWordAsLong	= 0x0A,
	addrWordAsReal	= 0x0B,
	addrLongAsLong	= 0x0C,
	addrLongAsReal	= 0x0D,
	addrRealAsReal	= 0x0E,
	addrReserved	= 0x0F,
	};

//////////////////////////////////////////////////////////////////////////
//								
// Address Listing Data
//

struct CAddrData
{
	CString 	m_Name;
	CAddress	m_Addr;
	BOOL		m_fPart;
	BOOL		m_fRead;
	UINT		m_uData;
	CString 	m_Type;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Non-COM Base Interface
//

#if !defined(IBASE_DEFINED)

#define IBASE_DEFINED

interface IBase
{
	// Object Destruction

	virtual UINT	Release 	(	void
						) = 0;
	};

#endif

//////////////////////////////////////////////////////////////////////////
//								
// Tag Creation Interface
//

interface IMakeTags
{
	// Management

	virtual BOOL InitImport		(	CString   Folder,
						BOOL      fMore,
						CFilename File
						) = 0;

	virtual void ImportDone		(	BOOL fSort
						) = 0;

	// Advanced

	virtual BOOL GetInitData	(	CInitData * &pInit
						) = 0;
	
	virtual void SetWriteBack	(	CString Code
						) = 0;

	// Folders

	virtual BOOL AddRootFolder	(	CString Class,
						CString Name,
						CString Desc
						) = 0;
	

	virtual void AddFolder		(	CString Class,
						CString Name,
						CString Desc
						) = 0;
	

	virtual void AddFolder		(	CString Class,
						CString Name
						) = 0;

	virtual void EndFolder		(	void
						) = 0;

	// Tag Creation
	
	virtual BOOL AddReal		(	CString Tag,
						CString Label,
						CString Value,
						UINT	Extent,
						UINT	Write,
						CString	Min,
						CString	Max,
						UINT	DP
						) = 0;

	virtual BOOL AddInt		(	CString Tag,
						CString Label,
						CString Value,
						UINT	Extent,
						UINT	Write,
						CString	Min,
						CString	Max,
						UINT	DP,
						UINT    TreatAs
						) = 0;

	virtual BOOL AddFnum		(	CString Tag,
						CString Label,
						CString Value,
						UINT	Extent,
						UINT	Write,
						CString Enum
						) = 0;

	virtual BOOL AddEnum		(	CString Tag,
						CString Label,
						CString Value,
						UINT	Extent,
						UINT	Write,
						CString Enum
						) = 0;

	virtual BOOL AddFlag		(	CString Tag,
						CString Label,
						CString Value,
						UINT	Extent,
						UINT	TreatAs,
						UINT	Write,
						CString State0,
						CString State1
						) = 0;

	virtual BOOL AddText		(	CString Tag,
						CString Label,
						CString Value,
						UINT    Len,
						UINT	Extent,
						UINT	Write,
						UINT    Encode
						) = 0;

	virtual BOOL AddSpec		(	CString Tag,
						CString Label,
						CString Value,
						UINT	Extent,
						UINT	Write,
						UINT	Spec
						) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Basic Driver Interface
//

interface IDriver : public IBase
{
	// Driver Data

	virtual WORD	GetID		(	void
						) = 0;

	virtual UINT	GetType 	(	void
						) = 0;

	virtual CString GetString	(	UINT ID
						) = 0;

	virtual UINT	GetFlags	(	void
						) = 0;

	// Binding Control

	virtual UINT	GetBinding	(	void
						) = 0;

	virtual void	GetBindInfo	(	CBindInfo &Info
						) = 0;

	// Configuration

	virtual CLASS	GetDriverConfig (	void
						) = 0;

	// Notifications

	virtual void	NotifyExtent	(	CAddress const & Addr,
						INT nSize,
						CItem * pConfig
						) = 0;

	virtual void	NotifyInit	(	CItem * pConfig
						) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Comms Driver Interface
//

interface ICommsDriver : public IDriver
{
	// Configuration

	virtual CLASS	GetDeviceConfig   (	void
						) = 0;

	// Tag Import

	virtual BOOL	MakeTags	(	IMakeTags	* pTags,
						CItem           * pConfig,
						CItem           * pDrvCfg,
						CString		  DevName
						) = 0;
						
	// Address Management

	virtual BOOL	ParseAddress	  (	CError	        & Error,
						CAddress        & Addr,
						CItem	        * pConfig,
						CString 	  Text
						) = 0;

	virtual BOOL	ParseAddress	  (	CError		& Error,
						CAddress	& Addr,
						CItem		* pConfig,
						CString		  Text,
						CItem	        * pDrvCfg
						) = 0;

	virtual BOOL	ExpandAddress	  (	CString         & Text,
						CItem	        * pConfig,
						CAddress const  & Addr
						) = 0;

	virtual BOOL    ExpandAddress	  (	CString		& Text,
						CItem		* pConfig,
						CAddress const  & Addr,
						CItem		* pDrvCfg
						) = 0;

	virtual BOOL	SelectAddress	  (	HWND		  hWnd,
						CAddress        & Addr,
						CItem	        * pConfig,
						BOOL		  fPart
						) = 0;


	virtual BOOL	SelectAddress	  (	HWND		  hWnd,
						CAddress        & Addr,
						CItem	        * pConfig,
						BOOL		  fPart,
						CItem	        * pDrvCfg
						) = 0;

	virtual BOOL	ListAddress	  (	CAddrData       * pRoot,
						CItem	        * pConfig,
						UINT		  uItem,
						CAddrData       & Data
						) = 0;

	virtual BOOL	IsReadOnly	  (	CItem	        * pConfig,
						CAddress const  & Addr
						) = 0;

	virtual BOOL	IsAddrNamed	  (	CItem	        * pConfig,
						CAddress const  & Addr
						) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Driver Access APIs
//

DLLAPI void	      C3AllowBetaDrivers(void);

DLLAPI void	      C3AllowDriverGroup(UINT uGroup, CString const &Model);

DLLAPI BOOL	      C3IsDriverAllowed(UINT ID);

DLLAPI UINT	      C3SelectDriver(HWND hWnd, UINT &ID, UINT Binding, UINT BindMask);

DLLAPI ICommsDriver * C3EnumDrivers(UINT n);

DLLAPI ICommsDriver * C3CreateDriver(UINT ID);

DLLAPI void	      C3GetTypeInfo(UINT uType, CTypeDef &Type);

DLLAPI UINT	      C3GetTypeScale(UINT uType);

DLLAPI UINT	      C3GetTypeTableBits(UINT uType);

DLLAPI UINT	      C3GetTypeLocalBits(UINT uType);

// End of File

#endif
