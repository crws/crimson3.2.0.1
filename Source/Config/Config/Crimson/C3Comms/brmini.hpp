
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BRMININET_HPP
	
#define	INCLUDE_BRMININET_HPP

/////////////////////////////////////////////////////////////////////////
//
//  B&R Mininet Serial Device Options
//

class CBRMininetSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBRMininetSerialDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// B&R Mininet Driver
//

class CBRMininetSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBRMininetSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	}; 

/*
//////////////////////////////////////////////////////////////////////////
//
// B&R Mininet TCP/IP Master Driver Options
//

class CBRMininetTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBRMininetTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

class CBRMininetTCPDriver : public CBRMininetSerialDriver
{
	public:
		// Constructor
		CBRMininetTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
	};
*/

// End of File

#endif
