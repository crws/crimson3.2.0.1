
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// UI for CDualisVisionSensorDeviceOptions
//

CDualisVisionSensorDeviceOptions_Schema RCDATA
BEGIN
	L"IP,IP Address,,IPAddress/IPAddress,,"
	L""
	L"\0"

	L"Port,TCP Port,,Integer/EditBox,|0||1|65535,"
	L""
	L"\0"

	L"Time4,Image Timeout,,Integer/EditBox,|0|s|0|120,"
	L"Indicate the period for which the driver will wait for an image to arrive "
	L"before assuming there is a problem with the link. In systems where the camera "
	L"is triggered only periodically, this value should be set higher than the longest "
	L"time during which an image might not arrive. Too high a value will result in "
	L"a longer delay when detecting a broken link."
	L"\0"

	L"Start,Start String,,String/EditBox,31|start,"
	L"Set the Start string used in the Result message. Set this to match that configured "
	L"in the target device."
	L"\0"

	L"Stop,Stop String,,String/EditBox,31|stop,"
	L"Set the Stop string used in the Result message. Set this to match that configured "
	L"in the target device."
	L"\0"

	L"Separator,Separator String,,String/EditBox,31|#,"
	L"Set the Separator string used in the Result message. Set this to match that configured "
	L"in the target device."
	L"\0"

	L"Image,Image Access Method,,Enum/DropDown,Result Message|Last Image (Command I)|Last Bad Image (Command F)|Last Result (Command R)|Release Trigger (Command T),"
	L""
	L"\0"

	L"Protocol,Protocol Version,,Integer/EditBox,|0||1|3,"
	L"Set the Protocol Version the driver is to use.  Set this to match that configured in the device."
	L"\0"

	"\0"
END

CDualisVisionSensorCameraDeviceOptions_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Target Device,IP,Port,Time4\0"
	L"G:1,root,Process Interface,Protocol,Start,Stop,Separator,Image\0"
	L"\0"
END

CDualisVisionSensorDataDeviceOptions_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Target Device,IP,Port\0"
	L"G:1,root,Process Interface,Protocol,Start,Stop,Separator\0"
	L"\0"
END

// End of File
