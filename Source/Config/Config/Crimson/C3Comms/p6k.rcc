
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "p6k.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "p6k.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "p6k.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "p6k.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CParker6KDeviceOptions
//

CParker6KDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|99,"
	"Select the drop number of the device to be addressed."
	"\0"

	"InitTime,Parker Initialization Time,,CUIEditInteger,|0|ms|1|65535,"
	"Indicate the amount of time that is required for initialization by "
	"the Parker 6K at power up."
	"\0"

	"Device,Controller,,CUIDropDown,6K Series|Gem6K Series,"
	"Select the series of the target device."
	"\0"

	"\0"
	
END

CParker6KDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device,Device,Drop,InitTime\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CParker6KTCPDeviceOptions
//

CParker6KTCPDeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the Parker device."
	"\0"

	"Socket,TCP Port,,CUIEditInteger,|0||1|9999,"
	"Indicate the TCP port number on which the Parker protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the Parker driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the Parker server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a Parker request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"TimeWD,Timeout,,CUIEditInteger,|0|sec|0|250,"
	"Indicate the period for which the Parker will base a connection timeout upon."
	"  This setting is helpful to quickly regain ethernet communications after "
	"a database download.  A value of 0 will disable the watchdog."
	"\0"

	"TickWD,Ticker,,CUIEditInteger,|0||0|50,"
	"Indicate the number of retries that should be attempted within the timeout period "
	"before the connection is closed.  A value of 0 will disable the watchdog."
	"\0"

	"Device,Controller,,CUIDropDown,6K Series|Gem6K Series,"
	"Select the series of the target device."
	"\0"

	"\0"
END

CParker6KTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Device,Addr,Socket\0"
	"G:1,root,Device Watchdog,TimeWD,TickWD\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END



///////////////////////////////////////////////////////////////////////////
//
// String Table
//
//

STRINGTABLE
BEGIN
	IDS_P6K_AXIS		"Axis"
	IDS_P6K_BRICK		"I/O Brick"
	IDS_P6K_INTRT		"Interrupt"
	IDS_P6K_IN		"Input"
	IDS_P6K_OUT		"Output"
	IDS_P6K_VAR		"Variable"
	IDS_P6K_BIT		".BIT"
	IDS_P6K_BITRANGE	"Bit must be in the range (%s..%s)."
END



/////////////////////////////////////////////////////////////////////////////
//
// Parker Element Dialog
//
//

ParkerElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT 2002, 10, 0, 20, 12
	CONTROL		"&Global Writes",		2005, "button", BS_AUTOCHECKBOX, 40, 0, 50, 12 
END	

// End of File
