
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_L5KFILE_HPP
	
#define	INCLUDE_L5KFILE_HPP

#include "l5kitem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Component Definition
//

struct CABTypeDef
{
	CString	m_Name;
	CString	m_Type;
	BOOL	m_fAlias;
	};

enum {
	lineGeneric,
	lineComment,
	lineAttribute,
	lineInitData,
	lineValue,
	};

enum {
	compHeader,
	compController,
	compDataType,
	compModule,
	compAddOn,
	compParameters,
	compLocalTags,
	compTag,
	compProgram,
	compRoutine,
	compFBDRoutine,
	compSFCRoutine,
	compSTRoutine,
	compTask,
	compTrend,
	compConfig,
	compConnection,
	compEndOfParse,
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Component 
//

class CABL5kComponent : public CABTypeDef
{
	public:
		// Constructor
		CABL5kComponent(void);

		// Operations
		void SetName(PCTXT pName);

		// Management
		void AddMember(CABTypeDef Member);

	public:
		// Data
		CArray <CABTypeDef>	m_Members;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Data Type Component 
//

class CABL5kDataType : public CABL5kComponent
{
	public:
		// Constructor
		CABL5kDataType(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Tag Component 
//

class CABL5kTag : public CABL5kComponent
{
	public:
		// Constructor
		CABL5kTag(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Controller Component
//

class CABL5kController : public CABL5kComponent
{
	public:
		// Constructor
		CABL5kController(void);

		// Destructor
		~CABL5kController(void);

		// Management
		void AddDataType(CABL5kComponent *pType);
		void AddTag(CString Name, CString Type);
		void AddAliasTag(CString Name, CString Type);

	public:
		// Data
		CList <CABL5kComponent *>	m_Types;
		CABL5kComponent			m_Tags;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File
//

class CABL5kFile
{
	public:
		// Constructor
		CABL5kFile(void);

		// Destructor
		~CABL5kFile(void);

		// Control
		BOOL OpenLoad(ITextStream &Stream);

	public:		
		// Data
		CABL5kController	m_Controller;

	protected:
		// Data
		ITextStream   * m_pStream;
		CString		m_Line;
		CString		m_Data;
		UINT		m_uState;

		CABL5kDataType * m_pType;

		// Implementation
		void CloseFile(void);
		void ReadLine(BOOL fParse, BOOL fEndOfLine = TRUE);
		
		// Parser
		BOOL CheckEndOfLine(void);
		void SkipComment(void);
		void SkipAttribute(void);
		void SkipQuotedText(void);
		void SkipInitData(void);
		
		// Parser
		void ParseData(void);
		BOOL ParseHeader(void);
		BOOL ParseController(void);
		BOOL ParseDataType(void);
		BOOL ParseModule(void);
		BOOL ParseConnection(void);
		BOOL ParseAddOn(void);
		BOOL ParseParameters(void);
		BOOL ParseLocalTags(void);
		BOOL ParseRoutine(void);
		BOOL ParseProgram(void);
		BOOL ParseTag(void);
	};

// End of File

#endif
