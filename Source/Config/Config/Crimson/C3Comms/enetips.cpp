
#include "intern.hpp"

#include "enetips.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// EthernetIP Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CEthernetIPSlaveDriverOptions, CUIItem);

// Constructor

CEthernetIPSlaveDriverOptions::CEthernetIPSlaveDriverOptions(void)
{
	m_RunIdle = 0;
	}

// Download Support

BOOL CEthernetIPSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_RunIdle));

	return TRUE;
	}

// Meta Data Creation

void CEthernetIPSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(RunIdle);
	}


//////////////////////////////////////////////////////////////////////////
//
// EthernetIP Slave Driver
//

// Instantiator

ICommsDriver *	Create_EthernetIPSlaveDriver(void)
{
	return New CEthernetIPSlaveDriver;
	}

// Constructor

CEthernetIPSlaveDriver::CEthernetIPSlaveDriver(void)
{
	m_wID		= 0x3512;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "EtherNet/IP"; 
	
	m_DriverName	= "Slave Adaptor";
	
	m_Version	= "1.01";
	
	m_ShortName	= "EtherNet/IP Slave";
	}

// Binding Control

UINT CEthernetIPSlaveDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEthernetIPSlaveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CEthernetIPSlaveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEthernetIPSlaveDriverOptions);
	}

CLASS CEthernetIPSlaveDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Address Management

BOOL CEthernetIPSlaveDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	Text.MakeUpper();

	if( Text[0] == 'C' ) {

		UINT uPos = Text.Find(':');
		
		if( uPos == NOTHING ) {
		
			Error.Set( CString(IDS_DRIVER_ADDR_COLON),
				   0
				   );
				   
			return FALSE;
			}

		UINT uConnection = tatoi(Text.Mid(1, uPos - 1));

		Addr.a.m_Offset	= 0;
		
		Addr.a.m_Table  = uConnection;

		Addr.a.m_Extra  = 0;
		
		uPos = Text.Find('.');

		if( uPos == NOTHING ) {

			Addr.a.m_Type = addrLongAsLong;
			}
		else {
			CString Type = Text.Mid(uPos + 1);
			
			if( Type == "WORD" ) {

				Addr.a.m_Type = addrWordAsWord;
				}
			
			else if( Type == "BYTE" ) {

				Addr.a.m_Type = addrWordAsWord;
				}
			else {
				Error.Set( CString(IDS_ERROR_ADDR),
					   0
					   );
				   
				return FALSE;
				}
			}

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CEthernetIPSlaveDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		Text.Printf("C%2.2d:%4.4d", Addr.a.m_Table, Addr.a.m_Offset);

		switch( Addr.a.m_Type ) {
			
			case addrByteAsByte:

				Text += ".BYTE";

				break;
			
			case addrWordAsWord:

				Text += ".WORD";

				break;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CEthernetIPSlaveDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CEthernetIPSlaveDialog Dlg(*this, Addr);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CEthernetIPSlaveDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEthernetIPSlaveDialog, CStdDialog);
		
// Constructor

CEthernetIPSlaveDialog::CEthernetIPSlaveDialog(CEthernetIPSlaveDriver &Driver, CAddress &Addr)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	SetName(TEXT("EthernetIPSlaveDlg"));
	}

// Message Map

AfxMessageMap(CEthernetIPSlaveDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CEthernetIPSlaveDialog)
	};

// Message Handlers

BOOL CEthernetIPSlaveDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetCaption();

	LoadList();

	LoadAddress();

	return FALSE;
	}

// Command Handlers

BOOL CEthernetIPSlaveDialog::OnOkay(UINT uID)
{
	GetAddress();
	
	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CEthernetIPSlaveDialog::SetCaption(void)
{
	CString Text;

	Text.Printf( CString(IDS_DRIVER_CAPTION1), 
		     GetWindowText(),
		     m_pDriver->GetString(stringShortName),
		     ""
		     );

	SetWindowText(Text);
	}

void CEthernetIPSlaveDialog::LoadList(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(1006);

	Combo.AddString(CString(IDS_SPACE_BYABY), addrByteAsByte);

	Combo.AddString(CString(IDS_SPACE_WAW), addrWordAsWord);

	Combo.AddString(CString(IDS_SPACE_LAL), addrLongAsLong);
		
	if( m_pAddr->m_Ref ) {
		
		Combo.SelectData(m_pAddr->a.m_Type);
		}
	else {
		Combo.SetCurSel(2);
		}
	}

void CEthernetIPSlaveDialog::LoadAddress(void)
{
	UINT uConnectPoint  = 0;

	UINT uConnectOffset = 0;
	
	if( m_pAddr->m_Ref ) {

		uConnectPoint  = m_pAddr->a.m_Table;

		uConnectOffset = 0;
		}
	
	GetDlgItem(1004).SetWindowText(CPrintf("%2.2d", uConnectPoint));
	
	GetDlgItem(1005).SetWindowText(CPrintf("%4.4d", uConnectOffset));

	GetDlgItem(1005).DisableWindow();
	}

void CEthernetIPSlaveDialog::GetAddress(void)
{
	UINT uConnectPoint  = tatoi(GetDlgItem(1004).GetWindowText());

	UINT uConnectOffset = 0;

	UINT uType          = ((CComboBox &) GetDlgItem(1006)).GetCurSelData();

	if( uConnectPoint || uConnectOffset ) {

		m_pAddr->a.m_Table  = uConnectPoint;

		m_pAddr->a.m_Offset = uConnectOffset;

		m_pAddr->a.m_Type   = uType;
		}
	else {
		m_pAddr->m_Ref = 0;
		}
	}

// End of File
