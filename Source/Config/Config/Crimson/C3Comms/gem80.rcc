
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

/*
LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "gem80.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "gem80.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "gem80.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "gem80.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
*/

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CGem80SerialDriverOptions
//

CGem80SerialDriverOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|255,"
	"Select the drop number of the device to be addressed."
	"\0"

	"\0"
END

CGem80SerialDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device,Drop\0"
	"\0"
END


//////////////////////////////////////////////////////////////////////////
//
// UI for CGem80SerialDeviceOptions
//

CGem80SerialDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|14,"
	"Select the drop number of the device to be addressed."
	"\0"

	"\0"
	
END

CGem80SerialDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device,Drop\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CGem80SerialExchangeDeviceOptions
//

CGem80SerialExchangeDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|14,"
	"Select the drop number of the device to be addressed."
	"\0"

	"Exchange,Word Limit,,CUIEditInteger,|0||1|128,"
	"Indicate the word limit as configured in the controller."
	"\0"

	"Ping,Ping,,CUIDropDown,Disable|Enable,"
	"When enabled, a read request will be sent to the slave device upon " 
	"initialization or communications error to determine if the device is "
	"online.  Note that when the Ping is disabled and a communcations error "
	"is present, the corresponding bit of CommsError will toggle."
	"\0"

	"Timeout,Time Out,,CUIEditInteger,|0||0|6000,"
	"Select the number of mSec that a device time out should be based upon."
	"\0"
     
	"Monitor,Monitor Mode,,CUIDropDown,Disable|Enable,"
	"Enabling Monitor Mode allows the slave device to be polled without causing "
	"a data transfer to the slave device.  Otherwise, current table data will be "
	"sent to the slave device on all exchanges.  "
	"\0"
	  
	"\0"
END

CGem80SerialExchangeDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device,Drop,Exchange,Ping,Timeout,Monitor\0"
	"\0"
END


//////////////////////////////////////////////////////////////////////////
//
// UI for CGem80TCPDeviceOptions
//

CGem80TCPDeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the GEM80 device."
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|9999,"
	"Indicate the TCP port number on which the GEM80 protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"
    
	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the GEM80 driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the GEM80 server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a GEM80 request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CGem80TCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Port\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END

// End of File
