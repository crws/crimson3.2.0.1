
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MICRODCI_HPP
	
#define	INCLUDE_MICRODCI_HPP

#define	BB addrBitAsBit
#define	YY addrByteAsByte
#define	YL addrByteAsLong
#define	WW addrWordAsWord
#define LL addrLongAsLong
#define	RR addrRealAsReal

#define	SP_A	('A')
#define	SP_B	('B')
#define	SP_C	('C')
#define	SP_F	('F')
#define	SP_H	('H')
#define	SP_L	('L')
#define	SP_Z	('Z')

class CMicromodDCIDriver;

//////////////////////////////////////////////////////////////////////////
//
// Micromod Micro-DCI Comms Driver
//

class CMicromodDCIDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMicromodDCIDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

// Address Helpers
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Micromod Micro-DCI Selection Dialog
//

class CMicromodDCIAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CMicromodDCIAddrDialog(CMicromodDCIDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

		// Destructor
		~CMicromodDCIAddrDialog(void);
		                
	protected:
		CMicromodDCIDriver * m_pGDrv;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk     (UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnTypeChange (UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);
		CString	GetDCIText(void);

		void	ShowAddress(CAddress Addr);
		void	SetStringPar(UINT uOffset, BOOL fIsA);
		void	SetStringNum(UINT uOffset, BOOL fIsA);
		UINT	GetStringNum(UINT uOffset, BOOL fIsA);

		// Helpers
		BOOL	IsTextSpace(UINT uTable);
		BOOL	EnableText(void);
		void	LoadPartDesc(void);
	};

// End of File

#endif
