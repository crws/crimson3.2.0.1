
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

#include "E3Modbus.hpp"

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Pre-defined Register
//

// Constructor

CE3Register::CE3Register(void)
{
	m_uTable	= 0;

	m_uType		= 0;

	m_uMin		= 0;

	m_uMax		= 0;
	}

bool CE3Register::operator ==(CE3Register const &That)
{
	return m_Prefix == That.m_Prefix && m_Caption == That.m_Caption;
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Device
//

// Constructors

CE3Model::CE3Model(void)
{
	}

CE3Model::CE3Model(CString Name)
{
	m_Name = Name;
	}

// Destructor

CE3Model::~CE3Model(void)
{
	}

// Properties

CString CE3Model::GetName(void)
{
	return m_Name;
	}

// Registers

CRegArray const &CE3Model::GetRegs(void) const
{
	return m_Registers;
	}

void CE3Model::MakeRegisters(CTextStreamMemory &Stream)
{
	TCHAR Buff[256];

	if( Stream.GetLine(Buff, 256) ) {

		CString Line(Buff);

		while( Line[0] != ',' ) {

			if( isdigit(Line[0]) ) {

				CStringArray Parts;

				Line.Tokenize(Parts, ',');

				UINT uColon	= Parts[0].Find(':');

				CString Range	= Parts[0].Mid(uColon + 1);

				UINT uTable	= tatoi(Parts[4]);

				CString Prefix	= Parts[3];

				Prefix.StripAll();

				UINT uType	= GetRegType(uTable);

				BOOL fReadOnly	= Parts[2] == "Y";

				CString Caption	= Parts[1];

				UINT uMin	= tatoi(Range);

				UINT uMax	= 0;

				UINT uHyphen	= Range.Find('-');

				if( uHyphen < NOTHING ) 
					
					uMax = tatoi(Range.Mid(uHyphen+1));
				else
					
					uMax = uMin;

				CE3Register Reg;

				Reg.m_Caption	= Caption;
				Reg.m_Prefix	= Prefix;
				Reg.m_uMax	= uMax;
				Reg.m_uMin	= uMin;
				Reg.m_fNamed	= uMin == uMax;
				Reg.m_uTable	= uTable;
				Reg.m_uType	= uType;
				Reg.m_fReadOnly	= fReadOnly;

				AddRegister(Reg);
					
				}
			
			if( !Stream.GetLine(Buff, 256) ) {

				break;
				}

			Line = Buff;
			}
		}
	}

BOOL CE3Model::FindRegister(CE3Register &Reg, UINT uTable, UINT uOffset) const
{
	for( UINT n = 0; n < m_Registers.GetCount(); n++ ) {

		CE3Register Search = m_Registers.GetAt(n);

		BOOL fMatch = Search.m_uTable == uTable		&&
			             uOffset  >= Search.m_uMin	&&
			             uOffset  <= Search.m_uMax;

		if( fMatch ) {

			Reg = Search;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CE3Model::FindRegister(CE3Register &Reg, CString const &Prefix, CString const &Caption) const
{
	for( UINT n = 0; n < m_Registers.GetCount(); n++ ) {

		CE3Register Search = m_Registers.GetAt(n);

		if( Search.m_Prefix == Prefix && Search.m_Caption == Caption) {

			Reg = Search;

			return TRUE;
			}		
		}

	return FALSE;
	}

// Implementation

BOOL CE3Model::AddRegister(CE3Register Reg)
{
	if( m_Registers.Find(Reg) == NOTHING ) {

		m_Registers.Append(Reg);

		return TRUE;
		}

	return FALSE;
	}

UINT CE3Model::GetRegType(UINT uTable)
{	
	switch( uTable ) {

		case 1:
		case 2:
			return addrWordAsWord;

		case 3:
		case 4:
			return addrBitAsBit;
		}

	return addrWordAsWord;
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Serial Driver
//

ICommsDriver * Create_E3ModbusDriver(void)
{
	return New CE3ModbusDriver;
	}

// Constructor

CE3ModbusDriver::CE3ModbusDriver(void)
{
	m_wID		= 0x40CD;

	m_uType		= driverMaster;

	m_Manufacturer	= "Red Lion";

	m_DriverName	= "E3 Master"; 

	m_Version	= "1.00";
	
	m_ShortName	= "E3 I/O";

	m_DevRoot	= "E3IO_";
	}

// Destructor

CE3ModbusDriver::~CE3ModbusDriver(void)
{
	}

// Configuration

CLASS CE3ModbusDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CE3ModbusDriverOptions);
	}

CLASS CE3ModbusDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CE3ModbusSerialDeviceOptions);
	}

// Binding

UINT CE3ModbusDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CE3ModbusDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CE3ModbusDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CE3ModbusBaseDeviceOptions *pDevice = (CE3ModbusBaseDeviceOptions *) pConfig;

	return pDevice->DoParseAddress(Error, Addr, Text);
	}

BOOL CE3ModbusDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CE3ModbusBaseDeviceOptions *pDevice = (CE3ModbusBaseDeviceOptions *) pConfig;

	return pDevice->DoExpandAddress(Text, Addr);
	}

BOOL CE3ModbusDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CE3ModbusDialog Dlg(ThisObject, Addr, pConfig, fPart);

	return Dlg.Execute();
	}

BOOL CE3ModbusDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	CE3ModbusBaseDeviceOptions *pDevice = (CE3ModbusBaseDeviceOptions *) pConfig;

	CE3Model Model = pDevice->GetCurrentModel();

	if( !pRoot ) {

		if( uItem == 0 ) {

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart      = FALSE;
			Data.m_Name       = "Discrete Inputs";
			Data.m_uData      = 4;

			return TRUE;
			}

		if( uItem == 1 ) {

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart      = FALSE;
			Data.m_Name       = "Discrete Outputs";
			Data.m_uData      = 3;

			return TRUE;
			}

		if( uItem == 2 ) {

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart      = FALSE;
			Data.m_Name       = "Analog Inputs";
			Data.m_uData      = 2;

			return TRUE;
			}

		if( uItem == 3 ) {

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart      = FALSE;
			Data.m_Name       = "Analog Outputs";
			Data.m_uData      = 1;

			return TRUE;
			}

		return FALSE;
		}
	else {
		if( pRoot->m_uData == NOTHING ) {

			return FALSE;
			}

		CRegArray Regs = Model.GetRegs();

		if( uItem < Regs.GetCount() ) {

			UINT uStart = 0;

			for( UINT n = 0; n < Regs.GetCount(); n++ ) {

				if( Regs.GetAt(n).m_uTable == pRoot->m_uData ) {

					uStart = n;

					break;
					}
				}

			CE3Register Reg = Regs.GetAt(uStart + uItem);

			if( Reg.m_uTable == pRoot->m_uData ) {

				UINT uTable = Reg.m_uTable;

				Data.m_Addr.a.m_Table  = Reg.m_fNamed ? addrNamed + uTable : uTable;
				Data.m_Addr.a.m_Offset = Reg.m_uMin;
				Data.m_Addr.a.m_Type   = Reg.m_uType;
				Data.m_fPart           = TRUE;
				Data.m_Name            = Reg.m_Caption;
				Data.m_uData           = NOTHING;
				Data.m_fRead           = Reg.m_fReadOnly;

				return TRUE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CE3ModbusDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	CE3ModbusBaseDeviceOptions *pDevice = (CE3ModbusBaseDeviceOptions *) pConfig;

	CE3Model Model = pDevice->GetCurrentModel();

	CE3Register Reg;

	UINT uTable = Addr.a.m_Table;

	if( uTable >= addrNamed ) {

		uTable -= addrNamed;
		}

	if( Model.FindRegister(Reg, uTable, Addr.a.m_Offset) ) {

		return Reg.m_fReadOnly;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus TCP Driver
//

ICommsDriver * Create_E3ModbusTCPDriver(void)
{
	return New CE3ModbusTCPDriver;
	}

// Constructor

CE3ModbusTCPDriver::CE3ModbusTCPDriver(void)
{
	m_wID		= 0x40CE;

	m_uType		= driverMaster;

	m_Manufacturer	= "Red Lion";

	m_DriverName	= "E3 Master TCP/IP";

	m_Version	= "1.00";
	
	m_ShortName	= "E3 I/O";

	m_DevRoot	= "E3IO_";
	}

// Destructor

CE3ModbusTCPDriver::~CE3ModbusTCPDriver(void)
{
	}

// Configuration

CLASS CE3ModbusTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CE3ModbusTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CE3ModbusTCPDeviceOptions);
	}

// Binding

UINT CE3ModbusTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CE3ModbusTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Serial Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CE3ModbusDriverOptions, CUIItem);

// Constructor

CE3ModbusDriverOptions::CE3ModbusDriverOptions(void)
{
	m_Protocol = 0;

	m_Track    = 0;
	
	m_Timeout  = 600;
	}

// UI Managament

void CE3ModbusDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Protocol" ) {

		pWnd->EnableUI("Track", !m_Protocol);
		}

	if( Tag == "Protocol" ) {

		BOOL fAscii = pItem->GetDataAccess("Protocol")->ReadInteger(pItem);

		m_Timeout   = fAscii ? 1000 : 600;

		pWnd->UpdateUI("Timeout");
		} 
	}

// Download Support

BOOL CE3ModbusDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Protocol));
	Init.AddByte(BYTE(m_Track));
	Init.AddWord(WORD(m_Timeout));

	return TRUE;
	}
			
// Meta Data Creation

void CE3ModbusDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Protocol);
	Meta_AddInteger(Track);
	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Base Device Options
//

AfxImplementDynamicClass(CE3ModbusBaseDeviceOptions, CUIItem);

// Constructors

CE3ModbusBaseDeviceOptions::CE3ModbusBaseDeviceOptions(void)
{
	m_Type		= 0;

	m_Ping		= 0;
	
	m_Drop		= 1;

	m_FlipReal	= 0;

	m_FlipLong	= 0;

	LoadFromFile();
	}

// Destructor

CE3ModbusBaseDeviceOptions::~CE3ModbusBaseDeviceOptions(void)
{
	}

// Download Support
		
BOOL CE3ModbusBaseDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Ping));
	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_FlipLong));
	Init.AddByte(BYTE(m_FlipReal));

	return TRUE;
	}

// Meta Data Creation

void CE3ModbusBaseDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Type);
	Meta_AddInteger(Drop);
	Meta_AddInteger(Ping);	
	Meta_AddInteger(FlipLong);
	Meta_AddInteger(FlipReal);
	}

// UI Managament

void CE3ModbusBaseDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Type" ) {

			CSystemItem *pSystem = GetDatabase()->GetSystemItem();

			if( pSystem ) {

				pSystem->Rebuild(1);
				}

			pWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);
			}
		}
	}

// E3 Devices

void CE3ModbusBaseDeviceOptions::AddE3Model(CE3Model Model)
{
	m_Models.Append(Model);	
	}

UINT CE3ModbusBaseDeviceOptions::GetE3ModelCount(void)
{
	return m_Models.GetCount();
	}

CE3Model const& CE3ModbusBaseDeviceOptions::GetCurrentModel(void)
{
	return m_Models.GetAt(m_Type);
	}

CE3Model CE3ModbusBaseDeviceOptions::GetE3Model(UINT uPos)
{
	return m_Models.GetAt(uPos);
	}

CRegArray const &CE3ModbusBaseDeviceOptions::GetModelRegs(void) const
{
	CE3Model const &Model = m_Models.GetAt(m_Type);

	return Model.GetRegs();
	}

// Address Management

BOOL CE3ModbusBaseDeviceOptions::DoParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CE3Model Model	= GetCurrentModel();

	UINT uFind	= Text.Find(L'.');

	CString Type;

	if( uFind < NOTHING ) {

		Type = Text.Mid(uFind + 1);

		Text = Text.Mid(0, uFind);
		}

	UINT uColon = Text.Find(L":");

	if( uColon < NOTHING ) {

		CString Prefix = Text.Mid(0, uColon);

		CString Caption = Text.Mid(uColon+1);

		UINT uBracket = Prefix.Find(L"[");

		UINT uOffset = 0;

		if( uBracket < NOTHING ) {

			uOffset = tatoi(Prefix.Mid(uBracket+1));

			Prefix = Prefix.Mid(0, uBracket);			
			}

		CE3Register Reg;

		if( Model.FindRegister(Reg, Prefix, Caption) ) {
			
			if( uOffset < Reg.m_uMin || uOffset > Reg.m_uMax ) {

				Error.Set(IDS_ERROR_OFFSETRANGE);

				return FALSE;
				}

			Addr.a.m_Table	= Reg.m_fNamed ? Reg.m_uTable + addrNamed : Reg.m_uTable;

			Addr.a.m_Offset	= uOffset;

			Addr.a.m_Type	= GetType(Type, Reg.m_uType);

			return TRUE;
			}
		}

	Error.Set(IDS_DRIVER_ADDR_INVALID);

	return FALSE;
	}

UINT CE3ModbusBaseDeviceOptions::FindTable(CString Prefix)
{
	CE3Model Model = GetCurrentModel();

	CRegArray Regs = Model.GetRegs();

	for( UINT n = 0; n < Regs.GetCount(); n++ ) {

		CE3Register Reg = Regs.GetAt(n);

		if( Prefix == Reg.m_Prefix ) {

			return Reg.m_uTable;
			}
		}

	return NOTHING;
	}

BOOL CE3ModbusBaseDeviceOptions::DoExpandAddress(CString &Text, CAddress const &Addr)
{
	CE3Model Model = GetCurrentModel();

	CE3Register Reg;

	UINT uTable = Addr.a.m_Table;

	if( uTable >= addrNamed ) {

		uTable -= addrNamed;
		}
	
	if( Model.FindRegister(Reg, uTable, Addr.a.m_Offset) ) {

		Text = CPrintf("%s[%u]:%s", Reg.m_Prefix, Addr.a.m_Offset, Reg.m_Caption);

		if( Addr.a.m_Type != Reg.m_uType ) {

			Text += L"." + GetTypeString(Addr.a.m_Type);
			}

		return TRUE;
		}

	return FALSE;
	}

CString CE3ModbusBaseDeviceOptions::GetTypeString(UINT uType)
{
	if( uType == addrWordAsLong ) {

		return "LONG";
		}

	else if( uType == addrWordAsReal ) {

		return "REAL";
		}

	return "";
	}

UINT CE3ModbusBaseDeviceOptions::GetType(CString Type, UINT uDefault)
{
	if( Type == "LONG" ) {

		return addrWordAsLong;
		}

	if( Type == "REAL" ) {

		return addrWordAsReal;
		}

	return uDefault;
	}

// Implementation

void CE3ModbusBaseDeviceOptions::LoadFromFile(void)
{
	CTextStreamMemory Stream;

	CFilename File = afxModule->GetFilename().WithName(L"Schema\\40CD.csv");

	TCHAR Buff[256];

	if( Stream.LoadFromFile(File) ) {

		while( Stream.GetLine(Buff, 256) ) {

			CString Line(Buff);

			if( Line[0] == 'E' ) {

				CString Name = Line.Left(Line.Find(','));

				CE3Model Model(Name);

				Model.MakeRegisters(Stream);
				
				AddE3Model(Model);
				}
			}
		}
	else {
		CE3Model Model(L"E3Generic");

		CE3Register Reg;

		PCTXT pPrefix[4] = {L"AO", L"AI", L"DO", L"DI"};

		for( UINT n = 1; n <= 4; n++ ) {

			Reg.m_Caption	= "Available_Internal_Registers";
			Reg.m_Prefix	= pPrefix[n - 1];			
			Reg.m_uMin	= 1;
			Reg.m_uMax	= 1024;
			Reg.m_fNamed	= FALSE;
			Reg.m_uTable	= n;
			Reg.m_uType	= n < 3 ? addrWordAsWord : addrBitAsBit;
			Reg.m_fReadOnly	= FALSE;

			Model.AddRegister(Reg);
			}

		AddE3Model(Model);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Serial Device Options
//

// Runtime Class

AfxImplementDynamicClass(CE3ModbusSerialDeviceOptions, CE3ModbusBaseDeviceOptions);

// Constructor

CE3ModbusSerialDeviceOptions::CE3ModbusSerialDeviceOptions(void)
{
	m_Timeout	= 1000;
	}

// Destructor

CE3ModbusSerialDeviceOptions::~CE3ModbusSerialDeviceOptions(void)
{
	}

// UI Loading

BOOL CE3ModbusSerialDeviceOptions::OnLoadPages(CUIPageList * pList)
{
	CE3ModbusSerialDeviceOptionsUIPage *pPage = New CE3ModbusSerialDeviceOptionsUIPage(this);

	pList->Append(pPage);

	return TRUE;
	}

// Download Support

BOOL CE3ModbusSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CE3ModbusBaseDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CE3ModbusSerialDeviceOptions::AddMetaData(void)
{
	CE3ModbusBaseDeviceOptions::AddMetaData();

	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CE3ModbusTCPDeviceOptions, CE3ModbusBaseDeviceOptions);

// Constructor

CE3ModbusTCPDeviceOptions::CE3ModbusTCPDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD(21, 1), MAKEWORD(168, 192)));

	m_Port    = 502;

	m_Keep    = TRUE;

	m_UsePing = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;
	}

// Destructor

CE3ModbusTCPDeviceOptions::~CE3ModbusTCPDeviceOptions(void)
{
	}

// UI Loading

BOOL CE3ModbusTCPDeviceOptions::OnLoadPages(CUIPageList * pList)
{
	CE3ModbusTCPDeviceOptionsUIPage *pPage = New CE3ModbusTCPDeviceOptionsUIPage(this);

	pList->Append(pPage);

	return TRUE;
	}

// Download Support

BOOL CE3ModbusTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CE3ModbusBaseDeviceOptions::MakeInitData(Init);

	Init.AddLong(m_Addr);
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_UsePing));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}
		
// Meta Data Creation

void CE3ModbusTCPDeviceOptions::AddMetaData(void)
{
	CE3ModbusBaseDeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(UsePing);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

////////////////////////////////////////////////////////////////////////
//
// E3 Modbus Base Device Options Custom UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CE3ModbusBaseDeviceOptionsUIPage, CUIPage);

// Constructor

CE3ModbusBaseDeviceOptionsUIPage::CE3ModbusBaseDeviceOptionsUIPage(CE3ModbusBaseDeviceOptions * pDevice)
{
	m_pDevice = pDevice;
	}

// Operations

BOOL CE3ModbusBaseDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Device Options", 1);

	LoadModels(pView, pItem);

	pView->AddUI(pItem, L"root", L"Drop");

	pView->AddUI(pItem, L"root", L"Ping");

	pView->EndGroup(TRUE);

	pView->StartGroup(L"Word Ordering", 2);

	pView->AddUI(pItem, L"root", L"FlipLong");

	pView->AddUI(pItem, L"root", L"FlipReal");

	pView->EndGroup(TRUE);

	return TRUE;
	}

// UI Help

void CE3ModbusBaseDeviceOptionsUIPage::LoadModels(IUICreate *pView, CItem *pItem)
{
	CString Form = L"Error";

	UINT uCount = m_pDevice->GetE3ModelCount();

	if( uCount > 0 ) {

		Form = L"";

		for( UINT n = 0; n < uCount; n++ ) {

			Form += m_pDevice->GetE3Model(n).GetName();

			if( n < (uCount - 1) ) {

				Form += L"|";
				}
			}
		}

	CUIData Data;

	Data.m_Tag	 = L"Type";

	Data.m_Label	 = L"E3 Model";

	Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

	Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

	Data.m_Format	 = Form;

	Data.m_Tip	 = L"The selected EtherTrak3 Model.";

	pView->AddUI(pItem, L"root", &Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// E3 Modbus Serial Device Options Custom UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CE3ModbusSerialDeviceOptionsUIPage, CE3ModbusBaseDeviceOptionsUIPage);

// Constructor

CE3ModbusSerialDeviceOptionsUIPage::CE3ModbusSerialDeviceOptionsUIPage(CE3ModbusBaseDeviceOptions * pDevice)
	: CE3ModbusBaseDeviceOptionsUIPage(pDevice)
{
	}
		
// Operations

BOOL CE3ModbusSerialDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CE3ModbusBaseDeviceOptionsUIPage::LoadIntoView(pView, pItem);

	return TRUE;
	}

////////////////////////////////////////////////////////////////////////
//
// E3 Modbus TCP Device Options Custom UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CE3ModbusTCPDeviceOptionsUIPage, CE3ModbusBaseDeviceOptionsUIPage);

// Constructor

CE3ModbusTCPDeviceOptionsUIPage::CE3ModbusTCPDeviceOptionsUIPage(CE3ModbusBaseDeviceOptions * pDevice)
	: CE3ModbusBaseDeviceOptionsUIPage(pDevice)
{
	}

// Operations

BOOL CE3ModbusTCPDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CE3ModbusBaseDeviceOptionsUIPage::LoadIntoView(pView, pItem);

	pView->StartGroup(L"Target Device", 1);

	pView->AddUI(pItem, L"root", L"Addr");

	pView->AddUI(pItem, L"root", L"Port");

	pView->EndGroup(TRUE);

	pView->StartGroup(L"Protocol Options", 1);

	pView->AddUI(pItem, L"root", L"Keep");

	pView->AddUI(pItem, L"root", L"UsePing");

	pView->AddUI(pItem, L"root", L"Time1");

	pView->AddUI(pItem, L"root", L"Time3");

	pView->AddUI(pItem, L"root", L"Time2");

	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CE3ModbusDialog, CStdDialog);

// Message Map

AfxMessageMap(CE3ModbusDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSelChanged)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChanged)
	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1003, CBN_SELCHANGE,  OnChannelChanged)

	AfxMessageEnd(CE3ModbusDialog)
	};

// Constructor

CE3ModbusDialog::CE3ModbusDialog(CE3ModbusDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
	: m_Addr(Addr), m_Driver(Driver)
{
	SetName(fPart ? L"E3ModbusPartialDlg" : L"E3ModbusFullDlg");

	m_pDevice  = (CE3ModbusBaseDeviceOptions *) pConfig;

	m_uChan    = 4;
	}

// Destructor

CE3ModbusDialog::~CE3ModbusDialog(void)
{
	}

// Message Handlers

BOOL CE3ModbusDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( m_Addr.m_Ref ) {

		CE3Register Reg;

		CE3Model Model = m_pDevice->GetCurrentModel();

		UINT uTable    = m_Addr.a.m_Table;

		if( uTable >= addrNamed ) {

			uTable -= addrNamed;
			}

		if( Model.FindRegister(Reg, uTable, m_Addr.a.m_Offset) ) {

			m_Reg       = Reg;

			UINT uTable = Reg.m_uTable;

			m_uChan     = uTable;
			}
		}	

	LoadChannels();

	LoadRegs();

	LoadTypes();

	ShowDetails();

	ShowAddress();

	DoEnables();

	return TRUE;
	}

BOOL CE3ModbusDialog::OnOkay(UINT uID)
{
	CError Error;

	CString Offset	= GetDlgItem(2002).GetWindowText();
	CString Type	= GetDlgItem(2005).GetWindowText();

	CString Text = CPrintf("%s[%s]:%s", m_Reg.m_Prefix, Offset, m_Reg.m_Caption);

	Text += Type;

	if( m_Driver.ParseAddress(Error, m_Addr, m_pDevice, Text) ) {

		EndDialog(TRUE);

		return TRUE;
		}

	if( !Error.IsOkay() ) {

		Error.Show(ThisObject);

		return FALSE;
		}

	EndDialog(FALSE);
	
	return FALSE;
	}

void CE3ModbusDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CE3ModbusDialog::OnSelChanged(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT uSel         = ListBox.GetCurSel();

	CComboBox &Combo  = (CComboBox &) GetDlgItem(1003);

	CString Prefix    = Combo.GetWindowText().Left(2);

	CString Caption   = ListBox.GetText(uSel);

	CE3Register Reg;

	if( m_pDevice->GetCurrentModel().FindRegister(Reg, Prefix, Caption) ) {

		m_Reg = Reg;

		GetDlgItem(2002).SetWindowText(CPrintf("%u", m_Reg.m_uMin));
		}

	LoadTypes();

	CListBox &TypeBox = (CListBox &) GetDlgItem(4001);

	TypeBox.SetCurSel(0);

	ShowDetails();

	DoEnables();
	}

void CE3ModbusDialog::OnTypeChanged(UINT uID, CWnd &Wnd)
{
	AppendType();
	}

void CE3ModbusDialog::OnChannelChanged(UINT uID, CWnd &Wnd)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(1003);

	m_uChan = Combo.GetCurSelData();

	LoadRegs();
	}

// Implementation

void CE3ModbusDialog::LoadRegs(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	CRegArray Regs = m_pDevice->GetModelRegs();

	for( UINT n = 0; n < Regs.GetCount(); n++ ) {

		CE3Register Reg = Regs.GetAt(n);

		BOOL fShow = Reg.m_uTable == m_uChan;

		if( fShow ) {

			Box.AddString(Reg.m_Caption);
			}
		}

	Box.Invalidate(TRUE);

	Box.SetRedraw(TRUE);
	}

void CE3ModbusDialog::LoadTypes(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(4001);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	if( m_Reg.m_uTable ) {

		switch( m_Reg.m_uType ) {

			case addrBitAsBit:

				Box.AddString("Bit as Bit", addrBitAsBit);

				break;

			case addrWordAsWord:

				Box.AddString("Word as Word", addrWordAsWord);

				Box.AddString("Word as Long", addrWordAsLong);

				Box.AddString("Word as Real", addrWordAsReal);

				break;
			}
		}

	Box.Invalidate(TRUE);

	Box.SetRedraw(TRUE);
	}

void CE3ModbusDialog::LoadChannels(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(1003);

	Combo.AddString(L"DI - Discrete Inputs",  4);
	Combo.AddString(L"DO - Discrete Outputs", 3);
	Combo.AddString(L"AI - Analog Inputs",    2);
	Combo.AddString(L"AO - Analog Outputs",   1);

	if( !m_Reg.m_uTable ) {

		Combo.SetCurSel(0);
		}
	else {
		for( UINT n = 0; n < Combo.GetCount(); n++ ) {

			if( Combo.GetItemData(n) == m_Reg.m_uTable ) {

				Combo.SetCurSel(n);
				
				break;
				}
			}
		}
	}

void CE3ModbusDialog::AppendType(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(4001);

	UINT uType = Box.GetCurSelData();

	CString Type = m_pDevice->GetTypeString(uType);

	if( !Type.IsEmpty() ) {

		GetDlgItem(2005).SetWindowText(L"." + Type);
		}
	else {
		GetDlgItem(2005).SetWindowText(L"");
		}
	}

void CE3ModbusDialog::ShowDetails(void)
{
	if( m_Reg.m_uTable ) {

		GetDlgItem(2001).SetWindowText(m_Reg.m_Prefix);

		GetDlgItem(2004).SetWindowText(m_Reg.m_Caption);

		GetDlgItem(3004).SetWindowText(CPrintf("%u", m_Reg.m_uMin));

		GetDlgItem(3006).SetWindowText(CPrintf("%u", m_Reg.m_uMax));

		GetDlgItem(3008).SetWindowText("10");

		GetDlgItem(2002).SetWindowText(CPrintf("%u", m_Reg.m_uMin));
		}
	else {
		GetDlgItem(2001).SetWindowText("");

		GetDlgItem(2004).SetWindowText("");

		GetDlgItem(3004).SetWindowText("");

		GetDlgItem(3006).SetWindowText("");

		GetDlgItem(3008).SetWindowText("");
		}

	ShowType();

	AppendType();
	}

void CE3ModbusDialog::ShowAddress(void)
{	
	if( m_Addr.m_Ref ) {

		GetDlgItem(2001).SetWindowText(m_Reg.m_Prefix);

		GetDlgItem(2002).SetWindowText(CPrintf("%u", m_Addr.a.m_Offset));

		GetDlgItem(2004).SetWindowText(m_Reg.m_Caption);

		if( m_Addr.a.m_Type != m_Reg.m_uType ) {

			CString Type = m_pDevice->GetTypeString(m_Addr.a.m_Type);

			GetDlgItem(2005).SetWindowText(L"." + Type);
			}

		CListBox &RegBox = (CListBox &) GetDlgItem(1001);

		RegBox.SetRedraw(FALSE);

		UINT uPos = RegBox.FindString(0, m_Reg.m_Caption);

		if( uPos < NOTHING ) {

			RegBox.SetCurSel(uPos);
			}

		RegBox.Invalidate(TRUE);

		RegBox.SetRedraw(TRUE);

		CListBox &TypeBox = (CListBox &) GetDlgItem(4001);
		
		TypeBox.SetRedraw(FALSE);

		for( UINT n = 0; n < TypeBox.GetCount(); n++ ) {

			if( TypeBox.GetItemData(n) == m_Addr.a.m_Type ) {

				TypeBox.SetCurSel(n);

				break;
				}
			}

		TypeBox.Invalidate(TRUE);

		TypeBox.SetRedraw(TRUE);
		}
	else {
		CListBox &RegBox = (CListBox &) GetDlgItem(1001);

		RegBox.SetCurSel(0);

		OnSelChanged(0, ThisObject);
		}
	}

void CE3ModbusDialog::ShowType(void)
{	
	CString Type;

	if( m_Reg.m_uTable ) {

		switch( m_Reg.m_uType ) {

			case addrBitAsBit:

				Type = "Bit";

				break;

			case addrWordAsWord:
			case addrWordAsLong:
			case addrWordAsReal:

				Type = "Word";

				break;
			}
		}

	GetDlgItem(3002).SetWindowText(Type);
	}

void CE3ModbusDialog::DoEnables(void)
{
	BOOL fEnable = (m_Reg.m_uTable > 0) && (m_Reg.m_uMin != m_Reg.m_uMax);

	GetDlgItem(2002).EnableWindow(fEnable);
	}

// End of File
