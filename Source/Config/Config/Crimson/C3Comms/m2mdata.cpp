
#include "intern.hpp"

#include "m2mdata.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Serial Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CM2MDataSerialDriverOptions, CUIItem);

// Constructor

CM2MDataSerialDriverOptions::CM2MDataSerialDriverOptions(void)
{
// Not used for Serial but kept here for simplicity
	m_Server	= DWORD(MAKELONG(MAKEWORD( 141, 183), MAKEWORD(  78, 64)));

	m_RecvPort	= 9000;

	m_SendPort	= 9500;

	m_LPosition	= 0;

	m_LTarget	= 0;

	m_LTypeS	= TEXT("Word");

	m_LError	= 0;

	m_LDEMCt	= 0;

	m_LDEMSel	= 0;

	m_LDEMStr	= NO_ID; // ID = 0, Count = 0;

	m_LDEMStr1	= NO_ID;
	m_LDEMStr2	= NO_ID;
	m_LDEMStr3	= NO_ID;
	m_LDEMStr4	= NO_ID;
	m_LDEMStr5	= NO_ID;
	m_LDEMStr6	= NO_ID;
	m_LDEMStr7	= NO_ID;
	m_LDEMStr8	= NO_ID;
	m_LDEMStr9	= NO_ID;
	m_LDEMStr10	= NO_ID;
	m_LDEMStr11	= NO_ID;
	m_LDEMStr12	= NO_ID;
	m_LDEMStr13	= NO_ID;
	m_LDEMStr14	= NO_ID;
	m_LDEMStr15	= NO_ID;
	m_LDEMStr16	= NO_ID;
	m_LDEMStr17	= NO_ID;
	m_LDEMStr18	= NO_ID;
	m_LDEMStr19	= NO_ID;
	m_LDEMStr20	= NO_ID;
	m_LDEMStr21	= NO_ID;
	m_LDEMStr22	= NO_ID;
	m_LDEMStr23	= NO_ID;
	m_LDEMStr24	= NO_ID;
	m_LDEMStr25	= NO_ID;
	m_LDEMStr26	= NO_ID;
	m_LDEMStr27	= NO_ID;
	m_LDEMStr28	= NO_ID;
	m_LDEMStr29	= NO_ID;
	m_LDEMStr30	= NO_ID;

	m_Next		= 0;
	m_Prev		= 0;
	m_SelLong	= 0;
	m_SelWord	= 0;
	m_SelByte	= 0;
	m_SelBit	= 0;

	m_FAppend	= 0;
	m_FInsert	= 0;
	m_FReplace	= 0;
	m_FDelete	= 0;

	m_LErrorS	= TEXT("None");

	m_LFunct	= 0;

	m_LType		= 1;

	m_NO_ID		= NO_ID;
	}

// UI Management

void CM2MDataSerialDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	m_pWnd = pWnd;

	if( Tag == TEXT("Next") ) {

		ShowDataList(m_pWnd);

		pItem->SetDirty();
		}

	if( Tag.IsEmpty() || Tag == TEXT("LTarget") ) {

		UINT i       = 0;

		BOOL fFirst  = FALSE;

		CString sDef = TEXT("");

		CString sText;

		while( i < MAXIDQ ) {

			UINT uSel = 0;

			sText     = *GetStringX(i);

			if( HasID(sText, &uSel) ) {

				if( !fFirst ) {

					sDef   = sText;

					fFirst = TRUE;
					}

				if( m_LTarget == uSel ) {

					break;
					}					
				}

			i++;
			}

		if( i >= MAXIDQ ) {

			sText = sDef;
			}

		m_LDEMStr = sText;

		SetStrParams();

		pWnd->UpdateUI(TEXT("LDEMCt"));
		pWnd->UpdateUI(TEXT("LTarget"));
		}
	}

// Download Support

BOOL CM2MDataSerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Server));   // not used in serial runtime. Kept for simplicity
	Init.AddWord(WORD(m_RecvPort)); // not used in serial runtime. Kept for simplicity
	Init.AddWord(WORD(m_SendPort)); // not used in serial runtime. Kept for simplicity

	CStringArray List;
	BYTE bStrNum[MAXIDQ];

	CString ss;

	UINT i;

	UINT LCount;

	for( i = 0, LCount = 0; i < MAXIDQ; i++ ) { // get only configured strings

		ss = *(GetStringX(i));

		if( GetIDFromString(ss) && GetCountFromString(ss) ) {

			bStrNum[LCount] = LOBYTE(i);

			List.Append(ss);

			LCount++;
			}
		}

	Init.AddByte(BYTE(LCount)); // number of configured strings

	// Sort List by ID number
	if( LCount ) {

		PBYTE pSortID = new BYTE [LCount];

		for( i = 0; i < LCount; i++ ) {

			ss         = List.GetAt(i);

			pSortID[i] = LOBYTE(GetHexStrVal(ss.Left(2)));
			}

		i      = 0;
		UINT j = 1;

		while( i < LCount ) {

			j = i + 1;

			if( j == LCount ) break;

			if( pSortID[i] < pSortID[j] ) i++;

			else {
				BYTE bI    = pSortID[i];
				BYTE bS    = bStrNum[i];

				pSortID[i] = pSortID[j];
				pSortID[j] = bI;

				bStrNum[i] = bStrNum[j];
				bStrNum[j] = bS;
				i = 0;
				}
			}

		for( i = 0; i < LCount; i++ ) {

			BYTE bS = bStrNum[i];

			ss      = List.GetAt(bS);

			WORD w  = LOWORD(ss.GetLength());

			Init.AddByte(bS);
			Init.AddByte(pSortID[i]);

			Init.AddWord((WORD)(w - 2)); // Number of types

			for( j = 2; j < w; j++ ) {

				Init.AddByte((BYTE)(ss[j] & 0xF)); // convert char to Addr.a.m_Type value
				}
			}

		delete [] pSortID;
		}

	return TRUE;
	}

void CM2MDataSerialDriverOptions::ExecuteFunction(UINT uID)
{
	switch( uID ) {

		case EFINIT:	InitData(); return;

		case SLDBAP:
		case SLDBIN:
		case SLDBRE:
		case SLDBDE:
			m_LFunct = GetStrFunction(uID);

			m_LType  = GetTypeFromTypeS();

			DoFunction();

			m_LFunct = 0;

			return;

		case SLDBNS:
		case FTDBSH:
			if( m_LTarget == 6 ) {

				SetFileTransfer(uID);

				UpdateDEMUI(m_pWnd);

				m_pWnd->UpdateUI(TEXT("LTarget"));
				}

			SelectDEMString();

			if( m_LDEMStr.GetLength() < 3 ) { // list doesn't exist

				m_LPosition = 0;
				}

			SetTargetData(); // Empty

			return;

		case SLDBSV:
			SaveDEMString();

			return;

		case EFUPDT:
			UINT uPos;

			if( FindStringX(m_LTarget, &uPos) ) {

				m_LDEMStr = *GetStringX(uPos);

				SetStrParams();

				UpdateDEMUI(m_pWnd);

				m_pWnd->UpdateUI(TEXT("LTarget"));
				}
			return;
		}
	}

// Download Support
// Meta Data Creation

void CM2MDataSerialDriverOptions::AddMetaData(void)
{
	Meta_AddInteger(Server);
	Meta_AddInteger(RecvPort);
	Meta_AddInteger(SendPort);
	Meta_AddInteger(LPosition);
	Meta_AddInteger(LTarget);
	Meta_AddString(LTypeS);
	Meta_AddInteger(LError);
	Meta_AddInteger(LDEMCt);
	Meta_AddInteger(LDEMSel);
	Meta_AddString (LDEMStr);
	Meta_AddString (LDEMStr1);
	Meta_AddString (LDEMStr2);
	Meta_AddString (LDEMStr3);
	Meta_AddString (LDEMStr4);
	Meta_AddString (LDEMStr5);
	Meta_AddString (LDEMStr6);
	Meta_AddString (LDEMStr7);
	Meta_AddString (LDEMStr8);
	Meta_AddString (LDEMStr9);
	Meta_AddString (LDEMStr10);
	Meta_AddString (LDEMStr11);
	Meta_AddString (LDEMStr12);
	Meta_AddString (LDEMStr13);
	Meta_AddString (LDEMStr14);
	Meta_AddString (LDEMStr15);
	Meta_AddString (LDEMStr16);
	Meta_AddString (LDEMStr17);
	Meta_AddString (LDEMStr18);
	Meta_AddString (LDEMStr19);
	Meta_AddString (LDEMStr20);
	Meta_AddString (LDEMStr21);
	Meta_AddString (LDEMStr22);
	Meta_AddString (LDEMStr23);
	Meta_AddString (LDEMStr24);
	Meta_AddString (LDEMStr25);
	Meta_AddString (LDEMStr26);
	Meta_AddString (LDEMStr27);
	Meta_AddString (LDEMStr28);
	Meta_AddString (LDEMStr29);
	Meta_AddString (LDEMStr30);
	Meta_AddInteger(Next);
	Meta_AddInteger(Prev);
	Meta_AddInteger(SelLong);
	Meta_AddInteger(SelWord);
	Meta_AddInteger(SelByte);
	Meta_AddInteger(SelBit);
	Meta_AddInteger(FAppend);
	Meta_AddInteger(FInsert);
	Meta_AddInteger(FReplace);
	Meta_AddInteger(FDelete);
	Meta_AddString (LErrorS);
	}

// Show Data List

BOOL CM2MDataSerialDriverOptions::ShowDataList(CViewWnd *pWnd)
{
	CM2MShowListDialog Dlg(*this);

	return Dlg.Execute((CWnd &)*pWnd);
	}

void CM2MDataSerialDriverOptions::InitData(void)
{
	UINT uID  = 0;

	UINT uPos = 0;

	m_LTarget = GetIDFromString(m_LDEMStr);

	m_LDEMStr = NO_ID;

	if( m_LTarget ) {

		if( FindStringX(m_LTarget, &uPos) ) {

			m_LDEMStr = *GetStringX(uPos);
			}

		else {
			m_LTarget = 0;
			}
		}

	if( !m_LTarget ) {

		uPos = 0;

		while( uPos < MAXIDQ ) {

			CString Test = *GetStringX(uPos);

			if( HasID(Test, &uID ) ) {

				m_LDEMStr = Test;

				m_LTarget = GetIDFromString(Test);

				break;
				}

			uPos++;
			}
		}

	if( uPos >= MAXIDQ ) {

		m_LTarget = 1;
		}

	m_LPosition = 0;

	SelectDEMString();

	SetTargetData();
	}

BOOL CM2MDataSerialDriverOptions::HasID(CString sTest, UINT *pID)
{
	if( sTest.GetLength() < 3 ) return FALSE;

	UINT u = GetIDFromString(sTest);

	if( u ) {

		*pID = u;

		return TRUE;
		}

	return FALSE;
	}

void CM2MDataSerialDriverOptions::DoFunction(void)
{
	CString sDD = MakeDataStr();

	if( !sDD.GetLength() ) m_LFunct = 0;

	UINT uNewPos;

	switch( m_LFunct ) {

		case LFNAPP:

			uNewPos     = AppendItem(sDD);

			UpdateSize();

			m_LPosition = uNewPos;

			SetTargetData();

			break;

		case LFNINS:
			InsertItem(sDD, m_LPosition);

			UpdateSize();

			SetTargetData();

			break;

		case LFNREP:
			ReplaceItem();

			SetTargetData();

			break;

		case LFNREM:

			uNewPos    = DeleteItem(m_LPosition);

			UpdateSize();

			m_LPosition = uNewPos;

			SetTargetData();

			break;

		default:
			break;

		}

	if( m_LError ) {

		m_pWnd->UpdateUI(TEXT("LError"));
		}

	m_pWnd->EnableUI(TEXT("LError"), m_LError);
	}

UINT CM2MDataSerialDriverOptions::GetStrFunction(UINT uID)
{
	switch( uID ) {

		case SLDBAP: return LFNAPP;
		case SLDBIN: return LFNINS;
		case SLDBRE: return LFNREP;
		case SLDBDE: return LFNREM;
		}

	return 0;
	}

void CM2MDataSerialDriverOptions::SetFileTransfer(UINT uID)
{
	UINT uPos;

	if( FindStringX(6, &uPos) ) {

		m_LDEMStr = *GetStringX(uPos);

		if( uID == SLDBNS ) {

			return; // Just select
			}

		UINT uLen = (m_LDEMStr.GetLength() - 3) / 2; // strip ID and file count

		if( !m_LDEMCt ) m_LDEMCt = uLen; // rebuild file block definition
		}

	else {
		if( uID == SLDBNS ) {

			m_LDEMCt = 0;
			}
		}

	m_LDEMStr = TEXT("06I"); // ID + file quantity word

	UINT uQty = m_LDEMCt;

	m_LDEMCt  = 1;

	for( uPos = 0; uPos < uQty; uPos++ ) {

		m_LType  = LTYPEW; // file size word

		m_LFunct = LFNAPP;

		DoFunction();

		m_LType  = LTYPEL; // file date/time long

		m_LFunct = LFNAPP;

		DoFunction();
		}
	}

// String Buffer Display

void CM2MDataSerialDriverOptions::SetTargetData(void)
{
	CString ss = m_LDEMStr;

	UINT uCnt  = GetCountFromString(ss);

	if( uCnt ) { // string exists

		if( uCnt > m_LPosition) {

			m_LType   = GetTypeFromDEMStr(m_LPosition);

			m_LDEMCt  = uCnt;

			m_LTarget = GetIDFromString(ss);

			return;
			}

		if( m_LPosition ) {

			m_LPosition = 0;

			SetTargetData(); // Display string @ 0
			}

		return;
		}

	if( !m_LTarget ) {

		m_LType     = 0;
		m_LPosition = 0;
		m_LDEMCt    = 0;
		}
	}

// Function Handling

CString CM2MDataSerialDriverOptions::MakeDataStr(void)
{
	if( m_LTarget >= IDACK && m_LTarget < TF210 ) {

		m_LError = EIDMIS;

		m_LFunct = 0;

		return TEXT("");
		}

	CString s;

	s.Printf( TEXT("%c"), MakeChar0(m_LType) );

	return s;
	}

UINT CM2MDataSerialDriverOptions::AppendItem(CString sAppend)
{
	UINT uCurrCt = m_LDEMCt;

	if( uCurrCt >= MAXENT ) {

		m_LError = ELFULL;

		return 0;
		}

	if( uCurrCt ) {

		m_LDEMStr += sAppend;
		m_LPosition++;
		}

	else m_LDEMStr.Printf(TEXT("%2.2X%s"), m_LTarget, sAppend);

	SetStrParams();

	SaveDEMString();

	return GetCountFromString(m_LDEMStr) - 1;
	}

void CM2MDataSerialDriverOptions::ReplaceItem(void)
{
	CString	DDS;

	DDS.Printf( TEXT("%s"), MakeDataStr() );

	if( !DDS.GetLength() ) return;

	CString ss    = m_LDEMStr;

	CString Front = GetFront(m_LPosition + 2, ss);

	CString Rear  = GetRear(m_LPosition + 3, ss);

	m_LDEMStr.Printf(TEXT("%s%s%s"), Front, DDS, Rear);

	SetStrParams();

	SaveDEMString();
	}

void CM2MDataSerialDriverOptions::InsertItem(CString sInsert, UINT uPosition)
{
	if( m_LDEMCt >= MAXENT ) {

		m_LError = ELFULL;

		return;
		}

	CString ss = m_LDEMStr;

	CString Front = GetFront(m_LPosition + 2, ss);
	CString Rear  = GetRear( m_LPosition + 2, ss);

	m_LDEMStr.Printf(TEXT("%s%s%s"), Front, sInsert, Rear);

	SetStrParams();

	SaveDEMString();
	}

UINT CM2MDataSerialDriverOptions::DeleteItem(UINT uPosition)
{
	UINT uPos	= uPosition + 2;

	CString ss	= m_LDEMStr;

	CString Front	= GetFront(uPos, ss);
	CString  Rear	= GetRear( uPos+1, ss);

	m_LDEMStr.Printf( TEXT("%s%s"), Front, Rear );

	if( m_LDEMStr.GetLength() < 3 ) {

		m_LDEMStr = m_NO_ID;
		m_LTarget = 0;
		m_LDEMSel = 0;
		}

	SetStrParams();

	SaveDEMString();

	return GetCountFromString(m_LDEMStr);
	}

// Access String Data
CString * CM2MDataSerialDriverOptions::GetStringX(UINT u)
{
	switch( u ) {

		case 0:  return &m_LDEMStr1;
		case 1:  return &m_LDEMStr2;
		case 2:  return &m_LDEMStr3;
		case 3:  return &m_LDEMStr4;
		case 4:  return &m_LDEMStr5;
		case 5:  return &m_LDEMStr6;
		case 6:  return &m_LDEMStr7;
		case 7:  return &m_LDEMStr8;
		case 8:  return &m_LDEMStr9;
		case 9:  return &m_LDEMStr10;
		case 10: return &m_LDEMStr11;
		case 11: return &m_LDEMStr12;
		case 12: return &m_LDEMStr13;
		case 13: return &m_LDEMStr14;
		case 14: return &m_LDEMStr15;
		case 15: return &m_LDEMStr16;
		case 16: return &m_LDEMStr17;
		case 17: return &m_LDEMStr18;
		case 18: return &m_LDEMStr19;
		case 19: return &m_LDEMStr20;
		case 20: return &m_LDEMStr21;
		case 21: return &m_LDEMStr22;
		case 22: return &m_LDEMStr23;
		case 23: return &m_LDEMStr24;
		case 24: return &m_LDEMStr25;
		case 25: return &m_LDEMStr26;
		case 26: return &m_LDEMStr27;
		case 27: return &m_LDEMStr28;
		case 28: return &m_LDEMStr29;
		case 29: return &m_LDEMStr30;
		}

	return &m_NO_ID;
	}

BOOL	CM2MDataSerialDriverOptions::FindStringX(UINT uMatch, UINT *pNum)
{
	for( UINT i = 0; i < MAXIDQ; i++ ) {

		CString sTest = *GetStringX(i);

		UINT    uID   = 0;

		if( HasID(sTest, &uID) ) {

			if( uMatch == uID ) {

				*pNum = i;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void	CM2MDataSerialDriverOptions::SelectDEMString(void)
{
	if( m_LDEMStr.GetLength() > 2 ) {

		if( !m_LTarget || (m_LTarget == GetIDFromString(m_LDEMStr)) ) {

			SetStrParams();

			return;
			}
		}

	UINT u = m_LTarget; // ID of previous string selected

	CString s;

	m_LDEMStr = NO_ID;

	if( !u ) { // no previous string selected (startup)

		if( !m_LDEMSel ) {

			while( u < MAXIDQ ) {

				s  = *(GetStringX(u));

				if( (BOOL)(m_LDEMSel = GetIDFromString(s)) ) { // first string found

					m_LDEMStr.Printf( TEXT("%s"), s );

					SetStrParams();

					return;
					}

				u++;
				}

			return;
			}

		else u = m_LDEMSel;
		}

	UINT i = 0;

	while( i < MAXIDQ ) {

		s = *(GetStringX(i));

		if( u == GetIDFromString(s) ) {

			m_LDEMStr.Printf( TEXT("%s"), s );

			break;
			}

		i++;
		}

	SetStrParams();
	}

void	CM2MDataSerialDriverOptions::SaveDEMString(void)
{
	UINT u      = GetIDFromString(m_LDEMStr);

	if( !u ) return;

	UINT i      = 0;

	UINT uFound = MAXIDQ + 1;

	UINT uEmpty = uFound;

	CString *ps = NULL;

	CString ss;

	while( i < MAXIDQ ) {

		ps = GetStringX(i);

		ss = *ps;

		UINT uID = GetIDFromString(ss);

		if( u == uID ) {

			uFound = i;

			break;
			}

		if( !uID && (uEmpty > MAXIDQ) ) {

			uEmpty = i;
			}

		i++;
		}

	if( uFound > MAXIDQ && (uEmpty < MAXIDQ) ) uFound = uEmpty;

	if( uFound < MAXIDQ ) {

		ps = GetStringX(uFound);

		ss = *ps;

		ss.Printf(TEXT("%s"), m_LDEMStr);

		*ps = ss;

		UpdateDEMUI(m_pWnd);

		return;
		}

	m_LError = ELFULL;
	}

void	CM2MDataSerialDriverOptions::SetStrParams(void)
{
	UINT uSize = m_LDEMStr.GetLength();

	m_LError    = 0;

	if( uSize > 2 ) {

		m_LDEMCt    = uSize - 2;
		m_LPosition = min(m_LPosition, m_LDEMCt - 1);
		m_LTarget   = GetIDFromString(m_LDEMStr);
		m_LDEMSel   = m_LTarget;

		return;
		}

	if( m_LTarget ) m_LDEMCt = 0; // new string

	else m_LTarget = 1;

	m_LPosition = 0;
	}

CString CM2MDataSerialDriverOptions::GetFront(UINT uPos, CString sStr)
{
	return uPos ? sStr.Left(uPos) : TEXT("");
	}

CString CM2MDataSerialDriverOptions::GetRear(UINT uPos, CString sStr)
{
	UINT uSL  = sStr.GetLength();

	return uPos < uSL ? sStr.Right(uSL - uPos) : TEXT("");
	}

UINT CM2MDataSerialDriverOptions::GetIDFromString(CString sStr)
{
	return GetHexStrVal(sStr.Left(2));
	}

UINT CM2MDataSerialDriverOptions::GetCountFromString(CString sStr)
{
	return sStr.GetLength() - 2;
	}

UINT  CM2MDataSerialDriverOptions::GetTypeFromDEMStr(UINT uPosition)
{
	switch( m_LDEMStr[uPosition + 2] ) {

		case CSBIT:  return LTYPEB;
		case CSBYTE: return LTYPEY;
		case CSWORD: return LTYPEW;
		}

	return LTYPEL;
	}

UINT CM2MDataSerialDriverOptions::GetTypeFromTypeS(void)
{
	CString sType = m_LTypeS;

	switch( sType[0] ) {

		case 'W':
		case CSWORD: return LTYPEW;
		case CSLONG: return LTYPEL;
		case CSBYTE: return LTYPEY;
		case CSBIT:  return LTYPEB;
		case 'B':    return sType[1] == 'y' ? LTYPEY : LTYPEB;
		}

	return LTYPEW;
	}

char CM2MDataSerialDriverOptions::MakeChar0(UINT uType)
{
	switch( uType ) {

		case LTYPEW: return CSWORD;
		case LTYPEY: return CSBYTE;
		case LTYPEB: return CSBIT;
		}

	return CSLONG;
	}

void CM2MDataSerialDriverOptions::UpdateSize(void)
{
	UINT uLen = m_LDEMStr.GetLength();

	if( uLen > MAXDEM) {

		m_LPosition = 0;

		m_LDEMCt    = MAXDEM;

		m_LDEMStr   = m_LDEMStr.Left(MAXDEM);

		return;
		}

	UINT uCt = GetCountFromString(m_LDEMStr);

	m_LDEMCt = uCt;

	if( m_LPosition >= uCt ) {

		m_LPosition = uCt ? uCt - 1 : 0;
		}
	}

// UI Updates

void CM2MDataSerialDriverOptions::UpdateDEMUI(CUIViewWnd *pWnd)
{
	pWnd->UpdateUI(TEXT("LDEMCt"));
	pWnd->UpdateUI(TEXT("LDEMStr"));

	EnableUIs(pWnd);
	}

void CM2MDataSerialDriverOptions::EnableUIs(CUIViewWnd *pWnd)
{
	pWnd->EnableUI(TEXT("LDEMCt"), TRUE);
	}

UINT CM2MDataSerialDriverOptions::GetHexStrVal(CString s)
{
	return tstrtoul(s, NULL, 16);
	}

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp TCP/IP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CM2MDataTCPDriverOptions, CM2MDataSerialDriverOptions);

// Constructor

CM2MDataTCPDriverOptions::CM2MDataTCPDriverOptions(void)
{
	}

void CM2MDataTCPDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CM2MDataSerialDriverOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CM2MDataTCPDriverOptions::MakeInitData(CInitData &Init)
{
	return CM2MDataSerialDriverOptions::MakeInitData(Init);
	}

//////////////////////////////////////////////////////////////////////////
// Serial Driver  Serial Driver  Serial Driver  Serial Driver/////////////
//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Serial Driver
//

// Instantiator

ICommsDriver *	Create_M2MDataSerialDriver(void)
{
	return New CM2MDataSerialDriver;
	}

// Constructor

CM2MDataSerialDriver::CM2MDataSerialDriver(void)
{
	m_wID		= 0x406B;

	m_uType		= driverSlave;
	
	m_Manufacturer	= TEXT("M2M Data Corp.");
	
	m_DriverName	= TEXT("Enabled Message Protocol");
	
	m_Version	= TEXT("1.00");
	
	m_ShortName	= TEXT("M2M Enabled Message");

	m_DevRoot	= TEXT("M2M_");

	AddSpaces();
	}

// Binding Control

UINT CM2MDataSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CM2MDataSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CM2MDataSerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CM2MDataSerialDriverOptions);
	}

CLASS CM2MDataSerialDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Address Management

BOOL CM2MDataSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CM2MDataAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Helpers

BOOL CM2MDataSerialDriver::HasList(UINT uTable)
{
	switch( uTable ) {

		case TDEMA:
		case TSCHA:	return TRUE;
		}

	return FALSE;
	}

// Access String Data
CString CM2MDataSerialDriver::GetStringX(UINT u, CItem * pConfig)
{
	if( ++u > MAXIDQ ) return TEXT("00");

	CString sHead = TEXT("LDEMStr");

	CString sSuff = TEXT("");

	if( u < 10 ) sSuff.Printf( TEXT("%1.1d"), u );

	else sSuff.Printf( TEXT("%2.2d"), u );

	return pConfig->GetDataAccess(sHead + sSuff)->ReadString(pConfig);
	}

CString CM2MDataSerialDriver::MatchStringX(UINT uID, CItem * pConfig, PBYTE pStrNum)
{
	if( uID ) {

		for( UINT i = 0; i < MAXIDQ; i++ ) {

			CString ss = GetStringX( i, pConfig );

			if( uID == tstrtoul( ss.Left(2), NULL, 16 ) ) {

				*pStrNum = LOBYTE(i);

				return ss;
				}
			}
		}

	return TEXT("00");
	}

// Text Format for DEM and SCH spaces:
// iii(Ttt_Pt)
// ppp = position in block (0-511)
// iii = id (1-199)
// t=datatype 0=Long, 1=Word, 2=Byte, 3=Bit
// Table is TDEMA or TSCHA
// Offset = Low 9 bits are position, high 5 bits are the string number (0-<MAXIDQ-1>)->(String1-String<MAXIDQ)
// Bits 9 and 10 are coded type
// Extra  = not used
// Cfg string format IITTTTTT... II=ID, T=Data type for position 0-n

// Text Format for RBE and CTL spaces:
// RWaaaaa
// Table  = TRBEW(Word)
// Offset = aaaaa (RTU Address Identifier);
// Extra  = not used
// Type   = always Word

// Text Format for FIL spaces:
// FAnnnnn - FLnnnnn
// nnnnn  Data DWORD offset
// Table  = indicated by Prefix
// Offset = 16 bits File offset;
// Extra  = not used
// Type   = always long

// Signals for SCH, RBE, and File Send to Server
// Text   = 0 - Item value, 1 = Signal Time or use driver time when X[1] == 1
// Table  = selected signal
// Offset = Value or Time
// Extra  = not used
// Type   = always Long

// Address Helpers

BOOL CM2MDataSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString sErr = TEXT("??");

	if( pSpace ) {

		Text.MakeUpper();

		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = LL;

		if( !HasList(pSpace->m_uTable) ) {

			Addr.a.m_Offset = tatoi(Text);

			if( pSpace->m_Prefix[1] == 'W') Addr.a.m_Type = WW;

			return TRUE;
			}

		UINT uFind = Text.Find(TEXT("_P"));

		if( uFind == NOTHING ) return FALSE;

		UINT uPos  = tatoi(Text.Mid(uFind+2));	// position in list

		UINT uID   = tatoi(Text);		// ID

		BYTE bStr  = 0;

		CString ss = MatchStringX(uID, pConfig, &bStr);

		UINT uCt   = ss.GetLength();

		if( uCt > uPos + 2 ) {

			Addr.a.m_Offset = uPos + (bStr << POSSTN);

			return TRUE;
			}

		if( uCt <= 2 ) sErr = TEXT("DEM/SCH List is not configured for this ID.");

		else {
			sErr.Printf( TEXT("%s1 - %s%d"),

				pSpace->m_Prefix,
				pSpace->m_Prefix,
				MAXENT
				);
			}
		}

	Error.Set( sErr, 0);

	return FALSE;
	}

BOOL CM2MDataSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
// DEM DrvStr Format is IITTT... II=ID, T=Data Type for position 0-n 
// Expand Format is: <Prefix>iii(Tt_Pppp)
// ppp = Low 9 bits of Addr.a.m_Offset
// iii = decimal of hex II
// t   = desired data type gotten from letter at string location = ppp + 2

	CSpace * pSpace = GetSpace(Addr);

	if( pSpace ) {

		if( !HasList(Addr.a.m_Table) ) return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);

		UINT uPos  = Addr.a.m_Offset & MAXENT;

		BYTE bStr  = LOBYTE(Addr.a.m_Offset >> POSSTN);

		CString ss = GetStringX(bStr, pConfig);

		UINT uLen  = ss.GetLength();

		if( uLen < 3 ) return FALSE;

		UINT uTyp  = CSLONG;

		if( uPos < uLen ) uTyp  = ss[uPos + 2];

		UINT uType = CELONG;

		switch( uTyp ) {

			case CSWORD: uType = CEWORD; break;
			case CSBYTE: uType = CEBYTE; break;
			case CSBIT:  uType = CEBIT; break;
			}

		Text.Printf( TEXT("%s%d(T%d_P%d)"),

			pSpace->m_Prefix,
			tstrtoul(ss.Left(2), NULL, 16),
			uType,
			uPos
			);

		return TRUE;
		}

	return FALSE;
	}

// protected

// Implementation for Direct Ref

void CM2MDataSerialDriver::AddSpaces(void)
{
// Demand Poll
	AddSpace(New CSpace(TDEMA, "D", "Demand Poll List Position",		10, 0,	  511,	LL));

// Scheduled Data
	AddSpace(New CSpace(TSCHA, "S",  "Scheduled Data List Position",	10, 0,	  511,	LL));
	AddSpace(New CSpace(TSCHE, "AS", "AS0=ID of Scheduled Data to send",	10, 0,      0,  LL));

// Control
//WW	AddSpace(New CSpace(TCTLR, "CR", "Control Data - Real",			10, 0,	65535,	addrRealAsReal));
//WW	AddSpace(New CSpace(TCTLL, "CL", "Control Data - Long",			10, 0,	65535,	addrLongAsLong));
	AddSpace(New CSpace(TCTLW, "CW", "Control Data ID",			10, 0,	65535,	addrWordAsWord));
//WW	AddSpace(New CSpace(TCTLY, "CY", "Control Data - Byte",			10, 0,	65535,	addrByteAsByte));
//WW	AddSpace(New CSpace(TCTLB, "CB", "Control Data - Bit",			10, 0,	65535,	addrBitAsBit));

// Report By Exception
//WW	AddSpace(New CSpace(TRBER, "RR", "Report By Exception - Real",		10, 0,	65535,	addrRealAsReal));
//WW	AddSpace(New CSpace(TRBEL, "RL", "Report By Exception - Long",		10, 0,	65535,	addrLongAsLong));
	AddSpace(New CSpace(TRBEW, "RW", "Report By Exception ID",		10, 0,	65535,	addrWordAsWord));
//WW	AddSpace(New CSpace(TRBEY, "RY", "Report By Exception - Byte",		10, 0,	65535,	addrByteAsByte));
//WW	AddSpace(New CSpace(TRBEB, "RB", "Report By Exception - Bit",		10, 0,	65535,	addrBitAsBit));
	AddSpace(New CSpace(TRBEE, "AR", "AR0=ID, AR1=Time/Execute",		10, 0,      1,  LL));

// File
	AddSpace(New CSpace(TF210, "FA", "File 210: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF211, "FB", "File 211: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF212, "FC", "File 212: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF213, "FD", "File 213: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF214, "FE", "File 214: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF215, "FF", "File 215: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF216, "FG", "File 216: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF217, "FH", "File 217: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF218, "FI", "File 218: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF219, "FJ", "File 219: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF220, "FK", "File 220: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF221, "FL", "File 221: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF222, "FM", "File 222: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF223, "FN", "File 223: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF224, "FO", "File 224: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF225, "FP", "File 225: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF226, "FQ", "File 226: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF227, "FR", "File 227: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF228, "FS", "File 228: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF229, "FT", "File 229: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF230, "FU", "File 230: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF231, "FV", "File 231: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF232, "FW", "File 232: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF233, "FX", "File 233: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF234, "FY", "File 234: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TF235, "FZ", "File 235: 4 Characters 1-20 Items",	10, 0,	   19,	LL));
	AddSpace(New CSpace(TFILE, "AF", "AF0=Req File #, AF1=Count/Trigger",	10, 0,      1,  LL));

	AddSpace(New CSpace(TZONE, "TZ", "Time Zone offset (use Time Manager)",	16, 0,      0,  LL));

	AddSpace(New CSpace(TMOUT, "TO", "Serial Timeout(ms) default = 20000",	10, 0,	    0,  LL));

	AddSpace(New CSpace(TERR,  "ER", "Run Time Error Values",		16, 0,      2,  LL));
	}

// Helpers
BOOL CM2MDataSerialDriver::IsFILID(UINT uTable)
{
	return uTable >= TF210 && uTable <= TF235;
	}

//////////////////////////////////////////////////////////////////////////
// TCP/IP Master Driver  TCP/IP Master Driver  TCP/IP Master Driver///////
//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_M2MDataTCPDriver(void)
{
	return New CM2MDataTCPDriver;
	}

// Constructor

CM2MDataTCPDriver::CM2MDataTCPDriver(void)
{
	m_wID		= 0x3527;

	m_uType		= driverSlave;
	
	m_Manufacturer	= TEXT("M2M Data");
	
	m_DriverName	= TEXT("Enabled Message Protocol");
	
	m_Version	= TEXT("1.00");
	
	m_ShortName	= TEXT("M2M Data - Enabled Message Protocol");
	}

// Binding Control

UINT CM2MDataTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CM2MDataTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS	CM2MDataTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CM2MDataTCPDriverOptions);
	}

//////////////////////////////////////////////////////////////////////////
// DIALOG DIALOG DIALOG DIALOG ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CM2MDataAddrDialog, CStdAddrDialog);
		
// Constructor

CM2MDataAddrDialog::CM2MDataAddrDialog(CM2MDataSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_pM2MDriver = &Driver;

	m_fPart	   = fPart;

	m_pDrvCfg  = pConfig;

	SetName(TEXT("M2MDataElementDlg"));
	}

// Message Map

AfxMessageMap(CM2MDataAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange)

	AfxDispatchCommand(DBUPD, OnButtonClicked)

	AfxDispatchCommand(DBNEXT, OnButtonClicked)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CM2MDataAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_DEMString = NO_ID;

	m_fUpdate   = FALSE;

	UINT i = 0;

	while( i < MAXIDQ ) {

		CString ss = GetStringX(i, m_pDrvCfg);

		if( GetDEMID(ss) ) { // find first programmed string

			m_DEMString = ss;

			break;
			}

		i++;
		}

	SetStrParams(m_DEMString);

	m_uPos		= 0;

	CAddress &Addr	= (CAddress &) *m_pAddr;

	FindSpace();

	CStdAddrDialog::LoadList();

	if( m_pSpace ) {

		if( IsSignal(m_pSpace->m_uTable) ) SetEnables(0);

		else SetEnables((BYTE)m_pSpace->m_Prefix[0]);
		}

	else SetEnables(0);

	DoShow(Addr);

	SetDlgFocus(1001);

	GetDlgItem(2002).EnableWindow(!IsFileID(Addr.a.m_Table));

	return m_fPart ? FALSE : !BOOL(Addr.a.m_Table);
	}

void CM2MDataAddrDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			if( IsSCHn(m_pSpace->m_uTable ) ) {

				if( m_uDEMCount ) Addr.m_Ref = GetSCH0(m_pSpace->m_uTable);

				else {
					SetEnables(0);

					m_pSpace = NULL;

					return;
					}
				}
			
			else m_pSpace->GetMinimum(Addr);

			DoShow(Addr);

			SetDlgFocus( IsFileID(Addr.a.m_Table) ? 1001 : DBPOS );

			GetDlgItem(2002).EnableWindow(!IsFileID(Addr.a.m_Table));
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(DBPRE).SetWindowText(TEXT("None"));
		GetDlgItem(DBPOS).SetWindowText(TEXT(""));
		GetDlgItem(DBTYP).SetWindowText(TEXT(""));
		GetDlgItem(DBTARV).SetWindowText(TEXT(""));
		GetDlgItem(DBDEVV).SetWindowText(TEXT(" "));

		SetEnables(0);
		}
	}

// Command Handlers

BOOL CM2MDataAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = GetAddressText();

		CError   Error(TRUE);

		CAddress Addr;

		if( !Text.IsEmpty() ) {
		
			if( m_pM2MDriver->DoParseAddress(Error, Addr, m_pDrvCfg, m_pSpace, Text) ) {

				*m_pAddr = Addr;

				if( !m_fUpdate ) {

					EndDialog(TRUE);
					}

				return TRUE;
				}

			Error.Show(ThisObject);

			return TRUE;
			}
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CM2MDataAddrDialog::OnButtonClicked(UINT uID)
{
	char c0   = (char)(GetDlgItem(DBPRE).GetWindowText())[0];

	UINT uPos = tatoi(GetDlgItem(DBPOS).GetWindowText());

	if( uID == DBNEXT ) {

		uPos++;

		GetDlgItem(DBPOS).SetWindowText(m_pSpace->GetValueAsText(uPos, 3));
		}

	UINT uMax = 0;

	switch( c0 ) {

		case 'D':
		case 'S':
			uMax = m_uDEMCount;
			break;

		case 'R':
		case 'C':
			uMax = MAXENT;
			break;

		case 'F':
			uMax = 0;
			break;

		case 'A':
			uMax = m_pSpace->m_uMaximum + 1;
			break;
		}

	if( uPos >= uMax ) {

		if( uID == DBNEXT ) {

			GetDlgItem(DBPOS).SetWindowText(TEXT("0"));

			OnButtonClicked(DBUPD);

			if( c0 == 'F' ) {

				GetDlgItem(DBPOS).EnableWindow(FALSE);

				SetDlgFocus(1001);
				}

			else {
				SetDlgFocus(DBPOS);
				}

			return TRUE;
			}

		else GetDlgItem(DBPOS).SetWindowText(TEXT("0"));
		}

	UINT uOldT = m_pSpace->m_uTable;

	m_fUpdate = TRUE;

	OnOkay(uID);

	m_fUpdate = FALSE;

	FindSpace();

	if( m_pSpace->m_uTable != uOldT ) LoadList();

	CAddress Addr = *m_pAddr;

	DoShow(Addr);

	if( uID != IDOK ) SetDlgFocus(DBPOS);

	return TRUE;
	}

// Overridables

void CM2MDataAddrDialog::SetAddressText(CString Text)
{
	char T0 = 0;

	if( !Text.IsEmpty() ) {

		Text.MakeUpper();

		T0	     = (char)Text[0];

		UINT uPreLen = IsSCHc(T0) ? 1 : 2;

		GetDlgItem(DBPRE).SetWindowText(Text.Left(uPreLen));

		SetPosInfo(Text);

		if( IsSCHc(T0) ) SetTargetInfo(Text);

		SetTypeInfo(Text);
		}

	SetEnables(BYTE(T0));
	}

CString CM2MDataAddrDialog::GetAddressText(void)
{
	CString ss = TEXT("<none>");

	if( m_pSpace ) {

		BYTE T0 = (char)m_pSpace->m_Prefix[0];

		m_uPos = tatoi(GetDlgItem(DBPOS).GetWindowText());

		if( T0 == 'A' ) {

			m_uPos = min(m_uPos, m_pSpace->m_uMaximum);

			ss.Printf( TEXT("%1.1d"), m_uPos );

			return ss;
			}

		if( IsSCHc(T0) ) {

			UINT uID = tatoi(GetDlgItem(DBTARV).GetWindowText());

			if( uID != GetDEMID(m_DEMString) ) {				

				CString ss  = MatchStringX(uID, m_pDrvCfg);

				if( ss.GetLength() > 2 ) {

					m_DEMString = ss;

					m_uDEMCount = ss.GetLength() - 2;
					}

				else uID = GetDEMID(m_DEMString);

				if( m_uPos > m_uDEMCount ) m_uPos = 0;
				}

			if( !m_uDEMCount ) {

				GetDlgItem(DBPOS).SetWindowText(TEXT("0"));

				GetDlgItem(DBTYP).SetWindowText(TEXT("Word"));

				GetDlgItem(DBTARV).SetWindowText(TEXT("0"));

				ss.Printf(TEXT("%c0(T0_P0)"), T0);

				return ss;
				}

			BYTE b   = (BYTE)m_DEMString[m_uPos + 2];

			switch(b) {

				case CSBIT:  b = CEBIT;  break;
				case CSBYTE: b = CEBYTE; break;
				case CSWORD: b = CEWORD; break;
				case CSLONG: b = CELONG; break;
				}

			ss.Printf( TEXT("%d(T%d_P%d)"), uID, b, m_uPos);
			}

		else if (T0 == 'F' ) { // construct full string

			ss.Printf( TEXT("%d(%d)"), m_uPos, m_pSpace->m_uTable);
			}

		else ss.Printf( TEXT("%d"), m_uPos );
		}

	return ss;
	}

// Override Help

void CM2MDataAddrDialog::SetPosInfo(CString Text)
{
	UINT uPos = 0;

	if( IsSCHc((char)Text[0]) ) {

		UINT uFind = Text.Find(TEXT("_P"));

		if( uFind < NOTHING ) uPos = tatoi(Text.Mid(uFind+2));
		}

	else uPos = tatoi(Text.Mid(1));

	CString sItem;

	sItem.Printf( TEXT("%d"), uPos );

	GetDlgItem(DBPOS).SetWindowText(sItem);
	}

void CM2MDataAddrDialog::SetTargetInfo(CString Text)
{
	UINT   uTargID = tatoi(Text.Mid(1));

	if( !uTargID ) uTargID = GetDEMID(m_DEMString);

	CString sTargID;

	sTargID.Printf( TEXT("%d"), uTargID );

	GetDlgItem(DBTARV).SetWindowText(sTargID);

	GetDlgItem(DBTARV).EnableWindow(TRUE);
	GetDlgItem(DBTART).EnableWindow(TRUE);
	}

void CM2MDataAddrDialog::SetTypeInfo(CString Text)
{
	char T0 = (char)Text[0];

	CString sText = TEXT("Long");

	if( IsSCHc(T0) ) {

		UINT uType = 0;

		UINT uFind = Text.Find(TEXT("(T"));

		if( uFind < NOTHING ) uType = tatoi(Text.Mid(uFind+2));

		else uType = GetTypeFromStr(m_DEMString, m_uPos);

		switch( uType ) {

			case CEWORD: sText = TEXT("Word"); break;
			case CEBYTE: sText = TEXT("Byte"); break;
			case CEBIT:  sText = TEXT("Bit");  break;
			}
		}

	else {
		if( T0 == 'R' || T0 == 'C' ) sText = TEXT("Word");
		}

	GetDlgItem(DBTYP).SetWindowText(sText);
	}

UINT CM2MDataAddrDialog::GetTypeFromStr(CString ss, UINT uPos)
{
	if( ss.GetLength() > uPos + 2 ) {

		switch( (UINT)ss[uPos+2] ) {

			case CSWORD: return CEWORD;
			case CSBYTE: return CEBYTE;
			case CSBIT:  return CEBIT;
			}
		}

	return CELONG;
	}

CString CM2MDataAddrDialog::GetStringX(UINT i, CItem *pConfig) {

	return m_pM2MDriver->GetStringX(i, pConfig);
	}

CString CM2MDataAddrDialog::MatchStringX(UINT uID, CItem *pConfig)
{
	BYTE bStrNum = 0;

	return m_pM2MDriver->MatchStringX(uID, pConfig, &bStrNum);
	}

UINT CM2MDataAddrDialog::GetDEMID(CString ss)
{
	return tstrtoul(ss.Left(2), NULL, 16);
	}

void CM2MDataAddrDialog::SetStrParams(CString ss)
{
	m_DEMString = ss;

	m_uDEMCount = ss.GetLength() - 2;
	}

// Helpers

void CM2MDataAddrDialog::DoShow(CAddress Addr)
{
	CString Text;

	BOOL fFile = IsFileID(Addr.a.m_Table);

	if( fFile ) {

		Addr.a.m_Offset = 0;
		}

	if( m_pM2MDriver->DoExpandAddress(Text, m_pDrvCfg, Addr) ) {

		SetAddressText(Text);

		return;
		}

	GetDlgItem(DBPRE ).SetWindowText(TEXT("None"));
	GetDlgItem(DBPOS ).SetWindowText(TEXT("0"));
	GetDlgItem(DBTYP ).SetWindowText(TEXT(""));
	GetDlgItem(DBTARV).SetWindowText(TEXT(""));

	SetEnables(0);
	}

void CM2MDataAddrDialog::SetEnables(BYTE bEnable)
{
	GetDlgItem(DBPRE ).EnableWindow(bEnable);

	GetDlgItem(DBPOS ).EnableWindow(bEnable);

	GetDlgItem(DBTYP ).EnableWindow(bEnable);
	GetDlgItem(DBTYPT).EnableWindow(bEnable);

	GetDlgItem(DBUPD ).EnableWindow(bEnable);
	GetDlgItem(DBNEXT).EnableWindow(bEnable);

	GetDlgItem(DBTART).EnableWindow(IsSCHc(bEnable));
	GetDlgItem(DBTARV).EnableWindow(IsSCHc(bEnable));
	}

DWORD CM2MDataAddrDialog::GetSCH0(UINT uTable)
{
	BYTE bStrNum = 0;

	CString ss;

	ss = m_pM2MDriver->MatchStringX(GetDEMID(m_DEMString), m_pDrvCfg, &bStrNum);

	if( ss.GetLength() > 2 ) {

		CAddress Addr;

		Addr.a.m_Table  = uTable;
		Addr.a.m_Offset = bStrNum << POSSTN;
		Addr.a.m_Type   = LL;
		Addr.a.m_Extra  = 0;

		return Addr.m_Ref;
		}

	return 0;
	}

BOOL CM2MDataAddrDialog::IsSCHn(UINT uTable)
{
	return uTable == TDEMA || uTable == TSCHA;
	}

BOOL CM2MDataAddrDialog::IsSCHc(char cPre)
{
	return cPre == 'D' || cPre == 'S';
	}

void CM2MDataAddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	CArray <int> Tabs;

	Tabs.Append(40);

	Tabs.Append(100);
	
	Tabs.Append(160);

	ListBox.SetTabStops(Tabs);

	CString Entry;
			
	Entry.Printf(TEXT("<%s>\t%s"), CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING);

	INDEX	         Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( AllowSpace(pSpace) ) {

				CString Entry;
			
				Entry.Printf(TEXT("%s\t%s"), pSpace->m_Prefix, pSpace->m_Caption);

				ListBox.AddString(Entry, DWORD(n) );

				if( pSpace == m_pSpace ) {

					Find = n;
					}
				}

			List.GetNext(n);
			}
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSelChange(1001, ListBox);
	}

UINT CM2MDataAddrDialog::IsSignal(UINT uTable)
{
	return uTable == TSCHE || uTable == TRBEE || uTable == TFILE;
	}

BOOL CM2MDataAddrDialog::IsFileID(UINT uTable)
{
	return uTable >= TF210 && uTable <= TF235;
	}

//////////////////////////////////////////////////////////////////////////
// DIALOG DIALOG DIALOG DIALOG ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Show List
//

// Runtime Class

AfxImplementRuntimeClass(CM2MShowListDialog, CStdDialog);
		
// Constructor

CM2MShowListDialog::CM2MShowListDialog(CM2MDataSerialDriverOptions &Options)
{
	m_pOpt = &Options;

	m_sDEM = m_pOpt->m_LDEMStr;

	SetName(TEXT("M2MDataShowListDlg"));
	}

// Message Map

AfxMessageMap(CM2MShowListDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1003, LBN_SELCHANGE, OnSelChange)

	AfxDispatchCommand(SLDBAP, OnButtonClicked)
	AfxDispatchCommand(SLDBIN, OnButtonClicked)
	AfxDispatchCommand(SLDBRE, OnButtonClicked)
	AfxDispatchCommand(SLDBDE, OnButtonClicked)
	AfxDispatchCommand(SLDBNS, OnButtonClicked)
	AfxDispatchCommand(FTDBSH, OnButtonClicked)
	AfxDispatchCommand(SLDBSV, OnButtonClicked)
	AfxDispatchCommand(SLDBCN, OnButtonClicked)

	AfxMessageEnd(CStdDialog)
	};

// Message Handlers

BOOL CM2MShowListDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	UINT uID   = 0;

	CString s  = TEXT("001");

	CString sx = TEXT("01");

	UINT uLen  = m_sDEM.GetLength();

	if( uLen > 2 ) {

		uID = tstrtoul(m_sDEM.Left(2), NULL, 16);

		m_pOpt->m_LTarget = uID;

		if( IsFileTransfer() ) {

			s.Printf(TEXT("%d"), (uLen - 3) / 2); // strip ID and file count

			GetDlgItem(FTDBMX).SetWindowText(s);
			}

		s.Printf(TEXT("%3.3d"), uID);

		sx = m_sDEM;
		}

	if( !uID ) {

		m_pOpt->m_LTarget = 1;
		}

	GetDlgItem(SLDBID).SetWindowText(s);

	m_sDEM = sx;

	m_pOpt->ExecuteFunction(EFINIT);

	UpdateParams();

	m_uDEMCount = m_pOpt->m_LDEMCt;

	LoadTypes();

	LoadList();

	SetTypeToBox(m_pOpt->m_LTypeS);

	return TRUE;
	}

void CM2MShowListDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox   = (CListBox &) GetDlgItem(1003);

	UINT  uPos          = ListBox.GetCurSel();

	m_pOpt->m_LPosition = uPos;

	LoadList();

	SetTypeToBox(GetTypeFromString(uPos));
	}

BOOL  CM2MShowListDialog::OnButtonClicked(UINT uID)
{
	UINT uDBID = tatoi(GetDlgItem(SLDBID).GetWindowText());

	if( uID == SLDBCN ) {

		m_pOpt->m_LTarget   = uDBID;

		m_pOpt->m_LDEMCt    = m_sDEM.GetLength() - 2;

		m_pOpt->m_LPosition = 0;

		m_pOpt->ExecuteFunction(EFUPDT);

		EndDialog(TRUE);

		return TRUE;
		}

	if( uID == SLDBNS || uID == FTDBSH ) { // Reload ID value

		m_pOpt->m_LTarget   = uDBID;

		m_pOpt->m_LPosition = 0;
		}

	if( IsFileTransfer() ) {

		UINT uQty = tatoi(GetDlgItem(FTDBMX).GetWindowText());

		if( uQty > 26 ) {

			uQty = 26;

			GetDlgItem(FTDBMX).SetWindowText(TEXT("26"));
			}

		m_pOpt->m_LDEMCt = uQty;

		if( uID == SLDBNS && !uQty ) {

			m_pOpt->m_LTarget = 6;

			m_pOpt->ExecuteFunction(EFUPDT);

			CString s = m_pOpt->m_LDEMStr;

			UINT uLen = s.GetLength();

			if( uLen > 3 ) {

				uQty = (uLen - 3) / 2;

				s.Printf(TEXT("%d"), uQty);

				GetDlgItem(FTDBMX).SetWindowText(s);
				}
			}
		}

	m_pOpt->m_LTypeS = GetTypeFromBox();

	m_pOpt->ExecuteFunction(uID);

	UpdateType();	

	UpdateParams();

	LoadList();

	SelectFocus(IsFileTransfer() ? FTDBMX : SLDBID);

	return TRUE;
	}

void CM2MShowListDialog::LoadTypes(void)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(SLDBTY);

	Box.AddString(TEXT("Word"));
	Box.AddString(TEXT("Long"));
	Box.AddString(TEXT("Byte"));
	Box.AddString(TEXT("Bit"));
	}

CString CM2MShowListDialog::GetTypeFromBox(void)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(SLDBTY);

	return GetTypeInfo(Box.GetCurSel());
	}

void CM2MShowListDialog::SetTypeToBox(CString sType)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(SLDBTY);

	Box.SetCurSel(GetTypeNumber(sType));
	}

// Helpers

CString CM2MShowListDialog::GetTypeInfo(UINT uType)
{
	switch( uType ) {

		case LTYPEL: return TEXT("Long");
		case LTYPEY: return TEXT("Byte");
		case LTYPEB: return TEXT("Bit");
		}

	return TEXT("Word");
	}

CString CM2MShowListDialog::GetTypeFromString(UINT uPos)
{
	if( m_sDEM.GetLength() > uPos + 2 ) {

		switch( (UINT)m_sDEM[uPos+2] ) {

			case CSLONG: return GetTypeInfo(LTYPEL);
			case CSBYTE: return GetTypeInfo(LTYPEY);
			case CSBIT:  return GetTypeInfo(LTYPEB);
			}
		}

	return GetTypeInfo(LTYPEW);
	}

UINT CM2MShowListDialog::GetTypeNumber(CString sType)
{
	switch( sType[0] ) {

		case 'L': return LTYPEL;
		case 'E': return LTYPEY;
		case '@': return LTYPEB;
		case 'B': return (sType[1] == 'y') ? LTYPEY : LTYPEB;
		}

	return LTYPEW;
	}

void CM2MShowListDialog::LoadList(void)
{
	if( IsFileTransfer() ) LoadListFIL();

	else LoadListDEM();
	}

void CM2MShowListDialog::LoadListDEM(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(SLDBLB);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	CString Entry;

	if( !m_uDEMCount ) {

		ListBox.AddString(TEXT("Empty"), NOTHING);

		ListBox.SetCurSel(0);
		}

	else {

		for( UINT i = 0; i < m_uDEMCount; i++ ) {

			Entry.Printf(TEXT("%3.3d\t%s"), i, GetTypeFromString(i));

			ListBox.AddString(Entry, NOTHING);
			}

		ListBox.SetCurSel(m_pOpt->m_LPosition);
		}

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);
	}

void CM2MShowListDialog::LoadListFIL(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(FTDBLB);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	CString Entry;

	UINT uQty = tatoi(GetDlgItem(FTDBMX).GetWindowText());

	Entry.Printf(TEXT("00 File Count = %d "), uQty);

	ListBox.AddString(Entry, NOTHING);

	for( UINT i = 1, j = 1; i <= uQty; i++, j += 2 ) {

		Entry.Printf(TEXT("%2.2d F%c (%d) Size\tWord"), j, i + '@', i + 209);

		ListBox.AddString(Entry, NOTHING);

		Entry.Printf(TEXT("%2.2d     Date/Time\tLong"), j + 1);

		ListBox.AddString(Entry, NOTHING);
		}

	ListBox.SetCurSel(0);

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);
	}

void  CM2MShowListDialog::UpdateParams(void)
{
	m_sDEM      = m_pOpt->m_LDEMStr;
	m_uDEMCount = m_pOpt->m_LDEMCt;
	m_uPosition = m_pOpt->m_LPosition;
	m_uType     = GetTypeNumber(m_pOpt->m_LTypeS);

	CString sId;

	if( m_sDEM.GetLength() > 2 ) {

		sId.Printf(TEXT("%3.3d"), tstrtoul(m_sDEM.Left(2), NULL, 16));
		}

	else {
		sId.Printf(TEXT("%3.3d"), m_pOpt->m_LTarget);
		}

	GetDlgItem(SLDBID).SetWindowText(sId);

	sId.Printf(TEXT("%d"), m_uDEMCount);

	GetDlgItem(SLDBCT).SetWindowText(sId);

	EnableList();
	}

void CM2MShowListDialog::UpdateType(void)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(SLDBTY);

	m_uType = Box.GetCurSel();
	}

void CM2MShowListDialog::SelectFocus(UINT uID)
{
	UINT uRtn = SLDBID;

	if( IsListFunction(uID) )     uRtn = SLDBLB;

	else if ( IsTypeSelect(uID) ) uRtn = SLDBTY;

	SetDlgFocus(uRtn);
	}

BOOL CM2MShowListDialog::IsListFunction(UINT uID)
{
	switch( uID ) {

		case SLDBAP:
		case SLDBIN:
		case SLDBRE:
		case SLDBDE: return TRUE;
		}

	return FALSE;
	}

BOOL CM2MShowListDialog::IsTypeSelect(UINT uID)
{
	return uID == SLDBTY;
	}

BOOL CM2MShowListDialog::IsNewShow(UINT uID)
{
	return uID == SLDBNS;
	}

BOOL CM2MShowListDialog::IsFileTransfer(void)
{
	return m_pOpt->m_LTarget == 6;
	}

void CM2MShowListDialog::EnableList(void)
{
	GetDlgItem(SLDBID).EnableWindow(TRUE);
	GetDlgItem(SLDBNS).EnableWindow(TRUE);
	GetDlgItem(SLDBSV).EnableWindow(TRUE);
	GetDlgItem(SLDBCN).EnableWindow(TRUE);

	BOOL fEnable = IsFileTransfer();

	// Demand Poll / Scheduled Data
	GetDlgItem(SLDBT1).EnableWindow(!fEnable);
	GetDlgItem(SLDBLB).EnableWindow(!fEnable);
	GetDlgItem(SLDBT2).EnableWindow(!fEnable);
	GetDlgItem(SLDBCT).EnableWindow(!fEnable);
	GetDlgItem(SLDBT3).EnableWindow(!fEnable);
	GetDlgItem(SLDBTY).EnableWindow(!fEnable);
	GetDlgItem(SLDBAP).EnableWindow(!fEnable);
	GetDlgItem(SLDBIN).EnableWindow(!fEnable);
	GetDlgItem(SLDBRE).EnableWindow(!fEnable);
	GetDlgItem(SLDBDE).EnableWindow(!fEnable);

	// File Transfer
	GetDlgItem(FTDBT1).EnableWindow(fEnable);
	GetDlgItem(FTDBLB).EnableWindow(fEnable);
	GetDlgItem(FTDBTX).EnableWindow(fEnable);
	GetDlgItem(FTDBMX).EnableWindow(fEnable);
	GetDlgItem(FTDBSH).EnableWindow(fEnable);
	}

// End of File
