
#include "intern.hpp"

#include "s5tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS5AS511TCPDeviceOptions, CUIItem);       

// Constructor

CS5AS511TCPDeviceOptions::CS5AS511TCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(200, 1), MAKEWORD(168, 192)));;

	m_Socket = 10010;

	m_Cache  = FALSE;

	m_Keep   = TRUE;

	m_Ping   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 200;

	m_Mode   = 0;
	}

// UI Managament

void CS5AS511TCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time2", !m_Keep);
			}
		}
	}

// Download Support

BOOL CS5AS511TCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Cache));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddByte(BYTE(m_Mode));

	return TRUE;
	}

// Meta Data Creation

void CS5AS511TCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Cache);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Mode);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master Driver
//

// Instantiator

ICommsDriver * Create_S5AS511TCPMasterDriver(void)
{
	return New CS5AS511TCPMasterDriver(FALSE);
	}

ICommsDriver * Create_S5AS511TCPMasterDriverV2(void)
{
	return New CS5AS511TCPMasterDriver(TRUE);
	}

// Constructor

CS5AS511TCPMasterDriver::CS5AS511TCPMasterDriver(BOOL fV2) : CS5AS511BaseDriver(fV2)
{
	m_DriverName = "S5 AS511 via TCP/IP";

	if( fV2 ) {
	
		m_wID	  = 0x33A2;

		m_Version = "2.02";
		}
	else {
		m_wID	  = 0x33A0;

		m_Version = "1.04";
		}
	}

// Configuration

CLASS CS5AS511TCPMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CS5AS511TCPDeviceOptions);
	}

// Binding Control

UINT CS5AS511TCPMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CS5AS511TCPMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// End of File
