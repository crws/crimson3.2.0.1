
#include "intern.hpp"

#include "masterk.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LS Master-K Serial Driver
//

// Instantiator

ICommsDriver *	Create_LSMasterKDriver(void)
{
	return New CLSMasterKDriver;
	}

// Constructor

CLSMasterKDriver::CLSMasterKDriver(void)
{
	m_wID		= 0x4046;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "LG - LS Industrial Systems";
	
	m_DriverName	= "Master-K via CNET";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Master-K";

	AddSpaces();
	}

// Binding Control

UINT CLSMasterKDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CLSMasterKDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Implementation

void CLSMasterKDriver::AddSpaces(void)
{
	AddSpace(New CSpace('P', "PW",  "I/O Relay Words",	10,  0,   31, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('M', "MW",  "Auxiliary Relay Words",10,  0,  191, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('K', "KW",	"Keep Relay Words",	10,  0,   31, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('L', "LW",  "Link Relay Words",	10,  0,   63, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('F', "FW",  "Special Relay Words",	10,  0,   63, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('T', "TX",  "Timer Contact Relay",	10,  0,  255, addrBitAsBit));
	AddSpace(New CSpace('U', "TW",  "Timer Elapsed Value",	10,  0,  255, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('C', "CX",  "Counter Contact Relay",10,  0,  255, addrBitAsBit));
	AddSpace(New CSpace('B', "CW",  "Counter Elapsed Value",10,  0,  255, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('S', "S",   "Step Controller",	10,  0,   99, addrWordAsWord,  addrWordAsReal));
	AddSpace(New CSpace('D', "D",   "Data Register",	10,  0, 4999, addrWordAsWord,  addrWordAsReal));
	}

// End of File
