
#include "intern.hpp"

#include "emroc3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Driver Options 3
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonRoc3MasterDriverOptions, CUIItem);       

// Constructor

CEmersonRoc3MasterDriverOptions::CEmersonRoc3MasterDriverOptions(void)
{
	m_Group  = 0;

	m_Unit   = 1;
	}

// UI Managament

void CEmersonRoc3MasterDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Unit" ) {

			if( pItem->GetDataAccess("Unit")->ReadInteger(pItem) == 240 ) {

				pWnd->Error(CString(IDS_DIRECT_CONNECT));

				pItem->GetDataAccess("Unit")->WriteInteger(pItem, 1);

				pWnd->UpdateUI();
				}
			}
		}
	}

// Download Support

BOOL CEmersonRoc3MasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Group));
	Init.AddByte(BYTE(m_Unit));
	
	return TRUE;
	}

// Meta Data Creation

void CEmersonRoc3MasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Device Options 3
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonRoc3MasterBaseDeviceOptions, CUIItem);       

// Constructor

CEmersonRoc3MasterBaseDeviceOptions::CEmersonRoc3MasterBaseDeviceOptions(void)
{
	m_Group		= 2;

	m_Unit		= 1;

	m_Poll		= 1000;

	m_OpID		= "LOI";

	m_Pass		= "";

	m_Acc		= 0;

	m_fUse		= FALSE;

	m_User		= 1;

	m_fChange	= FALSE;
	
	m_Increment	= 0;

	m_Ping		= 0x5B0000;

	m_Model		= NOTHING;

	m_pSlots	= new CSlotArray;

	// cppcheck-suppress sizeofDivisionMemfunc

	memset(PBYTE(m_uMark), 0, sizeof(WORD) * elements(m_uMark));
	}

// Destructor

CEmersonRoc3MasterBaseDeviceOptions::~CEmersonRoc3MasterBaseDeviceOptions(void)
{
	m_pSlots->Empty();

	delete m_pSlots;
	}

// UI Loading

BOOL CEmersonRoc3MasterBaseDeviceOptions::OnLoadPages(CUIPageList * pList)
{ 
	CEmersonRoc3MasterBaseDeviceOptionsUIPage * pPage = New CEmersonRoc3MasterBaseDeviceOptionsUIPage(this);

	pList->Append(pPage);
	
	return TRUE;			   
	}

// UI Managament

void CEmersonRoc3MasterBaseDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Pass" ) {

			m_PassNum = tatoi(m_Pass);
			}

		if( Tag.IsEmpty() || Tag == "Use" ) {

			pWnd->EnableUI("Acc", pItem->GetDataAccess("Use")->ReadInteger(pItem));
			}

		if( Tag == "Purge" ) {

			PurgeSlots();

			pWnd->Information(CString(IDS_OPERATION_IS));
			}

		if( Tag == "Unit" ) {

			if( pItem->GetDataAccess("Unit")->ReadInteger(pItem) == 240 ) {

				pWnd->Error(CString(IDS_DIRECT_CONNECT));

				pItem->GetDataAccess("Unit")->WriteInteger(pItem, 1);

				pWnd->UpdateUI();
				}
			}
		
		if( Tag.IsEmpty() || Tag == "User" ) {

			pWnd->EnableUI("Model", !pItem->GetDataAccess("User")->ReadInteger(pItem));
			}
		}

	CUIItem::OnUIChange(pWnd, pItem, Tag);
	}

void CEmersonRoc3MasterBaseDeviceOptions::LoadUI(IUICreate *pView, CItem *pItem)
{
	}

// Persistance

void CEmersonRoc3MasterBaseDeviceOptions::Init(void)
{
	CUIItem::Init();

	InitModel();
	}

void CEmersonRoc3MasterBaseDeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();

	m_PassNum  = tatoi(m_Pass);

	InitModel();
	}

// Persistance

void CEmersonRoc3MasterBaseDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "SlotList" ) {

			Tree.GetObject();

			Tree.GetName();

			UINT uSlots = Tree.GetValueAsInteger();

			for( UINT u = 0; u < uSlots; u++ ) {

				CSlot Slot;

				Tree.GetCollect();

				Tree.GetName();

				Slot.dwSlot   = Tree.GetValueAsInteger();

				Tree.GetName();

				Slot.dwTarget = Tree.GetValueAsInteger();

				Tree.GetName();

				Slot.Ref      = Tree.GetValueAsString();

				Tree.GetName();

				Slot.wExtent  = Tree.GetValueAsInteger() & 0xFFFF;

				AppendSlot(Slot);

				Tree.EndCollect();
				}

			Tree.EndObject();
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CEmersonRoc3MasterBaseDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutObject(L"SlotList");

	UINT uSlots = m_pSlots->GetCount();

	Tree.PutValue(L"Slots", uSlots);

	for( UINT u = 0; u < uSlots; u++ ) {

		Tree.PutCollect(L"Slot");

		Tree.PutValue(CPrintf(L"Slot%u",   u), m_pSlots->GetAt(u).dwSlot  );

		Tree.PutValue(CPrintf(L"Target%u", u), m_pSlots->GetAt(u).dwTarget);

		Tree.PutValue(CPrintf(L"Ref%u",    u), m_pSlots->GetAt(u).Ref     );

		Tree.PutValue(CPrintf(L"Ext%u",	   u), m_pSlots->GetAt(u).wExtent );

		Tree.EndCollect();
		}

	Tree.EndObject();
	}

// Model Select Support

void CEmersonRoc3MasterBaseDeviceOptions::InitModel(void)
{
	if( m_Model == NOTHING ) {

		m_Model = devDefault;
		}
	}

UINT CEmersonRoc3MasterBaseDeviceOptions::GetModel(void)
{
	return m_Model;
	}

UINT CEmersonRoc3MasterBaseDeviceOptions::GetModelMask(void)
{
	UINT uModel = m_Model - 1;

	if( uModel < NOTHING ) {

		return 1 << uModel;
		}

	return NOTHING;
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsModel(UINT uDevice)
{
	if( uDevice != devDefault ) {

		return GetModelMask() == uDevice;
		}

	return m_Model == devDefault;
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsFloType(void)
{
	return IsFlo103() || IsFlo107() || IsFlo407() || IsFlo500() || IsRoc300() || IsFlo107wo();
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsRocType(void)
{
	return IsRoc800() || IsRoc800L();
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsFlo103(void)
{
	return IsModel(devFlo103104);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsFlo107(void)
{
	return IsModel(devFlo107);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsFlo107wo(void)
{
	return IsModel(devFlo107wo);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsFlo407(void)
{
	return IsModel(devFlo407);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsFlo500(void)
{
	return IsModel(devFlo500);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsRoc300(void)
{
	return IsModel(devRoc300);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsRoc800(void)
{
	return IsModel(devRoc800);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsRoc800L(void)
{
	return IsModel(devRoc800L);
	}

// Slot Access 

BOOL CEmersonRoc3MasterBaseDeviceOptions::AppendSlot(CSlot Slot)
{
	m_pSlots->Append(Slot);

	return TRUE;
	}

DWORD CEmersonRoc3MasterBaseDeviceOptions::SlotExist(CString Text, CAddress Addr)
{
	UINT uCount = m_pSlots->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CSlot Slot = m_pSlots->GetAt(u);

		if( Text == Slot.Ref && Addr.m_Ref == Slot.dwTarget ) {

			return Slot.dwSlot;
			}
		}

	return 0;
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::FindNextSlot(CAddress &Address)
{
	CSlot Save;

	Save.dwSlot = 0;

	CSlot Addr;

	Addr.dwSlot = Address.m_Ref;

	for( UINT u = 0; u < m_pSlots->GetCount(); u++ ) {

		CSlot Slot = m_pSlots->GetAt(u);

		// cppcheck-suppress uninitStructMember

		if( IsLikeSlot(Slot, Addr) ) {

			UINT uMask = GetMask(Save) + 0xFFF;

			if( ((Save.dwSlot & uMask) < (Slot.dwSlot & uMask)) ) {

				Save = Slot;
				}
			}
		}

	UINT uUpdate = 0;

	if( Save.dwSlot ) {

		CAddress Update;

		Update.m_Ref = Save.dwSlot;

		uUpdate = Update.a.m_Offset + Save.wExtent;

		if( uUpdate > 0xFFFF ) {

			return FALSE;
			}
		}

	UINT uMask =  Address.a.m_Table == tabOpcode ? 0xFFF : 0xFFFF;

	Address.a.m_Offset &= ~uMask;

	Address.a.m_Offset += (uUpdate & uMask);

	return TRUE;
	}

void CEmersonRoc3MasterBaseDeviceOptions::UpdateSlots(CAddress const &Address, INT nSize)
{
	if( nSize == NOTHING || nSize <= 1 ) {

		return;
		}

	if( IsSlot(Address) ) {

		UINT uCount = m_pSlots->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CSlot Slot = m_pSlots->GetAt(u);

			if( Slot.dwSlot == Address.m_Ref ) {

				Slot.wExtent = WORD(nSize);

				m_pSlots->Remove(u);

				m_pSlots->Insert(u, Slot);
				}
			}
		}
	}

DWORD CEmersonRoc3MasterBaseDeviceOptions::FindSlotAddress(CAddress Addr)
{
	if( IsSlot(Addr) ) {

		for( UINT u = 0; u < m_pSlots->GetCount(); u++ ) {

			if( m_pSlots->GetAt(u).dwSlot == Addr.m_Ref ) {

				return m_pSlots->GetAt(u).dwTarget;					
				}
			}
		}

	return Addr.m_Ref;
	}

WORD CEmersonRoc3MasterBaseDeviceOptions::FindExtent(CAddress Addr)
{
	if( IsSlot(Addr) ) {

		for( UINT u = 0; u < m_pSlots->GetCount(); u++ ) {

			if( m_pSlots->GetAt(u).dwSlot == Addr.m_Ref - m_pSlots->GetAt(u).wExtent ) {

				return m_pSlots->GetAt(u).wExtent;					
				}
			}
		}

	return FindLongSize(Addr.a.m_Extra);
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsSlot(CAddress Addr)
{
	return IsDouble(Addr.a.m_Extra) || IsString(Addr.a.m_Extra) || Addr.a.m_Table == tabOpcode;
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsDouble(BYTE bType)
{
	return bType == typDouble;
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsString(BYTE bType)
{
	switch( bType ) {

		case typStr10:
		case typStr12:
		case typStr20:
		case typStr30:
		case typStr40:

			return TRUE;
		}

	return FALSE;
	}

BYTE CEmersonRoc3MasterBaseDeviceOptions::FindLongSize(BYTE bType)
{
	switch( bType ) {

		case typDouble:	return 2;

		case typStr10:	return 3;
		case typStr12:	return 3;
		case typStr20:	return 5;
		case typStr30:	return 8;
		case typStr40:	return 10;
		}

	return 1;
	}

void CEmersonRoc3MasterBaseDeviceOptions::Rebuild(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		m_pSlots->Empty();

		pSystem->Rebuild(1);

		Sort();

		ClearSlots();

		WalkSlots();
		}
	}

void CEmersonRoc3MasterBaseDeviceOptions::Sort(void)
{
	m_pSlots->Sort();
	}


// Download Support

BOOL CEmersonRoc3MasterBaseDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Group));
	Init.AddByte(BYTE(m_Unit));
	Init.AddWord(WORD(m_Poll));
	Init.AddWord(WORD(m_PassNum));
	Init.AddByte(BYTE(m_fUse));
	Init.AddByte(BYTE(m_Acc));
	Init.AddByte(BYTE(m_Increment));
	Init.AddLong(DWORD(m_Ping));
	Init.AddText(m_OpID);
	
	UINT uSlots = m_pSlots->GetCount();

	Init.AddLong(uSlots);

	for( UINT u = 0; u < uSlots; u++ ) {

		Init.AddLong(m_pSlots->GetAt(u).dwSlot);
		Init.AddLong(m_pSlots->GetAt(u).dwTarget);
		Init.AddWord(m_pSlots->GetAt(u).wExtent);
		}
	
	return TRUE;
	}

// Meta Data Creation

void CEmersonRoc3MasterBaseDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Poll);
	Meta_AddInteger(PassNum);
	Meta_AddInteger(Acc);
	Meta_AddString(OpID);
	Meta_AddString(Pass);
	Meta_AddBoolean(Use);
	Meta_AddInteger(Increment);
	Meta_AddInteger(Ping);
	Meta_AddInteger(User);
	Meta_AddInteger(Model);
	}

// Implementation

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsLikeSlot(CSlot s1, CSlot s2)
{
	UINT uMask = GetMask(s1);

	return ((s1.dwSlot & uMask) == (s2.dwSlot & uMask));
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::IsLikeTarget(CSlot s1, CSlot s2)
{
	UINT uMask = GetMask(s1);

	return ((s1.dwTarget & uMask) == (s2.dwTarget & uMask));
	}

BOOL CEmersonRoc3MasterBaseDeviceOptions::GetMask(CSlot Slot)
{
	CAddress Addr;

	Addr.m_Ref = Slot.dwSlot;
	
	return Addr.a.m_Table == tabOpcode ? 0xF0FFF000 : 0xF0FF0000;
	}

void CEmersonRoc3MasterBaseDeviceOptions::ClearSlots(void)
{
	if( m_pSlots->IsEmpty() ) {

		return;
		}

	UINT uCount = m_pSlots->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CSlot s = m_pSlots->GetAt(u);

		CAddress Addr = (CAddress&) s.dwSlot;

		Addr.a.m_Offset = 0;

		s.dwSlot = Addr.m_Ref;

		m_pSlots->SetAt(u, (const CSlot&)s);
		}
	}

void CEmersonRoc3MasterBaseDeviceOptions::WalkSlots(void)
{
	if( m_pSlots->IsEmpty() ) {

		return;
		}

	UINT uCount = m_pSlots->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CSlot s = m_pSlots->GetAt(u);

		if( FindNextSlot((CAddress&) s.dwSlot) ) {

			m_pSlots->SetAt(u, (const CSlot&)s);
			}
		}
	}

void CEmersonRoc3MasterBaseDeviceOptions::PurgeSlots(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		Rebuild();
				
		pSystem->Rebuild(1);
		}
	}

void CEmersonRoc3MasterBaseDeviceOptions::Print(CString Message)
{
/*	AfxTrace(L"\n%s", Message);

	for( UINT u = 0; u < m_pSlots->GetCount(); u++ ) {

		CSlot Slot = m_pSlots->GetAt(u);

		AfxTrace(L"\nSlot %u %8.8x %8.8x %s %u", u, Slot.dwSlot, Slot.dwTarget, Slot.Ref, Slot.wExtent);
		}
*/	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master 3 Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonRoc3MasterDeviceOptions, CEmersonRoc3MasterBaseDeviceOptions);

// Constructor

CEmersonRoc3MasterDeviceOptions::CEmersonRoc3MasterDeviceOptions(void)
{	
	m_Timeout = 1000;
	}

// Destructor

CEmersonRoc3MasterDeviceOptions::~CEmersonRoc3MasterDeviceOptions(void)
{
	}

// Download Support

BOOL CEmersonRoc3MasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CEmersonRoc3MasterBaseDeviceOptions::MakeInitData(Init);
	
	Init.AddWord(WORD(m_Timeout));

	return TRUE;
	}

// UI Loading

void CEmersonRoc3MasterDeviceOptions::LoadUI(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_SETTINGS), 1);

	pView->AddUI(pItem, L"root", L"Ping");

	pView->AddUI(pItem, L"root", L"User");

	pView->AddUI(pItem, L"root", L"Model");

	pView->AddUI(pItem, L"root", L"Increment");

	pView->AddUI(pItem, L"root", L"Timeout");

	pView->EndGroup(TRUE);
	}

// Meta Data Creation

void CEmersonRoc3MasterDeviceOptions::AddMetaData(void)
{
	CEmersonRoc3MasterBaseDeviceOptions::AddMetaData();

	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC TCP Master 3 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonRoc3TCPMasterDeviceOptions, CEmersonRoc3MasterBaseDeviceOptions);

// Constructor

CEmersonRoc3TCPMasterDeviceOptions::CEmersonRoc3TCPMasterDeviceOptions(void)
{
	m_IP1     = DWORD(MAKELONG(MAKEWORD(1, 1), MAKEWORD(168, 192)));

	m_IP2     = 0; 

	m_Port    = 4000;

	m_Keep    = TRUE;

	m_UsePing = TRUE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;
	}

// Destructor

CEmersonRoc3TCPMasterDeviceOptions::~CEmersonRoc3TCPMasterDeviceOptions(void)
{
	}

// Download Support

BOOL CEmersonRoc3TCPMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CEmersonRoc3MasterBaseDeviceOptions::MakeInitData(Init);

	Init.AddLong(LONG(m_IP1));
	Init.AddLong(LONG(m_IP2));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_UsePing));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// UI Management

void CEmersonRoc3TCPMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}

	CEmersonRoc3MasterBaseDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// UI Loading

void CEmersonRoc3TCPMasterDeviceOptions::LoadUI(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_SETTINGS), 1);

	pView->AddUI(pItem, L"root", L"Ping");

	pView->AddUI(pItem, L"root", L"User");

	pView->AddUI(pItem, L"root", L"Model");

	pView->AddUI(pItem, L"root", L"Increment");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_PROTOCOL_OPTIONS), 1);

	pView->AddUI(pItem, L"root", L"IP1");

	pView->AddUI(pItem, L"root", L"IP2");

	pView->AddUI(pItem, L"root", L"Port");

	pView->AddUI(pItem, L"root", L"Keep");

	pView->AddUI(pItem, L"root", L"UsePing");

	pView->AddUI(pItem, L"root", L"Time1");

	pView->AddUI(pItem, L"root", L"Time3");

	pView->AddUI(pItem, L"root", L"Time2");

	pView->EndGroup(TRUE);
	}

// Meta Data Creation

void CEmersonRoc3TCPMasterDeviceOptions::AddMetaData(void)
{
	CEmersonRoc3MasterBaseDeviceOptions::AddMetaData();

	Meta_AddInteger(IP1);
	Meta_AddInteger(IP2);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(UsePing);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Device Options 3	UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CEmersonRoc3MasterBaseDeviceOptionsUIPage, CUIPage);

// Constructor

CEmersonRoc3MasterBaseDeviceOptionsUIPage::CEmersonRoc3MasterBaseDeviceOptionsUIPage(CEmersonRoc3MasterBaseDeviceOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CEmersonRoc3MasterBaseDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_IDENTIFICATION), 1);

	pView->AddUI(pItem, L"root", L"Group");

	pView->AddUI(pItem, L"root", L"Unit");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_SECURITY), 1);

	pView->AddUI(pItem, L"root", L"OpID");

	pView->AddUI(pItem, L"root", L"Pass");

	pView->AddUI(pItem, L"root", L"Use");

	pView->AddUI(pItem, L"root", L"Acc");

	pView->EndGroup(TRUE);

	m_pOption->LoadUI(pView, pItem);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Serical Master Driver 3
//

// Instantiator

ICommsDriver * Create_EmersonRocMasterDriver3(void)
{
	return New CEmersonRocMasterDriver3;
	}

// Constructor

CEmersonRocMasterDriver3::CEmersonRocMasterDriver3(void)
{
	m_wID		= 0x40A6;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Emerson Process";
	
	m_DriverName	= "ROC Protocol Enhanced";
	
	m_Version	= "3.13";
	
	m_ShortName	= "ROC";

	InitTypes();

	AddSpaces();
	}

// Destructor

CEmersonRocMasterDriver3::~CEmersonRocMasterDriver3(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CEmersonRocMasterDriver3::GetBinding(void)
{
	return bindStdSerial;
	}

void CEmersonRocMasterDriver3::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEmersonRocMasterDriver3::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEmersonRoc3MasterDriverOptions);
	}

CLASS CEmersonRocMasterDriver3::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonRoc3MasterDeviceOptions);
	}

// Address Management

BOOL CEmersonRocMasterDriver3::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{	
	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt && !pOpt->m_User ) {

		CEmRocPreDefinedDialog Dlg(*this, Addr, pConfig, fPart);

		return Dlg.Execute(CWnd::FromHandle(hWnd));
		}
	
	CEmRocAddrDialog3 Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CEmersonRocMasterDriver3::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	switch( Addr.a.m_Extra ) {

		case typTime:	
			return TRUE;
		}

	return FALSE;
	}

// Address Helpers

BOOL CEmersonRocMasterDriver3::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CByteArray Array;

	CString Str = Text;

	UINT uFind  = Str.Find(',');

	while( uFind < NOTHING ) {
		
		Array.Append(tstrtol(Str.Mid(0, uFind), NULL, 10) & 0xFF);

		Str = Str.Mid(uFind + 1);

		uFind = Str.Find(',');

		if( uFind == NOTHING ) {

			for( BYTE t = 0; t < m_Types.GetCount(); t++ ) {

				if( Str == m_Types.GetAt(t) ) {

					Array.Append(t);

					break;
					}
				}

			CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

			if( pOpt ) {

				if( Array.GetCount() == 3 ) {

					Addr.a.m_Table  = tabOpcode;

					Addr.a.m_Extra  = Array.GetAt(ocType);

					Addr.a.m_Type   = TranslateType(Addr.a.m_Extra);

					Addr.a.m_Offset = ((Array.GetAt(ocTable) - 1) << 12) | Array.GetAt(ocLocation) - 1; 
					}

				else if( Array.GetCount() == 4 ) {

					Addr.a.m_Table = Array.GetAt(indPointType);

					Addr.a.m_Extra = Array.GetAt(indType);
	
					Addr.a.m_Type  = TranslateType(Addr.a.m_Extra);

					if( pOpt->m_Increment ) {

						Addr.a.m_Offset = (Array.GetAt(indParam  ) << 8) | Array.GetAt(indLogical);
						}
					else {
						Addr.a.m_Offset = (Array.GetAt(indLogical) << 8) | Array.GetAt(indParam  ); 
						}
					}
				else {
					Error.Set(CString(IDS_DRIVER_ADDR_INVALID), 0);

					return FALSE;
					}

				if( pOpt->IsSlot(Addr) ) {

					DWORD dwExist = pOpt->SlotExist(Text, Addr);

					if( dwExist ) {

						Addr.m_Ref =  dwExist;

						return TRUE;
						}

					CSlot Slot;

					Slot.dwTarget	= Addr.m_Ref;

					if( Addr.a.m_Table != tabOpcode ) {

						Addr.a.m_Table  = pOpt->IsDouble(Addr.a.m_Extra) ? tabDouble : tabStr;
						}

					if( pOpt->FindNextSlot(Addr) ) {

						Slot.dwSlot  = Addr.m_Ref;

						Slot.Ref     = Text;

						Slot.wExtent = pOpt->FindLongSize(Addr.a.m_Extra);

						pOpt->AppendSlot(Slot);	

						return TRUE;
						}

					Error.Set(CString(IDS_NO_AVAILABLE_SLOT));

					return FALSE;
					}

				return TRUE;
				}
			}
		}

	Error.Set(CString(IDS_DRIVER_ADDR_INVALID), 0);

	return FALSE;
	}

BOOL CEmersonRocMasterDriver3::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt ) {
		
		pOpt->Sort();

		if( pOpt->IsSlot(Addr) ) {

			UINT uFind = pOpt->FindSlotAddress(Addr);

			if( uFind == Addr.m_Ref ) {

				WORD wExtent  = pOpt->FindExtent(Addr); 

				CAddress Address;

				Address.m_Ref = Addr.m_Ref - wExtent;

				uFind = pOpt->FindSlotAddress(Address);

				if( uFind != Address.m_Ref ) {

					CSlot Slot;

					Slot.dwSlot   = Addr.m_Ref;

					Slot.dwTarget = uFind + wExtent/pOpt->FindLongSize(Addr.a.m_Extra);

					Slot.wExtent  = wExtent;

					pOpt->AppendSlot(Slot);
					}
				}

			if( IsOpcodeTable(Addr) ) {

				Text = CPrintf( "%u,%u,%s",
					
						GetOpTable(pConfig, Addr),
						GetLocation(pConfig, Addr),
						m_Types.GetAt(Addr.a.m_Extra)
						);

				return TRUE;
				}
			}				

		Text = CPrintf( "%u,%u,%u,%s",

				GetPointType(pConfig, Addr),
				GetLogical(pConfig, Addr),
				GetParameter(pConfig, Addr),
				m_Types.GetAt(Addr.a.m_Extra)					     
				);

		return TRUE;
		}
		
	return FALSE;
	}

// Notifications

void CEmersonRocMasterDriver3::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt ) {

		pOpt->UpdateSlots(Addr, nSize);
		}
	}

void CEmersonRocMasterDriver3::NotifyInit(CItem * pConfig)
{
	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->Rebuild();
		}
	}

// Field Access

CStringArray CEmersonRocMasterDriver3::GetTypes(void)
{
	return m_Types;
	}

BYTE CEmersonRocMasterDriver3::GetPointType(CItem * pConfig, CAddress Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt ) {

		Address.m_Ref = pOpt->FindSlotAddress(Addr);
		}

	return Address.a.m_Table;
	}

BYTE CEmersonRocMasterDriver3::GetLogical(CItem * pConfig, CAddress Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt ) {

		Address.m_Ref = pOpt->FindSlotAddress(Addr);

		if( pOpt->m_Increment ) {
			
			return Address.a.m_Offset & 0xFF;
			}
		}

	return BYTE(Address.a.m_Offset >> 8);
	}

BYTE CEmersonRocMasterDriver3::GetParameter(CItem * pConfig, CAddress Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt ) {

		Address.m_Ref = pOpt->FindSlotAddress(Addr);

		if( pOpt->m_Increment ) {
			
			return BYTE(Address.a.m_Offset >> 8);
			}
		}

	return Address.a.m_Offset & 0xFF;
	}

BYTE CEmersonRocMasterDriver3::GetLogicalCount(CItem *pConfig)
{
	CEmersonRoc3MasterDeviceOptions * pDev = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pDev->IsRocType() || !pDev->GetModel() ) {

		return 250;
		}

	return 128;
	}

BYTE CEmersonRocMasterDriver3::GetOpTable(CItem * pConfig, CAddress Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt ) {

		Address.m_Ref = pOpt->FindSlotAddress(Addr);
		}

	return BYTE(Address.a.m_Offset >> 12) + 1;
	}

WORD CEmersonRocMasterDriver3::GetLocation(CItem * pConfig, CAddress Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) pConfig;

	if( pOpt ) {

		Address.m_Ref = pOpt->FindSlotAddress(Addr);
		}

	return (Address.a.m_Offset & 0xFFF) + 1;
	}

BOOL CEmersonRocMasterDriver3::IsOpcodeTable(CAddress Addr)
{
	return Addr.a.m_Table == tabOpcode;
	}

BOOL CEmersonRocMasterDriver3::IsIO(UINT uPoint)
{
	return (uPoint > 0 && uPoint < 6) || uPoint == 56;
	}

CString CEmersonRocMasterDriver3::GetTypeText(UINT uType)
{
	if( uType < m_Types.GetCount() ) {

		return m_Types.GetAt(uType);
		}

	return CString("<type>");
	}

BYTE CEmersonRocMasterDriver3::TranslateType(UINT uType)
{
	switch( uType ) {

		case typUINT8:		return addrByteAsByte;
	
		case typHourMin:
		case typUINT16:		return addrWordAsWord;

		case typFlag:		return addrBitAsBit;

		case typReal:		return addrRealAsReal;
		}
	
	return addrLongAsLong;
	}

// Implementation

void CEmersonRocMasterDriver3::AddSpaces(void)
{								       
	
	}

void CEmersonRocMasterDriver3::InitTypes(void)
{
	// NOTE:  Below text selects should remain consistent with types enumeration.

	m_Types.Empty();

	CString Types = L"UINT8,UINT16,UINT32,Float,Double,AC10,AC12,AC20,AC30,AC40,TLP,BIN,TIME,HOURMI,CurrentTime";
	
	Types.Tokenize(m_Types, ','); 
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC TCP Master Driver 3
//

// Instantiator

ICommsDriver * Create_EmersonRocTCPMasterDriver3(void)
{
	return New CEmersonRocTCPMasterDriver3;
	}

// Constructor

CEmersonRocTCPMasterDriver3::CEmersonRocTCPMasterDriver3(void)
{
	m_wID		= 0x40D4;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Emerson Process";
	
	m_DriverName	= "ROC Protocol Enhanced TCP/IP";
	
	m_Version	= "3.13";
	
	m_ShortName	= "ROC";

	InitTypes();

	AddSpaces();
	}

// Destructor

CEmersonRocTCPMasterDriver3::~CEmersonRocTCPMasterDriver3(void)
{
	}

// Binding Control

UINT CEmersonRocTCPMasterDriver3::GetBinding(void)
{
	return bindEthernet;
	}

void CEmersonRocTCPMasterDriver3::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CEmersonRocTCPMasterDriver3::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEmersonRoc3MasterDriverOptions);
	}

CLASS CEmersonRocTCPMasterDriver3::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonRoc3TCPMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Address Selection Dialog - Numeric Entry - User Defined
//

// Runtime Class

AfxImplementRuntimeClass(CEmRocAddrDialog3, CStdDialog);
		
// Constructor

CEmRocAddrDialog3::CEmRocAddrDialog3(CEmersonRocMasterDriver3 &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;

	for( UINT u = 0; u < elements(m_Range); u++ ) {

		m_Range[u].Empty();
		}

	SetName(TEXT("EmRoc3Dlg"));
	}

// Message Map

AfxMessageMap(CEmRocAddrDialog3, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(2002, BN_CLICKED,	OnButton)

	AfxDispatchNotify(200,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(201,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(202,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(204,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(205,	EN_KILLFOCUS,	OnEditChange)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CEmRocAddrDialog3)
	};

// Message Handlers

BOOL CEmRocAddrDialog3::OnInitDialog(CWnd &Wnd, DWORD dwData)
{
	InitType();

	InitPrompt();

	InitSelect();

	DoEnables();

	return FALSE;
	}

// Notification Handlers

void CEmRocAddrDialog3::OnButton(UINT uID, CWnd &Wnd)
{
	if( uID == 2002 ) {

		DoEnables();
		}
	}

void CEmRocAddrDialog3::OnEditChange(UINT uID, CWnd &Wnd)
{
	if( IsEdit(uID) ) {

		INT  nCheck = tstrtol(GetDlgItem(uID).GetWindowText(), NULL, 10);

		UINT uIndex = uID - 200;

		m_Range[uIndex].Set(GetMin(uID), GetMax(uID));

		if( nCheck >= m_Range[uIndex].m_nFrom && nCheck <= m_Range[uIndex].m_nTo ) {

			m_Range[uIndex].Empty();
			}
		}
	}

// Command Handlers

BOOL CEmRocAddrDialog3::OnOkay(UINT uID)
{
	CEmersonRocMasterDriver3 * pDriver = (CEmersonRocMasterDriver3 *) m_pDriver;

	if( pDriver ) {

		CString TLP;

		CButton & Button = (CButton &)GetDlgItem(2002);

		BOOL fOpTable	 = Button.IsChecked();

		CError Error;

		if( !fOpTable ) {

			for( UINT u = 200; u <= 202; u++ ) {

				if( !CheckRange(u, Error) ) {

					Error.Show(ThisObject);

					GetDlgItem(u).SetFocus();
					
					return TRUE;
					}

				TLP += GetDlgItem(u).GetWindowTextW();

				TLP += ",";
				}
			}
		else {
			for( UINT u = 204; u <= 205; u++ ) {

				if( !CheckRange(u, Error) ) {

					Error.Show(ThisObject);

					GetDlgItem(u).SetFocus();
					
					return TRUE;
					}

				TLP += GetDlgItem(u).GetWindowTextW();

				TLP += ",";
				}
			}

		CComboBox &Box = (CComboBox &)GetDlgItem(!fOpTable ? 203 : 206);

		TLP += CPrintf(L"%s", pDriver->GetTypes().GetAt(Box.GetCurSel()));

		CAddress Addr;

		if( pDriver->ParseAddress(Error, Addr, m_pConfig, TLP) ) {

			m_pAddr->m_Ref = Addr.m_Ref;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CEmRocAddrDialog3::InitType(void)
{
	for( UINT u = 203; u <= 206; u += 3 ) {

		CComboBox &Box = (CComboBox &)GetDlgItem(u); 

		CEmersonRocMasterDriver3 * pDriver = (CEmersonRocMasterDriver3 *) m_pDriver;

		Box.AddStrings(pDriver->GetTypes());

		Box.SetCurSel(m_pAddr ? m_pAddr->a.m_Extra : 3);
		}
	}

void CEmRocAddrDialog3::InitPrompt(void)
{
	for( UINT u = 300; u <= 304; u++ ) {

		GetDlgItem(u).SetWindowTextW(CPrintf(CString(IDS_FMT_TO_FMT), GetMin(u), GetMax(u)));
		}
	}

void CEmRocAddrDialog3::InitSelect(void)
{
	CString Text = "%u";

	if( m_pAddr ) {

		CEmersonRocMasterDriver3 * pDriver = (CEmersonRocMasterDriver3 *) m_pDriver;

		if( pDriver ) {

			BOOL fOpcode = pDriver->IsOpcodeTable(*m_pAddr);

			CButton & Button = (CButton &)GetDlgItem(2002);

			Button.SetCheck(fOpcode);

			GetDlgItem(200).SetWindowTextW(CPrintf(Text, fOpcode ? GetMin(200) : pDriver->GetPointType(m_pConfig, *m_pAddr)));

			GetDlgItem(201).SetWindowTextW(CPrintf(Text, fOpcode ? GetMin(201) : pDriver->GetLogical  (m_pConfig, *m_pAddr)));

			GetDlgItem(202).SetWindowTextW(CPrintf(Text, fOpcode ? GetMin(202) : pDriver->GetParameter(m_pConfig, *m_pAddr)));

			GetDlgItem(204).SetWindowTextW(CPrintf(Text, fOpcode ? pDriver->GetOpTable  (m_pConfig, *m_pAddr) : GetMin(204)));

			GetDlgItem(205).SetWindowTextW(CPrintf(Text, fOpcode ? pDriver->GetLocation (m_pConfig, *m_pAddr) : GetMin(205)));
			
			return;
			}
		}

	for( UINT u = 200; u <= 205; u++ ) {

		if( IsEdit(u) ) {

			GetDlgItem(u).SetWindowTextW(CPrintf(Text, GetMin(u)));
			}
		}
	}
	

void CEmRocAddrDialog3::DoEnables(void)
{
	CButton & Button = (CButton &)GetDlgItem(2002);

	BOOL fEnable = Button.IsChecked();

	UINT u;

	for( u = 200; u <= 203; u++ ) {

		GetDlgItem(u).EnableWindow(!fEnable);
		}

	for( u = 204; u <= 206; u++ ) {

		GetDlgItem(u).EnableWindow(fEnable);
		}

	for( u = 300; u <= 302; u++ ) {

		GetDlgItem(u).EnableWindow(!fEnable);
		}

	for( u = 303; u <= 304; u++ ) {

		GetDlgItem(u).EnableWindow(fEnable);
		}
	}

BYTE CEmRocAddrDialog3::GetMin(UINT uID)
{
	switch( uID ) {

		case 204:
		case 205:
		case 303:
		case 304:	return 1;
		}

	return 0;
	}


BYTE CEmRocAddrDialog3::GetMax(UINT uID)
{
	switch( uID ) {

		case 300:
		case 200:	return 236;

		case 303:
		case 204:	return 16;

		case 304:
		case 205:	return 44;
		}

	return 255;
	}

BOOL CEmRocAddrDialog3::IsEdit(UINT uID)
{
	switch( uID ) {

		case 200:
		case 201:
		case 202:
		case 204:
		case 205:	return TRUE;
		}

	return FALSE;
	}

BOOL CEmRocAddrDialog3::CheckRange(UINT uID, CError &Error)
{
	UINT uIndex = uID - 200;

	if( !m_Range[uIndex].IsEmpty() ) {

		Error.Set(CPrintf("%s must be in the range (%u..%u).", ROC_MSG[uIndex],
								       m_Range[uIndex].m_nFrom,
								       m_Range[uIndex].m_nTo));

		return FALSE;
		}

	return TRUE;
	}

///////////////////////////////////////////////////////////////////////////
//
//  Predefined TLP Support
//
//

//////////////////////////////////////////////////////////////////////////
//
// Point Type Definitions (T)
//

static TLP const m_pPointType[] = {

	{	0,	"0-Configurable Opcode",			"OPC",		devFlo103104	},
	{	1,	"1-Discrete Inputs",				"DIN",		devFlo103104	},
	{	2,	"2-Discrete Outputs",				"DOU",		devFlo103104	},
	{	3,	"3-Analog Inputs",				"AIN",		devFlo103104	},
	{	4,	"4-Analog Outputs",				"AOU",		devFlo103104	},
	{	5,	"5-Pulse Inputs",				"PIN",		devFlo103104	},
	{	6,	"6-PID Parameters",				"PID",		0		},
	{	7,	"7-AGA Flow Parameters",			"AGA",		devFlo103104	},
	{	8,	"8-History Parameters",				"HST",		devFlo103104	},
	{	9,	"9-AGA Switched Run Parameters",		"???",		0		},
	{	10,	"10-AGA Flow Calculation Values",		"FLW",		devFlo103104	},
	{	11,	"11-Tanks",					"???",		0		},
	{	12,	"12-Clock",					"CLK",		devFlo103104	},
	{	13,	"13-Flags",					"FLG",		devFlo103104	},
	{	14,	"14-Comm Ports",				"COM",		0		},  	// Should we support?
	{	15,	"15-System Variables",				"SYS",		devFlo103104	},
	{	16,	"16-FST Registers",				"FST",		devFlo103104	},
	{	17,	"17-Soft Point Parameters",			"SFP",		devFlo103104	},
	{	18,	"",						"???",		0		},	
	{	19,	"19-Database Parameters",			"DBP",		devFlo103104	},
	{	20,	"20-Tasks",					"???",		0		},
	{	21,	"21-Information for User Defined Points 1",	"UDPI",		devFlo103104	},
	{	22,	"22-Information for User Defined Points 2",	"UDPI",		0		},
	{	23,	"23-Information for User Defined Points 3",	"UDPI",		0		},
	{	24,	"24-Information for User Defined Points 4",	"UDPI",		0		},
	{	25,	"25-Information for User Defined Points 5",	"UDPI",		0		},
	{	26,	"26-Information for User Defined Points 6",	"UDPI",		0		},
	{	27,	"27-Information for User Defined Points 7",	"UDPI",		0		},
	{	28,	"28-Information for User Defined Points 8",	"UDPI",		0		},
	{	29,	"29-Information for User Defined Points 9",	"UDPI",		0		},
	{	30,	"30-Information for User Defined Points 10",	"UDPI",		0		},
	{	31,	"31-Information for User Defined Points 11",	"UDPI",		0		},
	{	32,	"32-Information for User Defined Points 12",	"UDPI",		0		},
	{	33,	"33-Information for User Defined Points 13",	"UDPI",		0		},
	{	34,	"34-Information for User Defined Points 14",	"UDPI",		0		},
	{	35,	"35-Information for User Defined Points 15",	"UDPI",		0		},
	{	36,	"36-Information for User Defined Points 16",	"UDPI",		0		},
	{	37,	"37-Information for User Defined Points 17",	"UDPI",		0		},
	{	38,	"38-Information for User Defined Points 18",	"UDPI",		0		},
	{	39,	"39-Information for User Defined Points 19",	"UDPI",		0		},
	{	40,	"40-Mulitple Variable Sensor",			"???",		0		},
	{	41,	"41-Run Parameters",				"RUN",		devFlo103104	},
	{	42,	"42-Extra Run Parameters",			"ERN",		devFlo103104	},
	{	43,	"43-User List Parameters",			"ULP",		devFlo103104	},
	{	44,	"44-Power Control Parameters",			"PWR",		devFlo103104	},
	{	45,	"45-Meter Calibration and Sampler",		"SMP",		devFlo103104	},
	{	46,	"46-Meter Configuration Parameters",		"AGANEW",	devFlo103104	},
	{	47,	"47-Meter Flow Values",				"FLWNEW",	devFlo103104	},
	{	48,	"48-PID Control Parameters",			"PID",		devFlo103104	},
	{	49,	"",						"???",		0		},
	{	50,	"",						"???",		0		},						
	{	51,	"",						"???",		0		},
	{	52,	"52-Battery Parameters",			"???",		0		},
	{	53,	"53-Modbus Configuration Parameters",		"MBPAR",	0		},  	// Should we support?
	{	54,	"54-Modbus Function Tables",			"MBF",		0		},  	// Should we support?
	{	55,	"55-Modbus Special Function Table",		"MBSFT",	0		},  	// Should we support?
	{	56,	"56-Analog Input Calibration Parameters",	"AIC",		devFlo103104	},
	{	57,	"57-Keypad / LogOn Security Parameters",	"???",		0		},
	{	58,	"58-Revision Information",			"REV",		devFlo103104	},
	{	59,	"59-Program Flash Control Parameters",		"???",		0		},
	{	60,	"",						"???",		0		},
	{	61,	"",						"???",		0		},
	{	62,	"",						"???",		0		},
	{	63,	"",						"???",		0		},
	{	64,	"",						"???",		0		},
	{	65,	"",						"???",		0		},
	{	66,	"",						"???",		0		},
	{	67,	"",						"???",		0		},
	{	68,	"",						"???",		0		},
	{	69,	"",						"???",		0		},
	{	70,	"",						"???",		0		},
	{	71,	"71-Cause Configuration",			"UDP71",	0		},
	{	72,	"72-Effect Configuration",			"UDP72",	0		},
	{	73,	"",						"???",		0		},
	{	74,	"",						"???",		0		},
	{	75,	"",						"???",		0		},
	{	76,	"",						"???",		0		},
	{	77,	"",						"???",		0		},
	{	78,	"",						"???",		0		},
	{	79,	"",						"???",		0		},
	{	80,	"",						"???",		0		},
	{	81,	"",						"???",		0		},
	{	82,	"",						"???",		0		},
	{	83,	"",						"???",		0		},
	{	84,	"",						"???",		0		},
	{	85,	"",						"???",		0		},
	{	86,	"86-Extended History Parameters",		"EHST",		devFlo103104	},
	{	87,	"87-Expanded IO Information",			"EIO",		0/*devRoc800*/	},
	{	88,	"",						"???",		0,		},
	{	89,	"",						"???",		0,		},
	{	90,	"",						"???",		0,		},
	{	91,	"91-System Variables",				"SYS",		devRoc800	},
	{	92,	"92-Logon Parameters",				"LOGON",	devRoc800	},
	{	93,	"93-License Key Information",			"LKI",		devRoc800	},
	{	94,	"94-User C Configuration",			"UCC",		devRoc800	},
	{	95,	"95-ROC Comm Ports",				"COM",		devRoc800	},
	{	96,	"96-FST Parameters",				"FST",		devRoc800	},
	{	97,	"97-FST Register Tags",				"FSTT",		devRoc800	},
	{	98,	"98-Soft Point Parameters",			"SFP",		devRoc800	},
	{	99,	"99-Configurable Opcode",			"OPC",		devRoc800	},
	{      100,	"100-Power Control Parmeters",			"PWR",		devRoc800	},
	{      101,	"101-Discrete Inputs",				"DIN",		devRoc800	},   
	{      102,	"102-Discrete Outputs",				"DOU",		devRoc800	},
	{      103,	"103-Analog Inputs",				"AIN",		devRoc800	},
	{      104,	"104-Analog Outputs",				"AOU",		devRoc800	},
	{      105,	"105-Pulse Inputs",				"PIN",		devRoc800	},
	{      106,	"",						"???",		0,		},
	{      107,	"",						"???",		0,		},
	{      108,	"108-Multivariable Sensor",			"MVS",		devRoc800	},
	{      109,	"109-System Analog Inputs",			"SAI",		devRoc800	},
	{      110,	"110-PID Control Parameters",			"PID",		devRoc800	},
	{      111,	"",						"???",		0,		},
	{      112,	"112-Station Parameters",			"???",		0,/*devRoc800*/	},
	{      113,	"113-Orifice Meter Run Configuration",		"OMRC",		devRoc800	},
	{      114,	"114-Orifice Meter Run Values",			"OMRV",		devRoc800	},
	{      115,	"115-Turbine Meter Run Configuration",		"TMRC",		devRoc800	},
	{      116,	"116-Turbine Meter Run Values",			"TMRV",		devRoc800	},
	{      117,	"117-Modbus Configuration Parameters",		"MBPAR",	0,		},	// Should we support?
	{      118,	"118-Modbus Register to TLP Mapping",		"MBMAP",	0,		},	// Should we support?
	{      119,	"119-Modbus Event, Alarm, and History",		"MBEAH",	0,		},	// Should we support?
	{      120,	"120-Mobus Master Modem Configuration",		"MBMOD",	0,		},	// Should we support?
	{      121,	"121-Modbus Master Table",			"MBMAS",	0,		},	// Should we support?
	{      122,	"122-DS800 Configuration",			"DS8",		devRoc800	},
	{      123,	"123-Security Group Configuration",		"GRP",		devRoc800	}, 
	{      124,	"124-History Segment Configuration",		"HSC",		devRoc800	},
	{      125,	"125-History Segment 0 Point Configuration",	"HS0",		devRoc800	},
	{      126,	"",						"???",		0,		},
	{      127,	"",						"???",		0,		},
	{      128,	"",						"???",		0,		},
	{      129,	"",						"???",		0,		},
	{      130,	"",						"???",		0,		},
	{      131,	"",						"???",		0,		},
	{      132,	"",						"???",		0,		},
	{      133,	"",						"???",		0,		},
	{      134,	"",						"???",		0,		},
	{      135,	"",						"???",		0,		},
	{      136,	"136-ROC Clock",				"CLK",		devRoc800	},
	{      137,	"137-Internet Configuration Parameters",	"ICP",		devRoc800	},
	{      138,	"138-User Program Configuration",		"UPGM",		devRoc800	},
	{      139,	"139-Smart IO Module Information",		"SIOM",		devRoc800	},
	{      140,	"140-AC Input Output Module",			"ACIO",		devRoc800	},
	{      200,	"200-Liquid Preferences",			"UDP200",	devRoc800	},
	{      201,	"201-Liquid Products",				"UDP201",	devRoc800	},
	{      202,	"202-Density Interface",			"UDP202",	devRoc800	},
	{      203,	"203-Liquid Station",				"UDP203",	devRoc800	},
	{      204,	"204-Liquid Meters",				"UDP204",	devRoc800	},
	};

//////////////////////////////////////////////////////////////////////////
//
// Logical Number Definitions (L)
//

static TLP const m_pLogical[] = {

	{	1,	"",			"A1"		},
	{	2,	"",			"A2"		},
	{	3,	"",			"A3"		},
	{	4,	"",			"A4"		},
	{	5,	"",			"A5"		},
	{	6,	"",			"A6"		},
	{	7,	"",			"A7"		},
	{	8,	"",			"A8"		},
	{	9,	"",			"A9"		},
	{	10,	"",			"A10"		},
	{	11,	"",			"A11"		},
	{	12,	"",			"A12"		},
	{	13,	"",			"A13"		},
	{	14,	"",			"A14"		},
	{	15,	"",			"A15"		},
	{	16,	"",			"A16"		},
	{	17,	"",			"B1"		},
	{	18,	"",			"B2"		},
	{	19,	"",			"B3"		},
	{	20,	"",			"B4"		},
	{	21,	"",			"B5"		},
	{	22,	"",			"B6"		},
	{	23,	"",			"B7"		},
	{	24,	"",			"B8"		},
	{	25,	"",			"B9"		},
	{	26,	"",			"B10"		},
	{	27,	"",			"B11"		},
	{	28,	"",			"B12"		},
	{	29,	"",			"B13"		},
	{	30,	"",			"B14"		},
	{	31,	"",			"B15"		},
	{	32,	"",			"B16"		},
	{	33,	"",			"C1"		},
	{	34,	"",			"C2"		},
	{	35,	"",			"C3"		},
	{	36,	"",			"C4"		},
	{	37,	"",			"C5"		},
	{	38,	"",			"C6"		},
	{	39,	"",			"C7"		},
	{	40,	"",			"C8"		},
	{	41,	"",			"C9"		},
	{	42,	"",			"C10"		},
	{	43,	"",			"C11"		},
	{	44,	"",			"C12"		},
	{	45,	"",			"C13"		},
	{	46,	"",			"C14"		},
	{	47,	"",			"C15"		},
	{	48,	"",			"C16"		},
	{	49,	"",			"D1"		},
	{	50,	"",			"D2"		},
	{	51,	"",			"D3"		},
	{	52,	"",			"D4"		},
	{	53,	"",			"D5"		},
	{	54,	"",			"D6"		},
	{	55,	"",			"D7"		},
	{	56,	"",			"D8"		},
	{	57,	"",			"D9"		},
	{	58,	"",			"D10"		},
	{	59,	"",			"D11"		},
	{	60,	"",			"D12"		},
	{	61,	"",			"D13"		},
	{	62,	"",			"D14"		},
	{	63,	"",			"D15"		},
	{	64,	"",			"D16"		},
	{	65,	"",			"E1"		},
	{	66,	"",			"E2"		},
	{	67,	"",			"E3"		},
	{	68,	"",			"E4"		},
	{	69,	"",			"E5"		},
	{	70,	"",			"E6"		},
	{	71,	"",			"E7"		},
	{	72,	"",			"E8"		},
	{	73,	"",			"E9"		},
	{	74,	"",			"E10"		},
	{	75,	"",			"E11"		},
	{	76,	"",			"E12"		},
	{	77,	"",			"E13"		},
	{	78,	"",			"E14"		},
	{	79,	"",			"E15"		},
	{	80,	"",			"E16"		},
	{	81,	"",			"F1"		},
	{	82,	"",			"F2"		},
	{	83,	"",			"F3"		},
	{	84,	"",			"F4"		},
	{	85,	"",			"F5"		},
	{	86,	"",			"F6"		},
	{	87,	"",			"F7"		},
	{	88,	"",			"F8"		},
	{	89,	"",			"F9"		},
	{	90,	"",			"F10"		},
	{	91,	"",			"F11"		},
	{	92,	"",			"F12"		},
	{	93,	"",			"F13"		},
	{	94,	"",			"F14"		},
	{	95,	"",			"F15"		},
	{	96,	"",			"F16"		},
	{	97,	"",			"G1"		},
	{	98,	"",			"G2"		},
	{	99,	"",			"G3"		},
	{	100,	"",			"G4"		},
	{	101,	"",			"G5"		},
	{	102,	"",			"G6"		},
	{	103,	"",			"G7"		},
	{	104,	"",			"G8"		},
	{	105,	"",			"G9"		},
	{	106,	"",			"G10"		},
	{	107,	"",			"G11"		},
	{	108,	"",			"G12"		},
	{	109,	"",			"G13"		},
	{	110,	"",			"G14"		},
	{	111,	"",			"G15"		},
	{	112,	"",			"G16"		},
	{	113,	"",			"H1"		},
	{	114,	"",			"H2"		},
	{	115,	"",			"H3"		},
	{	116,	"",			"H4"		},
	{	117,	"",			"H5"		},
	{	118,	"",			"H6"		},
	{	119,	"",			"H7"		},
	{	120,	"",			"H8"		},
	{	121,	"",			"H9"		},
	{	122,	"",			"H10"		},
	{	123,	"",			"H11"		},
	{	124,	"",			"H12"		},
	{	125,	"",			"H13"		},
	{	126,	"",			"H14"		},
	{	127,	"",			"H15"		},
	{	128,	"",			"H16"		},

	{	129,	"",			"I1"		},
	{	130,	"",			"I2"		},
	{	131,	"",			"I3"		},
	{	132,	"",			"I4"		},
	{	133,	"",			"I5"		},
	{	134,	"",			"I6"		},
	{	135,	"",			"I7"		},
	{	136,	"",			"I8"		},
	{	137,	"",			"I9"		},
	{	138,	"",			"I10"		},
	{	139,	"",			"I11"		},
	{	140,	"",			"I12"		},
	{	141,	"",			"I13"		},
	{	142,	"",			"I14"		},
	{	143,	"",			"I15"		},
	{	144,	"",			"I16"		},
	{	145,	"",			"J1"		},
	{	146,	"",			"J2"		},
	{	147,	"",			"J3"		},
	{	148,	"",			"J4"		},
	{	149,	"",			"J5"		},
	{	150,	"",			"J6"		},
	{	151,	"",			"J7"		},
	{	152,	"",			"J8"		},
	{	153,	"",			"J9"		},
	{	154,	"",			"J10"		},
	{	155,	"",			"J11"		},
	{	156,	"",			"J12"		},
	{	157,	"",			"J13"		},
	{	158,	"",			"J14"		},
	{	159,	"",			"J15"		},
	{	160,	"",			"J16"		},
	{	161,	"",			"K1"		},
	{	162,	"",			"K2"		},
	{	163,	"",			"K3"		},
	{	164,	"",			"K4"		},
	{	165,	"",			"K5"		},
	{	166,	"",			"K6"		},
	{	167,	"",			"K7"		},
	{	168,	"",			"K8"		},
	{	169,	"",			"K9"		},
	{	170,	"",			"K10"		},
	{	171,	"",			"K11"		},
	{	172,	"",			"K12"		},
	{	173,	"",			"K13"		},
	{	174,	"",			"K14"		},
	{	175,	"",			"K15"		},
	{	176,	"",			"K16"		},
	{	177,	"",			"L1"		},
	{	178,	"",			"L2"		},
	{	179,	"",			"L3"		},
	{	180,	"",			"L4"		},
	{	181,	"",			"L5"		},
	{	182,	"",			"L6"		},
	{	183,	"",			"L7"		},
	{	184,	"",			"L8"		},
	{	185,	"",			"L9"		},
	{	186,	"",			"L10"		},
	{	187,	"",			"L11"		},
	{	188,	"",			"L12"		},
	{	189,	"",			"L13"		},
	{	190,	"",			"L14"		},
	{	191,	"",			"L15"		},
	{	192,	"",			"L16"		},
	{	193,	"",			"M1"		},
	{	194,	"",			"M2"		},
	{	195,	"",			"M3"		},
	{	196,	"",			"M4"		},
	{	197,	"",			"M5"		},
	{	198,	"",			"M6"		},
	{	199,	"",			"M7"		},
	{	200,	"",			"M8"		},
	{	201,	"",			"M9"		},
	{	202,	"",			"M10"		},
	{	203,	"",			"M11"		},
	{	204,	"",			"M12"		},
	{	205,	"",			"M13"		},
	{	206,	"",			"M14"		},
	{	207,	"",			"M15"		},
	{	208,	"",			"M16"		},
	{	209,	"",			"N1"		},
	{	210,	"",			"N2"		},
	{	211,	"",			"N3"		},
	{	212,	"",			"N4"		},
	{	213,	"",			"N5"		},
	{	214,	"",			"N6"		},
	{	215,	"",			"N7"		},
	{	216,	"",			"N8"		},
	{	217,	"",			"N9"		},
	{	218,	"",			"N10"		},
	{	219,	"",			"N11"		},
	{	220,	"",			"N12"		},
	{	221,	"",			"N13"		},
	{	222,	"",			"N14"		},
	{	223,	"",			"N15"		},
	{	224,	"",			"N16"		},
	{	225,	"",			"O1"		},
	{	226,	"",			"O2"		},
	{	227,	"",			"O3"		},
	{	228,	"",			"O4"		},
	{	229,	"",			"O5"		},
	{	230,	"",			"O6"		},
	{	231,	"",			"O7"		},
	{	232,	"",			"O8"		},
	{	233,	"",			"O9"		},
	{	234,	"",			"O10"		},
	{	235,	"",			"O11"		},
	{	236,	"",			"O12"		},
	{	237,	"",			"O13"		},
	{	238,	"",			"O14"		},
	{	239,	"",			"O15"		},
	{	240,	"",			"O16"		},
	{	241,	"",			"P1"		},
	{	242,	"",			"P2"		},
	{	243,	"",			"P3"		},
	{	244,	"",			"P4"		},
	{	245,	"",			"P5"		},
	{	246,	"",			"P6"		},
	{	247,	"",			"P7"		},
	{	248,	"",			"P8"		},
	{	249,	"",			"P9"		},
	{	250,	"",			"P10"		},
	{	123,	"",			"P11"		},
	{	124,	"",			"P12"		},
	{	125,	"",			"P13"		},
	{	126,	"",			"P14"		},
	{	127,	"",			"P15"		},
	{	128,	"",			"P16"		},

	};

//////////////////////////////////////////////////////////////////////////
//
// Default Parameter Definitions (P)
//

// NOTE :  Disabled double parameters were not available in UUT.

static PARAMETERS const m_pParameters[] = {

	0,	// Configurable Opcodes
	{
		{	{	0,	"0-Sequence/Revision Number",			"SEQREV"	},	typReal,	0},
		{	{	1,	"1-Data #1",					"DATA1"		},	typTLP,	0},
		{	{	2,	"2-Data #2",					"DATA2"		},	typTLP,	0},
		{	{	3,	"3-Data #3",					"DATA3"		},	typTLP,	0},
		{	{	4,	"4-Data #4",					"DATA4"		},	typTLP,	0},
		{	{	5,	"5-Data #5",					"DATA5"		},	typTLP,	0},
		{	{	6,	"6-Data #6",					"DATA6"		},	typTLP,	0},
		{	{	7,	"7-Data #7",					"DATA7"		},	typTLP,	0},
		{	{	8,	"8-Data #8",					"DATA8"		},	typTLP,	0},
		{	{	9,	"9-Data #9",					"DATA9"		},	typTLP,	0},
		{	{	10,	"10-Data #10",					"DATA10"	},	typTLP,	0},
		{	{	11,	"11-Data #11",					"DATA11"	},	typTLP,	0},
		{	{	12,	"12-Data #12",					"DATA12"	},	typTLP,	0},
		{	{	13,	"13-Data #13",					"DATA13"	},	typTLP,	0},
		{	{	14,	"14-Data #14",					"DATA14"	},	typTLP,	0},
		{	{	15,	"15-Data #15",					"DATA15"	},	typTLP,	0},
		{	{	16,	"16-Data #16",					"DATA16"	},	typTLP,	0},
		{	{	17,	"17-Data #17",					"DATA17"	},	typTLP,	0},
		{	{	18,	"18-Data #18",					"DATA18"	},	typTLP,	0},
		{	{	19,	"19-Data #19",					"DATA19"	},	typTLP,	0},
		{	{	20,	"20-Data #20",					"DATA20"	},	typTLP,	0},
		{	{	21,	"21-Data #21",					"DATA21"	},	typTLP,	0},
		{	{	22,	"22-Data #22",					"DATA22"	},	typTLP,	0},
		{	{	23,	"23-Data #23",					"DATA23"	},	typTLP,	0},
		{	{	24,	"24-Data #24",					"DATA24"	},	typTLP,	0},
		{	{	25,	"25-Data #25",					"DATA25"	},	typTLP,	0},
		{	{	26,	"26-Data #26",					"DATA26"	},	typTLP,	0},
		{	{	27,	"27-Data #27",					"DATA27"	},	typTLP,	0},
		{	{	28,	"28-Data #28",					"DATA28"	},	typTLP,	0},
		{	{	29,	"29-Data #29",					"DATA29"	},	typTLP,	0},
		{	{	30,	"30-Data #30",					"DATA30"	},	typTLP,	0},
		{	{	31,	"31-Data #31",					"DATA31"	},	typTLP,	0},
		{	{	32,	"32-Data #32",					"DATA32"	},	typTLP,	0},
		{	{	33,	"33-Data #33",					"DATA33"	},	typTLP,	0},
		{	{	34,	"34-Data #34",					"DATA34"	},	typTLP,	0},
		{	{	35,	"35-Data #35",					"DATA35"	},	typTLP,	0},
		{	{	36,	"36-Data #36",					"DATA36"	},	typTLP,	0},
		{	{	37,	"37-Data #37",					"DATA37"	},	typTLP,	0},
		{	{	38,	"38-Data #38",					"DATA38"	},	typTLP,	0},
		{	{	39,	"39-Data #39",					"DATA39"	},	typTLP,	0},
		{	{	40,	"40-Data #40",					"DATA40"	},	typTLP,	0},
		{	{	41,	"41-Data #41",					"DATA41"	},	typTLP,	0},
		{	{	42,	"42-Data #42",					"DATA42"	},	typTLP,	0},
		{	{	43,	"43-Data #43",					"DATA43"	},	typTLP,	0},
		{	{	44,	"44-Data #44",					"DATA44"	},	typTLP,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	1,	// Discrete Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Filter",			"FILTER"	},	typUINT8,	0},	
		{	{	2,	"2-Status",			"STATUS"	},	typUINT8,	0},
		{	{	3,	"3-Mode",			"MODE"		},	typUINT8,	0},
		{	{	4,	"4-Alarm Code",			"ALARM"		},	typUINT8,	1},
		{	{	5,	"5-Accumulated Value",		"ACCUM"		},	typUINT32,	0},
		{	{	6,	"6-On Counter",			"ONCTR"		},	typUINT32,	0},	
		{	{	7,	"7-Off Counter",		"OFFCTR"	},	typUINT32,	0},
		{	{	8,	"8-0% Pulse Width",		"MINCNT"	},	typUINT16,	0},
		{	{	9,	"9-100% Pulse Width",		"MAXCNT"	},	typUINT16,	0},
		{	{	10,	"10-Max Time / Max Count",	"SAMTIM"	},	typUINT16,	0},
		{	{	11,	"11-Units",			"UNITS"		},	typStr10,	0},
		{	{	12,	"12-Scan Period",		"SCANPR"	},	typUINT16,	0},
		{	{	13,	"13-Low Reading EU",		"MINEU"		},	typReal,	0},	
		{	{	14,	"14-High Reading EU",		"MAXEU"		},	typReal,	0},
		{	{	15,	"15-Low Alarm EU",		"LOAL"		},	typReal,	0},
		{	{	16,	"16-High Alarm EU",		"HIAL"		},	typReal,	0},
		{	{	17,     "17-Low Low Alarm EU",		"LOLOAL"	},	typReal,	0},
		{	{	18,	"18-High High Alarm EU",	"HIHIAL"	},	typReal,	0},
		{	{	19,	"19-Rate Alarm EU",		"RATEAL"	},	typReal,	0},	
		{	{	20,	"20-Alarm Deadband",		"ALDBND"	},	typReal,	0},
		{	{	21,	"21-EU Value",			"EU"		},	typReal,	0},
		{	{	22,	"22-TDI Value",			"TDICNT"	},	typUINT16,	1},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	2,	// Discrete Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Time On",			"TIMEON"	},	typUINT16,	0},
		{	{	2,	""				""		},	paramNone,	0},
		{	{	3,	"3-Status",			"STATUS"	},	typUINT8,	0},
		{	{	4,	"4-Mode",			"MODE"		},	typUINT8,	0},
		{	{	5,	"5-Alarm Code",			"ALARM"		},	typUINT8,	1},
		{	{	6,	"6-Accumulated Value",		"ACCUM"		},	typUINT32,	0},
		{	{	7,	"7-Units",			"UNITS"		},	typStr10,	0},
		{	{	8,	"8-Cycle Time",			"CYCTIM"	},	typUINT16,	0},
		{	{	9,	"9-0% Count",			"MINCNT"	},	typUINT16,	0},
		{	{	10,	"10-100% Count",		"MAXCNT"	},	typUINT16,	0},
		{	{	11,	"11-Low Reading EU",		"MINEU"		},	typReal,	0},
		{	{	12,	"12-High Reading EU",		"MAXEU"		},	typReal,	0},
		{	{	13,	"13-EU Value",			"EU"		},	typReal,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	3,	// Analog Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Units",			"UNITS"		},	typStr10,	0},	
		{	{	2,	"2-Scan Period",		"SCANPR"	},	typUINT16,	0},
		{	{	3,	"3-Filter",			"FILTER"	},	typUINT16,	0},
		{	{	4,	"4-Adjusted A/D 0%",		"MINRAW"	},	typUINT16,	0},
		{	{	5,	"5-Adjusted A/D 100%",		"MAXRAW"	},	typUINT16,	0},
		{	{	6,	"6-Low Reading EU",		"MINEU"		},	typReal,	0},	
		{	{	7,	"7-High Reading EU",		"MAXEU"		},	typReal,	0},
		{	{	8,	"8-Low Alarm EU",		"LOAL"		},	typReal,	0},
		{	{	9,	"9-High Alarm EU",		"HIAL"		},	typReal,	0},
		{	{	10,     "10-Low Low Alarm EU",		"LOLOAL"	},	typReal,	0},
		{	{	11,	"11-High High Alarm EU",	"HIHIAL"	},	typReal,	0},
		{	{	12,	"12-Rate Alarm EU",		"RATEAL"	},	typReal,	0},	
		{	{	13,	"13-Alarm Deadband",		"ALDBND"	},	typReal,	0},
		{	{	14,	"14-Filtered EU Value",		"EU"		},	typReal,	0},
		{	{	15,	"15-Mode",			"MODE"		},	typUINT8,	0},
		{	{	16,	"16-Alarm Code",		"ALARM"		},	typUINT8,	1},
		{	{	17,	"17-Raw A/D Input",		"CURRAW"	},	typUINT16,	1},
		{	{	18,	"18-Actual Scan Time",		"SCAN"		},	typUINT16,	1},
		{	{	19,	"19-Fault Value",		"FAULTVAL"	},	typReal,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	4,	// Analog Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Units",			"UNITS"		},	typStr10,	0},	
		{	{	2,	"2-Adjusted A/D 0%",		"MINRAW"	},	typUINT16,	0},
		{	{	3,	"3-Adjusted A/D 100%",		"MAXRAW"	},	typUINT16,	0},
		{	{	4,	"4-Low Reading EU",		"MINEU"		},	typReal,	0},	
		{	{	5,	"5-High Reading EU",		"MAXEU"		},	typReal,	0},
		{	{	6,	"6-EU Value",			"EU"		},	typReal,	0},
		{	{	7,	"7-Mode",			"MODE"		},	typUINT8,	0},
		{	{	8,	"8-Alarm Code",			"ALARM"		},	typUINT8,	1},
		{	{	9,	"9-Raw A/D Input",		"CURRAW"	},	typUINT16,	1},
		{	{	0,	"",				""		},	paramNone,	0},
		},
	5,	// Pulse Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Units",			"UNITS"		},	typStr10,	0},	
		{	{	2,	"2-Rate Flag",			"RATEFL"	},	typUINT8,	0},
		{	{	3,	"3-Rate Period",		"RATEPR"	},	typUINT8,	0},
		{	{	4,	"4-Filter Time",		"FILTER"	},	typUINT8,	0},
		{	{	5,	"5-Scan Period",		"SCANPR"	},	typUINT16,	0},
		{	{	6,	"6-Conversion",			"CONV"		},	typReal,	0},	
		{	{	7,	"7-Low Alarm EU",		"LOAL"		},	typReal,	0},
		{	{	8,	"8-High Alarm EU",		"HIAL"		},	typReal,	0},
		{	{	9,	"9-Low Low Alarm EU",		"LOLOAL"	},	typReal,	0},
		{	{	10,     "10-High High Alarm EU",	"HIHIAL"	},	typReal,	0},
		{	{	11,	"11-Rate Alarm EU",		"RATEAL"	},	typReal,	0},
		{	{	12,	"12-Alarm Deadband",		"ALDBND"	},	typReal,	0},	
		{	{	13,	"13-EU Value",			"EU"		},	typReal,	0},
		{	{	14,	"14-Mode",			"MODE"		},	typUINT8,	0},
		{	{	15,	"15-Alarm Code",		"ALARM"		},	typUINT8,	1},
		{	{	16,	"16-Accumulated Value",		"ACCUM"		},	typUINT32,	0},
		{	{	17,	"17-Current Rate",		"RATE"		},	typReal,	1},
		{	{	18,	"18-Today's Total",		"TDYTOT"	},	typReal,	0},
		{	{	19,	"19-Yesterday's Total",		"YDYTOT"	},	typReal,	1},
		{	{	20,	"20-Pulses for Day",		"TDYRAW"	},	typUINT32,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	7,	// AGA Flow Parameters
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	typStr10,	0},
		{	{	1,	"1-Latitude",					"LAT"		},	typReal,	0},	
		{	{	2,	"2-Elevation",					"ELAVTN"	},	typReal,	0},
		{	{	3,	"3-Calculation Method",				"METHOD"	},	typUINT8,	0},
		{	{	4,	"4-AGA Configuration",				"OPTION"	},	typUINT8,	0},
		{	{	5,	"5-Specific Gravity",				"SPGR"		},	typReal,	0},
		{	{	6,	"6-Heating Value",				"GASHV"		},	typReal,	0},	
		{	{	7,	"7-Gravity Acceleration Correction",		"GRAVIT"	},	typReal,	0},
		{	{	8,	"8-Scan Period",				"SCANPR"	},	typUINT16,	1},
		{	{	9,	"9-Pipe Diameter",				"PIPDIA"	},	typReal,	0},
		{	{	10,     "10-Orifice Diameter",				"ORFDIA"	},	typReal,	0},
		{	{	11,	"11-Orifice Reference Temperature",		"TMEAS"		},	typReal,	0},
		{	{	12,	"12-Orifice Material",				"OR_MAT"	},	typUINT8,	0},	
		{	{	13,	"13-Point Description",				"DESC"		},	typStr30,	0},
		{	{	14,	"14-Alarm Code",				"ALARM"		},	typUINT8,	1},
		{	{	15,	"15-Low Alarm",					"LOAL"		},	typReal,	0},
		{	{	16,	"16-High Alarm",				"HIAL"		},	typReal,	0},
		{	{	17,	"17-Viscosity",					"VISCOS"	},	typReal,	0},
		{	{	18,	"18-Specific Heat Ratio",			"SPHTRA"	},	typReal,	0},
		{	{	19,	"19-Contract or Base Pressure",			"BASEPR"	},	typReal,	0},
		{	{	20,	"20-Contract or Base Temperature",		"BASETP"	},	typReal,	0},
		{	{	21,	"21-Low DB Cutoff / Meter Factor",		"MINDP"		},	typReal,	0},
		{	{	22,	"22-User Correction Factor",			"FPWL"		},	typReal,	0},	
		{	{	23,	"23-N2 - Nitrogen",				"NITROG"	},	typReal,	0},
		{	{	24,	"24-CO2 - Carbon Dioxide",			"CARBDI"	},	typReal,	0},
		{	{	25,	"25-H2S - Hydrogen Sulfide",			"HYDSUL"	},	typReal,	0},
		{	{	26,	"26-H2O - Water",				"WATER"		},	typReal,	0},
		{	{	27,	"27-He - Helium",				"HELIUM"	},	typReal,	0},
		{	{	28,	"28-CH4 - Methane",				"METHANE"	},	typReal,	0},
		{	{	29,	"29-C2HS - Ethane",				"ETHANE"	},	typReal,	0},
		{	{	30,	"30-C3H8 - Propane",				"PROPAN"	},	typReal,	0},
		{	{	31,	"31-C4H10 - n-Butane",				"NBUTAN"	},	typReal,	0},
		{	{	32,	"32-C4H10 - i-Butane",				"IBUTAN"	},	typReal,	0},	
		{	{	33,	"33-C5H12 - n-Pentane",				"NPENTA"	},	typReal,	0},
		{	{	34,	"34-C5H12 - i-Pentane",				"IPENTA"	},	typReal,	0},
		{	{	35,	"35-C6H14 - n-Hexane",				"NHEXAN"	},	typReal,	0},
		{	{	36,	"36-C7H16 - n-Heptane",				"NHEPTA"	},	typReal,	0},
		{	{	37,	"37-C8H18 - n-Octane",				"NOCTAN"	},	typReal,	0},
		{	{	38,	"38-C9H20 - n-Nonane",				"NNONAN"	},	typReal,	0},
		{	{	39,	"39-C10H22 - n_Decane",				"NDECAN"	},	typReal,	0},
		{	{	40,	"40-O2 - Oxygen",				"OXYGEN"	},	typReal,	0},
		{	{	41,	"41-CO - Carbon Monoxide",			"CARBMO"	},	typReal,	0},
		{	{	42,	"42-H2 - Hydrogen",				"HYDROG"	},	typReal,	0},	
		{	{	43,	"43-Flow Units",				"FLOUNITS"	},	typUINT8,	0},
		{	{	44,	"44-Stacked Differential Pressure",		"DPSTEN"	},	typUINT8,	0},
		{	{	45,	"45-Low Differential Pressure Input",		"LO_TYP"	},	typTLP,	0},
		{	{	46,	"46-Meter Input",				"DP_TYP"	},	typTLP,	0},
		{	{	47,	"47-Static Pressure Input",			"FP_TYP"	},	typTLP,	0},
		{	{	48,	"48-Temperature Input",				"TP_TYP"	},	typTLP,	0},
		{	{	49,	"49-Low Differential Pressure Setpoint",	"LODPSP"	},	typReal,	0},
		{	{	50,	"50-High Differential Pressure Setpoint",	"HIDPSP"	},	typReal,	0},
		{	{	51,	"51-Meter Value",				"CURDP"		},	typReal,	0},
		{	{	52,	"52-Static Pressure",				"CURFP"		},	typReal,	0},	
		{	{	53,	"53-Temperature",				"CURTMP"	},	typReal,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	8,	// History Parameters
	{
		{	{	0,	"0-Point Tag Identification TLP #1",	"TAG#1"		},	typTLP,	1},
		{	{	1,	"1-History Log Point #1",		"HST#1"		},	typTLP,	1},	
		{	{	2,	"2-Archive Type #1",			"ARCH#1"	},	typUINT8,	1},
		{	{	3,	"3-Averaging or Rate Type #1",		"AVG#1"		},	typUINT8,	1},
		
		{	{	4,	"4-Point Tag Identification TLP #2",	"TAG#2"		},	typTLP,	1},
		{	{	5,	"5-History Log Point #2",		"HST#2"		},	typTLP,	1},	
		{	{	6,	"6-Archive Type #2",			"ARCH#2"	},	typUINT8,	1},
		{	{	7,	"7-Averaging or Rate Type #2",		"AVG#2"		},	typUINT8,	1},

		{	{	8,	"8-Point Tag Identification TLP #3",	"TAG#3"		},	typTLP,	1},
		{	{	9,	"9-History Log Point #3",		"HST#3"		},	typTLP,	1},	
		{	{	10,	"10-Archive Type #3",			"ARCH#3"	},	typUINT8,	1},
		{	{	11,	"11-Averaging or Rate Type #3",		"AVG#3"		},	typUINT8,	1},

		{	{	12,	"12-Point Tag Identification TLP #4",	"TAG#4"		},	typTLP,	1},
		{	{	13,	"13-History Log Point #4",		"HST#4"		},	typTLP,	1},	
		{	{	14,	"14-Archive Type #4",			"ARCH#4"	},	typUINT8,	1},
		{	{	15,	"15-Averaging or Rate Type #4",		"AVG#4"		},	typUINT8,	1},

		{	{	16,	"16-Point Tag Identification TLP #5",	"TAG#5"		},	typTLP,	1},
		{	{	17,	"17-History Log Point #5",		"HST#5"		},	typTLP,	1},	
		{	{	18,	"18-Archive Type #5",			"ARCH#5"	},	typUINT8,	1},
		{	{	19,	"19-Averaging or Rate Type #5",		"AVG#5"		},	typUINT8,	1},

		{	{	20,	"20-Point Tag Identification TLP #6",	"TAG#6"		},	typTLP,	1},
		{	{	21,	"21-History Log Point #6",		"HST#6"		},	typTLP,	1},	
		{	{	22,	"22-Archive Type #6",			"ARCH#6"	},	typUINT8,	1},
		{	{	23,	"23-Averaging or Rate Type #6",		"AVG#6"		},	typUINT8,	1},

		{	{	24,	"24-Point Tag Identification TLP #7",	"TAG#7"		},	typTLP,	1},
		{	{	25,	"25-History Log Point #7",		"HST#7"		},	typTLP,	1},	
		{	{	26,	"26-Archive Type #7",			"ARCH#7"	},	typUINT8,	1},
		{	{	27,	"27-Averaging or Rate Type #7",		"AVG#7"		},	typUINT8,	1},

		{	{	28,	"28-Point Tag Identification TLP #8",	"TAG#8"		},	typTLP,	1},
		{	{	29,	"29-History Log Point #8",		"HST#8"		},	typTLP,	1},	
		{	{	30,	"30-Archive Type #8",			"ARCH#8"	},	typUINT8,	1},
		{	{	31,	"31-Averaging or Rate Type #8",		"AVG#8"		},	typUINT8,	1},

		{	{	32,	"32-Point Tag Identification TLP #9",	"TAG#9"		},	typTLP,	1},
		{	{	33,	"33-History Log Point #9",		"HST#9"		},	typTLP,	0},	
		{	{	34,	"34-Archive Type #9",			"ARCH#9"	},	typUINT8,	0},
		{	{	35,	"35-Averaging or Rate Type #9",		"AVG#9"		},	typUINT8,	0},

		{	{	36,	"36-Point Tag Identification TLP #10",	"TAG#10"	},	typTLP,	1},
		{	{	37,	"37-History Log Point #10",		"HST#10"	},	typTLP,	0},	
		{	{	38,	"38-Archive Type #10",			"ARCH#10"	},	typUINT8,	0},
		{	{	39,	"39-Averaging or Rate Type #10",	"AVG#10"	},	typUINT8,	0},

		{	{	40,	"40-Point Tag Identification TLP #11",	"TAG#11"	},	typTLP,	1},
		{	{	41,	"41-History Log Point #11",		"HST#11"	},	typTLP,	0},	
		{	{	42,	"42-Archive Type #11",			"ARCH#11"	},	typUINT8,	0},
		{	{	43,	"43-Averaging or Rate Type #11",	"AVG#11"	},	typUINT8,	0},

		{	{	44,	"44-Point Tag Identification TLP #12",	"TAG#12"	},	typTLP,	1},
		{	{	45,	"45-History Log Point #12",		"HST#12"	},	typTLP,	0},	
		{	{	46,	"46-Archive Type #12",			"ARCH#12"	},	typUINT8,	0},
		{	{	47,	"47-Averaging or Rate Type #12",	"AVG#12"	},	typUINT8,	0},

		{	{	48,	"48-Point Tag Identification TLP #13",	"TAG#13"	},	typTLP,	1},
		{	{	49,	"49-History Log Point #13",		"HST#13"	},	typTLP,	0},	
		{	{	50,	"50-Archive Type #13",			"ARCH#13"	},	typUINT8,	0},
		{	{	51,	"51-Averaging or Rate Type #13",	"AVG#13"	},	typUINT8,	0},

		{	{	52,	"52-Point Tag Identification TLP #14",	"TAG#14"	},	typTLP,	1},
		{	{	53,	"53-History Log Point #14",		"HST#14"	},	typTLP,	0},	
		{	{	54,	"54-Archive Type #14",			"ARCH#14"	},	typUINT8,	0},
		{	{	55,	"55-Averaging or Rate Type #14",	"AVG#14"	},	typUINT8,	0},

		{	{	56,	"56-Point Tag Identification TLP #15",	"TAG#15"	},	typTLP,	1},
		{	{	57,	"57-History Log Point #15",		"HST#15"	},	typTLP,	0},	
		{	{	58,	"58-Archive Type #15",			"ARCH#15"	},	typUINT8,	0},
		{	{	59,	"59-Averaging or Rate Type #15",	"AVG#15"	},	typUINT8,	0},
		{	{	 0,	"",					""		},	paramNone,	0},
		},
	10,	// AGA Flow Calculation Values
	{
		{	{	0,	"0-Differential Pressure / Uncorrected Flow",	"CURDP"		},	typReal,	1},
		{	{	1,	"1-Static Pressure",				"CURFP"		},	typReal,	1},	
		{	{	2,	"2-Temperature",				"CURTMP"	},	typReal,	1},
		{	{	3,	"3-Instantaneous Flow",				"CURFLO"	},	typReal,	1},
		{	{	4,	"4-Instantaneous Energy",			"ENERGY"	},	typReal,	1},
		{	{	5,	"5-Flow Today",					"TDYFLO"	},	typReal,	1},
		{	{	6,	"6-Energy Today",				"TDYENG"	},	typReal,	1},	
		{	{	7,	"7-Flow Yesterday",				"YDYFLO"	},	typReal,	1},
		{	{	8,	"8-Energy Yesterday",				"YDYENG"	},	typReal,	1},
		{	{	9,	"9-hwPf / Uncorrected Flow Rate",		"HWPF"		},	typReal,	1},
		{	{	10,     "10-IMV / BMV",					"IMV/BMV"	},	typReal,	1},
		{	{	11,	"11-Sample Time",				"SAMPLE"	},	typReal,	1},
		{	{	12,	"12-Y / Fpm",					"EXPFTR"	},	typReal,	1},	
		{	{	13,	"13-Reynolds Number",				"FR"		},	typReal,	1},
		{	{	14,	"14-Ftf / Ftm",					"FTF"		},	typReal,	1},
		{	{	15,	"15-Fpv / S",					"FPV"		},	typReal,	1},
		{	{	16,	"16-Fgr",					"FGR"		},	typReal,	1},
		{	{	17,	"17-Cd / Ftm",					"FB"		},	typReal,	1},
		{	{	18,	"18-Fpb",					"FPB"		},	typReal,	1},
		{	{	19,	"19-Ftb",					"FTB"		},	typReal,	1},
		{	{	20,	"20-Ev",					"FA"		},	typReal,	1},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	12,	// Clock
	{
		{	{	0,	"0-Seconds",					"SECOND"	},	typUINT8,	0},
		{	{	1,	"1-Minutes",					"MINUTE"	},	typUINT8,	0},	
		{	{	2,	"2-Hours",					"HOUR"		},	typUINT8,	0},
		{	{	3,	"3-Day",					"DAY"		},	typUINT8,	0},
		{	{	4,	"4-Month",					"MONTH"		},	typUINT8,	0},
		{	{	5,	"5-Year",					"YEAR"		},	typUINT8,	0},
		{	{	6,	"6-Leap Year",					"LEAPYR"	},	typUINT8,	1},	
		{	{	7,	"7-Day of Week",				"DAYOWK"	},	typUINT8,	1},
		{	{	8,	"8-Time",					"TIME"		},	typUINT32,	0},
		{	{	9,	"9-Century",					"CENT"		},	typUINT8,	0},
		{	{	10,     "10-Daylight Savings Enable",			"DLSTEN"	},	typUINT8,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	13,	// Flags
	{
		{	{	0,	"0-CRC Check",					"CRCCHK"	},	typUINT8,	0},
		{	{	1,	"1-Power Mode",					"PWRMODE"	},	typUINT8,	0},	
		{	{	2,	"2-User Calc 2 Enable",				"CALC2EN"	},	typUINT8,	0},
		{	{	3,	"3-User Operator Port Enable",			"USRLOI"	},	typUINT8,	0},
		{	{	4,	"4-FST/Display Clear",				"FSTDSP"	},	typUINT8,	0},
		{	{	5,	"5-User Com1 Enable",				"USRCOM1"	},	typUINT8,	0},
		{	{	6,	"6-User Com2 Enable",				"USRCOM2"	},	typUINT8,	0},	
		{	{	7,	"7-User Calc 1 Enable",				"USRCALC1"	},	typUINT8,	0},
		{	{	8,	"8-RTS LOI Port",				"RTSLOI"	},	typUINT8,	0},
		{	{	9,	"9-RTS Comm Port 1",				"RTSCOM1"	},	typUINT8,	0},
		{	{	10,     "10-RTS Comm Port 2",				"RTSCOM2"	},	typUINT8,	0},
		{	{	11,	"11-Clear Config Memory",			"CLREEP"	},	typUINT8,	0},	
		{	{	12,	"12-I/O Scan Enable",				"IOSCAN"	},	typUINT8,	0},
		{	{	13,	"13-Auxilary Output 2 On",			"AUX2ON"	},	typUINT8,	0},
		{	{	14,	"14-Auxilary Output 1 On",			"AUX1ON"	},	typUINT8,	0},
		{	{	15,	"15-Cold Start",				"COLDST"	},	typUINT8,	0},
		{	{	16,	"16-Warm Start",				"WARMST"	},	typUINT8,	0},	
		{	{	17,	"17-Read I/O",					"READIIO"	},	typUINT8,	0},
		{	{	18,	"18-Write to Config Memory",			"WRITE"		},	typUINT8,	0},
		{	{	19,	"19-Config Memory Write Complete",		"COMPLT"	},	typUINT8,	0},
		{	{	20,     "20-Event Log Flag",				"EVTFLAG"	},	typUINT8,	0},
		{	{	21,	"21-LOI Port Security",				"LOISEC"	},	typUINT8,	0},	
		{	{	22,	"22-Comm Port 1 Security",			"COM1SEC"	},	typUINT8,	0},
		{	{	23,	"23-Comm Port 2 Security",			"COM2SEC"	},	typUINT8,	0},
		{	{	24,	"24-Termination Type Installed",		"TERMTYPE"	},	typUINT8,	0},
		{	{	25,	"25-Comm Port Pass Through Mode",		"PASSTHRU"	},	typUINT8,	0},
		{	{	26,	"26-Flag 26",					"FLAG26"	},	typUINT8,	0},	
		{	{	27,	"27-Flag 27",					"FLAG27"	},	typUINT8,	0},
		{	{	28,	"28-Flag 28",					"FLAG28"	},	typUINT8,	0},
		{	{	29,	"29-Flag 29",					"FLAG29"	},	typUINT8,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	15,	// System Variables
	{
		{	{	0,	"0-Device Address",				"ROCADR"	},	typUINT8,	0},
		{	{	1,	"1-Device Group",				"ROCGRP"	},	typUINT8,	0},	
		{	{	2,	"2-Station Name",				"STNNAME"	},	typStr20,	0},
		{	{	3,	"3-Active PIDs",				"#PIDS"		},	typUINT8,	0},
		{	{	4,	"4-Active Meter Runs",				"#AGAS"		},	typUINT8,	0},
		{	{	5,	"5-FST Instructions Per Cycle",			"FSTINST"	},	typUINT8,	0},
		{	{	6,	"6-Base Database Points - History 1",		"#RAM0"		},	typUINT8,	0},	
		{	{	7,	"7-RAM1 Database Points - History 2",		"#RAM1"		},	typUINT8,	0},
		{	{	8,	"8-RAM2 Database Points - History 3",		"#RAM2"		},	typUINT8,	0},
		{	{	9,	"9-Force End of Day",				"FORCE"		},	typUINT8,	0},
		{	{	10,     "10-Contract Hour",				"CONTRC"	},	typUINT8,	0},
		{	{	11,	"11-Version Name - Part Number",		"VERSION"	},	typStr20,	1},	
		{	{	12,	"12-Manufacturing ID",				"VENDORID"	},	typStr20,	1},
		{	{	13,	"13-Time Created",				"CREATETM"	},	typStr20,	1},
		{	{	14,	"14-ROM Serial Number",				"ROMSN"		},	typStr12,	1},
		{	{	15,	"15-Customer Name",				"CUSTNAME"	},	typStr20,	1},
		{	{	16,	"16-Maximum PIDs",				"MAXPIDS"	},	typUINT8,	1},	
		{	{	17,	"17-Maximum Meter Runs",			"MAXAGAS"	},	typUINT8,	1},
		{	{	18,	"18-Maximum Tanks",				"MAXTANKS"	},	typUINT8,	1},
		{	{	19,	"19-FSTs Possible",				"MAXFSTS"	},	typUINT8,	1},
		{	{	20,     "20-RAM Installed",				"RAM"		},	typUINT8,	1},
		{	{	21,	"21-ROM Installed",				"ROM"		},	typUINT8,	1},	
		{	{	22,	"22-MPU Loading",				"MPU"		},	typReal,	1},
		{	{	23,	"23-Utilities",					"UTIL"		},	typUINT8,	1},
		{	{	24,	"24-Device Type",				"ROCTYPE"	},	typUINT16,	1},
		{	{	25,	"25-Units Flag",				"UNITS"		},	typUINT8,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	16,	// FST Registers
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	typStr10,	0},
		{	{	1,	"1-Result Register",				"RR"		},	typReal,	0},	
		{	{	2,	"2-Register #1",				"R1"		},	typReal,	0},
		{	{	3,	"3-Register #2",				"R2"		},	typReal,	0},
		{	{	4,	"4-Register #3",				"R3"		},	typReal,	0},
		{	{	5,	"5-Register #4",				"R4"		},	typReal,	0},
		{	{	6,	"6-Register #5",				"R5"		},	typReal,	0},	
		{	{	7,	"7-Register #6",				"R6"		},	typReal,	0},
		{	{	8,	"8-Register #7",				"R7"		},	typReal,	0},
		{	{	9,	"9-Register #8",				"R8"		},	typReal,	0},
		{	{	10,     "10-Register #9",				"R9"		},	typReal,	0},
		{	{	11,	"11-Register #10",				"R10"		},	typReal,	0},	
		{	{	12,	"12-Timer #1",					"TMR1"		},	typUINT32,	0},
		{	{	13,	"13-Timer #2",					"TMR2"		},	typUINT32,	0},
		{	{	14,	"14-Timer #3",					"TMR3"		},	typUINT32,	0},
		{	{	15,	"15-Timer #4",					"TMR4"		},	typUINT32,	0},
		{	{	16,	"16-Message #1",				"MSG1"		},	typStr30,	0},	
		{	{	17,	"17-Message #2",				"MSG2"		},	typStr30,	0},	
		{	{	18,	"18-Message Data",				"MSGDATA"	},	typStr10,	1},
		{	{	19,	"19-Miscellaneous 1",				"MISC1"		},	typUINT8,	0},
		{	{	20,     "20-Miscellaneous 2",				"MISC2"		},	typUINT8,	0},
		{	{	21,	"21-Miscellaneous 3",				"MISC3"		},	typUINT8,	0},	
		{	{	22,	"22-Miscellaneous 4",				"MISC4"		},	typUINT8,	0},
		{	{	23,	"23-Compare Flag - SVD",			"CMPFLG"	},	typUINT8,	0},
		{	{	24,	"24-Run Flag",					"RUNFLG"	},	typUINT8,	0},
		{	{	25,	"25-Code Size",					"CODESIZE"	},	typUINT16,	1},
		{	{	26,	"26-Instruction Pointer",			"IP"		},	typUINT16,	0},
		{	{	27,	"27-Execution Delay",				"BREAK"		},	typUINT16,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	17,	// Soft Point Parameters
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	typStr10,	0},
		{	{	1,	"1-Integer Flag",				"INT1"		},	typUINT16,	0},
		{	{	2,	"2-Data #1",					"DATA1"		},	typReal,	0},
		{	{	3,	"3-Data #2",					"DATA2"		},	typReal,	0},
		{	{	4,	"4-Data #3",					"DATA3"		},	typReal,	0},
		{	{	5,	"5-Data #4",					"DATA4"		},	typReal,	0},
		{	{	6,	"6-Data #5",					"DATA5"		},	typReal,	0},
		{	{	7,	"7-Data #6",					"DATA6"		},	typReal,	0},
		{	{	8,	"8-Data #7",					"DATA7"		},	typReal,	0},
		{	{	9,	"9-Data #8",					"DATA8"		},	typReal,	0},
		{	{	10,	"10-Data #9",					"DATA9"		},	typReal,	0},
		{	{	11,	"11-Data #10",					"DATA10"	},	typReal,	0},
		{	{	12,	"12-Data #11",					"DATA11"	},	typReal,	0},
		{	{	13,	"13-Data #12",					"DATA12"	},	typReal,	0},
		{	{	14,	"14-Data #13",					"DATA13"	},	typReal,	0},
		{	{	15,	"15-Data #14",					"DATA14"	},	typReal,	0},
		{	{	16,	"16-Data #15",					"DATA15"	},	typReal,	0},
		{	{	17,	"17-Data #16",					"DATA16"	},	typReal,	0},
		{	{	18,	"18-Data #17",					"DATA17"	},	typReal,	0},
		{	{	19,	"19-Data #18",					"DATA18"	},	typReal,	0},
		{	{	20,	"20-Data #19",					"DATA19"	},	typReal,	0},
		{	{	21,	"21-Data #20",					"DATA20"	},	typReal,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	19,	// Database Parameters
	{
		{	{	0,	"0-Pointer to Tag",				"TAGPNTR"	},	typReal,	1},
		{	{	1,	"1-Archive Type",				"ARCHTYPE"	},	typUINT8,	1},
		{	{	2,	"2-Point Type",					"TYPE"		},	typUINT8,	1},
		{	{	3,	"3-Point/Logical Number",			"LOGICAL"	},	typUINT8,	1},
		{	{	4,	"4-Parameter Number",				"PARAM"		},	typUINT8,	1},
		{	{	5,	"5-Yesterday's Total",				"YDYTOTAL"	},	typReal,	1},
		{	{	6,	"6-Last Hour's Total",				"LASTHOUR"	},	typReal,	1},
		{	{	0,	"",						""		},	paramNone,	0},
		},
	21,	// Information for User Defined Points
	{
		{	{	0,	"0-Point Type Description",			"DESC"		},	typStr20,	1},
		{	{	1,	"1-Template Pointer",				"TEMPLATE"	},	typUINT32,	1},
		{	{	2,	"2-Number of Parameters",			"PARAMS"	},	typUINT8,	1},
		{	{	3,	"3-User Program Type",				"UPTYPE"	},	typUINT8,	1},
		{	{	0,	"",						""		},	paramNone,	0},
		},
	41,	// Run Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Atmospheric Pressure",		"ATMPRS"		},	typReal,	0},	
		{	{	2,	"2-Calculation Method II",		"METHOD"		},	typUINT8,	0},
		{	{	3,	"3-Spare",				"SPARE1"		},	typTLP,		0},
		{	{	4,	"4-Pipe Reference Temperature",		"PIPEREFT"		},	typReal,	0},
		{	{	5,	"5-Pipe Material",			"PIPEMAT"		},	typUINT8,	0},	
		{	{	6,	"6-Spare",				"SPARE2"		},	typUINT8,	0},
		{	{	7,	"7-Cd / Ftm",				"FB"			},	typReal,	1},
		{	{	8,	"8-Fr",					"FR"			},	typReal,	1},
		{	{	9,	"9-Y / Fpm",				"FY"			},	typReal,	1},	
		{	{	10,	"10-Fpb Factor",			"FPB"			},	typReal,	1},
		{	{	11,	"11-Ftb Factor",			"FTB"			},	typReal,	1},
		{	{	12,	"12-Ftf Factor",			"FTF"			},	typReal,	1},
		{	{	13,	"13-Fgr Factor",			"FGR"			},	typReal,	1},	
		{	{	14,	"14-Fpb Factor",			"FPB"			},	typReal,	1},

		{	{	15,	"15-History Point 1",			"HISTPT1"		},	typUINT8,	1},
		{	{	16,	"16-RollUp 1",				"ROLLUP1"		},	typUINT8,	1},
		{	{	17,	"17-TLP 1",				"TLP1"			},	typTLP,	1},
		{	{	18,	"18-Conversion 1",			"CONV1"			},	typReal,	1},

		{	{	19,	"19-History Point 2",			"HISTPT2"		},	typUINT8,	1},
		{	{	20,	"20-RollUp 2",				"ROLLUP2"		},	typUINT8,	1},
		{	{	21,	"21-TLP 2",				"TLP2"			},	typTLP,	1},
		{	{	22,	"22-Conversion 2",			"CONV2"			},	typReal,	1},

		{	{	23,	"23-History Point 3",			"HISTPT3"		},	typUINT8,	1},
		{	{	24,	"24-RollUp 3",				"ROLLUP3"		},	typUINT8,	1},
		{	{	25,	"25-TLP 3",				"TLP3"			},	typTLP,	1},
		{	{	26,	"26-Conversion 3",			"CONV3"			},	typReal,	1},

		{	{	27,	"27-History Point 4",			"HISTPT4"		},	typUINT8,	1},
		{	{	28,	"28-RollUp 4",				"ROLLUP4"		},	typUINT8,	1},
		{	{	29,	"29-TLP 4",				"TLP4"			},	typTLP,	1},
		{	{	30,	"30-Conversion 4",			"CONV4"			},	typReal,	1},

		{	{	31,	"31-History Point 5",			"HISTPT5"		},	typUINT8,	1},
		{	{	32,	"32-RollUp 5",				"ROLLUP5"		},	typUINT8,	1},
		{	{	33,	"33-TLP 5",				"TLP5"			},	typTLP,	1},
		{	{	34,	"34-Conversion 5",			"CONV5"			},	typReal,	1},

		{	{	35,	"35-History Point 6",			"HISTPT6"		},	typUINT8,	1},
		{	{	36,	"36-RollUp 6",				"ROLLUP6"		},	typUINT8,	1},
		{	{	37,	"37-TLP 6",				"TLP6"			},	typTLP,	1},
		{	{	38,	"38-Conversion 6",			"CONV6"			},	typReal,	1},

		{	{	39,	"39-History Point 7",			"HISTPT7"		},	typUINT8,	1},
		{	{	40,	"40-RollUp 7",				"ROLLUP7"		},	typUINT8,	1},
		{	{	41,	"41-TLP 7",				"TLP7"			},	typTLP,	1},
		{	{	42,	"42-Conversion 7",			"CONV7"			},	typReal,	1},

		{	{	43,	"43-History Point 8",			"HISTPT8"		},	typUINT8,	1},
		{	{	44,	"44-RollUp 8",				"ROLLUP8"		},	typUINT8,	1},
		{	{	45,	"45-TLP 8",				"TLP8"			},	typTLP,	1},
		{	{	46,	"46-Conversion 8",			"CONV8"			},	typReal,	1},

		{	{	47,	"47-History Point 9",			"HISTPT9"		},	typUINT8,	1},
		{	{	48,	"48-RollUp 9",				"ROLLUP9"		},	typUINT8,	1},
		{	{	49,	"49-TLP 9",				"TLP9"			},	typTLP,	1},
		{	{	50,	"50-Conversion 9",			"CONV9"			},	typReal,	1},

		{	{	51,	"51-History Point 10",			"HISTPT10"		},	typUINT8,	1},
		{	{	52,	"52-RollUp 10",				"ROLLUP10"		},	typUINT8,	1},
		{	{	53,	"53-TLP 10",				"TLP10"			},	typTLP,	1},
		{	{	54,	"54-Conversion 10",			"CONV10"		},	typReal,	1},

		{	{	0,	"",						""		},	paramNone,	0},
		},
	42,	// Extra Run Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Flow Today",				"FLOTDY"		},	typReal,	1},	
		{	{	2,	"2-Flow Yesterday",			"FLOYDY"		},	typReal,	1},
		{	{	3,	"3-Flow Month",				"FLOMTH"		},	typReal,	1},
		{	{	4,	"4-Flow Previous Month",		"FLOPRV"		},	typReal,	1},
		{	{	5,	"5-Flow Accumulated",			"FLOACC"		},	typReal,	1},	
		{	{	6,	"6-Minutes Today",			"MINTDY"		},	typReal,	1},
		{	{	7,	"7-Minutes Yesterday",			"MINYDY"		},	typReal,	1},
		{	{	8,	"8-Minutes Month",			"MINMTH"		},	typReal,	1},
		{	{	9,	"9-Minutes Previous Month",		"MINPRV"		},	typReal,	1},	
		{	{	10,	"10-Minutes Accumulated",		"MINACC"		},	typReal,	1},
		{	{	11,	"11-Energy Today",			"ENGTDY"		},	typReal,	1},
		{	{	12,	"12-Energy Yesterday",			"ENGYDY"		},	typReal,	1},
		{	{	13,	"13-Energy Month",			"ENGMTH"		},	typReal,	1},	
		{	{	14,	"14-Energy Previous Month",		"ENGPRV"		},	typReal,	1},
		{	{	15,	"15-Energy Accumulated",		"ENGACC"		},	typReal,	1},
		{	{	16,	"16-Uncorrected Today",			"UCCTDY"		},	typReal,	1},
		{	{	17,	"17-Uncorrected Yesterday",		"UCCYDY"		},	typReal,	1},
		{	{	18,	"18-Uncorrected Month",			"UCCMTH"		},	typReal,	1},
		{	{	19,	"19-Uncorrected Previous Month",	"UCCPRV"		},	typReal,	1},
		{	{	20,	"20-Uncorrected Accumulated",		"UCCACC"		},	typReal,	1},
		{	{	21,	"21-Orifice Plate Bore Diameter - d",	"ORIFD"			},	typReal,	1},
		{	{	22,	"22-Piper Internal Diameter - D",	"PIPED"			},	typReal,	1},
		{	{	23,	"23-Beta - Diameter Ratio",		"BETA"			},	typReal,	1},
		{	{	24,	"24-Ev - Velocity of Approach",		"EV"			},	typReal,	1},
		{	{	25,	"25-Cd - Coefficient of Discharge",	"CDISCH"		},	typReal,	1},
		{	{	26,	"26-Reynolds Number",			"REYNLD"		},	typReal,	1},
		{	{	27,	"27-Upstream Static Pressure",		"UPRSR"			},	typReal,	1},
		{	{	28,	"28-Molecular Weight",			"MLWGHT"		},	typReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	43,	// User List Parameters
	{
		{	{	0,	"0-Text 1",				"TEXT1"			},	typStr10,	0},
		{	{	1,	"1-Text 2",				"TEXT2"			},	typStr10,	0},	
		{	{	2,	"2-Text 3",				"TEXT3"			},	typStr10,	0},
		{	{	3,	"3-Text 4",				"TEXT4"			},	typStr10,	0},
		{	{	4,	"4-Text 5",				"TEXT5"			},	typStr10,	0},
		{	{	5,	"5-Text 6",				"TEXT6"			},	typStr10,	0},	
		{	{	6,	"6-Text 7",				"TEXT7"			},	typStr10,	0},
		{	{	7,	"7-Text 8",				"TEXT8"			},	typStr10,	0},
		{	{	8,	"8-Text 9",				"TEXT9"			},	typStr10,	0},
		{	{	9,	"9-Text 10",				"TEXT10"		},	typStr10,	0},	
		{	{	10,	"10-Text 11",				"TEXT11"		},	typStr10,	0},
		{	{	11,	"11-Text 12",				"TEXT12"		},	typStr10,	0},
		{	{	12,	"12-Text 13",				"TEXT13"		},	typStr10,	0},
		{	{	13,	"13-Text 14",				"TEXT14"		},	typStr10,	0},	
		{	{	14,	"14-Text 15",				"TEXT15"		},	typStr10,	0},
		{	{	15,	"15-Text 16",				"TEXT16"		},	typStr10,	0},
		{	{	16,	"16-Data 1",				"DATA1"			},	typTLP,	0},
		{	{	17,	"17-Data 2",				"DATA2"			},	typTLP,	0},
		{	{	18,	"18-Data 3",				"DATA3"			},	typTLP,	0},
		{	{	19,	"19-Data 4",				"DATA4"			},	typTLP,	0},
		{	{	20,	"20-Data 5",				"DATA5"			},	typTLP,	0},
		{	{	21,	"21-Data 6",				"DATA6"			},	typTLP,	0},
		{	{	22,	"22-Data 7",				"DATA7"			},	typTLP,	0},
		{	{	23,	"23-Data 8",				"DATA8"			},	typTLP,	0},
		{	{	24,	"24-Data 9",				"DATA9"			},	typTLP,	0},
		{	{	25,	"25-Data 10",				"DATA10"		},	typTLP,	0},
		{	{	26,	"26-Data 11",				"DATA11"		},	typTLP,	0},
		{	{	27,	"27-Data 12",				"DATA12"		},	typTLP,	0},
		{	{	28,	"28-Data 13",				"DATA13"		},	typTLP,	0},
		{	{	29,	"29-Data 14",				"DATA14"		},	typTLP,	0},
		{	{	30,	"30-Data 15",				"DATA15"		},	typTLP,	0},
		{	{	31,	"31-Data 16",				"DATA16"		},	typTLP,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	44,	// Power Control Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Status",				"STATUS"		},	typUINT16,	1},
		{	{	2,	"2-Enable",				"ENABLE"		},	typUINT16,	0},
		{	{	3,	"3-Valid Receive",			"VALRX"			},	typUINT16,	0},
		{	{	4,	"4-Start Time #1",			"STTM1"			},	typUINT16,	0},
		{	{	5,	"5-Start Time #2",			"STTM2"			},	typUINT16,	0},
		{	{	6,	"6-Start Time #3",			"STTM3"			},	typUINT16,	0},
		{	{	7,	"7-On Time #1",				"ONTM1"			},	typUINT16,	0},
		{	{	8,	"8-On Time #2",				"ONTM2"			},	typUINT16,	0},
		{	{	9,	"9-On Time #3",				"ONTM3"			},	typUINT16,	0},
		{	{	10,	"10-Off Time #1",			"OFFTM1"		},	typUINT16,	0},
		{	{	11,	"11-Off Time #2",			"OFFTM2"		},	typUINT16,	0},
		{	{	12,	"12-Off Time #3",			"OFFTM3"		},	typUINT16,	0},
		{	{	13,	"13-Active Time Zone",			"AZONE"			},	typUINT16,	0},
		{	{	14,	"14-Hold Time",				"HLDTM"			},	typUINT16,	0},
		{	{	15,	"15-Power Timer",			"PWRTMR"		},	typUINT16,	1},
		{	{	16,	"16-Discrete Output Number",		"LOGDO"			},	typUINT16,	0},
		{	{	17,	"17-Low Battery",			"LOBAT"			},	typReal,	0},
		{	{	18,	"18-On Counter",			"ONCNT"			},	typUINT32,	0},
		{	{	19,	"19-Off Counter",			"OFFCNT"		},	typUINT32,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	45,	// Meter Calibration and Sampler
	{
		{	{	0,	"0-Calibration Options",				"OPTION"		},	typUINT8,	0},
		{	{	1,	"1-Ambient Temperature of Mercury",			"AMBTMP"		},	typReal,	0},
		{	{	2,	"2-Temperatue of Mercury When Calibrated",		"MTPCAL"		},	typReal,	0},
		{	{	3,	"3-Calibration Weights Grav. Acceleration",		"CALWGT"		},	typReal,	0},
		{	{	4,	"4-Water Temperature When Calibrated",			"WTPCAL"		},	typReal,	0},
		{	{	5,	"5-Air Temperature When Calibrating",			"ATPCAL"		},	typReal,	0},
		{	{	6,	"6-User Correction Factor",				"CORFAC"		},	typReal,	0},
		{	{	7,	"7-Sampler Enable",					"SPLENA"		},	typUINT8,	0},
		{	{	8,	"8-Sampler Volume Accumulation",			"VOLACC"		},	typReal,	0},
		{	{	9,	"9-Sampler Duration",					"SPLDUR"		},	typReal,	0},
		{	{	10,	"10-SM Integrity Alarm Code",				"ALARM"			},	typUINT8,	1},
		{	{	11,	"11-SM Integrity Alarm Deadband Time",			"SMALRMDB"		},	typUINT16,	0},
		{	{	12,	"12-SM Alarm Control",					"SMALRMEN"		},	typUINT8,	0},
		{	{	13,	"13-Integrity Level - Turbine",				"INTEGLVL"		},	typUINT8,	0},
		{	{	 0,	"",							""			},	paramNone,	0},
		},
	46,	// Meter Configuration Parameters
	{
		{	{	0,	"0-Point Tag ID",					"TAG"			},	typStr10,	0},
		{	{	1,	"1-Point Description",					"DESC"			},	typStr30,	0},
		{	{	2,	"2-Calculation Method",					"CMTHI"			},	typUINT8,	0},
		{	{	3,	"3-Caluclation Method II",				"CMTHII"		},	typUINT8,	0},
		{	{	4,	"4-Options",						"AGACFG"		},	typUINT8,	0},
		{	{	5,	"",							""			},	paramNone,	0},	
		{	{	6,	"6-IMP / BMP",						"IMP"			},	typReal,	0},
		{	{	7,	"7-Pipe Diameter",					"PIPDIA"		},	typReal,	0},
		{	{	8,	"8-Pipe Reference Temperature",				"PIPERT"		},	typReal,	0},
		{	{	9,	"9-Pipe Material",					"ALPH"			},	typUINT8,	0},
		{	{	10,	"10-Orifice Diameter",					"ORFDIA"		},	typReal,	0},
		{	{	11,	"11-Orifice Reference Temperature",			"TMEAS"			},	typReal,	0},
		{	{	12,	"12-Orifice Material",					"ORMAT"			},	typUINT8,	0},
		{	{	13,	"13-Base or Contract Pressure",				"PBASE"			},	typReal,	0},
		{	{	14,	"14-Base or Contract Temperature",			"TBASE"			},	typReal,	0},
		{	{	15,	"15-Atmospheric Pressure",				"ATMPRS"		},	typReal,	0},
		{	{	16,	"16-Specific Gravity",					"SPGR"			},	typReal,	0},
		{	{	17,	"17-Heating Value",					"GASHV"			},	typReal,	0},
		{	{	18,	"18-Viscosity",						"VISCOS"		},	typReal,	0},
		{	{	19,	"19-Specific Heat Ratio",				"SPHTRA"		},	typReal,	0},
		{	{	20,	"20-Elevation",						"ELEVAT"		},	typReal,	0},
		{	{	21,	"21-Latitude",						"LATUDE"		},	typReal,	0},
		{	{	22,	"22-Local Gravitational Acceleration",			"GRAVIT"		},	typReal,	0},
		{	{	23,	"23-N2 - Nitrogen",					"NITROG"		},	typReal,	0},
		{	{	24,	"24-CO2 - Carbon Dioxide",				"CARBDI"		},	typReal,	0},
		{	{	25,	"25-H2S - Hydrogen Sulfide",				"HYDSUL"		},	typReal,	0},
		{	{	26,	"26-H2O - Water",					"WATER"			},	typReal,	0},
		{	{	27,	"27-He - Helium",					"HELIUM"		},	typReal,	0},
		{	{	28,	"28-CH4 - Methane",					"METHAN"		},	typReal,	0},
		{	{	29,	"29-C2H6 - Ethane",					"ETHANE"		},	typReal,	0},
		{	{	30,	"30-C3H8 -Propane",					"PROPAN"		},	typReal,	0},
		{	{	31,	"31-C4H10 - n-Butane",					"NBUTAN"		},	typReal,	0},
		{	{	32,	"32-C4H10 - i-Butane",					"IBUTAN"		},	typReal,	0},
		{	{	33,	"33-C5H12 - n-Pentane",					"NPENTA"		},	typReal,	0},
		{	{	34,	"34-C5H12 - i-Pentane",					"IPENTA"		},	typReal,	0},
		{	{	35,	"35-C6H14 - n-Hexane",					"NHEXAN"		},	typReal,	0},
		{	{	36,	"36-C7H16 - n-Heptane",					"NHEPTA"		},	typReal,	0},
		{	{	37,	"37-C8H18 - n-Octane",					"NOCTAN"		},	typReal,	0},
		{	{	38,	"38-C9H20 - n-Nonane",					"NNONAN"		},	typReal,	0},
		{	{	39,	"39-C10H22 - n-Decane",					"NDECAN"		},	typReal,	0},
		{	{	40,	"40-O2 - Oxygen",					"OXYGEN"		},	typReal,	0},
		{	{	41,	"41-CO - Carbon Monoxide",				"CARBMO"		},	typReal,	0},
		{	{	42,	"42-H2 - Hydrogen",					"HYDROG"		},	typReal,	0},
		{	{	43,	"43-Low hw Cutoff / Static K Factor",			"LOFLOW"		},	typReal,	0},
		{	{	44,	"44-High hw Setpoint",					"LODPSP"		},	typReal,	0},
		{	{	45,	"45-Low hw Setpoint",					"HIDPSP"		},	typReal,	0},
		{	{	46,	"46-Enable Stacked hw",					"STDPEN"		},	typUINT8,	0},
		{	{	47,	"47-Low hw TLP",					"LOTYP"			},	typTLP,	0},
		{	{	48,	"48-hw TLP / Uncorrected Flow Rate TLP",		"DPTYP"			},	typTLP,	0},
		{	{	49,	"49-Pf TLP",						"FPTYP"			},	typTLP,	0},
		{	{	50,	"50-Tf TLP",						"TPTYP"			},	typTLP,	0},
		{	{	51,	"51-hw / Uncorrected Flow Rate",			"CURDP"			},	typReal,	0},
		{	{	52,	"52-Pf - Static Pressure",				"CURSP"			},	typReal,	0},
		{	{	53,	"53-Tf - Temperature",					"CURTP"			},	typReal,	0},
		{	{	54,	"54-Alarm Code",					"ALARM"			},	typUINT8,	1},
		{	{	55,	"55-Low Alarm Flow",					"LOALM"			},	typReal,	0},
		{	{	56,	"56-High Alarm Flow",					"HIALM"			},	typReal,	0},
		{	{	57,	"57-Averaging Technique",				"AVGTYP"		},	typUINT8,	0},
		{	{	58,	"58-Full Recalculation Flag",				"FUCALC"		},	typUINT8,	0},
		{	{	59,	"59-Input TLP for Multiple K Factor Calc.",		"KFACTINP"		},	typTLP,		0},
		{	{	60,	"60-Deadband for Multiple K Factor Calc.",		"KFACTDB"		},	typReal,	1},
		{	{	61,	"61-Lowest K Factor",					"KFACTOR1"		},	typReal,	0},
		{	{	62,	"62-2nd K Factor",					"KFACTOR2"		},	typReal,	0},
		{	{	63,	"63-3rd K Factor",					"KFACTOR3"		},	typReal,	0},
		{	{	64,	"64-4th K Factor",					"KFACTOR4"		},	typReal,	0},
		{	{	65,	"65-5th K Factor",					"KFACTOR5"		},	typReal,	0},
		{	{	66,	"66-Lowest EU Value",					"EUVALUE1"		},	typReal,	0},
		{	{	67,	"67-2nd EU Value",					"EUVALUE2"		},	typReal,	0},
		{	{	68,	"68-3rd EU Value",					"EUVALUE3"		},	typReal,	0},
		{	{	69,	"69-4th EU Value",					"EUVALUE4"		},	typReal,	0},
		{	{	70,	"70-5th EU Value",					"EUVALUE5"		},	typReal,	0},	
		{	{	 0,	"",							""			},	paramNone,	0},	
		},
	47,	// Meter Flow Values
	{
		{	{	0,	"0-Flow Rate Per Day",					"FLOWDY"		},	typReal,	1},
		{	{	1,	"1-Energy Rate Per Day",				"ENGDAY"		},	typReal,	1},
		{	{	2,	"2-Flow Rate Per Hour",					"FLOWHR"		},	typReal,	1},
		{	{	3,	"3-Energy Rate Per Hour",				"ENGHR"			},	typReal,	1},
		{	{	4,	"4-Pressue Extension",					"HWPF"			},	typReal,	1},
		{	{	5,	"5-Expansion Factor / Fpm",				"EXPFTR"		},	typReal,	1},	
		{	{	6,	"6-CdFT",						"FR"			},	typReal,	1},
		{	{	7,	"7-Fn / Ftm",						"FB"			},	typReal,	1},
		{	{	8,	"8-Fpb",						"FPB"			},	typReal,	1},
		{	{	9,	"9-Ftb",						"FTB"			},	typReal,	1},
		{	{	10,	"10-Ftf",						"FTF"			},	typReal,	1},
		{	{	11,	"11-Fgr",						"FGR"			},	typReal,	1},
		{	{	12,	"12-Fpv",						"FPV"			},	typReal,	1},
		{	{	13,	"13-Zs",						"FA"			},	typReal,	1},
		{	{	14,	"14-Zb",						"ZB"			},	typReal,	1},
		{	{	15,	"15-Zf1",						"ZF"			},	typReal,	1},
		{	{	16,	"16-IMV / BMV",						"IMVBMV"		},	typReal,	1},
		{	{	17,	"17-d - Orifice Plate Bore Diameter",			"BORDIA"		},	typReal,	1},
		{	{	18,	"18-D - Meter Tube Internal Diameter",			"TUBDIA"		},	typReal,	1},
		{	{	19,	"19-Beta - Diameter Ratio",				"BETA"			},	typReal,	1},
		{	{	20,	"20-Ev - Velocity of Aproach",				"VELAPP"		},	typReal,	1},
		{	{	21,	"21-Average hw / Total Counts",				"AVGDP"			},	typReal,	1},
		{	{	22,	"22-Average Pf",					"AVGAP"			},	typReal,	1},
		{	{	23,	"23-Average Tf",					"AVGTP"			},	typReal,	1},
		{	{	24,	"24-Density",						"DENS"			},	typReal,	1},
		{	{	25,	"25-Base Density",					"BASDEN"		},	typReal,	1},
		{	{	26,	"26-Reynolds Number",					"REYNLD"		},	typReal,	1},
		{	{	27,	"27-Upstream Static Pressure",				"UPSPR"			},	typReal,	1},
		{	{	28,	"28-Molecular Weight",					"MOLWGT"		},	typReal,	1},
		{	{	29,	"29-Fam",						"FAM"			},	typReal,	1},
		{	{	30,	"30-Fwt",						"FWT"			},	typReal,	1},
		{	{	31,	"31-Fwl",						"FWL"			},	typReal,	1},
		{	{	32,	"32-Fpwl - Static",					"FPWLSP"		},	typReal,	1},
		{	{	33,	"33-Fpwl - Differential",				"FPWLDP"		},	typReal,	1},
		{	{	34,	"34-Fhgm",						"FHGM"			},	typReal,	1},
		{	{	35,	"35-Fhgt",						"FHGT"			},	typReal,	1},
		{	{	36,	"36-Flow Today",					"FLOTDY"		},	typReal,	1},
		{	{	37,	"37-Flow Yesterday",					"FLOYDY"		},	typReal,	1},
		{	{	38,	"38-Flow Month",					"FLOMTH"		},	typReal,	1},
		{	{	39,	"39-Flow Previous Month",				"FLOPRV"		},	typReal,	1},
		{	{	40,	"40-Flow Accumulated",					"FLOACC"		},	typReal,	1},
		{	{	41,	"41-Minutes Today",					"MINTDY"		},	typReal,	1},
		{	{	42,	"42-Minutes Yesterday",					"MINYDY"		},	typReal,	1},
		{	{	43,	"43-Minutes Month",					"MINMTH"		},	typReal,	1},
		{	{	44,	"44-Minutes Previous Month",				"MINPRV"		},	typReal,	1},
		{	{	45,	"45-Minutes Accumulated",				"MINACC"		},	typReal,	1},
		{	{	46,	"46-Energy Today",					"ENGTDY"		},	typReal,	1},
		{	{	47,	"47-Energy Yesterday",					"ENGYDY"		},	typReal,	1},
		{	{	48,	"48-Energy Month",					"ENGMTH"		},	typReal,	1},
		{	{	49,	"49-Energy Previous Month",				"ENGPRV"		},	typReal,	1},
		{	{	50,	"50-Energy Accumulated",				"ENGACC"		},	typReal,	1},
		{	{	51,	"51-Uncorrected Today",					"UCCTDY"		},	typReal,	1},
		{	{	52,	"52-Uncorrected Yesterday",				"UCCTDY"		},	typReal,	1},
		{	{	53,	"53-Uncorrected Month",					"UCCMTH"		},	typReal,	1},
		{	{	54,	"54-Uncorrected Previous Month",			"UCCPRV"		},	typReal,	1},
		{	{	55,	"55-Uncorrected Accumulated",				"UCCACC"		},	typReal,	1},
		{	{	56,	"56-Partial Recalculation Flag",			"PACALC"		},	typUINT8,	1},
		{	{	 0,	"",							""			},	paramNone,	0},	
		},
	48,	// PID
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	typStr10,	0},
		{	{	1,	"1-Control Type",				"CTRLT"		},	typUINT8,	0},
		{	{	2,	"2-Active Loop Status",				"ALS"		},	typUINT8,	1},
		{	{	3,	"3-Loop Period",				"LP"		},	typReal,	0},
		{	{	4,	"4-Actual Loop Period",				"ALP"		},	typReal,	1},
		{	{	5,	"5-Primary PV Input Point",			"PPIP"		},	typTLP,		0},
		{	{	6,	"6-Primary Setpoint",				"PS"		},	typReal,	0},
		{	{	7,	"7-Primary Setpoint Change Maximum",		"PSCM"		},	typReal,	0},
		{	{	8,	"8-Primary Proprotional Gain",			"PPG"		},	typReal,	0},
		{	{	9,	"9-Primary Reset (Integral) Gain",		"PIG"		},	typReal,	0},
		{	{      10,	"10-Primary Rate (Derivative) Gain",		"PDG"		},	typReal,	0},
		{	{      11,	"11-Primary Scale Factor",			"PSF"		},	typReal,	0},
		{	{      12,	"12-Primary Integral Deadband",			"PID"		},	typReal,	0},
		{	{      13,	"13-Primary Process Variable",			"PPV"		},	typReal,	0},
		{	{      14,	"14-Primary Change in Output",			"PCO"		},	typReal,	1},
		{	{      15,	"15-Override PV Input Point",			"OPIP"		},	typTLP,		0},
		{	{      16,	"16-Override Setpoint",				"OS"		},	typReal,	0},
		{	{      17,	"17-Override Setpoint Change Maximum",		"OSCM"		},	typReal,	0},
		{	{      18,	"18-Override Proprotional Gain",		"OPG"		},	typReal,	0},
		{	{      19,	"19-Override Reset (Integral) Gain",		"OIG"		},	typReal,	0},
		{	{      20,	"20-Override Rate (Derivative) Gain",		"ODG"		},	typReal,	0},
		{	{      21,	"21-Override Scale Factor",			"OSF"		},	typReal,	0},
		{	{      22,	"22-Override Integral Deadband",		"OID"		},	typReal,	0},
		{	{      23,	"23-Override Process Variable",			"OPV"		},	typReal,	0},
		{	{      24,	"24-Override Change in Output",			"OCO"		},	typReal,	1},
		{	{      25,	"25-Current Output of PID",			"COP"		},	typReal,	0},
		{	{      26,	"26-PID Output Point",				"POP"		},	typTLP,		0},
		{	{      27,	"27-Second Output of PID",			"SOP"		},	typTLP,		0},
		{	{      28,	"28-Output Low Limit Value",			"OLLV"		},	typReal,	0},
		{	{      29,	"29-Output High Limit Value",			"OHLV"		},	typReal,	0},
		{	{      30,	"30-Control Loop Selection",			"CLS"		},	typUINT8,	0},
		{	{      31,	"31-Switch to Override Loop Threshold",		"SOLT"		},	typReal,	0},
		{	{      32,	"32-Primary Loop PV and Setpoint Units",	"PUNTS"		},	typStr10,	0},
		{	{      33,	"33-Override Loop PV and Setpoint Units",	"OUNTS"		},	typStr10,	0},
		{	{      34,	"34-PID Output Units",				"POU"		},	typStr10,	0},
		{	{      35,	"35-Primary Loop Low EU Value",			"PLEU"		},	typReal,	0},
		{	{      36,	"36-Primary Loop High EU Value",		"PHEU"		},	typReal,	0},
		{	{      37,	"37-Override Loop Low EU Value",		"OLEU"		},	typReal,	0},
		{	{      38,	"38-Override Loop High EU Value",		"OHEU"		},	typReal,	0},
		{	{	0,	"",						""		},	paramNone,	0},
		},
	56,	// Analog Input Calibration Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	1},
		{	{	1,	"1-Raw Value 1",			"RAW1"			},	typUINT16,	1},
		{	{	2,	"2-Raw Value 2",			"RAW2"			},	typUINT16,	1},
		{	{	3,	"3-Raw Value 3",			"RAW3"			},	typUINT16,	1},
		{	{	4,	"4-Raw Value 4",			"RAW4"			},	typUINT16,	1},
		{	{	5,	"5-Raw Value 5",			"RAW5"			},	typUINT16,	1},
		{	{	6,	"6-EU Value 1",				"EU1"			},	typReal,	1},
		{	{	7,	"7-EU Value 2",				"EU2"			},	typReal,	1},
		{	{	8,	"8-EU Value 3",				"EU3"			},	typReal,	1},
		{	{	9,	"9-EU Value 4",				"EU4"			},	typReal,	1},
		{	{	10,	"10-EU Value 5",			"EU5"			},	typReal,	1},
		{	{	11,	"11-Press Effect",			"PRESSEFF"		},	typReal,	1},
		{	{	12,	"12-Set EU Value",			"SETVAL"		},	typReal,	0},
		{	{	13,	"13-Manual EU",				"MANUAL"		},	typReal,	1},
		{	{	14,	"14-Timer",				"TIMER"			},	typUINT16,	1},
		{	{	15,	"15-Mode",				"MODE"			},	typUINT8,	0},
		{	{	16,	"16-Type",				"TYPE"			},	typUINT8,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	58,	// Revision Information
	{
		{	{	0,	"0-Device Firmwaree Description",	"DESC"			},	typStr20,	1},
		{	{	1,	"1-Part Number",			"PART#"			},	typStr10,	1},
		{	{	2,	"2-Version",				"VERSION"		},	typStr10,	1},
		{	{	3,	"3-Information Present Flag",		"PRESENT"		},	typUINT8,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},

	86,	// Extended History Parameters
	{
		{	{	0,	"0-Maximum Number of Extended History Points",	"MAXPTS"	},	typUINT8,	1},
		{	{	1,	"1-Save Interval",				"INTERVAL"	},	typUINT8,	0},	
		
		{	{	2,	"2-Point Tag Identification TLP #1",		"TAG#1"		},	typTLP,	0},
		{	{	3,	"3-Extended History Log Point #1",		"HST#1"		},	typTLP,	0},
		{	{	4,	"4-Archive Type #1",				"ARCH1"		},	typUINT8,	0},
		{	{	5,	"5-Averaging or Rate Type #1",			"AVG#1"		},	typUINT8,	0},

		{	{	6,	"6-Point Tag Identification TLP #2",		"TAG#2"		},	typTLP,	0},
		{	{	7,	"7-Extended History Log Point #2",		"HST#2"		},	typTLP,	0},
		{	{	8,	"8-Archive Type #2",				"ARCH2"		},	typUINT8,	0},
		{	{	9,	"9-Averaging or Rate Type #2",			"AVG#2"		},	typUINT8,	0},

		{	{	10,	"10-Point Tag Identification TLP #3",		"TAG#3"		},	typTLP,	0},
		{	{	11,	"11-Extended History Log Point #3",		"HST#3"		},	typTLP,	0},
		{	{	12,	"12-Archive Type #3",				"ARCH3"		},	typUINT8,	0},
		{	{	13,	"13-Averaging or Rate Type #3",			"AVG#3"		},	typUINT8,	0},

		{	{	14,	"14-Point Tag Identification TLP #4",		"TAG#4"		},	typTLP,	0},
		{	{	15,	"15-Extended History Log Point #4",		"HST#4"		},	typTLP,	0},
		{	{	16,	"16-Archive Type #4",				"ARCH4"		},	typUINT8,	0},
		{	{	17,	"17-Averaging or Rate Type #4",			"AVG#4"		},	typUINT8,	0},

		{	{	18,	"18-Point Tag Identification TLP #5",		"TAG#5"		},	typTLP,	0},
		{	{	19,	"19-Extended History Log Point #5",		"HST#5"		},	typTLP,	0},
		{	{	20,	"20-Archive Type #5",				"ARCH5"		},	typUINT8,	0},
		{	{	21,	"21-Averaging or Rate Type #5",			"AVG#5"		},	typUINT8,	0},

		{	{	22,	"22-Point Tag Identification TLP #6",		"TAG#6"		},	typTLP,	0},
		{	{	23,	"23-Extended History Log Point #6",		"HST#6"		},	typTLP,	0},
		{	{	24,	"24-Archive Type #6",				"ARCH6"		},	typUINT8,	0},
		{	{	25,	"25-Averaging or Rate Type #6",			"AVG#6"		},	typUINT8,	0},

		{	{	26,	"26-Point Tag Identification TLP #7",		"TAG#7"		},	typTLP,	0},
		{	{	27,	"27-Extended History Log Point #7",		"HST#7"		},	typTLP,	0},
		{	{	28,	"28-Archive Type #7",				"ARCH7"		},	typUINT8,	0},
		{	{	29,	"29-Averaging or Rate Type #7",			"AVG#7"		},	typUINT8,	0},

		{	{	30,	"30-Point Tag Identification TLP #8",		"TAG#8"		},	typTLP,	0},
		{	{	31,	"31-Extended History Log Point #8",		"HST#8"		},	typTLP,	0},
		{	{	32,	"32-Archive Type #8",				"ARCH8"		},	typUINT8,	0},
		{	{	33,	"33-Averaging or Rate Type #8",			"AVG#8"		},	typUINT8,	0},

		{	{	34,	"34-Point Tag Identification TLP #9",		"TAG#9"		},	typTLP,	0},
		{	{	35,	"35-Extended History Log Point #9",		"HST#9"		},	typTLP,	0},
		{	{	36,	"36-Archive Type #9",				"ARCH9"		},	typUINT8,	0},
		{	{	37,	"37-Averaging or Rate Type #9",			"AVG#9"		},	typUINT8,	0},

		{	{	38,	"38-Point Tag Identification TLP #10",		"TAG#10"	},	typTLP,	0},
		{	{	39,	"39-Extended History Log Point #10",		"HST#10"	},	typTLP,	0},
		{	{	40,	"40-Archive Type #10",				"ARCH10"	},	typUINT8,	0},
		{	{	41,	"41-Averaging or Rate Type #10",		"AVG#10"	},	typUINT8,	0},

		{	{	42,	"42-Point Tag Identification TLP #11",		"TAG#11"	},	typTLP,	0},
		{	{	43,	"43-Extended History Log Point #11",		"HST#11"	},	typTLP,	0},
		{	{	44,	"44-Archive Type #11",				"ARCH11"	},	typUINT8,	0},
		{	{	45,	"45-Averaging or Rate Type #11",		"AVG#11"	},	typUINT8,	0},

		{	{	46,	"46-Point Tag Identification TLP #12",		"TAG#12"	},	typTLP,	0},
		{	{	47,	"47-Extended History Log Point #12",		"HST#12"	},	typTLP,	0},
		{	{	48,	"48-Archive Type #12",				"ARCH12"	},	typUINT8,	0},
		{	{	49,	"49-Averaging or Rate Type #12",		"AVG#12"	},	typUINT8,	0},

		{	{	50,	"50-Point Tag Identification TLP #13",		"TAG#13"	},	typTLP,	0},
		{	{	51,	"51-Extended History Log Point #13",		"HST#13"	},	typTLP,	0},
		{	{	52,	"52-Archive Type #13",				"ARCH13"	},	typUINT8,	0},
		{	{	53,	"53-Averaging or Rate Type #13",		"AVG#13"	},	typUINT8,	0},

		{	{	54,	"54-Point Tag Identification TLP #14",		"TAG#14"	},	typTLP,	0},
		{	{	55,	"55-Extended History Log Point #14",		"HST#14"	},	typTLP,	0},
		{	{	56,	"56-Archive Type #14",				"ARCH14"	},	typUINT8,	0},
		{	{	57,	"57-Averaging or Rate Type #14",		"AVG#14"	},	typUINT8,	0},

		{	{	58,	"58-Point Tag Identification TLP #15",		"TAG#15"	},	typTLP,	0},
		{	{	59,	"59-Extended History Log Point #15",		"HST#15"	},	typTLP,	0},
		{	{	60,	"60-Archive Type #15",				"ARCH15"	},	typUINT8,	0},
		{	{	61,	"61-Averaging or Rate Type #15",		"AVG#15"	},	typUINT8,	0},
					
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	/*	// User Defined ?? String Sizes ??
	87,	// Expanded I/O Information
	{
		{	{	0,	"0-System Mode",			"SYSMODE"		},	typUINT8,	1},
		{	{	1,	"1-Board Health",			"BRDSTS"		},	typUINT8,	1},
		{	{	2,	"2-Boot Version",			"BOOTVER"		},	paramString,	1},
		{	{	3,	"3-Flash Version",			"FIRMVER"		},	paramString,	1},
		{	{	4,	"4-Boot Part Number",			"BOOTPN"		},	paramString,	1},
		{	{	5,	"5-Flash Part Number",			"FIRMPN"		},	paramString,	1},
		{	{	6,	"6-Installed Active",			"ACTIVE"		},	typUINT8,	1},
		{	{	7,	"7-Module Slots Addressable",		"SLOTS"			},	typUINT8,	1},
		{	{	8,	"8-Onboard Temperature",		"BRDTEMP"		},	typReal,	1},
		{	{	9,	"9-+12V Module Incoming",		"INVOLTS"		},	typReal,	1},
		{	{      10,	"10-+12V Module",			"BRDVOLTS"		},	typReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	*/
	91,	// System Variables
	{
		{	{	0,	"0-ROC Address",			"ROCADR"		},	typUINT8,	0},
		{	{	1,	"1-ROC Group",				"ROCGRP"		},	typUINT8,	0},
		{	{	2,	"2-Station Name",			"NAME"			},	typStr20,	0},
		{	{	3,	"3-Part Number and Version",		"VER"			},	typStr20,	1},
		{	{	4,	"4-Time Created",			"VERTIME"		},	typStr20,	1},
		{	{	5,	"5-Manufacturer ID",			"ID"			},	typStr20,	1},
		{	{	6,	"6-Product Description",		"DESC"			},	typStr20,	1},
		{	{	7,	"7-Serial Number",			"SERNUM"		},	typUINT32,	1},
		{	{	8,	"8-Maximum Events",			"MAXEVT"		},	typUINT16,	1},
		{	{	9,	"9-Maximum Alarms",			"MAXALM"		},	typUINT16,	1},
		{	{      10,	"10-Maximum PIDs",			"MAXPID"		},	typUINT8,	1},
		{	{      11,	"11-Maximum Meter Runs",		"MAXMTR"		},	typUINT8,	1},
		{	{      12,	"12-Maximum FSTs",			"MAXFST"		},	typUINT8,	1},
		{	{      13,	"13-Event Index",			"EVTINDEX"		},	typUINT16,	1},
		{	{      14,	"14-Alarm Index",			"ALMINDEX"		},	typUINT16,	1},
		{	{      15,	"15-Active PIDs",			"PIDS"			},	typUINT8,	0},
		{	{      16,	"16-Active Stations",			"STATIONS"		},	typUINT8,	0},
		{	{      17,	"17-Active Orifice Meter Runs",		"OMRS"			},	typUINT8,	0},
		{	{      18,	"18-Active Linear Meter Runs",		"TMRS"			},	typUINT8,	0},
		{	{      19,	"19-FST Clear",				"FSTCLR"		},	typUINT8,	0},
		{	{      20,	"20-Clear Configuration Memory",	"CLRCONF"		},	typUINT8,	0},
		{	{      21,	"21-Wrtie to Configuration Memory",	"WRTCONF"		},	typUINT8,	0},
		{	{      22,	"22-Configuration Memory Write Complete","WRTDONE"		},	typUINT8,	1},
		{	{      23,	"23-MPU Loading",			"MPULOD"		},	typReal,	1},
		{	{      24,	"24-LCD On Off",			"LCDONOFF"		},	typUINT8,	0},
		{	{      25,	"25-IO Scanning",			"IOEN"			},	typUINT8,	0},
		{	{      26,	"26-Warm Start",			"WARM"			},	typUINT8,	0},
		{	{      27,	"27-Cold Start",			"COLDSTRT"		},	typUINT8,	0},
		{	{      28,	"28-LCD Status",			"LCDSTAT"		},	typUINT8,	1},
		{	{      29,	"29-LCD Video Mode",			"LCDMODE"		},	typUINT8,	0},
		{	{      30,	"30-Power Save Time",			"LCDPOWER"		},	typUINT8,	0},
		{	{      31,	"31-Baud Rate Generator #0 Rate",	"RATE1"			},	typUINT32,	0},
		{	{      32,	"32-Baud Rate Generator #1 Rate",	"RATE2"			},	typUINT32,	0},
		{	{      33,	"33-Baud Rate Generator #2 Rate",	"RATE3"			},	typUINT32,	0},
		{	{      34,	"34-Baud Rate Generator #3 Rate",	"RATE4"			},	typUINT32,	0},
		{	{      35,	"35-CRC Check",				"CRCCHK"		},	typUINT8,	0},
		{	{      36,	"36-LED Enable",			"LEDEN"			},	typUINT8,	0},
		{	{      37,	"37-Boot Part Number and Version",	"BOOTVER"		},	typStr20,	1},
		{	{      38,	"38-Boot Firmware Time Created",	"BOOTTIME"		},	typStr20,	1},
		{	{      39,	"39-Active Samplers",			"SAMPLERS"		},	typUINT8,	0},
		{	{      40,	"40-Clear History Configuration",	"CLRHIST"		},	typUINT8,	0},
		{	{      41,	"41-Flash Disk Space Used",		"FLSHUSED"		},	typUINT32,	1},
		{	{      42,	"42-Flash Disk Space Free",		"FLSHAVAL"		},	typUINT32,	1},
		{	{      43,	"43-Number of System Initializations",	"NUMINIT"		},	typUINT16,	0},
		{	{      44,	"44-Number of Warm Starts",		"NUMWARM"		},	typUINT16,	0},
		{	{      45,	"45-Number of Cold Starts",		"NUMCOLD"		},	typUINT16,	0},
		{	{      46,	"46-Number of Power Cycles",		"NUMPWR"		},	typUINT16,	0},
		{	{      47,	"47-Last Power Down Time",		"PWRDNTM"		},	typTime,	1},	// TIME
		{	{      48,	"48-Last Power Up Time",		"PWRUPTM"		},	typTime,	1},	// TIME
		{	{      49,	"49-Auto Logout Period",		"KDLOGOUT"		},	typUINT8,	0},
		{	{      50,	"50-Logical Compatibility Mode",	"COMPMODE"		},	typUINT8,	0},
		{	{      51,	"51-ROC Seris",				"SERIES"		},	typStr20,	1},
		{	{      52,	"52-Num Active Virtual DO",		"MAXVDO"		},	typUINT8,	0},
	//	{	{      53,	"53-System Rollover for Doubles",	"ROLLOVER"		},	typDouble,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	92,	// Logon Parameters
	{
		{	{	0,	"0-Operator ID",			"OPID"			},	typStr10,	0},
		{	{	1,	"1-List Security",			"SECLIST"		},	typUINT8,	0},
		{	{	2,	"2-Keypad Security Level - Read Enable","KEYREAD"		},	typUINT8,	0},
		{	{	3,	"3-Keypad Security Level - Write Enable","KEYWRT"		},	typUINT8,	0},
		{	{	4,	"4-Password",				"PSWD"			},	typUINT16,	0},
		{	{	5,	"5-Access Level",			"ACCESS"		},	typUINT8,	0},
		{	{	6,	"6-Group #1",				"USRGRP1"		},	typUINT8,	0},
		{	{	7,	"7-Group #2",				"USRGRP2"		},	typUINT8,	0},
		{	{	8,	"8-Group #3",				"USRGRP3"		},	typUINT8,	0},
		{	{	9,	"9-Group #4",				"USRGRP4"		},	typUINT8,	0},
		{	{      10,	"10-Group #5",				"USRGRP5"		},	typUINT8,	0},
		{	{      11,	"11-Group #6",				"USRGRP6"		},	typUINT8,	0},
		{	{      12,	"12-Group #7",				"USRGRP7"		},	typUINT8,	0},
		{	{      13,	"13-Group #8",				"USRGRP8"		},	typUINT8,	0},
		{	{      14,	"14-Group #9",				"USRGRP9"		},	typUINT8,	0},
		{	{      15,	"15-Group #10",				"USRGRP10"		},	typUINT8,	0},
		{	{      16,	"16-Group #11",				"USRGRP11"		},	typUINT8,	0},
		{	{      17,	"17-Group #12",				"USRGRP12"		},	typUINT8,	0},
		{	{      18,	"18-Group #13",				"USRGRP13"		},	typUINT8,	0},
		{	{      19,	"19-Group #14",				"USRGRP14"		},	typUINT8,	0},
		{	{      20,	"20-Group #15",				"USRGRP15"		},	typUINT8,	0},
		{	{      21,	"21-Group #16",				"USRGRP16"		},	typUINT8,	0},
		{	{      22,	"18-Group #17",				"USRGRP17"		},	typUINT8,	0},
		{	{      23,	"19-Group #18",				"USRGRP18"		},	typUINT8,	0},
		{	{      24,	"20-Group #19",				"USRGRP19"		},	typUINT8,	0},
		{	{      25,	"21-Group #20",				"USRGRP20"		},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	93,	// License Key Information
	{
		{	{	0,	"0-License Key Slot #",			"HKID"			},	typUINT8,	1},
		{	{	1,	"1-License Code #",			"HKS"			},	typUINT8,	1},
		{	{	2,	"2-Application Name",			"APPNAME"		},	typStr20,	1},
		{	{	3,	"3-Provider Name",			"PRONAME"		},	typStr20,	1},
		{	{	4,	"4-Application Code",			"APPCODE"		},	typUINT16,	1},
		{	{	5,	"5-Version",				"VER"			},	typStr10,	1},
		{	{	6,	"6-Quantity Total",			"QTOTAL"		},	typUINT8,	1},
		{	{	7,	"7-Quantity Remaining",			"QLEFT"			},	typUINT8,	1},
		{	{	8,	"8-Expiration Date",			"EXPDATE"		},	typTime,	1},	// TIME
		{	{	9,	"9-SW License Validity",		"SWL"			},	typUINT8,	1},
		{	{      10,	"10-Time Created",			"VALID"			},	typTime,	1},	// TIME
		{	{	0,	"",					""			},	paramNone,	0},
		},
	94,	// User C Configuration
	{
		{	{	0,	"0-Program Description",		"NAME"			},	typStr20,	1},
		{	{	1,	"1-Program Version String",		"VERSION"		},	typStr12,	1},
		{	{	2,	"2-Program Time Date Stamp",		"CREATED"		},	typTime,	1},	// TIME
		{	{	3,	"3-Program Library Version",		"LIBVER"		},	typStr12,	1},
		{	{	4,	"4-Program Enable",			"RUN"			},	typUINT8,	0},
		{	{	5,	"5-Program Clear",			"CLEAR"			},	typUINT8,	0},
		{	{	6,	"6-Program Status",			"STATUS"		},	typUINT8,	1},
		{	{	7,	"7-Program Disk Space Used",		"FLSHUSED"		},	typUINT32,	1},
		{	{	8,	"8-Program DRAM Used",			"DRAMUSED"		},	typUINT32,	1},
		{	{	9,	"9-Program Auto Restart Counter",	"RSTRTCNT"		},	typUINT32,	0},
		{	{      10,	"10-Program Entry Point",		"ENTRY"			},	typUINT32,	1},
		{	{      11,	"11-Program Handle",			"HANDLE"		},	typUINT32,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	95,	// ROC Comm Ports
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Baud Rate Generator Used",		"BAUD"			},	typUINT16,	0},
		{	{	2,	"2-Stop Bits",				"SBITS"			},	typUINT8,	0},
		{	{	3,	"3-Data Bits",				"DBITS"			},	typUINT8,	0},
		{	{	4,	"4-Parity",				"PARITY"		},	typUINT8,	0},
		{	{	5,	"5-Comm Type",				"TYPE"			},	typUINT8,	1},
		{	{	6,	"6-Store and Forward Port",		"SFPORT"		},	typUINT8,	0},
		{	{	7,	"7-Key On Delay",			"KEYON"			},	typReal,	0},
		{	{	8,	"8-Key Off Delay",			"KEYOFF"		},	typReal,	0},
		{	{	9,	"9-Modem Status",			"MSTAT"			},	typUINT8,	1},
		{	{      10,	"10-Modem Type",			"MTYPE"			},	typUINT8,	0},
		{	{      11,	"11-Connect Time",			"ONTIME"		},	typReal,	0},
		{	{      12,	"12-Configuration Command",		"CONFIGCM"		},	typStr40,	0},
		{	{      13,	"13-Connect Command",			"CONNECTC"		},	typStr40,	0},
		{	{      14,	"14-Disconnect Time",			"OFFTIME"		},	typReal,	0},
		{	{      15,	"15-Inactivity Time",			"INACTTMR"		},	typReal,	0},
		{	{      16,	"16-Modem Disconnect Command",		"DISCCMD"		},	typStr40,	0},
		{	{      17,	"17-SRBX Status",			"SRBXSTAT"		},	typUINT8,	1},
		{	{      18,	"18-Enable SRBX",			"SRBXEN"		},	typUINT8,	0},
		{	{      19,	"19-SRBX Alarm Index",			"SRBXINDE"		},	typUINT16,	1},
		{	{      20,	"20-SRBX Time Base #1",			"SRBXBASE"		},	typReal,	0},
		{	{      21,	"21-SRBX Retry Count #1",		"SRBXRTR2"		},	typUINT8,	0},
		{	{      22,	"22-SRBX Time Base #2",			"SRBXBAS2"		},	typReal,	0},
		{	{      23,	"23-SRBX Retry Count #2",		"SRBXRETR"		},	typUINT8,	0},
		{	{      24,	"24-SRBX Time Base #3",			"SRBXBAS3"		},	typReal,	0},
		{	{      25,	"25-SRBX Retry Count #3",		"SRBXRTR3"		},	typUINT8,	0},
		{	{      26,	"26-SRBX Host Address",			"SRBXHOSR"		},	typUINT8,	0},
		{	{      27,	"27-SRBX Host Group",			"SRBXHOST"		},	typUINT8,	0},
		{	{      28,	"28-Store & Forward Address #1",	"S&FADD1"		},	typUINT8,	0},
		{	{      29,	"29-Store & Forward Group #1",		"S&FGRP1"		},	typUINT8,	0},
		{	{      30,	"30-Store & Forward Address #2",	"S&FADD2"		},	typUINT8,	0},
		{	{      31,	"31-Store & Forward Group #2",		"S&FGRP2"		},	typUINT8,	0},
		{	{      32,	"32-Store & Forward Address #3",	"S&FADD3"		},	typUINT8,	0},
		{	{      33,	"33-Store & Forward Group #3",		"S&FGRP3"		},	typUINT8,	0},
		{	{      34,	"",					""			},	paramNone,	0},
		{	{      35,	"",					""			},	paramNone,	0},
		{	{      36,	"36-ROC Protocol  Valid Receive Ctr",	"ROCRXCNT"		},	typUINT16,	0},
		{	{      37,	"37-ROC Protocol Success Message Time",	"ROCMSGTI"		},	typTime,	0},	// TIME
		{	{      38,	"38-Modbus  Valid Receive Ctr",		"MBRXCNTR"		},	typUINT16,	0},
		{	{      39,	"39-Modbus Success Message Time",	"MBMSGTIM"		},	typTime,	0},	// TIME
		{	{      40,	"40-Number of Invalid Message Bytes",	"INVBYTES"		},	typUINT16,	0},
		{	{      41,	"41-Invalid Message Byte Time",		"INVTIME"		},	typTime,	1},	// TIME
		{	{      42,	"42-Transmit Counter",			"TXCNTR"		},	typUINT16,	0},
		{	{      43,	"43-Port Owner",			"OWNER"			},	typUINT8,	0},
		{	{      44,	"44-ROC Protocol Security Status",	"ROCSEC"		},	typUINT8,	0},
		{	{      45,	"",					""			},	paramNone,	0},
		{	{      46,	"",					""			},	paramNone,	0},
		{	{      47,	"47-Security Inactivity Timeout",	"SECTIMEO"		},	typUINT32,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},

	96,	// FST Parameters
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	typStr10,	0},
		{	{	1,	"1-Result Register",				"RR"		},	typReal,	0},	
		{	{	2,	"2-Register #1",				"R1"		},	typReal,	0},
		{	{	3,	"3-Register #2",				"R2"		},	typReal,	0},
		{	{	4,	"4-Register #3",				"R3"		},	typReal,	0},
		{	{	5,	"5-Register #4",				"R4"		},	typReal,	0},
		{	{	6,	"6-Register #5",				"R5"		},	typReal,	0},	
		{	{	7,	"7-Register #6",				"R6"		},	typReal,	0},
		{	{	8,	"8-Register #7",				"R7"		},	typReal,	0},
		{	{	9,	"9-Register #8",				"R8"		},	typReal,	0},
		{	{	10,     "10-Register #9",				"R9"		},	typReal,	0},
		{	{	11,	"11-Register #10",				"R10"		},	typReal,	0},	
		{	{	12,	"12-Timer #1",					"TMR1"		},	typUINT32,	0},
		{	{	13,	"13-Timer #2",					"TMR2"		},	typUINT32,	0},
		{	{	14,	"14-Timer #3",					"TMR3"		},	typUINT32,	0},
		{	{	15,	"15-Timer #4",					"TMR4"		},	typUINT32,	0},
		{	{	16,	"16-Message #1",				"MSG1"		},	typStr30,	0},	
		{	{	17,	"17-Message #2",				"MSG2"		},	typStr30,	0},	
		{	{	18,	"18-Message Data",				"MSGDATA"	},	typStr10,	1},
		{	{	19,	"19-Miscellaneous 1",				"MISC1"		},	typUINT8,	0},
		{	{	20,     "20-Miscellaneous 2",				"MISC2"		},	typUINT8,	0},
		{	{	21,	"21-Miscellaneous 3",				"MISC3"		},	typUINT8,	0},	
		{	{	22,	"22-Miscellaneous 4",				"MISC4"		},	typUINT8,	0},
		{	{	23,	"23-Compare Flag",				"CMPFLG"	},	typUINT8,	0},
		{	{	24,	"24-Run Flag",					"RUNFLG"	},	typUINT8,	0},
		{	{	25,	"25-Code Size",					"CODESIZE"	},	typUINT16,	1},
		{	{	26,	"26-Instruction Pointer",			"IP"		},	typUINT16,	0},
		{	{	27,	"27-Execution Delay",				"BREAK"		},	typUINT16,	0},
		{	{	28,	"28-FST Version",				"VER"		},	typStr10,	1},
		{	{	29,	"29-FST Description",				"DESC"		},	typStr40,	1},
		{	{	30,     "30-Message Data #2",				"MSGDATA2"	},	typStr10,	1},
		{	{	31,	"31-Steps per Task Cycle",			"INSTPC"	},	typUINT8,	0},	
		{	{	32,	"32-Actual Steps per Task Cycle",		"AINST"		},	typUINT8,	1},
		{	{	33,	"33-FST Cycle Time",				"DURAT"		},	typReal,	1},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	97,	// FST Register Tags
	{
		{	{	0,	"0-Register Tag 1",				"R1TAG"		},	typStr10,	0},
		{	{	1,	"1-Register Tag 2",				"R2TAG"		},	typStr10,	0},	
		{	{	2,	"2-Register Tag 3",				"R3TAG"		},	typStr10,	0},
		{	{	3,	"3-Register Tag 4",				"R4TAG"		},	typStr10,	0},
		{	{	4,	"4-Register Tag 5",				"R5TAG"		},	typStr10,	0},
		{	{	5,	"5-Register Tag 6",				"R6TAG"		},	typStr10,	0},
		{	{	6,	"6-Register Tag 7",				"R7TAG"		},	typStr10,	0},	
		{	{	7,	"7-Register Tag 8",				"R8TAG"		},	typStr10,	0},
		{	{	8,	"8-Register Tag 9",				"R9TAG"		},	typStr10,	0},
		{	{	9,	"9-Register Tag 10",				"R10TAG"	},	typStr10,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	98,	// Soft Point Parameters
	{
		{	{	0,	"0-Soft Point Description",		"TEXT1"			},	typStr40,	0},
		{	{	1,	"1-Float 1",				"DATA1"			},	typReal,	0},
		{	{	2,	"2-Float 2",				"DATA2"			},	typReal,	0},
		{	{	3,	"3-Float 3",				"DATA3"			},	typReal,	0},
		{	{	4,	"4-Float 4",				"DATA4"			},	typReal,	0},
		{	{	5,	"5-Float 5",				"DATA5"			},	typReal,	0},
		{	{	6,	"6-Float 6",				"DATA6"			},	typReal,	0},
		{	{	7,	"7-Float 7",				"DATA7"			},	typReal,	0},
		{	{	8,	"8-Float 8",				"DATA8"			},	typReal,	0},
		{	{	9,	"9-Float 9",				"DATA9"			},	typReal,	0},
		{	{      10,	"10-Float 10",				"DATA10"		},	typReal,	0},
		{	{      11,	"11-Float 11",				"DATA11"		},	typReal,	0},
		{	{      12,	"12-Float 12",				"DATA12"		},	typReal,	0},
		{	{      13,	"13-Float 13",				"DATA13"		},	typReal,	0},
		{	{      14,	"14-Float 14",				"DATA14"		},	typReal,	0},
		{	{      15,	"15-Float 15",				"DATA15"		},	typReal,	0},
		{	{      16,	"16-Float 16",				"DATA16"		},	typReal,	0},
		{	{      17,	"17-Float 17",				"DATA17"		},	typReal,	0},
		{	{      18,	"18-Float 18",				"DATA18"		},	typReal,	0},
		{	{      19,	"19-Float 19",				"DATA19"		},	typReal,	0},
		{	{      20,	"20-Float 20",				"DATA20"		},	typReal,	0},
		{	{      21,	"21-Long 1",				"LONG1"			},	typUINT32,	0},
		{	{      22,	"22-Long 2",				"LONG2"			},	typUINT32,	0},
		{	{      23,	"23-Short 1",				"SHORT1"		},	typUINT16,	0},
		{	{      24,	"24-Short 2",				"SHORT2"		},	typUINT16,	0},
		{	{      25,	"25-Short 3",				"SHORT3"		},	typUINT16,	0},
		{	{      26,	"26-Short 4",				"SHORT4"		},	typUINT16,	0},
		{	{      27,	"27-Short 5",				"SHORT5"		},	typUINT16,	0},
		{	{      28,	"28-Short 6",				"SHORT6"		},	typUINT16,	0},
		{	{      29,	"29-Short 7",				"SHORT7"		},	typUINT16,	0},
		{	{      30,	"30-Short 8",				"SHORT8"		},	typUINT16,	0},
		{	{      31,	"31-Short 9",				"SHORT9"		},	typUINT16,	0},
		{	{      32,	"32-Short 10",				"SHORT10"		},	typUINT16,	0},
		{	{      33,	"33-Byte 1",				"BYTE1"			},	typUINT8,	0},
		{	{      34,	"34-Byte 2",				"BYTE2"			},	typUINT8,	0},
		{	{      35,	"35-Byte 3",				"BYTE3"			},	typUINT8,	0},
		{	{      36,	"36-Byte 4",				"BYTE4"			},	typUINT8,	0},
		{	{      37,	"37-Byte 5",				"BYTE5"			},	typUINT8,	0},
		{	{      38,	"38-Byte 6",				"BYTE6"			},	typUINT8,	0},
		{	{      39,	"39-Byte 7",				"BYTE7"			},	typUINT8,	0},
		{	{      40,	"40-Byte 8",				"BYTE8"			},	typUINT8,	0},
		{	{      41,	"41-Byte 9",				"BYTE9"			},	typUINT8,	0},
		{	{      42,	"42-Byte 10",				"BYTE10"		},	typUINT8,	0},
		{	{      43,	"43-Double 1",				"DOUBLE1"		},	typDouble,	0},
		{	{      44,	"44-Double 2",				"DOUBLE2"		},	typDouble,	0},
		{	{      45,	"45-Double 3",				"DOUBLE3"		},	typDouble,	0},
		{	{      46,	"46-Double 4",				"DOUBLE4"		},	typDouble,	0},
		{	{      47,	"47-Double 5",				"DOUBLE5"		},	typDouble,	0},
		{	{      48,	"48-Double 6",				"DOUBLE6"		},	typDouble,	0},
		{	{      49,	"49-Double 7",				"DOUBLE7"		},	typDouble,	0},
		{	{      50,	"50-Double 8",				"DOUBLE8"		},	typDouble,	0},
		{	{      51,	"51-Double 9",				"DOUBLE9"		},	typDouble,	0},
		{	{      52,	"52-Double 10",				"DOUBLE10"		},	typDouble,	0},
		{	{      53,	"53-Long 3",				"LONG3"			},	typUINT32,	0},
		{	{      54,	"54-Long 4",				"LONG4"			},	typUINT32,	0},
		{	{      55,	"55-Long 5",				"LONG5"			},	typUINT32,	0},
		{	{      56,	"56-Long 6",				"LONG6"			},	typUINT32,	0},
		{	{      57,	"57-Long 7",				"LONG7"			},	typUINT32,	0},
		{	{      58,	"58-Long 8",				"LONG8"			},	typUINT32,	0},
		{	{      59,	"59-Long 9",				"LONG9"			},	typUINT32,	0},
		{	{      60,	"60-Long 10",				"LONG10"		},	typUINT32,	0},
		{	{      61,	"61-Logging Enable",			"LOGGING"		},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	99,	// Configurable Opcode
	{
		{	{	0,	"0-Sequence Revision #",		"REV"			},	typReal,	0},
		{	{	1,	"1-Data 1",				"DATA1"			},	typTLP,	0},
		{	{	2,	"2-Data 2",				"DATA2"			},	typTLP,	0},
		{	{	3,	"3-Data 3",				"DATA3"			},	typTLP,	0},
		{	{	4,	"4-Data 4",				"DATA4"			},	typTLP,	0},
		{	{	5,	"5-Data 5",				"DATA5"			},	typTLP,	0},
		{	{	6,	"6-Data 6",				"DATA6"			},	typTLP,	0},
		{	{	7,	"7-Data 7",				"DATA7"			},	typTLP,	0},
		{	{	8,	"8-Data 8",				"DATA8"			},	typTLP,	0},
		{	{	9,	"9-Data 9",				"DATA9"			},	typTLP,	0},
		{	{	10,	"10-Data 10",				"DATA10"		},	typTLP,	0},
		{	{	11,	"11-Data 11",				"DATA11"		},	typTLP,	0},
		{	{	12,	"12-Data 12",				"DATA12"		},	typTLP,	0},
		{	{	13,	"13-Data 13",				"DATA13"		},	typTLP,	0},
		{	{	14,	"14-Data 14",				"DATA14"		},	typTLP,	0},
		{	{	15,	"15-Data 15",				"DATA15"		},	typTLP,	0},
		{	{	16,	"16-Data 16",				"DATA16"		},	typTLP,	0},
		{	{	17,	"17-Data 17",				"DATA17"		},	typTLP,	0},
		{	{	18,	"18-Data 18",				"DATA18"		},	typTLP,	0},
		{	{	19,	"19-Data 19",				"DATA19"		},	typTLP,	0},
		{	{	20,	"20-Data 20",				"DATA20"		},	typTLP,	0},
		{	{	21,	"21-Data 21",				"DATA21"		},	typTLP,	0},
		{	{	22,	"22-Data 22",				"DATA22"		},	typTLP,	0},
		{	{	23,	"23-Data 23",				"DATA23"		},	typTLP,	0},
		{	{	24,	"24-Data 24",				"DATA24"		},	typTLP,	0},
		{	{	25,	"25-Data 25",				"DATA25"		},	typTLP,	0},
		{	{	26,	"26-Data 26",				"DATA26"		},	typTLP,	0},
		{	{	27,	"27-Data 27",				"DATA27"		},	typTLP,	0},
		{	{	28,	"28-Data 28",				"DATA28"		},	typTLP,	0},
		{	{	29,	"29-Data 29",				"DATA29"		},	typTLP,	0},
		{	{	30,	"30-Data 30",				"DATA30"		},	typTLP,	0},
		{	{	31,	"31-Data 31",				"DATA31"		},	typTLP,	0},
		{	{	32,	"32-Data 32",				"DATA32"		},	typTLP,	0},
		{	{	33,	"33-Data 33",				"DATA33"		},	typTLP,	0},
		{	{	34,	"34-Data 34",				"DATA34"		},	typTLP,	0},
		{	{	35,	"35-Data 35",				"DATA35"		},	typTLP,	0},
		{	{	36,	"36-Data 36",				"DATA36"		},	typTLP,	0},
		{	{	37,	"37-Data 37",				"DATA37"		},	typTLP,	0},
		{	{	38,	"38-Data 38",				"DATA38"		},	typTLP,	0},
		{	{	39,	"39-Data 39",				"DATA39"		},	typTLP,	0},
		{	{	40,	"40-Data 40",				"DATA40"		},	typTLP,	0},
		{	{	41,	"41-Data 41",				"DATA41"		},	typTLP,	0},
		{	{	42,	"42-Data 42",				"DATA42"		},	typTLP,	0},
		{	{	43,	"43-Data 43",				"DATA43"		},	typTLP,	0},
		{	{	44,	"44-Data 44",				"DATA44"		},	typTLP,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	100,	// Power Control Parameters
	{	
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Status",				"STATUS"		},	typUINT8,	1},
		{	{	2,	"2-Enable",				"ENABLE"		},	typUINT8,	0},
		{	{	3,	"3-Start Time #1",			"START1"		},	typUINT32,	0},
		{	{	4,	"4-Start Time #2",			"START2"		},	typUINT32,	0},
		{	{	5,	"5-Start Time #3",			"START3"		},	typUINT32,	0},
		{	{	6,	"6-On Time #1",				"ON1"			},	typUINT32,	0},
		{	{	7,	"7-On Time #2",				"ON2"			},	typUINT32,	0},
		{	{	8,	"8-On Time #3",				"ON3"			},	typUINT32,	0},
		{	{	9,	"9-Off Time #1",			"OFF1"			},	typUINT32,	0},
		{	{      10,	"10-Off Time #2",			"OFF2"			},	typUINT32,	0},
		{	{      11,	"11-Off Time #3",			"OFF3"			},	typUINT32,	0},
		{	{      12,	"12-Active Time Zone",			"ACTIVE"		},	typUINT8,	1},
		{	{      13,	"13-Hold Time",				"HLDTM"			},	typUINT32,	0},
		{	{      14,	"14-Power Time",			"PWRTMR"		},	typUINT32,	1},
		{	{      15,	"15-Discrete Output Number",		"DONUM"			},	typTLP,	0},
		{	{      16,	"16-Low Battery",			"LOBAT"			},	typReal,	0},
		{	{      17,	"17-Cumulative On Time",		"TIMEON"		},	typUINT32,	0},
		{	{      18,	"18-Cumulative Off Time",		"TIMEOFF"		},	typUINT32,	0},
		{	{      19,	"19-Low Battery Deadband",		"BATDB"			},	typReal,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	101,	// Discrete Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Scanning",			"SCANEN"	},	typUINT8,	0},	
		{	{	2,	"2-Filter",			"FILTER"	},	typReal,	0},
		{	{	3,	"3-Status",			"STATUS"	},	typUINT8,	0},
		{	{	4,	"4-Invert Status",		"INVERT"	},	typUINT8,	0},
		{	{	5,	"5-Latch Status",		"LATCH"		},	typUINT8,	0},
		{	{	6,	"6-Accumulated Value",		"ACCUM"		},	typUINT32,	0},	
		{	{	7,	"7-Cumulative On Time",		"ONCTR"		},	typReal,	0},
		{	{	8,	"8-Cumulative Off Time",	"OFFCTR"	},	typReal,	0},
		{	{	9,	"9-Alarming",			"ALEN"		},	typUINT8,	0},
		{	{	10,	"10-SRBX on Clear",		"SRBXCLR"	},	typUINT8,	0},
		{	{	11,	"11-SRBX on Set",		"SRBXSET"	},	typUINT8,	0},
		{	{	12,	"12-Alarm Code",		"ALARM"		},	typUINT8,	1},
		{	{	13,	"13-Module Scan Period",	"SCANPR"	},	typReal,	0},	
		{	{	14,	"14-Actual Scan Time",		"SCAN"		},	typReal,	1},
		{	{	15,	"15-Physical Status",		"PSTATUS"	},	typUINT8,	1},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	102,	// Discrete Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Units Tag",			"UNITS"		},	typStr10,	0},
		{	{	2,	"2-Scanning"			"SCANEN"	},	typUINT8,	0},
		{	{	3,	"3-Alarming",			"ALEN"		},	typUINT8,	0},
		{	{	4,	"4-SRBX on Clear",		"SRBXCLR"	},	typUINT8,	0},
		{	{	5,	"5-SRBX on Set",		"SRBXSET"	},	typUINT8,	0},
		{	{	6,	"6-Alarm Code",			"ALARM"		},	typUINT8,	1},
		{	{	7,	"7-Failsafe on Reset",		"CLRONRS"	},	typUINT8,	0},
		{	{	8,	"8-Auto Output",		"STATUS"	},	typUINT8,	0},
		{	{	9,	"9-Accumulated Value",		"ACCUM"		},	typUINT32,	0},
		{	{	10,	"10-Momentary Mode",		"MOMMODE"	},	typUINT8,	0},
		{	{	11,	"11-Momentary Active",		"MOMACTIV"	},	typUINT8,	1},
		{	{	12,	"12-Toggle Mode",		"TOGMODE"	},	typUINT8,	0},
		{	{	13,	"13-Timed Discrete Output",	"TDOMODE"	},	typUINT8,	0},
		{	{	14,	"14-Time On",			"TIMEON"	},	typReal,	0},
		{	{	15,	"15-Cycle Time",		"CYCTIM"	},	typReal,	0},
		{	{	16,	"16-Low Reading Time",		"LOWTIME"	},	typReal,	0},
		{	{	17,	"17-High Reading Time",		"HITIME"	},	typReal,	0},
		{	{	18,	"18-Low Reading EU",		"MINEU"		},	typReal,	0},
		{	{	19,	"19-High Reading EU",		"MAXEU"		},	typReal,	0},
		{	{	20,	"20-EU Value",			"EU"		},	typReal,	0},
		{	{	21,	"21-Manual Value",		"MANVAL"	},	typUINT8,	0},
		{	{	22,	"22-Fault Value",		"FAULTVAL"	},	typUINT8,	0},
		{	{	23,	"",				""		},	paramNone,	0},
		{	{	24,	"24-Physical Output",		"PHYSOUT"	},	typUINT8,	1},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	103,	// Analog Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Units Tag",			"UNITS"		},	typStr10,	0},	
		{	{	2,	"2-Scanning",			"SCANEN"	},	typUINT8,	0},
		{	{	3,	"3-Scan Period",		"SCANPR"	},	typReal,	0},
		{	{	4,	"4-Actual Scan Time",		"SCAN"		},	typReal,	1},
		{	{	5,	"5-Filter",			"FILTER"	},	typUINT8,	0},
		{	{	6,	"6-Averaging",			"AVGEN"		},	typUINT8,	0},	
		{	{	7,	"7-Raw AD Input",		"CURRAW"	},	typUINT16,	0},
		{	{	8,	"8-Zero Raw",			"MINRAW"	},	typUINT16,	0},
		{	{	9,	"9-Mid Point Raw #1",		"RAW1"		},	typUINT16,	0},
		{	{	10,     "10-Mid Point Raw #2",		"RAW2"		},	typUINT16,	0},
		{	{	11,	"11-Mid Point Raw #3",		"RAW3"		},	typUINT16,	0},
		{	{	12,	"12-Span Raw",			"MAXRAW"	},	typUINT16,	0},	
		{	{	13,	"13-Zero EU",			"MINEU"		},	typReal,	0},
		{	{	14,	"14-Mid Point EU #1",		"EU1"		},	typReal,	0},
		{	{	15,	"15-Mid Point EU #2",		"EU2"		},	typReal,	0},
		{	{	16,	"16-Mid Point EU #3",		"EU3"		},	typReal,	0},
		{	{	17,	"17-Span EU",			"MAXEU"		},	typReal,	0},
		{	{	18,	"18-Offset",			"ZEROSHIF"	},	typReal,	0},
		{	{	19,	"19-Set Value",			"SETVAL"	},	typReal,	0},
		{	{	20,	"20-Manual Value",		"MANVAL"	},	typReal,	1},
		{	{	21,	"21-EU Value",			"EU"		},	typReal,	0},	
		{	{	22,	"22-Clipping",			"CLIPEN"	},	typUINT8,	0},
		{	{	23,	"23-Low Low Alarm EU",		"LOLOAL"	},	typReal,	0},
		{	{	24,	"24-Low Alarm EU",		"LOAL"		},	typReal,	0},
		{	{	25,	"25-High Alarm EU",		"HIAL"		},	typReal,	0},
		{	{	26,	"26-High High Alarm EU",	"HIHIAL"	},	typReal,	0},	
		{	{	27,	"27-Rate Alarm EU",		"RATEAL"	},	typReal,	0},
		{	{	28,	"28-Alarm Deadband",		"ALDBNDL"	},	typReal,	0},
		{	{	29,	"29-Alarming",			"ALEN"		},	typUINT8,	0},
		{	{	30,     "30-SRBX on Clear",		"SRBXCLR"	},	typUINT8,	0},
		{	{	31,	"31-SRBX on Set",		"SRBXSET"	},	typUINT8,	0},
		{	{	32,	"32-Alarm Code",		"ALARM"		},	typUINT8,	1},	
		{	{	33,	"33-Calibration Timer",		"CALTMR"	},	typReal,	1},
		{	{	34,	"34-Calibration Mode",		"CALMODE"	},	typUINT8,	0},
		{	{	35,	"35-Calibration Type",		"CALTYPE"	},	typUINT8,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	104,	// Analog Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	typStr10,	0},
		{	{	1,	"1-Units Tag",			"UNITS"		},	typStr10,	0},	
		{	{	2,	"2-Scanning",			"SCANEN"	},	typUINT8,	0},
		{	{	3,	"3-Alarming",			"ALEN"		},	typUINT8,	0},
		{	{	4,	"4-SRBX on Clear",		"SRBXCLR"	},	typUINT8,	0},	
		{	{	5,	"5-SRBX on Set",		"SRBXSET"	},	typUINT8,	0},
		{	{	6,	"6-Alarm Code",			"ALARM"		},	typUINT8,	1},
		{	{	7,	"7-Failsafe on Reset",		"CLRONRS"	},	typUINT8,	0},
		{	{	8,	"8-Zero Raw",			"MINRAW"	},	typUINT16,	0},
		{	{	9,	"9-Span Raw",			"MAXRAW"	},	typUINT16,	0},
		{	{	10,	"10-Zero EU",			"MINEU"		},	typReal,	0},
		{	{	11,	"11-Span EU",			"MAXEU"		},	typReal,	0},	
		{	{	12,	"12-EU Value",			"EU"		},	typReal,	0},
		{	{	13,	"13-Raw DA Output",		"CURRAW"	},	typUINT16,	1},
		{	{	14,	"14-Manual Value",		"MANVAL"	},	typReal,	0},	
		{	{	15,	"15-Failsafe Value",		"FAULTVAL"	},	typReal,	0},
		{	{	16,	"16-Physical Value",		"PHYSVAL"	},	typReal,	1},
		{	{	0,	"",				""		},	paramNone,	0},
		},
	105,	// Pulse Inputs
	{
		{	{	0,	"0-Point Tag ID",			"TAG"		},	typStr10,	0},
		{	{	1,	"1-Units Tag",				"UNITS"		},	typStr10,	0},	
		{	{	2,	"2-Scanning",				"SCANEN"	},	typUINT8,	0},
		{	{	3,	"3-Scan Period",			"SCANPR"	},	typReal,	0},
		{	{	4,	"4-Accumulated Value",			"ACCUM"		},	typUINT32,	0},
		{	{	5,	"5-Contract Hour",			"CHOUR"		},	typUINT8,	0},
		{	{	6,	"6-Pulses for Day",			"PPD"		},	typUINT32,	1},	
		{	{	7,	"7-Current Rate Period",		"RATEPR"	},	typUINT8,	0},
		{	{	8,	"8-Conversion",				"CONV"		},	typUINT8,	0},
		{	{	9,	"9-Conversion Value",			"CONVAL"	},	typReal,	0},
		{	{	10,     "10-Current Rate",			"RATE"		},	typReal,	1},
		{	{	11,	"11-EU Value Mode",			"MODE"		},	typUINT8,	0},
		{	{	12,	"12-Rollover Maximum",			"MAXROLL"	},	typReal,	0},	
		{	{	13,	"13-EU Value",				"EU"		},	typReal,	0},
		{	{	14,	"14-Low Low Alarm EU",			"LOLOAL"	},	typReal,	0},
		{	{	15,	"15-Low Alarm EU",			"LOAL"		},	typReal,	0},
		{	{	16,	"16-High Alarm EU",			"HIAL"		},	typReal,	0},
		{	{	17,	"17-High High Alarm EU",		"HIHIAL"	},	typReal,	0},
		{	{	18,	"18-Rate Alarm EU",			"RATEAL"	},	typReal,	0},
		{	{	19,	"19-Alarm Deadband",			"ALDBND"	},	typReal,	0},
		{	{	20,	"20-Alarming",				"ALEN"		},	typUINT8,	0},
		{	{	21,	"21-SRBX on Clear",			"SRBXCLR"	},	typUINT8,	0},	
		{	{	22,	"22-SRBX on Set",			"SRBXSET"	},	typUINT8,	0},
		{	{	23,	"23-Alarm Code",			"ALARM"		},	typUINT8,	1},
		{	{	24,	"24-Todays Total",			"TDYTOT"	},	typReal,	0},
		{	{	25,	"25-Yesterdays Total",			"YDYTOT"	},	typReal,	1},
		{	{	26,	"26-Corrected Pulse Accumulation",	"CORACCUM"	},	typReal,	1},	
		{	{	27,	"27-Frequency",				"FREQNCY"	},	typReal,	0},
		{	{	 0,	"",					""		},	paramNone,	0},
		},
	108,	// Multi-Variable Sensor
	{
		{	{	0,	"0-Point Tag ID",			"TAG"		},	typStr10,	0},
		{	{	1,	"1-Sensor Address",			"ADDR"		},	typUINT8,	0},	
		{	{	2,	"2-Poll Mode",				"POLL"		},	typUINT8,	0},
		{	{	3,	"3-Units",				"UNITS"		},	typUINT8,	0},
		{	{	4,	"4-Inches H20",				"DPBASE"	},	typUINT8,	0},
		{	{	5,	"5-Pressure Tap Location",		"TAP"		},	typUINT8,	0},
		{	{	6,	"6-Action on Failure",			"FAILMODE"	},	typUINT8,	0},	
		{	{	7,	"7-Software Revision MVS Interface",	"REV"		},	typUINT8,	1},
		{	{	8,	"8-Sensor Voltage",			"CVOLTS"	},	typReal,	1},
		{	{	9,	"9-Sensor Alarming",			"SENSALEN"	},	typUINT8,	0},
		{	{	10,	"10-Sensor Alarm Code",			"SENSALAR"	},	typUINT8,	1},
		{	{	11,	"11-Sensor Range Status",		"SENSRANG"	},	typUINT8,	1},	
		{	{	12,	"12-Static Pressure Effect",		"ZEROSHIF"	},	typReal,	0},
		{	{	13,	"13-DP Zero Calibration Point",		"DPMINRAW"	},	typReal,	1},
		{	{	14,	"14-DP Calibration Mid Point #1",	"DPRAW1"	},	typReal,	1},
		{	{	15,	"15-DP Calibration Mid Point #2",	"DPRAW2"	},	typReal,	1},
		{	{	16,	"16-DP Calibration Mid Point #3",	"DPRAW3"	},	typReal,	1},	
		{	{	17,	"17-DP Span Calibration Point",		"DPMAXRAW"	},	typReal,	1},
		{	{	18,	"18-Manual DP",				"DPMANVAL"	},	typReal,	1},
		{	{	19,	"19-DP Reading",			"DPEU"		},	typReal,	0},
		{	{	20,	"20-DP Reverse Reading",		"DPREVEU"	},	typReal,	1},
		{	{	21,	"21-DP Fault Value",			"DPFALT"	},	typReal,	0},	
		{	{	22,	"22-DP Low Alarm EU",			"DPLOAL"	},	typReal,	0},
		{	{	23,	"23-DP High Alarm EU",			"DPHIAL"	},	typReal,	0},
		{	{	24,	"24-DP Alarm Deadband",			"DPALDB"	},	typReal,	0},
		{	{	25,	"25-DP Alarming",			"DPALEN"	},	typUINT8,	0},
		{	{	26,	"26-SRBX on Clear",			"DPSRBXCL"	},	typUINT8,	0},	
		{	{	27,	"27-SRBX on Set",			"DPSRBXSE"	},	typUINT8,	0},
		{	{	28,	"28-DP Alarm Code",			"DPALARM"	},	typUINT8,	1},
		{	{	29,	"29-SP Zero Calibration Point",		"SPMINRAW"	},	typReal,	1},
		{	{	30,	"30-SP Calibration Mid Point #1",	"SPRAW1"	},	typReal,	1},
		{	{	31,	"31-SP Calibration Mid Point #2",	"SPRAW2"	},	typReal,	1},	
		{	{	32,	"32-SP Calibration Mid Point #3",	"SPRAW3"	},	typReal,	1},
		{	{	33,	"33-SP Span Calibration Point",		"SPMAXRAW"	},	typReal,	1},
		{	{	34,	"34-Manual SP",				"SPMANVAL"	},	typReal,	1},
		{	{	35,	"35-SP Reading",			"SPEU"		},	typReal,	0},
		{	{	36,	"36-SP Fault Value",			"SPFALT"	},	typReal,	0},	
		{	{	37,	"37-SP Low Alarm EU",			"SPLOAL"	},	typReal,	0},
		{	{	38,	"38-SP High Alarm EU",			"SPHIAL"	},	typReal,	0},
		{	{	39,	"39-SP Alarm Deadband",			"SPALDB"	},	typReal,	0},
		{	{	40,	"40-SP Alarming",			"SPALEN"	},	typUINT8,	0},
		{	{	41,	"41-SRBX on Clear",			"SPSRBXCL"	},	typUINT8,	0},	
		{	{	42,	"42-SRBX on Set",			"SPSRBXSE"	},	typUINT8,	0},
		{	{	43,	"43-SP Alarm Code",			"SPALARM"	},	typUINT8,	1},
		{	{	44,	"44-TMP Zero Calibration Point",	"TMPMINRA"	},	typReal,	1},
		{	{	45,	"45-TMP Calibration Mid Point #1",	"TMPRAW1"	},	typReal,	1},
		{	{	46,	"46-TMP Calibration Mid Point #2",	"TMPRAW2"	},	typReal,	1},	
		{	{	47,	"47-TMP Calibration Mid Point #3",	"TMPRAW3"	},	typReal,	1},
		{	{	48,	"48-TMP Span Calibration Point",	"TMPMAXRA"	},	typReal,	1},
		{	{	49,	"49-Manual TMP",			"TMPMANVA"	},	typReal,	1},
		{	{	50,	"50-TMP Reading",			"TMPEU"		},	typReal,	0},
		{	{	51,	"51-TMP Fault Value",			"TMPFALT"	},	typReal,	0},	
		{	{	52,	"52-TMP Low Alarm EU",			"TMPLOAL"	},	typReal,	0},
		{	{	53,	"53-TMP High Alarm EU",			"TMPHIAL"	},	typReal,	0},
		{	{	54,	"54-TMP Alarm Deadband",		"TMPALDB"	},	typReal,	0},
		{	{	55,	"55-TMP Alarming",			"TMPALEN"	},	typUINT8,	0},
		{	{	56,	"56-SRBX on Clear",			"TMPSRBXC"	},	typUINT8,	0},	
		{	{	57,	"57-SRBX on Set",			"TMPSRBXS"	},	typUINT8,	0},
		{	{	58,	"58-TMP Alarm Code",			"TMPALARM"	},	typUINT8,	1},
		{	{	59,	"59-Calibrate Command",			"CALCMD"	},	typUINT8,	0},
		{	{	60,	"60-Calibrate Type",			"CALTYPE"	},	typUINT8,	0},
		{	{	61,	"61-Calibrate Set Value",		"CALVAL"	},	typReal,	0},	
		{	{	62,	"62-Sensor SRBX on Clear",		"SRBXCLR"	},	typUINT8,	0},
		{	{	63,	"63-Sensor SRBX on Set",		"SRBXSET"	},	typUINT8,	0},
		{	{	64,	"64-Pressure Offset",			"SPOFFSET"	},	typReal,	0},
		{	{	65,	"65-Sensor Type",			"SENSTYPE"	},	typUINT8,	1},
		{	{	 0,	"",					""		},	paramNone,	0},
		},
	109,	// System Analog Inputs
	{	
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Units Tag",				"UNITS"			},	typStr10,	0},
		{	{	2,	"2-Scanning",				"SCANEN"		},	typUINT8,	0},
		{	{	3,	"3-Scan Period",			"SCANPR"		},	typReal,	0},
		{	{	4,	"4-Actual Scan Time",			"SCAN"			},	typReal,	1},
		{	{	5,	"5-Filter",				"FILTER"		},	typUINT8,	0},
		{	{	6,	"6-Averaging",				"AVGEN"			},	typUINT8,	0},
		{	{	7,	"7-Raw AD Input",			"CURRAW"		},	typUINT16,	0},
		{	{	8,	"8-Zero Raw",				"MINRAW"		},	typUINT16,	1},
		{	{	9,	"9-Span Raw",				"MAXRAW"		},	typUINT16,	1},
		{	{      10,	"10-Zero EU",				"MINEU"			},	typReal,	1},
		{	{      11,	"11-Span EU",				"MAXEU"			},	typReal,	1},
		{	{      12,	"12-EU Value",				"EU"			},	typReal,	0},
		{	{      13,	"13-Clipping",				"CLIPEN"		},	typUINT8,	0},
		{	{      14,	"14-Low Low Alarm EU",			"LOLOAL"		},	typReal,	0},
		{	{      15,	"15-Low Alarm EU",			"LOAL"			},	typReal,	0},
		{	{      16,	"16-High Alarm EU",			"HIAL"			},	typReal,	0},
		{	{      17,	"17-High High Alarm EU",		"HIHIAL"		},	typReal,	0},
		{	{      18,	"18-Rate Alarm EU",			"RATEAL"		},	typReal,	0},
		{	{      19,	"19-Alarm Deadband",			"ALDBND"		},	typReal,	0},
		{	{      20,	"20-Alarming",				"ALEN"			},	typUINT8,	0},
		{	{      21,	"21-SRBX on Clear",			"SRBXCLR"		},	typUINT8,	0},
		{	{      22,	"22-SRBX on Set",			"SRBXSET"		},	typUINT8,	0},
		{	{      23,	"23-Alarm Code",			"ALARM"			},	typUINT8,	1},
		{	{      24,	"24-Units",				"UNITMODE"		},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	110,	// PID Control Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-PID Mode",				"MODE"			},	typUINT8,	0},
		{	{       2,	"2-Loop Period",			"LOOP"			},	typReal,	0},
		{	{       3,	"3-Actual Loop Period",			"ACTLOOP"		},	typReal,	1},
		{	{	4,	"4-Action on Process Variable Failure",	"FAILACTN"		},	typUINT8,	1},
		{	{	5,	"5-Discrete Output Control",		"DISCRETE"		},	typUINT8,	0},
		{	{	6,	"6-Reset Mode",				"RSMODE"		},	typUINT8,	0},
		{	{	7,	"7-Manual Tracking",			"MANTRK"		},	typUINT8,	0},
		{	{	8,	"8-Primary Input Point",		"P_INTLP"		},	typTLP,		0},
		{	{       9,	"9-Primary Process Variable",		"P_PV"			},	typReal,	0},
		{	{	10,	"10-Primary Setpoint Point",		"P_SPTLP"		},	typTLP,		0},
		{	{       11,	"11-Primary Setpoint",			"P_SP"			},	typReal,	0},
		{	{       12,	"12-Primary Setpoint Low Limit",	"P_LO"			},	typReal,	0},
		{	{       13,	"13-Primary Setpoint High Limit",	"P_HI"			},	typReal,	0},
		{	{       14,	"14-Primary Setpoint Max Change Rate",	"P_MAX"			},	typReal,	0},
		{	{       15,	"15-Primary Proportional Gain",		"P_PROP"		},	typReal,	0},
		{	{       16,	"16-Primary Integral Gain",		"P_ITGL"		},	typReal,	0},
		{	{       17,	"17-Primary Derivative Gain",		"P_DRVT"		},	typReal,	0},
		{	{       18,	"18-Primary Scale Factor",		"P_SCLE"		},	typReal,	0},
		{	{       19,	"19-Primary Integral Deadband",		"P_DBND"		},	typReal,	0},
		{	{       20,	"20-Primary Change in Output",		"P_DELTA"		},	typReal,	0},
		{	{	21,	"21-Override Loop Mode",		"P_EN"			},	typUINT8,	0},
		{	{	22,	"22-Loop Switch Select",		"SWITCH"		},	typUINT8,	0},
		{	{	23,	"23-Override Input Point",		"S_INTLP"		},	typTLP,		0},
		{	{       24,	"24-Override Process Variable",		"S_PV"			},	typReal,	0},
		{	{	25,	"25-Override Setpoint Point",		"S_SPTLP"		},	typTLP,		0},
		{	{       26,	"26-Override Setpoint",			"S_SP"			},	typReal,	0},
		{	{       27,	"27-Override Setpoint Low Limit",	"S_LO"			},	typReal,	0},
		{	{       28,	"28-Override Setpoint High Limit",	"S_HI"			},	typReal,	0},
		{	{       29,	"29-Override Setpoint Max Change Rate",	"S_MAX"			},	typReal,	0},
		{	{       30,	"30-Override Proportional Gain",	"S_PROP"		},	typReal,	0},
		{	{       31,	"31-Override Integral Gain",		"S_ITGL"		},	typReal,	0},
		{	{       32,	"32-Override Derivative Gain",		"S_DRVT"		},	typReal,	0},
		{	{       33,	"33-Override Scale Factor",		"S_SCLE"		},	typReal,	0},
		{	{       34,	"34-Override Integral Deadband",	"S_DBND"		},	typReal,	0},
		{	{       35,	"35-Override Change in Output",		"S_DELTA"		},	typReal,	1},
		{	{	36,	"36-Switch Status",			"SWSTAT"		},	typUINT8,	1},
		{	{       37,	"37-Current Output of PID",		"CURVAL"		},	typReal,	0},
		{	{	38,	"38-Output of PID Point",		"OUT_TLP"		},	typTLP,	0},
		{	{	39,	"39-Discrete Open PID Output",		"D0OPNPT"		},	typTLP,	0},
		{	{	40,	"40-Discrete Close PID Output",		"D0CLSPT"		},	typTLP,	0},
		{	{       41,	"41-Output Low Limit",			"OUTLOLIM"		},	typReal,	0},
		{	{       42,	"42-Output High Limit",			"OUTHILIM"		},	typReal,	0},
		{	{	43,	"43-Output Low Limit Status",		"OUTLOSTS"		},	typUINT8,	1},
		{	{	44,	"44-Output High Limit Status",		"OUTHISTS"		},	typUINT8,	1},
		{	{	45,	"45-Primary Process Value Status",	"PPVSTS"		},	typUINT8,	1},
		{	{	46,	"46-Primary Setpoint Low Limit Status",	"PSPLLSTS"		},	typUINT8,	1},
		{	{	47,	"47-Primary Setpoint High Limit Status","PSPHLSTS"		},	typUINT8,	1},
		{	{	48,	"48-Primary Setpoint Rate Limited",	"PSPRLIM"		},	typUINT8,	1},
		{	{	49,	"49-Override Process Variable Status",	"OPVSTS"		},	typUINT8,	1},
		{	{	50,	"50-Override Setpoint Low Limit Status","OSPLLSTS"		},	typUINT8,	1},
		{	{	51,	"51-Override Setpoint High Limit Status","OSPHLSTS"		},	typUINT8,	1},
		{	{	52,	"52-Override Setpont Rate Limited",	"OSPRLIM"		},	typUINT8,	1},
		{	{       53,	"53-Override Threshold Value",		"OVRTHRES"		},	typReal,	0},
		{	{       54,	"54-Action Wait Time",			"WAITTIME"		},	typReal,	0},
		{	{	55,	"55-Upstream Output Point",		"UPSTRTLP"		},	typTLP,	0},
		{	{	56,	"56-Downstream Output Point",		"DNSTRTLP"		},	typTLP,	0},
		{	{       57,	"57-Valve Dead Time",			"VALVE_DT"		},	typReal,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	/*
	112,	// Station Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		
		{	{	0,	"",					""			},	paramNone,	0},
		},
	*/
	113,	// Orifice Meter Run Configuration
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Point Description",			"PD"			},	typStr30,	0},
		{	{	2,	"2-Static Pressure Units",		"SPU"			},	typUINT8,	0},
		{	{	3,	"3-Static Pressure Tap",		"SPT"			},	typUINT8,	0},
		{	{	4,	"4-Alarming",				"ALM"			},	typUINT8,	0},
		{	{	5,	"5-SRBX on Clear",			"SRBXCLR"		},	typUINT8,	0},
		{	{	6,	"6-SRBX on Set",			"SRBXSET"		},	typUINT8,	0},
		{	{	7,	"7-Alarm Code",				"AC"			},	typUINT8,	1},
		{	{	8,	"8-Low Alarm Flow",			"LAF"			},	typReal,	0},
		{	{	9,	"9-High Alarm Flow",			"HAF"			},	typReal,	0},
		{	{      10,	"10-Alarm Deadband",			"AD"			},	typReal,	0},
		{	{      11,	"11-Station Number",			"SN"			},	typUINT8,	0},
		{	{      12,	"12-Pipe Diameter",			"PD"			},	typReal,	0},
		{	{      13,	"13-Pipe Reference Temp",		"PRT"			},	typReal,	0},
		{	{      14,	"14-Pipe Material",			"PM"			},	typUINT8,	0},
		{	{      15,	"15-Orifice Diameter",			"OD"			},	typReal,	0},
		{	{      16,	"16-Orifice Reference Temp",		"ORT"			},	typReal,	0},
		{	{      17,	"17-Orifice Material",			"OM"			},	typUINT8,	0},
		{	{      18,	"18-Viscosity",				"VIS"			},	typReal,	0},
		{	{      19,	"19-Specific Heat Ratio",		"SHR"			},	typReal,	0},
		{	{      20,	"20-Low DP Cutoff",			"LDPC"			},	typReal,	0},
		{	{      21,	"21-Stacked DP",			"SDP"			},	typUINT8,	0},
		{	{      22,	"22-High DP Setpoint",			"HDPSPT"		},	typReal,	0},
		{	{      23,	"23-Low DP Setpoint",			"LDPSPT"		},	typReal,	0},
		{	{      24,	"24-Low DP TLP",			"LDPTLP"		},	typTLP,		0},
		{	{      25,	"25-DP TLP",				"DPTLP"			},	typTLP,		0},
		{	{      26,	"26-DP (Differential Pressure, hw)",	"DP"			},	typReal,	0},
		{	{      27,	"27-SP TLP",				"SPTLP"			},	typTLP,		0},
		{	{      28,	"28-SP (Static Pressure, Pf)",		"SP"			},	typReal,	0},
		{	{      29,	"29-TMP TLP",				"TMPTLP"		},	typTLP,		0},
		{	{      30,	"30-TMP (Temperature, Tf)",		"TMP"			},	typReal,	0},
		{	{      31,	"31-Static Pressure Deadwt Calibrator",	"SPDC"			},	typUINT8,	0},
		{	{      32,	"32-Diff Pressure Deadwt Calibrator",	"DPDC"			},	typUINT8,	0},
		{	{      33,	"33-Calibration Weights Gravit Acc",	"CWGA"			},	typReal,	0},
		{	{      34,	"34-User Correction Factor",		"UCF"			},	typReal,	0},
		{	{      35,	"35-Differential Meter Type",		"DMT"			},	typUINT8,	0},
		{	{      36,	"36-Maintenance Mode",			"MM"			},	typUINT8,	0},
		{	{      37,	"37-Temperature Tap Location",		"TTL"			},	typUINT8,	0},
		{	{      38,	"38-Joule-Thomson Option",		"JTO"			},	typUINT8,	0},
		{	{      39,	"39-Joule-Thomson Coefficient Option",	"JTCO"			},	typUINT8,	0},
		{	{      40,	"40-Pressure Loss Option",		"PLO"			},	typUINT8,	0},
		{	{      41,	"41-Flow Calculations Alarming",	"FCAG"			},	typUINT8,	0},
		{	{      42,	"42-Flow Calculations Alarm Code",	"FCAC"			},	typUINT8,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	114,	// Orifice Meter Run Values
	{
		{	{	0,	"0-Flow Rate per Day",			"FLOWDY"			},	typReal,	1},
		{	{	1,	"1-Energy Rate per Day",		"ENGDAY"			},	typReal,	1},
		{	{	2,	"2-Flow Rate per Hour",			"FLOWHR"			},	typReal,	0},
		{	{	3,	"3-Energy Rate per Hour",		"ENGHR"			},	typReal,	1},
		{	{	4,	"4-Pressure Extension (hwPf)",		"HWPF"			},	typReal,	0},
		{	{	5,	"5-CdFT",				"CDFT"			},	typReal,	0},
		{	{	6,	"6-Velocity of Approach (Ev)",		"EV"			},	typReal,	0},
		{	{	7,	"7-Expansion Factor (Y1)",		"Y1"			},	typReal,	0},
		{	{	8,	"8-Orifice Plate Bore Diameter (d)",	"OPBD"			},	typReal,	1},
		{	{	9,	"9-Zf1",				"ZF1"			},	typReal,	0},
		{	{      10,	"10-Fpb",				"FPB"			},	typReal,	1},
		{	{      11,	"11-Ftb",				"FTB"			},	typReal,	1},
		{	{      12,	"12-Multiplier Value",			"MULT"			},	typReal,	0},
		{	{      13,	"13-Meter Tube Internal Diameter (D)",	"MTID"			},	typReal,	1},
		{	{      14,	"14-Diameter Ratio (Beta)",		"DR"			},	typReal,	0},
		{	{      15,	"15-Flowing Density",			"FD"			},	typReal,	0},
		{	{      16,	"16-Reynolds Number",			"RN"			},	typReal,	0},
		{	{      17,	"17-Upstream Static Pressure",		"USP"			},	typReal,	1},
		{	{      18,	"18-SP Fpwl",				"SPFPWL"		},	typReal,	1},
		{	{      19,	"19-Flow Today",			"TDYFLO"		},	typReal,	1},
		{	{      20,	"20-Flow Yesterday",			"YDYFLO"		},	typReal,	1},
		{	{      21,	"21-Flow Month",			"FLOMTH"		},	typReal,	1},
		{	{      22,	"22-Flow Previous Month",		"FPRMTH"		},	typReal,	1},
		{	{      23,	"23-Flow Accumulated",			"FLOACC"		},	typReal,	1},
		{	{      24,	"24-Minutes Today",			"TDYMIN"		},	typReal,	1},
		{	{      25,	"25-Minutes Yesterday",			"YDYMIN"		},	typReal,	1},
		{	{      26,	"26-Minutes Month",			"MTHMIN"		},	typReal,	1},
		{	{      27,	"27-Minutes Previous Month",		"MPRMIN"		},	typReal,	1},
		{	{      28,	"28-Minutes Accumulated",		"MINACC"		},	typReal,	1},
		{	{      29,	"29-Energy Today",			"TDYENG"		},	typReal,	1},
		{	{      30,	"30-Energy Yesterday",			"YDYENG"		},	typReal,	1},
		{	{      31,	"31-Energy Month",			"ENGMTH"		},	typReal,	1},
		{	{      32,	"32-Energy Previous Month",		"ENGPMTH"		},	typReal,	1},
		{	{      33,	"33-Energy Accumulated",		"ENGACC"		},	typReal,	1},
		{	{      34,	"34-Mass Rate Per Day",			"MASSDY"		},	typReal,	1},
		{	{      35,	"35-Mass Rate Per Hour",		"MASSHR"		},	typReal,	1},
		{	{      36,	"36-Mass Today",			"MASTDY"		},	typReal,	1},
		{	{      37,	"37-Mass Yesterday",			"MASYDY"		},	typReal,	1},
		{	{      38,	"38-Mass Month",			"MASMTH"		},	typReal,	1},
		{	{      39,	"39-Mass Previous Month",		"MASPMTH"		},	typReal,	1},
		{	{      40,	"40-Mass Accumulated",			"MASACC"		},	typReal,	1},
		{	{      41,	"41-DP Fpwl",				"DPFPWL"		},	typReal,	1},
		{	{      42,	"42-Flow Acc Double Precision",		"FLACCD"		},	typDouble,	1},
		{	{      43,	"43-Minutes Acc Double Precision",	"MNACCD"		},	typDouble,	1},
		{	{      44,	"44-Energy Acc Double Precision",	"ENACCD"		},	typDouble,	1},
		{	{      45,	"45-Mass Acc Double Precision",		"MSACCD"		},	typDouble,	1},
		{	{      46,	"46-Current Maint Flow Acc",		"CMFACC"		},	typDouble,	1},
		{	{      47,	"47-Current Maint Uncorrected Flow Acc","CMUFLA"		},	typDouble,	1},
		{	{      48,	"48-Prev Maint Flow Acc",		"PMFACC"		},	typDouble,	1},
		{	{      49,	"49-Prev Maint Uncorrected Flow Acc",	"PMUFACC"		},	typDouble,	1},
		{	{      50,	"50-Upstream Temperature",		"UPTMP"			},	typReal,	1},
		{	{      51,	"51-Joule-Thomson Coefficient",		"JTC"			},	typReal,	0},
		{	{      52,	"52-Pressure Loss",			"PL"			},	typReal,	0},
		{	{      53,	"53-Uncorrrected Today",		"UCCTDY"		},	typReal,	1},
		{	{      54,	"54-Uncorrrected Yesterday",		"UCCYDY"		},	typReal,	1},
		{	{      55,	"55-Uncorrrected Month",		"UCCMTH"		},	typReal,	1},
		{	{      56,	"56-Uncorrrected Previous Month",	"UCCPMTH"		},	typReal,	1},
		{	{      57,	"57-Uncorrrected Accumulated",		"UCCACC"		},	typReal,	1},
		{	{      58,	"58-Uncorrected Flow Acc Double",	"UCCACCD"		},	typDouble,	1},
		{	{      59,	"59-Flow Hour",				"FLOHR"			},	typReal,	1},
		{	{      60,	"60-Flow Previous Hour",		"FLOPHR"		},	typReal,	1},
		{	{      61,	"61-Energy Hour",			"ENGHR"			},	typReal,	1},
		{	{      62,	"62-Energy Previous Hour",		"ENGPHR"		},	typReal,	1},
		{	{      63,	"63-Mass Hour",				"MASSHR"		},	typReal,	1},
		{	{      64,	"64-Mass Previous Hour",		"MASSPHR"		},	typReal,	1},
		{	{      65,	"65-Uncorrected Flow Hour",		"UCCFLHR"		},	typReal,	1},
		{	{      66,	"66-Uncorrected Flow Previous Hour",	"UCCFLPHR"		},	typReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	
	115,	// Turbine Meter Run
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Point Description",			"DESC"			},	typStr30,	0},
		{	{	2,	"2-Static Pressure Units",		"SPU"			},	typUINT8,	0},
		{	{	3,	"3-Alarming",				"ALM"			},	typUINT8,	0},
		{	{	4,	"4-SRBX on Clear",			"SRBXCLR"		},	typUINT8,	0},
		{	{	5,	"5-SRBX on Set",			"SRBXSET"		},	typUINT8,	0},
		{	{	6,	"6-Alarm Code",				"ALMC"			},	typUINT8,	1},
		{	{	7,	"7-Low Alarm Flow",			"LOALMFLO"		},	typReal,	0},
		{	{	8,	"8-High Alarm Flow",			"HIALMFLO"		},	typReal,	0},
		{	{       9,	"9-Alarm Deadband",			"ALMDEAD"		},	typReal,	0},
		{	{      10,	"10-Station Number",			"SN"			},	typUINT8,	0},
		{	{      11,	"11-K-Factor",				"KFAC"			},	typReal,	0},
		{	{      12,	"12-No Flow Time",			"NFT"			},	typUINT32,	0},
		{	{      13,	"13-Uncorrected Flow Rate/Mass TLP",	"UCCFLTLP"		},	typTLP,		0},
		{	{      14,	"14-Uncorrected Flow Rate/Mass",	"UCCFLR"		},	typReal,	0},
		{	{      15,	"15-SP TLP",				"SPTLP"			},	typTLP,		0},
		{	{      16,	"16-SP (Static Pressure, Pf)",		"SP"			},	typReal,	0},
		{	{      17,	"17-TMP TLP",				"TMPTLP"		},	typTLP,		0},
		{	{      18,	"18-TMP (Temperature, Tf)",		"TMP"			},	typReal,	0},
		{	{      19,	"19-Static Pressure Deadwt Calibrator",	"SPDCAL"		},	typUINT8,	0},
		{	{      20,	"20-Calibration Weights Gravit Acc",	"CWGA"			},	typReal,	0},
		{	{      21,	"21-User Correction Factor",		"UCCFAC"		},	typReal,	0},
		{	{      22,	"22-Low Flow Cutoff",			"LFC"			},	typReal,	0},
		{	{      23,	"23-Speed of Sound Option",		"SOSO"			},	typUINT8,	0},
		{	{      24,	"24-K-Factor Option",			"KFO"			},	typUINT8,	0},
		{	{      25,	"25-K-Factor 1",			"KF1"			},	typReal,	0},
		{	{      26,	"26-K-Factor 1 Frequency",		"KF1F"			},	typReal,	0},
		{	{      27,	"27-K-Factor 2",			"KF2"			},	typReal,	0},
		{	{      28,	"28-K-Factor 2 Frequency",		"KF2F"			},	typReal,	0},
		{	{      29,	"29-K-Factor 3",			"KF3"			},	typReal,	0},
		{	{      30,	"30-K-Factor 3 Frequency",		"KF3F"			},	typReal,	0},
		{	{      31,	"31-K-Factor 4",			"KF4"			},	typReal,	0},
		{	{      32,	"32-K-Factor 4 Frequency",		"KF4F"			},	typReal,	0},
		{	{      33,	"33-K-Factor 5",			"KF5"			},	typReal,	0},
		{	{      34,	"34-K-Factor 5 Frequency",		"KF5F"			},	typReal,	0},
		{	{      35,	"35-K-Factor 6",			"KF6"			},	typReal,	0},
		{	{      36,	"36-K-Factor 6 Frequency",		"KF6F"			},	typReal,	0},
		{	{      37,	"37-K-Factor 7",			"KF7"			},	typReal,	0},
		{	{      38,	"38-K-Factor 7 Frequency",		"KF7F"			},	typReal,	0},
		{	{      39,	"39-K-Factor 8",			"KF8"			},	typReal,	0},
		{	{      40,	"40-K-Factor 8 Frequency",		"KF8F"			},	typReal,	0},
		{	{      41,	"41-K-Factor 9",			"KF9"			},	typReal,	0},
		{	{      42,	"42-K-Factor 9 Frequency",		"KF9F"			},	typReal,	0},
		{	{      43,	"43-K-Factor 10",			"KF10"			},	typReal,	0},
		{	{      44,	"44-K-Factor 10 Frequency",		"KF10F"			},	typReal,	0},
		{	{      45,	"45-K-Factor 11",			"KF11"			},	typReal,	0},
		{	{      46,	"46-K-Factor 11 Frequency",		"KF11F"			},	typReal,	0},
		{	{      47,	"47-K-Factor 12",			"KF12"			},	typReal,	0},
		{	{      48,	"48-K-Factor 12 Frequency",		"KF12F"			},	typReal,	0},
		{	{      49,	"49-Meter Input Type",			"MIT"			},	typUINT8,	0},
		{	{      50,	"50-Mass Pressure Compensation Option",	"MPCO"			},	typUINT8,	0},
		{	{      51,	"51-Calibration Pressure",		"CALPR"			},	typReal,	0},
		{	{      52,	"52-Pressure Effect Mass Comp Coef",	"PEMCC"			},	typReal,	0},
		{	{      53,	"53-Maintenance Mode",			"MM"			},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	
	
	116,	// Turbine Meter Run Values
	{
		{	{	0,	"0-Flow Rate Per Day",			"FLOWDY"		},	typReal,	1},
		{	{	1,	"1-Energy Rate Per Day",		"ENGDAY"		},	typReal,	1},
		{	{	2,	"2-Flow Rate Per Hour",			"FLOWHR"		},	typReal,	1},
		{	{	3,	"3-Energy Rate Per Hour",		"ENGHR"			},	typReal,	1},
		{	{	4,	"4-Pressure Multiplier",		"PRMULT"		},	typReal,	0},
		{	{	5,	"5-Temperature Multiplier",		"TMPMULT"		},	typReal,	0},
		{	{	6,	"6-Compressibility Multiplier",		"CMPMULT"		},	typReal,	0},
		{	{	7,	"7-Zf1",				"ZF1"			},	typReal,	0},
		{	{	8,	"8-Multiplier Value",			"MULT"			},	typReal,	1},
		{	{	9,	"9-Pulses Accumulated",			"PACC"			},	typUINT32,	1},
		{	{      10,	"10-Density",				"DENS"			},	typReal,	0},
		{	{      11,	"11-Fpwl",				"FPWL"			},	typReal,	1},
		{	{      12,	"12-Flow Today",			"FLOTDY"		},	typReal,	1},
		{	{      13,	"13-Flow Yesterday",			"FLOYDY"		},	typReal,	1},
		{	{      14,	"14-Flow Month",			"FLOMTH"		},	typReal,	1},
		{	{      15,	"15-Flow Previous Month",		"FLOPMTH"		},	typReal,	1},
		{	{      16,	"16-Flow Accumulated",			"FLOACC"		},	typReal,	1},
		{	{      17,	"17-Minutes Today",			"MINTDY"		},	typReal,	1},
		{	{      18,	"18-Minutes Yesterday",			"MINYDY"		},	typReal,	1},
		{	{      19,	"19-Minutes Month",			"MINMTH"		},	typReal,	1},
		{	{      20,	"20-Minutes Previous Month",		"MINPMTH"		},	typReal,	1},
		{	{      21,	"21-Minutes Accumulated",		"MINACC"		},	typReal,	1},
		{	{      22,	"22-Energy Today",			"ENGTDY"		},	typReal,	1},
		{	{      23,	"23-Energy Yesterday",			"ENGYDY"		},	typReal,	1},
		{	{      24,	"24-Energy Month",			"ENGMTH"		},	typReal,	1},
		{	{      25,	"25-Energy Previous Month",		"ENGPMTH"		},	typReal,	1},
		{	{      26,	"26-Energy Accumulated",		"ENGACC"		},	typReal,	1},
		{	{      27,	"27-Uncorrected Today",			"UCCTDY"		},	typReal,	1},
		{	{      28,	"28-Uncorrected Yesterday",		"UCCYDY"		},	typReal,	1},
		{	{      29,	"29-Uncorrected Month",			"UCCMTH"		},	typReal,	1},
		{	{      30,	"30-Uncorrected Previous Month",	"UCCPMTH"		},	typReal,	1},
		{	{      31,	"31-Uncorrected Accumuulated",		"UCCACC"		},	typReal,	1},
		{	{      32,	"32-Measured Speed Of Sound",		"MSOS"			},	typReal,	0},
		{	{      33,	"33-Calculated Speed Of Sound",		"CSOS"			},	typReal,	1},
		{	{      34,	"34-Mass Rate Per Day",			"MASSDY"		},	typReal,	1},
		{	{      35,	"35-Mass Rate Per Hour",		"MASSHR"		},	typReal,	1},
		{	{      36,	"36-Mass Today",			"MASSTDY"		},	typReal,	1},
		{	{      37,	"37-Mass Yesterday",			"MASSYDY"		},	typReal,	1},
		{	{      38,	"38-Mass Month",			"MASSMTH"		},	typReal,	1},
		{	{      39,	"39-Mass Previous Month",		"MASSPMTH"		},	typReal,	1},
		{	{      40,	"40-Mass Accumulated",			"MASSACC"		},	typReal,	1},
		{	{      41,	"41-Flow Acc Double Prec",		"FLACCD"		},	typDouble,	1},
		{	{      42,	"42-Minutes Acc Double Prec",		"MINACCD"		},	typDouble,	1},
		{	{      43,	"43-Energy Acc Double Prec",		"ENGACCD"		},	typDouble,	1},
		{	{      44,	"44-Uncorrected Flow Acc Double Prec",	"UCCACCD"		},	typDouble,	1},
		{	{      45,	"45-Mass Acc Double Prec",		"MASSACCD"		},	typDouble,	1},
		{	{      46,	"46-Current Maint Flow Acc",		"CMFACC"		},	typDouble,	1},
		{	{      47,	"47-Current Maint Uncorrected Flow Acc","CMUCCACC"		},	typDouble,	1},
		{	{      48,	"48-Prev Maint Flow Acc",		"PMFLOACC"		},	typDouble,	1},
		{	{      49,	"49-Prev Maint Uncorrected Flow Acc",	"PMUCCACC"		},	typDouble,	1},
		{	{      50,	"50-Flow Hour",				"FLOWHR"		},	typReal,	1},
		{	{      51,	"51-Flow Previous Hour",		"FLOWPHR"		},	typReal,	1},
		{	{      52,	"52-Energy Hour",			"ENGHR"			},	typReal,	1},
		{	{      53,	"53-Energy Previous Hour",		"ENGPHR"		},	typReal,	1},
		{	{      54,	"54-Mass Hour",				"MASSHR"		},	typReal,	1},
		{	{      55,	"55-Mass Previous Hour",		"MASSPHR"		},	typReal,	1},
		{	{      56,	"56-Uncorrected Flow Hour",		"UCCHR"			},	typReal,	1},
		{	{      57,	"57-Uncorrected Flow Previous Hour",	"UCCPHR"		},	typReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	
	
	117,	// Modbus Configuration Parameters
	{
		{	{	0,	"0-Transmission Mode",			"TRANSMOD"		},	typUINT8,	0},
		{	{	1,	"1-Byte Order",				"BYTE"			},	typUINT8,	0},
		{	{	2,	"2-Event Log Enable",			"EVENT"			},	typUINT8,	0},
		{	{	3,	"3-Slave Exception Status",		"SLAVESTA"		},	typUINT8,	1},
		{	{	4,	"4-Master Poll Request Trigger",	"MSTRPOLL"		},	typUINT8,	0},
		{	{	5,	"5-Master Starting Request Number",	"MSTRSTAR"		},	typUINT16,	0},
		{	{	6,	"6-Master Number of Requests",		"MSTRRQST"		},	typUINT16,	0},
		{	{	7,	"7-Master Continuous Polling Enable",	"MSTRCONT"		},	typUINT8,	0},
		{	{	8,	"8-Master Poll Request Delay",		"MSTRDLAY"		},	typReal,	0},
		{	{	9,	"",					""			},	paramNone,	0},
		{	{      10,	"10-Low Integer Scale",			"LOINT"			},	typUINT16,	0},
		{	{      11,	"11-High Integer Scale",		"HIGHINT"		},	typUINT16,	0},
		{	{      12,	"12-Low Float Scale 1",			"LFLT1"			},	typReal,	0},
		{	{      13,	"13-High Float Scale 1",		"HFLT1"			},	typReal,	0},
		{	{      14,	"14-Low Float Scale 2",			"LFLT2"			},	typReal,	0},
		{	{      15,	"15-High Float Scale 2",		"HFLT2"			},	typReal,	0},
		{	{      16,	"16-Low Float Scale 3",			"LFLT3"			},	typReal,	0},
		{	{      17,	"17-High Float Scale 3",		"HFLT3"			},	typReal,	0},
		{	{      18,	"18-Low Float Scale 4",			"LFLT4"			},	typReal,	0},
		{	{      19,	"19-High Float Scale 4",		"HFLT4"			},	typReal,	0},
		{	{      20,	"20-Low Float Scale 5",			"LFLT5"			},	typReal,	0},
		{	{      21,	"21-High Float Scale 5",		"HFLT5"			},	typReal,	0},
		{	{      22,	"22-Low Float Scale 6",			"LFLT6"			},	typReal,	0},
		{	{      23,	"23-High Float Scale 6",		"HFLT6"			},	typReal,	0},
		{	{      24,	"24-Low Float Scale 7",			"LFLT7"			},	typReal,	0},
		{	{      25,	"25-High Float Scale 7",		"HFLT7"			},	typReal,	0},
		{	{      26,	"26-Low Float Scale 8",			"LFLT8"			},	typReal,	0},
		{	{      27,	"27-High Float Scale 8",		"HFLT8"			},	typReal,	0},
		{	{      28,	"28-Master Poll Timeout",		"TIMEOUT"		},	typUINT8,	0},
		{	{      29,	"29-Master Poll Number of Retries"	"RETRIES"		},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	118,	// Modbus Register to TLP Mapping
	{
		{	{	0,	"0-Point Tag ID",			"MBMAP"			},	typStr10,	0},
		{	{	1,	"1-Start Register #1",			"START1"		},	typUINT16,	0},
		{	{	2,	"2-End Register #1",			"END1"			},	typUINT16,	0},
		{	{	3,	"3-ROC Parameter(s)",			"PARA1"			},	typTLP,	0},
		{	{	4,	"4-Indexing",				"INDEX1"		},	typUINT8,	0},
		{	{	5,	"5-Conversion Code",			"CONV1"			},	typUINT8,	0},
		{	{	6,	"6-Comm Port #1",			"COMPRT1"		},	typUINT8,	0},
		{	{	7,	"7-Start Register #2",			"START2"		},	typUINT16,	0},
		{	{	8,	"8-End Register #2",			"END2"			},	typUINT16,	0},
		{	{	9,	"9-ROC Parameter(s)",			"PARA2"			},	typTLP,	0},
		{	{	10,	"10-Indexing",				"INDEX2"		},	typUINT8,	0},
		{	{	11,	"11-Conversion Code",			"CONV2"			},	typUINT8,	0},
		{	{	12,	"12-Comm Port #2",			"COMPRT2"		},	typUINT8,	0},
		{	{	13,	"13-Start Register #3",			"START3"		},	typUINT16,	0},
		{	{	14,	"14-End Register #3",			"END3"			},	typUINT16,	0},
		{	{	15,	"15-ROC Parameter(s)",			"PARA3"			},	typTLP,	0},
		{	{	16,	"16-Indexing",				"INDEX3"		},	typUINT8,	0},
		{	{	17,	"17-Conversion Code",			"CONV3"			},	typUINT8,	0},
		{	{	18,	"18-Comm Port #3",			"COMPRT3"		},	typUINT8,	0},
		{	{	19,	"19-Start Register #4",			"START4"		},	typUINT16,	0},
		{	{	20,	"20-End Register #4",			"END4"			},	typUINT16,	0},
		{	{	21,	"21-ROC Parameter(s)",			"PARA4"			},	typTLP,	0},
		{	{	22,	"22-Indexing",				"INDEX4"		},	typUINT8,	0},
		{	{	23,	"23-Conversion Code",			"CONV4"			},	typUINT8,	0},
		{	{	24,	"24-Comm Port #4",			"COMPRT4"		},	typUINT8,	0},
		{	{	25,	"25-Start Register #5",			"START5"		},	typUINT16,	0},
		{	{	26,	"26-End Register #5",			"END5"			},	typUINT16,	0},
		{	{	27,	"27-ROC Parameter(s)",			"PARA5"			},	typTLP,	0},
		{	{	28,	"28-Indexing",				"INDEX5"		},	typUINT8,	0},
		{	{	29,	"29-Conversion Code",			"CONV5"			},	typUINT8,	0},
		{	{	30,	"30-Comm Port #5",			"COMPRT5"		},	typUINT8,	0},
		{	{	31,	"31-Start Register #6",			"START6"		},	typUINT16,	0},
		{	{	32,	"32-End Register #6",			"END6"			},	typUINT16,	0},
		{	{	33,	"33-ROC Parameter(s)",			"PARA6"			},	typTLP,	0},
		{	{	34,	"34-Indexing",				"INDEX6"		},	typUINT8,	0},
		{	{	35,	"35-Conversion Code",			"CONV6"			},	typUINT8,	0},
		{	{	36,	"36-Comm Port #6",			"COMPRT6"		},	typUINT8,	0},
		{	{	37,	"37-Start Register #7",			"START7"		},	typUINT16,	0},
		{	{	38,	"38-End Register #7",			"END7"			},	typUINT16,	0},
		{	{	39,	"39-ROC Parameter(s)",			"PARA7"			},	typTLP,	0},
		{	{	40,	"40-Indexing",				"INDEX7"		},	typUINT8,	0},
		{	{	41,	"41-Conversion Code",			"CONV7"			},	typUINT8,	0},
		{	{	42,	"42-Comm Port #7",			"COMPRT7"		},	typUINT8,	0},
		{	{	43,	"43-Start Register #8",			"START8"		},	typUINT16,	0},
		{	{	44,	"44-End Register #8",			"END8"			},	typUINT16,	0},
		{	{	45,	"45-ROC Parameter(s)",			"PARA8"			},	typTLP,	0},
		{	{	46,	"46-Indexing",				"INDEX8"		},	typUINT8,	0},
		{	{	47,	"47-Conversion Code",			"CONV8"			},	typUINT8,	0},
		{	{	48,	"48-Comm Port #8",			"COMPRT8"		},	typUINT8,	0},
		{	{	49,	"49-Start Register #9",			"START9"		},	typUINT16,	0},
		{	{	50,	"50-End Register #9",			"END9"			},	typUINT16,	0},
		{	{	51,	"51-ROC Parameter(s)",			"PARA9"			},	typTLP,	0},
		{	{	52,	"52-Indexing",				"INDEX9"		},	typUINT8,	0},
		{	{	53,	"53-Conversion Code",			"CONV9"			},	typUINT8,	0},
		{	{	54,	"54-Comm Port #9",			"COMPRT9"		},	typUINT8,	0},
		{	{	55,	"55-Start Register #10",		"START10"		},	typUINT16,	0},
		{	{	56,	"56-End Register #10",			"END10"			},	typUINT16,	0},
		{	{	57,	"57-ROC Parameter(s)",			"PARA10"		},	typTLP,	0},
		{	{	58,	"58-Indexing",				"INDEX10"		},	typUINT8,	0},
		{	{	59,	"59-Conversion Code",			"CONV10"		},	typUINT8,	0},
		{	{	60,	"60-Comm Port #10",			"COMPRT10"		},	typUINT8,	0},
		{	{	61,	"61-Start Register #11",		"START11"		},	typUINT16,	0},
		{	{	62,	"62-End Register #11",			"END11"			},	typUINT16,	0},
		{	{	63,	"63-ROC Parameter(s)",			"PARA11"		},	typTLP,	0},
		{	{	64,	"64-Indexing",				"INDEX11"		},	typUINT8,	0},
		{	{	65,	"65-Conversion Code",			"CONV11"		},	typUINT8,	0},
		{	{	66,	"66-Comm Port #11",			"COMPRT11"		},	typUINT8,	0},
		{	{	67,	"67-Start Register #12",		"START12"		},	typUINT16,	0},
		{	{	68,	"68-End Register #12",			"END12"			},	typUINT16,	0},
		{	{	69,	"69-ROC Parameter(s)",			"PARA12"		},	typTLP,	0},
		{	{	70,	"70-Indexing",				"INDEX12"		},	typUINT8,	0},
		{	{	71,	"71-Conversion Code",			"CONV12"		},	typUINT8,	0},
		{	{	72,	"72-Comm Port #12",			"COMPRT12"		},	typUINT8,	0},
		{	{	73,	"73-Start Register #13",		"START13"		},	typUINT16,	0},
		{	{	74,	"74-End Register #13",			"END13"			},	typUINT16,	0},
		{	{	75,	"75-ROC Parameter(s)",			"PARA13"		},	typTLP,	0},
		{	{	76,	"76-Indexing",				"INDEX13"		},	typUINT8,	0},
		{	{	77,	"77-Conversion Code",			"CONV13"		},	typUINT8,	0},
		{	{	78,	"78-Comm Port #13",			"COMPRT13"		},	typUINT8,	0},
		{	{	79,	"79-Start Register #14",		"START14"		},	typUINT16,	0},
		{	{	80,	"80-End Register #14",			"END14"			},	typUINT16,	0},
		{	{	81,	"81-ROC Parameter(s)",			"PARA14"		},	typTLP,	0},
		{	{	82,	"82-Indexing",				"INDEX14"		},	typUINT8,	0},
		{	{	83,	"83-Conversion Code",			"CONV14"		},	typUINT8,	0},
		{	{	84,	"84-Comm Port #14",			"COMPRT14"		},	typUINT8,	0},
		{	{	85,	"85-Start Register #15",		"START15"		},	typUINT16,	0},
		{	{	86,	"86-End Register #15",			"END15"			},	typUINT16,	0},
		{	{	87,	"87-ROC Parameter(s)",			"PARA15"		},	typTLP,	0},
		{	{	88,	"88-Indexing",				"INDEX15"		},	typUINT8,	0},
		{	{	89,	"89-Conversion Code",			"CONV15"		},	typUINT8,	0},
		{	{	90,	"90-Comm Port #15",			"COMPRT15"		},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	119,	// Modbus Event, Alarm, and History
	{
		{	{	0,	"0-Event Alarm Register",		"EVTALRM"		},	typUINT16,	0},
		{	{	1,	"1-Current Date Register",		"CURDATE"		},	typUINT16,	0},
		{	{	2,	"2-Current Time Register",		"CURTIME"		},	typUINT16,	0},
		{	{	3,	"3-Periodic History Register #1",	"PR1"			},	typUINT16,	0},
		{	{	4,	"4-Daily History Register #1",		"DLY1"			},	typUINT16,	0},
		{	{	5,	"5-History Segment",			"HS1"			},	typUINT16,	0},
		{	{	6,	"6-Start History Point",		"START1"		},	typUINT16,	0},
		{	{	7,	"7-End History Point",			"END1"			},	typUINT16,	0},
		{	{	8,	"8-Conversion Code",			"CONV1"			},	typUINT8,	0},
		{	{	9,	"9-Periodic History Register #2",	"PR2"			},	typUINT16,	0},
		{	{	10,	"10-Daily History Register #2",		"DLY2"			},	typUINT16,	0},
		{	{	11,	"11-History Segment",			"HS2"			},	typUINT16,	0},
		{	{	12,	"12-Start History Point",		"START2"		},	typUINT16,	0},
		{	{	13,	"13-End History Point",			"END2"			},	typUINT16,	0},
		{	{	14,	"14-Conversion Code",			"CONV2"			},	typUINT8,	0},
		{	{	15,	"15-Periodic History Register #3",	"PR3"			},	typUINT16,	0},
		{	{	16,	"16-Daily History Register #3",		"DLY3"			},	typUINT16,	0},
		{	{	17,	"17-History Segment",			"HS3"			},	typUINT16,	0},
		{	{	18,	"18-Start History Point",		"START3"		},	typUINT16,	0},
		{	{	19,	"19-End History Point",			"END3"			},	typUINT16,	0},
		{	{	20,	"20-Conversion Code",			"CONV3"			},	typUINT8,	0},
		{	{	21,	"21-Periodic History Register #4",	"PR4"			},	typUINT16,	0},
		{	{	22,	"22-Daily History Register #4",		"DLY4"			},	typUINT16,	0},
		{	{	23,	"23-History Segment",			"HS4"			},	typUINT16,	0},
		{	{	24,	"24-Start History Point",		"START4"		},	typUINT16,	0},
		{	{	25,	"25-End History Point",			"END4"			},	typUINT16,	0},
		{	{	26,	"26-Conversion Code",			"CONV4"			},	typUINT8,	0},
		{	{	27,	"27-Periodic History Register #5",	"PR5"			},	typUINT16,	0},
		{	{	28,	"28-Daily History Register #5",		"DLY5"			},	typUINT16,	0},
		{	{	29,	"29-History Segment",			"HS5"			},	typUINT16,	0},
		{	{	30,	"30-Start History Point",		"START5"		},	typUINT16,	0},
		{	{	31,	"31-End History Point",			"END5"			},	typUINT16,	0},
		{	{	32,	"32-Conversion Code",			"CONV5"			},	typUINT8,	0},
		{	{	33,	"33-Periodic History Register #6",	"PR6"			},	typUINT16,	0},
		{	{	34,	"34-Daily History Register #6",		"DLY6"			},	typUINT16,	0},
		{	{	35,	"35-History Segment",			"HS6"			},	typUINT16,	0},
		{	{	36,	"36-Start History Point",		"START6"		},	typUINT16,	0},
		{	{	37,	"37-End History Point",			"END6"			},	typUINT16,	0},
		{	{	38,	"38-Conversion Code",			"CONV6"			},	typUINT8,	0},
		{	{	39,	"39-Periodic History Register #7",	"PR7"			},	typUINT16,	0},
		{	{	40,	"40-Daily History Register #7",		"DLY7"			},	typUINT16,	0},
		{	{	41,	"41-History Segment",			"HS7"			},	typUINT16,	0},
		{	{	42,	"42-Start History Point",		"START7"		},	typUINT16,	0},
		{	{	43,	"43-End History Point",			"END7"			},	typUINT16,	0},
		{	{	44,	"44-Conversion Code",			"CONV7"			},	typUINT8,	0},
		{	{	45,	"45-Periodic History Register #8",	"PR8"			},	typUINT16,	0},
		{	{	46,	"46-Daily History Register #8",		"DLY8"			},	typUINT16,	0},
		{	{	47,	"47-History Segment",			"HS8"			},	typUINT16,	0},
		{	{	48,	"48-Start History Point",		"START8"		},	typUINT16,	0},
		{	{	49,	"49-End History Point",			"END8"			},	typUINT16,	0},
		{	{	50,	"50-Conversion Code",			"CONV8"			},	typUINT8,	0},
		{	{	51,	"51-Periodic History Register #9",	"PR9"			},	typUINT16,	0},
		{	{	52,	"52-Daily History Register #9",		"DLY9"			},	typUINT16,	0},
		{	{	53,	"53-History Segment",			"HS9"			},	typUINT16,	0},
		{	{	54,	"54-Start History Point",		"START9"		},	typUINT16,	0},
		{	{	55,	"55-End History Point",			"END9"			},	typUINT16,	0},
		{	{	56,	"56-Conversion Code",			"CONV9"			},	typUINT8,	0},
		{	{	57,	"57-Periodic History Register #10",	"PR10"			},	typUINT16,	0},
		{	{	58,	"58-Daily History Register #10",	"DLY10"			},	typUINT16,	0},
		{	{	59,	"59-History Segment",			"HS10"			},	typUINT16,	0},
		{	{	60,	"60-Start History Point",		"START10"		},	typUINT16,	0},
		{	{	61,	"61-End History Point",			"END10"			},	typUINT16,	0},
		{	{	62,	"62-Conversion Code",			"CONV10"		},	typUINT8,	0},
		{	{	63,	"63-Periodic History Register #11",	"PR11"			},	typUINT16,	0},
		{	{	64,	"64-Daily History Register #11",	"DLY11"			},	typUINT16,	0},
		{	{	65,	"65-History Segment",			"HS11"			},	typUINT16,	0},
		{	{	66,	"66-Start History Point",		"START11"		},	typUINT16,	0},
		{	{	67,	"67-End History Point",			"END11"			},	typUINT16,	0},
		{	{	68,	"68-Conversion Code",			"CONV11"		},	typUINT8,	0},
		{	{	69,	"69-Periodic History Register #12",	"PR12"			},	typUINT16,	0},
		{	{	70,	"70-Daily History Register #12",	"DLY12"			},	typUINT16,	0},
		{	{	71,	"71-History Segment",			"HS12"			},	typUINT16,	0},
		{	{	72,	"72-Start History Point",		"START12"		},	typUINT16,	0},
		{	{	73,	"73-End History Point",			"END12"			},	typUINT16,	0},
		{	{	74,	"74-Conversion Code",			"CONV12"		},	typUINT8,	0},
		{	{	75,	"75-Periodic History Register #13",	"PR13"			},	typUINT16,	0},
		{	{	76,	"76-Daily History Register #13",	"DLY13"			},	typUINT16,	0},
		{	{	77,	"77-History Segment",			"HS13"			},	typUINT16,	0},
		{	{	78,	"78-Start History Point",		"START13"		},	typUINT16,	0},
		{	{	79,	"79-End History Point",			"END13"			},	typUINT16,	0},
		{	{	80,	"80-Conversion Code",			"CONV13"		},	typUINT8,	0},
		{	{	81,	"81-Periodic History Register #14",	"PR14"			},	typUINT16,	0},
		{	{	82,	"82-Daily History Register #14",	"DLY14"			},	typUINT16,	0},
		{	{	83,	"83-History Segment",			"HS14"			},	typUINT16,	0},
		{	{	84,	"84-Start History Point",		"START14"		},	typUINT16,	0},
		{	{	85,	"85-End History Point",			"END14"			},	typUINT16,	0},
		{	{	86,	"86-Conversion Code",			"CONV14"		},	typUINT8,	0},
		{	{	87,	"87-Periodic History Register #15",	"PR15"			},	typUINT16,	0},
		{	{	88,	"88-Daily History Register #15",	"DLY15"			},	typUINT16,	0},
		{	{	89,	"89-History Segment",			"HS15"			},	typUINT16,	0},
		{	{	90,	"90-Start History Point",		"START15"		},	typUINT16,	0},
		{	{	91,	"91-End History Point",			"END15"			},	typUINT16,	0},
		{	{	92,	"92-Conversion Code",			"CONV15"		},	typUINT8,	0},
		{	{	93,	"93-Periodic History Register #16",	"PR16"			},	typUINT16,	0},
		{	{	94,	"94-Daily History Register #16",	"DLY16"			},	typUINT16,	0},
		{	{	95,	"95-History Segment",			"HS16"			},	typUINT16,	0},
		{	{	96,	"96-Start History Point",		"START16"		},	typUINT16,	0},
		{	{	97,	"97-End History Point",			"END16"			},	typUINT16,	0},
		{	{	98,	"98-Conversion Code",			"CONV16"		},	typUINT8,	0},
		{	{	99,	"99-Periodic History Register #17",	"PR17"			},	typUINT16,	0},
		{	{       100,	"100-Daily History Register #17",	"DLY17"			},	typUINT16,	0},
		{	{       101,	"101-History Segment",			"HS17"			},	typUINT16,	0},
		{	{	102,	"102-Start History Point",		"START17"		},	typUINT16,	0},
		{	{	103,	"103-End History Point",		"END17"			},	typUINT16,	0},
		{	{	104,	"104-Conversion Code",			"CONV17"		},	typUINT8,	0},
		{	{	105,	"105-Periodic History Register #18",	"PR18"			},	typUINT16,	0},
		{	{	106,	"106-Daily History Register #18",	"DLY18"			},	typUINT16,	0},
		{	{	107,	"107-History Segment",			"HS18"			},	typUINT16,	0},
		{	{	108,	"108-Start History Point",		"START18"		},	typUINT16,	0},
		{	{	109,	"109-End History Point",		"END18"			},	typUINT16,	0},
		{	{	110,	"110-Conversion Code",			"CONV18"		},	typUINT8,	0},
		{	{	111,	"111-Periodic History Register #19",	"PR19"			},	typUINT16,	0},
		{	{       112,	"112-Daily History Register #19",	"DLY19"			},	typUINT16,	0},
		{	{       113,	"113-History Segment",			"HS19"			},	typUINT16,	0},
		{	{	114,	"114-Start History Point",		"START19"		},	typUINT16,	0},
		{	{	115,	"115-End History Point",		"END19"			},	typUINT16,	0},
		{	{	116,	"116-Conversion Code",			"CONV19"		},	typUINT8,	0},
		{	{	117,	"117-Periodic History Register #20",	"PR20"			},	typUINT16,	0},
		{	{	118,	"118-Daily History Register #20",	"DLY20"			},	typUINT16,	0},
		{	{	119,	"119-History Segment",			"HS20"			},	typUINT16,	0},
		{	{	120,	"120-Start History Point",		"START20"		},	typUINT16,	0},
		{	{	121,	"121-End History Point",		"END20"			},	typUINT16,	0},
		{	{	122,	"122-Conversion Code",			"CONV20"		},	typUINT8,	0},
		{	{	123,	"123-History Index Mode",		"HNDXMODE"		},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	120,	// Modbus Master Modem Configuration
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-1st RTU Address",			"ADD1"			},	typUINT8,	0},
		{	{	2,	"2-1st Connect Command",		"CMD1"			},	typStr30,	0},
		{	{	3,	"3-2nd RTU Address",			"ADD2"			},	typUINT8,	0},
		{	{	4,	"4-2nd Connect Command",		"CMD2"			},	typStr30,	0},
		{	{	5,	"5-3rd RTU Address",			"ADD3"			},	typUINT8,	0},
		{	{	6,	"6-3rd Connect Command",		"CMD3"			},	typStr30,	0},
		{	{	7,	"7-4th RTU Address",			"ADD4"			},	typUINT8,	0},
		{	{	8,	"8-4th Connect Command",		"CMD4"			},	typStr30,	0},
		{	{	9,	"9-5th RTU Address",			"ADD5"			},	typUINT8,	0},
		{	{	10,	"10-5th Connect Command",		"CMD5"			},	typStr30,	0},
		{	{	11,	"11-6th RTU Address",			"ADD6"			},	typUINT8,	0},
		{	{	12,	"12-6th Connect Command",		"CMD6"			},	typStr30,	0},		
		{	{	0,	"",					""			},	paramNone,	0},
		},
	121,	// Modbus Master Table
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-RTU 1 Address",			"ADD1"			},	typUINT8,	0},
		{	{	2,	"2-Function Code Number",		"FUNC1"			},	typUINT8,	0},
		{	{	3,	"3-Slave Register Number",		"SLVREG1"		},	typUINT16,	0},
		{	{	4,	"4-Master Register Number",		"MSTREG1"		},	typUINT16,	0},
		{	{	5,	"5-Number of Registers",		"NUMREG1"		},	typUINT8,	0},
		{	{	6,	"6-Communication Status",		"STATUS1"		},	typUINT8,	0},
		{	{	7,	"7-RTU 2 Address",			"ADD2"			},	typUINT8,	0},
		{	{	8,	"8-Function Code Number",		"FUNC2"			},	typUINT8,	0},
		{	{	9,	"9-Slave Register Number",		"SLVREG2"		},	typUINT16,	0},
		{	{	10,	"10-Master Register Number",		"MSTREG2"		},	typUINT16,	0},
		{	{	11,	"11-Number of Registers",		"NUMREG2"		},	typUINT8,	0},
		{	{	12,	"12-Communication Status",		"STATUS2"		},	typUINT8,	0},
		{	{	13,	"13-RTU 3 Address",			"ADD3"			},	typUINT8,	0},
		{	{	14,	"14-Function Code Number",		"FUNC3"			},	typUINT8,	0},
		{	{	15,	"15-Slave Register Number",		"SLVREG3"		},	typUINT16,	0},
		{	{	16,	"16-Master Register Number",		"MSTREG3"		},	typUINT16,	0},
		{	{	17,	"17-Number of Registers",		"NUMREG3"		},	typUINT8,	0},
		{	{	18,	"18-Communication Status",		"STATUS3"		},	typUINT8,	0},
		{	{	19,	"19-RTU 4 Address",			"ADD4"			},	typUINT8,	0},
		{	{	20,	"20-Function Code Number",		"FUNC4"			},	typUINT8,	0},
		{	{	21,	"21-Slave Register Number",		"SLVREG4"		},	typUINT16,	0},
		{	{	22,	"22-Master Register Number",		"MSTREG4"		},	typUINT16,	0},
		{	{	23,	"23-Number of Registers",		"NUMREG4"		},	typUINT8,	0},
		{	{	24,	"24-Communication Status",		"STATUS4"		},	typUINT8,	0},
		{	{	25,	"25-RTU 5 Address",			"ADD5"			},	typUINT8,	0},
		{	{	26,	"26-Function Code Number",		"FUNC5"			},	typUINT8,	0},
		{	{	27,	"27-Slave Register Number",		"SLVREG5"		},	typUINT16,	0},
		{	{	28,	"28-Master Register Number",		"MSTREG5"		},	typUINT16,	0},
		{	{	29,	"29-Number of Registers",		"NUMREG5"		},	typUINT8,	0},
		{	{	30,	"30-Communication Status",		"STATUS5"		},	typUINT8,	0},
		{	{	31,	"31-RTU 6 Address",			"ADD6"			},	typUINT8,	0},
		{	{	32,	"32-Function Code Number",		"FUNC6"			},	typUINT8,	0},
		{	{	33,	"33-Slave Register Number",		"SLVREG6"		},	typUINT16,	0},
		{	{	34,	"34-Master Register Number",		"MSTREG6"		},	typUINT16,	0},
		{	{	35,	"35-Number of Registers",		"NUMREG6"		},	typUINT8,	0},
		{	{	36,	"36-Communication Status",		"STATUS6"		},	typUINT8,	0},
		{	{	37,	"37-RTU 7 Address",			"ADD7"			},	typUINT8,	0},
		{	{	38,	"38-Function Code Number",		"FUNC7"			},	typUINT8,	0},
		{	{	39,	"39-Slave Register Number",		"SLVREG7"		},	typUINT16,	0},
		{	{	40,	"40-Master Register Number",		"MSTREG7"		},	typUINT16,	0},
		{	{	41,	"41-Number of Registers",		"NUMREG7"		},	typUINT8,	0},
		{	{	42,	"42-Communication Status",		"STATUS7"		},	typUINT8,	0},
		{	{	43,	"43-RTU 8 Address",			"ADD8"			},	typUINT8,	0},
		{	{	44,	"44-Function Code Number",		"FUNC8"			},	typUINT8,	0},
		{	{	45,	"45-Slave Register Number",		"SLVREG8"		},	typUINT16,	0},
		{	{	46,	"46-Master Register Number",		"MSTREG8"		},	typUINT16,	0},
		{	{	47,	"47-Number of Registers",		"NUMREG8"		},	typUINT8,	0},
		{	{	48,	"48-Communication Status",		"STATUS8"		},	typUINT8,	0},
		{	{	49,	"49-RTU 9 Address",			"ADD9"			},	typUINT8,	0},
		{	{	50,	"50-Function Code Number",		"FUNC9"			},	typUINT8,	0},
		{	{	51,	"51-Slave Register Number",		"SLVREG9"		},	typUINT16,	0},
		{	{	52,	"52-Master Register Number",		"MSTREG9"		},	typUINT16,	0},
		{	{	53,	"53-Number of Registers",		"NUMREG9"		},	typUINT8,	0},
		{	{	54,	"54-Communication Status",		"STATUS9"		},	typUINT8,	0},
		{	{	55,	"55-RTU 10 Address",			"ADD10"			},	typUINT8,	0},
		{	{	56,	"56-Function Code Number",		"FUNC10"		},	typUINT8,	0},
		{	{	57,	"57-Slave Register Number",		"SLVREG10"		},	typUINT16,	0},
		{	{	58,	"58-Master Register Number",		"MSTREG10"		},	typUINT16,	0},
		{	{	59,	"59-Number of Registers",		"NUMREG10"		},	typUINT8,	0},
		{	{	60,	"60-Communication Status",		"STATUS10"		},	typUINT8,	0},
		{	{	61,	"61-RTU 11 Address",			"ADD11"			},	typUINT8,	0},
		{	{	62,	"62-Function Code Number",		"FUNC11"		},	typUINT8,	0},
		{	{	63,	"63-Slave Register Number",		"SLVREG11"		},	typUINT16,	0},
		{	{	64,	"64-Master Register Number",		"MSTREG11"		},	typUINT16,	0},
		{	{	65,	"65-Number of Registers",		"NUMREG11"		},	typUINT8,	0},
		{	{	66,	"66-Communication Status",		"STATUS11"		},	typUINT8,	0},
		{	{	67,	"67-RTU 12 Address",			"ADD12"			},	typUINT8,	0},
		{	{	68,	"68-Function Code Number",		"FUNC12"		},	typUINT8,	0},
		{	{	69,	"69-Slave Register Number",		"SLVREG12"		},	typUINT16,	0},
		{	{	70,	"70-Master Register Number",		"MSTREG12"		},	typUINT16,	0},
		{	{	71,	"71-Number of Registers",		"NUMREG12"		},	typUINT8,	0},
		{	{	72,	"72-Communication Status",		"STATUS12"		},	typUINT8,	0},
		{	{	73,	"73-RTU 13 Address",			"ADD13"			},	typUINT8,	0},
		{	{	74,	"74-Function Code Number",		"FUNC13"		},	typUINT8,	0},
		{	{	75,	"75-Slave Register Number",		"SLVREG13"		},	typUINT16,	0},
		{	{	76,	"76-Master Register Number",		"MSTREG13"		},	typUINT16,	0},
		{	{	77,	"77-Number of Registers",		"NUMREG13"		},	typUINT8,	0},
		{	{	78,	"78-Communication Status",		"STATUS13"		},	typUINT8,	0},
		{	{	79,	"79-RTU 14 Address",			"ADD14"			},	typUINT8,	0},
		{	{	80,	"80-Function Code Number",		"FUNC14"		},	typUINT8,	0},
		{	{	81,	"81-Slave Register Number",		"SLVREG14"		},	typUINT16,	0},
		{	{	82,	"82-Master Register Number",		"MSTREG14"		},	typUINT16,	0},
		{	{	83,	"83-Number of Registers",		"NUMREG14"		},	typUINT8,	0},
		{	{	84,	"84-Communication Status",		"STATUS14"		},	typUINT8,	0},
		{	{	85,	"85-RTU 15 Address",			"ADD15"			},	typUINT8,	0},
		{	{	86,	"86-Function Code Number",		"FUNC15"		},	typUINT8,	0},
		{	{	87,	"87-Slave Register Number",		"SLVREG15"		},	typUINT16,	0},
		{	{	88,	"88-Master Register Number",		"MSTREG15"		},	typUINT16,	0},
		{	{	89,	"89-Number of Registers",		"NUMREG15"		},	typUINT8,	0},
		{	{	90,	"90-Communication Status",		"STATUS15"		},	typUINT8,	0},
		{	{	91,	"91-RTU 16 Address",			"ADD16"			},	typUINT8,	0},
		{	{	92,	"92-Function Code Number",		"FUNC16"		},	typUINT8,	0},
		{	{	93,	"93-Slave Register Number",		"SLVREG16"		},	typUINT16,	0},
		{	{	94,	"94-Master Register Number",		"MSTREG16"		},	typUINT16,	0},
		{	{	95,	"95-Number of Registers",		"NUMREG16"		},	typUINT8,	0},
		{	{	96,	"96-Communication Status",		"STATUS16"		},	typUINT8,	0},
		{	{	97,	"97-RTU 17 Address",			"ADD17"			},	typUINT8,	0},
		{	{	98,	"98-Function Code Number",		"FUNC17"		},	typUINT8,	0},
		{	{	99,	"99-Slave Register Number",		"SLVREG17"		},	typUINT16,	0},
		{	{	100,	"100-Master Register Number",		"MSTREG17"		},	typUINT16,	0},
		{	{	101,	"101-Number of Registers",		"NUMREG17"		},	typUINT8,	0},
		{	{	102,	"102-Communication Status",		"STATUS17"		},	typUINT8,	0},
		{	{	103,	"103-RTU 18 Address",			"ADD18"			},	typUINT8,	0},
		{	{	104,	"104-Function Code Number",		"FUNC18"		},	typUINT8,	0},
		{	{	105,	"105-Slave Register Number",		"SLVREG18"		},	typUINT16,	0},
		{	{	106,	"106-Master Register Number",		"MSTREG18"		},	typUINT16,	0},
		{	{	107,	"107-Number of Registers",		"NUMREG18"		},	typUINT8,	0},
		{	{	108,	"108-Communication Status",		"STATUS18"		},	typUINT8,	0},
		{	{	109,	"109-RTU 19 Address",			"ADD19"			},	typUINT8,	0},
		{	{	110,	"110-Function Code Number",		"FUNC19"		},	typUINT8,	0},
		{	{	111,	"111-Slave Register Number",		"SLVREG19"		},	typUINT16,	0},
		{	{	112,	"112-Master Register Number",		"MSTREG19"		},	typUINT16,	0},
		{	{	113,	"113-Number of Registers",		"NUMREG19"		},	typUINT8,	0},
		{	{	114,	"114-Communication Status",		"STATUS19"		},	typUINT8,	0},
		{	{	115,	"115-RTU 20 Address",			"ADD20"			},	typUINT8,	0},
		{	{	116,	"116-Function Code Number",		"FUNC20"		},	typUINT8,	0},
		{	{	117,	"117-Slave Register Number",		"SLVREG20"		},	typUINT16,	0},
		{	{	118,	"118-Master Register Number",		"MSTREG20"		},	typUINT16,	0},
		{	{	119,	"119-Number of Registers",		"NUMREG20"		},	typUINT8,	0},
		{	{	120,	"120-Communication Status",		"STATUS20"		},	typUINT8,	0},
		{	{	121,	"121-RTU 21 Address",			"ADD21"			},	typUINT8,	0},
		{	{	122,	"122-Function Code Number",		"FUNC21"		},	typUINT8,	0},
		{	{	123,	"123-Slave Register Number",		"SLVREG21"		},	typUINT16,	0},
		{	{	124,	"124-Master Register Number",		"MSTREG21"		},	typUINT16,	0},
		{	{	125,	"125-Number of Registers",		"NUMREG21"		},	typUINT8,	0},
		{	{	126,	"126-Communication Status",		"STATUS21"		},	typUINT8,	0},
		{	{	127,	"127-RTU 22 Address",			"ADD22"			},	typUINT8,	0},
		{	{	128,	"128-Function Code Number",		"FUNC22"		},	typUINT8,	0},
		{	{	129,	"129-Slave Register Number",		"SLVREG22"		},	typUINT16,	0},
		{	{	130,	"130-Master Register Number",		"MSTREG22"		},	typUINT16,	0},
		{	{	131,	"131-Number of Registers",		"NUMREG22"		},	typUINT8,	0},
		{	{	132,	"132-Communication Status",		"STATUS22"		},	typUINT8,	0},
		{	{	133,	"133-RTU 23 Address",			"ADD23"			},	typUINT8,	0},
		{	{	134,	"134-Function Code Number",		"FUNC23"		},	typUINT8,	0},
		{	{	135,	"135-Slave Register Number",		"SLVREG23"		},	typUINT16,	0},
		{	{	136,	"136-Master Register Number",		"MSTREG23"		},	typUINT16,	0},
		{	{	137,	"137-Number of Registers",		"NUMREG23"		},	typUINT8,	0},
		{	{	138,	"138-Communication Status",		"STATUS23"		},	typUINT8,	0},
		{	{	139,	"139-RTU 24 Address",			"ADD24"			},	typUINT8,	0},
		{	{	140,	"140-Function Code Number",		"FUNC24"		},	typUINT8,	0},
		{	{	141,	"141-Slave Register Number",		"SLVREG24"		},	typUINT16,	0},
		{	{	142,	"142-Master Register Number",		"MSTREG24"		},	typUINT16,	0},
		{	{	143,	"143-Number of Registers",		"NUMREG24"		},	typUINT8,	0},
		{	{	144,	"144-Communication Status",		"STATUS24"		},	typUINT8,	0},
		{	{	145,	"145-RTU 25 Address",			"ADD25"			},	typUINT8,	0},
		{	{	146,	"146-Function Code Number",		"FUNC25"		},	typUINT8,	0},
		{	{	147,	"147-Slave Register Number",		"SLVREG25"		},	typUINT16,	0},
		{	{	148,	"148-Master Register Number",		"MSTREG25"		},	typUINT16,	0},
		{	{	149,	"149-Number of Registers",		"NUMREG25"		},	typUINT8,	0},
		{	{	150,	"150-Communication Status",		"STATUS25"		},	typUINT8,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	122,	// DS800 Configuration
	{
		{	{	0,	"0-Power Switch",			"POWER"			},	typUINT8,	0},
		{	{	1,	"1-RSI Enable",				"RSIEN"			},	typUINT8,	0},
		{	{	2,	"2-ETCP Enable",			"ETCPEN"		},	typUINT8,	0},
		{	{	3,	"3-IXD Enable",				"IXDEN"			},	typUINT8,	0},
		{	{	4,	"4-RSI Running",			"RSIRUN"		},	typUINT8,	1},
		{	{	5,	"5-ETCP Running",			"ETCPRUN"		},	typUINT8,	1},
		{	{	6,	"6-IXD Running",			"IXDRUN"		},	typUINT8,	1},
		{	{	7,	"7-Clean Stored Resources",		"CLRRES"		},	typUINT8,	0},
		{	{	8,	"8-Resource 1 Name",			"RESNAME1"		},	typStr20,	1},
		{	{	9,	"9-Resource 1 Status",			"RESSTAT1"		},	typUINT8,	1},
		{	{      10,	"10-Resource 1 Programmed Cycle Time",	"DEFTIME1"		},	typUINT32,	1},
		{	{      11,	"11-Resource 1 Current Cycle Time",	"ACTTIME1"		},	typUINT32,	1},
		{	{      12,	"12-Resource 1 Name",			"RESNAME2"		},	typStr20,	1},
		{	{      13,	"13-Resource 1 Status",			"RESSTAT2"		},	typUINT8,	1},
		{	{      14,	"14-Resource 1 Programmed Cycle Time",	"DEFTIME2"		},	typUINT32,	1},
		{	{      15,	"15-Resource 1 Current Cycle Time",	"ACTTIME2"		},	typUINT32,	1},
		{	{      16,	"16-Resource 1 Name",			"RESNAME3"		},	typStr20,	1},
		{	{      17,	"17-Resource 1 Status",			"RESSTAT3"		},	typUINT8,	1},
		{	{      18,	"18-Resource 1 Programmed Cycle Time",	"DEFTIME3"		},	typUINT32,	1},
		{	{      19,	"19-Resource 1 Current Cycle Time",	"ACTTIME3"		},	typUINT32,	1},
		{	{      20,	"20-Resource 1 Name",			"RESNAME4"		},	typStr20,	1},
		{	{      21,	"21-Resource 1 Status",			"RESSTAT4"		},	typUINT8,	1},
		{	{      22,	"22-Resource 1 Programmed Cycle Time",	"DEFTIME4"		},	typUINT32,	1},
		{	{      23,	"23-Resource 1 Current Cycle Time",	"ACTTIME4"		},	typUINT32,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	123,	// Security - Group Configuration
	{
		{	{	0,	"0-Group #1",				"SECGRP1"		},	typStr20,	0},
		{	{	1,	"1-Group #2",				"SECGRP2"		},	typStr20,	0},
		{	{	2,	"2-Group #3",				"SECGRP3"		},	typStr20,	0},
		{	{	3,	"3-Group #4",				"SECGRP4"		},	typStr20,	0},
		{	{	4,	"4-Group #5",				"SECGRP5"		},	typStr20,	0},
		{	{	5,	"5-Group #6",				"SECGRP6"		},	typStr20,	0},
		{	{	6,	"6-Group #7",				"SECGRP7"		},	typStr20,	0},
		{	{	7,	"7-Group #8",				"SECGRP8"		},	typStr20,	0},
		{	{	8,	"8-Group #9",				"SECGRP9"		},	typStr20,	0},
		{	{	9,	"9-Group #10",				"SECGRP10"		},	typStr20,	0},
		{	{	10,	"10-Group #11",				"SECGRP11"		},	typStr20,	0},
		{	{	11,	"11-Group #12",				"SECGRP12"		},	typStr20,	0},
		{	{	12,	"12-Group #13",				"SECGRP13"		},	typStr20,	0},
		{	{	13,	"13-Group #14",				"SECGRP14"		},	typStr20,	0},
		{	{	14,	"14-Group #15",				"SECGRP15"		},	typStr20,	0},
		{	{	15,	"15-Group #16",				"SECGRP16"		},	typStr20,	0},
		{	{	16,	"16-Group #17",				"SECGRP17"		},	typStr20,	0},
		{	{	17,	"17-Group #18",				"SECGRP18"		},	typStr20,	0},
		{	{	18,	"18-Group #19",				"SECGRP19"		},	typStr20,	0},
		{	{	19,	"19-Group #20",				"SECGRP20"		},	typStr20,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	124,	// History Segment Configuration
	{
		{	{	0,	"0-Segment Description",		"DESC"			},	typStr10,	0},
		{	{	1,	"1-Segment Size",			"SIZE"			},	typUINT16,	0},
		{	{	2,	"2-Maximum Segment Size",		"MAXSIZE"		},	typUINT16,	1},
		{	{	3,	"3-Periodic Entries",			"ENTRIES"		},	typUINT16,	0},
		{	{	4,	"4-Daily Entries",			"DAILY"			},	typUINT16,	0},
		{	{	5,	"5-Periodic Index",			"PERINDEX"		},	typUINT16,	1},
		{	{	6,	"6-Daily Index",			"DAYINDEX"		},	typUINT16,	1},
		{	{	7,	"7-Periodic Sample Rate",		"PERRATE"		},	typUINT8,	0},
		{	{	8,	"8-Contract Hour",			"CHOUR"			},	typUINT8,	0},
		{	{	9,	"9-ON OFF Switch",			"ENABLE"		},	typUINT8,	0},
		{	{      10,	"10-Free Space",			"FREE"			},	typUINT32,	1},
		{	{      11,	"11-Force End of Day",			"FORCE"			},	typUINT8,	0},
		{	{      12,	"12-Number of Configured Points",	"NCFGPTS"		},	typUINT16,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	125,	// History Segment 0 Point Configuration
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	1},
		{	{	1,	"1-Parameter Description",		"DESC"			},	typStr10,	0},
		{	{	2,	"2-History Log Point",			"POINT"			},	typTLP,		0},
		{	{	3,	"3-Archive Type",			"TYPE"			},	typUINT8,	0},
		{	{	4,	"4-Averaging Rate Type",		"METHOD"		},	typUINT8,	0},
		{	{	5,	"5-Current Value",			"CURVAL"		},	typReal,	1},
		{	{	6,	"6-Last Daily Value",			"YTDVAL"		},	typReal,	1},
		{	{	7,	"7-Today Min Time",			"TDYMINTM"		},	typTime,	1},	// TIME
		{	{	8,	"8-Today Min Value",			"TDYMINVA"		},	typReal,	1},
		{	{	9,	"9-Today Max Time",			"TDYMAXTM"		},	typTime,	1},	// TIME
		{	{      10,	"10-Today Max Value",			"TDYMAXVA"		},	typReal,	1},
		{	{      11,	"11-Yesterday Min Time",		"YDYMINTM"		},	typTime,	1},	// TIME
		{	{      12,	"12-Yesterday Min Value",		"YDYMINVA"		},	typReal,	1},
		{	{      13,	"13-Yesterday Max Time",		"YDYMAXTM"		},	typTime,	1},	// TIME
		{	{      14,	"14-Yesterday Max Value",		"YDYMAXVA"		},	typReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	136,	// ROC Clock
	{
		{	{	0,	"0-Seconds",				"SECOND"		},	typUINT8,	1},
		{	{	1,	"1-Minutes",				"MINUTE"		},	typUINT8,	1},	
		{	{	2,	"2-Hours",				"HOUR"			},	typUINT8,	1},
		{	{	3,	"3-Day",				"DAY"			},	typUINT8,	1},
		{	{	4,	"4-Month",				"MONTH"			},	typUINT8,	1},
		{	{	5,	"5-Year",				"YEAR"			},	typUINT16,	1},
		{	{	6,	"6-Day of Week",			"DAYOWK"		},	typUINT8,	1},
		{	{	7,	"7-Time",				"TIME"			},	typTime,	1},	// TIME
		{	{	8,      "8-Daylight Savings Enable",		"DLST"			},	typUINT8,	0},
		{	{	9,      "9-Microseconds",			"MICROSEC"		},	typUINT32,	1},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	137,	// Internet Configuration Parameters
	{
		{	{	0,	"0-MAC Address",			"MAC"			},	typStr12,	1},
		{	{	1,	"1-IP Address",				"IP"			},	typStr20,	0},
		{	{	2,	"2-Subnet Mask",			"SUB"			},	typStr20,	0},
		{	{	3,	"3-Gateway Address",			"GATE"			},	typStr20,	0},
		{	{	4,	"4-ROC Protocol IP Port Number",	"PORT"			},	typUINT16,	0},
		{	{	5,	"5-Num Active ROC Protocol Connections","CONNS"			},	typUINT8,	1},
		{	{	6,	"6-ROC Protocol Inactivity Time",	"INACTM"		},	typReal,	0},
		{	{	7,	"7-Reset ROC Protocol Connections",	"RESETCON"		},	typUINT8,	0},
		{	{	8,	"8-ROC Protocol Keep Alive Time",	"ALIVETM"		},	typUINT32,	0},
		{	{	9,	"9-Modbus IP Port Number",		"MBIPPORT"		},	typUINT16,	0},
		{	{      10,	"10-Current Modbus Connections",	"MBCURCON"		},	typUINT16,	1},
		{	{      11,	"11-Modbus Inactivity Time",		"MBINACTM"		},	typReal,	0},
		{	{      12,	"12-Reset Modbus Connections",		"MBCONRES"		},	typUINT8,	0},
		{	{      13,	"13-Modbus Keep Alive Time",		"MBALIVTM"		},	typUINT32,	0},
		{	{      14,	"14-Modbus Over TCP Address To Use",	"MBTCPUSE"		},	typUINT8,	0},
		{	{      15,	"15-Modbus Over TCP Slave Address",	"MBTCPSLV"		},	typUINT8,	0},
		{	{      16,	"16-ARP Protection Enable",		"ARPSTORM"		},	typUINT8,	0},
		{	{      17,	"17-ARP Packet Queue Limit",		"ARPQLIM"		},	typUINT32,	0},
		{	{      18,	"18-Table 1 TCP Option",		"T1TCPOP"		},	typUINT8,	0},
		{	{      19,	"19-Table 1 Server 1 IP Address",	"T1S1IP"		},	typUINT32,	0},
		{	{      20,	"20-Table 1 Server 1 Port Number",	"T1S1PRT"		},	typUINT16,	0},
		{	{      21,	"21-Table 1 Server 2 IP Address",	"T1S2IP"		},	typUINT32,	0},
		{	{      22,	"22-Table 1 Server 2 Port Number",	"T1S2PRT"		},	typUINT16,	0},
		{	{      23,	"23-Table 1 Server 3 IP Address",	"T1S3IP"		},	typUINT32,	0},
		{	{      24,	"24-Table 1 Server 3 Port Number",	"T1S3PRT"		},	typUINT16,	0},
		{	{      25,	"25-Table 1 Server 4 IP Address",	"T1S4IP"		},	typUINT32,	0},
		{	{      26,	"26-Table 1 Server 4 Port Number",	"T1S4PRT"		},	typUINT16,	0},
		{	{      27,	"27-Table 1 Server 5 IP Address",	"T1S5IP"		},	typUINT32,	0},
		{	{      28,	"28-Table 1 Server 5 Port Number",	"T1S5PRT"		},	typUINT16,	0},
		{	{      29,	"29-Table 1 Server 6 IP Address",	"T1S6IP"		},	typUINT32,	0},
		{	{      30,	"30-Table 1 Server 6 Port Number",	"T1S6PRT"		},	typUINT16,	0},
		{	{      31,	"31-Table 1 Server 7 IP Address",	"T1S7IP"		},	typUINT32,	0},
		{	{      32,	"32-Table 1 Server 7 Port Number",	"T1S7PRT"		},	typUINT16,	0},
		{	{      33,	"33-Table 1 Server 8 IP Address",	"T1S8IP"		},	typUINT32,	0},
		{	{      34,	"34-Table 1 Server 8 Port Number",	"T1S8PRT"		},	typUINT16,	0},
		{	{      35,	"35-Table 1 Server 9 IP Address",	"T1S9IP"		},	typUINT32,	0},
		{	{      36,	"36-Table 1 Server 9 Port Number",	"T1S9PRT"		},	typUINT16,	0},
		{	{      37,	"37-Table 1 Server 10 IP Address",	"T1S10IP"		},	typUINT32,	0},
		{	{      38,	"38-Table 1 Server 10 Port Number",	"T1S10PRT"		},	typUINT16,	0},
		{	{      39,	"39-Table 1 Server 11 IP Address",	"T1S11IP"		},	typUINT32,	0},
		{	{      40,	"40-Table 1 Server 11 Port Number",	"T1S11PRT"		},	typUINT16,	0},
		{	{      41,	"41-Table 1 Server 12 IP Address",	"T1S12IP"		},	typUINT32,	0},
		{	{      42,	"42-Table 1 Server 12 Port Number",	"T1S12PRT"		},	typUINT16,	0},
		{	{      43,	"43-Table 1 Server 13 IP Address",	"T1S13IP"		},	typUINT32,	0},
		{	{      44,	"44-Table 1 Server 13 Port Number",	"T1S13PRT"		},	typUINT16,	0},
		{	{      45,	"45-Table 1 Server 14 IP Address",	"T1S14IP"		},	typUINT32,	0},
		{	{      46,	"46-Table 1 Server 14 Port Number",	"T1S14PRT"		},	typUINT16,	0},
		{	{      47,	"47-Table 1 Server 15 IP Address",	"T1S15IP"		},	typUINT32,	0},
		{	{      48,	"48-Table 1 Server 15 Port Number",	"T1S15PRT"		},	typUINT16,	0},
		{	{      49,	"49-Table 1 Server 16 IP Address",	"T1S16IP"		},	typUINT32,	0},
		{	{      50,	"50-Table 1 Server 16 Port Number",	"T1S16PRT"		},	typUINT16,	0},
		{	{      51,	"51-Table 1 Server 17 IP Address",	"T1S17IP"		},	typUINT32,	0},
		{	{      52,	"52-Table 1 Server 17 Port Number",	"T1S17PRT"		},	typUINT16,	0},
		{	{      53,	"53-Table 1 Server 18 IP Address",	"T1S18IP"		},	typUINT32,	0},
		{	{      54,	"54-Table 1 Server 18 Port Number",	"T1S18PRT"		},	typUINT16,	0},
		{	{      55,	"55-Table 1 Server 19 IP Address",	"T1S19IP"		},	typUINT32,	0},
		{	{      56,	"56-Table 1 Server 19 Port Number",	"T1S19PRT"		},	typUINT16,	0},
		{	{      57,	"57-Table 1 Server 20 IP Address",	"T1S20IP"		},	typUINT32,	0},
		{	{      58,	"58-Table 1 Server 20 Port Number",	"T1S20PRT"		},	typUINT16,	0},
		{	{      59,	"59-Table 1 Server 21 IP Address",	"T1S21IP"		},	typUINT32,	0},
		{	{      60,	"60-Table 1 Server 21 Port Number",	"T1S21PRT"		},	typUINT16,	0},
		{	{      61,	"61-Table 1 Server 22 IP Address",	"T1S22IP"		},	typUINT32,	0},
		{	{      62,	"62-Table 1 Server 22 Port Number",	"T1S22PRT"		},	typUINT16,	0},
		{	{      63,	"63-Table 1 Server 23 IP Address",	"T1S23IP"		},	typUINT32,	0},
		{	{      64,	"64-Table 1 Server 23 Port Number",	"T1S23PRT"		},	typUINT16,	0},
		{	{      65,	"65-Table 1 Server 24 IP Address",	"T1S24IP"		},	typUINT32,	0},
		{	{      66,	"66-Table 1 Server 24 Port Number",	"T1S24PRT"		},	typUINT16,	0},
		{	{      67,	"67-Table 1 Server 25 IP Address",	"T1S25IP"		},	typUINT32,	0},
		{	{      68,	"68-Table 1 Server 25 Port Number",	"T1S25PRT"		},	typUINT16,	0},
		{	{      69,	"69-Table 2 TCP Option",		"T2TCPOP"		},	typUINT8,	0},
		{	{      70,	"70-Table 2 Server 1 IP Address",	"T2S1IP"		},	typUINT32,	0},
		{	{      71,	"71-Table 2 Server 1 Port Number",	"T2S1PRT"		},	typUINT16,	0},
		{	{      72,	"72-Table 2 Server 2 IP Address",	"T2S2IP"		},	typUINT32,	0},
		{	{      73,	"73-Table 2 Server 2 Port Number",	"T2S2PRT"		},	typUINT16,	0},
		{	{      74,	"74-Table 2 Server 3 IP Address",	"T2S3IP"		},	typUINT32,	0},
		{	{      75,	"75-Table 2 Server 3 Port Number",	"T2S3PRT"		},	typUINT16,	0},
		{	{      76,	"76-Table 2 Server 4 IP Address",	"T2S4IP"		},	typUINT32,	0},
		{	{      77,	"77-Table 2 Server 4 Port Number",	"T2S4PRT"		},	typUINT16,	0},
		{	{      78,	"78-Table 2 Server 5 IP Address",	"T2S5IP"		},	typUINT32,	0},
		{	{      79,	"79-Table 2 Server 5 Port Number",	"T2S5PRT"		},	typUINT16,	0},
		{	{      80,	"80-Table 2 Server 6 IP Address",	"T2S6IP"		},	typUINT32,	0},
		{	{      81,	"81-Table 2 Server 6 Port Number",	"T2S6PRT"		},	typUINT16,	0},
		{	{      82,	"82-Table 2 Server 7 IP Address",	"T2S7IP"		},	typUINT32,	0},
		{	{      83,	"83-Table 2 Server 7 Port Number",	"T2S7PRT"		},	typUINT16,	0},
		{	{      84,	"84-Table 2 Server 8 IP Address",	"T2S8IP"		},	typUINT32,	0},
		{	{      85,	"85-Table 2 Server 8 Port Number",	"T2S8PRT"		},	typUINT16,	0},
		{	{      86,	"86-Table 2 Server 9 IP Address",	"T2S9IP"		},	typUINT32,	0},
		{	{      87,	"87-Table 2 Server 9 Port Number",	"T2S9PRT"		},	typUINT16,	0},
		{	{      88,	"88-Table 2 Server 10 IP Address",	"T2S10IP"		},	typUINT32,	0},
		{	{      89,	"89-Table 2 Server 10 Port Number",	"T2S10PRT"		},	typUINT16,	0},
		{	{      90,	"90-Table 2 Server 11 IP Address",	"T2S11IP"		},	typUINT32,	0},
		{	{      91,	"91-Table 2 Server 11 Port Number",	"T2S11PRT"		},	typUINT16,	0},
		{	{      92,	"92-Table 2 Server 12 IP Address",	"T2S12IP"		},	typUINT32,	0},
		{	{      93,	"93-Table 2 Server 12 Port Number",	"T2S12PRT"		},	typUINT16,	0},
		{	{      94,	"94-Table 2 Server 13 IP Address",	"T2S13IP"		},	typUINT32,	0},
		{	{      95,	"95-Table 2 Server 13 Port Number",	"T2S13PRT"		},	typUINT16,	0},
		{	{      96,	"96-Table 2 Server 14 IP Address",	"T2S14IP"		},	typUINT32,	0},
		{	{      97,	"97-Table 2 Server 14 Port Number",	"T2S14PRT"		},	typUINT16,	0},
		{	{      98,	"98-Table 2 Server 15 IP Address",	"T2S15IP"		},	typUINT32,	0},
		{	{      99,	"99-Table 2 Server 15 Port Number",	"T2S15PRT"		},	typUINT16,	0},
		{	{     100,	"100-Table 2 Server 16 IP Address",	"T2S16IP"		},	typUINT32,	0},
		{	{     101,	"101-Table 2 Server 16 Port Number",	"T2S16PRT"		},	typUINT16,	0},
		{	{     102,	"102-Table 2 Server 17 IP Address",	"T2S17IP"		},	typUINT32,	0},
		{	{     103,	"103-Table 2 Server 17 Port Number",	"T2S17PRT"		},	typUINT16,	0},
		{	{     104,	"104-Table 2 Server 18 IP Address",	"T2S18IP"		},	typUINT32,	0},
		{	{     105,	"105-Table 2 Server 18 Port Number",	"T2S18PRT"		},	typUINT16,	0},
		{	{     106,	"106-Table 2 Server 19 IP Address",	"T2S19IP"		},	typUINT32,	0},
		{	{     107,	"107-Table 2 Server 19 Port Number",	"T2S19PRT"		},	typUINT16,	0},
		{	{     108,	"108-Table 2 Server 20 IP Address",	"T2S20IP"		},	typUINT32,	0},
		{	{     109,	"109-Table 2 Server 20 Port Number",	"T2S20PRT"		},	typUINT16,	0},
		{	{     110,	"110-Table 2 Server 21 IP Address",	"T2S21IP"		},	typUINT32,	0},
		{	{     111,	"111-Table 2 Server 21 Port Number",	"T2S21PRT"		},	typUINT16,	0},
		{	{     112,	"112-Table 2 Server 22 IP Address",	"T2S22IP"		},	typUINT32,	0},
		{	{     113,	"113-Table 2 Server 22 Port Number",	"T2S22PRT"		},	typUINT16,	0},
		{	{     114,	"114-Table 2 Server 23 IP Address",	"T2S23IP"		},	typUINT32,	0},
		{	{     115,	"115-Table 2 Server 23 Port Number",	"T2S23PRT"		},	typUINT16,	0},
		{	{     116,	"116-Table 2 Server 24 IP Address",	"T2S24IP"		},	typUINT32,	0},
		{	{     117,	"117-Table 2 Server 24 Port Number",	"T2S24PRT"		},	typUINT16,	0},
		{	{     118,	"118-Table 2 Server 25 IP Address",	"T2S25IP"		},	typUINT32,	0},
		{	{     119,	"119-Table 2 Server 25 Port Number",	"T2S25PRT"		},	typUINT16,	0},
		{	{     120,	"120-Table 3 TCP Option",		"T3TCPOP"		},	typUINT8,	0},
		{	{     121,	"121-Table 3 Server 1 IP Address",	"T3S1IP"		},	typUINT32,	0},
		{	{     122,	"122-Table 3 Server 1 Port Number",	"T3S1PRT"		},	typUINT16,	0},
		{	{     123,	"123-Table 3 Server 2 IP Address",	"T3S2IP"		},	typUINT32,	0},
		{	{     124,	"124-Table 3 Server 2 Port Number",	"T3S2PRT"		},	typUINT16,	0},
		{	{     125,	"125-Table 3 Server 3 IP Address",	"T3S3IP"		},	typUINT32,	0},
		{	{     126,	"126-Table 3 Server 3 Port Number",	"T3S3PRT"		},	typUINT16,	0},
		{	{     127,	"127-Table 3 Server 4 IP Address",	"T3S4IP"		},	typUINT32,	0},
		{	{     128,	"128-Table 3 Server 4 Port Number",	"T3S4PRT"		},	typUINT16,	0},
		{	{     129,	"129-Table 3 Server 5 IP Address",	"T3S5IP"		},	typUINT32,	0},
		{	{     130,	"130-Table 3 Server 5 Port Number",	"T3S5PRT"		},	typUINT16,	0},
		{	{     131,	"131-Table 3 Server 6 IP Address",	"T3S6IP"		},	typUINT32,	0},
		{	{     132,	"132-Table 3 Server 6 Port Number",	"T3S6PRT"		},	typUINT16,	0},
		{	{     133,	"133-Table 3 Server 7 IP Address",	"T3S7IP"		},	typUINT32,	0},
		{	{     134,	"134-Table 3 Server 7 Port Number",	"T3S7PRT"		},	typUINT16,	0},
		{	{     135,	"135-Table 3 Server 8 IP Address",	"T3S8IP"		},	typUINT32,	0},
		{	{     136,	"136-Table 3 Server 8 Port Number",	"T3S8PRT"		},	typUINT16,	0},
		{	{     137,	"137-Table 3 Server 9 IP Address",	"T3S9IP"		},	typUINT32,	0},
		{	{     138,	"138-Table 3 Server 9 Port Number",	"T3S9PRT"		},	typUINT16,	0},
		{	{     139,	"139-Table 3 Server 10 IP Address",	"T3S10IP"		},	typUINT32,	0},
		{	{     140,	"140-Table 3 Server 10 Port Number",	"T3S10PRT"		},	typUINT16,	0},
		{	{     141,	"141-Table 3 Server 11 IP Address",	"T3S11IP"		},	typUINT32,	0},
		{	{     142,	"142-Table 3 Server 11 Port Number",	"T3S11PRT"		},	typUINT16,	0},
		{	{     143,	"143-Table 3 Server 12 IP Address",	"T3S12IP"		},	typUINT32,	0},
		{	{     144,	"144-Table 3 Server 12 Port Number",	"T3S12PRT"		},	typUINT16,	0},
		{	{     145,	"145-Table 3 Server 13 IP Address",	"T3S13IP"		},	typUINT32,	0},
		{	{     146,	"146-Table 3 Server 13 Port Number",	"T3S13PRT"		},	typUINT16,	0},
		{	{     147,	"147-Table 3 Server 14 IP Address",	"T3S14IP"		},	typUINT32,	0},
		{	{     148,	"148-Table 3 Server 14 Port Number",	"T3S14PRT"		},	typUINT16,	0},
		{	{     149,	"149-Table 3 Server 15 IP Address",	"T3S15IP"		},	typUINT32,	0},
		{	{     150,	"150-Table 3 Server 15 Port Number",	"T3S15PRT"		},	typUINT16,	0},
		{	{     151,	"151-Table 3 Server 16 IP Address",	"T3S16IP"		},	typUINT32,	0},
		{	{     152,	"152-Table 3 Server 16 Port Number",	"T3S16PRT"		},	typUINT16,	0},
		{	{     153,	"153-Table 3 Server 17 IP Address",	"T3S17IP"		},	typUINT32,	0},
		{	{     154,	"154-Table 3 Server 17 Port Number",	"T3S17PRT"		},	typUINT16,	0},
		{	{     155,	"155-Table 3 Server 18 IP Address",	"T3S18IP"		},	typUINT32,	0},
		{	{     156,	"156-Table 3 Server 18 Port Number",	"T3S18PRT"		},	typUINT16,	0},
		{	{     157,	"157-Table 3 Server 19 IP Address",	"T3S19IP"		},	typUINT32,	0},
		{	{     158,	"158-Table 3 Server 19 Port Number",	"T3S19PRT"		},	typUINT16,	0},
		{	{     159,	"159-Table 3 Server 20 IP Address",	"T3S20IP"		},	typUINT32,	0},
		{	{     160,	"160-Table 3 Server 20 Port Number",	"T3S20PRT"		},	typUINT16,	0},
		{	{     161,	"161-Table 3 Server 21 IP Address",	"T3S21IP"		},	typUINT32,	0},
		{	{     162,	"162-Table 3 Server 21 Port Number",	"T3S21PRT"		},	typUINT16,	0},
		{	{     163,	"163-Table 3 Server 22 IP Address",	"T3S22IP"		},	typUINT32,	0},
		{	{     164,	"164-Table 3 Server 22 Port Number",	"T3S22PRT"		},	typUINT16,	0},
		{	{     165,	"165-Table 3 Server 23 IP Address",	"T3S23IP"		},	typUINT32,	0},
		{	{     166,	"166-Table 3 Server 23 Port Number",	"T3S23PRT"		},	typUINT16,	0},
		{	{     167,	"167-Table 3 Server 24 IP Address",	"T3S24IP"		},	typUINT32,	0},
		{	{     168,	"168-Table 3 Server 24 Port Number",	"T3S24PRT"		},	typUINT16,	0},
		{	{     169,	"169-Table 3 Server 25 IP Address",	"T3S25IP"		},	typUINT32,	0},
		{	{     170,	"170-Table 3 Server 25 Port Number",	"T3S25PRT"		},	typUINT16,	0},
		{	{     171,	"171-TCP Connection Timeout",		"TCPCNTO"		},	typUINT8,	0},
		{	{     172,	"172-Test IP Address",			"TESTIP"		},	typUINT32,	0},
		{	{     173,	"173-Test Port",			"TESTPORT"		},	typUINT16,	0},
		{	{     174,	"174-Test Start",			"TESTSTRT"		},	typUINT8,	0},
		{	{     175,	"175-Test Status",			"TESTSTS"		},	typUINT8,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	138,	// User Progarm Configuration
	{
		{	{	0,	"0-Host Library Version",		"LIBVER"		},	typStr12,	1},
		{	{	1,	"1-Host SRAM Used",			"SRAMUSED"		},	typUINT32,	1},	
		{	{	2,	"2-Host SRAM Free",			"SRAMFREE"		},	typUINT32,	1},
		{	{	3,	"3-Host DRAM Used",			"DRAMUSED"		},	typUINT32,	1},
		{	{	4,	"4-Host DRAM Free",			"DRAMFREE"		},	typUINT32,	1},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	139,	// Smart I/O Module Information
	{
		{	{	0,	"0-Module Type",			"MODTYPE"		},	typUINT8,	1},
		{	{	1,	"1-System Mode",			"MODMODE"		},	typUINT8,	1},	
		{	{	2,	"2-Board Health",			"MODHLTH"		},	typUINT8,	1},
		{	{	3,	"3-Boot Version",			"BOOTREV"		},	typStr10,	1},
		{	{	4,	"4-Boot Part Number",			"BOOTPN"		},	typStr20,	1},
		{	{	5,	"5-Boot Build Date",			"BOOTDATE"		},	typStr20,	1},
		{	{	6,	"6-Flash Version",			"MODREV"		},	typStr10,	1},
		{	{	7,	"7-Flash Part Number",			"MODPN"			},	typStr20,	1},
		{	{	8,      "8-Flash Build Date",			"MODDATE"		},	typStr20,	1},
		{	{	9,      "9-Module Specific Data",		"MODDATA"		},	typStr20,	1},
		{	{	10,     "10-Serial Number",			"MODSN"			},	typStr30,	1},
		{	{	11,     "11-Description",			"MODDESC"		},	typStr20,	1},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	140,	// AC I/O Module Information
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr10,	0},
		{	{	1,	"1-Power In",				"PWRIN"			},	typUINT8,	1},	
		{	{	2,	"2-Channel Mode",			"MODE"			},	typUINT8,	1},
		{	{	3,	"3-Scanning Input",			"SCANIN"		},	typUINT8,	0},
		{	{	4,	"4-Filter",				"FILTER"		},	typReal,	0},
		{	{	5,	"5-Status Input",			"STATIN"		},	typUINT8,	0},
		{	{	6,	"6-Physical Input",			"PHYIN"			},	typUINT8,	1},
		{	{	7,	"7-Scan Period",			"SCANPR"		},	typReal,	0},
		{	{	8,      "8-Actual Scan Time",			"SCAN"			},	typReal,	1},
		{	{	9,      "9-Input Invert Mode",			"INVERTI"		},	typUINT8,	0},
		{	{	10,     "10-Latch Mode",			"LATCH"			},	typUINT8,	0},
		{	{	11,     "11-Input Accumulated Value",		"IACCUM"		},	typUINT32,	0},
		{	{	12,	"12-Cumulative On Time",		"ONCTR"			},	typReal,	0},
		{	{	13,	"13-Cumulative Off Time",		"OFFCTR"		},	typReal,	0},	
		{	{	14,	"14-Input Alarming",			"IALEN"			},	typUINT8,	0},
		{	{	15,	"15-Input Alarm Code",			"ALMCODE"		},	typUINT8,	1},
		{	{	16,	"16-Input SRBX on Clear",		"ISRBXC"		},	typUINT8,	0},
		{	{	17,	"17-Input SRBX on Set",			"ISRBXS"		},	typUINT8,	0},
		{	{	18,	"18-Scanning Output",			"SCANOUT"		},	typUINT8,	0},
		{	{	19,	"19-Auto Output",			"AOSTATUS"		},	typUINT8,	0},
		{	{	20,     "20-Manual Output",			"MANVAL"		},	typUINT8,	0},
		{	{	21,     "21-Failsafe Output",			"FAULTVAL"		},	typUINT8,	0},
		{	{	22,     "22-Physical Output",			"PHYOUT"		},	typUINT8,	1},
		{	{	23,     "23-Output Accumulated Value",		"OACCUM"		},	typUINT32,	0},
		{	{	24,	"24-Failsafe on Reset Mode",		"CLRONRS"		},	typUINT8,	0},
		{	{	25,	"25-Momentary Mode",			"MOMODE"		},	typUINT8,	0},	
		{	{	26,	"26-Momentary Active",			"MOACTIV"		},	typUINT8,	1},
		{	{	27,	"27-Toggle Mode",			"TOGMODE"		},	typUINT8,	0},
		{	{	28,	"28-Timed Discrete Output",		"TDOMODE"		},	typUINT8,	0},
		{	{	29,	"29-Invert Output Mode",		"INVERTO"		},	typUINT8,	0},
		{	{	30,	"30-Time On",				"TIMEON"		},	typReal,	0},
		{	{	31,	"31-Cycle Time",			"CYCTIME"		},	typReal,	0},
		{	{	32,     "32-Units Tag",				"UNITS"			},	typStr10,	0},
		{	{	33,     "33-Low Reading Time",			"LOWRT"			},	typReal,	0},
		{	{	34,     "34-High Reading Time",			"HIGHRT"		},	typReal,	0},
		{	{	35,     "35-Low Reading EU",			"LOWREU"		},	typReal,	0},
		{	{	36,	"36-High Reading EU",			"HIGHREU"		},	typReal,	0},
		{	{	37,	"37-EU Value",				"EUVAL"			},	typReal,	0},	
		{	{	38,	"38-Inrush Time",			"INRUSH"		},	typReal,	0},
		{	{	39,	"39-Holding Current",			"CURRNT"		},	typReal,	1},
		{	{	40,	"40-Fault Reset",			"FLTRESET"		},	typUINT8,	0},
		{	{	41,	"41-Output Alarming",			"OALEN"			},	typUINT8,	0},
		{	{	42,	"42-Output Alarm Code",			"ALMCODE"		},	typUINT8,	1},
		{	{	43,	"43-Output SRBX On Clear",		"OSRBXC"		},	typUINT8,	0},
		{	{	44,     "44-Output SRBX On Set",		"OSRBXS"		},	typUINT8,	0},
		{	{	45,     "45-AC Frequency",			"ACFREQ"		},	typReal,	0},
		{	{	46,     "46-Failure Action",			"FAILACT"		},	typUINT8,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	200,	// Liquid Preferences
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr20,	0},
		{	{	1,	"1-Program Status",			"STATUS"		},	typUINT8,	1},
		{	{	2,	"2-Calculation Period",			"CALC_PER"		},	typReal,	0},
		{	{	3,	"",					""			},	paramNone,	0},
		{	{	4,	"4-Pressure Units Option",		"P_UNITS"		},	typUINT8,	0},
		{	{	5,	"5-Temperature Units Option",		"T_UNITS"		},	typUINT8,	0},
		{	{	6,	"6-Density Units Option",		"DI_UNITS"		},	typUINT8,	0},
		{	{	7,	"7-Correction Table Units Option",	"DO_UNITS"		},	typUINT8,	0},
		{	{	8,	"8-Linear Units Option",		"L_UNITS"		},	typUINT8,	0},
		{	{	9,	"",					""			},	paramNone,	0},
		{	{	10,	"10-Volume Units Option",		"V_UNITS"		},	typUINT8,	0},
		{	{	11,	"11-Mass Units Option",			"M_UNITS"		},	typUINT8,	0},
		{	{	12,	"12-Flow Rate Option",			"RATE_OPT"		},	typUINT8,	0},
		{	{	13,	"13-Rollover",				"ROLL_VAL"		},	typDouble,	1},
		{	{	14,	"14-Calculation Alarm Status",		"CALC_ALM"		},	typUINT8,	1},
		{	{	15,	"15-Calculation Alarm Reset",		"ALM_RST"		},	typUINT8,	0},
		{	{	16,	"16-Atmospheric Pressure",		"ATM_PR"		},	typDouble,	0},
		{	{	17,	"",					""			},	paramNone,	0},
		{	{	18,	"",					""			},	paramNone,	0},
		{	{	19,	"",					""			},	paramNone,	0},
		{	{	20,	"20-Gas Constant",			"R_JMOLK"		},	typDouble,	0},
		{	{	21,	"21-Methane Molecular Weight",		"MW_C1"			},	typDouble,	0},
		{	{	22,	"22-Ethane Molecular Weight",		"MW_C2"			},	typDouble,	0},
		{	{	23,	"23-Propane Molecular Weight",		"MW_C3"			},	typDouble,	0},
		{	{	24,	"24-n Butane Molecular Weight",		"MW_NC4"		},	typDouble,	0},
		{	{	25,	"25-i Butane Molecular Weight",		"MW_IC4"		},	typDouble,	0},
		{	{	26,	"26-n Pentane Molecular Weight",	"MW_NC5"		},	typDouble,	0},
		{	{	27,	"27-i Pentane Molecular Weight",	"MW_IC5"		},	typDouble,	0},
		{	{	28,	"28-n Hexane Molecular Weight",		"MW_NC6"		},	typDouble,	0},
		{	{	29,	"29-n Heptane Molecular Weight",	"MW_NC7"		},	typDouble,	0},
		{	{	30,	"30-Ethylene Molecular Weight",		"MW_C2H4"		},	typDouble,	0},
		{	{	31,	"31-Propylene Molecular Weight",	"MW_C3H6"		},	typDouble,	0},
		{	{	32,	"32-i Butene Molecular Weight",		"MW_NC4H8"		},	typDouble,	0},
		{	{	33,	"33-Nitrogen Molecular Weight",		"MW_N2"			},	typDouble,	0},
		{	{	34,	"34-Oxygen Molecular Weight",		"MW_O2"			},	typDouble,	0},
		{	{	35,	"35-Carbon Dioxide Molecular Weight",	"MW_CO2"		},	typDouble,	0},
		{	{	36,	"36-Hydrogen Sulfide Molecular Weight",	"MW_H2S"		},	typDouble,	0},
		{	{	37,	"37-Methane Critical Temperature",	"TC_C1"			},	typDouble,	0},
		{	{	38,	"38-Ethane Critical Temperature",	"TC_C2"			},	typDouble,	0},
		{	{	39,	"39-Propane Critical Temperature",	"TC_C3"			},	typDouble,	0},
		{	{	40,	"40-n Butane Critical Temperature",	"TC_NC4"		},	typDouble,	0},
		{	{	41,	"41-i Butane Critical Temperature",	"TC_IC4"		},	typDouble,	0},
		{	{	42,	"42-n Pentane Critical Temperature",	"TC_NC5"		},	typDouble,	0},
		{	{	43,	"43-i Pentane Critical Temperature",	"TC_IC5"		},	typDouble,	0},
		{	{	44,	"44-n Hexane Critical Temperature",	"TC_NC6"		},	typDouble,	0},
		{	{	45,	"45-n Heptane Critical Temperature",	"TC_NC7"		},	typDouble,	0},
		{	{	46,	"46-Ethylene Critical Temperature",	"TC_C2H4"		},	typDouble,	0},
		{	{	47,	"47-Propylene Critical Temperature",	"TC_C3H6"		},	typDouble,	0},
		{	{	48,	"48-i Butene Critical Temperature",	"TC_NC4H8"		},	typDouble,	0},
		{	{	49,	"49-Nitrogen Critical Temperature",	"TC_N2"			},	typDouble,	0},
		{	{	50,	"50-Oxygen Critical Temperature",	"TC_O2"			},	typDouble,	0},
		{	{	51,	"51-Carbon Dioxide Critical Temp",	"TC_CO2"		},	typDouble,	0},
		{	{	52,	"52-Hydrogen Sulfide Critical Temp",	"TC_H2S"		},	typDouble,	0},
		{	{	53,	"53-Methane Characteristic Volume",	"CV_C1"			},	typDouble,	0},
		{	{	54,	"54-Ethane Characteristic Volume",	"CV_C2"			},	typDouble,	0},
		{	{	55,	"55-Propane Characteristic Volume",	"CV_C3"			},	typDouble,	0},
		{	{	56,	"56-n Butane Characteristic Volume",	"CV_NC4"		},	typDouble,	0},
		{	{	57,	"57-i Butane Characteristic Volume",	"CV_IC4"		},	typDouble,	0},
		{	{	58,	"58-n Pentane Characteristic Volume",	"CV_NC5"		},	typDouble,	0},
		{	{	59,	"59-i Pentane Characteristic Volume",	"CV_IC5"		},	typDouble,	0},
		{	{	60,	"60-n Hexane Characteristic Volume",	"CV_NC6"		},	typDouble,	0},
		{	{	61,	"61-n Heptane Characteristic Volume",	"CV_NC7"		},	typDouble,	0},
		{	{	62,	"62-Ethylene Characteristic Volume",	"CV_C2H4"		},	typDouble,	0},
		{	{	63,	"63-Propylene Characteristic Volume",	"CV_C3H6"		},	typDouble,	0},
		{	{	64,	"64-i Butene Characteristic Volume",	"CV_NC4H8"		},	typDouble,	0},
		{	{	65,	"65-Nitrogen Characteristic Volume",	"CV_N2"			},	typDouble,	0},
		{	{	66,	"66-Oxygen Characteristic Volume",	"CV_O2"			},	typDouble,	0},
		{	{	67,	"67-Carbon Dioxide Characteristic Vol",	"CV_CO2"		},	typDouble,	0},
		{	{	68,	"68-Hydrogen Sulfide Characteristic Vol","CV_H2S"		},	typDouble,	0},
		{	{	69,	"69-Methane Acentric Factor",		"AF_C1"			},	typDouble,	0},
		{	{	70,	"70-Ethane Acentric Factor",		"AF_C2"			},	typDouble,	0},
		{	{	71,	"71-Propane Acentric Factor",		"AF_C3"			},	typDouble,	0},
		{	{	72,	"72-n Butane Acentric Factor",		"AF_NC4"		},	typDouble,	0},
		{	{	73,	"73-i Butane Acentric Factor",		"AF_IC4"		},	typDouble,	0},
		{	{	74,	"74-n Pentane Acentric Factor",		"AF_NC5"		},	typDouble,	0},
		{	{	75,	"75-i Pentane Acentric Factor",		"AF_IC5"		},	typDouble,	0},
		{	{	76,	"76-n Hexane Acentric Factor",		"AF_NC6"		},	typDouble,	0},
		{	{	77,	"77-n Heptane Acentric Factor",		"AF_NC7"		},	typDouble,	0},
		{	{	78,	"78-Ethylene Acentric Factor",		"AF_C2H4"		},	typDouble,	0},
		{	{	79,	"79-Propylene Acentric Factor",		"AF_C3H6"		},	typDouble,	0},
		{	{	80,	"80-i Butene Acentric Factor",		"AF_NC4H8"		},	typDouble,	0},
		{	{	81,	"81-Nitrogen Acentric Factor",		"AF_N2"			},	typDouble,	0},
		{	{	82,	"82-Oxygen Acentric Factor",		"AF_O2"			},	typDouble,	0},
		{	{	83,	"83-Carbon Dioxide Acentric Factor",	"AF_CO2"		},	typDouble,	0},
		{	{	84,	"84-Hydrogen Sulfide Acentric Factor",	"AF_H2S"		},	typDouble,	0},
		{	{	85,	"85-Neo Pentane Molecular Weight",	"MW_NEO"		},	typDouble,	0},
		{	{	86,	"86-Neo Pentane Critical Temperature",	"CT_NEO"		},	typDouble,	0},
		{	{	87,	"87-Neo Pentane Characteristic Volume",	"CV_NEO"		},	typDouble,	0},
		{	{	88,	"88-Neo Pentane Acentric Factor",	"AF_NEO"		},	typDouble,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
		
	201,	// Liquid Products
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr20,	0},
		{	{	1,	"1-Fluid Type",				"TYPE"			},	typUINT8,	0},
		{	{	2,	"2-API Measurement Standard",		"API_STD"		},	typUINT8,	0},
		{	{	3,	"3-Light Hydrocarbon Standard Version",	"LHYD_OPT"		},	typUINT8,	0},
		{	{	4,	"4-Compressibility Option",		"F_OPT"			},	typUINT8,	0},
		{	{	5,	"5-Compressibility Factor",		"COMP_FAC"		},	typDouble,	0},
		{	{	6,	"6-Equilibrium Base Pressure",		"EB_PRES"		},	typDouble,	0},
		{	{	7,	"7-Alpha Coefficient",			"ALPHA"			},	typDouble,	0},
		{	{	8,	"8-Base Density",			"B_DEN"			},	typDouble,	0},
		{	{	9,	"9-VCF Rounding Option",		"RNDG_OPT"		},	typUINT8,	0},
		{	{	10,	"",					""			},	paramNone,	0},
		{	{	11,	"11-Meter 1 K Factor",			"M1_KF"			},	typDouble,	0},
		{	{	12,	"12-Meter 1 Meter Factor",		"M1_MF"			},	typDouble,	0},
		{	{	13,	"13-Meter 1 Prove Sequence Number",	"M1_PSN"		},	typUINT32,	0},
		{	{	14,	"14-Meter 1 Prove Time and Date",	"M1_PDT"		},	typTime,	0},	// TIME
		{	{	15,	"15-Meter 2 K Factor",			"M2_KF"			},	typDouble,	0},
		{	{	16,	"16-Meter 2 Meter Factor",		"M2_MF"			},	typDouble,	0},
		{	{	17,	"17-Meter 2 Prove Sequence Number",	"M2_PSN"		},	typUINT32,	0},
		{	{	18,	"18-Meter 2 Prove Time and Date",	"M2_PDT"		},	typTime,	0},	// TIME
		{	{	19,	"19-Meter 3 K Factor",			"M3_KF"			},	typDouble,	0},
		{	{	20,	"20-Meter 3 Meter Factor",		"M3_MF"			},	typDouble,	0},
		{	{	21,	"21-Meter 3 Prove Sequence Number",	"M3_PSN"		},	typUINT32,	0},
		{	{	22,	"22-Meter 3 Prove Time and Date",	"M3_PDT"		},	typTime,	0},	// TIME
		{	{	23,	"23-Meter 4 K Factor",			"M4_KF"			},	typDouble,	0},
		{	{	24,	"24-Meter 4 Meter Factor",		"M4_MF"			},	typDouble,	0},
		{	{	25,	"25-Meter 4 Prove Sequence Number",	"M4_PSN"		},	typUINT32,	0},
		{	{	26,	"26-Meter 4 Prove Time and Date",	"M4_PDT"		},	typTime,	0},	// TIME
		{	{	27,	"27-Meter 5 K Factor",			"M5_KF"			},	typDouble,	0},
		{	{	28,	"28-Meter 5 Meter Factor",		"M5_MF"			},	typDouble,	0},
		{	{	29,	"29-Meter 5 Prove Sequence Number",	"M5_PSN"		},	typUINT32,	0},
		{	{	30,	"30-Meter 5 Prove Time and Date",	"M5_PDT"		},	typTime,	0},	// TIME
		{	{	31,	"31-Meter 6 K Factor",			"M6_KF"			},	typDouble,	0},
		{	{	32,	"32-Meter 6 Meter Factor",		"M6_MF"			},	typDouble,	0},
		{	{	33,	"33-Meter 6 Prove Sequence Number",	"M6_PSN"		},	typUINT32,	0},
		{	{	34,	"34-Meter 6 Prove Time and Date",	"M6_PDT"		},	typTime,	0},	// TIME
		{	{	35,	"35-Low Density Alarm Limit",		"LOW_ALM"		},	typDouble,	0},
		{	{	36,	"36-High Density Alarm Limit",		"HIGH_ALM"		},	typDouble,	0},
		{	{	37,	"37-Density Alarm Deadband",		"DEN_DBND"		},	typDouble,	0},
		{	{	38,	"38-Meter 1 Prove Sequence Counter",	"M1_PSC"		},	typUINT32,	0},
		{	{	39,	"39-Meter 2 Prove Sequence Counter",	"M2_PSC"		},	typUINT32,	0},
		{	{	40,	"40-Meter 3 Prove Sequence Counter",	"M3_PSC"		},	typUINT32,	0},
		{	{	41,	"41-Meter 4 Prove Sequence Counter",	"M4_PSC"		},	typUINT32,	0},
		{	{	42,	"42-Meter 5 Prove Sequence Counter",	"M5_PSC"		},	typUINT32,	0},
		{	{	43,	"43-Meter 6 Prove Sequence Counter",	"M6_PSC"		},	typUINT32,	0},
		{	{	44,	"44-Ethanol Table Option",		"ETH_OPT"		},	typUINT8,	0},
		{	{	45,	"45-Ethanol Mass Percent",		"ETH_PERC"		},	typDouble,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
		
	202,	// Density Interface
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr20,	0},
		{	{	1,	"1-Density Input Type",			"DEN_TYP"		},	typUINT8,	0},
		{	{	2,	"2-Raw Density Input TLP",		"DEN_TLP"		},	typTLP,		0},
		{	{	3,	"3-Raw Density Input",			"DEN_RAW"		},	typDouble,	0},
		{	{	4,	"4-Density Input Status Flag",		"STATUS"		},	typUINT8,	1},
		{	{	5,	"5-Density Temperature TLP",		"TEMP_TLP"		},	typTLP,		0},
		{	{	6,	"6-Density Temperature",		"DEN_TMP"		},	typDouble,	0},
		{	{	7,	"7-Density Pressure TLP",		"PRES_TLP"		},	typTLP,		0},
		{	{	8,	"8-Density Pressure",			"DEN_PSI"		},	typDouble,	0},
		{	{	9,	"9-Density Correction Factor",		"DEN_FCT"		},	typDouble,	0},
		{	{	10,	"10-Observed Density In Use",		"OBS_DEN"		},	typDouble,	1},
		{	{	11,	"11-Observed Relative Density In Use",	"REL_DEN"		},	typDouble,	1},
		{	{	12,	"12-Observed API Gravity In Use",	"API_GRV"		},	typDouble,	1},
		{	{	13,	"13-Observed Density Live Reading",	"OBS_DENL"		},	typDouble,	1},
		{	{	14,	"14-Density Backup Value",		"DEN_BACK"		},	typDouble,	0},
		{	{	15,	"15-Density Download Value",		"DEN_DNLD"		},	typDouble,	0},
		{	{	16,	"16-Hydrometer Correction Option",	"HYD_OPT"		},	typUINT8,	0},
		{	{	17,	"17-Periodic Time",			"PER_TIME"		},	typDouble,	1},
		{	{	18,	"18-Density Scanning Mode",		"DEN_SCAN"		},	typUINT8,	0},
		{	{	19,	"19-Manual Mode Off Scan Options",	"OFF_SCAN"		},	typUINT8,	0},
		{	{	20,	"20-Auto Mode Failure Options",		"FAIL_MOD"		},	typUINT8,	0},
		{	{	21,	"21-Low Density Failure Value",		"LOW_FAIL"		},	typDouble,	0},
		{	{	22,	"22-High Density Failure Value",	"HI_FAIL"		},	typDouble,	0},
		{	{	23,	"23-Product Change Alarm Delay",	"CHG_DEL"		},	typUINT16,	0},
		{	{	24,	"24-Density Alarming Option",		"ALM_OPT"		},	typUINT8,	0},
		{	{	25,	"25-SRBX on Clear",			"SRBX_CLR"		},	typUINT8,	0},
		{	{	26,	"26-SRBX on Set",			"SRBX_SET"		},	typUINT8,	0},
		{	{	27,	"27-Alarm Code",			"ALM_CODE"		},	typUINT8,	1},
		{	{	28,	"",					""			},	paramNone,	0},
		{	{	29,	"",					""			},	paramNone,	0},
		{	{	30,	"",					""			},	paramNone,	0},
		{	{	31,	"31-Basic Transducer Constant K0 A0",	"CONST_0"		},	typDouble,	0},
		{	{	32,	"32-Basic Transducer Constant K1 A1",	"CONST_1"		},	typDouble,	0},
		{	{	33,	"33-Basic Transducer Constant K2 A2",	"CONST_2"		},	typDouble,	0},
		{	{	34,	"",					""			},	paramNone,	0},
		{	{	35,	"",					""			},	paramNone,	0},
		{	{	36,	"36-Micro Motion Constant K18",		"MM_K18"		},	typDouble,	0},
		{	{	37,	"37-Micro Motion Constant K19",		"MM_K19"		},	typDouble,	0},
		{	{	38,	"38-Micro Motion Constant K20A",	"MM_K20A"		},	typDouble,	0},
		{	{	39,	"39-Micro Motion Constant K20B",	"MM_K20B"		},	typDouble,	0},
		{	{	40,	"40-Micro Motion Constant K21A",	"MM_K21A"		},	typDouble,	0},
		{	{	41,	"41-Micro Motion Constant K21B",	"MM_K21B"		},	typDouble,	0},
		{	{	42,	"",					""			},	paramNone,	0},
		{	{	43,	"",					""			},	paramNone,	0},
		{	{	44,	"44-UGC Calibration Pressure Pc",	"UGC_PC"		},	typDouble,	0},
		{	{	45,	"45-UGC Pressure Constant Kp1",		"UGC_KP1"		},	typDouble,	0},
		{	{	46,	"46-UGC Pressure Constant Kp2",		"UGC_KP2"		},	typDouble,	0},
		{	{	47,	"47-UGC Pressure Constant Kp3",		"UGC_KP3"		},	typDouble,	0},
		{	{	48,	"48-UGC Calibration Temperature Tc",	"UGC_TC"		},	typDouble,	0},
		{	{	49,	"49-UGC Pressure Constant Kt1",		"UGC_KT1"		},	typDouble,	0},
		{	{	50,	"50-UGC Pressure Constant Kt2",		"UGC_KT2"		},	typDouble,	0},
		{	{	51,	"51-UGC Pressure Constant Kt3",		"UGC_KT3"		},	typDouble,	0},
		{	{	52,	"",					""			},	paramNone,	0},
		{	{	53,	"",					""			},	paramNone,	0},
		{	{	54,	"54-Sarasota Constant K",		"SARA_K"		},	typDouble,	0},
		{	{	55,	"55-Sarasota Constant DO",		"SARA_DO"		},	typDouble,	0},
		{	{	56,	"56-Sarasota Temperature Coefficient",	"SARA_TC"		},	typDouble,	0},
		{	{	57,	"57-Sarasota Pressure Coefficient",	"SARA_PC"		},	typDouble,	0},
		{	{	58,	"58-Sarasota Constant TO",		"SARA_TO"		},	typDouble,	0},
		{	{	59,	"",					""			},	paramNone,	0},
		{	{	60,	"",					""			},	paramNone,	0},
		{	{	61,	"61-Velocity of Sound Option",		"VELS_OPT"		},	typUINT8,	0},
		{	{	62,	"62-Liquid Velocity of Sound",		"LIQ_VELS"		},	typDouble,	0},
		{	{	63,	"63-Calibration Velocity of Sound",	"CAL_VELS"		},	typDouble,	1},
		{	{	64,	"64-Test Mode",				"TST_MODE"		},	typUINT8,	0},
		{	{	65,	"65-Test Frequency",			"TST_FREQ"		},	typDouble,	0},
		{	{	66,	"",					""			},	paramNone,	0},
		{	{	67,	"67-Densiometer Constants Units Option","CNST_OPT"		},	typUINT8,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},

	203,	// Liquid Station
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr20,	0},
		{	{	1,	"1-Product Logical",			"PRODUCT"		},	typUINT8,	0},
		{	{	2,	"2-Flowrate Alarm Option",		"FAL_OPT"		},	typUINT8,	0},
		{	{	3,	"3-SRBX on Clear",			"SRBX_CLR"		},	typUINT8,	0},
		{	{	4,	"4-SRBX on Set",			"SRBX_SET"		},	typUINT8,	0},
		{	{	5,	"5-Flowrate Alarm Code",		"FAL_CODE"		},	typUINT8,	1},
		{	{	6,	"6-Low Flow Alarm Value",		"LOW_ALM"		},	typDouble,	0},
		{	{	7,	"7-High Flow Alarm Value",		"HIGH_ALM"		},	typDouble,	0},
		{	{	8,	"8-Alarm Deadband",			"ALM_DBND"		},	typDouble,	0},
		{	{	9,	"9-Base Temperature Option",		"BASE_OPT"		},	typUINT8,	0},
		{	{	10,	"10-Base Temperature",			"TEMP_B"		},	typDouble,	0},
		{	{	11,	"11-Densitometer Option",		"DEN_OPT"		},	typUINT8,	0},
		{	{	12,	"12-Density A Logical",			"DENA_LOG"		},	typUINT8,	0},
		{	{	13,	"13-Density B Logical",			"DENB_LOG"		},	typUINT8,	0},
		{	{	14,	"14-Selected Density Input",		"DEN_INPT"		},	typUINT8,	0},
		{	{	15,	"",					""			},	paramNone,	0},
		{	{	16,	"16-Observed Density",			"OBS_DEN"		},	typDouble,	1},
		{	{	17,	"17-Base Density",			"B_DEN"			},	typDouble,	1},
		{	{	18,	"18-Flow Calculation Alarm Option",	"FCA_OPT"		},	typUINT8,	0},
		{	{	19,	"19-Flow Calculation Alarm Code",	"FCA_CODE"		},	typUINT8,	1},
		{	{	20,	"20-Temperature Correction Table",	"TMP_TABL"		},	typUINT8,	1},
		{	{	21,	"21-Pressure Correction Table",		"PRS_TABL"		},	typUINT8,	1},
		{	{	22,	"22-K0",				"K0"			},	typDouble,	1},
		{	{	23,	"23-K1",				"K1"			},	typDouble,	0},
		{	{	24,	"24-K2",				"K2"			},	typDouble,	0},
		{	{	25,	"",					""			},	paramNone,	0},
		{	{	26,	"",					""			},	paramNone,	0},
		{	{	27,	"",					""			},	paramNone,	0},
		{	{	28,	"",					""			},	paramNone,	0},
		{	{	29,	"29-Indicated Quantity Flow Rate",	"IV_RATE"		},	typDouble,	1},
		{	{	30,	"30-Gross Volume Flow Rate",		"GV_RATE"		},	typDouble,	1},
		{	{	31,	"31-Gross Std Volume Flow Rate",	"GSV_RATE"		},	typDouble,	1},
		{	{	32,	"32-Net Std Volume Flow Rate",		"NSV_RATE"		},	typDouble,	1},
		{	{	33,	"33-BSW Volume Rate",			"BSW_RATE"		},	typDouble,	1},
		{	{	34,	"34-Mass Rate",				"MAS_RATE"		},	typDouble,	1},
		{	{	35,	"35-Indicated Quantity Current Hour",	"IV_HR"			},	typDouble,	1},
		{	{	36,	"36-Gross Volume Current Hour",		"GV_HR"			},	typDouble,	1},
		{	{	37,	"37-Gross Std Volume Current Hour",	"GSV_HR"		},	typDouble,	1},
		{	{	38,	"38-Net Std Volume Current Hour",	"NSV_HR"		},	typDouble,	1},
		{	{	39,	"39-BSW Volume Current Hour",		"BSW_HR"		},	typDouble,	1},
		{	{	40,	"40-Mass Current Hour",			"MAS_HR"		},	typDouble,	1},
		{	{	41,	"41-Indicated Quantity Last Hour",	"IV_LHR"		},	typDouble,	1},
		{	{	42,	"42-Gross Volume Last Hour",		"GV_LHR"		},	typDouble,	1},
		{	{	43,	"43-Gross Std Volume Last Hour",	"GSV_LHR"		},	typDouble,	1},
		{	{	44,	"44-Net Std Volume Last Hour",		"NSV_LHR"		},	typDouble,	1},
		{	{	45,	"45-BSW Volume Last Hour",		"BSW_LHR"		},	typDouble,	1},
		{	{	46,	"46-Mass Last Hour",			"MAS_LHR"		},	typDouble,	1},
		{	{	47,	"47-Indicated Quantity Today",		"IV_TDY"		},	typDouble,	1},
		{	{	48,	"48-Gross Volume Today",		"GV_TDY"		},	typDouble,	1},
		{	{	49,	"49-Gross Std Volume Today",		"GSV_TDY"		},	typDouble,	1},
		{	{	50,	"50-Net Std Volume Today",		"NSV_TDY"		},	typDouble,	1},
		{	{	51,	"51-BSW Volume Today",			"BSW_TDY"		},	typDouble,	1},
		{	{	52,	"52-Mass Today",			"MAS_TDY"		},	typDouble,	1},
		{	{	53,	"53-Indicated Quantity Yesterday",	"IV_YDAY"		},	typDouble,	1},
		{	{	54,	"54-Gross Volume Yesterday",		"GV_YDAY"		},	typDouble,	1},
		{	{	55,	"55-Gross Std Volume Yesterday",	"GSV_YDAY"		},	typDouble,	1},
		{	{	56,	"56-Net Std Volume Yesterday",		"NSV_YDAY"		},	typDouble,	1},
		{	{	57,	"57-BSW Volume Yesterday",		"BSW_YDAY"		},	typDouble,	1},
		{	{	58,	"58-Mass Yesterday",			"MAS_YDAY"		},	typDouble,	1},
		{	{	59,	"59-Indicated Quantity This Month",	"IV_MTH"		},	typDouble,	1},
		{	{	60,	"60-Gross Volume This Month",		"GV_MTH"		},	typDouble,	1},
		{	{	61,	"61-Gross Std Volume This Month",	"GSV_MTH"		},	typDouble,	1},
		{	{	62,	"62-Net Std Volume This Month",		"NSV_MTH"		},	typDouble,	1},
		{	{	63,	"63-BSW Volume This Month",		"BSW_MTH"		},	typDouble,	1},
		{	{	64,	"64-Mass This Month",			"MAS_MTH"		},	typDouble,	1},
		{	{	65,	"65-Indicated Quantity Prev Month",	"IV_PMTH"		},	typDouble,	1},
		{	{	66,	"66-Gross Volume Prev Month",		"GV_PMTH"		},	typDouble,	1},
		{	{	67,	"67-Gross Std Volume Prev Month",	"GSV_PMTH"		},	typDouble,	1},
		{	{	68,	"68-Net Std Volume Prev Month",		"NSV_PMTH"		},	typDouble,	1},
		{	{	69,	"69-BSW Volume Prev Month",		"BSW_PMTH"		},	typDouble,	1},
		{	{	70,	"70-Mass Prev Month",			"MAS_PMTH"		},	typDouble,	1},
		{	{	71,	"71-Indicated Quantity Total Accum",	"IV_TOT"		},	typDouble,	1},
		{	{	72,	"72-Gross Volume Total Accum",		"GV_TOT"		},	typDouble,	1},
		{	{	73,	"73-Gross Std Volume Total Accum",	"GSV_TOT"		},	typDouble,	1},
		{	{	74,	"74-Net Std Volume Total Accum",	"NSV_TOT"		},	typDouble,	1},
		{	{	75,	"75-BSW Volume Total Accum",		"BSW_TOT"		},	typDouble,	1},
		{	{	76,	"76-Mass Total Accum",			"MAS_TOT"		},	typDouble,	1},
		{	{	77,	"77-Maintenance Mode Option",		"MTN_OPT"		},	typUINT8,	0},
		{	{	78,	"",					""			},	paramNone,	0},
		{	{	79,	"",					""			},	paramNone,	0},
		{	{	80,	"",					""			},	paramNone,	0},
		{	{	81,	"",					""			},	paramNone,	0},
		{	{	82,	"",					""			},	paramNone,	0},
		{	{	83,	"",					""			},	paramNone,	0},
		{	{	84,	"",					""			},	paramNone,	0},
		{	{	85,	"",					""			},	paramNone,	0},
		{	{	86,	"",					""			},	paramNone,	0},
		{	{	87,	"",					""			},	paramNone,	0},
		{	{	88,	"",					""			},	paramNone,	0},
		{	{	89,	"",					""			},	paramNone,	0},
		{	{	90,	"",					""			},	paramNone,	0},
		{	{	91,	"91-FWA Pressure Previous Hour",	"FWA_PPH"		},	typDouble,	1},
		{	{	92,	"92-FWA Pressure Previous Day",		"FWA_PPD"		},	typDouble,	1},
		{	{	93,	"93-FWA Temperature Previous Hour",	"FWA_TPH"		},	typDouble,	1},
		{	{	94,	"94-FWA Temperature Previous Day",	"FWA_TPD"		},	typDouble,	1},
		{	{	95,	"95-FWA Observed Density Previous Hour","FWA_OBPH"		},	typDouble,	1},
		{	{	96,	"96-FWA Observed Density Previous Day",	"FWA_OBPD"		},	typDouble,	1},
		{	{	97,	"97-FWA Base Density Previous Hour",	"FWA_BDPH"		},	typDouble,	1},
		{	{	98,	"98-FWA Base Density Previous Day",	"FWA_BDPD"		},	typDouble,	1},
		{	{	99,	"99-Average Flowrate Option",		"FAVG_OPT"		},	typUINT8,	0},
		{	{	100,	"100-Average Flowrate Previous Hour",	"TWA_FPH"		},	typDouble,	1},
		{	{	101,	"101-Average Flowrate Previous Day",	"TWA_FPD"		},	typDouble,	1},
		{	{	102,	"",					""			},	paramNone,	0},
		{	{	103,	"",					""			},	paramNone,	0},
		{	{	104,	"104-Mole % Neo Pentane",		"M%_NEO"		},	typDouble,	0},
		{	{	105,	"105-Mole % Methane",			"M%_METH"		},	typDouble,	0},
		{	{	106,	"106-Mole % Ethane",			"M%_ETHA"		},	typDouble,	0},
		{	{	107,	"107-Mole % Propane",			"M%_PROPA"		},	typDouble,	0},
		{	{	108,	"108-Mole % n Butane",			"M%_NBUTA"		},	typDouble,	0},
		{	{	109,	"109-Mole % i Butane",			"M%_IBUTA"		},	typDouble,	0},
		{	{	110,	"110-Mole % n Pentane",			"M%_NPENT"		},	typDouble,	0},
		{	{	111,	"111-Mole % i Pentane",			"M%_IPENT"		},	typDouble,	0},
		{	{	112,	"112-Mole % Hexane",			"M%_HEXA"		},	typDouble,	0},
		{	{	113,	"113-Mole % Heptane",			"M%_HEPT"		},	typDouble,	0},
		{	{	114,	"114-Mole % Ethene",			"M%_ETHE"		},	typDouble,	0},
		{	{	115,	"115-Mole % Propene",			"M%_PROPE"		},	typDouble,	0},
		{	{	116,	"116-Mole % n Butene",			"M%_NBUTE"		},	typDouble,	0},
		{	{	117,	"117-Mole % Nitrogen",			"M%_NITRO"		},	typDouble,	0},
		{	{	118,	"118-Mole % Oxygen",			"M%_OXY"		},	typDouble,	0},
		{	{	119,	"119-Mole % Carbon Dioxide",		"M%_CO2"		},	typDouble,	0},
		{	{	120,	"120-Mole % Hydrogen Sulfide",		"M%_H2S"		},	typDouble,	0},
		{	{	121,	"121-Critical Temperature",		"CRT_TEMP"		},	typDouble,	1},
		{	{	122,	"122-Critical Pressure",		"CRT_PRES" 		},	typDouble,	1},
		{	{	123,	"123-Vapor Pressure Option",		"VAPR_PRS"		},	typUINT8,	0},
		{	{	124,	"124-Mole % Octane",			"M%_OCTAN"		},	typDouble,	0},
		{	{	125,	"125-Mole % Nonane",			"M%_NONAN"		},	typDouble,	0},
		{	{	126,	"126-Mole % Decane",			"M%_DECAN"		},	typDouble,	0},
		{	{	127,	"127-Mole % Helium",			"M%_HELIU" 		},	typDouble,	0},
		{	{	128,	"128-Mole % Water",			"M%_WATER" 		},	typDouble,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},

	204,	// Liquid Meters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	typStr20,	0},
		{	{	1,	"1-Meter Description",			"DESC"			},	typStr30,	0},
		{	{	2,	"2-Meter Name Model",			"NAME"			},	typStr20,	0},
		{	{	3,	"3-Meter Serial Number",		"SN"			},	typStr20,	0},
		{	{	4,	"4-Meter Size",				"SIZE"			},	typDouble,	0},
		{	{	5,	"5-Station Number",			"STATION"		},	typUINT8,	0},
		{	{	6,	"6-Enabled Disabled Option",		"ENBL_OPT"		},	typUINT8,	0},
		{	{	7,	"7-Volume Mass Option",			"MASS_OPT"		},	typUINT8,	0},
		{	{	8,	"8-Retroactive Adjustment Option",	"RTRO_OPT"		},	typUINT8,	0},
		{	{	9,	"9-Alternate Mode Option",		"ALTM_OPT"		},	typUINT8,	0},
		{	{	10,	"10-Flowrate Alarm Option",		"ALM_OPT"		},	typUINT8,	0},
		{	{	11,	"11-SRBX on Clear",			"SRBX_CLR"		},	typUINT8,	0},
		{	{	12,	"12-SRBX on Set",			"SRBX_SET"		},	typUINT8,	0},
		{	{	13,	"13-Flowrate Alarm Code",		"FAL_CODE"		},	typUINT8,	1},
		{	{	14,	"14-Low Flow Alarm",			"LOW_ALM"		},	typDouble,	0},
		{	{	15,	"15-High Flow Alarm",			"HIGH_ALM"		},	typDouble,	0},
		{	{	16,	"16-Alarm Deadband",			"ALM_DBND"		},	typDouble,	0},
		{	{	17,	"17-Flow Calculation Alarm Option",	"FLWC_OPT"		},	typUINT8,	0},
		{	{	18,	"18-Flow Calculation Alarm Code",	"FCA_CODE"		},	typUINT8,	1},
		{	{	19,	"19-Densitometer Option",		"DEN_OPT"		},	typUINT8,	0},
		{	{	20,	"20-Density Logical",			"DEN_LOG"		},	typUINT8,	0},
		{	{	21,	"21-Meter Density",			"MTR_DEN"		},	typDouble,	1},
		{	{	22,	"22-Observed Density",			"OBS_DEN"		},	typDouble,	1},
		{	{	23,	"23-Base Density",			"B_DEN"			},	typDouble,	1},
		{	{	24,	"",					""			},	paramNone,	0},
		{	{	25,	"25-Maintenance Accum Reset",		"MNT_RST"		},	typUINT8,	0},
		{	{	26,	"26-Alternate Accum Reset",		"ALT_RST"		},	typUINT8,	0},
		{	{	27,	"27-Uncorrected Flow Rate",		"FLW_TLP"		},	typTLP,	0},
		{	{	28,	"28-Pressure Input TLP",		"SP_TLP"		},	typTLP,	0},
		{	{	29,	"29-Temperature Input TLP",		"T_TLP"			},	typTLP,	0},
		{	{	30,	"30-BSW Input TLP",			"BSW_TLP"		},	typTLP,	0},
		{	{	31,	"31-Flow Input Frequency",		"FLW_FREQ"		},	typDouble,	1},
		{	{	32,	"32-Flow Input Pulse Accumulation",	"FLW_ACC"		},	typUINT32,	1},
		{	{	33,	"33-Pressure Input Value",		"SP_VAL"		},	typDouble,	0},
		{	{	34,	"34-Temperature Input Value",		"T_VAL"			},	typDouble,	0},
		{	{	35,	"35-BSW Percent Value",			"BSW_VAL"		},	typDouble,	0},
		{	{	36,	"36-Low Flow Cutoff",			"LOW_FLW"		},	typDouble,	0},
		{	{	37,	"",					""			},	paramNone,	0},
		{	{	38,	"",					""			},	paramNone,	0},
		{	{	39,	"39-Combined Correction Factor",	"CCF"			},	typDouble,	1},
		{	{	40,	"40-Correction Factor Sediment & Water","CSW"			},	typDouble,	1},
		{	{	41,	"41-Alpha",				"ALPHA"			},	typDouble,	1},
		{	{	42,	"42-CTPL Obs to Base In Use",		"CTPLR_OB"		},	typDouble,	1},
		{	{	43,	"43-CTPL Obs to Alt In Use",		"CTPLR_BA"		},	typDouble,	1},
		{	{	44,	"44-CTL Base to Alt",			"CTL_BA"		},	typDouble,	1},
		{	{	45,	"45-CPL Base to Alt",			"CPL_BA"		},	typDouble,	1},
		{	{	46,	"46-CTPL Base to Alt",			"CTPL_BA"		},	typDouble,	1},
		{	{	47,	"47-Compressibility Factor Base to Alt","F_BA"			},	typDouble,	1},
		{	{	48,	"48-CTL Observed to Base",		"CTL_OB"		},	typDouble,	1},
		{	{	49,	"49-CPL Observed to Base",		"CPL_OB"		},	typDouble,	1},
		{	{	50,	"50-CTPL Observed to Base",		"CTPL_OB"		},	typDouble,	1},
		{	{	51,	"51-Comp Factor Observed to Base",	"F_OB"			},	typDouble,	1},
		{	{	52,	"52-Spool Correction Option",		"SPL_OPT"		},	typUINT8,	0},
		{	{	53,	"53-Spool Linear Coefficient Option",	"SPLL_OPT"		},	typUINT8,	0},
		{	{	54,	"54-Spool Rotor Coefficient Option",	"SPLR_OPT"		},	typUINT8,	0},
		{	{	55,	"55-Spool Modulus E Option",		"SPLM_OPT"		},	typUINT8,	0},
		{	{	56,	"56-Spool Meter Linear Coefficient",	"SPL_COEF"		},	typDouble,	0},
		{	{	57,	"57-Spool Rotor Linear Coefficient",	"SPL_RTR"		},	typDouble,	0},
		{	{	58,	"58-Spool Modulus E",			"SPL_MOD"		},	typDouble,	0},
		{	{	59,	"59-Spool Meter Housing Radius",	"SPL_HRAD"		},	typDouble,	0},
		{	{	60,	"60-Spool Wall Thickness",		"SPL_WALL"		},	typDouble,	0},
		{	{	61,	"61-Spool Rotor Hub Area",		"SPL_HUB"		},	typDouble,	0},
		{	{	62,	"62-Spool Poisson Ratio",		"SPL_PRAT"		},	typDouble,	0},
		{	{	63,	"63-Spool Calibrated Temperature",	"SPL_CALT"		},	typDouble,	0},
		{	{	64,	"64-Spool Calibrated Pressure",		"SPL_CALP"		},	typDouble,	0},
		{	{	65,	"65-Spool Meter CTS",			"SPL_CTS"		},	typDouble,	0},
		{	{	66,	"66-Spool Meter CPS",			"SPL_CPS"		},	typDouble,	0},
		{	{	67,	"",					""			},	paramNone,	0},
		{	{	68,	"68-K Factor Option",			"KF_OPT"		},	typUINT8,	0},
		{	{	69,	"69-Meter Factor Option",		"MF_OPT"		},	typUINT8,	0},
		{	{	70,	"70-K Factor Manual Value",		"KF_MAN"		},	typDouble,	0},
		{	{	71,	"71-Meter Factor Manual Value",		"MF_MAN"		},	typDouble,	0},
		{	{	72,	"72-K Factor Live",			"KF_LIVE"		},	typDouble,	0},
		{	{	73,	"73-Meter Factor Live",			"MF_LIVE"		},	typDouble,	0},
		{	{	74,	"74-K Factor In Use",			"KF_USE"		},	typDouble,	1},
		{	{	75,	"75-Meter Factor In Use",		"MF_USE"		},	typDouble,	1},
		{	{	76,	"76-Meter Factor K Factor Option",	"MF_KF_OPT"		},	typUINT8,	0},
		{	{	77,	"77-Meter Factor 1 K Factor 1",		"MF_KF1"		},	typDouble,	0},
		{	{	78,	"78-Flowrate Frequency 1",		"FR_FRQ1"		},	typDouble,	0},
		{	{	79,	"79-Meter Factor 2 K Factor 2",		"MF_KF2"		},	typDouble,	0},
		{	{	80,	"80-Flowrate Frequency 2",		"FR_FRQ2"		},	typDouble,	0},
		{	{	81,	"81-Meter Factor 3 K Factor 3",		"MF_KF3"		},	typDouble,	0},
		{	{	82,	"82-Flowrate Frequency 3",		"FR_FRQ3"		},	typDouble,	0},
		{	{	83,	"83-Meter Factor 4 K Factor 4",		"MF_KF4"		},	typDouble,	0},
		{	{	84,	"84-Flowrate Frequency 4",		"FR_FRQ4"		},	typDouble,	0},
		{	{	85,	"85-Meter Factor 5 K Factor 5",		"MF_KF5"		},	typDouble,	0},
		{	{	86,	"86-Flowrate Frequency 5",		"FR_FRQ5"		},	typDouble,	0},
		{	{	87,	"87-Meter Factor 6 K Factor 6",		"MF_KF6"		},	typDouble,	0},
		{	{	88,	"88-Flowrate Frequency 6",		"FR_FRQ6"		},	typDouble,	0},
		{	{	89,	"89-Meter Factor 7 K Factor 7",		"MF_KF7"		},	typDouble,	0},
		{	{	90,	"90-Flowrate Frequency 7",		"FR_FRQ7"		},	typDouble,	0},
		{	{	91,	"91-Meter Factor 8 K Factor 8",		"MF_KF8"		},	typDouble,	0},
		{	{	92,	"92-Flowrate Frequency 8",		"FR_FRQ8"		},	typDouble,	0},
		{	{	93,	"93-Meter Factor 9 K Factor 9",		"MF_KF9"		},	typDouble,	0},
		{	{	94,	"94-Flowrate Frequency 9",		"FR_FRQ9"		},	typDouble,	0},
		{	{	95,	"95-Meter Factor 10 K Factor 10",	"MF_KF10"		},	typDouble,	0},
		{	{	96,	"96-Flowrate Frequency 10",		"FR_FRQ10"		},	typDouble,	0},
		{	{	97,	"97-Meter Factor 11 K Factor 11",	"MF_KF11"		},	typDouble,	0},
		{	{	98,	"98-Flowrate Frequency 11",		"FR_FRQ11"		},	typDouble,	0},
		{	{	99,	"99-Meter Factor 12 K Factor 12",	"MF_KF12"		},	typDouble,	0},
		{	{	100,	"100-Flowrate Frequency 12",		"FR_FRQ12"		},	typDouble,	0},
		{	{	101,	"",					""			},	paramNone,	0},
		{	{	102,	"102-Flowmeter Value",			"FLW_VAL"		},	typDouble,	0},
		{	{	103,	"103-Indicated Quantity Rate",		"IQ_RATE"		},	typDouble,	1},
		{	{	104,	"104-Gross Volume Flow Rate",		"GV_RATE"		},	typDouble,	1},
		{	{	105,	"105-Gross Standared Volume Flow Rate",	"GSV_RATE"		},	typDouble,	1},
		{	{	106,	"106-Net Standared Volume Flow Rate",	"NSV_RATE"		},	typDouble,	1},
		{	{	107,	"107-BSW Volume Rate",			"BSW_RATE"		},	typDouble,	1},
		{	{	108,	"108-Mass Flow Rate",			"MAS_RATE"		},	typDouble,	1},
		{	{	109,	"109-Indicated Quantity Current Hour",	"IQ_HR"			},	typDouble,	1},
		{	{	110,	"110-Gross Volume Current Hour",	"GV_HR"			},	typDouble,	1},
		{	{	111,	"111-Gross Standard Volume Current Hour","GSV_HR"		},	typDouble,	1},
		{	{	112,	"112-Net Standard Volume Current Hour",	"NSV_HR"		},	typDouble,	1},
		{	{	113,	"113-BSW Volume Current Hour",		"BSW_HR"		},	typDouble,	1},
		{	{	114,	"114-Mass Current Hour",		"MAS_HR"		},	typDouble,	1},
		{	{	115,	"115-Indicated Quantity Last Hour",	"IQ_LHR"		},	typDouble,	1},
		{	{	116,	"116-Gross Volume Last Hour",		"GV_LHR"		},	typDouble,	1},
		{	{	117,	"117-Gross Standard Volume Last Hour",	"GSV_LHR"		},	typDouble,	1},
		{	{	118,	"118-Net Standard Volume Last Hour",	"NSV_LHR"		},	typDouble,	1},
		{	{	119,	"119-BSW Volume Last Hour",		"BSW_LHR"		},	typDouble,	1},
		{	{	120,	"120-Mass Last Hour",			"MAS_LHR"		},	typDouble,	1},
		{	{	121,	"121-Indicated Quantity Today",		"IQ_TDY"		},	typDouble,	1},
		{	{	122,	"122-Gross Volume Today",		"GV_TDY"		},	typDouble,	1},
		{	{	123,	"123-Gross Standard Volume Today",	"GSV_TDY"		},	typDouble,	1},
		{	{	124,	"124-Net Standard Volume Today",	"NSV_TDY"		},	typDouble,	1},
		{	{	125,	"125-BSW Volume Today",			"BSW_TDY"		},	typDouble,	1},
		{	{	126,	"126-Mass Today",			"MAS_TDY"		},	typDouble,	1},
		{	{	127,	"127-Indicated Quantity Yesterday",	"IQ_YDAY"		},	typDouble,	1},
		{	{	128,	"128-Gross Volume Yesterday",		"GV_YDAY"		},	typDouble,	1},
		{	{	129,	"129-Gross Standard Volume Yesterday",	"GSV_YDAY"		},	typDouble,	1},
		{	{	130,	"130-Net Standard Volume Yesterday",	"NSV_YDAY"		},	typDouble,	1},
		{	{	131,	"131-BSW Volume Yesterday",		"BSW_YDAY"		},	typDouble,	1},
		{	{	132,	"132-Mass Yesterday",			"MAS_YDAY"		},	typDouble,	1},
		{	{	133,	"133-Indicated Quantity This Month",	"IQ_MTH"		},	typDouble,	1},
		{	{	134,	"134-Gross Volume This Month",		"GV_MTH"		},	typDouble,	1},
		{	{	135,	"135-Gross Standard Volume This Month",	"GSV_MTH"		},	typDouble,	1},
		{	{	136,	"136-Net Standard Volume This Month",	"NSV_MTH"		},	typDouble,	1},
		{	{	137,	"137-BSW Volume This Month",		"BSW_MTH"		},	typDouble,	1},
		{	{	138,	"138-Mass This Month",			"MAS_MTH"		},	typDouble,	1},
		{	{	139,	"139-Indicated Quantity Prev Month",	"IQ_PMTH"		},	typDouble,	1},
		{	{	140,	"140-Gross Volume Previous Month",	"GV_PMTH"		},	typDouble,	1},
		{	{	141,	"141-Gross Standard Volume Prev Month",	"GSV_PMTH"		},	typDouble,	1},
		{	{	142,	"142-Net Standard Volume Prev Month",	"NSV_PMTH"		},	typDouble,	1},
		{	{	143,	"143-BSW Volume Previous Month",	"BSW_PMTH"		},	typDouble,	1},
		{	{	144,	"144-Mass Previous Month",		"MAS_PMTH"		},	typDouble,	1},
		{	{	145,	"145-Indicated Quantity Total Accum",	"IQ_TOT"		},	typDouble,	1},
		{	{	146,	"146-Gross Volume Total Accum",		"GV_TOT"		},	typDouble,	1},
		{	{	147,	"147-Gross Standard Volume Total Accum","GSV_TOT"		},	typDouble,	1},
		{	{	148,	"148-Net Standard Volume Total Accum",	"NSV_TOT"		},	typDouble,	1},
		{	{	149,	"149-BSW Volume Total Accum",		"BSW_TOT"		},	typDouble,	1},
		{	{	150,	"150-Mass Total Accum",			"MAS_TOT"		},	typDouble,	1},
		{	{	151,	"151-Operating Mode Options",		"MTN_OPT "		},	typUINT8,	0},
		{	{	152,	"152-Maintenance Mode Comments Field",	"MNT_CMT"		},	typStr40,	0},
		{	{	153,	"153-Maintenance Mode Time",		"MNT_TIME"		},	typDouble,	1},
		{	{	154,	"",					""			},	paramNone,	0},
		{	{	155,	"155-Maintenance Mode Indicated Qty",	"MNT_IQ"		},	typDouble,	1},
		{	{	156,	"156-Maintenance Mode Gross Volume",	"MNT_GV"		},	typDouble,	1},
		{	{	157,	"157-Maintenance Mode Standard Volume",	"MNT_GSV"		},	typDouble,	1},
		{	{	158,	"158-Maintenance Mode Net Std Volume",	"MNT_NSV"		},	typDouble,	1},
		{	{	159,	"159-Maintenance Mode BSW Volume",	"MNT_BSW"		},	typDouble,	1},
		{	{	160,	"160-Maintenance Mode Mass",		"MNT_MASS"		},	typDouble,	1},
		{	{	161,	"161-Alternate Mode Comments Field",	"ALT_CMT"		},	typStr40,	0},
		{	{	162,	"162-Alternate Mode Time",		"ALT_TIME"		},	typDouble,	1},
		{	{	163,	"",					""			},	paramNone,	0},
		{	{	164,	"164-Alternate Mode Indicated Qty",	"ALT_IQ"		},	typDouble,	1},
		{	{	165,	"165-Alternate Mode Gross Volume",	"ALT_GV"		},	typDouble,	1},
		{	{	166,	"166-Alternate Mode Standard Volume",	"ALT_GSV"		},	typDouble,	1},
		{	{	167,	"167-Alternate Mode Net Std Volume",	"ALT_NSV"		},	typDouble,	1},
		{	{	168,	"168-Alternate Mode BSW Volume",	"ALT_BSW"		},	typDouble,	1},
		{	{	169,	"169-Alternate Mode Mass",		"ALT_MASS"		},	typDouble,	1},
		{	{	170,	"170-Prove Sequence Number",		"PRV_NO"		},	typUINT32,	0},
		{	{	171,	"171-Last Prove Date and Time",		"PRV_TD"		},	typTime,	0},	// TIME
		{	{	172,	"172-Last Prove Flow Rate",		"PRV_FLW"		},	typDouble,	0},
		{	{	173,	"173-Prove Sequence Counter",		"PRV_CNTR"		},	paramNone,	0},
		{	{	174,	"174-Average Flowrate Current Hour",	"TWA_FCH"		},	typDouble,	1},
		{	{	175,	"175-Average Flowrate Previous Hour",	"TWA_FPH"		},	typDouble,	1},
		{	{	176,	"176-Average Flowrate Current Day",	"TWA_FCD"		},	typDouble,	1},
		{	{	177,	"177-Average Flowrate Previous Day",	"TWA_FPD"		},	typDouble,	1},
		{	{	178,	"178-Raw Pulses Current Hour",		"RAW_CUR"		},	typDouble,	1},
		{	{	179,	"179-Raw Pulses Previous Hour",		"RAW_PRV"		},	typDouble,	1},
		{	{	180,	"180-Raw Pulses Today",			"RAW_TDY"		},	typDouble,	1},
		{	{	181,	"181-Raw Pulses Yesterday",		"RAW_YDAY"		},	typDouble,	1},
		{	{	182,	"182-Raw Pulses This Month",		"RAW_MTH"		},	typDouble,	1},
		{	{	183,	"183-Raw Pulses Last Month",		"RAW_LMTH"		},	typDouble,	1},
		{	{	184,	"184-Raw Pulses Total Accumulation",	"RAW_TOT"		},	typDouble,	1},
		{	{	185,	"185-Current Meter Input Status Code",	"CUR_STS"		},	typUINT8,	1},
		{	{	186,	"186-Current Hour Meter Input Status",	"CH_STS"		},	typUINT8,	1},
		{	{	187,	"187-Previous Hour Meter Input Status",	"PH_STS"		},	typUINT8,	1},
		{	{	188,	"188-Current Day Meter Input Status",	"CD_STS"		},	typUINT8,	1},
		{	{	189,	"189-Previous Day Meter Input Status",	"PD_STS"		},	typUINT8,	1},
		{	{	190,	"190-FWA CTPL Current Hour",		"FWACTPCH"		},	typDouble,	1},
		{	{	191,	"191-FWA CTPL Previous Hour",		"FWACTPPH"		},	typDouble,	1},
		{	{	192,	"192-FWA CTPL Current Day",		"FWACTPCD"		},	typDouble,	1},
		{	{	193,	"193-FWA CTPL Previous Day",		"FWACTPPD"		},	typDouble,	1},
		{	{	194,	"194-FWA Pressure Current Hour",	"FWA_PCH"		},	typDouble,	1},
		{	{	195,	"195-FWA Pressure Previous Hour",	"FWA_PPH"		},	typDouble,	1},
		{	{	196,	"196-FWA Pressure Current Day",		"FWA_PCD"		},	typDouble,	1},
		{	{	197,	"197-FWA Pressure Previous Day",	"FWA_PPD"		},	typDouble,	1},
		{	{	198,	"198-FWA Temperature Current Hour",	"FWA_TCH"		},	typDouble,	1},
		{	{	199,	"199-FWA Temperature Previous Hour",	"FWA_TPH"		},	typDouble,	1},
		{	{	200,	"200-FWA Temperature Current Day",	"FWA_TCD"		},	typDouble,	1},
		{	{	201,	"201-FWA Temperature Previous Day",	"FWA_TPD"		},	typDouble,	1},
		{	{	202,	"202-FWA CPLm Current Hour",		"FWA_CPCH"		},	typDouble,	1},
		{	{	203,	"203-FWA CPLm Previous Hour",		"FWA_CPPH"		},	typDouble,	1},
		{	{	204,	"204-FWA CPLm Current Day",		"FWA_CPCD"		},	typDouble,	1},
		{	{	205,	"205-FWA CPLm Previous Day",		"FWA_CPPD"		},	typDouble,	1},
		{	{	206,	"206-FWA CTLm Current Hour",		"FWA_CTCH"		},	typDouble,	1},
		{	{	207,	"207-FWA CTLm Previous Hour",		"FWA_CTPH"		},	typDouble,	1},
		{	{	208,	"208-FWA CTLm Current Day",		"FWA_CTCD"		},	typDouble,	1},
		{	{	209,	"209-FWA CTLm Previous Day",		"FWA_CTPD"		},	typDouble,	1},
		{	{	210,	"210-FWA Obs Density Current Hour",	"FWA_ODCH"		},	typDouble,	1},
		{	{	211,	"211-FWA Obs Density Previous Hour",	"FWA_ODPH"		},	typDouble,	1},
		{	{	212,	"212-FWA Obs Density Current Day",	"FWA_ODCD"		},	typDouble,	1},
		{	{	213,	"213-FWA Obs Density Previous Day",	"FWA_ODPD"		},	typDouble,	1},
		{	{	214,	"214-FWA Base Density Current Hour",	"FWA_BDCH"		},	typDouble,	1},
		{	{	215,	"215-FWA Base Density Previous Hour",	"FWA_BDPH"		},	typDouble,	1},
		{	{	216,	"216-FWA Base Density Current Day",	"FWA_BDCD"		},	typDouble,	1},
		{	{	217,	"217-FWA Base Density Previous Day",	"FWA_BDPD"		},	typDouble,	1},
		{	{	218,	"218-FWA CPSm Current Hour",		"FWA_PSCH"		},	typDouble,	1},
		{	{	219,	"219-FWA CPSm Previous Hour",		"FWA_PSPH"		},	typDouble,	1},
		{	{	220,	"220-FWA CPSm Current Day",		"FWA_PSCD"		},	typDouble,	1},
		{	{	221,	"221-FWA CPSm Previous Day",		"FWA_PSPD"		},	typDouble,	1},
		{	{	222,	"222-FWA CTSm Current Hour",		"FWA_TSCH"		},	typDouble,	1},
		{	{	223,	"223-FWA CTSm Previous Hour",		"FWA_TSPH"		},	typDouble,	1},
		{	{	224,	"224-FWA CTSm Current Day",		"FWA_TSCD"		},	typDouble,	1},
		{	{	225,	"225-FWA CTSm Previous Day",		"FWA_TSPC"		},	typDouble,	1},
		{	{	226,	"226-Top of Hour Indicated Total Accum","TOP_IND"		},	typDouble,	1},
		{	{	227,	"227-Top of Hour Gross Total Accum",	"TOP_GV"		},	typDouble,	1},
		{	{	228,	"228-Top of Hour Gross Std Total Accum","TOP_GSV"		},	typDouble,	1},
		{	{	229,	"229-Top of Hour Net Std Total Accum",	"TOP_NSV"		},	typDouble,	1},
		{	{	230,	"230-Top of Hour S & W Total Accum",	"TOP_S&W"		},	typDouble,	1},
		{	{	231,	"231-Top of Hour Mass Total Accum",	"TOP_MASS"		},	typDouble,	1},
		{	{	232,	"232-Top of Hour Raw Pulses Total Accum","TOP_RAW"		},	typDouble,	1},
		{	{	233,	"233-FWA K Factor Current Hour",	"FWA_KFCH"		},	typDouble,	1},
		{	{	234,	"234-FWA K Factor Previous Hour",	"FWA_KFPH"		},	typDouble,	1},
		{	{	235,	"235-FWA K Factor Current Day",		"FWA_KFCD"		},	typDouble,	1},
		{	{	236,	"236-FWA K Factor Previous Day",	"FWA_KFPD"		},	typDouble,	1},
		{	{	237,	"237-FWA Meter Factor Current Hour",	"FWA_MFCH"		},	typDouble,	1},
		{	{	238,	"238-FWA Meter Factor Previous Hour",	"FWA_MFPH"		},	typDouble,	1},
		{	{	239,	"239-FWA Meter Factor Current Day",	"FWA_MFCD"		},	typDouble,	1},
		{	{	240,	"240-FWA Meter Factor Previous Day",	"FWA_MFPD"		},	typDouble,	1},
		{	{	241,	"241-Flow Minutes Current Hour",	"FLW_MCH"		},	typDouble,	1},
		{	{	242,	"242-Flow Minutes Current Day",		"FLW_MCD"		},	typDouble,	1},
		{	{	243,	"243-Vapor Pressure",			"VAPR_PRE"		},	typDouble,	1},
		{	{	244,	"244-Shrinkage Factor",			"SHRNKG_F"		},	typDouble,	0},
		{	{	245,	"245-Flow Minute Accumulation",		"FLW_MN_A"		},	typDouble,	1},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	};

//////////////////////////////////////////////////////////////////////////
//
// Model Specific Point Types (T) Definitions
//

#include "emroctlp.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Model Specific Parameters (P) Definitions
//

#include "emrocfb103.cpp"
#include "emrocfb107.cpp"
#include "emrocfb107wo.cpp"
#include "emrocfb407.cpp"
#include "emrocfb500.cpp"
#include "emrocroc300.cpp"
#include "emrocroc800.cpp"
#include "emrocroc800l.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Predefined TLP Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEmRocPreDefinedDialog, CStdDialog);
		
// Constructor

CEmRocPreDefinedDialog::CEmRocPreDefinedDialog(CEmersonRocMasterDriver3 &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;

	FindPointTypeList();

	FindParamterList();

	for( UINT u = 0; u < elements(m_Range); u++ ) {

		m_Range[u].Empty();
		}

	SetName(TEXT("EmRocPDDlg"));
	}

// Message Map

AfxMessageMap(CEmRocPreDefinedDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(404,	BN_CLICKED,	OnButton)

	AfxDispatchNotify(200,	CBN_SELCHANGE,	OnSelChange)
	AfxDispatchNotify(201,	CBN_SELCHANGE,	OnSelChange)
	AfxDispatchNotify(202,	CBN_SELCHANGE,	OnSelChange)

	AfxDispatchNotify(400,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(401,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(402,	EN_KILLFOCUS,	OnEditChange)

	AfxMessageEnd(CEmRocPreDefinedDialog)
	};

// Message Handlers

BOOL CEmRocPreDefinedDialog::OnInitDialog(CWnd &Wnd, DWORD dwData)
{
	InitEdit();

	LoadPointType();

	OnSelChange(ID_T, Wnd);

	InitType();

	if( !MatchType() ) {

		InitCustom();
		}

	return FALSE;
	}

// Notification Handlers

void CEmRocPreDefinedDialog::OnButton(UINT uID, CWnd &Wnd)
{
	if( uID == ID_C ) {

		CButton &Button = (CButton &)GetDlgItem(ID_C);

		Button.IsChecked() ? InitCustom() : ExitCustom();
		}
	}

void CEmRocPreDefinedDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == ID_T ) {

		LoadLogical();

		LoadParameter();

		SetShortName();

		SetType();

		SetTLP();

		DoEnables();
		}

	if( uID == ID_L ) {

		SetShortName();	

		SetTLP();
		}

	if( uID == ID_P ) {

		SetShortName();

		SetType();

		SetTLP();
		}
	}

void CEmRocPreDefinedDialog::OnEditChange(UINT uID, CWnd &Wnd)
{
	if( IsEdit(uID) ) {

		INT  nCheck = tstrtol(GetDlgItem(uID).GetWindowText(), NULL, 10);

		UINT uIndex = uID - (ID_T + 200);

		m_Range[uIndex].Set(GetMin(uID), GetMax(uID));

		if( nCheck >= m_Range[uIndex].m_nFrom && nCheck <= m_Range[uIndex].m_nTo ) {

			m_Range[uIndex].Empty();
			}
		}
	}

// Command Handlers

BOOL CEmRocPreDefinedDialog::OnOkay(UINT uID)
{
	CError Error(TRUE);

	if( IsCustomTLP() ) {

		for( UINT e = ID_T + 200; e <= ID_P + 200; e++ ) {

			if( !CheckRange(e, Error) ) {

				Error.Show(ThisObject);

				GetDlgItem(e).SetFocus();
					
				return TRUE;
				}		
			}
		}

	CString Tlp;

	if( !GetCustomTLP(Tlp) ) {

		if( CheckInit() ) {

			Tlp  = GetDlgItem(302).GetWindowText();

			Tlp += ",%s";

			Tlp.Printf(Tlp, GetDlgItem(301).GetWindowText());
			}
		}

	if( !Tlp.IsEmpty() ) {

		CAddress Addr;

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Tlp ) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CEmRocPreDefinedDialog::LoadPointType(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_T);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	CStringArray Array;
		
	Array.Append(CString(IDS_DRIVER_NOSELECTION));

	CEmersonRoc3MasterDeviceOptions * pOpt = (CEmersonRoc3MasterDeviceOptions *) m_pConfig;

	UINT uSel = 0;

	UINT uPointType = NOTHING;

	if( m_pAddr->m_Ref ) {

		uPointType = m_pDriver->GetPointType(pOpt, *m_pAddr);
		}

	for( UINT u = 0; u < m_PointTypes.GetCount(); u++ ) {

		CString Name = m_PointTypes[u].m_Name;

		if( !Name.IsEmpty() ) {

			if( pOpt ) {

				if( !(m_PointTypes[u].m_Device & pOpt->GetModelMask()) ) {

					continue;
					}
				}

			Array.Append(m_PointTypes[u].m_Name);

			if( m_PointTypes[u].m_Number == uPointType ) {

				uSel = Array.GetCount() - 1;
				}
			}
		}
	
	Box.AddStrings(Array);

	Box.SetRedraw (TRUE);

	Box.Invalidate(TRUE);

	Box.SetCurSel(uSel);
	}

void CEmRocPreDefinedDialog::LoadLogical(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_L);

	if( CheckInit() ) {

		Box.SetRedraw(FALSE);

		Box.ResetContent();

		CStringArray Array;

		UINT uSel = 0;

		if( m_pAddr->m_Ref ) {

			uSel = m_pDriver->GetLogical(m_pConfig, *m_pAddr);
			}

		UINT uPointType = GetPointType();

		UINT uCount = m_pDriver->GetLogicalCount(m_pConfig);

		for( UINT u = 0; u < uCount; u++ ) {

			if( uPointType > 0 && uPointType < 6 ) {

				Array.Append(m_pLogical[u].m_Short);
				}
			else {
				CString Text = "%u";

				Text.Printf(Text, m_pLogical[u].m_Number);
				
				Array.Append(Text);
				}
			}

		Box.AddStrings(Array);

		Box.SetRedraw (TRUE);

		Box.Invalidate(TRUE);

		Box.EnableWindow(TRUE);

		Box.SetCurSel(uSel);

		return;
		}

	Box.SetCurSel(NOTHING);
	}

void CEmRocPreDefinedDialog::LoadParameter(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_P);

	if( CheckInit() ) {

		Box.SetRedraw(FALSE);

		Box.ResetContent();

		CStringArray Array;

		UINT uSel = 0;

		UINT uParameter = 0;

		if( m_pAddr->m_Ref ) {

			uParameter = m_pDriver->GetParameter(m_pConfig, *m_pAddr);
			}

		UINT uPointType = GetPointType();

		for( UINT i = 0; i < m_Parameters.GetCount(); i++ ) {

			if( m_Parameters[i].m_PointType == uPointType ) {

				for( UINT u = 0; u < elements(m_Parameters[i].m_Param); u++ ) {

					if( m_Parameters[i].m_Param[u].m_Type == paramNone ) {

						if( m_Parameters[i].m_Param[u].m_Param.m_Number == 0 ) {

							break;
							}

						continue;
						}

					Array.Append(m_Parameters[i].m_Param[u].m_Param.m_Name);

					if( m_Parameters[i].m_Param[u].m_Param.m_Number == uParameter ) {

						uSel = Array.GetCount() - 1;
						}
					}
				break;
				}
			}

		Box.AddStrings(Array);

		Box.SetRedraw (TRUE);

		Box.Invalidate(TRUE);

		Box.EnableWindow(TRUE);

		Box.SetCurSel(uSel);

		return;
		}

	Box.SetCurSel(NOTHING);
	}

UINT CEmRocPreDefinedDialog::GetPointType(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_T);

	UINT uPoint = Box.GetCurSel();

	UINT uFind = 0;

	if( uPoint > 0 ) {

		CString Point = Box.GetWindowText();

		uFind = Point.Find('-');

		if( uFind < NOTHING ) {

			uPoint = tstrtol(Point.Mid(0, uFind), NULL, 10);
			}
		}

	return uPoint;
	}

UINT CEmRocPreDefinedDialog::GetLogical(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_L);

	return Box.GetCurSel();
	}

UINT CEmRocPreDefinedDialog::GetParameter(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_P);
	
	CString Text = Box.GetWindowText();

	UINT uFind = Text.Find('-');

	if( uFind < NOTHING ) {

		return tstrtol(Text.Mid(0, uFind), NULL, 10);
		}
	
	return 0;
	}

void CEmRocPreDefinedDialog::SetShortName(void)
{
	if( CheckInit() ) {

		UINT uPointType = GetPointType();

		for( UINT u = 0; u < m_PointTypes.GetCount(); u++ ) {

			if( uPointType == m_PointTypes[u].m_Number ) {

				UINT uLogical = GetLogical();

				UINT uParameter = GetParameter();

				CString Text = m_PointTypes[u].m_Short;

				Text += "_";

				if( m_pDriver->IsIO(uPointType) ) {

					Text += m_pLogical[uLogical].m_Short;
					}
				else {
					CString Number = "%u";

					Number.Printf(Number, m_pLogical[uLogical].m_Number);

					Text += Number;
					}

				Text += "_";

				for( UINT i = 0; i < m_Parameters.GetCount(); i++ ) {

					if( uPointType == m_Parameters[i].m_PointType ) {

						Text += m_Parameters[i].m_Param[uParameter].m_Param.m_Short;

						Text += m_Parameters[i].m_Param[uParameter].m_fReadOnly ? "_RO" : "_RW";

						GetDlgItem(300).SetWindowText(Text);

						return;
						}
					}
				}
			}
		}

	GetDlgItem(300).SetWindowText("<short name>");
	}

void CEmRocPreDefinedDialog::SetType(void)
{
	if( CheckInit() ) {

		CEmersonRocMasterDriver3 * pDriver = (CEmersonRocMasterDriver3 *) m_pDriver;

		if( pDriver ) {

			UINT uPointType = GetPointType();

			for( UINT i = 0; i < m_Parameters.GetCount(); i++ ) {

				if( uPointType == m_Parameters[i].m_PointType ) {

					UINT uType = m_Parameters[i].m_Param[GetParameter()].m_Type;

					CString Text = pDriver->GetTypeText(uType);

					GetDlgItem(301).SetWindowText(Text);

					SetCustomType();

					return;
					}
				}
			}
		}

	GetDlgItem(301).SetWindowText("<type>");
	}

void CEmRocPreDefinedDialog::SetTLP(void)
{
	if( CheckInit() ) {

		UINT uPointType = GetPointType();

		for( UINT u = 0; u < m_PointTypes.GetCount(); u++ ) {

			if( uPointType == m_PointTypes[u].m_Number ) {

				UINT uLogical = GetLogical();

				UINT uParameter = GetParameter();

				CString Text = "%u,%u,%u";

				Text.Printf(Text, uPointType, uLogical, uParameter);
			
				GetDlgItem(302).SetWindowText(Text);

				SetCustomTLP();

				return;
				}
			}
		}
	
	GetDlgItem(302).SetWindowText("<tlp>");
	}

BOOL CEmRocPreDefinedDialog::CheckInit(void)
{
	CComboBox & Box = (CComboBox &) GetDlgItem(ID_T);

	if( Box.GetCurSel() ) {

		return TRUE;
		}

	return FALSE;
	}

// Custom TLP Support

void CEmRocPreDefinedDialog::SetCustomTLP(void)
{
	BOOL fInit = CheckInit();

	CLongArray Array;

	Array.Append(fInit ? GetPointType() : 0);

	Array.Append(fInit ? GetLogical()   : 0);

	Array.Append(fInit ? GetParameter() : 0);

	for( UINT u = 0; u <= 2; u++ ) {

		CString Text = "%u";
			
		Text.Printf(Text, Array.GetAt(u));

		GetDlgItem(u + ID_T + 200).SetWindowText(Text);
		}
	}

void CEmRocPreDefinedDialog::SetCustomType(void)
{
	CComboBox &Box = (CComboBox &)GetDlgItem(403);

	Box.SelectString(GetDlgItem(301).GetWindowText());
	}

void CEmRocPreDefinedDialog::InitEdit(void)
{
	for( UINT e = ID_T + 200; e <= ID_P + 200;e++ ) {

		GetDlgItem(e).SetWindowText("0");
		}
	}

void CEmRocPreDefinedDialog::InitCustom(void)
{
	for( UINT p = ID_T; p <= ID_P; p++ ) {

		CComboBox & Box = (CComboBox &)GetDlgItem(p);

		if( p == 200 ) {

			Box.SetCurSel(0);
			}
		else  {
			Box.ResetContent();

			Box.EnableWindow(FALSE);
			}
		}
	
	SetShortName();

	SetType();
	
	SetTLP();

	CButton &Button = (CButton &)GetDlgItem(ID_C);

	Button.SetCheck(TRUE);

	DoEnables();
	}

void CEmRocPreDefinedDialog::ExitCustom(void)
{
	for( UINT b = ID_T; b <= ID_P; b++ ) {

		INT nData      = tatoi(GetDlgItem(b + 200).GetWindowText());

		CComboBox &Box = (CComboBox &)GetDlgItem(b);

		UINT uCount    = Box.GetCount();
	
		for( UINT u = 0; u < uCount; u++ ) {
			
			CString Text = Box.GetLBText(u);

			UINT uFind   = Text.Find('-');
			
			if( tatoi(Text.Mid(0, uFind)) == nData ) {

				Box.SetCurSel(u);

				break;
				}
			}

		if( b == ID_T ) {

			LoadLogical();

			LoadParameter();
			}
		}

	SetShortName();

	SetType();
	
	SetTLP();

	DoEnables();	
	}

void CEmRocPreDefinedDialog::InitType(void)
{
	CComboBox &Box = (CComboBox &)GetDlgItem(403); 
	
	CEmersonRocMasterDriver3 * pDriver = (CEmersonRocMasterDriver3 *) m_pDriver;

	Box.AddStrings(pDriver->GetTypes());

	Box.SetCurSel(m_pAddr ? m_pAddr->a.m_Extra : 3);
	}

void CEmRocPreDefinedDialog::DoEnables(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_T);

	BOOL fEnable = Box.GetCurSel() == 0;

	for( UINT u = ID_T + 200; u <= ID_P + 1 + 200; u++ ) {

		GetDlgItem(u).EnableWindow(fEnable);
		}

	GetDlgItem(ID_L).EnableWindow(!fEnable);

	GetDlgItem(ID_P).EnableWindow(!fEnable);

	if( !fEnable ) {

		CButton & Button = (CButton &)GetDlgItem(ID_C);

		Button.SetCheck(fEnable);
		}
	}

BYTE CEmRocPreDefinedDialog::GetMin(UINT uID)
{
	return 0;
	}

BYTE CEmRocPreDefinedDialog::GetMax(UINT uID)
{
	if( uID == ID_T + 200 ) {

		return 236;
		}

	return 255;
	}

BOOL CEmRocPreDefinedDialog::IsEdit(UINT uID)
{
	switch( uID ) {

		case ID_T + 200:
		case ID_L + 200:
		case ID_P + 200:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CEmRocPreDefinedDialog::IsCustomTLP(void)
{
	CButton &Button = (CButton &)GetDlgItem(ID_C);

	return Button.IsChecked();
	}

BOOL CEmRocPreDefinedDialog::GetCustomTLP(CString &Tlp)
{
	if( IsCustomTLP() ) {

		for(UINT u = ID_T + 200; u <= ID_P + 200; u++ ) {

			Tlp += GetDlgItem(u).GetWindowText();

			Tlp += ",";
			}

		Tlp += GetDlgItem(ID_P + 1 + 200).GetWindowText();

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmRocPreDefinedDialog::MatchType(void)
{
	CString Predef = GetDlgItem(301).GetWindowText();

	CString Custom = GetDlgItem(403).GetWindowText();

	return Predef == Custom;
	}

BOOL CEmRocPreDefinedDialog::CheckRange(UINT uID, CError &Error)
{
	UINT uIndex = uID - (ID_T + 200);

	if( !m_Range[uIndex].IsEmpty() ) {

		Error.Set(CPrintf("%s must be in the range (%u..%u).", ROC_MSG[uIndex],
								       m_Range[uIndex].m_nFrom,
								       m_Range[uIndex].m_nTo));

		return FALSE;
		}

	return TRUE;
	}

// Model Specific List Support

void CEmRocPreDefinedDialog::FindParamterList(void)
{
	CEmersonRoc3MasterBaseDeviceOptions * pOpts = (CEmersonRoc3MasterBaseDeviceOptions *) m_pConfig;

	if( pOpts ) {

		if( pOpts->IsFlo103() ) {

			m_Parameters.Append(m_pParamFB103, elements(m_pParamFB103));
			
			return;
			}

		if( pOpts->IsFlo407() ) {

			m_Parameters.Append(m_pParamFB407, elements(m_pParamFB407));
			
			return;
			}

		if( pOpts->IsFlo500() ) {

			m_Parameters.Append(m_pParamFB500, elements(m_pParamFB500));
			
			return;
			}

		if( pOpts->IsRoc300() ) {

			m_Parameters.Append(m_pParamROC300, elements(m_pParamROC300));
			
			return;
			}

		if( pOpts->IsFlo107() ) {

			m_Parameters.Append(m_pParamFB107, elements(m_pParamFB107));
			
			return;
			}

		if( pOpts->IsFlo107wo() ) {

			m_Parameters.Append(m_pParamFB107wo, elements(m_pParamFB107wo));

			return;
			}

		if( pOpts->IsRoc800() ) {

			m_Parameters.Append(m_pParamROC800, elements(m_pParamROC800));

			return;
			}

		if( pOpts->IsRoc800L() ) {

			m_Parameters.Append(m_pParamROC800L, elements(m_pParamROC800L));

			return;
			}
		}

	m_Parameters.Append(m_pParameters, elements(m_pParameters));
	}

void CEmRocPreDefinedDialog::FindPointTypeList(void)
{
	CEmersonRoc3MasterBaseDeviceOptions * pOpts = (CEmersonRoc3MasterBaseDeviceOptions *) m_pConfig;

	if( pOpts ) {

		if( pOpts->IsFlo107() ) {

			m_PointTypes.Append(m_pPointTypeFB107, elements(m_pPointTypeFB107));

			return;
			}

		if( pOpts->IsFlo107wo() ) {

			m_PointTypes.Append(m_pPointTypeFB107wo, elements(m_pPointTypeFB107wo));

			return;
			}

		if( pOpts->IsFloType() ) {

			m_PointTypes.Append(m_pPointTypeFB, elements(m_pPointTypeFB));

			return;
			}

		if( pOpts->IsRocType() ) {

			m_PointTypes.Append(m_pPointTypeROC, elements(m_pPointTypeROC));

			return;
			}
		}

	m_PointTypes.Append(m_pPointType, elements(m_pPointType));
	}

// End of File
