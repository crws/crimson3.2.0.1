
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_OPC_UA_HPP

#define	INCLUDE_OPC_UA_HPP

//////////////////////////////////////////////////////////////////////////
//
// XML Parser
//

#undef   global

#include "pugixml.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Data Types
//

#define OpcUaId_Boolean				1
#define OpcUaId_SByte				2
#define OpcUaId_Byte				3
#define OpcUaId_Int16				4
#define OpcUaId_UInt16				5
#define OpcUaId_Int32				6
#define OpcUaId_UInt32				7
#define OpcUaId_Int64				8
#define OpcUaId_UInt64				9
#define OpcUaId_Float				10
#define OpcUaId_Double				11
#define OpcUaId_String				12
#define OpcUaId_DateTime			13
#define OpcUaId_Duration			290
#define OpcUaId_UtcTime				294
#define OpcUaId_LocaleId			295
#define OpcUaId_SignedSoftwareCertificate	344
#define OpcUaId_RedundancySupport		851
#define OpcUaId_ServerState			852
#define OpcUaId_ServerStatusDataType		862

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Tables
//

enum
{
	addrString = 1,
	addrArray  = 80
};

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Tree Builder
//

class COpcUaTreeBuilder
{
public:
	// Actions
	enum
	{
		actionAddNode,
		actionAddFolder,
		actionClimbTree,
	};

	// Operations
	void Reset(CString Root, WCHAR cSep);
	UINT Process(CString &Add, CString Node);

protected:
	// Data Members
	CString m_Path;
	WCHAR   m_cSep;
};

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Driver Options
//

class COpcUaDriverOptions : public CUIItem
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	COpcUaDriverOptions(void);

	// UI Managament
	void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Data Members
	UINT m_Debug;

protected:
	// Meta Data Creation
	void AddMetaData(void);
};

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Device Options
//

class COpcUaDeviceOptions : public CUIItem
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	COpcUaDeviceOptions(void);

	// UI Managament
	void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

	// Persistance
	void PostLoad(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Data Members
	CString    m_HostName;
	UINT       m_Port;
	CString    m_UserName;
	CString    m_Password;
	CByteArray m_FileData;
	CString    m_FileName;
	UINT       m_FileTime;
	UINT	   m_FileAuto;
	CByteArray m_NodeUsed;
	UINT	   m_Partial;
	UINT	   m_StrSize;
	UINT	   m_StrCode;

protected:
	// OPC Data Node
	struct COpcNode
	{
		CString m_Path;
		UINT    m_Type;
		UINT	m_Size;
		UINT	m_Slot;
		BOOL    m_fWrite;
		CString m_Node;
	};

	// Data Node List
	CArray <COpcNode>      m_Nodes;
	CArray <UINT>	       m_Slots;
	CArray <UINT>	       m_Sizes;
	CMap   <CString, UINT> m_Dict;
	CString		       m_Last;
	UINT		       m_StrPitch;
	UINT		       m_StrTable;
	UINT		       m_StrLimit;

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	BOOL  LoadBrowseFile(BOOL fForce);
	DWORD FindFileTime(CFilename Name);
	DWORD FindFileTime(HANDLE hFile);
	void  ParseBrowseData(void);
	bool  ConvertXml(CByteArray &Blob);
	BOOL  SetAddress(CAddress &Addr, UINT uNode);
	UINT  GetAddressNode(CAddress const &Addr);
	UINT  GetStringNode(UINT uTable, UINT uOffset);
	UINT  GetStringTable(COpcNode const &Node);
	UINT  GetStringOffset(COpcNode const &Node);
	UINT  GetArrayNode(UINT uTable, UINT uOffset);
	UINT  GetArrayTable(COpcNode const &Node);
	UINT  GetArrayOffset(COpcNode const &Node);
	UINT  GetNodeType(COpcNode const &Node);
	UINT  GetNodeType(UINT uNode);
	BOOL  CanWriteNode(COpcNode const &Node);
	BOOL  CanWriteNode(UINT uNode);
	UINT  GetNodeImage(COpcNode const &Node);
	UINT  GetNodeImage(UINT uNode);
	BOOL  BrowseDevice(CWnd *pWnd, CFilename File);
	PSTR  Convert(CString const &c);
	UINT  GetWireType(UINT uType);
	bool  CanUseType(UINT uType);
	void  FindStringLimit(void);

	// Friends
	friend class COpcUaDriver;
	friend class COpcUaDialog;
};

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Client Driver
//

class COpcUaDriver : public CBasicCommsDriver
{
public:
	// Constructor
	COpcUaDriver(void);

	// Destructor
	~COpcUaDriver(void);

	// Binding Control
	UINT GetBinding(void);
	void GetBindInfo(CBindInfo &Info);

	// Configuration
	CLASS GetDriverConfig(void);
	CLASS GetDeviceConfig(void);

	// Address Management
	BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
	BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
	BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

protected:
	// Data Members
	COpcUaTreeBuilder m_Build;
	UINT              m_uPos;
	BOOL		  m_fFolder;

	// Friends
	friend class COpcUaDialog;
};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class COpcUaDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	COpcUaDialog(COpcUaDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

protected:
	// Data
	COpcUaDriver        * m_pDriver;
	CAddress            * m_pAddr;
	COpcUaDeviceOptions * m_pConfig;
	BOOL		      m_fPart;
	CImageList            m_Images;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

	// Command Handlers
	BOOL OnOkay(UINT uID);

	// Notification Handlers
	void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
	BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
	BOOL OnTreeExpanding(UINT uID, NMTREEVIEW &Info);
	void OnTreeDblClk(UINT uID, NMHDR &Info);
};

// End of File

#endif
