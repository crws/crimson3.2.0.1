
#include "intern.hpp"

#include "ircon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ircon Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIrconDeviceOptions, CUIItem);

// Constructor

CIrconDeviceOptions::CIrconDeviceOptions(void)
{
	m_Drop	 = "0";
	
	m_fModel = TRUE;
	}

// UI Managament

void CIrconDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CIrconDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop[0]));
	Init.AddByte(BYTE(m_fModel));

	return TRUE;
	}

// Meta Data Creation

void CIrconDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString (Drop);
	Meta_AddBoolean(Model);
	}

//////////////////////////////////////////////////////////////////////////
//
// Ircon Comms Driver
//

// Instantiator

ICommsDriver *	Create_IrconDriver(void)
{
	return New CIrconDriver;
	}

// Constructor

CIrconDriver::CIrconDriver(void)
{
	m_wID		= 0x3352;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Ircon";
	
	m_DriverName	= "Modline";
	
	m_Version	= "1.11";
	
	m_ShortName	= "Ircon Modline";

	AddSpaces();
	}

// Binding Control

UINT	CIrconDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CIrconDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CIrconDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIrconDeviceOptions);
	}

// Implementation

void	CIrconDriver::AddSpaces(void)
{
	AddSpace( New CSpace("TT", "Temperature Request (R) (same as TO)",	 1, addrLongAsLong) );
	AddSpace( New CSpace("EM", "Emissivity",				 2, addrLongAsLong) );
	AddSpace( New CSpace("CE", "Response to Latest Emissivity Write",	53, addrLongAsLong) );
	AddSpace( New CSpace("ES", "E-Slope",					 3, addrLongAsLong) );
	AddSpace( New CSpace("CS", "Response to Latest E-Slope Write",		55, addrLongAsLong) );
	AddSpace( New CSpace("AH", "Analog output high temperature",		28, addrLongAsLong) );
	AddSpace( New CSpace("AL", "Analog output low temperature",		29, addrLongAsLong) );
	AddSpace( New CSpace("AO", "Analog output mode",			30, addrLongAsLong) );
	AddSpace( New CSpace("AA", "Analog Alarm Output",			52, addrLongAsLong) );
	AddSpace( New CSpace("AZ", "Analog Zero Scale",				 6, addrLongAsLong) );
	AddSpace( New CSpace("AF", "Analog Full Scale",				 7, addrLongAsLong) );
	AddSpace( New CSpace("RT", "Response Time",				 4, addrLongAsLong) );
	AddSpace( New CSpace("DR", "Peak Picker Decay Rate",			 5, addrLongAsLong) );
	AddSpace( New CSpace("PR", "Peak picker reset",				37, addrLongAsLong) );
	AddSpace( New CSpace("PS", "Peak Picker Modes/Reset",			 8, addrLongAsLong) );
	AddSpace( New CSpace("PK", "Peak Picker Reset Below",			 9, addrLongAsLong) );
	AddSpace( New CSpace("PD", "Peak Delay",				10, addrLongAsLong) );
	AddSpace( New CSpace("SG", "Input signal mode",				41, addrLongAsLong) );
	AddSpace( New CSpace("ST", "System Alarm Status (R)",			12, addrLongAsLong) );
	AddSpace( New CSpace("SW", "Switch input status (R)",			43, addrLongAsLong) );
	AddSpace( New CSpace("RC", "Instrument temperature (R)",		38, addrLongAsLong) );
	AddSpace( New CSpace("BT", "Isoblock temperature (R)",			31, addrLongAsLong) );
	AddSpace( New CSpace("RP", "Relay polarity",				39, addrLongAsLong) );
	AddSpace( New CSpace("RR", "Relay response",				40, addrLongAsLong) );
	AddSpace( New CSpace("SP", "PID Set Point",				16, addrLongAsLong) );
	AddSpace( New CSpace("AM", "PID Auto/Manual",				22, addrLongAsLong) );
	AddSpace( New CSpace("CP", "PID Controller Output",			23, addrLongAsLong) );
	AddSpace( New CSpace("KP", "Proportional Band",				17, addrLongAsLong) );
	AddSpace( New CSpace("KI", "Reset Rate (Integral)",			18, addrLongAsLong) );
	AddSpace( New CSpace("KD", "Rate Time (Derivative)",			19, addrLongAsLong) );
	AddSpace( New CSpace("BP", "Bumpless Transfer",				24, addrLongAsLong) );
	AddSpace( New CSpace("OA", "Hi Alarm-On/Off SP1",			25, addrLongAsLong) );
	AddSpace( New CSpace("OB", "Lo Alarm-On/Off SP2",			26, addrLongAsLong) );
	AddSpace( New CSpace("MT", "Match function (W)",			36, addrLongAsLong) );
	AddSpace( New CSpace("CM", "Response to Latest Match Function Write",	54, addrLongAsLong) );
	AddSpace( New CSpace("UN", "Units select",				45, addrLongAsLong) );
	AddSpace( New CSpace("UF", "Unit full scale (R)",			44, addrLongAsLong) );
	AddSpace( New CSpace("UZ", "Unit zero scale (R)",			46, addrLongAsLong) );
	AddSpace( New CSpace("FT", "Features Matrix (R)",			34, addrLongAsLong) );
	AddSpace( New CSpace("VR", "Version Number (R)",			13, addrLongAsLong) );
	AddSpace( New CSpace("MD", "Model - Bytes 1-4 (R)",			14, addrLongAsLong) );
	AddSpace( New CSpace("ME", "Model - Bytes 5-8 (R)",			47, addrLongAsLong) );
	AddSpace( New CSpace("MF", "Model - Bytes 9-12 (R)",			48, addrLongAsLong) );
	AddSpace( New CSpace("MG", "Model - Bytes 13-16 (R)",			49, addrLongAsLong) );
	AddSpace( New CSpace("MH", "Model - Bytes 17-20 (R)",			50, addrLongAsLong) );
	AddSpace( New CSpace("MI", "Model - Bytes 21-23 (R)",			51, addrLongAsLong) );
	AddSpace( New CSpace("SNA","Serial number -  Bytes 1-4 (R)",		42, addrLongAsLong) );
	AddSpace( New CSpace("SNB","Serial number -  Bytes 5-8 (R)",		58, addrLongAsLong) );
	AddSpace( New CSpace("SNC","Serial number -  Bytes 9-12 (R)",		59, addrLongAsLong) );
	AddSpace( New CSpace("SND","Serial number -  Bytes 13-16 (R)",		60, addrLongAsLong) );
	AddSpace( New CSpace("SNE","Serial number -  Bytes 17-20 (R)",		61, addrLongAsLong) );
	AddSpace( New CSpace("SNF","Serial number -  Bytes 21-24 (R)",		62, addrLongAsLong) );
	AddSpace( New CSpace("TP", "Controller Type (R)",			15, addrLongAsLong) );
	AddSpace( New CSpace("CL", "1 or 2 color mode (R-series only)",		32, addrLongAsLong) );
	AddSpace( New CSpace("DT", "Dirty window threshold (DWD only)",		33, addrLongAsLong) );
	AddSpace( New CSpace("LS", "Laser control",				35, addrLongAsLong) );
	AddSpace( New CSpace("AT", "Auto Tune",					21, addrLongAsLong) );
	AddSpace( New CSpace("AC", "Auto cal time period (in hours)",		27, addrLongAsLong) );
	AddSpace( New CSpace("KL", "(5)-Keyboard Lock, (3)-Load Demand",	20, addrLongAsLong) );
	AddSpace( New CSpace("TS3","Modline 3 - Track and Hold Mode",		57, addrLongAsLong) );
	AddSpace( New CSpace("TSS","TS (5)-Temp+Status - Status (R)",		11, addrLongAsLong) );
	AddSpace( New CSpace("TST","TS (5)-Temp+Status - Temperature (R)",	56, addrLongAsLong) );
	AddSpace( New CSpace("TIM","Timeout for this Device",			99, addrLongAsLong) );
	}

// End of File
