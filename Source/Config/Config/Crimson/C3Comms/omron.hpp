
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_OMRON_HPP
	
#define	INCLUDE_OMRON_HPP

//////////////////////////////////////////////////////////////////////////
//
// Omron C Series PLC
//

class COmronPLCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		COmronPLCDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL CheckAlignment(CSpace *pSpace);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
