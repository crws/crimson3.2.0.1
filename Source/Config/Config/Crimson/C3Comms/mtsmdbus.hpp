
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MTSMODBUS_HPP
	
#define	INCLUDE_MTSMODBUS_HPP

//////////////////////////////////////////////////////////////////////////
//
// MTS MODBUS Driver Options
//

class CMTSModbusDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMTSModbusDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Protocol;
		UINT m_MaxWords;
		UINT m_MaxBits;
		UINT m_Mode;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MTS MODBUS Device Options
//

class CMTSModbusDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMTSModbusDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		BOOL m_fRLCAuto;
		BOOL m_fDisable16;
		BOOL m_fDisable15;
		UINT m_Max01;
		UINT m_Max02;
		UINT m_Max03;
		UINT m_Max04;
		UINT m_Max15;
		UINT m_Max16;
		UINT m_Ping;
		BOOL m_fDisable5;
		BOOL m_fDisable6;
		BOOL m_fDisableCheck;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MTS MODBUS Driver
//

class CMTSModbusDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMTSModbusDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL CheckAlignment(CSpace *pSpace);

		
	protected:
		// Implementation
		void AddSpaces(void);

		// File Location
		UINT GetFile(CString Text, CSpace *pSpace, UINT *uOffsetEnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Address Selection
//

class CMTSModbusAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CMTSModbusAddrDialog(CMTSModbusDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);
		                
	protected:
		UINT	m_uDropNumber;

		// Overridables
		BOOL	AllowSpace(CSpace *pSpace);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

// End of File

#endif
