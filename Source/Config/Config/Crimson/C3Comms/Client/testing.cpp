
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Test Application
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Runtime Class

AfxImplementRuntimeClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestWnd;

	CPoint Pos = CPoint(280, 180);

	CSize Size = CSize (586, 400);

	CRect Rect = CRect (Pos, Size);

	pWnd->Create( L"Test Application",
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      Rect, AfxNull(CWnd), CMenu(L"Test"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_EXIT ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestWnd, CMenuWnd);

// Constructor

CTestWnd::CTestWnd(void)
{
	m_Accel.Create(L"Test");

	m_pEdit = New CEditCtrl;
	}

// Destructor

CTestWnd::~CTestWnd(void)
{
	}

// Message Map

AfxMessageMap(CTestWnd, CMenuWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestWnd)
	};

// Message Handlers

BOOL CTestWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

void CTestWnd::OnPostCreate(void)
{
	CRect Rect = GetClientRect() - 4;

	m_pEdit->Create( WS_CHILD | WS_CLIPCHILDREN | ES_MULTILINE,
			 Rect, ThisObject, IDVIEW
			 );

	m_pEdit->SetFont(afxFont(Fixed));

	m_pEdit->ShowWindow(SW_NORMAL);
	}

void CTestWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);
	}

BOOL CTestWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = CWnd::GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	DC.FrameRect(Rect, afxBrush(WHITE));

	Rect -= 1;

	DC.FrameRect(Rect, afxBrush(WHITE));

	return TRUE;
	}

void CTestWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		CRect Rect = GetClientRect() - 4;

		m_pEdit->SetWindowPos(Rect, TRUE);
		}
	}

void CTestWnd::OnSetFocus(CWnd &Wnd)
{
	m_pEdit->SetFocus();
	}

// Command Handlers

BOOL CTestWnd::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_NEW ) {

		ICommsDriver *pDriver = C3EnumDrivers(0);

		CLASS Class  = pDriver->GetDeviceConfig();

		CItem *pItem = AfxNewObject(CItem, Class);

		pItem->Init();

		CItemDialog Dlg(pItem, L"Properties");

		Dlg.Execute(ThisObject);

		pItem->Kill();

		delete pItem;

		pDriver->Release();

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_NEW ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

// End of File
