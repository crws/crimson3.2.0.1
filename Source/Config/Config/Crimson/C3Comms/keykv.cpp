
#include "intern.hpp"

#include "keykv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Series PLC Master Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKeyKVMasterSerialDeviceOptions, CUIItem);

// Constructor

CKeyKVMasterSerialDeviceOptions::CKeyKVMasterSerialDeviceOptions(void)
{
	m_Conn = 0;

	m_Drop = 1;
	}

// UI Managament

void CKeyKVMasterSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Conn" ) {

			pWnd->EnableUI("Drop", m_Conn);
			}
		}
	}

// Download Support	     

BOOL CKeyKVMasterSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Conn));
	Init.AddByte(BYTE(m_Drop));
			
	return TRUE;
	}

// Meta Data Creation

void CKeyKVMasterSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Conn);
	Meta_AddInteger(Drop);
	}



//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Series PLC Master Serial Driver
//
//

// Instantiator

ICommsDriver *Create_CKeyKVMasterSerialDriver(void)
{
	return New CKeyKVMasterSerialDriver;
	}

// Constructor

CKeyKVMasterSerialDriver::CKeyKVMasterSerialDriver(void)
{
	m_wID		= 0x3308;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Keyence";
	
 	m_DriverName	= "KV Series";
	
	m_Version	= "1.00";
	
	m_ShortName	= "KV";

	AddSpaces();
	}

// Configuration

CLASS CKeyKVMasterSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKeyKVMasterSerialDeviceOptions);
	}

// Binding Control

UINT CKeyKVMasterSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CKeyKVMasterSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}


// Implementation

void CKeyKVMasterSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpaceKeyKV(1, "DM", "Data Memory",		10, 0, 9999,	addrWordAsWord));
	AddSpace(New CSpaceKeyKV(2, "TM", "Temporary Memory",		10, 0,   99,	addrWordAsWord));
	AddSpace(New CSpaceKeyKV(3, "CC", "Counter Current Values",	10, 0,  999,	addrWordAsWord));
	AddSpace(New CSpaceKeyKV(4, "CP", "Counter Preset Values",	10, 0,  999,	addrWordAsWord));
	AddSpace(New CSpaceKeyKV(5, "TC", "Timer Current Values",	10, 0,  999,	addrWordAsWord));
	AddSpace(New CSpaceKeyKV(6, "TP", "Timer Preset Values",	10, 0,  999,	addrWordAsWord));


	}

// Address Helpers

BOOL CKeyKVMasterSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		UINT uExtra = pSpace->m_uTable;
	
		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			Addr.a.m_Table = addrNamed + uExtra;

			return TRUE;
			}
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Space Wrapper
//

// Constructor

CSpaceKeyKV::CSpaceKeyKV(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t)
{
	m_uTable	= uTable;
	
	m_Prefix	= p;

	m_Caption	= c;

	m_uRadix	= r;

	m_uMinimum	= n;
	
	m_uMaximum	= x;
	
	m_uType		= t;

	m_uSpan		= t;

	FindWidth();

	FindAlignment();
	}

// Matching

BOOL CSpaceKeyKV::MatchSpace(CAddress const &Addr)
{
	return m_uTable == (Addr.a.m_Table & 0x0F);
	}

// End of File
