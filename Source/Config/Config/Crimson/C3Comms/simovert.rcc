
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "simovert.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "simovert.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "simovert.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "simovert.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

///////////////////////////////////////////////////////////////////////////
//
// String Table
//
//

STRINGTABLE
BEGIN
	IDS_SIMO_PARAM		"Parameters"
	IDS_SIMO_CTRL		"Control Word"
	IDS_SIMO_FREQ		"Frequency Word"
	IDS_SIMO_SEND		"Send Command"
	IDS_SIMO_STAT		"Status Word"
	IDS_SIMO_REAL		"Actual Value"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CSimovertSerialDeviceOptions
//

CSimovertSerialDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|31,"
	"Select the drop number of the device to be addressed."
	"\0"

	"PZD,USS PZD Length,,CUIEditInteger,|0||1|13,"
	"Select the number of words of process data to be transferred.  "
	"This setting should match the USS PZD Length parameter in the "
	"Siemens drive."
	"\0"

	"PKW,USS PKW Length,,CUIDropDown,3|4|127 (variable)|127 (variable) - 0 PZD exchange,"
	"Select the number of words of data to be transferred.  A setting "
	"of 3 corresponds with only 16-bit data transfer, while a setting "
	"of 4 corresponds with only 32-bit data transfer.  In most cases, the "
	"variable setting is recommended, allowing access to 16-bit and 32-bit "
	"data.  This setting should match the USS PKW Length parameter in the "
	"Siemens drive.  "
	"Some drives only include the PKW area during "
	"parameterization and not during process data exchange.  In this case "
	"select the 127 (variable) - 0 PZD exchange setting."
	"\0"

	"ModelUSS,Model,,CUIDropDown,|Simovert||Simoreg|,"
	"Select the appropriate model."
	"\0"

	"\0"
END

CSimovertSerialDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device,Drop,PZD,PKW,ModelUSS\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Simovert Element Dialog
//
//

SimovertElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT 2002, 0, 0, 36, 12
	CONTROL	 "&Save to EEPROM", 2004, "button", BS_AUTOCHECKBOX, 0, 16, 60, 14
END	

// End of File
