//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_R29ID_HPP
	
#define	INCLUDE_R29ID_HPP

#include "j1939.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedItem : public CItem { };

class CCAN29bitIdEntryRawDriverOptions;
class CCAN29bitIdEntryRawDeviceOptions;
class CCANRawDriver;

//////////////////////////////////////////////////////////////////////////
//
// Raw/Generic CAN Sub Element
//

class CSUB : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CSUB(void);
		CSUB(UINT uSize);

		// Destructor
		~CSUB(void);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	  m_Size;
					
	protected:
		
		// Meta Data Creation
		void AddMetaData(void);
	};


/////////////////////////////////////////////////////////////////////////
//
// Raw/Generic SUB Storage Class 
//

class CSUBList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSUBList(void);

		// Destructor
		~CSUBList(void);

		// Item Access
		CSUB * GetItem(INDEX Index) const;
		CSUB * GetItem(UINT  uPos ) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw/Generic 29-bit Identifier
//

class CID29 : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CID29(void);
		CID29(CID29 * pID);
		
		// Destructor
		~CID29(void);
	       
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		void SetAddrRef(DWORD AddrRef);
		BOOL IsDirty(BOOL fReq, UINT uRepRate);
				
		// SUB List Management
		BOOL	   GrowSUBList(UINT uCount);
		CSUBList * GetSUBs(void);

		// Data Members
		UINT	  m_Number;
		CString	  m_Title;
		CSUBList* m_pSUBs;
		BOOL	  m_SendReq;
		UINT	  m_RepRate;
		CAddress  m_Addr;
		UINT	  m_Active;
		UINT	  m_Enhanced;
		       	
	protected:
		// Meta Data Creation
		void AddMetaData(void);

		
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw/Generic Parameter Group List Class
//

class CID29List : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CID29List(void);

		// Destructor
		~CID29List(void);

		// Item Access
		CID29 * GetItem(INDEX Index) const;
		CID29 * GetItem(UINT  uPos ) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Generic Driver Options
//

class CCAN29bitIdEntryRawDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCAN29bitIdEntryRawDriverOptions(void);

	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Generic Device Options Page
//

class CCAN29bitIdEntryRawDeviceOptionsUIPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCAN29bitIdEntryRawDeviceOptionsUIPage(CCAN29bitIdEntryRawDeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CCAN29bitIdEntryRawDeviceOptions * m_pOption;
	}; 

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Generic Device Options
//

class CCAN29bitIdEntryRawDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCAN29bitIdEntryRawDeviceOptions(void);

		// Destructor
		~CCAN29bitIdEntryRawDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);
		
		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		CID29	 *  GetID(UINT uNumber, UINT uMatch = 0);
		CID29	 *  FindID(CString Text);
		UINT	    GetNext(void);
		void	    InitIDs(void);

		// Overidables
		virtual CID29 * FindListID(CString Text);
		virtual CID29 * GetID(CString Text);

		// Config Data
		UINT	     m_BackOff;
		CID29List *  m_pIDs;
		UINT	     m_Last;
			
	protected:
				
		// Meta Data Creation
		void AddMetaData(void);

		// Debugging
		void ShowIDs(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Generic Driver
//

class CCAN29bitIdEntryRawDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CCAN29bitIdEntryRawDriver(void);

		// Destructor
		~CCAN29bitIdEntryRawDriver(void);

		// Driver Data
		UINT GetFlags(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Notifications
		void NotifyInit(CItem * pConfig);
		
		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig,CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		       		
	protected:
		// Data Members									
		BOOL m_fExpand;
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw Serial Driver Options
//

class CRawCANDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRawCANDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);


		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		CCodedItem *m_pService;
		UINT	    m_RxPDU;
		UINT	    m_TxPDU;
		UINT	    m_RxMail;
		UINT	    m_TxMail;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry True Raw Driver
//

class CCANRawDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CCANRawDriver(void);

		// Destructor
		~CCANRawDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
							
       	};

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Generic Message Selection Dialog
//

class CId29MessSelDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CId29MessSelDialog(CCAN29bitIdEntryRawDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:

		// Data Members
		CCAN29bitIdEntryRawDriver *	m_pDriver;
		CAddress	*		m_pAddr;
		CItem		*		m_pConfig;
		BOOL				m_fPart;

	
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
	       
		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadIDs(void);
		void LoadOptions(void);
		void LoadRepRate(void);
		void SetOptions(void);
		void SetRepRate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Generic Message Configuration Dialog
//

class CId29MessDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CId29MessDialog(CCAN29bitIdEntryRawDeviceOptions * pOptions, CSystemItem *pSystem);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnCreateEdit(UINT uID);
		BOOL OnRemove(UINT uID);
		BOOL OnOkay(UINT uID);
		BOOL OnCancelButton(UINT uID);

		// Notification Handlers
		void OnIDChange(UINT uID, CWnd &Wnd);
		void OnDiagChange(UINT uID, CWnd &Wnd);
		void OnEditChange(UINT uID, CWnd &Ctrl);
			
	protected:
		// Data Members
		CCAN29bitIdEntryRawDeviceOptions * m_pOptions;
		BYTE				   m_pSUBs[64];
		CID29List			 * m_pIDs;
		CSystemItem			 * m_pSystem;
		BOOL				   m_fRebuild;
		BOOL				   m_fChange;

		// Implementation
		void	LoadIDList(void);
		void	InitAll(void);
		void	SetSUBs(BOOL fInit);
		void    SetEnhanced(UINT uSel);
		CID29 * CreateID(void);
		BOOL	MakeID(CID29 * pIdent);
		BOOL	RemoveID(void);

		// Message Handlers
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);
		
		// Overridables
		virtual CID29 * NewID(void);
		virtual BOOL    SetID(CError &Error, CID29 * pID, UINT uID);
		virtual void    SetID(CID29 * pID);
		virtual void	DoEnables(void);
						
		// Helpers
		BOOL	IsInteger(UINT uID);
		BOOL	IsHex(UINT uID);
		BOOL	IsEdit(void);
		CString GetTitle(CID29 * pId);
		CID29 * FindID(CString Text);
		BOOL	IsIdValid(CError &Error, UINT uID);
		BOOL    IsIdValid(CError &Error, CString ID);
		BOOL	IsSubElementValid(CError &Error, UINT uSize);
		BOOL    IsTitleValid(CError &Error, CString Title);
	};


// End of File

#endif
