#include "intern.hpp"

#include "UnidriveM.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Menu
//

// Constructors

CUnidriveMMenu::CUnidriveMMenu(void)
{
	}

CUnidriveMMenu::CUnidriveMMenu(CString Name, CString Desc, UINT uMenu)
	: m_Name(Name), m_Desc(Desc)
{
	m_uMenu = uMenu;
	}

// Destructor

CUnidriveMMenu::~CUnidriveMMenu(void)
{
	}

// Operations

CString CUnidriveMMenu::GetName(void) const
{
	return m_Name;
	}

void CUnidriveMMenu::AddItem(CUnidriveMItem Item)
{
	// Check for duplicates before adding

	if( m_Items.Find(Item) == NOTHING ) {

		m_Items.Append(Item);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Item Comparison
//

bool CUnidriveMItem::operator == (CUnidriveMItem const &That) const
{
	return m_uItem == That.m_uItem && m_uMenu == That.m_uMenu;
	}

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Serial Driver
//

// Instatiator

ICommsDriver * Create_UnidriveMDriver(void)
{
	return New CUnidriveMDriver;
	}

// Constructor

CUnidriveMDriver::CUnidriveMDriver(void)
{
	m_wID		= 0x40B3;

	m_uType		= driverMaster;

	m_Manufacturer	= "Nidec - Control Techniques";

	m_DriverName	= "Unidrive M Pre-configured Modbus";

	m_Version	= "1.00";

	m_ShortName	= "Unidrive M";

	m_DevRoot	= "DRV";

	C3_PASSED();
	}

// Destructor

CUnidriveMDriver::~CUnidriveMDriver(void)
{
	}

// Binding

UINT CUnidriveMDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CUnidriveMDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Driver Configuration

CLASS CUnidriveMDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Device Configuration

CLASS CUnidriveMDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUnidriveMSerialDeviceOptions);
	}

// Address Management

BOOL CUnidriveMDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CUnidriveMDialog Dlg(ThisObject, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

CString CUnidriveMDriver::GetPrefix(UINT uTable)
{
	CString Prefix;

	switch( uTable ) {

		case SPACE_HOLD:
			Prefix = "D4";
			break;

		case SPACE_ANALOG:
			Prefix = "A3";
			break;

		case SPACE_OUTPUT:
			Prefix = "O1";
			break;

		case SPACE_INPUT:
			Prefix = "I1";
			break;

		case SPACE_HOLD32:
			Prefix = "L4";
			break;

		case SPACE_ANALOG32:
			Prefix = "L3";
			break;
		}

	return Prefix;
	}

UINT CUnidriveMDriver::GetMax(UINT uTable)
{
	return 65535;
	}

UINT CUnidriveMDriver::GetMin(UINT uTable)
{
	return 1;
	}

UINT CUnidriveMDriver::GetTable(CString const &Text)
{
	if( Text.Find(L"D4") < NOTHING ) return SPACE_HOLD;

	else if( Text.Find(L"A3") < NOTHING ) return SPACE_ANALOG;

	else if( Text.Find(L"O1") < NOTHING ) return SPACE_OUTPUT;

	else if( Text.Find(L"I1") < NOTHING ) return SPACE_INPUT;

	else if( Text.Find(L"L4") < NOTHING ) return SPACE_HOLD32;

	else if( Text.Find(L"L3") < NOTHING ) return SPACE_ANALOG32;

	return addrNamed;
	}

CString CUnidriveMDriver::GetRadixText(UINT uTable)
{
	return L"Decimal";
	}

BOOL CUnidriveMDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return DoParseAddress(Error, Addr, pConfig, Text);
	}

BOOL CUnidriveMDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return DoExpandAddress(Text, pConfig, Addr);
	}

BOOL CUnidriveMDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{	
	return ((CUnidriveMDeviceOptions *) pConfig)->DoListAddress(pRoot, uItem, Data);
	}

BOOL CUnidriveMDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CUnidriveMDeviceOptions *pDevice = (CUnidriveMDeviceOptions *) pConfig;

	UINT uTab = GetTable(Text);

	Addr.m_Ref = 0;

	if( !Text.IsEmpty() ) {

		UINT uFind = Text.Find(L'_');

		if( uFind < NOTHING ) {			

			UINT uType = addrWordAsWord;

			Addr.a.m_Table = addrNamed;

			UINT uMenu = wcstoul(Text, NULL, 10);

			UINT uItem = wcstoul(Text.Mid(uFind + 1), NULL, 10);

			UINT uRef = uMenu * 1000 + uItem;

			CUnidriveMItem Item;

			if( pDevice->FindItem(uRef, Item) ) {

				uType = Item.m_uType;
				}

			UINT uRegMode = pConfig->GetDataAccess("RegisterMode")->ReadInteger(pConfig);

			if( uRegMode == regModeStd ) {

				if( uItem > 99 ) {

					Error.Set(CString(IDS_CT_UDRV_ITEMRANGE));

					return FALSE;
					}
				else {
					Addr.a.m_Offset = uMenu * 100 + uItem - 1;
					}
				}
			else {
				Addr.a.m_Offset = uMenu * 256 + uItem - 1;
				}

			Addr.a.m_Type = uType;

			return TRUE;
			}
		else {
			uFind = Text.Find(GetPrefix(uTab));

			if( uFind < NOTHING ) {

				Addr.a.m_Offset = wcstol(Text.Mid(uFind + 2), NULL, 10);

				Addr.a.m_Type = (uTab >= SPACE_HOLD32) ? addrLongAsLong : addrWordAsWord;

				Addr.a.m_Table = GetTable(Text);

				if( Addr.a.m_Offset < GetMin(uTab) || Addr.a.m_Offset > GetMax(uTab) ) {

					CString Min = CPrintf("%d", GetMin(uTab));

					CString Max = CPrintf("%d", GetMax(uTab));

					Error.Set(CPrintf(IDS_DRIVER_ADDRRANGE, Min, Max));

					return FALSE;
					}

				return TRUE;
				}

			Error.Set(CString(IDS_DRIVER_ADDR_INVALID));

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CUnidriveMDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		UINT uMenu = 0;

		UINT uItem = 0;

		UINT uRegMode = pConfig->GetDataAccess("RegisterMode")->ReadInteger(pConfig);

		if( uRegMode == regModeStd ) {
	
			uMenu = Addr.a.m_Offset / 100;

			uItem = Addr.a.m_Offset - uMenu * 100 + 1;
			}
		else {		
			uMenu = Addr.a.m_Offset / 256;

			uItem = Addr.a.m_Offset - uMenu * 256 + 1;
			}

		Text = CPrintf("%2.2d_%3.3d_%6.6d", uMenu, uItem, Addr.a.m_Offset + 400000);

		return TRUE;
		}
	else {
		Text = CPrintf("%s%5.5d", GetPrefix(Addr.a.m_Table), Addr.a.m_Offset);

		return TRUE;
		}

	return FALSE;
	}

// Type Helpers

UINT CUnidriveMDriver::GetType(CString const &Text)
{
	if( Text.Find(L"Bit") < NOTHING ) return addrBitAsBit;
	
	else if( Text.Find(L"Word") < NOTHING ) return addrWordAsWord;
	
	else if( Text.Find(L"Long") < NOTHING ) return addrLongAsLong;

	return addrWordAsWord;
	}

CString	CUnidriveMDriver::GetTypeText(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
			return "Bit";

		case addrWordAsWord:
			return "Word";

		case addrLongAsLong:
			return "Long";
		}

	return "";
	}

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M TCP/IP Driver
//

// Instantiator

ICommsDriver * Create_UnidriveMTCPDriver(void)
{
	return New CUnidriveMTCPDriver();
	}

// Constructor

CUnidriveMTCPDriver::CUnidriveMTCPDriver(void)
{
	m_wID		= 0x40B4;

	m_uType		= driverMaster;

	m_Manufacturer	= "Nidec - Control Techniques";

	m_DriverName	= "Unidrive M Pre-configured Modbus TCP";

	m_Version	= "1.00";

	m_ShortName	= "Unidrive M";

	m_DevRoot	= "DRV";
	}

// Destructor

CUnidriveMTCPDriver::~CUnidriveMTCPDriver(void)
{
	}

// Binding

UINT CUnidriveMTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CUnidriveMTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CUnidriveMTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CUnidriveMTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUnidriveMTCPDeviceOptions);
	}

// End of File
