
#include "intern.hpp"

#include "lfd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LFD Driver
//

// Instantiator

ICommsDriver *	Create_LFDDriver(void)
{
	return New CLFDDriver;
	}

// Constructor

CLFDDriver::CLFDDriver(void)
{
	m_wID		= 0x3708;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "Red Lion";
	
	m_DriverName	= "Little Flexible Display";
	
	m_Version	= "1.00";
	
	m_ShortName	= "LFD";

	m_fSingle	= TRUE;
	}

// Binding Control

UINT CLFDDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CLFDDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// End of File
