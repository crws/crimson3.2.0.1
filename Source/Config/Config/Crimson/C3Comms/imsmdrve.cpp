
#include "intern.hpp"

#include "imsmdrve.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IMSMDrive Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIMSMDriveDeviceOptions, CUIItem);

// Constructor

CIMSMDriveDeviceOptions::CIMSMDriveDeviceOptions(void)
{
	m_DName		= "A";
	}

// UI Management

void CIMSMDriveDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "DName" ) {

		if( m_DName.GetLength() != 1 || m_DName[0] <= ' ' || m_DName[0] > 'z' ) m_DName = "A";

		pWnd->UpdateUI("DName");
	      	}
	}

// Download Support

BOOL CIMSMDriveDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddText(m_DName);

	return TRUE;
	}

// Meta Data Creation

void CIMSMDriveDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(DName);
	}

//////////////////////////////////////////////////////////////////////////
//
// Intelligent Motion Systems Driver
//

// Instantiator

ICommsDriver *	Create_IMSMDriveDriver(void)
{
	return New CIMSMDriveDriver;
	}

// Constructor

CIMSMDriveDriver::CIMSMDriveDriver(void)
{
	m_wID		= 0x4022;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Intelligent Motion Systems";
	
	m_DriverName	= "M-Drive";
	
	m_Version	= "1.00";
	
	m_ShortName	= "IMS M-Drive";

	AddSpaces();
	}

// Binding Control

UINT	CIMSMDriveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CIMSMDriveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS	CIMSMDriveDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIMSMDriveDeviceOptions);
	}

// Address Management

BOOL CIMSMDriveDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CIMSMDriveAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CIMSMDriveDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	CString sErr;

	UINT uPar1 = tatoi(Text);

	Addr.a.m_Offset = uPar1;
	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrLongAsLong;

	switch( pSpace->m_uTable ) {

		case 0x20:
		case 0x21:
		case 0x22:
		case 0x23:
		case 0x56:

			if( uPar1 <= pSpace->m_uMaximum && uPar1 >= pSpace->m_uMinimum ) return TRUE;

			sErr.Printf( "%s%d - %s%d",
				pSpace->m_Prefix,
				pSpace->m_uMinimum,
				pSpace->m_Prefix,
				pSpace->m_uMaximum
				);

			break;

		case 0x57:
			UINT uH;
			UINT uL;

			uH    = Text[0];
			uL    = Text[1];

			if( isalpha(uH) && (isalpha(uL) || isdigit(uL)) ) {

				Addr.a.m_Offset = (uH << 8) + uL;

				return TRUE;
				}

			sErr.Printf("[A-Z,a-z] [A-Z,a-z,0-9]");

			break;

		default: return TRUE;
		}

	Error.Set( sErr,
		0
		);

	return FALSE;
	}

BOOL CIMSMDriveDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	CString s = "";

	switch( Addr.a.m_Table ) {

		case 0x20:
		case 0x21:
		case 0x22:
		case 0x23:
		case 0x56:
			Text.Printf( "%s%d",
			pSpace->m_Prefix,
			Addr.a.m_Offset
			);

			break;

		case 0x57:
			s += HIBYTE(LOWORD(Addr.a.m_Offset));
			s += LOBYTE(LOWORD(Addr.a.m_Offset));
			Text.Printf( "%s%s",
			pSpace->m_Prefix,
			s
			);

			break;

		default:
			Text = pSpace->m_Prefix;
			break;
		}

	return TRUE;			
	}

// Implementation

void CIMSMDriveDriver::AddSpaces(void)
{
	AddSpace(New CSpace(AN,   " ",	"IO PARAMETERS...",			10, SPIO, FZ, LL));
	AddSpace(New CSpace(0x20, "DF",	"   Input Digital Filtering",		10,  1,  5, LL));
	AddSpace(New CSpace(0x21, "I",	"   Read Input Value",			10,  0,  6, LL));
	AddSpace(New CSpace(0x22, "O",	"   Set Output",			10,  0,  4, LL));
	AddSpace(New CSpace(0x23, "SI",	"   Setup IO Point",			10,  1,  5, LL));
	AddSpace(New CSpace(0x24, "TE",	"   Trip Enable",			10,  0,  0, LL));

	AddSpace(New CSpace(AN,   " ",	"POSITION PARAMETERS...",		10, SPPOSITION,  FZ, LL));
	AddSpace(New CSpace(0x30, "C1",	"   Counter 1 Motor Counts",		10,  0,  0, LL));
	AddSpace(New CSpace(0x31, "P",	"   Position Motor/Encoder Counts",	10,  0,  0, LL));
	AddSpace(New CSpace(0x32, "PC",	"   Capture Position at Trip",		10,  0,  0, LL));

	AddSpace(New CSpace(AN,   " ",	"ENCODER PARAMETERS...",		10, SPENCODER,  FZ, LL));
	AddSpace(New CSpace(0x40, "C2",	"   Counter 2",				10,  0,  0, LL));
	AddSpace(New CSpace(0x41, "DB",	"   Encoder Deadband",			10,  0,  0, LL));
	AddSpace(New CSpace(0x42, "EE",	"   Enable/Disable Encoder Functions",	10,  0,  0, LL));
	AddSpace(New CSpace(0x43, "PM",	"   Position Maintenance Enable",	10,  0,  0, LL));
	AddSpace(New CSpace(0x44, "SF",	"   Stall Factor",			10,  0,  0, LL));
	AddSpace(New CSpace(0x45, "SM",	"   Stall Mode",			10,  0,  0, LL));
	AddSpace(New CSpace(0x46, "ST",	"   Stall Flag",			10,  0,  0, LL));

	AddSpace(New CSpace(AN,   " ",	"MISCELLANEOUS...",			10, SPMISC,  FZ, LL));
	AddSpace(New CSpace(0x50, "BY",	"   BSY Flag",				10,  0,  0, LL));
	AddSpace(New CSpace(0x51, "EF",	"   Error Flag",			10,  0,  0, LL));
	AddSpace(New CSpace(0x52, "ER",	"   Error Number",			10,  0,  0, LL));
	AddSpace(New CSpace(0x53, "FD",	"   Factory Defaults",			10,  0,  0, LL));
	AddSpace(New CSpace(0x54, "IP",	"   Initialize Parameters",		10,  0,  0, LL));
	AddSpace(New CSpace(0x55, "S",	"   Save to EEProm",			10,  0,  0, LL));
	AddSpace(New CSpace(0x56, "UR",	"   User Register",			10,  0, 16, LL));
	AddSpace(New CSpace(0x57, "UV",	"   Read/Write One User Variable",	16, A0, zz, LL));
	AddSpace(New CSpace(0x58, "VR",	"   Firmware Version Number",		10,  0,  0, LL));

	AddSpace(New CSpace(AN,   " ",	"MOTION PARAMETERS...",			10, SPMOTION,  FZ, LL));
	AddSpace(New CSpace(0x01, "A",	"   Acceleration",			10,  0,  0, LL));
	AddSpace(New CSpace(0x02, "D",	"   Deceleration",			10,  0,  0, LL));
	AddSpace(New CSpace(0x03, "DE",	"   Drive Enable",			10,  0,  0, LL));
	AddSpace(New CSpace(0x04, "EX", "   Execute Program",			10,  0,  0, LL));
	AddSpace(New CSpace(0x05, "EXL","      Execute Program, Label",		10,  0,  4, LL));
	AddSpace(New CSpace(0x06, "HC",	"   Hold Current %",			10,  0,  0, LL));
	AddSpace(New CSpace(0x07, "HI",	"   Home to Index Mark",		10,  0,  0, LL));
	AddSpace(New CSpace(0x08, "HM",	"   Home to Home Switch",		10,  0,  0, LL));
	AddSpace(New CSpace(0x09, "HT",	"   Hold Current Delay Time",		10,  0,  0, LL));
	AddSpace(New CSpace(0x0A, "JE",	"   Jog Enable Flag",			10,  0,  0, LL));
	AddSpace(New CSpace(0x0B, "LM",	"   Limit Stop Mode",			10,  0,  0, LL));
	AddSpace(New CSpace(0x0C, "MA",	"   Move to Absolute Position",		10,  0,  0, LL));
	AddSpace(New CSpace(0x0D, "MD",	"   Motion Mode Setting",		10,  0,  0, LL));
	AddSpace(New CSpace(0x0E, "MR",	"   Move to Relative Position",		10,  0,  0, LL));
	AddSpace(New CSpace(0x0F, "MS",	"   Microstep Resolution",		10,  0,  0, LL));
	AddSpace(New CSpace(0x10, "MT",	"   Motor Settling Delay Time",		10,  0,  0, LL));
	AddSpace(New CSpace(0x11, "MV",	"   Moving Flag",			10,  0,  0, LL));
	AddSpace(New CSpace(0x12, "PS",	"   Pause Program",			10,  0,  0, LL));
	AddSpace(New CSpace(0x13, "RC",	"   Run Current %",			10,  0,  0, LL));
	AddSpace(New CSpace(0x14, "RS",	"   Resume Paused Program",		10,  0,  0, LL));
	AddSpace(New CSpace(0x15, "SL",	"   Slew Axis",				10,  0,  0, LL));
	AddSpace(New CSpace(0x16, "TIE","   Trip On Input, Execute",		10,  0,  0, LL));
	AddSpace(New CSpace(0x17, "TIL","      Trip On Input, Label",		10,  0,  4, LL));
	AddSpace(New CSpace(0x18, "TPE","   Trip On Position, Execute",		10,  0,  0, LL));
	AddSpace(New CSpace(0x19, "TPL","      Trip On Position, Label",	10,  0,  4, LL));
	AddSpace(New CSpace(0x1A, "V",	"   Current Velocity",			10,  0,  0, LL));
	AddSpace(New CSpace(0x1B, "VC",	"   Velocity Changing Flag",		10,  0,  0, LL));
	AddSpace(New CSpace(0x1C, "VI",	"   Initial Velocity",			10,  0,  0, LL));
	AddSpace(New CSpace(0x1D, "VM",	"   Maximum Velocity",			10,  0,  0, LL));
	}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection for Intelligent Motion Systems M-Drive
//

// Runtime Class

AfxImplementRuntimeClass(CIMSMDriveAddrDialog, CStdAddrDialog);
		
// Constructor

CIMSMDriveAddrDialog::CIMSMDriveAddrDialog(CIMSMDriveDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	if( m_uAllowSpace < SPMOTION || m_uAllowSpace > SPMISC ) {
		
		m_uAllowSpace = SPMOTION;
		}

	SetName(TEXT("IMSMDriveAddressDlg"));
	}

// Message Map

AfxMessageMap(CIMSMDriveAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CIMSMDriveAddrDialog)
	};

// Message Handlers

BOOL CIMSMDriveAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();
	
	if( !m_fPart ) {

		SetCaption();

		if( m_pSpace ) SetAllow();

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			return FALSE;
			}

		return TRUE;
		}

	else {
		SetAllow();

		ShowAddress(Addr);

		return FALSE;
		}
	}

// Notification Handlers

void CIMSMDriveAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CIMSMDriveAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			SetAllow();

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = m_pSpace->m_uMinimum;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			m_pSpace->m_uMinimum < SPMOTION ? ShowDetails() : ClearDetails();

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(2001).SetWindowText("None");
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2003).SetWindowText("");
		GetDlgItem(2010).SetWindowText("");

		ClearDetails();

		GetDlgItem(2001).EnableWindow(TRUE);
		GetDlgItem(2002).EnableWindow(FALSE);
		}
	}

BOOL CIMSMDriveAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		switch( m_pSpace->m_uTable ) {

			case addrNamed:

				if( IsHeader(m_pSpace->m_uMinimum) ) {

					m_uAllowSpace = m_pSpace->m_uMinimum;

					LoadList();

					GetDlgItem(3002).SetWindowText("");

					return TRUE;
					}

				break;

			default:
				Text += GetDlgItem(2002).GetWindowText();

				break;
			}		

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables
void CIMSMDriveAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	GetDlgItem(2001).EnableWindow(TRUE);

	CString s = m_pSpace->GetValueAsText(Addr.a.m_Offset);

	switch( Addr.a.m_Table ) {

		// Headers and String Entries
		case 0x05:
		case 0x17:
		case 0x19:
		case 0x58:
		case addrNamed: // Header

			GetDlgItem(2002).SetWindowText("");

			GetDlgItem(2002).EnableWindow(FALSE);

			if( Addr.a.m_Offset >= SPMOTION ) GetDlgItem(3002).SetWindowText("");

			GetDlgItem(3004).SetWindowText("");
			GetDlgItem(3006).SetWindowText("");
			GetDlgItem(3008).SetWindowText("");

			ShowDetails();

			return;

		case 0x57:
			s.Printf( "%c%c", HIBYTE(LOWORD(Addr.a.m_Offset)), LOBYTE(LOWORD(Addr.a.m_Offset)) );
			break;
		}

	if( m_pSpace->m_uMaximum ) {

		GetDlgItem(2002).EnableWindow(TRUE);

		GetDlgItem(2002).SetWindowText(s);

		SetDlgFocus(2002);

		ShowDetails();

		return;
		}

	GetDlgItem(2002).EnableWindow(FALSE);

	GetDlgItem(2002).SetWindowText("");

	SetDlgFocus(1001);
	}

void CIMSMDriveAddrDialog::ShowDetails(void)
{
	CStdAddrDialog::ShowDetails();

	BOOL fClear = FALSE;

	if( !m_pSpace->m_uMaximum ) fClear = TRUE;

	else {
		switch( m_pSpace->m_uTable ) {

			case 0x05:
			case 0x17:
			case 0x19:
			case 0x58:
				GetDlgItem(3002).SetWindowText("String");
				fClear = TRUE;
				break;

			case 0x57:
				GetDlgItem(3002).SetWindowText("ASCII");
				GetDlgItem(3008).SetWindowText("");
				break;
			}
		}

	if( fClear ) {

		GetDlgItem(3004).SetWindowText("");
		GetDlgItem(3006).SetWindowText("");
		GetDlgItem(3008).SetWindowText("");
		}
	}

// Helpers

BOOL CIMSMDriveAddrDialog::AllowSpace(CSpace *pSpace)
{
	return	pSpace->m_uTable == addrNamed

			? SelectList( pSpace, pSpace->m_uMinimum, m_uAllowSpace )

			: m_uAllowSpace == GetTableSpace(pSpace);
	}

BOOL CIMSMDriveAddrDialog::SelectList(CSpace *pSpace, UINT uCommand, UINT uAllow)
{
	if( IsHeader(uCommand) ) return TRUE;

	return uAllow == GetTableSpace(pSpace);
	}

void CIMSMDriveAddrDialog::SetAllow()
{
	if( m_pSpace->m_uMaximum < FZ ) {

		m_uAllowSpace = GetTableSpace(m_pSpace);

		return;
		}

	UINT uCommand = m_pSpace->m_uTable;

	for( UINT i = SPMOTION; i <= SPMISC; i++ ) {

		if( SelectList(m_pSpace, uCommand, i) ) {

			m_uAllowSpace = i;

			return;
			}
		}
	}

UINT CIMSMDriveAddrDialog::IsHeader(UINT uCommand)
{
	return uCommand >= SPMOTION && uCommand <= SPMISC ? 1 : 0;
	}

UINT CIMSMDriveAddrDialog::GetTableSpace(CSpace *pSpace)
{
	if( pSpace->m_uTable < LISTIO )		return SPMOTION;

	if( pSpace->m_uTable < LISTPOSITION )	return SPIO;

	if( pSpace->m_uTable < LISTENCODER )	return SPPOSITION;

	if( pSpace->m_uTable < LISTMISC )	return SPENCODER;

	return SPMISC;
	}

// End of File
