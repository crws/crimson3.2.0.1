
#include "intern.hpp"

#include "mitfx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX Series
//

// Instantiator

ICommsDriver *	Create_MitsubFxDriver(void)
{
	return New CMitsubFxDriver;
	}

// Constructor

CMitsubFxDriver::CMitsubFxDriver(void)
{
	m_wID		= 0x3303;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Mitsubishi Electric";
	
	m_DriverName	= "FX Series";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Mitsubishi FX";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Binding Control

UINT CMitsubFxDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMitsubFxDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Implementation

void CMitsubFxDriver::AddSpaces(void)
{
	AddSpace(New CSpace('D', "D",  "Data Registers",	10,  0, 8255, addrWordAsWord,  addrWordAsLong, 4));
	AddSpace(New CSpace('M', "M",  "Internal Relays",	10,  0, 4095, addrBitAsWord,   addrBitAsLong,  4));
	AddSpace(New CSpace('S', "S",  "State Relays",		10,  0, 1023, addrBitAsWord,   addrBitAsLong,  4));
	AddSpace(New CSpace('X', "X",  "Inputs",		 8,  0,  255, addrBitAsWord,   addrBitAsLong,  3));
	AddSpace(New CSpace('Y', "Y",  "Outputs",		 8,  0,  255, addrBitAsWord,   addrBitAsLong,  3));
	AddSpace(New CSpace('C', "C",  "Counter Values",	10,  0,  199, addrWordAsWord,  addrWordAsLong, 3));
	AddSpace(New CSpace('L', "LC", "Long Counter Values",	10,200,  255, addrLongAsLong,  addrLongAsLong, 3));
	AddSpace(New CSpace('T', "T",  "Timer Values",		10,  0,  255, addrWordAsWord,  addrWordAsLong, 3));
	AddSpace(New CSpace('B', "M8", "Special Relays",	10,  0,  255, addrBitAsWord,   addrBitAsWord,  3));
	
	}

BOOL CMitsubFxDriver::CheckAlignment(CSpace *pSpace)
{
	if( pSpace ) {

		if( pSpace->m_uType == addrBitAsWord ||
		    pSpace->m_uType == addrBitAsLong ) {

			return TRUE;
			}
		}  
	
	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX2N Series TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMitsFX2NMasterTCPDeviceOptions, CUIItem);

// Constructor

CMitsFX2NMasterTCPDeviceOptions::CMitsFX2NMasterTCPDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket  = 1024;

	m_Keep    = TRUE;

	m_Ping    = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;
	}

// Download Support	     

BOOL CMitsFX2NMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CMitsFX2NMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}


//////////////////////////////////////////////////////////////////////////
//
// Mistubishi FX2N Series Master TCP/IP
//

// Instantiator

ICommsDriver *	Create_MitsFX2NMasterTCPDriver(void)
{
	return New CMitsFX2NMasterTCPDriver;
	}

// Constructor

CMitsFX2NMasterTCPDriver::CMitsFX2NMasterTCPDriver(void)
{
	m_wID		= 0x406C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Mitsubishi";
	
	m_DriverName	= "FX2N Encapsulated TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "FX2N TCP/IP Master";

	}

// Binding Control

UINT CMitsFX2NMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CMitsFX2NMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CMitsFX2NMasterTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CMitsFX2NMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMitsFX2NMasterTCPDeviceOptions);
	}



// End of File
