
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CANCALIBRATION_HPP
	
#define	INCLUDE_CANCALIBRATION_HPP

//////////////////////////////////////////////////////////////////////////
//
// Declarations
//

class CCANCalibrationCreateEditDialog;
class CCANCalibrationAddressListDialog;

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Address
//

class CCANCalibrationAddress : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANCalibrationAddress(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Copying
		void InitFrom(CCANCalibrationAddress const &That);

		// CCP Properties
		CString m_Name;
		UINT    m_Address;
		UINT    m_Extension;
		UINT    m_Size;
		UINT    m_SizeX;
		UINT    m_SizeY;
		BOOL    m_fDaq;
		UINT    m_Type;
		UINT    m_Ref;

		// CCP DAQ Properties
		DWORD m_CanId;
		UINT  m_DaqId;
		UINT  m_Odt;
		UINT  m_Elem;
		UINT  m_DaqOffset;

		// Data Scaling
		int m_Radix;
		int m_Scale;
		int m_Offset;

		// Comparison Operators
		bool operator ==(const CCANCalibrationAddress &That) const;
		bool operator < (const CCANCalibrationAddress &That) const;
		bool operator > (const CCANCalibrationAddress &That) const;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Address List
//

class CCANCalibrationAddressList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Destructor
		~CCANCalibrationAddressList(void);

		// Item Access
		CCANCalibrationAddress * GetItem(INDEX Index) const;
		CCANCalibrationAddress * GetItem(UINT  uPos ) const;
		CCANCalibrationAddress * FindByRef(UINT uRef) const;
		CCANCalibrationAddress * FindByName(CString const &Name) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 SPN Defintion Helper
//

struct SPNDef
{
	// Properties
	CString m_Name;
	CString m_Abbrev;
	UINT    m_Number;
	UINT    m_Type;
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 SPN 
//

class CCANCalibrationSPN : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CCANCalibrationSPN(void);
		CCANCalibrationSPN(CString const &Name, CString const& Abbrev, UINT uNumber, UINT uBits);

		// Properties
		CString m_Name;
		CString m_Abbrev;
		UINT    m_Number;
		UINT    m_Bits;
		UINT    m_Offset;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 SPN List
//

class CCANCalibrationSPNList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
				
		// Destructor
		~CCANCalibrationSPNList(void);

		// Item Access
		CCANCalibrationSPN * GetItem(INDEX Index) const;
		CCANCalibrationSPN * GetItem(UINT  uPos ) const;
		CCANCalibrationSPN * FindByNumber(UINT uNum) const;
		CCANCalibrationSPN * FindByName(CString const &Name) const;
		CCANCalibrationSPN * FindByAbbrev(CString const &Abbrev) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 PGN Defintion
//

class CCANCalibrationPGN : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CCANCalibrationPGN(void);
		CCANCalibrationPGN(CString const &Name, CString const &Abbrev, UINT uNumber, UINT uPriority, BOOL fCommand, BOOL fUser, UINT uUpdate = 2500);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// SPN
		BOOL AddSPN(CCANCalibrationSPN * pSPN);
		BOOL RemoveSPN(CCANCalibrationSPN * pSPN);
		CCANCalibrationSPN * FindSPN(CString const &Abbrev);
		CCANCalibrationSPN * SPNAt(UINT uPos);
		UINT FindSPNOffset(CCANCalibrationSPN * pSPN);

		// Comparison Operators
		bool operator ==(const CCANCalibrationPGN &That) const;
		bool operator < (const CCANCalibrationPGN &That) const;
		bool operator > (const CCANCalibrationPGN &That) const;

		// Properties
		CString m_Name;
		CString m_Abbrev;
		UINT    m_Number;
		UINT    m_Priority;
		UINT    m_Update;
		BOOL    m_fCommand;
		BOOL    m_fUser;

		CCANCalibrationSPNList * m_pSPNs;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Data Members
		UINT m_BitOffset;
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 PGN List
//

class CCANCalibrationPGNList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Destructor
		~CCANCalibrationPGNList(void);

		// Item Access
		CCANCalibrationPGN * GetItem(INDEX Index) const;
		CCANCalibrationPGN * GetItem(UINT  uPos ) const;
		CCANCalibrationPGN * FindByNumber(UINT uNum) const;
		CCANCalibrationPGN * FindByName(CString const &Name) const;
		CCANCalibrationPGN * FindByAbbrev(CString const &Abbrev) const;
	};

/////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Driver Options
//

class CCANCalibrationDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANCalibrationDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_MasterJ1939;
	
	protected:	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Device Options
//

class CCANCalibrationDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANCalibrationDeviceOptions(void);
		
		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		void Init(void);
		void PostLoad(void);

		BOOL DoParseAddress(CError &Error, CAddress &Addr,CString Text);
		BOOL DoExpandAddress(CString &Text, CAddress const &Addr);
		BOOL DoSelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);

		// Address Management
		BOOL AddAddress(CCANCalibrationAddress const &Addr, CError &Error);
		BOOL RemoveAddress(CCANCalibrationAddress const &Addr);		
		UINT FindType(CCANCalibrationAddress const &Address);
		CCANCalibrationAddress * FindAddress(CString Name);

	protected:
		// Data Members
		CCANCalibrationAddressList       * m_pAddresses;
		CCANCalibrationPGNList           * m_pPGNs;

		UINT m_uTableMax;
		UINT m_uNamedMax;

		// Properties
		BOOL m_Ping;
		UINT m_Push;
		UINT m_Station;
		UINT m_Timeout;
		UINT m_DaqTime;
		UINT m_MasterId;
		UINT m_SlaveId;
		UINT m_SlaveJ1939;
		UINT m_Order;
		UINT m_Key;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT AllocRef(CCANCalibrationAddress const &Address);
		UINT FindNextNamed(void);
		UINT FindNextTable(void);
		UINT FindOffset(CError &Error, CString Text, CCANCalibrationAddress * pAddr);
		BOOL ValidateName(CString const &Name);

		// Limited J1939 Support
		BOOL AddPGN(CCANCalibrationPGN * pPGN);
		BOOL EditPGN(CCANCalibrationPGN * pPGN);
		BOOL RemovePGN(CCANCalibrationPGN * pPGN);
		void MakeDefaultPGNs(void);
		void MakeDM1(void);
		void MakeDM2(void);
		void MakeDM3(void);
		void MakeDM4(void);
		void MakeDM11(void);
		BOOL ParseJ1939DM(CError &Error, CAddress &Addr, CString Text);
		UINT FindPGNOffset(CCANCalibrationPGN * pPGN);
		CCANCalibrationPGN * PGNAt(UINT uPos);

		// Debug
		void DumpPGNs(void);
		
		// Friends
		friend class CCANCalibrationCreateEditDialog;
		friend class CCANCalibrationAddressListDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Driver
//

class CCANCalibrationDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CCANCalibrationDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
		CLASS GetDriverConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Address Configuration Dialog
//

class CCANCalibrationCreateEditDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCANCalibrationCreateEditDialog(CCANCalibrationAddress * pAddr, CCANCalibrationDeviceOptions * pOptions);

		// Dialog Result
		CCANCalibrationAddress * GetAddress(void);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uId);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);

	protected:
		// Data Members
		CCANCalibrationDeviceOptions * m_pDevice;
		CCANCalibrationAddress       * m_pAddr;

		// Implementation
		void DoEnables(void);
		void SetTextLimits(void);
		void ResetFields(void);
		void ShowAddress(CCANCalibrationAddress * pAddr);
		void SetComboSelData(CComboBox &Combo, UINT uData);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Address List Dialog
//

class CCANCalibrationAddressListDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCANCalibrationAddressListDialog(BOOL fSelect, CCANCalibrationDeviceOptions * pOptions);
		CCANCalibrationAddressListDialog(BOOL fSelect, CCANCalibrationDeviceOptions * pOptions, CAddress const &Addr);

		// Operations
		void GetAddress(CAddress &Addr);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uId);
		BOOL OnAddAddress(UINT uId);
		BOOL OnEditAddress(UINT uId);
		BOOL OnRemoveAddress(UINT uId);	

		// Notification Handlers
		void OnDblClk(UINT uId, NMHDR &HDR);
		void OnListItemChange(UINT uID, NMLISTVIEW &Info);
		void OnDMChange(UINT uID, CWnd &Wnd);
		void OnSPNChange(UINT uID, CWnd &Wnd);
		void OnListDblClk(UINT uID, CWnd &Wnd);
		
	protected:
		// Data Members
		CCANCalibrationDeviceOptions * m_pDevice;
		CAddress m_Addr;
		CString  m_Name;
		BOOL     m_fSelect;
		BOOL     m_fChange;
		UINT     m_uOffsetX;
		UINT     m_uOffsetY;

		// Implementation
		void DoEnables(CCANCalibrationAddress const * pAddr);
		void LoadList(void);
		void LoadJ1939(void);
		void LoadSPNs(void);
		BOOL IsDM(CString const &Text);
		void AddColHeaders(CListView &List);
		void LoadItem(UINT uPos, CListView &List, CCANCalibrationAddress const * pAddr);
		void ShowCurrentAddress(void);
		void ShowOffsets(CCANCalibrationAddress * pAddr, UINT uOffset);
	};

// End of File

#endif
