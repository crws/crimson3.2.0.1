
#include "intern.hpp"

#include "cdl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCatDataLinkDriverOptions, CUIItem);

// Constructor

CCatDataLinkDriverOptions::CCatDataLinkDriverOptions(void)
{
	m_Drop      = 0xAA;

	m_ScanMIDs  = 0;
	m_Scan1     = 0;
	m_Scan2     = 0;
	m_Scan3     = 0;
	m_Scan4     = 0;
	
	m_ScanFrom  = 0x21;
	m_ScanFrom1 = 0x21;
	m_ScanFrom2 = 0x21;
	m_ScanFrom3 = 0x21;
	m_ScanFrom4 = 0x21;

	m_ScanTo    = 0x29;
	m_ScanTo1   = 0x29;
	m_ScanTo2   = 0x29;
	m_ScanTo3   = 0x29;
	m_ScanTo4   = 0x29;

	m_Delay     = 30;
	m_Delay1    = 30;
	m_Delay2    = 30;
	m_Delay3    = 30;
	m_Delay4    = 30;

	m_Faults    = 0;

	m_Detail    = 0;
	}

// UI Managament

void CCatDataLinkDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "ScanMIDs" ) {

			pWnd->EnableUI("ScanFrom", m_ScanMIDs);
			pWnd->EnableUI("ScanTo",   m_ScanMIDs);
			pWnd->EnableUI("Delay",    m_ScanMIDs);
			pWnd->EnableUI("Scan1",    m_ScanMIDs);
			pWnd->EnableUI("Scan2",    m_ScanMIDs);
			pWnd->EnableUI("Scan3",    m_ScanMIDs);
			pWnd->EnableUI("Scan4",    m_ScanMIDs);
			}

		if( Tag.IsEmpty() || Tag == "ScanMIDs" || Tag == "Scan1" ) {

			pWnd->EnableUI("ScanFrom1", m_ScanMIDs && m_Scan1);
			pWnd->EnableUI("ScanTo1",   m_ScanMIDs && m_Scan1);
			pWnd->EnableUI("Delay1",    m_ScanMIDs && m_Scan1);
			}

		if( Tag.IsEmpty() || Tag == "ScanMIDs" || Tag == "Scan2" ) {

			pWnd->EnableUI("ScanFrom2", m_ScanMIDs && m_Scan2);
			pWnd->EnableUI("ScanTo2",   m_ScanMIDs && m_Scan2);
			pWnd->EnableUI("Delay2",    m_ScanMIDs && m_Scan2);
			}

		if( Tag.IsEmpty() || Tag == "ScanMIDs" || Tag == "Scan3" ) {

			pWnd->EnableUI("ScanFrom3", m_ScanMIDs && m_Scan3);
			pWnd->EnableUI("ScanTo3",   m_ScanMIDs && m_Scan3);
			pWnd->EnableUI("Delay3",    m_ScanMIDs && m_Scan3);
			}

		if( Tag.IsEmpty() || Tag == "ScanMIDs" || Tag == "Scan4" ) {

			pWnd->EnableUI("ScanFrom4", m_ScanMIDs && m_Scan4);
			pWnd->EnableUI("ScanTo4",   m_ScanMIDs && m_Scan4);
			pWnd->EnableUI("Delay4",    m_ScanMIDs && m_Scan4);
			}

		if( Tag.IsEmpty() || Tag == "Faults" ) {

			pWnd->EnableUI("Detail", m_Faults);
			}

		pWnd->EnableUI("Drop", FALSE);
		}
	}

// Download Support

BOOL CCatDataLinkDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_Faults));
	
	Init.AddByte(BYTE(m_Detail));

	Init.AddByte(BYTE(m_ScanMIDs));

	Init.AddByte(BYTE(1));
	Init.AddByte(BYTE(m_ScanFrom));
	Init.AddByte(BYTE(m_ScanTo));
	Init.AddWord(WORD(m_Delay));

	Init.AddByte(BYTE(m_Scan1));
	Init.AddByte(BYTE(m_ScanFrom1));
	Init.AddByte(BYTE(m_ScanTo1));
	Init.AddWord(WORD(m_Delay1));

	Init.AddByte(BYTE(m_Scan2));
	Init.AddByte(BYTE(m_ScanFrom2));
	Init.AddByte(BYTE(m_ScanTo2));
	Init.AddWord(WORD(m_Delay2));

	Init.AddByte(BYTE(m_Scan3));
	Init.AddByte(BYTE(m_ScanFrom3));
	Init.AddByte(BYTE(m_ScanTo3));
	Init.AddWord(WORD(m_Delay3));

	Init.AddByte(BYTE(m_Scan4));
	Init.AddByte(BYTE(m_ScanFrom4));
	Init.AddByte(BYTE(m_ScanTo4));
	Init.AddWord(WORD(m_Delay4));
	
	return TRUE;
	}

// Meta Data Creation

void CCatDataLinkDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(ScanMIDs);
	Meta_AddInteger(Scan1);
	Meta_AddInteger(Scan2);
	Meta_AddInteger(Scan3);
	Meta_AddInteger(Scan4);
	Meta_AddInteger(ScanFrom);
	Meta_AddInteger(ScanFrom1);
	Meta_AddInteger(ScanFrom2);
	Meta_AddInteger(ScanFrom3);
	Meta_AddInteger(ScanFrom4);
	Meta_AddInteger(ScanTo);
	Meta_AddInteger(ScanTo1);
	Meta_AddInteger(ScanTo2);
	Meta_AddInteger(ScanTo3);
	Meta_AddInteger(ScanTo4);
	Meta_AddInteger(Delay);
	Meta_AddInteger(Delay1);
	Meta_AddInteger(Delay2);
	Meta_AddInteger(Delay3);
	Meta_AddInteger(Delay4);
	Meta_AddInteger(Faults);
	Meta_AddInteger(Detail);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCatDataLinkDriverOptions2, CUIItem);

// Constructor

CCatDataLinkDriverOptions2::CCatDataLinkDriverOptions2(void)
{
	m_Drop    = 0xAA;

	m_Faults  = 0;

	m_Detail  = 0;

	m_Write   = 0;

	m_StdPend = 15;

	m_Conn	  = 0;	
	}

// UI Managament

void CCatDataLinkDriverOptions2::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Faults" ) {

			pWnd->EnableUI("Detail", m_Faults);
			}

		if( Tag == "Write" && m_Write ) {

			CString Text = "WARNING:\n\nWrites to PID parameters are performed at "
				       "the customer's own risk. Monico's products do not "
				       "perform range checks on written data. Further, they "
				       "are inherently complex electronic devices that cannot "
				       "be warranted as without fault. INCORRECTLY WRITTEN "
				       "DATA MAY CAUSE DAMAGE TO ENGINES AND OTHER EQUIPMENT. "
				       "If you enable writes, you hereby agree to hold harmless "
				       "and indemnify Monico, its agents, employees and suppliers "
				       "for any damage caused by use of the write facility or "
				       "by any software errors that such uses exposes. You also "
				       "agree to re-accept any Terms and Conditions under which "
				       "the product was supplied by Monico and the limitations "
				       "on liability included therein.\n\n"
				       "Do you accept these conditions?\n\n"
				       "ONLY CLICK 'YES' IF YOU WISH TO BE "
				       "LEGALLY BOUND BY THESE TERMS.";

			if( pWnd->NoYes(Text) == IDNO ) {

				m_Write = 0;

				pWnd->UpdateUI("Write");
				}
			}

		pWnd->EnableUI("Drop", FALSE);
		}
	}

// Download Support

BOOL CCatDataLinkDriverOptions2::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_Faults));
	
	Init.AddByte(BYTE(m_Detail));
	
	Init.AddByte(BYTE(m_Write));

	Init.AddByte(BYTE(m_StdPend));

	Init.AddWord(WORD(m_Conn));
	
	return TRUE;
	}

// Meta Data Creation

void CCatDataLinkDriverOptions2::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Faults);
	Meta_AddInteger(Detail);
	Meta_AddInteger(Write);
	Meta_AddInteger(StdPend);
	Meta_AddInteger(Conn);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCatDataLinkDeviceOptions, CUIItem);

// Constructor

CCatDataLinkDeviceOptions::CCatDataLinkDeviceOptions(void)
{
	m_Drop = 0x0E;	
	}

// Download Support

BOOL CCatDataLinkDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CCatDataLinkDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Fault Code Fields
//

static PCSTR const m_pFaults[] = {

	"FaultSource",
	"FaultCode",
	"FaultSubcode",
	"FaultFlags",
	"FaultCount",
	"FaultFirst",
	"FaultLast",
	" ",
	" ",
	" ",
	"ActFaultSource",
	"ActFaultCode",
	"ActFaultSubcode",
	"ActFaultDiagEvent",
	"ActFaultCount",
	"ActFaultFirst",
	"ActFaultLast",
	" ",
	" ",
	" ",
	"InActFaultSource",
	"InActFaultCode",
	"InActFaultSubcode",
	"InActFaultDiagEvent",
	"InActFaultCount",
	"InActFaultFirst",
	"InActFaultLast",
	" ",
	" ",
	" ",
	"ActEventFaultSource",
	"ActEventFaultCode",
	"ActEventFaultSubcode",
	"ActEventFaultDiagEvent",
	"ActEventFaultCount",
	"ActEventFaultFirst",
	"ActEventFaultLast",
	" ",
	" ",
	" ",
	"ActDiagsFaultSource",
	"ActDiagsFaultCode",
	"ActDiagsFaultSubcode",
	"ActDiagsFaultDiagEvent",
	"ActDiagsFaultCount",
	"ActDiagsFaultFirst",
	"ActDiagsFaultLast",
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Table Entry
//

struct CPID
{
	UINT	m_uPID;
	UINT	m_uMonico;
	PCSTR	m_pName;
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Table Data
//

static CPID const m_PID[] = {

	{	0x000001,	1,	"NoEngRatingMapsAvail"		 },
	{	0x000002,	2,	"EngineRatingMapInUse"		 },
	{	0x000003,	3,	"DetonationLevel"		 },	
	{	0x000007,	4,	"NumberOfDetonationSensors"	 },
	{	0x000008,	5,	"EngineConfiguration"		 },
	{	0x00000C,	6,	"OxygenSensorStatus"		 },
	{	0x00000D,	7,	"RemoteFaultReset"		 },
	{	0x000011,	8,	"IgnitionTimingCalibration"	 },
	{	0x000016,	9,	"InletManifoldTemp"		 },
	{	0x00F014,	10,	"CooldownDuration"		 },
	{	0x00F016,	11,	"ColdModeStatus"		 },
	{	0x00F01B,	12,	"EnginePrelubeStatus"		 },
	{	0x00F02A,	13,	"RemoteStartStatus"		 },
	{	0x00F02C,	14,	"EngineCoolantLevelStatus"	 },
	{	0x00F08F,	15,	"EngineControlSwitchPosition"	 },
	{	0x00F09C,	16,	"ShutdownNotifyRelayStatus"	 },
	{	0x00F0A6,	17,	"OverspeedTestSwitchPosition"	 },
	{	0x00F0A8,	18,	"RemoteEstopSwitchPosition"	 },
	{	0x00F0A9,	19,	"StartupModeStatus"		 },
	{	0x00F0AA,	20,	"AirShutoffStatus"		 },
	{	0x00F0AC,	21,	"MaxNumberOfCrankCycles"	 },
	{	0x00F0B0,	22,	"GeneratorPhaseSelect"		 },
	{	0x00F0B1,	23,	"RemoteEmergencyStop"		 },
	{	0x00F0B2,	24,	"CooldownOverrideControl"	 },
	{	0x00F0B3,	25,	"GenPotXformerRatioSP"		 },
	{	0x00F0B4,	26,	"GenCurrXformerRatioSP"		 },
	{	0x00F0B5,	27,	"EnginePrelubeStatus"		 },
	{	0x00F0B6,	28,	"EngLowFuelLevelStatus"		 },
	{	0x00F0C1,	29,	"AccelerationDelayTime"		 },
	{	0x00F0C2,	30,	"RemoteThrottleOverride"	 },
	{	0x00F0F2,	31,	"ECMInControlMarine"		 },
	{	0x00F0FD,	32,	"LowIdleSwitchPosition"		 },
	{	0x00F108,	33,	"SpeedGovAux1ProportionAdjust"	 },
	{	0x00F109,	34,	"SpeedGovAux1IntegralAdjust"	 },
	{	0x00F10A,	35,	"SpeedGovAux1DerivativeAdjust"	 },
	{	0x00F10B,	36,	"SpeedGovPriProportionalAdjust"	 },
	{	0x00F10C,	37,	"SpeedGovPriIntegralAdjust"	 },
	{	0x00F10D,	38,	"SpeedGovPriDerivativeAdjust"	 },
	{	0x00F111,	39,	"PercentFuelPosition"		 },
	{	0x00F112,	40,	"3600EngineStatus"		 },
	{	0x00F113,	41,	"EngineOperation"		 },
	{	0x00F115,	42,	"GasFuelCorrectionFactor"	 },
	{	0x00F116,	43,	"WastegatePositionCommand"	 },
	{	0x00F117,	44,	"ChokePositionCommand"		 },
	{	0x00F118,	45,	"PercentEngineLoadFactor"	 },
	{	0x00F119,	46,	"AuxiliaryStatus"		 },
	{	0x00F11C,	47,	"AFRControlProportionalAdjust"	 },
	{	0x00F11D,	48,	"AFRControlIntegralAdjust"	 },
	{	0x00F121,	49,	"EIS_Mode"			 },
	{	0x00F122,	50,	"NumberLoadSensTimingMaps"	 },
	{	0x00F123,	51,	"LoadSensitiveTimingMapInUse"	 },
	{	0x00F124,	52,	"EIS_SpecialTestStatus"		 },
	{	0x00F125,	53,	"AFRControlStatus"		 },
	{	0x00F129,	54,	"SpeedGovAux1GainStatus"	 },
	{	0x00F14F,	55,	"BackupECMStatusMarine"		 },
	{	0x00F192,	56,	"DiagnosticStatusSummary"	 },
	{	0x00F2C3,	57,	"FuelCorrectionFactor"		 },
	{	0x00F127,	58,	"GasFuelQuality3500"		 },
	{	0x00F1D3,	59,	"GenPhAPowerFactLeadLag"	 },
	{	0x00F1D4,	60,	"GenPhBPowerFactLeadLag"	 },
	{	0x00F1D5,	61,	"GenPhCPowerFactLeadLag"	 },
	{	0x00F1D6,	62,	"GenAvePowerFactorLeadLag"	 },
	{	0x00F213,	63,	"RemoteStartStop"		 },
	{	0x00F24D,	64,	"ShutdownEmergOverideSwSt"	 },
	{	0x00F24F,	65,	"GeneralAlarmOutStatusOveride"	 },
	{	0x00F28A,	66,	"GovGlobalGainAdjustment"	 },
	{	0x00F2CB,	67,	"EPGCircuitBreakerStatus"	 },
	{	0x00F2CC,	68,	"RemoteGenSyncControl"		 },
	{	0x00F2D6,	69,	"RemoteSyncControlReadiness"	 },
	{	0x00F2D7,	70,	"GenSyncControlStatus"		 },
	{	0x000040,	71,	"EngineRpm"			 },
	{	0x000042,	72,	"GenSetRingGearTeethSetpt"	 },
	{	0x000044,	73,	"EngineCoolantTemperature"	 },
	{	0x000047,	74,	"DesiredEngineTiming"		 },
	{	0x00004D,	75,	"TransmissionOilTempMarine"	 },
	{	0x00005E,	76,	"GeneratorSetHourmeter"		 },
	{	0x00F417,	77,	"EngineStatus"			 },
	{	0x00F420,	78,	"EngFrontAftercoolantTemp"	 },
	{	0x00F440,	79,	"RightExhaustTemperature"	 },
	{	0x00F441,	80,	"LeftExhaustTemperature"	 },
	{	0x00F442,	81,	"GenRMSVoltsPhase_A_To_B"	 },
	{	0x00F443,	82,	"GenRMSVoltsPhase_B_To_C"	 },
	{	0x00F444,	83,	"GenRMSVoltsPhase_C_To_A"	 },
	{	0x00F445,	84,	"GenRMSVoltsPhase_A_to_Neutral"	 },
	{	0x00F446,	85,	"GenRMSVoltsPhase_B_to_Neutral"	 },
	{	0x00F447,	86,	"GenRMSVoltsPhase_C_to_Neutral"	 },
	{	0x00F448,	87,	"GenPhase_A_RMSCurrent"		 },
	{	0x00F449,	88,	"GenPhase_B_RMSCurrent"		 },
	{	0x00F44A,	89,	"GenPhase_C_RMSCurrent"		 },
	{	0x00F44C,	90,	"GSC_RelayStatus"		 },
	{	0x00F44D,	91,	"GSC_RelayControl"		 },
	{	0x00F45B,	92,	"SpeedRampRate"			 },
	{	0x00F460,	93,	"GeneratorAlarmStatus"		 },
	{	0x00F461,	94,	"GeneratorShutdownStatus"	 },
	{	0x00F462,	95,	"GSC_SpareFaultAlarmStatus"	 },
	{	0x00F463,	96,	"GSC_SpareFaultShutdnStatus"	 },
	{	0x00F464,	97,	"Generator_LineToLine_Voltage"	 },
	{	0x00F465,	98,	"Generator_LineCurrent"		 },
	{	0x00F466,	99,	"EngineOverspeedSetpoint"	 },
	{	0x00F467,	100,	"EngineOilStepSpeedSetpoint"	 },
	{	0x00F468,	101,	"LoEngOilPresAtRatedSpeedSP"	 },
	{	0x00F469,	102,	"LoEngOilPresAtIdleSpeedSP"	 },
	{	0x00F46A,	103,	"HiEngineCoolantTempSetpoint"	 },
	{	0x00F46B,	104,	"LoEngineCoolantTempSetpoint"	 },
	{	0x00F46C,	105,	"GSC_Configuration"		 },
	{	0x00F46D,	106,	"CooldownTimeRemain"		 },
	{	0x00F48F,	107,	"EtherUsage"			 },
	{	0x00F4A0,	108,	"ExhaustTemperature"		 },
	{	0x00F4C3,	109,	"GeneratorAveRMS_Voltage"	 },
	{	0x00F4C4,	110,	"GeneratorTotalRMS_Current"	 },
	{	0x00F4D0,	111,	"GeneratorBusRMS_Voltage"	 },
	{	0x00F4D1,	112,	"GeneratorControlOutputStatus"	 },
	{	0x00F4D2,	113,	"GeneratorShutdownStatusExt1"	 },
	{	0x00F50A,	114,	"CooldownEngineSpeed"		 },
	{	0x00F50B,	115,	"CycleCrankTimeSetpont"		 },
	{	0x00F50C,	116,	"TotalCrankCycleTime"		 },
	{	0x00F50D,	117,	"CrankTerminateRPM"		 },
	{	0x00F510,	118,	"LowIdleSpeed"			 },
	{	0x00F518,	119,	"FuelRateOld"			 },
	{	0x00F51A,	120,	"FuelQualityValueInput"		 },
	{	0x00F51E,	121,	"IntakeManifoldAirFlow"		 },
	{	0x00F53E,	122,	"EngineOilTemperature"		 },
	{	0x00F5E0,	123,	"UserDefinedSwitchStatus"	 },
	{	0x00F430,	124,	"ExhaustPortTemperature_1"	 },
	{	0x00F431,	125,	"ExhaustPortTemperature_2"	 },
	{	0x00F432,	126,	"ExhaustPortTemperature_3"	 },
	{	0x00F433,	127,	"ExhaustPortTemperature_4"	 },
	{	0x00F434,	128,	"ExhaustPortTemperature_5"	 },
	{	0x00F435,	129,	"ExhaustPortTemperature_6"	 },
	{	0x00F436,	130,	"ExhaustPortTemperature_7"	 },
	{	0x00F437,	131,	"ExhaustPortTemperature_8"	 },
	{	0x00F438,	132,	"ExhaustPortTemperature_9"	 },
	{	0x00F439,	133,	"ExhaustPortTemperature_10"	 },
	{	0x00F43A,	134,	"ExhaustPortTemperature_11"	 },
	{	0x00F43B,	135,	"ExhaustPortTemperature_12"	 },
	{	0x00F43C,	136,	"ExhaustPortTemperature_13"	 },
	{	0x00F43D,	137,	"ExhaustPortTemperature_14"	 },
	{	0x00F43E,	138,	"ExhaustPortTemperature_15"	 },
	{	0x00F43F,	139,	"ExhaustPortTemperature_16"	 },
	{	0x00F598,	140,	"ExhaustPortTemperature_17"	 },
	{	0x00F599,	141,	"ExhaustPortTemperature_18"	 },
	{	0x00F59A,	142,	"ExhaustPortTemperature_19"	 },
	{	0x00F59B,	143,	"ExhaustPortTemperature_20"	 },
	{	0x00F593,	144,	"RightOddBankTurbineInletTemp"	 },
	{	0x00F595,	145,	"RightOddBankTurbineOutTemp"	 },
	{	0x00F594,	146,	"LeftEvenBankTurbineInletTemp"	 },
	{	0x00F596,	147,	"LeftEvenBankTurbineOutTemp"	 },
	{	0x00F55C,	148,	"LeftBankExhaustPort"		 },
	{	0x00F55D,	149,	"RightBankExhaustPort"		 },
	{	0x00F62B,	150,	"ActiveWarningSummaryStatus"	 },
	{	0x00F0E8,	151,	"EngCoolantPumpPressStatus"	 },
	{	0x00F1D0,	152,	"JacketWaterToEngOilDiffTemp"	 },
	{	0x00F48D,	153,	"EngineCoolantPressAbsolute"	 },
	{	0x00F57B,	154,	"EngOvercrankTimeOld"		 },
	{	0x00F57F,	155,	"EnginePurgeCycleTime"		 },
	{	0x00F58E,	156,	"GasFuelFlowNew"		 },
	{	0xD00020,	157,	"DetonationLevel_1"		 },
	{	0xD00021,	158,	"DetonationLevel_2"		 },
	{	0xD00022,	159,	"DetonationLevel_3"		 },
	{	0xD00023,	160,	"DetonationLevel_4"		 },
	{	0xD00024,	161,	"DetonationLevel_5"		 },
	{	0xD00025,	162,	"DetonationLevel_6"		 },
	{	0xD00026,	163,	"DetonationLevel_7"		 },
	{	0xD00027,	164,	"DetonationLevel_8"		 },
	{	0xD00028,	165,	"DetonationLevel_9"		 },
	{	0xD00029,	166,	"DetonationLevel_10"		 },
	{	0xD0002A,	167,	"DetonationLevel_11"		 },
	{	0xD0002B,	168,	"DetonationLevel_12"		 },
	{	0xD0002C,	169,	"DetonationLevel_13"		 },
	{	0xD0002D,	170,	"DetonationLevel_14"		 },
	{	0xD0002E,	171,	"DetonationLevel_15"		 },
	{	0xD0002F,	172,	"DetonationLevel_16"		 },
	{	0xD00030,	173,	"DetonationLevel_17"		 },
	{	0xD00031,	174,	"DetonationLevel_18"		 },
	{	0xD00032,	175,	"DetonationLevel_19"		 },
	{	0xD00033,	176,	"DetonationLevel_20"		 },
	{	0xD000EB,	177,	"TransformerSecondaryOutV_1"	 },
	{	0xD000EC,	178,	"TransformerSecondaryOutV_2"	 },
	{	0xD000ED,	179,	"TransformerSecondaryOutV_3"	 },
	{	0xD000EE,	180,	"TransformerSecondaryOutV_4"	 },
	{	0xD000EF,	181,	"TransformerSecondaryOutV_5"	 },
	{	0xD000F0,	182,	"TransformerSecondaryOutV_6"	 },
	{	0xD000F1,	183,	"TransformerSecondaryOutV_7"	 },
	{	0xD000F2,	184,	"TransformerSecondaryOutV_8"	 },
	{	0xD000F3,	185,	"TransformerSecondaryOutV_9"	 },
	{	0xD000F4,	186,	"TransformerSecondaryOutV_10"	 },
	{	0xD000F5,	187,	"TransformerSecondaryOutV_11"	 },
	{	0xD000F6,	188,	"TransformerSecondaryOutV_12"	 },
	{	0xD000F7,	189,	"TransformerSecondaryOutV_13"	 },
	{	0xD000F8,	190,	"TransformerSecondaryOutV_14"	 },
	{	0xD000F9,	191,	"TransformerSecondaryOutV_15"	 },
	{	0xD000FA,	192,	"TransformerSecondaryOutV_16"	 },
	{	0xD000FB,	193,	"TransformerSecondaryOutV_17"	 },
	{	0xD000FC,	194,	"TransformerSecondaryOutV_18"	 },
	{	0xD000FD,	195,	"TransformerSecondaryOutV_19"	 },
	{	0xD000FE,	196,	"TransformerSecondaryOutV_20"	 },
	{	0xD0027D,	197,	"MaxEngineHighIdleSpeed"	 },
	{	0xD0027E,	198,	"MinHighEngineIdleSpeed"	 },
	{	0xD0027F,	199,	"HiInletAirTempEngLoadSetpt"	 },
	{	0xD10066,	200,	"HydraxOilPressSwitchStatus"	 },
	{	0xD10120,	201,	"NormalStopInputStatus"		 },
	{	0xD10293,	202,	"EmmisionsFeedbackMode"		 },
	{	0xD00109,	203,	"FuelValvePosition"		 },
	{	0xD002AE,	204,	"MaxFuelFlowSetting"		 },
	{	0xD00453,	205,	"EngOil_to_EngCoolDiffTemp"	 },
	{	0xD100A0,	206,	"O2FeedbackFeaturEnableStatus"	 },
	{	0xD10104,	207,	"O2SensorStatus"		 },
	{	0xD1013A,	208,	"AutoPowerBalanceModeTandem"	 },
	{	0xD10167,	209,	"O2FeedbackEnabledStatus"	 },
	{	0x00F597,	210,	"EngAveExhaustPortTemp"		 },
	{	0x00F5C9,	211,	"RequestEngExhaustPortTemp"	 },
	{	0x00F55E,	212,	"ThermocoupleTemp_1"		 },
	{	0x00F55F,	213,	"ThermocoupleTemp_2"		 },
	{	0x00F560,	214,	"ThermocoupleTemp_3"		 },
	{	0x00F561,	215,	"ThermocoupleTemp_4"		 },
	{	0x00F562,	216,	"ThermocoupleTemp_5"		 },
	{	0x00F563,	217,	"ThermocoupleTemp_6"		 },
	{	0x00F564,	218,	"ThermocoupleTemp_7"		 },
	{	0x00F565,	219,	"ThermocoupleTemp_8"		 },
	{	0x00F566,	220,	"ThermocoupleTemp_9"		 },
	{	0x00F567,	221,	"ThermocoupleTemp_10"		 },
	{	0x00F568,	222,	"ThermocoupleTemp_11"		 },
	{	0x00F569,	223,	"ThermocoupleTemp_12"		 },
	{	0x00F56A,	224,	"ThermocoupleTemp_13"		 },
	{	0x00F56B,	225,	"ThermocoupleTemp_14"		 },
	{	0x00F56C,	226,	"ThermocoupleTemp_15"		 },
	{	0x00F56D,	227,	"ThermocoupleTemp_16"		 },
	{	0x00F56E,	228,	"ThermocoupleTemp_17"		 },
	{	0x00F56F,	229,	"ThermocoupleTemp_18"		 },
	{	0x00F570,	230,	"ThermocoupleTemp_19"		 },
	{	0x00F571,	231,	"ThermocoupleTemp_20"		 },
	{	0x00F572,	232,	"ThermocoupleTemp_21"		 },
	{	0x00F573,	233,	"ThermocoupleTemp_22"		 },
	{	0x00F574,	234,	"ThermocoupleTemp_23"		 },
	{	0x00F575,	235,	"ThermocoupleTemp_24"		 },
	{	0xD003A5,	236,	"RightTurboTurbineSpeed"	 },
	{	0xD003A6,	237,	"LeftTurboTurbineSpeed"		 },
	{	0xD0012F,	238,	"DesiredSpeedInputConfig"	 },
	{	0x00FC07,	239,	"WarningStatus"			 },
	{	0x00FC08,	240,	"ShutdownStatus"		 },
	{	0x00FC09,	241,	"EngineDerateStatus"		 },
	{	0x00FC0D,	242,	"SpareOutputsGSC"		 },
	{	0x00FC0F,	243,	"GeneratorTotalRealPower"	 },
	{	0x00FC10,	244,	"RelayDriverModuleRelayState"	 },
	{	0x00FC11,	245,	"GenPhase_A_RealPower"		 },
	{	0x00FC12,	246,	"GenPhase_B_RealPower"		 },
	{	0x00FC13,	247,	"GenPhase_C_RealPower"		 },
	{	0x00FC14,	248,	"GenPhase_A_ReactivePower"	 },
	{	0x00FC15,	249,	"GenPhase_B_ReactivePower"	 },
	{	0x00FC16,	250,	"GenPhase_C_ReactivePower"	 },
	{	0x00FC17,	251,	"GeneratorTotalReactivePower"	 },
	{	0x00FC18,	252,	"GenPhase_A_ApparentPower"	 },
	{	0x00FC19,	253,	"GenPhase_B_ApparentPower"	 },
	{	0x00FC1A,	254,	"GenPhase_C_ApparentPower"	 },
	{	0x00FC1B,	255,	"GeneratorTotalApparentPower"	 },
	{	0x00FC1C,	256,	"GeneratorTotalKWHours"		 },
	{	0x00FC1D,	257,	"GeneratorTotalKVARHours"	 },
	{	0x00FC1E,	258,	"GeneratorShutdownStatus"	 },
	{	0x00FC1F,	259,	"GeneratorAlarmStatus"		 },
	{	0x00FC27,	260,	"WarnStatus_2Marine"		 },
	{	0x00FC28,	261,	"ShutdnStatus_2Marine"		 },
	{	0x000015,	262,	"ThrottlePosition"		 },
	{	0x00F013,	263,	"SystemBatteryVoltage"		 },
	{	0x00F11B,	264,	"EngineCompressionRatio"	 },
	{	0x00F189,	265,	"EnginePowDeratePercentage"	 },
	{	0x000041,	266,	"ActualEngineTiming"		 },
	{	0x000046,	267,	"DesiredEngineSpeed"		 },
	{	0x00004B,	268,	"RatedEngineSpeed"		 },
	{	0x00004E,	269,	"TransmissionOilPressAbsolute"	 },
	{	0x000053,	270,	"AtmosphericPressure"		 },
	{	0x000054,	271,	"EngineOilPressureGauge"	 },
	{	0x000055,	272,	"BoostPressureGauge"		 },
	{	0x000058,	273,	"AirFilterRestriction"		 },
	{	0x00005A,	274,	"FilteredEngineOilPressure"	 },
	{	0x00005B,	275,	"TurboOutletPressAbs"		 },
	{	0x00005C,	276,	"LeftTurboInletPressure"	 },
	{	0x00005F,	277,	"RightTurboInletPressure"	 },
	{	0x00005D,	278,	"IntakeManifoldPressure"	 },
	{	0x00F40E,	279,	"EngineOilFilterDiffPressure"	 },
	{	0x00F410,	280,	"EffectiveRack"			 },
	{	0x00F411,	281,	"EffectiveRackLimit"		 },
	{	0x00F412,	282,	"EffectiveSmokeRackLimit"	 },
	{	0x00F415,	283,	"PeakAirFilterRestriction"	 },
	{	0x00F419,	284,	"UnfilteredEngOilPresAbsolute"	 },
	{	0x00F41C,	285,	"FuelFilterDiffPressure"	 },
	{	0x00F41F,	286,	"UnfilteredEngFuelPressAbsol"	 },
	{	0x00F44B,	287,	"GeneratorFrequency"		 },
	{	0x00F44E,	288,	"ActualExhaustOxygenPercent"	 },
	{	0x00F44F,	289,	"DesiredExhaustOxygenPercent"	 },
	{	0x00F4C7,	290,	"GeneratorPowerPercentOfRated"	 },
	{	0x00F4C8,	291,	"GeneratorPhase_A_PowerFactor"	 },
	{	0x00F4C9,	292,	"GeneratorPhase_B_PowerFactor"	 },
	{	0x00F4CA,	293,	"GeneratorPhase_C_PowerFactor"	 },
	{	0x00F4CB,	294,	"GeneratorAveragePowerFactor"	 },
	{	0x00F4CF,	295,	"GeneratorBusFrequency"		 },
	{	0x00F508,	296,	"CrankcaseAirPressureAbsolute"	 },
	{	0x00F509,	297,	"CrankcaseAirPressureGauge"	 },
	{	0x00F50E,	298,	"FilteredEngFuelPressAbsolute"	 },
	{	0x00F50F,	299,	"FilteredEngFuelPressureGauge"	 },
	{	0x00F511,	300,	"IntakeManifoldAirTemperture"	 },
	{	0x00F512,	301,	"ActualAirToFuelRation"		 },
	{	0x00F515,	302,	"PercentDroop"			 },
	{	0x00F517,	303,	"DesiredCombustionTime"		 },
	{	0x00F519,	304,	"AirToFuelDifferentialPressure"	 },
	{	0x00F51B,	305,	"ActualAirPressure"		 },
	{	0x00F51C,	306,	"DesIntakeManAirPress"		 },
	{	0x00F51D,	307,	"FuelTemperature"		 },
	{	0x00F51F,	308,	"RightAirRestriction"		 },
	{	0x00F520,	309,	"LeftAirRestriction"		 },
	{	0x00F524,	310,	"DesiredFullLoadExhaustOxygen"	 },
	{	0x00F525,	311,	"FuelConsumptionRate"		 },
	{	0x00F557,	312,	"BusToGeneratorPhaseDiff"	 },
	{	0x00F701,	313,	"CombustionProbe_1"		 },
	{	0x00F702,	314,	"CombustionProbe_2"		 },
	{	0x00F703,	315,	"CombustionProbe_3"		 },
	{	0x00F704,	316,	"CombustionProbe_4"		 },
	{	0x00F705,	317,	"CombustionProbe_5"		 },
	{	0x00F706,	318,	"CombustionProbe_6"		 },
	{	0x00F707,	319,	"CombustionProbe_7"		 },
	{	0x00F708,	320,	"CombustionProbe_8"		 },
	{	0x00F709,	321,	"CombustionProbe_9"		 },
	{	0x00F70A,	322,	"CombustionProbe_10"		 },
	{	0x00F70B,	323,	"CombustionProbe_11"		 },
	{	0x00F70C,	324,	"CombustionProbe_12"		 },
	{	0x00F70D,	325,	"CombustionProbe_13"		 },
	{	0x00F70E,	326,	"CombustionProbe_14"		 },
	{	0x00F70F,	327,	"CombustionProbe_15"		 },
	{	0x00F710,	328,	"CombustionProbe_16"		 },
	{	0x00F711,	329,	"EngineAverageCombProbe"	 },
	{	0x00F55A,	330,	"LeftEvenAverageCombTime"	 },
	{	0x00F55B,	331,	"RightOddAverageCombTime"	 },
	{	0x0000C8,	332,	"TotalFuelConsumed"		 },
	{	0x00F59C,	333,	"UnfilteredCombTime_1"		 },
	{	0x00F59D,	334,	"UnfilteredCombTime_2"		 },
	{	0x00F59E,	335,	"UnfilteredCombTime_3"		 },
	{	0x00F59F,	336,	"UnfilteredCombTime_4"		 },
	{	0x00F5A0,	337,	"UnfilteredCombTime_5"		 },
	{	0x00F5A1,	338,	"UnfilteredCombTime_6"		 },
	{	0x00F5A2,	339,	"UnfilteredCombTime_7"		 },
	{	0x00F5A3,	340,	"UnfilteredCombTime_8"		 },
	{	0x00F5A4,	341,	"UnfilteredCombTime_9"		 },
	{	0x00F5A5,	342,	"UnfilteredCombTime_10"		 },
	{	0x00F5A6,	343,	"UnfilteredCombTime_11"		 },
	{	0x00F5A7,	344,	"UnfilteredCombTime_12"		 },
	{	0x00F5A8,	345,	"UnfilteredCombTime_13"		 },
	{	0x00F5A9,	346,	"UnfilteredCombTime_14"		 },
	{	0x00F5AA,	347,	"UnfilteredCombTime_15"		 },
	{	0x00F5AB,	348,	"UnfilteredCombTime_16"		 },
	{	0x00F4A2,	349,	"HiCrankcasePressShutdnLevel"	 },
	{	0x00F4EA,	350,	"UnfiltEngOilPressGa"		 },
	{	0x00F513,	351,	"DesiredAirToFuelRatio3600"	 },
	{	0x00F57C,	352,	"SecondDesiredTiming"		 },
	{	0x00F57E,	353,	"DrivenEquipmentDelayTime"	 },
	{	0x00F5B1,	354,	"GasSpecificGravity"		 },
	{	0x00F5BA,	355,	"InletManifoldAirPressureAbsol"	 },
	{	0x00F61E,	356,	"ExtEnginePowerDeratePercent"	 },
	{	0x00FC2D,	357,	"TotalOperatingHours"		 },
	{	0xD00040,	358,	"IgnitionTiming_1"		 },
	{	0xD00041,	359,	"IgnitionTiming_2"		 },
	{	0xD00042,	360,	"IgnitionTiming_3"		 },
	{	0xD00043,	361,	"IgnitionTiming_4"		 },
	{	0xD00044,	362,	"IgnitionTiming_5"		 },
	{	0xD00045,	363,	"IgnitionTiming_6"		 },
	{	0xD00046,	364,	"IgnitionTiming_7"		 },
	{	0xD00047,	365,	"IgnitionTiming_8"		 },
	{	0xD00048,	366,	"IgnitionTiming_9"		 },
	{	0xD00049,	367,	"IgnitionTiming_10"		 },
	{	0xD0004A,	368,	"IgnitionTiming_11"		 },
	{	0xD0004B,	369,	"IgnitionTiming_12"		 },
	{	0xD0004C,	370,	"IgnitionTiming_13"		 },
	{	0xD0004D,	371,	"IgnitionTiming_14"		 },
	{	0xD0004E,	372,	"IgnitionTiming_15"		 },
	{	0xD0004F,	373,	"IgnitionTiming_16"		 },
	{	0xD00050,	374,	"IgnitionTiming_17"		 },
	{	0xD00051,	375,	"IgnitionTiming_18"		 },
	{	0xD00052,	376,	"IgnitionTiming_19"		 },
	{	0xD00053,	377,	"IgnitionTiming_20"		 },
	{	0xD00130,	378,	"EngineSpeedDropTime"		 },
	{	0xD00131,	379,	"EnginePrelubeTimeOutPeriod"	 },
	{	0xD004DB,	380,	"EngineStartChokePosition"	 },
	{	0xD004DC,	381,	"MaximumChokePosition3600"	 },
	{	0xD0010A,	382,	"FuelValveDifferentialPressure"	 },
	{	0xD00281,	383,	"FuelSpecificHeatRatio3500"	 },
	{	0xD00375,	384,	"ThrottleActuatorPositionComm"	 },
	{	0xD00377,	385,	"Engine2ThrottleActuatorTrim"	 },
	{	0xD00378,	386,	"DesInletManAirPressOffset"	 },
	{	0xD00379,	387,	"MasterEngDesExhaustO2"		 },
	{	0xD00418,	388,	"ChokeProportionalGainPercent"	 },
	{	0xD00419,	389,	"ChokeIntegralStabilityPercent"	 },
	{	0xD0041A,	390,	"ChokeDerivativeCompenPercent"	 },
	{	0xD00478,	391,	"GovProportionalGainPercent"	 },
	{	0xD00479,	392,	"GovIntegralStabilityPercent"	 },
	{	0xD0047A,	393,	"GovDerivativeCompenPercent"	 },
	{	0xD0047B,	394,	"GovAux1ProportionalGainPerc"	 },
	{	0xD0047C,	395,	"GovAux1IntegralStabilityPerc"	 },
	{	0xD0047D,	396,	"GovAux1DerivativeCompPerc"	 },
	{	0xD0047E,	397,	"GovAux2ProportionalGainPerc"	 },
	{	0xD0047F,	398,	"GovAux2IntegralStabilityPerc"	 },
	{	0xD00480,	399,	"GovAux2DerivativeCompPerc"	 },
	{	0xD00481,	400,	"WastegateProportionalGainPer"	 },
	{	0xD00482,	401,	"WastegateIntegralStabilityPerc" },
	{	0xD00483,	402,	"WastegateDerivativeCompPerc"	 },
	{	0x00F516,	403,	"ActualCombTime3600"		 },
	{	0x00FC88,	404,	"EngineOutputPower16CM34"	 },
	{	0xD005B1,	405,	"DesiredChargeDensityG3520"	 },
	{	0xD005B2,	406,	"ActualChargeDensityG3520"	 },
	{	0x00F636,	407,	"FuelBasedEngLoadFactor"	 },
	{	0xD00BB3,	408,	"FuelQualityMeasured"		 },
	{	0xD005B3,	409,	"DesEmissionsGainAdj",		 },
	{	0xD00371,	410,	"DeliveredFuelVol"		 },
	{	0xD0036F,	411,	"SmokeLimitFuelVol"		 },
	{	0xD00370,	412,	"TorqueLimitFuelVol"		 },
	{	0xD00D15,	413,	"Turbo1CompInletPAbs"		 },
	{	0xD00D16,	414,	"Turbo2CompInletPAbs"		 },
	{	0xD00D17,	415,	"Turbo3CompInletPAbs"		 },
	{	0xD00D18,	416,	"Turbo4CompInletPAbs"		 },
	{	0x00F5DB,	417,	"TurboInletTemp"		 },
	{	0xD00B18,	418,	"AveFuelRateBasedLoad"		 },
	{	0xD00BB7,	419,	"ExhGasMassFlowRate"		 },
	{	0xD00F5D,	420,	"AirFilterRestric_1"		 },
	{	0xD00F5E,	421,	"AirFilterRestric_2"		 },
	{	0xD00F5F,	422,	"AirFilterRestric_3"		 },
	{	0xD00F60,	423,	"AirFilterRestric_4"		 },
	{	0xD01075,	424,	"ActualEngNOxLevel"		 },
	{	0xD01076,	425,	"DesiredEngNOxLevel"		 },
	{	0x00F5BB,	426,	"IntakeManPressGage"		 },
	{	0x00F4D5,	427,	"EngVibrationVelocity"		 },
	{	0xD00D19,	428,	"FuelActuatorPosComm"		 },
	{	0xD00AF4,	429,	"AfterCoolPumpOutPAbs"		 },
	{	0xD00AF5,	430,	"EngCoolPumpOutPAbs"		 },
	{	0xD00AF6,	431,	"EngBlockCoolantOutP"		 },
	{	0xD004B5,	432,	"FuelRailPressure"		 },
	{	0xD00B01,	433,	"FuelRailTemp"			 },
	{	0x00F42C,	434,	"RearAfterCoolerTemp"		 },
	{	0x00F021,	435,	"FuelFilterStatus"		 },
	{	0x00F428,	436,	"TallTankFuelLevel"		 },
	{	0x00F02D,	437,	"EngOilLevelStatus"		 },
	{	0x00F032,	438,	"CoolantFlowStatus"		 },
	{	0x00F02B,	439,	"AftercoolLevStatus"		 },
	{	0x00F42A,	440,	"MachineSystemVolts"		 },
	{	0x00F023,	441,	"TCInOilFilterStatus"		 },
	{	0x00F024,	442,	"TCOutOilFilterStatus"		 },
	{	0x00F2FD,	443,	"TCScreenFilterStatus"		 },
	{	0x00F421,	444,	"TCOilOutletTemp"		 },
	{	0x00F42E,	445,	"DifferentialOilTemp"		 },
	{	0x00F5D6,	446,	"DesiredGear"			 },
	{	0x00F5D9,	447,	"ActualGear"			 },
	{	0x00F026,	448,	"TransChargeFiltStat"		 },
	{	0x00F025,	449,	"TransOilFilterStatus"		 },
	{	0x00F062,	450,	"LoSteerOilPressStat"		 },
	{	0x00F022,	451,	"HiSteerOilPressStat"		 },
	{	0x00F422,	452,	"BrakeAirPressGage"		 },
	{	0x00F0BA,	453,	"ParkBrakeSwitchPos"		 },
	{	0x00F066,	454,	"BrakeOilFilterStat"		 },
	{	0x00F029,	455,	"MstrCylOverstrokStat"		 },
	{	0x00F45D,	456,	"LeftFrontBrakeTemp"		 },
	{	0x00F45F,	457,	"LeftRearBrakeTemp"		 },
	{	0x00F45C,	458,	"RightFrontBrakeTemp"		 },
	{	0x00F45E,	459,	"RightRearBrakeTemp"		 },
	{	0x00F027,	460,	"HoistScreenFiltStat"		 },
	{	0x00F450,	461,	"LeftFrontSuspCylPres"		 },
	{	0x00F452,	462,	"LeftRearSuspCylPres"		 },
	{	0x00F451,	463,	"RtFrontSuspCylPres"		 },
	{	0x00F453,	464,	"RtRearSuspCylPres"		 },
	{	0x000018,	465,	"GroundSpeed"			 },
	{	0x00F5B8,	466,	"BodyPositionAngle"		 },
	{	0x00F477,	467,	"AmbientAirTemp"		 },
	{	0x00F4FE,	468,	"TruckPayloadTons"		 },
	{	0x00F22A,	469,	"FuelPresSensorStatus"		 },
	{	0x00F22B,	470,	"InAirTempSensStatus"		 },
	{	0x00F22C,	471,	"CoolLevSensorStatus"		 },
	{	0x00F30B,	472,	"ExhTempSensorStatus"		 },
	{	0x00F30C,	473,	"EngOilTempSensStatus"		 },
	{	0x00F312,	474,	"TranOilTempSenStatus"		 },
	{	0x00F313,	475,	"TranOilPresSenStatus"		 },
	{	0x00F2D4,	476,	"ThrottlePosSenStatus"		 },
	{	0x00F4A4,	477,	"EngVibrationLevel"		 },
	{	0x00F4D3,	478,	"JacketWaterTemp3600"		 },
	{	0x00F4D4,	479,	"JacketWaterPress3600"		 },
	{	0xD00F75,	480,	"ThrottlePosition"		 },
	{	0xD004B7,	481,	"DesiredFuelRailPressure"	 },
	{	0x000045,	482,	"HydraulicOilTemperature"	 },
	{	0x00F478,	483,	"TorqueConverterSpeed"		 },
	{	0x00F4B3,	484,	"TransIntermediateSpeed_1"	 },
	{	0x00F4B4,	485,	"TransIntermediateSpeed_2"	 },
	{	0x00F4B5,	486,	"TransOutputSpeed_1"		 },
	{	0x00F4B6,	487,	"TransOutputSpeed_2"		 },
	{	0xD00148,	488,	"TransClutchCurrent_1"		 },
	{	0xD00149,	489,	"TransClutchCurrent_2"		 },
	{	0xD0014A,	490,	"TransClutchCurrent_3"		 },
	{	0xD0014B,	491,	"TransClutchCurrent_4"		 },
	{	0xD0014C,	492,	"TransClutchCurrent_5"		 },
	{	0xD0014D,	493,	"TransClutchCurrent_6"		 },
	{	0xD0014E,	494,	"TransClutchCurrent_7"		 },
	{	0xD0014F,	495,	"TransClutchCurrent_8"		 },
	{	0xD00150,	496,	"TransClutchCurrent_9"		 },
	{	0xD00151,	497,	"TransClutchCurrent_10"		 },
	{	0xD001C7,	498,	"FuelLevelGaugePosition"	 },
	{	0x00F2DD,	499,	"ActionLampStatus"		 },
	{	0x00F2DE,	500,	"ActionAlarmStatus"		 },
	{	0x00F803,	501,	"SystemWarningStatus"		 },
	{	0xD100CF,	502,	"MonitSysOpModeSelSw"		 },
	{	0xD100CD,	503,	"ServiceSwitchStatus"		 },
	{	0xD100CE,	504,	"ClearSwitchStatus"		 },
	{	0xD100C5,	505,	"ECMInputLineStatus_0"		 },
	{	0xD100C6,	506,	"ECMInputLineStatus_1"		 },
	{	0xD100C7,	507,	"ECMInputLineStatus_2"		 },
	{	0xD100C8,	508,	"ECMInputLineStatus_3"		 },
	{	0xD100C9,	509,	"ECMInputLineStatus_4"		 },
	{	0xD100CA,	510,	"ECMInputLineStatus_5"		 },
	{	0x00F090,	511,	"TransSwitchPosition"		 },
	{	0x00F074,	512,	"DirectionSwitchPos"		 },
	{	0x00F00D,	513,	"ShiftLeverPosition"		 },
	{	0x00F154,	514,	"UpShiftSwitchPos"		 },
	{	0x00F153,	515,	"DnShiftSwitchPos"		 },
	{	0x00F212,	516,	"GearSelectorPos"		 },
	{	0x00F290,	517,	"MachControlModeStat"		 },
	{	0x00F1C4,	518,	"ServBrakeSwitchPos"		 },
	{	0xD10050,	519,	"ParkBrakeWarnIndSt"		 },
	{	0x00F2F6,	520,	"BrakeOilPressSwStat"		 },
	{	0x00F07C,	521,	"RideControlSwPos"		 },
	{	0x00F299,	522,	"RideControlConfig"		 },
	{	0x00F018,	523,	"NeutralSwitchPos"		 },
	{	0x00F1D1,	524,	"RideContrlSolenoidSt"		 },
	{	0x00F094,	525,	"StarterRelayStatus"		 },
	{	0x00F5DA,	526,	"RevSteerSolenoidSt"		 },
	{	0x00F605,	527,	"ServiceBrakeSwitch"		 },
	{	0xD0098D,	528,	"PrimSteerDisableOvrd"		 },
	{	0x00F2B3,	529,	"EngShutdnLampStatus"		 },
	{	0x00F031,	530,	"OperBackupThrottleSw"		 },
	{	0xD00189,	531,	"SecondThrottlePos"		 },
	{	0xD00188,	532,	"SecThrotPosSensDuty"		 },
	{	0x00F287,	533,	"RemControlPowerStat"		 },
	{	0xD10051,	534,	"EngOilPressWarnIndSt"		 },
	{	0xD1005D,	535,	"BrakeOilPressWarnInd"		 },
	{	0xD10060,	536,	"ChargeStsWarnIndSt"		 },
	{	0xD100AB,	537,	"EngMaintReqWarnIndSt",		 },
	{	0xD1004A,	538,	"AFRestrWarnIndStatus",		 },
	{	0xD100AA,	539,	"EngCollLevWarnIndSt",		 },
	{	0xD101FE,	540,	"ImpBrakApplWarnIndSt",		 },
	{	0xD1009A,	541,	"BrakOilTempWarnIndSt",		 },
	{	0x000017,	542,	"FuelLevelPercent",		 },
	{	0x000059,	543,	"TotalTattletale",		 },
	{	0x00F2CE,	544,	"NoOfCylindersG3600",		 },
	{	0x00F514,	545,	"HighIdleSpeed",		 },
	{	0xD00D14,	546,	"FuelQualityInUse",		 },
	{	0x00F4FF,	547,	"BucketPayload",		 },
	{	0x00F527,	548,	"CatalystTemperature",		 },
	{	0xD00EEC,	549,	"UreaFlowRate",			 },
	{	0x00F54F,	550,	"PowerTrainOilTemp",		 },
	{	0x00F427,	551,	"ShortFuelTankLevel",		 },
	{	0x00F603,	552,	"BodyRaiseStatus",		 },
	{	0x00F604,	553,	"BodyUpStatus",			 },
	{	0x00F4F7,	554,	"HoistLeverPosition",		 },
	{	0x00F5D8,	555,	"ShiftLeverPosition",		 },
	{	0x00FC29,	556,	"EngineDerateStatus2",		 },
	{	0xD016E9,	557,	"Eng1Throttle1Pos",		 },
	{	0xD016EA,	558,	"Eng1Throttle3Pos",		 },
	{	0xD016EB,	559,	"Eng2Throttle1Pos",		 },
	{	0xD016EC,	560,	"Eng2Throttle3Pos",		 },
	{	0xD016ED,	561,	"Throttle2Position",		 },
	{	0xD016EE,	562,	"Eng1Throt1DutyCycle",		 },
	{	0xD016EF,	563,	"Eng1Throt3DutyCycle",		 },
	{	0xD016F0,	564,	"Eng2Throt1DutyCycle",		 },
	{	0xD016F1,	565,	"Eng2Throt3DutyCycle",		 },
	{	0xD016F2,	566,	"Throttle2DutyCycle",		 },
	{	0xD016F3,	567,	"Eng1Throt1DesSpeed",		 },
	{	0xD016F4,	568,	"Eng1Throt3DesSpeed",		 },
	{	0xD016F5,	569,	"Eng2Throt1DesSpeed",		 },
	{	0xD016F6,	570,	"Eng2Throt3DesSpeed",		 },
	{	0xD016F7,	571,	"Throttle2DesSpeed",		 },
	{	0xD10D21,	572,	"Eng1CouplingFdbkStat",		 },
	{	0xD10D22,	573,	"Eng2CouplingFdbkStat",		 },
	{	0xD10D23,	574,	"EngCouplingModeSel",		 },
	{	0xD10D24,	575,	"Eng1DesThrotInStat",		 },
	{	0xD10D25,	576,	"Eng2DesThrotInStat",		 },
	{	0xD10D26,	577,	"Eng1ActThrotInStat",		 },
	{	0xD10D27,	578,	"Eng2ActThrotInStat",		 },
	{	0xD00D3B,	579,	"SpeedCtrlMaxSpdDiff",		 },
	{	0x00F7FA,	580,	"EngCoupMaxSpdDiff",		 },
	{	0x00F7FC,	581,	"EngDeCoupMaxSpdDiff",		 },
	{	0x00F0F1,	582,	"PrimaryECMHBStat",		 },
	{	0x00F413,	583,	"InjectionDuration",		 },
	{	0x00F540,	584,	"DesInjectionActuation",	 },
	{	0x00F541,	585,	"InjectorActuationPressure",	 },
	{	0x00F542,	586,	"InjectorActuationCurrent",	 },
	{	0x00FC21,	587,	"TotalEngIdleTime",		 },
	{	0x00FC22,	588,	"TotalEngIdleFuel",		 },
	{	0xD004B6,	589,	"FuelRailPressure2",		 },
	{	0x00F457,	590,	"DriveShaftSpeed",		 },
	{	0xD00D5A,	591,	"FrontDriveShaftSpeed",		 },
	{	0x00F58D,	592,	"LockupClutchPressure",		 },
	{	0xD100EC,	593,	"TransOilLevelStatus",		 },
	{	0xD003E0,	594,	"InjectionDurationCyl1",	 },
	{	0xD003E1,	595,	"InjectionDurationCyl2",	 },
	{	0xD003E2,	596,	"InjectionDurationCyl3",	 },
	{	0xD003E3,	597,	"InjectionDurationCyl4",	 },
	{	0xD003E4,	598,	"InjectionDurationCyl5",	 },
	{	0xD003E5,	599,	"InjectionDurationCyl6",	 },
	{	0xD003E6,	600,	"InjectionDurationCyl7",	 },
	{	0xD003E7,	601,	"InjectionDurationCyl8",	 },
	{	0xD003E8,	602,	"InjectionDurationCyl9",	 },
	{	0xD003E9,	603,	"InjectionDurationCyl10",	 },
	{	0xD003EA,	604,	"InjectionDurationCyl11",	 },
	{	0xD003EB,	605,	"InjectionDurationCyl12",	 },
	{	0xD003EC,	606,	"InjectionDurationCyl13",	 },
	{	0xD003ED,	607,	"InjectionDurationCyl14",	 },
	{	0xD003EE,	608,	"InjectionDurationCyl15",	 },
	{	0xD003EF,	609,	"InjectionDurationCyl16",	 },
	{	0xD003F0,	610,	"InjectionDurationCyl17",	 },
	{	0xD003F1,	611,	"InjectionDurationCyl18",	 },
	{	0xD003F2,	612,	"InjectionDurationCyl19",	 },
	{	0xD003F3,	613,	"InjectionDurationCyl20",	 },
	{	0xD003F4,	614,	"InjectionTimingCyl1",		 },
	{	0xD003F5,	615,	"InjectionTimingCyl2",		 },
	{	0xD003F6,	616,	"InjectionTimingCyl3",		 },
	{	0xD003F7,	617,	"InjectionTimingCyl4",		 },
	{	0xD003F8,	618,	"InjectionTimingCyl5",		 },
	{	0xD003F9,	619,	"InjectionTimingCyl6",		 },
	{	0xD003FA,	620,	"InjectionTimingCyl7",		 },
	{	0xD003FB,	621,	"InjectionTimingCyl8",		 },
	{	0xD003FC,	622,	"InjectionTimingCyl9",		 },
	{	0xD003FD,	623,	"InjectionTimingCyl10",		 },
	{	0xD003FE,	624,	"InjectionTimingCyl11",		 },
	{	0xD003FF,	625,	"InjectionTimingCyl12",		 },
	{	0xD00400,	626,	"InjectionTimingCyl13",		 },
	{	0xD00401,	627,	"InjectionTimingCyl14",		 },
	{	0xD00402,	628,	"InjectionTimingCyl15",		 },
	{	0xD00403,	629,	"InjectionTimingCyl16",		 },
	{	0xD00404,	630,	"InjectionTimingCyl17",		 },
	{	0xD00405,	631,	"InjectionTimingCyl18",		 },
	{	0xD00406,	632,	"InjectionTimingCyl19",		 },
	{	0xD00407,	633,	"InjectionTimingCyl20",		 },
	{	0xD01078,	634,	"DesiredEngNOxLvl",		 },
	{	0xD01258,	635,	"IntakeMassAirFlowRate",	 },
	{	0x00F4A5,	636,	"MassFuelFlowRate",		 },
	{	0xD01257,	637,	"TurboBypassValvePosition",	 },
	{	0x00F2D5,	638,	"PowerTakeOffMode",		 },
	{	0xD0050B,	639,	"NumberMisfiresCyl_1",		 },
	{	0xD0050C,	640,	"NumberMisfiresCyl_2",		 },
	{	0xD0050D,	641,	"NumberMisfiresCyl_3",		 },
	{	0xD0050E,	642,	"NumberMisfiresCyl_4",		 },
	{	0xD0050F,	643,	"NumberMisfiresCyl_5",		 },
	{	0xD00510,	644,	"NumberMisfiresCyl_6",		 },
	{	0xD00511,	645,	"NumberMisfiresCyl_7",		 },
	{	0xD00512,	646,	"NumberMisfiresCyl_8",		 },
	{	0xD00513,	647,	"NumberMisfiresCyl_9",		 },
	{	0xD00514,	648,	"NumberMisfiresCyl_10",		 },
	{	0xD00515,	649,	"NumberMisfiresCyl_11",		 },
	{	0xD00516,	650,	"NumberMisfiresCyl_12",		 },
	{	0xD00517,	651,	"NumberMisfiresCyl_13",		 },
	{	0xD00518,	652,	"NumberMisfiresCyl_14",		 },
	{	0xD00519,	653,	"NumberMisfiresCyl_15",		 },
	{	0xD0051A,	654,	"NumberMisfiresCyl_16",		 },
	{	0xD010F0,	655,	"NumberMisfiresCyl_17",		 },
	{	0xD010F1,	656,	"NumberMisfiresCyl_18",		 },
	{	0xD010F2,	657,	"NumberMisfiresCyl_19",		 },
	{	0xD010F3,	658,	"NumberMisfiresCyl_20",		 },
	{	0xD001A1,	659,	"ExhDifferTemp",		 },
	{	0x00F4E1,	660,	"DifferLubePress",		 },
	{	0x00F5C4,	661,	"BrakingCoolPumpSpeed",		 },
	{	0xD00B3D,	662,	"LeftRearWheelSpeed",		 },
	{	0xD00B3E,	663,	"RightRearWheelSpeed",		 },
	{	0xD009A1,	664,	"RightBrakeDifferTemp",		 },
	{	0xD0099E,	665,	"LeftBrakeDifferTemp",		 },
	{	0x00F456,	666,	"SteeringOilTemp",		 },
	{	0xD0099F,	667,	"FrontBrakeDifferTemp",		 },
	{	0xD009A0,	668,	"RearBrakeDifferTemp",		 },
	{	0xD01092,	669,	"ElectDriveCoolSpeed",		 },
	{	0xD009A2,	670,	"FrontStrutDifferPress",	 },
	{	0xD009A3,	671,	"RearStrutDifferPress",		 },
	{	0x00FC2E,	672,	"TotalDistanceTravelled",	 },
	{	0x00F58B,	673,	"MachinePitch",			 },
	{	0x00F2F7,	674,	"DifferFiltSwitchBypass",	 },
	{	0x00F2FA,	675,	"TransLockoutStatus",		 },
	{	0x00F002,	676,	"TransmissionGear",		 },
	{	0xD101EF,	677,	"TransChargeFiltBypass",	 },
	{	0x00F2FC,	678,	"MachineLockSwitch",		 },
	{	0x00F5D1,	679,	"ParkBrakeDrag",		 },
	{	0x00F1AD,	680,	"DifferOilLevelSwPos",		 },
	{	0x00F5B7,	681,	"BodyPositionDutyCycle",	 },
	{	0x00F2C6,	682,	"BucketPayloadStatus",		 },
	{	0xD00C81,	683,	"AutoLubeWarnStatus",		 },
	{	0xD10018,	684,	"AutoLubeSolStatus",		 },
	{	0x00F4F8,	685,	"DesiredHoistStatus",		 },
	{	0x00F231,	686,	"RetarderStatus",		 },
	{	0x00F257,	687,	"EngRetarderStatus",		 },
	{	0xD10230,	688,	"RetarderMode",			 },
	{	0xD00259,	689,	"LeftFrontStrutPressExt",	 },
	{	0xD00261,	690,	"LeftRearStrutPressExt",	 },
	{	0xD00260,	691,	"RightFrontStrutPressExt",	 },
	{	0xD00262,	692,	"RightRearStrutPressExt",	 },
	{	0x00FC32,	693,	"TotalDistanceTravForward",	 },
	{	0x00FC33,	694,	"TotalDistanceTravReverse",	 },
	{	0x00F602,	695,	"ParkBrakeStatus",		 },
	{	0x00F191,	696,	"TruckPayloadState",		 },
	{	0xD006BB,	697,	"WeightSpeedAvgDuration",	 },
	{	0xD00FCA,	698,	"FrontTireWeightSpeed",		 },
	{	0xD00FCB,	699,	"RearTireWeightSpeed",		 },
	{	0xD005F5,	700,	"TurboBypassValvePosCmd",	 },
	{	0xD01192,	701,	"ThrottleValveDiffPress",	 },
	{	0xD01255,	702,	"DesThrottleValveDiffPress",	 },
	{	0xD01079,	703,	"ExhNOxLvlSnsrCalibSlope",	 },
	{	0xD0109E,	704,	"ExhNOxLvlSnsrPwrCmd",		 },
	{	0xD10986,	705,	"ExhNOxLvlSnsrPwrStat",		 },
	{	0xD0109F,	706,	"ExhNOxLvlSnsrHtrCmd",		 },
	{	0xD10987,	707,	"ExhNOxLvlSnsrHtrStat",		 },
	{	0xD00D61,	708,	"EmmissionsFeedbackOverride",	 },
	{	0xD01209,	709,	"ActAftTreatInOxySnsrVolt",	 },
	{	0xD0120A,	710,	"ActAftTreatOutOxySnsrVolt",	 },
	{	0xD0120B,	711,    "DesAftTreatInOxySnsrVolt",	 },
	{	0xD0120C,	712,    "DesAftTreatOutOxySnsrVolt",	 },
	{	0xD0120D,	713,    "AftTreatOutOxySnsrVoltOffset",	 },
	{	0xD0120E,	714,    "AftTreatInOxySnsrFuelTrim",	 },
	{	0xD0120F,	715,    "AftTreatOutOxySnsrFuelTrim",	 },
	{	0xD01124,	716,    "AftTreat1ConvEff",		 },
	{	0xD01125,	717,    "AftTreat2ConvEff",		 },
	{	0xD0112C,	718,    "AftTreat1ReagConduct",		 },
	{	0xD0112D,	719,    "AftTreat2ReagConduct",		 },
	{	0xD0113A,	720,    "AftTreat1ExhGasDiffPress",	 },
	{	0xD0113B,	721,    "AftTreat2ExhGasDiffPress",	 },
	{	0xD0113C,	722,    "AftTreat1InGasTemp",		 },
	{	0xD0113D,	723,    "AftTreat2InGasTemp",		 },
	{	0xD0113E,	724,    "AftTreat1ReagConcent",		 },
	{	0xD0113F,	725,    "AftTreat2ReagConcent",		 },
	{	0xD01140,	726,    "AftTreat1OutGasTemp",		 },
	{	0xD01141,	727,    "AftTreat2OutGasTemp",		 },
	{	0xD0114A,	728,    "AftTreat1ReagPumpSpeed",	 },
	{	0xD0114B,	729,    "AftTreat2ReagPumpSpeed",	 },
	{	0xD0114C,	730,    "AftTreat1ReagValvePos",	 },
	{	0xD0114D,	731,    "AftTreat2ReagValvePos",	 },
	{	0xD01152,	732,    "AftTreat1ReagHtrValveCmd_T1",	 },
	{	0xD01154,	733,    "AftTreat2ReagHtrValveCmd_T1",	 },
	{	0xD01153,	734,    "AftTreat1ReagHtrValveCmd_T2",	 },
	{	0xD01155,	735,    "AftTreat2ReagHtrValveCmd_T2",	 },
	{	0xD0115A,	736,    "AftTreat1ReagLevel_T1",	 },
	{	0xD0115C,	737,    "AftTreat2ReagLevel_T1",	 },
	{	0xD0115B,	738,    "AftTreat1ReagLevel_T2",	 },
	{	0xD0115D,	739,    "AftTreat2ReagLevel_T2",	 },
	{	0xD0115E,	740,    "AftTreat1ReagTemp_T1",		 },
	{	0xD01160,	741,    "AftTreat2ReagTemp_T1",		 },
	{	0xD0115F,	742,    "AftTreat1ReagTemp_T2",		 },
	{	0xD01161,	743,    "AftTreat2ReagTemp_T2",		 },
	{	0xD01164,	744,    "AftTreat1ReagPumpCmdPos",	 },
	{	0xD01165,	745,    "AftTreat2ReagPumpCmdPos",	 },
	{	0x00000E,	746,	"LegacyOilPressure",		 },
	{	0x00000F,	747,	"LegacyBoostPressure",		 },
	{	0x00F001,	748,	"MarineThrottleSyncStatus",	 },
	{	0x00F402,	749,	"CMS_LampStatus",		 },
	{	0xD00E9D,	750,	"AcceleratorPedalPosition",	 },
	{	0x00F577,	751,	"LeftHydraulicCaseDrainTemp",	 },
	{	0x00F578,	752,	"CenterHydraulicCaseDrainTemp",	 },
	{	0x00F579,	753,	"RightHydraulicCaseDrainTemp",	 },
	{	0x00F46F,	754,	"BrakeOilTemp",			 },
	{	0x00F425,	755,	"GearboxOilPressure",		 },
	{	0x00F426,	756,	"GearboxOilTemp",		 },
	{	0x00F00E,	757,	"VehicleIdentificationCode",	 },
	{	0x00F455,	758,	"SecondaryBrakeAirPress",	 },
	{	0x00F4B2,	759,	"TransmissionInputSpeed",	 },
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link
//

// Instantiator

ICommsDriver * Create_CatDataLinkDriver1(void)
{
	return New CCatDataLinkDriver(catStandard);
	}

ICommsDriver * Create_CatDataLinkDriver2(void)
{
	return New CCatDataLinkDriver(catExpansion);
	}

ICommsDriver * Create_CatDataLinkDriver3(void)
{
	return New CCatDataLinkDriver(catVersion2);
	}

ICommsDriver * Create_CatDataLinkTester(void)
{
	return New CCatDataLinkDriver(catTester);
	}

ICommsDriver * Create_CatDataLinkDriver4(void)
{
	return New CCatDataLinkDriver(catRaw);
	}

// Constructor

CCatDataLinkDriver::CCatDataLinkDriver(UINT uDriver)
{
	m_uDriver       = uDriver;

	m_wID		= FindID();

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Caterpillar";
	
	m_DriverName	= FindFullName();
	
	m_Version	= (uDriver == catVersion2) ? "2.00" : "1.00";
	
	m_ShortName	= FindShortName();

	m_DevRoot	= "CDL";

	m_fSingle	= (uDriver != catRaw) ? TRUE : FALSE;

	BuildMaps();
	}

// Driver Data

UINT CCatDataLinkDriver::GetFlags(void)
{
	return CBasicCommsDriver::GetFlags() | dflagRemotable;
	}

// Binding Control

UINT CCatDataLinkDriver::GetBinding(void)
{
	switch( m_uDriver ) {

		case catStandard:  return bindRawSerial;
		case catExpansion: return bindCatLink;
		case catTester:    return bindCatLink;
		case catVersion2:  return bindCatLink;
		case catRaw:	   return bindCatLink;
		}

	AfxAssert(FALSE);

	return 0;
	}

void CCatDataLinkDriver::GetBindInfo(CBindInfo &Info)
{
	if( m_uDriver == catStandard ) {

		CBindSerial &Serial = (CBindSerial &) Info;

		Serial.m_Physical = physicalRS485;
		Serial.m_BaudRate = 62500;
		Serial.m_DataBits = 8;
		Serial.m_StopBits = 1;
		Serial.m_Parity   = parityNone;
		Serial.m_Mode     = modeTwoWire;
		}
	}

// Configuration

CLASS CCatDataLinkDriver::GetDriverConfig(void)
{
	switch( m_uDriver ) {

		case catStandard:
		
		case catExpansion:
		
		case catTester:

			return AfxRuntimeClass(CCatDataLinkDriverOptions);

		case catVersion2:
		
		case catRaw:

			return AfxRuntimeClass(CCatDataLinkDriverOptions2);

		}

	return NULL;
	}

CLASS CCatDataLinkDriver::GetDeviceConfig(void)
{
	switch( m_uDriver ) {

		case catRaw:

			return AfxRuntimeClass(CCatDataLinkDeviceOptions);
		}
	
	return NULL;
	}

// Address Management

BOOL CCatDataLinkDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	if( IsFault(Text) ) {

		for( UINT n = 0; n < elements(m_pFaults); n++ ) {

			UINT l = strlen(m_pFaults[n]);

			if( Text.Left(l) == m_pFaults[n] ) {

				Addr.a.m_Type	= addrLongAsLong;

				Addr.a.m_Table	= n + 1;
				
				Addr.a.m_Extra	= 0;

				Addr.a.m_Offset	= tatoi(Text.Mid(l));

				return TRUE;
				}
			}

		Error.Set("Invalid fault code property.", 0);

		return FALSE;
		}

	if( Text.Left(3) == "MID" ) {

		Addr.a.m_Type	= addrLongAsLong;

		Addr.a.m_Table	= 100;
		
		Addr.a.m_Extra	= 0;

		Addr.a.m_Offset	= BYTE(tstrtoul(Text.Mid(3), NULL, 16));

		return TRUE;
		}

	if( Text.Left(3) == "SER" ) {

		Addr.a.m_Type	= addrByteAsByte;

		Addr.a.m_Table	= 101;
		
		Addr.a.m_Extra	= 0;

		Addr.a.m_Offset	= BYTE(tstrtoul(Text.Mid(3), NULL, 10));

		return TRUE;
		}

	if( Text.Left(4) == "POLL" ) {

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = 102;

		Addr.a.m_Extra	= 0;

		Addr.a.m_Offset = BYTE(tstrtoul(Text.Mid(4), NULL, 16));

		return TRUE;
		}

	if( Text.Left(10) == "Report_PID" ) {

		Addr.a.m_Type   = addrLongAsLong;
			
		Addr.a.m_Table  = 103; 

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = WORD(tstrtoul(Text.Mid(6), NULL, 10));

		return TRUE;
		}

	if( Text.Left(10) == "Report_SRC" ) {

		Addr.a.m_Type   = addrByteAsByte;

		Addr.a.m_Table  = 104;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = WORD(tstrtoul(Text.Mid(6), NULL, 10));

		return TRUE;
		}

	if( Text.Left(10) == "Report_VAL" ) {

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = 105;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = WORD(tstrtoul(Text.Mid(6), NULL, 10));

		return TRUE;
		}

	UINT uBase  = addrNamed;

	UINT uColon = Text.Find(':');

	UINT  uPID  = NOTHING;

	if( uColon < NOTHING ) {

		if( Text[uColon+1] == 'S' ) {

			uBase = addrNamed + 3;
			}

		if( Text[uColon+1] == 'E' ) {

			uBase = addrNamed + 6;
			}

		Text = Text.Left(uColon);
		}

	if( isdigit(Text[0]) ) {

		UINT  uMonico = tatoi(Text);

		INDEX Find    = m_MapMonico.FindName(uMonico);

		if( m_MapMonico.Failed(Find) ) {

			UINT uRest = Text.FindNot(TEXT("0123456789_"));

			Text       = Text.Mid(uRest);
			}
		else {
			UINT n = m_MapMonico.GetData(Find);

			uPID   = m_PID[n].m_uPID;
			}
		}

	if( uPID == NOTHING ) {

		INDEX Find = m_MapName.FindName(Text);

		if( m_MapName.Failed(Find) ) {

			UINT i;

			for( i = 0; Text[i]; i++ ) {

				if( !isxdigit(Text[i]) ) {

					break;
					}
				}

			if( !Text[i] ) {
			
				uPID = tstrtoul(Text, 0, 16);
				}
			}
		else {
			UINT n = m_MapName.GetData(Find);

			uPID   = m_PID[n].m_uPID;
			}
		}

	if( uPID < NOTHING ) {

		if( HIWORD(uPID) == 0x00 ) {

			Addr.a.m_Type	= addrLongAsLong;

			Addr.a.m_Table	= uBase + 0;
			
			Addr.a.m_Extra	= 0;

			Addr.a.m_Offset	= LOWORD(uPID);

			return TRUE;
			}

		if( HIWORD(uPID) == 0xD0 ) {

			Addr.a.m_Type	= addrLongAsLong;

			Addr.a.m_Table	= uBase + 1;
			
			Addr.a.m_Extra	= 0;

			Addr.a.m_Offset	= LOWORD(uPID);

			return TRUE;
			}

		if( HIWORD(uPID) == 0xD1 ) {

			Addr.a.m_Type	= addrLongAsLong;

			Addr.a.m_Table	= uBase + 2;
			
			Addr.a.m_Extra	= 0;

			Addr.a.m_Offset	= LOWORD(uPID);

			return TRUE;
			}

		Error.Set("Internal Error.", 0);

		return FALSE;
		}

	Error.Set("Invalid PID name or identifier.", 0);

	return FALSE;
	}

BOOL CCatDataLinkDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		if( Addr.a.m_Table >= 1 && Addr.a.m_Table <= 57 ) {

			UINT n = Addr.a.m_Table - 1;

			Text.Printf( "%s%2.2u",
				     CString(m_pFaults[n]),
				     Addr.a.m_Offset
				     );

			return TRUE;
			}

		if( Addr.a.m_Table == 100 ) {

			Text.Printf("MID%2.2X", Addr.a.m_Offset);

			return TRUE;
			}

		if( Addr.a.m_Table == 101 ) {

			Text.Printf("SER%2.2u", Addr.a.m_Offset);

			return TRUE;
			}

		if( Addr.a.m_Table == 102 ) {

			Text.Printf("POLL%2.2X", Addr.a.m_Offset);
		
			return TRUE;
			}

		if( Addr.a.m_Table == 103 ) {

			Text.Printf("Report_PID%3.3u", Addr.a.m_Offset);

			return TRUE;
			}

		if( Addr.a.m_Table == 104 ) {

			Text.Printf("Report_SRC%3.3u", Addr.a.m_Offset);

			return TRUE;
			}

		if( Addr.a.m_Table == 105 ) {

			Text.Printf("Report_VAL%3.3u", Addr.a.m_Offset);

			return TRUE;
			}

		if( Addr.a.m_Table >= addrNamed ) {

			UINT  uPID  = NOTHING;

			PCSTR pType = "";

			switch( (Addr.a.m_Table - addrNamed) % 3 ) {
				
				case 0: uPID = MAKELONG(Addr.a.m_Offset, 0x00); break;
				case 1: uPID = MAKELONG(Addr.a.m_Offset, 0xD0); break;
				case 2: uPID = MAKELONG(Addr.a.m_Offset, 0xD1); break;

				}

			switch( (Addr.a.m_Table - addrNamed) / 3 ) {
				
				case 0: pType = "";  break;
				case 1: pType = "S"; break;
				case 2: pType = "E"; break;

				}

			if( uPID < NOTHING ) {

				INDEX Find = m_MapPID.FindName(uPID);

				if( m_MapPID.Failed(Find) ) {

					if( HIWORD(uPID) ) {

						Text.Printf("%6.6X", uPID);
						}
					else
						Text.Printf("%4.4X", uPID);
					}
				else {
					UINT n = m_MapPID.GetData(Find);

					Text.Printf("%3.3u_", m_PID[n].m_uMonico);

					Text += m_PID[n].m_pName;
					}

				if( *pType ) {

					Text += ':';

					Text += pType;
					}

				return TRUE;
				}
			}

		Text = "???";
		}

	return TRUE;
	}

BOOL CCatDataLinkDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CCatLinkDialog Dlg(ThisObject, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CCatDataLinkDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	UINT uCount = elements(m_PID);

	UINT uPitch = 50;

	if( !pRoot ) {

		UINT uGroups = (uCount + uPitch - 1) / uPitch;

		if( uItem < uGroups ) {

			UINT uMin = uPitch * uItem + 1;

			UINT uMax = min(uMin + uPitch  - 1, uCount);

			Data.m_Name.Printf("Monico IDs %3.3u - %3.3u", uMin, uMax);

			Data.m_Addr.a.m_Type  = addrLongAsLong;
			
			Data.m_Addr.a.m_Table = addrNamed + uItem;

			Data.m_uData = MAKELONG(uItem, 0);
			
			Data.m_fPart = TRUE;

			Data.m_fRead = TRUE;

			return TRUE;
			}

		if( uItem - uGroups < elements(m_pFaults) ) {

			Data.m_Name = m_pFaults[uItem - uGroups];

			Data.m_Addr.a.m_Type   = addrLongAsLong;
			
			Data.m_Addr.a.m_Table  = uItem - uGroups + 1;

			Data.m_Addr.a.m_Extra  = 0;

			Data.m_Addr.a.m_Offset = 0;

			Data.m_uData = MAKELONG(uItem, 1);
			
			Data.m_fPart = TRUE;

			Data.m_fRead = TRUE;

			return TRUE;
			}

		if( uItem - uGroups == elements(m_pFaults) + 0 ) {

			Data.m_Name = "MID Status";

			Data.m_Addr.a.m_Type   = addrLongAsLong;
			
			Data.m_Addr.a.m_Table  = 100;

			Data.m_Addr.a.m_Extra  = 0;

			Data.m_Addr.a.m_Offset = 0;

			Data.m_uData = MAKELONG(uItem, 1);
			
			Data.m_fPart = TRUE;

			Data.m_fRead = TRUE;

			return TRUE;
			}

		if( uItem - uGroups == elements(m_pFaults) + 1 ) {

			Data.m_Name = "Serial Number";

			Data.m_Addr.a.m_Type   = addrByteAsByte;
			
			Data.m_Addr.a.m_Table  = 101;

			Data.m_Addr.a.m_Extra  = 0;

			Data.m_Addr.a.m_Offset = 0;

			Data.m_uData = MAKELONG(uItem, 1);
			
			Data.m_fPart = TRUE;

			Data.m_fRead = TRUE;

			return TRUE;
			}

		return FALSE;
		}
	else {
		if( HIWORD(pRoot->m_uData) == 0 ) {

			UINT uIndex = uItem + pRoot->m_uData * uPitch;

			if( uIndex < uCount && uItem < uPitch ) {

				UINT uPID  = m_PID[uIndex].m_uPID;

				UINT uBase = addrNamed;

				Data.m_Name.Printf("%3.3u_", m_PID[uIndex].m_uMonico);

				Data.m_Name  = Data.m_Name + m_PID[uIndex].m_pName;

				Data.m_uData = MAKELONG(uItem, 1);
				
				Data.m_fPart = FALSE;

				Data.m_fRead = !IsWritable(uPID);

				if( HIWORD(uPID) == 0x00 ) {

					Data.m_Addr.a.m_Type	= addrLongAsLong;

					Data.m_Addr.a.m_Table	= uBase + 0;
					
					Data.m_Addr.a.m_Extra	= 0;

					Data.m_Addr.a.m_Offset	= LOWORD(uPID);

					return TRUE;
					}

				if( HIWORD(uPID) == 0xD0 ) {

					Data.m_Addr.a.m_Type	= addrLongAsLong;

					Data.m_Addr.a.m_Table	= uBase + 1;
					
					Data.m_Addr.a.m_Extra	= 0;

					Data.m_Addr.a.m_Offset	= LOWORD(uPID);

					return TRUE;
					}

				if( HIWORD(uPID) == 0xD1 ) {

					Data.m_Addr.a.m_Type	= addrLongAsLong;

					Data.m_Addr.a.m_Table	= uBase + 2;
					
					Data.m_Addr.a.m_Extra	= 0;

					Data.m_Addr.a.m_Offset	= LOWORD(uPID);

					return TRUE;
					}

				return TRUE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CCatDataLinkDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	if( m_uDriver == catTester ) {

		return FALSE;
		}

	if( Addr.m_Ref ) {

		if( Addr.a.m_Table >= addrNamed ) {

			if( (Addr.a.m_Table - addrNamed) / 3 == 1 ) {

				return FALSE;
				}

			if( (Addr.a.m_Table - addrNamed) / 3 == 2 ) {

				return FALSE;
				}

			if( (Addr.a.m_Table - addrNamed) / 3 == 0 ) {

				UINT uPID  = NOTHING;

				switch( (Addr.a.m_Table - addrNamed) % 3 ) {
					
					case 0: uPID = MAKELONG(Addr.a.m_Offset, 0x00); break;
					case 1: uPID = MAKELONG(Addr.a.m_Offset, 0xD0); break;
					case 2: uPID = MAKELONG(Addr.a.m_Offset, 0xD1); break;

					}

				if( IsWritable(uPID) ) {

					return FALSE;
					}
				}
			}
		}

	return TRUE;
	}

BOOL CCatDataLinkDriver::DoListNewFaults(void)
{
	switch( m_uDriver ) {

		case catVersion2:
		case catRaw:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatDataLinkDriver::DoListPoll(void)
{	
	switch( m_uDriver ) {

		case catVersion2:
		case catRaw:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatDataLinkDriver::DoListReport(void)
{
	switch( m_uDriver ) {

		case catVersion2:
	
			return TRUE;
		}

	return FALSE;
	}

// Implementation

WORD CCatDataLinkDriver::FindID(void)
{
	switch( m_uDriver ) {

		case catStandard:  return 0x4039;
		case catExpansion: return 0x403D;
		case catTester:    return 0x4041;
		case catVersion2:  return 0x404F;
		case catRaw:	   return 0x407E;
		}

	AfxAssert(FALSE);

	return 0;
	}

CString CCatDataLinkDriver::FindShortName(void)
{
	switch( m_uDriver ) {

		case catStandard:  return "CDL Monitor 1.0";
		case catExpansion: return "CDL Monitor 1.0";
		case catTester:    return "CDL Tester";
		case catVersion2:  return "CDL Monitor 2.0";
		case catRaw:	   return "CDL Manual 1.0";
		}

	AfxAssert(FALSE);

	return "";
	}

CString CCatDataLinkDriver::FindFullName(void)
{
	switch( m_uDriver ) {

		case catStandard:  return "Data Link Monitor 1.0";
		case catExpansion: return "Data Link Monitor 1.0";
		case catTester:    return "Data Link Tester";
		case catVersion2:  return "Data Link Monitor 2.0";
		case catRaw:	   return "Data Link Manual Monitor 1.0";
		}

	AfxAssert(FALSE);

	return "";
	}

void CCatDataLinkDriver::BuildMaps(void)
{
	for( UINT n = 0; n < elements(m_PID); n++ ) {

		m_MapName  .Insert(m_PID[n].m_pName,   n);

		m_MapPID   .Insert(m_PID[n].m_uPID,    n);

		m_MapMonico.Insert(m_PID[n].m_uMonico, n);
		}
	}

BOOL CCatDataLinkDriver::IsWritable(UINT uPID)
{
	switch( uPID ) {

		case 0x000D:	// 7
		case 0x0047:	// 74
		case 0xF0B0:	// 22
		case 0xF0B1:	// 23
		case 0xF0B2:    // 24
		case 0xF0C2:	// 30
		case 0xF11C:	// 47
		case 0xF11D:	// 48
		case 0xF125:	// 53
		case 0xF213:	// 63
		case 0xF44D:	// 91
		case 0xF51A:    // 120
		case 0xF524:	// 310
		case 0xF57C:	// 352
		case 0xF5B1:	// 354
		case 0xD00281:	// 383
		case 0xD100A0:	// 206
		case 0xD10167:  // 209
		case 0xD00478:  // 391
		case 0xD00479:	// 392
		case 0xD0047A:	// 393
		case 0xD005B3:	// 409
		case 0xD01078:  // 634
		case 0x00FC2E:	// 672
		case 0x00FC32:	// 693
		case 0x00FC33:	// 694
		case 0xD006BB:	// 697
		case 0xD01079:  // 703
		case 0xD0109E:	// 704
		case 0xD0109F:	// 706

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatDataLinkDriver::IsFault(CString Text)
{
	if( Text.Left(5) == "Fault"	|| 
	    Text.Left(6) == "Active"	|| 
	    Text.Left(8) == "InActive" ) {

		    return TRUE;
		}

	for( UINT u = 0; u < elements(m_pFaults); u++ ) {

		CString Fault = m_pFaults[u];

		if( Text.Left(9) == Fault.Left(9) ) {

			return TRUE;
			}
		}	

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCatLinkDialog, CStdDialog);
		
// Constructor

CCatLinkDialog::CCatLinkDialog(CCatDataLinkDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;
	
	SetName(TEXT("CatDataLinkDialog"));
	}

// Message Map

AfxMessageMap(CCatLinkDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify (2001, LBN_SELCHANGE, OnSelChange)
	AfxDispatchNotify (1001, LBN_DBLCLK,    OnDblClk   )

	AfxMessageEnd(CCatLinkDialog)
	};

// Message Handlers

BOOL CCatLinkDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadPIDs();

	LoadCategories();

	DoEnables();

	return FALSE;
	}

// Notification Handlers

void CCatLinkDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	DoEnables();
	}

void CCatLinkDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	OnOkay(IDOK);
	}

// Command Handlers

BOOL CCatLinkDialog::OnOkay(UINT uID)
{
	CListBox &Cat = (CListBox &) GetDlgItem(2001);

	UINT     uCat = Cat.GetCurSelData();

	if( uCat < 999 ) {

		if( uCat < 10 ) {

			CListBox &List = (CListBox &) GetDlgItem(1001);

			UINT     uPID  = List.GetCurSelData();

			UINT     uBase = addrNamed + 3 * uCat;

			if( HIWORD(uPID) == 0x00 ) {

				m_pAddr->a.m_Type   = addrLongAsLong;

				m_pAddr->a.m_Table  = uBase + 0;
				
				m_pAddr->a.m_Extra  = 0;

				m_pAddr->a.m_Offset = LOWORD(uPID);
				}

			if( HIWORD(uPID) == 0xD0 ) {

				m_pAddr->a.m_Type   = addrLongAsLong;

				m_pAddr->a.m_Table  = uBase + 1;
				
				m_pAddr->a.m_Extra  = 0;

				m_pAddr->a.m_Offset = LOWORD(uPID);
				}

			if( HIWORD(uPID) == 0xD1 ) {

				m_pAddr->a.m_Type   = addrLongAsLong;

				m_pAddr->a.m_Table  = uBase + 2;
				
				m_pAddr->a.m_Extra  = 0;

				m_pAddr->a.m_Offset = LOWORD(uPID);
				}
			}
		else {
			UINT uElement = 0;

			if( uCat < 100 ) {

				uElement = tatoi(GetDlgItem(3001).GetWindowText());

				if( uElement >= 50 ) {

					Error("Invalid element number.");

					SetDlgFocus(3001);

					return TRUE;
					}
				
				m_pAddr->a.m_Type = addrLongAsLong;
				}

			if( uCat == 110 || uCat == 112 ) {

				uElement = tstrtoul(GetDlgItem(3001).GetWindowText(), NULL, 16);

				if( uElement >= 256 ) {

					Error("Invalid MID.");

					SetDlgFocus(3001);

					return TRUE;

					}

				m_pAddr->a.m_Type = addrLongAsLong;
				}

			if( uCat == 111 ) {

				uElement = tstrtoul(GetDlgItem(3001).GetWindowText(), NULL, 10);

				if( uElement >= 16 ) {

					Error("Invalid Serial Number Index.");

					SetDlgFocus(3001);

					return TRUE;
					}

				m_pAddr->a.m_Type = addrByteAsByte;
				}

			if( uCat >= 113 && uCat <= 115  ) {

				uElement = tstrtoul(GetDlgItem(3001).GetWindowText(), NULL, 10);

				if( uElement >= 360 ) {

					Error("Invalid Report Index.");

					SetDlgFocus(3001);

					return TRUE;
					}

				m_pAddr->a.m_Type = addrLongAsLong;
				}

			m_pAddr->a.m_Table  = uCat - 10;
			
			m_pAddr->a.m_Extra  = 0;

			m_pAddr->a.m_Offset = uElement;
			}
		}
	else
		m_pAddr->m_Ref = 0;
		
	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CCatLinkDialog::LoadPIDs(void)
{
	CListBox &List = (CListBox &) GetDlgItem(1001);

	List.SetRedraw(FALSE);
	
	List.SetTabStops(CArray <int> (10));

	for( UINT n = 0; n < elements(m_PID); n++ ) {

		CPrintf Text( "%3.3u\t%s",
			      m_PID[n].m_uMonico,
			      CString(m_PID[n].m_pName)
			      );

		List.AddString(Text, m_PID[n].m_uPID);
		}

	UINT uPID = 1;

	if( m_pAddr->a.m_Table >= addrNamed ) {

		if( m_fPart ) {

			UINT uPitch = 50;

			UINT uIndex = (m_pAddr->a.m_Table - addrNamed) * uPitch;

			uPID        = m_PID[uIndex].m_uPID;
			}
		else {
			switch( (m_pAddr->a.m_Table - addrNamed) % 3 ) {
				
				case 0: uPID = MAKELONG(m_pAddr->a.m_Offset, 0x00); break;
				case 1: uPID = MAKELONG(m_pAddr->a.m_Offset, 0xD0); break;
				case 2: uPID = MAKELONG(m_pAddr->a.m_Offset, 0xD1); break;
				}
			}
		}

	if( !List.SelectData(DWORD(uPID)) ) {

		if( HIWORD(uPID) ) {

			List.InsertString(0, CPrintf("???\tCustom PID %6.6X", uPID), uPID);
			}
		else
			List.InsertString(0, CPrintf("???\tCustom PID %4.4X", uPID), uPID);

		List.SelectData(DWORD(uPID));
		}

	List.SetRedraw(TRUE);

	List.Invalidate(FALSE);
	}

void CCatLinkDialog::LoadCategories(void)
{
	CListBox  &List = (CListBox  &) GetDlgItem(2001);

	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(3001);

	List.AddString("No Selection", 999);

	List.AddString("PID Data Value",  0);

	List.AddString("PID Status Word", 1);
	
	List.AddString("PID Enable Flag", 2);

	List.AddString("Fault Source", 11);

	List.AddString("Fault Code", 12);

	List.AddString("Fault Subcode", 13);

	List.AddString("Fault Flags", 14);

	List.AddString("Fault Count", 15);

	List.AddString("Fault First", 16);

	List.AddString("Fault Last", 17);

	if( m_pDriver->DoListNewFaults() ) {

		List.AddString("Active Fault Source", 21);

		List.AddString("Active Fault Code", 22);

		List.AddString("Active Fault Subcode", 23);

		List.AddString("Active Fault Diag/Event", 24);

		List.AddString("Active Fault Count", 25);

		List.AddString("Active Fault First", 26);

		List.AddString("Active Fault Last", 27);

		List.AddString("InActive Fault Source", 31);

		List.AddString("InActive Fault Code", 32);

		List.AddString("InActive Fault Subcode", 33);

		List.AddString("InActive Fault Diag/Event", 34);

		List.AddString("InActive Fault Count", 35);

		List.AddString("InActive Fault First", 36);

		List.AddString("InActive Fault Last", 37);

		List.AddString("Active Events Fault Source", 41);

		List.AddString("Active Events Fault Code", 42);

		List.AddString("Active Events Fault Subcode", 43);

		List.AddString("Active Events Fault Diag/Event", 44);

		List.AddString("Active Events Fault Count", 45);

		List.AddString("Active Events Fault First", 46);

		List.AddString("Active Events Fault Last", 47);

		List.AddString("Active Diags Fault Source", 51);

		List.AddString("Active Diags Fault Code", 52);

		List.AddString("Active Diags Fault Subcode", 53);

		List.AddString("Active Diags Fault Diag/Event", 54);

		List.AddString("Active Diags Fault Count", 55);

		List.AddString("Active Diags Fault First", 56);

		List.AddString("Active Diags Fault Last", 57);
		}

	List.AddString("MID Status", 110);

	List.AddString("Serial Number", 111);

	if( m_pDriver->DoListPoll() ) {

		List.AddString("MID Poll Counter", 112);
		}

	if( m_pDriver->DoListReport() ) {

		List.AddString("Report PID List", 113);

		List.AddString("Report PID Source", 114);

		List.AddString("Report PID Value", 115);
		}

	if( m_pAddr->m_Ref ) {

		if( m_pAddr->a.m_Table >= 0 && m_pAddr->a.m_Table <= 57 ) {

			List.SelectData(10 + m_pAddr->a.m_Table);

			Edit.SetWindowText(CPrintf("%u", m_pAddr->a.m_Offset));

			SetDlgFocus(Edit);

			return;
			}

		if( m_pAddr->a.m_Table == 100 || m_pAddr->a.m_Table == 102 ) {

			List.SelectData(10 + m_pAddr->a.m_Table);

			Edit.SetWindowText(CPrintf("%2.2X", m_pAddr->a.m_Offset));

			SetDlgFocus(Edit);

			return;
			}

		if( m_pAddr->a.m_Table == 101 ) {

			List.SelectData(10 + m_pAddr->a.m_Table);

			Edit.SetWindowText(CPrintf("%2.2u", m_pAddr->a.m_Offset));

			SetDlgFocus(Edit);

			return;
			}

		if( m_pAddr->a.m_Table >= 103 && m_pAddr->a.m_Table <= 105 ) {

			List.SelectData(10 + m_pAddr->a.m_Table);

			Edit.SetWindowText(CPrintf("%3.3u", m_pAddr->a.m_Offset));

			SetDlgFocus(Edit);

			return;
			}

		if( m_pAddr->a.m_Table >= addrNamed ) {

			if( !m_fPart ) {

				List.SelectData((m_pAddr->a.m_Table - addrNamed) / 3);
				}
			else
				List.SelectData(0);

			Edit.SetWindowText("0");

			SetDlgFocus(1001);

			return;
			}
		}

	List.SelectData(999);

	Edit.SetWindowText("0");

	SetDlgFocus(List);
	}

void CCatLinkDialog::DoEnables(void)
{
	CListBox &Cat = (CListBox &) GetDlgItem(2001);

	UINT     uCat = Cat.GetCurSelData();

	GetDlgItem(1001).EnableWindow(uCat < 10);
	
	GetDlgItem(3001).EnableWindow(uCat > 10 && uCat < 999);
	}

// End of File
