
#include "intern.hpp"

#include "ezm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// EZ Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEZMasterDeviceOptions, CUIItem);       

// Constructor

CEZMasterDeviceOptions::CEZMasterDeviceOptions(void)
{
	m_Group = 1;

	m_Unit   = 1;
	}

// Download Support

BOOL CEZMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Group));
	Init.AddWord(WORD(m_Unit));
	
	return TRUE;
	}

// Meta Data Creation

void CEZMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	}

//////////////////////////////////////////////////////////////////////////
//
// EZ Master Driver
//

// Instantiator

ICommsDriver * Create_EZMasterDriver(void)
{
	return New CEZMasterDriver;
	}

// Constructor

CEZMasterDriver::CEZMasterDriver(void)
{
	m_wID		= 0x4015;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "EZ Automation";
	
	m_DriverName	= "EZ Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "EZ Master";

	AddSpaces();
	}

// Destructor

CEZMasterDriver::~CEZMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CEZMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEZMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEZMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEZMasterDeviceOptions);
	}

// Implementation

void CEZMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"I",	"I Discretes",	10, 1, 65535, addrBitAsBit));
	AddSpace(New CSpace(2,	"O",	"O Discretes",	10, 1, 65535, addrBitAsBit));
	AddSpace(New CSpace(3,	"S",	"S Discretes",	10, 1, 65535, addrBitAsBit));
	AddSpace(New CSpace(4,	"IR",	"IR Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(5,	"OR",	"OR Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(6,	"R",	"R Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal)); 
	AddSpace(New CSpace(7,	"SD",	"SD Discretes",	10, 1, 65535, addrBitAsBit));
	AddSpace(New CSpace(8,	"SR",	"SR Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(18, "XR",	"XR Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(19, "#R",	"#R Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(20, "XS",	"XS Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(21, "#S",	"#S Registers",	10, 1, 65535, addrWordAsWord, addrWordAsReal)); 

	}

// End of File