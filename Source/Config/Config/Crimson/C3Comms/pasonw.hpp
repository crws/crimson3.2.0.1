//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PASONWITS_HPP
	
#define	INCLUDE_PASONWITS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Pason WITS Driver
//

class CPasonWITSDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CPasonWITSDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Helpers

	protected:
		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
