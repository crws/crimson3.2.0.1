
#include "intern.hpp"

#include "adam.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CAdam4000DeviceOptions, CUIItem);
				   
// Constructor

CAdam4000DeviceOptions::CAdam4000DeviceOptions(void)
{
	m_Drop = 1;

	m_Device = 0;

	m_fChecksum = TRUE;
	}

// UI Managament

void CAdam4000DeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CAdam4000DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_fChecksum));

	Init.AddByte(BYTE(m_Device));
	
	return TRUE;
	}

// Meta Data Creation

void CAdam4000DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddBoolean(Checksum);

	Meta_AddInteger(Device);
	}



//////////////////////////////////////////////////////////////////////////
//
// Adam 4017-18 Input Module Driver
//

// Instantiator

ICommsDriver * Create_Adam4017Driver(void)
{
	return New CAdam4017Driver;
	}

// Constructor

CAdam4017Driver::CAdam4017Driver(void)
{
	m_wID		= 0x3337;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Adam";
	
	m_DriverName	= "4000 Series Modules";
	
	m_Version	= "1.01";
	
	m_ShortName	= "4000 Series";

	AddSpaces();
	}

// Binding Control

UINT CAdam4017Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CAdam4017Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}


// Configuration

CLASS CAdam4017Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CAdam4000DeviceOptions);
	}

// Address Management

BOOL CAdam4017Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CAdam4000AddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Implementation

void CAdam4017Driver::AddSpaces(void)
{
	AddSpace(New CSpace("Config",	"Configuration",		SPACE_CONFIG));
	AddSpace(New CSpace("ConSt",	"Configuration Status",		SPACE_CONSTAT));
	AddSpace(New CSpace("Ver",	"Read Firmware Version",	SPACE_VERSION));
	AddSpace(New CSpace("Name",	"Read Module Name",		SPACE_NAME));
	AddSpace(New CSpace("Span",	"Span Calibration",		SPACE_SPAN));
	AddSpace(New CSpace("Offset",	"Offset Calibration",		SPACE_OFFSET));
	AddSpace(New CSpace("ChanSt",	"Channel Status",		SPACE_CHANSTAT));
	AddSpace(New CSpace("Ch0",	"Channel 0",			SPACE_CH0,		addrRealAsReal));
	AddSpace(New CSpace("Ch1",	"Channel 1",			SPACE_CH1,		addrRealAsReal));
	AddSpace(New CSpace("Ch2",	"Channel 2",			SPACE_CH2,		addrRealAsReal));
	AddSpace(New CSpace("Ch3",	"Channel 3",			SPACE_CH3,		addrRealAsReal));
	AddSpace(New CSpace("Ch4",	"Channel 4",			SPACE_CH4,		addrRealAsReal));
	AddSpace(New CSpace("Ch5",	"Channel 5",			SPACE_CH5,		addrRealAsReal));
	AddSpace(New CSpace("Ch6",	"Channel 6",			SPACE_CH6,		addrRealAsReal));
	AddSpace(New CSpace("Ch7",	"Channel 7",			SPACE_CH7,		addrRealAsReal));
	AddSpace(New CSpace("DigIn",	"Digital Data In",		SPACE_DIGIN));
	AddSpace(New CSpace("SynSamp",	"Synchronized Sampling",	SPACE_SYNSAMP));
	AddSpace(New CSpace("ReadSyn",	"Read Synchronized Data",	SPACE_READSYN));
	AddSpace(New CSpace("RstSt",	"Reset Status",			SPACE_RSTSTAT));
	AddSpace(New CSpace("DigOut",	"Digital Data Out",		SPACE_DIGOUT));
	AddSpace(New CSpace("Ch0Val",	"Channel 0 Value",		SPACE_CH0L));
	AddSpace(New CSpace("Ch1Val",	"Channel 1 Value",		SPACE_CH1L));
		
	}


//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Module Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CAdam4000AddrDialog, CStdAddrDialog);
		
// Constructor

CAdam4000AddrDialog::CAdam4000AddrDialog(CAdam4017Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_uDevice = pConfig->GetDataAccess("Device")->ReadInteger(pConfig);
	
	}

// Overridables

BOOL CAdam4000AddrDialog::AllowSpace(CSpace *pSpace)
{
	// TODO:  Model should not be dependent on order of UI list!
	
	switch( pSpace->m_uMinimum ) {

		case SPACE_CONFIG:
		case SPACE_CONSTAT:
		case SPACE_VERSION:
		case SPACE_NAME:
		
		
			return TRUE;

		case SPACE_SPAN:
		case SPACE_OFFSET:
		case SPACE_CHANSTAT:
		case SPACE_CH0:
		case SPACE_CH1:
		case SPACE_CH2:
		case SPACE_CH3:
		case SPACE_CH4:
		case SPACE_CH5:
		case SPACE_CH6:
		case SPACE_CH7:

			switch( m_uDevice ) {

				case MODEL_4017:
				case MODEL_4018:
					return TRUE;
				}

			return FALSE;


		case SPACE_DIGIN:
		case SPACE_RSTSTAT:
		case SPACE_SYNSAMP:
		case SPACE_READSYN:

			switch( m_uDevice ) {

				case MODEL_4050:
				case MODEL_4051:
				case MODEL_4053:
				case MODEL_4060:

					return TRUE;
				}

			return FALSE;

		case SPACE_DIGOUT:

			switch( m_uDevice ) {

				case MODEL_4050:
				case MODEL_4060:
					return TRUE;
				}
			
			return FALSE;

		case SPACE_CH0L:
		case SPACE_CH1L:

			switch( m_uDevice ) {

				case MODEL_4080:
							
					return TRUE;
				}

			return FALSE;
		}
		
	return FALSE;
	}

// End of File
