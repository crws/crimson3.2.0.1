
#include "intern.hpp"

#include "comlim.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// COMLI Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CABBCOMLIDeviceOptions, CUIItem);

// Constructor

CABBCOMLIDeviceOptions::CABBCOMLIDeviceOptions(void)
{
	m_Drop       = 1;
	}

// UI Management

void CABBCOMLIDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CABBCOMLIDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CABBCOMLIDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
       	}

//////////////////////////////////////////////////////////////////////////
//
// COMLI Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CABBCOMLIDriverOptions, CUIItem);

// Constructor

CABBCOMLIDriverOptions::CABBCOMLIDriverOptions(void)
{
	m_Enhanced       = 1;
	}

// Download Support

BOOL CABBCOMLIDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Enhanced));

	return TRUE;
	}

// Meta Data Creation

void CABBCOMLIDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Enhanced);
       	}

//////////////////////////////////////////////////////////////////////////
//
// ABB COMLI Master
//

// Instantiator

ICommsDriver *	Create_ABBCOMLIMasterDriver(void)
{
	return New CABBCOMLIMasterDriver;
	}

// Constructor

CABBCOMLIMasterDriver::CABBCOMLIMasterDriver(void)
{
	m_wID		= 0x331E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "ABB";
	
	m_DriverName	= "COMLI Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "ABB COMLI Master";

	AddSpaces();
	}

// Binding Control

UINT CABBCOMLIMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CABBCOMLIMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CABBCOMLIMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CABBCOMLIDriverOptions);
	}

CLASS CABBCOMLIMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABBCOMLIDeviceOptions);
	}

// Address Helpers

BOOL CABBCOMLIMasterDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		if( pSpace->m_uTable == 1 ) {

			UINT u = tstrtoul(Text, NULL, 8);

			if( u < pSpace->m_uMinimum || u > pSpace->m_uMaximum ) {

				Error.Set("[100] - [1000]", 0);

				return FALSE;
				}

			if( u % 4 ) {

				Error.Set("[nnn0] / [nnn4]", 0);

				return FALSE;
				}
			}

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	return FALSE;
	}

// Implementation

void CABBCOMLIMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "A", "Analogue Inputs",				 8, 0100,  01000, addrWordAsWord));
	AddSpace(New CSpace(2, "R", "Registers",				10,    0,  65535, addrWordAsWord));
	AddSpace(New CSpace(3, "Y", "I/O - 8 Bit Bytes (0nnnn0 - 0nnnn7)",	 8,    0,  03777, addrByteAsByte));
	AddSpace(New CSpace(4, "B", "I/O - 1 Bit (Octal)",			 8,    0, 037777, addrBitAsBit));
	AddSpace(New CSpace(5, "F", "Floating Point",				10,    0,    255, addrRealAsReal));
	AddSpace(New CSpace(6, "T", "Timers",					10,    0,    511, addrLongAsLong));
	AddSpace(New CSpace(7, "DR","Date/Time Y(=DR0),M,D,h,m,s(=DR5)",	10,    0,      5, addrByteAsByte));
	AddSpace(New CSpace(8, "DW","Write DW0-DW5 when DW6 != 0",		10,    0,      6, addrByteAsByte));
	AddSpace(New CSpace(9, "E", "Time Marked Events (E0=Queue Status)",	10,    0,     54, addrWordAsWord));
	AddSpace(New CSpace(10,"G", "Get Time Marked Events (Set G != 0)",	10,    0,      0, addrBitAsBit));
	}

// End of File
