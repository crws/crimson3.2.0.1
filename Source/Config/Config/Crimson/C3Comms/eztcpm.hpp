#include "ezm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EZTCPM_HPP
	
#define	INCLUDE_EZTCPM_HPP 

//////////////////////////////////////////////////////////////////////////
//
// EZ TCP/IP Master Driver Options
//

class CEZMasterTCPDeviceOptions : public CEZMasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEZMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Group;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// EZ TCP/IP Master Driver
//

class CEZMasterTCPDriver : public CEZMasterDriver
{
	public:
		// Constructor
		CEZMasterTCPDriver(void);

		//Destructor
		~CEZMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

#endif

// End of File