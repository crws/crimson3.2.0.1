
#include "intern.hpp"

#include "hlink.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader(); 

//////////////////////////////////////////////////////////////////////////
//
// Hardy Instruments Hardy Link Device Options
//
//

// Dynamic Class

AfxImplementDynamicClass(CHardyLinkDeviceOptions, CUIItem);

// Constructor

CHardyLinkDeviceOptions::CHardyLinkDeviceOptions(void)
{
	m_ID	= 10;

	m_Mode  = 0;

	m_Check = 0;
	}

// Download Support

BOOL CHardyLinkDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_ID));

	Init.AddByte(BYTE(m_Mode));

	Init.AddByte(BYTE(m_Check));

	return TRUE;
	}

// Meta Data Creation

void CHardyLinkDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(ID);

	Meta_AddInteger(Mode);

	Meta_AddInteger(Check);
	}

//////////////////////////////////////////////////////////////////////////
//
// Hardy Instruments Hardy Link Driver
//
//

// Instantiator

ICommsDriver *	Create_HardyLinkSerialMasterDriver(void)
{
	return New CHardyLinkDriver;
	}


// Constructor

CHardyLinkDriver::CHardyLinkDriver(void)
{
	m_wID		= 0x4064;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Hardy Instruments";
	
	m_DriverName	= "Hardy Link";
	
	m_Version	= "1.00";
	
	m_ShortName	= "HardyLink";

	AddSpaces();
	}

// Binding Control

UINT CHardyLinkDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CHardyLinkDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}


// Configuration

CLASS CHardyLinkDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CHardyLinkDeviceOptions);
	}


// Implementation

void CHardyLinkDriver::AddSpaces(void)     
{
	AddSpace(New CSpace(addrNamed,	"A",	"Accumulated Total",	10,  1,  1, addrRealAsReal));
	AddSpace(New CSpace(addrNamed,	"G",	"Gross",		10,  2,  2, addrRealAsReal));
	AddSpace(New CSpace(addrNamed,	"N",	"Net",			10,  3,  3, addrRealAsReal));
	AddSpace(New CSpace(addrNamed,	"T",	"Tare",			10,  4,  4, addrRealAsReal));
	AddSpace(New CSpace(        5,	"DE",	"Deadband",		10,  1,  8, addrRealAsReal));
	AddSpace(New CSpace(        6,  "P",	"Preact",		10,  1,  8, addrRealAsReal));
	AddSpace(New CSpace(	    7,  "S",	"Setpoint",		10,  1,  8, addrRealAsReal));
	AddSpace(New CSpace(addrNamed,  "DI",	"Dipswitch",		10,  8,  8, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "L",	"LED Status",		10,  9,  9, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "REL",	"Relay",		10, 10, 10, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "REM",  "Remote",		10, 11, 11, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,	"C",	"ROC",			10, 12, 12, addrLongAsLong));
	AddSpace(New CSpace(addrNamed,  "E",	"Excitation Monitor",	10, 13, 13, addrLongAsLong));
	AddSpace(New CSpace(addrNamed,  "I",	"Load Cell Input",	10, 14, 14, addrRealAsReal));
	AddSpace(New CSpace(addrNamed,  "IREM",	"Rem. Level Status",	10, 15, 15, addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "M",	"Net/Gross Toggle",	10, 16, 16, addrBitAsBit));
	AddSpace(New CSpace(addrNamed,  "U",    "Display Units Toggle", 10, 17, 17, addrBitAsBit));
	AddSpace(New CSpace(addrNamed,  "H",    "Hold/Unhold Toggle",	10, 18, 18, addrBitAsBit));
	AddSpace(New CSpace(addrNamed,  "AT",	"Auto Tare",		10, 19, 19, addrBitAsBit));
	AddSpace(New CSpace(addrNamed,  "AZ",	"Auto Zero",		10, 20, 20, addrBitAsBit));
	AddSpace(New CSpace(addrNamed,  "NTOT",	"Add Net to Acc Total", 10, 21, 21, addrBitAsBit));
	AddSpace(New CSpace(addrNamed,  "ERR",	"Last Error",		10, 22, 22, addrLongAsLong));
	}


// End of File
