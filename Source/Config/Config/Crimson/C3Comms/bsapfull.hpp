
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BSAPFULL_HPP
	
#define	INCLUDE_BSAPFULL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBSAPFULLUDPDriverOptions;
class CBSAPFULLUDPSDriverOptions;

class CBSAPFULLTagsUDPDeviceOptions;
class CBSAPFULLUDPDeviceOptions;

class CBSAPFULLTagsUDPSDeviceOptions;
class CBSAPFULLUDPSDeviceOptions;

class CBSAPFULLTagsUDPDriver;
class CBSAPFULLUDPDriver;

class CBSAPFULLTagsUDPSDriver;
class CBSAPFULLUDPSDriver;

class CBSAPFULLSerialDriverOptions;
class CBSAPFULLSerialSDriverOptions;

class CBSAPFULLTagsSerialDeviceOptions;
class CBSAPFULLTagsSerialSDeviceOptions;

class CBSAPFULLSerialDeviceOptions;
class CBSAPFULLSerialSDeviceOptions;

class CBSAPFULLTagsSerialDriver;
class CBSAPFULLTagsSerialSDriver;

class CBSAPFULLSerialDriver;
class CBSAPFULLSerialSDriver;

class CBSAPFULLTagsDialog;
class CBSAPFULLTagsUDPDialog;

class CBSAPFULLTagsSerialDialog;

//////////////////////////////////////////////////////////////////////////
//
// Address Decoders
//

#define Index(r)	((r).a.m_Offset)

//////////////////////////////////////////////////////////////////////////
//
// Tag Data
//

struct CTagDataE {
	
	CString	m_Name;
	DWORD	m_Addr;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Decoders
//

#define Addr(a)		((CAddress &) (a).m_Addr)

//////////////////////////////////////////////////////////////////////////
//
// Space Definitions
//
enum {				// table numbers
	SPBIT0		= 10,
	SPBIT1		= 11,
	SPBIT2		= 12,
	SPBIT3		= 13,
	SPBIT4		= 14,
	SPBIT5		= 15,
	SPBIT6		= 16,
	SPREAL0		= 20,
	SPREAL1		= 21,
	SPREAL2		= 22,
	SPREAL3		= 23,
	SPREAL4		= 24,
	SPREAL5		= 25,
	SPREAL6		= 26,
	SPSTRING0	= 30,
	SPSTRING1	= 31,
	SPSTRING2	= 32,
	SPSTRING3	= 33,
	SPSTRING4	= 34,
	SPSTRING5	= 35,
	SPSTRING6	= 36,
	SPBITINT	= 40,
	SPREALINT	= 50,
	SPSTRINGINT	= 60,
	SPBOOLACK	= 70,
	SPREALACK	= 71,
	SPBYTEACK	= 72,
	SPWORDACK	= 73,
	SPDWORDACK	= 74,
	SPNRT0		= 90,
	SPNRT1		= 91,
	SPNRT2		= 92,
	SPNRT3		= 93,
	SPNRT4		= 94,
	SPNRT5		= 95,
	SPNRT6		= 96,
	SPBYTE0		= 110,
	SPBYTE1		= 111,
	SPBYTE2		= 112,
	SPBYTE3		= 113,
	SPBYTE4		= 114,
	SPBYTE5		= 115,
	SPBYTE6		= 116,
	SPWORD0		= 120,
	SPWORD1		= 121,
	SPWORD2		= 122,
	SPWORD3		= 123,
	SPWORD4		= 124,
	SPWORD5		= 125,
	SPWORD6		= 126,
	SPDWORD0	= 130,
	SPDWORD1	= 131,
	SPDWORD2	= 132,
	SPDWORD3	= 133,
	SPDWORD4	= 134,
	SPDWORD5	= 135,
	SPDWORD6	= 136,
	SPBYTEINT	= 140,
	SPWORDINT	= 150,
	SPDWORDINT	= 160,
	};

enum {				// Runtime data types
	BTYPEBIT	= 0,
	BTYPEREAL	= 2,
	BTYPEBYTE	= 3,
	BTYPEWORD	= 4,
	BTYPELONG	= 5,
	BTYPESTRING	= 6,
	};

// type and inhibits bit definition positions
enum {
	TIALMI	= 0x0004,
	TIDYNM	= 0x0008,
	TIMANL	= 0x0010,
	TICTRL	= 0x0020,
	TIALME	= 0x0040,
	};

#define	MAXSTRNLEN	80
#define MAXDWORDLEN	80 / sizeof(DWORD)
#define	STRSELMASK	0x80000000
#define	STREXTFLAG	0x2

#define	STRHDR	"Bristol Babcock Variable Names for "
#define	STRUSED	"This name or function is already in use"

//////////////////////////////////////////////////////////////////////////
//
// Utility Templates
//

inline int AfxCompare(CTagDataE const &d1, CTagDataE const &d2)
{
	if( Index(Addr(d1)) > Index(Addr(d2)) )	return +1;

	if( Index(Addr(d1)) < Index(Addr(d2)) )	return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Collections
//

typedef CArray <CTagDataE> CTagDataEArray;

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Master Driver Options
//

class CBSAPFULLUDPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLUDPDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Local;
		UINT m_Global;
		UINT m_PollH;
		UINT m_Level;
		UINT m_Level1;
		UINT m_Level2;
		UINT m_Level3;
		UINT m_Level4;
		UINT m_Level5;
		UINT m_Level6;
		UINT m_IPAddr;
		UINT m_Socket;
		UINT m_Count;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
		BOOL CheckLevelCounts(void);
		void UpdateLevels(CUIViewWnd *pWnd);
		BOOL CheckBitCount(UINT uCurrent, UINT *uNew);
		BOOL CheckClearLower(UINT uThis);
		void ClearLowerLevels(UINT uThis);
		BYTE MakeLevelMask(BYTE bLevel);
	};

//////////////////////////////////////////////////////////////////////////
//
// BSAP Full Serial Master Driver Options
//

class CBSAPFULLSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLSerialDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Local;
		UINT m_Global;
		UINT m_PollH;
		UINT m_Level;
		UINT m_Level1;
		UINT m_Level2;
		UINT m_Level3;
		UINT m_Level4;
		UINT m_Level5;
		UINT m_Level6;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
		BOOL CheckLevelCounts(void);
		void UpdateLevels(CUIViewWnd *pWnd);
		BOOL CheckBitCount(UINT uCurrent, UINT *uNew);
		BOOL CheckClearLower(UINT uThis);
		void ClearLowerLevels(UINT uThis);
		BYTE MakeLevelMask(BYTE bLevel);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags Device Options
//

#define	LICPOS	256	// list item actual count position
#define	LIDPOS	257	// list item config count position

class CBSAPFULLTagsDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLTagsDeviceOptions(void);

		// Attributes
		CString GetDeviceName(void);

		// Operations
		BOOL	CreateTag (CError &Error, CString const &Name, CAddress Addr, BOOL fNew, BOOL fUpdate);
		void	DeleteTag(CAddress const &Addr, BOOL fKill);
		BOOL	RenameTag(CString const &Name, CAddress const &Addr);
		CString	CreateName(CAddress Addr, CString PostFix);
		void	ListTags(CTagDataEArray &List);
		BOOL	AccessArrayValue(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);

		virtual	BOOL SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		CViewWnd * GetViewWnd(void);

		// Persistance
		void	Load(CTreeFile &Tree);
		void	Save(CTreeFile &Tree);
		void    PostLoad(void);

		// Download Support
		void    PrepareData(void);
		BOOL	MakeInitData(CInitData &Init);

		// Meta Data Creation
		void	AddMetaData(void);

		// NRT Tags
		void	CreateNRTTags(void);

	public:
		// Public Data
		DWORD	m_ArraySelect;
		DWORD	m_ArrayValue;
		CString	m_ArrayString;

	protected:

		// Type Definitions
		// Tag Creation
		typedef	CMap   <CString, DWORD> CTagsFwdMap;
		typedef	CMap   <DWORD, CString> CTagsRevMap;
		typedef	CArray <DWORD>          CAddrArray;

		// Runtime
		typedef	CArray	<CString>	CNameArray;	// Parameter Names
		typedef	CArray	<BYTE>		CTableArray;	// Parameter Table Number
		typedef	CArray	<WORD>		COffsetArray;	// Addr.a.m_Offset for item
		typedef	CArray	<WORD>		CMSDArray;	// MSD Address for runtime
		typedef	CArray	<WORD>		CLNumArray;	// List Number
		typedef	CArray	<BYTE>		CTypeArray;	// Type
		typedef	CArray	<BYTE>		CDevArray;	// Device Address
		typedef	CArray	<BYTE>		CLevArray;	// Device Level
		typedef	CArray	<WORD>		CGlobArray;	// Global Address of tag's device
		typedef	CArray	<WORD>		CStatusArray;	// 1st array item
		typedef CArray	<DWORD>		CLNArr;		// Line Number Sort

		CNameArray	NameList;
		CTableArray	TableList;
		COffsetArray	OffsetList;
		CMSDArray	MSDList;
		CLNumArray	LNumList;
		CTypeArray	TypeList;
		CDevArray	DevList;
		CLevArray	LevList;
		CGlobArray	GlobList;
		CStatusArray	AStatusList;
		CLNArr		LNSort;

		UINT	m_uItem;
		UINT    m_uStrIndex;
		CString	m_Name;
		BYTE	m_Table;
		WORD	m_Offset;
		WORD	m_Index;
		WORD	m_MSD;
		WORD	m_LNum;	// High byte is list number, Low byte is List position
		BYTE	m_Type;
		BYTE	m_Dev;
		BYTE	m_Lev;
		BYTE	m_bLocalAddress;
		WORD	m_Global;
		WORD	m_ArrayStatus;
		BYTE	m_MLevel;
		BYTE	m_Path;
		BOOL	m_fIsMaster;
		WORD	m_DevGlobal;
		WORD	DevNumList[15];	// list of device numbers @ Level used
		void	*m_pDriver;
		CViewWnd * m_pViewWnd;

		struct IMPORT_INDICES {

			WORD	wBit[7];
			WORD    wByte[7];
			WORD	wWord[7];
			WORD	wDword[7];
			WORD	wReal[7];
			WORD	wString[7];
			WORD	wBitInt;
			WORD	wByteInt;
			WORD	wWordInt;
			WORD	wDwordInt;
			WORD	wRealInt;
			WORD	wStringInt;
			WORD	wBoolAck;
			WORD	wByteAck;
			WORD	wWordAck;
			WORD	wDwordAck;
			WORD	wRealAck;
			};

		IMPORT_INDICES	m_ImportInx;

		// Tag Map
		CTagsFwdMap	m_TagsFwdMap;
		CTagsRevMap	m_TagsRevMap;
		CAddrArray	m_AddrArray;
		CAddrArray	m_PrepAddr;
		CStringArray	m_Tags;

		// Data Members
		CArray <UINT>	m_Errors;
		CTagDataEArray	m_List;
		CArray <DWORD>	m_ListIndexes;
		CArray <BYTE>	m_ListNumbers;
		CArray <PBYTE>	m_ListItemPtrs;

		// Create Tag Helpers
		BOOL	IsBITTable(UINT uTable);
		BOOL	IsBYTETable(UINT uTable);
		BOOL	IsWORDTable(UINT uTable);
		BOOL	IsDWORDTable(UINT uTable);
		BOOL	IsREALTable(UINT uTable);
		BOOL	IsSTRINGTable(UINT uTable);
		BOOL	IsNRTTable(UINT uTable);
		BOOL	IsNRTName(CString sName);
		BOOL	IsACK(UINT uTable);

		// Download Support Protected
		DWORD	MakeMREF(UINT uSelect);
		BYTE	MakeRuntimeType(UINT uSelect);
		void	SortBBTags(UINT uCount);
		UINT	CountLNumItems(UINT uCount);
		BOOL	CheckListNum(UINT uLNum);
		void	SortLNums(void);
		UINT	CheckListItem(UINT uLNum, UINT uItem);
		WORD	CountNumOfArrays(UINT uCount);

		// Persistance Help
		void	LoadArrays(CTreeFile &Tree);
		void	SaveArrays(CTreeFile &Tree);

		// Property Save Filter
		BOOL	SaveProp(CString Tag) const;

		CString	MakeNRTTagName(UINT uSuffix);
		void	SetNRTData(CString sName, DWORD dRef);

		// Device Config Info Save
		void	AppendToLists(DWORD dVal);
		void	InsertListItem(DWORD dVal);
		void	DeleteListItem(DWORD dVal);
		void	RebuildMaps(UINT uSkip);
		BOOL	IsValidName(CString sName);

		void	ASetName(CString sName);
		void	ASetMSD(DWORD dNew);
		void	ASetType(DWORD dNew);
		void	ASetLNum(DWORD dNew);
		void	ASetOffs(DWORD dNew);
		void	ASetLevel(DWORD dNew);
		void	ASetDevAdd(DWORD dNew);
		void	ASetGlobal(DWORD dNew);
		void	ASetArrayStatus(DWORD dNew);
		void	ASetMLevel(DWORD dNew);
		void	ASetPath(DWORD dNew);
		void	ASetTable(DWORD dNew);
		DWORD	ASetDevNum(DWORD dItem);

		UINT	CheckLineNumbers(UINT uLNum);

		DWORD	AGetArrayCount(void);
		CString	AGetName(UINT uItem);
		DWORD	AGetMSD (UINT uItem);
		DWORD	AGetType(UINT uItem);
		DWORD	AGetLNum(UINT uItem);
		DWORD	AGetOffs(UINT uItem);
		DWORD	AGetLevel(UINT uItem);
		DWORD	AGetDevAdd(UINT uItem);
		DWORD	AGetGlobal(UINT uItem);
		DWORD	AGetArrayStatus(UINT uItem);
		DWORD	AGetMLevel(UINT uItem);
		DWORD	AGetPath(UINT uItem);
		DWORD	AGetTable(UINT uItem);
		DWORD	AFindName(void);
		UINT	AFindIndex(DWORD dSel);
		DWORD	AFindUnUsedMSD(void);
		BOOL	ANoMSDMatch(DWORD dData);
		DWORD	DeletedItemCheck(void);

		// Import/Export
		void	OnImport(CWnd *pWnd);
		void	OnExport(CWnd *pWnd);

		// Make Tags
		CString CreateName(PCTXT pFormat,CString PostFix);
		UINT    CreateIndex (BOOL fUpdate);
		UINT	GetNewIndex(void);
		CString	GetVarType(CAddress const &Addr);

		CString	GetTagName(CString const &Text);

		// Address Helpers
		UINT	GetIndex(CAddress const &Addr);
		BOOL	UsesName(UINT uTable);
		BOOL    SetStringOffset(CAddress &Addr);

		// Import/Export Support
		void	Export(FILE *pFile);
		BOOL	Import(FILE *pFile);
		WORD	MakeImportIndex(UINT uTable);
		WORD	MakeImportValue(CString s);
		BOOL	AssignColumns(UINT *pColumns, char *sLine);
		UINT	FindColItem(CString s, CString Line);
		void	ExpandType(CString &Text, UINT uItem, BOOL fStr);
		void	ShowErrors(CWnd *pWnd);
		DWORD	GetAddrFromName(CString sName);

		// Friends
		friend	class CBSAPFULLTagsDialog;
		friend	class CBSAPFULLTagsUDPDialog;
		friend	class CBSAPFULLTagsSerialDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags UDP Master Device Options
//

class CBSAPFULLTagsUDPDeviceOptions : public CBSAPFULLTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLTagsUDPDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

		void	AddMetaData(void);
		UINT	m_Push;

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
		void	OnManage(CWnd *pWnd);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Master Device Options
//

class CBSAPFULLUDPDeviceOptions : public CBSAPFULLTagsUDPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLUDPDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Local;
		UINT	m_Global;
		UINT	m_Level;
		UINT	m_MLevel;
		UINT	m_Path;
		UINT	m_IPAddr;
		UINT	m_Socket;
		UINT	m_Keep;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags Serial Device Options
//

class CBSAPFULLTagsSerialDeviceOptions : public CBSAPFULLTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLTagsSerialDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		void	AddMetaData(void);
		UINT	m_Push;

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
		void	OnManage(CWnd *pWnd);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Master Device Options
//

class CBSAPFULLSerialDeviceOptions : public CBSAPFULLTagsSerialDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLSerialDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Local;
		UINT	m_Global;
		UINT	m_Level;
		UINT	m_MLevel;
		UINT	m_Path;

protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Slave Driver Options
//

class CBSAPFULLUDPSDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLUDPSDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Local;
		UINT m_Global;
		UINT m_Level;
		UINT m_IPAddr;
		UINT m_Socket;
		UINT m_Count;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BSAP Full Serial Slave Driver Options
//

class CBSAPFULLSerialSDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLSerialSDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Local;
		UINT m_Global;
		UINT m_Level;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags UDP Slave Device Options
//

class CBSAPFULLTagsUDPSDeviceOptions : public CBSAPFULLTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLTagsUDPSDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

		void	AddMetaData(void);
		UINT	m_Push;

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
		void	OnManage(CWnd *pWnd);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Slave Device Options
//

class CBSAPFULLUDPSDeviceOptions : public CBSAPFULLTagsUDPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLUDPSDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags Serial Device Options
//

class CBSAPFULLTagsSerialSDeviceOptions : public CBSAPFULLTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLTagsSerialSDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		void	AddMetaData(void);
		UINT	m_Push;

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
		void	OnManage(CWnd *pWnd);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Slave Device Options
//

class CBSAPFULLSerialSDeviceOptions : public CBSAPFULLTagsSerialSDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBSAPFULLSerialSDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data

protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

enum {
	WARRNAM	=  1,	// save Name
	WARRMSD	=  2,	// save MSD
	WARRTYP	=  3,	// save Data Type
	WARRLNA	=  7,	// save List Number and Position
	WARRLNM	=  8,	// save List Number
	WARRLPS	=  9,	// save List Position
	WARROFF	= 10,	// save Offset
	WARRLVL	= 11,	// save Level Selection
	WARRDVA	= 12,	// save Device Address Selection
	WARRGLB	= 13,	// save Global Address
	WARRMLV	= 14,	// save Master Device Level
	WARRPAT	= 15,	// save Path
	WARRTAB	= 16,	// save Table number
	WARRDGL	= 17,	// save Device Global
	WARRDNM	= 18,	// save Device Number
	WARRAST	= 19,	// save Array Status

	RARRNAM	= 21,	// read Name
	RARRMSD	= 22,	// read MSD
	RARRTYP	= 23,	// read Type
	RARRLNA	= 27,	// read List Number and Position
	RARRLNM	= 28,	// read List Number
	RARRLPS	= 29,	// read List Position
	RARROFF	= 30,	// read Offset
	RARRLVL = 31,	// read Level Selection
	RARRDVA	= 32,	// read Device Address Selection
	RARRGLB	= 33,	// read Global Address Selection
	RARRMLV	= 34,	// read Master Device Level
	RARRPAT	= 35,	// read Path
	RARRTAB	= 36,	// read Table number
	RARRDGL	= 37,	// read Device Global
	RARRDNM	= 38,	// read Device Number
	RARRAST	= 39,	// read Array Status

	WARRITM	= 40,	// set item select
	RARRCNT	= 41,	// read Array Size
	FINDNAM	= 42,	// Find Name In List
	FNXTMSD = 43,	// Find Next MSD
	CHKMSD	= 44,	// Check if MSD is used
	RARRITM = 45,	// get current selection
	WFREEZE	= 46,	// Use this Index
	WCASTAT	= 47,	// Clear Array Status (for master)

	EARRFNC	= 50,	// save to arrays
	FAPPEND	= 51,	// append values
	FINSERT	= 52,	// insert @ m_uItem
	FDELETE	= 53,	// delete & compact @ m_uItem
	FREPLCE	= 54,	// replace @ m_uItem
	FCLEAR	= 55,	// clear holding values
	FMASTER	= 56,	// Master Flag

	ADDRLOC	= 60,	// Hold Local Address
	REPALOC	= 61,	// Update Local Address
	GETALOC	= 62,	// Get Old Local Address

	MAKENRT	= 70,	// Create NRT Tags for Master
	DRVPTR	= 71,	// Set pointer to driver
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags UDP Master Driver
//

class CBSAPFULLTagsUDPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBSAPFULLTagsUDPDriver(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Master Driver
//

class CBSAPFULLUDPDriver : public CBSAPFULLTagsUDPDriver
{
	public:
		// Constructor
		CBSAPFULLUDPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags Serial Master Driver
//

class CBSAPFULLTagsSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBSAPFULLTagsSerialDriver(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Master Driver
//

class CBSAPFULLSerialDriver : public CBSAPFULLTagsSerialDriver
{
	public:
		// Constructor
		CBSAPFULLSerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags UDP Slave Driver
//

class CBSAPFULLTagsUDPSDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBSAPFULLTagsUDPSDriver(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Slave Driver
//

class CBSAPFULLUDPSDriver : public CBSAPFULLTagsUDPSDriver
{
	public:
		// Constructor
		CBSAPFULLUDPSDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags Serial Slave Driver
//

class CBSAPFULLTagsSerialSDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBSAPFULLTagsSerialSDriver(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Driver
//

class CBSAPFULLSerialSDriver : public CBSAPFULLTagsSerialSDriver
{
	public:
		// Constructor
		CBSAPFULLSerialSDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

enum {				// dialog uID's
	ETNAME		= 2001,	// Tag Name
	PBCREATE	= 4011,	// Create Tag button
	PBREFRSH	= 4012,	// Update display button
	PBBOOL		= 4013, // Create BOOL Alarm Accept
	PBREAL		= 4014, // Create REAL Alarm Accept
	GBSLAVE		= 4019,	// Slave Configuration Group Box Label
	CBTYPE		= 4021,	// Data type
	INDEXTXT	= 4022,	// "Index"
	DINDEX		= 4023,	// Address Index
	ETSLVTXT	= 4025,	// Device Number to set TMS/NRT creation
	ETSLVNUM	= 4026,	// Device Number to set TMS/NRT creation
	ETSLVGTX	= 4027, // "Global Dest"
	ETSLVGNM	= 4028,	// Global Device Address
	LEVELTXT	= 4029,	// "Device Level"
	CBLEVEL		= 4030,	// Level of Tag's Device
	MSDTXT		= 4031,	// "MSD Address"
	MSDVAL		= 4032,	// MSD Address
	LNUMTXT		= 4033,	// "List Number"
	LISTNM		= 4034,	// Slave List Number
	LISTPOS		= 4035,	// "List Position:"
	LISTPVAL	= 4036, // Position in List
	ARRAYTXT	= 4037,	// "Array Number"
	ETARRAY		= 4038,	// Array Number
	CBCOLUMN	= 4039, // Column Count
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Selection Dialog
//

class CBSAPFULLMasterTagsDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBSAPFULLMasterTagsDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBSAPFULLMasterTagsDialog(void);

		// Initialisation
		void	SetCaption(CString const &Text);
		void	SetSelect(BOOL fSelect);
		                
	protected:
		// Data Members
		CString		m_Caption;
		HTREEITEM	m_hRoot;
		HTREEITEM	m_hSelect;
		DWORD		m_dwSelect;
		CImageList	m_Images;
		BOOL		m_fLoad;
		BOOL		m_fSelect;
		BOOL		m_fUpdate;
		CViewWnd	*m_pViewWnd;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL	OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void	OnComboChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);
		BOOL	OnCreateTag(UINT uID);
		BOOL	OnCreateBOOLAck(UINT uID);
		BOOL	OnCreateREALAck(UINT uID);
		BOOL	AddAckSpace(BOOL fBool);
		BOOL	AddNewTag(CError Error, CAddress Addr, CString Name);
		void	UpdateTagList(CAddress Addr, CString Name);
		BOOL	OnUpdateAddress(UINT uID);
		UINT	FindNextNRT(void);

		// Tag Name Loading
		void	LoadNames(void);
		void	LoadNames(CTreeView &Tree);
		void	LoadRoot(CTreeView &Tree);
		void	LoadTags(CTreeView &Tree);
		void	LoadTag (CTreeView &Tree, CString Name, CAddress Addr);

		// Implementation
		void	ShowAddress(CAddress Addr);
		void	ShowName(UINT uIndex);
		void	ShowIndx(UINT uIndex);
		void	ShowCBType(UINT uIndex, BOOL fNRT, BOOL fStr);
		void	SetCBType(UINT uVal, BOOL fNRT, BOOL fStr);
		void	ShowMSD(UINT uIndex);
		void	ShowLNum(UINT uIndex);
		void	ShowPVal(DWORD dVal, UINT uID);
		void	ShowLevel(UINT uIndex);
		void	ShowDevAdd(UINT uIndex);
		void	ShowGlobal(UINT uIndex);
		BOOL	DeleteTag(void);
		BOOL	RenameTag(void);
		void	UpdateTagAddr(void);

		// Implementation
		BOOL	IsDown(UINT uCode);
		DWORD	MakeAddrFromSelects(UINT uOffset);
		UINT	GetSelect(UINT uTable, UINT uOffset);
		UINT	GetSelectFromAddr(UINT uTable, UINT uOffset);
		UINT	FindNextUnusedOffset(UINT uTable);
		BOOL	OffsetUsed(UINT uTable, UINT uOffset, UINT uSelect);
		UINT	GetNRTFromAddr(UINT uOffset);
		BOOL	IsNRTTable(UINT uTable);
		BOOL	IsBitTable(UINT uTable);
		BOOL    IsByteTable(UINT uTable);
		BOOL	IsWordTable(UINT uTable);
		BOOL	IsDwordTable(UINT uTable);
		BOOL	IsIntTable(UINT uTable);
		BOOL	IsRealTable(UINT uTable);
		BOOL	IsStrTable(UINT uTable);

		// Dialog Box Data Handling
		void	LoadCBAll(void);
		void	LoadCBType(void);
		void	LoadCBLevel(void);
		void	LoadCBArray(void);
		void	SetBoxPosition(UINT uID, UINT uPos);
		UINT	GetBoxPosition(UINT uID);
		void	ClearBox(CComboBox & ccb);
		void	ClearBox(UINT uID);
		UINT	GetCBPos(void);
		UINT	GetCBType(void);
		UINT	GetETMSD(void);
		UINT	GetETLNum(void);
		UINT	GetETIndex(void);
		BYTE	GetDeviceAddr(void);
		WORD	GetGlobal(void);
		BYTE	GetLevel(void);
		DWORD	GetArrayValue(void);
		void	DoDlgEnables(void);
		WORD	MakeDevNum(UINT uLevel, UINT uDevice);

		// Device Config Array Access
		DWORD	AccessArrayVal(DWORD dSel, DWORD dVal);
		CString	AccessArrayStr(DWORD dSel, DWORD dVal, CString sStr);
		void	SetArrayItems(DWORD dSel, DWORD dVal, CString sStr);
		void	DSetItem(DWORD dVal);
		void	DAppendItem(DWORD dVal);
		void	DSetName(CString sStr);
		void	DSetMSD (DWORD dVal);
		void	DSetType(DWORD dVal);
		void	DSetLNum(DWORD dVal);
		void	DSetOffs(DWORD dVal);
		void	DSetLevel(DWORD dVal);
		void	DSetDevAdd(DWORD dVal);
		void	DSetGlobal(DWORD dVal);
		void	DSetDevGlobal(DWORD dVal);
		void	DSetTable(DWORD dVal);
		void	DSetArrayStatus(void);
		void	DReplace(UINT uItem);
		void	ClearHolding(void);
		void	DFreezeIndex(UINT uIndex);
		DWORD	DSetDevNum(DWORD dVal);
		CString	DGetName(UINT uItem);
		DWORD	DGetIndx(UINT uItem);
		DWORD	DGetMSD (UINT uItem);
		DWORD	DGetType(UINT uItem);
		DWORD	DGetLNum(UINT uItem);
		DWORD	DGetOffs(UINT uItem);
		DWORD	DGetLevel(UINT uItem);
		DWORD	DGetDevAdd(UINT uItem);
		DWORD	DGetTable(UINT uItem);
		DWORD	DGetLocalAddr(void);
		DWORD	DGetGlobalAddr(BOOL fOfDevice);
		DWORD	DGetDevNumExtra(UINT uIndex);
		DWORD	DGetDevNumFromExtra(UINT uExtra);
		DWORD	DGetMLevel(void);
		DWORD	DGetPath(void);
		DWORD	DGetArrayCount(void);
		DWORD	DFindUnUsedMSD(void);
		DWORD	DCheckMSD(DWORD dItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Selection Dialog
//

class CBSAPFULLSlaveTagsDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBSAPFULLSlaveTagsDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBSAPFULLSlaveTagsDialog(void);

		// Initialisation
		void	SetCaption(CString const &Text);
		void	SetSelect(BOOL fSelect);
		                
	protected:
		// Data
		CString		m_Caption;
		HTREEITEM	m_hRoot;
		HTREEITEM	m_hSelect;
		DWORD		m_dwSelect;
		CImageList	m_Images;
		BOOL		m_fLoad;
		BOOL		m_fSelect;
		BOOL		m_fUpdate;
		CViewWnd	*m_pViewWnd;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL	OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void	OnComboChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);
		BOOL	OnCreateTag(UINT uID);
		BOOL	AddNewTag(CError Error, CAddress Addr, CString Name);
		BOOL	OnUpdateAddress(UINT uID);
		UINT	FindNextNRT(void);

		// Tag Name Loading
		void	LoadNames(void);
		void	LoadNames(CTreeView &Tree);
		void	LoadRoot(CTreeView &Tree);
		void	LoadTags(CTreeView &Tree);
		void	LoadTag (CTreeView &Tree, CString Name, CAddress Addr);

		// Implementation
		void	ShowAddress(CAddress Addr);
		void	ShowName(UINT uIndex);
		void	ShowIndx(UINT uIndex);
		void	ShowCBType(UINT uIndex, BOOL fNRT);
		void	SetCBType(UINT uVal, BOOL fNRT);
		void	ShowMSD(UINT uIndex);
		void	ShowLNum(UINT uIndex);
		void	ShowPVal(DWORD dVal, UINT uID);
		void	ShowLevel(UINT uIndex);
		void	ShowDevAdd(UINT uIndex);
		void	ShowGlobal(UINT uIndex);
		void	ShowArrayStatus(UINT uIndex);
		BOOL	DeleteTag(void);
		BOOL	RenameTag(void);
		void	UpdateTagAddr(void);

		// Implementation
		BOOL	IsDown(UINT uCode);
		DWORD	MakeAddrFromSelects(UINT uOffset);
		UINT	FindAvailableMSD(UINT uMSD, CAddress Addr);
		UINT	GetSelect(UINT uTable, UINT uOffset);
		UINT	GetSelectFromAddr(UINT uTable, UINT uOffset);
		UINT	GetNRTFromAddr(UINT uOffset);
		BOOL	IsNRTTable(UINT uTable);
		BOOL	IsBitTable(UINT uTable);
		BOOL	IsRealTable(UINT uTable);
		BOOL	IsStrTable(UINT uTable);

		// Dialog Box Data Handling
		void	LoadCBAll(void);
		void	LoadCBType(void);
		void	LoadCBLevel(void);
		void	LoadCBArray(void);
		void	SetBoxPosition(UINT uID, UINT uPos);
		UINT	GetBoxPosition(UINT uID);
		void	ClearBox(CComboBox & ccb);
		void	ClearBox(UINT uID);
		UINT	GetCBType(void);
		UINT	GetETMSD(void);
		UINT	GetETLNum(void);
		UINT	GetETLPos(void);
		UINT	GetETNandP(void);
		UINT	GetETIndex(void);
		BYTE	GetDeviceAddr(void);
		WORD	GetGlobal(void);
		UINT	GetArrayStatus(void);
		UINT	GetArrayData(void);
		BYTE	GetLevel(void);
		DWORD	GetArrayValue(void);
		void	DoDlgEnables(void);
		WORD	MakeDevNum(UINT uLevel, UINT uDevice);
		UINT	MakeAStatus(void);

		// Device Config Array Access
		DWORD	AccessArrayVal(DWORD dSel, DWORD dVal);
		CString	AccessArrayStr(DWORD dSel, DWORD dVal, CString sStr);
		void	SetArrayItems(DWORD dSel, DWORD dVal, CString sStr);
		void	DSetItem(DWORD dVal);
		void	DAppendItem(DWORD dVal);
		void	DSetName(CString sStr);
		void	DSetMSD (DWORD dVal);
		void	DSetType(DWORD dVal);
		void	DSetLNLP(DWORD dVal);
		void	DSetLNum(DWORD dVal);
		void	DSetOffs(DWORD dVal);
		void	DSetLevel(DWORD dVal);
		void	DSetDevAdd(DWORD dVal);
		void	DSetGlobal(DWORD dVal);
		void	DSetDevGlobal(DWORD dVal);
		void	DSetTable(DWORD dVal);
		void	DSetArrayStatus(DWORD dVal);
		void	DReplace(UINT uItem);
		void	ClearHolding(void);
		void	DFreezeIndex(UINT uIndex);
		DWORD	DSetDevNum(DWORD dVal);
		CString	DGetName(UINT uItem);
		DWORD	DGetIndx(UINT uItem);
		DWORD	DGetMSD (UINT uItem);
		DWORD	DGetType(UINT uItem);
		DWORD	DGetLNLP(UINT uItem);
		DWORD	DGetLNum(UINT uItem);
		DWORD	DGetLPos(UINT uItem);
		DWORD	DGetOffs(UINT uItem);
		DWORD	DGetLevel(UINT uItem);
		DWORD	DGetDevAdd(UINT uItem);
		DWORD	DGetTable(UINT uItem);
		DWORD	DGetLocalAddr(void);
		DWORD	DGetGlobalAddr(BOOL fOfDevice, UINT uItem);
		DWORD	DGetArrayStatus(UINT uItem);
		DWORD	DGetDevNumExtra(UINT uIndex);
		DWORD	DGetDevNumFromExtra(UINT uExtra);
		DWORD	DGetMLevel(void);
		DWORD	DGetPath(void);
		DWORD	DGetArrayCount(void);
		DWORD	DFindUnUsedMSD(void);
		DWORD	DCheckMSD(DWORD dItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Master Selection Dialog
//

class CBSAPFULLTagsUDPDialog : public CBSAPFULLMasterTagsDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBSAPFULLTagsUDPDialog(CBSAPFULLTagsUDPDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBSAPFULLTagsUDPDialog(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Master Selection Dialog
//

class CBSAPFULLTagsSerialDialog : public CBSAPFULLMasterTagsDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBSAPFULLTagsSerialDialog(CBSAPFULLTagsSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBSAPFULLTagsSerialDialog(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Slave Selection Dialog
//

class CBSAPFULLTagsUDPSDialog : public CBSAPFULLSlaveTagsDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBSAPFULLTagsUDPSDialog(CBSAPFULLTagsUDPSDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBSAPFULLTagsUDPSDialog(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Selection Dialog
//

class CBSAPFULLTagsSerialSDialog : public CBSAPFULLSlaveTagsDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBSAPFULLTagsSerialSDialog(CBSAPFULLTagsSerialSDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBSAPFULLTagsSerialSDialog(void);
	};

// End of File

#endif
