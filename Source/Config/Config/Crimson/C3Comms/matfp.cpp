
#include "intern.hpp"

#include "matfp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Matsushita Series
//

// Instantiator

ICommsDriver *	Create_MatsushitaFPDriver(void)
{
	return New CMatsushitaFPDriver;
	}

// Constructor

CMatsushitaFPDriver::CMatsushitaFPDriver(void)
{
	m_wID		= 0x3331;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Matsushita";
	
	m_DriverName	= "FP Series";
	
	m_Version	= "1.10";
	
	m_ShortName	= "Matsushita FP";

	AddSpaces();

	C3_PASSED();
	}

// Binding Control

UINT CMatsushitaFPDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMatsushitaFPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CMatsushitaFPDriver::AddSpaces(void)
{
	AddSpace(New CSpace( 1, "DT",  "Data Registers",		10,      0,   32765,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 2, "SDT", "Special Data Registers",	10,   9000,    9255,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 3, "LD",  "Link Data Registers",		10,	 0,    8447,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 4, "FL",  "File Registers",		10,	 0,   32765,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 5, "IX",  "X Index Registers",		10,	 0,    9999,	addrWordAsWord));
	AddSpace(New CSpace( 6, "IY",  "Y Index Registers",		10,	 0,    9999,	addrWordAsWord));
	AddSpace(New CSpace( 7, "SV",  "Timer-Counter Set Value",	10,	 0,    1024,	addrWordAsWord));
	AddSpace(New CSpace( 8, "EV",  "Timer-Counter Elapsed Value",	10,	 0,    1024,	addrWordAsWord));
	AddSpace(New CSpace( 9, "C",   "Counter Contact",		10,	 0,    1024,	addrBitAsBit));
	AddSpace(New CSpace(10, "T",   "Timer Contact",			10,	 0,    1024,	addrBitAsBit));
	AddSpace(New CSpace(11, "R",   "Internal Relay",		16,	 0,  0x255F,	addrBitAsBit));
	AddSpace(New CSpace(12, "SR",  "Special Internal Relay",	16, 0x9000,  0x910F,	addrBitAsBit));
	AddSpace(New CSpace(13, "LR",  "Link Relay",			16,	 0,  0x639F,	addrBitAsBit));
	AddSpace(New CSpace(14, "X",   "External Input Relay",		16,	 0,  0x511F,	addrBitAsBit));
	AddSpace(New CSpace(15, "Y",   "External Output Relay",		16,	 0,  0x511F,	addrBitAsBit));
	AddSpace(New CSpace(16, "WR",  "16 Internal Relays",		16,	 0,  0x0970,	addrWordAsWord));
	AddSpace(New CSpace(18, "WL",  "16 Link Relays",		16,	 0,  0x6390,	addrWordAsWord));
	AddSpace(New CSpace(19, "WX",  "16 External Input Relays",	16,	 0,  0x5110,	addrWordAsWord));
	AddSpace(New CSpace(20, "WY",  "16 External Output Relays",	16,	 0,  0x5110,	addrWordAsWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series v2 - MEWTOCOL-COM (Ascii) 
//

// Instantiator

ICommsDriver *	Create_MatsushitaFPDriver2(void)
{
	return New CMatsushitaFPDriver2;
	}

// Constructor

CMatsushitaFPDriver2::CMatsushitaFPDriver2(void)
{
	m_wID		= 0x4048;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Panasonic - Matsushita";
	
	m_DriverName	= "FP Series - FP MEWTOCOL-COM";
	
	m_Version	= "2.00";
	
	m_ShortName	= "MEWTOCOL-COM";
	}

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series v2 - MEWTOCOL-DAT (Binary)  
//

// Instantiator

ICommsDriver *	Create_MatsushitaFPDatDriver(void)
{
	return New CMatsushitaFPDatDriver;
	}

// Constructor

CMatsushitaFPDatDriver::CMatsushitaFPDatDriver(void)
{
	m_wID		= 0x4049;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Panasonic - Matsushita";
	
	m_DriverName	= "FP Series - MEWTOCOL-DAT (Binary)";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MEWTOCOL-DAT";

	DeleteAllSpaces();

	AddSpaces();
	}

 // Implementation

void CMatsushitaFPDatDriver::AddSpaces(void)
{
	AddSpace(New CSpace( 1, "DT",  "Data Registers",		10,      0,   32765,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 2, "SDT", "Special Data Registers",	10,   9000,    9255,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 3, "LD",  "Link Data Registers",		10,	 0,    8447,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 4, "FL",  "File Registers",		10,	 0,   32765,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace( 7, "SV",  "Timer-Counter Set Values",	10,	 0,     255,	addrWordAsWord));
	AddSpace(New CSpace( 8, "EV",  "Timer-Counter Elapsed Values",	10,	 0,     255,	addrWordAsWord));
	AddSpace(New CSpace(12, "SR",  "Special Internal Relays",	16, 0x9000,  0x910F,	addrBitAsBit));
	AddSpace(New CSpace(16, "WR",  "Internal Relays",		16,	 0,  0x0970,	addrWordAsWord));
	AddSpace(New CSpace(18, "WL",  "Link Relays",			16,	 0,  0x6390,	addrWordAsWord));
	AddSpace(New CSpace(19, "WX",  "External Input Relays",		16,	 0,  0x5110,	addrWordAsWord));
	AddSpace(New CSpace(20, "WY",  "External Output Relays",	16,	 0,  0x5110,	addrWordAsWord));
	AddSpace(New CSpace(21, "DTC", "Data Contact Info",		10,      0,   32765,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(22, "SDC", "Special Data Contact Info",	10,   9000,    9255,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(23, "LDC", "Link Data Contact Info",	10,	 0,    8447,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(24, "FLC", "File Contact Info",		10,	 0,   32765,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(27, "T",   "Timer Contact",			10,	 0,     255,	addrWordAsWord));
	AddSpace(New CSpace(28, "C",   "Counter Contact",		10,	 0,     255,	addrWordAsWord));
	AddSpace(New CSpace(32, "SRC", "Spec. Internal Relay Contact",	16, 0x9000,  0x910F,	addrBitAsBit));
	AddSpace(New CSpace(36, "R",   "Internal Relay Contact",	16,	 0,  0x0970,	addrWordAsWord));
	AddSpace(New CSpace(38, "L",   "Link Relay Contact",		16,	 0,  0x6390,	addrWordAsWord));
	AddSpace(New CSpace(39, "X",   "Ext. Input Relay Contact",	16,	 0,  0x5110,	addrWordAsWord));
	AddSpace(New CSpace(40, "Y",   "Ext. Output Relay Contact",	16,	 0,  0x5110,	addrWordAsWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series - MEWTOCOL-COM TCP/IP Master Driver
//

// Instantiator

ICommsDriver *	Create_MatsushitaFPTCPDriver(void)
{
	return New CMatsushitaFPTCPDriver;
	}

// Constructor

CMatsushitaFPTCPDriver::CMatsushitaFPTCPDriver(void)
{
	m_wID		= 0x404A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Panasonic - Matsushita";
	
	m_DriverName	= "FP Series - FP MEWTOCOL-COM TCP/IP Master";
	
	m_Version	= "2.00";
	
	m_ShortName	= "FP TCP/IP Master";
	}

// Destructor

CMatsushitaFPTCPDriver::~CMatsushitaFPTCPDriver(void)
{

	}

// Binding Control

UINT CMatsushitaFPTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CMatsushitaFPTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CMatsushitaFPTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMatsushitaFPTCPDriverOptions);
	}

// Configuration

CLASS CMatsushitaFPTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMatsushitaFPTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series - TCP/IP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMatsushitaFPTCPDeviceOptions, CUIItem);       

// Constructor

CMatsushitaFPTCPDeviceOptions::CMatsushitaFPTCPDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD( 200, 15), MAKEWORD(  168, 192)));;

	m_Port   = 1025;

	m_Unit   = 1;

	m_Ping   = 0;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_ETLan  = 0;

	m_Rs1	 = 0;

	m_Rs2	 = 0;

	m_Rs3	 = 0;

	m_Rs1R	 = 0;

	m_Rs2R	 = 0;

	m_Rs3R	 = 0;

	SetPages(2);
	}

// UI Managament

void CMatsushitaFPTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "ETLan" ) {

			BOOL fEnable = m_ETLan == 2;

			pWnd->EnableUI("Rs1",  fEnable);

			pWnd->EnableUI("Rs2",  fEnable);

			pWnd->EnableUI("Rs3",  fEnable);

			pWnd->EnableUI("Rs1R", fEnable);

			pWnd->EnableUI("Rs2R", fEnable);

			pWnd->EnableUI("Rs3R", fEnable);
			}
		}
	}

// Download Support

BOOL CMatsushitaFPTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddWord(WORD(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_ETLan));
	Init.AddByte(BYTE(m_Rs1));
	Init.AddByte(BYTE(m_Rs2));
	Init.AddByte(BYTE(m_Rs3));
	Init.AddByte(BYTE(m_Rs1R));
	Init.AddByte(BYTE(m_Rs2R));
	Init.AddByte(BYTE(m_Rs3R));
	
	return TRUE;
	}

// Meta Data Creation

void CMatsushitaFPTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(ETLan);
	Meta_AddInteger(Rs1);
	Meta_AddInteger(Rs2);
	Meta_AddInteger(Rs3);
	Meta_AddInteger(Rs1R);
	Meta_AddInteger(Rs2R);
	Meta_AddInteger(Rs3R);
	}

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series - Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CMatsushitaFPTCPDriverOptions, CUIItem);       

// Constructor

CMatsushitaFPTCPDriverOptions::CMatsushitaFPTCPDriverOptions(void)
{
	m_Source = 64;

	m_Port   = 1026;
	}

// Download Support

BOOL CMatsushitaFPTCPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Source));
	Init.AddWord(WORD(m_Port));
	
	return TRUE;
	}

// Meta Data Creation

void CMatsushitaFPTCPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Source);
	Meta_AddInteger(Port);
	}

// End of File
