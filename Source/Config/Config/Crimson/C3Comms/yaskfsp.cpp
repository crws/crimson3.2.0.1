
#include "intern.hpp"

#include "yaskfsp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa FSP Drive Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskFSPDeviceOptions, CUIItem);

// Constructor

CYaskFSPDeviceOptions::CYaskFSPDeviceOptions(void)
{
	m_Axis   = 0;
	}

// UI Management

void CYaskFSPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CYaskFSPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Axis));

	return TRUE;
	}

// Meta Data Creation

void CYaskFSPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Axis);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa FSP Drive Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaFSPDriver(void)
{
	return New CYaskawaFSPDriver;
	}

// Constructor

CYaskawaFSPDriver::CYaskawaFSPDriver(void)
{
	m_wID		= 0x4038;

	m_uType		= driverMaster;

	m_Manufacturer	= "Yaskawa";

	m_DriverName	= "FSP Drive";

	m_Version	= "1.00";

	m_ShortName	= "Yaskawa FSP Drive";

	AddSpaces();
	}

// Binding Control

UINT	CYaskawaFSPDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CYaskawaFSPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS422;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CYaskawaFSPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskFSPDeviceOptions);
	}

// Address Management

BOOL CYaskawaFSPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	if( fPart && Addr.a.m_Table >= HIMM && Addr.a.m_Table <= HSEQ ) return FALSE;

	CYaskawaFSPAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation	

void CYaskawaFSPDriver::AddSpaces(void)
{
/* -1- include if Sequential Commands Added
	AddSpace(New CSpace(HIMM, " ",	"IMMEDIATE COMMANDS...",			10, 0,     0, LL));
/-1- */
	AddSpace(New CSpace( 3, "VARI", "   VARIABLE__(R72/W81)",			10, 1,   100, LL));
	AddSpace(New CSpace( 2, "PARI", "   PARAMETER__(R85/W80)",			16, 0, 0x600, WW));
	AddSpace(New CSpace(10, "POLI", "   POLLING (Status)_(0)",			10, 0,     0, WW));
	AddSpace(New CSpace(11, "ACCI", "   ACCELERATION__(64)",			10, 0,     0, LL));
	AddSpace(New CSpace(12, "CONI", "   CONTROL__(69)",				10, 0,     0, BB));
	AddSpace(New CSpace(13, "GAII", "   GAIN__(71)",				10, 0,     0, WW));
	AddSpace(New CSpace( 1, "GFAI", "   GET FROM ARRAY_(160)",			10, 1,  1000, LL));
	AddSpace(New CSpace(14, "GTVI", "   GET VERSION__(63)",				10, 0,     0, LL));
	AddSpace(New CSpace(15, "JRKI", "   JERK TIME__(74)",				10, 0,     0, LL));
	AddSpace(New CSpace(16, "RFAI", "   Set Variable = ARRAY[Index]_(159)",		10, 0,     0, LL));
	AddSpace(New CSpace(17, "RFAI1","      READ FROM ARRAY - Index",		10, 0,     0, WW));
	AddSpace(New CSpace(18, "RFAI2","      READ FROM ARRAY - Variable",		10, 0,     0, BB));
	AddSpace(New CSpace(19, "RUNI", "   RUN__(78)",					10, 0,     0, BB));
	AddSpace(New CSpace(20, "S1OI", "   SET OUTPUT__(79)",				10, 0,     0, BB));
	AddSpace(New CSpace(21, "S1OI1","      SET OUTPUT - Number",			10, 0,     0, WW));
	AddSpace(New CSpace(22, "S1OI2","      SET OUTPUT - State",			10, 0,     0, BB));
	AddSpace(New CSpace(23, "SNOI", "   SET OUTPUTS__(107)",			10, 0,     0, BB));
	AddSpace(New CSpace(24, "SNOI1","      SET OUTPUTS - Mask",			10, 0,     0, LL));
	AddSpace(New CSpace(25, "SNOI2","      SET OUTPUTS - State",			10, 0,     0, LL));
	AddSpace(New CSpace(26, "SZAI", "   SET ZERO POSITION_(95)",			10, 0,     0, BB));
	AddSpace(New CSpace(27, "SPDI", "   SPEED__(83)",				10, 0,     0, LL));
	AddSpace(New CSpace(28, "STAI", "   START__(82)",				10, 0,     0, BB));
// YET	AddSpace(New CSpace(29, "STPI", "   STOP (Version 2.91)_(84)",			10, 0,     0, BB));
	AddSpace(New CSpace(30, "STXI", "   STOP EX__(153)",				10, 0,     0, BB));
	AddSpace(New CSpace(31, "STXI1","      STOP EX - Type",				10, 0,     0, BB));
	AddSpace(New CSpace(32, "STXI2","      STOP EX - Servo State",			10, 0,     0, BB));
// YET	AddSpace(New CSpace(33, "STMI", "   STOP MOTION (Version 2.91)_(99)",		10, 0,     0, BB));
	AddSpace(New CSpace(34, "TQLI", "   TORQUE LIMITS__(87)",			10, 0,     0, BB));
	AddSpace(New CSpace(35, "TQLI1","      TORQUE LIMIT - Forward",			10, 0,     0, WW));
	AddSpace(New CSpace(36, "TQLI2","      TORQUE LIMIT - Reverse",			10, 0,     0, WW));
	AddSpace(New CSpace(37, "WRII", "   Set ARRAY[Index] = Value_(158)",		10, 0,     0, BB));
	AddSpace(New CSpace(38, "WRII1","      WRITE TO ARRAY - Index",			10, 0,     0, WW));
	AddSpace(New CSpace(39, "WRII2","      WRITE TO ARRAY - Value",			10, 0,     0, LL));
	AddSpace(New CSpace(ERROP, "ERROR","Command Fault Response",			10, 0,     0, LL));

/* -1- include if Sequential commands added
	AddSpace(New CSpace(HSEQ, " ",	"SEQUENTIAL COMMANDS...",			10, 0.     0, LL));
	AddSpace(New CSpace(  6, "VARS", "   VARIABLE__(R72/W81)",			10, 1,   100, LL));
	AddSpace(New CSpace(  5, "PARS", "   PARAMETER__(R85/W80)",			16, 0, 0x600, WW));
	AddSpace(New CSpace(100, "ACCS", "   ACCELERATION__(64)",			10, 0,     0, LL));
	AddSpace(New CSpace(101, "CONS", "   CONTROL__(69)",				10, 0,     0, BB));
	AddSpace(New CSpace(102, "DELS", "   DELAY__(144)",				10, 0,     0, LL));
	AddSpace(New CSpace(103, "ECDS", "   ECAM DISENGAGE_(122)",			10, 0,     0, BB));
	AddSpace(New CSpace(104, "ECES", "   ECAM ENGAGE__(121)",			10, 0,     0, BB));
	AddSpace(New CSpace(105, "ECES1","      ECAM ENGAGE - ID",			10, 0,     0, BB));
	AddSpace(New CSpace(106, "ECES2","      ECAM ENGAGE - MODE",			10, 0,     0, BB));
	AddSpace(New CSpace(107, "ENGS", "   ENGAGE VIRTUAL AXIS_(136)",		10, 0,     0, BB));
	AddSpace(New CSpace(108, "ENGS1","      ENGAGE VIRTUAL AXIS - ID",		10, 0,     0, BB));
	AddSpace(New CSpace(109, "ENGS2","      ENGAGE VIRTUAL AXIS - Direction",	10, 0,     0, BB));
	AddSpace(New CSpace(110, "FOSS", "   FAST OUTPUT SETTING_(154)",		10, 0,     0, BB));
	AddSpace(New CSpace(111, "FOSS1","      FAST OUTPUT SETTING - Variable",	10, 0,     0, BB));
	AddSpace(New CSpace(112, "FOSS2","      FAST OUTPUT SETTING - Condition",	10, 0,     0, BB));
	AddSpace(New CSpace(113, "FOSS3","      FAST OUTPUT SETTING - Value",		10, 0,     0, LL));
	AddSpace(New CSpace(114, "GAIS", "   GAIN__(71)",				10, 0,     0, WW));
	AddSpace(New CSpace(  4, "GFAS", "   GET FROM ARRAY_(160)",			10, 1,  1000, LL));
	AddSpace(New CSpace(115, "GOAS", "   GO__(112)",				10, 0,     0, BB));
	AddSpace(New CSpace(116, "GOAS1","      GO - Target",				10, 0,     0, LL));
	AddSpace(New CSpace(117, "GOAS2","      GO - Time",				10, 0,     0, LL));
	AddSpace(New CSpace(118, "GODS", "   GO D__(128)",				10, 0,     0, BB));
	AddSpace(New CSpace(119, "GODS1","      GO D - Target",				10, 0,     0, LL));
	AddSpace(New CSpace(120, "GODS2","      GO D - Time",				10, 0,     0, LL));
	AddSpace(New CSpace(121, "GOHS", "   GO H__(117)",				10, 0,     0, BB));
	AddSpace(New CSpace(122, "HARS", "   HARD HOME__(131)",				10, 0,     0, BB));
	AddSpace(New CSpace(123, "HARS1","      HARD HOME - Torque",			10, 0,     0, WW));
	AddSpace(New CSpace(124, "HARS2","      HARD HOME - Speed",			10, 0,     0, LL));
	AddSpace(New CSpace(125, "HMCS", "   HOME C__(133)",				10, 0,     0, LL));
	AddSpace(New CSpace(126, "HMSS", "   HOME SW__(132)",				10, 0,     0, BB));
	AddSpace(New CSpace(127, "HMSS1","      HOME SW - Speed to switch",		10, 0,     0, LL));
	AddSpace(New CSpace(128, "HMSS2","      HOME SW - Return speed",		10, 0,     0, LL));
	AddSpace(New CSpace(129, "HSCS", "   HOME SW C__(130)",				10, 0,     0, BB));
	AddSpace(New CSpace(130, "HSCS1","      HOME SW C - Speed to switch",		10, 0,     0, LL));
	AddSpace(New CSpace(131, "HSCS2","      HOME SW C - Speed to C pulse",		10, 0,     0, LL));
	AddSpace(New CSpace(132, "JRKS", "   JERK TIME__(74)",				10, 0,     0, LL));
	AddSpace(New CSpace(133, "LATS", "   LATCHING TRIGGER_(152)",			10, 0,     0, BB));
	AddSpace(New CSpace(134, "MVAS", "   MOVE__(113)",				10, 0,     0, LL));
	AddSpace(New CSpace(135, "MVAS1","      MOVE - Distance",			10, 0,     0, LL));
	AddSpace(New CSpace(136, "MVAS2","      MOVE - Time",				10, 0,     0, LL));
	AddSpace(New CSpace(137, "MVDS", "   MOVE D__(129)",				10, 0,     0, LL));
	AddSpace(New CSpace(138, "MVDS1","      MOVE D - Distance",			10, 0,     0, LL));
	AddSpace(New CSpace(139, "MVDS2","      MOVE D - Time",				10, 0,     0, LL));
	AddSpace(New CSpace(140, "MVHS", "   MOVE H__(118)",				10, 0,     0, LL));
	AddSpace(New CSpace(141, "MVRS", "   MOVE R__(119)",				10, 0,     0, LL));
	AddSpace(New CSpace(142, "RFAS", "   Set Variable = ARRAY[Index]_(159)",	10, 0,     0, LL));
	AddSpace(New CSpace(143, "RFAS1","      READ FROM ARRAY - Index",		10, 0,     0, WW));
	AddSpace(New CSpace(144, "RFAS2","      READ FROM ARRAY - Variable",		10, 0,     0, BB));
	AddSpace(New CSpace(145, "REGS", "   REGISTRATION DISTANCE_(151)",		10, 0,     0, LL));
	AddSpace(New CSpace(146, "RUNS", "   RUN__(78)",				10, 0,     0, BB));
	AddSpace(New CSpace(147, "S1OS", "   SET OUTPUT__(79)",				10, 0,     0, BB));
	AddSpace(New CSpace(148, "S1OS1","      SET OUTPUT - Number",			10, 0,     0, WW));
	AddSpace(New CSpace(149, "S1OS2","      SET OUTPUT - State",			10, 0,     0, BB));
	AddSpace(New CSpace(150, "SNOS", "   SET OUTPUTS__(107)",			10, 0,     0, BB));
	AddSpace(New CSpace(151, "SNOS1","      SET OUTPUTS - Mask",			10, 0,     0, LL));
	AddSpace(New CSpace(152, "SNOS2","      SET OUTPUTS - State",			10, 0,     0, LL));
	AddSpace(New CSpace(153, "SZAS", "   SET ZERO POSITION_(95)",			10, 0,     0, BB));
	AddSpace(New CSpace(154, "SLDS", "   SLIDE__(115)",				10, 0,     0, LL));
	AddSpace(New CSpace(155, "SLNS", "   SLIDE ANALOG__(102)",			10, 0,     0, BB));
	AddSpace(New CSpace(156, "SPDS", "   SPEED__(83)",				10, 0,     0, LL));
	AddSpace(New CSpace(157, "SPCS", "   SPEED CONTROL_(100)",			10, 0,     0, BB));
	AddSpace(New CSpace(158, "STPS", "   STOP__(84)",				10, 0,     0, BB));
	AddSpace(New CSpace(159, "STXS", "   STOP EX__(153)",				10, 0,     0, BB));
	AddSpace(New CSpace(160, "STXS1","      STOP EX - Type",			10, 0,     0, BB));
	AddSpace(New CSpace(161, "STXS2","      STOP EX - Servo State",			10, 0,     0, BB));
	AddSpace(New CSpace(162, "STMS", "   STOP MOTION__(99)",			10, 0,     0, BB));
	AddSpace(New CSpace(163, "TQES", "   TORQUE__(116)",				10, 0,     0, WW));
	AddSpace(New CSpace(164, "TQAS", "   TORQUE ANALOG_(103)",			10, 0,     0, BB));
	AddSpace(New CSpace(165, "TQLS", "   TORQUE LIMITS__(87)",			10, 0,     0, BB));
	AddSpace(New CSpace(166, "TQLS1","      TORQUE LIMIT - Forward",		10, 0,     0, WW));
	AddSpace(New CSpace(167, "TQLS2","      TORQUE LIMIT - Reverse",		10, 0,     0, WW));
	AddSpace(New CSpace(168, "WEXS", "   WAIT EXACT__(145)",			10, 0,     0, LL));
	AddSpace(New CSpace(169, "WFSS", "   WAIT FOR START_(146)",			10, 0,     0, BB));
	AddSpace(New CSpace(170, "WAIS", "   WAIT INPUT__(109)",			10, 0,     0, BB));
	AddSpace(New CSpace(171, "WAIS1","      WAIT INPUT - Number",			10, 0,     0, BB));
	AddSpace(New CSpace(172, "WAIS2","      WAIT INPUT - Condition",		10, 0,     0, BB));
	AddSpace(New CSpace(173, "WAIS3","      WAIT INPUT - State",			10, 0,     0, BB));
	AddSpace(New CSpace(174, "WAIS4","      WAIT INPUT - Time",			10, 0,     0, LL));
	AddSpace(New CSpace(175, "WASS", "   WAIT STOP__(148)",				10, 0,     0, LL));
	AddSpace(New CSpace(176, "WAVS", "   WAIT VAR__(110)",				10, 0,     0, BB));
	AddSpace(New CSpace(177, "WAVS1","      WAIT VAR - Variable",			10, 0,     0, BB));
	AddSpace(New CSpace(178, "WAVS2","      WAIT VAR - Condition",			10, 0,     0, BB));
	AddSpace(New CSpace(179, "WAVS3","      WAIT VAR - Value",			10, 0,     0, LL));
	AddSpace(New CSpace(180, "WRIS", "   Set ARRAY[Index] = Value_(158)",		10, 0,     0, BB));
	AddSpace(New CSpace(181, "WRIS1","      WRITE TO ARRAY - Index",		10, 0,     0, WW));
	AddSpace(New CSpace(182, "WRIS2","      WRITE TO ARRAY - Value",		10, 0,     0, LL));
/-1- */
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa FSP Drive Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CYaskawaFSPAddrDialog, CStdAddrDialog);
		
// Constructor

CYaskawaFSPAddrDialog::CYaskawaFSPAddrDialog(CYaskawaFSPDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fPart = fPart;

	if( m_uAllowSpace != HSEQ ) {
		
		m_uAllowSpace = HIMM;
		}

	SetName(m_fPart ? TEXT("PartStdAddrDlg") : TEXT("YaskFSPAddressDlg"));
	}

// Message Map

AfxMessageMap(CYaskawaFSPAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CYaskawaFSPAddrDialog)
	};

// Message Handlers

BOOL CYaskawaFSPAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();
	
	if( !m_fPart ) {

		SetCaption();

		if( m_pSpace ) {

			SetAllow();
			}

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			if( !Addr.m_Ref ) {

				m_pSpace->GetMinimum(Addr);
				}

			if( !IsHeader(m_pSpace->m_uTable) ) {

				ShowAddress(Addr);
				ShowDetails();
				}

			return FALSE;
			}

		return TRUE;
		}
	else {
		LoadElementUI();

		LoadType();

		SetAllow();

		if( !IsHeader(m_pSpace->m_uTable) ) ShowAddress(Addr);

		return FALSE;
		}
	}

// Notification Handlers

void CYaskawaFSPAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CYaskawaFSPAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			SetAllow();

			UINT uTable = m_pSpace->m_uTable;

			if( IsHeader(uTable) ) {

				m_fIsHeader = TRUE;

				LoadList();

				uTable = m_pSpace->m_uTable;
				}

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			Addr.a.m_Offset = HasParameter(uTable) ? m_pSpace->m_uMinimum : 0;

			ShowDetails();

			ShowAddress(Addr);
			}

		else {
			if( HasParameter(pSpace->m_uTable) ) {

				GetDlgItem(2002).EnableWindow(TRUE);
				}
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(2001).SetWindowText("None");
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2003).SetWindowText("");
		GetDlgItem(2009).SetWindowText("");
		GetDlgItem(2010).SetWindowText("");
		GetDlgItem(2011).SetWindowText("");
		GetDlgItem(2012).SetWindowText("");
		GetDlgItem(2013).SetWindowText("");

		ClearDetails();

		GetDlgItem(2001).EnableWindow(TRUE);
		GetDlgItem(2002).EnableWindow(FALSE);
		}
	}

BOOL CYaskawaFSPAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		UINT uTable  = m_pSpace->m_uTable;

		if( IsHeader(uTable) ) {

			if( m_fPart ) return FALSE;

			SetAllow();

			LoadList();

			GetDlgItem(3002).SetWindowText("");

			UpdateInfo();

			return TRUE;
			}

		if( HasParameter(uTable) ) {

			Text += GetDlgItem(2002).GetWindowText();
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables

void CYaskawaFSPAddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 40, 100, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf("<%s>\t%s", CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING);

	INDEX	         Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( AllowSpace(pSpace) ) {

				CString cs = pSpace->m_Caption;
				CString cd = "";

				UINT l = cs.GetLength();
				UINT i = 0;

				while( i < l ) {

					if( cs[i] == '_' ) cd += '\t';
					else cd += cs[i];
					i++;
					}

				CString Entry;
			
				Entry.Printf("%s\t%s", pSpace->m_Prefix, cd);

				ListBox.AddString(Entry, DWORD(n) );

				if( m_fIsHeader ) {

					if( !IsHeader(pSpace->m_uTable) ) {

						if( pSpace->m_uMinimum != ERROP ) {

							Find        = n;
							m_fIsHeader = FALSE;
							}
						}
					}

				else {
					if( pSpace == m_pSpace ) Find = n;
					}
				}

			List.GetNext(n);
			}
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSpaceChange(1001, ListBox);
	}

void CYaskawaFSPAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	GetDlgItem(2001).EnableWindow(TRUE);

	UINT uTable = Addr.a.m_Table;

	BOOL fParam = HasParameter(uTable);

	if( fParam ) {

		CString s;

		if( uTable == 2 || uTable == 5 ) {

			s.Printf( "%X", Addr.a.m_Offset );
			}

		else {

			s.Printf( "%d", Addr.a.m_Offset );
			}

		GetDlgItem(2002).SetWindowText( s );

		GetDlgItem(2002).EnableWindow(TRUE);

		SetDlgFocus(2002);
		}

	else {
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2002).EnableWindow(FALSE);
		}

	UpdateMode();

	UpdateInfo();

	if( IsHeader(Addr.a.m_Table) ) GetDlgItem(3002).SetWindowText("");

	SetDlgFocus(fParam ? 2002 : 1001);
	}

// Helpers

BOOL CYaskawaFSPAddrDialog::AllowSpace(CSpace *pSpace)
{
	return pSpace->m_Prefix == "ERROR" ? TRUE : SelectList( pSpace->m_uTable, m_uAllowSpace );
	}

BOOL CYaskawaFSPAddrDialog::SelectList(UINT uTable, UINT uAllow)
{
	if( IsHeader(uTable) ) return TRUE;

	switch( uAllow ) {

		case HIMM:	return IsImmediate(uTable);

		case HSEQ:	return IsSequential(uTable);
		}

	return FALSE;
	}

void CYaskawaFSPAddrDialog::SetAllow()
{
	m_uAllowSpace = HIMM;

	if( m_pSpace ) {

		m_uAllowSpace = IsImmediate(m_pSpace->m_uTable) ? HIMM : HSEQ;

		return;
		}
	}

UINT CYaskawaFSPAddrDialog::IsHeader(UINT uCommand)
{
	return uCommand == HIMM || uCommand == HSEQ ? 1 : 0;
	}

BOOL CYaskawaFSPAddrDialog::IsImmediate(UINT uTable)
{
	return uTable <= 3 || (uTable >= 10 && uTable < 100) || uTable == HIMM || uTable == ERROP;
	}

BOOL CYaskawaFSPAddrDialog::IsSequential(UINT uTable)
{
	return (uTable >= 4 && uTable <= 6) || uTable >= 100 || uTable == HSEQ || uTable == ERROP;
	}

BOOL CYaskawaFSPAddrDialog::HasParameter(UINT uTable)
{
	return uTable >= 1 && uTable <= 6;
	}

void CYaskawaFSPAddrDialog::UpdateMode(void)
{
/* include if Sequential commands added
	if( IsHeader( m_pSpace->m_uTable ) ) SetListMode();

	else {
		switch( m_pSpace->m_Prefix[3] ) {

			case 'I':
				GetDlgItem(2013).SetWindowText("IMMEDIATE");
				break;

			case 'S':
				GetDlgItem(2013).SetWindowText("SEQUENTIAL");
				break;

			case 'O': // Error
				GetDlgItem(2013).SetWindowText("ALL MODES");
				break;
			}
		}
/*/
// remove following if Sequential commands added
	GetDlgItem(2013).SetWindowText(" ");
	}

void CYaskawaFSPAddrDialog::UpdateInfo(void)
{
	CString sInfo1 = "";
	CString sInfo2 = "";
	CString sInfo3 = "";
	CString sInfo4 = "";

	UINT uT = m_pSpace->m_uTable;

	if( !HasParameter(uT) ) {

		switch( uT ) {

			case  10:
			case  14:
				sInfo2 = SetInfoText(10, 0);
				break;

			case  11:
			case  12:
			case  13:
			case  15:
			case  26:
			case  27:
			case 102:
			case 121:
			case 125:
			case 133:
			case 140:
			case 141:
			case 145:
			case 154:
			case 157:
			case 163:
			case 168:
			case 175:
				sInfo2 = SetInfoText(11, 0);
				break;

			case  19:
				sInfo2 = "Data = Program Label Number";
				break;

			case  20:
				sInfo1 = "Data = 1 -> State = Integer";
				sInfo2 = "Data = 2 -> State = Variable Number";
				break;

			case  23:
				sInfo1 = "Data = 1 -> Mask  + State = Integers";
				sInfo2 = "Data = 2 -> Mask  = Variable Number";
				sInfo3 = "Data = 3 -> State = Variable Number";
				sInfo4 = "Data = 4 -> Mask  + State = Variables";
				break;

			case  28:
			case 162:
			case 155:
			case 164:
			case 103:
			case 169:
				sInfo2 = SetInfoText(0, 0);
				break;

			case  29:
				sInfo2 = "Data = 0 -> Stop and Disable Motor";
				sInfo3 = "Data = 1 -> Stop, Motor Enabled";
				break;

			case  31:
				sInfo1 = "Data selects Rate of Deceleration:";
				sInfo2 = "0 = Profile Acceleration";
				sInfo3 = "1 = Emergency (Quick)";
				sInfo4 = "2 = Emergency (Quick) + Program Stop";
				break;

			case  32:
				sInfo1 = "Data selects Motor State after Stop:";
				sInfo2 = "0 = Motor Enabled";
				sInfo3 = "1 = Motor Disabled";
				break;

			case  37:
				sInfo1 = "Data = 1 -> Index + Value = Integers";
				sInfo2 = "Data = 2 -> Index = Variable Number";
				sInfo3 = "Data = 3 -> Value = Variable Number";
				sInfo4 = "Data = 4 -> Index + Value = Variable Numbers";
				break;

			case  87:
			case 115:
			case 134:
			case 104:
			case 118:
			case 137:
			case 129:
			case 122:
			case 126:
			case 107:
			case 130:
				sInfo2 = SetInfoText(2, 0);
				break;

			case 142:
				sInfo1 = "Data = 1, Index = Integer";
				sInfo2 = "Data = 2, Index = Variable Number";
				break;

			case 160:
				sInfo2 = SetInfoText(3, 4);
				break;

			case 176:
				sInfo2 = SetInfoText(3, 3);
				break;

			case ERROP:
				sInfo1 = "Command not executed (32-bits):";
				sInfo2 = "High Word = Opcode Number";
				sInfo3 = "Low Word  = Fault Number";
				sInfo4 = SetInfoText(12, 0);
				break;

			case HIMM:
			case HSEQ:
				SetListMode();
				ResetHelp();
				break;

			default:
				sInfo2 = SetInfoText(11, 0);
				break;
			}
		}

	else {
		switch( uT ) {

			case 1:
			case 4:
				sInfo2 = SetInfoText(10, 0);
				break;

			case 2:
			case 5:
				sInfo2 = SetInfoText( 11, 0 );
				break;
			case 3:
			case 6:
				sInfo2 = SetInfoText( 11, 0 );
				sInfo4 = "(67 = User Var_01...76 = Var_10)";
				break;

			default:
				sInfo2 = "";
			}
		}

	GetDlgItem(2009).SetWindowText( sInfo1 );
	GetDlgItem(2009).EnableWindow(  sInfo1 != "" );

	GetDlgItem(2010).SetWindowText( sInfo2 );
	GetDlgItem(2010).EnableWindow(  sInfo2 != "" );

	GetDlgItem(2011).SetWindowText( sInfo3 );
	GetDlgItem(2011).EnableWindow(  sInfo3 != "" );

	GetDlgItem(2012).SetWindowText( sInfo4 );
	GetDlgItem(2012).EnableWindow(  sInfo4 != "" );
	}

CString CYaskawaFSPAddrDialog::SetInfoText(UINT uSel, UINT uPar)
{
	CString s = "";

	switch( uSel ){

		case 0:
			return "Data != 0 sends Command";

		case 2:
			s.Printf( "Data != 0 sends %s1 + %s2",
				m_pSpace->m_Prefix,
				m_pSpace->m_Prefix
				);

			break;

		case 3:
			s.Printf( "Data != 0 sends %s1 -> %s%1.1d",
				m_pSpace->m_Prefix,
				m_pSpace->m_Prefix,
				uPar
				);

			break;

		case 10:
			return "Read Only";

		case 11:
			return "(Write) Data = Value";

		case 12:
			return "(Write) ERROR = 0";
		}

	return s;
	}

void CYaskawaFSPAddrDialog::SetListMode(void)
{
	ClearDetails();
	ClearAddress();
	ResetHelp();
	GetDlgItem(2013).SetWindowText(m_pSpace->m_uTable == HIMM ? "IMMEDIATE" : "SEQUENTIAL");
	}

void CYaskawaFSPAddrDialog::ResetHelp(void)
{
	for( UINT i = 2009; i < 2013; i++ ) {

		GetDlgItem(i).SetWindowText("");
		GetDlgItem(i).EnableWindow(FALSE);
		}
	}

// End of File

