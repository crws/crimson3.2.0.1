
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RLC_HPP
	
#define	INCLUDE_RLC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Device Codes
//

#define DEVICE_C48		0
#define DEVICE_LEGEND		1
#define	DEVICE_PAX		2
#define	DEVICE_PAXI		3
#define DEVICE_TP48		4
#define DEVICE_TPCU		5
#define	DEVICE_CUB5		6 // Master Cub5 Selection
// Cub5 Sub Menu
#define	DEVICE_CUB5C		0
#define	DEVICE_CUB5TM		1
//#define	DEVICE_CUB5T	? // Temperature
//#define	DEVICE_CUB5S	? // Strain Gage
//#define	DEVICE_CUB5P	? // Process
//#define	DEVICE_CUB5H	? // AC
//#define	DEVICE_CUB5D	? // DC
//#define	DEVICE_CUB5R	? // Rate

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument
//

class CRlcDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CRlcDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument Device Options
//

class CRlcDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRlcDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);


		// Download Support	     
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Drop;
		BOOL	m_fProtocol;
		UINT	m_StdDlyRead;
		UINT	m_StdDlyWrite;
		UINT	m_IdxDlyRead;
		UINT	m_IdxDlyWrite;
		UINT	m_CmdDly;
		BOOL	m_fRspTime;
		UINT	m_Device;
		UINT	m_Cub5;

	protected:
			
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument Address Selection Dialog
//

class CRlcAddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRlcAddrDialog(CBasicCommsDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
		// Data Members
		CBasicCommsDriver * m_pDriver;
		CAddress          * m_pAddr;
		BOOL		    m_fPart;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnClear(UINT uID);

		// Implementation
		void ShowAddress(CAddress &Addr);
		void ShowInformation(void);
	};

// End of File

#endif
