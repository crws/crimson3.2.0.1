
#include "intern.hpp"

#include "unitelm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CUnitelDriverOptions, CUIItem);

// Constructor

CUnitelDriverOptions::CUnitelDriverOptions(void)
{
	m_Drop     = 6;
	m_Category = 0;
	}

// UI Management

void CUnitelDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CUnitelDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Drop));
	Init.AddWord(WORD(m_Category));

	return TRUE;
	}

// Meta Data Creation

void CUnitelDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Category);
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CUnitelSDriverOptions, CUIItem);

// Constructor

CUnitelSDriverOptions::CUnitelSDriverOptions(void)
{
	m_Drop     = 6;
	m_Category = 0;
	m_Master   = 0;
	}

// UI Management

void CUnitelSDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CUnitelSDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Drop));
	Init.AddWord(WORD(m_Category));
	Init.AddWord(WORD(m_Master));

	return TRUE;
	}

// Meta Data Creation

void CUnitelSDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Category);
	Meta_AddInteger(Master);
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CUnitelMasterDeviceOptions, CUIItem);

// Constructor

CUnitelMasterDeviceOptions::CUnitelMasterDeviceOptions(void)
{
	m_Drop     = 0;
	m_Category = 0;
	}

// UI Management

void CUnitelMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CUnitelMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Drop));

	Init.AddWord(WORD(m_Category));

	return TRUE;
	}

// Meta Data Creation

void CUnitelMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Category);
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Master
//

// Instantiator

ICommsDriver *	Create_UnitelwayMasterDriver(void)
{
	return New CUnitelwayMasterDriver;
	}

// Constructor

CUnitelwayMasterDriver::CUnitelwayMasterDriver(void)
{
	m_wID		= 0x3316;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Schneider - Telemechanique";
	
	m_DriverName	= "Uni-Telway Master";
	
	m_Version	= "1.30";
	
	m_ShortName	= "Uni-Telway Master";

	AddSpaces();
	}

// Binding Control

UINT CUnitelwayMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CUnitelwayMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CUnitelwayMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CUnitelDriverOptions);
	}

CLASS CUnitelwayMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUnitelMasterDeviceOptions);
	}

// Implementation

void CUnitelwayMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(10,"M",   "Internal Bits",		10,  0, 65535,	addrBitAsBit));
	AddSpace(New CSpace(8, "MB",  "Internal Bytes",		10,  0, 65535,	addrByteAsByte));
	AddSpace(New CSpace(1, "MW",  "Internal Words",		10,  0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(4, "MDW", "Internal Double Words",	10,  0, 65535,	addrLongAsLong));
	AddSpace(New CSpace(7, "MF",  "Internal Reals",		10,  0, 65535,	addrRealAsReal));
	AddSpace(New CSpace(11,"S",   "System Bits",		10,  0, 65535,	addrBitAsBit));
	AddSpace(New CSpace(9, "SB",  "System Bytes",		10,  0, 65535,	addrByteAsByte));
	AddSpace(New CSpace(2, "SW",  "System Words",		10,  0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(5, "SDW", "System Double Words",	10,  0, 65535,	addrLongAsLong));
	AddSpace(New CSpace(3, "KW",  "Constant Words",		10,  0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(6, "KDW", "Constant Double Words",	10,  0, 65535,	addrLongAsLong));
	AddSpace(New CSpace(12,"KF",  "Constant Reals",		10,  0, 65535,	addrRealAsReal));
	AddSpace(New CSpace(13,"TP",  "PL7 Timer Preset",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(14,"TV",  "PL7 Timer Value",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(15,"TR",  "PL7 Timer Running",	10,  0, 63,	addrByteAsByte));
	AddSpace(New CSpace(16,"TD",  "PL7 Timer Done",		10,  0, 63,	addrByteAsByte));
	AddSpace(New CSpace(17,"TMP", "IEC Timer Preset",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(18,"TMV", "IEC Timer Value",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(19,"TMQ", "IEC Timer Output",	10,  0, 63,	addrByteAsByte));
	AddSpace(New CSpace(20,"CP",  "Counter Preset",		10,  0, 31,	addrWordAsWord));
	AddSpace(New CSpace(21,"CV",  "Counter Value",		10,  0, 31,	addrWordAsWord));
	AddSpace(New CSpace(22,"CE",  "Counter Empty",		10,  0, 31,	addrByteAsByte));
	AddSpace(New CSpace(23,"CD",  "Counter Done",		10,  0, 31,	addrByteAsByte));
	AddSpace(New CSpace(24,"CF",  "Counter Full",		10,  0, 31,	addrByteAsByte));
	AddSpace(New CSpace(1, "W",   "Do Not Use-Conversion Tool",10,  0, 65535,addrWordAsWord));
	AddSpace(New CSpace(25,"P",   "Highest Slave Address for Master",10,  0, 0,addrWordAsWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Application Master / Network Slave
//

// Instantiator

ICommsDriver *	Create_UnitelwayMSDriver(void)
{
	return New CUnitelwayMSDriver;
	}

// Constructor

CUnitelwayMSDriver::CUnitelwayMSDriver(void)
{
	m_wID		= 0x4010;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Schneider - Telemechanique";
	
	m_DriverName	= "Uni-Telway Slave";
	
	m_Version	= "1.40";
	
	m_ShortName	= "Uni-Telway Slave";
	}

// Configuration

CLASS CUnitelwayMSDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CUnitelSDriverOptions);
	}

CLASS CUnitelwayMSDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUnitelMasterDeviceOptions);
	}

// End of File
