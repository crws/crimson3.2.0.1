
#include "intern.hpp"

#include "l5keip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Base Tag List
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kTagList, CABL5kList);

// Static Data

// Constructor

CABL5kTagList::CABL5kTagList(void)
{
	m_pConfig = NULL;

	m_pTypes  = NULL;
	}

// Destructor

CABL5kTagList::~CABL5kTagList(void)
{
	Kill();
	}

// Persistance

void CABL5kTagList::Init(void)
{
	CABL5kList::Init();
	
	FindManager();
	}

void CABL5kTagList::PostLoad(void)
{
	CABL5kList::PostLoad();

	FindManager();
	}

// Operations

BOOL CABL5kTagList::CreateTag(CError &Error, CString const &Name, CString const &Type, CString Info)
{
	if( m_pTypes->IsAtomicType(Type) ) {

		CABL5kAtomicTag *pCreate = New CABL5kAtomicTag(Name, Type, Info);

		AppendItem(pCreate);
				
		// dictionary management

		m_pConfig->AddAtomicTag(pCreate);

		return TRUE;
		}
	else {
		if( m_pTypes->IsArrayType(Type) ) {

			return ExpandArray(Error, Name, Type);
			}
		else 
			return ExpandType (Error, Name, Type, Info);
		}
	}

// Expansion Helpers

void CABL5kTagList::DimParse(CArray <UINT> &List, CString const &Type)
{
	UINT p1, p2;

	if( (p1 = Type.FindRev('[')) < NOTHING ) {

		if( (p2 = Type.Find(']', p1)) < NOTHING ) {
		
			CStringArray Dims;

			Type.Mid(p1 + 1, p2 - (p1 + 1)).Tokenize(Dims, ',');

			for( UINT n = Dims.GetCount(); n; n -- ) {
				
				List.Append(tatoi(Dims[n - 1]));
				}
			}
		}
	}

void CABL5kTagList::DimGetIndices(CArray <UINT> const &Dims, CArray <UINT> &List, UINT uIndex)
{
	if( Dims.GetCount() ) {

		CString Text;

		DimExpand(Dims, uIndex, Text);

		DimParse(List, Text);
		}
	}

UINT CABL5kTagList::DimGetIndex(CArray <UINT> const &Dims, CArray <UINT> const &List)
{
	switch( Dims.GetCount() ) {

		case 3:			
			return List[0] +  List[1] * Dims[0] + List[2] * (Dims[1] * Dims[0]);

		case 2:
			return List[0] + (List[1] * Dims[0]);

		case 1:
			return List[0];

		default:
			return 0;
		}
	}

UINT CABL5kTagList::DimGetSize(CArray <UINT> const &Dims)
{
	UINT uSize = 1;

	for( UINT n = 0; n < Dims.GetCount(); n ++ ) {
		
		uSize *= Dims[n];
		}

	return uSize;
	}

BOOL CABL5kTagList::DimExpand(CArray <UINT> const &Dims, UINT uIndex, CString &Text)
{
	if( Dims.GetCount() ) {

		Text += "[";

		switch( Dims.GetCount() ) {
			
			case 3:
				Text += CPrintf("%d,", uIndex / (Dims[1] * Dims[0]));

				uIndex = uIndex % (Dims[1] * Dims[0]);
			
			case 2:
				Text += CPrintf("%d,", uIndex / Dims[0]);
			
			case 1:
				Text += CPrintf("%d", uIndex % Dims[0]);
				break;
			}

		Text += "]";
		
		return TRUE;
		}

	return FALSE;
	}

// Debug

void CABL5kTagList::ShowList(void)
{
	CABL5kList::ShowList(this);
	}

// Overridables

BOOL CABL5kTagList::LoadConfig(CError &Error, CABL5kController &Config)
{
	Kill();

	CArray <CABTypeDef> &Tags = Config.m_Tags.m_Members;

	if( Tags.GetCount() ) {

		CMap <CString, CString> List;

		UINT uCount = Tags.GetCount();

		for( UINT n = 0; n < uCount; n ++ ) {

			CABTypeDef &Tag = (CABTypeDef &) Tags[n];

			if( Tag.m_fAlias ) {
				
				CString Name = Tag.m_Name;

				CString Type = Tag.m_Type;
				
				List.Insert(Name, Type);
				}
			else {
				CString Name = Tag.m_Name;

				CString Type = Tag.m_Type;

				ShowStatus(CPrintf("Expanding Data Tag %d of %d, %s", n, uCount, Name));
				
				if( FindItem(Name) ) {
					
					Error.Set(CPrintf("Duplicate tag name <%s>.", Name), 0);

					ThrowError();
					}

				if( !CreateTag(Error, Name, Type) ) {

					Error.Set(CPrintf("Failed to create tag name <%s>.", Name), 0);
					
					ThrowError();
					}			
				}
			}

		ResolveAliasTags(Error, List);

		afxThread->SetStatusText("");
		
		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CABL5kTagList::ResolveAliasTags(CError &Error, CMap <CString, CString> &List)
{
	static UINT uDepth = 0;
	
	CMap <CString, CString> Fail;	
	
	while( List.GetCount() ) {
		
		INDEX   Index = List.GetHead();

		if( !List.Failed(Index) ) {

			CString Alias = List.GetName(Index);

			CString  Name = List.GetData(Index);

			ShowStatus(CPrintf("Resolving Alias Tag %s", Name));
			
			CABL5kItem *pItem;

			if( (pItem = FindItem(Name)) ) {

				CString Type = pItem->GetType();

				UINT p1;

				if( (p1 = Type.Find('[')) < NOTHING ) {
					
					Type = Type.Left(p1);
					}

				CreateTag(Error, Alias, Type, Name);
				}
			else {
				if( uDepth > 3 ) {

					CString Type = "DINT";

					CreateTag(Error, Alias, Type, Name);
					}
				else {
					Fail.Insert(Name, Alias);
					}				
				}

			List.Remove(Index);
			}
		}

	if( Fail.GetCount() ) {

		uDepth ++;

		ResolveAliasTags(Error, Fail);
		}
	}

void CABL5kTagList::FindManager(void)
{
	CItem *pItem = GetParent();

	while( pItem ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CABL5kDeviceOptions)) ) {
			
			break;
			}
		
		pItem =	pItem->GetParent();
		}

	m_pConfig = (CABL5kDeviceOptions *) pItem;

	m_pTypes  = m_pConfig->m_pTypes;
	}

BOOL CABL5kTagList::ExpandType(CError &Error, CString Name, CString Type, CString Info)
{
	AfxAssert(!m_pTypes->IsAtomicType(Type));

	CABL5kStructTag *pRoot = New CABL5kStructTag(Name, Type, Info);

	AppendItem(pRoot);

	return ExpandType(Error, pRoot);
	}

BOOL CABL5kTagList::ExpandType(CError &Error, CABL5kStructTag *pItem)
{
	INDEX Index = m_pTypes->FindName(pItem->GetType());

	if( !m_pTypes->Failed(Index) ) {

		CABL5kTagList *pTags = (CABL5kTagList *) pItem->GetElements();

		CABL5kList   *pTypes = m_pTypes->GetItem(Index)->GetElements();

		INDEX i = pTypes->GetHead();

		while( !pTypes->Failed(i) ) {
			
			CString Name = pTypes->GetName(i);

			CString Type = pTypes->GetType(i);
			
			if( !pTags->CreateTag(Error, Name, Type) ) {
				
				return FALSE;
				}
			
			pTypes->GetNext(i);
			}
		
		return TRUE;
		}

	Error.Set(CPrintf("Cannot find data type [%s].", pItem->GetType()), 0);

	DeleteItem(pItem);
	
	ThrowError();
	
	return FALSE;
	}

BOOL CABL5kTagList::ExpandArray(CError &Error, CString Name, CString Type)
{
	AfxAssert(!m_pTypes->IsAtomicType(Type));

	CABL5kStructTag *pRoot = New CABL5kStructTag(Name, Type);

	AppendItem(pRoot);

	return ExpandArray(Error, pRoot);
	}

BOOL CABL5kTagList::ExpandArray(CError &Error, CABL5kStructTag *pItem)
{
	CString Type = pItem->GetType();

	CString Name = pItem->GetName();

	CArray <UINT> List;
	
	DimParse(List, Type);

	if( List.GetCount() ) {

		UINT p1;
		
		if( (p1 = Type.Find('[')) < NOTHING ) {
			
			Type = Type.Left(p1);
			}		
		
		CABL5kTagList *pElems = (CABL5kTagList *) pItem->GetElements();

		UINT uSize = DimGetSize(List);
		
		for( UINT n = 0; n < uSize; n ++ ) {
			
			CString Index;

			DimExpand(List, n, Index);

			if( !pElems->CreateTag(Error, Name + Index, Type) ) {
				
				return FALSE;
				}
			}

		return TRUE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

void CABL5kTagList::ShowStatus(CString Text)
{
	afxThread->SetStatusText(Text);
	}

// debug

void CABL5kTagList::ShowList(CArray <CNameIndex> const &List)
{
	UINT uCount = List.GetCount();

	for( UINT n = 0; n < uCount; n ++ ) {

		AfxTrace("[%d]<%s>\n", List[n].m_uIndex, List[n].m_Name);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Data Type
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kAtomicTag, CABL5kAtomicItem);

// Constructor

CABL5kAtomicTag::CABL5kAtomicTag(void)
{
	}

CABL5kAtomicTag::CABL5kAtomicTag(CString const &Name, CString const &Type, CString Info)
{
	m_Name = Name;

	m_Type = Type;

	m_Info = Info;
	}

// Operations

CString CABL5kAtomicTag::GetFullName(void)
{
	CString Name;

	CItem *pItem = this;

	for( UINT n = 0; pItem; n ++ ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CABL5kItem)) ) {

			CABL5kItem *p = (CABL5kItem *) pItem;
			
			if( p->IsAtomic() ) {

				if( n )
					Name.Insert(0, ".");
				
				Name.Insert(0, p->GetName());				
				}

			if( p->IsStruct() && !p->IsArray() ) {

				if( n )
					Name.Insert(0, ".");
				
				Name.Insert(0, p->GetName());					
				}			
			}
		
		pItem = pItem->GetParent(2);

		if( pItem->IsKindOf(AfxRuntimeClass(CABL5kDeviceOptions)) ) {
			
			break;
			}
		}

	return Name;	
	}

// Meta Data Creation

void CABL5kAtomicTag::AddMetaData(void)
{
	CABL5kAtomicItem::AddMetaData();

	Meta_AddString(Info);
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Structure Data Type
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kStructTag, CABL5kStructItem);

// Constructor

CABL5kStructTag::CABL5kStructTag(void)
{
	m_pElements = New CABL5kTagList;
	}

CABL5kStructTag::CABL5kStructTag(CString Name, CString Type)
{
	m_Name = Name;

	m_Type = Type;
	
	m_pElements = New CABL5kTagList;
	}

CABL5kStructTag::CABL5kStructTag(CString Name, CString Type, CString Info)
{
	m_Name = Name;

	m_Type = Type;

	m_Info = Info;
	
	m_pElements = New CABL5kTagList;
	}

// Meta Data Creation

void CABL5kStructTag::AddMetaData(void)
{
	CABL5kStructItem::AddMetaData();

	Meta_AddString(Info);
	}

// End of File
