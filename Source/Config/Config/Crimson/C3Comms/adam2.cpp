
#include "intern.hpp"

#include "adam2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series v2 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CAdam4000v2DeviceOptions, CUIItem);
				   
// Constructor

CAdam4000v2DeviceOptions::CAdam4000v2DeviceOptions(void)
{
	m_Drop	   = 1;

	m_pCmds	   = new CAdmCmdArray;

	m_pMdls	   = new CAdmMdlArray;

	m_uTypes   = 0;

	m_uLastRef = 0;

	m_uLastTxt = 0;

	m_Model    = 0;
	}

// Destructor

CAdam4000v2DeviceOptions::~CAdam4000v2DeviceOptions(void)
{
	delete m_pCmds;

	delete m_pMdls;

	m_CmdList.Empty();

	m_SelList.Empty();
	}

// UI Loading

BOOL CAdam4000v2DeviceOptions::OnLoadPages(CUIPageList * pList)
{ 
	InitParams();

	CAdam4000v2DeviceOptionsUIPage * pPage = New CAdam4000v2DeviceOptionsUIPage(this);

	pList->Append(pPage);
	
	return TRUE;			   
	}

// UI Management

void CAdam4000v2DeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag == L"Model" ) {

		ApplyFilter();
		}
	}

// Cmd List Access

CStringArray CAdam4000v2DeviceOptions::GetCmdTypes(void)
{
	return m_SelTypes;
	}

CStringArray CAdam4000v2DeviceOptions::GetCmdList(UINT uType)
{
	if( uType < m_SelTypes.GetCount() ) {

		return m_SelList[uType];
		}
	
	CStringArray Empty;

	return Empty;
	}

CAddress CAdam4000v2DeviceOptions::GetAddress(CString Text)
{
	if( !m_pCmds->IsEmpty() ) {

		UINT uCount = m_pCmds->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			if( m_pCmds->GetAt(u).m_Text == Text ) {

				return m_pCmds->GetAt(u).m_Addr;
				}
			}
		}

	CAdmCmd * pCmd = AddCmd(Text);

	if( pCmd ) {

		return pCmd->m_Addr;
		}

	CAddress Addr;

	Addr.m_Ref = 0;

	return Addr;
	}

CString CAdam4000v2DeviceOptions::GetText(DWORD dwRef)
{
	if( !m_pCmds->IsEmpty() ) {

		UINT uCount = m_pCmds->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			if( m_pCmds->GetAt(u).m_Addr.m_Ref == dwRef ) {

				return m_pCmds->GetAt(u).m_Text;
				}
			}
		}

	CString Empty;

	return Empty;
	}

CAdmCmd * CAdam4000v2DeviceOptions::GetCommand(CAddress Addr)
{
	if( !m_pCmds->IsEmpty() ) {

		UINT uCount = m_pCmds->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			if( m_pCmds->GetAt(u).m_Addr.m_Ref ==  Addr.m_Ref ) {

				return (CAdmCmd *) &m_pCmds->GetAt(u);
				}
			}
		}

	return NULL;
	}

CString	CAdam4000v2DeviceOptions::GetSelectListElement(UINT uType, UINT uElement)
{
	if( uType < m_SelTypes.GetCount() ) {

		return FindLineElement(m_SelTypes.GetAt(uType), uElement);
		}

	CString Empty;

	return Empty;
	}

CString	CAdam4000v2DeviceOptions::GetSelectListElement(UINT uType, UINT uCmd, UINT uElement)
{
	if( uType < m_SelTypes.GetCount() ) {

		if( uCmd < m_SelList[uType].GetCount() ) {

			return FindLineElement(m_SelList[uType].GetAt(uCmd), uElement);
			}
		}

	CString Empty;

	return Empty;
	}

void CAdam4000v2DeviceOptions::GetSelectIndexes(CString Text, UINT &uType, UINT &uCmd)
{
	UINT uFind = Text.Find('_');

	if( uFind < NOTHING ) {

		CString Type = Text.Mid(0, uFind);

		UINT uTypes = m_SelTypes.GetCount();

		for( UINT t = 0; t < uTypes; t++ ) {

			if( Type == FindLineElement(m_SelTypes.GetAt(t), defCmd) ) {

				uType = t;

				Text = Text.Mid(uFind + 1);

				uFind = Text.Find('.');

				CString Cmd = uFind < NOTHING ? Text.Mid(0, uFind) : Text;

				uFind = Cmd.Find('_');

				if( uFind < NOTHING ) {

					Cmd = Cmd.Mid(0, uFind);
					}

				UINT uCount = m_SelList[t].GetCount();
					
				for( UINT c = 0; c < uCount; c++ ) {

					if( Cmd == FindLineElement(m_SelList[t].GetAt(c), defCmd) ) {

						uCmd = c;

						break;
						}
					}
				}
			}

		return;
		}
	}

BYTE CAdam4000v2DeviceOptions::GetCommandForm(CString Text)
{
	if( Text == L"TEXT" ) {

		return formText;
		}

	if( Text == L"HEX" ) {

		return formHex;
		}

	if( Text == L"CMD" ) {

		return formCmd;
		}

	if( !Text.IsEmpty() ) {

		return formDigitM | (tstrtol(Text, NULL, 10) & 0xF);
		}

	return formInt;
	}

BYTE CAdam4000v2DeviceOptions::GetCommandForm(CAddress Addr)
{
	if( !m_pCmds->IsEmpty() ) {

		UINT uCount = m_pCmds->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			if( m_pCmds->GetAt(u).m_Addr.m_Ref == Addr.m_Ref ) {

				if( m_pCmds->GetAt(u).m_bForm == formHex ) {

					if( Addr.a.m_Type == addrRealAsReal ) {

						return formReal;
						}
					}						

				return m_pCmds->GetAt(u).m_bForm;
				}
			}
		}	

	return formInt;
	}

void CAdam4000v2DeviceOptions::EmptyCmdList(void)
{
	if( m_pCmds ) {

		m_pCmds->Empty();

		m_uLastRef = 0;

		m_uLastTxt = 0;
		}
	}

void CAdam4000v2DeviceOptions::InitParams(void)
{
	InitModels();

	InitCmdList();
	}

// Model List Access

CString CAdam4000v2DeviceOptions::GetModelEnum(void)
{
	CString Models;

	if( !m_pMdls->IsEmpty() ) {

		UINT uCount = m_pMdls->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			Models += m_pMdls->GetAt(u).m_Model;

			Models += "|";
			}
		}

	Models += CString(IDS_GENERIC);

	return Models;
	}

// Persistance

void CAdam4000v2DeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"Cmds" ) {

			Tree.GetObject();

			Tree.GetName();

			UINT uCount = Tree.GetValueAsInteger();

			for( UINT u = 0; u < uCount; u++ ) {

				CAdmCmd ac;

				Tree.GetCollect();

				do {
					Name = Tree.GetName();

					if( Name == L"Read" ) {

						ac.m_pRead = Tree.GetValueAsString();
						}

					else if( Name == L"Write" ) {

						ac.m_pWrite = Tree.GetValueAsString();
						}

					else if( Name == L"CmdType" ) {

						ac.m_bCmdType = BYTE(Tree.GetValueAsInteger());
						}

					else if( Name == L"Cmd" ) {
											
						ac.m_bCmd = BYTE(Tree.GetValueAsInteger());
						}

					else if( Name == L"Chan" ) {

						ac.m_bChan = BYTE(Tree.GetValueAsInteger());
						}

					else if( Name == L"Form" ) {

						ac.m_bForm = BYTE(Tree.GetValueAsInteger());
						}

					else if( Name == L"Misc" ) {

						ac.m_bMisc = BYTE(Tree.GetValueAsInteger());
						}

					else if( Name == L"Addr" ) {

						ac.m_Addr.m_Ref	= Tree.GetValueAsInteger();
						}

					else if( Name == L"Text" ) {

						ac.m_Text = Tree.GetValueAsString();
						}

					} while ( !Tree.IsEndOfData() );

				m_pCmds->Append(ac);

				Tree.EndCollect();
				}

			Tree.EndObject();
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CAdam4000v2DeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutObject(L"Cmds");

	UINT uCount = m_pCmds->GetCount();

	Tree.PutValue(L"AdmCmds", uCount);

	for( UINT u = 0; u < uCount; u++ ) {

		Tree.PutCollect(L"Cmd");

		Tree.PutValue(L"Read",	  m_pCmds->GetAt(u).m_pRead);

		Tree.PutValue(L"Write",   m_pCmds->GetAt(u).m_pWrite);

		Tree.PutValue(L"CmdType", m_pCmds->GetAt(u).m_bCmdType);

		Tree.PutValue(L"Cmd",	  m_pCmds->GetAt(u).m_bCmd);

		Tree.PutValue(L"Chan",	  m_pCmds->GetAt(u).m_bChan);

		Tree.PutValue(L"Form",	  m_pCmds->GetAt(u).m_bForm);

		Tree.PutValue(L"Misc",	  m_pCmds->GetAt(u).m_bMisc);

		Tree.PutValue(L"Addr",	  m_pCmds->GetAt(u).m_Addr.m_Ref);

		Tree.PutValue(L"Text",	  m_pCmds->GetAt(u).m_Text);

		Tree.EndCollect();
		}

	Tree.EndObject();
	}

void CAdam4000v2DeviceOptions::PostLoad(void)
{
	CAddress Addr;
	
	UINT uCount = m_pCmds->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		Addr.m_Ref = m_pCmds->GetAt(u).m_Addr.m_Ref;

		if( Addr.a.m_Table == addrNamed ) {

			MakeMax(m_uLastRef, Addr.a.m_Offset);
			}
		else {
			MakeMax(m_uLastTxt, Addr.m_Ref);
			}
		}
	}


// Download Support

BOOL CAdam4000v2DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	UINT uCount = m_pCmds->GetCount();

	Init.AddWord(WORD(uCount));

	for( UINT u = 0; u < uCount; u++ ) {

		CAdmCmd ac = m_pCmds->GetAt(u);

		Init.AddText(ac.m_pRead);

		Init.AddText(ac.m_pWrite);

		Init.AddByte(BYTE(ac.m_bChan));

		Init.AddByte(BYTE(ac.m_bForm));

		Init.AddByte(BYTE(ac.m_bMisc));

		Init.AddLong(LONG(ac.m_Addr.m_Ref));
		}
	
	return TRUE;
	}

// Meta Data Creation

void CAdam4000v2DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Model);
	}

// Implementation

void CAdam4000v2DeviceOptions::InitModels(void)
{
	if( m_pMdls->IsEmpty() ) {

		CFilename File = afxModule->GetFilename();

		File.ChangeName(L"schema\\40CA.csv");

		CTextStreamMemory Stream;

		if( !Stream.LoadFromFile(File) ) {

			return;
			}

		CStringArray Match;

		Match.Append(L"<Module><CommandArray>");

		Match.Append(L"<GroupDefs><Prefix><Name>");
		
		WCHAR sLine[512] = {0};

		CString Line;

		Stream.SetSpecial();

		Stream.SetPrev();

		while( Stream.GetLine(sLine, elements(sLine) ) ) {

			Line += sLine;

			CString Start = Line.Mid(0, Match.GetAt(0).GetLength());

			if( Start == Match.GetAt(0) ) {

				Line.Empty();

				while( Stream.GetLine(sLine, elements(sLine) ) ) {

					Line += sLine;

					UINT uMatch = Match.GetAt(1).GetLength();

					CString End = Line.Mid(Line.GetLength() - uMatch - 2, uMatch);

					if( End == Match.GetAt(1) ) {
					
						break;
						}
					else {
						UINT uFind = Line.Find(',');

						if( uFind < NOTHING ) {

							CAdmMdl Mdl;

							Mdl.m_Model  = Line.Mid(0, uFind);

							Line = Line.Mid(uFind + 1);

							Line.Tokenize(Mdl.m_Cmds, ',');

							m_pMdls->Append(Mdl);
							}						

						Line.Empty();
						}
					}
				}
			}

		Stream.Close();
		}
	}

void CAdam4000v2DeviceOptions::InitCmdList(void)
{
	if( !m_CmdList.GetCount() ) {

		CFilename File = afxModule->GetFilename();

		File.ChangeName(L"schema\\40CA.csv");

		CTextStreamMemory Stream;

		if( !Stream.LoadFromFile(File) ) {

			return;
			}

		CStringArray Match;

		Match.Append(L"<GroupDefs><Prefix><Name>");

		Match.Append(L"<Group><Prefix><Name><ReadSyntax><WriteSyntax><Type><Form><Misc>");
		
		WCHAR sLine[512] = {0};

		BOOL fEnd = FALSE;

		CString Line;

		Stream.SetSpecial();

		Stream.SetPrev();

		while( Stream.GetLine(sLine, elements(sLine) ) ) {

			Line += sLine;

			CString Start = Line.Mid(0, Match.GetAt(0).GetLength());

			if( !fEnd ) {
				
				if( Start == Match.GetAt(0) ) {

					UINT uMatch = Match.GetAt(1).GetLength();

					CString End = Line.Mid(Line.GetLength() - uMatch - 2, uMatch);

					if( End == Match.GetAt(1) ) {

						PrepareList(Line);

						Line.Tokenize(m_CmdTypes, L",");

						m_CmdTypes.Remove(0);

						m_CmdTypes.Remove(m_CmdTypes.GetCount() - 1);

						fEnd = TRUE;

						Line.Empty();
						}
					}
				else {
					Line.Empty();
					}
				}
			}

		if( !m_CmdTypes.IsEmpty() ) {

			m_uTypes = m_CmdTypes.GetCount();

			CStringArray Array;

			PrepareList(Line);

			Line.Tokenize(Array, L",");

			LoadCmdList(Array, m_uTypes);
			}
		
		Stream.Close();
		}
	}

void CAdam4000v2DeviceOptions::ApplyFilter(void)
{
	MakeSelTypes();

	MakeSelCmdList();
	}

void CAdam4000v2DeviceOptions::PrepareList(CString &Line)
{
	UINT uFind = Line.Find(L"><");

	if( uFind < NOTHING ) {

		Line  = Line.Mid(uFind + 1);

		uFind = Line.Find(L"><");

		if( uFind < NOTHING ) {

			Line.Insert(uFind + 1, L"\t");
			}
		}

	Line.Replace(L",", L"\t");

	Line.Replace(L"\n", L",");
	}

void CAdam4000v2DeviceOptions::LoadCmdList(CStringArray Array, UINT uCmds)
{
	if( !m_CmdList.GetCount() ) {

		UINT uCount = Array.GetCount();

		CString Cmd;

		for( UINT c = 0, u = 0; c < uCmds; c++ ) {

			CStringArray Entry;

			for( ; u < uCount; u++ ) {

				CString Line = Array.GetAt(u);

				UINT uFind = Line.Find(L"\t");

				if( uFind < NOTHING ) {

					if( Cmd.IsEmpty() ) {

						Cmd = Line.Mid(0, uFind);
						}

					if( Cmd == Line.Mid(0, uFind) ) {

						Line = Line.Mid(uFind + 1);

						Entry.Append(Line);

						continue;
						}
						
					Cmd = Line.Mid(0, uFind);

					break;
					}
				}

			m_CmdList.Append(Entry);
			}
		
		ApplyFilter();
		}
	}

CAdmCmd * CAdam4000v2DeviceOptions::AddCmd(CString Text)
{
	CString CmdType, Cmd, Chan, Type;
	
	if( ParseCommand(Text, CmdType, Cmd, Chan, Type) ) {

		CAdmCmd ac;

		ac.m_Text     = Text;

		ac.m_bCmdType = defNothing;

		ac.m_bCmd     = defNothing;

		ac.m_bChan    = defNothing;

		UINT uCmdTypes = m_CmdTypes.GetCount();

		for( BYTE t = 0; t < uCmdTypes; t++ ) {

			if( CmdType == GetCommandListElement(t, defCmd) ) {

				ac.m_bCmdType = t;

				UINT uCmds = m_CmdList[t].GetCount();

				for( BYTE c = 0; c < uCmds; c++ ) {

					if( Cmd == GetCommandListElement(t, c, defCmd) ) {

						ac.m_bCmd = c;

						if( !Chan.IsEmpty() ) {

							ac.m_bChan = tstrtol(Chan, NULL, 10) & 0xFF;
							}

						ac.m_pRead  = GetCommandListElement(t, c, defRead);

						ac.m_pWrite = GetCommandListElement(t, c, defWrite);

						UINT uFind = Text.Find('.');

						if( uFind < NOTHING ) {

							Text = Text.Mid(uFind + 1);
							}

						CString Form = GetCommandListElement(t, c, defForm);

						ac.m_bForm = GetCommandForm(Form);

						ac.m_bMisc = watoi(GetCommandListElement(t, c, defFormat)) & 0xFF;

						if( SetAddress(ac.m_bForm == formText ? Form : Text, ac.m_Addr) ) {

							m_pCmds->Append(ac);

							return (CAdmCmd *)&m_pCmds->GetAt(m_pCmds->GetCount() - 1);
							}
						}
					}
				}
			}
		}
	
	return NULL;
	}

BOOL CAdam4000v2DeviceOptions::ParseCommand(CString Text, CString &CmdType, CString &Cmd, CString &Chan, CString &Type)
{
	CString Work = Text;

	UINT uFind = Work.Find('.');

	if( uFind < NOTHING ) {

		Type = Work.Mid(uFind + 1);

		Work = Work.Mid(0, uFind);
		}

	uFind = Work.Find('_');

	if( uFind < NOTHING ) {

		CmdType = Work.Mid(0, uFind);

		Work = Work.Mid(uFind + 1);

		uFind = Work.Find('_');

		if( uFind < NOTHING ) {

			Cmd  = Work.Mid(0, uFind);

			Chan = Work.Mid(uFind + 1);
			}
		else {
			Cmd = Work;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CAdam4000v2DeviceOptions::SetAddress(CString Type, CAddress &Addr)
{
	Type.ToUpper();

	if( Type == L"TEXT" ) {

		return SetTextAddr(Addr);
		}

	return SetNamedAddr(Type, Addr);
	}

BOOL CAdam4000v2DeviceOptions::SetNamedAddr(CString Type, CAddress &Addr)
{
	m_uLastRef++;

	Addr.m_Ref	= m_uLastRef;
	
	Addr.a.m_Table  = addrNamed;

	Addr.a.m_Type   = addrLongAsLong;

	Type.ToUpper();

	UINT uFind = Type.Find('|');

	uFind++;

	if( Type.Mid(uFind) == L"BIT" ) {

		Addr.a.m_Type = addrBitAsBit;
		}
	
	else if( Type.Mid(uFind) == L"BYTE" || Type.Mid(uFind) == L"CHAR" ) {

		Addr.a.m_Type = addrByteAsByte;
		}

	else if( Type.Mid(uFind) == L"WORD" ) {

		Addr.a.m_Type = addrWordAsWord;
		}

	else if( Type.Mid(uFind) == L"REAL" ) {

		Addr.a.m_Type = addrRealAsReal;
		}

	return TRUE;
	}

BOOL CAdam4000v2DeviceOptions::SetTextAddr(CAddress &Addr)
{
	Addr.m_Ref    = m_uLastTxt;

	Addr.a.m_Type = addrLongAsLong;

	Addr.a.m_Offset += 85;

	if( Addr.a.m_Offset == 0 ) {

		Addr.a.m_Table++;
		}

	m_uLastTxt = Addr.m_Ref;
	
	return TRUE;
	}

CString	CAdam4000v2DeviceOptions::GetCommandListElement(UINT uType, UINT uElement)
{
	if( uType < m_CmdTypes.GetCount() ) {

		return FindLineElement(m_CmdTypes.GetAt(uType), uElement);
		}

	CString Empty;

	return Empty;
	}

CString	CAdam4000v2DeviceOptions::GetCommandListElement(UINT uType, UINT uCmd, UINT uElement)
{
	if( uType < m_CmdTypes.GetCount() ) {

		if( uCmd < m_CmdList[uType].GetCount() ) {

			return FindLineElement(m_CmdList[uType].GetAt(uCmd), uElement);
			}
		}

	CString Empty;

	return Empty;
	}

CString CAdam4000v2DeviceOptions::FindLineElement(CString Line, UINT uElement, PCTXT pDel)
{
	CString Find;

	for( UINT e = 0; e < uElement; e++ ) {

		UINT uFind = Line.Find(pDel);

		if( uFind < NOTHING ) {

			Find = Line.Mid(0, uFind);

			Line = Line.Mid(uFind + 1);

			continue;
			}

		uFind = Line.Find(L"\0");

		if( uFind < NOTHING ) {

			return Line;
			}
		}

	return Find;
	}

void CAdam4000v2DeviceOptions::MakeSelTypes(void)
{
	if( m_pMdls ) {

		m_SelTypes.Empty();

		if( m_Model < m_pMdls->GetCount() ) {

			CAdmMdl Sel = m_pMdls->GetAt(m_Model);

			UINT uSels  = Sel.m_Cmds.GetCount();

			MakeSelType(L"CMD");

			for( UINT u = 0; u < uSels; u++ ) {

				CString Type = FindLineElement(Sel.m_Cmds.GetAt(u), defCmd, L"_");

				BOOL fMake = TRUE;

				if( u > 0 ) {

					UINT uTypes = m_SelTypes.GetCount();

					for( UINT t = 0; t < uTypes; t++ ) {

						if( Type == FindLineElement(m_SelTypes.GetAt(t), defCmd) ) {

							fMake = FALSE;

							break;
							}
						}
					}

				if( fMake ) {

					MakeSelType(Type);
					}
				}
			}
		}

	if( m_SelTypes.IsEmpty() ) {

		MakeAllSelTypes();
		}
	}

void CAdam4000v2DeviceOptions::MakeSelType(CString Type)
{
	UINT uTypes  = m_CmdTypes.GetCount();

	for( UINT t = 0; t < uTypes; t++ ) {

		if( GetCommandListElement(t, defCmd) == Type ) {

			m_SelTypes.Append(m_CmdTypes.GetAt(t));

			return;
			}
		}
	}

void CAdam4000v2DeviceOptions::MakeAllSelTypes(void)
{
	m_SelTypes.Empty();
	
	UINT uTypes  = m_CmdTypes.GetCount();

	for( UINT t = 0; t < uTypes; t++ ) {

		m_SelTypes.Append(m_CmdTypes.GetAt(t));
		}
	}

void CAdam4000v2DeviceOptions::MakeSelCmdList(void)
{
	EmptySelList();

	if( m_pMdls ) {

		if( m_Model < m_pMdls->GetCount() ) {

			UINT uTypes = m_SelTypes.GetCount();

			if( uTypes > 0 ) {

				for( UINT u = 0; u < uTypes; u++ ) {

					CAdmMdl Sel = m_pMdls->GetAt(m_Model);

					UINT uSels  = u == 0 ? m_CmdList[u].GetCount() : Sel.m_Cmds.GetCount();

					for( UINT s = 0; s < uSels; s++ ) {

						if( u == 0 ) {

							MakeSelCmd(u, FindLineElement(m_CmdList[u].GetAt(s), defCmd));

							continue;
							}

						CString Cmd = FindLineElement(Sel.m_Cmds.GetAt(s), defCmd, PCTXT(L"_"));

						if( Cmd == FindLineElement(m_SelTypes.GetAt(u), defCmd) ) {

							Cmd = FindLineElement(Sel.m_Cmds.GetAt(s), defDesc, PCTXT(L"_"));

							MakeSelCmd(u, Cmd);
							}
						}
					}
				}
			}	
		}

	if( !m_SelList.GetCount() ) {

		MakeAllCmds();
		}
	}

void CAdam4000v2DeviceOptions::MakeSelCmd(UINT uType, CString Cmd)
{
	for( UINT t = 0; t < m_uTypes; t++ ) {

		UINT uCmds = m_CmdList[t].GetCount();

		for( UINT c = 0; c < uCmds; c++ ) {

			if( Cmd == GetCommandListElement(t, c, defCmd) ) {

				CStringArray Array;

				if( m_SelList.GetCount() > uType ) {

					Array = m_SelList[uType];

					m_SelList.Remove(uType);
					}

				Array.Append(m_CmdList[t].GetAt(c));

				m_SelList.Append(Array);

				return;
				}
			}
		}
	}


void CAdam4000v2DeviceOptions::MakeAllCmds(void)
{
	EmptySelList();

	for( UINT t = 0; t < m_uTypes; t++ ) {

		m_SelList.Append(m_CmdList[t]);
		}
	}

void CAdam4000v2DeviceOptions::EmptySelList(void)
{
	m_SelList.Empty();
	}

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series v2 Device Options UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CAdam4000v2DeviceOptionsUIPage, CUIPage);

// Constructor

CAdam4000v2DeviceOptionsUIPage::CAdam4000v2DeviceOptionsUIPage(CAdam4000v2DeviceOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CAdam4000v2DeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_DEVICE), 1);

	pView->AddUI(pItem, L"root", L"Drop");

	CUIData Model;

	Model.m_Tag	 = "Model";

	Model.m_Label 	 = CString(IDS_MODEL);

	Model.m_ClassText = AfxNamedClass(L"CUITextEnum");

	Model.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

	Model.m_Format	 = m_pOption->GetModelEnum();

	Model.m_Tip	 = CString(IDS_FORMAT);
				
	pView->AddUI(pItem, L"root", &Model);

	pView->EndGroup(TRUE);

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Module v2 Driver
//

// Instantiator

ICommsDriver * Create_Adam4000v2Driver(void)
{
	return New CAdam4000v2Driver;
	}

// Constructor

CAdam4000v2Driver::CAdam4000v2Driver(void)
{
	m_wID		= 0x40CA;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Adam";
	
	m_DriverName	= "4000 Series Module";
	
	m_Version	= "2.00";
	
	m_ShortName	= "4000 Series";

	m_DevRoot	= "MOD";
	}

// Binding Control

UINT CAdam4000v2Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CAdam4000v2Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}


// Configuration

CLASS CAdam4000v2Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CAdam4000v2DeviceOptions);
	}

// Notifications

void CAdam4000v2Driver::NotifyInit(CItem * pConfig)
{
	CAdam4000v2DeviceOptions * pOpt = (CAdam4000v2DeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->EmptyCmdList();
		}
	}

// Address Management

BOOL CAdam4000v2Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CAdam4000v2DeviceOptions * pOpt = (CAdam4000v2DeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->InitParams();

		CAdam4000v2AddrDialog Dlg(*this, Addr, pConfig);
	
		return Dlg.Execute(CWnd::FromHandle(hWnd));
		}

	return FALSE;
	} 

// Address Helpers

BOOL CAdam4000v2Driver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CAdam4000v2DeviceOptions * pOpt = (CAdam4000v2DeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->InitParams();

		Addr = pOpt->GetAddress(Text);

		if( Addr.m_Ref > 0 ) {

			return TRUE;
			}
		}

	Error.Set("Invalid Command", 0);

	return FALSE;
	}

BOOL CAdam4000v2Driver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CAdam4000v2DeviceOptions * pOpt = (CAdam4000v2DeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->InitParams();

		Text = pOpt->GetText(Addr.m_Ref);

		return !Text.IsEmpty();
		}

	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Module v2 Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CAdam4000v2AddrDialog, CStdDialog);
		
// Constructor

CAdam4000v2AddrDialog::CAdam4000v2AddrDialog(CAdam4000v2Driver &Driver, CAddress &Addr, CItem *pConfig)
{
	SetName(TEXT("Adam4000v2AddrDlg"));

	m_pDriver = &Driver;

	m_pConfig = pConfig;

	m_pAddr   = &Addr;

	m_uType   = 0;

	m_uCmd	  = 0;
	}

// Message Map

AfxMessageMap(CAdam4000v2AddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(idListType, LBN_SELCHANGE,	OnSelectChange)
	AfxDispatchNotify(idListCmd,  LBN_SELCHANGE,	OnSelectChange)
	AfxDispatchNotify(idTypeEdit, CBN_SELCHANGE,	OnCmdSelChange)
	
	AfxMessageEnd(CAdam4000v2AddrDialog)
	};

// Message Handlers

BOOL CAdam4000v2AddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		LoadList(idListType, pOpts->GetCmdTypes());

		InitChannel();

		SetCurrentSelect();
		}

	return FALSE;
	}

// Notification Handlers

void CAdam4000v2AddrDialog::OnSelectChange(UINT uID, CWnd &Wnd)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		CListBox &ListBox = (CListBox &) GetDlgItem(uID);

		UINT uPos = ListBox.GetCurSel();

		if( uID == idListType ) {

			m_uType = uPos;
	
			LoadList(idListCmd, pOpts->GetCmdList(m_uType));
			}

		if( uID == idListCmd ) {

			m_uCmd = uPos;

			SetDataType(FALSE);

			SetAccess();

			SetRead();

			SetWrite();
	
			EnableChannel();
			}
		}
	}

void CAdam4000v2AddrDialog::OnCmdSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == idTypeEdit ) {

		SetType();
		}
	}

// Command Handlers

BOOL CAdam4000v2AddrDialog::OnOkay(UINT uID)
{
	if( m_pDriver ) {

		CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

		if( pOpts ) {

			CError Error(TRUE);

			CAddress Addr;

			CString  Text = pOpts->GetSelectListElement(m_uType, defCmd);

			Text += "_";

			Text += pOpts->GetSelectListElement(m_uType, m_uCmd, defCmd);

			if( GetDlgItem(idComboEdit).IsEnabled() ) {

				CComboBox &Box = (CComboBox &)GetDlgItem(idComboEdit);

				Text += CPrintf(L"_%u", Box.GetCurSel());
				}

			CString Type =  pOpts->GetSelectListElement(m_uType, m_uCmd, defType);

			if( !Type.IsEmpty() ) {

				Text += ".";

				CComboBox &Box = (CComboBox &)GetDlgItem(idTypeEdit);

				Text += Box.GetWindowTextW();
				}

			if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				*m_pAddr = Addr;

				EndDialog(TRUE);

				return TRUE;
				}
			}
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CAdam4000v2AddrDialog::LoadList(UINT uID, CStringArray Array)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(uID);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 44 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	if( uID == idListType ) {

		ListBox.AddStrings(Array);
		}

	if( uID == idListCmd ) {

		UINT uAdd = Array.GetCount();

		for( UINT u = 0; u < uAdd; u++ ) {

			CString Add;

			CString Line = Array.GetAt(u);

			UINT uFind = Line.Find(L"\t");

			if( uFind < NOTHING ) {

				uFind = Line.Find(L"\t", uFind + 1);

				if( uFind < NOTHING ) {

					Line = Line.Mid(0, uFind);

					ListBox.AddString(Line);
					}
				}
			}
		}

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	ListBox.SetCurSel(0);

	OnSelectChange(uID, ListBox);
	}

void CAdam4000v2AddrDialog::SetListBoxSel(UINT uID, UINT uSel)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(uID);

	ListBox.SetCurSel(uSel);

	if( uID == idListType ) {

		m_uType = uSel;
		}

	if( uID == idListCmd ) {

		m_uCmd = uSel;
		}

	OnSelectChange(uID, ListBox);
	}

void CAdam4000v2AddrDialog::SetCurrentSelect(void)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		CAdmCmd * pCmd = pOpts->GetCommand(*m_pAddr);

		if( pCmd ) {

			if( pCmd->m_bCmdType < defNothing ) {

				UINT uType = 0;

				UINT uCmd  = 0;

				pOpts->GetSelectIndexes(pCmd->m_Text, uType, uCmd);
				
				SetListBoxSel(idListType, uType);

				if( pCmd->m_bCmd < defNothing ) {

					LoadList(idListCmd, pOpts->GetCmdList(uType));

					SetListBoxSel(idListCmd, uCmd);

					if( pCmd->m_bChan < defNothing ) {

						CComboBox &Box = (CComboBox &)GetDlgItem(idComboEdit);

						Box.SetCurSel(pCmd->m_bChan);

						EnableChannel();
						}
					
					SetDataType(TRUE);
					}
				}
			}
		}
	}

void CAdam4000v2AddrDialog::Clear(void)
{
	CStringArray Array;

	LoadList(idListCmd, Array);

	GetDlgItem(idDetType).SetWindowText(CString(IDS_TYPE));

	GetDlgItem(idDetAccess).SetWindowText(CString(IDS_ACCESS));

	GetDlgItem(idDetRead).SetWindowText(CString(IDS_READ));

	GetDlgItem(idDetWrite).SetWindowTextW(CString(IDS_WRITE));

	}

void CAdam4000v2AddrDialog::SetType(void)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		CString	Text = GetDlgItem(idTypeEdit).GetWindowText();
		
		Text.ToUpper();

		GetDlgItem(idDetType).SetWindowTextW(Text);
		}
	}

void CAdam4000v2AddrDialog::SetAccess(void)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		CString Text = CString(IDS_READWRITE);
	
		if( pOpts->GetSelectListElement(m_uType, m_uCmd, defRead).IsEmpty() ) {

			Text = CString(IDS_WRITE_ONLY);
			}

		if( pOpts->GetSelectListElement(m_uType, m_uCmd, defWrite).IsEmpty() ) {

			Text = CString(IDS_READ_ONLY);
			}

		GetDlgItem(idDetAccess).SetWindowTextW(Text);
		}
	}

void CAdam4000v2AddrDialog::SetRead(void)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		GetDlgItem(idDetRead).SetWindowTextW(pOpts->GetSelectListElement(m_uType, m_uCmd, defRead));
		}
	}

void CAdam4000v2AddrDialog::SetWrite(void)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		GetDlgItem(idDetWrite).SetWindowTextW(pOpts->GetSelectListElement(m_uType, m_uCmd, defWrite));
		}
	}

void CAdam4000v2AddrDialog::InitChannel(void)
{
	CComboBox &Box = (CComboBox &)GetDlgItem(idComboEdit);

	for( UINT c = 0; c < 8; c++ ) {

		Box.AddString(CPrintf(L"%u", c));
		}

	Box.SetCurSel(0);
	}

void CAdam4000v2AddrDialog::EnableChannel(void)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		BOOL fEnable = pOpts->GetSelectListElement(m_uType, m_uCmd, defDesc).Find(L" N ") < NOTHING;

		for( UINT u = idComboText; u <= idComboEdit; u++ ) {

			GetDlgItem(u).EnableWindow(fEnable);
			}
		}
	}

void CAdam4000v2AddrDialog::SetDataType(BOOL fInit)
{
	CAdam4000v2DeviceOptions * pOpts = (CAdam4000v2DeviceOptions *)m_pConfig;

	if( pOpts ) {

		CComboBox &Box = (CComboBox &)GetDlgItem(idTypeEdit);

		Box.ResetContent();

		CString Content = pOpts->GetSelectListElement(m_uType, m_uCmd, defType);

		CStringArray Array;

		Content.Tokenize(Array, '|');

		Box.AddStrings(Array);
		
		BOOL fEnable = Array.GetCount() > 1;

		for( UINT u = idTypeText; u <= idTypeEdit; u++ ) {

			GetDlgItem(u).EnableWindow(fEnable);
			}

		UINT uSel = fInit ? pOpts->GetCommandForm(*m_pAddr) - 1 : 0;

		Box.SetCurSel(fEnable ? uSel : 0);
		
		SetType();
		}
	}

// End of File
