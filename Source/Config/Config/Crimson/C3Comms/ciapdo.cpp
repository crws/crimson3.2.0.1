
#include "intern.hpp"

#include "ciapdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Element Storage Class
//

// Dynamic Class

AfxImplementDynamicClass(CElement, CMetaItem);	  

// Constructor

CElement::CElement(void)
{
	m_Index = 0;

	m_Sub   = 0;

	m_Size  = 0;

	AddMeta();
	}

// Data Access

UINT CElement::GetElementIndex(void)
{
	return m_Index;
	}

UINT CElement::GetElementSub(void)
{
	return m_Sub;
	}

UINT CElement::GetElementSize(void)
{
	return m_Size;
	}

UINT CElement::GetElementSizeSel(void)
{
	switch( m_Size ) {

		case 1:	return sizeByte;
		case 2:	return sizeWord;
		case 4:	return sizeLong;
		}

	return 0;
	}

BOOL CElement::SetElementIndex(UINT uIndex)
{
	m_Index = uIndex & 0xFFFF;

	return TRUE;
	}

BOOL CElement::SetElementSub(UINT uSub)
{
	m_Sub = uSub & 0xFF;

	return TRUE;
	}

BOOL CElement::SetSize(UINT uSize)
{
	m_Size = uSize & 0xFF;

	return TRUE;
	}
 
BOOL CElement::SetElementSize(UINT uSize)
{
	switch( uSize ) {

		case sizeByte:  m_Size = 1;	break;
		case sizeWord:	m_Size = 2;	break;
		case sizeLong:	m_Size = 4;	break;
		}

	return TRUE;
	}

BOOL CElement::SetElementSize(CString Size)
{
	switch( Size.GetAt(0) ) {

		case 'B':	m_Size = 1;	break;
		case 'W':	m_Size = 2;	break;
		case 'L':	m_Size = 4;	break;
		}

	return TRUE;
	}

BOOL CElement::SetElement(CString Text)
{
	UINT uFind = Text.Find('-');

	if( uFind == NOTHING ) {

		uFind = Text.Find('/');
		}

	if( uFind < NOTHING ) {
	
		m_Index = tstrtoul(Text.Mid(1, uFind), NULL, 16);

		m_Sub   = tstrtoul(Text.Mid(uFind + 1, Text.GetLength() - 1), NULL, 16);

		SetElementSize(Text.Left(1));

		return TRUE;
		}
	
	return FALSE;
	}
 
CString CElement::GetElementString(void)
{
	CString Text;

	Text.Printf("%s%4.4X-%2.2X", GetPrefix(), m_Index, m_Sub);

	return Text;
	}

CString CElement::GetPrefix(void)
{	
	switch( m_Size ) {

		case 1:		return "B";
		case 2:		return "W";
		case 4:		return "L";

		}

	return "";
	}

UINT CElement::GetType(void)
{
	switch( m_Size ) {

		case 1:		return addrByteAsByte;
		case 2:		return addrWordAsWord;
		case 4:		return addrLongAsLong;

		}

	return 0;
	}

// Download Support

BOOL CElement::MakeInitData(CInitData &Init)
{
	Init.AddWord(WORD(m_Index));

	Init.AddByte(BYTE(m_Sub));

	Init.AddByte(BYTE(m_Size));
	
	return TRUE;
	}

// Meta Data Creation

void CElement::AddMetaData(void)	
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Index);

	Meta_AddInteger(Sub);

	Meta_AddInteger(Size);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Element List Class
//

// Dynamic Class

AfxImplementDynamicClass(CElementList, CItemList);

// Constructor

CElementList::CElementList(void)
{
	}

// Destructor

CElementList::~CElementList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CElement * CElementList::GetItem(INDEX Index) const
{
	return (CElement *) CItemList::GetItem(Index);
	}

CElement * CElementList::GetItem(UINT uPos) const
{
	return (CElement *) CItemList::GetItem(uPos);
	}

// Operations

CElement * CElementList::AppendItem(CElement * pEl)
{
	pEl->SetParent(this);

	if( CItemList::AppendItem(pEl) ) {

		return pEl;
		}  

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// PDO Storage Class
//

// Dynamic Class

AfxImplementDynamicClass(CPDO, CMetaItem);

// Constructor

CPDO::CPDO(void)
{
	m_Type   = 0;
	
	m_Number = 1;

	m_Manual = 0;

	m_Event  = EVT_DEF;

	m_pElements = New CElementList;

	AddMeta();
	}

// Download Support

BOOL CPDO::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_Type));

	Init.AddWord(WORD(m_Number));

	if( m_pElements ) {

		UINT uCount = m_pElements->GetItemCount();

		Init.AddWord(WORD(uCount));

		for( UINT u = 0; u < uCount; u++ ) {

			m_pElements->GetItem(u)->MakeInitData(Init);
			}
		}
	else {
		Init.AddWord(0);
		}

	Init.AddByte(BYTE(m_Manual));

	Init.AddWord(WORD(m_Event));
		
	return TRUE;
	}

// Meta Data Creation

void CPDO::AddMetaData(void)	
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Type);
	
	Meta_AddInteger(Number);

	Meta_AddCollect(Elements);

	Meta_AddInteger(Manual);

	Meta_AddInteger(Event);
	}

CString CPDO::GetPDOString(void)
{
	CString Text = m_Type ? "T" : "R";

	Text += "PDO ";
	
	Text += CPrintf(L"%u", m_Number);

	if( m_Manual ) {

		Text += CPrintf(L".%u", m_Event);
		}
	
	return Text;
	}


BOOL CPDO::AddElement(CString Text)
{
	UINT uSize = 0;
	
	switch( Text.GetAt(0) ) {

		case 'B':
			uSize = 1;
			break;

		case 'W':
			uSize = 2;
			break;

		case 'L':
			uSize = 4;
			break;

		}

	if( CanAddElement(uSize) ) {
	
		CElement * pElement = New CElement();
			
		pElement->SetElement(Text);

		m_pElements->AppendItem(pElement);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPDO::CanAddElement(UINT uSize)
{
	UINT uCurrent = 0; 

	UINT uElement = 0;

	for( UINT u = 0; u < m_pElements->GetItemCount(); u++ ) {

		uElement = m_pElements->GetItem(u)->GetElementSize();

		if( uElement == 0 ) {

			uElement++;
			}

		uCurrent += uElement;
		}

	return (8 - uCurrent >= uSize);
	}

 

//////////////////////////////////////////////////////////////////////////
//
// PDO List Class
//

// Dynamic Class

AfxImplementDynamicClass(CPDOList, CItemList);

// Constructor

CPDOList::CPDOList(void)
{
	}

// Destructor

CPDOList::~CPDOList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CPDO * CPDOList::GetItem(INDEX Index) const
{
	return (CPDO *) CItemList::GetItem(Index);
	}

CPDO * CPDOList::GetItem(UINT uPos) const
{
	return (CPDO *) CItemList::GetItem(uPos);
	}

// Operations

CPDO * CPDOList::AppendItem(CPDO * pPDO)
{
	pPDO->SetParent(this);

	if( CItemList::AppendItem(pPDO) ) {

		return pPDO;
		}  

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave
//

// Instantiator

ICommsDriver * Create_CANOpenPDOSlaveDriver(void)
{
	return New CCANOpenPDOSlave;
	}

// Constructor

CCANOpenPDOSlave::CCANOpenPDOSlave(void)
{
	m_wID		= 0x340B;

	m_uType		= driverMaster;
	
	m_DriverName	= "PDO Slave";
	
	m_Version	= "1.02";
	
	m_ShortName	= "PDO Slave";

	m_DevRoot	= "DEV";

	m_fSingle       = TRUE;

	m_fDirty	= FALSE;
	}

// Configuration

CLASS CCANOpenPDOSlave::GetDriverConfig(void)
{	
	return AfxRuntimeClass(CCANOpenPDOSlaveDriverOptions);
	}

// Address Management

BOOL CCANOpenPDOSlave::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CCANOpenPDODialog Dlg(*this, Addr, pConfig);  
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CCANOpenPDOSlave::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{	
	if( Addr.m_Ref && Text.IsEmpty() ) {

		return TRUE;
		}

	UINT uFind   = Text.Find(L"PDO");

	UINT uNum    = NOTHING;

	UINT uManual = NOTHING;

	UINT uEvent  = NOTHING;

	if( uFind < NOTHING ) {

		uManual = Text.Find('.');

		CString Manual = "";

		CString Number = "";

		if( uManual < NOTHING ) {

			Manual  = Text.Mid(uManual + 1);

			Number  = Text.Mid(5, uManual);

			uManual = 1;
			}
		else {
			Number = Text.Mid(5);
			}

		uNum = tstrtol(Number, NULL, 10);

		if( uNum < PDO_MIN || uNum > PDO_MAX ) {

			Error.Set(GetError(ERR_PDO), 0);

			return FALSE;
			}

		if( !Manual.IsEmpty() ) {

			uEvent = tstrtol(Manual, NULL, 10);

			if( uEvent < EVT_MIN || uEvent > EVT_MAX ) {

				Error.Set(GetError(ERR_EVT), 0);

				return FALSE;
				}
			}
		}
	
	if( LookupPDO(Addr, pConfig, Text, uNum, uManual, uEvent) ) {

		return TRUE;
		}

	return FALSE;
	}


BOOL CCANOpenPDOSlave::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CPDO * pPDO = LookupPDO(Addr, pConfig);

	if( pPDO && pPDO->m_pElements ) {

		UINT uIndex = Addr.a.m_Offset & 0x7;

		if( uIndex < pPDO->m_pElements->GetItemCount() ) {

			CElement * pElement = pPDO->m_pElements->GetItem(uIndex);

			if( pElement ) {

				if( uIndex == 0 && !Text.IsEmpty() ) {

					Text.Printf("%cPDO %u", Addr.a.m_Extra ? 'T' : 'R', (Addr.a.m_Offset & 0x7FFF) >> 3);
		
					return TRUE;
					}

				Text = pElement->GetElementString();

				return TRUE;
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

CPDO * CCANOpenPDOSlave::FindPDO(CAddress Addr, CItem *pConfig)
{
	CCANOpenPDOSlaveDriverOptions * pOptions = (CCANOpenPDOSlaveDriverOptions *) pConfig;

	if( pOptions ) {

		INDEX Index = pOptions->m_pPDOs->GetHead();
	
		while( !pOptions->m_pPDOs->Failed(Index) ) {

			CPDO * pPDO = (CPDO *)pOptions->m_pPDOs->GetItem(Index);

			if( Addr.a.m_Table == 0x10 ) {

				if( pPDO->m_Number == GetPDONumber(Addr) ) {

					if( pPDO->m_Type == Addr.a.m_Extra ) {

						return pPDO;
						}
					}
				}

			pOptions->m_pPDOs->GetNext(Index);
			}
		}

	return NULL;
	}

CPDO* CCANOpenPDOSlave::LookupPDO(CAddress &Addr, CItem *pConfig, CString Text, UINT uNum, UINT uManual, UINT uEvent)
{
	CCANOpenPDOSlaveDriverOptions * pOptions = (CCANOpenPDOSlaveDriverOptions *) pConfig;
	
	if( pOptions ) {
	
		UINT uCount = pOptions->m_pPDOs->GetItemCount();

		for( UINT x = 0; x < uCount; x++ ) {

			CPDO * pPDO = pOptions->m_pPDOs->GetItem(x);

			if( pPDO ) {

				if( pPDO->GetPDOString() == Text ) {

					Addr.a.m_Offset = pPDO->m_Number << 3;

					Addr.a.m_Table  = 0x10;

					Addr.a.m_Type   = addrLongAsLong;

					Addr.a.m_Extra  = pPDO->m_Type;

					return pPDO;
					}

				UINT uItems = pPDO->m_pElements->GetItemCount();

				for( UINT y = 0; y < uItems; y++ ) {
					
					CElement * pElement = pPDO->m_pElements->GetItem(y);

					if( pElement ) {

						UINT uMid = isdigit(Text.GetAt(0)) ? 1 : 0;

						if( pElement->GetElementString().Mid(uMid) == Text ) {

							Addr.a.m_Offset = (pPDO->m_Number << 3) | y;

							Addr.a.m_Table  = 0x10;

							Addr.a.m_Type   = pElement->GetType();

							Addr.a.m_Extra  = pPDO->m_Type;

							return pPDO;
							}
						}
					}
				}
			}
		}

	return CreatePDO(Addr, pConfig, Text, uNum, uManual, uEvent);
	}

CPDO* CCANOpenPDOSlave::LookupPDO(CAddress Addr, CItem *pConfig)
{
	CCANOpenPDOSlaveDriverOptions * pOptions = (CCANOpenPDOSlaveDriverOptions *) pConfig;

	if( pOptions ) {
		
		UINT uCount = pOptions->m_pPDOs->GetItemCount();

		for( UINT x = 0; x < uCount; x++ ) {

			CPDO * pPDO = pOptions->m_pPDOs->GetItem(x);

			if( pPDO ) {

				if( GetPDONumber(Addr) == pPDO->m_Number && 
					Addr.a.m_Extra == pPDO->m_Type ) {

					return pPDO;
					}
				}
			}
		}

	return CreatePDO(Addr, pConfig);
	}

CPDO* CCANOpenPDOSlave::CreatePDO(CAddress &Addr, CItem *pConfig, CString Text, UINT uNum, UINT uManual, UINT uEvent)
{
	CPDO * pPDO	= New CPDO;
		
	pPDO->m_Type    = Text.Left(1) == CString("T");

	pPDO->m_Number  = uNum	  < NOTHING ? uNum    : 0;

	pPDO->m_Manual  = uManual < NOTHING ? uManual : 0;

	pPDO->m_Event   = uEvent  < NOTHING ? uEvent  : EVT_DEF;

	Addr.a.m_Table  = 0x10;

	Addr.a.m_Offset = pPDO->m_Number << 3;

	Addr.a.m_Type   = addrLongAsLong;

	Addr.a.m_Extra  = pPDO->m_Type;
	
	CCANOpenPDOSlaveDriverOptions * pOptions = (CCANOpenPDOSlaveDriverOptions *) pConfig;

	if( pOptions ) {

		pOptions->m_pPDOs->AppendItem(pPDO);

		return (CPDO *)pOptions->m_pPDOs->GetItem(pOptions->m_pPDOs->GetItemCount() - 1);
		}

	return NULL;
	}

CPDO* CCANOpenPDOSlave::CreatePDO(CAddress Addr, CItem *pConfig)
{
	CPDO * pPDO = New CPDO;

	CCANOpenPDOSlaveDriverOptions * pOptions = (CCANOpenPDOSlaveDriverOptions *) pConfig;

	if( pOptions ) {

		CPDOList * pList = pOptions->m_pPDOs;

		if( pList ) {
		
			pPDO->m_Type    = 0;

			pPDO->m_Number  = GetPDONumber(Addr);
	
			pList->AppendItem(pPDO);

			return pList->GetItem(pList->GetItemCount() - 1);
			}
		}

	return NULL;
	}
 
BOOL CCANOpenPDOSlave::DeletePDO(CAddress Addr, CItem *pConfig)
{
	CPDO * pPDO = FindPDO(Addr, pConfig);

	return DeletePDO(pPDO, pConfig);
	}

BOOL CCANOpenPDOSlave::DeletePDO(CPDO * pPDO, CItem * pConfig)
{
	CCANOpenPDOSlaveDriverOptions * pOptions = (CCANOpenPDOSlaveDriverOptions *) pConfig;

	if( pOptions ) {

		pOptions->m_pPDOs->RemoveItem(pPDO);

		delete pPDO;

		return TRUE;
		}

	return FALSE;
	}

void CCANOpenPDOSlave::SetDirty(CAddress &Addr)
{
	m_fDirty = !m_fDirty;
	
	UINT uToggle = m_fDirty ? 0 : 0x8000;

	Addr.a.m_Offset &= 0x7FFF;
	
	Addr.a.m_Offset |= uToggle;
	}

UINT CCANOpenPDOSlave::GetPDONumber(CAddress &Addr)
{
	return (Addr.a.m_Offset & 0x7FFF) >> 3;
	}

CString CCANOpenPDOSlave::GetError(UINT uError)
{
	CString Error = CString(IDS_VALID_VALUES_ARE);

	if( uError == ERR_PDO ) {

		Error = CPrintf(CString(IDS_PDO_NUMBER) + Error, PDO_MIN, PDO_MAX);
		}

	if( uError == ERR_EVT ) {

		Error = CPrintf(IDS("Event"	) + Error, EVT_MIN, EVT_MAX);
		}

	return Error;
	}

//////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANOpenPDOSlaveDriverOptions, CCANOpenSlaveDriverOptions);

// Constructor

CCANOpenPDOSlaveDriverOptions::CCANOpenPDOSlaveDriverOptions(void)
{	
	m_Drop	    = 2;

	m_Offset    = "0";

	m_Type      = 0;

	m_Restrict  = 0;

	m_Increment = 1;

	m_Entry     = 0;

	m_Profile   = 0;

	m_pPDOs     = New CPDOList;

	m_uCompact  = 0;

	m_Op	    = 0;
	}

// UI Managament

void CCANOpenPDOSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {
	
		if( Tag == "Update" || Tag.IsEmpty() ) {

			CWnd * Button = pWnd->GetDlgItemPtr(0x7F00);
					
			Button->EnableWindow(!m_Update);
			}
		
		if( Tag.IsEmpty() ) {

			pWnd->UpdateUI("Entry");

			pWnd->EnableUI("Increment", FALSE);
			}
		}

	}

// Download Support

BOOL CCANOpenPDOSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CCANOpenSlaveDriverOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_Profile));
	
	Init.AddByte(BYTE(m_Op));

	if( m_pPDOs ) {

		UINT uCount = m_pPDOs->GetItemCount();

		Init.AddWord(WORD(uCount));

		for( UINT u = 0; u < uCount; u++ ) {

			m_pPDOs->GetItem(u)->MakeInitData(Init);
			}
		}
	else {
		Init.AddWord(0);
		}

	return TRUE;
	}

// Meta Data Creation

void CCANOpenPDOSlaveDriverOptions::AddMetaData(void)
{
	CCANOpenSlaveDriverOptions::AddMetaData();

	Meta_AddInteger(Profile);

	Meta_AddCollect(PDOs);

	Meta_AddInteger(Op);
	}

// EDS File Support

void CCANOpenPDOSlaveDriverOptions::WriteDeviceInfo(ITextStream &Stream)
{
	CDatabase * pBase = ( CDatabase * ) this->GetDatabase();

	CString Model = pBase->GetModelName();

	CString RPDO = "%u\r\n";

	RPDO.Printf(RPDO, GetPDOCount(FALSE));

	CString TPDO = "%u\r\n";

	TPDO.Printf(TPDO, GetPDOCount(TRUE));

	CString Compact = "%2.2x\r\n";

	Compact.Printf(Compact, m_uCompact);

	Stream.PutLine(CString(L"[DeviceInfo]\r\n"));
	Stream.PutLine(CString(L"VendorName=Red Lion Controls Inc\r\n"));
	Stream.PutLine(CString(L"VendorNumber=0x0000022C\r\n"));
	Stream.PutLine(CString(L"ProductName=G3\r\n"));
	Stream.PutLine(CString(L"ProductNumber=0x4733\r\n"));
	Stream.PutLine(CString(L"RevisionNumber=1\r\n"));
	Stream.PutLine(CPrintf(L"OrderCode=%s\r\n", Model));
	Stream.PutLine(CString(L"BaudRate_10=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_20=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_50=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_125=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_250=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_500=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_800=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_1000=1\r\n"));
	Stream.PutLine(CString(L"SimpleBootUpMaster=0\r\n"));
	Stream.PutLine(CString(L"SimpleBootUpSlave=1\r\n"));
	Stream.PutLine(CString(L"Granularity=8\r\n"));
	Stream.PutLine(CString(L"DynamicChannelsSupported=0\r\n"));
	Stream.PutLine(CString(L"GroupMessaging=0\r\n"));
	Stream.PutLine(CPrintf(L"CompactPDO=0x%s%s%s%s%s%s\r\n", Compact, L"NrOfRXPDO=", RPDO, L"NrOfTXPDO=", TPDO, L"LSS_Supported=0\r\n\r\n"  ));
	}

void CCANOpenPDOSlaveDriverOptions::WriteOptionalObjects(ITextStream &Stream)
{
	// Non-Mandatory objects 0x1000 - 0x1FFF and 0x6000 - 0xFFFF

	UINT uCount = 0;

	CString Man = GetMandatoryObjects(OBJ_OPT, uCount);

	CString Rem = GetRemainingObjects(OBJ_OPT, uCount);
		
	CString Objects = "[OptionalObjects]\r\n"
			  "SupportedObjects=";

	CString Format = "%u";

	Format.Printf(Format, uCount);

	Objects += Format;

	Objects += "\r\n";

	Objects += Man;

	Objects += Rem;

	Objects += "\r\n";

	Stream.PutLine(Objects);

	CString Name [] = { "1008", "1009", "100A", "100C", "100D", "1016" };

	CString Desc  [] = { "Manufacturer Device Name",
		             "Manufacturer Hardware Version",
			     "Manufacturer Software Version",
			     "Guard Time",
			     "Life Time Factor",
			     "Consumer Heartbeat Time"};
	
	for( UINT u = 0; u < elements(Name); u++ ) {

		WriteObject(Stream, Name[u], Desc[u]);
		}
	}

BOOL CCANOpenPDOSlaveDriverOptions::IsObjectMandatory(UINT uIndex, UINT uType) 
{
	if( uType == OBJ_OPT || uType == OBJ_ALL ) {

		switch( uIndex ) {     

			case 0x1008:
			case 0x1009:
			case 0x100A:
			case 0x100C:
			case 0x100D:
			case 0x1016:
			
				return TRUE;
			}
		}

	if( uType == OBJ_MAND || uType == OBJ_ALL ) {

				        
		switch( uIndex) {

			case 0x1000:
			case 0x1001:
			case 0x1018:

				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCANOpenPDOSlaveDriverOptions::IsPDO(void)
{
	return TRUE;
	}

UINT CCANOpenPDOSlaveDriverOptions::GetPDOCount(BOOL fTPDO)
{
	UINT uCount = 0;

	UINT uStart = 0;
	
	UINT uEnd = m_Objects.Find('?');

	UINT uLen = m_Objects.GetLength();

	CString Name = "";

	CString Access = fTPDO ? "wo" : "ro";

	while( uEnd < uLen ) {

		Name = m_Objects.Mid(uStart, uEnd - uStart);

		if( GetAccessType(Name) == Access ) {

			uCount++;
			}

		uStart = uEnd + 1;

		uEnd = m_Objects.Find('?', uStart);
		}

	return uCount;
	}

BOOL CCANOpenPDOSlaveDriverOptions::GetObjects(void)
{
	CTextStreamMemory Stream;

	CDatabase * pBase = GetDatabase();  

	if( Stream.LoadFromFile(pBase->GetFilename()) ) {

		CTreeFile Tree;	

		if( Tree.OpenLoad(Stream) ) {

			afxThread->SetWaitMode(TRUE);

			m_Objects.Empty();

			CString Temp = "";

			CString Path [] = {"C3Data",
					   "System",
					   "Comms", 
					   "Option0",
					   "Ports", 
					   "CommsPortCAN",
					   "Config",
					   "PDOs",
					   "PDO",
					   "Type",
					   "Elements",
					   "Element",
					   "Index",
					   "Sub",
					   "Size"
					   };
					   

			UINT    uState [] = {0,	// c3data
					    0,	// system
					    0,	// comms
					    1,	// option0
					    0,	// ports
					    0,	// commsportcan
					    1,	// config
				            1,	// pdos
					    0,	// pdo
					    3,	// type
					    1,	// elements
					    0,	// element
					    4,	// index
					    5,	// sub
					    6};	// size

			UINT uPos = 0;

			UINT uSize = 0;

			UINT uObject = 0;

			UINT uCollect = 0;

			UINT uWrite = 0;

			UINT uIndex = 0;

			UINT uSub = 0;

			Tree.SetCompressed();

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				if( Name == Path[uPos] ) {

					switch( uState[uPos] ) {

						case 0:	
							GetObject(Tree, uObject);
							
							break;
						case 1: 	      
							Tree.GetCollect();

							uCollect++;
						
							break;
						
						case 3: 
							uWrite = Tree.GetValueAsInteger();
							break;

						case 4:	
							uIndex = Tree.GetValueAsInteger();
							break;

						case 5:
							uSub = Tree.GetValueAsInteger();
							break;

						case 6:
							uSize = Tree.GetValueAsInteger();

							CElement Element;

							Element.SetSize(uSize);
						
							m_Objects += Element.GetPrefix();

							Temp.Printf("%4.4x/%3.3u^%u^%u?", uIndex, uSub, Element.GetType(), uWrite);

							m_Objects += Temp;

							Tree.EndObject();

							uObject--;

							if( Tree.IsEndOfData() ) {

								End(Tree, uObject, uCollect);

								uPos = 7;
								}
							else {
								uPos = 10;
								}

							break;
					       	}
					
					if( ++uPos == elements(Path) ) {

						break;
						}
					}
				}

			afxThread->SetWaitMode(FALSE);

			while( uCollect > 0 ) {

				Tree.EndCollect();

				uCollect--;
				}

			while( uObject > 0 ) {

				Tree.EndObject();

				uObject--;
				}

			Tree.Close();

			ParseObjects();

			return TRUE;
			}
		}   

	return FALSE;
	}

void CCANOpenPDOSlaveDriverOptions::ParseObjects(void)
{
	UINT uStart = 0;    

	CString Objects = "";
	
	UINT uEnd = m_Objects.Find('/');

	UINT uLen = m_Objects.GetLength();

	CString Name = "";

	while( uEnd < uLen ) {

		Name = m_Objects.Mid(uStart, uEnd - uStart);

		UINT u = 0;

		UINT uSub = 0;

		BOOL fSet = FALSE;

		UINT Start = 0;

		UINT End = 0;

		UINT e = Objects.Find('/');
		
		while( !Objects.IsEmpty() && e < NOTHING ) {

			CString Object = Objects.Mid(u, e - u);

			if( Name == Object ) {

				u = e + 1;

				e = Objects.Find('^', u); 

				Start = uEnd + 1;
			       			
				End = m_Objects.Find('^', Start);
				
				CString ObjectSub = Objects.Mid(u, e - u);

				CString NameSub = m_Objects.Mid(Start, End - Start);

				UINT uObjectSub = tatoi(ObjectSub);

				UINT uNameSub = tatoi(NameSub);

				if( uNameSub > uObjectSub ) {

					uSub = uNameSub;
					}

				if( m_uCompact < uNameSub ) {

					m_uCompact = uNameSub;
					}

				break;
				}

			e = Objects.Find('?', e + 1);

			u = e + 1;

			e = Objects.Find('/', u);
			}

		if( uSub ) {

			UINT uPos = Objects.Find(Name);

			uPos = Objects.Find('/', uPos);

			CString Temp;

			Temp.Printf("%3.3u", uSub);
			
			Objects.SetAt(uPos + 1, Temp.GetAt(0));

			Objects.SetAt(uPos + 2, Temp.GetAt(1));

			Objects.SetAt(uPos + 3, Temp.GetAt(2));

			fSet = TRUE;
		    	}
	       	
		uEnd = m_Objects.Find('?', uEnd + 1);

		Name = m_Objects.Mid(uStart, uEnd - uStart);

		if( !fSet && (Objects.IsEmpty() || u >= Objects.GetLength()) ) {

			Objects += Name + "?";
			}

		uStart = uEnd + 1;

		uEnd = m_Objects.Find('/', uStart);
		}

	m_Objects.Empty();

	m_Objects += CString(Objects);
	}

//////////////////////////////////////////////////////////////////////////
//
// PDO Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCANOpenPDODialog, CStdDialog);
		
// Constructor

CCANOpenPDODialog::CCANOpenPDODialog(CCANOpenPDOSlave &Driver, CAddress &Addr, CItem *pConfig)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pPDO	  = NULL;

	m_pConfig = (CCANOpenPDOSlaveDriverOptions *) pConfig;

	SetName(L"CANOpenPDODlg");
	}

// Message Map

AfxMessageMap(CCANOpenPDODialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown)
	AfxDispatchNotify(2002, EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(2004, CBN_SELCHANGE,  OnSelChange)
	AfxDispatchNotify(2006, CBN_SELCHANGE,	OnSelChange)
	AfxDispatchNotify(2008, EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(3002, EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(3004, EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(3006, CBN_SELCHANGE,  OnSelChange)
	
     	AfxDispatchCommand(3008, OnAddElement)
	AfxDispatchCommand(IDOK, OnOkay)
		
	AfxMessageEnd(CCANOpenPDODialog)
	};

// Message Handlers

BOOL CCANOpenPDODialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	CString Text;

	if( m_pDriver->ExpandAddress(Text, m_pConfig, Addr) ) {

		m_pPDO = m_pDriver->LookupPDO(Addr, m_pConfig);
		}
	
	LoadPDO(m_pPDO); 

	LoadObjectInfo(m_pPDO);

	LoadManualEvent(m_pPDO);

	LoadElementInfo();

	DoEnables();
		
	return TRUE;
	}

// Notification Handlers

void CCANOpenPDODialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( uID == 1001 ) {

		m_hSelect = Info.itemNew.hItem;

		LoadElementInfo();

		DoEnables();
		}
	}


BOOL CCANOpenPDODialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{	 
	if( uID == 1001 ) {

		if( Info.wVKey == VK_DELETE ) {
		
			if( GetKeyState(Info.wVKey) & 0x8000 ) {

				return DeleteElement();
				}
			}
		}
      
	return TRUE;
	}

void CCANOpenPDODialog::OnEditChange(UINT uID, CWnd &Wnd)
{
	if( uID == 3002 || uID == 3004 ) {

		if( m_hSelect != m_hRoot ) {

			CTreeViewItem Item = FindItem();

			INDEX Index = m_pPDO->m_pElements->GetHead();

			while( !m_pPDO->m_pElements->Failed(Index) ) {

				CElement * pElement = (CElement *)m_pPDO->m_pElements->GetItem(Index);

				if( CString(Item.GetText()) == pElement->GetElementString() ) {

					UINT uValue = GetEditValue(uID); 

					if( uID == 3002 ) {

						pElement->SetElementIndex(uValue);
						}
					else {
						pElement->SetElementSub(uValue);
						}

					UpdateItem(Item, pElement->GetElementString());

					return;
					}

				m_pPDO->m_pElements->GetNext(Index);
				}
			
			return;
			}
		}

	if( uID == 2002 ) {

		if( m_hSelect == m_hRoot ) {

			if( m_pPDO ) {

				UINT uEdit = GetEditValue(uID);

				if( uEdit < PDO_MIN || uEdit > PDO_MAX ) {

					Wnd.Error(m_pDriver->GetError(ERR_PDO));

					GetDlgItem(uID).SetFocus();
					}
				else {
					m_pPDO->m_Number = uEdit;
					}
				}
			}
		}

	if( uID == 2008 ) {

		if( m_hSelect == m_hRoot ) {

			if( m_pPDO ) {

				UINT uEdit = GetEditValue(uID);

				if( uEdit < EVT_MIN || uEdit > EVT_MAX ) {

					Wnd.Error(m_pDriver->GetError(ERR_EVT));

					GetDlgItem(uID).SetFocus();
					}
				else {
					m_pPDO->m_Event = uEdit;
					}
				}
			}
		}
	}


void CCANOpenPDODialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 3006 ) {

		if( m_hRoot != m_hSelect ) {

			CTreeViewItem Item = FindItem();

			INDEX Index = m_pPDO->m_pElements->GetHead();

			while( !m_pPDO->m_pElements->Failed(Index) ) {

				CElement * pElement = (CElement *)m_pPDO->m_pElements->GetItem(Index);

				if( CString(Item.GetText()) == pElement->GetElementString() ) {

					CComboBox &Size = (CComboBox &) GetDlgItem(uID);

					pElement->SetElementSize(Size.GetCurSel());

					UpdateItem(Item, pElement->GetElementString());

					return;
					}

				m_pPDO->m_pElements->GetNext(Index);
				}
			
			return;
			}
		}

	if( uID == 2004 ) {

		if( m_hSelect == m_hRoot ) {

			if( m_pPDO ) {

				CComboBox &Type = (CComboBox &) GetDlgItem(uID);

				m_pPDO->m_Type = Type.GetCurSel();

				CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

				UINT uImage = Tree.GetItem(m_hRoot).GetImage();

				uImage = m_pPDO->m_Type ? uImage + 1 : uImage - 1;

				CTreeViewItem Node = Tree.GetRoot();

				Node.SetImages(uImage);

				Tree.SetItem(Node);

				ResetManual();

				DoEnables();
				}
			}
		 }

	if( uID == 2006 ) {

		if( m_hSelect == m_hRoot ) {

			if( m_pPDO ) {

				CComboBox &Manual = (CComboBox &) GetDlgItem(uID);

				m_pPDO->m_Manual = Manual.GetCurSel();

				DoEnables();
				}
			}
		}
	}
		
BOOL CCANOpenPDODialog::OnAddElement(UINT uID)
{
	CComboBox &Size = (CComboBox &) GetDlgItem(3006);

	CElement Element;

	Element.SetElementSize(Size.GetCurSel());
	
	Element.SetElementIndex(GetEditValue(3002));

	Element.SetElementSub(GetEditValue(3004));

	if( m_pPDO ) {

		if( m_pPDO->AddElement(Element.GetElementString()) ) {

			CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

			CTreeViewItem Node;

			Node.SetText(Element.GetElementString().ToUpper());

			Node.SetParam(NOTHING);

			Node.SetImages(24);

			Tree.InsertItem(m_hRoot, NULL, Node);

			Tree.Expand(m_hRoot, TVE_EXPAND);

			return TRUE;
			}

		CError Error;

		Error.Set( "PDO has reached the 8-byte limit",
				   0
				   );

		Error.Show(ThisObject);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANOpenPDODialog::OnOkay(UINT uID)
{
	CAddress Addr;

	CError Error;

	if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, m_pPDO->GetPDOString()) ) {

		CAddress Dummy;

		INDEX Item = m_pPDO->m_pElements->GetHead();

		if( m_pPDO->m_pElements->Failed(Item) ) {

			Error.Set(CString(IDS_ELEMENTS_MUST_BE));

			Error.Show(ThisObject);

			return FALSE;
			}

		while( !m_pPDO->m_pElements->Failed(Item) ) {

			CString String = m_pPDO->m_pElements->GetItem(Item)->GetElementString();
			
			if( !m_pDriver->ParseAddress(Error, Dummy, m_pConfig, String) ) {
				
				Error.Show(ThisObject);

				EndDialog(TRUE);

				return TRUE;
				}

			m_pPDO->m_pElements->GetNext(Item);
			}
		
		m_pDriver->SetDirty(Addr);

		*m_pAddr = Addr;
		}
	else {
		Error.Show(ThisObject);

		GetDlgItem(2002).SetFocus();

		return FALSE;
		}

	EndDialog(TRUE);
	
	return TRUE;
	}

// PDO Loading

void CCANOpenPDODialog::LoadPDO(CPDO * pPDO)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_NOTOOLTIPS
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES;

	m_Images.Create(L"CommsManager", 16, 0, IMAGE_BITMAP, 0);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SendMessage(TVM_SETUNICODEFORMAT);
	
	LoadRoot(Tree, pPDO);

	LoadElements(Tree, pPDO);

	Tree.SetFocus();

	m_hSelect = m_hRoot;

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CCANOpenPDODialog::LoadRoot(CTreeView &Tree, CPDO *pPDO)
{
	CTreeViewItem Node;

	PTXT Text = L"PDO";

	Node.SetText(Text);

	Node.SetParam(NOTHING);

	Node.SetImages(10);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CCANOpenPDODialog::LoadElements(CTreeView &Tree, CPDO *pPDO)
{
	CElementList * pList = pPDO ? pPDO->m_pElements : NULL;

	for( UINT u = 0; u < UINT(pList ? pList->GetItemCount() : 0); u++ ) {

		LoadElement(Tree, pList ? pList->GetItem(u)->GetElementString() : "");
		}
	}

void CCANOpenPDODialog::LoadElement(CTreeView &Tree, PCTXT Text)
{
	CTreeViewItem Node;

	Node.SetText(Text);

	Node.SetParam(NOTHING);

	Node.SetImages(24);

	m_hSelect = Tree.InsertItem(m_hRoot, NULL, Node);	
	}

void CCANOpenPDODialog::LoadObjectInfo(CPDO * pPDO)
{
	CComboBox &Type = (CComboBox &) GetDlgItem(2004);

	Type.AddString("RPDO");

	Type.AddString("TPDO");

	Type.SetCurSel(pPDO ? pPDO->m_Type : 0);

	CEditCtrl &Number = (CEditCtrl &) GetDlgItem(2002);

	CString Num = "%u";

	Num.Printf(Num, pPDO ? pPDO->m_Number : 1);
		
	Number.SetWindowText(Num);
	}

void CCANOpenPDODialog::LoadManualEvent(CPDO * pPDO)
{
	CComboBox &Manual = (CComboBox &) GetDlgItem(2006);

	Manual.AddString(CString(IDS_NO));

	Manual.AddString(CString(IDS_YES));

	Manual.SetCurSel(pPDO ? pPDO->m_Manual : 0);

	CEditCtrl &Event = (CEditCtrl &) GetDlgItem(2008);

	CString Num = "%u";

	Num.Printf(Num, pPDO && pPDO->m_Manual ? pPDO->m_Event : EVT_DEF);
		
	Event.SetWindowText(Num);
	}

void CCANOpenPDODialog::LoadElementInfo(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(3002);

	CEditCtrl &Sub = (CEditCtrl &) GetDlgItem(3004);

	CComboBox &Size = (CComboBox &) GetDlgItem(3006);

	if( Size.GetCount() == 0 ) {

		Size.AddString(CString(IDS_SPACE_BYTE));

		Size.AddString(CString(IDS_SPACE_WORD));

		Size.AddString(CString(IDS_SPACE_LONG));
		}

	if( m_hSelect == m_hRoot ) {

		Edit.SetWindowText("0000");

		Sub.SetWindowText("00");

		Size.SetCurSel(0);
		
		return;
		}

	CTreeViewItem Item = FindItem();

	INDEX Index = m_pPDO->m_pElements->GetHead();

	while( !m_pPDO->m_pElements->Failed(Index) ) {

		CElement * pElement = (CElement *)m_pPDO->m_pElements->GetItem(Index);

		if( CString(Item.GetText()) == pElement->GetElementString() ) {

			CString Text;

			Text.Printf("%4.4X", pElement->GetElementIndex());

			Edit.SetWindowText(Text);

			Text.Printf("%2.2X", pElement->GetElementSub());

			Sub.SetWindowText(Text);
	
			Size.SetCurSel(pElement->GetElementSizeSel());

			return;
			}

		m_pPDO->m_pElements->GetNext(Index);
		}
	}

// Helpers

BOOL CCANOpenPDODialog::DeleteElement(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	m_hSelect = Tree.GetSelection(); 

	INDEX Index = m_pPDO->m_pElements->GetHead();

	while( !m_pPDO->m_pElements->Failed(Index) ) {

		CElement * pElement = (CElement *)m_pPDO->m_pElements->GetItem(Index);

		if( Tree.GetItemText(m_hSelect) == pElement->GetElementString() ) {

			m_pPDO->m_pElements->RemoveItem(Index);

			Tree.DeleteItem(m_hSelect);

			return TRUE;
			}

		m_pPDO->m_pElements->GetNext(Index);
		}

	return FALSE;
	}

void CCANOpenPDODialog::DoEnables(void)
{
	BOOL fEnable = m_hSelect == m_hRoot;
	
	GetDlgItem(2002).EnableWindow(fEnable);

	GetDlgItem(2004).EnableWindow(fEnable);

	GetDlgItem(3008).EnableWindow(fEnable);

	CComboBox &Type   = (CComboBox &) GetDlgItem(2004);

	CComboBox &Manual = (CComboBox &) GetDlgItem(2006);

	fEnable = Type.GetCurSel();

	for( UINT u = 2005; u <= 2006; u++ ) {

		GetDlgItem(u).EnableWindow(fEnable);
		}

	for( UINT u = 2007; u <= 2008; u++ ) {
		
		GetDlgItem(u).EnableWindow(fEnable && Manual.GetCurSel());
		}
	}

CTreeViewItem CCANOpenPDODialog::FindItem(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	return Tree.GetItem(m_hSelect);
	}

void CCANOpenPDODialog::UpdateItem(CTreeViewItem Item, CString Text)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);
	
	Item.SetText(Text.ToUpper());

	Tree.SetItem(Item);
	}
	
UINT CCANOpenPDODialog::GetEditValue(UINT uID)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(uID);

	UINT uRadix = (uID == 2002 || uID == 2008) ? 10 : 16;

	return tstrtoul(Edit.GetWindowText(), NULL, uRadix);
	}

void CCANOpenPDODialog::ResetManual(void)
{
	if( !m_pPDO->m_Type ) {

		CComboBox & Manual = (CComboBox &)GetDlgItem(2006);

		Manual.SetCurSel(0);

		m_pPDO->m_Manual = FALSE;
		}
	}

// End of File
