
#include "intern.hpp"

#include "bannermb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Banner Modbus TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBannerMbusTCPDeviceOptions, CUIItem);       

// Constructor

CBannerMbusTCPDeviceOptions::CBannerMbusTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// Download Support

BOOL CBannerMbusTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CBannerMbusTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Banner Modbus TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_BannerModbusTCPDriver(void)
{
	return New CBannerModbusTCPDriver;
	}

// Constructor

CBannerModbusTCPDriver::CBannerModbusTCPDriver(void)
{
	m_wID		= 0x351E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Banner";
	
	m_DriverName	= "PresencePLUS Data";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Banner PresencePLUS Data";

	AddSpaces();
	}

// Destructor

CBannerModbusTCPDriver::~CBannerModbusTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CBannerModbusTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBannerModbusTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CBannerModbusTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CBannerModbusTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBannerMbusTCPDeviceOptions);
	}

// Implementation

void CBannerModbusTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SPH, " ",	"Input Flags...",		10, SPINPUTFLAGS, SPMAX, addrBitAsBit));
	AddSpace(New CSpace("IT",	"   Trigger",			CIT,	addrBitAsBit));
	AddSpace(New CSpace("IR",	"   Remote Teach",		CIR,	addrBitAsBit));
	AddSpace(New CSpace("IP",	"   Product Change",		CIP,	addrBitAsBit));
	AddSpace(New CSpace("IS",	"   BCR String Change",		CIS,	addrBitAsBit));

	AddSpace(New CSpace(SPH, " ",	"Output Flags...",		10, SPOUTPUTFLAGS, SPMAX, addrBitAsBit));
	AddSpace(New CSpace("OP",	"   Pass",			COP,	addrBitAsBit));
	AddSpace(New CSpace("OF",	"   Fail",			COF,	addrBitAsBit));
	AddSpace(New CSpace("OE",	"   Error",			COE,	addrBitAsBit));
	AddSpace(New CSpace("OR",	"   Ready",			COR,	addrBitAsBit));
	AddSpace(New CSpace("IO1",	"   I/O 1",			CIO1,	addrBitAsBit));
	AddSpace(New CSpace("IO2",	"   I/O 2",			CIO2,	addrBitAsBit));
	AddSpace(New CSpace("IO3",	"   I/O 3",			CIO3,	addrBitAsBit));
	AddSpace(New CSpace("IO4",	"   I/O 4",			CIO4,	addrBitAsBit));
	AddSpace(New CSpace("IO5",	"   I/O 5",			CIO5,	addrBitAsBit));
	AddSpace(New CSpace("IO6",	"   I/O 6",			CIO6,	addrBitAsBit));

	AddSpace(New CSpace(SPH, " ",	"ACK Flags...",			10, SPACKFLAGS, SPMAX, addrBitAsBit));
	AddSpace(New CSpace("AT",	"   Trigger",			CAT,	addrBitAsBit));
	AddSpace(New CSpace("AR",	"   Remote Teach",		CAR,	addrBitAsBit));
	AddSpace(New CSpace("AP",	"   Product Change",		CAP,	addrBitAsBit));
	AddSpace(New CSpace("AS",	"   BCR String Change",		CAS,	addrBitAsBit));

	AddSpace(New CSpace(SPH, " ",	"Output Registers 3xxxx...",	10, SPOUTPUTR3, SPMAX, addrWordAsWord));
	AddSpace(New CSpace("O3O",	"   Output Flags",		CO3O,	addrWordAsWord));
	AddSpace(New CSpace("O3A",	"   ACK Flags",			CO3A,	addrWordAsWord));
	AddSpace(New CSpace("O3I",	"   Inspection Number",		CO3I,	addrWordAsWord));
	AddSpace(New CSpace("O3E",	"   System Error Count",	CO3E,	addrWordAsWord));
	AddSpace(New CSpace("O3M",	"   Frame Count",		CO3M,	addrLongAsLong));
	AddSpace(New CSpace("O3P",	"   Pass Count",		CO3P,	addrLongAsLong));
	AddSpace(New CSpace("O3F",	"   Fail Count",		CO3F,	addrLongAsLong));
	AddSpace(New CSpace("O3T",	"   Missed Triggers",		CO3T,	addrLongAsLong));
	AddSpace(New CSpace("O3C",	"   Iteration Count",		CO3C,	addrLongAsLong));
	AddSpace(New CSpace(CO3L, "O3L","   Location",			10, 1, 464,	addrLongAsLong));

	AddSpace(New CSpace(SPH, " ",	"Output Registers 41xxx...",	10, SPOUTPUTR4, SPMAX, addrWordAsWord));
	AddSpace(New CSpace("O4O",	"   Output Flags",		CO4O,	addrWordAsWord));
	AddSpace(New CSpace("O4A",	"   ACK Flags",			CO4A,	addrWordAsWord));
	AddSpace(New CSpace("O4I",	"   Inspection Number",		CO4I,	addrWordAsWord));
	AddSpace(New CSpace("O4E",	"   System Error Count",	CO4E,	addrWordAsWord));
	AddSpace(New CSpace("O4M",	"   Frame Count",		CO4M,	addrLongAsLong));
	AddSpace(New CSpace("O4P",	"   Pass Count",		CO4P,	addrLongAsLong));
	AddSpace(New CSpace("O4F",	"   Fail Count",		CO4F,	addrLongAsLong));
	AddSpace(New CSpace("O4T",	"   Missed Triggers",		CO4T,	addrLongAsLong));
	AddSpace(New CSpace("O4C",	"   Iteration Count",		CO4C,	addrLongAsLong));
	AddSpace(New CSpace(CO4L, "O4L","   Location",			10, 1, 464,	addrLongAsLong));

	AddSpace(New CSpace(SPH, " ",	"Input Registers...",		10, SPINPUTR, SPMAX, addrWordAsWord));
	AddSpace(New CSpace("IRI",	"   Input Flags",		CIRI,	addrWordAsWord));
	AddSpace(New CSpace("IRP",	"   Product Select",		CIRP,	addrWordAsWord));
	AddSpace(New CSpace(CIRC, "IRC","   BCR Compare String Regs.",	10,  40, 139,	addrWordAsWord));
	AddSpace(New CSpace(CIRM, "IRM","   BCR Compare Mask Regs.",	10, 140, 239,	addrWordAsWord));

	AddSpace(New CSpace( 1, "4",	"Modbus Register Address",	10, 1,	65535, WW, WL));
	}

BOOL CBannerModbusTCPDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Address Management

BOOL CBannerModbusTCPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	if( fPart && Addr.a.m_Table == SPH ) return FALSE;

	CBannerModbusTCPAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

/////////////////////////////////////////////////////////////////////
//
// Banner Modbus TCP Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CBannerModbusTCPAddrDialog, CStdAddrDialog);
		
// Constructor

CBannerModbusTCPAddrDialog::CBannerModbusTCPAddrDialog(CBannerModbusTCPDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	if( m_uAllowSpace < SPINPUTFLAGS || m_uAllowSpace > SPINPUTR ) m_uAllowSpace = CREG;

	m_Element = "StdElementDlg";
	}

// Message Map

AfxMessageMap(CBannerModbusTCPAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CBannerModbusTCPAddrDialog)
	};

// Message Handlers

BOOL CBannerModbusTCPAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();
	
	if( !m_fPart ) {

		SetCaption();

		SetAllow();

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			if( m_pSpace->m_uTable < SPH ) {

				ShowAddress(Addr);
				ShowDetails();
				LoadType();
				SetDlgFocus(2002);
				}

			else {
				if( m_pSpace->m_uMinimum < HEADER_SPACE_OFFSET ) {

					ShowAddress(Addr);
					ShowDetails();
					LoadType();
					SetDlgFocus(1001);
					}

				else NoAddress(TRUE);
				}

			return FALSE;
			}

		return TRUE;
		}
	else {
		FindSpace();

		LoadType();

		ShowAddress(Addr);

		if( TRUE ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				ShowAddress(Addr);
				}
			}

		SetAddressFocus();

		return FALSE;
		}
	}

// Notification Handlers

void CBannerModbusTCPAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CBannerModbusTCPAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		UINT uAllow = m_uAllowSpace;

		SetAllow();

		BOOL fAllowChange = uAllow != m_uAllowSpace;

		if( m_pSpace->m_uTable == CREG ) LoadType();

		UINT uHasAddr   = HasAddr(m_pSpace);

		CString sOffset = GetAddressText();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			m_pSpace->GetMinimum(Addr);

			GetDlgItem(2001).SetWindowText(uHasAddr ? m_pSpace->m_Prefix : "");

			if( !uHasAddr ) Addr.a.m_Offset = 0;

			sOffset = m_pSpace->GetValueAsText(m_pSpace->m_uMinimum);

			if( fAllowChange ) LoadList();

			switch( uHasAddr ) {

				case 2:
					GetDlgItem(2002).SetWindowText(sOffset);
					GetDlgItem(2002).EnableWindow(TRUE);
					// fall through
				case 1:
					LoadType();
					ShowAddress(Addr);
					ShowDetails();
					break;

				default:
					NoAddress(FALSE);
					break;
				}
			}

		SetDlgFocus( uHasAddr == 2 ? 2002 : 1001 );

		return;
		}

	m_pSpace = NULL;

	m_uAllowSpace = CREG;

	LoadList();

	NoAddress(TRUE);
	}

BOOL CBannerModbusTCPAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		UINT uHasAddr = HasAddr(m_pSpace);

		switch( uHasAddr ) {

			case 0:
				if( m_fPart ) return FALSE;

				m_uAllowSpace = m_pSpace->m_uMinimum;

				LoadList();
				ClearDetails();

				return TRUE;

			case 1:
				break;

			case 2:
				Text += GetDlgItem(2002).GetWindowText();

				if( m_pSpace->m_uTable == CREG && GetTypeCode() != addrWordAsWord ) {

					Text += GetTypeText();
					}

				break;
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( uHasAddr == 2 ? 2002 : 1001 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables

BOOL CBannerModbusTCPAddrDialog::AllowSpace(CSpace *pSpace)
{
	UINT uTable = pSpace->m_uTable;

	if( uTable == CREG || uTable == SPH ) return TRUE;

	if( uTable == addrNamed ) {

		if( m_uAllowSpace == CREG ) return FALSE;

		uTable = HIBYTE(pSpace->m_uMinimum) & 0xF;
		}

	return IsAllowed(uTable, m_uAllowSpace);
	}


BOOL CBannerModbusTCPAddrDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrWordAsWord:
		case addrWordAsLong:
		case addrLongAsLong:	return TRUE;
		}

	return FALSE;
	}

void CBannerModbusTCPAddrDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	if( m_pSpace->m_uTable < addrNamed ) {

		CAddress Addr;

		m_pSpace->GetMinimum(Addr);

		Addr.a.m_Type = Addr.a.m_Table == CREG ? GetTypeCode() : m_pSpace->m_uType;

		m_pDriver->ExpandAddress(Min, m_pConfig, Addr);

		m_pSpace->GetMaximum(Addr);

		m_pDriver->ExpandAddress(Max, m_pConfig, Addr);

		Rad = m_pSpace->GetRadixAsText();
		}

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

// Helpers
void CBannerModbusTCPAddrDialog::SetAllow()
{
	if( m_pSpace ) {

		UINT uTable = m_pSpace->m_uTable;
		UINT uMin   = m_pSpace->m_uMinimum;

		if( uTable == CREG ) {

			m_uAllowSpace = CREG;

			return;
			}

		if( uTable == SPH ) {

			m_uAllowSpace = uMin;

			return;
			}

		for( UINT i = SPINPUTFLAGS; i <= SPINPUTR; i++ ) {

			UINT u = uTable != addrNamed ? uTable : HIBYTE(uMin) & 0xF;

			if( IsAllowed( u, i ) ) {

				m_uAllowSpace = i;

				return;
				}
			}
		}

	m_uAllowSpace = CREG;
	}

BOOL CBannerModbusTCPAddrDialog::IsAllowed(UINT uCommand, UINT uAllowed)
{
	if( uCommand == CIRC || uCommand == CIRM ) return (uAllowed == SPINPUTR);

	return uCommand == (uAllowed & 0xF);
	}

UINT CBannerModbusTCPAddrDialog::HasAddr(CSpace * pSpace)
{
	if( m_pSpace->m_uTable < SPH ) return 2;

	return m_pSpace->m_uMinimum < HEADER_SPACE_OFFSET ? 1 : 0;
	}

void CBannerModbusTCPAddrDialog::NoAddress(BOOL fPutNone)
{
	if( fPutNone ) ClearAddress();
	ClearDetails();
	ClearType();
	GetDlgItem(2002).SetWindowText("");
	GetDlgItem(2002).EnableWindow(FALSE);
	SetDlgFocus(1001);
	}

// End of File
