
#include "intern.hpp"

#include "l5kdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ETHIP2A_HPP
	
#define	INCLUDE_ETHIP2A_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Tag Info
//

typedef CMap  <CString, INDEX> CNameIndex;
typedef CMap  <UINT,    INDEX> CSlotIndex;

struct CEIPTagInfo
{
	CString	m_Name;
	UINT	m_Slot;
	UINT	m_Type;
	UINT	m_uX;
	UINT	m_uY;
	UINT	m_uZ;
	CLongArray m_Dims;
	CAddress m_Addr;
	};

//////////////////////////////////////////////////////////////////////////
//
// L5K Tag List
//

class CEIPL5KList : public CItemList
{
	public:
		CEIPL5KList(void);

		CString	m_Name;
		BOOL	m_fProgram;
	};

//////////////////////////////////////////////////////////////////////////
//
// L5K Tag Entry
//

class CEIPL5KEntry : public CItem
{
	public:
		CEIPL5KEntry(void);

		CString m_Name;
		CString m_Scope;
		CString m_Type;
		CString	m_Alias;
		UINT	m_uType;
		BOOL	m_fStruct;
		BOOL	m_fProgram;
		BOOL	m_fAlias;

		CEIPL5KList * m_pMembers;
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Driver
//

class CEIP2aMasterDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CEIP2aMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL CheckDims(CLongArray const &MaxDims, CLongArray const &Dims);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL IsAddrNamed(CItem *pConfig, CAddress const &Addr);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Driver Options
//

class CEIP2aMasterDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEIP2aMasterDriverOptions(void);

	protected:
		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Device Options
//

class CEIP2aMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEIP2aMasterDeviceOptions(void);

		// Destructor
		~CEIP2aMasterDeviceOptions(void);

		// Attributes
		BOOL IsListFull(void) const;

		// Tag Lookup
		CEIPTagInfo * FindTag(CString Name) const;
		CEIPTagInfo * FindTag(UINT    Slot) const;
		BOOL TagExists(CString Name);

		// Tag Addition
		CEIPTagInfo * AddTag(CString Name, UINT Type, BOOL fNamed);
		BOOL AddTagToDevice(CError &Error, CString Text, UINT uType);
		BOOL LoadFromFile(CFilename Filename);
		BOOL SetSavedFilename(CString const &Name);
		CFilename GetSavedFilename(void);

		// Tag Deletion
		BOOL DeleteTag(CString Name);
		void ClearTags(void);

		// Tag Information
		BOOL IsAtomic(CString const& Type);
		UINT GetAtomicType(CString const &Type);
		void ExpandDims(CString &Text, CLongArray const &Dims, UINT uIndex);
		void ExpandDimsText(CString &Text, CLongArray const &Dims);
		void ExpandDimsIndex(UINT uIndex, CLongArray const &InDims, CLongArray &OutDims);
		BOOL ParseDims(CLongArray &Dims, CString const &Type);

		// Persistance
		void Init(void);
		void PostLoad(void);
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		void Kill(void);

		// Public Data
		UINT	m_IPAddr;
		UINT	m_Port;
		UINT	m_LoadFile;
		UINT    m_Routing;
		CString m_Route;
		CString	m_Path;

		BOOL DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);

	protected:
		// Type Definitions
		typedef CList <CEIPTagInfo *>  CTagList;

		// Data Members
		CTagList   m_Tags;
		CNameIndex m_Names;
		CSlotIndex m_TableSlots;
		CSlotIndex m_NamedSlots;
		UINT	   m_TableAlloc;
		UINT       m_NamedAlloc;
		UINT	   m_Limit;
		UINT	   m_Push;
		BOOL	   m_fL5KInit;
		INDEX	   m_ListIndex;

		CEIPL5KList		* m_pL5KTags;
		CABL5kController	  m_Controller;
		CArray<CEIPL5KList *>	  m_pPrograms;

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		void MakeTagInit(CInitData &Init, INDEX i, UINT &Slot);

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void MakeNameIndex(void);
		void MakeSlotIndex(void);
		BOOL FindLowestSlot(CSlotIndex const &Slots, UINT uStart, UINT uMax);
		UINT FindNextSlot(CSlotIndex const &Slots, UINT uStart, UINT uMax);

		// L5K Support
		void BuildL5KTags(CEIPL5KList *pList, CArray<CABL5kDesc>  const &Tags, CString Base);
		void KillL5KTags(void);
		void DoKillL5KTags(CEIPL5KList *pList);
		void ExpandStructDims(CArray<CABL5kDesc> &ArrMembers, CLongArray const &Dims, CEIPL5KEntry *pEntry, CString const &BaseType);
		void FindAliasType(CEIPL5KEntry *pEntry);
		BOOL FindStructAlias(CEIPL5KEntry *pEntry, CEIPL5KList *pList, UINT uDepth);
		void BuildStructTypes(CEIPL5KEntry *pEntry, CEIPL5KList *pList, CString Base);

		friend class CEIP2aMasterDialog;
		friend class CEIP2aSelectionDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Base Dialog
//

class CEIP2aDialogBase : public CStdDialog
{
	public:
		// Constructor
		CEIP2aDialogBase(CEIP2aMasterDriver &Driver, CItem * pConfig);

	protected:

		// Data Members
		BOOL		  m_fLoading;
		HTREEITEM	  m_hRoot;

		CEIP2aMasterDeviceOptions	* m_pConfig;
		CEIP2aMasterDriver		* m_pDriver;

		// L5K Tag Display
		void LoadTree(CTreeView &Tree, CString const &RootName, CEIPL5KList *pTags, CArray<CEIPL5KList *> const &Programs);
		void LoadRoot(CTreeView &Tree, CString const &RootName);
		void LoadProgramTags(CTreeView &Tree, CArray<CEIPL5KList *> Programs);
		void LoadTags(CTreeView &Tree, CString Root, CEIPL5KList *pTags);
		void LoadTags(CTreeView &Tree, HTREEITEM hRoot, CEIPL5KList *pTags);
		void LoadTag(CTreeView &Tree, HTREEITEM hRoot, CEIPL5KEntry *pTag);

		// UI Management
		void ShowDetails(UINT uMin, UINT uMax, UINT uType, UINT uAlias, UINT uAtom, CEIPL5KEntry const *pEntry);
		void PopulateListBox(CListBox &ListBox, CNameIndex &Names);

		// Tag Management
		BOOL AddTagToDevice(CString Text, UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Management Dialog
//

class CEIP2aMasterDialog : public CEIP2aDialogBase
{
	public:
		AfxDeclareRuntimeClass();

		// Constructor
		CEIP2aMasterDialog(CEIP2aMasterDriver &Driver, CItem * pConfig);

		// Destructor
		~CEIP2aMasterDialog(void);

	protected:
		AfxDeclareMessageMap();

		// Data Members
		CString m_Filename;

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void SetAddressFocus(void);
		BOOL OnOkay(UINT uId);
		BOOL OnAddTag(UINT uId);
		BOOL OnDelTag(UINT uId);
		BOOL OnImport(UINT uID);
		BOOL OnAddL5K(UINT uID);
		void OnTreeDblClk(UINT uID, NMHDR &Info);
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		void OnListSelChanged(UINT uID, CWnd &Wnd);

		// UI Handlers
		void ShowDetails(CEIPL5KEntry const *pEntry);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Address Selection Dialog
//

class CEIP2aSelectionDialog : public CEIP2aDialogBase
{
	public:
		AfxDeclareRuntimeClass();

		// Constructor
		CEIP2aSelectionDialog(CEIP2aMasterDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart, BOOL fSelect);

		// Destructor
		~CEIP2aSelectionDialog(void);

	protected:
		AfxDeclareMessageMap();

		// Data Members
		BOOL		  m_fPart;		
		BOOL		  m_fSelect;
		CAddress	* m_pAddr;
		UINT		  m_uLastFocus;

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uId);
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnTreeDblClk(UINT uID, NMHDR &Info);
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		void OnListSelChanged(UINT uID, CWnd &Wnd);

		// UI Handlers
		void ShowDetails(CEIPL5KEntry const *pEntry);
		void ShowAddress(void);
		void DoEnables(void);
		void DoEnables(CString const &TagName);
		void EnableOffsets(CLongArray const &Dims);
		void InitOffsets(CLongArray const &Dims);
		void UpdateSelectedText(void);

		// Address Management
		void GetSelectedEntry(CEIPL5KEntry &Entry, CTreeView const &Tree, BOOL fTrim);
		void GetSelectedEntry(CEIPL5KEntry &Entry, CListBox const &ListBox);
		BOOL MapTag(CString Text);
		BOOL MapFromTree(CTreeView &Tree, CString Text);
		BOOL DoMapTag(CString Text, CAddress Addr);
	};

// End of File

#endif
