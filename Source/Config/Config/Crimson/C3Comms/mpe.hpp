
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MPE_HPP
	
#define	INCLUDE_MPE_HPP

//////////////////////////////////////////////////////////////////////////
//
// MP Electronics Driver
//

class CMPEDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CMPEDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
