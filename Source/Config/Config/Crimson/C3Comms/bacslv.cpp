
#include "intern.hpp"

#include "bacslv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// BACNet Slave Base Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBACNetSlaveDriverOptions, CUIItem);

// Constructor

CBACNetSlaveDriverOptions::CBACNetSlaveDriverOptions(void)
{
	m_Device = 7000;

	m_Limit  = 100;

	m_Name   = 0;

	m_Desc   = 0;

	m_Rich   = 0;

	m_Port	 = 0xBAC0;

	m_Pass   = "password";
}

// UI Managament

void CBACNetSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

	}
}

// Download Support

BOOL CBACNetSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(m_Device);

	Init.AddText(m_DevName);

	Init.AddWord(WORD(m_Limit));

	Init.AddByte(BYTE(m_Name));

	Init.AddByte(BYTE(m_Desc));

	Init.AddByte(BYTE(m_Rich));

	Init.AddText(m_Pass);

	Init.AddWord(WORD(m_Port));

	return TRUE;
}

// Persistance

void CBACNetSlaveDriverOptions::Init(void)
{
	m_Name = 1;

	m_Desc = 1;

	m_Rich = 1;

	CUIItem::Init();
}

// Meta Data Creation

void CBACNetSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddString(DevName);
	Meta_AddInteger(Limit);
	Meta_AddInteger(Name);
	Meta_AddInteger(Desc);
	Meta_AddInteger(Rich);
	Meta_AddString(Pass);
	Meta_AddInteger(Port);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBACNetMSTPSlaveDriverOptions, CBACNetSlaveDriverOptions);

// Constructor

CBACNetMSTPSlaveDriverOptions::CBACNetMSTPSlaveDriverOptions(void)
{
	m_ThisDrop = 10;

	m_LastDrop = 127;

	m_Optim1   = 0;

	m_Optim2   = 0;

	m_TxFast = 0;
}

// UI Managament

void CBACNetMSTPSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

	}

	CBACNetSlaveDriverOptions::OnUIChange(pWnd, pItem, Tag);
}

// Download Support

BOOL CBACNetMSTPSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CBACNetSlaveDriverOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_ThisDrop));

	Init.AddByte(BYTE(m_LastDrop));

	Init.AddByte(BYTE(m_Optim1));

	Init.AddByte(BYTE(m_Optim2));

	Init.AddByte(BYTE(m_TxFast));

	BOOL fHwDelay = GetDatabase()->HasFlag(L"Canyon");

	Init.AddByte(BYTE(fHwDelay));

	return TRUE;
}

// Meta Data Creation

void CBACNetMSTPSlaveDriverOptions::AddMetaData(void)
{
	CBACNetSlaveDriverOptions::AddMetaData();

	Meta_AddInteger(ThisDrop);
	Meta_AddInteger(LastDrop);
	Meta_AddInteger(Optim1);
	Meta_AddInteger(Optim2);
	Meta_AddInteger(TxFast);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Slave
//

// Constructor

CBACNetSlaveDriver::CBACNetSlaveDriver(void)
{
	m_Manufacturer = "BACnet";

	AddSpaces();
}

// Destructor

CBACNetSlaveDriver::~CBACNetSlaveDriver(void)
{
}

// Configuration

CLASS CBACNetSlaveDriver::GetDeviceConfig(void)
{
	return NULL;
}

// Implementation

void CBACNetSlaveDriver::AddSpaces(void)
{
	AddSpace(New CSpace(100, "AI", "Analog Inputs", 10, 1, 9999, addrRealAsReal, addrLongAsLong));
	AddSpace(New CSpace(101, "AO", "Analog Outputs", 10, 1, 9999, addrRealAsReal, addrLongAsLong));
	AddSpace(New CSpace(102, "AV", "Analog Values", 10, 1, 9999, addrRealAsReal, addrLongAsLong));

	AddSpace(New CSpace(103, "BI", "Binary Inputs", 10, 1, 9999, addrBitAsBit));
	AddSpace(New CSpace(104, "BO", "Binary Outputs", 10, 1, 9999, addrBitAsBit));
	AddSpace(New CSpace(105, "BV", "Binary Values", 10, 1, 9999, addrBitAsBit));

	AddSpace(New CSpace(113, "MI", "Multistate Inputs", 10, 1, 9999, addrBitAsBit, addrBitAsLong));
	AddSpace(New CSpace(114, "MO", "Multistate Outputs", 10, 1, 9999, addrBitAsBit, addrBitAsLong));
	AddSpace(New CSpace(119, "MV", "Multistate Values", 10, 1, 9999, addrBitAsBit, addrBitAsLong));
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Slave
//

// Instantiator

ICommsDriver * Create_BACNet8023Slave(void)
{
	return New CBACNet8023Slave;
}

// Constructor

CBACNet8023Slave::CBACNet8023Slave(void)
{
	m_wID		= 0x4030;

	m_uType		= driverSlave;

	m_DriverName	= "802.3 Slave";

	m_Version	= "1.10";

	m_ShortName	= "BACnet 802.3";

	m_DevRoot	= "DEV";
}

// Binding Control

UINT CBACNet8023Slave::GetBinding(void)
{
	return bindEthernet;
}

void CBACNet8023Slave::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;

	Ether.m_UDPCount = 0;
}

// Configuration

CLASS CBACNet8023Slave::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBACNetSlaveDriverOptions);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Slave
//

// Instantiator

ICommsDriver * Create_BACNetIPSlave(void)
{
	return New CBACNetIPSlave;
}

// Constructor

CBACNetIPSlave::CBACNetIPSlave(void)
{
	m_wID		= 0x4032;

	m_uType		= driverSlave;

	m_DriverName	= "UDP/IP Slave";

	m_Version	= "1.11";

	m_ShortName	= "BACnet UDP/IP";

	m_DevRoot	= "DEV";
}

// Binding Control

UINT CBACNetIPSlave::GetBinding(void)
{
	return bindEthernet;
}

void CBACNetIPSlave::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;

	Ether.m_UDPCount = 1;
}

// Configuration

CLASS CBACNetIPSlave::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBACNetSlaveDriverOptions);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Slave
//

// Instantiator

ICommsDriver * Create_BACNetMSTPSlave(void)
{
	return New CBACNetMSTPSlave;
}

// Constructor

CBACNetMSTPSlave::CBACNetMSTPSlave(void)
{
	m_wID		= 0x4031;

	m_uType		= driverSlave;

	m_DriverName	= "MS/TP Slave";

	m_Version	= "1.11";

	m_ShortName	= "BACnet";

	m_DevRoot	= "DEV";
}

// Binding Control

UINT CBACNetMSTPSlave::GetBinding(void)
{
	return bindRawSerial;
}

void CBACNetMSTPSlave::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 76800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
}

// Configuration

CLASS CBACNetMSTPSlave::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBACNetMSTPSlaveDriverOptions);
}

// End of File
