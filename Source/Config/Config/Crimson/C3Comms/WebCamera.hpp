
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WEB_CAMERA
	
#define	INCLUDE_WEB_CAMERA

/////////////////////////////////////////////////////////////////////////
//
// Image Update Modes
//

enum {
	updatePeriodic = 0,
	updateManual   = 1
	};

/////////////////////////////////////////////////////////////////////////
//
// Image Format Modes
//

enum {
	imgModeAuto = 0,
	imgModeJPG  = 1,
	imgModeBMP  = 2,
	imgModePNG  = 3
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Driver Options
//

class CWebCameraDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWebCameraDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Device Options
//

class CWebCameraDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWebCameraDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		CString m_URI;
		UINT    m_Camera;
		UINT    m_Addr;
		UINT    m_Port;
		UINT    m_Mode;
		UINT    m_Time4;
		UINT    m_Auth;
		UINT    m_Update;
		UINT    m_Period;
		UINT    m_Scale;
		UINT    m_uLastScale;
		CString m_User;
		CString m_Pass;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Driver
//

class CWebCameraDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CWebCameraDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
