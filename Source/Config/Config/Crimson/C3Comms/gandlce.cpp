
#include "intern.hpp"

#include "gandlce.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Giddings and Lewis C/E Driver
//

// Instantiator

ICommsDriver *	Create_GandLCEDriver(void)
{
	return New CGandLCEDriver;
	}

// Constructor

CGandLCEDriver::CGandLCEDriver(void)
{
	m_wID		= 0x401C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Giddings and Lewis";
	
	m_DriverName	= "C/E Controller";
	
	m_Version	= "1.00";
	
	m_ShortName	= "G & L C/E Controller";

	AddSpaces();
	}

// Binding Control

UINT	CGandLCEDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CGandLCEDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

// Address Management

// Address Helpers

// Implementation	

void CGandLCEDriver::AddSpaces(void)
{
	AddSpace(New CSpace("IEXE",	"EXECUTE Initialization...\t( I )",	90 ));
	AddSpace(New CSpace("ISEL",	"...Selector",				 1 ));
	AddSpace(New CSpace("IPHS",	"...Phases",				 2 ));
	AddSpace(New CSpace("IBS1",	"...<unused>",				 3 ));
	AddSpace(New CSpace("IBS2",	"...<unused>",				 4 ));
	AddSpace(New CSpace("ISO",	"...Stabilizer Offset",			 5 ));
	AddSpace(New CSpace("IFFO",	"...Forward Frontstop Coarse Offset",	 6 ));
	AddSpace(New CSpace("IWO",	"...Whip Offset",			 7 ));
	AddSpace(New CSpace("INO",	"...Nip Offset",			 8 ));
	AddSpace(New CSpace("IRFO",	"...Reverse Frontstop Coarse Offset",	 9 ));
	AddSpace(New CSpace("IDSP",	"...DS Side Guide Position",		10 ));
	AddSpace(New CSpace("IOSP",	"...OS Side Guide Position",		11 ));

	AddSpace(New CSpace("PEXE",	"EXECUTE Position Update...\t( P )",	91 ));
	AddSpace(New CSpace("POIP",	"...C/E Offset In Position",		21 ));
	AddSpace(New CSpace("POP",	"...C/E Offset Position",		22 ));
	AddSpace(New CSpace("PSC",	"Status Code",				23 ));
	AddSpace(New CSpace("PFL1",	"Flag Word 1",				24 ));
	AddSpace(New CSpace("PFL2",	"Flag Word 2",				25 ));
	AddSpace(New CSpace("PFL3",	"Flag Word 3",				26 ));
	AddSpace(New CSpace("PWR",	"Whip Resolver",			27 ));
	AddSpace(New CSpace("PFFR",	"Frontstop Fine Resolver",		28 ));
	AddSpace(New CSpace("PFCR",	"Frontstop Coarse Resolver",		29 ));
	AddSpace(New CSpace("PSR",	"Stabilizer Resolver",			30 ));
	AddSpace(New CSpace("PNR",	"Nip Resolver",				31 ));
	AddSpace(New CSpace("POSR",	"OS Side Guide Resolver",		32 ));
	AddSpace(New CSpace("PDSR",	"DS Side Guide Resolver",		33 ));
	AddSpace(New CSpace("PWP",	"Whip Position",			34 ));
	AddSpace(New CSpace("PFP",	"Frontstop Position",			35 ));
	AddSpace(New CSpace("PSP",	"Stabilizer Position",			36 ));
	AddSpace(New CSpace("PNP",	"Nip Position",				37 ));
	AddSpace(New CSpace("POSP",	"OS Side Guide Position",		38 ));
	AddSpace(New CSpace("PDSP",	"DS Side Guide Position",		39 ));

	AddSpace(New CSpace("CEXE",	"EXECUTE Send Commands...\t( C )",	92 ));
	AddSpace(New CSpace("CBCT",	"...Bundle Count",			41 ));
	AddSpace(New CSpace("CUO",	"...Ups/Outs",				42 ));
	AddSpace(New CSpace("CBCL",	"...Board Caliper",			43 ));
	AddSpace(New CSpace("COC",	"...Offset Command",			44 ));
	AddSpace(New CSpace("CWC",	"...Whip Command",			45 ));
	AddSpace(New CSpace("CFC",	"...Frontstop Command",			46 ));
	AddSpace(New CSpace("CSC",	"...Stablilizer Command",		47 ));
	AddSpace(New CSpace("CNC",	"...Nip Command",			48 ));
	AddSpace(New CSpace("COS",	"...OS Side Guide Command",		49 ));
	AddSpace(New CSpace("CDS",	"...DS Side Guide Command",		50 ));

	AddSpace(New CSpace("SS",	"Stop Setup\t\t( S )",			61 ));
	AddSpace(New CSpace("DV",	"R/W Downstacking Value\t( K/k )",	62 ));
	AddSpace(New CSpace("SSV",	"Send Spanking Value\t( s )",		63 ));
	}

// End of File
