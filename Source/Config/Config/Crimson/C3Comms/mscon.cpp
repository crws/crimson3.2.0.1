
#include "intern.hpp"

#include "mscon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMicroscanDeviceOptions, CUIItem);
				   
// Constructor

CMicroscanDeviceOptions::CMicroscanDeviceOptions(void)
{
	m_Drop = 1;
	}

// UI Managament

void CMicroscanDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CMicroscanDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CMicroscanDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CMicroscanDriverOptions, CUIItem);
				   
// Constructor

CMicroscanDriverOptions::CMicroscanDriverOptions(void)
{
	m_Concentrator = 1;
	m_Port         = 1;
	m_TxPreamble   = "";
	m_DatePrefix   = 0;
	m_TimePrefix   = 0;
	m_StampSep     = 32;
	m_StampLoc     = 0;
	m_TxPostamble  = "^M";
	m_TxDrop       = 1;
	m_RxTerm       = 13;
	m_RxTime       = 250;
	m_TxTime       = 1000;
	m_Protocol     = 0;
	m_STX	       = 0x00;
	m_ETX	       = 0x03;
	m_REQ	       = 0x05;
	m_RES	       = 0x04;
	m_ACK	       = 0x06;
	m_NAK	       = 0x15;
	m_LRC	       = 0;
	m_AckNak       = 1;
	m_ReqRes       = 0;
	m_RetryCount   = 3;
	m_RetryInf     = 0;
	}

// UI Loading

void CMicroscanDriverOptions::LoadUI(CUIViewWnd *pWnd, UINT uPage)
{
	#if !defined(C3_BUILD)

	if( uPage == 0 ) {

		pWnd->LoadPage(this, "CMicroscanDriverOptionsUIPage", TRUE);

		pWnd->StartGroup("Additional Options", 1);

		pWnd->AddButton("Click for more configuration...", "", 0x7F00);

		pWnd->EndGroup(TRUE);
		}

	if( uPage == 1 ) {

		pWnd->LoadPage(this, "CMicroscanDriverOptionsUIMain", TRUE);
		}

	m_fTime = (m_TimePrefix || m_DatePrefix);

	#endif
	}

// UI Managament

void CMicroscanDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Button7F00" ) {

			CItemDialog Dialog(this, "Additional Options");

			Dialog.Execute(*pWnd);
			}

		if( Tag.IsEmpty() || Tag == "Concentrator" ) {

			pWnd->GetDlgItem(0x7F00).EnableWindow(m_Concentrator);

			pWnd->EnableUI(this, "Port", m_Concentrator);
			}

		if( Tag.IsEmpty() || Tag == "Protocol" || Tag == "AckNak" || Tag == "ReqRes" || Tag == "RetryInf" ) {

			BOOL fEnable = (m_Protocol > 0);

			pWnd->EnableUI(this, "RxTerm", !fEnable);
			pWnd->EnableUI(this, "AckNak",  fEnable);
			pWnd->EnableUI(this, "ReqRes",  fEnable);

			pWnd->EnableUI(this, "TxTime",   fEnable && m_AckNak);
			pWnd->EnableUI(this, "RetryInf", fEnable && m_AckNak);
			pWnd->EnableUI(this, "TxTime",   fEnable && m_AckNak);
			
			pWnd->EnableUI(this, "RetryCount", fEnable && m_AckNak && !m_RetryInf);

			pWnd->EnableUI(this, "STX", fEnable);
			pWnd->EnableUI(this, "ETX", fEnable);
			pWnd->EnableUI(this, "ACK", fEnable && m_AckNak);
			pWnd->EnableUI(this, "NAK", fEnable && m_AckNak);
			pWnd->EnableUI(this, "REQ", fEnable && m_ReqRes);
			pWnd->EnableUI(this, "RES", fEnable);
			}

		if( Tag.IsEmpty() || Tag == "TimePrefix" || Tag == "DatePrefix" ) {

			BOOL fEnable = (m_TimePrefix || m_DatePrefix);

			pWnd->EnableUI(this, "StampSep", fEnable);
			pWnd->EnableUI(this, "StampLoc", fEnable);

			if( fEnable && !m_fTime ) {

				CString Text = "In order for time-date stamping to operate, you must send the current "
					       "time, date and time zone from your PC using the Link menu. You may then "
					       "enable the Time Manager to allow SNTP time synchronization if this is "
					       "required, but this function will only operate correctly once the time "
					       "zone has been set.";

				pWnd->Information(Text);

				m_fTime = TRUE;
				}
			}
		}
	}

// View Creation

CViewWnd * CMicroscanDriverOptions::CreateItemView(void)
{
	#if !defined(C3_BUILD)

	return New CUIItemViewWnd(1);

	#else

	return NULL;

	#endif
	}

// Download Support

BOOL CMicroscanDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Concentrator));
	Init.AddByte(BYTE(m_Port));
	Init.AddByte(BYTE(m_DatePrefix));
	Init.AddByte(BYTE(m_TimePrefix));
	Init.AddByte(BYTE(m_StampSep));
	Init.AddByte(BYTE(m_StampLoc));

	AddString(Init, m_TxPreamble);
	AddString(Init, m_TxPostamble);

	Init.AddByte(BYTE(m_TxDrop));

	Init.AddByte(BYTE(m_RxTerm));
	Init.AddWord(WORD(m_RxTime));
	Init.AddWord(WORD(m_TxTime));

	Init.AddByte(BYTE(m_Protocol));
	Init.AddByte(BYTE(m_LRC));
	Init.AddByte(BYTE(m_AckNak));
	Init.AddByte(BYTE(m_ReqRes));
	Init.AddByte(BYTE(0));
	Init.AddByte(BYTE(m_RetryInf ? 255 : m_RetryCount));

	Init.AddByte(BYTE(m_STX));
	Init.AddByte(BYTE(m_ETX));
	Init.AddByte(BYTE(m_ACK));
	Init.AddByte(BYTE(m_NAK));
	Init.AddByte(BYTE(m_REQ));
	Init.AddByte(BYTE(m_RES));

	return TRUE;
	}

// Meta Data Creation

void CMicroscanDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Concentrator);
	Meta_AddInteger(Port);
	Meta_AddInteger(DatePrefix);
	Meta_AddInteger(TimePrefix);
	Meta_AddInteger(StampSep);
	Meta_AddInteger(StampLoc);
	Meta_AddString (TxPreamble);
	Meta_AddString (TxPostamble);
	Meta_AddInteger(TxDrop);
	Meta_AddInteger(RxTerm);
	Meta_AddInteger(RxTime);
	Meta_AddInteger(TxTime);
	Meta_AddInteger(Protocol);
	Meta_AddInteger(RES);
	Meta_AddInteger(REQ);
	Meta_AddInteger(STX);
	Meta_AddInteger(ETX);
	Meta_AddInteger(ACK);
	Meta_AddInteger(NAK);
	Meta_AddInteger(LRC);
	Meta_AddInteger(AckNak);
	Meta_AddInteger(ReqRes);
	Meta_AddInteger(RetryCount);
	Meta_AddInteger(RetryInf);
	}

// Implementation

void CMicroscanDriverOptions::AddString(CInitData &Init, CString const &Data)
{
	CString Work;

	for( UINT n = 0; n < Data.GetLength(); n++ ) {

		if( Data[n] == '^' ) {

			if( Data[n+1] == '^' ) {

				Work += char('^');

				n    += 1;

				continue;
				}

			if( Data[n+1] > 65 ) {

				Work += char(Data[n+1] - 64);

				n    += 1;
				
				continue;
				}
			}

		Work += Data[n];
		}

	Init.AddText(Work);
	}

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Driver
//

// Instantiator

ICommsDriver *	Create_MicroscanDriver(void)
{
	return New CMicroscanDriver;
	}

// Constructor

CMicroscanDriver::CMicroscanDriver(void)
{
	m_wID		= 0x402D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Microscan";
			
	m_DriverName	= "Multi-Drop";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Multi-Drop";

	m_DevRoot	= "RDR";

	AddSpaces();
	}

// Binding Control

UINT CMicroscanDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMicroscanDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CMicroscanDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMicroscanDeviceOptions);
	}

CLASS CMicroscanDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMicroscanDriverOptions);
	}

// Implementation     

void CMicroscanDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "C", "Read Code", 10, 1, 600, addrByteAsByte));

	AddSpace(New CSpace(addrNamed, "COUNT", "Read Count",    10, 1, 0, addrLongAsLong));

	AddSpace(New CSpace(addrNamed, "STAMP", "Time Stamp",    10, 2, 0, addrLongAsLong));

	AddSpace(New CSpace(addrNamed, "FRACT", "Time Fraction", 10, 3, 0, addrLongAsLong));

	////////

	AddSpace(New CSpace(2, "R", "Command Reply", 10, 1, 600, addrByteAsByte));

	AddSpace(New CSpace(addrNamed, "CCOUNT", "Command Reply Count", 10, 4, 0, addrLongAsLong));

	AddSpace(New CSpace(addrNamed, "CCODE",  "Command Code",        10, 5, 0, addrLongAsLong));

	AddSpace(New CSpace(addrNamed, "CSEQ",   "Command Sequence",    10, 6, 0, addrLongAsLong));
	}

// End of File
