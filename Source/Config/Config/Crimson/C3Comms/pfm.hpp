
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PFM_HPP
	
#define	INCLUDE_PFM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Plant Floor Marquee Driver
//

class CPFMDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CPFMDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
