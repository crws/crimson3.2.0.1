
#include "intern.hpp"

#include "lecom2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CLecom2MasterSerialDeviceOptions, CUIItem);

// Constructor

CLecom2MasterSerialDeviceOptions::CLecom2MasterSerialDeviceOptions(void)
{
	m_Drop 	= 1;

	m_Ping  = 0;
	}

// Download Support	     

BOOL CLecom2MasterSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddWord(WORD(m_Ping));
	
	return TRUE;
	}

// Meta Data Creation

void CLecom2MasterSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Ping);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 Space Wrapper Class
//

// Constructors

CSpaceLecom2::CSpaceLecom2(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT uSubMin, UINT uSubMax)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= x;
	m_uType		= t;
	m_uSpan		= t;
	m_uSubMin	= uSubMin;
	m_uSubMax	= uSubMax;

	FindAlignment();

	FindWidth();
	}

// Limits

void CSpaceLecom2::GetMinimum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);

	UINT uAddr = m_uMinimum * 100 + m_uSubMin;

	Addr.a.m_Extra  = HIWORD(uAddr);
	Addr.a.m_Offset = LOWORD(uAddr);
	}

void CSpaceLecom2::GetMaximum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);

	UINT uAddr = m_uMaximum * 100 + m_uSubMax;

	Addr.a.m_Extra  = HIWORD(uAddr);
	Addr.a.m_Offset = LOWORD(uAddr);
	}

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 Master Serial Driver
//

// Instantiator

ICommsDriver * Create_Lecom2MasterSerialDriver(void)
{
	return New CLecom2MasterSerialDriver;
	}

// Constructor

CLecom2MasterSerialDriver::CLecom2MasterSerialDriver(void)
{
	m_wID		= 0x3345;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Lenze";
	
	m_DriverName	= "LECOM-A/B Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "LECOM-A/B";	

	AddSpaces();
	}

// Destructor

CLecom2MasterSerialDriver::~CLecom2MasterSerialDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CLecom2MasterSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CLecom2MasterSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CLecom2MasterSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CLecom2MasterSerialDeviceOptions);
	}

// Implementation     

void CLecom2MasterSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpaceLecom2(1, "C",  "Fixed Point Decimal", 10, 0, 9999, addrLongAsLong, 0, 99));
	AddSpace(New CSpaceLecom2(2, "I",  "Integer",		  10, 0, 9999, addrLongAsLong, 0, 99));
	AddSpace(New CSpaceLecom2(3, "H",  "Hexadecimal",	  10, 0, 9999, addrLongAsLong, 0, 99));
	}

// Address Management

BOOL CLecom2MasterSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CLecom2Dialog Dlg(ThisObject, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CLecom2MasterSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uPos  = Text.Find(':');
	
	if( uPos == NOTHING ) {
	
		Error.Set( CString(IDS_DRIVER_ADDR_COLON),
			   0
			   );
			   
		return FALSE;
		}
	else {
		UINT uType = pSpace->m_uType;

		UINT uCode = tatoi(Text.Mid(0, uPos));

		UINT uSub  = tatoi(Text.Mid(uPos + 1));

		if( uCode < pSpace->m_uMinimum || uCode > pSpace->m_uMaximum ) {

			CString Text;

			Text.Printf( CString(IDS_LECOM_CODE_RANGE), 
				     pSpace->m_uMinimum,
				     pSpace->m_uMaximum
				     );
				
			Error.Set( Text,
				   0
				   );
						   
			return FALSE;
			}
		else {
			CSpaceLecom2 *Space2 = (CSpaceLecom2 *) pSpace;

			if( uSub < Space2->m_uSubMin || uSub > Space2->m_uSubMax ) {
				
				CString Text;

				Text.Printf( CString(IDS_LECOM_SUB_RANGE), 
					     Space2->m_uSubMin,
					     Space2->m_uSubMax
					     );
					
				Error.Set( Text,
					   0
					   );

				return FALSE;
				}
			}

		UINT uAddr = uCode * 100 + uSub;

		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Type   = uType;
		Addr.a.m_Extra  = HIWORD(uAddr);
		Addr.a.m_Offset = LOWORD(uAddr);

		return TRUE;
		}
	}

BOOL CLecom2MasterSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpace *pSpace = GetSpace(Addr);

		UINT uAddr = MAKELONG(Addr.a.m_Offset, Addr.a.m_Extra);

		UINT uCode = uAddr / 100;

		UINT uSub  = uAddr % 100;

		Text.Printf( "%s%4.4d:%2.2d", 
			     pSpace->m_Prefix,
			     uCode,
			     uSub
			     );

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 via MPI Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CLecom2Dialog, CStdAddrDialog);
		
// Constructor

CLecom2Dialog::CLecom2Dialog(CLecom2MasterSerialDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "Lecom2ElementDlg";
	}

// Overridables

void CLecom2Dialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CLecom2Dialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	if( fEnable ) {

		UINT uFind = Text.Find(':');

		GetDlgItem(2002).SetWindowText(Text.Left(uFind));

		GetDlgItem(2004).SetWindowText(":");

		GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
		}

	GetDlgItem(2002).EnableWindow(fEnable);
	
	GetDlgItem(2004).EnableWindow(fEnable);
	
	GetDlgItem(2005).EnableWindow(fEnable);
	}

CString CLecom2Dialog::GetAddressText(void)
{
	CString Text;

	Text += GetDlgItem(2002).GetWindowText();

	Text += ":";
	
	Text += GetDlgItem(2005).GetWindowText();

	return Text;
	}

// End of File
