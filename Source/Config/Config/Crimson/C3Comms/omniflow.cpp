
#include "intern.hpp"

#include "omniflow.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Serial Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(COmniFlowDriverOptions, CUIItem);

// Constructor

COmniFlowDriverOptions::COmniFlowDriverOptions(void)
{
	m_Protocol = 0;
	}

// UI Managament

void COmniFlowDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	
	}

// Download Support

BOOL COmniFlowDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Protocol));
	
	return TRUE;
	}

// Meta Data Creation

void COmniFlowDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Protocol);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COmniFlowDeviceOptions, CUIItem);

// Constructor

COmniFlowDeviceOptions::COmniFlowDeviceOptions(void)
{
	m_Drop		= 1;

	m_Ping		= 3001;

	m_fDisable5	= FALSE;

	m_fDisable6	= FALSE;
	}

// UI Managament

void COmniFlowDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	
	}

// Download Support

BOOL COmniFlowDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddWord(WORD(m_Ping));
	Init.AddByte(BYTE(m_fDisable5));
	Init.AddByte(BYTE(m_fDisable6));
	
	return TRUE;
	}

// Meta Data Creation

void COmniFlowDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Ping);
	Meta_AddBoolean(Disable5);
	Meta_AddBoolean(Disable6);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Serial Driver
//

// Instantiator

ICommsDriver *	Create_OmniFlowMasterDriver(void)
{
	return New COmniFlowMasterDriver;
	}

// Constructor

COmniFlowMasterDriver::COmniFlowMasterDriver(void)
{
	m_wID		= 0x40B1;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omni Flow";
	
	m_DriverName	= "Modbus Master";
	
	m_Version	= "1.03";
	
	m_ShortName	= "Omni Flow";

	m_DevRoot	= "OMNI";

	AddSpaces();  

	C3_PASSED();
	}

// Binding Control

UINT COmniFlowMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void COmniFlowMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS COmniFlowMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(COmniFlowDriverOptions);
	}

// Configuration

CLASS COmniFlowMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COmniFlowDeviceOptions);
	}

BOOL COmniFlowMasterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	switch( Addr.a.m_Table ) {

		case SPACE_IS:
		case SPACE_IR:
		case SPACE_RAB:
		case SPACE_CPB:

			return TRUE;

		case SPACE_CPD:

			return IsLogical();
		}

	return FALSE;
	}

// Address Helpers

BOOL COmniFlowMasterDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		UINT uMult = 0;

		switch( Addr.a.m_Table ) {

			case SPACE_HRD:
			case SPACE_STR8:	uMult = 2;	break;
			case SPACE_STR16:	uMult = 4;	break;
			case SPACE_STR32:	uMult = 8;	break;
			}
			
		if( uMult ) {

			UINT uOffset = Addr.a.m_Offset * uMult;

			Addr.a.m_Offset = uOffset & 0xFFFF;

			Addr.a.m_Extra  = uOffset >> 16;
			}

		return TRUE;
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}
		
BOOL COmniFlowMasterDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{	
	UINT uDiv = 0;

	switch( Addr.a.m_Table ) {

		case SPACE_HRD:
		case SPACE_STR8:	uDiv = 2;	break;
		case SPACE_STR16:	uDiv = 4;	break;
		case SPACE_STR32:	uDiv = 8;	break;
		}

	CAddress Address;

	Address.m_Ref = Addr.m_Ref;
			
	if( uDiv ) {

		UINT uOffset = Addr.a.m_Offset | Addr.a.m_Extra << 16;

		Address.a.m_Offset = uOffset / uDiv;

		Address.a.m_Extra  = 0;
		}

	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Address) ) {

		return TRUE;
		}
	
	return FALSE;
	}

// Data Access

BOOL COmniFlowMasterDriver::IsLogical(void)
{
	return m_wID == 0x40B1 || m_wID == 0x40B2;
	}

// Implementation

void COmniFlowMasterDriver::AddSpaces(void)
{
	AddSpace(New COmniSpace(SPACE_CS,	"CS",		"Coil Status",					10, 0,	    65535,	addrBitAsBit,	addrBitAsBit));
	
//	AddSpace(New COmniSpace(SPACE_IS,	"IS",		"Input Status",					10, 0,	    65535,	addrBitAsBit,	addrBitAsLong));
					  
	AddSpace(New COmniSpace(SPACE_HR,	"HR",		"Holding Registers",				10, 0,	    65535,	addrWordAsWord,	addrWordAsReal));
	
//	AddSpace(New COmniSpace(SPACE_IR,	"IR",		"Input Registers",				10, 0,	    65535,	addrWordAsWord,	addrWordAsReal));

	AddSpace(New COmniSpace(SPACE_HRL,	"LHR",		"32-bit Holding Registers",			10,  0,	    65535,	addrLongAsLong,	addrLongAsReal));

	AddSpace(New COmniSpace(SPACE_HRD,	"DHR",		"64-bit Holding Registers",			10,  0,     65535,	addrLongAsLong,	addrLongAsReal));

	AddSpace(New COmniSpace(SPACE_STR8,	"STR8_",	"8 Byte String Data",				10,  0,	    65535,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_STR16,	"STR16_",	"16 Byte String Data",				10,  0,	    65535,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_STR32,	"STR32_",	"32 Byte String Data",				10,  0,	    65535,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_EC,	"EC",		"Exception Code",				10,  1,	        0,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_CPB,	"CPB",		"Manual Custom Packet/Archive Data",		10,  0,	      250,	addrByteAsByte,	addrByteAsByte));

	AddSpace(New COmniSpace(SPACE_CDP,	"CDP",		"Custom/Archive Data Point",			10,  1,	        0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_CDC,	"CRC",		"Custom/Archive Record Count",			10,  1,	        0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_CDR,	"CDR",		"Send Manual Custom/Archive Request",		10,  1,	        0,	addrBitAsBit,	addrBitAsBit));

	AddSpace(New COmniSpace(SPACE_CPD,	"CPD",		"Custom Packet Data (Auto)",			10,  0,	    65535,	addrWordAsWord,	addrWordAsReal)); 

	AddSpace(New COmniSpace(SPACE_BRP,	"BRP",		"ASCII Text Buffer Read Point",			10,  1,		0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_BRR,	"BRR",		"Send ASCII Text Buffer Read Request",		10,  1,		0,	addrBitAsBit,	addrBitAsBit));

	AddSpace(New COmniSpace(SPACE_RAB,	"RAB",		"Read ASCII Text Buffer Data",			10,  0,	128/4*255,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_BWP,	"BWP",		"ASCII Text Buffer Write Point",		10,  1,		0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_BWQ,	"BWQ",		"ASCII Text Buffer Write Quantity (Bytes)",	10,  1,		0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_BWR,	"BWR",		"Send ASCII Text Buffer Write Data",		10,  1,		0,	addrBitAsBit,	addrBitAsBit));

	AddSpace(New COmniSpace(SPACE_WAB,	"WAB",		"Write ASCII Text Buffer Data",			10,  0, 128/4*255,	addrLongAsLong,	addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow TCP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(COmniFlowMasterTCPDriverOptions, CUIItem);

// Constructor

COmniFlowMasterTCPDriverOptions::COmniFlowMasterTCPDriverOptions(void)
{
	
	}

// Download Support

BOOL COmniFlowMasterTCPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);
	
	return TRUE;
	}

// Meta Data Creation

void COmniFlowMasterTCPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}


//////////////////////////////////////////////////////////////////////////
//
// Omni Flow TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COmniFlowMasterTCPDeviceOptions, CUIItem);       

// Constructor

COmniFlowMasterTCPDeviceOptions::COmniFlowMasterTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));;

	m_Port   = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_PingReg = 3001;

	m_fDisable5	= FALSE;

	m_fDisable6	= FALSE;
	}

// UI Managament

void COmniFlowMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL COmniFlowMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{	
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddWord(WORD(m_PingReg));
	Init.AddByte(BYTE(m_fDisable5));
	Init.AddByte(BYTE(m_fDisable6));
	
	return TRUE;
	}

// Meta Data Creation

void COmniFlowMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(PingReg);
	Meta_AddBoolean(Disable5);
	Meta_AddBoolean(Disable6);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Master TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_OmniFlowMasterTCPDriver(void)
{
	return New COmniFlowMasterTCPDriver;
	}

// Constructor

COmniFlowMasterTCPDriver::COmniFlowMasterTCPDriver(void)
{
	m_wID		= 0x40B2;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omni Flow";
	
	m_DriverName	= "TCP/IP Modbus Master";
	
	m_Version	= "1.03";
	
	m_ShortName	= "Omni Flow";

	C3_PASSED();
	}

// Destructor

COmniFlowMasterTCPDriver::~COmniFlowMasterTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT COmniFlowMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void COmniFlowMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS COmniFlowMasterTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(COmniFlowMasterTCPDriverOptions);
	}

// Configuration

CLASS COmniFlowMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COmniFlowMasterTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Space
//

// Constructors

COmniSpace::COmniSpace(void)
{
	}

COmniSpace::COmniSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s) : CSpace(uTable, p, c, r, n, x, t, s)
{

	}

// Overridables

void COmniSpace::GetMaximum(CAddress &Addr)
{
	switch( Addr.a.m_Table ) {

		case SPACE_HRD:
		case SPACE_STR8:
		case SPACE_STR16:
		case SPACE_STR32:

			Addr.a.m_Table	= m_uTable;

			Addr.a.m_Extra	= 0xF;

			Addr.a.m_Offset	= 0xFFFF;

			return;
		}

	CSpace::GetMaximum(Addr);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Modicon Support
//
/////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Modicon Serial Driver
//

// Instantiator

ICommsDriver *	Create_OmniFlowModiconMasterDriver(void)
{
	return New COmniFlowModiconMasterDriver;
	}

// Constructor

COmniFlowModiconMasterDriver::COmniFlowModiconMasterDriver(void)
{
	m_wID		= 0x40B6;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omni Flow";
	
	m_DriverName	= "Modbus Master Modicon Compatible";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Omni Flow";

	m_DevRoot	= "OMNI";

	DeleteAllSpaces();

	AddSpaces();  

	C3_PASSED();
	}

// Destructor

COmniFlowModiconMasterDriver::~COmniFlowModiconMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Implementation

void COmniFlowModiconMasterDriver::AddSpaces(void)
{
	AddSpace(New COmniSpace(SPACE_CS,	"CS",		"Coil Status",					10, 1,	    65535,	addrBitAsBit,	addrBitAsBit));
	
//	AddSpace(New COmniSpace(SPACE_IS,	"IS",		"Input Status",					10, 1,	    65535,	addrBitAsBit,	addrBitAsBit));
					  
	AddSpace(New COmniSpace(SPACE_HR,	"HR",		"Holding Registers",				10, 1,	    65535,	addrWordAsWord,	addrWordAsWord));
	
//	AddSpace(New COmniSpace(SPACE_IR,	"IR",		"Input Registers",				10, 1,	    65535,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_HRL,	"LHR",		"32-bit Holding Registers",			10,  1,	    65535,	addrLongAsLong,	addrLongAsReal));

	AddSpace(New COmniSpace(SPACE_HRD,	"DHR",		"64-bit Holding Registers",			10,  1,     65535,	addrLongAsLong,	addrLongAsReal));

	AddSpace(New COmniSpace(SPACE_STR8,	"STR8_",	"8 Byte String Data",				10,  1,	    65535,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_STR16,	"STR16_",	"16 Byte String Data",				10,  1,	    65535,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_STR32,	"STR32_",	"32 Byte String Data",				10,  1,	    65535,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_EC,	"EC",		"Exception Code",				10,  1,	        0,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_CPB,	"CPB",		"Manual Custom Packet/Archive Data",		10,  0,	      250,	addrByteAsByte,	addrByteAsByte));

	AddSpace(New COmniSpace(SPACE_CDP,	"CDP",		"Custom/Archive Data Point",			10,  1,	        0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_CDC,	"CRC",		"Custom/Archive Record Count",			10,  1,	        0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_CDR,	"CDR",		"Send Manual Custom/Archive Request",		10,  1,	        0,	addrBitAsBit,	addrBitAsBit));

	AddSpace(New COmniSpace(SPACE_CPD,	"CPD",		"Custom Packet Data (Auto)",			10,  1,	    65535,	addrWordAsWord,	addrWordAsReal)); 

	AddSpace(New COmniSpace(SPACE_BRP,	"BRP",		"ASCII Text Buffer Read Point",			10,  1,		0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_BRR,	"BRR",		"Send ASCII Text Buffer Read Request",		10,  1,		0,	addrBitAsBit,	addrBitAsBit));

	AddSpace(New COmniSpace(SPACE_RAB,	"RAB",		"Read ASCII Text Buffer Data",			10,  0,	128/4*255,	addrLongAsLong,	addrLongAsLong));

	AddSpace(New COmniSpace(SPACE_BWP,	"BWP",		"ASCII Text Buffer Write Point",		10,  1,		0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_BWQ,	"BWQ",		"ASCII Text Buffer Write Quantity (Bytes)",	10,  1,		0,	addrWordAsWord,	addrWordAsWord));

	AddSpace(New COmniSpace(SPACE_BWR,	"BWR",		"Send ASCII Text Buffer Write Data",		10,  1,		0,	addrBitAsBit,	addrBitAsBit));

	AddSpace(New COmniSpace(SPACE_WAB,	"WAB",		"Write ASCII Text Buffer Data",			10,  0, 128/4*255,	addrLongAsLong,	addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Modicon Master TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_OmniFlowModiconMasterTCPDriver(void)
{
	return New COmniFlowModiconMasterTCPDriver;
	}

// Constructor

COmniFlowModiconMasterTCPDriver::COmniFlowModiconMasterTCPDriver(void)
{
	m_wID		= 0x40B7;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omni Flow";
	
	m_DriverName	= "TCP/IP Modbus Master Modicon Compatible";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Omni Flow";

	C3_PASSED();
	}

// Destructor

COmniFlowModiconMasterTCPDriver::~COmniFlowModiconMasterTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT COmniFlowModiconMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void COmniFlowModiconMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS COmniFlowModiconMasterTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(COmniFlowMasterTCPDriverOptions);
	}

// Configuration

CLASS COmniFlowModiconMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COmniFlowMasterTCPDeviceOptions);
	}



// End of File
