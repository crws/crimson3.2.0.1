
#include "intern.hpp"

#include "mbtcps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave TCP/IP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CModbusSlaveTCPDriverOptions, CUIItem);

// Constructor

CModbusSlaveTCPDriverOptions::CModbusSlaveTCPDriverOptions(void)
{
	m_Socket   = 502;

	m_Count    = 2;

	m_Restrict = 0;

	m_SecAddr  = 0;

	m_SecMask  = 0;

	m_FlipLong = 1;

	m_FlipReal = 1;

	m_Timeout  = 10000;
	}

// UI Managament

void CModbusSlaveTCPDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Restrict" ) {

		pWnd->EnableUI("SecMask", m_Restrict>0);

		pWnd->EnableUI("SecAddr", m_Restrict>0);
		}
	}

// Download Support

BOOL CModbusSlaveTCPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Socket));
	Init.AddWord(WORD(m_Count));
	Init.AddByte(BYTE(m_Restrict));
	Init.AddLong(LONG(m_SecAddr));
	Init.AddLong(LONG(m_SecMask));
	Init.AddByte(BYTE(m_FlipLong));
	Init.AddByte(BYTE(m_FlipReal));
	Init.AddLong(LONG(m_Timeout));

	return TRUE;
	}

// Meta Data Creation

void CModbusSlaveTCPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Socket);
	Meta_AddInteger(Count);
	Meta_AddInteger(Restrict);
	Meta_AddInteger(SecAddr);
	Meta_AddInteger(SecMask);
	Meta_AddInteger(FlipLong);
	Meta_AddInteger(FlipReal);
	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave TCP/IP Driver
//

// Instantiator

ICommsDriver *	Create_ModbusSlaveTCPDriver(void)
{
	return New CModbusSlaveTCPDriver;
	}

// Constructor

CModbusSlaveTCPDriver::CModbusSlaveTCPDriver(void)
{
	m_wID		= 0x3501;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "TCP/IP Slave";
	
	m_Version	= "1.03";
	
	m_ShortName	= "Modbus TCP/IP Slave";

	DeleteAllSpaces();

	AddSpaces();

	C3_PASSED();
	}

// Binding Control

UINT CModbusSlaveTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CModbusSlaveTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CModbusSlaveTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModbusSlaveTCPDriverOptions);
	}

CLASS CModbusSlaveTCPDriver::GetDeviceConfig(void)
{
	return NULL;
	}


// Implementation

void CModbusSlaveTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "4",  "Holding Registers",		10, 1, 65535, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(2, "3",  "Analog Inputs",			10, 1, 65535, addrWordAsWord, addrWordAsReal));
					  
	AddSpace(New CSpace(3, "0",  "Digital Coils",			10, 1, 65535, addrBitAsBit));
	
	AddSpace(New CSpace(4, "1",  "Digital Inputs",			10, 1, 65535, addrBitAsBit));

	AddSpace(New CSpace(5, "L4", "Holding Registers (32-bit)",	10, 1, 65535, addrLongAsLong, addrLongAsReal));
	
//	AddSpace(New CSpace(6, "L3", "Analog Inputs (32-bit)",		10, 1, 65535, addrLongAsLong, addrLongAsReal)); 
	}


//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Server (Slave) TCP/IP Driver - Multiple Devices
//

// Instantiator

ICommsDriver *	Create_ModbusDeviceServerTCPDriver(void)
{
	return New CModbusDeviceServerTCPDriver;
	}

// Constructor

CModbusDeviceServerTCPDriver::CModbusDeviceServerTCPDriver(void)
{
	m_wID		= 0x4083;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "Device Gateway";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Modbus Gateway";

	DeleteAllSpaces();

	AddSpaces();
	}

CLASS CModbusDeviceServerTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CModbusDeviceServerDeviceOptions);
	}

// End of File
