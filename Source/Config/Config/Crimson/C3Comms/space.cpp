
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Space Definition Wrapper Class
//

// Constructors

CSpace::CSpace(void)
{
	}

CSpace::CSpace(CString p, CString c, UINT n)
{
	m_uTable	= addrNamed;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= 10;
	m_uMinimum	= n;
	m_uMaximum	= n;
	m_uType		= addrLongAsLong;
	m_uSpan         = addrLongAsLong;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(CString p, CString c, UINT n, UINT t)
{
	m_uTable	= addrNamed;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= 10;
	m_uMinimum	= n;
	m_uMaximum	= n;
	m_uType		= t;
	m_uSpan         = t;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= max(n, x);
	m_uType		= t;
	m_uSpan         = t;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= max(n, x);
	m_uType		= t;
	m_uSpan         = s;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT w)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= max(n, x);
	m_uType		= t;
	m_uSpan         = s;
	m_uWidth	= w;

	FindAlignment();
	}

// Legacy Constructors

CSpace::CSpace(PCSTR p, PCSTR c, UINT n)
{
	m_uTable	= addrNamed;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= 10;
	m_uMinimum	= n;
	m_uMaximum	= n;
	m_uType		= addrLongAsLong;
	m_uSpan         = addrLongAsLong;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(PCSTR p, PCSTR c, UINT n, UINT t)
{
	m_uTable	= addrNamed;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= 10;
	m_uMinimum	= n;
	m_uMaximum	= n;
	m_uType		= t;
	m_uSpan         = t;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= max(n, x);
	m_uType		= t;
	m_uSpan         = t;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= max(n, x);
	m_uType		= t;
	m_uSpan         = s;

	FindAlignment();

	FindWidth();
	}

CSpace::CSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT w)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= max(n, x);
	m_uType		= t;
	m_uSpan         = s;
	m_uWidth	= w;

	FindAlignment();
	}

// Destructor

CSpace::~CSpace(void)
{
	}

// Validation

BOOL CSpace::IsNamed(void)
{
	return m_uMinimum >= m_uMaximum;
	}

BOOL CSpace::IsOutOfRange(UINT uOffset)
{
	return uOffset < m_uMinimum || uOffset > m_uMaximum;
	}

BOOL CSpace::IsBadAlignment(UINT uOffset)
{
	return (uOffset - m_uMinimum) % m_uAlign;
	}

BOOL CSpace::IsBadAlignment(UINT uOffset, UINT uType)
{
	UINT uAlign = 1;

	switch( uType ) {

		case addrByteAsWord:
		case addrWordAsLong:
		case addrWordAsReal:
			
			uAlign = 2;

			break;

		case addrByteAsLong:
		case addrByteAsReal:
			
			uAlign = 4;

			break;

		case addrBitAsByte:
			
			uAlign = 8;

			break;

		case addrBitAsWord:
			
			uAlign = 16;

			break;

		case addrBitAsLong:
		case addrBitAsReal:	
			
			uAlign = 32;

			break;
		}

	return (uOffset - m_uMinimum) % uAlign;
	}

// Alignment

void CSpace::FindAlignment(void)
{
	switch( m_uType ) {

		case addrBitAsBit:
		case addrByteAsByte:
		case addrWordAsWord:
		case addrLongAsLong:
		case addrLongAsReal:
		case addrRealAsReal:
			
			m_uAlign = 1;

			return;

		case addrByteAsWord:
		case addrWordAsLong:
		case addrWordAsReal:
			
			m_uAlign = 2;

			return;

		case addrByteAsLong:
		case addrByteAsReal:
			
			m_uAlign = 4;

			return;

		case addrBitAsByte:
			
			m_uAlign = 8;

			return;

		case addrBitAsWord:
			
			m_uAlign = 16;

			return;

		case addrBitAsLong:
		case addrBitAsReal:	
			
			m_uAlign = 32;

			return;

		case addrReserved:

			m_uAlign = 1;

			return;
		}

	AfxAssert(FALSE);
	}

// Type Conversion

UINT CSpace::TypeFromModifier(CString Text)
{
	if( Text == L"BYTE" ) {

		switch( m_uType ) {

			case addrBitAsBit:

				return addrBitAsByte;
			}
		
		return m_uType;
		}
	
	if( Text == L"WORD" ) {

		switch( m_uType ) {

			case addrBitAsBit:
			case addrBitAsByte:

				return addrBitAsWord;

			case addrByteAsByte:

				return addrByteAsWord;
			}
		
		return m_uType;
		}
	
	if( Text == L"LONG" ) {

		switch( m_uType ) {

			case addrBitAsBit:
			case addrBitAsByte:
			case addrBitAsWord:

				return addrBitAsLong;

			case addrByteAsByte:
			case addrByteAsWord:

				return addrByteAsLong;

			case addrWordAsWord:

				return addrWordAsLong;

			case addrRealAsReal:

				return addrLongAsLong;
			}
		
		return m_uType;
		}
	
	if( Text == L"REAL" ) {

		switch( m_uType ) {

			case addrByteAsByte:
			case addrByteAsWord:
			case addrByteAsLong:

				return addrByteAsReal;

			case addrWordAsWord:
			case addrWordAsLong:

				return addrWordAsReal;

			case addrLongAsLong:

				return addrLongAsReal;
			}
		
		return m_uType;
		}
	
	return m_uType;
	}

CString	CSpace::GetTypeModifier(UINT uType)
{
	switch( uType ) {
		
		case addrBitAsByte:
		case addrByteAsByte:
			
			return L"BYTE";

		case addrBitAsWord:
		case addrByteAsWord:
		case addrWordAsWord:
			
			return L"WORD";

		case addrBitAsLong:
		case addrByteAsLong:
		case addrWordAsLong:
		case addrLongAsLong:
			
			return L"LONG";

		case addrBitAsReal:
		case addrByteAsReal:
		case addrWordAsReal:
		case addrLongAsReal:
		case addrRealAsReal:
			
			return L"REAL";
		}

	return L"";
	}

// Printing

CString	CSpace::GetTypeText(void)
{
	return GetTypeAsText(m_uType);
	}

CString	CSpace::GetTypeAsText(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:	return IDS_SPACE_BAB;
		case addrBitAsByte:	return IDS_SPACE_BABY;
		case addrBitAsWord:	return IDS_SPACE_BAW;
		case addrBitAsLong:	return IDS_SPACE_BAL;
		case addrBitAsReal:	return IDS_SPACE_BAR;
		case addrByteAsByte:	return IDS_SPACE_BYABY;
		case addrByteAsWord:	return IDS_SPACE_BYAW;
		case addrByteAsLong:	return IDS_SPACE_BYAL;
		case addrByteAsReal:	return IDS_SPACE_BYAR;
		case addrWordAsWord:	return IDS_SPACE_WAW;
		case addrWordAsLong:	return IDS_SPACE_WAL;
		case addrWordAsReal:	return IDS_SPACE_WAR;
		case addrLongAsLong:	return IDS_SPACE_LAL;
		case addrLongAsReal:	return IDS_SPACE_LAR;
		case addrRealAsReal:	return IDS_SPACE_RAR;
		case addrReserved:	return L"";
		}

	return L"";
	}

CString	CSpace::GetNativeText(void)
{
	switch( m_uType ) {

		case addrBitAsBit:
		case addrBitAsByte:
		case addrBitAsWord:
		case addrBitAsLong:
		case addrBitAsReal:
			
			return IDS_SPACE_BIT;
		
		case addrByteAsByte:
		case addrByteAsWord:
		case addrByteAsLong:
		case addrByteAsReal:
			
			return IDS_SPACE_BYTE;
		
		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:
			
			return IDS_SPACE_WORD;
		
		case addrLongAsLong:
		case addrLongAsReal:
			
			return IDS_SPACE_LONG;
		
		case addrRealAsReal:
			
			return IDS_SPACE_REAL;
		}

	return L"";
	}

CString	CSpace::GetRadixAsText(void)
{
	switch( m_uRadix ) {

		case radixMix:	return IDS_SPACE_MIXED;
		case radixBin:	return IDS_SPACE_BINARY;
		case radixOct:	return IDS_SPACE_OCTAL;
		case radixDec:	return IDS_SPACE_DECIMAL;
		case radixHex:	return IDS_SPACE_HEX;
		}

	return CPrintf(IDS_SPACE_BASE, m_uRadix);
	}

CString	CSpace::GetValueAsText(UINT uValue)
{
	return GetValueAsText(uValue, m_uWidth);
	}

CString	CSpace::GetValueAsText(UINT uValue, UINT uWidth)
{
	CString Text;

	DWORD dwMul = 1;

	for( UINT n = 0; n < uWidth; n++ ) {

		dwMul *= m_uRadix;
		}

	while( dwMul /= m_uRadix ) {
		
		PCTXT pHex = L"0123456789ABCDEF";
			
		UINT uChar = UINT(uValue / dwMul % m_uRadix);
			
		Text      += pHex[uChar];
		}

	return Text;
	}

// Matching

BOOL CSpace::MatchSpace(CAddress const &Addr)
{
	if( m_uTable == Addr.a.m_Table ) {

		if( m_uTable == addrNamed ) {

			if( m_uMinimum < m_uMaximum ) {

				return TRUE;
				}

			if( Addr.a.m_Offset == m_uMinimum ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Limits

void CSpace::GetMinimum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= m_uMinimum;
	}

void CSpace::GetMaximum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= m_uMaximum;
	}

// Helper

void CSpace::FindWidth(void)
{
	UINT uWork = m_uMaximum;

	UINT uWidth;

	for( uWidth = 0; uWork; uWidth++ ) {

		uWork /= m_uRadix;
		}

	m_uWidth = uWidth;
	}

// End of File
