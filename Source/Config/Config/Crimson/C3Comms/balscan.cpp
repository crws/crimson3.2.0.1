
#include "intern.hpp"

#include "balscan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Baldor CANOpen SDO Slave
//

// Instantiator

ICommsDriver * Create_CANOpenBaldorSDOSlaveDriver(void)
{
	return New CCANOpenBaldorSDOSlave;
	}

// Constructor

CCANOpenBaldorSDOSlave::CCANOpenBaldorSDOSlave(void)
{
	m_wID		= 0x340C;

	m_uType		= driverSlave;
	
	m_DriverName	= "Baldor SDO Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Baldor SDO Slave";

	m_DevRoot	= "DEV";

	m_fSingle       = TRUE;
	}

// End of File
