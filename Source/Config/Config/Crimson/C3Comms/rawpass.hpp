
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RAWTCPP_HPP
	
#define	INCLUDE_RAWTCPP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedItem : public CItem { };

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Driver Options
//

class CRawTCPPassiveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRawTCPPassiveDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		CCodedItem *m_pService;
		UINT	    m_Port;
		UINT	    m_Keep;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Driver
//

class CRawTCPPassiveDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CRawTCPPassiveDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
