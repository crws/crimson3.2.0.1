#include "s7ppi.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_S7TCP_HPP
	
#define	INCLUDE_S7TCP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 200 PLC TCP/IP Device Options
//

class CS7IsoTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS7IsoTCPDeviceOptions(void);

		// Persistance
		void PostLoad(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT	m_Addr;
		UINT	m_Port;
		UINT	m_Keep;
		UINT	m_Ping;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;
		UINT	m_Type;
		UINT	m_uConn;
		UINT    m_Slot;
		UINT    m_Rack;
		CString m_Conn;
		CString m_Client;
		UINT    m_uClient;
		UINT	m_BlkOff;
		UINT	m_Addr2;
		UINT    m_TSAP;
		UINT	m_STsap;
		UINT	m_CTsap;
		UINT    m_STsap2;
		UINT	m_CTsap2;
		
					
	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);

		// Helper
		void SetHexadecimal(CUIViewWnd *pWnd, CString Tag, CString &Text, UINT &uTarget);

	}; 

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 200 via ISO TCP/IP Master Driver
//
//

class CS7ISOMasterTCPDriver : public CS7PPI2Driver
{
	public:
		// Constructor
		CS7ISOMasterTCPDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS  GetDeviceConfig(void);

	};

// End of File

#endif
