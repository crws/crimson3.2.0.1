
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DEBUGTCP_HPP
	
#define	INCLUDE_DEBUGTCP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Debug TCP Driver Options
//

class CDebugTcpDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDebugTcpDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Port;
		UINT m_Mode;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Debug TCP Driver
//

class CDebugTcpDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CDebugTcpDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
