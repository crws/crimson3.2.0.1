
#include "intern.hpp"

#include "logix5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CABLogix5Dialog, CStdDialog);
		
// Constructor

CABLogix5Dialog::CABLogix5Dialog(CABLogix5Driver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = (CABLogix5DeviceOptions *) pConfig;

	m_fPart   = fPart;

	m_fSelect = TRUE;
	
#if !defined(C3_VERSION)

	m_Images.Create("CommsManager", 16, 0, IMAGE_BITMAP, 0);

#endif

	SetName(fPart ? TEXT("PartABLogix5Dlg") : TEXT("FullABLogix5Dlg"));
	}

// Destructor

CABLogix5Dialog::~CABLogix5Dialog(void)
{
	afxThread->SetStatusText(TEXT(""));
	}

// Message Map

AfxMessageMap(CABLogix5Dialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown   )
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange    )

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(4002, OnCreateTag )

	AfxMessageEnd(CABLogix5Dialog)
	};

// Message Handlers

BOOL CABLogix5Dialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(m_Caption);

	GetDlgItem(IDCANCEL).EnableWindow(m_fSelect);

	GetDlgItem(IDOK).SetWindowText(m_fSelect ? CString("OK") : CString("Close"));

	if( m_fPart ) {

		ShowAddress(*m_pAddr);

		ShowStatus (*m_pAddr);
		}
	else {
		LoadNames();

		LoadTypes();
		}

	return FALSE;
	}

// Notification Handlers

void CABLogix5Dialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_hSelect  = Info.itemNew.hItem;

	m_dwSelect = Info.itemNew.lParam;

	CAddress Addr = *m_pAddr;
	
	if( IsTable(Addr) ) {
		
		Addr.a.m_Offset = 0;
		}

	if( m_dwSelect == Addr.m_Ref ) {
		
		Addr = *m_pAddr;
		}
	else
		Addr = (CAddress &) m_dwSelect;

	LoadTriplets();
		
	ShowAddress(Addr);
	
	ShowDetails(Addr);

	ShowStatus (Addr);
	}

BOOL CABLogix5Dialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	if( Info.wVKey == VK_DELETE ) {
		
		if( IsDown(VK_SHIFT) ) {

			return DeleteTag();
			}
		}

	if( Info.wVKey == VK_F2 ) {		
		
		return RenameTag();
		}

	return FALSE;
	}

void CABLogix5Dialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

	// cppcheck-suppress unreadVariable

	DWORD       Data = Combo.GetCurSelData();

	CAddress    Addr = (CAddress &) Data;

	GetDlgItem(4003).EnableWindow(AllowArray(Addr));
	}

// Command Handlers

BOOL CABLogix5Dialog::OnOkay(UINT uID)
{
	if( m_fSelect ) {
	
		CAddress Addr;
		
		if( !m_fPart ) {

			if( !m_dwSelect ) {

				m_pAddr->m_Ref = 0;

				EndDialog(TRUE);

				return TRUE;
				}

			Addr = (CAddress &) m_dwSelect;
			}
		else 
			Addr = *m_pAddr;

		CString Name;

		m_pConfig->ExpandAddress(Name, Addr);

		CString Text;

		Text += m_pConfig->GetTagName(Name);

		Text += GetOffsetText(Addr);

		Text += GetTripletText(Addr);

		CError Error(TRUE);

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {
			
			*m_pAddr = Addr;

			m_pConfig->SetDirty();
			
			EndDialog(TRUE);
			
			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus(2002);

		return TRUE;
		}

	EndDialog(TRUE);
	
	return TRUE;
	}

BOOL CABLogix5Dialog::OnCreateTag(UINT uID)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(4001);

	CString     Name = m_pConfig->CreateName();

	DWORD       Data = Combo.GetCurSelData();

	CStringDialog Dlg(CString("Create Tag"), CString("Tag Name"), Name);

	if( !IsDown(VK_SHIFT) ) {
		
		if( Dlg.Execute(ThisObject) ) {

			Name = Dlg.GetData();
			}
		else
			return TRUE;
		}

	CAddress   Addr = (CAddress &) Data;

	CButton &Button = (CButton &) GetDlgItem(4003);

	if( Button.IsChecked() ) {

		m_pConfig->MakeArray(Addr);
		}

	CError Error(TRUE);

	if( m_pConfig->CreateTag(Error, Name, Addr, TRUE) ) {

		CError Error(FALSE);

		if( m_pConfig->ParseAddress(Error, Addr, Name) ) {
			
			CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);
			
			LoadTag(Tree, Name, Addr);
			
			Tree.Expand(m_hRoot, TVE_EXPAND);
			
			Tree.SetFocus();
			
			m_pConfig->SetDirty();
			}
		else
			AfxAssert(FALSE);
		}
	else
		Error.Show(ThisObject);

	return TRUE;
	}

// Initialisation

void CABLogix5Dialog::SetCaption(CString const &Text)
{
	m_Caption = Text;
	}

void CABLogix5Dialog::SetSelect(BOOL fSelect)
{
	m_fSelect = fSelect;
	}

// Tree Loading

void CABLogix5Dialog::LoadNames(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_NOHSCROLL
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_EDITLABELS
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

#if defined(C3_VERSION)

	Tree.SendMessage(TVM_SETUNICODEFORMAT, 1);
	
#else
	
	Tree.SendMessage(TVM_SETUNICODEFORMAT);

#endif
	
	LoadNames(Tree);

	Tree.SetFocus();
	}

void CABLogix5Dialog::LoadNames(CTreeView &Tree)
{
	LoadRoot(Tree);
	
	LoadTags(Tree);

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CABLogix5Dialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	Node.SetText(m_pConfig->GetDeviceName());

	Node.SetParam(FALSE);

	Node.SetImages(3);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CABLogix5Dialog::LoadTags(CTreeView &Tree)
{
	CTagDataL5Array List;
	
	m_pConfig->ListTags(List);

	List.Sort();

	m_fLoad = TRUE;

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CTagDataL5 const &Data = List[n];

		LoadTag(Tree, Data.m_Name, (CAddress &) Data.m_Addr);
		}

	m_fLoad = FALSE;
	}

void CABLogix5Dialog::LoadTag(CTreeView &Tree, CString Name, CAddress Addr)
{
	CTreeViewItem Node;

	Node.SetText(Name);

	Node.SetParam(Addr.m_Ref);

	Node.SetImages(0x34);

	HTREEITEM hNode = Tree.InsertItem(m_hRoot, NULL, Node);

	if( m_fLoad ) {

		CAddress Test = *m_pAddr;

		if( m_pConfig->IsTable(Test) ) {
			
			Test.a.m_Offset = 0;
			}		

		if( Addr.m_Ref == Test.m_Ref ) {	

			Tree.SelectItem(hNode, TVGN_CARET);
			}
		}
	else 
		Tree.SelectItem(hNode, TVGN_CARET);
	}

// Data Type Loading

void CABLogix5Dialog::LoadTypes(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(4001);

	Combo.SetRedraw(FALSE);

	AddBool(Combo);

	AddByte(Combo);

	AddWord(Combo);

	AddLong(Combo);

	AddReal(Combo);

	/*AddString(Combo);*/

	AddTimer(Combo);
	
	AddCounter(Combo);
	
	AddControl(Combo);

	Combo.SetCurSel(0);

	Combo.SetRedraw(TRUE);

	OnTypeChange(4001, Combo);
	}

void CABLogix5Dialog::AddReal(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeReal(Addr) ) {

		Combo.AddString( "Real", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddLong(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeLong(Addr) ) {

		Combo.AddString( "Long", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddWord(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeWord(Addr) ) {

		Combo.AddString( "Word", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddByte(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeByte(Addr) ) {

		Combo.AddString( "Byte", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddBool(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeBool(Addr) ) {

		Combo.AddString( "Boolean", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddString(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeString(Addr) ) {

		Combo.AddString( "String", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddTimer(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeTimer(Addr) ) {

		Combo.AddString( "Timer", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddCounter(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeCounter(Addr) ) {

		Combo.AddString( "Counter", LPARAM(Addr.m_Ref) );
		}
	}

void CABLogix5Dialog::AddControl(CComboBox &Combo)
{
	CAddress Addr;

	if( m_pConfig->MakeControl(Addr) ) {

		Combo.AddString( "Control", LPARAM(Addr.m_Ref) );
		}
	}

// Tripet Text Loading

void CABLogix5Dialog::LoadTriplets(void)
{
	CAddress Addr = (CAddress &) m_dwSelect;

	if( IsTriplet(Addr) ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(2003);

		Combo.SetRedraw(FALSE);

		CStringArray List;

		m_pConfig->GetTriplets(Addr, List);

		for( UINT n = 0; n < List.GetCount(); n ++ ) {
			
			Combo.AddString(List[n], n);
			}

		UINT uOffset = m_pConfig->GetOffset(Addr);

		Combo.SetCurSel(uOffset % 3);

		Combo.SetRedraw(TRUE);
		}
	}

// Implementation

void CABLogix5Dialog::ShowDetails(CAddress Addr)
{
	if( Addr.m_Ref ) {

		CString  Type = GetTypeAsText(Addr);

		CString   Min;

		CString   Max;

		m_pConfig->ExpandAddress(Min, Addr);

		if( IsArray(Addr) ) {

			Addr.a.m_Offset = 9999;

			m_pConfig->ExpandAddress(Max, Addr);
			}
		else
			Max = Min;

		CString Info;
		
		m_pConfig->ExpandType(Info, Addr);
			
		GetDlgItem(3002).SetWindowText(Type);
		GetDlgItem(3004).SetWindowText(Min);
		GetDlgItem(3006).SetWindowText(Max);
		GetDlgItem(3008).SetWindowText(Info);
		
		return;
		}

	GetDlgItem(3002).SetWindowText("");
	GetDlgItem(3004).SetWindowText("");
	GetDlgItem(3006).SetWindowText("");
	GetDlgItem(3008).SetWindowText("");
	}

void CABLogix5Dialog::ShowAddress(CAddress Addr)
{
	if( Addr.m_Ref ) {

		CString Name;

		m_pConfig->ExpandAddress(Name, Addr);

		CString  Tag = m_pConfig->GetTagName(Name);

		GetDlgItem(2001).SetWindowText(Tag);

		UINT uOffset = m_pConfig->GetOffset(Addr, Name);

		if( IsTriplet(Addr) ) {
			
			CComboBox &Combo = (CComboBox &) GetDlgItem(2003);

			Combo.SelectData(uOffset % 3);
			
			uOffset /= 3;
			}

		if( IsArray(Addr) ) {
			
			GetDlgItem(2002).SetWindowText(CPrintf("%d", uOffset));
			}

		GetDlgItem(2002).ShowWindow(IsArray(Addr));
			
		GetDlgItem(2003).ShowWindow(IsTriplet(Addr));

		GetDlgItem(2002).EnableWindow(m_fSelect);

		GetDlgItem(2003).EnableWindow(m_fSelect);

		return;
		}

	GetDlgItem(2001).SetWindowText("None");

	GetDlgItem(2002).ShowWindow(FALSE);

	GetDlgItem(2003).ShowWindow(FALSE);
	}

void CABLogix5Dialog::ShowStatus(CAddress Addr)
{
	if( FALSE ) {

		if( Addr.m_Ref ) {

			CString Text;

			Text.Printf("[%X]", Addr.m_Ref);

			CString Name;

			m_pConfig->ExpandAddress(Name, Addr);

			Text += " - " + Name;

			Text += CPrintf(", idx %d", m_pConfig->GetIndex(Addr));

			afxThread->SetStatusText(Text);
			
			return;
			}

		afxThread->SetStatusText(TEXT(""));
		}
	}

CString CABLogix5Dialog::GetTypeAsText(CAddress Addr)
{
	return CSpace("", "", 0).GetTypeAsText(Addr.a.m_Type);
	}

CString	CABLogix5Dialog::GetOffsetText(CAddress Addr)
{
	if( IsArray(Addr)) {

		CString Text;
		
		Text += sepTableOpen;
		
		Text += GetDlgItem(2002).GetWindowText();

		Text += sepTableClose;
		
		return Text;
		}

	return "";
	}

CString	CABLogix5Dialog::GetTripletText(CAddress Addr)
{
	if( IsTriplet(Addr)) {

		CString Text;

		Text += ".";
		
		Text += GetDlgItem(2003).GetWindowText();

		return Text;
		}

	return "";
	}

BOOL CABLogix5Dialog::DeleteTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString  Name;

		m_pConfig->ExpandAddress(Name, Addr);

		CString Text  = CPrintf("Do you really want to delete %s ?", m_pConfig->GetTagName(Name));

		if( NoYes(Text) == IDYES ) {

			CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

			Tree.SetRedraw(FALSE);

			HTREEITEM hPrev = m_hSelect;

			Tree.MoveSelection();

			m_pConfig->DeleteTag(Addr, TRUE);

			Tree.DeleteItem(hPrev);

			Tree.SetRedraw(TRUE);

			m_pConfig->SetDirty();

			return TRUE;
			}		
		}

	return FALSE;
	}

BOOL CABLogix5Dialog::RenameTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString Name;

		if( m_pConfig->ExpandAddress(Name, Addr) ) {

			CString Title   = "Rename Tag";
			
			CString Group   = "Tag Name";

			CString TagName = m_pConfig->GetTagName(Name);

			CStringDialog Dlg(Title, Group, TagName);

			if( Dlg.Execute(ThisObject) ) {

				CTreeView &Tree = (CTreeView &) GetDlgItem(1001);
				
				CString    Name = Dlg.GetData();			

				if( m_pConfig->RenameTag(Name, Addr) ) {

					Tree.SetRedraw(FALSE);

					HTREEITEM hPrev = m_hSelect;

					Tree.MoveSelection();

					Tree.DeleteItem(hPrev);

					LoadTag(Tree, Name, Addr);

					Tree.SetRedraw(TRUE);

					m_pConfig->SetDirty();
					
					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Address Attributes

BOOL CABLogix5Dialog::IsTable(CAddress const &Addr)
{
	return m_pConfig->IsTable(Addr);
	}

BOOL CABLogix5Dialog::IsArray(CAddress const &Addr)
{
	return m_pConfig->IsArray(Addr);
	}

BOOL CABLogix5Dialog::IsTriplet(CAddress const &Addr)
{
	return m_pConfig->IsTriplet(Addr);
	}

BOOL CABLogix5Dialog::AllowArray(CAddress const &Addr)
{
	return m_pConfig->AllowArray(Addr);
	}

// Implementation

BOOL CABLogix5Dialog::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
	}

// End of File
