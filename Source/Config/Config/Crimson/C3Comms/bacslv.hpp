
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BACSLV_HPP
	
#define	INCLUDE_BACSLV_HPP

//////////////////////////////////////////////////////////////////////////
//
// BACNet Slave Base Driver Options
//

class CBACNetSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBACNetSlaveDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT    m_Device;
		CString m_DevName;
		UINT    m_Limit;
		UINT	m_Name;
		UINT	m_Desc;
		UINT	m_Rich;
		UINT    m_Port;
		CString m_Pass;

	protected:
		// Persistance
		void Init(void);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet Slave MS/TP Driver Options
//
			
class CBACNetMSTPSlaveDriverOptions : public CBACNetSlaveDriverOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBACNetMSTPSlaveDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_ThisDrop;
		UINT m_LastDrop;
		UINT m_Optim1;
		UINT m_Optim2;
		UINT m_TxFast;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Slave
//

class CBACNetSlaveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBACNetSlaveDriver(void);

		// Destructor
		~CBACNetSlaveDriver(void);
	
		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Slave
//

class CBACNet8023Slave : public CBACNetSlaveDriver
{
	public:
		// Constructor
		CBACNet8023Slave(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Slave
//

class CBACNetIPSlave : public CBACNetSlaveDriver
{
	public:
		// Constructor
		CBACNetIPSlave(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Slave
//

class CBACNetMSTPSlave : public CBACNetSlaveDriver
{
	public:
		// Constructor
		CBACNetMSTPSlave(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
	};

// End of File

#endif
