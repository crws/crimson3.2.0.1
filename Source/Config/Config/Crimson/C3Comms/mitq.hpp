
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MITQ_HPP
	
#define	INCLUDE_MITQ_HPP


//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series TCP/IP Master
//

class CMitsQMasterTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMitsQMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);		
		BOOL IsExtendedAddress(UINT uTable);

	protected:
		// Implementation
		void AddSpaces(void);

	};
 
//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series TCP/IP Device Options
//

class CMitsQMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMitsQMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Code;
		UINT m_Monitor;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_Net;
		UINT m_Plc;
		UINT m_Cpu;

	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series Address Selection Dialog
//

class CMitsubQDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CMitsubQDialog(CStdCommsDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
		// Overridables
		BOOL AllowType(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series Space Wrapper
//

class CMitsQSpace : public CSpace
{	
	public:
		// Constructors
		CMitsQSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);
		CMitsQSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Limits
		void GetMaximum(CAddress &Addr);
	};

// End of File

#endif
