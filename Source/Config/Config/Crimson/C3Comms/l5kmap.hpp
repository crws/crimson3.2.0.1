
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_L5KMAP_HPP
	
#define	INCLUDE_L5KMAP_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE TEXT(__FILE__)

//////////////////////////////////////////////////////////////////////////
//
// Data Mapper Wrapper
//

template <typename CData> class CMapper
{
	public:
		CMapper(void);

		// Attributes
		UINT GetCount(void) const;

		// Operations
		void Empty(void);
		UINT Append(CData const &Data);
		UINT Find(CData const &Data) const;

		// Lookup Operator
		CData const & operator [] (UINT uIndex) const;

	protected:
		// String Map
		typedef CMap   <CData, UINT> CDataMap;
		typedef CArray <INDEX>       CIndexList;
		
		// Data
		CDataMap	m_DataMap;
		CIndexList	m_Lookup;
		UINT		m_uCount;
	};

/////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CData>

#define	TP2 CMapper <CData>

//////////////////////////////////////////////////////////////////////////
//
// Data Mapper Wrapper
//

TP1 TP2::CMapper(void)
{
	Empty();
	}

// Attributes

TP1 UINT TP2::GetCount(void) const
{
	return m_uCount;
	}

// Operations

TP1 void TP2::Empty(void)
{
	m_DataMap.Empty();
	
	m_Lookup .Empty();

	m_uCount = 0;
	}

TP1 UINT TP2::Append(CData const &Name)
{
	m_DataMap.Insert(Name, m_uCount);

	INDEX Index = m_DataMap.FindName(Name);

	if( !m_DataMap.Failed(Index) ) {

		m_Lookup.Append(Index);

		return m_uCount ++;
		}

	return NOTHING;
	}

TP1 UINT TP2::Find(CData const &Name) const
{
	INDEX Index = m_DataMap.FindName(Name);

	if( !m_DataMap.Failed(Index) ) {
	    
		return m_DataMap.GetData(Index);
		}

	return NOTHING;
	}

// Lookup Operator

TP1 CData const & TP2::operator [] (UINT uIndex) const
{
	INDEX  Index = m_Lookup[uIndex];

	return m_DataMap.GetName(Index);
	}

// End of File

#endif
