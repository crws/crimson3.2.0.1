
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMSMDRIVE_HPP

#define	INCLUDE_IMSMDRIVE_HPP

#define	AN	addrNamed
#define	LL	addrLongAsLong

// Function Code Definition
#define	FZ		0x7F00 // Section Title
#define	LISTMOTION	0x00
#define	LISTIO		0x20
#define	LISTPOSITION	0x30
#define	LISTENCODER	0x40
#define	LISTMISC	0x50

#define	SPMOTION	0x7E00
#define	SPIO		0x7E01
#define	SPPOSITION	0x7E02
#define	SPENCODER	0x7E03
#define	SPMISC		0x7E04

#define	A0		0x4130
#define zz		0x7A7A

#define	SPNONE		0x2000
	
//////////////////////////////////////////////////////////////////////////
//
// IMS MDrive Device Options
//

class CIMSMDriveDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIMSMDriveDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		CString	m_DName;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Intelligent Motion Systems MDrive Driver
//

class CIMSMDriveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIMSMDriveDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);

		// Friend
		friend class CACTechSSAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Intelligent Motion Systems Address Selection
//

class CIMSMDriveAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CIMSMDriveAddrDialog(CIMSMDriveDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		UINT	m_uAllowSpace;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overridables
		void	ShowAddress(CAddress Addr);
		void	ShowDetails();

		// Helpers
		BOOL	AllowSpace(CSpace *pSpace);
		BOOL	SelectList(CSpace *pSpace, UINT uCommand, UINT uAllow);
		void	SetAllow(void);
		UINT	IsHeader(UINT uCommand);
		UINT	GetTableSpace(CSpace *pSpace);
	};

// End of File

#endif
