
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TOSVERT_HPP
	
#define	INCLUDE_TOSVERT_HPP

//////////////////////////////////////////////////////////////////////////
//
// Toshiba G3 Tosvert-130 Inverter Device Options
//

class CTosvertMasterSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTosvertMasterSerialDeviceOptions(void);
				
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
	
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Toshiba G3 Tosvert-130 Inverter Driver
//

class CTosvertMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CTosvertMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);


		// Implementation
		void AddSpaces(void);
		BOOL CheckAlignment(CSpace *pSpace);
	};


// End of File

#endif
