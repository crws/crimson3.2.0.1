
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KOLLMORGEN600_HPP

#define	INCLUDE_KOLLMORGEN600_HPP

#define	AN	addrNamed
#define	BT	addrBitAsBit
#define	BB	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	SPIT	0 // Space - Item Type
#define	SPAT	1 // Space - Alpha Header
#define	SPFT	2 // Space - Function Header
#define	SPLT	3 // Space - List Selection Header

#define	SELFN	0 // Function List Selected
#define	SELAL	1 // Alpha List Selected
#define	SELKW	2 // Keyord List Selected

#define	HMN	0x7001	// Minimum Header Maximum

#define	LFN	0x7130	// By Function List Selection
#define	LAL	0x7131	// Alphabetic List Selection

#define	ABA	0x7001	// Alphabetic Header Maximums
#define	ABB	0x7002	// Alphabetic Header Maximums
#define	ABC	0x7003	// Alphabetic Header Maximums
#define	ABD	0x7004	// Alphabetic Header Maximums
#define	ABE	0x7005	// Alphabetic Header Maximums
#define	ABF	0x7006	// Alphabetic Header Maximums
#define	ABG	0x7007	// Alphabetic Header Maximums
#define	ABH	0x7008	// Alphabetic Header Maximums
#define	ABI	0x7009	// Alphabetic Header Maximums
#define	ABJ	0x700A	// Alphabetic Header Maximums
#define	ABK	0x700B	// Alphabetic Header Maximums
#define	ABL	0x700C	// Alphabetic Header Maximums
#define	ABM	0x700D	// Alphabetic Header Maximums
#define	ABN	0x700E	// Alphabetic Header Maximums
#define	ABO	0x700F	// Alphabetic Header Maximums
#define	ABP	0x7010	// Alphabetic Header Maximums
#define	ABQ	0x7011	// Alphabetic Header Maximums
#define	ABR	0x7012	// Alphabetic Header Maximums
#define	ABS	0x7013	// Alphabetic Header Maximums
#define	ABT	0x7014	// Alphabetic Header Maximums
#define	ABU	0x7015	// Alphabetic Header Maximums
#define	ABV	0x7016	// Alphabetic Header Maximums
#define	ABW	0x7017	// Alphabetic Header Maximums
#define	ABX	0x7018	// Alphabetic Header Maximums
#define	ABY	0x7019	// Alphabetic Header Maximums
#define	ABZ	0x701A	// Alphabetic Header Maximums

#define	HAV	0x7101	// Actual Values Header Maximums
#define	HAM	0x7102	// Amplifier Header Maximums
#define	HAI	0x7103	// Analog I/O Header Maximums
#define	HBS	0x7104	// Basic Setup Header Maximums
#define	HCM	0x7105	// Communication Header Maximums
#define	HCU	0x7106	// Current Header Maximums
#define	HDI	0x7107	// Digital I/O Header Maximums
#define	HDS	0x7108	// Drive Status Header Maximums
#define	HFB	0x710A	// Feedback Header Maximums
#define	HGR	0x710B	// Gearing Header Maximums
#define	HMT	0x710C	// Motor Header Maximums
#define	HOS	0x710D	// Oscilloscope Header Maximums
#define	HPO	0x710E	// Position Header Maximums
#define	HVE	0x710F	// Velocity Header Maximums

#define	NAV	0x6001	// Actual Values Named Items Maximums
#define	NAM	0x6002	// Amplifier Named Items Maximums
#define	NAI	0x6003	// Analog I/O Named Items Maximums
#define	NBS	0x6004	// Basic Setup Named Items Maximums
#define	NCM	0x6005	// Communication Named Items Maximums
#define	NCU	0x6006	// Current Named Items Maximums
#define	NDI	0x6007	// Digital I/O Named Items Maximums
#define	NDS	0x6008	// Drive Status Named Items Maximums
#define	NFB	0x600A	// Feedback Named Items Maximums
#define	NGR	0x600B	// Gearing Named Items Maximums
#define	NMT	0x600C	// Motor Named Items Maximums
#define	NOS	0x600D	// Oscilloscope Named Items Maximums
#define	NPO	0x600E	// Position Named Items Maximums
#define	NVE	0x600F	// Velocity Named Items Maximums


// Selection ID's
// In AddSpaces - //FB indicates a fieldbus command that is not implemented

#define	ACC		1	// "Acceleration", @@WW
#define	ACCR		2	// "Acceleration - Home/Jog", @@WW
#define	ACCUNIT		3	// "Type of acceleration command", @@LL
#define	ACTFAULT	4	// "Active Fault Mode", @@BB
#define	ACTIVE		5	//R "Output stage active/inhibited", @@BB
#define	ACTRS232	6	// "Activate RS232 Watchdog", @@BB
#define	ADDR		7	// "Multidrop Address", @@BB
#define	ADDRFB		8	// "Fieldbus Address - Drive 400 Slave", @@WW
#define	AENA		9	// "Auto-Enable", @@BB
#define	ALIAS		10	// "Drive Name String", @@LL
#define	AN10TX		11	// "Additional Torque Tuning Value", @@WW
#define	AN11NR		12	// "Max. # of change-able INxTRIG", @@BB
#define	AN11RANGE	13	// "Range of the analog change of INxTRIG", @@LL
#define	ANTRIG		14	//Table "Analog Output Scaling", @@LL
#define	ANCNFG		15	// "Analog Input Configuration", @@BB
#define	ANDB		16	// "Dead Band of Analog Velocity Input", @@RR1
#define	ANIN		17	//Table "Analog Input Voltage", @@LL
#define	ANOFF		18	//Table "Analog Input Offset", @@WW
#define	ANOUT		19	//Table "Analog Output Configuration", @@BB
#define	ANZERO		20	//Table W "Analog Input Zero", @@BIT
#define	AUTOHOME	21	// "Automatic Homing", @@BB
#define	AVZ		22	//Table "Analog Output Filter Time Constant", @@RR1

#define	CALCRK		23	//*Manu. Use Only* W "Calculate Resolver Parameters", @@BIT
#define	CBAUD		24	// "CAN Bus Baud Rate", @@WW
#define	CLRFAULT	25	//W "Clear Drive Fault", @@BIT
#define	CLRHR		26	//W "Bit 5 of STAT is cleared", @@BIT
#define	CLRORDER	27	//W "Delete a Motion Task", @@WW
#define	CLRWARN		28	// "Warning Mode", @@BB
#define	COLDSTART	29	//W "Drive Reset", @@BIT

#define	DAOFFSET	30	//Table "Analog Output Offset", @@WW
#define	DEC		31	// "Deceleration", @@WW
#define	DECDIS		32	// "Deceleration - Disable Output", @@WW
#define	DECR		33	// "Deceleration Ramp - Home/Jog", @@WW
#define	DECSTOP		34	// "Fast Stop Ramp", @@WW
#define	DEVICE		35	// Table "Device ID Numeric Data", @@LL
#define	DICONT		36	//R "Continuous Current", @@RR1
#define	DIPEAK		37	//R "Peak Rated Current", @@RR1
#define	DIR		38	// "Count Direction", @@BB
#define	DIS		39	//W "Disable", @@BIT
#define	DREF		40	// "Homing Direction", @@BB
#define	DRVSTAT		41	// "Internal Status", @@LL
#define	EN		42	// "Enable", @@BIT
#define	ENCIN		43	// "Encoder Pulse Input", @@LL
#define	ENCLINES	44	// "SinCos Encoder Resolution", @@WW
#define	ENCMODE		45	// "Encoder Emulation", @@BB
#define	ENCOUT		46	// "Encoder Emulation Resolution", @@LL
#define	ENZERO		47	// "Zero Pulse Offset", @@WW
#define	ERRCODE		48	// Table "Fault Message String (16 Chars.)", @@LL
#define	ERRCODEA	49	// "Output Error Register", @@LL
#define	EXTMUL		50	// "External Encoder Multiplier", @@WW
#define	EXTWD		51	// "External Fieldbus Watchdog", @@LL
#define	ERSP		226	// "Response Error String", @@LL

#define	FBTYPE		52	// "Encoder/Resolver Selection", @@BB
#define	FLASH		53	// "Send Data to External Flash", @@BB
#define	FLTCNT		54	//R Table33 "Fault Frequency", @@LL
#define	FLTHIST		55	//R Table20 "Fault History", @@LL
#define	FLUXM		56	// "Rated Flux", @@RR

#define	GEARI		57	// "Gearing Input Factor", @@WW
#define	GEARMODE	58	// "Secondary Position Source", @@BB
#define	GEARO		59	// "Gearing Output Factor", @@WW
#define	GP		60	// "Proportional Gain - Position Loop", @@RR1
#define	GPFBT		61	// "Feed Forward - Actual Current", @@RR1
#define	GPFFV		62	// "Feed Forward - Velocity", @@RR1
#define	GPTN		63	// "Integral - Position Loop", @@RR1
#define	GPV		64	// "Proportional Gain - Velocity Controller", @@RR1
#define	GV		65	// "Proportional Gain - Velocity Loop", @@RR1
#define	GVFBT		66	// "First Order TC - Velocity Loop", @@RR1
#define	GVFILT		67	// "% Output Filtered - Velocity Loop", @@BB
#define	GVFR		68	// "Feed Forward - Actual Velocity", @@RR1
#define	GVT2		69	// "Second TC - Velocity Loop", @@RR1
#define	GVTN		70	// "Integral - Velocity Loop", @@RR1

#define	HVER		71	//R Table13 "Hardware Version Data", @@LL

#define	ICURR		72	//R "Current", @@RR
#define	I2T		73	//R "RMS Current Loading %", @@LL
#define	I2TLIM		74	// "I2T Warning %", @@BB
#define	ICMD		75	//R "Current Command", @@RR
#define	ICONT		76	// "Rated Current", @@RR
#define	ID		77	//R "D-Component of Current Monitor", @@RR
#define	IMAX		78	//R "Current Limit for Drive/Motor", @@RR
#define	INAD		225	//R "A/D Channels Input counts"
#define	INS		79	//R Table4 "Digital Input Status", @@BB
#define	INMODE		80	//Table4 "Digital Input Function", @@BB
#define	INTRIG		81	//Table4 "INMODE Trigger Data", @@LL
#define	INPOS		82	//R "In-Position Status", @@BB
#define	INPT		83	// "In-Position Delay", @@WW
#define	IPEAK		84	// "Peak Current - Application", @@RR1
#define	IQ		85	//R "Q-Component of Current Monitor", @@RR
#define	ISCALE		86	//Table2 "Analog Current Scaling", @@RR1

#define	KILL		87	//W "Kill", @@BIT
#define	KC		88	// "I-Controller Prediction Current", @@RR1
#define	KEYLOCK		89	// "Lock the Push Buttons", @@BB
#define	KTN		90	// "Integral - Current Controller", @@RR1

#define	LIND		91	// "Stator Inductance", @@RR1
#define	LATCH		92	//R Table2 "Latched 32/16-Bit Position (DRVSTAT)", @@LL
#define	LATCHX		93	//R Table2 "Latched 32/16-Bit Position (TRJSTAT)", @@LL
#define	LTCH16		220	// "16 bit Position @ INx Rising" NOV 2008 S300
#define	LTCH32		221	// "32 bit Position @ INx Rising" NOV 2008 S300
#define	LED		94	// Table 3 "LED Display", @@BB
#define	LEDSTAT		95	// "Display Page", @@WW
#define	LOAD		96	//W "Load Parameters from EEPROM", @@BIT
#define	MAXTEMPE	97	// "Switch off - Ambient �C", @@WW
#define	MAXTEMPH	98	// "Switch off - Heat Sink �C", @@WW
#define	MAXTEMPM	99	// "Switch off - Motor (Ohms)", @@RR1
#define	MBRAKE		100	// "Motor Holding Brake Select", @@BB
#define	MDBCNT		101	//R "Number of Motor Data Sets", @@BB
#define	MDBGET		102	//R "Get Actual Motor Data Set String", @@LL
#define	MDBSET		103	// "Set Actual Motor Data Set", @@WW
#define	MH		104	//W "Start Homing", @@BIT
#define	MICONT		105	// "Motor Continuous Current Rating", @@RR1
#define	MIPEAK		106	// "Motor Peak Current Rating", @@RR1
#define	MJOG		107	//W "Start Jog Mode", @@BIT
#define	MKT		108	// "Motor KT", @@RR1
#define	MLGC		109	// "Adaptive Gain Q-rated - Current Loop", @@RR1
#define	MLGD		110	// "Adaptive Gain D - Current Loop", @@RR1
#define	MLGP		111	// "Adaptive Gain Q-peak - Current Loop", @@RR1
#define	MLGQ		112	// "Adaptive Gain Absolute - Current Loop", @@RR1
#define	MNAME		113	// "Motor Name String", @@LL
#define	MNUMBER		114	// "Motor Number", @@WW
#define	MONITOR		115	// Table2 "Monitor Output Voltage", @@WW
#define	MOVE		116	// "Start Motion Task", @@WW
#define	MPHASE		117	// "Motor Phase, Feedback Offset", @@WW
#define	MPOLES		118	// "Number of Motor Poles", @@BB
#define	MRD		119	//W "Homing to Resolver Zero, Mode 5", @@BIT
#define	MRESBW		120	// "Resolver Bandwidth", @@WW
#define	MRESPOLES	121	// "Number of Resolver Poles", @@BB
#define	MSPEED		122	// "Maximum Rated Motor Velocity", @@RR1
#define	MTANGLP		123	// "Current Lead", @@WW
#define	MTMUX		214	// "Presetting for Motion Task"
#define	MTMUXR		218	// "MTMUX Write Response String"
#define	MUNIT		124	// "Units for Velocity Parameters", @@BB
#define	MVANGLB		125	// "Velocity Lead (Start Phi)", @@LL
#define	MVANGLF		126	// "Velocity Lead (Limit Phi)", @@WW
#define	MVANGLP		127	// "Velocity Lead (Commutation Angle)", @@WW

#define	NONBTB		128	// "Mains-BTB Check On/Off", @@BB
#define	NREF		129	// "Homing Mode", @@BB

#define	O_ACC		130	// Table2 "Acceleration Time - Motion Task 0", @@WW
#define	O_C		131	// "Control Variable - Motion Task 0", @@WW
#define	O_DEC		132	// Table2 "Deceleration Time - Motion Task 0", @@WW
#define	O_FN		133	// "Next Task Number - Motion Task 0", @@WW
#define	O_FT		134	// "Delay before Next Motion Task", @@WW
#define	O_P		135	// "Target Position - Motion Task 0", @@LL
#define	O_V		136	// "Target Speed - Motion Task 0", @@LL
#define	OUTS		137	//  Table3 "Digital Output Status", @@BB
#define	OCOPYQ		215	// "Execute OCOPY <data> = Quantity"
#define	OCOPYS		216	// "...OCOPY Source Task Number"
#define	OCOPYD		217	// "...OCOPY Destination Task Number"
#define	OCOPYR		219	// "OCOPY Write Response String"
#define	OMODE		138	//Table3 "Digital Output Function", @@BB
#define	OTRIG		139	//Table3 "OMODE Trigger Data", @@LL
#define	OPMODE		140	// "Operating Mode", @@BB
#define	OPTION		141	//R "Option Slot ID", @@WW


#define	PASSCM		222	// "Parameter Change Password" NOV 2008 S300
#define	PBAL		142	// "Regen Power - Actual", @@LL
#define	PBALMAX		143	// "Regen Power - Maximum", @@LL
#define	PBALRES		144	// "Regen Resistor - Select", @@BB
#define	PBAUD		145	// "Profibus Baud Rate", @@RR1
#define	PE		146	//R "Following Error - Actual", @@LL
#define	PEINPOS		147	// "In-Position Window", @@LL
#define	PEMAX		148	// "Following Error - Maximum", @@LL
#define	PFB		149	// "Actual Position from Feedback", @@LL
#define	PFB0		150	//R "Position from External Encoder", @@LL
#define	PGEARI		151	// "Position Resolution - Numerator", @@LL
#define	PGEARO		152	// "Position Resolution - Denominator", @@LL
#define	PMODE		153	// "Line Phase Error Mode", @@BB
#define	POSCNFG		154	// "Axes Type", @@BB
#define	PRD		155	//R "20-bit Position Feedback", @@LL
#define	PSTATE		156	//R "Profibus State String", @@LL
#define	PTMIN		157	// "Min. Acceleration for Motion Tasks", @@WW
#define	PUNIT		158	// "Position Resolution", @@LL
#define	PV		159	//R "Actual Velocity - Position Loop", @@LL
#define	PVMAX		160	// "Max. Velocity - Position Loop", @@LL
#define	PVMAXN		161	// "Max. Neg Velocity - Position Loop", @@LL
#define	PVMAXP		223	// "Maximum Positive Velocity" NOV 2008 S300

#define	READY		162	//R "Software Enable Status", @@BB
#define	REFIP		163	// "Peak Rated Current for Homing 7", @@RR1
#define	REFPOS		164	// "Reference Switch Position", @@LL
#define	REMOTE		165	//R "Hardware Enable Status", @@BB
#define	RESPHASE	166	// "Resolver Phase", @@WW
#define	RK		167	// "Resolver Sine Gain Adjust", @@WW
#define	ROFFS		168	// "Reference Offset", @@LL
#define	ROFFSA		224	// "Offset to Encoder Position" NOV 2008 S300
#define	RS232T		169	// "RS232 Watchdog Time", @@
#define	RSTVAR		170	//W "Restore Variables to Default", @@BIT

#define	S_STOP		171	//W "Stop Motor and Disable Drive", @@BIT
#define	SAVE		172	//W "Save Data in EEPROM", @@BIT
#define	SCANX		173	//W "Restart Communications", @@BIT
#define	SERIALNO	174	//R "Drive Serial Number", @@LL
#define	SETREF		175	//W "Set Reference Point", @@BIT
#define	SLOTIO		176	//R "I/O States - Expansion Card", @@LL
#define	SSIGRAY		177	// "SSI Code Select", @@BB
#define	SSIINV		178	// "SSI Clock", @@BB
#define	SSIMODE		179	// "SSI Mode", @@BB
#define	SSIOUT		180	// "SSI Baud Rate", @@BB
#define	STAT		181	//R "Drive Status Word", @@WW
#define	STATCODE	182	//R "Status Variable Warnings", @@LL
#define	STATIO		183	//R Table8 "I/O Status", @@BB
#define	STATUS		184	//R Table5 "Detailed Amplifier Status", @@WW
#define	STOP		185	//W "Stop Motion Task", @@BIT
#define	SWCNFG		186	// "Position Register 1...4 Configuration", @@WW
#define	SWCNFG2		187	// "Position Register 0 & 5 Configuration", @@WW
#define	SWE		188	//Table 6 "Position Register Data", @@LL
#define	SWEN		189	//Table 6 "Cam Position Register Data", @@LL
#define	TCURR		190	// "Digital Current Command", @@RR1
#define	TEMPE		191	//R "Ambient Temperature", @@LL
#define	TEMPH		192	//R "Heat Sink Temperature", @@LL
#define	TEMPM		193	//R "Motor Temperature", @@LL
#define	TRJSTAT		194	//R "Status 2", @@LL
#define	TRUN		195	//R "Run-Time Counter String", @@LL
#define	UID		196	// "User ID", @@WW
#define	VEL		197	//R "Actual Velocity", @@RR
#define	VBUS		198	//R "DC-bus Voltage", @@LL
#define	VBUSBAL		199	// "Maximum Line Voltage", @@WW
#define	VBUSMAX		200	// "Maximum DC-bus Voltage", @@LL
#define	VBUSMIN		201	// "Minimum DC-bus Voltage", @@LL
#define	VCMD		202	//R "Internal Velocity RPM", @@RR
#define	VER		203	//R Table13 "Firmware Version String", @@LL
#define	VJOG		204	// "Jog Mode Speed", @@LL
#define	VLIM		205	// "Maximum Velocity", @@RR1
#define	VMAX		206	//R "Maximum System Speed", @@RR
#define	VMIX		207	// "Velocity Mix", @@RR1
#define	VMUL		208	// "Velocity Scale Factor", @@LL
#define	VOSPD		209	// "Overspeed", @@RR1
#define	VREF		210	// "Homing Speed", @@LL
#define	VSCALE		211	// Table2 "Velocity Scaling - Analog Input", @@WW

#define	WMASK		212	// "Warning as Fault Mask", @@LL
#define	USRDEF		213	// "User Defined Command String", @@LL

// Commands added Nov 2008 for S300/S700
//	MTMUX		214	// "Presetting for Motion Task"
//	OCOPYQ		215-217	// "Execute OCOPY <data> = Quantity"
//	MTMUXR		218	// "MTMUX Write Response String"
//	OCOPYR		219	// "OCOPY Write Response String"

//	LTCH16		220	// "16 bit Position @ INx Rising"
//	LTCH32		221	// "32 bit Position @ INx Rising"
//	PASSCM		222	// "Parameter Change Password"
//	PVMAXP		223	// "Maximum Positive Velocity"
//	ROFFSA		224	// "Offset to Encoder Position"
//	INAD		225	// "A/D Channels Input counts"
//	ERSP		226	// "Response Error String"


#define	HTMIN		235	// Minimum Header Table Address
#define	HAL		235	// Alpha Header Table Address
#define	HFN		236	// Function Header Table Address
#define	LSA		237	// Select Alpha Table Address
#define	LSF		238	// Select Function Table Address
#define	KEYW		239	// "LIST - KEYWORD"

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen600 Driver
//

class CKollmorgen600Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CKollmorgen600Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Data
		BOOL	m_fList;

		// Implementation
		void	AddSpaces(void);

		// Helpers
		BOOL	IsStringItem(UINT uTable);
		CSpace * GetSpace(CAddress const &Addr);
		BOOL MatchSpace(CSpace *pSpace, CAddress const &Addr);

		// Friend
		friend class CKollmorgen600AddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen600 Address Selection
//

class CKollmorgen600AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CKollmorgen600AddrDialog(CKollmorgen600Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		UINT	m_uAllowSpace;
		UINT	m_uListSel;
		BOOL	m_fPart;
		BOOL	m_fListTypeChange;
		CString	m_KWText;
		CKollmorgen600Driver * m_pKMDriver;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// List Selection
		void	LoadList(void);
		void	LoadEntry(CListBox * pBox, CString p, CString c, DWORD n);
		void	DoDesignatedHeader(CListBox * pBox);
		INDEX	DoSelections(CListBox * pBox);
		INDEX	DoItems(CListBox * pBox);
		INDEX	DoKeyWd(CListBox * pBox);
		void	DoRemainingHeaders(CListBox * pBox, BOOL fKeyDone);
		BOOL	AllowSpace(CSpace *pSpace);

		// Helpers
		void	SetAllow(void);
		BOOL	SelectList(UINT uCommand, UINT uAllow);
		UINT	ListMember(UINT uCommand);
		UINT	SelectTableItemList(UINT uTable);
		BOOL	NeedDefaultSpace(UINT uSel);
		UINT	SelectDefaultSpace(void);
		BOOL	IsAlphaHeader(UINT uSel);
		BOOL	IsFunctHeader(UINT uSel);
		BOOL	IsHeader(UINT uSel);
		UINT	HeaderSpaceType(CSpace * pSpace);
		UINT	GetHeaderSpace(UINT uTable, UINT uMax);
		BOOL	ListSelectMade(CSpace * pSpace);
		void	ClearInfo(UINT uClear);
		BOOL	DoShowAddress(CAddress Addr);
	};

// End of File

#endif
