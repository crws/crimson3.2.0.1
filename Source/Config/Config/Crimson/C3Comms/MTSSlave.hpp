
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MTSSLAVE_HPP
	
#define	INCLUDE_MTSSLAVE_HPP

class CMTSAsciiSlaveDriver;

//////////////////////////////////////////////////////////////////////////
//
// MTS Ascii Slave Comms Driver
//

class CMTSAsciiSlaveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMTSAsciiSlaveDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
