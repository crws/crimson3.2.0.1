
#include "intern.hpp"

#include "iairobo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IAI Robocylinder Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIairoboDeviceOptions, CUIItem);

// Constructor

CIairoboDeviceOptions::CIairoboDeviceOptions(void)
{
	m_Drop	 = 1;
	}

// UI Managament

void CIairoboDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CIairoboDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CIairoboDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// IAI Robocylinder Comms Driver
//

// Instantiator

ICommsDriver *	Create_IairoboDriver(void)
{
	return New CIairoboDriver;
	}

// Constructor

CIairoboDriver::CIairoboDriver(void)
{
	m_wID		= 0x3393;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Intelligent Actuator";
	
	m_DriverName	= "Robocylinder/E-Con";
	
	m_Version	= "1.10";
	
	m_ShortName	= "Intelligent Actuator Robocylinder";

	AddSpaces();
	}

// Binding Control

UINT	CIairoboDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CIairoboDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CIairoboDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIairoboDeviceOptions);
	}

// Implementation

void	CIairoboDriver::AddSpaces(void)
{
	AddSpace( New CSpace(1,	"MEM",  "Read/Write Memory (R4,RM,WM)",		16,	0, 0x7FFF, LL ) );
	AddSpace( New CSpace("S",	"Status Inquiry (n)",				2,	LL) );
	AddSpace( New CSpace("STS",	"Status",					3,	LL) );
	AddSpace( New CSpace("ALM",	"Alarms",					4,	LL) );
	AddSpace( New CSpace("INP",	"Input Status",					5,	LL) );
	AddSpace( New CSpace("OUT",	"Output Status",				6,	LL) );
	AddSpace( New CSpace("A",	"Absolute Move (a)",				7,	LL) );
	AddSpace( New CSpace("C",	"Absolute Move mm (c)",				31,	LL) );
	AddSpace( New CSpace("D",	"Stop Motion (d)",				8,	LL) );
	AddSpace( New CSpace("E",	"Increment Move mm (e)",			32,	LL) );
	AddSpace( New CSpace("FVE",	"   Velocity Data mm",				33,	LL) );
	AddSpace( New CSpace("FAC",	"   Acceleration Data mm",			34,	LL) );
	AddSpace( New CSpace("F",	"Send FVE/FAC mm (f)",				35,	LL) );
	AddSpace( New CSpace("I",	"Position Band Change (i)",			29,	LL) );
	AddSpace( New CSpace("K",	"Decelerate and Stop (k)",			36,	LL) );
	AddSpace( New CSpace("M",	"Increment Move (m)",				9,	LL) );
	AddSpace( New CSpace("O",	"Home to 'HP' (o)",				10,	LL) );
	AddSpace( New CSpace("HP",	"Home position (0 -> Motor/Forward)",		37,	LL) );
	AddSpace( New CSpace("Q",	"Servo On/Off (q)",				11,	LL) );
	AddSpace( New CSpace("Q3",	"Position Move (Q3)",				12,	LL) );
	AddSpace( New CSpace("R",	"Alarm Reset (r)",				30,	LL) );
	AddSpace( New CSpace("VVE",	"   Velocity Data",				13,	LL) );
	AddSpace( New CSpace("VAC",	"   Acceleration Data",				14,	LL) );
	AddSpace( New CSpace("V",	"Send VVE/VAC (v)",				15,	LL) );
	AddSpace( New CSpace("Q1",	"Move Point Data->Edit Area (Q1)",		17,	LL) );
	AddSpace( New CSpace("T",	"Send Address Allocation (T4)",			19,	LL) );
	AddSpace( New CSpace("T4R",	"Address Allocation Response",			20,	LL) );
	AddSpace( New CSpace("W",	"Send Data (W4)",				22,	LL) );
	AddSpace( New CSpace("W4R",	"Data Write Response",				23,	LL) );
	AddSpace( New CSpace("V5",	"Move Edit Area->Point (V5)",			25,	LL) );
	AddSpace( New CSpace("V5R",	"Edit Area->Point Response",			26,	LL) );
	AddSpace( New CSpace("X",	"Write 1 Point Item (XQ1-XT4-XW4-XV5)",		27,	LL) );
	AddSpace( New CSpace("XQ1",	"   Source Point Number",			16,	LL) );
	AddSpace( New CSpace("XT4",	"   Address Allocation",			18,	LL) );
	AddSpace( New CSpace("XW4",	"   Data Write",				21,	LL) );
	AddSpace( New CSpace("XV5",	"   Destination Point Number",			24,	LL) );
	AddSpace( New CSpace("XA",	"Write P/V/A/L to Point (XQ1-(Add)-XWx-XV5)",	42,	LL) );
	AddSpace( New CSpace("XWP",	"   Position for XA (Add=0x400)",		38,	LL) );
	AddSpace( New CSpace("XWV",	"   Velocity for XA (Add=0x404)",		39,	LL) );
	AddSpace( New CSpace("XWA",	"   Acceleration for XA (Add=0x405)",		40,	LL) );
	AddSpace( New CSpace("XWM",	"   Moving Current Limit for XA (Add=0x407)",	41,	LL) );
	AddSpace( New CSpace("XOK",	"Status of Point Data Write",			28,	LL) );
	}

// End of File
