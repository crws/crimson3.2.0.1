
#include "intern.hpp"

#include "df1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CDF1MasterSerialDriverOptions, CUIItem);

// Constructor

CDF1MasterSerialDriverOptions::CDF1MasterSerialDriverOptions(void)
{
	m_Address = 0;

	m_fHalf   = FALSE;
	
	m_fCRC    = TRUE;
	}

// Download Support

BOOL CDF1MasterSerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Address));
	Init.AddByte(BYTE(m_fHalf));
	Init.AddByte(BYTE(m_fCRC));

	return TRUE;
	}

// Meta Data Creation

void CDF1MasterSerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Address);
	Meta_AddBoolean(Half);
	Meta_AddBoolean(CRC);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDF1MasterSerialDeviceOptions, CUIItem);

// Constructor

CDF1MasterSerialDeviceOptions::CDF1MasterSerialDeviceOptions(void)
{
	m_Device = 1;

	m_Dest	 = 1;

	m_Time1	 = 1000; 

	m_Time2	 = 500;

	m_Root   = "";

	m_Name   = 0x02;

	m_Label  = 0x7C;
	}

// Download Support	     

BOOL CDF1MasterSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddByte(BYTE(m_Dest));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	
	return TRUE;
	}

// Meta Data Creation

void CDF1MasterSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Dest);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddString(Root);
	Meta_AddInteger(Name);
	Meta_AddInteger(Label);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Base Driver
//

// Instantiator

ICommsDriver *	Create_DF1Driver(void)
{
	return New CDF1Driver;
	}

// Constructor

CDF1Driver::CDF1Driver(void)
{
	m_wID		= 0x3365;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "DF1 Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "DF1 Master";	

	AddSpaces();

	C3_PASSED();
	}

// Binding Control

UINT CDF1Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CDF1Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CDF1Driver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDF1MasterSerialDriverOptions);
	}

// Configuration

CLASS CDF1Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDF1MasterSerialDeviceOptions);
	}

// Implementation     

void CDF1Driver::AddSpaces(void)
{
	AddSpace(New CSpaceAB(1, "O", "Outputs",	10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(2, "I", "Inputs",		10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(3, "S", "Status",		10, 0, 9999, addrWordAsWord, AB_FILE_NONE, AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(4, "B", "Bits",		10, 0, 9999, addrWordAsWord, 3,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(5, "N", "Integers",	10, 0, 9999, addrWordAsWord, 7,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(6, "T", "Timers",		10, 0, 9999, addrWordAsWord, 4,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));

	AddSpace(New CSpaceAB(7, "C", "Counters",	10, 0, 9999, addrWordAsWord, 5,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));
	
	AddSpace(New CSpaceAB(8, "F", "Floating Point", 10, 0, 9999, addrLongAsReal, 8,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(9, "L", "Long Word",	10, 0, 9999, addrLongAsLong, 9,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(10,"R", "Strings",	10, 0,  124, addrByteAsByte, 10,	   AB_FILE_MIN, AB_FILE_MAX));
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Serial Slave Driver
//

// Instantiator

ICommsDriver *	Create_DF1SerialSlaveDriver(void)
{
	return New CDF1SerialSlaveDriver;
	}

// Constructor

CDF1SerialSlaveDriver::CDF1SerialSlaveDriver(void)
{
	m_wID		= 0x340A;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "DF1 Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "DF1 Slave";	

	AddSpaces();
	}

// Binding Control

UINT CDF1SerialSlaveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CDF1SerialSlaveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CDF1SerialSlaveDriver::GetDeviceConfig(void)
{
	return NULL;
	}


// Implementation     

void CDF1SerialSlaveDriver::AddSpaces(void)
{
//	AddSpace(New CSpaceAB(1, "O", "Outputs",	10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(2, "I", "Inputs",		10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(3, "S", "Status",		10, 0, 9999, addrWordAsWord, AB_FILE_NONE, AB_FILE_MIN, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(4, "B", "Bits",		10, 0, 9999, addrWordAsWord, 3,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(5, "N", "Integers",	10, 0, 9999, addrWordAsWord, 7,		   AB_FILE_MIN, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(6, "T", "Timers",		10, 0, 9999, addrWordAsWord, 4,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));

//	AddSpace(New CSpaceAB(7, "C", "Counters",	10, 0, 9999, addrWordAsWord, 5,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));
	
//	AddSpace(New CSpaceAB(8, "F", "Floating Point", 10, 0, 9999, addrLongAsReal, 8,		   AB_FILE_MIN, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(9, "L", "Long Word",	10, 0, 9999, addrLongAsLong, 9,		   AB_FILE_MIN, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(10,"R", "Strings",	10, 0,  124, addrByteAsByte, 10,	   AB_FILE_MIN, AB_FILE_MAX));
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 over TCP/IP Base Driver
//

// Constructor

CPLC5DF1TCPDriver::CPLC5DF1TCPDriver(void)
{
	}

// Binding Control

UINT CPLC5DF1TCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CPLC5DF1TCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CPLC5DF1TCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CPLC5DF1TCPDriver::GetDeviceConfig(void)
{
	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master via DF1 over TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CPLC5DF1MasterTCPDeviceOptions, CUIItem);

// Constructor

CPLC5DF1MasterTCPDeviceOptions::CPLC5DF1MasterTCPDeviceOptions(void)
{
	m_Device  = 1;
	
	m_Address = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Port	  = 2222;
	
	m_Drop	  = 1;

	m_Keep    = TRUE;

	m_Ping    = TRUE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;

	m_Root    = "";

	m_Name    = 0x02;

	m_Label   = 0x7C;
	}

// UI Managament

void CPLC5DF1MasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support	     

BOOL CPLC5DF1MasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddLong(DWORD(m_Address));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CPLC5DF1MasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Address);
	Meta_AddInteger(Port);
	Meta_AddInteger(Drop);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddString (Root);
	Meta_AddInteger(Name);
	Meta_AddInteger(Label);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Encapsulated DF1 TCP/IP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEncDF1MasterTCPDeviceOptions, CPLC5DF1MasterTCPDeviceOptions);

// Constructor

CEncDF1MasterTCPDeviceOptions::CEncDF1MasterTCPDeviceOptions(void) : CPLC5DF1MasterTCPDeviceOptions()
{
	m_TimeD = 1000;

	m_TimeA = 500;
	}

// Download Support	     

BOOL CEncDF1MasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CPLC5DF1MasterTCPDeviceOptions::MakeInitData(Init);

	Init.AddWord(WORD(m_TimeD));

	Init.AddWord(WORD(m_TimeA));
	
	return TRUE;
	}

// Meta Data Creation

void CEncDF1MasterTCPDeviceOptions::AddMetaData(void)
{
	CPLC5DF1MasterTCPDeviceOptions::AddMetaData();

	Meta_AddInteger(TimeD);

	Meta_AddInteger(TimeA);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master via DF1 over TCP/IP
//

// Instantiator

ICommsDriver * Create_PLC5DF1MasterTCPDriver(void)
{
	return New CPLC5DF1MasterTCPDriver;
	}

// Constructor

CPLC5DF1MasterTCPDriver::CPLC5DF1MasterTCPDriver(void)
{
	m_wID		= 0x3503;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "DF1 Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "DF1 Master";
	}

// Configuration

CLASS CPLC5DF1MasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CPLC5DF1MasterTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Encapsulated DF1 TCP/IP Master
//

// Instantiator

ICommsDriver * Create_EncDF1MasterTCPDriver(void)
{
	return New CEncDF1MasterTCPDriver;
	}

// Constructor

CEncDF1MasterTCPDriver::CEncDF1MasterTCPDriver(void)
{
	m_wID		= 0x4057;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Encapsulated DF1 Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Enc. DF1 Master";
	}

// Configuration

CLASS CEncDF1MasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEncDF1MasterTCPDeviceOptions);
	}

CLASS CEncDF1MasterTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDF1MasterSerialDriverOptions);
	}


//////////////////////////////////////////////////////////////////////////
//
// AllenBradley DF1 Slave TCP/IP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CDf1SlaveTCPDriverOptions, CUIItem);

// Constructor

CDf1SlaveTCPDriverOptions::CDf1SlaveTCPDriverOptions(void)
{
	m_Device   = 1;
	
	m_Port     = 2222;

	m_Count    = 2;

	m_Restrict = 0;

	m_SecAddr  = 0;

	m_SecMask  = 0;
	}

// UI Managament

void CDf1SlaveTCPDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Restrict" ) {

		pWnd->EnableUI("SecMask", m_Restrict>0);

		pWnd->EnableUI("SecAddr", m_Restrict>0);
		}
	}

// Download Support

BOOL CDf1SlaveTCPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddWord(WORD(m_Port));
	Init.AddWord(WORD(m_Count));
	Init.AddByte(BYTE(m_Restrict));
	Init.AddLong(LONG(m_SecAddr));
	Init.AddLong(LONG(m_SecMask));

	return TRUE;
	}

// Meta Data Creation

void CDf1SlaveTCPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Port);
	Meta_AddInteger(Count);
	Meta_AddInteger(Restrict);
	Meta_AddInteger(SecAddr);
	Meta_AddInteger(SecMask);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Slave over TCP/IP
//

// Instantiator

ICommsDriver * Create_PLC5DF1SlaveTCPDriver(void)
{
	return New CPLC5DF1SlaveTCPDriver;
	}

// Constructor

CPLC5DF1SlaveTCPDriver::CPLC5DF1SlaveTCPDriver(void)
{
	m_wID		= 0x3504;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "DF1 Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "DF1 Slave";

	DeleteAllSpaces();

	AddSpaces();
	}

CLASS CPLC5DF1SlaveTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDf1SlaveTCPDriverOptions);
	} 

// Address Management

BOOL CPLC5DF1SlaveTCPDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	if( CABDriver::ParseAddress(Error, Addr, pConfig, Text) ) {

		if( Addr.a.m_Extra == 1 || Addr.a.m_Extra == 2 || Addr.a.m_Extra == 3 ) {

			if( Addr.a.m_Table == 0 ) {

				Addr.a.m_Table = AB_FILE_NONE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation     

void CPLC5DF1SlaveTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpaceAB(1, "O", "Outputs",	10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(2, "I", "Inputs",		10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(3, "S", "Status",		10, 0, 9999, addrWordAsWord, AB_FILE_NONE, AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(4, "B", "Bits",		10, 0, 9999, addrWordAsWord, 3,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(5, "N", "Integers",	10, 0, 9999, addrWordAsWord, 7,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(6, "T", "Timers",		10, 0, 9999, addrWordAsWord, 4,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));

	AddSpace(New CSpaceAB(7, "C", "Counters",	10, 0, 9999, addrWordAsWord, 5,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));
	
	AddSpace(New CSpaceAB(8, "F", "Floating Point", 10, 0, 9999, addrLongAsReal, 8,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(9, "L", "Long Word",	10, 0, 9999, addrLongAsLong, 9,		   AB_FILE_MIN, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(10,"R", "Strings",	10, 0,  124, addrByteAsLong, 10,	   AB_FILE_MIN, AB_FILE_MAX));
	}

// Helpers 

BOOL CPLC5DF1SlaveTCPDriver::IsSlave(void)
{
	return TRUE;
	}

// Driver Data

UINT CPLC5DF1SlaveTCPDriver::GetFlags(void)
{
	#if defined(C3_VERSION)

	return CStdCommsDriver::GetFlags();

	#endif

	return 0;
	}


// End of File
