
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TI500_HPP
	
#define	INCLUDE_TI500_HPP

//////////////////////////////////////////////////////////////////////////
//
// Simatic TI-500 Series
//

class CTI500Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CTI500Driver(void);  

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helper
		BOOL CheckAlignment(CSpace *pSpace);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic TI-500 Series v2
//

class CTI500v2Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CTI500v2Driver(void);  

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helper
		BOOL CheckAlignment(CSpace *pSpace);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

	protected:
		// Implementation
		void AddSpaces(CItem * pConfig);
	};

//////////////////////////////////////////////////////////////////////////
//
// TI 500 Master Device Options
//

class CTI500v2MasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTI500v2MasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		BOOL m_fBlock;
		
	
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TI 500 Master TCP/IP Device Options
//

class CTI500MasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTI500MasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		BOOL m_fBlock;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TI 500 Series Master TCP/IP Driver
//

class CTI500MasterTCPDriver : public CTI500v2Driver
{
	public:
		// Constructor
		CTI500MasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};




// End of File

#endif
