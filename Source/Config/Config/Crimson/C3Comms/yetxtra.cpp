
#include "intern.hpp"

#include "yetxtra.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// YET Xtra Drive Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYETXtraDeviceOptions, CUIItem);

// Constructor

CYETXtraDeviceOptions::CYETXtraDeviceOptions(void)
{
	m_Axis   = 0;
	}

// UI Management

void CYETXtraDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CYETXtraDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Axis));

	return TRUE;
	}

// Meta Data Creation

void CYETXtraDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Axis);
	}

//////////////////////////////////////////////////////////////////////////
//
// YET Xtra Drive Driver
//

// Instantiator

ICommsDriver *	Create_YETXtraDriver(void)
{
	return New CYETXtraDriver;
	}

// Constructor

CYETXtraDriver::CYETXtraDriver(void)
{
	m_wID		= 0x401B;

	m_uType		= driverMaster;

	m_Manufacturer	= "YET";

	m_DriverName	= "Xtra Drive";

	m_Version	= "1.00";

	m_ShortName	= "YET Xtra Drive";

	AddSpaces();
	}

// Binding Control

UINT	CYETXtraDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CYETXtraDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS422;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CYETXtraDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYETXtraDeviceOptions);
	}

// Address Management

BOOL CYETXtraDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CYETXtraAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CYETXtraDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) {

		return FALSE;
		}

	CString sErr;

	if( pSpace->m_uTable < addrNamed ) {

		UINT uOffset = (UINT) tstrtoul(Text, NULL, pSpace->m_uRadix);

		if( uOffset >= pSpace->m_uMinimum && uOffset <= pSpace->m_uMaximum ) {

			Addr.a.m_Extra  = 0;
			Addr.a.m_Offset = uOffset;
			Addr.a.m_Type   = pSpace->m_uType;
			Addr.a.m_Table  = pSpace->m_uTable;

			return TRUE;
			}
		else {
			switch( pSpace->m_uTable ) {

				case 1:
				case 3:
				case 4:
				case 6:
					sErr.Printf( "%s%d - %s%d",
						pSpace->m_Prefix,
						pSpace->m_uMinimum,
						pSpace->m_Prefix,
						pSpace->m_uMaximum
						);
					break;

				case 2:
				case 5:
					sErr.Printf( "%s%X - %s%X",
						pSpace->m_Prefix,
						pSpace->m_uMinimum,
						pSpace->m_Prefix,
						pSpace->m_uMaximum
						);
					break;
				}
			}

		Error.Set( sErr,
			0 );

		return FALSE;
		}

	Addr.a.m_Offset = pSpace->m_uMinimum;
	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Type   = pSpace->m_uType;
	Addr.a.m_Extra  = 0;

	return TRUE;
	}

BOOL CYETXtraDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) {

		return FALSE;
		}

	if( Addr.a.m_Table < addrNamed ) {

		if( pSpace->m_uRadix == 16 ) {

			Text.Printf( "%s%X",
				pSpace->m_Prefix,
				Addr.a.m_Offset
				);
			}

		else {
			Text.Printf( "%s%d",
				pSpace->m_Prefix,
				Addr.a.m_Offset
				);
			}

		return TRUE;
		}

	Text = pSpace->m_Prefix;

	return TRUE;			
	}

// Implementation	

void CYETXtraDriver::AddSpaces(void)
{
/* -1- reinclude if Sequential Commands Added
	AddSpace(New CSpace(AN, " ",	"IMMEDIATE COMMANDS...",			10, SPIMMEDIATE,FZ, LL));
-1- */
	AddSpace(New CSpace( 3, "VARI", "   VARIABLE\t\t(R72/W81)",			10,   1,       100, LL));
	AddSpace(New CSpace( 2, "PARI", "   PARAMETER\t\t(R85/W80)",			16,   0,     0x600, WW));
	AddSpace(New CSpace(AN, "POLI", "   POLLING (Status)\t(0)",			10,   0,	FA, WW));
	AddSpace(New CSpace(AN, "ACCI", "   ACCELERATION\t\t(64)",			10,  64,	FA, LL));
	AddSpace(New CSpace(AN, "CONI", "   CONTROL\t\t(69)",				10,  69,	FA, BB));
	AddSpace(New CSpace(AN, "GAII", "   GAIN\t\t(71)",				10,  71,	FA, WW));
	AddSpace(New CSpace( 1, "GFAI", "   GET FROM ARRAY\t(160)",			10,   1,      1000, LL));
	AddSpace(New CSpace(AN, "GTVI", "   GET VERSION\t\t(63)",			10,  63,	FA, LL));
	AddSpace(New CSpace(AN, "JRKI", "   JERK TIME\t\t(74)",				10,  74,	FA, LL));
	AddSpace(New CSpace(AN, "RFAI", "   Set Variable = ARRAY[Index]\t(159)",	10, 159,	FA, LL));
	AddSpace(New CSpace(AN, "RFAI1","      READ FROM ARRAY - Index\t---",		10, CP1+159,	FA, WW));
	AddSpace(New CSpace(AN, "RFAI2","      READ FROM ARRAY - Variable\t---",	10, CP2+159,	FA, BB));
	AddSpace(New CSpace(AN, "RUNI", "   RUN\t\t(78)",				10,  78,	FA, BB));
	AddSpace(New CSpace(AN, "S1OI", "   SET OUTPUT\t\t(79)",			10,  79,	FA, BB));
	AddSpace(New CSpace(AN, "S1OI1","      SET OUTPUT - Number\t---",		10, CP1+79,	FA, WW));
	AddSpace(New CSpace(AN, "S1OI2","      SET OUTPUT - State\t---",		10, CP2+79,	FA, BB));
	AddSpace(New CSpace(AN, "SNOI", "   SET OUTPUTS\t\t(107)",			10, 107,	FA, BB));
	AddSpace(New CSpace(AN, "SNOI1","      SET OUTPUTS - Mask\t---",		10, CP1+107,	FA, LL));
	AddSpace(New CSpace(AN, "SNOI2","      SET OUTPUTS - State\t---",		10, CP2+107,	FA, LL));
	AddSpace(New CSpace(AN, "SZAI", "   SET ZERO POSITION\t(95)",			10,  95,	FA, BB));
	AddSpace(New CSpace(AN, "SPDI", "   SPEED\t\t(83)",				10,  83,	FA, LL));
	AddSpace(New CSpace(AN, "STAI", "   START\t\t(82)",				10,  82,	FA, BB));
	AddSpace(New CSpace(AN, "STPI", "   STOP (Version 2.91)\t(84)",			10,  84,	FA, BB));
	AddSpace(New CSpace(AN, "STXI", "   STOP EX\t\t(153)",				10, 153,	FA, BB));
	AddSpace(New CSpace(AN, "STXI1","      STOP EX - Type\t---",			10, CP1+153,	FA, BB));
	AddSpace(New CSpace(AN, "STXI2","      STOP EX - Servo State\t---",		10, CP2+153,	FA, BB));
	AddSpace(New CSpace(AN, "STMI", "   STOP MOTION (Version 2.91)\t(99)",		10,  99,	FA, BB));
	AddSpace(New CSpace(AN, "TQLI", "   TORQUE LIMITS\t\t(87)",			10,  87,	FA, BB));
	AddSpace(New CSpace(AN, "TQLI1","      TORQUE LIMIT - Forward\t---",		10, CP1+87,	FA, WW));
	AddSpace(New CSpace(AN, "TQLI2","      TORQUE LIMIT - Reverse\t---",		10, CP2+87,	FA, WW));
	AddSpace(New CSpace(AN, "WRII", "   Set ARRAY[Index] = Value\t(158)",		10, 158,	FA, BB));
	AddSpace(New CSpace(AN, "WRII1","      WRITE TO ARRAY - Index\t---",		10, CP1+158,	FA, WW));
	AddSpace(New CSpace(AN, "WRII2","      WRITE TO ARRAY - Value\t---",		10, CP2+158,	FA, LL));
	AddSpace(New CSpace(AN, "ERROR","Command Fault Response\t---",			10, 199,	FA, LL));

/* -1- reinclude if Sequential commands added
	AddSpace(New CSpace(AN, " ",	"SEQUENTIAL COMMANDS...",			10, SPSEQUENTIAL,  FZ, LL));
	AddSpace(New CSpace( 6, "VARS", "   VARIABLE\t\t(R72/W81)",			10,   1,       100, LL));
	AddSpace(New CSpace( 5, "PARS", "   PARAMETER\t\t(R85/W80)",			16,   0,     0x600, WW));
	AddSpace(New CSpace(AN, "ACCS", "   ACCELERATION\t\t(64)",			10,  64+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "CONS", "   CONTROL\t\t(69)",				10,  69+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "DELS", "   DELAY\t\t(144)",				10, 144+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "ECDS", "   ECAM DISENGAGE\t(122)",			10, 122+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "ECES", "   ECAM ENGAGE\t\t(121)",			10, 121+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "ECES1","      ECAM ENGAGE - ID",			10, CP1S+121,	FB, BB));
	AddSpace(New CSpace(AN, "ECES2","      ECAM ENGAGE - MODE",			10, CP2S+121,	FB, BB));
	AddSpace(New CSpace(AN, "ENGS", "   ENGAGE VIRTUAL AXIS\t(136)",		10, 136+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "ENGS1","      ENGAGE VIRTUAL AXIS - ID",		10, CP1S+136,	FB, BB));
	AddSpace(New CSpace(AN, "ENGS2","      ENGAGE VIRTUAL AXIS - Direction",	10, CP2S+136,	FB, BB));
	AddSpace(New CSpace(AN, "FOSS", "   FAST OUTPUT SETTING\t(154)",		10, 154+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "FOSS1","      FAST OUTPUT SETTING - Variable",		10, CP1S+154,	FB, BB));
	AddSpace(New CSpace(AN, "FOSS2","      FAST OUTPUT SETTING - Condition",	10, CP2S+154,	FB, BB));
	AddSpace(New CSpace(AN, "FOSS3","      FAST OUTPUT SETTING - Value",		10, CP3S+154,	FB, LL));
	AddSpace(New CSpace(AN, "GAIS", "   GAIN\t\t(71)",				10,  71+SQ,	FB, WW));
	AddSpace(New CSpace( 4, "GFAS", "   GET FROM ARRAY\t(160)",			10,   1,      1000, LL));
	AddSpace(New CSpace(AN, "GOAS", "   GO\t\t(112)",				10, 112+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "GOAS1","      GO - Target",				10, CP1S+112,	FB, LL));
	AddSpace(New CSpace(AN, "GOAS2","      GO - Time",				10, CP2S+112,	FB, LL));
	AddSpace(New CSpace(AN, "GODS", "   GO D\t\t(128)",				10, 128+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "GODS1","      GO D - Target",				10, CP1S+128,	FB, LL));
	AddSpace(New CSpace(AN, "GODS2","      GO D - Time",				10, CP2S+128,	FB, LL));
	AddSpace(New CSpace(AN, "GOHS", "   GO H\t\t(117)",				10, 117+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "HARS", "   HARD HOME\t\t(131)",			10, 131+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "HARS1","      HARD HOME - Torque",			10, CP1S+131,	FB, WW));
	AddSpace(New CSpace(AN, "HARS2","      HARD HOME - Speed",			10, CP2S+131,	FB, LL));
	AddSpace(New CSpace(AN, "HMCS", "   HOME C\t\t(133)",				10, 133+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "HMSS", "   HOME SW\t\t(132)",				10, 132+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "HMSS1","      HOME SW - Speed to switch",		10, CP1S+132,	FB, LL));
	AddSpace(New CSpace(AN, "HMSS2","      HOME SW - Return speed",			10, CP2S+132,	FB, LL));
	AddSpace(New CSpace(AN, "HSCS", "   HOME SW C\t\t(130)",			10, 130+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "HSCS1","      HOME SW C - Speed to switch",		10, CP1S+130,	FB, LL));
	AddSpace(New CSpace(AN, "HSCS2","      HOME SW C - Speed to C pulse",		10, CP2S+130,	FB, LL));
	AddSpace(New CSpace(AN, "JRKS", "   JERK TIME\t\t(74)",				10,  74+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "LATS", "   LATCHING TRIGGER\t(152)",			10, 152+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "MVAS", "   MOVE\t\t(113)",				10, 113+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "MVAS1","      MOVE - Distance",			10, CP1S+113,	FB, LL));
	AddSpace(New CSpace(AN, "MVAS2","      MOVE - Time",				10, CP2S+113,	FB, LL));
	AddSpace(New CSpace(AN, "MVDS", "   MOVE D\t\t(129)",				10, 129+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "MVDS1","      MOVE D - Distance",			10, CP1S+129,	FB, LL));
	AddSpace(New CSpace(AN, "MVDS2","      MOVE D - Time",				10, CP2S+129,	FB, LL));
	AddSpace(New CSpace(AN, "MVHS", "   MOVE H\t\t(118)",				10, 118+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "MVRS", "   MOVE R\t\t(119)",				10, 119+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "RFAS", "   Set Variable = ARRAY[Index]\t(159)",	10, 159+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "RFAS1","      READ FROM ARRAY - Index",		10, CP1S+159,	FB, WW));
	AddSpace(New CSpace(AN, "RFAS2","      READ FROM ARRAY - Variable",		10, CP2S+159,	FB, BB));
	AddSpace(New CSpace(AN, "REGS", "   REGISTRATION DISTANCE\t(151)",		10, 151+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "RUNS", "   RUN\t\t(78)",				10,  78+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "S1OS", "   SET OUTPUT\t\t(79)",			10,  79+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "S1OS1","      SET OUTPUT - Number",			10, CP1S+79,	FB, WW));
	AddSpace(New CSpace(AN, "S1OS2","      SET OUTPUT - State",			10, CP2S+79,	FB, BB));
	AddSpace(New CSpace(AN, "SNOS", "   SET OUTPUTS\t\t(107)",			10, 107+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "SNOS1","      SET OUTPUTS - Mask",			10, CP1S+107,	FB, LL));
	AddSpace(New CSpace(AN, "SNOS2","      SET OUTPUTS - State",			10, CP2S+107,	FB, LL));
	AddSpace(New CSpace(AN, "SZAS", "   SET ZERO POSITION\t(95)",			10,  95+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "SLDS", "   SLIDE\t\t(115)",				10, 115+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "SLNS", "   SLIDE ANALOG\t\t(102)",			10, 102+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "SPDS", "   SPEED\t\t(83)",				10,  83+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "SPCS", "   SPEED CONTROL\t(100)",			10, 100+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "STPS", "   STOP\t\t(84)",				10,  84+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "STXS", "   STOP EX\t\t(153)",				10, 153+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "STXS1","      STOP EX - Type",				10, CP1S+153,	FB, BB));
	AddSpace(New CSpace(AN, "STXS2","      STOP EX - Servo State",			10, CP2S+153,	FB, BB));
	AddSpace(New CSpace(AN, "STMS", "   STOP MOTION\t\t(99)",			10,  99+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "TQES", "   TORQUE\t\t(116)",				10, 116+SQ,	FB, WW));
	AddSpace(New CSpace(AN, "TQAS", "   TORQUE ANALOG\t(103)",			10, 103+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "TQLS", "   TORQUE LIMITS\t\t(87)",			10,  87+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "TQLS1","      TORQUE LIMIT - Forward",			10, CP1S+87,	FB, WW));
	AddSpace(New CSpace(AN, "TQLS2","      TORQUE LIMIT - Reverse",			10, CP2S+87,	FB, WW));
	AddSpace(New CSpace(AN, "WEXS", "   WAIT EXACT\t\t(145)",			10, 145+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "WFSS", "   WAIT FOR START\t(146)",			10, 146+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "WAIS", "   WAIT INPUT\t\t(109)",			10, 109+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "WAIS1","      WAIT INPUT - Number",			10, CP1S+109,	FB, BB));
	AddSpace(New CSpace(AN, "WAIS2","      WAIT INPUT - Condition",			10, CP2S+109,	FB, BB));
	AddSpace(New CSpace(AN, "WAIS3","      WAIT INPUT - State",			10, CP3S+109,	FB, BB));
	AddSpace(New CSpace(AN, "WAIS4","      WAIT INPUT - Time",			10, CP4S+109,	FB, LL));
	AddSpace(New CSpace(AN, "WASS", "   WAIT STOP\t\t(148)",			10, 148+SQ,	FB, LL));
	AddSpace(New CSpace(AN, "WAVS", "   WAIT VAR\t\t(110)",				10, 110+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "WAVS1","      WAIT VAR - Variable",			10, CP1S+110,	FB, BB));
	AddSpace(New CSpace(AN, "WAVS2","      WAIT VAR - Condition",			10, CP2S+110,	FB, BB));
	AddSpace(New CSpace(AN, "WAVS3","      WAIT VAR - Value",			10, CP3S+110,	FB, LL));
	AddSpace(New CSpace(AN, "WRIS", "   Set ARRAY[Index] = Value\t(158)",		10, 158+SQ,	FB, BB));
	AddSpace(New CSpace(AN, "WRIS1","      WRITE TO ARRAY - Index",			10, CP1S+158,	FB, WW));
	AddSpace(New CSpace(AN, "WRIS2","      WRITE TO ARRAY - Value",			10, CP2S+158,	FB, LL));
-1- */
	}

// Helpers

CSpace * CYETXtraDriver::GetSpace(CAddress const &Addr)
{
	CSpaceList & List = CStdCommsDriver::GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( MatchSpace(pSpace, Addr) ) {

				return pSpace;
				}

			List.GetNext(n);
			}
		}

	return NULL;
	}

BOOL CYETXtraDriver::MatchSpace(CSpace * pSpace, CAddress const &Addr)
{
	UINT uTable = pSpace->m_uTable;

	if( uTable == Addr.a.m_Table ) {

		if( uTable == addrNamed ) {

			if( Addr.a.m_Offset == pSpace->m_uMinimum ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// YET Xtra Drive Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CYETXtraAddrDialog, CStdAddrDialog);
		
// Constructor

CYETXtraAddrDialog::CYETXtraAddrDialog(CYETXtraDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	if( m_uAllowSpace < SPIMMEDIATE || m_uAllowSpace > SPSEQUENTIAL ) {

		m_uAllowSpace = SPIMMEDIATE;
		}

	m_pYETDriver = &Driver;

	SetName(TEXT("YETXtraAddressDlg"));
	}

// Message Map

AfxMessageMap(CYETXtraAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CYETXtraAddrDialog)
	};

// Message Handlers

BOOL CYETXtraAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	m_pSpace = m_pYETDriver->GetSpace(*m_pAddr);
	
	if( !m_fPart ) {

		SetCaption();

		if( m_pSpace ) {

			SetAllow();
			}

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			return FALSE;
			}

		return TRUE;
		}
	else {
		SetAllow();

		ShowAddress(Addr);

		return FALSE;
		}
	}

// Notification Handlers

void CYETXtraAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CYETXtraAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			SetAllow();

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = m_pSpace->m_uMinimum;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			m_pSpace->m_uMinimum < SPIMMEDIATE ? ShowDetails() : ClearDetails();

			ShowDetails();

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(2001).SetWindowText("None");
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2003).SetWindowText("");
		GetDlgItem(2009).SetWindowText("");
		GetDlgItem(2010).SetWindowText("");
		GetDlgItem(2011).SetWindowText("");
		GetDlgItem(2012).SetWindowText("");
		GetDlgItem(2013).SetWindowText("");

		ClearDetails();

		GetDlgItem(2001).EnableWindow(TRUE);
		GetDlgItem(2002).EnableWindow(FALSE);
		}
	}

BOOL CYETXtraAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		if( IsHeader(m_pSpace->m_uMinimum) ) {

			m_uAllowSpace = m_pSpace->m_uMinimum;

			LoadList();

			GetDlgItem(3002).SetWindowText("");

			UpdateInfo();

			return TRUE;
			}

		if( m_pSpace->m_uTable < addrNamed ) {

			Text += GetDlgItem(2002).GetWindowText();
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pYETDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables
void CYETXtraAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	GetDlgItem(2001).EnableWindow(TRUE);

	if( Addr.a.m_Table < addrNamed ) {

		CString s;

		if( Addr.a.m_Table == 2 || Addr.a.m_Table == 5 ) {

			s.Printf( "%X", Addr.a.m_Offset );
			}

		else {

			s.Printf( "%d", Addr.a.m_Offset );
			}

		GetDlgItem(2002).SetWindowText( s );

		GetDlgItem(2002).EnableWindow(TRUE);

		SetDlgFocus(2002);
		}

	else {
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2002).EnableWindow(FALSE);
		}

	UpdateMode();

	UpdateInfo();

	if( Addr.a.m_Offset >= SPIMMEDIATE ) {

		GetDlgItem(3002).SetWindowText("");
		}

	if( Addr.a.m_Table < addrNamed ) {

		SetDlgFocus(2002);
		}

	else {
		SetDlgFocus(1001);
		}
	}

// Helpers

BOOL CYETXtraAddrDialog::AllowSpace(CSpace *pSpace)
{
	if( pSpace->m_Prefix == "ERROR" ) {

		return TRUE;
		}

	return SelectList( pSpace, pSpace->m_uMinimum, m_uAllowSpace );
	}

BOOL CYETXtraAddrDialog::SelectList(CSpace *pSpace, UINT uCommand, UINT uAllow)
{
	if( IsHeader(uCommand) ) {

		return TRUE;
		}

	UINT uEn = pSpace->m_uMaximum;

	switch( uAllow ) {

		case SPIMMEDIATE:

			if( pSpace->m_uTable <= 3 ) {

				return TRUE;
				}

			return uEn == FA;

		case SPSEQUENTIAL:

			if( pSpace->m_uTable >= 4 && pSpace->m_uTable <= 6 ) {

				return TRUE;
				}

			return uEn == FB;
		}

	return FALSE;
	}

void CYETXtraAddrDialog::SetAllow()
{
	UINT uCommand = m_pSpace->m_uMinimum;

	for( UINT i = SPIMMEDIATE; i <= SPSEQUENTIAL; i++ ) {

		if( SelectList(m_pSpace, uCommand, i) ) {

			m_uAllowSpace = i;

			return;
			}
		}
	}

UINT CYETXtraAddrDialog::IsHeader(UINT uCommand)
{
	return uCommand == SPIMMEDIATE || uCommand == SPSEQUENTIAL ? 1 : 0;
	}

void CYETXtraAddrDialog::UpdateMode(void)
{
/* reinclude if Sequential commands added
	if( IsHeader( m_pSpace->m_uMinimum ) ) {

		if( m_pSpace->m_uMinimum == SPIMMEDIATE ) {

			GetDlgItem(2013).SetWindowText("IMMEDIATE");
			}

		else {
			GetDlgItem(2013).SetWindowText("SEQUENTIAL");
			}
		}

	else {
		switch( m_pSpace->m_Prefix[3] ) {

			case 'I':
				GetDlgItem(2013).SetWindowText("IMMEDIATE");
				break;

			case 'S':
				GetDlgItem(2013).SetWindowText("SEQUENTIAL");
				break;

			case 'O': // Error
				GetDlgItem(2013).SetWindowText("ALL MODES");
				break;
			}
		}
*/
// remove following if Sequential commands added
	GetDlgItem(2013).SetWindowText(" ");
	}

void CYETXtraAddrDialog::UpdateInfo(void)
{
	CString sInfo1 = "";
	CString sInfo2 = "";
	CString sInfo3 = "";
	CString sInfo4 = "";

	UINT uT = m_pSpace->m_uTable;

	if( uT == addrNamed && m_pSpace->m_uMaximum < FZ ) {

		switch( m_pSpace->m_uMinimum ) {

			case  64:
			case  69:
			case  71:
			case  73:
			case  74:
			case  83:
			case  95:
			case 100:
			case 115:
			case 116:
			case 117:
			case 118:
			case 119:
			case 133:
			case 144:
			case 145:
			case 148:
			case 151:
			case 152:
				sInfo2 = SetInfoText(11, 0);
				break;

			case   0:
			case  63:
				sInfo2 = SetInfoText(10, 0);
				break;

			case  70:
			case  82:
			case  99:
			case 102:
			case 103:
			case 122:
			case 146:
				sInfo2 = SetInfoText(0, 0);
				break;

			case  78:
				sInfo2 = "Data = Program Label Number";
				break;

			case  79:
				sInfo1 = "Data = 1 -> State = Integer";
				sInfo2 = "Data = 2 -> State = Variable Number";
				break;

			case  80:
			case  81:
			case  87:
			case 112:
			case 113:
			case 121:
			case 128:
			case 129:
			case 130:
			case 131:
			case 132:
			case 136:
			case 153:
				sInfo2 = SetInfoText(2, 0);
				break;

			case CP1 + 153:
				sInfo1 = "Data selects Rate of Deceleration:";
				sInfo2 = "0 = Profile Acceleration";
				sInfo3 = "1 = Emergency (Quick)";
				sInfo4 = "2 = Emergency (Quick) + Program Stop";
				break;

			case CP2 + 153:
				sInfo1 = "Data selects Motor State after Stop:";
				sInfo2 = "0 = Motor Enabled";
				sInfo3 = "1 = Motor Disabled";
				break;

			case  84:
				sInfo2 = "Data = 0 -> Stop and Disable Motor";
				sInfo3 = "Data = 1 -> Stop, Motor Enabled";
				break;

			case 107:
				sInfo1 = "Data = 1 -> Mask  + State = Integers";
				sInfo2 = "Data = 2 -> Mask  = Variable Number";
				sInfo3 = "Data = 3 -> State = Variable Number";
				sInfo4 = "Data = 4 -> Mask  + State = Variables";
				break;

			case 109:
				sInfo2 = SetInfoText(3, 4);
				break;

			case 110:
				sInfo2 = SetInfoText(3, 3);
				break;

			case 158:
				sInfo1 = "Data = 1 -> Index + Value = Integers";
				sInfo2 = "Data = 2 -> Index = Variable Number";
				sInfo3 = "Data = 3 -> Value = Variable Number";
				sInfo4 = "Data = 4 -> Index + Value = Variable Numbers";
				break;

			case 159:
				sInfo1 = "Data = 1, Index = Integer";
				sInfo2 = "Data = 2, Index = Variable Number";
				break;

			case 199:
				sInfo1 = "Command not executed (32-bits):";
				sInfo2 = "High Word = Opcode Number";
				sInfo3 = "Low Word  = Fault Number";
				sInfo4 = SetInfoText(12, 0);
				break;

			default:
				sInfo2 = SetInfoText(11, 0);
				break;
			}
		}

	else {
		switch( uT ) {

			case 1:
			case 4:
				sInfo2 = SetInfoText(10, 0);
				break;

			case 2:
			case 5:
				sInfo2 = SetInfoText( 11, 0 );
				break;
			case 3:
			case 6:
				sInfo2 = SetInfoText( 11, 0 );
				sInfo4 = "(User Var_01 = 67...Var_10 = 76)";
				break;

			default:
				sInfo2 = "";
			}
		}

	GetDlgItem(2009).SetWindowText( sInfo1 );
	GetDlgItem(2009).EnableWindow(  sInfo1 != "" );

	GetDlgItem(2010).SetWindowText( sInfo2 );
	GetDlgItem(2010).EnableWindow(  sInfo2 != "" );

	GetDlgItem(2011).SetWindowText( sInfo3 );
	GetDlgItem(2011).EnableWindow(  sInfo3 != "" );

	GetDlgItem(2012).SetWindowText( sInfo4 );
	GetDlgItem(2012).EnableWindow(  sInfo4 != "" );
	}

CString CYETXtraAddrDialog::SetInfoText(UINT uSel, UINT uPar)
{
	CString s = "";

	switch( uSel ){

		case 0:
			return "Data != 0 sends Command";

		case 2:
			s.Printf( "Data != 0 sends %s1 + %s2",
				m_pSpace->m_Prefix,
				m_pSpace->m_Prefix
				);

			break;

		case 3:
			s.Printf( "Data != 0 sends %s1 -> %s%1.1d",
				m_pSpace->m_Prefix,
				m_pSpace->m_Prefix,
				uPar
				);

			break;

		case 10:
			return "Read Only";

		case 11:
			return "(Write) Data = Value";

		case 12:
			return "(Write) ERROR = 0";
		}

	return s;
	}

// End of File
