#include "ciasdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CIAPDO_HPP
	
#define	INCLUDE_CIAPDO_HPP

//////////////////////////////////////////////////////////////////////////
//
// Constants
//
//

#define PDO_MIN	1
#define PDO_MAX	4
#define EVT_MIN 0
#define EVT_MAX 0xFFFF
#define EVT_DEF 1000
#define ERR_PDO	1
#define ERR_EVT	2


//////////////////////////////////////////////////////////////////////////
//
// Element Sizes
//
//
enum ElementSizes {

	sizeByte	= 0,
	sizeWord	= 1,
	sizeLong	= 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Element Storage Class
//
//

class CElement : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CElement(void);

		// Data Access
		UINT GetElementIndex(void);
		UINT GetElementSub(void);
		UINT GetElementSize(void);
		UINT GetElementSizeSel(void);
		BOOL SetElementIndex(UINT uIndex);
		BOOL SetElementSub(UINT uSub);
		BOOL SetSize(UINT uSize);
		BOOL SetElementSize(UINT uSize);
		BOOL SetElementSize(CString Size);
		BOOL SetElement(CString Text);
		
		CString GetElementString(void);
		CString GetPrefix(void);
		UINT    GetType(void);

		 		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);

	protected:

		UINT m_Index;
		UINT m_Sub;
		UINT m_Size;
	};

/////////////////////////////////////////////////////////////////////////
//
//  Element List Class
//
//

class CElementList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CElementList(void);

		// Destructor
		~CElementList(void);
		 
		// Item Access
		CElement * GetItem(INDEX Index) const;
		CElement * GetItem(UINT  uPos ) const;

		// Operations
		CElement * AppendItem(CElement * pEl);
	};




//////////////////////////////////////////////////////////////////////////
//
//  PDO Storage Class
//
//

class CPDO : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPDO(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);

		// Data Members
		UINT		m_Type;
		UINT		m_Number;
		UINT		m_Manual;
		UINT		m_Event;
		CElementList *	m_pElements;

		CString GetPDOString(void);
		BOOL	AddElement(CString Text);

	protected:

		BOOL CanAddElement(UINT uSize);
		

	};


//////////////////////////////////////////////////////////////////////////
//
//  PDO List Class
//
//

class CPDOList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPDOList(void);

		// Destructor
		~CPDOList(void);

		// Item Access
		CPDO * GetItem(INDEX Index) const;
		CPDO * GetItem(UINT  uPos ) const;

		// Operations
		CPDO * AppendItem(CPDO * pPDO);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave
//

class CCANOpenPDOSlave : public CCANOpenSlave
{
	public:
		// Constructor
		CCANOpenPDOSlave(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	
		// PDO Access
		CPDO* FindPDO(CAddress Addr, CItem *pConfig);
		CPDO* LookupPDO(CAddress &Addr, CItem *pConfig, CString Text, UINT uNum, UINT uManual, UINT uEvent);
		CPDO* LookupPDO(CAddress Addr, CItem *pConfig);
		CPDO* CreatePDO(CAddress &Addr, CItem *pConfig, CString Text, UINT uNum, UINT uManual, UINT uEvent);
		CPDO* CreatePDO(CAddress Addr, CItem *pConfig);
		BOOL  DeletePDO(CAddress Addr, CItem *pConfig);
		BOOL  DeletePDO(CPDO * pPDO, CItem * pConfig);
		void  SetDirty(CAddress &Addr);
		UINT  GetPDONumber(CAddress &Addr);
				
		// Errors
		CString  GetError(UINT uError);
		
	protected:

		BOOL m_fDirty;
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave DriverOptions
//

class CCANOpenPDOSlaveDriverOptions : public CCANOpenSlaveDriverOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANOpenPDOSlaveDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT		m_Profile;
		CPDOList *	m_pPDOs;
		UINT		m_Op;
			
	protected:

		UINT		m_uCompact;
			
		// Meta Data Creation
		void AddMetaData(void);

		// EDS File Support
		void WriteDeviceInfo(ITextStream &Stream);
		void WriteOptionalObjects(ITextStream &Stream);
		BOOL IsObjectMandatory(UINT uIndex, UINT uType);
		BOOL IsPDO(void);
		UINT GetPDOCount(BOOL fTPDO);
		UINT GetCompactPDO(void);
		BOOL GetObjects(void);
		void ParseObjects(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PDO Selection
//

class CCANOpenPDODialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CCANOpenPDODialog(CCANOpenPDOSlave &Driver, CAddress &Addr, CItem *pConfig);
		                
	protected:

		// Data
		CCANOpenPDOSlave *		m_pDriver;
		CAddress	 *		m_pAddr;
		CCANOpenPDOSlaveDriverOptions *	m_pConfig;
		HTREEITEM			m_hRoot;
		HTREEITEM			m_hSelect;
		CPDO *				m_pPDO;
		CImageList			m_Images;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnEditChange(UINT uID, CWnd &Wnd);
		void OnSelChange(UINT uID, CWnd &Wnd);
		
		// Command Handlers
		BOOL OnAddElement(UINT uID);
		BOOL OnOkay(UINT uID);
				
		// PDO Loading
		void LoadPDO(CPDO * pPDO);
		void LoadRoot(CTreeView &Tree, CPDO * pPDO);
		void LoadElements(CTreeView &Tree, CPDO * pPDO);
		void LoadElement(CTreeView &Tree, PCTXT Text);
		void LoadObjectInfo(CPDO * pPDO);
		void LoadManualEvent(CPDO * pPDO);
		void LoadElementInfo(void);
		
		// Helpers
		BOOL DeleteElement(void);
		void DoEnables(void);
		void UpdateItem(CTreeViewItem Item, CString Text);
		UINT GetEditValue(UINT uID);
		void ResetManual(void);
		
		CTreeViewItem	FindItem(void);

	};




// End of File

#endif
