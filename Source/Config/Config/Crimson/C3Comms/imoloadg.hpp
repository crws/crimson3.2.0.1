
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMOGLOAD_HPP

#define	INCLUDE_IMOGLOAD_HPP

// Forward Declarations

class CIMOGLoaderPortDriver;
class CIMOGAddrDialog;

//////////////////////////////////////////////////////////////////////////
//
// IMO G Series Loader Port Driver
//

class CIMOGLoaderPortDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIMOGLoaderPortDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);

		// Address Checking
		UINT	ParseAddress( CString Text, UINT * pOffset, UINT uType );
		BOOL	IsIO(UINT uTable);
		UINT	GetValue( CString t, UINT * p, UINT uMax );
	};

//////////////////////////////////////////////////////////////////////////
//
// IMO G Series Address Selection
//

class CIMOGAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CIMOGAddrDialog(CIMOGLoaderPortDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Implementation
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Overridables
		void	ShowAddress(CAddress Addr);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
	};

// End of File

#endif
