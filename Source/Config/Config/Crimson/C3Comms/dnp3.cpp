#include "intern.hpp"

#include "dnp3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// DNP3 Event Configuration Item
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3EventConfig, CUIItem);

// Constructor

CDnp3EventConfig::CDnp3EventConfig(void)
{
	m_Mode   = 0;

	m_Limit  = 0;
   	}

CDnp3EventConfig::CDnp3EventConfig(UINT uMode, UINT uLimit)
{
	m_Mode  = uMode;

	m_Limit = uLimit;
	}

// Download Support

BOOL CDnp3EventConfig::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));
	
	Init.AddWord(WORD(m_Limit));
	
	return TRUE;
	}

// Data Access

void CDnp3EventConfig::GetModeData(UINT uItem, CUIData &Data)
{
	Data.m_Tag	  = L"Mode";

	Data.m_Label      = m_Label[uItem];

	Data.m_ClassText  = AfxNamedClass(L"CUITextEnum");
	
	Data.m_ClassUI    = AfxNamedClass(L"CUIDropDown");
	
	Data.m_Format     = CString(IDS_ALL_EVENTSMOST);
	
	Data.m_Tip        = CString(IDS_SPECIFY_WHICH);

	if( uItem == eventAI ) {

		Data.m_Format += CString(IDS_CURRENT_VALUE);
		}
	}

void CDnp3EventConfig::GetLimitData(UINT uItem, CUIData &Data)
{
	Data.m_Tag	 = L"Limit";

	Data.m_Label     = m_Label[uItem];
	
	Data.m_ClassText = AfxNamedClass(L"CUITextInteger");
	
	Data.m_ClassUI   = AfxNamedClass(L"CUIEditBox");
	
	Data.m_Format    = L"0|0||0|32000";
	
	Data.m_Tip       = CString(IDS_SPECIFY_MAXIMUM);
	}

// Meta Data Creation

void CDnp3EventConfig::AddMetaData(void)	
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Mode);

	Meta_AddInteger(Limit);
	}

///////////////////////////////////////////////////////////////////////////
//
// DNP3 Event Config List
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3EventConfigList, CItemList);

// Constructor

CDnp3EventConfigList::CDnp3EventConfigList(void)
{	
	}

// Item Access

CDnp3EventConfig * CDnp3EventConfigList::GetItem(UINT uPos) const
{
	return (CDnp3EventConfig *) CItemList::GetItem(uPos);
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3DriverOptions, CUIItem);

// Constructor

CDnp3DriverOptions::CDnp3DriverOptions(void)
{
	m_Source = 2;	
	}

// Download Support

BOOL CDnp3DriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Source));

	return TRUE;
	}

// Meta Data Creation

void CDnp3DriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Source);	
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3DeviceOptions, CUIItem);

// Constructor

CDnp3DeviceOptions::CDnp3DeviceOptions(void)
{
	m_Dest	= 1;

	m_TO	= 2000;

	m_Link  = 60000;
	}

// Download Support

BOOL CDnp3DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Dest));

	Init.AddWord(WORD(m_TO));

	Init.AddLong(LONG(m_Link));

	return TRUE;
	}

// Meta Data Creation

void CDnp3DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Dest);

	Meta_AddInteger(TO);

	Meta_AddInteger(Link);
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3MasterDeviceOptions, CDnp3DeviceOptions);

// Constructor

CDnp3MasterDeviceOptions::CDnp3MasterDeviceOptions(void)
{
	m_TO	 = 1000;

	m_Poll   = 0;

	m_Class0 = 5000;

	m_Class1 = 1000;

	m_Class2 = 1000;

	m_Class3 = 1000;
	}

// UI Managament

void CDnp3MasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Poll" ) {

			BOOL fEnable = pItem->GetDataAccess("Poll")->ReadInteger(pItem) ? TRUE : FALSE;

			pWnd->EnableUI("Class0", fEnable);

			pWnd->EnableUI("Class1", fEnable);

			pWnd->EnableUI("Class2", fEnable);

			pWnd->EnableUI("Class3", fEnable);
			}
		}			
	}

// Download Support

BOOL CDnp3MasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CDnp3DeviceOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_Poll));

	Init.AddLong(LONG(m_Class0));

	Init.AddLong(LONG(m_Class1));

	Init.AddLong(LONG(m_Class2));

	Init.AddLong(LONG(m_Class3));
	
	return TRUE;
	}

// Meta Data Creation

void CDnp3MasterDeviceOptions::AddMetaData(void)
{
	CDnp3DeviceOptions::AddMetaData();

	Meta_AddInteger(Poll);	

	Meta_AddInteger(Class0);

	Meta_AddInteger(Class1);

	Meta_AddInteger(Class2);

	Meta_AddInteger(Class3);
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3SlaveDeviceOptions, CDnp3DeviceOptions);

// Constructor

CDnp3SlaveDeviceOptions::CDnp3SlaveDeviceOptions(void)
{
	m_Unsol    = 0;

	m_Class1   = 1;

	m_Class2   = 1;

	m_Class3   = 1;

	m_Retain   = 0;

	m_AiCalc   = 0;

	m_pEvents  = New CDnp3EventConfigList;
	}

// Persistance

void CDnp3SlaveDeviceOptions::Init(void)
{
	CDnp3DeviceOptions::Init();

	InitEvents();
	}

void CDnp3SlaveDeviceOptions::PostLoad(void)
{
	CDnp3DeviceOptions::PostLoad();

	if( !m_pEvents->GetHead() ) {

		InitEvents();
		}
	}

// UI Loading

BOOL CDnp3SlaveDeviceOptions::OnLoadPages(CUIPageList * pList)
{
	CDnp3SlaveDeviceOptionsUIPage * pPage = New CDnp3SlaveDeviceOptionsUIPage(this);

	pList->Append(pPage);
		
	return TRUE;			   
	}

// UI Managament

void CDnp3SlaveDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Unsol" ) {

			BOOL fEnable = pItem->GetDataAccess("Unsol")->ReadInteger(pItem) ? TRUE : FALSE;

			pWnd->EnableUI("Class1", fEnable);

			pWnd->EnableUI("Class2", fEnable);

			pWnd->EnableUI("Class3", fEnable);
			}
		}			
	}

// Download Support

BOOL CDnp3SlaveDeviceOptions::MakeInitData(CInitData &Init)
{
	CDnp3DeviceOptions::MakeInitData(Init);

	BYTE bMask = m_Unsol ? BYTE(m_Class1) << 0 | 
			       BYTE(m_Class2) << 1 | 
			       BYTE(m_Class3) << 2 : 0;

	Init.AddByte(BYTE(bMask));

	m_pEvents->MakeInitData(Init);
	
	Init.AddByte(BYTE(m_Retain));

	Init.AddByte(BYTE(m_AiCalc));

	return TRUE;
	}

// Implementation

void CDnp3SlaveDeviceOptions::InitEvents(void)
{
	UINT Mode []  = { 0,
			  0,
			  0,
			  1,
			  0,
			  1,
			  0};

	UINT Limit [] = { 100,
			  100,
			  100,
			  30,
			  30,
			  30,
			  100};
	
	for( UINT e = eventBI; e < eventTotal; e++ ) {

		AddEvent(Mode[e], Limit[e]);
		}
	}

void CDnp3SlaveDeviceOptions::AddEvent(UINT uMode, UINT uLimit)
{
	m_pEvents->AppendItem(New CDnp3EventConfig(uMode, uLimit));
	}

// Meta Data Creation

void CDnp3SlaveDeviceOptions::AddMetaData(void)
{
	CDnp3DeviceOptions::AddMetaData();

	Meta_AddInteger(Unsol);	

	Meta_AddInteger(Class1);

	Meta_AddInteger(Class2);

	Meta_AddInteger(Class3);

	Meta_AddCollect(Events);
	
	Meta_AddInteger(Retain);

	Meta_AddInteger(AiCalc);
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Slave Device Options UI Page
//


// Runtime Class

AfxImplementRuntimeClass(CDnp3SlaveDeviceOptionsUIPage, CUIPage);

// Constructor

CDnp3SlaveDeviceOptionsUIPage::CDnp3SlaveDeviceOptionsUIPage(CDnp3SlaveDeviceOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CDnp3SlaveDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_DEVICE_SETTINGS), 1);

	pView->AddUI(pItem, L"root", L"Dest");

	if( pItem->IsKindOf(AfxRuntimeClass(CDnp3TcpSlaveDeviceOptions)) ) {

		pView->AddUI(pItem, L"root", L"Addr");

		pView->AddUI(pItem, L"root", L"Addr2");

		pView->AddUI(pItem, L"root", L"Prot");

		pView->AddUI(pItem, L"root", L"Socket");

		pView->AddUI(pItem, L"root", L"UdpPort");

		pView->AddUI(pItem, L"root", L"Time1");
		}

	pView->AddUI(pItem, L"root", L"TO");

	pView->AddUI(pItem, L"root", L"Link");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_EVENT_MODE), 1);

	CDnp3EventConfigList * pEvents = m_pOption->m_pEvents;

	UINT uEvents = pEvents->GetItemCount();

	for( UINT m = 0; m < uEvents; m++ ) {

		CDnp3EventConfig * pCfg = pEvents->GetItem(m);

		if( pCfg ) {

			CUIData Mode;
			
			pCfg->GetModeData(m, Mode);

			pView->AddUI(pCfg, L"root", &Mode);
			}
		}

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_EVENT_BUFFER), 1);

	for( UINT l = 0; l < uEvents; l++ ) {

		CDnp3EventConfig * pCfg = pEvents->GetItem(l);

		if( pCfg ) {

			CUIData Limit;
			
			pCfg->GetLimitData(l, Limit);

			pView->AddUI(pCfg, L"root", &Limit);
			}
		}
	
	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_EVENT_BUFFER_FULL), 1);

	pView->AddUI(pItem, L"root", L"Retain");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_ANALOG_INPUT), 1);

	pView->AddUI(pItem, L"root", L"AiCalc");

	pView->EndGroup(TRUE);

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// DNP3 TCP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3TcpMasterDeviceOptions, CDnp3MasterDeviceOptions);

// Constructor

CDnp3TcpMasterDeviceOptions::CDnp3TcpMasterDeviceOptions(void) 
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD(101, 1), MAKEWORD(168, 192)));

	m_Socket  = 20000;

	m_UdpPort = 20000;

	m_Time1	  = 5000;

	m_Link    = 30000;

	m_Keep    = 1;

	m_Time3	  = 200;

	m_Prot    = 0;
	}

// UI Managament

void CDnp3TcpMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "Prot" ) {

			pWnd->EnableUI("Socket",  m_Prot != 2);

			pWnd->EnableUI("UdpPort", m_Prot != 1);

			pWnd->EnableUI("Link",    m_Prot != 2);
			}
		}

	CDnp3MasterDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CDnp3TcpMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CDnp3MasterDeviceOptions::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));

	Init.AddWord(m_Prot != 2 ? WORD(m_Socket)  : WORD(0));

	Init.AddWord(m_Prot != 1 ? WORD(m_UdpPort) : WORD(0));

	Init.AddWord(WORD(m_Time1));

	Init.AddByte(BYTE(m_Keep));

	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CDnp3TcpMasterDeviceOptions::AddMetaData(void)
{
	CDnp3MasterDeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);

	Meta_AddInteger(Socket);

	Meta_AddInteger(UdpPort);

	Meta_AddInteger(Time1);

	Meta_AddInteger(Keep);

	Meta_AddInteger(Time3);

	Meta_AddInteger(Prot);
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 TCP Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDnp3TcpSlaveDeviceOptions, CDnp3SlaveDeviceOptions);

// Constructor

CDnp3TcpSlaveDeviceOptions::CDnp3TcpSlaveDeviceOptions(void) 
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD(101, 1), MAKEWORD(168, 192)));

	m_Addr2   = 0;

	m_Socket  = 20000;

	m_UdpPort = 20000;

	m_Time1	  = 5000;

	m_Link    = 30000;

	m_Prot    = 0;
	}

// UI Managament

void CDnp3TcpSlaveDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Prot" ) {

			pWnd->EnableUI("Socket",  m_Prot != 2);

			pWnd->EnableUI("UdpPort", m_Prot != 1);

			pWnd->EnableUI("Link",    m_Prot != 2);
			}
		}

	CDnp3SlaveDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CDnp3TcpSlaveDeviceOptions::MakeInitData(CInitData &Init)
{
	CDnp3SlaveDeviceOptions::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));

	Init.AddWord(m_Prot != 2 ? WORD(m_Socket)  : WORD(0));

	Init.AddWord(m_Prot != 1 ? WORD(m_UdpPort) : WORD(0));

	Init.AddWord(WORD(m_Time1));

	Init.AddLong(LONG(m_Addr2));

	return TRUE;
	}

// Meta Data Creation

void CDnp3TcpSlaveDeviceOptions::AddMetaData(void)
{
	CDnp3SlaveDeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);

	Meta_AddInteger(Addr2);

	Meta_AddInteger(Socket);

	Meta_AddInteger(UdpPort);

	Meta_AddInteger(Time1);

	Meta_AddInteger(Prot);
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Base Driver
//

// Constructor

CDnp3BaseDriver::CDnp3BaseDriver(void)
{
	m_Manufacturer	= "DNP3";

	C3_PASSED();
	}

// Address Management

BOOL CDnp3BaseDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CDnp3AddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CDnp3BaseDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type = StripType(pSpace, Text);

	UINT uFind = Text.Find(':');

	UINT uExtra = 0;

	if( uFind < NOTHING ) {

		CString Sel = Text.Mid(uFind + 1);

		if( isdigit(Sel.GetAt(0)) ) {

			uExtra = watoi(Text.Mid(uFind + 1));
			}
		else {
			UINT uProp = GetPropAsUint(Sel, pSpace);

			if( uProp < NOTHING ) {

				uExtra = uProp;
				}
			}

		Text = Text.Mid(0, uFind);
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		CDnpSpace * pDnp = (CDnpSpace *)pSpace;

		if( pDnp ) {

			Addr.a.m_Type  = pDnp->GetWholeType(Type);

			Addr.a.m_Extra = uExtra & 0xF;

			if( Addr.a.m_Table < addrNamed ) {

				if( Addr.a.m_Extra > 0 ) {

					Addr.a.m_Type = addrLongAsLong;
					}
				}

			if( IsDouble(Addr) ) {

				Addr.a.m_Offset *= 2;
				}
			}

		return TRUE;
		}	
	
	return FALSE;
	}

BOOL CDnp3BaseDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		if( pSpace->IsNamed() ) {

			Text = pSpace->m_Prefix;
			}
		else {
			UINT uOffset = Addr.a.m_Offset;

			UINT uType   = Addr.a.m_Type;

			UINT uProp   = Addr.a.m_Extra;

			if( IsDouble(Addr) ) {

				uOffset /= 2;
				}

			if( uType == pSpace->m_uType || uProp ) {

				Text.Printf( L"%s%s:%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset),
					     GetPropAsText(uProp, pSpace)
					     );
				}
			else {	
				Text.Printf( L"%s%s:%s.%s",
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText (uOffset),
					     GetPropAsText(uProp, pSpace),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CDnp3BaseDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	if( m_uType == driverMaster ) {

		BYTE e = Addr.a.m_Extra;

		switch(Addr.a.m_Table) {

			case 1:
			case 3:
			case 20:
			case 21:
			case 30:
			case 234:
				return e < 3;

			case 2:
			case 4:
			case 11:
			case 13:
			case 22:
			case 23:
			case 32:
			case 42:
			case 43:
			case 239:
			case 235:
			case 237:
			case 238:
				return TRUE;

			case 10:
			case 34:
			case 40:
			case 236:
				return e && e < 3;
			}
		}

	return FALSE;
	}

// Data Access

BOOL CDnp3BaseDriver::IsBinaryCommand(UINT uTable)
{
	return ( uTable == 10 || uTable == 11 || uTable == 13 );
	}


BOOL CDnp3BaseDriver::IsFeedback(UINT uTable)
{
	return uTable == 239;
	}

// Text Lookup
		
CString CDnp3BaseDriver::GetPropAsText(UINT uProp, CSpace * pSpace)
{
	if( pSpace ) {
	
		if( IsBinaryCommand(pSpace->m_uTable) ) {

			switch( uProp ) {

				case 0:		return GetPropText(textControl);
				case 1:		return GetPropText(textFlags);
				case 2:		return GetPropText(textTimeStamp);
				case 3:		return GetPropText(textClass);
				case 4:		return GetPropText(textOnTime);
				case 5:		return GetPropText(textOffTime);
				}
			}
			
		else if ( IsFeedback(pSpace->m_uTable) ) {
						
			switch( uProp ) {

				case 0:		return GetPropText(textObject);
				case 1:		return GetPropText(textPoint);
				case 2:		return GetPropText(textFlags);
				case 3:		return GetPropText(textTimeStamp);
				}
			}
				
		else {
			switch( uProp ) {

				case 0:		return GetPropText(textCurrent);
				case 1:		return GetPropText(textFlags);
				case 2:		return GetPropText(textTimeStamp);
				case 3:		return GetPropText(textClass);
				case 4:		return GetPropText(textFreeze);
				}
			}
		}

	return "";
	}

UINT CDnp3BaseDriver::GetPropAsUint(CString Prop, CSpace * pSpace)
{
	if( pSpace ) {

		Prop.ToLower();
	
		if( IsBinaryCommand(pSpace->m_uTable) ) {

			if( Prop == GetPropText(textControl).ToLower())		return 0;
			if( Prop == GetPropText(textFlags).ToLower())		return 1;
			if( Prop == GetPropText(textTimeStamp).ToLower())	return 2;
			if( Prop == GetPropText(textClass).ToLower())		return 3;
			if( Prop == GetPropText(textOnTime).ToLower())		return 4;
			if( Prop == GetPropText(textOffTime).ToLower())		return 5;
			}

		else if ( IsFeedback(pSpace->m_uTable) ) {
						
			if( Prop == GetPropText(textObject).ToLower())		return 0;
			if( Prop == GetPropText(textPoint).ToLower())		return 1;
			if( Prop == GetPropText(textFlags).ToLower())		return 2;
			if( Prop == GetPropText(textTimeStamp).ToLower())	return 3; 
			}
		else {
			if( Prop == GetPropText(textCurrent).ToLower())		return 0;
			if( Prop == GetPropText(textFlags).ToLower())		return 1;
			if( Prop == GetPropText(textTimeStamp).ToLower())	return 2;
			if( Prop == GetPropText(textClass).ToLower())		return 3;
			if( Prop == GetPropText(textFreeze).ToLower())		return 4;
			}
		}

	return NOTHING;
	}

// Implementation

void CDnp3BaseDriver::AddSpaces(void)
{
	BOOL fMaster = m_uType == driverMaster;

	AddSpace(New CDnpSpace(1,	"BI",	"Binary Inputs",			10, 0, 65535, addrBitAsBit,   addrLongAsLong, 3));
	AddSpace(New CDnpSpace(3,	"DBI",	"Double-bit Binary Inputs",		10, 0, 65535, addrByteAsByte, addrLongAsLong, 3));
	AddSpace(New CDnpSpace(10,	"BO",	"Binary Outputs",			10, 0, 65535, addrByteAsByte, addrLongAsLong, fMaster ? 5 : 3));
	AddSpace(New CDnpSpace(20,	"C",	"Counters",				10, 0, 65535, addrWordAsWord, addrLongAsLong, fMaster ? 4 : 3));
	AddSpace(New CDnpSpace(21,	"FC",	"Frozen Counters",			10, 0, 65535, addrWordAsWord, addrLongAsLong, 3));
	AddSpace(New CDnpSpace(30,	"AI",	"Analog Inputs",			10, 0, 65535, addrWordAsWord, addrRealAsReal, 3));
	AddSpace(New CDnpSpace(234,	"AIL",	"Analog Input 64-bit Value",		10, 0, 32767, addrLongAsLong, addrLongAsLong, 0));

	if( !fMaster ) {

		AddSpace(New CDnpSpace(34,	"AID",	"Analog Input Deadband",	10, 0, 65535, addrWordAsWord, addrRealAsReal, 2));
		AddSpace(New CDnpSpace(233,	"AIDL",	"Analog Input 64-bit Deadband",	10, 0, 32767, addrLongAsLong, addrLongAsLong, 0));
		}

	AddSpace(New CDnpSpace(40,	"AO",	"Analog Outputs",			10, 0, 65535, addrWordAsWord, addrRealAsReal, 3));
	AddSpace(New CDnpSpace(236,	"AOL",	"Analog Output 64-bit Value",		10, 0, 32767, addrLongAsLong, addrLongAsLong, 0));

	if( !fMaster ) {

		return;
		}

	// Event Access
	AddSpace(New CDnpSpace(2,	"BIE",	"Binary Input Events",			10, 0, 65535, addrBitAsBit,   addrLongAsLong, 2));
	AddSpace(New CDnpSpace(4,	"DBIE",	"Double-bit Binary Input Events",	10, 0, 65535, addrByteAsByte, addrLongAsLong, 2));
	AddSpace(New CDnpSpace(11,	"BOE",	"Binary Output Events",			10, 0, 65535, addrBitAsBit,   addrLongAsLong, 2));
	//AddSpace(New CDnpSpace(12,	"BOC",	"Binary Output Commands",		10, 0, 65535, addrBitAsBit,   addrLongAsLong, 5));
	//AddSpace(New CDnpSpace(13,	"BOCE",	"Binary Output Command Events",		10, 0, 65535, addrBitAsBit,   addrLongAsLong, 2));
	AddSpace(New CDnpSpace(22,	"CE",	"Counter Events",			10, 0, 65535, addrWordAsWord, addrLongAsLong, 2));
	AddSpace(New CDnpSpace(23,	"FCE",	"Frozen Counter Events",		10, 0, 65535, addrWordAsWord, addrLongAsLong, 2));
	AddSpace(New CDnpSpace(32,	"AIE",	"Analog Input Events",			10, 0, 65535, addrWordAsWord, addrRealAsReal, 2));
	AddSpace(New CDnpSpace(235,	"AIEL",	"Analog Input Event 64-bit Value",	10, 0, 32767, addrLongAsLong, addrLongAsLong, 0));
	//AddSpace(New CDnpSpace(41,	"AOC",	"Analog Output Commands",		10, 0, 65535, addrWordAsWord, addrRealAsReal, 3)); 
	AddSpace(New CDnpSpace(42,	"AOE",	"Analog Output Events",			10, 0, 65535, addrWordAsWord, addrRealAsReal, 2)); 
	AddSpace(New CDnpSpace(237,	"AOEL",	"Analog Output Event 64-bit Value",	10, 0, 32767, addrLongAsLong, addrLongAsLong, 0)); 
	//AddSpace(New CDnpSpace(43,	"AOCE",	"Analog Output Command Events",		10, 0, 65535, addrWordAsWord, addrRealAsReal, 2));
	//AddSpace(New CDnpSpace(238,	"AOCEL","Analog Output Cmd Event 64-bit Value",	10, 0, 32767, addrLongAsLong, addrLongAsLong, 0));
		
	// Latest error support	
	AddSpace(New CDnpSpace(239,	"FB",	"Failed Cmd/Write Feedback",		10, 0,   100, addrLongAsLong, addrLongAsLong, 3)); 

	// Commands
	AddSpace(New CDnpSpace(addrNamed, "ST", "Sync Time",				10, 5,	   0, addrBitAsBit,   addrBitAsBit,   0));			
	AddSpace(New CDnpSpace(addrNamed, "CR", "Cold Restart",				10, 1,     0, addrBitAsBit,   addrBitAsBit,   0));
	AddSpace(New CDnpSpace(addrNamed, "WR", "Warm Restart",				10, 2,	   0, addrBitAsBit,   addrBitAsBit,   0));
	AddSpace(New CDnpSpace(addrNamed, "UE", "Unsolicited Msg Enable Mask",		10, 3,	   0, addrByteAsByte, addrByteAsByte, 0));
	AddSpace(New CDnpSpace(addrNamed, "UD", "Unsolicited Msg Disable Mask",		10, 4,	   0, addrByteAsByte, addrByteAsByte, 0));
	}

// Text Lookup

CString CDnp3BaseDriver::GetPropText(UINT uEnum)
{
	switch(uEnum) {

		case textCurrent:	return L"CurrentValue";	
		case textFlags:		return L"Flags";		
		case textTimeStamp:	return L"TimeStamp";
		case textClass:		return L"Class";
		case textFreeze:	return L"Freeze";
		case textControl:	return L"Control";
		case textOnTime:	return L"OnTime";
		case textOffTime:	return L"OffTime";
		case textObject:	return L"Object";
		case textPoint:		return L"Point";
		}

	return "";
	}

// Helpers

BOOL CDnp3BaseDriver::IsDouble(CAddress const &Addr)
{
	UINT uTable = Addr.a.m_Table;

	return ( uTable <= 238 && uTable >= 233 ); 
	}
	

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Base Driver
//

// Constructor

CDnp3SerialDriver::CDnp3SerialDriver(void)
{
	C3_PASSED();
	}

// Binding Control

UINT CDnp3SerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CDnp3SerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CDnp3SerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDnp3DriverOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Master Driver
//

// Instantiator

ICommsDriver * Create_Dnp3MasterSerialDriver(void)
{
	return New CDnp3MasterSerialDriver;
	}

// Constructor

CDnp3MasterSerialDriver::CDnp3MasterSerialDriver(void)
{
	m_uType		= driverMaster;

	m_wID		= 0x40C0;

	m_DriverName	= "Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "DNP3 Master";

	m_DevRoot	= "DEV";

	AddSpaces();
	
	C3_PASSED();
	}

// Configuration

CLASS CDnp3MasterSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDnp3MasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Slave Driver
//

// Instantiator

ICommsDriver * Create_Dnp3SlaveSerialDriver(void)
{
	return New CDnp3SlaveSerialDriver;
	}

// Constructor

CDnp3SlaveSerialDriver::CDnp3SlaveSerialDriver(void)
{
	m_uType		= driverSlave;

	m_wID		= 0x40C1;

	m_DriverName	= "Slave";
	
	m_Version	= "1.03";
	
	m_ShortName	= "DNP3 Slave";
	
	m_fSingle	= TRUE;

	AddSpaces();
	
	C3_PASSED();
	}

// Configuration

CLASS CDnp3SlaveSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDnp3SlaveDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Slave Driver v2
//

// Instantiator

ICommsDriver * Create_Dnp3SlaveSerialv2Driver(void)
{
	return New CDnp3SlaveSerialv2Driver;
	}

// Constructor

CDnp3SlaveSerialv2Driver::CDnp3SlaveSerialv2Driver(void)
{
	m_uType      = driverMaster;

	m_wID        = 0x40E6;

	m_DriverName = "Slave w/ Direct Tag Mapping";

	m_ShortName  = "DNP3 Slave DTM";

	m_Version.SetAt(0, '2');
	}

BOOL CDnp3SlaveSerialv2Driver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return Addr.a.m_Extra == 2;
	}

/////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Base Driver
//

// Constructor

CDnp3TcpDriver::CDnp3TcpDriver(void)
{
	C3_PASSED();
	}

// Binding Control

UINT CDnp3TcpDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CDnp3TcpDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CDnp3TcpDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDnp3DriverOptions);
	}

/////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Master Driver
//

// Instantiator

ICommsDriver * Create_Dnp3MasterTcpDriver(void)
{
	return New CDnp3MasterTcpDriver;
	}

// Constructor

CDnp3MasterTcpDriver::CDnp3MasterTcpDriver(void)
{
	m_uType		= driverMaster;

	m_wID		= 0x40C2;

	m_DriverName	= "IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "DNP3 Master";

	m_DevRoot	= "DEV";

	AddSpaces();
	
	C3_PASSED();
	}

// Configuration

CLASS CDnp3MasterTcpDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDnp3TcpMasterDeviceOptions);
	}

/////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Slave Driver
//

// Instantiator

ICommsDriver * Create_Dnp3SlaveTcpDriver(void)
{
	return New CDnp3SlaveTcpDriver;
	}

// Constructor

CDnp3SlaveTcpDriver::CDnp3SlaveTcpDriver(void)
{
	m_uType		= driverSlave;

	m_wID		= 0x40C3;

	m_DriverName	= "IP Slave";
	
	m_Version	= "1.04";
	
	m_ShortName	= "DNP3 Slave";

	AddSpaces();
	
	C3_PASSED();
	}

// Configuration

CLASS CDnp3SlaveTcpDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDnp3TcpSlaveDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Slave Driver v2
//

// Instantiator

ICommsDriver * Create_Dnp3SlaveTcpv2Driver(void)
{
	return New CDnp3SlaveTcpv2Driver;
	}

// Constructor

CDnp3SlaveTcpv2Driver::CDnp3SlaveTcpv2Driver(void)
{
	m_uType      = driverMaster;

	m_wID        = 0x40E7;
	
	m_DriverName = "IP Slave w/ Direct Tag Mapping";
	
	m_ShortName  = "DNP3 Slave DTM";

	m_Version.SetAt(0, '2');
	}

BOOL CDnp3SlaveTcpv2Driver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return Addr.a.m_Extra == 2;
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CDnp3AddrDialog, CStdAddrDialog);
		
// Constructor

CDnp3AddrDialog::CDnp3AddrDialog(CDnp3BaseDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element     = "Dnp3ElementDlg";
	}

// Message Map

AfxMessageMap(CDnp3AddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )
	AfxDispatchNotify(2004, CBN_SELCHANGE,  OnPropChange)
	
	AfxMessageEnd(CDnp3AddrDialog)
	};

// Message and Notification Handlers

BOOL CDnp3AddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData) 
{	
	BOOL r = CStdAddrDialog::OnInitDialog(Focus, dwData);

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	OnSpaceChange(1001, ListBox);

	return r;
	}

void CDnp3AddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2004);

	Combo.ResetContent();

	CDnpSpace * pSpace = GetCurSpace();

	if( pSpace && m_pDriver ) {

		CStringArray Class;

		CAddress Addr;

		pSpace->GetMaximum(Addr);

		CDnp3BaseDriver * pDriver = (CDnp3BaseDriver *)m_pDriver;

		if( pDriver ) {

			CString Prop;

			for( UINT u = 0; u <= Addr.a.m_Extra; u++ ) {

				Prop.Printf(L"%u-%s", u, pDriver->GetPropAsText(u, pSpace));

				Class.Append(Prop);
				}				

			Combo.AddStrings(Class);
		
			Combo.SetCurSel(m_pAddr->a.m_Extra);

			if( !m_fPart ) {
	
				CStdAddrDialog::OnSpaceChange(uID, Wnd);
				}

			OnPropChange(2004, Wnd);
			}
		}
	else {
		CStdAddrDialog::OnSpaceChange(uID, Wnd);		
		}
	}

void CDnp3AddrDialog::OnPropChange(UINT uID, CWnd &Wnd)
{
	if( uID == 2004 ) {

		if( m_pSpace ) {

			LoadType();

			CListBox &ListBox = (CListBox &) GetDlgItem(4001);

			if( ListBox.GetCurSel() == NOTHING ) {

				ListBox.SetCurSel(0);
				}
			}
		}
	}

// Overridables

BOOL CDnp3AddrDialog::AllowType(UINT uType)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2004);

	CDnpSpace * pSpace = GetCurSpace();

	if( pSpace && m_pDriver ) {

		if( pSpace->m_uTable < addrNamed ) {

			CDnp3BaseDriver * pDriver = (CDnp3BaseDriver *)m_pDriver;

			if( Combo.GetCurSel() || pDriver->IsFeedback(pSpace->m_uTable) ) {

				return uType == addrLongAsLong;
				}

			CAddress Addr;

			pSpace->GetMinimum(Addr);

			if( Addr.a.m_Type <= addrByteAsByte ) {

				return uType <= Addr.a.m_Type;
				}
			}
		}
	

	switch( uType ) {

		case addrBitAsBit:
		case addrByteAsByte:
		case addrWordAsWord:
		case addrLongAsLong:
		case addrRealAsReal:
			
			return TRUE;
		}

	return FALSE;
	}

void CDnp3AddrDialog::SetAddressText(CString Text)
{
	CEditCtrl &Index = (CEditCtrl &) GetDlgItem(2002);

	CComboBox &Combo = (CComboBox &) GetDlgItem(2004);

	/*Combo.ShowWindow(TRUE);

	CDnpSpace * pSpace = GetCurSpace();;

	if( pSpace && pSpace->m_uTable != addrNamed) {

		if( pSpace->m_uTable == 239 ) {
				
			Index.SetWindowText("0");
			
			Index.EnableWindow(FALSE);

			Combo.EnableWindow(TRUE);

			return;
			}
		}*/ 

	if( Text.IsEmpty() ) {

		Index.SetWindowText(L"");

		Index.EnableWindow(FALSE);

		Combo.SetCurSel(0);

		Combo.EnableWindow(FALSE);
		}
	else {	
		UINT uFind = Text.Find(':');

		if( uFind < NOTHING ) {

			Index.SetWindowText(Text.Mid(0, uFind));

			Index.EnableWindow(TRUE);

			CString Sel = Text.Mid(uFind + 1);

			CComboBox &Combo = (CComboBox &) GetDlgItem(2004);

			UINT uCount = Combo.GetCount();

			for( UINT u = 0; u < uCount; u++ ) {

				CString ComboSel = Combo.GetLBText(u).ToLower();

				ComboSel = ComboSel.Mid(ComboSel.Find('-') + 1);

				if( Sel == ComboSel.ToLower() ) {

					Combo.SetCurSel(u);

					break;
					}
				}
			
			Combo.UpdateWindow();

			Combo.EnableWindow(TRUE);
			}
		else {	
			Index.SetWindowText(Text);

			Index.EnableWindow(TRUE);

			Combo.SetCurSel(0);

			Combo.EnableWindow(FALSE);
			}
		}
	}

CString CDnp3AddrDialog::GetAddressText(void)
{
	CEditCtrl &Index   = (CEditCtrl &) GetDlgItem(2002);

	CComboBox &Combo   = (CComboBox &) GetDlgItem(2004);

	CDnpSpace * pSpace = GetCurSpace();

	if( pSpace && Index.IsEnabled() ) {

		CString Text = Index.GetWindowText();

		if( Combo.IsEnabled() ) {

			Text += CPrintf(L":%u", Combo.GetCurSel());
			}

		return Text;
		}

	return L"";
	}

void CDnp3AddrDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	if( !m_pSpace->IsNamed() ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(2004);

		CString Prop = Combo.GetWindowText();

		Prop = Prop.Mid(Prop.Find('-') + 1);

		CAddress Addr;

		Addr.a.m_Type = GetTypeCode();

		m_pSpace->GetMinimum(Addr);

		m_pDriver->ExpandAddress(Min, m_pConfig, Addr);

		Min = Min.Mid(0, Min.Find(':') + 1);

		Min += Prop;

		m_pSpace->GetMaximum(Addr);

		m_pDriver->ExpandAddress(Max, m_pConfig, Addr);

		Max = Max.Mid(0, Max.Find(':') + 1);

		Max += Prop;

		Rad = m_pSpace->GetRadixAsText();
		}

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}


// Helpers

CDnpSpace * CDnp3AddrDialog::GetCurSpace(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	INDEX i = INDEX(ListBox.GetCurSelData());

	UINT uSel = ListBox.GetCurSel();

	return (CDnpSpace *)(uSel > 0 ? m_pDriver->GetSpace(i) : m_pDriver->GetSpace(*m_pAddr));
	}

//////////////////////////////////////////////////////////////////////////
//
// DNP Space Wrapper
//

// Constructor

CDnpSpace::CDnpSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT em) : CSpace(uTable, p, c, r, n, x, t, s)
{
	m_ExtraMax = em;
	}

// Limits

void CDnpSpace::GetMinimum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);
	
	Addr.a.m_Type = m_uType;
	}

void CDnpSpace::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	Addr.a.m_Extra = m_ExtraMax;
	}

// Type Access

UINT CDnpSpace::GetWholeType(CString Type)
{
	if( Type == L"BYTE" ) {

		return addrByteAsByte;
		}
		
	if( Type == L"WORD" ) {

		return addrWordAsWord;
		}

	if( Type == L"LONG" ) {

		return addrLongAsLong;
		}

	if( Type == L"REAL" ) {

		return addrRealAsReal;
		}

	return addrBitAsBit;
	}

// End of File
