
#include "intern.hpp"

#include "impact.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Camera Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CImpactCameraDriverOptions, CUIItem);

// Constructor

CImpactCameraDriverOptions::CImpactCameraDriverOptions(void)
{
	
	}

// UI Managament

void CImpactCameraDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {
	
		}
	}

// Download Support

BOOL CImpactCameraDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CImpactCameraDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Camera Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CImpactCameraDeviceOptions, CUIItem);

// Constructor

CImpactCameraDeviceOptions::CImpactCameraDeviceOptions(void)
{
	m_Camera = 0;

	m_IP     = DWORD(MAKELONG(MAKEWORD(128, 0), MAKEWORD(168, 192)));

	m_Port   = 80;

	m_Time4  = 10;

	m_Scale  = 1;

	m_Width  = 320;

	m_Height = 240;
	}

// UI Managament

void CImpactCameraDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Scale" ) {

			pWnd->EnableUI("Width", m_Scale);

			pWnd->EnableUI("Height", m_Scale);
			}

		pWnd->UpdateUI();	
		}
	}

// Download Support

BOOL CImpactCameraDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Camera));

	Init.AddLong(LONG(m_IP));
	
	Init.AddWord(WORD(m_Port));

	Init.AddWord(WORD(m_Time4));

	Init.AddByte(BYTE(m_Scale));

	Init.AddWord(WORD(m_Width));

	Init.AddWord(WORD(m_Height));

	return TRUE;
	}

// Persistence

void CImpactCameraDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		if(Name == "Device") {

			m_Camera = Tree.GetValueAsInteger();

			continue;
			}

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

// Meta Data Creation

void CImpactCameraDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Camera);

	Meta_AddInteger(IP);

	Meta_AddInteger(Port);

	Meta_AddInteger(Time4);

	Meta_AddInteger(Scale);

	Meta_AddInteger(Width);

	Meta_AddInteger(Height);
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Camera Driver
//

// Instantiator

ICommsDriver *	Create_ImpactCameraDriver(void)
{
	return New CImpactCameraDriver;
	}

// Constructor

CImpactCameraDriver::CImpactCameraDriver(void)
{
	m_wID		= 0x40A4;

	m_uType		= driverCamera;
	
	m_Manufacturer	= "PPT Vision";
	
	m_DriverName	= "IMPACT Vision Camera";
	
	m_Version	= "1.00";

	m_DevRoot	= "Cam";
	
	m_ShortName	= "IMPACT";
	}

// Configuration

CLASS CImpactCameraDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CImpactCameraDriverOptions);
	}

CLASS CImpactCameraDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CImpactCameraDeviceOptions);
	}

// Binding Control

UINT CImpactCameraDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
