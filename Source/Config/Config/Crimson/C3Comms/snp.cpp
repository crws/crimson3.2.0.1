
#include "intern.hpp"

#include "snp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SNP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSNPDeviceOptions, CUIItem);

// Constructor

CSNPDeviceOptions::CSNPDeviceOptions(void)
{
	m_Drop		= "";

	m_fBreakFree	= FALSE;

	m_Slot		= 1;
	}

// UI Managament

void CSNPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Drop" ) {

			CString Text = pItem->GetDataAccess(Tag)->ReadString(pItem);

			for ( UINT i = 0; i < Text.GetLength(); i++ ) {

				if ( ( Text[i] >= 'A' ) && ( Text[i] <= 'Z' ) )

					continue;

				if ( ( Text[i] >= 'a' ) && ( Text[i] <= 'z' ) )

					continue;


				if ( ( Text[i] >= '0' ) && ( Text[i] <= '9' ) )

					continue;

				if ( !i ) {

					pWnd->Error(CString(IDS_SNP_ID_ERROR1));

					break;
					}

				if ( ( Text[i] >= '#' ) && ( Text[i] <= '&' ) )

					continue;

				if ( ( Text[i] == '+' ) || ( Text[i] == '-' ) || ( Text[i] == '_' ) || Text[i] == '@' )
				
					continue;

				if ( ( Text[i] >= '<' ) && ( Text[i] <= '>' ) )

					continue;

				pWnd->Error(CString(IDS_SNP_ID_ERROR));

				break;
				}
			}
		}
	}

// Download Support

BOOL CSNPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddText(m_Drop);

	Init.AddByte(BYTE(m_fBreakFree));

	Init.AddByte(BYTE(m_Slot));

	return TRUE;
	}

// Meta Data Creation

void CSNPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Drop);
	Meta_AddBoolean(BreakFree);
	Meta_AddInteger(Slot);
	}

//////////////////////////////////////////////////////////////////////////
//
// SNP Master Serial Driver
//

// Instantiator

ICommsDriver *	Create_SNPDriver(void)
{
	return New CSNPDriver;
	}

// Constructor

CSNPDriver::CSNPDriver(void)
{
	m_wID		= 0x337B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "GE";
	
	m_DriverName	= "SNP Master";
	
	m_Version	= "1.05";
	
	m_ShortName	= "SNP Master";

	AddSpaces();
	}

// Binding Control

UINT CSNPDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSNPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS422;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CSNPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSNPDeviceOptions);
	}

// Implementation

void CSNPDriver::AddSpaces(void)
{
	AddSpace(New CSpace('R', "R",	"Data Registers",	10, 1,32767, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('A', "AI",	"Analog Inputs",	10, 1, 9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('B', "AQ",	"Analog Outputs",	10, 1, 9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('I', "I",	"Digital Inputs",	10, 1,32768, addrBitAsBit));
	AddSpace(New CSpace('M', "M",	"Memory Coils",		10, 1,32768, addrBitAsBit));
	AddSpace(New CSpace('Q', "Q",	"Digital Outputs",	10, 1,32768, addrBitAsBit));
	AddSpace(New CSpace('T', "T",	"Temporary Coils",	10, 1, 9999, addrBitAsBit));
	AddSpace(New CSpace('E', "SA",	"SA Discretes",		10, 1,  128, addrBitAsBit));
	AddSpace(New CSpace('D', "SB",	"SB Discretes",		10, 1,  128, addrBitAsBit));
	AddSpace(New CSpace('C', "SC",	"SC Discretes",		10, 1,  128, addrBitAsBit));
	AddSpace(New CSpace('S', "S",	"S Discretes",		10, 1,  128, addrBitAsBit));
	AddSpace(New CSpace('G', "G",	"Global Discretes",	10, 1, 7680, addrBitAsBit));
	}

// Address Helpers

BOOL CSNPDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 'R':
		case 'A':
		case 'B':
		
			return FALSE;
		}

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// SNP-X Master Serial Driver
//

// Instantiator

ICommsDriver *	Create_SNPXDriver(void)
{
	return New CSNPXDriver;
	}

// Constructor

CSNPXDriver::CSNPXDriver(void)
{
	m_wID		= 0x4035;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "GE";
	
	m_DriverName	= "SNP-X Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "SNP-X Master";

	C3_PASSED();
	}

// End of File
