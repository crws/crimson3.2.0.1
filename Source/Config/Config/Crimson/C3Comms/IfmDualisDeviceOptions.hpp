
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DUALISVISIONSENSORDEVICEOPTIONS_HPP
	
#define	INCLUDE_DUALISVISIONSENSORDEVICEOPTIONS_HPP

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Device Options
//

class CDualisVisionSensorDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDualisVisionSensorDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT         m_Camera;
		UINT         m_IP;
		UINT         m_Port;
		UINT         m_Time4;
		CString	     m_Start;
		CString	     m_Stop;
		CString	     m_Separator;
		UINT         m_Image;
		UINT	     m_Protocol;

		// Persistence
		void Load(CTreeFile &File);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Camera Device Options
//

class CDualisVisionSensorCameraDeviceOptions : public CDualisVisionSensorDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDualisVisionSensorCameraDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Camera Device Options
//

class CDualisVisionSensorDataDeviceOptions : public CDualisVisionSensorDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDualisVisionSensorDataDeviceOptions(void);
	};

// End of File

#endif
