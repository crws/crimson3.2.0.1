
#include "intern.hpp"

#include "yaskns6.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// YaskawaNS600 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaNS600DeviceOptions, CUIItem);

// Constructor

CYaskawaNS600DeviceOptions::CYaskawaNS600DeviceOptions(void)
{
	m_Drop = 1;
	}

// Download Support

BOOL CYaskawaNS600DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaNS600DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Sigma II Comms Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaNS600Driver(void)
{
	return New CYaskawaNS600Driver;
	}

// Constructor

CYaskawaNS600Driver::CYaskawaNS600Driver(void)
{
	m_wID		= 0x3384;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "NS600 Indexer";
	
	m_Version	= "1.10";
	
	m_ShortName	= "Yaskawa NS600 Indexer";

	AddSpaces();
	}

// Binding Control

UINT	CYaskawaNS600Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CYaskawaNS600Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration
CLASS	CYaskawaNS600Driver::GetDeviceConfig()
{
	return AfxRuntimeClass(CYaskawaNS600DeviceOptions);
	}

// Implementation

void	CYaskawaNS600Driver::AddSpaces(void)
{
	AddSpace( New CSpace(3,  "PAR",  "PRM : Parameter Read-Write",			16,	0,	0xFFF, LL) );
	AddSpace( New CSpace(4,  "PAT",  "TRM : Temporary Parameter Write",		16,	0,	0xFFF, LL) );
	AddSpace( New CSpace(AN, "ABS",  "ABSPGRES : Absolute PG Reset",		10,	20,	0,     BB) );
	AddSpace( New CSpace(AN, "ACC",  "ACC : Acceleration Reservation",		10,	21,	0,     LL) );
	AddSpace( New CSpace(1,  "ALMH", "ALMn : Alarm History Read",			10,	0,	9,     LL) );
	AddSpace( New CSpace(AN, "ALM",  "ALM : Alarm or Warning Read",			10,	22,	0,     LL) );
	AddSpace( New CSpace(AN, "ALR",  "ARES :Alarm Reset",				10,	23,	0,     BB) );
	AddSpace( New CSpace(AN, "ATC",  "ALMTRCCLR : Alarm Trace Clear",		10,	24,	0,     BB) );
	AddSpace( New CSpace(AN, "ATI",  "INERTIA : Auto-tuning Inertia Display",	10,	25,	0,     LL) );
	AddSpace( New CSpace(AN, "ATS",  "TUNESTORE : Auto-tuning Inertia Save",	10,	26,	0,     BB) );
	AddSpace( New CSpace(AN, "CZE",  "CURZERO : Motor Current Zero Adjustment",	10,	27,	0,     BB) );
	AddSpace( New CSpace(AN, "DBM",  "DBRMS : DB Load Ratio Monitor",		10,	28,	0,     LL) );
	AddSpace( New CSpace(AN, "DEC",  "DEC : Deceleration Reservation",		10,	29,	0,     LL) );
	AddSpace( New CSpace(AN, "ERR",  "ERR : Error Read",				10,	30,	0,     LL) );
	AddSpace( New CSpace(AN, "HOM",  "ZRN : Homing Start Execution",		10,	31,	0,     BB) );
	AddSpace( New CSpace(AN, "JGP",  "JOGP : Jog Forward",				10,	38,	0,     LL) );
	AddSpace( New CSpace(AN, "JGN",  "JOGN : Jog Reverse",				10,	39,	0,     LL) );
	AddSpace( New CSpace(AN, "JSI",  "JSPDINIT : JOG Speed Table Initialization",	10,	40,	0,     LL) );
	AddSpace( New CSpace(2,  "JSR",  "JSPDT : Jog speed reservation",		10,	0,	15,    LL) );
	AddSpace( New CSpace(AN, "JSS",  "JSPDSTORE : JOG Speed Table Save",		10,	41,	0,     BB) );
	AddSpace( New CSpace(AN, "MLS",  "MLTLIMSET : Multi Turn Limit Setting",	10,	42,	0,     LL) );
	AddSpace( New CSpace(AN, "MON1",   "PUN : Current Issue Position",		10,	43,	0,     LL) );
	AddSpace( New CSpace(AN, "MON2",   "PER : Position Error",			10,	44,	0,     LL) );
	AddSpace( New CSpace(AN, "MON3",   "NFB : Motor Speed",				10,	45,	0,     LL) );
	AddSpace( New CSpace(AN, "MON4",   "NREF : Speed Reference",			10,	46,	0,     LL) );
	AddSpace( New CSpace(AN, "MON5",   "TREF : Torque Reference",			10,	47,	0,     LL) );
	AddSpace( New CSpace(AN, "MON6",   "STS : Status Flag",				10,	48,	0,     LL) );
	AddSpace( New CSpace(AN, "MON7",   "PFB : Current Motor Position",		10,	49,	0,     LL) );
	AddSpace( New CSpace(AN, "MON8",   "POS : Target Position",			10,	50,	0,     LL) );
	AddSpace( New CSpace(AN, "MON9",   "DST : Target Distance",			10,	51,	0,     LL) );
	AddSpace( New CSpace(AN, "MON10",  "RPOS : Registration Target Position",	10,	52,	0,     LL) );
	AddSpace( New CSpace(AN, "MON11",  "RDST : Registration Target Distance",	10,	53,	0,     LL) );
	AddSpace( New CSpace(AN, "MSZ",  "MTSIZE : Motor Capacity Display",		10,	54,	0,     LL) );
	AddSpace( New CSpace(AN, "MTD",  "MTTYPE : Motor Type Display",			10,	55,	0,     LL) );
	AddSpace( New CSpace(AN, "NSI",  "IN2 : NS600 Input Signal Monitor",		10,	56,	0,     LL) );
	AddSpace( New CSpace(AN, "NSJ",  "IN2TEST : NS600 Input Signal Reservation",	10,	57,	0,     LL) );
	AddSpace( New CSpace(AN, "NSM",  "OUT2 : NS600 Side Output Signal Monitor",	10,	58,	0,     LL) );
	AddSpace( New CSpace(AN, "NSR",  "OUT2TEST : NS600 Output Signal Reservation",	10,	59,	0,     LL) );
	AddSpace( New CSpace(AN, "NST",  "TYPE : NS600 Type Code Display",		10,	60,	0,     LL) );
	AddSpace( New CSpace(AN, "NSV",  "VER : NS600 Software Version Display",	10,	61,	0,     LL) );
	AddSpace( New CSpace(AN, "NSY",  "YSPEC : NS600 Y Spec. No. Display",		10,	62,	0,     LL) );
	AddSpace( New CSpace(AN, "PAI",  "PRMINIT : Parameter Initialization",		10,	63,	0,     LL) );
	AddSpace( New CSpace(AN, "PT",   "PGTYPE : PG Type Display",			10,	64,	0,     LL) );
	AddSpace( New CSpace(AN, "PV",   "PGVER : PG Software Version Display",		10,	65,	0,     LL) );
	AddSpace( New CSpace(5,  "PGA",  "RDSTT : Program Table Register Distance",	10,	0,	127,   LL) );
	AddSpace( New CSpace(6,  "PGB",  "RSPDT : Program Table Register Speed",	10,	0,	127,   LL) );
	AddSpace( New CSpace(7,  "PGC",  "SPDT : Program Table Position Speed",		10,	0,	127,   LL) );
//	AddSpace( New CSpace(8,  "PGD",  "Program Table POST - Alpha",			10,	0,	127,   LL) );
//	AddSpace( New CSpace(9,  "PGE",  "Program Table POST - Numeric",		10,	0,	127,   LL) );
	AddSpace( New CSpace(AN, "PGI",  "PGMINIT : Program Initialization",		10,	66,	0,     LL) );
	AddSpace( New CSpace(11, "PGL",  "LOOPT : Program Table LOOP Read-Write",	10,	0,	127,   LL) );
	AddSpace( New CSpace(12, "PGN",  "NEXTT : Program Table NEXT Read-Write",	10,	0,	127,   LL) );
	AddSpace( New CSpace(AN, "PGP",  "PGMSTEP : Program Pass Through Monitor",	10,	67,	0,     LL) );
	AddSpace( New CSpace(AN, "PGQ",  "LOOP : Program Pass Through Monitor",		10,	68,	0,     LL) );
	AddSpace( New CSpace(AN, "PGR",  "PGMRES : Program Reset",			10,	69,	0,     LL) );
	AddSpace( New CSpace(AN, "PGS",  "STARTsss : Program Operation Start",		10,	71,	0,     LL) );
	AddSpace( New CSpace(AN, "PGT",  "START : Program Operation Restart",		10,	70,	0,     LL) );
	AddSpace( New CSpace(AN, "PGV",  "PGMSTORE : Program Table Save",		10,	72,	0,     LL) );
//	AddSpace( New CSpace(13, "PGW",  "Program Table Event - Alpha",			10,	0,	127,   LL) );
//	AddSpace( New CSpace(14, "PGX",  "Program Table Event - Numeric",		10,	0,	127,   LL) );
	AddSpace( New CSpace(AN, "PGY",  "STOP : Program Operation Interruption",	10,	73,	0,     LL) );
	AddSpace( New CSpace(AN, "PGZ",  "EVTIME : Program Lapse of Time Monitor",	10,	74,	0,     LL) );
	AddSpace( New CSpace(AN, "POA",  "STA : Positioning Start Absolute",		10,	75,	0,     LL) );
	AddSpace( New CSpace(AN, "POR",  "STI : Positioning Start Relative",		10,	76,	0,     LL) );
	AddSpace( New CSpace(AN, "POS",  "ST : Positioning Start",			10,	77,	0,     LL) );
	AddSpace( New CSpace(AN, "POT",  "POSA : Target Position Reservation",		10,	78,	0,     LL) );
	AddSpace( New CSpace(AN, "POU",  "POSI : Target Position Reservation",		10,	79,	0,     LL) );
	AddSpace( New CSpace(AN, "POX",  "SKIP : Positioning Stop",			10,	80,	0,     BB) );
	AddSpace( New CSpace(AN, "POY",  "HOLD : Positioning Interruption",		10,	81,	0,     BB) );
	AddSpace( New CSpace(AN, "PRA",  "RSA : Position Start with Registration",	10,	83,	0,     LL) );
	AddSpace( New CSpace(AN, "PRR",  "RSI : Position Start with Registration",	10,	84,	0,     LL) );
	AddSpace( New CSpace(AN, "PRS",  "RS : Position Start with Registration",	10,	85,	0,     LL) );
	AddSpace( New CSpace(AN, "PRV",  "SPD : Positioning Speed Reservation",		10,	86,	0,     LL) );
	AddSpace( New CSpace(110,"PTRF", "Read Program Table Position Function",	10,	0,	127,   LL) );
	AddSpace( New CSpace(111,"PTRV", "Read Program Table Position Value",		10,	0,	127,   LL) );
	AddSpace( New CSpace(112,"PTWS", "Send PTWA+PTWN <Data=Table Position>",	10,	0,	0,     LL) );
	AddSpace( New CSpace(113,"PTWF", "   Write Program Table Position Function",	10,	0,	127,   LL) );
	AddSpace( New CSpace(114,"PTWV", "   Write Program Table Position Value",	10,	0,	127,   LL) );
	AddSpace( New CSpace(AN, "RGM",  "RGRM : Regenerative Load Ratio Monitor",	10,	87,	0,     LL) );
	AddSpace( New CSpace(AN, "RJF",  "RJOGP : Jog Forward with Registration",	10,	88,	0,     LL) );
	AddSpace( New CSpace(AN, "RJR",  "RJOGN : Jog Reverse with Registration",	10,	89,	0,     LL) );
	AddSpace( New CSpace(AN, "RRD",  "RDST : Registration Distance Reservation",	10,	90,	0,     LL) );
	AddSpace( New CSpace(AN, "RRS",  "RSPD : Registration Speed Reservation",	10,	91,	0,     LL) );
	AddSpace( New CSpace(AN, "RST",  "RES : Reset",					10,	92,	0,     BB) );
	AddSpace( New CSpace(AN, "SET",  "ZSET : Coordinates Setting",			10,	93,	0,     LL) );
	AddSpace( New CSpace(AN, "SFF",  "STIFF : Rigidity Monitor/Reservation",	10,	94,	0,     LL) );
	AddSpace( New CSpace(AN, "SGI",  "IN1 : SGDH Input Signal Monitor",		10,	95,	0,     LL) );
	AddSpace( New CSpace(AN, "SGO",  "OUT1 : SGDH Side Output Signal Monitor",	10,	96,	0,     LL) );
	AddSpace( New CSpace(AN, "SGT",  "SVTYPE : SGDH Type Code Display",		10,	97,	0,     LL) );
	AddSpace( New CSpace(AN, "SGV",  "SVVER : SGDH Software Version Display",	10,	98,	0,     LL) );
	AddSpace( New CSpace(AN, "SGY",  "SVYSPEC : SGDH Y Spec. No. Display",		10,	99,	0,     LL) );
	AddSpace( New CSpace(AN, "SVO",  "SVON/SVOFF : Servo ON / OFF",			10,	100,	0,     BB) );
	AddSpace( New CSpace(AN, "TRMS", "TRMS : Total Load ratio Monitor",		10,	101,	0,     LL) );
	AddSpace( New CSpace(AN, "ZI",   "ZONEINIT : ZONE Table Initialization",	10,	102,	0,     LL) );
	AddSpace( New CSpace(15, "ZN",   "ZONENT : Zone negative position limit",	10,	0,	31,    LL) );
	AddSpace( New CSpace(16, "ZP",   "ZONEPT : Zone positive position limit",	10,	0,	31,    LL) );
	AddSpace( New CSpace(AN, "ZS",   "ZONESTORE : ZONE Table Save",			10,	103,	0,     BB) );
	}

// End of File
