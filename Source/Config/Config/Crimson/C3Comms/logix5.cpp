
#include "intern.hpp"

#include "logix5.hpp"

#include "df1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver
//

// Constructor

CABLogix5Driver::CABLogix5Driver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Native Tag Addressing";
	
	m_Version	= "1.01";
	
	m_ShortName	= "AB Tag Names";

	C3_PASSED();
	}

// Address Management

BOOL CABLogix5Driver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CABLogix5DeviceOptions *) pConfig)->ParseAddress(Error, Addr, Text);
	}

BOOL CABLogix5Driver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CABLogix5DeviceOptions *) pConfig)->ExpandAddress(Text, Addr);
	}

BOOL CABLogix5Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CABLogix5DeviceOptions *) pConfig)->SelectAddress(hWnd, Addr, fPart);
	}

BOOL CABLogix5Driver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CABLogix5DeviceOptions *) pConfig)->ListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver - Serial Master
//

// Instantiator

ICommsDriver *	Create_ABLogix5SerialMaster(void)
{
	return New CABLogix5SerialMaster;
	}

// Constructor

CABLogix5SerialMaster::CABLogix5SerialMaster(void)
{
	m_wID		= 0x4023;
	}

// Binding Control

UINT CABLogix5SerialMaster::GetBinding(void)
{
	return bindStdSerial;
	}

void CABLogix5SerialMaster::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CABLogix5SerialMaster::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDF1MasterSerialDriverOptions);
	}

// Configuration

CLASS CABLogix5SerialMaster::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABLogix5SerialMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver - EIP Master
//

// Instantiator

ICommsDriver *	Create_ABLogix5EIPMaster(void)
{
	return New CABLogix5EIPMasterDriver;
	}

// Constructor

CABLogix5EIPMasterDriver::CABLogix5EIPMasterDriver(void)
{
	m_wID		= 0x4024;
	}

// Binding Control

UINT CABLogix5EIPMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CABLogix5EIPMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CABLogix5EIPMasterDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CABLogix5EIPMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABLogix5EIPMasterDeviceOptions);
	}

// End of File
