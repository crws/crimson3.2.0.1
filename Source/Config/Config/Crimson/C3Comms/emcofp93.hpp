//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EMCOFP93_HPP
	
#define	INCLUDE_EMCOFP93_HPP

// String Items
#define	C19	19	// Table 3
#define	C24	24	// Table 4
#define	C26	26	// Table 5
#define	C38	38	// Table 6
#define	C44	44	// Table 7
#define	C49	49	// Table 8
#define	C94	94	// Table 9

// All other Items
#define	C03	 3
#define	C04	 4
#define	C05	 5
#define	C06	 6
#define	C51	51
#define	C46	46
#define	C41	41
#define	C21	21
#define	C11	11
#define	C15	15
#define	C35	35
#define	C31	31
#define	C50	50
#define	C91	91
#define	C92	92
#define	C93	93
#define	C66	66
#define	C25	25
#define	C18	18
#define	C45	45
#define	C68	68
#define	C69	69
#define	C90	90
#define	C08	 8
#define	C07	 7
#define	C40	40
#define	C48	48
#define	C43	43
#define	C23	23
#define	C13	13
#define	C17	17
#define	C37	37
#define	C33	33
#define	C47	47
#define	C42	42
#define	C22	22
#define	C12	12
#define	C16	16
#define	C36	36
#define	C32	32
#define	C62	62
#define	C61	61
#define	C20	20
#define	C65	65
#define	C01	 1
#define	C02	 2
#define	C27	27
#define	C67	67
#define	C10	10
#define	C14	14
#define	C34	34
#define	C81	81
#define	C76	76
#define	C77	77
#define	C80	80
#define	C75	75
#define	C78	78
#define	C79	79
#define	C82	82
#define	C83	83
#define	C85	85
#define	C84	84
#define	C71	71
#define	C70	70
#define	C74	74
#define	C72	72
#define	C73	73
#define	C56	56
#define	C52	52
#define	C53	53
#define	C57	57
#define	C58	58
#define	C54	54
#define	C55	55
#define	C59	59
#define	C63	63
#define	C60	60
#define	C64	64
#define	C30	30

//////////////////////////////////////////////////////////////////////////
//
// EMCO FP-93 Device Options
//

class CEMCOFP93DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEMCOFP93DeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Driver
//

class CEMCOFP93SerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEMCOFP93SerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);

		// Helper
		UINT IsStringItem(UINT uID, UINT uOffset);
		UINT FLItemToString(UINT uOffset);
	};

// End of File

#endif
