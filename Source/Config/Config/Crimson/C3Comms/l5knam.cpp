
#include "intern.hpp"

#include "l5kdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Tags List
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kNames, CMetaItem);

// Constructor

CABL5kNames::CABL5kNames(void)
{
	m_pConfig = NULL;

	m_pTags   = New CABL5kItemList;
	}

// Destructor

CABL5kNames::~CABL5kNames(void)
{
	}

// Operations

void CABL5kNames::LoadTags(CArray <CABL5kDesc> const &Tags, CArray <CABL5kDesc> &Fail, CString Scope)
{
	m_Scope = Scope;	
	
	LoadTags(Tags, Fail);
	}

void CABL5kNames::LoadTags(CArray <CABL5kDesc> const &Tags, CArray <CABL5kDesc> &Fail)
{
	m_pTags->DeleteAllItems(TRUE);
	
	CArray <CABL5kDesc> Alias;

	for( UINT n = 0; n < Tags.GetCount(); n ++ ) {

		CABL5kDesc Tag = Tags[n];

		if( Tag.IsAlias() ) {

			Alias.Append(Tag);
			
			continue;
			}

		if( !MakeTag(m_pTags, Tag.m_Name, Tag.m_Type, L"") ) {

			PrependScope(Tag);
			
			Fail.Append(Tag);
			}
		}

	UINT uDepth = 0;

	ResolveAliasTags(Alias, uDepth);

	Fail.Append(Alias);
	}

void CABL5kNames::LoadTag(CABL5kItemList *pTags, CString const &Name, CString const &Type, CString const &Info)
{
	CABL5kItem *pTag = New CABL5kItem;

	pTag->m_Name = Name;

	PrependScope(*pTag);		
	
	pTag->m_Type = Type;
	
	pTag->m_Info = Info;

	pTags->AppendItem(pTag);

	m_pConfig->AddToMap(pTag);
	}

// Persistance

void CABL5kNames::Init(void)
{
	CMetaItem::Init();

	FindConfig();
	}

void CABL5kNames::PostLoad(void)
{
	CMetaItem::PostLoad();

	FindConfig();
	}

// Meta Data Creation

void CABL5kNames::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString(Scope);
	Meta_AddCollect(Tags);
	}

// Implementation

void CABL5kNames::PrependScope(CABL5kDesc &Tag)
{
	if( GetParent()->IsKindOf(AfxRuntimeClass(CABL5kDeviceOptions)) ) {

		return;
		}

	if( !m_Scope.IsEmpty() ) {
		
		Tag.m_Name = m_Scope + "," + Tag.m_Name;
		}
	}

void CABL5kNames::PrependScope(CABL5kItem &Tag)
{
	if( GetParent()->IsKindOf(AfxRuntimeClass(CABL5kDeviceOptions)) ) {

		return;
		}

	if( !m_Scope.IsEmpty() ) {
		
		Tag.m_Name = m_Scope + "," + Tag.m_Name;
		}
	}

void CABL5kNames::FindConfig(void)
{
	CItem *pItem = this;

	while( pItem ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CABL5kDeviceOptions)) ) {

			break;
			}

		pItem = pItem->GetParent();
		}

	if( pItem ) {

		m_pConfig = (CABL5kDeviceOptions *) pItem;
		}
	}

void CABL5kNames::ResolveAliasTags(CArray <CABL5kDesc> &List, UINT uDepth)
{
	CArray <CABL5kDesc> Fail;

	while( List.GetCount() ) {

		for( UINT n = 0; n < List.GetCount(); n ++ ) {

			CABL5kDesc     Tag = List[n];

			CABL5kItem *pAlias = FindTag(Tag.m_Type);

			CString       Name = Tag.m_Name;

			CString       Type = pAlias ? pAlias->m_Type : L"DINT";

			if( Tag.m_Type.FindRev('[') < NOTHING ) {

				UINT p1;

				if( (p1 = Type.FindRev('[')) < NOTHING ) {
					
					Type = Type.Left(p1);
					}
				}

			UINT p1;

			if( (p1 = Tag.m_Type.FindRev('.')) < NOTHING ) {

				CString Elem = Tag.m_Type.Mid(p1+1);

				if( m_pConfig->m_Types.IsAtomicType(Type) ) {

					PTXT  pError = NULL;

					// cppcheck-suppress ignoredReturnValue

					wcstoul(Elem, &pError, 10);

					if( !*pError ) {
				
						Type = L"BOOL";
						}
					}
				}
			
			CString       Info = Tag.m_Type;
			
			if( FindTag(Name) || !MakeTag(m_pTags, Name, Type, Info) ) {

				Fail.Append(Tag);
				}

			List.Remove(n);

			break;
			}
		}

	if( Fail.GetCount() && uDepth < 2 ) {

		ResolveAliasTags(Fail, ++ uDepth);
		}

	if( Fail.GetCount() ) {		

		for( UINT n = 0; n < Fail.GetCount(); n ++ ) {

			CABL5kDesc Tag = Fail[n];

			PrependScope(Tag);
			
			List.Append(Tag);	
			}		
		}
	}

// Tag List

BOOL CABL5kNames::MakeTag(CABL5kItemList *pTags, CString const &Name, CString Type, CString const &Info)
{
	if( m_pConfig->m_Types.IsAtomicType(Type) ) {

		LoadTag(pTags, Name, Type, Info);

		return TRUE;;
		}

	CArray <UINT> uDims;

	if( m_pConfig->ParseDims(uDims, Type) ) {

		CABL5kStruct *pRoot = New CABL5kStruct;

		pRoot->m_Name = Name;

		pRoot->m_Type = Type;

		pRoot->m_Info = Info;

		pTags->AppendItem(pRoot);
				
		UINT uCount = uDims.GetCount();

		Type = Type.StripToken('[');

		UINT n;

		UINT uSize;

		for( n = 0, uSize = 1; n < uCount; uSize *= uDims[n++] );

		for( n = 0; n < uSize; n++ ) {

			CString Array = Name;;

			m_pConfig->ExpandDims(Array, uDims, n); 

			CABL5kStruct *pTag = New CABL5kStruct;

			pTag->m_Name = Array;

			pTag->m_Type = Type;

			pTag->m_Info = Info;

			pRoot->m_pMembers->AppendItem(pTag);

			ExpandType(pTag->m_pMembers, Array, Type);
			}

		return TRUE;
		}

	CABL5kStruct *pRoot = New CABL5kStruct;

	pRoot->m_Name = Name;

	pRoot->m_Type = Type;

	pRoot->m_Info = Info;

	pTags->AppendItem(pRoot);

	return ExpandType(pRoot->m_pMembers, Name, Type);
	}

CABL5kItem * CABL5kNames::FindTag(CString const &Name)
{
	UINT p1, p2 = 0;

	while( (p1 = Name.Find('.', p2)) < NOTHING ) {

		CString Root = Name.Left(p1);

		CABL5kItem *pRoot = FindTag(Root);
		
		if( pRoot ) {

			if( pRoot->IsStruct() ) {
				
				CABL5kItemList const *pTags = ((CABL5kStruct *) pRoot)->m_pMembers;

				CABL5kItem *pFind = FindTag(pTags, Name);
				
				if( pFind ) {					
					
					return pFind;
					}

				p2 = p1 + 1;

				continue;
				}

			return pRoot;
			}
		
		//AfxAssert(FALSE);

		return NULL;
		}

	if( (p1 = Name.Find('[')) < NOTHING ) {

		CString Root = Name.Left(p1);
		
		CABL5kItem *pRoot = FindTag(Root);
		
		if( pRoot ) {

			if( pRoot->IsStruct() ) {

				CABL5kItemList const *pTags = ((CABL5kStruct *) pRoot)->m_pMembers;
				
				return FindTag(pTags, Name);
				}

			// TODO -- array of atomic type

			return pRoot;
			}
		
		return NULL;
		}

	return FindTag(m_pTags, Name);
	}

CABL5kItem * CABL5kNames::FindTag(CABL5kItemList const *pTags, CString const &Name)
{
	// TODO -- dictionary ?

	for( UINT n = 0; n < pTags->GetItemCount(); n++ ) {

		CABL5kItem *pTag = pTags->GetItem(n);
		
		if( pTag->m_Name == Name ) {
			
			return pTag;
			}
		}

	return NULL;
	}

// Type Expansion

BOOL CABL5kNames::ExpandType(CABL5kItemList *pTags, CString const &Name, CString const &Type)
{
	return  ExpandPDT(pTags, Name, Type) || 
		ExpandUDT(pTags, Name, Type) ;;
	}

BOOL CABL5kNames::ExpandPDT(CABL5kItemList *pTags, CString const &Name, CString const &Type)
{
	return m_pConfig->m_Types.ExpandPDT( this,
					     pTags, 
					     Name, 
					     Type
					     );
	}

BOOL CABL5kNames::ExpandUDT(CABL5kItemList *pTags, CString const &Name, CString const &Type)
{
	return m_pConfig->m_Types.ExpandUDT( this,
					     pTags, 
					     Name, 
					     Type
					     );
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Tag Names List
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kNamesList, CItemList);

// Constructor

CABL5kNamesList::CABL5kNamesList(void)
{
	m_Class = AfxRuntimeClass(CABL5kNames);
	}

// Item Location

CABL5kNames * CABL5kNamesList::GetItem(UINT uPos) const
{
	return (CABL5kNames *) CItemList::GetItem(uPos);
	}

// End of File
