
#include "intern.hpp"

#include "sma.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader(); 

//////////////////////////////////////////////////////////////////////////
//
// SMA Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CSmaDriverOptions, CUIItem);

// Constructor

CSmaDriverOptions::CSmaDriverOptions(void)
{
	m_Src = 0;
	}

// Download Support

BOOL CSmaDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Src));
	
	return TRUE;
	}

// Meta Data Creation

void CSmaDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Src);
	}


//////////////////////////////////////////////////////////////////////////
//
// SMA Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSmaDeviceOptions, CUIItem);

// Constructor

CSmaDeviceOptions::CSmaDeviceOptions(void)
{
	m_Mode  = 0;

	m_Addr  = 1;

	m_Addr2 = 0;

	m_Poll  = 1000;

	m_List.Empty();

	m_Index = 0;

	m_MemStore  = 0;

	m_MemStore2 = 0;
	}

// UI Managament

void CSmaDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Param" ) {

			switch( m_Param ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					OnImport(pWnd);
					break;

				case 2:
					OnExport(pWnd);
					break;

				}

			return;
			}
		}	
	}

// Persistance

void CSmaDeviceOptions::Init(void)
{
	CUIItem::Init();
	}

void CSmaDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "List" ) {

			Tree.GetObject();

			LoadParams(Tree);

			Tree.EndObject();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CSmaDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);	

	if( m_List.GetCount() ) {

		Tree.PutObject(TEXT("List"));

		SaveParams(Tree);

		Tree.EndObject();
		}
	}

// Address List Support

CString CSmaDeviceOptions::GetDeviceName(void)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pData = pItem->FindMetaData(TEXT("Name"));

	return pData ? pData->ReadString(pItem) : TEXT("Device");
	}

CString CSmaDeviceOptions::GetName(CAddress Addr)
{
	CParamArray List;
	
	ListParams(List);

	UINT n;

	for( n = 0; n < List.GetCount(); n ++ ) {

		if( Addr.m_Ref == List.GetAt(n).m_Addr) {

			return List.GetAt(n).m_Name;
			}
		}

	return CString("");
	}

DWORD CSmaDeviceOptions::GetAddress(CString Name)
{
	CParamArray List;
	
	ListParams(List);

	UINT n;

	for( n = 0; n < List.GetCount(); n ++ ) {

		if( Name == List.GetAt(n).m_Name ) {

			return List.GetAt(n).m_Addr;
			}
		}

	return 0;
	}

BOOL CSmaDeviceOptions::IsWritable(CAddress Addr)
{
	CParamArray List;
	
	ListParams(List);

	UINT n;

	for( n = 0; n < List.GetCount(); n ++ ) {

		if( Addr.m_Ref == List.GetAt(n).m_Addr) {

			return List.GetAt(n).m_fWrite;
			}
		}

	return FALSE;
	}

BOOL CSmaDeviceOptions::CreateParam(CError &Error, CString const &Name, CAddress Addr, BOOL fWrite, IMakeTags *pTags)
{
	CString Tag = Name;

	BOOL fParse = ParseAddress(Error, Addr, Tag);

	if( !pTags && fParse ) {

		Error.Set(CString("This name is already in use."), 0);

		return FALSE;
		}

	if( Tag.Find(' ') < NOTHING ) {

		Tag.Replace(' ', '_');
		}
	
	UINT uOffset = CreateIndex();

	if( Addr.a.m_Offset == 0 ) {

		Addr.m_Ref      = 0;

		Addr.a.m_Table  = 1;

		Addr.a.m_Type   = addrLongAsLong;
	
		Addr.a.m_Offset = uOffset;
		}
		
	CParam Param;

	Param.m_Name = Tag;

	Param.m_Addr = Addr.m_Ref;

	Param.m_fWrite = fWrite;

	if( !fParse ) {

		m_List.Append(Param);
		}

	if( pTags ) {

		CString Value = L'[' + Tag + L']';

		Tag.Replace('-', '_');

		pTags->AddInt( Tag,
			       GetDeviceName() + Tag,
			       Value,
			       0,
			       fWrite,
			       L"",
			       L"",
			       0,
			       0
			       );
		}

	return TRUE;
	}

void CSmaDeviceOptions::DeleteParam(CAddress const &Addr, CString const &Name)
{
	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		if( m_List.GetAt(n).m_Addr == Addr.m_Ref ) {

			if( m_List.GetAt(n).m_Name == Name ) {

				m_List.Remove(n);

				return;
				}
			}
		}
	}

BOOL CSmaDeviceOptions::RenameParam(CString const &Name, CAddress const &Addr)
{
	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		if( m_List.GetAt(n).m_Addr == Addr.m_Ref ) {

			CParam Param;

			Param.m_Addr   = m_List.GetAt(n).m_Addr;

			Param.m_fWrite = m_List.GetAt(n).m_fWrite;

			Param.m_Name   = Name;			

			DeleteParam(Addr, Name);

			m_List.Insert(n, Param);	

			return TRUE;
			}
		}
	
	return FALSE;
	}

void CSmaDeviceOptions::SetParamWrite(CString const Name, BOOL fWrite)
{
	CAddress Addr;

	Addr.m_Ref = GetAddress(Name);

	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		if( m_List.GetAt(n).m_Addr == Addr.m_Ref ) {

			CParam Param;

			Param.m_Addr   = Addr.m_Ref;

			Param.m_fWrite = fWrite;

			Param.m_Name   = Name;			

			DeleteParam(Addr, Name);

			m_List.Insert(n, Param);	
			}
		}
	}

void CSmaDeviceOptions::ListParams(CParamArray &List)
{
	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		List.Append(m_List.GetAt(n));
		}
	}

BOOL CSmaDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CString Test = Text;

	if( Test.Find(' ') < NOTHING ) {

		Test.Replace(' ', '_');
		}

	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		if( m_List.GetAt(n).m_Name == Test ) {

			Addr.m_Ref = m_List.GetAt(n).m_Addr;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CSmaDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		if( m_List.GetAt(n).m_Addr == Addr.m_Ref ) {

			Text = m_List.GetAt(n).m_Name;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CSmaDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CSmaDriver Driver;
	
	CSmaAddrDialog Dlg(Driver, Addr, this, fPart, TRUE);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Download Support

BOOL CSmaDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));

	Init.AddByte(BYTE(m_Addr));

	Init.AddByte(BYTE(m_Addr2));

	Init.AddWord(WORD(m_Poll));

	if( m_List.IsEmpty() ) {

		Init.AddWord(0);

		return TRUE;
		}

	UINT uCount = m_List.GetCount();

	Init.AddWord(WORD(uCount));

	for( UINT u = 0; u < uCount; u++ ) {

		Init.AddText(m_List.GetAt(u).m_Name);

		Init.AddLong(m_List.GetAt(u).m_Addr);
		}

	Init.AddByte(BYTE(m_MemStore));

	Init.AddByte(BYTE(m_MemStore2));

	return TRUE;
	}

// Meta Data Creation

void CSmaDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Mode);

	Meta_AddInteger(Addr);

	Meta_AddInteger(Addr2);

	Meta_AddInteger(Poll);

	Meta_AddInteger(Param);

	Meta_AddInteger(MemStore);

	Meta_AddInteger(MemStore2);
	}

// Implementation

UINT CSmaDeviceOptions::CreateIndex(void)
{
	return m_Index++;
	}

void CSmaDeviceOptions::SaveParams(CTreeFile &Tree)
{
	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {
		
		Tree.PutValue(TEXT("Name"), m_List[n].m_Name);

		Tree.PutValue(TEXT("Data"), m_List[n].m_Addr);

		Tree.PutValue(TEXT("Write"), m_List[n].m_fWrite ? 1 : 0);
		}
	}

void CSmaDeviceOptions::LoadParams(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Name" ) {
			
			CString Name = Tree.GetValueAsString();

			Tree.GetName();

			CAddress Addr;
			
			Addr.m_Ref = Tree.GetValueAsInteger();

			Tree.GetName();

			BOOL fWrite = (BOOL)Tree.GetValueAsInteger();

			CreateParam( CError(FALSE), Name, Addr, fWrite);

			continue;
			}
		}
	}

void CSmaDeviceOptions::OnManage(CWnd *pWnd)
{
	CAddress Addr;

	Addr.m_Ref = 0;

	CSmaDriver Driver;
	
	CSmaAddrDialog Dlg(Driver, Addr, this, FALSE, FALSE);

	Dlg.Execute(*pWnd);
	}

void CSmaDeviceOptions::OnImport(CWnd *pWnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("SMA Parameters"));

	Dlg.SetCaption(CPrintf("Import Parameters for %s", GetDeviceName()));

	Dlg.SetFilter(TEXT("Sunny Online Data (*.suo)|*.suo|CSV Files (*.csv)|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

			CString Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
			}
		else {
			if( m_List.GetCount() ) {
				
				CString Text =  TEXT("You are about to make changes to the SMA device configuration \n")
						TEXT("that may invalidate data tag references in the database.\n\n")
						TEXT("If you choose to continue all parameters will be replaced with \n")
						TEXT("data from the selected file and you should run the Rebuild Comms \n")
						TEXT("Blocks utility from the File menu after this operation is complete.\n\n")
						TEXT("Do you want to continue?");

				if( pWnd->YesNo(Text) == IDNO ) {

					fclose(pFile);
					
					return;
					}

				m_List.Empty();
				}

			BOOL fSUO = Dlg.GetFilename().GetType() ==  "suo";

			if( Import(pFile, fSUO) ) {

				SetDirty();
				}

			Dlg.SaveLastPath(TEXT("SMA Parameters"));
			
			fclose(pFile);
			}
		}
	}

void CSmaDeviceOptions::OnExport(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("SMA Parameters"));

	Dlg.SetCaption(CPrintf("Export Parameters for %s", GetDeviceName()));

	Dlg.SetFilter (TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
			fclose(pFile);

			CString Text = TEXT("The selected file already exists.\n\n")
				       TEXT("Do you want to overwrite it?");

			if( pWnd->YesNo(Text) == IDNO ) {

				return;
				}
			}

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			CString Text = TEXT("Unable to open file for writing.");

			pWnd->Error(Text);

			return;
			}

		Export(pFile);

		fclose(pFile);

		Dlg.SaveLastPath(TEXT("SMA Parameters"));
		}
	}

BOOL CSmaDeviceOptions::Import(FILE *pFile, BOOL fSemi, IMakeTags *pTags)
{
	DWORD dwPos = ftell(pFile);

	UINT uLine = 0;

	fseek(pFile, dwPos, SEEK_SET);

	UINT uTarget = fSemi ? 3 : 6;

	while( !feof(pFile) ) {

		char sLine[5120] = {0};

		fgets(sLine, sizeof(sLine), pFile);

		uLine++;

		if( uLine == uTarget ) {

			CStringArray List;

			sLine[strlen(sLine)-1] = 0;

			char Token = fSemi ? ';' : ',';

			CString(sLine).Tokenize(List, Token);

			CAddress Addr;

			UINT uCount = List.GetCount();

			for( UINT u = 0; u < uCount; u++ ) {

				if( u == 0 && List.GetAt(u) == "TimeStamp" ) {

					continue;
					}

				Addr.m_Ref = 0;

				CreateParam(CError(FALSE), List.GetAt(u), Addr, FALSE, pTags);
				}

			break;
			}
		}
	
	return TRUE;
	}


void CSmaDeviceOptions::Export(FILE *pFile)
{
	CParamArray List;

	ListParams(List);

	fprintf(pFile, TEXT("\n\n\n\n\n"));

	for( UINT n = 0; n < List.GetCount(); n ++ ) {
		
		CString  Name = List[n].m_Name;

		fprintf(pFile, TEXT("%s,"), PCTXT(Name));
		}

	fprintf(pFile, TEXT("\n"));
	}

//////////////////////////////////////////////////////////////////////////
//
// SMA Driver
//

// Instantiator

ICommsDriver *	Create_SmaDriver(void)
{
	return New CSmaDriver;
	}


// Constructor

CSmaDriver::CSmaDriver(void)
{
	m_wID		= 0x4097;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SMA";
	
	m_DriverName	= "Data Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "SMA DATA";

	m_DevRoot	= "DEV";

	AddSpaces();
	}

// Binding Control

UINT CSmaDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSmaDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CSmaDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CSmaDriverOptions);
	}


CLASS CSmaDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSmaDeviceOptions);
	}

// Driver Data

UINT CSmaDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagImport;
	}

// Tag Import

BOOL CSmaDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	CSmaDeviceOptions * pOpt = (CSmaDeviceOptions *)pConfig;

	if( pOpt ) {

		COpenFileDialog Dlg;

		Dlg.LoadLastPath(TEXT("SMA Parameters"));

		Dlg.SetCaption(CPrintf("Import Parameters for %s", pOpt->GetDeviceName()));

		Dlg.SetFilter(TEXT("Sunny Online Data (*.suo)|*.suo|CSV Files (*.csv)|*.csv"));

		if( Dlg.Execute(*afxMainWnd) ) {

			FILE *pFile;
		
			if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

				CString Text = TEXT("Unable to open file for reading.");

				afxMainWnd->Error(Text);
				}
			else {
				CString File = Dlg.GetFilename();

				if( pTags->InitImport(pOpt->GetDeviceName(), FALSE, File) ) {

					BOOL fSUO = Dlg.GetFilename().GetType() ==  "suo";

					if( pOpt->Import(pFile, fSUO, pTags) ) {

						pOpt->SetDirty();
						}

					pTags->ImportDone(TRUE);

					Dlg.SaveLastPath(TEXT("SMA Parameters"));
			
					fclose(pFile);

					return TRUE;
					}

				fclose(pFile);
				}
			}
		}

	return FALSE;
	}


// Address Management

BOOL CSmaDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{	
	CSmaDeviceOptions * pOpt = (CSmaDeviceOptions *)pConfig;

	if( pOpt ) {

		return pOpt->SelectAddress(hWnd, Addr, fPart);
		}

	return FALSE;
	}

BOOL CSmaDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	CSmaDeviceOptions * pOpt = (CSmaDeviceOptions *)pConfig;

	if( pOpt ) {

		return !pOpt->IsWritable(Addr);
		}

	return FALSE;
	}

// Address Helpers

BOOL CSmaDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) pConfig;

	if( pDev ) {
		
		Addr.m_Ref = pDev->GetAddress(Text);
				
		return TRUE;
		}

	return FALSE;
	}

BOOL CSmaDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) pConfig;

	if( pDev ) {
		
		CString Name = pDev->GetName(Addr);

		if( !Name.IsEmpty() ) {

			Text = Name;

			return TRUE;
			}
		}

	return FALSE;
	}


// Implementation

void CSmaDriver::AddSpaces(void)     
{
	AddSpace(New CSpace(1, "ALL", "All Channels",	   10, 0, 65535, addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Sma Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSmaTCPDeviceOptions, CSmaDeviceOptions);       

// Constructor

CSmaTCPDeviceOptions::CSmaTCPDeviceOptions(void)
{
	m_IP   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Port = 49999;

	m_Group = 1;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CSmaTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CSmaTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Group));
	Init.AddWord(WORD(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CSmaTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// SMA TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_SmaTCPDriver(void)
{
	return New CSmaTCPDriver;
	}

// Constructor

CSmaTCPDriver::CSmaTCPDriver(void)
{
	m_wID		= 0x4098;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SMA";
	
	m_DriverName	= "NET TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "SMA NET";
	}

// Destructor

CSmaTCPDriver::~CSmaTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CSmaTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CSmaTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CSmaTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSmaTCPDeviceOptions);
	}


//////////////////////////////////////////////////////////////////////////
//
// SMA Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSmaAddrDialog, CStdDialog);
		
// Constructor

CSmaAddrDialog::CSmaAddrDialog(CSmaDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart, BOOL fSelect)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;

	m_fSelect = fSelect;
		
	SetName(TEXT("SmaDlg"));
	}

// Message Map

AfxMessageMap(CSmaAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED,	OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown   )
	
	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(1002, OnCreate)
	AfxDispatchCommand(1003, OnWrite)

	AfxMessageEnd(CSmaAddrDialog)
	};

// Message Handlers

BOOL CSmaAddrDialog::OnInitDialog(CWnd &Wnd, DWORD dwData)
{
	SetWindowText(m_fSelect ? CString("Select Parameter") : CString("Manage Parameters"));

	GetDlgItem(IDCANCEL).EnableWindow(m_fSelect);

	GetDlgItem(1002).EnableWindow(!m_fSelect);

	GetDlgItem(1003).EnableWindow(!m_fSelect);

	GetDlgItem(IDOK).SetWindowText(m_fSelect ? CString("OK") : CString("Close"));

	GetDlgItem(IDHELP).EnableWindow(FALSE);

	LoadParameters();

	return FALSE;
	}

// Notification Handlers

void CSmaAddrDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( uID == 1001 ) {

		m_hSelect  = Info.itemNew.hItem;

		m_dwSelect = Info.itemNew.lParam;

		CAddress Addr = *m_pAddr;
	
		Addr.m_Ref = m_dwSelect;

		CButton &Button = (CButton &) GetDlgItem(1003);

		CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;

		if( pDev ) {

			Button.SetCheck(pDev->IsWritable(Addr));
			}
		}
	}

BOOL CSmaAddrDialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	if( Info.wVKey == VK_DELETE ) {
		
		if( (GetKeyState(VK_SHIFT) & 0x8000) ) {

			return DeleteParameter();
			}
		}

	if( Info.wVKey == VK_F2 ) {		
		
		return RenameParameter();
		}

	return FALSE;
	}

// Command Handlers

BOOL CSmaAddrDialog::OnCreate(UINT uID)
{
	CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;

	CString Name = TEXT("<Name>");
	
	CStringDialog Dlg(CString("Create Parameter"), CString("Parameter Name"), Name);

	if( !(GetKeyState(VK_SHIFT) & 0x8000) ) {
		
		if( Dlg.Execute(ThisObject) ) {

			Name = Dlg.GetData();
			}
		else
			return TRUE;
		}

	CAddress   Addr;

	Addr.m_Ref = 0;

	CError Error(TRUE);

	if( pDev->CreateParam(Error, Name, Addr, FALSE) ) {

		CError Error(FALSE);

		if( pDev->ParseAddress(Error, Addr, Name) ) {
			
			CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);
			
			LoadParameter(Tree, Name, Addr, FALSE);
			
			Tree.Expand(m_hRoot, TVE_EXPAND);
			
			Tree.SetFocus();
			
			pDev->SetDirty();
			}
		else
			AfxAssert(FALSE);
		}
	else
		Error.Show(ThisObject);

	return TRUE;
	}

BOOL CSmaAddrDialog::OnWrite(UINT uID)
{
	CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;

	if( pDev ) {

		CButton &Button = (CButton &) GetDlgItem(1003);

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		pDev->SetParamWrite(Tree.GetItemText(m_hSelect), Button.IsChecked());
		}

	return TRUE;
	}

BOOL CSmaAddrDialog::OnOkay(UINT uID)
{
	CError Error(TRUE);

	CAddress Addr;

	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Tree.GetItemText(m_hSelect) ) ) {

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CSmaAddrDialog::LoadParameters(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_NOHSCROLL
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_EDITLABELS
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

	Tree.SendMessage(TVM_SETUNICODEFORMAT, 1);

	LoadRoot(Tree);

	LoadParameters(Tree);

	Tree.SetFocus();

	SetSelection(*m_pAddr);
	}

void CSmaAddrDialog::LoadParameters(CTreeView &Tree)
{
	CParamArray List;

	CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;
	
	pDev->ListParams(List);

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CParam const &Data = List[n];

		LoadParameter(Tree, Data.m_Name, (CAddress &) Data.m_Addr, Data.m_fWrite);
		}

	Tree.Expand(m_hRoot, TVE_EXPAND);

	m_hSelect = m_hRoot;
	
	Tree.SelectItem(m_hSelect);
	}

void CSmaAddrDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;

	Node.SetText(pDev->GetDeviceName());

	Node.SetParam(FALSE);

	//Node.SetImages(3);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CSmaAddrDialog::LoadParameter(CTreeView &Tree, CString Name, CAddress Addr, BOOL fWrite)
{
	CTreeViewItem Node;

	Node.SetText(Name);

	Node.SetParam(Addr.m_Ref);

	//Node.SetImages(0x34);

	HTREEITEM hNode = Tree.InsertItem(m_hRoot, NULL, Node);

	Tree.SelectItem(hNode, TVGN_CARET);

	CButton &Button = (CButton &) GetDlgItem(1003);

	Button.SetCheck(fWrite);
	}

BOOL CSmaAddrDialog::SetSelection(CString Name)
{
	CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);

	HTREEITEM hNode = Tree.GetChild(m_hSelect);

	while( Tree.GetItemText(hNode) != Name ) {
						
		hNode = Tree.GetNext(hNode);
		} 

	m_hSelect = hNode;

	Tree.SelectItem(hNode);

	return TRUE;
	}

BOOL CSmaAddrDialog::SetSelection(CAddress Addr)
{
	CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;

	if( m_fSelect && pDev && Addr.m_Ref ) {

		CString Name = pDev->GetName(Addr);

		return SetSelection(Name);
		}

	return FALSE;
	}

BOOL CSmaAddrDialog::DeleteParameter(void)
{
	if( m_dwSelect ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		CString Name = Tree.GetItemText(m_hSelect);

		CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;

		if( pDev ) {

			CAddress Addr;

			Addr.m_Ref = pDev->GetAddress(Name);

			if( pDev->ExpandAddress(Name, Addr) )  {

				CString Text  = CPrintf("Do you really want to delete %s ?", Name);
				
				if( NoYes(Text) == IDYES ) {

					Tree.SetRedraw(FALSE);

					HTREEITEM hPrev = m_hSelect;

					Tree.MoveSelection();

					pDev->DeleteParam(Addr, Name);

					Tree.DeleteItem(hPrev);

					Tree.SetRedraw(TRUE);

					pDev->SetDirty();

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CSmaAddrDialog::RenameParameter(void)
{
	if( m_dwSelect ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		CString Name = Tree.GetItemText(m_hSelect);

		CSmaDeviceOptions * pDev = (CSmaDeviceOptions *) m_pConfig;

		if( pDev ) {

			CAddress Addr;

			Addr.m_Ref = pDev->GetAddress(Name);

			if( pDev->ExpandAddress(Name, Addr) ) {

				CString Title   = "Rename Parameter";
			
				CString Group   = "Parameter";
			
				CStringDialog Dlg(Title, Group, Name);

				if( Dlg.Execute(ThisObject) ) {

					CString Param = Dlg.GetData();			

					if(pDev->RenameParam(Param, Addr) ) {

						Tree.SetRedraw(FALSE);

						Tree.DeleteChildren(m_hRoot);

						LoadParameters(Tree);

						Tree.SetRedraw(TRUE);

						SetSelection(Param);

						pDev->SetDirty();
					
						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

// End of File
