
#include "intern.hpp"

#include "rawact.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CRawTCPActiveDriverOptions, CUIItem);

// Constructor

CRawTCPActiveDriverOptions::CRawTCPActiveDriverOptions(void)
{
	m_pService = NULL;

	m_IP       = DWORD(MAKELONG(MAKEWORD( 4, 200), MAKEWORD(  9, 192)));;

	m_Port     = 1234;

	m_Timeout  = 10000;

	m_Keep     = 0;
	}

// Download Support

BOOL CRawTCPActiveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pService);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddWord(WORD(m_Timeout));
	Init.AddByte(BYTE(m_Keep));

	return TRUE;
	}

// Meta Data Creation

void CRawTCPActiveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Service);
	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(Keep);

	Meta_SetName(IDS_DRIVER_OPTIONS);
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Port
//

// Instantiator

ICommsDriver * Create_RawTCPActiveDriver(void)
{
	return New CRawTCPActiveDriver;
	}

// Constructor

CRawTCPActiveDriver::CRawTCPActiveDriver(void)
{
	m_wID		= 0x3704;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Raw TCP/IP Active";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Raw TCP/IP Active";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Configuration

CLASS CRawTCPActiveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CRawTCPActiveDriverOptions);
	}

// Binding Control

UINT CRawTCPActiveDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
