
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ENMBTCPM_HPP
	
#define	INCLUDE_ENMBTCPM_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Encapsulated Modbus TCP/IP Master Driver Options
//

class CEnModbusMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEnModbusMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		BOOL m_fDisable16;
		BOOL m_fDisable15;
		BOOL m_fDisable5;
		BOOL m_fDisable6;
		BOOL m_fMode;
		UINT m_PingReg;
		UINT m_Max01;
		UINT m_Max02;
		UINT m_Max03;
		UINT m_Max04;
		UINT m_Max15;
		UINT m_Max16;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Encapsulated Modbus TCP/IP Master Driver
//

class CEnModbusMasterTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEnModbusMasterTCPDriver(void);

		//Destructor
		~CEnModbusMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helpers
		BOOL CheckAlignment(CSpace *pSpace);
						
	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
