
#include "intern.hpp"

#include "rlc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CRlcDeviceOptions, CUIItem);

// Constructor

CRlcDeviceOptions::CRlcDeviceOptions(void)
{
	m_Drop		= 0;

	m_fProtocol	= 0;

	m_StdDlyRead	= 0;

	m_StdDlyWrite	= 150;

	m_IdxDlyRead	= 0;

	m_IdxDlyWrite	= 100;

	m_CmdDly	= 500;

	m_fRspTime	= 0;

	m_Device	= 0;

	m_Cub5		= 0;
	}

// UI Managament

void CRlcDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Device" ) {

		switch( m_Device ) {

			case DEVICE_LEGEND:
				pWnd->EnableUI("Protocol", TRUE);
				pWnd->EnableUI("StdDlyRead", TRUE);
				pWnd->EnableUI("StdDlyWrite", TRUE);
				pWnd->EnableUI("IdxDlyRead", FALSE);
				pWnd->EnableUI("IdxDlyWrite", FALSE);
				pWnd->EnableUI("CmdDly", FALSE);
				pWnd->EnableUI("RspTime", FALSE);
				pWnd->EnableUI("Cub5",FALSE);
				m_fRspTime = 0;
				break;

			case DEVICE_PAX:			
			case DEVICE_PAXI:
				pWnd->EnableUI("Protocol", TRUE);
				pWnd->EnableUI("StdDlyRead", TRUE);
				pWnd->EnableUI("StdDlyWrite", TRUE);
				pWnd->EnableUI("IdxDlyRead", FALSE);
				pWnd->EnableUI("IdxDlyWrite", FALSE);
				pWnd->EnableUI("CmdDly", FALSE);
				pWnd->EnableUI("RspTime", TRUE);
				pWnd->EnableUI("Cub5",FALSE);
				break;

			case DEVICE_TP48:
				pWnd->EnableUI("Protocol", TRUE);
				pWnd->EnableUI("StdDlyRead", TRUE);
				pWnd->EnableUI("StdDlyWrite", TRUE);
				pWnd->EnableUI("IdxDlyRead", FALSE);
				pWnd->EnableUI("IdxDlyWrite", FALSE);
				pWnd->EnableUI("CmdDly", TRUE);
				pWnd->EnableUI("RspTime", TRUE);
				pWnd->EnableUI("Cub5",FALSE);
			       	break;
		       			
			case DEVICE_TPCU:
				pWnd->EnableUI("Protocol", TRUE);
			       	pWnd->EnableUI("StdDlyRead", TRUE);
				pWnd->EnableUI("StdDlyWrite", TRUE);
				pWnd->EnableUI("IdxDlyRead", FALSE);
				pWnd->EnableUI("IdxDlyWrite", FALSE);
				pWnd->EnableUI("CmdDly", TRUE);
				pWnd->EnableUI("RspTime", FALSE);
				pWnd->EnableUI("Cub5",FALSE);
				m_fRspTime = 0;
				break;

			case DEVICE_CUB5:
				pWnd->EnableUI("Protocol", TRUE);
				pWnd->EnableUI("StdDlyRead", TRUE);
				pWnd->EnableUI("StdDlyWrite", TRUE);
				pWnd->EnableUI("IdxDlyRead", FALSE);
				pWnd->EnableUI("IdxDlyWrite", FALSE);
				pWnd->EnableUI("CmdDly", FALSE);
				pWnd->EnableUI("RspTime", TRUE);
				pWnd->EnableUI("Cub5",TRUE);
				break;

			default:
				pWnd->EnableUI("Protocol", TRUE);
				pWnd->EnableUI("StdDlyRead", TRUE);
				pWnd->EnableUI("StdDlyWrite", TRUE);
				pWnd->EnableUI("IdxDlyRead", FALSE);
				pWnd->EnableUI("IdxDlyWrite", FALSE);
				pWnd->EnableUI("CmdDly", FALSE);
				pWnd->EnableUI("RspTime", FALSE);
				pWnd->EnableUI("Cub5",FALSE);
				m_fRspTime = 0;
				break;
			}

		pWnd->UpdateUI();	
		}
	}


// Download Support	     

BOOL CRlcDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_fProtocol));
	Init.AddWord(WORD(m_StdDlyRead));
	Init.AddWord(WORD(m_StdDlyWrite));
	Init.AddWord(WORD(m_IdxDlyRead));
	Init.AddWord(WORD(m_IdxDlyWrite));
	Init.AddWord(WORD(m_CmdDly));
	Init.AddByte(BYTE(m_fRspTime));
	Init.AddByte(BYTE(m_Device));
	Init.AddByte(BYTE(m_Cub5));
		
	return TRUE;
	}

// Meta Data Creation


void CRlcDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddBoolean(Protocol);

	Meta_AddInteger(StdDlyRead);

	Meta_AddInteger(StdDlyWrite);

	Meta_AddInteger(IdxDlyRead);

	Meta_AddInteger(IdxDlyWrite);

	Meta_AddInteger(CmdDly);

	Meta_AddBoolean(RspTime);

	Meta_AddInteger(Device);

	Meta_AddInteger(Cub5);
	}

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CRlcAddrDialog, CStdDialog);
		
// Constructor

CRlcAddrDialog::CRlcAddrDialog(CBasicCommsDriver &Driver, CAddress &Addr, BOOL fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	SetName(fPart ? TEXT("PartRlcAddrDlg") : TEXT("FullRlcAddrDlg"));
	}

// Message Map

AfxMessageMap(CRlcAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,    OnOkay )
	AfxDispatchCommand(IDCLEAR, OnClear)

	AfxDispatchNotify(1002, LBN_DBLCLK, OnDblClk)

	AfxMessageEnd(CRlcAddrDialog)
	};

// Message Handlers

BOOL CRlcAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	ShowAddress(*m_pAddr);

	ShowInformation();
	
	return FALSE;
	}

// Notification Handlers

void CRlcAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

// Command Handlers

BOOL CRlcAddrDialog::OnOkay(UINT uID)
{
	CError   Error;

	CString  Text;

	CAddress Addr;

	Text = GetDlgItem(1002).GetWindowText();
	
	if( m_pDriver->ParseAddress(Error, Addr, NULL, Text) ) {

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	Error.Show(ThisObject);

	SetDlgFocus(1002);

	return FALSE;
	}

BOOL CRlcAddrDialog::OnClear(UINT uID)
{
	GetDlgItem(1002).SetWindowText("");

	return TRUE;
	}

// Implementation

void CRlcAddrDialog::ShowAddress(CAddress &Addr)
{
	CString Text = "";

	m_pDriver->ExpandAddress(Text, NULL, Addr);
	
	GetDlgItem(1002).SetWindowText(Text);
	}

void CRlcAddrDialog::ShowInformation(void)
{
	if( !m_fPart ) {

		CString Text = IDS_RLC_FORMAT;
				
		GetDlgItem(2001).SetWindowText(Text);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument
//

// Instantiator

ICommsDriver *	Create_RlcDriver(void)
{
	return New CRlcDriver;
	}

// Constructor

CRlcDriver::CRlcDriver(void)
{
	m_wID		= 0x330F;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Red Lion";
	
	m_DriverName	= "RLC Instrument";
	
	m_Version	= "1.01";
	
	m_ShortName	= "RLC Meter";

	m_DevRoot	= "DEV";

	C3_PASSED();
	}

// Binding Control

UINT CRlcDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CRlcDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CRlcDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CRlcDeviceOptions);
	}

// Address Management

BOOL CRlcDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	UINT uLen = Text.GetLength();

	Text.MakeUpper();

	if( uLen == 1 && isalpha(Text[0]) ) {

		UINT uIndex = Text[0];

		Addr.a.m_Offset = uIndex;
		Addr.a.m_Table  = addrNamed;
		Addr.a.m_Extra	= 1;
		Addr.a.m_Type   = addrLongAsLong;

		return TRUE;
		}

	if( uLen == 2 && isalpha(Text[0]) && isalnum(Text[1]) ) {

		UINT uIndex = MAKEWORD(Text[0], Text[1]);

		Addr.a.m_Offset = uIndex;
		Addr.a.m_Table  = addrNamed;
		Addr.a.m_Extra	= 1;
		Addr.a.m_Type   = addrLongAsLong;

		return TRUE;
		}

/*	if( uLen == 3 && Text[0) == 'I' ) {

		PTXT pError    = NULL;

		CString Offset = Text.Mid(1);

		UINT uIndex    = strtoul(Offset, &pError, 10);

		if( !(pError && pError[0]) ) {

			Addr.a.m_Offset = uIndex;
			Addr.a.m_Table  = addrNamed;
			Addr.a.m_Extra	= 2;
			Addr.a.m_Type   = addrLongAsLong;

			return TRUE;
			}
		}

*/	if( uLen == 3 && Text[1] == '-' && Text[2] == 'B') {   

		UINT uIndex = Text[0];

		Addr.a.m_Offset = uIndex;
		Addr.a.m_Table  = addrNamed;
		Addr.a.m_Extra  = 3;
		Addr.a.m_Type  	= addrLongAsLong;

		return TRUE;
		}
		
	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CRlcDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{	
	if( Addr.a.m_Extra == 1 ) {

		char sText[4];
		
		sText[0] = LOBYTE(Addr.a.m_Offset);
		sText[1] = HIBYTE(Addr.a.m_Offset);
		sText[2] = 0;

		Text = sText;

		return TRUE;
		}

	if( Addr.a.m_Extra == 2 ) {

		UINT uIndex = Addr.a.m_Offset;

		Text.Printf("I%2.2u", uIndex);

		return TRUE;
		}
	
	if( Addr.a.m_Extra == 3 ) {

		Text.Printf("%c-B", LOBYTE(Addr.a.m_Offset));

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CRlcDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CRlcAddrDialog Dlg(*this, Addr, FALSE);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CRlcDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	if( !pRoot && !uItem ) {

		Data.m_Addr.a.m_Offset = 0;
		Data.m_Addr.a.m_Table  = addrNamed;
		Data.m_Addr.a.m_Extra  = 0;
		Data.m_Addr.a.m_Type   = addrLongAsLong;

		Data.m_fPart = TRUE;
		Data.m_fRead = FALSE;
		Data.m_Name  = "Registers";
		Data.m_uData = 0;

		return TRUE;
		}

	return FALSE;
	}

// End of File
