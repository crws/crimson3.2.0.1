
#include "intern.hpp"

#include "animatic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Changesets 1095 and 1103 included

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Animatics Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CAnimaticsDeviceOptions, CUIItem);

// Constructor

CAnimaticsDeviceOptions::CAnimaticsDeviceOptions(void)
{
	m_Drop = 0;

	m_SMClass = 1;
	}

// Download Support

BOOL CAnimaticsDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte((BYTE)m_Drop);
	Init.AddByte((BYTE)m_SMClass);

	return TRUE;
	}

// Meta Data Creation

void CAnimaticsDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(SMClass);
	}

//////////////////////////////////////////////////////////////////////////
//
// Animatics Comms Driver
//

// Instantiator

ICommsDriver *	Create_AnimaticsDriver(void)
{
	return New CAnimaticsDriver;
	}

// Constructor

CAnimaticsDriver::CAnimaticsDriver(void)
{
	m_wID		= 0x3355;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Animatics";
	
	m_DriverName	= "Smart Motor";
	
	m_Version	= "2.00";
	
	m_ShortName	= "Animatics Smart Motor";

	AddSpaces();
	}

// Binding Control

UINT	CAnimaticsDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CAnimaticsDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Device Config
CLASS	CAnimaticsDriver::GetDeviceConfig()
{
	return AfxRuntimeClass(CAnimaticsDeviceOptions);
	}

// Implementation

void	CAnimaticsDriver::AddSpaces(void)
{
	AddSpace( New CSpace("A",		"Acceleration (C4 = A, C5 = AC-R)",		CMA ) );
	AddSpace( New CSpace(CM5AB,	"AB",	"8 Bit Array (C5)",				 10, 0,  200, addrByteAsByte ) );
	AddSpace( New CSpace(CM5AF,	"AF",	"Real Array (C5)",				 10, 0,    7, addrRealAsReal ) );
	AddSpace( New CSpace(CM5AL,	"AL",	"32 Bit Array (C5)",				 10, 0,   50, addrLongAsLong ) );
	AddSpace( New CSpace("AMPS",		"User Maximum Current",				CMAMPS ) );
 	AddSpace( New CSpace(		"AT",	"Target Acceleration (C5)",			CM5AT ) );
	AddSpace( New CSpace(CM5AW,	"AW",	"16 Bit Array (C5)",				 10, 0,  100, addrWordAsWord ) );

	AddSpace( New CSpace("Be",		"Error Exceeded Bit (R)",			CMBe ) );
	AddSpace( New CSpace("Bt",		"Move in Progress Status Bit (R)",		CMBt ) );
	AddSpace( New CSpace("Bo",		"Motor Off Bit (R)",				CMBo ) );
	AddSpace( New CSpace("Bw",		"Position Wrap-Around Occurred (R)",		CMBw ) );
	AddSpace( New CSpace("Brkeng",		"Engage Brake (W)",				CMBRKENG ) );
	AddSpace( New CSpace("Brkrel",		"Release Brake (W)",				CMBRKRLS ) );
	AddSpace( New CSpace("Brksrv",		"Brake Engage/Release (W)",			CMBRKSRV ) );

	AddSpace( New CSpace("CLK",		"Software Internal Clock",			CMCLK ) );
	AddSpace( New CSpace("CTR",		"External Pulse Counter (C4R)",			CMCTR ) );
	AddSpace( New CSpace(CM5CTR,	"CT5R",	"Encoder Position 0=Internal. 1=External (C5)",	 10, 0, 1, addrLongAsLong) );
 
	AddSpace( New CSpace("D",		"Commanded Relative Move (C4)",			CMD ) );
	AddSpace( New CSpace("DEA",		"Actual Derivative Error (R)",			CM5DEA ) );
	AddSpace( New CSpace("DEL",		"Derivative Error Limit (C5)",			CM5DEL ) );
	AddSpace( New CSpace("DT",		"Target Deceleration (C5)",			CM5DT ) );

	AddSpace( New CSpace("E",		"Max Following Error (C4 = E, C5 = EL)",	CME ) );
	AddSpace( New CSpace("EA",		"Read Following Error (C5R)",			CM5EA ) );
	AddSpace( New CSpace("ECHO",		"Echo Received Characters (W)",			CMECHO ) );
	AddSpace( New CSpace(CM5EIGN,	"EIGN",	"Set User Port as Input (C5W)",			 10, 0, 6, addrLongAsLong) );
	AddSpace( New CSpace("EILN",		"Port D -> Neg. limit (C5W)",			CM5EILN ) );
	AddSpace( New CSpace("EILP",		"Port C -> Pos. limit (C5W)",			CM5EILP ) );
	AddSpace( New CSpace("EISM",		"Port G -> G Command (C5W)",			CM5EISM ) );
	AddSpace( New CSpace("END",		"End Running Program (W)",			CMEND ) );
	AddSpace( New CSpace(CM5EOBK,	"EOBK",	"Brake Control (C5W)",				 10, 0, 6, addrLongAsLong ) );
	AddSpace( New CSpace("EOFF",		"Echo Characters Off (W)",			CMECHO_OFF ) );

	AddSpace( New CSpace("F",		"Buffered K's->PID filter (C4)",		CMF ) );

	AddSpace( New CSpace("GSUB",		"Execute GOSUB (W)",				CMGOSUB ) );
	AddSpace( New CSpace("G",		"Go Command (W)",				CMG ) );
 

	AddSpace( New CSpace("I",		"Last Index Position (C4R)",			CMI ) );
	AddSpace( New CSpace(CM5INA,	"INA",	"Read Raw Analog Value (C5R)",			 10, 0, 6, addrLongAsLong ) );
	AddSpace( New CSpace(CM5INV,	"INV",	"Read Scaled Analog Value (C5R)",		 10, 0, 6, addrLongAsLong ) );
	AddSpace( New CSpace(CM5IO,	"IO",	"I/O State - IN/OR/OS (C5)",			 10, 0, 6, addrLongAsLong ) );
	AddSpace( New CSpace(CM5IOW,	"IOW",	"I/O Word IOW0=Local. IOW1=Ext'd.(C5)",		 10, 0, 1, addrLongAsLong ) );

	AddSpace( New CSpace("KA",		"Accel Feed Fwd coef. (kaff)",			CMKA ) );
	AddSpace( New CSpace("KD",		"Differential coef. (kd)",			CMKD ) );
	AddSpace( New CSpace("KG",		"Graviational Term (kg)",			CMKG ) );
	AddSpace( New CSpace("KI",		"Integral coef. (ki)",				CMKI ) );
	AddSpace( New CSpace("KL",		"Integral Limit term (kl)",			CMKL ) );
	AddSpace( New CSpace("KP",		"Proportional coef. (kp)",			CMKP ) );
	AddSpace( New CSpace("KS",		"Differential Sample Rate (ks)",		CMKS ) );
	AddSpace( New CSpace("KV",		"Vel. Feed Fwd coef.(kvff)",			CMKV ) );

	AddSpace( New CSpace("MF0",		"Reset Counter, Counter only Mode (W)",		CMMF0 ) );
	AddSpace( New CSpace("MF1",		"Reset Counter-1 int. ct./ext. ct. (W)",	CMMF1 ) );
	AddSpace( New CSpace("MF2",		"Reset Counter-2 int. cts/ext. ct (W)",		CMMF2 ) );
	AddSpace( New CSpace("MF4",		"Reset Counter-4 int. cts/ext. ct (W)",		CMMF4 ) );
	AddSpace( New CSpace("MFA",		"Accel over Master Dist. (C5rW)",		CM5MFA ) );
	AddSpace( New CSpace("MFD",		"Decel over Master Dist. (C5rW)",		CM5MFD ) );
	AddSpace( New CSpace("MFDIV",		"Denominator for Ratio",			CMMFDIV ) );
	AddSpace( New CSpace("MFMUL",		"Numerator for Ratio",				CMMFMUL ) );
	AddSpace( New CSpace("MFR",		"MFMUL/MFDIV ratio-MFx counter mode (W)",	CMMFR ) );
	AddSpace( New CSpace("MFS",		"Stay at Slew for Distance (C5rW)",		CM5MFS ) );
	AddSpace( New CSpace("MFX",		"Slow based on MFD (C5W)",			CM5MFX ) );
	AddSpace( New CSpace("MINV0",		"Set Motor Commutation to Default (C5W)",	CM5MINV0 ) );
	AddSpace( New CSpace("MINV1",		"Set Motor Commutation to Invert (C5W)",	CM5MINV1 ) );
	AddSpace( New CSpace("MP",		"Set Buffered Position Mode (W)",		CMMP ) );
	AddSpace( New CSpace("MSR",		"MFMUL/MFDIV ratio-MS counter mode (W)",	CMMSR ) );
	AddSpace( New CSpace("MS0",		"Enable Mode Step/Dir.-counter only (W)",	CMMS0 ) );
	AddSpace( New CSpace("MS",		"Enable Mode Step/Dir. Follow Enc2 (W)",	CMMS ) );
	AddSpace( New CSpace("MT",		"Set/Apply Mode Torque (W)",			CMMT ) );
	AddSpace( New CSpace("MV",		"Set Buffered Velocity Mode (W)",		CMMV ) );

	AddSpace( New CSpace("O",		"Set Origin (W)",				CMO ) );
	AddSpace( New CSpace("OFF",		"Turn Motor Servo Off (W)",			CMOFF ) );
	AddSpace( New CSpace("OSH",		"Shift Present Position (C5rW)",		CM5OSH ) );
	AddSpace( New CSpace(CM5OUT,	"OUT",	"Set Port as Output (C5W)",			 10, 0, 6, addrLongAsLong ) );

	AddSpace( New CSpace("P",		"Position (C4 = P, C5 = PT)",			CMP ) );
	AddSpace( New CSpace("PA",		"Actual Position (C5R)",			CM5PA ) );
	AddSpace( New CSpace("PC",		"Commanded Position (C5R)",			CM5PC ) );
	AddSpace( New CSpace("PE",		"Present Position Error (C4R)",			CMPE ) );
	AddSpace( New CSpace("PRA",		"Read Relative Position (C5R)",			CM5PRA ) );
	AddSpace( New CSpace("PRT",		"Target Relative Position",			CM5PRT ) );
 
	AddSpace( New CSpace("RSB",		"Status Byte - (S).0 to (S).7 (C4R)",		CMRSB ) );
	AddSpace( New CSpace("RUN",		"Run Stored User Program (W)",			CMRUN ) );

	AddSpace( New CSpace("SADDR",		"Change Unit Address (W)",			CMSADDR ) );
	AddSpace( New CSpace("SFast",		"Fast Stop [S] (W)",				CMS ) );
	AddSpace( New CSpace("SILENT",		"No Transmit to Aux Channel (W)",		CMSILENT1 ) );
	AddSpace( New CSpace("SLEEP",		"Ignore Host Commands (W)",			CMSLEEP ) );
	AddSpace( New CSpace("SLEEP1",		"Ignore Aux Ch. Commands (W)",			CMSLEEP1 ) );
	AddSpace( New CSpace("SLD",		"Soft Limit Disable (C5W)",			CM5SLD ) );
	AddSpace( New CSpace("SLE",		"Soft Limit Enable (C5W)",			CM5SLE ) );
	AddSpace( New CSpace("SLM0",		"Soft Limit Trigger Flag-No Fault (C5W)",	CM5SLM0 ) );
	AddSpace( New CSpace("SLM1",		"Soft Limit Trigger Flag-Fault (C5W)",		CM5SLM1 ) );
	AddSpace( New CSpace("SLN",		"Soft Limit Set Negative Limit (C5)",		CM5SLN ) );
	AddSpace( New CSpace("SLP",		"Soft Limit Set Positive Limit (C5)",		CM5SLP ) );
	AddSpace( New CSpace("SSRN",		"SSRSP Current Response Number <r>",		CMSUBRR ) );
	AddSpace( New CSpace("SSRSP",		"GOSUB with a String Response (W)",		CMSUBRS ) );
	AddSpace( New CSpace(CM5GSTR,	"GSTR",	"GOSUB String Response <r>",			10, 0, 15, addrLongAsLong));
	AddSpace( New CSpace("SSR01",		"SSRSP Response Word 1 <r>",			CMSUBR1 ) );
	AddSpace( New CSpace("SSR02",		"SSRSP Response Word 2 <r>",			CMSUBR2 ) );
	AddSpace( New CSpace("SSR03",		"SSRSP Response Word 3 <r>",			CMSUBR3 ) );
	AddSpace( New CSpace("SSR04",		"SSRSP Response Word 4 <r>",			CMSUBR4 ) );
	AddSpace( New CSpace("SSR05",		"SSRSP Response Word 5 <r>",			CMSUBR5 ) );
	AddSpace( New CSpace("SSR06",		"SSRSP Response Word 6 <r>",			CMSUBR6 ) );
	AddSpace( New CSpace("SSR07",		"SSRSP Response Word 7 <r>",			CMSUBR7 ) );
	AddSpace( New CSpace("SSR08",		"SSRSP Response Word 8 <r>",			CMSUBR8 ) );
	AddSpace( New CSpace("SSR09",		"SSRSP Response Word 9 <r>",			CMSUBR9 ) );
	AddSpace( New CSpace("SSR10",		"SSRSP Response Word 10 <r>",			CMSUBR10 ) );
	AddSpace( New CSpace("SSR11",		"SSRSP Response Word 11 <r>",			CMSUBR11 ) );
	AddSpace( New CSpace("SSR12",		"SSRSP Response Word 12 <r>",			CMSUBR12 ) );
	AddSpace( New CSpace("SSR13",		"SSRSP Response Word 13 <r>",			CMSUBR13 ) );
	AddSpace( New CSpace("SSR14",		"SSRSP Response Word 14 <r>",			CMSUBR14 ) );
	AddSpace( New CSpace("SSR15",		"SSRSP Response Word 15 <r>",			CMSUBR15 ) );
	AddSpace( New CSpace("SSR16",		"SSRSP Response Word 16 <r>",			CMSUBR16 ) );
	AddSpace( New CSpace("STACK",		"Reset Stack Pointer (W)",			CMSTACK ) );

	AddSpace( New CSpace("T",		"Buffered Torque Magnitude",			CMT ) );
	AddSpace( New CSpace("TALK1",		"Transmit to Aux Channel (W)",			CMTALK1 ) );

	AddSpace( New CSpace("UAI",		"Port A -> Input Port (C4W)",			CMUAI ) );
	AddSpace( New CSpace("UBI",		"Port B -> Input Port (C4W)",			CMUBI ) );
	AddSpace( New CSpace("UCI",		"Port C -> Input Port (C4W)",			CMUCI ) );
	AddSpace( New CSpace("UDI",		"Port D -> Input Port (C4W)",			CMUDI ) );
	AddSpace( New CSpace("UEI",		"Port E -> Input Port (C4W)",			CMUEI ) );
	AddSpace( New CSpace("UFI",		"Port F -> Input Port (C4W)",			CMUFI ) );
	AddSpace( New CSpace("UGI",		"Port G -> Input Port (C4W)",			CMUGI ) ); 
	AddSpace( New CSpace("UG2",		"Reset Port G to Synch. GO (C4W)",		CMUGC ) );
	AddSpace( New CSpace("UAO",		"Port A -> Output Port (C4W)",			CMUAO ) );
	AddSpace( New CSpace("UBO",		"Port B -> Output Port (C4W)",			CMUBO ) );
	AddSpace( New CSpace("UCO",		"Port C -> Output Port (C4W)",			CMUCO ) );
	AddSpace( New CSpace("UDO",		"Port D -> Output Port (C4W)",			CMUDO ) );
	AddSpace( New CSpace("UEO",		"Port E -> Output Port (C4W)",			CMUEO ) );
	AddSpace( New CSpace("UFO",		"Port F -> Output Port (C4W)",			CMUFO ) );
	AddSpace( New CSpace("UGO",		"Port G -> Output Port (C4W)",			CMUGO ) );
	AddSpace( New CSpace("UCP",		"Port C -> right limit input (C4W)",		CMUCP ) );
	AddSpace( New CSpace("UDM",		"Port D -> left limit input (C4W)",		CMUDM ) );
	AddSpace( New CSpace("UA",		"Set Voltage on Port A (C4rW)",			CMUA ) );
	AddSpace( New CSpace("UB",		"Set Voltage on Port B (C4rW)",			CMUB ) );
	AddSpace( New CSpace("UC",		"Set Voltage on Port C (C4rW)",			CMUC ) );
	AddSpace( New CSpace("UD",		"Set Voltage on Port D (C4rW)",			CMUD ) );
	AddSpace( New CSpace("UE",		"Set Voltage on Port E (C4rW)",			CMUE ) );
	AddSpace( New CSpace("UF",		"Set Voltage on Port F (C4rW)",			CMUF ) );
	AddSpace( New CSpace("UG",		"Set Voltage on Port G (C4rW)",			CMUG ) );

	AddSpace( New CSpace("VRA",		"Read Actual Velocity (C5R)",			CM5VA ) );
	AddSpace( New CSpace("Vel",		"Commanded Velocity (C4 = V, C5 = VT)",		CMV ) );
	AddSpace( New CSpace("Vaaa",		"Variable aaa",					CMaaa ) );
	AddSpace( New CSpace("Vbbb",		"Variable bbb",					CMbbb ) );
	AddSpace( New CSpace("Vccc",		"Variable ccc",					CMccc ) );
	AddSpace( New CSpace("Vddd",		"Variable ddd",					CMddd ) );
	AddSpace( New CSpace("Veee",		"Variable eee",					CMeee ) );
	AddSpace( New CSpace("Vfff",		"Variable fff",					CMfff ) );
	AddSpace( New CSpace("Vggg",		"Variable ggg",					CMggg ) );
	AddSpace( New CSpace("Vhhh",		"Variable hhh",					CMhhh ) );
	AddSpace( New CSpace("Viii",		"Variable iii",					CMiii ) );
	AddSpace( New CSpace("Vjjj",		"Variable jjj",					CMjjj ) );
	AddSpace( New CSpace("Vkkk",		"Variable kkk",					CMkkk ) );
	AddSpace( New CSpace("Vlll",		"Variable lll",					CMlll ) );
	AddSpace( New CSpace("Vmmm",		"Variable mmm",					CMmmm ) );
	AddSpace( New CSpace("Vnnn",		"Variable nnn",					CMnnn ) );
	AddSpace( New CSpace("Vooo",		"Variable ooo",					CMooo ) );
	AddSpace( New CSpace("Vppp",		"Variable ppp",					CMppp ) );
	AddSpace( New CSpace("Vqqq",		"Variable qqq",					CMqqq ) );
	AddSpace( New CSpace("Vrrr",		"Variable rrr",					CMrrr ) );
	AddSpace( New CSpace("Vsss",		"Variable sss",					CMsss ) );
	AddSpace( New CSpace("Vttt",		"Variable ttt",					CMttt ) );
	AddSpace( New CSpace("Vuuu",		"Variable uuu",					CMuuu ) );
	AddSpace( New CSpace("Vvvv",		"Variable vvv",					CMvvv ) );
	AddSpace( New CSpace("Vwww",		"Variable www",					CMwww ) );
	AddSpace( New CSpace("Vxxx",		"Variable xxx",					CMxxx ) );
	AddSpace( New CSpace("Vyyy",		"Variable yyy",					CMyyy ) );
	AddSpace( New CSpace("Vzzz",		"Variable zzz",					CMzzz ) );
	AddSpace( New CSpace("Vaa",		"Variable aa",					CMaa ) );
	AddSpace( New CSpace("Vbb",		"Variable bb",					CMbb ) );
	AddSpace( New CSpace("Vcc",		"Variable cc",					CMcc ) );
	AddSpace( New CSpace("Vdd",		"Variable dd",					CMdd ) );
	AddSpace( New CSpace("Vee",		"Variable ee",					CMee ) );
	AddSpace( New CSpace("Vff",		"Variable ff",					CMff ) );
	AddSpace( New CSpace("Vgg",		"Variable gg",					CMgg ) );
	AddSpace( New CSpace("Vhh",		"Variable hh",					CMhh ) );
	AddSpace( New CSpace("Vii",		"Variable ii",					CMii ) );
	AddSpace( New CSpace("Vjj",		"Variable jj",					CMjj ) );
	AddSpace( New CSpace("Vkk",		"Variable kk",					CMkk ) );
	AddSpace( New CSpace("Vll",		"Variable ll",					CMll ) );
	AddSpace( New CSpace("Vmm",		"Variable mm",					CMmm ) );
	AddSpace( New CSpace("Vnn",		"Variable nn",					CMnn ) );
	AddSpace( New CSpace("Voo",		"Variable oo",					CMoo ) );
	AddSpace( New CSpace("Vpp",		"Variable pp",					CMpp ) );
	AddSpace( New CSpace("Vqq",		"Variable qq",					CMqq ) );
	AddSpace( New CSpace("Vrr",		"Variable rr",					CMrr ) );
	AddSpace( New CSpace("Vss",		"Variable ss",					CMss ) );
	AddSpace( New CSpace("Vtt",		"Variable tt",					CMtt ) );
	AddSpace( New CSpace("Vuu",		"Variable uu",					CMuu ) );
	AddSpace( New CSpace("Vvv",		"Variable vv",					CMvv ) );
	AddSpace( New CSpace("Vww",		"Variable ww",					CMww ) );
	AddSpace( New CSpace("Vxx",		"Variable xx",					CMxx ) );
	AddSpace( New CSpace("Vyy",		"Variable yy",					CMyy ) );
	AddSpace( New CSpace("Vzz",		"Variable zz",					CMzz ) );
	AddSpace( New CSpace("Va",		"Variable a",					CMa ) );
	AddSpace( New CSpace("Vb",		"Variable b",					CMb ) );
	AddSpace( New CSpace("Vc",		"Variable c",					CMc ) );
	AddSpace( New CSpace("Vd",		"Variable d",					CMd ) );
	AddSpace( New CSpace("Ve",		"Variable e",					CMe ) );
	AddSpace( New CSpace("Vf",		"Variable f",					CMf ) );
	AddSpace( New CSpace("Vg",		"Variable g",					CMg ) );
	AddSpace( New CSpace("Vh",		"Variable h",					CMh ) );
	AddSpace( New CSpace("Vi",		"Variable i",					CMi ) );
	AddSpace( New CSpace("Vj",		"Variable j",					CMj ) );
	AddSpace( New CSpace("Vk",		"Variable k",					CMk ) );
	AddSpace( New CSpace("Vl",		"Variable l",					CMl ) );
	AddSpace( New CSpace("Vm",		"Variable m",					CMm ) );
	AddSpace( New CSpace("Vn",		"Variable n",					CMn ) );
	AddSpace( New CSpace("Vo",		"Variable o",					CMo ) );
	AddSpace( New CSpace("Vp",		"Variable p",					CMp ) );
	AddSpace( New CSpace("Vq",		"Variable q",					CMq ) );
	AddSpace( New CSpace("Vr",		"Variable r",					CMr ) );
	AddSpace( New CSpace("Vs",		"Variable s",					CMs ) );
	AddSpace( New CSpace("Vt",		"Variable t",					CMt ) );
	AddSpace( New CSpace("Vu",		"Variable u",					CMu ) );
	AddSpace( New CSpace("Vv",		"Variable v",					CMv ) );
	AddSpace( New CSpace("Vw",		"Variable w",					CMw ) );
	AddSpace( New CSpace("Vx",		"Variable x",					CMx ) );
	AddSpace( New CSpace("Vy",		"Variable y",					CMy ) );
	AddSpace( New CSpace("Vz",		"Variable z",					CMz ) );

	AddSpace( New CSpace(CM5W5,	"W5S",	"Status Word (C5R)",				 10, 0, 16, addrLongAsLong) );
	AddSpace( New CSpace("WAKE",		"Terminate Sleep State (W)",			CMWAKE ) );
	AddSpace( New CSpace("WAKE1",		"Terminate Sleep1 State (W)",			CMWAKE1 ) );
	AddSpace( New CSpace("WStat",		"Status Word - (W).0 to (W).15 (R)",		CMW ) );

	AddSpace( New CSpace("X",		"Ramp Stop (W)",				CMX ) );

	AddSpace( New CSpace(CM5Z5W,	"Z5W",	"Clear Status Word bit (<data> = bit #) (W)",	 10, 0, 16, addrLongAsLong) );
	AddSpace( New CSpace("Zall",		"Software Reset-Everything [ZW] (W)",		CMZ ) );
	AddSpace( New CSpace("Za",		"Reset Current Limit Violation (W)",		CMZa ) );
	AddSpace( New CSpace("Zb",		"Reset Serial Data Parity Error (W)",		CMZb ) );
	AddSpace( New CSpace("Zc",		"Reset Comms Buffer Overflow (W)",		CMZc ) );
	AddSpace( New CSpace("Zd",		"Reset User Math Overflow (W)",			CMZd ) );
	AddSpace( New CSpace("Zf",		"Reset Comms Framing Error (W)",		CMZf ) );
	AddSpace( New CSpace("Zl",		"Reset Left Limit Seen (W)",			CMZl ) );
	AddSpace( New CSpace("Zr",		"Reset Right Limit Seen (W)",			CMZr ) );
	AddSpace( New CSpace("Zs",		"Reset Command Syntax Error (W)",		CMZs ) );
	AddSpace( New CSpace("Zu",		"Reset Array Out of Range (W)",			CMZu ) );
	AddSpace( New CSpace("Zw",		"Reset Wrap-Around Indication (W)",		CMZw ) );
	AddSpace( New CSpace("ZSB",		"Reset User System Bits [ZS] (W)",		CMZS ) );
	}

// End of File
