
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MSCON_HPP
	
#define	INCLUDE_MSCON_HPP

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Device Options
//

class CMicroscanDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMicroscanDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Driver Options
//

class CMicroscanDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMicroscanDriverOptions(void);

		// UI Loading
		void LoadUI(CUIViewWnd *pWnd, UINT uPage);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// View Creation
		CViewWnd * CreateItemView(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT    m_Concentrator;
		UINT    m_Port;
		UINT    m_TimePrefix;
		UINT    m_DatePrefix;
		UINT	m_StampSep;
		UINT	m_StampLoc;
		CString m_TxPreamble;
		CString m_TxPostamble;
		UINT	m_TxDrop;
		UINT    m_RxTerm;
		UINT	m_RxTime;
		UINT	m_TxTime;
		UINT    m_Protocol;
		UINT	m_RES;
		UINT	m_REQ;
		UINT	m_STX;
		UINT	m_ETX;
		UINT	m_ACK;
		UINT	m_NAK;
		UINT	m_LRC;
		UINT	m_AckNak;
		UINT	m_ReqRes;
		UINT	m_RetryCount;
		UINT	m_RetryInf;

	protected:
		// Data Members
		BOOL m_fTime;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void AddString(CInitData &Init, CString const &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Master Driver
//

class CMicroscanDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMicroscanDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration	
		CLASS GetDeviceConfig(void);
		CLASS GetDriverConfig(void);

	protected:
		// Implementation
		void AddSpaces(void); 
	};
 
// End of File

#endif
