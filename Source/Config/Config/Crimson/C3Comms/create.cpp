
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Driver Instantiators
//

extern	ICommsDriver *	Create_3psNetDriver(void);
extern  ICommsDriver *  Create_ABBCOMLIMasterDriver(void);
extern  ICommsDriver *  Create_ABL5kDriver(void);
extern  ICommsDriver *  Create_ABLogix5EIPMaster(void);
extern  ICommsDriver *  Create_ABLogix5SerialMaster(void);
extern	ICommsDriver *	Create_ABMicro800SerialMaster(void);
extern	ICommsDriver *	Create_ABMicro800TcpMaster(void);
extern  ICommsDriver *  Create_ABUltra3Driver(void);
extern	ICommsDriver *	Create_AcromagTCPDriver(void);
extern  ICommsDriver *  Create_ACTechSSDriver(void);
extern  ICommsDriver *  Create_ACTechSSUDPDriver(void);
extern	ICommsDriver *	Create_Adam4000v2Driver(void);
extern  ICommsDriver *  Create_Adam4017Driver(void);
extern  ICommsDriver *  Create_AdenusDriver(void);
extern  ICommsDriver *  Create_AlphaTernaryDriver(void);
extern  ICommsDriver *  Create_AlstomSNPMasterDriver(void);
extern  ICommsDriver *  Create_AlstomSRTPMasterTCPDriver(void);
extern  ICommsDriver *  Create_AnimaticsDriver(void);
extern  ICommsDriver *  Create_AppliedMotionDriver(void);
extern  ICommsDriver *  Create_BACNet8023Driver(void);
extern  ICommsDriver *  Create_BACNet8023Slave(void);
extern  ICommsDriver *  Create_BACNetIPDriver(void);
extern  ICommsDriver *  Create_BACNetIPSlave(void);
extern  ICommsDriver *  Create_BACNetMSTPDriver(void);
extern  ICommsDriver *  Create_BACNetMSTPSlave(void);
extern	ICommsDriver *	Create_BaldorSerialMasterDriver(void);
extern  ICommsDriver *  Create_BannerCameraDriver(void);
extern  ICommsDriver *  Create_BannerModbusTCPDriver(void);
extern  ICommsDriver *  Create_BBBSAPSerialDriver(void);
extern  ICommsDriver *  Create_BBBSAPSerialSDriver(void);
extern  ICommsDriver *  Create_BBBSAPTCPDriver(void);
extern  ICommsDriver *  Create_BBBSAPTCPSDriver(void);
extern  ICommsDriver *  Create_BeckhoffADSTCPDriver(void);
extern  ICommsDriver *  Create_BeckhoffPlusTCPDriver(void);
extern	ICommsDriver *	Create_BMikeLSEthernetMasterDriver(void);
extern	ICommsDriver *	Create_BMikeLSFinalLenSlaveDriver(void);
extern	ICommsDriver *	Create_BMikeLSFinalLenSlaveUdpDriver(void);
extern	ICommsDriver *	Create_BMikeLSMasterDriver(void);
extern	ICommsDriver *	Create_BoulderWindPowerRawCANDriver(void);
extern	ICommsDriver *	Create_BRMininetSerialDriver(void);
extern	ICommsDriver *	Create_BSAPFULLSerialDriver(void);
extern	ICommsDriver *	Create_BSAPFULLSerialSDriver(void);
extern	ICommsDriver *	Create_BSAPFULLUDPDriver(void);
extern	ICommsDriver *	Create_BSAPFULLUDPSDriver(void);
extern	ICommsDriver *	Create_CAN29bitIdEntryRawDriver(void);
extern	ICommsDriver *	Create_CANCalibrationDriver(void);
extern	ICommsDriver *  Create_CANGenericDriver(void);
extern  ICommsDriver *  Create_CANJ1939Driver(void);
extern  ICommsDriver *  Create_CANOpenBaldorSDOSlaveDriver(void);
extern  ICommsDriver *  Create_CANOpenPDOSlaveDriver(void);
extern  ICommsDriver *  Create_CANOpenSDODriver(void);
extern  ICommsDriver *  Create_CANOpenSlaveDriver(void);
extern	ICommsDriver *	Create_CANRawDriver(void);
extern  ICommsDriver *  Create_CatDataLinkDriver1(void);
extern  ICommsDriver *  Create_CatDataLinkDriver2(void);
extern  ICommsDriver *  Create_CatDataLinkDriver3(void);
extern	ICommsDriver *	Create_CatDataLinkDriver4(void);
extern  ICommsDriver *  Create_CatDataLinkTester(void);
extern  ICommsDriver *  Create_CatPyroMasterDriver(void);
extern  ICommsDriver *  Create_CKeyKVMasterSerialDriver(void);
extern	ICommsDriver *	Create_CKoyoEbcMasterUdpDriver(void);
extern  ICommsDriver *  Create_CKoyoMasterUDPDriver(void);
extern  ICommsDriver *  Create_CMitsAQMasterTCPDriver(void);
extern	ICommsDriver *	Create_CMitsAQMasterUDPDriver(void);
extern  ICommsDriver *  Create_CMitsQMasterTCPDriver(void);
extern  ICommsDriver *  Create_CognexCameraDriver(void);
extern	ICommsDriver *	Create_CognexDataDriver(void);
extern  ICommsDriver *  Create_ContrexMRotaryDriver(void);
extern  ICommsDriver *  Create_ContrexMTrimDriver(void);
extern  ICommsDriver *  Create_CoriolisSerialDriver(void);
extern  ICommsDriver *  Create_CoriolisTCPDriver(void);
extern  ICommsDriver *  Create_CParker6KTCPDriver(void);
extern  ICommsDriver *  Create_CruisairDriver(void);
extern  ICommsDriver *  Create_CS7MPITCPDriver(void);
extern  ICommsDriver *  Create_CTC2xxxSerialDriver(void);
extern	ICommsDriver *	Create_CTI2572Driver(void);
extern	ICommsDriver *	Create_CTICampMasterSerialDriver(void);
extern	ICommsDriver *	Create_CTINitpMasterDriver(void);
extern	ICommsDriver *	Create_CTINitpMasterTCPDriver(void);
extern  ICommsDriver *  Create_CTQuantumDriver(void);
extern  ICommsDriver *  Create_CYaskawaSMCTCPDriver(void);
extern  ICommsDriver *  Create_CYaskawaTCPDriver(void);
extern  ICommsDriver *  Create_CYokoFam3TCPDriver(void);
extern  ICommsDriver *  Create_DeviceNetSlaveDriver(void);
extern  ICommsDriver *  Create_DeviceNetSlave2Driver(void);
extern  ICommsDriver *  Create_DF1Driver(void);
extern  ICommsDriver *  Create_DF1MasterEIPDriver(void);
extern  ICommsDriver *  Create_DF1SerialSlaveDriver(void);
extern	ICommsDriver *	Create_Dnp3MasterSerialDriver(void);
extern	ICommsDriver *	Create_Dnp3MasterTcpDriver(void);
extern	ICommsDriver *	Create_Dnp3SlaveSerialDriver(void);
extern  ICommsDriver *  Create_Dnp3SlaveSerialv2Driver(void);
extern	ICommsDriver *	Create_Dnp3SlaveTcpDriver(void);
extern  ICommsDriver *  Create_Dnp3SlaveTcpv2Driver(void);
extern	ICommsDriver *	Create_DometicAirNetDriver(void);
extern	ICommsDriver *	Create_DoMoreDriver(void);
extern	ICommsDriver *	Create_DoMoreUDPDriver(void);
extern  ICommsDriver *  Create_DPviaMPIDriver(void);
extern  ICommsDriver *  Create_DummySerialDriver(void);
extern  ICommsDriver *  Create_DuplineModbusDriver(void);
extern  ICommsDriver *  Create_DVLT6000Driver(void);
extern	ICommsDriver *	Create_E3ModbusDriver(void);
extern	ICommsDriver *	Create_E3ModbusTCPDriver(void);
extern	ICommsDriver *	Create_EatonELCSerialDriver(void);
extern  ICommsDriver *  Create_EcodriveDriver(void);
extern	ICommsDriver *	Create_EIP2aMasterDriver(void);
extern  ICommsDriver *  Create_ElmoDriver(void);
extern  ICommsDriver *  Create_EMCOFP93SerialDriver(void);
extern  ICommsDriver *  Create_EmersonEPDriver(void);
extern  ICommsDriver *  Create_EmersonEPTCPDriver(void);
extern  ICommsDriver *	Create_EmersonFXSerialDriver(void);
extern  ICommsDriver *  Create_EmersonMCDriver(void);
extern	ICommsDriver *	Create_EmersonRocMasterDriver(void);
extern	ICommsDriver *	Create_EmersonRocMasterDriver2(void);
extern	ICommsDriver *	Create_EmersonRocMasterDriver3(void);
extern	ICommsDriver *	Create_EmersonRocTCPMasterDriver3(void);
extern	ICommsDriver *	Create_EncDF1MasterTCPDriver(void);
extern  ICommsDriver *  Create_EnModbusMasterTCPDriver(void);
extern  ICommsDriver *  Create_EthernetIPSlaveDriver(void);
extern  ICommsDriver *  Create_Euro590Driver(void);
extern  ICommsDriver *  Create_Euro631Driver(void);
extern  ICommsDriver *  Create_Euro635Driver(void);
extern  ICommsDriver *  Create_Euro690Driver(void);
extern	ICommsDriver *	Create_EuroInvensysEPowerSerialDriver(void);
extern	ICommsDriver *	Create_EuroInvensysEPowerTCPDriver(void);
extern	ICommsDriver *	Create_EuroInvensysMini8SerialDriver(void);
extern	ICommsDriver *	Create_EuroInvensysMini8TCPDriver(void);
extern	ICommsDriver *	Create_EuroInvensysNanoDacSerialDriver(void);
extern	ICommsDriver *	Create_EuroInvensysNanoDacTCPDriver(void);
extern	ICommsDriver *	Create_EuroInvensysT2550SerialDriver(void);
extern	ICommsDriver *	Create_EuroInvensysT2550TCPDriver(void);
extern	ICommsDriver *	Create_EuroInvensys3504SerialDriver(void);
extern	ICommsDriver *	Create_EuroInvensys3504TCPDriver(void);
extern  ICommsDriver *  Create_EuroOld590Driver(void);
extern  ICommsDriver *  Create_EurortnxDriver(void);
extern  ICommsDriver *  Create_EurothermUMDriver(void);
extern  ICommsDriver *  Create_EZMasterDriver(void);
extern  ICommsDriver *  Create_EZMasterTCPDriver(void);
extern	ICommsDriver *  Create_FatekPLCSerialDriver(void);
extern	ICommsDriver *  Create_FatekPLCUDPDriver(void);
extern  ICommsDriver *  Create_FestoFPCDriver(void);
extern  ICommsDriver *  Create_FlowCommMasterDriver(void);
extern  ICommsDriver *  Create_GalilSerialDriver(void);
extern  ICommsDriver *  Create_GalilTCPDriver(void);
extern  ICommsDriver *  Create_GandLCEDriver(void);
extern  ICommsDriver *  Create_GarminDriver(void);
extern  ICommsDriver *  Create_Gem80SerialJKMasterDriver(void);
extern  ICommsDriver *  Create_Gem80SerialMasterDriver(void);
extern  ICommsDriver *  Create_Gem80SerialSlaveDriver(void);
extern  ICommsDriver *  Create_Gem80TCPMasterDriver(void);
extern  ICommsDriver *  Create_GeSrtpMasterTCPDriver(void);
extern	ICommsDriver *	Create_HardyLinkSerialMasterDriver(void);
extern  ICommsDriver *  Create_HBMAEDDriver(void);
extern  ICommsDriver *  Create_HitachiHDriver(void);
extern	ICommsDriver *  Create_Honeywell900NetDriver(void);
extern	ICommsDriver *  Create_Honeywell900SerDriver(void);
extern  ICommsDriver *  Create_HoneywellIPC620Driver(void);
extern  ICommsDriver *  Create_HoneywellUDC9000Driver(void);
extern  ICommsDriver *  Create_IairoboDriver(void);
extern  ICommsDriver *  Create_IaixselDriver(void);
extern	ICommsDriver *	Create_IcpDasDconDriver(void);
extern  ICommsDriver *  Create_Idec3PLCDriver(void);
extern	ICommsDriver *  Create_IdecMicroSmartMasterTCPDriver(void);
extern  ICommsDriver *  Create_IfmCoDeSysSpDriver(void);
extern  ICommsDriver *  Create_IfmDualisCameraDriver(void);
extern  ICommsDriver *  Create_IfmDualisDataDriver(void);
extern  ICommsDriver *  Create_IMOGLoaderPortDriver(void);
extern  ICommsDriver *  Create_IMOK7Driver(void);
extern  ICommsDriver *  Create_IMOLoaderPortDriver(void);
extern	ICommsDriver *	Create_ImpactCameraDriver(void);
extern	ICommsDriver *	Create_ImpactDataDriver(void);
extern  ICommsDriver *  Create_IMSMDriveDriver(void);
extern  ICommsDriver *  Create_IndramatCLCDriver(void);
extern	ICommsDriver *	Create_IqanDriver(void);
extern  ICommsDriver *  Create_IrconDriver(void);
extern	ICommsDriver *	Create_JulaboSerialDriver(void);
extern  ICommsDriver *  Create_KEBDIN2Driver(void);
extern	ICommsDriver *	Create_KEBDin2MasterTCPDriver(void);
extern  ICommsDriver *  Create_KEBSlaveDriver(void);
extern  ICommsDriver *  Create_KingBusASCIIDriver(void);
extern  ICommsDriver *  Create_Kollmorgen600Driver(void);
extern  ICommsDriver *  Create_KoyoDirectNetDriver(void);
extern  ICommsDriver *  Create_KoyoKSequenceDriver(void);
extern  ICommsDriver *  Create_KrohneDriver(void);
extern  ICommsDriver *  Create_LaetusBarcodeDriver(void);
extern  ICommsDriver *  Create_Lecom2MasterSerialDriver(void);
extern  ICommsDriver *  Create_LFDDriver(void);
extern  ICommsDriver *  Create_LinkedSerialDriver(void);
extern  ICommsDriver *  Create_LMBDriver(void);
extern  ICommsDriver *  Create_LSMasterKDriver(void);
extern	ICommsDriver *	Create_M2MDataSerialDriver(void);
extern	ICommsDriver *	Create_M2MDataTCPDriver(void);
extern  ICommsDriver *  Create_MaguireMLANDriver(void);
extern  ICommsDriver *  Create_MaguireMLANTCPDriver(void);
extern  ICommsDriver *  Create_MatsushitaFPDatDriver(void);
extern  ICommsDriver *  Create_MatsushitaFPDriver(void);
extern  ICommsDriver *  Create_MatsushitaFPDriver2(void);
extern  ICommsDriver *  Create_MatsushitaFPTCPDriver(void);
extern  ICommsDriver *  Create_MELServoDriver(void);
extern  ICommsDriver *  Create_Mentor2MasterSerialDriver(void);
extern	ICommsDriver *	Create_MetasysN2SystemDriver3(void);
extern	ICommsDriver *	Create_MicromodDCIDriver(void);
extern  ICommsDriver *  Create_MicromodDriver(void);
extern	ICommsDriver *	Create_MicromodTCPDriver(void);
extern  ICommsDriver *  Create_MicroscanDriver(void);
extern	ICommsDriver *	Create_MicroScanHawkCameraDriver(void);
extern	ICommsDriver *	Create_MitsFX2NMasterTCPDriver(void);
extern  ICommsDriver *  Create_MitsubADriver(void);
extern  ICommsDriver *  Create_MitsubFxDriver(void);
extern	ICommsDriver *	Create_ML1SerialDriver(void);
extern	ICommsDriver *	Create_ML1SerialTesterDriver(void);
extern	ICommsDriver *	Create_ML1TCPDriver(void);
extern	ICommsDriver *	Create_ML1TCPTesterDriver(void);
extern	ICommsDriver *	Create_ModbusDeviceServerSerialDriver(void);
extern	ICommsDriver *	Create_ModbusDeviceServerTCPDriver(void);
extern  ICommsDriver *  Create_ModbusDriver(void);
extern  ICommsDriver *  Create_ModbusMasterTCPDriver(void);
extern  ICommsDriver *  Create_ModbusMonitorASCIIDriver(void);
extern  ICommsDriver *  Create_ModbusMonitorRTUDriver(void);
extern  ICommsDriver *  Create_ModbusSlaveASCIIDriver(void);
extern  ICommsDriver *  Create_ModbusSlaveRTUDriver(void);
extern  ICommsDriver *  Create_ModbusSlaveTCPDriver(void);
extern  ICommsDriver *  Create_ModemClientDriver(void);
extern  ICommsDriver *  Create_ModemClientOptionDriver(void);
extern  ICommsDriver *  Create_ModemServerDriver(void);
extern  ICommsDriver *  Create_ModemServerOptionDriver(void);
extern  ICommsDriver *  Create_ModemSMSDriver(void);
extern  ICommsDriver *  Create_ModemSMSOptionDriver(void);
extern	ICommsDriver *	Create_MonicoJ1939Driver(void);
extern	ICommsDriver *	Create_MonicoSNMPDriver(void);
extern	ICommsDriver *	Create_MonicoSNMPDriver2(void);
extern	ICommsDriver *	Create_MotronaSerialMasterDriver(void);
extern  ICommsDriver *  Create_MPEDriver(void);
extern  ICommsDriver *  Create_MTSAsciiSlaveDriver(void);
extern  ICommsDriver *  Create_MTSDDADriver(void);
extern  ICommsDriver *  Create_MTSDDAMaster(void);
extern  ICommsDriver *  Create_MTSModbusDriver(void);
extern	ICommsDriver *	Create_MurMelsec4SlaveDriver(void);
extern	ICommsDriver *	Create_NViewMasterDriver(void);
extern	ICommsDriver *	Create_OmniFlowMasterDriver(void);
extern	ICommsDriver *	Create_OmniFlowMasterTCPDriver(void);
extern	ICommsDriver *	Create_OmniFlowModiconMasterDriver(void);
extern	ICommsDriver *	Create_OmniFlowModiconMasterTCPDriver(void);
extern	ICommsDriver *	Create_OmronFINsG9spMasterDriver(void);
extern	ICommsDriver *	Create_OmronFINsG9spMasterUDPDriver(void);
extern  ICommsDriver *  Create_OmronFINSMasterDriver(void);
extern  ICommsDriver *  Create_OmronFINSMasterUDPDriver(void);
extern  ICommsDriver *  Create_OmronPLCDriver(void);
extern  ICommsDriver *  Create_OPCDriver(void);
extern  ICommsDriver *  Create_OpcUaDriver(void);
extern  ICommsDriver *  Create_PacSci830SerialDriver(void);
extern  ICommsDriver *  Create_PAMasterTCPDriver(void);
extern  ICommsDriver *  Create_PanFp7SerialMasterDriver(void);
extern  ICommsDriver *	Create_PanFp7TcpMasterDriver(void);
extern  ICommsDriver *  Create_Parker6KSerialDriver(void);
extern  ICommsDriver *  Create_ParkerAcroloopDriver(void);
extern	ICommsDriver *	Create_PasonWITSDriver(void);
extern  ICommsDriver *  Create_PFMDriver(void);
extern  ICommsDriver *  Create_PhoenixNanoLCDriver(void);
extern  ICommsDriver *  Create_PhoenixNanoLCTCPDriver(void);
extern  ICommsDriver *  Create_PLC5DF1MasterTCPDriver(void);
extern  ICommsDriver *  Create_PLC5DF1SlaveTCPDriver(void);
extern	ICommsDriver *	Create_PortForwardDriver(void);
extern  ICommsDriver *  Create_PPS4201Driver(void);
extern  ICommsDriver *  Create_ProfibusDPMasterDriver(void);
extern  ICommsDriver *  Create_ProfibusDPSlaveDriver(void);
extern  ICommsDriver *  Create_QuicksilverDriver(void);
extern  ICommsDriver *  Create_RawSerialDriver(void);
extern  ICommsDriver *  Create_RawTCPActiveDriver(void);
extern  ICommsDriver *  Create_RawTCPPassiveDriver(void);
extern  ICommsDriver *  Create_RawUDPDriver(void);
extern  ICommsDriver *  Create_RexrothSISDriver(void);
extern  ICommsDriver *  Create_RlcDriver(void);
extern  ICommsDriver *  Create_S5AS511MasterDriver(void);
extern  ICommsDriver *  Create_S5AS511MasterDriverV2(void);
extern  ICommsDriver *  Create_S5AS511TCPMasterDriver(void);
extern  ICommsDriver *  Create_S5AS511TCPMasterDriverV2(void);
extern	ICommsDriver *	Create_S71KDriver(void);
extern  ICommsDriver *  Create_S7ISOMasterTCPDriver(void);
extern  ICommsDriver *  Create_S7MPIDriver(void);
extern	ICommsDriver *	Create_S7MpiExtTcpDriver(void);
extern  ICommsDriver *  Create_S7PPI2Driver(void);
extern  ICommsDriver *  Create_S7PPIDriver(void);
extern  ICommsDriver *  Create_S7DirectDriver(void);
extern	ICommsDriver *	Create_SchneiderModbusSerialDriver(void);
extern	ICommsDriver *  Create_SchneiderModbusTCPDriver(void);
extern  ICommsDriver *  Create_SEWMovilinkASerialDriver(void);
extern  ICommsDriver *  Create_SEWMovilinkBSerialDriver(void);
extern  ICommsDriver *  Create_SimovertSerialDriver(void);
extern	ICommsDriver *	Create_SkeletonMasterDriver(void);
extern	ICommsDriver *	Create_SkeletonSlaveDriver(void);
extern  ICommsDriver *  Create_SLCDH485Driver(void);
extern	ICommsDriver *	Create_SmaDriver(void);
extern  ICommsDriver *  Create_SmcLc8Driver(void);
extern	ICommsDriver *  Create_SnmpDriver(void);
extern  ICommsDriver *  Create_SNPDriver(void);
extern  ICommsDriver *  Create_SNPXDriver(void);
extern	ICommsDriver *	Create_SpiMasterDriver(void);
extern  ICommsDriver *  Create_SquareDDriver(void);
extern  ICommsDriver *  Create_SSDFireDriver(void);
extern	ICommsDriver *	Create_StiebelWpmIISerialDriver(void);
extern  ICommsDriver *  Create_TestDriver(void);
extern	ICommsDriver *	Create_TestMasterDriver(void);
extern	ICommsDriver *	Create_TestMasterTCPDriver(void);
extern  ICommsDriver *  Create_TestNamedCommsDriver(void);
extern  ICommsDriver *  Create_TestStream1Driver(void);
extern  ICommsDriver *  Create_TestStream2Driver(void);
extern  ICommsDriver *  Create_TestTableCommsDriver(void);
extern  ICommsDriver *  Create_TI500Driver(void);
extern	ICommsDriver *	Create_TI500MasterTCPDriver(void);
extern	ICommsDriver *	Create_TI500v2Driver(void);
extern	ICommsDriver *	Create_ToshExPlusMasterDriver(void);
extern  ICommsDriver *  Create_ToshT2MasterSerialDriver(void);
extern  ICommsDriver *  Create_ToshT2UDPDriver(void);
extern	ICommsDriver *	Create_TotalFlowTcpMasterDriver(void);
extern	ICommsDriver *	Create_TotalFlowSerialMasterDriver(void);
extern	ICommsDriver *	Create_TotalFlow2TcpMasterDriver(void);
extern	ICommsDriver *	Create_TotalFlow2SerialMasterDriver(void);
extern	ICommsDriver *	Create_TotalFlow3SerialMasterDriver(void);
extern	ICommsDriver *	Create_TotalFlow3TcpMasterDriver(void);
extern  ICommsDriver *  Create_ToyodaTCPDriver(void);
extern	ICommsDriver *	Create_UnidriveMDriver(void);
extern	ICommsDriver *	Create_UnidriveMTCPDriver(void);
extern	ICommsDriver *	Create_UniPCOMAsciiDriver(void);
extern	ICommsDriver *	Create_UniPCOMBinaryDriver(void);
extern	ICommsDriver *	Create_UniPCOMTcpADriver(void);
extern	ICommsDriver *	Create_UniPCOMTcpBDriver(void);
extern  ICommsDriver *  Create_UnitelwayMasterDriver(void);
extern  ICommsDriver *  Create_UnitelwayMSDriver(void);
extern  ICommsDriver *  Create_UnitelwayTCPDriver(void);
extern  ICommsDriver *  Create_UnitronicsM90Driver(void);
extern	ICommsDriver *	Create_WebCameraDriver(void);
extern	ICommsDriver *	Create_YamahaRcxDriver(void);
extern	ICommsDriver *	Create_YamahaRcxTcpDriver(void);
extern	ICommsDriver *	Create_YamahaTsSeriesMasterDriver(void);
extern	ICommsDriver *	Create_YaskawaACDrivesDriver(void);
extern	ICommsDriver *	Create_YaskawaACDrivesTCPDriver(void);
extern  ICommsDriver *  Create_YaskawaFSPDriver(void);
extern  ICommsDriver *  Create_YaskawaLegendDriver(void);
extern  ICommsDriver *  Create_YaskawaMemobusTCPDriver(void);
extern	ICommsDriver *	Create_YaskawaMp3000IecDriver(void);
extern  ICommsDriver *  Create_YaskawaMPIECDriver(void);
extern  ICommsDriver *  Create_YaskawaMPMasterDriver(void);
extern  ICommsDriver *  Create_YaskawaNS600Driver(void);
extern  ICommsDriver *  Create_YaskawaSeries7Driver(void);
extern  ICommsDriver *  Create_YaskawaSeries7TCPDriver(void);
extern  ICommsDriver *  Create_YaskawaSGDHDriver(void);
extern  ICommsDriver *  Create_YaskawaUnivSMCDriver(void);
extern  ICommsDriver *  Create_YETXtraDriver(void);
extern  ICommsDriver *  Create_YokoFam3Driver(void);

extern	ICommsDriver *	Create_DebugSerialDriver(void);
extern	ICommsDriver *	Create_DebugTcpDriver(void);

//////////////////////////////////////////////////////////////////////////
//
// Non-Released Drivers
//

//	Create_DF1SerialSlaveDriver,
//	Create_EmersonRocMasterDriver,
//	Create_EuroInvensysEPowerTCPDriver,
//	Create_EuroInvensysT2550SerialDriver,
//	Create_EuroInvensysT2550TCPDriver,
//	Create_S7MpiExtTcpDriver,
//	Create_ToshExPlusMasterDriver,

//////////////////////////////////////////////////////////////////////////
//
// Non-Supported Drivers
//

//	Create_TestStream1Driver,
//	Create_TestStream2Driver,

//////////////////////////////////////////////////////////////////////////
//
// Non-Compiling Drivers
//

//	Create_BeckhoffPlusTCPDriver,

//////////////////////////////////////////////////////////////////////////
//
// Instantiator Table
//

static ICommsDriver * (*m_List[])(void) =
{
	Create_3psNetDriver,
	Create_ABBCOMLIMasterDriver,
	Create_ABL5kDriver,
	Create_ABLogix5EIPMaster,
	Create_ABLogix5SerialMaster,
	Create_ABMicro800SerialMaster,
	Create_ABMicro800TcpMaster,
	Create_ABUltra3Driver,
	Create_AcromagTCPDriver,
	Create_ACTechSSDriver,
	Create_ACTechSSUDPDriver,
	Create_Adam4000v2Driver,
	Create_Adam4017Driver,
	Create_AdenusDriver,
	Create_AlphaTernaryDriver,
	Create_AlstomSNPMasterDriver,
	Create_AlstomSRTPMasterTCPDriver,
	Create_AnimaticsDriver,
	Create_AppliedMotionDriver,
	Create_BACNet8023Driver,
	Create_BACNet8023Slave,
	Create_BACNetIPDriver,
	Create_BACNetIPSlave,
	Create_BACNetMSTPDriver,
	Create_BACNetMSTPSlave,
	Create_BaldorSerialMasterDriver,
	Create_BannerCameraDriver,
	Create_BannerModbusTCPDriver,
	Create_BBBSAPSerialDriver,
	Create_BBBSAPSerialSDriver,
	Create_BBBSAPTCPDriver,
	Create_BBBSAPTCPSDriver,
	Create_BeckhoffADSTCPDriver,
	Create_BMikeLSEthernetMasterDriver,
	Create_BMikeLSFinalLenSlaveDriver,
	Create_BMikeLSFinalLenSlaveUdpDriver,
	Create_BMikeLSMasterDriver,
	Create_BoulderWindPowerRawCANDriver,
	Create_BRMininetSerialDriver,
	Create_BSAPFULLSerialDriver,
	Create_BSAPFULLSerialSDriver,
//	Create_BSAPFULLUDPDriver,
//	Create_BSAPFULLUDPSDriver,
	Create_CAN29bitIdEntryRawDriver,
	Create_CANCalibrationDriver,
	Create_CANGenericDriver,
	Create_CANJ1939Driver,
	Create_CANOpenBaldorSDOSlaveDriver,
	Create_CANOpenPDOSlaveDriver,
	Create_CANOpenSDODriver,
	Create_CANOpenSlaveDriver,
	Create_CANRawDriver,
	Create_CatDataLinkDriver1,
	Create_CatDataLinkDriver2,
	Create_CatDataLinkDriver3,
	Create_CatDataLinkDriver4,
	Create_CatDataLinkTester,
	Create_CatPyroMasterDriver,
	Create_CKeyKVMasterSerialDriver,
	Create_CKoyoEbcMasterUdpDriver,
	Create_CKoyoMasterUDPDriver,
	Create_CMitsAQMasterTCPDriver,
	Create_CMitsAQMasterUDPDriver,
	Create_CMitsQMasterTCPDriver,
	Create_CognexCameraDriver,
	Create_CognexDataDriver,
	Create_ContrexMRotaryDriver,
	Create_ContrexMTrimDriver,
	Create_CoriolisSerialDriver,
	Create_CoriolisTCPDriver,
	Create_CParker6KTCPDriver,
	Create_CruisairDriver,
	Create_CS7MPITCPDriver,
	Create_CTC2xxxSerialDriver,
	Create_CTI2572Driver,
	Create_CTICampMasterSerialDriver,
	Create_CTINitpMasterDriver,
	Create_CTINitpMasterTCPDriver,
	Create_CTQuantumDriver,
	Create_CYaskawaSMCTCPDriver,
	Create_CYaskawaTCPDriver,
	Create_CYokoFam3TCPDriver,
	Create_DeviceNetSlaveDriver,
	Create_DeviceNetSlave2Driver,
	Create_DF1Driver,
	Create_DF1MasterEIPDriver,
	Create_Dnp3MasterSerialDriver,
	Create_Dnp3MasterTcpDriver,
	Create_Dnp3SlaveSerialDriver,
	Create_Dnp3SlaveSerialv2Driver,
	Create_Dnp3SlaveTcpDriver,
	Create_Dnp3SlaveTcpv2Driver,
	Create_DometicAirNetDriver,
	Create_DoMoreDriver,
	Create_DoMoreUDPDriver,
//	Create_DPviaMPIDriver,
	Create_DummySerialDriver,
	Create_DuplineModbusDriver,
	Create_DVLT6000Driver,
	Create_E3ModbusDriver,
	Create_E3ModbusTCPDriver,
	Create_EatonELCSerialDriver,
	Create_EcodriveDriver,
	Create_EIP2aMasterDriver,
	Create_ElmoDriver,
	Create_EMCOFP93SerialDriver,
	Create_EmersonEPDriver,
	Create_EmersonEPTCPDriver,
	Create_EmersonFXSerialDriver,
	Create_EmersonMCDriver,
//	Create_EmersonRocMasterDriver,
	Create_EmersonRocMasterDriver2,
	Create_EmersonRocMasterDriver3,
	Create_EmersonRocTCPMasterDriver3,
	Create_EncDF1MasterTCPDriver,
	Create_EnModbusMasterTCPDriver,
	Create_EthernetIPSlaveDriver,
	Create_Euro590Driver,
	Create_Euro631Driver,
	Create_Euro635Driver,
	Create_Euro690Driver,
	Create_EuroInvensysEPowerSerialDriver,
	Create_EuroInvensysEPowerTCPDriver,
	Create_EuroInvensysMini8SerialDriver,
	Create_EuroInvensysMini8TCPDriver,
//	Create_EuroInvensysNanoDacSerialDriver,
	Create_EuroInvensysNanoDacTCPDriver,
//	Create_EuroInvensysT2550SerialDriver,
//	Create_EuroInvensysT2550TCPDriver,
	Create_EuroInvensys3504SerialDriver,
	Create_EuroInvensys3504TCPDriver,
	Create_EuroOld590Driver,
	Create_EurortnxDriver,
	Create_EurothermUMDriver,
	Create_EZMasterDriver,
	Create_EZMasterTCPDriver,
	Create_FatekPLCSerialDriver,
	Create_FatekPLCUDPDriver,
	Create_FestoFPCDriver,
	Create_FlowCommMasterDriver,
	Create_GalilSerialDriver,
	Create_GalilTCPDriver,
	Create_GandLCEDriver,
	Create_GarminDriver,
	Create_Gem80SerialJKMasterDriver,
	Create_Gem80SerialMasterDriver,
	Create_Gem80SerialSlaveDriver,
	Create_Gem80TCPMasterDriver,
	Create_GeSrtpMasterTCPDriver,
	Create_HardyLinkSerialMasterDriver,
	Create_HBMAEDDriver,
	Create_HitachiHDriver,
	Create_Honeywell900SerDriver,
	Create_Honeywell900NetDriver,
	Create_HoneywellIPC620Driver,
	Create_HoneywellUDC9000Driver,
	Create_IairoboDriver,
	Create_IaixselDriver,
	Create_IcpDasDconDriver,
	Create_Idec3PLCDriver,
	Create_IdecMicroSmartMasterTCPDriver,
	Create_IfmCoDeSysSpDriver,
	Create_IfmDualisCameraDriver,
	Create_IfmDualisDataDriver,
	Create_IMOGLoaderPortDriver,
	Create_IMOK7Driver,
	Create_IMOLoaderPortDriver,
	Create_ImpactCameraDriver,
	Create_ImpactDataDriver,
//	Create_IMSMDriveDriver,
	Create_IndramatCLCDriver,
	Create_IqanDriver,
	Create_IrconDriver,
	Create_JulaboSerialDriver,
	Create_KEBDIN2Driver,
	Create_KEBDin2MasterTCPDriver,
	Create_KEBSlaveDriver,
	Create_KingBusASCIIDriver,
	Create_Kollmorgen600Driver,
	Create_KoyoDirectNetDriver,
	Create_KoyoKSequenceDriver,
	Create_KrohneDriver,
	Create_LaetusBarcodeDriver,
	Create_Lecom2MasterSerialDriver,
	Create_LFDDriver,
	Create_LinkedSerialDriver,
	Create_LMBDriver,
	Create_LSMasterKDriver,
	Create_M2MDataSerialDriver,
	Create_M2MDataTCPDriver,
	Create_MaguireMLANDriver,
	Create_MaguireMLANTCPDriver,
	Create_MatsushitaFPDatDriver,
	Create_MatsushitaFPDriver,
	Create_MatsushitaFPDriver2,
	Create_MatsushitaFPTCPDriver,
	Create_MELServoDriver,
	Create_Mentor2MasterSerialDriver,
	Create_MetasysN2SystemDriver3,
	Create_MicromodDCIDriver,
	Create_MicromodDriver,
	Create_MicromodTCPDriver,
	Create_MicroscanDriver,
	Create_MicroScanHawkCameraDriver,
	Create_MitsFX2NMasterTCPDriver,
	Create_MitsubADriver,
	Create_MitsubFxDriver,
	Create_ML1SerialDriver,
	Create_ML1SerialTesterDriver,
	Create_ML1TCPDriver,
	Create_ML1TCPTesterDriver,
	Create_ModbusDeviceServerSerialDriver,
	Create_ModbusDeviceServerTCPDriver,
	Create_ModbusDriver,
	Create_ModbusMasterTCPDriver,
//	Create_ModbusMonitorASCIIDriver,
	Create_ModbusMonitorRTUDriver,
	Create_ModbusSlaveASCIIDriver,
	Create_ModbusSlaveRTUDriver,
	Create_ModbusSlaveTCPDriver,
	Create_ModemClientDriver,
	Create_ModemClientOptionDriver,
	Create_ModemServerDriver,
	Create_ModemServerOptionDriver,
	Create_ModemSMSDriver,
	Create_ModemSMSOptionDriver,
	Create_MonicoJ1939Driver,
	Create_MonicoSNMPDriver,
	Create_MonicoSNMPDriver2,
	Create_MotronaSerialMasterDriver,
	Create_MPEDriver,
	Create_MTSDDADriver,
	Create_MTSDDAMaster,
	Create_MTSModbusDriver,
	Create_MurMelsec4SlaveDriver,
	Create_NViewMasterDriver,
	Create_OmniFlowMasterDriver,
	Create_OmniFlowMasterTCPDriver,
	Create_OmniFlowModiconMasterDriver,
	Create_OmniFlowModiconMasterTCPDriver,
	Create_OmronFINsG9spMasterDriver,
	Create_OmronFINsG9spMasterUDPDriver,
	Create_OmronFINSMasterDriver,
	Create_OmronFINSMasterUDPDriver,
	Create_OmronPLCDriver,
	Create_OPCDriver,
	Create_OpcUaDriver,
	Create_PacSci830SerialDriver,
	Create_PAMasterTCPDriver,
	Create_PanFp7SerialMasterDriver,
	Create_PanFp7TcpMasterDriver,
	Create_Parker6KSerialDriver,
	Create_ParkerAcroloopDriver,
	Create_PasonWITSDriver,
	Create_PhoenixNanoLCDriver,
	Create_PhoenixNanoLCTCPDriver,
	Create_PFMDriver,
	Create_PLC5DF1MasterTCPDriver,
	Create_PLC5DF1SlaveTCPDriver,
	Create_PortForwardDriver,
	Create_PPS4201Driver,
	Create_ProfibusDPMasterDriver,
	/*Create_ProfibusDPSlaveDriver,*/
	Create_QuicksilverDriver,
	Create_RawSerialDriver, 
	Create_RawTCPActiveDriver,
	Create_RawTCPPassiveDriver,
	Create_RawUDPDriver,
	Create_RexrothSISDriver,
	Create_RlcDriver,
	Create_S5AS511MasterDriver,
	Create_S5AS511MasterDriverV2,
	Create_S5AS511TCPMasterDriver,
	Create_S5AS511TCPMasterDriverV2,
	Create_S71KDriver,
	Create_S7ISOMasterTCPDriver,
	Create_S7MPIDriver,
//	Create_S7MpiExtTcpDriver,
	Create_S7PPI2Driver,
	Create_S7PPIDriver,
	Create_S7DirectDriver,
	Create_SchneiderModbusSerialDriver,
	Create_SchneiderModbusTCPDriver,
	Create_SEWMovilinkASerialDriver,
	Create_SEWMovilinkBSerialDriver,
	Create_SimovertSerialDriver,
//	Create_SkeletonMasterDriver,
//	Create_SkeletonSlaveDriver,
	Create_SLCDH485Driver,
	Create_SmaDriver,
	Create_SmcLc8Driver,
	Create_SnmpDriver,
	Create_SNPDriver,
	Create_SNPXDriver,
	Create_SpiMasterDriver,
	Create_SquareDDriver,
	Create_SSDFireDriver,
	Create_StiebelWpmIISerialDriver,
	Create_TestMasterDriver,
	Create_TestMasterTCPDriver,
	Create_TI500Driver,
	Create_TI500MasterTCPDriver,
	Create_TI500v2Driver,
//	Create_ToshExPlusMasterDriver,
	Create_ToshT2MasterSerialDriver,
	Create_ToshT2UDPDriver, 
	Create_TotalFlowTcpMasterDriver,
	Create_TotalFlowSerialMasterDriver,
	Create_TotalFlow2TcpMasterDriver,
	Create_TotalFlow2SerialMasterDriver,
	Create_TotalFlow3SerialMasterDriver,
	Create_TotalFlow3TcpMasterDriver,
//	Create_ToyodaTCPDriver,
	Create_UnidriveMDriver,
	Create_UnidriveMTCPDriver,
	Create_UniPCOMAsciiDriver,
	Create_UniPCOMBinaryDriver,
	Create_UniPCOMTcpADriver,
	Create_UniPCOMTcpBDriver,
	Create_UnitelwayMasterDriver,
	Create_UnitelwayMSDriver,
	Create_UnitronicsM90Driver,
	Create_WebCameraDriver,
	Create_YamahaRcxDriver,
	Create_YamahaRcxTcpDriver,
	Create_YamahaTsSeriesMasterDriver,
	Create_YaskawaACDrivesDriver,
	Create_YaskawaACDrivesTCPDriver,
	Create_YaskawaFSPDriver,
	Create_YaskawaLegendDriver,
	Create_YaskawaMemobusTCPDriver,
	Create_YaskawaMp3000IecDriver,
	Create_YaskawaMPIECDriver,
	Create_YaskawaMPMasterDriver,
	Create_YaskawaNS600Driver,
	Create_YaskawaSeries7Driver,
	Create_YaskawaSeries7TCPDriver,
	Create_YaskawaSGDHDriver,
	Create_YaskawaUnivSMCDriver,
	Create_YETXtraDriver,
	Create_YokoFam3Driver,

	Create_DebugSerialDriver,
	Create_DebugTcpDriver,
	};

//////////////////////////////////////////////////////////////////////////
//
// Instantiator Index
//

static BOOL              m_fMade = FALSE;

static CMap <UINT, UINT> m_Index;

/////////////////////////////////////////////////////////////////////////
//
// Driver Creation APIs
//

DLLAPI ICommsDriver * C3EnumDrivers(UINT n)
{
	if( n < elements(m_List) ) {

		return m_List[n]();
		}

	return NULL;
	}

DLLAPI ICommsDriver * C3CreateDriver(UINT ID)
{
	if( !m_fMade ) {

		for( UINT n = 0; n < elements(m_List); n++ ) {

			ICommsDriver *pDriver = m_List[n]();

			UINT          ID      = pDriver->GetID();

			if( !m_Index.Insert(ID, n) ) {

				AfxTrace(L"Duplicate Driver ID %4.4X\n", ID);

				AfxAssert(FALSE);
				}

			pDriver->Release();
			}

		m_fMade = TRUE;
		}

	if( m_Index.GetCount() ) {

		INDEX nPos = m_Index.FindName(ID);

		if( !m_Index.Failed(nPos) ) {

			UINT uPos = m_Index.GetData(nPos);

			return m_List[uPos]();
			}
		}

	return NULL;
	}

// End of File
