//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CANGEN_HPP
	
#define	INCLUDE_CANGEN_HPP

#include "r29id.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAN 11-bit/29-bit Constants
//

#define CP_DEF		2018

//////////////////////////////////////////////////////////////////////////
//
// CAN 11-bit/29-bit Identifier
//

class CCanId : public CID29
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCanId(void);
		CCanId(CCanId * pID);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_DefSize;
	
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Generic Device Options
//

class CCANGenericDeviceOptions : public CCAN29bitIdEntryRawDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANGenericDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overidables
		CID29 * FindListID(CString Text);
		CID29 * GetID(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Generic Device Options Page
//

class CCANGenericDeviceOptionsUIPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCANGenericDeviceOptionsUIPage(CCANGenericDeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CCANGenericDeviceOptions * m_pOption;
	}; 


//////////////////////////////////////////////////////////////////////////
//
// CAN Generic Driver
//

class CCANGenericDriver : public CCAN29bitIdEntryRawDriver
{
	public:
		// Constructor
		CCANGenericDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN 11-bit/29-bit Identifier Generic Message Configuration Dialog
//

class CCANGenericDialog : public CId29MessDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCANGenericDialog(CCANGenericDeviceOptions * pOptions, CSystemItem * pSystem);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnEditChange(UINT uID, CWnd &Ctrl);

	protected:

		// Implementation
		void InitDefinition(void);
		
		// Overridables
		CID29 * NewID(void);
		BOOL    SetID(CError &Error, CID29 * pID, UINT uID);
		void    SetID(CID29 * pID);
		void	DoEnables(void);

	};

// End of File

#endif
