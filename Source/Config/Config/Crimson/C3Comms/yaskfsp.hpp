
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YASKAWAFSP_HPP

#define	INCLUDE_YASKAWAFSP_HPP

#define	AN	addrNamed
#define BB	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	ERROP	199
#define	HIMM	238
#define	HSEQ	239

#define	SQ		0x2000 // Offset for Sequential Opcodes

#define	CP1		0x100 // Immediate Cache parameter 1
#define	CP2		0x200 // Immediate Cache parameter 2
#define	CP3		0x300 // Immediate Cache parameter 3
#define	CP4		0x400 // Immediate Cache parameter 4
#define	CP5		0x500 // Immediate Cache parameter 5

#define	CP1S		0x100+SQ // Sequential Cache parameter 1
#define	CP2S		0x200+SQ // Sequential Cache parameter 2
#define	CP3S		0x300+SQ // Sequential Cache parameter 3
#define	CP4S		0x400+SQ // Sequential Cache parameter 4
#define	CP5S		0x500+SQ // Sequential Cache parameter 5

// Command Numbers for runtime
// Table Numbers 10 - 39
#define	POLI	0
#define	ACCI	64
#define	CONI	69
#define	GAII	71
#define	GTVI	63
#define	JRKI	74
#define	RFAI	159
#define	RFAI1	CP1+159
#define	RFAI2	CP2+159
#define	RUNI	78
#define	S1OI	79
#define	S1OI1	CP1+79
#define	S1OI2	CP2+79
#define	SNOI	107
#define	SNOI1	CP1+107
#define	SNOI2	CP2+107
#define	SZAI	95
#define	SPDI	83
#define	STAI	82
#define	STPI	84
#define	STXI	153
#define	STXI1	CP1+153
#define	STXI2	CP2+153
#define	STMI	99
#define	TQLI	87
#define	TQLI1	CP1+87
#define	TQLI2	CP2+87
#define	WRII	158
#define	WRII1	CP1+158
#define	WRII2	CP2+158

// Table Numbers 100+
#define	ACCS	64+SQ
#define	CONS	69+SQ
#define	DELS	144+SQ
#define	ECDS	122+SQ
#define	ECES	121+SQ
#define	ECES1	CP1S+121
#define	ECES2	CP2S+121
#define	ENGS	136+SQ
#define	ENGS1	CP1S+136
#define	ENGS2	CP2S+136
#define	FOSS	154+SQ
#define	FOSS1	CP1S+154
#define	FOSS2	CP2S+154
#define	FOSS3	CP3S+154
#define	GAIS	71+SQ
#define	GOAS	112+SQ
#define	GOAS1	CP1S+112
#define	GOAS2	CP2S+112
#define	GODS	128+SQ
#define	GODS1	CP1S+128
#define	GODS2	CP2S+128
#define	GOHS	117+SQ
#define	HARS	131+SQ
#define	HARS1	CP1S+131
#define	HARS2	CP2S+131
#define	HMCS	133+SQ
#define	HMSS	132+SQ
#define	HMSS1	CP1S+132
#define	HMSS2	CP2S+132
#define	HSCS	130+SQ
#define	HSCS1	CP1S+130
#define	HSCS2	CP2S+130
#define	JRKS	74+SQ
#define	LATS	152+SQ
#define	MVAS	113+SQ
#define	MVAS1	CP1S+113
#define	MVAS2	CP2S+113
#define	MVDS	129+SQ
#define	MVDS1	CP1S+129
#define	MVDS2	CP2S+129
#define	MVHS	118+SQ
#define	MVRS	119+SQ
#define	RFAS	159+SQ
#define	RFAS1	CP1S+159
#define	RFAS2	CP2S+159
#define	REGS	151+SQ
#define	RUNS	78+SQ
#define	S1OS	79+SQ
#define	S1OS1	CP1S+79
#define	S1OS2	CP2S+79
#define	SNOS	107+SQ
#define	SNOS1	CP1S+107
#define	SNOS2	CP2S+107
#define	SZAS	95+SQ
#define	SLDS	115+SQ
#define	SLNS	102+SQ
#define	SPDS	83+SQ
#define	SPCS	100+SQ
#define	STPS	84+SQ
#define	STXS	153+SQ
#define	STXS1	CP1S+153
#define	STXS2	CP2S+153
#define	STMS	99+SQ
#define	TQES	116+SQ
#define	TQAS	103+SQ
#define	TQLS	87+SQ
#define	TQLS1	CP1S+87
#define	TQLS2	CP2S+87
#define	WEXS	145+SQ
#define	WFSS	146+SQ
#define	WAIS	109+SQ
#define	WAIS1	CP1S+109
#define	WAIS2	CP2S+109
#define	WAIS3	CP3S+109
#define	WAIS4	CP4S+109
#define	WASS	148+SQ
#define	WAVS	110+SQ
#define	WAVS1	CP1S+110
#define	WAVS2	CP2S+110
#define	WAVS3	CP3S+110
#define	WRIS	158+SQ
#define	WRIS1	CP1S+158
#define	WRIS2	CP2S+158
	
//////////////////////////////////////////////////////////////////////////
//
// Yaskawa FSP Drive Master Device Options
//

class CYaskFSPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskFSPDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Axis;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa FSP Drive Master Driver
//

class CYaskawaFSPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaFSPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

	protected:
		// Data
		UINT	m_Axis;

		// Implementation
		void	AddSpaces(void);

		// Helpers

		// Friend
		friend class CYaskawaFSPAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa FSP Drive Address Selection
//

class CYaskawaFSPAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CYaskawaFSPAddrDialog(CYaskawaFSPDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Data Members
		UINT	m_uAllowSpace;
		BOOL	m_fChecked;
		BOOL	m_fIsHeader;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overridables
		void	LoadList(void);
		void	ShowAddress(CAddress Addr);

		// Helpers
		BOOL	AllowSpace(CSpace *pSpace);
		BOOL	SelectList(UINT uTable, UINT uAllow);
		void	SetAllow(void);
		UINT	IsHeader(UINT uCommand);
		BOOL	IsImmediate(UINT uTable);
		BOOL	IsSequential(UINT uTable);
		BOOL	HasParameter(UINT uTable);
		void	UpdateMode(void);
		void	UpdateInfo(void);
		CString	SetInfoText(UINT uSel, UINT uPar);
		void	SetListMode(void);
		void	ResetHelp(void);
	};

// End of File

#endif
