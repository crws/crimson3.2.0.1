
#include "intern.hpp"

#include "nview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// N-View Master Driver
//

// Instantiator

ICommsDriver *	Create_NViewMasterDriver(void)
{
	return New CNViewMasterDriver;
	}

// Constructor

CNViewMasterDriver::CNViewMasterDriver(void)
{
	m_wID		= 0x40AA;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "N-Tron";
	
	m_DriverName	= "N-View Monitor";
	
	m_Version	= "1.00";
	
	m_ShortName	= "N-View";

	m_DevRoot	= "SWITCH";

	AddSpaces();
	}

// Binding Control

UINT CNViewMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CNViewMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_Binding  = bindEthernet;
	Ether.m_TCPCount = 0;
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CNViewMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CNViewMasterDriverOptions);
	}

CLASS CNViewMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CNViewMasterDeviceOptions);
	}

// Address Management

BOOL CNViewMasterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return TRUE;
	}

// Implementation

void CNViewMasterDriver::AddSpaces(void)
{
	AddSpace( New CSpace(	'D',			// Tag to represent table to runtime
				"D",			// Prefix presented to user
				"Data",			// Long name presented to user
				10,			// Radix of numeric portion
				0,			// Minimum value of numeric portion
				64200,			// Maximum value of numeric portion
				addrLongAsLong,		// Type of data and presentation
				addrLongAsLong,		// Same as above in most instances
				5			// Width of numeric portion
				));
	}

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CNViewMasterDriverOptions, CUIItem);

// Constructor

CNViewMasterDriverOptions::CNViewMasterDriverOptions(void)
{
	m_Empty = 0;
	}

// UI Managament

void CNViewMasterDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support	     

BOOL CNViewMasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Empty));
	
	return TRUE;
	}

// Meta Data Creation

void CNViewMasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Empty);
	}

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CNViewMasterDeviceOptions, CUIItem);

// Constructor

CNViewMasterDeviceOptions::CNViewMasterDeviceOptions(void)
{
	m_Mode     = 1;
	m_MACBlock = 0;
	m_MAC4     = 0;
	m_MAC5     = 0;
	m_MAC6	   = 1;
	m_IPAddr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));
	m_Time     = 5000;
	}

// UI Managament

void CNViewMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == L"Mode" ) {

			pWnd->EnableUI(this, L"MACBlock", m_Mode == 1);
			pWnd->EnableUI(this, L"MAC4",     m_Mode == 1);
			pWnd->EnableUI(this, L"MAC5",     m_Mode == 1);
			pWnd->EnableUI(this, L"MAC6",     m_Mode == 1);
			pWnd->EnableUI(this, L"IPAddr",   m_Mode == 2);
			pWnd->EnableUI(this, L"Name",	  m_Mode == 3);
			}
		}
	}

// Download Support	     

BOOL CNViewMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));
	Init.AddByte(BYTE(m_MACBlock));
	Init.AddByte(BYTE(m_MAC4));
	Init.AddByte(BYTE(m_MAC5));
	Init.AddByte(BYTE(m_MAC6));
	Init.AddLong(LONG(m_IPAddr));
	Init.AddText(m_Name);
	Init.AddWord(WORD(m_Time));
	
	return TRUE;
	}

// Meta Data Creation

void CNViewMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddInteger(MACBlock);
	Meta_AddInteger(MAC4);
	Meta_AddInteger(MAC5);
	Meta_AddInteger(MAC6);
	Meta_AddInteger(IPAddr);
	Meta_AddString (Name);
	Meta_AddInteger(Time);
	}

// End of File
