
#include "intern.hpp"

#include "bacnet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBACNetDeviceOptions, CUIItem);

// Constructor

CBACNetDeviceOptions::CBACNetDeviceOptions(void)
{
	m_Device = 7000;
}

// UI Managament

void CBACNetDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

	}
}

// Object Allocation

UINT CBACNetDeviceOptions::ObjectIndex(DWORD ObjectID)
{
	UINT   uSize = m_ObjID.GetCount() / sizeof(DWORD);

	PDWORD pData = PDWORD(m_ObjID.GetPointer());

	UINT n;

	for( n = 0; n < uSize; n++ ) {

		if( pData[n] == ObjectID ) {

			return n + 1;
		}
	}

	m_ObjID.Append(LOBYTE(LOWORD(ObjectID)));
	m_ObjID.Append(HIBYTE(LOWORD(ObjectID)));
	m_ObjID.Append(LOBYTE(HIWORD(ObjectID)));
	m_ObjID.Append(HIBYTE(HIWORD(ObjectID)));

	return n + 1;
}

UINT CBACNetDeviceOptions::GetObjectCount(void)
{
	return m_ObjID.GetCount() / sizeof(DWORD);
}

DWORD CBACNetDeviceOptions::GetObjectID(UINT uObject)
{
	return PDWORD(m_ObjID.GetPointer())[uObject];
}

// Meta Data Creation

void CBACNetDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddBlob(ObjID);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBACNet8023DeviceOptions, CBACNetDeviceOptions);

// Constructor

CBACNet8023DeviceOptions::CBACNet8023DeviceOptions(void)
{
	m_Time1 = 500;
}

// UI Managament

void CBACNet8023DeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

	}

	CBACNetDeviceOptions::OnUIChange(pWnd, pItem, Tag);
}

// Download Support

BOOL CBACNet8023DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Time1));

	Init.AddLong(m_Device);

	Init.AddWord(WORD(m_ObjID.GetCount()));

	PDWORD pData = PDWORD(m_ObjID.GetPointer());

	UINT   uSize = m_ObjID.GetCount() / sizeof(DWORD);

	for( UINT n = 0; n < uSize; n++ ) {

		Init.AddLong(pData[n]);
	}

	return TRUE;
}

// Meta Data Creation

void CBACNet8023DeviceOptions::AddMetaData(void)
{
	CBACNetDeviceOptions::AddMetaData();

	Meta_AddInteger(Time1);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBACNetIPDeviceOptions, CBACNetDeviceOptions);

// Constructor

CBACNetIPDeviceOptions::CBACNetIPDeviceOptions(void)
{
	m_Time1 = 500;
}

// UI Managament

void CBACNetIPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

	}

	CBACNetDeviceOptions::OnUIChange(pWnd, pItem, Tag);
}

// Download Support

BOOL CBACNetIPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Time1));

	Init.AddLong(m_Device);

	Init.AddWord(WORD(m_ObjID.GetCount()));

	PDWORD pData = PDWORD(m_ObjID.GetPointer());

	UINT   uSize = m_ObjID.GetCount() / sizeof(DWORD);

	for( UINT n = 0; n < uSize; n++ ) {

		Init.AddLong(pData[n]);
	}

	return TRUE;
}

// Meta Data Creation

void CBACNetIPDeviceOptions::AddMetaData(void)
{
	CBACNetDeviceOptions::AddMetaData();

	Meta_AddInteger(Time1);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBACNetMSTPDeviceOptions, CBACNetDeviceOptions);

// Constructor

CBACNetMSTPDeviceOptions::CBACNetMSTPDeviceOptions(void)
{
	m_Broke    = 0;

	m_Type     = 0;

	m_TimeMode = 0;

	m_Time1    = 500;
}

// UI Managament

void CBACNetMSTPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "TimeMode" ) {

			pWnd->EnableUI("Time1", m_TimeMode);
		}
	}

	CBACNetDeviceOptions::OnUIChange(pWnd, pItem, Tag);
}

// Download Support

BOOL CBACNetMSTPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Type));

	Init.AddByte(BYTE(m_Broke));

	Init.AddWord(WORD(m_TimeMode ? m_Time1 : 0));

	Init.AddLong(m_Device);

	Init.AddWord(WORD(m_ObjID.GetCount()));

	PDWORD pData = PDWORD(m_ObjID.GetPointer());

	UINT   uSize = m_ObjID.GetCount() / sizeof(DWORD);

	for( UINT n = 0; n < uSize; n++ ) {

		Init.AddLong(pData[n]);
	}

	return TRUE;
}

// Meta Data Creation

void CBACNetMSTPDeviceOptions::AddMetaData(void)
{
	CBACNetDeviceOptions::AddMetaData();

	Meta_AddInteger(Broke);
	Meta_AddInteger(Type);
	Meta_AddInteger(TimeMode);
	Meta_AddInteger(Time1);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBACNetMSTPDriverOptions, CUIItem);

// Constructor

CBACNetMSTPDriverOptions::CBACNetMSTPDriverOptions(void)
{
	m_ThisDrop = 10;

	m_LastDrop = 127;

	m_Optim1   = 0;

	m_Optim2   = 0;

	m_TxFast   = 0;
}

// UI Managament

void CBACNetMSTPDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

	}

	CUIItem::OnUIChange(pWnd, pItem, Tag);
}

// Download Support

BOOL CBACNetMSTPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_ThisDrop));

	Init.AddByte(BYTE(m_LastDrop));

	Init.AddByte(BYTE(m_Optim1));

	Init.AddByte(BYTE(m_Optim2));

	Init.AddByte(BYTE(m_TxFast));

	BOOL fHwDelay = GetDatabase()->HasFlag(L"Canyon");

	Init.AddByte(BYTE(fHwDelay));

	return TRUE;
}

// Meta Data Creation

void CBACNetMSTPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(ThisDrop);
	Meta_AddInteger(LastDrop);
	Meta_AddInteger(Optim1);
	Meta_AddInteger(Optim2);
	Meta_AddInteger(TxFast);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Driver
//

// Constructor

CBACNetDriver::CBACNetDriver(void)
{
	m_Manufacturer = "BACnet";

	m_pMap          = New CBACNetMapping;
}

// Destructor

CBACNetDriver::~CBACNetDriver(void)
{
	delete m_pMap;
}

// Address Management

BOOL CBACNetDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	UINT uDot = Text.Find('.');

	if( uDot < NOTHING ) {

		CString Obj  = Text.Left(uDot);

		CString Prop = Text.Mid(uDot+1);

		UINT    uNum = Text.FindOne(TEXT("0123456789"));

		if( uNum < NOTHING ) {

			CString Abbr       = Obj.Left(uNum);

			CString Inst       = Obj.Mid(uNum);

			BYTE    ObjectType = m_pMap->GetObjectType(Abbr);

			DWORD   ObjectInst = tatoi(Inst);

			DWORD   ObjectID   = ((ObjectType << 22) | ObjectInst);

			UINT    uDot       = Prop.Find('.');

			BYTE    Priority   = 0x00;

			BOOL   fRelinquish = FALSE;

			if( uDot < NOTHING ) {

				if( tolower(Prop[uDot+1]) == 'p' ) {

					Priority = BYTE(tatoi(Prop.Mid(uDot+2)));

					if( Priority > 16 ) {

						Error.Set("Invalid priority level.", 0);

						return FALSE;
					}

					CString Rel = Prop.Mid(uDot+1);

					UINT   uRel = Rel.Find('.');

					if( uRel < NOTHING ) {

						Rel = Rel.Mid(uRel+1);

						if( tolower(Rel[0]) == 'r' ) {

							fRelinquish = TRUE;
						}
					}

					Prop = Prop.Left(uDot);
				}
				else {
					Error.Set("Invalid priority encoding.", 0);

					return FALSE;
				}
			}

			BYTE PropID = m_pMap->GetPropID(Prop);

			BYTE TypeID = m_pMap->GetPropType(ObjectType, PropID);

			////////

			if( ObjectType == 0xFF ) {

				Error.Set("Invalid object type.", 0);

				return FALSE;
			}

			if( PropID == 0xFF ) {

				Error.Set("Invalid property name.", 0);

				return FALSE;
			}

			if( TypeID == 0xFF ) {

				Error.Set("Property not supported by object.", 0);

				return FALSE;
			}

			if( fRelinquish ) {

				SetRelinquish(TypeID);
			}

			EncodePriority(ObjectType, PropID, Priority);

			Encode(Addr, pConfig, ObjectID, PropID, TypeID);

			return TRUE;
		}
	}

	Error.Set("Invalid BACNet address.", 0);

	return FALSE;
}

BOOL CBACNetDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	DWORD ObjectID = 0;

	BYTE  PropID   = 0;

	BYTE  TypeID   = 0;

	BYTE  Priority = 0xFF;

	if( Decode(Addr, pConfig, ObjectID, PropID, TypeID) ) {

		BYTE  ObjectType = BYTE(ObjectID >> 22);

		DWORD ObjectInst = ObjectID & 0x03FFFFF;

		if( DecodePriority(ObjectType, PropID, Priority) ) {

			if( Priority ) {

				Text.Printf("%s%u.%s.p%u",
					    m_pMap->GetObjectAbbr(ObjectType),
					    ObjectInst,
					    m_pMap->GetPropName(PropID),
					    Priority
				);

				if( IsRelinquish(TypeID) ) {

					Text += ".r";
				}

				return TRUE;
			}
		}

		Text.Printf("%s%u.%s",
			    m_pMap->GetObjectAbbr(ObjectType),
			    ObjectInst,
			    m_pMap->GetPropName(PropID)
		);

		return TRUE;
	}

	return FALSE;
}

BOOL CBACNetDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CBACNetDialog Dlg(ThisObject, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
}

BOOL CBACNetDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
}

BOOL CBACNetDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
}

// Implementation

BOOL CBACNetDriver::Encode(CAddress &Addr, CItem *pConfig, DWORD ObjectID, BYTE PropID, BYTE TypeID)
{
	CBACNetDeviceOptions *pDev = (CBACNetDeviceOptions *) pConfig;

	if( pDev ) {

		UINT uObject    = pDev->ObjectIndex(ObjectID);

		Addr.a.m_Type   = (TypeID == 4) ? addrRealAsReal : addrLongAsLong;

		Addr.a.m_Offset = MAKEWORD(PropID, LOBYTE(uObject));

		Addr.a.m_Extra  = (HIBYTE(uObject) & 0x0F);

		Addr.a.m_Table  = addrNamed + TypeID;

		return TRUE;
	}

	return FALSE;
}

BOOL CBACNetDriver::Decode(CAddress const &Addr, CItem *pConfig, DWORD &ObjectID, BYTE &PropID, BYTE &TypeID)
{
	CBACNetDeviceOptions *pDev = (CBACNetDeviceOptions *) pConfig;

	if( pDev ) {

		UINT uObject = MAKEWORD(HIBYTE(Addr.a.m_Offset), LOBYTE(Addr.a.m_Extra)) - 1;

		if( uObject < pDev->GetObjectCount() ) {

			ObjectID = pDev->GetObjectID(uObject);

			PropID   = LOBYTE(Addr.a.m_Offset);

			TypeID   = LOBYTE(Addr.a.m_Table & 0x0F);

			return TRUE;
		}
	}

	return FALSE;
}

// Priority Helpers

BOOL CBACNetDriver::DecodePriority(BYTE ObjectType, BYTE &PropID, BYTE &Priority)
{
	if( HasPriority(ObjectType) ) {

		if( PropID >= 230 && PropID <= 246 ) {

			Priority = BYTE(PropID - 230);

			PropID   = 85;

			return TRUE;
		}

		if( PropID == 85 ) {

			Priority = 0;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CBACNetDriver::EncodePriority(BYTE ObjectType, BYTE &PropID, BYTE &Priority)
{
	if( HasPriority(ObjectType) ) {

		if( PropID == 85 ) {

			PropID = BYTE(230 + Priority);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CBACNetDriver::HasPriority(BYTE ObjectType)
{
	switch( ObjectType ) {

		case 1:
		case 2:
		case 4:
		case 5:
		case 14:
		case 19:
			return TRUE;
	}

	return FALSE;
}

BYTE CBACNetDriver::GetRelinquish(void)
{
	return m_pMap->GetTypeID("relinquish");
}

BOOL CBACNetDriver::SetRelinquish(BYTE &TypeID)
{
	TypeID = GetRelinquish();

	return TRUE;
}

BOOL CBACNetDriver::IsRelinquish(BYTE TypeID)
{
	return TypeID == GetRelinquish();
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Master
//

// Instantiator

ICommsDriver * Create_BACNet8023Driver(void)
{
	return New CBACNet8023;
}

// Constructor

CBACNet8023::CBACNet8023(void)
{
	m_wID		= 0x4029;

	m_uType		= driverMaster;

	m_DriverName	= "802.3 Master";

	m_Version	= "1.00";

	m_ShortName	= "BACnet 802.3";

	m_DevRoot	= "DEV";
}

// Binding Control

UINT CBACNet8023::GetBinding(void)
{
	return bindEthernet;
}

void CBACNet8023::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;

	Ether.m_UDPCount = 0;
}

// Configuration

CLASS CBACNet8023::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBACNet8023DeviceOptions);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Master
//

// Instantiator

ICommsDriver * Create_BACNetIPDriver(void)
{
	return New CBACNetIP;
}

// Constructor

CBACNetIP::CBACNetIP(void)
{
	m_wID		= 0x402B;

	m_uType		= driverMaster;

	m_DriverName	= "UDP/IP Master";

	m_Version	= "1.00";

	m_ShortName	= "BACnet UDP/IP";

	m_DevRoot	= "DEV";
}

// Binding Control

UINT CBACNetIP::GetBinding(void)
{
	return bindEthernet;
}

void CBACNetIP::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;

	Ether.m_UDPCount = 1;
}

// Configuration

CLASS CBACNetIP::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBACNetIPDeviceOptions);
}

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Master
//

// Instantiator

ICommsDriver * Create_BACNetMSTPDriver(void)
{
	return New CBACNetMSTP;
}

// Constructor

CBACNetMSTP::CBACNetMSTP(void)
{
	m_wID		= 0x402A;

	m_uType		= driverMaster;

	m_DriverName	= "MS/TP Master";

	m_Version	= "1.01";

	m_ShortName	= "BACnet";

	m_DevRoot	= "DEV";
}

// Binding Control

UINT CBACNetMSTP::GetBinding(void)
{
	return bindRawSerial;
}

void CBACNetMSTP::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 76800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
}

// Configuration

CLASS CBACNetMSTP::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBACNetMSTPDeviceOptions);
}

CLASS CBACNetMSTP::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBACNetMSTPDriverOptions);
}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBACNetDialog, CStdDialog);

// Constructor

CBACNetDialog::CBACNetDialog(CBACNetDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pDriver = &Driver;

	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;

	SetName(TEXT("BACNetDlg"));
}

// Message Map

AfxMessageMap(CBACNetDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

		AfxDispatchCommand(IDOK, OnOkay)
		AfxDispatchCommand(204, OnClick)

		AfxDispatchNotify(200, CBN_SELCHANGE, OnSelChange)
		AfxDispatchNotify(202, CBN_SELCHANGE, OnSelChange)
		AfxDispatchNotify(203, CBN_SELCHANGE, OnSelChange)

		AfxMessageEnd(CBACNetDialog)
};

// Message Handlers

BOOL CBACNetDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadObjectList();

	return FALSE;
}

// Notification Handlers

void CBACNetDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 200 ) {

		BYTE ObjectType = BYTE(((CComboBox &) Wnd).GetCurSelData());

		if( ObjectType == 0xFF ) {

			CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(201);

			CComboBox &Prop = (CComboBox &) GetDlgItem(202);

			CComboBox &Prio = (CComboBox &) GetDlgItem(203);

			CButton  &Check = (CButton   &) GetDlgItem(204);

			CStatic   &Type = (CStatic   &) GetDlgItem(300);

			Edit.SetWindowText("");

			Edit.EnableWindow(FALSE);

			Prop.ResetContent();

			Prop.EnableWindow(FALSE);

			Prio.ResetContent();

			Prio.EnableWindow(FALSE);

			Check.ShowWindow(SW_HIDE);

			Type.SetWindowText("");
		}
		else {
			CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(201);

			CComboBox &Prop = (CComboBox &) GetDlgItem(202);

			if( !Edit.GetWindowText() ) {

				DWORD ObjectID = 0;

				BYTE  PropID   = 0;

				BYTE  TypeID   = 0;

				if( m_pDriver->Decode(*m_pAddr, m_pConfig, ObjectID, PropID, TypeID) ) {

					Edit.SetWindowText(CPrintf("%u", ObjectID & 0x3FFFFF));
				}
				else
					Edit.SetWindowText("1");
			}

			Edit.EnableWindow(TRUE);

			Prop.EnableWindow(TRUE);

			LoadPropertyList(ObjectType);
		}
	}

	if( uID == 202 ) {

		ShowPropType((CComboBox &) Wnd);

		ShowPriority((CComboBox &) Wnd);
	}

	if( uID == 203 ) {

		ShowRelinquish((CComboBox &) Wnd);
	}
}

// Command Handlers

BOOL CBACNetDialog::OnOkay(UINT uID)
{
	BYTE ObjectType = BYTE(((CComboBox &) GetDlgItem(200)).GetCurSelData());

	if( ObjectType == 0xFF ) {

		m_pAddr->m_Ref = 0;
	}
	else {
		DWORD ObjectInst = tatoi(GetDlgItem(201).GetWindowText());

		DWORD ObjectID   = DWORD((ObjectType << 22) | ObjectInst);

		BYTE  PropID     = LOBYTE(((CComboBox &) GetDlgItem(202)).GetCurSelData());

		BYTE  TypeID     = FindTypeID((CComboBox &) GetDlgItem(202));

		BYTE  Priority   = LOBYTE(((CComboBox &) GetDlgItem(203)).GetCurSelData());

		m_pDriver->EncodePriority(ObjectType, PropID, Priority);

		m_pDriver->Encode(*m_pAddr, m_pConfig, ObjectID, PropID, TypeID);
	}

	EndDialog(TRUE);

	return TRUE;
}

BOOL CBACNetDialog::OnClick(UINT uID)
{
	if( uID == 204 ) {

		CComboBox &Box = (CComboBox &) GetDlgItem(202);

		ShowPropType(Box);
	}

	return TRUE;
}

// Implementation

void CBACNetDialog::LoadObjectList(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(200);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	for( UINT n = 0;; n++ ) {

		BYTE Type = m_pDriver->m_pMap->EnumObjectTypes(n);

		if( Type == 0xFF ) {

			break;
		}

		CString Name = m_pDriver->m_pMap->GetObjectName(Type);

		Box.AddString(Name, DWORD(Type));
	}

	Box.AddString(TEXT("<none>"), DWORD(0xFF));

	DWORD ObjectID = 0;

	BYTE  PropID   = 0;

	BYTE  TypeID   = 0;

	if( m_pDriver->Decode(*m_pAddr, m_pConfig, ObjectID, PropID, TypeID) ) {

		Box.SelectData(DWORD(ObjectID >> 22));
	}
	else
		Box.SelectData(DWORD(0xFF));

	Box.SetRedraw(TRUE);

	Box.Invalidate(TRUE);

	OnSelChange(200, Box);
}

void CBACNetDialog::LoadPropertyList(BYTE ObjectType)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(202);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	DWORD PV = NOTHING;

	for( UINT n = 0;; n++ ) {

		WORD Data = m_pDriver->m_pMap->EnumObjectProps(ObjectType, n);

		if( Data == 0xFFFF ) {

			break;
		}

		if( HIBYTE(Data) == 0xFF ) {

			continue;
		}

		CString Name = m_pDriver->m_pMap->GetPropName(LOBYTE(Data));

		if( LOBYTE(Data) == 85 ) {

			PV = Data;
		}

		Box.AddString(Name, DWORD(Data));
	}

	DWORD ObjectID = 0;

	BYTE  PropID   = 0;

	BYTE  TypeID   = 0;

	BYTE  Priority = 0xFF;

	BOOL  fOkay    = FALSE;

	if( m_pDriver->Decode(*m_pAddr, m_pConfig, ObjectID, PropID, TypeID) ) {

		m_pDriver->DecodePriority(ObjectType, PropID, Priority);

		if( Box.SelectData(MAKEWORD(PropID, m_pDriver->IsRelinquish(TypeID) ? 4 : TypeID)) ) {

			fOkay = TRUE;
		}
	}

	if( !fOkay ) {

		if( PV == NOTHING ) {

			Box.SetCurSel(0);
		}
		else {
			Box.SelectData(PV);

			Priority = 0;
		}
	}

	Box.SetRedraw(TRUE);

	Box.Invalidate(TRUE);

	ShowPriority(Priority, TypeID);

	ShowPropType(Box);
}

void CBACNetDialog::LoadPriority(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(203);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	Box.AddString("Level 0 - Device Default", DWORD(0));
	Box.AddString("Level 1 - Manual Life Safety", DWORD(1));
	Box.AddString("Level 2 - Automatic Life Safety", DWORD(2));
	Box.AddString("Level 3", DWORD(3));
	Box.AddString("Level 4", DWORD(4));
	Box.AddString("Level 5 - Critical Equipment Control", DWORD(5));
	Box.AddString("Level 6 - Minimum On/Off", DWORD(6));
	Box.AddString("Level 7", DWORD(7));
	Box.AddString("Level 8 - Manual Operator", DWORD(8));
	Box.AddString("Level 9", DWORD(9));
	Box.AddString("Level 10", DWORD(10));
	Box.AddString("Level 11", DWORD(11));
	Box.AddString("Level 12", DWORD(12));
	Box.AddString("Level 13", DWORD(13));
	Box.AddString("Level 14", DWORD(14));
	Box.AddString("Level 15", DWORD(15));
	Box.AddString("Level 16", DWORD(16));

	Box.SetRedraw(TRUE);

	Box.Invalidate(TRUE);
}

void CBACNetDialog::ShowPropType(CComboBox &Box)
{
	ShowPropType(FindTypeID(Box));
}

void CBACNetDialog::ShowPropType(BYTE TypeID)
{
	CPrintf Info = CPrintf("BACnet type is %s.", m_pDriver->m_pMap->GetTypeName(TypeID));

	GetDlgItem(300).SetWindowText(Info);
}

void CBACNetDialog::ShowPriority(CComboBox &Box)
{
	BYTE PropID     = LOBYTE(Box.GetCurSelData());

	BYTE ObjectType = BYTE(((CComboBox &) GetDlgItem(200)).GetCurSelData());

	BYTE Priority   = 0xFF;

	m_pDriver->DecodePriority(ObjectType, PropID, Priority);

	ShowPriority(Priority, FindTypeID(Box));
}

void CBACNetDialog::ShowPriority(BYTE Priority, BYTE TypeID)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(203);

	CButton &Check = (CButton   &) GetDlgItem(204);

	if( Priority == 0xFF ) {

		Box.ResetContent();

		Box.EnableWindow(FALSE);

		Check.SetCheck(FALSE);

		Check.ShowWindow(SW_HIDE);
	}
	else {
		LoadPriority();

		Box.SelectData(Priority);

		Box.EnableWindow(TRUE);

		ShowRelinquish(Priority, TypeID);
	}
}

void CBACNetDialog::ShowRelinquish(CComboBox &Box)
{
	CComboBox &Type = (CComboBox &) GetDlgItem(202);

	ShowRelinquish(BYTE(Box.GetCurSel()), FindTypeID(Type));
}

void CBACNetDialog::ShowRelinquish(BYTE Priority, BYTE TypeID)
{
	CButton &Check = (CButton   &) GetDlgItem(204);

	Check.ShowWindow(SW_SHOW);

	if( Priority > 0 ) {

		Check.SetCheck(m_pDriver->IsRelinquish(TypeID));

		Check.EnableWindow(TRUE);

		return;
	}

	Check.SetCheck(FALSE);

	Check.EnableWindow(FALSE);
}

// Helpers

BYTE CBACNetDialog::FindTypeID(CComboBox &Box)
{
	CButton &Check = (CButton &) GetDlgItem(204);

	return Check.IsChecked() ? m_pDriver->GetRelinquish() : HIBYTE(Box.GetCurSelData());
}

// End of File
