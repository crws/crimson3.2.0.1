
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MITFX_HPP
	
#define	INCLUDE_MITFX_HPP

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX Series
//

class CMitsubFxDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMitsubFxDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Helper
		BOOL CheckAlignment(CSpace *pSpace);
	
	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX2N Series TCP/IP Device Options
//

class CMitsFX2NMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMitsFX2NMasterTCPDeviceOptions(void);

		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
	
	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX2N Series TCP/IP Master
//

class CMitsFX2NMasterTCPDriver : public CMitsubFxDriver
{
	public:
		// Constructor
		CMitsFX2NMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};



// End of File

#endif
