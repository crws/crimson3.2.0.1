
#include "intern.hpp"

#include "imoloadg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IMO G Series Loader Port Driver
//

// Instantiator

ICommsDriver *	Create_IMOGLoaderPortDriver(void)
{
	return New CIMOGLoaderPortDriver;
	}

// Constructor

CIMOGLoaderPortDriver::CIMOGLoaderPortDriver(void)
{
	m_wID		= 0x4006;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "IMO";
	
	m_DriverName	= "G Series Loader Port";
	
	m_Version	= "1.00";
	
	m_ShortName	= "G Series";

	AddSpaces();
	}

// Binding Control

UINT	CIMOGLoaderPortDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CIMOGLoaderPortDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CIMOGLoaderPortDriver::GetDeviceConfig(void)
{
	return NULL;
	}


// Address Management

BOOL CIMOGLoaderPortDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CIMOGAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CIMOGLoaderPortDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT    uTable  = pSpace->m_uTable;

	UINT	uType = pSpace->m_uType;

	UINT    uOffset = 0;

	UINT    fFail   = 0;

	PTXT pError = NULL;

	if( IsIO( uTable ) ) {

		fFail = ParseAddress( Text, &uOffset, uType );
		}

	else {
		UINT uPos = Text.Find('.');

		CString Offset = Text.Left( uPos );

		uOffset = tstrtoul( Offset, &pError, 10 );

		fFail   = ( pError && pError[0] ) ? 1 : 0;
		}

	if( !fFail ) {

		fFail = pSpace->IsOutOfRange(uOffset) ? 2 : 0;
		}

	switch ( fFail ) {

		case 1:

			Error.Set( CString(IDS_ERROR_ADDR),
				   0
				   );
		
			return FALSE;

		case 2:

			Error.Set( CString(IDS_ERROR_OFFSETRANGE),
				   0
				   );

			return FALSE;			
		}

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uOffset;
	
	Addr.a.m_Table	= uTable;
	
	Addr.a.m_Type	= uType;

	return TRUE;
	}

BOOL CIMOGLoaderPortDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpace *pSpace = GetSpace(Addr);

		if( pSpace ) {

			if( IsIO(Addr.a.m_Table) ) {

				UINT uOffset = Addr.m_Ref & 0xFFF;

				UINT uV1 = 0;
				UINT uV2 = 0;
				UINT uV3 = 0;

				switch ( Addr.a.m_Type ) {

					case addrBitAsBit:
						uV1 = uOffset/512;
						uV2 = (uOffset%512)/64;
						uV3 = uOffset%64;
						break;

					case addrByteAsByte:
						uV1 = uOffset/64;
						uV2 = (uOffset%64)/8;
						uV3 = uOffset%8;
						break;

					case addrWordAsWord:
						uV1 = uOffset/32;
						uV2 = (uOffset%32)/4;
						uV3 = uOffset%4;
						break;

					case addrLongAsLong:
						uV1 = uOffset/16;
						uV2 = (uOffset%16)/2;
						uV3 = uOffset%2;
						break;
					}

				Text.Printf( "%s%1.1d.%1.1d.%2.2d",
				     pSpace->m_Prefix,
					uV1,
					uV2,
					uV3
					);
				}

			else {

				Text.Printf( "%s%s",   
					pSpace->m_Prefix, 
					pSpace->GetValueAsText(Addr.a.m_Offset)
					);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

void CIMOGLoaderPortDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"MX",	"M Bits",		10,  0,	65535,	addrBitAsBit));

	AddSpace(New CSpace(2,	"IX",	"I Bits",		10,  0,	1023,	addrBitAsBit));

	AddSpace(New CSpace(3,	"QX",	"Q Bits",		10,  0,	1023,	addrBitAsBit));

	AddSpace(New CSpace(4,	"MB",	"M Bytes",		10,  0,	65528,	addrByteAsByte));

	AddSpace(New CSpace(5,	"IB",	"I Bytes",		10,  0,	127,	addrByteAsByte));

	AddSpace(New CSpace(6,	"QB",	"Q Bytes",		10,  0,	127,	addrByteAsByte));

	AddSpace(New CSpace(7,	"MW",	"M Words",		10,  0,	65520,	addrWordAsWord));

	AddSpace(New CSpace(8,	"IW",	"I Words",		10,  0,	63,	addrWordAsWord));

	AddSpace(New CSpace(9,	"QW",	"Q Words",		10,  0,	63,	addrWordAsWord));

	AddSpace(New CSpace(10,	"MD",	"M Double Words",	10,  0,	65504,	addrLongAsLong));

	AddSpace(New CSpace(11,	"ID",	"I Double Words",	10,  0,	31,	addrLongAsLong));

	AddSpace(New CSpace(12,	"QD",	"Q Double Words",	10,  0,	31,	addrLongAsLong));

//	AddSpace(New CSpace(17,	"ER",	"Comms Error",		10,  0, 0,	addrLongAsLong));
	}

// Address Checking

UINT CIMOGLoaderPortDriver::ParseAddress( CString Text, UINT * pOffset, UINT uType )
{
 	UINT uOffset = 0;
	UINT uValue1 = 0;
	UINT uValue2 = 0;
	UINT uValue3 = 0;
	UINT uError;
	UINT uBitMax = 1;

	UINT uPos1;
	UINT uPos2;
	UINT uPos3;

	if( (uPos1 = Text.Find('.')) == NOTHING ) {

		return 1;
		}

	uError = GetValue( Text.Left(uPos1), &uValue1, 1 );

	if( uError ) {

		return uError;
		}

	if( (uPos2 = Text.Find('.', uPos1+1)) == NOTHING ) {

		return 1;
		}

	uError = GetValue( Text.Mid( uPos1+1, uPos2-uPos1-1 ), &uValue2, 7 );

	if( uError ) {

		return uError;
		}

	if( (uPos3 = Text.Find('.', uPos2+1)) == NOTHING ) {

		uPos3 = Text.GetLength();
		}

	switch ( uType ) {

		case addrBitAsBit:
			uBitMax = 63;
			break;

		case addrByteAsByte:
			uBitMax = 7;
			break;

		case addrWordAsWord:
			uBitMax = 3;
			break;

		case addrLongAsLong:
			uBitMax = 1;
			break;

		default:
			return 1;
		}

	uError = GetValue( Text.Mid( uPos2+1, uPos3-uPos2-1 ), &uValue3, uBitMax );

	if( uError ) {

		return uError;
		}

	switch ( uType ) {

		case addrBitAsBit:
			uOffset = (512*uValue1) + (64*uValue2) + uValue3;
			break;

		case addrByteAsByte:
			uOffset = (64*uValue1) + (8*uValue2) + uValue3;
			break;

		case addrWordAsWord:
			uOffset = (32*uValue1) + (4*uValue2) + uValue3;
			break;

		case addrLongAsLong:
			uOffset = (16*uValue1) + (2*uValue2) + uValue3;
			break;

		default:
			break;
		}

	*pOffset = uOffset;

	return 0;
	}

BOOL CIMOGLoaderPortDriver::IsIO(UINT uTable)
{
	switch ( uTable ) {

		case 2:
		case 3:
		case 5:
		case 6:
		case 8:
		case 9:
		case 11:
		case 12:  // address represented by d.d.dd

			return TRUE;
		}

	return FALSE;
	}

UINT CIMOGLoaderPortDriver::GetValue( CString t, UINT * p, UINT uMax )
{
	PTXT pError = NULL;
	
	UINT u;

	u = tstrtoul(t, &pError, 10 );

	if( pError && pError[0] ){

		return 1;
		}

	if( u > uMax ) {

		return 2;
		}

	*p = u;
	
	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// IMO G Series Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CIMOGAddrDialog, CStdAddrDialog);
		
// Constructor

CIMOGAddrDialog::CIMOGAddrDialog(CIMOGLoaderPortDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	}

// Message Map

AfxMessageMap(CIMOGAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(CIMOGAddrDialog)
	};

BOOL CIMOGAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CStdAddrDialog::LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;
	
	if( !m_fPart ) {

		CStdAddrDialog::SetCaption();

		CStdAddrDialog::FindSpace();

		CStdAddrDialog::LoadList();

		OnSpaceChange( 1001, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			CStdAddrDialog::SetAddressFocus();

			return FALSE;
			}

		return TRUE;
		}
	else {
		CStdAddrDialog::FindSpace();

		CStdAddrDialog::LoadType();
		
		ShowAddress(Addr);

		CStdAddrDialog::SetAddressFocus();

		return FALSE;
		}
	}

// Overridables


void CIMOGAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	Text = Text.Mid(m_pSpace->m_Prefix.GetLength());

	CStdAddrDialog::SetAddressText(Text);
	}

void CIMOGAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		CStdAddrDialog::LoadType();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			Addr.a.m_Type = CStdAddrDialog::GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		CStdAddrDialog::ClearType();

		CStdAddrDialog::ClearAddress();

		CStdAddrDialog::ClearDetails();
		}
	}

// End of File
