
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MITA_HPP
	
#define	INCLUDE_MITA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series
//

class CMitsubADriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMitsubADriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		
		// Configuration	
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series TCP/IP Master
//

class CMitsAQMasterTCPDriver : public CMitsubADriver
{
	public:
		// Constructor
		CMitsAQMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series TCP/IP Master
//

class CMitsAQMasterUDPDriver : public CMitsAQMasterTCPDriver
{
	public:
		// Constructor
		CMitsAQMasterUDPDriver(void);

		// Binding Control
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

 
//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series Driver Options
//

class CMitsubAQMasterDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMitsubAQMasterDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		BOOL m_fCRC;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series Device Options
//

class CMitsubAQMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMitsubAQMasterDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
		UINT m_Melsec;
		UINT m_Net;
		UINT m_PC;
		UINT m_Cpu;
		

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series TCP/IP Device Options
//

class CMitsAQMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMitsAQMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Code;
		UINT m_Monitor;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_PC;
		UINT m_Melsec;
		UINT m_Cpu;

	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series UDP/IP Device Options
//

class CMitsAQMasterUDPDeviceOptions : public CMitsAQMasterTCPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

	};


//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series Address Selection Dialog
//

class CMitsubADialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CMitsubADialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pItem, BOOL fPart);

	protected:
		// Overridables
		BOOL AllowType(UINT uType);
		BOOL AllowSpace(CSpace *pSpace);

		// Data Members
		UINT m_uMelsec;
		BOOL m_fEthernet;

	};

// End of File

#endif
