
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MAGUIREMLAN_HPP
	
#define	INCLUDE_MAGUIREMLAN_HPP

//////////////////////////////////////////////////////////////////////////
//
// MaguireMLAN Comms Driver
//

#define	AN	addrNamed
#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong

// Space Definitions
#define	SPGBI	1
#define	SPCWW	2
#define	SPCWL	3
#define	SPLST	4
#define	SPPAR0	5
#define	SPPAR1	6
#define	SPRATE	7
#define	SPSET	8
#define	SPSKEY	9
#define	SPSTAR	10
#define	SPSTAT	11
#define	SPTAG	12
#define	SPTAR	13
#define	SPTOTR	14
#define	SPTOTS	15
#define	SPTYP	16
#define	SP_VER	17
#define	SPYST	18

class CMaguireMLANDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMaguireMLANDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Management

		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers

		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		UINT	ParsePar (CString Text, BOOL fIsPar0);
		CString	ExpandPar(UINT uOffset, BOOL fIsPar0);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Address Selection
//

class CMaguireMLANAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CMaguireMLANAddrDialog(CMaguireMLANDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Destructor
		~CMaguireMLANAddrDialog(void);
		                
	protected:
		// Data Members
		CMaguireMLANDriver * m_pDriverData;

		BOOL	m_fPart;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overrideables
		void	ShowAddress(CAddress Addr);

		// Helpers;
	};

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN TCP/IP Driver Options
//

class CMagMLANTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMagMLANTCPDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Unit;
		UINT m_Port;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN TCP/IP Driver
//

class CMaguireMLANTCPDriver : public CMaguireMLANDriver
{
	public:
		// Constructor
		CMaguireMLANTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration

		CLASS GetDeviceConfig(void);
	}; 

// End of File

#endif
