
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SRTPMTCP_HPP
	
#define	INCLUDE_SRTPMTCP_HPP

#include "snp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GE TCP/IP Master via SRTP Driver Options
//

class CGeSrtpMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGeSrtpMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Addr;
		UINT    m_Addr2;
		UINT	m_Port;
		//CString m_Unit;
		UINT	m_Keep;
		UINT	m_Ping;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// GE TCP/IP Master via SRTP Driver
//

class CGeSrtpMasterTCPDriver : public CSNPDriver
{
	public:
		// Constructor
		CGeSrtpMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

// End of File

#endif
