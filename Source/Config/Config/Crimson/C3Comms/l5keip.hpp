
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_L5KEIP_HPP
	
#define	INCLUDE_L5KEIP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "l5kitem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kDeviceOptions;
class CABL5kDriver;
class CABL5kDialog;

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

class CABL5kDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kDeviceOptions(void);

		// Address Management
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL DoExpandAddress(CString &Text, CAddress const &Addr);
		BOOL DoSelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart, BOOL fSelect);
		BOOL DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
		
		// Dialog Support
		CString GetDeviceName(void);

		// Mapped Address Support		
		void AddAtomicTag(CABL5kAtomicTag *pTag);
		void MapAtomicTag(CABL5kAtomicTag *pTag);

		// L5k Support
		BOOL LoadFromFile(CError &Error, CFilename const &File);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		
		// Persistance
		void Save(CTreeFile &Tree);
		void Load(CTreeFile &Tree);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT		  m_Device;
		DWORD		  m_Address;
		UINT		  m_Slot;
		UINT		  m_Time;
		UINT              m_Push;
		CABL5kTypeList  * m_pTypes;
		CABL5kTagList   * m_pNames;
		CString		  m_Controller;
				
	protected:
		// Data
		CArray <CString> m_NamedList;
		CArray <CString> m_TableList;
		CArray <UINT>	 m_NamedDict;
		CArray <UINT>	 m_TableDict;
		UINT	         m_uTable;
		UINT		 m_uNamed;

		// Meta Data Creation
		void AddMetaData(void);

		// Property Save Filter
		BOOL SaveProp(CString Tag) const;

		// Dictionary of Names
		void LoadMapping(CTreeFile &Tree, CArray <CString> &List, CArray <UINT> &Dict);
		void LoadNames(CTreeFile &Tree, CArray <CString> &List);
		void LoadEntry(CTreeFile &Tree, CArray <UINT> &List);

		void SaveMapping(CTreeFile &Tree, CArray <CString> &List, CArray <UINT> &Dict);
		void SaveNames(CTreeFile &Tree, CArray <CString> &List);
		void SaveEntry(CTreeFile &Tree, CArray <UINT> &List);

		void SaveTags(CStringArray &Tags, CArray <CString> &List, CArray <UINT> &Dict);
		
		void BuildNameLists(CABL5kList &List);

		void CheckAlignment(CAddress &Addr);

		// Implementation
		void ThrowError(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver
//

class CABL5kDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CABL5kDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class CABL5kDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CABL5kDialog(CABL5kDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart, BOOL fSelect);

		// Destructor
		~CABL5kDialog(void);

	protected:
		// Data
		CABL5kDriver	     * m_pDriver;
		CAddress	     * m_pAddr;
		CABL5kDeviceOptions  * m_pConfig;
		BOOL		       m_fPart;
		CImageList             m_Images;
		BOOL		       m_fSelect;
		BOOL		       m_fCreate;
		BOOL		       m_fLoad;
		HTREEITEM              m_hRoot;
		CABL5kItem 	     * m_pSelect;
		CABL5kTagList	     * m_pNames;
		CABL5kTypeList	     * m_pTypes;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnImport(UINT uID);
		BOOL OnCreate(UINT uID);
		
		// Tag Loading
		void LoadTagNames(void);
		void LoadRoot(CTreeView &Tree);
		void LoadTree(CTreeView &Tree);
		void LoadTags(CTreeView &Tree, HTREEITEM hRoot, CABL5kList *pList);
		void LoadTag(CTreeView &Tree, HTREEITEM hRoot, CABL5kItem *pItem);

		// Data Type Loading
		void LoadDataTypes(void);
		void LoadDataTypes(CComboBox &Combo, CABL5kList *pList);

		// Implmentation
		void SetCaption(void);
		void SetAddressText (CString Text);
		void SetOffsetText  (CString const &Text);
		void SetAddrTypeText(CString const &Text);
		void SetMinimumText (CString const &Text);
		void SetMaximumText (CString const &Text);
		void SetDataTypeText(CString const &Text);
		void SetDataDescText(CString const &Text);
		
		CString GetAddressText(void);	
		CString GetCreateType(void);
		CABL5kItem * CreateTag(CError &Error, CString const &Name, CString const &Type);		
		BOOL IsDown(UINT uCode);

		void EnableOkay(BOOL fEnable);
		void EnableCancel(BOOL fEnable);
		void EnableImport(BOOL fEnable);
		
		void HideTableIndices(void);
		void ShowTableIndices(UINT uIndex, CString Type);
		void ShowTableIndices(UINT uIndex);

		// Debug
		void ClearStatus(void);
		void ShowStatus(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Decoding Macros
//

#define IsTable(Addr)	((Addr).a.m_Table  < addrNamed)

#define IsNamed(Addr)	((Addr).a.m_Table == addrNamed)

#define GetTable(Addr)	((Addr).a.m_Table)

#define GetOffset(Addr)	((Addr).a.m_Offset)

#define GetIndex(Addr)	(IsTable(Addr) ? GetTable(Addr) : GetOffset(Addr))

// End of File

#endif
