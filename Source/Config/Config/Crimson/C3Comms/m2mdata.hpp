//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_M2MSERIAL_HPP
	
#define	INCLUDE_M2MSERIAL_HPP

class CM2MDataSerialDriverOptions;
class CM2MDataTCPDriverOptions;
class CM2MDataSerialDriver;
class CM2MDataTCPDriver;
class CM2MDataAddrDialog;
class CM2MShowListDialog;

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp TCP/IP Slave Driver Options
//

#define	NO_ID	"00"
#define MAXIDQ	30

class CM2MDataSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CM2MDataSerialDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		void ExecuteFunction(UINT uID);

	public:
		// Public Data
		UINT	m_Server;
		UINT	m_RecvPort;
		UINT	m_SendPort;
		UINT	m_LPosition;
		UINT	m_LTarget;
		CString	m_LTypeS;
		UINT	m_LError;
		UINT	m_LDEMCt;
		UINT	m_LDEMSel;
		CString	m_LDEMStr;
		CString	m_LDEMStr1;
		CString	m_LDEMStr2;
		CString	m_LDEMStr3;
		CString	m_LDEMStr4;
		CString	m_LDEMStr5;
		CString	m_LDEMStr6;
		CString	m_LDEMStr7;
		CString	m_LDEMStr8;
		CString	m_LDEMStr9;
		CString	m_LDEMStr10;
		CString	m_LDEMStr11;
		CString	m_LDEMStr12;
		CString	m_LDEMStr13;
		CString	m_LDEMStr14;
		CString	m_LDEMStr15;
		CString	m_LDEMStr16;
		CString	m_LDEMStr17;
		CString	m_LDEMStr18;
		CString	m_LDEMStr19;
		CString	m_LDEMStr20;
		CString	m_LDEMStr21;
		CString	m_LDEMStr22;
		CString	m_LDEMStr23;
		CString	m_LDEMStr24;
		CString	m_LDEMStr25;
		CString	m_LDEMStr26;
		CString	m_LDEMStr27;
		CString	m_LDEMStr28;
		CString	m_LDEMStr29;
		CString	m_LDEMStr30;
		UINT	m_Next;
		UINT	m_Prev;
		UINT	m_SelLong;
		UINT	m_SelWord;
		UINT	m_SelByte;
		UINT	m_SelBit;
		UINT	m_FAppend;
		UINT	m_FInsert;
		UINT	m_FReplace;
		UINT	m_FDelete;
		CString	m_LErrorS;

	protected:
		// Data
		CString		m_NO_ID;
		UINT		m_LFunct;
		UINT		m_LType;
		CUIViewWnd	*m_pWnd;

		// Download Support
		// Meta Data Creation
		void	AddMetaData(void);

		// Show Data List
		BOOL	ShowDataList(CViewWnd *pWnd);
		void	InitData(void);
		BOOL	HasID(CString sTest, UINT *pID);

		// Do String Functions
		void	DoFunction(void);
		UINT	GetStrFunction(UINT uID);
		void	SetFileTransfer(UINT uID);

		// String Buffer Display
		void	SetTargetData(void);
		// Function Handling
		CString	MakeDataStr(void);
		CString *GetStringPar(UINT *pCount, UINT *pLen);
		UINT	AppendItem(CString sAppend);
		void	ReplaceItem(void);
		void	InsertItem(CString sInsert, UINT uPosition);
		UINT	DeleteItem(UINT uPosition);

		// Access String Data
		CString	*GetStringX(UINT u);
		BOOL	FindStringX(UINT uMatch, UINT *pNum);
		void	SelectDEMString(void);
		void	SaveDEMString(void);
		void	SetStrParams(void);
		CString	GetFront(UINT uPos, CString sStr);
		CString	GetRear(UINT uPos, CString sStr);
		UINT	GetIDFromString(CString sStr);
		UINT	GetCountFromString(CString sStr);
		UINT	GetTypeFromDEMStr(UINT uPosition);
		UINT	GetTypeFromTypeS(void);
		char	MakeChar0(UINT uType);
		void	UpdateSize(void);
		void	UpdateItemCount(void);
		// UI Updates
		void	UpdateDEMUI(CUIViewWnd *pWnd);
		void	EnableUIs(CUIViewWnd *pWnd);

		UINT	GetHexStrVal(CString s);
	};

class CM2MDataTCPDriverOptions : public CM2MDataSerialDriverOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CM2MDataTCPDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Serial Driver
//

class CM2MDataSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CM2MDataSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		BOOL	HasList(UINT uTable);
		// Access String Data
		CString	GetStringX(UINT u, CItem *pConfig);
		CString MatchStringX(UINT uID, CItem *pConfig, PBYTE pStrNum);

		// Data

	protected:
		// Data

		// Implementation
		void	AddSpaces();

		// Helpers
		BOOL	IsFILID(UINT uTable);
	};

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp TCP Slave Driver
//

class CM2MDataTCPDriver : public CM2MDataSerialDriver
{
	public:
		// Constructor
		CM2MDataTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Address Selection
//

class CM2MDataAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CM2MDataAddrDialog(CM2MDataSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		CM2MDataSerialDriver *m_pM2MDriver;
		CItem	*m_pDrvCfg;
		CString	 m_DEMString;
		UINT	 m_uDEMCount;
		UINT	 m_uPos;
		BOOL	 m_fUpdate;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSelChange(UINT uID, CWnd &Wnd);

		// Command Handler
		BOOL	OnOkay(UINT uID);
		BOOL	OnButtonClicked(UINT uID);

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Override Help
		void	SetPosInfo(CString Text);
		void	SetTargetInfo(CString Text);
		void	SetTypeInfo(  CString Text);
		UINT	GetTypeFromStr(CString ss, UINT uPos);

		CString GetStringX(UINT i, CItem *pConfig);
		CString MatchStringX(UINT uID, CItem *pConfig);
		UINT	GetDEMID(CString ss);
		void	SetStrParams(CString ss);

		// Helpers
		void	DoShow(CAddress Addr);
		void	SetEnables(BYTE bEnable);
		DWORD	GetSCH0(UINT uTable);
		BOOL	IsSCHn(UINT uTable);
		BOOL	IsSCHc(char cPre);
		void	LoadList(void);
		UINT	IsSignal(UINT uTable);
		BOOL	IsFileID(UINT uTable);
	};

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Show Block List
//

class CM2MShowListDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CM2MShowListDialog(CM2MDataSerialDriverOptions &Options);
		                
	protected:
		// Data
		CM2MDataSerialDriverOptions *m_pOpt;
		CString	 m_sDEM;
		UINT	 m_uDEMCount;
		UINT	 m_uPosition;
		UINT	 m_uType;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSelChange(UINT uID, CWnd &Wnd);
		BOOL	OnButtonClicked(UINT uID);

		void	LoadTypes(void);
		CString	GetTypeFromBox(void);
		void	SetTypeToBox(CString sType);

		// Helpers
		CString	GetTypeInfo(UINT uType);
		CString	GetTypeFromString(UINT uPos);
		UINT	GetTypeNumber(CString sType);
		void	LoadList(void);
		void	LoadListDEM(void);
		void	LoadListFIL(void);
		void	UpdateParams(void);
		void	UpdateType(void);
		void	SelectFocus(UINT uID);
		BOOL	IsListFunction(UINT uID);
		BOOL	IsTypeSelect(UINT uID);
		BOOL	IsNewShow(UINT uID);
		BOOL	IsFileTransfer(void);
		void	EnableList(void);
	};

// Table Numbers
enum {

// Signal
	TSCHE	=  1,
	TRBEE	=  2,
	TFILE	=  3,

	TZONE	=  8,
	TERR	=  9,
	TMOUT	= 10,

// Demand Poll
	TDEMA	= 11,

// Scheduled Data
	TSCHA	= 21,

// Report By Exception Data
//	TRBEL	= 31,
	TRBEW	= 32,
//	TRBEY	= 33,
//	TRBEB	= 34,
//	TRBER	= 35,

// Control
//	TCTLL	= 41,
	TCTLW	= 42,
//	TCTLY	= 43,
//	TCTLB	= 44,
//	TCTLR	= 45,

// File
	TF210	= 210,
	TF211	= 211,
	TF212	= 212,
	TF213	= 213,
	TF214	= 214,
	TF215	= 215,
	TF216	= 216,
	TF217	= 217,
	TF218	= 218,
	TF219	= 219,
	TF220	= 220,
	TF221	= 221,
	TF222	= 222,
	TF223	= 223,
	TF224	= 224,
	TF225	= 225,
	TF226	= 226,
	TF227	= 227,
	TF228	= 228,
	TF229	= 229,
	TF230	= 230,
	TF231	= 231,
	TF232	= 232,
	TF233	= 233,
	TF234	= 234,
	TF235	= 235,
	};

// Opcodes (invalid DEM/SCH addresses)
enum {
	IDACK	= 200,
	IDDEM	= 201,
	IDRBE	= 202,
	IDCTL	= 203,
	};

// List Operation Functions
enum {
	LFNNOP	= 0, // no op
	LFNAPP	= 1, // append
	LFNREP	= 2, // replace
	LFNINS	= 3, // insert
	LFNREM	= 4, // remove
	};

#define	LL	addrLongAsLong
#define	WW	addrWordAsWord
#define	YY	addrByteAsByte
#define	BB	addrBitAsBit
#define	RR	addrRealAsReal

// Drop down type list
#define	LTYPEW	0
#define	LTYPEL	1
#define	LTYPEY	2
#define	LTYPEB	3

// Cfg String Types
#define	CSLONG	'L'
#define	CSWORD	'I'
#define	CSBYTE	'E'
#define	CSBIT	'@'

// Expand String Values
#define	CEBIT	1
#define	CEBYTE	8
#define	CEWORD	16
#define	CELONG	32

// Dialog Box Numbers
// Gateway Block Selections
enum {
	DBPRE	= 2001,
	DBPOS	= 2002,
	DBTYP	= 2003,
	DBTYPT	= 2004,
	DBTART	= 2010,
	DBTARV	= 2011,
	DBDEVT	= 2021,
	DBDEVV	= 2022,
	DBDEVS	= 2023,
	DBUPD	= 2030,
	DBNEXT	= 2031,
	};

// Demand Poll / Scheduled Data Block Editting
enum {
	SLDBT1	= 1002,
	SLDBLB	= 1003,
	SLDBID	= 2051,
	SLDBT2	= 2052,
	SLDBCT	= 2053,
	SLDBT3	= 2054,
	SLDBTY	= 2055,
	SLDBNS	= 2056,
	SLDBAP	= 2060,
	SLDBIN  = 2061,
	SLDBRE	= 2062,
	SLDBDE	= 2063,
// Save and Cancel DEM/SCH/FIL
	SLDBSV	= 2064,
	SLDBCN	= 2065
	};

// File Transfer Editting Dialog
enum {
	FTDBT1	= 3001,
	FTDBLB	= 3002,
	FTDBTX	= 3003,
	FTDBMX	= 3004,
	FTDBSH	= 3005
	};

// Execute Function ID's
enum {
	EFINIT	= 0,
	EFUPDT	= 1
	};

// Error Message Equates
#define	ELFULL	1
#define	EIDMIS	2
#define	ECTLRBE	3

// Max Entry Value
#define	MAXENT	511
#define	MAXDEM	(MAXENT + 3) // 2 ID + 512 data

// Type Position
#define	POSSTN	11

// End of File

#endif
