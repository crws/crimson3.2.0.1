
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMOK7_HPP
	
#define	INCLUDE_IMOK7_HPP

//////////////////////////////////////////////////////////////////////////
//
// IMO K7 Series
//

class CIMOK7Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CIMOK7Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
