//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_UNITEL47_HPP
	
#define	INCLUDE_UNITEL47_HPP

//////////////////////////////////////////////////////////////////////////
//
// Unitelway47 Master Device Options
//

class CUnitel47MasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUnitel47MasterDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Category;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Telemechanique UNI-Telway
//

class CUnitelway47MasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CUnitelway47MasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Telemechanique UNI-Telway Application Master / Network Slave
/*/

class CUnitelway47MSDriver : public CUnitelway47MasterDriver
{
	public:
		// Constructor
		CUnitelway47MSDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};
*/

// End of File

#endif
