
#include "intern.hpp"

#include "DebugSerial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug Serial Driver
//

// Instantiator

ICommsDriver *	Create_DebugSerialDriver(void)
{
	return New CDebugSerialDriver;
	}

// Constructor

CDebugSerialDriver::CDebugSerialDriver(void)
{
	m_wID		= 0x40E5;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Debug Console";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Debug Console";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Binding Control

UINT CDebugSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CDebugSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// End of File
