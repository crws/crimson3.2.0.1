
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Standard Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CStdAddrDialog, CStdDialog);
		
// Constructor

CStdAddrDialog::CStdAddrDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pConfig = pConfig;

	m_pSpace  = NULL;

	m_Element = L"StdElementDlg";

	SetName(fPart ? L"PartStdAddrDlg" : L"FullStdAddrDlg");
	}

// Message Map

AfxMessageMap(CStdAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CStdAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;
	
	if( !m_fPart ) {

		SetCaption();

		FindSpace();

		LoadList();

		if( m_pSpace ) {

			ShowAddress(Addr);

			SetAddressFocus();

			return FALSE;
			}

		return TRUE;
		}
	else {
		FindSpace();

		LoadType();

		ShowAddress(Addr);

		if( TRUE ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				ShowAddress(Addr);
				}
			}

		SetAddressFocus();

		return FALSE;
		}
	}

// Notification Handlers

void CStdAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CStdAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		LoadType();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		ClearType();

		ClearAddress();

		ClearDetails();
		}
	}

void CStdAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		ShowDetails();

		ShowType(GetTypeCode());
		}
	}

// Command Handlers

BOOL CStdAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		Text += GetTypeText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CStdAddrDialog::SetCaption(void)
{
	CString Text;

	Text.Printf( CString(IDS_DRIVER_CAPTION), 
		     GetWindowText(), 
		     m_pDriver->GetString(stringShortName)
		     );

	SetWindowText(Text);
	}

void CStdAddrDialog::LoadElementUI(void)
{
	CWnd  &Wnd = GetDlgItem(2001);

	CRect Rect = Wnd.GetClientRect();

	Wnd.ClientToScreen(Rect);

	ScreenToClient(Rect);

	CDlgLoader Loader;

	Loader.SetName(PCTXT(m_Element));

	Loader.SetOrigin(Rect.GetTopRight() + CSize(4, -2));

	if( Loader.Load() ) {

		Loader.Process(ThisObject, FALSE);

		Loader.Free();
		}
	}

void CStdAddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 40, 100, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf( L"<%s>\t%s",
		      CString(IDS_DRIVER_NONE),
		      CString(IDS_DRIVER_NOSELECTION)
		      );

	ListBox.AddString(Entry, NOTHING);

	INDEX	         Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( AllowSpace(pSpace) ) {

				CString Entry;
			
				Entry.Printf(L"%s\t%s", pSpace->m_Prefix, pSpace->m_Caption);

				ListBox.AddString(Entry, DWORD(n) );

				if( pSpace == m_pSpace ) {

					Find = n;
					}
				}

			List.GetNext(n);
			}
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSpaceChange(1001, ListBox);
	}

void CStdAddrDialog::LoadType(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(4001);
	
	ListBox.SetRedraw(FALSE);

	ListBox.ResetContent();

	UINT uFind = m_pSpace->m_uType;

	if( m_pSpace->m_uType > m_pSpace->m_uSpan ) {

		ListBox.AddString(m_pSpace->GetTypeAsText(m_pSpace->m_uType), m_pSpace->m_uType);

		ListBox.AddString(m_pSpace->GetTypeAsText(m_pSpace->m_uSpan), m_pSpace->m_uSpan);

		if( UINT(m_pAddr->a.m_Type) == m_pSpace->m_uType ) {

			uFind = m_pSpace->m_uType;
			}

		if( UINT(m_pAddr->a.m_Type) == m_pSpace->m_uSpan ) {

			uFind = m_pSpace->m_uSpan;
			}
		}
	else {
		for( UINT uType = m_pSpace->m_uType; uType <= m_pSpace->m_uSpan; uType++ ) {

			if( AllowType(uType) ) {

				ListBox.AddString(m_pSpace->GetTypeAsText(uType), uType);

				if( UINT(m_pAddr->a.m_Type) == uType ) {

					uFind = uType;
					}
				}
			}
		}

	ListBox.SelectData(DWORD(uFind));

	ListBox.EnableWindow(TRUE);

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnTypeChange(4001, ListBox);
	}

void CStdAddrDialog::FindSpace(void)
{
	m_pSpace = m_pDriver->GetSpace(*m_pAddr);
	}

void CStdAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	Text = Text.Mid(m_pSpace->m_Prefix.GetLength());

	SetAddressText(Text.Left(Text.Find('.')));
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);

	ShowType(Addr.a.m_Type);
	}

void CStdAddrDialog::ShowType(UINT uType)
{
	if( uType == m_pSpace->m_uType ) {

		GetDlgItem(2003).SetWindowText(L"");

		return;
		}

	CString Text = L"." + m_pSpace->GetTypeModifier(uType);

	GetDlgItem(2003).SetWindowText(Text);
	}

void CStdAddrDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	if( !m_pSpace->IsNamed() ) {

		CAddress Addr;

		Addr.a.m_Type = GetTypeCode();

		m_pSpace->GetMinimum(Addr);

		m_pDriver->ExpandAddress(Min, m_pConfig, Addr);

		m_pSpace->GetMaximum(Addr);

		m_pDriver->ExpandAddress(Max, m_pConfig, Addr);

		Rad = m_pSpace->GetRadixAsText();
		}

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

void CStdAddrDialog::ClearAddress(void)
{
	GetDlgItem(2001).SetWindowText(CString(IDS_DRIVER_NONE));

	GetDlgItem(2003).SetWindowText(L"");

	GetDlgItem(IDCLEAR).EnableWindow(FALSE);

	SetAddressText(L"");
	}

void CStdAddrDialog::ClearDetails(void)
{
	GetDlgItem(3002).SetWindowText(L"");

	GetDlgItem(3004).SetWindowText(L"");
	
	GetDlgItem(3006).SetWindowText(L"");
	
	GetDlgItem(3008).SetWindowText(L"");
	}

void CStdAddrDialog::ClearType(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(4001);
	
	ListBox.EnableWindow(FALSE);

	ListBox.ResetContent();
	}

UINT CStdAddrDialog::GetTypeCode(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(4001);

	UINT uPos  = ListBox.GetCurSel();

	return UINT(ListBox.GetItemData(uPos));
	}

CString CStdAddrDialog::GetTypeText(void)
{
	if( m_pSpace ) {

		UINT uType = GetTypeCode();

		return L"." + m_pSpace->GetTypeModifier(uType);
		}

	return L"";
	}

// Overridables

BOOL CStdAddrDialog::AllowType(UINT uType)
{
	return TRUE;
	}

BOOL CStdAddrDialog::AllowSpace(CSpace *pSpace)
{
	return TRUE;
	}

void CStdAddrDialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CStdAddrDialog::SetAddressText(CString Text)
{
	GetDlgItem(2002).SetWindowText(Text);

	GetDlgItem(2002).EnableWindow(!Text.IsEmpty());
	}

CString CStdAddrDialog::GetAddressText(void)
{
	return GetDlgItem(2002).GetWindowText();
	}

// End of File
