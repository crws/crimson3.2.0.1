
#include "intern.hpp"

#include "bbbsap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBBBSAPTagsDialog, CStdAddrDialog);

// Constructor

CBBBSAPTagsDialog::CBBBSAPTagsDialog(CBBBSAPTagsBaseDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fSelect	= TRUE;

	m_fUpdate	= FALSE;

	m_fSlave	= ((CBBBSAPTagsDeviceOptions *)(pConfig))->m_fSlave;
	
	m_Images.Create(TEXT("CommsManager"), 16, 0, IMAGE_BITMAP, 0);

	SetName(fPart ? TEXT("PartBBBSAPDlg") : TEXT("FullBBBSAPDlg"));
	}

// Destructor

CBBBSAPTagsDialog::~CBBBSAPTagsDialog(void)
{
//	afxThread->SetStatusText("");
	}

// Initialisation

void CBBBSAPTagsDialog::SetCaption(CString const &Text)
{
	m_Caption = Text;
	}

void CBBBSAPTagsDialog::SetSelect(BOOL fSelect)
{
	m_fSelect = fSelect;
	}

// Message Map

AfxMessageMap(CBBBSAPTagsDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_COMMAND)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown   )

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(4001, OnCreateTag)
	AfxDispatchCommand(4011, OnUpdateAddress)
	AfxDispatchCommand(4002, OnButtonClicked)
	AfxDispatchCommand(4003, OnButtonClicked)
	AfxDispatchCommand(4004, OnButtonClicked)
	AfxDispatchCommand(4005, OnButtonClicked)
	AfxDispatchCommand(4006, OnButtonClicked)

	AfxMessageEnd(CBBBSAPTagsDialog)
	};

// Message Handlers

BOOL CBBBSAPTagsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(m_Caption);

	GetDlgItem(IDCANCEL).EnableWindow(m_fSelect);

	GetDlgItem(IDOK).SetWindowText(m_fSelect ? TEXT("OK") : TEXT("Close"));

	EnableRadioButtons();

	SetRadioButton(addrByteAsByte, FALSE);

	for( UINT n = LISTNUMTXT; n <= LISTINFO; n++ ) { // slave specific fields

		GetDlgItem(n).EnableWindow(m_fSlave);
		}

	if( m_fPart ) {

		ShowAddress(*m_pAddr);
		}
	else {
		LoadNames();
		}

	CAddress &Addr = *m_pAddr;

	UpdateRadios(Addr);

	UpdateList(((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetIndex(Addr));

	DoWarning(FALSE);

	return FALSE;
	}

BOOL CBBBSAPTagsDialog::OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl)
{
	if( uID == IDCANCEL ) {

		((CBBBSAPTagsDeviceOptions *)(m_pConfig))->WalkTags();
		}
	
	return CStdAddrDialog::OnCommand(uID, uNotify, Ctrl);
	}

// Notification Handlers

void CBBBSAPTagsDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_hSelect  = Info.itemNew.hItem;

	m_dwSelect = Info.itemNew.lParam;

	CAddress Addr   = *m_pAddr;

	if( m_fSlave ) {

		Addr.a.m_Offset &= 0x3FF;

		Addr.a.m_Offset |= GetListNumber() << 10;
		}

	else Addr.a.m_Offset = 0;

	if( m_dwSelect == Addr.m_Ref ) {
		
		Addr = *m_pAddr;
		}
	else
		Addr = (CAddress &) m_dwSelect;


	ShowAddress(Addr);

	ShowDetails(Addr);

	UpdateRadios(Addr);

	UpdateList(((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetIndex(Addr));

	DoWarning(FALSE);
	}

BOOL CBBBSAPTagsDialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	DoWarning(FALSE);

	if( Info.wVKey == VK_DELETE ) {
		
		if( IsDown(VK_SHIFT) ) {

			return DeleteTag();
			}
		}

	if( Info.wVKey == VK_F2 ) {		
		
		return RenameTag();
		}

	return FALSE;
	}

// Command Handlers

BOOL CBBBSAPTagsDialog::OnOkay(UINT uID)
{
	DoWarning(FALSE);

	((CBBBSAPTagsDeviceOptions *)(m_pConfig))->WalkTags();

	if( m_fSelect ) {
	
		CAddress Addr;
		
		if( !m_fPart ) {

			if( !m_dwSelect ) {

				m_pAddr->m_Ref = 0;

				EndDialog(TRUE);

				return TRUE;
				}

			Addr = (CAddress &) m_dwSelect;
			}
		else 
			Addr = *m_pAddr;

		CString Name;

		(CBBBSAPTagsBaseDriver *)(m_pDriver)->ExpandAddress(Name, m_pConfig, Addr);

		CString Text;

		Text += ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetTagName(Name);

		CError Error(TRUE);

		if( (CBBBSAPTagsBaseDriver *)(m_pDriver)->ParseAddress(Error, Addr, m_pConfig, Text) ) {
			
			*m_pAddr = Addr;

			((CBBBSAPTagsDeviceOptions *)(m_pConfig))->SetDirty();
			
			EndDialog(TRUE);
			
			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus(2002);

		return TRUE;
		}

	EndDialog(TRUE);
	
	return TRUE;
	}

BOOL CBBBSAPTagsDialog::OnCreateTag(UINT uID)
{
	DoWarning(FALSE);

	CAddress Addr;

	Addr.m_Ref = MakeAddrFromButtons();

	if( !Addr.a.m_Table ) {

		Addr.a.m_Table = SPLONG;
		Addr.a.m_Type  = addrLongAsLong;
		UpdateRadios(Addr);
		if( !Addr.a.m_Offset ) ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetNewIndex();
		}

	CString Name = ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->CreateName(Addr);

	CStringDialog Dlg(TEXT("Create Tag"), TEXT("Tag Name"), Name);

	Dlg.SetLimit(MAX_TAG_LENGTH);

	if( !IsDown(VK_SHIFT) ) {
		
		if( Dlg.Execute(ThisObject) ) {

			Name = Dlg.GetData();

			((CBBBSAPTagsDeviceOptions *)(m_pConfig))->SetPrevious(Name);
			}
		else
			return TRUE;

		}

	CError Error(TRUE);

	if( ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, Name, Addr, TRUE, FALSE) ) {

		CError Error(FALSE);

		if( (CBBBSAPTagsBaseDriver *)(m_pDriver)->ParseAddress(Error, Addr, m_pConfig, Name) ) {
			
			CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);
			
			LoadTag(Tree, Name, Addr);
			
			Tree.Expand(m_hRoot, TVE_EXPAND);
			
			Tree.SetFocus();
			
			((CBBBSAPTagsDeviceOptions *)(m_pConfig))->SetDirty();

			SetRadioButtons(Addr);
			}
		else
			AfxAssert(FALSE);
		}
	else {
		UpdateRadios(Addr);

		Error.Show(ThisObject);
		}

	UpdateList(((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetIndex(Addr));

	return TRUE;
	}

BOOL CBBBSAPTagsDialog::OnButtonClicked(UINT uID)
{
	SetRadioButton(uID, TRUE);

	return TRUE;
	}

BOOL CBBBSAPTagsDialog::OnUpdateAddress(UINT uID)
{
	BOOL fRedoList = FALSE;

	if( m_dwSelect ) {

		CAddress Addr	= (CAddress &)m_dwSelect;
		CAddress ANew;

		UINT uOffset    = ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetIndex(Addr);

		UINT uIndex     = m_fSlave ? uOffset & 0x3FF : uOffset;

		CString Name	= FindNameFromAddr(Addr);

		ANew.m_Ref	= MakeAddrFromButtons();

		ANew.m_Ref	|= uIndex;

		if( ANew.m_Ref == Addr.m_Ref ) return TRUE; // no change

		m_fUpdate	= TRUE;

		fRedoList	= GetListNumber(Addr.a.m_Offset) != GetListNumber(ANew.a.m_Offset);

		if( DeleteTag() ) {

			CError Error(TRUE);

			Addr.m_Ref = ANew.m_Ref;

			if( ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, Name, Addr, fRedoList, TRUE) ) {

				if( m_fSlave ) Addr.a.m_Offset |= (ANew.a.m_Offset & 0xFC00); // add list #

				CError Error(FALSE);

				if( (CBBBSAPTagsBaseDriver *)(m_pDriver)->ParseAddress(Error, Addr, m_pConfig, Name) ) {

					CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);

					LoadTag(Tree, Name, Addr);
				
					Tree.Expand(m_hRoot, TVE_EXPAND);
				
					Tree.SetFocus();
				
					((CBBBSAPTagsDeviceOptions *)(m_pConfig))->SetDirty();

					SetRadioButtons(Addr);

					if( fRedoList ) {

						CTagDataBArray List;

						List.Empty();

						((CBBBSAPTagsDeviceOptions *)(m_pConfig))->ListTags(List);
						}
					}
				}
			else {
				m_fUpdate = FALSE;

				AfxAssert(FALSE);
				}
			}
		}

	m_fUpdate = FALSE;

	SetDlgFocus(4001);

	DoWarning(fRedoList);

	return TRUE;
	}

// Tree Loading

void CBBBSAPTagsDialog::LoadNames(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_NOHSCROLL
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_EDITLABELS
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

#if defined(C3_VERSION)

	Tree.SendMessage(TVM_SETUNICODEFORMAT, 1);
	
#else	
	Tree.SendMessage(TVM_SETUNICODEFORMAT);

#endif
	
	LoadNames(Tree);

	Tree.SetFocus();
	}

void CBBBSAPTagsDialog::LoadNames(CTreeView &Tree)
{
	LoadRoot(Tree);
	
	LoadTags(Tree);

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CBBBSAPTagsDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	Node.SetText(((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetDeviceName());

	Node.SetParam(FALSE);

	Node.SetImages(3);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CBBBSAPTagsDialog::LoadTags(CTreeView &Tree)
{
	CTagDataBArray List;
	
	((CBBBSAPTagsDeviceOptions *)(m_pConfig))->ListTags(List);

//	List.Sort();

	m_fLoad = TRUE;

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CTagDataB const &Data = List[n];

		LoadTag(Tree, Data.m_Name, (CAddress &) Data.m_Addr);
		}

	m_fLoad = FALSE;
	}

void CBBBSAPTagsDialog::LoadTag(CTreeView &Tree, CString Name, CAddress Addr)
{
	CTreeViewItem Node;

	Node.SetText(Name);

	Node.SetParam(Addr.m_Ref);

	Node.SetImages(0x34);

	HTREEITEM hNode = Tree.InsertItem(m_hRoot, NULL, Node);

	if( m_fLoad ) {

		CAddress Test = *m_pAddr;

		if( Addr.m_Ref == Test.m_Ref ) {	

			Tree.SelectItem(hNode, TVGN_CARET);
			}
		}
	else
		Tree.SelectItem(hNode, TVGN_CARET);

	SetRadioButtons(Addr);
	}

// Implementation

void CBBBSAPTagsDialog::ShowDetails(CAddress Addr)
{
	if( Addr.m_Ref ) {

		CString Type = CSpace("", "", 0).GetTypeAsText(Addr.a.m_Type);
			
		GetDlgItem(3002).SetWindowText(Type);
		
		return;
		}

	GetDlgItem(3002).SetWindowText("");
	}

void CBBBSAPTagsDialog::ShowAddress(CAddress Addr)
{
	if( Addr.m_Ref ) {

		CString  Tag = FindNameFromAddr(Addr);

		GetDlgItem(2001).SetWindowText(Tag);

		EnableRadioButtons();

		SetRadioButtons(Addr);

		SetListNumber(((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetIndex(Addr));

		return;
		}

	GetDlgItem(2001).SetWindowText(TEXT("None"));

	GetDlgItem(2002).ShowWindow(FALSE);

	GetDlgItem(2003).ShowWindow(FALSE);
	}

BOOL CBBBSAPTagsDialog::DeleteTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString  Name;

		(CBBBSAPTagsBaseDriver *)(m_pDriver)->ExpandAddress(Name, m_pConfig, Addr);

		if( !m_fUpdate ) {

			CString Text  = CPrintf("Do you really want to delete %s ?", ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetTagName(Name));

			m_fUpdate = NoYes(Text) == IDYES;
			}

		if( m_fUpdate ) {

			CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

			Tree.SetRedraw(FALSE);

			HTREEITEM hPrev = m_hSelect;

			Tree.MoveSelection();

			((CBBBSAPTagsDeviceOptions *)(m_pConfig))->DeleteTag(Addr, TRUE);

			Tree.DeleteItem(hPrev);

			Tree.SetRedraw(TRUE);

			((CBBBSAPTagsDeviceOptions *)(m_pConfig))->SetDirty();

			return TRUE;
			}		
		}

	return FALSE;
	}

BOOL CBBBSAPTagsDialog::RenameTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString Name;

		if( (CBBBSAPTagsBaseDriver *)(m_pDriver)->ExpandAddress(Name, m_pConfig, Addr) ) {

			CString Title   = TEXT("Rename Variable");
			
			CString Group   = TEXT("Variable Name");

			CString TagName = ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->GetTagName(Name);

			CStringDialog Dlg(Title, Group, TagName);

			Dlg.SetLimit(MAX_TAG_LENGTH);

			if( Dlg.Execute(ThisObject) ) {

				CTreeView &Tree = (CTreeView &) GetDlgItem(1001);
				
				CString    Name = Dlg.GetData();			

				if( ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->RenameTag(Name, Addr) ) {

					Tree.SetRedraw(FALSE);

					HTREEITEM hPrev = m_hSelect;

					Tree.MoveSelection();

					Tree.DeleteItem(hPrev);

					LoadTag(Tree, Name, Addr);

					Tree.SetRedraw(TRUE);

					((CBBBSAPTagsDeviceOptions *)(m_pConfig))->SetDirty();
					
					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

BOOL CBBBSAPTagsDialog::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
	}

CString CBBBSAPTagsDialog::FindNameFromAddr(CAddress Addr)
{
	CString Name = TEXT("?");

	if( Addr.m_Ref ) {

		(CBBBSAPTagsBaseDriver *)(m_pDriver)->ExpandAddress(Name, m_pConfig, Addr);
		}

	return Name;
	}

void CBBBSAPTagsDialog::UpdateRadios(CAddress Addr)
{
	if( Addr.m_Ref ) {
		
		SetRadioButtons(Addr);

		return;
		}
	
	ClearRadioButtons();

	SetRadioButton(BUTREAL, TRUE);
	}

// Radio button handling

UINT CBBBSAPTagsDialog::GetRadioButton(void)
{
	UINT uResult = 0;

	for( UINT i = BUTBIT; i <= BUTSTR; i++ ) {

		CButton &Button = (CButton &) GetDlgItem(i);

		if( Button.IsChecked() ) {

			ClearRadioButtons();

			uResult = i - BUTBIT + 1;

			SetRadioButton(ButtonToType(uResult), FALSE);

			return uResult;
			}
		}

	return uResult;
	}

void CBBBSAPTagsDialog::SetRadioButton(UINT uItem, BOOL fID)
{
	if( fID ) {

		for( UINT i = BUTBIT; i <= BUTSTR; i++ ) {

			PutRadioButton(i, i == uItem);
			}
		}

	else {	
		PutRadioButton(BUTBIT,	uItem == addrBitAsBit);
		PutRadioButton(BUTBYTE,	uItem == addrByteAsByte);
		PutRadioButton(BUTWORD,	uItem == addrWordAsWord);
		PutRadioButton(BUTLONG,	uItem == addrLongAsLong);
		PutRadioButton(BUTREAL,	uItem == addrRealAsReal);
		PutRadioButton(BUTSTR,  uItem == addrLongAsLong && ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->IsString(*m_pAddr));
		}
	}

void CBBBSAPTagsDialog::SetRadioButtons(CAddress Addr)
{
	UINT j = BTYPEBIT;

	ClearRadioButtons();

	for( UINT i = BUTBIT; i <= BUTSTR; i++, j++ ) {

		BOOL fSet = Addr.a.m_Type == ButtonToType(j);

		if( fSet && Addr.a.m_Type == addrLongAsLong ) {

			BOOL fStr = ((CBBBSAPTagsDeviceOptions *)(m_pConfig))->IsString(Addr);

			fSet = i == BUTLONG ? !fStr : fStr;
			}

		PutRadioButton(i, fSet);
		}
	}

void CBBBSAPTagsDialog::PutRadioButton(UINT uID, BOOL fYes)
{
	CButton &Check = (CButton &) GetDlgItem(uID);

	Check.SetCheck( fYes );
	}

void CBBBSAPTagsDialog::ClearRadioButtons(void)
{
	for( UINT i = BUTBIT; i <= BUTSTR; i++ ) {

		CButton &Check = (CButton &) GetDlgItem(i);

		Check.SetCheck(FALSE);
		}
	}

void CBBBSAPTagsDialog::EnableRadioButtons(void)
{
	for( UINT i = BUTBIT; i <= BUTSTR; i++ ) {

		CButton &Check = (CButton &) GetDlgItem(i);

		Check.EnableWindow(!(i == BUTSTR && m_fSlave));
		}
	}

UINT CBBBSAPTagsDialog::ButtonToType(UINT uButton)
{
	switch( uButton ) {

		case BTYPEBIT:	return addrBitAsBit;
		case BTYPEBYTE:	return addrByteAsByte;
		case BTYPEWORD:	return addrWordAsWord;
		case BTYPELONG:	return addrLongAsLong;
		case BTYPEREAL:	return addrRealAsReal;
		case BTYPESTR:	return addrLongAsLong;
		}

	return 0;
	}

UINT CBBBSAPTagsDialog::ButtonToTable(void)
{
	CAddress Addr;

	Addr.m_Ref = MakeAddrFromButtons();

	return Addr.a.m_Table;
	}

// Other Helpers
UINT CBBBSAPTagsDialog::GetTypeFromTable(UINT uTable)
{
	switch( uTable ) {

		case SPBIT:	return addrBitAsBit;

		case SPBYTE:	return addrByteAsByte;

		case SPWORD:	return addrWordAsWord;

		case SPREAL:	return addrRealAsReal;
		}

	return addrLongAsLong;
	}

DWORD CBBBSAPTagsDialog::MakeAddrFromButtons(void)
{
	CAddress Addr;

	Addr.a.m_Table   = GetRadioButton();
	Addr.a.m_Type    = GetTypeFromTable(Addr.a.m_Table);
	Addr.a.m_Extra   = 0;

	Addr.a.m_Offset = 0;

	if( m_fSlave ) {

		Addr.a.m_Offset |= GetListNumber() << 10;
		}

	return Addr.m_Ref;
	}

void CBBBSAPTagsDialog::UpdateList(UINT uNum)
{
	GetDlgItem(LISTNUMTXT).EnableWindow(m_fSlave);
	GetDlgItem(LISTNUM).EnableWindow(m_fSlave);

	SetListNumber(uNum);

	if( !m_fSlave ) {

		GetDlgItem(LISTNUM).SetWindowText(TEXT(""));
		}
	}

void CBBBSAPTagsDialog::SetListNumber(UINT uNum)
{
	CString s = "";

	if( m_fSlave ) s.Printf(TEXT("%d"), GetListNumber(uNum));

	GetDlgItem(LISTNUM).SetWindowText(s);

	GetDlgItem(TXTADDR).EnableWindow(TRUE);

	GetDlgItem(TXTHEX).EnableWindow(TRUE);

	GetDlgItem(LISTVAL).EnableWindow(TRUE);

	s.Printf(TEXT("%4.4x"), uNum);

	GetDlgItem(LISTVAL).SetWindowText(s);
	}

UINT CBBBSAPTagsDialog::GetListNumber(void)
{
	return m_fSlave ? min(63, tatoi(GetDlgItem(LISTNUM).GetWindowText())) : 0;
	}

UINT CBBBSAPTagsDialog::GetListNumber(UINT uAddr)
{
	return m_fSlave ? uAddr >> 10 : 0;
	}

void CBBBSAPTagsDialog::DoWarning(BOOL fSet)
{
	CString s = fSet ? STRGBWARN : "";

	GetDlgItem(LISTWARN).SetWindowText(s);
	}

// End of File
