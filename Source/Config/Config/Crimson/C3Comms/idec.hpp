
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IDEC_HPP
	
#define	INCLUDE_IDEC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Short Names for Types
//

#define BB	addrBitAsBit
#define YY	addrByteAsByte
#define WW	addrWordAsWord

//////////////////////////////////////////////////////////////////////////
//
// IDEC Micro 3 Series / ONC Driver
//

class CIdec3PLCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIdec3PLCDriver(void);

		// Binding Control
		void GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);

		// Address Checking
		BOOL IsByteAndBit(CSpace *pSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// IDEC MicroSmart TCP/IP Master Device Options
//

class CIdecMicroSmartMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIdecMicroSmartMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_IP1;
		UINT m_IP2;
		UINT m_Port;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IDEC MicroSmart TCP/IP Master Driver
//

class CIdecMicroSmartMasterTCPDriver : public CIdec3PLCDriver
{
	public:
		// Constructor
		CIdecMicroSmartMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	
	};

// End of File

#endif
