
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PHOENIXNANOLC_HPP
	
#define	INCLUDE_PHOENIXNANOLC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Phoenix Contact nanoLC Driver
//

class CPhoenixNanoLCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CPhoenixNanoLCDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC TCP Device Options
//

class CPhoenixNanoLCTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPhoenixNanoLCTCPDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC TCP Driver
//

class CPhoenixNanoLCTCPDriver : public CPhoenixNanoLCDriver
{
	public:
		// Constructor
		CPhoenixNanoLCTCPDriver(void);

		//Destructor
		~CPhoenixNanoLCTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
						
	protected:
		// Implementation
	};

// End of File

#endif
