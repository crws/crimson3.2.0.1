
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UNIDRIVEM_HPP

#define INCLUDE_UNIDRIVEM_HPP

// Forward Declarations

class	CUnidriveMMenu;
struct	CUnidriveMItem;

// Referenced Message

#define IDM_COMMS_BUILD_BLOCK	0xC11A

// Typedefs

typedef	CMap<UINT, CUnidriveMMenu *>	CMenuMap;
typedef	CArray<CUnidriveMItem>		CItemArray;

// Special menu for regular Modbus access

#define MENU_MODBUS 999

// Modbus Spaces

#define	SPACE_HOLD	0x01
#define	SPACE_ANALOG	0x02
#define	SPACE_OUTPUT	0x03
#define	SPACE_INPUT	0x04
#define	SPACE_HOLD32	0x05
#define	SPACE_ANALOG32	0x06

// Modbus Register Address Modes

enum {
	regModeStd = 1,
	regModeMod = 3
	};

// Drive Modes

enum {
	drvModeOpenLoop	= 1,
	drvModeRFCA	= 3,
	drvModeRFCS	= 5,
	drvModeRegen	= 7,
	drvModeModbus	= 9
	};

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Serial Driver Options
//

class CUnidriveMSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUnidriveMSerialDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Base Device Options
//

class CUnidriveMDeviceOptions : public CUIItem
{
	public:

		// Constructor
		CUnidriveMDeviceOptions(void);

		// Destructor
		~CUnidriveMDeviceOptions(void);

		UINT m_RegisterMode;
		UINT m_DriveMode;
		UINT m_uLastDriveMode;
		
		// Menu and Item Management
		CMenuMap GetMenus(void);
		void AddItem(CUnidriveMItem const &Item);
		BOOL AddMenu(CUnidriveMMenu *pMenu);
		void DeleteItem(CUnidriveMItem Item);
		BOOL FindItem(UINT uRef, CUnidriveMItem &Item);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		BOOL DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);

		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);

	protected:

		INDEX		m_ListIndex;

		// Menu Headers
		void AddHeadersRFCA(CMenuMap &MenuMap);
		void AddHeadersRFCS(CMenuMap &MenuMap);
		void AddHeadersOpenLoop(CMenuMap &MenuMap);
		void AddHeadersRegen(CMenuMap &MenuMap);
		void AddHeadersCommon(CMenuMap &MenuMap);

		// Space Management
		void AddSpacesRFCA(CMenuMap &MenuMap);
		void AddSpacesRFCS(CMenuMap &MenuMap);
		void AddSpacesOpenLoop(CMenuMap &MenuMap);
		void AddSpacesRegen(CMenuMap &MenuMap);
		BOOL OnChangeMode(CWnd *pWnd, UINT uMode);		

		// Menu Map Management
		void InitMapRFCA(CMenuMap &MenuMap);
		void InitMapRFCS(CMenuMap &MenuMap);
		void InitMapRegen(CMenuMap &MenuMap);
		void InitMapOpenLoop(CMenuMap &MenuMap);
		void CleanupMenus(void);

		// Import/Export
		UINT		m_Push;
		CMenuMap	m_DevMenus;

		void OnImport(CWnd *pWnd);
		void OnExport(CWnd *pWnd);

		BOOL ImportItems(FILE *pFile);
		BOOL ExportItems(FILE *pFile);

		TCHAR GetSeparator(void);

		// Comms Rebuild / View Refresh
		void SendRefreshRebuildMsg(CWnd *pWnd);

		// Persistence Helpers		
		void SaveMenus(CTreeFile &Tree);		
		void LoadMenus(CTreeFile &Tree);
		void LoadItems(CTreeFile &Tree);
		void SaveItems(CTreeFile &Tree);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Serial Device Options
//

class CUnidriveMSerialDeviceOptions : public CUnidriveMDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUnidriveMSerialDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Ping;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M TCP Device Options
//

class CUnidriveMTCPDeviceOptions : public CUnidriveMDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUnidriveMTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_Unit;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Serial Driver
//

class CUnidriveMDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CUnidriveMDriver(void);

		// Destructor
		virtual ~CUnidriveMDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL	 SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	 ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	 ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	 ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

		// Address Helpers
		CString GetPrefix(UINT uTable);
		CString	GetRadixText(UINT uTable);
		UINT	GetTable(CString const &Text);
		UINT	GetMax(UINT uTable);
		UINT	GetMin(UINT uTable);

		// Static Type Helpers
		static UINT	GetType(CString const &Text);
		static CString	GetTypeText(UINT uType);

	protected:
		
		// Implementation
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	};

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M TCP/IP Driver
//

class CUnidriveMTCPDriver : public CUnidriveMDriver
{
	public:
		// Constructor
		CUnidriveMTCPDriver(void);

		// Destructor
		~CUnidriveMTCPDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
	};

struct CUnidriveMItem
{
	TCHAR	m_pDesc[64];
	UINT	m_uItem;
	UINT	m_uType;
	UINT	m_uMenu;

	bool operator == (CUnidriveMItem const &That) const;
	};

class CUnidriveMMenu
{
	public:
		// Constructors
		CUnidriveMMenu(void);
		CUnidriveMMenu(CString Name, CString Desc, UINT uMenu);

		// Destructor
		~CUnidriveMMenu(void);

		// Operations
		void AddItem(CUnidriveMItem Item);		
		CString GetName(void) const;

		// Data Members
		CItemArray	m_Items;
		CString		m_Name;
		CString		m_Desc;
		UINT		m_uMenu;
	};

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Address Selection Dialog
//

class CUnidriveMDialog : public CStdDialog
{
	public:
		AfxDeclareRuntimeClass();

		// Constructor
		CUnidriveMDialog(CUnidriveMDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		static int SortHelpAlpha(const void *a, const void *b);
		static int SortHelpNum  (const void *a, const void *b);

	protected:

		AfxDeclareMessageMap();

		// Data Members
		UINT	m_uHeader;
		UINT	m_uFunction;
		UINT	m_uCurrentMode;
		
		CItemArray	   m_LoadedItems;
		CMenuMap	   m_MenuMap;
		CUnidriveMItem	   m_CurrentItem;
		CUnidriveMDriver * m_pDriver;
		CAddress	 * m_pAddr;

		CUnidriveMDeviceOptions * m_pConfig;

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnSortAlpha(UINT uID);
		BOOL OnSortNum(UINT uID);
		BOOL OnOkay(UINT uID);
		void OnDblClk(UINT uID, CWnd &Wnd);
		BOOL OnSearch(UINT uID);
		
		void OnMenuSelChanged(UINT uID, CWnd &Wnd);
		void OnItemSelChanged(UINT uID, CWnd &Wnd);
		
		void BuildMenuMap(void);
		void LoadMenus(void);
		void LoadItems(CItemArray const &Items);
		void SetCurrentSel(void);
		void ShowDetails(void);
		void DoEnables(void);
		void DoSort(BOOL IsAlpha);
	};

// End of File

#endif
