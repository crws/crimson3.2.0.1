
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ADAM_HPP
	
#define	INCLUDE_ADAM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Adam Constants
//

#define SPACE_CONFIG	1
#define SPACE_CONSTAT	2
#define	SPACE_VERSION	3
#define SPACE_NAME	4
#define SPACE_SPAN	5
#define SPACE_OFFSET	6
#define SPACE_CHANSTAT	7
#define	SPACE_CH0	8
#define SPACE_CH1	9
#define SPACE_CH2	10
#define SPACE_CH3	11
#define SPACE_CH4	12
#define SPACE_CH5	13
#define SPACE_CH6	14
#define SPACE_CH7	15
#define SPACE_DIGIN	16
#define SPACE_SYNSAMP	17
#define SPACE_READSYN	18
#define	SPACE_RSTSTAT	19 
#define SPACE_DIGOUT	20
#define SPACE_CH0L	21
#define SPACE_CH1L	22

#define MODEL_4017	0
#define MODEL_4018	1
#define MODEL_4050	2
#define MODEL_4051	3
#define MODEL_4053	4
#define MODEL_4060	5
#define MODEL_4080	6

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Device Options
//

class CAdam4000DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAdam4000DeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		BOOL m_fChecksum;
		UINT m_Device;
									

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Adam 4017-18 Input Module Driver
//

class CAdam4017Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CAdam4017Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);


		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Module Address Selection Dialog
//

class CAdam4000AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAdam4000AddrDialog(CAdam4017Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Data Members

		UINT m_uDevice;

		// Overridables
		BOOL AllowSpace(CSpace *pSpace);

	};


// End of File

#endif
