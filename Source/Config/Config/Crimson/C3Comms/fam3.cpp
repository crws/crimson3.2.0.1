
#include "intern.hpp"

#include "fam3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 Communications
//

// Instantiator

ICommsDriver *	Create_YokoFam3Driver(void)
{
	return New CYokoFam3Driver;
	}

// Constructor

CYokoFam3Driver::CYokoFam3Driver(void)
{
	m_wID		= 0x4018;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yokogawa";
	
	m_DriverName	= "FA-M3 PLC Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "FA-M3";

	AddSpaces();
	}

// Binding Control

UINT CYokoFam3Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CYokoFam3Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CYokoFam3Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYokoFam3DeviceOptions);
	}


// Implementation

void CYokoFam3Driver::AddSpaces(void)
{
	AddSpace(New CSpace(0x18, "X",  "Input Relay",				10,  1, 65535, addrBitAsBit));
	AddSpace(New CSpace(0x19, "Y",  "Output Relay",				10,  1, 65535, addrBitAsBit));
	AddSpace(New CSpace(0x09, "I",  "Internal Relay",			10,  1, 65535, addrBitAsBit));
	//AddSpace(New CSpace(0x05, "E",  "Shared Relay",				10,  1, 65535, addrBitAsBit));
	AddSpace(New CSpace(0x0D, "M",  "Special Relay",			10,  1, 65535, addrBitAsBit));
	AddSpace(New CSpace(0x14, "T",  "Timer Relay",				10,  1, 65535, addrBitAsBit));
	AddSpace(New CSpace(0x03, "C",  "Counter Relay",			10,  1, 65535, addrBitAsBit));
	AddSpace(New CSpace(0x0C, "L",  "Link Relay",				10,  1, 65535, addrBitAsBit));
	AddSpace(New CSpace(0x04, "D",  "Data Register",			10,  1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x02, "B",  "File Register",			10,  1, 65535, addrWordAsWord, addrWordAsReal));
	//AddSpace(New CSpace(0x12, "R",  "Shared Register",			10,  1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x16, "V",  "Index Register",			10,  1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x1A, "Z",  "Special Register",			10,  1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x17, "W",  "Link Register",			10,  1, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x20, "TS", "Timer Value",				10,  1, 65535, addrWordAsWord));
	AddSpace(New CSpace(0x21, "TP", "Timer Present Value",			10,  1, 65535, addrWordAsWord));
	AddSpace(New CSpace(0x25, "TI", "Timer Present Value (count-up)",	10,  1, 65535, addrWordAsWord));
	AddSpace(New CSpace(0x30, "CS", "Counter Value",			10,  1, 65535, addrWordAsWord));
	AddSpace(New CSpace(0x31, "CP", "Counter Present Value",		10,  1, 65535, addrWordAsWord));
	AddSpace(New CSpace(0x35, "CI", "Counter Present Value (count-up)",	10,  1, 65535, addrWordAsWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Yokogaw FA-M3 TCP/IP
//

// Instantiator

ICommsDriver *	Create_CYokoFam3TCPDriver(void)
{
	return New CYokoFam3TCPDriver;
	}

// Constructor

CYokoFam3TCPDriver::CYokoFam3TCPDriver(void)
{
	m_wID		= 0x3515;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yokogawa";
	
	m_DriverName	= "FA-M3 PLC TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "FA-M3 TCP/IP";

	}

// Binding Control

UINT CYokoFam3TCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYokoFam3TCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYokoFam3TCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CYokoFam3TCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYokoFam3TCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC Device Options
//

/*** NOTE:  Serial Modules do not support binary format at this time (m_Mode = 0) ***/


// Dynamic Class

AfxImplementDynamicClass(CYokoFam3DeviceOptions, CUIItem);

// Constructor

CYokoFam3DeviceOptions::CYokoFam3DeviceOptions(void)
{
	m_Mode = 1;
	
	m_Station = 1;

	m_Cpu = 1;

	m_Wait = 0;

	m_fCheck = FALSE;

	m_fTerm = FALSE;
		
	}

// Download Support	     

BOOL CYokoFam3DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);
		
	Init.AddByte(BYTE(m_Cpu));
	Init.AddByte(BYTE(m_Mode));
	Init.AddByte(BYTE(m_Station));
	Init.AddByte(BYTE(m_Wait));
	Init.AddByte(BYTE(m_fCheck));
	Init.AddByte(BYTE(m_fTerm));
       	
	return TRUE;
	}

// Meta Data Creation

void CYokoFam3DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Cpu);
	Meta_AddInteger(Mode);
	Meta_AddInteger(Station);
	Meta_AddInteger(Wait);
	Meta_AddBoolean(Check);
	Meta_AddBoolean(Term);
	
	}


//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYokoFam3TCPDeviceOptions, CUIItem);

// Constructor

CYokoFam3TCPDeviceOptions::CYokoFam3TCPDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Port  = 12289;

	m_Cpu    = 1;

	m_Keep    = TRUE;

	m_Ping    = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;

	m_Mode    = 1;
		
	}

// UI Managament

void CYokoFam3TCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support	     

BOOL CYokoFam3TCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Cpu));
	Init.AddByte(BYTE(m_Mode));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
		
	return TRUE;
	}

// Meta Data Creation

void CYokoFam3TCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Cpu);
	Meta_AddInteger(Mode);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}
 	  
// End of File
