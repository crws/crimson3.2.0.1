
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// UI for CImpactDataDriverOptions
//

CImpactDataDriverOptionsUIList RCDATA
BEGIN
	
	"\0"
END

CImpactDataDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CImpactDataDeviceOptions
//

CImpactDataDeviceOptionsUIList RCDATA
BEGIN
	"IP,IP Address,,CUIIPAddress,,"
	""
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|65535,"
	""
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want this driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Impact Dialog
//
//

ImpactDlg DIALOG 0, 0, 0, 0
CAPTION "Select Command"
BEGIN
	GROUPBOX	"&Vision System",	1000,	  4,   4, 214, 180
	LISTBOX					1001,	 10,  18, 200,  80, XS_LISTBOX

	DEFPUSHBUTTON   "Select Property",	2001,	 10, 104,  60, 20, XS_BUTTONFIRST
	PUSHBUTTON	"Select Type",		2003,	 84, 104,  60, 20, XS_BUTTONFIRST
	LTEXT		"<No Selection>",	2002,	 10, 135, 200, 24

	DEFPUSHBUTTON   "OK",			IDOK,		 83, 164,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	127, 164,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",			IDHELP,		171, 164,  40,  14, XS_BUTTONREST
END

//////////////////////////////////////////////////////////////////////////
//
// Impact Property Dialog
//
//

ImpactPropDlg DIALOG 0, 0, 0, 0
CAPTION "Select Property"
BEGIN
	GROUPBOX	"&Vision System",	2000,	  4,   4, 215, 144

	LTEXT		"&By:",			3001,    10,  23,  30,  12
	COMBOBOX				3002,	 45,  21, 165,  36, CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP

	LTEXT		"&Name:",		3003,    10,  49,  30,  12
	COMBOBOX				3004,	 45,  47, 165,  36, CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP

	LTEXT		"T&ask:",		3005,    10,  75,  30,  12
	COMBOBOX				3006,	 45,  73, 165,  36, CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP

	LTEXT		"T&ool:",		3007,    10, 101,  30,  12
	COMBOBOX				3008,	 45,  99, 165,  36, CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP

	LTEXT		"&Property:",		3009,    10, 127,  30,  12
	COMBOBOX				3010,	 45, 125, 165,  36, CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP

	DEFPUSHBUTTON   "OK",			IDOK,		 83, 160,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	127, 160,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",			IDHELP,		171, 160,  40,  14, XS_BUTTONREST
END

//////////////////////////////////////////////////////////////////////////
//
// Impact Type Dialog
//
//

ImpactTypeDlg DIALOG 0, 0, 0, 0
CAPTION "Select Type"
BEGIN
	LISTBOX					2005,	 10,  10, 82,  60, XS_LISTBOX

	DEFPUSHBUTTON   "OK",			IDOK,		 10, 80,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	 54, 80,  40,  14, XS_BUTTONREST
END

//////////////////////////////////////////////////////////////////////////
//
// Impact User Defined Data Dialog
//
//

ImpactUserDlg DIALOG 0, 0, 0, 0
CAPTION "Select User Defined Data"
BEGIN
	LISTBOX					2007,		  10,  10, 282, 262, XS_LISTBOX

	DEFPUSHBUTTON   "OK",			IDOK,		 204, 280,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	 248, 280,  40,  14, XS_BUTTONREST
END

//////////////////////////////////////////////////////////////////////////
//
// Impact List Management Dialog
//
//

ImpactManageDlg DIALOG 0, 0, 0, 0
CAPTION "Manage Impact Camera Lists"
BEGIN
	GROUPBOX	"&Vision System",	1000,	  4,   4, 385, 134
	LISTBOX					1001,	 10,  18,  60,  60, XS_LISTBOX
	LISTBOX					1002,	 84,  18, 298, 120, XS_LISTBOX

	DEFPUSHBUTTON   "Add List Item",	2001,	 10,  84,  60,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Delete List Item",	2002,	 10, 100,  60,  14, XS_BUTTONREST
	PUSHBUTTON	"Reset To Default",	2003,	 10, 116,  60,  14, XS_BUTTONREST
	
	//DEFPUSHBUTTON   "OK",			IDOK,		163, 144,  40,  14, XS_BUTTONFIRST
	//PUSHBUTTON	"Cancel",		IDCANCEL,	207, 144,  40,  14, XS_BUTTONREST
	DEFPUSHBUTTON	"Close",		IDCANCEL,	351, 144,  40,  14, XS_BUTTONFIRST
END


// End of File
