
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "ciadio.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "ciadio.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "ciadio.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "ciadio.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

///////////////////////////////////////////////////////////////////////////
//
// String Table
//
//

STRINGTABLE
BEGIN
	IDS_CAN_CHNG_INC	"This change will require 'Validate All Tags' to be executed to correct all references to this device.\n\nDo you want to proceed?"
	IDS_CAN_EDS	        "Electronic Data Sheet"
	IDS_CAN_EDS_UPDATE	"Create/Update EDS File"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CCANOpenSDODeviceOptions
//

CCANOpenSDODeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Settings,Drop,Init,Delay,Ping,Time,BackOff\0"
	"G:1,root,Element Options,Offset,Type,Increment,Entry\0"
	"\0"
END

CCANOpenSDODeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,0|0||0|255,"
	"Select the drop number of the target device."
	"\0"
	
	"Init,Send NMT Start,,CUIDropDown,No|Yes,"
	"Indicate whether the G3 should send a NMT Start command to the target "
	"device. Note that devices should not need this command, but that some "
	"CANOpen implementations will not respond to SDO reads with the correct "
	"data unless the Start is sent. Do not select this option unless you "
	"cannot get valid data without it, or network performance will suffer."
	"\0"
	
	"Delay,Communications Delay,,CUIEditInteger,0|0|msec|0|2000,"
	"Indicate the period of time for which the driver should wait after sending " 
	"a NMT Start command before resuming communications.  This setting should "
	"represent the amount of time the target device requires to initialize."
	"\0"
		
	"Ping,Ping Register (Hex),,CUIEditString,5,"
	"Indicate the Code Number that the driver should use to detect the online "
	"device.  You must be sure that this register is valid in the target device or "
	"the connection will never be established."
	"\0"
		 
	"Offset,Index Offset (Hex),,CUIEditString,5,"
	"Indicate the Index offset, if any, that should be used to calculate each Element "
	"for the target device."
	"\0"
       	
	"Type,Index Calculation,,CUIDropDown,Index = Offset - Code|Index = Offset + Code,"
	"Indicate the calculation that should be used to determine the actual Index "
	"for this device."
	"\0"

	"Time,Transaction Timeout,,CUIEditInteger,0|0|msec|25|2000,"
	"Indicate the period of time for which the driver should wait for a reply to a CAN request "
	"for this device."
	"\0"
	
	"Restrict,Restrict Element,,CUIDropDown,No|Disable Sub-Index Only|Disable Sub-Index and Limit Index (0xFF),"
	"Element restrictions should be used for custom applications only.  Any locations freed by these restrictions "
	"will be used for data transfer."
	"\0"

	"Increment,Increment,,CUIDropDown,Index|Sub-Index,"
	"Indicate how the element should increment when the 'Next' address or the indices of an array are selected."
	"\0"

	"Entry,Index Entry,,CUIDropDown,Hexadecimal|Decimal,"
	"Indicate the mode of entry for the Element Index."
	"\0"

	"Transfer,Data Transfer,,CUIDropDown,Expedited|Normal,"
	""
	"\0"	

	"BackOff,Write Delay,,CUIEditInteger,0|0|msec|0|5000,"
	"Indicate the mimimum update period for consecutive writes to the same location."
	"\0"

	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CCANOpenSlaveDriverOptions
//

CCANOpenSlaveDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Driver Settings,Drop,NodeGuard,Increment\0"
	"G:1,root,Electronic Data Sheet,Update,File\0"
	"\0"
END

CCANOpenSlaveDriverOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,0|0||0|255,"
	"Select the drop number of this target device."
	"\0"

	"Increment,Increment,,CUIDropDown,Index|Sub-Index,"
	"Indicate how the element should change between elements in a gateway block."
	"\0"

	"NodeGuard,Enable Node Guarding,,CUIDropDown,No|Yes,"
	"Enable node guarding according to the CANOpen Master device."
	"\0"

	"Update,File Update,,CUIDropDown,Manual|Automatic,"
	"In order for communications to be successful, the data transfer settings "
	"configured here must match those in the master. To aid network configuration, "
	"an Electronic Data Sheet (EDS file) is supplied, however changes made here "
	"must be replicated in the EDS file, and the EDS file re-registered with your "
	"network configuration tool.  Select the appropriate mode for EDS file updates."
	"\0"
     
	"File,Filename,,CUIPickFile,EDS|Electronic Data Sheet (*.eds)|*.eds,"
	"Specify the name and location of the Electronic Data Sheet (EDS) file to be "
	"updated. Any changes must be re-registered with your network configuration "
	"tool."
	"\0"
  	    
	"\0"
END
 
//////////////////////////////////////////////////////////////////////////
//
// UI for CCANOpenPDOSlaveDriverOptions
//

CCANOpenPDOSlaveDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Driver Settings,Drop,NodeGuard,Op\0"
	"G:1,root,Electronic Data Sheet,Update,File\0"
	"\0"
END

CCANOpenPDOSlaveDriverOptionsUIList RCDATA
BEGIN
	"Profile,Device Profile,,CUIDropDown,None|Generic I/O Module (DS-401),"
	"\0"

	"Op,Begin in the Operational State,,CUIDropDown,No|Yes,"
	"Indicate whether the G3 should begin in the Operation State.  Selecting yes "
	"is useful if the target device does not support NMT services.  "
	"\0"
       
	"\0"
END
 
//////////////////////////////////////////////////////////////////////////
//
// CANOpen Element Dialog
//
//

CANOpenElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT		2002,	 0, 0, 24, 12, 
	CTEXT		".",	2004,	25, 1, 12, 12
	EDITTEXT		2005,	40, 0, 20, 12,
END

/////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Configuration Dialog
//

CANOpenPDODlg DIALOG 0, 0, 0, 0
CAPTION "PDO Configurator"
BEGIN
	////////
	GROUPBOX	"Process Data Object",		1000,		  4,   4, 85, 182
	CONTROL		"",				1001, "CTreeView", WS_TABSTOP, 10,  18, 70, 162, WS_EX_CLIENTEDGE
	
	////////
	GROUPBOX	"Object",			2000,		 93,   4, 130,  83
	CTEXT		"Object Type:	",		2003		111,  18,  50,  12, 
	COMBOBOX					2004,		177,  16,  41,  36, XS_DROPDOWNLIST | CBS_HASSTRINGS
    	CTEXT		"Object Number:	",		2001,		111,  36,  50,  12,
	EDITTEXT					2002,		177,  34,  40,  12, 

	////////

	CTEXT		"Manual Event:	"		2005,		111,  54,  50,  12,
	COMBOBOX					2006		177,  52,  41,	24, XS_DROPDOWNLIST | CBS_HASSTRINGS
	CTEXT		"Event Timer (ms):   "		2007,		111,  72,  50,  12,
	EDITTEXT					2008,		177,  70,  40,  12,

       
	////////
	GROUPBOX	"PDO Element (Hexadecimal)",	3000,		 93,   91, 130,  73
	CTEXT		"Index: ",			3001,		111,  105,  50,  12,
	EDITTEXT					3002,		173,  103,  20,  12, 
	CTEXT		"/",				3003,		193,  105,  12,  12,
	EDITTEXT					3004,		203,  103,  14,  12,

	CTEXT		"Size:   ",			3005,		111,  124,  50,  12,
	COMBOBOX					3006,		173,  122,  44,  36, XS_DROPDOWNLIST | CBS_HASSTRINGS
	PUSHBUTTON      "Add &Element",			3008,		 98,  144, 120,  14, XS_BUTTONREST
		
	
	DEFPUSHBUTTON	"OK",		IDOK,		 93, 172,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	137, 172,  40,  14, XS_BUTTONREST
//	PUSHBUTTON	"Help",		IDHELP,		181, 172,  40,  14, XS_BUTTONREST
END

// End of File
