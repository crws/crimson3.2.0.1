
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BECKHOFF_HPP
	
#define	INCLUDE_BECKHOFF_HPP

#define	AN	addrNamed
#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff ADS TCP/IP Master Device Options
//

class CBeckhoffADSTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBeckhoffADSTCPDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_AMSPort;
		UINT	m_AMSID;
		UINT	m_AMSTail;
		BOOL	m_fUseIP;
		UINT	m_Addr;
		UINT	m_Socket;
		UINT	m_Keep;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff ADS TCP/IP Master Driver
//

class CBeckhoffADSTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBeckhoffADSTCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);

	protected:

		// Implementation
		void	AddSpaces(void);
		BOOL	CheckAlignment(CSpace *pSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Address Selection
//

class CBeckhoffADSTCPAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBeckhoffADSTCPAddrDialog(CBeckhoffADSTCPDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		BOOL	AllowType(UINT uType);
	};

// End of File

#endif
