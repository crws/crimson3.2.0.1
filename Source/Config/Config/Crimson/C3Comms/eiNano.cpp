
#include "intern.hpp"

#include "eiNano.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 2009-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm/Invensys NanoDac TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEuroInvensysNanoDacTCPDeviceOptions, CUIItem);       

// Constructor

CEuroInvensysNanoDacTCPDeviceOptions::CEuroInvensysNanoDacTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CEuroInvensysNanoDacTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CEuroInvensysNanoDacTCPDeviceOptions::MakeInitData(CInitData &Init)
{	
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CEuroInvensysNanoDacTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Comms Driver
//

// Constructor

CEuroInvensysNanoDacDriver::CEuroInvensysNanoDacDriver(void)
{
	AddSpaces();

	InitEIE();
	}

// Destructor

CEuroInvensysNanoDacDriver::~CEuroInvensysNanoDacDriver(void)
{
	Clear_EIE();

	DeleteAllSpaces();
	}

// Address Management

BOOL CEuroInvensysNanoDacDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CEuroInvensysNanoDacAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL  CEuroInvensysNanoDacDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	m_pEIE->uTable	= pSpace->m_uTable;

	if( !(IsNotStdModbus(pSpace->m_uTable)) ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	UINT uTable	= pSpace->m_uTable;

	Addr.a.m_Table	= uTable;

	UINT uOffset	= tatoi(Text);

	UINT uPos	= GetPosFromOffset(uTable, uOffset);

	CString s	= GetParamString(uTable, uPos);

	UINT uFind	= s.Find('|');

	Addr.a.m_Extra	= 0;

	switch( s[uFind - 1] ) {

		case 'F': Addr.a.m_Type = RR; break;				// Reals

		case 'T': Addr.a.m_Type	= LL;	Addr.a.m_Extra = 2; break;	// Time

		case 'L': Addr.a.m_Type = LL;	Addr.a.m_Extra = 1;	break;	// 32 bit int

		default:  Addr.a.m_Type  = WW; break;				// 16 bit int
		}

	if( uOffset > pSpace->m_uMaximum ) {

		uOffset = 0;
		}

	m_pEIE->uOffset	= uOffset;

	Addr.a.m_Offset = uOffset;

	return TRUE;
	}

BOOL  CEuroInvensysNanoDacDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uTable	= Addr.a.m_Table;

		m_pEIE->uTable	= uTable;

		if( !IsNotStdModbus(uTable ) ) {

			return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		UINT uType	= Addr.a.m_Type;

		UINT uOffset	= Addr.a.m_Offset;

		CString sName	= ParseParamString( GetParamString( uTable, GetPosFromOffset(uTable, uOffset) ), uOffset);

		CString sType	= uType == WW ? L"" : uType == RR ? L".REAL" : L".LONG";

		Text.Printf(TEXT("%s%d_%s%s"),

				pSpace->m_Prefix,
				uOffset,
				sName,
				sType
				);

		return TRUE;
		}

	return FALSE;
	}

EIESTRINGS * CEuroInvensysNanoDacDriver::GetEIEPtr(void)
{
	return m_pEIE;
	}

BOOL CEuroInvensysNanoDacDriver::IsNotStdModbus(UINT uTable)
{
	return uTable >= SP_ALSC && uTable <= SP_ZIR;
	}

BOOL CEuroInvensysNanoDacDriver::HasBlocks(void)
{
	return m_pEIE->uBlkCt[m_pEIE->uTable] > 1;
	}

BOOL CEuroInvensysNanoDacDriver::IsUsrLin(void) {

	return m_pEIE->uTable == SP_USRL;
	}

CString CEuroInvensysNanoDacDriver::GetParamString(UINT uTable, UINT uSelect)
{
	CString s = SNOTU;

	switch( uTable ) {

		case SP_ALSC:	s = LoadAlarmSumChan(uSelect);	break;
		case SP_ALSG:	s = LoadAlarmSumGbl(uSelect);	break;
		case SP_ALSS:	s = LoadAlarmSumSys(uSelect);	break;
		case SP_BCD:	s = LoadBCD(uSelect);		break;
		case SP_CALA:	s = LoadChanAlmData(uSelect);	break;
 		case SP_CHAS:	s = LoadChanAlarm(uSelect);	break;
 		case SP_CHMN:	s = LoadChanMain(uSelect);	break;
 		case SP_CMSG:	s = LoadCustom(uSelect);	break;
		case SP_DCO:	s = LoadDCOut(uSelect);		break;
		case SP_DIO:	s = LoadDigitalIO(uSelect);	break;
		case SP_GRCD:	s = LoadGroupRecord(uSelect);	break;
		case SP_GTRD:	s = LoadGroupTrend(uSelect);	break;
		case SP_HUM:	s = LoadHumidity(uSelect);	break;
		case SP_INSG:	s = LoadInstrGeneral(uSelect);	break;
		case SP_INSS:	s = LoadInstrString(uSelect);	break;
		case SP_LGC2:	s = LoadLgc2(uSelect);		break;
		case SP_LGC8:	s = LoadLgc8(uSelect);		break;
		case SP_LDG1:	s = LoadLoopDiagMain1(uSelect);	break;
		case SP_LDG2:	s = LoadLoopDiagMain2(uSelect);	break;
		case SP_LOP1:	s = LoadLoopOP1(uSelect);	break;
		case SP_LOP2:	s = LoadLoopOP2(uSelect);	break;
		case SP_LPD1:	s = LoadLoopPID1(uSelect);	break;
		case SP_LPD2:	s = LoadLoopPID2(uSelect);	break;
		case SP_LSET:	s = LoadLoopSetup(uSelect);	break;
		case SP_LSP1:	s = LoadLoopSP1(uSelect);	break;
		case SP_LSP2:	s = LoadLoopSP2(uSelect);	break;
		case SP_LTUN:	s = LoadLoopTune(uSelect);	break;
		case SP_MATH:	s = LoadMath2(uSelect);		break;
		case SP_MUX:	s = LoadMux8(uSelect);		break;
		case SP_NANO:	s = LoadNano(uSelect);		break;
		case SP_NET:	s = LoadNetwork(uSelect);	break;
		case SP_NETS:	s = LoadNetStr(uSelect);	break;
		case SP_OR:	s = LoadOR(uSelect);		break;
		case SP_STER:	s = LoadSterilizer(uSelect);	break;
		case SP_STRS:	s = LoadStrings(uSelect);	break;
		case SP_TIME:	s = LoadTimer(uSelect);		break;
		case SP_USRL:	s = LoadUsrLin(uSelect);	break;
		case SP_USRV:	s = LoadUsrVal(uSelect);	break;
		case SP_VAAS:	s = LoadVirtAlmStat(uSelect);	break;
		case SP_VALA:	s = LoadVirtAlarm(uSelect);	break;
		case SP_VMT:	s = LoadVirtMain(uSelect);	break;
		case SP_ZIR:	s = LoadZirconia(uSelect);	break;
		}

	return s;
	}

CString CEuroInvensysNanoDacDriver::ParseParamString(CString sParam, UINT uOffset)
{
	UINT uTable	= m_pEIE->uTable;

	UINT uFind	= sParam.Find('@');

	CString sName	= sParam.Left(uFind);

	CString sHead	= m_pEIE->sName[uTable];

	UINT uBlock = 1;

	if( uTable == SP_USRL ) {

		uBlock = uOffset;
		}

	else {
		uFind = sParam.Find('$');

		UINT uInc = tatoi(sParam.Mid(uFind + 1));

		if( uInc ) {

			UINT uAdjust = (m_pEIE->uQty[uTable] - uInc) * m_pEIE->uBlkCt[uTable];

			if( uAdjust ) {	// multi-range table

				if( uOffset >= uAdjust ) {	// item is in upper range

					uOffset -= uAdjust;
					}
				}

			uBlock = (uOffset / uInc) + 1;
			}

		m_pEIE->uIncr = uInc;
		}

	CString sBlk;

	switch( m_pEIE->uTable ) {

		case SP_DCO:
			sBlk = m_pEIE->sDCO[uBlock-1] + m_pEIE->sDCO[3];
			return sHead + sBlk + sName;

		case SP_DIO:
			sBlk = m_pEIE->sDIO[uBlock-1];
			return sHead + sBlk + sName;
		}

	uFind		= sHead.Find('[');

	sBlk.Printf( L"[%d]", uBlock);

	if( uFind < NOTHING ) {

		UINT uBPos0	= uFind;
		UINT uBPos1 = sHead.Find(']');

		if( IsUsrLin() ) {

			sBlk.Printf( L"[%d]", (uBlock / 65) + 1);

			if( uBlock % 65 ) {

				sName.Printf( L"%s%d", sName, GetUsrLinSegm(uBlock));
				}
			}

		sName.Printf( L"%s%s%s%s", sHead.Left(uBPos0), sBlk, sHead.Mid(uBPos1+1), sName);
		}

	else {
		sName.Printf( L"%s%s", sHead, sName );
		}

	return sName;
	}

UINT CEuroInvensysNanoDacDriver::GetPosFromOffset(UINT uTable, UINT uOffset)
{
	EIESTRINGS *pE	= m_pEIE;

	UINT uQty	= pE->uQty[uTable];

	UINT i		= 1;

	UINT uBlkCt	= m_pEIE->uBlkCt[uTable];

	while( i <= uQty ) {

		CString s  = GetParamString(uTable, i);

		UINT uFind = s.Find('|');

		UINT uBase = tatoi(s.Mid(uFind + 1));

		if( uBase == uOffset ) {

			return i;
			}

		else {
			if( IsUsrLin() ) {

				if( uOffset % 65 ) {	// is X or Y value

					UINT u = uOffset;

					while( u > 65 ) {

						u -= 65;
						}

					return (u % 2) ? 2 : 3;
					}

				return 1;
				}

			uFind     = s.Find('$');

			m_pEIE->uIncr = tatoi(s.Mid(uFind + 1));

			UINT uInc = m_pEIE->uIncr;

			for( UINT k = 0; k < uBlkCt; k++ ) {

				if( uBase + ( k * uInc ) == uOffset ) {

					return i;
					}
				}
			}

		i++;
		}

	return 1;
	}

BOOL CEuroInvensysNanoDacDriver::IsStringSelect(UINT uTable)
{
	switch( uTable ) {

		case SP_INSS:
		case SP_NETS:
		case SP_STRS: return TRUE;
		}

	return FALSE;
	}

UINT CEuroInvensysNanoDacDriver::GetUsrLinSegm(UINT uOffset)
{
	return ((uOffset % 65) + 1) / 2;
	}

// Implementation

void  CEuroInvensysNanoDacDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SP_ALSC,	"AC",  "Alarm Summary - Channel",	10,	0,	107,	WW, LL));
	AddSpace(New CSpace(SP_ALSG,	"AG",  "Alarm Summary - Global",	10,	0,	8,	WW, LL));
	AddSpace(New CSpace(SP_ALSS,	"AS",  "Alarm Summary - System",	10,	0,	31,	WW, LL));
	AddSpace(New CSpace(SP_BCD,	"BD",  "BCDInput",			10,	0,	23,	WW, LL));
	AddSpace(New CSpace(SP_CALA,	"CA",  "Channel Alarm Values",		10,	0,	135,	WW, RR));
	AddSpace(New CSpace(SP_CHAS,	"CS",  "Channel/Alarm Ack and Status",	10,	0,	15,	WW, RR));
	AddSpace(New CSpace(SP_CHMN,	"CN",  "Channel Main",			10,	0,	147,	WW, RR));
	AddSpace(New CSpace(SP_CMSG,	"CG",  "Custom Message/Triggers",	10,	0,	1020,	WW, LL));
	AddSpace(New CSpace(SP_DCO,	"DO",  "DC Output",			10,	0,	32,	WW, RR));
	AddSpace(New CSpace(SP_DIO,	"DI",  "Digital IO",			10,	0,	76,	WW, RR));
	AddSpace(New CSpace(SP_GRCD,	"GR",  "Group Recording",		10,	0,	32,	WW, RR));
	AddSpace(New CSpace(SP_GTRD,	"GT",  "Group Trend",			10,	0,	9,	WW, LL));
	AddSpace(New CSpace(SP_HUM,	"HU",  "Humidity",			10,	0,	9,	WW, RR));
	AddSpace(New CSpace(SP_INSG,	"IG",  "Instrument - General",		10,	0,	94,	WW, RR));
	AddSpace(New CSpace(SP_INSS,	"IS",  "Instrument - Strings",		10,	0,	2099,	WW, LL));
	AddSpace(New CSpace(SP_LGC2,	"L2G", "Logic 2",			10,	0,	83,	WW, RR));
	AddSpace(New CSpace(SP_LGC8,	"L8G", "Logic 8",			10,	0,	25,	WW, LL));
	AddSpace(New CSpace(SP_LDG1,	"L1D", "Loop 1 Diagnostics/Main",	10,	0,	32,	WW, RR));
	AddSpace(New CSpace(SP_LDG2,	"L2D", "Loop 2 Diagnostics/Main",	10,	0,	32,	WW, RR));
	AddSpace(New CSpace(SP_LOP1,	"L1O", "Loop OP1",			10,	0,	37,	WW, RR));
	AddSpace(New CSpace(SP_LOP2,	"L2O", "Loop OP2",			10,	0,	37,	WW, RR));
	AddSpace(New CSpace(SP_LPD1,	"L1P", "Loop PID1",			10,	0,	34,	WW, RR));
	AddSpace(New CSpace(SP_LPD2,	"L2P", "Loop PID2",			10,	0,	34,	WW, RR));
	AddSpace(New CSpace(SP_LSET,	"LS",  "Loop Setup",			10,	0,	17,	WW, LL));
	AddSpace(New CSpace(SP_LSP1,	"L1S", "Loop SP1",			10,	0,	20,	WW, RR));
	AddSpace(New CSpace(SP_LSP2,	"L2S", "Loop SP2",			10,	0,	20,	WW, RR));
	AddSpace(New CSpace(SP_LTUN,	"LT",  "Loop Tune",			10,	0,	15,	WW, RR));
	AddSpace(New CSpace(SP_MATH,	"MA",  "Math2",				10,	0,	168,	WW, RR));
	AddSpace(New CSpace(SP_MUX,	"MU",  "Mux8",				10,	0,	63,	WW, RR));
	AddSpace(New CSpace(SP_NANO,	"NN",  "Nano access",			10,	0,	0,	WW, LL));
	AddSpace(New CSpace(SP_NET,	"NW",  "Network Data",			10,	0,	52,	WW, RR));
	AddSpace(New CSpace(SP_NETS,	"NS",  "Network Strings",		10,	0,	1249,	WW, LL));
	AddSpace(New CSpace(SP_OR,	"OR",  "OR Block access",		10,	0,	107,	WW, LL));
	AddSpace(New CSpace(SP_STER,	"SR",  "Sterilizer",			10,	0,	45,	WW, RR));
	AddSpace(New CSpace(SP_STRS,	"SS",  "General String responses",	10,	0,	1603,	WW, LL));
	AddSpace(New CSpace(SP_TIME,	"TM",  "Timers",			10,	0,	23,	WW, LL));
	AddSpace(New CSpace(SP_USRL,	"UL",  "User Linearize",		10,	0,	259,	WW, RR));
	AddSpace(New CSpace(SP_USRV,	"UV",  "User Values",			10,	0,	59,	WW, RR));
	AddSpace(New CSpace(SP_VAAS,	"VS",  "Virt Alarm Status",		10,	0,	237,	WW, RR));
	AddSpace(New CSpace(SP_VALA,	"VA",  "Virt Alarm Data",		10,	0,	237,	WW, RR));
	AddSpace(New CSpace(SP_VMT,	"VM",  "Virt Main/Trend",		10,	0,	307,	WW, RR));
	AddSpace(New CSpace(SP_ZIR,	"ZR",  "Zirconia",			10,	0,	69,	WW, RR));
	}

// Helpers

void CEuroInvensysNanoDacDriver::InitEIE(void)
{
	m_pEIE	= &EIESTR;

	SetSelectionInfo();
	}

void CEuroInvensysNanoDacDriver::SetSelectionInfo(void)
{
	CString s;

	EIESTRINGS *pE	= m_pEIE;

	for( UINT n = 1; n < NUMT; n++ ) {

		switch( n ) {

			case SP_ALSC: s = L"AlarmSummary.Channel.Alarm[n].Q3[36";	break;
			case SP_ALSG: s = L"AlarmSummary.Q9[1";				break;
			case SP_ALSS: s = L"AlarmSummary.System.Alarm[n].Q1[32";	break;
			case SP_BCD:  s = L"BCDInput[n].Q12[2";				break;
			case SP_CALA: s = L"Channel[n].AlarmQ34[4";			break;
			case SP_CHAS: s = L"Channel[n].Q4[4";				break;
			case SP_CHMN: s = L"Channel[n].Q37[4";				break;
			case SP_CMSG: s = L"CustomMessage.Q20[1";			break;
			case SP_DCO:  s = L"DCOutput.Q11[3";				break;
			case SP_DIO:  s = L"DigitalIO.Q11[7";				break;
			case SP_GRCD: s = L"Group.Recording.Q33[1";			break;
			case SP_GTRD: s = L"Group.Trend.Q10[1";				break;
			case SP_HUM:  s = L"Humidity.Q10[1";				break;
			case SP_INSG: s = L"Instrument.Q95[1";				break;
			case SP_INSS: s = L"Instrument.Q29[1";				break;
			case SP_LGC2: s = L"Lgc2[n].Q7[12";				break;
			case SP_LGC8: s = L"Lgc8[n].Q13[2";				break;
			case SP_LDG1: s = L"Loop.1.Q33[1A22B22";			break;
			case SP_LDG2: s = L"Loop.2.Q33[1A22B22";			break;
			case SP_LOP1: s = L"Loop1.OP.Q38[1";				break;
			case SP_LOP2: s = L"Loop.2.OP.Q38[1";				break;
			case SP_LPD1: s = L"Loop1.PID.Q35[1";				break;
			case SP_LPD2: s = L"Loop.2.PID.Q35[1";				break;
			case SP_LSET: s = L"Loop.Q16[1";				break;
			case SP_LSP1: s = L"Loop.1.SP.Q21[1";				break;
			case SP_LSP2: s = L"Loop.2.SP.Q21[1";				break;
			case SP_LTUN: s = L"Loop.Q16[1";				break;
			case SP_MATH: s = L"Math2[n].Q13[13";				break;
			case SP_MUX:  s = L"Mux8[n].Q16[4";				break;
			case SP_NANO: s = L"nano_ui.Q1[1";				break;
			case SP_NET:  s = L"Network.Q53[1A31B31";			break;
			case SP_NETS: s = L"Network.Q17[1";				break;
			case SP_OR:   s = L"OR[n].Q9[12";				break;
			case SP_STER: s = L"Steriliser.Q46[1";				break;
			case SP_STRS: s = L"Q65[1";					break;
			case SP_TIME: s = L"Timer[n].Q6[4";				break;
			case SP_USRL: s = L"UserLin[n].Q3[4";				break;
			case SP_USRV: s = L"UsrVal[n].Q5[12";				break;
			case SP_VAAS: s = L"VirtualChannel[n].AlarmQ4[14";		break;
			case SP_VALA: s = L"VirtualChannel[n].AlarmQ34[14";		break;
			case SP_VMT:  s = L"VirtualChannel[n].Q22[14";			break;
			case SP_ZIR:  s = L"Zirconia.Q70[1";				break;
			}

		UINT uFind	= s.FindRev('Q');

		pE->sName[n]	= s.Left(uFind);

		pE->uQty[n]	= tatoi(s.Mid(uFind + 1));

		uFind	= s.FindRev('[');

		pE->uBlkCt[n]	= tatoi(s.Mid(uFind+1));
		}

	pE->sDCO[0]	= SDCOA;
	pE->sDCO[1]	= SDCOB;
	pE->sDCO[2]	= SDCOC;
	pE->sDCO[3]	= SDCOD;

	pE->sDIO[0]	= SDIOA;
	pE->sDIO[1]	= SDIOB;
	pE->sDIO[2]	= SDIOC;
	pE->sDIO[3]	= SDIOD;
	pE->sDIO[4]	= SDIOE;
	pE->sDIO[5]	= SDIOF;
	pE->sDIO[6]	= SDIOG;

	// Initial values specific to SP_ALSC
	pE->uTable	= SP_ALSC;
	pE->uAddr	= 4496;
	pE->uNPos	= 1;
	pE->uOffset	= 0;

	pE->uBlock	= 1;
	pE->uIncr	= 0;
	pE->uBPos	= 0;
	pE->uSPos	= 0;
	pE->uSegm	= 1;
	}

void CEuroInvensysNanoDacDriver::Clear_EIE(void)
{
	EIESTRINGS * pE = m_pEIE;

	if( pE ) {

		UINT i;

		for( i = 0; i < NUMT; i++ ) {

			pE->sName[i] = L"";
			}

		for( i = 0; i < 4; i++ ) {

			pE->sDCO[i]  = L"";
			}

		for( i = 0; i < 7; i++ ) {

			pE->sDIO[i]  = L"";
			}
		}
	}

// String Loading
CString CEuroInvensysNanoDacDriver::LoadAlarmSumChan(UINT uSelect)
{
// SP_ALSC
	switch( uSelect % 4 ) {

		case 2: return	L"Num@4496#3$3|0";
		case 3: return	L"Status@4497#3$3|1";
		}

	return	L"Ack@4498#3$3|2";
	}

CString CEuroInvensysNanoDacDriver::LoadAlarmSumGbl(UINT uSelect)
{
// SP_ALSG
	switch( uSelect % 10) {

		case 2: return	L"AnyChanAlarm@416$0|0";
		case 3: return	L"AnySystemAlarm@417$0|1";
		case 4: return	L"GlobalAck@419$0|3";
		case 5: return	L"StatusWord_1@420$0|4";
		case 6: return	L"StatusWord_2@421$0|5";
		case 7: return	L"StatusWord_3@422$0|6";
		case 8: return	L"StatusWord_4@423$0|7";
		case 9: return	L"StatusWord_5@424$0|8";
		}

	return	L"AnyAlarm@418$0|2";
	}

CString CEuroInvensysNanoDacDriver::LoadAlarmSumSys(UINT uSelect)
{
// SP_ALSS
	return	L"ID@4624#1$1|0";
	}

CString CEuroInvensysNanoDacDriver::LoadBCD(UINT uSelect)
{
// SP_BCD
	switch( uSelect % 13 ) {

		case  2: return	L"DecByte@11984#12$12|8";
		case  3: return	L"In1@11976#12$12|0";
		case  4: return	L"In2@11977#12$12|1";
		case  5: return	L"In3@11978#12$12|2";
		case  6: return	L"In4@11979#12$12|3";
		case  7: return	L"In5@11980#12$12|4";
		case  8: return	L"In6@11981#12$12|5";
		case  9: return	L"In7@11982#12$12|6";
		case 10: return	L"In8@11983#12$12|7";
		case 11: return	L"Tens@11987#12$12|11";
		case 12: return	L"Units@11986#12$12|10";
		}

	return	L"BCDVal@11985#12$12|9";
	}

CString CEuroInvensysNanoDacDriver::LoadChanAlmData(UINT uSelect)
{
// SP_CALA
	switch( uSelect % 35 ) {

		case  2: return	L"1.Active@6219#128$34|11";
		case  3: return	L"1.Amount@6216#128$34F|8";
		case  4: return	L"1.AverageTime@6218#128$34T|10";
		case  5: return	L"1.Block@6210#128$34|2";
		case  6: return	L"1.ChangeTime@6217#128$34|9";
		case  7: return	L"1.Deviation@6215#128$34F|7";
		case  8: return	L"1.Dwell@6213#128$34T|5";
		case  9: return	L"1.Hysteresis@6212#128$34F|4";
		case 10: return	L"1.Inactive@6222#128$34|14";
		case 11: return	L"1.Latch@6209#128$34|1";
		case 12: return	L"1.NotAcknowledged@6223#128$34|15";
		case 13: return	L"1.Reference@6214#128$34F|6";
		case 14: return	L"1.Threshold@6211#128$34F|3";
		case 15: return	L"1.Type@6208#128$34|0";
		case 16: return	L"2.Acknowledgement@6256#128$34|33";
		case 17: return	L"2.Active@6251#128$34|28";
		case 18: return	L"2.Amount@6248#128$34F|25";
		case 19: return	L"2.AverageTime@6250#128$34T|27";
		case 20: return	L"2.Block@6242#128$34|19";
		case 21: return	L"2.ChangeTime@6249#128$34|26";
		case 22: return	L"2.Deviation@6247#128$34F|24";
		case 23: return	L"2.Dwell@6245#128$34T|22";
		case 24: return	L"2.Hysteresis@6244#128$34F|21";
		case 25: return	L"2.Inactive@6254#128$34|31";
		case 26: return	L"2.Latch@6241#128$34|18";
		case 27: return	L"2.NotAcknowledged@6255#128$34|32";
		case 28: return	L"2.Reference@6246#128$34F|23";
		case 29: return	L"2.Threshold@6243#128$34F|20";
		case 30: return	L"2.Type@6240#128$34|17";
		case 31: return	L"1.x6220n@6220#128$34|12";
		case 32: return	L"1.x6221n@6221#128$34|13";
		case 33: return	L"2.x6252n@6252#128$34|29";
		case 34: return	L"2.x6253n@6253#128$34|30";
		}

	return	L"1.Acknowledgement@6224#128$34|16";
	}

CString CEuroInvensysNanoDacDriver::LoadChanAlarm(UINT uSelect)
{
// SP_CHAS
	switch( uSelect % 5 ) {

		case 2: return	L"Alarm1.Status@258#4$2|0";
		case 3: return	L"Alarm2.Acknowledge@433#2$2|9";
		case 4: return	L"Alarm2.Status@259#4$2|1";
		}

	return	L"Alarm1.Acknowledge@432#2$2|8";
	}

CString CEuroInvensysNanoDacDriver::LoadChanMain(UINT uSelect)
{
// SP_CHMN
	switch( uSelect % 38 ) {

		case  2: return	L"Main.ExtCJTemp@6157#128$35|21";
		case  3: return	L"Main.FaultResponse@6160#128$35|24";
		case  4: return	L"Main.Filter@6158#128$35F|22";
		case  5: return	L"Main.InputHigh@6148#128$35F|12";
		case  6: return	L"Main.InputLow@6147#128$35F|11";
		case  7: return	L"Main.InternalCJTemp@6165#128$35F|29";
		case  8: return	L"Main.IPAdjustState@6166#128$35|30";
		case  9: return	L"Main.LinType@6150#128$35|14";
		case 10: return	L"Main.MeasuredValue@6164#128$35F|28";
		case 11: return	L"Main.Offset@6167#128$35F|31";
		case 12: return	L"Main.PV@256#4$2F|0";
		case 13: return	L"Main.RangeHigh@6152#128$35F|16";
		case 14: return	L"Main.RangeLow@6151#128$35F|15";
		case 15: return	L"Main.RangeUnits@6153#128$35|17";
		case 16: return	L"Main.Resolution@6145#128$35|9";
		case 17: return	L"Main.ScaleHigh@6155#128$35F|19";
		case 18: return	L"Main.ScaleLow@6154#128$35F|18";
		case 19: return	L"Main.SensorBreakType@6159#128$35|23";
		case 20: return	L"Main.SensorBreakVal@6161#128$35|25";
		case 21: return	L"Main.Shunt@6149#128$35F|13";
		case 22: return	L"Main.Status@257#4$2|1";
		case 23: return	L"Main.TestSignal@6146#128$35|10";
		case 24: return	L"Main.Type@6144#128$35|8";
		case 25: return	L"Trend.Colour@6176#128$35|40";
		case 26: return	L"Trend.SpanHigh@6178#128$35F|42";
		case 27: return	L"Trend.SpanLow@6177#128$35F|41";
		case 28: return	L"z6162n@6162#128$35|26";
		case 29: return	L"z6163n@6163#128$35|27";
		case 30: return	L"z6168n@6168#128$35|32";
		case 31: return	L"z6169n@6169#128$35|33";
		case 32: return	L"z6170n@6170#128$35|34";
		case 33: return	L"z6171n@6171#128$35|35";
		case 34: return	L"z6172n@6172#128$35|36";
		case 35: return	L"z6173n@6173#128$35|37";
		case 36: return	L"z6174n@6174#128$35|38";
		case 37: return	L"z6175n@6175#128$35|39";
		}

	return	L"Main.CJType@6156#128$35F|20";
	}

CString CEuroInvensysNanoDacDriver::LoadCustom(UINT uSelect)
{
// SP_CMSG
	switch( uSelect % 21 ) {

		case  2: return	L"Message2@24165$101|111";
		case  3: return	L"Message3@24266$101|212";
		case  4: return	L"Message4@24367$101|313";
		case  5: return	L"Message5@24468$101|414";
		case  6: return	L"Message6@24569$101|515";
		case  7: return	L"Message7@24670$101|616";
		case  8: return	L"Message8@24771$101|717";
		case  9: return	L"Message9@24872$101|818";
		case 10: return	L"Message10@24973$101|919";
		case 11: return	L"Trigger1@10480$0|0";
		case 12: return	L"Trigger2@10481$0|1";
		case 13: return	L"Trigger3@10482$0|2";
		case 14: return	L"Trigger4@10483$0|3";
		case 15: return	L"Trigger5@10484$0|4";
		case 16: return	L"Trigger6@10485$0|5";
		case 17: return	L"Trigger7@10486$0|6";
		case 18: return	L"Trigger8@10487$0|7";
		case 19: return	L"Trigger9@10488$0|8";
		case 20: return	L"Trigger10@10489$0|9";
		}

	return	L"Message1@24064$101|10";
	}

CString CEuroInvensysNanoDacDriver::LoadDCOut(UINT uSelect)
{
// SP_DCO
	switch( uSelect % 12 ) {

		case  2: return	L"MeasuredValue@5546#16$11F|10";
		case  3: return	L"OPAdjustState@5539#16$11|3";
		case  4: return	L"OutputHigh@5542#16$11F|6";
		case  5: return	L"OutputLow@5541#16$11F|5";
		case  6: return	L"PV@5537#16$11F|1";
		case  7: return	L"Resolution@5540#16$11|4";
		case  8: return	L"ScaleHigh@5544#16$11F|8";
		case  9: return	L"ScaleLow@5543#16$11F|7";
		case 10: return	L"Status@5538#16$11|2";
		case 11: return	L"Type@5536#16$11|0";
		}

	return	L"FallbackPV@5545#16$11F|9";
	}

CString CEuroInvensysNanoDacDriver::LoadDigitalIO(UINT uSelect)
{
// SP_DIO
	switch( uSelect % 12 ) {

		case  2: return	L"Inertia@5383#16$11F|7";
		case  3: return	L"Invert@5379#16$11|3";
		case  4: return	L"MinOnTime@5378#16$11F|2";
		case  5: return	L"ModuleIdent@5386#16$11|10";
		case  6: return	L"Output@5380#16$11|4";
		case  7: return	L"PV@5377#16$11F|1";
		case  8: return	L"StandbyAction@5385#16$11|9";
		case  9: return	L"Type@5376#16$11|0";
		case 10: return	L"z5n@5381#16$11|5";
		case 11: return	L"z6n@5382#16$11|6";
		}

	return	L"Backlash@5384#16$11F|8";
	}

CString CEuroInvensysNanoDacDriver::LoadGroupRecord(UINT uSelect)
{
// SP_GRCD
	switch( uSelect % 34 ) {

		case  2: return	L"Channel2En@4132$0|4";
		case  3: return	L"Channel3En@4133$0|5";
		case  4: return	L"Channel4En@4134$0|6";
		case  5: return	L"Compression@4160$0|32";
		case  6: return	L"Enable@4128$0|0";
		case  7: return	L"FlashDuration@4153$0F|25";
		case  8: return	L"FlashFree@4152$0F|24";
		case  9: return	L"FlashSize@4151$0F|23";
		case 10: return	L"Interval@4130$0L|2";
		case 11: return	L"Status@4150$0|22";
		case 12: return	L"Suspend@4149$0|21";
		case 13: return	L"VirtualChan1En@4135$0|7";
		case 14: return	L"VirtualChan2En@4136$0|8";
		case 15: return	L"VirtualChan3En@4137$0|9";
		case 16: return	L"VirtualChan4En@4138$0|10";
		case 17: return	L"VirtualChan5En@4139$0|11";
		case 18: return	L"VirtualChan6En@4140$0|12";
		case 19: return	L"VirtualChan7En@4141$0|13";
		case 20: return	L"VirtualChan8En@4142$0|14";
		case 21: return	L"VirtualChan9En@4143$0|15";
		case 22: return	L"VirtualChan10En@4144$0|16";
		case 23: return	L"VirtualChan11En@4145$0|17";
		case 24: return	L"VirtualChan12En@4146$0|18";
		case 25: return	L"VirtualChan13En@4147$0|19";
		case 26: return	L"VirtualChan14En@4148$0|20";
		case 27: return	L"z4129n@4129$0|1";
		case 28: return	L"z4154n@4154$0|26";
		case 29: return	L"z4155n@4155$0|27";
		case 30: return	L"z4156n@4156$0|28";
		case 31: return	L"z4157n@4157$0|29";
		case 32: return	L"z4158n@4158$0|30";
		case 33: return	L"z4159n@4159$0|31";
		}

	return	L"Channel1En@4131$0|3";
	}

CString CEuroInvensysNanoDacDriver::LoadGroupTrend(UINT uSelect)
{
// SP_GTRD
	switch( uSelect % 11 ) {

		case  2: return	L"MajorDivisions@4100$0|2";
		case  3: return	L"Point1@4102$0|4";
		case  4: return	L"Point2@4103$0|5";
		case  5: return	L"Point3@4104$0|6";
		case  6: return	L"Point4@4105$0|7";
		case  7: return	L"Point5@4106$0|8";
		case  8: return	L"Point6@4107$0|9";
		case  9: return	L"z4099n@4099$0|1";
		case 10: return	L"z4101n@4100$0|3";
		}

	return	L"Interval@4098$0L|0";
	}

CString CEuroInvensysNanoDacDriver::LoadHumidity(UINT uSelect)
{
// SP_HUM
	switch( uSelect % 11 ) {

		case  2: return	L"DryTemp@11901$0F|5";
		case  3: return	L"Pressure@11904$0F|8";
		case  4: return	L"PsychroConst@11903$0F|7";
		case  5: return	L"RelHumid@11896$0F|0";
		case  6: return	L"Resolution@11905$0|9";
		case  7: return	L"SBrk@11902$0|6";
		case  8: return	L"WetOffset@11899$0F|3";
		case  9: return	L"WetTemp@11900$0F|4";
		case 10: return	L"z11898n@11898$0|2";
		}

	return	L"DewPoint@11897$0F|1";
	}

CString CEuroInvensysNanoDacDriver::LoadInstrGeneral(UINT uSelect)
{
// SP_INSG
	switch( uSelect % 96 ) {

		case  2: return	L"Clock.Time@4225$0T|1";
		case  3: return	L"Display.AlarmPanel@4331$0|79";
		case  4: return	L"Display.Brightness@4240$0|16";
		case  5: return	L"Display.DualLoopControl@4251$0|27";
		case  6: return	L"Display.FaceplateCycling@4254$0|30";
		case  7: return	L"Display.HistoryBackground@4264$0|40";
		case  8: return	L"Display.HomePage@4243$0|19";
		case  9: return	L"Display.HorizontalBar@4248$0|24";
		case 10: return	L"Display.HorizontalTrend@4246$0|22";
		case 11: return	L"Display.HPageTimeout@4244$0|20";
		case 12: return	L"Display.HTrendScaling@4253$0|29";
		case 13: return	L"Display.LoopControl@4250$0|26";
		case 14: return	L"Display.LoopSetpointColour@4255$0|31";
		case 15: return	L"Display.Numeric@4249$0|25";
		case 16: return	L"Display.PromoteListView@4330$0|78";
		case 17: return	L"Display.ScreenSaverAfter@4241$0|17";
		case 18: return	L"Display.ScreenSaverBrightness@4242$0|18";
		case 19: return	L"Display.TrendBackground@4252$0|28";
		case 20: return	L"Display.VerticalBar@4247$0|23";
		case 21: return	L"Display.VerticalTrend@4245$0|21";
		case 22: return	L"Info.ConfigRev@4256$0L|32";
		case 23: return	L"Info.IM@199$0|0";
		case 24: return	L"Info.LineVoltage@4262$0F|38";
		case 25: return	L"Info.MicroBoardIssue@4266$0|42";
		case 26: return	L"Info.NvolWrites@4261$0L|37";
		case 27: return	L"Info.PSUType@4265$0|41";
		case 28: return	L"Info.SecurityRev@4260$0L|36";
		case 29: return	L"Info.Type@4258$0|34";
		case 30: return	L"IOFitted.1A1B@4340$0|88";
		case 31: return	L"IOFitted.2A2B@4341$0|89";
		case 32: return	L"IOFitted.3A3B@4343$0|91";
		case 33: return	L"IOFitted.4AC@4345$0|93";
		case 34: return	L"IOFitted.5AC@4346$0|94";
		case 35: return	L"IOFitted.LALC@4342$0|90";
		case 36: return	L"IOFitted.LBLC@4344$0|92";
		case 37: return	L"Locale.DateFormat@4273$0|49";
		case 38: return	L"Locale.DSTenable@4275$0|51";
		case 39: return	L"Locale.EndDay@4282$0|58";
		case 40: return	L"Locale.EndMonth@4283$0|59";
		case 41: return	L"Locale.EndOn@4281$0|57";
		case 42: return	L"Locale.EndTime@4280$0T|56";
		case 43: return	L"Locale.Language@4272$0|48";
		case 44: return	L"Locale.StartDay@4278$0|54";
		case 45: return	L"Locale.StartMonth@4279$0|55";
		case 46: return	L"Locale.StartOn@4277$0|53";
		case 47: return	L"Locale.StartTime@4276$0T|52";
		case 48: return	L"Locale.TimeZone@4274$0|50";
		case 49: return	L"PromoteList.PromoteParam1@4320$0L|68";
		case 50: return	L"PromoteList.PromoteParam2@4321$0L|69";
		case 51: return	L"PromoteList.PromoteParam3@4322$0L|70";
		case 52: return	L"PromoteList.PromoteParam4@4323$0L|71";
		case 53: return	L"PromoteList.PromoteParam5@4324$0L|72";
		case 54: return	L"PromoteList.PromoteParam6@4325$0L|73";
		case 55: return	L"PromoteList.PromoteParam7@4326$0L|74";
		case 56: return	L"PromoteList.PromoteParam8@4327$0L|75";
		case 57: return	L"PromoteList.PromoteParam9@4328$0L|76";
		case 58: return	L"PromoteList.PromoteParam10@4329$0L|77";
		case 59: return	L"Security.CommsPass@4289$0|65";
		case 60: return	L"Security.DefaultConfig@4290$0|66";
		case 61: return	L"Security.EngineerAccess@4288$0|64";
		case 62: return	L"Security.FeaturePass@4291$0L|67";
		case 63: return	L"z4227n@4227$0|3";
		case 64: return	L"z4228n@4228$0|4";
		case 65: return	L"z4229n@4229$0|5";
		case 66: return	L"z4230n@4230$0|6";
		case 67: return	L"z4231n@4231$0|7";
		case 68: return	L"z4232n@4232$0|8";
		case 69: return	L"z4233n@4233$0|9";
		case 70: return	L"z4234n@4234$0|10";
		case 71: return	L"z4235n@4235$0|11";
		case 72: return	L"z4236n@4236$0|12";
		case 73: return	L"z4237n@4237$0|13";
		case 74: return	L"z4238n@4238$0|14";
		case 75: return	L"z4239n@4239$0|15";
		case 76: return	L"z4257n@4257$0|33";
		case 77: return	L"z4259n@4259$0|35";
		case 78: return	L"z4263n@4263$0|39";
		case 79: return	L"z4267n@4267$0|43";
		case 80: return	L"z4268n@4268$0|44";
		case 81: return	L"z4269n@4269$0|45";
		case 82: return	L"z4270n@4270$0|46";
		case 83: return	L"z4271n@4271$0|47";
		case 84: return	L"z4284n@4284$0|60";
		case 85: return	L"z4285n@4285$0|61";
		case 86: return	L"z4286n@4286$0|62";
		case 87: return	L"z4287n@4287$0|63";
		case 88: return	L"z4332n@4332$0|80";
		case 89: return	L"z4333n@4333$0|81";
		case 90: return	L"z4334n@4334$0|82";
		case 91: return	L"z4335n@4335$0|83";
		case 92: return	L"z4336n@4336$0|84";
		case 93: return	L"z4337n@4337$0|85";
		case 94: return	L"z4338n@4338$0|86";
		case 95: return	L"z4339n@4339$0|87";
		}

	return	L"Clock.DST@4226$0|2";
	}

CString CEuroInvensysNanoDacDriver::LoadInstrString(UINT uSelect)
{
// SP_INSS
	switch( uSelect % 30 ) {

		case  2: return	L"Info.Name@17503$0|95";
		case  3: return	L"Info.Version@17524$0|116";
		case  4: return	L"Info.Bootrom@17530$0|122";
		case  5: return	L"Notes.Note@21760$0|140";
		case  6: return	L"Notes.Note1@21888$0|280";
		case  7: return	L"Notes.Note2@22016$0|420";
		case  8: return	L"Notes.Note3@22144$0|560";
		case  9: return	L"Notes.Note4@22272$0|700";
		case 10: return	L"Notes.Note5@22400$0|840";
		case 11: return	L"Notes.Note6@22528$0|980";
		case 12: return	L"Notes.Note7@22656$0|1120";
		case 13: return	L"Notes.Note8@22784$0|1260";
		case 14: return	L"Notes.Note9@22912$0|1400";
		case 15: return	L"Notes.Note10@23040$0|1540";
		case 16: return	L"PromoteList.PromoteParam1Desc@25344$0|1700";
		case 17: return	L"PromoteList.PromoteParam2Desc@25365$0|1721";
		case 18: return	L"PromoteList.PromoteParam3Desc@25386$0|1742";
		case 19: return	L"PromoteList.PromoteParam4Desc@25407$0|1763";
		case 20: return	L"PromoteList.PromoteParam5Desc@25428$0|1784";
		case 21: return	L"PromoteList.PromoteParam6Desc@25449$0|1805";
		case 22: return	L"PromoteList.PromoteParam7Desc@25470$0|1826";
		case 23: return	L"PromoteList.PromoteParam8Desc@25491$0|1847";
		case 24: return	L"PromoteList.PromoteParam9Desc@25512$0|1868";
		case 25: return	L"PromoteList.PromoteParam10Desc@25533$0|1889";
		case 26: return	L"Security.EngineerPassword@25555$0|2000";
		case 27: return	L"Security.OperatorPassword@25655$0|2100";
		case 28: return	L"Security.PassPhrase@17482$0|74";
		case 29: return	L"Security.SupervisorPassword@25605$0|2050";
		}

	return	L"Clock.Date@17408$0|0";
	}

CString CEuroInvensysNanoDacDriver::LoadLgc2(UINT uSelect)
{
// SP_LGC2
	switch( uSelect % 8 ) {

		case 2: return	L"In1@12025#7$7F|1";
		case 3: return	L"In2@12026#7$7F|2";
		case 4: return	L"Invert@12028#7$7|4";
		case 5: return	L"Oper@12024#7$7|0";
		case 6: return	L"Out@12029#7$7|5";
		case 7: return	L"OutputStatus@12030#7$7|6";
		}

	return	L"FallbackType@12027#7$7|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLgc8(UINT uSelect)
{
// SP_LGC8
	switch( uSelect % 14 ) {

		case  2: return	L"In2@12112#13$13|4";
		case  3: return	L"In3@12113#13$13|5";
		case  4: return	L"In4@12114#13$13|6";
		case  5: return	L"In5@12115#13$13|7";
		case  6: return	L"In6@12116#13$13|8";
		case  7: return	L"In7@12117#13$13|9";
		case  8: return	L"In8@12118#13$13|10";
		case  9: return	L"InInvert@12109#13$13|1";
		case 10: return	L"NumIn@12110#13$13|2";
		case 11: return	L"Oper@12108#13$13|0";
		case 12: return	L"Out@12119#13$13|11";
		case 13: return	L"OutInvert@12120#13$13|12";
		}

	return	L"In1@12111#13$13|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopDiagMain1(UINT uSelect)
{
// SP_LDG1
	switch( uSelect % 34 ) {

		case  2: return	L"Diag.Error@525$0F|13";
		case  3: return	L"Diag.IntegralOutContrib@529$0F|17";
		case  4: return	L"Diag.LoopBreakAlarm@527$0F|15";
		case  5: return	L"Diag.LoopMode@5777$0|22";
		case  6: return	L"Diag.PropOutContrib@528$0F|16";
		case  7: return	L"Diag.SBrk@531$0|19";
		case  8: return	L"Diag.SchedCBH@5781$0F|26";
		case  9: return	L"Diag.SchedCBL@5782$0F|27";
		case 10: return	L"Diag.SchedLPBrk@5784$0F|29";
		case 11: return	L"Diag.SchedMR@5783$0F|28";
		case 12: return	L"Diag.SchedOPHi@5786$0F|31";
		case 13: return	L"Diag.SchedOPLo@5787$0F|32";
		case 14: return	L"Diag.SchedPB@5778$0F|23";
		case 15: return	L"Diag.SchedR2G@5785$0F|30";
		case 16: return	L"Diag.SchedTd@5780$0F|25";
		case 17: return	L"Diag.SchedTi@5779$0F|24";
		case 18: return	L"Diag.TargetOutVal@526$0F|14";
		case 19: return	L"Diag.WrkOPHi@533$0F|21";
		case 20: return	L"Diag.WrkOPLo@532$0F|20";
		case 21: return	L"Main.ActiveOut@516$0F|4";
		case 22: return	L"Main.AutoMan@513$0|1";
		case 23: return	L"Main.Inhibit@517$0|5";
		case 24: return	L"Main.IntHold@518$0|6";
		case 25: return	L"Main.PV@512$0F|0";
		case 26: return	L"Main.TargetSP@514$0F|2";
		case 27: return	L"Main.WorkingSP@515$0F|3";
		case 28: return	L"z519n@519$0|7";
		case 29: return	L"z520n@520$0|8";
		case 30: return	L"z521n@521$0|9";
		case 31: return	L"z522n@522$0|10";
		case 32: return	L"z523n@523$0|11";
		case 33: return	L"z524n@524$0|12";
		}

	return	L"Diag.DerivativeOutContrib@530$0F|18";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopDiagMain2(UINT uSelect)
{
// SP_LDG2
	switch( uSelect % 34 ) {

		case  2: return	L"Diag.Error@653$0F|13";
		case  3: return	L"Diag.IntegralOutContrib@657$0F|17";
		case  4: return	L"Diag.LoopBreakAlarm@655$0|15";
		case  5: return	L"Diag.LoopMode@6033$0|22";
		case  6: return	L"Diag.PropOutContrib@656$0F|16";
		case  7: return	L"Diag.SBrk@659$0|19";
		case  8: return	L"Diag.SchedCBH@6037$0F|26";
		case  9: return	L"Diag.SchedCBL@6038$0F|27";
		case 10: return	L"Diag.SchedLPBrk@6040$0F|29";
		case 11: return	L"Diag.SchedMR@6039$0F|28";
		case 12: return	L"Diag.SchedOPHi@6042$0F|31";
		case 13: return	L"Diag.SchedOPLo@6043$0F|32";
		case 14: return	L"Diag.SchedPB@6034$0F|23";
		case 15: return	L"Diag.SchedR2G@6041$0F|30";
		case 16: return	L"Diag.SchedTd@6036$0F|25";
		case 17: return	L"Diag.SchedTi@6035$0F|24";
		case 18: return	L"Diag.TargetOutVal@654$0F|14";
		case 19: return	L"Diag.WrkOPHi@661$0F|21";
		case 20: return	L"Diag.WrkOPLo@660$0F|20";
		case 21: return	L"Main.ActiveOut@644$0F|4";
		case 22: return	L"Main.AutoMan@641$0|1";
		case 23: return	L"Main.Inhibit@645$0|5";
		case 24: return	L"Main.IntHold@646$0|6";
		case 25: return	L"Main.PV@640$0F|0";
		case 26: return	L"Main.TargetSP@642$0F|2";
		case 27: return	L"Main.WorkingSP@643$0F|3";
		case 28: return	L"z647n@647$0|7";
		case 29: return	L"z648n@648$0|8";
		case 30: return	L"z649n@649$0|9";
		case 31: return	L"z650n@650$0|10";
		case 32: return	L"z651n@651$0|11";
		case 33: return	L"z652n@652$0|12";
		}

	return	L"Diag.DerivativeOutContrib@658$0F|18";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopOP1(UINT uSelect)
{
// SP_LOP1
	switch( uSelect % 39 ) {

		case  2: return	L"Loop1.OP.Ch1Out@523$0F|0";
		case  3: return	L"Loop1.OP.Ch1PotBreak@5753$0|14";
		case  4: return	L"Loop1.OP.Ch1PotPosition@5752$0F|13";
		case  5: return	L"Loop1.OP.Ch1TravelTime@5748$0F|9";
		case  6: return	L"Loop1.OP.Ch2Deadband@5743$0F|4";
		case  7: return	L"Loop1.OP.Ch2OnOffHysteresis@5747$0F|8";
		case  8: return	L"Loop1.OP.Ch2Out@524$0F|1";
		case  9: return	L"Loop1.OP.Ch2PotBreak@5755$0|16";
		case 10: return	L"Loop1.OP.Ch2PotPosition@5754$0F|15";
		case 11: return	L"Loop1.OP.Ch2TravelTime@5749$0F|10";
		case 12: return	L"Loop1.OP.CoolType@5763$0|24";
		case 13: return	L"Loop1.OP.EnablePowerFeedforward@5761$0|22";
		case 14: return	L"Loop1.OP.FeedForwardGain@5765$0F|26";
		case 15: return	L"Loop1.OP.FeedForwardOffset@5766$0F|27";
		case 16: return	L"Loop1.OP.FeedForwardTrimLimit@5767$0F|28";
		case 17: return	L"Loop1.OP.FeedForwardType@5764$0|25";
		case 18: return	L"Loop1.OP.FeedForwardVal@5768$0F|29";
		case 19: return	L"Loop1.OP.FF_Rem@5773$0F|34";
		case 20: return	L"Loop1.OP.ForcedOP@5775$0F|36";
		case 21: return	L"Loop1.OP.ManStartup@5776$0|37";
		case 22: return	L"Loop1.OP.ManualMode@5759$0|20";
		case 23: return	L"Loop1.OP.ManualOutVal@5760$0F|21";
		case 24: return	L"Loop1.OP.MeasuredPower@5762$0F|23";
		case 25: return	L"Loop1.OP.NudgeLower@5751$0|12";
		case 26: return	L"Loop1.OP.NudgeRaise@5750$0|11";
		case 27: return	L"Loop1.OP.OutputHighLimit@5741$0F|2";
		case 28: return	L"Loop1.OP.OutputLowLimit@5742$0F|3";
		case 29: return	L"Loop1.OP.PotBreakMode@5756$0|17";
		case 30: return	L"Loop1.OP.Rate@5744$0F|5";
		case 31: return	L"Loop1.OP.RateDisable@5745$0|6";
		case 32: return	L"Loop1.OP.RemOPH@5772$0F|33";
		case 33: return	L"Loop1.OP.RemOPL@5771$0F|32";
		case 34: return	L"Loop1.OP.SafeOutVal@5758$0F|19";
		case 35: return	L"Loop1.OP.SbrkOP@5774$0F|35";
		case 36: return	L"Loop1.OP.SensorBreakMode@5757$0|18";
		case 37: return	L"Loop1.OP.TrackEnable@5770$0|31";
		case 38: return	L"Loop1.OP.TrackOutVal@5769$0F|30";
		}

	return	L"Ch1OnOffHysteresis@5746$0F|7";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopOP2(UINT uSelect)
{
// SP_LOP2
	switch( uSelect % 39 ) {

		case  2: return	L"Ch1Out@651$0F|0";
		case  3: return	L"Ch1PotBreak@6009$0|14";
		case  4: return	L"Ch1PotPosition@6008$0F|13";
		case  5: return	L"Ch1TravelTime@6004$0F|9";
		case  6: return	L"Ch2Deadband@5999$0F|4";
		case  7: return	L"Ch2OnOffHysteresis@6003$0F|8";
		case  8: return	L"Ch2Out@652$0F|1";
		case  9: return	L"Ch2PotBreak@6011$0|16";
		case 10: return	L"Ch2PotPosition@6010$0F|15";
		case 11: return	L"Ch2TravelTime@6005$0F|10";
		case 12: return	L"CoolType@6019$0|24";
		case 13: return	L"EnablePowerFeedforward@6017$0|22";
		case 14: return	L"FeedForwardGain@6021$0F|26";
		case 15: return	L"FeedForwardOffset@6022$0F|27";
		case 16: return	L"FeedForwardTrimLimit@6023$0F|28";
		case 17: return	L"FeedForwardType@6020$0|25";
		case 18: return	L"FeedForwardVal@6024$0F|29";
		case 19: return	L"FF_Rem@6029$0F|34";
		case 20: return	L"ForcedOP@6031$0F|36";
		case 21: return	L"ManStartup@6032$0|37";
		case 22: return	L"ManualMode@6015$0|20";
		case 23: return	L"ManualOutVal@6016$0F|21";
		case 24: return	L"MeasuredPower@6018$0F|23";
		case 25: return	L"NudgeLower@6007$0|12";
		case 26: return	L"NudgeRaise@6006$0|11";
		case 27: return	L"OutputHighLimit@5997$0F|2";
		case 28: return	L"OutputLowLimit@5998$0F|3";
		case 29: return	L"PotBreakMode@6012$0|17";
		case 30: return	L"Rate@6000$0F|5";
		case 31: return	L"RateDisable@6001$0|6";
		case 32: return	L"RemOPH@6028$0F|33";
		case 33: return	L"RemOPL@6027$0F|32";
		case 34: return	L"SafeOutVal@6014$0F|19";
		case 35: return	L"SbrkOP@6030$0F|35";
		case 36: return	L"SensorBreakMode@6013$0|18";
		case 37: return	L"TrackEnable@6026$0|31";
		case 38: return	L"TrackOutVal@6025$0F|30";
		}

	return	L"Ch1OnOffHysteresis@6002$0F|7";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopPID1(UINT uSelect)
{
// SP_LPD1
	switch( uSelect % 36 ) {

		case  2: return	L"Boundary1-2@5689$0F|4";
		case  3: return	L"Boundary2-3@5690$0F|5";
		case  4: return	L"CutbackHigh@5695$0F|10";
		case  5: return	L"CutbackHigh2@5703$0F|18";
		case  6: return	L"CutbackHigh3@5711$0F|26";
		case  7: return	L"CutbackLow@5696$0F|11";
		case  8: return	L"CutbackLow2@5704$0F|19";
		case  9: return	L"CutbackLow3@5712$0F|27";
		case 10: return	L"DerivativeTime@5693$0F|8";
		case 11: return	L"DerivativeTime2@5701$0F|16";
		case 12: return	L"DerivativeTime3@5709$0F|24";
		case 13: return	L"IntegralTime@5692$0F|7";
		case 14: return	L"IntegralTime2@5700$0F|15";
		case 15: return	L"IntegralTime3@5708$0F|23";
		case 16: return	L"LoopBreakTime@5698$0F|13";
		case 17: return	L"LoopBreakTime2@5706$0F|21";
		case 18: return	L"LoopBreakTime3@5714$0F|29";
		case 19: return	L"ManualReset@5697$0F|12";
		case 20: return	L"ManualReset2@5705$0F|20";
		case 21: return	L"ManualReset3@5713$0F|28";
		case 22: return	L"NumSets@5686$0|1";
		case 23: return	L"OutputHi@5715$0F|30";
		case 24: return	L"OutputHi2@5717$0F|32";
		case 25: return	L"OutputHi3@5719$0F|34";
		case 26: return	L"OutputLo@5716$0F|31";
		case 27: return	L"OutputLo2@5718$0F|33";
		case 28: return	L"OutputLo3@5720$0F|35";
		case 29: return	L"ProportionalBand@5691$0F|6";
		case 30: return	L"ProportionalBand2@5699$0F|14";
		case 31: return	L"ProportionalBand3@5707$0F|22";
		case 32: return	L"RelCh2Gain@5694$0F|9";
		case 33: return	L"RelCh2Gain2@5702$0F|17";
		case 34: return	L"RelCh2Gain3@5710$0F|25";
		case 35: return	L"SchedulerRemoteInput@5687$0F|2";
		case 36: return	L"SchedulerType@5685$0|0";
		}

	return	L"ActiveSet@5688$0|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopPID2(UINT uSelect)
{
// SP_LPD2
	switch( uSelect % 36 ) {

		case  2: return	L"Boundary1-2@5945$0F|4";
		case  3: return	L"Boundary2-3@5946$0F|5";
		case  4: return	L"CutbackHigh@5951$0F|10";
		case  5: return	L"CutbackHigh2@5959$0F|18";
		case  6: return	L"CutbackHigh3@5967$0F|26";
		case  7: return	L"CutbackLow@5952$0F|11";
		case  8: return	L"CutbackLow2@5960$0F|19";
		case  9: return	L"CutbackLow3@5968$0F|27";
		case 10: return	L"DerivativeTime@5949$0F|8";
		case 11: return	L"DerivativeTime2@5957$0F|16";
		case 12: return	L"DerivativeTime3@5965$0F|24";
		case 13: return	L"IntegralTime@5948$0F|7";
		case 14: return	L"IntegralTime2@5956$0F|15";
		case 15: return	L"IntegralTime3@5964$0F|23";
		case 16: return	L"LoopBreakTime@5954$0F|13";
		case 17: return	L"LoopBreakTime2@5962$0F|21";
		case 18: return	L"LoopBreakTime3@5970$0F|29";
		case 19: return	L"ManualReset@5953$0F|12";
		case 20: return	L"ManualReset2@5961$0F|20";
		case 21: return	L"ManualReset3@5969$0F|28";
		case 22: return	L"NumSets@5942$0|1";
		case 23: return	L"OutputHi@5971$0F|30";
		case 24: return	L"OutputHi2@5973$0F|32";
		case 25: return	L"OutputHi3@5975$0F|34";
		case 26: return	L"OutputLo@5972$0F|31";
		case 27: return	L"OutputLo2@5974$0F|33";
		case 28: return	L"OutputLo3@5976$0F|35";
		case 29: return	L"ProportionalBand@5947$0F|6";
		case 30: return	L"ProportionalBand2@5955$0F|14";
		case 31: return	L"ProportionalBand3@5963$0F|22";
		case 32: return	L"RelCh2Gain@5950$0F|9";
		case 33: return	L"RelCh2Gain2@5958$0F|17";
		case 34: return	L"RelCh2Gain3@5966$0F|25";
		case 35: return	L"SchedulerRemoteInput@5943$0F|2";
		case 36: return	L"SchedulerType@5941$0|0";
		}

	return	L"ActiveSet@5944$0|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopSetup(UINT uSelect)
{
// SP_LSET
	switch( uSelect % 17 ) {

		case  2: return	L"1.Setup.CH1ControlType@5633$0|1";
		case  3: return	L"1.Setup.CH2ControlType@5634$0|2";
		case  4: return	L"1.Setup.ControlAction@5635$0|3";
		case  5: return	L"1.Setup.DerivativeType@5637$0|5";
		case  6: return	L"1.Setup.LoopType@5632$0|0";
		case  7: return	L"1.Setup.PBUnits@5636$0|4";
		case  8: return	L"1.Setup.SPAccess@5799$0|6";
		case  9: return	L"2.Setup.AutoManAccess@6056$0|15";
		case 10: return	L"2.Setup.CH1ControlType@5889$0|9";
		case 11: return	L"2.Setup.CH2ControlType@5890$0|10";
		case 12: return	L"2.Setup.ControlAction@5891$0|11";
		case 13: return	L"2.Setup.DerivativeType@5893$0|13";
		case 14: return	L"2.Setup.LoopType@5888$0|8";
		case 15: return	L"2.Setup.PBUnits@5892$0|12";
		case 16: return	L"2.Setup.SPAccess@6055$0|14";
		}

	return	L"1.AutoManAccess@5800$0|7";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopSP1(UINT uSelect)
{
// SP_LSP1
	switch( uSelect % 21 ) {

		case  2: return	L"AltSPSelect@5729$0|9";
		case  3: return	L"ManualTrack@5735$0|15";
		case  4: return	L"RangeHigh@5721$0F|1";
		case  5: return	L"RangeLow@5722$0F|2";
		case  6: return	L"Rate@5730$0F|10";
		case  7: return	L"RateDisable@5731$0|11";
		case  8: return	L"RateDone@522$0|0";
		case  9: return	L"ServoToPV@5740$0|20";
		case 10: return	L"SP1@5724$0F|4";
		case 11: return	L"SP2@5725$0F|5";
		case 12: return	L"SPHighLimit@5726$0F|6";
		case 13: return	L"SPIntBal@5739$0|19";
		case 14: return	L"SPLowLimit@5727$0F|7";
		case 15: return	L"SPSelect@5723$0|3";
		case 16: return	L"SPTrack@5736$0|16";
		case 17: return	L"SPTrim@5732$0F|12";
		case 18: return	L"SPTrimHighLimit@5733$0F|13";
		case 19: return	L"SPTrimLowLimit@5734$0F|14";
		case 20: return	L"TrackPV@5737$0F|17";
		case 21: return	L"TrackSP@5738$0F|18";
		}

	return	L"AltSP@5728$0F|8";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopSP2(UINT uSelect)
{
// SP_LSP2
	switch( uSelect % 21 ) {

		case  2: return	L"AltSPSelect@5985$0|9";
		case  3: return	L"ManualTrack@5991$0|15";
		case  4: return	L"RangeHigh@5977$0F|1";
		case  5: return	L"RangeLow@5978$0F|2";
		case  6: return	L"Rate@5986$0F|10";
		case  7: return	L"RateDisable@5987$0|11";
		case  8: return	L"RateDone@650$0|0";
		case  9: return	L"ServoToPV@5996$0|20";
		case 10: return	L"SP1@5980$0F|4";
		case 11: return	L"SP2@5981$0F|5";
		case 12: return	L"SPHighLimit@5982$0F|6";
		case 13: return	L"SPIntBal@5995$0|19";
		case 14: return	L"SPLowLimit@5983$0F|7";
		case 15: return	L"SPSelect@5979$0|3";
		case 16: return	L"SPTrack@5992$0|16";
		case 17: return	L"SPTrim@5988$0F|12";
		case 18: return	L"SPTrimHighLimit@5989$0F|13";
		case 19: return	L"SPTrimLowLimit@5990$0F|14";
		case 20: return	L"TrackPV@5993$0F|17";
		case 21: return	L"TrackSP@5994$0F|18";
		}

	return	L"AltSP@5984$0F|8";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopTune(UINT uSelect)
{
// SP_LTUN
	switch( uSelect % 17 ) {

		case  2: return	L"Loop.1.Tune.AutoTuneR2G@5684$0|10";
		case  3: return	L"1.Tune.OutputHighLimit@5682$0F|8";
		case  4: return	L"1.Tune.OutputLowLimit@5683$0F|9";
		case  5: return	L"1.Tune.Stage@520$0|1";
		case  6: return	L"1.Tune.StageTime@521$0F|2";
		case  7: return	L"1.Tune.State@519$0|0";
		case  8: return	L"1.Tune.Type@5680$0|6";
		case  9: return	L"2.Tune.AutotuneEnable@5937$0|12";
		case 10: return	L"2.Tune.AutoTuneR2G@5940$0|15";
		case 11: return	L"2.Tune.OutputHighLimit@5938$0|13";
		case 12: return	L"2.Tune.OutputLowLimit@5939$0|14";
		case 13: return	L"2.Tune.Stage@648$0|4";
		case 14: return	L"2.Tune.StageTime@649$0|5";
		case 15: return	L"2.Tune.State@647$0|3";
		case 16: return	L"2.Tune.Type@5936$0|11";
		}

	return	L"1.Tune.AutotuneEnable@5681$0|7";
	}

CString CEuroInvensysNanoDacDriver::LoadMath2(UINT uSelect)
{
// SP_MATH
	switch( uSelect % 14 ) {
		
		case  2: return	L"FallbackVal@12203#13$13F|5";
		case  3: return	L"HighLimit@12204#13$13F|6";
		case  4: return	L"In1@12199#13$13F|1";
		case  5: return	L"In1Mul@12198#13$13F|0";
		case  6: return	L"In2@12201#13$13F|3";
		case  7: return	L"In2Mul@12200#13$13F|2";
		case  8: return	L"LowLimit@12205#13$13F|7";
		case  9: return	L"Oper@12202#13$13|4";
		case 10: return	L"Out@12206#13$13F|8";
		case 11: return	L"Resolution@12210#13$13|12";
		case 12: return	L"Select@12208#13$13|10";
		case 13: return	L"Status@12209#13$13|11";
		}

	return	L"Fallback@12207#13$13|9";
	}

CString CEuroInvensysNanoDacDriver::LoadMux8(UINT uSelect)
{
// SP_MUX
	switch( uSelect % 17 ) {

		case  2: return	L"FallbackVal@12135#16$16F|1";
		case  3: return	L"HighLimit@12137#16$16F|3";
		case  4: return	L"In1@12139#16$16F|5";
		case  5: return	L"In2@12140#16$16F|6";
		case  6: return	L"In3@12141#16$16F|7";
		case  7: return	L"In4@12142#16$16F|8";
		case  8: return	L"In5@12143#16$16F|9";
		case  9: return	L"In6@12144#16$16F|10";
		case 10: return	L"In7@12145#16$16F|11";
		case 11: return	L"In8@12146#16$16F|12";
		case 12: return	L"LowLimit@12138#16$16F|4";
		case 13: return	L"Out@12147#16$16F|13";
		case 14: return	L"Resolution@12149#16$16|15";
		case 15: return	L"Select@12136#16$16|2";
		case 16: return	L"Status@12148#16$16|14";
		}

	return	L"Fallback@12134#16$16F|0";
	}

CString CEuroInvensysNanoDacDriver::LoadNano(UINT uSelect)
{
// SP_NANO
	return L"Access@11264$0|0";
	}

CString CEuroInvensysNanoDacDriver::LoadNetwork(UINT uSelect)
{
// SP_NET
	switch( uSelect % 54 ) {

		case  2: return	L"Archive.CSVDateFormat@4381$0|27";
		case  3: return	L"Archive.CSVHeaders@4379$0|25";
		case  4: return	L"Archive.CSVHeadings@4380$0|26";
		case  5: return	L"Archive.CSVIncludeValues@4377$0|23";
		case  6: return	L"Archive.CSVMessages@4378$0|24";
		case  7: return	L"Archive.CSVTabDelimiter@4382$0|28";
		case  8: return	L"Archive.Destination@4369$0|15";
		case  9: return	L"Archive.FileFormat@4373$0|19";
		case 10: return	L"Archive.MediaDuration@4376$0F|22";
		case 11: return	L"Archive.MediaFree@4384$0F|30";
		case 12: return	L"Archive.MediaSize@4375$0F|21";
		case 13: return	L"Archive.OnFull@4374$0|20";
		case 14: return	L"Archive.Period@4437$0|52";
		case 15: return	L"Archive.Trigger@4435$0|50";
		case 16: return	L"DemandArchive.PrimaryStatus@4432$0|47";
		case 17: return	L"DemandArchive.SecStatus@4433$0|48";
		case 18: return	L"DemandArchive.Status@4434$0|49";
		case 19: return	L"DemandArchive.SuspendSchedule@4436$0|51";
		case 20: return	L"Interface.IPType@4354$0|0";
		case 21: return	L"Modbus.Address@4416$0|31";
		case 22: return	L"Modbus.InputTimeout@4417$0|32";
		case 23: return	L"Modbus.SerialMode@4419$0|34";
		case 24: return	L"Modbus.TimeFormat@4420$0|35";
		case 25: return	L"Modbus.UnitIdEnable@4418$0|33";
		case 26: return	L"z4355n@4355$0|1";
		case 27: return	L"z4356n@4356$0|2";
		case 28: return	L"z4357n@4357$0|3";
		case 29: return	L"z4358n@4358$0|4";
		case 30: return	L"z4359n@4359$0|5";
		case 31: return	L"z4360n@4360$0|6";
		case 32: return	L"z4361n@4361$0|7";
		case 33: return	L"z4362n@4362$0|8";
		case 34: return	L"z4363n@4363$0|9";
		case 35: return	L"z4364n@4364$0|10";
		case 36: return	L"z4365n@4365$0|11";
		case 37: return	L"z4366n@4366$0|12";
		case 38: return	L"z4367n@4367$0|13";
		case 39: return	L"z4368n@4368$0|14";
		case 40: return	L"z4370n@4370$0|16";
		case 41: return	L"z4371n@4371$0|17";
		case 42: return	L"z4383n@4383$0|29";
		case 43: return	L"z4421n@4421$0|36";
		case 44: return	L"z4422n@4422$0|37";
		case 45: return	L"z4423n@4423$0|38";
		case 46: return	L"z4424n@4424$0|39";
		case 47: return	L"z4425n@4425$0|40";
		case 48: return	L"z4426n@4426$0|41";
		case 49: return	L"z4427n@4427$0|42";
		case 50: return	L"z4428n@4428$0|43";
		case 51: return	L"z4429n@4429$0|44";
		case 52: return	L"z4430n@4430$0|45";
		case 53: return	L"z4431n@4431$0|46";
		}

	return	L"Archive.ArchiveRate@4372$0|18";
	}

CString CEuroInvensysNanoDacDriver::LoadNetStr(UINT uSelect)
{
// SP_NETS
	switch( uSelect % 18 ) {

		case  2: return	L"Archive.PrimaryUser@17894$0|348";
		case  3: return	L"Archive.PServerIPAddress@17876$0|330";
		case  4: return	L"Archive.RemotePath@17775$0|220";
		case  5: return	L"Archive.SecondaryPassword@25795$0|1095";
		case  6: return	L"Archive.SecondaryUser@17994$0|518";
		case  7: return	L"Archive.SServerIPAddress@17976$0|500";
		case  8: return	L"Archive.Trigger@4435$0|0";
		case  9: return	L"DemandArchive.LastWrittenOn@18176$0|700";
		case 10: return	L"FTPserver.Password@25885$0|1185";
		case 11: return	L"FTPserver.Username@18094$0|618";
		case 12: return	L"Interface.ClientIdentifier@18197$0|721";
		case 13: return	L"Interface.Gateway@17700$0|136";
		case 14: return	L"Interface.IPaddress@17664$0|100";
		case 15: return	L"Interface.MAC@17736$0|172";
		case 16: return	L"Interface.SubnetMask@17682$0|118";
		case 17: return	L"Modbus.PrefMasterIP@18076$0|600";
		}

	return	L"Archive.PrimaryPassword@25705$0|1000";
	}

CString CEuroInvensysNanoDacDriver::LoadOR(UINT uSelect)
{
// SP_OR
	switch( uSelect % 10 ) {

		case 2: return	L"Input2@11521#16$9|1";
		case 3: return	L"Input3@11522#16$9|2";
		case 4: return	L"Input4@11523#16$9|3";
		case 5: return	L"Input5@11524#16$9|4";
		case 6: return	L"Input6@11525#16$9|5";
		case 7: return	L"Input7@11526#16$9|6";
		case 8: return	L"Input8@11527#16$9|7";
		case 9: return	L"Output@11528#16$9|8";
		}

	return	L"Input1@11520#16$9|0";
	}

CString CEuroInvensysNanoDacDriver::LoadSterilizer(UINT uSelect)
{
// SP_STER
	switch( uSelect % 47 ) {

		case  2: return	L"CycleNumber@11780$0L|4";
		case  3: return	L"CycleStatus@11784$0|8";
		case  4: return	L"CycleTime@11813$0T|37";
		case  5: return	L"EquilibrationTime@11788$0T|12";
		case  6: return	L"FailureDwell1@11810$0T|34";
		case  7: return	L"FailureDwell2@11819$0T|43";
		case  8: return	L"FailureDwell3@11820$0T|44";
		case  9: return	L"FailureDwell4@11821$0T|45";
		case 10: return	L"FileByTag@11809$0|33";
		case 11: return	L"Fvalue@11814$0T|38";
		case 12: return	L"Input1PV@11776$0F|0";
		case 13: return	L"Input2PV@11777$0F|1";
		case 14: return	L"Input3PV@11778$0F|2";
		case 15: return	L"Input4PV@11779$0F|3";
		case 16: return	L"InputType1@11805$0|29";
		case 17: return	L"InputType2@11806$0|30";
		case 18: return	L"InputType3@11807$0|31";
		case 19: return	L"InputType4@11808$0|32";
		case 20: return	L"IP1BandHigh@11786$0F|10";
		case 21: return	L"IP1BandLow@11787$0F|11";
		case 22: return	L"IP1TargetSP@11783$0F|7";
		case 23: return	L"IP2BandHigh@11792$0F|16";
		case 24: return	L"IP2BandLow@11793$0F|17";
		case 25: return	L"IP2TargetSP@11798$0F|22";
		case 26: return	L"IP3BandHigh@11794$0F|18";
		case 27: return	L"IP3BandLow@11795$0F|19";
		case 28: return	L"IP3TargetSP@11799$0F|23";
		case 29: return	L"IP4BandHigh@11796$0F|20";
		case 30: return	L"IP4BandLow@11797$0F|21";
		case 31: return	L"IP4TargetSP@11800$0F|24";
		case 32: return	L"LowLimit@11818$0F|42";
		case 33: return	L"MeasuredTemp@11815$0F|39";
		case 34: return	L"PassedOutput@11804$0|28";
		case 35: return	L"Remaining@11790$0T|14";
		case 36: return	L"RunningOutput@11803$0|27";
		case 37: return	L"Start121@11801$0|25";
		case 38: return	L"Start134@11802$0|26";
		case 39: return	L"StartCycle@11781$0|5";
		case 40: return	L"SterilisingTime@11789$0T|13";
		case 41: return	L"TargetTemperature@11817$0F|41";
		case 42: return	L"TargetTime@11785$0T|9";
		case 43: return	L"TargetTime121@11811$0T|35";
		case 44: return	L"TargetTime134@11812$0T|36";
		case 45: return	L"ZTemperatureInterval@11816$0F|40";
		case 46: return	L"z11782n@11782$0|6";
		}

	return	L"AutoCounter@11791$0|15";
	}

CString CEuroInvensysNanoDacDriver::LoadStrings(UINT uSelect)
{
// SP_STRS
	switch( uSelect % 66 ) {

		case  2: return	L"Channel.1.Main.Units@18709$0|21";
		case  3: return	L"Channel.2.Main.Descriptor@18715$0|27";
		case  4: return	L"Channel.2.Main.Units@18736$0|48";
		case  5: return	L"Channel.3.Main.Descriptor@18742$0|54";
		case  6: return	L"Channel.3.Main.Units@18763$0|75";
		case  7: return	L"Channel.4.Main.Descriptor@18769$0|81";
		case  8: return	L"Channel.4.Main.Units@18790$0|102";
		case  9: return	L"Group.Trend.Descriptor@23296$0|1100";
		case 10: return	L"Loop.1.Setup.LoopName@23808$0|1400";
		case 11: return	L"Loop.2.Setup.LoopName@23824$0|1424";
		case 12: return	L"Math2.1.Units@26948$0|1532";
		case 13: return	L"Math2.2.Units@26954$0|1538";
		case 14: return	L"Math2.3.Units@26960$0|1544";
		case 15: return	L"Math2.4.Units@26966$0|1550";
		case 16: return	L"Math2.5.Units@26972$0|1556";
		case 17: return	L"Math2.6.Units@26978$0|1562";
		case 18: return	L"Math2.7.Units@26984$0|1568";
		case 19: return	L"Math2.8.Units@26990$0|1574";
		case 20: return	L"Math2.9.Units@26996$0|1580";
		case 21: return	L"Math2.10.Units@27002$0|1586";
		case 22: return	L"Math2.11.Units@27008$0|1592";
		case 23: return	L"Math2.12.Units@27014$0|1598";
		case 24: return	L"nano_ui.Password@21504$0|1000";
		case 25: return	L"Steriliser.FileTag@26871$0|1450";
		case 26: return	L"UserVal.1.Units@26876$0|1460";
		case 27: return	L"UserVal.2.Units@26882$0|1466";
		case 28: return	L"UserVal.3.Units@26888$0|1472";
		case 29: return	L"UserVal.4.Units@26894$0|1478";
		case 30: return	L"UserVal.5.Units@26900$0|1484";
		case 31: return	L"UserVal.6.Units@26906$0|1490";
		case 32: return	L"UserVal.7.Units@26912$0|1496";
		case 33: return	L"UserVal.8.Units@26918$0|1502";
		case 34: return	L"UserVal.9.Units@26924$0|1508";
		case 35: return	L"UserVal.10.Units@26930$0|1514";
		case 36: return	L"UserVal.11.Units@26936$0|1520";
		case 37: return	L"UserVal.12.Units@26942$0|1526";
		case 38: return	L"VirtualChannel.1.Main.Descriptor@19200$0|130";
		case 39: return	L"VirtualChannel.1.Main.Units@19221$0|160";
		case 40: return	L"VirtualChannel.2.Main.Descriptor@19227$0|190";
		case 41: return	L"VirtualChannel.2.Main.Units@19248$0|220";
		case 42: return	L"VirtualChannel.3.Main.Descriptor@19254$0|250";
		case 43: return	L"VirtualChannel.3.Main.Units@19275$0|280";
		case 44: return	L"VirtualChannel.4.Main.Descriptor@19281$0|310";
		case 45: return	L"VirtualChannel.4.Main.Units@19302$0|340";
		case 46: return	L"VirtualChannel.5.Main.Descriptor@19308$0|370";
		case 47: return	L"VirtualChannel.5.Main.Units@19329$0|400";
		case 48: return	L"VirtualChannel.6.Main.Descriptor@19335$0|430";
		case 49: return	L"VirtualChannel.6.Main.Units@19356$0|460";
		case 50: return	L"VirtualChannel.7.Main.Descriptor@19362$0|490";
		case 51: return	L"VirtualChannel.7.Main.Units@19383$0|520";
		case 52: return	L"VirtualChannel.8.Main.Descriptor@19389$0|550";
		case 53: return	L"VirtualChannel.8.Main.Units@19410$0|580";
		case 54: return	L"VirtualChannel.9.Main.Descriptor@19416$0|610";
		case 55: return	L"VirtualChannel.9.Main.Units@19437$0|640";
		case 56: return	L"VirtualChannel.10.Main.Descriptor@19443$0|670";
		case 57: return	L"VirtualChannel.10.Main.Units@19464$0|700";
		case 58: return	L"VirtualChannel.11.Main.Descriptor@19470$0|730";
		case 59: return	L"VirtualChannel.11.Main.Units@19491$0|760";
		case 60: return	L"VirtualChannel.12.Main.Descriptor@19497$0|790";
		case 61: return	L"VirtualChannel.12.Main.Units@19518$0|820";
		case 62: return	L"VirtualChannel.13.Main.Descriptor@19524$0|850";
		case 63: return	L"VirtualChannel.13.Main.Units@19545$0|880";
		case 64: return	L"VirtualChannel.14.Main.Descriptor@19551$0|910";
		case 65: return	L"VirtualChannel.14.Main.Units@19573$0|940";
		}

	return	L"Channel.1.Main.Descriptor@18688$0|0";
	}

CString CEuroInvensysNanoDacDriver::LoadTimer(UINT uSelect)
{
// SP_TIME
	switch( uSelect % 7 ) {

		case 2: return	L"In@12005#6$6|5";
		case 3: return	L"Out@12001#6$6|1";
		case 4: return	L"Time@12002#6$6T|2";
		case 5: return	L"Triggered@12003#6$6|3";
		case 6: return	L"Type@12004#6$6|4";
		}

	return	L"ElapsedTime@12000#6$6T|0";
	}

CString CEuroInvensysNanoDacDriver::LoadUsrLin(UINT uSelect)
{
// SP_USRL
	switch( uSelect % 4 ) {

		case 2: return	L"X@10497#192$65F|1";
		case 3: return	L"Y@10498#192$65F|2";
		}

	return	L"NumberOfBreakpoints@10496#192$65|0";
	}

CString CEuroInvensysNanoDacDriver::LoadUsrVal(UINT uSelect)
{
// SP_USRV
	switch( uSelect % 6 ) {
		case 2: return	L"LowLimit@11917#5$5F|1";
		case 3: return	L"Resolution@11920#5$5|4";
		case 4: return	L"Status@11919#5$5|3";
		case 5: return	L"Val@11918#5$5F|2";
		}

	return	L"HighLimit@11916#5$5F|0";
	}

CString CEuroInvensysNanoDacDriver::LoadVirtAlmStat(UINT uSelect)
{
// SP_VAAS
	switch( uSelect % 5 ) {

		case 2: return	L"1.Status@290#4$2|0";
		case 3: return	L"2.Acknowledge@449#2$2|29";
		case 4: return	L"2.Status@291#4$2|1";
		}

	return	L"1.Acknowledge@448#2$2|28";
	}

CString CEuroInvensysNanoDacDriver::LoadVirtAlarm(UINT uSelect)
{
// SP_VALA
	switch( uSelect % 35 ) {

		case  2: return	L"1.Active@7243#128$34|11";
		case  3: return	L"1.Amount@7240#128$34F|8";
		case  4: return	L"1.AverageTime@7242#128$34T|10";
		case  5: return	L"1.Block@7234#128$34|2";
		case  6: return	L"1.ChangeTime@7241#128$34|9";
		case  7: return	L"1.Deviation@7239#128$34F|7";
		case  8: return	L"1.Dwell@7237#128$34T|5";
		case  9: return	L"1.Hysteresis@7236#128$34F|4";
		case 10: return	L"1.Inactive@7246#128$34|14";
		case 11: return	L"1.Latch@7233#128$34|1";
		case 12: return	L"1.NotAcknowledged@7247#128$34|15";
		case 13: return	L"1.Reference@7238#128$34F|6";
		case 14: return	L"1.Threshold@7235#128$34F|3";
		case 15: return	L"1.Type@7232#128$34|0";
		case 16: return	L"2.Acknowledgement@7280#128$34|33";
		case 17: return	L"2.Active@7275#128$34|28";
		case 18: return	L"2.Amount@7272#128$34F|25";
		case 19: return	L"2.AverageTime@7274#128$34|27";
		case 20: return	L"2.Block@7266#128$34|19";
		case 21: return	L"2.ChangeTime@7273#128$34|26";
		case 22: return	L"2.Deviation@7271#128$34F|24";
		case 23: return	L"2.Dwell@7269#128$34|22";
		case 24: return	L"2.Hysteresis@7268#128$34F|21";
		case 25: return	L"2.Inactive@7278#128$34|31";
		case 26: return	L"2.Latch@7265#128$34|18";
		case 27: return	L"2.NotAcknowledged@7279#128$34|32";
		case 28: return	L"2.Reference@7270#128$34F|23";
		case 29: return	L"2.Threshold@7267#128$34F|20";
		case 30: return	L"2.Type@7264#128$34|17";
		case 31: return	L"1.x7244n@7244#128$34|12";
		case 32: return	L"1.x7245n@7245#128$34|13";
		case 33: return	L"2.x7276n@7276#128$34|29";
		case 34: return	L"2.x7277n@7277#128$34|30";
		}

	return	L"1.Acknowledgement@7248#128$34|16";
	}

CString CEuroInvensysNanoDacDriver::LoadVirtMain(UINT uSelect)
{
// SP_VMT
	switch( uSelect % 23 ) {

		case  2: return	L"Main.HighCutOff@7173#128$21F|33";
		case  3: return	L"Main.Input1@7175#128$21F|35";
		case  4: return	L"Main.Input2@7176#128$21F|36";
		case  5: return	L"Main.LowCutOff@7172#128$21F|32";
		case  6: return	L"Main.ModbusInput@7174#128$21F|34";
		case  7: return	L"Main.Operation@7169#128$21|29";
		case  8: return	L"Main.Period@7178#128$21L|38";
		case  9: return	L"Main.Preset@7180#128$21|40";
		case 10: return	L"Main.PresetValue@7181#128$21F|41";
		case 11: return	L"Main.PV@288#4$2F|0";
		case 12: return	L"Main.Reset@7179#128$21|39";
		case 13: return	L"Main.Resolution@7170#128$21|30";
		case 14: return	L"Main.Rollover@7185#128$21|43";
		case 15: return	L"Main.Status@289#4$2|1";
		case 16: return	L"Main.TimeRemaining@7177#128$21T|37";
		case 17: return	L"Main.Trigger@7182#128$21|42";
		case 18: return	L"Main.Type@7168#128$21|28";
		case 19: return	L"Main.UnitsScaler@7171#128$21F|31";
		case 20: return	L"Trend.Colour@7200#128$21|44";
		case 21: return	L"Trend.SpanHigh@7202#128$21F|46";
		case 22: return	L"Trend.SpanLow@7201#128$21F|45";
		}

	return	L"Main.Disable@7203#128$21|47";
	}

CString CEuroInvensysNanoDacDriver::LoadZirconia(UINT uSelect)
{
// SP_ZIR
	switch( uSelect % 71 ) {

		case  2: return	L"BalanceIntegral@10397$0|29";
		case  3: return	L"CarbonPot@10386$0F|18";
		case  4: return	L"Clean.AbortClean@10421$0|53";
		case  5: return	L"Clean.CantClean@10435$0|67";
		case  6: return	L"Clean.CleanAbort@10436$0|68";
		case  7: return	L"Clean.CleanEnable@10418$0|50";
		case  8: return	L"Clean.CleanFreq@10410$0T|42";
		case  9: return	L"Clean.CleanMaxTemp@10420$0F|52";
		case 10: return	L"Clean.CleanMsgReset@10419$0|51";
		case 11: return	L"Clean.CleanProbe@10416$0|48";
		case 12: return	L"Clean.CleanRecoveryTime@10422$0T|54";
		case 13: return	L"Clean.CleanTemp@10437$0|69";
		case 14: return	L"Clean.CleanTime@10411$0T|43";
		case 15: return	L"Clean.CleanValve@10415$0|47";
		case 16: return	L"Clean.LastCleanMv@10423$0F|55";
		case 17: return	L"Clean.MaxRcovTime@10413$0T|45";
		case 18: return	L"Clean.MinRcovTime@10412$0T|44";
		case 19: return	L"Clean.ProbeFault@10414$0|46";
		case 20: return	L"Clean.Time2Clean@10417$0T|49";
		case 21: return	L"CleanFreq@10377$0T|9";
		case 22: return	L"CleanProbe@10394$0|26";
		case 23: return	L"CleanState@10393$0|25";
		case 24: return	L"CleanTime@10378$0T|10";
		case 25: return	L"CleanValve@10392$0|24";
		case 26: return	L"DewPoint@10387$0F|19";
		case 27: return	L"GasRef@10370$0F|2";
		case 28: return	L"GasRefs.CO_Ideal@10409$0F|41";
		case 29: return	L"GasRefs.CO_InUse@10404$0F|36";
		case 30: return	L"GasRefs.CO_Local@10401$0F|33";
		case 31: return	L"GasRefs.CO_Remote@10402$0F|34";
		case 32: return	L"GasRefs.CO_RemoteEn@10403$0|35";
		case 33: return	L"GasRefs.H2_InUse@10408$0F|40";
		case 34: return	L"GasRefs.H2_Local@10405$0F|37";
		case 35: return	L"GasRefs.H2_Remote@10406$0F|38";
		case 36: return	L"GasRefs.H2_RemoteEn@10407$0|39";
		case 37: return	L"MaxRcovTime@10380$0T|12";
		case 38: return	L"MinCalTemp@10374$0F|6";
		case 39: return	L"MinRcovTime@10379$0T|11";
		case 40: return	L"NumResolution@10369$0|1";
		case 41: return	L"Oxygen@10388$0F|20";
		case 42: return	L"OxygenExp@10381$0|13";
		case 43: return	L"OxygenType@10400$0|32";
		case 44: return	L"ProbeFault@10390$0|22";
		case 45: return	L"ProbeInput@10384$0F|16";
		case 46: return	L"ProbeOffset@10385$0F|17";
		case 47: return	L"ProbeState@10399$0|31";
		case 48: return	L"ProbeStatus@10396$0|28";
		case 49: return	L"ProbeType@10368$0|0";
		case 50: return	L"ProcFactor@10376$0F|8";
		case 51: return	L"PVFrozen@10391$0|23";
		case 52: return	L"RemGasEn@10372$0|4";
		case 53: return	L"RemGasRef@10371$0F|3";
		case 54: return	L"SootAlm@10389$0|21";
		case 55: return	L"TempInput@10382$0F|14";
		case 56: return	L"TempOffset@10383$0F|15";
		case 57: return	L"Time2Clean@10395$0T|27";
		case 58: return	L"Tolerance@10375$0F|7";
		case 59: return	L"WrkGas@10373$0F|5";
		case 60: return	L"z10424n@10424$0|56";
		case 61: return	L"z10425n@10425$0|57";
		case 62: return	L"z10426n@10426$0|58";
		case 63: return	L"z10427n@10427$0|59";
		case 64: return	L"z10428n@10428$0|60";
		case 65: return	L"z10429n@10429$0|61";
		case 66: return	L"z10430n@10430$0|62";
		case 67: return	L"z10431n@10431$0|63";
		case 68: return	L"z10432n@10432$0|64";
		case 69: return	L"z10433n@10433$0|65";
		case 70: return	L"z10434n@10434$0|66";
		}

	return	L"aC_CO_O2@10398$0F|30";
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Serial Comms Driver
//

// Instantiator

ICommsDriver *	Create_EuroInvensysNanoDacSerialDriver(void)
{
	return New CEuroInvensysNanoDacSerialDriver;
	}

// Destructor

CEuroInvensysNanoDacSerialDriver::~CEuroInvensysNanoDacSerialDriver(void)
{
	}

CEuroInvensysNanoDacSerialDriver::CEuroInvensysNanoDacSerialDriver(void)
{
	m_wID		= 0x409F;

	m_uType		= driverMaster;

	m_Manufacturer	= "Eurotherm";

	m_DriverName	= "NanoDac Serial";

	m_Version	= "1.00";

	m_ShortName	= "Eurotherm NanoDac";
	}

// Binding Control

UINT  CEuroInvensysNanoDacSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void  CEuroInvensysNanoDacSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys TCP Comms Driver
//

// Instantiator

ICommsDriver *	Create_EuroInvensysNanoDacTCPDriver(void)
{
	return New CEuroInvensysNanoDacTCPDriver;
	}

CEuroInvensysNanoDacTCPDriver::CEuroInvensysNanoDacTCPDriver(void)
{
	m_wID		= 0x3542;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Eurotherm";
	
	m_DriverName	= "NanoDac TCP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Eurotherm NanoDac";
	}

// Destructor

CEuroInvensysNanoDacTCPDriver::~CEuroInvensysNanoDacTCPDriver(void)
{
	}

// Binding Control

UINT CEuroInvensysNanoDacTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEuroInvensysNanoDacTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CEuroInvensysNanoDacTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CEuroInvensysNanoDacTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEuroInvensysNanoDacTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEuroInvensysNanoDacAddrDialog, CStdAddrDialog);
		
// Constructor

CEuroInvensysNanoDacAddrDialog::CEuroInvensysNanoDacAddrDialog(CEuroInvensysNanoDacDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pSpace  = NULL;

	m_pGDrv   = &Driver;

	m_uNType  = 0;

	m_pEIE	= m_pGDrv->GetEIEPtr();

	SetName(TEXT("CEuroInvensysNanoDacAddressDlg"));
	}

// Destructor
CEuroInvensysNanoDacAddrDialog::~CEuroInvensysNanoDacAddrDialog(void)
{
	}

// Message Map

AfxMessageMap(CEuroInvensysNanoDacAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001,  LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(DNAME, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(DNUMB, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(DSEGM, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(4001,  LBN_SELCHANGE,	OnTypeChange )

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CEuroInvensysNanoDacAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	LoadList();

	GetDlgItem(DNAME).SetWindowText(TEXT(""));
	GetDlgItem(DNUMB).SetWindowText(TEXT(""));
	GetDlgItem(DSEGM).SetWindowText(TEXT(""));

	SetBoxPosition(DNAME, 0);
	SetBoxPosition(DNUMB, 0);
	SetBoxPosition(DSEGM, 0);

	if( m_pSpace ) {

		UINT uTable = Addr.m_Ref ? Addr.a.m_Table : m_pSpace->m_uTable;

		m_pEIE->uTable = uTable;

		BOOL f = m_pGDrv->IsNotStdModbus(uTable);

		GetDlgItem(DNAME).EnableWindow(f);
		GetDlgItem(DNUMB).EnableWindow(f);

		BOOL f1 = m_pGDrv->IsUsrLin();

		GetDlgItem(DSEGM).EnableWindow(f1);
		GetDlgItem(DSEGT).EnableWindow(f1);

		ClearSelData();
		InitSelects();

		if( f ) {

			if( Addr.m_Ref ) {

				CString Text;

				InitFromAddr(Addr);

				if( m_pGDrv->DoExpandAddress(Text, NULL, Addr) ) {

					GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

					ShowSelection();
					}

				return FALSE;
				}

			OnSpaceChange(1001, Focus);

			ShowSelection();

			return FALSE;
			}
	
		if( !m_fPart ) {

			if( m_pSpace ) {

				ShowAddress(Addr);

				SetAddressFocus();

				return FALSE;
				}

			return TRUE;
			}

		else {
			LoadType();

			ShowAddress(Addr);

			if( TRUE ) {

				CString Text = m_pSpace->m_Prefix;

				Text += GetAddressText();

				Text += GetTypeText();

				CError   Error(FALSE);

				CAddress Addr;
			
				if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

					CAddress Addr;

					m_pSpace->GetMinimum(Addr);

					ShowAddress(Addr);
					}
				}

			SetAddressFocus();

			return FALSE;
			}
		}

	ClearSelData();

	GetDlgItem(DNAME).EnableWindow(FALSE);
	GetDlgItem(DNUMB).EnableWindow(FALSE);
	GetDlgItem(DSEGM).EnableWindow(FALSE);
	GetDlgItem(DMODB).EnableWindow(FALSE);

	return FALSE;
	}

// Notification Handlers

void CEuroInvensysNanoDacAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CEuroInvensysNanoDacAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		BOOL f = m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable);

		LoadType();

		if( m_pSpace != pSpace ) {

			if( !f ) {

				LoadType();

				m_pSpace      = NULL;

				CStdAddrDialog::OnSpaceChange(uID, Wnd);

				ShowDetails();
				}
			}

		if( f ) {

			m_pEIE->uTable = m_pSpace->m_uTable;

			ClearAddress();
			ClearDetails();
			ClearSelData();
			InitSelects();

			InitItemInfo(m_pSpace->m_uTable);

			ShowSelection();
			}

		GetDlgItem(DNAME).EnableWindow(f);
		}

	else {
		ClearAddress();
		ClearDetails();
		ClearSelData();
		InitSelects();

		m_pSpace = NULL;
		}
	}

void CEuroInvensysNanoDacAddrDialog::OnComboChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		if( m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable) ) {

			UINT uTable	= m_pSpace->m_uTable;

			EIESTRINGS * pE	= m_pEIE;

			pE->uNPos	= GetBoxPosition(DNAME);

			pE->uBPos	= GetBoxPosition(DNUMB);

			pE->uSPos	= GetBoxPosition(DSEGM);

			pE->uBlock	= pE->uBPos + 1;

			pE->uSegm	= pE->uSPos + 1;

			pE->uOffset	= GetOffsetFromPos(uTable, 1 + pE->uNPos);

			ShowSelection();
			}

		switch( uID ) {

			case DNAME:
			case DNUMB:
			case DSEGM:
				SetDlgFocus(uID);
				break;

			default:
				SetDlgFocus(DPADD);
				break;
			}
		}

	else {
		SetDlgFocus(1001);
		}
	}

void CEuroInvensysNanoDacAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		ShowDetails();

		ShowType(GetTypeCode());
		}
	}

// Command Handlers
BOOL CEuroInvensysNanoDacAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		CString sAdd = GetDlgItem(DPADD).GetWindowTextW();

		while( sAdd.GetLength() > 1 ) {

			if( sAdd[0] == '0' ) {

				sAdd = sAdd.Mid(1);
				}

			else break;
			}

		Text  += sAdd;

		if( !m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable) ) {

			Text += GetTypeText();
			}

		CError   Error(TRUE);

		CAddress Addr;

		if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			if( m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable) ) {

				Addr.a.m_Table	= m_pSpace->m_uTable;
				Addr.a.m_Offset	= m_pEIE->uOffset;
				Addr.a.m_Type	= m_pSpace->m_uType;
				Addr.a.m_Extra	= 1;
				}

			else {
				Addr.a.m_Table	= SP_4;
				Addr.a.m_Offset	= 1;
				Addr.a.m_Type	= m_pSpace->m_uType;
				Addr.a.m_Extra	= 0;
				}
			}

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overrides
void CEuroInvensysNanoDacAddrDialog::LoadType(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(4001);
	
	ListBox.SetRedraw(FALSE);

	ListBox.ResetContent();

	UINT uFind = m_pSpace->m_uType;

	if( m_pSpace->m_uType > m_pSpace->m_uSpan ) {

		ListBox.AddString(m_pSpace->GetTypeAsText(m_pSpace->m_uType), m_pSpace->m_uType);

		ListBox.AddString(m_pSpace->GetTypeAsText(m_pSpace->m_uSpan), m_pSpace->m_uSpan);

		if( UINT(m_pAddr->a.m_Type) == m_pSpace->m_uType ) {

			uFind = m_pSpace->m_uType;
			}

		if( UINT(m_pAddr->a.m_Type) == m_pSpace->m_uSpan ) {

			uFind = m_pSpace->m_uSpan;
			}
		}
	else {
		for( UINT uType = m_pSpace->m_uType; uType <= m_pSpace->m_uSpan; uType++ ) {

			if( AllowType(uType) ) {

				ListBox.AddString(m_pSpace->GetTypeAsText(uType), uType);

				if( UINT(m_pAddr->a.m_Type) == uType ) {

					uFind = uType;
					}
				}
			}
		}

	ListBox.SelectData(DWORD(uFind));

	ListBox.EnableWindow(TRUE);

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnTypeChange(4001, ListBox);
	}

BOOL CEuroInvensysNanoDacAddrDialog::AllowType(UINT uType)
{
	switch( m_uNType ) {

		case 0:
			switch( uType ) {

				case addrLongAsLong:
				case addrRealAsReal:
				case addrWordAsWord:
						return TRUE;
				}
			break;

		case LL: return uType == LL;
		case RR: return uType == RR;
		case WW: return uType == WW;
		}

	return FALSE;
	}

void CEuroInvensysNanoDacAddrDialog::ShowDetails(void)
{
	if( m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable ) ) {

		ShowNanoDacDetails();
		}

	else {
		CStdAddrDialog::ShowDetails();
		}
	}

// Selecton Handling
UINT CEuroInvensysNanoDacAddrDialog::GetOffsetFromPos(UINT uTable, UINT uPos)
{
	CString s = m_pGDrv->GetParamString(uTable, uPos);

	UINT uFind = s.Find('|');

	UINT uOffs = tatoi(s.Mid(uFind+1));

	UINT uAdj  = 0;

	uFind	= s.Find('$');

	uAdj	= tatoi(s.Mid(uFind+1));

	m_pEIE->uIncr = uAdj;

	if( m_pGDrv->IsUsrLin() ) {

		uAdj = 65 * m_pEIE->uBPos;

		m_pEIE->uIncr = 0;

		if( uOffs ) {	// is X or Y value

			uAdj += m_pEIE->uSPos * 2;
			}

		return uOffs + uAdj;
		}

	else {
		if( !uAdj ) {

			uFind = s.Find('#');	// contiguous range throughout table selection

			if( uFind < NOTHING ) {

				uAdj = m_pEIE->uQty[m_pEIE->uTable];
				}
			}
		}

	return uOffs + (m_pEIE->uBPos * uAdj);
	}

void CEuroInvensysNanoDacAddrDialog::InitItemInfo(UINT uTable)
{
	ClearSelData();

	m_pEIE->uTable	= uTable;

	m_pEIE->uBlock	= 1;

	m_pEIE->uIncr	= 0;

	m_pEIE->uNPos	= m_pGDrv->GetPosFromOffset(uTable, 0) - 1;

	m_pEIE->uBPos	= 0;

	m_pEIE->uSPos	= 0;

	m_pEIE->uSegm	= 1;

	m_pEIE->uOffset	= 0;

	LoadInfo();
	}

void CEuroInvensysNanoDacAddrDialog::InitFromAddr(CAddress Addr)
{
	EIESTRINGS *pE	= m_pEIE;

	UINT uTable	= Addr.a.m_Table;
	UINT uOffset	= Addr.a.m_Offset;

	pE->uTable	= uTable;
	pE->uOffset	= uOffset;
	pE->uNPos	= m_pGDrv->GetPosFromOffset(uTable, uOffset) - 1;

	if( m_pGDrv->IsUsrLin() ) {

		pE->uBPos	= uOffset / 65;
		pE->uSegm	= m_pGDrv->GetUsrLinSegm(uOffset);
		pE->uSPos	= pE->uSegm - 1;
		}

	else {
		if( m_pEIE->uIncr ) {

			UINT uIncr   = m_pEIE->uIncr;

			UINT uAdjust = (m_pEIE->uQty[uTable] - uIncr) * m_pEIE->uBlkCt[uTable];

			if( uAdjust ) {	// multi-range table

				if( uOffset >= uAdjust ) {	// item is in upper range

					uOffset -= uAdjust;
					}
				}

			pE->uBPos = uOffset / m_pEIE->uIncr;
			}

		else {
			pE->uBPos = 0;
			}

		pE->uSPos	= 0;
		pE->uSegm	= 1;
		}

	pE->uBlock	= pE->uBPos + 1;

	LoadInfo();
	}

void CEuroInvensysNanoDacAddrDialog::LoadInfo(void)
{
	LoadNames();

	SetBlockNumbers();

	SetBoxPosition(DNUMB, m_pEIE->uBlock - 1);

	SetSegmNumbers();

	BOOL fSeg = EnableSegments();

	GetDlgItem(DSEGM).EnableWindow(fSeg);

	SetBoxPosition(DNAME, m_pEIE->uNPos);

	SetBoxPosition(DSEGM, (fSeg ? m_pEIE->uSegm - 1 : 0));

	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
	}

void CEuroInvensysNanoDacAddrDialog::LoadNames(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DNAME);

	ClearBox(DNAME);

	EIESTRINGS *pE	= m_pEIE;

	UINT uTable	= pE->uTable;

	CString sName	= pE->sName[uTable];

	CString sFound	= sName;

	for( UINT i = 1; i <= pE->uQty[uTable]; i++ ) {

		CString s = sName + m_pGDrv->GetParamString(uTable, i);

		CString t = StripAddr(s);

		Box.AddString(t);

		if( i == pE->uNPos + 1 ) {

			sFound = t;
			}
		}

	GetDlgItem(DNAME).EnableWindow(TRUE);
	GetDlgItem(DNAME).SetWindowTextW(sFound);
	}

void CEuroInvensysNanoDacAddrDialog::SetBoxPosition(UINT uID, UINT uPos)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CEuroInvensysNanoDacAddrDialog::GetBoxPosition(UINT uID)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	return Box.GetCurSel();
	}

void CEuroInvensysNanoDacAddrDialog::SetBlockNumbers(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DNUMB);

	ClearBox(DNUMB);

	UINT uBlkCt = m_pEIE->uBlkCt[m_pEIE->uTable];

	for( UINT i = 1; i <= uBlkCt; i++ ) {

		CString sCount = GetBlockString(i);

		Box.AddString(sCount);
		}

	GetDlgItem(DNUMB).EnableWindow(uBlkCt > 1 );
	}

CString CEuroInvensysNanoDacAddrDialog::GetBlockString(UINT uSel)
{
	CString s;

	switch( m_pEIE->uTable ) {

		case SP_DCO:
			switch( uSel ) {
				case 1: s = SDCOA;	break;
				case 2: s = SDCOB;	break;
				case 3: s = SDCOC;	break;
				}

			return s + SDCOD;

		case SP_DIO:
			switch( uSel ) {

				case 1: return SDIOA;
				case 2: return SDIOB;
				case 3: return SDIOC;
				case 4: return SDIOD;
				case 5: return SDIOE;
				case 6: return SDIOF;
				case 7: return SDIOG;
				}
		}

	s.Printf( L"%d", uSel );

	return s;
	}

void CEuroInvensysNanoDacAddrDialog::SetSegmNumbers(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DSEGM);

	ClearBox(DSEGM);

	BOOL f = m_pGDrv->IsUsrLin();

	if( f ) {

		for( UINT i = 1; i <= 32; i++ ) {

			CString sCount;

			sCount.Printf(TEXT("%d"), i);

			Box.AddString(sCount);
			}

		return;
		}

	else {
		Box.AddString(L"0");
		}

	GetDlgItem(DSEGM).EnableWindow(f);
	}

void CEuroInvensysNanoDacAddrDialog::ShowSelection(void)
{
	EIESTRINGS * pE = m_pEIE;

	CSpace     * pS = m_pSpace;

	GetDlgItem(2001).SetWindowTextW(pS->m_Prefix);

	CString s = pE->uTable == SP_NANO ? L"0" : pS->GetValueAsText(pE->uOffset);

	while( s.GetLength() > 1 ) {

		if( s[0] == '0' ) {

			s = s.Mid(1);
			}

		else break;
		}

	GetDlgItem(DPADD).SetWindowTextW(s);

	SetBoxPosition(DNAME, pE->uNPos);
	SetBoxPosition(DNUMB, pE->uBPos);

	BOOL fSeg = EnableSegments();

	if( !fSeg ) {

		pE->uSPos = 0;
		}
	
	SetBoxPosition(DSEGM, pE->uSPos);

	GetDlgItem(DSEGM).EnableWindow(fSeg);

	ShowNanoDacDetails();

	m_uNType = GetDataType(pE->uTable, pE->uNPos + 1);

	LoadType();

	GetDlgItem(3002).SetWindowTextW(pS->GetTypeAsText(m_uNType));

	m_uNType = 0;
	}

BOOL CEuroInvensysNanoDacAddrDialog::EnableSegments(void)
{
	return m_pGDrv->IsUsrLin() && (BOOL)(m_pEIE->uOffset % 65);
	}

// Helpers
void CEuroInvensysNanoDacAddrDialog::InitSelects(void)
{
	m_pEIE->uNPos = 0;
	m_pEIE->uBPos = 0;
	m_pEIE->uSPos = 0;
	}

void CEuroInvensysNanoDacAddrDialog::ClearSelData(void)
{
	GetDlgItem(DPADD).SetWindowText(SNONE);
	GetDlgItem(DMODB).SetWindowText(SNONE);

	ClearBox(DNAME);
	ClearBox(DNUMB);
	}

void CEuroInvensysNanoDacAddrDialog::ClearBox(CComboBox & ccb)
{
	UINT uCount = ccb.GetCount();

	for( UINT i = 0; i < uCount; i++ ) {

		ccb.DeleteString(0);
		}
	}

void CEuroInvensysNanoDacAddrDialog::ClearBox(UINT uID)
{
	ClearBox((CComboBox &)GetDlgItem(uID));
	}

void CEuroInvensysNanoDacAddrDialog::ShowNanoDacDetails(void)
{
	CAddress Addr;
	CString  Min;
	CString  Max;

	UINT uTable	= m_pSpace->m_uTable;

	Addr.a.m_Table  = uTable;
	Addr.a.m_Offset = m_pSpace->m_uMinimum;
	Addr.a.m_Type	= m_pSpace->m_uType;

	m_pGDrv->DoExpandAddress(Min, NULL, Addr);

	GetDlgItem(3004).SetWindowText(Min);

	UINT uMax	= m_pSpace->m_uMaximum;

	switch( uTable ) {

		case SP_INSS: uMax -= 49;	break;	// display maximums as start of strings
		case SP_NETS: uMax -= 64;	break;
		case SP_STRS: uMax -= 5;	break;
		}

	Addr.a.m_Offset = uMax;

	m_pGDrv->DoExpandAddress(Max, NULL, Addr);

	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(L"Decimal");

	ShowModbusAddr();
	}

void CEuroInvensysNanoDacAddrDialog::ShowModbusAddr(void)
{
	UINT uActual	= GetModbusAddr();

	CString sAddr;

	if( m_pGDrv->IsStringSelect(m_pEIE->uTable) || (m_pEIE->uTable == SP_CMSG && m_pEIE->uOffset > 9) ) {

		sAddr.Printf( L"%d", uActual);
		}

	else {
		sAddr.Printf( L"%d / %d", uActual, 0x8000 + (uActual * 2) );
		}

	GetDlgItem(DMODB).EnableWindow(TRUE);

	GetDlgItem(DMODB).SetWindowTextW(sAddr);
	}

UINT CEuroInvensysNanoDacAddrDialog::GetModbusAddr(void)
{
	CString sParam	= m_pGDrv->GetParamString(m_pEIE->uTable, m_pEIE->uNPos+1);

	CString sAddr	= sParam.Mid(sParam.Find('@') + 1);

	UINT uAddr	= tatoi(sAddr);			// base Modbus address for selection

	if( m_pEIE->uBlkCt[m_pEIE->uTable] > 1 ) {	// construct Modbus Address

		if( m_pGDrv->IsUsrLin() ) {

			if( uAddr != 10496 ) {	// is X or Y address

				uAddr += m_pEIE->uSPos * 2;
				}
			}

		UINT uInc = tatoi(sParam.Mid(sParam.Find('#') + 1));

		uAddr += (m_pEIE->uBlock - 1) * uInc;
		}

	return uAddr;
	}

CString CEuroInvensysNanoDacAddrDialog::StripAddr(CString sSelect)
{
	return sSelect.Left(sSelect.Find('@'));
	}

UINT CEuroInvensysNanoDacAddrDialog::GetDataType(UINT uTable, UINT uSelect)
{
	CString s = m_pGDrv->GetParamString( uTable, uSelect );

	UINT uFind = s.Find('|');

	switch( s[uFind - 1] ) {

		case 'F': return RR;
		case 'L':
		case 'T': return LL;
		}

	return WW;
	}

// End of File
