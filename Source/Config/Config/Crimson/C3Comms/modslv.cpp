
#include "intern.hpp"

#include "modslv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CModbusSlaveDriverOptions, CUIItem);

// Constructor

CModbusSlaveDriverOptions::CModbusSlaveDriverOptions(void)
{
	m_Drop     = 1;

	m_FlipLong = 1;

	m_FlipReal = 1;
	}

// UI Managament

void CModbusSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CModbusSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_FlipLong));

	Init.AddByte(BYTE(m_FlipReal));
	
	return TRUE;
	}

// Meta Data Creation

void CModbusSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(FlipLong);
	Meta_AddInteger(FlipReal);
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Gateway Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CModbusDeviceServerDriverOptions, CModbusSlaveDriverOptions);

// Constructor

CModbusDeviceServerDriverOptions::CModbusDeviceServerDriverOptions(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Server Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CModbusDeviceServerDeviceOptions, CUIItem);

// Constructor

CModbusDeviceServerDeviceOptions::CModbusDeviceServerDeviceOptions(void)
{
	m_Unit     = 1;

	m_FlipLong = 0;
	
	m_FlipReal = 0;

	m_UseFlip  = 0;
	}

// UI Managament

void CModbusDeviceServerDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "UseFlip")  {

		pWnd->EnableUI("FlipLong", m_UseFlip);

		pWnd->EnableUI("FlipReal", m_UseFlip);
		}
	}

// Download Support

BOOL CModbusDeviceServerDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Unit));

	Init.AddByte(BYTE(m_UseFlip));
	
	if( m_UseFlip ) {

		Init.AddByte(BYTE(m_FlipLong));
		Init.AddByte(BYTE(m_FlipReal));
		}	

	return TRUE;
	}

// Meta Data Creation

void CModbusDeviceServerDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Unit);
	Meta_AddInteger(FlipLong);
	Meta_AddInteger(FlipReal);
	Meta_AddInteger(UseFlip);
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Base Driver
//

// Constructor

CModbusSlaveDriver::CModbusSlaveDriver(void)
{
	AddSpaces();
	}

// Configuration

CLASS CModbusSlaveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModbusSlaveDriverOptions);
	}

CLASS CModbusSlaveDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Address Management

BOOL CModbusSlaveDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CModbusSlaveAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CModbusSlaveDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	#if defined(C3_VERSION)

	UINT uFind = Text.Find(L"-S16");

	#else

	UINT uFind = Text.Find("-S16");

	#endif

	BOOL fSigned16 = uFind < NOTHING;

	if( fSigned16 ) {

		Text = Text.Left(uFind);
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Extra = fSigned16;

		return TRUE;
		}

	return FALSE;
	}

BOOL CModbusSlaveDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr) ) {

		if( Addr.a.m_Extra ) {

			Text += "-S16";
			}

		return TRUE;
		}

	return FALSE;
	}


// Implementation

void CModbusSlaveDriver::AddSpaces(void)
{
	AddSpace(New CSpace(2, "4", "Holding Registers",		10,  1, 65535, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(1, "3", "Analog Inputs",			10,  1, 65535, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(3, "0", "Digital Coils",			10,  1, 65535, addrBitAsBit));

	AddSpace(New CSpace(4, "1", "Digital Inputs",			10,  1, 65535, addrBitAsBit));

	AddSpace(New CSpace(6, "L4", "Holding Registers (32-bit)",	10,  1, 65535, addrLongAsLong, addrLongAsReal));
	
//	AddSpace(New CSpace(5, "L3", "Analog Inputs (32-bit)",		10,  1, 65535, addrLongAsLong, addrLongAsReal)); 
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave RTU Driver
//

// Instantiator

ICommsDriver * Create_ModbusSlaveRTUDriver(void)
{
	return New CModbusSlaveRTUDriver;
	}

// Constructor

CModbusSlaveRTUDriver::CModbusSlaveRTUDriver(void)
{
	m_wID		= 0x3401;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "RTU Slave";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Modbus RTU Slave";

	C3_PASSED();
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave ASCII Driver
//

// Instantiator

ICommsDriver * Create_ModbusSlaveASCIIDriver(void)
{
	return New CModbusSlaveASCIIDriver;
	}

// Constructor

CModbusSlaveASCIIDriver::CModbusSlaveASCIIDriver(void)
{
	m_wID		= 0x3402;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "ASCII Slave";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Modbus ASCII Slave";

	C3_PASSED();
	}


//////////////////////////////////////////////////////////////////////////
//
// Modbus Serial Device Gateway Slave Driver
//

// Instantiator

ICommsDriver * Create_ModbusDeviceServerSerialDriver(void)
{
	return New CModbusDeviceServerSerialDriver;
	}


// Constructor

CModbusDeviceServerSerialDriver::CModbusDeviceServerSerialDriver(void)
{
	m_wID		= 0x4084;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "Device Gateway";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Modbus Gateway";
	}

// Configuration

CLASS CModbusDeviceServerSerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModbusDeviceServerDriverOptions);
	}

CLASS CModbusDeviceServerSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CModbusDeviceServerDeviceOptions);
	}


//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CModbusSlaveAddrDialog, CStdAddrDialog);
		
// Constructor

CModbusSlaveAddrDialog::CModbusSlaveAddrDialog(CModbusSlaveDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "ModbusSlaveElementDlg";
	}

// Message Map

AfxMessageMap(CModbusSlaveAddrDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(CModbusSlaveAddrDialog)
	};

// Message Handlers

BOOL CModbusSlaveAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( CStdAddrDialog::OnInitDialog(Focus, dwData) ) {

		GetDlgItem(2007).EnableWindow(FALSE);

		return TRUE;
		}
	
	return FALSE;
	}

// Notification Handlers

void CModbusSlaveAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	BOOL fEnable = FALSE;
	
	if( m_pSpace ) {

		UINT uTable = m_pSpace->m_uTable;

		fEnable = uTable == 1 || uTable == 2;
		}

	GetDlgItem(2007).EnableWindow(fEnable);
   	}

void CModbusSlaveAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnTypeChange(uID, Wnd);
	
	BOOL fEnable = FALSE;

	if( m_pSpace ) {

		UINT uType = m_pSpace->m_uType;

		CListBox &ListBox = (CListBox &) GetDlgItem(4001);

		fEnable = uType + ListBox.GetCurSel() == addrWordAsWord;
		}

	CButton &Check = (CButton &) GetDlgItem(2007);

	Check.EnableWindow(fEnable);

	if( !fEnable ) {

		Check.SetCheck(FALSE);
		}
	}

// Overridables

void CModbusSlaveAddrDialog::SetAddressText(CString Text)
{
	#if defined(C3_VERSION)

	UINT uFind = Text.Find(L"-S16");

	#else

	UINT uFind = Text.Find("-S16");

	#endif

	if( uFind < NOTHING ) {

		CButton &Check = (CButton &) GetDlgItem(2007);

		Check.SetCheck(TRUE);

		Text = Text.Left(uFind);
		}
		
	CStdAddrDialog::SetAddressText(Text);
	}

CString CModbusSlaveAddrDialog::GetAddressText(void)
{
	CString Text = CStdAddrDialog::GetAddressText();

	CButton &Check = (CButton &) GetDlgItem(2007);

	if( Check.IsChecked() ) {

		Text += "-S16";
		}

	return Text;
	}

// End of File
