
#include "intern.hpp"

#include "yaskiec.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaMPIECDeviceOptions, CUIItem);

// Constructor

CYaskawaMPIECDeviceOptions::CYaskawaMPIECDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_ExtendedWrite	= 0;

	m_Endian	= 0;

	m_IXHead	= IEC_BIT_LOBYTE;

	m_QXHead	= IEC_BIT_LOBYTE;

	m_IBHead	= IEC_LO_R_LO;

	m_QBHead	= IEC_LO_R_LO;

	m_QHead		= IEC_HI_R_LO;

	m_Model		= 0;

	SetIECMax();
	}

// UI Management

void CYaskawaMPIECDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	BOOL fChange = FALSE;

	if(
		Tag.IsEmpty()   ||
		Tag == "IXHead" ||
		Tag == "QXHead" ||
		Tag == "QBHead" ||
		Tag == "IBHead" ||
		Tag == "QHead"   ) {

		fChange = TRUE;
		}

	if( fChange ) { 

		SetIECMax();

		pWnd->UpdateUI("IXHead");
		pWnd->UpdateUI("QXHead");
		pWnd->UpdateUI("QBHead");
		pWnd->UpdateUI("IBHead");
		pWnd->UpdateUI("QHead");
		pWnd->EnableUI("QHead", FALSE);
		}

	if( Tag.IsEmpty() || Tag == "Keep" ) {

		pWnd->EnableUI("Time3", !m_Keep);
		}

	CUIItem::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CYaskawaMPIECDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(LOBYTE(m_ExtendedWrite));
	Init.AddByte(LOBYTE(m_Endian));
	Init.AddWord(WORD(m_IXHead));
	Init.AddWord(WORD(m_QXHead));
	Init.AddWord(WORD(m_IBHead));
	Init.AddWord(WORD(m_QBHead));
	Init.AddWord(WORD(m_QHead));
	Init.AddByte(LOBYTE(m_Model));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaMPIECDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(ExtendedWrite);
	Meta_AddInteger(Endian);
	Meta_AddInteger(IXHead);
	Meta_AddInteger(QXHead);
	Meta_AddInteger(IBHead);
	Meta_AddInteger(QHead);
	Meta_AddInteger(QBHead);
	Meta_AddInteger(IXHigh);
	Meta_AddInteger(QXHigh);
	Meta_AddInteger(IBHigh);
	Meta_AddInteger(QHigh);
	Meta_AddInteger(QBHigh);
	Meta_AddInteger(Model);

	SetIECMax();
	}

// Overridables

void CYaskawaMPIECDeviceOptions::SetIECMax(void)
{
	m_IXHigh = 32767; // modbus 00000+

	m_QXHigh = 32767; // modbus 10000+

	m_QBHigh = 32767; // modbus 30000+

	m_IBHigh = 32767; // modbus 40000+

	m_QHigh  = 32767; // modbus 41024+

	m_QHead  = min(32767, m_IBHead + 2048);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaMPIECDriver(void)
{
	return New CYaskawaMPIECDriver;
	}

// Constructor

CYaskawaMPIECDriver::CYaskawaMPIECDriver(void)
{
	m_wID		= 0x353B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= TEXT("Yaskawa");
	
	m_DriverName	= TEXT("TCP/IP MP2000iec");
	
	m_Version	= TEXT("1.00");
	
	m_ShortName	= TEXT("Yaskawa MP2000iec");

	m_DevRoot	= TEXT("MP");

	m_fIsQ		= FALSE;

	AddSpaces();  
	}

// Binding Control

UINT CYaskawaMPIECDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYaskawaMPIECDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYaskawaMPIECDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaMPIECDeviceOptions);
	}

// Address Management

BOOL CYaskawaMPIECDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CYaskawaMPIECAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CYaskawaMPIECDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	switch( Addr.a.m_Table ) {

		case SP_QX:
		case SP_QB:
		case SP_Q:
		case SP_Q32:
		case SP_Q64:
		case SP_1:
		case SP_3:
		case SP_5:
		case SP_6:
				return TRUE;

		}

	return FALSE;
	}

BOOL CYaskawaMPIECDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	GetHeads(pConfig);

	UINT uTable = pSpace->m_uTable;

	if( !IsIEC(uTable) ) {

		BOOL fOk = CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);

		if( fOk ) {

			Addr.a.m_Extra = Is64Bit(uTable) ? 1 : 0;

			if( Addr.a.m_Type == addrWordAsLong ) {

				Addr.a.m_Type = LL;
				}
			}

		return fOk;
		}

	m_fIsQ		= uTable == SP_Q;

	UINT uType	= GetTypeFromModifier(StripType(pSpace, Text));

	UINT IECByte	= tstrtoul(Text, NULL, 10);

	UINT IECBit	= 0;

	BOOL fIsBit	= IsBit(uTable);

	if( fIsBit ) {

		UINT uFind = Text.Find('_');

		if( uFind < NOTHING ) {

			IECBit = min(7, tstrtoul(Text.Mid(uFind+1), NULL, 10));
			}
		}

	Addr.a.m_Table	= uTable;
	Addr.a.m_Extra	= Is64Bit(uTable) ? 1 : IsBit(uTable) ? IECBit : 0;
	Addr.a.m_Type	= uType;
	Addr.a.m_Offset	= IECByte;

	UINT uMin	= GetIECMinByteNumber(uTable);

	UINT uMax	= GetIECMaxByteNumber(Addr);

	CString sErr	= "";

	if( IECByte < uMin || IECByte > uMax ) {

		if( fIsBit ) {

			sErr.Printf(TEXT("%s%d_0 - %s%d_7"),

				pSpace->m_Prefix,
				uMin,
				pSpace->m_Prefix,
				uMax
				);
			}

		else {
			sErr.Printf( TEXT("%s%d - %s%d"),

				pSpace->m_Prefix,
				uMin,
				pSpace->m_Prefix,
				uMax
				);
			}
	
		Error.Set( sErr, 0 );

		return FALSE;
		}

	Addr.a.m_Offset = IECToModbus(Addr, uMin, fIsBit);

	Addr.a.m_Extra  = Is64Bit(Addr.a.m_Table) ? 1 : 0; // clear extra used for bit

	return TRUE;
	}

BOOL CYaskawaMPIECDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	GetHeads(pConfig);

	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uTable = pSpace->m_uTable;

		if( !IsIEC(uTable) ) {

			return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		m_fIsQ       = uTable == SP_Q;

		UINT uOffset = Addr.a.m_Offset;

		UINT uBase   = GetIECMinByteNumber(uTable);

		if( IsBit(uTable) ) {

			Text.Printf(TEXT("%s%d_%1.1d"),

				pSpace->m_Prefix,
				(uOffset / 8) + uBase,
				(uOffset % 8)
				);

			return TRUE;
			}

		uOffset = uBase + (uOffset * 2);

		Text.Printf("%s%d",

			pSpace->m_Prefix,
			uOffset
			);

		if( Addr.a.m_Type != pSpace->m_uType ) {

			Text.Printf("%s.%s",

				Text,
				pSpace->GetTypeModifier(Addr.a.m_Type)
				);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaMPIECDriver::CheckAlignment(CSpace *pSpace)
{
	if( IsBit(pSpace->m_uTable) ) {

		return FALSE;
		}

	if( Is64Bit(pSpace->m_uTable) ) {

		pSpace->m_uAlign = 64;
		}

	return TRUE;
	}

BOOL CYaskawaMPIECDriver::IsBit(UINT uTable)
{
	switch( uTable ) {

		case SP_0:
		case SP_1:
		case SP_IX:
		case SP_QX:
			return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaMPIECDriver::IsIEC(UINT uTable)
{
	return uTable >= SP_IX;
	}

void CYaskawaMPIECDriver::SetHeads(UINT *puIX, UINT *puQX, UINT *puQB, UINT *puIB, UINT *puQ)
{
	m_uIX = *puIX;
	m_uQX = *puQX;
	m_uQB = *puQB;
	m_uIB = *puIB;
	m_uQ  = GetQMax();
	}

DWORD CYaskawaMPIECDriver::ModbusToIEC(UINT uTable, CAddress Addr)
{
	UINT uMin  = GetIECMinByteNumber(uTable);

	UINT uMOff = Addr.a.m_Offset % 10000;

	Addr.a.m_Table = uTable;

	switch( uTable ) {

		case SP_IX:
		case SP_QX:
			Addr.a.m_Offset = uMOff / 8;
			Addr.a.m_Extra  = uMOff % 8;
			break;

		default:
			Addr.a.m_Offset = uMOff * 2;
			Addr.a.m_Extra  = Is64Bit(uTable) ? 1 : 0;
			break;
		}

	Addr.a.m_Offset += uMin;

	return Addr.m_Ref;
	}

void CYaskawaMPIECDriver::SetIsQ(BOOL fSet)
{
	m_fIsQ = fSet;
	}

BOOL CYaskawaMPIECDriver::GetIsQ(void)
{
	return m_fIsQ;
	}

UINT CYaskawaMPIECDriver::GetQMax(void)
{
	return min(32767, m_uIB + 2048);
	}

UINT CYaskawaMPIECDriver::GetQOffset(void)
{
	return 1024;
	}

// Overridables

BOOL CYaskawaMPIECDriver::Is64Bit(UINT uTable)
{
	switch( uTable ) {

		case SP_6:
		case SP_8:
		case SP_Q64:
		case SP_L64:
			return TRUE;
		}

	return FALSE;
	}

UINT CYaskawaMPIECDriver::IECToModbus(CAddress Addr, UINT uMin, BOOL fIsBit)
{
	UINT uOffset	= Addr.a.m_Offset - uMin;

	UINT uExtra	= Addr.a.m_Extra & 7;

	if( fIsBit ) {

		return (uOffset * 8) + uExtra;
		}

	if( Addr.a.m_Type != WW ) {

		uOffset -= uOffset % (uExtra == 1 ? 8 : 4); // align Dbls and Longs
		}

	return uOffset / 2;
	}

UINT CYaskawaMPIECDriver::GetIECMinByteNumber(UINT uTable)
{
	if( !IsIEC(uTable) ) return 0;

	switch( uTable ) {

		case SP_IX:	return m_uIX;
		case SP_QX:	return m_uQX;

		case SP_QB:
		case SP_Q32:
		case SP_Q64:	return m_uQB;

		case SP_IB:	return m_uIB;
		case SP_Q:	return m_uQ;

		case SP_L32:
		case SP_L64:	return m_uIB;
		}

	return 0;
	}

UINT CYaskawaMPIECDriver::GetIECMaxByteNumber(CAddress Addr)
{
	switch( Addr.a.m_Table ) {

		case SP_IX:	// Byte # => 32767
			return m_uIXHigh;

		case SP_QX:	// Byte # => 32767
			return m_uQXHigh;

		case SP_QB:	// Byte # => 332767
			return m_uQBHigh;

		case SP_IB:	// Byte # => 40000-41023
			return m_uIBHigh;

		case SP_Q:	// Byte # => 40000-432767
			return m_uQHigh;

		case SP_Q32:	// Byte # => 332766 (332767)
		case SP_L32:	
			return GetDblMax();

		case SP_Q64:	// Byte # => 332764 (332765, 332766, 332767)
		case SP_L64:
			return GetLongMax();

		case SP_5:
		case SP_7:	return 0xFFFE;

		case SP_6:
		case SP_8:	return 0xFFFC;
		}

	return 0xFFFF; // Modbus maximum
	}

UINT CYaskawaMPIECDriver::GetIBMax(void)
{
	return 41023;
	}

// Implementation

void CYaskawaMPIECDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SP_IX,	"IX",	"Input  Bit/Memory Location",		10,	0,  4095, BB));
	AddSpace(New CSpace(SP_QX,	"QX",	"Output Bit/Memory Location",		10,	0,  4095, BB));
	AddSpace(New CSpace(SP_QB,	"QW",	"Output Word",				10,	0, 32767, WW, LL));
	AddSpace(New CSpace(SP_IB,	"IW",	"Input  Word",				10,	0,  1023, WW, LL));
	AddSpace(New CSpace(SP_Q,	"I",	"Input  Word (Read-Only Section)",	10,  1024, 32767, WW, LL));
	AddSpace(New CSpace(SP_Q32,	"QD",	"Real:  Output Double Word",		10,	0, 32766, RR));
	AddSpace(New CSpace(SP_Q64,	"QL",	"LReal: Output Long Word",		10,	0, 32764, RR));
	AddSpace(New CSpace(SP_L32,	"ID",	"Real:  Input Double Word",		10,	0, 32746, RR));
	AddSpace(New CSpace(SP_L64,	"IL",	"LReal: Input Long Word",		10,	0, 32764, RR));

	AddSpace(New CSpace(SP_0,	"0",	"MB Coils",				10,     0,  0xFFFF, BB));
	AddSpace(New CSpace(SP_1,	"1",	"MB Inputs",				10,     0,  0xFFFF, BB));
	AddSpace(New CSpace(SP_3,	"3",	"MB Input Registers",			10,     0,  0xFFFF, WW, LL));
	AddSpace(New CSpace(SP_4,	"4",	"MB Holding Registers",			10,     0,  0xFFFF, WW, LL));
	AddSpace(New CSpace(SP_5,	"QDM",	"MB Real: Input Register 32 Bit",	10,     0,  0xFFFE, RR));
	AddSpace(New CSpace(SP_6,	"QLM",	"MB Real: Input Register 64 Bit",	10,     0,  0xFFFC, RR));
	AddSpace(New CSpace(SP_7,	"IDM",	"MB Real: Holding Register 32 Bit",	10,     0,  0xFFFE, RR));
	AddSpace(New CSpace(SP_8,	"IDL",	"MB Real: Holding Register 64 Bit",	10,     0,  0xFFFC, RR));
	}

// Overridables

UINT CYaskawaMPIECDriver::GetBitMax(UINT uMin)
{
	return uMin + 511;	// max byte number = 4095 bits / 8
	}

UINT CYaskawaMPIECDriver::GetWordMax(UINT uMin)
{
	return 32766;
	}

// Helpers

UINT CYaskawaMPIECDriver::GetTypeFromModifier(CString Type)
{
	if( Type == "WORD" ) return addrWordAsWord;
	if( Type == "LONG" ) return addrLongAsLong;
	if( Type == "REAL" ) return addrRealAsReal;

	return 0;
	}

void CYaskawaMPIECDriver::GetHeads(CItem *pConfig)
{
	m_uIX = pConfig->GetDataAccess("IXHead")->ReadInteger(pConfig);

	m_uQX = pConfig->GetDataAccess("QXHead")->ReadInteger(pConfig);

	m_uQB = pConfig->GetDataAccess("QBHead")->ReadInteger(pConfig);

	m_uIB = pConfig->GetDataAccess("IBHead")->ReadInteger(pConfig);

	m_uQ  = GetQMax();

	m_uIXHigh = GetBitMax(m_uIX);
	m_uQXHigh = GetBitMax(m_uQX);
	m_uQBHigh = GetWordMax(m_uQB);
	m_uIBHigh = GetWordMax(m_uIB);
	m_uQHigh  = GetWordMax(min(32766, m_uIBHigh + 2048));

	pConfig->GetDataAccess("IXHigh")->WriteInteger(pConfig, m_uIXHigh);

	pConfig->GetDataAccess("QXHigh")->WriteInteger(pConfig, m_uQXHigh);

	pConfig->GetDataAccess("QBHigh")->WriteInteger(pConfig, m_uQBHigh);

	pConfig->GetDataAccess("IBHigh")->WriteInteger(pConfig, m_uIBHigh);

	pConfig->GetDataAccess("QHigh")->WriteInteger(pConfig, m_uQHigh);
	}

UINT CYaskawaMPIECDriver::GetLowWordMax(UINT uMin)
{
	return 32767; // 1023 bytes
	}

UINT CYaskawaMPIECDriver::GetDblMax(void)
{
	return 32764;
	}

UINT CYaskawaMPIECDriver::GetLongMax(void)
{
	return 32760;
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CYaskawaMPIECAddrDialog, CStdAddrDialog);
		
// Constructor

CYaskawaMPIECAddrDialog::CYaskawaMPIECAddrDialog(CYaskawaMPIECDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fUpdateModbus = FALSE;

	SetName(TEXT("YaskawaMPIECElementDlg"));

	m_uIX = pConfig->GetDataAccess("IXHead")->ReadInteger(pConfig);

	m_uQX = pConfig->GetDataAccess("QXHead")->ReadInteger(pConfig);

	m_uQB = pConfig->GetDataAccess("QBHead")->ReadInteger(pConfig);

	m_uIB = pConfig->GetDataAccess("IBHead")->ReadInteger(pConfig);

	m_uQ  = Driver.GetQMax();

	Driver.SetHeads(&m_uIX, &m_uQX, &m_uQB, &m_uIB, &m_uQ);
	}

// Message Map

AfxMessageMap(CYaskawaMPIECAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxDispatchCommand(2008, OnButtonClicked)
	AfxDispatchCommand(2009, OnButtonClicked)

	AfxMessageEnd(CYaskawaMPIECAddrDialog)
	};

// Message Handlers

BOOL CYaskawaMPIECAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	if( !m_fPart ) {

		SetCaption();

		FindSpace();

		LoadList();

		if( m_pSpace ) {

			DoShow(m_pSpace->m_uTable, Addr);

			SetAddressFocus();

			return FALSE;
			}

		else {
			GetDlgItem(5000).SetWindowText(TEXT(""));

			for( UINT ui = 2007; ui <= 2009; ui++ ) {

				GetDlgItem(ui).EnableWindow(FALSE);
				}
			}

		GetDlgItem(2007).SetWindowText(TEXT("0"));

		return TRUE;
		}
	else {
		FindSpace();

		LoadList();

		LoadType();

		DoShow(Addr.a.m_Table, Addr);

		if( m_pSpace ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				GetMPMinimum(m_pSpace->m_uTable, Addr);
				}

			DoShow(Addr.a.m_Table, Addr);
			}

		else {
			GetDlgItem(5000).SetWindowText(TEXT(""));

			for( UINT ui = 2007; ui <= 2009; ui++ ) {

				GetDlgItem(ui).EnableWindow(FALSE);
				}
			}

		SetAddressFocus();

		return FALSE;
		}
	}

void CYaskawaMPIECAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		LoadType();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			if( Addr.a.m_Table == SP_Q ) {

				Addr.a.m_Offset = 0;
				}

			DoShow(m_pSpace->m_uTable, Addr);
			}
		}

	else {
		m_pSpace = NULL;

		ClearType();

		ClearAddress();

		ClearDetails();

		GetDlgItem(2007).SetWindowText(TEXT("0"));

		GetDlgItem(5000).SetWindowText(TEXT(""));

		for( UINT ui = 2007; ui <= 2009; ui++ ) {

			GetDlgItem(ui).EnableWindow(FALSE);
			}
		}
	} 

void CYaskawaMPIECAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		ShowDetails();

		ShowType(GetTypeCode());
		}
	}

// Command Handlers

BOOL CYaskawaMPIECAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text   = m_pSpace->m_Prefix;

		Text += GetAddressText();

		Text += GetTypeText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			if( !m_fUpdateModbus ) {

				EndDialog(TRUE);
				}

			return TRUE;
			}

		if( m_fUpdateModbus ) {

			return FALSE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CYaskawaMPIECAddrDialog::OnButtonClicked(UINT uID)
{
	if( !m_pSpace ) {

		return TRUE;
		}

	if( !IsIEC(m_pSpace->m_uTable) ) {

		return TRUE;
		}

	CAddress Save;

	CAddress Addr	= *m_pAddr;

	if( Addr.m_Ref ) {
		
		Save.m_Ref = Addr.m_Ref;
		}
	else {
		Save.a.m_Table	= m_pSpace->m_uTable;
		Save.a.m_Offset	= m_pSpace->m_uMinimum;
		Save.a.m_Type	= m_pSpace->m_uType;
		Save.a.m_Extra	= Is64Bit(m_pSpace->m_uTable) ? 1 : 0;
		}

	CString Type	= GetDlgItem(2003).GetWindowText();

	UINT uFind	= Type.Find('L');

	Addr.a.m_Table	= m_pSpace->m_uTable;

	Addr.a.m_Type	= uFind < NOTHING ? LL : m_pSpace->m_uType;

	Addr.a.m_Extra	= Is64Bit(Addr.a.m_Table) ? 1 : 0;

	if( uID == 2009 ) { // Update Parameter Address from Modbus

		CAddress A;

//		A.m_Ref = Addr.m_Ref;

		A.m_Ref = GetModbusEntry(Addr, m_pSpace->m_uTable);

		if( m_pSpace->m_uTable == SP_Q ) {

			if( A.a.m_Offset > GetIBMax() ) {
			
				A.a.m_Offset -= GetQOffset();
				}
			}

		Addr.m_Ref = ModbusToIEC(m_pSpace->m_uTable, A);

		ShowParameter(Addr);
		}

	Addr.a.m_Offset	= GetIECEntry(Addr);

	DoShow(Addr.a.m_Table, Addr);

	m_fUpdateModbus = TRUE;

	if( OnOkay(uID) ) {

		Addr = *m_pAddr;
		}

	else {
		Addr.m_Ref = Save.m_Ref;

		DoShow(Addr.a.m_Table, Addr);

		m_fUpdateModbus = TRUE;

		OnOkay(uID);
		}			

	m_fUpdateModbus = FALSE;

	DoShow(Addr.a.m_Table, Addr);

	SetDlgFocus(2002);

	return TRUE;
	}

// Implementation

BOOL CYaskawaMPIECAddrDialog::AllowType(UINT uType)
{
	if( m_pSpace ) {

		switch( uType ) {

			case addrBitAsBit:
			case addrByteAsByte:
			case addrWordAsWord:
			case addrLongAsLong:
			case addrRealAsReal:
				return TRUE;
			}
		}

	return FALSE;
	}

void CYaskawaMPIECAddrDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	UINT uTable = m_pSpace->m_uTable;

	CAddress Addr;

	Addr.a.m_Table  = uTable;
	Addr.a.m_Extra  = Is64Bit(uTable) ? 1 : 0;
	Addr.a.m_Type   = GetTypeCode();

	if( IsIEC(uTable) ) {

		Min = ExpandIECAddress(Addr, FALSE);
		Max = ExpandIECAddress(Addr, TRUE);
		}
		
	else {
		if( !m_pSpace->IsNamed() ) {

			m_pSpace->GetMinimum(Addr);

			m_pDriver->ExpandAddress(Min, m_pConfig, Addr);

			m_pSpace->GetMaximum(Addr);

			m_pDriver->ExpandAddress(Max, m_pConfig, Addr);
			}
		}

	if( !m_pSpace->IsNamed() ) {
		
		Rad = m_pSpace->GetRadixAsText();
		}

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

// Overridables

void CYaskawaMPIECAddrDialog::ShowModbus(CAddress Addr)
{
	UINT uOffset = Addr.a.m_Offset;

	if( IsIEC(Addr.a.m_Table) ) {
		
		uOffset %= 32768;
		}

	CString s1;

	switch( Addr.a.m_Table ) {

		case SP_0:
		case SP_IX:	s1.Printf(TEXT("0"));			break;

		case SP_1:
		case SP_QX:	s1.Printf(TEXT("1"));			break;

		case SP_3:
		case SP_5:
		case SP_6:
		case SP_QB:
		case SP_Q32:
		case SP_Q64:	s1.Printf(TEXT("3"));			break;

		case SP_4:
		case SP_7:
		case SP_8:
		case SP_Q:
		case SP_IB:
		case SP_L32:
		case SP_L64:
				if( Addr.a.m_Table == SP_Q ) {
					
					uOffset += GetQOffset();
					}

				// Fall thru

		default:	s1.Printf(TEXT("4"));	break;
		}

	CString s;

	s.Printf(TEXT("%s%5.5d"), s1, uOffset);

	GetDlgItem(2007).SetWindowText(s);
	}

void CYaskawaMPIECAddrDialog::ShowInfo(UINT uTable)
{
	CString sInfoM = TEXT("");

	if( !IsIEC(uTable) ) {

		if( uTable ) {

			sInfoM = MBMBSEL;
			}
		}

	else {
		switch( uTable ) {

			case SP_IX:
				sInfoM = MBMSGB0;
				break;

			case SP_QX:
				sInfoM = MBMSGB1;
				break;

			case SP_QB:
				sInfoM = MBMSGQB;
				break;

			case SP_IB:
				sInfoM = MBMSGIB;
				break;

			case SP_Q:
				sInfoM = MBMSGQ;
				break;

			case SP_Q32:
				sInfoM = MBMSGQ4;
				break;

			case SP_Q64:
				sInfoM = MBMSGQ8;
				break;

			case SP_L32:
				sInfoM = MBMSGA4;
				break;

			case SP_L64:
				sInfoM = MBMSGA8;
				break;
			}
		}

	GetDlgItem(5000).SetWindowText(sInfoM);
	}

// Helpers

void CYaskawaMPIECAddrDialog::DoShow(UINT uTable, CAddress Addr)
{
	GetDlgItem(2002).EnableWindow(TRUE);

	CStdAddrDialog::ShowAddress(Addr);

	ShowDetails();

	ShowModbus(Addr);

	ShowInfo(uTable);

	BOOL fIsIEC = IsIEC(uTable);

	SetIsQ(fIsIEC ? uTable == SP_Q : (Addr.a.m_Offset % 10000) > 1023);

	for( UINT ui = 2007; ui <= 2009; ui++ ) {

		GetDlgItem(ui).EnableWindow(fIsIEC);
		}
	}

CString CYaskawaMPIECAddrDialog::ExpandIECAddress(CAddress Addr, BOOL fIsMax)
{
	UINT uVal = fIsMax ? GetIECMaxByteNumber(Addr) : GetIECMinByteNumber(Addr.a.m_Table);

	CString s;

	CString sPre = m_pSpace->m_Prefix;

	if( IsBit(Addr.a.m_Table) ) {
		
		s.Printf("%s%5.5d_%1.1d", sPre, uVal, fIsMax ? 7 : 0);
		}
	else {
		s.Printf("%s%5.5d", sPre, uVal);
		}

	return s;
	}

void CYaskawaMPIECAddrDialog::ShowParameter(CAddress Addr)
{
	CString s = TEXT("");

	switch( Addr.a.m_Table ) {

		case SP_IX:
		case SP_QX:
			s.Printf(TEXT("%d_%1.1d"),

				Addr.a.m_Offset,
				Addr.a.m_Extra & 7
				);
			break;

		default:
			s.Printf(TEXT("%d"), Addr.a.m_Offset);
			break;
		}

	GetDlgItem(2002).SetWindowText(s);
	}

UINT CYaskawaMPIECAddrDialog::GetIECEntry(CAddress Addr)
{
	CString s   = GetDlgItem(2002).GetWindowText();

	UINT ut     = tatoi(s);

	UINT uTable = Addr.a.m_Table;

	UINT uMin   = GetIECMinByteNumber(uTable);

	if( ut < uMin ) ut = uMin;

	Addr.a.m_Offset = ut;

	UINT ubit   = 0;

	BOOL fIsBit = IsBit(uTable);

	if( fIsBit ) {

		UINT uFind  = s.Find('_');

		if( uFind < NOTHING ) {

			ubit = min(7, tatoi(s.Mid(uFind+1)));
			}
		}

	Addr.a.m_Extra = fIsBit ? ubit : Is64Bit(uTable) ? 1 : 0;

	return IECToModbus(Addr, uMin, fIsBit);
	}

DWORD CYaskawaMPIECAddrDialog::GetModbusEntry(CAddress Addr, UINT uIECTable)
{
	CString s = GetDlgItem(2007).GetWindowText();

	UINT ut = (UINT)tatoi(s.Left(1));

	UINT uo = (UINT)tatoi(s.Mid(1));

	Addr.a.m_Table = SP_4;

	switch( ut ) {

		case 0:	Addr.a.m_Table = SP_0;	break;
		case 1: Addr.a.m_Table = SP_1;	break;
		case 3: Addr.a.m_Table = SP_3;	break;
		}

	ut = (ut * 10000) + uo;

	if( uIECTable == SP_Q ) {

		ut = max( ut, GetIBMax() + 1 );
		}
		
	Addr.a.m_Offset = ut;

	Addr.m_Ref      = NormalizeMDB(Addr);

	SetIsQ(uIECTable != SP_IB && Addr.a.m_Offset > GetIBMax());

	return Addr.m_Ref;
	}

DWORD CYaskawaMPIECAddrDialog::NormalizeMDB(CAddress Addr)
{
	switch( Addr.a.m_Type ) {

		case LL:
			Addr.a.m_Offset %= 0xFFFE;
			break;

		case RR:
			Addr.a.m_Offset &= Addr.a.m_Extra ? 0xFFFC : 0xFFFE;
			break;
		}

	return Addr.m_Ref;
	}

void CYaskawaMPIECAddrDialog::GetMPMinimum(UINT uTable, CAddress &Addr)
{
	Addr.a.m_Table	= uTable;

	Addr.a.m_Extra	= Is64Bit(uTable) ? 1 : 0;

	Addr.a.m_Offset	= GetIECMinByteNumber(uTable);
	}

//

BOOL CYaskawaMPIECAddrDialog::IsBit(UINT uTable)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->IsBit(uTable);
	}

BOOL CYaskawaMPIECAddrDialog::Is64Bit(UINT uTable)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->Is64Bit(uTable);
	}

BOOL CYaskawaMPIECAddrDialog::IsIEC(UINT uTable)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->IsIEC(uTable);
	}

UINT CYaskawaMPIECAddrDialog::IECToModbus(CAddress Addr, UINT uMin, BOOL fIsBit)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->IECToModbus(Addr, uMin, fIsBit);
	}

DWORD CYaskawaMPIECAddrDialog::ModbusToIEC(UINT uTable, CAddress Addr)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->ModbusToIEC(uTable, Addr);
	}

UINT CYaskawaMPIECAddrDialog::GetIECMinByteNumber(UINT uTable)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->GetIECMinByteNumber(uTable);
	}

UINT CYaskawaMPIECAddrDialog::GetIECMaxByteNumber(CAddress Addr)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->GetIECMaxByteNumber(Addr);
	}

void CYaskawaMPIECAddrDialog::SetIsQ(BOOL fSet)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->SetIsQ(fSet);
	}

UINT CYaskawaMPIECAddrDialog::GetIBMax(void)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->GetIBMax();
	}

UINT CYaskawaMPIECAddrDialog::GetQOffset(void)
{
	CYaskawaMPIECDriver * pDriver = (CYaskawaMPIECDriver *) m_pDriver;

	return pDriver->GetQOffset();
	}

// End of File
