
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KEBSLAVE_HPP
	
#define	INCLUDE_KEBSLAVE_HPP

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Driver Options
//

class CKEBSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKEBSlaveDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Device Options
//

class CKEBSlaveDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKEBSlaveDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Model;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Driver
//

class CKEBSlaveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKEBSlaveDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control

		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);

		// Helper
		UINT ToHex(CString Text, UINT uMaxCt);
	};

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Address Selection
//

class CKEBSlaveDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CKEBSlaveDialog(CKEBSlaveDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

		// Message Map
		AfxDeclareMessageMap();
		                
	protected:
		// Overridables
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowAddress(CAddress Addr);
		void	ShowDetails(void);

		// Helper
		BOOL	IsValidSetChar(CString s);
	};

// End of File

#endif
