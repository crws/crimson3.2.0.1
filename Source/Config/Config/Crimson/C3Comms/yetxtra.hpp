
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YETXTRA_HPP

#define	INCLUDE_YETXTRA_HPP

#define	AN	addrNamed
#define BB	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Valid Function Code Definitions
#define	FA		0xA000 // Immediate Commands
#define	FB		0xA001 // Sequential Commands
#define	FZ		0xA010 // Header

#define	SPIMMEDIATE	0xE000
#define	SPSEQUENTIAL	0xE001

#define	SQ		0x2000 // Offset for Sequential Opcodes

#define	CP1		0x100 // Immediate Cache parameter 1
#define	CP2		0x200 // Immediate Cache parameter 2
#define	CP3		0x300 // Immediate Cache parameter 3
#define	CP4		0x400 // Immediate Cache parameter 4
#define	CP5		0x500 // Immediate Cache parameter 5

#define	CP1S		0x100+SQ // Sequential Cache parameter 1
#define	CP2S		0x200+SQ // Sequential Cache parameter 2
#define	CP3S		0x300+SQ // Sequential Cache parameter 3
#define	CP4S		0x400+SQ // Sequential Cache parameter 4
#define	CP5S		0x500+SQ // Sequential Cache parameter 5
	
//////////////////////////////////////////////////////////////////////////
//
// YET Xtra Drive Master Device Options
//

class CYETXtraDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYETXtraDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Axis;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Xtra Drive Master Driver
//

class CYETXtraDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYETXtraDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		CSpace * GetSpace(CAddress const &Addr);
		BOOL MatchSpace(CSpace *pSpace, CAddress const &Addr);

	protected:
		// Data
		UINT	m_Axis;

		// Implementation
		void	AddSpaces(void);

		// Friend
		friend class CYETXtraAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Xtra Drive Address Selection
//

class CYETXtraAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CYETXtraAddrDialog(CYETXtraDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Data Members
		UINT	m_uAllowSpace;
		BOOL	m_fChecked;
		CYETXtraDriver * m_pYETDriver;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overridables
		void	ShowAddress(CAddress Addr);

		// Helpers
		BOOL	AllowSpace(CSpace *pSpace);
		BOOL	SelectList(CSpace *pSpace, UINT uCommand, UINT uAllow);
		void	SetAllow(void);
		UINT	IsHeader(UINT uCommand);
		void	UpdateMode(void);
		void	UpdateInfo(void);
		CString	SetInfoText(UINT uSel, UINT uPar);
	};

// End of File

#endif
