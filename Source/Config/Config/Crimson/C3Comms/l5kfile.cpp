
#include "intern.hpp"

#include "l5keip.hpp"

#include "l5kfile.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File
//

// Constructor

CABL5kFile::CABL5kFile(void)
{
	m_pStream = NULL;
	}

// Destructor

CABL5kFile::~CABL5kFile(void)
{
	m_pStream = NULL;
	}

// Control
 
BOOL CABL5kFile::OpenLoad(ITextStream &Stream)
{
	if( !m_pStream ) {

		m_pStream = &Stream;

		m_uState  = compHeader;

		m_Data.Empty();

		ReadLine(TRUE);

		if( !m_Controller.m_Name.IsEmpty() ) {

			return TRUE;
			}

		CloseFile();		
		}

	return FALSE;
	}

// Implementation

void CABL5kFile::CloseFile(void)
{
	m_pStream = NULL;
	}

BOOL CABL5kFile::CheckEndOfLine(void)
{
	if( m_Line.Find("\r\n") == NOTHING ) {
		
		CString More = m_Line;

		do {
			ReadLine(FALSE, FALSE);
			
			m_Line = More + m_Line;
			
			} while( m_Line.Find("\r\n") == NOTHING );
		}

	return TRUE;
	}

void CABL5kFile::ReadLine(BOOL fParse, BOOL fEndOfLine)
{
	m_Line.Expand(512);

	if( m_pStream->GetLine(PTXT(PCTXT(m_Line)), 512) ) {
	
		m_Line.FixLength();

		if( fEndOfLine && CheckEndOfLine() ) {

			m_Line.TrimBoth();

			if( m_Line.IsEmpty() ) {
		
				ReadLine(fParse);

				return;
				}

			if( fParse ) {

				if( m_uState == compRoutine ) {
					
					SkipQuotedText();
					
					ParseData();
					}
				else {
					SkipComment();

					SkipAttribute();

					SkipInitData();

					ParseData();
					}
				}
			}
		}
	}

// Alternative Parser

void CABL5kFile::SkipComment(void)
{
	PCTXT pToken[] = { "(*", "*)" };

	UINT p1;

	if( (p1 = m_Line.Find(pToken[0])) < NOTHING ) {

		m_Data += m_Line.Left(p1);
		
		m_Line  = m_Line.Mid(p1 + strlen(pToken[0]));

		while( (p1 = m_Line.Find(pToken[1])) == NOTHING ) {
			
			ReadLine(FALSE);
			}

		m_Line  = m_Line.Mid(p1 + strlen(pToken[1]));

		SkipComment();
		}
	}

void CABL5kFile::SkipAttribute(void)
{
	PCTXT pToken[] = { "(", ")" };

	UINT p1;

	if( (p1 = m_Line.Find(pToken[0])) < NOTHING ) {

		m_Data += m_Line.Left(p1);
		
		m_Line  = m_Line.Mid(p1 + strlen(pToken[0]));

		SkipQuotedText();
		
		while( (p1 = m_Line.Find(pToken[1])) == NOTHING ) {
			
			ReadLine(FALSE);

			SkipQuotedText();
			}

		m_Line = m_Line.Mid(p1 + strlen(pToken[1]));
		
		SkipAttribute();
		}
	}

void CABL5kFile::SkipQuotedText(void)
{
	UINT p1, p2;

	if( (p1 = m_Line.Find('"')) < NOTHING ) {

		if( (p2 = m_Line.Find('"', p1 + 1)) < NOTHING ) {

			m_Line = m_Line.Left(p1) + m_Line.Mid(p2 + 1);
			}
		else {
			m_Line = m_Line.Mid(p1);
			}
		}
	}

void CABL5kFile::SkipInitData(void)
{
	PCTXT pToken[] = { "[", "]" };

	UINT p1;

	if( (p1 = m_Line.Find(":=")) < NOTHING ) {

		m_Data += m_Line.Left(p1);

		m_Line  = m_Line.Mid(p1);

		if( (p1 = m_Line.Find(pToken[0])) < NOTHING ) {

			m_Data += m_Line.Left(p1);

			m_Line  = m_Line.Mid(p1 + strlen(pToken[0]));

			for( UINT uSkip = 1, n = 0; uSkip; ) {

				char cData = m_Line[n ++];

				if( strchr(pToken[0], cData) ) {

					uSkip ++;
					}

				if( strchr(pToken[1], cData) ) {

					uSkip --;
					}

				if( n >= m_Line.GetLength() ) {

					n = 0;
					
					ReadLine(FALSE);
					}
				}

			m_Line  = m_Line.Mid(n);

			m_Data += m_Line;
			}
		else {
			m_Data += m_Line;
			}
		}
	}

BOOL CABL5kFile::ParseHeader(void)
{
	if( m_uState == compHeader ) {

		if( m_Data.Find("IE_VER") < NOTHING ) {

			CString Version = m_Data.Mid(m_Data.Find(":="));

			Version.TrimBoth();
			}
		else {
			CStringArray List;

			m_Data.Tokenize(List, ' ');

			if( List.GetCount() >= 2 ) {

				CString Comp = List[0];

				CString Name = List[1];

				if( Comp == "CONTROLLER" ) {

					m_Controller.SetName(Name);
					
					m_uState = compController;
					}
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseController(void)
{
	if( m_uState == compController ) {

		if( m_Data == "END_CONTROLLER" ) {

			m_uState = compEndOfParse;
			}
		else {
			m_Data.TrimRight();

			CStringArray List;

			m_Data.Tokenize(List, ' ');

			if( List.GetCount() >= 2 ) {

				CString Comp = List[0];

				CString Name = List[1];

				if( Comp == "DATATYPE" ) {

					m_pType = New CABL5kDataType;

					m_pType->SetName(Name);

					m_Controller.AddDataType(m_pType);

					m_uState = compDataType;
					}

				if( Comp == "MODULE" ) {

					m_uState = compModule;
					}

				if( Comp == "ADD_ON_INSTRUCTION_DEFINITION" ) {

					m_pType = New CABL5kDataType;

					m_pType->SetName(Name);

					m_Controller.AddDataType(m_pType);

					m_uState = compAddOn;
					}

				if( Comp == "PROGRAM" ) {

					m_uState = compProgram;
					}
				}
			else {
				if( List.GetCount() == 1 ) {
					
					CString Comp = List[0];

					if( Comp == "TAG" ) {
						
						m_uState = compTag;
						}
					}
				else	
				    	AfxAssert(FALSE);
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseDataType(void)
{
	if( m_uState == compDataType ) {

		if( m_Data == "END_DATATYPE" ) {

			m_pType  = NULL;
			
			m_uState = compController;			
			}
		else {
			m_Data.Remove(';');

			CStringArray List;

			m_Data.Tokenize(List, ' ');

			if( List.GetCount() >= 2 ) {

				CString Name = List[1];

				CString Type = List[0];

				CABTypeDef Member;

				Member.m_Name   = Name;

				Member.m_Type   = Type;

				Member.m_fAlias = FALSE;
				
				m_pType->AddMember(Member);
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseModule(void)
{
	if( m_uState == compModule ) {

		if( m_Data == "END_MODULE" ) {
			
			m_uState = compController;			
			}
		else {
			CStringArray List;

			m_Data.Tokenize(List, ' ');

			if( List.GetCount() >= 2 ) {

				CString Comp = List[0];

				CString Name = List[1];
				
				if( Comp == "CONNECTION" ) {

					m_uState = compConnection;
					}
				}			
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseConnection(void)
{
	if( m_uState == compConnection ) {

		if( m_Data == "END_CONNECTION" ) {

			m_uState = compModule;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseAddOn(void)
{
	if( m_uState == compAddOn ) {

		if( m_Data == "END_ADD_ON_INSTRUCTION_DEFINITION" ) {

			m_uState = compController;
			}
		else {
			CStringArray List;

			m_Data.Tokenize(List, ' ');

			AfxAssert(List.GetCount());

			if( List.GetCount() > 1 ) {
				
				CString Comp = List[0];

				CString Name = List[1];

				if( Comp == "ROUTINE" ) {
					
					m_uState = compRoutine;
					}
				}
			else {				
				CString Comp = List[0];

				if( Comp == "PARAMETERS" ) {
					
					m_uState = compParameters;
					}
				
				if( Comp == "LOCAL_TAGS" ) {
					
					m_uState = compLocalTags;
					}
				
				if( Comp == "ROUTINE" ) {
					
					m_uState = compRoutine;
					}
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseParameters(void)
{
	if( m_uState == compParameters ) {

		if( m_Data == "END_PARAMETERS" ) {

			m_pType  = NULL;
			
			m_uState = compAddOn;
			}
		else {
			if( m_pType ) {

				m_Data.Remove(';');

				CStringArray List;

				m_Data.Tokenize(List, ':');

				if( List.GetCount() >= 2 ) {

					CString Name = List[0];

					CString Type = List[1];

					Name.TrimBoth();

					Type.TrimBoth();

					CABTypeDef Member;

					Member.m_Name   = Name;

					Member.m_Type   = Type;

					Member.m_fAlias = FALSE;
					
					m_pType->AddMember(Member);
					}
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseLocalTags(void)
{
	if( m_uState == compLocalTags ) {

		if( m_Data == "END_LOCAL_TAGS" ) {

			m_uState = compAddOn;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseRoutine(void)
{
	if( m_uState == compRoutine ) {

		if( m_Data == "END_ROUTINE" ) {

			m_uState = compAddOn;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseTag(void)
{
	if( m_uState == compTag ) {

		if( m_Data == "END_TAG" ) {

			//m_uState = compController;

			m_uState = compEndOfParse;
			}
		else {
			m_Data.Remove(';');

			m_Data.Replace(":=", " ");

			m_Data.TrimRight();

			CStringArray List;

			m_Data.Tokenize(List, ' ');

			if( List.GetCount() > 2 ) {

				CString Name = List[0];

				CString Type = List[2];

				if( List[1] == ":" ) {

					m_Controller.AddTag(Name, Type);
					}

				if( List[1] == "OF" ) {

					m_Controller.AddAliasTag(Name, Type);
					}
				}
			else
				AfxAssert(FALSE);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseProgram(void)
{
	if( m_uState == compProgram ) {

		if( m_Data == "END_PROGRAM" ) {

			m_uState = compController;
			}
		else {
			m_Data.TrimRight();

			CStringArray List;

			m_Data.Tokenize(List, ' ');

			if( List.GetCount() >= 2 ) {

				CString Comp = List[0];

				CString Name = List[1];

				if( Comp == "TAG" ) {
					
					m_uState = compTag;
					}

				if( Comp == "ROUTINE" ) {
					
					m_uState = compRoutine;
					}

				if( Comp == "FBD_ROUTINE" ) {
					
					m_uState = compFBDRoutine;
					}

				if( Comp == "ST_ROUTINE" ) {
					
					m_uState = compSTRoutine;
					}

				if( Comp == "SFC_ROUTINE" ) {
					
					m_uState = compSFCRoutine;
					}
				}
			}

		return TRUE;
		}

	return FALSE;
	}

void CABL5kFile::ParseData(void)
{
	if( m_Data.IsEmpty() ) {

		m_Data = m_Line;
		}

	if( m_Data.IsEmpty() ) {
		
		ReadLine(TRUE);
		}

	ParseHeader()     ||
	ParseController() ||
	ParseDataType()   ||
	ParseModule()     ||
	ParseConnection() ||
	ParseAddOn()      ||
	ParseParameters() ||
	ParseLocalTags()  ||
	ParseRoutine()    ||
	ParseProgram()    ||
	ParseTag()        ;;

	if( m_uState != compEndOfParse ) {

		m_Data.Empty();

		ReadLine(TRUE);
		}
	}


//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Component 
//

// Constructor

CABL5kComponent::CABL5kComponent(void)
{
	m_fAlias = FALSE;
	}

// Operations

void CABL5kComponent::SetName(PCTXT pName)
{
	m_Name = pName;
	}

// Management

void CABL5kComponent::AddMember(CABTypeDef Member)
{
	m_Members.Append(Member);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Data Type Component
//

// Constructor

CABL5kDataType::CABL5kDataType(void)
{
	m_Type = "DATATYPE";
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Tag Component
//

// Constructor

CABL5kTag::CABL5kTag(void)
{
	m_Type = "TAG";
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File Controller Component
//

// Constructor

CABL5kController::CABL5kController(void)
{
	m_Type = "CONTROLLER";
	}

// Destructor

CABL5kController::~CABL5kController(void)
{
	INDEX n = m_Types.GetHead();

	while( !m_Types.Failed(n) ) {

		delete m_Types[n];
		
		m_Types.GetNext(n);
		}

	m_Types.Empty();
	}

// Management

void CABL5kController::AddDataType(CABL5kComponent *pType)
{
	m_Types.Append(pType);
	}

void CABL5kController::AddTag(CString Name, CString Type)
{
	CABTypeDef Member;

	Member.m_Name   = Name;

	Member.m_Type   = Type;

	Member.m_fAlias = FALSE;
	
	m_Tags.AddMember(Member);
	}

void CABL5kController::AddAliasTag(CString Name, CString Type)
{
	CABTypeDef Member;

	Member.m_Name   = Name;

	Member.m_Type   = Type;

	Member.m_fAlias = TRUE;

	m_Tags.AddMember(Member);
	}

// End of File
