
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_3PSNET_HPP
	
#define	INCLUDE_3PSNET_HPP

/////////////////////////////////////////////////////////////////////////
//
// 3psNet CAN Bus Enumerations
//

enum DevMessType {

	messGetConfigRevision	= 0x0000,
	messSetShuntCal		= 0x0001,
	messSetRawData		= 0x0002,
	messSetCapacity		= 0x0003,
	messGetCapacity		= 0x0004,
	messSetSpan0		= 0x0007,
	messGetSpan0		= 0x0008,
	messSetSpan1		= 0x0009,
	messGetSpan1		= 0x000A,
	messSetZero0		= 0x000B,
	messGetZero0		= 0x000C,
	messSetZero1		= 0x000D,
	messGetZero1		= 0x000E,
	messSetTare0		= 0x000F,
	messGetTare0		= 0x0010,
	messSetTare1		= 0x0011,
	messGetTare1		= 0x0012,
	messSetCold0		= 0x0013,
	messGetCold0		= 0x0014,
	messSetRoom0		= 0x0015,
	messGetRoom0		= 0x0016,
	messSetHot0		= 0x0017,
	messGetHot0		= 0x0018,
	messSetCold1		= 0x0019,
	messGetCold1		= 0x001A,
	messSetRoom1		= 0x001B,
	messGetRoom1		= 0x001C,
	messSetHot1		= 0x001D,
	messGetHot1		= 0x001E,
	messSetTempPoint0	= 0x001F,
	messGetTempPoint0	= 0x0020,
	messSetTempPoint1	= 0x0021,
	messGetTempPoint1	= 0x0022,
	messSetFilterThreshold  = 0x0026,
	messGetFilterThreshold  = 0x0027,
	messSetFilterConstant   = 0x0028,
	messGetFilterConstant   = 0x0029,
	messIntRelayValue	= 0x07EF,
	messSetRelayValue	= 0x0702,
	messSetTXID		= 0x0A02,
	messGetTXID		= 0x0A03,
	messSetPowerDown	= 0x0A04,

	messBroadcastStatus	= 0xFF01,
	messBroadcastBattery	= 0xFF02,
	messBroadcastTemp	= 0xFF03,
	messBroadcastValue	= 0xFF04,

	messZeroAngle		= 0x0602,
	messResetLength		= 0x1A05,

	};

enum DevType {

	devLoadSensHard		= 2,
	devLoadSensWireless	= 3,
	devA2BSensorHard	= 4,
	devA2BSensorWireless	= 5,
	devAngleSensorHardBoom  = 6,
	devRelayOutputHard4	= 7,
	devWindSpeedWireless	= 8,
	devPayoutHard		= 9,
	devRemoteAntenna	= 10,
	devSpeedHard		= 11,
	devAnalogOuputHard1	= 13,
	devAnalogInput		= 12,
	devDualAngleWireless	= 14,
	devSlewSensorHard	= 15,
	devDualAngleSensorHard	= 16,
	devPayoutWireless	= 17,
	devPayoutSpeedWireless	= 18,
	devTorqueWireless	= 19,
	devTorqueHookWireless	= 20,
	devPressureSensor	= 21,
	devSwitchInputsHard8    = 35,
	devBoomLengthHard	= 26,
	devCustom		= 255,
	};

enum DevPos {

	posLoadSensHard		= 0,
	posLoadSensWireless	= 1,
	posA2BSensorHard	= 2,
	posA2BSensorWireless	= 3,
	posAngleSensorHardBoom  = 4,
	posRelayOutputHard4	= 5,
	posWindSpeedWireless	= 6,
	posPayoutHard		= 7,
	posRemoteAntenna	= 8,
	posSpeedHard		= 9,
	posAnalogOuputHard1	= 10,
	posAnalogInput		= 11,
	posSwitchInputsHard8    = 12,
	posSlewSensorHard	= 13,
	posDualAngleSensorHard	= 14,
	posPayoutWireless	= 15,
	posPayoutSpeedWireless	= 16,
	posTorqueWireless	= 17,
	posTorqueHookWireless	= 18,
	posPressureSensor	= 19,
	posDualAngleWireless	= 20,
	posBoomLengthHard	= 21,
	posCustom		= 255,
	};



/////////////////////////////////////////////////////////////////////////
//
// 3psNet CAN Bus Driver Options
//

class C3psNetDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		C3psNetDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Source;
	
			
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};




/////////////////////////////////////////////////////////////////////////
//
// 3psNet CAN Bus Device Options
//

class C3psNetDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		C3psNetDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_ID;
		UINT m_Custom;
		UINT m_Wireless;
		UINT m_Interval;
		UINT m_Transact;

		// Access
		BYTE GetDeviceType(void);
		BOOL IsCustom(void);

	protected:

		UINT m_DeviceType;
	
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL IsWireless(void);
				
	};



//////////////////////////////////////////////////////////////////////////
//
// 3psNet CAN Bus Driver
//

class C3psNetDriver : public CStdCommsDriver
{
	public:
		// Constructor
		C3psNetDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

	protected:
		// Data Members
		UINT m_uMsgs;

		// Implementation
		void AddSpaces(void);
		void AddSpaces(UINT uDeviceType, BOOL fCustom);
		void AddMsg(UINT uSpace);
	};

// End of File

#endif
