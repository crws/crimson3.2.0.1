
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YASKMP_HPP
	
#define	INCLUDE_YASKMP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Series Master Driver Options
//

class CYaskawaMPMasterDeviceOptions : public CStdDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaMPMasterDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Memobus TCP/IP Master Device Options
//

class CYaskawaMPTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaMPTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Master Serial Driver
//
		 
class CYaskawaMPMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaMPMasterDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		
		BOOL	HasHexBits(CSpace *pSpace);
		UINT	FindHexBit(CString &Text, CSpace *pSpace);
		CString FindHexChar(CString &Text, CSpace *pSpace);
				
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Memobus TCP/IP Master Driver
//

class CYaskawaMemobusTCPDriver : public CYaskawaMPMasterDriver
{
	public:
		// Constructor
		CYaskawaMemobusTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Address Selection
//

class CYaskawaMPDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CYaskawaMPDialog(CStdCommsDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};


// End of File

#endif
