
#include "intern.hpp"

#include "kebdin2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// KEBDIN2 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKEBDIN2DeviceOptions, CUIItem);

// Constructor

CKEBDIN2DeviceOptions::CKEBDIN2DeviceOptions(void)
{
	m_Drop 	= 1;
	}

// Download Support	     

BOOL CKEBDIN2DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CKEBDIN2DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	} 

//////////////////////////////////////////////////////////////////////////
//
// KEBDIN2 Master Serial Driver
//

// Instantiator

ICommsDriver * Create_KEBDIN2Driver(void)
{
	return New CKEBDIN2Driver;
	}

// Constructor

CKEBDIN2Driver::CKEBDIN2Driver(void)
{
	m_wID		= 0x339B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "KEB";
	
	m_DriverName	= "DIN66019II";
	
	m_Version	= "1.01";
	
	m_ShortName	= "DIN66019II";	

	AddSpaces();
	}

// Destructor

CKEBDIN2Driver::~CKEBDIN2Driver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CKEBDIN2Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CKEBDIN2Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CKEBDIN2Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKEBDIN2DeviceOptions);
	}

// Implementation     

void CKEBDIN2Driver::AddSpaces(void)
{
//*!*/ Comment format indicates Write operations that generated "Service Not Available" when tested.
//*!*/ Tested code for these remains in runtime.  The functions can be enabled here and in the runtime
//*!*/ by reversing the formats //*!*/ and /*!*/.
	AddSpace(New CSpace(AN, "S0A",  "Parameter : Set",			16,  1,	0,	addrLongAsLong));

	AddSpace(New CSpace( 1, "R2A",  "Parameter Lower Limit",		16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace( 2, "R2B",  "Parameter Higher Limit",		16,  1,	0x7FFF,	addrLongAsLong));
//*!*/	AddSpace(New CSpace( 5, "S2W",  "Send W2A+W2B <Data = Parameter>",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace( 3, "W2A",  "   Parameter Lower Limit to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace( 4, "W2B",  "   Parameter Higher Limit to Send",	16,  0,	0,	addrLongAsLong));

	AddSpace(New CSpace( 6, "R3A",  "Parameter Default Value",		16,  1,	0x7FFF,	addrLongAsLong));

	AddSpace(New CSpace( 7, "R4A",  "Parameter Characteristics 1",		16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace( 8, "R4B",  "Parameter Characteristics 2",		16,  1,	0x7FFF,	addrLongAsLong));
//*!*/	AddSpace(New CSpace( 9, "S4W",  "Send W4A+W4B <Data = Parameter>",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(20, "W4A",  "   Parameter Characteristics 1 to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(21, "W4B",  "   Parameter Characteristics 2 to Send",	16,  0,	0,	addrLongAsLong));

	AddSpace(New CSpace(10, "R5A",  "Scaling Divisor",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(11, "R5B",  "Scaling Multiplier",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(12, "R5C",  "Scaling Offset",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(13, "R5D",  "Scaling Flags",			16,  1,	0x7FFF,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(14, "S5W",  "Send W5A...W5D <Data = Parameter>",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(22, "W5A",  "   Scaling Divisor to Send",		16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(23, "W5B",  "   Scaling Multiplier to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(24, "W5C",  "   Scaling Offset to Send",		16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(25, "W5D",  "   Scaling Flags to Send",		16,  0,	0,	addrLongAsLong));

	AddSpace(New CSpace(28, "CM6",  "Parameter Name",			16,  1,	0x7FFF,	addrLongAsLong));

	AddSpace(New CSpace(37, "R7A",  "Text Index Display",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(38, "R7B",  "Text Number Display",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(39, "R7C",  "Text Index Combivis",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(40, "R7D",  "Text Number Combivis",			16,  1,	0x7FFF,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(45, "S7W",  "Send W7A...W7D <Data = Parameter>",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(41, "W7A",  "   Text Index Display to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(42, "W7B",  "   Text Number Display to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(43, "W7C",  "   Text Index Combivis to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(44, "W7D",  "   Text Number Combivis to Send",	16,  0,	0,	addrLongAsLong));

	AddSpace(New CSpace(46, "R10A", "Group Index",				16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(47, "R10B", "Group Code",				16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(48, "R10C", "Group Characteristic",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(49, "R10D", "Group High Parameter",			16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(50, "R10E", "Group Number of Parameters",		16,  1,	0x7FFF,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(56, "S10W", "Send W10A...W10E <Data = Parameter>",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(51, "W10A", "   Group Index to Send",		16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(52, "W10B", "   Group Code to Send",		16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(53, "W10C", "   Group Characteristic to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(54, "W10D", "   Group High Parameter to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(55, "W10E", "   Group Number of Parameters to Send",16,  0,	0,	addrLongAsLong));

	AddSpace(New CSpace(57, "CM11", "Group Name",				16,  1,	0x7FFF,	addrLongAsLong));

	AddSpace(New CSpace(83, "R12B", "Text Bitmask\t(Initialize W12A)",	16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(84, "R12C", "Text Number\t(Initialize W12A)",	16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(85, "R12D", "Text\t(Initialize W12A)",		16,  1,	0x7FFF,	addrLongAsLong));
	AddSpace(New CSpace(89, "W12A", "Text Definition Index",		16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(96, "S12W", "Send W12A...W12D <Data=Text Index>",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(89, "W12A", "   Text Definition Index to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(90, "W12B", "   Text Bitmask to Send",		16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(91, "W12C", "   Text Number to Send",		16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(92, "W12D", "   Text (32 char max.) to Send",	10,  0,	9,	addrLongAsLong));

	AddSpace(New CSpace(15, "R16A", "32 Bit Process Words",			10,  1,	2,	addrLongAsLong));
	AddSpace(New CSpace(16, "S16W", "Send W16A1 + W16A2",			10,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(26, "W16A", "   2x32 Bit Process Words to Send",	10,  1,	2,	addrLongAsLong));

	AddSpace(New CSpace(17, "R17A", "16 Bit Process Words",			10,  1,	4,	addrLongAsLong));
	AddSpace(New CSpace(18, "S17W", "Send W17A1...W17A4",			10,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(27, "W17A", "   4x16 Bit Process Words to Send",	10,  1,	4,	addrLongAsLong));

	AddSpace(New CSpace(66, "R18A", "Process Data TimeStamp",		16,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(67, "R18B", "Process Data 1 IN",			16,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(68, "R18C", "Process Data 2 IN",			16,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(69, "R18D", "Process Data 3 IN",			16,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(70, "R18E", "Process Data 4 IN",			16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(76, "S18W", "Send W18A...W18E",			16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(71, "W18A", "   Process Data TimeStamp to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(72, "W18B", "   Process Data 1 OUT to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(73, "W18C", "   Process Data 2 OUT to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(74, "W18D", "   Process Data 3 OUT to Send",	16,  0,	0,	addrLongAsLong));
//*!*/	AddSpace(New CSpace(75, "W18E", "   Process Data 4 OUT to Send",	16,  0,	0,	addrLongAsLong));

	AddSpace(New CSpace(30, "S48",  "Send W48D1+W48D2",			10,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(77, "W48D", "   2x32 Bit Process Data to Send",	10,  1,	2,	addrLongAsLong));
	AddSpace(New CSpace(29, "R48D", "   S48 Response, 2 Data, 1 Function Code",	10,  1,	3,	addrLongAsLong));

	AddSpace(New CSpace(32, "S49",  "Send W49D1...W49D4",			10,  0,	0,	addrLongAsLong));
	AddSpace(New CSpace(78, "W49D", "   4x16 Bit Process Data to Send",	10,  1,	4,	addrLongAsLong));
	AddSpace(New CSpace(31, "R49D", "   S49 Response, 4 Data, 1 Function Code",	10,  1,	5,	addrLongAsLong));

	AddSpace(New CSpace(19, "ERR",  "Latest Error + Service Code",		10,  0,	0,	addrLongAsLong));
	}

// Address Management

BOOL CKEBDIN2Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CKEBDIN2Dialog Dlg(ThisObject, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CKEBDIN2Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		UINT uParam = 0;

		BOOL fErr = FALSE;

		CString Err;

		Addr.a.m_Type   = addrLongAsLong;
		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Extra  = 0;

		if( !NeedSet(Addr.a.m_Table) ) {

			if( pSpace->m_uMaximum ) {

				uParam = ToHex( Text, 4 );

				if( pSpace->m_uMinimum > uParam || pSpace->m_uMaximum < uParam ) {

					fErr = TRUE;

					if( pSpace->m_uRadix == 10 ) {

						Err.Printf( " 1 - %1.1d ",
							pSpace->m_uMaximum
							);
						}

					else {
						Err.Printf( " 1 - %4.4X ",
							pSpace->m_uMaximum
							);
						}
					}

				else {
					Addr.a.m_Offset = uParam;

					return TRUE;
					}
				}

			else {
				Addr.a.m_Offset = 0;

				return TRUE;
				}
			}

		else {
			UINT uPos = 0;

			BYTE bSet = 0;

			uParam = ToHex(Text, 4);

			uPos  = Text.Find(':');

			if( uPos < NOTHING ) {

				bSet   = LOBYTE(ToHex(Text.Mid( uPos + 1 ), 2) );

				if( bSet > 0xF ) {

					Err.Printf( "%4.4X:<0 - 0xF>",
						uParam
						);

					fErr = TRUE;
					}
				}

			if( uParam & 0x8000 ) {

				Err.Printf( "<0 - 0x7FFF>:<0 - 0xF>" );

				fErr = TRUE;
				}
					
			else {
				Addr.a.m_Table  = pSpace->m_uTable;
				Addr.a.m_Extra  = bSet;
				Addr.a.m_Offset = uParam | 0x8000;
				}
			}

		if( fErr ) {
				
			Error.Set( Err,
				0
				);
						   
			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CKEBDIN2Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpace * pSpace;

		if( NeedSet(Addr.a.m_Table) ) {

			CAddress A;

			A.a.m_Table  = Addr.a.m_Table;
			A.a.m_Offset = 1;
			A.a.m_Extra  = 0;

			pSpace = GetSpace(A);

			if( Addr.a.m_Offset & 0x8000 ) {

				Text.Printf( "%s%4.4X:%1.1X",
					pSpace->m_Prefix,
					Addr.a.m_Offset & 0x7FFF,
					Addr.a.m_Extra
					);
				}

			else { // initial entry

				Text.Printf( "P0000:0" );
				}

			}

		else {
			pSpace = GetSpace(Addr);

			switch( pSpace->m_uTable ) {

				case  3:
				case  4:
				case  5:
				case  9:
				case 14:
				case 16:
				case 18:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 30:
				case 32:
				case 41:
				case 42:
				case 43:
				case 44:
				case 45:
				case 51:
				case 52:
				case 53:
				case 54:
				case 55:
				case 56:
				case 66:
				case 67:
				case 68:
				case 69:
				case 70:
				case 71:
				case 72:
				case 73:
				case 74:
				case 75:
				case 76:
				case 89:
				case 90:
				case 91:
				case 92:
				case 96:
					Text.Printf( "%s",
						pSpace->m_Prefix
						);
					break;

				case 15:
				case 17:
				case 26:
				case 27:
				case 29:
				case 31:
				case 77:
				case 78: 
					Text.Printf( "%s%1.1d",
						pSpace->m_Prefix,
						Addr.a.m_Offset
						);
					break;

				default:
					Text.Printf( "%s%4.4X",
						pSpace->m_Prefix,
						Addr.a.m_Offset
						);
					break;
				}
			}

		return TRUE;
		}

	return FALSE;
	
	}

UINT CKEBDIN2Driver::ToHex(CString Text, UINT uMaxCt)
{
	UINT uResult = 0;
	UINT uChar;

	Text.MakeUpper();

	for( UINT i = 0; i < uMaxCt; i++ ) {

		if( isdigit(Text[i]) || (Text[i] >= 'A' && Text[i] <= 'F') || (Text[i] == ' ') ) {

			uResult <<= 4;

			if( Text[i] != ' ' ) {

				uChar = Text[i] - '0';

				uResult += ( uChar <= 9 ? uChar : uChar - 7 );
				}
			}

		else break;
		}

	return uResult;
	}

BOOL CKEBDIN2Driver::NeedSet(UINT uTable)
{
	return uTable == 28 || uTable == 57 || uTable == addrNamed;
	}

//////////////////////////////////////////////////////////////////////////
//
// KEBDIN2 via MPI Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CKEBDIN2Dialog, CStdAddrDialog);
		
// Constructor

CKEBDIN2Dialog::CKEBDIN2Dialog(CKEBDIN2Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "KEBDIN2ElementDlg";
	}

AfxMessageMap(CKEBDIN2Dialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	};

// Overridables

BOOL CKEBDIN2Dialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;

	CAddress A;

	A.a.m_Type   = addrLongAsLong;
	A.a.m_Extra  = 0;
	A.a.m_Table  = Addr.a.m_Table;

	if( Addr.a.m_Table ) {

		if( NeedSet(Addr.a.m_Table) ) {

			A.a.m_Table  = Addr.a.m_Table;
			A.a.m_Offset = 1;
			A.a.m_Extra  = 0;
			}

		else {

			switch( ParameterType(Addr.a.m_Table) ) {

				case KNOP:
					A.a.m_Offset = 0;
					break;
  
				case KPAR:
				case KARR:
					A.a.m_Offset = 1;
					break;
				}
			}
		}

	m_pSpace = m_pDriver->GetSpace(A);
	
	if( !m_fPart ) {

		SetCaption();

		LoadList();

		if( m_pSpace ) {

			ShowAddress(Addr);

			SetAddressFocus();

			return FALSE;
			}

		return TRUE;
		}

	else {
		LoadType();
		
		ShowAddress(Addr);

		SetAddressFocus();

		return FALSE;
		}
	}

void CKEBDIN2Dialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CKEBDIN2Dialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	BOOL fSet    = FALSE;

	UINT uFind   = Text.GetLength();

	CString c1   = "";
	CString c2   = "";

	if( fEnable ) {

		if( NeedSet(m_pSpace->m_uTable) ) {

			uFind = Text.Find(':');

			if( uFind < NOTHING ) {

				c2 = Text.Mid(uFind+1);

				if( !c2 || tatoi(c2) > 0xF ) c2 = "0";
				}

			fSet = TRUE;

			c1 = Text.Left( min( uFind, 4 ) );
			}

		else {

			switch( ParameterType( m_pSpace->m_uTable) ) {

				case KNOP:
					fEnable = FALSE;
					break;

				case KARR:
					c1 = Text.Left( 1 );
					break;

				case KPAR:
				default:
					c1 = Text.Left( min( uFind, 4 ) );
					break;
				}
			}

		ShowDetails();
		}

	GetDlgItem(2002).SetWindowText(c1);

	GetDlgItem(2005).SetWindowText(c2);

	GetDlgItem(2002).EnableWindow(fEnable);
	
	GetDlgItem(2004).EnableWindow(fSet);
	
	GetDlgItem(2005).EnableWindow(fSet);
	
	GetDlgItem(2006).EnableWindow(fEnable);
	
	GetDlgItem(2007).EnableWindow(fSet);
	}

CString CKEBDIN2Dialog::GetAddressText(void)
{
	CString Text;

	CString c1;

	Text += GetDlgItem(2002).GetWindowText();

	if( NeedSet(m_pSpace->m_uTable) ) {

		Text += ":";

		c1 = GetDlgItem(2005).GetWindowText();

		if( !c1 ) c1 = "0";

		UINT u = tatoi(c1);

		if( u > 0xF ) c1 = "0";
	
		Text += c1;
		}

	return Text;
	}

void CKEBDIN2Dialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	Text = Text.Mid(m_pSpace->m_Prefix.GetLength());

	SetAddressText(Text.Left(Text.Find('.')));
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);

	ShowType(addrLongAsLong);
	}

void CKEBDIN2Dialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	CAddress Addr;

	Addr.a.m_Type  = addrLongAsLong;

	Addr.a.m_Extra = 0;

	if( NeedSet( m_pSpace->m_uTable ) ) {

		Min = "0000:0";
		Max = "7FFF:F";
		Rad = "Hexadecimal";
		}

	else {
		switch( ParameterType( m_pSpace->m_uTable ) ) {

			case KNOP:
				Min = "";
				Max = "";
				Rad = "";

				break;

			case KARR:
			case KPAR:

				m_pSpace->GetMinimum(Addr);

				m_pDriver->ExpandAddress(Min, m_pConfig, Addr);

				m_pSpace->GetMaximum(Addr);

				m_pDriver->ExpandAddress(Max, m_pConfig, Addr);

				Rad = m_pSpace->GetRadixAsText();

				break;
			}
		}

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

// Helper
UINT CKEBDIN2Dialog::ParameterType( UINT uTable )
{
	switch( uTable ) {

		case 3:
		case 4:
		case 5:
		case 9:
		case 14:
		case 16:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 30:
		case 32:
		case 41:
		case 42:
		case 43:
		case 44:
		case 45:
		case 51:
		case 52:
		case 53:
		case 54:
		case 55:
		case 56:
		case 66:
		case 67:
		case 68:
		case 69:
		case 70:
		case 71:
		case 72:
		case 73:
		case 74:
		case 75:
		case 76:
		case 89:
		case 90:
		case 91:
		case 92:
//		case 93:
//		case 94:
//		case 95:
		case 96:
			return KNOP; // No Parameter

		case 15:
		case 17:
		case 29:
		case 31:
		case 77:
		case 78:
			return KARR; // Data Array Offset

		case 1:
		case 2:
		case 6:
		case 7:
		case 8:
		case 10:
		case 11:
		case 12:
		case 13:
		case 26:
		case 27:
		case 28:
		case 37:
		case 38:
		case 39:
		case 40:
		case 46:
		case 47:
		case 48:
		case 49:
		case 50:
		case 57:
		case 83:
		case 84:
		case 85:
//		case 86:
//		case 87:
//		case 88:
		       return KPAR; // Parameter Address needed
		}

	return KSET; // Paramter + Set Number
	}

BOOL CKEBDIN2Dialog::NeedSet(UINT uTable)
{
	return uTable == 28 || uTable == 57 || uTable == addrNamed;
	}

// End of File

