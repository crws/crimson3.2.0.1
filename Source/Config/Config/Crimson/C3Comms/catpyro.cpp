
#include "intern.hpp"

#include "catpyro.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CatPyro Master Driver
//

// Instantiator

ICommsDriver *	Create_CatPyroMasterDriver(void)
{
	return New CCatPyroMasterDriver;
	}

// Constructor

CCatPyroMasterDriver::CCatPyroMasterDriver(void)
{
//	m_wID		= 0x40A7;	// Original SkelMaster ID
	m_wID		= 0x40AD;	// CAT Pyrometer 179-9001 

	m_uType		= driverMaster;
	
//	m_Manufacturer	= "Skeleton";
	m_Manufacturer	= "Monico";
	
//	m_DriverName	= "Master";
	m_DriverName	= "Pyrometer 179-9001";
	
	m_Version	= "1.00";
	
//	m_ShortName	= "Skeleton Master";
	m_ShortName	= "Pyro1";

	AddSpaces();
	}

// Binding Control

UINT CCatPyroMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CCatPyroMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CCatPyroMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CCatPyroMasterDriverOptions);
	}

CLASS CCatPyroMasterDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Implementation

void CCatPyroMasterDriver::AddSpaces(void)
{
	AddSpace( New CSpace(	'D',			// Tag to represent table to runtime
				"D",			// Prefix presented to user
				"Pyro Data",		// Long name presented to user
				10,			// Radix of numeric portion
				0,			// Minimum value of numeric portion
//				50,			// Maximum value of numeric portion
				25,			// Maximum value of numeric portion
				addrLongAsLong,		// Type of data and presentation
				addrLongAsLong,		// Same as above in most instances
				2			// Width of numeric portion
				));
	}

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCatPyroMasterDriverOptions, CUIItem);

// Constructor

CCatPyroMasterDriverOptions::CCatPyroMasterDriverOptions(void)
{
	m_Drop = 0x4D;
	}

// UI Managament

void CCatPyroMasterDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support	     

BOOL CCatPyroMasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CCatPyroMasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

// End of File
