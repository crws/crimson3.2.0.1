
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#include "ctquantm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Control Techniques Quantum Driver : Control Techniques Mentor2 Driver
//

// Instantiator

ICommsDriver *	Create_CTQuantumDriver(void)
{
	return New CCTQuantumDriver;
	}

// Constructor

CCTQuantumDriver::CCTQuantumDriver(void)
{
	m_wID		= 0x403F;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Nidec - Control Techniques";
	
	m_DriverName	= "Quantum";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Control Techniques Quantum";
	}

// End of File
