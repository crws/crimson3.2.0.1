
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RAWSER_HPP
	
#define	INCLUDE_RAWSER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedItem : public CItem { };

//////////////////////////////////////////////////////////////////////////
//
// Raw Serial Driver Options
//

class CRawSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRawSerialDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);


		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		CCodedItem *m_pService;
		UINT        m_Mode;
		UINT	    m_Buffer;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw Serial Driver
//

class CRawSerialDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CRawSerialDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
	};

// End of File

#endif
