
#include "intern.hpp"

#include "s5as511.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS5AS511DriverOptions, CUIItem);       

// Constructor

CS5AS511DriverOptions::CS5AS511DriverOptions(void)
{
	m_Cache = FALSE;

	m_Mode  = 0; 
	}

// Download Support

BOOL CS5AS511DriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Cache));
	
	Init.AddByte(BYTE(m_Mode));

	return TRUE;
	}

// Meta Data Creation

void CS5AS511DriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Cache);
	
	Meta_AddInteger(Mode);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master Driver
//

// Instantiator

ICommsDriver * Create_S5AS511MasterDriver(void)
{
	return New CS5AS511MasterDriver(FALSE);
	}

ICommsDriver * Create_S5AS511MasterDriverV2(void)
{
	return New CS5AS511MasterDriver(TRUE);
	}

// Constructor

CS5AS511MasterDriver::CS5AS511MasterDriver(BOOL fV2) : CS5AS511BaseDriver(fV2)
{
	m_DriverName = "S5 via AS511";

	if( fV2 ) {

		m_wID	  = 0x33A1;

		m_Version = "2.04"; 
		}
	else {
		m_wID	  = 0x339F;

		m_Version = "1.04";
		}
	}

// Configuration

CLASS CS5AS511MasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CS5AS511DriverOptions);
	}

// Binding Control

UINT CS5AS511MasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CS5AS511MasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// End of File
