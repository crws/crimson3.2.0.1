#include "intern.hpp"

#include "micromod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Micromod Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMicromodDeviceOptions, CUIItem);

// Constructor

CMicromodDeviceOptions::CMicromodDeviceOptions(void)
{
	m_Drop		= 1;
	m_Database	= 0;
	m_UnlockGNR	= 0;
	m_TagPrefix	= L"";

	m_ArraySelect	= 0;
	m_ArrayValue	= 0;

	m_fIsOldDB	= FALSE;
	m_fWasOldDB	= FALSE;
	m_fMakeTags	= FALSE;

	m_pTags		= NULL;
	m_pViewWnd	= NULL;

	m_IMPList.Empty();
	m_LSPList.Empty();
	}

// UI Management

void CMicromodDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	m_pViewWnd = pWnd;

	if( pItem == this ) {

		if( Tag == "Push" ) {

			if( m_fIsOldDB ) {

				CString s1 =	TEXT("You have loaded a database from a previous version of the software.\r\n")
						TEXT("You must import the original MIF file for the device ");

				CString s2 =	TEXT("Do you wish to import the MIF now?");

				s1.Printf( L"%s%s.\r\n\n%s", s1, GetDeviceName(), s2);

				if( pWnd->YesNo(s1) == IDNO ) {

					return;
					}

				m_Push = 1;
				}

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					OnImport(pWnd);
					break;

				case 2:
					OnExport(pWnd);
					break;
				}
			}
		}
	}

CString CMicromodDeviceOptions::GetDeviceName(void)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pData = pItem->FindMetaData(TEXT("Name"));

	return pData ? pData->ReadString(pItem) : TEXT("Device");
	}

// Persistance

void CMicromodDeviceOptions::Init(void)
{
	CUIItem::Init();
	}

void CMicromodDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == TEXT("MMList") ) {

			Tree.GetObject();

			LoadArrays(Tree);

			Tree.EndObject();

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CMicromodDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	if( m_IMPList.GetCount() ) {

		Tree.PutObject(TEXT("MMList"));

		SaveArrays(Tree);

		Tree.EndObject();
		}
	}

BOOL CMicromodDeviceOptions::CreateMMTag(CError &Error, CStringArray List, UINT uArrayPos, IMakeTags *pTags) {

	if( pTags ) {

		CString sName = MakeNameString(List[ALIASPOS]);	// add TagPrefix if neeeded

		CString sLabel = sName;

		CString sValue = "[" + sName + "]";

		CString sItem1;

		UINT uType = tatoi(List[NTYPPOS]);

		switch( uType ) {

			case 99:	// future

				UINT i;
				DWORD dExtent;

				char cmin[16];
				char cmax[16];

				sItem1 = List[LRNGPOS];

				for( i = 0; i < sItem1.GetLength(); i++ ) {

					cmin[i] = LOBYTE(sItem1[i]);
					}

				cmin[i] = 0;

				sItem1 = List[HRNGPOS];

				for( i = 0; i < sItem1.GetLength(); i++ ) {

					cmax[i] = LOBYTE(sItem1[i]);
					}

				cmax[i] = 0;

				dExtent  = strtol((const char *)cmax, NULL, 10);
		
				dExtent -= strtol((const char *)cmin, NULL, 10);

				pTags->AddEnum(sName, sLabel, sValue, dExtent, 0, L"");

				return TRUE;

			case 3:		// Bit

				pTags->AddFlag( sName,
						sLabel,
						sValue,
						0,			// Extent = One Item
						0,			// Treat As...
						2,			// Read/Write
						L"OFF",			// State 0
						L" ON"			// State 1
						);
				return TRUE;

			case 10:	// Real
				pTags->AddReal(	sName,
						sLabel,
						sValue,
						0,			// Extent = One Item
						2,			// Read/Write
						List[LRNGPOS],		// Min
						List[HRNGPOS],		// Max
						3			// DP
						);

				return TRUE;

			case 0:		// Count
			case 5:		// Short State
			case 9:		// Long State
			case 11:	// ASCII Long Integer
			case 12:	// MSEC TIME Long Integer
			default:
				pTags->AddInt(	sName,
						sLabel, 
						sValue,
						0,			// Extent = One Item
						2,			// Read/Write
						List[LRNGPOS],		// Min
						List[HRNGPOS],		// Max
						0,			// DP
						0			// TreatAs
						);
			}
		}

	return TRUE;
	}

BOOL CMicromodDeviceOptions::CreateStdTag(CError &Error, CString Name, CAddress Addr, IMakeTags *pTags) {

	if( !pTags ) {

		return FALSE;
		}

	CString sLabel = Name;
	CString sValue = L"[" + Name + L"]";

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			pTags->AddFlag( Name,
					sLabel,
					sValue,
					0,			// Extent = One Item
					0,			// Treat As...
					2,			// Read/Write
					L"OFF",			// State 0
					L" ON"			// State 1
					);
				return TRUE;

		case addrRealAsReal:
			pTags->AddReal(	Name,
					sLabel,
					sValue,
					0,			// Extent = One Item
					2,			// Read/Write
					L"",			// Min
					L"",			// Max
					3			// DP
					);

			return TRUE;


		default:
			pTags->AddInt(	Name,
					sLabel, 
					sValue,
					0,			// Extent = One Item
					2,			// Read/Write
					L"",			// Min
					L"",			// Max
					0,			// DP
					0			// TreatAs
					);
			return TRUE;

		}

	return FALSE;
	}

BOOL CMicromodDeviceOptions::CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fMake, BOOL fWantExist) {

	if( Addr.a.m_Table != SP_65 ) {

		m_TagsFwdMap.Insert(Name, Addr.m_Ref);
		m_TagsRevMap.Insert(Addr.m_Ref, Name);

		CreateStdTag(CError(FALSE), Name, Addr, m_pTags);

		return TRUE;
		}

	INDEX Index;

	UINT uCount = m_IMPList.GetCount();

	UINT uPos = 0;

	CString sImp;

	CString sName = Name;

	CStringArray List;

	while( uPos < uCount ) {

		sImp  = m_IMPList.GetAt(uPos);

		List.Empty();

		sImp.Tokenize(List, ',');

		CString sTest1 = List[ALIASPOS];

		CString sTest2 = m_TagPrefix.GetLength() ? m_TagPrefix + "_" : "";

		CString sTest3;

		sTest3.Printf( L"%s%s", sTest2, sTest1 );

		if( sTest1.Find(sName) < NOTHING || sName.Find(sTest1) < NOTHING ) {

			if( sTest1 == sName || sTest3 == sName ) {
	
				sName = MakeNameString(sName);	// add Tag Prefix if needed

				break;
				}
			}

		uPos++;
		}

	m_TagsFwdMap.Insert(sName, Addr.m_Ref);
	m_TagsRevMap.Insert(Addr.m_Ref, sName);

	if( fMake ) {

		if( uPos >= uCount ) return FALSE;	// alias not programmed

		Index = m_TagsFwdMap.FindName(sName);

		UINT uEPos = sImp.GetLength() - 1;

		if( sImp[uEPos] == ',' ) {

			sImp.Left( uEPos );	// strip a terminating ','
			}

		uEPos = sImp.FindRev(',');	// find ',' before the entry number

		UINT uEVal = tatoi( sImp.Mid(uEPos + 1) );

		CString sErr = "";

		if( HIWORD(uEVal) ) {	// tag already mapped

			return TRUE;
			}

		if( fWantExist ) {

			if( m_pTags ) {	// making tags from existing list

				if( m_TagsFwdMap.Failed(Index) ) {

					sErr.Printf( L"Tag %s does not exist", sName );

					Error.Set(sErr);

					return FALSE;
					}
				}
			}

		else {
			if( !m_TagsFwdMap.Failed(Index)  ) {

				sErr.Printf( L"Tag %s exists", sName );

				Error.Set(sErr);

				return FALSE;
				}
			}

		if( m_pTags ) {

			sImp.Printf( L"%d", tatoi(List[ENTRYPOS]) | 0xFF0000 );

			List.SetAt(ENTRYPOS, sImp);

			m_IMPList.SetAt(uPos, UpdateIMPString(List));

			CreateMMTag(CError(FALSE), List, uPos, m_pTags);

			return TRUE;
			}
		}

	return TRUE;
	}

void CMicromodDeviceOptions::CreateTagsFromIMP(void) {

	UINT uTotal = m_IMPList.GetCount();

	CStringArray List;

	for( UINT i = 0; i < uTotal; i++ ) {

		List.Empty();

		CString sIMP = m_IMPList.GetAt(i);

		sIMP.Tokenize(List, ',');

		UINT uTagged = tatoi(List[ENTRYPOS]);

		if( !HIWORD(uTagged) ) {

			if( CreateMMTag(CError(FALSE), List, i, m_pTags) ) {

				List.SetAt(ENTRYPOS, CPrintf( L"%d", uTagged | 0xFF0000 ));

				m_IMPList.SetAt(i, UpdateIMPString(List));
				}
			}
		}
	}

void CMicromodDeviceOptions::DeleteParam(CAddress const &Addr, CString const &Name)
{
	INDEX Index = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {

		DWORD dRef = m_TagsFwdMap.GetData(Index);

		if( dRef == Addr.m_Ref ) {

			m_TagsFwdMap.Remove(Index);
			m_TagsRevMap.Remove(Index);
			}
		}
	}

BOOL CMicromodDeviceOptions::RenameParam(CString const &Name, CAddress const &Addr)
{
	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		if( m_List.GetAt(n).m_Addr == Addr.m_Ref ) {

			CTagData Param;

			Param.m_Addr   = m_List.GetAt(n).m_Addr;

			Param.m_Name   = Name;			

			DeleteParam(Addr, Name);

			m_List.Insert(n, Param);	

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CMicromodDeviceOptions::FinishMakeTags(BOOL fMake, BOOL fWantExist) {

	m_uTotalCount = m_IMPList.GetCount();

	for( UINT i = 0; i < m_uTotalCount; i++ ) {

		if( IsNewAlias(i) ) {

			MakeNewTag(i, fMake, fWantExist);
			}
		}

	return TRUE;
	}

BOOL CMicromodDeviceOptions::MakeNewTag(UINT uItem, BOOL fMake, BOOL fWantExist) {

	CStringArray List;

	List.Empty();

	m_IMPList[uItem].Tokenize(List, ',');

	CAddress Addr;

	Addr.a.m_Table  = SP_65;
	Addr.a.m_Offset = tatoi(List[NUMPOS]);
	Addr.a.m_Type   = GetDataType(List[ATYPPOS]);
	Addr.a.m_Extra  = 0;

	return CreateTag(CError(FALSE), List[ALIASPOS], Addr, fMake, fWantExist);
	}

// Address Management

BOOL CMicromodDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CString Name	= Text;

	if( Name.Left(2) == "A_" ) {

		Name = Name.Mid(2);		// strip prefix if present
		}

	UINT uItem = Name.Find('.');

	if( uItem < NOTHING ) {

		Name = Name.Left(uItem);	// strip data type if present
		}

	CString sWork = L"";

	UINT uID = 12345;

	UINT uFind;

	if( isdigit(Name[0]) ) {		// not making Tags from import

		uID   = FindNewIDFromOld(tatoi(Name), m_OldIDList.GetCount());

		Name  = GetIMPStringItem(uID, ALIASPOS);

		sWork = m_IMPList.GetAt(uID);

		uFind = sWork.FindRev(',');				// prior to Entry Number

		UINT uEntr = tatoi(sWork.Mid(uFind + 1));

		uEntr |= 0xFF0000;					// flag entry number as tag mapping complete

		sWork.Printf( L"%s,%d", sWork.Left(uFind), uEntr);	// make tentative IMP string
		}

	INDEX  Index = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {

		Addr.m_Ref = m_TagsFwdMap.GetData(Index);

		if( uID < 12345 ) {

			m_IMPList.SetAt(uID, sWork);	// update tag status of old database item with new string
			}

		return TRUE;
		}

	uFind = m_TagPrefix.GetLength();

	if( uFind ) {

		sWork = Name.Mid(uFind + 1);

		Index = m_TagsFwdMap.FindName(sWork);

		if( !m_TagsFwdMap.Failed(Index) ) {

			Addr.m_Ref = m_TagsFwdMap.GetData(Index);

			return TRUE;
			}
		}


	return FALSE;
	}

UINT CMicromodDeviceOptions::FindNewIDFromOld(UINT uOldID, UINT uMax) {

	UINT uID = uOldID;

	for( UINT i = 0; i < uMax; i++ ) {

		if( uID == (UINT)(m_OldIDList.GetAt(i)) - 1 ) {		// original ID positions were 0 based, MIF entries were 1 based

			return i;
			}
		}

	return uID;
	}

BOOL CMicromodDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	CAddress Find = Addr;

	INDEX Index = m_TagsRevMap.FindName(Find.m_Ref);

	if( !m_TagsRevMap.Failed(Index) ) {

		Text = m_TagsRevMap.GetData(Index);

		if( isdigit(Text[0]) ) {	// not making tags from import

			Text = GetIMPStringItem(tatoi(Text), ALIASPOS);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			m_List.Empty();
			
			ListTags(m_List);

			if( !m_List.GetCount() ) {

				return FALSE;
				}
			}
		else {
			if( uItem >= m_List.GetCount() ) {

				return FALSE;
				}
			}
		
		CTagData &TagData = (CTagData &) m_List[uItem];

		CAddress Addr = (CAddress &) TagData.m_Addr;
			
		Data.m_Name   = TagData.m_Name;
		Data.m_Addr   = Addr;
		Data.m_fPart  = FALSE;

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	Data.m_Type = TEXT("Flag");	break;

			case addrLongAsLong:	Data.m_Type = TEXT("Long");	break;

			default:		Data.m_Type = TEXT("Real");	break;
			}

		return TRUE;
		}

	return FALSE;
	}

void CMicromodDeviceOptions::ListTags(CTagDataArray &List)
{
	INDEX Index = m_TagsFwdMap.GetHead();

	while( !m_TagsFwdMap.Failed(Index) ) {

		CTagData Data;

		Data.m_Name = m_TagsFwdMap.GetName(Index);
		
		Data.m_Addr = m_TagsFwdMap.GetData(Index);

		List.Append(Data);
		
		m_TagsFwdMap.GetNext(Index);
		}
	}

BOOL CMicromodDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CMicromodDriver Driver;

	CMicromodAddrDialog Dlg(Driver, Addr, this, fPart);

	Dlg.SetSelect(TRUE);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

void CMicromodDeviceOptions::IMPListAppend(CString sIMPLine) {

	m_IMPList.Append(sIMPLine);
	}

void CMicromodDeviceOptions::IMPListInsert(CString sIMPLine, UINT uPos) {


	}

BOOL CMicromodDeviceOptions::Import(FILE *pFile, CString sType, IMakeTags *pTags) {

	m_pTags = pTags;

	m_fMakeTags = TRUE;

	m_uOverwrite = m_IMPList.GetCount();

	if( m_uOverwrite ) {

		SaveOldIMPNames();
		}

	m_fImportOverwrite = TRUE;

	return ImportAliasFile(pFile, sType);
	}

CString CMicromodDeviceOptions::UpdateIMPString(CStringArray List) {

	UINT uEnd = List.GetCount();

	CString s = L"";

	for( UINT n = 0; n < uEnd; n++ ) {

		s += List[n];

		if( n < uEnd - 1 ) {

			s += ',';
			}
		}

	return s;
	}

void CMicromodDeviceOptions::SortIMPListByFG(BOOL fAssign) {

	UINT uCount = m_IMPList.GetCount();

	CString Line;

	CStringArray List;

	CStringArray CA;

	UINT i;

	for( i = 0; i < uCount; i++ ) {

		Line = m_IMPList[i];

		List.Empty();

		Line.Tokenize(List, ',');

		UINT uBit = tatoi(List[FGBITPOS]);

		Line.Printf( "%6.6u%1.1d,%d",

			tatoi(List[FGOFFPOS]),
			uBit,
			i
			);

		CA.Append(Line);
		}

	CA.Sort();			// sort in order of Foreground offset, plus bit if necessary

	List.Empty();

	for( i = 0; i < uCount; i++ ) {	// initialize List to ensure proper sequence

		List.Append( L" " );
		}

	for( i = 0; i < uCount; i++ ) {

		UINT uFind = CA[i].Find(',');

		UINT uPos  = tatoi(CA[i].Mid(uFind + 1));	// original position of this item

		CString sL = m_IMPList.GetAt(uPos);		// get this item

		if( fAssign ) {

			Line.Printf( L"%4.4d", i );		// re-assign offset numbers

			uFind = sL.Find(',');			// offset number is left of the first ','

			sL.Printf( L"%s%s", Line, sL.Mid(uFind) );
			}

		List.SetAt( i, sL );
		}

	for( i = 0; i < uCount; i++ ) {

		Line = List.GetAt(i);

		m_IMPList.SetAt(i, Line);
		}
	}

void CMicromodDeviceOptions::SortIMPListByOffset(BOOL fReassign) {

	CStringArray List;

	List.Empty();

	CString s;

	UINT uTotal = m_IMPList.GetCount();

	for( UINT i = 0; i < uTotal; i++ ) {

		s = m_IMPList.GetAt(i);

		if( tatoi(s) < 65535 ) {	// 65535 is deleted item

			List.Append( s );
			}
		}

	List.Sort();

	uTotal = List.GetCount();

	m_IMPList.Empty();

	for( UINT i = 0; i < uTotal; i++ ) {

		m_IMPList.Append( L" " );

		CString sItem = List.GetAt(i);

		if( fReassign ) {		// if reassigning offset numbers

			s.Printf( L"%d", i );	// new offset

			sItem.Printf( L"%s%s", s, sItem.Mid(sItem.Find(',')) );
			}

		m_IMPList.SetAt(i, sItem);
		}
	}

BOOL CMicromodDeviceOptions::NameExists(CString sName, PDWORD pAddress) {

	CString s = MakeNameString(sName);

	INDEX Index = m_TagsFwdMap.FindName(s);

	if( !m_TagsFwdMap.Failed(Index) ) {

		*pAddress = m_TagsFwdMap.GetData(Index);

		return TRUE;
		}

	return FALSE;
	}

void CMicromodDeviceOptions::RebuildMaps(void) {

	UINT uCount = m_IMPList.GetCount();

	if( !uCount ) {

		return;
		}

	SortIMPListByFG(FALSE);

	m_TagsFwdMap.Empty();
	m_TagsRevMap.Empty();

	CStringArray List;

	CString s;

	UINT uIndex = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		List.Empty();

		s = m_IMPList[i];

		s.Tokenize(List, ',');

		CString sName = MakeNameString(List[ALIASPOS]);

		INDEX Inx = m_TagsFwdMap.FindName(sName);

		if( m_TagsFwdMap.Failed(Inx) ) {

			CAddress Addr;

			Addr.a.m_Table	= SP_65;
			Addr.a.m_Extra	= 0;
			Addr.a.m_Type	= GetDataType(List[ATYPPOS]);
			Addr.a.m_Offset	= uIndex;

			m_TagsFwdMap.Insert( sName, Addr.m_Ref );
			m_TagsRevMap.Insert( Addr.m_Ref, sName );

			s.Printf( L"%d", uIndex );

			List.SetAt(NUMPOS, s);

			m_IMPList.SetAt(uIndex++, UpdateIMPString(List));
			}
		}
	}

CViewWnd * CMicromodDeviceOptions::GetView(void) {

	return m_pViewWnd;
	}

IMakeTags * CMicromodDeviceOptions::CheckMakeTags(void) {

	return m_pTags;
	}

void CMicromodDeviceOptions::SetTagPointer(IMakeTags * pTags) {

	m_pTags = pTags;
	}

// Download Support

BOOL CMicromodDeviceOptions::MakeInitData(CInitData &Init)
{
	if( m_ArraySelect ) {

		AccessArrayValue();

		m_ArraySelect = 0;

		return TRUE;
		}

	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Database));
	Init.AddLong(LONG(m_UnlockGNR));

	SortIMPListByOffset(FALSE);			// sort IMP List by Offset value

	UINT uCount = m_IMPList.GetCount();

	Init.AddWord(LOWORD(uCount + 1));		// include terminator in count

	CStringArray List;

	CString s;

	CArray <DWORD>	dLSP;
	CArray <WORD>	wFG;
	CArray <WORD>	wBit;
	CArray <WORD>	wSze;

	dLSP.Empty();
	wFG.Empty();
	wBit.Empty();
	wSze.Empty();

	UINT i;

	for( i = 0; i < uCount; i++ ) {	// ensure proper sequencing by initializing the arrays to 0

		dLSP.Append(0);
		wFG.Append(0);
		wBit.Append(0);
		wSze.Append(0);
		}

	for( i = 0; i < uCount; i++ ) {

		s = m_IMPList[i];

		List.Empty();

		s.Tokenize(List, ',');

		dLSP.SetAt(i, (DWORD)tatoi(List[NLSPPOS]));
		wFG.SetAt( i, LOWORD(tatoi(List[FGOFFPOS])));
		wSze.SetAt(i, min(4, LOWORD(tatoi(List[DLENPOS]))));

		UINT u = (UINT)tatoi(List[NTYPPOS]) == 3 ? (UINT)tatoi(List[FGBITPOS]) : 255;	// runtime uses 255 to signal non-bit
		wBit.SetAt(i, LOWORD(u));
		}

	for( i = 0; i < uCount; i++ ) {

		Init.AddLong((LONG)dLSP[i]);
		}

	Init.AddLong((unsigned long)0xFFFFFFFF);	// add Terminator

	for( i = 0; i < uCount; i++ ) {

		Init.AddWord((WORD)wFG[i]);
		}

	Init.AddWord((WORD)0xFFFF);			// add Terminator

	for( i = 0; i < uCount; i++ ) {

		Init.AddWord((WORD)wSze[i]);
		}

	Init.AddWord((WORD)0xFFFF);			// add Terminator

	for( i = 0; i < uCount; i++ ) {

		Init.AddWord((WORD)wBit[i]);
		}

	Init.AddWord((WORD)0xFFFF);			// add Terminator

	return TRUE;
	}

CString CMicromodDeviceOptions::DoTCPAccess(UINT uSelect, PDWORD pValue, CString sString) {

	m_ArraySelect = uSelect;
	m_ArrayValue  = *pValue;
	m_ArrayString = sString;

	CInitData Init;

	CMicromodDeviceOptions::MakeInitData(Init);

	*pValue = m_ArrayValue;

	return m_ArrayString;
	}

BOOL CMicromodDeviceOptions::AccessArrayValue(void)
{
	UINT uFunc	= m_ArraySelect;

	UINT uCurrCount	= m_IMPList.GetCount();

	BOOL fHasList	= (BOOL)uCurrCount;

	DWORD dValue	= m_ArrayValue;

	CString sImp	= m_IMPList[dValue];

	CStringArray List;

	List.Empty();

	sImp.Tokenize(List, ',');

	CStringArray Move;

	Move.Empty();

	CString sTemp;

	switch( uFunc ) {

		case RARRLSP:
			dValue = fHasList ? tatoi(List[NLSPPOS]) : 0;
			break;

		case RARRFG:
			dValue = fHasList ? tatoi(List[FGOFFPOS]) : 0;
			break;

		case RARRSZE:
			dValue = fHasList ? tatoi(List[DLENPOS]) : 0;
			break;

		case RARRBIT:
			dValue = fHasList ? tatoi(List[FGBITPOS]) : 0;
			break;

		case RARRALS:
			m_ArrayString	= fHasList ? List[ALSPPOS] : "None";
			break;

		case RARROFF:
			dValue = fHasList ? tatoi(List[NUMPOS]) : 0;
			break;

		case RARRTYP:
			dValue = fHasList ? LOWORD(GetDataType(List[ATYPPOS])) : 0;
			break;

		case RARRNAM:
			if( fHasList ) {

				m_ArrayValue  = LOWORD(tatoi(List[NUMPOS]));

				m_ArrayString = MakeNameString(List[ALIASPOS]);
				}

			else {
				m_ArrayString = TEXT("None");
				}
			break;

		case RARRCNT:
			dValue = uCurrCount;
			break;

		case GIMPSTR:
			m_ArrayString = m_IMPList[m_ArrayValue];
			break;

		default:
			return FALSE;
		}

	m_ArrayValue = (UINT)dValue;

	return TRUE;
	}

UINT CMicromodDeviceOptions::GetItemFromIMPByPosition(UINT uArrayPos, UINT uItem) {

	return GetIMPIntItem(uArrayPos, uItem);
	}

UINT CMicromodDeviceOptions::MakeNewOffset(void) {

	return m_IMPList.GetCount();
	}

void CMicromodDeviceOptions::SaveOldIMPNames(void) {

	m_OldNameList.Empty();
	m_OldAddrList.Empty();

	for( UINT i = 0; i < m_IMPList.GetCount(); i++ ) {

		CString s = GetIMPStringItem(i, ALIASPOS);
		m_OldNameList.Append(MakeNameString(s));
		m_OldAddrList.Append(GetIMPStringItem(i, NUMPOS));
		}
	}

// Persistance

void CMicromodDeviceOptions::LoadArrays(CTreeFile &Tree)
{
	m_IMPList.Empty();

	m_TagsFwdMap.Empty();
	m_TagsRevMap.Empty();

	CString RefName;
	CAddress   Addr;

	// support for loading old databases
	m_fIsOldDB  = FALSE;
	m_fWasOldDB = FALSE;

	EmptyOldLists();
	m_OldIDList.Empty();

	CString sWork;

	while( !Tree.IsEndOfData() ) {

		CString sName = Tree.GetName();

		if( sName == TEXT("IMP") ) {

			m_IMPList.Append(Tree.GetValueAsString());
			}

		else if( sName == TEXT("RNAM") ) {	// Name/Address Maps

			RefName = Tree.GetValueAsString();

			sName   = Tree.GetName();

			Addr.m_Ref = Tree.GetValueAsInteger();

			m_TagsFwdMap.Insert(RefName, Addr.m_Ref);
			m_TagsRevMap.Insert(Addr.m_Ref, RefName);
			}

		else if( sName == TEXT("LSP") ) {

			m_fIsOldDB = TRUE;

			sWork.Printf( L"%10.10d", Tree.GetValueAsInteger() );

			m_LSPList.Append(sWork);

			m_OldIDList.Append(0);	// to be filled when MIF import is done
			}

		else if( sName == TEXT("OFF") ) {

			m_fIsOldDB = TRUE;

			sWork.Printf( L"%d", Tree.GetValueAsInteger());

			m_OFFList.Append(sWork);
			}

		else if( sName == TEXT("SZE") ) {

			m_fIsOldDB = TRUE;

			sWork.Printf( L"%d", Tree.GetValueAsInteger());

			m_SZEList.Append(sWork);
			}

		else if( sName == TEXT("BIT") ) {

			m_fIsOldDB = TRUE;

			sWork.Printf( L"%d", Tree.GetValueAsInteger());

			m_BITList.Append(sWork);
			}

		else if( sName == TEXT("ALS") ) {

			m_fIsOldDB = TRUE;

			sWork = Tree.GetValueAsString();

			m_ALSList.Append(sWork);
			}
		}

	m_fWasOldDB = m_fIsOldDB;

	if( m_fIsOldDB ) {

		MakeIMPStringFromOldDB();
		}
	}

void CMicromodDeviceOptions::SaveArrays(CTreeFile &Tree)
{
	UINT uCnt = m_IMPList.GetCount();

	CTagDataArray List;

	ListTags(List);

	CString	s;
	UINT    u1;

	for( UINT n = 0; n < uCnt; n++ ) {

		s = m_IMPList.GetAt(n);

		Tree.PutValue( TEXT("IMP"), s );

		s  = List[n].m_Name;

		u1 = List[n].m_Addr;

		Tree.PutValue( TEXT("RNAM"),  s);

		Tree.PutValue( TEXT("RDAT"),  u1);
		}
	}

// Device Options Tag Management
void CMicromodDeviceOptions::OnManage(CWnd *pWnd) {

	if( ShowOldDBWarning(pWnd, FALSE) ) {

		CAddress Addr;

		Addr.m_Ref = 0;

		CMicromodDriver Driver;

		CMicromodAddrDialog Dlg(Driver, Addr, this, FALSE);

		Dlg.SetCaption(CPrintf("Micromod Tag Names for %s", m_DevName));

		Dlg.SetSelect(FALSE);

		Dlg.Execute(*pWnd);
		}
	}

// Export/Import support

void CMicromodDeviceOptions::OnImport(CWnd *pWnd) {

	m_fMakeTags = FALSE;

	CString s =	TEXT("This selection only imports alias names and properties.  It does not create tags.\n");

	if( !m_LSPList.GetCount() ) {

		s += TEXT("If you wish to create tags using the alias names in a new, imported, file,");
		s += TEXT("use the \"Create Data Tags\" selection below.\n\n");
		}

	s.Printf( L"%s%s", s, S_MMQUERY );

	m_OldNameList.Empty();

	if( pWnd->YesNo(s) == IDYES ) {

		m_uOverwrite = 0;

		if( !HandleOldImport(pWnd) ) {

			m_uOverwrite = m_IMPList.GetCount();

			Import(pWnd);
			}
		}
	}

void CMicromodDeviceOptions::OnExport(CWnd *pWnd)
{
	if( ShowOldDBWarning(pWnd, TRUE) ) {

		CSaveFileDialog Dlg;

		Dlg.LoadLastPath(TEXT("Micromod Tag Names"));

		Dlg.SetCaption(CPrintf("Export Parameters for %s", m_DevName));

		Dlg.SetFilter (TEXT("CSV Files|*.csv"));

		if( Dlg.Execute(*pWnd) ) {

			FILE *pFile;
		
			if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
				fclose(pFile);

				CString Text = TEXT("The selected file already exists.\n\n")
					       TEXT("Do you want to overwrite it?");

				if( pWnd->YesNo(Text) == IDNO ) {
			
					return;
					}
				}

			if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

				CString Text = TEXT("Unable to open file for writing.");

				pWnd->Error(Text);

				return;
				}

			Export(pFile);

			fclose(pFile);

			Dlg.SaveLastPath(TEXT("Micromod Tag Names"));
			}
		}
	}

void CMicromodDeviceOptions::Export(FILE *pFile) {

	UINT uTotal = m_IMPList.GetCount();

	if( !uTotal ) {

		return;
		}

	CString s;

	s.Printf( L"1,%d,24,,,,,,,,,,,,,,,,,,,,,\n", uTotal );

	fprintf(pFile, TEXT("# File,,,Inst,FG,BG,BG,Num,,,,,,,,,,,,,,,,\n"));
	fprintf(pFile, TEXT("# Type,Version,Time,ID,Sig,Sig,Sig LSP,Sections,,,,,,,,,,,,,,,,\n"));
	fprintf(pFile, TEXT("CSV,Crimson 3, Fri May 13 14:25:47 2011, I2,37028,0,0,1,,,,,,,,,,,,,,,,\n"));
	fprintf(pFile, TEXT("# Section,Num,Num,,,,,,,,,,,,,,,,,,,,,\n"));
	fprintf(pFile, TEXT("# Num,Lines,Elements,,,,,,,,,,,,,,,,,,,,,\n"));
	fprintf(pFile, s);
	fprintf(pFile, TEXT("# Line,,,,,Bottom,Top,Bottom,Top,EU,Data,Data,Data,Data,FG,FG,FG Bit,Quality,Quality,Quality,,Read/,List,Entry\n"));
	fprintf(pFile, TEXT("# Num,Name,Alias,LSP,LSP,EU Range,EU Range,EU Limit,EU Limit,Desc,Type,Type,Type,Len,Flag,Offset,Position,Flag,Offset,Bit Position,Enabled,Write,Num,Num\n"));

	for( UINT i = 0; i < uTotal; i++ ) {

		s = m_IMPList.GetAt(i);

		UINT u = s.FindRev(',');	// find ',' before the Entry Value

		s.Printf( L"%s,%d", s.Left(u), LOWORD(tatoi(s.Mid(u + 1))) );	// don't export Tag Made flag

		fprintf(pFile, s);
		fprintf(pFile, TEXT("\n"));
		}
	}

void CMicromodDeviceOptions::Import(CWnd *pWnd) {

	m_fImportOverwrite = m_uOverwrite && m_uOverwrite < NOTHING;	// aliases exist and not import for old db

	if( m_fImportOverwrite ) {

		if( !ConfirmImport(pWnd) ) {

			return;
			}
		}
	
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Micromod Tag Names"));

	Dlg.SetCaption(CPrintf("Import Parameters for %s", m_DevName));

	Dlg.SetFilter(TEXT("Micromod MIF Files (*.MIF)|*.MIF|CSV Files (*.csv)|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;

		CFilename File = Dlg.GetFilename();

		CString Text;

		if( !(pFile = fopen(File, TEXT("rt"))) ) {

			Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
			}
		else {
			m_uTotalCount     = m_IMPList.GetCount();

			UINT uImportCount = m_uTotalCount;

			CString s;

			if( m_uOverwrite == NOTHING ) {

				if( (uImportCount = ImportMIF(pFile)) ) {

					m_fWasOldDB = m_fIsOldDB;

					m_fIsOldDB  = FALSE;

					SetDirty();
					}
				}

			else {
				if( ImportAliasFile(pFile, File.GetType()) ) {

					SetDirty();
					}

				FinishMakeTags(FALSE, FALSE);	// Make tags if aren't mapped

				m_fWasOldDB = m_fIsOldDB;

				m_fIsOldDB  = FALSE;
				}

			Dlg.SaveLastPath(TEXT("Micromod Tag Names"));
			
			fclose(pFile);

			if( m_uOverwrite ) {

				if( m_uOverwrite == NOTHING ) {

					Text.Printf( L"Old Aliases Updated = %d.\n\n", uImportCount );

					m_uOverwrite = 0;

					m_fIsOldDB   = FALSE;
					}

				else {
					m_uTotalCount = m_IMPList.GetCount();

					uImportCount = m_uTotalCount - uImportCount;

					if( uImportCount ) {

						Text.Printf( L"Aliases Imported = %d.", uImportCount);
						}

					else {
						CStringArray List;

						CString s = L"";

						UINT i = 0;

						while( i < m_uTotalCount ) {

							List.Empty();

							s = m_IMPList.GetAt(i);

							s.Tokenize(List, ',');

							if( HIWORD(tatoi(List.GetAt(ENTRYPOS))) ) {

								i++;
								}

							else break;
							}

						if( i < m_uTotalCount ) {

							Text.Printf( L"Execute \"File\\Utilities\\Rebuild Comms Blocks\" when finished.\n");
							}

						else {
							Text = L"Done\n\n";
							}
						}
					}

				pWnd->Information(Text);
				}

			EmptyOldLists();
			}
		}
	}

// Import Old Database help

void CMicromodDeviceOptions::MakeIMPStringFromOldDB(void) {

	UINT uTotal = m_LSPList.GetCount();

	CStringArray List;

	CString sWork;

	m_IMPList.Empty();

	CString sDev = GetDeviceName();

	sDev.MakeLower();

	for( UINT i = 0; i < uTotal; i++ ) {

		UINT uTypeNum;

		CString sTypeAlpha = MakeOldDBDataType(i, &uTypeNum);

		sWork.Printf( L"%d,%s.,,%s,%s,,,,,,%s,%d,,%d,Y,%s,%s,N,,,Y,RW,,%d",

			i,
			sDev,
			m_ALSList.GetAt(i),
			m_LSPList.GetAt(i),
			sTypeAlpha,
			uTypeNum,
			tatoi(m_SZEList.GetAt(i)),
			m_OFFList.GetAt(i),
			m_BITList.GetAt(i),
			i
			);

		m_OldDBLoadList.Append(sWork);
		}
	}

CString CMicromodDeviceOptions::MakeOldDBDataType(UINT uPosition, UINT * pTypeNum) {

	BOOL fBit = tatoi(m_BITList.GetAt(uPosition)) < 16;

	UINT uSze = tatoi(m_SZEList.GetAt(uPosition));

	CString sALS = m_ALSList.GetAt(uPosition);

	if( uSze == 1 ) {

		if( fBit ) {

			*pTypeNum = 3;
			return L"BIT";
			}
		}

	if( uSze == 4 ) {

		if( sALS.Left(3) == L"SEQ" ) {	// only known exception to size = 4 not a float

			*pTypeNum = 12;

			return L"MSEC TIME";
			}

		*pTypeNum = 10;

		return L"FLOAT PT";
		}

	*pTypeNum = 9;

	return L"LONG STATE";	// generic
	}

void CMicromodDeviceOptions::CompleteOldDBFields(void) {

	UINT uTotal = m_NewIMPList.GetCount();

	if( uTotal ) {

		m_IMPList.Empty();

		for( UINT i = 0; i < uTotal; i++ ) {

			m_IMPList.Append(L"");
			}

		CStringArray List;

		CString sWork;

		for( UINT i = 0; i < uTotal; i++ ) {

			sWork = m_NewIMPList.GetAt(i);

			List.Empty();

			sWork.Tokenize(List, ',');

			List.SetAt(ALIASPOS, MakeNameString(List[ALIASPOS]));

			UINT uTagged = (UINT)tatoi(List[ENTRYPOS]) | 0xFF0000;

			sWork.Printf( L"%d", uTagged);

			List.SetAt(ENTRYPOS, sWork);

			sWork = UpdateIMPString(List);

			m_IMPList.SetAt(i, sWork);
			}

		AddToMaps();

		m_NewIMPList.Empty();
		}
	}

void CMicromodDeviceOptions::AddToMaps(void) {

	UINT uTotal = m_IMPList.GetCount();

	CStringArray List;

	for( UINT i = 0; i < uTotal; i++ ) {

		CString sWork = m_IMPList.GetAt(i);

		List.Empty();

		sWork.Tokenize(List, ',');

		sWork = List[ALIASPOS];

		CAddress Addr;

		Addr.a.m_Table	= SP_65;
		Addr.a.m_Extra	= 0;
		Addr.a.m_Offset	= tatoi(List[NUMPOS]);

		switch( tatoi(List[NTYPPOS]) ) {

			case 3:
				Addr.a.m_Type = addrBitAsBit;
				break;

			case 10:
				Addr.a.m_Type = addrRealAsReal;
				break;

			default:
				Addr.a.m_Type = addrLongAsLong;
				break;

			}

		m_TagsFwdMap.Insert(sWork, Addr.m_Ref);
		m_TagsRevMap.Insert(Addr.m_Ref, sWork);
		}
	}

void CMicromodDeviceOptions::EmptyOldLists(void) {

	m_LSPList.Empty();
	m_OFFList.Empty();
	m_SZEList.Empty();
	m_BITList.Empty();
	m_ALSList.Empty();
	}

// other import help
BOOL CMicromodDeviceOptions::ConfirmImport(CWnd *pWnd) {

	CString Text =	TEXT("Aliases are present.\r\n")
			TEXT("Do you wish to import another list?");

	return pWnd->YesNo(Text) == IDYES;
	}

BOOL CMicromodDeviceOptions::ImportAliasFile(FILE *pFile, CString sType) {

	UINT n = ImportMIF(pFile);

	m_uTotalCount = n;

	SortIMPListByFG(FALSE);

	if( !m_fMakeTags ) {
	
		RebuildMaps();
		}

	else {
		SortIMPListByFG(TRUE);	// reassign offsets if necessary
		}

	CString status;

	status.Printf( L"%d Aliases Imported ", n );

	afxThread->SetStatusText(status);

	return TRUE;
	}

UINT CMicromodDeviceOptions::ImportMIF(FILE *pFile) {

	DWORD dwPos = ftell(pFile);

	UINT uLine  = 0;

	fseek(pFile, dwPos, SEEK_SET);

	BOOL fDoneHeader = FALSE;

	UINT n = 0;

	while( !feof(pFile) ) {

		char sIn[1024] = {0};

		fgets(sIn, sizeof(sIn), pFile);

		UINT uLen = strlen(sIn);

		if( uLen ) {

			if( isdigit(sIn[0]) ) {

				while( sIn[uLen - 1] < ' ' ) {

					sIn[uLen - 1] = 0;

					uLen = strlen(sIn);
					}
				}
			}

		if( !uLen ) {

			continue;
			}

		CString sLine = CString(sIn);

		if( !fDoneHeader ) {

			fDoneHeader = DoneHeader(sIn, &uLine);

			continue;
			}

		if( sLine.GetLength() ) {

			sLine = ClearUnwantedSpaces(sLine);

			if( m_uOverwrite == NOTHING ) {

				AddOldIMPLine(sLine, n);
				}

			else if( m_uOverwrite ) {

				if( m_fImportOverwrite ) {

					AddIMPLineOverwrite(sLine, n);
					}

				else {
					AddIMPLineNoOverwrite(sLine, n);
					}
				}

			else {
				AddIMPLineAppend(sLine, n);
				}

			CStringArray List;

			List.Empty();

			sLine.Tokenize(List, ',');

			CString status;

			status.Printf( L"Imported %s ", MakeNameString(List[ALIASPOS]) );

			afxThread->SetStatusText(status);

			n++;
			}
		}

	return n;
	}

CString CMicromodDeviceOptions::ClearUnwantedSpaces(CString sIn) {

	while( (sIn.Right(1))[0] <= ' ' || (sIn.Right(1))[0] == ',' ) {

		sIn = sIn.Left(sIn.GetLength() - 1);

		if( sIn.GetLength() <= 1 ) {

			return L"";
			}
		}

	while( sIn[0] <= ' ' ) {

		sIn = sIn.Mid(1);

		if( sIn.GetLength() <= 1 ) {

			return L"";
			}
		}

	UINT uPos = 0;

	UINT uCnt = 0;

	while( uCnt < 24 && uPos < sIn.GetLength() - 1 ) {

		UINT uFind = (sIn.Mid(uPos)).Find(',');

		if( uFind < NOTHING ) {

			uCnt++;

			uFind += uPos;

			if( sIn[uFind + 1] == ' ' ) {

				if( uFind + 2 < sIn.GetLength() ) {

					CString sLeft	= sIn.Left(uFind);

					CString sRight	= uCnt < 24 ? sIn.Mid(uFind + 2) : L"";

					sIn.Printf( L"%s,%s", sLeft, sRight );
					}
				}
			}

		else {
			break;
			}

		uPos = uFind + 1;
		}

	return sIn;
	}

void CMicromodDeviceOptions::AddOldIMPLine(CString sLine, UINT uLine) {

	CStringArray List;

	List.Empty();

	sLine.Tokenize(List, ',');

	if( !List.GetCount() ) {

		return;
		}

	UINT uCurrSize = m_NewIMPList.GetCount();

	CString sWork  = sLine.Mid(sLine.Find(','));

	m_OldIDList.SetAt(uCurrSize, LOWORD(tatoi(sLine)));	// save original ID for later recompile

	sWork.Printf( L"%4.4d%s", uCurrSize, sWork);		// add next ID number to saved string

	sWork = AddTagPrefix(sWork);

	m_NewIMPList.Append(sWork);

	return;
	}

void CMicromodDeviceOptions::AddIMPLineOverwrite(CString sLine, UINT uLine) {

	CStringArray List;

	List.Empty();

	sLine.Tokenize(List, ',');

	if( !List.GetCount() ) {

		return;
		}

	HandleOverwrite(List);
	}

void CMicromodDeviceOptions::AddIMPLineNoOverwrite(CString sLine, UINT uLine) {

	CStringArray List;

	List.Empty();

	sLine.Tokenize(List, ',');

	if( !List.GetCount() ) {

		return;
		}

	CString sWork;

	sWork = List[ALIASPOS];		// Alias of imported line

	for( UINT i = 0; i < m_IMPList.GetCount(); i++ ) {

		if( m_IMPList[i].Find(sWork) < NOTHING ) {	// user rejected overwrite when Alias matches existing one

			return;
			}
		}

	sLine.Printf( L"%4.4d", MakeNewOffset() );	// make 4 digit number for sort algorithm

	List.SetAt(NUMPOS, sLine);

	HandleOverwrite(List);

	return;
	}

void CMicromodDeviceOptions::AddIMPLineAppend(CString sLine, UINT uLine) {

	CStringArray List;

	List.Empty();

	sLine.Tokenize(List, ',');

	if( !List.GetCount() ) {

		return;
		}

	CString sWork;

	sLine.Printf( L"%4.4d", MakeNewOffset() );	// make 4 digit number for sort algorithm

	List.SetAt(NUMPOS, sLine);

	sLine = MakeNameString(List[ALIASPOS]);

	List.SetAt(ALIASPOS, sLine);

	IMPListAppend(UpdateIMPString(List));
	}

CString CMicromodDeviceOptions::AddTagPrefix(CString sLine) {

	CStringArray List;

	sLine.Tokenize(List, ',');

	CString s = m_TagPrefix + L"_";

	UINT uLen = s.GetLength();

	if( uLen > 1 ) {

		if( List[ALIASPOS].Left(uLen) != s ) {

			s.Printf( L"%s%s", s, List.GetAt(ALIASPOS) );

			List.SetAt(ALIASPOS, s);

			return UpdateIMPString(List);
			}
		}

	return sLine;
	}

BOOL CMicromodDeviceOptions::HandleOldImport(CWnd *pWnd) {

	if( !m_LSPList.GetCount() ) {

		return FALSE;	// do regular import
		}

	CString s =	TEXT("You are about to import a file to update information about existing tags.\n")
			TEXT("This must be the original file used to create that database, or there may be tags that are mapped incorrectly.\n")
			TEXT("New tags will not be made, but all the Aliases will be imported\n\n");

	s += S_MMQUERY;

	if( pWnd->YesNo(s) == IDYES ) {

		m_uOverwrite = NOTHING;

		m_NewIMPList.Empty();

		Import(pWnd);

		CompleteOldDBFields();

		s = TEXT("When all imports are done, you must choose \"File\\Utilities\\Rebuild Comms Blocks\"");

		pWnd->Information( s );

		m_uOverwrite = 0;
		}

	return TRUE;
	}

// Import functions
BOOL CMicromodDeviceOptions::DoneHeader(char * sIn, UINT *pLine) {

	UINT uLine = *pLine;

	CString sLine = CString(sIn);

	CStringArray List;

	List.Empty();

	switch( uLine ) {

		case 0:
			if( IsControlLine(sLine, "MIF") || IsControlLine(sLine, "CSV") ) {

				*pLine = 5;
				}

			return FALSE;

		case 5:
			return IsControlLine(sLine, "Alias");
		}

	return FALSE;
	}

// helpers
BOOL CMicromodDeviceOptions::IsControlLine(CString sLine, CString sMatch) {

	CString A = sLine;
	CString B = sMatch;

	A.MakeUpper();
	B.MakeUpper();

	return A.Find(B) < NOTHING;
	}

void CMicromodDeviceOptions::HandleOverwrite(CStringArray List) {

	if( !m_OldNameList.GetCount() ) {

		SaveOldIMPNames();
		}

	CString sAlias1	= List.GetAt(ALIASPOS);

	CString sAlias2	= MakeNameString(sAlias1);

	UINT uPos = m_OldNameList.Find(sAlias1);

	if( uPos == NOTHING ) {

		uPos = m_OldNameList.Find(sAlias2);
		}

	if( uPos < NOTHING ) {

		List.SetAt(ALIASPOS, sAlias2);					// update existing name in List

		CString sIMP = m_IMPList.GetAt(uPos);

		List.SetAt(ENTRYPOS, sIMP.Mid(sIMP.FindRev(',') + 1));		// carry over the "mapped" property

		m_IMPList.SetAt(uPos, UpdateIMPString(List));			// write updated string

		return;
		}

	List.SetAt( ALIASPOS, sAlias2 );					// update new name in List

	List.SetAt( NUMPOS,   CPrintf("%4.4d", MakeNewOffset()) );		// new offset in list

	List.SetAt( ENTRYPOS, CPrintf("%d", tatoi(List.GetAt(NUMPOS)) & 0xFFFF) );	// set "not mapped"

	m_IMPList.Append(UpdateIMPString(List));
	}

UINT CMicromodDeviceOptions::GetDataType(CString sType) {

	if( IsControlLine(sType, "DISC" ) ) {

		return 0;
		}

	return IsControlLine(sType, "FLOAT") ? 0xE : 0xC;
	}

CString CMicromodDeviceOptions::MakeNameString(CString sName) {

	CString s = sName;

	UINT    u = m_TagPrefix.GetLength();

	if( u ) {

		if( s.Find('_') != u || s.Find(m_TagPrefix) == NOTHING ) {

			s.Printf( L"%s_%s",

				m_TagPrefix,
				s
				);
			}
		}

	return s;
	}

BOOL CMicromodDeviceOptions::IsNewAlias(UINT uSel) {

	return !(BOOL)HIWORD(GetIMPIntItem(uSel, ENTRYPOS));
	}

CString CMicromodDeviceOptions::GetIMPStringItem(UINT uSel, UINT uItem) {

	CStringArray List;

	List.Empty();

	CString s = m_IMPList.GetAt(uSel);

	s.Tokenize(List, ',');

	return List[uItem];
	}

UINT CMicromodDeviceOptions::GetIMPIntItem(UINT uSel, UINT uItem) {

	return tatoi(GetIMPStringItem(uSel, uItem));
	}

BOOL CMicromodDeviceOptions::ShowOldDBWarning(CWnd *pWnd, BOOL fQuitAnyway) {

	CString s;

	if( m_LSPList.GetCount() ) {

		s.Printf( L"%s%s", S_MMWARN1, S_MMWARN2);

		if( !fQuitAnyway ) {

			s += S_MMWARN3;
			s += S_MMQUERY;
			}

		else {
			pWnd->Information(s);

			return FALSE;
			}

		return pWnd->YesNo(s) == IDYES;
		}

	return TRUE;
	}

BYTE CMicromodDeviceOptions::CheckOldDB(void) {

	BYTE b = m_fIsOldDB ? 1 : 0;

	if( m_fWasOldDB ) {

		b |= 2;
		}

	return b;
	}

BOOL CMicromodDeviceOptions::OkToReplaceQuery(CWnd *pWnd) {
	
	CString Text =	TEXT("You are about to make changes to the device configuration \n")
			TEXT("that may invalidate data tag references in the database.\n\n")
			TEXT("If you choose to continue, Aliases may be replaced with \n")
			TEXT("data from the selected file and you must run the Rebuild Comms \n")
			TEXT("Blocks utility from the File\\Utilities menu after this operation is complete.\n\n")
			TEXT("Do you want to continue?");

	return pWnd->YesNo(Text) == IDYES;
	}

BOOL CMicromodDeviceOptions::OkToKeepQuery(CWnd *pWnd) {
	
	CString Text =	TEXT("Do you wish to REMOVE all existing Aliases while importing?\n")
			TEXT("If No, and matching Aliases are found, you will have the option of keeping or overwriting the properties.\n")
			TEXT("If Yes, you will be asked to confirm you want to remove existing Aliases.");

	return pWnd->YesNo(Text) == IDYES;
	}

BOOL CMicromodDeviceOptions::OkToRemoveQuery(CWnd *pWnd) {
	
	CString Text =	TEXT("Select Yes to REMOVE all existing Aliases before importing.");

	return pWnd->YesNo(Text) == IDYES;
	}

BOOL CMicromodDeviceOptions::OkToOverwriteQuery(CWnd *pWnd) {
	
	CString Text =	Text =	TEXT("Do you wish to OVERWRITE the properties of the Alias if it is present?");

	return pWnd->YesNo(Text) == IDYES;
	}

BOOL CMicromodDeviceOptions::OkFinalQuery(CWnd *pWnd) {

	CString Text;

	if( m_fImportOverwrite ) {

		Text =	TEXT("Select Yes to overwrite the properties of matching Aliases and continue.");
		}

	else {
		Text =	TEXT("Select Yes to KEEP the properties of matching Aliases and continue.");
		}

	return pWnd->YesNo(Text) == IDYES;
	}

// Meta Data Creation

void CMicromodDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Database);
	Meta_AddInteger(UnlockGNR);
	Meta_AddInteger(ArraySelect);
	Meta_AddInteger(ArrayValue);
	Meta_AddString (ArrayString);
	Meta_AddString (TagPrefix);
	Meta_AddInteger(Push);
	}

//////////////////////////////////////////////////////////////////////////
//
// Micromod TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMicromodTCPDeviceOptions, CUIItem);       

// Constructor

CMicromodTCPDeviceOptions::CMicromodTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 502;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_Drop		= 1;

	m_Database	= 0;

	m_UnlockGNR	= 0;

	m_TagPrefix	= "T1";

	CMicromodDeviceOptions::m_ArraySelect	= 0;

	m_fIsOldDB	= FALSE;
	m_fWasOldDB	= FALSE;

	m_ArraySelect	= 0;
	m_ArrayValue	= 0;
	m_ArrayString	= TEXT("");

	m_IMPList.Empty();
	m_LSPList.Empty();
	}

// UI Management

void CMicromodTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	m_pViewWnd = pWnd;

	if( pItem == this ) {

		if( Tag == "Push" ) {

			if( m_fIsOldDB ) {

				CString s1 =	TEXT("You have loaded a database from a previous version of the software.\r\n")
						TEXT("You must import the original MIF file for the device ");

				CString s2 =	TEXT("Do you wish to import the MIF now?");

				s1.Printf( L"%s%s.\r\n\n%s", s1, GetDeviceName(), s2);

				if( pWnd->YesNo(s1) == IDNO ) {

					return;
					}

				m_Push = 1;
				}

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					OnImport(pWnd);
					break;

				case 2:
					OnExport(pWnd);
					break;
				}
			}

		if( Tag.IsEmpty() || Tag == TEXT("Keep") ) {

			pWnd->EnableUI(TEXT("Time3"), !m_Keep);
			}
		}
	}

// Download Support

BOOL CMicromodTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	if( m_ArraySelect ) {

		m_ArrayString = DoTCPAccess(m_ArraySelect, &m_ArrayValue, m_ArrayString);

		m_ArraySelect = 0;

		return TRUE;
		}

	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Database));
	Init.AddLong(LONG(m_UnlockGNR));

	SortIMPListByOffset(FALSE);			// sort IMP List by Offset value

	UINT uCount = m_IMPList.GetCount();

	Init.AddWord(LOWORD(uCount + 1));		// include terminator in count

	CStringArray List;

	CString s;

	CArray <DWORD>	dLSP;
	CArray <WORD>	wFG;
	CArray <WORD>	wBit;
	CArray <WORD>	wSze;

	dLSP.Empty();
	wFG.Empty();
	wBit.Empty();
	wSze.Empty();

	UINT i;

	for( i = 0; i < uCount; i++ ) {	// ensure proper sequencing by initializing the arrays to 0

		dLSP.Append(0);
		wFG.Append(0);
		wBit.Append(0);
		wSze.Append(0);
		}

	for( i = 0; i < uCount; i++ ) {

		s = m_IMPList[i];

		List.Empty();

		s.Tokenize(List, ',');

		dLSP.SetAt(i, (DWORD)tatoi(List[NLSPPOS]));
		wFG.SetAt( i, LOWORD(tatoi(List[FGOFFPOS])));
		wSze.SetAt(i, min(4, LOWORD(tatoi(List[DLENPOS]))));

		UINT u = (UINT)tatoi(List[NTYPPOS]) == 3 ? (UINT)tatoi(List[FGBITPOS]) : 255;	// runtime uses 255 to signal non-bit
		wBit.SetAt(i, LOWORD(u));
		}

	for( i = 0; i < uCount; i++ ) {

		Init.AddLong((LONG)dLSP[i]);
		}

	Init.AddLong((unsigned long)0xFFFFFFFF);	// add Terminator

	for( i = 0; i < uCount; i++ ) {

		Init.AddWord((WORD)wFG[i]);
		}

	Init.AddWord((WORD)0xFFFF);			// add Terminator

	for( i = 0; i < uCount; i++ ) {

		Init.AddWord((WORD)wSze[i]);
		}

	Init.AddWord((WORD)0xFFFF);			// add Terminator

	for( i = 0; i < uCount; i++ ) {

		Init.AddWord((WORD)wBit[i]);
		}

	Init.AddWord((WORD)0xFFFF);			// add Terminator

	return TRUE;
	}

// Meta Data Creation

void CMicromodTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
 	Meta_AddInteger(Drop);
	Meta_AddInteger(Database);
	Meta_AddInteger(UnlockGNR);
	Meta_AddInteger(ArraySelect);
	Meta_AddInteger(ArrayValue);
	Meta_AddString (ArrayString);
	Meta_AddString (TagPrefix);
	Meta_AddInteger(Push);
      	}

//////////////////////////////////////////////////////////////////////////
//
// Micromod Drivers
//

// Instantiator

ICommsDriver *	Create_MicromodDriver(void)
{
	return New CMicromodDriver;
	}

// Constructor

CMicromodDriver::CMicromodDriver(void)
{
	m_wID		= 0x4051;

	m_uType		= driverMaster;
	
	m_Manufacturer	= TEXT("MicroMod");
	
	m_DriverName	= TEXT("Extended Modbus");
	
	m_Version	= TEXT("2.00");
	
	m_ShortName	= TEXT("Micromod Extended Modbus");

	m_DevRoot	= TEXT("MOD");

	AddSpaces();  
	}

// Binding Control

UINT CMicromodDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMicromodDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CMicromodDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMicromodDeviceOptions);
	}

// Driver Data

UINT CMicromodDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagImport;
	}

// Tag Import

BOOL CMicromodDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	CMicromodDeviceOptions * pOpt = (CMicromodDeviceOptions *)pConfig;

	if( pOpt ) {

		CViewWnd *pWnd = pOpt->GetView();

		if( pOpt->CheckOldDB() & 1 ) {

			pWnd->Information("A database from a previous version has been loaded, and the original MIF file for the device must be imported first.");

			return TRUE;
			}

		if( !pOpt->ShowOldDBWarning(pWnd, TRUE) ) {

			return TRUE;
			}

		CString Text;

		pOpt->SetTagPointer(pTags);

		m_pMMConfig = pOpt;

		BOOL fDoImport = TRUE;

		UINT uCount = GetArrayCNT();

		if( uCount ) {

//			Text =	TEXT("Aliases are present in this device\n")
//				TEXT("Do you wish to create new tag names for any existing aliases that\n")
//				TEXT("that have been imported via the Import button but are not yet mapped?");

//			if( pWnd->YesNo(Text) == IDYES ) {

//				fDoImport = FALSE;
//				}

//			else {
				Text =	TEXT("This operation will delete all aliases and tags for the device.\n")
					TEXT("It will be necessary to run \"File\\Utilities\\Rebuild Comms Blocks\" when finished.\n\n")
					TEXT("Do you wish to continue?");

				if( pWnd->YesNo(Text) == IDNO ) {

					return TRUE;
					}

				m_pMMConfig->m_IMPList.Empty();
//				}
			}

		if( fDoImport ) {

			if( pOpt->CheckOldDB() ) {

				Text = L"";
				}

			else {
				Text = pOpt->GetDeviceName();
				}

			COpenFileDialog Dlg;

			Dlg.LoadLastPath(TEXT("Micromod Tag Names"));

			Dlg.SetCaption(CPrintf("Import Parameters for %s", pOpt->GetDeviceName()));

			Dlg.SetFilter(TEXT("Micromod MIF Files|*.MIF|CSV Files (*.csv)|*.csv"));

			if( Dlg.Execute(*afxMainWnd) ) {

				FILE *pFile;
		
				if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

					Text = TEXT("Unable to open file for reading.");

					afxMainWnd->Error(Text);
					}
				else {
					CString File = Dlg.GetFilename();

					if( pTags->InitImport(Text, FALSE, File) ) {

						CString sType = Dlg.GetFilename().GetType();

						if( pOpt->Import(pFile, sType, pTags) ) {

							pOpt->SetDirty();
							}

						pOpt->FinishMakeTags(TRUE, TRUE);

						pTags->ImportDone(TRUE);

						Dlg.SaveLastPath(TEXT("Micromod Tag Names"));
			
						fclose(pFile);

						return TRUE;
						}
					}
				}

			else {
				return FALSE;
				}
			}

		else {	// try to figure out why new tags don't get created (try rebuilding)
			pOpt->CreateTagsFromIMP();
			}
		}

	return FALSE;
	}

// Address Management

BOOL CMicromodDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pMMConfig = (CMicromodDeviceOptions *)pConfig;

	if( m_pMMConfig ) {

		return m_pMMConfig->SelectAddress(hWnd, Addr, fPart);
		}

	return FALSE;
	}

// Address Helpers
BOOL CMicromodDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text) {

	m_pMMConfig = (CMicromodDeviceOptions *)pConfig;

	CSpace *pSpace = GetSpace(Text);

	CString sParse = Text;

	if( pSpace ) {

		sParse = sParse.Mid(pSpace->m_Prefix.GetLength());

		return DoParseAddress(Error, Addr, pConfig, pSpace, sParse);
		}

	// if the space is not found, then creating tags from basic param name
	if( m_pMMConfig ) {

		return m_pMMConfig->ParseAddress(Error, Addr, sParse);
		}

	return FALSE;
	}

BOOL CMicromodDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	m_pMMConfig = (CMicromodDeviceOptions *)pConfig;

	UINT uTable  = pSpace->m_uTable;

	if( uTable < SP_65 || uTable == SP_RDB ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	if( uTable == SP_RDBO ) {

		Addr.a.m_Table  = uTable;
		Addr.a.m_Offset = 0;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = LL;

		return TRUE;
		}

	return m_pMMConfig->ParseAddress(CError(FALSE), Addr, Text);
	}

BOOL CMicromodDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	m_pMMConfig = (CMicromodDeviceOptions *)pConfig;

	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uTable = Addr.a.m_Table;

		if( uTable < SP_65 ) {

			return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		if( uTable == SP_65 ) {

			CAddress A;

			A.m_Ref = Addr.m_Ref;

			if( Addr.a.m_Offset >= m_pMMConfig->m_IMPList.GetCount() ) {	// an old database was imported

				A.a.m_Offset = 0;
				}

			if( m_pMMConfig->ExpandAddress(Text, A) ) {

				Text.Printf( L"A_%s", Text);

				return TRUE;
				}

			return FALSE;
			}

		if( uTable != SP_RDBO ) {

			return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		if( uTable == SP_RDBO ) {  // print only prefix

			Text.Printf( TEXT("%s"), pSpace->m_Prefix );

			return TRUE;
			}

		CString sType = Addr.a.m_Type == addrLongAsLong ?  TEXT("") : TEXT(".REAL");

		Text.Printf( TEXT("%s%d%s"),

			pSpace->m_Prefix,
			Addr.a.m_Offset,
			sType
			);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CMicromodDriver::ListAddress( CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data) {

	return ((CMicromodDeviceOptions *)pConfig)->ListAddress(pRoot, uItem, Data);
	}

BOOL CMicromodDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 3:
		case 4:
			return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodDriver::IsFunc65(UINT uTable)
{
	return uTable >= SP_65 && uTable < SP_ERR;
	}

BOOL CMicromodDriver::Is65Command(UINT uTable)
{
	return uTable == SP_RDB || uTable == SP_RDBO;
	}

// Array Access
DWORD CMicromodDriver::GetLSPFromList(UINT uItem)
{
	DoArraySelect(RARRLSP, uItem);

	return GetArrayValue();
	}

WORD CMicromodDriver::GetFGFromList(UINT uItem)
{
	DoArraySelect(RARRFG, uItem);

	return LOWORD(GetArrayValue());
	}

WORD CMicromodDriver::GetSZEFromList(UINT uItem)
{
	DoArraySelect(RARRSZE, uItem);

	return LOWORD(GetArrayValue());
	}

WORD CMicromodDriver::GetBITFromList(UINT uItem)
{
	DoArraySelect(RARRBIT, uItem);

	return LOWORD(GetArrayValue());
	}

WORD CMicromodDriver::GetTYPFromList(UINT uItem) {

	DoArraySelect(RARRTYP, uItem);

	return LOWORD(GetArrayValue());
	}

CString CMicromodDriver::GetNAMFromList(UINT uItem) {

	DoArraySelect(RARRNAM, uItem);

	return GetArrayString();
	}

CString CMicromodDriver::GetIMPLine(UINT uItem) {

	DoArraySelect(GIMPSTR, uItem);

	return GetArrayString();
	}

UINT CMicromodDriver::GetBlkFromLSP(DWORD dLSP)
{
	return (UINT)(9 + ((dLSP & LSP_BLK_MASK) >> LSP_BLK_POS));
	}

UINT CMicromodDriver::GetOccFromLSP(DWORD dLSP)
{
	return (UINT)(dLSP & LSP_OCC_MASK) >> LSP_OCC_POS;
	}

DWORD CMicromodDriver::GetArrayLSP(UINT uItem)
{
	DoArraySelect(RARRLSP, uItem);

	return GetArrayValue();
	}

WORD CMicromodDriver::GetArrayFG(UINT uItem)
{
	DoArraySelect(RARRFG, uItem);

	return LOWORD(GetArrayValue());
	}

WORD CMicromodDriver::GetArraySZE(UINT uItem)
{
	DoArraySelect(RARRSZE, uItem);

	return LOWORD(GetArrayValue());
	}

WORD CMicromodDriver::GetArrayBIT(UINT uItem)
{
	DoArraySelect(RARRBIT, uItem);

	return LOWORD(GetArrayValue());
	}

WORD CMicromodDriver::GetArrayTYP(UINT uItem) {

	DoArraySelect(RARRTYP, uItem);

	return LOWORD(GetArrayValue());
	}

WORD CMicromodDriver::GetArrayOFF(UINT uItem) {

	DoArraySelect(RARROFF, uItem);

	return LOWORD(GetArrayValue());
	}

CString CMicromodDriver::GetArrayALSP(UINT uItem) {

	DoArraySelect(RARRALS, uItem);

	return GetArrayString();
	}

WORD CMicromodDriver::GetArrayCNT(void)
{
	DoArraySelect(RARRCNT, 0);

	return LOWORD(GetArrayValue());
	}

void CMicromodDriver::DoArraySelect(UINT uFunc, UINT uValue)
{
	SetArrayFunc(uFunc);

	SetArrayValue(uValue);

	CInitData Init;

	m_pMMConfig->MakeInitData(Init);
	}

void CMicromodDriver::SetArrayFunc(UINT uFunc)
{
	m_pMMConfig->GetDataAccess(TEXT("ArraySelect"))->WriteInteger(m_pMMConfig, uFunc);
	}

void CMicromodDriver::SetArrayValue(DWORD dValue)
{
	m_pMMConfig->GetDataAccess(TEXT("ArrayValue"))->WriteInteger(m_pMMConfig, dValue);
	}

void CMicromodDriver::SetArrayString(CString sStr) {

	m_pMMConfig->GetDataAccess(TEXT("ArrayString"))->WriteString(m_pMMConfig, sStr);
	}

DWORD CMicromodDriver::GetArrayValue(void)
{
	return m_pMMConfig->GetDataAccess(TEXT("ArrayValue"))->ReadInteger(m_pMMConfig);
	}

CString CMicromodDriver::GetArrayString(void) {

	return m_pMMConfig->GetDataAccess(TEXT("ArrayString"))->ReadString(m_pMMConfig);
	}

void CMicromodDriver::SetMMConfigPtr(CItem *pMMConfig)
{
	m_pMMConfig = (CMicromodDeviceOptions *)pMMConfig;
	}

// Implementation

void CMicromodDriver::AddSpaces(void)
{
	// Micromod Modbus Function Code 65 Blocks
	AddSpace(New CSpace(SP_65,	"A_",		"Attribute",				10, 0,1023, addrBitAsBit, addrRealAsReal));

	AddSpace(New CSpace(SP_RDB,	"RDB_",		"Read Database Command",		10, 0,   9,     LL));
	AddSpace(New CSpace(SP_RDBO,	"RDBO",		"   Database Offset for RDB",		10, 0,   0,     LL));

	AddSpace(New CSpace(3,		"0",  "Digital Coils",			10, 1, 65535, addrBitAsBit));
	
	AddSpace(New CSpace(4,		"1",  "Digital Inputs",			10, 1, 65535, addrBitAsBit));
	
	AddSpace(New CSpace(2,		"3",  "Analog Inputs",			10, 1, 65535, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(1,		"4",  "Holding Registers",		10, 1, 65535, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(5,		"L4", "Holding Registers (32-bit)",	10, 1, 65535, addrLongAsLong, addrLongAsReal));

	AddSpace(New CSpace(SP_ERR,	"ERR",		"LSP of Latest Data Error",		10, 0,    0,	LL));
	AddSpace(New CSpace(SP_NAK,	"NAK",		"NAK response",				10, 0,    0,	YY));
	}

// Helpers

UINT CMicromodDriver::GetTypeFromModifier(CString Type)
{
	return Type == TEXT("REAL") ? addrRealAsReal : Type == TEXT("BIT") ? addrBitAsBit : addrLongAsLong;
	}

UINT CMicromodDriver::CountNewAliases(void) {

	UINT uTotal = m_pMMConfig->m_IMPList.GetCount();

	UINT uCount = 0;

	CStringArray List;

	for( UINT i = 0; i < uTotal; i++ ) {

		List.Empty();

		CString s = m_pMMConfig->m_IMPList.GetAt(i);

		s.Tokenize(List, ',');

		if( !(BOOL)HIWORD(tatoi(List[ENTRYPOS])) ) {

			uCount++;
			}
		}

	return uCount;
	}

//////////////////////////////////////////////////////////////////////////
//
// Micromod TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_MicromodTCPDriver(void)
{
	return New CMicromodTCPDriver;
	}

// Constructor

CMicromodTCPDriver::CMicromodTCPDriver(void)
{
	m_wID		= 0x353C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= TEXT("Micromod");
	
	m_DriverName	= TEXT("TCP/IP Master");
	
	m_Version	= TEXT("2.00");
	
	m_ShortName	= TEXT("Micromod TCP/IP Master");

	m_DevRoot	= TEXT("MOD");
	}

// Destructor

CMicromodTCPDriver::~CMicromodTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CMicromodTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CMicromodTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CMicromodTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMicromodTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////////////////
//
// Micromod Address Selection
//
//

// Runtime Class

AfxImplementRuntimeClass(CMicromodAddrDialog, CStdAddrDialog);
		
// Constructor

CMicromodAddrDialog::CMicromodAddrDialog(CMicromodDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_pMDriver	= &Driver;

	m_fPart		= fPart;

	m_pMMConfig	= (CMicromodDeviceOptions *)pConfig;

	m_uHasLSP	= 0;

	m_fDoAll	= FALSE;

	m_fDoNotEnd	= FALSE;

	m_uCurrSel	= 0;

	m_fSelect	= FALSE;

	m_pMDriver->SetMMConfigPtr(pConfig);

	SetName(TEXT("MicromodElementDlg"));
	}

// Initialisation

void CMicromodAddrDialog::SetCaption(CString const &Text)
{
	m_Caption = Text;
	}

void CMicromodAddrDialog::SetSelect(BOOL fSelect)
{
	m_fSelect = fSelect;
	}

// Message Map

AfxMessageMap(CMicromodAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSpaceChange)

	AfxDispatchNotify(1051, LBN_SELCHANGE, OnSelectionChange)

	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxDispatchCommand(VINSERT, OnButtonClicked)
	AfxDispatchCommand(VREPLCE, OnButtonClicked)
	AfxDispatchCommand(VDELETE, OnButtonClicked)
	AfxDispatchCommand(VLOADID, OnButtonClicked)
	AfxDispatchCommand(LHEXSEL, OnButtonClicked)
	AfxDispatchCommand(EDBYLSP, OnButtonClicked)
	AfxDispatchCommand(EDCBVAL, OnButtonClicked)

	AfxMessageEnd(CStdAddrDialog)
	};

//////////////////////////////////////////////////////////////////////////
//
// Micromod Address Selection
//

MMBLOCKNAMES CMicromodAddrDialog::BlockNameList[] = {

	{  0, "--"},
	{  1, "IF"},
	{  2, "SE"},
	{  3, "ICN"},
	{  4, "OMC"},
	{  5, "ES"},
	{  6, "LP"},
	{  7, "CL"},
	{  8, "08"},
	{  9, "NM"},
	{ 10, "TM"},
	{ 11, "TOT"},
	{ 12, "LN"},
	{ 13, "PW"},
	{ 14, "PA"},
	{ 15, "EX"},
	{ 16, "IC"},
	{ 17, "OC"},
	{ 18, "DI"},
	{ 19, "VCI"},
	{ 20, "CJI"},
	{ 21, "TI"},
	{ 22, "TTI"},
	{ 23, "DIM"},
	{ 24, "DOM"},
	{ 25, "DDOM"},
	{ 26, "WDOM"},
	{ 27, "VCIM"},
	{ 28, "CJIM"},
	{ 29, "TIM"},
	{ 30, "AOM"},
	{ 31, "RTI"},
	{ 32, "RIM"},
	{ 33, "RTTI"},
	{ 34, "WRIM"},
	{ 35, "PID"},
	{ 36, "RI"},
	{ 37, "MSC"},
	{ 38, "ML"},
	{ 39, "SM"},
	{ 30, "SEQ"},
	{ 41, "RIO"},
	{ 42, "RDIM"},
	{ 43, "RDOM"},
	{ 44, "44"},
	{ 45, "45"},
	{ 46, "46"},
	{ 47, "47"},
	{ 48, "AIN"},
	{ 49, "AOUT"},
	{ 50, "DISP"},
	{ 51, "DIF"},
	{ 52, "PAD"},
	{ 53, "TL"},
	{ 54, "ST"},
	{ 55, "TMPL"},
	{ 56, "RSK"}
	};

// Message Handlers

BOOL CMicromodAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( m_pMMConfig->CheckOldDB() & 1 ) {

		Information("A database from a previous version has been loaded, and the original MIF file for the device must be imported first.");

		EndDialog(0);

		return TRUE;
		}

	CAddress &Addr	= (CAddress &) *m_pAddr;

	if( Addr.a.m_Table && Addr.a.m_Table != SP_65 ) {

		m_fSelect = TRUE;
		}

	GetDlgItem(IDOK).SetWindowTextW( m_fSelect ? CString("OK") : CString("Close") );

	if( m_pMMConfig->m_LSPList.GetCount() ) {

		CString s;

		s.Printf( L"%s%s%s%s", S_MMWARN1, S_MMWARN2, S_MMWARN3, S_MMQUERY );

		if( YesNo(s) == IDNO ) {

			return TRUE;
			}
		}

	m_fCheck	= FALSE;

	m_fEHexSel	= FALSE;

	SetEHexSel();

	m_fELSPSel	= TRUE;

	SetELSPSel();

	m_pBList	= BlockNameList;

	LoadNames();

	LoadTagNameList();

	if( !m_fPart ) {

		CStdAddrDialog::SetCaption();

		SetDlgFocus(1001);

		FindSpace();

		LoadList();

		InitEdit(Addr);

		if( m_pSpace ) {

			m_uHasLSP = HasLSP(m_pSpace->m_uTable);

			if( m_uHasLSP ) {

				GetDlgItem(LBTAGS).EnableWindow(TRUE);

				UINT uItem = GetItemFromOffset(Addr.a.m_Offset);

				if( uItem == NOTHING ) {

					Addr.a.m_Offset = 0;

					uItem = 0;
					}

				m_uCurrSel = uItem;

				SetLBoxPosition(LBTAGS, uItem);

				OnSelectionChange(LBTAGS, Focus);

				DoShow(m_pSpace->m_uTable, Addr);

				return FALSE;
				}

			CStdAddrDialog::ShowAddress(Addr);

			GetDlgItem(ALSVAL).EnableWindow(FALSE);

			GetDlgItem(ALSTXT).EnableWindow(FALSE);

			SetLSPEnables(FALSE);
			}

		ClearTagDItems();

		GetDlgItem(LBTAGS).EnableWindow(FALSE);

		return TRUE;
		}
	else {
		FindSpace();

		LoadList();

		LoadType();

		InitEdit(Addr);

		m_uHasLSP = HasLSP(Addr.a.m_Table);

		GetDlgItem(LBTAGS).EnableWindow((BOOL)m_uHasLSP);

		if( (BOOL)m_uHasLSP ) {

			m_uCurrSel = (UINT)Addr.a.m_Offset;

			OnSelectionChange(LBTAGS, Focus);
			}

		DoShow(Addr.a.m_Table, Addr);

		if( m_pSpace ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);
			
			if( !m_pMDriver->ParseAddress(Error, Addr, m_pMMConfig, Text) ) {

				m_pSpace->GetMinimum(Addr);
				}

			DoShow(Addr.a.m_Table, Addr);
			}

		SetAddressFocus();

		return FALSE;
		}
	}

// Notification Handlers

void CMicromodAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pMDriver->GetSpace(Index);

		if( m_pSpace ) {

			LoadType();
			SetTypeFromArrayValue(addrLongAsLong);
			}

		EnableTagDisplay(FALSE);

		CAddress Addr;

		if( m_pSpace ) {

			UINT uTable = m_pSpace->m_uTable;

			if( GetArrayCNT() && (m_uHasLSP = HasLSP(uTable)) ) {

				Addr.a.m_Table  = SP_65;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Type   = GetArrayTYP(0);

				Addr.a.m_Offset = LOWORD(GetOffsetFromItem(0));

				m_uCurrSel	= Addr.a.m_Offset;

				LoadTagNameList();

				GetDlgItem(LBTAGS).EnableWindow(TRUE);
				}

			else {
				GetDlgItem(CURRFG).EnableWindow(Is65Command(uTable));
				GetDlgItem(FGTXT ).EnableWindow(Is65Command(uTable));

				m_pSpace->GetMinimum(Addr);

				if( m_pSpace != pSpace ) {

					Addr.a.m_Type = GetTypeCode();
					}

				GetDlgItem(LBTAGS).EnableWindow(FALSE);
				}

			if( Addr.a.m_Table != SP_65 ) {

				GetDlgItem(ALSVAL).EnableWindow(FALSE);

				GetDlgItem(ALSTXT).EnableWindow(FALSE);

				m_fSelect = TRUE;
				}

			DoShow(Addr.a.m_Table, Addr);

			InitEdit(Addr);
			}

		else {
			m_pSpace = pSpace;

			LoadList();

			CListBox &Box = (CListBox &) GetDlgItem(1051);

			Box.SetRedraw(FALSE);

			Box.ResetContent();
			}
		}
	else {
		ClearType();

		ClearAddress();

		ClearDetails();

		SetLSPEnables(FALSE);

		EnableTagDisplay(FALSE);

		GetDlgItem(LBTAGS).EnableWindow(FALSE);

		m_pSpace = NULL;
		}
	}

void CMicromodAddrDialog::OnSelectionChange(UINT uID, CWnd &hWnd) {

	UINT uPos	= GetLBoxPosition(LBTAGS);

	m_uCurrSel	= uPos;

	LoadTagNameList();

	CAddress Addr;

	Addr.a.m_Table	= SP_65;
	Addr.a.m_Extra	= 0;
	Addr.a.m_Type	= m_pMDriver->GetTYPFromList(uPos);
	Addr.a.m_Offset	= LOWORD(tatoi(m_pMMConfig->m_IMPList[uPos]));

	DoShow( SP_65, Addr);

	InitEdit(Addr);
	}

// Command Handlers

BOOL CMicromodAddrDialog::OnOkay(UINT uID)
{
	if( !m_fSelect ) {

		EndDialog(TRUE);

		return TRUE;
		}

	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		Text += GetTypeText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pMDriver->ParseAddress(Error, Addr, m_pMMConfig, Text) ) {

			*m_pAddr = Addr;

			if( !m_fDoNotEnd ) {

				EndDialog(TRUE);
				}

			m_fDoNotEnd = FALSE;

			return TRUE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

void CMicromodAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	CString Text = L"";

	if( m_pSpace ) {

		ShowDetails();

		UINT u = GetTypeCode();

		switch( u ) {

			case addrBitAsBit:

				Text = L".Bit";

				GetDlgItem(EDSZE).SetWindowTextW(L"1");

				GetDlgItem(EDSZE).EnableWindow(FALSE);
				break;

			case addrLongAsLong:
				Text = L".Long";

				GetDlgItem(EDSZE).SetWindowTextW(L"4");

				GetDlgItem(EDSZE).EnableWindow(TRUE);
				break;

			case addrRealAsReal:
				Text = L".Real";

				GetDlgItem(EDSZE).SetWindowTextW(L"4");

				GetDlgItem(EDSZE).EnableWindow(FALSE);
				break;
			}
		}

	GetDlgItem(2003).SetWindowTextW(Text);
	}

BOOL CMicromodAddrDialog::OnButtonClicked(UINT uID)
{
	DWORD dCLSP = GetDlgEntry(CURRLSP);
	DWORD dELSP = GetDlgEntry(EDLSP);

	switch( uID ) {

		case LHEXSEL:	// select hex/decimal LSP display

			m_fEHexSel ^= 1;

			SetEHexSel();

			SetDlgEntry(CURRLSP, dCLSP);

			SetDlgEntry(EDLSP, dELSP);

			ShowALSPEditText();

			return TRUE;

		case EDBYLSP:

			m_fELSPSel ^= 1;

			SetELSPSel();

			EnableEdits(TRUE);

			ShowALSPEditText();

			return TRUE;

		case EDCBVAL:
			ShowALSPEditText();

			return TRUE;

		case VLOADID:	// display parameters of attribute in Current ID

			CAddress Addr;

			Addr.a.m_Table	= SP_65;
			Addr.a.m_Extra	= 0;

			UINT uItem = GetCURRIDValue();

			Addr.a.m_Type	= m_pMDriver->GetTYPFromList(uItem);

			UINT uCNT;
			uCNT = GetArrayCNT();

			MakeMin(uItem, uCNT - 1);

			Addr.a.m_Offset = LOWORD(GetOffsetFromItem(uItem));

			m_uCurrSel = uItem;

			DoShow(Addr.a.m_Table, Addr);

			ShowALSPEditText();

			return TRUE;
		}

	BOOL fHasList = GetArrayCNT();

	if( !fHasList ) {

		if( uID != VINSERT ) {

			return TRUE;
			}
		}

	CString sPre = L"";

	if( !m_fELSPSel ) {

		MakeALSPTextFromEdit();		// load new LSP into window
		}

	UINT uPos  = fHasList ? GetLBoxPosition(LBTAGS) : 0;

	m_uCurrSel = uPos;

	ShowALSPEditText();	// update data

	CStringArray List;

	List.Empty();

	CString sEFG = GetDlgItem(EDFG).GetWindowTextW();	// requested FG Offset position

	DWORD dLSP = tatoi(GetDlgItem(EDLSP).GetWindowTextW());

	if( !m_fELSPSel ) {

		UINT uBlk = GetCBoxPosition(EDCBVAL) << LSP_BLK_POS;

		if( !uBlk ) {

			Information(L"Invalid Block Selection");

			SetDlgFocus(EDCBVAL);

			return TRUE;
			}

		UINT uOcc = (tatoi(GetDlgItem(EDOCC).GetWindowTextW()) - 1) << LSP_OCC_POS;
		UINT uPar = (tatoi(GetDlgItem(EDPAR).GetWindowTextW()));

		dLSP = uOcc | uPar | uBlk;

		if( m_fEHexSel ) {

			sPre.Printf( L"%x", dLSP );
			}

		else sPre.Printf( L"%d", dLSP );

		GetDlgItem(EDLSP).SetWindowTextW(sPre);
		}

	if( !dLSP ) {

		Information(L"Invalid LSP value");

		SetDlgFocus(EDLSP);

		return TRUE;
		}

	CString sAlias = GetDlgItem(ALSVAL).GetWindowTextW();

	if( !sAlias.GetLength() ) {

		Information(L"Invalid Name");

		SetDlgFocus(ALSVAL);

		return TRUE;
		}

	DWORD dAddress = NOTHING;

	BOOL fExists = m_pMMConfig->NameExists(sAlias, &dAddress);

	if( fExists )  {

		if( YesNo(L"This name exists. Do you want to continue?") == IDNO ) {

			SetDlgFocus(ALSVAL);

			return TRUE;
			}
		}

	UINT uOSze = fHasList ? GetSZEFromList(uPos) : 0;

	switch( uID ) {

		case VREPLCE:

			if( fExists ) {

				if( LOWORD(dAddress) != uPos ) {

					Information(L"The same name exists at a different position.");

					return TRUE;
					}
				}

			UINT uMove;

			uMove = GetTypeCode();

			if( GetTypeCode() != GetArrayTYP(uPos) ) {

				Information( "Error: The data type must be the same." );

				return TRUE;
				}

			UINT uESze;

			uESze = GetDlgEntry(EDSZE);

			if( uMove == 0xC ) {	// bits and reals are always the same length

				if( uOSze > uESze ) {

					PackAliases( uPos + 1, uOSze - uESze );	// subtract difference from succeeding FG's
					}

				else if (uOSze < uESze ) {

					ExpandAliases(uPos + 1, uESze - uOSze);	// add difference to succeeding FG's
					}
				}

			sEFG.Printf( L"%s,%d", sEFG, uPos );	// new FG offset, List position

			AddIMP( sEFG, &List, 0 );

			break;

		case VINSERT:

			if( fExists ) {

				Information("Name is already used.");

				return TRUE;
				}

			UINT uAction;

			uAction = fHasList ? GetFGPosition(sEFG) : INSERTAPPD;	// numeric position, or control code

			switch( uAction ) {

				case INSERTAPPD:	// append to end

					sEFG.Printf( L"%s,%d", sEFG, fHasList ? GetArrayCNT() : 0 );

					AddIMP( sEFG, &List, 2 );

					m_uCurrSel = GetArrayCNT() - 1;

					break;

				case INSERTSAME:	// same FG

					Information(L"Foreground offset matches selection.  Use 'Replace' or make correct value.");

					return TRUE;

				case INSERTNDEF:

					Information(L"New bit position not defined (addr.n).");

					return TRUE;

				case INSERTCANT:

					Information(L"Cannot insert.  All bits are assigned, or bit number is invalid.");

					return TRUE;

				default:

					sEFG.Printf( L"%s,%d", sEFG, uAction );

					AddIMP( sEFG, &List, 1 );

					break;
				}

			if( m_pMMConfig->m_pTags ) {

				if( YesNo("Do you wish to create a tag using this name?") == IDYES ) {

					m_pMMConfig->CreateTagsFromIMP();
					}
				}

			break;

		case VDELETE:

			sPre =	TEXT("It is recommended that the tag mapped to %s is set to \"No Selection\", before deleting this Alias.\n")
				TEXT("Doing so will maintain a valid reference throughout the database.\n")
				TEXT("Do you wish to exit now, and perform that operation?");
	
			sPre.Printf( sPre, m_pMDriver->GetNAMFromList(uPos) );

			if( YesNo(sPre) == IDYES ) {

				EndDialog(TRUE);

				return TRUE;
				}

			if( DeleteAlias(uPos) ) {

				PackAliases(uPos, uOSze);
				}

			SetLBoxPosition(LBTAGS, m_uCurrSel);

			break;
		}

	LoadTagNameList();

	m_pMMConfig->RebuildMaps();

	if( uID == VREPLCE || uID == VDELETE ) {

		Information(L"You must choose File\\Utilities\\Rebuild Comms Blocks now.");

		EndDialog(TRUE);
		}

	return TRUE;
	}

void CMicromodAddrDialog::SetIMP(CString sFG, CStringArray *pList) {

	CString sItem;

	CString sAlias	= GetDlgItem(ALSVAL).GetWindowTextW();

	DWORD dLSP	= GetEditLSP();
	UINT uType	= GetTypeCode();
	UINT uAddrOff	= tatoi(sFG.Mid(sFG.Find(',') + 1));

	CString sTypeA;
	CString sTypeN;
	CString sSize;

	switch( uType ) {

		case 0:	// Bit
			sTypeA	= L"DISCRETE";
			sTypeN	= L"3";
			sSize	= L"1";
			break;

		case 0xC:
			sTypeA	= L"COUNT";
			sTypeN	= L"0";
			sSize	= L"4";
			break;

		case 0xE:
		default:
			sTypeA	= L"FLOAT PT";
			sTypeN	= L"10";
			sSize	= L"4";
			break;
		}

	(*pList).Empty();

	if( uAddrOff < GetArrayCNT() ) {		// not appending

		sItem = m_pMMConfig->m_IMPList.GetAt(uAddrOff);

		sItem.Tokenize(*pList, ',');
		}

	else {
		for( UINT i = 0; i <= ENTRYPOS; i++ ) {

			(*pList).Append(L"");	// init fields
			}
		}

	if( (*pList).GetCount() ) {

		sItem.Printf( L"%d", uAddrOff );
		(*pList).SetAt(NUMPOS, sItem);
		(*pList).SetAt(ENTRYPOS, sItem);

		(*pList).SetAt(NAMEPOS,	 sAlias);
		(*pList).SetAt(ALIASPOS, sAlias);

		(*pList).SetAt(ALSPPOS,	GetDlgItem(EDALSP).GetWindowTextW());

		sItem.Printf( L"%d", dLSP );
		(*pList).SetAt(NLSPPOS, sItem );

		(*pList).SetAt(ATYPPOS, sTypeA );
		(*pList).SetAt(NTYPPOS, sTypeN );

		(*pList).SetAt(DLENPOS, sSize  );

		sItem.Printf( L"%d", tatoi(sFG));
		(*pList).SetAt(FGOFFPOS, sItem );

		uAddrOff = sFG.Find('.');
		sItem = (!uType && uAddrOff < NOTHING) ? sFG.Mid(uAddrOff + 1, 1) : L"0";
		(*pList).SetAt(FGBITPOS, sItem);
		}
	}

void CMicromodAddrDialog::AddIMP(CString sFG, CStringArray *pList, UINT uFunc) {
				
	SetIMP(sFG, pList);

	if( (*pList).GetCount() ) {

		CString sItem	= m_pMMConfig->UpdateIMPString(*pList);

		UINT uOffset	= tatoi((*pList)[NUMPOS]);

		switch( uFunc ) {

			case 0:
				m_pMMConfig->m_IMPList.SetAt(uOffset, sItem);
				break;

			case 1:
				m_pMMConfig->m_IMPList.Insert(uOffset, sItem );
				break;

			case 2:
				m_pMMConfig->m_IMPList.Append(sItem);
				break;
			}

		if( uFunc ) {

			CAddress Addr;

			Addr.a.m_Table	= SP_65;
			Addr.a.m_Offset	= uOffset;
			Addr.a.m_Extra	= 0;
			Addr.a.m_Type	= m_pMDriver->GetArrayTYP(uOffset);

			m_pMMConfig->CreateTag(CError(FALSE), (*pList)[ALIASPOS], Addr, FALSE, FALSE);
			}
		}

	m_fSelect = FALSE;
	}

void CMicromodAddrDialog::LoadTagNameList(void) {

	UINT uTotal = GetArrayCNT();

	CListBox &Box = (CListBox &)GetDlgItem(LBTAGS);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	int nTab[] = { 40, 60, 160 };

	Box.SetTabStops(elements(nTab), nTab);

	CStringArray List;

	m_pMMConfig->SortIMPListByFG(FALSE);

	for( UINT n = 0; n < uTotal; n++ ) {

		CString s = m_pMMConfig->m_IMPList[n];

		List.Empty();

		s.Tokenize(List, ',');

		s.Printf( L"%d\t%d\t%s", tatoi(List[FGOFFPOS]), tatoi(List[NUMPOS]), List[ALIASPOS] );

		Box.AddString(s, (DWORD)tatoi(List[NUMPOS]));
		}

	Box.SelectData((DWORD)m_uCurrSel);

	Box.SetRedraw(TRUE);

	Box.Invalidate(TRUE);
	}

void CMicromodAddrDialog::EnableTagDisplay(BOOL fYes) {

	GetDlgItem(CURRID ).EnableWindow(TRUE);
	GetDlgItem(CURRSPC).EnableWindow(TRUE);
	GetDlgItem(ATTYPE).EnableWindow(TRUE);
	GetDlgItem(ALSVAL).EnableWindow(TRUE);

	GetDlgItem(LBTAGG).EnableWindow(fYes);
	GetDlgItem(LBTAGS).EnableWindow(fYes);

	GetDlgItem(LHEXSEL).EnableWindow(fYes);

	GetDlgItem(EDALSP).EnableWindow(fYes);

	GetDlgItem(4001).EnableWindow(TRUE);

	GetDlgItem(1000).EnableWindow(!fYes);

	GetDlgItem(1050).EnableWindow(fYes);
	GetDlgItem(1051).EnableWindow(fYes);
	GetDlgItem(VLOADID).EnableWindow(fYes);

	DoTagDEnable(LSPTXT, CURROCC, fYes);

	EnableEdits(fYes);
	}

void CMicromodAddrDialog::DoTagDEnable(UINT uIDS, UINT uIDE, BOOL fYes) {

	for( UINT i = uIDS; i <= uIDE; i++ ) {

		GetDlgItem(i).EnableWindow(fYes);
		}
	}

void CMicromodAddrDialog::ClearTagDItems(void) {

	ClearItem(ALPHLSP);
	ClearItem(CURRLSP);
	ClearItem(CURRFG);
	ClearItem(CURRPAR);
	ClearItem(CURRSZE);
	ClearItem(CURROCC);
	ClearItem(EDLSP);
	ClearItem(EDFG);
	ClearItem(EDPAR);
	ClearItem(EDSZE);
	ClearItem(EDOCC);
	ClearItem(EDALSP);

	GetDlgItem(LHEXSEL).EnableWindow(FALSE);

	SetCBoxPosition(EDCBVAL, 0);
	}

void CMicromodAddrDialog::ClearItem(UINT uID) {

	GetDlgItem(uID).SetWindowTextW(L"");
	}

// Overridables

BOOL CMicromodAddrDialog::AllowType(UINT uType)
{
	if( m_pSpace ) {

		if( IsFunc65(m_pSpace->m_uTable) ) {

			switch( uType ) {

				case addrBitAsBit:
				case addrLongAsLong:
				case addrRealAsReal:
					return TRUE;
				}

			return FALSE;
			}
		}

	return TRUE;
	}

void CMicromodAddrDialog::SetAddressText(CString Text)
{
	if( !Text.IsEmpty() ) {

		GetDlgItem(CURRID).SetWindowText(Text);
		GetDlgItem(CURRID).EnableWindow (TRUE);

		return;
		}

	GetDlgItem(CURRID).SetWindowText(Text);
	GetDlgItem(CURRID).EnableWindow (FALSE);
	ClearDetails();
	ClearType();
	SetLSPEnables(FALSE);
	}

CString CMicromodAddrDialog::GetAddressText(void)
{
	CString s = GetDlgItem(CURRID).GetWindowText();

	return s;
	}

void CMicromodAddrDialog::ShowDetails(void)
{
	UINT uTable = m_pSpace->m_uTable;

	if( uTable == SP_65 ) {

		return;
		}

	CString s = m_pSpace->GetNativeText();

	GetDlgItem(3002).SetWindowText(s);

	CString  Min = L"";

	CString  Max = L"";

	CString  Rad = L"";

	if( uTable < addrNamed ) {

		CAddress Addr;

		Addr.a.m_Type = GetTypeCode();

		m_pSpace->GetMinimum(Addr);

		m_pMDriver->ExpandAddress(Min, m_pMMConfig, Addr);
			
		UINT uFind = Min.Find('.');

		if( uFind < NOTHING ) {

			switch( Min[uFind+1] ) {

				case 'B':
					s = L"Bit";
					break;
				case 'L':
					s = L"Long";
					break;

				default:
					s = L"Real";
					break;
				}
			}

		GetDlgItem(3002).SetWindowTextW(s);

		s.Printf( L".%s", s );

		GetDlgItem(2003).SetWindowTextW(s);

		Min = Min.Left(uFind);

		m_pSpace->GetMaximum(Addr);

		m_pMDriver->ExpandAddress(Max, m_pMMConfig, Addr);

		uFind = Max.Find('.');

		if( uFind < NOTHING ) {

			Max = Max.Left(uFind);
			}

		Rad = m_pSpace->GetRadixAsText();
		}

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

// Block Combobox
void CMicromodAddrDialog::LoadNames(void) {

	CComboBox & Box = (CComboBox &) GetDlgItem(EDCBVAL);

	ClearCBox(EDCBVAL);

	MMBLOCKNAMES * p = m_pBList;

	for( UINT i = 0; i < elements(BlockNameList); i++, p++ ) {

		Box.AddString(p->Name, p->Item);
		}

	SetCBoxPosition(EDCBVAL, 0);
	}

void CMicromodAddrDialog::SetCBoxPosition(UINT uID, UINT uPos) {

	CComboBox & Box = (CComboBox &) GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CMicromodAddrDialog::GetCBoxPosition(UINT uID) {

	CComboBox & Box = (CComboBox &) GetDlgItem(uID);

	return Box.GetCurSel();
	}

void CMicromodAddrDialog::ClearCBox(UINT uID) {

	CComboBox & Box = (CComboBox &) GetDlgItem(uID);

	UINT uCount = Box.GetCount();

	for( UINT i = 0; i < uCount; i++ ) {

		Box.DeleteString(i);
		}
	}

void CMicromodAddrDialog::SetLBoxPosition(UINT uID, UINT uPos) {

	CListBox & Box = (CListBox &) GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CMicromodAddrDialog::GetLBoxPosition(UINT uID) {

	CListBox & Box = (CListBox &) GetDlgItem(uID);

	return Box.GetCurSel();
	}

// Editing
void CMicromodAddrDialog::InitEdit(CAddress Addr) {

	if( Addr.a.m_Table != SP_65 ) {

		SetCBoxPosition(EDCBVAL, 0);

		EnableEdits(FALSE);

		return;
		}

	EnableEdits(TRUE);

	UINT uItem  = GetItemFromOffset(Addr.a.m_Offset);

	SetEditLSP(GetLSPFromList(uItem));	// sets LSP, Par, Occ

	SetEditBytes(GetSZEFromList(uItem));

	ShowALSPEditText();

	UINT uType = m_pMDriver->GetTYPFromList(uItem);

	CString s;

	s.Printf( L"%d", GetFGFromList(uItem) );

	if( !uType ) {

		s.Printf( L"%s.%1.1d", s, GetBITFromList(uItem) );
		}

	GetDlgItem(EDFG).SetWindowTextW(s);
	}

void CMicromodAddrDialog::EnableEdits(BOOL fYes) {

	BOOL fEd  = fYes && !m_fSelect;
	BOOL fLSP = fEd  &&  m_fELSPSel;
	BOOL fBlk = fEd  && !m_fELSPSel;

	GetDlgItem(ELSPTXT).EnableWindow(fLSP);
	GetDlgItem(EDLSP  ).EnableWindow(fLSP);
	GetDlgItem(EDBYLSP).EnableWindow(fEd);
	GetDlgItem(CURRLSP).EnableWindow(fYes);
	GetDlgItem(LHEXSEL).EnableWindow(fYes);

	GetDlgItem(EFGTXT).EnableWindow(fEd);
	GetDlgItem(EDFG  ).EnableWindow(fEd);

	if( fYes ) {

		GetDlgItem(CURRFG).EnableWindow(TRUE);	// otherwise control from selection of Read Database command
		}

	GetDlgItem(EPARTXT).EnableWindow(fBlk);
	GetDlgItem(EDPAR  ).EnableWindow(fBlk);

	BOOL fSze = fBlk && GetTypeCode() == 0xC;	// Bit & Real Types are fixed sizes

	GetDlgItem(ESZETXT).EnableWindow(fSze);
	GetDlgItem(EDSZE  ).EnableWindow(fSze);

	GetDlgItem(EOCCTXT).EnableWindow(fBlk);
	GetDlgItem(EDOCC  ).EnableWindow(fBlk);

	GetDlgItem(EDCBTXT).EnableWindow(fBlk);
	GetDlgItem(EDCBVAL).EnableWindow(fBlk);
	GetDlgItem(EDALSP).EnableWindow(fBlk);

	GetDlgItem(VREPLCE).EnableWindow(fEd);
	GetDlgItem(VINSERT).EnableWindow(fEd);
	GetDlgItem(VDELETE).EnableWindow(fEd);

	DoTagDEnable(VLOADID, ALPHLSP, fYes);
	}

void CMicromodAddrDialog::SetEditLSP(DWORD dLSP) {

	SetDlgEntry(EDLSP, dLSP);

	SetEditPar(GetParFromLSP(dLSP));

	SetEditOcc(GetOccFromLSP(dLSP) + 1);

	SetCBoxPosition(EDCBVAL, GetBlkFromLSP(dLSP) - 9);
	}

void CMicromodAddrDialog::SetEditPar(DWORD dOff) {

	SetDlgEntry(EDPAR, dOff);
	}

void CMicromodAddrDialog::SetEditBytes(DWORD dOff) {

	SetDlgEntry(EDSZE, dOff);
	}

void CMicromodAddrDialog::SetEditOcc(DWORD dOff) {

	SetDlgEntry(EDOCC, dOff);
	}

DWORD CMicromodAddrDialog::GetEditLSP(void) {

	return GetDlgEntry(EDLSP);
	}

// Array Access
DWORD CMicromodAddrDialog::GetLSPFromList(UINT uItem)
{
	return m_pMDriver->GetLSPFromList(uItem);
	}

WORD CMicromodAddrDialog::GetFGFromList(UINT uItem)
{
	return m_pMDriver->GetFGFromList(uItem);
	}

WORD CMicromodAddrDialog::GetSZEFromList(UINT uItem)
{
	return m_pMDriver->GetSZEFromList(uItem);
	}

WORD CMicromodAddrDialog::GetBITFromList(UINT uItem)
{
	return m_pMDriver->GetBITFromList(uItem);
	}

UINT CMicromodAddrDialog::GetBlkFromLSP(DWORD dLSP)
{
	return m_pMDriver->GetBlkFromLSP(dLSP);
	}

UINT CMicromodAddrDialog::GetParFromLSP(DWORD dLSP)
{
	return (UINT)(dLSP & LSP_PAR_MASK);
	}

UINT CMicromodAddrDialog::GetOccFromLSP(DWORD dLSP)
{
	return m_pMDriver->GetOccFromLSP(dLSP);
	}

DWORD CMicromodAddrDialog::MakeBaseLSP(UINT uTable)
{
	return (uTable - 9) << LSP_BLK_POS;
	}

DWORD CMicromodAddrDialog::MakeParFromDialog(void)
{
	CString s = GetDlgItem(CURRPAR).GetWindowText();

	return (DWORD)tstrtoul(s, NULL, 10) & LSP_PAR_MASK;
	}

DWORD CMicromodAddrDialog::MakeOccFromDialog(void)
{
	CString s = GetDlgItem(CURROCC).GetWindowText();

	return (DWORD)((tstrtoul(s, NULL, 10) - 1) << LSP_OCC_POS) & LSP_OCC_MASK;
	}

UINT CMicromodAddrDialog::GetItemFromOffset(WORD wOffset) {

	UINT u = GetArrayCNT();

	for( UINT i = 0; i < u; i++ ) {

		if( wOffset == LOWORD(tatoi(m_pMMConfig->m_IMPList[i])) ) {

			return i;
			}
		}

	return NOTHING;
	}

UINT CMicromodAddrDialog::GetOffsetFromItem(UINT uItem) {

	if( uItem < GetArrayCNT() ) {

		return tatoi(m_pMMConfig->m_IMPList[uItem]);
		}

	return NOTHING;
	}

DWORD CMicromodAddrDialog::GetArrayLSP(void)
{
	return GetArrayLSP(GetCURRIDValue());
	}

DWORD CMicromodAddrDialog::GetArrayLSP(UINT uItem)
{
	return m_pMDriver->GetArrayLSP(uItem);
	}

WORD CMicromodAddrDialog::GetArrayFG(void)
{
	return GetArrayFG(GetCURRIDValue());
	}

WORD CMicromodAddrDialog::GetArrayFG(UINT uItem)
{
	return m_pMDriver->GetArrayFG(uItem);
	}

WORD CMicromodAddrDialog::GetArraySZE(void)
{
	return GetArraySZE(GetCURRIDValue());
	}

WORD CMicromodAddrDialog::GetArraySZE(UINT uItem)
{
	return m_pMDriver->GetArraySZE(uItem);
	}

WORD CMicromodAddrDialog::GetArrayBIT(void)
{
	return GetArrayBIT(GetCURRIDValue());
	}

WORD CMicromodAddrDialog::GetArrayBIT(UINT uItem)
{
	return m_pMDriver->GetArrayBIT(uItem);
	}

WORD CMicromodAddrDialog::GetArrayTYP(UINT uItem) {

	return m_pMDriver->GetArrayTYP(uItem);
	}

WORD CMicromodAddrDialog::GetArrayOFF(UINT uItem) {

	return m_pMDriver->GetArrayOFF(uItem);
	}

CString CMicromodAddrDialog::GetArrayALSP(UINT uItem) {

	return m_pMDriver->GetArrayALSP(uItem);
	}

WORD CMicromodAddrDialog::GetArrayCNT(void)
{
	return LOWORD(m_pMDriver->GetArrayCNT());
	}

void CMicromodAddrDialog::DoArraySelect(UINT uFunc, UINT uValue)
{
	m_pMDriver->DoArraySelect(uFunc, uValue);
	}

void CMicromodAddrDialog::SetArrayFunc(UINT uFunc)
{
	m_pMDriver->SetArrayFunc(uFunc);
	}

void CMicromodAddrDialog::SetArrayValue(DWORD dValue)
{
	m_pMDriver->SetArrayValue(dValue);
	}

DWORD CMicromodAddrDialog::GetArrayValue(void)
{
	return m_pMDriver->GetArrayValue();
	}

// Helpers
DWORD CMicromodAddrDialog::GetDlgEntry(UINT uID)
{
	CString s = GetDlgItem(uID).GetWindowText();

	if( IsLSPSel(uID) ) {

		return MakeLSPValueFromText(s);
		}

	return tstrtoul(s, NULL, 10);
	}

void CMicromodAddrDialog::SetDlgEntry(UINT uID, DWORD dData)
{
	CString s;

	if( IsLSPSel(uID) ) {

		s = MakeLSPTextFromValue(dData);
		}

	else {
		s.Printf(TEXT("%d"), dData);
		}

	GetDlgItem(uID).SetWindowText(s);
	}

WORD CMicromodAddrDialog::GetCURRIDValue(void) {

	return LOWORD(GetDlgEntry(CURRID));
	}

void CMicromodAddrDialog::SetLSPEnables(BOOL fSet)
{
	GetDlgItem(CURRLSP).EnableWindow(TRUE);

	GetDlgItem(LHEXSEL).EnableWindow(TRUE);

	GetDlgItem(VLOADID).EnableWindow(fSet);
	}

void CMicromodAddrDialog::DoShow(UINT uTable, CAddress Addr)
{
	GetDlgItem(CURRID).EnableWindow(TRUE);

	if( !m_pMMConfig->m_LSPList.GetCount() ) {

		m_pMMConfig->RebuildMaps();
		}

	CString s = "None";

	if( m_pSpace ) {

		s = m_pSpace->m_Prefix;
		}

	GetDlgItem(CURRSPC).SetWindowTextW(s);

	BOOL f65 = IsAttribute(Addr.a.m_Table);

	s.Printf( L"%d", Addr.a.m_Offset );

	SetAddressText(s);

	s = m_pSpace->GetTypeModifier(Addr.a.m_Type);

	GetDlgItem(ATTYPE).SetWindowTextW( "." + s );

	if( f65 ) {

		m_uHasLSP = SP_65;

		ShowArrayInfo(Addr);

		ClearDetails();
		}

	else {
		ShowDetails();
		}

	BOOL f1 = IsFunc65(uTable);
	BOOL f2 = f1 && !Is65Command(uTable);

	if( !f1 ) {

		ClearTagDItems();
		}

	if( f1 ) {

		if( !f2 ) {

			UINT uDBase = m_pMMConfig->GetDataAccess(TEXT("Database"))->ReadInteger(m_pMMConfig);

			switch( uDBase ) {

				case 0:	s = TEXT("Current");	break;
				case 1:	s = TEXT("Main");	break;
				case 2: s = TEXT("Default");	break;
				case 3:	s = TEXT("Module");	break;
				}

			GetDlgItem(CURRFG).SetWindowText(s);

			s = TEXT("DBase:");

			GetDlgItem(FGTXT).SetWindowTextW( CString("DBase:") );
			}

		else {
			GetDlgItem(FGTXT).SetWindowTextW( CString("FG Offset") );
			}

		GetDlgItem(FGTXT).EnableWindow(TRUE);
		GetDlgItem(CURRFG).EnableWindow(TRUE);
		}

	GetDlgItem(LBTAGS).EnableWindow(f2);

	GetDlgItem(LSPTXT ).EnableWindow(f2);
	GetDlgItem(CURRLSP).EnableWindow(f2);
	GetDlgItem(LHEXSEL).EnableWindow(f2);
	GetDlgItem(SZETXT ).EnableWindow(f2);
	GetDlgItem(CURRSZE).EnableWindow(f2);
	GetDlgItem(PARTXT ).EnableWindow(f2);
	GetDlgItem(CURRPAR).EnableWindow(f2);
	GetDlgItem(OCCTXT ).EnableWindow(f2);
	GetDlgItem(CURROCC).EnableWindow(f2);
	GetDlgItem(VLOADID).EnableWindow(f2);
	GetDlgItem(ALPHLSP).EnableWindow(f2);
	}

void CMicromodAddrDialog::ShowArrayInfo(CAddress Addr)
{
	UINT uTable = m_uHasLSP;

	SetLSPEnables((BOOL)uTable);

	if( !uTable ) {

		CString sZero = TEXT("0");

		GetDlgItem(CURRLSP).SetWindowText(sZero);
		GetDlgItem(CURRFG).SetWindowText(sZero);
		GetDlgItem(CURRSZE).SetWindowText(sZero);
		GetDlgItem(CURRPAR).SetWindowText(sZero);
		GetDlgItem(ALPHLSP).SetWindowTextW(TEXT(""));

		GetDlgItem(CURROCC).SetWindowText(TEXT("1"));

		return;
		}

	UINT uItem = GetItemFromOffset(Addr.a.m_Offset);

	if( uItem < GetArrayCNT() ) {

		CString s;

		s.Printf( "%s", m_pMMConfig->GetIMPStringItem(uItem, ALIASPOS) );

		GetDlgItem(ALSVAL).SetWindowTextW(s);

		DWORD dVal = GetArrayLSP(uItem);

		SetDlgEntry(CURRLSP, dVal);

		GetDlgItem(ALPHLSP).SetWindowTextW(GetArrayALSP(uItem));

		SetDlgEntry(CURRPAR, GetParFromLSP(dVal));

		SetDlgEntry(CURROCC, GetOccFromLSP(dVal) + 1);

		SetDlgEntry(CURRSZE, GetArraySZE(uItem));

		UINT uType = GetArrayTYP(uItem);

		SetTypeFromArrayValue(uType);

		s.Printf( L"%d", GetArrayFG() );

		if( !uType ) {
			s.Printf( L"%s.%1.1d", s, GetArrayBIT(uItem));
			}

		GetDlgItem(CURRFG).SetWindowTextW(s);

		SetLBoxPosition(LBTAGS, uItem);
		}
	}

void CMicromodAddrDialog::SetTypeFromArrayValue(UINT uType) {

	CListBox &Box = (CListBox &)GetDlgItem(4001);

	Box.SelectData(uType);

	CString s;

	switch( uType ) {

		case 0:
			s = L".BIT";
			break;

		case 0xC:
			s = L".LONG";
			break;

		default:
			s = L".REAL";
			break;
		}

	GetDlgItem(2003).SetWindowTextW(s);
	}

BOOL CMicromodAddrDialog::IsFunc65(UINT uTable)
{
	return m_pMDriver->IsFunc65(uTable);
	}

BOOL CMicromodAddrDialog::IsAttribute(UINT uTable)
{
	return uTable == SP_65;
	}

BOOL CMicromodAddrDialog::Is65Command(UINT uTable)
{
	return m_pMDriver->Is65Command(uTable);
	}

UINT CMicromodAddrDialog::HasLSP(UINT uTable)
{
	return IsAttribute(uTable) ? SP_65 : 0;
	}

void CMicromodAddrDialog::SetEHexSel(void) {

	CButton & Button = (CButton &)GetDlgItem(LHEXSEL);

	Button.SetCheck(m_fEHexSel);
	}

BOOL CMicromodAddrDialog::IsLSPSel(UINT uID) {

	return uID == CURRLSP || uID == EDLSP;
	}

DWORD CMicromodAddrDialog::MakeLSPValueFromText(CString sLSP) {

	UINT uRadix = m_fEHexSel ? 16 : 10;

	return tstrtoul(sLSP, NULL, uRadix);
	}

CString CMicromodAddrDialog::MakeLSPTextFromValue(DWORD dLSP) {

	CString s;

	if( m_fEHexSel ) {

		s.Printf(L"%8.8lX", dLSP);
		}

	else {
		s.Printf(L"%d", dLSP);
		}

	return s;
	}

CString CMicromodAddrDialog::MakeALSPTextFromEdit(void) {

	UINT  uBlk;
	UINT  uOcc;
	UINT  uPar;

	if( m_fELSPSel ) {

		DWORD dLSP = GetDlgEntry(EDLSP);

		uBlk = GetBlkFromLSP(dLSP) - 9;
		uOcc = GetOccFromLSP(dLSP) + 1;
		uPar = GetParFromLSP(dLSP);
		}

	else {
		uBlk = GetCBoxPosition(EDCBVAL);
		uOcc = GetDlgEntry(EDOCC);
		uPar = GetDlgEntry(EDPAR);

		DWORD dLSP = (uBlk << LSP_BLK_POS) + ((uOcc - 1) << LSP_OCC_POS) + uPar;

		GetDlgItem(EDLSP).SetWindowTextW(MakeLSPTextFromValue(dLSP));
		}

	return MakeALSPTextFromParams(uBlk, uOcc, uPar);
	}

CString CMicromodAddrDialog::MakeALSPTextFromParams(UINT uBlk, UINT uOcc, UINT uPar) {

	CString s;

	UINT i = 0;

	while( i < elements(BlockNameList) ) {

		if( m_pBList[i].Item == uBlk ) {

			s = m_pBList[uBlk].Name;

			break;
			}

		i++;
		}

	s.Printf( L"%s%d.%d",

		s,
		uOcc,
		uPar
		);

	return s;
	}

void CMicromodAddrDialog::ShowALSPEditText(void) {

	GetDlgItem(EDALSP).SetWindowTextW(MakeALSPTextFromEdit());
	}

void CMicromodAddrDialog::SetELSPSel(void) {

	CButton & Button = (CButton &)GetDlgItem(EDBYLSP);

	Button.SetCheck(m_fELSPSel);
	}

// Insert and Append Help
UINT CMicromodAddrDialog::GetFGPosition(CString sFGSel) {

	UINT uFGSel = tatoi(sFGSel);

	UINT uTotal = GetArrayCNT();

	UINT uFG    = GetFGFromList(uTotal - 1);

	if( uFG < uFGSel ) {

		return INSERTAPPD;	// append to end of list
		}

	UINT i = 0;

	while( i < uTotal ) {

		uFG = GetFGFromList(i);

		if( uFG < uFGSel ) {

			i++;

			continue;
			}

		if( uFG == uFGSel ) {

			if( !GetLBoxPosition(4001) ) {	// check if bit FG request

				return INSERTSAME;	// not bit, and FG position is the same
				}

			break;	// found first position of FG with bits
			}

		else return i;	// insert at this position
		}

	UINT uBitSel = 0;

	if( sFGSel.Find('.' < NOTHING) ) {

		uBitSel = tatoi(sFGSel.Right(1));

		if( uBitSel > 7 ) {

			return INSERTCANT;	// invalid bit selection
			}
		}

	else {
		return INSERTNDEF;		// bit not specified
		}

	UINT uEnd = i + 8;

	CStringArray List;

	while( i < uEnd ) {

		CString sWork = m_pMMConfig->m_IMPList.GetAt(i);

		sWork.Tokenize(List, ',');

		if( (UINT)tatoi(List[FGOFFPOS]) == uFGSel && (UINT)tatoi(List[FGBITPOS]) < uBitSel ) {

			i++;

			continue;
			}

		return i;	// found insert position
		}

	return INSERTCANT;	// can't insert bit
	}

void CMicromodAddrDialog::ExpandAliases(UINT uPos, UINT uSize) {

	UINT uLast = GetArrayCNT();

	CStringArray List;

	CString sWork;

	for( UINT i = uPos; i < uLast; i++ ) {

		List.Empty();

		sWork = m_pMMConfig->m_IMPList.GetAt(i);

		sWork.Tokenize(List, ',');

		sWork.Printf( L"%d", tatoi(List[FGOFFPOS]) + uSize);

		List.SetAt(FGOFFPOS, sWork);

		m_pMMConfig->m_IMPList.SetAt(i, m_pMMConfig->UpdateIMPString(List));
		}
	}

BOOL CMicromodAddrDialog::DeleteAlias(UINT uPos) {

	CMicromodDeviceOptions * pDO = m_pMMConfig;

	CString s = pDO->m_IMPList[uPos];

	UINT uLast = uPos == pDO->m_IMPList.GetCount() - 1;

	CString t = L"65535";

	s.Printf( L"%s%s", t, s.Mid(s.Find(',')) );

	pDO->m_IMPList.SetAt(uPos, s);

	pDO->SortIMPListByOffset(TRUE);	// remove item and reassign offsets

	if( (BOOL)uLast ) {	// last item was removed

		m_uCurrSel = pDO->m_IMPList.GetCount() - 1;

		return FALSE;	// indicate last item was removed
		}

	return TRUE;
	}

void CMicromodAddrDialog::PackAliases(UINT uPos, UINT uSize) {

	UINT uLast = GetArrayCNT();

	CStringArray List;

	CString sWork;

	for( UINT i = uPos; i < uLast; i++ ) {

		List.Empty();

		CString sWork = m_pMMConfig->m_IMPList.GetAt(i);

		sWork.Tokenize(List, ',');

		sWork.Printf( L"%d", tatoi(List[FGOFFPOS]) - uSize );

		List.SetAt(FGOFFPOS, sWork);

		m_pMMConfig->m_IMPList.SetAt(i, m_pMMConfig->UpdateIMPString(List));
		}
	}

// End of File
