
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SPACE_HPP
	
#define	INCLUDE_SPACE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Radix Codes
//

enum Radix
{
	radixMix = 0,
	radixBin = 2,
	radixOct = 8,
	radixDec = 10,
	radixHex = 16
	};

//////////////////////////////////////////////////////////////////////////
//
// Driver Space Definition
//

class CSpace
{
	public:
		// Constructors
		CSpace(CString p, CString c, UINT n);
		CSpace(CString p, CString c, UINT n, UINT t);
		CSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);
		CSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);
		CSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT w);

		// Legacy Constructors
		CSpace(PCSTR p, PCSTR c, UINT n);
		CSpace(PCSTR p, PCSTR c, UINT n, UINT t);
		CSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t);
		CSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s);
		CSpace(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT w);

		// Destructor
		virtual ~CSpace(void);

		// Validation
		BOOL IsNamed(void);
		BOOL IsOutOfRange(UINT uOffset);
		BOOL IsBadAlignment(UINT uOffset, UINT uType);
		BOOL IsBadAlignment(UINT uOffset);

		// Type Conversion
		CString	 GetTypeModifier(UINT uType);
		UINT     TypeFromModifier(CString Text);

		// Printing
		CString	GetTypeText(void);
		CString	GetTypeAsText(UINT uType);
		CString	GetNativeText(void);
		CString	GetRadixAsText(void);
		CString	GetValueAsText(UINT uValue);
		CString	GetValueAsText(UINT uValue, UINT uWidth);

		// Matching
		virtual BOOL MatchSpace(CAddress const &Addr);

		// Limits
		virtual void GetMinimum(CAddress &Addr);
		virtual void GetMaximum(CAddress &Addr);

		// Data Members
		UINT	m_uTable;
		CString	m_Prefix;
		CString	m_Caption;
		UINT	m_uAlign;
		UINT	m_uRadix;
		UINT	m_uMinimum;
		UINT	m_uMaximum;
		UINT	m_uType;
		UINT	m_uSpan;
		UINT	m_uWidth;

	protected:
		// Constructors
		CSpace(void);

		// Implementation
		void FindAlignment(void);
		void FindWidth(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CList <CSpace *> CSpaceList;

// End of File

#endif
