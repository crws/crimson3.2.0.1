
#include "intern.hpp"

#include "cruisair.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cruisair Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCruisairDriverOptions, CUIItem);

// Constructor

CCruisairDriverOptions::CCruisairDriverOptions(void)
{
	m_Timeout = 1000;
	}

// Download Support

BOOL CCruisairDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Timeout));

	return TRUE;
	}

// Meta Data Creation

void CCruisairDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
// Cruisair Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCruisairDeviceOptions, CUIItem);

// Constructor

CCruisairDeviceOptions::CCruisairDeviceOptions(void)
{
	m_Chiller = 1;
	}

// Download Support

BOOL CCruisairDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Chiller));

	return TRUE;
	}

// Meta Data Creation

void CCruisairDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Chiller);
	}

//////////////////////////////////////////////////////////////////////////
//
// Cruisair Driver
//

// Instantiator

ICommsDriver *	Create_CruisairDriver(void)
{
	return New CCruisairDriver;
	}

// Constructor

CCruisairDriver::CCruisairDriver(void)
{
	m_wID		= 0x4027;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Cruisair";
	
	m_DriverName	= "Temperature Controller";
	
	m_Version	= "1.20";
	
	m_ShortName	= "Cruisair Temperature Controller";

	AddSpaces();
	}

// Binding Control

UINT	CCruisairDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CCruisairDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CCruisairDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CCruisairDriverOptions);
	}

CLASS CCruisairDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCruisairDeviceOptions);
	}

// Address Management

BOOL CCruisairDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	if( fPart && Addr.a.m_Table == SPH ) return FALSE;
	
	CCruisairAddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL CCruisairDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	BOOL f = CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);

	if( f ) Addr.a.m_Extra = SetLocalStatus(pSpace);

	return f;
	}

// Implementation	

void CCruisairDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SPH, " ",	 "CHILLER STATUS...",			10,	SPCHILL, 0, LL));
	AddSpace(New CSpace(AN,  "CSN",  "   Current Stage number ",		10,	CSN,	 0, LL));
	AddSpace(New CSpace(AN,  "LFS",  "   Loop Water flow switch",		10,	LFS,	 0, LL));
	AddSpace(New CSpace(AN,  "SVN",  "   Software Version #90-xx",		10,	SVN,	 0, LL));
	AddSpace(New CSpace(AN,  "SIT",  "   Seawater Inlet Temp",		10,	SIT,	 0, LL));
	AddSpace(New CSpace(AN,  "SOT",  "   Seawater Outlet Temp",		10,	SOT,	 0, LL));
	AddSpace(New CSpace(AN,  "LST",  "   Loop Supply Temp",			10,	LST,	 0, LL));
	AddSpace(New CSpace(AN,  "LRT",  "   Loop Return Temp",			10,	LRT, 	 0, LL));
	AddSpace(New CSpace(AN,  "LFT",  "   Loop Freeze Temp",			10,	LFT,	 0, LL));
	AddSpace(New CSpace(AN,  "RLP",  "   R22 Low Pressure",			10,	RLP,	 0, LL));
	AddSpace(New CSpace(AN,  "RHP",  "   R22 High Pressure",		10,	RHP,	 0, LL));
	AddSpace(New CSpace(AN,  "LPR",  "   Loop Pressure",			10,	LPR, 	 0, LL));
	AddSpace(New CSpace(AN,  "SPR",  "   Seawater Pressure",		10,	SPR, 	 0, LL));
	AddSpace(New CSpace(AN,  "CCU",  "   Compressor Current",		10,	CCU,	 0, LL));
	AddSpace(New CSpace(AN,  "SPC",  "   Seawater Pump Current",		10,	SPC,	 0, LL));
	AddSpace(New CSpace(AN,  "LPC",  "   Loop Pump Current",		10,	LPC,	 0, LL));
	AddSpace(New CSpace(AN,  "ACV",  "   AC Voltage",			10,	ACV,	 0, LL));
	AddSpace(New CSpace(AN,  "CRT",  "   Compressor run time",		10,	CRT,	 0, LL));
	AddSpace(New CSpace(AN,  "RVT",  "   Reversing Valve run time",		10,	RVT,	 0, LL));
	AddSpace(New CSpace(AN,  "SPT",  "   Seawater Pump run time",		10,	SPT,	 0, LL));
	AddSpace(New CSpace(AN,  "LPT",  "   Loop Pump run time",		10,	LPT,	 0, LL));
	AddSpace(New CSpace(AN,  "HRT",  "   Heater run time",			10,	HRT,	 0, LL));
	AddSpace(New CSpace(AN,  "MODE", "   Operating Mode",			10,	MODE,	 0, LL));
	AddSpace(New CSpace(AN,  "DLYT", "   Delay Timer in Seconds",		10,	DLYT,	 0, LL));
	AddSpace(New CSpace(AN,  "IMT",  "   Immersion Heater Temperature",	10,	IMT,	 0, LL));
	AddSpace(New CSpace(AN,  "FDB",  "   Fault Display Bits",		10,	FDB,	 0, LL));
	AddSpace(New CSpace(AN,  "TGR",  "   Toggle Rotate On/Off",		10,	TGR,	 0, BB));
	AddSpace(New CSpace(AN,  "ISN",  "   Increment Stage Number",		10,	ISN,	 0, BB));
	AddSpace(New CSpace(AN,  "DSN",  "   Decrement Stage Number",		10,	DSN,	 0, BB));
	AddSpace(New CSpace(AN,  "TGC",  "   Toggle Chiller On/Off",		10,	TGC,	 0, BB));
	AddSpace(New CSpace(AN,  "C1LG", "   Clear All Logs, this device",	10,	C1LG,	 0, BB));
	AddSpace(New CSpace(AN,  "C1FH", "   Clear Fault History",		10,	C1FH,	 0, BB));
	AddSpace(New CSpace(AN,  "C1IO", "   Clear I/O Limits",			10,	C1IO,	 0, BB));
	AddSpace(New CSpace(AN,  "C1HH", "   Clear Heater Hours",		10,	C1HH,	 0, BB));
	AddSpace(New CSpace(AN,  "C1CH", "   Clear Compressor Hours",		10,	C1CH,	 0, BB));

	AddSpace(New CSpace(SPH, " ",	 "FAULTS AND FAULT HISTORY...",		10,	SPFAULT, 0, LL));
	AddSpace(New CSpace(AN,  "FCN",  "   Number of faults",			10,	FCN,	 0, LL));
	AddSpace(New CSpace(AN,  "FAB",  "   Fault Active Bits",		10,	FAB,	 0, LL));
	AddSpace(New CSpace(AN,  "ACL",  "   Low AC voltage fault count",	10,	ACL,	 0, LL));
	AddSpace(New CSpace(AN,  "FSW",  "   Flow Switch fault count",		10,	FSW,	 0, LL));
	AddSpace(New CSpace(AN,  "HDS",  "   High Discharge fault count",	10,	HDS,	 0, LL));
	AddSpace(New CSpace(AN,  "LLT",  "   Loop Low Temp fault count",	10,	LLT,	 0, LL));
	AddSpace(New CSpace(AN,  "LHT",  "   Loop High Temp fault count",	10,	LHT,	 0, LL));
	AddSpace(New CSpace(AN,  "SLT",  "   Seawater Low Temp fault count",	10,	SLT,	 0, LL));
	AddSpace(New CSpace(AN,  "EEC",  "   Eprom Error count",		10,	EEC,	 0, LL));
	AddSpace(New CSpace(AN,  "LOP",  "   Loop Out Probe fault count",	10,	LOP,	 0, LL));
	AddSpace(New CSpace(AN,  "LSP",  "   Loop Supply Probe fault count",	10,	LSP,	 0, LL));
	AddSpace(New CSpace(AN,  "FCT",  "   Total Fault Count",		10,	FCT,	 0, LL));
	AddSpace(New CSpace(AN,  "FCAC", "   AC Voltage Fault Count",		10,	FCAC,	 0, LL));
	AddSpace(New CSpace(AN,  "FCFS", "   Flow Switch Fault Count",		10,	FCFS,	 0, LL));
	AddSpace(New CSpace(AN,  "FCLF", "   Low Freon Suction Fault Count",	10,	FCLF,	 0, LL));
	AddSpace(New CSpace(AN,  "FCHF", "   High Freon Discharge Fault Count",	10,	FCHF,	 0, LL));
	AddSpace(New CSpace(AN,  "FCLL", "   Loop Low Temp Error Fault Count",	10,	FCLL,	 0, LL));
	AddSpace(New CSpace(AN,  "FCLH", "   Loop High Temp Error Fault Count",	10,	FCLH,	 0, LL));
	AddSpace(New CSpace(AN,  "FCSL", "   Seawater Low Temp Err Fault Count",10,	FCSL,	 0, LL));
	AddSpace(New CSpace(AN,  "FCEE", "   Eprom Error Fault Count",		10,	FCEE,	 0, LL));
	AddSpace(New CSpace(AN,  "FCLO", "   Loop Out Probe Error Fault Count",	10,	FCLO,	 0, LL));
	AddSpace(New CSpace(AN,  "FCLS", "   Loop Supply Probe Err Fault Count",10,	FCLS,	 0, LL));
	AddSpace(New CSpace(AN,  "FCSO", "   SeawaterOut Probe Err Fault Count",10,	FCSO,	 0, LL));
	AddSpace(New CSpace(AN,  "FCSI", "   SeawaterIn Probe Err Fault Count",	10,	FCSI,	 0, LL));
	AddSpace(New CSpace(AN,  "FCLR", "   Loop Rtn Probe Err Fault Count",	10,	FCLR,	 0, LL));
	AddSpace(New CSpace(AN,  "FCNC", "   Network Conn Error Fault Count",	10,	FCNC,	 0, LL));
	AddSpace(New CSpace(AN,  "FCLD", "   Loop Temp Diff. Error Fault Count",10,	FCLD,	 0, LL));
	AddSpace(New CSpace(AN,  "FCSD", "   Seawater Temp Diff Err Fault Count",10,	FCSD,	 0, LL));
	AddSpace(New CSpace(AN,  "FCIH", "   Immr. Htr. Hi Temp Err Fault Count",10,	FCIH,	 0, LL));
	AddSpace(New CSpace(AN,  "FCIP", "   Immr. Htr. Probe Error Fault Count",10,	FCIP,	 0, LL));
	AddSpace(New CSpace(AN,  "FADD", "   Additional Fault Bits",		10,	FADD,	 0, LL));

	AddSpace(New CSpace(SPH, " ",	 "INPUT/OUTPUT LOG...",			10,	SPIOLOG, 0, LL));
	AddSpace(New CSpace(AN,  "SPN",  "   Min Suction Pressure",		10,	SPN,	 0, LL));
	AddSpace(New CSpace(AN,  "DPX",  "   Max Discharge Pressure",		10,	DPX,	 0, LL));
	AddSpace(New CSpace(AN,  "CCX",  "   Max Compressor current",		10,	CCX,	 0, LL));
	AddSpace(New CSpace(AN,  "SWPN", "   Min Seawater Pressure",		10,	SWPN,	 0, LL));
	AddSpace(New CSpace(AN,  "LPN",  "   Min Loopwater pressure",		10,	LPN,	 0, LL));
	AddSpace(New CSpace(AN,  "SPCX", "   Max Seawater Pump Current",	10,	SPCX,	 0, LL));
	AddSpace(New CSpace(AN,  "LPCX", "   Max Loop Pump Current",		10,	LPCX,	 0, LL));
	AddSpace(New CSpace(AN,  "ACLN", "   Min AC voltage",			10,	ACLN,	 0, LL));
	AddSpace(New CSpace(AN,  "ACLX", "   Max AC voltage",			10,	ACLX,	 0, LL));
	AddSpace(New CSpace(AN,  "RLTN", "   Min Return Loop Temp",		10,	RLTN,	 0, LL));
	AddSpace(New CSpace(AN,  "RLTX", "   Max Return Loop Temp",		10,	RLTX,	 0, LL));
	AddSpace(New CSpace(AN,  "LSTN", "   Min Loop Supply Temp",		10,	LSTN,	 0, LL));
	AddSpace(New CSpace(AN,  "LSTX", "   Max Loop Supply Temp",		10,	LSTX,	 0, LL));
	AddSpace(New CSpace(AN,  "SITN", "   Min Seawater In Temp",		10,	SITN,	 0, LL));
	AddSpace(New CSpace(AN,  "SITX", "   Max Seawater In Temp",		10,	SITX,	 0, LL));
	AddSpace(New CSpace(AN,  "SOTN", "   Min Seawater Out Temp",		10,	SOTN,	 0, LL));
	AddSpace(New CSpace(AN,  "SOTX", "   Max Seawater Out Temp",		10,	SOTX,	 0, LL));
	AddSpace(New CSpace(AN,  "LOTN", "   Min Loop Out Temp",		10,	LOTN,	 0, LL));
	AddSpace(New CSpace(AN,  "LOTX", "   Max Loop Out Temp",		10,	LOTX,	 0, LL));
	AddSpace(New CSpace(AN,  "IHTN", "   Min Immersion Heater Temp",	10,	IHTN,	 0, LL));
	AddSpace(New CSpace(AN,  "IHTX", "   Max Immersion Heater Temp",	10,	IHTX,	 0, LL));

	AddSpace(New CSpace(SPH, " ", 	"GLOBAL PARAMETERS...",			10,	SPPARAM,FG, LL));
	AddSpace(New CSpace(HSP, "HSP",  "   Heating Setpoint",			10,	  1,	 6, LL));
	AddSpace(New CSpace(HDF, "HDF",  "   Heating Differential",		10,	  1,	 6, LL));
	AddSpace(New CSpace(CSP, "CSP",  "   Cooling Setpoint",			10,	  1,	 6, LL));
	AddSpace(New CSpace(CDF, "CDF",  "   Cooling Differential",		10,	  1,	 6, LL));
	AddSpace(New CSpace(AN,  "LFLT", "   Loop Freeze Limit Temp",		10,	LFLT,	 0, LL));
	AddSpace(New CSpace(AN,  "LFLD", "   Loop Freeze Limit Differential",	10,	LFLD,	 0, LL));
	AddSpace(New CSpace(AN,  "LHLT", "   Loop Hi Limit Temp",		10,	LHLT,	 0, LL));
	AddSpace(New CSpace(AN,  "LHLD", "   Loop Hi Limit Differential",	10,	LHLD,	 0, LL));
	AddSpace(New CSpace(AN,  "SFLT", "   Seawater Freeze Limit Temp",	10,	SFLT,	 0, LL));
	AddSpace(New CSpace(AN,  "SFLD", "   Seawater Freeze Limit Diff.",	10,	SFLD,	 0, LL));
	AddSpace(New CSpace(AN,  "SPL",  "   Suction pressure Limit",		10,	SPL, 	 0, LL));
	AddSpace(New CSpace(AN,  "SPLD", "   Suction pressure Limit Diff.",	10,	SPLD,	 0, LL));
	AddSpace(New CSpace(AN,  "DPL",  "   Discharge pressure Limit",		10,	DPL, 	 0, LL));
	AddSpace(New CSpace(AN,  "DPLD", "   Discharge pressure Limit Diff.",	10,	DPLD,	 0, LL));
	AddSpace(New CSpace(AN,  "IHLT", "   Immersion Heater Temp Limit",	10,	IHLT,	 0, LL));
	AddSpace(New CSpace(AN,  "CRD",  "   Compressor restart delay",		10,	CRD, 	 0, LL));
	AddSpace(New CSpace(AN,  "ERD",  "   Error retry delay",		10,	ERD, 	 0, LL));
	AddSpace(New CSpace(AN,  "NET",  "   Network Status",			10,	NET,	 0, BB));
	AddSpace(New CSpace(PGEN,"PGEN", "   Generic Item",			10,	  0,  6999, LL));

	AddSpace(New CSpace(SPH, " ",	 "GLOBAL CONTROL...",			10,	SPCNTRL,FG, LL));
	AddSpace(New CSpace(AN,  "SOFF", "   Off",				10,	SOFF,	 0, BB));
	AddSpace(New CSpace(AN,  "COOL", "   Cool",				10,	COOL,	 0, BB));
	AddSpace(New CSpace(AN,  "HEAT", "   Heat",				10,	HEAT,	 0, BB));
	AddSpace(New CSpace(AN,  "BOTH", "   Cool + Heat",			10,	BOTH,	 0, BB));
	AddSpace(New CSpace(AN,  "RESH", "   Resistance Heat",			10,	RESH,	 0, BB));
	AddSpace(New CSpace(AN,  "FAHR", "   Fahrenheit",			10,	FAHR,	 0, BB));
	AddSpace(New CSpace(AN,  "CENT", "   Centigrade",			10,	CENT,	 0, BB));
	AddSpace(New CSpace(AN,  "HILD", "   High Load Limits",			10,	HILD,	 0, BB));
	AddSpace(New CSpace(AN,  "LOLD", "   Low Load Limits",			10,	LOLD,	 0, BB));
	AddSpace(New CSpace(AN,  "INIT", "   Initialize",			10,	CINIT,	 0, BB));
	AddSpace(New CSpace(AN,  "RDEF", "   Restore Defaults",			10,	RDEF,	 0, BB));
	AddSpace(New CSpace(AN,  "BUZZ", "   Buzzer",				10,	BUZZ,	 0, BB));
	AddSpace(New CSpace(AN,  "CALL", "   Clear All Logs",			10,	CALL,	 0, BB));
	AddSpace(New CSpace(AN,  "CAFH", "   Clear All Fault Histories",	10,	CAFH,	 0, BB));
	AddSpace(New CSpace(AN,  "CAIO", "   Clear All I/O Limits",		10,	CAIO,	 0, BB));
	AddSpace(New CSpace(AN,  "CLPH", "   Clear Loop Pump Hours",		10,	CLPH,	 0, BB));
	AddSpace(New CSpace(AN,  "CSPH", "   Clear Seawater Pump Hours",	10,	CSPH,	 0, BB));
	AddSpace(New CSpace(AN,  "CAHH", "   Clear All Heater Hours",		10,	CAHH,	 0, BB));
	AddSpace(New CSpace(AN,  "CACH", "   Clear All Compressor Hours",	10,	CACH,	 0, BB));
	AddSpace(New CSpace(AN,  "CDFT", "   Clear Displayed Fault",		10,	CDFT,	 0, BB));
	}

// Helpers
UINT CCruisairDriver::SetLocalStatus(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case  SPH:	return NOTVALID;

		case   AN:	return IsGlobalItem(pSpace->m_uMinimum) ? ISGLOBAL : ISLOCAL;

		case PGEN:	return ISGLOBAL | ISLOCAL;
		}

	return ISGLOBAL;
	}

BOOL CCruisairDriver::IsGlobalItem(UINT uMin)
{
	switch( uMin ) {

		case LFLT:
		case LFLD:
		case LHLT:
		case LHLD:
		case SFLT:
		case SFLD:
		case SPL:
		case SPLD:
		case DPL:
		case DPLD:
		case IHLT:
		case CRD:
		case ERD:
		case NET:
		case SOFF:
		case COOL:
		case HEAT:
		case BOTH:
		case RESH:
		case FAHR:
		case CENT:
		case HILD:
		case LOLD:
		case CINIT:
		case RDEF:
		case BUZZ:
		case CALL:
		case CAFH:
		case CAIO:
		case CLPH:
		case CSPH:
		case CAHH:
		case CACH:
		case CDFT:
			return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Cruisair Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CCruisairAddrDialog, CStdAddrDialog);
		
// Constructor

CCruisairAddrDialog::CCruisairAddrDialog(CCruisairDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Chiller = pConfig->GetDataAccess("Chiller")->ReadInteger(pConfig);

	m_fPart = fPart;

	if( m_uAllowSpace < SPCHILL || m_uAllowSpace > SPCNTRL ) m_uAllowSpace = SPCNTRL;
	}

// Message Map

AfxMessageMap(CCruisairAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CCruisairAddrDialog)
	};

// Message Handlers

BOOL CCruisairAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	SetFunctionText(m_pSpace ? m_pSpace->m_uTable : AN );

	SetAllow();
	
	if( !m_fPart ) {

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			UINT uT = m_pSpace->m_uTable;

			if( uT != SPH ) {

				ShowAddress(Addr);
				ShowDetails();
				SetDlgFocus(uT < AN ? 2002 : 1001);
				}

			else {
				NoAddress(FALSE);
				}
			}

		return !(BOOL(m_pSpace));
		}
	else {
		LoadType();

		ShowAddress(Addr);

		if( TRUE ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				ShowAddress(Addr);
				}
			}

		SetAddressFocus();

		return FALSE;
		}
	}

// Notification Handlers

void CCruisairAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CCruisairAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		UINT uAllow = m_uAllowSpace;

		SetAllow();

		BOOL fAllowChange = uAllow != m_uAllowSpace;

		CString sOffset = GetAddressText();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			m_pSpace->GetMinimum(Addr);

			SetFunctionText(m_pSpace->m_uTable);

			GetDlgItem(2001).SetWindowText( m_pSpace->m_uTable != SPH ? m_pSpace->m_Prefix : "");

			sOffset = m_pSpace->GetValueAsText(m_pSpace->m_uMinimum);

			if( fAllowChange || m_pSpace->m_uMinimum >= SPCHILL ) LoadList();

			UINT uFocus = 1001;

			if( (m_pSpace->m_uTable < SPH) ) {

				GetDlgItem(2002).SetWindowText(sOffset);
				GetDlgItem(2002).EnableWindow(TRUE);
				uFocus = 2002;
				ShowDetails();
				ShowAddress(Addr);
				}

			else NoAddress(FALSE);

			SetDlgFocus(uFocus);

			return;
			}
		}
	else {
		m_uAllowSpace = SPCNTRL;

		if( m_pSpace ) LoadList();

		SetFunctionText(AN);

		NoAddress(TRUE);

		m_pSpace = NULL;
		}
	}

BOOL CCruisairAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( (m_pSpace->m_uTable == SPH) ) {

			if( m_fPart ) return FALSE;

			m_uAllowSpace = m_pSpace->m_uMinimum;

			LoadList();
			ClearDetails();

			return TRUE;
			}

		CString Text = m_pSpace->m_Prefix;

		if( m_pSpace->m_uTable < AN ) {

			Text += GetDlgItem(2002).GetWindowText();
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables

BOOL CCruisairAddrDialog::AllowSpace(CSpace *pSpace)
{
	UINT uTable = pSpace->m_uTable;

	if( uTable == SPH ) return TRUE;

	UINT uCommand = uTable >= AN ? pSpace->m_uMinimum : uTable;

	return SelectList( uCommand, m_uAllowSpace );
	}

// Helpers

BOOL CCruisairAddrDialog::SelectList(UINT uCommand, UINT uAllow)
{
	switch( uAllow ) {

		case SPCHILL:	return ( ListMember(uCommand) == SPCHILL );
		case SPPARAM:	return ( ListMember(uCommand) == SPPARAM );
		case SPFAULT:	return ( ListMember(uCommand) == SPFAULT );
		case SPIOLOG:	return ( ListMember(uCommand) == SPIOLOG );
		case SPCNTRL:	return ( ListMember(uCommand) == SPCNTRL );
		}

	return FALSE;
	}

UINT CCruisairAddrDialog::ListMember(UINT uCommand)
{
	switch( uCommand ) {

		case CSN:
		case LFS:
		case SVN:
		case SIT:
		case SOT:
		case LST:
		case LRT:
		case LFT:
		case RLP:
		case RHP:
		case LPR:
		case SPR:
		case CCU:
		case SPC:
		case LPC:
		case ACV:
		case CRT:
		case RVT:
		case SPT:
		case LPT:
		case HRT:
		case MODE:
		case DLYT:
		case IMT:
		case FDB:
		case TGR:
		case ISN:
		case DSN:
		case TGC:
		case C1LG:
		case C1FH:
		case C1IO:
		case C1HH:
		case C1CH:
			return SPCHILL;

		case LFLT:
		case LFLD:
		case LHLT:
		case LHLD:
		case SFLT:
		case SFLD:
		case SPL:
		case SPLD:
		case DPL:
		case DPLD:
		case HSP:
		case HDF:
		case CSP:
		case CDF:
		case IHLT:
		case CRD:
		case ERD:
		case NET:
		case PGEN:
			return SPPARAM;

		case FCN:
		case FAB:
		case ACL:
		case FSW:
		case HDS:
		case LLT:
		case LHT:
		case SLT:
		case EEC:
		case LOP:
		case LSP:
		case FCT:
		case FCAC:
		case FCFS:
		case FCLF:
		case FCHF:
		case FCLL:
		case FCLH:
		case FCSL:
		case FCEE:
		case FCLO:
		case FCLS:
		case FCSO:
		case FCSI:
		case FCLR:
		case FCNC:
		case FCLD:
		case FCSD:
		case FCIH:
		case FCIP:
		case FADD:
			return SPFAULT;


		case SPN:
		case DPX:
		case CCX:
		case SWPN:
		case LPN:
		case SPCX:
		case LPCX:
		case ACLN:
		case ACLX:
		case RLTN:
		case RLTX:
		case LSTN:
		case LSTX:
		case SITN:
		case SITX:
		case SOTN:
		case SOTX:
		case LOTN:
		case LOTX:
		case IHTN:
		case IHTX:
			return SPIOLOG;

		case SOFF:
		case COOL:
		case HEAT:
		case BOTH:
		case RESH:
		case FAHR:
		case CENT:
		case HILD:
		case LOLD:
		case CINIT:
		case RDEF:
		case BUZZ:
		case CALL:
		case CAFH:
		case CAIO:
		case CLPH:
		case CSPH:
		case CAHH:
		case CACH:
		case CDFT:

			return SPCNTRL;
		}

	return SPCNTRL;
	}

void CCruisairAddrDialog::SetAllow()
{
	if( !m_pSpace ) {

		m_uAllowSpace = SPCNTRL;
		return;
		}

	UINT uTable = m_pSpace->m_uTable;
	UINT uMin   = m_pSpace->m_uMinimum;

	if( uTable == SPH ) {

		m_uAllowSpace = uMin;
		return;
		}

	UINT uCommand = uTable >= AN ? uMin : uTable;

	for( UINT i = SPCHILL; i <= SPCNTRL; i++ ) {

		if( SelectList(uCommand, i) ) {

			m_uAllowSpace = i;

			return;
			}
		}
	}

void CCruisairAddrDialog::NoAddress(BOOL fPutNone)
{
	if( fPutNone ) ClearAddress();
	ClearDetails();
	GetDlgItem(2002).SetWindowText("");
	GetDlgItem(2002).EnableWindow(FALSE);
	SetDlgFocus(1001);
	}

void CCruisairAddrDialog::SetFunctionText(UINT uTable)
{
	CString s = "";

	if( uTable == PGEN ) {

		s.Printf( !m_Chiller ? "Global" : "Chiller %d", m_Chiller );

		s += " Command Number";
		}

	else {
		if( !m_Chiller && uTable >= HSP && uTable <= CDF ) s = "Stage Number";
		}

	GetDlgItem(2000).SetWindowText( s );
	}

// End of File
