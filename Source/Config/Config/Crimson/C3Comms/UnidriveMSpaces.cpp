#include "intern.hpp"

#include "UnidriveM.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Common Spaces
//

// Address format -- Menu.Item: mm_iii

void CUnidriveMDeviceOptions::AddHeadersCommon(CMenuMap &MenuMap)
{
	MenuMap.Insert(MENU_MODBUS, New CUnidriveMMenu("Modbus", "User Defined Parameters", MENU_MODBUS));
	}

// Space Headers

void CUnidriveMDeviceOptions::AddHeadersRFCA(CMenuMap &MenuMap)
{
	MenuMap.Insert(1,  New CUnidriveMMenu("Menu 01", "Speed References", 1));
	MenuMap.Insert(2,  New CUnidriveMMenu("Menu 02", "Speed Ramps", 2));
	MenuMap.Insert(3,  New CUnidriveMMenu("Menu 03", "Speed Control and Position Feedback", 3));
	MenuMap.Insert(4,  New CUnidriveMMenu("Menu 04", "Torque and current control", 4));
	MenuMap.Insert(5,  New CUnidriveMMenu("Menu 05", "Motor Control", 5));
	MenuMap.Insert(6,  New CUnidriveMMenu("Menu 06", "Sequencer and Clock", 6));
	MenuMap.Insert(7,  New CUnidriveMMenu("Menu 07", "Analog I/O", 7));
	MenuMap.Insert(8,  New CUnidriveMMenu("Menu 08", "Digital I/O", 8));
	MenuMap.Insert(9,  New CUnidriveMMenu("Menu 09", "User Functions 1", 9));
	MenuMap.Insert(10, New CUnidriveMMenu("Menu 10", "Status and Trips", 10));
	MenuMap.Insert(11, New CUnidriveMMenu("Menu 11", "Miscellaneous", 11));
	MenuMap.Insert(12, New CUnidriveMMenu("Menu 12", "User Functions 2 and Brake Control", 12));
	MenuMap.Insert(13, New CUnidriveMMenu("Menu 13", "Standard Motion Controller", 13));
	MenuMap.Insert(14, New CUnidriveMMenu("Menu 14", "User PID Controller", 14));
	MenuMap.Insert(18, New CUnidriveMMenu("Menu 18", "Application Menu 1", 18));
	MenuMap.Insert(19, New CUnidriveMMenu("Menu 19", "Application Menu 2", 19));
	MenuMap.Insert(20, New CUnidriveMMenu("Menu 20", "Application Menu 3", 20));
	MenuMap.Insert(21, New CUnidriveMMenu("Menu 21", "Motor 2 Parameters", 21));
	MenuMap.Insert(22, New CUnidriveMMenu("Menu 22", "Menu 0 Set-up", 22));
	MenuMap.Insert(31, New CUnidriveMMenu("Menu 31", "AMC General Set-up", 31));
	MenuMap.Insert(32, New CUnidriveMMenu("Menu 32", "AMC Master Position", 32));
	MenuMap.Insert(33, New CUnidriveMMenu("Menu 33", "AMC Slave Position", 33));
	MenuMap.Insert(34, New CUnidriveMMenu("Menu 34", "AMC Reference Selector", 34));
	MenuMap.Insert(35, New CUnidriveMMenu("Menu 35", "AMC Cam", 35));
	MenuMap.Insert(36, New CUnidriveMMenu("Menu 36", "AMC Cam Table", 36));
	MenuMap.Insert(37, New CUnidriveMMenu("Menu 37", "AMC Electronic Gear Box", 37));
	MenuMap.Insert(38, New CUnidriveMMenu("Menu 38", "AMC Profile Generator", 38));
	MenuMap.Insert(39, New CUnidriveMMenu("Menu 39", "AMC Position Control Loop", 39));
	MenuMap.Insert(40, New CUnidriveMMenu("Menu 40", "AMC Homing System", 40));
	MenuMap.Insert(41, New CUnidriveMMenu("Menu 41", "AMC Control and Status", 41));
	}

void CUnidriveMDeviceOptions::AddHeadersRFCS(CMenuMap &MenuMap)
{
	MenuMap.Insert(1,  New CUnidriveMMenu("Menu 01", "Speed References", 1));
	MenuMap.Insert(2,  New CUnidriveMMenu("Menu 02", "Speed Ramps", 2));
	MenuMap.Insert(3,  New CUnidriveMMenu("Menu 03", "Speed Control and Position Feedback", 3));
	MenuMap.Insert(4,  New CUnidriveMMenu("Menu 04", "Torque and current control", 4));
	MenuMap.Insert(5,  New CUnidriveMMenu("Menu 05", "Motor Control", 5));
	MenuMap.Insert(6,  New CUnidriveMMenu("Menu 06", "Sequencer and Clock", 6));
	MenuMap.Insert(7,  New CUnidriveMMenu("Menu 07", "Analog I/O", 7));
	MenuMap.Insert(8,  New CUnidriveMMenu("Menu 08", "Digital I/O", 8));
	MenuMap.Insert(9,  New CUnidriveMMenu("Menu 09", "User Functions 1", 9));
	MenuMap.Insert(10, New CUnidriveMMenu("Menu 10", "Status and Trips", 10));
	MenuMap.Insert(11, New CUnidriveMMenu("Menu 11", "Miscellaneous", 11));
	MenuMap.Insert(12, New CUnidriveMMenu("Menu 12", "User Functions 2 and Brake Control", 12));
	MenuMap.Insert(13, New CUnidriveMMenu("Menu 13", "Standard Motion Controller", 13));
	MenuMap.Insert(14, New CUnidriveMMenu("Menu 14", "User PID Controller", 14));
	MenuMap.Insert(18, New CUnidriveMMenu("Menu 18", "Application Menu 1", 18));
	MenuMap.Insert(19, New CUnidriveMMenu("Menu 19", "Application Menu 2", 19));
	MenuMap.Insert(20, New CUnidriveMMenu("Menu 20", "Application Menu 3", 20));
	MenuMap.Insert(21, New CUnidriveMMenu("Menu 21", "Motor 2 Parameters", 21));
	MenuMap.Insert(22, New CUnidriveMMenu("Menu 22", "Menu 0 Set-up", 22));
	MenuMap.Insert(31, New CUnidriveMMenu("Menu 31", "AMC General Set-up", 31));
	MenuMap.Insert(32, New CUnidriveMMenu("Menu 32", "AMC Master Position", 32));
	MenuMap.Insert(33, New CUnidriveMMenu("Menu 33", "AMC Slave Position", 33));
	MenuMap.Insert(34, New CUnidriveMMenu("Menu 34", "AMC Reference Selector", 34));
	MenuMap.Insert(35, New CUnidriveMMenu("Menu 35", "AMC Cam", 35));
	MenuMap.Insert(36, New CUnidriveMMenu("Menu 36", "AMC Cam Table", 36));
	MenuMap.Insert(37, New CUnidriveMMenu("Menu 37", "AMC Electronic Gear Box", 37));
	MenuMap.Insert(38, New CUnidriveMMenu("Menu 38", "AMC Profile Generator", 38));
	MenuMap.Insert(39, New CUnidriveMMenu("Menu 39", "AMC Position Control Loop", 39));
	MenuMap.Insert(40, New CUnidriveMMenu("Menu 40", "AMC Homing System", 40));
	MenuMap.Insert(41, New CUnidriveMMenu("Menu 41", "AMC Control and Status", 41));
	}

void CUnidriveMDeviceOptions::AddHeadersOpenLoop(CMenuMap &MenuMap)
{
	MenuMap.Insert(1,  New CUnidriveMMenu("Menu 01", "Frequency References", 1));
	MenuMap.Insert(2,  New CUnidriveMMenu("Menu 02", "Frequency Ramps", 2));
	MenuMap.Insert(3,  New CUnidriveMMenu("Menu 03", "Frequency Slaving and Position Feedback", 3));
	MenuMap.Insert(4,  New CUnidriveMMenu("Menu 04", "Torque and Current control", 4));
	MenuMap.Insert(5,  New CUnidriveMMenu("Menu 05", "Motor Control", 5));
	MenuMap.Insert(6,  New CUnidriveMMenu("Menu 06", "Sequencer and Clock", 6));
	MenuMap.Insert(7,  New CUnidriveMMenu("Menu 07", "Analog I/O", 7));
	MenuMap.Insert(8,  New CUnidriveMMenu("Menu 08", "Digital I/O", 8));
	MenuMap.Insert(9,  New CUnidriveMMenu("Menu 09", "User Functions 1", 9));
	MenuMap.Insert(10, New CUnidriveMMenu("Menu 10", "Status and Trips", 10));
	MenuMap.Insert(11, New CUnidriveMMenu("Menu 11", "Miscellaneous", 11));
	MenuMap.Insert(12, New CUnidriveMMenu("Menu 12", "User Functions 2 and Brake Control", 12));
	MenuMap.Insert(13, New CUnidriveMMenu("Menu 13", "Standard Motion Controller", 13));
	MenuMap.Insert(14, New CUnidriveMMenu("Menu 14", "User PID Controller", 14));
	MenuMap.Insert(18, New CUnidriveMMenu("Menu 18", "Application Menu 1", 18));
	MenuMap.Insert(19, New CUnidriveMMenu("Menu 19", "Application Menu 2", 19));
	MenuMap.Insert(20, New CUnidriveMMenu("Menu 20", "Application Menu 3", 20));
	MenuMap.Insert(21, New CUnidriveMMenu("Menu 21", "Motor 2 Parameters", 21));
	MenuMap.Insert(22, New CUnidriveMMenu("Menu 22", "Menu 0 Set-up", 22));	
	}

void CUnidriveMDeviceOptions::AddHeadersRegen(CMenuMap &MenuMap)
{
	MenuMap.Insert(3,  New CUnidriveMMenu("Menu 03", "Regen Control", 3));
	MenuMap.Insert(4,  New CUnidriveMMenu("Menu 04", "CurrentControl",	4));
	MenuMap.Insert(5,  New CUnidriveMMenu("Menu 05", "Regen Status", 5));
	MenuMap.Insert(6,  New CUnidriveMMenu("Menu 06", "Sequencer and Clock", 6));
	MenuMap.Insert(7,  New CUnidriveMMenu("Menu 07", "Analog I/O", 7));
	MenuMap.Insert(8,  New CUnidriveMMenu("Menu 08", "Regen Digital I/O", 8));
	MenuMap.Insert(9,  New CUnidriveMMenu("Menu 09", "User Functions 1", 9));
	MenuMap.Insert(10, New CUnidriveMMenu("Menu 10", "Status and Trips", 10));
	MenuMap.Insert(11, New CUnidriveMMenu("Menu 11", "Miscellaneous", 11));
	MenuMap.Insert(12, New CUnidriveMMenu("Menu 12", "User Functions 2", 12));
	MenuMap.Insert(14, New CUnidriveMMenu("Menu 14", "User PID Controller", 14));
	MenuMap.Insert(18, New CUnidriveMMenu("Menu 18", "Application Menu 1", 18));
	MenuMap.Insert(19, New CUnidriveMMenu("Menu 19", "Application Menu 2", 19));
	MenuMap.Insert(20, New CUnidriveMMenu("Menu 20", "Application Menu 3", 20));
	MenuMap.Insert(21, New CUnidriveMMenu("Menu 21", "Regen 2 Parameters", 21));
	MenuMap.Insert(22, New CUnidriveMMenu("Menu 22", "Menu 0 Set-up", 22));
	}

// End of File
