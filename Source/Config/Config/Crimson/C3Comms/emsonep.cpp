
#include "intern.hpp"

#include "emsonep.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emerson Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonDriverOptions, CUIItem);

// Constructor

CEmersonDriverOptions::CEmersonDriverOptions(void)
{
	m_Delay = 0;
	}

// UI Management

void CEmersonDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CEmersonDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Delay));

	return TRUE;
	}

// Meta Data Creation

void CEmersonDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Delay);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonEPDeviceOptions, CUIItem);

// Constructor

CEmersonEPDeviceOptions::CEmersonEPDeviceOptions(void)
{
	m_Unit     = 1;

	m_EmID     = DVEPB;

	m_Type     = DVSEPB;

	m_DispMode = DMPRE;
	}

// UI Management

void CEmersonEPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "DispMode" || Tag == "Type") {

		BOOL fShow = FALSE;

		m_EmID     = GetID(m_Type);

		switch( m_Type ) {

			case DVSEPB:
			case DVSEPI:
				m_DispMode = DMPRE;
				break;

			case DVSSP:
			case DVSSK:
			case DVSGP:
			case DVSST:
			case DVSMP:
				fShow = TRUE;
				break;

			case DVSMC:
			case DVSEPP:
			case DVSBR:
				m_DispMode = DMREG;
				break;
			}

		pWnd->UpdateUI("Type");
		pWnd->UpdateUI("DispMode");
		pWnd->EnableUI("DispMode", fShow);
		}
	}

// Download Support

BOOL CEmersonEPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	m_EmID = GetID(m_Type);

	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_EmID));

	return TRUE;
	}

// Meta Data Creation

void CEmersonEPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Unit);
	Meta_AddInteger(Type);
	Meta_AddInteger(DispMode);
//	Meta_AddInteger(EmID);
       	}

UINT CEmersonEPDeviceOptions::GetType(UINT uID)
{
	switch( uID ) {

		case DVEPP: return DVSEPP;
		case DVSP:  return DVSSP;
		case DVSK:  return DVSSK;
		case DVGP:  return DVSGP;
		case DVST:  return DVSST;
		case DVMC:  return DVSMC;
		case DVBR:  return DVSBR;
		case DVMP:  return DVSMP;
		case DVEPI: return DVSEPI;
		}

	return DVSEPB;
	}

UINT CEmersonEPDeviceOptions::GetID(UINT uType)
{
	switch( uType ) {

		case DVSEPP: return DVEPP;
		case DVSSP:  return DVSP;
		case DVSSK:  return DVSK;
		case DVSGP:  return DVGP;
		case DVSST:  return DVST;
		case DVSMC:  return DVMC;
		case DVSBR:  return DVBR;
		case DVSMP:  return DVMP;
		case DVSEPI :return DVEPI;
		}

	return DVEPB;
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Driver
//

// Instantiator

ICommsDriver *	Create_EmersonEPDriver(void)
{
	return New CEmersonEPDriver;
	}

// Constructor

CEmersonEPDriver::CEmersonEPDriver(void)
{
	m_wID		= 0x4034;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Nidec - Control Techniques";
	
	m_DriverName	= "Pre-Configured Modbus";
	
	m_Version	= "2.50";
	
	m_ShortName	= "Pre-Configured Modbus";

	m_DevRoot	= "DRV";

	m_uListSel	= SELMB;

	m_DeviceSelect	= 1;

	m_fDriverType	= FALSE;

	InitSpaces();

	AddSpaces(FALSE);

	AddSKSpaces();
	AddValidateSpacesSP();
	}

CEmersonEPDriver::~CEmersonEPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT	CEmersonEPDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CEmersonEPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEmersonEPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEmersonDriverOptions);
	}

// Configuration
CLASS CEmersonEPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonEPDeviceOptions);
	}

// Address Management

BOOL CEmersonEPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	if( fPart && Addr.a.m_Table >= TLMB && Addr.a.m_Table < AN ) return FALSE;
	
	CEmersonEPAddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL CEmersonEPDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace || !Text[0] ) return FALSE;

	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Type   = pSpace->m_uType;
	Addr.a.m_Extra  = m_uListSel;

	UINT uOffset    = tatoi(Text);

	BOOL fSol       = IsSolutionMod(pSpace->m_uTable);

	if( pSpace->m_uTable != AN ) {

		if( uOffset < pSpace->m_uMinimum || uOffset > pSpace->m_uMaximum ) {

			CString sE;

			if( fSol ) {

				sE.Printf( "%s%2.2d - %s%2.2d",

					pSpace->m_Prefix,
					pSpace->m_uMinimum,
					pSpace->m_Prefix,
					pSpace->m_uMaximum
					);
				}

			else {
				sE.Printf( "%s%4.4d - %s%4.4d",

					pSpace->m_Prefix,
					pSpace->m_uMinimum,
					pSpace->m_Prefix,
					pSpace->m_uMaximum
					);
				}

			Error.Set( sE, 0 );

			return FALSE;
			}

		if( fSol ) {

			Text.MakeUpper();

			if( Text.Find(TEXT("LONG")) < NOTHING ) {
				
				Addr.a.m_Type = LL;
				}
			}

		Addr.a.m_Offset = uOffset;

		return TRUE;
		}

	if( IsCommanderP(pConfig) ) { // Commander Premapped

		Addr.a.m_Offset = 50000 + (uOffset % 10000);
		}

	else Addr.a.m_Offset = uOffset;

	return TRUE;
	}

BOOL CEmersonEPDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	UINT Device;

	Device = pConfig->GetDataAccess("Type")->ReadInteger(pConfig);

	Device = GetID(Device);

	SetDeviceSelect(&Device);

	
	// Load the correct spaces for the EP-B or EP-I
	if( m_DeviceSelect == DVEPB ) {

		AddSpacesB();
		}

	if ( m_DeviceSelect == DVEPI ) {

		AddSpacesI();
		}

	CAddress A;

	A.m_Ref = Addr.m_Ref;

	if( A.a.m_Table == AN ) {

		UINT uBaseAddr = A.a.m_Offset % 10000;

		if( IsCommanderP(pConfig) ) { // Commander Premapped

			A.a.m_Offset = 50000 + uBaseAddr;
			}

		else {
			if( A.a.m_Offset >= 50000 ) A.a.m_Offset = 40000 + uBaseAddr;
			}
		}

	CSpace *pSpace = GetSpace(A);

	if( !pSpace ) return FALSE;

	if( pSpace->m_uTable == AN ) {

		UINT uOffset = pSpace->m_uMinimum;

		if( uOffset > 49999 ) uOffset = 40000 + (uOffset % 10000);

		Text.Printf( "%s%5.5d",
			pSpace->m_Prefix,
			uOffset
			);
		}

	else {
		CString sLong;

		switch( Addr.a.m_Table ) {

			case GMBO:
			case GMBI:
			case GMBA:
			case GMBB:
			case GMBC:
			case GMBD:
			case GMBE:
			case GMBF:
			case MCO:
			case MCI:
			case MCH:
				Text.Printf( "%s%4.4d",
				pSpace->m_Prefix,
				Addr.a.m_Offset
				);

				return TRUE;

			case TLKY:
				if( Addr.a.m_Offset == 0xA ) Text.Printf("N/A");
				else if( Addr.a.m_Offset == 0xF ) Text.Printf("N/A");
				else {
					Text.Printf( "%X",
						Addr.a.m_Offset
						);
					}

				return TRUE;

			case M15:
			case M16:
			case M17:
				sLong = A.a.m_Type == LL ? "LONG" : "";

				 Text.Printf( "%s%2.2d%s",
					 pSpace->m_Prefix,
					 Addr.a.m_Offset,
					 sLong
					 );

				 return TRUE;

			default:
				Text.Printf( "%s%d",
					pSpace->m_Prefix,
					Addr.a.m_Offset
					);

				return TRUE;
			}
		}

	return TRUE;
	}

BOOL CEmersonEPDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Listing Control
void CEmersonEPDriver::SetListSelect(UINT *pListSelect)
{
	m_uListSel = (*pListSelect) & 0xF;
	}

void CEmersonEPDriver::SetDeviceSelect(UINT *pDeviceSelect)
{
	m_DeviceSelect = *pDeviceSelect;

	}

void CEmersonEPDriver::GetLoaded(UINT *pLoaded)
{
	UINT u = 0;

	if( m_fM15Loaded ) u |= 1;
	if( m_fM16Loaded ) u |= 2;
	if( m_fM17Loaded ) u |= 4;
	if( m_fMCLoaded  ) u |= 8;

	*pLoaded = u;
	}

void CEmersonEPDriver::InitSpaces(void)
{
	DeleteAllSpaces();

	ClearLoaded();
	}

// Space Selection
void CEmersonEPDriver::SetIBSpaces(BOOL fDialog)
{

	AddSpacesIB();
	AddSpacesMB();

	if( fDialog ) {

		AddSpacesIBH();
		AddPreHSpaces();
		}
	}

void CEmersonEPDriver::SetSPSpaces(BOOL fDialog)
{
	AddSPSpaces();
	AddSpacesMB();

	if( fDialog ) {

		AddSpacesComH(7);
		AddPreHSpaces();
		}
	}

void CEmersonEPDriver::SetSKSpaces(BOOL fDialog)
{
	AddSKSpaces();
	AddSpacesMB();

	if( fDialog ) {

		AddSpacesComH(1);
		AddPreHSpaces();
		}
	}

void CEmersonEPDriver::SetGPSpaces(BOOL fDialog)
{
	AddGPSpaces();
	AddSpacesMB();

	if( fDialog ) {

		AddSpacesComH(3);
		AddPreHSpaces();
		}
	}

void CEmersonEPDriver::SetSTSpaces(BOOL fDialog)
{
	AddSTSpaces();
	AddSpacesMB();

	if( fDialog ) {

		AddSpacesComH(7);
		AddPreHSpaces();
		}
	}

void CEmersonEPDriver::SetMPSpaces(BOOL fDialog)
{
	AddMPSpaces();
	AddSpacesMB();

	if( fDialog ) {

		AddSpacesComH(0x10);
		AddPreHSpaces();
		}
	}

void CEmersonEPDriver::SetMCSpaces(void)
{
	AddSpacesMC();
	}

void CEmersonEPDriver::SetNonPSpaces(void)
{
	AddSpacesMB();
	AddSpacesMBH();
	}

void CEmersonEPDriver::SetValidateSpaces(UINT uSelDev)
{
	if( uSelDev == DVSP ) {

		AddValidateSpacesSK();
		AddValidateSpacesMP();
		}

	if( uSelDev == DVSK ) {

		AddValidateSpacesSP();
		AddValidateSpacesMP();
		}

	if( uSelDev == DVMP ) {

		AddValidateSpacesSP();
		AddValidateSpacesSK();
		}
	}

BOOL CEmersonEPDriver::GetDriverType(void)
{
	return m_fDriverType;
	}


// Implementation

void CEmersonEPDriver::AddSpace( CSpace * pSpace )
{
	CAddress Addr;

	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Offset = pSpace->m_uMinimum;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = pSpace->m_uType;

	CSpace *p = GetSpace(Addr);

	if( p == NULL ) {

		CStdCommsDriver::AddSpace(pSpace);
		}
	}

// Generic Modbus
void CEmersonEPDriver::AddSpacesMB()
{
	// Generic Modbus Address Entry

	if( m_fMBLoaded ) return;

	AddSpace(New CSpace(GMBO,"O0",	"   Modbus 0nnnn Output Bits",			10,	1,	9999,	BB));
	AddSpace(New CSpace(GMBI,"I1",	"   Modbus 1nnnn Input Bits",			10,	1,	9999,	BB));
	AddSpace(New CSpace(GMBA,"A3",	"   Modbus 3nnnn Read-Only Words",		10,	1,	9999,	WW));
	AddSpace(New CSpace(GMBB,"D3",	"   Modbus 3nnnn 32 Bits (Addr, Addr+1)",	10,	1,	9999,	LL));
	AddSpace(New CSpace(GMBC,"L3",	"   Modbus 3nnnn 32 Bits (1 Address)",		10,	1,	9999,	LL));
	AddSpace(New CSpace(GMBD,"D4",	"   Modbus 4nnnn 32 Bits (Addr, Addr+1)",	10,	1,	9999,	LL));
	AddSpace(New CSpace(GMBE,"H4",	"   Modbus 4nnnn Words",			10,	1,	9999,	WW));
	AddSpace(New CSpace(GMBF,"L4",	"   Modbus 4nnnn 32 Bits (1 Address)",		10,	1,	9999,	LL));

	m_fMBLoaded = TRUE;
	}

// EPB/EPI Headers
void CEmersonEPDriver::AddSpacesIBH() {

	if( m_fIBHLoaded ) return;

	AddSpace(New CSpace(TDIA,"",	"Diagnostics Information...",			10,	0,	0,	LL));
	AddSpace(New CSpace(THOM,"",	"Homing Operations...",				10,	0,	0,	LL));
	AddSpace(New CSpace(TIOS,"",	"I/O Operations...",				10,	0,	0,	LL));
	AddSpace(New CSpace(TINX,"",	"Index Operations...",				10,	0,	0,	LL));
	AddSpace(New CSpace(TJOG,"",	"Jog Operations...",				10,	0,	0,	LL));
	AddSpace(New CSpace(TMOT,"",	"Motion Operations...",				10,	0,	0,	LL));
	AddSpace(New CSpace(TPUL,"",	"Pulse Operations...",				10,	0,	0,	LL));
	AddSpace(New CSpace(TREG,"",	"Registration Operations...",			10,	0,	0,	LL));
	AddSpace(New CSpace(TSTT,"",	"Status Information...",			10,	0,	0,	LL));
	AddSpace(New CSpace(TSYS,"",	"System Operations...",				10,	0,	0,	LL));

	m_fIBHLoaded = TRUE;
	}

// Modbus Headers
void CEmersonEPDriver::AddSpacesMBH() {

	if( m_fMBHLoaded ) return;

	AddSpace(New CSpace(THDO,"",	"0nnnn (DIGITAL OUTPUTS)...",			10,	0,	0,	LL));
	AddSpace(New CSpace(THDI,"",	"1nnnn (DIGITAL INPUTS)...",			10,	0,	0,	LL));
	AddSpace(New CSpace(THAI,"",	"3nnnn (ANALOG I/O)...",			10,	0,	0,	LL));
	AddSpace(New CSpace(THHR,"",	"4nnnn (HOLDING REGISTERS)...",			10,	0,	0,	LL));

	m_fMBHLoaded = TRUE;
	}

// SP Pre-Mapped Headers
void CEmersonEPDriver::AddSpacesComH(UINT uMenuSol) {

	if( m_fComHLoaded ) return;

	AddSpace(New CSpace(TM01, "_M01", "Speed Reference",				10,	0,	1,	LL));
	AddSpace(New CSpace(TM02, "_M02", "Ramps",					10,	0,	1,	LL));
	AddSpace(New CSpace(TM03, "_M03", "Speed loop / Frequency",			10,	0,	1,	LL));
	AddSpace(New CSpace(TM04, "_M04", "Torque & Current Control",			10,	0,	1,	LL));
	AddSpace(New CSpace(TM05, "_M05", "Motor Control",				10,	0,	1,	LL));
	AddSpace(New CSpace(TM06, "_M06", "Sequencer Control",				10,	0,	1,	LL));
	AddSpace(New CSpace(TM07, "_M07", "Analog IO",					10,	0,	1,	LL));
	AddSpace(New CSpace(TM08, "_M08", "Digital IO",					10,	0,	1,	LL));
	AddSpace(New CSpace(TM09, "_M09", "Logic / Mpot / Binary Sum",			10,	0,	1,	LL));
	AddSpace(New CSpace(TM10, "_M10", "Status Trips",				10,	0,	1,	LL));
	AddSpace(New CSpace(TM11, "_M11", "General",					10,	0,	1,	LL));
	AddSpace(New CSpace(TM12, "_M12", "Thresholds / Brake",				10,	0,	1,	LL));
	AddSpace(New CSpace(TM13, "_M13", "Position Control",				10,	0,	1,	LL));
	AddSpace(New CSpace(TM14, "_M14", "User PID",					10,	0,	1,	LL));

	if( uMenuSol & 1 )
		AddSpace(New CSpace(TM15, "_M15", "Solutions Module 15",		10,	0,	1,	LL));

	if( uMenuSol & 2 )
		AddSpace(New CSpace(TM16, "_M16", "Solutions Module 16",		10,	0,	1,	LL));

	if( uMenuSol & 4 )
		AddSpace(New CSpace(TM17, "_M17", "Solutions Module 17",		10,	0,	1,	LL));

	AddSpace(New CSpace(TM18, "_M18", "App Menu 1 - Word / Bit",			10,	0,	1,	LL));
	AddSpace(New CSpace(TM19, "_M19", "App Menu 2 - Word / Bit",			10,	0,	1,	LL));
	AddSpace(New CSpace(TM20, "_M20", "App Menu 3 - Word / DWord",			10,	0,	1,	LL));
	AddSpace(New CSpace(TM21, "_M21", "Motor 2",					10,	0,	1,	LL));

	if( uMenuSol & 0x10 ) {

		AddSpace(New CSpace(TM22, "_M22", "Menu0 Source",			10,	0,	1,	LL));
		AddSpace(New CSpace(TM23, "_M23", "Menu0 Functions",			10,	0,	1,	LL));
		}

	m_fComHLoaded = TRUE;
	}

// Function Headers
void CEmersonEPDriver::AddSpacesMFH(void) {

	if( m_fMFHLoaded ) return;

	AddSpace(New CSpace(TLMF,"",	"Machine Functions",				10,	0,	1,	LL));
	AddSpace(New CSpace(TLMB,"",	"Modbus Functions",				10,	0,	0,	LL));

	m_fMFHLoaded = TRUE;
	}

// Keyword Header
void CEmersonEPDriver::AddSpacesKYH(void) {

	if( m_fKYHLoaded ) return;

	AddSpace(New CSpace(TLKY, "",	"List By Keyword",				16,	0xA,	0xF,	LL));

	m_fKYHLoaded = TRUE;
	}

// Sort Alpha/Numeric Headers
void CEmersonEPDriver::AddSpacesSortH(void) {

	if( m_fSortHLoaded ) return;

	AddSpace(New CSpace(TLAL, "",	"Sort A - Z",					16,	0xA,	0xF,	LL));
	AddSpace(New CSpace(TLRO, "",	"Sort 0 - n",					16,	0xA,	0xF,	LL));

	m_fSortHLoaded = TRUE;
	}

// On Load
void CEmersonEPDriver::AddSpaces(BOOL fPart)
{
	AddSpacesIB();
	AddCommanderSpaces(fPart);
	AddSpacesMC();
	AddSpacesMB();
	}

// All Commander Premapped
void CEmersonEPDriver::AddCommanderSpaces(BOOL fPart)
{
	switch( m_DeviceSelect ) {

		case DVSK:
			AddSKSpaces();
			break;

		case DVSP:
			AddSPSpaces();
			break;

		case DVMP:
			AddMPSpaces();
			break;

		default:
			return;
		}

	if( !fPart ) SetValidateSpaces(m_DeviceSelect);
	}

// All SP Pre-mapped
void CEmersonEPDriver::AddSPSpaces(void)
{
	AddSpacesSP1();
	AddSpacesSM15();
	AddSpacesSM16();
	AddSpacesSM17();
	}

// All SK Pre-mapped
void CEmersonEPDriver::AddSKSpaces(void)
{
	AddSpacesSK1();
	AddSpacesSM15();
	}

// All GP Pre-mapped
void CEmersonEPDriver::AddGPSpaces(void)
{
	AddSpacesGP1();
	AddSPSpaces();
	AddSpacesSM15();
	AddSpacesSM16();
	}

// All ST Pre-mapped
void CEmersonEPDriver::AddSTSpaces(void)
{
	AddSpacesST1();
	AddSPSpaces();
	AddSpacesSM15();
	AddSpacesSM16();
	AddSpacesSM17();
	}

// All MP Pre-mapped
void CEmersonEPDriver::AddMPSpaces(void)
{
	AddSpacesMP1();
	}

// All Pre-Mapped Extra Headers
void CEmersonEPDriver::AddPreHSpaces(void)
{
	AddSpacesMFH();
	AddSpacesKYH();
	AddSpacesSortH();
	}

// Helpers
BOOL CEmersonEPDriver::IsCommanderP(CItem *pConfig)
{
	UINT uDevice    = pConfig->GetDataAccess("Type")->ReadInteger(pConfig);

	uDevice		= GetID(uDevice);

	UINT m_DispMode = pConfig->GetDataAccess("DispMode")->ReadInteger(pConfig);

	return uDevice >= DVSP && uDevice <= DVMP && m_DispMode == DMPRE;
	}

BOOL CEmersonEPDriver::IsSolutionMod(UINT uTable)
{
	return uTable >= M15 && uTable <= M17;
	}

void CEmersonEPDriver::ClearLoaded(void)
{
	m_fM15Loaded   = FALSE;
	m_fM16Loaded   = FALSE;
	m_fM17Loaded   = FALSE;
	m_fMCLoaded    = FALSE;
	m_fSPSLoaded   = FALSE;
	m_fSKSLoaded   = FALSE;
	m_fMPSLoaded   = FALSE;
	m_fVSPLoaded   = FALSE;
	m_fVSKLoaded   = FALSE;
	m_fVMPLoaded   = FALSE;
	m_fIBLoaded    = FALSE;
	m_fILoaded     = FALSE;
	m_fBLoaded     = FALSE;
	m_fMBLoaded    = FALSE;
	m_fIBHLoaded   = FALSE;
	m_fMBHLoaded   = FALSE;
	m_fComHLoaded  = FALSE;
	m_fMFHLoaded   = FALSE;
	m_fKYHLoaded   = FALSE;
	m_fSortHLoaded = FALSE;
	}

UINT CEmersonEPDriver::GetID(UINT uType)
{
	switch( uType ) {

		case DVSEPP: return DVEPP;
		case DVSSP:  return DVSP;
		case DVSSK:  return DVSK;
		case DVSGP:  return DVGP;
		case DVSST:  return DVST;
		case DVSMC:  return DVMC;
		case DVSBR:  return DVBR;
		case DVSMP:  return DVMP;
		case DVSEPI :return m_fDriverType ? DVMP : DVEPI;
		}

	return DVEPB;
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonEPTCPDeviceOptions, CUIItem);

// Constructor

CEmersonEPTCPDeviceOptions::CEmersonEPTCPDeviceOptions(void)
{
	m_Addr     = DWORD(MAKELONG(MAKEWORD( 42, 1), MAKEWORD(  168, 192)));

	m_Socket   = 502;

	m_Unit     = 1;

	m_Keep     = TRUE;

	m_Time1    = 5000;

	m_Time2    = 2500;

	m_Time3    = 200;

	m_Type	   = DVTEPP;

	m_EmID	   = DVEPP;

	m_DispMode = DMPRE;
	}

// UI Management

void CEmersonEPTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}

	if( Tag.IsEmpty() || Tag == "DispMode" || Tag == "Type") {

		BOOL fShow = FALSE;

		m_EmID     = GetID(m_Type);

		switch( m_EmID ) {

			case DVSP:
			case DVSK:
			case DVGP:
			case DVST:
			case DVMP:
				fShow      = TRUE;
				break;

			case DVMC:
			case DVEPP:
			case DVBR:
			default:
				m_DispMode = DMREG;
				break;
			}

		pWnd->UpdateUI("Type");
		pWnd->UpdateUI("DispMode");
		pWnd->EnableUI("DispMode", fShow);
		}

	}

// Download Support

BOOL CEmersonEPTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	m_EmID = GetID(m_Type);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_EmID));

	return TRUE;
	}

// Meta Data Creation

void CEmersonEPTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	m_Type = GetType(m_EmID);

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Type);
	Meta_AddInteger(DispMode);
	Meta_AddInteger(EmID);
       	}

UINT CEmersonEPTCPDeviceOptions::GetType(UINT uID)
{
	switch( uID ) {

		case DVSP:  return DVTSP;
		case DVSK:  return DVTSK;
		case DVGP:  return DVTGP;
		case DVST:  return DVTST;
		case DVMC:  return DVTMC;
		case DVBR:  return DVTBR;
		case 15:
		case DVMP:  return DVTMP;
		}

	return DVTEPP;
	}

UINT CEmersonEPTCPDeviceOptions::GetID(UINT uType)
{
	switch( uType ) {

		case DVTSP:  return DVSP;
		case DVTSK:  return DVSK;
		case DVTGP:  return DVGP;
		case DVTST:  return DVST;
		case DVTMC:  return DVMC;
		case DVTBR:  return DVBR;
		case DVTMP:  return DVMP;
		}

	return DVEPP;
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP TCP/IP Driver
//

// Instantiator

ICommsDriver *Create_EmersonEPTCPDriver(void)
{
	return New CEmersonEPTCPDriver;
	}

// Constructor

CEmersonEPTCPDriver::CEmersonEPTCPDriver(void)
{
	m_wID		= 0x3521;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "NIdec - Control Techniques";
	
	m_DriverName	= "Pre-Configured Modbus TCP";
	
	m_Version	= "2.20";
	
	m_ShortName	= "Pre-Configured Modbus TCP";

	m_fDriverType	= TRUE;
	}

// Destructor

CEmersonEPTCPDriver::~CEmersonEPTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CEmersonEPTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEmersonEPTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CEmersonEPTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonEPTCPDeviceOptions);
	}

// End of File
