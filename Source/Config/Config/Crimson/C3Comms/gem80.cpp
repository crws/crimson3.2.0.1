
#include "intern.hpp"

#include "gem80.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM 80 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CGem80SerialDriverOptions, CUIItem);

// Constructor

CGem80SerialDriverOptions::CGem80SerialDriverOptions(void)
{
	m_Drop = 0;
	}

// Download Support

BOOL CGem80SerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CGem80SerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM 80 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CGem80SerialDeviceOptions, CUIItem);
				   
// Constructor

CGem80SerialDeviceOptions::CGem80SerialDeviceOptions(void)
{
	m_Drop = 0;
	}

// UI Managament

void CGem80SerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CGem80SerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CGem80SerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM 80 Exchange Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CGem80SerialExchangeDeviceOptions, CUIItem);
				   
// Constructor

CGem80SerialExchangeDeviceOptions::CGem80SerialExchangeDeviceOptions(void)
{
	m_Drop		= 0; 

	m_Exchange	= 32;

	m_Ping		= 1;

	m_Timeout	= 1500;

	m_Monitor       = 0;
	}

// UI Managament

void CGem80SerialExchangeDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	
	}

// Download Support

BOOL CGem80SerialExchangeDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Exchange));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Timeout));
	Init.AddByte(BYTE(m_Monitor));

	return TRUE;
	}

// Meta Data Creation

void CGem80SerialExchangeDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Exchange);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(Monitor);
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Base Driver
//

// Constructor

CGem80Driver::CGem80Driver(void)
{
	}

// Implementation     

void CGem80Driver::AddSpaces(void)
{
	AddSpace(New CSpace('A',  "A",  "Input Words",		10, 0, 1023, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('B',  "B",  "Output Words",		10, 0, 1023, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('C',  "C",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('D',  "D",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('E',  "E",  "Timing Flags and RTC",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('F',  "F",  "Fault Flag Words",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('G',  "G",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('H',  "H",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('I',  "I",  "Serial Link Control",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('J',  "J",  "J Comms Tables",	10, 0, 1023, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('K',  "K",  "K Comms Tables",	10, 0, 1023, addrWordAsWord, addrWordAsLong));  
	AddSpace(New CSpace('L',  "L",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('M',  "M",  "Maintainer Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('N',  "N",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('O',  "O",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('Q',  "Q",  "Floating Point Tables",10, 0,32767, addrRealAsReal, addrRealAsReal));
	AddSpace(New CSpace('R',  "R",  "R Retained Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('U',  "U",  "General Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('W',  "W",  "W Retained Tables",	10, 0,32767, addrWordAsWord, addrWordAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Base Serial Driver
//

// Constructor

CGem80SerialDriver::CGem80SerialDriver(void)
{

	}

// Binding Control

UINT CGem80SerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CGem80SerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial Program Port Master Driver
//

// Instantiator

ICommsDriver *	Create_Gem80SerialMasterDriver(void)
{
	return New CGem80SerialMasterDriver;
	}

// Constructor

CGem80SerialMasterDriver::CGem80SerialMasterDriver(void)
{
	m_wID		= 0x3318;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Alstom";
			
	m_DriverName	= "GEM80 ESP Program Port Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "GEM80 ESP Master";	

	AddSpaces();
	}

// Configuration

CLASS CGem80SerialMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CGem80SerialDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial J/K Tables Master Driver
//

// Instantiator

ICommsDriver *	Create_Gem80SerialJKMasterDriver(void)
{
	return New CGem80SerialJKMasterDriver;
	}

// Constructor

CGem80SerialJKMasterDriver::CGem80SerialJKMasterDriver(void)
{
	m_wID		= 0x3312;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Alstom";
			
	m_DriverName	= "GEM80 ESP J/K Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "GEM80 ESP J/K Master";	

	AddSpaces();
	}

// Configuration

CLASS CGem80SerialJKMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CGem80SerialExchangeDeviceOptions);
	} 

void CGem80SerialJKMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace('J',  "J",  "J Tables (Write)", 10, 0, 127, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('K',  "K",  "K Tables (Read)",  10, 0, 127, addrWordAsWord, addrWordAsLong));  
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial Slave Driver
//

// Instantiator

ICommsDriver *	Create_Gem80SerialSlaveDriver(void)
{
	return New CGem80SerialSlaveDriver;
	}

// Constructor

CGem80SerialSlaveDriver::CGem80SerialSlaveDriver(void)
{
	m_wID		= 0x3403;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Alstom";
			
	m_DriverName	= "GEM80 ESP J/K Slave";
	
	m_Version	= "1.01";
	
	m_ShortName	= "GEM80 ESP J/K Slave";	

	AddSpaces();
	}

// Configuration

CLASS CGem80SerialSlaveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CGem80SerialDriverOptions);
	}

CLASS CGem80SerialSlaveDriver::GetDeviceConfig(void)
{
	return NULL;
	}

void CGem80SerialSlaveDriver::AddSpaces(void)
{
	AddSpace(New CSpace('J',  "J",  "J Tables (Write)", 10, 0, 127, addrWordAsWord));
	AddSpace(New CSpace('K',  "K",  "K Tables (Read)",  10, 0, 127, addrWordAsWord));  
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CGem80TCPDeviceOptions, CUIItem);

// Constructor

CGem80TCPDeviceOptions::CGem80TCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Port = 1024;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CGem80TCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support	     

BOOL CGem80TCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CGem80TCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Master TCP/IP Driver
//

// Instantiator

ICommsDriver *	Create_Gem80TCPMasterDriver(void)
{
	return New CGem80TCPMasterDriver;
	}

// Constructor

CGem80TCPMasterDriver::CGem80TCPMasterDriver(void)
{
	m_wID		= 0x3510;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Alstom";
	
	m_DriverName	= "GEM80 ESP TCP/IP Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "GEM80 ESP Master";

	m_fMapDisable	= TRUE;

	AddSpaces();
	}

// Binding Control

UINT	CGem80TCPMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void	CGem80TCPMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS   CGem80TCPMasterDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CGem80TCPMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CGem80TCPDeviceOptions);
	}


//////////////////////////////////////////////////////////////////////////
//
// Alstom Alspa via SNP Master Driver
//

// Instantiator

ICommsDriver *	Create_AlstomSNPMasterDriver(void)
{
	return New CAlstomSNPMasterDriver;
	}

// Constructor

CAlstomSNPMasterDriver::CAlstomSNPMasterDriver(void)
{
	m_wID		= 0x3335;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Alstom";
			
	m_DriverName	= "ALSPA Master via SNP";
	
	m_Version	= "1.01";
	
	m_ShortName	= "ALSPA SNP Master";	
		
	}


//////////////////////////////////////////////////////////////////////////
//
// Alstom Alspa via SRTP Master TCP Driver
//

// Instantiator

ICommsDriver *	Create_AlstomSRTPMasterTCPDriver(void)
{
	return New CAlstomSRTPMasterTCPDriver;
	}

// Constructor

CAlstomSRTPMasterTCPDriver::CAlstomSRTPMasterTCPDriver(void)
{
	m_wID		= 0x3511;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Alstom";
			
	m_DriverName	= "ALSPA TCP/IP Master via SRTP";
	
	m_Version	= "1.01";
	
	m_ShortName	= "ALSPA SRTP Master";

	m_fMapDisable	= TRUE;

	}



// End of File
