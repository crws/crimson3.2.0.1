
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ENETIPS_HPP
	
#define	INCLUDE_ENETIPS_HPP

//////////////////////////////////////////////////////////////////////////
//
// EthernetIP Slave Driver Options
//

class CEthernetIPSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEthernetIPSlaveDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_RunIdle;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// EthernetIP Slave Driver
//

class CEthernetIPSlaveDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CEthernetIPSlaveDriver(void);

		// Destructor
		// ~CEthernetIPSlaveDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet IP Selection Dialog
//

class CEthernetIPSlaveDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CEthernetIPSlaveDialog(CEthernetIPSlaveDriver &Driver, CAddress &Addr);
		                
	protected:
		// Data
		CEthernetIPSlaveDriver  * m_pDriver;
		CAddress		* m_pAddr;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void SetCaption(void);
		void LoadList(void);
		void LoadAddress(void);
		void GetAddress(void);
	};

// End of File

#endif
