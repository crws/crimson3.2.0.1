
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MASTERK_HPP
	
#define	INCLUDE_MASTERK_HPP

//////////////////////////////////////////////////////////////////////////
//
// LS Master-K Serial Driver
//
//

class CLSMasterKDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CLSMasterKDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);


	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
