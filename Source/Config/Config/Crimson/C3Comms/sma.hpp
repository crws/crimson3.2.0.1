
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SMA_HPP
	
#define	INCLUDE_SMA_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Parameter Data
//

struct CParam {
	
	CString  m_Name;
	DWORD    m_Addr;
	BOOL	 m_fWrite;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Collections
//

typedef CArray <CParam> CParamArray;

/////////////////////////////////////////////////////////////////////////
//
// SMA Driver Options
//

class CSmaDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSmaDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Src;
					
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};




/////////////////////////////////////////////////////////////////////////
//
// SMA Device Options
//

class CSmaDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSmaDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void Init(void);
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Address List Support
		CString	GetDeviceName(void);
		CString GetName(CAddress Addr);
		DWORD	GetAddress(CString Name);
		BOOL    IsWritable(CAddress Addr);
		BOOL    CreateParam(CError &Error, CString const &Name, CAddress Addr, BOOL fWrite, IMakeTags *pTags = NULL);
		void    DeleteParam(CAddress const &Addr, CString const &Name);
		BOOL    RenameParam(CString const &Name, CAddress const &Addr);
		CString CreateName(void);
		void    ListParams(CParamArray &List);
		void	SetParamWrite(CString const Name, BOOL fWrite);
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	Import(FILE *pFile, BOOL fSemi, IMakeTags *pTags = NULL);
		
		// Public Data
		UINT m_Mode;
		UINT m_Addr;
		UINT m_Addr2;
		UINT m_Poll;
		UINT m_Param;
		UINT m_MemStore;
		UINT m_MemStore2;

	protected:

		CParamArray m_List;
		UINT	    m_Index;
	
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT    CreateIndex(void);
		void	SaveParams(CTreeFile &Tree);
		void    LoadParams(CTreeFile &Tree);
		void	OnManage(CWnd *pWnd);
		void	OnImport(CWnd *pWnd);
		void	OnExport(CWnd *pWnd);
		void	Export(FILE *pFile);
	};



//////////////////////////////////////////////////////////////////////////
//
// Sma Driver
//

class CSmaDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSmaDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Driver Data
		UINT GetFlags(void);

		// Tag Import
		BOOL MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		
	protected:

		// Implementation
		void AddSpaces(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// SMa TCP/IP Master Device Options
//

class CSmaTCPDeviceOptions : public CSmaDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSmaTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Group;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SMA TCP/IP Master Driver
//

class CSmaTCPDriver : public CSmaDriver
{
	public:
		// Constructor
		CSmaTCPDriver(void);

		//Destructor
		~CSmaTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SMA Address Selection Dialog
//

class CSmaAddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSmaAddrDialog(CSmaDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart, BOOL fSelect);

	protected:

		// Data
		CSmaDriver *	m_pDriver;
		CAddress *	m_pAddr;
		CItem *		m_pConfig;
		BOOL		m_fPart;
		BOOL		m_fSelect;
		CString         m_Caption;
		HTREEITEM       m_hRoot;
		HTREEITEM       m_hSelect;
		DWORD		m_dwSelect;
		CImageList      m_Images;
								
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Wnd, DWORD dwData);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);

		// Command Handlers
		BOOL OnCreate(UINT uID);
		BOOL OnWrite(UINT uID);
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadParameters(void);
		void LoadParameters(CTreeView &Tree);
		void LoadRoot(CTreeView &Tree);
		void LoadParameter(CTreeView &Tree, CString Name, CAddress Addr, BOOL fWrite);
		BOOL SetSelection(CString Text);
		BOOL SetSelection(CAddress Addr);
		BOOL DeleteParameter(void);
		BOOL RenameParameter(void);
	};


// End of File

#endif
