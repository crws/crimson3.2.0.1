
#include "intern.hpp"

#include "emfxser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emerson FX Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonFXSerialDeviceOptions, CUIItem);

// Constructor

CEmersonFXSerialDeviceOptions::CEmersonFXSerialDeviceOptions(void)
{
	m_Drop     = "1";
	}

// UI Management

void CEmersonFXSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CEmersonFXSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop[0]));

	return TRUE;
	}

// Meta Data Creation

void CEmersonFXSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson FX Serial Driver
//

// Instantiator

ICommsDriver *	Create_EmersonFXSerialDriver(void)
{
	return New CEmersonFXSerialDriver;
	}

// Constructor

CEmersonFXSerialDriver::CEmersonFXSerialDriver(void)
{
	m_wID		= 0x4067;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Nidec - Control Techniques";
	
	m_DriverName	= "FX Serial Driver";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Nidec FX Serial Driver";

	m_DevRoot	= "FXD";

	AddSpaces();
	}

// Binding Control

UINT	CEmersonFXSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CEmersonFXSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEmersonFXSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonFXSerialDeviceOptions);
	}

// Address Management

BOOL CEmersonFXSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CEmersonFXSerialDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CEmersonFXSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		Text.MakeUpper();

		UINT p0 = Text[0];

		UINT uT = pSpace->m_uTable;

		Addr.a.m_Table   = uT;
		Addr.a.m_Type    = pSpace->m_uType;
		Addr.a.m_Extra   = 0;

		if( uT == ERR ) {

			Addr.a.m_Offset = ERRCFGO;

			return TRUE;
			}

		if( !isalpha(p0) ) {

			p0 = 'A';
			}

		UINT uO = 0;

		if( !IsStringItem(uT) ) {

			uO  = (p0 - 'A') << 11;

			uO |= GetItemType(uT, uO);
			}

		UINT uLen = Text.GetLength();

		if( uLen > 1 ) {

			if( !HasDescription(uT, uO) ) {

				switch( Text[uLen - 1] ) {

					case 'B':
						uO |= DATABIN;
						break;

					case 'H':
						uO |= DATAHEX;
						break;
					}
				}

			if( isdigit(Text[1]) ) {

				uO += tatoi(Text.Mid(1)) & MAXPAR;

				uO |= PARBIT;
				}
			}

		Addr.a.m_Offset = uO;

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonFXSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	UINT uT = Addr.a.m_Table;
	UINT uO = Addr.a.m_Offset;

	if( IsStringItem(uT) ) {

		Text.Printf( "%s", pSpace->m_Prefix );

		return TRUE;
		}

	char c1 = (char)uT;
	char c2 = char(GetLetter2(uO));
	char c3 = 0;

	if( !HasDescription(uT, uO) ) {

		UINT uType = uO & DATATXT;

		switch( uType ) {

			case DATAHEX:	c3 = 'H';	break;
			case DATABIN:	c3 = 'B';	break;
			}
		}

	if( HasPar(uT, uO) ) {

		Text.Printf( "%c%c%d%c",

			c1,
			c2,
			Addr.a.m_Offset & MAXPAR,
			c3
			);

		return TRUE;
		}

	Text.Printf( "%c%c%c",

		c1,
		c2,
		c3
		);

	return TRUE;
	}

// Implementation	

void CEmersonFXSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(HAA, "A",	"A Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAB, "B",	"B Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAC, "C",	"C Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAD, "D",	"D Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAE, "E",	"E Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAF, "F",	"F Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAG, "G",	"G Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAH, "H",	"H Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAI, "I",	"I Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAJ, "J",	"J Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAK, "K",	"K Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAL, "L",	"L Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAM, "M",	"M Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAN, "N",	"N Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAO, "O",	"O Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAP, "P",	"P Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAQ, "Q",	"Q Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAR, "R",	"R Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAS, "S",	"S Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAT, "T",	"T Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAU, "U",	"U Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAV, "V",	"V Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAW, "W",	"W Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAX, "X",	"X Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAY, "Y",	"Y Commands",	10,	0,	MAXVAL,	LL));
	AddSpace(New CSpace(HAZ, "Z",	"Z Commands",	10,	0,	MAXVAL,	LL));
	// string response holders
	AddSpace(New CSpace(AIR, "AIR",	"AI Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(CBR, "CBR",	"CB Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(DUR, "DUR",	"DU Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(EQR, "EQR",	"EQ Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(IDR, "IDR",	"ID Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(PIR, "PIR",	"PI Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(QCR, "QCR",	"QC Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(TMR, "TMR",	"TM Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(TWR, "TWR",	"TW Response",	10,	0,	19,	LL));
	AddSpace(New CSpace(VUR, "VUR",	"VU Response",	10,	0,	19,	LL));
	// Error Response
	AddSpace(New CSpace(ERR, "@E",	"Reply Error",	10,	0,	19,	LL));
	}

// Helpers

BOOL CEmersonFXSerialDriver::HasPar(UINT uTable, UINT uOffset)
{
	return uOffset & PARBIT;
	}

BOOL CEmersonFXSerialDriver::IsStringItem(UINT uTable)
{
	return uTable < 'A';
	}

UINT CEmersonFXSerialDriver::GetLetter2(UINT uOffset)
{
	return 'A' + ((uOffset >> 11) % 26);
	}


UINT CEmersonFXSerialDriver::GetItemType(UINT uT, UINT uO)
{
	UINT uItem = GetLetter2(uO);

	UINT uForm = DATADEC;

	switch( uT ) {

		case 'A':
			switch( uItem ) {

				case 'G':
				case 'S':
					uForm  = DATAHEX;
					break;
				case 'I':
					uForm  = DATATXT;
					break;
				}

			break;

		case 'B':
			switch( uItem ) {

				case 'D':
				case 'K':
				case 'R':
					uForm  = DATABIN;
					break;
				}

			break;

		case 'C':
			switch( uItem ) {

				case 'B':
					uForm  = DATATXT;
					break;
				case 'E':
				case 'M':
					uForm  = DATABIN;
					break;
				case 'F':
				case 'G':
				case 'H':
				case 'K':
				case 'N':
				case 'Q':
				case 'S':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'D':
			switch( uItem ) {

				case 'G':
					uForm  = DATAHEX;
					break;
				case 'U':
					uForm  = DATATXT;
					break;
				}

			break;

		case 'E':
			switch( uItem ) {

				case 'G':
				case 'T':
					uForm  = DATAHEX;
					break;
				case 'Q':
					uForm  = DATATXT;
					break;
				}

			break;

		case 'F':
			switch( uItem ) {

				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'S':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'G':
			switch( uItem ) {

				case 'F':
				case 'S':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'H':
			switch( uItem ) {

				case 'F':
					uForm  = DATABIN;
					break;
				case 'G':
				case 'W':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'I':
			switch( uItem ) {

				case 'D':
					uForm  = DATATXT;
					break;
				case 'G':
				case 'O':
					uForm  = DATAHEX;
					break;
				case 'P':
					uForm  = DATABIN;
					break;
				}

			break;

		case 'J':
			switch( uItem ) {

				case 'T':
					uForm  = DATABIN;
					break;
				}

			break;

		case 'K':
			switch( uItem ) {

				case 'I':
					uForm  = DATABIN;
					break;
				}

			break;

		case 'L':
			switch( uItem ) {

				case 'P':
				case 'R':
				case 'T':
				case 'V':
					uForm  = DATABIN;
					break;
				case 'X':
				case 'Y':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'M':
			switch( uItem ) {

				case 'F':
				case 'M':
				case 'P':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'N':
			switch( uItem ) {

				case 'T':
				case 'V':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'O':
			switch( uItem ) {

				case 'M':
				case 'P':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'P':
			switch( uItem ) {

				case 'F':
				case 'G':
					uForm  = DATAHEX;
					break;
				case 'I':
					uForm  = DATATXT;
					break;
				}

			break;

		case 'Q':
			switch( uItem ) {

				case 'C':
					uForm  = DATATXT;
					break;
				case 'U':
					uForm  = DATAHEX;
					break;
				}

			break;

		case 'S':
			switch( uItem ) {

				case 'G':
					uForm  = DATAHEX;
					break;
				case 'P':
					uForm  = DATABIN;
					break;
				}

			break;

		case 'T':
			switch( uItem ) {

				case 'M':
				case 'W':
					uForm  = DATATXT;
					break;
				}

			break;

		case 'V':
			switch( uItem ) {

				case 'G':
				case 'P':
					uForm  = DATAHEX;
					break;
				case 'U':
					uForm  = DATATXT;
					break;
				}

			break;

		case 'W':
			break;

		case 'X':
			switch( uItem ) {

				case 'C':
				case 'P':
				case 'V':
					uForm  = DATAHEX;
					break;
				}

			break;

		case ERR:
			uForm = DATATXT;
			break;
		}

	return uForm;
	}

BOOL CEmersonFXSerialDriver::HasDescription(UINT uTable, UINT uOffset)
{
	if( IsStringItem(uTable) ) return TRUE;

	UINT uItem = GetLetter2(uOffset);

	switch( uTable ) {

		case 'A':
			switch( uItem ) {

				case 'G':
				case 'I':
				case 'L':
				case 'S':
				case 'T':
				case 'V':
					return TRUE;
				}
			break;

		case 'B':
			switch( uItem ) {

				case 'C':
				case 'D':
				case 'K':
				case 'R':
					return TRUE;
				}
			break;

		case 'C':
			switch( uItem ) {

				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'X':
				case 'Y':
				case 'Z':
					return TRUE;
				}
			break;

		case 'D':
			switch( uItem ) {

				case 'A':
				case 'B':
				case 'D':
				case 'G':
				case 'L':
				case 'O':
				case 'P':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'Y':
					return TRUE;
				}
			break;

		case 'E':
			switch( uItem ) {

				case 'E':
				case 'G':
				case 'M':
				case 'P':
				case 'Q':
				case 'T':
				case 'V':
				case 'W':
					return TRUE;
				}
			break;

		case 'F':
			switch( uItem ) {

				case 'A':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'S':
				case 'T':
				case 'V':
				case 'W':
				case 'Y':
				case 'Z':
					return TRUE;
				}
			break;

		case 'G':
			switch( uItem ) {

				case 'F':
				case 'S':
					return TRUE;
				}
			break;

		case 'H':
			switch( uItem ) {

				case 'A':
				case 'D':
				case 'F':
				case 'G':
				case 'I':
				case 'L':
				case 'M':
				case 'O':
				case 'P':
				case 'R':
				case 'T':
				case 'W':
					return TRUE;
				}
			break;

		case 'I':
			switch( uItem ) {

				case 'B':
				case 'C':
				case 'D':
				case 'F':
				case 'G':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'T':
					return TRUE;
				}
			break;

		case 'J':
			switch( uItem ) {

				case 'A':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'S':
				case 'T':
					return TRUE;
				}
			break;

		case 'K':
			switch( uItem ) {

				case 'A':
				case 'B':
				case 'C':
				case 'E':
				case 'F':
				case 'I':
				case 'M':
				case 'N':
				case 'O':
				case 'R':
				case 'S':
				case 'T':
				case 'X':
				case 'Y':
				case 'Z':
					return TRUE;
				}
			break;

		case 'L':
			switch( uItem ) {

				case 'D':
				case 'L':
				case 'P':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
					return TRUE;
				}
			break;

		case 'M':
			switch( uItem ) {

				case 'A':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'L':
				case 'M':
				case 'P':
				case 'S':
				case 'T':
				case 'V':
				case 'Z':
					return TRUE;
				}
			break;

		case 'N':
			switch( uItem ) {

				case 'D':
				case 'I':
				case 'P':
				case 'T':
				case 'V':
					return TRUE;
				}
			break;

		case 'O':
			switch( uItem ) {

				case 'B':
				case 'D':
				case 'F':
				case 'M':
				case 'P':
				case 'T':
					return TRUE;
				}
			break;

		case 'P':
			switch( uItem ) {

				case 'A':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'L':
				case 'N':
				case 'O':
				case 'P':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'Y':
					return TRUE;
				}
			break;

		case 'Q':
			switch( uItem ) {

				case 'C':
				case 'U':
					return TRUE;
				}
			break;

		case 'R':
			switch( uItem ) {

				case 'A':
				case 'D':
				case 'L':
				case 'O':
				case 'P':
				case 'R':
				case 'S':
				case 'T':
				case 'W':
					return TRUE;
				}
			break;

		case 'S':
			switch( uItem ) {

				case 'C':
				case 'D':
				case 'F':
				case 'G':
				case 'J':
				case 'L':
				case 'M':
				case 'P':
				case 'R':
				case 'S':
				case 'T':
				case 'V':
					return TRUE;
				}
			break;

		case 'T':
			switch( uItem ) {

				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'L':
				case 'M':
				case 'N':
				case 'Q':
				case 'R':
				case 'T':
				case 'V':
				case 'W':
					return TRUE;
				}
			break;

		case 'U':
			switch( uItem ) {

				case 'D':
				case 'N':
				case 'R':
					return TRUE;
				}
			break;

		case 'V':
			switch( uItem ) {

				case 'C':
				case 'E':
				case 'G':
				case 'L':
				case 'P':
				case 'U':
					return TRUE;
				}
			break;

		case 'W':
			switch( uItem ) {

				case 'L':
				case 'P':
				case 'R':
				case 'T':
				case 'V':
					return TRUE;
				}
			break;

		case 'X':
			switch( uItem ) {

				case 'C':
				case 'P':
				case 'T':
				case 'V':
					return TRUE;
				}
			break;

		case 'Y':
			break;

		case 'Z':
			switch( uItem ) {

				case 'F':
				case 'I':
				case 'M':
				case 'R':
				case 'T':
					return TRUE;
				}
			break;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson FX Serial Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CEmersonFXSerialDialog, CStdAddrDialog);
		
// Constructor

CEmersonFXSerialDialog::CEmersonFXSerialDialog(CEmersonFXSerialDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fPart		= fPart;

	m_pDriverData	= &Driver;

	m_pCfg		= pConfig;

	m_fUpdateInfo	= FALSE;

	SetName(TEXT("EmersonFXSerAddressDlg"));
	}

// Message Map

AfxMessageMap(CEmersonFXSerialDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxDispatchCommand(2007, OnButtonClicked)

	AfxMessageEnd(CEmersonFXSerialDialog)
	};

// Message Handlers

BOOL CEmersonFXSerialDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	GetDlgItem(2001).EnableWindow(TRUE);
	GetDlgItem(2002).EnableWindow(TRUE);

	InitFXInfo();

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	LoadList();
	
	if( !m_fPart ) {

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			if( !Addr.m_Ref ) m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			}

		SetDlgFocus(2002);

		return !(BOOL(m_pSpace));
		}
	else {
		if( m_pSpace ) {

			CString Text = m_pSpace->m_Prefix;

			if( IsNotERR(0, Text[0]) ) {

				Text += GetAddressText();

				if( Text.GetLength() == 1 ) {

					Text += 'A';
					}
				}

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);
				}

			ShowAddress(Addr);
			}

		return FALSE;
		}
	}

// Notification Handlers

void CEmersonFXSerialDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CEmersonFXSerialDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		CAddress Addr;

		if( m_pSpace != pSpace ) {

			Addr.a.m_Table  = m_pSpace->m_uTable;

			Addr.a.m_Offset = 0;

			Addr.a.m_Type   = addrLongAsLong;

			Addr.a.m_Extra  = 0;

			ShowAddress(Addr);
			}

		return;
		}

	GetDlgItem(2001).SetWindowText("");
	GetDlgItem(2002).SetWindowText("");
	GetDlgItem(2002).EnableWindow(FALSE);

	InitFXInfo();

	m_pSpace = NULL;
	}

BOOL CEmersonFXSerialDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		if( IsNotERR(0, Text[0]) ) {

			CString sPar = GetDlgItem(2002).GetWindowText();

			Text        += sPar;
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			if( !m_fUpdateInfo ) {

				EndDialog(TRUE);
				}

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CEmersonFXSerialDialog::OnButtonClicked(UINT uID)
{
	m_fUpdateInfo = TRUE;

	OnOkay(uID);

	m_fUpdateInfo = FALSE;

	CAddress Addr = *m_pAddr;

	ShowAddress(Addr);

	SetDlgFocus(2002);

	return TRUE;
	}

// Overrideables

void CEmersonFXSerialDialog::ShowAddress(CAddress Addr)
{
	CString sP = "A";
	CString sO = "A";

	CString sShow;

	if( m_pDriverData->ExpandAddress(sShow, m_pCfg, Addr) ) {

		sP = sShow.Left(1);
		sO = sShow.Right(sShow.GetLength() - 1);
		}

	GetDlgItem(2001).SetWindowText(sP);
	GetDlgItem(2002).SetWindowText(sO);
	GetDlgItem(2002).EnableWindow(IsNotERR(Addr.a.m_Table, 0));

	GetInfo(Addr.a.m_Table, Addr.a.m_Offset);
	}

// Information

void CEmersonFXSerialDialog::GetInfo(UINT uT, UINT uO)
{
	CString sMore = "<More...>";

	InitFXInfo();

	GetDlgItem(2007).EnableWindow(FALSE);

	UINT i        = 0;
	UINT uEnd     = 2021;

	UINT uDlgItem = 2010;

	CString Text;

	if( m_pDriverData->IsStringItem(uT) ) {

		Text = GetDlgItem(2001).GetWindowText() + GetDlgItem(2002).GetWindowText();

		Text = Text.Left(3);

		if( IsNotERR(uT, 0) ) {

			Text = "String Response - Use Command " + Text.Left(2) + " to request data";
			}

		else {
			Text += " - Received Invalid Response or None";
			}

		GetDlgItem(2010).SetWindowText(Text);
		}

	else {
		UINT uItem = m_pDriverData->GetLetter2(uO);

		switch( uT ) {

			case 'A':
				GetDlgItem(uDlgItem++).SetWindowText("AG - AUTO GAIN");
				GetDlgItem(uDlgItem++).SetWindowText("AI - AXIS NAME (Resp -> AIR)");
				GetDlgItem(uDlgItem++).SetWindowText("AL - ACCEL TIME ENTRY USER LIMIT");
				GetDlgItem(uDlgItem++).SetWindowText("AS - AUTOMATIC SERIAL REPORTING FLAGS");
				GetDlgItem(uDlgItem++).SetWindowText("ATn - ACCEL RAMP");
				GetDlgItem(uDlgItem++).SetWindowText("AV - ANALOG COMMAND CHANNEL VELOCITY");
				return;

			case 'B':
				GetDlgItem(uDlgItem++).SetWindowText("BCn - PRESS FEED");
				GetDlgItem(uDlgItem++).SetWindowText("BD - BCD SELECT LINES");
				GetDlgItem(uDlgItem++).SetWindowText("BK - BRAKE OVERRIDE");
				GetDlgItem(uDlgItem++).SetWindowText("BR - BRIDGE INHIBIT");
				return;

			case 'C':
				GetDlgItem(2007).EnableWindow(TRUE);
				i = 0;

				if( uItem < 'N' ) {

					GetDlgItem(2021).SetWindowText(sMore);
					uEnd = 2020;
					}

				while( uDlgItem <= uEnd ) {

					char c = (char)(uItem + i);

					Text = GetCString(c);

					GetDlgItem(uDlgItem++).SetWindowText(Text);

					i += UINT(Text[1] - c + 1);
					}
				return;

			case 'D':
				GetDlgItem(2007).EnableWindow(TRUE);
				i = 0;

				if( uItem == 'A' ) {

					GetDlgItem(2021).SetWindowText(sMore);
					uEnd = 2020;
					}

				while( uDlgItem <= uEnd ) {

					char c = (char)(uItem + i);

					Text = GetDString(c);

					GetDlgItem(uDlgItem++).SetWindowText(Text);

					i += UINT(Text[1] - c + 1);
					}
				return;

			case 'E':
				GetDlgItem(uDlgItem++).SetWindowText("EEn - EXTERNAL ENCODER COUNTING MODE (1,2)");
				GetDlgItem(uDlgItem++).SetWindowText("EGn - ERROR GAIN => CURRENT / MIN / MAX / DEF");
				GetDlgItem(uDlgItem++).SetWindowText("EMn - OPERATION MODES");
				GetDlgItem(uDlgItem++).SetWindowText("EPn - ENCODER POSITION DATA");
				GetDlgItem(uDlgItem++).SetWindowText("EQn - MATH EQUATIONS (Resp -> EQR)");
				GetDlgItem(uDlgItem++).SetWindowText("ET - ENABLE PARALLEL INTERFACE TYPE");
				GetDlgItem(uDlgItem++).SetWindowText("EVn - END OF INDEX VELOCITY INDEX NUMBER");
				GetDlgItem(uDlgItem++).SetWindowText("EW - EARLY WARNING COUNT");
				return;

			case 'F':
				GetDlgItem(2007).EnableWindow(TRUE);
				i = 0;

				if( uItem < 'K' ) {

					GetDlgItem(2021).SetWindowText(sMore);
					uEnd = 2020;
					}

				while( uDlgItem <= uEnd ) {

					char c = (char)(uItem + i);

					Text = GetFString(c);

					GetDlgItem(uDlgItem++).SetWindowText(Text);

					i += UINT(Text[1] - c + 1);
					}
				return;

			case 'G':
				GetDlgItem(uDlgItem++).SetWindowText("GF - PRESS FEED FLAGS (= Command G");
				GetDlgItem(uDlgItem++).SetWindowText("GSn - GAIN SCALING FACTOR => TACH / ERROR");
				return;

			case 'H':
				GetDlgItem(uDlgItem++).SetWindowText("HAn - HOME ACCEL TIME");
				GetDlgItem(uDlgItem++).SetWindowText("HDn - HOME DECEL TIME");
				GetDlgItem(uDlgItem++).SetWindowText("HF - SERIAL HOLD");
				GetDlgItem(uDlgItem++).SetWindowText("HGn - HOME FLAGS");
				GetDlgItem(uDlgItem++).SetWindowText("HIn - HOME FEED ON VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("HLn - HOME DISTANCE LIMIT");
				GetDlgItem(uDlgItem++).SetWindowText("HMn - HOME");
				GetDlgItem(uDlgItem++).SetWindowText("HOn - HOME FEED OFF VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("HPn - HOME REFERENCE POSITION");
				GetDlgItem(uDlgItem++).SetWindowText("HRn - HOME RESOLVER (OFFSET) VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("HTn - HOME TYPE");
				GetDlgItem(uDlgItem++).SetWindowText("HWn - DRIVE CONFIGURATION PARAMETERS");
				return;

			case 'I':
				GetDlgItem(uDlgItem++).SetWindowText("IBn - IBS INPUT WORD SETUP");
				GetDlgItem(uDlgItem++).SetWindowText("ICn - INDEX FEED TORQUE LIMIT");
				GetDlgItem(uDlgItem++).SetWindowText("IDn - VERSION & REVISION DATA (Resp -> IDR)");
				GetDlgItem(uDlgItem++).SetWindowText("IFn - INPUT FUNCTION / INPUT LINE ASSIGNMENT");
				GetDlgItem(uDlgItem++).SetWindowText("IGn - INTEGRAL ERROR GAIN DATA");
				GetDlgItem(uDlgItem++).SetWindowText("IMn - INITIALIZATION COMMANDS");
				GetDlgItem(uDlgItem++).SetWindowText("INn - INDEX => EXECUTE / RETURN TO POSITION");
				GetDlgItem(uDlgItem++).SetWindowText("IOn - MONITORING SELECTIONS and I/O DATA");
				GetDlgItem(uDlgItem++).SetWindowText("IPn - INPUT FUNCTION POLARITY");
				GetDlgItem(uDlgItem++).SetWindowText("ITn - INDEX TYPE");
				return;

			case 'J':
				GetDlgItem(uDlgItem++).SetWindowText("JA - JOG ACCEL TIME");
				GetDlgItem(uDlgItem++).SetWindowText("JD - JOG DECEL TIME");
				GetDlgItem(uDlgItem++).SetWindowText("JEn - INTERNAL TIME BASE OVERIDE / JOG TIME BASE");
				GetDlgItem(uDlgItem++).SetWindowText("JF - JOG FAST VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("JG - SERIAL JOG VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("JS - JOG SLOW VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("JT - JOG SERIAL TORQUE FLAG");
				return;

			case 'K':
				GetDlgItem(2007).EnableWindow(TRUE);
				i = 0;

				if( uItem <= 'C' ) {

					GetDlgItem(2021).SetWindowText(sMore);
					uEnd = 2020;
					}

				while( uDlgItem <= uEnd ) {

					char c = (char)(uItem + i);

					Text = GetKString(c);

					GetDlgItem(uDlgItem++).SetWindowText(Text);

					i += UINT(Text[1] - c + 1);
					}
				return;

			case 'L':
				GetDlgItem(uDlgItem++).SetWindowText("LDn - FEED LIMIT DISTANCE");
				GetDlgItem(uDlgItem++).SetWindowText("LLn - PRESS FEED => ANALOG LOOP CONTROL");
				GetDlgItem(uDlgItem++).SetWindowText("LP - POSITION LOOP CONTROL FLAG");
				GetDlgItem(uDlgItem++).SetWindowText("LR - LINEAR RAMPS ENABLE");
				GetDlgItem(uDlgItem++).SetWindowText("LSn - PLS POSITIONS");
				GetDlgItem(uDlgItem++).SetWindowText("LT - TORQUE LINEARIZATION");
				GetDlgItem(uDlgItem++).SetWindowText("LUn - PRESS FEED PARAMETERS");
				GetDlgItem(uDlgItem++).SetWindowText("LV - VELOCITY LOOP ENABLE");
				GetDlgItem(uDlgItem++).SetWindowText("LWn - PLS PULSE WIDTH");
				GetDlgItem(uDlgItem++).SetWindowText("LXn - PLS PGO MASK");
				GetDlgItem(uDlgItem++).SetWindowText("LYn - PLS PGO PATTERN");
				return;

			case 'M':
				GetDlgItem(uDlgItem++).SetWindowText("MA - MINIMUM ACCEL TIME");
				GetDlgItem(uDlgItem++).SetWindowText("MC - MASTER SENSOR COUNT");
				GetDlgItem(uDlgItem++).SetWindowText("MDn - MASTER CYCLE PGO DEGREE POSITION");
				GetDlgItem(uDlgItem++).SetWindowText("MEn - MAXIMUM ERRORS");
				GetDlgItem(uDlgItem++).SetWindowText("MF - MASTER CYCLE FLAGS");
				GetDlgItem(uDlgItem++).SetWindowText("MLn - MASTER PRODUCT POSITIONS / LENGTHS / LIMITS");
				GetDlgItem(uDlgItem++).SetWindowText("MMn - MASTER CYCLE PGO OUTPUT MASK");
				GetDlgItem(uDlgItem++).SetWindowText("MPn - MASTER CYCLE PGO PATTERN");
				GetDlgItem(uDlgItem++).SetWindowText("MS - MASTER CYCLE AVERAGING COUNT");
				GetDlgItem(uDlgItem++).SetWindowText("MTn - MASTER CYCLE PGO ON TIME");
				GetDlgItem(uDlgItem++).SetWindowText("MVn - DRIVE VELOCITY => MAX / MAX CAL. DEF CAL.");
				GetDlgItem(uDlgItem++).SetWindowText("MZ - MASTER SENSOR VALID ZONE");
				return;

			case 'N':
				GetDlgItem(uDlgItem++).SetWindowText("NDn - T-16 DISPLAY PARAMETERS");
				GetDlgItem(uDlgItem++).SetWindowText("NI - NUMBER OF INDEXES");
				GetDlgItem(uDlgItem++).SetWindowText("NPn - NUMBER OF PROGRAMS / STEPS");
				GetDlgItem(uDlgItem++).SetWindowText("NTn - ZERO TORQUE VALUE => CURR / MIN / MAX / DEF");
				GetDlgItem(uDlgItem++).SetWindowText("NVn - ZERO VELOCITY VALUE => CURR / MIN / MAX / DEF");
				return;

			case 'O':
				GetDlgItem(uDlgItem++).SetWindowText("OBn - INTERBUS OUTPUT PROCESS DATA SELECT");
				GetDlgItem(uDlgItem++).SetWindowText("ODn - INDEX PGO DELAY TIME (+/-MS) FROM END OF INDEX");
				GetDlgItem(uDlgItem++).SetWindowText("OFn - OUTPUT FUNCTION LINE NUMBERS ASSIGNMENT");
				GetDlgItem(uDlgItem++).SetWindowText("OMn - INDEX PGO MASK");
				GetDlgItem(uDlgItem++).SetWindowText("OPn - INDEX PGO PATTERN");
				GetDlgItem(uDlgItem++).SetWindowText("OTn - INDEX PGO ON TIME");
				return;

			case 'P':
				GetDlgItem(2007).EnableWindow(TRUE);
				i = 0;

				if( uItem < 'G' ) {

					GetDlgItem(2021).SetWindowText(sMore);
					uEnd = 2020;
					}

				while( uDlgItem <= uEnd ) {

					char c = (char)(uItem + i);

					Text = GetPString(c);

					GetDlgItem(uDlgItem++).SetWindowText(Text);

					i += UINT(Text[1] - c + 1);
					}
				return;

			case 'Q':
				GetDlgItem(uDlgItem++).SetWindowText("QCn - USER REGISTER NAMES (Resp -> QCR)");
				GetDlgItem(uDlgItem++).SetWindowText("QU - GENERAL STATUS QUERY");
				return;

			case 'R':
				GetDlgItem(uDlgItem++).SetWindowText("RAn - RESOLVER / ENCODER / WEB FAULT LIM.");
				GetDlgItem(uDlgItem++).SetWindowText("RDn - ROLL DIAMETER DATA");
				GetDlgItem(uDlgItem++).SetWindowText("RLn - 525 PMC => EXT DRIVE => ERR GAIN RNG / GAIN OFF.");
				GetDlgItem(uDlgItem++).SetWindowText("ROn - HOME RESOLVER OFFSET");
				GetDlgItem(uDlgItem++).SetWindowText("RP - ROTARY ABSOLUTE POSITION");
				GetDlgItem(uDlgItem++).SetWindowText("RR - INITIALIZE NEW ROLL RATIO");
				GetDlgItem(uDlgItem++).SetWindowText("RS - RESET DRIVE");
				GetDlgItem(uDlgItem++).SetWindowText("RT - REAL-TIME CLOCK");
				GetDlgItem(uDlgItem++).SetWindowText("RWn - WEB RATIO DATA");
				return;

			case 'S':
				GetDlgItem(uDlgItem++).SetWindowText("SC - FOLLOWER CYC MIN ACCEL TIME / SERIAL CONT.");
				GetDlgItem(uDlgItem++).SetWindowText("SD - STOP DECEL TIME");
				GetDlgItem(uDlgItem++).SetWindowText("SF - FOLLOWER SENSOR MISSED ANGLE");
				GetDlgItem(uDlgItem++).SetWindowText("SG - SUSPEND FLAGS");
				GetDlgItem(uDlgItem++).SetWindowText("SJ - STOP JOG");
				GetDlgItem(uDlgItem++).SetWindowText("SLn - LINES => INDEX / PROGRAM / OUTPUT / INPUT");
				GetDlgItem(uDlgItem++).SetWindowText("SM - MASTER CYCLE SENSOR ABSENCE ANGLE");
				GetDlgItem(uDlgItem++).SetWindowText("SP - SUSPEND / RESUME PROGRAM FLAG");
				GetDlgItem(uDlgItem++).SetWindowText("SRn - ENCODER PARAMETERS");
				GetDlgItem(uDlgItem++).SetWindowText("SS - MASTER MAXIMUM VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("ST - STOP ALL MOTION");
				GetDlgItem(uDlgItem++).SetWindowText("SVn - MAXIMUM SYNC VELOCITY / WEB RPM LIMIT");
				return;

			case 'T':
				GetDlgItem(2007).EnableWindow(TRUE);
				i = 0;

				if( uItem == 'A' ) {

					GetDlgItem(2021).SetWindowText(sMore);
					uEnd = 2020;
					}

				while( uDlgItem <= uEnd ) {

					char c = (char)(uItem + i);

					Text = GetTString(c);

					GetDlgItem(uDlgItem++).SetWindowText(Text);

					i += UINT(Text[1] - c + 1);
					}
				return;

			case 'U':
				GetDlgItem(uDlgItem++).SetWindowText("UDn - USER REGISTER DECIMAL POINT POSITION");
				GetDlgItem(uDlgItem++).SetWindowText("UNn - NON-VOLATILE NUMBER STORAGE");
				GetDlgItem(uDlgItem++).SetWindowText("URn - USER REGISTER DATA");
				return;

			case 'V':
				GetDlgItem(uDlgItem++).SetWindowText("VCn - VEL. CONVERSION NMBR => REAL TIME / SYNC / WEB");
				GetDlgItem(uDlgItem++).SetWindowText("VEn - INDEX VELOCITY");
				GetDlgItem(uDlgItem++).SetWindowText("VGn - VEL. GAIN - CURR / MIN / MAX / DEF / 525 DEF");
				GetDlgItem(uDlgItem++).SetWindowText("VLn - VELOCITY USER ENTRY LIMIT");
				GetDlgItem(uDlgItem++).SetWindowText("VPn - VEL. USER DEC. PT. => REAL TIME / SYNC / CTR WIND");
				GetDlgItem(uDlgItem++).SetWindowText("VUn - USER VELOCITY UNITS (Resp -> VUR)");
				return;

			case 'W':
				GetDlgItem(uDlgItem++).SetWindowText("WL - GLOBAL EXTERNAL INPUT TIMEOUT");
				GetDlgItem(uDlgItem++).SetWindowText("WPn - IN-POSITION WINDOW ENABLE / TIME LIMIT");
				GetDlgItem(uDlgItem++).SetWindowText("WR - IN-POSITION RANGE");
				GetDlgItem(uDlgItem++).SetWindowText("WT - IN-POSITION TIME");
				GetDlgItem(uDlgItem++).SetWindowText("WV - WEB VELOCITY");
				return;

			case 'X':
				GetDlgItem(uDlgItem++).SetWindowText("XC - 525 PMC EXTERNAL DRIVE CURRENT FEEDBACK");
				GetDlgItem(uDlgItem++).SetWindowText("XP - 525 PMC EXTERNAL DRIVE FAULT INPUT POLARITY");
				GetDlgItem(uDlgItem++).SetWindowText("XT - 525 PMC EXTERNAL DRIVE TACH CONFIGURATION");
				GetDlgItem(uDlgItem++).SetWindowText("XV - 525 PMC EXTERNAL DRIVE TACH SCALING GAIN");
				return;

			case 'Y':
				return;

			case 'Z':
				GetDlgItem(uDlgItem++).SetWindowText("ZFn - PCM-19 FEED CONTROL QUEUE");
				GetDlgItem(uDlgItem++).SetWindowText("ZIn - PCM-19 QUEUE FLAGS AND POSITIONS");
				GetDlgItem(uDlgItem++).SetWindowText("ZMn - PCM-19 MEASURING QUEUE");
				GetDlgItem(uDlgItem++).SetWindowText("ZR - ZERO RELATIVE POSITION DISPLAY");
				GetDlgItem(uDlgItem++).SetWindowText("ZTn - PCM-19 MEASURING QUEUE PRODUCT TYPE");
				return;
			}
		}
	}

CString CEmersonFXSerialDialog::GetCString(char cNext)
{
	CString Text[24];

	UINT i = 0;

	Text[i++] = "CAn - FOLLOWER CYCLE LIMITS => RATIO / POS / NEG";
	Text[i++] = "CBn - USER MESSAGES (Resp -> CBR)";
	Text[i++] = "CCn - CURRENT INDEX / PROGRAM / BATCH COUNT";
	Text[i++] = "CD - CURRENT DWELL TIME";
	Text[i++] = "CE - CONVEYOR EXECUTING";
	Text[i++] = "CFn - ANALOG CHANNEL / LOOP INP. FULL SCALE VALUE";
	Text[i++] = "CGn - CURRENT GAIN => FINAL / MIN / MAX / DEF";
	Text[i++] = "CHn - ANALOG CHANNEL READING";
	Text[i++] = "CIn - CURRENT INDEX / FOLLOWER CYCLE / MASTER CYCLE";
	Text[i++] = "CKn - CHECKSUMS";
	Text[i++] = "CL - CLEAR SUSPEND";
	Text[i++] = "CM - CALIBRATE MODE";
	Text[i++] = "CNn - ANALOG CHANNEL / LOOP INPUT NULL VALUES";
	Text[i++] = "COn - INDEX REPEAT COUNT";
	Text[i++] = "CPn - POSITION DATA";
	Text[i++] = "CQ - FLAGS => CONVEYOR / CYCLE STATUS";
	Text[i++] = "CRn - CONVEYOR DATA / TIMES / FLAGS";
	Text[i++] = "CS - CURRENT PROGRAM NUMBER / STEP";
	Text[i++] = "CTn - CALIBRATE TEST";
	Text[i++] = "CUn - CONVEYOR PARAMETERS";
	Text[i++] = "CVn - CURRENT SCALED COMMAND VELOCITY";
	Text[i++] = "CXn - PRODUCT DATA => POSITIONS / SPACING";
	Text[i++] = "CY - CYCLE NUMBER EDIT COMMAND";
	Text[i++] = "CZ - FEED CONVEYOR ACCEL / DECEL TABLE";

	for( i = 0; i < 24; i++ ) {

		if( cNext <= Text[i][1] ) return Text[i];
		}

	return "";
	}

CString CEmersonFXSerialDialog::GetDString(char cNext)
{
	CString Text[13];

	UINT i = 0;

	Text[i++] = "DA - FOLLOWER CYCLE ADJUST MIN DECEL TIME";
	Text[i++] = "DB - ANALOG DEADBAND";
	Text[i++] = "DDn - INPUT FILTERING (DEBOUNCE DELAY)";
	Text[i++] = "DGn - DIFFERENTIAL GAIN => ACTUAL / MIN / MAX / DEF";
	Text[i++] = "DL - DECEL ENTRY USER LIMIT";
	Text[i++] = "DO - FOLLOWER DROPOUT POSITION ANGLE (DEG)";
	Text[i++] = "DPn - DISTANCE DECIMAL POINT POSITION PCX / WEB";
	Text[i++] = "DSn - DISTANCE OR POSITION";
	Text[i++] = "DTn - INDEX DECEL RATE";
	Text[i++] = "DUn - DISTANCE UNITS - USER / WEB (Resp -> DUR)";
	Text[i++] = "DVn - CURRENT D/A VELOCITY";
	Text[i++] = "DWn - DWELL TIME";
	Text[i++] = "DY - USER POWER UP DELAY";

	for( i = 0; i < 13; i++ ) {

		if( cNext <= Text[i][1] ) return Text[i];
		}

	return "";
	}

CString CEmersonFXSerialDialog::GetFString(char cNext)
{
	CString Text[21];

	UINT i = 0;

	Text[i++] = "FAn - FEED ACCEL TIME";
	Text[i++] = "FC - FOLLOWER CYCLE CORRECTION SPAN";
	Text[i++] = "FDn - FEED DECEL TIME";
	Text[i++] = "FE - FOLLOWING ERROR";
	Text[i++] = "FF - CYCLE FLAGS";
	Text[i++] = "FGn - INDEX FLAGS";
	Text[i++] = "FHn - HARDWARE FAULT STATUS";
	Text[i++] = "FI - I / O FLAGS";
	Text[i++] = "FJ - JOG FLAGS";
	Text[i++] = "FKn - OUTPUT DURATION TIMES => BATCH / JOB / JOBQ";
	Text[i++] = "FLn - FOLLOWER LENGTHS / LIMITS";
	Text[i++] = "FMn - FOLLOWER / MASTER RATIO ARRAY";
	Text[i++] = "FNn - FOLLOWER CYCLES";
	Text[i++] = "FO - INFEED OVERLAP LIMIT / PHASE ANGLE ADV. INV.";
	Text[i++] = "FP - FEEDBACK POSITION";
	Text[i++] = "FS - FAULT STATUS";
	Text[i++] = "FTn - PROFILE TABLE";
	Text[i++] = "FVn - FEED VELOCITY";
	Text[i++] = "FW - PRESS FEED FAULT STATUS";
	Text[i++] = "FYn - PRESS FEED DELAY TIMES";
	Text[i++] = "FZ - SENSOR VALID ZONE";

	for( i = 0; i < 21; i++ ) {

		if( cNext <= Text[i][1] ) return Text[i];
		}

	return "";
	}

CString CEmersonFXSerialDialog::GetKString(char cNext)
{
	CString Text[15];

	UINT i = 0;

	Text[i++] = "KAn - CAM START PROFILE POSITIONS FOR MASTER";
	Text[i++] = "KBn - CAM START PROFILE POSITIONS FOR FOLLOWER";
	Text[i++] = "KCn - CYCLE CTS => CUT / BATCH / LIM / BATCH LIM / TOTAL";
	Text[i++] = "KEn - CAM SENSOR POSITION CORRECTION RATE";
	Text[i++] = "KFn - CAM PROFILE FOLLOWER POSITIONS";
	Text[i++] = "KI - KILL I / O";
	Text[i++] = "KMn - CAM PROFILE MASTER POSITIONS";
	Text[i++] = "KNn - CAM STOP PROFILE POSITIONS FOR MASTER";
	Text[i++] = "KOn - CAM STOP PROFILE POSITIONS FOR FOLLOWER";
	Text[i++] = "KRn - CAM REGISTRATION SENSOR POSITIONS";
	Text[i++] = "KSn - CAM MASTER PLS PARAMETERS";
	Text[i++] = "KT - CAM POINT START INDEX TO MASTER POSITION TABLE";
	Text[i++] = "KX - CAM POINT START INDEX VELOCITY";
	Text[i++] = "KY - CAM POINT START => INDEX ACCEL TIME";
	Text[i++] = "KZ - CAM POINT START => INDEX DECEL TIME";

	for( i = 0; i < 15; i++ ) {

		if( cNext <= Text[i][1] ) return Text[i];
		}

	return "";
	}

CString CEmersonFXSerialDialog::GetPString(char cNext)
{
	CString Text[17];

	UINT i = 0;

	Text[i++] = "PA - FOLLOWER CYCLE AVERAGING SUMMATION COUNT";
	Text[i++] = "PCn - PROGRAM COUNT";
	Text[i++] = "PDn - EDIT PROGRAM STEP DATA";
	Text[i++] = "PEn - PROGRAM SETUP OPCODE";
	Text[i++] = "PF - POWER-UP FLAGS";
	Text[i++] = "PGn - PROGRAM FLAGS";
	Text[i++] = "PHn - CORRECTIONS => PHASE DB / LIM / STEPS PER SEC";
	Text[i++] = "PIn - PROGRAM ID (Resp -> PIR)";
	Text[i++] = "PLn - DISTANCE / POSITION ENTRY PCX LIMITS => NEG / POS";
	Text[i++] = "PN - NEGATIVE PHASE ANGLE ERROR LIMIT";
	Text[i++] = "POn - EDIT PROGRAM STEP OPERAND DATA";
	Text[i++] = "PP - POSITIVE PHASE ANGLE ERROR LIMIT / INFEED ADV.";
	Text[i++] = "PRn - PROGRAM NUMBER";
	Text[i++] = "PSn - PROGRAM SELECT / PROGRAM SIZE";
	Text[i++] = "PTn - PRESS FEED => DATA REPORT";
	Text[i++] = "PU - POWER UP COUNT";
	Text[i++] = "PY - DRIVE / ENCODER DIRECTION POLARITY";

	for( i = 0; i < 17; i++ ) {

		if( cNext <= Text[i][1] ) return Text[i];
		}

	return "";
	}

CString CEmersonFXSerialDialog::GetTString(char cNext)
{
	CString Text[13];

	UINT i = 0;

	Text[i++] = "TA - FOLLOWER PHASE ANGLE / MASTER PHASE DIST.";
	Text[i++] = "TBn - TIME BASE RATIO => INTERNAL / ANALOG OVERRIDE";
	Text[i++] = "TCn - FOLLOWER PHASE ANGLE / INCREMENTS";
	Text[i++] = "TD - TRAVEL LIMIT DECEL TIME";
	Text[i++] = "TEn - PARALLEL INTERFACE DATA";
	Text[i++] = "TLn - SOFTWARE TRAVEL LIMIT => CCW / CW";
	Text[i++] = "TMn - TIME - POWER UP / LAST HDWE FAULT (Resp -> TMR)";
	Text[i++] = "TNn - LOOP CONTROL PARAMETERS";
	Text[i++] = "TQn - TORQUE MONITOR => VALUE / ENABLE";
	Text[i++] = "TRn - USER REV. => MOTOR TURNS / ENC. 1 TURNS";
	Text[i++] = "TT - PARALLEL INTERFACE FIELD NUMBER";
	Text[i++] = "TVn - VELOCITY => DRIVE-SYNC DRIVE / ENC-WEB ENC.";
	Text[i++] = "TWn - PARALLEL INTERFACE BUFFER (Resp -> TWR)";

	for( i = 0; i < 13; i++ ) {

		if( cNext <= Text[i][1] ) return Text[i];
		}

	return "";
	}

void CEmersonFXSerialDialog::InitFXInfo(void)
{
	for( UINT i = 2010; i <= 2021; i++ ) {

		GetDlgItem(i).SetWindowText("");
		GetDlgItem(i).EnableWindow(TRUE);
		}
	}

BOOL CEmersonFXSerialDialog::IsNotERR(UINT uT, TCHAR cPfx)
{
	return !(uT == ERR || cPfx == '@');
	}

// End of File
