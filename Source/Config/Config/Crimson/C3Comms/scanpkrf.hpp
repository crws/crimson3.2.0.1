
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SCANPAKRFID_HPP
	
#define	INCLUDE_SCANPAKRFID_HPP

//////////////////////////////////////////////////////////////////////////
//
// Scanpak RFID TCP/IP Driver Options
//

class CScanpakRFIDTCPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CScanpakRFIDTCPDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Count;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	}; 

//////////////////////////////////////////////////////////////////////////
//
// Scanpak RFID Device Options
//

class CScanpakRFIDTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CScanpakRFIDTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_ReaderIP;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Scanpak RFID TCP/IP Driver
//

class CScanpakRFIDTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CScanpakRFIDTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
