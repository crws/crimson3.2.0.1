
#include "intern.hpp"

#include "kebslave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CKEBSlaveDriverOptions, CUIItem);

// Constructor

CKEBSlaveDriverOptions::CKEBSlaveDriverOptions(void)
{
	m_Drop = 1;
	}

// UI Management

void CKEBSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CKEBSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CKEBSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKEBSlaveDeviceOptions, CUIItem);

// Constructor

CKEBSlaveDeviceOptions::CKEBSlaveDeviceOptions(void)
{
	m_Model = 576;
	}

// Download Support

BOOL CKEBSlaveDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Model));

	return TRUE;
	}

// Meta Data Creation

void CKEBSlaveDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	Meta_AddInteger(Model);
	}

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Driver
//

// Instantiator

ICommsDriver *	Create_KEBSlaveDriver(void)
{
	return New CKEBSlaveDriver;
	}

// Constructor

CKEBSlaveDriver::CKEBSlaveDriver(void)
{
	m_wID		= 0x3409;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "KEB";
	
	m_DriverName	= "DIN66019II Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "KEB DIN66019II Slave";

	AddSpaces();
	}

// Binding Control

UINT CKEBSlaveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CKEBSlaveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CKEBSlaveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CKEBSlaveDriverOptions);
	}

CLASS CKEBSlaveDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKEBSlaveDeviceOptions);
	}

// Implementation

void CKEBSlaveDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "S", "Parameter:Set", 16,  1, 32767, addrLongAsLong));
	}

// Address Management

BOOL CKEBSlaveDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CKEBSlaveDialog Dlg(ThisObject, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CKEBSlaveDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	BOOL fErr = FALSE;

	CString Err;

	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Extra  = 0;

	UINT uPos = 0;

	BYTE bSet = 0;

	UINT uParam = ToHex(Text, 4);

	if( !uParam || (uParam & 0x8000) ) {

		Err.Printf( "<1 - 0x7FFF>:<0 - 0x7>" );

		fErr = TRUE;
		}

	else {
		uPos  = Text.Find(':');

		if( uPos < NOTHING ) {

			bSet = LOBYTE(ToHex(Text.Mid( uPos + 1 ), 2) );

			if( bSet > 7 ) {

				Err.Printf( "%4.4X:<0 - 7>",
					uParam
					);

				fErr = TRUE;
				}
					
			else {
				Addr.a.m_Extra  = bSet;
				Addr.a.m_Offset = uParam;
				}
			}
		}

	if( fErr ) {
				
		Error.Set( Err,
			0
			);
						   
		return FALSE;
		}

	return TRUE;
	}

BOOL CKEBSlaveDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		Text.Printf( "S%4.4X:%1.1X",
			Addr.a.m_Offset,
			Addr.a.m_Extra
			);

		return TRUE;
		}

	return FALSE;	
	}

UINT CKEBSlaveDriver::ToHex(CString Text, UINT uMaxCt)
{
	UINT uResult = 0;
	UINT uChar;

	Text.MakeUpper();

	for( UINT i = 0; i < uMaxCt; i++ ) {

		if( isdigit(Text[i]) || (Text[i] >= 'A' && Text[i] <= 'F') || (Text[i] == ' ') ) {

			uResult <<= 4;

			if( Text[i] != ' ' ) {

				uChar = Text[i] - '0';

				uResult += ( uChar <= 9 ? uChar : uChar - 7 );
				}
			}

		else break;
		}

	return uResult;
	}

//////////////////////////////////////////////////////////////////////////
//
// KEBSlave via MPI Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CKEBSlaveDialog, CStdAddrDialog);
		
// Constructor

CKEBSlaveDialog::CKEBSlaveDialog(CKEBSlaveDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "KEBDIN2ElementDlg";
	}

AfxMessageMap(CKEBSlaveDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	};

// Overridables

BOOL CKEBSlaveDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;

	m_pSpace = m_pDriver->GetSpace(Addr);
	
	if( !m_fPart ) {

		SetCaption();

		LoadList();

		if( m_pSpace ) {

			ShowAddress(Addr);

			SetAddressFocus();

			return FALSE;
			}

		return TRUE;
		}

	else {
		LoadType();
		
		ShowAddress(Addr);

		SetAddressFocus();

		return FALSE;
		}
	}

void CKEBSlaveDialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CKEBSlaveDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	UINT uFind   = Text.GetLength();

	CString c1   = "";
	CString c2   = "";

	if( fEnable ) {

		uFind = Text.Find(':');

		if( uFind < NOTHING ) {

			c2 = Text.Mid(uFind+1);

			c2.MakeUpper();

			if( !IsValidSetChar(c2) ) c2 = "0";

			c1 = Text.Left( min( uFind, 4 ) );
			}

		else {
			c1 = Text.Left( min( uFind, 4 ) );
			}

		ShowDetails();
		}

	GetDlgItem(2002).SetWindowText(c1);

	GetDlgItem(2005).SetWindowText(c2);

	GetDlgItem(2002).EnableWindow(fEnable);
	
	GetDlgItem(2004).EnableWindow(fEnable);
	
	GetDlgItem(2005).EnableWindow(fEnable);
	
	GetDlgItem(2006).EnableWindow(fEnable);
	
	GetDlgItem(2007).EnableWindow(fEnable);
	}

CString CKEBSlaveDialog::GetAddressText(void)
{
	CString Text = GetDlgItem(2002).GetWindowText();

	CString c1   = GetDlgItem(2005).GetWindowText();

	if( c1.GetLength() != 1 ) c1 = "0";

	Text += ":";

	c1.MakeUpper();

	if( !IsValidSetChar(c1) ) c1 = "0";
	
	Text += c1;

	return Text;
	}

void CKEBSlaveDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	Text = Text.Mid(m_pSpace->m_Prefix.GetLength());

	SetAddressText(Text.Left(Text.Find('.')));
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);

	ShowType(addrLongAsLong);
	}

void CKEBSlaveDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	CAddress Addr;

	Addr.a.m_Type  = addrLongAsLong;

	Addr.a.m_Extra = 0;

	Min = "0001:0";
	Max = "7FFF:7";
	Rad = "Hexadecimal";

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

// Helper
BOOL	CKEBSlaveDialog::IsValidSetChar(CString s)
{
	return s.GetLength() == 1 && s[0] >= '0' && s[0] <= '7';
	}

// End of File
