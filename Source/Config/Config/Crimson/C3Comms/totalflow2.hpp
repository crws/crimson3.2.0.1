
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TOTALFLOW2_HPP

#define INCLUDE_TOTALFLOW2_HPP

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Slot Definition
//

struct CTotalFlowSlot {

	UINT	m_App;
	UINT	m_Arr;
	UINT	m_Reg;
	UINT	m_Type;
	UINT	m_Size;
	UINT	m_Slot;
	BOOL	m_IsString;

	bool operator ==(const CTotalFlowSlot &That);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Application Definition
//

class CTotalFlowApp : public CMetaItem
{
	public:

		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlowApp(void);
		CTotalFlowApp(CString Name, UINT uNum);
		CTotalFlowApp(CTotalFlowApp * pApp);

		// Destructor
		~CTotalFlowApp(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT    m_Lookup;
		UINT	m_App;
		CString	m_Name;

		// Comparision Operators
		bool operator ==(const CTotalFlowApp &That) const;
		bool operator < (const CTotalFlowApp &That) const;
		bool operator > (const CTotalFlowApp &That) const;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Application List
//

class CTotalFlowAppList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlowAppList(void);

		// Destructor
		~CTotalFlowAppList(void);

		// Item Access
		CTotalFlowApp * GetItem(INDEX Index) const;
		CTotalFlowApp * GetItem(UINT  uPos ) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Device Options
//

class CTotalFlow2MasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlow2MasterDeviceOptions(void);

		// Destructor
		~CTotalFlow2MasterDeviceOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Slot Management
		virtual BOOL GetSlot(UINT uAddr, CTotalFlowSlot &Slot);
		virtual UINT AddSlot(CTotalFlowSlot &Slot, UINT uStart, UINT uEnd);
		virtual void DeleteSlot(CTotalFlowSlot &Slot);
		virtual void EmptySlots(void);
		virtual UINT GetSlotSize(CTotalFlowSlot const &Slot);
		
		// Application Management
		BOOL    AddApplication(CString const &Name, UINT uNumber);
		BOOL    UsingNamedApps(void);
		UINT    ParseApp(CString App);
		CString ExpandApp(UINT uApp);
		CString FindApp(UINT uApp);
		CString FindApp(CString App);
				
		// Persistence
		void Load(CTreeFile &File);
		void Save(CTreeFile &File);

		// Slot Access
		BOOL Expand(CTotalFlowSlot Slot, CAddress Addr, CString &Text);
		
		// Data Members
		CTotalFlowAppList * m_pApplications;

	protected:
		
		struct CImportError
		{
			CString	m_Message;
			UINT	m_uLine;
			};

		typedef CArray<CImportError> CErrorArray;

		// Data Members
		CString	m_Name;
		CString	m_Code;
		UINT	m_Update;
		UINT	m_Timeout;
		UINT	m_StringSize;
		UINT	m_UseApp;
		UINT	m_AppConfig;
		UINT	m_AppImport;
		UINT	m_AppExport;
		UINT    m_Purge;

		CMap <UINT, INDEX>	m_SlotIndex;
		CList<CTotalFlowSlot>	m_Slots;
		
		// Meta Data Creation
		void AddMetaData(void);
		UINT FindNextSlot(UINT uStart, UINT uEnd);
		void MakeSlotInit(CInitData &Init);
		void MakeAppInit(CInitData &Init);

		// Implementation
		void OnImportApps(CWnd *pWnd);
		void OnExportApps(CWnd *pWnd);
		BOOL ImportApps(FILE *pFile, CErrorArray &Errors);
		BOOL ExportApps(FILE *pFile);
		void LogImportError(CErrorArray &Errors, CString Message, UINT uLine);
		void Purge(CUIViewWnd *pWnd);

		// Helper
		void PrintSlots(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Serial Master Device Options
//

class CTotalFlow2SerialMasterDeviceOptions : public CTotalFlow2MasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlow2SerialMasterDeviceOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT	m_Estab;
		UINT    m_Time1;
		UINT	m_UnkeyDelay;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Master Device Options
//

class CTotalFlow2TcpMasterDeviceOptions : public CTotalFlow2MasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlow2TcpMasterDeviceOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT    m_IP;
		UINT    m_Port;
		UINT    m_Keep;
		UINT    m_Ping;
		UINT    m_Time1;
		UINT    m_Time2;
		UINT    m_Time3;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Comms Driver
//

class CTotalFlow2MasterDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CTotalFlow2MasterDriver(void);

		// Destructor
		~CTotalFlow2MasterDriver(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

		// Notifications
		void NotifyInit(CItem * pConfig);

		// Address Reference
		virtual UINT GetAddress(CAddress Addr);

	protected:
		// Address Helpers
		BOOL CheckBounds(UINT uApp, UINT uArr, UINT uReg);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Serial Master Comms Driver
//

class CTotalFlow2SerialMasterDriver : public CTotalFlow2MasterDriver
{
	public:
		// Constructor
		CTotalFlow2SerialMasterDriver(void);

		// Destructor
		~CTotalFlow2SerialMasterDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Master Comms Driver
//

class CTotalFlow2TcpMasterDriver : public CTotalFlow2MasterDriver
{
	public:
		// Constructor
		CTotalFlow2TcpMasterDriver(void);

		// Destructor
		~CTotalFlow2TcpMasterDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class CTotalFlow2Dialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CTotalFlow2Dialog(CTotalFlow2MasterDriver * pDriver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		CAddress * m_pAddr;
		CItem	 * m_pConfig;
		BOOL	   m_fPart;

		CTotalFlow2MasterDriver * m_pDriver;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnSelChange(UINT uID, CWnd &Wnd);
		void OnAppNumChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadTypeList(void);
		void LoadApplicationList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Application Configuration Dialog
//

class CTotalFlow2AppDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CTotalFlow2AppDialog(CTotalFlow2MasterDeviceOptions *pDevice);

		// Destructor
		~CTotalFlow2AppDialog(void);
		                
	protected:

		// Message Map
		AfxDeclareMessageMap();
		
		// Data Members
		CTotalFlow2MasterDeviceOptions * m_pDevice;
		CTotalFlowAppList	       * m_pApps;
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnAddApp(UINT uID);
		BOOL OnRemoveApp(UINT uID);
		BOOL OnEditApp(UINT uID);
		void OnListItemChange(UINT uID, NMLISTVIEW &Info);
		void OnEditKillFocus(UINT uID, CWnd &Wnd);
		
		// Implementation
		BOOL AddApplication(CString Name, UINT uNumber);
		BOOL RemoveApplication(CString Name, UINT uNumber);
		BOOL EditApplication(CString Name, UINT uOld, UINT uNew);
		void InitListView(void);
		void UpdateListView(void);
		void DoEnables(void);
		BOOL ValidateAppNumber(CString Text, UINT& uNum);
	};

//////////////////////////////////////////////////////////////////////////
//
// Application Number Edit Dialog
//

class CTotalFlow2AppEditDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CTotalFlow2AppEditDialog(CString App, CString Value);

		// Data Access
		CString GetData(void);

	protected:
		// Data Members
		CString m_Group;
		CString m_Data;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
	};

#endif

// End of File
