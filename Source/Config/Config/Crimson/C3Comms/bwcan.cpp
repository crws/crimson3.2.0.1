
#include "intern.hpp"

#include "bwcan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Boulder Wind Power CAN Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBoulderWindPowerRawCANDriverOptions, CRawCANDriverOptions);

// Constructor

CBoulderWindPowerRawCANDriverOptions::CBoulderWindPowerRawCANDriverOptions(void)
{
	m_pService = NULL;

	m_RxPDU    = 32;

	m_TxPDU    = 32;

	m_RxMail   = 32;

	m_TxMail   = 32;
	}

// UI Managament

void CBoulderWindPowerRawCANDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "RxMail" ) {

		pWnd->ShowUI("RxMail", TRUE);
		}

	if( Tag.IsEmpty() || Tag == "TxMail" ) {

		pWnd->ShowUI("TxMail", TRUE);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Boulder Wind Power CAN Driver
//

// Instantiator

ICommsDriver * Create_BoulderWindPowerRawCANDriver(void)
{
	return New CBoulderWindPowerRawCANDriver;
	}

// Constructor			        

CBoulderWindPowerRawCANDriver::CBoulderWindPowerRawCANDriver(void)
{
	m_wID		= 0x40AF;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "Boulder Wind Power";
	
	m_DriverName	= "29-bit CAN Port";
	
	m_Version	= "1.00";
	
	m_ShortName	= "CAN Port";

	m_fSingle	= TRUE;
	}

// Configuration

CLASS CBoulderWindPowerRawCANDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBoulderWindPowerRawCANDriverOptions);
	}

// End of file
