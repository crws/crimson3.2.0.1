
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

/*
LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "pcom.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "pcom.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "pcom.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "pcom.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
*/

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CColumn
//

CColumnUIList RCDATA
BEGIN
	"Type,Data Type,,CUIDropDown,Bit|Byte|Word|Long|Real|User Defined,"
	"Define the data type of the selected column. "
	"\0"

	"Bytes,Byte Count,,CUIEditInteger,|0||1|500,"
	"Enter the number of bytes that exists in each cell of the selected column. "
	"\0"
 
	"\0"
END

CColumnUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Column,Type,Bytes\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CDataTable
//

CDataTableUIList RCDATA
BEGIN
	"Cols,Columns,,CUIEditInteger,|0||1|32,"
	"Define the number of columns that exist in this data table. "
	"\0"

	"Rows,Rows,,CUIEditInteger,|0||1|32767,"
	"Define the number of rows that exist in this data table. "
	"\0"
 
	"\0"
END

CDataTableUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Data Table,Cols,Rows\0"
	"\0"
END


//////////////////////////////////////////////////////////////////////////
//
// UI for CUniPCOMDeviceOptions
//

CUniPCOMDeviceOptionsUIList RCDATA
BEGIN
	"Unit,Unit Number,,CUIEditInteger,|0||0|255,"
	"Indicate the ID of the target device. "
	"\0"

	"\0"
END

CUniPCOMDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Unit\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CUniPCOMTCPDeviceOptions
//

CUniPCOMTCPDeviceOptionsUIList RCDATA
BEGIN
	"IP,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the PCOM device."
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|60000,"
	"Indicate the TCP port number on which the PCOM protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"
  
	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the Unitronics driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the PCOM server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"an PCOM request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CUniPCOMTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,IP,Port,Unit\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CUniPCOMBinaryTCPDeviceOptions
//


 
CUniPCOMBinaryTCPDeviceOptionsUIList RCDATA
BEGIN
	"IP,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the PCOM device."
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|60000,"
	"Indicate the TCP port number on which the PCOM protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"
  
	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the Unitronics driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the PCOM server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"an PCOM request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END



CUniPCOMBinaryTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,IP,Port,Unit\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END


/////////////////////////////////////////////////////////////////////////
//
// Unitronics Binary Data Table Configuration Dialog
//

CPCOMDataTableDlg DIALOG 0, 0, 0, 0
CAPTION "Data Table Management"
BEGIN
	
	GROUPBOX	"Data Table Definitions",	1000,		  4,   4, 128, 172
	CONTROL		"",				1001, "CTreeView", WS_TABSTOP, 10,  18, 70, 152, WS_EX_CLIENTEDGE
	PUSHBUTTON	"Create",			2001,		 85, 18,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Edit",				2002,		 85, 37,  40,  14, XS_BUTTONREST

	
	DEFPUSHBUTTON   "OK",		IDOK,		  4, 180,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	 48, 180,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",		IDHELP,		 92, 180,  40,  14, XS_BUTTONREST
END

//////////////////////////////////////////////////////////////////////////
//
// Unitronics Binary PCOM Element Dialog
//
//

UniBinaryElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT		2002,	 0, 0, 22, 12, 
	CTEXT		":",	2004,	25, 1, 14, 12
	EDITTEXT		2005,	42, 0, 26, 12,
END

// End of File
