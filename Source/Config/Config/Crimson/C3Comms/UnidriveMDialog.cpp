#include "intern.hpp"

#include "UnidriveM.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unidrive M Address Selection Dialog
//

AfxImplementRuntimeClass(CUnidriveMDialog, CStdDialog);

AfxMessageMap(CUnidriveMDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(3002, OnSearch)
	AfxDispatchCommand(3003, OnSortAlpha)
	AfxDispatchCommand(3004, OnSortNum)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnMenuSelChanged)
	AfxDispatchNotify(1002, LBN_SELCHANGE, OnItemSelChanged)
	AfxDispatchNotify(1002, LBN_DBLCLK, OnDblClk)

	AfxMessageEnd(CUnidriveMDialog)
	};

// Constructor

CUnidriveMDialog::CUnidriveMDialog(CUnidriveMDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pDriver = &Driver;

	m_pConfig = (CUnidriveMDeviceOptions *) pConfig;

	m_pAddr	= &Addr;

	SetName(TEXT("FullUnidriveMDlg"));
	}

// Message Handlers

BOOL CUnidriveMDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_uCurrentMode = m_pConfig->GetDataAccess(TEXT("DriveMode"))->ReadInteger(m_pConfig);

	BuildMenuMap();

	LoadMenus();

	CEditCtrl &OffsetCtrl = (CEditCtrl &) GetDlgItem(1005);

	OffsetCtrl.LimitText(5);

	if( m_pAddr && m_pAddr->m_Ref ) {

		SetCurrentSel();
		}

	DoEnables();

	return TRUE;
	}

BOOL CUnidriveMDialog::OnOkay(UINT uID)
{
	CListBox &ItemBox = (CListBox &) GetDlgItem(1002);

	if( uID == IDOK ) {

		UINT uSel = ItemBox.GetCurSel();

		UINT uRef = ItemBox.GetItemData(uSel);

		UINT uMenu = uRef / 1000;

		UINT uItem = uRef % 1000;

		CString Text;

		CAddress Addr;

		CError Error;

		if( uMenu == MENU_MODBUS ) {

			Text = m_pDriver->GetPrefix(uItem) + GetDlgItem(1005).GetWindowText();

			Addr.a.m_Table = uItem;
			}
		else {
			Text = ItemBox.GetText(uSel);

			UINT uTab = Text.Find(L'\t');

			if( uTab < NOTHING ) {

				Text = Text.Left(uTab);
				}
			}

		CUnidriveMItem Item;

		if( m_pConfig->FindItem(uRef, Item) || uRef == 0 || Text.IsEmpty() ) {

			if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				*m_pAddr = Addr;

				EndDialog(TRUE);

				return TRUE;
				}

			Error.Show(ThisObject);

			return FALSE;
			}
		else
			AfxAssert(FALSE);
		}

	EndDialog(TRUE);

	return TRUE;
	}

void CUnidriveMDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

BOOL CUnidriveMDialog::OnSearch(UINT uID)
{
	CString Search = GetDlgItem(3001).GetWindowText().ToLower();

	if( !Search.IsEmpty() ) {	

		CItemArray MatchingItems;

		CMenuMap MenuMap = m_MenuMap;
		
		for( INDEX i = MenuMap.GetHead(); !MenuMap.Failed(i); MenuMap.GetNext(i) ) {

			CItemArray LookIn = MenuMap.GetData(i)->m_Items;

			for( UINT n = 0; n < LookIn.GetCount(); n++ ) {

				CString Desc = LookIn[n].m_pDesc;

				if( Desc.ToLower().Find(Search) < NOTHING ) {

					MatchingItems.Append(LookIn[n]);
					}
				}
			}
		
		UINT uCount = MatchingItems.GetCount();

		CString ResultText;

		if( uCount == 0 ) {

			ResultText = "No matches found";
			}
		else {
			CString Format = uCount > 1 ? "Found %d matches" : "Found %d match";

			ResultText.Printf(Format, uCount);
			}

		GetDlgItem(1000).SetWindowText(ResultText);

		CListBox &MenuBox = (CListBox &) GetDlgItem(1001);

		MenuBox.SetCurSel(0);

		LoadItems(MatchingItems);
		}

	return TRUE;
	}

BOOL CUnidriveMDialog::OnSortAlpha(UINT uID)
{
	DoSort(TRUE);

	return TRUE;
	}

BOOL CUnidriveMDialog::OnSortNum(UINT uID)
{
	DoSort(FALSE);

	return TRUE;
	}

void CUnidriveMDialog::OnMenuSelChanged(UINT uID, CWnd &Wnd)
{
	// Load the premapped items for the selected menu
	
	CListBox &MenuBox = (CListBox &) GetDlgItem(1001);

	UINT uMenu = MenuBox.GetCurSelData();

	CMenuMap MenuMap = m_MenuMap;

	INDEX i = MenuMap.FindName(uMenu);

	if( i && i != INDEX(NOTHING) ) {

		CUnidriveMMenu *pMenu = MenuMap[i];

		CString MenuDesc = CPrintf("%s - %s", pMenu->GetName(), pMenu->m_Desc);

		GetDlgItem(1000).SetWindowText(MenuDesc);

		CItemArray Items(pMenu->m_Items);

		LoadItems(Items);
		}
	else {
		GetDlgItem(1000).SetWindowText(L"Items");

		CItemArray Empty;

		LoadItems(Empty);
		}

	DoEnables();

	ShowDetails();
	}

void CUnidriveMDialog::OnItemSelChanged(UINT uID, CWnd &Wnd)
{
	ShowDetails();

	DoEnables();
	}

void CUnidriveMDialog::BuildMenuMap(void)
{
	m_MenuMap = m_pConfig->GetMenus();
	}

void CUnidriveMDialog::LoadMenus(void)
{
	CListBox &MenuBox = (CListBox &) GetDlgItem(1001);

	MenuBox.SetRedraw(FALSE);

	MenuBox.ResetContent();

	MenuBox.AddString(CPrintf("<%s>", CString(IDS_DRIVER_NOSELECTION)), 0);

	CMenuMap MenuMap = m_MenuMap;

	for( INDEX i = MenuMap.GetHead(); !MenuMap.Failed(i); MenuMap.GetNext(i) ) {

		CString Name = MenuMap.GetData(i)->GetName();

		MenuBox.AddString(Name, MenuMap[i]->m_uMenu);
		}

	MenuBox.SetRedraw(TRUE);

	MenuBox.Invalidate(TRUE);
	}

void CUnidriveMDialog::LoadItems(CItemArray const &Items)
{
	m_LoadedItems = Items;

	CListBox &ItemBox = (CListBox &) GetDlgItem(1002);

	ItemBox.SetRedraw(FALSE);

	ItemBox.ResetContent();

	for( UINT n = 0; n < Items.GetCount(); n++ ) {

		CString Desc;

		UINT uRef = Items[n].m_uMenu * 1000 + Items[n].m_uItem;

		if( Items[n].m_uMenu == MENU_MODBUS ) {

			Desc.Printf("%s", Items[n].m_pDesc);
			}
		else {
			Desc.Printf(L"%2.2d_%3.3d\t%s", Items[n].m_uMenu, Items[n].m_uItem, Items[n].m_pDesc);
			}

		ItemBox.AddString(Desc, uRef);
		}

	ItemBox.SetRedraw(TRUE);

	ItemBox.Invalidate(TRUE);
	}

void CUnidriveMDialog::SetCurrentSel(void)
{
	// If there's a mapped address, find it and select it.

	CListBox &MenuBox = (CListBox &) GetDlgItem(1001);

	CListBox &ItemBox = (CListBox &) GetDlgItem(1002);

	UINT uRegMode = m_pConfig->GetDataAccess(TEXT("RegisterMode"))->ReadInteger(m_pConfig);

	UINT uItem = 0;

	UINT uMenu = 0;

	if( m_pAddr->a.m_Table == addrNamed ) {

		if( uRegMode == regModeStd ) {
	
			uMenu = m_pAddr->a.m_Offset / 100;

			uItem = m_pAddr->a.m_Offset - uMenu * 100 + 1;
			}
		else {		
			uMenu = m_pAddr->a.m_Offset / 256;

			uItem = m_pAddr->a.m_Offset - uMenu * 256 + 1;
			}
		}
	else {
		uMenu = MENU_MODBUS;

		uItem = m_pAddr->a.m_Table;

		GetDlgItem(1005).SetWindowText(CPrintf("%5.5d", m_pAddr->a.m_Offset));

		GetDlgItem(1004).SetWindowText(m_pDriver->GetPrefix(m_pAddr->a.m_Table));
		}
	
	for( UINT n = 0; n < MenuBox.GetCount(); n++ ) {

		if( MenuBox.GetItemData(n) == uMenu ) {

			MenuBox.SetCurSel(n);

			break;
			}
		}

	CMenuMap MenuMap = m_MenuMap;		

	INDEX i = MenuMap.FindName(uMenu);

	if( i && i != INDEX(NOTHING) ) {

		CUnidriveMMenu *pMenu = MenuMap[i];

		CString MenuDesc = CPrintf("%s - %s", pMenu->GetName(), pMenu->m_Desc);

		GetDlgItem(1000).SetWindowText(MenuDesc);

		CItemArray Items(pMenu->m_Items);

		LoadItems(Items);
		}

	for( UINT k = 0; k < ItemBox.GetCount(); k++ ) {

		UINT uData = ItemBox.GetItemData(k) % 1000;

		if( uData == uItem ) {

			ItemBox.SetCurSel(k);

			break;
			}
		}

	ShowDetails();
	}

void CUnidriveMDialog::ShowDetails(void)
{
	CListBox &ItemBox = (CListBox &) GetDlgItem(1002);

	UINT uSel = ItemBox.GetCurSel();

	CString Text = ItemBox.GetText(uSel);

	UINT uRef = ItemBox.GetItemData(uSel);

	CUnidriveMItem Item;

	if( m_pConfig->FindItem(uRef, Item) ) {

		UINT uMenu = uRef / 1000;

		UINT uItem = uRef % 1000;

		if( uMenu == MENU_MODBUS ) {

			CString MinText = CPrintf("%s%5.5d", m_pDriver->GetPrefix(uItem), m_pDriver->GetMin(uItem));
			CString MaxText = CPrintf("%s%5.5d", m_pDriver->GetPrefix(uItem), m_pDriver->GetMax(uItem));

			GetDlgItem(2002).SetWindowText(MinText);
			GetDlgItem(2004).SetWindowText(MaxText);
			GetDlgItem(2006).SetWindowText(m_pDriver->GetRadixText(uItem));
			GetDlgItem(2008).SetWindowText(CUnidriveMDriver::GetTypeText(Item.m_uType));
			GetDlgItem(1004).SetWindowText(m_pDriver->GetPrefix(uItem));
			}

		else if( uMenu == 0 ) {

			GetDlgItem(2008).SetWindowText(L"");
			GetDlgItem(2006).SetWindowText(L"");
			GetDlgItem(2004).SetWindowText(L"");
			GetDlgItem(2002).SetWindowText(L"");
			GetDlgItem(1004).SetWindowText(L"");
			}		
		else {
			CString Type = CUnidriveMDriver::GetTypeText(Item.m_uType);

			GetDlgItem(2008).SetWindowText(Type);
			GetDlgItem(2006).SetWindowText(L"");
			GetDlgItem(2004).SetWindowText(L"");
			GetDlgItem(2002).SetWindowText(L"");
			GetDlgItem(1004).SetWindowText(L"");
			}
		}
	}

void CUnidriveMDialog::DoEnables(void)
{
	// Enable the offset box for Modbus addresses, else disable it

	CListBox &ItemBox = (CListBox &) GetDlgItem(1002);

	UINT uSel = ItemBox.GetCurSel();

	CString Text = ItemBox.GetText(uSel);

	UINT uRef = ItemBox.GetItemData(uSel);

	UINT uMenu = uRef / 1000;

	if( uSel != NOTHING && uMenu == MENU_MODBUS ) {

		GetDlgItem(1005).EnableWindow(TRUE);

		if( GetDlgItem(1005).GetWindowTextLength() == 0 ) {

			GetDlgItem(1005).SetWindowText(TEXT("00001"));
			}
		}
	else {
		GetDlgItem(1004).SetWindowText(L"Offset");

		GetDlgItem(1005).EnableWindow(FALSE);
		}
	}

void CUnidriveMDialog::DoSort(BOOL IsAlpha)
{
	UINT uCount = m_LoadedItems.GetCount();
	
	CUnidriveMItem *pSort = new CUnidriveMItem[ uCount ];

	memcpy(pSort, m_LoadedItems.GetPointer(), uCount * sizeof(*pSort));

	if( IsAlpha )
		qsort(pSort, uCount, sizeof(*pSort), SortHelpAlpha);
	else
		qsort(pSort, uCount, sizeof(*pSort), SortHelpNum);

	CItemArray Sorted;

	for( UINT n = 0; n < uCount; n++ ) {

		Sorted.Append(pSort[n]);
		}

	delete [] pSort;

	LoadItems(Sorted);	
	}

int CUnidriveMDialog::SortHelpAlpha(const void *a, const void *b)
{	
	CUnidriveMItem *pLeft  = (CUnidriveMItem *) a;
	CUnidriveMItem *pRight = (CUnidriveMItem *) b;

	return wstricmp(pLeft->m_pDesc, pRight->m_pDesc);
	}

int CUnidriveMDialog::SortHelpNum(const void *a, const void *b)
{
	CUnidriveMItem *pLeft  = (CUnidriveMItem *) a;
	CUnidriveMItem *pRight = (CUnidriveMItem *) b;

	int Left  = pLeft ->m_uMenu * 1000 + pLeft ->m_uItem;
	int Right = pRight->m_uMenu * 1000 + pRight->m_uItem;

	return Left - Right;
	}

// End of File
