
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KELTECPS_HPP
	
#define	INCLUDE_KELTECPS_HPP

class CKeltecPowerSupplyDriver;

// Data Type abbreviations
#define	AN	addrNamed
#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Space Definitions
enum {
// Queries - Address Named
	STQ  = 10,	// Status Query (1)
	OCQ  = 11,	// Output Current Query (6)
	OVQ  = 12,	// Output Voltage Query (7)
	OPQ  = 13,	// Output Power Query (8)
	TMQ  = 14,	// Temperature Query (=)
	CSQ  = 15,	// Current State Query (C)
// Commands
	ARDY = 20,	// Activate Ready Mode Command (4)
	ARUN = 21,	// Activate Run Mode Command (5)
	SLFT = 22,	// Self Test Command (2)
	FRST = 23,	// Fault Reset Command (9)
	REBT = 24,	// Reboot (!)
	SETC = 25,	// Compute and set checksum (*)
	MTMD = 26,	// Maintenance Mode (M)
	RELD = 27,	// Reload (P)
// Read Only Table
	ADQ  =  1,	// ADC Query (A)
// R/W values named
	SRI  =  2,	// Set/Read Current (I)
	FTMO =  3,	// Set/Read Fault Timeout (X)
// R/W Table
	NVWD =  4,	// Set/Read 16 bit NV Parameters (Y)
	NVBY =  5,	// Set/Read  8 bit NV Parameters (Y)
// String Response
	IDQ  =  6,	// ID String (0)
// User Commands
	USR  = 17,	// Send USRC
	USRC = 18,	// User Command String
	USRR = 19,	// USRC Response
// Latest Error Code
	ERRC = 30,	// Latest Error Code (Internal)
	};

//////////////////////////////////////////////////////////////////////////
//
// Keltec Power Supply Comms Driver
//

class CKeltecPowerSupplyDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKeltecPowerSupplyDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Other Helpers

	protected:

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner Address Selection
//

class CKeltecPowerSupplyAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CKeltecPowerSupplyAddrDialog(CKeltecPowerSupplyDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		BOOL	OnOkay(UINT uID);

		// Helpers
		void	HandleDetails(UINT uTable, UINT uOffset);
	};

// End of File

#endif
