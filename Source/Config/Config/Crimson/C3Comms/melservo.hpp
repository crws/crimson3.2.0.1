
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MELSERVO_HPP

#define	INCLUDE_MELSERVO_HPP

#define	AN	addrNamed
#define BB	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Device ID's
#define	J2SCL	0
#define	J2SA	1
#define	J2SCP	2
#define	J3A	3

// Group ID's
#define	STATION		0
#define	GROUP		1
#define	ALLUNITS	2

// Data Spaces
#define	SPARLP	1
#define	SPARA	2
#define	SSTSL	3
#define	SSTSA	4
#define	SALN	5
#define	SALT	6
#define	SALSL	7
#define	SALSA	8
#define	SACU	9
#define	SEXT0	10
#define	SEXTA	11
#define	SLAT	12
#define	SGPRR	13
#define	SGPRD	14
#define	SGSV	15
#define	SSEP	16
#define	SCUP	17
#define	SSVR	18
#define	SACL	19
#define	SSCL	20
#define	SOMS	21
#define	STOM	22
#define	SEIS	23
#define	SEOS	24
#define	SWPEC	25
#define	SWPEA	26
#define	SWGER	27
#define	SWGED	28
#define	SCACHE	30
#define	SSTSP	31
#define	SALSP	32
#define	SPOS	33
#define	SSPD	34
#define	SACC	35
#define	SDEC	36
#define	SDWL	37
#define	SAUX	38
#define	SWEEP	39
#define	SGENR	40
#define	SGENW	41
#define	SPARLPH	42
#define	SPARAH	43
// July 25 2008 additions for MR-J3
#define	SSTS3U	44	// 01 00-0E Unit
#define	SSTS3N	45	// 01 00-0E Name
#define	SGRP	46	// 04 01 Parameter Group Read
#define	SPAR3	47	// 05 00-FF Parameter Read Real
#define	SPAR3H	48	// 05 00-FF Parameter Read Long
#define	SPARU	49	// 06 00-FF Parameter Upper Limit Real
#define	SPARUH	50	// 06 00-FF Parameter Upper Limit Long
#define	SPARL	51	// 07 00-FF Parameter Lower Limit Real
#define	SPARLH	52	// 07 00-FF Parameter Lower Limit Long
#define	SPARAB	53	// 08 00-FF Parameter Abbreviation
#define	SPAREN	54	// 09 00-FF Write enable/disable of Parameters
#define	SALSU	55	// 35 00-0E Unit
#define	SALSN	56	// 35 00-0E Name
#define	STOM3M	57	// 00 12 Test Operation Mode Read
#define	STOM3S	58	// 00 21 Read TOM status

#define	SPBAD	99	// Bad selection in Part Dialog
// Information Size
#define	MAXINFO	18

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi MELServo Driver
//

class CMELServoDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMELServoDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);

		// Helpers
		BOOL	IsGeneric(UINT uTable);
		BOOL	IsJ3UnitAndName(UINT uTable);
		BOOL	IsJ3ParameterItem(UINT uTable);
		UINT	IsSpecialItem(CItem *pConfig, UINT uTable);
		BOOL	DeviceIsJ3(CItem *pConfig);

		// Friend
		friend class CMELServoAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi MELServo Device Options
//

class CMELServoDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMELServoDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Addr;
		UINT	m_Group;
		UINT	m_ServoType;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MELServo Address Selection
//

class CMELServoAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CMELServoAddrDialog(CMELServoDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		UINT	m_uDevice;
		CMELServoDriver *m_pMDriver;
		CItem  *m_pConfig;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		BOOL	OnOkay(UINT uID);

		// Overridables
		void	ShowAddress(CAddress Addr);
		BOOL	AllowSpace(CSpace *pSpace);

		// Helpers
		void	ShowInfo(void);
		void	ClearInfo(void);
		void	SetAddrEntry(UINT uTable);
		void	ShowAddrAndInfo(CAddress Addr);
	};

// Info Strings
#define	S_0	L"0  = "
#define	S_1	L"1  = "
#define	S_2	L"2  = "
#define	S_3	L"3  = "
#define	S_4	L"4  = "
#define	S_5	L"5  = "
#define	S_6	L"6  = "
#define	S_7	L"7  = "
#define	S_8	L"8  = "
#define	S_9	L"9  = "
#define	S_A	L"A  = "
#define	S_B	L"B  = "
#define	S_C	L"C  = "
#define	S_D	L"D  = "
#define	S_E	L"E  = "
#define	S_F	L"F  = "
#define	S_10	L"10 = "
#define	S_11	L"11 = "

#define	S_S00	L"Current Position"
#define	S_S01	L"Command Position"
#define	S_S02	L"Command Remaining Distance"
#define	S_S03	L"Program Number"
#define	S_S04	L"Step Number"
#define	S_S05	L"Cumulative Feedback Pulses"
#define	S_S06	L"Servo Motor Speed"
#define	S_S07	L"Droop Pulses"
#define	S_S08	L"Override"
#define	S_S09	L"Torque Limit Voltage"
#define	S_S0A	L"Regenerative Load Ratio"
#define	S_S0B	L"Effective Load Ratio"
#define	S_S0C	L"Peak Load Ratio"
#define	S_S0D	L"Instantaneous Torque"
#define	S_S0E	L"Within One Revolution Position"
#define	S_S0F	L"ABS Counter"
#define	S_S10	L"Load Inertia Moment Ratio"
#define	S_S11	L"Bus Voltage"
// in CP
#define S_S12	L"Point table Number" // 03
#define	S_S18	L"Select Point Number (1 - 31)"
// in A
#define	S_S14	L"Cumulative Command Pulses" // 03
#define	S_S15	L"Command Pulse Frequency" // 04
#define	S_S16	L"Analog Speed Command/Limit Voltage" // 05
#define	S_S17	L"Analog Torque Command/Limit Voltage" // 06

#define	S_S19	L"W = "
#define	S_S20	L"W = 90"
#define	S_S21	L"STRING"
#define	S_S22	L"User Selected"
#define	S_REAL	L"REAL"
#define	S_CAUT	L"CAUTION:"
#define	S_MANY	L"Too many writes will damage the device"

#define	S_S30	L"A - Basic Setting Parameter"
#define	S_S31	L"B - Gain Filter Parameter"
#define	S_S32	L"C - Extension Setting Parameter"
#define	S_S33	L"D - I/O Setting Parameter"
#define	S_S34	L"Use String Tag, Packed High-To-Low"
#define	S_S35	L"Length = 16"
#define	S_S36	L"PACKED CHARACTERS"
#define	S_S37	L"Length = 4"
#define	S_S38	L"Length = As Appropriate"
#define	S_S39	L"Distance"
#define	S_S40	L"Temporary Stop"
#define	S_S41	L"Set Data != 0 "
#define	S_S42	L" (Set Data != 0)"
#define	S_S43	L"       signals and pulse train inputs..."
#define	S_S44	L"Jog Operation"
#define	S_S45	L"Positioning Operation"
#define	S_S46	L"Motor-less Operation"
#define	S_S47	L"Output signal (DO) forced output"

// Macro
#define	MS(w, x, y, z)	w[x].Printf( L"%s%s", y, z )
// End of File

#endif
