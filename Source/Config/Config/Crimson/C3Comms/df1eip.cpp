
#include "intern.hpp"

#include "df1eip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master DF1 over EIP Device Options
//

AfxImplementDynamicClass(CDF1MasterEIPDeviceOptions, CUIItem);

// Constructor

CDF1MasterEIPDeviceOptions::CDF1MasterEIPDeviceOptions(void)
{
	m_Device  = 1;
	
	m_Address = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Slot	  = 0;

	m_Time	  = 1000;

	m_Abort   = 0;

	m_Root    = "";

	m_Name    = 0x02;

	m_Label   = 0x7C;
	}

// UI Managament

void CDF1MasterEIPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CDF1MasterEIPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddLong(DWORD(m_Address));
	Init.AddWord(WORD(m_Slot));
	Init.AddWord(WORD(m_Time));
	Init.AddByte(BYTE(m_Abort));

	return TRUE;
	}

// Meta Data Creation

void CDF1MasterEIPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Address);
	Meta_AddInteger(Slot);
	Meta_AddInteger(Time);
	Meta_AddInteger(Abort);
	Meta_AddString (Root);
	Meta_AddInteger(Name);
	Meta_AddInteger(Label);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master DF1 over EIP Driver
//

// Instantiator

ICommsDriver * Create_DF1MasterEIPDriver(void)
{
	return New CDF1MasterEIPDriver;
	}

// Constructor

CDF1MasterEIPDriver::CDF1MasterEIPDriver(void)
{
	m_wID		= 0x350E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "DF1 Master via PCCC/EIP";
	
	m_Version	= "1.01";
	
	m_ShortName	= "DF1 Master via PCCC/EIP";
	
	AddSpaces();

	C3_PASSED();
	}

// Binding Control

UINT CDF1MasterEIPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CDF1MasterEIPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CDF1MasterEIPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CDF1MasterEIPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDF1MasterEIPDeviceOptions);
	}

// Implementation     

void CDF1MasterEIPDriver::AddSpaces(void)
{
	AddSpace(New CSpaceAB(1,	"O", "Outputs",		10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(2,	"I", "Inputs",		10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(3,	"S", "Status",		10, 0, 9999, addrWordAsWord, AB_FILE_NONE, AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(4,	"B", "Bits",		10, 0, 9999, addrWordAsWord, 3,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(5,	"N", "Integers",	10, 0, 9999, addrWordAsWord, 7,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(6,	"T", "Timers",		10, 0, 9999, addrWordAsWord, 4,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));

	AddSpace(New CSpaceAB(7,	"C", "Counters",	10, 0, 9999, addrWordAsWord, 5,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));
	
	AddSpace(New CSpaceAB(8,	"F", "Floating Point",	10, 0, 9999, addrLongAsReal, 8,		   AB_FILE_MIN, AB_FILE_MAX));

	AddSpace(New CSpaceAB(9,	"L", "Long Word",	10, 0, 9999, addrLongAsLong, 9,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(10,	"R","String Area",	10, 0,  124, addrByteAsByte, 10,	   AB_FILE_MIN, AB_FILE_MAX));
	}

// End of File
