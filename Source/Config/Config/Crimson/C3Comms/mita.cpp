
#include "intern.hpp"

#include "mita.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series
//

// Instantiator

ICommsDriver *	Create_MitsubADriver(void)
{
	return New CMitsubADriver;
	}

// Constructor

CMitsubADriver::CMitsubADriver(void)
{
	m_wID		= 0x3396;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Mitsubishi Electric";
	
	m_DriverName	= "A/Q/FX Series PLC - MELSEC";
	
	m_Version	= "1.02";
	
	m_ShortName	= "MELSEC";

	AddSpaces();

	C3_PASSED();
	}

// Binding Control

UINT CMitsubADriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMitsubADriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CMitsubADriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMitsubAQMasterDriverOptions);
	}

// Configuration

CLASS CMitsubADriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMitsubAQMasterDeviceOptions);
	}

// Address Management

BOOL CMitsubADriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CMitsubADialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void CMitsubADriver::AddSpaces(void)
{
	AddSpace(New CSpace('X', "X",  "Input",			 16,  0, 0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('Y', "Y",  "Output",		 16,  0, 0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('1', "DX", "Direct Input",		 16,  0, 0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('2', "DY", "Direct Output",		 16,  0, 0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('F', "F",  "Annunciator",		 10,  0,   2047, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('M', "M",  "Internal Relay",	 10,  0,   9255, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('L', "L",  "Latch Relay",		 10,  0,   8191, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('B', "B",  "Link Relay",		 16,  0, 0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('S', "S",  "State Relay",		 10,  0,   4095, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('V', "V",  "Edge Relay",		 10,  0,   2047, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('4', "SB", "Special Link Relay",     16,  0,  0x7FF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('5', "SW", "Special Link Register",  16,  0,  0x7FF, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('6', "SM", "Special Relay",		 10,  0,   2047, addrBitAsBit,   addrBitAsWord));
	AddSpace(New CSpace('7', "SD", "Special Register",       10,  0,   2047, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('D', "D",  "Data Register",		 10,  0,  32767, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('R', "R",  "File Register",		 10,  0,  32767, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('Z', "Z",  "Index Register",	 10,  0,     15, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('W', "W",  "Link Register",		 16,  0, 0x657F, addrWordAsWord));
	AddSpace(New CSpace('T', "TN", "Timer Current Value",	 10,  0,   2047, addrWordAsWord));
	AddSpace(New CSpace('8', "TS", "Timer Contact",		 10,  0,   2047, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('9', "TC", "Timer Coil",		 10,  0,   2047, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('C', "CN", "Counter Current Value",	 10,  0,   1023, addrWordAsWord));
	AddSpace(New CSpace('A', "CS", "Counter Contact",        10,  0,   1023, addrBitAsBit, addrBitAsWord));
	AddSpace(New CSpace('E', "CC", "Counter Coil",		 10,  0,   1023, addrBitAsBit, addrBitAsWord));
	//AddSpace(New CSpace('3', "SN", "Ret. Timer Curent Value",10,  0,   2047, addrWordAsWord));

	}

//////////////////////////////////////////////////////////////////////////
//
// Mistubishi A / Q Series Master TCP/IP
//

// Instantiator

ICommsDriver *	Create_CMitsAQMasterTCPDriver(void)
{
	return New CMitsAQMasterTCPDriver;
	}

// Constructor

CMitsAQMasterTCPDriver::CMitsAQMasterTCPDriver(void)
{
	m_wID		= 0x3507;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Mitsubishi";
	
	m_DriverName	= "A/Q/FX Series TCP/IP Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "A/Q/FX TCP/IP Master";

	C3_PASSED();
	}

// Binding Control

UINT CMitsAQMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CMitsAQMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CMitsAQMasterTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CMitsAQMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMitsAQMasterTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Mistubishi A / Q Series Master UDP/IP
//

// Instantiator

ICommsDriver *	Create_CMitsAQMasterUDPDriver(void)
{
	return New CMitsAQMasterUDPDriver;
	}

// Constructor

CMitsAQMasterUDPDriver::CMitsAQMasterUDPDriver(void)
{
	m_wID		= 0x4076;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Mitsubishi";
	
	m_DriverName	= "A/Q/FX Series UDP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "A/Q/FX UDP/IP Master";
	}

// Binding Control

void CMitsAQMasterUDPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_UDPCount = 1;

	Ether.m_TCPCount = 0;
	}

// Configuration

CLASS CMitsAQMasterUDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMitsAQMasterUDPDeviceOptions);
	}


//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CMitsubAQMasterDriverOptions, CUIItem);

// Constructor

CMitsubAQMasterDriverOptions::CMitsubAQMasterDriverOptions(void)
{
	m_fCRC	= TRUE;
	}

// Download Support

BOOL CMitsubAQMasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_fCRC));

	return TRUE;
	}

// Meta Data Creation

void CMitsubAQMasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddBoolean(CRC);
	}

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMitsubAQMasterDeviceOptions, CUIItem);
				   
// Constructor

CMitsubAQMasterDeviceOptions::CMitsubAQMasterDeviceOptions(void)
{
	m_Drop		= 0;

	m_Melsec	= 0;

	m_Net		= 0;

	m_PC		= 0;

	m_Cpu		= 0;
	}

// UI Managament

void CMitsubAQMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Melsec" ) {

		pWnd->EnableUI("PC", m_Melsec == 2);

		pWnd->EnableUI("Cpu", m_Melsec == 2);

		pWnd->EnableUI("Net", m_Melsec == 2);
		}	
	}

// Download Support

BOOL CMitsubAQMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Melsec));
	Init.AddByte(BYTE(m_PC));
	Init.AddByte(BYTE(m_Cpu));
	Init.AddByte(BYTE(m_Net));
	
	return TRUE;
	}

// Meta Data Creation

void CMitsubAQMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Melsec);
	Meta_AddInteger(PC);
	Meta_AddInteger(Cpu);
	Meta_AddInteger(Net);
	}

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMitsAQMasterTCPDeviceOptions, CUIItem);

// Constructor

CMitsAQMasterTCPDeviceOptions::CMitsAQMasterTCPDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket  = 5002;

	m_Code    = 0;

	m_Monitor = 10;

	m_Keep    = TRUE;

	m_Ping    = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;

	m_PC	  = 255;

	m_Melsec  = 0;

	m_Cpu	  = 0;
	}

// UI Managament

void CMitsAQMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag == "PC" ) {

			if ( m_PC > 64 ) {

				m_PC = 255;

				pWnd->UpdateUI("PC");
				}
			}
		}
	}

// Download Support	     

BOOL CMitsAQMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Code));
	Init.AddWord(WORD(m_Monitor));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_PC));
	Init.AddByte(BYTE(0x00));
	Init.AddByte(BYTE(m_Cpu));

	return TRUE;
	}

// Meta Data Creation

void CMitsAQMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Code);
	Meta_AddInteger(Monitor);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(PC);
	Meta_AddInteger(Melsec);
	Meta_AddInteger(Cpu);
	}

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series UDP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMitsAQMasterUDPDeviceOptions, CMitsAQMasterTCPDeviceOptions);


//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A / Q Series Address Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CMitsubADialog, CStdAddrDialog);
		
// Constructor

CMitsubADialog::CMitsubADialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pItem, BOOL fPart) : CStdAddrDialog(Driver, Addr, pItem, fPart)
{
	m_uMelsec = pItem->GetDataAccess("Melsec")->ReadInteger(pItem);

	m_fEthernet = pItem->GetDataAccess("PC")->ReadInteger(pItem) ? TRUE : FALSE;

	}

// Overridables

BOOL CMitsubADialog::AllowType(UINT uType)
{
	// TODO -- This works here, but doesn't stop direct address entry!

	return uType != addrBitAsByte;
	}

BOOL CMitsubADialog::AllowSpace(CSpace *pSpace)
{
	switch(pSpace->m_uTable) {

		case '1':
		case '2':
		case 'V':
		case '4':
		case '5':
		case '6':
		case '7':
		case 'Z':
		case '3':
		case 'L':

			if( m_uMelsec < 2 ) {

				return FALSE;
				}
			break;
		
		/*case 'S':
	
			if( m_fEthernet ) {

				return TRUE;
				}

			if( m_uMelsec < 2 ) {

				return FALSE;
				}
			break;*/
		}

	return TRUE;

	}


// End of File
