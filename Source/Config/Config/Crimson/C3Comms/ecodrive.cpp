
#include "intern.hpp"

#include "ecodrive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
//  Ecodrive Driver
//

// Instantiator

ICommsDriver *	Create_EcodriveDriver(void)
{
	return New CEcodriveDriver;
	}

// Constructor

CEcodriveDriver::CEcodriveDriver(void)
{
	m_wID		= 0x338E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Rexroth Indramat";
	
	m_DriverName	= "Ecodrive";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Ecodrive";

	AddSpaces();
	}

// Binding Control

UINT	CEcodriveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CEcodriveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CEcodriveDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CEcodriveAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CEcodriveDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) {

		return FALSE;
		}

	UINT uTable = pSpace->m_uTable;

	if( uTable >= LPP ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	CString Lft;

	CString Rgt;

	UINT uOffset;

	UINT uSet = 0;

	UINT uFind1  = Text.Find('(');

	UINT uFind2  = Text.Find(')');

	BOOL fBad    = TRUE;

	if( (uFind1 < uFind2 - 1) && (uFind2 < NOTHING) ) {

		Lft = Text.Left(uFind1);

		Rgt = Text.Mid(uFind1+1, uFind2 - uFind1 - 1);

		uSet = tatoi(Rgt);

		fBad = uSet > 7;
		}

	if( fBad ) {

		Error.Set( "aaaa(0) - aaaa(7)",
			0
			);

		return FALSE;
		}

	uOffset = tatoi( Lft );

	if( uOffset > 4095 ) {

		Error.Set( "0(n) - 4095(n)",
			0
			);

		return FALSE;
		}

	Addr.a.m_Table  = uTable;
	Addr.a.m_Type   = addrRealAsReal;
	Addr.a.m_Extra  = uSet;
	Addr.a.m_Offset = uOffset;

	return TRUE;
	}

BOOL CEcodriveDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	switch( Addr.a.m_Table ) {

		case SERR:
		case RLST:
		case WLST:
		case LCLR:
			Text.Printf( "%s",
				pSpace->m_Prefix
				);

			break;

		case LPP:
		case LPV:
		case LPA:
		case LPJ:
			Text.Printf( "%s%d",
				pSpace->m_Prefix,
				Addr.a.m_Offset
				);

			break;

		default:
			Text.Printf( "%s%d(%d)",
				pSpace->m_Prefix,
				Addr.a.m_Offset,
				Addr.a.m_Extra
				);

			break;
		}

	return TRUE;
	}

// Implementation

void CEcodriveDriver::AddSpaces(void)
{
	AddSpace(New CSpace(PCUR,"PCUR","P-x-Current Value",			10,  0, 4095,	addrRealAsReal));

	AddSpace(New CSpace(SCUR,"SCUR","S-x-Current Value",			10,  0, 4095,	addrRealAsReal));

	AddSpace(New CSpace(LPP, "POSL","Position List Item (P-4006)",		10,  1,	64,	addrRealAsReal));

	AddSpace(New CSpace(LPV, "VELL","Velocity List Item (P-4007)",		10,  1,	64,	addrRealAsReal));

	AddSpace(New CSpace(LPA, "ACCL","Acceleration List Item (P-4008)",	10,  1,	64,	addrRealAsReal));

	AddSpace(New CSpace(LPJ, "JRKL","Jerk List Item (P-4009)",		10,  1,	64,	addrRealAsReal));

	AddSpace(New CSpace(RLST,"RLST","Get List  < Bits 3-2-1-0 = J-A-V-P >",	10,  0, 0,	addrByteAsByte));

	AddSpace(New CSpace(WLST,"WLST","Send List < Bits 3-2-1-0 = J-A-V-P >",	10,  0, 0,	addrByteAsByte));

	AddSpace(New CSpace(LCLR,"LCLR","Clear lists and set new size",		10,  0, 0,	addrByteAsByte));

	AddSpace(New CSpace(PMIN,"PMIN","P-x-Minimum Value (Read Only)",	10,  0,	4095,	addrRealAsReal));

	AddSpace(New CSpace(PMAX,"PMAX","P-x-Maximum Value (Read Only)",	10,  0,	4095,	addrRealAsReal));

	AddSpace(New CSpace(SMIN,"SMIN","S-x-Minimum Value (Read Only)",	10,  0,	4095,	addrRealAsReal));

	AddSpace(New CSpace(SMAX,"SMAX","S-x-Maximum Value (Read Only)",	10,  0,	4095,	addrRealAsReal));

	AddSpace(New CSpace(SERR,"ERR","Latest Error",				10,  0, 0,	addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Ecodrive Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CEcodriveAddrDialog, CStdAddrDialog);
		
// Constructor

CEcodriveAddrDialog::CEcodriveAddrDialog(CEcodriveDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "EcodriveElementDlg";
	}

// Overridables


void CEcodriveAddrDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	UINT uTable = SERR;

	CString L = "";
	CString R = "";

	if( m_pSpace ) {

		uTable = m_pSpace->m_uTable;

		if( fEnable ) {

			if( uTable < LPP ) {

				UINT uFind1 = Text.Find('(');

				UINT uFind2 = Text.Find(')');

				if( uFind1 < NOTHING ) {

					L = Text.Left(uFind1);

					if( uFind2 < NOTHING && uFind2 > uFind1 + 1 ) {

						R = Text.Mid( uFind1+1, uFind2 - uFind1 - 1 );
						}
					}
				}

			else {
				if( uTable < SERR ) {

					L = Text;
					}
				}

			ShowDetails();
			}
		}

	GetDlgItem(2002).SetWindowText(L);

	GetDlgItem(2005).SetWindowText(R);

	if( !m_pSpace || uTable >= SERR ) {

		fEnable = FALSE;
		}

	BOOL fEW = fEnable && (uTable < LPP);

	GetDlgItem(2002).EnableWindow(fEnable);

	GetDlgItem(2004).EnableWindow(fEW);

	GetDlgItem(2005).EnableWindow(fEW);

	GetDlgItem(2006).EnableWindow(fEW);
	}

CString CEcodriveAddrDialog::GetAddressText(void)
{
	if( m_pSpace && (m_pSpace->m_uTable < LPP) ) {

		CString Text;

		Text = GetDlgItem(2002).GetWindowText();

		Text += "(";
		
		Text += GetDlgItem(2005).GetWindowText();

		Text += ")";

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

void CEcodriveAddrDialog::ShowDetails(void)
{
	if( !m_pSpace ) return;

	CString  Min;

	CString  Max;

	if( m_pSpace->m_uTable >= LPP ) {

		CStdAddrDialog::ShowDetails();

		return;
		}

	UINT uTable = m_pSpace->m_uTable;

	UINT uExtra   = 0;

	CAddress Addr;

	Addr.a.m_Table = uTable;

	Addr.a.m_Extra = uExtra;

	Addr.a.m_Offset = 0;

	Addr.a.m_Type = addrRealAsReal;

	m_pDriver->DoExpandAddress(Min, m_pConfig, Addr);

	Addr.a.m_Offset = 4095;

	Addr.a.m_Extra  = 7;

	m_pDriver->DoExpandAddress(Max, m_pConfig, Addr);

	GetDlgItem(3004).SetWindowText(Min);

	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText("Decimal");
	}

// Helpers

// End of File
