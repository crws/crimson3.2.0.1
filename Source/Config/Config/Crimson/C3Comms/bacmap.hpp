
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BACMAP_HPP
	
#define	INCLUDE_BACMAP_HPP

//////////////////////////////////////////////////////////////////////////
//
// BACNet Constant Mappings
//

class CBACNetMapping

{
	public:
		// Constructor
		CBACNetMapping(void);

		// Enumeration
		BYTE EnumObjectTypes(UINT uIndex);
		WORD EnumObjectProps(BYTE ObjectType, UINT uIndex);
		BYTE GetPropType(BYTE ObjectType, BYTE PropID);

		// Lookups
		CString GetTypeName(BYTE TypeID);
		CString GetObjectName(BYTE ObjectType);
		CString GetObjectAbbr(BYTE ObjectType);
		CString GetPropName(BYTE PropID);
		BYTE    GetTypeID(CString Name);
		BYTE    GetObjectType(CString Abbr);
		BYTE	GetPropID(CString Name);

	protected:
		// Raw Schema Row
		struct CSchemaRow
		{
			char m_Prop[40];
			char m_Type[40];
			};

		// Raw Schema Data
		struct CSchemaEntry
		{
			BYTE       m_Obj;
			CSchemaRow m_Row[40];
			};

		// Raw Type Data
		struct CTypeEntry
		{
			BYTE m_ID;
			char m_Name[40];
			};

		// Raw Alias Data
		struct CAliasEntry
		{
			char m_Name[40];
			char m_Type[40];
			};

		// Raw Object Data
		struct CObjectEntry
		{
			char    m_Abbr[8];
			char    m_Name[40];
			BYTE    m_ID;
			};

		// Raw Property Data
		struct CPropEntry
		{
			char m_Name[40];
			BYTE m_ID;
			};

		// Raw Mapping Data
		static CSchemaEntry m_SchemaList[];
		static CTypeEntry   m_TypeList[];
		static CAliasEntry  m_AliasList[];
		static CObjectEntry m_ObjectList[];
		static CPropEntry   m_PropList[];

		// Constructed Maps
		CMap <BYTE, CString> m_TypeFwd;
		CMap <CString, BYTE> m_TypeRev;
		CMap <BYTE, CString> m_ObjectFwdAbbr;
		CMap <BYTE, CString> m_ObjectFwdName;
		CMap <CString, BYTE> m_ObjectRevAbbr;
		CMap <CString, BYTE> m_ObjectRevName;
		CMap <BYTE, CString> m_PropFwd;
		CMap <CString, BYTE> m_PropRev;

		// Implementation
		void ScanTypes(void);
		void ScanAliases(void);
		void ScanObjects(void);
		void ScanProps(void);

		// Validation
		void ValidatePropTypes(void);
	};

// End of File

#endif
