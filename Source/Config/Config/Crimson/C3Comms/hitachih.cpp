
#include "intern.hpp"

#include "hitachih.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hitachi H Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CHitachiHDeviceOptions, CUIItem);

// Constructor

CHitachiHDeviceOptions::CHitachiHDeviceOptions(void)
{
	m_Loop		= 0xFF;

	m_Unit		= 0xFF;

	m_fCheck	= 1;

	m_fMulti	= 0;

	m_Drop		= 0;
	}

// UI Management

void CHitachiHDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Multi" ) {

		pWnd->EnableUI("Drop", m_fMulti);
		}
 	}

// Download Support

BOOL CHitachiHDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Loop));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_fCheck));
	Init.AddByte(BYTE(m_fMulti));
	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CHitachiHDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Loop);
	Meta_AddInteger(Unit);
	Meta_AddBoolean(Check);
	Meta_AddBoolean(Multi);
	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Hitachi H Driver
//

// Instantiator

ICommsDriver *	Create_HitachiHDriver(void)
{
	return New CHitachiHDriver;
	}

// Constructor

CHitachiHDriver::CHitachiHDriver(void)
{
	m_wID		= 0x4001;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Hitachi";
	
	m_DriverName	= "Hitachi H Series";
	
	m_Version	= "1.0";
	
	m_ShortName	= "Hitachi Master";

	AddSpaces();
	}

// Binding Control

UINT CHitachiHDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CHitachiHDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CHitachiHDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CHitachiHDeviceOptions);
	}

// Address Helpers

BOOL CHitachiHDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) {

		return FALSE;
		}

	UINT uLeft    = 0;
	UINT uRight   = 0;

	if( (uLeft = Text.Find('.')) < NOTHING ) {

		Text   = Text.Left(uLeft);
		}

	UINT uLen    = Text.GetLength();
	UINT uX      = pSpace->m_uMaximum;
	UINT uO      = xtoin(Text);

	CString sErr = "";
	CString sPre = pSpace->m_Prefix;

	if( sPre == "WX" || sPre == "WY" ) {

		Text.Printf( "%4.4X", uO );

		uLeft  = xtoin(Text.Left(uLen - 1));
		uRight = xtoin(Text.Right(1));

		if( uRight < 0 || uRight > 9 || uLeft > (uX >> 4) ) {

			sErr = pSpace->m_Caption;
			}
		}

	else {
		if( sPre[0] == 'X' || sPre[0] == 'Y' ) {

			Text.Printf( "%5.5X", uO );

			uLeft  = xtoin(Text.Left(uLen - 2));
			uRight = xtoin(Text.Right(2));

			if( uRight < 0 || uRight > 0x5F || uLeft > (uX >> 8) ) {

				sErr = pSpace->m_Caption;
				}
			}

		else {
			if( uO > uX ) {

				sErr.Printf( "%s0 - %s%X",

					sPre,
					sPre,
					uX
					);
				}

			else {
				return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
				}
			}
		}

	if( sErr[0] ) {

		Error.Set( sErr, 0 );

		return FALSE;
		}

	Addr.a.m_Offset = uO;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Table  = pSpace->m_uTable;

	Addr.a.m_Type   = pSpace->m_uType;

	return TRUE;
	}

BOOL CHitachiHDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
		}
	
	return FALSE;
	}

// Implementation

void CHitachiHDriver::AddSpaces(void)
{
	AddSpace(New CSpace( 1,	"WL0",	"Link Words      00000-0FFFF", 	 16, 0,	0xFFFF,	WAW));
	AddSpace(New CSpace( 2,	"WL1",	"Link Words      10000-1FFFF",	 16, 0,	0xFFFF,	WAW));
	AddSpace(New CSpace( 3,	"WX",	"Input Words    000.0-4FF.9",	 16, 0,	0x4FF9,	WAW));
	AddSpace(New CSpace( 4,	"WY",	"Output Words 000.0-4FF.9",	 16, 0,	0x4FF9,	WAW));
	AddSpace(New CSpace( 5,	"WR",	"R Words - 16 Bits",		 16, 0,	0xFFFF,	WAW));
	AddSpace(New CSpace(42,	"DR",	"R Words - 32 Bits",		 16, 0,	0xFFFE,	LAL));
	AddSpace(New CSpace( 6,	"WM",	"M Words",			 16, 0,	0xFFFF,	WAW));
	AddSpace(New CSpace( 7,	"TCP",	"Timer/Counter Preset",		 16, 0,	0xFFF,	WAW));
	AddSpace(New CSpace( 8,	"TCA",	"Timer/Counter Current",	 16, 0,	0xFFF,	WAW));
	AddSpace(New CSpace( 9,	"L0",	"Link Bits 0      00000-0FFFF", 16, 0,	0xFFFF,	BAB));
	AddSpace(New CSpace(10,	"L1",	"Link Bits 1      10000-1FFFF", 16, 0,	0xFFFF,	BAB));
	AddSpace(New CSpace(11,	"L2",	"Link Bits 2      20000-2FFFF", 16, 0,	0xFFFF,	BAB));
	AddSpace(New CSpace(12,	"L3",	"Link Bits 3      30000-3FFFF", 16, 0,	0xFFFF,	BAB));
	AddSpace(New CSpace(13,	"X0",	"Input Bits    0 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(14,	"X1",	"Input Bits    1 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(15,	"X2",	"Input Bits    2 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(16,	"X3",	"Input Bits    3 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(17,	"X4",	"Input Bits    4 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(18,	"Y0",	"Output Bits 0 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(19,	"Y1",	"Output Bits 1 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(20,	"Y2",	"Output Bits 2 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(21,	"Y3",	"Output Bits 3 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(22,	"Y4",	"Output Bits 4 00.00-FF.5F",	 16, 0,	0xFF5F,	 BAB));
	AddSpace(New CSpace(23,	"M",	"M Bits",			 16, 0,	0xFFFF,	 BAB));
	AddSpace(New CSpace(24,	"R0",	"R Bits 0xxxx",			 16, 0,	0xFFFF,  BAB));
	AddSpace(New CSpace(25,	"R1",	"R Bits 1xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(26,	"R2",	"R Bits 2xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(27,	"R3",	"R Bits 3xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(28,	"R4",	"R Bits 4xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(29,	"R5",	"R Bits 5xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(30,	"R6",	"R Bits 6xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(31,	"R7",	"R Bits 7xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(32,	"R8",	"R Bits 8xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(33,	"R9",	"R Bits 9xxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(34,	"RA",	"R Bits Axxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(35,	"RB",	"R Bits Bxxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(36,	"RC",	"R Bits Cxxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(37,	"RD",	"R Bits Dxxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(38,	"RE",	"R Bits Exxxx",			 16, 0,	0xFFFF,  BAB));
//	AddSpace(New CSpace(39,	"RF",	"R Bits Fxxxx",			 16, 0,	0xFFFF,  BAB));
	AddSpace(New CSpace(40,	"CL",	"TC Clear",			 16, 0,	0xFFFF,	 BAB));
	AddSpace(New CSpace(41,	"ERR",	"Response Error",		 16, 0,	     0,	 LAL));
	AddSpace(New CSpace(43,	"ADJ",	"TM Comms Delay (0 - 150)",	 16, 0,	     0,	 LAL));
	}

UINT CHitachiHDriver::xtoin(CString Offset)
{
	UINT uLen = Offset.GetLength();

	UINT u    = 0;
	
	UINT v    = 0;

	Offset.MakeUpper();

	for( UINT i = 0; i < uLen; i++ ) {

		TCHAR c = Offset[i];

		v       = 0x10;

		if( c >= '0' && c <= '9' ) {
			
			v = c - '0';
			}
		else {
			if( c >= 'A' && c <= 'F' ) {
				
				v = c - '7';
				}
			}

		if( v < 0x10 ) {

			u *= 16;

			u += v;
			}
		}

	return u;
	}

// End of File
