
#include "intern.hpp"

#include "spi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SPI Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSpiMasterDeviceOptions, CUIItem);

// Constructor

CSpiMasterDeviceOptions::CSpiMasterDeviceOptions(void)
{
	m_DevID  = 0x20;

	m_Addr   = 0x20;

	m_BU[0]  = m_DevID;

	m_BU[1]  = m_Addr;
	}

// UI Managament

void CSpiMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag == "DevID" || Tag == "Addr" ) {

		if( pItem->GetDataAccess(Tag)->ReadInteger(pItem) < 0x20 ) {

			m_DevID = m_BU[0];

			m_Addr  = m_BU[1];

			pWnd->UpdateUI();

			return;
			}

		m_BU[0] = m_DevID;

		m_BU[1] = m_Addr;
		}
	}


// Download Support

BOOL CSpiMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_DevID));

	Init.AddByte(BYTE(m_Addr));
	
	return TRUE;
	}

// Meta Data Creation

void CSpiMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(DevID);
			     
	Meta_AddInteger(Addr);
	}


//////////////////////////////////////////////////////////////////////////
//
// SPI Master Driver
//

// Instantiator

ICommsDriver *	Create_SpiMasterDriver(void)
{
	return New CSpiMasterDriver;
	}

// Constructor

CSpiMasterDriver::CSpiMasterDriver(void)
{
	m_wID		= 0x4091;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SPI";
	
	m_DriverName	= "Tributary Station";
	
	m_Version	= "1.00";
	
	m_ShortName	= "SPI";

	AddSpaces();
	}

// Binding Control

UINT	CSpiMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSpiMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
 	}

// Configuration

CLASS CSpiMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSpiMasterDeviceOptions);
	}

// Address Management

BOOL CSpiMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CSpiMasterDialog Dlg(ThisObject, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Implementation	

void CSpiMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "SPIL", "SPI Command (LONG)",	16, 0x2020, 0xFFFF, addrLongAsLong, addrLongAsReal));

	AddSpace(New CSpace(2, "SPIW", "SPI Command (WORD)",	16, 0x2020, 0xFFFF, addrWordAsWord));

	AddSpace(New CSpace(3, "SPIB", "SPI Command (BYTE)",	16, 0x2020, 0xFFFF, addrByteAsByte));

	
	}

//////////////////////////////////////////////////////////////////////////
//
// SPI Master Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CSpiMasterDialog, CStdAddrDialog);
		
// Constructor

CSpiMasterDialog::CSpiMasterDialog(CSpiMasterDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "SpiElementDlg";
	}

void CSpiMasterDialog::SetAddressFocus(void)
{
	SetDlgFocus(2004);
	}

void CSpiMasterDialog::SetAddressText(CString Text)
{
	if( !m_pSpace ) {

		GetDlgItem(2002).SetWindowText("<cmd1>");

		GetDlgItem(2002).EnableWindow(FALSE);

		GetDlgItem(2004).SetWindowText("<cmd2>");

		GetDlgItem(2004).EnableWindow(FALSE);

		return;
		}
	
	GetDlgItem(2002).SetWindowText(Text.Mid(Text.GetLength() - 4, 2));

	GetDlgItem(2002).EnableWindow(!Text.IsEmpty());

	GetDlgItem(2004).SetWindowText(Text.Mid(Text.GetLength() - 2, 2));

	GetDlgItem(2004).EnableWindow(!Text.IsEmpty());
       	}

CString CSpiMasterDialog::GetAddressText(void)
{
	return GetDlgItem(2002).GetWindowText() + GetDlgItem(2004).GetWindowText();
	}


// End of File