
#include "intern.hpp"

#include "imok7.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX Series
//

// Instantiator

ICommsDriver *	Create_IMOK7Driver(void)
{
	return New CIMOK7Driver;
	}

// Constructor

CIMOK7Driver::CIMOK7Driver(void)
{
	m_wID		= 0x339C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "IMO";
	
	m_DriverName	= "PLC-CNET";
	
	m_Version	= "1.00";
	
	m_ShortName	= "IMO CNET";

	AddSpaces();
	}

// Binding Control

UINT	CIMOK7Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CIMOK7Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CIMOK7Driver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"PW",	"Input/Output Relay",		16,  0,	0xFFF,	addrWordAsWord));
	AddSpace(New CSpace(2,	"MW",	"Auxiliary Relay",		16,  0,	0xFFF,	addrWordAsWord));
	AddSpace(New CSpace(3,	"LW",	"Link Relay",			16,  0,	0xFFF,	addrWordAsWord));
	AddSpace(New CSpace(4,	"KW",	"Keep Relay",			16,  0,	0xFFF,	addrWordAsWord));
	AddSpace(New CSpace(5,	"CW",	"Counter",			10,  0,	255,	addrWordAsWord));
	AddSpace(New CSpace(6,	"TW",	"Timer",			10,  0,	255,	addrWordAsWord));
	AddSpace(New CSpace(7,	"DW",	"Data Register",		16,  0,	0xFFF,	addrWordAsWord));
	AddSpace(New CSpace(8,	"SW",	"Step Relay",			16,  0,	0xFFF,	addrWordAsWord));
	AddSpace(New CSpace(9,	"PX",	"Input/Output Bits",		16,  0,	0xFFF,	addrBitAsBit));
	AddSpace(New CSpace(10,	"MX",	"Auxiliary Relay Bits",		16,  0,	0xFFF,	addrBitAsBit));
	AddSpace(New CSpace(11,	"LX",	"Link Relay Bits",		16,  0,	0xFFF,	addrBitAsBit));
	AddSpace(New CSpace(12,	"KX",	"Keep Relay Bits",		16,  0,	0xFFF,	addrBitAsBit));
//	AddSpace(New CSpace(13,	"CX",	"Counter Bits",			10,  0,	255,	addrBitAsBit));
//	AddSpace(New CSpace(14,	"TX",	"Timer Bits",			10,  0,	255,	addrBitAsBit));
	AddSpace(New CSpace(15,	"FW",	"Status (Read Only)",		16,  0,	0xFFF,	addrWordAsWord));
	AddSpace(New CSpace(16,	"FX",	"Status Bits (Read Only)",	16,  0, 0xFFF,	addrBitAsBit));
//	AddSpace(New CSpace(17,	"ER",	"Comms Error",			16,  0, 0,	addrLongAsLong));
	}

// End of File
