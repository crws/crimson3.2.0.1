
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMPACTD_HPP
	
#define	INCLUDE_IMPACTD_HPP

enum {
	listNone	= 0,
	listObjects	= 1,
	listObjProps	= 2,
	listProgs	= 3,
	listTasks	= 4,
	listTools	= 5,
	listProps	= 6,
	listUser	= 7,

	// Insert new configurable lists here !!!

	listTypes	= 21,
	listMenu	= 22,
	listManage	= 23,
	
	// Insert new static lists here !!!

	listEnd		= 31,
	};

enum {
	menuNone	= 0,
	menuData	= 1,
	menuRun		= 2,
	menuTrigger	= 3,
	menuOnline	= 4,
	menuOffline	= 5,
	menuIsOnline    = 6,
	menuAbort	= 7,
	menuUser	= 8,
	menuGenErr	= 9,
	menuSynErr	= 10,
	menuDataErr	= 11,
	
	};

enum {
	tableBit	= 0,
	tableInt	= 1,
	tableReal	= 2,
	tableCmd	= 3,//0x11,
	tableStr	= 4,//0x20,
	tableGenErr     = 5,//0xED
	tableSynErr	= 6,//0xEE,
	tableDataErr    = 7,//0xEF,
	};

enum {
	tObject   = 0,
	tTask     = 1,
	tRun	  = 2,
	tTrig     = 3,
	tUser	  = 4,
	tGen	  = 5,
	tSyn      = 6,
	tData     = 7,
	tOnline   = 8,
	tOffline  = 9,
	tIsOnline = 10,
	tAbort	  = 11,
	};

#define TYPES	 8
#define SYM_W	 10
#define SYM_S	 6

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String Address Meta Item
//

class CImpactStrAddr : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CImpactStrAddr(void);
		CImpactStrAddr(CString Str, CAddress Addr, BYTE bType);
		
		// Destructor
		~CImpactStrAddr(void);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		//void Init(void);
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		
		// Data Access
		CString	   GetString(void);
		CString	   GetFullString(void);
		CAddress   GetAddress(void);
		BOOL	   Set(CString Str, CAddress Addr, BYTE bType); 
						
	protected:
		// Data Members
		CString    m_String;
		CAddress   m_Addr;
		CString    m_Send;	
		CString	   m_Sym;	
		BYTE	   m_Type;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void SetSendString(CString Str);
		void SetUserSendString(CString Str);
		void SetSymString(CString Str);
		void SetUserSymString(CString Str);

		// Helper
		void SendReplace(char cChar, CString Text);
		void SendReplaceLast(char cReplace, char cWith);
		BOOL IsType(CString Str);
	};

/////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String Address Storage Class 
//

class CImpactStrAddrList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImpactStrAddrList(void);

		// Destructor
		~CImpactStrAddrList(void);

		// Item Access
		CImpactStrAddr * GetItem(INDEX Index) const;
		CImpactStrAddr * GetItem(UINT  uPos ) const;

		// Operations
		CImpactStrAddr * AppendItem(CImpactStrAddr * pString);

	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String Meta Item
//

class CImpactStr : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CImpactStr(void);
		
		// Destructor
		~CImpactStr(void);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		
		// Data Access
		CStringArray * GetStrings(void);

		// Helpers
		BOOL DoesExist(CString Str);
						
	protected:
		// Data Members
		CStringArray * m_pStrings;

		// Meta Data Creation
		void AddMetaData(void);
	};

/////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String Storage Class 
//

class CImpactList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImpactList(void);

		// Destructor
		~CImpactList(void);

		// Item Access
		CImpactStr * GetItem(INDEX Index) const;
		CImpactStr * GetItem(UINT  uPos ) const;

		// Operations
		CImpactStr * AppendItem(CImpactStr * pString);

	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Driver Options
//

class CImpactDataDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImpactDataDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Device Options
//

class CImpactDataDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImpactDataDeviceOptions(void);

		// Destructor
		~CImpactDataDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// Persistance
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Vision System List Access
		CString GetListElement(UINT uList, UINT uElement);
		UINT	GetListCount(UINT uList);
		BOOL	AddListItem(UINT uList, CString Item);
		BOOL	DeleteListItem(UINT uList, CString Item);
		BOOL    ResetList(UINT uList);

		// Address List Access
		CString GetAddrText(DWORD dwRef);
		CString GetAddrFullText(DWORD dwRef);

		// Address Reference Update
		BOOL SetAddr(CString Text, CAddress &Addr);
		
		// Helpers
		BOOL IsNamed(CAddress Addr);
		BOOL IsString(CAddress Addr);
		BOOL IsType(CString Text);


		// Config Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:

		// Vision System Lists
		CImpactList * m_pImpactList;

		// Address List
		CImpactStrAddrList * m_pAddrList;

		UINT m_uOffset[TYPES];
		UINT m_uTable [TYPES];
		
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void		InitLists(void);
		void		InitList(UINT uList);
		void		EmptyLists(void);
		void		InitObjectList(void);
		void		InitObjPropList(void);
		void		InitProgList(void);
		void		InitTaskList(void);
		void		InitToolList(void);
		void		InitPropList(void);
		void		InitTypeList(void);
		void		InitMenuList(void);
		void		InitManageList(void);
		void		InitUserList(void);
		BOOL		Import(FILE *pFile);
		void		OnImport(CWnd *pWnd);
		void		OnExport(CWnd *pWnd);
		void		Export(FILE *pFile);
		CString		GetDeviceName(void);
		BOOL		CanAdd(UINT uList, CString Item);
		BOOL		CanDelete(UINT uList, CString Item, UINT& u);
		BOOL		CanReset(UINT uList);
		BOOL		CanEdit(UINT uList);
		BOOL		DoesExist(UINT uList, CString Item, UINT& u);
		BOOL		SetExist(CString Item, CAddress &Addr);
		BOOL		ListExist(CString Item);
		BOOL		SetNamed(CString Text, CAddress &Addr, UINT uTable);
		BOOL		SetLatestError(CString Text, CAddress &Addr);
		BOOL		SetString(CString Text, CAddress &Addr);
		BYTE		FindType(CString Text);
		BOOL		IsUsed(DWORD dwRef);
		DWORD		GetNextNamed(UINT uTable);
		DWORD		GetNextString(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Device Options Custom PGN Page
//

class CImpactDataDeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CImpactDataDeviceOptionsUIPage(CImpactDataDeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CImpactDataDeviceOptions * m_pOption;
	}; 

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Driver
//

class CImpactDataDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CImpactDataDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Binding Control
		UINT GetBinding(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Address Selection Dialog
//

class CImpactDataDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CImpactDataDialog(CImpactDataDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Vision System List Access
		CString GetListElement(UINT uList, UINT uElement);
		UINT	GetListCount(UINT uList);

		// Command Access
		void SetCommand(CString Cmd);
		void SetType(CString Type);

		// Selection Access
		CString GetSelectedText(void);

	protected:

		// Data
		CImpactDataDriver *	m_pDriver;
		CAddress	*	m_pAddr;
		CItem		*	m_pConfig;
		BOOL			m_fPart;
		CString			m_Text;

	
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
	       
		// Command Handlers
		BOOL OnSelectProperty(UINT uID);
		BOOL OnSelectType(UINT uID);
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadList(UINT uSel);
		void DoEnables(void);
		void SetSelectText(CString Text);

		// Helpers
		BOOL IsData(CString Text);
		BOOL IsTrig(CString Text);
		BOOL IsSetOnline(CString Text);
		BOOL IsSetOffline(CString Text);
		BOOL IsIsOnline(CString Text);
		BOOL IsAbort(CString Text);
		BOOL IsUser(CString Text);
		BOOL IsGenErr(CString Text);
		BOOL IsSynErr(CString Text);
		BOOL IsDataErr(CString Text);
		BOOL IsNone(CString Text);
		BOOL IsType(CString Text);


	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Property Selection Dialog
//

class CImpactPropDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CImpactPropDialog(CImpactDataDialog * Dlg, BOOL fRun, CString Prop);
		                
	protected:

		// Data
		CImpactDataDialog *	m_pDlg;
		BOOL			m_fRun;
		CString			m_Prop;
					
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
	       
		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadLists(void);
		void ReloadLists(void);
		void SetSelection(void);
		void DoEnables(void);
		void LoadList(UINT uID);
		UINT FindList(UINT uID); 
		BOOL IsComplete(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Type Selection Dialog
//

class CImpactTypeDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CImpactTypeDialog(CImpactDataDialog * Dlg);
		                
	protected:

		// Data
		CImpactDataDialog *	m_pDlg;
					
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
	       
		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadList(void);
		void SetSelection(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact User Defined Data Selection Dialog
//

class CImpactUserDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CImpactUserDialog(CImpactDataDialog * Dlg);
		                
	protected:

		// Data
		CImpactDataDialog *	m_pDlg;
					
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
	       
		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadList(void);
		void SetSelection(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact List Management Dialog
//

class CImpactManageDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CImpactManageDialog(CImpactDataDeviceOptions * Device);
		                
	protected:

		// Data
		CImpactDataDeviceOptions *	m_pDevice;
					
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
	       
		// Command Handlers
		BOOL OnAdd(UINT uID);
		BOOL OnDelete(UINT uID);
		BOOL OnDefault(UINT uID);
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadLeftList(void);
		void LoadRightList(UINT uSel);
		
	};

// End of File

#endif
