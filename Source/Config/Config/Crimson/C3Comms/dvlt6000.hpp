
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DVLT6000_HPP
	
#define	INCLUDE_DVLT6000_HPP

//////////////////////////////////////////////////////////////////////////
//
// Danfoss 6000 VLT Device Options
//

class CDVLT6000DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDVLT6000DeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Ping;
				

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Danfoss VLT 6000 Serial Master Driver
//

class CDVLT6000Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CDVLT6000Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration	
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);


		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
					
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Danfoss VLT 6000  Series Space Wrapper
//

class CSpaceDanVLT : public CSpace
{
	public:
		// Constructors
		CSpaceDanVLT(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Danfoss Address Selection Dialog
//

class CDanfossAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDanfossAddrDialog(CDVLT6000Driver &Driver, CAddress &Addr, BOOL fPart);

	protected:
	
		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};




// End of File

#endif
