
#include "intern.hpp"

#include "pcom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unitronics Column Item
//

// Dynamic Class

AfxImplementDynamicClass(CColumn, CUIItem);

// Constructor

CColumn::CColumn(void)
{
	m_Type   = 2;

	m_Label  = "Column";

	m_Bytes  = 2;

	AddMeta();
   	}

// UI Managament

void CColumn::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Type" ) {

		UINT uType = pItem->GetDataAccess("Type")->ReadInteger(pItem);

		pWnd->EnableUI("Bytes", uType == 5);

		SetBytes(uType);

		pWnd->UpdateUI("Bytes");
		}
	}

// Persistance

void CColumn::Save(CTreeFile &Tree)
{
	Tree.PutValue(L"Type",  m_Type);

	Tree.PutValue(L"Label", m_Label);

	Tree.PutValue(L"Bytes", m_Bytes);
	}


// Download Support

BOOL CColumn::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_Type));

	Init.AddWord(WORD(m_Bytes));

	return TRUE;
	}

// Meta Data Creation

void CColumn::AddMetaData(void)	
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Type);

	Meta_AddString(Label);

	Meta_AddInteger(Bytes);

	Meta_SetName(IDS_PCOM_COL);
	}


// Data Access


UINT CColumn::GetType(void)
{
	return m_Type;
	}

UINT CColumn::GetTypeAs(void)
{
	switch( m_Type ) {

		case 0:
			return addrBitAsBit;
		case 1:
			return addrByteAsByte;
		case 2:
			return addrWordAsWord;
		case 3:
			return addrLongAsLong;
		case 4:
			return addrRealAsReal;
		case 5:
			return addrByteAsByte;		
		}
	
	return 0;
	}


UINT CColumn::GetVisual(void)
{
	switch( m_Type ) {

		case 1:
		case 2:
		case 3:

			return 1;

		case 4:
			return 2;

		case 5:
			return 3;
		}

	return m_Type;
	}

void CColumn::SetType(UINT uType)
{	
	m_Type = uType;
	}

CString CColumn::GetLabel(void)
{
	return m_Label;
	}

void CColumn::SetLabel(CString Label)
{
	m_Label = Label;
	}

void CColumn::SetByte(UINT uBytes)
{
	m_Bytes = uBytes;
	}

void CColumn::SetBytes(UINT uType)
{
	switch( uType ) {

		case 0:
		case 1:
			m_Bytes = 1;	break;
		case 2:
			m_Bytes = 2;	break;
		case 3:
		case 4:
			m_Bytes = 4;	break;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitronics Column Item List
//


// Dynamic Class

AfxImplementDynamicClass(CColumnList, CItemList);

// Constructor

CColumnList::CColumnList(void)
{
		
	}

// Item Access

CColumn * CColumnList::GetItem(INDEX Index) const
{
	return (CColumn *) CItemList::GetItem(Index);
	}

CColumn * CColumnList::GetItem(UINT uPos) const
{
	return (CColumn *) CItemList::GetItem(uPos);
	}

// Operations

CColumn * CColumnList::AppendItem(CColumn * pColumn)
{
	pColumn->SetParent(this);

	if( CItemList::AppendItem(pColumn) ) {

		return pColumn;
		}

	return NULL;
	}

INDEX CColumnList::InsertItem(CColumn * pColumn, CColumn * pBefore)
{
	pColumn->SetParent(this);
	
	return CItemList::InsertItem(pColumn, pBefore);
	}


//////////////////////////////////////////////////////////////////////////
//
// Unitronics DataTable Item
//

// Dynamic Class

AfxImplementDynamicClass(CDataTable, CUIItem);

// Constructor

CDataTable::CDataTable(void)
{
	m_pColumns = new CColumnList;

	m_Rows  = 10;

	m_Cols  = 10;

	m_Label = "Table";

	AddMeta();
	}				  

// UI Managament

void CDataTable::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Cols" ) {

		if( m_Cols < m_pColumns->GetItemCount() ) {

			while( m_Cols < m_pColumns->GetItemCount() ) {

				CColumn * pColumn = NULL;

				for( INDEX i = m_pColumns->GetHead(); !m_pColumns->Failed(i); m_pColumns->GetNext(i) ) {

					pColumn = m_pColumns->GetItem(i);
					}
	
				m_pColumns->RemoveItem(pColumn);
				}
			}
		
		if( m_Cols > m_pColumns->GetItemCount() ) {

			CString Suffix;

			CString Format = " %u";

			while( m_Cols > m_pColumns->GetItemCount() ) {

				CColumn * pCol = new CColumn;

				m_pColumns->AppendItem(pCol);

				UINT uPos = m_pColumns->GetItemCount() - 1;

				pCol = m_pColumns->GetItem(uPos);
	
				Suffix.Printf(Format, uPos);

				pCol->SetLabel(pCol->GetLabel() + Suffix);

				pCol->SetDatabase(GetDatabase());
				}
			}   
		}
	}


// Data Access

CColumn * CDataTable::GetColumn(void)
{
	INDEX i = m_pColumns->GetHead(); 

	UINT  u = 0;
	
	while( !m_pColumns->Failed(i) ) {

		u++;

		m_pColumns->GetNext(i);
		}

	CColumn * pColumn = GetColumn(u - 1);

	if( u < 32 ) {

		return pColumn;
		}

	return NULL;
	}

CColumn	* CDataTable::GetColumn(UINT uPos)
{
	return m_pColumns->GetItem(uPos);
	}

void CDataTable::DeleteColumn(CItem * pItem)
{
	m_pColumns->RemoveItem(pItem);
	}

CColumn * CDataTable::FindColumn(CString Label)
{
	for( INDEX i = m_pColumns->GetHead(); !m_pColumns->Failed(i); m_pColumns->GetNext(i) ) {
	
		CColumn * pColumn = m_pColumns->GetItem(i);
		
		if( pColumn->GetLabel() == Label ) {

			return pColumn;
			}
		}

	return NULL;
	}


CString CDataTable::GetLabel(void)
{
	return m_Label;
	}

void CDataTable::SetLabel(CString Label)
{
	m_Label = Label;
	}

CColumnList * CDataTable::GetColumnList(void)
{
	return m_pColumns;
	}

// Persistance

void CDataTable::Save(CTreeFile &Tree)
{
	Tree.PutValue(L"Label", m_Label);
	
	Tree.PutValue(L"Rows", m_Rows);

	Tree.PutValue(L"Cols", m_Cols);

	Tree.PutCollect(L"Columns");

	UINT uCount = m_pColumns->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CColumn * pColumn = m_pColumns->GetItem(u);

		if( pColumn ) {

			Tree.PutObject(L"Column");

			pColumn->Save(Tree);

			Tree.EndObject();
			}
		}
		
	Tree.EndCollect();  
	}

// Download Support

BOOL CDataTable::MakeInitData(CInitData &Init)
{
	Init.AddWord(WORD(m_Rows));

	Init.AddByte(BYTE(m_Cols));
  
	if( m_pColumns ) {

		UINT uCount = m_pColumns->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {

			m_pColumns->GetItem(u)->MakeInitData(Init);
			}
		}
	else {
		Init.AddWord(0);
		}
       
	return TRUE;
	}

// Meta Data Creation

void CDataTable::AddMetaData(void)	
{
	CUIItem::AddMetaData();

	Meta_AddString(Label);

	Meta_AddInteger(Rows);

	Meta_AddInteger(Cols);

	Meta_AddCollect(Columns);
	
	Meta_SetName(IDS_PCOM_TABLE);	
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitronics Data Table List
//


// Dynamic Class

AfxImplementDynamicClass(CDataTableList, CItemList);

// Constructor

CDataTableList::CDataTableList(void)
{
	
	}

// Item Access

CDataTable * CDataTableList::GetItem(INDEX Index) const
{
	return (CDataTable *) CItemList::GetItem(Index);
	}

CDataTable * CDataTableList::GetItem(UINT uPos) const
{
	return (CDataTable *) CItemList::GetItem(uPos);
	}

// Operations

CDataTable * CDataTableList::AppendItem(CDataTable * pTable)
{
	pTable->SetParent(this);

	if( CItemList::AppendItem(pTable) ) {

		return pTable;
		}

	return NULL;
	}

INDEX CDataTableList::InsertItem(CDataTable * pTable, CDataTable * pBefore)
{
	pTable->SetParent(this);
	
	return CItemList::InsertItem(pTable, pBefore);
	}



//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CUniPCOMDeviceOptions, CUIItem);       

// Constructor

CUniPCOMDeviceOptions::CUniPCOMDeviceOptions(void)
{
	m_Unit   = 1;
	}

// Download Support

BOOL CUniPCOMDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Unit));
	
	return TRUE;
	}

// Meta Data Creation

void CUniPCOMDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Unit);
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Device Options UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CUniPCOMBinaryDeviceOptionsUIPage , CUIPage);

// Constructor

CUniPCOMBinaryDeviceOptionsUIPage::CUniPCOMBinaryDeviceOptionsUIPage(void) 
{

	}

// Operations

BOOL CUniPCOMBinaryDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Device Identification", 1);

	pView->AddUI(pItem, L"root", L"Unit");

	pView->EndGroup(TRUE);

	pView->StartGroup(L"Data Tables", 1);

	pView->AddButton(L"Manage", L"Manage");

	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CUniPCOMBinaryDeviceOptions, CUniPCOMDeviceOptions);       

// Constructor

CUniPCOMBinaryDeviceOptions::CUniPCOMBinaryDeviceOptions(void)
{
	m_pTables = New CDataTableList;
	}

// UI Loading

BOOL CUniPCOMBinaryDeviceOptions::OnLoadPages(CUIPageList * pList)
{ 
	CUniPCOMBinaryDeviceOptionsUIPage * pPage = New CUniPCOMBinaryDeviceOptionsUIPage;

	pList->Append(pPage);
	
	return TRUE;
	}

// UI Managament

void CUniPCOMBinaryDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Manage") {

			CPCOMDataTableDialog Dlg(pItem);  
	
			Dlg.Execute(*pWnd); 
			}  
		}		
	}

// Data Access

CDataTable * CUniPCOMBinaryDeviceOptions::GetTable(void)
{
	CDataTable * pTable = new CDataTable;
	
	m_pTables->AppendItem(pTable);

	INDEX i = m_pTables->GetHead();
	
	UINT  u = 0;
	
	while( !m_pTables->Failed(i) ) {

		u++;
		
		m_pTables->GetNext(i);
		}

	CString Suffix;

	CString Format = " %u";

	Suffix.Printf(Format, u);

	pTable->SetLabel(pTable->GetLabel() + Suffix);
	
	return GetTable(u - 1);
	}

CDataTable * CUniPCOMBinaryDeviceOptions::GetTable(UINT uPos)
{
	return m_pTables->GetItem(uPos);
	}

void CUniPCOMBinaryDeviceOptions::DeleteTable(CItem * pItem)
{
	m_pTables->RemoveItem(pItem);
	}

CDataTable * CUniPCOMBinaryDeviceOptions::FindTable(CString Label)
{
	for( INDEX i = m_pTables->GetHead(); !m_pTables->Failed(i); m_pTables->GetNext(i) ) {
	
		CDataTable * pTable = m_pTables->GetItem(i);
		
		if( pTable->GetLabel() == Label ) {

			return pTable;
			}
		
		}

	return NULL;
	}

CDataTableList * CUniPCOMBinaryDeviceOptions::GetTableList(void)
{
	return m_pTables;
	}

// Download Support

BOOL CUniPCOMBinaryDeviceOptions::MakeInitData(CInitData &Init)
{
	CUniPCOMDeviceOptions::MakeInitData(Init);

	if( m_pTables ) {

		UINT uCount = m_pTables->GetItemCount();

		Init.AddByte(BYTE(uCount));

		for( UINT u = 0; u < uCount; u++ ) {

			m_pTables->GetItem(u)->MakeInitData(Init);
			}
		}
	else {
		Init.AddWord(0);
		}

	
	return TRUE;
	}

// Meta Data Creation

void CUniPCOMBinaryDeviceOptions::AddMetaData(void)
{
	CUniPCOMDeviceOptions::AddMetaData();

	Meta_AddCollect(Tables);

	Meta_SetName(IDS_PCOM_DEVICE_OPTIONS);
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM ASCII Master Driver
//

// Instantiator

ICommsDriver *	Create_UniPCOMAsciiDriver(void)
{
	return New CUniPCOMAsciiDriver;
	}

// Constructor

CUniPCOMAsciiDriver::CUniPCOMAsciiDriver(void)
{
	m_wID		= 0x4062;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Unitronics";
	
	m_DriverName	= "PCOM ASCII Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "PCOM ASCII";

	AddSpaces();
	}

// Binding Control

UINT	CUniPCOMAsciiDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CUniPCOMAsciiDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
 	}

// Configuration


CLASS CUniPCOMAsciiDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUniPCOMDeviceOptions);
	}

// Implementation	

void CUniPCOMAsciiDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "A",  "Output Bit",		10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(2,  "B",  "Memory Bit",		10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(3,  "E",  "Input Bit",		10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(4,  "G",  "System Bit",		10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(5,  "M",  "Counter Scan Bit",	10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(6,  "T",  "Timer Scan Bit",		10, 0, 9999, addrBitAsBit ));
	AddSpace(New CSpace(7,  "MI", "Memory Integer",		10, 0, 9999, addrWordAsWord ));
	AddSpace(New CSpace(8,  "ML", "Memory Long",		10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(9,  "MD", "Memory Double Word",	10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(10, "MR", "Memory Floats",		10, 0, 9999, addrRealAsReal ));
	AddSpace(New CSpace(11, "SI", "System Integer",		10, 0, 9999, addrWordAsWord ));
	AddSpace(New CSpace(12, "SL", "System Long",		10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(13, "SD", "System Double Word",	10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(14, "TP", "Timer Preset",		10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(15, "TV", "Timer Value",		10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(16, "CP", "Counter Preset",		10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(17, "CV", "Counter Value",		10, 0, 9999, addrLongAsLong ));
	AddSpace(New CSpace(18, "RT", "Real Time Clock",	10, 18,  18, addrLongAsLong ));
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Master Driver
//

// Instantiator

ICommsDriver *	Create_UniPCOMBinaryDriver(void)
{
	return New CUniPCOMBinaryDriver;
	}

// Constructor

CUniPCOMBinaryDriver::CUniPCOMBinaryDriver(void)
{
	m_wID		= 0x4065;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Unitronics";
	
	m_DriverName	= "PCOM Binary Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "PCOM Binary";
	}

// Configuration

CLASS CUniPCOMBinaryDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUniPCOMBinaryDeviceOptions);
	}

// Address Management

BOOL CUniPCOMBinaryDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	DeleteAllSpaces();

	AddSpaces(pConfig);

	CUniPCOMBinaryDialog Dlg(ThisObject, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation	

void CUniPCOMBinaryDriver::AddSpaces(CItem * pItem)
{
	CUniPCOMBinaryDeviceOptions * pOptions = (CUniPCOMBinaryDeviceOptions *)pItem;

	if( pOptions ) {

		UINT uSpaces = pOptions->GetTableList()->GetItemCount();

		CString Format = "%u";

		CString Text;

		for( UINT u = 1; u <= uSpaces; u++ ) {

			Text.Printf(Format, u);

			CDataTable * pTable = pOptions->GetTableList()->GetItem(u - 1);

			if( pTable ) {

				AddSpace(New CBinaryPCOMSpace(u, CString("T" + Text),  pTable));
				}
			}
		}
	}

// Address Helpers

BOOL CUniPCOMBinaryDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CUniPCOMBinaryDeviceOptions * pOptions = (CUniPCOMBinaryDeviceOptions *)pConfig;

	if( pOptions ) {	 

		CSpace *pSpace = GetSpace(Text);

		if( !pSpace ) {

			AddSpaces(pConfig);

			pSpace = GetSpace(Text);
			}

		if( pSpace ) {

			CString Type = StripType(pSpace, Text);

			UINT uFind = Text.Find(':');

			if( uFind < NOTHING ) {	
				
				UINT uPre = pSpace->m_Prefix.GetLength();

				CString Col = Text.Mid(uPre, uFind - uPre);

				UINT uCol = tstrtoul(Col, NULL, 10);

				CDataTable * pTable = pOptions->GetTable(pSpace->m_uTable - 1);

				if( pTable ) {

					UINT uCount = pTable->GetColumnList()->GetItemCount(); 
			
					if( uCol >= uCount ) {

						Error.Set( CPrintf("Column index must be a value from 0 to %u.", uCount - 1),
							0
							);

						return FALSE;
						}

					CString Row = Text.Mid(uFind + 1);

					UINT uRow = tstrtoul(Row, NULL, 10);

					if( uRow >= pTable->m_Rows ) {

						Error.Set( CPrintf("Row index must be a value from 0 to %u.", pTable->m_Rows - 1),
							   0
							   );
		
						return FALSE;
						} 
		
					Addr.a.m_Table   = pSpace->m_uTable;

					Addr.a.m_Type    = pTable->GetColumnList()->GetItem(uCol)->GetTypeAs();
		    
					Addr.a.m_Offset  = uRow;

					Addr.a.m_Offset |= ((uCol & 0x10)  << 11);

					Addr.a.m_Extra   = (uCol & 0xF); 

					return TRUE;
					}
				}
			}
		}

	Error.Set( "Invalid address.",
		    0
		    );


	return FALSE;
	}

BOOL CUniPCOMBinaryDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CBinaryPCOMSpace *pSpace = (CBinaryPCOMSpace *)GetSpace(Addr);

	if( pSpace ) {

		UINT uRow  = Addr.a.m_Offset & 0x7FFF;

		UINT uCol  = Addr.a.m_Extra | ((Addr.a.m_Offset & 0x8000) >> 11);

		UINT uType = Addr.a.m_Type;

		Text.Printf( "%s%2.2u:%5.5u.%s", pSpace->m_Prefix,
						 uCol,
						 uRow,
						 pSpace->GetTypeModifier(uType)
						 ); 
		return TRUE;
		} 
	
	return FALSE;
	}




//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Master TCP/IP Device Options
//

// Dynamic Class				   

AfxImplementDynamicClass(CUniPCOMTcpDeviceOptions, CUniPCOMDeviceOptions);       

// Constructor					     

CUniPCOMTcpDeviceOptions::CUniPCOMTcpDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD( 1, 200), MAKEWORD(  168, 192)));;

	m_Port   = 20256;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CUniPCOMTcpDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CUniPCOMTcpDeviceOptions::MakeInitData(CInitData &Init)
{
	CUniPCOMDeviceOptions::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CUniPCOMTcpDeviceOptions::AddMetaData(void)
{
	CUniPCOMDeviceOptions::AddMetaData();	

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}


//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM ASCII TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_UniPCOMTcpADriver(void)
{
	return New CUniPCOMTcpADriver;
	}

// Constructor

CUniPCOMTcpADriver::CUniPCOMTcpADriver(void)
{
	m_wID		= 0x4063;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Unitronics";
	
	m_DriverName	= "PCOM ASCII TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "PCOM ASCII";
	}

// Binding Control

UINT CUniPCOMTcpADriver::GetBinding(void)
{
	return bindEthernet;
	}

void CUniPCOMTcpADriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CUniPCOMTcpADriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUniPCOMTcpDeviceOptions);
	}


/////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CUniPCOMBinaryTcpDeviceOptions, CUniPCOMBinaryDeviceOptions);       

// Constructor

CUniPCOMBinaryTcpDeviceOptions::CUniPCOMBinaryTcpDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD( 1, 200), MAKEWORD(  168, 192)));;

	m_Port   = 20256;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

 // UI Managament

void CUniPCOMBinaryTcpDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}

	CUniPCOMBinaryDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CUniPCOMBinaryTcpDeviceOptions::MakeInitData(CInitData &Init)
{
	CUniPCOMBinaryDeviceOptions::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CUniPCOMBinaryTcpDeviceOptions::AddMetaData(void)
{
	CUniPCOMBinaryDeviceOptions::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_UniPCOMTcpBDriver(void)
{
	return New CUniPCOMTcpBDriver;
	}

// Constructor

CUniPCOMTcpBDriver::CUniPCOMTcpBDriver(void)
{
	m_wID		= 0x4066;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Unitronics";
	
	m_DriverName	= "PCOM Binary TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "PCOM Binary";
	}

// Binding Control

UINT CUniPCOMTcpBDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CUniPCOMTcpBDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CUniPCOMTcpBDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUniPCOMBinaryTcpDeviceOptions);
	}


//////////////////////////////////////////////////////////////////////////
//
// PCOM Data Table Management Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CPCOMDataTableDialog, CStdDialog);
		
// Constructor

CPCOMDataTableDialog::CPCOMDataTableDialog(CItem *pConfig)
{
	m_hSelect = NULL;

	m_hRoot	  = NULL;

	m_pConfig = (CUniPCOMBinaryDeviceOptions *) pConfig;

	SetName(L"CPCOMDataTableDlg");
	}

// Message Map

AfxMessageMap(CPCOMDataTableDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown	)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(IDCANCEL, OnCancel)
	AfxDispatchCommand(2001, OnCreate)
	AfxDispatchCommand(2002, OnEdit)

   	AfxMessageEnd(CPCOMDataTableDialog)
	};

// Message Handlers

BOOL CPCOMDataTableDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadDataTables();

	DoEnables();

	return TRUE;
	}

// Command Handlers

BOOL CPCOMDataTableDialog::OnOkay(UINT uID)
{
	if( uID == IDOK ) {

		EndDialog(TRUE);
	
		return TRUE;
		}

	return FALSE;
	}

BOOL CPCOMDataTableDialog::OnCancel(UINT uID)
{
	if( uID == IDCANCEL ) {

		// TODO !

		return TRUE;
		}

	return FALSE;
	}


BOOL CPCOMDataTableDialog::OnCreate(UINT uID)
{
	if( uID == 2001 ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		BOOL fTable = (m_hSelect == m_hRoot);

		CItem * pItem = NULL;

		CDataTable * pTable = NULL;

		if( fTable ) {
			
			pItem = (CItem *)m_pConfig->GetTable();
			}
		else {
			pTable = m_pConfig->FindTable(Tree.GetItemText(m_hSelect));

			if( pTable ) {
			
				pItem = (CItem *)pTable->GetColumn();
				}
			}

		if( pItem ) {

			CItemDialog Dlg(pItem, fTable ? "Create Table" : "Create Column");

			if( Dlg.Execute(*this) ) {

				CTreeViewItem Node;

				Node.SetParam(NULL);

				if( fTable ) {

					pTable = (CDataTable *)pItem;

					ShowTable(Tree, m_hSelect, pTable);
					}
				else {
					
					CColumn * pColumn = (CColumn *)pItem;

					ShowColumn(Tree, m_hSelect, pColumn);
					}
							
				return TRUE;
				}

			if( fTable ) {

				m_pConfig->DeleteTable(pItem);
				}
			
			else if ( pTable && pItem ) {
				
				pTable->DeleteColumn(pItem);
				}   
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPCOMDataTableDialog::OnEdit(UINT uID)
{
	if( uID == 2002 ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		CDataTableList * pList = m_pConfig->GetTableList(); 

		CString Label = Tree.GetItemText(m_hSelect);

		for( INDEX x = pList->GetHead(); !pList->Failed(x); pList->GetNext(x) ) {
			
			CDataTable * pTable = pList->GetItem(x);

			HTREEITEM hNode = Tree.GetParent(m_hSelect);

			if( pTable->GetLabel() == Label ) {

				CItemDialog Dlg(pTable, "Edit Table");

				Dlg.Execute(*this);

				UpdateTable(Tree, hNode, pTable);
				
				return TRUE;
				}
			
			CString Text = Tree.GetItemText(hNode);

			if( Text == pTable->GetLabel() ) {

				CColumnList * pCols = pTable->GetColumnList();

				for( INDEX y = pCols->GetHead(); !pCols->Failed(y); pCols->GetNext(y) ) {

					CColumn * pColumn = pCols->GetItem(y);

					if( pColumn->GetLabel() == Label ) {

						CItemDialog Dlg(pColumn, L"Edit Column");

						Dlg.Execute(*this);

						UpdateColumn(Tree, hNode, pColumn);

						return TRUE;
						}
					}
				} 
			}

		return TRUE;
		}

	return FALSE;
	}


// Notification Handlers		     

void CPCOMDataTableDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( uID == 1001 ) {

		m_hSelect = Info.itemNew.hItem;

		DoEnables();
		} 
	}


BOOL CPCOMDataTableDialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{	 
	if( uID == 1001 ) {

		if( Info.wVKey == VK_DELETE ) {
		
			if( GetKeyState(Info.wVKey) & 0x8000 ) {

				CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

				CItem * pItem = m_pConfig->FindTable(Tree.GetItemText(m_hSelect));

				m_pConfig->DeleteTable(pItem);
				
				Tree.DeleteItem(m_hSelect);
				
				return TRUE;
				}
			} 
		} 
      
	return TRUE;
	}


// Implementation

void CPCOMDataTableDialog::LoadDataTables(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle =	  TVS_DISABLEDRAGDROP
			| TVS_HASLINES
			| TVS_HASBUTTONS
			| TVS_SHOWSELALWAYS
			| TVS_NOTOOLTIPS; 

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.DeleteItem(TVI_ROOT);

	LoadRoot(Tree);

	LoadTables(Tree);

	Tree.SetFocus();

	m_hSelect = m_hRoot;

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CPCOMDataTableDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	PTXT Text = L"Data Table List";

	Node.SetText(Text);

	Node.SetParam(NULL);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CPCOMDataTableDialog::LoadTables(CTreeView &Tree)
{
	CDataTableList * pList = m_pConfig->GetTableList(); 

	CString Label = Tree.GetItemText(m_hSelect);

	for( INDEX x = pList->GetHead(); !pList->Failed(x); pList->GetNext(x) ) {
			
		CDataTable * pTable = pList->GetItem(x);

		ShowTable(Tree, m_hSelect, pTable);
		}
     	}

void CPCOMDataTableDialog::DoEnables(void)
{
	GetDlgItem(IDCANCEL).EnableWindow(FALSE);

	if( m_hSelect != m_hRoot ) {

		GetDlgItem(2001).EnableWindow(FALSE);

		GetDlgItem(2002).EnableWindow(TRUE);

		return;
		}

	GetDlgItem(2001).EnableWindow(TRUE);

	GetDlgItem(2002).EnableWindow(FALSE);
	}

void CPCOMDataTableDialog::ShowTable(CTreeView &Tree, HTREEITEM hNode, CDataTable * pTable)
{
	CTreeViewItem Node;

	Node.SetParam(NULL);
	
	Node.SetText(pTable->GetLabel());

	hNode = Tree.InsertItem(hNode, NULL, Node);

	while( Tree.GetItemText(hNode) != pTable->GetLabel() ) {
						
		hNode = Tree.GetNextItem(hNode, TVGN_CHILD);
		} 
		
	UINT uCols = pTable->GetDataAccess(L"Cols")->ReadInteger(pTable);
    	
	for( UINT u = 0; u < uCols; u++ ) {

		CColumn * pCol = pTable->GetColumn(u);

		ShowColumn(Tree, hNode, pCol);
		} 
	
	Tree.Expand(m_hSelect, TVE_EXPAND);
	}

void CPCOMDataTableDialog::ShowColumn(CTreeView &Tree, HTREEITEM hNode, CColumn * pColumn)
{
	CTreeViewItem Node;

	Node.SetParam(NULL);
	
	Node.SetText(pColumn->GetLabel());

	//Node.SetImages(13 + pColumn->GetVisual());

	Tree.InsertItem(hNode, NULL, Node);

	Tree.Expand(hNode, TVE_EXPAND);
	}

void CPCOMDataTableDialog::UpdateTable(CTreeView &Tree, HTREEITEM hNode, CDataTable * pTable)
{
   	CTreeViewItem Node;

	Node.SetParam(NULL);
	
	Node.SetText(pTable->GetLabel());

	hNode = Tree.InsertItem(hNode, m_hSelect, Node);

	while( Tree.GetItemText(hNode) != pTable->GetLabel() ) {
						
		hNode = Tree.GetNextItem(hNode, TVGN_CHILD);
		}
		
	UINT uCols = pTable->GetDataAccess(L"Cols")->ReadInteger(pTable);
    	
	for( UINT u = 0; u < uCols; u++ ) {

		CColumn * pCol = pTable->GetColumn(u);

		ShowColumn(Tree, hNode, pCol);
		}

	Tree.DeleteItem(m_hSelect);

	Tree.UpdateWindow();
	
	Tree.Expand(m_hSelect, TVE_EXPAND);
     	}


void CPCOMDataTableDialog::UpdateColumn(CTreeView &Tree, HTREEITEM hNode, CColumn * pColumn)
{
	CTreeViewItem Node;

	Node.SetParam(NULL);
	
	Node.SetText(pColumn->GetLabel());

	//Node.SetImages(13 + pColumn->GetVisual());

	Tree.InsertItem(hNode, m_hSelect, Node);

	Tree.DeleteItem(m_hSelect);
				
	Tree.UpdateWindow();
	}


//////////////////////////////////////////////////////////////////////////
//
// Unitronics Binary PCOM Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CUniPCOMBinaryDialog, CStdAddrDialog);
		
// Constructor

CUniPCOMBinaryDialog::CUniPCOMBinaryDialog(CUniPCOMBinaryDriver &Driver, CAddress &Addr, CItem * pItem, BOOL fPart) : CStdAddrDialog(Driver, Addr, pItem, fPart)
{
	m_Element = L"UniBinaryElementDlg";
	}

// Message Map

AfxMessageMap(CUniPCOMBinaryDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(2002, EN_KILLFOCUS,	OnColumnChange )
	
	AfxMessageEnd(CUniPCOMBinaryDialog)
	};

BOOL CUniPCOMBinaryDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	BOOL fReturn = CStdAddrDialog::OnInitDialog(Focus, dwData);

	OnColumnChange(2002, *this);

	return fReturn;
	}

void CUniPCOMBinaryDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	OnColumnChange(2002, Wnd);
	}
 
void CUniPCOMBinaryDialog::OnColumnChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace && uID == 2002 ) {

		ShowDetails();

		ClearType();

		CBinaryPCOMSpace *pSpace = (CBinaryPCOMSpace *)m_pSpace;

		CString Type = GetDlgItem(uID).GetWindowText();

		if( pSpace->SetType(tstrtoul(Type, NULL, 10)) ) {

			LoadType();
			}
		else {
			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(TRUE);

			CAddress Addr;
		
			m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text);	
			}
		}
	}

// Overridables

BOOL CUniPCOMBinaryDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrByteAsByte:
		case addrWordAsWord:
		case addrLongAsLong:
		case addrRealAsReal:

			return TRUE;
		}
	
	return FALSE;
	}


void CUniPCOMBinaryDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty() && m_pSpace;
	
	if( m_pSpace ) {

		UINT uFind = Text.Find(':');

		GetDlgItem(2002).SetWindowText(Text.Left(uFind));

		GetDlgItem(2005).SetWindowText(Text.Mid(uFind + 1));
     		}

	GetDlgItem(2002).EnableWindow(fEnable);

	GetDlgItem(2005).EnableWindow(fEnable);
	}

CString CUniPCOMBinaryDialog::GetAddressText(void)
{
	if( m_pSpace ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += ":";
		
		Text += GetDlgItem(2005).GetWindowText();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

/////////////////////////////////////////////////////////////////////////
//
// Binary PCOM Space Wrapper Class
//

// Constructors

CBinaryPCOMSpace::CBinaryPCOMSpace(UINT uTable, CString p, CDataTable * pTable)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= pTable->GetLabel();
	m_uRadix	= 10;
	m_uMinimum	= 0;
	m_uMaximum	= pTable->m_Rows - 1;
	m_uType		= addrWordAsWord;
	m_uSpan		= m_uType;
	m_uAlign	= 1;

	m_pTable        = pTable;

	FindWidth();
	}

BOOL CBinaryPCOMSpace::SetType(UINT uCol)
{
	if( uCol < m_pTable->GetColumnList()->GetItemCount() ) {

		m_uType = m_pTable->GetColumnList()->GetItem(uCol)->GetTypeAs();

		m_uSpan = m_uType; 

		FindWidth();

		return TRUE;
		}

	return FALSE;
	}

// Limits

void CBinaryPCOMSpace::GetMinimum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= m_uMinimum;
	}

void CBinaryPCOMSpace::GetMaximum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	UINT uCols = m_pTable->GetColumnList()->GetItemCount() - 1;

	Addr.a.m_Extra	= uCols & 0xF;

	Addr.a.m_Offset	= (m_uMaximum | ((uCols & 0x10) << 11));
	}


// End of File