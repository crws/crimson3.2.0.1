 
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DRIAIR_HPP
	
#define	INCLUDE_DRIAIR_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CDriAirDriver;

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	LL	addrLongAsLong

#define	USRALLOW	0x405E

// Space Definitions
enum {
	// Network enabled functions
	STAT	=   1,
	STOP	=   2,
	STRT	=   3,
	CEQ	=   4,
	DEW	=   5,
	ION	=   6,
	IOFF	=   7,
	LRN	=   8,
	LOFF	=   9,
	LON	=  10,
	MTR	=  11,
	VLV	=  12,
	PD1	=  13,
	PD2	=  14,
	PT1	=  15,
	PT2	=  16,
	RST	=  17,
	LDON	=  18,
	LDOFF	=  19,
	LDT	=  20,
	LDD	=  21,
	S1OFF	=  22,
	S1ON	=  23,
	S2OFF	=  24,
	S2ON	=  25,
	S1D	=  26,
	S2D	=  27,
	S1A	=  28,
	S2A	=  29,
	S1I	=  30,
	S2I	=  31,
	ADDR	=  32,
	// Original functions
	CTRP	=  40,
	DBND	=  41,
	DBNM	=  42,
	DBN1D	=  43,
	DBN1M	=  44,
	DBN2D	=  45,
	DBN2M	=  46,
	DPCD	=  47,
	DPCM	=  48,
	D1PCD	=  49,
	D1PCM	=  50,
	D2PCD	=  51,
	D2PCM	=  52,
	DTRP	=  53,
	DBOTD	=  54,
	DBOTM	=  55,
	DTOPD	=  56,
	DTOPM	=  57,
	EOFF	=  58,
	EON	=  59,
	FLGS	=  60,
	EXHI	=  61,
	EXLO	=  62,
	HDFF	=  63,
	IBNDD	=  64,
	IBNDM	=  65,
	I1BND	=  66,
	I1BNM	=  67,
	I2BND	=  68,
	I2BNM	=  69,
	INIT	=  70,
	INPT	=  71,
	IPCD	=  72,
	IPCM	=  73,
	I1PCD	=  74,
	I1PCM	=  75,
	I2PCD	=  76,
	I2PCM	=  77,
	IBOTD	=  78,
	IBOTM	=  79,
	ITOPD	=  80,
	ITOPM	=  81,
	OON	=  82,
	OOFF	=  83,
	PBND	=  84,
	PIDD	=  85,
	PIDM	=  86,
	PPRD	=  87,
	PPRM	=  88,
	P1PRD	=  89,
	P1PRM	=  90,
	P2PRD	=  91,
	P2PRM	=  92,
	PHT	=  93,
	PXB1	=  94,
	PXB2	=  95,
	BIAS1	=  96,
	BIAS2	=  97,
	PBOTD	=  98,
	PBOTM	=  99,
	PTOPD	= 100,
	PTOPM	= 101,
	RGEN	= 102,
	SETP	= 103,
	USRF1	= 104,
	USRF2	= 105,
	SPRCD	= 106,
	SPRCM	= 107,
	S1PRD	= 108,
	S1PRM	= 109,
	S2PRD	= 110,
	S2PRM	= 111,
	SBTD	= 112,
	SBTM	= 113,
	STPD	= 114,
	STPM	= 115,
	TEMPM	= 116,
	TON	= 117,
	TOFF	= 118,
	EVRQ	= 120,
	EVTA	= 121,
	EVTB	= 122,
	EVTC	= 123,
	EVTD	= 124,
	EVTE	= 125,
	EVTF	= 126,
	EVTG	= 127,
	EVTH	= 128,
	EVTI	= 129,
	EVTJ	= 130,
	STRQ	= 140,
	STSA	= 141,
	STSB	= 142,
	STSC	= 143,
	STSD	= 144,
	STSE	= 145,
	STSF	= 146,
	STSG	= 147,
	STSH	= 148,
	STSI	= 149,
	STSJ	= 150,
	DIAG	= 235,
	SCMD	= 236,
	UCMD	= 237,
	URSP	= 238,
	VOFF	= 239 // not available to programmer
	};

/////////////////////////////////////////////////////////////////////////
//
//  DriAir Device Options
//

class CDriAirDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDriAirDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
		UINT m_User;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dri-Air Comms Driver
//

class CDriAirDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CDriAirDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		void	SetUserAccess(UINT uUSRAccess);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
		void	AddUSRSpaces(BOOL fOk);

		// Helpers
	};

// End of File

#endif
