
#include "intern.hpp"

#include "mitq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mistubishi Q Series Master TCP/IP
//

// Instantiator

ICommsDriver *	Create_CMitsQMasterTCPDriver(void)
{
	return New CMitsQMasterTCPDriver;
	}

// Constructor

CMitsQMasterTCPDriver::CMitsQMasterTCPDriver(void)
{
	m_wID		= 0x350B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Mitsubishi";
	
	m_DriverName	= "Q Series TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Q TCP/IP Master";

	AddSpaces();
	}

// Binding Control

UINT CMitsQMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CMitsQMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CMitsQMasterTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CMitsQMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMitsQMasterTCPDeviceOptions);
	}

// Address Management

BOOL CMitsQMasterTCPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CMitsubQDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CMitsQMasterTCPDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( IsExtendedAddress(pSpace->m_uTable) ) {

		CString Type = StripType(pSpace, Text);

		PTXT pError = NULL;

		UINT uPos = wcstoul(Text, &pError, pSpace->m_uRadix);

		if( pError && pError[0] ) {
			
			Error.Set(IDS_ERROR_ADDR);
			
			return FALSE;
			}
		else {
			if( pSpace->IsOutOfRange(uPos) ) {
			
				Error.Set(IDS_ERROR_OFFSETRANGE);
				
				return FALSE;
				}
			}

		Addr.a.m_Type	= pSpace->TypeFromModifier(Type);

		Addr.a.m_Table	= pSpace->m_uTable;
	
		Addr.a.m_Extra	= uPos / 65536;

		Addr.a.m_Offset	= uPos % 65536;
	
		return TRUE;
		}
	else {
		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}
	}

BOOL CMitsQMasterTCPDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( IsExtendedAddress(Addr.a.m_Table) ) {

		CSpace *pSpace = GetSpace(Addr);

		if( pSpace ) {

			UINT uOffset	= Addr.a.m_Offset + (65536 * Addr.a.m_Extra);

			UINT uType	= Addr.a.m_Type;

			if( uType == pSpace->m_uType ) {

				Text.Printf( L"%s%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			else {
				Text.Printf( L"%s%s.%s",
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}

			return TRUE;
			}

		return FALSE;
		}
	else {
		return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
		}
	}

BOOL CMitsQMasterTCPDriver::IsExtendedAddress(UINT uTable)
{
	switch( uTable ) {

		case 'D':
		case '0':
			return TRUE;
		}

	return FALSE;
	}

// Implementation

void CMitsQMasterTCPDriver::AddSpaces(void)
{
	AddSpace(New CMitsQSpace('X', "X",  "Input",			 16,  0,  0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('Y', "Y",  "Output",			 16,  0,  0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('1', "DX", "Direct Input",		 16,  0,  0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('2', "DY", "Direct Output",		 16,  0,  0x1FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('F', "F",  "Annunciator",		 10,  0,   32767, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('M', "M",  "Internal Relay",		 10,  0,   61439, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('L', "L",  "Latch Relay",		 10,  0,   32767, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('B', "B",  "Link Relay",		 16,  0,  0xEFFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('S', "S",  "Step Relay",		 10,  0,   16383, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('V', "V",  "Edge Relay",		 10,  0,   32767, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('4', "SB", "Special Link Relay",	 16,  0,  0x7FFF, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('5', "SW", "Special Link Register",	 16,  0,  0x7FFF, addrWordAsWord, addrWordAsReal));
	AddSpace(New CMitsQSpace('6', "SM", "Special Relay",		 10,  0,    2047, addrBitAsBit,   addrBitAsWord));
	AddSpace(New CMitsQSpace('7', "SD", "Special Register",		 10,  0,    2047, addrWordAsWord, addrWordAsReal));
	AddSpace(New CMitsQSpace('D', "D",  "Data Register",		 10,  0, 1048575, addrWordAsWord, addrWordAsReal));
	AddSpace(New CMitsQSpace('R', "R",  "File Register",		 10,  0,   32767, addrWordAsWord, addrWordAsReal));
	AddSpace(New CMitsQSpace('0', "ZR", "Extended File Register",	 10,  0, 1048575, addrWordAsWord, addrWordAsReal));
	AddSpace(New CMitsQSpace('Z', "Z",  "Index Register",		 10,  0,      19, addrWordAsWord, addrWordAsReal));
	AddSpace(New CMitsQSpace('W', "W",  "Link Register",		 16,  0,  0x7FFF, addrWordAsWord));
	AddSpace(New CMitsQSpace('T', "TN", "Timer Current Value",	 10,  0,   32767, addrWordAsWord));
	AddSpace(New CMitsQSpace('8', "TS", "Timer Contact",		 10,  0,   32767, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('9', "TC", "Timer Coil",		 10,  0,   32767, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('C', "CN", "Counter Current Value",	 10,  0,   32767, addrWordAsWord));
	AddSpace(New CMitsQSpace('A', "CS", "Counter Contact",		 10,  0,   32767, addrBitAsBit, addrBitAsWord));
	AddSpace(New CMitsQSpace('E', "CC", "Counter Coil",		 10,  0,   32767, addrBitAsBit, addrBitAsWord));
	//AddSpace(New CMitsQSpace('3', "SN", "Ret. Timer Current Value",10,  0,    2047, addrWordAsWord));
	}


//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMitsQMasterTCPDeviceOptions, CUIItem);

// Constructor

CMitsQMasterTCPDeviceOptions::CMitsQMasterTCPDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket  = 5002;

	m_Code    = 0;

	m_Monitor = 10;

	m_Keep    = TRUE;

	m_Ping    = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;

	m_Net	  = 1;

	m_Plc	  = 1;

	m_Cpu     = 0;
	}

// UI Managament

void CMitsQMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support	     

BOOL CMitsQMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Code));
	Init.AddWord(WORD(m_Monitor));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_Net));
	Init.AddByte(BYTE(m_Plc));
	Init.AddByte(BYTE(m_Cpu));

	return TRUE;
	}

// Meta Data Creation

void CMitsQMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Code);
	Meta_AddInteger(Monitor);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Net);
	Meta_AddInteger(Plc);
	Meta_AddInteger(Cpu);
	}

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series Address Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CMitsubQDialog, CStdAddrDialog);
		
// Constructor

CMitsubQDialog::CMitsubQDialog(CStdCommsDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	}

// Overridables

BOOL CMitsubQDialog::AllowType(UINT uType)
{
	// TODO -- This works here, but doesn't stop direct address entry!

	return uType != addrBitAsByte;
	}

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series Space Wrapper
//

// Constructors

CMitsQSpace::CMitsQSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t) : CSpace(uTable, p, c, r, n, x, t)
{
	}

CMitsQSpace::CMitsQSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s) : CSpace(uTable, p, c, r, n, x, t, s)
{
	}

// Limits

void CMitsQSpace::GetMaximum(CAddress &Addr)
{
	if( m_uMaximum >= 65536 ) {

		Addr.a.m_Table	= m_uTable;

		Addr.a.m_Offset	= m_uMaximum % 65536;

		Addr.a.m_Extra	= m_uMaximum / 65536;
		}
	else
		CSpace::GetMaximum(Addr);
	}

// End of File
