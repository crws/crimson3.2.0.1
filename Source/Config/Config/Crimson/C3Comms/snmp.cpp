
#include "intern.hpp"

#include "snmp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SNMP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CSnmpDriverOptions, CUIItem);

// Constructor

CSnmpDriverOptions::CSnmpDriverOptions(void)
{
	m_Port1     = 161;
	m_Port2	    = 162;
	m_Comm      = L"public";
	m_TabLimit  = 999;
	m_ChgLimit  = 1;
	m_AclAddr1  = 0;
	m_AclMask1  = 0;
	m_AclAddr2  = 0;
	m_AclMask2  = 0;
	m_TrapFrom  = 0;
	m_TrapAddr1 = 0;
	m_TrapMode1 = 0;
	m_TrapAddr2 = 0;
	m_TrapMode2 = 0;
	}

// UI Managament

void CSnmpDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(pWnd);
		}
		
	if( Tag == "TrapMode1" || Tag == "TrapMode2" ) {

		DoEnables(pWnd);
		}
		
	if( Tag == "AclAddr1" || Tag == "AclAddr2" ) {

		DoEnables(pWnd);
		}
	}

// Download Support

BOOL CSnmpDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Port1));
	Init.AddWord(WORD(m_Port2));
	Init.AddText(m_Comm);
	Init.AddWord(WORD(m_TabLimit));
	Init.AddLong(m_AclAddr1);
	Init.AddLong(m_AclMask1);
	Init.AddLong(m_AclAddr2);
	Init.AddLong(m_AclMask2);
	Init.AddLong(m_TrapFrom);
	Init.AddLong(m_TrapAddr1);
	Init.AddWord(WORD(m_TrapMode1));
	Init.AddLong(m_TrapAddr2);
	Init.AddWord(WORD(m_TrapMode2));
	Init.AddWord(WORD(m_ChgLimit));
			
	return TRUE;
	}

// Meta Data Creation

void CSnmpDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Port1);
	Meta_AddInteger(Port2);
	Meta_AddString (Comm);
	Meta_AddInteger(TabLimit);
	Meta_AddInteger(AclAddr1);
	Meta_AddInteger(AclMask1);
	Meta_AddInteger(AclAddr2);
	Meta_AddInteger(AclMask2);
	Meta_AddInteger(TrapFrom);
	Meta_AddInteger(TrapAddr1);
	Meta_AddInteger(TrapMode1);
	Meta_AddInteger(TrapAddr2);
	Meta_AddInteger(TrapMode2);
	Meta_AddInteger(ChgLimit);
	}

// Data Access

UINT CSnmpDriverOptions::GetChangeLimit(void)
{
	return m_ChgLimit;
	}

UINT CSnmpDriverOptions::GetTableLimit(void)
{
	return m_TabLimit;
	}

// Implementation

void CSnmpDriverOptions::DoEnables(CUIViewWnd *pWnd)
{
	pWnd->EnableUI("TrapAddr1", m_TrapMode1 > 0);

	pWnd->EnableUI("TrapAddr2", m_TrapMode2 > 0);

	pWnd->EnableUI("TrapFrom",  m_TrapMode1 == 1 || m_TrapMode2 == 1);

	pWnd->EnableUI("AclMask1",  m_AclAddr1 > 0);

	pWnd->EnableUI("AclMask2",  m_AclAddr2 > 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// SNMP Driver
//

// Instantiator

ICommsDriver * Create_SnmpDriver(void)
{
	return New CSnmpDriver;
	}

// Constructor

CSnmpDriver::CSnmpDriver(void)
{
	m_wID		= 0x409D;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "SNMP";
	
	m_DriverName	= "SNMP Agent";
	
	m_Version	= "1.01";
	
	m_ShortName	= "SNMP";

	AddSpaces();
	}

// Configuration

CLASS CSnmpDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CSnmpDriverOptions);
	}

CLASS CSnmpDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Binding Control

UINT CSnmpDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CSnmpDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Address Management

BOOL CSnmpDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem * pDrvCfg)
{
	SetLimits(pDrvCfg);

	CSnmpAddrDialog Dlg(*this, Addr, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CSnmpDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem *pDrvCfg)
{
	SetLimits(pDrvCfg);

	return CStdCommsDriver::ParseAddress(Error, Addr, pConfig, Text);
	}

BOOL CSnmpDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace * pSpace, CString Text)
{
	CSnmpSpace * pSnmp = (CSnmpSpace *) pSpace;

	UINT uFind = Text.Find(':');

	UINT uChange = 0;

	if( uFind < NOTHING ) {

		uChange = tatoi(Text.Mid(uFind + 1));

		if( uChange > pSnmp->GetChangeLimit() ) {

			Error.Set(CString(IDS_ON_CHANGE_VALUE));

			return FALSE;
			}

		Text = Text.Left(uFind);
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		if( uChange ) {

			uChange -= 1;

			pSnmp->EncodeChange(Addr, uChange);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CSnmpDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.a.m_Table == 6 ) {

		CSnmpSpace *pSpace = (CSnmpSpace *) GetSpace(Addr);

		if( pSpace ) {

			Text.Printf( L"%s%s:%4.4u", 
				     pSpace->m_Prefix, 
				     pSpace->GetValueAsText(Addr.a.m_Offset & 0x3FF),
				     pSpace->DecodeChange(Addr)
				     );

			return TRUE;
			}

		return FALSE;
		}
	
	return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
	}

 // Implementation

void CSnmpDriver::AddSpaces(void)
{
	AddSpace(New CSnmpSpace(3, "genD",	"Generic Data",		10, 1, 999, addrLongAsLong));

	AddSpace(New CSnmpSpace(4, "genT",	"Generic Trap",		10, 1, 999, addrLongAsLong));

	AddSpace(New CSnmpSpace(6, "genAT",	"Generic Analog Trap",	10, 1, 999, addrLongAsLong));
	}

void CSnmpDriver::SetLimits(CItem * pDrvCfg)
{
	CSnmpDriverOptions * pOpt = (CSnmpDriverOptions *) pDrvCfg;

	if( pOpt ) {

		CSpaceList & List = GetSpaceList();

		INDEX Index = List.GetHead();

		while( !List.Failed(Index) ) {

			CSnmpSpace * pSnmp = (CSnmpSpace *) List.GetAt(Index);

			if( pSnmp ) {

				pSnmp->SetChangeLimit(pOpt->GetChangeLimit());

				pSnmp->SetTableLimit(pOpt->GetTableLimit());
				}

			List.GetNext(Index);
			}	
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// SNMP Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSnmpAddrDialog, CStdAddrDialog);
		
// Constructor

CSnmpAddrDialog::CSnmpAddrDialog(CSnmpDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element     = "SnmpElementDlg";
	}

// Message Map

AfxMessageMap(CSnmpAddrDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	
	AfxMessageEnd(CSnmpAddrDialog)
	};

BOOL CSnmpAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	BOOL f = CStdAddrDialog::OnInitDialog(Focus, dwData);

	DoEnables();

	return f;
	}

void CSnmpAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	DoEnables();
	}

// Overridables

void CSnmpAddrDialog::SetAddressText(CString Text)
{
	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		GetDlgItem(2004).SetWindowText(Text.Mid(uFind + 1));

		Text = Text.Left(uFind);
		}
	
	CStdAddrDialog::SetAddressText(Text);
	}

CString CSnmpAddrDialog::GetAddressText(void)
{
	CString Text = CStdAddrDialog::GetAddressText();

	if( GetDlgItem(2004).IsWindowVisible()) {

		Text += ":";

		Text += GetDlgItem(2004).GetWindowText();
		}

	return Text;
	}

// Helpers

void CSnmpAddrDialog::DoEnables(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT uSel  = ListBox.GetCurSel();

	UINT uShow = SW_HIDE;

	if( uSel ) {

		CSpaceList &List = m_pDriver->GetSpaceList();

		INDEX Index = List.GetHead();

		UINT u = 0;

		while( !List.Failed(Index) ) {

			if( u == uSel - 1 ) {

				break;
				}
	
			List.GetNext(Index);

			u++;
			}

		CSpace * pSpace = m_pDriver->GetSpace(Index);

		if( pSpace && pSpace->m_uTable == 6 ) {

			uShow = SW_SHOW;
			}
		}

	GetDlgItem(2004).ShowWindow(uShow);

	GetDlgItem(2005).ShowWindow(uShow);
	}

//////////////////////////////////////////////////////////////////////////
//
// SNMP Space
//

 CSnmpSpace::CSnmpSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t) : CSpace(uTable, p, c, r, n, x, t)
 {
	m_uChgLimit = 1;
	}

// Limits

void CSnmpSpace::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	if( m_uTable == 6 ) {

		EncodeChange(Addr, m_uChgLimit);
		}
	}

UINT CSnmpSpace::GetChangeLimit(void)
{
	return m_uChgLimit + 1;
	}

void CSnmpSpace::SetChangeLimit(UINT uMax)
{
	m_uChgLimit = uMax - 1;
	}

UINT CSnmpSpace::GetTableLimit(void)
{
	return m_uMaximum;
	}

void CSnmpSpace::SetTableLimit(UINT uMax)
{
	m_uMaximum = uMax;
	}

// Implementation

void CSnmpSpace::EncodeChange(CAddress &Addr, UINT uChange)
{
	Addr.a.m_Extra	|=  (uChange & 0xF);

	Addr.a.m_Offset |= ((uChange & 0x3F0) << 6);
	}

UINT CSnmpSpace::DecodeChange(CAddress Addr)
{
	return (((Addr.a.m_Offset & 0xFC00) >> 6) | Addr.a.m_Extra) + 1;
	}

// End of File
