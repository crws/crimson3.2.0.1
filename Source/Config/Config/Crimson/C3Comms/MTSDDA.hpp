
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MTSDDA_HPP
	
#define	INCLUDE_MTSDDA_HPP

class CMTSDDADriver;
class CMTSDDAMasterDriver;

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Comms Driver - Application Master / Network Slave
//

class CMTSDDADriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMTSDDADriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Master Comms Driver
//

class CMTSDDAMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMTSDDAMasterDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:

		// Implementation
		void	AddSpaces(void);

	};

// End of File

#endif
