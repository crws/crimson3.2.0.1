
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IRCON_HPP
	
#define	INCLUDE_IRCON_HPP

class CIrconDeviceOptions;
class CIrconDriver;
class CIrconDialog;


//////////////////////////////////////////////////////////////////////////
//
// Ircon Device Options
//

class CIrconDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIrconDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		CString m_Drop;
		UINT    m_fModel;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ircon Comms Driver
//

class CIrconDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIrconDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Device Options
		CLASS	GetDeviceConfig(void);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
