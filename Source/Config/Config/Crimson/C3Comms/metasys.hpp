
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_METASYS_HPP
	
#define	INCLUDE_METASYS_HPP 


//////////////////////////////////////////////////////////////////////////
//
// MetaSys Space Wrapper Class
//

class CMetaSysSpace : public CSpace
{
	public:
		// Constructors
		CMetaSysSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT MinReg, UINT MaxReg, UINT Attr);
			
		// Limits
		void GetMaximum(CAddress &Addr);

		// Data Members
		UINT	m_uMinReg;
		UINT    m_uMaxReg;
		UINT    m_uAttribute;

	};

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 Driver Options
//

class CMetaSysDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMetaSysDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
			
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 Device Options
//

class CMetaSysDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMetaSysDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
			
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Slave Driver
//
//

class CMetasysN2SystemDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMetasysN2SystemDriver(void);

		//Destructor
		~CMetasysN2SystemDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Metasys Address Selection
//

class CMetaSysAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CMetaSysAddrDialog(CMetasysN2SystemDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Slave Driver - Single Device - Current Value Only
//
//

class CMetasysN2SystemDriver2 : public CStdCommsDriver
{
	public:
		// Constructor
		CMetasysN2SystemDriver2(void);

		//Destructor
		~CMetasysN2SystemDriver2(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Slave Driver - Multi Device - Current Value Only
//
//

class CMetasysN2SystemDriver3 : public CMetasysN2SystemDriver2
{
	public:
		// Constructor
		CMetasysN2SystemDriver3(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

// End of File


#endif
