
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "mbtcpm.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "mbtcpm.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "mbtcpm.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "mbtcpm.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CModbusTCPMasterDeviceOptions
//

CModbusMasterTCPDeviceOptionsUIList RCDATA
BEGIN
	"Addr,Primary IP Address,,CUIIPAddress,,"
	"Indicate the primary IP address of the Modbus device."
	"\0"

	"Addr2,Fallback IP Address,,CUIIPAddress,,"
	"Indicate the secondary IP address of the Modbus device. This will be used "
	"if the primary address does not respond, and can be used to implement "
	"redundant communications. Leave the field at 0.0.0.0 to disable fallback."
	"\0"

	"Socket,TCP Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the TCP port number on which the Modbus protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Unit,Unit Number,,CUIEditInteger,|0||0|255,"
	"Indicate the unit within the Modbus server that you wish to address. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the Modbus driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the Modbus server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a Modbus request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"Disable16,Disable Code 16 (Multiple Words),,CUICheck,,"
	"Indicate whether you want to disable multiple register writes using code 16. "
	"If this code is disabled, the driver will write words one-at-a-time instead. "
	"\0"

	"Disable15,Disable Code 15 (Multiple Bits),,CUICheck,,"
	"Indicate whether you want to disable multiple register writes using code 15. "
	"If this code is disabled, the driver will write bits one-at-a-time instead. "
	""
	"\0"

	"Disable6,Disable Code 6 (Single Word),,CUICheck,,"
	"Indicate whether you want to disable single register writes using code 6. "
	"If this code is disabled, the driver will use code 16 for all word writes." 
	"\0"

	"Disable5,Disable Code 5 (Single Bit),,CUICheck,,"
	"Indicate whether you want to disable single register writes using code 5. "
	"If this code is disabled, the driver will use code 15 for all bit writes."
	"\0"

	"PingReg,Ping Holding Register,,CUIEditInteger,|0||0|65535,"
	"Indicate the Holding Register that the driver should use to detect the online "
	"device.  This should be set to the Extended Address format of a known 16-bit "
	"holding register, omitting the leading 4.  If the slave has no Holding "
	"Registers, then use a setting of 0, which will disable the ping.  You must be "
	"sure that this register is valid in the target device or the connection will "
	"never be established."
	"\0"

	"Max03,Code 03,,CUIEditInteger,|0|words|0|128,"
	"Indicate the maximum number of words to be read in a single code 03 exchange. "
	"The default value will work for nearly all applications, and a larger value can "
	"be used to improve performance if you are sure the target device supports it."
	"\0"

	"Max16,Code 16,,CUIEditInteger,|0|words|0|128,"
	"Indicate the maximum number of words to be written in a single code 16 exchange. "
	"The default value will work for nearly all applications, and a larger value can "
	"be used to improve performance if you are sure the target device supports it."
	"\0"

	"Max01,Code 01,,CUIEditInteger,|0|bits|0|2048,"
	"Indicate the maximum number of bits to be read in a single code 01 exchange. "
	"The default value will work for nearly all applications, and a larger value can "
	"be used to improve performance if you are sure the target device supports it."
	"\0"

	"Max15,Code 15,,CUIEditInteger,|0|bits|0|2048,"
	"Indicate the maximum number of bits to be written in a single code 15 exchange. "
	"The default value will work for nearly all applications, and a larger value can "
	"be used to improve performance if you are sure the target device supports it."
	"\0"

	"Max02,Code 02,,CUIEditInteger,|0|bits|0|2048,"
	"Indicate the maximum number of words to be read in a single code 02 exchange. "
	"The default value will work for nearly all applications, and a larger value can "
	"be used to improve performance if you are sure the target device supports it."
	"\0"

	"Max04,Code 04,,CUIEditInteger,|0|words|0|128,"
	"Indicate the maximum number of words to be read in a single code 04 exchange. "
	"The default value will work for nearly all applications, and a larger value can "
	"be used to improve performance if you are sure the target device supports it."
	"\0"

	"IgnoreReadEx,Ignore Read Exceptions,,CUIDropDown,No|Yes,"
	"When enabled, G3 will ignore any exception received in response to a read request and "
	"return a value of 0."
	"\0"

	"FlipLong,In Long Blocks,,CUIDropDown,High Then Low|Low Then High,"
	""
	"\0"

	"FlipReal,In Real Blocks,,CUIDropDown,High Then Low|Low Then High,"
	""
	"\0"

	"\0"
END

#if defined(C3_VERSION) //KEEP

CModbusMasterTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Addr2,Socket,Unit\0"
	"G:1,root,Protocol Options,PingReg,IgnoreReadEx,Keep,Ping,Time1,Time3,Time2\0"
	"G:2,root,Word Ordering,FlipLong,FlipReal\0"
	"G:2,root,Register Writes,Disable16,Disable6,Disable15,Disable5\0"
	"G:2,root,Frame Register Limits,Max03,Max16,Max01,Max15,Max02,Max04\0"
	"\0"
END

#else //KEEP

CModbusMasterTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Socket,Unit\0"
	"G:1,root,Protocol Options,PingReg,IgnoreReadEx,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END

CModbusMasterTCPDeviceOptionsUIPage1 RCDATA
BEGIN
	"P:1\0"
	"G:2,root,Word Ordering,FlipLong,FlipReal\0"
	"G:2,root,Register Writes,Disable16,Disable6,Disable15,Disable5\0"
	"G:2,root,Frame Register Limits,Max03,Max16,Max01,Max15,Max02,Max04\0"
	"\0"
END

#endif //KEEP

// End of File
