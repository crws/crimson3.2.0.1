
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BBBSAP_HPP
	
#define	INCLUDE_BBBSAP_HPP

//////////////////////////////////////////////////////////////////////////
//
// BSAP Space Definitions
//
enum {
	SPBIT		= 1,
	SPBYTE		= 2,
	SPWORD		= 3,
	SPLONG		= 4,
	SPREAL		= 5,
	SPSTR		= 6,
	};

enum {
	BUTBIT		= 4002,
	BUTBYTE		= 4003,
	BUTWORD		= 4004,
	BUTLONG		= 4005,
	BUTREAL		= 4006,
	BUTSTR		= 4007,

	// Slave specific fields
	LISTNUMTXT	= 4020,
	LISTNUM		= 4021,
	TXTADDR		= 4022,
	TXTHEX		= 4023,
	LISTVAL		= 4024,
	LISTWARN	= 4025,
	LISTINFO	= 4026,
	};

enum {
	BTYPEBIT	= 1,
	BTYPEBYTE	= 2,
	BTYPEWORD	= 3,
	BTYPELONG	= 4,
	BTYPEREAL	= 5,
	BTYPESTR	= 6,
	};

#define	STRUSED	"This name or function is already in use"

// String Selections Import/Export
#define	STRHDR	"Bristol Babcock BSAP Variable Names for "

// Warning to update gateway block
#define	STRGBWARN "Redo Block Size in every gateway block that uses this item."

#define MAX_CHARS	80
#define MAX_TAG_LENGTH	24

//////////////////////////////////////////////////////////////////////////
//
// Tag Data
//

struct CTagDataB {
	
	CString	m_Name;
	DWORD	m_Addr;
	WORD    m_Index;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Collections
//

typedef CArray <CTagDataB> CTagDataBArray;

//////////////////////////////////////////////////////////////////////////
//
// Address Template
//

inline int AfxCompare(CTagDataB const &m1, CTagDataB const &m2)
{	
	if( m1.m_Index > m2.m_Index ) return +1;

	if( m1.m_Index < m2.m_Index ) return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock TCP Slave Driver Options
//

class CBBBSAPTCPSDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTCPSDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Socket;
		UINT m_Count;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// BSAP Serial Slave Driver Options
//

class CBBBSAPSerialSDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPSerialSDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags Device Options
//

class CBBBSAPTagsDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTagsDeviceOptions(void);

		// Attributes
		CString GetDeviceName(void);

		// Operations
		BOOL	CreateTag (CError &Error, CString const &Name, CAddress Addr, BOOL fNew, BOOL fUpdate);
		void	DeleteTag(CAddress const &Addr, BOOL fKill);
		BOOL	RenameTag(CString const &Name, CAddress const &Addr);
		CString	CreateName(CAddress Addr);
		void	ListTags(CTagDataBArray &List);
		void    WalkTags(BOOL fForce = FALSE);
		void	SetPrevious(CString Text);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void	Load(CTreeFile &Tree);
		void	Save(CTreeFile &Tree);

		// Download Support
		void	PrepareData(void);
		CTagDataBArray SortBBList(CTagDataBArray List);
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Push;
		BOOL	m_fSlave;

	protected:

		// Type Definitions
		typedef	CMap   <CString, DWORD> CTagsFwdMap;
		typedef	CMap   <DWORD, CString> CTagsRevMap;
		typedef	CArray <DWORD>          CAddrArray;

		// Tag Map
		CTagsFwdMap	m_TagsFwdMap;
		CTagsRevMap	m_TagsRevMap;
		CAddrArray	m_AddrArray;
		CAddrArray	m_PrepAddr;
		CStringArray	m_Tags;

		// Data Members
		CArray <UINT>	m_Errors;
		CTagDataBArray	m_List;
		CArray <UINT>	m_StrIndex;
		CString		m_Previous;
		
		// Meta Data Creation
		void	AddMetaData(void);

		// Property Save Filter
		BOOL	SaveProp(CString Tag) const;

		// Implementation
		void	OnManage(CWnd *pWnd);
		void	OnImport(CWnd *pWnd);
		void	OnExport(CWnd *pWnd);
		
		CString CreateName(PCTXT pFormat);
		UINT    CreateIndex (BOOL fUpdate);
		DWORD   CreateIndexS(BOOL fUpdate, DWORD Ref);
		UINT	GetNewIndex(void);
		DWORD	GetNewIndexS(DWORD Ref);
		CString	GetVarType(UINT uType);

		void	SaveTags(CTreeFile &Tree);
		void	LoadTags(CTreeFile &Tree);
		void    Rebuild(void);
		CString	GetTagName(CString const &Text);

		// Address Helpers
		WORD	GetIndex(CAddress const &Addr);
		BOOL	UsesName(UINT uTable);
		BOOL	IsString(CAddress Addr);
		BOOL    SetStringOffset(CAddress &Addr, UINT uIndex);

		// Import/Export Support
		void	Export(FILE *pFile);
		BOOL	Import(FILE *pFile);
		CString	FixLine(CString sCheck);
		BOOL	NextComma(CString sCheck, UINT *pPos);
		UINT	CheckItem(PCTXT sCheck, PCTXT sItem);
		BOOL	ParseType(CAddress &Addr, CString Text);
		void	ExpandType(CString &Text, CAddress const &Addr);
		void	ShowErrors(CWnd *pWnd);

		// Friends
		friend	class CBBBSAPTagsDialog;
		friend	class CBBBSAPTagsTCPDialog;
		friend	class CBBBSAPTagsSerialDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags TCP Device Options
//

class CBBBSAPTagsTCPDeviceOptions : public CBBBSAPTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTagsTCPDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags TCP Slave Device Options
//

class CBBBSAPTagsTCPSDeviceOptions : public CBBBSAPTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTagsTCPSDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags Serial Device Options
//

class CBBBSAPTagsSerialDeviceOptions : public CBBBSAPTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTagsSerialDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Tags Serial Slave Device Options
//

class CBBBSAPTagsSerialSDeviceOptions : public CBBBSAPTagsDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTagsSerialSDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock TCP/IP Master Device Options
//

class CBBBSAPTCPDeviceOptions : public CBBBSAPTagsTCPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTCPDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Addr;
		UINT	m_Socket;
		UINT	m_Keep;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock TCP/IP Slave Device Options
//

class CBBBSAPTCPSDeviceOptions : public CBBBSAPTagsTCPSDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPTCPSDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Master Device Options
//

class CBBBSAPSerialDeviceOptions : public CBBBSAPTagsSerialDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPSerialDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Addr;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Slave Device Options
//

class CBBBSAPSerialSDeviceOptions : public CBBBSAPTagsSerialSDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBBBSAPSerialSDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags TCP Driver
//

class CBBBSAPTagsBaseDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBBBSAPTagsBaseDriver(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP Driver
//

class CBBBSAPTCPDriver : public CBBBSAPTagsBaseDriver
{
	public:
		// Constructor
		CBBBSAPTCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
//		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP Slave Driver
//

class CBBBSAPTCPSDriver : public CBBBSAPTagsBaseDriver
{
	public:
		// Constructor
		CBBBSAPTCPSDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Driver
//

class CBBBSAPSerialDriver : public CBBBSAPTagsBaseDriver
{
	public:
		// Constructor
		CBBBSAPSerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
//		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Driver
//

class CBBBSAPSerialSDriver : public CBBBSAPTagsBaseDriver
{
	public:
		// Constructor
		CBBBSAPSerialSDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Selection Dialog
//

class CBBBSAPTagsDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBBBSAPTagsDialog(CBBBSAPTagsBaseDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBBBSAPTagsDialog(void);

		// Initialisation
		void	SetCaption(CString const &Text);
		void	SetSelect(BOOL fSelect);
		                
	protected:
		// Data Members
		CString		m_Caption;
		HTREEITEM	m_hRoot;
		HTREEITEM	m_hSelect;
		DWORD		m_dwSelect;
		CImageList	m_Images;
		BOOL		m_fLoad;
		BOOL		m_fSelect;
		BOOL		m_fUpdate;
		BOOL		m_fSlave;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL	OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);

		// Notification Handlers
		void	OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL	OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);

		// Command Handlers
		BOOL	OnOkay(UINT uID);
		BOOL	OnCreateTag(UINT uID);
		BOOL	OnButtonClicked(UINT uID);
		BOOL	OnUpdateAddress(UINT uID);

		// Tag Name Loading
		void	LoadNames(void);
		void	LoadNames(CTreeView &Tree);
		void	LoadRoot(CTreeView &Tree);
		void	LoadTags(CTreeView &Tree);
		void	LoadTag (CTreeView &Tree, CString Name, CAddress Addr);

		// Implementation
		void	ShowDetails(CAddress Addr);
		void	ShowAddress(CAddress Addr);
		BOOL	DeleteTag(void);
		BOOL	RenameTag(void);
		void	UpdateTagAddr(void);

		// Implementation
		BOOL	IsDown(UINT uCode);
		CString	FindNameFromAddr(CAddress Addr);

		// Radio button handling
		void	UpdateRadios(CAddress Addr);
		UINT	GetRadioButton(void);
		void	SetRadioButton(UINT Item, BOOL fID);
		void	SetRadioButtons(CAddress Addr);
		void	PutRadioButton(UINT uID, BOOL fYes);
		void	ClearRadioButtons(void);
		void	EnableRadioButtons(void);
		UINT	ButtonToType(UINT uButton);
		UINT	ButtonToTable(void);

		// Other Helpers
		UINT	GetTypeFromTable(UINT uTable);
		DWORD	MakeAddrFromButtons(void);
		void	UpdateList(UINT uNum);
		void	SetListNumber(UINT uNum);
		UINT	GetListNumber(void);
		UINT	GetListNumber(UINT uAddr);
		void	DoWarning(BOOL fSet);
	};

// End of File

#endif
