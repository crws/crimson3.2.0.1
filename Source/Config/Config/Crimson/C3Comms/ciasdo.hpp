
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CIASDO_HPP
	
#define	INCLUDE_CIASDO_HPP

//////////////////////////////////////////////////////////////////////////
//
// CANOpen Constants
//

#define OBJ_ALL		0
#define OBJ_MAND	1
#define OBJ_OPT		2
#define OBJ_MANU	3

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCANOpenDialog;


//////////////////////////////////////////////////////////////////////////
//
// CANOpen Driver Base Class
//

class CCANOpenDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CCANOpenDriver(void);

		// Driver Data
		UINT GetFlags(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Data
		CCANOpenDialog * m_pDlg;

		// Slave Detection
		virtual BOOL IsSlave(void);
	
		// Helpers
		UINT EncodeIndex(UINT uIndex, CItem *pConfig);
		UINT DecodeIndex(UINT uIndex, CItem *pConfig);

		// Implementation
		void AddSpaces(CItem *pConfig);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Master
//

class CCANOpenSDO : public CCANOpenDriver
{
	public:
		// Constructor
		CCANOpenSDO(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen Slave
//

class CCANOpenSlave : public CCANOpenDriver
{
	public:
		// Constructor
		CCANOpenSlave(void);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Slave Detection
		BOOL IsSlave(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Master Device Options
//

class CCANOpenSDODeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANOpenSDODeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT	m_Drop;
		UINT	m_Init;
		CString m_Ping;
		CString	m_Offset;
		UINT	m_Type;
		UINT    m_Time;
		UINT	m_Restrict;
		UINT    m_Increment;
		UINT    m_Entry;
		UINT    m_Delay;
		UINT    m_Transfer;
		UINT    m_BackOff;
			
	protected:
		// Derrived Data
		UINT    m_uPing;
		UINT    m_uOffset;

		// Meta Data Creation
		void AddMetaData(void);

		// Helper
		void SetHexadecimal(CUIViewWnd *pWnd, CString Tag, CString &Text, UINT &uTarget);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen Slave DriverOptions
//

class CCANOpenSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCANOpenSlaveDriverOptions(void);

		// Persistance
		void Save(CTreeFile &Tree);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT    m_Drop;
		CString m_Offset;
		UINT    m_Type;
		UINT    m_Restrict;
		UINT    m_Increment;
		UINT    m_Entry;
		UINT    m_NodeGuard;
		CString m_File;
		UINT    m_Update;
				
	protected:
		// Data
		CString m_Objects;
		
		// Meta Data Creation
		void AddMetaData(void);

		// EDS File
		void CreateEDSFile(void);
		void WriteFileInfo(ITextStream &Stream);
	virtual	void WriteDeviceInfo(ITextStream &Stream);
		void WriteDummyUsage(ITextStream &Stream);
		void WriteObjects(ITextStream &Stream);
		void WriteComments(ITextStream &Stream);
		void WriteMandatoryObjects(ITextStream &Stream);
	virtual	void WriteOptionalObjects(ITextStream &Stream);
		void WriteManufacturerObjects(ITextStream &Stream);
		void WriteObject(ITextStream &Stream, CString Name, CString Desc);
		void WriteMandatoryObject(ITextStream &Stream, CString Name, CString Desc);
		BOOL WriteObjectWithSubIndexes(ITextStream &Stream, CString Name, CString Desc);
		void WriteMandatoryObjectWithSubIndexes(ITextStream &Stream, CString Name, CString Desc);
		BOOL WriteFile(HANDLE hFile, PCTXT pText);
		BOOL WriteFile(HANDLE hFile, CString Text);
		BOOL WriteFile(HANDLE hFile, PBYTE pData, DWORD uLen);
	virtual	BOOL GetObjects(void);

		// Helpers
		void GetObject(CTreeFile &Tree, UINT &uObject);
		void GetCollect(CTreeFile &Tree, UINT &uCollect, UINT &uObject);
		void GetNext(CTreeFile &Tree, UINT &uObject, UINT &uCollect);
		void GetNext(CTreeFile &Tree, UINT &uObject, UINT &uCollect, UINT uSize);
		void End(CTreeFile &Tree, UINT &uObject, UINT &uCollect);
		
		CString GetName(CString Name);
		CString GetDesc(CString Name);
		CString GetObjectType(CString Name);
		CString GetDataType(CString Name);
		CString GetAccessType(CString Name);
		UINT    GetSize(CString Name);
		BOOL	IsObjectValid(UINT uType, UINT uIndex);
	virtual	BOOL	IsObjectMandatory(UINT uIndex, UINT uType);
		CString GetMandatoryObjects(UINT uType, UINT &uCount);
		CString GetRemainingObjects(UINT uType, UINT &uCount);
	virtual BOOL	IsPDO(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen Space Wrapper
//

class CSpaceCAN : public CSpace
{
	public:
		// Constructors
		CSpaceCAN(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen Address Selection
//

class CCANOpenDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CCANOpenDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Driver Access
		void SetSpace(CSpace * pSpace);
		                
	protected:

		// Message Map
		AfxDeclareMessageMap();

		// Notification Handlers
		void OnSpaceChange(UINT uID, CWnd &Wnd);

		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

// End of File

#endif
