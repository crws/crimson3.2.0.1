
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_S7PPI_HPP
	
#define	INCLUDE_S7PPI_HPP

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via PPI
//

class CS7PPIDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CS7PPIDriver(void);

		// Binding
		void GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via PPI Revision 2
//

class CS7PPI2Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CS7PPI2Driver(void);

		// Binding
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL CheckAlignment(CSpace *pSpace);
		
	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
