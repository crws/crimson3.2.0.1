
#include "intern.hpp"

#include "wifi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// WiFi Station Driver
//

// Instantiator

ICommsDriver *	Create_WifiStationDriver(void)
{
	return New CWifiStationDriver;
	} 

// Constructor

CWifiStationDriver::CWifiStationDriver(void)
{
	m_wID	       = WORD(0xFF10);

	m_uType	       = driverWifi;
				      	
	m_Manufacturer = "<System>";	
	
	m_DriverName   = IDS("Wi-Fi Client");
	
	m_Version      = "1.00";
	
	m_ShortName    = "Client";

	m_fSingle      = TRUE;	

	m_Binding      = bindWifi;

	C3_PASSED();
	}
	
// Binding Control

UINT CWifiStationDriver::GetBinding(void)
{
	return m_Binding;
	}

// Configuration

CLASS CWifiStationDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CWifiStationDriverOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// WiFi Station Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CWifiStationDriverOptions, CUIItem);

// Constructor

CWifiStationDriverOptions::CWifiStationDriverOptions(void)
{	
	m_Type = 1;

	m_Mode = 1;

	m_Addr = UINT(MAKELONG(MAKEWORD(20,  3), MAKEWORD(168, 192)));

	m_Mask = UINT(MAKELONG(MAKEWORD(0, 255), MAKEWORD(255, 255)));

	m_Gate = 0;
	}

// UI Management

void CWifiStationDriverOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		DoEnables(pHost);
		}
	}

// Download Support

BOOL CWifiStationDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Type));
	Init.AddData(m_Ssid, 64);
	Init.AddData(m_Pass, 32);
	
	if( !m_Mode ) {

		Init.AddByte(0);
		Init.AddLong(m_Addr);
		Init.AddLong(m_Mask);
		Init.AddLong(m_Gate);
		}
	else {
		Init.AddByte(1);
		}

	return TRUE;
	}

// Meta Data Creation

void CWifiStationDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Type);
	Meta_AddString(Ssid);
	Meta_AddString(Pass);
	Meta_AddInteger(Mode);
	Meta_AddInteger(Addr);
	Meta_AddInteger(Mask);
	Meta_AddInteger(Gate);
	}

// Implementation

void CWifiStationDriverOptions::DoEnables(IUIHost *pHost)
{
	BOOL fStation  = TRUE;

	BOOL fPassword = TRUE;

	BOOL fManual   = (m_Mode == 0);    

	pHost->EnableUI(this, "Ssid", fStation);
	pHost->EnableUI(this, "Pass", fPassword);
	pHost->EnableUI(this, "Addr", fManual);
	pHost->EnableUI(this, "Mask", fManual);
	pHost->EnableUI(this, "Gate", fManual);
	}

// End of File
