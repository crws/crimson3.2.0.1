
#include "intern.hpp"

#include "mlinka.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK A Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSEWMovilinkADeviceOptions, CUIItem);
				   
// Constructor

CSEWMovilinkADeviceOptions::CSEWMovilinkADeviceOptions(void)
{
	m_Drop		= 0;

	m_Device	= 0;

	m_Broadcast	= 0;

	}

// UI Managament

void CSEWMovilinkADeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag == "Broadcast" || Tag.IsEmpty() ) {

		pWnd->EnableUI("Drop", !(pItem->GetDataAccess("Broadcast")->ReadInteger(pItem)));
		}
	}

// Download Support

BOOL CSEWMovilinkADeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_Device));

	Init.AddByte(BYTE(m_Broadcast));
	
	return TRUE;
	}

// Meta Data Creation

void CSEWMovilinkADeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Device);

	Meta_AddInteger(Broadcast);
			
	}

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK B Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSEWMovilinkBDeviceOptions, CSEWMovilinkADeviceOptions);
				   
// Constructor

CSEWMovilinkBDeviceOptions::CSEWMovilinkBDeviceOptions(void)
{
	m_Drop		= 0;

	m_Device	= 0;

	m_Broadcast	= 0;

	}



//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK A Driver
//

// Instantiator

ICommsDriver *	Create_SEWMovilinkASerialDriver(void)
{
	return New CSEWMovilinkASerialDriver;
	}

// Constructor

CSEWMovilinkASerialDriver::CSEWMovilinkASerialDriver(void)
{
	m_wID		= 0x338B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SEW";
			
	m_DriverName	= "MOVILINK A Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "MOVILINK A";	

	AddSpaces();

	}

// Binding Control

UINT CSEWMovilinkASerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSEWMovilinkASerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CSEWMovilinkASerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSEWMovilinkADeviceOptions);
	}



// Implementation     

void CSEWMovilinkASerialDriver::AddSpaces(void)
{
	AddSpace(New CSpaceMovilink(SEW_ID_INDX,SEW_INDEX,"I",	addrLongAsLong,	"Index 8300 - 25000",			8300, 25000));
	AddSpace(New CSpaceMovilink(SEW_ID_IPOS,SEW_IPOS, "H",	addrLongAsLong,	"IPOS Variable",			0,    511));
	AddSpace(New CSpaceMovilink(SEW_ID_PD,	0xFFC0,	 "PD",	addrLongAsLong,	"Process Output Data",			1,	3));
	AddSpace(New CSpaceMovilink(SEW_ID_SEND,0xFFD0, "SPD",	addrLongAsLong,	"Send Process Output Data",		1,	3));
	AddSpace(New CSpaceMovilink(addrNamed,	0x207E, "000",	addrLongAsLong,	"Speed [rpm x 1000]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2135,	"001",	addrLongAsLong,	"User Display [   ]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x207F, "002",	addrLongAsLong,	"Frequency [Hz]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2080, "003",	addrLongAsLong,	"Actual Position",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2081,	"004",	addrLongAsLong,	"Output Current [% x 1000]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2082, "005",	addrLongAsLong,	"Active Current [% x 1000]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2083,	"006",	addrLongAsLong,	"Motor Utilization 1 [% x 1000]",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed, 	0x2084,	"007",	addrLongAsLong,	"Motor Utilization 2 [% x 1000]",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2085,	"008",	addrLongAsLong,	"DC Link Voltage [V]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2086,	"009",	addrLongAsLong,	"Output Current [A]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"010",	addrLongAsLong,	"Inverter Status",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"011",	addrLongAsLong,	"Operational Status",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"012",	addrLongAsLong,	"Error Status",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"013",	addrLongAsLong,	"Active Parameter Set",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2087,	"014",	addrLongAsLong,	"Heat Sink Temperature [C]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2088,	"015",	addrLongAsLong,	"Mains ON Operation Time [ms]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2089,	"016",	addrLongAsLong,	"Operating Time (Enabled) [ms]",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208A,	"017",	addrLongAsLong,	"Electrical Energy [Ws]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208B,	"020",	addrLongAsLong,	"Analog Input AI1 [V]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208C,	"021",	addrLongAsLong,	"Analog Input AI2 [V]",			0,0, TRUE));
    	AddSpace(New CSpaceMovilink(addrNamed,	0x208D,	"022",	addrLongAsLong,	"External Current Limit [% x 1000]",	0,0, TRUE));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x208E,	"030",	addrLongAsLong,	"Binary Input DI00",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208F,	"031",	addrLongAsLong,	"Binary Input DI01"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2090,	"032",	addrLongAsLong,	"Binary Input DI02"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2091,	"033",	addrLongAsLong,	"Binary Input DI03"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2092,	"034",	addrLongAsLong,	"Binary Input DI04"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2093,	"035",	addrLongAsLong,	"Binary Input DI05"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208E,	"036",	addrLongAsLong,	"Status Binary Inputs (DI00 - DI05)",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2094,	"040",	addrLongAsLong,	"Binary Input DI10"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2095,	"041",	addrLongAsLong,	"Binary Input DI11"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2096,	"042",	addrLongAsLong,	"Binary Input DI12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2097,	"043",	addrLongAsLong,	"Binary Input DI13"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2098,	"044",	addrLongAsLong,	"Binary Input DI14"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2099,	"045",	addrLongAsLong,	"Binary Input DI15"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209A,	"046",	addrLongAsLong,	"Binary Input DI16"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209B,	"047",	addrLongAsLong,	"Binary Input DI17"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209C,	"048",	addrLongAsLong,	"Staus Binary Inputs (DI10 - DI17)",	0,0, TRUE));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x209D, "050",	addrLongAsLong,	"Binary Output DB00",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209E,	"051",	addrLongAsLong,	"Binary Output DO01"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209F,	"052",	addrLongAsLong,	"Binary Output DO02"));
       	AddSpace(New CSpaceMovilink(addrNamed,	0x209D,	"053",	addrLongAsLong,	"Status Binary Outputs (DB00, DO01, DO02)",0,0,TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A0,	"060",	addrLongAsLong,	"Binary Output DO10"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A1,	"061",	addrLongAsLong,	"Binary Output DO11"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A2,	"062",	addrLongAsLong,	"Binary Output DO12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A3,	"063",	addrLongAsLong,	"Binary Output DO13"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A4,	"064",	addrLongAsLong,	"Binary Output DO14"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A5,	"065",	addrLongAsLong,	"Binary Output DO15"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A6,	"066",	addrLongAsLong,	"Binary Output DO16"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A7,	"067",	addrLongAsLong,	"Binary Output DO17"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A8,	"068",	addrLongAsLong,	"Status Binary Outputs (DO10 - DO17)",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A9,	"071",	addrLongAsLong,	"Unit Rated Current [A]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AA,	"072",	addrLongAsLong,	"Option 1",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AB,	"073",	addrLongAsLong,	"Option 2",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AC,	"074",	addrLongAsLong,	"Firmware Option 1",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AD,	"075",	addrLongAsLong,	"Firmware Option 2",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x206C,	"076",	addrLongAsLong,	"Firmware Basic Unit",			0,0, TRUE));
     	AddSpace(New CSpaceMovilink(addrNamed,	0x22AE,	"077",	addrLongAsLong,	"Technology Function"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AE,	"080",	addrLongAsLong,	"Error t-0",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AF,	"081",	addrLongAsLong,	"Error t-1",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20B0,	"082",	addrLongAsLong,	"Error t-2",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20B1,	"083",	addrLongAsLong,	"Error t-3",				0,0, TRUE));
       	AddSpace(New CSpaceMovilink(addrNamed,	0x20B2,	"084",	addrLongAsLong,	"Error t-4",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2103,	"090",	addrLongAsLong,	"PD Configuration",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2104,	"091",	addrLongAsLong,	"Fieldbus Type",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2105,	"092",	addrLongAsLong,	"Fieldbus Baud Rate"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2106,	"093",	addrLongAsLong,	"Fieldbus Address"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2107,	"094",	addrLongAsLong,	"PO1 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2108,	"095",	addrLongAsLong,	"PO2 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2109,	"096",	addrLongAsLong,	"PO3 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210A,	"097",	addrLongAsLong,	"PI1 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210B,	"098",	addrLongAsLong,	"PI2 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210C,	"099",	addrLongAsLong,	"PI3 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210D, "100",	addrLongAsLong,	"Setpoint Source"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210E,	"101",	addrLongAsLong,	"Control Signal Source"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210F, "110",	addrLongAsLong,	"AI1 Scaling"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2110,	"111",	addrLongAsLong,	"AI1 Offset [V]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2111,	"112",	addrLongAsLong,	"AI1 Operation Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2112,	"113",	addrLongAsLong,	"AI1 Voltage Offset [V]"));
       	AddSpace(New CSpaceMovilink(addrNamed,	0x2113,	"114",	addrLongAsLong,	"AI1 Speed Offset [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2114,	"115",	addrLongAsLong,	"Filter Setpoint [ms x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2115,	"120",	addrLongAsLong,	"AI2 Operation Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x226B,	"121",	addrLongAsLong,	"AI2 Additional Setpoint Potentiometer"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x225F,	"122",	addrLongAsLong,	"AI2 Local Potentiometer Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2116,	"130",	addrLongAsLong,	"Ramp t11 UP CW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2117,	"131",	addrLongAsLong,	"Ramp t11 DOWN CW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2118,	"132",	addrLongAsLong,	"Ramp t11 UP CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2119,	"133",	addrLongAsLong,	"Ramp t11 DOWN CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211A,	"134",	addrLongAsLong,	"Ramp t12 UP=DOWN [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211B,	"135",	addrLongAsLong,	"S Pattern t12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211C,	"136",	addrLongAsLong,	"Stop Ramp t13 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211D,	"137",	addrLongAsLong,	"Emergency Ramp t14 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x225A,	"138",	addrLongAsLong,	"Ramp Limit"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211E,	"140",	addrLongAsLong,	"Ramp t21 UP CW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211F,	"141",	addrLongAsLong,	"Ramp t21 DOWN CW [ms]"));
    	AddSpace(New CSpaceMovilink(addrNamed,	0x2120,	"142",	addrLongAsLong,	"Ramp t21 UP CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2121,	"143",	addrLongAsLong,	"Ramp t21 DOWN CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2122,	"144",	addrLongAsLong,	"Ramp t22 UP=DOWN [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2123,	"145",	addrLongAsLong,	"S Pattern t22"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2124,	"146",	addrLongAsLong,	"Stop Ramp t23 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2125,	"147",	addrLongAsLong,	"Emergency ramp t24 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2126,	"150",	addrLongAsLong,	"Ramp t3 UP [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2127,	"151",	addrLongAsLong,	"Ramp t3 DOWN [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2128,	"152",	addrLongAsLong,	"Save Last Setpoint"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2129,	"160",	addrLongAsLong,	"Internal Setpoint n11 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212A,	"161",	addrLongAsLong,	"Internal Setpoint n12 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212B,	"162",	addrLongAsLong,	"Internal Setpoint n13 [rpm x 1000]"));
     	AddSpace(New CSpaceMovilink(addrNamed,	0x226E,	"163",	addrLongAsLong,	"Internal Setpoint n11 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x226F,	"164",	addrLongAsLong,	"Internal Setpoint n12 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2270,	"165",	addrLongAsLong,	"Internal Setpoint n13 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212C,	"170",	addrLongAsLong,	"Internal Setpoint n21 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212D,	"171",	addrLongAsLong,	"Internal Setpoint n22 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212E,	"172",	addrLongAsLong,	"Internal Setpoint n23 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2271,	"173",	addrLongAsLong,	"Internal Setpoint n21 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2272,	"174",	addrLongAsLong,	"Internal Setpoint n22 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2273,	"175",	addrLongAsLong,	"Internal Setpoint n23 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212F,	"200",	addrLongAsLong,	"P-Gain Speed Controller"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2130,	"201",	addrLongAsLong,	"Time Constant n-control [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2131,	"202",	addrLongAsLong,	"Gain Accel. Precontrol"));
     	AddSpace(New CSpaceMovilink(addrNamed,	0x2132,	"203",	addrLongAsLong,	"Filter Accel. Precontrol [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2133,	"204",	addrLongAsLong,	"Filter Speed Actual Value [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20F4,	"205",	addrLongAsLong,	"Load Precontrol [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20F5,	"206",	addrLongAsLong,	"Sample Time n-control"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2252,	"207",	addrLongAsLong,	"Load Precontrol VFC [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2134,	"210",	addrLongAsLong,	"P-Gain Hold Controller"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213D,	"220",	addrLongAsLong,	"P-Gain (DRS)"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2136,	"221",	addrLongAsLong,	"Master Gear Ratio Factor"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2137,	"222",	addrLongAsLong,	"Slave Gear Ratio Factor"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2138,	"223",	addrLongAsLong,	"Mode Selection"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2139,	"224",	addrLongAsLong,	"Slave Counter [Inc]"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x213A,	"225",	addrLongAsLong,	"Offset 1 [Inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213B,	"226",	addrLongAsLong,	"Offset 2 [Inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213C,	"227",	addrLongAsLong,	"Offset 3 [Inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20F6, "228",	addrLongAsLong,	"Precontrol Filter (DRS)"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213E,	"230",	addrLongAsLong,	"Synchronous Encoder"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213F,	"231",	addrLongAsLong,	"Factor Slave Encoder"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2140,	"232",	addrLongAsLong,	"Factor Slave Sync. Encoder"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2141,	"240",	addrLongAsLong,	"Synchronization Speed [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2142,	"241",	addrLongAsLong,	"Synchronization Ramp [ms]"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x2260,	"250",	addrLongAsLong,	"PI-controller"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2261,	"251",	addrLongAsLong,	"P-gain"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2262,	"252",	addrLongAsLong,	"I-component"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2111,	"253",	addrLongAsLong,	"PI Actual Value Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210F,	"254",	addrLongAsLong,	"PI Actual Value Scaling"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x226C,	"255",	addrLongAsLong,	"PI Actual Value Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2143,	"300",	addrLongAsLong,	"Start/Stop Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2144,	"301",	addrLongAsLong,	"Minimum Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2145,	"302",	addrLongAsLong,	"Maximum Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2146,	"303",	addrLongAsLong,	"Current Limit 1 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21F0,	"304",	addrLongAsLong,	"Torque Limit [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2147,	"310",	addrLongAsLong,	"Start/Stop Speed 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2148,	"311",	addrLongAsLong,	"Minimum Speed 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2149,	"312",	addrLongAsLong,	"Maximum Speed 2 [rpm x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x214A,	"313",	addrLongAsLong,	"Current Limit 2 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214B,	"320",	addrLongAsLong,	"Automatic Adjustment 1"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x214C,	"321",	addrLongAsLong,	"Boost 1 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214D,	"322",	addrLongAsLong,	"IxR Compensation 1 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214E, "323",	addrLongAsLong,	"Premagnetizing Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214F,	"324",	addrLongAsLong,	"Slip Compensation 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2282,	"325",	addrLongAsLong,	"No-load Vibration Control"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2150,	"330",	addrLongAsLong,	"Automatic Adjustment 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2151,	"331",	addrLongAsLong,	"Boost 2 [V]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2152,	"332",	addrLongAsLong,	"IxR Compensation 2 [V]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2153,	"333",	addrLongAsLong,	"Premagnetizing Time 2 [ms]"));
    	AddSpace(New CSpaceMovilink(addrNamed,	0x2154,	"334",	addrLongAsLong,	"Slip Compensation 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2155,	"340",	addrLongAsLong,	"Motor Protection 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2156, "341",	addrLongAsLong,	"Cooling Type 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2157,	"342",	addrLongAsLong,	"Motor Protection 2"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x2158,	"343",	addrLongAsLong,	"Cooling Type 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2159,	"350",	addrLongAsLong,	"Change Direction of Rotation 1"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x215A,	"351",	addrLongAsLong,	"Change Direction of Rotation 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215B,	"400",	addrLongAsLong,	"Speed Reference Value [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215C,	"401",	addrLongAsLong,	"Speed Reference Hysteresis [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215D,	"402",	addrLongAsLong,	"Speed Reference Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215E,	"403",	addrLongAsLong,	"Speed Reference Signal"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215F,	"410",	addrLongAsLong,	"Speed Window Center [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2160,	"411",	addrLongAsLong,	"Speed Window Range Width [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2161,	"412",	addrLongAsLong,	"Speed Window Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2162,	"413",	addrLongAsLong,	"Speed Window Signal"));
   	AddSpace(New CSpaceMovilink(addrNamed,	0x2163,	"420",	addrLongAsLong,	"Speed Comparator Hysteresis [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2164,	"421",	addrLongAsLong,	"Speed Comparator Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2165,	"422",	addrLongAsLong,	"Speed Comparator Signal"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2166,	"430",	addrLongAsLong,	"Current Reference Value [% x 1000]"));
    	AddSpace(New CSpaceMovilink(addrNamed,	0x2167,	"431",	addrLongAsLong,	"Current Reference Hysteresis [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2168,	"432",	addrLongAsLong,	"Current Reference Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2169,	"433",	addrLongAsLong,	"Current Reference Signal"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216A,	"440",	addrLongAsLong,	"Imax Signal Hysteresis [% x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x216B,	"441",	addrLongAsLong,	"Imax Signal Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216C,	"442",	addrLongAsLong,	"Imax Signal"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x226D,	"450",	addrLongAsLong,	"PI Actual Value Threshold"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216D,	"500",	addrLongAsLong,	"Speed Monitoring 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216E,	"501",	addrLongAsLong,	"Speed Monitoring Delay Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216F,	"502",	addrLongAsLong,	"Speed Monitoring 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2170,	"503",	addrLongAsLong,	"Speed Monitoring Delay Time 2 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2171,	"510",	addrLongAsLong,	"Positioning Tolerance Slave"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2172,	"511",	addrLongAsLong,	"Prewarning Lag Error"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2173,	"512",	addrLongAsLong,	"Lag Error Limit"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2174,	"513",	addrLongAsLong,	"Delay Lag Error Signal [ms]"));
       	AddSpace(New CSpaceMovilink(addrNamed,	0x2175,	"514",	addrLongAsLong,	"Counter LED Display"));
    	AddSpace(New CSpaceMovilink(addrNamed,	0x2176,	"515",	addrLongAsLong,	"Delay In-Position Signal [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2177,	"520",	addrLongAsLong,	"Mains OFF Response Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2231,	"521",	addrLongAsLong,	"Mains OFF Response"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x208F,	"600",	addrLongAsLong,	"Binary Input DI01"));
  	//AddSpace(New CSpaceMovilink(addrNamed,	0x2090,	"601",	addrLongAsLong,	"Binary Input DI02"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2091,	"602",	addrLongAsLong,	"Binary Input DI03"));
      	//AddSpace(New CSpaceMovilink(addrNamed,	0x2092,	"603",	addrLongAsLong,	"Binary Input DI04"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2093,	"604",	addrLongAsLong,	"Binary Input DI05"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2094,	"610",	addrLongAsLong,	"Binary Input DI10"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2095,	"611",	addrLongAsLong,	"Binary Input DI11"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2096,	"612",	addrLongAsLong,	"Binary Input DI12"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2097,	"613",	addrLongAsLong,	"Binary Input DI13"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2098,	"614",	addrLongAsLong,	"Binary Input DI14"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2099,	"615",	addrLongAsLong,	"Binary Input DI15"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x209A,	"616",	addrLongAsLong,	"Binary Input DI16"));
     	//AddSpace(New CSpaceMovilink(addrNamed,	0x209B,	"617",	addrLongAsLong,	"Binary Input DI17"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x209E,	"620",	addrLongAsLong,	"Binary Output DO01"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x209F,	"621",	addrLongAsLong,	"Binary Output DO02"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A0,	"630",	addrLongAsLong,	"Binary Output DO10"));
  	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A1,	"631",	addrLongAsLong,	"Binary Output DO11"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A2,	"632",	addrLongAsLong,	"Binary Output DO12"));
      	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A3,	"633",	addrLongAsLong,	"Binary Output DO13"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A4,	"634",	addrLongAsLong,	"Binary Output DO14"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A5,	"635",	addrLongAsLong,	"Binary Output DO15"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A6,	"636",	addrLongAsLong,	"Binary Output DO16"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x20A7,	"637",	addrLongAsLong,	"Binary Output DO17"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2178,	"640",	addrLongAsLong,	"Analog Output AO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2179,	"641",	addrLongAsLong,	"Analog Scaling AO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217A,	"642",	addrLongAsLong,	"Operating Mode AO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217B,	"643",	addrLongAsLong,	"Analog Output AO2"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x217C,	"644",	addrLongAsLong,	"Analog Scaling AO2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217D,	"645",	addrLongAsLong,	"Operating Mode AO2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217E,	"700",	addrLongAsLong,	"Operating Mode 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217F,	"701",	addrLongAsLong,	"Operating Mode 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2180,	"710",	addrLongAsLong,	"Standstill Current 1 [A]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2181,	"711",	addrLongAsLong,	"Standstill Current 2 [A]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2182,	"720",	addrLongAsLong,	"Setpoint Stop Function 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2183,	"721",	addrLongAsLong,	"Stop Setpoint 1 [rpm x 1000]"));
     	AddSpace(New CSpaceMovilink(addrNamed,	0x2184,	"722",	addrLongAsLong,	"Start Offset 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2185,	"723",	addrLongAsLong,	"Setpoint Stop Function 2"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x2186,	"724",	addrLongAsLong,	"Stop Setpoint 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2187,	"725",	addrLongAsLong,	"Start Offset 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2188,	"730",	addrLongAsLong,	"Brake Function 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222D,	"731",	addrLongAsLong,	"Brake Release Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2189,	"732",	addrLongAsLong,	"Brake Apply Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218A,	"733",	addrLongAsLong,	"Brake Function 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222E,	"734",	addrLongAsLong,	"Brake Release Time 2 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218B,	"735",	addrLongAsLong,	"Brake Apply Time 2 [ms]"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x227C,	"736",	addrLongAsLong,	"Brake Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218C,	"740",	addrLongAsLong,	"Speed Skip Window Center 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218D,	"741",	addrLongAsLong,	"Speed Skip Width 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218E,	"742",	addrLongAsLong,	"Speed Skip Window Center 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218F,	"743",	addrLongAsLong,	"Speed Skip Width 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2190,	"750",	addrLongAsLong,	"Slave Setpoint"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2191,	"751",	addrLongAsLong,	"Scaling Slave Setpoint"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x225E,	"760",	addrLongAsLong,	"Lockout Run/Stop Buttons"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2192,	"802",	addrLongAsLong,	"Factory Setting"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2193,	"803",	addrLongAsLong,	"Parameter Lock"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2194,	"804",	addrLongAsLong,	"Reset Statistic Data"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2195,	"810",	addrLongAsLong,	"RS-485 Address"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2196,	"811",	addrLongAsLong,	"RS-485 Group Address"));
   	AddSpace(New CSpaceMovilink(addrNamed,	0x2197, "812",	addrLongAsLong,	"RS-485 Timeout Delay [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2198,	"813",	addrLongAsLong,	"SBus Address"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2199,	"814",	addrLongAsLong,	"SBus Group Address"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219A,	"815",	addrLongAsLong,	"SBus Timeout Delay [ms]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x219B,	"816",	addrLongAsLong,	"SBus Baud Rate"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219C,	"817",	addrLongAsLong,	"SBus Synchronization ID"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x221C,	"818",	addrLongAsLong,	"CAN Synchronizatio ID"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219E,	"819",	addrLongAsLong,	"Fieldbus Timeout Delay [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219F,	"820",	addrLongAsLong,	"Brake 4-quadrant Operation 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A0,	"821",	addrLongAsLong,	"Brake 4-quadrant Operation 2"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21A1,	"830",	addrLongAsLong,	"Fault Response EXT.FAULT"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A2,	"831",	addrLongAsLong,	"Fault Response FIELDBUS TIMEOUT"));
   	AddSpace(New CSpaceMovilink(addrNamed,	0x21A3,	"832",	addrLongAsLong,	"Fault Response MOTOR OVERLOAD"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A4,	"833",	addrLongAsLong,	"Fault Response RS-485 TIMEOUT"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A5,	"834",	addrLongAsLong,	"Fault Response DRS LAG ERROR"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A8,	"835",	addrLongAsLong,	"Fault Response TF Sensor SIGNAL"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21A7,	"836",	addrLongAsLong,	"Fault Response SBus TIMEOUT"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A9,	"840",	addrLongAsLong,	"Manual Reset Response"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AA,	"841",	addrLongAsLong,	"Auto Reset Response"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AB,	"842",	addrLongAsLong,	"Restart Time Response [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222B,	"850",	addrLongAsLong,	"Speed Scaling Factor Numerator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222C,	"851",	addrLongAsLong,	"Speed Scaling Factor Denominator"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x2244,	"852",	addrLongAsLong,	"User Dimension"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AC,	"860",	addrLongAsLong,	"PWM Frequency 1 [kHz]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AD,	"861",	addrLongAsLong,	"PWM Frequency 2 [kHz]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222F,	"862",	addrLongAsLong,	"PWM Fix 1"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x2230,	"863",	addrLongAsLong,	"PWM Fix 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2070,	"870",	addrLongAsLong,	"Setpoint Description PO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2071,	"871",	addrLongAsLong,	"Setpoint Description PO2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2072,	"872",	addrLongAsLong,	"Setpoint Description PO3"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2073,	"873",	addrLongAsLong,	"Actual Value Description PI1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2074,	"874",	addrLongAsLong,	"Actual Value Description PI2"));
     	AddSpace(New CSpaceMovilink(addrNamed,	0x2075,	"875",	addrLongAsLong,	"Actual Value Description PI3"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21AE,	"876",	addrLongAsLong,	"PO Data Enable"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AF,	"900",	addrLongAsLong,	"IPOS Reference Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B0,	"901",	addrLongAsLong,	"IPOS Reference Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B1,	"902",	addrLongAsLong,	"IPOS Reference Speed 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B2,	"903",	addrLongAsLong,	"IPOS Reference Travel Type"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B3,	"910",	addrLongAsLong,	"IPOS Gain X-controller"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x21B4,	"911",	addrLongAsLong,	"IPOS Positioning Ramp 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21F8,	"912",	addrLongAsLong,	"IPOS Positionint Ramp 2 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B5,	"913",	addrLongAsLong,	"IPOS Travelling Speed CW [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B6,	"914",	addrLongAsLong,	"IPOS Travelling Speed CCW [rpm x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21B7,	"915",	addrLongAsLong,	"IPOS Speed Feedforward"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B8,	"916",	addrLongAsLong,	"IPOS Ramp Type"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B9,	"920",	addrLongAsLong,	"IPOS SW Limit Switch CW"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BA,	"921",	addrLongAsLong,	"IPOS SW Limit Switch CCW"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BB, "922",	addrLongAsLong,	"IPOS Positioning Window"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BC,	"923",	addrLongAsLong,	"IPOS Lag Error Window"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BD, "930",	addrLongAsLong,	"IPOS Override"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2219,	"941",	addrLongAsLong,	"IPOS Source Actual Position"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2246,	"942",	addrLongAsLong,	"IPOS Encoder Factor Numerator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2247,	"943",	addrLongAsLong,	"IPOS Encoder Factor Denominator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2253,	"944",	addrLongAsLong,	"IPOS Encoder Scaling Ext. Encoder"));
      	AddSpace(New CSpaceMovilink(addrNamed,	0x2288,	"945",	addrLongAsLong,	"IPOS Encoder Type (X14)"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2249,	"950",	addrLongAsLong,	"DIP Encoder Type"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2248,	"951",	addrLongAsLong,	"DIP Counting Direction"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x224A,	"952",	addrLongAsLong,	"DIP Cycle Frequency [% x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x224B,	"953",	addrLongAsLong,	"DIP Position Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x224D,	"954",	addrLongAsLong,	"DIP Zero Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2250,	"955",	addrLongAsLong,	"DIP Encoder Scaling"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2283,	"960",	addrLongAsLong,	"IPOS Modulo Function"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2284,	"961",	addrLongAsLong,	"IPOS Modulo Numerator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2285,	"962",	addrLongAsLong,	"IPOS Modulo Denominator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2286,	"963",	addrLongAsLong,	"IPOS Modulo Encoder Resolution"));
      	
	
	AddSpace(New CSpaceMovilink(addrNamed,	0xFFFF,	"9999",	addrLongAsLong,	"Latest Exception Code",		0,0, TRUE));  
	}


// Address Management

BOOL CSEWMovilinkASerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CSEWMovilinkAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CSEWMovilinkASerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uExtra = 0;

	if( StripType(pSpace, Text) == "LONG" ) {

		UINT uSuffix = Text.Find('-');

		uExtra = GetSuffix(Text.Mid(uSuffix));

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text.Left(uSuffix)) ) {

			if ( pSpace->m_uTable == addrNamed ) {

				Addr.a.m_Offset = GetOffset(pSpace);
				}

			else if ( pSpace->m_uTable == SEW_ID_IPOS ) {

				Addr.a.m_Offset += SEW_IPOS_OFFSET;
				}
			
			Addr.a.m_Extra  = uExtra;

			// Handle Status Word Special Case !!

			if( !Addr.a.m_Extra && Addr.a.m_Offset == 0x2076 ) {

				uExtra = tstrtol(Text, NULL, 10);

				Addr.a.m_Extra = uExtra - 10;
				}

			return TRUE;
			}
		}
	
	Error.Set( CString(IDS_DRIVER_ADDR_INVALID), 0 );

	return FALSE;
	}

BOOL CSEWMovilinkASerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr) ) {

		// Handle Status Word Special Case !!

		if( Text == "010" && Addr.a.m_Extra ) {

			UINT uText = tstrtol(Text, NULL, 10);

			uText += Addr.a.m_Extra;

			Text = "0";

			Text.Printf(Text + "%u", uText);
			} 
		
		else {	
			// Adjust IPOS (H) so display is correct !!

			if( Text.Left(1) == "H" ) {

				UINT uVar = tstrtol(Text.Mid(1), NULL, 10);

				if( uVar >= 1000 ) {

					if( GetSuffix(Addr.a.m_Table, Addr.a.m_Extra) != "" ) {

						uVar -=	1000;
						}
				
					Text = "H";

					Text.Printf(Text + "%4.4u", uVar);
					}
				} 

			Text += GetSuffix(Addr.a.m_Table, Addr.a.m_Extra);
			}
					
		return TRUE;
		}

	return FALSE;
	
	}

CString CSEWMovilinkASerialDriver::GetSuffix(UINT uTable, UINT uValue)
{
	if( uTable == addrNamed || uTable == 1 || uTable == 2 ) {

		switch ( uValue ) {

			case 1:
				return "-RW";

			case 2:
				return "-RWV";

			}
		}

	if ( uTable == 4 && uValue == 1 ) {

		return "-C";
		}
	
	return "";
	}

UINT   CSEWMovilinkASerialDriver::GetSuffix(CString Text)
{
	if ( Text == "-RW" || Text == "-C" ) {

		return 1;
		}

	if ( Text == "-RWV" ) {

		return 2;
		}
	
	return 0;
	
	}

UINT  CSEWMovilinkASerialDriver::GetOffset(CSpace *pSpace)
{
	CSpaceMovilink * pSew = (CSpaceMovilink *)pSpace;

	return pSew->m_Index;
	
	}

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK B Driver
//

// Instantiator

ICommsDriver *	Create_SEWMovilinkBSerialDriver(void)
{
	return New CSEWMovilinkBSerialDriver;
	}

// Constructor

CSEWMovilinkBSerialDriver::CSEWMovilinkBSerialDriver(void)
{
	m_wID		= 0x4044;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SEW";
			
	m_DriverName	= "MOVILINK B Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MOVILINK B";	

	DeleteAllSpaces();

	AddSpaces();
	}

// Configuration

CLASS CSEWMovilinkBSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSEWMovilinkBDeviceOptions);
	}


// Implementation     

void CSEWMovilinkBSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpaceMovilink(SEW_ID_INDX,SEW_INDEX,"I",	addrLongAsLong,	"Index 8300 - 25000",			8300, 25000));
	AddSpace(New CSpaceMovilink(SEW_ID_IPOS,SEW_IPOS, "H",	addrLongAsLong,	"IPOS Variable",			0,   1200));
	AddSpace(New CSpaceMovilink(SEW_ID_PD,	0xFFC0,	 "PD",	addrLongAsLong,	"Process Output Data",			1,	3));
	AddSpace(New CSpaceMovilink(SEW_ID_SEND,0xFFD0, "SPD",	addrLongAsLong,	"Send Process Output Data",		1,	3));
	AddSpace(New CSpaceMovilink(addrNamed,	0x207E, "000",	addrLongAsLong,	"Speed [rpm x 1000]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2135,	"001",	addrLongAsLong,	"User Display [   ]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x207F, "002",	addrLongAsLong,	"Frequency [Hz]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2080, "003",	addrLongAsLong,	"Actual Position",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2081,	"004",	addrLongAsLong,	"Output Current [% x 1000]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2082, "005",	addrLongAsLong,	"Active Current [% x 1000]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2083,	"006",	addrLongAsLong,	"Motor Utilization 1 [% x 1000]",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed, 	0x2084,	"007",	addrLongAsLong,	"Motor Utilization 2 [% x 1000]",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2085,	"008",	addrLongAsLong,	"DC Link Voltage [V]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2086,	"009",	addrLongAsLong,	"Output Current [A]",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"010",	addrLongAsLong,	"Inverter Status",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"011",	addrLongAsLong,	"Operational Status",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"012",	addrLongAsLong,	"Error Status",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2076,	"013",	addrLongAsLong,	"Active Parameter Set",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2087,	"014",	addrLongAsLong,	"Heat Sink Temperature [C]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2088,	"015",	addrLongAsLong,	"Mains ON Operation Time [min]",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2089,	"016",	addrLongAsLong,	"Operating Time (Enabled) [min]",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208A,	"017",	addrLongAsLong,	"Electrical Energy [Ws]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2403, "018",  addrLongAsLong, "Motor Temperature 1 [%]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2404, "019",  addrLongAsLong, "Motor Temperature 1 [%]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208B,	"020",	addrLongAsLong,	"Analog Input AI1 [mV]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208C,	"021",	addrLongAsLong,	"Analog Input AI2 [mV]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208D,	"022",	addrLongAsLong,	"External Current Limit [% x 1000]",	0,0, TRUE));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x208E,	"030",	addrLongAsLong,	"Binary Input DI00",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x208F,	"031",	addrLongAsLong,	"Binary Input DI01"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2090,	"032",	addrLongAsLong,	"Binary Input DI02"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2091,	"033",	addrLongAsLong,	"Binary Input DI03"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2092,	"034",	addrLongAsLong,	"Binary Input DI04"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2093,	"035",	addrLongAsLong,	"Binary Input DI05"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22D7,	"036",	addrLongAsLong,	"Binary Input DI06"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22D8, "037",  addrLongAsLong, "Binary Input DI07"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x208E, "039",  addrLongAsLong, "Staus Binary Inputs (DI00 - DI07)",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2094,	"040",	addrLongAsLong,	"Binary Input DI10"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2095,	"041",	addrLongAsLong,	"Binary Input DI11"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2096,	"042",	addrLongAsLong,	"Binary Input DI12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2097,	"043",	addrLongAsLong,	"Binary Input DI13"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2098,	"044",	addrLongAsLong,	"Binary Input DI14"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2099,	"045",	addrLongAsLong,	"Binary Input DI15"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209A,	"046",	addrLongAsLong,	"Binary Input DI16"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209B,	"047",	addrLongAsLong,	"Binary Input DI17"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209C,	"048",	addrLongAsLong,	"Staus Binary Inputs (DI10 - DI17)",	0,0, TRUE));
//	AddSpace(New CSpaceMovilink(addrNamed,	0x209D, "050",	addrLongAsLong,	"Binary Output DB00",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209E,	"051",	addrLongAsLong,	"Binary Output DO01"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209F,	"052",	addrLongAsLong,	"Binary Output DO02"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22D4,	"053",	addrLongAsLong,	"Binary Output DO03"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22D5, "054",  addrLongAsLong, "Binary Output DO04"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22D6, "055",  addrLongAsLong, "Binary Output DO05"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x209D, "059",  addrLongAsLong, "Staus Binary Outputs (DO01 - DO05)",	0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A0,	"060",	addrLongAsLong,	"Binary Output DO10"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A1,	"061",	addrLongAsLong,	"Binary Output DO11"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A2,	"062",	addrLongAsLong,	"Binary Output DO12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A3,	"063",	addrLongAsLong,	"Binary Output DO13"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A4,	"064",	addrLongAsLong,	"Binary Output DO14"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A5,	"065",	addrLongAsLong,	"Binary Output DO15"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A6,	"066",	addrLongAsLong,	"Binary Output DO16"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A7,	"067",	addrLongAsLong,	"Binary Output DO17"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A8,	"068",	addrLongAsLong,	"Status Binary Outputs (DO10 - DO17)",	0,0, TRUE));
	//AddSpace(New CSpaceMovilink(addrNamed,  0x206D, "070",  addrLongAsLong, "Unit Type",				0,0, TRUE));	
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A9,	"071",	addrLongAsLong,	"Unit Rated Current [mA]",		0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22E2,	"072",	addrLongAsLong,	"Option Encoder",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22E3, "---",  addrLongAsLong, "Firmware Encoder",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AA,	"073",	addrLongAsLong,	"Option Fieldbus",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,  0x20AC, "---",  addrLongAsLong, "Firmware Fieldbus",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AB,	"074",	addrLongAsLong,	"Option Extension",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,  0x20AD, "---",	addrLongAsLong,	"Firmware Extension",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x206C,	"076",	addrLongAsLong,	"Firmware Basic Unit",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22AE,	"078",	addrLongAsLong,	"Technology Function",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22BA, "079",	addrLongAsLong, "Device Type",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AE,	"080",	addrLongAsLong,	"Error t-0",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20AF,	"081",	addrLongAsLong,	"Error t-1",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20B0,	"082",	addrLongAsLong,	"Error t-2",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20B1,	"083",	addrLongAsLong,	"Error t-3",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20B2,	"084",	addrLongAsLong,	"Error t-4",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2103,	"090",	addrLongAsLong,	"PD Configuration",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2104,	"091",	addrLongAsLong,	"Fieldbus Type",			0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2105,	"092",	addrLongAsLong,	"Fieldbus Baud Rate"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2106,	"093",	addrLongAsLong,	"Fieldbus Address"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2107,	"094",	addrLongAsLong,	"PO1 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2108,	"095",	addrLongAsLong,	"PO2 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2109,	"096",	addrLongAsLong,	"PO3 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210A,	"097",	addrLongAsLong,	"PI1 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210B,	"098",	addrLongAsLong,	"PI2 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210C,	"099",	addrLongAsLong,	"PI3 Setpoint",				0,0, TRUE));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210D, "100",	addrLongAsLong,	"Setpoint Source"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210E,	"101",	addrLongAsLong,	"Control Signal Source"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2288, "102",  addrLongAsLong, "Frequence Scaling [kHz]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x210F, "110",	addrLongAsLong,	"AI1 Scaling"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2110,	"111",	addrLongAsLong,	"AI1 Offset [V]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2111,	"112",	addrLongAsLong,	"AI1 Operation Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2112,	"113",	addrLongAsLong,	"AI1 Voltage Offset [V]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2113,	"114",	addrLongAsLong,	"AI1 Speed Offset [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2114,	"115",	addrLongAsLong,	"Filter Setpoint [ms x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2115,	"120",	addrLongAsLong,	"AI2 Operation Mode"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x226B,	"121",	addrLongAsLong,	"AI2 Additional Setpoint Potentiometer"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x225F,	"122",	addrLongAsLong,	"AI2 Local Potentiometer Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2116,	"130",	addrLongAsLong,	"Ramp t11 UP CW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2117,	"131",	addrLongAsLong,	"Ramp t11 DOWN CW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2118,	"132",	addrLongAsLong,	"Ramp t11 UP CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2119,	"133",	addrLongAsLong,	"Ramp t11 DOWN CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211A,	"134",	addrLongAsLong,	"Ramp t12 UP=DOWN [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211B,	"135",	addrLongAsLong,	"S Pattern t12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211C,	"136",	addrLongAsLong,	"Stop Ramp t13 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211D,	"137",	addrLongAsLong,	"Emergency Ramp t14 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x225A,	"138",	addrLongAsLong,	"Ramp Limit"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22E0, "139",	addrLongAsLong, "Ramp Monitoring 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211E,	"140",	addrLongAsLong,	"Ramp t21 UP CW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x211F,	"141",	addrLongAsLong,	"Ramp t21 DOWN CW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2120,	"142",	addrLongAsLong,	"Ramp t21 UP CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2121,	"143",	addrLongAsLong,	"Ramp t21 DOWN CCW [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2122,	"144",	addrLongAsLong,	"Ramp t22 UP=DOWN [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2123,	"145",	addrLongAsLong,	"S Pattern t22"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2124,	"146",	addrLongAsLong,	"Stop Ramp t23 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2125,	"147",	addrLongAsLong,	"Emergency ramp t24 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22E1, "149",	addrLongAsLong, "Ramp Monitoring 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2126,	"150",	addrLongAsLong,	"Ramp t3 UP [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2127,	"151",	addrLongAsLong,	"Ramp t3 DOWN [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2128,	"152",	addrLongAsLong,	"Save Last Setpoint"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2129,	"160",	addrLongAsLong,	"Internal Setpoint n11 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212A,	"161",	addrLongAsLong,	"Internal Setpoint n12 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212B,	"162",	addrLongAsLong,	"Internal Setpoint n13 [rpm x 1000]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x226E,	"163",	addrLongAsLong,	"Internal Setpoint n11 PI-controller [%x1000]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x226F,	"164",	addrLongAsLong,	"Internal Setpoint n12 PI-controller [%x1000]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2270,	"165",	addrLongAsLong,	"Internal Setpoint n13 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212C,	"170",	addrLongAsLong,	"Internal Setpoint n21 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212D,	"171",	addrLongAsLong,	"Internal Setpoint n22 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212E,	"172",	addrLongAsLong,	"Internal Setpoint n23 [rpm x 1000]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2271,	"173",	addrLongAsLong,	"Internal Setpoint n21 PI-controller [%x1000]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2272,	"174",	addrLongAsLong,	"Internal Setpoint n22 PI-controller [%x1000]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2273,	"175",	addrLongAsLong,	"Internal Setpoint n23 PI-controller [%x1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x212F,	"200",	addrLongAsLong,	"P-Gain Speed Controller"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2130,	"201",	addrLongAsLong,	"Time Constant n-control [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2131,	"202",	addrLongAsLong,	"Gain Accel. Precontrol"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2132,	"203",	addrLongAsLong,	"Filter Accel. Precontrol [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2133,	"204",	addrLongAsLong,	"Filter Speed Actual Value [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20F4,	"205",	addrLongAsLong,	"Load Precontrol [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20F5,	"206",	addrLongAsLong,	"Sample Time n-control"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2252,	"207",	addrLongAsLong,	"Load Precontrol VFC [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2134,	"210",	addrLongAsLong,	"P-Gain Hold Controller"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213D,	"220",	addrLongAsLong,	"P-Gain (DRS)"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2136,	"221",	addrLongAsLong,	"Master Gear Ratio Factor"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2137,	"222",	addrLongAsLong,	"Slave Gear Ratio Factor"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2138,	"223",	addrLongAsLong,	"Mode Selection"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2139,	"224",	addrLongAsLong,	"Slave Counter [Inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213A,	"225",	addrLongAsLong,	"Offset 1 [Inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213B,	"226",	addrLongAsLong,	"Offset 2 [Inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213C,	"227",	addrLongAsLong,	"Offset 3 [Inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20F6, "228",	addrLongAsLong,	"Precontrol Filter (DRS)"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213E,	"230",	addrLongAsLong,	"Synchronous Encoder"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x213F,	"231",	addrLongAsLong,	"Factor Slave Encoder"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2140,	"232",	addrLongAsLong,	"Factor Slave Sync. Encoder"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22D3, "233",  addrLongAsLong, "External Encoder Increments"));
	//AddSpace(New CSpaceMovilink(addrNamed,        , "234",	addrLongAsLong, "Master Encoder Increments"));

	AddSpace(New CSpaceMovilink(addrNamed,	0x2141,	"240",	addrLongAsLong,	"Synchronization Speed [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2142,	"241",	addrLongAsLong,	"Synchronization Ramp [ms]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2260,	"250",	addrLongAsLong,	"PI-controller"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2261,	"251",	addrLongAsLong,	"P-gain"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2262,	"252",	addrLongAsLong,	"I-component"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2111,	"253",	addrLongAsLong,	"PI Actual Value Mode"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x210F,	"254",	addrLongAsLong,	"PI Actual Value Scaling"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x226C,	"255",	addrLongAsLong,	"PI Actual Value Offset"));
	
	AddSpace(New CSpaceMovilink(addrNamed,	0x232E, "260",  addrLongAsLong, "Operation Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x232F, "261",  addrLongAsLong, "Cycle Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2330, "262",  addrLongAsLong, "Interruption"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2331, "263",  addrLongAsLong, "Factor Kp"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2332, "264",	addrLongAsLong, "Reset Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2333, "265",	addrLongAsLong, "Squeeze Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2334, "266",  addrLongAsLong, "Pre-control"));

	AddSpace(New CSpaceMovilink(addrNamed,  0x2335, "270",  addrLongAsLong, "Setpoint Source"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2336, "271",	addrLongAsLong, "Setpoint"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2337, "272",	addrLongAsLong,	"IPOS Setpoint Address"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2338, "273",	addrLongAsLong, "Time Constant [s]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2339, "274",	addrLongAsLong, "Setpoint Scaling"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x233A, "275",	addrLongAsLong, "Actual Value Source"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x233B, "276",  addrLongAsLong, "IPOS Actual Value Address"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x233C, "277",	addrLongAsLong, "Actual Value Scaling"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x233D, "278",	addrLongAsLong, "Actual Value Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x233E, "279",	addrLongAsLong,	"Time Constant Actual Value [ms]"));

	AddSpace(New CSpaceMovilink(addrNamed,  0x233F, "280",	addrLongAsLong, "Minimum Offset + Actual Value"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2340, "281",	addrLongAsLong,	"Maximum Offset + Actual Value"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2341, "282",	addrLongAsLong, "Minimum Output PID-Controller"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2342, "283",	addrLongAsLong, "Maximum Output PID-Controller"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2343, "284",	addrLongAsLong, "Minimum Output Process-Controller"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2344, "285",	addrLongAsLong, "Maximum Output Process-Controller"));

	AddSpace(New CSpaceMovilink(addrNamed,	0x2143,	"300",	addrLongAsLong,	"Start/Stop Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2144,	"301",	addrLongAsLong,	"Minimum Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2145,	"302",	addrLongAsLong,	"Maximum Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2146,	"303",	addrLongAsLong,	"Current Limit 1 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21F0,	"304",	addrLongAsLong,	"Torque Limit [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2147,	"310",	addrLongAsLong,	"Start/Stop Speed 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2148,	"311",	addrLongAsLong,	"Minimum Speed 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2149,	"312",	addrLongAsLong,	"Maximum Speed 2 [rpm x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x214A,	"313",	addrLongAsLong,	"Current Limit 2 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214B,	"320",	addrLongAsLong,	"Automatic Adjustment 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214C,	"321",	addrLongAsLong,	"Boost 1 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214D,	"322",	addrLongAsLong,	"IxR Compensation 1 [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214E, "323",	addrLongAsLong,	"Premagnetizing Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x214F,	"324",	addrLongAsLong,	"Slip Compensation 1 [rpm x 1000]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x2282,	"325",	addrLongAsLong,	"No-load Vibration Control"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2150,	"330",	addrLongAsLong,	"Automatic Adjustment 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2151,	"331",	addrLongAsLong,	"Boost 2 [V]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2152,	"332",	addrLongAsLong,	"IxR Compensation 2 [V]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2153,	"333",	addrLongAsLong,	"Premagnetizing Time 2 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2154,	"334",	addrLongAsLong,	"Slip Compensation 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2155,	"340",	addrLongAsLong,	"Motor Protection 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2156, "341",	addrLongAsLong,	"Cooling Type 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2157,	"342",	addrLongAsLong,	"Motor Protection 2"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x2158,	"343",	addrLongAsLong,	"Cooling Type 2"));
	
	AddSpace(New CSpaceMovilink(addrNamed,  0x22CB, "344",	addrLongAsLong, "Interval [s]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x239A, "345",	addrLongAsLong, "In-UL-Monitoring 1 [A]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x239B, "346",	addrLongAsLong, "In-UL-Monitoring 2 [A]"));
		
	AddSpace(New CSpaceMovilink(addrNamed,	0x2159,	"350",	addrLongAsLong,	"Change Direction of Rotation 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215A,	"351",	addrLongAsLong,	"Change Direction of Rotation 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215B,	"400",	addrLongAsLong,	"Speed Reference Value [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215C,	"401",	addrLongAsLong,	"Speed Reference Hysteresis [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215D,	"402",	addrLongAsLong,	"Speed Reference Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215E,	"403",	addrLongAsLong,	"Speed Reference Signal"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x215F,	"410",	addrLongAsLong,	"Speed Window Center [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2160,	"411",	addrLongAsLong,	"Speed Window Range Width [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2161,	"412",	addrLongAsLong,	"Speed Window Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2162,	"413",	addrLongAsLong,	"Speed Window Signal"));
   	AddSpace(New CSpaceMovilink(addrNamed,	0x2163,	"420",	addrLongAsLong,	"Speed Comparator Hysteresis [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2164,	"421",	addrLongAsLong,	"Speed Comparator Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2165,	"422",	addrLongAsLong,	"Speed Comparator Signal"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2166,	"430",	addrLongAsLong,	"Current Reference Value [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2167,	"431",	addrLongAsLong,	"Current Reference Hysteresis [% x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2168,	"432",	addrLongAsLong,	"Current Reference Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2169,	"433",	addrLongAsLong,	"Current Reference Signal"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216A,	"440",	addrLongAsLong,	"Imax Signal Hysteresis [% x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x216B,	"441",	addrLongAsLong,	"Imax Signal Delay Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216C,	"442",	addrLongAsLong,	"Imax Signal"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x226D,	"450",	addrLongAsLong,	"PI Actual Value Threshold"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216D,	"500",	addrLongAsLong,	"Speed Monitoring 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216E,	"501",	addrLongAsLong,	"Speed Monitoring Delay Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x216F,	"502",	addrLongAsLong,	"Speed Monitoring 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2170,	"503",	addrLongAsLong,	"Speed Monitoring Delay Time 2 [ms]"));
	
	AddSpace(New CSpaceMovilink(addrNamed,  0x2280, "504",  addrLongAsLong, "Encoder Monitoring Motor"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22C7, "505",	addrLongAsLong, "Synchronous Encoder Monitoring"));

	AddSpace(New CSpaceMovilink(addrNamed,	0x2171,	"510",	addrLongAsLong,	"Positioning Tolerance Slave"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2172,	"511",	addrLongAsLong,	"Prewarning Lag Error"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2173,	"512",	addrLongAsLong,	"Lag Error Limit"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2174,	"513",	addrLongAsLong,	"Delay Lag Error Signal [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2175,	"514",	addrLongAsLong,	"Counter LED Display"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2176,	"515",	addrLongAsLong,	"Delay In-Position Signal [ms]"));
	
	AddSpace(New CSpaceMovilink(addrNamed,  0x242B, "516",	addrLongAsLong, "Encoder Monitoring X41"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x242C, "517",  addrLongAsLong,	"Pulse Monitoring X41"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x242D, "518",  addrLongAsLong, "Encoder Monitoring X42"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x242E, "519",  addrLongAsLong, "Pulse Monitoring X42"));

	
	AddSpace(New CSpaceMovilink(addrNamed,	0x2177,	"520",	addrLongAsLong,	"Mains OFF Response Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2231,	"521",	addrLongAsLong,	"Mains OFF Response"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22DF, "522",  addrLongAsLong, "Phase Failure Monitoring"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22C8, "530",  addrLongAsLong, "Sensor Type 1"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22C9, "531",	addrLongAsLong, "Sensor Type 2"));

	AddSpace(New CSpaceMovilink(addrNamed,  0x2444, "540",	addrLongAsLong, "Response Oscillation/Warning"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2445, "541",	addrLongAsLong, "Response Oscillation/Error"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2446, "542",	addrLongAsLong, "Response Oilage./Warning"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2447, "543",	addrLongAsLong, "Response Oilage./Error"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2448, "544",	addrLongAsLong, "Response Oilage./Temperature"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2449, "545",	addrLongAsLong, "Response Oilage./OK"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x244A, "549",	addrLongAsLong, "Response Brake Wear"));
/*
	AddSpace(New CSpaceMovilink(addrNamed,	0x208F,	"600",	addrLongAsLong,	"Binary Input DI01"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x2090,	"601",	addrLongAsLong,	"Binary Input DI02"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2091,	"602",	addrLongAsLong,	"Binary Input DI03"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2092,	"603",	addrLongAsLong,	"Binary Input DI04"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2093,	"604",	addrLongAsLong,	"Binary Input DI05"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22D7,	"605",	addrLongAsLong,	"Binary Input DI06"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22D8,	"606",	addrLongAsLong,	"Binary Input DI07"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2094,	"610",	addrLongAsLong,	"Binary Input DI10"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2095,	"611",	addrLongAsLong,	"Binary Input DI11"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2096,	"612",	addrLongAsLong,	"Binary Input DI12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2097,	"613",	addrLongAsLong,	"Binary Input DI13"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2098,	"614",	addrLongAsLong,	"Binary Input DI14"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2099,	"615",	addrLongAsLong,	"Binary Input DI15"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209A,	"616",	addrLongAsLong,	"Binary Input DI16"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209B,	"617",	addrLongAsLong,	"Binary Input DI17"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209E,	"620",	addrLongAsLong,	"Binary Output DO01"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x209F,	"621",	addrLongAsLong,	"Binary Output DO02"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22D4,	"622",	addrLongAsLong,	"Binary Output DO03"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22D5,	"623",	addrLongAsLong,	"Binary Output DO04"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22D6,	"624",	addrLongAsLong,	"Binary Output DO05"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A0,	"630",	addrLongAsLong,	"Binary Output DO10"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x20A1,	"631",	addrLongAsLong,	"Binary Output DO11"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A2,	"632",	addrLongAsLong,	"Binary Output DO12"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A3,	"633",	addrLongAsLong,	"Binary Output DO13"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A4,	"634",	addrLongAsLong,	"Binary Output DO14"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A5,	"635",	addrLongAsLong,	"Binary Output DO15"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A6,	"636",	addrLongAsLong,	"Binary Output DO16"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x20A7,	"637",	addrLongAsLong,	"Binary Output DO17"));
*/	AddSpace(New CSpaceMovilink(addrNamed,	0x2178,	"640",	addrLongAsLong,	"Analog Output AO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2179,	"641",	addrLongAsLong,	"Analog Scaling AO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217A,	"642",	addrLongAsLong,	"Operating Mode AO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217B,	"643",	addrLongAsLong,	"Analog Output AO2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217C,	"644",	addrLongAsLong,	"Analog Scaling AO2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217D,	"645",	addrLongAsLong,	"Operating Mode AO2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217E,	"700",	addrLongAsLong,	"Operating Mode 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x217F,	"701",	addrLongAsLong,	"Operating Mode 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2180,	"710",	addrLongAsLong,	"Standstill Current 1 [A]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2181,	"711",	addrLongAsLong,	"Standstill Current 2 [A]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2182,	"720",	addrLongAsLong,	"Setpoint Stop Function 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2183,	"721",	addrLongAsLong,	"Stop Setpoint 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2184,	"722",	addrLongAsLong,	"Start Offset 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2185,	"723",	addrLongAsLong,	"Setpoint Stop Function 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2186,	"724",	addrLongAsLong,	"Stop Setpoint 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2187,	"725",	addrLongAsLong,	"Start Offset 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2188,	"730",	addrLongAsLong,	"Brake Function 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222D,	"731",	addrLongAsLong,	"Brake Release Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2189,	"732",	addrLongAsLong,	"Brake Apply Time 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218A,	"733",	addrLongAsLong,	"Brake Function 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222E,	"734",	addrLongAsLong,	"Brake Release Time 2 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218B,	"735",	addrLongAsLong,	"Brake Apply Time 2 [ms]"));
	//AddSpace(New CSpaceMovilink(addrNamed,	0x227C,	"736",	addrLongAsLong,	"Brake Time [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218C,	"740",	addrLongAsLong,	"Speed Skip Window Center 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218D,	"741",	addrLongAsLong,	"Speed Skip Width 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218E,	"742",	addrLongAsLong,	"Speed Skip Window Center 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x218F,	"743",	addrLongAsLong,	"Speed Skip Width 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2190,	"750",	addrLongAsLong,	"Slave Setpoint"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2191,	"751",	addrLongAsLong,	"Scaling Slave Setpoint"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x225E,	"760",	addrLongAsLong,	"Lockout Run/Stop Buttons"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22DD, "770",  addrLongAsLong, "Energy Savings Function"));

	AddSpace(New CSpaceMovilink(addrNamed,  0x2320, "780",	addrLongAsLong, "Ethernet IP Address"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2321, "781",  addrLongAsLong, "Ethernet Subnet Mask"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2322, "782",	addrLongAsLong, "Ethernet Standard Gateway"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2325, "783",  addrLongAsLong, "Ethernet Baud Rate"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2323, "784",  addrLongAsLong, "Ethernet MAC Address"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2411, "785",	addrLongAsLong, "EtherNet/IP Startup Configuration"));


	
	AddSpace(New CSpaceMovilink(addrNamed,	0x2192,	"802",	addrLongAsLong,	"Factory Setting"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2193,	"803",	addrLongAsLong,	"Parameter Lock"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2194,	"804",	addrLongAsLong,	"Reset Statistic Data"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2195,	"810",	addrLongAsLong,	"RS-485 Address"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2196,	"811",	addrLongAsLong,	"RS-485 Group Address"));
   	AddSpace(New CSpaceMovilink(addrNamed,	0x2197, "812",	addrLongAsLong,	"RS-485 Timeout Delay [ms]"));
//	AddSpace(New CSpaceMovilink(addrNamed,	0x2198,	"813",	addrLongAsLong,	"SBus Address"));
//	AddSpace(New CSpaceMovilink(addrNamed,	0x2199,	"814",	addrLongAsLong,	"SBus Group Address"));
//	AddSpace(New CSpaceMovilink(addrNamed,	0x219A,	"815",	addrLongAsLong,	"SBus Timeout Delay [ms]"));
//	AddSpace(New CSpaceMovilink(addrNamed,	0x219B,	"816",	addrLongAsLong,	"SBus Baud Rate"));
//	AddSpace(New CSpaceMovilink(addrNamed,	0x219C,	"817",	addrLongAsLong,	"SBus Synchronization ID"));
//	AddSpace(New CSpaceMovilink(addrNamed,	0x221C,	"818",	addrLongAsLong,	"CAN Synchronizatio ID"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219E,	"819",	addrLongAsLong,	"Fieldbus Timeout Delay [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219F,	"820",	addrLongAsLong,	"Brake 4-quadrant Operation 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A0,	"821",	addrLongAsLong,	"Brake 4-quadrant Operation 2"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21A1,	"830",	addrLongAsLong,	"Fault Response EXT.FAULT"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A2,	"831",	addrLongAsLong,	"Fault Response FIELDBUS TIMEOUT"));
   	AddSpace(New CSpaceMovilink(addrNamed,	0x21A3,	"832",	addrLongAsLong,	"Fault Response MOTOR OVERLOAD"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A4,	"833",	addrLongAsLong,	"Fault Response RS-485 TIMEOUT"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A5,	"834",	addrLongAsLong,	"Fault Response DRS LAG ERROR"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A8,	"835",	addrLongAsLong,	"Fault Response TF Sensor SIGNAL"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21A7,	"836",	addrLongAsLong,	"Fault Response SBus 1 TIMEOUT"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22E8, "837",  addrLongAsLong, "Fault Response SBus 2 TIMEOUT"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22FE, "838",	addrLongAsLong, "Fault Response SW_Limit Switch"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21A9,	"840",	addrLongAsLong,	"Manual Reset Response"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AA,	"841",	addrLongAsLong,	"Auto Reset Response"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AB,	"842",	addrLongAsLong,	"Restart Time Response [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222B,	"850",	addrLongAsLong,	"Speed Scaling Factor Numerator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222C,	"851",	addrLongAsLong,	"Speed Scaling Factor Denominator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2244,	"852",	addrLongAsLong,	"User Dimension"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AC,	"860",	addrLongAsLong,	"PWM Frequency 1 [kHz]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AD,	"861",	addrLongAsLong,	"PWM Frequency 2 [kHz]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x222F,	"862",	addrLongAsLong,	"PWM Fix 1"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x2230,	"863",	addrLongAsLong,	"PWM Fix 2"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x227B, "864",	addrLongAsLong, "PWM Frequency CFC [kHz]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2070,	"870",	addrLongAsLong,	"Setpoint Description PO1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2071,	"871",	addrLongAsLong,	"Setpoint Description PO2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2072,	"872",	addrLongAsLong,	"Setpoint Description PO3"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2073,	"873",	addrLongAsLong,	"Actual Value Description PI1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2074,	"874",	addrLongAsLong,	"Actual Value Description PI2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2075,	"875",	addrLongAsLong,	"Actual Value Description PI3"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21AE,	"876",	addrLongAsLong,	"PO Data Enable"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22E9,	"880",	addrLongAsLong,	"Protocol SBus 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2198,	"881",	addrLongAsLong,	"Address SBus 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2199,	"882",	addrLongAsLong,	"Group Address SBus 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219A,	"883",	addrLongAsLong,	"Timeout Delay SBus 1 [s]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219B,	"884",	addrLongAsLong,	"Baud Rate SBus 1 [kBaud]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x219C,	"885",	addrLongAsLong,	"Sychronization ID SBus 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x231D,	"886",	addrLongAsLong,	"Address CANopen 1"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2304,	"887",	addrLongAsLong,	"Synchronization Ext. Controller 1/2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x231F,	"888",	addrLongAsLong,	"Synchronization Time SBus 1/2 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2432, "889",	addrLongAsLong,	"SBus 1 Parameter Channel 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22EA,	"890",	addrLongAsLong,	"Protocol SBus 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22E4,	"891",	addrLongAsLong,	"Address SBus 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22E5,	"892",	addrLongAsLong,	"Group Address SBus 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22E6,	"893",	addrLongAsLong,	"Timeout Delay SBus 2 [s]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22EB,	"894",	addrLongAsLong,	"Baud Rate SBus 2 [kBaud]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22E7,	"895",	addrLongAsLong,	"Sychronization ID SBus 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x231E,	"896",	addrLongAsLong,	"Address CANopen 2"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2433,	"899",	addrLongAsLong,	"SBus 2 Parameter Channel 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21AF,	"900",	addrLongAsLong,	"IPOS Reference Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B0,	"901",	addrLongAsLong,	"IPOS Reference Speed 1 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B1,	"902",	addrLongAsLong,	"IPOS Reference Speed 2 [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B2,	"903",	addrLongAsLong,	"IPOS Reference Travel Type"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2287, "904",  addrLongAsLong, "IPOS Reference Tral To Zero Pulse"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22C0, "905",	addrLongAsLong, "IPOS Hiperface Offset (X15) [inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B3,	"910",	addrLongAsLong,	"IPOS Gain X-controller"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B4,	"911",	addrLongAsLong,	"IPOS Positioning Ramp 1 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21F8,	"912",	addrLongAsLong,	"IPOS Positionint Ramp 2 [ms]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B5,	"913",	addrLongAsLong,	"IPOS Travelling Speed CW [rpm x 1000]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B6,	"914",	addrLongAsLong,	"IPOS Travelling Speed CCW [rpm x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x21B7,	"915",	addrLongAsLong,	"IPOS Speed Feedforward"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B8,	"916",	addrLongAsLong,	"IPOS Ramp Type"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22D9, "917",	addrLongAsLong, "IPOS Ramp Mode"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21B9,	"920",	addrLongAsLong,	"IPOS SW Limit Switch CW"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BA,	"921",	addrLongAsLong,	"IPOS SW Limit Switch CCW"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BB, "922",	addrLongAsLong,	"IPOS Positioning Window"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BC,	"923",	addrLongAsLong,	"IPOS Lag Error Window"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x21BD, "930",	addrLongAsLong,	"IPOS Override"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22CA, "933",	addrLongAsLong, "IPOS Jerk Time [s]"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x22B8, "938",	addrLongAsLong, "IPOS Execution Speed Task 1"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x2302, "939",	addrLongAsLong, "IPOS Execution Speed Task 2"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2219,	"941",	addrLongAsLong,	"IPOS Source Actual Position"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2246,	"942",	addrLongAsLong,	"IPOS Encoder Factor Numerator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2247,	"943",	addrLongAsLong,	"IPOS Encoder Factor Denominator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2253,	"944",	addrLongAsLong,	"IPOS Encoder Scaling Ext. Encoder"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22BB,	"945",	addrLongAsLong,	"IPOS Encoder Type (X14)"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22BE,	"946",	addrLongAsLong,	"IPOS Counting Direction (X14)"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x22BF,	"947",	addrLongAsLong,	"IPOS Hiperface Offset (X14) [inc]"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2249,	"950",	addrLongAsLong,	"DIP Encoder Type"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2248,	"951",	addrLongAsLong,	"DIP Counting Direction"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x224A,	"952",	addrLongAsLong,	"DIP Cycle Frequency [% x 1000]"));
  	AddSpace(New CSpaceMovilink(addrNamed,	0x224B,	"953",	addrLongAsLong,	"DIP Position Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x224D,	"954",	addrLongAsLong,	"DIP Zero Offset"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2250,	"955",	addrLongAsLong,	"DIP Encoder Scaling"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2283,	"960",	addrLongAsLong,	"IPOS Modulo Function"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2284,	"961",	addrLongAsLong,	"IPOS Modulo Numerator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2285,	"962",	addrLongAsLong,	"IPOS Modulo Denominator"));
	AddSpace(New CSpaceMovilink(addrNamed,	0x2286,	"963",	addrLongAsLong,	"IPOS Modulo Encoder Resolution"));
      	
	AddSpace(New CSpaceMovilink(addrNamed,  0x2409, "970",	addrLongAsLong, "IPOS DPRAM Synchronization"));
	AddSpace(New CSpaceMovilink(addrNamed,  0x240A, "971",	addrLongAsLong, "IPOS Synchronization Phase [ms]"));
	
	AddSpace(New CSpaceMovilink(addrNamed,	0xFFFF,	"9999",	addrLongAsLong,	"Latest Exception Code",		0,0, TRUE));  
	}


//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSEWMovilinkAddrDialog, CStdAddrDialog);
		
// Constructor

CSEWMovilinkAddrDialog::CSEWMovilinkAddrDialog(CSEWMovilinkASerialDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "MovilinkElementDlg";
	}
 
// Overridables

void CSEWMovilinkAddrDialog::SetAddressText(CString Text)
{
	ShowElements();
	
	CSEWMovilinkASerialDriver *pDriver = NULL;

	UINT uEnd = Text.Find('-');

	if ( m_pSpace && m_pDriver ) {

		pDriver = (CSEWMovilinkASerialDriver *) m_pDriver;

		UINT uTable = m_pSpace->m_uTable;

		CString Working = Text.Mid(uEnd);

		if ( uTable == 4 ) {

			if ( pDriver->GetSuffix(Working) ) {

				CButton   &Check = (CButton   &) GetDlgItem(2007);

				Check.SetCheck(TRUE);

				}
			}

		else if ( uTable != 3 ) {
			
			UINT uSet = pDriver->GetSuffix(Working);
			
			if ( uSet ) {

				SetRadioGroup(2004, 2005, uSet - 1);
				}
			}
		}

	CStdAddrDialog::SetAddressText(Text.Left(uEnd));
	}

CString CSEWMovilinkAddrDialog::GetAddressText(void)
{
	CString Text = "";

	CSEWMovilinkASerialDriver *pDriver = NULL;

	CSpaceMovilink *pSpace = NULL;

	if ( m_pDriver && m_pSpace ) {

		Text += CStdAddrDialog::GetAddressText();

		UINT uTable = m_pSpace->m_uTable;

		pDriver = (CSEWMovilinkASerialDriver *) m_pDriver;

		if ( uTable == addrNamed ) {

			Text += m_pSpace->m_Prefix;
			}

		else if ( uTable == 4 ) {

			CButton   &Check = (CButton   &) GetDlgItem(2007);

			Text += pDriver->GetSuffix(uTable, Check.IsChecked());

			return Text;
			}

		pSpace = (CSpaceMovilink *)m_pSpace;

		Text += pDriver->GetSuffix(uTable, pSpace->m_ReadOnly ? 0 : GetRadioGroup(2004, 2005) + 1);
		}

	return 	Text;

	}

// Helpers

void CSEWMovilinkAddrDialog::ShowElements(void)
{
	UINT uParam = m_pSpace ? m_pSpace->m_uTable : 0;
	
	switch( uParam ) { 

		case SEW_ID_PD:

		       	ShowProcessData();
		
			break;

		case SEW_ID_SEND:

			ShowProcessDataInfo();
	
			break;
		
		default:

			ShowNoData();

			break;
		}
	}

void CSEWMovilinkAddrDialog::ShowProcessData(void)
{
	if ( m_pSpace ) {

		GetDlgItem(2004).ShowWindow(FALSE);

		GetDlgItem(2005).ShowWindow(FALSE);

		GetDlgItem(2006).ShowWindow(FALSE);

		GetDlgItem(2009).ShowWindow(FALSE);

		GetDlgItem(2007).ShowWindow(FALSE);

		}
	}

void CSEWMovilinkAddrDialog::ShowNoData(void)
{ 
	GetDlgItem(2004).ShowWindow(TRUE);

	GetDlgItem(2005).ShowWindow(TRUE);

	BOOL fEnable = FALSE;

	if ( m_pSpace ) {

		CSpaceMovilink * pSpace = (CSpaceMovilink *) m_pSpace;

		fEnable = !pSpace->m_ReadOnly;
		}
		
	GetDlgItem(2004).EnableWindow(fEnable);

	GetDlgItem(2005).EnableWindow(fEnable);

	GetDlgItem(2006).ShowWindow(FALSE);

	GetDlgItem(2009).ShowWindow(FALSE);

	GetDlgItem(2007).ShowWindow(FALSE);

	SetRadioGroup(2004, 2005, 1);

	}

void CSEWMovilinkAddrDialog::ShowProcessDataInfo(void)
{
	if ( m_pSpace ) {

		GetDlgItem(2004).ShowWindow(FALSE);

		GetDlgItem(2005).ShowWindow(FALSE);

		GetDlgItem(2006).ShowWindow(TRUE);

		GetDlgItem(2009).ShowWindow(TRUE);

		GetDlgItem(2007).ShowWindow(TRUE);

		}
	}

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK Space Wrapper Class
//

// Constructors

CSpaceMovilink::CSpaceMovilink(UINT t, UINT i, CString p, AddrType type, CString c, UINT n, UINT x, BOOL fRO) : CSpace(t, p, c, 10, n, x, type)
{
	m_Index		= i;

	m_ReadOnly	= fRO;

	if ( m_uTable == addrNamed ) {

		m_uMinimum = m_Index;
		}
	}


// End of File
