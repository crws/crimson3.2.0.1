
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ABBASE_HPP
	
#define	INCLUDE_ABBASE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABDriver;
class CABDialog;
class CSLCDH485Driver;
class CABDH485Dialog;
class CAbTagsCreator;

//////////////////////////////////////////////////////////////////////////
//
// File and Address Ranges
//

#define	AB_FILE_NONE		224
#define	AB_FILE_MIN		9
#define	AB_FILE_MAX		223
#define	AB_ADDR_MIN		0
#define	AB_ADDR_MAX		9999
#define AB_STR_MAX		80

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Space Wrapper Class
//

class CSpaceAB : public CSpace
{
	public:
		// Constructors
		CSpaceAB(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT fd, UINT fn, UINT fx, BOOL fTriplet = FALSE);

		// Public Data
		UINT m_FileDef;
		UINT m_FileMin;
		UINT m_FileMax;
		BOOL m_fTriplet;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Base Driver
//

class CABDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CABDriver(void);

		// Destructor
		~CABDriver(void);

		// Driver Data
		UINT GetFlags(void);

		// Tag Import
		BOOL MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

		// Space List Access
		CSpaceAB * GetSpace(INDEX Index);
		CSpaceAB * GetSpace(CAddress const &Addr);
		CSpaceAB * GetSpace(CString Text);

		// Helpers
		BOOL IsString(UINT uCode);
		
	protected:
		// Implementation
		void CreateTag(IMakeTags *pTags, CStringArray const &List, CAbTagsCreator &AbTags);
		BOOL ParseCount(CString &Value, UINT &uBit, BOOL &fRead, CString const &Text);
		BOOL ParseTimer(CString &Value, UINT &uBit, BOOL &fRead, CString const &Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Selection Dialog
//

class CABDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CABDialog(CABDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		CABDriver     *	m_pDriver;
		CAddress      *	m_pAddr;
		BOOL		m_fPart;
		CSpaceAB      *	m_pSpace;
		UINT		m_uDevice;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSelChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void SetCaption(void);
		void LoadList(void);
		UINT GetOffset(void);
		BOOL ValidOffset(UINT uOffset);
		void FindSpace(void);
		void ShowAddress(UINT uFile, UINT uOffset);
		void ClearAddress(void);
		void ShowDetails(void);
		void ClearDetails(void);

		// Overridables
	   	virtual BOOL AllowSpace(CSpace *pSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley DH485 Selection Dialog
//


class CABDH485Dialog : public CABDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CABDH485Dialog(CSLCDH485Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
       
	protected:
		// Overridables
	   	virtual BOOL AllowSpace(CSpace *pSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Data Tag String Types
//

#define	TAG_NAME		0
#define	TAG_LABEL		1


//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Data Tag String Helper
//

class CAbTagsCreator : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAbTagsCreator(void);
		CAbTagsCreator(CString Name);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		CViewWnd * CreateView(UINT uType);

		// Persistance
		void GetTagConfig(CItem * pItem);
		void SaveTagConfig(CItem * pItem);

		// Data Access
		CString	m_Root;
		UINT	m_Define;
		UINT	m_File;
		UINT	m_Symbol;
		UINT	m_Desc1;
		UINT	m_Desc2;
		UINT	m_Desc3;
		UINT	m_Desc4;
		UINT	m_Desc5;
		
		// Protected Access
		void GetNext(UINT &uIndex, UINT uMask);
		UINT GetMask(UINT uMask);
				
	protected:

		// Data Members
		UINT	m_Name;
		UINT	m_Label;

		// Implementaion
		BOOL CheckIndex(UINT uIndex, UINT uMask);
		void SetDefaults(void);
		void UpdateFields(UINT uValue);
					
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
