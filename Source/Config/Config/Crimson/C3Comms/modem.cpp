
#include "intern.hpp"

#include "modem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver (External)
//

// Instantiator

ICommsDriver *	Create_ModemClientDriver(void)
{
	return New CModemClientDriver;
	} 

// Constructor

CModemClientDriver::CModemClientDriver(void)
{
	m_wID	       = WORD(0xFF01);

	m_uType	       = driverModem;
				      	
	m_Manufacturer = "<System>";	
	
	m_DriverName   = "PPP and Modem Client";
	
	m_Version      = "1.00";
	
	m_ShortName    = "PPP Client";

	m_fSingle      = TRUE;	

	m_Binding      = bindStdSerial;

	C3_PASSED();
	}
	
// Binding Control

UINT CModemClientDriver::GetBinding(void)
{
	return m_Binding;
	}

void CModemClientDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CModemClientDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModemClientDriverOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver (Option Card)
//

// Instantiator

ICommsDriver *	Create_ModemClientOptionDriver(void)
{
	return New CModemClientDriverOptionCard;
	}

// Constructor

CModemClientDriverOptionCard::CModemClientDriverOptionCard(void)
{
	m_wID	       = WORD(0xFF04);

	m_uType	       = driverModem;	
	
	m_Manufacturer = "Cellular Modem Driver";  	
	
	m_DriverName   = "PPP and Modem Client";
	
	m_Version      = "1.00";
	
	m_ShortName    = "PPP Client";

	m_fSingle      = TRUE;	

	m_Binding      = bindModem;

	C3_PASSED();
	}
	
// Binding Control

UINT CModemClientDriverOptionCard::GetBinding(void)
{
	return m_Binding;
	}

void CModemClientDriverOptionCard::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CModemClientDriverOptionCard::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModemClientDriverOptionsOptionCard);
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver Options (External)
//

// Dynamic Class

AfxImplementDynamicClass(CModemClientDriverOptions, CUIItem);

// Constructor

CModemClientDriverOptions::CModemClientDriverOptions(void)
{	
	m_Connect    = 1;	

	m_Safe	     = 0;

	m_Demand     = 1;

	m_Timeout    = 240;

	m_SMS	     = 0;	
	
	m_ModemBrand = 0;

	m_Route	     = 0;

	m_Addr	     = DWORD(MAKELONG(MAKEWORD(0, 100), MAKEWORD(168, 192)));

	m_Mask	     = DWORD(MAKELONG(MAKEWORD(0, 255), MAKEWORD(255, 255)));	

	m_NewMask    = GetDefInit() ^ 0xFFFF;

	m_LogFile    = 0;

	m_DynDNS     = 0;

	m_DNSMode    = 0;

	m_DNS1       = 0x08080808;

	m_DNS2       = 0x08080404;

	m_Location   = 2;
	}

// UI Management

void CModemClientDriverOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Connect" ) {

			CUITextElement *pUI = pHost->FindUI(L"ModemBrand");

			switch( m_Connect ) {

				case 1:
					((CUITextEnum *) pUI)->SetMask(0x0011);
					break;

				case 2:
				case 3:
					((CUITextEnum *) pUI)->SetMask(0x000F);
					break;
				}

			if( Tag == "Connect" ) {

				m_ModemBrand = 0;

				pHost->UpdateUI("ModemBrand");
				}

			DoEnables(pHost);
			}

		if( Tag == "ModemBrand") {

			if( m_ModemBrand ) {

				m_NewMask = GetDefInit() ^ 0xFFFF;

				pHost->UpdateUI("NewMask");
				}

			DoEnables(pHost);
			}

		if( Tag == "Demand" ) {

			DoEnables(pHost);
			}

		if( Tag == "Route" ) {

			DoEnables(pHost);
			}			
		
		if( Tag == "DynDNS" ) {

			DoEnables(pHost);
			}

		if( Tag == "DNSMode" ) {

			DoEnables(pHost);
			}

		if( Tag == "NewMask" ) {

			if( m_NewMask & 0x4000 ) {

				if( (m_NewMask & 0x6000) != 0x6000 ) {

					m_NewMask |= 0x6000;

					pHost->UpdateUI("NewMask");
					}
				}
			}
		}
	}

// Persistance

void CModemClientDriverOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "MoreMask") {

			UINT uMask = Tree.GetValueAsInteger();			

			if( m_Connect != 1 ) {

				m_NewMask = 0x7F00 | uMask;
				}
			else 
				m_NewMask = uMask;

			continue;
			}		

		if( Name == "More") {

			UINT uMask = Tree.GetValueAsInteger();			

			if( m_Connect != 1 ) {

				m_NewMask = 0x7F80 | uMask;
				}
			else 
				m_NewMask = uMask;

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}

	RegisterHandle();  
	}

// Download Support

BOOL CModemClientDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	//	UINT		m_uMode;
	//	UINT		m_uConnect;
	//	UINT		m_uDevice;
	//	UINT		m_uBaudRate;
	//	UINT		m_uTimeout;
	//	char		m_sInit[48];
	//	char		m_sDial[32];
	//	BOOL		m_fCHAP;
	//	char		m_sUser[32];
	//	char		m_sPass[16];
	//	BOOL		m_fGate;
	//	CIPAddr		m_RemAddr;
	//	CIPAddr		m_RemMask;
	//	CIPAddr		m_LocAddr;
	//	IRecvSMS *	m_pSMS;
	//	DWORD		m_InitMask;
	//	BOOL		m_fLogFile;
	//	UINT		m_uDynDNS;
	//	char		m_sDynUser[32];
	//	char		m_sDynPass[16];
	//	char		m_sDynHost[24];
	//	BYTE		m_bLocation;
	//	BYTE		m_bRoaming;
	//	BYTE		m_bSIMSlot;
	//	BYTE		m_bCellRadio;
	//	BYTE		m_bMore[40];

	Init.AddLong(m_Safe ? 1 : 0);
	Init.AddLong(m_Connect+1);
	Init.AddLong(0);
	Init.AddLong(115200);
	Init.AddLong(m_Demand ? m_Timeout : 0);
	Init.AddData(m_Init, 48);
	Init.AddData(m_Dial, 32);	
	Init.AddLong(0);
	Init.AddData(m_User, 32);
	Init.AddData(m_Pass, 16);
	Init.AddLong(m_Route==0);
	Init.AddLong(m_Addr);
	Init.AddLong(m_Mask);
	Init.AddLong(0);
	Init.AddLong(m_SMS);

	if( m_ModemBrand ) {

		Init.AddLong(GetDefInit());
		}
	else
		Init.AddLong(m_NewMask ^ 0xFFFF);

	Init.AddLong(m_LogFile);
	Init.AddLong(m_DynDNS);
	Init.AddData(m_DynUser, 32);
	Init.AddData(m_DynPass, 16);
	Init.AddData(m_DynHost, 24);
	Init.AddByte(2);	
	Init.AddByte(2);
	Init.AddByte(2);
	Init.AddByte(0);
	Init.AddData("", 40);

	Init.AddLong(m_DNSMode);
	Init.AddLong(m_DNS1);
	Init.AddLong(m_DNS2);

	return TRUE;
	}

// Meta Data Creation

void CModemClientDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Dummy);
	Meta_AddInteger(Connect);
	Meta_AddInteger(Safe);
	Meta_AddInteger(Demand); 	
	Meta_AddInteger(Location); 
	Meta_AddInteger(Timeout);
	Meta_AddInteger(SMS);
	Meta_AddInteger(ModemBrand);
	Meta_AddString (Init);
	Meta_AddString (Dial);
	Meta_AddString (User);
	Meta_AddString (Pass);
	Meta_AddInteger(Route);
	Meta_AddInteger(Addr);
	Meta_AddInteger(Mask);
	Meta_AddInteger(NewMask);
	Meta_AddInteger(LogFile);
	Meta_AddInteger(DynDNS);
	Meta_AddString (DynUser);
	Meta_AddString (DynPass);
	Meta_AddString (DynHost);
	Meta_AddInteger(DNSMode);
	Meta_AddInteger(DNS1);
	Meta_AddInteger(DNS2);
	}

// Implementation

void CModemClientDriverOptions::DoEnables(IUIHost *pHost)
{
	BOOL fModem  = (m_Connect >= 1);	

	BOOL fSMS    = (m_Connect >= 2);

	BOOL fDemand = (m_Demand  == 1);

	BOOL fRoute  = (m_Route   == 1);

	BOOL fDynDNS = (m_DynDNS  >= 1);

	BOOL fDNS    = (m_DNSMode >= 2);
	
	pHost->EnableUI(this, "Timeout",    fDemand);
	pHost->EnableUI(this, "SMS",        fSMS);	
	pHost->EnableUI(this, "Init",       fModem);		
	pHost->EnableUI(this, "Dial",       fModem);
	pHost->EnableUI(this, "Addr",	    fRoute);
	pHost->EnableUI(this, "Mask",	    fRoute);
	pHost->EnableUI(this, "DynUser",    fDynDNS);
	pHost->EnableUI(this, "DynPass",    fDynDNS);
	pHost->EnableUI(this, "DynHost",    fDynDNS);
	pHost->EnableUI(this, "DNS1",       fDNS);
	pHost->EnableUI(this, "DNS2",       fDNS);
	pHost->EnableUI(this, "ModemBrand", fModem);
	pHost->EnableUI(this, "NewMask",    fModem && !m_ModemBrand);
	}

UINT CModemClientDriverOptions::GetDefInit(void)
{
	switch( m_ModemBrand ) {

		case 1:
			return 0x0018;

		case 2:
			return 0xFF18;

		case 3:
			return 0x001D;

		case 4:
			return 0xFF00;
		}

	return 0xFF18;
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Client Driver Options (Option Card)
//

// Dynamic Class

AfxImplementDynamicClass(CModemClientDriverOptionsOptionCard, CUIItem);

// Constructor

CModemClientDriverOptionsOptionCard::CModemClientDriverOptionsOptionCard(void)
{	
	m_Connect    = 2;	
	
	m_Safe       = 0;

	m_Demand     = 1;

	m_Timeout    = 240;

	m_SMS        = 0;

	m_Roaming    = 0;

	m_SIMSlot    = 0;

	m_CellRadio  = 0;

	m_Auth       = 0;

	m_Route      = 0;

	m_Addr       = DWORD(MAKELONG(MAKEWORD(0, 100), MAKEWORD(168, 192)));

	m_Mask       = DWORD(MAKELONG(MAKEWORD(0, 255), MAKEWORD(255, 255)));	

	m_LogFile    = 0;

	m_DynDNS     = 0;

	m_DNSMode    = 0;

	m_DNS1       = 0x08080808;

	m_DNS2       = 0x08080404;

	m_Location   = 0;
	}

// UI Management

void CModemClientDriverOptionsOptionCard::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			UINT uMask    = 0x5C;

			UINT uDefault = 2;

			if( pItem->GetDatabase()->HasFlag(L"Graphite") ) {

				uMask    = 0x10;

				uDefault = 4;
			}

			if( pItem->GetDatabase()->HasFlag(L"Canyon") ) {

				uMask    = 0x10;

				uDefault = 4;
			}

			if( pItem->GetDatabase()->HasFlag(L"Manticore") ) {

				uMask    = 0x40;

				uDefault = 6;
			}

			CUITextElement *pUI = pHost->FindUI(L"Connect");

			((CUITextEnum *) pUI)->SetMask(uMask);

			if( !((1 << m_Connect) & uMask) ) {

				m_Connect = uDefault; 
				}

			CheckConnect(pHost);

			DoEnables(pHost);
			}

		if( Tag == "Connect" ) {

			CheckConnect(pHost);

			DoEnables(pHost);
			}

		if( Tag == "Demand" ) {

			DoEnables(pHost);
			}

		if( Tag == "Route" ) {

			DoEnables(pHost);
			}
		
		if( Tag == "DynDNS" ) {

			DoEnables(pHost);
			}
		
		if( Tag == "DNSMode" ) {

			DoEnables(pHost);
			}

		if( Tag == "Auth" ) {

			DoEnables(pHost);
			}
		}
	}

// Download Support

BOOL CModemClientDriverOptionsOptionCard::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	//	UINT		m_uMode;
	//	UINT		m_uConnect;
	//	UINT		m_uDevice;
	//	UINT		m_uBaudRate;
	//	UINT		m_uTimeout;
	//	char		m_sInit[48];
	//	char		m_sDial[32];
	//	BOOL		m_fCHAP;
	//	char		m_sUser[32];
	//	char		m_sPass[16];
	//	BOOL		m_fGate;
	//	CIPAddr		m_RemAddr;
	//	CIPAddr		m_RemMask;
	//	CIPAddr		m_LocAddr;
	//	IRecvSMS *	m_pSMS;
	//	DWORD		m_InitMask;
	//	BOOL		m_fLogFile;
	//	UINT		m_uDynDNS;
	//	char		m_sDynUser[32];
	//	char		m_sDynPass[16];
	//	char		m_sDynHost[24];
	//	BYTE		m_bLocation;
	//	BYTE		m_bRoaming;
	//	BYTE		m_bSIMSlot;
	//	BYTE		m_bCellRadio;
	//      BYTE		m_bAuth;
	//      char            m_sPIN[4];
	//	BYTE		m_bMore[35];

	Init.AddLong(m_Safe ? 1 : 0);
	Init.AddLong(m_Connect+1);	
	Init.AddLong(0);
	Init.AddLong(115200);
	Init.AddLong(m_Demand ? m_Timeout : 0);
	Init.AddData(m_Init, 48);
	Init.AddData(m_Dial, 32);	
	Init.AddLong(0);
	Init.AddData(m_User, 32);
	Init.AddData(m_Pass, 16);
	Init.AddLong(m_Route==0);
	Init.AddLong(m_Addr);
	Init.AddLong(m_Mask);
	Init.AddLong(0);
	Init.AddLong(m_SMS);	
	Init.AddLong(0x001D);
	Init.AddLong(m_LogFile);
	Init.AddLong(m_DynDNS);
	Init.AddData(m_DynUser, 32);
	Init.AddData(m_DynPass, 16);
	Init.AddData(m_DynHost, 24);
	Init.AddByte(BYTE(m_Location));	
	Init.AddByte(BYTE(m_Roaming));
	Init.AddByte(BYTE(m_SIMSlot));
	Init.AddByte(BYTE(m_CellRadio));
	Init.AddByte(BYTE(m_Auth));
	Init.AddData(m_PIN, 4);
	Init.AddData("", 35);

	Init.AddLong(m_DNSMode);
	Init.AddLong(m_DNS1);
	Init.AddLong(m_DNS2);

	return TRUE;
	}

// Meta Data Creation

void CModemClientDriverOptionsOptionCard::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Connect);
	Meta_AddInteger(Safe);
	Meta_AddInteger(Demand);
	Meta_AddInteger(Location);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(SMS);
	Meta_AddInteger(Roaming);
	Meta_AddInteger(SIMSlot);
	Meta_AddString (PIN);
	Meta_AddInteger(CellRadio);
	Meta_AddInteger(Auth);
	Meta_AddString (Init);
	Meta_AddString (Dial);
	Meta_AddString (User);
	Meta_AddString (Pass);
	Meta_AddInteger(Route);
	Meta_AddInteger(Addr);
	Meta_AddInteger(Mask);
	Meta_AddInteger(LogFile);
	Meta_AddInteger(DynDNS);
	Meta_AddString (DynUser);
	Meta_AddString (DynPass);
	Meta_AddString (DynHost);
	Meta_AddInteger(DNSMode);
	Meta_AddInteger(DNS1);
	Meta_AddInteger(DNS2);
	}

// Implementation

void CModemClientDriverOptionsOptionCard::DoEnables(IUIHost *pHost)
{
	BOOL fModem  = (m_Connect >= 1);		

	BOOL fSMS    = (m_Connect >= 2);	

	BOOL fLTE    = (m_Connect == 6);

	BOOL fDemand = (m_Demand  == 1);

	BOOL fRoute  = (m_Route   == 1);

	BOOL fDynDNS = (m_DynDNS  >= 1);

	BOOL fDNS    = (m_DNSMode >= 2);
	
	pHost->EnableUI(this, "Demand",    !fLTE);
	pHost->EnableUI(this, "Timeout",   fDemand);
	pHost->EnableUI(this, "SMS",       fSMS);
	pHost->EnableUI(this, "Roaming",   fLTE);
	pHost->EnableUI(this, "SIMSlot",   fLTE);
	pHost->EnableUI(this, "PIN",       fLTE);
	pHost->EnableUI(this, "CellRadio", fLTE);
	pHost->EnableUI(this, "Auth",      fLTE);
	pHost->EnableUI(this, "User",      !fLTE || m_Auth);
	pHost->EnableUI(this, "Pass",      !fLTE || m_Auth);
	pHost->EnableUI(this, "Init",      fModem);
	pHost->EnableUI(this, "Location",  fSMS && !fLTE);
	pHost->EnableUI(this, "Dial",      fModem);
	pHost->EnableUI(this, "Addr",	   fRoute);
	pHost->EnableUI(this, "Mask",	   fRoute);
	pHost->EnableUI(this, "DynUser",   fDynDNS);
	pHost->EnableUI(this, "DynPass",   fDynDNS);
	pHost->EnableUI(this, "DynHost",   fDynDNS);
	pHost->EnableUI(this, "DNS1",      fDNS);
	pHost->EnableUI(this, "DNS2",      fDNS);
	}

void CModemClientDriverOptionsOptionCard::CheckConnect(IUIHost *pHost)
{
	if( m_Connect == 6 ) {

		m_Demand  = 0;

		m_Timeout = 0;

		pHost->UpdateUI(L"Timeout");
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver (External)
//

// Instantiator

ICommsDriver *	Create_ModemServerDriver(void)
{
	return New CModemServerDriver;
	}

// Constructor

CModemServerDriver::CModemServerDriver(void)
{
	m_wID	       = WORD(0xFF02);

	m_uType	       = driverModem;	
	
	m_Manufacturer = "<System>";	
	
	m_DriverName   = "PPP and Modem Server";
	
	m_Version      = "1.00";
	
	m_ShortName    = "PPP Server";

	m_fSingle      = TRUE;
	
	m_Binding      = bindStdSerial;

	C3_PASSED();
	}

// Binding Control

UINT CModemServerDriver::GetBinding(void)
{
	return m_Binding;
	}

void CModemServerDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CModemServerDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModemServerDriverOptions);
	}	

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver (Option Card)
//

// Instantiator

ICommsDriver *	Create_ModemServerOptionDriver(void)
{
	return New CModemServerDriverOptionCard;
	}

// Constructor

CModemServerDriverOptionCard::CModemServerDriverOptionCard(void)
{
	m_wID	       = WORD(0xFF05);

	m_uType	       = driverModem;	
	
	m_Manufacturer = "Cellular Modem Driver";	
	
	m_DriverName   = "PPP and Modem Server";
	
	m_Version      = "1.00";
	
	m_ShortName    = "PPP Server";

	m_fSingle      = TRUE;
	
	m_Binding      = bindModem;

	C3_PASSED();
	}

// Binding Control

UINT CModemServerDriverOptionCard::GetBinding(void)
{
	return m_Binding;
	}

void CModemServerDriverOptionCard::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CModemServerDriverOptionCard::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModemServerDriverOptionsOptionCard);
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver Options (External)
//

// Dynamic Class

AfxImplementDynamicClass(CModemServerDriverOptions, CUIItem);

// Constructor

CModemServerDriverOptions::CModemServerDriverOptions(void)
{	
	m_Connect    = 1;

	m_Timeout    = 240;

	m_SMS	     = 0;	

	m_ModemBrand = 0;

	m_LocAddr    = DWORD(MAKELONG(MAKEWORD(  1, 200), MAKEWORD(168, 192)));

	m_RemAddr    = DWORD(MAKELONG(MAKEWORD(  2, 200), MAKEWORD(168, 192)));

	m_RemMask    = DWORD(MAKELONG(MAKEWORD(255, 255), MAKEWORD(255, 255)));	
	
	m_NewMask    = GetDefInit() ^ 0xFFFF;
	
	m_LogFile    = 0;

	m_Location   = 2;
	}

// UI Management

void CModemServerDriverOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Connect" ) {

			CUITextElement *pUI = pHost->FindUI(L"ModemBrand");

			switch( m_Connect ) {

				case 1:
					((CUITextEnum *) pUI)->SetMask(0x0011);
					break;

				case 2:
				case 3:
					((CUITextEnum *) pUI)->SetMask(0x000F);
					break;
				}

			if( Tag == "Connect" ) {

				m_ModemBrand = 0;

				pHost->UpdateUI("ModemBrand");
				}

			DoEnables(pHost);
			}

		if( Tag == "ModemBrand") {

			if( m_ModemBrand ) {

				m_NewMask = GetDefInit() ^ 0xFFFF;

				pHost->UpdateUI("NewMask");
				}

			DoEnables(pHost);
			}

		if( Tag == "Demand" ) {

			DoEnables(pHost);
			}

		if( Tag == "Route" ) {

			DoEnables(pHost);
			}			
		
		if( Tag == "DynDNS" ) {

			DoEnables(pHost);
			}

		if( Tag == "NewMask" ) {

			if( m_NewMask & 0x4000 ) {

				if( (m_NewMask & 0x6000) != 0x6000 ) {

					m_NewMask |= 0x6000;

					pHost->UpdateUI("NewMask");
					}
				}
			}
		}
	}

void CModemServerDriverOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "MoreMask") {

			UINT uMask = Tree.GetValueAsInteger();			

			if( m_Connect != 1 ) {

				m_NewMask = 0x7F00 | uMask;
				}
			else 
				m_NewMask = uMask;

			continue;
			}		

		if( Name == "More") {

			UINT uMask = Tree.GetValueAsInteger();			

			if( m_Connect != 1 ) {

				m_NewMask = 0x7F80 | uMask;
				}
			else 
				m_NewMask = uMask;

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}

	RegisterHandle();  
	}

// Download Support

BOOL CModemServerDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	//	UINT		m_uMode;
	//	UINT		m_uConnect;
	//	UINT		m_uDevice;
	//	UINT		m_uBaudRate;
	//	UINT		m_uTimeout;
	//	char		m_sInit[48];
	//	char		m_sDial[32];
	//	BOOL		m_fCHAP;
	//	char		m_sUser[32];
	//	char		m_sPass[16];
	//	BOOL		m_fGate;
	//	CIPAddr		m_RemAddr;
	//	CIPAddr		m_RemMask;
	//	CIPAddr		m_LocAddr;
	//	IRecvSMS *	m_pSMS;
	//	DWORD		m_InitMask;
	//	BOOL		m_fLogFile;
	//	UINT		m_uDynDNS;
	//	char		m_sDynUser[32];
	//	char		m_sDynPass[16];
	//	char		m_sDynHost[24];
	//	BYTE		m_bLocation;
	//	BYTE		m_bRoaming;
	//	BYTE		m_bSIMSlot;
	//	BYTE		m_bCellRadio;
	//	BYTE		m_bMore[40];

	Init.AddLong(2);
	Init.AddLong(m_Connect+1);
	Init.AddLong(0);
	Init.AddLong(115200);
	Init.AddLong(m_Timeout);
	Init.AddData(m_Init, 48);
	Init.AddData("",     32);
	Init.AddLong(1);
	Init.AddData(m_User, 32);
	Init.AddData(m_Pass, 16);
	Init.AddLong(0);
	Init.AddLong(m_RemAddr);
	Init.AddLong(m_RemMask);
	Init.AddLong(m_LocAddr);
	Init.AddLong(m_SMS);		

	if( m_ModemBrand ) {

		Init.AddLong(GetDefInit());
		}
	else
		Init.AddLong(m_NewMask ^ 0xFFFF);

	Init.AddLong(m_LogFile);
	Init.AddLong(0);
	Init.AddData("", 32);
	Init.AddData("", 16);
	Init.AddData("", 24);
	Init.AddByte(2);			
	Init.AddByte(2);
	Init.AddByte(2);
	Init.AddByte(0);
	Init.AddData("", 40);

	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(0);

	return TRUE;
	}

// Meta Data Creation

void CModemServerDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Connect);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(Location);
	Meta_AddInteger(SMS);	
	Meta_AddInteger(ModemBrand);
	Meta_AddString (Init);
	Meta_AddString (User);
	Meta_AddString (Pass);
	Meta_AddInteger(LocAddr);
	Meta_AddInteger(RemAddr);
	Meta_AddInteger(RemMask);
	Meta_AddInteger(NewMask);
	Meta_AddInteger(LogFile);
	}

// Implementation

void CModemServerDriverOptions::DoEnables(IUIHost *pHost)
{	
	BOOL fModem = (m_Connect >= 1);	

	BOOL fSMS   = (m_Connect >= 2);

	pHost->EnableUI(this, "SMS",        fSMS);
	pHost->EnableUI(this, "Init",       fModem);
	pHost->EnableUI(this, "ModemBrand", fModem);
	pHost->EnableUI(this, "NewMask",    fModem && !m_ModemBrand);
	}

UINT CModemServerDriverOptions::GetDefInit(void)
{
	switch( m_ModemBrand ) {

		case 1:
			return 0x0018;

		case 2:
			return 0xFF18;

		case 3:
			return 0x001D;

		case 4:
			return 0xFF00;
		}

	return 0xFF18;
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Server Driver Options (Option Card)
//

// Dynamic Class

AfxImplementDynamicClass(CModemServerDriverOptionsOptionCard, CUIItem);

// Constructor

CModemServerDriverOptionsOptionCard::CModemServerDriverOptionsOptionCard(void)
{		
	m_Connect  = 2;	

	m_Timeout  = 240;

	m_SMS	   = 0;	

	m_LocAddr  = DWORD(MAKELONG(MAKEWORD(  1, 200), MAKEWORD(168, 192)));

	m_RemAddr  = DWORD(MAKELONG(MAKEWORD(  2, 200), MAKEWORD(168, 192)));

	m_RemMask  = DWORD(MAKELONG(MAKEWORD(255, 255), MAKEWORD(255, 255)));
	
	m_LogFile  = 0;

	m_Location = 0;
	}

// UI Management

void CModemServerDriverOptionsOptionCard::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			CUITextElement *pUI = pHost->FindUI(L"Connect");

			((CUITextEnum *) pUI)->SetMask(0x0C);

			DoEnables(pHost);
			}

		if( Tag == "Connect" ) {

			DoEnables(pHost);
			}

		if( Tag == "Demand" ) {

			DoEnables(pHost);
			}

		if( Tag == "Route" ) {

			DoEnables(pHost);
			}
		}
	}

// Download Support

BOOL CModemServerDriverOptionsOptionCard::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	//	UINT		m_uMode;
	//	UINT		m_uConnect;
	//	UINT		m_uDevice;
	//	UINT		m_uBaudRate;
	//	UINT		m_uTimeout;
	//	char		m_sInit[48];
	//	char		m_sDial[32];
	//	BOOL		m_fCHAP;
	//	char		m_sUser[32];
	//	char		m_sPass[16];
	//	BOOL		m_fGate;
	//	CIPAddr		m_RemAddr;
	//	CIPAddr		m_RemMask;
	//	CIPAddr		m_LocAddr;
	//	IRecvSMS *	m_pSMS;
	//	DWORD		m_InitMask;
	//	BOOL		m_fLogFile;
	//	UINT		m_uDynDNS;
	//	char		m_sDynUser[32];
	//	char		m_sDynPass[16];
	//	char		m_sDynHost[24];
	//	BYTE		m_bLocation;
	//	BYTE		m_bRoaming;
	//	BYTE		m_bSIMSlot;
	//	BYTE		m_bCellRadio;
	//	BYTE		m_bMore[40];

	Init.AddLong(2);
	Init.AddLong(m_Connect+1);
	Init.AddLong(0);
	Init.AddLong(115200);
	Init.AddLong(m_Timeout);
	Init.AddData(m_Init, 48);
	Init.AddData("",     32);
	Init.AddLong(1);
	Init.AddData(m_User, 32);
	Init.AddData(m_Pass, 16);
	Init.AddLong(0);
	Init.AddLong(m_RemAddr);
	Init.AddLong(m_RemMask);
	Init.AddLong(m_LocAddr);
	Init.AddLong(m_SMS);
	Init.AddLong(0x001D);
	Init.AddLong(m_LogFile);
	Init.AddLong(0);
	Init.AddData("", 32);
	Init.AddData("", 16);
	Init.AddData("", 24);
	Init.AddByte(BYTE(m_Location));		
	Init.AddByte(2);
	Init.AddByte(2);
	Init.AddByte(0);
	Init.AddData("", 40);

	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(0);

	return TRUE;
	}

// Meta Data Creation

void CModemServerDriverOptionsOptionCard::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Connect);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(Location);	
	Meta_AddInteger(SMS);	
	Meta_AddString (Init);
	Meta_AddString (User);
	Meta_AddString (Pass);
	Meta_AddInteger(LocAddr);
	Meta_AddInteger(RemAddr);
	Meta_AddInteger(RemMask);
	Meta_AddInteger(LogFile);
	}

// Implementation

void CModemServerDriverOptionsOptionCard::DoEnables(IUIHost *pHost)
{			
	BOOL fModem = (m_Connect >= 1);		

	BOOL fSMS   = (m_Connect >= 2);	

	pHost->EnableUI(this, "Location", fSMS);
	pHost->EnableUI(this, "SMS",      fSMS);
	pHost->EnableUI(this, "Init",     fModem);
	}

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver (External)
//

// Instantiator

ICommsDriver *	Create_ModemSMSDriver(void)
{
	return New CModemSMSDriver;
	}

// Constructor

CModemSMSDriver::CModemSMSDriver(void)
{
	m_wID	       = WORD(0xFF03);

	m_uType	       = driverModem;
	
	m_Manufacturer = "<System>";
					  	
	m_DriverName   = "SMS via GSM Modem";
	
	m_Version      = "1.00";
	
	m_ShortName    = "SMS Modem";

	m_fSingle      = TRUE;

	m_Binding      = bindStdSerial;

	C3_PASSED();
	}

// Binding Control

UINT CModemSMSDriver::GetBinding(void)
{
	return m_Binding;
	}

void CModemSMSDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CModemSMSDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModemSMSDriverOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver (Option Card)
//

// Instantiator

ICommsDriver *	Create_ModemSMSOptionDriver(void)
{
	return New CModemSMSDriverOptionCard;
	}

// Constructor

CModemSMSDriverOptionCard::CModemSMSDriverOptionCard(void)
{
	m_wID	       = WORD(0xFF06);

	m_uType	       = driverModem;
	
	m_Manufacturer = "Cellular Modem Driver";

	m_DriverName   = "SMS via GSM Modem";
	
	m_Version      = "1.00";
	
	m_ShortName    = "SMS Modem";

	m_fSingle      = TRUE;

	m_Binding      = bindModem;

	C3_PASSED();
	}

// Binding Control

UINT CModemSMSDriverOptionCard::GetBinding(void)
{
	return m_Binding;
	}

void CModemSMSDriverOptionCard::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CModemSMSDriverOptionCard::GetDriverConfig(void)
{
	return AfxRuntimeClass(CModemSMSDriverOptionsOptionCard);
	}

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver Options (External)
//

// Dynamic Class

AfxImplementDynamicClass(CModemSMSDriverOptions, CUIItem);

// Constructor

CModemSMSDriverOptions::CModemSMSDriverOptions(void)
{
	m_NewMask    = 0x7FE7;

	m_Location   = 2;

	m_ModemBrand = 0;
	}

// UI Management

void CModemSMSDriverOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			CUITextElement *pUI = pHost->FindUI(L"ModemBrand");

			((CUITextEnum *) pUI)->SetMask(0x000F);

			DoEnables(pHost);
			}

		if( Tag == "ModemBrand") {

			if( m_ModemBrand ) {

				m_NewMask = GetDefInit() ^ 0xFFFF;

				pHost->UpdateUI("NewMask");
				}

			DoEnables(pHost);
			}

		if( Tag == "NewMask" ) {

			if( m_NewMask & 0x4000 ) {

				if( (m_NewMask & 0x6000) != 0x6000 ) {

					m_NewMask |= 0x6000;

					pHost->UpdateUI("NewMask");
					}
				}
			}
		}
	}

void CModemSMSDriverOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "MoreMask") {

			UINT uMask = Tree.GetValueAsInteger();			

			if( m_Connect != 1 ) {

				m_NewMask = 0x7F00 | uMask;
				}
			else 
				m_NewMask = uMask;

			continue;
			}		

		if( Name == "More") {

			UINT uMask = Tree.GetValueAsInteger();			

			if( m_Connect != 1 ) {

				m_NewMask = 0x7F80 | uMask;
				}
			else 
				m_NewMask = uMask;

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}

	RegisterHandle();  
	}

// Download Support

BOOL CModemSMSDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	//	UINT		m_uMode;
	//	UINT		m_uConnect;
	//	UINT		m_uDevice;
	//	UINT		m_uBaudRate;
	//	UINT		m_uTimeout;
	//	char		m_sInit[48];
	//	char		m_sDial[32];
	//	BOOL		m_fCHAP;
	//	char		m_sUser[32];
	//	char		m_sPass[16];
	//	BOOL		m_fGate;
	//	CIPAddr		m_RemAddr;
	//	CIPAddr		m_RemMask;
	//	CIPAddr		m_LocAddr;
	//	IRecvSMS *	m_pSMS;
	//	DWORD		m_InitMask;
	//	BOOL		m_fLogFile;
	//	UINT		m_uDynDNS;
	//	char		m_sDynUser[32];
	//	char		m_sDynPass[16];
	//	char		m_sDynHost[24];
	//	BYTE		m_bLocation;
	//	BYTE		m_bRoaming;
	//	BYTE		m_bSIMSlot;
	//	BYTE		m_bCellRadio;
	//	BYTE		m_bMore[40];

	Init.AddLong(0);
	Init.AddLong(3);
	Init.AddLong(0);
	Init.AddLong(115200);
	Init.AddLong(30);
	Init.AddData(m_Init, 48);
	Init.AddData("",     32);
	Init.AddLong(1);
	Init.AddData("", 32);
	Init.AddData("", 16);
	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(1); 		

	if( m_ModemBrand ) {

		Init.AddLong(GetDefInit());
		}
	else
		Init.AddLong(m_NewMask ^ 0xFFFF);

	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddData("", 32);
	Init.AddData("", 16);
	Init.AddData("", 24);
	Init.AddByte(2);		
	Init.AddByte(2);
	Init.AddByte(2);
	Init.AddByte(0);
	Init.AddData("", 40);

	return TRUE;
	}

// Meta Data Creation

void CModemSMSDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Location);
	Meta_AddString (Init);
	Meta_AddInteger(NewMask);	
	Meta_AddInteger(ModemBrand);
	}

// Implementation

void CModemSMSDriverOptions::DoEnables(IUIHost *pHost)
{	
	pHost->EnableUI(this, "NewMask", !m_ModemBrand);
	}									     

UINT CModemSMSDriverOptions::GetDefInit(void)
{
	switch( m_ModemBrand ) {

		case 1:
			return 0x0018;

		case 2:
			return 0xFF18;

		case 3:
			return 0x001D;

		case 4:
			return 0xFF00;
		}

	return 0xFF18;
	}

//////////////////////////////////////////////////////////////////////////
//
// SMS Driver Options (Option Card)
//

// Dynamic Class

AfxImplementDynamicClass(CModemSMSDriverOptionsOptionCard, CUIItem);

// Constructor

CModemSMSDriverOptionsOptionCard::CModemSMSDriverOptionsOptionCard(void)
{
	m_Connect  = 0;

	m_Location = 0;	
	}

// UI Management

void CModemSMSDriverOptionsOptionCard::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CModemSMSDriverOptionsOptionCard::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	//	UINT		m_uMode;
	//	UINT		m_uConnect;
	//	UINT		m_uDevice;
	//	UINT		m_uBaudRate;
	//	UINT		m_uTimeout;
	//	char		m_sInit[48];
	//	char		m_sDial[32];
	//	BOOL		m_fCHAP;
	//	char		m_sUser[32];
	//	char		m_sPass[16];
	//	BOOL		m_fGate;
	//	CIPAddr		m_RemAddr;
	//	CIPAddr		m_RemMask;
	//	CIPAddr		m_LocAddr;
	//	IRecvSMS *	m_pSMS;
	//	DWORD		m_InitMask;
	//	BOOL		m_fLogFile;
	//	UINT		m_uDynDNS;
	//	char		m_sDynUser[32];
	//	char		m_sDynPass[16];
	//	char		m_sDynHost[24];
	//	BYTE		m_bLocation;
	//	BYTE		m_bRoaming;
	//	BYTE		m_bSIMSlot;
	//	BYTE		m_bCellRadio;
	//	BYTE		m_bMore[40];

	Init.AddLong(0);
	Init.AddLong(3);
	Init.AddLong(0);
	Init.AddLong(115200);
	Init.AddLong(30);
	Init.AddData(m_Init, 48);
	Init.AddData("",     32);
	Init.AddLong(1);
	Init.AddData("", 32);
	Init.AddData("", 16);
	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddLong(1);
	Init.AddLong(0x001D);
	Init.AddLong(0);
	Init.AddLong(0);
	Init.AddData("", 32);
	Init.AddData("", 16);
	Init.AddData("", 24);
	Init.AddByte(BYTE(m_Location));	
	Init.AddByte(2);
	Init.AddByte(2);
	Init.AddByte(0);
	Init.AddData("", 40);

	return TRUE;
	}

// Meta Data Creation

void CModemSMSDriverOptionsOptionCard::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Connect);
	Meta_AddInteger(Location);
	Meta_AddString (Init);
	}

// End of File
