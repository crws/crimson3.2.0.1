
#include "intern.hpp"

#include "mtsmdbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MTS Modbus Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CMTSModbusDriverOptions, CUIItem);

// Constructor

CMTSModbusDriverOptions::CMTSModbusDriverOptions(void)
{
	m_Protocol = 0;
	
	m_MaxWords = 48;

	m_MaxBits  = 768;
	}

// Download Support

BOOL CMTSModbusDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Protocol));

	return TRUE;
	}

// Meta Data Creation

void CMTSModbusDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Protocol);
	Meta_AddInteger(MaxWords);
	Meta_AddInteger(MaxBits);
	}

//////////////////////////////////////////////////////////////////////////
//
// MTS Modbus Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMTSModbusDeviceOptions, CUIItem);

// Constructor

CMTSModbusDeviceOptions::CMTSModbusDeviceOptions(void)
{
	m_Drop		= 1;

	m_fRLCAuto	= FALSE;
	
	m_fDisable16	= FALSE;
	
	m_fDisable15	= FALSE;
	
	m_Max01		= 512;
	
	m_Max02		= 512;
	
	m_Max03		= 32;
	
	m_Max04		= 32;
	
	m_Max15		= 512;
	
	m_Max16		= 32;

	m_Ping		= 1;

	m_fDisable5	= FALSE;

	m_fDisable6	= FALSE;

	m_fDisableCheck	= FALSE;
	}

// UI Managament

void CMTSModbusDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		// NOTE -- Not honored by driver yet!

		pWnd->EnableUI("RLCAuto", FALSE);
		}
	
	if( Tag.IsEmpty() || Tag == "Disable5" ) {

		pWnd->EnableUI("Disable15", !m_fDisable5);
		}

	if( Tag.IsEmpty() || Tag == "Disable6" ) {

		pWnd->EnableUI("Disable16", !m_fDisable6);
		}
	
	if( Tag.IsEmpty() || Tag == "Disable15" ) {

		pWnd->EnableUI("Max15", !m_fRLCAuto && !m_fDisable15 && !m_fDisable5);

		pWnd->EnableUI("Disable5", !m_fDisable15);
		}

	if( Tag.IsEmpty() || Tag == "Disable16" ) {

		pWnd->EnableUI("Max16", !m_fRLCAuto && !m_fDisable16 && !m_fDisable6);

		pWnd->EnableUI("Disable6", !m_fDisable16);
	      	}

	if( Tag.IsEmpty() || Tag == "RLCAuto" ) {
		
		pWnd->EnableUI("Disable15", !m_fRLCAuto  && !m_fDisable5);
		pWnd->EnableUI("Disable16", !m_fRLCAuto  && !m_fDisable6);

		pWnd->EnableUI("Max01", !m_fRLCAuto);
		pWnd->EnableUI("Max02", !m_fRLCAuto);
		pWnd->EnableUI("Max03", !m_fRLCAuto);
		pWnd->EnableUI("Max04", !m_fRLCAuto);
		
		pWnd->EnableUI("Max15", !m_fRLCAuto && !m_fDisable15);
		pWnd->EnableUI("Max16", !m_fRLCAuto && !m_fDisable16);
		}
	}

// Download Support

BOOL CMTSModbusDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Drop));
	Init.AddByte(BYTE(m_fRLCAuto));
	Init.AddByte(BYTE(m_fDisable15));
	Init.AddByte(BYTE(m_fDisable16));
	Init.AddWord(WORD(m_Max01));
	Init.AddWord(WORD(m_Max02));
	Init.AddWord(WORD(m_Max03));
	Init.AddWord(WORD(m_Max04));
	Init.AddWord(WORD(m_Max15));
	Init.AddWord(WORD(m_Max16));
	Init.AddWord(WORD(m_Ping));
	Init.AddByte(BYTE(m_fDisable5));
	Init.AddByte(BYTE(m_fDisable6));
	Init.AddByte(BYTE(m_fDisableCheck));

	return TRUE;
	}

// Meta Data Creation

void CMTSModbusDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddBoolean(RLCAuto);
	Meta_AddBoolean(Disable16);
	Meta_AddBoolean(Disable15);
	Meta_AddInteger(Max01);
	Meta_AddInteger(Max02);
	Meta_AddInteger(Max03);
	Meta_AddInteger(Max04);
	Meta_AddInteger(Max15);
	Meta_AddInteger(Max16);
	Meta_AddInteger(Ping);
	Meta_AddBoolean(Disable5);
	Meta_AddBoolean(Disable6);
	Meta_AddBoolean(DisableCheck);
	}

//////////////////////////////////////////////////////////////////////////
//
// MTS Modbus Driver
//

// Instantiator

ICommsDriver *	Create_MTSModbusDriver(void)
{
	return New CMTSModbusDriver;
	}

// Constructor

CMTSModbusDriver::CMTSModbusDriver(void)
{
	m_wID		= 0x3398;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "MTS";
	
	m_DriverName	= "Modbus Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Modbus Master";

	AddSpaces();  
	}

// Binding Control

UINT CMTSModbusDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMTSModbusDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CMTSModbusDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMTSModbusDriverOptions);
	}

// Configuration

CLASS CMTSModbusDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMTSModbusDeviceOptions);
	}

// Address Management

BOOL CMTSModbusDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CMTSModbusAddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CMTSModbusDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uTable = pSpace->m_uTable;

	if ( uTable == 5 || uTable == 6 ) {

		uTable -= 4;

		}
		
	UINT uFile = 0;

	UINT uFind = Text.FindRev('(');

	if( uFind < NOTHING ) {

		UINT uLast = Text.Find(')', uFind);

		if( uLast < NOTHING ) {

			if( pSpace->m_uTable == 7 ) {

				uFile = tatoi(Text.Mid(uFind+1));

				Text.Delete(uFind, uLast - uFind + 1);
				}
			else {
				Error.Set( "File numbers not allowed on this address type.",
					   0
					   );

				return FALSE;
				}
			}
		else {
			Error.Set( "No closing bracket on file number.",
				   0
				   );

			return FALSE;
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Extra = uFile;

		Addr.a.m_Table = uTable;

		return TRUE;
		}

	return FALSE;
	}

BOOL CMTSModbusDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uOffset = Addr.a.m_Offset;

		UINT uType   = Addr.a.m_Type;

		if( uType == pSpace->m_uType ) {

			if( Addr.a.m_Table == 7 ) {
				
				Text.Printf( "%s%s(%d)",
					     pSpace->m_Prefix,
					     pSpace->GetValueAsText(uOffset),
					     Addr.a.m_Extra
					     );
				}
			else {
				Text.Printf( "%s%s", 
				             pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset)
				             );
				}
			}
		else {
			Text.Printf( "%s%s.%s", 
				     pSpace->m_Prefix, 
				     pSpace->GetValueAsText (uOffset),
				     pSpace->GetTypeModifier(uType)
				     );
			}
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CMTSModbusDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
		case 2:
		case 5:
		case 6:

			return FALSE;
		}

	return TRUE;
	}

// Implementation

void CMTSModbusDriver::AddSpaces(void)
{
	AddSpace(New CSpace(3, "0",  "Digital Coils",			10, 1, 65535, addrBitAsBit));
	
	AddSpace(New CSpace(4, "1",  "Digital Inputs",			10, 1, 65535, addrBitAsBit));
	
	AddSpace(New CSpace(2, "3",  "Analog Inputs",			10, 1, 65535, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(1, "4",  "Holding Registers",		10, 1, 65535, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(7, "6",  "File Registers",			10, 0, 9999,  addrWordAsWord));

	AddSpace(New CSpace(6, "L3", "Analog Inputs (32-bit)",		10, 1, 65535, addrWordAsLong));
	
	AddSpace(New CSpace(5, "L4", "Holding Registers (32-bit)",	10, 1, 65535, addrWordAsLong));
	
	AddSpace(New CSpace(8, "A",  "Actual Device Address",		10, 0, 0,     addrWordAsWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CMTSModbusAddrDialog, CStdAddrDialog);
		
// Constructor

CMTSModbusAddrDialog::CMTSModbusAddrDialog(CMTSModbusDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "MTSModbusElementDlg";

	m_uDropNumber = pConfig->GetDataAccess("Drop")->ReadInteger(pConfig);
	}

// Overridables

BOOL CMTSModbusAddrDialog::AllowSpace(CSpace *pSpace)
{
	if( pSpace->m_uTable == 5 || pSpace->m_uTable == 6 ) {

		return FALSE;
		}

	if( pSpace->m_uTable == 8 && m_uDropNumber < 256 ) {

		return FALSE;
		}
	
	return TRUE;
	}


void CMTSModbusAddrDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	if( m_pSpace && m_pSpace->m_uTable == 7 ) {

		if( fEnable ) {

			UINT uFind = Text.Find('(');

			UINT uLast = Text.Find(')', uFind);

			GetDlgItem(2002).SetWindowText(Text.Left(uFind));

			GetDlgItem(2004).SetWindowText("(");

			GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1, uLast-uFind-1));

			GetDlgItem(2006).SetWindowText(")");
			}
		else {
			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2006).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fEnable);
		
		GetDlgItem(2005).EnableWindow(fEnable);
		
		GetDlgItem(2006).EnableWindow(fEnable);
		}
	else {
		if( fEnable ) {

			GetDlgItem(2002).SetWindowText(Text);

			GetDlgItem(2005).SetWindowText("");

			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2006).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		
		GetDlgItem(2006).EnableWindow(FALSE);
		}
	}

CString CMTSModbusAddrDialog::GetAddressText(void)
{
	if( m_pSpace && m_pSpace->m_uTable == 7 ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += "(";
		
		Text += GetDlgItem(2005).GetWindowText();

		Text += ")";

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

// End of File
