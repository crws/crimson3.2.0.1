
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EMFXSER_HPP
	
#define	INCLUDE_EMFXSER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Emerson FX Serial Device Options
//

class CEmersonFXSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonFXSerialDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		CString	m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// Space Contractions
#define	BB	addrBitAsBit
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

//////////////////////////////////////////////////////////////////////////
//
// Emerson FX Serial Driver
//

class CEmersonFXSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEmersonFXSerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		BOOL	IsStringItem(UINT uTable);
		UINT	GetLetter2(UINT uOffset);
		BOOL	HasDescription(UINT uTable, UINT uOffset);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);

		// Helpers
		BOOL	HasPar(UINT uTable, UINT uOffset);

		// Information
		UINT	GetItemType(UINT uT, UINT uO);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson FX Serial Address Selection
//

class CEmersonFXSerialDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CEmersonFXSerialDialog(CEmersonFXSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		CItem	*m_pCfg;
		BOOL	m_fUpdateInfo;

		CEmersonFXSerialDriver * m_pDriverData;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		BOOL	OnButtonClicked(UINT uID);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overrideables
		void	ShowAddress(CAddress Addr);

		// Information
		void	GetInfo(UINT uT, UINT uO);
		void	InitFXInfo(void);
		CString	GetCString(char cNext);
		CString	GetDString(char cNext);
		CString	GetFString(char cNext);
		CString	GetKString(char cNext);
		CString	GetPString(char cNext);
		CString	GetTString(char cNext);

		// Helpers
		BOOL	IsNotERR(UINT uT, TCHAR cPfx);
	};

#define	PARBIT	0x0400	// item may have a parameter
#define	DATADEC	0x0000	// data transferred in decimal
#define	DATAHEX	0x0200	// data transferred in hex
#define	DATABIN	0x0100	// data transferred in binary
#define	DATATXT	0x0300	// data transferred in ascii

#define	MAXPAR	0xFF

#define	MAXVAL	((('Z' - 'A') << 11) + MAXPAR)

#define	ERRCFGO	(('E' - '@') << 11) + DATATXT;

// Table definitions
#define	HAA	'A'
#define	HAB	'B'
#define	HAC	'C'
#define	HAD	'D'
#define	HAE	'E'
#define	HAF	'F'
#define	HAG	'G'
#define	HAH	'H'
#define	HAI	'I'
#define	HAJ	'J'
#define	HAK	'K'
#define	HAL	'L'
#define	HAM	'M'
#define	HAN	'N'
#define	HAO	'O'
#define	HAP	'P'
#define	HAQ	'Q'
#define	HAR	'R'
#define	HAS	'S'
#define	HAT	'T'
#define	HAU	'U'
#define	HAV	'V'
#define	HAW	'W'
#define	HAX	'X'
#define	HAY	'Y'
#define	HAZ	'Z'
// string response holders
#define	BEGSTRR	 1
#define	AIR	 1
#define	CBR	 2
#define	DUR	 3
#define	EQR	 4
#define	IDR	 5
#define	PIR	 6
#define	QCR	 7
#define	TMR	 8
#define	TWR	 9
#define	VUR	10
// Error Response String
#define	ERR	11
#define	ENDSTRR	11

// End of File

#endif
