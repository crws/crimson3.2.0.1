
#include "intern.hpp"

#include "schmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Schneider via MODBUS Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSchneiderModbusDeviceOptions, CModbusDeviceOptions);

// Constructor

CSchneiderModbusDeviceOptions::CSchneiderModbusDeviceOptions(void)
{
	m_Ping		= 0;

	m_ByteR		= 2;

	m_ByteL		= 2;
	}

// Download Support

BOOL CSchneiderModbusDeviceOptions::MakeInitData(CInitData &Init)
{	
	CModbusDeviceOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_ByteR));
  	Init.AddByte(BYTE(m_ByteL));
  
	return TRUE;
	}

// Meta Data Creation

void CSchneiderModbusDeviceOptions::AddMetaData(void)
{
	CModbusDeviceOptions::AddMetaData();

	Meta_AddInteger(ByteR);
	Meta_AddInteger(ByteL);
	} 


//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via MODBUS Driver
//

// Instantiator

ICommsDriver *	Create_SchneiderModbusSerialDriver(void)
{
	return New CSchneiderModbusSerialDriver;
	}

// Constructor

CSchneiderModbusSerialDriver::CSchneiderModbusSerialDriver(void)
{
	m_wID		= 0x408A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Schneider";
	
	m_DriverName	= "PLC via Modbus";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Schneider Master";

	DeleteAllSpaces();

	AddSpaces();
	}

// Configuration

CLASS CSchneiderModbusSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSchneiderModbusDeviceOptions);
	}

// Implementation

void CSchneiderModbusSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "%MW", "Memory Words",	10, 0, 65534, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(2, "%IW", "Input Words",	10, 0, 65534, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(3, "%M",  "Memory Bits",	10, 0, 65534, addrBitAsBit));
	
	AddSpace(New CSpace(4, "%I",  "Input Bits",	10, 0, 65534, addrBitAsBit));
	}

//////////////////////////////////////////////////////////////////////////
//
// Schneider Modbus TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSchneiderModbusTCPDeviceOptions, CUIItem);       

// Constructor

CSchneiderModbusTCPDeviceOptions::CSchneiderModbusTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_fDisable16	= FALSE;
	
	m_fDisable15	= FALSE;

	m_fDisable5	= FALSE;

	m_fDisable6	= FALSE;

	m_PingReg	= 0;

	m_Max01		= 512;
	
	m_Max02		= 512;
	
	m_Max03		= 32;
	
	m_Max04		= 32;
	
	m_Max15		= 512;
	
	m_Max16		= 32;

	m_ByteR		= 2;

	m_ByteL		= 2;

	SetPages(2);
	}

// UI Managament

void CSchneiderModbusTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		
		if( Tag.IsEmpty() || Tag == "Disable5" ) {

			pWnd->EnableUI("Disable15", !m_fDisable5);
			}

		if( Tag.IsEmpty() || Tag == "Disable6" ) {

			pWnd->EnableUI("Disable16", !m_fDisable6);
			}
	
		if( Tag.IsEmpty() || Tag == "Disable15" ) {

			pWnd->EnableUI("Max15", !m_fDisable15 && !m_fDisable5);

			pWnd->EnableUI("Disable5", !m_fDisable15);
			}

		if( Tag.IsEmpty() || Tag == "Disable16" ) {

			pWnd->EnableUI("Max16", !m_fDisable16 && !m_fDisable6);

			pWnd->EnableUI("Disable6", !m_fDisable16);
	      		}
		}
	}

// Download Support

BOOL CSchneiderModbusTCPDeviceOptions::MakeInitData(CInitData &Init)
{	
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_fDisable15));
	Init.AddByte(BYTE(m_fDisable16));
	Init.AddByte(BYTE(m_fDisable5));
	Init.AddByte(BYTE(m_fDisable6));
	Init.AddWord(WORD(m_PingReg));
	Init.AddWord(WORD(m_Max01));
	Init.AddWord(WORD(m_Max02));
	Init.AddWord(WORD(m_Max03));
	Init.AddWord(WORD(m_Max04));
	Init.AddWord(WORD(m_Max15));
	Init.AddWord(WORD(m_Max16));
	Init.AddByte(BYTE(m_ByteR));
	Init.AddByte(BYTE(m_ByteL));
	
	return TRUE;
	}

// Meta Data Creation

void CSchneiderModbusTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddBoolean(Disable15);
	Meta_AddBoolean(Disable16);
	Meta_AddBoolean(Disable5);
	Meta_AddBoolean(Disable6);
	Meta_AddInteger(PingReg);
	Meta_AddInteger(Max01);
	Meta_AddInteger(Max02);
	Meta_AddInteger(Max03);
	Meta_AddInteger(Max04);
	Meta_AddInteger(Max15);
	Meta_AddInteger(Max16);
	Meta_AddInteger(ByteR);
	Meta_AddInteger(ByteL);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via MODBUS TCP/IP Master Driver
//

// Instantiator

ICommsDriver *	Create_SchneiderModbusTCPDriver(void)
{
	return New CSchneiderModbusTCPDriver;
	}

// Constructor

CSchneiderModbusTCPDriver::CSchneiderModbusTCPDriver(void)
{
	m_wID		= 0x408B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Schneider";
	
	m_DriverName	= "PLC via Modbus TCP/IP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Schneider Master";

	DeleteAllSpaces();

	AddSpaces();
	}

// Configuration

CLASS CSchneiderModbusTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSchneiderModbusTCPDeviceOptions);
	}

// Implementation

void CSchneiderModbusTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "%MW", "Memory Words",	10, 0, 65534, addrWordAsWord, addrWordAsReal));
		
	AddSpace(New CSpace(2, "%IW", "Input Words",	10, 0, 65534, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(3, "%M",  "Memory Bits",	10, 0, 65534, addrBitAsBit));
	
	AddSpace(New CSpace(4, "%I",  "Input Bits",	10, 0, 65534, addrBitAsBit));
	}


// End of File
