
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "ei3504.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "ei3504.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "ei3504.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "ei3504.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CEuroInvensys3504TCPMasterDeviceOptions
//

CEuroInvensys3504TCPDeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP,,CUIIPAddress,,"
	"Indicate the IP address device."
	"\0"

	"Socket,TCP Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the TCP port number on which the protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Unit,Unit Number,,CUIEditInteger,|0||0|255,"
	"Indicate the unit within the server that you wish to address. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CEuroInvensys3504TCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Socket,Unit\0"
	"G:1,root,Protocol Options,Keep,Time1,Time3,Time2\0"
	"\0"
END

CEuroInvensys3504AddressDlg DIALOG 0, 0, 0, 0
CAPTION "Item Selection"
BEGIN
	GROUPBOX	"&Data Item",	1000,		  4,   4, 148, 182
	LISTBOX				1001,		 10,  18, 136, 168, XS_LISTBOX | LBS_USETABSTOPS
	GROUPBOX	"&Parameter",	2000,		156,   4, 184,  86
	LTEXT		"<Pfx>",	2001,		166,  17,  22,  12
	EDITTEXT			2002,		194,  16,  24,	12
	LTEXT		"&Name:"	2011,		166,  36,  24,  12
	COMBOBOX			2021,		194,  34, 140,  48, XS_DROPDOWNLIST
	LTEXT		"Loop:	"	2030,		166,  55,  24,  12
	COMBOBOX			2031,		194,  52,  24,  30, XS_DROPDOWNLIST
	LTEXT		"Segment:"	2034,		228,  55,  24,  12
	COMBOBOX			2035,		256,  52,  24,  30, XS_DROPDOWNLIST
	LTEXT		"Modbus:",	2032,		166,  70,  24,	12
	LTEXT		" ",		2033,		194,  70,  20,  12
	GROUPBOX	"Details",	3000,		156,  94, 184,  64
	LTEXT		"Type:",	3001,		164, 108,  40,  10
	LTEXT		"Real",		3002,		208, 108,  40,  10
	LTEXT		"Minimum:",	3003,		164, 120,  40,  10
	LTEXT		"<min>",	3004,		208, 120, 120,  10
	LTEXT		"Maximum:",	3005,		164, 132,  40,  10
	LTEXT		"<max>",	3006,		208, 132, 120,  10
	LTEXT		"Radix:",	3007,		164, 144,  40,  10
	LTEXT		"Decimal",	3008,		208, 144,  40,  10
	GROUPBOX	"Data &Type",	4000,		  4, 190, 128,  42
	LISTBOX				4001,		 10, 202, 114,  36, XS_LISTBOX | LBS_USETABSTOPS

	DEFPUSHBUTTON   "OK",		IDOK,		156, 218,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	208, 218,  40,  14, XS_BUTTONREST

END

// End of File
