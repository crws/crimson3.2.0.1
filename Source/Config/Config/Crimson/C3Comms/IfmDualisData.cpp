
#include "intern.hpp"

#include "IfmDualisData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IfmDualisDeviceOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Data Driver
//

// Instantiator

ICommsDriver *	Create_IfmDualisDataDriver(void)
{
	return New CIfmDualisData;
	}

// Constructor

CIfmDualisData::CIfmDualisData(void)
{
	m_wID		= 0x3545;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "ifm effector";
	
	m_DriverName	= "Dualis Object Recognition Sensor Data";
	
	m_Version	= "1.00";

	m_DevRoot	= "Dev";
	
	m_ShortName	= "Dualis Data";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Configuration

CLASS CIfmDualisData::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CIfmDualisData::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDualisVisionSensorDataDeviceOptions);
	}

// Binding Control

UINT CIfmDualisData::GetBinding(void)
{
	return bindEthernet;
	}

// Address Management

// Implementation

void CIfmDualisData::AddSpaces(void)
{
	AddSpace(New CSpace(     "Protocol",	"Protocol Version",		1,	  addrLongAsLong));
	AddSpace(New CSpace(     "Error",	"Error Code",			2,	  addrLongAsLong));
	AddSpace(New CSpace(     "Trigger",	"Trigger Pulse",		3,	  addrLongAsLong));
	//AddSpace(New CSpace(     "SelApp",	"Select the Application",	4,	  addrLongAsLong));
	//AddSpace(New CSpace(     "ActRes",	"Activate Result Output",	5,	  addrLongAsLong));
	AddSpace(New CSpace('s', "Stats",	"Statistics",			10, 0, 2, addrLongAsLong));
	AddSpace(New CSpace('R', "Result",	"Last Result",			10, 0, 1, addrLongAsLong));
	}

// End of File
