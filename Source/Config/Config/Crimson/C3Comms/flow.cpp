
#include "intern.hpp"

#include "flow.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// FlowComm Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CFlowCommMasterDeviceOptions, CUIItem);       

// Constructor

CFlowCommMasterDeviceOptions::CFlowCommMasterDeviceOptions(void)
{
	m_Drop = 0;
	}

// Download Support

BOOL CFlowCommMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
		
	return TRUE;
	}

// Meta Data Creation

void CFlowCommMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}



//////////////////////////////////////////////////////////////////////////
//
// FlowComm Master Driver
//

// Instantiator

ICommsDriver * Create_FlowCommMasterDriver(void)
{
	return New CFlowCommMasterDriver;
	}

// Constructor

CFlowCommMasterDriver::CFlowCommMasterDriver(void)
{
	m_wID		= 0x4025;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "FlowCom";
	
	m_DriverName	= "Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "FlowCom";

	AddSpaces();
	}

// Destructor

CFlowCommMasterDriver::~CFlowCommMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CFlowCommMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CFlowCommMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CFlowCommMasterDriver::GetDeviceConfig(void)
{
	// Protocol supports multi-drop, but FlowComm does not support multi-drop at this time.

	return AfxRuntimeClass(CFlowCommMasterDeviceOptions);
	}


// Address Management

BOOL CFlowCommMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CFlowCommAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CFlowCommMasterDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	UINT uOffset = 0;

	SetHexadecimal(Text, uOffset);

	if( uOffset ) {

		Addr.a.m_Offset = (uOffset - FLOW_OFFSET) * FLOW_MAX;
		Addr.a.m_Table  = 1;
		Addr.a.m_Extra	= 0;
		Addr.a.m_Type   = addrByteAsByte;
			
		return TRUE;
		}

	Error.Set(CPrintf(CString(IDS_DRIVER_ADDRRANGE), TEXT("0x20"), TEXT("0xFF")), 0);

	return FALSE;
       	}

BOOL CFlowCommMasterDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{	
	UINT uOffset = (Addr.a.m_Offset / FLOW_MAX) + FLOW_OFFSET;
	
	if( uOffset >= 0x20 ) {

		Text.Printf("%2.2X", uOffset);

		return TRUE;
		}
	
	return FALSE;
	}


// Implementation

void CFlowCommMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"I",	"Instruction Code",	10, 0, 80, addrByteAsByte));
	}

// Helper

BOOL CFlowCommMasterDriver::SetHexadecimal(CString &Text, UINT &uTarget)
{
	UINT uLength = Text.GetLength();

	if ( uLength != 2 ) {

		return FALSE;
		}
	
	Text = Text.Left(uLength);

	Text = Text.ToUpper();

	for( UINT u = 0; u < uLength - 1; u++ ) {

		if( u == 0 && ( Text[u] < '2' || Text[u] > 'F' ) ) {

			return FALSE;
			}
		
		if( u == 1 && ( Text[u] < '0' || Text[u] > 'F' ) ) {

			return FALSE;
			}
		}
		
	uTarget = tstrtoul(Text, NULL, 16);

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// FlowComm Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CFlowCommAddrDialog, CStdDialog);
		
// Constructor

CFlowCommAddrDialog::CFlowCommAddrDialog(CFlowCommMasterDriver &Driver, CAddress &Addr, BOOL fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	SetName(fPart ? TEXT("PartFlowCommAddrDlg") : TEXT("FullFlowCommAddrDlg"));
	}

// Message Map

AfxMessageMap(CFlowCommAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,    OnOkay )
	AfxDispatchCommand(IDCLEAR, OnClear)

	AfxDispatchNotify(1002, LBN_DBLCLK, OnDblClk)

	AfxMessageEnd(CFlowCommAddrDialog)
	};

// Message Handlers

BOOL CFlowCommAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	ShowAddress(*m_pAddr);

	ShowInformation();
	
	return FALSE;
	}

// Notification Handlers

void CFlowCommAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

// Command Handlers

BOOL CFlowCommAddrDialog::OnOkay(UINT uID)
{
	CError   Error;

	CString  Text;

	CAddress Addr;

	Text = GetDlgItem(1002).GetWindowText();
	
	if( m_pDriver->ParseAddress(Error, Addr, NULL, Text) ) {

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	Error.Show(ThisObject);

	SetDlgFocus(1002);

	return FALSE;
	}

BOOL CFlowCommAddrDialog::OnClear(UINT uID)
{
	GetDlgItem(1002).SetWindowText("");

	return TRUE;
	}

// Implementation

void CFlowCommAddrDialog::ShowAddress(CAddress &Addr)
{
	CString Text = "";

	m_pDriver->ExpandAddress(Text, NULL, Addr);
	
	GetDlgItem(1002).SetWindowText(Text);
	}

void CFlowCommAddrDialog::ShowInformation(void)
{
	if( !m_fPart ) {

		CString Text = CPrintf(CString(IDS_DRIVER_ADDRRANGE), TEXT("0x20"), TEXT("0xFF"));

		Text += "\n\n";
		
		Text += CString(IDS_FLOW_FORMAT);
				
		GetDlgItem(2001).SetWindowText(Text);
		}
	}



// End of File