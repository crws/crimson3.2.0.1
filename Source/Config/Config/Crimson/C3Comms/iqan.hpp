
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IQAN_HPP
	
#define	INCLUDE_IQAN_HPP 


/////////////////////////////////////////////////////////////////////////
//
// Parker IQAN CAN Bus Driver Options
//

class CIqanDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIqanDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Source;
		UINT m_Op;
	
			
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};




/////////////////////////////////////////////////////////////////////////
//
// Parker IQAN CAN Bus Device Options
//

class CIqanDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIqanDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Send1;
		UINT m_Rate1;
		UINT m_Send2;
		UINT m_Rate2;
		UINT m_Send3;
		UINT m_Rate3;
		UINT m_Send4;
		UINT m_Rate4;
		
					
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN CAN Bus Driver
//

class CIqanDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIqanDriver(void);

		// Driver Data
		UINT GetFlags(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		 

	protected:

		// Implementation
		void AddSpaces(void);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN CAN Bus Driver Address Selection Dialog
//

class CIqanAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CIqanAddrDialog(CIqanDriver &Driver, CAddress &Addr,  CItem *pConfig, BOOL fPart);

	protected:

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN CAN Bus Driver Space
//

class CIqanSpace : public CSpace
{
	public:
		// Constructors
		CIqanSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT io, UINT ao);

		// Public Data
		UINT m_uMinBit;
		UINT m_uMaxBit;

		// Matching
		BOOL MatchSpace(CAddress const &Addr);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
	};

// End of File

#endif
