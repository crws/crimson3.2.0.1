
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RAWUDP_HPP
	
#define	INCLUDE_RAWUDP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedItem : public CItem { };

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Driver Options
//

class CRawUDPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRawUDPDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		CCodedItem * m_pService;
		UINT	     m_Port;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Driver
//

class CRawUDPDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CRawUDPDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
