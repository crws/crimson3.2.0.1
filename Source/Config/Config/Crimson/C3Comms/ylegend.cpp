
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#include "ylegend.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Legend/SMC3010 Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaLegendDriver(void)
{
	return New CYaskawaLegendDriver;
	}

// Constructor

CYaskawaLegendDriver::CYaskawaLegendDriver(void)
{
	m_wID		= 0x3391;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "Legend/SMC 3010";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Yaskawa Legend";

	AddSpaces();
	}

// Binding Control

UINT	CYaskawaLegendDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CYaskawaLegendDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void	CYaskawaLegendDriver::AddSpaces(void)
{
	AddSpace( New CSpace(addrNamed,	"AB",	"Abort Motion and Program",	10, 1, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"ABA",	"Abort Motion Only",		10, 2, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"AC",	"Acceleration",			10, 3, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"AD",	"After Distance",		10, 4, 0, addrLongAsLong) );
	AddSpace( New CSpace(	5,	"AF",	"Enable Digital Feedback",	10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(	6,	"AFA",	"Enable Analog Feedback",	10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"AI",	"After Input",			10, 7, 0, addrLongAsLong) );
	AddSpace( New CSpace(	8,	"AL",	"Arm Latch",			10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(	9,	"AM",	"After Motion",			10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"AO",	"Analog Output",		10, 10, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"AP",	"After Absolute Postion",	10, 11, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"AR",	"After Relative Distance",	10, 12, 0, addrLongAsLong) );
	AddSpace( New CSpace(	13,	"AS",	"At Speed",			10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"AT",	"At Time",			10, 14, 0, addrLongAsLong) );
	AddSpace( New CSpace(	15,	"BG",	"Begin Motion",			10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"BL",	"Reverse Software Limit",	10, 16, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"BN",	"Burn",				10, 17, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"BP",	"Burn Program",			10, 18, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"BV",	"Burn Variables",		10, 19, 0, addrBitAsBit) );

	AddSpace( New CSpace(addrNamed,	"CB",	"Clear Bit",			10, 20, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CD",	"Contour Data",			10, 21, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CE",	"Configure Encoder",		10, 22, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CM",	"Contour Mode (WO)",		10, 23, 0, addrBitAsBit) );

	AddSpace( New CSpace(addrNamed,	"DC",	"Deceleration",			10, 24, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"DE",	"Dual (Auxiliary) Encoder Position",10, 25, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"DP",	"Define Position",		10, 26, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"DT",	"Delta Time",			10, 27, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"DV",	"Dual Velocity Enable (Dual Loop)",10, 28, 0, addrLongAsLong) );

	AddSpace( New CSpace(	29,	"EA",	"ECAM Master Axis",		10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"EB",	"Enable ECAM Mode Enable",	10, 30, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"EC",	"ECAM Counter",			10, 31, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"EG",	"ECAM Engage",			10, 32, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"EM",	"ECAM Cycle",			10, 33, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"EQ",	"ECAM Quit (Disengage)",	10, 34, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"ER",	"Error Limit",			10, 35, 0, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"FA",	"Acceleration Feed Forward",	10, 36, 0, addrRealAsReal) );
	AddSpace( New CSpace(	37,	"FE",	"Find Edge",			10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(	38,	"FI",	"Find Index",			10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"FL",	"Forward Software Limit",	10, 39, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"FV",	"Velocity Feed Forward",	10, 40, 0, addrLongAsLong) );

	AddSpace( New CSpace(	41,	"GA",	"Master Axis for Gearing",	10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"GR",	"Gear Ratio",			10, 42, 0, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"HM",	"Home",				10, 43, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"HX",	"Halt Program (WO)",		10, 44, 0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"IL",	"Integrator Limit",		10, 45, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"IP",	"Increment Position",		10, 46, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"IT",	"Independent Time Constant",	10, 47, 0, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"JG",	"Jog",				10, 48, 0, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"KD",	"Derivative Constant",		10, 49, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"KI",	"Integrator",			10, 50, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"KP",	"Proportional Constant",	10, 51, 0, addrRealAsReal) );

	AddSpace( New CSpace(	52,	"MC",	"Motion Complete (In Position)",10, 1, 4, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"MF",	"Forward Motion to Position",	10, 53, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"MM",	"Master Modulus",		10, 54, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"MO",	"Motor Off",			10, 55, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"MR",	"Reverse Motion to Position",	10, 56, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"MT",	"Motor Type",			10, 57, 0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"NA",	"Number of Axes",		10, 58, 0, addrLongAsLong) );

	AddSpace( New CSpace(	59,	"OB",	"Output Bit",			10, 1, 4, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"OE",	"Off on Error - Enable/Disable",10, 60, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"OF",	"Offset",			10, 61, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"OP",	"Output Port",			10, 62, 0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"PA",	"Position Absolute",		10, 63, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"PR",	"Position Relative",		10, 64, 0, addrRealAsReal) );

	AddSpace( New CSpace(	65,	"RL",	"Report Latched Position",	10, 1, 4, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"RS",	"Reset",			10, 66, 0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"SB",	"Set Bit",			10, 67, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"SH",	"Servo Here",			10, 68, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"SP",	"Speed",			10, 69, 0, addrRealAsReal) );
	AddSpace( New CSpace(	70,	"ST",	"Stop",				10, 1, 4, addrBitAsBit) );

	AddSpace( New CSpace(addrNamed,	"TB",	"Tell Status Byte",		10, 71, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"TC",	"Tell Error Code",		10, 72, 0, addrLongAsLong) );
	AddSpace( New CSpace(	73,	"TD",	"Tell Dual Encoder",		10, 1, 4, addrRealAsReal) );
	AddSpace( New CSpace(	74,	"TE",	"Tell Error",			10, 1, 4, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"TI",	"Tell Inputs",			10, 75, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"TL",	"Torque Limit",			10, 76, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"TM",	"Time Command",			10, 77, 0, addrLongAsLong) );
	AddSpace( New CSpace(	78,	"TP",	"Tell Position",		10, 1, 4, addrLongAsLong) );
	AddSpace( New CSpace(	79,	"TS",	"Tell Switches",		10, 1, 4, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"TT",	"Tell Torque",			10, 80, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"TV",	"Tell Velocity",		10, 81, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"TW",	"Timeout for In Position",	10, 82, 0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"VA",	"Vector Acceleration",		10, 83, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"VD",	"Vector Deceleration",		10, 84, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"VE",	"Vector Sequence End",		10, 85, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"VR",	"Vector Speed Ratio",		10, 86, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"VS",	"Vector Speed",			10, 87, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"VT",	"Vector Time Constant",		10, 88, 0, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"WC",	"Wait for Contour Data",	10, 89, 0, addrBitAsBit) );
	AddSpace( New CSpace(addrNamed,	"WT",	"Wait",				10, 90, 0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"PF",	"Position Format (x.y)",	10, 91, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"VF",	"Variable Format (x.y)",	10, 92, 0, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"YP",	"User Variable YP",		10, 93, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YQ",	"User Variable YQ",		10, 94, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YR",	"User Variable YR",		10, 95, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YS",	"User Variable YS",		10, 96, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YT",	"User Variable YT",		10, 97, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YU",	"User Variable YU",		10, 98, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YV",	"User Variable YV",		10, 99, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YW",	"User Variable YW",		10, 100, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YX",	"User Variable YX",		10, 101, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"YY",	"User Variable YY",		10, 102, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"IA",	"Ethernet IP Address",		10, 103, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"IH",	"Open Internet Handle (WO)",	10, 104, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"IHA",	"Internet Handle - Handle",	10, 105, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"IHB",	"Internet Handle - IP",		10, 106, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"IHC",	"Internet Handle - Port",	10, 107, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"IHD",	"Internet Handle - Protocol",	10, 108, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"IHE",	"Internet Handle - Terminate (WO)",	10, 109, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CH",	"Connect Handle (WO)",		10, 110, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CHA",	"Connect Handle - Axis",	10, 111, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CHB",	"Connect Handle - Send",	10, 112, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CHC",	"Connect Handle - Receive",	10, 113, 0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"XQ",	"Execute Program (WO)",		10, 114, 0, addrLongAsLong) );
	}


//////////////////////////////////////////////////////////////////////////
//
// Yaskawa TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaTCPDeviceOptions, CUIItem);

// Constructor

CYaskawaTCPDeviceOptions::CYaskawaTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 23;

	m_DefHandle = 9;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CYaskawaTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "DefHandle" ) {

			pWnd->UpdateUI("DefHandle");
			}
		}
	}

// Download Support

BOOL CYaskawaTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_DefHandle));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(DefHandle);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_CYaskawaTCPDriver(void)
{
	return New CYaskawaTCPDriver;
	}

// Constructor

CYaskawaTCPDriver::CYaskawaTCPDriver(void)
{
	m_wID		= 0x3509;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "TCP/IP Master (Legacy Only)";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Yaskawa SMC TCP/IP Master";
	}

// Binding Control

UINT CYaskawaTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYaskawaTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYaskawaTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CYaskawaTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Universal SMC Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaUnivSMCDriver(void)
{
	return New CYaskawaUnivSMCDriver;
	}

// Constructor

CYaskawaUnivSMCDriver::CYaskawaUnivSMCDriver(void)
{
	m_wID		= 0x400F;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "Universal SMC";
	
	m_Version	= "1.30";
	
	m_ShortName	= "Yaskawa Universal SMC";

	AddUnivSpaces();
	}

// Binding Control

UINT	CYaskawaUnivSMCDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CYaskawaUnivSMCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void	CYaskawaUnivSMCDriver::AddUnivSpaces(void)
{
	AddSpace( New CSpace(addrNamed,	"AB",	"Abort Motion and Program",	10,  1, 0, addrBitAsBit  ) );
	AddSpace( New CSpace(addrNamed,	"ABA",	"Abort Motion Only",		10,  2, 0, addrBitAsBit  ) );
	AddSpace( New CSpace(	3,	"AC",	"Acceleration",			10,  1, 8, addrLongAsLong) );
	AddSpace( New CSpace(	126,	"AE",	"Absolute Encoder",		10,  1, 8, addrLongAsLong) );
	AddSpace( New CSpace(	5,	"AF",	"Enable Digital Feedback",	10,  1, 8, addrBitAsBit  ) );
	AddSpace( New CSpace(	6,	"AFA",	"Enable Analog Feedback",	10,  1, 8, addrBitAsBit  ) );
	AddSpace( New CSpace(	8,	"AL",	"Arm Latch",			10,  1, 8, addrBitAsBit  ) );
	AddSpace( New CSpace(addrNamed,	"AO",	"Analog Output",		10, 10, 0, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"BG",	"Begin Motion (Axis Pattern)",	10, 15, 0, addrLongAsLong) );
	AddSpace( New CSpace(	16,	"BL",	"Reverse Software Limit",	10,  1, 8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"BN",	"Burn",				10, 17, 0, addrBitAsBit  ) );
	AddSpace( New CSpace(addrNamed,	"BP",	"Burn Program",			10, 18, 0, addrBitAsBit  ) );
	AddSpace( New CSpace(addrNamed,	"BV",	"Burn Variables",		10, 19, 0, addrBitAsBit  ) );

	AddSpace( New CSpace(addrNamed,	"CB",	"Clear Bit (Bit Number)",	10, 20, 0, addrLongAsLong) );
	AddSpace( New CSpace(	21,	"CD",	"Contour Data",			10,  1, 8, addrLongAsLong) );
	AddSpace( New CSpace(	22,	"CE",	"Configure Encoder",		10,  1, 8, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"CM",	"Contour Mode (Axis Pattern)",	10, 23, 0, addrBitAsBit  ) );

	AddSpace( New CSpace(	24,	"DC",	"Deceleration",			10,  1, 8, addrLongAsLong) );
	AddSpace( New CSpace(	25,	"DE",	"Dual (Aux) Encoder Position",	10,  1, 8, addrRealAsReal) );
	AddSpace( New CSpace(	26,	"DP",	"Define Position",		10,  1, 8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"DT",	"Delta Time",			10, 27, 0, addrLongAsLong) );
	AddSpace( New CSpace(	28,	"DV",	"Dual Velocity Enable",		10,  1, 8, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"EA",	"ECAM Master Axis (Axis)",	10, 29,  0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"EB",	"ECAM Mode Enable",		10, 30,  0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"EC",	"ECAM Counter",			10, 31,  0, addrLongAsLong) );
	AddSpace( New CSpace(	32,	"EG",	"ECAM Engage",			10,  1,  8, addrLongAsLong) );
	AddSpace( New CSpace(	33,	"EM",	"ECAM Cycle",			10,  1,  8, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"EQ",	"ECAM Quit",			10, 34,  0, addrLongAsLong) );
	AddSpace( New CSpace(	35,	"ER",	"Error Limit",			10,  1,  8, addrRealAsReal) );

	AddSpace( New CSpace(	36,	"FA",	"Acceleration Feed Forward",	10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"FE",	"Find Edge (Axis Pattern)",	10, 37,  0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"FI",	"Find Index (Axis)",		10, 38,  0, addrLongAsLong) );
	AddSpace( New CSpace(	39,	"FL",	"Forward Software Limit",	10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	40,	"FV",	"Velocity Feed Forward",	10,  1,  8, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"GR",	"Gear Ratio",			10, 42,  0, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"HM",	"Home (Axis Pattern)",		10, 43,  0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"HX",	"Halt Program",		10, 44,  0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"IA",	"IP Address (TCP-Read Only)",	10, 103, 0, addrLongAsLong) );
	AddSpace( New CSpace(	45,	"IL",	"Integrator Limit",		10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	46,	"IP",	"Increment Position",		10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	47,	"IT",	"Independent Time Constant",	10,  1,  8, addrRealAsReal) );

	AddSpace( New CSpace(	48,	"JG",	"Jog",				10,  1,  8, addrRealAsReal) );

	AddSpace( New CSpace(	49,	"KD",	"Derivative Constant",		10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	50,	"KI",	"Integrator",			10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	51,	"KP",	"Proportional Constant",	10,  1,  8, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"MM",	"Master Modulus",		10, 54,  0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"MO",	"Motor Off (Axis Pattern)",	10, 55,  0, addrLongAsLong) );

	AddSpace( New CSpace(	59,	"OB",	"Output Bit",			10,  1,  8, addrLongAsLong) );
	AddSpace( New CSpace(	60,	"OE",	"Off on Error - Enable/Disable",10,  1,  8, addrLongAsLong) );
	AddSpace( New CSpace(	61,	"OF",	"Offset",			10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"OP",	"Output Port",			10, 62,  0, addrLongAsLong) );

	AddSpace( New CSpace(	63,	"PA",	"Position Absolute",		10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	64,	"PR",	"Position Relative",		10,  1,  8, addrRealAsReal) );

	AddSpace( New CSpace(	127,	"RP",	"Reference Position",		10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"RS",	"Reset",			10, 66,  0, addrBitAsBit  ) );

	AddSpace( New CSpace(addrNamed,	"SB",	"Set Bit (Bit Number)",		10, 67,  0, addrLongAsLong) );
	AddSpace( New CSpace(	125,	"SC",	"Stop Code",			10,  1,  8, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"SH",	"Servo Here (Axis Pattern)",	10, 68,  0, addrLongAsLong) );
	AddSpace( New CSpace(	69,	"SP",	"Speed",			10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"ST",	"Stop (Axis Pattern)",		10, 70,  0, addrLongAsLong) );

	AddSpace( New CSpace(addrNamed,	"TB",	"Tell Status Byte",		10, 71,  0, addrLongAsLong) );
	AddSpace( New CSpace(addrNamed,	"TC",	"Tell Error Code",		10, 72,  0, addrLongAsLong) );
	AddSpace( New CSpace(	73,	"TD",	"Tell Dual Encoder",		10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	74,	"TE",	"Tell Error",			10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"TI",	"Tell Inputs (Axis 1 only)",	10, 75,  0, addrLongAsLong) );
	AddSpace( New CSpace(  147,	"TIA",	"Tell Inputs (Any Axis)",	10,  1, 16, addrLongAsLong) );
	AddSpace( New CSpace(	76,	"TL",	"Torque Limit",			10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(addrNamed,	"TM",	"Time Command",			10, 77,  0, addrLongAsLong) );
	AddSpace( New CSpace(	78,	"TP",	"Tell Position",		10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	79,	"TS",	"Tell Switches",		10,  1,  8, addrLongAsLong) );
	AddSpace( New CSpace(	80,	"TT",	"Tell Torque",			10,  1,  8, addrRealAsReal) );
	AddSpace( New CSpace(	81,	"TV",	"Tell Velocity",		10,  1,  8, addrLongAsLong) );
	AddSpace( New CSpace(	82,	"TW",	"Time Wait",			10,  1,  8, addrLongAsLong) );

	AddSpace( New CSpace(	129,	"UV",	"User Variables  UV000-UV999",	10, 0, 999, addrRealAsReal) );

	AddSpace( New CSpace(	115,	"V0",	"Array Word  (16) V0000-V0999",	10, 0, 999, addrWordAsWord) );
	AddSpace( New CSpace(	116,	"V1",	"Array Word  (16) V1000-V1999",	10, 0, 999, addrWordAsWord) );
	AddSpace( New CSpace(	117,	"V2",	"Array Word  (16) V2000-V2999",	10, 0, 999, addrWordAsWord) );
	AddSpace( New CSpace(	118,	"V3",	"Array Word  (16) V3000-V3999",	10, 0, 999, addrWordAsWord) );
	AddSpace( New CSpace(	119,	"V4",	"Array Word  (16) V4000-V4999",	10, 0, 999, addrWordAsWord) );
	AddSpace( New CSpace(	120,	"V5",	"Array Word  (16) V5000-V5999",	10, 0, 999, addrWordAsWord) );
	AddSpace( New CSpace(	121,	"V6",	"Array Word  (16) V6000-V6999",	10, 0, 999, addrWordAsWord) );
	AddSpace( New CSpace(	122,	"V7",	"Array Word  (16) V7000-V7999",	10, 0, 999, addrWordAsWord) );

	AddSpace( New CSpace(	130,	"D0",	"Array DWord (32) D0000-D0999",	10, 0, 999, addrLongAsLong) );
	AddSpace( New CSpace(	131,	"D1",	"Array DWord (32) D1000-D1999",	10, 0, 999, addrLongAsLong) );
	AddSpace( New CSpace(	132,	"D2",	"Array DWord (32) D2000-D2999",	10, 0, 999, addrLongAsLong) );
	AddSpace( New CSpace(	133,	"D3",	"Array DWord (32) D3000-D3999",	10, 0, 999, addrLongAsLong) );
	AddSpace( New CSpace(	134,	"D4",	"Array DWord (32) D4000-D4999",	10, 0, 999, addrLongAsLong) );
	AddSpace( New CSpace(	135,	"D5",	"Array DWord (32) D5000-D5999",	10, 0, 999, addrLongAsLong) );
	AddSpace( New CSpace(	136,	"D6",	"Array DWord (32) D6000-D6999",	10, 0, 999, addrLongAsLong) );
	AddSpace( New CSpace(	137,	"D7",	"Array DWord (32) D7000-D7999",	10, 0, 999, addrLongAsLong) );

	AddSpace( New CSpace(	138,	"R0",	"Array Real  (32) R0000-R0999",	10, 0, 999, addrRealAsReal) );
	AddSpace( New CSpace(	139,	"R1",	"Array Real  (32) R1000-R1999",	10, 0, 999, addrRealAsReal) );
	AddSpace( New CSpace(	140,	"R2",	"Array Real  (32) R2000-R2999",	10, 0, 999, addrRealAsReal) );
	AddSpace( New CSpace(	141,	"R3",	"Array Real  (32) R3000-R3999",	10, 0, 999, addrRealAsReal) );
	AddSpace( New CSpace(	142,	"R4",	"Array Real  (32) R4000-R4999",	10, 0, 999, addrRealAsReal) );
	AddSpace( New CSpace(	143,	"R5",	"Array Real  (32) R5000-R5999",	10, 0, 999, addrRealAsReal) );
	AddSpace( New CSpace(	144,	"R6",	"Array Real  (32) R6000-R6999",	10, 0, 999, addrRealAsReal) );
	AddSpace( New CSpace(	145,	"R7",	"Array Real  (32) R7000-R7999",	10, 0, 999, addrRealAsReal) );

	AddSpace( New CSpace(addrNamed,	"XQ",	"Execute Program",		10, 114, 0, addrLongAsLong) );

	AddSpace( New CSpace(	146,	"AW",	"V,D,R Array Access Control",	10,   0, 0, addrBitAsBit) );

	AddSpace( New CSpace(	148,	"WO",	"Write Specific Output Bit",	10, 0, 899, addrByteAsByte) );
	AddSpace( New CSpace(	164,	"USR",	"Device Control Fn 1. Response",10, 0,   7, addrLongAsLong) );
	AddSpace( New CSpace(	165,	"HDS",	"TCP Handle Swap, <data> = 'A' - 'P'",	10, 0,   0, addrByteAsByte) );

//	AddSpace( New CSpace(	150,	"TXT",	"Read/Write Command Strings",	10,   0, 63, addrLongAsLong) );

//	AddSpace( New CSpace(	151,	"TXTI",	"TXTn Data as Integer",		10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	152,	"TXTR",	"TXTn Data as Real",		10,   0, 63, addrRealAsReal) );
//	AddSpace( New CSpace(	153,	"TXTSA","TXTn Data as String",		10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	154,	"TXTSB","TXTn Data as String",		10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	155,	"TXTSC","TXTn Data as String",		10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	156,	"TXTSD","TXTn Data as String",		10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	157,	"TXTSE","TXTn Data as String",		10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	158,	"TXTSF","TXTn Data as String",		10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	159,	"TXTSG","TXTn Data as String",		10,   0, 63, addrLongAsLong) );

//	AddSpace( New CSpace(	160,	"WTXT",	"Write Only Command Strings",	10,   0, 63, addrLongAsLong) );

//	AddSpace( New CSpace(	161,	"WTXI",	"Send WTXTn, Data = Integer",	10,   0, 63, addrLongAsLong) );
//	AddSpace( New CSpace(	162,	"WTXR",	"Send WTXTn, Data = Real",	10,   0, 63, addrRealAsReal) );
//	AddSpace( New CSpace(	163,	"WTXS",	"Send WTXTn, Data = String",	10,   0, 63, addrLongAsLong) );

//	AddSpace( New CSpace(addrNamed,	"IH",	"Open Internet Handle (A - H)",	10, 104, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"IHA",	"Internet Handle - Handle",	10, 105, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"IHB",	"Internet Handle - IP",		10, 106, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"IHC",	"Internet Handle - Port",	10, 107, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"IHD",	"Internet Handle - Protocol",	10, 108, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"IHE",	"Internet Handle - Terminate",	10, 109, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"CH",	"Connect Handle",		10, 110, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"CHA",	"Connect Handle - Axis",	10, 111, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"CHB",	"Connect Handle - Send",	10, 112, 0, addrLongAsLong) );
//	AddSpace( New CSpace(addrNamed,	"CHC",	"Connect Handle - Receive",	10, 113, 0, addrLongAsLong) );
//
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_CYaskawaSMCTCPDriver(void)
{
	return New CYaskawaSMCTCPDriver;
	}

// Constructor

CYaskawaSMCTCPDriver::CYaskawaSMCTCPDriver(void)
{
	m_wID		= 0x350C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "TCP/IP Universal SMC Master";
	
	m_Version	= "1.50";
	
	m_ShortName	= "Yaskawa SMC TCP/IP Master";
	}

// Binding Control

UINT CYaskawaSMCTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYaskawaSMCTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYaskawaSMCTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CYaskawaSMCTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaTCPDeviceOptions);
	}
 
// End of File
