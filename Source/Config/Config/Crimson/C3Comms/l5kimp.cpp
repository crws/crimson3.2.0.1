
#include "intern.hpp"

#include "l5kdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File
//

// Constructor

CABL5kFile::CABL5kFile(void)
{
	m_pStream = NULL;
	}

// Destructor

CABL5kFile::~CABL5kFile(void)
{
	m_pStream = NULL;
	}

// Control
 
BOOL CABL5kFile::OpenLoad(ITextStream &Stream)
{
	if( !m_pStream ) {

		m_pStream = &Stream;

		ParseFile();

		if( TRUE || m_Version == L"2.6" ) {

			//ShowController();

			return TRUE;
			}

		CloseFile();

		return FALSE;
		}

	return FALSE;
	}

// Reading

// Implementation

void CABL5kFile::CloseFile(void)
{
	m_pStream = NULL;
	}

void CABL5kFile::ReadWork(void)
{
	if( m_pStream->GetLine(m_sLine, elements(m_sLine)) ) {

		UINT uLen = wstrlen(m_sLine);

		UINT uPos;
		
		for( uPos = 0; uPos < uLen; uPos++ ) {

			TCHAR cData = m_sLine[uPos];

			if( wisspace(cData) ) {
				
				continue;
				}

			if( isprint(cData) ) {
				
				break;
				}
			}
		
		m_Work.QuickInit(m_sLine + uPos, uLen - uPos);

		m_Work.TrimRight();

		if( m_Work.IsEmpty() ) {

			ReadWork();
			}
		}
	else
		m_Work.Empty();
	}

void CABL5kFile::ParseFile(void)
{
	do {
		ReadWork();

		SkipHeader();

		m_Component = m_Work.StripToken(' ');

		ParseVersion()		|| 
		ParseController()	;; 

		} while( !m_Work.IsEmpty() );
	}

BOOL CABL5kFile::ParseVersion(void)
{
	if( m_Component == L"IE_VER" ) {

		AfxAssert(m_Work.Find(';') < NOTHING);

		m_Work.Delete(m_Work.GetLength() - 1, 1);

		m_Work.StripToken(L":=");

		m_Work.TrimLeft();
		
		m_Version = m_Work;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseController(void)
{
	if( m_Component == L"CONTROLLER" ) {

		SkipDescription();

		CString   Comp = m_Component;
		
		m_Logix.m_Name = m_Work.StripToken(' ');

		while( !m_Work.EndsWith(L"END_" + Comp) ) {

			m_Component = m_Work.StripToken(' ');

			ParseDataType()			          ||
			ParseModule()			          ||
			ParseAOI()			          ||
			ParseTags(m_Logix.m_Name, m_Logix.m_Tags) ||
			ParseProgram()			          ||
			ParseComponent(L"TASK")                   ||
			ParseComponent(L"TREND")                  ||
			ParseComponent(L"CONFIG")                 ;;
			
			ReadWork();
			};
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseDataType(void)
{
	if( m_Component == L"DATATYPE" ) {

		SkipDescription();
		
		CString Comp = m_Component;

		CString Name = m_Work.StripToken(' ');

		CABL5kList UDT(Name);

		while( !m_Work.EndsWith(L"END_" + Comp) ) {

			if( m_Work[m_Work.GetLength() - 1] == ';' ) {

				m_Work.Delete(m_Work.GetLength() - 1, 1);
			
				CString Type = m_Work.StripToken(' ');
				
				CString Name = m_Work.StripToken(' ');

				CABL5kDesc Member(Name, Type);
				
				UDT.Append(Member);
				}

			ReadWork();

			SkipDescription();
			}

		m_Logix.AddUDT(UDT);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseModule(void)
{
	if( m_Component == L"MODULE" ) {

		SkipDescription();

		CString Comp = m_Component;

		CString Name = m_Work.StripToken(' ');

		while( !m_Work.EndsWith(L"END_" + Comp) ) {

			StripInPlace(m_Work, L"[", L"]");

			if( FALSE && m_Work.StartsWith(L"ExtendedProp") ) {
				
				}

			if( FALSE && m_Work.StartsWith(L"ConfigData") ) {

				}

			m_Component = m_Work.StripToken(' ');

			ParseComponent(L"CONNECTION");

			ReadWork();

			SkipDescription();
			};
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseAOI(void)
{
	CString Comp;

	CString Name;

	if( m_Component == L"ENCODED_DATA" ) {

		Comp	    = m_Component;

		m_Component = m_Work.TokenLast(' ').StripToken(',');

		ReadWork();

		Name	    = m_Work.TokenLast(' ').StripToken(',');

		Name.Remove('"');
		}

	if( m_Component == L"ADD_ON_INSTRUCTION_DEFINITION" ) {

		SkipDescription();

		Comp = Comp.IsEmpty() ? m_Component            : Comp;

		Name = Name.IsEmpty() ? m_Work.StripToken(' ') : Name;
				
		CABL5kList AOI(Name);
		
		while( !m_Work.EndsWith(L"END_" + Comp) ) {
			
			m_Component = m_Work.StripToken(' ');
			
			ParseParameters(AOI)		||
			ParseComponent(L"LOCAL_TAGS")	||
			ParseRoutine(L"ST_ROUTINE")	||
			ParseRoutine(L"FBD_ROUTINE")	||
			ParseRoutine(L"ROUTINE")	;;
			
			ReadWork();
			}

		m_Logix.AddAOI(AOI);

		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseParameters(CABL5kList &Params)
{
	if( m_Component == L"PARAMETERS" ) {

		CString Comp = m_Component;

		while( !m_Work.EndsWith(L"END_" + Comp) ) {

			if( m_Work[m_Work.GetLength() - 1] == ';' ) {

				m_Work.Delete(m_Work.GetLength() - 1, 1);

				CString Name = m_Work.StripToken(' ');

				CString Symb = m_Work.StripToken(' ');

				if( Symb == L":" ) {

					CString Type = m_Work.StripToken(':');

					Type.TrimRight();
					
					Params.m_List.Append(CABL5kDesc(Name, Type));
					}

				else if( Symb == L"OF" ) {

					CString Type = m_Work.StripToken(':');

					Type.TrimRight();

					Params.m_List.Append(CABL5kAlias(Name, Type));
					}
				}

			ReadWork();

			SkipDescription();
			};
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ReadTag(CString Component)
{
	m_Work.Empty();
	
	CString Line;

	do {
		ReadWork();

		StripInPlace(m_Work, L"\"", L"\"");

		StripInPlace(m_Work, L"\'", L"\'");

		StripInPlace(m_Work, L"(", L")");

		Line += m_Work;

		if( Line.EndsWith(L"END_" + Component) ) {
			
			return FALSE;
			}

		} while( Line[Line.GetLength() - 1] != ';' );

	m_Work = Line;

	return TRUE;
	}

BOOL CABL5kFile::ParseTags(CString Parent, CArray <CABL5kDesc> &Tags)
{
	if( m_Component == L"TAG" ) {

		afxThread->SetStatusText(CPrintf(IDS_AB_READING_TAGS, Parent));

		CString Comp = m_Component;

		while( ReadTag(Comp) ) {

			SkipDescription();

			CString Name = m_Work.StripToken(' ');

			if( m_Work.StripToken(' ') == L"OF" ) {
					
				CString Type = m_Work.StripToken(' ');
				
				Tags.Append(CABL5kAlias(Name, Type));
				}
			else {
				CString Type = m_Work.StripToken(' ');

				Tags.Append(CABL5kDesc(Name, Type));
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseProgram(void)
{
	if( m_Component == L"PROGRAM" ) {

		CString Comp = m_Component;

		CString Name = m_Work.StripToken(' ');

		CABL5kList Program(Name);

		while( !m_Work.EndsWith(L"END_" + Comp) ) {

			m_Component = m_Work.StripToken(' ');
			
			ParseTags(Name, Program.m_List)	||
			ParseRoutine(L"ROUTINE")	||
			ParseRoutine(L"FBD_ROUTINE")	||
			ParseRoutine(L"ST_ROUTINE")	||
			ParseRoutine(L"SFC_ROUTINE")	;;

			ReadWork();
			};

		m_Logix.AddProgram(Program);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ReadComponent(CString Comp)
{
	CString Line;

	do {
		ReadWork();

		SkipDescription();

		StripInPlace(m_Work, L"\"", L"\"");

		StripInPlace(m_Work, L"[", L"]");

		Line += m_Work;

		StripInPlace(Line, L"[", L"]");

		if( Line.EndsWith(L"END_" + Comp) ) {
			
			return FALSE;
			}

		} while( Line[Line.GetLength() - 1] != ';' );

	m_Work = Line;

	return TRUE;
	}

BOOL CABL5kFile::ParseComponent(CString Comp)
{
	if( m_Component == Comp ) {

		CString  Name = m_Work.StripToken(' ');

		while( !m_Work.EndsWith(L"END_" + Comp) ) {

			if( !ReadComponent(Comp) ) {
				
				break;
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CABL5kFile::ParseRoutine(CString Comp)
{
	if( m_Component == Comp ) {

		do {
			ReadWork();

			} while( !m_Work.EndsWith(L"END_" + Comp) );
		
		return TRUE;
		}

	return FALSE;
	}

void CABL5kFile::SkipHeader(void)
{
	UINT p1;

	if( (p1 = m_Work.Find(L"(*")) < NOTHING ) {

		CString Line = m_Work;

		do {
			ReadWork();

			Line += m_Work;

			} while( m_Work.Find(L"*)") == NOTHING );

		StripInPlace(Line, L"(*", L"*)");
		
		m_Work = Line;
		}

	if( m_Work.IsEmpty() ) {
		
		ReadWork();
		}
	}

void CABL5kFile::SkipDescription(void)
{
	StripInPlace(m_Work, L"\"", L"\"");

	StripInPlace(m_Work, L"(", L")");

	UINT p1;

	if( (p1 = m_Work.Find('(')) < NOTHING ) {

		CString Line;

		Line += m_Work;

		do {
			ReadWork();

			StripInPlace(m_Work, L"\"", L"\"");

			Line += m_Work;

			} while( m_Work.Find(')') == NOTHING );

		StripInPlace(Line, L"(", L")");
		
		m_Work = Line;
		}

	if( m_Work.IsEmpty() ) {
		
		ReadWork();
		}
	}

void CABL5kFile::StripInPlace(CString &Text, PCUTF c1, PCUTF c2)
{
	UINT p1, p2;

	while( (p1 = Text.Find(c1)) < NOTHING ) {

		p1 += wstrlen(c1);

		if( (p2 = Text.Find(c2, p1)) == NOTHING ) {
			
			break;
			}

		while( Text.Mid(p1, p2 - p1).Find(c1) < NOTHING ) {
			
			p1 += wstrlen(c1);
			}

		Text = Text.Left(p1 - wstrlen(c1)) + Text.Mid(p2 + wstrlen(c2));
		} 
	}

// Debug

void CABL5kFile::ShowController(void)
{
	AfxTrace(L"Controller Data Types...\n");
	
	ShowDataTypes(m_Logix.m_UDTs);

	AfxTrace(L"\nController [%s] Tags\n", m_Logix.m_Name);
	
	if( m_Logix.m_Tags.GetCount() == 0 ) {

		AfxTrace(L"\t - no tags\n");
		}
	else			
		ShowTagNames(m_Logix.m_Tags);

	UINT n;

	for( n = 0; n < m_Logix.m_Programs.GetCount(); n ++ ) {

		CABL5kList Program = m_Logix.m_Programs[n];

		AfxTrace(L"Program [%s] Tags\n", Program.m_Name);

		ShowTagNames(Program.m_List);
		}

	for( n = 0; n < m_Logix.m_AOIs.GetCount(); n ++ ) {

		CABL5kList AOI = m_Logix.m_AOIs[n];

		AfxTrace(L"AOI [%s] Parameters\n", AOI.m_Name);

		if( AOI.m_List.GetCount() == 0 ) {
			
			AfxTrace(L"\t - no parameters\n");
			}
		else {
			ShowTagNames(AOI.m_List);
			}
		}
	}

void CABL5kFile::ShowDataTypes(CArray <CABL5kList> const &Types)
{
	for( UINT n = 0; n < Types.GetCount(); n++ ) {
		
		CABL5kList UDT = Types[n];

		AfxTrace(L"type [%s], %d members\n", UDT.m_Name, UDT.m_List.GetCount());

		for( UINT m = 0; m < UDT.m_List.GetCount(); m++ ) {

			AfxTrace(L"\t%s of type %s\n", UDT.m_List[m].m_Name, UDT.m_List[m].m_Type);
			}
		}
	}

void CABL5kFile::ShowTagNames(CArray <CABL5kDesc> const &Tags)
{
	for( UINT n = 0; n < Tags.GetCount(); n++ ) {
		
		CABL5kDesc Item = Tags[n];

		AfxTrace(L"\t<%s> of type [%s] %s\n", 
			 Item.m_Name, 
			 Item.m_Type,
			 Item.IsAlias() ? L"Alias Tag" : L""
			 );
		}
	}

// End of File
