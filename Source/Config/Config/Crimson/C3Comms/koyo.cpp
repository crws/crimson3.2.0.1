
#include "intern.hpp"

#include "koyo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();


//////////////////////////////////////////////////////////////////////////
//
// PLC Koyo Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKoyoDeviceOptions, CUIItem);

// Constructor

CKoyoDeviceOptions::CKoyoDeviceOptions(void)
{
	m_Drop       = 0;
	
	m_PingEnable = 0;

	m_PingReg    = 0;

	m_Backup     = 0;
	}

// UI Managament

void CKoyoDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "PingEnable" ) {

		pWnd->EnableUI("PingReg", pItem->GetDataAccess("PingEnable")->ReadInteger(pItem));
		}

	if( Tag.IsEmpty() || Tag == "PingReg" )  {
		
		UINT uReg = pItem->GetDataAccess("PingReg")->ReadInteger(pItem);

		if( IsOctal(uReg) ) {

			m_Backup = m_PingReg;
			}
		else {			
			pWnd->Error(CString(IDS_ERROR_ADDR));

			m_PingReg = m_Backup;
			}
		}

	pWnd->UpdateUI();
	}


// Download Support

BOOL CKoyoDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_PingEnable));

	Init.AddWord(WORD(OctalToDec(m_PingReg)));

	return TRUE;
	}

// Meta Data Creation

void CKoyoDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(PingEnable);

	Meta_AddInteger(PingReg);
	}

// Helpers

BOOL CKoyoDeviceOptions::IsOctal(UINT uReg)
{
	while( uReg >= 1 ) {

		UINT uMod = uReg % 10;

		if( uMod == 8 || uMod == 9 ) {

			return FALSE;
			}

		uReg /= 10;
		}
       	
	return TRUE;
	}

UINT CKoyoDeviceOptions::OctalToDec(UINT uReg)
{
	UINT uDec = 0;

	UINT uMult = 1;

	while( uReg >= 1 ) {

		UINT uMod = uReg % 10;

		uDec += uMod * uMult;

		uMult *= 8;

		uReg /= 10;
		}

	return uDec;
	}

////////////////////////////////////////////////////////////////////////
//
// PLC Direct Koyo Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CKoyoDirectNetDriverOptions, CUIItem);

// Constructor

CKoyoDirectNetDriverOptions::CKoyoDirectNetDriverOptions(void)
{
	m_MasterID = 0;

	}

// Download Support

BOOL CKoyoDirectNetDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_MasterID));

	return TRUE;
	}

// Meta Data Creation

void CKoyoDirectNetDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(MasterID);
	}



//////////////////////////////////////////////////////////////////////////
//
// PLC Direct Koyo - DirectNet
//

// Instantiator

ICommsDriver * Create_KoyoDirectNetDriver(void)
{
	return New CKoyoDirectNetDriver;
	}

// Constructor

CKoyoDirectNetDriver::CKoyoDirectNetDriver(void)
{
	m_wID		= 0x330C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "PLC Direct Koyo";
	
	m_DriverName	= "DirectNet";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Koyo DirectNet";

	AddSpaces();
	}

// Configuration

CLASS CKoyoDirectNetDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CKoyoDirectNetDriverOptions);
	}

// Configuration

CLASS CKoyoDirectNetDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKoyoDeviceOptions);
	}


// Binding Control

UINT CKoyoDirectNetDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CKoyoDirectNetDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CKoyoDirectNetDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "V",   "Data Registers",	  8, 0, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(2,  "TMR", "Current Timer",		  8, 0,   511, addrWordAsWord));
	AddSpace(New CSpace(3,  "CTR", "Current Count",		  8, 0,   511, addrWordAsWord));
	AddSpace(New CSpace(4,  "X",   "Inputs X",		  8, 0,  1023, addrBitAsByte ));
	AddSpace(New CSpace(5,  "GX",  "Inputs GX",	          8, 0,  2047, addrBitAsByte ));
	AddSpace(New CSpace(6,  "SP",  "Inputs Special Relay",    8, 0,   511, addrBitAsByte ));
	AddSpace(New CSpace(7,  "Y",   "Outputs Y",	          8, 0,  1023, addrBitAsByte ));
	AddSpace(New CSpace(8,  "C",   "Outputs C",		  8, 0,  2047, addrBitAsByte ));
	AddSpace(New CSpace(9,  "ST",  "Outputs Stage Bits",      8, 0,  1023, addrBitAsByte ));
	AddSpace(New CSpace(10, "A",   "Outputs TMR Status Bits", 8, 0,   496, addrBitAsByte ));
	AddSpace(New CSpace(11, "B",   "Outputs CTR Status Bits", 8, 0,   496, addrBitAsByte ));
	}

// Address Helpers

BOOL CKoyoDirectNetDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
			return FALSE;
		}

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// PLC Direct Koyo - K Sequence
//

// Instantiator

ICommsDriver *	Create_KoyoKSequenceDriver(void)
{
	return New CKoyoKSequenceDriver;
	}

// Constructor

CKoyoKSequenceDriver::CKoyoKSequenceDriver(void)
{
	m_wID		= 0x3378;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "PLC Direct Koyo";
	
	m_DriverName	= "K Sequence";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Koyo K Sequence";

	AddSpaces();
	}

// Configuration

CLASS CKoyoKSequenceDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKoyoDeviceOptions);
	}


// Binding Control

UINT CKoyoKSequenceDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CKoyoKSequenceDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode	  = modeTwoWire;
	}

// Implementation

void CKoyoKSequenceDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "V",   "Data Registers",	  8, 0, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(2,  "TMR", "Current Timer",		  8, 0,   511, addrWordAsWord));
	AddSpace(New CSpace(3,  "CTR", "Current Count",		  8, 0,   511, addrWordAsWord));
	AddSpace(New CSpace(4,  "X",   "Inputs X",		  8, 0,  1023, addrBitAsWord ));
	AddSpace(New CSpace(5,  "GX",  "Inputs GX",	          8, 0,  2047, addrBitAsWord ));
	AddSpace(New CSpace(6,  "SP",  "Inputs Special Relay",    8, 0,   511, addrBitAsWord ));
	AddSpace(New CSpace(7,  "Y",   "Outputs Y",	          8, 0,  1023, addrBitAsWord ));
	AddSpace(New CSpace(8,  "C",   "Outputs C",		  8, 0,  2047, addrBitAsWord ));
	AddSpace(New CSpace(9,  "ST",  "Outputs Stage Bits",      8, 0,  1023, addrBitAsWord ));
	AddSpace(New CSpace(10, "A",   "Outputs TMR Status Bits", 8, 0,   496, addrBitAsWord ));
	AddSpace(New CSpace(11, "B",   "Outputs CTR Status Bits", 8, 0,   496, addrBitAsWord ));
	//AddSpace(New CSpace(12, "GY",  "Outputs GY",		  8, 0,  2047, addrBitAsWord )); 
	}

// Address Helpers

BOOL CKoyoKSequenceDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
			return FALSE;
		}

	return TRUE;
	}



//////////////////////////////////////////////////////////////////////////
//
// Koyo PLC Master Udp/IP
//

// Instantiator

ICommsDriver *	Create_CKoyoMasterUDPDriver(void)
{
	return New CKoyoMasterUdpDriver;
	}

// Constructor

CKoyoMasterUdpDriver::CKoyoMasterUdpDriver(void)
{
	m_wID		= 0x350D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "PLC Direct Koyo";
	
	m_DriverName	= "ECOM UDP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "ECOM Master";

	AddSpaces();
	}

// Binding Control

UINT CKoyoMasterUdpDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CKoyoMasterUdpDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_UDPCount = 1;
	
	Ether.m_TCPCount = 0;
	}

// Configuration

CLASS CKoyoMasterUdpDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CKoyoMasterUdpDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKoyoPingMasterUdpDeviceOptions);
	}

// Implementation

void CKoyoMasterUdpDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "V",   "Data Registers",	  8, 0,  27485, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(2,  "TMR", "Current Timer",		  8, 0,    511, addrWordAsWord));
	AddSpace(New CSpace(3,  "CTR", "Current Count",		  8, 0,    511, addrWordAsWord));
	AddSpace(New CSpace(4,  "X",   "Inputs X",		  8, 0,   1023, addrBitAsWord, addrBitAsLong ));
	AddSpace(New CSpace(5,  "GX",  "Inputs GX",	          8, 0,   2047, addrBitAsWord, addrBitAsLong ));
	AddSpace(New CSpace(6,  "SP",  "Inputs Special Relay",    8, 0,    511, addrBitAsWord, addrBitAsLong ));
	AddSpace(New CSpace(7,  "Y",   "Outputs Y",	          8, 0,   1023, addrBitAsWord, addrBitAsLong ));
	AddSpace(New CSpace(8,  "C",   "Outputs C",		  8, 0,   2047, addrBitAsWord, addrBitAsLong ));
	AddSpace(New CSpace(9,  "S",   "Outputs Stage Bits",      8, 0,   1023, addrBitAsWord, addrBitAsLong ));
	AddSpace(New CSpace(10,	"T",   "TMR Status Bits",	  8, 0,    255, addrBitAsWord, addrBitAsLong ));
	AddSpace(New CSpace(11,	"CT",  "CTR Status Bits",         8, 0,    255, addrBitAsWord, addrBitAsLong ));

	}

// Address Helpers

BOOL CKoyoMasterUdpDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
			return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Koyo PLC Master Udp/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKoyoMasterUdpDeviceOptions, CUIItem);

// Constructor

CKoyoMasterUdpDeviceOptions::CKoyoMasterUdpDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket  = 1033;

	m_Keep    = TRUE;

	m_Ping    = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;
	}

// UI Managament

void CKoyoMasterUdpDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support	     

BOOL CKoyoMasterUdpDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
		
	return TRUE;
	}

// Meta Data Creation

void CKoyoMasterUdpDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Koyo PLC Master Ping Udp/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKoyoPingMasterUdpDeviceOptions, CKoyoMasterUdpDeviceOptions);

// Constructor

CKoyoPingMasterUdpDeviceOptions::CKoyoPingMasterUdpDeviceOptions(void) 
{
	m_PingEnable = 0;

	m_PingReg    = 0;

	m_Backup     = 0;
	} 

// UI Managament

void CKoyoPingMasterUdpDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "PingEnable" ) {

			pWnd->EnableUI("PingReg", pItem->GetDataAccess("PingEnable")->ReadInteger(pItem));
			}

		if( Tag.IsEmpty() || Tag == "PingReg" )  {
		
			UINT uReg = pItem->GetDataAccess("PingReg")->ReadInteger(pItem);

			if( IsOctal(uReg) ) {

				m_Backup = m_PingReg;
				}
			else {			
				pWnd->Error(CString(IDS_ERROR_ADDR));

				m_PingReg = m_Backup;
				}
			}

		pWnd->UpdateUI();

		CKoyoMasterUdpDeviceOptions::OnUIChange(pWnd, pItem, Tag);
		}
	}

// Download Support	     

BOOL CKoyoPingMasterUdpDeviceOptions::MakeInitData(CInitData &Init)
{
	CKoyoMasterUdpDeviceOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_PingEnable));

	Init.AddWord(WORD(OctalToDec(m_PingReg)));
		
	return TRUE;
	}

// Meta Data Creation

void CKoyoPingMasterUdpDeviceOptions::AddMetaData(void)
{
	CKoyoMasterUdpDeviceOptions::AddMetaData();

	Meta_AddInteger(PingEnable);
	
	Meta_AddInteger(PingReg);
	}

// Helpers

BOOL CKoyoPingMasterUdpDeviceOptions::IsOctal(UINT uReg)
{
	while( uReg >= 1 ) {

		UINT uMod = uReg % 10;

		if( uMod == 8 || uMod == 9 ) {

			return FALSE;
			}

		uReg /= 10;
		}
       	
	return TRUE;
	}

UINT CKoyoPingMasterUdpDeviceOptions::OctalToDec(UINT uReg)
{
	UINT uDec = 0;

	UINT uMult = 1;

	while( uReg >= 1 ) {

		UINT uMod = uReg % 10;

		uDec += uMod * uMult;

		uMult *= 8;

		uReg /= 10;
		}

	return uDec;
	}

//////////////////////////////////////////////////////////////////////////
//
// Koyo PLC Master TCP/IP
//

// Instantiator

ICommsDriver *	Create_CKoyoEbcMasterUdpDriver(void)
{
	return New CKoyoEbcMasterUdpDriver;
	}

// Constructor

CKoyoEbcMasterUdpDriver::CKoyoEbcMasterUdpDriver(void)
{
	m_wID		= 0x406D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "PLC Direct Koyo";
	
	m_DriverName	= "EBC UDP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "EBC Master";

	DeleteAllSpaces();

	AddSpaces();
	}

// Configuration

CLASS CKoyoEbcMasterUdpDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKoyoMasterUdpDeviceOptions);
	}


// Address Management

BOOL CKoyoEbcMasterUdpDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CKoyoEbcDialog Dlg(ThisObject, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CKoyoEbcMasterUdpDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uFind = Text.Find(':');

	UINT uSlot = 0;

	if( uFind < NOTHING ) {

		uSlot = tstrtol(Text.Mid(0, uFind), NULL, 10);

		CSpaceEbc * pEbc = (CSpaceEbc *)pSpace;

		if( uSlot > pEbc->m_SlotMax ) {

			Error.Set( CPrintf("The slot number must not be greater than %u.", pEbc->m_SlotMax),
					   0
					   );

			return FALSE;
			}

		Text = Text.Mid(uFind + 1);
		}
		
	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Extra = uSlot;

		return TRUE;
		}

	return FALSE;
	}

BOOL CKoyoEbcMasterUdpDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		Text.Printf( "%s%s:%s", pSpace->m_Prefix, 
					pSpace->GetValueAsText(Addr.a.m_Extra),
					pSpace->GetValueAsText(Addr.a.m_Offset)
				        );
		return TRUE;
		}
	
	return FALSE;
	}



// Implementation

void CKoyoEbcMasterUdpDriver::AddSpaces(void)
{
	AddSpace(New CSpaceEbc(1,  "DI",  "Discrete Input",	10, 0,  63, addrBitAsBit,	0, 15));
	AddSpace(New CSpaceEbc(2,  "DO",  "Discrete Output",	10, 0,  63, addrBitAsBit,	0, 15));
	AddSpace(New CSpaceEbc(3,  "AI",  "Analog Input(WORD)", 10, 1,  16, addrWordAsWord,	0, 15));
	AddSpace(New CSpaceEbc(4,  "AO",  "Analog Output(WORD)",10, 1,  16, addrWordAsWord,	0, 15));
//	AddSpace(New CSpaceEbc(6,  "AIL", "Analog Input(LONG)", 10, 1,  16, addrLongAsLong,	0, 15));
//	AddSpace(New CSpaceEbc(7,  "AOL", "Analog Output(LONG)",10, 1,  16, addrLongAsLong,	0, 15));
	}


//////////////////////////////////////////////////////////////////////////
//
// Koyo EBC Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CKoyoEbcDialog, CStdAddrDialog);
		
// Constructor

CKoyoEbcDialog::CKoyoEbcDialog(CKoyoEbcMasterUdpDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "KoyoEbcElementDlg";
	}

// Overridables

void CKoyoEbcDialog::SetAddressText(CString Text)
{
	GetDlgItem(2005).EnableWindow(!Text.IsEmpty());
	
	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		GetDlgItem(2005).SetWindowText(Text.Mid(0, uFind));
		
		Text = Text.Mid(uFind + 1);
		}

	CStdAddrDialog::SetAddressText(Text);
	}

CString CKoyoEbcDialog::GetAddressText(void)
{      
	return GetDlgItem(2005).GetWindowText() + ":" + CStdAddrDialog::GetAddressText();
	}

//////////////////////////////////////////////////////////////////////////
//
// Koyo EBC Space Wrapper Class
//

// Constructors

CSpaceEbc::CSpaceEbc(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT sn, UINT sx)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= x;
	m_uType		= t;
	m_uSpan		= t;
	m_SlotMin	= sn;
	m_SlotMax	= sx;

	FindAlignment();

	FindWidth();
	}

void CSpaceEbc::GetMinimum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= m_SlotMin;

	Addr.a.m_Offset	= m_uMinimum;
	}

void CSpaceEbc::GetMaximum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= m_SlotMax;

	Addr.a.m_Offset	= m_uMaximum;
	}


// End of File
