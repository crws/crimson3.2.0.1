
#include "intern.hpp"

#include "stream.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Stream Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CTestStreamDriverOptions, CUIItem);

// Constructor

CTestStreamDriverOptions::CTestStreamDriverOptions(void)
{
	m_pEnable = NULL;

	m_Files   = 10;

	m_Fields  = 14;

	m_Saved   = 14;

	m_Size    = 1;

	m_Start   = 0x8080;

	m_End     = 0x8181;
	}

// Download Support

BOOL CTestStreamDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddWord(WORD(m_Files));
	Init.AddWord(WORD(m_Fields));
	Init.AddWord(WORD(m_Saved));
	Init.AddWord(WORD(m_Size));
	Init.AddWord(WORD(m_Start));
	Init.AddWord(WORD(m_End));

	return TRUE;
	}

// Meta Data Creation

void CTestStreamDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddInteger(Files);
	Meta_AddInteger(Fields);
	Meta_AddInteger(Saved);
	Meta_AddInteger(Size);
	Meta_AddInteger(Start);
	Meta_AddInteger(End);

	Meta_SetName(IDS_DRIVER_OPTIONS);
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Streamer Driver #1
//

// Instantiator

ICommsDriver *	Create_TestStream1Driver(void)
{
	return New CTestStream1Driver;
	}

// Constructor

CTestStream1Driver::CTestStream1Driver(void)
{
	m_wID		= 0x3F02;

	m_uType		= driverStreamer;
	
	m_Manufacturer	= "PQ Systems";
	
	m_DriverName	= "TERN Controller";
	
	m_Version	= "1.00";

	m_DevRoot	= "Dev";
	
	m_ShortName	= "TERN Controller";
	}

// Configuration

CLASS CTestStream1Driver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CTestStreamDriverOptions);
	}

CLASS CTestStream1Driver::GetDeviceConfig(void)
{
	return NULL;
	}

// Binding Control

UINT CTestStream1Driver::GetBinding(void)
{
	return bindRawSerial;
	}

void CTestStream1Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Streamer Driver #2
//

// Instantiator

ICommsDriver *	Create_TestStream2Driver(void)
{
	return New CTestStream2Driver;
	}

// Constructor

CTestStream2Driver::CTestStream2Driver(void)
{
	m_wID		= 0x3F03;

	m_uType		= driverStreamer;
	
	m_Manufacturer	= "PQ Systems";
	
	m_DriverName	= "TERN Controller";
	
	m_Version	= "1.00";

	m_DevRoot	= "Dev";
	
	m_ShortName	= "TERN Controller";
	}

// Configuration

CLASS CTestStream2Driver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CTestStreamDriverOptions);
	}

CLASS CTestStream2Driver::GetDeviceConfig(void)
{
	return NULL;
	}

// Binding Control

UINT CTestStream2Driver::GetBinding(void)
{
	return bindCAN;
	}

void CTestStream2Driver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 1000000;

	CAN.m_Terminate = FALSE;
	}

// End of File
