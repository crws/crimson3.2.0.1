
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_QUICKSILVER_HPP
	
#define	INCLUDE_QUICKSILVER_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CQuicksilverDriver;
class CQuicksilverDialog;

#define	LL	addrLongAsLong

// Header Table Definitions
#define	ALLHDR	190
#define	GENHDR	191
#define	USRHDR	192
#define	AHDR	201
#define	BHDR	202
#define	CHDR	203
#define	DHDR	204
#define	EHDR	205
#define	FHDR	206
#define	GHDR	207
#define	HHDR	208
#define	IHDR	209
#define	JHDR	210
#define	KHDR	211
#define	LHDR	212
#define	MHDR	213
#define	NHDR	214
#define	OHDR	215
#define	PHDR	216
#define	QHDR	217
#define	RHDR	218
#define	SHDR	219
#define	THDR	220
#define	UHDR	221
#define	VHDR	222
#define	WHDR	223
#define	XHDR	224
#define	YHDR	225
#define	ZHDR	226

#define	QERR	275

#define	GENMAX	7
#define	USRMIN	20
#define	USRMAX	44

//////////////////////////////////////////////////////////////////////////
//
//  Quicksilver Driver Options
//

class CQuicksilverDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CQuicksilverDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Group;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Quicksilver Device Options
//

class CQuicksilverDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CQuicksilverDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Address;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Comms Driver
//

class CQuicksilverDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CQuicksilverDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Device Options
		CLASS	GetDriverConfig(void);
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart, CItem *pDev);
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Quicksilver Selection Dialog
//

class CQuicksilverDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CQuicksilverDialog(CQuicksilverDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		UINT	m_CurrentList;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

 		// Overridables
		BOOL	AllowSpace(CSpace * pSpace);

		// Helpers
		BOOL	IsHeader(UINT uTable);
		BOOL	IsAnyHeader(UINT uTable);
		BOOL	IsReloadList(UINT uTable);
		void	SetAllowSpace(UINT uTable);
		BOOL	IsGeneralCommand(UINT uTable, UINT uMin);
		BOOL	IsUserCommand(UINT uTable);
		BOOL	IsNonUseful(CSpace * pSpace);
	};

// End of File

#endif
