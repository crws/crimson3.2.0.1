
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CTI2572_HPP
	
#define	INCLUDE_CTI2572_HPP 

#include "ti500.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI Space Wrapper
//

class CSpaceCTI : public CSpace
{
	public:
		// Constructors
		CSpaceCTI(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Limits
		void GetMaximum(CAddress &Addr);
	};



//////////////////////////////////////////////////////////////////////////
//
// CTI Master TCP/IP Device Options
//

class CCTIMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCTIMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CTI Master Serial Device Options
//

class CCTIMasterSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCTIMasterSerialDeviceOptions(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// CTI 2572 TCP/IP Master Driver
//

class CCTI2572Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CCTI2572Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	
	protected:
		// Implementation
		void AddSpaces(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// CTI 25xx Serial CAMP Master Driver
//

class CCTICampMasterSerialDriver : public CCTI2572Driver
{
	public:
		// Constructor
		CCTICampMasterSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master Serial Driver
//

class CCTINitpMasterDriver : public CTI500v2Driver
{
	public:
		// Constructor
		CCTINitpMasterDriver(void);
			
	};

//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master TCP/IP Driver
//

class CCTINitpMasterTCPDriver : public CTI500MasterTCPDriver
{
	public:
		// Constructor
		CCTINitpMasterTCPDriver(void);
			
	};



#endif

// End of File