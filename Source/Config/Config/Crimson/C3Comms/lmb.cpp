
#include "intern.hpp"

#include "lmb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LMB Driver
//

// Instantiator

ICommsDriver *	Create_LMBDriver(void)
{
	return New CLMBDriver;
	}

// Constructor

CLMBDriver::CLMBDriver(void)
{
	m_wID		= 0x3703;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "Red Lion";
	
	m_DriverName	= "Big Flexible Display";
	
	m_Version	= "1.00";
	
	m_ShortName	= "BFD";

	m_fSingle	= TRUE;
	}

// Binding Control

UINT CLMBDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CLMBDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// End of File
