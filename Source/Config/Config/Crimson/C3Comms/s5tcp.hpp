
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_S5AS511_HPP
	
#define	INCLUDE_S5AS511_HPP

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 AS511 Base Driver
//

#include "s5base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 TCP Device Options
//

class CS5AS511TCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS5AS511TCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Cache;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Mode;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 TCP Master Driver
//

class CS5AS511TCPMasterDriver : public CS5AS511BaseDriver
{
	public:
		// Constructor
		CS5AS511TCPMasterDriver(BOOL fV2);

		// Configuration
		CLASS GetDeviceConfig(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
