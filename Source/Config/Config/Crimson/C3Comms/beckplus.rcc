
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "beckplus.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "beckplus.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "beckplus.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "beckplus.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// Accelerators
//

BeckhoffPlusMenu ACCELERATORS
BEGIN
	VK_F2,		IDM_ITEM_RENAME,	VIRTKEY
END

//////////////////////////////////////////////////////////////////////////
//
// UI Page Schemas
//

CBeckhoffPlusTagsTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Tag Names,Push\0"
	"\0"
END

CBeckhoffPlusTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Target NetID Port,AMSPort\0"
	"G:1,root,Device Identification,Addr,Socket\0"
	"G:1,root,Protocol Options,Keep,Time1,Time3,Time2\0"
	"G:1,root,Tag Names,Push\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Item Schemas
//

CBeckhoffPlusTagsTCPDeviceOptionsUIList RCDATA
BEGIN
	"Push,,,CUIPushRow,Manage...|Import...|Export..."
	""
	"\0"
	
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CBeckhoffPlusTCPDeviceOptions
//

CBeckhoffPlusTCPDeviceOptionsUIList RCDATA
BEGIN
	"AMSPort,AMS Port,,CUIEditInteger,|0||1|19999,"
	"Set the AMS Port Number for the device."
	"\0"

	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the device."
	"\0"

	"Socket,TCP Port,,CUIEditInteger,|0||1|49999,"
	"Indicate the TCP port number on which the protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the Yaskawa SMC driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a command or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

///////////////////////////////////////////////////////////////////////////
//
// String Table
//
//

STRINGTABLE
BEGIN
	IDS_BECKHOFFPLUS_TAGS	"Tags"
END

//////////////////////////////////////////////////////////////////////////
//
// Combo Style
//

#define	XS_COMBO_STYLE	CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP

//////////////////////////////////////////////////////////////////////////
//
// Partial Beckhoff Plus Address Selection Dialog
//

PartBeckhoffPlusDlg DIALOG 0, 0, 0, 0
CAPTION "Select Variable Name"
BEGIN
	////////
	GROUPBOX	"&Variable Name",	2000,		 4,   4, 138,  34
	LTEXT		"",			2001,		10,  19,  70,  12
	EDITTEXT				2002,		84,  18,  20,  12, 
	COMBOBOX				2003,		106,  18,  32,  44, XS_COMBO_STYLE
	////////
	DEFPUSHBUTTON   "OK",			IDOK,		 4,  44,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	48,  44,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",			IDHELP,		92,  44,  40,  14, XS_BUTTONREST
END	

//////////////////////////////////////////////////////////////////////////
//
// Full Beckhoff Plus Address Selection Dialog
//

FullBeckhoffPlusDlg DIALOG 0, 0, 0, 0
CAPTION "Select Variable Name"
BEGIN
	////////
	GROUPBOX	"Variable &Names",	1000,		  4,   4, 130, 172
	CONTROL		"",			1001, "CTreeView", WS_TABSTOP, 10,  18, 116, 152, WS_EX_CLIENTEDGE
	
	////////
	GROUPBOX	"&Variable Name",	2000,		138,   4, 160,  34
	LTEXT		"",			2001,		144,  19, 150,  12

	////////
	GROUPBOX	"Details",		3000,		138,  42, 160,  32	
	LTEXT		"Type:",		3001,		146,  56,  40,  10
	LTEXT		"<type>",		3002,		186,  56,  50,  10
	
	////////
	GROUPBOX	"Create Name for",	4000,		138,  80, 160,  76
	COMBOBOX				4001,		190,  92, 100, 110, XS_DROPDOWNLIST | CBS_HASSTRINGS
	LTEXT		"Memory Type:"		4009,		142,  93,  46,  10,
	PUSHBUTTON      "&Create...",		4002,		142, 134,  40,  14, XS_BUTTONREST
	RADIOBUTTON	"BYTE",			4003,		254, 112,  40,   8, XS_RADIOFIRST
	RADIOBUTTON	"WORD",			4004,		254, 122,  40,   8, XS_RADIOREST
	RADIOBUTTON	"LONG",			4005,		254, 132,  40,   8, XS_RADIOREST
	RADIOBUTTON	"REAL",			4006,		254, 142,  40,   8, XS_RADIOREST
	EDITTEXT				4007,		190, 112,  24,  14,
	LTEXT		"Hex Offset:",		4008,		142, 114,  36,  10


	////////
	DEFPUSHBUTTON   "OK",		IDOK,		138, 162,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	182, 162,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",		IDHELP,		226, 162,  40,  14, XS_BUTTONREST
END

// End of File
