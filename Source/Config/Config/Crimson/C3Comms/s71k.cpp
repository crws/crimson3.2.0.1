
#include "intern.hpp"

#include "s71k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CS71KDriverOptions, CUIItem);

// Constructor

CS71KDriverOptions::CS71KDriverOptions(void)
{
	
	}

// Download Support

BOOL CS71KDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);


	return TRUE;
	}

// Meta Data Creation

void CS71KDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS71KDeviceOptions, CUIItem);

// Constructor

CS71KDeviceOptions::CS71KDeviceOptions(void)
{
	m_Addr     = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));
	
	m_Addr2    = DWORD(0);

	m_Port	   = 102;

	m_Keep     = TRUE;

	m_Ping     = FALSE;

	m_Time1    = 5000;

	m_Time2    = 2500;

	m_Time3    = 200;

	m_TSAP     = 0;
		   
	m_STsap    = 0x0101;
		   
	m_CTsap    = 0x0101;
		   
	m_STsap2   = 0x0000;
		   
	m_CTsap2   = 0x0000;

	m_Type     = 0;

	m_Slot     = 0;

	m_Rack     = 0;

	m_Type2	   = 0;

	m_Slot2    = 0;

	m_Rack2    = 0;

	m_LastRef  = 0;

	m_pBlocks  = new CDbRefArray;
	}

// Destructor

CS71KDeviceOptions::~CS71KDeviceOptions(void)
{
	m_pBlocks->Empty();

	delete m_pBlocks;
	}

// UI Loading

BOOL CS71KDeviceOptions::OnLoadPages(CUIPageList * pList)
{ 
	CS71KDeviceOptionsUIPage * pPage = New CS71KDeviceOptionsUIPage(this);

	pList->Append(pPage);
	
	return TRUE;			   
	}


// UI Managament

void CS71KDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pHost->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "Addr2" ) {

			pHost->EnableUI("Rack2",  !m_TSAP && m_Addr2);
						  
			pHost->EnableUI("Slot2",  !m_TSAP && m_Addr2);

			pHost->EnableUI("STsap2", m_TSAP && m_Addr2);

			pHost->EnableUI("CTsap2", m_TSAP && m_Addr2);
			}

		if( Tag.IsEmpty() || Tag == "Type" || Tag == "Type2" ) {

			pHost->EnableUI("Type",  FALSE);

			pHost->EnableUI("Type2", FALSE);
			}

		if( Tag.IsEmpty() || Tag == "TSAP" ) {

			BOOL fEnable = pItem->GetDataAccess("TSAP")->ReadInteger(pItem);

			BOOL fType   = pItem->GetDataAccess("Type")->ReadInteger(pItem);

			pHost->EnableUI("Type",   !fEnable);
						  
			pHost->EnableUI("Conn",   !fEnable && fType);

			pHost->EnableUI("Client", !fEnable);

			pHost->EnableUI("Rack",   !fEnable && !fType);
						  
			pHost->EnableUI("Slot",   !fEnable);
						  
			pHost->EnableUI("STsap",  fEnable);

			pHost->EnableUI("CTsap",  fEnable);

			pHost->EnableUI("STsap2", fEnable && m_Addr2);

			pHost->EnableUI("CTsap2", fEnable && m_Addr2);

			pHost->EnableUI("Type2", !fEnable && m_Addr2);
			}

		if( Tag == "Purge" ) {

			PurgeDataBlocks();

			pHost->GetWindow().Information(CString(IDS_OPERATION_IS));
			}
		}    
	}

// Persistance

void CS71KDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"DBs" ) {

			Tree.GetObject();

			Tree.GetName();

			UINT uBlocks = Tree.GetValueAsInteger();

			for( UINT u = 0; u < uBlocks; u++ ) {

				CDataBlockRef DB;

				Tree.GetCollect();

				Tree.GetName();

				DB.m_Ref	= Tree.GetValueAsInteger();

				Tree.GetName();

				DB.m_Block	= WORD(Tree.GetValueAsInteger());

				Tree.GetName();

				DB.m_Offset	= WORD(Tree.GetValueAsInteger());

				Tree.GetName();

				DB.m_Type	= BYTE(Tree.GetValueAsInteger());

				Tree.GetName();

				DB.m_Text	= Tree.GetValueAsString();

				Tree.GetName();

				DB.m_Extent	= WORD(Tree.GetValueAsInteger());

				m_pBlocks->Append(DB);

				Tree.EndCollect();
				}

			Tree.EndObject();
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CS71KDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutObject(L"DBs");

	UINT uBlocks = m_pBlocks->GetCount();

	Tree.PutValue(L"DataBlocks", uBlocks);

	for( UINT u = 0; u < uBlocks; u++ ) {

		Tree.PutCollect(L"DB");

		Tree.PutValue(CPrintf(L"Ref%u",    u), m_pBlocks->GetAt(u).m_Ref);

		Tree.PutValue(CPrintf(L"Block%u",  u), m_pBlocks->GetAt(u).m_Block);

		Tree.PutValue(CPrintf(L"Offset%u", u), m_pBlocks->GetAt(u).m_Offset);

		Tree.PutValue(CPrintf(L"Type%u",   u), m_pBlocks->GetAt(u).m_Type);

		Tree.PutValue(CPrintf(L"Text%u",   u), m_pBlocks->GetAt(u).m_Text);

		Tree.PutValue(CPrintf(L"Ext%u",	   u), m_pBlocks->GetAt(u).m_Extent);

		Tree.EndCollect();
		}

	Tree.EndObject();
	}

void CS71KDeviceOptions::PostLoad(void)
{
	UINT uCount = m_pBlocks->GetCount();

	CAddress Last;

	for( UINT u = 0; u < uCount; u++ ) {

		CDataBlockRef Block = m_pBlocks->GetAt(u);

		Last.m_Ref = m_LastRef;

		CAddress Ref;

		Ref.m_Ref  = Block.m_Ref;

		if( Ref.a.m_Table < Last.a.m_Table ) {

			continue;
			}
	
		if( Ref.a.m_Table > Last.a.m_Table ) {

			m_LastRef = Block.m_Ref;
			}

		else if( Ref.a.m_Offset > Last.a.m_Offset ) {

			m_LastRef = Block.m_Ref;
			}			
		}
	}

// Download Support

BOOL CS71KDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_Slot));
	Init.AddByte(BYTE(m_Rack));
	Init.AddLong(LONG(m_Addr2));
	Init.AddByte(BYTE(m_Slot2));
	Init.AddByte(BYTE(m_Rack2));

	UINT uDBs = m_pBlocks->GetCount();

	Init.AddLong(uDBs);

	for( UINT u = 0; u < uDBs; u++ ) {

		Init.AddLong(m_pBlocks->GetAt(u).m_Ref);
		Init.AddWord(m_pBlocks->GetAt(u).m_Block);
		Init.AddWord(m_pBlocks->GetAt(u).m_Offset);
		Init.AddByte(m_pBlocks->GetAt(u).m_Type);
		Init.AddWord(m_pBlocks->GetAt(u).m_Extent);
		}

	Init.AddByte(BYTE(m_TSAP));
	Init.AddWord(WORD(m_STsap));
	Init.AddWord(WORD(m_CTsap));
	Init.AddWord(WORD(m_STsap2));
	Init.AddWord(WORD(m_CTsap2));
	
	return TRUE;
	}

// Meta Data Creation

void CS71KDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Type);
	Meta_AddInteger(Slot);
	Meta_AddInteger(Rack);
	Meta_AddInteger(Addr2);
	Meta_AddInteger(Type2);
	Meta_AddInteger(Slot2);
	Meta_AddInteger(Rack2);
	Meta_AddInteger(TSAP);
	Meta_AddInteger(STsap);
	Meta_AddInteger(CTsap);
	Meta_AddInteger(STsap2);
	Meta_AddInteger(CTsap2);
	}

// Data Block Access

BOOL CS71KDeviceOptions::IsDataBlock(UINT uTable)
{
	return ((uTable >= boundMin) && (uTable <= boundMax));
	}

BOOL CS71KDeviceOptions::IsDataBlock(CString Text)
{
	return Text.Find(':') < NOTHING;	
	}

BOOL  CS71KDeviceOptions::ExpandDataBlock(DWORD dwRef, WORD &wBlock, WORD &wOffset)
{
	UINT uCount = m_pBlocks->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CDataBlockRef Block = m_pBlocks->GetAt(u);

		if( IsDataBlockMatch(&Block, dwRef) || IsUpdateCandidate(&Block, dwRef) ) {

			wBlock	= Block.m_Block;

			wOffset = Block.m_Offset + WORD(dwRef - Block.m_Ref);

			return TRUE;
			}
		}

	return FALSE;
	}

// Data Block Operations

DWORD CS71KDeviceOptions::AppendDataBlock(CDataBlockRef &Block) 
{
	if( !DataBlockExist(Block) ) {

		if( !UpdateDataBlock(Block) ) {

			if( Block.m_Ref == 0 ) {

				CAddress Ref;

				Ref.m_Ref    = FindNextDataBlockRef();

				Ref.a.m_Type = Block.m_Type;

				Block.m_Ref  = Ref.m_Ref;

				m_LastRef    = Ref.m_Ref;

				m_pBlocks->Append(Block);
				}
			}
		}

	return Block.m_Ref;
	}

void CS71KDeviceOptions::UpdateDataBlocks(CAddress const &Address, INT nSize)
{
	if( nSize == NOTHING || nSize <= 1 ) {

		return;
		}

	if( IsDataBlock(Address.a.m_Table) ) {

		UINT uCount = m_pBlocks->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CDataBlockRef Block = m_pBlocks->GetAt(u);

			if( Block.m_Ref == Address.m_Ref ) {

				Block.m_Extent = WORD(nSize);

				m_pBlocks->SetAt(u, Block);

				return;
				}

			WORD wExt  = GetDataBlockExtent(&Block);

			BYTE bSize = GetSizeOfEntity(Block.m_Type);

			if( Block.m_Ref + wExt - bSize == Address.m_Ref ) {

				Block.m_Extent += WORD(nSize - 1);

				m_pBlocks->SetAt(u, Block);
				
				return;
				}
			}
		}
	}

// Device Access

CString CS71KDeviceOptions::GetDeviceName(void)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pData = pItem->FindMetaData(TEXT("Name"));

	return pData ? pData->ReadString(pItem) : TEXT("Device");
	}

// Import Operations

BOOL CS71KDeviceOptions::ImportDB(FILE *pFile, CString Path, IMakeTags *pTags, CString &Error)
{
	if( pTags ) {

		CString File = FindFilename(Path);

		if( pTags->AddRootFolder(L"", GetDeviceName(), L"") ) {

			afxThread->SetWaitMode(TRUE);

			pTags->AddFolder(L"", File, Path);

			UINT uDB  = tatoi( File.Mid(2) );

			UINT uExt = 0;
		
			wchar_t sLine[512] = {0};

			while ( !feof(pFile) ) {

				if( fgetws(sLine, sizeof(sLine), pFile) ) {

					CStringArray Array;

					CString Line = sLine;

					while( Line.GetAt(0) == S7_DB_DELIM ) {

						Line = Line.Mid(1);
						}

					Line.Tokenize(Array, S7_DB_DELIM);

					if( Array[dbNum].IsEmpty() ) {

						continue;
						}

					if( Array[dbNum].Find('.') == NOTHING ) {

						pTags->EndFolder();

						afxThread->SetWaitMode(FALSE);

						return FALSE;
						}

					if( Array[dbType].StartsWith(L"Array[0..") ) {

						UINT uEnd = Array[dbType].Find(']');

						if( uEnd < NOTHING ) {

							uExt = 1 + tstrtol(Array[dbType].Mid(9, uEnd - 1), NULL, 10);
							}
						}
					
					MakeDBTag(uDB, Array, pTags, Error, uExt);
					
					uExt = 0;
					}
				}

			pTags->EndFolder();

			afxThread->SetWaitMode(FALSE);
			}
		}

	return TRUE;
	}

BOOL CS71KDeviceOptions::ImportTags(FILE *pFile, CString Path, IMakeTags *pTags, CString &Error)
{
	if( pTags ) {

		CString File = FindFilename(Path);

		if( pTags->AddRootFolder(L"", GetDeviceName(), L"") ) {

			afxThread->SetWaitMode(TRUE);

			pTags->AddFolder(L"", File, Path);

			UINT uRow = 0;
			
			char sLine[512] = {0};

			while ( !feof(pFile) ) {

				if( fgets(sLine, sizeof(sLine), pFile) ) {

					if( uRow > 0 ) {

						MakeTag(sLine, pTags, Error);
						}

					uRow++;
					}
				}

			pTags->EndFolder();

			afxThread->SetWaitMode(FALSE);
			}
		}

	return TRUE;
	}

CString CS71KDeviceOptions::FindFilename(CString Path)
{
	CString File = Path;

	UINT uBegin = 0;

	while( File.Find('\\') < NOTHING ) {

		uBegin = File.Find('\\');

		File = File.Mid(uBegin + 1);
		}

	uBegin = 0;
			
	while( File.Find('/') < NOTHING ) {

		uBegin = File.Find('/');

		File = File.Mid(uBegin + 1);
		}

	UINT uEnd   = File.Find('.');

	if( uEnd < NOTHING ) {

		File = File.Mid(0, uEnd);
		}

	Validate(File);

	return File;
	}


// Data Block Implementation

BOOL  CS71KDeviceOptions::DataBlockExist(CDataBlockRef &Exist)
{
	UINT uCount = m_pBlocks->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CDataBlockRef Block = m_pBlocks->GetAt(u);

		if( IsDataBlockMatch(&Block, &Exist) ) {

			if( GetReference(Block, Exist) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CS71KDeviceOptions::UpdateDataBlock(CDataBlockRef &Add)
{
	UINT uCount = m_pBlocks->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CDataBlockRef Block = m_pBlocks->GetAt(u);

		if( Block.m_Ref == m_LastRef ) {
			
			if( GetReference(Block, Add) ) {

				m_pBlocks->Remove(u);

				m_pBlocks->Insert(u, Block);

				return TRUE;
				}

			Add.m_Ref = 0;

			break;
			}
		}

	return FALSE;
	}

DWORD CS71KDeviceOptions::FindNextDataBlockRef(void)
{
	CAddress Addr;

	Addr.m_Ref = 0;

	Addr.a.m_Table = boundMin;

	UINT uCount = m_pBlocks->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CDataBlockRef Block = m_pBlocks->GetAt(u);

		if( m_LastRef == Block.m_Ref ) {

			Addr.m_Ref  =  m_LastRef;

			if( IsEndOfBlock(Addr.a.m_Offset, Block) ) {

				Addr.a.m_Table++;

				Addr.a.m_Offset = 0;

				return Addr.m_Ref;
				}

			UINT uExtent = GetDataBlockExtent(&Block);

			MakeMax(uExtent, boundExt);
				
			Addr.a.m_Offset += uExtent;
			
			return Addr.m_Ref;
			}
		}

	return max(m_LastRef, Addr.m_Ref);
	}

BOOL CS71KDeviceOptions::IsEndOfBlock(WORD wOffset, CDataBlockRef &Block)
{
	UINT uExtent = GetDataBlockExtent(&Block);

	MakeMax(uExtent, boundExt);

	UINT uOffset = wOffset + uExtent + GetSizeOfEntity(Block.m_Type);

	return uOffset > 0xFFFF;
	}

BOOL  CS71KDeviceOptions::IsThisDataBlock(CDataBlockRef * pThis, CDataBlockRef * pBlock)
{
	if( pThis && pBlock )  {

		if( pThis->m_Block == pBlock->m_Block ) {

			if( pThis->m_Type == pBlock->m_Type ) {

				if( pThis->m_Offset <= pBlock->m_Offset ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL  CS71KDeviceOptions::IsUpdateCandidate(CDataBlockRef * pBlock, CDataBlockRef * pAdd)
{
	if( pBlock && pAdd ) {

		if( IsThisDataBlock(pBlock, pAdd) ) {

			UINT uOffset = pBlock->m_Offset + GetDataBlockExtent(pBlock);

			return pAdd->m_Offset == uOffset;
			}
		}

	return FALSE;
	}

BOOL  CS71KDeviceOptions::IsUpdateCandidate(CDataBlockRef * pBlock, DWORD dwRef)
{
	if( pBlock  ) {

		UINT uNext = GetDataBlockMaxRef(pBlock) + GetSizeOfEntity(pBlock->m_Type);

		return dwRef == uNext;
		}

	return FALSE;
	}

BOOL  CS71KDeviceOptions::IsDataBlockMatch(CDataBlockRef * pBlock, CDataBlockRef * pAdd)
{
	if( pBlock && pAdd )  {

		if( IsThisDataBlock(pBlock, pAdd) ) {

			UINT uOffset = pBlock->m_Offset + GetDataBlockExtent(pBlock);

			return pAdd->m_Offset < uOffset;
			}
		}

	return FALSE;
	}

BOOL  CS71KDeviceOptions::IsDataBlockMatch(CDataBlockRef * pBlock, DWORD dwRef)
{
	if( pBlock ) {

		if( dwRef >= pBlock->m_Ref ) {

			return dwRef <= GetDataBlockMaxRef(pBlock);
			}
		}

	return FALSE;
	}

BOOL  CS71KDeviceOptions::IsDataBlockOverlap(CDataBlockRef * pBlock, CDataBlockRef * pAdd, WORD &wOffset, WORD &wExtent)
{
	if( IsDataBlockMatch(pBlock, pAdd) || IsUpdateCandidate(pBlock, pAdd) ) {

		wOffset = (pAdd->m_Offset - pBlock->m_Offset) / GetSizeOfEntity(pAdd->m_Type);

		if( wOffset + pAdd->m_Extent <= pBlock->m_Extent ) {

			wExtent = 0;

			return TRUE;
			}
		
		wExtent = pBlock->m_Extent - wOffset + pAdd->m_Extent;
			
		return pBlock->m_Extent + wExtent > pBlock->m_Extent;
		}
	
	return FALSE;
	}

BOOL CS71KDeviceOptions::GetReference(CDataBlockRef &Block, CDataBlockRef &Add)
{
	WORD wOffset = 0;

	WORD wExtent = 0;

	if( IsDataBlockOverlap(&Block, &Add, wOffset, wExtent ) ) {

		CAddress Address;

		Address.m_Ref = Block.m_Ref;

		if( !IsEndOfBlock(Address.a.m_Offset, Block) ) {

			Address.a.m_Offset += wOffset * GetSizeOfEntity(Block.m_Type);

			Add.m_Ref = Address.m_Ref;

			Block.m_Extent += wExtent;

			return TRUE;
			}
		}

	return FALSE;
	}

WORD  CS71KDeviceOptions::GetDataBlockExtent(CDataBlockRef * pBlock)
{
	if( pBlock ) {

		return pBlock->m_Extent * GetSizeOfEntity(pBlock->m_Type);
		}

	return 0;
	}

BYTE  CS71KDeviceOptions::GetSizeOfEntity(UINT uType)
{
	switch( uType ) {

		case addrByteAsWord:	return 2;
		
		case addrByteAsReal:
		case addrByteAsLong:	return 4;		
		}

	return 1;
	}

DWORD  CS71KDeviceOptions::GetDataBlockMaxRef(CDataBlockRef * pBlock)
{
	if( pBlock ) {

		UINT uSize = GetSizeOfEntity(pBlock->m_Type);

		return pBlock->m_Ref + pBlock->m_Extent * uSize - uSize;
		}

	return 0;
	}

void CS71KDeviceOptions::PurgeDataBlocks(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		PrintDataBlocks();

		RebuildDataBlocks();

		pSystem->Rebuild(1);

		PrintDataBlocks();
		}
	}

void CS71KDeviceOptions::SortDataBlocks(void)
{
	Print(CString("\nSort "));

	m_pBlocks->Sort();

	PrintDataBlocks();
	}

void CS71KDeviceOptions::RebuildDataBlocks(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		m_pBlocks->Empty();

		m_LastRef = 0;

		pSystem->Rebuild(1);

		SortDataBlocks();

		CompressDataBlocks();

		WalkDataBlocks();
		}
	}

void CS71KDeviceOptions::WalkDataBlocks(void)
{
	Print(CString("\nWalk "));

	UINT uCount = m_pBlocks->GetCount();

	CAddress Ref;

	Ref.m_Ref     = 0;

	Ref.a.m_Table = boundMin;

	m_LastRef     = Ref.m_Ref;

	for( UINT u = 0; u < uCount; u++ ) {

		CDataBlockRef Block = m_pBlocks->GetAt(u);

		Ref.m_Ref = FindNextDataBlockRef();

		if( IsEndOfBlock(Ref.a.m_Offset, Block) ) {

			Ref.a.m_Table++;

			Ref.a.m_Offset = 0;
			}
			 
		Ref.a.m_Type = Block.m_Type;

		Block.m_Ref  = Ref.m_Ref;

		m_LastRef    = Ref.m_Ref;

		m_pBlocks->Remove(u);

		m_pBlocks->Insert(u, Block);
		}

	PrintDataBlocks();
	}

void  CS71KDeviceOptions::CompressDataBlocks(void)
{
	Print(CString("\nCompress "));

	if( !m_pBlocks->IsEmpty() ) {

		UINT uCount = m_pBlocks->GetCount();	

		for( UINT u = 0; u < uCount - 1; u++ ) {

			CDataBlockRef Block = m_pBlocks->GetAt(u);

			CDataBlockRef Next  = m_pBlocks->GetAt(u+1);

			if( IsDataBlockSpanning(Block, Next) ) {

				Block.m_Extent += Next.m_Extent;

				Block.m_Ref = 0;

				m_pBlocks->Remove(u);

				m_pBlocks->Insert(u, Block);

				m_pBlocks->Remove(u+1);

				uCount--;
				}
			}
		}

	PrintDataBlocks();
	}
		
BOOL  CS71KDeviceOptions::IsDataBlockSpanning(CDataBlockRef &Block, CDataBlockRef &Next)
{
	CAddress Ref;

	Ref.m_Ref = Block.m_Ref;

	if( IsEndOfBlock(Ref.a.m_Offset, Block) ) {

		return( GetDataBlockExtent(&Block) == Next.m_Offset ); 
		}

	return FALSE;
	}

void CS71KDeviceOptions::PrintDataBlocks(void)
{
	if( S7_1K_DEBUG ) {

		CAddress Ref;

		UINT uCount = m_pBlocks->GetCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CDataBlockRef Block = m_pBlocks->GetAt(u);

			Ref.m_Ref = Block.m_Ref;

			AfxTrace(L"\nBlock %u %s ", u, Block.m_Text);

			AfxTrace(L"Ref %8.8x table %2.2x offset %u type %u ", Ref.m_Ref,
									      Ref.a.m_Table,
									      Ref.a.m_Offset,
									      Ref.a.m_Type);

			AfxTrace(L"datablock %u db offset %u db type %u ext %u", Block.m_Block,
										 Block.m_Offset,
										 Block.m_Type,
										 Block.m_Extent);
			}
		}
	}

// Tag Creation

void CS71KDeviceOptions::MakeDBTag(UINT uDB, CStringArray Array, IMakeTags * pTags, CString &Error, UINT uExtent)
{
	CString Tag = TEXT("DB%5.5u:");

	Tag.Printf(Tag, uDB);

	UINT uCount = Array.GetCount();

	if( uCount >= dbVal ) {

		UINT uBit  = 0;

		CString Number = Array[dbNum];

		UINT uFind = Number.Find('.');

		if( uFind < NOTHING ) {

			uBit = tatoi(Number.Mid(uFind + 1));

			Number = Number.Mid(0, uFind);
			}

		Tag += Number;

		CString Type = Array[dbType];

		uFind = Type.FindRev(' ');

		if( uFind < NOTHING ) {

			Type = Type.Mid(uFind + 1);
			}

		Tag += AdjustType(Type);
				
		CString Value = L'[' + Tag + L']';

		CString Name = Array[dbName];

		Validate(Name);

		if( IsFlag(Type) ) {

			if( !pTags->AddFlag( Name, Name, Value, uExtent, 3 | uBit << 8, TRUE, L"", L"") ) {

				Error += GetErrorText(Name, Array[dbName]);
				}
			}

		else if( IsReal(Type) ) {

			if( !pTags->AddReal( Name, Name, Value, uExtent, TRUE, L"", L"", 0) ) {

				Error += GetErrorText(Name, Array[dbName]);
				}
			}

		else {  
			if( !pTags->AddInt( Name, Name, Value, uExtent, TRUE, L"", L"", 0, 3) ) {

				Error += GetErrorText(Name, Array[dbName]);
				}
			}
		}
	}

void CS71KDeviceOptions::MakeTag(CString Text, IMakeTags * pTags, CString &Error)
{
	CStringArray Array;

	Text.Tokenize(Array, ',');

	UINT uCount = Array.GetCount();

	if( uCount >= tagTag ) {

		UINT uBit  = 0;

		CString Number = Array[tagTag];

		UINT uFind = Number.Find('.');

		if( uFind < NOTHING ) {

			uBit = tatoi(Number.Mid(uFind + 1));

			Number = Number.Mid(0, uFind);
			}

		CString Tag = Array[tagTag];

		Tag.Delete(0, 1);

		if( isalpha(Tag.GetAt(1)) ) {

			Tag.Delete(1, 1);
			}

		Tag.Insert(1, 'B');

		Tag += AdjustType(Array[tagType]);
				
		CString Value = L'[' + Tag + L']';

		CString Name = Array[tagName];

		Validate(Name);

		if( IsFlag(Array[tagType]) ) {

			if( !pTags->AddFlag( Name, Name, Value, 0, 3 | uBit << 8, TRUE, L"", L"") ) {

				Error += GetErrorText(Name, Array[tagName]);
				}
			}

		else if( IsReal(Array[tagType]) ) {

			if( !pTags->AddReal( Name, Name, Value, 0, TRUE, L"", L"", 0) ) {

				Error += GetErrorText(Name, Array[tagName]);
				}
			}

		else {
			if( !pTags->AddInt( Name, Name, Value, 0, TRUE, L"", L"", 0, 3) ) {

				Error += GetErrorText(Name, Array[tagName]);
				}
			}
		}
	}

// Helpers

CString	CS71KDeviceOptions::GetErrorText(CString Name, CString Tag)
{
	CString Error = Tag;

	if( Error.GetLength() > 35 ) {

		Error = Error.Mid(0, 32);

		Error += CString("...");
		}

	Error +=  CString("\t");

	Error += Name.GetLength() > 32 ? CPrintf(IDS_ERR_TOO_LONG, 32) : CPrintf(IDS_ERR_EXIST_AS, Name);

	Error += CString("\r\n%c");
	
	Error.Printf(Error, S7_DELIM);

	return Error;
	}

void CS71KDeviceOptions::Validate(CString &String)
{
	UINT uCount = String.GetLength();

	for( UINT u = 0; u < uCount; u++ ) {

		TCHAR c = String.GetAt(u);

		if( u == 0 && isdigit(c) ) {

			String.Replace(c, '_');
			}

		else if( !isalnum(c) || isspace(c) ) {

			String.Replace(c, '_');
			}
		}
	}

void CS71KDeviceOptions::Print(CString Text)
{
	if( S7_1K_DEBUG ) {

		AfxTrace(L"%s", Text);
		}
	}

CString CS71KDeviceOptions::AdjustType(CString Type)
{
	Type.ToUpper();

	if( Type == "REAL" ) {

		return CString(".REAL");
		}

	if( Type == "WORD" || Type == "INT" || Type == "UINT" ) {

		return CString(".WORD");
		}

	if( Type == "LONG" || Type == "DINT" || Type == "DWORD" ) {

		return CString(".LONG");
		}

	return CString(".BYTE");
	}

BOOL CS71KDeviceOptions::IsReal(CString Type)
{
	Type.ToUpper();

	return Type == CString("REAL");
	}

BOOL CS71KDeviceOptions::IsFlag(CString Type)
{
	Type.ToUpper();

	return Type == CString("BOOL");
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Device Options UI Page
//


// Runtime Class

AfxImplementRuntimeClass(CS71KDeviceOptionsUIPage, CUIPage);

// Constructor

CS71KDeviceOptionsUIPage::CS71KDeviceOptionsUIPage(CS71KDeviceOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CS71KDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_DEVICE_2), 1);

	pView->AddUI(pItem, L"root", L"Addr");

	pView->AddUI(pItem, L"root", L"Type");

	pView->AddUI(pItem, L"root", L"TSAP");

	pView->AddUI(pItem, L"root", L"STsap");

	pView->AddUI(pItem, L"root", L"CTsap");

	//pView->AddUI(pItem, L"root", L"Rack");

	//pView->AddUI(pItem, L"root", L"Slot");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_FALLBACK), 1);

	pView->AddUI(pItem, L"root", L"Addr2");

	pView->AddUI(pItem, L"root", L"Type2");

	pView->AddUI(pItem, L"root", L"STsap2");

	pView->AddUI(pItem, L"root", L"CTsap2");

	//pView->AddUI(pItem, L"root", L"Rack2");

	//pView->AddUI(pItem, L"root", L"Slot2");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_PROTOCOL_OPTIONS), 1);

	pView->AddUI(pItem, L"root", L"Keep");

	pView->AddUI(pItem, L"root", L"Ping");

	pView->AddUI(pItem, L"root", L"Time1");

	pView->AddUI(pItem, L"root", L"Time3");

	pView->AddUI(pItem, L"root", L"Time2");

	pView->EndGroup(TRUE);

	return TRUE;
	}



//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Driver
//

// Instantiator

ICommsDriver *	Create_S71KDriver(void)
{
	return New CS71KDriver;
	}

// Constructor

CS71KDriver::CS71KDriver(void)
{
	m_wID		= 0x40C9;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 TCP/IP Master with TIA S7 Tag Import";
	
	m_Version	= "1.02";
	
	m_ShortName	= "S7";

	AddSpaces();
	}

// Configuration

CLASS CS71KDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CS71KDriverOptions);
	}

// Configuration

CLASS CS71KDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CS71KDeviceOptions);
	}

// Driver Data

UINT CS71KDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagImport;
	}

// Binding Control

UINT CS71KDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CS71KDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Implementation

void CS71KDriver::AddSpaces(void)
{
	AddSpace(New CSpaceS71K(spaceQB, "QB", "Output Bytes",	10, 0,  65535, addrByteAsByte, addrByteAsReal));
   
	AddSpace(New CSpaceS71K(spaceIB, "IB", "Input Bytes",	10, 0,  65535, addrByteAsByte, addrByteAsReal));
	
	AddSpace(New CSpaceS71K(spaceMB, "MB", "Flag Bytes",	10, 0,  65535, addrByteAsByte, addrByteAsReal));

	AddSpace(New CSpaceS71K(spaceDB, "DB", "Data Blocks",	10, 0,  65535, addrByteAsByte, addrByteAsReal));
	}

// Notifications

void CS71KDriver::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *) pConfig;

	if( pOpt ) {

		pOpt->UpdateDataBlocks(Addr, nSize);
		}
	}

void CS71KDriver::NotifyInit(CItem * pConfig)
{
	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->RebuildDataBlocks();
		}
	}

// Address Management

BOOL CS71KDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CS71KDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CS71KDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *) pConfig;

	if( pOpt ) {

		UINT uDB = 0;

		CString Simple = Text;

		if( pOpt->IsDataBlock(Text) ) {

			UINT uFind = Text.Find(':');

			uDB = tatoi(Text.Mid(0, uFind));

			Simple = Simple.Mid(uFind + 1);
			}

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Simple) ) {

			if( uDB ) {

				if( uDB == 0 || uDB > blockMax ) {

					Error.Set(CPrintf(CString(IDS_DATA_BLOCK_MUST), blockMin, blockMax));

					return FALSE;
					}

				CDataBlockRef DB;

				DB.m_Ref	= 0;

				DB.m_Text	= Text;

				DB.m_Block	= WORD(uDB);

				DB.m_Offset	= Addr.a.m_Offset;

				DB.m_Type	= Addr.a.m_Type;

				DB.m_Extent	= 1;

				Addr.m_Ref      = pOpt->AppendDataBlock(DB);

				if( Addr.m_Ref == 0 ) {

					Error.Set(CString(IDS_UNABLE_TO_MAP));

					return FALSE;
					}
				}
			}
		
		return TRUE; 
		}

	return FALSE;
	}

BOOL CS71KDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *) pConfig;

	CSpaceS71K * pSpace = (CSpaceS71K *) GetSpace(Addr);

	if( pOpt && pSpace ) {

		if( pOpt->IsDataBlock(Addr.a.m_Table) ) {

			WORD wBlock  = pSpace->GetBlock(Addr);

			WORD wOffset = Addr.a.m_Offset;

			BYTE bType   = Addr.a.m_Type; 

			pOpt->ExpandDataBlock(Addr.m_Ref, wBlock, wOffset);

			if( bType == pSpace->m_uType ) {

				Text.Printf( "%s%5.5u:%s", 
					pSpace->m_Prefix, 
					wBlock,
					pSpace->GetValueAsText(wOffset)
					);
				}
	
			else {
				Text.Printf( "%s%5.5u:%s.%s", 
					pSpace->m_Prefix, 
					wBlock,
					pSpace->GetValueAsText (wOffset),
					pSpace->GetTypeModifier(bType)
					);
				}

			return TRUE;
			}
		
		if( Addr.a.m_Type == pSpace->m_uType ) {

			Text.Printf( "%s%s", 
				     pSpace->m_Prefix, 
				     pSpace->GetValueAsText(Addr.a.m_Offset)
				     );
			}
		else {
			Text.Printf( "%s%s.%s", 
				     pSpace->m_Prefix, 
				     pSpace->GetValueAsText (Addr.a.m_Offset),
				     pSpace->GetTypeModifier(Addr.a.m_Type)
				     );
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CS71KDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Tag Import

BOOL CS71KDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *)pConfig;

	if( pOpt ) {

		COpenFileDialog Dlg;

		Dlg.LoadLastPath(TEXT("S7 Files"));

		Dlg.SetCaption(CPrintf("S7 Import for %s", pOpt->GetDeviceName()));

		Dlg.SetFilter(TEXT("S7 Data Block File (*.s7db)|*.s7db|CSV Files (*.csv)|*.csv"));

		if( Dlg.Execute(*afxMainWnd) ) {

			FILE *pFile;

			CString Text = "";
		
			if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

				Text = CString(IDS_UNABLE_TO_OPEN);

				afxMainWnd->Error(Text);
				}
			else {
				CString Path = Dlg.GetFilename();

				CString File = pOpt->FindFilename(Path);

				File.ToUpper();

				CString Error = "";
				
				if( Dlg.GetFilename().EndsWith(TEXT(".s7db")) ) {
					
					if( File.Mid(0,2) == CString(L"DB") ) {

						if( !pOpt->ImportDB(pFile, Path, pTags, Error) ) {

							Text = CString(IDS_IMPORT_OPERATION);
							}
						}
					else {
						Text = CString(IDS_DATA_BLOCK_FILES);
						}
					}

				else if( Dlg.GetFilename().EndsWith(TEXT(".csv")) ) {
			
					pOpt->ImportTags(pFile, Path, pTags, Error);
					}
				else {
					Text = CString(IDS_UNSUPPORTED_FILE);
					}

				Dlg.SaveLastPath(TEXT("S7 Files"));
			
				fclose(pFile);

				if( !Text.IsEmpty() ) {

					afxMainWnd->Error(Text);
					}

				if( !Error.IsEmpty() ) {

					CStringArray List;

					Error.Tokenize(List, S7_DELIM);

					CS7ImportErrorDialog Dlg(List);

					Dlg.Execute();
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CS71KDialog, CStdAddrDialog);
		
// Constructor

CS71KDialog::CS71KDialog(CS71KDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = L"S71KElementDlg";
	}

// Overridables

void CS71KDialog::SetAddressFocus(void)
{
	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *) m_pConfig;

	if( pOpt && m_pSpace && pOpt->IsDataBlock(m_pSpace->m_uTable) ) {

		SetDlgFocus(2005);

		return;
		}
	
	SetDlgFocus(2002);
	}

void CS71KDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *) m_pConfig;

	if( pOpt && m_pSpace && pOpt->IsDataBlock(m_pSpace->m_uTable) ) {

		if( fEnable ) {

			UINT uFind = Text.Find(':');

			GetDlgItem(2002).SetWindowText(Text.Left(uFind));

			GetDlgItem(2004).SetWindowText(":");

			GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fEnable);
		
		GetDlgItem(2005).EnableWindow(fEnable);
		}
	else {
		if( fEnable ) {

			GetDlgItem(2002).SetWindowText(Text);

			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2005).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		}
	}

CString CS71KDialog::GetAddressText(void)
{
	CS71KDeviceOptions * pOpt = (CS71KDeviceOptions *) m_pConfig;

	if( pOpt && m_pSpace && pOpt->IsDataBlock(m_pSpace->m_uTable) ) {
	
		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += ":";
		
		Text += GetDlgItem(2005).GetWindowText();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Space Wrapper Class
//

// Constructor

CSpaceS71K::CSpaceS71K(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s) : CSpace(uTable, p, c, r, n, x, t, s)
{
	}

// Data Access

WORD CSpaceS71K::GetBlock(CAddress Addr)
{
	if( Addr.a.m_Extra == 2 ) {

		return WORD(blockMax);
		}

	return WORD(blockMin);
	}

// Matching

BOOL CSpaceS71K::MatchSpace(CAddress const &Addr)
{
	if( CSpace::MatchSpace(Addr) ) {

		return TRUE;
		}

	if( m_uTable == spaceDB ) {

		return Addr.a.m_Table >= 0x0A;
		}

	return FALSE;
	}

// Limits

void CSpaceS71K::GetMinimum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);

	if( m_uTable >= 0x0A ) {

		Addr.a.m_Extra = 1;
		}
	}

void CSpaceS71K::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	if( m_uTable >= 0x0A ) {

		Addr.a.m_Extra = 2;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 Import Error List
//

// Runtime Class

AfxImplementRuntimeClass(CS7ImportErrorDialog, CStdDialog);
		
// Constructor

CS7ImportErrorDialog::CS7ImportErrorDialog(CStringArray List)
{
	m_Errors = List;

	SetName(L"S7ImportErrorDlg");
	}

// Message Map

AfxMessageMap(CS7ImportErrorDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(1003, OnCopy)
	
	AfxMessageEnd(CS7ImportErrorDialog)
	};
 
// Message Handlers

BOOL CS7ImportErrorDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadErrorList();
		
	GetDlgItem(1002).SetWindowText(CString(IDS_ERR_IMPORT));

	return TRUE;
	}

// Command Handlers

BOOL CS7ImportErrorDialog::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CS7ImportErrorDialog::OnCopy(UINT uID)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("S7 Files"));

	Dlg.SetCaption(CPrintf("S7 Import Errors"));

	Dlg.SetFilter(TEXT("TEXT Files (*.txt)|*.txt|CSV Files (*.csv)|*.csv"));

	if( Dlg.Execute(*afxMainWnd) ) {

		FILE *pFile;

		CString Text = "";
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			Text = CString(IDS_UNABLE_TO_OPEN_2);

			afxMainWnd->Error(Text);
			}
		else {
			AfxAssume(pFile);

			CString Path = Dlg.GetFilename();
			
			CString Error = "";

			BOOL fCSV = Dlg.GetFilename().EndsWith(TEXT(".csv"));
			
			UINT uCount = m_Errors.GetCount();

			for( UINT u = 0; u < uCount; u++ ) {

				CString Text = m_Errors.GetAt(u);  

				if( fCSV ) {

					Text.Replace('\t', ',');

					Text.Remove('\r');
					}

				fprintf(pFile, TEXT("%s"), PCTXT(Text));
				}

			Information(CString(IDS_COPY_IS_COMPLETE));
			
			fclose(pFile);
			}
		}

	return TRUE;
	}

// Implementation

void CS7ImportErrorDialog::LoadErrorList(void)
{
	if( !m_Errors.IsEmpty() ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		Box.ResetContent();

		int nTab[] = { 150 };

		Box.SetTabStops(elements(nTab), nTab);

		Box.AddStrings(m_Errors);
		}
	}


// End of File
