
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EURO635_HPP
	
#define	INCLUDE_EURO635_HPP

#define AN addrNamed
#define LL addrLongAsLong
#define	WW addrWordAsWord
#define	YY addrByteAsByte
#define	BB addrBitAsBit

class CEuro635Driver;

//////////////////////////////////////////////////////////////////////////
//
// Euro635 Comms Driver
//

class CEuro635Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuro635Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
