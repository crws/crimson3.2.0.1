
#include "intern.hpp"

#include "krohne.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Krohne Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CKrohneDeviceOptions, CUIItem);

// Constructor

CKrohneDeviceOptions::CKrohneDeviceOptions(void)
{
	m_Device  = 0x91;

	m_Address = 0;
	}

// UI Management

void CKrohneDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CKrohneDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddByte(BYTE(m_Address));

	return TRUE;
	}

// Meta Data Creation

void CKrohneDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Address);
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Named Comms Driver
//

// Instantiator

ICommsDriver *	Create_KrohneDriver(void)
{
	return New CKrohneDriver;
	}

// Constructor

CKrohneDriver::CKrohneDriver(void)
{
	m_wID		= 0x4002;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Krohne";
	
	m_DriverName	= "Flowmeter IFC020D";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Krohne";

	AddSpaces();
	}

// Binding Control

UINT	CKrohneDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CKrohneDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CKrohneDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CKrohneDeviceOptions);
	}

// Implementation

void CKrohneDriver::AddSpaces(void)
{
	AddSpace(New CSpace("C00",	"Reset Device",		  1, BB));
//	AddSpace(New CSpace("C01",	"Reserved",		  2, BB));
//	AddSpace(New CSpace("C02",	"Reserved",		  3, BB));
//	AddSpace(New CSpace("C03",	"Reserved",		  4, BB));
//	AddSpace(New CSpace("C04",	"Reserved",		  5, BB));
	AddSpace(New CSpace("C05",	"Zero Flow Calibrate",	  6, BB));
	AddSpace(New CSpace("C06",	"Reset Totalizers",	  7, BB));
	AddSpace(New CSpace("C07",	"Write Totalizers",	  8, BB));
	AddSpace(New CSpace("C08",	"Reset Errors",		  9, BB));
//	AddSpace(New CSpace("C09",	"Simulation",		 10, BB));
	AddSpace(New CSpace("C17",	"Cached Pos Totalizer",	 11, RR));
	AddSpace(New CSpace("C27",	"Cached Neg Totalizer",	 12, RR));

        AddSpace(New CSpace("M00",	"Flow raw",              13, RR));
        AddSpace(New CSpace("M01",	"Flow tau",              14, RR));
        AddSpace(New CSpace("M02",	"Flow raw %",            15, RR));
        AddSpace(New CSpace("M03",	"Flow tau %",            16, RR));
        AddSpace(New CSpace("M04",	"Counter + (4 Bytes)",   17, RR));
        AddSpace(New CSpace("M05",	"Counter - (4 Bytes)",   18, RR));
        AddSpace(New CSpace("M06",	"Actual Status",         19, BB));

        AddSpace(New CSpace("S00",	"Module Errors",         20, BB));
        AddSpace(New CSpace("S01",	"Module Warnings",       21, BB));
								 
        AddSpace(New CSpace("S10",	"EEPROM Errors",         22, BB));
        AddSpace(New CSpace("S11",	"Warning Levels",        23, BB));
        AddSpace(New CSpace("S12",	"Output Errors",         24, BB));
//      AddSpace(New CSpace("S13",	"reserved",              25, BB));
        AddSpace(New CSpace("S13",	"Indicator Status",      26, BB));

        AddSpace(New CSpace("S30",	"Display EEPROM Error",  27, BB));
        AddSpace(New CSpace("S31",	"Other Display Errors",  28, BB));

        AddSpace(New CSpace("S70",	"ADC EEPROM Error",      29, BB));
        AddSpace(New CSpace("S71",	"Other ADC Errors",      30, BB));
        AddSpace(New CSpace("S72",	"Parameter 7 Errors",    31, BB));
        AddSpace(New CSpace("S73",	"Other ADC Errors",      32, BB));
        AddSpace(New CSpace("S74",	"Maintenance Service",   33, BB));

        AddSpace(New CSpace("N00",	"Write Parameter Block", 34, WW));
        AddSpace(New CSpace("P00",	"Full Scale Format",     35, BB));
        AddSpace(New CSpace("P01",	"Full Scale",            36, RR));
        AddSpace(New CSpace("P02",	"Time Constant",         37, WW));
        AddSpace(New CSpace("P03",	"Diameter",              38, RR));
        AddSpace(New CSpace("P04",	"Main Constant",         39, RR));
        AddSpace(New CSpace("P05",	"Nullpunct",             40, RR));
        AddSpace(New CSpace("P06",	"Maintenance Bytes 0-4", 41, LL));
        AddSpace(New CSpace("P07",	"Maintenance Bytes 5-6", 42, WW));

        AddSpace(New CSpace("P10",	"Output Format Out 0",   43, LL));
        AddSpace(New CSpace("P11",	"Range Low Out 0",       44, BB));
        AddSpace(New CSpace("P12",	"Range High Out 0",      45, BB));
        AddSpace(New CSpace("P13",	"Imax Limit Out 0",      46, BB));
        AddSpace(New CSpace("P14",	"Error Value Out 0",     47, BB));
        AddSpace(New CSpace("P15",	"SMU on Out 0",          48, BB));
        AddSpace(New CSpace("P16",	"SMU off Out 0",         49, BB));
        AddSpace(New CSpace("P17",	"Output Format Out 1",   50, LL));
        AddSpace(New CSpace("P18",	"Range Low Out 1",       51, BB));
        AddSpace(New CSpace("P19",	"Range High Out 1",      52, BB));
        AddSpace(New CSpace("P1A",	"Imax Limit Out 1",      53, BB));
        AddSpace(New CSpace("P1B",	"Error Value Out 1",     54, BB));
        AddSpace(New CSpace("P1C",	"SMU on Out 1",          55, BB));
        AddSpace(New CSpace("P1D",	"SMU off Out 1",         56, BB));

        AddSpace(New CSpace("P20",	"Frequency Format",      57, LL));
        AddSpace(New CSpace("P21",	"Units Format",          58, BB));
        AddSpace(New CSpace("P22",	"Full Scale",            59, RR));
        AddSpace(New CSpace("P23",	"Pulse Time",            60, BB));
        AddSpace(New CSpace("P24",	"Fmax",                  61, BB));
        AddSpace(New CSpace("P25",	"SMU on",                62, BB));
        AddSpace(New CSpace("P26",	"SMU off",               63, BB));

        AddSpace(New CSpace("P30",	"Indicator Function",    64, BB));
        AddSpace(New CSpace("P31",	"On Limit",              65, WW));
        AddSpace(New CSpace("P32",	"Off Limit",             66, WW));
								 
        AddSpace(New CSpace("P40",	"Totalizer Function",    67, WW));
        AddSpace(New CSpace("P41",	"SMU on",                68, BB));
        AddSpace(New CSpace("P42",	"SMU off",               69, BB));
        AddSpace(New CSpace("P43",	"+ Totalizer Format",    70, WW));
        AddSpace(New CSpace("P44",	"- Totalizer Format",    71, WW));
								 
        AddSpace(New CSpace("P50",	"Display Function",      72, WW));
        AddSpace(New CSpace("P51",	"Flow Format",           73, WW));
        AddSpace(New CSpace("P52",	"Counters Format",       74, WW));

        AddSpace(New CSpace("P60",	"Location Bytes 1-4",    75, LL));
        AddSpace(New CSpace("P61",	"Location Bytes 5-8",    76, LL));
        AddSpace(New CSpace("P62",	"Location Bytes 9-10",   77, WW));
        AddSpace(New CSpace("P63",	"FSK Function",          78, BB));
        AddSpace(New CSpace("P64",	"FSK Channel Address",   79, BB));
//      AddSpace(New CSpace("P65",	"RS485 Channel Mode",    80, BB));

        AddSpace(New CSpace("P70",	"Language",              81, BB));
        AddSpace(New CSpace("P71",	"Units Text Bytes 1-4",  82, LL));
        AddSpace(New CSpace("P72",	"Units Text Bytes 5-8",  83, LL));
        AddSpace(New CSpace("P73",	"Units Text Bytes 9-10", 84, WW));
        AddSpace(New CSpace("P74",	"Volume Conversion",     85, RR));
        AddSpace(New CSpace("P75",	"Time Conversion",       86, RR));
        AddSpace(New CSpace("P76",	"Password",              87, WW));
        AddSpace(New CSpace("P77",	"Device Type",           88, WW));
        AddSpace(New CSpace("P78",	"Maintenance Service",   89, LL));
        AddSpace(New CSpace("P79",	"Device Number",         90, LL));
								 
        AddSpace(New CSpace("E00",	"Last Error Response",   91, WW));
//      AddSpace(New CSpace("ERR",	"Comms Error",           92, LL));
	}							 

// End of File
