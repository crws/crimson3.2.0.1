
#include "intern.hpp"

#include "mpe.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MP Electronics Driver
//

// Instantiator

ICommsDriver *	Create_MPEDriver(void)
{
	return New CMPEDriver;
	}

// Constructor

CMPEDriver::CMPEDriver(void)
{
	m_wID		= 0x402C;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "MP Electronics";
	
	m_DriverName	= "GTI Series and Similar";
	
	m_Version	= "1.00";
	
	m_ShortName	= "GTI Series";

	m_fSingle	= TRUE;
	}

// Binding Control

UINT CMPEDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMPEDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// End of File
