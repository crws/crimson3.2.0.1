
#include "intern.hpp"

#include "eatonelc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CEatonELCSerialDriverOptions, CUIItem);

// Constructor

CEatonELCSerialDriverOptions::CEatonELCSerialDriverOptions(void)
{
	m_Protocol = 0;
	}

// UI Managament

void CEatonELCSerialDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CEatonELCSerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Protocol));

	return TRUE;
	}

// Meta Data Creation

void CEatonELCSerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Protocol);
	}

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEatonELCSerialDeviceOptions, CUIItem);

// Constructor

CEatonELCSerialDeviceOptions::CEatonELCSerialDeviceOptions(void)
{
	m_Drop		= 1;

	m_fIgnoreReadEx = FALSE;
	}

// UI Managament

void CEatonELCSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CEatonELCSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_fIgnoreReadEx));

	return TRUE;
	}

// Meta Data Creation

void CEatonELCSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddBoolean(IgnoreReadEx);
	}

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Driver
//

// Instantiator

ICommsDriver *	Create_EatonELCSerialDriver(void)
{
	return New CEatonELCSerialDriver;
	}

// Constructor

CEatonELCSerialDriver::CEatonELCSerialDriver(void)
{
	m_wID		= 0x4085;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Eaton";
	
	m_DriverName	= "ELC Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Eaton ELC Master";

	AddSpaces();
	}

// Binding Control

UINT CEatonELCSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEatonELCSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEatonELCSerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEatonELCSerialDriverOptions);
	}

// Configuration

CLASS CEatonELCSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEatonELCSerialDeviceOptions);
	}

// Address Management

BOOL CEatonELCSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CEatonELCSerialAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CEatonELCSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type = StripType(pSpace, Text);

	CString sErr = "";

	CString sPre = pSpace->m_Prefix;

	BOOL fErr    = FALSE;

	PTXT pError  = NULL;

	UINT uOffset = tstrtoul(Text, &pError, 10);

	if( pError && pError[0] ) {

		sErr.Printf(TEXT("%s??"), sPre);

		fErr = TRUE;
		}

	if( !fErr ) {

		if( pSpace->IsOutOfRange(uOffset) ) {

			sErr.Printf(TEXT("%s%d - %s%d"), sPre, pSpace->m_uMinimum, sPre, pSpace->m_uMaximum);

			fErr = TRUE;
			}

		else {
			Addr.a.m_Type	= NormalizeType(pSpace->TypeFromModifier(Type));

			Addr.a.m_Table	= pSpace->m_uTable;
			Addr.a.m_Extra	= 0;
			Addr.a.m_Offset	= uOffset;

			return TRUE;
			}
		}

	AfxTouch(fErr);

	Error.Set(sErr, 0);

	return FALSE;
	}

BOOL CEatonELCSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uOffset = Addr.a.m_Offset;

		if( Addr.a.m_Type == pSpace->m_uType ) {

			Text.Printf( TEXT("%s%s"), 
				pSpace->m_Prefix, 
				pSpace->GetValueAsText(uOffset)
				);
			}
		else {
			Text.Printf( TEXT("%s%s.%s"),
				pSpace->m_Prefix, 
				pSpace->GetValueAsText (uOffset),
				pSpace->GetTypeModifier(NormalizeType(Addr.a.m_Type))
				);
			}
		
		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CEatonELCSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SP_D,	"D",	"Holding Register",		10,    0, 9999, addrWordAsWord, addrRealAsReal));
	
	AddSpace(New CSpace(SP_M,	"M",	"Memory Bit",			10,    0, 4095, addrBitAsBit));
	
	AddSpace(New CSpace(SP_S,	"S",	"Step Point/Memory Bit",	10,    0, 1023, addrBitAsBit));
	
	AddSpace(New CSpace(SP_X,	"X",	"External Input",		10,    0,  255, addrBitAsBit));
	
	AddSpace(New CSpace(SP_Y,	"Y",	"External Output",		10,    0,  255, addrBitAsBit));

	AddSpace(New CSpace(SP_T,	"TV",	"Timer Value",			10,    0,  255, addrWordAsWord));
	
	AddSpace(New CSpace(SP_U,	"TC",	"Timer Contact",		10,    0,  255, addrBitAsBit));
	
	AddSpace(New CSpace(SP_C,	"CV",	"Counter Value - 16 Bits",	10,    0,  255, addrWordAsWord));
	
	AddSpace(New CSpace(SP_A,	"CL",	"Counter Value - 32 Bits",	10,    0,  255, addrLongAsLong));
	
	AddSpace(New CSpace(SP_B,	"CC",	"Counter Contact",		10,    0,  255, addrBitAsBit));
	}

UINT CEatonELCSerialDriver::NormalizeType(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:	return addrLongAsLong;

		case addrWordAsReal:	return addrRealAsReal;
		}

	return uType;
	}

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CEatonELCSerialAddrDialog, CStdAddrDialog);
		
// Constructor

CEatonELCSerialAddrDialog::CEatonELCSerialAddrDialog(CEatonELCSerialDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	}

// Overridables

BOOL CEatonELCSerialAddrDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrWordAsWord:
		case addrLongAsLong:
		case addrRealAsReal:

			return TRUE;
		}

	return FALSE;
	}

// End of File
