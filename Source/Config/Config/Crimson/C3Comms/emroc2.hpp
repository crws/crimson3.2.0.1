
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EMROC2_HPP
	
#define	INCLUDE_EMROC2_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "emroc.hpp"


//////////////////////////////////////////////////////////////////////////
//
// Table Defs
//

#define T_BIT	182
#define T_BYTE	183
#define T_WORD	184
#define T_LONG	185
#define T_REAL	186
#define T_STR_L	187
#define T_STR_H	194

#define T_BIT_E		1
#define T_BYTE_E	2
#define T_WORD_E	3
#define T_LONG_E	4
#define T_REAL_E	5
#define T_STR_L_E	6
#define T_STR_H_E	146

#define T_BIT_E1	201
#define T_BYTE_E1	202
#define T_WORD_E1	203
#define T_LONG_E1	204
#define T_REAL_E1	205

#define T_BIT_E2	206
#define T_BYTE_E2	207
#define T_WORD_E2	208
#define T_LONG_E2	209
#define T_REAL_E2	210

#define T_BIT_E3	211
#define T_BYTE_E3	212
#define T_WORD_E3	213
#define T_LONG_E3	214
#define T_REAL_E3	215

// Devices

enum {
	devFlo103104 = 1,
	devRoc800    = 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Driver
//

class CEmersonRocMasterDriver2 : public CStdCommsDriver
{
	public:
		// Constructor
		CEmersonRocMasterDriver2(void);

		//Destructor
		~CEmersonRocMasterDriver2(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	IsIO(UINT uPoint);
		UINT	GetPointType(CAddress Addr,  CItem *pConfig);
		UINT	GetLogical(CAddress Addr,  CItem *pConfig);
		UINT	GetParameter(CAddress Addr,  CItem *pConfig);
		BOOL	IsString(UINT uTable);
		UINT	FindPointTypeFromShort(CString Short, CItem *pConfig);
		UINT	FindLogicalFromShort(UINT uPointType, CString Short);
		UINT	FindParameterFromShort(UINT uPoinType, CString Short);
		CString FindPointTypeShort(UINT uPointType);
		CString FindLogicalShort(UINT uPointType, UINT uLogical);
		CString FindParameterShort(UINT uPointType, UINT uParameter);
		BYTE	GetLogicalMask(CItem *pConfig);
		BYTE	GetLogicalCount(CItem *pConfig);
		BYTE	GetLogicalShift(CItem *pConfig);
		BYTE	GetParameterMask(CItem *pConfig);

	protected:
		
		// Implementation
		void AddSpaces(void);
		UINT FindDataType(UINT uPointType, UINT uParameter);
		BOOL SetAddress(CAddress &Addr, UINT uPointType, UINT uLogical, UINT uParameter, CError &Error, CItem *pConfig);
		BOOL HasExtendedPointType(CItem *pConfig);
		BOOL HasExtendedParameter(CItem *pConfig);
		BYTE FindTable(UINT uType, UINT uLogical, CItem *pConfig);
		
		

		
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Constants
//

#define ID_T		200
#define ID_L		201
#define ID_P		202
#define ID_SHORT	300
#define ID_TYPE		301
#define OFF_ROC800	78

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Address Selection Dialog
//

class CEmRocAddrDialog2 : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEmRocAddrDialog2(CEmersonRocMasterDriver2 &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Data
		CEmersonRocMasterDriver2 *	m_pDriver;
		CAddress *			m_pAddr;
		CItem *				m_pConfig;
		BOOL				m_fPart;
						
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Wnd, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadPointType(void);
		void LoadLogical(void);
		void LoadParameter(void);
		UINT GetPointType(void);
		UINT GetLogical(void);
		UINT GetParameter(void);
		void SetShortName(void);
		void SetType(void);
		void SetTLP(void);
		BOOL CheckInit(void);

		
	};
		

#endif

// End of File