
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EURO631_HPP
	
#define	INCLUDE_EURO631_HPP

#define AN addrNamed
#define LL addrLongAsLong
#define	WW addrWordAsWord
#define	YY addrByteAsByte
#define	BB addrBitAsBit

class CEuro631Driver;

//////////////////////////////////////////////////////////////////////////
//
// Euro631 Comms Driver
//

class CEuro631Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuro631Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
