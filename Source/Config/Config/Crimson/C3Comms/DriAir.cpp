
#include "intern.hpp"

#include "driair.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DriAir Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDriAirDeviceOptions, CUIItem);

// Constructor

CDriAirDeviceOptions::CDriAirDeviceOptions(void)
{
	m_Drop = 0;
	m_User = 0;
	}

// UI Management

void CDriAirDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
 	}

// Download Support

BOOL CDriAirDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddWord(WORD(m_User));

	return TRUE;
	}

// Meta Data Creation

void CDriAirDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(User);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dri-Air Comms Driver
//

// Instantiator

ICommsDriver *	Create_DriAirDriver(void)
{
	return New CDriAirDriver;
	}

// Constructor

CDriAirDriver::CDriAirDriver(void)
{
	m_wID		= 0x405E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Dri-Air";
	
	m_DriverName	= "ADC";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Dri-Air ADC";

	m_DevRoot	= "ADC";

	AddSpaces();
	AddUSRSpaces(TRUE);
	}

// Binding Control

UINT  CDriAirDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void  CDriAirDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CDriAirDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDriAirDeviceOptions);
	}

// Address Management

BOOL CDriAirDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	SetUserAccess(pConfig->GetDataAccess("User")->ReadInteger(pConfig));

	CStdAddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

void CDriAirDriver::SetUserAccess(UINT uUSRAccess)
{
	DeleteAllSpaces();

	AddSpaces();

	AddUSRSpaces(uUSRAccess == USRALLOW);
	}

// Implementation

void  CDriAirDriver::AddSpaces(void)
{
	// Network enabled functions
	AddSpace(New CSpace(STAT, "STAT",	"Status Values",			10,      0,   79, LL)); //
	AddSpace(New CSpace(STOP, "STOP",	"Stop System",				10,      0,    0, YY)); //=value
	AddSpace(New CSpace(STRT, "STRT",	"Start system",				10,      0,    0, YY)); //
	AddSpace(New CSpace(CEQ,  "CEQ",	"Erases event queue",			10,      0,    0, YY)); //
	AddSpace(New CSpace(DEW,  "DEW",	"Displays dewpoint Data",		10,      0,    3, LL)); //
	AddSpace(New CSpace(ION,  "ION",	"Turns on Current testing",		10,      0,    0, YY)); //
	AddSpace(New CSpace(IOFF, "IOFF",	"Turns off Current testing",		10,      0,    0, YY)); //
	AddSpace(New CSpace(LRN,  "LRN",	"Learn Currents Process",		10,      0,    0, YY)); //
	AddSpace(New CSpace(LOFF, "LOFF",	"Turns Loop Break Alarm off",		10,      0,    0, YY)); //
	AddSpace(New CSpace(LON,  "LON",	"Turns Loop Break Alarm on",		10,      0,    0, YY)); //
	AddSpace(New CSpace(MTR,  "MTR",	"Select Motor",				10,      0,    0, YY)); //
	AddSpace(New CSpace(VLV,  "VLV",	"Select Air Valve",			10,      0,    0, YY)); //
	AddSpace(New CSpace(PD1,  "P2OFF",	"Select 1 Hopper System",		10,      0,    0, YY)); //
	AddSpace(New CSpace(PD2,  "P2ON",	"Select 2 Hopper System",		10,      0,    0, YY)); //
	AddSpace(New CSpace(PT1,  "P1SA",	"Process Temp Hopper 1, SP/Actual",	10,      0,    1, LL)); //=value
	AddSpace(New CSpace(PT2,  "P2SA",	"Process Temp Hopper 2, SP/Actual",	10,      0,    1, LL)); //=value
	AddSpace(New CSpace(RST,  "RST",	"Resets System",			10,      0,    0, YY)); //
	AddSpace(New CSpace(LDOFF,"LDOFF",	"Loader Off",				10,      0,    0, YY)); //
	AddSpace(New CSpace(LDON, "LDON",	"Loader On",				10,      0,    0, YY)); //
	AddSpace(New CSpace(LDT,  "LDTIM",	"Loader On Time",			10,      0,    0, YY)); //
	AddSpace(New CSpace(LDD,  "LDDWL",	"Loader Dwell Time",			10,      0,    0, YY)); //
	AddSpace(New CSpace(S1OFF,"S1OFF",	"Hopper 1 Setback Off",			10,      0,    0, YY)); //
	AddSpace(New CSpace(S1ON, "S1ON",	"Hopper 1 Setback On",			10,      0,    0, YY)); //
	AddSpace(New CSpace(S2OFF,"S2OFF",	"Hopper 2 Setback Off",			10,      0,    0, YY)); //
	AddSpace(New CSpace(S2ON, "S2ON",	"Hopper 2 Setback On",			10,      0,    0, YY)); //
	AddSpace(New CSpace(S1D,  "S1D",	"Hopper 1 Setback Delta",		10,      0,    0, YY)); //
	AddSpace(New CSpace(S2D,  "S2D",	"Hopper 2 Setback Delta",		10,      0,    0, YY)); //
	AddSpace(New CSpace(S1A,  "S1A",	"Hopper 1 Setback Activate Delta T",	10,      0,    0, YY)); //
	AddSpace(New CSpace(S2A,  "S2A",	"Hopper 2 Setback Activate Delta T",	10,      0,    0, YY)); //
	AddSpace(New CSpace(S1I,  "S1I",	"Hopper 1 Setback Inhibit Time",	10,      0,    0, YY)); //
	AddSpace(New CSpace(S2I,  "S2I",	"Hopper 2 Setback Inhibit Time",	10,      0,    0, YY)); //
	AddSpace(New CSpace(ADDR, "ADDR",	"Select Network Address",		10,      0,    0, YY)); //
/*	AddSpace(New CSpace(DIAG, "DIAG",	"*Diagnostic Commands Follow",		10,	 0,    0, BB)); // 
	// Original Functions
	AddSpace(New CSpace(CTRP, "CTRP",	"*Regen Cooling trip point",		10,      0,    0, LL)); //=value
	AddSpace(New CSpace(DBND, "DBND",	"*Derivative Boundary Enable Data",	10,      0,  127, LL)); //=value range * 40 /9 = ##
	AddSpace(New CSpace(DBNM, "DBNM",	"*Derivative Boundary Enable Name",	10,      0,  127, LL)); //=value
	AddSpace(New CSpace(DBN1D,"D1BND",	"*Derivative Boundary Enable 1 Data",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(DBN1M,"D1BNM",	"*Derivative Boundary Enable 1 Name",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(DBN2D,"D2BND",	"*Derivative Boundary Enable 2 Data",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(DBN2D,"D2BNM",	"*Derivative Boundary Enable 2 Name",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(DPCD, "DPCD",	"*Process PID D term Data",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(DPCM, "DPCM",	"*Process PID D term Name",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(D1PCD,"D1PCD",	"*Process PID D term 1 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(D1PCM,"D1PCM",	"*Process PID D term 1 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(D2PCD,"D2PCD",	"*Process PID D term 2 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(D2PCM,"D2PCM",	"*Process PID D term 2 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(DTRP, "DTRP",	"*Regen Drying trip point",		10,      0,    0, LL)); //=value
	AddSpace(New CSpace(DBOTD,"DBOTD",	"*Regen Bottom PID D Data",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(DBOTM,"DBOTM",	"*Regen Bottom PID D Name",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(DTOPD,"DTOPD",	"*Regen Top PID D Data",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(DTOPM,"DTOPM",	"*Regen Top PID D Name",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(EOFF, "EOFF",	"*Turns Ethernet support off",		10,      0,    0, BB)); //
	AddSpace(New CSpace(EON,  "EON",	"*Turns Ethernet support on",		10,      0,    0, BB)); //
	AddSpace(New CSpace(FLGS, "FLGS",	"*User1+User2+Dynamic Flags",		10,      0,    2, LL)); //
	AddSpace(New CSpace(EXHI, "EXHI",	"*Heat Exchanger Hi Limit ",		10,      0,    0, LL)); //=value
	AddSpace(New CSpace(EXLO, "EXLO",	"*Heat Exchanger Low Limit ",		10,      0,    0, LL)); //=value
	AddSpace(New CSpace(HDFF, "HDFF",	"*Heater Current Alarm Diff.",		10,      0,    0, LL)); //=value
	AddSpace(New CSpace(IBNDD,"IBND",	"*Integral Boundary Enable Data",	10,      0,  127, LL)); //=range * 40 /9 = ##
	AddSpace(New CSpace(IBNDM,"IBND",	"*Integral Boundary Enable Name",	10,      0,  127, LL)); //=range * 40 /9 = ##
	AddSpace(New CSpace(I1BND,"I1BND",	"*Integral Boundary Enable 1 Data",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(I1BNM,"I1BNM",	"*Integral Boundary Enable 1 Name",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(I2BND,"I2BND",	"*Integral Boundary Enable 2 Data",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(I2BNM,"I2BNM",	"*Integral Boundary Enable 2 Name",	10,      0,  127, LL)); //=value(1 or 2) range * 40 /9 = ##
	AddSpace(New CSpace(INIT, "INIT",	"*Loads EEPROM defaults",		10,      0,    0, BB)); //
	AddSpace(New CSpace(INPT, "INPT",	"*Switch Inputs",			10,      0,    0, LL)); //
	AddSpace(New CSpace(IPCD, "IPRCD",	"*Process PID I term Data",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(IPCM, "IPRCM",	"*Process PID I term Name",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(I1PCD,"IPRCD",	"*Process PID I term 1 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(I1PCM,"IPRCM",	"*Process PID I term 1 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(I2PCD,"IPRCD",	"*Process PID I term 2 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(I2PCM,"IPRCM",	"*Process PID I term 2 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(IBOTD,"IBOTD",	"*Regen Bottom PID I Data",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(IBOTM,"IBOTM",	"*Regen Bottom PID I Name",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(ITOPD,"ITOPD",	"*Regen Top PID I term",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(ITOPM,"ITOPM",	"*Regen Top PID I term",		10,      0,  127, LL)); //=value
	AddSpace(New CSpace(OON,  "OON",	"*Turns on output (1-16)",		10,      1,   16, LL)); //
	AddSpace(New CSpace(OOFF, "OOFF",	"*Turns off output (1-16)",		10,      1,   16, LL)); //
	AddSpace(New CSpace(PBND, "PBND",	"*Prop. Boundary disable",		10,      0,    0, LL)); //=value range * 40 /9 = ##
	AddSpace(New CSpace(PIDD, "PIDD",	"*Process/Regen PID value",		10,      0,    0, LL)); //
	AddSpace(New CSpace(PIDM, "PIDM",	"*Process/Regen PID Name",		10,      0,    0, LL)); //
	AddSpace(New CSpace(PPRD, "PPRD",	"*Process PID P term Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(PPRM, "PPRM",	"*Process PID P term Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(P1PRD,"P1PRD",	"*Process PID P term 1 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(P1PRM,"P1PRM",	"*Process PID P term 1 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(P2PRD,"P2PRD",	"*Process PID P term 2 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(P2PRM,"P2PRM",	"*Process PID P term 2 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(PHT,  "PHT",	"*Preheat time in seconds",		10,      0,    0, LL)); //=value (1-255)
	//	AddSpace(New CSpace(PXB1, "P1XB",	"*Process PID Control Bits 1",	10,      0,   19, LL)); // //B=128,P=64,I=32,D=16,DE/DT=8,ERR CHK=4,OUT LIMIT=2,SCALE=1=value(1 or 2)
	//	AddSpace(New CSpace(PXB2, "P2XB",	"*Process PID Control Bits 2",	10,      0,   19, LL)); // //B=128,P=64,I=32,D=16,DE/DT=8,ERR CHK=4,OUT LIMIT=2,SCALE=1=value(1 or 2)
	AddSpace(New CSpace(BIAS1,"BIAS1",	"*Process PID B term 1",		10,      0,   19, LL)); //=value(1 or 2)
	AddSpace(New CSpace(BIAS2,"BIAS2",	"*Process PID B term 2",		10,      0,   19, LL)); //=value(1 or 2)
	AddSpace(New CSpace(PBOTD,"PBOTD",	"*Regen Bottom PID P term Data",	10,      0,    0, LL)); //=value
	AddSpace(New CSpace(PBOTM,"PBOTM",	"*Regen Bottom PID P term Name",	10,      0,    0, LL)); //=value
	AddSpace(New CSpace(PTOPD,"PTOPD",	"*Regen Top PID P term Data",		10,      0,    0, LL)); //=value
	AddSpace(New CSpace(PTOPM,"PTOPM",	"*Regen Top PID P term Name",		10,      0,    0, LL)); //=value
	AddSpace(New CSpace(RGEN, "RGEN",	"*Regen String",			10,      0,   79, LL)); //=value
	AddSpace(New CSpace(SETP, "SETP",	"*Regen Setpoint",			10,      0,    0, LL)); //=value
	AddSpace(New CSpace(USRF1,"USR1F",	"*Writes User 1 Flags",			10,      0,   19, LL)); //=value
	AddSpace(New CSpace(USRF2,"USR2F",	"*Writes User 1 Flags",			10,      0,   19, LL)); //=value
	AddSpace(New CSpace(SPRCD,"SPRCD",	"*Process PID S term Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(SPRCM,"SPRCM",	"*Process PID S term Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(S1PRD,"S1PRD",	"*Process PID S term 1 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(S1PRM,"S2PRM",	"*Process PID S term 1 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(S2PRD,"S1PRD",	"*Process PID S term 2 Data",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(S2PRM,"S2PRM",	"*Process PID S term 2 Name",		10,      0,  127, LL)); //=value(1 or 2)
	AddSpace(New CSpace(SBTD, "SBTD",	"*Regen Bottom PID Scale Fact Data",	10,      0,  127, LL)); //=value
	AddSpace(New CSpace(SBTM, "SBTM",	"*Regen Bottom PID Scale Fact Name",	10,      0,  127, LL)); //=value
	AddSpace(New CSpace(STPD, "STPD",	"*Regen Top PID Scale Fact Data",	10,      0,  127, LL)); //=value
	AddSpace(New CSpace(STPM, "STPM",	"*Regen Top PID Scale Fact Name",	10,      0,  127, LL)); //=value
	AddSpace(New CSpace(TEMPM,"TEMPM",	"*Show process & Regen Names",		10,      0,   49, LL)); //
	AddSpace(New CSpace(TON,  "TON",	"*Thermocouple error check On",		10,      0,    0, LL)); //=ALL
	AddSpace(New CSpace(TOFF, "TOFF",	"*Thermocouple error check Off",	10,      0,    0, LL)); //=ALL
	AddSpace(New CSpace(EVRQ, "EVRQ",	"*Read Events n -> n+9",		10,      0,    0, YY));
	AddSpace(New CSpace(EVTA, "EVTA",	"*  Events Response String n",		10,      0,   79, LL));
	AddSpace(New CSpace(EVTB, "EVTB",	"*  Events Response String n+1",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTC, "EVTC",	"*  Events Response String n+2",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTD, "EVTD",	"*  Events Response String n+3",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTE, "EVTE",	"*  Events Response String n+4",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTF, "EVTF",	"*  Events Response String n+5",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTG, "EVTG",	"*  Events Response String n+6",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTH, "EVTH",	"*  Events Response String n+7",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTI, "EVTI",	"*  Events Response String n+8",	10,      0,   79, LL));
	AddSpace(New CSpace(EVTJ, "EVTJ",	"*  Events Response String n+9",	10,      0,   79, LL));
	AddSpace(New CSpace(STRQ, "STRQ",	"*Read Status items n -> n+9",		10,      0,    0, YY));
	AddSpace(New CSpace(STSA, "STSA",	"*  Status Response String n",		10,      0,   79, LL));
	AddSpace(New CSpace(STSB, "STSB",	"*  Status Response String n+1",	10,      0,   79, LL));
	AddSpace(New CSpace(STSC, "STSC",	"*  Status Response String n+2",	10,      0,   79, LL));
	AddSpace(New CSpace(STSD, "STSD",	"*  Status Response String n+3",	10,      0,   79, LL));
	AddSpace(New CSpace(STSE, "STSE",	"*  Status Response String n+4",	10,      0,   79, LL));
	AddSpace(New CSpace(STSF, "STSF",	"*  Status Response String n+5",	10,      0,   79, LL));
	AddSpace(New CSpace(STSG, "STSG",	"*  Status Response String n+6",	10,      0,   79, LL));
	AddSpace(New CSpace(STSH, "STSH",	"*  Status Response String n+7",	10,      0,   79, LL));
	AddSpace(New CSpace(STSI, "STSI",	"*  Status Response String n+8",	10,      0,   79, LL));
	AddSpace(New CSpace(STSJ ,"STSJ",	"*  Status Response String n+9",	10,      0,   79, LL));
*/	}

void CDriAirDriver::AddUSRSpaces(BOOL fUSRAccess)
{
	if( fUSRAccess ) {
		AddSpace(New CSpace(SCMD, "SCMD",	"Send UCMD",			10,	 0,    0, YY));
		AddSpace(New CSpace(UCMD, "UCMD",	"   User Custom Command",	10,      0,   19, LL));
		AddSpace(New CSpace(URSP, "URSP",	"   Response to User Command",	10,      0,   19, LL));
		}
	}

// Helpers

// End of File
