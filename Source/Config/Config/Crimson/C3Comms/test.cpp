
#include "intern.hpp"

#include "test.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTestModeDeviceOptions, CUIItem);       

// Constructor

CTestModeDeviceOptions::CTestModeDeviceOptions(void)
{
	m_Mode = 0;
	}

// Download Support

BOOL CTestModeDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));
	
	return TRUE;
	}

// Meta Data Creation

void CTestModeDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Mode);
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Master Driver
//

// Instantiator

ICommsDriver * Create_TestMasterDriver(void)
{
	return New CTestMasterDriver;
	}

// Constructor

CTestMasterDriver::CTestMasterDriver(void)
{
	m_wID		= 0x40D1;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "<TESTER>";
	
	m_DriverName	= "Test Master Driver";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Test Master";

	AddSpaces();
	}

// Destructor

CTestMasterDriver::~CTestMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CTestMasterDriver::GetBinding(void)
{
	return bindTest;
	}

void CTestMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CTestMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTestModeDeviceOptions);
	}

// Driver Data

UINT CTestMasterDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagRpcMaster | dflagRemotable;
	}

// Implementation

void CTestMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"R",	"Regs",	10, 1, 65535, addrWordAsWord, addrWordAsReal));
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Master TCP Driver
//

// Instantiator

ICommsDriver * Create_TestMasterTCPDriver(void)
{
	return New CTestMasterTCPDriver;
	}

// Constructor

CTestMasterTCPDriver::CTestMasterTCPDriver(void)
{
	m_wID		= 0x40D2;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "<TESTER>";
	
	m_DriverName	= "Test Master TCP Driver";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Test Master";
	}

// Binding Control

UINT CTestMasterTCPDriver::GetBinding(void)
{
	return bindTestIp;
	}

void CTestMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// End of File