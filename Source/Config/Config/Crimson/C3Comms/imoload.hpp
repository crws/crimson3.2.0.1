
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMOLOAD_HPP

#define	INCLUDE_IMOLOAD_HPP

//////////////////////////////////////////////////////////////////////////
//
// IMO Loader Port Driver
//

class CIMOLoaderPortDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIMOLoaderPortDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);

		// Address Checking
		BOOL IsByteAndBit(CSpace * pSpace);
	};

// End of File

#endif
