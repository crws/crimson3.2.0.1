
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMPACT_HPP
	
#define	INCLUDE_IMPACT_HPP

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Camera Driver Options
//

class CImpactCameraDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImpactCameraDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Camera Device Options
//

class CImpactCameraDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImpactCameraDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Camera;
		UINT m_IP;
		UINT m_Port;
		UINT m_Time4;
		UINT m_Scale;
		UINT m_Width;
		UINT m_Height;

		// Persistence
		void Load(CTreeFile &File);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Camera Driver
//

class CImpactCameraDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CImpactCameraDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
