
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#include "drvnamed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Named Comms Driver
//

// Instantiator

ICommsDriver *	Create_TestNamedCommsDriver(void)
{
	return New CTestNamedCommsDriver;
	}

// Constructor

CTestNamedCommsDriver::CTestNamedCommsDriver(void)
{
	m_wID		= 0x0010;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Test Drivers";
	
	m_DriverName	= "Named Comms Driver";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Drive";

	AddSpaces();
	}

// Binding Control

UINT	CTestNamedCommsDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CTestNamedCommsDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void	CTestNamedCommsDriver::AddSpaces(void)
{
	AddSpace( New CNamedSpace('A', "First",		"This is the first Entry") );

	AddSpace( New CNamedSpace('B', "Second",	"This is the second Entry") );

	AddSpace( New CNamedSpace('C', "Third",		"This is the third Entry") );

	AddSpace( New CNamedSpace('D', "Fourth",	"This is another Entry") );
	}

// End of File
