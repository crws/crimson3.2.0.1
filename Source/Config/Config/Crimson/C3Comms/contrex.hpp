
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CONTREX_HPP
	
#define	INCLUDE_CONTREX_HPP

class CContrexRotaryDeviceOptions;
class CContrexTrimDeviceOptions;
class CContrexMRotaryDriver;
class CContrexMTrimDriver;

#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

//////////////////////////////////////////////////////////////////////////
//
// Contrex Device Options
//

class CContrexRotaryDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CContrexRotaryDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Type;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

class CContrexTrimDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CContrexTrimDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Type;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Contrex M-Rotary Comms Driver
//

class CContrexMRotaryDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CContrexMRotaryDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Device Options
		CLASS	GetDeviceConfig(void);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Contrex M-Trim Comms Driver
//

class CContrexMTrimDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CContrexMTrimDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Device Options
		CLASS	GetDeviceConfig(void);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
