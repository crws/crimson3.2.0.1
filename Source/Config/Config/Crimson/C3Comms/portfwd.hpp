
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PORTFWD_HPP
	
#define	INCLUDE_PORTFWD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Port Forwarder Driver Options
//

class CPortForwardDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPortForwardDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Protocol;
		UINT m_TargetIP;
		UINT m_TargetPort;
		UINT m_ExposePort;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Port Forwarder Driver
//

class CPortForwardDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CPortForwardDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Binding Control
		UINT GetBinding (void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
