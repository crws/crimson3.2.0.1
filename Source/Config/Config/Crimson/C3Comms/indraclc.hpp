
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INDRAMATCLC_HPP
	
#define	INCLUDE_INDRAMATCLC_HPP

#define	PARAMETERMAX	4095
#define	SETMAXSTR	"(31)"
#define	SETMAX		  31
#define	BITMAXSTR	"(15)"
#define	BITPREFIX	"IB"

// Data Spaces
#define	SPINT		1
#define	SPREAL		2
#define	SPGINT		3
#define	SPGREAL		4
#define	SPIOW		5
#define	SPIOB		6
#define	SPAXIS		7
#define	SPCARD		8
#define	SPTASK		9
#define	SPSER		10
#define	SPERR		20
#define	SPTWC		21
#define	SPTXW		22
#define	SPTRC		23
#define	SPTXR		24
#define	SPRXD		25

//////////////////////////////////////////////////////////////////////////
//
// Indramat CLC Device Options
//

class CIndramatCLCDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIndramatCLCDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Indramat CLC Driver
//

class CIndramatCLCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIndramatCLCDriver(void);

		// Destructor
		~CIndramatCLCDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	
	protected:

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Indramat CLC Address Selection
//

class CIndramatCLCDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CIndramatCLCDialog(CIndramatCLCDriver &Driver, CAddress &Addr, BOOL fPart);

		// Message Map
		AfxDeclareMessageMap();
		                
	protected:
		// Overridables
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowAddress(CAddress Addr);
		void	ShowDetails(void);
		void	ClearParSet(void);
	};

// End of File

#endif
