
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CDL_HPP
	
#define	INCLUDE_CDL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Driver Types
//

enum
{
	catStandard  = 0,
	catExpansion = 1,
	catTester    = 2,
	catVersion2  = 3,
	catRaw       = 4
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver Options
//

class CCatDataLinkDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCatDataLinkDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Drop;
		UINT	m_ScanMIDs;
		UINT	m_Scan1;
		UINT	m_Scan2;
		UINT	m_Scan3;
		UINT	m_Scan4;
		UINT	m_ScanFrom;
		UINT	m_ScanFrom1;
		UINT	m_ScanFrom2;
		UINT	m_ScanFrom3;
		UINT	m_ScanFrom4;
		UINT	m_ScanTo;
		UINT	m_ScanTo1;
		UINT	m_ScanTo2;
		UINT	m_ScanTo3;
		UINT	m_ScanTo4;
		UINT	m_Delay;
		UINT	m_Delay1;
		UINT	m_Delay2;
		UINT	m_Delay3;
		UINT	m_Delay4;
		UINT	m_Faults;
		UINT	m_Detail;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver Options
//

class CCatDataLinkDriverOptions2 : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCatDataLinkDriverOptions2(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Drop;
		UINT	m_Faults;
		UINT	m_Detail;
		UINT	m_Write;
		UINT    m_StdPend;
		UINT	m_Conn;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Device Options
//

class CCatDataLinkDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCatDataLinkDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link
//

class CCatDataLinkDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CCatDataLinkDriver(UINT uDriver);

		// Driver Data
		UINT GetFlags(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
		BOOL DoListNewFaults(void);
		BOOL DoListPoll(void);
		BOOL DoListReport(void);

	protected:
		// Lookup Maps
		CMap <CString, UINT> m_MapName;
		CMap <UINT,    UINT> m_MapPID;
		CMap <UINT,    UINT> m_MapMonico;

		// Data Members
		UINT m_uDriver;

		// Implementation
		void    BuildMaps(void);
		WORD    FindID(void);
		CString FindShortName(void);
		CString FindFullName(void);
		BOOL    IsWritable(UINT uPID);
		BOOL	IsFault(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class CCatLinkDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CCatLinkDialog(CCatDataLinkDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		CCatDataLinkDriver * m_pDriver;
		CAddress           * m_pAddr;
		CItem	           * m_pConfig;
		BOOL		     m_fPart;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);
		void OnDblClk   (UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadPIDs(void);
		void LoadCategories(void);
		void DoEnables(void);
	};

// End of File

#endif
