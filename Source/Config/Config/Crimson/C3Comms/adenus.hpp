
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ADENUS_HPP
	
#define	INCLUDE_ADENUS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Adenus Telnet Driver
//

class CAdenusDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CAdenusDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
