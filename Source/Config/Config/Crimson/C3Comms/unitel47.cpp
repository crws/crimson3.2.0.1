
#include "intern.hpp"

#include "unitel47.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Master Device Options
//

// This is the config from version 187, which worked with the TSX47, while subsequent code did not.

// Dynamic Class

AfxImplementDynamicClass(CUnitel47MasterDeviceOptions, CUIItem);

// Constructor

CUnitel47MasterDeviceOptions::CUnitel47MasterDeviceOptions(void)
{
	m_Drop     = 6;
	m_Category = 0;
	}

// UI Management

void CUnitel47MasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CUnitel47MasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Drop));
	Init.AddWord(WORD(m_Category));

	return TRUE;
	}

// Meta Data Creation

void CUnitel47MasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Category);
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Master
//
#define MVERSION "1.47"

// Instantiator

ICommsDriver *	Create_Unitelway47MasterDriver(void)
{
	return New CUnitelway47MasterDriver;
	}

// Constructor

CUnitelway47MasterDriver::CUnitelway47MasterDriver(void)
{
	m_wID		= 0x4059;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Schneider - Telemechanique";
	
	m_DriverName	= "Uni-telway TSX47 Master";
	
	m_Version	= MVERSION;
	
	m_ShortName	= "Uni-telway TSX47 Master";

	AddSpaces();
	}

// Binding Control

UINT CUnitelway47MasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CUnitelway47MasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CUnitelway47MasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUnitel47MasterDeviceOptions);
	}

// Implementation

void CUnitelway47MasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(10,"M",   "Internal Bits",		10,  0, 65535,	addrBitAsBit));
	AddSpace(New CSpace(8, "MB",  "Internal Bytes",		10,  0, 65535,	addrByteAsByte));
	AddSpace(New CSpace(1, "MW",  "Internal Words",		10,  0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(4, "MDW", "Internal Double Words",	10,  0, 65535,	addrLongAsLong));
	AddSpace(New CSpace(7, "MF",  "Internal Reals",		10,  0, 65535,	addrRealAsReal));
	AddSpace(New CSpace(11,"S",   "System Bits",		10,  0, 65535,	addrBitAsBit));
	AddSpace(New CSpace(9, "SB",  "System Bytes",		10,  0, 65535,	addrByteAsByte));
	AddSpace(New CSpace(2, "SW",  "System Words",		10,  0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(5, "SDW", "System Double Words",	10,  0, 65535,	addrLongAsLong));
	AddSpace(New CSpace(3, "KW",  "Constant Words",		10,  0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(6, "KDW", "Constant Double Words",	10,  0, 65535,	addrLongAsLong));
	AddSpace(New CSpace(12,"KF",  "Constant Reals",		10,  0, 65535,	addrRealAsReal));
	AddSpace(New CSpace(13,"TP",  "PL7 Timer Preset",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(14,"TV",  "PL7 Timer Value",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(15,"TR",  "PL7 Timer Running",	10,  0, 63,	addrByteAsByte));
	AddSpace(New CSpace(16,"TD",  "PL7 Timer Done",		10,  0, 63,	addrByteAsByte));
	AddSpace(New CSpace(17,"TMP", "IEC Timer Preset",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(18,"TMV", "IEC Timer Value",	10,  0, 63,	addrWordAsWord));
	AddSpace(New CSpace(19,"TMQ", "IEC Timer Output",	10,  0, 63,	addrByteAsByte));
	AddSpace(New CSpace(20,"CP",  "Counter Preset",		10,  0, 31,	addrWordAsWord));
	AddSpace(New CSpace(21,"CV",  "Counter Value",		10,  0, 31,	addrWordAsWord));
	AddSpace(New CSpace(22,"CE",  "Counter Empty",		10,  0, 31,	addrByteAsByte));
	AddSpace(New CSpace(23,"CD",  "Counter Done",		10,  0, 31,	addrByteAsByte));
	AddSpace(New CSpace(24,"CF",  "Counter Full",		10,  0, 31,	addrByteAsByte));
	AddSpace(New CSpace(1, "W",   "Do Not Use-Conversion Tool",10,  0, 65535,addrWordAsWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Application Master / Network Slave
/*/
// The slave was not implemented for the TSX47

// Instantiator

ICommsDriver *	Create_Unitelway47MSDriver(void)
{
	return New CUnitelway47MSDriver;
	}

// Constructor

CUnitelway47MSDriver::CUnitelway47MSDriver(void)
{
	m_wID		= 0x405A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Schneider - Telemechanique";
	
	m_DriverName	= "Uni-telway TSX47 Slave";
	
	m_Version	= MVERSION;
	
	m_ShortName	= "Uni-telway TSX47 Slave";

	AddSpaces();
	}

// Configuration

CLASS CUnitelway47MSDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CUnitel47MasterDeviceOptions);
	}
*/

// End of File
