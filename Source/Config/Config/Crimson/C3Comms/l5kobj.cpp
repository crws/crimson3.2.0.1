
#include "intern.hpp"

#include "l5kdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item Description
//

// Constructor

CABL5kDesc::CABL5kDesc(void)
{
	m_fAlias = FALSE;
	}

CABL5kDesc::CABL5kDesc(CString Name, CString Type)
{
	m_fAlias = FALSE;
	
	m_Name    = Name;
	
	m_Type    = Type;
	}

// Attribute

BOOL CABL5kDesc::IsAlias(void)
{
	return m_fAlias;
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Alias Tags
//

// Constructor

CABL5kAlias::CABL5kAlias(CString Name, CString Type) : CABL5kDesc(Name, Type)
{
	m_fAlias = TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item with a list
//

// Constructor

CABL5kList::CABL5kList(void)
{
	}

CABL5kList::CABL5kList(CString Name)
{
	m_Name = Name;
	}

void CABL5kList::Append(CABL5kDesc &Entry)
{
	m_List.Append(Entry);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Controller
//

// Constructor

CABL5kController::CABL5kController(void)
{
	}

// Operations

void CABL5kController::AddUDT(CABL5kList UDT)
{
	m_UDTs.Append(UDT);
	}

void CABL5kController::AddTag(CABL5kDesc Tag)
{
	m_Tags.Append(Tag);
	}

void CABL5kController::AddProgram(CABL5kList Code)
{
	m_Programs.Append(Code);
	}

void CABL5kController::AddAOI(CABL5kList Code)
{
	m_AOIs.Append(Code);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kItem, CMetaItem);

// Constructor

CABL5kItem::CABL5kItem(void)
{
	m_fStruct = FALSE;
	}

CABL5kItem::CABL5kItem(CString Name, CString Type)
{
	m_fStruct = FALSE;

	m_Name    = Name;
	
	m_Type    = Type;
	}

CABL5kItem::CABL5kItem(CString Name, CString Type, CString Info)
{
	m_fStruct = FALSE;

	m_Name    = Name;
	
	m_Type    = Type;

	m_Info    = Info;
	}

BOOL CABL5kItem::IsStruct(void)
{
	return m_fStruct;
	}

// Meta Data Creation

void CABL5kItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString(Name);
	Meta_AddString(Type);
	Meta_AddString(Info);
	}

// Implementation

void CABL5kItem::ImportFixName(void)
{
	CItem *pItem = this->GetParent();

	while( pItem ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CABL5kNames)) ) {

			break;
			}

		if( pItem->IsKindOf(AfxRuntimeClass(CABL5kStruct)) ) {

			CString Root = m_Name.Left(m_Name.Find('.'));

			if( Root.Find('[') == NOTHING ) {

				CString Text;
				
				Text += ((CABL5kStruct * ) pItem)->m_Name;
				
				Text += L".";

				m_Name.Insert(0, Text);
				}
			}

		pItem = pItem->GetParent();
		}

	AfxAssert(pItem);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Structure Item
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kStruct, CABL5kItem);

// Constructor

CABL5kStruct::CABL5kStruct(void)
{
	m_fStruct  = TRUE;

	m_pMembers = New CABL5kItemList;
	}

// Destructor

CABL5kStruct::~CABL5kStruct(void)
{
	}

// Meta Data Creation

void CABL5kStruct::AddMetaData(void)
{
	CABL5kItem::AddMetaData();

	Meta_AddCollect(Members);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item List
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kItemList, CItemList);

// Constructor

CABL5kItemList::CABL5kItemList(void)
{
	m_Class = NULL;
	}

// Item Location

CABL5kItem * CABL5kItemList::GetItem(UINT uPos) const
{
	return (CABL5kItem *) CItemList::GetItem(uPos);
	}

// End of File
