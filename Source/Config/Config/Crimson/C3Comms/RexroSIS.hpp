
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_REXROTHSIS_HPP

#define	INCLUDE_REXROTHSIS_HPP

#define MODELECO	0
#define MODELSYNAX	1

#define SPCLIST 10
#define SPCERR	11

//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS Device Options
//

class CRexrothSISDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRexrothSISDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_SDrop;
		UINT m_RDrop;
//		UINT m_Model;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS Driver
//

#define	AN	addrNamed

class CRexrothSISDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CRexrothSISDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers


	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS Address Selection
//

class CRexrothSISAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CRexrothSISAddrDialog(CRexrothSISDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		UINT	m_uModel;

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowDetails(void);

		// Helpers
		void	ClearAllWindowText(void);
	};


// End of File

#endif
