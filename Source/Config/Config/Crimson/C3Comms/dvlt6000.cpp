
#include "intern.hpp"

#include "dvlt6000.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Danfoss 6000 VLT Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDVLT6000DeviceOptions, CUIItem);
				   
// Constructor

CDVLT6000DeviceOptions::CDVLT6000DeviceOptions(void)
{
	m_Drop = 1;

	m_Ping = 1;
	}

// UI Managament

void CDVLT6000DeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CDVLT6000DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddWord(WORD(m_Ping));
	
	return TRUE;
	}

// Meta Data Creation

void CDVLT6000DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Ping);
	}

//////////////////////////////////////////////////////////////////////////
//
// Danfoss VLT 6000 Driver
//

// Instantiator

ICommsDriver *	Create_DVLT6000Driver(void)
{
	return New CDVLT6000Driver;
	}

// Constructor

CDVLT6000Driver::CDVLT6000Driver(void)
{
	m_wID		= 0x3372;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Danfoss";
			
	m_DriverName	= "VLT / FC Series";
	
	m_Version	= "1.14";
	
	m_ShortName	= "VLT/FC";	

	AddSpaces();

	}

// Binding Control

UINT CDVLT6000Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CDVLT6000Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CDVLT6000Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDVLT6000DeviceOptions);
	}

// Address Management

BOOL CDVLT6000Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CDanfossAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Implementation     

void CDVLT6000Driver::AddSpaces(void)
{
	AddSpace(New CSpaceDanVLT(1, "P", "Parameter Registers", 10, 0, 4095, addrLongAsLong));

	AddSpace(New CSpace(addrNamed, "CTRL", "Control Word (Write Only)",	10, 2, 2, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "STAT", "Status Word (Read Only)",	10, 3, 3, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "BUS",  "Bus Reference (Write Only)",    10, 4, 4, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "OUT",  "Output Frequency (Read Only)",  10, 5, 5, addrWordAsWord));
	
	}

// Address Helpers

BOOL CDVLT6000Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		UINT uFind = Text.Find(':');

		UINT uIndex = 0;

		if( uFind < NOTHING ) {

			uIndex = tstrtol(Text.Mid(uFind + 1), NULL, 10);

			if( uIndex > 63 ) {

				Error.Set(CPrintf(CString(IDS_DRIVER_ADDRRANGE), TEXT("0"), TEXT("63")), 0);

				return FALSE;
				}

			Text = Text.Mid(0, uFind);
			}

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			Addr.a.m_Table  |= addrNamed;

			Addr.a.m_Extra   = uIndex & 0x0F;

			Addr.a.m_Offset |= ((uIndex & 0x30) << 10);

			return TRUE;
			}
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CDVLT6000Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpaceDanVLT *pSpace = (CSpaceDanVLT *)GetSpace(Addr);

	if( pSpace ) {

		UINT uIndex = ((Addr.a.m_Offset >> 10) & 0x30) | Addr.a.m_Extra;
		
		if( uIndex ) {

			UINT uOffset = Addr.a.m_Offset & 0x0FFF;

			Text.Printf("%s%4.4u:%2.2u", pSpace->m_Prefix, uOffset, uIndex);

			return TRUE;
			}
		}

	return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
	}

//////////////////////////////////////////////////////////////////////////
//
// Danfoss VLT Space Wrapper
//

// Constructor

CSpaceDanVLT::CSpaceDanVLT(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t) : CSpace(uTable, p, c, r, n, x, t)
{

	}

// Matching

BOOL CSpaceDanVLT::MatchSpace(CAddress const &Addr)
{
	return m_uTable == (Addr.a.m_Table & 0x0F);
	}


//////////////////////////////////////////////////////////////////////////
//
// Danfoss Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CDanfossAddrDialog, CStdAddrDialog);
		
// Constructor

CDanfossAddrDialog::CDanfossAddrDialog(CDVLT6000Driver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element     = "DanfossElementDlg";
	}

// Overridables

void CDanfossAddrDialog::SetAddressText(CString Text)
{
	CEditCtrl &Param = (CEditCtrl &) GetDlgItem(2002);

	CEditCtrl &Index = (CEditCtrl &) GetDlgItem(2004);

	BOOL fEnable = !Text.IsEmpty();

	Param.EnableWindow(fEnable);

	UINT uFind = Text.Find(':');

	Index.EnableWindow(fEnable);

	GetDlgItem(3001).EnableWindow(fEnable);

	if( uFind < NOTHING ) {

		Param.SetWindowText(Text.Mid(0, uFind));

		Index.SetWindowText(Text.Mid(uFind + 1));

		return;
		}

	Param.SetWindowText(Text);

	Index.SetWindowText(fEnable ? "00" : " ");
	}

CString CDanfossAddrDialog::GetAddressText(void)
{
	CEditCtrl &Param = (CEditCtrl &) GetDlgItem(2002);

	CEditCtrl &Index = (CEditCtrl &) GetDlgItem(2004);

	CString Text = Param.GetWindowText();

	if( !Index.GetWindowText().IsEmpty() ) {

		Text += ":";

		Text += Index.GetWindowText();
		}

	return Text;
	}
 
// End of File
