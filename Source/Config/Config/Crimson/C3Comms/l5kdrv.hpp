
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_L5KDRV_HPP
	
#define	INCLUDE_L5KDRV_HPP

#include "l5kmap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// References
//

#define	IDM_COMMS_BUILD_BLOCK	0xC11A

//////////////////////////////////////////////////////////////////////////
//
// Address Decoding Macros
//

#define IsTable(Addr)	((Addr).a.m_Table  < addrNamed)

#define IsNamed(Addr)	((Addr).a.m_Table == addrNamed)

#define GetTable(Addr)	((Addr).a.m_Table)

#define GetOffset(Addr)	((Addr).a.m_Offset)

#define GetIndex(Addr)	(IsTable(Addr) ? GetTable(Addr) : GetOffset(Addr))

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kDesc;
class CABL5kAlias;
class CABL5kList;
class CABL5kController;

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item Description
//

class CABL5kDesc
{
	public:
		// Constructor
		CABL5kDesc(void);
		CABL5kDesc(CString Name, CString Type);

		// Attribute
		BOOL IsAlias(void);

		// Properties
		CString	m_Name;
		CString	m_Type;

	protected:
		// Data Members
		BOOL	m_fAlias;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Alias Tag
//

class CABL5kAlias : public CABL5kDesc
{
	public:
		// Constructor
		CABL5kAlias(CString Name, CString Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item with a list
//

class CABL5kList
{
	public:
		// Constructor
		CABL5kList(void);
		CABL5kList(CString Name);

		// Operations
		void Append(CABL5kDesc &Entry);

		// Properties
		CString			m_Name;
		CArray	<CABL5kDesc>	m_List;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Controller
//

class CABL5kController
{
	public:
		// Constructor
		CABL5kController(void);

		// Operations
		void AddUDT    (CABL5kList UDT);
		void AddTag    (CABL5kDesc Tag);
		void AddProgram(CABL5kList Code);
		void AddAOI    (CABL5kList Code);

		// Properties
		CString	                m_Name;
		CArray	<CABL5kList>	m_UDTs;
		CArray	<CABL5kDesc>	m_Tags;
		CArray	<CABL5kList>	m_Programs;
		CArray	<CABL5kList>	m_AOIs;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kItem;
class CABL5kStruct;
class CABL5kItemList;

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item
//

class CABL5kItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kItem(void);
		CABL5kItem(CString Name, CString Type);
		CABL5kItem(CString Name, CString Type, CString Info);

		// Attribute
		BOOL IsStruct(void);

		// Properties
		CString	m_Name;
		CString	m_Type;
		CString	m_Info;

	protected:
		// Data Members
		BOOL	m_fStruct;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void ImportFixName(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Structure Item
//

class CABL5kStruct : public CABL5kItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kStruct(void);
		
		// Destructor
		~CABL5kStruct(void);

		// Public Data
		CABL5kItemList	* m_pMembers;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Item List
//

class CABL5kItemList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kItemList(void);

		// Item Location
		CABL5kItem * GetItem(UINT uPos) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kTypes;
class CABL5kNames;
class CABL5kNamesList;
class CABL5kDeviceOptions;
class CABL5kDialog;

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Data Types 
//

class CABL5kTypes
{
	public:
		// Constructor
		CABL5kTypes(void);

		// Loading
		void KillUDTs(void);
		void LoadUDTs(CArray <CABL5kList> const &Types);

		// Attributes
		DWORD GetAtomicType(CString const &Name);
		BOOL  IsAtomicType(CString const &Name);

		// Type Expansion
		BOOL ExpandPDT(CABL5kNames *pNames, CABL5kItemList *pList, CString const &Name, CString const &Type);
		BOOL ExpandUDT(CABL5kNames *pNames, CABL5kItemList *pList, CString const &Name, CString const &Type);

		// Debug
		void ShowUDTs(void);

	//protected:
		// Raw Atomic Data Type
		struct CAtomicEntry {
			
			PCUTF	m_pName;
			DWORD	m_Type;
			};

		// Raw Structured Info
		struct CStructInfo {
			
			PCUTF	m_pName;
			PCUTF	m_pType;
			};

		// Raw Predefined Data Type
		struct CStructEntry {
			
			PCUTF		m_pName;
			CStructInfo	m_Elements[244];
			};

		// Raw Atomic Types
		static CAtomicEntry	m_AtomicList[];

		// Raw Predefined Types
		static CStructEntry	m_PredefinedList[];

		// TESTING
		static UINT GetPDTCount(void);

		// User Defined Types
		CArray <CABL5kList>	m_UDTList;

	protected:
		// Data Members
		CABL5kNames           * m_pNames;

		// Implementation
		BOOL ExpandPDT(CABL5kItemList *pTags, CString const &Name, CString const &Type);
		BOOL ExpandUDT(CABL5kItemList *pTags, CString const &Name, CString const &Type);
		BOOL ExpandFromType(CABL5kItemList *pTags, CString const &Name, CABL5kList Type);
		BOOL ExpandFromType(CABL5kItemList *pTags, CString const &Name, CStructEntry Type);
		BOOL ExpandType(CABL5kItemList *pTags, CString const &Root, CString Name, CString Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Tag Names
//

class CABL5kNames : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kNames(void);

		// Destructor
		~CABL5kNames(void);

		// Operations
		void LoadTags(CArray <CABL5kDesc> const &Tags, CArray <CABL5kDesc> &Fail);
		void LoadTags(CArray <CABL5kDesc> const &Tags, CArray <CABL5kDesc> &Fail, CString Scope);
		void LoadTag(CABL5kItemList *pTags, CString const &Name, CString const &Type, CString const &Info);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Public Data
		CString		 m_Scope;
		CABL5kItemList * m_pTags;

	protected:
		// Data Members
		CABL5kDeviceOptions * m_pConfig;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindConfig(void);
		void ResolveAliasTags(CArray <CABL5kDesc> &List, UINT uDepth);
		void PrependScope(CABL5kDesc &Tag);
		void PrependScope(CABL5kItem &Tag);

		// Tag List
		BOOL MakeTag(CABL5kItemList *pTags, CString const &Name, CString Type, CString const &Info);
		
		CABL5kItem * FindTag(CString const &Name);
		CABL5kItem * FindTag(CABL5kItemList const *pTags, CString const &Name);

		// Type Expansion
		BOOL ExpandType(CABL5kItemList *pTags, CString const &Name, CString const &Type);
		BOOL ExpandPDT (CABL5kItemList *pTags, CString const &Name, CString const &Type);
		BOOL ExpandUDT (CABL5kItemList *pTags, CString const &Name, CString const &Type);

		// Debug
		void ShowTags(void);
		void ShowTags(CABL5kItemList *pTags, UINT uDepth);

		// Friend
		friend class CABL5kDialog;
		friend class CABL5kDeviceOptions;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k Tag Names List
//


class CABL5kNamesList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kNamesList(void);

		// Item Location
		CABL5kNames * GetItem(UINT uPos) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

class CABL5kDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kDeviceOptions(void);

		// Destructor
		~CABL5kDeviceOptions(void);

		// Address Management
		void CheckAlignment(CAddress &Addr);
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL DoExpandAddress(CString &Text, CAddress const &Addr);
		BOOL DoSelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart, BOOL fSelect);
		BOOL DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Load(CTreeFile &Tree);
		void PostLoad(void);
		void Save(CTreeFile &Tree);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// L5k Support
		BOOL LoadFromFile(CFilename const &Name);

		// Name Map Support
		void         AddToMap(CABL5kItem *pTag);
		CABL5kItem * FindFromMap(CString Name);
		
		BOOL ParseDims(CArray <UINT> &uDims, CString Text);
		BOOL ExpandDims(CString &Text, CArray <UINT> Dims, UINT uIndex);
		UINT CheckDims(CArray <UINT> List, CArray <UINT> Dims);

	public:
		// Public Data
		UINT			m_Device;
		DWORD			m_Address;
		UINT			m_Routing;
		UINT			m_Port;
		UINT			m_Slot;
		UINT			m_Time;
		UINT			m_Push;
		CString			m_Path;

		CString			m_DeviceName;
		CABL5kNames	      * m_pController;
		CABL5kNamesList	      * m_pPrograms;
		CABL5kTypes	        m_Types;
		
	protected:
		// Object Maps
		typedef CZeroMap <CString, CABL5kItem *> CNameMap;
		typedef CMapper  <CString>               CStringMapper;
		typedef CMapper  <UINT>                  CIndexMapper;

		// Data Members
		CStringMapper		m_NamedList;
		CStringMapper		m_TableList;
		CIndexMapper		m_NamedDict;
		CIndexMapper		m_TableDict;
		CNameMap		m_NameMap;
		UINT			m_uProgram;

		// Meta Data Creation
		void AddMetaData(void);

		// Property Save Filter
		BOOL SaveProp(CString Tag) const;

		// Implementation
		BOOL AddText(CInitData &Init, CString const &Text);
		void AddName(CInitData &Init, CString Name);

		CString GetScope(CString Text);

		void SaveMapping(CTreeFile &Tree, CIndexMapper &Dict);
		void LoadMapping(CTreeFile &Tree, CIndexMapper &Dict);
		void BuildNameMap(void);
		void BuildNameMap(CABL5kNames *pNames);
		void BuildNameMap(CString Scope, CABL5kItemList *pTags);
		void AddToDict(CIndexMapper &Dict, CString const &Name, CStringMapper const &List);

		// List Support
		BOOL ListTags(CAddrData *pRoot, UINT uItem, CAddrData &Data, CABL5kItemList *pTags);

		// Help
		void ShowFailedTags(CArray <CABL5kDesc> const &Fail);

		// Debug
		void ShowTags(void);
		void ShowTags(CABL5kItemList *pTags, UINT uDepth);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver
//

class CABL5kDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CABL5kDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class CABL5kDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CABL5kDialog(CABL5kDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart, BOOL fSelect);

		// Destructor
		~CABL5kDialog(void);

		// Attributes
		BOOL FileImported(void);

	protected:
		// Data
		CABL5kDriver	     * m_pDriver;
		CAddress	     * m_pAddr;
		CABL5kDeviceOptions  * m_pConfig;
		BOOL		       m_fPart;
		CImageList             m_Images;
		BOOL		       m_fSelect;
		BOOL		       m_fCreate;
		BOOL		       m_fLoading;
		BOOL                   m_fImport;
		HTREEITEM              m_hRoot;
		HTREEITEM              m_hSelect;
		CABL5kItem          *  m_pSelect;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnImport(UINT uID);

		// Implementation
		void SetCaption(void);
		void LoadTags(void);
		void LoadRoot(CTreeView &Tree);
		void LoadTags(CTreeView &Tree, CString Root, CABL5kItemList *pTags);
		void LoadTags(CTreeView &Tree, CString Root, CABL5kNamesList *pPrograms);
		void LoadTags(CTreeView &Tree, HTREEITEM hRoot, CABL5kItemList *pTags);
		void LoadTag(CTreeView &Tree, HTREEITEM hRoot, CABL5kItem *pTag);
		void SetAddressText(CString Text);
		void SetAddrTypeText(CString const &Text);
		void SetDataTypeText(CString const &Text);
		void SetMinimumText(CString const &Text);
		void SetMaximumText(CString const &Text);
		void SetDataDescText(CString const &Text);
		
		void    GetVisibleIndices(CArray <UINT> &List);
		CString GetAddressText(void);		
		void    HideTableIndices(void);
		void    ShowTableIndices(UINT uIndex, CArray <UINT> Dims);
		void    EnableOkay(BOOL fEnable);
		void    EnableCancel(BOOL fEnable);
		void    EnableImport(BOOL fEnable);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley L5k File
//

class CABL5kFile
{
	public:
		// Constructor
		CABL5kFile(void);

		// Destructor
		~CABL5kFile(void);

		// Control
		BOOL OpenLoad(ITextStream &Stream);

	public:
		// Public Data
		CABL5kController	m_Logix;

	protected:
		// Data
		ITextStream           * m_pStream;
		TCHAR			m_sLine[8192];
		CString			m_Component;		
		CString			m_Version;
		CString			m_Work;

		// Implementation
		void CloseFile(void);
		void ReadWork(void);
		void ParseFile(void);
		BOOL ParseVersion(void);
		BOOL ParseController(void);
		BOOL ParseDataType(void);
		BOOL ParseModule(void);		
		BOOL ParseAOI(void);
		BOOL ParseParameters(CABL5kList &Params);
		BOOL ReadTag(CString Component);
		BOOL ParseTags(CString Parent, CArray <CABL5kDesc> &Tags);
		BOOL ParseProgram(void);
		BOOL ReadComponent(CString Comp);
		BOOL ParseComponent(CString Comp);
		BOOL ParseRoutine(CString Comp);
		void SkipHeader(void);
		void SkipDescription(void);		
		void StripInPlace(CString &Text, PCUTF c1, PCUTF c2);

		// Debug
		void ShowController(void);
		void ShowDataTypes(CArray <CABL5kList> const &Types);
		void ShowTagNames(CArray <CABL5kDesc> const &Tags);
	};

// End of File

#endif
