
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EMROC_HPP
	
#define	INCLUDE_EMROC_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Driver Options
//

class CEmersonRocMasterDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonRocMasterDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Group;
		UINT m_Unit;
		
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Device Options
//

class CEmersonRocMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonRocMasterDeviceOptions(void);

		// UI Managament
	        void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Group;
		UINT m_Unit;
		UINT m_Model;
		UINT m_Poll;
		UINT m_Acc;
		UINT m_PassNum;
		BOOL m_fUse;
		
		CString m_OpID;
		CString m_Pass;

		BOOL m_fChange;
		
		
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Space Wrapper Class
//

class CSpaceROC : public CSpace
{
	public:
		// Constructors
		CSpaceROC(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT cd = 0, UINT cn = 0, UINT cx = 0);

		// Public Data
		UINT m_Collect;
		UINT m_ColMin;
		UINT m_ColMax;
		BOOL m_fCol;

	protected:

		// Matching
		BOOL MatchSpace(CAddress const &Addr);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);

	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Driver
//

class CEmersonRocMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEmersonRocMasterDriver(void);

		//Destructor
		~CEmersonRocMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// String Access
		UINT GetOperand(UINT uTable);


	protected:
		// Implementation
		void AddSpaces(void);

		// Helpers
		BOOL IsString(UINT uTable);
		UINT GetStringLen(UINT uTable);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Address Selection Dialog
//

class CEmRocAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEmRocAddrDialog(CEmersonRocMasterDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
	
		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};


#endif

// End of File