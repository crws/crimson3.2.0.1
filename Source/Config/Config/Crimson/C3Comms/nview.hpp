
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_NVIEWMASTER_HPP
	
#define	INCLUDE_NVIEWMASTER_HPP

//////////////////////////////////////////////////////////////////////////
//
// N-View Master Driver
//

class CNViewMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CNViewMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
	
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// N-View Master Driver Options
//

class CNViewMasterDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CNViewMasterDriverOptions(void);

		// Public Data
		UINT m_Empty;
	
	protected:
		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// N-View Master Device Options
//

class CNViewMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CNViewMasterDeviceOptions(void);

		// Public Data
		UINT    m_Mode;
		UINT    m_IPAddr;
		UINT    m_MACBlock;
		UINT    m_MAC4;
		UINT    m_MAC5;
		UINT    m_MAC6;
		CString m_Name;
		UINT    m_Time;
	
	protected:
		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
