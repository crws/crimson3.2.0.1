#include "s7tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_S7MPI_HPP
	
#define	INCLUDE_S7MPI_HPP

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Driver Options
//

class CS7MPIDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS7MPIDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_LastDrop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Device Options
//

class CS7MPIDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS7MPIDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_MPIDrop;
		UINT m_Drop;
		UINT m_Timeout;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Space Wrapper
//

class CSpaceS7 : public CSpace
{
	public:
		// Constructors
		CSpaceS7(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Driver
//

class CS7MPIDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CS7MPIDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL CheckAlignment(CSpace *pSpace);
	
       protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Address Selection
//

class CS7MPIDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CS7MPIDialog(CS7MPIDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 PLC TCP/IP Device Options
//

class CS7MPITCPDeviceOptions : public CS7IsoTCPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS7MPITCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
	}; 

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 PLC PLC TCP/IP Master
//

class CS7MPITCPDriver : public CS7MPIDriver
{
	public:
		// Constructor
		CS7MPITCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Option Card - Driver Options
//

class CS7DirectDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS7DirectDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_BaudRate;
		UINT m_ThisDrop;
		UINT m_LastDrop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Option Card - Device Options
//

class CS7DirectDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS7DirectDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_ThatDrop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Option Card
//

class CS7DirectDriver : public CS7MPIDriver
{
	public:
		// Constructor
		CS7DirectDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 Extended Addressing via MPI Space Wrapper
//

class CSpaceS7Ext : public CSpace
{
	public:
		// Constructors
		CSpaceS7Ext(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 PLC Extended DB TCP/IP Device Options
//

class CS7MPIExtTCPDeviceOptions : public CS7MPITCPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS7MPIExtTCPDeviceOptions(void);
	}; 

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 Extended Addressing via MPI Driver
//

class CS7MpiExtTcpDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CS7MpiExtTcpDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL CheckAlignment(CSpace *pSpace);
		BOOL IsDB(UINT uTable);


       protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Address Selection
//

class CS7MPIXDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CS7MPIXDialog(CS7MpiExtTcpDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

// End of File

#endif
