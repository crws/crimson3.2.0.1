
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "honeyudc.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "honeyudc.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "honeyudc.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "honeyudc.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CHoneywellUDC9000DeviceOptions
//

CHoneywellUDC9000DeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the device."
	"\0"

	"Socket,TCP Port,,CUIEditInteger,|0||1|9999,"
	"Indicate the TCP port number on which the protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CHoneywellUDC9000DeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Socket\0"
	"G:1,root,Protocol Options,Keep,Time1,Time3,Time2\0"
	"\0"
END	

//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC Element Dialog
//
//

HoneywellUDCElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT		2002,	 0, 0,  24, 12
	CTEXT		"(",	2004,	26, 1,   8, 12
	EDITTEXT		2005,	36, 0,  20, 12
	CTEXT		")",	2006,	60, 1,   8, 12
	LTEXT	"Block",	2007,	 4, 14, 24, 12
	LTEXT	"Parameter",	2008,	32, 14, 30, 12
END


// End of File
