
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_JULABOSERIAL_HPP

#define	INCLUDE_JULABOSERIAL_HPP

#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	USRALLOW	16488 // (0x4068)

enum {
	JSPT	=  1,
	JPAR	=  2,
	JPV	=  3,
	JHIL	=  4,
	JMODE	=  5,
	JSTAT	=  6,
	JSTSTR	=  7,
	JVERS	=  8,
	JUSRS	= 15,
	JUSRC	= 16,
	JUSRR	= 17
	};

/////////////////////////////////////////////////////////////////////////
//
//  Julabo Device Options
//

class CJulaboDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CJulaboDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
		UINT m_User;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Julabo Serial Driver
//

class CJulaboSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CJulaboSerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		void	SetUserAccess(UINT uUSRAccess);

	protected:
		// Data
		UINT	m_uUSRAccess;

		// Implementation
		void	AddSpaces(void);
		void	AddUSRSpaces(BOOL fOk);

		// Helpers
	};

//////////////////////////////////////////////////////////////////////////
//
// Julabo Serial Address Selection
//

class CJulaboSerialDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CJulaboSerialDialog(CJulaboSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		CItem	*m_pCfg;
		BOOL	m_fUpdateInfo;

		CJulaboSerialDriver * m_pDriverData;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		BOOL	OnButtonClicked(UINT uID);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overrideables
		void	ShowAddress(CAddress Addr);

		// Information
		void	GetInfo(UINT uT, UINT uO, UINT uY);

		// Helpers
		BOOL	IsStringItem(UINT uT);
	};

// End of File

#endif


