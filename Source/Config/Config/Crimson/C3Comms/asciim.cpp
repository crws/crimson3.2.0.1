
#include "intern.hpp"

#include "asciim.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ASCII Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CASCIIMDeviceOptions, CUIItem);

// Constructor

CASCIIMDeviceOptions::CASCIIMDeviceOptions(void)
{
	m_RFrame	= "";
	m_WFrame	= "";
	m_RspTime	= 1000;
	m_RspTerm	= 13;
	m_RspLen	=  0;
	m_NakInd	= "";
	m_Radix		=  0;
	m_fRspEcho	= FALSE;
	m_EchoCmd	=  0;
	m_EchoRsp	=  0;
	m_RspStart	=  0;
	m_RspSkip	=  0;
	m_fRspBack	= FALSE;
	m_fRspWrite	= TRUE;

	SetPages(2);
	}

// UI Management

void CASCIIMDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "RspEcho" ) {

		pWnd->EnableUI("EchoCmd", m_fRspEcho);
		pWnd->EnableUI("EchoRsp", m_fRspEcho);
	      	}

	if( Tag.IsEmpty() || Tag == "RFrame" ) {

		if( m_RFrame.GetLength() > 31 ) m_RFrame = m_RFrame.Left(31);

		pWnd->UpdateUI(m_RFrame);
		}

	if( Tag.IsEmpty() || Tag == "WFrame" ) {

		if( m_WFrame.GetLength() > 31 ) m_WFrame = m_WFrame.Left(31);

		pWnd->UpdateUI(m_WFrame);
		}

	if( Tag.IsEmpty() || Tag == "RspTerm" ) {

		if( !m_RspTerm && !m_RspLen ) {

			m_RspLen = 63;
			pWnd->UpdateUI("RspLen");
			}

		if( m_RspTerm > 256 ) {

			m_RspTerm = 13;
			}

		pWnd->UpdateUI("RspTerm");
		}

	if( Tag.IsEmpty() || Tag == "RspLen" ) {

		if( !m_RspTerm && !m_RspLen ) m_RspTerm = 13;

		pWnd->UpdateUI("RspLen");
		pWnd->UpdateUI("RspTerm");
		}

	if( Tag.IsEmpty() || Tag == "RspSkip" ) {

		if( !m_RspSkip ) {

			if( m_fRspBack ) m_fRspBack = FALSE;
			}

		pWnd->UpdateUI("RspBack");
		}

	if( Tag.IsEmpty() || Tag == "RspBack" ) {

		if( m_fRspBack ) {

			if( !m_RspSkip ) m_RspSkip = 1;

			pWnd->UpdateUI("RspSkip");
			}
		}
	}

// Download Support

BOOL CASCIIMDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddText(m_RFrame);
	Init.AddText(m_WFrame);

	Init.AddWord(WORD(m_RspTime));
	Init.AddWord(WORD(m_RspTerm));
	Init.AddWord(WORD(m_RspLen));
	Init.AddText(m_NakInd);

	Init.AddWord(WORD(m_Radix));

	Init.AddByte(BYTE(m_fRspEcho));
	Init.AddWord(WORD(m_EchoCmd));
	Init.AddWord(WORD(m_EchoRsp));

	Init.AddWord(WORD(m_RspStart));
	Init.AddWord(WORD(m_RspSkip));
	Init.AddByte(BYTE(m_fRspBack));
	Init.AddByte(BYTE(m_fRspWrite));

	return TRUE;
	}

// Meta Data Creation

void CASCIIMDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(RFrame);
	Meta_AddString(WFrame);

	Meta_AddInteger(RspTime);
	Meta_AddInteger(RspTerm);
	Meta_AddInteger(RspLen);
	Meta_AddString(NakInd);

	Meta_AddInteger(Radix);

	Meta_AddBoolean(RspEcho);
	Meta_AddInteger(EchoCmd);
	Meta_AddInteger(EchoRsp);	

	Meta_AddInteger(RspStart);
	Meta_AddInteger(RspSkip);
	Meta_AddBoolean(RspBack);
	Meta_AddBoolean(RspWrite);
	}

//////////////////////////////////////////////////////////////////////////
//
// ASCII Master Drive Driver
//

// Instantiator

ICommsDriver *	Create_ASCIIMDriver(void)
{
	return New CASCIIMDriver;
	}

// Constructor

CASCIIMDriver::CASCIIMDriver(void)
{
	m_wID		= 0x401F;

	m_uType		= driverMaster;

	m_Manufacturer	= "<System>";

	m_DriverName	= "ASCII Master";

	m_Version	= "1.00";

	m_ShortName	= "ASCII Master";

	AddSpaces();
	}

// Binding Control

UINT	CASCIIMDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CASCIIMDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CASCIIMDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CASCIIMDeviceOptions);
	}

// Address Management

BOOL CASCIIMDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CASCIIMAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CASCIIMDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	CString sErr = "";

	UINT uPar1   = tatoi(Text);

	UINT uFind1;

	if( uPar1 <= pSpace->m_uMaximum && uPar1 >= pSpace->m_uMinimum ) {

		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = pSpace->m_uType;

		if( pSpace->m_uTable == SPTXR || pSpace->m_uTable == SPWTXR ) {

			uFind1 = Text.Find('(');

			if( uFind1 && (uFind1 < NOTHING) ) {

				if( isdigit(Text[uFind1+1]) ) {

					Addr.a.m_Extra = Text[uFind1+1] - '0';

					if( Addr.a.m_Extra > 8 ) Addr.a.m_Extra = 0;
					}
				}
			}

		uPar1 += (uPar1 << 8);

		Addr.a.m_Offset = uPar1;

		return TRUE;
		}

	switch( pSpace->m_uTable ) {

		case SPTXR:
		case SPWTXR:
			sErr.Printf( "%s%d(0) - %s%d(8)",
				pSpace->m_Prefix,
				pSpace->m_uMinimum,
				pSpace->m_uMaximum
				);
			break;

		default:
			sErr.Printf( "%s%d - %s%d",
				pSpace->m_Prefix,
				pSpace->m_uMinimum,
				pSpace->m_Prefix,
				pSpace->m_uMaximum
				);
			break;
		}

	Error.Set( sErr,
		0
		);

	return FALSE;
	}

BOOL CASCIIMDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	switch( Addr.a.m_Table ) {

		case SPTXR:
		case SPWTXR:

			Text.Printf( "%s%d(%1.1d)",
				pSpace->m_Prefix,
				LOBYTE(Addr.a.m_Offset),
				Addr.a.m_Extra
				);

			break;

		case SPWRSP:

			Text.Printf( "WRSP" );

			break;

		default:

			Text.Printf( "%s%d",
			pSpace->m_Prefix,
			LOBYTE(Addr.a.m_Offset)
			);

			break;
		}

	return TRUE;
	}

// Implementation	

void CASCIIMDriver::AddSpaces(void)
{
	AddSpace(New CSpace( SPTXT,  "TXT",	"Read/Write Instruction Text\tSTRING",	10,   0, SZMAX, LL));
	AddSpace(New CSpace( SPTXR,  "TXR",	"Real/Integer Data for TXTn\tREAL",	10,   0, SZMAX, RR));
	AddSpace(New CSpace( SPTXS,  "TXS",	"String Data for TXTn\tSTRING",		10,   0, SZMAX, LL));
	AddSpace(New CSpace( SPWTX,  "WTX",	"Write Only Instruction Text\tSTRING",	10,   0, SZMAX, LL));
	AddSpace(New CSpace( SPWTXR, "WTXR",	"Send Real/Integer Data for WTXn\tREAL",10,   0, SZMAX, RR));
	AddSpace(New CSpace( SPWTXS, "WTXS",	"Send String Data for WTXn\tSTRING",	10,   0, SZMAX, LL));
	AddSpace(New CSpace( SPWCMD, "WCMD",	"Send WTXn - Omit 'Write Frame'\tBYTE",	10,   0, SZMAX, BB));
	AddSpace(New CSpace( SPWRSP, "WRSP",	"Latest WTXn Response\tSTRING",		10,   0,    20, LL));
	}

//////////////////////////////////////////////////////////////////////////
//
// ASCII Master Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CASCIIMAddrDialog, CStdAddrDialog);
		
// Constructor

CASCIIMAddrDialog::CASCIIMAddrDialog(CASCIIMDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	SetName(TEXT("ASCIIMAddrDlg"));
	}

// Message Map

AfxMessageMap(CASCIIMAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CASCIIMAddrDialog)
	};

// Message Handlers

BOOL CASCIIMAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();
	
	if( !m_fPart ) {

		SetCaption();

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			return FALSE;
			}

		return TRUE;
		}
	else {
		ShowAddress(Addr);

		return FALSE;
		}
	}

// Notification Handlers

void CASCIIMAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CASCIIMAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = m_pSpace->m_uMinimum;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(2001).SetWindowText("None");
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2006).SetWindowText("");
		GetDlgItem(2007).SetWindowText("");

		ClearDetails();

		GetDlgItem(2001).EnableWindow(TRUE);
		GetDlgItem(2002).EnableWindow(FALSE);
		GetDlgItem(2006).EnableWindow(FALSE);
		GetDlgItem(2007).EnableWindow(FALSE);
		}
	}

BOOL CASCIIMAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		Text += GetDlgItem(2002).GetWindowText();

		if( GetsMaxDP(m_pSpace->m_uTable) ) {

			Text += "(";
			Text += GetDlgItem(2007).GetWindowText();
			Text += ")";
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables
void CASCIIMAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	UINT uAddr  = LOBYTE(Addr.a.m_Offset);
	UINT uDP    = Addr.a.m_Extra;
	UINT uTable = Addr.a.m_Table;

	BOOL fNotR  = uTable != SPWRSP;

	GetDlgItem(2001).EnableWindow(TRUE);
	GetDlgItem(2002).EnableWindow(fNotR);

	BOOL fDP = GetsMaxDP(uTable);

	GetDlgItem(2006).EnableWindow(fDP);
	GetDlgItem(2007).EnableWindow(fDP);

	GetDlgItem(2002).SetWindowText(fNotR ? m_pSpace->GetValueAsText(uAddr) : "");

	GetDlgItem(3002).SetWindowText(m_pSpace->GetTypeAsText(Addr.a.m_Type));
	GetDlgItem(3008).SetWindowText("Decimal");

	CString Min = m_pSpace->m_Prefix + (fNotR ? "0" : "");
	CString Max = m_pSpace->m_Prefix + (fNotR ? m_pSpace->GetValueAsText(m_pSpace->m_uMaximum) : "");

	if( fDP ) {

		GetDlgItem(2006).SetWindowText("Write - Max DP");
		CString sShow;
		sShow.Printf("%1.1d", uDP);
		GetDlgItem(2007).SetWindowText(sShow);
		Min += "(0)";
		Max += "(8)";
		}

	else {
		GetDlgItem(2006).SetWindowText("");
		GetDlgItem(2007).SetWindowText("");
		}

	GetDlgItem(3004).SetWindowText(Min);
	GetDlgItem(3006).SetWindowText(Max);

	SetDlgFocus(2002);
	}

BOOL CASCIIMAddrDialog::GetsMaxDP(UINT uTable)
{
	return uTable == SPTXR || uTable == SPWTXR;
	}

// End of File
