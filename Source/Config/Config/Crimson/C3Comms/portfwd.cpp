
#include "intern.hpp"

#include "portfwd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Port Forward Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CPortForwardDriverOptions, CUIItem);

// Constructor

CPortForwardDriverOptions::CPortForwardDriverOptions(void)
{
	m_Protocol   = 0;
	
	m_TargetIP   = DWORD(MAKELONG(MAKEWORD(2, 200), MAKEWORD(168, 192)));

	m_TargetPort = 102;
	
	m_ExposePort = 102;
	}

// Download Support

BOOL CPortForwardDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Protocol));
	Init.AddLong(LONG(m_TargetIP));
	Init.AddWord(WORD(m_TargetPort));
	Init.AddWord(WORD(m_ExposePort));

	return TRUE;
	}

// Meta Data Creation

void CPortForwardDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Protocol);
	Meta_AddInteger(TargetIP);
	Meta_AddInteger(TargetPort);
	Meta_AddInteger(ExposePort);
	}

//////////////////////////////////////////////////////////////////////////
//
// Port Forward Driver
//

// Instantiator

ICommsDriver *	Create_PortForwardDriver(void)
{
	return New CPortForwardDriver;
	}

// Constructor

CPortForwardDriver::CPortForwardDriver(void)
{
	m_wID		= 0x4072;

	m_uType		= driverUtility;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Port Forwarder";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Port Forwarder";
	}

// Configuration

CLASS CPortForwardDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CPortForwardDriverOptions);
	}

// Binding Control

UINT CPortForwardDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CPortForwardDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount  = 2;
	
	Ether.m_UDPCount  = 0;
	}

// End of File
