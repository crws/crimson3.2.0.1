
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_P6K_HPP
	
#define	INCLUDE_P6K_HPP

//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Device Options
//

class CParker6KDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CParker6KDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_InitTime;
		UINT m_Device;
						

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Master Driver
//

class CParker6KSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CParker6KSerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration	
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
								
	protected:
		// Implementation
		void AddSpaces(void);
		BOOL IsString(CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parker 6K TCP/IP Master Driver Options
//

class CParker6KTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CParker6KTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_TimeWD;
		UINT m_TickWD;
		UINT m_Device;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parker 6K TCP/IP Master Driver
//

class CParker6KTCPDriver : public CParker6KSerialDriver
{
	public:
		// Constructor
		CParker6KTCPDriver(void);

		//Destructor
		//~CParker6KTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
					
	protected:
		// Implementation
		void AddSpaces(void);
	};
 
//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Space Wrapper Class
//

class CSpaceParker6K : public CSpace
{
	public:
		// Constructors
		CSpaceParker6K(UINT uTag, CString p, CString c, UINT n, UINT x, AddrType t, AddrType Span, UINT i=0);

		// Public Data
		UINT m_Info;
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CList <CSpaceParker6K *> CSpaceParker6KList;


//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Address Selection Dialog
//

class CParker6KDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CParker6KDialog(CParker6KSerialDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

	protected:
		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Helpers
		BOOL	IsGlobalWrite(void);
		void	SetGlobalWrite(BOOL fCheck);
	};

////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
//

#define P6K_AXIS	1
#define P6K_BRICK	2
#define P6K_INPUT	4
#define P6K_OUTPUT	8
#define P6K_RO		16
#define P6K_BIT		32
#define P6K_VAR		64

// End of File	

#endif
