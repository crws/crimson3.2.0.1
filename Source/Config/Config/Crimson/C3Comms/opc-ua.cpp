
#include "intern.hpp"

#include "opc-ua.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Tree Builder
//

// Operations

void COpcUaTreeBuilder::Reset(CString Root, WCHAR cSep)
{
	m_Path = Root + cSep;

	m_cSep = cSep;
}

UINT COpcUaTreeBuilder::Process(CString &Add, CString Node)
{
	UINT    uPos = Node.FindRev(m_cSep);

	CString Path = Node.Left(uPos+1);

	CString Name = Node.Mid(uPos+1);

	if( !Path.StartsWith(m_Path) ) {

		m_Path.Delete(m_Path.GetLength() - 1, 1);

		m_Path = m_Path.Left(m_Path.FindRev(m_cSep) + 1);

		Add.Empty();

		return actionClimbTree;
	}

	if( Path != m_Path ) {

		CString Rest   = Path.Mid(m_Path.GetLength());

		CString Folder = Rest.Left(Rest.Find(m_cSep));

		m_Path += Folder;

		m_Path += m_cSep;

		Add = Folder;

		return actionAddFolder;
	}

	Add = Name;

	return actionAddNode;
}

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(COpcUaDriverOptions, CUIItem);

// Constructor

COpcUaDriverOptions::COpcUaDriverOptions(void)
{
	m_Debug = 0;
}

// UI Managament

void COpcUaDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

	}

	CUIItem::OnUIChange(pWnd, pItem, Tag);
}

// Download Support

BOOL COpcUaDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Debug));

	return TRUE;
}

// Meta Data Creation

void COpcUaDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Debug);
}

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COpcUaDeviceOptions, CUIItem);

// Constructor

COpcUaDeviceOptions::COpcUaDeviceOptions(void)
{
	m_HostName = "192.168.1.217";

	m_Port     = 4840;

	m_FileTime = 0;

	m_FileAuto = FALSE;

	m_Partial  = FALSE;

	m_StrSize  = 1024;

	m_StrCode  = 0;

	FindStringLimit();
}

// UI Managament

void COpcUaDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Browse" ) {

			if( m_HostName.IsEmpty() ) {

				pWnd->Error(CString(IDS_PLEASE_ENTER_2));
			}
			else {
				CSaveFileDialog Dlg;

				Dlg.LoadLastPath(L"OpcUaBrowseFiles");

				Dlg.SetCaption(CString(IDS_SAVE_BROWSE_FILE));

				Dlg.SetFilter(CString(IDS_BROWSE_FILE));

				if( m_FileAuto ) {

					Dlg.SetFilename(m_FileName);
				}

				if( Dlg.ExecAndCheck() ) {

					CFilename File = Dlg.GetFilename();

					if( BrowseDevice(pWnd, File) ) {

						Dlg.SaveLastPath(L"OpcUaBrowseFiles");

						m_FileName = File;

						OnUIChange(pWnd, pItem, L"FileName");

						m_FileAuto = TRUE;

						pWnd->UpdateUI(this, L"FileName");

						CPrintf Msg(CString(IDS_DOWNLOAD), m_Nodes.GetCount());

						pWnd->Information(Msg);
					}
				}
			}
		}

		if( Tag == "FileName" ) {

			if( LoadBrowseFile(TRUE) ) {

				m_FileAuto = FALSE;

				ParseBrowseData();

				CSystemItem *pSystem = GetDatabase()->GetSystemItem();

				if( pSystem ) {

					pSystem->Rebuild(1);
				}

				CSysProxy System;

				System.Bind(pWnd);

				CItem *pPort = GetParent(AfxNamedClass(L"CCommsPort"));

				System.ItemUpdated(pPort, updateChildren);
			}
		}

		if( Tag == "StrSize" || Tag == "StrCode" ) {

			FindStringLimit();

			CSystemItem *pSystem = GetDatabase()->GetSystemItem();

			if( pSystem ) {

				pSystem->Rebuild(1);
			}
		}
	}
}

// Persistance

void COpcUaDeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();

	FindStringLimit();

	LoadBrowseFile(FALSE);

	ParseBrowseData();
}

// Download Support

BOOL COpcUaDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddText(m_HostName);

	Init.AddWord(WORD(m_Port));

	Init.AddText(m_UserName);

	Init.AddText(m_Password);

	Init.AddByte(BYTE(m_Partial));

	Init.AddWord(WORD(m_StrSize));

	Init.AddByte(BYTE(m_StrCode));

	if( TRUE ) {

		UINT c = m_Nodes.GetCount();

		Init.AddWord(WORD(c));

		for( UINT n = 0; n < c; n++ ) {

			if( m_NodeUsed[n] ) {

				Init.AddText(m_Nodes[n].m_Node);
			}
			else
				Init.AddWord(0);
		}
	}

	if( TRUE ) {

		UINT c = m_Slots.GetCount();

		Init.AddWord(WORD(c));

		for( UINT n = 0; n < c; n++ ) {

			Init.AddWord(WORD(m_Slots[n]));
		}
	}

	if( TRUE ) {

		UINT c = m_Sizes.GetCount();

		Init.AddWord(WORD(c));

		for( UINT n = 0; n < c; n++ ) {

			Init.AddWord(WORD(m_Sizes[n]));
		}
	}

	return TRUE;
}

// Meta Data Creation

void COpcUaDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(HostName);
	Meta_AddInteger(Port);
	Meta_AddString(UserName);
	Meta_AddString(Password);
	Meta_AddString(FileName);
	Meta_AddInteger(FileTime);
	Meta_AddInteger(FileAuto);
	Meta_AddBlob(FileData);
	Meta_AddBlob(NodeUsed);
	Meta_AddInteger(Partial);
	Meta_AddInteger(StrSize);
	Meta_AddInteger(StrCode);
}

// Implementation

BOOL COpcUaDeviceOptions::LoadBrowseFile(BOOL fForce)
{
	if( !m_FileName.IsEmpty() ) {

		CFilename Name = m_FileName;

		if( fForce || m_FileData.IsEmpty() || FindFileTime(Name) > m_FileTime ) {

			HANDLE hFile = Name.OpenReadSeq();

			if( hFile != INVALID_HANDLE_VALUE ) {

				UINT  uSize = GetFileSize(hFile, NULL);

				DWORD uRead = 0;

				CByteArray Blob;

				Blob.SetCount(uSize);

				if( ReadFile(hFile, PBYTE(Blob.GetPointer()), uSize, &uRead, NULL) ) {

					if( uRead == uSize ) {

						Blob.Append('\r');

						Blob.Append('\n');

						Blob.Append(0);

						if( Name.GetType() != L"xml" || ConvertXml(Blob) ) {

							m_FileData = Blob;

							m_FileTime = FindFileTime(hFile);

							m_NodeUsed.Empty();

							CloseHandle(hFile);

							SetDirty();

							return TRUE;
						}
					}
				}

				CloseHandle(hFile);
			}
		}
	}
	else {
		if( fForce ) {

			m_FileData.Empty();

			m_NodeUsed.Empty();

			SetDirty();

			return TRUE;
		}
	}

	return FALSE;
}

DWORD COpcUaDeviceOptions::FindFileTime(CFilename Name)
{
	HANDLE hFile = Name.OpenRead();

	if( hFile != INVALID_HANDLE_VALUE ) {

		DWORD dwTime = FindFileTime(hFile);

		CloseHandle(hFile);

		return dwTime;
	}

	return 0;
}

DWORD COpcUaDeviceOptions::FindFileTime(HANDLE hFile)
{
	FILETIME FileTime;

	GetFileTime(hFile, NULL, NULL, &FileTime);

	ULARGE_INTEGER Time;

	Time.LowPart  = FileTime.dwLowDateTime;

	Time.HighPart = FileTime.dwHighDateTime;

	return DWORD(Time.QuadPart / UINT64(10000000));
}

void COpcUaDeviceOptions::ParseBrowseData(void)
{
	m_Nodes.Empty();

	m_Slots.Empty();

	m_Sizes.Empty();

	m_Dict.Empty();

	UINT uSum = 0;

	if( m_FileData.GetCount() ) {

		for( UINT p = 0; p < 2; p++ ) {

			PCSTR pText = PCSTR(m_FileData.GetPointer());

			while( *pText ) {

				PCSTR pFind = strchr(pText, '\n');

				if( pFind ) {

					CString Line(pText, pFind - pText);

					Line.TrimBoth();

					CStringArray Fields;

					Line.Tokenize(Fields, ';');

					if( Fields.GetCount() == 5 || Fields.GetCount() == 6 ) {

						COpcNode Node;

						if( Fields[0].StartsWith(L"p=") && Fields[1].StartsWith(L"t=") ) {

							UINT uIndex = m_Nodes.GetCount();

							Node.m_Path = Fields[0].Mid(2);

							Node.m_Type = watoi(Fields[1].Mid(2));

							if( Fields.GetCount() == 6 ) {

								if( p == 1 ) {

									UINT uSize = watoi(Fields[2].Mid(2));

									if( (uSum % 64000) + uSize > 64000 ) {

										uSum -= uSum % 64000;

										uSum += 64000;
									}

									Node.m_Size   = m_Sizes.Append(uSize);

									Node.m_Slot   = uSum;

									Node.m_fWrite = watoi(Fields[3].Mid(2));

									Node.m_Node   = Fields[4] + L";" + Fields[5];

									uSum += uSize;

									m_Dict.Insert(Node.m_Path, uIndex);

									m_Nodes.Append(Node);
								}
							}
							else {
								if( p == 0 ) {

									Node.m_Size   = NOTHING;

									Node.m_Slot   = NOTHING;

									Node.m_fWrite = watoi(Fields[2].Mid(2));

									Node.m_Node   = Fields[3] + L";" + Fields[4];

									if( Node.m_Type == OpcUaId_String ) {

										Node.m_Slot = m_Slots.Append(uIndex);
									}

									m_Dict.Insert(Node.m_Path, uIndex);

									m_Nodes.Append(Node);
								}
							}
						}
					}

					pText = pFind + 1;

					continue;
				}

				break;
			}
		}
	}

	while( m_NodeUsed.GetCount() < m_Nodes.GetCount() ) {

		m_NodeUsed.Append(0);
	}
}

bool COpcUaDeviceOptions::ConvertXml(CByteArray &Blob)
{
	// TODO -- XML Import...

	CStringArray Lines;

	pugi::xml_document doc;

	if( !doc.load_buffer(Blob.GetPointer(), Blob.GetCount()).status ) {

		auto top = doc.children().begin();

		if( !_stricmp(top->name(), "UANodeSet") ) {

			struct CNode
			{
				CString id;
				CString name;
				CString parent;
				CString type;
			};

			CArray <CNode>         Nodes;

			CMap   <CString, UINT> Dict;

			for( auto x = top->begin(); x != top->end(); x++ ) {

				pugi::string_t n = x->name();

				if( n == "UAObject" || n == "UAVariable" ) {

					auto a1 = x->find_attribute([] (pugi::xml_attribute const &a) { return !_stricmp(a.name(), "NodeId");       });
					auto a2 = x->find_attribute([] (pugi::xml_attribute const &a) { return !_stricmp(a.name(), "BrowseName");   });
					auto a3 = x->find_attribute([] (pugi::xml_attribute const &a) { return !_stricmp(a.name(), "ParentNodeId"); });
					auto a4 = x->find_attribute([] (pugi::xml_attribute const &a) { return !_stricmp(a.name(), "DataType");     });

					if( !!a1 && !!a2 && !!a3 ) {

						CNode node;

						node.id     = !!a1 ? a1.value() : "";
						node.name   = !!a2 ? a2.value() : "";
						node.parent = !!a3 ? a3.value() : "";
						node.type   = !!a4 ? a4.value() : "";

						node.name   = node.name.Mid(node.name.Find(':')+1);

						Dict.Insert(node.id, Nodes.Append(node));
					}
				}
			}

			for( UINT n = 0; n < Nodes.GetCount(); n++ ) {

				CNode const &node = Nodes[n];

				if( !node.type.IsEmpty() ) {

					CPrintf query("/UANodeSet/Aliases/Alias[@Alias = \"%s\"]", node.type);

					auto    tnset = doc.select_nodes(Convert(query));

					if( !tnset.empty() ) {

						auto tnode = tnset.begin()->node().first_child();

						UINT tcode = atoi(tnode.value() + 2);

						UINT wtype = GetWireType(tcode);

						if( CanUseType(wtype) ) {

							CString  name   = node.name;

							CString  parent = node.parent;

							while( !!parent ) {

								INDEX find = Dict.FindName(parent);

								if( !Dict.Failed(find) ) {

									CNode const &prev = Nodes[Dict.GetData(find)];

									name   = "/" + name;

									name   = prev.name + name;

									parent = prev.parent;

									continue;
								}

								break;
							}

							CString line;

							line += "p=Objects/" + name + ";";

							line += CPrintf("t=%u;", wtype);

							line += "w=1;";

							line += node.id;

							Lines.Append(line);
						}
					}
				}
			}

			if( Lines.GetCount() ) {

				Blob.Empty();

				Lines.Sort();

				for( UINT n = 0; n < Lines.GetCount(); n++ ) {

					PCSTR p = Convert(Lines[n]);

					UINT  c = strlen(p);

					Blob.Append(PCBYTE(p), c);

					Blob.Append(BYTE(10));

					Blob.Append(BYTE(13));
				}

				Blob.Append(0);
			}

			return true;
		}
	}

	return false;
}

BOOL COpcUaDeviceOptions::SetAddress(CAddress &Addr, UINT uNode)
{
	COpcUaDeviceOptions::COpcNode const &Node = m_Nodes[uNode];

	if( Node.m_Size == NOTHING ) {

		if( Node.m_Slot == NOTHING ) {

			Addr.a.m_Table  = addrNamed;

			Addr.a.m_Type   = GetNodeType(Node);

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = uNode;
		}
		else {
			Addr.a.m_Table  = GetStringTable(Node);

			Addr.a.m_Type   = addrLongAsLong;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = GetStringOffset(Node);
		}
	}
	else {
		Addr.a.m_Table  = GetArrayTable(Node);

		Addr.a.m_Type   = GetNodeType(Node);

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = GetArrayOffset(Node);
	}

	return TRUE;
}

UINT COpcUaDeviceOptions::GetAddressNode(CAddress const &Addr)
{
	if( Addr.a.m_Table >= addrString && Addr.a.m_Table < addrArray ) {

		UINT uNode = GetStringNode(Addr.a.m_Table, Addr.a.m_Offset);

		if( uNode < m_Nodes.GetCount() ) {

			return uNode;
		}
	}

	if( Addr.a.m_Table >= addrArray && Addr.a.m_Table < addrNamed ) {

		UINT uNode = GetArrayNode(Addr.a.m_Table, Addr.a.m_Offset);

		if( uNode < m_Nodes.GetCount() ) {

			return uNode;
		}
	}

	if( Addr.a.m_Table == addrNamed ) {

		UINT uNode = Addr.a.m_Offset;

		if( uNode < m_Nodes.GetCount() ) {

			return uNode;
		}
	}

	return NOTHING;
}

UINT COpcUaDeviceOptions::GetStringNode(UINT uTable, UINT uOffset)
{
	UINT uSlot = 0;

	uSlot += (uTable - addrString) * m_StrTable;

	uSlot += (uOffset / m_StrPitch);

	return m_Slots[uSlot];
}

UINT COpcUaDeviceOptions::GetStringTable(COpcNode const &Node)
{
	return addrString + Node.m_Slot / m_StrTable;
}

UINT COpcUaDeviceOptions::GetStringOffset(COpcNode const &Node)
{
	return m_StrPitch * (Node.m_Slot % m_StrTable);
}

UINT COpcUaDeviceOptions::GetArrayNode(UINT uTable, UINT uOffset)
{
	// TODO -- This needs a dictionary!!!

	UINT uSlot = 64000 * (uTable - addrArray) + uOffset;

	for( UINT n = 0; n < m_Nodes.GetCount(); n++ ) {

		COpcNode const &Node = m_Nodes[n];

		if( Node.m_Size < NOTHING ) {

			if( Node.m_Slot == uSlot ) {

				return n;
			}
		}
	}

	return 0;
}

UINT COpcUaDeviceOptions::GetArrayTable(COpcNode const &Node)
{
	return addrArray + Node.m_Slot / 64000;
}

UINT COpcUaDeviceOptions::GetArrayOffset(COpcNode const &Node)
{
	return Node.m_Slot % 64000;
}

UINT COpcUaDeviceOptions::GetNodeType(COpcNode const &Node)
{
	switch( Node.m_Type ) {

		case OpcUaId_Boolean:
		case OpcUaId_SByte:
		case OpcUaId_Byte:
		case OpcUaId_Int16:
		case OpcUaId_UInt16:
		case OpcUaId_Int32:
		case OpcUaId_UInt32:

			return addrLongAsLong;

		case OpcUaId_Int64:
		case OpcUaId_UInt64:

			return addrLongAsLong;

		case OpcUaId_Float:
		case OpcUaId_Double:

			return addrRealAsReal;

		case OpcUaId_DateTime:

			return addrLongAsLong;
	}

	return 0;
}

UINT COpcUaDeviceOptions::GetNodeType(UINT uNode)
{
	if( uNode < m_Nodes.GetCount() ) {

		return GetNodeType(m_Nodes[uNode]);
	}

	return 0;
}

BOOL COpcUaDeviceOptions::CanWriteNode(COpcNode const &Node)
{
	return Node.m_fWrite;
}

BOOL COpcUaDeviceOptions::CanWriteNode(UINT uNode)
{
	if( uNode < m_Nodes.GetCount() ) {

		return CanWriteNode(m_Nodes[uNode]);
	}

	return 0;
}

UINT COpcUaDeviceOptions::GetNodeImage(COpcNode const &Node)
{
	if( Node.m_Slot == NOTHING ) {

		if( GetNodeType(Node) == addrLongAsLong ) {

			return CanWriteNode(Node) ? 0x0B : 0x0F;
		}

		if( GetNodeType(Node) == addrRealAsReal ) {

			return CanWriteNode(Node) ? 0x0C : 0x10;
		}

		return 0x2A;
	}

	return CanWriteNode(Node) ? 0x0D : 0x11;
}

UINT COpcUaDeviceOptions::GetNodeImage(UINT uNode)
{
	if( uNode < m_Nodes.GetCount() ) {

		return GetNodeImage(m_Nodes[uNode]);
	}

	return 0;
}

BOOL COpcUaDeviceOptions::BrowseDevice(CWnd *pWnd, CFilename File)
{
	CFilename This = afxModule->GetFilename();

	CFilename Prog = This.WithName(L"Utils\\OpcBrowse.exe");

	CFilename Path = This.GetDirectory();

	CString   Args;

	Args += CPrintf("opc.tcp://%s:%u", m_HostName, m_Port);

	if( !m_UserName.IsEmpty() ) {

		Args += " -u \"" + m_UserName + "\"";
	}

	if( !m_Password.IsEmpty() ) {

		Args += " -p \"" + m_Password + "\"";
	}

	Args += L" \"";

	Args += File;

	Args += L"\"";

	SHELLEXECUTEINFO Info;

	memset(&Info, 0, sizeof(Info));

	Info.cbSize       = sizeof(Info);
	Info.fMask	  = SEE_MASK_UNICODE | SEE_MASK_NOCLOSEPROCESS;
	Info.lpFile       = Prog;
	Info.lpParameters = Args;
	Info.lpDirectory  = Path;
	Info.nShow        = SW_HIDE;

	afxThread->SetWaitMode(TRUE);

	if( ShellExecuteEx(&Info) ) {

		DWORD dwCode = NOTHING;

		for( ;;) {

			if( WaitForSingleObject(Info.hProcess, 30000) == WAIT_TIMEOUT ) {

				CString Text;

				Text += IDS("The browsing operation is taking a long time.");

				Text += L"\n\n";

				Text += IDS("This can happen with complex devices.");

				Text += L"\n\n";

				Text += IDS("Do you want to continue the operation?");

				if( afxMainWnd->YesNo(Text) == IDYES ) {

					continue;
				}

				TerminateProcess(Info.hProcess, 300);
			}

			break;
		}

		if( GetExitCodeProcess(Info.hProcess, &dwCode) && !dwCode ) {

			CloseHandle(Info.hProcess);

			afxThread->SetWaitMode(FALSE);

			return TRUE;
		}

		afxThread->SetWaitMode(FALSE);

		pWnd->Error(CString(IDS_DOWNLOAD_2));

		return FALSE;
	}

	afxThread->SetWaitMode(FALSE);

	pWnd->Error(CString(IDS_DOWNLOAD_UTILITY));

	return FALSE;
}

PSTR COpcUaDeviceOptions::Convert(CString const &c)
{
	static char s[4096];

	PSTR w = s;

	for( PCTXT p = c; (*w = char(*p)); p++, w++ );

	return s;
}

UINT COpcUaDeviceOptions::GetWireType(UINT uType)
{
	switch( uType ) {

		case OpcUaId_Double:
		case OpcUaId_Float:
		case OpcUaId_Byte:
		case OpcUaId_SByte:
		case OpcUaId_Int16:
		case OpcUaId_UInt16:
		case OpcUaId_Int32:
		case OpcUaId_UInt32:
		case OpcUaId_Int64:
		case OpcUaId_UInt64:
		case OpcUaId_Boolean:
		case OpcUaId_DateTime:
		case OpcUaId_String:

			return uType;

		case OpcUaId_Duration:

			return OpcUaId_Double;

		case OpcUaId_ServerState:
		case OpcUaId_ServerStatusDataType:
		case OpcUaId_RedundancySupport:

			return OpcUaId_Int32;

		case OpcUaId_LocaleId:
		case OpcUaId_SignedSoftwareCertificate:

			return OpcUaId_String;

		case OpcUaId_UtcTime:

			return OpcUaId_DateTime;
	}

	return uType;
}

bool COpcUaDeviceOptions::CanUseType(UINT uType)
{
	switch( uType ) {

		case OpcUaId_Boolean:
		case OpcUaId_SByte:
		case OpcUaId_Byte:
		case OpcUaId_Int16:
		case OpcUaId_UInt16:
		case OpcUaId_Int32:
		case OpcUaId_UInt32:
		case OpcUaId_Int64:
		case OpcUaId_UInt64:
		case OpcUaId_Float:
		case OpcUaId_Double:
		case OpcUaId_DateTime:
		case OpcUaId_String:

			return true;
	}

	return false;
}

void COpcUaDeviceOptions::FindStringLimit(void)
{
	UINT uPack = 4 / (m_StrCode + 1);

	m_StrPitch = ((m_StrSize + uPack - 1) / uPack);

	m_StrTable = 64000 / m_StrPitch;

	m_StrLimit = (addrArray - addrArray) * m_StrTable;
}

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Driver
//

// Instantiator

ICommsDriver * Create_OpcUaDriver(void)
{
	return New COpcUaDriver;
}

// Constructor

COpcUaDriver::COpcUaDriver(void)
{
	m_wID		= 0x40E2;

	m_uType		= driverMaster;

	m_Manufacturer	= "OPC UA";

	m_DriverName	= "Client";

	m_Version	= "1.01";

	m_ShortName	= "OPC UA Client";

	m_DevRoot	= "DEV";
}

// Destructor

COpcUaDriver::~COpcUaDriver(void)
{
}

// Binding Control

UINT COpcUaDriver::GetBinding(void)
{
	return bindEthernet;
}

void COpcUaDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;

	Ether.m_UDPCount = 0;
}

// Configuration

CLASS COpcUaDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(COpcUaDriverOptions);
}

CLASS COpcUaDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COpcUaDeviceOptions);
}

// Address Management

BOOL COpcUaDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	COpcUaDeviceOptions *pDev = (COpcUaDeviceOptions *) pConfig;

	INDEX Find = pDev->m_Dict.FindName(Text);

	if( !pDev->m_Dict.Failed(Find) ) {

		UINT uNode = pDev->m_Dict.GetData(Find);

		if( uNode < pDev->m_Nodes.GetCount() ) {

			pDev->SetAddress(Addr, uNode);

			pDev->m_NodeUsed.SetAt(uNode, 1);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL COpcUaDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	COpcUaDeviceOptions *pDev  = (COpcUaDeviceOptions *) pConfig;

	UINT                 uNode = pDev->GetAddressNode(Addr);

	if( uNode < pDev->m_Nodes.GetCount() ) {

		Text = pDev->m_Nodes[uNode].m_Path;

		return TRUE;
		}

	return FALSE;
}

BOOL COpcUaDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	COpcUaDialog Dlg(ThisObject, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
}

BOOL COpcUaDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	COpcUaDeviceOptions *pDev = (COpcUaDeviceOptions *) pConfig;

	if( uItem == 0 ) {

		if( !pRoot ) {

			m_Build.Reset(L"Objects", '/');

			m_uPos    = 0;

			m_fFolder = FALSE;
		}
		else {
			if( pRoot->m_uData < NOTHING ) {

				return 0;
			}
		}
	}

	COpcUaDeviceOptions::COpcNode const &Node = pDev->m_Nodes[m_uPos];

	CString Name;

	UINT    uAct = m_Build.Process(Name, (Node.m_Size < NOTHING) ? L"" : Node.m_Path);

	if( uAct == COpcUaTreeBuilder::actionClimbTree ) {

		return 0;
	}

	if( uAct == COpcUaTreeBuilder::actionAddFolder ) {

		Data.m_Name       = Name;

		Data.m_Addr.m_Ref = 0;

		Data.m_fPart      = FALSE;

		Data.m_fRead      = FALSE;

		Data.m_uData      = NOTHING;

		return TRUE;
	}

	Data.m_Name = Name;

	Data.m_fPart = FALSE;

	Data.m_fRead = !pDev->CanWriteNode(m_uPos);

	Data.m_uData = m_uPos;

	pDev->SetAddress(Data.m_Addr, m_uPos);

	m_uPos++;

	return TRUE;
}

BOOL COpcUaDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	COpcUaDeviceOptions *pDev  = (COpcUaDeviceOptions *) pConfig;

	UINT                 uNode = pDev->GetAddressNode(Addr);

	return !pDev->CanWriteNode(uNode);
}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(COpcUaDialog, CStdDialog);

// Constructor

COpcUaDialog::COpcUaDialog(COpcUaDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pDriver = &Driver;

	m_pAddr   = &Addr;

	m_pConfig = (COpcUaDeviceOptions *) pConfig;

	m_fPart   = fPart;

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TreeIcon16"), afxColor(MAGENTA));

	SetName(TEXT("OpcUaDlg"));
}

// Message Map

AfxMessageMap(COpcUaDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

		AfxDispatchCommand(IDOK, OnOkay)

		AfxDispatchNotify(1000, TVN_SELCHANGED, OnTreeSelChanged)
		AfxDispatchNotify(1000, TVN_KEYDOWN, OnTreeKeyDown)
		AfxDispatchNotify(1000, TVN_ITEMEXPANDING, OnTreeExpanding)
		AfxDispatchNotify(1000, NM_DBLCLK, OnTreeDblClk)

		AfxMessageEnd(COpcUaDialog)
};

// Message Handlers

BOOL COpcUaDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1000);

	DWORD dwStyle = TVS_HASBUTTONS
		| TVS_NOTOOLTIPS
		| TVS_SHOWSELALWAYS
		| TVS_DISABLEDRAGDROP
		| TVS_HASLINES
		| TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

	Tree.SetImageList(TVSIL_STATE, m_Images);

	HTREEITEM hShow = NULL;

	HTREEITEM hInit = NULL;

	HTREEITEM hLast = NULL;

	COpcUaTreeBuilder Build;

	Build.Reset(L"Objects", '/');

	CTreeViewItem RootItem;

	RootItem.SetText(L"Objects");

	RootItem.SetImages(2);

	RootItem.SetParam(NOTHING);

	HTREEITEM hRoot = Tree.InsertItem(NULL, NULL, RootItem);

	HTREEITEM hWork = hRoot;

	UINT      uAddr = m_pConfig->GetAddressNode(*m_pAddr);

	for( UINT n = 0; n < m_pConfig->m_Nodes.GetCount(); n++ ) {

		COpcUaDeviceOptions::COpcNode const &Node = m_pConfig->m_Nodes[n];

		for( ;;) {

			CString Name;

			UINT    uAct = Build.Process(Name, Node.m_Path);

			if( uAct == COpcUaTreeBuilder::actionClimbTree ) {

				hWork = Tree.GetParent(hWork);

				continue;
			}

			if( uAct == COpcUaTreeBuilder::actionAddFolder ) {

				HTREEITEM hFind = NULL;

				for( HTREEITEM hScan = Tree.GetNextItem(hWork, TVGN_CHILD); hScan; hScan = Tree.GetNextItem(hScan, TVGN_NEXT) ) {

					if( Tree.GetItemText(hScan) == Name ) {

						hFind = hScan;

						break;
					}
				}

				if( hFind ) {

					hWork = hFind;
				}
				else {
					CTreeViewItem Item;

					Item.SetText(Name);

					Item.SetImages(34);

					Item.SetParam(NOTHING);

					hWork = Tree.InsertItem(hWork, NULL, Item);
				}

				continue;
			}

			CTreeViewItem Item;

			Item.SetText(Name);

			Item.SetImages(m_pConfig->GetNodeImage(n));

			Item.SetParam(n);

			HTREEITEM hNode = Tree.InsertItem(hWork, NULL, Item);

			if( !hInit ) {

				hInit = hNode;
			}

			if( Node.m_Path == m_pConfig->m_Last ) {

				hLast = hNode;
			}

			if( n == uAddr ) {

				hShow = hNode;
			}

			break;
		}
	}

	if( !hShow ) {

		hShow = hLast;
	}

	if( !hShow ) {

		hShow = hInit;
	}

	if( !hShow ) {

		Tree.Expand(hRoot, TVE_EXPAND);
	}
	else {
		Tree.EnsureVisible(hShow);

		Tree.SelectItem(hShow, TVGN_CARET);

		GetDlgItem(IDOK).EnableWindow(TRUE);
	}

	Tree.SetFocus();

	return FALSE;
}

// Command Handlers

BOOL COpcUaDialog::OnOkay(UINT uID)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1000);

	HTREEITEM hItem = Tree.GetSelection();

	UINT      uNode = Tree.GetItemParam(hItem);

	if( uNode < NOTHING ) {

		COpcUaDeviceOptions::COpcNode const &Node = m_pConfig->m_Nodes[uNode];

		m_pConfig->SetAddress(*m_pAddr, uNode);

		m_pConfig->m_NodeUsed.SetAt(uNode, 1);

		m_pConfig->m_Last = Node.m_Path;

		EndDialog(IDOK);

		return TRUE;
	}

	return FALSE;
}

// Notification Handlers

void COpcUaDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( Info.itemNew.lParam < NOTHING ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1000);

		Tree.EnsureVisible(Info.itemNew.hItem);

		GetDlgItem(IDOK).EnableWindow(TRUE);
	}
	else
		GetDlgItem(IDOK).EnableWindow(FALSE);
}

BOOL COpcUaDialog::OnTreeExpanding(UINT uID, NMTREEVIEW &Info)
{
	if( Info.action == TVE_EXPAND && Info.itemNew.iImage == 34 ) {

		CTreeView &   Tree = (CTreeView &) GetDlgItem(1000);

		CTreeViewItem Item(Info.itemNew.hItem);

		Item.SetImages(35);

		Tree.SetItem(Item);
	}

	if( Info.action == TVE_COLLAPSE && Info.itemNew.iImage == 35 ) {

		CTreeView &   Tree = (CTreeView &) GetDlgItem(1000);

		CTreeViewItem Item(Info.itemNew.hItem);

		Item.SetImages(34);

		Tree.SetItem(Item);
	}

	return FALSE;
}

BOOL COpcUaDialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	return FALSE;
}

void COpcUaDialog::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( GetDlgItem(IDOK).IsEnabled() ) {

		SendMessage(WM_COMMAND, IDOK);
	}
	else {
		CTreeView &Tree = (CTreeView &) GetDlgItem(1000);

		Tree.Expand(Tree.GetSelection(), TVE_TOGGLE);
	}
}

// End of File
