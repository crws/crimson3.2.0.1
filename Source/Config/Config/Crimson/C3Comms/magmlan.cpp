
#include "intern.hpp"

#include "magmlan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MaguireMLAN Comms Driver
//

// Instantiator

ICommsDriver *	Create_MaguireMLANDriver(void)
{
	return New CMaguireMLANDriver;
	}

// Constructor

CMaguireMLANDriver::CMaguireMLANDriver(void)
{
	m_wID		= 0x404B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Maguire";
	
	m_DriverName	= "MLAN";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Maguire MLAN";

	AddSpaces();
	}

// Binding Control

UINT	CMaguireMLANDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CMaguireMLANDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 1200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CMaguireMLANDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CMaguireMLANAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CMaguireMLANDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	UINT uT = pSpace->m_uTable;

	if( uT != SPPAR0 && uT != SPPAR1 && uT != SP_VER )

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);

	CString s;

	BOOL fIs0      = uT == SPPAR0;

	Addr.a.m_Table = uT;
	Addr.a.m_Type  = fIs0 || uT == SPPAR1 ? WW : LL;
	Addr.a.m_Extra = 0;

	switch( uT ) {

		case SP_VER:
			Addr.a.m_Offset = 0;
			return TRUE;

		case SPPAR0:
		case SPPAR1:
			Text.MakeUpper();

			UINT c;
			BOOL fOk;
			BOOL f3;

			f3  = Text.GetLength() == 3;

			fOk = f3 || !fIs0 && Text.GetLength() == 2;

			c = Text[0];

			if( fIs0 ) fOk &= (c >= '1' && c <= '9') || (c >= 'A' && c <= 'C');

			else fOk &= isalpha(c);

			fOk &= isalpha(Text[1]);

			if( f3 ) {

				BOOL f1;

				c  = Text[2];

				f1 = isalpha(c);

				if( fIs0 ) fOk &= f1;

				else fOk &= f1 || c == '1' || c == '2';
				}

			if( fOk ) {

				Addr.a.m_Offset = ParsePar(Text, fIs0);
				return TRUE;
				}
		}

	if( fIs0 ) s = "PARn[1-9,A-C][A-Z][A-Z]";

	else s = "PARa[A-Z][A-Z], PARa[A-Z][A-Z][A-Z,1,2]";

	Error.Set( s, 0 );

	return FALSE;
	}

BOOL CMaguireMLANDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	UINT uT = Addr.a.m_Table;

	switch( uT ) {

		case SPPAR0:
		case SPPAR1:
			Text.Printf( "%s%s",

				pSpace->m_Prefix,
				ExpandPar(Addr.a.m_Offset, uT == SPPAR0)
				);
			break;

		case SP_VER:
			Text.Printf( "VER" );
			break;

		case SPSKEY:
			Text.Printf( "%s",
				pSpace->m_Prefix
				);
			break;			

		default:
			if( uT == AN ) return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);

			Text.Printf( "%s%d",
				pSpace->m_Prefix,
				Addr.a.m_Offset
				);
			break;
		}

	return TRUE;
	}

UINT  CMaguireMLANDriver::ParsePar(CString Text, BOOL fIsPar0)
{
	UINT Result = 0;

	UINT u   = Text[0];

	Result   = fIsPar0 ? (isdigit(u) ? u - '0' : u - '7') : u - 'A';

	Result <<= 5;

	Result  += Text[1] - 'A';

	Result <<= 5;

	if( Text.GetLength() == 3 ) {

		u = Text[2];

		Result += 1 + (isdigit(u) ? 25 + u - '0' : u - 'A');
		}

	return Result;
	}

CString	CMaguireMLANDriver::ExpandPar(UINT uOffset, BOOL fIsPar0)
{
	CString Text = "";
	CString s1;

	UINT  u = uOffset >> 10;

	s1.Printf("%c", fIsPar0 ? (u < 10 ? '0' + u : 'A' + u - 10) : 'A' + u);

	Text += s1;

	s1.Printf( "%c", 'A' + ((uOffset & 0x3E0) >> 5) );

	Text += s1;

	u = uOffset & 0x1F;

	if( u ) {

		u -= 1;

		s1.Printf( "%c", u < 26 ? 'A' + u : '1' + u - 26 );

		Text += s1;
		}

	return Text;
	}

// Implementation

void	CMaguireMLANDriver::AddSpaces(void)
{
	AddSpace( New CSpace(   "ACR", "Abort Cycle (1) or Retry (2)",			 27, YY));
	AddSpace( New CSpace(   "ADD", "Get Address",					 54, WW));
	AddSpace( New CSpace(   "ALM", "Silence Alarm",					 82, BB));
	AddSpace( New CSpace(1, "BI",  "Get Batch Info",			10,   1,  4, WW));
	AddSpace( New CSpace(   "BW",  "Send BWW + BWF",				 83, BB));
	AddSpace( New CSpace(   "BWW", "   Batch Weight",				183, WW));
	AddSpace( New CSpace(   "BWF", "   Batch Flag Parameter",			283, WW));
	AddSpace( New CSpace(   "CLT", "Clear Totals",					 24, BB));
	AddSpace( New CSpace(   "CTI", "Clear Totals Immediately",			 28, BB));
	AddSpace( New CSpace(2, "CWW", "Get Cycle Weight(Word), Time",		10,   1,  2, LL));
	AddSpace( New CSpace(3, "CWL", "Get Cycle Weight(Long), Time",		10,   1,  2, LL));
	AddSpace( New CSpace(   "ECM", "Get Extrusion Control Mode",			 60, WW));
	AddSpace( New CSpace(4, "LST", "Get Linespeed Target, Time, Counts",	10,   1,  3, LL));
	AddSpace( New CSpace(   "MMS", "Mixer Motor State",				 66, LL));
	AddSpace( New CSpace(   "PCC", "Pause Current Cycle",				 25, LL));
	AddSpace( New CSpace(5, "PARn","Parameter-First character=hex digit ",	10,1025, 13114, WW));
	AddSpace( New CSpace(6, "PARa","Parameter-First character=alpha",	10,   0, 26428, WW));
	AddSpace( New CSpace(7, "RATE","Downstream Rate, Counts Per Length",	10,   1,  2, WW));
	AddSpace( New CSpace(   "RK",  "Set Remote Keypad",				 88, LL));
	AddSpace( New CSpace(8, "S", "Settings (S0=0->Read,=1->Fill,=2->Send)",	10,   0, 27, LL));
	AddSpace( New CSpace(9, "SKEY","Send Keystroke",			10,   0,  0, YY));
	AddSpace( New CSpace(   "SSR", "Steady State Rate",				 64, LL));
	AddSpace( New CSpace(   "SSS", "Start/Stop/Status",				 55, LL));
	AddSpace( New CSpace(   "SS0", "Start/Stop/Status-SubCmd 0 Response",		155, YY));
	AddSpace( New CSpace(10,"STAR", "* Function 82, * Function 52",		10,   1,  2, WW));
	AddSpace( New CSpace(11,"STAT","Status",				10,   1,  3, WW));
	AddSpace( New CSpace(12,"TAG", "Set Tag - Recipe, Work Order, Operator",10,   1,  3, LL));
	AddSpace( New CSpace(13,"TAR", "Target Throughput, Extrusion Control",	10,   1,  2, LL));
	AddSpace( New CSpace(14,"TOTR","Totals, then Reset (Set TOTR0 = 1)",	10,   0, 17, LL));
	AddSpace( New CSpace(15,"TOTS","Totals, No Reset",			10,   1, 17, LL));
	AddSpace( New CSpace(16,"TYP", "Get Load Cell and Software Type",	10,   1,  2, YY));
	AddSpace( New CSpace(   "WTU", "Weight Units",					 85, YY));
	AddSpace( New CSpace(17,"VER", "Version String",			10,   0,  1, LL));
	AddSpace( New CSpace(   "VOLT","Set Voltage",					 59, LL));
	AddSpace( New CSpace(18,"YST", "Get Yield Status/Steady State Rate",	10,   1,  2, LL));
	AddSpace( New CSpace(   "YSET","Set Yield",					 33, LL));
	AddSpace( New CSpace(   "NAK", "NAK Received",					255, LL));
	}

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CMaguireMLANAddrDialog, CStdAddrDialog);
		
// Constructor

CMaguireMLANAddrDialog::CMaguireMLANAddrDialog(CMaguireMLANDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_pDriverData = &Driver;

	m_fPart = fPart;
	}

CMaguireMLANAddrDialog::~CMaguireMLANAddrDialog(void)
{
	}

// Message Map

AfxMessageMap(CMaguireMLANAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CMaguireMLANAddrDialog)
	};

// Message Handlers

BOOL CMaguireMLANAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	m_pSpace = m_pDriverData->GetSpace(*m_pAddr);
	
	if( !m_fPart ) {

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			ShowDetails();

			return FALSE;
			}

		return TRUE;
		}
	else {
		ShowAddress(Addr);

		ShowDetails();

		return FALSE;
		}
	}

// Notification Handlers

void CMaguireMLANAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CMaguireMLANAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = m_pSpace->m_uMinimum;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			Addr.a.m_Table < AN	 &&
			Addr.a.m_Table != SP_VER  &&
			Addr.a.m_Table != SPSKEY
				? ShowDetails() : ClearDetails();

			ShowAddress(Addr);

			ShowDetails();
			}
		}
	else {
		m_pSpace = NULL;

		ClearType();

		ClearAddress();

		ClearDetails();
		}
	}

BOOL CMaguireMLANAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		if( m_pSpace->m_uTable < AN ) Text += GetDlgItem(2002).GetWindowText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables
void CMaguireMLANAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
	GetDlgItem(2001).EnableWindow(TRUE);

	GetDlgItem(2003).SetWindowText("");
	GetDlgItem(2003).EnableWindow(FALSE);

	LoadType();

	UINT uT = Addr.a.m_Table;

	if( uT == addrNamed || uT == SP_VER || uT == SPSKEY ) {

		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2002).EnableWindow(FALSE);
		return;
		}

	CString s;

	switch( Addr.a.m_Table ) {

		case SPPAR0:
		case SPPAR1:
			s = m_pDriverData->ExpandPar(Addr.a.m_Offset, Addr.a.m_Table == SPPAR0);
			break;

		default:
			s = m_pSpace->GetValueAsText(Addr.a.m_Offset, 3);
			break;
		}

	GetDlgItem(2002).SetWindowText( s );
	GetDlgItem(2002).EnableWindow(TRUE);

	SetDlgFocus(2002);
	}

// Helpers

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMagMLANTCPDeviceOptions, CUIItem);       

// Constructor

CMagMLANTCPDeviceOptions::CMagMLANTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 1, 0), MAKEWORD(  168, 192)));

	m_Unit   = 0;

	m_Port	 = 9999;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// Download Support

BOOL CMagMLANTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddByte(BYTE(m_Unit));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CMagMLANTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_MaguireMLANTCPDriver(void)
{
	return New CMaguireMLANTCPDriver;
	}

// Constructor

CMaguireMLANTCPDriver::CMaguireMLANTCPDriver(void)
{
	m_wID		= 0x3523;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Maguire";
	
	m_DriverName	= "MLAN TCP/IP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Maguire MLAN TCP/IP";
	}

// Binding Control

UINT CMaguireMLANTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CMaguireMLANTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS	CMaguireMLANTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMagMLANTCPDeviceOptions);
	}

// End of File
