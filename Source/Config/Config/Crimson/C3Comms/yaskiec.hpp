
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YASKAWAMPIEC_HPP
	
#define	INCLUDE_YASKAWAMPIEC_HPP

#define	YASKAWAMPIEC_IDENT 0x353B

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Device Options
//

class CYaskawaMPIECDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaMPIECDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_ExtendedWrite;
		UINT m_Endian;
		UINT m_IXHead;
		UINT m_QXHead;
		UINT m_IBHead;
		UINT m_QHead;
		UINT m_QBHead;
		UINT m_IXHigh;
		UINT m_QXHigh;
		UINT m_IBHigh;
		UINT m_QHigh;
		UINT m_QBHigh;
		UINT m_Model;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Overridables
		virtual	void SetIECMax(void);
	};

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Spaces
enum {
	SP_0	=  1, // coils 00000 - 09999
	SP_1	=  2, // input bits 10000 - 19999
	SP_3	=  3, // read-only registers 30000 - 39999
	SP_4	=  4, // holding registers 40000 - 49999
	SP_5	=  5, // 32 bit read only reals 30000 - 39998
	SP_6	=  6, // 64 bit read only reals 30000 - 39996
	SP_7	=  7, // 32 bit holding register reals 40000 - 49998
	SP_8	=  8, // 64 bit holding register reals 40000 - 49996
	SP_IX	= 20, // coils 00000 - 09999
	SP_QX	= 21, // input bits 10000 - 19999
	SP_QB	= 22, // registers 30000-39999
	SP_IB	= 23, // registers 40000-41023
	SP_Q	= 24, // registers 41024-49999
	SP_Q32	= 25, // 32 bit reals 30000-39998
	SP_Q64	= 26, // 64 bit reals 30000-39996
	SP_L32	= 27, // 32 bit reals 40000-49998
	SP_L64	= 28, // 64 bit reals 40000-49996
	};

// Device option presets
#define	IEC_BIT_LOBYTE	24560
#define	IEC_BIT_LO	25600
#define	IEC_BIT_HI	26870
#define	IEC_LO_R_LO	24576
#define	IEC_LO_R_HI	26623
#define	IEC_HI_R_LO	(IEC_LO_R_LO + 2048)
#define	IEC_HI_R_HI	32760

#define	MBMSGB0	(TEXT("Default Modbus = 000000 - 000127"))
#define	MBMSGB1	(TEXT("Default Modbus = 100000 - 100127"))
#define	MBMSGQB	(TEXT("Default Modbus = 300000 - 301023"))
#define	MBMSGIB	(TEXT("Default Modbus = 400000 - 401023"))
#define	MBMSGQ	(TEXT("Default Modbus = 401024 - 402047"))
#define	MBMSGQ4	(TEXT("Default Modbus = 300000 - 301022"))
#define	MBMSGQ8	(TEXT("Default Modbus = 300000 - 301020"))
#define	MBMSGA4	(TEXT("Default Modbus = 400000 - 402046"))
#define	MBMSGA8	(TEXT("Default Modbus = 400000 - 402044"))
#define	MBMBSEL	(TEXT("nnnn0 = Standard Modbus nnnn1"))

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MPxxxx IEC Driver
//

class CYaskawaMPIECDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaMPIECDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Data

		// Address Helpers
		BOOL	IsReadOnly(CItem *pConfig, CAddress const &Addr);
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	CheckAlignment(CSpace *pSpace);
		BOOL	IsBit(UINT uTable);
		BOOL	IsIEC(UINT uTable);
		void	SetHeads(UINT *puIX, UINT *puQX, UINT *puQB, UINT *puIB, UINT *puQ);
		DWORD	ModbusToIEC(UINT uTable, CAddress Addr);
		void	SetIsQ(BOOL fSet);
		BOOL	GetIsQ(void);
		UINT	GetQMax(void);
		UINT	GetQOffset(void);

		// Overridables
		virtual	BOOL Is64Bit(UINT uTable);
		virtual	UINT IECToModbus(CAddress Addr, UINT uMin, BOOL fIsBit);
		virtual	UINT GetIECMinByteNumber(UINT uTable);
		virtual	UINT GetIECMaxByteNumber(CAddress Addr);
		virtual UINT GetIBMax(void);

	protected:

		// Data
		UINT	m_uIX;
		UINT	m_uQX;
		UINT	m_uQB;
		UINT	m_uIB;
		UINT	m_uQ;
		BOOL	m_fIsQ;
		BOOL	m_fQHigh; // Q setting is higher than IB setting
		UINT	m_uIXHigh;
		UINT	m_uQXHigh;
		UINT	m_uIBHigh;
		UINT	m_uQBHigh;
		UINT	m_uQHigh;

		// Implementation
		void	AddSpaces(void);

		// Overridables
		virtual	UINT GetBitMax(UINT uMin);
		virtual	UINT GetWordMax(UINT uMin);
		
		// Helpers
		UINT	GetTypeFromModifier(CString Type);
		void	GetHeads(CItem *pConfig);
		UINT	GetLowWordMax(UINT uMin);
		UINT	GetDblMax(void);
		UINT	GetLongMax(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MPxxxx IEC Address Selection
//

class CYaskawaMPIECAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CYaskawaMPIECAddrDialog(CYaskawaMPIECDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		BOOL	m_fUpdateModbus;
		BOOL	m_fQHigh;

		UINT	m_uIX;
		UINT	m_uQX;
		UINT	m_uQB;
		UINT	m_uIB;
		UINT	m_uQ;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handler
		BOOL	OnOkay(UINT uID);
		void	OnTypeChange(UINT uID, CWnd &Wnd);
		BOOL	OnButtonClicked(UINT uID);

		// Implementation
		BOOL	AllowType(UINT uType);
		void	ShowDetails(void);

		// Overridables
		virtual	void ShowModbus(CAddress Addr);
		virtual	void ShowInfo(UINT uTable);

		// Helpers
		void	DoShow(UINT uTable, CAddress Addr);
		CString	ExpandIECAddress(CAddress Addr, BOOL fIsMax);
		void	ShowParameter(CAddress Addr);
		UINT	GetIECEntry(CAddress Addr);
		DWORD	GetModbusEntry(CAddress Addr, UINT uIECTable);
		DWORD	NormalizeMDB(CAddress Addr);
		void	GetMPMinimum(UINT uTable, CAddress &Addr);
		DWORD	IECEntryToModbusOffset(CAddress Addr);
		//
		BOOL	IsBit(UINT uTable);
		BOOL	Is64Bit(UINT uTable);
		BOOL	IsIEC(UINT uTable);
		UINT	IECToModbus(CAddress Addr, UINT uMin, BOOL fIsBit);
		DWORD	ModbusToIEC(UINT uTable, CAddress Addr);
		UINT	GetIECMinByteNumber(UINT uTable);
		UINT	GetIECMaxByteNumber(CAddress Addr);
		void	SetIsQ(BOOL fSet);
		UINT    GetIBMax(void);
		UINT	GetQOffset(void);
	};

// End of File

#endif
