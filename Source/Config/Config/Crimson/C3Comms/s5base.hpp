
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_S5BASE_HPP
	
#define	INCLUDE_S5BASE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Simatic S5 via AS511 Space Wrapper
//

class CSpaceS5 : public CSpace
{
	public:
		// Constructors
		CSpaceS5(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Base Driver
//

class CS5AS511BaseDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CS5AS511BaseDriver(BOOL fV2);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(BOOL fV2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Address Selection
//

class CS5AS511Dialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CS5AS511Dialog(CS5AS511BaseDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

// End of File

#endif
