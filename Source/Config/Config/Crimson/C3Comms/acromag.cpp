
#include "intern.hpp"

#include "acromag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Acromag TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CAcromagTCPDeviceOptions, CUIItem);       

// Constructor

CAcromagTCPDeviceOptions::CAcromagTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_fDisable16	= FALSE;
	
	m_fDisable15	= FALSE;

	m_fDisable5	= FALSE;

	m_fDisable6	= FALSE;

	m_PingReg	= 1;

	m_Max01		= 512;
	
	m_Max02		= 512;
	
	m_Max03		= 32;
	
	m_Max04		= 32;
	
	m_Max15		= 512;
	
	m_Max16		= 32;

	SetPages(2);
	}

// UI Management

void CAcromagTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		
		if( Tag.IsEmpty() || Tag == "Disable5" ) {

			pWnd->EnableUI("Disable15", !m_fDisable5);
			}

		if( Tag.IsEmpty() || Tag == "Disable6" ) {

			pWnd->EnableUI("Disable16", !m_fDisable6);
			}
	
		if( Tag.IsEmpty() || Tag == "Disable15" ) {

			pWnd->EnableUI("Max15", !m_fDisable15 && !m_fDisable5);

			pWnd->EnableUI("Disable5", !m_fDisable15);
			}

		if( Tag.IsEmpty() || Tag == "Disable16" ) {

			pWnd->EnableUI("Max16", !m_fDisable16 && !m_fDisable6);

			pWnd->EnableUI("Disable6", !m_fDisable16);
	      		}
		}
	}

// Download Support

BOOL CAcromagTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_fDisable15));
	Init.AddByte(BYTE(m_fDisable16));
	Init.AddByte(BYTE(m_fDisable5));
	Init.AddByte(BYTE(m_fDisable6));
	Init.AddWord(WORD(m_PingReg));
	Init.AddWord(WORD(m_Max01));
	Init.AddWord(WORD(m_Max02));
	Init.AddWord(WORD(m_Max03));
	Init.AddWord(WORD(m_Max04));
	Init.AddWord(WORD(m_Max15));
	Init.AddWord(WORD(m_Max16));

	return TRUE;
	}

// Meta Data Creation

void CAcromagTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddBoolean(Disable15);
	Meta_AddBoolean(Disable16);
	Meta_AddBoolean(Disable5);
	Meta_AddBoolean(Disable6);
	Meta_AddInteger(PingReg);
	Meta_AddInteger(Max01);
	Meta_AddInteger(Max02);
	Meta_AddInteger(Max03);
	Meta_AddInteger(Max04);
	Meta_AddInteger(Max15);
	Meta_AddInteger(Max16);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Acromag TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_AcromagTCPDriver(void)
{
	return New CAcromagTCPDriver;
	}

// Constructor

CAcromagTCPDriver::CAcromagTCPDriver(void)
{
	m_wID		= 0x352A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Acromag";
	
	m_DriverName	= "TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Acromag TCP/IP Master";

	m_DevRoot	= "ACR";

	AddSpaces();
	}

// Destructor

CAcromagTCPDriver::~CAcromagTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CAcromagTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CAcromagTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CAcromagTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CAcromagTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CAcromagTCPDeviceOptions);
	}

// Implementation

void CAcromagTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(11, "IC3",  "Input Counter\t(300xx)",		10,  4,    19, addrLongAsLong, addrLongAsLong, 4));

	AddSpace(New CSpace(12, "CS3",	"Counter Alarm Status   (30020)",	10,  0,     0, addrWordAsWord, addrWordAsWord, 4));

	AddSpace(New CSpace(13, "CT3",	"Counter Timer\t(300xx)",		10, 21,    28, addrWordAsWord, addrWordAsWord, 4));

	AddSpace(New CSpace(14, "CP4",	"Counter Preset\t(400xx)",		10, 16,    31, addrLongAsLong, addrLongAsLong, 4));

	AddSpace(New CSpace(15, "DI4",	"Digital Inputs\t(40039)",		10,  0,     0, addrWordAsWord, addrWordAsWord, 4));

	AddSpace(New CSpace(16, "DO4",	"Digital Outputs\t(40040)",		10,  0,     0, addrWordAsWord, addrWordAsWord, 4));

	AddSpace(New CSpace(17, "CS4",	"Counter Start\t(40043)",		10,  0,     0, addrWordAsWord, addrWordAsWord, 4));

	AddSpace(New CSpace(18, "CH4",	"Counter Stop\t(40044)",		10,  0,     0, addrWordAsWord, addrWordAsWord, 4));

	AddSpace(New CSpace(19, "CR4",	"Counter Reset\t(40045)",		10,  0,     0, addrWordAsWord, addrWordAsWord, 4));

	AddSpace(New CSpace(21, "IR3",	"Generic Input Registers    (3xxxx)",	10,  1,  9999, addrWordAsWord, addrWordAsReal, 4));

	AddSpace(New CSpace(20, "OR4",	"Generic Holding Registers (4xxxx)",	10,  1,  9999, addrWordAsWord, addrWordAsReal, 4));
	
	AddSpace(New CSpace(1, "4",	"Modbus Holding Registers",		10,  1, 65535, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(2, "3",	"Modbus Analog Inputs",			10,  1, 65535, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(3, "0",	"Modbus Digital Coils",			10,  1, 65535, addrBitAsBit));

	AddSpace(New CSpace(4, "1",	"Modbus Digital Inputs",		10,  1, 65535, addrBitAsBit));

	AddSpace(New CSpace(5, "L4",	"Modbus Holding Registers (32-bit)",	10,  1, 65535, addrLongAsLong, addrLongAsReal));

	AddSpace(New CSpace(6, "L3",	"Modbus Analog Inputs (32-bit)",	10,  1, 65535, addrLongAsLong, addrLongAsReal));
	}

// Address Management

BOOL CAcromagTCPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CAcromagTCPAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}
 

// Address Helpers

BOOL CAcromagTCPDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{	
	UINT uTable = pSpace->m_uTable;

	if ( uTable == 6 ) {

		uTable -= 4;
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Table = uTable;

		return TRUE;
		}

	return FALSE;
	}

BOOL CAcromagTCPDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
		case 2:
		case 5:
		case 6:

			return FALSE;
		}

	return TRUE;
	}


/////////////////////////////////////////////////////////////////////
//
// Acromag TCP Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CAcromagTCPAddrDialog, CStdAddrDialog);
		
// Constructor

CAcromagTCPAddrDialog::CAcromagTCPAddrDialog(CAcromagTCPDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	
	}

// Overridables

BOOL CAcromagTCPAddrDialog::AllowSpace(CSpace *pSpace)
{
	if( pSpace->m_uTable == 6 ) {

		return FALSE;
		}
	
	return TRUE;
	}


// End of File
