
#include "intern.hpp"

#include "l5keip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

// Runtime Class

AfxImplementRuntimeClass(CABL5kDialog, CStdDialog);

// Constructor

CABL5kDialog::CABL5kDialog(CABL5kDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart, BOOL fSelect)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = (CABL5kDeviceOptions *) pConfig;

	m_pNames  = m_pConfig->m_pNames;

	m_pTypes  = m_pConfig->m_pTypes;

	m_fPart   = fPart;

	m_fSelect = fSelect;

	m_fCreate = FALSE;

	m_pSelect = NULL;

	m_Images.Create("CommsManager", 16, 0, IMAGE_BITMAP, 0);
	
	SetName(fPart ? "CABL5kEIPPartDlg" : "CABL5kEIPFullDlg");
	}

// Destructor

CABL5kDialog::~CABL5kDialog(void)
{
	ClearStatus();
	}

// Message Map

AfxMessageMap(CABL5kDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(5001, OnImport)
	AfxDispatchCommand(4008, OnCreate)

	AfxMessageEnd(CABL5kDialog)
	};

// Message Handlers

BOOL CABL5kDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;
	
	if( !m_fPart ) {

		//
		BOOL fDebug = FALSE;

		for( UINT uID = 4000; uID <= 4008; uID ++ ) {

			GetDlgItem(uID).ShowWindow(fDebug ? SW_SHOW : SW_HIDE);
			}

		SetCaption();

		afxThread->SetWaitMode(TRUE);
		
		LoadTagNames();

		afxThread->SetWaitMode(FALSE);

		LoadDataTypes();

		if( Addr.m_Ref ) {

			ShowTableIndices(GetOffset(Addr));
		
			SetDlgFocus(2002);
			
			return FALSE;
			}

		HideTableIndices();
		
		EnableCancel(m_fSelect);

		GetDlgItem(IDOK).SetWindowText(m_fSelect ? "OK" : "Close");

		return TRUE;
		}
	else {
		CString Name;

		if( m_pConfig->DoExpandAddress(Name, Addr) ) {

			CString Type = m_pNames->FindItem(Name)->GetType();
			
			SetCaption();				
			
			SetAddressText(Name);

			ShowTableIndices(GetOffset(Addr), Type);

			SetDlgFocus(2002);
			}

		return FALSE;
		}
	}

// Notification Handlers

void CABL5kDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_pSelect  = (CABL5kItem *) Info.itemNew.lParam;

	if( m_pSelect && m_pSelect->IsAtomic() ) {

		CABL5kAtomicTag *pSelect = (CABL5kAtomicTag *) m_pSelect;

		CString Name = pSelect->GetFullName();

		CString Type = pSelect->GetType();

		SetDataTypeText(Type);

		if( TRUE ) {

			CArray <UINT> List;

			m_pNames->DimParse(List, Type);			
			
			//
			UINT   uMin = 0;

			CString Min = Name;

			m_pNames->DimExpand(List, uMin, Min);

			SetMinimumText(Min);

			//
			UINT   uMax = m_pNames->DimGetSize(List) - 1;			

			CString Max = Name;
			
			m_pNames->DimExpand(List, uMax, Max);

			SetMaximumText(Max);
			}

		UINT uType = m_pTypes->GetAtomicType(Type);
		
		SetAddrTypeText (CSpace("", "", 0).GetTypeAsText(uType));
		
		SetAddressText  (Name);
		
		ShowTableIndices(GetOffset(*m_pAddr));

		SetDataDescText (pSelect->m_Info);

		EnableOkay(TRUE);
		
		ShowStatus();

		return;
		}

	if( m_pSelect && m_pSelect->IsStruct() ) {

		CABL5kStructTag *pSelect = (CABL5kStructTag *) m_pSelect;

		SetDataTypeText(pSelect->GetType());
	
		SetMinimumText ("");
		
		SetMaximumText ("");

		SetAddrTypeText("");
		
		SetAddressText ("");

		HideTableIndices();

		SetDataDescText(pSelect->m_Info);

		EnableOkay(!m_fSelect);
		
		ClearStatus();

		return;
		}

	SetDataTypeText("");
	
	SetMinimumText("");
	
	SetMaximumText("");
	
	SetAddrTypeText("");
	
	SetAddressText ("");

	HideTableIndices();

	SetDataDescText("");

	ClearStatus();
	}

// Command Handlers

BOOL CABL5kDialog::OnOkay(UINT uID)
{
	if( m_fSelect ) {

		CString Text = GetAddressText();

		if( !Text.IsEmpty() ) {

			CError Error;

			CAddress Addr;
			
			if( m_pConfig->DoParseAddress(Error, Addr, Text) ) {
				
				*m_pAddr = Addr;

				afxThread->SetWaitMode(TRUE);
				
				EndDialog(TRUE);

				afxThread->SetWaitMode(FALSE);

				return TRUE;
				}

			Error.Show(ThisObject);

			SetDlgFocus(2002);

			return TRUE;		
			}

		m_pAddr->m_Ref = 0;
		}

	afxThread->SetWaitMode(TRUE);

	EndDialog(TRUE);

	afxThread->SetWaitMode(FALSE);

	return TRUE;		
	}

BOOL CABL5kDialog::OnImport(UINT uID)
{
	if( m_pNames->GetItemCount() ) {

		if( ThisObject.YesNo("All tags will be removed - continue ?") == IDNO ) {
			
			return TRUE;
			}
		}

	COpenFileDialog Dlg;

	Dlg.LoadLastPath("AB Tags");

	Dlg.SetCaption(CPrintf("Import Tags from %s", m_pConfig->GetDeviceName()));

	Dlg.SetFilter("RSLogix 5000 Import/Export Files (*.l5k)|*.l5k");

	if( Dlg.Execute(ThisObject) ) {
		
		CFilename Filename = Dlg.GetFilename();

		CError Error;

		try {
			afxThread->SetWaitMode(TRUE);

			if( m_pConfig->LoadFromFile(Error, Filename) ) {

				SetCaption();
				
				LoadDataTypes();				

				LoadTagNames();				

				CString Text =  "You have made changes to the Logix Tag Names that \n"
						"may invalidate data tag references in the database.\n\n"
						"You should correct this by running the Validate All Tags \n"
						"utility from the Data Tags window.";

				Information(Text);
				
				Dlg.SaveLastPath("AB Tags");
				}

			afxThread->SetWaitMode(FALSE);

			return TRUE;
			}

		catch(CUserException const &) {

			afxThread->SetWaitMode(FALSE);

			m_pTypes->Init();

			Error.Show(ThisObject);
		
			return TRUE;
			}

		afxThread->SetWaitMode(FALSE);
		
		return TRUE;
		}

	return TRUE;
	}

BOOL CABL5kDialog::OnCreate(UINT uID)
{
	CString   Type = GetCreateType();

	CString Format = CPrintf("%s_Tag", m_pConfig->GetDeviceName());

	CString   Name = m_pNames->CreateName(Format + "%d");

	if( !IsDown(VK_SHIFT) ) {

		CString Title = CPrintf("Create Tag of data type %s", Type);

		CString Group = "Tag Name";
		
		CStringDialog Dlg(Title, Group, Name);

		if( !Dlg.Execute(ThisObject) ) {
			
			return TRUE;
			}

		Name = Dlg.GetData();
		}

	CError Error;

	CABL5kItem *pTag;

	if( pTag = CreateTag(Error, Name, Type) ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		m_fCreate = TRUE;

		LoadTag(Tree, m_hRoot, pTag);

		m_fCreate = FALSE;

		if( m_fSelect ) {

			if( pTag->IsArray() && pTag->IsAtomic() ) {

				SetDlgFocus(2002);

				return TRUE;
				}
			}

		Tree.SetFocus();
		
		return TRUE;
		}
	else {
		Error.Show(ThisObject);
		
		return TRUE;
		}
	}

// Data Type Loading

void CABL5kDialog::LoadDataTypes(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(4001);

	Combo.SetRedraw(FALSE);

	Combo.ResetContent();

	LoadDataTypes(Combo, m_pTypes);

	Combo.SetCurSel(0);

	Combo.SetRedraw(TRUE);

	Combo.Invalidate(TRUE);

	UINT uDim[3] = { 0 };

	DWORD dwID[3] = { 4003, 4005, 4007 };

	for( UINT n = 0; n < elements(dwID); n ++ ) {
		
		GetDlgItem(dwID[n]).SetWindowText(CPrintf("%d", uDim[n]));
		}
	}

void CABL5kDialog::LoadDataTypes(CComboBox &Combo, CABL5kList *pList)
{
	INDEX Index = pList->GetHead();

	while( !pList->Failed(Index) ) {

		CABL5kItem *pItem = pList->GetItem(Index);

		Combo.AddString(pItem->GetName(), DWORD(pItem));

		pList->GetNext(Index);
		}
	}

// Tag Loading

void CABL5kDialog::LoadTagNames(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

	Tree.SendMessage(TVM_SETUNICODEFORMAT);
	
	Tree.DeleteItem(TVI_ROOT);
	
	m_fLoad = TRUE;
	
	LoadRoot(Tree);

	LoadTags(Tree, m_hRoot, m_pNames);

	m_fLoad = FALSE;

	Tree.SetFocus();
	}

void CABL5kDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	Node.SetText(m_pConfig->GetDeviceName());

	Node.SetParam(NULL);

	Node.SetImages(0x03);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CABL5kDialog::LoadTags(CTreeView &Tree, HTREEITEM hRoot, CABL5kList *pList)
{
	INDEX Index = pList->GetHead();

	while( !pList->Failed(Index) ) {

		LoadTag(Tree, hRoot, pList->GetItem(Index));
		
		pList->GetNext(Index);
		}

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CABL5kDialog::LoadTag(CTreeView &Tree, HTREEITEM hRoot, CABL5kItem *pItem)
{
	if( pItem->IsAtomic() ) {

		CTreeViewItem Node;

		Node.SetText(pItem->m_Name);

		Node.SetParam(DWORD(pItem));

		Node.SetImages(0x34);

		HTREEITEM hNode = Tree.InsertItem(hRoot, NULL, Node);

		if( m_fSelect ) {

			CString Name;

			if( m_pConfig->DoExpandAddress(Name, *m_pAddr) ) {

				if( pItem->IsArray() ) {
					
					Name = Name.Left(Name.FindRev('['));
					}

				CString Load = ((CABL5kAtomicTag *) pItem)->GetFullName();

				if( Name == Load ) {

					Tree.SelectItem(hNode, TVGN_CARET);
					}
				}
			}

		if( m_fCreate ) {

			Tree.SelectItem(hNode, TVGN_CARET);
			
			m_fCreate = FALSE;
			}
		
		return;
		}

	if( pItem->IsStruct() ) {
	
		CTreeViewItem Node;

		Node.SetText(pItem->m_Name);

		Node.SetParam(DWORD(pItem));

		Node.SetImages(0x34);

		HTREEITEM hNode = Tree.InsertItem(hRoot, NULL, Node);

		if( m_fCreate ) {

			Tree.SelectItem(hNode, TVGN_CARET);

			m_fCreate = FALSE;
			}

		LoadTags(Tree, hNode, pItem->GetElements());
		
		return;
		}
	}

// Implementation

void CABL5kDialog::SetCaption(void)
{
	CString Text;

	if( m_fSelect ) {

		Text.Printf( "Select Address for %s", 
			     m_pDriver->GetString(stringShortName)
			     );
		}
	else {
		Text.Printf( "View Data Tags for %s", 
			     m_pDriver->GetString(stringShortName)
			     );
		}
		
	if( !m_pConfig->m_Controller.IsEmpty() ) {
		
		Text += CPrintf(" [%s]", m_pConfig->m_Controller);
		}

	SetWindowText(Text);
	}

void CABL5kDialog::SetAddressText(CString Text)
{
	UINT p1, p2;

	if( (p1 = Text.FindRev('.')) == NOTHING ) {

		p1 = 0;
		}

	if( (p2 = Text.Find('[', p1)) < NOTHING) {
		
		Text = Text.Left(p2);
		}

	GetDlgItem(2001).SetWindowText(Text);
	}

void CABL5kDialog::HideTableIndices(void)
{
	DWORD dwID[3] = { 2002, 2003, 2004 };		

	for( UINT n = 0; n < elements(dwID); n ++ ) {

		GetDlgItem(dwID[n]).ShowWindow(SW_HIDE);
		}
	}

void CABL5kDialog::ShowTableIndices(UINT uIndex, CString Type)
{
	CArray <UINT> Dims;

	m_pNames->DimParse(Dims, Type);

	if( Dims.GetCount() ) {

		CArray <UINT> List;

		m_pNames->DimGetIndices(Dims, List, uIndex);
	
		DWORD dwID[3] = { 2002, 2003, 2004 };		

		for( UINT n = 0; n < elements(dwID); n ++ ) {

			CWnd &Wnd = GetDlgItem(dwID[n]);
			
			if( n < Dims.GetCount() ) {
				
				Wnd.ShowWindow(SW_SHOW);
				
				Wnd.SetWindowText(CPrintf("%d", List[n]));
				}
			else
				Wnd.ShowWindow(SW_HIDE);

			Wnd.EnableWindow(m_fSelect);
			}
		}
	else
		HideTableIndices();
	}

void CABL5kDialog::ShowTableIndices(UINT uIndex)
{
	if( m_pSelect && m_pSelect->IsAtomic() ) {

		CString Type = m_pSelect->GetType();

		ShowTableIndices(uIndex, Type);
		}
	else 
		HideTableIndices();
	}

void CABL5kDialog::SetAddrTypeText(CString const &Text)
{
	GetDlgItem(3002).SetWindowText(Text);
	}

void CABL5kDialog::SetMinimumText(CString const &Text)
{
	GetDlgItem(3004).SetWindowText(Text);
	}

void CABL5kDialog::SetMaximumText(CString const &Text)
{
	GetDlgItem(3006).SetWindowText(Text);
	}

void CABL5kDialog::SetDataTypeText(CString const &Text)
{
	GetDlgItem(3008).SetWindowText(Text);
	}

void CABL5kDialog::SetDataDescText(CString const &Text)
{
	GetDlgItem(3010).SetWindowText(Text);
	}

CString CABL5kDialog::GetAddressText(void)
{
	CString Text = GetDlgItem(2001).GetWindowText();

	CString Index;

	DWORD dwID[3] = { 2002, 2003, 2004 };

	for( UINT n = elements(dwID), m = 0; n; n -- ) {

		CWnd &Wnd = GetDlgItem(dwID[n - 1]);

		if( Wnd.IsWindowVisible() ) {

			if( m ++ ) {
				
				Index += ",";
				}

			Index += Wnd.GetWindowText();
			}
		}

	if( !Index.IsEmpty() ) {

		Text += "[";

		Text += Index;

		Text += "]";
		}		

	return Text;
	}

CString CABL5kDialog::GetCreateType(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(4001);
	
	CString     Type = Combo.GetLBText(Combo.GetCurSel());

	UINT uDim[3] = { 0 };
	
	DWORD dwID[3] = { 4003, 4005, 4007 };

	for( UINT n = 0; n < elements(dwID); n ++ ) {

		CWnd &Wnd = GetDlgItem(dwID[n]);

		uDim[n] = tatoi(Wnd.GetWindowText());
		}
	
	CString Index;

	if( uDim[0] > 0 ) {

		Index = CPrintf("[%d]", uDim[0]);

		if( uDim[1] > 0 ) {

			Index = CPrintf("[%d,%d]", uDim[1], uDim[0]);

			if( uDim[2] > 0 ) {

				Index = CPrintf("[%d,%d,%d]", uDim[2], uDim[1], uDim[0]);
				}
			}
		}

	return Type + Index;
	}

CABL5kItem * CABL5kDialog::CreateTag(CError &Error, CString const &Name, CString const &Type)
{
	if( !m_pNames->FindName(Name) ) {

		if( m_pNames->CreateTag(Error, Name, Type) ) {

			return m_pNames->FindItem(Name);
			}

		return NULL;
		}
		
	Error.Set("A tag with that name already exists.", 0);
	
	return NULL;
	}

BOOL CABL5kDialog::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
	}

void CABL5kDialog::EnableOkay(BOOL fEnable)
{
	GetDlgItem(IDOK).EnableWindow(fEnable);
	}

void CABL5kDialog::EnableCancel(BOOL fEnable)
{
	GetDlgItem(IDCANCEL).EnableWindow(fEnable);
	}

void CABL5kDialog::EnableImport(BOOL fEnable)
{
	GetDlgItem(5001).EnableWindow(fEnable);
	}

// Debug

void CABL5kDialog::ClearStatus(void)
{
	afxThread->SetStatusText("");
	}

void CABL5kDialog::ShowStatus(void)
{
	if( FALSE ) {

		CABL5kAtomicTag *pSelect = (CABL5kAtomicTag *) m_pSelect;

		CString Text;

		Text += pSelect->GetFullName();
		
		afxThread->SetStatusText(Text);
		}
	}

// End of File
