
#include "intern.hpp"

#include "metasys.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();



/////////////////////////////////////////////////////////////////////////
//
// Metasys Space Wrapper Class
//

// Constructors

CMetaSysSpace::CMetaSysSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT MinReg, UINT MaxReg, UINT Attr)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= x;
	m_uType		= t;
	m_uSpan		= s;

	m_uMinReg	= MinReg;
	m_uMaxReg	= MaxReg;
	m_uAttribute    = Attr;

	FindAlignment();

	FindWidth();
	}

// Limits

void CMetaSysSpace::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);
	
	Addr.a.m_Offset  = Addr.a.m_Offset * m_uAttribute;

	Addr.a.m_Offset += (m_uAttribute - 1);

	Addr.a.m_Extra   = m_uMaxReg - m_uMinReg;
	}

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CMetaSysDriverOptions, CUIItem);

// Constructor

CMetaSysDriverOptions::CMetaSysDriverOptions(void)
{
	m_Drop = 1;
	}

// Download Support

BOOL CMetaSysDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CMetaSysDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMetaSysDeviceOptions, CUIItem);

// Constructor

CMetaSysDeviceOptions::CMetaSysDeviceOptions(void)
{
	m_Drop = 2;
	}

// Download Support

BOOL CMetaSysDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CMetaSysDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Johnson Controls MetaSys N2 System Slave Driver
//

// Instantiator

ICommsDriver * Create_MetasysN2SystemDriver(void)
{
	return New CMetasysN2SystemDriver;
	}

// Constructor

CMetasysN2SystemDriver::CMetasysN2SystemDriver(void)
{
	m_wID		= 0x405D;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Johnson Controls";
	
	m_DriverName	= "MetaSys N2 System";
	
	m_Version	= "BETA";
	
	m_ShortName	= "MetaSys";

	AddSpaces();
	}

// Destructor

CMetasysN2SystemDriver::~CMetasysN2SystemDriver(void)
{
	DeleteAllSpaces();
	}

// Configuration

CLASS CMetasysN2SystemDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMetaSysDriverOptions);
	}

CLASS CMetasysN2SystemDriver::GetDeviceConfig(void)
{
	return NULL;
	}


// Binding Control

UINT CMetasysN2SystemDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMetasysN2SystemDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CMetasysN2SystemDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CMetaSysAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}
    
// Implementation

void CMetasysN2SystemDriver::AddSpaces(void)
{
	AddSpace(New CMetaSysSpace(1,	"AI", "Analog Inputs",		10, 1, 256, addrRealAsReal, addrRealAsReal, 1, 1,14));
	AddSpace(New CMetaSysSpace(2,	"AO", "Analog Outputs",		10, 1, 256, addrRealAsReal, addrRealAsReal, 3, 3, 5));
	AddSpace(New CMetaSysSpace(3,	"BI", "Binary Inputs",		10, 1, 256, addrRealAsReal, addrRealAsReal, 2, 2, 4));
	AddSpace(New CMetaSysSpace(4,	"BO", "Binary Outputs",		10, 1, 256, addrWordAsWord, addrWordAsWord, 4, 4, 7));
	AddSpace(New CMetaSysSpace(5,	"IF", "Internal Floats",	10, 1, 256, addrRealAsReal, addrRealAsReal, 5, 5, 2));
	AddSpace(New CMetaSysSpace(6,	"II", "Internal Integers",	10, 1, 256, addrWordAsWord, addrWordAsWord, 6, 6, 2));
	AddSpace(New CMetaSysSpace(7,	"IB", "Internal Bytes",		10, 1, 256, addrByteAsByte, addrByteAsByte, 7, 7, 2));
	}

// Address Helpers

BOOL CMetasysN2SystemDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type  = StripType(pSpace, Text);

	CString Sub = Text;

	CMetaSysSpace * pMeta = (CMetaSysSpace *) pSpace;

	UINT uFind = Sub.Find(':');

	Text = Sub.Mid(0, uFind);

	UINT uRegion = pMeta->m_uMinReg;
	
	UINT uAttr   = pMeta->m_uAttribute;

	if( uFind < NOTHING ) {

		PTXT pError = NULL;

		UINT uEnd = Sub.Mid(uFind + 1).Find(':');

		if( uEnd < NOTHING ) {

			uRegion = tstrtoul(Sub.Mid(uFind + 1, uEnd), &pError, pMeta->m_uRadix);

			if( uRegion < pMeta->m_uMinReg || uRegion > pMeta->m_uMaxReg ) {
			
				Error.Set( CString("Invalid region"),
					   0
					   );
			
				return FALSE;
				}
		
			Sub = Sub.Mid(uFind + 1);

			uFind = Sub.Find(':');

			if( uFind ) {

				Sub = Sub.Mid(uFind + 1);

				pError = NULL;

				uAttr = tstrtol(Sub, &pError, pMeta->m_uRadix);

				if( uAttr < 1 || uAttr > pMeta->m_uAttribute ) {

					Error.Set( CString("Invalid attribute"),
						   0
					           );

					return FALSE;
					}
				}
			}
		}

	Text += "." + Type;

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Offset = (Addr.a.m_Offset - 1) * pMeta->m_uAttribute;

		Addr.a.m_Offset += ( uAttr - 1 );

		Addr.a.m_Extra  = uRegion - pMeta->m_uMinReg;

		return TRUE;
		}

	return FALSE;
	}

BOOL CMetasysN2SystemDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CMetaSysSpace * pMeta = (CMetaSysSpace *) GetSpace(Addr);

	if( pMeta ) {

		UINT uOffset = (Addr.a.m_Offset + 1) / pMeta->m_uAttribute;

		UINT uAttr   = Addr.a.m_Offset % pMeta->m_uAttribute + 1;

		UINT uRegion = Addr.a.m_Extra + pMeta->m_uMinReg;

		UINT uType   = Addr.a.m_Type;

		if( uType == pMeta->m_uType ) {

			Text.Printf("%s%s:%s:%s", pMeta->m_Prefix,
						  pMeta->GetValueAsText(uOffset),
						  pMeta->GetValueAsText(uRegion),
						  pMeta->GetValueAsText(uAttr));
			
		
			return TRUE;
			}

		Text.Printf("%s%s:%s:%s.%s", pMeta->m_Prefix,
					     pMeta->GetValueAsText(uOffset),
					     pMeta->GetValueAsText(uRegion),
					     pMeta->GetValueAsText(uAttr),
					     pMeta->GetTypeModifier(uType));
						
		return TRUE;
		}

	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// MetaSys Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CMetaSysAddrDialog, CStdAddrDialog);
		
// Constructor

CMetaSysAddrDialog::CMetaSysAddrDialog(CMetasysN2SystemDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "MetaSysElementDlg";
	}

// Overridables

void CMetaSysAddrDialog::SetAddressText(CString Text)
{	
	CMetaSysSpace * pSpace = (CMetaSysSpace *) m_pSpace;

	if( pSpace ) {
       
		CAddress Addr;

		if( pSpace->MatchSpace(*m_pAddr) ) {

			Addr.m_Ref = m_pAddr->m_Ref;
			}
		else {
			pSpace->GetMinimum(Addr);
			}

		GetDlgItem(2005).EnableWindow(pSpace->m_uMinReg != pSpace->m_uMaxReg);

		GetDlgItem(2005).SetWindowText(pSpace->GetValueAsText(Addr.a.m_Extra  + pSpace->m_uMinReg));

		GetDlgItem(2007).SetWindowText(pSpace->GetValueAsText(Addr.a.m_Offset % pSpace->m_uAttribute + 1));

		GetDlgItem(2002).SetWindowText(pSpace->GetValueAsText(Addr.a.m_Offset / pSpace->m_uAttribute));
		}

	else {
		GetDlgItem(2005).EnableWindow(FALSE);
		}

	GetDlgItem(2002).EnableWindow(pSpace != NULL);

	GetDlgItem(2007).EnableWindow(pSpace != NULL);
	
	}

CString CMetaSysAddrDialog::GetAddressText(void)
{
	return GetDlgItem(2002).GetWindowText() + ":" + 
	       GetDlgItem(2005).GetWindowText() + ":" +
	       GetDlgItem(2007).GetWindowText();
	}


//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Slave Driver - Single Device - Current Value Only
//
//

// Instantiator

ICommsDriver * Create_MetasysN2SystemDriver2(void)
{
	return New CMetasysN2SystemDriver2;
	}

// Constructor

CMetasysN2SystemDriver2::CMetasysN2SystemDriver2(void)
{
	m_wID		= 0x405F;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Johnson Controls";
	
	m_DriverName	= "MetaSys N2 System (v2)";
	
	m_Version	= "2.00";
	
	m_ShortName	= "MetaSys N2";

	AddSpaces();
	}

// Destructor

CMetasysN2SystemDriver2::~CMetasysN2SystemDriver2(void)
{
	DeleteAllSpaces();
	}

// Configuration

CLASS CMetasysN2SystemDriver2::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMetaSysDriverOptions);
	}


CLASS CMetasysN2SystemDriver2::GetDeviceConfig(void)
{
	return NULL;
	}


// Binding Control

UINT CMetasysN2SystemDriver2::GetBinding(void)
{
	return bindStdSerial;
	}

void CMetasysN2SystemDriver2::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CMetasysN2SystemDriver2::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"AI", "Analog Inputs",		10, 1, 256, addrRealAsReal));
	AddSpace(New CSpace(2,	"AO", "Analog Outputs",		10, 1, 256, addrRealAsReal));
	AddSpace(New CSpace(3,	"BI", "Binary Inputs",		10, 1, 256, addrBitAsBit));
	AddSpace(New CSpace(4,	"BO", "Binary Outputs",		10, 1, 256, addrBitAsBit));
	AddSpace(New CSpace(5,	"IF", "Internal Floats",	10, 1, 256, addrRealAsReal));
	AddSpace(New CSpace(6,	"II", "Internal Integers",	10, 1, 256, addrWordAsWord));
	AddSpace(New CSpace(7,	"IB", "Internal Bytes",		10, 1, 256, addrByteAsByte));
	}


//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Slave Driver - Multi Device - Current Value Only
//
//

// Instantiator

ICommsDriver * Create_MetasysN2SystemDriver3(void)
{
	return New CMetasysN2SystemDriver3;
	}

// Constructor

CMetasysN2SystemDriver3::CMetasysN2SystemDriver3(void)
{
	m_wID		= 0x4060;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Johnson Controls";
	
	m_DriverName	= "MetaSys N2 Slave";
	
	m_Version	= "1.01";
	
	m_ShortName	= "N2 Slave";
	}

// Configuration

CLASS CMetasysN2SystemDriver3::GetDriverConfig(void)
{
	return NULL;
	}


CLASS CMetasysN2SystemDriver3::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMetaSysDeviceOptions);
	}

// End of File