
#include "intern.hpp"

#include "ti500.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simatic TI-500 Series
//

// Instantiator

ICommsDriver *Create_TI500Driver(void)
{
	return New CTI500Driver;
	}

// Constructor

CTI500Driver::CTI500Driver(void)
{
	m_wID		= 0x3317;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "TI-500 Series";
	
	m_Version	= "1.03";
	
	m_ShortName	= "TI 500";

	AddSpaces();
	}

// Configuration

CLASS CTI500Driver::GetDeviceConfig(void)
{
	return NULL;
	}

// Binding Control

UINT CTI500Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CTI500Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}


// Address Helper

BOOL CTI500Driver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Implementation

void CTI500Driver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"V",	"Data Registers",	10, 1, 26624,	addrWordAsWord, addrWordAsReal));
//*	AddSpace(New CSpace(2,	"K",	"K Memory",		10, 1, 1024,	addrWordAsWord		  ));
//*	AddSpace(New CSpace(3,	"DCP",	"DCP Memory",		10, 1, 1024,	addrWordAsWord		  ));
//*	AddSpace(New CSpace(4,	"DCC",	"DCC Memory",		10, 1, 1024,	addrWordAsWord		  ));
	AddSpace(New CSpace(5,	"STW",	"STW Memory",		10, 1, 1024,	addrWordAsWord		  ));
//*	AddSpace(New CSpace(6,	"G",	"G Memory",		10, 1, 1024,	addrWordAsWord		  ));
	AddSpace(New CSpace(8,	"TCC",	"TCC Memory",		10, 1, 20480,	addrWordAsWord		  ));
	AddSpace(New CSpace(9,	"TCP",	"TCP Memory",		10, 1, 20480,	addrWordAsWord		  ));
	AddSpace(New CSpace(13,	"WX",	"Input Memory",		10, 1, 1024,	addrWordAsWord		  ));
	AddSpace(New CSpace(15,	"WY",	"Output Memory",	10, 1, 1024,	addrWordAsWord		  ));
	AddSpace(New CSpace(0x21, "C",  "Control Bits",		10, 1, 0xFFFF,	addrBitAsBit		  ));
	AddSpace(New CSpace(0x25, "X",  "Input Bits",		10, 1, 0xFFFF,	addrBitAsBit		  ));
	AddSpace(New CSpace(0x27, "Y",  "Output Bits",		10, 1, 0xFFFF,	addrBitAsBit		  ));

	}

//////////////////////////////////////////////////////////////////////////////////
//
// TI 500 v2 Master Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTI500v2MasterDeviceOptions, CUIItem);       

// Constructor

CTI500v2MasterDeviceOptions::CTI500v2MasterDeviceOptions(void)
{
	m_fBlock = FALSE;
	}

// Download Support

BOOL CTI500v2MasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_fBlock));
	
	return TRUE;
	}

// Meta Data Creation

void CTI500v2MasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddBoolean(Block);
	}
 
//////////////////////////////////////////////////////////////////////////
//
// Simatic TI-500 Series v2
//

// Instantiator

ICommsDriver *Create_TI500v2Driver(void)
{
	return New CTI500v2Driver;
	}

// Constructor

CTI500v2Driver::CTI500v2Driver(void	)
{
	m_wID		= 0x407B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "TI-500 Series";
	
	m_Version	= "2.00";
	
	m_ShortName	= "TI 500";

	AddSpaces(NULL);
	}

// Configuration

CLASS CTI500v2Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTI500v2MasterDeviceOptions);
	}

// Binding Control

UINT CTI500v2Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CTI500v2Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}


// Address Helper

BOOL CTI500v2Driver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Address Management

BOOL CTI500v2Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	DeleteAllSpaces();

	AddSpaces(pConfig);
	
	return CStdCommsDriver::SelectAddress(hWnd, Addr, pConfig, fPart);
	}

// Implementation

void CTI500v2Driver::AddSpaces(CItem * pConfig)
{
	BOOL fBlock = TRUE;
	
	if( pConfig ) {
		
		fBlock = pConfig->GetDataAccess("Block")->ReadInteger(pConfig);
		}

	AddSpace(New CSpace(1,	  "V",		"Data Registers",			10, 1, 26624,	addrWordAsWord, addrWordAsReal));
//	AddSpace(New CSpace(2,	  "K",		"K Memory",				10, 1, 1024,	addrWordAsWord		  ));
//	AddSpace(New CSpace(3,	  "DCP",	"DCP Memory",				10, 1, 1024,	addrWordAsWord		  ));
//	AddSpace(New CSpace(4,	  "DCC",	"DCC Memory",				10, 1, 1024,	addrLongAsLong, addrLongAsReal));
	AddSpace(New CSpace(5,	  "STW",	"STW Memory",				10, 1, 1024,	addrWordAsWord, addrWordAsReal));
//	AddSpace(New CSpace(6,	  "G",		"G Memory",				10, 1, 1024,	addrWordAsWord		  ));
	AddSpace(New CSpace(8,	  "TCC",	"TCC Memory",				10, 1, 20480,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(9,	  "TCP",	"TCP Memory",				10, 1, 20480,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(13,	  "WX",		"Input Memory",				10, 1, 1024,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(15,	  "WY",		"Output Memory",			10, 1, 1024,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x21, "C",		"Control Bits",				10, 1, 0xFFFF,	addrBitAsBit		  ));
	AddSpace(New CSpace(0x25, "X",		"Input Bits",				10, 1, 0xFFFF,	addrBitAsBit		  ));
	AddSpace(New CSpace(0x27, "Y",		"Output Bits",				10, 1, 0xFFFF,	addrBitAsBit		  ));
	
	if( fBlock ) {
	
		AddSpace(New CSpace(0x22, "CP",		"Control Bits Packed",			10, 1, 0xFFFF,  addrBitAsByte		  ));
		AddSpace(New CSpace(0x26, "XP",		"Input Bits Packed",			10, 1, 0xFFFF,  addrBitAsByte		  ));
		AddSpace(New CSpace(0x28, "YP",		"Output Bits Packed",			10, 1, 0xFFFF,	addrBitAsByte		  ));
		}
/*	AddSpace(New CSpace(0x31, "LPV",	"Loop Process Variable",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x32, "LSP",	"Loop Setpoint",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x33, "LMN",	"Loop Output",				10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x34, "LMX",	"Loop Bias",				10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x35, "LERR",	"Loop Error",				10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x36, "LKC",	"Loop Gain",				10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x37, "LTD",	"Loop Rate",				10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x38, "LTI",	"Loop Reset",				10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x39, "LVF",	"Loop V Flags",				10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x3A, "LRSF",	"RAMP SOAK Flags",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x3B, "APV",	"Analog Alarm Process Variable",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x3C, "ASP",	"Analog Alarm Setpoint",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x3D, "AVF",	"Analog Alarm Flags",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(0x41, "LPVL",	"Loop Process Low Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x42, "LPVH",	"Loop Process High Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x43, "APVL",	"Analog Alarm Process Low Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x44, "APVH",	"Analog Alarm Process High Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x45, "LTS",	"Loop Sample Rate",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x46, "ATS",	"Analog Alarm Sample Rate",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x47, "LHA",	"Loop High Alarm Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x48, "LLA",	"Loop Low Alarm Limit",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x49, "LODA",	"Loop Orange Dev Alarm Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x4A, "LYDA",	"Loop Yellow Dev Alarm Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x4B, "LSPL",	"Loop Setpoint Low Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x4C, "LSPH",	"Loop Setpoint High Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x4D, "LCFH",	"MSW of Loop C Flags",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x4E, "LCFL",	"LSW of Loop C Flags",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(0x4F, "LHHA",	"Loop High High Alarm Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x50, "LLLA",	"Loop Low Low Alarm Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x51, "LRCA",	"Loop ROC Alarm Limit",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x52, "LADB",	"Loop Alarm Deadband",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x53, "AHA",	"Analog Alarm High Alarm Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x54, "ALA",	"Analog Alarm Low Alarm Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x55, "AODA",	"Analog Alarm Orange Dev Alarm Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x56, "AYDA",	"Analog Alarm Yellow Dev Alarm Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x57, "ASPL",	"Analog Alarm Setpoint Low Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x58, "ASPH",	"Analog Alarm Setpoint High Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x59, "ACFH",	"MSW of Analog Alarm C Flags",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x5A, "ACFL",	"LSW of Analog Alarm C Flags",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x5B, "AHHA",	"Analog Alarm High High Alarm Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x5C, "ALLA",	"Analog Alarm Low Low Alarm Limit",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x5D, "ARCA",	"Analog Alarm ROC Alarm Limit",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x5E, "AADB",	"Analog Alarm Alarm Deadband",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x5F, "AERR",	"Analog Alarm Error",			10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x60, "LRSN",	"Loop RAMP SOAK Step Number",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x61, "DXW",	"Discrete Input Word",			10, 1,8192,	addrWordAsWord, addrWordAsReal)); 
	AddSpace(New CSpace(0x62, "DYW",	"Discrete Output Word",			10, 1,8192,	addrWordAsWord, addrWordAsReal)); 
	AddSpace(New CSpace(0x63, "CRW",	"Control Relay Word",			10, 1,56320,	addrWordAsWord, addrWordAsReal)); 
	AddSpace(New CSpace(0x64, "LACK",	"Loop Alarm Alarm ACK Flags",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x65, "AACK",	"Analog Alarm Alarm Alarm ACK Flags",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x66, "LPET",	"Loop Peak Elasped Time Value",		10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x67, "APET",	"Analog Alarm Peak Elasped Time Value",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(0x68, "PPET",	"SF PGM Peak Elasped Time Value",	10, 1, 512,	addrWordAsWord, addrWordAsReal));
*/	}


//////////////////////////////////////////////////////////////////////////////////
//
// TI 500 Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTI500MasterTCPDeviceOptions, CUIItem);       

// Constructor

CTI500MasterTCPDeviceOptions::CTI500MasterTCPDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD( 52, 1), MAKEWORD(  168, 192)));

	m_Port   = 1505;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_fBlock = FALSE;
	}

// UI Managament

void CTI500MasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CTI500MasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_fBlock));
	
	return TRUE;
	}

// Meta Data Creation

void CTI500MasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddBoolean(Block);
	}


//////////////////////////////////////////////////////////////////////////
//
// TI 500 TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_TI500MasterTCPDriver(void)
{
	return New CTI500MasterTCPDriver;
	}

// Constructor

CTI500MasterTCPDriver::CTI500MasterTCPDriver(void)
{
	m_wID		= 0x407C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "TI 500 Series TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "TI 500 Series";
	}

// Binding Control

UINT CTI500MasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CTI500MasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CTI500MasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTI500MasterTCPDeviceOptions);
	}
 
// End of File
