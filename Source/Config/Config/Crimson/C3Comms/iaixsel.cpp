
#include "intern.hpp"

#include "iaixsel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IAI XSEL Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIaixselDeviceOptions, CUIItem);

// Constructor

CIaixselDeviceOptions::CIaixselDeviceOptions(void)
{
	}

// Download Support

BOOL CIaixselDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// IAI XSEL Comms Driver
//

// Instantiator

ICommsDriver *	Create_IaixselDriver(void)
{
	return New CIaixselDriver;
	}

// Constructor

CIaixselDriver::CIaixselDriver(void)
{
	m_wID		= 0x339D;

	m_uType		= driverMaster;

	m_Manufacturer	= "Intelligent Actuator";

	m_DriverName	= "XSEL";

	m_Version	= "1.10";

	m_ShortName	= "IAI";

	AddSpaces();
	}

// Binding Control

UINT	CIaixselDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CIaixselDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CIaixselDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIaixselDeviceOptions);
	}

// Address Management

BOOL	CIaixselDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CIaixselAddrDialog Dlg(*this, Addr, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL	CIaixselDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	UINT uTable = pSpace->m_uTable;

	UINT uParamValue = 0;

	UINT uParam = 0;

	BOOL fParam2 = FALSE;

	BOOL fParamError = FALSE;

	CString sPErr = "";

	if( uTable == AN ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	else {
		uParam = CheckParameters( pSpace );

		switch( uParam ) {

			case NOPARAM:
			case PNTNUM:
			case PRTNUM:
			case AXISPATTERN:
			case AXISNUM:
			case ADDCOMMBYTE:
			case PRGNUM:
			case USERSTRING:
				break;

			default:

				UINT uFind = Text.FindRev('(');

				UINT uLast = Text.Find(')', uFind);

				if( (uFind < NOTHING) && (uLast < NOTHING)) {

					if( uParam != AXISSCALAR && uParam != CLASSDEV ) {

						uParamValue = tatoi(Text.Mid(uFind+1));
						}

					else {
						TCHAR c = Text[uFind+1];

						if( c >= '0' && c <= '9' ) {
							
							uParamValue = c - '0';
							}
						else {
							if( c >= 'A' && c <= 'F' ) {
								
								uParamValue = c - '7';
								}
							else {
								if( c < 'a' && c > 'f' ) {
									
									uParamValue = 99;
									}
								else
									uParamValue = c - 'W';
								}
							}
						}

					switch( uParam ) {

						case PNTNUMAXIS:
							if( uParamValue > 8 ) {

								sPErr = "Axis (1) - (8)";

								fParamError = TRUE;
								}

							if( !uParamValue ) uParamValue = 1;
							break;

						case AXISPATNUM:
							if( uParamValue > 8 ) {

								sPErr = "Item (1) - (8)";

								fParamError = TRUE;
								}

							if( !uParamValue ) uParamValue = 1;

							break;

						case FLGPRGNUM:
						case VARPRGNUM:
							if( uParamValue > 31 ) {

								sPErr = "Prog No (0) - (31)";

								fParamError = TRUE;
								}
							break;

						case AXISSCALAR:
						case CLASSDEV:
							if( uParamValue > 15 ) {

								sPErr = uParam==7 ? "Scalar Type" : "Device Number";

								sPErr += " (0) - (9) (A) - (F)";

								fParamError = TRUE;
								}
							break;
						}

					if( fParamError ) {

						Error.Set( sPErr,
							0
							);

						return FALSE;
						}

					fParam2 = TRUE;

					Text.Delete(uFind, uLast-uFind+1);
					}

				else {
					Error.Set( " (nn) ",
						0
						);

					return FALSE;
					}
				break;
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Table = uTable;

		BOOL fModError = FALSE;

		switch( uTable ) {

			case C0B:
			case C0D:
				fModError = BOOL( Addr.a.m_Offset % 8 );
				break;

			case C0C:
				fModError = BOOL( (Addr.a.m_Offset-300) % 8 );
				break;
			}

		if( fModError ) {

			Error.Set( CString(IDS_ERROR_BADOFFSET),
				0
				);

			return FALSE;
			}

		if( fParam2 ) {

			switch ( uParam ) {

				case PNTNUMAXIS:
				case AXISPATNUM:
					if( uParamValue == 0 ) uParamValue = 1;
				case AXISSCALAR:
				case CLASSDEV:
					Addr.a.m_Extra = uParamValue;
					break;

				case FLGPRGNUM:
				case VARPRGNUM:
					Addr.a.m_Extra = uParamValue & 0xF;

					if( uParamValue > 15 ) Addr.a.m_Offset |= 0x8000;

					break;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CIaixselDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	UINT uOffset = Addr.a.m_Offset;
	UINT uExtra  = Addr.a.m_Extra;

	if( pSpace ) {

		UINT uParam = CheckParameters(pSpace);

		switch( uParam ) {

			case NOPARAM:
			case USERSTRING:
				Text.Printf( "%s",
					pSpace->m_Prefix
				);
				break;

			case PNTNUM:
			case AXISPATTERN:
			case AXISNUM:
			case ADDCOMMBYTE:
			case PRGNUM:
				Text.Printf( "%s%s",
					pSpace->m_Prefix,
					pSpace->GetValueAsText(uOffset)
					);
				break;

			case PRTNUM:
				Text.Printf( "%s%s",
					pSpace->m_Prefix,
					pSpace->GetValueAsText(uOffset)
					);
				break;


			case PNTNUMAXIS:
			case AXISPATNUM:
				Text.Printf( "%s%s(%1.1d)",
					pSpace->m_Prefix,
					pSpace->GetValueAsText(uOffset),
					uExtra
					);
				break;

			case AXISSCALAR:
			case CLASSDEV:
				Text.Printf( "%s%s(%1.1X)",
					pSpace->m_Prefix,
					pSpace->GetValueAsText(uOffset),
					uExtra
					);
				break;

			case FLGPRGNUM:
			case VARPRGNUM:
				Text.Printf( "%s%s(%2.2d)",
					pSpace->m_Prefix,
					pSpace->GetValueAsText(uOffset & 0x7FFF),
					(uOffset & 0x8000) ? uExtra+16 : uExtra
					);
				break;
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void	CIaixselDriver::AddSpaces(void)
{
	AddSpace( New CSpace(C01A,	"C201A",    "Version Code Inquiry - Model",		16,	0,	2,	LL ) );
	AddSpace( New CSpace(C01B,	"C201B",    "Version Code Inquiry - Unit",		16,	0,	2,	LL ) );
	AddSpace( New CSpace(C01C,	"C201C",    "Version Code Inquiry - Version",		16,	0,	2,	LL ) );
	AddSpace( New CSpace(C01D,	"C201D",    "Version Code Inquiry - Y/M/D",		16,	0,	2,	LL ) );
	AddSpace( New CSpace(C01E,	"C201E",    "Version Code Inquiry - H/M/S",		16,	0,	2,	LL ) );

	AddSpace( New CSpace(AN,	"C208",     "No. of Active Points",			10,	0x208,	2,	LL ) );

	AddSpace( New CSpace(C09A,	"C209A",    "Active Point Acceleration",		10,	1,	0xFFF,	LL ) );
	AddSpace( New CSpace(C09B,	"C209B",    "Active Point Deceleration",		10,	1,	0xFFF,	LL ) );
	AddSpace( New CSpace(C09C,	"C209C",    "Active Point Velocity",			10,	1,	0xFFF,	LL ) );
	AddSpace( New CSpace(C09D,	"C209D",    "Active Point Position",			10,	1,	0xFFF,	LL ) );
	AddSpace( New CSpace(C09E,	"C209E",    "Active Point Axis Pattern",		10,	1,	0xFFF,	LL ) );

	AddSpace( New CSpace(C0B,	"C20B",     "Input Port - Input Bytes",			10,	0,	0xFFF8,	YY ) );
	AddSpace( New CSpace(C0BB,	"C20BB",    "Input Port - Input Bits",			10,	0,	0xFFFF,	BB ) );

	AddSpace( New CSpace(C0C,	"C20C",     "Output Port - Output Bytes",		10,	300,	0xFFF8,	YY ) );
	AddSpace( New CSpace(CCB,	"C20CB",    "Output Port - Output Bits",		10,	300,	0xFFFF,	BB ) );

	AddSpace( New CSpace(C0D,	"C20D",     "Flag - Flag Bytes",			10,	0,	0x7FF8,	YY ) );
	AddSpace( New CSpace(C0DB,	"C20DB",    "Flag - Flag Bits",				10,	0,	0x7FFF,	BB ) );

	AddSpace( New CSpace(C0E,	"C20E",     "R/W Integer Variable",			10,	1,	1299,	LL ) );

	AddSpace( New CSpace(C0F,	"C20F",     "R/W Real Variable 0-1299",			10,	1,	1299,	RR ) );
	AddSpace( New CSpace(C0FA,	"C20F1",    "R/W Real Variable 1000 - 1999",		10,     0,	 999,	RR ) );

	AddSpace( New CSpace(C12A,	"C212A",    "Axes Status",				16,	0,	255,	YY ) );
	AddSpace( New CSpace(C12B,	"C212B",    "Axis Sensor Input Status",			16,	0,	255,	YY ) );
	AddSpace( New CSpace(C12C,	"C212C",    "Axes related error code",			16,	0,	255,	LL ) );
	AddSpace( New CSpace(C12D,	"C212D",    "Encoder Status",				16,	0,	255,	LL ) );
	AddSpace( New CSpace(C12E,	"C212E",    "Current Position",				16,	0,	255,	LL ) );

	AddSpace( New CSpace(C13A,	"C213A",    "Program Status - Status",			16,	1,	255,	LL ) );
	AddSpace( New CSpace(C13B,	"C213B",    "Program Status - Step",			16,	1,	255,	LL ) );
	AddSpace( New CSpace(C13C,	"C213C",    "Program Status - Error",			16,	1,	255,	LL ) );
	AddSpace( New CSpace(C13D,	"C213D",    "Program Status - Error Step",		16,	1,	255,	LL ) );

	AddSpace( New CSpace(AN,	"C215A",    "System Status - Mode",			16,	0x152,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C215B",    "System Status - Error High",		16,	0x153,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C215C",    "System Status - Error New",		16,	0x154,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C215D",    "System Status - Byte 1",			16,	0x155,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C215E",    "System Status - Byte 2",			16,	0x156,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C215F",    "System Status - Byte 3",			16,	0x157,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C215G",    "System Status - Byte 4",			16,	0x158,	0,	LL ) );

	AddSpace( New CSpace(AN,	"C232F",    "Servo OFF <Axis Pattern>",			16,	0x322,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C232N",    "Servo ON <Axis Pattern>",			16,	0x323,	0,	LL ) );

	AddSpace( New CSpace(AN,	"C233C",    "EXECUTE Homing <Axis Pattern>",		16,	0x332,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C233A",    "   Homing End Search Velocity",		16,	0x333,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C233B",    "   Homing Creep Velocity",			16,	0x334,	0,	LL ) );

	AddSpace( New CSpace(AN,	"C234E",    "EXECUTE Absolute Move <Axis Pattern>",	16,	0x342,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C234A",    "   Absolute Move Acceleration",		16,	0x343,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C234B",    "   Absolute Move Deceleration",		16,	0x344,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C234C",    "   Absolute Move Velocity",		16,	0x345,	0,	LL ) );
	AddSpace( New CSpace(C34,	"C234D",    "   Absolute Move Axis Position",		10,	1,	8,	LL ) );

	AddSpace( New CSpace(AN,	"C235E",    "EXECUTE Relative Move <Axis Pattern>",	16,	0x352,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C235A",    "   Relative Move Acceleration",		16,	0x353,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C235B",    "   Relative Move Deceleration",		16,	0x354,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C235C",    "   Relative Move Velocity",		16,	0x355,	0,	LL ) );
	AddSpace( New CSpace(C35,	"C235D",    "   Relative Move Axis Position",		10,	1,	8,	LL ) );

	AddSpace( New CSpace(AN,	"C236F",    "EXECUTE Jog <Axis Pattern>",		16,	0x362,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C236A",    "   Jog Acceleration",			16,	0x363,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C236B",    "   Jog Deceleration",			16,	0x364,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C236C",    "   Jog Velocity",				16,	0x365,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C236D",    "   Jog Position",				16,	0x366,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C236E",    "   Jog Direction <0/1 = Pos/Neg>",		16,	0x367,	0,	LL ) );

	AddSpace( New CSpace(AN,	"C237E",    "EXECUTE Move to Point <Axis Pattern>",	16,	0x372,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C237A",    "   Move to Point Acceleration",		16,	0x373,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C237B",    "   Move to Point Deceleration",		16,	0x374,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C237C",    "   Move to Point Velocity",		16,	0x375,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C237D",    "   Move to Point - Point Number",		16,	0x376,	0,	LL ) );

	AddSpace( New CSpace(C38,	"C238A",    "Stop and Cancel <Axis Pattern>",		16,	0,	255,	LL ) );

	AddSpace( New CSpace(AN,	"C244A",    "   Write 1 Point - Acceleration",		16,	0x442,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C244B",    "   Write 1 Point - Deceleration",		16,	0x443,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C244C",    "   Write 1 Point - Velocity",		16,	0x444,	0,	LL ) );
	AddSpace( New CSpace(C44D,	"C244D",    "   Write 1 Point - Axis Positions",	10,	1,	8,	LL ) );
// C244G and C244H are a possible 3rd way of writing point data, but suppressed for now
// Code to support them are in the dld
//	AddSpace( New CSpace(AN,	"C244G",    "   1 Point Write - Point Number",		16,	0x446,	0,	LL ) );
//	AddSpace( New CSpace(AN,	"C244H",    "Send 244A,B,C,D,G <Axis Pattern>",		10,	0x447,	0,	LL ) );
	AddSpace( New CSpace(C44E,	"C244E",    "Write 1 Point <Point Number>",		16,	1,	0xFF,	LL ) );
	AddSpace( New CSpace(AN,	"C244F",    "Write 1 Point <(Axis Pattern<<16),Point>",	10,	0x445,	0,	LL ) );
	AddSpace( New CSpace(C46,	"C246A",    "Clear Point Data <Count to clear>",	10,	1,	0xFFF,	LL ) );

	AddSpace( New CSpace(AN,	"C252",     "Alarm Reset",				10,	0x252,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C253",     "Execute Program",				10,	0x253,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C254",     "Stop Program",				10,	0x254,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C255",     "Hold Program",				10,	0x255,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C256",     "Execute Program 1 Step",			10,	0x256,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C257",     "Resume Program Execution",			10,	0x257,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C25B",     "Software Reset",				10,	0x25B,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C25C",     "Drive Power Recovery",			16,	0x25C,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C25E",     "Hold Release",				16,	0x25E,	0,	LL ) );

	AddSpace( New CSpace(C62,	"C262A",    "Speed Change <Speed>",			16,	0,	255,	LL ) );

// added Apr 07, K-O, X,T
	AddSpace( New CSpace(CRQAT,	"C2A1K",    "Input Scalar Type for Axis Request",	16,	0,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1L",    "Axis Work Coordinate System",		16,	0xA1C,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1M",    "Axis Tool Coordinate System",		16,	0xA1D,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1N",    "Axis Common Status",			16,	0xA1E,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1O",    "Axis Pattern Response",			16,	0xA1F,	0,	LL ) );
	AddSpace( New CSpace(CRSPST,	"C2A1P",    "Status for Selected Axis",			10,	1,	8,	LL ) );
	AddSpace( New CSpace(CRSPIN,	"C2A1Q",    "Sensor Input Status for Selected Axis",	10,	1,	8,	LL ) );
	AddSpace( New CSpace(CRSPEC,	"C2A1R",    "Relation Error Code for Selected Axis",	10,	1,	8,	LL ) );
	AddSpace( New CSpace(CRSPEN,	"C2A1S",    "Encoder Status for Selected Axis",		10,	1,	8,	LL ) );
	AddSpace( New CSpace(CRSPPL,	"C2A1T",    "Present Location for Selected Axis",	10,	1,	8,	LL ) );

	AddSpace( New CSpace(AN,	"C2D4F",    "EXECUTE Absolute Move <Axis Pattern>",	16,	0xD4F,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D4A",    "   Absolute Move - Acceleration",		16,	0xD4A,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D4B",    "   Absolute Move - Deceleration",		16,	0xD4B,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D4C",    "   Absolute Move - Speed",			16,	0xD4C,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D4D",    "   Absolute Move - Positioning Type",	16,	0xD4D,	0,	LL ) );
	AddSpace( New CSpace(CD4E,	"C2D4E",    "   Absolute Move - Axis Coordinate Data",	16,	1,	8,	LL ) );

	AddSpace( New CSpace(AN,	"C2D5F",    "EXECUTE Relative Move <Axis Pattern>",	16,	0xD5F,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D5A",    "   Relative Move - Acceleration",		16,	0xD5A,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D5B",    "   Relative Move - Deceleration",		16,	0xD5B,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D5C",    "   Relative Move - Speed",			16,	0xD5C,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D5D",    "   Relative Move - Positioning Type",	16,	0xD5D,	0,	LL ) );
	AddSpace( New CSpace(CD5E,	"C2D5E",    "   Relative Move - Axis Coordinate Data",	16,	1,	8,	LL ) );

	AddSpace( New CSpace(AN,	"C2D6F",    "EXECUTE Move To Point <Axis Pattern>",	16,	0xD6F,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D6A",    "   Move To Point - Acceleration",		16,	0xD6A,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D6B",    "   Move To Point - Deceleration",		16,	0xD6B,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D6C",    "   Move To Point - Speed",			16,	0xD6C,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D6D",    "   Move To Point - Positioning Type",	16,	0xD6D,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2D6E",    "   Move To Point - Point Number",		16,	0xD6E,	0,	LL ) );

//	AddSpace( New CSpace(CSND,	"SEND",     "Custom User String Send",			10,	0,	19,	LL ) );
//	AddSpace( New CSpace(CRCV,	"RESP",     "Custom User String Response",		10,	0,	19,	LL ) );

	AddSpace( New CSpace(AN,	"C200",     "Test Call",				10,	0x200,	0,	LL ) );
	AddSpace( New CSpace(AN,	"CERR",     "Latest Error",				10,	0xE02,	0,	LL ) );
	AddSpace( New CSpace(CA1,	"C2A1A",    "1 Axis Status Request - Axis Number/Type",	16,	1,	8,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1B",    "1 Axis Work Coordinate System",		16,	0xA13,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1C",    "1 Axis Tool Coordinate System",		16,	0xA14,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1D",    "1 Axis Common Status",			16,	0xA15,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1E",    "1 Axis Pattern Response",			16,	0xA16,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1F",    "1 Axis Status",				16,	0xA17,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1G",    "1 Axis Sensor Input Status",		16,	0xA18,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1H",    "1 Axis Relation Error Code",		16,	0xA19,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1I",    "1 Axis Encoder Status",			16,	0xA1A,	0,	LL ) );
	AddSpace( New CSpace(AN,	"C2A1J",    "1 Axis Present Location",			16,	0xA1B,	0,	LL ) );
	}

// Helpers

UINT CIaixselDriver::CheckParameters(CSpace * pSpace)
{
	if( pSpace ) {

		switch( pSpace->m_uTable ) {

			case C09A:
			case C09B:
			case C09C:
			case C09E:
			case C46:
				return PNTNUM; // Point Number

			case C09D:
				return PNTNUMAXIS; // Point Number & Axis Request

			case C0B:
			case C0C:
			case CCB:
			case C0BB:
				return PRTNUM; // Port Number

			case C0D:
			case C0DB:
				return FLGPRGNUM; // Flag Number & Program Number

			case C0E:
			case C0F:
			case C0FA:
				return VARPRGNUM; // Variable Number & Program Number

			case C12A:
			case C12B:
			case C12C:
			case C12D:
			case C62:
			case C44E:
				return AXISPATTERN; // Axis Pattern

			case CA1:
				return AXISSCALAR; // Axis Number & Scalar Location

			case C12E:
				return AXISPATNUM; // Axis Pattern & Axis Number

			case C34:
			case C35:
			case C44D:
			case CD4E:
			case CD5E:
			case CRSPST:
			case CRSPIN:
			case CRSPEC:
			case CRSPEN:
			case CRSPPL:
				return AXISNUM; // Axis Number

			case C01A:
			case C01B:
			case C01C:
			case C01D:
			case C01E:
				return CLASSDEV; // Unit Classification & Device Number

			case C38:
				return PRGNUM; // Additional Command Byte

			case C13A:
			case C13B:
			case C13C:
			case C13D:
				return PRGNUM; // Program Number

			case CSND:
			case CRCV:
				return USERSTRING; // User String

			default: // no parameter
				break;
			}
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Iaixsel Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CIaixselAddrDialog, CStdAddrDialog);

// Constructor

CIaixselAddrDialog::CIaixselAddrDialog(CIaixselDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "IaixselElementDlg";
	}

// Overridables

void CIaixselAddrDialog::SetAddressText(CString Text)
{
	BOOL fEnable = FALSE;

	UINT uClear = 0;

	CString sCaption1 = "";

	CString sCaption2 = "";

	CString sParam2 = "";

	UINT uCaption = GetParameters();

	if( uCaption ) {

		fEnable = !Text.IsEmpty();

		uClear = 1;

		switch( uCaption ) {

			case PNTNUMAXIS:
				sCaption2 = "Axis Number";
				uClear = 0;
			case PNTNUM:
				sCaption1 = "Point Number";
				break;

			case PRTNUM:
				sCaption1 = "Port Number";
				break;

			case FLGPRGNUM:
				sCaption1 = "Flag Number";
				sCaption2 = "Program Number";
				uClear = 0;
				break;

			case VARPRGNUM:
				sCaption1 = "Variable Number";
				sCaption2 = "Program Number";
				uClear = 0;
				break;

			case AXISPATTERN:
				sCaption1 = "Axis Pattern";
				break;

			case AXISPATNUM:
				sCaption1 = "Axis Pattern";
				sCaption2 = "Axis Number";
				uClear = 0;
				break;

			case AXISSCALAR:
				sCaption2 = "Scalar Type";
				uClear = 0;
			case AXISNUM:
				sCaption1 = "Axis Number";
				break;

			case CLASSDEV:
				sCaption1 = "Unit Class";
				sCaption2 = "Device Number";
				uClear = 0;
				break;

			case ADDCOMMBYTE:
				sCaption1 = "Command Byte";
				break;

			case PRGNUM:
				sCaption1 = "Program Number";
				break;

			case USERSTRING:
				uClear = 2;
				break;
			}

		GetDlgItem(2010).SetWindowText( sCaption1 );

		GetDlgItem(2011).SetWindowText( sCaption2 );
		}

	else uClear = 2;

	if( fEnable ) {

		GetDlgItem(2002).SetWindowText(Text);

		if( sCaption2[0] ) {

			UINT uFind = Text.Find('(');

			UINT uLast = Text.Find(')');

			if( uFind < NOTHING && uLast < NOTHING ) {

				GetDlgItem(2002).SetWindowText(Text.Left(uFind));

				GetDlgItem(2004).SetWindowText("(");

				sParam2 = Text.Mid(uFind+1, uLast-uFind-1);

				if( uCaption == 2 || uCaption == 8 ) {

					if( sParam2[0] == '0' || sParam2[0] > '8' ) sParam2 = "1";
					}

				GetDlgItem(2005).SetWindowText(sParam2);

				GetDlgItem(2006).SetWindowText(")");
				}

			else uClear = 1;
			}
		}

	else uClear = 2;

	ShowDetails();

	if( uClear > 1 ) {

		GetDlgItem(2002).SetWindowText("");

		GetDlgItem(2010).SetWindowText("");
		}

	if( uClear ) {

		GetDlgItem(2004).SetWindowText("");

		GetDlgItem(2005).SetWindowText("");

		GetDlgItem(2006).SetWindowText("");

		GetDlgItem(2011).SetWindowText("");
		}

	GetDlgItem(2002).EnableWindow(uClear < 2);

	GetDlgItem(2010).EnableWindow(uClear < 2);

	GetDlgItem(2004).EnableWindow(!uClear);

	GetDlgItem(2005).EnableWindow(!uClear);

	GetDlgItem(2006).EnableWindow(!uClear);

	GetDlgItem(2011).EnableWindow(!uClear);
	}

CString CIaixselAddrDialog::GetAddressText(void)
{
	CString Text = "";

	switch( GetParameters() ) {

		case PNTNUM:
		case PRTNUM:
		case AXISPATTERN:
		case AXISNUM:
		case ADDCOMMBYTE:
		case PRGNUM:
			Text += GetDlgItem(2002).GetWindowText();

			break;

		case PNTNUMAXIS:
		case FLGPRGNUM:
		case VARPRGNUM:
		case AXISSCALAR:
		case AXISPATNUM:
		case CLASSDEV:
			Text += GetDlgItem(2002).GetWindowText();

			Text += "(";

			Text += GetDlgItem(2005).GetWindowText();

			Text += ")";

			break;
		}

	return Text;
	}

// Helpers

UINT CIaixselAddrDialog::GetParameters(void)
{
	if( m_pSpace ) {

		switch( m_pSpace->m_uTable ) {

			case C09A:
			case C09B:
			case C09C:
			case C09E:
			case C46:
				return PNTNUM; // Point Number

			case C09D:
				return PNTNUMAXIS; // Point Number & Axis Request

			case C0B:
			case C0C:
			case CCB:
			case C0BB:
				return PRTNUM; // Port Number

			case C0D:
			case C0DB:
				return FLGPRGNUM; // Flag Number & Program Number

			case C0E:
			case C0F:
			case C0FA:
				return VARPRGNUM; // Variable Number & Program Number

			case C12A:
			case C12B:
			case C12C:
			case C12D:
			case C62:
			case C44E:
				return AXISPATTERN; // Axis Pattern

			case CA1:
				return AXISSCALAR; // Axis Number & Scalar Location

			case C12E:
				return AXISPATNUM; // Axis Pattern & Item Position

			case C34:
			case C35:
			case C44D:
			case CD4E:
			case CD5E:
			case CRSPST:
			case CRSPIN:
			case CRSPEC:
			case CRSPEN:
			case CRSPPL:
				return AXISNUM; // Axis Number

			case C01A:
			case C01B:
			case C01C:
			case C01D:
			case C01E:
				return CLASSDEV; // Unit Classification & Device Number

			case C38:
				return ADDCOMMBYTE; // Additional Command Byte

			case C13A:
			case C13B:
			case C13C:
			case C13D:
				return PRGNUM; // Program Number

			case CSND:
			case CRCV:
				return USERSTRING; // User String


			default: // no parameter
				break;
			}
		}

	return 0;
	}

void CIaixselAddrDialog::ShowDetails(void)
{
	if( !m_pSpace ) return;

	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	if( m_pSpace->m_uTable < addrNamed ) {

		CAddress Addr;

		UINT uParam = GetParameters();

		Addr.a.m_Type = GetTypeCode();

		m_pSpace->GetMinimum(Addr);

		switch( uParam ) {

			case PNTNUMAXIS:
			case AXISPATNUM:
				Addr.a.m_Extra = 1;
				break;

			default:
				Addr.a.m_Extra = 0;
				break;
			}

		m_pDriver->DoExpandAddress(Min, m_pConfig, Addr);

		m_pSpace->GetMaximum(Addr);

		switch( uParam ) {

			case PNTNUMAXIS:
			case AXISPATNUM:
				Addr.a.m_Extra = 8;
				break;;

			case FLGPRGNUM:
			case VARPRGNUM:
				Addr.a.m_Offset |= 0x8000;
			case AXISSCALAR:
			case CLASSDEV:
				Addr.a.m_Extra = 15;
				break;

			default:
				Addr.a.m_Extra = 0;
				break;
			}

		m_pDriver->DoExpandAddress(Max, m_pConfig, Addr);

		Rad = m_pSpace->GetRadixAsText();

		if( uParam == USERSTRING ) {

			Min = "1  Char";
			Max = "80 Char.";
			Rad = "";
			GetDlgItem(3002).SetWindowText("String");
			GetDlgItem(3002).EnableWindow(TRUE);
			}
		}

	GetDlgItem(3004).SetWindowText(Min);

	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

// End of File
