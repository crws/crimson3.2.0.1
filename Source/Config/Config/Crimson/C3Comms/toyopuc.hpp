
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TOYODAPUC_HPP
#define	INCLUDE_TOYODAPUC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC TCP/IP Device Options
//

class CToyodaDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CToyodaDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Drop;
		UINT	m_Program;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC TCP/IP Device Options
//

class CToyodaTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CToyodaTCPDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_IPAddr;
		UINT	m_Program;
		UINT	m_Port;
		UINT	m_Unit;
		UINT	m_Keep;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Serial Driver
//

class CToyodaPUCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CToyodaPUCDriver(void);

		// Configuration

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC TCP/IP Master Driver
//

class CToyodaTCPDriver : public CToyodaPUCDriver
{
	public:
		// Constructor

		CToyodaTCPDriver(void);

		// Destructor
		~CToyodaTCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration

		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Address Selection
//

class CToyodaPUCAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CToyodaPUCAddrDialog(CToyodaPUCDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Message Map
		AfxDeclareMessageMap();

		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnTypeChange(UINT uID, CWnd &Wnd);
		void	LoadType(void);
		void	ShowAddress(CAddress Addr);
		void	ShowDetails(void);
		CString	GetAddressText(void);

		// Helper
		void	DoCheck(BOOL fCheck);
		void	DoProgram(CString Program);

		// Data Members
		BOOL	m_fEnProgram;
		UINT	m_uType;
	};

// End of File

#endif
