
#include "intern.hpp"

#include "dnets.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CDeviceNetSlaveDriverOptions, CUIItem);

// Constructor

CDeviceNetSlaveDriverOptions::CDeviceNetSlaveDriverOptions(void)
{
	m_Mac      = 1;

	m_fBit     = FALSE;

	m_BitRecv  = 1;
	
	m_BitSend  = 8;
	
	m_fPoll    = TRUE;
	
	m_PollRecv = 16;

	m_PollSend = 16;

	m_fData    = FALSE;
	
	m_DataRecv = 32;

	m_DataSend = 32;

	m_fIntel   = FALSE;

	m_fExport  = FALSE;

	m_fDirty   = FALSE;

	m_pStream  = NULL;
	}

// Persistance

void CDeviceNetSlaveDriverOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	if( m_fExport && !m_Profile.IsEmpty() ) {

		if( m_fDirty ) {
		
			CreateEDSFile();

			m_fDirty = FALSE;
			}
		}
	}

// UI Managament

void CDeviceNetSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Bit" ) {

			pWnd->EnableUI("BitRecv", m_fBit);

			pWnd->EnableUI("BitSend", m_fBit);
			}

		if( Tag.IsEmpty() || Tag == "Poll" ) {

			pWnd->EnableUI("PollRecv", m_fPoll);

			pWnd->EnableUI("PollSend", m_fPoll);
			}

		if( Tag.IsEmpty() || Tag == "Data" ) {

			pWnd->EnableUI("DataRecv", m_fData);

			pWnd->EnableUI("DataSend", m_fData);
			}

		if( Tag.IsEmpty() || Tag == "Export" ) {

			pWnd->EnableUI("Profile", m_fExport);
			}

		if( m_fExport && !m_fDirty ) {
		
			if( Tag != "Mac" && Tag != "Intel" ) {

				m_fDirty = TRUE;

				return;
				}
			}
		}
	}

// Download Support

BOOL CDeviceNetSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mac));

	Init.AddByte(BYTE(m_fBit));

	Init.AddByte(BYTE(m_BitRecv));

	Init.AddByte(BYTE(m_BitSend));

	Init.AddByte(BYTE(m_fPoll));

	Init.AddWord(WORD(m_PollRecv));

	Init.AddWord(WORD(m_PollSend));

	Init.AddByte(BYTE(m_fData));

	Init.AddWord(WORD(m_DataRecv));

	Init.AddWord(WORD(m_DataSend));

	Init.AddByte(BYTE(m_fIntel));

	return TRUE;
	}

// Meta Data Creation

void CDeviceNetSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Mac);

	Meta_AddBoolean(Bit);
	
	Meta_AddInteger(BitRecv);
	
	Meta_AddInteger(BitSend);
	
	Meta_AddBoolean(Poll);
	
	Meta_AddInteger(PollRecv);

	Meta_AddInteger(PollSend);
	
	Meta_AddBoolean(Data);
	
	Meta_AddInteger(DataRecv);

	Meta_AddInteger(DataSend);

	Meta_AddBoolean(Intel);

	Meta_AddString(Profile);

	Meta_AddBoolean(Export);
	}

// EDS File

void CDeviceNetSlaveDriverOptions::CreateEDSFile(void)
{
	CTextStreamMemory Stream;

	if( Stream.OpenSave() ) {

		m_pStream = &Stream;

		afxThread->SetWaitMode(TRUE);
		
		WriteEDSHead();

		WriteEDSFile();

		WriteEDSDev ();

		WriteEDSInfo();

		WriteEDSAssy();

		Stream.SaveToFile(m_Profile, saveRaw);

		afxThread->SetWaitMode(FALSE);
		}

	m_pStream = NULL;
	}

void CDeviceNetSlaveDriverOptions::WriteEDSHead(void)
{
	CString Text;
	
	Text = CPrintf(	TEXT("\r\n")
			TEXT("$ Red Lion Controls %s EDS www.redlion.net\r\n")
			TEXT("\r\n"),
			L"Crimson 3.2"
			);

	WriteFile(Text);
	}

void CDeviceNetSlaveDriverOptions::WriteEDSFile(void)
{
	CString Text;
	
	Text = CPrintf(	TEXT("[File]\r\n")
			TEXT("\tDescText  \t= \"%s\";\r\n")
			TEXT("\tCreateDate\t= 01-01-2013;\r\n")
			TEXT("\tCreateTime\t= 12:00:00;\r\n")
			TEXT("\tModDate   \t= 01-01-2013;\r\n")
			TEXT("\tModTime   \t= 12:00:00;\r\n")
			TEXT("\tRevision  \t= 1.1;\r\n")
			TEXT("\r\n"),
		        L"Crimson 3.2"
	);
	
	WriteFile(Text);
	}

void CDeviceNetSlaveDriverOptions::WriteEDSDev(void)
{
	CString Text;
	
	Text = CPrintf(	TEXT("[Device]\r\n")
			TEXT("\tVendCode   \t= 176;\r\n")
			TEXT("\tVendName   \t= \"Red Lion Controls\";\r\n")
			TEXT("\tProdType   \t= 0x2B;\r\n")
			TEXT("\tProdTypeStr\t= \"Generic\";\r\n")
			TEXT("\tProdCode   \t= 0x100;\r\n")
			TEXT("\tMajRev     \t= 1;\r\n")
			TEXT("\tMinRev     \t= 1;\r\n")
			TEXT("\tProdName   \t= \"%s-SERVER\";\r\n")
			TEXT("\tCatalog    \t= \"\";\r\n")
			TEXT("\r\n"),
			L"Crimson 3.2"
			);
	
	WriteFile(Text);
	}

void CDeviceNetSlaveDriverOptions::WriteEDSInfo(void)
{
	WriteFile(TEXT("[IO_Info]\r\n"));

	WORD wDefault = 0;

	wDefault |= (m_fPoll ? 0x0001 << 0 : 0x0000);
	
	wDefault |= (m_fBit  ? 0x0001 << 1 : 0x0000);

	WriteFile(CPrintf(TEXT("\tDefault\t\t= 0x%4.4X;\r\n"), wDefault));

	if( m_fPoll ) {

		CString Text = CPrintf(	TEXT("\tPollInfo\t= 0x%4.4X,\r\n")
					TEXT("\t\t\t  %d,\r\n")
					TEXT("\t\t\t  %d;\r\n"),
					wDefault, 
					m_fBit ? 2 : 1,
					m_fBit ? 2 : 1
					);

		WriteFile(Text);
		}

	if( m_fBit ) {
		
		CString Text = CPrintf( TEXT("\tStrobeInfo\t= 0x%4.4X,\r\n")
					TEXT("\t\t\t  1,\r\n")
					TEXT("\t\t\t  1;\r\n"),
					wDefault
					);
		WriteFile(Text);
		}

	if( m_fBit ) {

		WriteIO( TEXT("Input"), 
			 1, 
			 m_BitSend, 
			 0, 
			 wDefault, 
			 TEXT("Bit-Strobe Response"), 
			 TEXT("20 04 24 66 30 03")
			 );
		}

	if( m_fPoll ) {

		WriteIO( TEXT("Input"), 
			 m_fBit ? 2 : 1, 
			 m_PollSend, 
			 0, 
			 wDefault, 
			 TEXT("Polled Response"), 
			 TEXT("20 04 24 68 30 03")
			 );
		}

	if( m_fBit ) {

		WriteIO( TEXT("Output"), 
			 1, 
			 m_BitRecv, 
			 1, 
			 wDefault, 
			 TEXT("Bit-Strobe Command"), 
			 TEXT("20 04 24 67 30 03")
			 );
		}
		
	if( m_fPoll ) {

		WriteIO( TEXT("Output"), 
			 m_fBit ? 2 : 1, 
			 m_PollRecv, 
			 0, 
			 wDefault, 
			 TEXT("Polled Command"), 
			 TEXT("20 04 24 69 30 03")
			 );
		}
	}

void CDeviceNetSlaveDriverOptions::WriteEDSAssy(void) 
{
	WriteFile(TEXT("\r\n[Assembly]\r\n\tRevision\t= 2;\r\n"));
	
	WriteAssy(1, TEXT("Data Read"),           TEXT("20 04 24 64 30 03"), m_DataSend); 

	WriteAssy(2, TEXT("Data Write"),          TEXT("20 04 24 65 30 03"), m_DataRecv); 

	WriteAssy(3, TEXT("Bit-Strobe Response"), TEXT("20 04 24 66 30 03"), m_BitSend); 

	WriteAssy(4, TEXT("Bit-Strobe Command"),  TEXT("20 04 24 67 30 03"), m_BitRecv); 

	WriteAssy(5, TEXT("Polled Response"),     TEXT("20 04 24 68 30 03"), m_PollSend); 

	WriteAssy(6, TEXT("Polled Command"),      TEXT("20 04 24 69 30 03"), m_PollRecv); 
	}

void CDeviceNetSlaveDriverOptions::WriteIO(CString p, UINT i, UINT s, UINT b, UINT m, CString n, CString t)
{
	CString Text;
	
	Text = CPrintf(	TEXT("\r\n")
			TEXT("\t%-12.12s\t= %d,\r\n")
			TEXT("\t\t\t  %d,\r\n")
			TEXT("\t\t\t  0x%4.4X,\r\n")
			TEXT("\t\t\t  \"%s\",\r\n")
			TEXT("\t\t\t  6,\r\n")
			TEXT("\t\t\t  \"%s\",\r\n")
			TEXT("\t\t\t  \"%s\";\r\n"),
			CPrintf(TEXT("%s%d"), p, i),
			s,
			b,
			m,
			n,
			t, 
			n
			);

	WriteFile(Text);
	}

void CDeviceNetSlaveDriverOptions::WriteAssy(UINT i, CString d, CString p, UINT s) 
{
	CString Text;

	Text = CPrintf( TEXT("\r\n")
		        TEXT("\t%-12.12s\t= \"%s\",\r\n")
		        TEXT("\t\t\t  \"%s\",\r\n")
		        TEXT("\t\t\t  %d,\r\n")
		        TEXT("\t\t\t  0,\r\n")
		        TEXT("\t\t\t  ,,\r\n")
		        TEXT("\t\t\t  %d,;\r\n"),
		        CPrintf(TEXT("Assem%d"), i),
			d,
		        p, 
		        s,
		        s
		        );

	WriteFile(Text);
	}

BOOL CDeviceNetSlaveDriverOptions::WriteFile(PCTXT pText)
{
	return m_pStream->PutLine(pText);
	}

BOOL CDeviceNetSlaveDriverOptions::WriteFile(CString Text)
{
	return m_pStream->PutLine(Text);
	}

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave 1.x
//

// Instantiator

ICommsDriver * Create_DeviceNetSlaveDriver(void)
{
	return New CDeviceNetSlave;
	}

// Constructor

CDeviceNetSlave::CDeviceNetSlave(void)
{
	m_wID		= 0x3408;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "DeviceNet";
	
	m_DriverName	= "Predefined Group 2 Server";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Group 2 Server";

	m_DevRoot	= "DEV";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Configuration

CLASS CDeviceNetSlave::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDeviceNetSlaveDriverOptions);
	}

// Binding Control

UINT CDeviceNetSlave::GetBinding(void)
{
	return bindDeviceNet;
	}

void CDeviceNetSlave::GetBindInfo(CBindInfo &Info)
{
	CBindDevNet &DevNet = (CBindDevNet &) Info;

	DevNet.m_BaudRate = 125000;

	DevNet.m_Mac      = 1;
	}

// Implementation

void CDeviceNetSlave::AddSpaces(void)
{
	AddSpace(New CSpace(103, "BitC",  "I/O Bit Strobe Command Data",  10, 0,     0, addrBitAsBit,   addrBitAsBit  ));

	AddSpace(New CSpace(102, "BitR",  "I/O Bit Strobe Response Data", 10, 0,     3, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(105, "PollC", "I/O Polled Command Data",      10, 0, 65535, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(104, "PollR", "I/O Polled Response Data",     10, 0, 65535, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(101, "IOC",   "I/O Consumed Data",            10, 0, 65535, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(100, "IOP",   "I/O Produced Data",            10, 0, 65535, addrWordAsWord, addrWordAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave 2.x
//

// Instantiator

ICommsDriver * Create_DeviceNetSlave2Driver(void)
{
	return New CDeviceNetSlave2;
	}

// Constructor

CDeviceNetSlave2::CDeviceNetSlave2(void)
{
	m_wID		= 0x4077;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "DeviceNet";
	
	m_DriverName	= "Predefined Group 2 Server";
	
	m_Version	= "2.01";
	
	m_ShortName	= "Group 2 Server";

	m_DevRoot	= "DEV";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Configuration

CLASS CDeviceNetSlave2::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDeviceNetSlaveDriverOptions);
	}

// Binding Control

UINT CDeviceNetSlave2::GetBinding(void)
{
	return bindDeviceNet;
	}

void CDeviceNetSlave2::GetBindInfo(CBindInfo &Info)
{
	CBindDevNet &DevNet = (CBindDevNet &) Info;

	DevNet.m_BaudRate = 125000;

	DevNet.m_Mac      = 1;
	}

// Implementation

void CDeviceNetSlave2::AddSpaces(void)
{
	AddSpace(New CSpace(103, "BitC",  "I/O Bit Strobe Command Data",  10, 0,     0, addrBitAsBit,   addrBitAsBit  ));

	AddSpace(New CSpace(102, "BitR",  "I/O Bit Strobe Response Data", 10, 0,     3, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(105, "PollC", "I/O Polled Command Data",      10, 0, 65535, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(104, "PollR", "I/O Polled Response Data",     10, 0, 65535, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(101, "IOC",   "I/O Consumed Data",            10, 0, 65535, addrWordAsWord, addrWordAsLong));

	AddSpace(New CSpace(100, "IOP",   "I/O Produced Data",            10, 0, 65535, addrWordAsWord, addrWordAsLong));
	}

// End of File
