
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YLEGEND_HPP
	
#define	INCLUDE_YLEGEND_HPP

#define	SPACE_TXTS 153

//////////////////////////////////////////////////////////////////////////
//
// Yaskasw Legend Driver
//

class CYaskawaLegendDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaLegendDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);
		
	protected:
		
		// Implementation
		void	AddSpaces(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskasw Universal SMC Driver
//

class CYaskawaUnivSMCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaUnivSMCDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);
		
	protected:
		
		// Implementation
		void	AddUnivSpaces(void);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa TCP/IP Master Device Options
//

class CYaskawaTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_DefHandle; // obsoleted July 19 2007
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa TCP/IP Master Driver
//

class CYaskawaTCPDriver : public CYaskawaLegendDriver
{
	public:
		// Constructor
		CYaskawaTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Universal SMC TCP/IP Master Driver
//

class CYaskawaSMCTCPDriver : public CYaskawaUnivSMCDriver
{
	public:
		// Constructor
		CYaskawaSMCTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

// End of File

#endif
