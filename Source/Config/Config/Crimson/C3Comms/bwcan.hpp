//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BWCAN_HPP
	
#define	INCLUDE_BWCAN_HPP

#include "r29id.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Boulder Wind Power 29-bit Identifier Entry True Raw CAN Driver Options
//

class CBoulderWindPowerRawCANDriverOptions : public CRawCANDriverOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBoulderWindPowerRawCANDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
	};

//////////////////////////////////////////////////////////////////////////
//
// Boulder Wind Power 29-bit Identifier Entry True Raw CAN Driver
//

class CBoulderWindPowerRawCANDriver : public CCANRawDriver
{
	public:
		// Constructor
		CBoulderWindPowerRawCANDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
       	};

// End of File

#endif
