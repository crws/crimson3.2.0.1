
#include "intern.hpp"

#include "emroc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonRocMasterDriverOptions, CUIItem);       

// Constructor

CEmersonRocMasterDriverOptions::CEmersonRocMasterDriverOptions(void)
{
	m_Group  = 0;

	m_Unit   = 1;
	}

// Download Support

BOOL CEmersonRocMasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Group));
	Init.AddByte(BYTE(m_Unit));
	
	return TRUE;
	}

// Meta Data Creation

void CEmersonRocMasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	}


//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonRocMasterDeviceOptions, CUIItem);       

// Constructor

CEmersonRocMasterDeviceOptions::CEmersonRocMasterDeviceOptions(void)
{
	m_Group = 2;

	m_Unit  = 1;

	m_Model = 0;

	m_Poll  = 1000;

	m_OpID  = "LOI";

	m_Pass  = "";

	m_Acc   = 0;

	m_fUse  = FALSE;

	m_fChange = FALSE;
	}

// UI Managament

void CEmersonRocMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Model" ) {

			if( m_fChange ) {

				CString Text =  "You have made changes to the Emerson Process device configuration \n"
						"that may invalidate data tag references in the database.\n\n"
						"You should correct this by running the Rebuild Comms Blocks utility \n"
						"from the File menu.";

				pWnd->Information(Text);

				m_fChange = FALSE;
				}
			}

		if( Tag == "Pass" ) {

			m_PassNum = tatoi(m_Pass);
			}

		if( Tag.IsEmpty() || Tag == "Use" ) {

			pWnd->EnableUI("Acc", pItem->GetDataAccess("Use")->ReadInteger(pItem));
			}
		}
	}

// Persistance

void CEmersonRocMasterDeviceOptions::PostLoad(void)
{
	m_PassNum   = tatoi(m_Pass);
	}

// Download Support

BOOL CEmersonRocMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Group));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Model));
	Init.AddWord(WORD(m_Poll));
	Init.AddWord(WORD(m_PassNum));
	Init.AddByte(BYTE(m_fUse));
	Init.AddByte(BYTE(m_Acc));
	Init.AddText(m_OpID);

	return TRUE;
	}

// Meta Data Creation

void CEmersonRocMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Model);
	Meta_AddInteger(Poll);
	Meta_AddInteger(PassNum);
	Meta_AddInteger(Acc);
	Meta_AddString(OpID);
	Meta_AddString(Pass);
	Meta_AddBoolean(Use);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Space Wrapper Class
//

// Constructors

CSpaceROC::CSpaceROC(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT cd, UINT cn, UINT cx)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= x;
	m_uType		= t;
	m_uSpan		= t;
	m_Collect	= cd;
	m_ColMin	= cn;
	m_ColMax	= cx;

	m_fCol		= !(cd == 0 && cn == 0 && cx == 0);
	
	FindAlignment();

	FindWidth();
	}

// Matching

BOOL CSpaceROC::MatchSpace(CAddress const &Addr)
{	
	if( m_uTable == Addr.a.m_Table ) {

		if( m_uTable == addrNamed ) {

			if( m_uMinimum < m_uMaximum ) {

				return TRUE;
				}

			if( Addr.a.m_Offset == m_uMinimum ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Limits

void CSpaceROC::GetMinimum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= m_fCol ? ((m_uMinimum << 5) | m_ColMin) : m_uMinimum;
	}

void CSpaceROC::GetMaximum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= m_fCol ? ((m_uMaximum << 5) | m_ColMax) : m_uMaximum;
	}

 
//////////////////////////////////////////////////////////////////////////
//
// CEmersonRocMasterDriver
//

// Instantiator

ICommsDriver * Create_EmersonRocMasterDriver(void)
{
	return New CEmersonRocMasterDriver;
	}

// Constructor

CEmersonRocMasterDriver::CEmersonRocMasterDriver(void)
{
	m_wID		= 0x408C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Emerson Process";
	
	m_DriverName	= "FloBoss/ROC";
	
	m_Version	= "1.00";
	
	m_ShortName	= "ROC";

	AddSpaces();
	}

// Destructor

CEmersonRocMasterDriver::~CEmersonRocMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CEmersonRocMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEmersonRocMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEmersonRocMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEmersonRocMasterDriverOptions);
	}

CLASS CEmersonRocMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonRocMasterDeviceOptions);
	}

// Address Management

BOOL CEmersonRocMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CEmRocAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CEmersonRocMasterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	CEmersonRocMasterDeviceOptions * pOpt = (CEmersonRocMasterDeviceOptions *)pConfig;

	if( pOpt ) {

		UINT uDevice = pOpt->m_Model;

		if( uDevice == 0 ) {	// Floboss 103,104

			if( Addr.a.m_Table == addrNamed ) {

				if( Addr.a.m_Offset >= 7 && Addr.a.m_Offset <= 59 ) {

					return TRUE;
					}

				if( Addr.a.m_Offset >= 76 && Addr.a.m_Offset <= 84 ) {

					return TRUE;
					}

				switch( Addr.a.m_Offset ) {

					case 2:	// CL06
					case 3:	// CL07

						return TRUE;
					}

				return FALSE;
				}

			if( Addr.a.m_Table >= 202 && Addr.a.m_Table <= 222 ) {

				return TRUE;
				}

			if( Addr.a.m_Table >= 196 && Addr.a.m_Table <= 200 ) {

				return TRUE;
				}

			switch( Addr.a.m_Table ) {

				case 5:		// DI04
				case 29:	// DO05
				case 55:	// AI16
				case 57:	// AI18
				case 83:	// AO08
				case 84:	// AO09
				case 100:	// PI15
				case 102:	// PI17
				case 104:	// PI19
				case 105:	// PI19C
				case 135:	// AGA08
				case 141:	// AGA14
				case 108:	// HP00
				case 117:	// FST18

					return TRUE;

				case 109:
				case 110:
				case 111:

					if( Addr.a.m_Offset <= 8 ) {

						return TRUE;
						}
				}

			return FALSE;
			}
		}

	return FALSE;
	}

// Address Helpers

BOOL CEmersonRocMasterDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uFind = Text.Find(':');

	UINT uCollect = 0;

	if( uFind < NOTHING ) {

		uCollect = tatoi(Text.Mid(0, uFind));

		Text = Text.Mid(uFind + 1);
		}
	
	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		if( pSpace ) {

			if( IsString(pSpace->m_uTable) ) {

				if( Addr.a.m_Offset > pSpace->m_uMinimum ) {

					Addr.a.m_Offset *= GetOperand(pSpace->m_uTable);
					}

				if( Addr.a.m_Offset > pSpace->m_uMaximum ) {

					Error.Set( CString(IDS_ERROR_OFFSETRANGE), 0);
				
					return FALSE;
					}
				}

			CSpaceROC * pRoc = (CSpaceROC *) pSpace;

			if( uCollect < pRoc->m_ColMin || uCollect > pRoc->m_ColMax ) {

				Error.Set( CString(IDS_DRIVER_ADDR_INVALID), 0 );

				return FALSE;
				}

			if( uCollect ) {

				Addr.a.m_Offset = ((Addr.a.m_Offset << 5) | (uCollect & 0x1F));
				}
	
			return TRUE;
			}
		}

	return FALSE;
	}



BOOL CEmersonRocMasterDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpaceROC * pSpace = (CSpaceROC *)GetSpace(Addr);

	CAddress addr;

	addr.m_Ref = Addr.m_Ref;

	if( pSpace ) {

		if( IsString(pSpace->m_uTable)) {

			if( addr.a.m_Offset > pSpace->m_uMinimum ) {

				addr.a.m_Offset = addr.a.m_Offset / GetOperand(pSpace->m_uTable);
				}
			}

		if( pSpace->IsNamed() ) {

			Text = pSpace->m_Prefix;
			}
		else {
			BOOL fColl   = !(pSpace->m_Collect == 0 && pSpace->m_ColMax == 0 && pSpace->m_ColMin == 0);
			
			UINT uOffset = fColl ? (addr.a.m_Offset & 0xFFE0) >> 5 : addr.a.m_Offset;

			UINT uColl   = fColl ? (Addr.a.m_Offset & 0x1F  ) >> 0 : 0;

			if( uColl ) {

				Text.Printf( L"%s%s:%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uColl),
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			else {
				Text.Printf( L"%s%s",
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			}

		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CEmersonRocMasterDriver::AddSpaces(void)
{								       
	// OpCode 0

	// TODO:  Opcode 0 is extremely dependant on correct user configuration.  Should we support it at all? 
	// TODO:  At the very least, work on this support before releasing

/*	AddSpace(New CSpaceROC(addrNamed, "AP",		"Alarm Pointer",				10,  4,	  0,	addrWordAsWord));
	AddSpace(New CSpaceROC(addrNamed, "EV",		"Event Pointer",				10,  5,	  0,	addrWordAsWord));
	AddSpace(New CSpaceROC(addrNamed, "HHP",	"Hourly History Pointer",			10,  6,	  0,	addrWordAsWord));
	AddSpace(New CSpaceROC(	     239, "SAI",	"Diagnostic or System AI",			10,  1,	  5,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     238, "DI",		"Discrete Input Point",				10,  1,	  7,	addrBitAsBit));
	AddSpace(New CSpaceROC(	     237, "TDI",	"Timed Duration Input, EU",			10,  0, 127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     236, "AI",		"Analog Input, EU",				10,  0, 127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     235, "MRCGF",	"Meter Run Current Gas Flow MCF/day",		10,  0, 127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     234, "MRCE",	"Meter Run Current Energy MMBTU/day",		10,  0,	127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     233, "MRTGF",	"Meter Run Total MCF Since Contract Hr",	10,  0,	127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     232, "MRTE",	"Meter Run Total MMBTU Since Contract Hr",	10,  0, 127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     231, "PIRAC",	"Pulse Input Raw Accumulator Counts",		10,  0, 127,	addrLongAsLong));
	AddSpace(New CSpaceROC(	     230, "PIR", 	"Pulse Input Rate, EU",				10,  0,	127,	addrLongAsLong));
	AddSpace(New CSpaceROC(	     229, "PIT",	"Pulse Input Total Today, EU",			10,  0,	127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     228, "PIDST",	"PID Loop Status",				10,  0,	127,	addrByteAsByte));
	AddSpace(New CSpaceROC(	     227, "PIDPS",	"PID Loop Primary Setpoint",			10,  0,	127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     226, "PIDSS",	"PID Loop Secondary Setpoint",			10,  0, 127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     225, "AO",		"Analog Output, EU",				10,  0, 127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     224, "TDO",	"Timed Duration Output, EU",			10,  0,	127,	addrRealAsReal));
	AddSpace(New CSpaceROC(	     223, "DO",		"Discrete Output Point",			10,  1,   7,	addrBitAsBit));
*/			
		
	// Opcode 6

	AddSpace(New CSpaceROC(addrNamed, "NDI",	"Number of Discrete Inputs",			10,  7,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NAI",	"Number of Analog Inputs",			10,  8,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NDO",	"Number of Discrete Outputs",			10,  9,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NAO",	"Number of Analog Outputs",			10, 10,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMR",	"Number of Active AGA Meter Runs",		10, 11,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NPI",	"Number of Pulse Inputs",			10, 12,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NAPID",	"Number of Active PIDs",			10, 13,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NTS",	"Number of Tanks",				10, 14,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NHP",	"Number of Configured History Points",		10, 15,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NEHP",	"Number of Extended History Points",		10, 16,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NFST",	"Number of FSTs",				10, 17,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NSP",	"Number of Soft Points",			10, 18,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NCP",	"Number of Comm Ports",				10, 19,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "TYPE",	"Device Type",					10, 20,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NOPS",	"Number of Configurable Opcodes",		10, 21,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(	     208, "CUST",	"Customer Name (Str)",				10,  0,   1 * GetOperand(208),	addrLongAsLong));
	AddSpace(New CSpaceROC(addrNamed, "NT22",	"Number of User Defined Point Type 22",		10, 22,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT23",	"Number of User Defined Point Type 23",		10, 23,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT25",	"Number of User Defined Point Type 25",		10, 24,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT26",	"Number of User Defined Point Type 26",		10, 25,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT27",	"Number of User Defined Point Type 27",		10, 26,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT28",	"Number of User Defined Point Type 28",		10, 27,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT29",	"Number of User Defined Point Type 29",		10, 28,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT30",	"Number of User Defined Point Type 30",		10, 29,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT31",	"Number of User Defined Point Type 31",		10, 30,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT32",	"Number of User Defined Point Type 32",		10, 31,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT33",	"Number of User Defined Point Type 33",		10, 32,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT34",	"Number of User Defined Point Type 34",		10, 33,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT35",	"Number of User Defined Point Type 35",		10, 34,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT36",	"Number of User Defined Point Type 36",		10, 35,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT37",	"Number of User Defined Point Type 37",		10, 36,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT38",	"Number of User Defined Point Type 38",		10, 37,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NT39",	"Number of User Defined Point Type 39",		10, 38,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMVS",	"Number of MVS",				10, 39,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NRP",	"Number of Run Parameters",			10, 40,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NERP",	"Number of Extra Run Parameters",		10, 41,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NUL",	"Number of User Lists",				10, 42,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NPC",	"Number of Power Control",			10, 43,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMCAL",	"Number of Meter Calibration Parameters",	10, 44,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMCFG",	"Number of Meter Configuration Parameters",	10, 45,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMFV",	"Number of Meter Flow Values",			10, 46,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NPID",	"Number of PID Control",			10, 47,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NBP",	"Number of Battery Parameters",			10, 48,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMDC",	"Number of Modbus Configuration",		10, 49,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMDF",	"Number of Modbus Function Tables",		10, 50,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NMDSF",	"Number of Modbus Special Function",		10, 51,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NAIC",	"Number of AI Calculation Values",		10, 52,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NLON",	"Number of Logon Parameters",			10, 53,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NRI",	"Number of Revision Information",		10, 54,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "NPF",	"Number of Program Flash",			10, 55,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "CP6R",	"Communication Port Opcode 6 Request",		10, 56,	  0,			addrByteAsByte));

	// Opcode 103

	AddSpace(New CSpaceROC(	     207, "ID",		"Product Identification (Str)",			10,  0,   1 * GetOperand(207),	addrLongAsLong));
	AddSpace(New CSpaceROC(	     206, "HID",	"Hardware Identification Number (Str)",		10,  0,   1 * GetOperand(206),	addrLongAsLong));
	AddSpace(New CSpaceROC(	     205, "FTD",	"Firmware Time and Date (Str)",			10,  0,	  1 * GetOperand(205),	addrLongAsLong));
	AddSpace(New CSpaceROC(addrNamed, "UN",		"Unit Number",					10, 57,	  0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "GN",		"Group Number",					10, 58,   0,			addrByteAsByte));
	AddSpace(New CSpaceROC(	     204, "STN",	"Station Name (Str)",				10,  0,   1 * GetOperand(204),	addrLongAsLong));
	AddSpace(New CSpaceROC(addrNamed, "CTD",	"Current Time and Date",			10, 59,	  0,			addrLongAsLong));

	// Opcode 107

	AddSpace(New CSpaceROC(	 203,	"HTAG",		"Historical Base RAM Tag (Str)",		10,  0,   14 * GetOperand(203), addrLongAsLong));
	AddSpace(New CSpaceROC(	 202,	"HLOC",		"Historical Base RAM Period Location",		10,  0,	  14,			addrWordAsWord));
	
  	// Opcode 165 

	AddSpace(New CSpaceROC(222,	"HPAT",		"History Point Archive Type",			10,  0,	 99,	addrByteAsByte));
	AddSpace(New CSpaceROC(221,	"HPPT",		"History Point Type",				10,  0,	 99,	addrByteAsByte));
	AddSpace(New CSpaceROC(220,	"HPLN",		"History Point Logic Number",			10,  0,	 99,	addrByteAsByte));
	AddSpace(New CSpaceROC(219,	"HPPN",		"History Point Parameter Number",		10,  0,	 99,	addrByteAsByte));

	// Opcode 105

	AddSpace(New CSpaceROC(218,	"HPCV",		"History Point Current Value",			10,  0,  99,	addrRealAsReal));
	AddSpace(New CSpaceROC(217,	"HMINV",	"History Point Min Contract Hour Value",	10,  0,  99,	addrRealAsReal));
	AddSpace(New CSpaceROC(216,	"HMAXV",	"History Point Max Contract Hour Value",	10,  0,  99,	addrRealAsReal));
	AddSpace(New CSpaceROC(215,	"HMINT",	"History Point Min Time",			10,  0,  99,	addrLongAsLong));
	AddSpace(New CSpaceROC(214,	"HMAXT",	"History Point Max Time",			10,  0,  99,	addrLongAsLong));
	AddSpace(New CSpaceROC(213,	"HMINYV",	"History Point Min Yesterday Value",		10,  0,  99,	addrRealAsReal));
	AddSpace(New CSpaceROC(212,	"HMAXYV",	"History Point Max Yesterday Value",		10,  0,  99,	addrRealAsReal));
	AddSpace(New CSpaceROC(211,	"HMINYT",	"History Point Yesterday Min Time",		10,  0,  99,	addrLongAsLong));
	AddSpace(New CSpaceROC(210,	"HMAXYT",	"History Point Yesterday Max Time",		10,  0,  99,	addrLongAsLong));
	AddSpace(New CSpaceROC(209,	"HPVLH",	"History Point Value During Last Hour",		10,  0,	 99,	addrRealAsReal));

	// OpCode 180/181

	// Point Type 1 - Discrete Input

	AddSpace(New CSpaceROC(	 1,	"DI00_",	"Discrete Input Point Tag ID (Str)",	10,   0,	 69 * GetOperand(1),	addrLongAsLong));
	AddSpace(New CSpaceROC(  2,	"DI01_",	"Discrete Input Filter",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(	 3,	"DI02_",	"Discrete Input Status",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(	 4,	"DI03_",	"Discrete Input Modes",			10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(	 5,	"DI04_",	"Discrete Input Alarm Code",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(	 6,	"DI05_",	"Discrete Input Acc. Value",		10,   0,	 69,			addrLongAsLong));
	AddSpace(New CSpaceROC(	 7,	"DI06_",	"Discrete Input On Counter",		10,   0,	 69,			addrLongAsLong));
	AddSpace(New CSpaceROC(	 8,	"DI07_",	"Discrete Input Off Counter",		10,   0,	 69,			addrLongAsLong));
//	AddSpace(New CSpaceROC(	 9,	"DI08_",	"Discrete Input 0% Pulse Width",	10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC( 10,	"DI09_",	"Discrete Input 100% Pulse Width",	10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC( 11,	"DI10_",	"Discrete Input Maximum Count",		10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC( 12,	"DI11_",	"Discrete Input Units (Str)",		10,   0,	 69,			addrLongAsLong));
	AddSpace(New CSpaceROC( 13,	"DI12_",	"Discrete Input Scan Period(50 ms int)",10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC( 14,	"DI13_",	"Discrete Input Low Reading EU ",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 15,	"DI14_",	"Discrete Input High Reading EU ",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 16,	"DI15_",	"Discrete Input Low Alarm EU ",		10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 17,	"DI16_",	"Discrete Input High Alarm EU ",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 18,	"DI17_",	"Discrete Input Low Low Alarm EU",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 19,	"DI18_",	"Discrete Input High High Alarm EU",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 20,	"DI19_",	"Discrete Input Rate Alarm EU ",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 21,	"DI20_",	"Discrete Input Alarm Deadband ",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 22,	"DI21_",	"Discrete Input EU Value ",		10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 23,	"DI22_",	"Discrete Input TDI Count",		10,   0,	 69,			addrWordAsWord));


	// Point Type 2 - Discrete Output

	AddSpace(New CSpaceROC( 24,	"DO00_",	"Discrete Output Point Tag ID (Str)",	10,   0,	 69 * GetOperand(24),	addrLongAsLong));
	AddSpace(New CSpaceROC( 25,	"DO01_",	"Discrete Output Time On(50 ms int)",	10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC( 26,	"DO02_",	"Not Used",				10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC( 27,	"DO03_",	"Discrete Output Status",		10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC( 28,	"DO04_",	"Discrete Output Mode",			10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC( 29,	"DO05_",	"Discrete Output Alarm Code",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC( 30,	"DO06_",	"Discrete Output Acc. Value",		10,   0,	 69,			addrLongAsLong));
	AddSpace(New CSpaceROC( 31,	"DO07_",	"Discrete Output Units (Str)",		10,   0,	 69 * GetOperand(31),	addrLongAsLong));
	AddSpace(New CSpaceROC( 32,	"DO08_",	"Discrete Output Cycle Time",		10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC( 33,	"DO09_",	"Discrete Output 0% Count",		10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC( 34,	"DO10_",	"Discrete Output 100% Count",		10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC( 35,	"DO11_",	"Discrete Output Low Reading EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC( 36,	"DO12_",	"Discrete Output High Reading EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC( 37,	"DO13_",	"Discrete Output EU Value ",		10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC( 38,	"DO14_",	"Discrete Output Alarm Mode",		10,   0,	 69,			addrByteAsByte)); // 107

	// Point Type 3 - Analog Input

	AddSpace(New CSpaceROC(   39, "AI00_",	"Analog Input Point Tag ID (Str)",	10,   0,	 69 * GetOperand(39),	addrLongAsLong));
	AddSpace(New CSpaceROC(   40, "AI01_",	"Analog Input Units (Str)",		10,   0,	 69 * GetOperand(40),	addrLongAsLong));
	AddSpace(New CSpaceROC(   41, "AI02_",	"Analog Input Scan Period(50 ms int)",	10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   42, "AI03_",	"Analog Input Filter(50 ms int)",	10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   43, "AI04_",	"Analog Input Adjusted D/A 0%",		10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   44, "AI05_",	"Analog Input Adjusted D/A 100%",	10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   45, "AI06_",	"Analog Input Low Reading EU ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   46, "AI07_",	"Analog Input High Reading EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   47, "AI08_",	"Analog Input Low Alarm EU ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   48, "AI09_",	"Analog Input High Alarm EU ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   49, "AI10_",	"Analog Input Low Low Alarm EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   50, "AI11_",	"Analog Input High High Alarm EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   51, "AI12_",	"Analog Input Rate Alarm EU ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   52, "AI13_",	"Analog Input Alarm Deadband ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   53, "AI14_",	"Analog Input Filterd EU's ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   54, "AI15_",	"Analog Input Mode",			10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(   55, "AI16_",	"Analog Input Alarm Code",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(   56, "AI17_",	"Analog Input Raw D/A",			10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   57, "AI18_",	"Analog Input Scan Time(50 ms int)",	10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   58, "AI19_",	"Analog Input Fault EU Value ",		10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   59, "AI20_",	"Analog Input Cal Zero Value",		10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC(   60, "AI21_",	"Analog Input Cal Midpoint 1 Value",	10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC(   61, "AI22_",	"Analog Input Cal Midpoint 2 Value",	10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC(   62, "AI23_",	"Analog Input Cal Midpoint 3 Value",	10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC(   63, "AI24_",	"Analog Input Cal Span EU Value",	10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC(   64, "AI25_",	"Analog Input Cal Zero Value ",		10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   65, "AI26_",	"Analog Input Cal Midpoint 1 Value",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   66, "AI27_",	"Analog Input Cal Midpoint 2 Value",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   67, "AI28_",	"Analog Input Cal Midpoint 3 Value",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   68, "AI29_",	"Analog Input Cal Span EU Value",	10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   69, "AI30_",	"Anlaog Input Offset ",			10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   70, "AI31_",	"Analog Input Set EU Value ",		10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   71, "AI32_",	"Analog Input Manual EU ",		10,   0,	 69,			addrRealAsReal));
//	AddSpace(New CSpaceROC(   72, "AI33_",	"Analog Input Timer",			10,   0,	 69,			addrWordAsWord));
//	AddSpace(New CSpaceROC(   73, "AI34_",	"Analog Input Calibration Mode",	10,   0,	 69,			addrByteAsByte));
//	AddSpace(New CSpaceROC(   74, "AI35_",	"Analog Input Calibration Type",	10,   0,	 69,			addrByteAsByte));


	// Point Type 4 - Analog Output

	AddSpace(New CSpaceROC(   75, "AO00_",	"Analog Output Point Tag ID (Str)",	10,   0,	 69 * GetOperand(75),	addrLongAsLong));
	AddSpace(New CSpaceROC(   76, "AO01_",	"Analog Output Units (Str)",		10,   0,	 69 * GetOperand(76),	addrLongAsLong));
	AddSpace(New CSpaceROC(   77, "AO02_",	"Analog Output Adjusted D/A 0%",	10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   78, "AO03_",	"Analog Output Adjusted D/A 100%",	10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   79, "AO04_",	"Analog Output Low Reading EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   80, "AO05_",	"Analog Output High Reading EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   81, "AO06_",	"Analog Output EU Value ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   82, "AO07_",	"Analog Output Mode",			10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(   83, "AO08_",	"Analog Output Alarm Code",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(   84, "AO09_",	"Analog Output Raw D/A",		10,   0,	 69,			addrWordAsWord));
	

	// Point Type 5 - Pulse Input

	AddSpace(New CSpaceROC(   85, "PI00_",	"Pulse Input Point Tag ID (Str)",	10,   0,	 69 * GetOperand(85),	addrLongAsLong));
	AddSpace(New CSpaceROC(   86, "PI01_",	"Pulse Input Units (Str)",		10,   0,	 69 * GetOperand(86),	addrLongAsLong));
	AddSpace(New CSpaceROC(   87, "PI02_",	"Pulse Input Rate Flag",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(   88, "PI03_",	"Pulse Input Rate Period",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(   89, "PI04_",	"Pulse Input Filter Time",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(   90, "PI05_",	"Pulse Input Scan Period(50 ms int)",   10,   0,	 69,			addrWordAsWord));
	AddSpace(New CSpaceROC(   91, "PI06_",	"Pulse Input Conversion Factor ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   92, "PI07_",	"Pulse Input Low Alarm EU ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   93, "PI08_",	"Pulse Input High Alarm EU ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   94, "PI09_",	"Pulse Input Low Low Alarm EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   95, "PI10_",	"Pulse Input High High Alarm EU ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   96, "PI11_",	"Pulse Input Rate Alarm EU ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   97, "PI12_",	"Pulse Input Alarm Deadband ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   98, "PI13_",	"Pulse Input EU Value ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(   99, "PI14_",	"Pulse Input Mode",			10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(  100, "PI15_",	"Pulse Input Alarm Code",		10,   0,	 69,			addrByteAsByte));
	AddSpace(New CSpaceROC(  101, "PI16_",	"Pulse Input Acc. Value",		10,   0,	 69,			addrLongAsLong));
	AddSpace(New CSpaceROC(  102, "PI17_",	"Pulse Input Current Rate ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(  103, "PI18_",	"Pulse Input Today's Total ",		10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(  104, "PI19_",	"Pulse Input Yesterday's Total ",	10,   0,	 69,			addrRealAsReal));
	AddSpace(New CSpaceROC(  105, "PI19C_",	"Pulse Input Continuous Accumulator",	10,   0,	 69,			addrLongAsLong));
	AddSpace(New CSpaceROC(  106, "PI20_",	"Pulse Input EU Units",			10,   0,	 69,			addrLongAsLong));
//	AddSpace(New CSpaceROC(  107, "PI21_",	"Pulse Input Frequency (Hertz) ",	10,   0,	 69,			addrRealAsReal)); // 107


	// Point Type 7 -  AGA Flow Parameters

	AddSpace(New CSpaceROC(  127, "AGA00_",	"AGA Flow Point Tag ID (Str)",		10,   0,	127 * GetOperand(127),	addrLongAsLong));
	AddSpace(New CSpaceROC(  128, "AGA01_",	"AGA Latitude",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  129, "AGA02_",	"AGA Elevation",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  130, "AGA03_",	"AGA Calculation Method",		10,   0,	127,			addrByteAsByte));
	AddSpace(New CSpaceROC(  131, "AGA04_",	"AGA Configuration Options",		10,   0,	127,			addrByteAsByte));
	AddSpace(New CSpaceROC(  132, "AGA05_",	"AGA Specific Gravity",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  133, "AGA06_",	"AGA Heating Value",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  134, "AGA07_",	"AGA Gravity Acceleration",		10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  135, "AGA08_",	"AGA Scan Period",			10,   0,	127,			addrWordAsWord));
	AddSpace(New CSpaceROC(  136, "AGA09_",	"AGA Pipe Diameter",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  137, "AGA10_",	"AGA Orifice Diameter",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  138, "AGA11_",	"AGA Orifice Measured Temperature",	10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  139, "AGA12_",	"AGA Orifice Material",			10,   0,	127,			addrByteAsByte));
	AddSpace(New CSpaceROC(  140, "AGA13_",	"AGA Meter Run Description (Str)",	10,   0,	127 * GetOperand(140),	addrLongAsLong));
	AddSpace(New CSpaceROC(  141, "AGA14_",	"AGA Alarm Code",			10,   0,	127,			addrByteAsByte));
	AddSpace(New CSpaceROC(  142, "AGA15_",	"AGA Low Alarm EU - Flow",		10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  143, "AGA16_",	"AGA High Alarm EU - Flow",		10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  144, "AGA17_",	"AGA Viscosity",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  145, "AGA18_",	"AGA Specific Heat Ratio",		10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  146, "AGA19_",	"AGA Contact or Base Pressure",		10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  147, "AGA20_",	"AGA Contact or Base Temperature",	10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  148, "AGA21_",	"AGA Low Differential Pressure",	10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  149, "AGA22_",	"AGA User Correction Factor",		10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  150, "AGA23_",	"AGA Nitrogen",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  151, "AGA24_",	"AGA Carbon Dioxide",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  152, "AGA25_",	"AGA Hydrogen Sulfide",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  153, "AGA26_",	"AGA Water",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  154, "AGA27_",	"AGA Helium",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  155, "AGA28_",	"AGA Methane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  156, "AGA29_",	"AGA Ethane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  157, "AGA30_",	"AGA Propane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  158, "AGA31_",	"AGA n-Butane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  159, "AGA32_",	"AGA i-Butane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  160, "AGA33_",	"AGA n-Pentane",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  161, "AGA34_",	"AGA i-Pentane",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  162, "AGA35_",	"AGA n-Hexane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  163, "AGA36_",	"AGA n-Heptane",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  164, "AGA37_",	"AGA n-Octane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  165, "AGA38_",	"AGA n-Nonane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  166, "AGA39_",	"AGA n-Decane",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  167, "AGA40_",	"AGA Oxygen",				10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  168, "AGA41_",	"AGA Carbon Monoxide",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  169, "AGA42_",	"AGA Hydrogen",				10,   0,	127,			addrRealAsReal));
//	AddSpace(New CSpaceROC(  170, "AGA43_",	"AGA Calculation Units",		10,   0,	127,			addrByteAsByte));
	AddSpace(New CSpaceROC(  171, "AGA44_",	"AGA Enable Stacked Diff Pressure (hw)",10,   0,	127,			addrByteAsByte));
//	AddSpace(New CSpaceROC(  172, "AGA45_",	"AGA Low Diff Pressure (hw) (Str)",	10,   0,	127 * GetOperand(172),	addrLongAsLong));
//	AddSpace(New CSpaceROC(  173, "AGA46_",	"AGA Diff Pressure (hw) Input (Str)",	10,   0,	127 * GetOperand(173),	addrLongAsLong));
//	AddSpace(New CSpaceROC(  174, "AGA47_",	"AGA Static Pressure Input (Pf) (Str)",	10,   0,	127 * GetOperand(174),	addrLongAsLong));
//	AddSpace(New CSpaceROC(  175, "AGA48_",	"AGA Temperature Input (Tf) (Str)",	10,   0,	127 * GetOperand(175),	addrLongAsLong));
	AddSpace(New CSpaceROC(  176, "AGA49_",	"AGA Low Diff Pressure (hw) Setpoint",	10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  177, "AGA50_",	"AGA High Diff Pressure (hw) Setpoint",	10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  178, "AGA51_",	"AGA MV Diff Pressure (hw)",		10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  179, "AGA52_",	"AGA Static Flowing Pressure (Pf)",	10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  180, "AGA53_",	"AGA Flowing Temperature (Tf)",		10,   0,	127,			addrRealAsReal));


	// Point Type 8 - History Parameters

	AddSpace(New CSpaceROC(  108, "HP00_",	"History Param Point Tag ID (Str)",	10,   1,	15 * GetOperand(108),	addrLongAsLong));
	AddSpace(New CSpaceROC(  109, "HP01_",	"History Param History Log Point (Str)",10,   1,	15 * GetOperand(109),	addrLongAsLong));
	AddSpace(New CSpaceROC(  110, "HP02_",	"History Param Archive Type",		10,   1,	15,			addrByteAsByte));
	AddSpace(New CSpaceROC(  111, "HP03_",	"History Param Averaging/Rate Type",	10,   1,	15,			addrByteAsByte));

	
	// Point Type 12 - ROC Clock via Opcodes 7 and 8

	AddSpace(New CSpaceROC(addrNamed, "CL00",  "ROC Time",				10,   1,	0,			addrLongAsLong));
	AddSpace(New CSpaceROC(addrNamed, "CL06",  "ROC Leap Year",			10,   2,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "CL07",  "ROC Day of Week",			10,   3,	0,			addrByteAsByte));
	

	// Point Type 13 - System Flags

	AddSpace(New CSpaceROC(	181,	    "SF",  "System Flags",			10,   0,       30,			addrByteAsByte));
		

	// Point Type 15 - System Variables

	AddSpace(New CSpaceROC(addrNamed, "SV00",  "ROC Address",			10,  60,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV01",  "ROC Group",				10,  61,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(      201, "SV02_", "Station Name (Str)",		10,   0,	1 * GetOperand(201),	addrLongAsLong));
	AddSpace(New CSpaceROC(addrNamed, "SV03",  "Active PIDs",			10,  63,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV04",  "Active AGA Meter Runs",		10,  64,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV05",  "Number of FST Instructions",	10,  65,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV06",  "Number of History Points",		10,  66,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV07",  "Number of Extended History Points", 10,  67,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV08",  "Number of RAM2/History 3 Points",	10,  68,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV09",  "Force End of Day",			10,  69,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV10",  "Contract Hour",			10,  70,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(      200, "SV11_", "Version Name - Part Number (Str)",	10,   0,	1 * GetOperand(200),	addrLongAsLong));
	AddSpace(New CSpaceROC(      199, "SV12_", "Manufacturing Identification (Str)",10,   0,	1 * GetOperand(199),	addrLongAsLong));
	AddSpace(New CSpaceROC(      198, "SV13_", "Time Created (Str)",		10,   0,	1 * GetOperand(198),	addrLongAsLong));
	AddSpace(New CSpaceROC(      197, "SV14_", "Unit Serial Number (Str)",		10,   0,	1 * GetOperand(197),	addrLongAsLong));
	AddSpace(New CSpaceROC(      196, "SV15_", "Customer Name (Str)",		10,   0,	1 * GetOperand(196),	addrLongAsLong));
	AddSpace(New CSpaceROC(addrNamed, "SV16",  "Maximum PIDs",			10,  76,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV17",  "Maximum AGA Meter Runs",		10,  77,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV18",  "Maximum Tanks",			10,  78,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV19",  "FSTs Possible",			10,  79,	0,			addrByteAsByte));
//	AddSpace(New CSpaceROC(addrNamed, "SV20",  "RAM Installed",			10,  80,	0,			addrByteAsByte));
//	AddSpace(New CSpaceROC(addrNamed, "SV21",  "ROM Installed",			10,  81,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV22",  "MPU Loading",			10,  82,	0,			addrRealAsReal));
//	AddSpace(New CSpaceROC(addrNamed, "SV23",  "Utilities",				10,  83,	0,			addrByteAsByte));
	AddSpace(New CSpaceROC(addrNamed, "SV24",  "Type of ROC or FloBoss",		10,  84,	0,			addrWordAsWord));
	AddSpace(New CSpaceROC(addrNamed, "SV25",  "Units Flag",			10,  85,	0,			addrByteAsByte));
 
	// Point Type 16 - FST Parameters

	AddSpace(New CSpaceROC(  112, "FST00_",	"FST Point Tag ID (Str)",		10,   0,	127 * GetOperand(112),	addrLongAsLong));
	AddSpace(New CSpaceROC(  113, "FST01_",	"FST Result Register",			10,   0,	127,			addrRealAsReal));
	AddSpace(New CSpaceROC(  114, "FST02_",	"FST Register",				10,   0,	127,			addrRealAsReal, 1, 1, 10));
	AddSpace(New CSpaceROC(  115, "FST12_",	"FST Timer",				10,   0,	127,			addrLongAsLong, 1, 1, 4));
	AddSpace(New CSpaceROC(  116, "FST16_",	"FST Message (Str)",			10,   0,	127 * GetOperand(116),	addrLongAsLong, 1, 1, 2));
	AddSpace(New CSpaceROC(  117, "FST18_",	"FST Message Data (Str)",		10,   0,	127 * GetOperand(117),	addrLongAsLong));
	AddSpace(New CSpaceROC(  118, "FST19_",	"FST Misc",				10,   0,	127,			addrByteAsByte, 1, 1, 4));
	AddSpace(New CSpaceROC(  119, "FST23_",	"FST Compare Flag - SVD",		10,   0,	127,			addrByteAsByte));
	AddSpace(New CSpaceROC(  120, "FST24_",	"FST Run Flag",				10,   0,	127,			addrByteAsByte));
	AddSpace(New CSpaceROC(  121, "FST25_",	"FST Code Size",			10,   0,	127,			addrWordAsWord));
	AddSpace(New CSpaceROC(  122, "FST26_",	"FST Instruction Pointer",		10,   0,	127,			addrWordAsWord));
	AddSpace(New CSpaceROC(  123, "FST27_",	"FST Execution Delay",			10,   0,	127,			addrWordAsWord));

	
	// Point Type 17 - Soft Points

  	AddSpace(New CSpaceROC(  124, "SP00_",	"Soft Point Tag ID (Str)",		10,   0,	127 * GetOperand(124),	addrLongAsLong));
	AddSpace(New CSpaceROC(  125, "SP01_",	"Soft Point Integer Flag",		10,   0,	127,			addrWordAsWord));
	AddSpace(New CSpaceROC(  126, "SP02_",	"Soft Point Data",			10,   0,	127,			addrRealAsReal, 1, 1, 20));
	}

// Helpers

BOOL CEmersonRocMasterDriver::IsString(UINT uTable)
{
	switch( uTable ) {

		case 1:
		case 12:
		case 24:
		case 31:
		case 39:
		case 40:
		case 75:
		case 76:
		case 85:
		case 86:
		case 108:
		case 109:
		case 112:
		case 116:
		case 117:
		case 124:
		case 127:
		case 140:
		case 172:
		case 173:
		case 174:
		case 175:
		case 196:
		case 197:
		case 198:
		case 199:
		case 200:
		case 201:
		case 203:
		case 204:
		case 205:
		case 206:
		case 207:
		case 208:

			return TRUE;
		}


	return FALSE;
	}

UINT CEmersonRocMasterDriver::GetStringLen(UINT uTable)
{
	switch( uTable ) {

		case 1:
		case 12:
		case 24:
		case 31:
		case 39:
		case 40:
		case 75:
		case 76:
		case 85:
		case 86:
		case 108:
		case 109:
		case 112:
		case 117:
		case 124:
		case 127:
		case 172:
		case 173:
		case 174:
		case 175:
		case 203:

			return 10;

		case 197:

			return 12;

		case 196:
		case 198:
		case 199:
		case 200:
		case 201:
		case 204:
		case 205:
		case 206:
		case 208:

			return 20;

		case 116:
		case 140:

			return 30;

		case 207:
			
			return 40;
		}

	return 4;
	}

UINT CEmersonRocMasterDriver::GetOperand(UINT uTable)
{
	UINT uLen = GetStringLen(uTable);

	while( uLen % 4 != 0 ) {

		uLen++;
		}

	return uLen / sizeof(DWORD);
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEmRocAddrDialog, CStdAddrDialog);
		
// Constructor

CEmRocAddrDialog::CEmRocAddrDialog(CEmersonRocMasterDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element     = "EmRocElementDlg";
	}

// Overridables

void CEmRocAddrDialog::SetAddressText(CString Text)
{
	CEditCtrl &Coll  = (CEditCtrl &) GetDlgItem(2002);

	CEditCtrl &Index = (CEditCtrl &) GetDlgItem(2004);

	UINT uFind = Text.Find(':');

	BOOL fCol = uFind < NOTHING ? TRUE : FALSE;

	Coll.ShowWindow(fCol);

	BOOL fEnable = FALSE;

	if( m_pSpace && m_pDriver ) {

		CEmersonRocMasterDriver * pDriver = (CEmersonRocMasterDriver *)m_pDriver;

		fEnable = m_pSpace->m_uTable != addrNamed;
		
		if( fEnable ) {

			if( pDriver->GetOperand(m_pSpace->m_uTable) == m_pSpace->m_uMaximum ) {

				fEnable = FALSE;
				}
			}
		}

	Index.EnableWindow(fEnable);

	if( fCol ) {

		Coll.SetWindowText(Text.Mid(0, uFind));

		Index.SetWindowText(Text.Mid(uFind + 1));

		return;
		}

	Coll.SetWindowText(" ");

	Index.SetWindowText(Text);
	}

CString CEmRocAddrDialog::GetAddressText(void)
{
	CEditCtrl &Coll  = (CEditCtrl &) GetDlgItem(2002);

	CEditCtrl &Index = (CEditCtrl &) GetDlgItem(2004);

	CString Text = Coll.GetWindowText();

	if( Text.IsEmpty() ) {

		return Index.GetWindowText();
		}

	Text += ":";

	Text += Index.GetWindowText();
	
	return Text;
	}


// End of File