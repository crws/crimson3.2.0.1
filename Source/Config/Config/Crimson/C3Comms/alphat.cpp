
#include "intern.hpp"

#include "alphat.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Alpha Gear Ternary Comms Driver
//

// Instantiator

ICommsDriver *	Create_AlphaTernaryDriver(void)
{
	return New CAlphaTernaryDriver;
	}

// Constructor

CAlphaTernaryDriver::CAlphaTernaryDriver(void)
{
	m_wID		= 0x4043;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Alpha Gear";
	
	m_DriverName	= "Ternary";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Alpha Gear Ternary";

	AddSpaces();
	}

// Binding Control

UINT	CAlphaTernaryDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CAlphaTernaryDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management
BOOL CAlphaTernaryDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CAlphaTernaryAddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL CAlphaTernaryDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	if( pSpace->m_uTable != DC ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	Text.MakeLower();

	TCHAR c = Text[0];

	if( CString(VS).Find(c) < NOTHING ) {

		Addr.a.m_Table  = DC;
		Addr.a.m_Offset = c;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = LL;

		return TRUE;
		}

	Error.Set( CString(VS) + CString(VS1), 0 );

	return FALSE;
	}

BOOL CAlphaTernaryDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( Addr.a.m_Table != DC ) {

		return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
		}

	Text.Printf( "%s%1c",
		pSpace->m_Prefix,
		Addr.a.m_Offset 
		);

	return TRUE;
	}

// Information Text Space for Dialog
void	CAlphaTernaryDriver::AddRequiredSpaces(BOOL fData)
{
	if( fData ) {

		DeleteAllSpaces();
		AddSpaces();
		}

	else {
		AddSpace( New CSpace( " ", " ",							1, LL ));
		AddSpace( New CSpace( "INFO1", "[xxx] = Written data value.",			2, LL ));
		AddSpace( New CSpace( "INFO2", "Write 0 for Common, Write 1 for Point",		3, LL ));
		AddSpace( New CSpace( "INFO3", "DC - Load D1... before writing letter value.",	4, LL ));
		AddSpace( New CSpace( "INFO4", "D1 - Write the value for <n>.",			5, LL ));
		AddSpace( New CSpace( "INFO5", "Current device loses communication.",		6, LL ));
		}
	}

// Implementation
void	CAlphaTernaryDriver::AddSpaces(void)
{
	AddSpace( New CSpace( 1, "R",	 "Read/Write Register",				16,	0,  0x7FFF, LL ));
	AddSpace( New CSpace( 2, "STAT", "Internal Status Flag (R/O)",			10,	0,	 0, WW ));
	AddSpace( New CSpace( 3, "ALRM", "Current Alarm/Warning Code (R/O)",		10,	0,	 0, WW ));
	AddSpace( New CSpace( 4, "MPI",  "Monitored Input PIO Signal Status (R/O)",	10,	0,	 0, WW ));
	AddSpace( New CSpace( 5, "MPO",  "Monitored Output PIO Signal Status (R/O)",	10,	0,	 0, WW ));
	AddSpace( New CSpace( 6, "Q1C",  "Non-Volatile Common -> Window Area",		10,	0,	 0, WW ));
	AddSpace( New CSpace( 7, "Q1P",  "[Non-Volatile Point] -> Window Area",		10,	0,	 0, WW ));
	AddSpace( New CSpace( 8, "Q2",   "Window Area -> Execution (INFO2)",		10,	0,	 0, WW ));
	AddSpace( New CSpace( 9, "Q3C",  "Non-Volatile Common -> Execution",		10,	0,	 0, WW ));
	AddSpace( New CSpace(10, "Q3P",  "[Non-Volatile Point] -> Execution",		10,	0,	 0, WW ));
	AddSpace( New CSpace(11, "Q4",   "Execution to Window (INFO2)",			10,	0,	 0, WW ));
	AddSpace( New CSpace(12, "V5C",  "Window Area Common -> Non-Volatile",		10,	0,	 0, WW ));
	AddSpace( New CSpace(13, "V5P",  "Window Area Point -> [Non-Volatile]",		10,	0,	 0, WW ));
	AddSpace( New CSpace(14, "V6C",  "Execution Common -> Non-Volatile",		10,	0,	 0, WW ));
	AddSpace( New CSpace(15, "V6P",  "Execution Point -> [Non-Volatile]",		10,	0,	 0, WW ));
	AddSpace( New CSpace(16, "DC_",  "Direct Command [Direct Command Letter]",	16,	0,	 0, YY ));
	AddSpace( New CSpace(DC, "D1",   "  [DC_ Parameter 1] for 'a'-'z' selection",	10,	'a',	'z',LL ));
	AddSpace( New CSpace(18, "Dl2",  "  [DC_ Parameter l2 (moving current limit)]",	10,	0,	 0, LL ));
	AddSpace( New CSpace(19, "Dv2",  "  [DC_ Parameter v2 (velocity)]",		10,	0,	 0, LL ));
	AddSpace( New CSpace(20, "Dv3",  "  [DC_ Parameter v3 (acceleration)]",		10,	0,	 0, LL ));
	AddSpace( New CSpace(21, "Dz2",  "  [DC_ Parameter z2 (direction selector)]",	10,	0,	 0, LL ));
	AddSpace( New CSpace(22, "WAX",  "Change Axis Address via AXN and AXS",		10,	0,	 0, BB ));
	AddSpace( New CSpace(23, "AXN",  "  [New Axis Address] (INFO5)",		10,	0,	 0, YY ));
	AddSpace( New CSpace(24, "AXS",  "  [Serial Number of unit] (INFO5)",		10,	0,	 0, LL ));

// Reserved Spaces
//	AddSpace( New CSpace(98, "T",    "Set Register Address for Write",		16,	0,  0x7FFF, LL ));
//	AddSpace( New CSpace(99, "W",    "Write to Register set by T command",		16,	0,	 0, LL ));
	}

//////////////////////////////////////////////////////////////////////////
//
// Alpha Gear Ternary Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CAlphaTernaryAddrDialog, CStdAddrDialog);
		
// Constructor
CAlphaTernaryAddrDialog::CAlphaTernaryAddrDialog(CAlphaTernaryDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fPart		= fPart;

	m_pDriverData	= &Driver;
	}

// Destructor
CAlphaTernaryAddrDialog::~CAlphaTernaryAddrDialog(void)
{
	if( m_pDriverData ) {

		m_pDriverData->AddRequiredSpaces(TRUE);
		}
	}

// Message Map

AfxMessageMap(CAlphaTernaryAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CAlphaTernaryAddrDialog)
	};

// Notification Handlers

BOOL CAlphaTernaryAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	// Which should it be???
	DWORD d = dwData;
	// cppcheck-suppress redundantAssignment
	d = Addr.m_Ref;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Offset = 1;

	FindSpace();

	Addr.m_Ref = d;

	if( !m_pSpace ) m_pDriverData->AddRequiredSpaces(FALSE);

	return CStdAddrDialog::OnInitDialog(Focus, dwData);
	}

void CAlphaTernaryAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);
	}

void CAlphaTernaryAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

BOOL CAlphaTernaryAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( m_pSpace->m_uTable == addrNamed ) return TRUE;

		return CStdAddrDialog::OnOkay(uID);
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// End of File
