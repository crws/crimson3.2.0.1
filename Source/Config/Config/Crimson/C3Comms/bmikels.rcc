
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

/*
LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "bmikels.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "bmikels.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "bmikels.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "bmikels.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
*/

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CBMikeLSMasterDeviceOptions
//

CBMikeLSMasterDeviceOptionsUIList RCDATA
BEGIN
	"Scan,Scan Rate,,CUIEditInteger,|0|ms|0|60000,"
	"Specify the rate for which the driver should request non-realtime data from the Beta LaserMike device.  "
	"\0"

	"Timeout,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait for a reply to a LaserSpeed request.  "
	"\0"

	"Auto,Automatic Baud Rate Detection,,Enum/DropDown,Disable|Enable,"
	"When enabled, the Red Lion device will assist in the Laser Speed's Automatic Baud Rate and Framing "
	"Detection mechanism for use of the configured baud rate and framing in Crimson.  "
	"\0"
 
	"\0"
END

CBMikeLSMasterDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Protocol Options,Timeout,Scan,Auto\0"
	"\0"	
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CBMikeLSEternetMasterDeviceOptions
//

CBMikeLSEthernetMasterDeviceOptionsUIList RCDATA
BEGIN

	"IP,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the Beta LaserMike device.  "
	"\0"

	"Port,UDP Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the LaserSpeed's Destination UDP Port for real time data."
	"\0"
	
	"\0"
END

CBMikeLSEthernetMasterDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,IP,Port\0"
	"G:1,root,Protocol Options,Timeout,Scan\0"
	"\0"	
END


//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Address Selection Dialog
//
//

LaserSpeedDlg DIALOG 0, 0, 0, 0
CAPTION "Select Data"
BEGIN
	////////
	GROUPBOX	"&Data",		-1,		  4,   4, 258,  64
	LTEXT		"Data Group",		-1,		 10,  18,  50,  10
	LTEXT		"Data Item",		-1,		138,  18, 100,  10
	LTEXT		"<type>",		301,		138,  48,  60,  10
	COMBOBOX				200,		  8,  28, 120, 100, CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP
	COMBOBOX				201,		136,  28, 120, 100, CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_VSCROLL | WS_TABSTOP
	////////
	DEFPUSHBUTTON   "OK",			IDOK,		  4,  74,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	 48,  74,  40,  14, XS_BUTTONREST
//	PUSHBUTTON	"Help",			IDHELP,		 92,  74,  40,  14, XS_BUTTONREST
END	

//////////////////////////////////////////////////////////////////////////
//
// UI for CBMikeLSSlaveDriverOptions
//

CBMikeLSSlaveDriverOptionsUIList RCDATA
BEGIN
	 
	"\0"
END

CBMikeLSSlaveDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"\0"	
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CBMikeLSSlaveUdpDriverOptions
//

CBMikeLSSlaveUdpDriverOptionsUIList RCDATA
BEGIN
	
	"\0"	
END

CBMikeLSSlaveUdpDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Target Device Identification,IP,Port\0"
	
	"\0"	
END

// End of File
