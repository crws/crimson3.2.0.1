
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YAMTS_HPP
	
#define	INCLUDE_YAMTS_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Serial Driver Options
//

class CYamTsDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYamTsDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);


		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Serial Device Options
//

class CYamTsDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYamTsDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Station;
		
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Serial Driver
//

class CYamTsMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYamTsMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);

		// File Location
		UINT GetFile(CString Text, CSpace *pSpace, UINT *uOffsetEnd);
	};

// End of File

#endif
