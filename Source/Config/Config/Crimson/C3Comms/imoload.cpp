
#include "intern.hpp"

#include "imoload.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IMO Loader Port Driver
//

// Instantiator

ICommsDriver *	Create_IMOLoaderPortDriver(void)
{
	return New CIMOLoaderPortDriver;
	}

// Constructor

CIMOLoaderPortDriver::CIMOLoaderPortDriver(void)
{
	m_wID		= 0x4005;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "IMO";
	
	m_DriverName	= "K Series Loader Port";
	
	m_Version	= "1.00";
	
	m_ShortName	= "K Series";

	AddSpaces();
	}

// Binding Control

UINT	CIMOLoaderPortDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CIMOLoaderPortDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Helpers

BOOL CIMOLoaderPortDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT    uTable  = pSpace->m_uTable;

	UINT    uType   = pSpace->m_uType;

	UINT    uOffset = 0;

	BOOL    fFail   = FALSE;

	UINT    uPos    = Text.Find('.');

	CString Offset  = Text.Left(uPos);

	if( IsByteAndBit(pSpace) ) {

		PTXT    pError = NULL;

		CString sLeft  = Offset.Left(Offset.GetLength()-1);
		
		CString sRight = Offset.Right(1);

		uOffset = tstrtoul(sLeft , &pError, 10);
		
		if( pError && pError[0] ) {
			
			fFail = TRUE;
			}
		else {
			uOffset *= 16;

			uOffset += tstrtoul(sRight, &pError, 16);
			
			if( pError && pError[0] ) {

				fFail = TRUE;
				}
			}
		}
	else {
		PTXT pError = NULL;

		uOffset = tstrtoul(Offset, &pError, pSpace->m_uRadix);

		if( pError && pError[0] ) {

			fFail = TRUE;
			}
		}

	if( fFail ) {
	
		Error.Set( CString(IDS_ERROR_ADDR),
			   0
			   );
		
		return FALSE;
		}

	if( pSpace->IsOutOfRange(uOffset) ) {

		Error.Set( CString(IDS_ERROR_OFFSETRANGE),
			   0
			   );

		return FALSE;
		}
		
	if( pSpace->IsBadAlignment(uOffset) ) {
	
		Error.Set( CString(IDS_ERROR_BADOFFSET),
			   0
			   );
		
		return FALSE;
		}

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uOffset;
	
	Addr.a.m_Table	= uTable;
	
	Addr.a.m_Type	= uType;

	return TRUE;
	}

BOOL CIMOLoaderPortDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpace *pSpace = GetSpace(Addr);

		if( pSpace ) {

			if( IsByteAndBit(pSpace) ) {

				UINT uOffset = Addr.m_Ref & 0xFFF;

				Text.Printf( "%s%d%1.1X",
				     pSpace->m_Prefix,
					uOffset/16,
					uOffset%16
					);
				}

			else {

				Text.Printf( "%s%s",   
					pSpace->m_Prefix, 
					pSpace->GetValueAsText(Addr.a.m_Offset)
					);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

void CIMOLoaderPortDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"M",	"M Table - Aux",		16,  0,	191*16+15,	addrBitAsBit));

	AddSpace(New CSpace(2,	"P",	"P Table - I/O",		16,  0,	31*16+15,	addrBitAsBit));

	AddSpace(New CSpace(3,	"K",	"K Table - Keep",		16,  0,	31*16+15,	addrBitAsBit));

	AddSpace(New CSpace(4,	"L",	"L Table - Link",		16,  0,	63*16+15,	addrBitAsBit));

	AddSpace(New CSpace(5,	"F",	"F Table - Special",		16,  0, 63*16+15,	addrBitAsBit));

	AddSpace(New CSpace(6,	"D",	"Data Registers",		10,  0,	4999,		addrWordAsWord));

	AddSpace(New CSpace(7,	"TV",	"T Table - Timer Values",	10,  0,	255,		addrWordAsWord));

	AddSpace(New CSpace(8,	"TB",	"T0 Table - Timer Bits",	10,  0,	255,		addrBitAsBit));

	AddSpace(New CSpace(9,	"CV",	"C Table - Counter Values",	10,  0,	255,		addrWordAsWord));

	AddSpace(New CSpace(10,	"CB",	"C0 Table - Counter Bits",	10,  0,	255,		addrBitAsBit));

//	AddSpace(New CSpace(17,	"ER",	"Comms Error",			10,  0, 0,		addrLongAsLong));
	}

// Address Checking

BOOL CIMOLoaderPortDriver::IsByteAndBit(CSpace * pSpace)
{
	if( pSpace ) {

		return (pSpace->m_uTable <= 5 );
		}

	return FALSE;
	}

// End of File
