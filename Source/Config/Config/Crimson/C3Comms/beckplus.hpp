
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BECKHOFFPLUS_HPP
	
#define	INCLUDE_BECKHOFFPLUS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBeckhoffPlusTagsTCPDeviceOptions;
class CBeckhoffPlusTCPDeviceOptions;
class CBeckhoffPlusTagsTCPDriver;
class CBeckhoffPlusTCPDriver;
class CBeckhoffPlusTagsTCPDialog;

//////////////////////////////////////////////////////////////////////////
//
// Address Decoders
//

#define Index(r)	((r).a.m_Offset)

//////////////////////////////////////////////////////////////////////////
//
// Tag Data
//

struct CTagData {
	
	CString	m_Name;
	DWORD	m_Addr;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Decoders
//

#define Addr(a)		((CAddress &) (a).m_Addr)

//////////////////////////////////////////////////////////////////////////
//
// Space Definitions
//
// First 8 are the same selection numbers as previous driver
#define	SPFMEM	  1	// Fixed Memory Address
#define	SPFI	  2	// Fixed Input Address
#define	SPFO	  3	// Fixed Output Address
#define	SPRAS	 10	// Read ADS State
#define	SPRDS	 11	// Read Device State
#define	SPWC	 12	// Write Control
#define	SPWCA	 13	// Write Control ADS State
#define	SPWCD	 14	// Write Control Device State
// Addresses defined by controller, dependent on name string
#define	SPMEM	237	// Memory
#define	SPINP	238	// Inputs
#define	SPOUT	239	// Outputs

// String Selections
#define	STRFM	"Fixed Memory Address"
#define	STRFI	"Fixed Input Address"
#define	STRFO	"Fixed Output Address"
#define	STRRAS	"Read ADS State"
#define	STRRDS	"Read Device State"
#define	STRWC	"Execute Write Control (Bit)"
#define	STRWCA	"  ADS State for Write Control"
#define	STRWCD	"  Device State for Write Control"
#define	STRMEM	"Memory"
#define	STRINP	"Input"
#define	STROUT	"Output"

#define	STRUSED	"This name or function is already in use"

// String Selections Import/Export
#define	STRHDR	"Beckhoff Variable Names for "

//////////////////////////////////////////////////////////////////////////
//
// Utility Templates
//

inline int AfxCompare(CTagData const &d1, CTagData const &d2)
{
	if( Index(Addr(d1)) > Index(Addr(d2)) )	return +1;

	if( Index(Addr(d1)) < Index(Addr(d2)) )	return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Collections
//

typedef CArray <CTagData> CTagDataArray;

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus Tags TCP Device Options
//

class CBeckhoffPlusTagsTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBeckhoffPlusTagsTCPDeviceOptions(void);

		// Attributes
		CString GetDeviceName(void);

		// Operations
		BOOL	CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fNew);
		void	DeleteTag(CAddress const &Addr, BOOL fKill);
		BOOL	RenameTag(CString const &Name, CAddress const &Addr);
		CString	CreateName(CAddress Addr);
		void	ListTags(CTagDataArray &List);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void	Load(CTreeFile &Tree);
		void	Save(CTreeFile &Tree);

		// Download Support
		void	PrepareData(void);
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT           m_Push;

		// Type Definitions
		typedef	CMap   <CString, DWORD> CTagsFwdMap;
		typedef	CMap   <DWORD, CString> CTagsRevMap;
		typedef	CArray <DWORD>          CAddrArray;

		// Tag Map
		CTagsFwdMap	m_TagsFwdMap;
		CTagsRevMap	m_TagsRevMap;
		CAddrArray	m_AddrArray;
		CAddrArray	m_PrepAddr;
		CAddrArray	m_AddrDict;
		CStringArray	m_Tags;

	protected:
		// Data Members
		CArray <UINT>	m_Errors;
		CTagDataArray	m_List;

		// PrepareData Helper
		BOOL	StripDeviceName(CString *Name);

		// Meta Data Creation
		void	TagInit(void);
		void	AddMetaData(void);

		// Property Save Filter
		BOOL	SaveProp(CString Tag) const;

		// Implementation
		void	OnManage(CWnd *pWnd);
		void	OnImport(CWnd *pWnd);
		void	OnExport(CWnd *pWnd);
		
		CString CreateName(PCTXT pFormat);
		UINT    CreateIndex(void);
		CString	GetVarType(UINT uType);
		CString	GetDefVarText(UINT uTable);

		void	SaveTags(CTreeFile &Tree);
		void	LoadTags(CTreeFile &Tree);
		CString	GetTagName(CString const &Text);

		// Address Helpers
		UINT	GetIndex(CAddress const &Addr);
		BOOL	IsNotIndexed(UINT uTable);
		BOOL	IsFixed(UINT uTable);
		BOOL	UsesName(UINT uTable);

		// Dictionary Helper
		BOOL	InDictionary(DWORD m_Ref);

		// Import/Export Support
		void	Export(FILE *pFile);
		BOOL	Import(FILE *pFile);
		void	ParseType(CAddress &Addr, CString Text);
		void	ExpandType(CString &Text, CAddress const &Addr);
		void	ShowErrors(CWnd *pWnd);

		// Friends
		friend	class CBeckhoffPlusTagsTCPDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus TCP/IP Master Device Options
//

class CBeckhoffPlusTCPDeviceOptions : public CBeckhoffPlusTagsTCPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBeckhoffPlusTCPDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_AMSPort;
		UINT	m_Addr;
		UINT	m_Socket;
		UINT	m_Keep;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus Tags TCP Driver
//

class CBeckhoffPlusTagsTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBeckhoffPlusTagsTCPDriver(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus TCP Driver
//

class CBeckhoffPlusTCPDriver : public CBeckhoffPlusTagsTCPDriver
{
	public:
		// Constructor
		CBeckhoffPlusTCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CBeckhoff Plus TCP Selection Dialog
//

class CBeckhoffPlusTagsTCPDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBeckhoffPlusTagsTCPDialog(CBeckhoffPlusTagsTCPDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CBeckhoffPlusTagsTCPDialog(void);

		// Initialisation
		void	SetCaption(CString const &Text);
		void	SetSelect(BOOL fSelect);
		                
	protected:
		// Data
		CBeckhoffPlusTagsTCPDriver		*m_pDriver;
		CBeckhoffPlusTagsTCPDeviceOptions	*m_pConfig;
		CAddress	*m_pAddr;
		BOOL		m_fPart;
		CString		m_Caption;
		HTREEITEM	m_hRoot;
		HTREEITEM	m_hSelect;
		DWORD		m_dwSelect;
		CImageList	m_Images;
		BOOL		m_fLoad;
		BOOL		m_fSelect;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL	OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);

		// Command Handlers
		BOOL	OnOkay(UINT uID);
		BOOL	OnCreateTag(UINT uID);
		void	OnTypeChange(UINT uID, CWnd &Wnd);
		DWORD	GetTypeRef(void);

		// Tag Name Loading
		void	LoadNames(void);
		void	LoadNames(CTreeView &Tree);
		void	LoadRoot(CTreeView &Tree);
		void	LoadTags(CTreeView &Tree);
		void	LoadTag (CTreeView &Tree, CString Name, CAddress Addr);

		// Data Type Loading
		void	LoadTypes(void);
		void	AddType(UINT uTable, CComboBox &Combo, CString Text);
		UINT	GetDefaultType(UINT uTable);

		// Implementation
		void	ShowDetails(CAddress Addr);
		void	ShowAddress(CAddress Addr);
		BOOL	DeleteTag(void);
		BOOL	RenameTag(void);

		// Implementation
		BOOL	IsDown(UINT uCode);
		CString	FindNameFromAddr(CAddress Addr);

		// Radio button handling
		void	UpdateCombos(CAddress Addr);
		UINT	GetRadioButton(void);
		void	SetRadioButton(UINT uType);
		void	PutRadioButton(UINT uID, BOOL fYes);
		void	ClearRadioButtons(void);
		void	EnableRadioButtons(void);
		void	DisableRadioButtons(void);
		UINT	TypeToButton(UINT uType);
		UINT	ButtonToType(UINT uButton);
	};

// End of File

#endif
