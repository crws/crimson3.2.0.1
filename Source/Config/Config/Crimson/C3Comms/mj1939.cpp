

#include "intern.hpp"

#include "mj1939.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Monico J1939 Driver
//

// Instantiator

ICommsDriver * Create_MonicoJ1939Driver(void)
{
	return New CMonicoJ1939Driver;
	}

// Constructor			        

CMonicoJ1939Driver::CMonicoJ1939Driver(void)
{
	m_wID		= 0x4090;

	m_uType		= driverSlave;

	m_Manufacturer	= "SAE";
	
	m_DriverName	= "J1939";
	
	m_Version	= "1.01";
	
	m_ShortName	= "J1939";

	m_DevRoot	= "ECU";
	}

// Data Access

CString CMonicoJ1939Driver::GetFullPGString(CPGN * pPGN, BOOL fNum)
{
	CString Format = "";

	if( pPGN ) { 

		if( fNum ) {

			Format = pPGN->m_Prefix;     
					        
			Format += "\t";
		
			Format += pPGN->m_Title;
			}
		else {
			Format = pPGN->m_Prefix;
			}		
		}

	return Format;
	}

CString CMonicoJ1939Driver::GetPGString(CPGN * pPGN, UINT uOffset)
{
	CString Format = "ERROR";

	if( pPGN && pPGN->GetSPNs() ) {

		if( uOffset < SPN_MAX && uOffset < pPGN->GetSPNs()->GetItemCount() ) {

			if( pPGN->GetSPNs()->GetItem(uOffset)->GetSize() == 0 ) {

				return Format;
				}

			Format = pPGN->m_Prefix + CString(".");

			CString Number = "%5.5X.%4.4X.%u";

			UINT uSize = pPGN->GetSPNs()->GetItem(uOffset)->GetSize() & 0x7F;

			Number.Printf(Number, pPGN->m_Number * 2, (pPGN->GetSPNs()->GetItem(uOffset)->m_Number) * 2, uSize);

			Format += Number;
			}
		} 

	return Format;
	}


// End of File
