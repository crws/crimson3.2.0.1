
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WPMII_HPP
	
#define	INCLUDE_WPMII_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Definitions

#define	M5		0x8000


//////////////////////////////////////////////////////////////////////////
//
// CStiebelWpmIISerialDriver
//

class CStiebelWpmIISerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CStiebelWpmIISerialDriver(void);

		//Destructor
		~CStiebelWpmIISerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);


	protected:
		// Implementation
		void AddSpaces(void);
	};

#endif

// End of File