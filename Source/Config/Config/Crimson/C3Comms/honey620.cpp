
#include "intern.hpp"

#include "honey620.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CHoneywell620DriverOptions, CUIItem);

// Constructor

CHoneywell620DriverOptions::CHoneywell620DriverOptions(void)
{
	m_Connection = 0;
	}

// UI Management

void CHoneywell620DriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CHoneywell620DriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Connection));

	return TRUE;
	}

// Meta Data Creation

void CHoneywell620DriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Connection);
	}

//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Master
//

// Instantiator

ICommsDriver *	Create_HoneywellIPC620Driver(void)
{
	return New CHoneywellIPC620Driver;
	}

// Constructor

CHoneywellIPC620Driver::CHoneywellIPC620Driver(void)
{
	m_wID		= 0x3313;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Honeywell";
	
	m_DriverName	= "IPC620";
	
	m_Version	= "1.10";
	
	m_ShortName	= "Honeywell IPC620";

	AddSpaces();
	}

// Binding Control

UINT CHoneywellIPC620Driver::GetBinding(void)
{
	return bindStdSerial;
	}

// Configuration

CLASS CHoneywellIPC620Driver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CHoneywell620DriverOptions);
	}

void CHoneywellIPC620Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}

// Implementation

void CHoneywellIPC620Driver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "D", "Data Registers",			10, 4096, 9999, addrWordAsWord));
	AddSpace(New CSpace(2, "I", "I/O and Internal Registers",	10,    0, 9999, addrWordAsWord));
	}

// End of File
