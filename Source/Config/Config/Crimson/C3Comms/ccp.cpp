
#include "intern.hpp"

#include "ccp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Address
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationAddress, CMetaItem);

// Constructor

CCANCalibrationAddress::CCANCalibrationAddress(void)
{
	m_Address   = 0;
	m_Extension = 0;
	m_Size      = 0;
	m_SizeX     = 0;
	m_SizeY     = 0;
	m_fDaq      = 0;
	m_Type      = 0;
	m_Ref       = 0;
	m_CanId     = 0;
	m_DaqId     = 0;
	m_Odt       = 0;
	m_Elem      = 0;
	m_DaqOffset = 0;
	m_Radix     = 0;
	m_Scale     = 0;
	m_Offset    = 0;
	}

// Download Support

BOOL CCANCalibrationAddress::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddLong(m_Address);
	Init.AddByte(BYTE(m_Extension));
	Init.AddByte(BYTE(m_Size));
	Init.AddLong(m_SizeX);
	Init.AddLong(m_SizeY);
	Init.AddByte(BYTE(m_fDaq));
	Init.AddByte(BYTE(m_Type));
	Init.AddLong(m_Ref);
	Init.AddLong(m_CanId);
	Init.AddByte(BYTE(m_DaqId));
	Init.AddByte(BYTE(m_Odt));
	Init.AddByte(BYTE(m_Elem));
	Init.AddByte(BYTE(m_DaqOffset));
	Init.AddLong(m_Radix);
	Init.AddLong(m_Scale);
	Init.AddLong(m_Offset);

	return TRUE;
	}

// Copying

void CCANCalibrationAddress::InitFrom(CCANCalibrationAddress const &That)
{
	m_Name      = That.m_Name;
	m_Address   = That.m_Address;
	m_Extension = That.m_Extension;
	m_Size      = That.m_Size;
	m_SizeX     = That.m_SizeX;
	m_SizeY     = That.m_SizeY;
	m_fDaq      = That.m_fDaq;
	m_DaqOffset = That.m_DaqOffset;
	m_Type      = That.m_Type;
	m_Ref       = That.m_Ref;
	m_CanId     = That.m_CanId;
	m_DaqId     = That.m_DaqId;
	m_Odt       = That.m_Odt;
	m_Elem      = That.m_Elem;
	m_Radix     = That.m_Radix;
	m_Scale     = That.m_Scale;
	m_Offset    = That.m_Offset;
	}

// Comparison Operators

bool CCANCalibrationAddress::operator ==(const CCANCalibrationAddress &That) const
{
	return m_Ref == That.m_Ref;
	}

bool CCANCalibrationAddress::operator < (const CCANCalibrationAddress &That) const
{
	return m_Ref < That.m_Ref;	
	}

bool CCANCalibrationAddress::operator > (const CCANCalibrationAddress &That) const
{
	return m_Ref > That.m_Ref;
	}

// Meta Data Creation

void CCANCalibrationAddress::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Address);
	Meta_AddInteger(Extension);
	Meta_AddInteger(Size);
	Meta_AddInteger(SizeX);
	Meta_AddInteger(SizeY);
	Meta_AddBoolean(Daq);
	Meta_AddInteger(DaqOffset);
	Meta_AddInteger(Type);
	Meta_AddInteger(Ref);

	Meta_AddInteger(CanId);
	Meta_AddInteger(DaqId);
	Meta_AddInteger(Odt);
	Meta_AddInteger(Elem);

	Meta_AddInteger(Radix);
	Meta_AddInteger(Scale);
	Meta_AddInteger(Offset);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Master Address List
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationAddressList, CItemList);

// Destructor

CCANCalibrationAddressList::~CCANCalibrationAddressList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CCANCalibrationAddress * CCANCalibrationAddressList::GetItem(INDEX Index) const
{
	return (CCANCalibrationAddress *) CItemList::GetItem(Index);
	}

CCANCalibrationAddress * CCANCalibrationAddressList::GetItem(UINT uPos) const
{
	return (CCANCalibrationAddress *) CItemList::GetItem(uPos);
	}

CCANCalibrationAddress * CCANCalibrationAddressList::FindByRef(UINT uRef) const
{
	UINT uTable = (uRef & 0x00FF0000) >> 16;

	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationAddress * pAddr = (CCANCalibrationAddress *) GetItem(i);

		if( uTable < addrNamed ) {

			if( uTable == ((pAddr->m_Ref & 0x00FF0000) >> 16) ) {

				return pAddr;
				}
			}
		else {
			if( pAddr->m_Ref == uRef ) {

				return pAddr;
				}
			}
		}

	return NULL;
	}

CCANCalibrationAddress * CCANCalibrationAddressList::FindByName(CString const &Name) const
{
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationAddress * pAddr = (CCANCalibrationAddress *) GetItem(i);

		if( pAddr->m_Name == Name ) {

			return pAddr;
			}
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 SPN Defintion
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationSPN, CMetaItem);

// Constructors

CCANCalibrationSPN::CCANCalibrationSPN(void)
{
	m_Number = 0;

	m_Bits   = 1;
	}

CCANCalibrationSPN::CCANCalibrationSPN(CString const &Name, CString const& Abbrev, UINT uNumber, UINT uBits)
{
	m_Name   = Name;

	m_Abbrev = Abbrev;

	m_Number = uNumber;

	m_Bits   = uBits;
	}

// Download Support

BOOL CCANCalibrationSPN::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddLong(m_Number);

	Init.AddLong(m_Bits);

	Init.AddLong(m_Offset);
	
	return TRUE;
	}

// Meta Data Creation
		
void CCANCalibrationSPN::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString(Name);

	Meta_AddString(Abbrev);

	Meta_AddInteger(Number);

	Meta_AddInteger(Bits);

	Meta_AddInteger(Offset);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 SPN List
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationSPNList, CItemList);

// Destructor

CCANCalibrationSPNList::~CCANCalibrationSPNList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CCANCalibrationSPN * CCANCalibrationSPNList::GetItem(INDEX Index) const
{
	return (CCANCalibrationSPN *) CItemList::GetItem(Index);
	}

CCANCalibrationSPN * CCANCalibrationSPNList::GetItem(UINT  uPos ) const
{
	return (CCANCalibrationSPN *) CItemList::GetItem(uPos);
	}

CCANCalibrationSPN * CCANCalibrationSPNList::FindByNumber(UINT uNum) const
{
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationSPN * pSPN = GetItem(i);

		if( pSPN->m_Number == uNum ) {

			return pSPN;
			}
		}

	return NULL;
	}

CCANCalibrationSPN * CCANCalibrationSPNList::FindByName(CString const &Name) const
{	
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationSPN * pSPN = GetItem(i);

		if( pSPN->m_Name == Name ) {

			return pSPN;
			}
		}

	return NULL;
	}

CCANCalibrationSPN * CCANCalibrationSPNList::FindByAbbrev(CString const &Abbrev) const
{
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationSPN * pSPN = GetItem(i);

		if( pSPN->m_Abbrev == Abbrev ) {

			return pSPN;
			}
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 PGN Defintion
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationPGN, CMetaItem);

// Constructors

CCANCalibrationPGN::CCANCalibrationPGN(void)
{
	m_BitOffset = 0;

	m_Number    = 0;

	m_Priority  = 6;

	m_Update    = 1000;

	m_fCommand  = FALSE;

	m_fUser     = FALSE;

	m_pSPNs     = New CCANCalibrationSPNList;
	}

CCANCalibrationPGN::CCANCalibrationPGN(CString const &Name, CString const &Abbrev, UINT uNumber, UINT uPriority, BOOL fCommand, BOOL fUser, UINT uUpdate)
{
	m_Name      = Name;

	m_Abbrev    = Abbrev;

	m_BitOffset = 0;

	m_Number    = uNumber;

	m_Priority  = uPriority;

	m_Update    = uUpdate;

	m_fCommand  = fCommand;

	m_fUser     = fUser;

	m_pSPNs     = New CCANCalibrationSPNList;
	}

// Download Support

BOOL CCANCalibrationPGN::MakeInitData(CInitData &Init)
{	
	CMetaItem::MakeInitData(Init);

	Init.AddLong(m_Number);

	Init.AddByte(BYTE(m_Priority));

	Init.AddLong(m_Update);

	Init.AddByte(BYTE(m_fCommand));

	m_pSPNs->MakeInitData(Init);

	return FALSE;
	}

BOOL CCANCalibrationPGN::AddSPN(CCANCalibrationSPN * pSPN)
{
	pSPN->m_Offset = m_BitOffset;

	m_pSPNs->AppendItem(pSPN);

	m_BitOffset += pSPN->m_Bits;

	return TRUE;
	}

BOOL CCANCalibrationPGN::RemoveSPN(CCANCalibrationSPN * pSPN)
{
	// LATER

	return FALSE;
	}

CCANCalibrationSPN * CCANCalibrationPGN::FindSPN(CString const &Abbrev)
{
	return m_pSPNs->FindByAbbrev(Abbrev);
	}

CCANCalibrationSPN * CCANCalibrationPGN::SPNAt(UINT uPos)
{
	return m_pSPNs->GetItem(uPos);
	}

UINT CCANCalibrationPGN::FindSPNOffset(CCANCalibrationSPN * pSPN)
{
	for( UINT n = 0; n < m_pSPNs->GetItemCount(); n++ ) {

		if( m_pSPNs->GetItem(n)->m_Abbrev == pSPN->m_Abbrev ) {

			return n;
			}
		}

	return NOTHING;
	}

// Comparison Operators

bool CCANCalibrationPGN::operator ==(const CCANCalibrationPGN &That) const
{
	return m_Number == That.m_Number;
	}

bool CCANCalibrationPGN::operator < (const CCANCalibrationPGN &That) const
{
	return m_Number < That.m_Number;
	}

bool CCANCalibrationPGN::operator > (const CCANCalibrationPGN &That) const
{
	return m_Number > That.m_Number;
	}

// Meta Data Creation

void CCANCalibrationPGN::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString(Name);

	Meta_AddString(Abbrev);

	Meta_AddInteger(Number);

	Meta_AddInteger(Priority);

	Meta_AddInteger(Update);

	Meta_AddInteger(BitOffset);

	Meta_AddBoolean(Command);

	Meta_AddBoolean(User);

	Meta_AddCollect(SPNs);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration J1939 PGN List
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationPGNList, CItemList)

// Destructor

CCANCalibrationPGNList::~CCANCalibrationPGNList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CCANCalibrationPGN * CCANCalibrationPGNList::GetItem(INDEX Index) const
{
	return (CCANCalibrationPGN *) CItemList::GetItem(Index);
	}

CCANCalibrationPGN * CCANCalibrationPGNList::GetItem(UINT  uPos ) const
{
	return (CCANCalibrationPGN *) CItemList::GetItem(uPos);
	}

CCANCalibrationPGN * CCANCalibrationPGNList::FindByNumber(UINT uNum) const
{
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationPGN *pPGN = GetItem(i);

		if( pPGN->m_Number == uNum ) {

			return pPGN;
			}
		}

	return NULL;
	}

CCANCalibrationPGN * CCANCalibrationPGNList::FindByName(CString const &Name) const
{
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationPGN *pPGN = GetItem(i);

		if( pPGN->m_Name == Name ) {

			return pPGN;
			}
		}

	return NULL;
	}

CCANCalibrationPGN * CCANCalibrationPGNList::FindByAbbrev(CString const &Abbrev) const
{
	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CCANCalibrationPGN *pPGN = GetItem(i);

		if( pPGN->m_Abbrev == Abbrev ) {

			return pPGN;
			}
		}

	return NULL;
	}

/////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationDriverOptions, CUIItem);

// Constructor

CCANCalibrationDriverOptions::CCANCalibrationDriverOptions(void)
{	
	m_MasterJ1939 = 2;
	}

// Download Support

BOOL CCANCalibrationDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_MasterJ1939));

	return TRUE;
	}
	
// Meta Data Creation

void CCANCalibrationDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(MasterJ1939);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANCalibrationDeviceOptions, CUIItem);

// Constructor

CCANCalibrationDeviceOptions::CCANCalibrationDeviceOptions(void)
{
	m_Ping      = TRUE;

	m_Push       = 0;

	m_Station    = 0;

	m_Timeout    = 1000;

	m_DaqTime    = 1000;

	m_MasterId   = 0;

	m_SlaveId    = 0;

	m_SlaveJ1939 = 0;

	m_Order      = 0;

	m_Key        = 0;
	
	m_pAddresses = New CCANCalibrationAddressList;

	m_pPGNs      = New CCANCalibrationPGNList;
	}

// UI Managament

void CCANCalibrationDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Push" ) {
			
			if( m_Push == 0 ) {

				HGLOBAL hPrev = m_pAddresses->TakeSnapshot();

				CCANCalibrationAddressListDialog Dlg(FALSE, this);

				if( Dlg.Execute(pHost->GetWindow()) ) {

					HGLOBAL hData = m_pAddresses->TakeSnapshot();

					CCmdSubItem * pCmd = New CCmdSubItem( L"Config/Addresses",
									      hPrev,
									      hData
									      );

					if( pCmd->IsNull() ) {

						delete pCmd;
						}
					else {
						pHost->SaveExtraCmd(pCmd);

						SetDirty();
						}

					CSystemItem *pSystem = GetDatabase()->GetSystemItem();

					if( pSystem ) {

						pSystem->Rebuild(1);
						}
					}
				else {
					m_pAddresses->LoadSnapshot(hPrev);

					GlobalFree(hPrev);
					}
				}

			// LATER -- import/export addresses
			}
		}
	}

// Download Support

void CCANCalibrationDeviceOptions::PrepareData(void)
{
	m_uTableMax = 0;

	m_uNamedMax = 0;

	for( INDEX i = m_pAddresses->GetHead(); !m_pAddresses->Failed(i); m_pAddresses->GetNext(i) ) {

		CCANCalibrationAddress *pAddress =  m_pAddresses->GetItem(i);

		UINT uTab = (pAddress->m_Ref & 0x00FF0000) >> 16;

		if( uTab < addrNamed ) {

			UINT uPos = uTab;

			MakeMax(m_uTableMax, uPos);
			}
		else {
			UINT uPos = pAddress->m_Ref & 0xFFFF;

			MakeMax(m_uNamedMax, uPos);
			}
		}
	}

BOOL CCANCalibrationDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Station));
	Init.AddLong(m_Timeout);
	Init.AddLong(m_DaqTime);
	Init.AddLong(m_MasterId);
	Init.AddLong(m_SlaveId);
	Init.AddByte(BYTE(m_SlaveJ1939));
	Init.AddByte(BYTE(m_Order));
	Init.AddLong(m_Key);
	Init.AddLong(m_uTableMax);
	Init.AddLong(m_uNamedMax);
	
	m_pAddresses->MakeInitData(Init);

	m_pPGNs->MakeInitData(Init);
	
	return TRUE;
	}

// Persistance

void CCANCalibrationDeviceOptions::Init(void)
{
	CUIItem::Init();

	MakeDefaultPGNs();
	}

void CCANCalibrationDeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();
	
	for( INDEX i = m_pPGNs->GetHead(); !m_pPGNs->Failed(i); m_pPGNs->GetNext(i) ) {

		if( m_pPGNs->GetItem(i)->m_fUser ) {

			m_pPGNs->RemoveItem(i);
			}
		}
	
	if( m_pPGNs->GetItemCount() == 0 ) {

		MakeDefaultPGNs();
		}
	}

BOOL CCANCalibrationDeviceOptions::DoParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CCANCalibrationAddress * pAddr = NULL;

	CString Name;

	UINT uPos = NOTHING;

	if( Text.Find('$') < NOTHING ) {

		return ParseJ1939DM(Error, Addr, Text);
		}

	if( (uPos = Text.Find('[')) < NOTHING ) {

		Name = Text.Mid(0, uPos);
		}
	else {
		Name = Text;
		}
	
	if( (pAddr = FindAddress(Name)) ) {

		Addr.m_Ref     = pAddr->m_Ref;

		Addr.a.m_Type  = FindType(*pAddr);

		if( Addr.a.m_Table < addrNamed ) {

			UINT uOffset = FindOffset(Error, Text, pAddr);

			if( uOffset == NOTHING ) {

				return FALSE;
				}

			Addr.a.m_Offset = uOffset;
			}

		return TRUE;
		}
	
	Error.Set(CString(IDS_THAT_ADDRESS_DOES));

	return FALSE;
	}

BOOL CCANCalibrationDeviceOptions::DoExpandAddress(CString &Text, CAddress const &Addr)
{
	if( Addr.a.m_Extra == 1 ) {

		CCANCalibrationPGN * pPGN = PGNAt(Addr.a.m_Table);

		if( pPGN ) {

			CCANCalibrationSPN * pSPN = pPGN->SPNAt(Addr.a.m_Offset);

			if( pSPN ) {

				Text.Printf("$%s.%s", pPGN->m_Abbrev, pSPN->m_Abbrev);

				return TRUE;
				}
			}

		return FALSE;
		}
	
	UINT uRef = (Addr.a.m_Table << 16) + Addr.a.m_Offset;

	CCANCalibrationAddress * pAddr = NULL;

	if( (pAddr = m_pAddresses->FindByRef(uRef)) ) {
		
		Text = pAddr->m_Name;

		if( Addr.a.m_Table < addrNamed ) {

			UINT uOffsetY = Addr.a.m_Offset / pAddr->m_SizeX;

			UINT uOffsetX = Addr.a.m_Offset - (uOffsetY * pAddr->m_SizeX);

			if( pAddr->m_SizeX == 1 ) {

				Text += CPrintf("[%u]", uOffsetY);
				}

			else if( pAddr->m_SizeY == 1 ) {

				Text += CPrintf("[%u]", uOffsetX);
				}
			else {
				Text += CPrintf("[%u][%u]", uOffsetY, uOffsetX);
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationDeviceOptions::DoSelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CCANCalibrationAddressListDialog Dlg(TRUE, this, Addr);

	if( Dlg.Execute(CWnd::FromHandle(hWnd)) ) {

		Dlg.GetAddress(Addr);

		return TRUE;
		}

	return FALSE;
	}

// Address Management

BOOL CCANCalibrationDeviceOptions::AddAddress(CCANCalibrationAddress const &Addr, CError &Error)
{
	if( !ValidateName(Addr.m_Name) ) {

		Error.Set(CString(IDS_ADDRESS_NAME));

		return FALSE;
		}

	CCANCalibrationAddress * pCopy = New CCANCalibrationAddress;

	pCopy->InitFrom(Addr);

	CCANCalibrationAddress *pTest = m_pAddresses->FindByName(Addr.m_Name);

	if( !pTest ) {

		pCopy->m_Ref = AllocRef(*pCopy);

		m_pAddresses->AppendItem(pCopy);

		return TRUE;
		}

	delete pCopy;

	Error.Set(CString(IDS_UNABLE_TO_CREATE));

	return FALSE;
	}

BOOL CCANCalibrationDeviceOptions::RemoveAddress(CCANCalibrationAddress const &Addr)
{
	INDEX i = m_pAddresses->FindItemIndex(&Addr);

	return m_pAddresses->RemoveItem(i);
	}

CCANCalibrationAddress * CCANCalibrationDeviceOptions::FindAddress(CString Name)
{
	for( INDEX i = m_pAddresses->GetHead(); !m_pAddresses->Failed(i); m_pAddresses->GetNext(i) ) {

		CCANCalibrationAddress *pAddress =  m_pAddresses->GetItem(i);

		if( pAddress->m_Name == Name ) {

			return pAddress;
			}
		}

	return NULL;
	}

// Meta Data Creation

void CCANCalibrationDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Ping);
	Meta_AddInteger(Push);
	Meta_AddInteger(Station);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(DaqTime);
	Meta_AddInteger(MasterId);
	Meta_AddInteger(SlaveId);
	Meta_AddInteger(SlaveJ1939);
	Meta_AddInteger(Order);
	Meta_AddInteger(Key);
	Meta_AddCollect(Addresses);
	Meta_AddCollect(PGNs);
	}

// Implementation

UINT CCANCalibrationDeviceOptions::AllocRef(CCANCalibrationAddress const &Address)
{
	if( Address.m_SizeX > 1 || Address.m_SizeY > 1 ) {

		return FindNextTable();
		}
	else { 
		return FindNextNamed();
		}

	return 0;
	}

UINT CCANCalibrationDeviceOptions::FindNextNamed(void)
{
	UINT uRef = addrNamed << 16;

	while( m_pAddresses->FindByRef(uRef) ) {

		uRef++;
		}

	return uRef;
	}

UINT CCANCalibrationDeviceOptions::FindNextTable(void)
{
	UINT uTable = 1;

	UINT uRef   = uTable << 16;

	CCANCalibrationAddress * pAddr = NULL;

	while( (pAddr = m_pAddresses->FindByRef(uRef)) ) {

		uTable++;

		uRef = uTable << 16;
		}

	return uRef;
	}

UINT CCANCalibrationDeviceOptions::FindOffset(CError &Error, CString Text, CCANCalibrationAddress * pAddr)
{
	UINT uOffsetX = 0;

	UINT uOffsetY = 0;

	UINT uBrackets = Text.Count(L'[');

	if( uBrackets == 0 ) {

		Error.Set(CString(IDS_OFFSET_IS));
		}

	UINT uPos = Text.Find('[');

	Text = Text.Mid(uPos + 1);

	if( uBrackets == 1 ) {

		if( pAddr->m_SizeX == 1 ) {

			uOffsetX = 0;

			uOffsetY = tstrtoul(Text, NULL, 10);
			}
		
		else if( pAddr->m_SizeY == 1 ) {

			uOffsetY = 0;

			uOffsetX = tstrtoul(Text, NULL, 10);
			}
		else {
			Error.Set(CString(IDS_THIS_DIMENSIONAL));

			return NOTHING;
			}

		return uOffsetX + uOffsetY * pAddr->m_SizeX;
		}

	if( uBrackets == 2 ) {

		uOffsetY = tstrtoul(Text, NULL, 10);

		uPos = Text.Find('[');

		if( uPos != NOTHING ) {

			Text = Text.Mid(uPos + 1);

			uOffsetX = tstrtoul(Text, NULL, 10);
			}

		if( uOffsetX >= pAddr->m_SizeX || uOffsetY >= pAddr->m_SizeY ) {

			Error.Set(CString(IDS_OFFSET_IS_OUT_OF));

			return NOTHING;
			}

		return uOffsetX + uOffsetY * pAddr->m_SizeX;
		}

	Error.Set(CString(IDS_ADDRESS_FORMAT_IS));

	return NOTHING;
	}

BOOL CCANCalibrationDeviceOptions::ValidateName(CString const &Name)
{
	for( UINT n = 0; n < Name.GetLength(); n++ ) {

		if( !isalnum(Name[n]) && Name[n] != '_' ) {

			return FALSE;
			}
		}

	return TRUE;
	}

UINT CCANCalibrationDeviceOptions::FindType(CCANCalibrationAddress const &Address)
{
	switch( Address.m_Size ) {

		case 1: return addrByteAsByte;

		case 2: return addrWordAsWord;

		case 4: 
			BYTE bFlag = Address.m_Type & 0x0F;

			return bFlag == 2 ? addrRealAsReal : addrLongAsLong;
		}

	return addrWordAsWord;
	}

// Limited J1939 Support

BOOL CCANCalibrationDeviceOptions::AddPGN(CCANCalibrationPGN * pPGN)
{
	m_pPGNs->AppendItem(pPGN);

	return TRUE;
	}

BOOL CCANCalibrationDeviceOptions::EditPGN(CCANCalibrationPGN * pPGN)
{
	// LATER

	return FALSE;
	}

BOOL CCANCalibrationDeviceOptions::RemovePGN(CCANCalibrationPGN * pPGN)
{
	// LATER

	return FALSE;
	}


void CCANCalibrationDeviceOptions::MakeDefaultPGNs(void)
{
	MakeDM1();
	MakeDM2();
	MakeDM3();
	MakeDM4();
	MakeDM11();
	}

void CCANCalibrationDeviceOptions::MakeDM1(void)
{
	CCANCalibrationPGN * pPGN = New CCANCalibrationPGN( "Active Diagnostic Trouble Codes",
							    "DM1",
							    65226,
							    6,
							    FALSE,
							    FALSE,
							    0
							    );

	AddPGN(pPGN);

	SPNDef Defs[] = {
			
		{ "Protect Lamp",			"PL" ,	 987,	2 },
		{ "Amber Warning Lamp",			"AWL",	 624,	2 },
		{ "Red Stop Lamp",			"RSL",	 623,	2 },	
		{ "Malfunction Indicator Lamp",		"MIL",	1213,	2 },
		{ "Flash Protect Lamp",			"FPL" ,	3041,	2 },
		{ "Flash Amber Warning Lamp",		"FAWL",	3040,	2 },		
		{ "Flash Red Stop Lamp",		"FRSL",	3039,	2 },
		{ "Flash Malfunction Indicator Lamp",	"FMIL",	3038,	2 },
	};

	UINT n;

	for( n = 0; n < elements(Defs); n++ ) {

		pPGN->AddSPN(New CCANCalibrationSPN(Defs[n].m_Name, Defs[n].m_Abbrev, Defs[n].m_Number, Defs[n].m_Type));
		}

	SPNDef Codes[] = {

		{ "Suspect Parameter Number %u",	"SPN%u",	1214,	19 },
		{ "Failure Mode Identifier %u",		"FMI%u",	1215,	 5 },
		{ "SPN Conversion Method %u",		"SCM%u",	1706,	 1 },		
		{ "Occurrence Count %u",		"OC%u" ,	1216,	 7 },
	};
	
	for( n = 1; n <= 50; n++ ) {

		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[0].m_Name, n), CPrintf(Codes[0].m_Abbrev, n), Codes[0].m_Number, Codes[0].m_Type));
		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[1].m_Name, n), CPrintf(Codes[1].m_Abbrev, n), Codes[1].m_Number, Codes[1].m_Type));
		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[2].m_Name, n), CPrintf(Codes[2].m_Abbrev, n), Codes[2].m_Number, Codes[2].m_Type));
		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[3].m_Name, n), CPrintf(Codes[3].m_Abbrev, n), Codes[3].m_Number, Codes[3].m_Type));
		}
	}

void CCANCalibrationDeviceOptions::MakeDM2(void)
{
	CCANCalibrationPGN * pPGN = New CCANCalibrationPGN("Previously Active Diagnostic Trouble Codes",
							   "DM2",
							   65227,
							   6,
							   FALSE,
							   FALSE
							   );

	AddPGN(pPGN);

	SPNDef Defs[] = {

		{ "Protect Lamp",			"PL" ,	 987,	2 },
		{ "Amber Warning Lamp",			"AWL",	 624,	2 },
		{ "Red Stop Lamp",			"RSL",	 623,	2 },
		{ "Malfunction Indicator Lamp",		"MIL",	1213,	2 },	
		{ "Flash Protect Lamp",			"FPL" ,	3041,	2 },
		{ "Flash Amber Warning Lamp",		"FAWL",	3040,	2 },
		{ "Flash Red Stop Lamp",		"FRSL",	3039,	2 },
		{ "Flash Malfunction Indicator Lamp",	"FMIL",	3038,	2 },
	};

	UINT n;

	for( n = 0; n < elements(Defs); n++ ) {

		pPGN->AddSPN(New CCANCalibrationSPN(Defs[n].m_Name, Defs[n].m_Abbrev, Defs[n].m_Number, Defs[n].m_Type));
		}

	SPNDef Codes[] = {

		{ "Suspect Parameter Number %u",	"SPN%u",	1214,	19 },
		{ "Failure Mode Identifier %u",		"FMI%u",	1215,	 5 },
		{ "SPN Conversion Method %u",		"SCM%u",	1706,	 1 },		
		{ "Occurrence Count %u",		"OC%u" ,	1216,	 7 },
	};
	
	for( n = 1; n <= 50; n++ ) {

		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[0].m_Name, n), CPrintf(Codes[0].m_Abbrev, n), Codes[0].m_Number, Codes[0].m_Type));
		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[1].m_Name, n), CPrintf(Codes[1].m_Abbrev, n), Codes[1].m_Number, Codes[1].m_Type));
		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[2].m_Name, n), CPrintf(Codes[2].m_Abbrev, n), Codes[2].m_Number, Codes[2].m_Type));
		pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Codes[3].m_Name, n), CPrintf(Codes[3].m_Abbrev, n), Codes[3].m_Number, Codes[3].m_Type));
		}
	}

void CCANCalibrationDeviceOptions::MakeDM3(void)
{
	CCANCalibrationPGN * pPGN = New CCANCalibrationPGN("Reset Previous DTCs",
							   "DM3",
							   65228,
							   6,
							   TRUE,
							   FALSE,
							   0
							   );

	AddPGN(pPGN);
	}		

void CCANCalibrationDeviceOptions::MakeDM4(void)
{
	CCANCalibrationPGN * pPGN = New CCANCalibrationPGN("Freeze Frame Parameters",
							   "DM4",
							   65229,
							   6,
							   FALSE,
							   FALSE
							   );

	AddPGN(pPGN);

	SPNDef Defs[] = {

		{ "Freeze Frame Length %u",		"FFL%u",	NOTHING,	 8 },
		{ "Suspect Parameter Number %u",	"SPN%u",	   1214,	19 },
		{ "Failure Mode Identifier %u",		"FMI%u",	   1215,	 5 },
		{ "Occurrence Count %u",		"OC%u" ,	   1216,	 7 },
		{ "SPN Conversion Method %u",		"SCM%u",	   1706,	 1 },		
		{ "Engine Torque Mode %u",		"ETM%u",	    899,         8 },
		{ "Boost (MAP) %u",			"BST%u",	    102,	 8 },
		{ "Engine Speed %u",			"ES%u",		    190,	16 },
		{ "Engine %% Load %u",			"EPL%u",	     92,	 8 },
		{ "Engine Coolant Temperature %u",	"ECT%u",	    110,	 8 },
		{ "Vehicle Speed %u",			"VS%u",		     84,	16 },
		{ "Starts Since Active %u",		"SSA%u",	     84,	 8 },
		{ "FSS Index %u",			"FSS%u",	 NOTHING,	 8 },
		};


	for( UINT m = 1; m <= 10; m++ ) {

		for( UINT n = 0; n < elements(Defs); n++ ) {

			pPGN->AddSPN(New CCANCalibrationSPN(CPrintf(Defs[n].m_Name, m), CPrintf(Defs[n].m_Abbrev, m), Defs[n].m_Number, Defs[n].m_Type));
			}
		}

	
	}

void CCANCalibrationDeviceOptions::MakeDM11(void)
{
	CCANCalibrationPGN * pPGN = New CCANCalibrationPGN("Reset For Active DTCs",
							   "DM11",
							   65235,
							   6,
							   TRUE,
							   FALSE,
							   0
							   );

	AddPGN(pPGN);
	}

BOOL CCANCalibrationDeviceOptions::ParseJ1939DM(CError &Error, CAddress &Addr, CString Text)
{
	Addr.m_Ref = 0;

	UINT uPos = Text.Find(L"$");

	if( uPos < NOTHING ) {

		Text = Text.Mid(uPos + 1);

		uPos = Text.Find(L".");

		if( uPos < NOTHING ) {

			CString Abbrev = Text.Left(uPos);

			Text = Text.Mid(uPos + 1);

			CCANCalibrationPGN * pPGN = m_pPGNs->FindByAbbrev(Abbrev);

			if( pPGN ) {

				CCANCalibrationSPN * pSPN = pPGN->FindSPN(Text);

				if( pSPN ) {

					Addr.a.m_Extra  = 1;

					Addr.a.m_Type   = addrLongAsLong;
		
					Addr.a.m_Table  = FindPGNOffset(pPGN);

					Addr.a.m_Offset = pPGN->FindSPNOffset(pSPN);

					return TRUE;
					}
				}
			}
		}

	Error.Set(CString(IDS_THAT_IS));

	return FALSE;
	}

UINT CCANCalibrationDeviceOptions::FindPGNOffset(CCANCalibrationPGN * pPGN)
{
	for( UINT n = 0; n < m_pPGNs->GetItemCount(); n++ ) {

		if( m_pPGNs->GetItem(n)->m_Number == pPGN->m_Number ) {

			return n;
			}
		}

	return NOTHING;
	}

CCANCalibrationPGN * CCANCalibrationDeviceOptions::PGNAt(UINT uPos)
{	
	return m_pPGNs->GetItem(uPos);
	}

// Debug

void CCANCalibrationDeviceOptions::DumpPGNs(void)
{
	for( UINT n = 0; n < m_pPGNs->GetItemCount(); n++ ) {

		CCANCalibrationPGN *pPGN = m_pPGNs->GetItem(n);

		AfxTrace(L"%u (%s) %s:\n",
			pPGN->m_Number,
			PCTXT(pPGN->m_Abbrev),
			PCTXT(pPGN->m_Name)
			);

		CCANCalibrationSPNList * pList = pPGN->m_pSPNs;

		for( UINT m = 0; m < pList->GetItemCount(); m++ ) {
			
			CCANCalibrationSPN * pSPN = pList->GetItem(m);

			AfxTrace(L"\t %u (%s) %s\n",
				pSPN->m_Number,
				pSPN->m_Abbrev,
				pSPN->m_Name
				);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Driver
//

// Instantiator

ICommsDriver * Create_CANCalibrationDriver(void)
{
	return New CCANCalibrationDriver;
	}

// Constructor

CCANCalibrationDriver::CCANCalibrationDriver(void)
{
	m_wID          = 0x40E0;

	m_uType        = driverMaster;

	m_Manufacturer = "CAN";

	m_DriverName   = "CCP Master";
	 
	m_Version      = "1.00";

	m_ShortName    = "CCP";

	m_DevRoot      = "ECU";
	}

// Configuration

CLASS CCANCalibrationDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCANCalibrationDeviceOptions);
	}

CLASS CCANCalibrationDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CCANCalibrationDriverOptions);
	}

// Binding Control

UINT CCANCalibrationDriver::GetBinding(void)
{
	return bindCAN;
	}

void CCANCalibrationDriver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 250000;

	CAN.m_Terminate = FALSE;
	}

// Address Management

BOOL CCANCalibrationDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CCANCalibrationDeviceOptions * pDevice = (CCANCalibrationDeviceOptions *) pConfig;

	pDevice->DoSelectAddress(hWnd, Addr, fPart);
	
	return TRUE;
	}

BOOL CCANCalibrationDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CCANCalibrationDeviceOptions * pDevice = (CCANCalibrationDeviceOptions *) pConfig;

	return pDevice->DoParseAddress(Error, Addr, Text);
	}

BOOL CCANCalibrationDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CCANCalibrationDeviceOptions * pDevice = (CCANCalibrationDeviceOptions *) pConfig;

	return pDevice->DoExpandAddress(Text, Addr);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Configuration Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCANCalibrationCreateEditDialog, CStdDialog);

// Constructor

CCANCalibrationCreateEditDialog::CCANCalibrationCreateEditDialog(CCANCalibrationAddress * pAddr, CCANCalibrationDeviceOptions * pOptions)
{
	SetName(L"CANCalibrationCreateEditDialog");

	m_pDevice = pOptions;

	m_pAddr   = pAddr;
	}

// Dialog Result

CCANCalibrationAddress * CCANCalibrationCreateEditDialog::GetAddress(void)
{
	return m_pAddr;
	}

// Message Map

AfxMessageMap(CCANCalibrationCreateEditDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(4001, CBN_SELCHANGE, OnSelChange)

	AfxMessageEnd(CCANCalibrationCreateEditDialog)
	};

// Message Handlers

BOOL CCANCalibrationCreateEditDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CComboBox &DaqBox = (CComboBox &) GetDlgItem(4001);

	DaqBox.AddString("Disabled", FALSE);
	DaqBox.AddString("Enabled" , TRUE );

	DaqBox.SetCurSel(m_pAddr ? m_pAddr->m_fDaq : 0);

	CComboBox &DataSize = (CComboBox &) GetDlgItem(2004);

	DataSize.AddString("1", 1);
	DataSize.AddString("2", 2);
	DataSize.AddString("4", 4);

	DataSize.SetCurSel(0);

	CComboBox &TypeBox = (CComboBox &) GetDlgItem(2007);

	TypeBox.AddString("0x00", 0x00);
	TypeBox.AddString("0x01", 0x01);
	TypeBox.AddString("0x02", 0x02);
	TypeBox.AddString("0x03", 0x03);
	TypeBox.AddString("0x81", 0x81);
	TypeBox.AddString("0x82", 0x82);
	TypeBox.AddString("0x83", 0x83);

	TypeBox.SetCurSel(0);

	DoEnables();

	if( m_pAddr ) {

		ShowAddress(m_pAddr);
		}
	else {	
		ResetFields();
		}

	return TRUE;
	}

BOOL CCANCalibrationCreateEditDialog::OnOkay(UINT uId)
{
	CComboBox &Combo    = (CComboBox &) GetDlgItem(4001);
	CComboBox &DataSize = (CComboBox &) GetDlgItem(2004);
	CComboBox &TypeBox  = (CComboBox &) GetDlgItem(2007);

	BOOL fNew = FALSE;

	if( !m_pAddr ) {

		fNew = TRUE;

		m_pAddr = New CCANCalibrationAddress;
		}

	CError Error;

	CString Name = GetDlgItem(2001).GetWindowText(); 

	if( Name != m_pAddr->m_Name && m_pDevice->FindAddress(Name) ) {

		Error.Set(CString(IDS_THAT_NAME_IS));

		Error.Show(ThisObject);

		if( fNew ) {

			delete m_pAddr;

			m_pAddr = NULL;
			}

		return FALSE;
		}

	m_pAddr->m_Name      = Name;

	m_pAddr->m_Address   = tstrtoul(GetDlgItem(2002).GetWindowText(), NULL, 16);

	m_pAddr->m_Extension = tstrtoul(GetDlgItem(2003).GetWindowText(), NULL, 16);

	m_pAddr->m_Size      = DataSize.GetCurSelData();

	m_pAddr->m_Type      = TypeBox.GetCurSelData();

	m_pAddr->m_SizeX     = tstrtoul(GetDlgItem(2005).GetWindowText(), NULL, 10);

	m_pAddr->m_SizeY     = tstrtoul(GetDlgItem(2006).GetWindowText(), NULL, 10);

	m_pAddr->m_Radix     = tstrtol(GetDlgItem(3001).GetWindowText(), NULL, 10);

	m_pAddr->m_Scale     = tstrtol(GetDlgItem(3002).GetWindowText(), NULL, 10);

	m_pAddr->m_Offset    = tstrtol(GetDlgItem(3003).GetWindowText(), NULL, 10);	

	m_pAddr->m_fDaq      = Combo.GetCurSelData();

	m_pAddr->m_DaqId     = tstrtoul(GetDlgItem(4002).GetWindowText(), NULL, 10);

	m_pAddr->m_Odt       = tstrtoul(GetDlgItem(4003).GetWindowText(), NULL, 10);

	m_pAddr->m_Elem      = tstrtoul(GetDlgItem(4004).GetWindowText(), NULL, 10);

	m_pAddr->m_DaqOffset = tstrtoul(GetDlgItem(4005).GetWindowText(), NULL, 10);

	m_pAddr->m_CanId     = tstrtoul(GetDlgItem(4006).GetWindowText(), NULL, 16);

	if( fNew ) {

		if( m_pDevice->AddAddress(*m_pAddr, Error) ) {

			delete m_pAddr;

			EndDialog(TRUE);

			return TRUE;
			}
		}
	else {
		EndDialog(TRUE);

		return TRUE;
		}

	Error.Show(ThisObject);

	return FALSE;
	}

// Notification Handlers

void CCANCalibrationCreateEditDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	DoEnables();

	CComboBox &DaqBox = (CComboBox &) GetDlgItem(4001);

	BOOL fDaq = DaqBox.GetCurSelData();

	if( fDaq ) {

		if( GetDlgItem(4002).GetWindowText() == "N/A" ) {

			GetDlgItem(4002).SetWindowText("0");

			GetDlgItem(4003).SetWindowText("0");

			GetDlgItem(4004).SetWindowText("0");

			GetDlgItem(4005).SetWindowText("0");

			GetDlgItem(4006).SetWindowText("0");
			}
		}
	}

// Dialog Result

void CCANCalibrationCreateEditDialog::DoEnables(void)
{
	CComboBox &DaqBox = (CComboBox &) GetDlgItem(4001);

	BOOL fDaq = DaqBox.GetCurSelData();

	GetDlgItem(4002).EnableWindow(fDaq);
	GetDlgItem(4003).EnableWindow(fDaq);
	GetDlgItem(4004).EnableWindow(fDaq);
	GetDlgItem(4005).EnableWindow(fDaq);
	GetDlgItem(4006).EnableWindow(fDaq);
	}

void CCANCalibrationCreateEditDialog::SetTextLimits(void)
{
	((CEditCtrl &) GetDlgItem(4005)).LimitText(1);
	}

// Implementation

void CCANCalibrationCreateEditDialog::ResetFields(void)
{
	GetDlgItem(2001).SetWindowText("");
	GetDlgItem(2002).SetWindowText("");	
	GetDlgItem(2003).SetWindowText("0");
	GetDlgItem(2005).SetWindowText("1");
	GetDlgItem(2006).SetWindowText("1");

	GetDlgItem(3001).SetWindowText("0");
	GetDlgItem(3002).SetWindowText("1");
	GetDlgItem(3003).SetWindowText("0");

	GetDlgItem(4002).SetWindowText("0");
	GetDlgItem(4003).SetWindowText("0");
	GetDlgItem(4004).SetWindowText("0");
	GetDlgItem(4005).SetWindowText("0");
	GetDlgItem(4006).SetWindowText("0");
	}

void CCANCalibrationCreateEditDialog::ShowAddress(CCANCalibrationAddress * pAddr)
{
	CComboBox &DaqBox   = (CComboBox &) GetDlgItem(4001);
	CComboBox &DataSize = (CComboBox &) GetDlgItem(2004);
	CComboBox &TypeBox  = (CComboBox &) GetDlgItem(2007);

	GetDlgItem(2001).SetWindowText(pAddr->m_Name);

	GetDlgItem(2002).SetWindowText(CPrintf("0x%8.8X", pAddr->m_Address  ));	
	GetDlgItem(2003).SetWindowText(CPrintf("%2.2X"  , pAddr->m_Extension));
	GetDlgItem(2005).SetWindowText(CPrintf("%u", pAddr->m_SizeX ));
	GetDlgItem(2006).SetWindowText(CPrintf("%u", pAddr->m_SizeY ));

	GetDlgItem(3001).SetWindowText(CPrintf("%i", pAddr->m_Radix ));
	GetDlgItem(3002).SetWindowText(CPrintf("%i", pAddr->m_Scale ));
	GetDlgItem(3003).SetWindowText(CPrintf("%i", pAddr->m_Offset));	

	SetComboSelData(DaqBox,   pAddr->m_fDaq);
	SetComboSelData(DataSize, pAddr->m_Size);	
	SetComboSelData(TypeBox,  pAddr->m_Type);

	BOOL fDaq = pAddr->m_fDaq;
	
	GetDlgItem(4002).SetWindowText(fDaq ? CPrintf("%u", pAddr->m_DaqId     ) : "N/A");
	GetDlgItem(4003).SetWindowText(fDaq ? CPrintf("%u", pAddr->m_Odt       ) : "N/A");
	GetDlgItem(4004).SetWindowText(fDaq ? CPrintf("%u", pAddr->m_Elem      ) : "N/A");
	GetDlgItem(4005).SetWindowText(fDaq ? CPrintf("%u", pAddr->m_DaqOffset ) : "N/A");
	GetDlgItem(4006).SetWindowText(fDaq ? CPrintf("0x%8.8X", pAddr->m_CanId) : "N/A");
	}

void CCANCalibrationCreateEditDialog::SetComboSelData(CComboBox &Combo, UINT uData)
{
	for( UINT n = 0; n < Combo.GetCount(); n++ ) {

		Combo.SetCurSel(n);

		if( Combo.GetCurSelData() == uData ) {

			break;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCANCalibrationAddressListDialog, CStdDialog);

// Constructors

CCANCalibrationAddressListDialog::CCANCalibrationAddressListDialog(BOOL fSelect, CCANCalibrationDeviceOptions * pOptions)
{
	m_Addr.m_Ref = 0;

	m_pDevice    = pOptions;

	m_fSelect    = fSelect;

	m_fChange    = FALSE;

	m_uOffsetX   = 0;

	m_uOffsetY   = 0;

	SetName(L"CANCalibrationAddressListDialog");
	}

CCANCalibrationAddressListDialog::CCANCalibrationAddressListDialog( BOOL fSelect,
								    CCANCalibrationDeviceOptions * pOptions,
								    CAddress const &Addr
								    )
{
	m_Addr     = Addr;

	m_pDevice  = pOptions;

	m_fSelect  = fSelect;

	m_fChange  = FALSE;

	m_uOffsetX = 0;

	m_uOffsetY = 0;

	SetName(L"CANCalibrationAddressListDialog");
	}

// Operations

void CCANCalibrationAddressListDialog::GetAddress(CAddress &Addr)
{
	if( IsDM(m_Name) ) {

		CError Error;

		m_pDevice->DoParseAddress(Error, Addr, m_Name);

		return;
		}

	CCANCalibrationAddress *pAddr = m_pDevice->FindAddress(m_Name);

	if( pAddr ) {

		Addr.m_Ref     = pAddr->m_Ref;

		Addr.a.m_Type  = m_pDevice->FindType(*pAddr);

		if( Addr.a.m_Table < addrNamed ) {

			Addr.a.m_Offset = m_uOffsetX + m_uOffsetY * pAddr->m_SizeX;
			}
		}
	else {
		Addr.m_Ref = 0;
		}
	}

// Message Map

AfxMessageMap(CCANCalibrationAddressListDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchCommand(5001, OnAddAddress   )
	AfxDispatchCommand(5002, OnEditAddress  )
	AfxDispatchCommand(5003, OnRemoveAddress)

	AfxDispatchNotify(1001, NM_DBLCLK,       OnDblClk        )
	AfxDispatchNotify(1001, LVN_ITEMCHANGED, OnListItemChange)
	AfxDispatchNotify(7001, LBN_SELCHANGE,   OnDMChange      )
	AfxDispatchNotify(7002, LBN_SELCHANGE,   OnSPNChange     )
	AfxDispatchNotify(7002, LBN_DBLCLK,      OnListDblClk    )

	AfxMessageEnd(CCANCalibrationAddressListDialog)
	};

// Message Handlers

BOOL CCANCalibrationAddressListDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( m_fSelect ) {

		GetDlgItem(5001).ShowWindow(FALSE);
		GetDlgItem(5002).ShowWindow(FALSE);
		GetDlgItem(5003).ShowWindow(FALSE);
		}
	else {
		GetDlgItem(6001).ShowWindow(FALSE);
		GetDlgItem(6002).ShowWindow(FALSE);
		GetDlgItem(6003).ShowWindow(FALSE);
		GetDlgItem(6004).ShowWindow(FALSE);
		GetDlgItem(7000).ShowWindow(FALSE);
		GetDlgItem(7001).ShowWindow(FALSE);
		GetDlgItem(7002).ShowWindow(FALSE);
		}
		
	LoadList();

	LoadJ1939();

	if( m_fSelect ) {

		ShowCurrentAddress();
		}

	return TRUE;
	}

BOOL CCANCalibrationAddressListDialog::OnOkay(UINT uId)
{
	CString Text = GetDlgItem(101).GetWindowText();

	if( !IsDM(Text) ) {

		CListView &List    = (CListView &) GetDlgItem(1001);

		CListViewItem Item = List.GetItem(List.GetSelection());
	
		m_uOffsetX = tstrtoul(GetDlgItem(6002).GetWindowText(), NULL, 10);

		m_uOffsetY = tstrtoul(GetDlgItem(6004).GetWindowText(), NULL, 10);
		}

	m_Name = Text;

	EndDialog(m_fSelect ? TRUE : m_fChange);

	return TRUE;
	}

BOOL CCANCalibrationAddressListDialog::OnAddAddress(UINT uId)
{
	CCANCalibrationCreateEditDialog Dlg(NULL, m_pDevice);

	if( Dlg.Execute(ThisObject) ) {

		LoadList();

		m_fChange = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationAddressListDialog::OnEditAddress(UINT uId)
{	
	CListView &List    = (CListView &) GetDlgItem(1001);

	CListViewItem Item = List.GetItem(List.GetSelection());

	CString Name = Item.GetText();

	CCANCalibrationAddress * pAddr = m_pDevice->FindAddress(Name);

	if( pAddr ) {
		
		CCANCalibrationCreateEditDialog Dlg(pAddr, m_pDevice);

		if( Dlg.Execute(ThisObject) ) {

			LoadList();

			m_fChange = TRUE;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCANCalibrationAddressListDialog::OnRemoveAddress(UINT uId)
{
	CString Prompt = CString(IDS_ARE_YOU_SURE_YOU);

	if( YesNo(Prompt) == IDYES ) {

		CListView &List    = (CListView &) GetDlgItem(1001);

		CListViewItem Item = List.GetItem(List.GetSelection());

		CString Name = Item.GetText();

		CCANCalibrationAddress * pAddr = m_pDevice->FindAddress(Name);

		if( pAddr ) {

			if( m_pDevice->RemoveAddress(*pAddr) ) {

				LoadList();

				m_fChange = TRUE;

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

// Notification Handlers

void CCANCalibrationAddressListDialog::OnDblClk(UINT uId, NMHDR &HDR)
{
	if( m_fSelect ) {

		OnOkay(uId);
		}
	else {
		OnEditAddress(uId);
		}
	}

void CCANCalibrationAddressListDialog::OnListItemChange(UINT uID, NMLISTVIEW &Info)
{
	CListView    &List = (CListView &) GetDlgItem(1001);

	CListViewItem Item = List.GetItem(List.GetSelection());

	CString Text(Item.GetText());

	CCANCalibrationAddress * pAddr = m_pDevice->m_pAddresses->FindByName(Text);

	if( pAddr ) {

		CAddress Addr;

		Addr.m_Ref = pAddr->m_Ref;

		DoEnables(pAddr);

		ShowOffsets(pAddr, Addr.a.m_Offset);

		GetDlgItem(101).SetWindowText(pAddr->m_Name);
		}
	}

void CCANCalibrationAddressListDialog::OnDMChange(UINT uID, CWnd &Wnd)
{
	LoadSPNs();
	}

void CCANCalibrationAddressListDialog::OnSPNChange(UINT uID, CWnd &Wnd)
{
	CListBox &PGN = (CListBox &) GetDlgItem(7001);

	CListBox &SPN = (CListBox &) GetDlgItem(7002);

	CCANCalibrationPGN * pPGN = (CCANCalibrationPGN *) PGN.GetCurSelData();

	CCANCalibrationSPN * pSPN = (CCANCalibrationSPN *) SPN.GetCurSelData();

	CString Text = CPrintf("$%s.%s", pPGN->m_Abbrev, pSPN->m_Abbrev);

	GetDlgItem(101).SetWindowText(Text);
	}

void CCANCalibrationAddressListDialog::OnListDblClk(UINT uID, CWnd &Wnd)
{
	OnOkay(uID);
	}

// Implementation

void CCANCalibrationAddressListDialog::DoEnables(CCANCalibrationAddress const * pAddr)
{
	GetDlgItem(6002).EnableWindow(pAddr->m_SizeX > 1);
	GetDlgItem(6004).EnableWindow(pAddr->m_SizeY > 1);
	}

void CCANCalibrationAddressListDialog::LoadList(void)
{
	CListView &List = (CListView &) GetDlgItem(1001);

	DWORD dwStyle = LVS_REPORT | LVS_EDITLABELS | LVS_SHOWSELALWAYS;

	List.SetWindowStyle(dwStyle, dwStyle);

	DWORD dwExStyle =   LVS_EX_FULLROWSELECT
			  | LVS_EX_GRIDLINES;

	List.SetExtendedListViewStyle(dwExStyle, dwExStyle);

	List.DeleteAllItems();

	List.SetFocus();

	BOOL fDelete = TRUE;

	do { 
		fDelete = List.DeleteColumn(0);

		} while( fDelete );

	AddColHeaders(List);

	CCANCalibrationAddressList * pList = m_pDevice->m_pAddresses;

	UINT n = 0;

	for( INDEX i = pList->GetHead(); !pList->Failed(i); pList->GetNext(i) ) {

		CCANCalibrationAddress * pAddr = pList->GetItem(i);

		LoadItem(n, List, pAddr);

		n++;
		}
	}

void CCANCalibrationAddressListDialog::LoadJ1939(void)
{
	CListBox &List = (CListBox &) GetDlgItem(7001);
	
	for( UINT n = 0; n < m_pDevice->m_pPGNs->GetItemCount(); n++ ) {

		CCANCalibrationPGN * pPGN = m_pDevice->m_pPGNs->GetItem(n);

		if( !pPGN->m_fCommand ) {

			List.AddString(pPGN->m_Abbrev, DWORD(pPGN));
			}
		}
	}

void CCANCalibrationAddressListDialog::LoadSPNs(void)
{
	CListBox &List = (CListBox &) GetDlgItem(7002);

	CListBox &DMs  = (CListBox &)GetDlgItem(7001);

	List.ResetContent();

	CCANCalibrationPGN * pPGN = (CCANCalibrationPGN *) DMs.GetCurSelData();
	
	for( UINT n = 0; n < pPGN->m_pSPNs->GetItemCount(); n++ ) {

		CCANCalibrationSPN * pSPN = pPGN->m_pSPNs->GetItem(n);

		CString Text = CPrintf("%s\t%s", pSPN->m_Abbrev, pSPN->m_Name);

		List.AddString(Text, DWORD(pSPN));
		}
	}

BOOL CCANCalibrationAddressListDialog::IsDM(CString const &Text)
{
	return Text[0] == '$';
	}

void CCANCalibrationAddressListDialog::AddColHeaders(CListView &List)
{
	CRect Rect = List.GetWindowRect();

	int cx = Rect.GetWidth() / 10;
	
	CListViewColumn Col0 ( 0, LVCFMT_LEFT, 2 * cx, "Name");
	CListViewColumn Col1 ( 1, LVCFMT_LEFT, 2 * cx, "Address");
	CListViewColumn Col2 ( 2, LVCFMT_LEFT, 1 * cx, "Ext");
	CListViewColumn Col3 ( 3, LVCFMT_LEFT, 1 * cx, "Data Size");
	CListViewColumn Col4 ( 4, LVCFMT_LEFT, 1 * cx, "X Size");
	CListViewColumn Col5 ( 5, LVCFMT_LEFT, 1 * cx, "Y Size");
	CListViewColumn Col6 ( 6, LVCFMT_LEFT, 1 * cx, "Type Flag");
	CListViewColumn Col7 ( 7, LVCFMT_LEFT, 1 * cx, "Radix");
	CListViewColumn Col8 ( 8, LVCFMT_LEFT, 1 * cx, "Scale");
	CListViewColumn Col9 ( 9, LVCFMT_LEFT, 1 * cx, "Offset");
	CListViewColumn Col10(10, LVCFMT_LEFT, 1 * cx, "DAQ");
	CListViewColumn Col11(11, LVCFMT_LEFT, 1 * cx, "DAQ Num");
	CListViewColumn Col12(12, LVCFMT_LEFT, 1 * cx, "PID");
	CListViewColumn Col13(13, LVCFMT_LEFT, 1 * cx, "Elem");
	CListViewColumn Col14(14, LVCFMT_LEFT, 1 * cx, "Start");
	CListViewColumn Col15(15, LVCFMT_LEFT, 2 * cx, "CAN ID");

	List.InsertColumn( 0, Col0 );
	List.InsertColumn( 1, Col1 );
	List.InsertColumn( 2, Col2 );
	List.InsertColumn( 3, Col3 );
	List.InsertColumn( 4, Col4 );
	List.InsertColumn( 5, Col5 );
	List.InsertColumn( 6, Col6 );
	List.InsertColumn( 7, Col7 );
	List.InsertColumn( 8, Col8 );
	List.InsertColumn( 9, Col9 );
	List.InsertColumn(10, Col10);
	List.InsertColumn(11, Col11);
	List.InsertColumn(12, Col12);
	List.InsertColumn(13, Col13);
	List.InsertColumn(14, Col14);
	List.InsertColumn(15, Col15);
	}

void CCANCalibrationAddressListDialog::LoadItem(UINT uPos, CListView &List, CCANCalibrationAddress const * pAddr)
{
	CListViewItem NameItem(uPos, 0, pAddr->m_Name, 0);

	NameItem.SetParam(pAddr->m_Ref);

	UINT k = List.InsertItem(NameItem);

	BOOL fDaq = pAddr->m_fDaq;

	CListViewItem AddrItem  (k,  1, CPrintf("0x%8.8X", pAddr->m_Address  ), 0);
	CListViewItem ExtItem   (k,  2, CPrintf("0x%2.2X", pAddr->m_Extension), 0);
	CListViewItem SizeItem  (k,  3, CPrintf("%u",    pAddr->m_Size  ), 0);
	CListViewItem XSizeItem (k,  4, CPrintf("%u",    pAddr->m_SizeX ), 0);
	CListViewItem YSizeItem (k,  5, CPrintf("%u",    pAddr->m_SizeY ), 0);
	CListViewItem TypeItem  (k,  6, CPrintf("%2.2X", pAddr->m_Type  ), 0);

	CListViewItem RadixItem (k,  7, CPrintf("%i", pAddr->m_Radix ), 0);
	CListViewItem ScaleItem (k,  8, CPrintf("%i", pAddr->m_Scale ), 0);
	CListViewItem OffsetItem(k,  9, CPrintf("%i", pAddr->m_Offset), 0);

	CListViewItem DaqItem   (k, 10, fDaq ? "Yes" : "No", 0);

	CListViewItem DaqNumItem(k, 11, fDaq ? CPrintf("%u", pAddr->m_DaqId     ) : "N/A", 0);
	CListViewItem PidItem   (k, 12, fDaq ? CPrintf("%u", pAddr->m_Odt       ) : "N/A", 0);
	CListViewItem ElemItem  (k, 13, fDaq ? CPrintf("%u", pAddr->m_Elem      ) : "N/A", 0);
	CListViewItem StartItem (k, 14, fDaq ? CPrintf("%u", pAddr->m_DaqOffset ) : "N/A", 0);

	CListViewItem CanIdItem (k, 15, fDaq ? CPrintf("0x%8.8X", pAddr->m_CanId) : "N/A", 0);
		
	List.SetItem(AddrItem);
	List.SetItem(ExtItem);
	List.SetItem(SizeItem);
	List.SetItem(XSizeItem);
	List.SetItem(YSizeItem);
	List.SetItem(TypeItem);

	List.SetItem(RadixItem);
	List.SetItem(ScaleItem);
	List.SetItem(OffsetItem);

	List.SetItem(DaqItem);
	List.SetItem(DaqNumItem);
	List.SetItem(PidItem);
	List.SetItem(ElemItem);
	List.SetItem(StartItem);
	List.SetItem(CanIdItem);
	}

void CCANCalibrationAddressListDialog::ShowCurrentAddress(void)
{
	if( m_Addr.m_Ref ) {

		if( m_Addr.a.m_Extra ) {

			CListBox &PGNList = (CListBox &) GetDlgItem(7001);

			CListBox &SPNList = (CListBox &) GetDlgItem(7002);

			CCANCalibrationPGN * pPGN = m_pDevice->m_pPGNs->GetItem(m_Addr.a.m_Table);

			if( pPGN ) {

				for( UINT m = 0; m < PGNList.GetCount(); m++ ) {

					if( (CCANCalibrationPGN *) PGNList.GetItemData(m) == pPGN ) {

						PGNList.SetCurSel(m);

						break;
						}
					}

				LoadSPNs();

				CCANCalibrationSPN * pSPN = pPGN->m_pSPNs->GetItem(m_Addr.a.m_Offset);

				if( pSPN ) {

					for( UINT n = 0; n < SPNList.GetCount(); n++ ) {

						if( (CCANCalibrationSPN *) SPNList.GetItemData(n) == pSPN ) {

							SPNList.SetCurSel(n);

							CString Text = CPrintf("$%s.%s", pPGN->m_Abbrev, pSPN->m_Abbrev);

							GetDlgItem(101).SetWindowText(Text);

							break;
							}
						}
					}
				}
			}
		else {
			CListView &List = (CListView &) GetDlgItem(1001);	

			UINT uRef = m_Addr.m_Ref & 0x00FFFFFF;

			CCANCalibrationAddress *pFind = m_pDevice->m_pAddresses->FindByRef(uRef);

			if( pFind ) {

				for( UINT n = 0; n < List.GetItemCount(); n++ ) {

					CListViewItem Item = List.GetItem(n);

					CString Text(Item.GetText());

					if( Text == pFind->m_Name ) {

						List.SetItemState(n, LVIS_SELECTED, LVIS_SELECTED);

						break;
						}
					}

				GetDlgItem(101).SetWindowText(pFind->m_Name);

				ShowOffsets(pFind, m_Addr.a.m_Offset);
			
				DoEnables(pFind);
				}		
			}	
		}
	}

void CCANCalibrationAddressListDialog::ShowOffsets(CCANCalibrationAddress * pAddr, UINT uOffset)
{
	if( pAddr->m_SizeX > 1 || pAddr->m_SizeY > 1 ) {

		UINT uOffsetY = uOffset / pAddr->m_SizeX;

		UINT uOffsetX = uOffset - (uOffsetY * pAddr->m_SizeX);

		GetDlgItem(6002).SetWindowText(CPrintf("%u", uOffsetX));

		GetDlgItem(6004).SetWindowText(CPrintf("%u", uOffsetY));
		}
	else {
		GetDlgItem(6002).SetWindowText("");

		GetDlgItem(6004).SetWindowText("");
		}
	}

// End of File
