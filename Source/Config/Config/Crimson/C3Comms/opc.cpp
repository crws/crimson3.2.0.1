
#include "intern.hpp"

#include "opc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC Space Wrapper Class
//

// Constructors

CSpaceOPC::CSpaceOPC(CString p, CString c, UINT n, UINT t, UINT a) :

	CSpace( p,
		c,
		n,
		t
		)
{
	m_uAccess = a;
	}

//////////////////////////////////////////////////////////////////////////
//
// OPC Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COPCDeviceOptions, CUIItem);       

// Constructor

COPCDeviceOptions::COPCDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));
	
	m_Socket = 790;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_fWide  = TRUE;
	}

// Destructor

COPCDeviceOptions::~COPCDeviceOptions(void)
{
	DeleteAllSpaces();
	}

// UI Managament

void COPCDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			m_FileLast = m_File;

			pWnd->EnableUI(L"Time3", !m_Keep);
			}

		if( Tag == L"Keep" ) {

			pWnd->EnableUI(L"Time3", !m_Keep);
			}

		if( Tag == L"File" ) {

			if( !m_FileLast.IsEmpty() && m_FileLast != m_File ) {
			
				CString Text = IDS_OPC_FILE;
			
				if( pWnd->NoYes(Text) == IDNO ) {
				
					m_File = m_FileLast;

					pWnd->UpdateUI(L"File");

					return;
					}
				}

			DeleteAllSpaces();

			LoadTarget();

			pWnd->UpdateUI(L"Addr");

			pWnd->UpdateUI(L"Socket");

			m_FileLast = m_File;

			// TODO -- Refresh the Resource Pane
			}
		}
	}

// Persistance

void COPCDeviceOptions::PostLoad(void)
{
	DeleteAllSpaces();

	LoadTarget();
	}

// Download Support

BOOL COPCDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_fWide));

	return TRUE;
	}

// Space List Access

CSpaceList & COPCDeviceOptions::GetSpaceList(void)
{
	return m_List;
	}
		
// Meta Data Creation

void COPCDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddString (File);
       	}

// Space List Support

void COPCDeviceOptions::DeleteAllSpaces(void)
{
	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			delete m_List[n];

			m_List.GetNext(n);
			}

		m_List.Empty();
		}
	}

// Datatbase Helpers

BOOL COPCDeviceOptions::LoadTarget(void)
{
	CTextStreamMemory Stream;

	if( Stream.LoadFromFile(m_File) ) {

		CTreeFile Tree;
	
		if( Tree.OpenLoad(Stream) ) {

			CString Code = Tree.GetName();
			
			if( Code == L"C3Data" ) {

				Tree.GetObject();

				C3LoadTarget(Tree);

				Tree.EndObject();

				return TRUE;
				}

			if( Code == L"C2Data" ) {

				Tree.GetObject();

				C2LoadTarget(Tree);

				Tree.EndObject();

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void COPCDeviceOptions::C3LoadTarget(CTreeFile &Tree)
{
	m_fWide = TRUE;
	
	while( !Tree.IsEndOfData() ) {

		if( Tree.GetName() == L"System" ) {

			Tree.GetObject();

			C3LoadTargetSystem(Tree);

			Tree.EndObject();

			return;
			}
		}
	}

void COPCDeviceOptions::C3LoadTargetSystem(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {
		
		CString Name = Tree.GetName();

		if( Name == L"Comms" ) {
		
			Tree.GetObject();

			C3LoadTargetComms(Tree);

			Tree.EndObject();

			continue;
			}

		if( Name == L"Tags" ) {
		
			Tree.GetObject();

			while( !Tree.IsEndOfData() ) {

				if( Tree.GetName() == L"Tags" ) {

					Tree.GetObject();

					C3LoadTargetTags(Tree);

					Tree.EndObject();

					continue;
					}
				}

			Tree.EndObject();

			continue;
			}
		}
	}

void COPCDeviceOptions::C3LoadTargetComms(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"Services" ) {

			Tree.GetObject();

			C3LoadTargetServices(Tree);

			Tree.EndObject();

			continue;
			}
		
		if( Name == L"Ethernet" ) {

			Tree.GetObject();

			C3LoadTargetEthernet(Tree);

			Tree.EndObject();
			
			continue;
			}
		}
	}

void COPCDeviceOptions::C3LoadTargetEthernet(CTreeFile &Tree)
{
	// TODO -- Coded Item

	while( !Tree.IsEndOfData() ) {

		Tree.GetName();
		}
	}

void COPCDeviceOptions::C3LoadTargetServices(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"OPCServer" ) {

			Tree.GetObject();

			C3LoadTargetServer(Tree);

			Tree.EndObject();

			continue;
			}
		}
	}

void COPCDeviceOptions::C3LoadTargetServer(CTreeFile &Tree)
{
	// TODO -- Coded Item.

	while( !Tree.IsEndOfData() ) {

		Tree.GetName();
		}
	}

void COPCDeviceOptions::C3LoadTargetTags(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Type  = Tree.GetName();

		UINT    uType = NOTHING;

		if( Type == L"TagFlag" ) {

			uType = addrBitAsBit;
			}

		if( Type == L"TagSimple" ) {

			uType = addrLongAsLong;
			}

		if( Type == L"TagNumeric" ) {

			uType = addrLongAsLong;
			}

		if( Type == L"TagString" ) {

			uType = addrReserved;
			}

		if( uType == NOTHING ) {

			continue;
			}
		
		Tree.GetObject();

		UINT    uCount  = 0;

		INT     NDX     = 0;

		UINT    uAccess = 0;

		UINT    uExtent = 0;

		CString Name, Desc;
			
		while( !Tree.IsEndOfData() ) {

			CString Prop = Tree.GetName();

			if( Prop == L"NDX" ) {

				NDX = Tree.GetValueAsInteger();

				uCount++;
				
				continue;
				}

			if( Prop == L"Name" ) {

				Name = Tree.GetValueAsString();

				uCount++;
				
				continue;
				}

			if( Prop == L"Extent" ) {

				uExtent = Tree.GetValueAsInteger();

				uCount++;
				
				continue;
				}
				
			if( Prop == L"Desc" ) {

				Desc = Tree.GetValueAsString();

				uCount++;

				continue;
				}

			if( Prop == L"Access" ) {

				uAccess = Tree.GetValueAsInteger();
				
				uCount++;

				continue;
				}

			if( Prop == L"TreatAs" ) {

				if( uType == addrLongAsLong ) {
				
					UINT uTreatAs = Tree.GetValueAsInteger();

					if( uTreatAs == 2 ) {

						uType = addrRealAsReal; 
						}
					 }

				uCount++;

				continue;
				}

			if( Prop == L"ScaleTo" ) {

				UINT uScaleTo = Tree.GetValueAsInteger();

				if( uScaleTo ) {

					if( uScaleTo == 2 ) {

						uType = addrRealAsReal;
						}
					else {
						uType = addrLongAsLong;
						}						
					}

				uCount++;

				continue;
				}
			}

		if( uCount >= 5 ) {

			if( !uExtent ) {

				AfxTrace(L"Adding : Name=%s Desc=%s NDX=%d Access=%d Type=%d\n", PCTXT(Name), PCTXT(Desc), NDX, uAccess, uType);

				m_List.Append(New CSpaceOPC(Name, Desc, NDX, uType, uAccess));
				}
			}

		Tree.EndObject();
		}
	}

// C2 Databases

void COPCDeviceOptions::C2LoadTarget(CTreeFile &Tree)
{
	m_fWide = FALSE;
	
	while( !Tree.IsEndOfData() ) {

		if( Tree.GetName() == "System" ) {

			Tree.GetObject();

			C2LoadTargetSystem(Tree);

			Tree.EndObject();

			return;
			}
		}
	}

void COPCDeviceOptions::C2LoadTargetSystem(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {
		
		CString Name = Tree.GetName();

		if( Name == "Comms" ) {
		
			Tree.GetObject();

			C2LoadTargetComms(Tree);

			Tree.EndObject();

			continue;
			}

		if( Name == "OPC" ) {
		
			Tree.GetObject();

			C2LoadTargetServer(Tree);

			Tree.EndObject();

			continue;
			}
		}
	}

void COPCDeviceOptions::C2LoadTargetComms(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Ethernet" ) {

			Tree.GetObject();

			C2LoadTargetEthernet(Tree);

			Tree.EndObject();
			
			continue;
			}

		if( Name == "Tags" ) {
		
			Tree.GetObject();

			while( !Tree.IsEndOfData() ) {

				if( Tree.GetName() == "Tags" ) {

					Tree.GetObject();

					C2LoadTargetTags(Tree);

					Tree.EndObject();

					continue;
					}
				}

			Tree.EndObject();

			continue;
			}
		}
	}

void COPCDeviceOptions::C2LoadTargetEthernet(CTreeFile &Tree)
{
	UINT uMode = 0;
	
	while ( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Mode" ) {

			// TODO -- Warn if not enabled

			uMode = Tree.GetValueAsInteger();
			
			continue;
			}

		if( uMode == 2 ) {
			
			if( Name == "Address" ) {  

				m_Addr = Tree.GetValueAsInteger();
				}
			}
		}
	}

void COPCDeviceOptions::C2LoadTargetServer(CTreeFile &Tree)
{
	while ( !Tree.IsEndOfData() ) {

		// TODO -- Warn if not enabled

		if( Tree.GetName() == "Socket" ) {

			m_Socket = Tree.GetValueAsInteger();
			}
		}
	}

void COPCDeviceOptions::C2LoadTargetTags(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Type  = Tree.GetName();

		UINT    uType = NOTHING;

		if( Type == "TagIntegerVar" || Type == "TagMultiVar" || Type == "TagIntegerForm" ) {

			uType = addrLongAsLong;
			}
		
		if( Type == "TagFlagVar" || Type == "TagFlagForm" ) {
			
			uType = addrBitAsBit;
			}

		if( Type == "TagRealVar" || Type == "TagRealForm" ) {

			uType = addrRealAsReal;
			}

		if( Type == "TagStringVar" || Type == "TagStringForm" ) {

			uType = addrReserved;
			}

		if( uType == NOTHING ) {

			continue;
			}
		
		Tree.GetObject();

		UINT    uCount  = 0;

		INT     NDX     = 0;

		UINT    uAccess = 0;

		CString Name, Label;
			
		while( !Tree.IsEndOfData() ) {

			CString Prop = Tree.GetName();

			if( uCount >= 4 ) {

				continue;
				}

			if( Prop == "NDX" ) {

				NDX = Tree.GetValueAsInteger();

				uCount++;
				
				continue;
				}

			if( Prop == "Name" ) {

				Name = Tree.GetValueAsString();

				uCount++;
				
				continue;
				}
				
			if( Prop == "Label" ) {

				Label = Tree.GetValueAsString();

				uCount++;

				continue;
				}

			if( Prop == "Access" ) {

				uCount++;

				uAccess = Tree.GetValueAsInteger();
				
				continue;
				}
			}

		if( uCount >= 3 ) {

			m_List.Append(New CSpaceOPC(Name, Label, NDX, uType, uAccess));
			}

		Tree.EndObject();
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// OPC Driver
//

// Instantiator

ICommsDriver * Create_OPCDriver(void)
{
	return New COPCDriver;
	}

// Constructor

COPCDriver::COPCDriver(void)
{
	m_wID		= 0x3707;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "OPCWorx Proxy";
	
	m_DriverName	= "TCP/IP Master";
	
	m_Version	= "1.03";
	
	m_ShortName	= "OPC TCP/IP Master";

	m_DevRoot	= "HMI";

	m_fSingle	= FALSE;

	m_fMapDisable	= FALSE;
	}

// Binding Control

UINT COPCDriver::GetBinding(void)
{
	return bindEthernet;
	}

void COPCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS COPCDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COPCDeviceOptions);
	}

// Address Management

BOOL COPCDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CSpaceOPC *pSpace = (CSpaceOPC *) GetSpace((COPCDeviceOptions *) pConfig, Text);

	if( pSpace ) {

		if( pSpace->m_uType == addrReserved ) {

			Addr.a.m_Type	= addrLongAsLong;

			Addr.a.m_Table	= 1 + pSpace->m_uAccess;
			
			Addr.a.m_Extra	= (pSpace->m_uMinimum & 0x3C00) >> 10;

			Addr.a.m_Offset	= (pSpace->m_uMinimum & 0x03FF) <<  6;
			}
		else {
			UINT    uLen    = pSpace->m_Prefix.GetLength();

			CString Type    = StripType(pSpace, Text.Mid(uLen));

			Addr.a.m_Type	= pSpace->TypeFromModifier(Type);

			Addr.a.m_Table	= pSpace->m_uTable;
			
			Addr.a.m_Extra	= pSpace->m_uAccess;

			Addr.a.m_Offset	= pSpace->m_uMinimum;
			}
			
		return TRUE;
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL COPCDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.a.m_Table >= 1 && Addr.a.m_Table <= 3 ) {

		CAddress Find;

		Find.a.m_Type    = addrReserved;

		Find.a.m_Table   = addrNamed;

		Find.a.m_Extra   = Addr.a.m_Table - 1;

		Find.a.m_Offset  = (Addr.a.m_Offset & 0xFFC0) >>  6;
		
		Find.a.m_Offset |= (Addr.a.m_Extra  & 0x000F) << 10;

		return ExpandAddress(Text, pConfig, Find);
		}
	else {
		CSpaceOPC *pSpace = (CSpaceOPC *) GetSpace((COPCDeviceOptions *) pConfig, Addr);

		if( pSpace ) {

			Text = pSpace->m_Prefix;
			
			return TRUE;
			}
	
		return FALSE;
		}
	}

BOOL COPCDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	COPCAddrDialog Dlg(ThisObject, Addr, pConfig);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL COPCDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {
		
		CSpaceList &List = ((COPCDeviceOptions *) pConfig)->GetSpaceList();
	
		if( List.GetCount() ) {

			if( uItem == 0 ) {

				m_n = List.GetHead();
				}
			else {
				if( !List.GetNext(m_n) ) {

					return FALSE;
					}
				}

			CSpaceOPC *pSpace = (CSpaceOPC *) List[m_n];

			Data.m_Name       = /* pSpace->m_Caption */ pSpace->m_Prefix;
			
			Data.m_fPart      = FALSE;

			if( pSpace->m_uType == addrReserved ) {

				Data.m_Addr.a.m_Type   = addrLongAsLong;

				Data.m_Addr.a.m_Table  = 1 + pSpace->m_uAccess;
				
				Data.m_Addr.a.m_Extra  = (pSpace->m_uMinimum & 0x3C00) >> 10;

				Data.m_Addr.a.m_Offset = (pSpace->m_uMinimum & 0x03FF) <<  6;
				}
			else {
				Data.m_Addr.a.m_Type   = pSpace->m_uType;
				
				Data.m_Addr.a.m_Table  = pSpace->m_uTable;
				
				Data.m_Addr.a.m_Extra  = pSpace->m_uAccess;
				
				Data.m_Addr.a.m_Offset = pSpace->m_uMinimum;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Space List Helpers

CSpace * COPCDriver::GetSpace(COPCDeviceOptions *pConfig, CAddress const &Addr)
{
	CSpaceList &List = pConfig->GetSpaceList();
	
	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( pSpace->MatchSpace(Addr) ) {

				return pSpace;
				}

			List.GetNext(n);
			}
		}

	return NULL;
	}

CSpace * COPCDriver::GetSpace(COPCDeviceOptions *pConfig, CString Text)
{
	CSpaceList &List = pConfig->GetSpaceList();

	CSpace *pSave = NULL;

	UINT uLen = 0;

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			UINT uMatch = pSpace->m_Prefix.GetLength();

			if( pSpace->m_Prefix == Text.Left(uMatch) && uMatch > uLen) {

				pSave = pSpace;

				uLen = uMatch;
				}
			
			List.GetNext(n);
			}
		}
		
	return pSave;
	}

// Implementation

CString COPCDriver::StripType(CSpace *pSpace, CString &Text)
{
	CString Type;

	UINT    uPos;

	if( (uPos = Text.Find('.')) == NOTHING ) {

		Type = pSpace->GetTypeModifier(pSpace->m_uType);
		}
	else {
		Type = Text.Mid(uPos + 1);

		Text = Text.Left(uPos);
		}

	return Type;
	}

//////////////////////////////////////////////////////////////////////////
//
// OPC Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(COPCAddrDialog, CStdDialog);
		
// Constructor

COPCAddrDialog::COPCAddrDialog(COPCDriver &Driver, CAddress &Addr, CItem *pConfig)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = (COPCDeviceOptions *) pConfig;

	m_pSpace  = NULL;

	SetName(L"OPCAddrDlg");
	}

// Message Map

AfxMessageMap(COPCAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(COPCAddrDialog)
	};

// Message Handlers

BOOL COPCAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetCaption();

	FindSpace();

	LoadList();

	return FALSE;
	}

// Notification Handlers

void COPCAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void COPCAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pConfig->GetSpaceList()[Index];

		LoadType();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);
			}
		}
	else {
		m_pSpace = NULL;
		}
	}

void COPCAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	}

// Command Handlers

BOOL COPCAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		Text += GetTypeText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void COPCAddrDialog::SetCaption(void)
{
	CString Text;

	Text.Printf( CString(IDS_DRIVER_CAPTION), 
		     GetWindowText(), 
		     m_pDriver->GetString(stringShortName)
		     );

	SetWindowText(Text);
	}

void COPCAddrDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 80, 0 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf("<%s>", CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING);

	INDEX Find = INDEX(NOTHING);

	CSpaceList &List = m_pConfig->GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			CString Entry;

			if( pSpace->m_Prefix == pSpace->m_Caption ) {

				Entry.Printf("%s", pSpace->m_Prefix);
				}
			else {
				Entry.Printf("%s\t%s", pSpace->m_Prefix, pSpace->m_Caption);
				}

			ListBox.AddString(Entry, DWORD(n) );

			if( pSpace == m_pSpace ) {

				Find = n;
				}

			List.GetNext(n);
			}
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSpaceChange(1001, ListBox);
	}

void COPCAddrDialog::LoadType(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(4001);
	
	ListBox.SetRedraw(FALSE);

	ListBox.ResetContent();

	if( m_pSpace->m_uType == addrReserved ) {

		ListBox.AddString (L"String", DWORD(addrReserved));

		ListBox.SelectData(DWORD(addrReserved));
		}
	else {
		UINT uFind = m_pSpace->m_uType;

		for( UINT uType = m_pSpace->m_uType; uType <= m_pSpace->m_uSpan; uType++ ) {

			ListBox.AddString(m_pSpace->GetTypeAsText(uType), uType);

			if( UINT(m_pAddr->a.m_Type) == uType ) {

				uFind = uType;
				}
			}

		ListBox.SelectData(DWORD(uFind));
		}

	ListBox.EnableWindow(TRUE);

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnTypeChange(4001, ListBox);
	}

void COPCAddrDialog::FindSpace(void)
{
	m_pSpace = m_pDriver->GetSpace(m_pConfig, *m_pAddr);
	}

UINT COPCAddrDialog::GetTypeCode(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(4001);

	UINT uPos  = ListBox.GetCurSel();

	return UINT(ListBox.GetItemData(uPos));
	}

CString COPCAddrDialog::GetTypeText(void)
{
	if( m_pSpace ) {

		UINT uType = GetTypeCode();
		
		if( uType == addrReserved ) {

			return L"";
			}

		return L"." + m_pSpace->GetTypeModifier(uType);
		}

	return L"";
	}

// End of File
