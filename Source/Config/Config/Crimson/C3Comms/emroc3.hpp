
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EMROC3_HPP
	
#define	INCLUDE_EMROC3_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master 3 Constants
//

 static CString ROC_MSG[6] = { "Point Type", "Logical Number", "Parameter", "", "Table Number", "Location" }; const

 //////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Mater 3 Enumerations
//

enum Index {

	indPointType	= 0,
	indLogical	= 1,
	indParam	= 2,
	indType		= 3,
	};

enum Opcode {

	ocTable		= 0,
	ocLocation	= 1,
	ocType		= 2,
	};

enum Types {

	typUINT8	= 0,
	typUINT16	= 1,
	typUINT32	= 2,
	typReal		= 3,
	typDouble	= 4,
	typStr10	= 5,
	typStr12	= 6,
	typStr20	= 7,
	typStr30	= 8,
	typStr40	= 9,
	typTLP		= 10,
	typFlag		= 11,
	typTime		= 12,
	typHourMin	= 13,
	typCurTime	= 14,
	};

enum Tables {

	tabPTMax	= 236,
	tabDouble	= 237,
	tabStr		= 238,
	tabOpcode	= 239,
	};


//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master 3 Slot Definition
//

struct CSlot {

	DWORD	dwSlot;
	DWORD	dwTarget;
	CString	Ref;
	WORD	wExtent;
	};

typedef CArray <CSlot> CSlotArray;

// Sort Help

inline int AfxCompare(CSlot const &s1, CSlot const &s2)
{
	UINT uMask = 0x00FFFFFF;

	if( ((s1.dwTarget & uMask) > (s2.dwTarget & uMask)) )	return +1;

	if( ((s1.dwTarget & uMask) < (s2.dwTarget & uMask)) )	return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master 3 Driver Options
//

class CEmersonRoc3MasterDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonRoc3MasterDriverOptions(void);

		// UI Managament
	        void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Group;
		UINT m_Unit;
		
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master 3 Base Device Options
//

class CEmersonRoc3MasterBaseDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonRoc3MasterBaseDeviceOptions(void);

		// Destructor
		~CEmersonRoc3MasterBaseDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
	        void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		virtual void LoadUI(IUICreate *pView, CItem *pItem);

		// Persistance
		void Init(void);
		void PostLoad(void);
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT	m_Group;
		UINT	m_Unit;
		UINT	m_Poll;
		UINT	m_Acc;
		UINT	m_PassNum;
		BOOL	m_fUse;
		UINT	m_Increment;
		UINT	m_Ping;
		UINT	m_User;
		CString m_OpID;
		CString m_Pass;
		BOOL	m_fChange;
		UINT	m_Model;
		
		// Model Select Support
		void InitModel(void);
		UINT GetModel(void);
		UINT GetModelMask(void);
		BOOL IsModel(UINT uDevice);
		BOOL IsFloType(void);
		BOOL IsRocType(void);
		BOOL IsFlo103(void);
		BOOL IsFlo107(void);
		BOOL IsFlo107wo(void);
		BOOL IsFlo407(void);
		BOOL IsFlo500(void);
		BOOL IsRoc300(void);
		BOOL IsRoc800(void);
		BOOL IsRoc800L(void);
		
		// Slot Access
		BOOL	AppendSlot(CSlot Slot);
		DWORD   SlotExist(CString Text, CAddress Addr);
		BOOL	FindNextSlot(CAddress &Address);
		void    UpdateSlots(CAddress const &Address, INT nSize);
		DWORD	FindSlotAddress(CAddress Addr);
		WORD	FindExtent(CAddress Addr);
		BOOL	IsSlot(CAddress Addr);
		BOOL	IsDouble(BYTE bType);
		BOOL	IsString(BYTE bType);
		BYTE	FindLongSize(BYTE bType);
		void	Rebuild(void);
		void	Sort(void);
								
	protected:

		CSlotArray * m_pSlots;
		WORD	     m_uMark[3];
						
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL IsLikeSlot(CSlot s1, CSlot s2);
		BOOL IsLikeTarget(CSlot s1, CSlot s2);
		BOOL GetMask(CSlot Slot);
		void ClearSlots(void);
		void WalkSlots(void);
		void PurgeSlots(void);
		void Print(CString Message);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master 3 Serial Device Options
//

class CEmersonRoc3MasterDeviceOptions : public CEmersonRoc3MasterBaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonRoc3MasterDeviceOptions(void);

		// Destructor
		~CEmersonRoc3MasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// UI Loading
		void LoadUI(IUICreate *pView, CItem *pItem);

	protected:						
		// Meta Data Creation
		void AddMetaData(void);

		// Data Members
		UINT m_Timeout;
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC TCP Master 3 Device Options
//

class CEmersonRoc3TCPMasterDeviceOptions : public CEmersonRoc3MasterBaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonRoc3TCPMasterDeviceOptions(void);

		// Destructor
		~CEmersonRoc3TCPMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Data Members
		UINT m_IP1;
		UINT m_IP2;
		UINT m_Port;
		UINT m_Keep;
		UINT m_UsePing;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		
		// UI Loading
		void LoadUI(IUICreate *pView, CItem *pItem);

	protected:		
		// Meta Data Creation
		void AddMetaData(void);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master 3 Device Options UI Page
//

class CEmersonRoc3MasterBaseDeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEmersonRoc3MasterBaseDeviceOptionsUIPage(CEmersonRoc3MasterBaseDeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CEmersonRoc3MasterBaseDeviceOptions * m_pOption;
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC Master Driver 3
//

class CEmersonRocMasterDriver3 : public CStdCommsDriver
{
	public:
		// Constructor
		CEmersonRocMasterDriver3(void);

		//Destructor
		~CEmersonRocMasterDriver3(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
		

		// Address Helpers
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Notifications
		void	NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);
		void	NotifyInit(CItem * pConfig);

		// Field Access
		CStringArray	GetTypes(void);
		BYTE		GetPointType(CItem *pConfig, CAddress Addr);
		BYTE		GetLogical(CItem *pConfig, CAddress Addr);
		BYTE		GetParameter(CItem *pConfig, CAddress Addr);
		BYTE		GetLogicalCount(CItem *pConfig);
		BYTE		GetOpTable(CItem *pConfig, CAddress Addr);
		WORD		GetLocation(CItem * pConfig, CAddress Addr);
		BOOL		IsOpcodeTable(CAddress Addr);
		BOOL		IsIO(UINT uPoint);
		CString		GetTypeText(UINT uType);
		BYTE		TranslateType(UINT uType);
		
	protected:

		CStringArray   m_Types;
				
		// Implementation
		void	AddSpaces(void);
		void	InitTypes(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC TCP Master Driver 3
//

class CEmersonRocTCPMasterDriver3 : public CEmersonRocMasterDriver3
{
	public:
		// Constructor
		CEmersonRocTCPMasterDriver3(void);

		// Destructor
		~CEmersonRocTCPMasterDriver3(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Address Selection Dialog - Numeric Entry - User Defined
//

class CEmRocAddrDialog3 : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEmRocAddrDialog3(CEmersonRocMasterDriver3 &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Data
		CEmersonRocMasterDriver3 *	m_pDriver;
		CAddress *			m_pAddr;
		CItem *				m_pConfig;
		BOOL				m_fPart;
		CRange				m_Range[6];
								
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Wnd, DWORD dwData);

		// Notification Handlers
		void OnButton(UINT uID, CWnd &Wnd);
		void OnEditChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void InitType(void);
		void InitPrompt(void);
		void InitSelect(void);
		void DoEnables(void);
		BYTE GetMin(UINT uID);
		BYTE GetMax(UINT uID);
		BOOL IsEdit(UINT uID);
		BOOL CheckRange(UINT uID, CError &Error);
	};

//////////////////////////////////////////////////////////////////////////
//
// Predefined TLP Support
//

// Devices

enum {
	devDefault   = 0,	
	devFlo103104 = 1,
	devRoc800    = 2,
	devRoc800L   = 4,
	devFlo107    = 8,
	devFlo407    = 16,
	devFlo500    = 32,
	devRoc300    = 64,
	devFlo107wo  = 128,
	};

// Constants

#define ID_T		200
#define ID_L		201
#define ID_P		202
#define ID_SHORT	300
#define ID_TYPE		301
#define ID_C		404

//////////////////////////////////////////////////////////////////////////
//
// TLP Structure
//

struct TLP {

	UINT  m_Number;
	PCSTR m_Name;
	PCSTR m_Short;
	UINT  m_Device;
	};

//////////////////////////////////////////////////////////////////////////
//
// Parameter Definitions (P)
//


enum {
	paramNone = 0xFF,
	};

struct PARAMETER {

	TLP	m_Param;
	UINT	m_Type;
	BOOL	m_fReadOnly;
	};

struct PARAMETERS {

	UINT	  m_PointType;
	PARAMETER m_Param[257];
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Address Selection Dialog - Predefined TLP's
//

class CEmRocPreDefinedDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEmRocPreDefinedDialog(CEmersonRocMasterDriver3 &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Data
		CEmersonRocMasterDriver3 *	m_pDriver;
		CAddress *			m_pAddr;
		CItem *				m_pConfig;
		BOOL				m_fPart;
		CArray<TLP>			m_PointTypes;
		CArray<PARAMETERS>		m_Parameters;
		CRange				m_Range[3];
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Wnd, DWORD dwData);

		// Notification Handlers
		void OnButton(UINT uID, CWnd &Wnd);
		void OnSelChange(UINT uID, CWnd &Wnd);
		void OnEditChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadPointType(void);
		void LoadLogical(void);
		void LoadParameter(void);
		UINT GetPointType(void);
		UINT GetLogical(void);
		UINT GetParameter(void);
		void SetShortName(void);
		void SetType(void);
		void SetTLP(void);
		BOOL CheckInit(void);

		// Custom TLP Support
		void SetCustomTLP(void);
		void SetCustomType(void);
		void InitEdit(void);
		void InitCustom(void);
		void ExitCustom(void);
		void InitType(void);
		void DoEnables(void);
		BYTE GetMin(UINT uID);
		BYTE GetMax(UINT uID);
		BOOL IsEdit(UINT uID);
		BOOL IsCustomTLP(void);
		BOOL GetCustomTLP(CString &Tlp);
		BOOL MatchType(void);
		BOOL CheckRange(UINT uID, CError &Error);

		// Model Specific List Support
		void FindParamterList (void);
		void FindPointTypeList(void);
	};
		

#endif

// End of File