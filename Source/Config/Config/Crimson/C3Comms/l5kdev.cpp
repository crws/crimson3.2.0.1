
#include "intern.hpp"

#include "l5kdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kDeviceOptions, CUIItem);

// Constructor

CABL5kDeviceOptions::CABL5kDeviceOptions(void)
{
	m_Device      = 0;
	
	m_Address     = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Routing     = 1;

	m_Port	      = 1;
	
	m_Slot	      = 0;

	m_Path        = L"";

	m_Time	      = 1000;

	m_pController = New CABL5kNames;

	m_pPrograms   = New CABL5kNamesList;

	m_DeviceName  = L"Device";
	}

// Destructor

CABL5kDeviceOptions::~CABL5kDeviceOptions(void)
{
	}

// Address Management

void CABL5kDeviceOptions::CheckAlignment(CAddress &Addr)
{
	if( Addr.a.m_Type == addrBitAsLong ) {

		Addr.a.m_Offset = (Addr.a.m_Offset / 32) * 32;
		}
	}

void CABL5kDeviceOptions::AddToDict(CIndexMapper &Dict, CString const &Name, CStringMapper const &List)
{
	UINT uIndex = List.Find(Name);

	AfxAssert(uIndex < NOTHING);

	if( Dict.Find(uIndex) == NOTHING ) {
		
		Dict.Append(uIndex);
		}			
	}

BOOL CABL5kDeviceOptions::DoParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CABL5kItem *pTag;

	if( (pTag = FindFromMap(Text)) ) {

		CString Name = pTag->m_Name;

		CString Type = pTag->m_Type;

		CArray <UINT> Dims;
		
		if( ParseDims(Dims, Type) ) {

			CArray <UINT> List;

			if( ParseDims(List, Text) ) {

				if( CheckDims(List, Dims) < NOTHING ) {
					
					Error.Set(L"Array dimension out of range", 0);

					return FALSE;
					}
				}

			AddToDict(m_TableDict, Name, m_TableList);

			UINT uIndex = m_TableList.Find(Name);

			UINT uEntry = m_TableDict.Find(uIndex) + 1;

			if( uEntry < addrNamed ) {

				UINT uFactor = 1;
				
				UINT uOffset = 0;

				for( UINT n = 0; n < Dims.GetCount(); n++ ) {
					
					uOffset += List[n] * uFactor;
					
					uFactor *= Dims[n];
					}

				//
				Addr.a.m_Extra  = 0;
				
				Addr.a.m_Type   = m_Types.GetAtomicType(Type);

				Addr.a.m_Table  = uEntry;

				Addr.a.m_Offset = uOffset;
				
				CheckAlignment(Addr);
			
				return TRUE;
				}

			Error.Set(L"Unable to allocate resource for this table tag\n.", 0);

			return FALSE;
			}
		else {
			AddToDict(m_NamedDict, Name, m_NamedList);

			UINT uIndex = m_NamedList.Find(Name);

			UINT uEntry = m_NamedDict.Find(uIndex) + 1;

			//
			Addr.a.m_Extra  = 0;
			
			Addr.a.m_Type   = m_Types.GetAtomicType(Type);

			Addr.a.m_Table  = addrNamed;

			Addr.a.m_Offset = uEntry;
			
			CheckAlignment(Addr);
		
			return TRUE;
			}
		}

	Error.Set(L"Unable to find tag with this name " + Text, 0);
	
	return FALSE;
	}

BOOL CABL5kDeviceOptions::DoExpandAddress(CString &Text, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		if( Addr.a.m_Extra & 0x02 ) {
			
			UINT uEntry = MAKELONG(Addr.a.m_Offset, Addr.a.m_Table);
			
			if( uEntry < m_TableList.GetCount() ) {
				
				Text = m_TableList[uEntry];

				CABL5kItem *pTag;

				if( (pTag = FindFromMap(Text)) ) {
					
					CString Type = pTag->m_Type;

					CArray <UINT> Dims;

					if( ParseDims(Dims, Type) ) {
						
						ExpandDims(Text, Dims, 0);

						return TRUE;
						}
					}
				}

			return FALSE;
			}

		if( Addr.a.m_Extra & 0x01 ) {

			UINT uEntry = MAKELONG(Addr.a.m_Offset, Addr.a.m_Table);

			if( uEntry < m_NamedList.GetCount() ) {

				Text = m_NamedList[uEntry];
				
				return TRUE;
				}

			return FALSE;
			}

		if( IsTable(Addr) ) {

			CABL5kItem *pTag;

			UINT uEntry = Addr.a.m_Table - 1;

			if( uEntry < m_TableDict.GetCount() ) {
				
				Text = m_TableList[m_TableDict[uEntry]];

				if( (pTag = FindFromMap(Text)) ) {
					
					CString Type = pTag->m_Type;

					CArray <UINT> Dims;

					if( ParseDims(Dims, Type) ) {
						
						ExpandDims(Text, Dims, Addr.a.m_Offset);

						return TRUE;
						}
					}
				}

			return FALSE;
			}

		if( IsNamed(Addr) ) {

			UINT uEntry = Addr.a.m_Offset - 1;

			if( uEntry < m_NamedDict.GetCount() ) {

				Text = m_NamedList[m_NamedDict[uEntry]];

				return TRUE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CABL5kDeviceOptions::DoSelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart, BOOL fSelect)
{
	if( Addr.a.m_Extra & 0x01 ) {

		CError Error(FALSE);

		CString Text;

		return DoExpandAddress(Text, Addr) && DoParseAddress(Error, Addr, Text);
		}
			
	CABL5kDriver Driver;	

	CABL5kDialog Dialog(Driver, Addr, this, fPart, fSelect);

	BOOL fResult = Dialog.Execute(CWnd::FromHandle(hWnd));

	if( Dialog.FileImported() ) {
		
		CWnd::FromHandle(hWnd).PostMessage(WM_COMMAND, IDM_COMMS_BUILD_BLOCK);

		CWnd::FromHandle(hWnd).PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);
		}
	
	return fResult;
	}

BOOL CABL5kDeviceOptions::DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			Data.m_Addr.m_Ref = 0;
			Data.m_fPart	  = FALSE;
			Data.m_Name	  = L"Controller Tags";
			Data.m_uData      = DWORD(m_pController->m_pTags);

			return TRUE;
			}

		if( uItem == 1 ) {
			
			Data.m_Addr.m_Ref = 0;
			Data.m_fPart	  = FALSE;
			Data.m_Name	  = L"Program Tags";
			Data.m_uData      = 1;

			m_uProgram        = 0;

			return TRUE;
			}

		return FALSE;
		}
	else {
		if( pRoot->m_uData == 1 ) {

			while( m_uProgram < m_pPrograms->GetItemCount() ) {

				CABL5kNames *pProgram = m_pPrograms->GetItem(m_uProgram ++);
				
				if( pProgram->m_pTags->GetItemCount() ) {

					Data.m_Addr.m_Ref = 0;
					Data.m_fPart	  = FALSE;
					Data.m_Name	  = pProgram->m_Scope;
					Data.m_uData      = DWORD(pProgram->m_pTags);

					return TRUE;
					}
				}

			return FALSE;
			}

		if( pRoot->m_uData ) {

			return ListTags( pRoot, 
					 uItem, 
					 Data, 
					 (CABL5kItemList *) pRoot->m_uData
					 );
			}

		return FALSE;
		}
	}

// UI Management

void CABL5kDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Push" ) {

			DWORD dwAddr = 0;

			DoSelectAddress( pHost->GetWindow(),
				         (CAddress &) dwAddr, 
					 FALSE, 
					 FALSE
					 );
			
			return;
			}

		if( Tag.IsEmpty() || Tag == "Routing" ) {

			BOOL fEnable = m_Routing ? TRUE : FALSE;

			pHost->EnableUI("Port", fEnable);

			pHost->EnableUI("Slot", fEnable);

			pHost->EnableUI("Path", fEnable);
			}
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}
		
// Persistance

void CABL5kDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"Named" ) {

			Tree.GetCollect();
			
			LoadMapping(Tree, m_NamedDict);

			Tree.EndCollect();
			
			continue;
			}

		if( Name == L"Table" ) {

			Tree.GetCollect();
			
			LoadMapping(Tree, m_TableDict);

			Tree.EndCollect();

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CABL5kDeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();
	
	BuildNameMap();
	}

void CABL5kDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutCollect(L"Named");

	SaveMapping(Tree, m_NamedDict);

	Tree.EndCollect();

	Tree.PutCollect(L"Table");

	SaveMapping(Tree, m_TableDict);

	Tree.EndCollect();
	}

// Download Support

BOOL CABL5kDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddLong(DWORD(m_Address));
	Init.AddWord(WORD(m_Routing ? m_Port : 0xFFFF));
	Init.AddWord(WORD(m_Routing ? m_Slot : 0xFFFF));	
	Init.AddWord(WORD(m_Time));

	AddText(Init, m_Path);	

	if( TRUE ) {
		
		UINT uCount = m_NamedDict.GetCount();

		Init.AddWord(WORD(uCount));

		for( UINT n = 0; n < uCount; n ++ ) {

			UINT   uIndex = m_NamedDict[n];

			CString  Name = m_NamedList[uIndex];

			AddName(Init, Name);

			Init.AddByte(BYTE(0));
			}
		}

	if( TRUE ) {
		
		UINT uCount = m_TableDict.GetCount();

		Init.AddWord(WORD(uCount));

		for( UINT n = 0; n < uCount; n ++ ) {

			UINT  uIndex = m_TableDict[n];

			CString Name = m_TableList[uIndex];

			AddName(Init, Name);

			CString Type = FindFromMap(Name)->m_Type;
			
			CArray <UINT> Dims;
			
			ParseDims(Dims, Type);
			
			UINT uSize = Dims.GetCount();
			
			Init.AddByte(BYTE(uSize));

			for( UINT m = 0; m < uSize; m ++ ) {
				
				Init.AddWord(WORD(Dims[m]));
				}
			}
		}

	return TRUE;
	}

// L5k Support

BOOL CABL5kDeviceOptions::LoadFromFile(CFilename const &Name)
{
	CTextStreamMemory Stream;

	Stream.SetPrev();

	if( Stream.LoadFromFile(Name) ) {
		
		CABL5kFile File;

		if( File.OpenLoad(Stream) ) {

			afxThread->SetStatusText(CString(IDS_AB_EXPANDING_TAGS));

			//
			if( TRUE ) {

				m_Types.KillUDTs();

				m_NamedList.Empty();

				m_TableList.Empty();

				m_NamedDict.Empty();

				m_TableDict.Empty();

				m_NameMap.Empty();

				m_pPrograms->DeleteAllItems(TRUE);
				}

			//
			CArray <CABL5kDesc> Fail;

			CABL5kController const &Logix = File.m_Logix;
			
			m_Types.LoadUDTs(Logix.m_UDTs);

			m_Types.LoadUDTs(Logix.m_AOIs);

			m_DeviceName = Logix.m_Name;
			
			m_pController->LoadTags(Logix.m_Tags, Fail);

			for( UINT n = 0; n < Logix.m_Programs.GetCount(); n ++ ) {

				CABL5kList    Program = Logix.m_Programs[n];

				CABL5kNames *pProgram = New CABL5kNames;

				m_pPrograms->AppendItem(pProgram);
				
				pProgram->LoadTags(Program.m_List, Fail, Program.m_Name);
				}

			ShowFailedTags(Fail);

			ShowTags();

			afxThread->SetStatusText(CPrintf(IDS_AB_EXPANDED_TAGS, m_NameMap.GetCount()));
			
			return TRUE;
			}
		}

	return FALSE;
	}

// Name Map Support

void CABL5kDeviceOptions::AddToMap(CABL5kItem *pTag)
{
	CString Name = pTag->m_Name;
	
	CString Type = pTag->m_Type;

	m_NameMap.Insert(Name, pTag);

	CArray <UINT> Dims;
	
	if( ParseDims(Dims, Type) ) {
		
		m_TableList.Append(Name);
		}
	else
		m_NamedList.Append(Name);
	}

CABL5kItem * CABL5kDeviceOptions::FindFromMap(CString Name)
{
	// TODO -- improve this to look at thispart.notthispart[...]
	
	UINT p1, p2;

	if( (p1 = Name.FindRev('.')) < NOTHING ) {

		if( (p2 = Name.Find('[', p1)) < NOTHING ) {
		
			Name = Name.Left(p2);
			}
		}
	else {
		if( (p2 = Name.Find('[')) < NOTHING ) {

			Name = Name.Left(p2);
			}
		}

	return m_NameMap[Name];
	}

// Meta Data Creation

void CABL5kDeviceOptions::AddMetaData(void)
{
	Meta_AddInteger(Device);
	Meta_AddInteger(Address);
	Meta_AddInteger(Routing);
	Meta_AddInteger(Port);
	Meta_AddInteger(Slot);
	Meta_AddString (Path);
	Meta_AddInteger(Time);
	Meta_AddInteger(Push);
	Meta_AddObject (Controller);
	Meta_AddCollect(Programs);
	Meta_AddString (DeviceName);	
	}

// Property Save Filter

BOOL CABL5kDeviceOptions::SaveProp(CString Tag) const
{
	if( Tag == "Push" ) {
		
		return FALSE;
		}

	return CMetaItem::SaveProp(Tag);
	}

BOOL CABL5kDeviceOptions::ParseDims(CArray <UINT> &Dims, CString Text)
{
	UINT p1, p2;

	if( (p1 = Text.FindRev('.')) < NOTHING ) {
		
		Text = Text.Mid(p1 + 1);
		}

	if( (p1 = Text.Find('[')) < NOTHING ) {

		p1++;

		if( (p2 = Text.Find(']', p1)) < NOTHING ) {			
			
			CStringArray List;

			Text.Mid(p1, p2 - p1).Tokenize(List, ',');

			UINT c = List.GetCount();

			for( UINT n = 0; n < c; n++ ) {

				Dims.Append(tatoi(List[c - n - 1]));
				}
			}
		}

	return Dims.GetCount() > 0;
	}

BOOL CABL5kDeviceOptions::ExpandDims(CString &Text, CArray <UINT> uDims, UINT uIndex)
{
	UINT uCount = uDims.GetCount();

	if( uCount ) {

		Text += L"[";

		switch( uCount - 1 ) {

			case 0:
				Text += CPrintf(L"%d", uIndex % uDims[0]);
				break;

			case 1:
				Text += CPrintf(L"%d", uIndex / uDims[0]);

				Text += L",";

				Text += CPrintf(L"%d", uIndex % uDims[0]);

				break;

			case 2:
				Text += CPrintf(L"%d", uIndex / (uDims[1] * uDims[0]));

				uIndex = uIndex % (uDims[1] * uDims[0]);

				Text += L",";

				Text += CPrintf(L"%d", uIndex / uDims[0]);

				Text += L",";

				Text += CPrintf(L"%d", uIndex % uDims[0]);

				break;
			}

		Text += L"]";

		return TRUE;
		}

	return FALSE;
	}

UINT CABL5kDeviceOptions::CheckDims(CArray <UINT> List, CArray <UINT> Dims)
{
	for( UINT n = 0; n < Dims.GetCount(); n ++ ) {
		
		if( List[n] >= Dims[n] ) {
			
			return n;
			}
		}

	return NOTHING;
	}

// Implementation

BOOL CABL5kDeviceOptions::AddText(CInitData &Init, CString const &Text)
{
	UINT uLen = Text.GetLength() + 1;

	Init.AddWord(WORD(uLen));

	for( UINT n = 0; n < uLen; n ++) {

		Init.AddByte(BYTE(Text[n]));
		}

	return TRUE;
	}

void CABL5kDeviceOptions::AddName(CInitData &Init, CString Name)
{
	CString Scope = GetScope(Name);

	if( !Scope.IsEmpty() ) {

		CString Text;

		Text += L"PROGRAM:";
		
		Text += Scope;

		Text += L".";

		Text += Name.Mid(Scope.GetLength() + 1);				
		
		AddText(Init, Text);
		}
	else
		AddText(Init, Name);
	}

CString CABL5kDeviceOptions::GetScope(CString Text)
{
	Text = Text.StripToken('.');

	Text = Text.StripToken('[');

	if( Text.Find(',') < NOTHING ) {
		
		return Text.StripToken(',');
		}

	return L"";
	}

void CABL5kDeviceOptions::SaveMapping(CTreeFile &Tree, CIndexMapper &Dict)
{
	UINT uCount = Dict.GetCount();

	for( UINT n = 0; n < uCount; n ++ ) {

		Tree.PutValue(CPrintf(L"Ref%d", n), Dict[n]);
		}
	}

void CABL5kDeviceOptions::LoadMapping(CTreeFile &Tree, CIndexMapper &Dict)
{
	while( !Tree.IsEndOfData() ) {
		
		CString	Name = Tree.GetName();

		if( Name.Left(3) == L"Ref" ) {

			Dict.Append(Tree.GetValueAsInteger());
			}
		}
	}

void CABL5kDeviceOptions::BuildNameMap(void)
{
	BuildNameMap(m_pController);

	for( UINT n = 0; n < m_pPrograms->GetItemCount(); n++ ) {

		CABL5kNames *pProgram = m_pPrograms->GetItem(n);
		
		BuildNameMap(pProgram);
		}
	}

void CABL5kDeviceOptions::BuildNameMap(CABL5kNames *pNames)
{
	CString         Scope = pNames->m_Scope;
	
	CABL5kItemList *pTags = pNames->m_pTags;

	BuildNameMap(Scope, pTags);
	}

void CABL5kDeviceOptions::BuildNameMap(CString Scope, CABL5kItemList *pTags)
{
	for( UINT n = 0; n < pTags->GetItemCount(); n ++ ) {
		
		CABL5kItem *pTag = pTags->GetItem(n);

		if( pTag->IsStruct() ) {

			CABL5kItemList *pMembers  = ((CABL5kStruct *) pTag)->m_pMembers;
			
			BuildNameMap(Scope, pMembers);
			
			continue;
			}

		AddToMap(pTag);
		}
	}

// List Support

BOOL CABL5kDeviceOptions::ListTags(CAddrData *pRoot, UINT uItem, CAddrData &Data, CABL5kItemList *pTags)
{
	if( uItem < pTags->GetItemCount() ) {

		CABL5kItem *pTag = pTags->GetItem(uItem);

		CString     Name = pTag->m_Name;
		
		if( pTag->IsStruct() ) {

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart	  = FALSE;
			Data.m_Name	  = Name;
			Data.m_uData      = DWORD(((CABL5kStruct *) pTag)->m_pMembers);

			return TRUE;
			}
		else {
			CString Scope = GetScope(Name);

			if( !Scope.IsEmpty() ) {
				
				Name = Name.Mid(Scope.GetLength() + 1);
				}

			UINT uIndex;

			if( (uIndex = m_TableList.Find(pTag->m_Name)) < NOTHING ) {
				
				Data.m_Addr.a.m_Extra  = 0x02;
				Data.m_Addr.a.m_Type   = m_Types.GetAtomicType(pTag->m_Type);
				Data.m_Addr.a.m_Table  = HIWORD(uIndex) & 0xFF;
				Data.m_Addr.a.m_Offset = LOWORD(uIndex);

				Data.m_fPart           = TRUE;
				Data.m_Name            = Name;
				Data.m_uData           = 0;
				
				return TRUE;
				}
			
			if( (uIndex = m_NamedList.Find(pTag->m_Name)) < NOTHING ) {

				Data.m_Addr.a.m_Extra  = 0x01;
				Data.m_Addr.a.m_Type   = m_Types.GetAtomicType(pTag->m_Type);
				Data.m_Addr.a.m_Table  = HIWORD(uIndex) & 0xFF;
				Data.m_Addr.a.m_Offset = LOWORD(uIndex);
				
				Data.m_fPart           = TRUE;
				Data.m_Name            = Name;
				Data.m_uData           = 0;

				return TRUE;
				}
			
			return FALSE;
			}
		}

	return FALSE;
	}

//

void CABL5kDeviceOptions::ShowFailedTags(CArray <CABL5kDesc> const &Fail)
{
	BOOL fEnable = TRUE;

	if( fEnable && Fail.GetCount() ) {

		CString Text = L"Failed to correctly load the following tags..\n\n";

		UINT uCount = Fail.GetCount();

		MakeMin(uCount, 16);

		for( UINT n = 0; n < uCount; n ++ ) {

			CABL5kDesc Desc = Fail[n];

			if( n ) {
				
				Text += ", \n";
				}
			
			Text += Desc.m_Name;					

			if( Desc.IsAlias() ) {
						
				Text += " alias of ";
				}
			else 
				Text += " of data type ";						

			Text += Desc.m_Type;
			}

		if( uCount < Fail.GetCount() ) {

			Text += ", \n";
			
			Text += CPrintf("and %d more..", Fail.GetCount() - uCount);
			}

		Text += ".";
				
		CWnd::GetActiveWindow().Information(Text);
		}
	}

// Debug

void CABL5kDeviceOptions::ShowTags(void)
{
#if 1

	AfxTrace(L"Controller Tags\n");

	static UINT uDepth = 0;

	ShowTags(m_pController->m_pTags, uDepth);

	for( UINT n = 0; n < m_pPrograms->GetItemCount(); n++ ) {

		CABL5kNames *pProgram = m_pPrograms->GetItem(n);

		AfxTrace(L"Program <%s> Tags\n", pProgram->m_Scope);
		
		ShowTags(pProgram->m_pTags, uDepth);
		}
#endif
	}

void CABL5kDeviceOptions::ShowTags(CABL5kItemList *pTags, UINT uDepth)
{
	for( UINT n = 0; n < pTags->GetItemCount(); n ++ ) {

		CABL5kItem *pTag = (CABL5kItem *) pTags->GetItem(n);

		for( UINT d = 0; d < uDepth; d++,AfxTrace(L"\t") );
		
		CString Name = pTag->m_Name;

		CString Type = pTag->m_Type;

		AfxTrace(L"-%s of type <%s>\n", Name, Type);
		}
	}

// End of File
