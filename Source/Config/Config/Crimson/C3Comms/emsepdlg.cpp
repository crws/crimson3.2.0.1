
#include "intern.hpp"

#include "emsonep.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Address Selection
// 

// Runtime Class

AfxImplementRuntimeClass(CEmersonEPAddrDialog, CStdAddrDialog);
		
// Constructor

CEmersonEPAddrDialog::CEmersonEPAddrDialog(CEmersonEPDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fPart		= fPart;

	m_pDriverData	= &Driver;

	m_fIsTCP	= m_pDriverData->GetDriverType();

	m_Device	= pConfig->GetDataAccess("Type")->ReadInteger(pConfig);

	m_Device	= GetID(m_Device);

	m_DispMode	= pConfig->GetDataAccess("DispMode")->ReadInteger(pConfig);

	m_pSortSpace	= NULL;

	m_KWText	= "";
	m_uAlpha0	= 0;

	m_fListTypeChg	= FALSE;

	SetDefaults();

	m_pDriverData->SetDeviceSelect(&m_Device);

	}

CEmersonEPAddrDialog::~CEmersonEPAddrDialog(void)
{
	m_pDriverData->InitSpaces();

	// Load references to all spaces
	m_pDriverData->SetMCSpaces();
	m_pDriverData->SetIBSpaces(FALSE);

	UINT uBaseDevice = ValidateDevSel();

	switch( uBaseDevice ) {

		case DVSP:
		case DVGP:
		case DVST:
			m_pDriverData->SetSPSpaces(FALSE);
			uBaseDevice = DVSP;
			break;

		case DVMP:
			m_pDriverData->SetMPSpaces(FALSE);
			uBaseDevice = DVMP;
			break;

		case DVEPB:
		case DVEPI:
		case DVMC:
		case DVSK:
			m_pDriverData->SetSKSpaces(FALSE);
			break;

		default:
			m_pDriverData->InitSpaces();
			m_pDriverData->SetNonPSpaces();
			return;
		}

	if( !m_fPart ) m_pDriverData->SetValidateSpaces(uBaseDevice);
	}

// Message Map

AfxMessageMap(CEmersonEPAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(CEmersonEPAddrDialog)
	};

// Message Handlers

BOOL CEmersonEPAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	SetTheDeviceList();

	if( IsIB() ) SetOrderedList();

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	CheckExisting(PDWORD(&Addr.m_Ref));

	SetDefaultList();

	m_fListTypeChg = m_pSpace ? IsHeaderSpace(m_pSpace->m_uTable) : TRUE;

	m_uMenuSel = 1;

	if( m_pAddr->m_Ref ) {

		if( IsPreM() ) {

			m_uListSel = Addr.a.m_Extra;
			m_uAlpha0  = m_uListSel == SELAZ ? 'A' : 0;
			}

		else {
			m_uListSel = IsMC() ? SELNM : SELMB;
			m_uAlpha0  = 0;
			}

		UINT uTable = Addr.a.m_Table;

		if( IsCommanderP() ) {

			if( !IsComHeader(uTable) ) {

				if( !IsSolutionModule(uTable) ) SelectMenuNumber(Addr.a.m_Offset);

				else {
					m_uMenuSel = uTable - M15 + 15;
					}
				}

			else {
				m_uMenuSel = uTable + 1 - TM01;
				}
			}


		if( !IsGenericMB(uTable) ) VerifySelects(); // correct for earlier databases
		}

	SetAllow();

	BOOL fShow = FALSE;
	
	if( !m_fPart ) {

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			if( !Addr.m_Ref ) m_pSpace->GetMinimum(Addr);

			ShowDetails();

			ShowAddress(Addr);

			if( Addr.m_Ref ) fShow = ShowOK(Addr.a.m_Table);
			}

		GetDlgItem(2002).EnableWindow(fShow);

		if( !fShow || !IsSolutionModule(Addr.a.m_Table) ) GetDlgItem(2003).SetWindowText("");

		SetDlgFocus(fShow ? 2002 : 1001);

		return !(BOOL(m_pSpace));
		}
	else {
		if( m_pSpace && !IsHeaderSpace(m_pSpace->m_uTable) ) {

			LoadType();

			ShowAddress(Addr);

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			CAddress Addr;

			if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				fShow = Addr.a.m_Table != AN;
				}
			
			else {
				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				Addr.a.m_Extra = m_uListSel;

				ShowAddress(Addr);

				fShow = ShowOK(Addr.a.m_Table);
				}
			}

		GetDlgItem(2002).EnableWindow(fShow);

		GetDlgItem(2003).SetWindowText("");

		return FALSE;
		}
	}

// Notification Handlers

void CEmersonEPAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CEmersonEPAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		LoadType();

		if( m_pSpace != pSpace ) {

			HandleSpace(pSpace);

			if( !m_pSpace ) { // sort/keyword failed

				SetDefaultAllow();
				ClearDetails();
				ClearAddress();
				m_pSpace = NULL;

				return;
				}

			CAddress Addr;

			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			GetDlgItem(2001).SetWindowText( m_pSpace->m_Prefix );

			CString sOffset = m_pSpace->GetValueAsText(m_pSpace->m_uMinimum);

			GetDlgItem(2002).SetWindowText(sOffset);

			ShowDetails();

			ShowAddress(Addr);

			if( ShowOK(m_pSpace->m_uTable) ) {

				GetDlgItem(2002).EnableWindow(TRUE);

				if( !IsSolutionModule(m_pSpace->m_uTable) ) {

					GetDlgItem(2003).SetWindowText("");
					}
				}

			SetDlgFocus(1001);

			return;
			}
		}
	else {
		SetDefaultAllow();

		ClearDetails();

		ClearAddress();

		m_pSpace = NULL;
		}
	}

BOOL CEmersonEPAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		UINT uTable = m_pSpace->m_uTable;

		if( uTable == TLKY ) {

			m_fListTypeChg = TRUE;

			m_KWPrev = m_uAllowList;

			m_KWText = GetDlgItem(2002).GetWindowText();

			if( m_KWText.IsEmpty() ) m_KWText = "a";

			m_KWText.MakeLower();

			LoadList();

			return TRUE;
			}

		if( IsHeaderSpace(uTable) ) {

			if( m_fPart ) return FALSE;

			SetAllow();
			LoadList();

			return TRUE;
			}

		UINT uC = m_pSpace->m_Prefix[0];

		if( isdigit(uC) && !IsCommanderP() ) return TRUE;

		if( isalpha(uC) ) {

			if( IsCommanderP() && !IsGenericMB(uTable) ) return TRUE;
			}

		CString Text;

		Text = m_pSpace->m_Prefix;

		Text += GetDlgItem(2002).GetWindowText();

		if( m_pSpace->m_uType == WW && m_pSpace->m_uSpan == LL ) {

			Text += GetTypeText();
			}

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			Addr.a.m_Extra = m_uListSel;

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CEmersonEPAddrDialog::AllowType(UINT uType)
{
	return uType == WW || uType == LL;
	}

void CEmersonEPAddrDialog::SetTheDeviceList(void)
{
	m_pDriverData->InitSpaces();

	switch( m_Device ) {

		case DVSK:	m_pDriverData->SetSKSpaces(TRUE);	return;

		case DVST:
		case DVGP:
		case DVSP:	m_pDriverData->SetSPSpaces(TRUE);	return;

		case DVMP:	m_pDriverData->SetMPSpaces(TRUE);	return;

		case DVMC:	m_pDriverData->SetMCSpaces();		return;
		}

	m_pDriverData->SetIBSpaces(TRUE);
	}

BOOL CEmersonEPAddrDialog::GetMenuNumber(CString Prefix, UINT uMin, UINT *pNum)
{
	if( isdigit(Prefix[0]) && isdigit(Prefix[1]) ) {

		*pNum = tatoi(Prefix);

		return TRUE;
		}

	return FALSE;
	}

void CEmersonEPAddrDialog::MBToMenu(UINT uTable, UINT uOffset)
{
	if( IsGenericMB(uTable) ) {

		m_uMenuSel = max( 1, (uOffset / 100) % 100 );

		if( IsInvalidMenuNumber(m_uMenuSel) ) m_uMenuSel = 1;

		return;
		}

	SelectMenuNumber(uOffset);
	}

BOOL CEmersonEPAddrDialog::ValidPrefix(UINT uP0)
{
	return isalpha(uP0) ? IsIB() || IsMC() : isdigit(uP0) ? IsCommanderP() : FALSE;
	}

UINT CEmersonEPAddrDialog::ValidateDevSel(void)
{
	switch( m_Device ) {

		case DVEPP:
		case DVSP:
		case DVGP:
		case DVST:
		case DVMP:
		case DVMC:
		case DVBR:
			return m_Device;
		}

	return DVSK;
	}

// Helpers
void CEmersonEPAddrDialog::CheckExisting(PDWORD pdRef)
{
	CAddress Addr;

	Addr.m_Ref = *pdRef;

	m_uTopLevel = GetDefaultTop();

	if( Addr.m_Ref ) {

		if( IsGenericMB(Addr.a.m_Table) ) {

			m_uTopLevel     = TLMB;

			Addr.a.m_Offset = max(1, Addr.a.m_Offset);

			Addr.a.m_Extra  = SELNM;
			}

		if( IsMCItem(Addr.a.m_Table) ) {

			m_uTopLevel     = TLMC;

			Addr.a.m_Extra  = SELNM;

			FindSpace();

			return;
			}
		}

	if( IsIB() ) {

		m_pDriverData->InitSpaces();

		m_pDriverData->SetIBSpaces(!m_fPart); // EPB/EPI Premapped Spaces

		if( IsI() ) {

			m_pDriverData->AddSpacesI();
			}

		if( IsB() ) {
			
			m_pDriverData->AddSpacesB();
			}

		if( !Addr.m_Ref ) {

			Addr.a.m_Table  = AN;
			Addr.a.m_Offset = 1007;
			Addr.a.m_Extra  = SELNM;
			Addr.a.m_Type   = BB;
			m_uTopLevel     = TLMF;
			*m_pAddr        = Addr;
			}
		}

	else {
		if( IsCommanderP() ) {

			m_pDriverData->InitSpaces();

			if( IsSPP() ) m_pDriverData->SetSPSpaces(!m_fPart);

			if( IsSKP() ) m_pDriverData->SetSKSpaces(!m_fPart);

			if( IsGPP() ) m_pDriverData->SetGPSpaces(!m_fPart);

			if( IsSTP() ) m_pDriverData->SetSTSpaces(!m_fPart);

			if( IsMPP() ) m_pDriverData->SetMPSpaces(!m_fPart);

			UINT uTable = Addr.a.m_Table;

			BOOL fSol   = IsSolutionModule(uTable);

			if( !IsGenericMB(uTable) ) {

				if( !Addr.m_Ref || ( !fSol && Addr.a.m_Offset < 100) ) {

					Addr.a.m_Offset = 50100;
					Addr.a.m_Table  = AN;
					Addr.a.m_Extra  = SELNM;
					Addr.a.m_Type   = WW;
					m_uTopLevel     = TLMF;
					*m_pAddr        = Addr;
					}

				else {
					if( !fSol ) {

						Addr.a.m_Offset = 50000 + Addr.a.m_Offset % 10000;
						}
					}
				}
			}

		else {
			if( IsMC() ) {

				m_pDriverData->SetMCSpaces();

				Addr.a.m_Table  = MCH;
				Addr.a.m_Extra  = SELNM;
				Addr.a.m_Type   = WW;
				Addr.a.m_Offset = 1;
				m_uTopLevel     = TLMC;
				*m_pAddr        = Addr;
				}

			else {				
				m_pDriverData->InitSpaces();

				m_pDriverData->SetNonPSpaces();
				}

			}
		}

	if( !Addr.m_Ref ) {

		Addr.a.m_Table  = GMBE;
		Addr.a.m_Extra  = SELNM;
		Addr.a.m_Type   = WW;
		Addr.a.m_Offset = 1;
		m_uTopLevel     = TLMB;
		*m_pAddr        = Addr;
		}

	if( !Addr.a.m_Offset ) Addr.a.m_Offset = 1;

	CheckSpace(PDWORD(&Addr.m_Ref));

	FindSpace();
	}

void CEmersonEPAddrDialog::CheckSpace(PDWORD pdRef)
{
	CAddress A;

	A.m_Ref = *pdRef;

	FindSpace();

	if( !m_pSpace ) {

		UINT uT = A.a.m_Table;

		UINT uO = A.a.m_Offset;

		if( !IsMCItem(A.a.m_Table) ) {

			if( IsSolutionModule(uT) ) {

				uO += 100 * (415 + uT - M15);
				}

			GetMBGeneric(&uT, &uO, A.a.m_Type);

			A.a.m_Table  = uT;
			A.a.m_Offset = uO;

			m_uTopLevel = TLMB;
			}

		*pdRef = A.m_Ref;

		*m_pAddr = A;
		}
	}

void CEmersonEPAddrDialog::HandleSpace(CSpace *pSpace)
{
	UINT uHead = m_pSpace->m_uTable;

	m_fListTypeChg = FALSE;

	if( IsHeaderSpace(uHead) ) {

		m_uListSel = GetListSelFromHeader(uHead);

		BOOL fSort = IsSortHeader(uHead);

		if( !fSort && (uHead != m_uAllowList) ) {

			SetDefaultAllow();

			m_pSortSpace = pSpace;
			}

		if( IsTopLevel(uHead) ) {

			if( m_uTopLevel != uHead ) {

				m_uTopLevel  = uHead;

				SetAllowFromRef(uHead);
				}
			}

		else {
			if( m_uTopLevel == TLKY ) m_uTopLevel = GetDefaultTop();

			if( fSort ) {

				m_uAlpha0  = 0;
				m_uListSel = SELNM;

				if( uHead == TLAL ) {

					m_uAlpha0  = 'A';
					m_uListSel = SELAZ;
					}

				m_pSortSpace = NULL;

				if( pSpace ) {

					if( !IsHeaderSpace(pSpace->m_uTable) ) {

						m_pSortSpace = pSpace;
						}
					}

				else m_uTopLevel = TLKY; // nothing to sort
				}

			else {
				m_uAllowList = uHead;

				if( IsComHeader(uHead) ) m_uMenuSel = uHead - TM01 + 1;
				}
			}

		m_pDriverData->SetListSelect(&m_uListSel);

		m_fListTypeChg = TRUE;

		LoadList();
		}

	else {
		if( !pSpace && IsCommanderP() ) {

			UINT uMenu = 0;

			if( GetMenuNumber(m_pSpace->m_Prefix, m_pSpace->m_uMinimum, &uMenu) ) {

				m_uMenuSel = uMenu;

				LoadList();
				}
			}
		}
	}

void CEmersonEPAddrDialog::SetDefaults(void)
{
	m_uTopLevel = GetDefaultTop();
	SetDefaultAllow();
	SetDefaultList();
	}

UINT CEmersonEPAddrDialog::GetDefaultTop(void)
{
	return IsPreM() ? TLMF : IsMC() ? TLMC : TLMB;
	}

void CEmersonEPAddrDialog::SetDefaultAllow(void)
{
	m_uAllowList = THHR;

	switch( m_uTopLevel ) {

		case TLKY:
		case TLMF:
			m_uAllowList = IsIB() ? TDIA : IsCommanderP() ? TM01 : THHR;
			return;

		case TLMC:
			m_uAllowList = TMCH;
			return;

		case TLMB:
		default:
			m_uAllowList = THHR;
		}
	}

void CEmersonEPAddrDialog::SetDefaultList(void)
{
	m_uListSel = GetListControl(IsPreM());
	}

void CEmersonEPAddrDialog::SetAllow(void)
{
	if( !m_pSpace ) {

		SetDefaultAllow();
		return;
		}

	UINT uTable  = m_pSpace->m_uTable;
	UINT uOffset = m_pSpace->m_uMinimum;

	UINT uAllow  = GetMBType(uTable, uOffset);

	if( IsGenericMB(uTable) ) {

		m_uAllowList = uAllow;
		return;
		}

	if( IsIB() ) {

		if( !(m_uAllowList = GetIBItemType(uTable, uOffset)) ) {

			m_uAllowList = TDIA;
			}
		return;
		}

	if( IsCommanderP() ) {

		m_uAllowList = TM01 + (IsInvalidMenuNumber(m_uMenuSel) ? 0 : m_uMenuSel - 1);
		return;
		}

	if( IsMC() ) {

		m_uAllowList = TMCH;
		return;
		}

	SetAllowFromMB(uOffset);
	}

void CEmersonEPAddrDialog::SetAllowFromRef(UINT uHead)
{
	if( !m_pAddr->m_Ref ) {

		SetDefaultAllow();
		return;
		}

	CAddress &Addr = (CAddress &) *m_pAddr;

	UINT t = Addr.a.m_Table;
	UINT o = Addr.a.m_Offset;
	UINT uList;

	if( IsCommanderP() ) {

		switch( uHead ) {

			case TLMF:
				MBToMenu(t, o);
				m_uAllowList = TM01 - 1 + m_uMenuSel;
				break;

			case TLMB:
				m_uAllowList = THHR;
				break;
			}

		return;
		}

	if( IsIB() ) {

		if( uHead == TLMF ) {

			m_uAllowList = (uList = GetIBItemType(t, o)) ? uList : TDIA;
			return;
			}
		}

	if( IsMC() ) {

		m_uAllowList = TMCH;
		}

	SetAllowFromMB(o);
	}

void CEmersonEPAddrDialog::SetAllowFromMB(UINT uOffset)
{
	switch( uOffset / 10000 ) {

		case 0:  m_uAllowList = THDO;	return;
		case 1:  m_uAllowList = THDI;	return;
		case 2:  m_uAllowList = THAI;	return;
		default: m_uAllowList = THHR;	return;
		}
	}

void CEmersonEPAddrDialog::SelectMenuNumber(UINT uOffset)
{
	m_uMenuSel = 1;

	if( IsCommanderP() ) {

		m_uMenuSel = (uOffset / 100) % 100;
		}
	}

BOOL CEmersonEPAddrDialog::IsInvalidMenuNumber(UINT uSel)
{
	BOOL fMP = m_Device == DVMP;

	if( fMP ) {

		return (uSel >= 15 && uSel <= 17) || uSel > TMENDM;
		}

	if( uSel > TMEND ) return TRUE;

	UINT uLoaded;

	m_pDriverData->GetLoaded(&uLoaded);

	switch( uSel ) {

		case  0:
			return TRUE;

		case 19:
			return !fMP;

		case 15: 
			if( !(uLoaded & 1) ) return TRUE;
			break;

		case 16:
			if( !(uLoaded & 2) ) return TRUE;
			break;

		case 17:
			if( !(uLoaded & 4) ) return TRUE;
			break;
		}

	return !( fMP || (m_Device >= DVSP && m_Device <= DVST) );
	}

BOOL CEmersonEPAddrDialog::IsPreM(void)
{
	return IsIB() || IsCommanderP();
	}

BOOL CEmersonEPAddrDialog::IsIB(void)
{
	return IsI() || IsB();
	}

BOOL CEmersonEPAddrDialog::IsI(void)
{
	return m_Device == DVEPI;
	}

BOOL CEmersonEPAddrDialog::IsB(void)
{
	return m_Device == DVEPB;
	}

BOOL CEmersonEPAddrDialog::IsSPP(void)
{
	return m_Device == DVSP && m_DispMode == DMPRE;
	}

BOOL CEmersonEPAddrDialog::IsSKP(void)
{
	return m_Device == DVSK && m_DispMode == DMPRE;
	}

BOOL CEmersonEPAddrDialog::IsGPP(void)
{
	return m_Device == DVGP && m_DispMode == DMPRE;
	}

BOOL CEmersonEPAddrDialog::IsSTP(void)
{
	return m_Device == DVST && m_DispMode == DMPRE;
	}

BOOL CEmersonEPAddrDialog::IsMPP(void)
{
	return m_Device == DVMP;
	}

BOOL CEmersonEPAddrDialog::IsMC(void)
{
	return m_Device == DVMC;
	}

BOOL CEmersonEPAddrDialog::IsCommanderP(void)
{
	return IsSPP() || IsSKP() || IsGPP() || IsSTP() || IsMPP();
	}

BOOL CEmersonEPAddrDialog::IsHeaderSpace(UINT uHead)
{
	return uHead >= TLMB && uHead < addrNamed;
	}

BOOL CEmersonEPAddrDialog::IsTopLevel(UINT uHead)
{
	return uHead >= TLMB && uHead <= TLMC;
	}

BOOL CEmersonEPAddrDialog::IsSortHeader(UINT uHead)
{
	return uHead == TLAL || uHead == TLRO;
	}

BOOL CEmersonEPAddrDialog::IsModbusHeader(UINT uHead)
{
	return uHead >= THDO && uHead <= THHR;
	}

BOOL CEmersonEPAddrDialog::IsHeaderForIBDevice(UINT uHead)
{
	return IsEPBHeader(uHead) || IsEPIHeader(uHead);
	}

BOOL CEmersonEPAddrDialog::IsEPBHeader(UINT uHead)
{
	if( IsB() ) {

		switch( uHead ) {

			case TDIA:
			case TIOS:
			case TSTT:
			case TSYS:	return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEmersonEPAddrDialog::IsEPIHeader(UINT uHead)
{
	return IsI() && uHead >= TDIA && uHead <= TSYS;
	}

BOOL CEmersonEPAddrDialog::IsComHeader(UINT uHead)
{
	if( IsCommanderP() ) {

		if( uHead < TM01 ) return FALSE;

		if( m_Device == DVMP && uHead <= TMENDM ) return TRUE;

		return uHead <= TMEND;
		}

	return FALSE;
	}

BOOL CEmersonEPAddrDialog::IsMCHeader(UINT uHead)
{
	return uHead == TMCH;
	}

BOOL CEmersonEPAddrDialog::IsValidAlpha0(void)
{
	return m_uAlpha0 >= 'A' && m_uAlpha0 <= 'Z';
	}

BOOL CEmersonEPAddrDialog::ShowOK(UINT uTable)
{
	return !(uTable == AN || IsStringItem(uTable));
	}

BOOL CEmersonEPAddrDialog::IsStringItem(UINT uTable)
{
	return (uTable >= TFPN) && (uTable <= TPSN);
	}

UINT CEmersonEPAddrDialog::GetListSelFromHeader(UINT uHeader)
{
	if( IsEPBHeader(uHeader) ) return GetListControl(TRUE);

	if( IsEPIHeader(uHeader) ) return GetListControl(TRUE);

	if( IsComHeader(uHeader) ) return GetListControl(TRUE);

	if( IsModbusHeader(uHeader) ) return SELMB;

	if( IsMCHeader(uHeader) ) return TMCH;

	if( IsTopLevel(uHeader) ) SetDefaultList();

	return m_uListSel; // no change if other header
	}

UINT CEmersonEPAddrDialog::GetListControl(BOOL fPreM)
{
	return fPreM ? (IsValidAlpha0() ? SELAZ : SELNM) : IsMC() ? SELNM : SELMB;
	}

void CEmersonEPAddrDialog::SetOrderedList(void)
{
	UINT u = 0;

// Control Techniques Ordered List EPB/EPI May 07

	if( m_Device == DVEPB || m_Device == DVEPI ) {

		BOOL f = m_Device == DVEPI;

		memset(m_uDIA, 0, sizeof(m_uDIA));
		memset(m_uHOM, 0, sizeof(m_uHOM));
		memset(m_uIOS, 0, sizeof(m_uIOS));
		memset(m_uINX, 0, sizeof(m_uINX));
		memset(m_uJOG, 0, sizeof(m_uJOG));
		memset(m_uMOT, 0, sizeof(m_uMOT));
		memset(m_uPUL, 0, sizeof(m_uPUL));
		memset(m_uREG, 0, sizeof(m_uREG));
		memset(m_uSTT, 0, sizeof(m_uSTT));
		memset(m_uSYS, 0, sizeof(m_uSYS));

		m_uDIA[u++] = 1007;

		if( f ) m_uDIA[u++] = 30002;

		m_uDIA[u++] = 30401;
		m_uDIA[u++] = 31001;
		m_uDIA[u++] = 31002;
		m_uDIA[u++] = 31003;
		m_uDIA[u++] = 31005;
		m_uDIA[u++] = 31006;
		m_uDIA[u++] = 31007;
		m_uDIA[u++] = 31009;
		m_uDIA[u++] = 31010;
		m_uDIA[u++] = 31011;
		m_uDIA[u++] = 31013;
		m_uDIA[u++] = 31014;
		m_uDIA[u++] = 31015;
		m_uDIA[u++] = 31017;
		m_uDIA[u++] = 31018;
		m_uDIA[u++] = 31019;
		m_uDIA[u++] = 31021;
		m_uDIA[u++] = 31022;
		m_uDIA[u++] = 31023;
		m_uDIA[u++] = 31025;
		m_uDIA[u++] = 31026;
		m_uDIA[u++] = 31027;
		m_uDIA[u++] = 31029;
		m_uDIA[u++] = 31030;
		m_uDIA[u++] = 31031;
		m_uDIA[u++] = 31033;
		m_uDIA[u++] = 31034;
		m_uDIA[u++] = 31035;
		m_uDIA[u++] = 31037;
		m_uDIA[u++] = 31038;
		m_uDIA[u++] = 31039;
		m_uDIA[u++] = 32028;
		m_uDIA[u++] = 32033;
		m_uDIA[u++] = 40020;
		m_uDIA[u++] = 40021;
		m_uDIA[u++] = 40023;
		m_uDIA[u++] = 40701;
		m_uDIA[u++] = 40702;
		m_uDIA[u++] = 40703;
		m_uDIA[u++] = 40704;
		m_uDIA[u++] = 40705;
		m_uDIA[u++] = 40707;
		m_uDIA[u++] = 40708;
		m_uDIA[u++] = 40709;
		m_uDIA[u++] = 40711;
		m_uDIA[u++] = 40712;
		m_uDIA[u++] = 40713;
		m_uDIA[u++] = 40716;

		if( !f ) m_uDIA[u++] = 42036;

		m_uDIA[u++] = 42045;

		if( f ) {

			u = 0;

			m_uHOM[u++] = 41101;
			m_uHOM[u++] = 41102;
			m_uHOM[u++] = 41104;
			m_uHOM[u++] = 41106;
			m_uHOM[u++] = 41110;
			m_uHOM[u++] = 41112;
			m_uHOM[u++] = 41113;
			m_uHOM[u++] = 41115;
			m_uHOM[u++] = 41117;
			m_uHOM[u++] = 41118;
			m_uHOM[u++] = 41119;
			}

		u = 0;

		m_uIOS[u++] = 2;
		m_uIOS[u++] = 18;
		m_uIOS[u++] = 33;
		m_uIOS[u++] = 49;
		m_uIOS[u++] = 65;
		m_uIOS[u++] = 129;

		if( !f ) m_uIOS[u++] = 257;

		m_uIOS[u++] = 10001;
		m_uIOS[u++] = 10017;
		m_uIOS[u++] = 10033;
		m_uIOS[u++] = 10049;
		m_uIOS[u++] = 10065;
		m_uIOS[u++] = 10097;
		m_uIOS[u++] = 30101;
		m_uIOS[u++] = 30102;
		m_uIOS[u++] = 30103;
		m_uIOS[u++] = 30104;
		m_uIOS[u++] = 30105;
		m_uIOS[u++] = 30107;

		if( !f ) m_uIOS[u++] = 32101;

		m_uIOS[u++] = 32103;
		m_uIOS[u++] = 32104;
		m_uIOS[u++] = 40101;
		m_uIOS[u++] = 40102;
		m_uIOS[u++] = 40103;
		m_uIOS[u++] = 40104;
		m_uIOS[u++] = 40105;
		m_uIOS[u++] = 40111;
		m_uIOS[u++] = 40201;
		m_uIOS[u++] = 40301;
		m_uIOS[u++] = 40401;
		m_uIOS[u++] = 40451;

		if( !f ) {
			m_uIOS[u++] = 40601;
			m_uIOS[u++] = 40602;
			m_uIOS[u++] = 40603;
			}

		m_uIOS[u++] = 40651;
		m_uIOS[u++] = 40652;
		m_uIOS[u++] = 40654;
		m_uIOS[u++] = 40656;
		m_uIOS[u++] = 40657;
		m_uIOS[u++] = 40659;

		if( !f ) m_uIOS[u++] = 41205;
	
		if( f ) {

			u = 0;
	
			m_uINX[u++] = 10115;
			m_uINX[u++] = 10117;
			m_uINX[u++] = 40469;
			m_uINX[u++] = 40471;
			m_uINX[u++] = 42600;
			m_uINX[u++] = 42601;
			m_uINX[u++] = 43001;
			m_uINX[u++] = 43002;
			m_uINX[u++] = 43004;
			m_uINX[u++] = 43006;
			m_uINX[u++] = 43008;
			m_uINX[u++] = 43012;
			m_uINX[u++] = 43013;
			m_uINX[u++] = 43014;
			m_uINX[u++] = 43015;
			m_uINX[u++] = 43016;
			m_uINX[u++] = 43017;
			m_uINX[u++] = 43019;
			m_uINX[u++] = 43026;
			m_uINX[u++] = 43027;
			m_uINX[u++] = 43029;
			m_uINX[u++] = 43031;
			m_uINX[u++] = 43033;
			m_uINX[u++] = 43037;
			m_uINX[u++] = 43038;
			m_uINX[u++] = 43039;
			m_uINX[u++] = 43040;
			m_uINX[u++] = 43051;
			m_uINX[u++] = 43052;
			m_uINX[u++] = 43054;
			m_uINX[u++] = 43056;
			m_uINX[u++] = 43058;
			m_uINX[u++] = 43062;
			m_uINX[u++] = 43063;
			m_uINX[u++] = 43064;
			m_uINX[u++] = 43065;
			m_uINX[u++] = 43076;
			m_uINX[u++] = 43077;
			m_uINX[u++] = 43079;
			m_uINX[u++] = 43081;
			m_uINX[u++] = 43083;
			m_uINX[u++] = 43087;
			m_uINX[u++] = 43088;
			m_uINX[u++] = 43089;
			m_uINX[u++] = 43090;
			m_uINX[u++] = 43101;
			m_uINX[u++] = 43102;
			m_uINX[u++] = 43104;
			m_uINX[u++] = 43106;
			m_uINX[u++] = 43108;
			m_uINX[u++] = 43112;
			m_uINX[u++] = 43113;
			m_uINX[u++] = 43114;
			m_uINX[u++] = 43115;
			m_uINX[u++] = 43126;
			m_uINX[u++] = 43127;
			m_uINX[u++] = 43129;
			m_uINX[u++] = 43131;
			m_uINX[u++] = 43133;
			m_uINX[u++] = 43137;
			m_uINX[u++] = 43138;
			m_uINX[u++] = 43139;
			m_uINX[u++] = 43140;
			m_uINX[u++] = 43151;
			m_uINX[u++] = 43152;
			m_uINX[u++] = 43154;
			m_uINX[u++] = 43156;
			m_uINX[u++] = 43158;
			m_uINX[u++] = 43162;
			m_uINX[u++] = 43163;
			m_uINX[u++] = 43164;
			m_uINX[u++] = 43165;
			m_uINX[u++] = 43176;
			m_uINX[u++] = 43177;
			m_uINX[u++] = 43179;
			m_uINX[u++] = 43181;
			m_uINX[u++] = 43183;
			m_uINX[u++] = 43187;
			m_uINX[u++] = 43188;
			m_uINX[u++] = 43189;
			m_uINX[u++] = 43190;
			m_uINX[u++] = 43201;
			m_uINX[u++] = 43202;
			m_uINX[u++] = 43204;
			m_uINX[u++] = 43206;
			m_uINX[u++] = 43208;
			m_uINX[u++] = 43212;
			m_uINX[u++] = 43213;
			m_uINX[u++] = 43214;
			m_uINX[u++] = 43215;
			m_uINX[u++] = 43226;
			m_uINX[u++] = 43227;
			m_uINX[u++] = 43229;
			m_uINX[u++] = 43231;
			m_uINX[u++] = 43233;
			m_uINX[u++] = 43237;
			m_uINX[u++] = 43238;
			m_uINX[u++] = 43239;
			m_uINX[u++] = 43240;
			m_uINX[u++] = 43251;
			m_uINX[u++] = 43252;
			m_uINX[u++] = 43254;
			m_uINX[u++] = 43256;
			m_uINX[u++] = 43258;
			m_uINX[u++] = 43262;
			m_uINX[u++] = 43263;
			m_uINX[u++] = 43264;
			m_uINX[u++] = 43265;
			m_uINX[u++] = 43276;
			m_uINX[u++] = 43277;
			m_uINX[u++] = 43279;
			m_uINX[u++] = 43281;
			m_uINX[u++] = 43283;
			m_uINX[u++] = 43287;
			m_uINX[u++] = 43288;
			m_uINX[u++] = 43289;
			m_uINX[u++] = 43290;
			m_uINX[u++] = 43301;
			m_uINX[u++] = 43302;
			m_uINX[u++] = 43304;
			m_uINX[u++] = 43306;
			m_uINX[u++] = 43308;
			m_uINX[u++] = 43312;
			m_uINX[u++] = 43313;
			m_uINX[u++] = 43314;
			m_uINX[u++] = 43315;
			m_uINX[u++] = 43326;
			m_uINX[u++] = 43327;
			m_uINX[u++] = 43329;
			m_uINX[u++] = 43331;
			m_uINX[u++] = 43333;
			m_uINX[u++] = 43337;
			m_uINX[u++] = 43338;
			m_uINX[u++] = 43339;
			m_uINX[u++] = 43340;
			m_uINX[u++] = 43351;
			m_uINX[u++] = 43352;
			m_uINX[u++] = 43354;
			m_uINX[u++] = 43356;
			m_uINX[u++] = 43358;
			m_uINX[u++] = 43362;
			m_uINX[u++] = 43363;
			m_uINX[u++] = 43364;
			m_uINX[u++] = 43365;
			m_uINX[u++] = 43376;
			m_uINX[u++] = 43377;
			m_uINX[u++] = 43379;
			m_uINX[u++] = 43381;
			m_uINX[u++] = 43383;
			m_uINX[u++] = 43387;
			m_uINX[u++] = 43388;
			m_uINX[u++] = 43389;
			m_uINX[u++] = 43390;

			// cppcheck-suppress redundantAssignment

			u = 0;

			m_uJOG[u++] = 41151;
			m_uJOG[u++] = 41153;
			m_uJOG[u++] = 41155;
			m_uJOG[u++] = 41157;

			// cppcheck-suppress redundantAssignment

			u = 0;

			m_uMOT[u++] = 149;
			m_uMOT[u++] = 277;
			m_uMOT[u++] = 1004;
			m_uMOT[u++] = 1101;
			m_uMOT[u++] = 1102;
			m_uMOT[u++] = 1103;
			m_uMOT[u++] = 1104;
			m_uMOT[u++] = 1105;
			m_uMOT[u++] = 1106;
			m_uMOT[u++] = 1107;
			m_uMOT[u++] = 1108;
			m_uMOT[u++] = 1151;
			m_uMOT[u++] = 9951;
			m_uMOT[u++] = 40081;
			m_uMOT[u++] = 40221;
			m_uMOT[u++] = 40609;
			m_uMOT[u++] = 41021;
			m_uMOT[u++] = 41022;
			m_uMOT[u++] = 41023;
			m_uMOT[u++] = 41024;
			m_uMOT[u++] = 41025;
			m_uMOT[u++] = 41026;
			m_uMOT[u++] = 41028;
			m_uMOT[u++] = 41029;
			m_uMOT[u++] = 41201;
			m_uMOT[u++] = 41203;
			m_uMOT[u++] = 41306;
			m_uMOT[u++] = 41311;
			m_uMOT[u++] = 41316;
			m_uMOT[u++] = 41321;
			m_uMOT[u++] = 41326;
			m_uMOT[u++] = 41331;
			m_uMOT[u++] = 41336;
			m_uMOT[u++] = 41341;
			m_uMOT[u++] = 41307;
			m_uMOT[u++] = 41312;
			m_uMOT[u++] = 41317;
			m_uMOT[u++] = 41322;
			m_uMOT[u++] = 41327;
			m_uMOT[u++] = 41332;
			m_uMOT[u++] = 41337;
			m_uMOT[u++] = 41342;
			m_uMOT[u++] = 42024;
			m_uMOT[u++] = 42028;
			m_uMOT[u++] = 42034;
			m_uMOT[u++] = 42036;
			m_uMOT[u++] = 42044;
			m_uMOT[u++] = 42049;
			m_uMOT[u++] = 42050;
			m_uMOT[u++] = 49401;
			m_uMOT[u++] = 49403;

			// cppcheck-suppress redundantAssignment

			u = 0;

			m_uPUL[u++] = 41003;
			m_uPUL[u++] = 41005;
			m_uPUL[u++] = 41009;
			m_uPUL[u++] = 41010;
			m_uPUL[u++] = 41011;

			// cppcheck-suppress redundantAssignment

			u = 0;

			m_uREG[u++] = 147;
			m_uREG[u++] = 148;
			m_uREG[u++] = 275;
			m_uREG[u++] = 276;
			m_uREG[u++] = 10116;
			m_uREG[u++] = 40219;
			m_uREG[u++] = 40220;
			m_uREG[u++] = 40470;
			m_uREG[u++] = 43010;
			m_uREG[u++] = 43035;
			m_uREG[u++] = 43060;
			m_uREG[u++] = 43085;
			m_uREG[u++] = 43110;
			m_uREG[u++] = 43135;
			m_uREG[u++] = 43160;
			m_uREG[u++] = 43185;
			m_uREG[u++] = 43210;
			m_uREG[u++] = 43235;
			m_uREG[u++] = 43260;
			m_uREG[u++] = 43285;
			m_uREG[u++] = 43310;
			m_uREG[u++] = 43335;
			m_uREG[u++] = 43360;
			m_uREG[u++] = 43385;
			}
	
		u = 0;

		if( f ) {	
			m_uSTT[u++] = 10083;
			m_uSTT[u++] = 10084;
			m_uSTT[u++] = 10085;
			}

		m_uSTT[u++] = 30001;

		if( !f ) {
			m_uSTT[u++] = 30002;
			m_uSTT[u++] = 30004;
			m_uSTT[u++] = 32001;
			}

		if( f ) m_uSTT[u++] = 32003;

		m_uSTT[u++] = 32021;

		if( !f ) {
			m_uSTT[u++] = 32023;
			m_uSTT[u++] = 32024;
			}

		m_uSTT[u++] = 32026;

		if( f ) m_uSTT[u++] = 32032;

		m_uSTT[u++] = 32034;
		m_uSTT[u++] = 32035;
		m_uSTT[u++] = 32036;
		m_uSTT[u++] = 32038;
		m_uSTT[u++] = 32039;
		m_uSTT[u++] = 32040;
		m_uSTT[u++] = 32041;
		m_uSTT[u++] = 32042;
		m_uSTT[u++] = 32061;
		m_uSTT[u++] = 32063;

		if( !f ) m_uSTT[u++] = 32065;

		m_uSTT[u++] = 39982;
		m_uSTT[u++] = 39983;
		m_uSTT[u++] = 39984;

		if( f ) m_uSTT[u++] = 39985;

		m_uSTT[u++] = 39988;

		if( f ) m_uSTT[u++] = 39990;

		if( !f ) {
			m_uSTT[u++] = 40003;
			m_uSTT[u++] = 40005;

			// Velocity Presets for EPB
			m_uSTT[u++] = 41101;
			m_uSTT[u++] = 41103;
			m_uSTT[u++] = 41105;
			m_uSTT[u++] = 41107;
			m_uSTT[u++] = 41109;
			m_uSTT[u++] = 41111;
			m_uSTT[u++] = 41113;
			m_uSTT[u++] = 41115;
			m_uSTT[u++] = 41117;
			m_uSTT[u++] = 41119;
			m_uSTT[u++] = 41121;
			m_uSTT[u++] = 41123;
			m_uSTT[u++] = 41125;
			m_uSTT[u++] = 41127;
			m_uSTT[u++] = 41129;
			m_uSTT[u++] = 41131;
			}

		m_uSTT[u++] = 40715;
		m_uSTT[u++] = 42003;
		m_uSTT[u++] = 42101;
		m_uSTT[u++] = 42107;
		m_uSTT[u++] = 42108;
		m_uSTT[u++] = 42109;
		m_uSTT[u++] = 42110;
		m_uSTT[u++] = 42111;
		m_uSTT[u++] = 42112;
		m_uSTT[u++] = 42113;
		m_uSTT[u++] = 42114;
		m_uSTT[u++] = 42115;
		m_uSTT[u++] = 42116;
		m_uSTT[u++] = 42117;
		m_uSTT[u++] = 42118;

		if( f ) m_uSTT[u++] = 42602;

		m_uSTT[u++] = 49903;

		// cppcheck-suppress redundantAssignment
	
		u = 0;
	
		m_uSYS[u++] = 1001;
		m_uSYS[u++] = 1002;
		m_uSYS[u++] = 1003;

		if( !f ) {
			m_uSYS[u++] = 1004;
			m_uSYS[u++] = 9951;
			}

		m_uSYS[u++] = 40001;
		m_uSYS[u++] = 40002;

		if( f ) m_uSYS[u++] = 40003;

		m_uSYS[u++] = 40004;

		if( f ) {
			m_uSYS[u++] = 40005;
			m_uSYS[u++] = 40017;
			}

		if( !f ) {
			m_uSYS[u++] = 40018;
			m_uSYS[u++] = 40019;
			m_uSYS[u++] = 40051;
			m_uSYS[u++] = 40081;
			m_uSYS[u++] = 40604;
			m_uSYS[u++] = 40605;
			}

		m_uSYS[u++] = 40753;

		if( !f ) {
			m_uSYS[u++] = 40001;
			m_uSYS[u++] = 40002;
			m_uSYS[u++] = 40003;

			}

		if( f ) m_uSYS[u++] = 41108;

		if( f ) m_uSYS[u++] = 41116;

		m_uSYS[u++] = 42002;
		m_uSYS[u++] = 42021;
		m_uSYS[u++] = 42023;

		if( !f ) m_uSYS[u++] = 42024;

		m_uSYS[u++] = 42025;
		m_uSYS[u++] = 42026;

		if( !f ) m_uSYS[u++] = 42028;

		m_uSYS[u++] = 42029;
		m_uSYS[u++] = 42031;
		m_uSYS[u++] = 42032;

		if( !f ) m_uSYS[u++] = 42024;

		m_uSYS[u++] = 42035;

		if( !f ) m_uSYS[u++] = 42044;

		m_uSYS[u++] = 42046;
		m_uSYS[u++] = 42047;
		m_uSYS[u++] = 42048;

		if( !f ) {
			m_uSYS[u++] = 42049;
			m_uSYS[u++] = 42050;
			}

		if( f ) {
			m_uSYS[u++] = 42051;
			m_uSYS[u++] = 42053;
			}

		m_uSYS[u++] = 42061;
		m_uSYS[u++] = 42062;

		if( !f ) {
			m_uSYS[u++] = 49401;
			m_uSYS[u++] = 49403;
			}	
		}
	}

BOOL CEmersonEPAddrDialog::GetAddressFromItem(UINT uTable, UINT uOffset, UINT *pAddress)
{
	UINT uBase = 0;

	*pAddress = uOffset;

	switch( uTable ) {

		case IAC:
			uBase = 43006;
			break;

		case ICR:
			uBase = 43014;
			break;

		case IXC:
			uBase = 43013;
			break;

		case IDC:
			uBase = 43008;
			break;

		case IDS2:
			uBase = 43002;
			break;

		case IDW:
			uBase = 43012;
			break;

		case IXT:
			uBase = 43001;
			break;

		case IXV:
			uBase = 43004;
			break;

		case RGO:
			uBase = 43010;
			break;

		case CNX:
			uBase = 43015;
			break;

		case FPC:
			*pAddress = 31002 + ( (10 - uOffset) * 4 );
			return TRUE;

		case FPT:
			*pAddress = 31003 + ( (10 - uOffset) * 4 );
			return TRUE;

		case FTY:
			*pAddress = 31001 + ( (10 - uOffset) * 4 );
			return TRUE;
		}

	if( uBase ) {

		*pAddress = uBase + (uOffset * 25);
		return TRUE;
		}

	return FALSE;
	}

UINT CEmersonEPAddrDialog::GetSpaceFromAddress(UINT *pOffset)
{
	UINT i;

	UINT uOffset = *pOffset;
	UINT uAddr   = 0;
	UINT uTable  = 0;

	for( i = 0; i <= 15; i++ ) {

		switch( uOffset - (i * 25) ) {

			case 43001:	uTable = IXT;	break;
			case 43002:	uTable = IDS2;	break;
			case 43004:	uTable = IXV;	break;
			case 43006:	uTable = IAC;	break;
			case 43008:	uTable = IDC;	break;
			case 43010:	uTable = RGO;	break;
			case 43012:	uTable = IDW;	break;
			case 43013:	uTable = IXC;	break;
			case 43014:	uTable = ICR;	break;
			case 43015:	uTable = CNX;	break;
			}

		uAddr = i;

		if( uTable ) {

			*pOffset = uAddr;
			return uTable;
			}
		}

	for( i = 0; i < 10; i++ ) {

		switch( uOffset - (i * 4) ) {

			case 31001:	uTable = FTY;	break;
			case 31002:	uTable = FPC;	break;
			case 31003:	uTable = FPT;	break;
			}

		uAddr = 10 - i;

		if( uTable ) {

			*pOffset = uAddr;
			return uTable;
			}
		}

	return AN;
	}

BOOL CEmersonEPAddrDialog::IsDO(UINT uOffset)
{
	return uOffset < 10000;
	}

BOOL CEmersonEPAddrDialog::IsDI(UINT uOffset)
{
	return uOffset >= 10000 && uOffset <= 19999;
	}

BOOL CEmersonEPAddrDialog::IsAI(UINT uOffset)
{
	return uOffset >= 30000 && uOffset <= 39999;
	}

BOOL CEmersonEPAddrDialog::IsHR(UINT uOffset)
{
	UINT uMax = IsCommanderP() ? 59999 : 49999;

	return uOffset >= 40000 && uOffset <= uMax;
	}

BOOL CEmersonEPAddrDialog::IsGenericMB(UINT uTable)
{
	return (uTable >= GMBO) && (uTable <= GMBF);
	}

BOOL CEmersonEPAddrDialog::IsMCItem(UINT uTable)
{
	return (uTable >= MCO) && (uTable < MCH);
	}

BOOL CEmersonEPAddrDialog::IsSolutionModule(UINT uTable)
{
	return uTable >= M15 && uTable <= M17;
	}

UINT CEmersonEPAddrDialog::GetMBType(UINT uTable, UINT uOffset)
{
	UINT uAdd;

	switch( uTable ) {

		case GMBO:	return THDO;
		case GMBI:	return THDI;
		case GMBA:
		case GMBB:
		case GMBC:	return THAI;
		case GMBD:
		case GMBE:
		case GMBF:	return THHR;

		case MCO:	return THDO;
		case MCI:	return THDI;
		case MCH:	return THHR;

		default:	if( m_uTopLevel == TLMB ) return 0;
		}

	if( GetAddressFromItem(uTable, uOffset, &uAdd) ) uOffset = uAdd;

	if( GetIBItemType(uTable, uAdd) ) { // check if valid for this device type

		if( IsDO(uOffset) ) return THDO;
		if( IsDI(uOffset) ) return THDI;
		if( IsAI(uOffset) ) return THAI;
		if( IsHR(uOffset) ) return THHR;
		}

	return 0;
	}

void CEmersonEPAddrDialog::GetMBGeneric(UINT *pTable, UINT *pOffset, UINT uType)
{
	UINT uAdd;

	if( IsIB() ) {

		if( GetAddressFromItem(*pTable, *pOffset, &uAdd) ) *pOffset = uAdd;

		*pTable = GMBE;
		}

	if( IsMC() ) {

		switch( *pTable ) {

			case MCO:	break;
			case MCI:	*pOffset = (*pOffset % 10000) + 10000;	break;
			case MCH:	*pOffset = (*pOffset % 10000) + 40000;	break;
			}
		}

	BOOL fLong = uType == LL;

	switch( *pOffset / 10000 ) {

		case 0:
			*pTable = GMBO;
			break;

		case 1:
			*pTable = GMBI;
			break;

		case 3:
			*pTable = fLong ? GMBB : GMBA;
			break;

		case 4:
		case 5:
			*pTable = fLong ? (IsCommanderP() ? GMBF : GMBD) : GMBE;
			break;
		}

	*pOffset = *pOffset % 10000;
	}

BOOL CEmersonEPAddrDialog::IsDia(UINT uTable, UINT uOffset)
{
	return IsItem( m_uDIA, uTable, uOffset, sizeof(m_uDIA) / sizeof(UINT) );
	}

BOOL CEmersonEPAddrDialog::IsHom(UINT uTable, UINT uOffset)
{
	return IsI() ? IsItem( m_uHOM, uTable, uOffset, sizeof(m_uHOM) / sizeof(UINT) ) : FALSE;
	}

BOOL CEmersonEPAddrDialog::IsInx(UINT uTable, UINT uOffset)
{
	return IsI() ? IsItem( m_uINX, uTable, uOffset, sizeof(m_uINX) / sizeof(UINT) ) : FALSE;
	}

BOOL CEmersonEPAddrDialog::IsIos(UINT uTable, UINT uOffset)
{
	return IsItem( m_uIOS, uTable, uOffset, sizeof(m_uIOS) / sizeof(UINT) );
	}

BOOL CEmersonEPAddrDialog::IsJog(UINT uTable, UINT uOffset)
{
	return IsI() ? IsItem( m_uJOG, uTable, uOffset, sizeof(m_uJOG) / sizeof(UINT) ) : FALSE;
	}

BOOL CEmersonEPAddrDialog::IsMot(UINT uTable, UINT uOffset)
{
	return IsI() ? IsItem( m_uMOT, uTable, uOffset, sizeof(m_uMOT) / sizeof(UINT) ) : FALSE;
	}

BOOL CEmersonEPAddrDialog::IsPul(UINT uTable, UINT uOffset)
{
	return IsI() ? IsItem( m_uPUL, uTable, uOffset, sizeof(m_uPUL) / sizeof(UINT) ) : FALSE;
	}

BOOL CEmersonEPAddrDialog::IsReg(UINT uTable, UINT uOffset)
{
	return IsI() ? IsItem( m_uREG, uTable, uOffset, sizeof(m_uREG) / sizeof(UINT) ) : FALSE;
	}

BOOL CEmersonEPAddrDialog::IsStt(UINT uTable, UINT uOffset)
{
	return IsItem( m_uSTT, uTable, uOffset, sizeof(m_uSTT) / sizeof(UINT) );
	}

BOOL CEmersonEPAddrDialog::IsSys(UINT uTable, UINT uOffset)
{
	return IsItem( m_uSYS, uTable, uOffset, sizeof(m_uSYS) / sizeof(UINT) );
	}

BOOL CEmersonEPAddrDialog::IsItem(UINT *pTable, UINT uTable, UINT uOffset, UINT uCount)
{
	UINT uAddress;

	if( !GetAddressFromItem(uTable, uOffset, &uAddress) ) uAddress = uOffset;

	if( !uAddress ) return FALSE;

	for( UINT i = 0; i < uCount; i++ ) {

		if( uAddress == pTable[i] ) return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonEPAddrDialog::MatchIBAllow(CSpace *pSpace)
{
	if( isdigit(pSpace->m_Prefix[0]) ) return FALSE;

	return GetIBItemType(pSpace->m_uTable, pSpace->m_uMinimum) == m_uAllowList;
	}

UINT CEmersonEPAddrDialog::GetIBItemType(UINT uTable, UINT uOffset)
{
	if( IsDia(uTable, uOffset) ) return TDIA;
	if( IsHom(uTable, uOffset) ) return THOM;
	if( IsInx(uTable, uOffset) ) return TINX;
	if( IsIos(uTable, uOffset) ) return TIOS;
	if( IsJog(uTable, uOffset) ) return TJOG;
	if( IsMot(uTable, uOffset) ) return TMOT;
	if( IsPul(uTable, uOffset) ) return TPUL;
	if( IsReg(uTable, uOffset) ) return TREG;
	if( IsStt(uTable, uOffset) ) return TSTT;
	if( IsSys(uTable, uOffset) ) return TSYS;

	return 0;
	}

UINT CEmersonEPAddrDialog::IsIBorSPListItem(UINT uTable, UINT uOffset, UINT uP0)
{
	if( IsIB() ) return GetIBItemType(uTable, uOffset);

	if( IsCommanderP() ) return isdigit(uP0) ? (uOffset / 100) % 100 : 0;

	return 0;
	}

void CEmersonEPAddrDialog::VerifySelects(void)
{
	m_uTopLevel = IsPreM() ? TLMF : IsMC() ? TLMC : TLMB;

	m_uListSel  = GetListControl(IsPreM());
	}

UINT * CEmersonEPAddrDialog::GetIBArray(UINT *pSize)
{
	switch( m_uAllowList ) {

		case TDIA:	*pSize = sizeof(m_uDIA);	return m_uDIA;
		case THOM:	*pSize = sizeof(m_uHOM);	return m_uHOM;
		case TIOS:	*pSize = sizeof(m_uIOS);	return m_uIOS;
		case TINX:	*pSize = sizeof(m_uINX);	return m_uINX;
		case TJOG:	*pSize = sizeof(m_uJOG);	return m_uJOG;
		case TMOT:	*pSize = sizeof(m_uMOT);	return m_uMOT;
		case TPUL:	*pSize = sizeof(m_uPUL);	return m_uPUL;
		case TREG:	*pSize = sizeof(m_uREG);	return m_uREG;
		case TSTT:	*pSize = sizeof(m_uSTT);	return m_uSTT;
		case TSYS:	*pSize = sizeof(m_uSYS);	return m_uSYS;
		}

	*pSize = 0;

	return NULL;
	}

UINT CEmersonEPAddrDialog::GetID(UINT uType)
{
	switch( uType ) {

		case DVSEPP: return DVEPP;
		case DVSSP:  return DVSP;
		case DVSSK:  return DVSK;
		case DVSGP:  return DVGP;
		case DVSST:  return DVST;
		case DVSMC:  return DVMC;
		case DVSBR:  return DVBR;
		case DVSMP:  return DVMP;
		case DVSEPI :return m_fIsTCP ? DVMP : DVEPI;
		}

	return DVEPB;
	}

// End of File
