
#include "intern.hpp"

#include "MicroDCI.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MicromodDCI Comms Driver
//

// Instantiator

ICommsDriver *	Create_MicromodDCIDriver(void)
{
	return New CMicromodDCIDriver;
	}

// Constructor

CMicromodDCIDriver::CMicromodDCIDriver(void)
{
	m_wID		= 0x4093;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Micromod";
	
	m_DriverName	= "Micro-DCI";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Micromod Micro-DCI";

	AddSpaces();
	}

// Binding Control

UINT	CMicromodDCIDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CMicromodDCIDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Address Helpers

BOOL CMicromodDCIDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CMicromodDCIAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CMicromodDCIDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uTable = pSpace->m_uTable;

	if( uTable == SP_A || uTable == SP_F ) {

		Addr.a.m_Table  = uTable;
		Addr.a.m_Type	= YL;
		Addr.a.m_Extra	= 0;

		BOOL f		= uTable == SP_A;

		UINT uSz1 = f ?  16 :   8;
		UINT uSz2 = f ? 319 : 639;

		UINT uVal	= Text.Find('_');

		if( uVal < NOTHING ) {

			uVal = tatoi(Text.Mid(uVal+2)); // n of _An or _Fn

			uVal = uSz1 * min(uVal, uSz2); // actual offset
			}

		else {
			uVal = (tatoi(Text) / uSz1) * uSz1;

			uVal = min(uVal, pSpace->m_uMaximum);
			}

		Addr.a.m_Offset	= uVal;

		return TRUE;
		}

	return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
	}

BOOL CMicromodDCIDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uTable = pSpace->m_uTable;

		if( uTable != SP_A && uTable != SP_F ) {

			return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		BOOL f	= uTable == SP_A;

		UINT d2 = f ? 16  : 8;
		char c	= f ? 'A' : 'F';

		UINT uOffset = Addr.a.m_Offset;

		if( uOffset % d2 ) {	// adjust for duplicate tag/gateway block

			uOffset = (uOffset + d2 - 1) / d2 * d2;
			}

		UINT u	= uOffset / d2; // A or F number

		Text.Printf(L"%s%d_%c%d",
			pSpace->m_Prefix,
			Addr.a.m_Offset,
			c,
			u
			);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void	CMicromodDCIDriver::AddSpaces(void)
{
	AddSpace( New CSpace( SP_C, "C",	"Real - Low Precision",		10, 0,	 767, RR ));
	AddSpace( New CSpace( SP_H, "H",	"Real - High Precision",	10, 0,	 127, RR ));
	AddSpace( New CSpace( SP_B, "B",	"Byte",				10, 0,	 767, YY, YL));
	AddSpace( New CSpace( SP_L, "L",	"Bit",				10, 0,	2047, BB ));
	AddSpace( New CSpace( SP_A, "SL",	"A Text: 10 characters",	10, 0,	5104, YL ));
	AddSpace( New CSpace( SP_F, "SS",	"F Text: 5 characters",		10, 0,	5112, YL ));
	AddSpace( New CSpace( SP_Z, "Z",	"Direct Access Enable",		10, 0,     0, BB ));
	}

//////////////////////////////////////////////////////////////////////////
//
// Micromod MicroDCI Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CMicromodDCIAddrDialog, CStdAddrDialog);
		
// Constructor

CMicromodDCIAddrDialog::CMicromodDCIAddrDialog(CMicromodDCIDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pSpace  = NULL;

	m_pGDrv = &Driver;

	SetName(TEXT("CMicromodDCIAddressDlg"));
	}

// Destructor
CMicromodDCIAddrDialog::~CMicromodDCIAddrDialog(void)
{
	}

// Message Map

AfxMessageMap(CMicromodDCIAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001,	LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001,	LBN_SELCHANGE,	OnTypeChange )

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CMicromodDCIAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	if( !m_fPart ) {

		SetCaption();

		FindSpace();

		LoadList();

		if( m_pSpace ) {

			ShowAddress(Addr);

			SetAddressFocus();

			return FALSE;
			}

		return TRUE;
		}

	else {
		FindSpace();

		LoadPartDesc();

		LoadType();

		ShowAddress(Addr);

		if( TRUE ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			Text += GetTypeText();

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);

				ShowAddress(Addr);
				}
			}

		EnableText();

		SetAddressFocus();

		return FALSE;
		}
	}

// Notification Handlers

void CMicromodDCIAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CMicromodDCIAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		LoadType();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		ClearType();

		ClearAddress();

		ClearDetails();
		}
	}

void CMicromodDCIAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnTypeChange(uID, Wnd);
	}

// Command Handlers
BOOL CMicromodDCIAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( !IsTextSpace(m_pSpace->m_uTable) ) {

			return CStdAddrDialog::OnOkay(uID);
			}

		CString	Text = GetDCIText();

		CError	Error(TRUE);

		CAddress Addr;

		if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			Addr.a.m_Table	= m_pSpace->m_uTable;
			Addr.a.m_Offset	= 0;
			Addr.a.m_Type	= YL;
			Addr.a.m_Extra	= 0;
			}

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

CString CMicromodDCIAddrDialog::GetDCIText(void)
{
	BOOL fA		= m_pSpace->m_uTable == SP_A;

	UINT uStrNum	= min((UINT)tatoi(GetDlgItem(2029).GetWindowText()), (UINT)(fA ? 319 : 639));

	UINT uOffset	= uStrNum * (UINT)(fA ? 16 : 8);

	CString Text;

	Text.Printf(L"%s%d_%c%d",
		
		m_pSpace->m_Prefix,
		uOffset,
		(char)(fA ? 'A' : 'F'),
		uStrNum
		);

	return Text;
	}

void CMicromodDCIAddrDialog::ShowAddress(CAddress Addr)
{
	CStdAddrDialog::ShowAddress(Addr);

	if( EnableText() ) {

		SetStringPar(Addr.a.m_Offset, Addr.a.m_Table == SP_A);
		}
	}

void CMicromodDCIAddrDialog::SetStringPar(UINT uOffset, BOOL fIsA)
{
	SetStringNum(uOffset, fIsA);
	}

void CMicromodDCIAddrDialog::SetStringNum(UINT uOffset, BOOL fIsA)
{
	CString s;

	s.Printf(L"%d", GetStringNum(uOffset, fIsA));

	GetDlgItem(2029).SetWindowText(s);
	}

UINT CMicromodDCIAddrDialog::GetStringNum(UINT uOffset, BOOL fIsA)
{
	UINT uSize = fIsA ? 16 : 8;

	UINT uMax  = uSize * (UINT)(fIsA ? 319 : 639);

	uOffset    = min(uOffset / uSize * uSize, uMax);

	return uOffset / uSize;
	}

// Helpers
BOOL CMicromodDCIAddrDialog::IsTextSpace(UINT uTable)
{
	return uTable == SP_A || uTable == SP_F;
	}

BOOL CMicromodDCIAddrDialog::EnableText(void)
{
	BOOL f = m_pSpace && IsTextSpace(m_pSpace->m_uTable);

	GetDlgItem(2028).EnableWindow(f);
	GetDlgItem(2029).EnableWindow(f);

	GetDlgItem(2002).EnableWindow(!f);

	return f;
	}

void CMicromodDCIAddrDialog::LoadPartDesc(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	int nTab[] = { 40, 100, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	INDEX	         Find = INDEX(NOTHING);

	CSpaceList &List = m_pDriver->GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( pSpace->m_uTable == m_pSpace->m_uTable ) {

				CString Entry;
			
				Entry.Printf(L"%s\t%s", pSpace->m_Prefix, pSpace->m_Caption);

				ListBox.AddString(Entry, DWORD(n) );

				if( pSpace == m_pSpace ) {

					Find = n;

					break;
					}
				}

			List.GetNext(n);
			}
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);
	}

// End of File
