
#include "intern.hpp"

#include "IfmDualisCamera.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IfmDualisDeviceOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Camera Driver
//

// Instantiator

ICommsDriver *	Create_IfmDualisCameraDriver(void)
{
	return New CIfmDualisCamera;
	}

// Constructor

CIfmDualisCamera::CIfmDualisCamera(void)
{
	m_wID		= 0x3F05;

	m_uType		= driverCamera;
	
	m_Manufacturer	= "Ifm Effector";
	
	m_DriverName	= "Dualis Object Recognition Sensor Camera";
	
	m_Version	= "1.00";

	m_DevRoot	= "Cam";
	
	m_ShortName	= "Dualis Camera";
	}

// Configuration

CLASS CIfmDualisCamera::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CIfmDualisCamera::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDualisVisionSensorCameraDeviceOptions);
	}

// Binding Control

UINT CIfmDualisCamera::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
