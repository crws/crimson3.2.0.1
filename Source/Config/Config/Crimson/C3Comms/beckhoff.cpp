
#include "intern.hpp"

#include "beckhoff.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff ADS TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBeckhoffADSTCPDeviceOptions, CUIItem);

// Constructor

CBeckhoffADSTCPDeviceOptions::CBeckhoffADSTCPDeviceOptions(void)
{
	m_AMSPort	= 800;

	m_Addr		= DWORD(MAKELONG(MAKEWORD(20, 21), MAKEWORD(16, 172) ));

	m_AMSID		= m_Addr;

	m_AMSTail	= 1;

	m_fUseIP	= TRUE;

	m_Socket	= 48898;

	m_Keep		= TRUE;

	m_Time1		= 5000;

	m_Time2		= 2500;

	m_Time3		= 200;
	}

// UI Managament

void CBeckhoffADSTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "AMSTail" ) {

			m_AMSTail = max(1, min( m_AMSTail, 9 ));

			pWnd->UpdateUI("AMSTail");
			}

		if( Tag.IsEmpty() || Tag == "UseIP" || Tag == "Addr" ) {

			if( m_fUseIP ) {

				m_AMSID = m_Addr;
				}

			pWnd->UpdateUI("AMSID");
			}

		if( Tag == "AMSID" ) {

			m_fUseIP = FALSE;

			pWnd->UpdateUI("UseIP");
			}
		}
	}

// Download Support

BOOL CBeckhoffADSTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_AMSPort));
	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddLong(LONG(m_AMSID));
	Init.AddWord(WORD(m_AMSTail));

	return TRUE;
	}

// Meta Data Creation

void CBeckhoffADSTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(AMSPort);
	Meta_AddInteger(AMSID);
	Meta_AddInteger(AMSTail);
	Meta_AddBoolean(UseIP);
	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff ADS TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_BeckhoffADSTCPDriver(void)
{
	return New CBeckhoffADSTCPDriver;
	}

// Constructor

CBeckhoffADSTCPDriver::CBeckhoffADSTCPDriver(void)
{
	m_wID		= 0x351F;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Beckhoff";
	
	m_DriverName	= "ADS/AMS TCP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Beckhoff ADS/AMS TCP";

	AddSpaces();
	}

// Binding Control

UINT CBeckhoffADSTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBeckhoffADSTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CBeckhoffADSTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CBeckhoffADSTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBeckhoffADSTCPDeviceOptions);
	}

// Address Management

BOOL CBeckhoffADSTCPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CBeckhoffADSTCPAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CBeckhoffADSTCPDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);

	switch( Addr.a.m_Type ) {

		case BB:	break;

		case YY:
		case addrBitAsByte:	Addr.a.m_Type = YY; break;

		case WW:
		case addrByteAsWord:	Addr.a.m_Type = WW; break;

		case LL:
		case addrWordAsLong:
		case addrByteAsLong:	Addr.a.m_Type = LL; break;

		case addrByteAsReal:
		case addrWordAsReal:
		case addrLongAsReal:	Addr.a.m_Type = RR; break;
		}
	
	return TRUE;
	}

// Implementation

void CBeckhoffADSTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace( 1, "M",  "%M Memory\t(Group = 4020)",		10,  0, 0xFFFF,	YY, RR));
	AddSpace(New CSpace( 2, "I",  "%I Input\t(Group = F021 / F020)",	10,  0, 0xFFFF,	BB, YY));
	AddSpace(New CSpace( 3, "O",  "%Q Output\t(Group = F031 / F030)",	10,  0, 0xFFFF,	BB, YY));
	AddSpace(New CSpace(AN, "RAS","Read ADS State",				10, 10,     10,	WW));
	AddSpace(New CSpace(AN, "RDS","Read Device State",			10, 11,     11,	WW));
	AddSpace(New CSpace(AN, "WC", "Write Control - Send WCA + WCD",		10, 12,     12,	BB));
	AddSpace(New CSpace(AN, "WCA","   ADS State",				10, 13,     13,	WW));
	AddSpace(New CSpace(AN, "WCD","   Device State",			10, 14,     14,	WW));
	AddSpace(New CSpace(98, "ERRV", "Error Value in Response",		10,  0,      0, LL));
	AddSpace(New CSpace(99, "ERRI", "Item in Error",			10,  0,      0, LL));
	}

BOOL CBeckhoffADSTCPDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Type Selection
//

// Runtime Class

AfxImplementRuntimeClass(CBeckhoffADSTCPAddrDialog, CStdAddrDialog);
		
// Constructor

CBeckhoffADSTCPAddrDialog::CBeckhoffADSTCPAddrDialog(CBeckhoffADSTCPDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	}

// Overridables

BOOL CBeckhoffADSTCPAddrDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case BB:
		case YY:
		case WW:
		case LL:
		case RR:
			return TRUE;
		}

	return FALSE;
	}

// End of File
