
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_E3MODBUS_HPP

#define INCLUDE_E3MODBUS_HPP

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Pre-defined Register
//

struct CE3Register
{
	// Constructor
	CE3Register(void);

	UINT	m_uTable;
	UINT	m_uType;
	UINT	m_uMin;
	UINT	m_uMax;
	BOOL	m_fNamed;
	BOOL    m_fReadOnly;
	CString	m_Prefix;
	CString m_Caption;

	bool operator ==(CE3Register const &That);
	};

typedef CArray<CE3Register> CRegArray;

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Models
//

class CE3Model
{
	public:
		// Constructors
		CE3Model(void);
		CE3Model(CString Name);

		// Destructor
		~CE3Model(void);

		// Properties
		CString GetName(void);

		// Registers
		CRegArray const &GetRegs(void) const;
		void MakeRegisters(CTextStreamMemory &Stream);
		BOOL FindRegister(CE3Register &Reg, UINT uTable, UINT uOffset) const;
		BOOL FindRegister(CE3Register &Reg, CString const &Prefix, CString const &Caption) const;
		BOOL AddRegister(CE3Register Reg);

	protected:
		CString		m_Name;
		CRegArray	m_Registers;

		// Implementation
		UINT GetRegType(UINT uTable);
	};

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Serial Driver
//

class CE3ModbusDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CE3ModbusDriver(void);

		// Destructor
		~CE3ModbusDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Serial Driver Options
//

class CE3ModbusDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CE3ModbusDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	
		// Public Data
		UINT m_Protocol;
		UINT m_Track;
		UINT m_Timeout;
			
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus TCP Driver
//

class CE3ModbusTCPDriver : public CE3ModbusDriver
{
	public:
		// Constructor
		CE3ModbusTCPDriver(void);

		// Destructor
		~CE3ModbusTCPDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Base Device Options
//

class CE3ModbusBaseDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CE3ModbusBaseDeviceOptions(void);

		// Destructor
		~CE3ModbusBaseDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// E3 Models
		void AddE3Model(CE3Model Model);
		UINT GetE3ModelCount(void);
		CE3Model const &GetCurrentModel(void);
		CE3Model GetE3Model(UINT uPos);
		CRegArray const &GetModelRegs(void) const;

		// Address Management
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL DoExpandAddress(CString &Text, CAddress const &Addr);

		// Type Helpers
		CString GetTypeString(UINT uType);
		UINT GetType(CString Type, UINT uDefault);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Data Members
		UINT		  m_Type;
		CArray<CE3Model>  m_Models;
		UINT		  m_Ping;
		UINT		  m_Drop;		
		UINT		  m_FlipLong;
		UINT		  m_FlipReal;

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		void DoEnables();

		// Implementation
		void LoadFromFile(void);
		UINT FindTable(CString Prefix);
	};

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Serial Device Options
//

class CE3ModbusSerialDeviceOptions : public CE3ModbusBaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CE3ModbusSerialDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Destructor
		~CE3ModbusSerialDeviceOptions(void);

	protected:
		// Data Members
		UINT	m_Timeout;
		
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus TCP Device Options
//

class CE3ModbusTCPDeviceOptions : public CE3ModbusBaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CE3ModbusTCPDeviceOptions(void);

		// Destructor
		~CE3ModbusTCPDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		DWORD	m_Addr;
		UINT	m_Port;
		UINT	m_Keep;
		UINT	m_UsePing;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;
		
		// Meta Data Creation
		void AddMetaData(void);
	};

////////////////////////////////////////////////////////////////////////
//
// E3 Modbus Base Device Options Custom UI Page
//

class CE3ModbusBaseDeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CE3ModbusBaseDeviceOptionsUIPage(CE3ModbusBaseDeviceOptions * pDevice);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CE3ModbusBaseDeviceOptions	* m_pDevice;

		// UI Help
		void LoadModels(IUICreate *pView, CItem *pItem);
		void LoadRegs(IUICreate *pView, CItem *pItems);
	};

////////////////////////////////////////////////////////////////////////
//
// E3 Modbus Serial Device Options Custom UI Page
//

class CE3ModbusSerialDeviceOptionsUIPage : public CE3ModbusBaseDeviceOptionsUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CE3ModbusSerialDeviceOptionsUIPage(CE3ModbusBaseDeviceOptions * pDevice);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);
	};

////////////////////////////////////////////////////////////////////////
//
// E3 Modbus TCP Device Options Custom UI Page
//

class CE3ModbusTCPDeviceOptionsUIPage : public CE3ModbusBaseDeviceOptionsUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CE3ModbusTCPDeviceOptionsUIPage(CE3ModbusBaseDeviceOptions * pDevice);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
//  E3 Modbus Dialog
//

class CE3ModbusDialog : public CStdDialog
{
	public:
		AfxDeclareRuntimeClass();

		// Constructor
		CE3ModbusDialog(CE3ModbusDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Destructor
		~CE3ModbusDialog(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Data Members
		CE3ModbusDriver&		  m_Driver;
		CE3ModbusBaseDeviceOptions	* m_pDevice;
		CAddress&			  m_Addr;
		CE3Register			  m_Reg;
		UINT				  m_uChan;

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uID);
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSelChanged(UINT uID, CWnd &Wnd);
		void OnTypeChanged(UINT uID, CWnd &Wnd);
		void OnChannelChanged(UINT uID, CWnd &Wnd);

		// Implementation
		void LoadRegs(void);
		void LoadTypes(void);
		void LoadChannels(void);
		void AppendType(void);
		void GetTypeString(UINT uType);
		void ShowDetails(void);
		void ShowAddress(void);
		void ShowType(void);
		void DoEnables(void);
	};

#endif

// End of File
