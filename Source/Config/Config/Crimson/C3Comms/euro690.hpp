
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EUROTHERM690_HPP
	
#define	INCLUDE_EUROTHERM690_HPP


//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 690 Ascii Master Device Options
//

class CEurotherm690DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEurotherm690DeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Group;
		UINT m_Unit;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 690 Ascii Master Driver
//

class CEuro690Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuro690Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);

		// Helper
		void GetIDs( UINT uOffset, PBYTE pHi, PBYTE pLo );
	};

// End of File

#endif
