
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EUROINVENSYSEPOWER_HPP
	
#define	INCLUDE_EUROINVENSYSEPOWER_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CEuroInvensysEPowerDriver;
class CEuroInvensysEPowerSerialDriver;
class CEuroInvensysEPowerTCPDriver;
class CEuroInvensysEPowerAddrDialog;

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	WL	addrWordAsLong
#define	WR	addrWordAsReal
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Register Table Spaces
#define	SP_0	 1	// Modbus Digital Outputs  00000
#define	SP_1	 2	// Modbus Digital Inputs   10000
#define	SP_3	 3	// Modbus Read-Only Words  30000
#define	SP_4	 4	// Modbus Read/Write Words 40000
// The following are Modbus Holding Registers
#define	SP_ACC	10	// Access
#define	SP_COM	11	// Comms
#define	SP_CON	12	// Control
#define	SP_COU	13	// Counter
#define	SP_CUS	14	// CustPage
#define	SP_CST	15	// CustPage Strings
#define	SP_ENE	16	// Energy
#define	SP_EVE	17	// EventLog
#define	SP_EVS	18	// EventLog Status
#define	SP_FAU	19	// Faultdet
#define	SP_FIR	20	// FiringOP
#define	SP_INS	21	// Instrument
#define	SP_IOI	22	// Analog IP IO
#define	SP_IOO	23	// Analog OP IO
#define	SP_IOD	24	// Digital IO
#define	SP_IOR	25	// Relay IO
#define	SP_IPM	26	// IPMonitor
#define	SP_LG2	27	// LGC2
#define	SP_LG8	28	// LGC8
#define	SP_LTC	29	// LTC
#define	SP_MAT	30	// Math2
#define	SP_MOD	31	// Modultr
#define	SP_AAC	32	// Network AlmAck
#define	SP_ADE	33	// Network AlmDet
#define	SP_ADI	34	// Network AlmDis
#define	SP_ALA	35	// Network AlmLat
#define	SP_ASI	36	// Network AlmSig
#define	SP_AST	37	// Network AlmStop
#define	SP_MEA	38	// Network Meas
#define	SP_NSU	39	// Network Setup
#define	SP_PLM	40	// PLM
#define	SP_PLC	41	// PLM Channel
#define	SP_QST	42	// QStart
#define	SP_SET	43	// SetProv
#define	SP_TIM	44	// Timer
#define	SP_TOT	45	// Total
#define	SP_USR	46	// UsrVal

// Dialog box numbers
#define	DPADD	2002
#define	DNAME	2021
#define	DNUMB	2031
#define	DMODB	2033

struct EIESTRINGS
{
	UINT	uQty;		// Number of strings entered for selection
	UINT	uBlkCt;		// Number of blocks for selection
	UINT	uBlock;		// Selected block
	UINT	uBPos;		// Position of Block # in List
	UINT	uAddr;		// ID
	UINT	uNPos;		// Position of Name in List
	UINT	uTable;		// Table number selected
	UINT	uOffset;	// Addr.a.m_Offset value
	}; 

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm/Invensys EPower TCP/IP Master Driver Options
//

class CEuroInvensysEPowerTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEuroInvensysEPowerTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Comms Driver
//

class CEuroInvensysEPowerDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuroInvensysEPowerDriver(void);

		// Destructor
		~CEuroInvensysEPowerDriver(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		EIESTRINGS * GetEIEPtr(void);

		BOOL	IsNotStdModbus(UINT uTable);

		void	InitItemInfo   (UINT uTable);
		void	LoadItemInfo   (UINT uTable);
		CString	LoadItemStrings(UINT uTable, BOOL fStore, UINT uSelect);

		BOOL	HasBlocks(void);

		CString	Get_sName(void);
		void	Set_sName(CString sName);
		UINT	GetAddrFromName(void);

	protected:

		// Data
		EIESTRINGS	EIESTR;
		EIESTRINGS	*m_pEIE;

		CString m_sName;	// Selected string

		// Implementation
		void	AddSpaces(void);

		CString	ParseParamString(CString sParamString, BOOL fStore);
		CString	ParseAddr(CString sAddr, BOOL fStore);
		void	ParseModifiers(CString sParamString, UINT *pInc);

		// Helpers

		// Other Help
		void	InitEIE(void);

		// String Info Loading

 		void 	LoadACCInfo(void);
		void 	LoadCOMInfo(void);
		void 	LoadCONInfo(void);
		void 	LoadCOUInfo(void);
		void 	LoadCUSInfo(void);
		void 	LoadCSTInfo(void);
		void 	LoadENEInfo(void);
		void 	LoadEVEInfo(void);
		void 	LoadEVSInfo(void);
		void 	LoadFAUInfo(void);
		void 	LoadFIRInfo(void);
		void 	LoadINSInfo(void);
		void 	LoadIOIInfo(void);
		void 	LoadIOOInfo(void);
		void 	LoadIODInfo(void);
		void 	LoadIORInfo(void);
		void 	LoadIPMInfo(void);
		void 	LoadLG2Info(void);
		void 	LoadLG8Info(void);
		void 	LoadLTCInfo(void);
		void 	LoadMATInfo(void);
		void 	LoadMODInfo(void);
		void 	LoadAACInfo(void);
		void 	LoadADEInfo(void);
		void 	LoadADIInfo(void);
		void 	LoadALAInfo(void);
		void 	LoadASIInfo(void);
		void 	LoadASTInfo(void);
		void 	LoadMEAInfo(void);
		void 	LoadNSUInfo(void);
		void 	LoadPLMInfo(void);
		void 	LoadPLCInfo(void);
		void 	LoadQSTInfo(void);
		void 	LoadSETInfo(void);
		void 	LoadTIMInfo(void);
		void 	LoadTOTInfo(void);
		void 	LoadUSRInfo(void);

		// String Loading
 		CString	LoadACC(UINT uSel);
		CString	LoadCOM(UINT uSel);
		CString	LoadCON(UINT uSel);
		CString	LoadCOU(UINT uSel);
		CString	LoadCUS(UINT uSel);
		CString	LoadCST(UINT uSel);
		CString	LoadENE(UINT uSel);
		CString	LoadEVE(UINT uSel);
		CString	LoadEVS(UINT uSel);
		CString	LoadFAU(UINT uSel);
		CString	LoadFIR(UINT uSel);
		CString	LoadINS(UINT uSel);
		CString	LoadIOI(UINT uSel);
		CString	LoadIOO(UINT uSel);
		CString	LoadIOD(UINT uSel);
		CString	LoadIOR(UINT uSel);
		CString	LoadIPM(UINT uSel);
		CString	LoadLG2(UINT uSel);
		CString	LoadLG8(UINT uSel);
		CString	LoadLTC(UINT uSel);
		CString	LoadMAT(UINT uSel);
		CString	LoadMOD(UINT uSel);
		CString	LoadAAC(UINT uSel);
		CString	LoadADE(UINT uSel);
		CString	LoadADI(UINT uSel);
		CString	LoadALA(UINT uSel);
		CString	LoadASI(UINT uSel);
		CString	LoadAST(UINT uSel);
		CString	LoadMEA(UINT uSel);
		CString	LoadNSU(UINT uSel);
		CString	LoadPLM(UINT uSel);
		CString	LoadPLC(UINT uSel);
		CString	LoadQST(UINT uSel);
		CString	LoadSET(UINT uSel);
		CString	LoadTIM(UINT uSel);
		CString	LoadTOT(UINT uSel);
		CString	LoadUSR(UINT uSel);
	};

class CEuroInvensysEPowerSerialDriver : public CEuroInvensysEPowerDriver
{
	public:
		// Constructor
		CEuroInvensysEPowerSerialDriver(void);

		// Destructor
		~CEuroInvensysEPowerSerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);
	};

class CEuroInvensysEPowerTCPDriver : public CEuroInvensysEPowerDriver
{
	public:
		// Constructor
		CEuroInvensysEPowerTCPDriver(void);

		// Destructor
		~CEuroInvensysEPowerTCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Invensys EuroInvensys Selection Dialog
//

class CEuroInvensysEPowerAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEuroInvensysEPowerAddrDialog(CEuroInvensysEPowerDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

		// Destructor
		~CEuroInvensysEPowerAddrDialog(void);
		                
	protected:
		CEuroInvensysEPowerDriver * m_pGDrv;

		EIESTRINGS	*m_pEIE;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk     (UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnComboChange(UINT uID, CWnd &Wnd);
		void	OnTypeChange (UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);
		void	LoadType(void);

		// Overrides
		BOOL	AllowType(UINT uType);

		// Selection Handling
		void	InitItemInfo(UINT uTable, BOOL fNew);
		void	LoadNames(void);
		void	SetItemData(void);
		void	SetBoxPosition(UINT uID, UINT uPos);
		UINT	GetBoxPosition(UINT uID);
		void	SetItemAddr (void);
		void	SetNameInfo(UINT uTable);
		void	SetBlockNumbers(void);
		void	GetParamData(UINT uPos);
		UINT	GetAddrFromText(void);

		// Helpers
		void	InitSelects(void);
		void	ClearSelData(void);
		void	ClearBox(CComboBox & ccb);
		void	ClearBox(UINT uID);
		void	ShowEPowerDetails(void);
		CString	StripAddr(CString sSelect);
		char	GetItemType(void);
	};

// String Format:
// {
//	Parameter Name
//	(Number of bits in parameter)
//	if( String 0 ) {
//		number of blocks-
//		if( number of blocks > 1 ) {
//			[block 0 starting address]
//			=increment between blocks
//			}
//		else {
//			[block address]
//	else {
//		offset from start of block
//		}
//	}

#define	SNOTU	L"Not DefinedW"
#define	SNONE	L""

// Access Strings
#define	SACC0	L"Access.ConfigurationPasscodeW@2021"
#define	SACC1	L"Access.EngineerPasscodeW@2020"
#define	SACC2	L"Access.GotoY@2018"
#define	SACC3	L"Access.IMY@199"
#define	SACC4	L"Access.KeylockY@2025"
#define	SACC5	L"Access.PasscodeW@2019"
#define	SACC6	L"Access.QuickStartPasscodeW@2022"

// Simplify initialization
#define	SACC1N	L"Access.ConfigurationPasscodeW"
#define	SACC1A	2021

// Comms Strings
#define	SCOM0	L"Comms.RmtPanel.AddressY@1942"
#define	SCOM1	L"Comms.RmtPanel.BaudY@1943"
#define	SCOM2	L"Comms.User.AddressY@1900"
#define	SCOM3	L"Comms.User.BaudY@1901"
#define	SCOM4	L"Comms.User.DCHP_enableB@1920"
#define	SCOM5	L"Comms.User.Default_Gateway_1Y@1912"
#define	SCOM6	L"Comms.User.Default_Gateway_2Y@1913"
#define	SCOM7	L"Comms.User.Default_Gateway_3Y@1914"
#define	SCOM8	L"Comms.User.Default_Gateway_4Y@1915"
#define	SCOM9	L"Comms.User.DelayY@1903"
#define	SCOM10	L"Comms.User.Extension_CyclesY@1945"
#define	SCOM11	L"Comms.User.IdY@1898"
#define	SCOM12	L"Comms.User.IP_address_1Y@1904"
#define	SCOM13	L"Comms.User.IP_address_2Y@1905"
#define	SCOM14	L"Comms.User.IP_address_3Y@1906"
#define	SCOM15	L"Comms.User.IP_address_4Y@1907"
#define	SCOM16	L"Comms.User.MAC1Y@1929"
#define	SCOM17	L"Comms.User.MAC2Y@1930"
#define	SCOM18	L"Comms.User.MAC3Y@1931"
#define	SCOM19	L"Comms.User.MAC4Y@1932"
#define	SCOM20	L"Comms.User.MAC5Y@1933"
#define	SCOM21	L"Comms.User.MAC6Y@1934"
#define	SCOM22	L"Comms.User.NetStatusY@1941"
#define	SCOM23	L"Comms.User.NetworkY@1921"
#define	SCOM24	L"Comms.User.Network_VersionY@1944"
#define	SCOM25	L"Comms.User.Occupied_StationsY@1946"
#define	SCOM26	L"Comms.User.ParityY@1902"
#define	SCOM27	L"Comms.User.PNDevNumY@3073"
#define	SCOM28	L"Comms.User.PNinitModeY@3072"
#define	SCOM29	L"Comms.User.Pref_Mstr_IP_1Y@1916"
#define	SCOM30	L"Comms.User.Pref_Mstr_IP_2Y@1917"
#define	SCOM31	L"Comms.User.Pref_Mstr_IP_3Y@1918"
#define	SCOM32	L"Comms.User.Pref_Mstr_IP_4Y@1919"
#define	SCOM33	L"Comms.User.ProtocolY@1899"
#define	SCOM34	L"Comms.User.ShowMacB@1928"
#define	SCOM35	L"Comms.User.Subnet_Mask_1Y@1908"
#define	SCOM36	L"Comms.User.Subnet_Mask_2Y@1909"
#define	SCOM37	L"Comms.User.Subnet_Mask_3Y@1910"
#define	SCOM38	L"Comms.User.Subnet_Mask_4Y@1911"
#define	SCOM39	L"Comms.User.UnitIdentY@1927"

// Control Strings Loops =  1-4
#define	SCON0	L"Control_1_AlmAck.ClosedLoopY@951#50"
#define	SCON1	L"Control_1_AlmAck.LimitationY@953#50"
#define	SCON2	L"Control_1_AlmAck.PVTransferY@952#50"
#define	SCON3	L"Control_1_AlmDet.ClosedLoopY@942#50"
#define	SCON4	L"Control_1_AlmDet.LimitationY@944#50"
#define	SCON5	L"Control_1_AlmDet.PVTransferY@943#50"
#define	SCON6	L"Control_1_AlmDis.ClosedLoopY@939#50"
#define	SCON7	L"Control_1_AlmDis.LimitationY@941#50"
#define	SCON8	L"Control_1_AlmDis.PVTransferY@940#50"
#define	SCON9	L"Control_1_AlmLat.ClosedLoopY@948#50"
#define	SCON10	L"Control_1_AlmLat.LimitationY@950#50"
#define	SCON11	L"Control_1_AlmLat.PVTransferY@949#50"
#define	SCON12	L"Control_1_AlmSig.ClosedLoopY@945#50"
#define	SCON13	L"Control_1_AlmSig.LimitationY@947#50"
#define	SCON14	L"Control_1_AlmSig.PVTransferY@946#50"
#define	SCON15	L"Control_1_AlmStop.ClosedLoopY@954#50"
#define	SCON16	L"Control_1_AlmStop.LimitationY@956#50"
#define	SCON17	L"Control_1_AlmStop.PVTransferY@955#50"
#define	SCON18	L"Control_1_Diag.OutputR@937#50"
#define	SCON19	L"Control_1_Diag.PAOPR@938#50"
#define	SCON20	L"Control_1_Diag.StatusY@936#50"
#define	SCON21	L"Control_1_Limit.PV1R@929#50"
#define	SCON22	L"Control_1_Limit.PV2R@930#50"
#define	SCON23	L"Control_1_Limit.PV3R@931#50"
#define	SCON24	L"Control_1_Limit.SP1R@932#50"
#define	SCON25	L"Control_1_Limit.SP2R@933#50"
#define	SCON26	L"Control_1_Limit.SP3R@934#50"
#define	SCON27	L"Control_1_Limit.TIR@935#50"
#define	SCON28	L"Control_1_Main.PVR@924#50"
#define	SCON29	L"Control_1_Main.SPR@925#50"
#define	SCON30	L"Control_1_Main.TIR@928#50"
#define	SCON31	L"Control_1_Main.TransferPVR@926#50"
#define	SCON32	L"Control_1_Main.TransferSpanR@927#50"
#define	SCON33	L"Control_1_Setup.EnLimitY@918#50"
#define	SCON34	L"Control_1_Setup.FFGainR@921#50"
#define	SCON35	L"Control_1_Setup.FFOffsetR@922#50"
#define	SCON36	L"Control_1_Setup.FFTypeY@920#50"
#define	SCON37	L"Control_1_Setup.NominalPVR@917#50"
#define	SCON38	L"Control_1_Setup.StandbyY@916#50"
#define	SCON39	L"Control_1_Setup.TransferEnY@919#50"

// Counter Strings Loops =  1-4
#define	SCOU0	L"Counter_1_ClearOverflowB@2578#19"
#define	SCOU1	L"Counter_1_ClockB@2574#19"
#define	SCOU2	L"Counter_1_CountL@2576#19"
#define	SCOU3	L"Counter_1_DirectionB@2571#19"
#define	SCOU4	L"Counter_1_EnableB@2570#19"
#define	SCOU5	L"Counter_1_OverFlowB@2573#19"
#define	SCOU6	L"Counter_1_ResetB@2577#19"
#define	SCOU7	L"Counter_1_RippleCarryB@2572#19"
#define	SCOU8	L"Counter_1_TargetL@2575#19"

// Customer Page Strings for Numerics Loops =  1-4
#define	SCUS0	L"CustPage_1_CISP1L@2040#20"
#define	SCUS1	L"CustPage_1_CISP2L@2041#20"
#define	SCUS2	L"CustPage_1_CISP3L@2042#20"
#define	SCUS3	L"CustPage_1_CISP4L@2043#20"
#define	SCUS4	L"CustPage_1_Style1Y@2044#20"
#define	SCUS5	L"CustPage_1_Style2Y@2045#20"
#define	SCUS6	L"CustPage_1_Style3Y@2046#20"
#define	SCUS7	L"CustPage_1_Style4Y@2047#20"

// Customer Page Strings for Text Loops =  1-4
#define	SCST0	L"CustPage_1_UserText1L@16384#20"
#define	SCST1	L"CustPage_1_UserText2L@16389#20"
#define	SCST2	L"CustPage_1_UserText3L@16394#20"
#define	SCST3	L"CustPage_1_UserText4L@16399#20"

// Energy StringsLoops =  1-5
#define	SENE0	L"Energy_1_AutoScaleUnitsB@2831#20"
#define	SENE1	L"Energy_1_HoldB@2821#20"
#define	SENE2	L"Energy_1_InputR@2822#20"
#define	SENE3	L"Energy_1_prvTotEnergyR@2832#20"
#define	SENE4	L"Energy_1_prvUsrEnergyR@2833#20"
#define	SENE5	L"Energy_1_PulseB@2825#20"
#define	SENE6	L"Energy_1_PulseLenW@2826#20"
#define	SENE7	L"Energy_1_PulseScaleY@2828#20"
#define	SENE8	L"Energy_1_ResetB@2823#20"
#define	SENE9	L"Energy_1_TotEnergyR@2824#20"
#define	SENE10	L"Energy_1_TotEnergyUnitY@2829#20"
#define	SENE11	L"Energy_1_TypeB@2830#20"
#define	SENE12	L"Energy_1_UsrEnergyR@2820#20"
#define	SENE13	L"Energy_1_UsrEnergyUnitY@2827#20"

// Event Log StringsLoops =  1-40
#define	SEVE0	L"EventLog.Event_1_IDY@1807#2"
#define	SEVE1	L"EventLog.Event_1_TypeY@1806#2"

// Event Log Status String
#define	SEVS0	L"EventLog.StatusY@1887"

// Fault Detect Strings
#define	SFAU0	L"Faultdet.AlarmStatus1W@1704"
#define	SFAU1	L"Faultdet.AlarmStatus2W@1705"
#define	SFAU2	L"Faultdet.AnyFuseAlY@1699"
#define	SFAU3	L"Faultdet.AnyNetwAlY@1698"
#define	SFAU4	L"Faultdet.GeneralAckY@1695"
#define	SFAU5	L"Faultdet.GlobalDisY@1700"
#define	SFAU6	L"Faultdet.StratStatusW@1702"
#define	SFAU7	L"Faultdet.WatchdogY@1703"

// Firing OP StringsLoops =  1-4
#define	SFIR0	L"FiringOP_1_DelayedTriggerY@1210#21"
#define	SFIR1	L"FiringOP_1_EnableY@1214#21"
#define	SFIR2	L"FiringOP_1_InR@1211#21"
#define	SFIR3	L"FiringOP_1_LoadCouplingY@1204#21"
#define	SFIR4	L"FiringOP_1_LoadTypeY@1205#21"
#define	SFIR5	L"FiringOP_1_ModeY@1206#21"
#define	SFIR6	L"FiringOP_1_PaLimitInR@1212#21"
#define	SFIR7	L"FiringOP_1_SafetyRampR@1207#21"
#define	SFIR8	L"FiringOP_1_SafetyRampStatusY@1213#21"
#define	SFIR9	L"FiringOP_1_SoftStartR@1208#21"
#define	SFIR10	L"FiringOP_1_SoftStopR@1209#21"

// Instrument Strings
#define	SINS0	L"Instrument.Configuration.IOModulesY@2209"
#define	SINS1	L"Instrument.Configuration.PwrModTypeY@2228"
#define	SINS2	L"Instrument.Configuration.LoadCouplingY@2202"
#define	SINS3	L"Instrument.Configuration.LoadCoupling2ndNetworkY@2210"
#define	SINS4	L"Instrument.Configuration.LoadMFittedB@2212"
#define	SINS5	L"Instrument.Configuration.NetTypeY@2199"
#define	SINS6	L"Instrument.Configuration.PowerModulesY@2198"
#define	SINS7	L"Instrument.Configuration.PwrMod1RevY@2204"
#define	SINS8	L"Instrument.Configuration.PwrMod2RevY@2205"
#define	SINS9	L"Instrument.Configuration.PwrMod3RevY@2206"
#define	SINS10	L"Instrument.Configuration.PwrMod4RevY@2207"
#define	SINS11	L"Instrument.Configuration.RemotePVR@2211"
#define	SINS12	L"Instrument.Configuration.TimerResY@2208"
#define	SINS13	L"Instrument.Display.LanguageY@2169"
#define	SINS14	L"Instrument.Display.SerialNoL@2170"
#define	SINS15	L"Instrument.IDW@122"
#define	SINS16	L"Instrument.ModeY@199"

// IO Strings - Anal. IP Loops =  1-5
#define	SIOI0	L"IO.AnalogIP_1_Main.MeasValR@1491#15"
#define	SIOI1	L"IO.AnalogIP_1_Main.PVR@1492#15"
#define	SIOI2	L"IO.AnalogIP_1_Main.RangeHighR@1489#15"
#define	SIOI3	L"IO.AnalogIP_1_Main.RangeLowR@1490#15"
#define	SIOI4	L"IO.AnalogIP_1_Main.TypeY@1488#15"

// IO Strings - Anal. OP Loops =  1-4
#define	SIOO0	L"IO.AnalogOP_1_AlmAck.OutputFaultY@1572#21"
#define	SIOO1	L"IO.AnalogOP_1_AlmDet.OutputFaultY@1569#21"
#define	SIOO2	L"IO.AnalogOP_1_AlmDis.OutputFaultY@1568#21"
#define	SIOO3	L"IO.AnalogOP_1_AlmLat.OutputFaultY@1571#21"
#define	SIOO4	L"IO.AnalogOP_1_AlmSig.OutputFaultY@1570#21"
#define	SIOO5	L"IO.AnalogOP_1_AlmStop.OutputFaultY@1573#21"
#define	SIOO6	L"IO.AnalogOP_1_Main.MeasValR@1567#21"
#define	SIOO7	L"IO.AnalogOP_1_Main.PVR@1566#21"
#define	SIOO8	L"IO.AnalogOP_1_Main.RangeHighR@1564#21"
#define	SIOO9	L"IO.AnalogOP_1_Main.RangeLowR@1565#21"
#define	SIOO10	L"IO.AnalogOP_1_Main.TypeY@1563#21"

// IO Strings - Digital In Loops =  1-8
#define	SIOD0	L"IO.Digital_1_InvertB@1369#15"
#define	SIOD1	L"IO.Digital_1_MeasValB@1370#15"
#define	SIOD2	L"IO.Digital_1_PVB@1371#15"
#define	SIOD3	L"IO.Digital_1_TypeY@1368#15"

// IO Strings - Relay Loops =  1-4
#define	SIOR0	L"IO.Relay_1_MeasValB@1648#12"
#define	SIOR1	L"IO.Relay_1_PVB@1647#12"

// IP Monitor Strings Loops =  1-4
#define	SIPM0	L"IPMonitor_1_AlarmDaysY@2655#22"
#define	SIPM1	L"IPMonitor_1_AlarmTimeL@2653#22"
#define	SIPM2	L"IPMonitor_1_DaysAboveY@2654#22"
#define	SIPM3	L"IPMonitor_1_InR@2647#22"
#define	SIPM4	L"IPMonitor_1_InStatusB@2656#22"
#define	SIPM5	L"IPMonitor_1_MaxR@2649#22"
#define	SIPM6	L"IPMonitor_1_MinR@2650#22"
#define	SIPM7	L"IPMonitor_1_OutB@2652#22"
#define	SIPM8	L"IPMonitor_1_ResetB@2648#22"
#define	SIPM9	L"IPMonitor_1_ThresholdR@2646#22"
#define	SIPM10	L"IPMonitor_1_TimeAboveL@2651#22"

// LGC2 Strings Loops =  1-4
#define	SL2G0	L"Lgc2_1_FallbackTypeY@2743#10"
#define	SL2G1	L"Lgc2_1_HysteresisR@2747#10"
#define	SL2G2	L"Lgc2_1_In1R@2741#10"
#define	SL2G3	L"Lgc2_1_In2R@2742#10"
#define	SL2G4	L"Lgc2_1_InvertY@2744#10"
#define	SL2G5	L"Lgc2_1_OperY@2740#10"
#define	SL2G6	L"Lgc2_1_OutB@2745#10"
#define	SL2G7	L"Lgc2_1_StatusB@2746#10"

// LGC8 Strings Loops =  1-4
#define	SL8G0	L"Lgc8_1_In1B@2481#23"
#define	SL8G1	L"Lgc8_1_In2B@2482#23"
#define	SL8G2	L"Lgc8_1_In3B@2483#23"
#define	SL8G3	L"Lgc8_1_In4B@2484#23"
#define	SL8G4	L"Lgc8_1_In5B@2485#23"
#define	SL8G5	L"Lgc8_1_In6B@2486#23"
#define	SL8G6	L"Lgc8_1_In7B@2487#23"
#define	SL8G7	L"Lgc8_1_In8B@2488#23"
#define	SL8G8	L"Lgc8_1_InInvertY@2479#23"
#define	SL8G9	L"Lgc8_1_NumInY@2480#23"
#define	SL8G10	L"Lgc8_1_OperY@2478#23"
#define	SL8G11	L"Lgc8_1_OutB@2489#23"
#define	SL8G12	L"Lgc8_1_OutInvertB@2490#23"

// LTC Strings
#define	SLTC0	L"LTC.AlmAck.FuseY@2802"
#define	SLTC1	L"LTC.AlmAck.TempY@2803"
#define	SLTC2	L"LTC.AlmDet.FuseY@2796"
#define	SLTC3	L"LTC.AlmDet.TempY@2797"
#define	SLTC4	L"LTC.AlmDis.FuseY@2794"
#define	SLTC5	L"LTC.AlmDis.TempY@2795"
#define	SLTC6	L"LTC.AlmLat.FuseY@2800"
#define	SLTC7	L"LTC.AlmLat.TempY@2801"
#define	SLTC8	L"LTC.AlmSig.FuseY@2798"
#define	SLTC9	L"LTC.AlmSig.TempY@2799"
#define	SLTC10	L"LTC.AlmStop.FuseY@2804"
#define	SLTC11	L"LTC.AlmStop.TempY@2805"
#define	SLTC12	L"LTC.MainPrm.AlFuseInY@2792"
#define	SLTC13	L"LTC.MainPrm.AlTempInY@2793"
#define	SLTC14	L"LTC.MainPrm.IPR@2782"
#define	SLTC15	L"LTC.MainPrm.OP1R@2788"
#define	SLTC16	L"LTC.MainPrm.OP2R@2789"
#define	SLTC17	L"LTC.MainPrm.OP3R@2790"
#define	SLTC18	L"LTC.MainPrm.OP4R@2791"
#define	SLTC19	L"LTC.MainPrm.PAOPR@2783"
#define	SLTC20	L"LTC.MainPrm.S1R@2784"
#define	SLTC21	L"LTC.MainPrm.S2R@2785"
#define	SLTC22	L"LTC.MainPrm.S3R@2786"
#define	SLTC23	L"LTC.MainPrm.S4R@2787"
#define	SLTC24	L"LTC.MainPrm.TapNbY@2781"
#define	SLTC25	L"LTC.MainPrm.TypeY@2780"

// MATH2 Strings Loops =  1-4
#define	SMAT0	L"Math2_1_FallbackY@2242#24"
#define	SMAT1	L"Math2_1_FallbackValR@2235#24"
#define	SMAT2	L"Math2_1_HighLimitR@2236#24"
#define	SMAT3	L"Math2_1_In1R@2231#24"
#define	SMAT4	L"Math2_1_In1MulR@2230#24"
#define	SMAT5	L"Math2_1_In2R@2233#24"
#define	SMAT6	L"Math2_1_In2MulR@2232#24"
#define	SMAT7	L"Math2_1_LowLimitR@2237#24"
#define	SMAT8	L"Math2_1_OperY@2234#24"
#define	SMAT9	L"Math2_1_OutR@2239#24"
#define	SMAT10	L"Math2_1_ResolutionY@2240#24"
#define	SMAT11	L"Math2_1_SelectB@2243#24"
#define	SMAT12	L"Math2_1_StatusB@2238#24"
#define	SMAT13	L"Math2_1_UnitsY@2241#24"

// Modulator Strings Loops =  1-4
#define	SMOD0	L"Modultr_1_CycleTimeW@1119#22"
#define	SMOD1	L"Modultr_1_InR@1117#22"
#define	SMOD2	L"Modultr_1_LgcModeY@1120#22"
#define	SMOD3	L"Modultr_1_MinOnTimeW@1118#22"
#define	SMOD4	L"Modultr_1_ModeY@1122#22"
#define	SMOD5	L"Modultr_1_OutR@1116#22"
#define	SMOD6	L"Modultr_1_PLMinW@1121#22"
#define	SMOD7	L"Modultr_1_SwitchPAY@1126#22"

// Network AlmAck Loops 1-4
#define	SAAC0	L"Network_1_AlmAck.ChopOffY@391#165"
#define	SAAC1	L"Network_1_AlmAck.FreqFaultY@388#165"
#define	SAAC2	L"Network_1_AlmAck.FuseBlownY@385#165"
#define	SAAC3	L"Network_1_AlmAck.MainsVoltFaultY@394#165"
#define	SAAC4	L"Network_1_AlmAck.MissMainsY@382#165"
#define	SAAC5	L"Network_1_AlmAck.NetworkDipsY@387#165"
#define	SAAC6	L"Network_1_AlmAck.OpenThyrY@384#165"
#define	SAAC7	L"Network_1_AlmAck.OverCurrentY@396#165"
#define	SAAC8	L"Network_1_AlmAck.OverTempY@386#165"
#define	SAAC9	L"Network_1_AlmAck.PB24VFailY@389#165"
#define	SAAC10	L"Network_1_AlmAck.PLFY@392#165"
#define	SAAC11	L"Network_1_AlmAck.PLUY@393#165"
#define	SAAC12	L"Network_1_AlmAck.PreTempY@395#165"
#define	SAAC13	L"Network_1_AlmAck.ThyrSCY@383#165"
#define	SAAC14	L"Network_1_AlmAck.TLFY@390#165"

// Network AlmDet Loops 1-4
#define	SADE0	L"Network_1_AlmDet.ChopOffY@346#165"
#define	SADE1	L"Network_1_AlmDet.FreqFaultY@343#165"
#define	SADE2	L"Network_1_AlmDet.FuseBlownY@340#165"
#define	SADE3	L"Network_1_AlmDet.MainsVoltFaultY@349#165"
#define	SADE4	L"Network_1_AlmDet.MissMainsY@337#165"
#define	SADE5	L"Network_1_AlmDet.NetworkDipsY@342#165"
#define	SADE6	L"Network_1_AlmDet.OpenThyrY@339#165"
#define	SADE7	L"Network_1_AlmDet.OverCurrentY@351#165"
#define	SADE8	L"Network_1_AlmDet.OverTempY@341#165"
#define	SADE9	L"Network_1_AlmDet.PB24VFailY@344#165"
#define	SADE10	L"Network_1_AlmDet.PLFY@347#165"
#define	SADE11	L"Network_1_AlmDet.PLUY@348#165"
#define	SADE12	L"Network_1_AlmDet.PreTempY@350#165"
#define	SADE13	L"Network_1_AlmDet.ThyrSCY@338#165"
#define	SADE14	L"Network_1_AlmDet.TLFY@345#165"

// Network AlmDis Loops 1-4
#define	SADI0	L"Network_1_AlmDis.ChopOffY@331#165"
#define	SADI1	L"Network_1_AlmDis.FreqFaultY@328#165"
#define	SADI2	L"Network_1_AlmDis.FuseBlownY@325#165"
#define	SADI3	L"Network_1_AlmDis.MainsVoltFaultY@334#165"
#define	SADI4	L"Network_1_AlmDis.MissMainsY@322#165"
#define	SADI5	L"Network_1_AlmDis.NetworkDipsY@327#165"
#define	SADI6	L"Network_1_AlmDis.OpenThyrY@324#165"
#define	SADI7	L"Network_1_AlmDis.OverCurrentY@336#165"
#define	SADI8	L"Network_1_AlmDis.OverTempY@326#165"
#define	SADI9	L"Network_1_AlmDis.PB24VFailY@329#165"
#define	SADI10	L"Network_1_AlmDis.PLFY@332#165"
#define	SADI11	L"Network_1_AlmDis.PLUY@333#165"
#define	SADI12	L"Network_1_AlmDis.PreTempY@335#165"
#define	SADI13	L"Network_1_AlmDis.ThyrSCY@323#165"
#define	SADI14	L"Network_1_AlmDis.TLFY@330#165"

// Network AlmLat Loops 1-4
#define	SALA0	L"Network_1_AlmLat.ChopOffY@376#165"
#define	SALA1	L"Network_1_AlmLat.FreqFaultY@373#165"
#define	SALA2	L"Network_1_AlmLat.FuseBlownY@370#165"
#define	SALA3	L"Network_1_AlmLat.MainsVoltFaultY@379#165"
#define	SALA4	L"Network_1_AlmLat.MissMainsY@367#165"
#define	SALA5	L"Network_1_AlmLat.NetworkDipsY@372#165"
#define	SALA6	L"Network_1_AlmLat.OpenThyrY@369#165"
#define	SALA7	L"Network_1_AlmLat.OverCurrentY@381#165"
#define	SALA8	L"Network_1_AlmLat.OverTempY@371#165"
#define	SALA9	L"Network_1_AlmLat.PB24VFailY@374#165"
#define	SALA10	L"Network_1_AlmLat.PLFY@377#165"
#define	SALA11	L"Network_1_AlmLat.PLUY@378#165"
#define	SALA12	L"Network_1_AlmLat.PreTempY@380#165"
#define	SALA13	L"Network_1_AlmLat.ThyrSCY@368#165"
#define	SALA14	L"Network_1_AlmLat.TLFY@375#165"

// Network AlmSig Loops 1-4
#define	SASI0	L"Network_1_AlmSig.ChopOffY@361#165"
#define	SASI1	L"Network_1_AlmSig.FreqFaultY@358#165"
#define	SASI2	L"Network_1_AlmSig.FuseBlownY@355#165"
#define	SASI3	L"Network_1_AlmSig.MainsVoltFaultY@364#165"
#define	SASI4	L"Network_1_AlmSig.MissMainsY@352#165"
#define	SASI5	L"Network_1_AlmSig.NetworkDipsY@357#165"
#define	SASI6	L"Network_1_AlmSig.OpenThyrY@354#165"
#define	SASI7	L"Network_1_AlmSig.OverCurrentY@366#165"
#define	SASI8	L"Network_1_AlmSig.OverTempY@356#165"
#define	SASI9	L"Network_1_AlmSig.PB24VFailY@359#165"
#define	SASI10	L"Network_1_AlmSig.PLFY@362#165"
#define	SASI11	L"Network_1_AlmSig.PLUY@363#165"
#define	SASI12	L"Network_1_AlmSig.PreTempY@365#165"
#define	SASI13	L"Network_1_AlmSig.ThyrSCY@353#165"
#define	SASI14	L"Network_1_AlmSig.TLFY@360#165"

// Network AlmStop Loops 1-4
#define	SAST0	L"Network_1_AlmStop.ChopOffY@406#165"
#define	SAST1	L"Network_1_AlmStop.FreqFaultY@403#165"
#define	SAST2	L"Network_1_AlmStop.FuseBlownY@400#165"
#define	SAST3	L"Network_1_AlmStop.MainsVoltFaultY@409#165"
#define	SAST4	L"Network_1_AlmStop.MissMainsY@397#165"
#define	SAST5	L"Network_1_AlmStop.NetworkDipsY@402#165"
#define	SAST6	L"Network_1_AlmStop.OpenThyrY@399#165"
#define	SAST7	L"Network_1_AlmStop.OverCurrentY@411#165"
#define	SAST8	L"Network_1_AlmStop.OverTempY@401#165"
#define	SAST9	L"Network_1_AlmStop.PB24VFailY@404#165"
#define	SAST10	L"Network_1_AlmStop.PLFY@407#165"
#define	SAST11	L"Network_1_AlmStop.PLUY@408#165"
#define	SAST12	L"Network_1_AlmStop.PreTempY@410#165"
#define	SAST13	L"Network_1_AlmStop.ThyrSCY@398#165"
#define	SAST14	L"Network_1_AlmStop.TLFY@405#165"

// Network Measure Loops 1-4
#define	SMEA0	L"Network_1_Meas.FrequencyR@280#165"
#define	SMEA1	L"Network_1_Meas.HtSinkTempR@282#165"
#define	SMEA2	L"Network_1_Meas.HtSinkTmp2R@283#165"
#define	SMEA3	L"Network_1_Meas.HtSinkTmp3R@284#165"
#define	SMEA4	L"Network_1_Meas.IR@259#165"
#define	SMEA5	L"Network_1_Meas.I2R@260#165"
#define	SMEA6	L"Network_1_Meas.I3R@261#165"
#define	SMEA7	L"Network_1_Meas.IavgR@262#165"
#define	SMEA8	L"Network_1_Meas.IrmsMaxR@288#165"
#define	SMEA9	L"Network_1_Meas.IsqR@264#165"
#define	SMEA10	L"Network_1_Meas.IsqBurstR@263#165"
#define	SMEA11	L"Network_1_Meas.IsqMaxR@265#165"
#define	SMEA12	L"Network_1_Meas.PR@273#165"
#define	SMEA13	L"Network_1_Meas.PBurstR@272#165"
#define	SMEA14	L"Network_1_Meas.PFR@275#165"
#define	SMEA15	L"Network_1_Meas.QR@276#165"
#define	SMEA16	L"Network_1_Meas.SR@274#165"
#define	SMEA17	L"Network_1_Meas.VR@266#165"
#define	SMEA18	L"Network_1_Meas.V2R@267#165"
#define	SMEA19	L"Network_1_Meas.V3R@268#165"
#define	SMEA20	L"Network_1_Meas.VavgR@269#165"
#define	SMEA21	L"Network_1_Meas.VlineR@256#165"
#define	SMEA22	L"Network_1_Meas.Vline2R@257#165"
#define	SMEA23	L"Network_1_Meas.Vline3R@258#165"
#define	SMEA24	L"Network_1_Meas.VrmsMaxR@289#165"
#define	SMEA25	L"Network_1_Meas.VsqR@270#165"
#define	SMEA26	L"Network_1_Meas.VsqBurstR@281#165"
#define	SMEA27	L"Network_1_Meas.VsqMaxR@271#165"
#define	SMEA28	L"Network_1_Meas.ZR@277#165"
#define	SMEA29	L"Network_1_Meas.Z2R@278#165"
#define	SMEA30	L"Network_1_Meas.Z3R@279#165"

// Network Setup Loops 1-4
#define	SNSU0	L"Network_1_Setup.ChopOffNbY@294#165"
#define	SNSU1	L"Network_1_Setup.ChopOffThreshold1Y@292#165"
#define	SNSU2	L"Network_1_Setup.ChopOffThreshold2W@293#165"
#define	SNSU3	L"Network_1_Setup.ChopOffWindowW@295#165"
#define	SNSU4	L"Network_1_Setup.FreqDriftThresholdR@319#165"
#define	SNSU5	L"Network_1_Setup.HeaterTypeY@303#165"
#define	SNSU6	L"Network_1_Setup.HeatsinkPreTempY@298#165"
#define	SNSU7	L"Network_1_Setup.HeatsinkTmaxY@290#165"
#define	SNSU8	L"Network_1_Setup.IextScaleR@306#165"
#define	SNSU9	L"Network_1_Setup.IMaximumY@136#165"
#define	SNSU10	L"Network_1_Setup.INominalR@309#165"
#define	SNSU11	L"Network_1_Setup.NetTypeY@307#165"
#define	SNSU12	L"Network_1_Setup.OverIThresholdW@302#165"
#define	SNSU13	L"Network_1_Setup.OverVoltThresholdY@296#165"
#define	SNSU14	L"Network_1_Setup.PLFAdjustedY@299#165"
#define	SNSU15	L"Network_1_Setup.PLFAdjustReqY@305#165"
#define	SNSU16	L"Network_1_Setup.PLFSensitivityY@300#165"
#define	SNSU17	L"Network_1_Setup.PLUthresholdY@301#165"
#define	SNSU18	L"Network_1_Setup.UnderVoltThresholdY@297#165"
#define	SNSU19	L"Network_1_Setup.VdipsThresholdY@291#165"
#define	SNSU20	L"Network_1_Setup.VextScaleR@320#165"
#define	SNSU21	L"Network_1_Setup.VlineNominalR@304#165"
#define	SNSU22	L"Network_1_Setup.VloadNominalR@308#165"
#define	SNSU23	L"Network_1_Setup.VMaximumY@w321#165"
#define	SNSU24	L"Network_1_Setup.ZrefR@313#165"
#define	SNSU25	L"Network_1_Setup.Zref2R@314#165"
#define	SNSU26	L"Network_1_Setup.Zref3R@315#165"

// PLM Strings
#define	SPLM0	L"PLM.AlmAck.PrOverPsY@1734"
#define	SPLM1	L"PLM.AlmDet.PrOverPsY@1731"
#define	SPLM2	L"PLM.AlmDis.PrOverPsY@1730"
#define	SPLM3	L"PLM.AlmLat.PrOverPsY@1733"
#define	SPLM4	L"PLM.AlmSig.PrOverPsY@1732"
#define	SPLM5	L"PLM.AlmStop.PrOverPsY@1735"
#define	SPLM6	L"PLM.Main.PeriodW@1714"
#define	SPLM7	L"PLM.Main.TypeY@1713"
#define	SPLM8	L"PLM.Network.EfficiencyY@1728"
#define	SPLM9	L"PLM.Network.MasterAddrY@1729"
#define	SPLM10	L"PLM.Network.PmaxR@1724"
#define	SPLM11	L"PLM.Network.PrR@1727"
#define	SPLM12	L"PLM.Network.PsR@1726"
#define	SPLM13	L"PLM.Network.PtR@1725"
#define	SPLM14	L"PLM.Network.TotalChannelsY@1723"
#define	SPLM15	L"PLM.Network.TotalStationY@1722"
#define	SPLM16	L"PLM.Station.AddressY@1715"
#define	SPLM17	L"PLM.Station.NumChanY@1717"
#define	SPLM18	L"PLM.Station.PLMOut1W@1718"
#define	SPLM19	L"PLM.Station.PLMOut2W@1719"
#define	SPLM20	L"PLM.Station.PLMOut3W@1720"
#define	SPLM21	L"PLM.Station.PLMOut4W@1721"
#define	SPLM22	L"PLM.Station.StatusY@1716"

// PLM Channel Strings Loops =  1-4
#define	SPLC0	L"PLMChan_1_GroupY@1747#15"
#define	SPLC1	L"PLMChan_1_PLMInW@1749#15"
#define	SPLC2	L"PLMChan_1_PLMOutW@1750#15"
#define	SPLC3	L"PLMChan_1_PZMaxR@1746#15"
#define	SPLC4	L"PLMChan_1_ShedFactorY@1748#15"

// Quick Start Strings
#define	SQST0	L"QStart.AnalogIP1FuncY@2122"
#define	SQST1	L"QStart.AnalogIP2FuncY@2123"
#define	SQST2	L"QStart.AnalogOP1FuncY@2120"
#define	SQST3	L"QStart.DigitalIP2FuncY@2121"
#define	SQST4	L"QStart.EnergyY@2135"
#define	SQST5	L"QStart.FeedbackY@2119"
#define	SQST6	L"QStart.FinishY@2118"
#define	SQST7	L"QStart.FiringModeY@2126"
#define	SQST8	L"QStart.LoadCurrentY@2124"
#define	SQST9	L"QStart.LoadCurrentValW@2134"
#define	SQST10	L"QStart.LoadTypeY@2129"
#define	SQST11	L"QStart.LoadVoltageY@2125"
#define	SQST12	L"QStart.Relay1Y@2128"
#define	SQST13	L"QStart.TransferY@2127"

// Set Prov Strings Loops =  1-4
#define	SSET0	L"SetProv_1_DisRampY@1292#20"
#define	SSET1	L"SetProv_1_EngWorkingSPR@1301#20"
#define	SSET2	L"SetProv_1_HiRangeR@1299#20"
#define	SSET3	L"SetProv_1_LimitR@1297#20"
#define	SSET4	L"SetProv_1_LocalSPR@1288#20"
#define	SSET5	L"SetProv_1_RampRateR@1291#20"
#define	SSET6	L"SetProv_1_Remote1R@1294#20"
#define	SSET7	L"SetProv_1_Remote2R@1295#20"
#define	SSET8	L"SetProv_1_RemSelectY@1296#20"
#define	SSET9	L"SetProv_1_SPSelectY@1290#20"
#define	SSET10	L"SetProv_1_SPTrackY@1298#20"
#define	SSET11	L"SetProv_1_SPUnitsY@1300#20"
#define	SSET12	L"SetProv_1_WorkingSPR@1289#20"

// Timer Strings Loops =  1-4
#define	STIM0	L"Timer_1_ElapsedTimeL@2326#17"
#define	STIM1	L"Timer_1_InB@2331#17"
#define	STIM2	L"Timer_1_OutB@2327#17"
#define	STIM3	L"Timer_1_TimeL@2328#17"
#define	STIM4	L"Timer_1_TriggeredB@2329#17"
#define	STIM5	L"Timer_1_TypeY@2330#17"

// Totalizer Strings Loops =  1-4
#define	STOT0	L"Total_1_AlarmOutB@2396#21"
#define	STOT1	L"Total_1_AlarmSPR@2394#21"
#define	STOT2	L"Total_1_HoldB@2401#21"
#define	STOT3	L"Total_1_InR@2399#21"
#define	STOT4	L"Total_1_ResetB@2402#21"
#define	STOT5	L"Total_1_ResolutionY@2398#21"
#define	STOT6	L"Total_1_RunB@2400#21"
#define	STOT7	L"Total_1_TotalOutR@2395#21"
#define	STOT8	L"Total_1_UnitsY@2397#21"

// User Variable Strings Loops =  1-4
#define	SUSR0	L"UsrVal_1_HighLimitR@1956#16"
#define	SUSR1	L"UsrVal_1_LowLimitR@1957#16"
#define	SUSR2	L"UsrVal_1_ResolutionY@1955#16"
#define	SUSR3	L"UsrVal_1_StatusB@1959#16"
#define	SUSR4	L"UsrVal_1_UnitsY@1954#16"
#define	SUSR5	L"UsrVal_1_ValR@1958#16"

// End of File
#endif
