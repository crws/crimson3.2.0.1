
#include "intern.hpp"

#include "impactd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String Address Meta Item
//

// Dynamic Class

AfxImplementDynamicClass(CImpactStrAddr, CMetaItem);

// Constructor

CImpactStrAddr::CImpactStrAddr(void)
{
	m_String = "";

	m_Addr.m_Ref = 0;

	m_Send  = "";

	m_Sym   = "";

	m_Type  = 0;
   	}

CImpactStrAddr::CImpactStrAddr(CString Str, CAddress Addr, BYTE bType)
{
	m_Addr.m_Ref = 0; 

	Set(Str, Addr, bType);
	}

// Destructor

CImpactStrAddr::~CImpactStrAddr(void)
{
	
	} 

// Download Support

BOOL CImpactStrAddr::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_Type));

	Init.AddLong(LONG(m_Addr.m_Ref));

	Init.AddText(m_Send);

	return TRUE;
	}

// Persistance

void CImpactStrAddr::Load(CTreeFile &Tree)
{
	AddMeta();

	Tree.GetName();
	
	CString Str = Tree.GetValueAsString();

	Tree.GetName();

	CAddress Addr;
	
	Addr.m_Ref = Tree.GetValueAsInteger();

	Tree.GetName();

	BYTE bType = Tree.GetValueAsInteger() & 0xFF;

	this->Set(Str, Addr, bType);	
	}

void CImpactStrAddr::Save(CTreeFile &Tree)
{
	Tree.PutValue(L"Str", m_String);

	Tree.PutValue(L"Addr", m_Addr.m_Ref);

	Tree.PutValue(L"Type", m_Type);
	}

// Data Access

CString CImpactStrAddr::GetString(void)
{
	return m_Sym;
	}

CString CImpactStrAddr::GetFullString(void)
{
	return m_String;
	}

CAddress CImpactStrAddr::GetAddress(void)
{
	return m_Addr;
	}

BOOL CImpactStrAddr::Set(CString Str, CAddress Addr, BYTE bType)
{
	if( Addr.m_Ref ) {

		m_Addr.m_Ref = Addr.m_Ref;

		if( m_String.IsEmpty() ) {

			m_String = Str;
			}


		m_Type = bType;

		SetSendString(Str);

		SetSymString(Str);

		return TRUE;
		}

	return FALSE;
	}

// Meta Data Creation

void CImpactStrAddr::AddMetaData(void)	
{
	CMetaItem::AddMetaData();

	Meta_AddString(String);

	Meta_AddInteger(Addr);

	Meta_AddInteger(Type);
	}

// Implementation

void CImpactStrAddr::SetSendString(CString Str)
{
	if( m_Type == tUser ) {

		SetUserSendString(Str);

		return;
		}

	UINT uFind = Str.Find('.');

	if( uFind < NOTHING ) {

		Str = Str.Left(uFind);
		}

	m_Send = Str;

	SendReplace(':', ".");
	
	SendReplace(' ', "\x25\x25\x32\x30");  

	SendReplace('=', "\x25\x25\x33\x44");

	SendReplace(',', "\x25\x25\x32\x43");

	if( m_Type != tRun ) {

		SendReplaceLast('.', ':');
		}
	}

void CImpactStrAddr::SetUserSendString(CString Str)
{
	UINT uType = Str.Find('.');

	UINT uPos  = 0;

	CString Type = Str;

	m_Send = Str;

	while( uType < NOTHING ) {

		if( !IsType(Type) ) {

			uPos += uType + 1;

			Type = Type.Mid(uType + 1);

			uType = Type.Find('.');

			continue;
			}
		
		break;
		}

	if( uPos && IsType(Type) ) {

		m_Send = Str.Left(uPos - 1);
		}

	SendReplace(' ', "\x25\x25\x32\x30");  

	SendReplace('=', "\x25\x25\x33\x44");

	SendReplace(',', "\x25\x25\x32\x43");
	}


void CImpactStrAddr::SetSymString(CString Str)
{
	if( m_Type == tUser ) {

		SetUserSymString(Str);

		return;
		}

	CString Sym = Str;

	Sym.Remove(' ');

	Sym.Remove('-');

	UINT uFind = Sym.Find(':');

	m_Sym = "";

	while( uFind < NOTHING ) {

		m_Sym += Sym.Mid(0, min(SYM_W, uFind));

		m_Sym += ":";
		
		Sym = Sym.Mid(uFind + 1);

		uFind = Sym.Find(':');
		}

	uFind = Sym.Find('.');
	
	if( uFind < NOTHING ) {

		m_Sym += Sym.Mid(0, min(SYM_W, uFind));

		m_Sym += Sym.Mid(uFind, SYM_S + 1);
		}
	else {
		m_Sym += Sym.Mid(0, Sym.GetLength());
		}
	}

void CImpactStrAddr::SetUserSymString(CString Str)
{
	m_Sym = "";

	CString Sym = Str;

	Sym.Remove(' ');

	Sym.Remove('-');

	UINT uLen = Sym.GetLength();

	UINT uMark = 0;

	for ( UINT u = 0; u < uLen; u++ ) {

		if( !isalpha(Sym.GetAt(u)) ) {

			m_Sym += Sym.Mid(uMark, u - uMark);

			uMark = u;
			}

		else if( u - uMark == SYM_S ) {

			m_Sym += Sym.Mid(uMark, u - uMark + 1);
			
			while( isalpha(Sym.GetAt(u) ) ) {

				u++;
				}

			uMark = u;
			}

		else if( u + 1 >= uLen ) {

			m_Sym += Sym.Mid(uMark, uLen - uMark);

			break;
			}
		}
	}

// Helper

void CImpactStrAddr::SendReplace(char cChar, CString Text)
{
	UINT uFind = m_Send.Find(cChar);

	while( uFind < NOTHING ) {

		m_Send.Delete(uFind, 1);

		m_Send.Insert(uFind, Text);

		uFind = m_Send.Find(cChar);
		}
	}

void CImpactStrAddr::SendReplaceLast(char cReplace, char cWith)
{
	UINT uFind = m_Send.Find(cReplace);

	CString Text = m_Send;

	if( uFind < NOTHING ) {

		UINT uPos = uFind;

		while( Text.Mid(uFind + 1).Find(cReplace) < NOTHING ) {

			Text = Text.Mid(uFind + 1);

			uFind = Text.Find(cReplace);

			uPos += uFind + 1;
			}

		m_Send.SetAt(uPos, cWith);
		}
	}

BOOL CImpactStrAddr::IsType(CString Str)
{
	if( Str == L"AsBool" ) {

		return TRUE;
		}

	if( Str == L"AsInt" ) {

		return TRUE;
		}

	if( Str == L"AsReal" ) {

		return TRUE;
		}

	if( Str == L"AsString" ) {

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String Address Storage Class 
//

// Dynamic Class

AfxImplementDynamicClass(CImpactStrAddrList, CItemList);

// Constructor

CImpactStrAddrList::CImpactStrAddrList(void)
{
	}

// Destructor

CImpactStrAddrList::~CImpactStrAddrList(void)
{

	}

// Item Access

CImpactStrAddr * CImpactStrAddrList::GetItem(INDEX Index) const
{
	return (CImpactStrAddr *) CItemList::GetItem(Index);
	}

CImpactStrAddr * CImpactStrAddrList::GetItem(UINT uPos) const
{
	return (CImpactStrAddr *) CItemList::GetItem(uPos);
	}

// Operations

CImpactStrAddr * CImpactStrAddrList::AppendItem(CImpactStrAddr * pStr)
{
	pStr->SetParent(this);

	if( CItemList::AppendItem(pStr) ) {

		return pStr;
		}  

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String List Meta Item
//

// Dynamic Class

AfxImplementDynamicClass(CImpactStr, CMetaItem);

// Constructor

CImpactStr::CImpactStr(void)
{
	m_pStrings = new CStringArray();
   	}

// Destructor

CImpactStr::~CImpactStr(void)
{
	delete m_pStrings;
	} 

// Download Support

BOOL CImpactStr::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);
	
	return TRUE;
	}

// Persistance

void CImpactStr::Load(CTreeFile &Tree)
{
	AddMeta();

	UINT u = 0;
	
	while( Tree.GetName() == CString(CPrintf("Str%u", u)) ) {

		CString Text = Tree.GetValueAsString();

		if( !DoesExist(Text) ) {

			m_pStrings->Append(Text);
			}
		u++;
		}
	}

void CImpactStr::Save(CTreeFile &Tree)
{
	UINT uCount = m_pStrings->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		Tree.PutValue(CPrintf("Str%u", u), m_pStrings->GetAt(u));
		}
	}

// Data Access

CStringArray * CImpactStr::GetStrings(void)
{
	return m_pStrings;
	}

// Helpers

BOOL CImpactStr::DoesExist(CString Str)
{
	UINT uCount = m_pStrings->GetCount();

	for( UINT u = 0; u < uCount; u++ ) {

		if( Str == m_pStrings->GetAt(u) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Meta Data Creation

void CImpactStr::AddMetaData(void)	
{
	CMetaItem::AddMetaData();

	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact String Storage Class 
//

// Dynamic Class

AfxImplementDynamicClass(CImpactList, CItemList);

// Constructor

CImpactList::CImpactList(void)
{
	}

// Destructor

CImpactList::~CImpactList(void)
{
	
	}

// Item Access

CImpactStr * CImpactList::GetItem(INDEX Index) const
{
	return (CImpactStr *) CItemList::GetItem(Index);
	}

CImpactStr * CImpactList::GetItem(UINT uPos) const
{
	return (CImpactStr *) CItemList::GetItem(uPos);
	}

// Operations

CImpactStr * CImpactList::AppendItem(CImpactStr * pStr)
{
	pStr->SetParent(this);

	if( CItemList::AppendItem(pStr) ) {

		return pStr;
		}  

	return NULL;
	}


//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CImpactDataDriverOptions, CUIItem);

// Constructor

CImpactDataDriverOptions::CImpactDataDriverOptions(void)
{
	
	}

// UI Managament

void CImpactDataDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {
	
		}
	}

// Download Support

BOOL CImpactDataDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CImpactDataDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CImpactDataDeviceOptions, CUIItem);

// Constructor

CImpactDataDeviceOptions::CImpactDataDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD(128, 0), MAKEWORD(168, 192)));

	m_Port   = 80;

	m_Keep   = FALSE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;	

	m_pImpactList  = new CImpactList;

	for( UINT e = 0; e <= listEnd; e++ ) {

		m_pImpactList->AppendItem( New CImpactStr );
		}

	InitLists();

	m_pAddrList = new CImpactStrAddrList;

	for( UINT t = 0; t < TYPES; t++ ) {

		m_uOffset[t] = 1;

		switch( t ) {

			case tableBit:
			case tableInt:
			case tableReal:		m_uTable[t] = t + 1;	break;
			case tableCmd:		m_uTable[t] = 0x11;	break;
			case tableStr:		m_uTable[t] = 0x20;	break;
			case tableGenErr:	m_uTable[t] = 0xED;	break;
			case tableSynErr:	m_uTable[t] = 0xEE;	break;
			case tableDataErr:	m_uTable[t] = 0xEF;	break;
			}
		}
	}

// Destructor

CImpactDataDeviceOptions::~CImpactDataDeviceOptions(void)
{
	delete m_pAddrList;

	delete m_pImpactList;
	}

// UI Loading

BOOL CImpactDataDeviceOptions::OnLoadPages(CUIPageList * pList)
{ 
	CImpactDataDeviceOptionsUIPage * pPage = New CImpactDataDeviceOptionsUIPage(this);

	pList->Append(pPage);
	
	return TRUE;			   
	}

// Persistance

void CImpactDataDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "ImpactList" ) {

			Tree.GetCollect();

			for( UINT u = listObjects; u <= listUser; u++ ) {

				CImpactStr * pStrList = m_pImpactList->GetItem(u);

				if( Tree.GetName() == "StrList" ) {

					Tree.GetCollect();

					pStrList->Load(Tree);

					Tree.EndCollect();
					}
				}

			Tree.EndCollect();
			
			continue;
			}
		
		if( Name == "AddrList" ) {

			Tree.GetCollect();

			while ( Tree.GetName() == "ImpAddr" ) {

				CImpactStrAddr * pAddr = new CImpactStrAddr;

				Tree.GetObject();

				pAddr->Load(Tree);

			 	m_pAddrList->AppendItem(pAddr);

				Tree.EndObject();
				}

			Tree.EndCollect();

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CImpactDataDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);	

	Tree.PutCollect(L"ImpactList");

	for( UINT u = listObjects; u <= listUser; u++ ) {

		CImpactStr * pStrList = m_pImpactList->GetItem(u);

		Tree.PutCollect(L"StrList");

		pStrList->Save(Tree);

		Tree.EndCollect();
		}

	Tree.EndCollect();

	Tree.PutCollect(L"AddrList");

	for( UINT n = 0; n < m_pAddrList->GetItemCount(); n++ ) {

		CImpactStrAddr * pAddr = m_pAddrList->GetItem(n);

		Tree.PutObject(L"ImpAddr");

		pAddr->Save(Tree);

		Tree.EndObject();
		}

	Tree.EndCollect();
	}

// UI Managament

void CImpactDataDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag == "Manage" ) {

			CImpactManageDialog Dlg(this);
	
			Dlg.Execute(*pWnd);
			}

		if( Tag == "Import" ) {

			OnImport(pWnd);
			}

		if( Tag == "Export" ) {

			OnExport(pWnd);
			}
		}
	}

void CImpactDataDeviceOptions::OnImport(CWnd *pWnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Impact Camera Lists"));

	Dlg.SetCaption(CPrintf("Import Lists for %s", GetDeviceName()));

	Dlg.SetFilter(TEXT("CSV Files (*.csv)|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

			CString Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
			}
		else {
			if( TRUE ) {
				
				CString Text =  TEXT("You are about to make changes to the Impact Data device configuration \n")
						TEXT("that may invalidate data tag references in the database.\n\n")
						TEXT("If you choose to continue all parameters will be replaced with \n")
						TEXT("data from the selected file and you should run the Rebuild Comms \n")
						TEXT("Blocks utility from the File menu after this operation is complete.\n\n")
						TEXT("Do you want to continue?");

				if( pWnd->YesNo(Text) == IDNO ) {

					fclose(pFile);
					
					return;
					}

				EmptyLists();
				}

			if( Import(pFile) ) {

				SetDirty();
				}

			Dlg.SaveLastPath(TEXT("Impact Camera Lists"));
			
			fclose(pFile);
			}
		}
	}

void CImpactDataDeviceOptions::OnExport(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Impact Camera Lists"));

	Dlg.SetCaption(CPrintf("Export Lists for %s", GetDeviceName()));

	Dlg.SetFilter (TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
			fclose(pFile);

			CString Text = TEXT("The selected file already exists.\n\n")
				       TEXT("Do you want to overwrite it?");

			if( pWnd->YesNo(Text) == IDNO ) {
			
				return;
				}
			}

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			CString Text = TEXT("Unable to open file for writing.");

			pWnd->Error(Text);

			return;
			}

		Export(pFile);

		fclose(pFile);

		Dlg.SaveLastPath(TEXT("Impact Camera Lists"));
		}
	}

BOOL CImpactDataDeviceOptions::Import(FILE *pFile)
{
	for( UINT l = listObjects; l <= listUser; l++ ) {

		char sLine[5120] = {0};

		if( !feof(pFile) ) {

			fgets(sLine, sizeof(sLine), pFile);

			CStringArray List;

			sLine[strlen(sLine)-1] = 0;

			CString(sLine).Tokenize(List, ',');

			UINT uCount = List.GetCount();
	
			for( UINT u = 0; u < uCount; u++ ) {

				m_pImpactList->GetItem(l)->GetStrings()->Append(List.GetAt(u));
				}
			}
		}
	
	return TRUE;
	}


void CImpactDataDeviceOptions::Export(FILE *pFile)
{
	for( UINT l = listObjects; l <= listUser; l++ ) {

		UINT uCount = m_pImpactList->GetItem(l)->GetStrings()->GetCount();

		for( UINT n = 0; n < uCount; n++ ) {
		
			CString  Name = m_pImpactList->GetItem(l)->GetStrings()->GetAt(n);
			
			fprintf(pFile, TEXT("%s,"), PCTXT(Name));
			}

		fprintf(pFile,TEXT("\n"));
		}
	}

CString CImpactDataDeviceOptions::GetDeviceName(void)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pData = pItem->FindMetaData(TEXT("Name"));

	return pData ? pData->ReadString(pItem) : TEXT("Device");
	}


// Download Support

BOOL CImpactDataDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	if( m_pAddrList ) {

		UINT uCount = m_pAddrList->GetItemCount();

		Init.AddWord(WORD(uCount));

		for( UINT u = 0; u < uCount; u++ ) {

			m_pAddrList->GetItem(u)->MakeInitData(Init);
			}
		}
	else {
		Init.AddWord(0);
		}  

	return TRUE;
	}

// Vision System List Access

CString CImpactDataDeviceOptions::GetListElement(UINT uList, UINT uElement)
{
	return m_pImpactList->GetItem(uList)->GetStrings()->GetAt(uElement);
	}

UINT CImpactDataDeviceOptions::GetListCount(UINT uList)
{
	return m_pImpactList->GetItem(uList)->GetStrings()->GetCount();
	}

BOOL CImpactDataDeviceOptions::AddListItem(UINT uList, CString Item)
{
	if( CanAdd(uList, Item) ) {

		m_pImpactList->GetItem(uList)->GetStrings()->Append(CString(Item));

		this->SetDirty();
		}
	
	return TRUE;
	}

BOOL CImpactDataDeviceOptions::DeleteListItem(UINT uList, CString Item)
{
	UINT u = 0;
	
	if( CanDelete(uList, Item, u) ) {

		BOOL fAsk  = FALSE;

		UINT uAddr = m_pAddrList->GetItemCount();

		for( UINT i = 0; i < uAddr; i++ ) {

			CImpactStrAddr * pAddr = m_pAddrList->GetItem(i);

			if( pAddr ) {

				CString Addr = pAddr->GetFullString();

				if( !Addr.IsEmpty() ) {

					UINT uFind = Addr.Find('.');

					if( uFind < NOTHING ) {

						Addr = Addr.Left(uFind);
						}

					CStringArray Sub;

					Addr.Tokenize(Sub, ':');
	
					UINT uStr = Sub.GetCount();

					for( UINT n = 0; n < uStr; n++ ) {

						if( Item == Sub[n] || Item == Addr ) {

							if( !fAsk ) {

								CWnd Wnd;

								CString Text = CString(IDS_YOU_ARE_ABOUT_TO);

								if( Wnd.YesNo(Text) == IDNO ) {

									return FALSE;
									}
								}

							m_pAddrList->RemoveItem(m_pAddrList->GetItem(i));

							fAsk = TRUE;
							}
						}
					}
				}
			}

		m_pImpactList->GetItem(uList)->GetStrings()->Remove(u);

		CSystemItem *pSystem = GetDatabase()->GetSystemItem();

		pSystem->Rebuild(1);

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactDataDeviceOptions::ResetList(UINT uList)
{
	if( CanReset(uList) ) {

		CWnd Wnd;

		CString Text = CString(IDS_YOU_ARE_ABOUT_TO_2);

		if( Wnd.YesNo(Text) == IDNO ) {

			return FALSE;
			}
	
		m_pImpactList->GetItem(uList)->GetStrings()->Empty();
	
		InitList(uList);

		CSystemItem *pSystem = GetDatabase()->GetSystemItem();

		pSystem->Rebuild(1);

		return TRUE;
		}

	return FALSE;
	}

// Address List Access

CString CImpactDataDeviceOptions::GetAddrText(DWORD dwRef)
{
	UINT uCount = m_pAddrList->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CAddress Addr = m_pAddrList->GetItem(u)->GetAddress();

		if( Addr.m_Ref == dwRef ) {

			return m_pAddrList->GetItem(u)->GetString();
			}
		}

	return TEXT("");
	}

CString CImpactDataDeviceOptions::GetAddrFullText(DWORD dwRef)
{
	UINT uCount = m_pAddrList->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CAddress Addr = m_pAddrList->GetItem(u)->GetAddress();

		if( Addr.m_Ref == dwRef ) {

			return m_pAddrList->GetItem(u)->GetFullString();
			}
		}

	return TEXT("");
	}

// Address Reference Update

BOOL CImpactDataDeviceOptions::SetAddr(CString Text, CAddress &Addr)
{	
	if( Text == m_pImpactList->GetItem(listMenu)->GetStrings()->GetAt(0) ) {

		return FALSE;
		}

	if( SetExist(Text, Addr) ) {

		return TRUE;
		}

	if( !ListExist(Text) ) {

		return FALSE;
		}	

	UINT uType = Text.Find('.');

	if( uType < NOTHING ) {

		CString Type = Text.Mid(uType + 1);

		while( !IsType(Type) ) {

			uType = Type.Find('.');

			if( uType < NOTHING ) {

				Type = Type.Mid(uType + 1);

				continue;
				}

			break;
			}

		UINT uTypes = m_pImpactList->GetItem(listTypes)->GetStrings()->GetCount();

		for( UINT u = 0; u < uTypes; u++ ) {

			if( Type == m_pImpactList->GetItem(listTypes)->GetStrings()->GetAt(u) ) {

				switch( u ) {

					case tableBit:		return SetNamed(Text, Addr, tableBit);		
					case tableInt:		return SetNamed(Text, Addr, tableInt);		
					case tableReal:		return SetNamed(Text, Addr, tableReal);			
					case tableStr - 1:	return SetString(Text, Addr);		
					}
				}
			}
		}

	if( SetLatestError(Text, Addr) ) {

		return TRUE;
		}

	UINT uCmd = tableCmd;
	
	if( SetNamed(Text, Addr, uCmd) ) {

		return TRUE;
		}

	return FALSE;
	}

// Helpers

BOOL CImpactDataDeviceOptions::IsNamed(CAddress Addr)
{
	if( Addr.a.m_Table >= 1 && Addr.a.m_Table <= 0x10 ) {

		return TRUE;
		}

	return FALSE;
	}


BOOL CImpactDataDeviceOptions::IsString(CAddress Addr)
{
	if( Addr.a.m_Table >= 0x20 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactDataDeviceOptions::IsType(CString Text)
{
	UINT uCount = GetListCount(listTypes);	

	for( UINT u = 0; u < uCount; u++ ) {
		
		if( Text == GetListElement(listTypes, u) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Meta Data Creation

void CImpactDataDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

// Implementaion

void CImpactDataDeviceOptions::InitLists(void)
{
	InitObjectList();

	InitObjPropList();

	InitProgList();

	InitTaskList();

	InitToolList();

	InitPropList();

	InitTypeList();

	InitMenuList();

	InitManageList();

	InitUserList();
	}

void CImpactDataDeviceOptions::InitList(UINT uList)
{
	if( !(m_pImpactList->GetItem(uList)->GetStrings()->IsEmpty()) ) {

		return;
		}
	
	switch( uList ) {

		case listObjects:	InitObjectList();	break;
		case listObjProps:	InitObjPropList();	break;
		case listProgs:		InitProgList();		break;
		case listTasks:		InitTaskList();		break;
		case listTools:		InitToolList();		break;
		case listProps:		InitPropList();		break;
		case listUser:		InitUserList();		break;
		}
	}

void CImpactDataDeviceOptions::EmptyLists(void)
{
	for( UINT l = listObjects; l <= listUser; l++ ) {

		m_pImpactList->GetItem(l)->GetStrings()->Empty();
		}
	}

void CImpactDataDeviceOptions::InitObjectList(void)
{
	m_pImpactList->GetItem(listObjects)->GetStrings()->Append(CString(L"System"));	
	m_pImpactList->GetItem(listObjects)->GetStrings()->Append(CString(L"Camera"));
	m_pImpactList->GetItem(listObjects)->GetStrings()->Append(CString(L"File Camera"));
	m_pImpactList->GetItem(listObjects)->GetStrings()->Append(CString(L"System Log"));
	m_pImpactList->GetItem(listObjects)->GetStrings()->Append(CString(L"File Manager"));
	}

void CImpactDataDeviceOptions::InitObjPropList(void)
{
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Name"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Description"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Type"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Device Name"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Device Comment"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Task Timeout"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Digital Shift"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Gain"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Offset"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Number of Active Images"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Log Entry"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Display Tab Program Filter"));
	m_pImpactList->GetItem(listObjProps)->GetStrings()->Append(CString(L"Available Flash"));
	}

void CImpactDataDeviceOptions::InitProgList(void)
{
	m_pImpactList->GetItem(listProgs)->GetStrings()->Append(CString(L"Inspection"));
	}


void CImpactDataDeviceOptions::InitTaskList(void)
{
	m_pImpactList->GetItem(listTasks)->GetStrings()->Append(CString(L"ImageIn Task"));
	}

void CImpactDataDeviceOptions::InitToolList(void)
{
	m_pImpactList->GetItem(listTools)->GetStrings()->Append(CString(L"Blob"));
	m_pImpactList->GetItem(listTools)->GetStrings()->Append(CString(L"Contrast"));
	m_pImpactList->GetItem(listTools)->GetStrings()->Append(CString(L"Origin"));
	}


void CImpactDataDeviceOptions::InitPropList(void)
{
	m_pImpactList->GetItem(listProps)->GetStrings()->Append(CString(L"Name"));
	m_pImpactList->GetItem(listProps)->GetStrings()->Append(CString(L"Description"));
	m_pImpactList->GetItem(listProps)->GetStrings()->Append(CString(L"Type"));
	m_pImpactList->GetItem(listProps)->GetStrings()->Append(CString(L"File Name"));
	}

void CImpactDataDeviceOptions::InitTypeList(void)
{
	m_pImpactList->GetItem(listTypes)->GetStrings()->Append(CString(L"AsBool"));
	m_pImpactList->GetItem(listTypes)->GetStrings()->Append(CString(L"AsInt"));
	m_pImpactList->GetItem(listTypes)->GetStrings()->Append(CString(L"AsReal"));
	m_pImpactList->GetItem(listTypes)->GetStrings()->Append(CString(L"AsString"));
	}


void CImpactDataDeviceOptions::InitMenuList(void)
{
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"No Selection"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Get/Set Data Command"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Run Command"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Trigger Camera"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Camera Online Command"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Camera Offline Command"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Camera IsOnline Command"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Abort Running Tasks"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"User Defined Data"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Last General Error"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Last Syntax Error"));
	m_pImpactList->GetItem(listMenu)->GetStrings()->Append(CString(L"Last Data Error"));
	}

void CImpactDataDeviceOptions::InitManageList(void)
{
	m_pImpactList->GetItem(listManage)->GetStrings()->Append(CString(L"Objects"));
	m_pImpactList->GetItem(listManage)->GetStrings()->Append(CString(L"Object Properties"));
	m_pImpactList->GetItem(listManage)->GetStrings()->Append(CString(L"Programs"));
	m_pImpactList->GetItem(listManage)->GetStrings()->Append(CString(L"Tasks"));
	m_pImpactList->GetItem(listManage)->GetStrings()->Append(CString(L"Tools"));
	m_pImpactList->GetItem(listManage)->GetStrings()->Append(CString(L"Properties"));
	m_pImpactList->GetItem(listManage)->GetStrings()->Append(CString(L"User Defined Data"));
	}

void CImpactDataDeviceOptions::InitUserList(void)
{
	m_pImpactList->GetItem(listUser)->GetStrings()->Append(CString(L"Inspection:ImageIn Task:Contrast"));
	}

BOOL CImpactDataDeviceOptions::CanAdd(UINT uList, CString Item)
{
	if( CanEdit(uList) ) {

		if( uList < listUser ) {

			UINT uFind = Item.Find('.');

			if( uFind == NOTHING ) {

				uFind = Item.Find(':');
				}

			if( uFind < NOTHING ) {

				CWnd w;

				w.Error(CString(IDS_THIS_LIST_CAN_NOT));

				return FALSE;
				}
			}

		UINT u = 0;

		if( DoesExist(uList, Item, u) ) {
			
			CWnd w;

			w.Error(CPrintf(CString(IDS_FMT_ALREADY), Item, m_pImpactList->GetItem(listManage)->GetStrings()->GetAt(uList - 1)));

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactDataDeviceOptions::CanDelete(UINT uList, CString Item, UINT& u)
{
	if( CanEdit(uList) ) {

		if( DoesExist(uList, Item, u) ) {
		
			return TRUE;
			}
		}

	CWnd w;

	w.Error(CPrintf(CString(IDS_FMT_DOES_NOT), Item, m_pImpactList->GetItem(listManage)->GetStrings()->GetAt(uList - 1)));

	return FALSE;
	}

BOOL CImpactDataDeviceOptions::CanReset(UINT uList)
{
	if( CanEdit(uList) ) {

		return TRUE;
		}

	CWnd w;

	w.Error(CPrintf(CString(IDS_FMT_LIST_IS_NOT), m_pImpactList->GetItem(listManage)->GetStrings()->GetAt(uList - 1)));

	return FALSE;
	}

BOOL CImpactDataDeviceOptions::CanEdit(UINT uList)
{
	return ( (uList >= listObjects) && (uList <= listUser) );
	}

BOOL CImpactDataDeviceOptions::DoesExist(UINT uList, CString Item, UINT& u)
{
	UINT uCount = m_pImpactList->GetItem(uList)->GetStrings()->GetCount();

	for( u = 0; u < uCount; u++ ) {

		CString Text = m_pImpactList->GetItem(uList)->GetStrings()->GetAt(u);

		if( Text == Item ) {

			return TRUE;
			}
		}	

	return FALSE;
	}

BOOL CImpactDataDeviceOptions::SetExist(CString Item, CAddress &Addr)
{
	UINT uCount = m_pAddrList->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		CImpactStrAddr * pAddr = (CImpactStrAddr *) m_pAddrList->GetItem(u);

		if( pAddr ) {

			if( Item == pAddr->GetString() || Item == pAddr->GetFullString() ) {

				Addr.m_Ref = pAddr->GetAddress().m_Ref;

				return TRUE;
				}
			}
		}	

	return FALSE;
	}

BOOL CImpactDataDeviceOptions::ListExist(CString Item)
{
	CString User = Item;

	UINT uPos = User.Find('.');

	if( uPos < NOTHING ) {

		UINT uFind =  User.Find('.');

		uPos = 0;

		while( uFind < NOTHING ) {

			uPos += uFind + 1;

			User = User.Mid(uFind + 1);

			uFind =  User.Find('.');
			}

		User = Item.Mid(0, uPos - 1);
		}

	uPos = Item.Find(':');

	if( uPos == NOTHING ) {

		uPos = Item.Find('.');
		}

	BOOL fFound = TRUE;	

	while ( fFound && uPos < NOTHING ) {

		fFound = FALSE;

		CString Text = Item.Mid(0, uPos);

		Item = Item.Mid(uPos +  1);

		UINT uCount = m_pImpactList->GetItemCount(); 

		for( UINT u = 0; u < uCount; u++ ) {

			CImpactStr * pStr = (CImpactStr *) m_pImpactList->GetItem(u);

			if( pStr ) {
			
				UINT uStrs =  pStr->GetStrings()->GetCount();

				for( UINT n = 0; n < uStrs; n++ ) {

					if( Text == pStr->GetStrings()->GetAt(n) ) {

						fFound = TRUE;

						break;
						}

					else if( u == listUser ) {

						if( User == pStr->GetStrings()->GetAt(n) ) {

							return TRUE;
							}
						}
					}

				if( fFound ) {

					break;
					}
				}
			}

		uPos = Item.Find(':');

		if( uPos == NOTHING ) {

			uPos = Item.Find('.');
			}
		}	

	return fFound;
	}

BOOL CImpactDataDeviceOptions::SetNamed(CString Text, CAddress &Addr, UINT uTable)
{
	Addr.m_Ref = GetNextNamed(uTable); 

	m_pAddrList->AppendItem(new CImpactStrAddr(Text, Addr, FindType(Text)));

	return TRUE;
	}

BOOL CImpactDataDeviceOptions::SetString(CString Text, CAddress &Addr)
{
	Addr.m_Ref = GetNextString();	

	m_pAddrList->AppendItem(new CImpactStrAddr(Text, Addr, FindType(Text)));

	return TRUE;
	}

BOOL CImpactDataDeviceOptions::SetLatestError(CString Text, CAddress &Addr)
{
	Addr.m_Ref = 0;

	if( Text == m_pImpactList->GetItem(listMenu)->GetStrings()->GetAt(menuGenErr) ) {

		Addr.a.m_Table = 0xED;

		Addr.a.m_Type  = addrLongAsLong;

		m_pAddrList->AppendItem(new CImpactStrAddr(Text, Addr, tGen));

		return TRUE;
		}

	if( Text == m_pImpactList->GetItem(listMenu)->GetStrings()->GetAt(menuSynErr) ) {

		Addr.a.m_Table = 0xEE;

		Addr.a.m_Type  = addrLongAsLong;

		m_pAddrList->AppendItem(new CImpactStrAddr(Text, Addr, tSyn));

		return TRUE;
		}	

	if( Text == m_pImpactList->GetItem(listMenu)->GetStrings()->GetAt(menuDataErr) ) {

		Addr.a.m_Table = 0xEF;

		Addr.a.m_Type  = addrLongAsLong;

		m_pAddrList->AppendItem(new CImpactStrAddr(Text, Addr, tData));

		return TRUE;
		}

	return FALSE;
	}

BYTE CImpactDataDeviceOptions::FindType(CString Text)
{
	UINT uCount  = GetListCount(listUser);

	CString User = Text;

	UINT uFind = User.Find('.');

	UINT uPos  = 0;

	while( uFind < NOTHING ) {

		User = User.Mid(uFind + 1);

		uPos += (uFind + 1);

		uFind = User.Find('.');
		}

	User = Text.Left(uPos - 1);

	for( UINT i = 0; i < uCount; i++ ) {

		if( User == GetListElement(listUser, i) ) {

			return tUser;
			}
		}

	CString Part = Text;

	uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		Part = Text.Left(uFind);

		Text = Text.Mid(uFind + 1);
		}

	BOOL fRun = FALSE;

	for( UINT l = listObjects; l <= listMenu; l++ ) {

		uCount = GetListCount(l);

		if( fRun ) {

			uFind = Text.Find(':');

			if( uFind < NOTHING ) {

				Part = Text.Left(uFind);

				Text = Text.Mid(uFind + 1);
				}
			else {
				uFind = Text.Find('.');

				if( uFind < NOTHING ) {

					Part = Text.Left(uFind);

					Text = Text.Mid(uFind + 1);
					}
				}
			}

		for( UINT u = 0; u < uCount; u++ ) {

			CString Str = GetListElement(l, u);

			if( Part == Str || User == Str ) {

				switch( l ) {

					case listObjects: 

						return tObject;
						
					case listProgs:

						fRun = TRUE;
					
						continue;

					case listMenu:

						switch( u ) {

							case 3:	return tTrig;
							case 4: return tOnline;
							case 5: return tOffline;
							case 6: return tIsOnline;
							case 7: return tAbort;
							}

						break;
												
					case listProps:

						return tTask; 

					default:
						continue;
					}

				return BYTE(fRun ? tRun : tTask);
				}			
			}
		}

	return BYTE(fRun ? tRun : tTask);
	}

BOOL  CImpactDataDeviceOptions::IsUsed(DWORD dwRef)
{
	UINT uList = m_pAddrList->GetItemCount();

	for( UINT u = 0; u < uList; u++ ) {

		if( dwRef == m_pAddrList->GetItem(u)->GetAddress().m_Ref ) {

			return TRUE;
			}
		}

	return FALSE;
	}

DWORD CImpactDataDeviceOptions::GetNextNamed(UINT uTable)
{
	CAddress Addr;

	Addr.m_Ref = 0;

	while( (m_uTable[uTable] < m_uTable[tableCmd] - 3) || (uTable == tableCmd) || (uTable == tableGenErr) || (uTable == tableSynErr) || (uTable == tableDataErr) ) {

		if( m_uOffset[uTable] == 0 ) {

			if( m_uTable[uTable] < tableCmd - 3 ) {

				m_uTable[uTable] += 3;

				m_uOffset[uTable]++;
				}
			}

		switch( uTable ) {

			case tableBit:	Addr.a.m_Type = addrBitAsBit;	break;
			case tableInt:	Addr.a.m_Type = addrLongAsLong;	break;
			case tableReal:	Addr.a.m_Type = addrRealAsReal;	break;
			case tableCmd:	Addr.a.m_Type = addrBitAsBit;	break;
			}

		Addr.a.m_Table	= m_uTable[uTable];

		Addr.a.m_Offset = m_uOffset[uTable];

		m_uOffset[uTable]++;

		if( !IsUsed(Addr.m_Ref) ) {

			return Addr.m_Ref;
			}
		}

	return 0;
	}


DWORD CImpactDataDeviceOptions::GetNextString(void)
{
	CAddress Addr;

	Addr.m_Ref = 0;

	while( m_uTable[tableStr] != 0xFF ) {

		if( m_uOffset[tableStr] == 0 ) {

			if( m_uTable[tableStr] < 0xEE - 1) {

				m_uTable[tableStr]++;

				m_uOffset[tableStr] += 17;
				}
			}

		Addr.a.m_Table	= m_uTable[tableStr];

		Addr.a.m_Offset = m_uOffset[tableStr];

		Addr.a.m_Type   = addrLongAsLong;

		// Support up to 67 chars !!

		m_uOffset[tableStr] += 17;	

		if( !IsUsed(Addr.m_Ref) ) {

			return Addr.m_Ref;
			}
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Device Options UI Page
//


// Runtime Class

AfxImplementRuntimeClass(CImpactDataDeviceOptionsUIPage, CUIPage);

// Constructor

CImpactDataDeviceOptionsUIPage::CImpactDataDeviceOptionsUIPage(CImpactDataDeviceOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CImpactDataDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Target Device", 1);

	pView->AddUI(pItem, L"root", L"IP");

	pView->EndGroup(TRUE);

	pView->StartGroup(L"Protocol Options", 1);

	pView->AddUI(pItem, L"root", L"Keep");

	pView->AddUI(pItem, L"root", L"Ping");

	pView->AddUI(pItem, L"root", L"Time1");

	pView->AddUI(pItem, L"root", L"Time3");

	pView->AddUI(pItem, L"root", L"Time2");

	pView->EndGroup(TRUE);

	pView->StartGroup(L"Impact Camera Lists", 3);

	pView->AddButton(L"Manage", L"Manage");

	pView->AddButton(L"Export", L"Export");

	pView->AddButton(L"Import", L"Import");

	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Driver
//

// Instantiator

ICommsDriver *	Create_ImpactDataDriver(void)
{
	return New CImpactDataDriver;
	}

// Constructor

CImpactDataDriver::CImpactDataDriver(void)
{
	m_wID		= 0x40A5;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "PPT Vision";
	
	m_DriverName	= "IMPACT Inspection Builder HTTP Master";
	
	m_Version	= "1.00";

	m_DevRoot	= "Data";
	
	m_ShortName	= "IMPACT";
	}

// Configuration

CLASS CImpactDataDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CImpactDataDriverOptions);
	}

CLASS CImpactDataDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CImpactDataDeviceOptions);
	}

// Binding Control

UINT CImpactDataDriver::GetBinding(void)
{
	return bindEthernet;
	}

// Address Management

BOOL CImpactDataDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CImpactDataDialog Dlg(*this, Addr, pConfig, fPart);  
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CImpactDataDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

BOOL CImpactDataDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}

// Address Helpers

BOOL CImpactDataDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CImpactDataDeviceOptions * pOpt = (CImpactDataDeviceOptions *) pConfig;
	
	if( pOpt ) {

		if( pOpt->SetAddr(Text, Addr) ) {

			return TRUE;
			}
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CImpactDataDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CImpactDataDeviceOptions * pOpt = (CImpactDataDeviceOptions *) pConfig;

	if( pOpt ) {

		Text = pOpt->GetAddrText(DWORD(Addr.m_Ref));

		if( !Text.IsEmpty() ) {

			return TRUE;
			}
		}

	return FALSE;	
	}

////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Data Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CImpactDataDialog, CStdDialog);
		
// Constructor

CImpactDataDialog::CImpactDataDialog(CImpactDataDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
{
	m_pDriver = &Driver;
	
	m_pAddr    = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;

	m_Text    = "";
	
	SetName(L"ImpactDlg");
 	}

// Vision System List Access

CString CImpactDataDialog::GetListElement(UINT uList, UINT uElement)
{
	CString Element;
	
	CImpactDataDeviceOptions * pOpt = (CImpactDataDeviceOptions *) m_pConfig;

	if( pOpt ) {

		Element = pOpt->GetListElement(uList, uElement);
		}

	return Element;
	}

UINT CImpactDataDialog::GetListCount(UINT uList)
{
	UINT uCount = 0;

	CImpactDataDeviceOptions * pOpt = (CImpactDataDeviceOptions *) m_pConfig;

	if( pOpt ) {

		uCount = pOpt->GetListCount(uList);
		}

	return uCount;
	}

// Command Access

void CImpactDataDialog::SetCommand(CString Cmd)
{
	SetSelectText(Cmd);
	}

void CImpactDataDialog::SetType(CString Type)
{
	CString Text = GetDlgItem(2002).GetWindowTextW();

	CString User = Text;

	UINT uFind = User.Find('.');

	UINT uPos  = 0;

	while( uFind < NOTHING ) {

		User = User.Mid(uFind + 1);

		uPos += (uFind + 1);

		uFind = User.Find('.');
		}

	User = Text.Mid(uPos);

	//if( IsUser(Text) ) {

		if( IsType(User) ) {

			Text = Text.Left(uPos - 1);
			}
	//	}
	
	else if( uFind < NOTHING ) {

		Text = Text.Left(uFind);
		}

	Text = Text + "." + Type;

	SetSelectText(Text);
	}

// Selection Access

CString CImpactDataDialog::GetSelectedText(void)
{
	CString Text = GetDlgItem(2002).GetWindowTextW();

	if( Text != GetListElement(listMenu, 0) ) {

		return Text;
		}

	return m_Text;
	}

// Message Map

AfxMessageMap(CImpactDataDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange) 

	AfxDispatchCommand(2001, OnSelectProperty)
	AfxDispatchCommand(2003, OnSelectType)
	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CImpactDataDialog)
	};

// Message Handlers

BOOL CImpactDataDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadList(0);

	CImpactDataDeviceOptions * pOpt = (CImpactDataDeviceOptions *) m_pConfig;

	if( pOpt ) {

		CString Text = pOpt->GetAddrFullText(m_pAddr->m_Ref);

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		if( IsTrig(Text) ) {

			Box.SetCurSel(menuTrigger);
			}
		
		else if( IsSetOnline(Text) ) {

			Box.SetCurSel(menuOnline);
			}
		
		else if( IsSetOffline(Text) ) {

			Box.SetCurSel(menuOffline);
			}

		else if( IsIsOnline(Text) ) {

			Box.SetCurSel(menuIsOnline);
			}

		else if( IsAbort(Text) ) {

			Box.SetCurSel(menuAbort);
			}		

		else if( IsSynErr(Text) ) {

			Box.SetCurSel(menuSynErr);
			}

		else if( IsDataErr(Text) ) {

			Box.SetCurSel(menuDataErr);
			}
		
		else if( IsGenErr(Text) ) {

			Box.SetCurSel(menuGenErr);
			}

		else if( IsUser(Text) ) {

			Box.SetCurSel(menuUser);
			}

		else if( IsData(Text) ) {

			Box.SetCurSel(menuData);
			}

		else if( IsNone(Text) ) {

			Box.SetCurSel(menuNone);
			}
		else {
			Box.SetCurSel(menuRun);
			}

		SetSelectText(Text);
		
		m_Text = Text;
		}

	DoEnables();

	GetDlgItem(IDHELP).EnableWindow(FALSE);

	return TRUE;
	}

// Notification Handlers

void CImpactDataDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 1001 ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		if( Box.GetCurSel() == menuUser ) {

			CImpactUserDialog Dlg(this);  
	
			Dlg.Execute();

			return;
			}

		DoEnables();
		}
	}

// Command Handlers

BOOL CImpactDataDialog::OnSelectProperty(UINT uID)
{
	if( uID == 2001 ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		BOOL fRun = (Box.GetCurSel() == menuRun);

		CImpactDataDeviceOptions * pOpt = (CImpactDataDeviceOptions *) m_pConfig;

		if( pOpt ) {

			CString Prop;

			CImpactPropDialog Dlg(this, fRun, Prop);  
	
			return Dlg.Execute();
			}
		}

	return TRUE;
	}

BOOL CImpactDataDialog::OnSelectType(UINT uID)
{
	if( uID == 2003 ) {

		CImpactTypeDialog Dlg(this);  
	
		return Dlg.Execute();
		}

	return TRUE;
	}

BOOL CImpactDataDialog::OnOkay(UINT uID)
{
	if( uID == IDOK ) {

		CError Error;

		CAddress Addr;

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, GetDlgItem(2002).GetWindowTextW()) ) {

			m_pAddr->m_Ref = Addr.m_Ref;

			EndDialog(TRUE);

			return TRUE;
			}

		m_pAddr->m_Ref  = 0;
	
		EndDialog(TRUE);
		}

	return TRUE;				    
	}

// Implementation

void CImpactDataDialog::LoadList(UINT uSel)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uCount = GetListCount(listMenu);

	for( UINT u = 0; u < uCount; u++ ) {

		Box.AddString(GetListElement(listMenu, u));
		};

	if( uSel < uCount ) {

		Box.SetCurSel(uSel);
		}
	else 
		Box.SetCurSel(0);
	}

void CImpactDataDialog::DoEnables(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uSel = Box.GetCurSel();

	CString Text = GetDlgItem(2002).GetWindowTextW();

	BOOL fEnable = FALSE;

	BOOL fOK = FALSE;

	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		fEnable = TRUE;
		}

	uFind = Text.Find('.');
	
	if( uFind < NOTHING ) {

		fOK = TRUE;
		}

	switch( uSel ) {

		case menuNone:

			GetDlgItem(2002).SetWindowTextW(GetListElement(listMenu, uSel));

			GetDlgItem(2001).EnableWindow(FALSE);
			GetDlgItem(2003).EnableWindow(FALSE);
			GetDlgItem(IDOK).EnableWindow(TRUE);

			break;

		case menuData:

			GetDlgItem(2001).EnableWindow(TRUE);
			GetDlgItem(2003).EnableWindow(fEnable);
			GetDlgItem(IDOK).EnableWindow(fOK);
			break;

		case menuRun:

			GetDlgItem(2001).EnableWindow(TRUE);
			GetDlgItem(2003).EnableWindow(FALSE);
			GetDlgItem(IDOK).EnableWindow(Text.Mid(1, Text.GetLength() - 2) != GetListElement(listMenu, 0));
			break;

		case menuTrigger:
		case menuOnline:
		case menuOffline:
		case menuIsOnline:
		case menuAbort:

			if( !fOK ) {

				GetDlgItem(2002).SetWindowTextW(GetListElement(listMenu, uSel));
				}
									
			GetDlgItem(2001).EnableWindow(FALSE);
			GetDlgItem(2003).EnableWindow(TRUE);
			GetDlgItem(IDOK).EnableWindow(fOK);
			break;

		

		case menuUser:

			GetDlgItem(2001).EnableWindow(FALSE);
			GetDlgItem(2003).EnableWindow(TRUE);
			GetDlgItem(IDOK).EnableWindow(fOK);
			break;

		case menuGenErr:
		case menuSynErr:
		case menuDataErr:

			GetDlgItem(2002).SetWindowTextW(GetListElement(listMenu, uSel));
			
			GetDlgItem(2001).EnableWindow(FALSE);
			GetDlgItem(2003).EnableWindow(FALSE);
			GetDlgItem(IDOK).EnableWindow(TRUE);
			break;
		}
	}

void CImpactDataDialog::SetSelectText(CString Text)
{
	if( !Text.IsEmpty() ) {

		GetDlgItem(2002).SetWindowTextW(Text);
		}

	DoEnables();
	}

// Helpers

BOOL CImpactDataDialog::IsData(CString Text)
{
	UINT uFind = Text.Find('.');

	return ( uFind < NOTHING );
	}

BOOL CImpactDataDialog::IsTrig(CString Text)
{	
	UINT uFind = Text.Find('.');

	if( uFind < NOTHING ) {

		Text = Text.Left(uFind);
		}

	return ( Text == GetListElement(listMenu, menuTrigger) );
	}

BOOL CImpactDataDialog::IsSetOnline(CString Text)
{	
	UINT uFind = Text.Find('.');

	if( uFind < NOTHING ) {

		Text = Text.Left(uFind);
		}

	return ( Text == GetListElement(listMenu, menuOnline) );
	}

BOOL CImpactDataDialog::IsSetOffline(CString Text)
{	
	UINT uFind = Text.Find('.');

	if( uFind < NOTHING ) {

		Text = Text.Left(uFind);
		}

	return ( Text == GetListElement(listMenu, menuOffline) );
	}

BOOL CImpactDataDialog::IsIsOnline(CString Text)
{	
	UINT uFind = Text.Find('.');

	if( uFind < NOTHING ) {

		Text = Text.Left(uFind);
		}

	return ( Text == GetListElement(listMenu, menuIsOnline) );
	}

BOOL CImpactDataDialog::IsAbort(CString Text)
{	
	UINT uFind = Text.Find('.');

	if( uFind < NOTHING ) {

		Text = Text.Left(uFind);
		}

	return ( Text == GetListElement(listMenu, menuAbort) );
	}

BOOL CImpactDataDialog::IsUser(CString Text)
{
	CString User = Text;

	UINT uFind = User.Find('.');

	UINT uPos  = 0;

	while( uFind < NOTHING ) {

		User = User.Mid(uFind + 1);

		uPos += (uFind + 1);

		uFind = User.Find('.');
		}

	if( IsType(User) ) {

		UINT uCount = GetListCount(listUser);

		User = Text.Left(uPos - 1);

		for( UINT u = 0; u < uCount; u++ ) {
			
			if( GetListElement(listUser, u) == User ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL  CImpactDataDialog::IsGenErr(CString Text)
{
	return ( Text == GetListElement(listMenu, menuGenErr) );	
	}

BOOL CImpactDataDialog::IsSynErr(CString Text)
{
	return ( Text == GetListElement(listMenu, menuSynErr) );
	}

BOOL CImpactDataDialog::IsDataErr(CString Text)
{
	return ( Text == GetListElement(listMenu, menuDataErr) );
	}

BOOL CImpactDataDialog::IsNone(CString Text)
{
	return ( Text.IsEmpty() || Text == GetListElement(listMenu, menuNone) );
	}

BOOL CImpactDataDialog::IsType(CString Text)
{
	CImpactDataDeviceOptions * pOpt = (CImpactDataDeviceOptions *) m_pConfig;

	if( pOpt ) {

		return pOpt->IsType(Text);
		}

	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Property Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CImpactPropDialog, CStdDialog);
		
// Constructor

CImpactPropDialog::CImpactPropDialog(CImpactDataDialog * Dlg, BOOL fRun, CString Prop) 
{
	m_pDlg = Dlg;

	m_fRun = fRun;

	m_Prop = Prop;
	
	SetName(L"ImpactPropDlg");
 	}

// Message Map

AfxMessageMap(CImpactPropDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(3002, CBN_SELCHANGE, OnSelChange) 
	AfxDispatchNotify(3004, CBN_SELCHANGE, OnSelChange) 
	AfxDispatchNotify(3006, CBN_SELCHANGE, OnSelChange) 
	AfxDispatchNotify(3008, CBN_SELCHANGE, OnSelChange) 
	AfxDispatchNotify(3010, CBN_SELCHANGE, OnSelChange) 

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CImpactPropDialog)
	};

// Message Handlers

BOOL CImpactPropDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadLists();

	SetSelection();

	DoEnables();

	return TRUE;
	}

// Notification Handlers

void CImpactPropDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 3002 ) {

		ReloadLists();
		}	

	if( uID >= 3004 && uID <= 3010 && (uID % 2 == 0) ) {

		DoEnables();
		}
	}

// Command Handlers

BOOL CImpactPropDialog::OnOkay(UINT uID)
{
	CString Cmd;
	
	for( UINT u = 3004; u <= 3010; u += 2 ) {

		CComboBox &Box = (CComboBox &) GetDlgItem(u);

		CString Part  = Box.GetWindowTextW();

		if( Part != m_pDlg->GetListElement(listMenu, 0) ) {

			if( !Cmd.IsEmpty() ) {

				Cmd += ":";
				}

			Cmd += Box.GetWindowTextW();
			}
		}

	m_pDlg->SetCommand(Cmd);

	EndDialog(TRUE);

	return TRUE;				    
	}

// Implementation

void CImpactPropDialog::LoadLists(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(3002);

	if( !m_fRun ) {

		CStringArray List;

		List.Append(L"Vision System Object");

		List.Append(L"Task");

		Box.AddStrings(List);

		Box.SetCurSel(0);
		}

	Box.EnableWindow(!m_fRun);

	for( UINT uID = 3004; uID <= 3010; uID += 2 )  {

		LoadList(uID);
		}

	DoEnables();
	}

void CImpactPropDialog::ReloadLists(void)
{	
	for( UINT uID = 3004; uID <= 3010; uID += 2 )  {

		CComboBox &Box = (CComboBox &) GetDlgItem(uID);

		Box.ResetContent();

		LoadList(uID);
		}

	DoEnables();
	}
	

void CImpactPropDialog::SetSelection(void)
{
	CString Sel = m_pDlg->GetSelectedText();

	if( !Sel.IsEmpty() ) {

		UINT uFind = Sel.Find('.');

		if( uFind == NOTHING ) {

			Sel = Sel + ":";
			}

		uFind = Sel.Find(':');

		UINT uList = listObjects;

		BOOL fFind = FALSE;

		UINT uBox  = 3004;

		CComboBox & By = (CComboBox &) GetDlgItem(3002);
		
		By.SetCurSel(0);

		while ( uFind < NOTHING ) {

			CString Text = Sel.Left(uFind);

			for( UINT u = uList; u <= listProps; u++, uList++ ) {

				UINT uLists = m_pDlg->GetListCount(u);

				switch( u ) {

				case listObjects:
					
					uBox = 3004;
					break;

				case listObjProps:
					
					uBox = 3010;
					break;

				case listProgs:
					
					By.SetCurSel(1);

					ReloadLists();

					uBox = 3004;
					break;

				case listTasks:
					
					uBox = 3006;
					break;

				case listTools:
					
					uBox = 3008;
					break;

				case listProps:
					
					uBox = 3010;
					break;
					}
				
				for( UINT n = 0; n < uLists; n++ ) {

					if( Text == m_pDlg->GetListElement(u, n) ) {

						CComboBox & Box = (CComboBox &)GetDlgItem(uBox);

						if( Box.IsEnabled() ) {

							Box.SetCurSel(n + 1);
							}

						Sel = Sel.Mid(uFind + 1);

						uFind = Sel.Find(':');

						fFind = TRUE;

						if( uFind == NOTHING ) {

							uFind = Sel.Find('.');

							uList = (uList == listObjects) ? listObjProps - 1 : listProps - 1;
							}
						
						break;
						}
					}

				if( fFind ) {

					fFind = FALSE;

					uList++;

					break;
					}

				if( u == listProps ) {

					uFind = NOTHING;
					}
				}
			}
		}
	}

void CImpactPropDialog::DoEnables(void)
{	
	CComboBox &Src = (CComboBox &) GetDlgItem(3002);

	BOOL fEnable = m_fRun || Src.GetCurSel();

	GetDlgItem(3006).EnableWindow(fEnable);	

	GetDlgItem(3008).EnableWindow(fEnable);	

	GetDlgItem(3010).EnableWindow(!m_fRun);

	GetDlgItem(IDHELP).EnableWindow(FALSE);

	GetDlgItem(IDOK).EnableWindow(IsComplete());
	}


void CImpactPropDialog::LoadList(UINT uID)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(uID); 

	UINT uList = FindList(uID);

	UINT uCount = m_pDlg->GetListCount(uList);

	Box.AddString(m_pDlg->GetListElement(listMenu, 0));

	for( UINT u = 0; u < uCount; u++  ) {

		Box.AddString(m_pDlg->GetListElement(uList, u));
		}

	Box.SetCurSel(0);
	}

UINT CImpactPropDialog::FindList(UINT uID) 
{
	CComboBox &Src = (CComboBox &) GetDlgItem(3002);

	UINT uList = listNone;

	UINT uSel = Src.GetCurSel();

	switch( uID ) {

		case 3004:
			uList = uSel ? listProgs : listObjects;
			break;

		case 3006:
			uList = uSel ? listTasks : listNone;
			break;

		case 3008:
			uList = uSel ? listTools : listNone;
			break;

		case 3010:
			uList = uSel ? listProps : listObjProps;
			break;
		}

	return uList;
	}

BOOL CImpactPropDialog::IsComplete(void)
{
	for( UINT uID = 3004; uID <= 3010; uID += 2 )  {

		if( uID == 3006 || uID == 3008 ) {

			continue;
			}

		CComboBox &Box = (CComboBox &) GetDlgItem(uID);

		if( m_fRun && uID == 3004 && Box.GetCurSel() ) {

			return TRUE;
			}

		if( !Box.GetCurSel() ) {

			return FALSE;
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Type Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CImpactTypeDialog, CStdDialog);
		
// Constructor

CImpactTypeDialog::CImpactTypeDialog(CImpactDataDialog * Dlg) 
{
	m_pDlg = Dlg;

	SetName(L"ImpactTypeDlg");
 	}

// Message Map

AfxMessageMap(CImpactTypeDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(2005, LBN_SELCHANGE, OnSelChange) 

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CImpactTypeDialog)
	};

// Message Handlers

BOOL CImpactTypeDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadList();

	SetSelection();	

	return TRUE;
	}

// Notification Handlers

void CImpactTypeDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	}

// Command Handlers

BOOL CImpactTypeDialog::OnOkay(UINT uID)
{
	CListBox &Box = (CListBox &) GetDlgItem(2005);

	m_pDlg->SetType(m_pDlg->GetListElement(listTypes, Box.GetCurSel()));
	
	EndDialog(TRUE);

	return TRUE;				    
	}

// Implementations

void CImpactTypeDialog::LoadList(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(2005);

	UINT uCount = m_pDlg->GetListCount(listTypes);

	for( UINT u = 0; u < uCount; u++ ) {

		Box.AddString(m_pDlg->GetListElement(listTypes, u));
		}

	Box.SetCurSel(1);
	}

void CImpactTypeDialog::SetSelection(void)
{
	CString Sel = m_pDlg->GetSelectedText();

	if( !Sel.IsEmpty() ) {

		CString Type = Sel;

		UINT uFind = Type.Find('.');

		UINT uPos  = 0;

		while( uFind < NOTHING ) {

			Type = Type.Mid(uFind + 1);

			uPos += (uFind + 1);

			uFind = Type.Find('.');
			}

		Type = Sel.Mid(uPos);

		UINT uCount = m_pDlg->GetListCount(listTypes);

		for( UINT n = 0; n < uCount; n++ ) {

			if( m_pDlg->GetListElement(listTypes, n) ==  Type ) {

				CListBox &Box = (CListBox &) GetDlgItem(2005);

				Box.SetCurSel(n);
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Type Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CImpactUserDialog, CStdDialog);
		
// Constructor

CImpactUserDialog::CImpactUserDialog(CImpactDataDialog * Dlg) 
{
	m_pDlg = Dlg;

	SetName(L"ImpactUserDlg");
 	}

// Message Map

AfxMessageMap(CImpactUserDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(2007, LBN_SELCHANGE, OnSelChange) 

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CImpactUserDialog)
	};

// Message Handlers

BOOL CImpactUserDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadList();

	SetSelection();	

	return TRUE;
	}

// Notification Handlers

void CImpactUserDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	
	}

// Command Handlers

BOOL CImpactUserDialog::OnOkay(UINT uID)
{
	CListBox &Box = (CListBox &) GetDlgItem(2007);

	m_pDlg->SetCommand(m_pDlg->GetListElement(listUser, Box.GetCurSel()));
	
	EndDialog(TRUE);

	return TRUE;				    
	}

// Implementations

void CImpactUserDialog::LoadList(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(2007);

	UINT uCount = m_pDlg->GetListCount(listUser);

	for( UINT u = 0; u < uCount; u++ ) {

		Box.AddString(m_pDlg->GetListElement(listUser, u));
		}

	Box.SetCurSel(0);
	}

void CImpactUserDialog::SetSelection(void)
{
	CString Sel = m_pDlg->GetSelectedText();

	if( !Sel.IsEmpty() ) {

		CString User = Sel;

		UINT uPos = User.Find('.');

		if( uPos < NOTHING ) {

			UINT uFind =  User.Find('.');

			uPos = 0;

			while( uFind < NOTHING ) {

				uPos += uFind + 1;
	
				User = User.Mid(uFind + 1);

				uFind =  User.Find('.');
				}

			User = Sel.Mid(0, uPos - 1);
			}

		UINT uCount = m_pDlg->GetListCount(listUser);

		for( UINT n = 0; n < uCount; n++ ) {

			if( m_pDlg->GetListElement(listUser, n) ==  User ) {

				CListBox &Box = (CListBox &) GetDlgItem(2007);

				Box.SetCurSel(n);

				break;
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// PPT Vision Impact Management Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CImpactManageDialog, CStdDialog);
		
// Constructor

CImpactManageDialog::CImpactManageDialog(CImpactDataDeviceOptions * Device) 
{
	m_pDevice = Device;

	SetName(L"ImpactManageDlg");
 	}

// Message Map

AfxMessageMap(CImpactManageDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange) 

	AfxDispatchCommand(2001, OnAdd)
	AfxDispatchCommand(2002, OnDelete)
	AfxDispatchCommand(2003, OnDefault)
	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CImpactManageDialog)
	};

// Message Handlers

BOOL CImpactManageDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadLeftList();

	OnSelChange(1001, Focus);

	return TRUE;
	}

// Notification Handlers

void CImpactManageDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 1001 ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		LoadRightList(Box.GetCurSel());
		}
	}

// Command Handlers

BOOL CImpactManageDialog::OnAdd(UINT uID)
{
	if( uID == 2001 ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		UINT uList = Box.GetCurSel() + 1;

		CString Title   = "Add to " + Box.GetText(uList - 1) + " List";
			
		CStringDialog Add(Title, CString(""), CString(""));
		
		if( Add.Execute(ThisObject) ) {

			CString Param = Add.GetData();	

			m_pDevice->AddListItem(uList, Param);

			LoadRightList(Box.GetCurSel());
			}
		}

	return TRUE;
	}

BOOL CImpactManageDialog::OnDelete(UINT uID)
{
	if( uID == 2002 ) {

		CListBox &List = (CListBox &) GetDlgItem(1001);

		UINT uList = List.GetCurSel() + 1;

		CListBox &Item = (CListBox &) GetDlgItem(1002);

		UINT uItem = Item.GetCurSel();

		m_pDevice->DeleteListItem(uList, Item.GetText(uItem));

		LoadRightList(List.GetCurSel());
		}


	return TRUE;
	}

BOOL CImpactManageDialog::OnDefault(UINT uID)
{
	if( uID == 2003 ) {

		CListBox &List = (CListBox &) GetDlgItem(1001);

		UINT uList = List.GetCurSel() + 1;

		m_pDevice->ResetList(uList);

		LoadRightList(List.GetCurSel());
		}

	return TRUE;
	}

BOOL CImpactManageDialog::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;				    
	}

// Implementation

void CImpactManageDialog::LoadLeftList(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uCount = m_pDevice->GetListCount(listManage);

	for( UINT u = 0; u < uCount; u++ ) {

		Box.AddString(m_pDevice->GetListElement(listManage, u));
		}

	Box.SetCurSel(0);
	}

void CImpactManageDialog::LoadRightList(UINT uSel)
{
	CListBox &Box = (CListBox &) GetDlgItem(1002);

	Box.ResetContent();

	UINT uList = uSel + 1;

	UINT uCount = m_pDevice->GetListCount(uList);

	for( UINT u = 0; u < uCount; u++ ) {

		Box.AddString(m_pDevice->GetListElement(uList, u));
		}

	Box.SetCurSel(0);
	}

// End of File
