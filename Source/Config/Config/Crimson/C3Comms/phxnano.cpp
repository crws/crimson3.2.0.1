
#include "intern.hpp"

#include "phxnano.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Phoenix Nano LC Driver
//

// Instantiator

ICommsDriver *	Create_PhoenixNanoLCDriver(void)
{
	return New CPhoenixNanoLCDriver;
	}

// Constructor

CPhoenixNanoLCDriver::CPhoenixNanoLCDriver(void)
{
	m_wID		= 0x4052;

	m_uType		= driverMaster;

	m_Manufacturer	= "Phoenix Contact";

	m_DriverName	= "nanoLC";

	m_Version	= "1.00";

	m_ShortName	= "Phoenix nanoLC";

	m_DevRoot	= "nanoLC";

	AddSpaces();  
	}

// Binding Control

UINT CPhoenixNanoLCDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CPhoenixNanoLCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CPhoenixNanoLCDriver::AddSpaces(void)
{
	AddSpace(New CSpace( 1, "R",	"Holding Registers",		10, 0, 32767,	addrLongAsLong));

	AddSpace(New CSpace( 3, "Q",	"Digital Outputs",		10, 0,  4095,	addrBitAsBit));

	AddSpace(New CSpace( 4, "I",	"Digital Inputs",		10, 0,  4095,	addrBitAsBit));

	AddSpace(New CSpace( 5, "F",	"Flags",			10, 0,  4095,	addrBitAsBit));

	AddSpace(New CSpace( 7, "TCA",	"Timer/Counter Accumulated",	10, 0,  2047,	addrLongAsLong));

	AddSpace(New CSpace( 8, "TCP",	"Timer/Counter Preset",		10, 0,  2047,	addrLongAsLong));

	AddSpace(New CSpace( 9, "OTA",	"Output Time Accumulated",	10, 0,  2047,	addrLongAsLong));

	AddSpace(New CSpace(10, "OTP",	"Output Time Preset",		10, 0,  2047,	addrLongAsLong));

	AddSpace(New CSpace(11, "HSA",	"High Speed Accumulated",	10, 0,  2047,	addrLongAsLong));

	AddSpace(New CSpace(12, "HSP",	"High Speed Preset",		10, 0,  2047,	addrLongAsLong));

	AddSpace(New CSpace( 2, "A",	"Analog Inputs",		10, 0,  4095,	addrWordAsWord));

	AddSpace(New CSpace( 6, "O",	"Analog Outputs",		10, 0,  4095,	addrWordAsWord));

	AddSpace(New CSpace(20, "M",	"Direct Modbus Addressing",	10, 0, 65535,	addrWordAsWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Phoenix Nano LC TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CPhoenixNanoLCTCPDeviceOptions, CUIItem);       

// Constructor

CPhoenixNanoLCTCPDeviceOptions::CPhoenixNanoLCTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Management

void CPhoenixNanoLCTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CPhoenixNanoLCTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CPhoenixNanoLCTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Phoenix Nano LC TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_PhoenixNanoLCTCPDriver(void)
{
	return New CPhoenixNanoLCTCPDriver;
	}

// Constructor

CPhoenixNanoLCTCPDriver::CPhoenixNanoLCTCPDriver(void)
{
	m_wID		= 0x3525;

	m_uType		= driverMaster;

	m_Manufacturer	= "Phoenix Contact";

	m_DriverName	= "nanoLC TCP";

	m_Version	= "1.00";

	m_ShortName	= "Phoenix nanoLC TCP";

	m_DevRoot	= "nano";
	}

// Destructor

CPhoenixNanoLCTCPDriver::~CPhoenixNanoLCTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CPhoenixNanoLCTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CPhoenixNanoLCTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CPhoenixNanoLCTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CPhoenixNanoLCTCPDeviceOptions);
	}

// End of File
