
#include "intern.hpp"

#include "indraclc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Indramat CLC Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIndramatCLCDeviceOptions, CUIItem);

// Constructor

CIndramatCLCDeviceOptions::CIndramatCLCDeviceOptions(void)
{
	m_Drop = 0;
	}

// Download Support

BOOL CIndramatCLCDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CIndramatCLCDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Indramat CLC Driver
//

// Instantiator

ICommsDriver * Create_IndramatCLCDriver(void)
{
	return New CIndramatCLCDriver;
	}

// Constructor

CIndramatCLCDriver::CIndramatCLCDriver(void)
{
	m_wID		= 0x3349;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Indramat";
	
	m_DriverName	= "CLC Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Indramat CLC";	

	AddSpaces();
	}

// Destructor

CIndramatCLCDriver::~CIndramatCLCDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CIndramatCLCDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CIndramatCLCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration
CLASS CIndramatCLCDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIndramatCLCDeviceOptions);
	}

// Implementation     

void CIndramatCLCDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SPINT,	"VI",  "Integer Parameters",		10, 0, 4095, addrLongAsLong));
	AddSpace(New CSpace(SPREAL,	"VR",  "Real Parameters",		10, 0, 4095, addrRealAsReal));
	AddSpace(New CSpace(SPGINT,	"GI",  "Global Integer Parameters",	10, 0, 4095, addrLongAsLong));
	AddSpace(New CSpace(SPGREAL,	"GR",  "Global Real Parameters",	10, 0, 4095, addrRealAsReal));
	AddSpace(New CSpace(SPIOW,	"IW",  "I/O Register Words",		10, 0, 4095, addrWordAsWord));
	AddSpace(New CSpace(SPIOB,	"IB",  "I/O Register Bits",		10, 0, 4095, addrBitAsBit));
	AddSpace(New CSpace(SPAXIS,	"AX",  "Axis Parameters",		10, 0, 4095, addrRealAsReal));
	AddSpace(New CSpace(SPCARD,	"CD",  "Card Parameters",		10, 0, 4095, addrRealAsReal));
	AddSpace(New CSpace(SPTASK,	"TP",  "Task Parameters",		10, 0, 4095, addrRealAsReal));
	AddSpace(New CSpace(SPSER,	"SC",  "SERCOS Drive Parameters",	10, 0, 4095, addrRealAsReal));
	AddSpace(New CSpace(SPERR,	"ERR",  "Error Text",			10, 0,    9, addrLongAsLong));
	AddSpace(New CSpace(SPTWC,	"TWC",  "User Write Command Text",	10, 0,    7, addrLongAsLong));
	AddSpace(New CSpace(SPTXW,	"TXW",  "Send User Write Command",	10, 0,    0, addrBitAsBit));
	AddSpace(New CSpace(SPTRC,	"TRC",  "User Read Command Text",	10, 0,    7, addrLongAsLong));
	AddSpace(New CSpace(SPTXR,	"TXR",  "Send User Read Command",	10, 0,    0, addrBitAsBit));
	AddSpace(New CSpace(SPRXD,	"RXD",  "User Command - Read Response",	10, 0,   14, addrLongAsLong));
	}

// Address Management

BOOL CIndramatCLCDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CIndramatCLCDialog Dlg(ThisObject, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CIndramatCLCDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) {

		return FALSE;
		}

	UINT uPos1 = 1;
	UINT uPos2 = 1;

	UINT uPar  = 0;
	UINT uSet  = 0;

	CString sErr;

	BOOL fIsIOWord   = pSpace->m_uTable == SPIOW;

	BOOL fParseError = FALSE;

	BOOL fParam = pSpace->m_uTable < SPERR;

	BOOL fIsSendText = pSpace->m_uTable == SPTXW || pSpace->m_uTable == SPTXR;

	if( fParam && !fIsIOWord) {

		uPos1 = Text.Find('(');
		uPos2 = Text.Find(')');
	
		if( uPos1 == NOTHING || uPos2 == NOTHING || uPos1 >= uPos2 ) {

			sErr.Printf( "%spppp(ss)",
				pSpace->m_Prefix
				);

			fParseError = TRUE;
			}
		}

	else {
		if( !fIsSendText ) {

			if( !isdigit(Text[0]) ) {

				if( fIsIOWord ) {

					sErr.Printf( "IWdddd" );
					}

				else {

					sErr.Printf( "%sd",
						pSpace->m_Prefix
						);
					}

				fParseError = TRUE;
				}
			}
		}

	if( !fParseError ) {

		if( fIsSendText ) {

			uPar = 0;
			}

		else {
			if( fIsIOWord || pSpace->m_uTable == SPERR ) {

				uPar = tatoi(Text);
				}

			else {
				uPar = tatoi(Text.Mid(0, uPos1));
				}
			}

		uSet = fParam && !fIsIOWord ? tatoi(Text.Mid(uPos1 + 1, uPos2 - uPos1 - 1)) : 0;

		if( uPar > pSpace->m_uMaximum ) {

			fParseError = TRUE;

			if( fParam ) {

				if( !fIsIOWord ) {

					sErr.Printf( "%s0000(ss) - %s%4.4d(ss)",
						pSpace->m_Prefix,
						pSpace->m_Prefix,
						pSpace->m_uMaximum
						);
					}

				else {
					sErr.Printf( "IW0000 - IW4095" );
					}
				}

			else {
				sErr.Printf( "%s0 - %s%1.1d",
					pSpace->m_Prefix,
					pSpace->m_Prefix,
					pSpace->m_uMaximum
					);
				}
			}

		else {
			if( fParam && !fIsIOWord ) {

				if( pSpace->m_uType == addrBitAsBit ) {

					if( uSet > 15 ) {

						sErr.Printf( "%spppp(00) - %spppp%s",
							pSpace->m_Prefix,
							pSpace->m_Prefix,
							BITMAXSTR
							);

						fParseError = TRUE;
						}
					}

				else {
					if( uSet > SETMAX ) {

						sErr.Printf( "%spppp(00) - %spppp%s",
							pSpace->m_Prefix,
							pSpace->m_Prefix,
							SETMAXSTR
							);

						fParseError = TRUE;
						}
					}
				}
			}
		}

	if( fParseError ) {

		Error.Set( sErr,
			0
			);

		return FALSE;
		}

	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Type   = pSpace->m_uType;
	Addr.a.m_Extra  = uSet > 15 ? 1 : 0;
	Addr.a.m_Offset = (uSet << 12) + uPar;

	return TRUE;
	}

BOOL CIndramatCLCDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uAddr = Addr.a.m_Offset;

		if( Addr.a.m_Table < SPERR ) {

			if( Addr.a.m_Table != SPIOW ) {

				Text.Printf( "%s%4.4d(%2.2d)", 
					pSpace->m_Prefix,
					uAddr & 0xFFF,
					(uAddr >> 12) + (Addr.a.m_Extra ? 16 : 0)
					);
				}

			else {
				Text.Printf( "%s%4.4d",
					pSpace->m_Prefix,
					uAddr & 0xFFF
					);
				}
			}

		else {
			if( Addr.a.m_Table == SPTXW || Addr.a.m_Table == SPTXR) {

				Text.Printf( "%s",
					pSpace->m_Prefix
					);
				}

			else {					
				Text.Printf( "%s%1.1d",
					pSpace->m_Prefix,
					uAddr & 0xF
					);
				}
			}

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Indramat CLC Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CIndramatCLCDialog, CStdAddrDialog);
		
// Constructor

CIndramatCLCDialog::CIndramatCLCDialog(CIndramatCLCDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "IndramatCLCElementDlg";
	}

AfxMessageMap(CIndramatCLCDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	};

// Overridables

BOOL CIndramatCLCDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;

	m_pSpace = m_pDriver->GetSpace(Addr);
	
	if( !m_fPart ) {

		SetCaption();

		LoadList();

		if( m_pSpace ) {

			ShowAddress(Addr);

			SetDlgFocus(2002);

			return FALSE;
			}

		ClearParSet();

		return TRUE;
		}
	else {
		LoadType();
		
		ShowAddress(Addr);

		SetDlgFocus(2002);

		return FALSE;
		}
	}

void CIndramatCLCDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	if( m_pSpace == NULL ) {

		ClearParSet();

		return;
		}

	ShowDetails();
	}

void CIndramatCLCDialog::SetAddressText(CString Text)
{
	BOOL fEnable   = !Text.IsEmpty();
	BOOL fParam    = FALSE;
	BOOL fIsIOWord = FALSE;

	if( m_pSpace ) {

		if( m_pSpace->m_uTable < SPERR ) {

			fParam = TRUE;
			fIsIOWord = m_pSpace->m_uTable == SPIOW;
			}
		}

	if( fEnable ) {

		UINT uPos = 0;

		CString s;
		CString t;

		BOOL fHasPrefix = FALSE;

		Text.MakeUpper();

		if( Text[0] == 'I' && Text[1] == 'W' ) {

			fIsIOWord = TRUE;
			}

		while( isalpha(Text[uPos]) ) {

			fHasPrefix = TRUE;
			uPos++;
			}

		s = fHasPrefix ? Text.Mid(uPos) : Text;

		if( fParam && !fIsIOWord ) {

			UINT uFind1 = s.Find('(');
			UINT uFind2 = s.Find(')');

			t = s.Left( uFind1 );

			GetDlgItem(2005).SetWindowText(s.Mid(uFind1+1, uFind2 - uFind1 - 1));
			}

		else {
			t = s;

			GetDlgItem(2005).SetWindowText("");
			}

		GetDlgItem(2002).SetWindowText(t);
		}

	GetDlgItem(2002).EnableWindow(fEnable);
	
	GetDlgItem(2005).EnableWindow(fEnable && fParam && !fIsIOWord);
	}

CString CIndramatCLCDialog::GetAddressText(void)
{
	CString sSet = "";

	CString sPre  = GetDlgItem(2001).GetWindowText();

	CString Text;

	Text += GetDlgItem(2002).GetWindowText();

	BOOL fParam    = sPre.GetLength() == 2;

	BOOL fIsIOWord = sPre[0] == 'I' && sPre[1] == 'W';

	if( fParam && !fIsIOWord ) {

		sSet      = GetDlgItem(2005).GetWindowText();

		UINT uSet = tatoi(sSet);

		if( sPre == BITPREFIX ) {

			if( uSet > 15 ) {

				Text += BITMAXSTR;
				return Text;
				}
			}

		else {
			if( uSet > SETMAX ) {

				Text += SETMAXSTR;

				return Text;
				}
			}

		Text += "(";
	
		Text += sSet;

		Text += ")";
		}

	return Text;
	}

void CIndramatCLCDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	SetAddressText(Text);
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);

	ShowDetails();
	}

// Helpers

void CIndramatCLCDialog::ShowDetails(void)
{
	if( !m_pSpace ) return;

	CListBox &ListBox = (CListBox &) GetDlgItem(4001);

	ListBox.ResetContent();

	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	ListBox.AddString(m_pSpace->GetTypeAsText(m_pSpace->m_uType));

	CString sMin   = m_pSpace->GetValueAsText(m_pSpace->m_uMinimum);
	CString sMinEx ="(00)";

	CString sMax   = m_pSpace->GetValueAsText(m_pSpace->m_uMaximum);
	CString sMaxEx = m_pSpace->m_uTable == SPIOB ? BITMAXSTR : SETMAXSTR;

	CString sSet = "( Set ) ";
	CString sPar = "Parameter";

	CString Radix  = "10";

	switch( m_pSpace->m_uTable ) {

		case SPERR:
		case SPTWC:
		case SPTRC:
		case SPRXD:
			sPar   = "Array Item";
		case SPIOW:
			sSet   = "";
			sMinEx = "";
			sMaxEx = "";
			break;

		case SPTXW:
		case SPTXR:
			sPar   = "";
			sSet   = "";
			sMin   = "";
			sMinEx = "";
			sMax   = "";
			sMaxEx = "";
			Radix  = "";
			ClearParSet();
			break;

		case SPIOB:
			sSet   = "( Bit ) ";
			break;

		case SPINT:
		case SPREAL:
		case SPGINT:
		case SPGREAL:
			sSet = "(Handle)";
			break;
		}

	CString Min  = sMin;
	Min         += sMinEx;

	CString Max  = sMax;
	Max         += sMaxEx;

	GetDlgItem(2007).SetWindowText(sPar);

	GetDlgItem(2008).SetWindowText(sSet);

	GetDlgItem(3004).SetWindowText(Min);

	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Radix);
	}

void CIndramatCLCDialog::ClearParSet(void)
{
	GetDlgItem(2002).SetWindowText("");

	GetDlgItem(2002).EnableWindow(FALSE);

	GetDlgItem(2005).SetWindowText("");
		
	GetDlgItem(2005).EnableWindow(FALSE);

	GetDlgItem(2007).SetWindowText("");

	GetDlgItem(2008).SetWindowText("");
	}

// End of File
