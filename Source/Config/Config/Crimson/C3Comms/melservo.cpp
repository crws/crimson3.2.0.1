
#include "intern.hpp"

#include "melservo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MELServo Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMELServoDeviceOptions, CUIItem);

// Constructor

CMELServoDeviceOptions::CMELServoDeviceOptions(void)
{
	m_Addr      = 1;

	m_Group     = 0;

	m_ServoType = J2SCL;
	}

// UI Management

void CMELServoDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	UINT uG = pItem->GetDataAccess("Group")->ReadInteger(pItem);

	if( uG != GROUP ) {

		return;
		}

	UINT uA = pItem->GetDataAccess("Addr")->ReadInteger(pItem);

	if( Tag == "Addr" ) {

		if( uA < 10 || uA > 15 ) {

			m_Addr = uA < 10 ? 10 : 15;

			pWnd->UpdateUI("Addr");
			}
		}

	else {
		if( Tag == "Group" ) {

			if( uA < 10 || uA > 15 ) {

				m_Group = 0;

				pWnd->UpdateUI("Group");
				}
			}
		}
	}

// Download Support

BOOL CMELServoDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Addr));
	Init.AddWord(WORD(m_Group));
	Init.AddWord(WORD(m_ServoType));

	return TRUE;
	}

// Meta Data Creation

void CMELServoDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Group);
	Meta_AddInteger(ServoType);
	}

//////////////////////////////////////////////////////////////////////////
//
// MELServo Driver
//

// Instantiator

ICommsDriver *	Create_MELServoDriver(void)
{
	return New CMELServoDriver;
	}

// Constructor

CMELServoDriver::CMELServoDriver(void)
{
	m_wID		= 0x401E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Mitsubishi Electric";
	
	m_DriverName	= "MELSERVO";
	
	m_Version	= "2.00";
	
	m_ShortName	= "Mitsubishi MELSERVO";

	m_DevRoot	= "Servo";

	AddSpaces();
	}

// Binding Control

UINT	CMELServoDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CMELServoDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS422;
	Serial.m_BaudRate = 57600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS	CMELServoDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMELServoDeviceOptions);
	}

// Address Management

BOOL	CMELServoDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CMELServoAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CMELServoDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uSI = IsSpecialItem(pConfig, pSpace->m_uTable);

	if( !uSI ) {

		if( pSpace->m_uTable == STOM && !DeviceIsJ3(pConfig) ) {

			if( Text[0] < '0' && Text[0] > '6' ) Text = "6"; // only J3 goes to 7
			} 

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	StripType(pSpace, Text);

	PTXT pError	= NULL;

	Addr.a.m_Table	= pSpace->m_uTable;

	UINT uOffset;

	if( uSI == 1 ) {

		Addr.a.m_Offset = 0;
		Addr.a.m_Type	= LL;

		uOffset	= tstrtoul(Text, &pError, 16);

		if( pError && pError[0] ) return FALSE;

		Addr.a.m_Extra	= uOffset;

		return TRUE;
		}

	UINT uFind = Text.Find('(');

	Addr.a.m_Extra = 0;

	if( uFind < NOTHING) {

		BYTE bGrp = BYTE(Text[uFind+1]);

		if( bGrp < 'A' + 4 ) {

			Addr.a.m_Extra = bGrp - 'A';
			}
		}

	Addr.a.m_Type = pSpace->m_uType;

	uOffset       = tatoi(Text);

	if( uOffset <= 255 ) {

		Addr.a.m_Offset = uOffset;

		return TRUE;
		}

	CString sErr;

	sErr.Printf( "%s:\n  [A-D]001 - [A-D]255",
		pSpace->m_Prefix
		);

	Error.Set( sErr,
		0
		);

	return FALSE;
	}

BOOL CMELServoDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	UINT uSI = IsSpecialItem(pConfig, Addr.a.m_Table);

	if( !uSI ) {

		return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
		}

	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( uSI == 1 ) {

		Text.Printf( "%s%X",

			pSpace->m_Prefix,
			Addr.a.m_Extra
			);

		return TRUE;
		}

	BYTE bGrp = BYTE(Addr.a.m_Extra);

	Text.Printf( "%s%d(%c)",

		pSpace->m_Prefix,
		Addr.a.m_Offset,
		bGrp + 'A'
		);

	return TRUE;
	}

// Implementation	

void CMELServoDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SPARLP, "PARC", L"R/W Parameter - Real",			10, 0, 0x5A, RR));
	AddSpace(New CSpace(SPARLPH,"PARCH",L"R/W Parameter - Long",			10, 0, 0x5A, LL));
	AddSpace(New CSpace(SPARA, "PARA", L"R/W Parameter - Real",			10, 0, 0x54, RR));
	AddSpace(New CSpace(SPARAH,"PARAH",L"R/W Parameter - Long",			10, 0, 0x54, LL));
	AddSpace(New CSpace(SPAR3, "PAR",  L"R/W Parameter - Real",			10, 1,  255, RR));
	AddSpace(New CSpace(SPAR3H,"PARH", L"R/W Parameter - Long",			10, 1,  255, LL));
	AddSpace(New CSpace(SPARU, "PARU", L"Read Parameter Upper Limit - Real",		10, 1,  255, RR));
	AddSpace(New CSpace(SPARUH,"PARUH",L"Read Parameter Upper Limit - Long",		10, 1,  255, LL));
	AddSpace(New CSpace(SPARL, "PARL", L"Read Parameter Lower Limit - Real",		10, 1,  255, RR));
	AddSpace(New CSpace(SPARLH,"PARLH",L"Read Parameter Lower Limit - Long",		10, 1,  255, LL));
	AddSpace(New CSpace(SPARAB,"PARAB",L"Read Parameter Abbreviation",		10, 1,  255, LL));
	AddSpace(New CSpace(SPAREN,"PAREN",L"Read Parameter Write Enable/Disable",	10, 1,  255, LL));
	AddSpace(New CSpace(SGRP,  "PARGP",L"Parameter Group",				10, 0,    3, LL));
	AddSpace(New CSpace(SSTSL, "STSC", L"Read Status Display Data",			16, 0,   17, RR));
	AddSpace(New CSpace(SSTSA, "STSA", L"Read Status Display Data",			16, 0,   14, RR));
	AddSpace(New CSpace(SSTSP, "STSP", L"Read Status Display Data",			16, 0,   16, RR));
	AddSpace(New CSpace(SSTS3U,"STSU", L"Read J3 Item Units String",			10, 0,    1, LL));
	AddSpace(New CSpace(SSTS3N,"STSN", L"Read J3 Item Name String",			10, 0,    2, LL));
	AddSpace(New CSpace(SPOS,  "PPOS", L"Point Table Position",			10, 1,   31, RR));
	AddSpace(New CSpace(SSPD,  "PSPD", L"Point Table Speed",				10, 1,   31, LL));
	AddSpace(New CSpace(SACC,  "PACC", L"Point Table Acceleration",			10, 1,   31, LL));
	AddSpace(New CSpace(SDEC,  "PDEC", L"Point Table Deceleration",			10, 1,   31, LL));
	AddSpace(New CSpace(SDWL,  "PDWL", L"Point Table Dwell",				10, 1,   31, LL));
	AddSpace(New CSpace(SAUX,  "PAUX", L"Point Table Auxiliary Function",		10, 1,   31, LL));
	AddSpace(New CSpace(SWEEP, "EEPW", L"Read a Point and Write to EEPROM",		10, 0,    0, BB));
	AddSpace(New CSpace(SALN,  "ALN",  L"Read Alarm History Number",			10, 0,    5, BB));
	AddSpace(New CSpace(SALT,  "ALT",  L"Read Alarm History - Time",			10, 0,    5, LL));
	AddSpace(New CSpace(SALSL, "ALSC", L"Read CL Status At Alarm Occurrence",	16, 0,   17, RR));
	AddSpace(New CSpace(SALSA, "ALSA", L"Read A Status At Alarm Occurrence",		16, 0,   14, RR));
	AddSpace(New CSpace(SALSP, "ALSP", L"Read CP Status At Alarm Occurrence",	16, 0,   16, RR));
	AddSpace(New CSpace(SALSU, "ALSU", L"Read Units String At Alarm Occurrence",	10, 0,    1, LL));
	AddSpace(New CSpace(SALSN, "ALSN", L"Read Name String At Alarm Occurrence",	10, 0,    2, LL));
	AddSpace(New CSpace(SACU,  "ACU",  L"Read/Reset Current Alarm",			10, 0,    0, WW));
	AddSpace(New CSpace(SEXT0, "EXTC", L"External I/O Signals",			10, 0,    5, LL));
	AddSpace(New CSpace(SEXTA, "EXTA", L"External I/O Signals",			10, 0,    1, LL));
	AddSpace(New CSpace(SLAT,  "LAT",  L"Current Position Latch Data",		10, 0,    0, RR));
	AddSpace(New CSpace(SGPRR, "GPR",  L"General Purpose R Register",		10, 1,    4, RR));
	AddSpace(New CSpace(SGPRD, "GPD",  L"General Purpose D Register",		10, 1,    4, RR));
	AddSpace(New CSpace(SGSV,  "GSV",  L"Read/Write Group Setting",			10, 0,    0, BB));
	AddSpace(New CSpace(SSEP,  "SEP",  L"Read Motor End Pulse Unit Position",	10, 0,    0, LL));
	AddSpace(New CSpace(SCUP,  "CUP",  L"Read Command Unit Position",		10, 0,    0, LL));
	AddSpace(New CSpace(SSVR,  "SVR",  L"Read Software Version String",		10, 0,    3, LL));
	AddSpace(New CSpace(SACL,  "ACL",  L"Clear Alarm History",			10, 0,    0, BB));
	AddSpace(New CSpace(SSCL,  "SCL",  L"Clear Status Display Data",			10, 0,    0, BB));
	AddSpace(New CSpace(SOMS,  "OMS",  L"Operation Mode Selection",			10, 0,    0, BB));
	AddSpace(New CSpace(STOM3M,"TOMM", L"Read Test Operation Mode",			10, 0,    6, LL));
	AddSpace(New CSpace(STOM3S,"TOMS", L"Read Test Operation Status",		10, 0,    6, LL));
	AddSpace(New CSpace(STOM,  "TOM",  L"Write Test Operation Data Item",		10, 0,    7, LL));
	AddSpace(New CSpace(SEIS,  "EIS",  L"External Input Enable/Disable",		10, 0,    0, BB));
	AddSpace(New CSpace(SEOS,  "EOS",  L"External Output Enable/Disable",		10, 0,    0, BB));
	AddSpace(New CSpace(SWPEC, "WPEC", L"Write Parameter To EEPROM",			10, 0, 0x5A, RR));
	AddSpace(New CSpace(SWPEA, "WPEA", L"Write Parameter To EEPROM",			10, 0, 0x54, RR));
	AddSpace(New CSpace(SWGER, "WGER", L"Write R Register To EEPROM",		10, 1,    4, RR));
//	AddSpace(New CSpace(SWGED, "WGED", L"Write D Register To EEPROM",		10, 1,    4, LL));
	AddSpace(New CSpace(SCACHE,"READ", L"Read TOM and OMS Selections",		10, 0,    3, LL));
	AddSpace(New CSpace(SGENR, "GENR", L"Read Generic Item as String",		16, 1,0xFFFF, LL));
	AddSpace(New CSpace(SGENW, "GENW", L"Write Generic Item as String",		16, 0,    3, LL));
	AddSpace(New CSpace(SPBAD, "???",  L"Invalid Selection",				10, 0,    0, LL));
	}

// Helpers

BOOL CMELServoDriver::IsJ3UnitAndName(UINT uTable)
{
	switch( uTable ) {

		case SSTS3U:
		case SSTS3N:
		case SALSU:
		case SALSN:
			return TRUE;
		}

	return FALSE;
	}

BOOL CMELServoDriver::IsJ3ParameterItem(UINT uTable)
{
	switch( uTable ) {

		case SPAR3:
		case SPAR3H:
		case SPARU:
		case SPARUH:
		case SPARL:
		case SPARLH:
		case SPARAB:
		case SPAREN:
			return TRUE;
		}

	return FALSE;
	}

UINT CMELServoDriver::IsSpecialItem(CItem *pConfig, UINT uTable)
{
	if( DeviceIsJ3(pConfig) ) {

		if( IsJ3UnitAndName(uTable) ) {

			return 1;
			}

		if( IsJ3ParameterItem(uTable) ) {

			return 2;
			}
		}

	return 0;
	}

BOOL CMELServoDriver::DeviceIsJ3(CItem *pConfig)
{
	return pConfig->GetDataAccess("ServoType")->ReadInteger(pConfig) == J3A;
	}


//////////////////////////////////////////////////////////////////////////
//
// MELServo Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CMELServoAddrDialog, CStdAddrDialog);
		
// Constructor

CMELServoAddrDialog::CMELServoAddrDialog(CMELServoDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_uDevice  = pConfig->GetDataAccess("ServoType")->ReadInteger(pConfig);

	m_pMDriver = &Driver;

	m_pConfig  = pConfig;

	SetName(fPart ? TEXT("PartMELServoAddressDlg") : TEXT("MELServoAddressDlg"));
	}

// Message Map

AfxMessageMap(CMELServoAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CMELServoAddrDialog)
	};

// Message Handlers

BOOL CMELServoAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();
	
	if( !m_fPart ) {

		SetCaption();

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddrAndInfo(Addr);

			return FALSE;
			}

		return TRUE;
		}
	else {
		ShowAddrAndInfo(Addr);

		if( GetDlgItem(2002).GetWindowText() != "" ) SetDlgFocus(2002);

		return FALSE;
		}
	}

// Notification Handlers

void CMELServoAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CMELServoAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = m_pSpace->m_uMinimum;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			ShowAddrAndInfo(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(2001).SetWindowText("None");
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2007).SetWindowText("");
		GetDlgItem(3020).SetWindowText("");

		GetDlgItem(2001).EnableWindow(TRUE);
		GetDlgItem(2002).EnableWindow(FALSE);
		GetDlgItem(2005).EnableWindow(FALSE);
		GetDlgItem(2007).EnableWindow(FALSE);

		ClearInfo();
		}
	}

// Command Handlers

BOOL CMELServoAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CAddress Addr;

		if( !AllowSpace(m_pSpace) ) {

			Addr.a.m_Table  = SPBAD;
			Addr.a.m_Offset = 0;
			Addr.a.m_Type   = addrLongAsLong;
			Addr.a.m_Extra  = 0;

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		if( m_pMDriver->IsSpecialItem(m_pConfig, m_pSpace->m_uTable) == 2 ) {

			CString s = GetDlgItem(2007).GetWindowText();

			Text += '(';

			Text += s[0] >= 'A' && s[0] <= 'D' ? s : "A";

			Text += ')';
			}

		CError   Error(TRUE);
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables

void CMELServoAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	UINT uPreLen    = m_pSpace->m_Prefix.GetLength();

	CString AddrTxt = Text.Mid(uPreLen);

	CString c = "";

	if( m_pMDriver->IsSpecialItem(m_pConfig, Addr.a.m_Table) == 2 ) {

		UINT uFind = Text.Find('(');

		if( uFind < NOTHING) {

			UINT e  = (Addr.a.m_Extra & 3) + 'A';

			c      += char(e);

			AddrTxt = Text.Mid(uPreLen, uFind - uPreLen);
			}

		else c += 'A';
		}

	GetDlgItem(2007).SetWindowText(c);

	Text = AddrTxt;

	SetAddressText(Text);

	SetAddrEntry(Addr.a.m_Table);
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);
	}

BOOL CMELServoAddrDialog::AllowSpace(CSpace *pSpace)
{
	UINT uT = pSpace->m_uTable;

	switch( m_uDevice ) { // dis-allowed spaces

		case J2SCL:
			switch( uT ) {

				case SPARA:
				case SPARAH:
				case SPAR3:
				case SPAR3H:
				case SPARU:
				case SPARUH:
				case SPARL:
				case SPARLH:
				case SPARAB:
				case SPAREN:
				case SGRP:
				case SSTSA:
				case SSTSP:
				case SALSA:
				case SALSP:
				case SALSU:
				case SALSN:
				case SEXTA:
				case SSEP:
				case SCUP:
				case SWPEA:
				case SPOS:
				case SSPD:
				case SACC:
				case SDEC:
				case SDWL:
				case SAUX:
				case SWEEP:
				case SSTS3U:
				case SSTS3N:
				case STOM3M:
				case STOM3S:
				case SPBAD:
					return FALSE;
				}

			return TRUE;

		case J2SCP:
			switch( uT ) {

				case SPARA:
				case SPARAH:
				case SPAR3:
				case SPAR3H:
				case SPARU:
				case SPARUH:
				case SPARL:
				case SPARLH:
				case SPARAB:
				case SPAREN:
				case SGRP:
				case SSTSL:
				case SSTSA:
				case SALSL:
				case SALSA:
				case SALSU:
				case SALSN:
				case SEXTA:
				case SLAT:
				case SGPRR:
				case SGPRD:
				case SSEP:
				case SCUP:
				case SWPEA:
				case SWGER:
				case SWGED:
				case SSTS3U:
				case SSTS3N:
				case STOM3M:
				case STOM3S:
				case SPBAD:
					return FALSE;
				}

			return TRUE;

		case J2SA:
			switch( uT ) {
				case SPARLP:
				case SPARLPH:
				case SPAR3:
				case SPAR3H:
				case SPARU:
				case SPARUH:
				case SPARL:
				case SPARLH:
				case SPARAB:
				case SPAREN:
				case SGRP:
				case SSTSL:
				case SSTSP:
				case SALSL:
				case SALSP:
				case SALSU:
				case SALSN:
				case SEXT0:
				case SLAT:
				case SGPRR:
				case SGPRD:
				case SWPEC:
				case SWGER:
				case SWGED:
				case SPOS:
				case SSPD:
				case SACC:
				case SDEC:
				case SDWL:
				case SAUX:
				case SWEEP:
				case SSTS3U:
				case SSTS3N:
				case STOM3M:
				case STOM3S:
				case SPBAD:
					return FALSE;
				}

			return TRUE;

		case J3A:
			switch( uT ) {

				case SPARLP:
				case SPARLPH:
				case SPARA:
				case SPARAH:
				case SSTSL:
				case SSTSP:
				case SPOS:
				case SSPD:
				case SACC:
				case SDEC:
				case SDWL:
				case SAUX:
				case SWEEP:
				case SALSL:
				case SALSA:
				case SALSP:
				case SEXTA:
				case SLAT:
				case SGPRR:
				case SGPRD:
				case SWPEC:
				case SWPEA:
				case SWGER:
				case SWGED:
				case SGSV:
				case SCACHE:
				case SPBAD:
					return FALSE;
				}

			return TRUE;
		}

	return TRUE;
	}

// Helpers
void CMELServoAddrDialog::ShowInfo(void)
{
	GetDlgItem(2003).SetWindowText("Data Type:");
	GetDlgItem(2003).EnableWindow(TRUE);

	if( !m_pSpace ) {

		ClearInfo();

		return;
		}

	CString s[MAXINFO];
	CString sType = L"INTEGER";
	CString sCode = L"R = ";

	UINT uRadix   = 0;
	UINT uCt      = 0;
	UINT uT       = m_pSpace->m_uTable;

	BOOL fL       = m_uDevice == J2SCL;
	BOOL fJ3Grp   = FALSE;

	switch( uT ) {

		case SPAR3:
		case SPAR3H:
		case SPARU:
		case SPARUH:
		case SPARL:
		case SPARLH:
		case SPARAB:
		case SPAREN:
			fJ3Grp = TRUE;
		case SPARLP:
		case SPARA:
		case SPARLPH:
		case SPARAH:

			s[uCt]  = L"Select Parameter";

			if( !fJ3Grp ) {

				s[uCt] += L" (0 - ";

				switch( uT ) {

					case SPARLP:
					case SPARLPH:	s[uCt] += L"90"; break;
					case SPARA:
					case SPARAH:	s[uCt] += L"84"; break;

					default:	s[uCt] += L"255"; break;
					}

				s[uCt++] += L")";
				}

			else {
				s[uCt++] += L" number 1 - 255.";
				s[uCt++]  = L"Select Group as follows:";
				s[uCt++]  = S_S30;
				s[uCt++]  = S_S31;
				s[uCt++]  = S_S32;
				s[uCt++]  = S_S33;
				}

			switch( uT ) {

				case SPARU:
				case SPARUH:
					sCode += L"6";
					break;

				case SPARL:
				case SPARLH:
					sCode += L"7";
					break;

				case SPARAB:
					sCode    += L"8";
					sType     = S_S36;
					uCt++;
					s[uCt++]  = S_S34;
					s[uCt++]  = S_S37;
					break;

				case SPAREN:
					sCode += L"9";
					break;

				default:
					sCode += L"5";
					break;
				}

			uRadix    = 10;

			BOOL fLong;

			fLong = TRUE;

			switch( uT ) {

				case SPARLP:
				case SPARA:
				case SPAR3:
				case SPARU:
				case SPARL:
					sType = S_REAL;
					fLong = FALSE;
					break;
				}

			if( uT != SPARAB && uT != SPAREN ) {

				sCode    += L" / W = 84";

				s[uCt]    = L"Use the value as a";
				s[uCt++] += !fLong ? L" Real Number" : L"n Integer";
				}

			break;

		case SGRP:
			s[uCt++]  = L"Group Identifiers:";

			MS(s, uCt, S_0, S_S30); uCt++;
			MS(s, uCt, S_1, S_S31); uCt++;
			MS(s, uCt, S_2, S_S32); uCt++;
			MS(s, uCt, S_3, S_S33); uCt++;

			sCode    += L"4 / W = 85";
			break;

		case SSTSL:
		case SSTSP:
		case SALSL:
		case SALSP:
			MS(s, uCt, S_0, S_S00); uCt++;			// Current Position
			MS(s, uCt, S_1, S_S01 ); uCt++;			// Command Position
			MS(s, uCt, S_2, S_S02 ); uCt++;			// Command Remaining Distance
			MS(s, uCt, S_3, fL ? S_S03 : S_S12); uCt++;	// Program Number : Point Table Number
			MS(s, uCt, S_4, fL ? S_S04 : S_S05); uCt++;	// Step Number : Cumulative Feedback Pulses
			MS(s, uCt, S_5, fL ? S_S05 : S_S06); uCt++;	// Cumulative Feedback Pulses : Servo Motor Speed
			MS(s, uCt, S_6, fL ? S_S06 : S_S07); uCt++;	// Servo Motor Speed :  Droop Pulses
			MS(s, uCt, S_7, fL ? S_S07 : S_S08); uCt++;	// Droop Pulses : Override
			MS(s, uCt, S_8, fL ? S_S08 : S_S09); uCt++;	// Override : Torque Limit Voltage
			MS(s, uCt, S_9, fL ? S_S09 : S_S0A); uCt++;	// Torque Limit Voltage : Regenerative Load Ratio
			MS(s, uCt, S_A, fL ? S_S0A : S_S0B); uCt++;	// Regenerative Load Ratio : Effective Load Ratio
			MS(s, uCt, S_B, fL ? S_S0B : S_S0C); uCt++;	// Effective Load Ratio : Peak Load Ratio
			MS(s, uCt, S_C, fL ? S_S0C : S_S0D); uCt++;	// Peak Load Ratio : Instantaneous Torque
			MS(s, uCt, S_D, fL ? S_S0D : S_S0E); uCt++;	// Instantaneous Torque : Within One Revolution Position
			MS(s, uCt, S_E, fL ? S_S0E : S_S0F); uCt++;	// Within One Revolution Position : ABS Counter
			MS(s, uCt, S_F, fL ? S_S0F : S_S10); uCt++;	// ABS Counter : Load Inertia Moment Ratio
			MS(s, uCt, S_10,fL ? S_S10 : S_S11); uCt++;	// Load Inertia Moment Ratio : Bus Voltage

			if( fL ) {
				MS(s, uCt, S_11, S_S11);		// Bus Voltage
				uCt++;
				}

			else s[uCt++] = L"";

			sType = S_REAL;
			sCode += uT == SSTSL ? L"1" : L"35";
			uRadix = 16;
			break;

		case SSTSA:
		case SALSA:
		case SALSU:
		case SALSN:
		case SSTS3U:
		case SSTS3N:
			MS(s, uCt, S_0, S_S05); uCt++; // Cumulative Feedback Pulses
			MS(s, uCt, S_1, S_S06); uCt++; // Servo Motor Speed
			MS(s, uCt, S_2, S_S07); uCt++; // Droop Pulses
			MS(s, uCt, S_3, S_S14); uCt++; // Cumulative Command Pulses
			MS(s, uCt, S_4, S_S15); uCt++; // Command Pulse Frequency
			MS(s, uCt, S_5, S_S16); uCt++; // Voltage-Analog Speed Command/Limit
			MS(s, uCt, S_6, S_S17); uCt++; // Voltage-Analog Torque Command/Limit
			MS(s, uCt, S_7, S_S0A); uCt++; // Regenerative Load Ratio
			MS(s, uCt, S_8, S_S0B); uCt++; // Effective Load Ratio
			MS(s, uCt, S_9, S_S0C); uCt++; // Peak Load Ratio
			MS(s, uCt, S_A, S_S0D); uCt++; // Instantaneous Torque
			MS(s, uCt, S_B, S_S0E); uCt++; // Within One-Revolution Position
			MS(s, uCt, S_C, S_S0F); uCt++; // ABS Counter
			MS(s, uCt, S_D, S_S10); uCt++; // Load Inertia Moment Ratio
			MS(s, uCt, S_E, S_S11); uCt++; // Bus Voltage

			if( uT == SSTSA || uT == SALSA ) {

				sType  = S_REAL;
				sCode += uT == SSTSA ? L"1" : L"35";
				}

			else {
				s[uCt++] = "";
				s[uCt++] = S_S34;
				s[uCt++] = S_S38;
				sType    = S_S36;
				sCode   += uT == SALSU || uT == SALSN ? L"35" : L"1";
				}

			uRadix = 16;
			break;

		case SPOS:
			sType  = S_REAL;
			sCode += L"40 / W = C0";
			uRadix = 10;
			s[uCt++] = S_S18;
			break;

		case SSPD:
			sCode += L"50 / W = C6";
			uRadix = 10;
			s[uCt++] = S_S18;
			break;

		case SACC:
			sCode += L"54 / W = C7";
			uRadix = 10;
			s[uCt++] = S_S18;
			break;

		case SDEC:
			sCode += L"58 / W = C8";
			uRadix = 10;
			s[uCt++] = S_S18;
			break;

		case SDWL:
			sCode += L"60 / W = CA";
			uRadix = 10;
			s[uCt++] = S_S18;
			break;

		case SAUX:
			sCode += L"64 / W = CB";
			uRadix = 10;
			s[uCt++] = S_S18;
			break;

		case SWEEP:
			sCode  = L"R/W Codes Vary";
			uRadix = 10;
			s[uCt++] = L"Write the point number (1 - 31) to execute.";
			s[uCt++] = L"Read always returns 0.";
			s[uCt++] = L"";
			s[uCt++] = L"Read the designated point, and write its";
			s[uCt++] = L"Position, Speed, Acceleration,";
			s[uCt++] = L"Deceleration, Dwell, and Auxiliary";
			s[uCt++] = L"Function, to EEPROM and RAM.";
			s[uCt++] = L"";
			s[uCt++] = S_CAUT;
			s[uCt++] = S_MANY;
			break;

		case SALN:
		case SALT:
			MS(s, uCt, S_0, L"Most Recent Alarm"); uCt++;
			MS(s, uCt, S_1, L"First Alarm in Past"); uCt++;
			MS(s, uCt, S_2, L"Second Alarm in Past"); uCt++;
			MS(s, uCt, S_3, L"Third Alarm in Past"); uCt++;
			MS(s, uCt, S_4, L"Fourth Alarm in Past"); uCt++;
			MS(s, uCt, S_5, L"Fifth Alarm in Past"); uCt++;
			sCode += L"33";
			uRadix = 10;
			break;

		case SACU:
			s[uCt] = S_S41;	s[uCt++] += L"to reset alarm";
			sCode += L"2 / W = 82";
			break;

		case SEXT0:
			MS(s, uCt, S_0, L"Input Device Status"); uCt++;
			MS(s, uCt, S_1, L"External Input Pin Status"); uCt++;
			MS(s, uCt, S_2, L"Inputs Switched Via Communications"); uCt++;
			MS(s, uCt, S_3, L"Output Device Status"); uCt++;
			MS(s, uCt, S_4, L"External Output Pin Status"); uCt++;
			MS(s, uCt, S_5, L"Communication Input Device Signal"); uCt++;
			s[uCt++] = L"     -- Write only - Command 92";
			sCode += L"12";
			uRadix = 10;
			break;

		case SEXTA:
			s[uCt++]   = L"0 = External Input Pin Status";
			s[uCt++]   = L"1 = External Output Pin Status";
			sCode += L"12";
			uRadix = 10;
			break;

		case SLAT:
			sType = S_REAL;
			sCode += L"6C";
			uRadix = 0;
			break;

		case SSEP:
		case SCUP:
			sCode += L"2";
			break;

		case SGSV:
			sCode += L"1F / W = 9F";
			s[uCt++]   = L"Write Format - Master Station Control";
			s[uCt++]   = L"Response Disable : Data = 0xa - 0xf";
			s[uCt++]   = L"Response Enable  : Data = 0x1a - 0x1f";
			s[uCt++]   = L"No Group Designation : Data = 0";
			s[uCt++]   = L"";
			s[uCt++]   = L"NOTE:";
			s[uCt++]   = L"All Group responses will be ignored unless";
			s[uCt++]   = L"the GSV command for that group has been";
			s[uCt++]   = L"sent with bit 4 set to 1.";
			s[uCt++]   = L"Repeat the command after a power cycle";
			s[uCt++]   = L"to enable group response";
			break;

		case SGPRR:
		case SGPRD:
			sType = S_REAL;
			uRadix = 10;
			s[uCt++] = L"Registers 1-4";
			sCode += (uT == SGPRR ? L"6D / W = B9" : L"6E / W = BA");
			break;

		case SSVR:
			s[uCt++] = S_S34;
			s[uCt++] = S_S35;
			sType    = S_S36;
			sCode += L"2";
			uRadix = 0;
			break;

		case STOM:
			BOOL fJ3;
			fJ3 = m_uDevice == J3A;

			s[uCt++] = L"Parameters for Command Code A0:";

			MS(s, uCt, S_0, L"Speed"); uCt++;
			MS(s, uCt, S_1, L"Accel/Decel Time Constant"); uCt++;

			if( !fJ3 ) {
				MS(s, uCt, S_2, L"Clear Time Constant");	s[uCt++] += S_S42;
				MS(s, uCt, S_3, S_S39);			uCt++;
				MS(s, uCt, S_4, S_S40);			s[uCt++] += S_S42;
				}
			else {
				MS(s, uCt, S_2, S_S39);			uCt++;
				MS(s, uCt, S_3, L"Direction (Data in Decimal)");	uCt++;
				s[uCt++] = L"  Write Data = 10 -> command unit forward";
				s[uCt++] = L"  Write Data = 11 -> command unit reverse";
				s[uCt++] = L"  Write Data = 20 -> encoder unit forward";
				s[uCt++] = L"  Write Data = 21 -> encoder unit reverse";
				s[uCt++] = L"  Any other value is ignored";
				MS(s, uCt, S_4, L"Start Command");	s[uCt++] += S_S42;
				MS(s, uCt, S_5, S_S40); uCt++;
				s[uCt++] = L"  Data Settings:";
				s[uCt++] = L"    1=Temporary stop, 2=Continue, 3=Clear";
				}

			s[uCt++] = L"";
			s[uCt++] = L"Parameters for Command Code 92:";

			MS(s, uCt, fJ3 ? S_6 : S_5, L"Input Signal For Test Operation");	uCt++;
			MS(s, uCt, fJ3 ? S_7 : S_6, L"Forced Output From Signal Pin");	uCt++;

			uRadix = 10;
			sCode  = L"W = A0 / 92";
			break;

		case STOM3M:
		case STOM3S:
			sCode   += L"0";
			s[uCt++] = L"Responses:";

			if(uT == STOM3M ) {

				MS(s, uCt, S_0, L"Normal (not Test Operation)"); uCt++;
				MS(s, uCt, S_1, S_S44); uCt++;
				MS(s, uCt, S_2, S_S45); uCt++;
				MS(s, uCt, S_3, S_S46); uCt++;
				MS(s, uCt, S_4, S_S47); uCt++;
				}

			else {
				MS(s, uCt, S_0, L"Positioning completion-waiting for start"); uCt++;
				MS(s, uCt, S_1, L"During Positioning operation"); uCt++;
				MS(s, uCt, S_2, L"During Temporary Stop"); uCt++;
				}
			break;

		case SOMS:
			s[uCt++] = L"Send Data Value as follows:";
			MS(s, uCt, S_0, L"Exit From Operation Mode"); uCt++;
			MS(s, uCt, S_1, S_S44); uCt++;
			MS(s, uCt, S_2, S_S45); uCt++;
			MS(s, uCt, S_3, S_S46); uCt++;
			MS(s, uCt, S_4, S_S47); uCt++;
			sCode  = L"W = 8B";
			break;

		case SACL:
		case SSCL:
			s[uCt] = S_S41; s[uCt++] += L"to execute";
			sCode    = S_S19;
			sCode += uT == SACL ? L"82" : L"81";
			break;

		case SEIS:
			s[uCt++] = L"Set Data =";
			MS(s, uCt, S_0, L"Disable Input Devices, Analog input"); uCt++;
			s[uCt++] = S_S43;
			s[uCt++] = L"";
			MS(s, uCt, S_1, L"Enable Input Devices, Analog input"); uCt++;
			s[uCt++] = S_S43;
			sCode    = S_S20;
			break;

		case SEOS:
			s[uCt++] = L"Set Data =";
			MS(s, uCt, S_0, L"Change Output Devices (DO) into the"); uCt++;
			s[uCt++] = L"       value of command OMS or TOM...";
			s[uCt++] = L"";
			MS(s, uCt, S_1, L"Enables External Output Devices (DO)"); uCt++;
			sCode    = S_S20;
			break;

		case SWPEC:
		case SWPEA:
		case SWGER:
		case SWGED:
			sType = S_REAL;
			uRadix = 10;

			s[uCt++]  = S_CAUT;
			s[uCt++]  = S_MANY;
			s[uCt++]  = "";
			switch( uT ) {
				case SWPEC:
				case SWPEA:
					if( uT == SWPEC ) {
						s[uCt++]  = L"Parameters 0 - 90";
						}
					else {
						s[uCt++]  = L"Parameters 0 - 84";
						}
					sCode = L"W = 84";
					break;

				case SWGER:
					sType = S_REAL;
				case SWGED:
					sCode    = S_S19;
					sCode   += uT == SWGER ? L"B9" : L"BA";
					s[uCt++] = L"Registers 1 - 4";
					break;
				}
			break;

		case SCACHE:
			sCode    = L"Internal";
			uRadix   = 10;
			s[uCt++] = L"Display most recent TOM/OMS Selection";
			s[uCt++] = L"";
			MS(s, uCt, S_0, L"TOM0 - Speed"); uCt++;
			MS(s, uCt, S_1, L"TOM1 - Acceleration"); uCt++;
			MS(s, uCt, S_2, L"TOM3 - Distance"); uCt++;
			s[uCt++] = L"";
			MS(s, uCt, S_3, L"Operation Mode Setting"); uCt++;
			break;

		case SGENR:
			sType    = S_S21;
			sCode    = S_S22;
			uRadix   = 0;
			s[uCt++] = L"Command Code = Upper 8 bits.";
			s[uCt++] = L"Data Number  = Lower 8 bits.";
			s[uCt++] = L"E.g. 051A reads Parameter 1A.";
			s[uCt++] = L"";
			s[uCt++] = L"The response to GENR includes every";
			s[uCt++] = L"byte in the response, from the initial";
			s[uCt++] = L"STX (0x2), through to and including,";
			s[uCt++] = L"the data terminating ETX (0x3).";
			s[uCt++] = L"Data formats may vary, and must be";
			s[uCt++] = L"parsed to extract the correct value.";
			s[uCt++] = L"";
			s[uCt++] = L"A Write to GENR is ignored.";
			s[uCt++] = L"";
			s[uCt++] = S_S34;
			s[uCt++] = S_S38;
			break;

		case SGENW:
			sType    = S_S21;
			sCode    = S_S22;
			uRadix   = 0;
			s[uCt++] = L"String Format = ccnnddd...";
			s[uCt++] = L"cc = Hex Command Number.";
			s[uCt++] = L"nn = Hex Data Number.";
			s[uCt++] = L"ddd... = data to write";
			s[uCt++] = L"The driver formats the rest.";
			s[uCt++] = L"E.g. 841A00000003 writes the data string";
			s[uCt++] = L"00000003 to Parameter 1A";
			s[uCt++] = L"";
			s[uCt++] = L"A read of GENW returns the most recent";
			s[uCt++] = L"string written to the driver, with one byte,";
			s[uCt++] = L"the ACK/NAK code, added to the end.";
			s[uCt++] = L"";
			s[uCt++] = S_S34;
			s[uCt++] = S_S38;
			break;

		default:
			uCt = 0;
			sType = L"NONE";
			sCode = L"";
			break;
		}

	for( UINT i = 0; i < MAXINFO; i++ ) {

		GetDlgItem(3001 + i).SetWindowText( i < uCt ? s[i] : "" );
		GetDlgItem(3001 + i).EnableWindow( i < uCt ? TRUE : FALSE );
		}

	GetDlgItem(2004).SetWindowText( sType );
	GetDlgItem(2004).EnableWindow( TRUE );
	GetDlgItem(2005).SetWindowText( uRadix == 16 ? L":Hexadecimal" : uRadix == 10 ? L":Decimal" : L"" );
	GetDlgItem(2005).EnableWindow( uRadix );
	GetDlgItem(2007).EnableWindow( fJ3Grp );
	GetDlgItem(3019).EnableWindow( TRUE );
	GetDlgItem(3020).SetWindowText( sCode );
	GetDlgItem(3020).EnableWindow( TRUE );
	}

void CMELServoAddrDialog::ClearInfo(void)
{
	for( UINT i = 3001; i <= 3000+MAXINFO; i++ ) {

		GetDlgItem( i ).EnableWindow(FALSE);
		GetDlgItem( i ).SetWindowText("");
		}

	GetDlgItem(2004).SetWindowText( "" );
	GetDlgItem(2004).EnableWindow( FALSE );
	GetDlgItem(2005).SetWindowText( "" );
	GetDlgItem(2005).EnableWindow( FALSE );
	GetDlgItem(2007).SetWindowText( "" );
	GetDlgItem(2007).EnableWindow( FALSE );
	GetDlgItem(3020).SetWindowText( "" );
	GetDlgItem(3020).EnableWindow( FALSE );
	}

void CMELServoAddrDialog::SetAddrEntry(UINT uTable)
{
	switch( uTable ) {

		case SGRP:
		case SACU:
		case SLAT:
		case SGSV:
		case SSEP:
		case SCUP:
		case SSCL:
		case SACL:
		case SOMS:
		case SEIS:
		case SEOS:
		case SSVR:
		case STOM3M:
		case STOM3S:
		case SGENW:
			GetDlgItem(2002).SetWindowText("");
			GetDlgItem(2002).EnableWindow(FALSE);
			return;
		}

	GetDlgItem(2002).EnableWindow(TRUE);

	BOOL fGrp = m_pMDriver->IsSpecialItem(m_pConfig, uTable) == 2;

	GetDlgItem(2007).EnableWindow(fGrp);
	}

void CMELServoAddrDialog::ShowAddrAndInfo(CAddress Addr)
{
	ShowAddress(Addr);
	ShowInfo();
	}

// End of File
