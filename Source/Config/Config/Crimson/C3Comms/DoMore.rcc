//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// UI for CUnidriveMSerialDeviceOptions
//

CDoMoreSerialDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|255,"
	"Indicate the drop number of the device to be addressed."
	"\0"

	"PingEnable,Enable Ping,,CUIDropDown,No|Yes,"
	"Selects whether or not the driver should attempt a read from a simple data "
	"register before attempting further communications. "
	"Enabling the ping allows the driver to quickly detect an offline device. "
	"\0" 

	"Ping,Ping Data (V) Register,,CUIEditInteger,|0||0|65535,"
	"Select the register to ping to check that the target device is online. "
	"You must be sure that this register is valid in the target device or "
	"the connection will never be established."
	"\0"

	"Timeout,Reply Timeout,,CUIEditInteger,|0|ms|0|10000,"
	"Select how long to wait for a reply from the target device."
	"\0"

	"Password,Password,,String/EditBox,*8,"
	"Provide the password required for logging-on to the PLC."
	"\0"

	"\0"
END

CDoMoreSerialDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Options,Drop,PingEnable,Ping,Timeout,Password\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CUnidriveMSerialDeviceOptions
//

CDoMoreUDPDeviceOptionsUIList RCDATA
BEGIN
	"IP,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the device."
	"\0"

	"Port,UDP Port,,CUIEditInteger,|0||1|9999,"
	"Indicate the UDP port number on which the protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"PingEnable,Enable Ping,,CUIDropDown,No|Yes,"
	"Selects whether or not the driver should attempt a read from a simple data "
	"register before attempting further communications. "
	"Enabling the ping allows the driver to quickly detect an offline device. "
	"\0" 

	"Ping,Ping Data (V) Register,,CUIEditInteger,|0||0|65535,"
	"Select the register to ping to check that the target device is online. "
	"You must be sure that this register is valid in the target device or "
	"the connection will never be established."
	"\0"

	"Password,Password,,String/EditBox,*8,"
	"Provide the password required for logging-on to the PLC."
	"\0"

	"\0"
END

CDoMoreUDPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Options,IP,Port,PingEnable,Ping,Password\0"
	"G:1,root,Protocol Options,Keep,Time1,Time3,Time2\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Do-More PLC Element Dialog
//

DoMoreElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT				2002,		 0,  0,  40,  12,
	COMBOBOX				2004,		44,  0,  44,  12, XS_DROPDOWNLIST
END

// End of File
