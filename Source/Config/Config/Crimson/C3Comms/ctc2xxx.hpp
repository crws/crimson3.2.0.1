
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CTC2XXX_HPP
	
#define	INCLUDE_CTC2XXX_HPP

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Device Options
//

class CCTC2xxxSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCTC2xxxSerialDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Protocol;
		UINT m_Drop;
		

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Master Driver
//

class CCTC2xxxSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CCTC2xxxSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration	
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);


	protected:
		// Implementation
		void AddSpaces(void); 
	};

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Address Selection Dialog
//

class CCTC2xxxAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCTC2xxxAddrDialog(CCTC2xxxSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Data members
		UINT m_uMode;

		// Overridables
		BOOL AllowSpace(CSpace *pSpace);
		void FindSpace(void);
		
	};
 
// End of File

#endif
