
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_OPCDRIVER_HPP
	
#define	INCLUDE_OPCDRIVER_HPP

//////////////////////////////////////////////////////////////////////////
//
// OPC Space Wrapper Class
//

class CSpaceOPC : public CSpace
{
	public:
		// Constructors
		CSpaceOPC(CString p, CString c, UINT n, UINT t, UINT a);

		// Public Data
		UINT m_uAccess;
	};

//////////////////////////////////////////////////////////////////////////
//
// OPC Device Options
//

class COPCDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COPCDeviceOptions(void);

		// Destructor
		~COPCDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Space List Access
		CSpaceList & GetSpaceList(void);

	public:
		// Public Data
		UINT    m_Addr;
		UINT    m_Socket;
		UINT    m_Keep;
		UINT    m_Ping;
		UINT    m_Time1;
		UINT    m_Time2;
		UINT    m_Time3;
		CString m_File;
		CString m_FileLast;

	protected:
		// Data
		CSpaceList m_List;
		BOOL       m_fWide;

		// Meta Data Creation
		void AddMetaData(void);

		// Space List Support
		void DeleteAllSpaces(void);

		// Datatbase Helpers
		BOOL LoadTarget(void);
		
		void C3LoadTarget(CTreeFile &Tree);
		void C3LoadTargetSystem(CTreeFile &Tree);
		void C3LoadTargetEthernet(CTreeFile &Tree);
		void C3LoadTargetComms(CTreeFile &Tree);
		void C3LoadTargetServices(CTreeFile &Tree);
		void C3LoadTargetTags(CTreeFile &Tree);
		void C3LoadTargetServer(CTreeFile &Tree);

		void C2LoadTarget(CTreeFile &Tree);
		void C2LoadTargetSystem(CTreeFile &Tree);
		void C2LoadTargetEthernet(CTreeFile &Tree);
		void C2LoadTargetComms(CTreeFile &Tree);
		void C2LoadTargetTags(CTreeFile &Tree);
		void C2LoadTargetServer(CTreeFile &Tree);
	};

//////////////////////////////////////////////////////////////////////////
//
// OPC Driver
//

class COPCDriver : public CBasicCommsDriver 
{
	public:
		// Constructor
		COPCDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

	protected:
		// Data
		INDEX m_n;

		// Space List Helpers
		CSpace * GetSpace(COPCDeviceOptions *pConfig, CAddress const &Addr);
		CSpace * GetSpace(COPCDeviceOptions *pConfig, CString Text);

		// Implementation
		CString StripType(CSpace *pSpace, CString &Text);

		// Friends
		friend class COPCAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class COPCAddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		COPCAddrDialog(COPCDriver &Driver, CAddress &Addr, CItem *pConfig);

	protected:
		// Data Members
		COPCDriver        * m_pDriver;
		COPCDeviceOptions * m_pConfig;
		CAddress	  * m_pAddr;
		CSpace            * m_pSpace;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSpaceChange(UINT uID, CWnd &Wnd);
		void OnTypeChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void    SetCaption(void);
		void    LoadList(void);
		void    LoadType(void);
		void    FindSpace(void);
		UINT    GetTypeCode(void);
		CString GetTypeText(void);
	};

// End of File

#endif
