
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EATONELC_HPP
	
#define	INCLUDE_EATONELC_HPP

// Space Definitions
enum {
	SP_D	= 1,	// Holding Registers
	SP_M	= 2,	// Memory Bits
	SP_S	= 3,	// Step Bits
	SP_X	= 4,	// External Input Bits
	SP_Y	= 5,	// External Output Bits
	SP_T	= 6,	// Timer Value
	SP_U	= 7,	// Timer Output Contact
	SP_C	= 8,	// Counter Value - 16 bits
	SP_B	= 9,	// Counter Output Contact
	SP_A	= 10	// Counter Value - 32 bits
	};

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Driver Options
//

class CEatonELCSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEatonELCSerialDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Protocol;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Device Options
//

class CEatonELCSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEatonELCSerialDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		BOOL m_fIgnoreReadEx;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Driver
//

class CEatonELCSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEatonELCSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		
	protected:
		// Implementation
		void AddSpaces(void);
		UINT NormalizeType(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Address Selection
//

class CEatonELCSerialAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CEatonELCSerialAddrDialog(CEatonELCSerialDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		BOOL	AllowType(UINT uType);
	};

// End of File

#endif
