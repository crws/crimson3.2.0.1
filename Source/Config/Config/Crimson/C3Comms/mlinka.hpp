
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MLINKA_HPP
	
#define	INCLUDE_MLINKA_HPP

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK Device Options
//

class CSEWMovilinkADeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSEWMovilinkADeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Device;
		UINT m_Broadcast;
				

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK B Device Options
//

class CSEWMovilinkBDeviceOptions : public CSEWMovilinkADeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSEWMovilinkBDeviceOptions(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK Space Wrapper Class
//

class CSpaceMovilink : public CSpace
{
	public:
		// Constructors

		CSpaceMovilink(UINT t, UINT i, CString p, AddrType type, CString c, UINT n=0, UINT x=0, BOOL fRO=FALSE);

		// Public Data

		UINT m_Index;
		BOOL m_ReadOnly;
			
	};

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK Constants
//

#define SEW_ID_INDX		1
#define SEW_ID_IPOS		2
#define	SEW_ID_PD		3
#define SEW_ID_SEND		4

#define SEW_INDEX		0x0001
#define SEW_IPOS		0x0003

#define SEW_IPOS_OFFSET		11000

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK A Master Driver
//

class CSEWMovilinkASerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSEWMovilinkASerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration	
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		CString GetSuffix(UINT uTable, UINT uValue);
		UINT	GetSuffix(CString Text);
		UINT	GetOffset(CSpace *pSpace);
								
	protected:
		// Implementation
		void AddSpaces(void); 
	};

//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK A Master Driver
//

class CSEWMovilinkBSerialDriver : public CSEWMovilinkASerialDriver
{
	public:
		// Constructor
		CSEWMovilinkBSerialDriver(void);

		// Configuration	
		CLASS GetDeviceConfig(void);


	protected:
		// Implementation
		void AddSpaces(void); 
	};


//////////////////////////////////////////////////////////////////////////
//
// SEW MOVILINK Address Selection Dialog
//

class CSEWMovilinkAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSEWMovilinkAddrDialog(CSEWMovilinkASerialDriver &Driver, CAddress &Addr, BOOL fPart);
		
	protected:
		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Helpers
		void ShowElements(void);
	       	void ShowProcessData(void);
		void ShowNoData(void);
	      	void ShowProcessDataInfo(void);
		
	};

// End of File

#endif
