 #include "modslv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MBTCPS_HPP
	
#define	INCLUDE_MBTCPS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave TCP/IP Driver Options
//

class CModbusSlaveTCPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModbusSlaveTCPDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Socket;
		UINT m_Count;
		UINT m_Restrict;
		UINT m_SecAddr;
		UINT m_SecMask;
		UINT m_FlipLong;
		UINT m_FlipReal;
		UINT m_Timeout;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave TCP/IP Driver
//

class CModbusSlaveTCPDriver : public CModbusSlaveDriver
{
	public:
		// Constructor
		CModbusSlaveTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Server (Slave) TCP/IP Driver - Multiple Devices
//

class CModbusDeviceServerTCPDriver : public CModbusSlaveTCPDriver
{
	public:
		// Constructor
		CModbusDeviceServerTCPDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

// End of File

#endif
