
#include "intern.hpp"

#include "IfmDualisDeviceOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor  Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDualisVisionSensorDeviceOptions, CUIItem);

// Constructor

CDualisVisionSensorDeviceOptions::CDualisVisionSensorDeviceOptions(void)
{
	m_Camera    = 0;

	m_IP        = DWORD(MAKELONG(MAKEWORD(49, 15), MAKEWORD(168, 192)));

	m_Port      = 50010;

	m_Time4     = 10;

	m_Start     = "start";

	m_Stop      = "stop";

	m_Separator = "#";

	m_Image     = 0;

	m_Protocol  = 2;
	}

// Download Support

BOOL CDualisVisionSensorDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Camera));

	Init.AddLong(LONG(m_IP));
	
	Init.AddWord(WORD(m_Port));

	Init.AddWord(WORD(m_Time4));

	Init.AddByte(BYTE(m_Image));

	Init.AddText(m_Start);

	Init.AddText(m_Stop);

	Init.AddText(m_Separator);

	Init.AddByte(BYTE(m_Protocol));

	return TRUE;
	}

// Persistence

void CDualisVisionSensorDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		if(Name == "Device") {

			m_Camera = Tree.GetValueAsInteger();

			continue;
			}

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

// Meta Data Creation

void CDualisVisionSensorDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Camera);
	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Time4);
	Meta_AddInteger(Image);
	Meta_AddString(Start);
	Meta_AddString(Stop);
	Meta_AddString(Separator);
	Meta_AddInteger(Protocol);
	}

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Camera Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDualisVisionSensorCameraDeviceOptions, CDualisVisionSensorDeviceOptions);

// Constructor

CDualisVisionSensorCameraDeviceOptions::CDualisVisionSensorCameraDeviceOptions(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Data Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDualisVisionSensorDataDeviceOptions, CDualisVisionSensorDeviceOptions);

// Constructor

CDualisVisionSensorDataDeviceOptions::CDualisVisionSensorDataDeviceOptions(void)
{
	}

// End of File
