
#include "intern.hpp"

#include "eztcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// EZ Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEZMasterTCPDeviceOptions, CEZMasterDeviceOptions);       

// Constructor

CEZMasterTCPDeviceOptions::CEZMasterTCPDeviceOptions(void)
{
	m_IP   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Port = 49999;

	m_Group = 1;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CEZMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CEZMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Group));
	Init.AddWord(WORD(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CEZMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// EZ Master TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_EZMasterTCPDriver(void)
{
	return New CEZMasterTCPDriver;
	}

// Constructor

CEZMasterTCPDriver::CEZMasterTCPDriver(void)
{
	m_wID		= 0x350F;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "EZ Automation";
	
	m_DriverName	= "EZ TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "EZ TCP/IP Master";
	}

// Destructor

CEZMasterTCPDriver::~CEZMasterTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CEZMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEZMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CEZMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEZMasterTCPDeviceOptions);
	}

// End of File