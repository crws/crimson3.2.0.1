
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// UI for CMicromodDeviceOptions
//

CMicromodDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|255,"
	"Indicate the unit number of this device."
	"\0"

	"Database,Database Type,,CUIDropDown,Current|Main|Default|Module,"
	"Select Database type to access."
	"\0"

	// this has been removed - remains for legacy apps in C2
	"UnlockGNR,Generic Command Unlock,,CUIEditInteger,|0||0|99999,"
	"Enter the unlock code to enable generic commands."
	"\0"

	"TagPrefix,Tag Prefix,,CUIEditString,6,"
	""
	"\0"

	"ArraySelect,Function Select,,CUIEditInteger,|0||0|10,"
	""
	"\0"

	"ArrayValue,Value To Access,,CUIEditInteger,|0||0|10,"
	""
	"\0"

	"ArrayString,Alpha LSP,,CUIEditString,16,"
	""
	"\0"

	"Push,,,CUIPushRow,Manage...|Import...|Export..."
	""
	"\0"

	"\0"
END

CMicromodDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Identification,Drop,Database\0"
	"G:1,root,Unique Tag Prefix for Device,TagPrefix\0"
	"G:1,root,Tag Names,Push\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Micromod Element Dialog
//
//

MicromodElementDlg DIALOG 0, 0, 0, 0
CAPTION "Micromod"
BEGIN
	GROUPBOX	"&Item Selection",	1000,	  4,   4, 156,  80
		LISTBOX				1001,	 10,  18, 142,  62, XS_LISTBOX | LBS_USETABSTOPS

	GROUPBOX	"Data &Type",		4000,	  4,  86, 156,  46
		LISTBOX				4001,	 10,  96, 142,  38, XS_LISTBOX | LBS_USETABSTOPS

	GROUPBOX	"&Alias Names",			1050,	  4, 134, 156, 184
		LTEXT	"FG Offset    ID      Name",    1052,	 10, 143, 140,  10
		LISTBOX					1051,	 10, 152, 142, 164, XS_LISTBOX | LBS_USETABSTOPS

	GROUPBOX	"&Selection ID",	2000,	162,   4, 168,  40
		LTEXT		"<Prefix>",	2001,	170,  16,  20,  12			// CURSPC
		EDITTEXT			2002,	192,  15,  20,	12			// CURRID
		LTEXT		"<type>",	2003,	214,  16,  20,  12			// ATTYPE
		PUSHBUTTON	"&Load ID #",	2008,   298,  13,  30,  14, XS_BUTTONREST	// VLOADID
		LTEXT		"Alias:",	2040,	170,  30,  20,  12			// ALSTXT
		EDITTEXT			2041,   192,  29, 105,  12			// ALSVAL

	GROUPBOX	"Current Selection",	2010,	162,  46, 168,  67
		LTEXT		"LSP:",		2011,	170,  58,  34,  10			// LSPTXT
		LTEXT		"0000000000",	2012,   206,  58,  34,  10			// CURRLSP
		CONTROL	"Hex",		2013, "button", BS_AUTOCHECKBOX,  244,  58,  20,  10	// LHEXSEL
		LTEXT		"<ALSP>",	2009,	170,  71,  35,  10			// ALPHLSP

		LTEXT		"FG Offset:",	2015,	170,  87,  34,  10			// OFFTXT
		LTEXT		"<00>",		2016,	206,  87,  24,  10			// CURROFF

		LTEXT		"Parameter:",	2019,	244,  87,  34,  10			// PARTXT
		LTEXT		"<00>",		2020,   278,  87,  21,  10			// CURRPAR

		LTEXT		"Bytes:"	2023,	170,  99,  34,  10			// SZETXT
		LTEXT		"0",		2024,	206,  99,  24,  10			// CURRSZE

		LTEXT		"Occurrence:",	2027,	244,  99,  34,  10			// OCCTXT
		LTEXT		"<00>"		2028,	278,  99,  19,	10			// CURROCC

	GROUPBOX	"LSP Edit",		2211,	162, 115, 168, 129
		LTEXT		"LSP:",		2111,	170, 127,  34,  12			// ELSPTXT			
		EDITTEXT			2112,   226, 127,  34,  12			// EDLSP
		CONTROL	"Edit via LSP",	2150, "button", BS_AUTOCHECKBOX, 264, 127,  45,  12	// EDBYLSP

		LTEXT		"&FG Offset:",	2115,	170, 142,  34,  12			// EOFFTXT
		EDITTEXT			2116,   226, 142,  34,  12			// EDOFF

		LTEXT		"&Occurrence:",	2127,	170, 157,  34,  12			// EOCCTXT
		EDITTEXT			2128,	226, 157,  34,	12			// EDOCC

		LTEXT		"&Parameter:",	2119,	170, 172,  34,  12			// EPARTXT
		EDITTEXT			2120,   226, 172,  34,  12			// EDPAR

		LTEXT		"&Bytes:"	2123,	170, 187,  34,  12			// ESZETXT
		EDITTEXT			2124,   226, 187,  34,  12			// EDSZE

		LTEXT		"Bloc&k:",	2137,	170, 203,  34,  12			// EDCBTXT
		COMBOBOX			2138,	226, 201,  34,  14, XS_DROPDOWNLIST	// EDCBVAL

		LTEXT		"<AL&SP>",	2139,	264, 203,  34,  12			// EDALSP

		PUSHBUTTON	"&Replace",	2141,   170, 220,  40,  14, XS_BUTTONREST	// VREPLCE
		PUSHBUTTON	"&Add Item",	2143,	226, 220,  40,  14, XS_BUTTONREST	// VINSERT
		PUSHBUTTON	"&Delete",	2144,	282, 220,  40,  14, XS_BUTTONREST	// VDELETE

	GROUPBOX	"Details",		3000,	162, 246, 168,  66
		LTEXT		"Type:",	3001,	168, 258,  36,  10
		LTEXT		"<type>",	3002,	218, 258,  58,  10
		LTEXT		"Minimum:",	3003,	168, 270,  36,  10
		LTEXT		"<min>",	3004,	218, 270,  58,  10
		LTEXT		"Maximum:",	3005,	168, 282,  36,  10
		LTEXT		"<max>",	3006,	218, 282,  58,  10
		LTEXT		"Radix:",	3007,	168, 294,  36,  10
		LTEXT		"<radix>",	3008,	218, 294,  58,  10

	DEFPUSHBUTTON   "OK",			IDOK,	  170, 316,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL, 264, 316,  40,  14, XS_BUTTONREST
END

CMicromodTCPDeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the device."
	"\0"

	"Socket,TCP Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the TCP port number on which the protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"Drop,Drop Number,,CUIEditInteger,|0||0|255,"
	"Indicate the unit number of this device."
	"\0"

	"Database,Database Type,,CUIDropDown,Current|Main|Default|Module,"
	"Select Database type to access."
	"\0"

	"UnlockGNR,Generic Command Unlock,,CUIEditInteger,|0||0|99999,"
	"Enter the unlock code to enable generic commands."
	"\0"

	"TagPrefix,Tag Prefix,,CUIEditString,6,"
	""
	"\0"

	"ArraySelect,Function Select,,CUIEditInteger,|0||0|10,"
	""
	"\0"

	"ArrayValue,Value To Access,,CUIEditInteger,|0||0|10,"
	""
	"\0"

	"Push,,,CUIPushRow,Manage...|Import...|Export..."
	""
	"\0"

	"\0"
END

CMicromodTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Socket\0"
	"G:1,root,Protocol Options,Keep,Time1,Time3,Time2\0"
	"G:1,root,Identification,Drop,Database\0"
	"G:1,root,Unique Tag Prefix for Device,TagPrefix\0"
	"G:1,root,Tag Names,Push\0"
	"\0"
END

// End of File
