
#include "intern.hpp"

#include "rawser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Raw Serial Port Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CRawSerialDriverOptions, CUIItem);

// Constructor

CRawSerialDriverOptions::CRawSerialDriverOptions(void)
{
	m_pService = NULL;

	m_Mode = 0;

	m_Buffer = 4096;
	}

// UI Managament

void CRawSerialDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}


// Download Support

BOOL CRawSerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pService);

	Init.AddByte(BYTE(m_Mode));

	Init.AddWord(WORD(m_Buffer));

	return TRUE;
	}

// Meta Data Creation

void CRawSerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Service);

	Meta_AddInteger(Mode);

	Meta_AddInteger(Buffer);

	Meta_SetName(IDS_DRIVER_OPTIONS);
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw Serial Port
//

// Instantiator

ICommsDriver *	Create_RawSerialDriver(void)
{
	return New CRawSerialDriver;
	}

// Constructor

CRawSerialDriver::CRawSerialDriver(void)
{
	m_wID		= 0x3701;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Raw Serial Port";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Raw Serial";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Configuration

CLASS CRawSerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CRawSerialDriverOptions);
	}

// End of File
