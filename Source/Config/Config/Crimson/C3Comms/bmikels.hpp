
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BMIKELS_HPP
	
#define	INCLUDE_BMIKELS_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Master Device Options
//

class CBMikeLSMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBMikeLSMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Scan;
		UINT m_Timeout;
		UINT m_Auto;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Table Defs
//

#define T_BIT	1
#define T_BYTE	2
#define T_WORD	3
#define T_LONG	4
#define T_REAL	5
#define T_STR	6

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define ID_G	200
#define ID_I	201
#define ID_T	301

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Master Driver
//

class CBMikeLSMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBMikeLSMasterDriver(void);

		//Destructor
		~CBMikeLSMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Data Access
		UINT GetGroup(CAddress const &Addr);
		UINT GetItem(CAddress const &Addr);
		UINT GetGroupIndex(CAddress const &Addr);
		UINT GetItemIndex(CAddress const &Addr);
		
	protected:
		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Master Ethernet Device Options
//

class CBMikeLSEthernetMasterDeviceOptions : public CBMikeLSMasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBMikeLSEthernetMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_IP;
		UINT m_Port;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Ethernet Master Driver
//

class CBMikeLSEthernetMasterDriver : public CBMikeLSMasterDriver
{
	public:
		// Constructor
		CBMikeLSEthernetMasterDriver(void);

		//Destructor
		~CBMikeLSEthernetMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Address Selection Dialog
//

class CBMikeLSAddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CBMikeLSAddrDialog(CBMikeLSMasterDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Data
		CBMikeLSMasterDriver *	m_pDriver;
		CAddress *		m_pAddr;
		CItem *			m_pConfig;
		BOOL			m_fPart;
				
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Wnd, DWORD dwData);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadGroup(void);
		void LoadItem(void);
		void SetType(void);
		BOOL CheckInit(void);


	};

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Slave Device Options
//

class CBMikeLSSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBMikeLSSlaveDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
								
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Slave UDP Device Options
//

class CBMikeLSSlaveUdpDriverOptions : public CBMikeLSEthernetMasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBMikeLSSlaveUdpDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Final Length Slave Driver
//

class CBMikeLSFinalLenSlaveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBMikeLSFinalLenSlaveDriver(void);

		//Destructor
		~CBMikeLSFinalLenSlaveDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Final Length Slave Driver
//

class CBMikeLSFinalLenSlaveUdpDriver : public CBMikeLSFinalLenSlaveDriver
{
	public:
		// Constructor
		CBMikeLSFinalLenSlaveUdpDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
		
	};


#endif

// End of File