
#include "intern.hpp"

#include "iddcon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ICP DAS Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIcpDasDeviceOptions, CUIItem);
				   
// Constructor

CIcpDasDeviceOptions::CIcpDasDeviceOptions(void)
{
	m_Drop = 1;

	m_fChecksum = FALSE;
	}

// UI Managament

void CIcpDasDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CIcpDasDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_fChecksum));

	return TRUE;
	}

// Meta Data Creation

void CIcpDasDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddBoolean(Checksum);
	}

//////////////////////////////////////////////////////////////////////////
//
// ICP DAS DCON IO Module Driver
//

// Instantiator

ICommsDriver * Create_IcpDasDconDriver(void)
{
	return New CIcpDasDconDriver;
	}

// Constructor

CIcpDasDconDriver::CIcpDasDconDriver(void)
{
	m_wID		= 0x4074;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "ICP DAS";
	
	m_DriverName	= "DCON Serial Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "DCON";

	AddSpaces();
	}

// Binding Control

UINT CIcpDasDconDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CIcpDasDconDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}


// Configuration

CLASS CIcpDasDconDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIcpDasDeviceOptions);
	}

// Implementation

void CIcpDasDconDriver::AddSpaces(void)
{
	AddSpace(New CSpace(        1, "AOx",	"Channel Analog Output Data",	10,  0, 15, addrRealAsReal));
	AddSpace(New CSpace(	    2, "AIx",	"Channel Analog Input Data",	10,  0, 15, addrRealAsReal));
	
	AddSpace(New CSpace(        3, "DOx",	"Digital Output Data",		10,  0, 15, addrBitAsBit));
	AddSpace(New CSpace(	    4, "DIx",	"Digital Input Data",		10,  0, 15, addrBitAsBit));
	AddSpace(New CSpace(	    5, "DCx",   "Digital Input Counter",	10,  0, 15, addrWordAsWord));
	
	AddSpace(New CSpace(	    6, "SVx",	"Channel Safe Value",		10,  0,  3, addrRealAsReal));
	AddSpace(New CSpace(	    7, "PVx",	"Channel PowerOn Value",		10,  0,  3, addrRealAsReal));

	AddSpace(New CSpace(	    8, "SSx",	"Set Channel Safe Value",	10,  0,  3, addrBitAsBit));
	AddSpace(New CSpace(	    9, "SPx",	"Set Channel PowerOn Value",	10,  0,  3, addrBitAsBit));

	AddSpace(New CSpace(addrNamed, "AO",	"Single Analog Output",		10, 17, 17, addrRealAsReal));
	AddSpace(New CSpace(addrNamed, "AI",	"Single Analog Input",		10, 18, 18, addrRealAsReal));
	
	AddSpace(New CSpace(addrNamed, "LL",	"DI Low Latched Status",	10, 11, 11, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "HL",    "DI High Latched Status",	10, 12, 12, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "CL",	"Clear Latched DI Status",	10, 13, 13, addrBitAsBit));

	AddSpace(New CSpace(addrNamed, "Addr",	"Module Address",		10,  1,  1, addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "Baud",	"Module Baudrate",		10,  2,  2, addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "CDir",	"Counter Update Direction",	10,  3,  3, addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "SumE",	"Checksum Enabled",		10,  4,  4, addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "Code",  "Type Code",			10, 16, 16, addrByteAsByte));

	AddSpace(New CSpace(addrNamed, "WHOK",	"Watchdog Host OK",		10,  5,  5, addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "WdgS",	"Watchdog Module Status",	10,  6,  6, addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "WdgT",	"Watchdog Timeout",		10,  7,  7, addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "WdgE",  "Watchdog Timeout Enable",	10,  8,  8, addrBitAsBit));

	AddSpace(New CSpace(addrNamed, "PVal",	"PowerOn Value",		10,  9,  9, addrLongAsLong, addrLongAsReal));
	AddSpace(New CSpace(addrNamed, "SVal",  "Safe Value",			10, 10, 10, addrLongAsLong, addrLongAsReal));

	AddSpace(New CSpace(addrNamed, "PSet",  "Set Digital PowerOn Value",	10, 14, 14, addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "SSet",  "Set Digital Safe Value",	10, 15, 15, addrBitAsBit));
	
	AddSpace(New CSpace(addrNamed, "AnaP",  "Set Single Analog PowerOn",	10, 19, 19, addrBitAsBit));
	}
 
// End of File
