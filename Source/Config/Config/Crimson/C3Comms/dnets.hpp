
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DNETS_HPP
	
#define	INCLUDE_DNETS_HPP

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave Driver Options
//

class CDeviceNetSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDeviceNetSlaveDriverOptions(void);

		// Persistance
		void Save(CTreeFile &Tree);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT	m_Mac;
		BOOL	m_fBit;
		UINT	m_BitRecv;
		UINT	m_BitSend;
		BOOL	m_fPoll;
		UINT	m_PollRecv;
		UINT	m_PollSend;
		BOOL	m_fData;
		UINT	m_DataRecv;
		UINT	m_DataSend;
		UINT	m_fIntel;
		CString m_Profile;
		BOOL    m_fExport;
				
	protected:
		// Data
		BOOL           m_fDirty;
		ITextStream  * m_pStream;

		// Meta Data Creation
		void AddMetaData(void);

		// EDS File
		void CreateEDSFile(void);
		void WriteEDSHead(void);
		void WriteEDSFile(void);
		void WriteEDSDev(void);
		void WriteEDSInfo(void);
		void WriteEDSAssy(void); 
		void WriteIO(CString p, UINT i, UINT s, UINT b, UINT m, CString n, CString t);
		void WriteAssy(UINT i, CString d, CString p, UINT s) ;
		BOOL WriteFile(PCTXT pText);
		BOOL WriteFile(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave 1.x
//

class CDeviceNetSlave : public CStdCommsDriver
{
	public:
		// Constructor
		CDeviceNetSlave(void);

		// Configuration
		CLASS GetDriverConfig(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave 2.x
//

class CDeviceNetSlave2 : public CStdCommsDriver
{
	public:
		// Constructor
		CDeviceNetSlave2(void);

		// Configuration
		CLASS GetDriverConfig(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
