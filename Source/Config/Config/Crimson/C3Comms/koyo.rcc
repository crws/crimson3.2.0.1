
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

/*
LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "koyo.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "koyo.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "koyo.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "koyo.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
*/

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CKoyoDeviceOptions
//

CKoyoDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|255,"
	"Select the drop number of the device to be addressed."
	"\0"
      
	"PingEnable,Enable Ping,,CUIDropDown,No|Yes,"
	"Enabling the ping allows the driver to quickly detect an offline device.  "
	"\0" 
	
	"PingReg,Ping Data (V) Register,,CUIEditInteger,|0||0|177777,"
	"Select the Data Register that should be used to ping the device."
	"\0"
       
	"\0"
END

CKoyoDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Settings,Drop,PingEnable,PingReg\0"
	"\0"
END


//////////////////////////////////////////////////////////////////////////
//
// UI for CKoyoDirectNetDriverOptions
//

CKoyoDirectNetDriverOptionsUIList RCDATA
BEGIN
	"MasterID,Master ID,,CUIEditInteger,|0||0|1,"
	"\0" 

	"\0"
END

CKoyoDirectNetDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Driver Settings,MasterID\0"
	"\0"
END


/////////////////////////////////////////////////////////////////////////
//
// UI for CKoyoUDPDeviceOptions
//

CKoyoMasterUDPDeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the Koyo device."
	"\0"

	"Socket,Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the port number on which the Koyo protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the Koyo driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the Koyo server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a Koyo request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CKoyoMasterUDPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Socket\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END

/////////////////////////////////////////////////////////////////////////
//
// UI for CKoyoPingMasterUDPDeviceOptions
//

CKoyoPingMasterUDPDeviceOptionsUIList RCDATA
BEGIN
	"PingEnable,Enable Ping,,CUIDropDown,No|Yes,"
	"Enabling the ping allows the driver to quickly detect an offline device.  "
	"\0" 
	
	"PingReg,Ping Data (V) Register,,CUIEditInteger,|0||0|177777,"
	"Select the Data Register that should be used to ping the device."
	"\0"

	"\0"
END

CKoyoPingMasterUDPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Socket\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2\0"
	"G:1,root,Device Detection,PingEnable,PingReg\0"
	"\0"
END



//////////////////////////////////////////////////////////////////////////
//
// Koyo EBC Element Dialog
//
//

KoyoEbcElementDlg DIALOG 0, 0, 0, 0
CAPTION ""
BEGIN
	EDITTEXT		2005,	 0, 0, 22, 12, 
	CTEXT		":",	2004,	25, 1, 14, 12
	EDITTEXT		2002,	42, 0, 26, 12,
END



// End of File
