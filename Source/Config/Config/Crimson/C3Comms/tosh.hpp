
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TOSH_HPP
	
#define	INCLUDE_TOSH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CToshT2Driver;
class CToshT2Dialog;

#define	SERIEST		0
#define	SERIESEX	1
#define	SERIESS		2

// Space Definitions
#define	SPACE_D		1
#define	SPACE_F		11
#define	SPACE_XW	2
#define	SPACE_YW	3
#define	SPACE_C		4
#define	SPACE_R		5
#define	SPACE_Z		6
#define	SPACE_RW	7
#define	SPACE_ZW	8
#define	SPACE_T		9
#define	SPACE_CT	10

//////////////////////////////////////////////////////////////////////////
//
// Toshiba PLC Device Options
//

class CToshT2MasterSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CToshT2MasterSerialDeviceOptions(void);
				
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_Drop;
		UINT m_Model;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 Master Serial Driver - Supports Toshiba T-Series and EX-Series PLCs.
//
//

class CToshT2MasterSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CToshT2MasterSerialDriver(void);

		// Destructor
		~CToshT2MasterSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		UINT	GetModel(CItem *pConfig);
		CString GetSuffix(UINT uIndex);
		BOOL	IsExtra(UINT uTable);
		UINT	GetExtra(CString Suffix);
		BOOL	CheckAlignment(CSpace *pSpace);
		void	SetTypeSelection(UINT uType);

       
	protected:
		// Data
		UINT	m_uSelectedType;

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 Master Address Selection Dialog - Supports Toshiba T-Series and EX-Series PLCs.
//

class CToshT2AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CToshT2AddrDialog(CToshT2MasterSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		BOOL	AllowSpace(CSpace *pSpace);
		BOOL	AllowType(UINT uType);
	};




//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 PLC UDP/IP Master
//

class CToshT2UDPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CToshT2UDPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS GetDeviceConfig(void); 

	protected:
		// Implementation     
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 PLC UDP/IP  Device Options
//

class CToshT2UDPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CToshT2UDPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_Addr;
		UINT m_Port;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
				
	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Toshiba EX 40 + PLC Serial Master
//

class CToshExPlusMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CToshExPlusMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void); 

	protected:
		// Implementation     
		void AddSpaces(void);
	};
   

// End of File


#endif
