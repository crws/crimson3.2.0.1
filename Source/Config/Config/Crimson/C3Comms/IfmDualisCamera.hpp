
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DUALISCAMERADRIVER_HPP
	
#define	INCLUDE_DUALISCAMERADRIVER_HPP

//////////////////////////////////////////////////////////////////////////
//
// ifm effector Dualis Vision Sensor Camera Driver
//

class CIfmDualisCamera : public CBasicCommsDriver
{
	public:
		// Constructor
		CIfmDualisCamera(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
