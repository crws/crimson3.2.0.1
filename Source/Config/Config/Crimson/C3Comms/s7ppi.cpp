
#include "intern.hpp"

#include "s7ppi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via PPI
//

// Instantiator

ICommsDriver *	Create_S7PPIDriver(void)
{
	return New CS7PPIDriver;
	}

// Constructor

CS7PPIDriver::CS7PPIDriver(void)
{
	m_wID		= 0x3327;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 via PPI";
	
	m_Version	= "1.02";
	
	m_ShortName	= "S7 PPI";

	AddSpaces();
	}

// Binding

void CS7PPIDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CS7PPIDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "VW",	"Variable Memory",	10, 0, 9999, addrByteAsWord, addrByteAsReal));
	AddSpace(New CSpace(2,  "IW",	"Input Images",		10, 0, 9999, addrByteAsWord/*, addrByteAsReal*/));
	AddSpace(New CSpace(3,  "QW",	"Output Images",	10, 0, 9999, addrByteAsWord/*, addrByteAsReal*/));
	AddSpace(New CSpace(4,  "MW",	"Bit Memory",		10, 0, 9999, addrByteAsWord, addrByteAsReal));
	AddSpace(New CSpace(5,  "AIW",	"Analog Inputs",	10, 0, 9999, addrByteAsWord));
	AddSpace(New CSpace(6,  "AQW",	"Analog Outputs",	10, 0, 9999, addrByteAsWord));
	AddSpace(New CSpace(7,  "T",	"Timers",		10, 0, 9999, addrWordAsWord));
	AddSpace(New CSpace(8,  "C",	"Counters",		10, 0, 9999, addrWordAsWord));

	/** NOTE:  IW and QW registers have been successfully tested as longs, however, to avoid customer
		   confusion due to already released IW and QW byte swapping, IW and QW longs are omitted. **/
	}


//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via PPI Revision 2
//

// Instantiator

ICommsDriver *	Create_S7PPI2Driver(void)
{
	return New CS7PPI2Driver;
	}

// Constructor

CS7PPI2Driver::CS7PPI2Driver(void)
{
	m_wID		= 0x339E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 via PPI";
	
	m_Version	= "2.00";
	
	m_ShortName	= "S7 PPI";

	AddSpaces();
	}

// Binding

void CS7PPI2Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CS7PPI2Driver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	if( Text[1] == 'W' ) {

		if( Text.Find(TEXT(".WORD")) == NOTHING ) {

			Text += ".WORD";
			}
		
		Text.SetAt(1, 'x');
		}

	return CStdCommsDriver::ParseAddress(Error, Addr, pConfig, Text);
	}

// Address Helpers

BOOL CS7PPI2Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace->m_uTable >= 12 && pSpace->m_uTable <= 14 ) {

		UINT uDot = Text.FindRev(':');

		if( uDot == NOTHING ) {

			Error.Set( "Bit level references must include a bit number.",
				   0
				   );

			return FALSE;
			}

		UINT uByte = tatoi(Text);

		UINT uBit  = tatoi(Text.Mid(uDot+1));

		Addr.a.m_Type   = addrBitAsBit;

		Addr.a.m_Offset = uByte * 8 + uBit;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Table  = pSpace->m_uTable;

		return TRUE;
		}

	return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
	}

BOOL CS7PPI2Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		if( pSpace->m_uTable >= 12 && pSpace->m_uTable <= 14 ) {

			UINT uAddr = Addr.a.m_Offset;

			UINT uByte = uAddr / 8;

			UINT uBit  = uAddr % 8;

			Text.Printf("%s%u:%u", pSpace->m_Prefix, uByte, uBit);

			return TRUE;
			}

		return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
		}
	
	return FALSE;
	}

// Implementation

void CS7PPI2Driver::AddSpaces(void)
{
	AddSpace(New CSpace( 1,  "Vx",	"Variable Memory",	10, 0,  9999, addrByteAsByte,addrByteAsReal));
	
	AddSpace(New CSpace( 2,  "Ix",	"Input Images",		10, 0,  9999, addrByteAsByte,addrByteAsReal));

	AddSpace(New CSpace(12,  "I",	"Input Images",		10, 0, 39999, addrBitAsBit));
	
	AddSpace(New CSpace( 3,  "Qx",	"Output Images",	10, 0,  9999, addrByteAsByte,addrByteAsReal));

	AddSpace(New CSpace(13,  "Q",	"Output Images",	10, 0, 39999, addrBitAsBit));
	
	AddSpace(New CSpace( 4,  "Mx",	"Bit Memory",		10, 0,  9999, addrByteAsByte,addrByteAsReal));

	AddSpace(New CSpace(14,  "M",	"Bit Memory",		10, 0, 39999, addrBitAsBit));
	
	AddSpace(New CSpace( 5,  "AIW",	"Analog Inputs",	10, 0,  9999, addrByteAsWord));
	
	AddSpace(New CSpace( 6,  "AQW",	"Analog Outputs",	10, 0,  9999, addrByteAsWord));
	
	AddSpace(New CSpace( 7,  "T",	"Timers",		10, 0,  9999, addrWordAsWord));
	
	AddSpace(New CSpace( 8,  "C",	"Counters",		10, 0,  9999, addrWordAsWord));
	}

BOOL CS7PPI2Driver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}


// End of File
