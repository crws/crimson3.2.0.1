
#include "intern.hpp"

#include "ps830.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific 830 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CPacSci830DeviceOptions, CUIItem);
				   
// Constructor

CPacSci830DeviceOptions::CPacSci830DeviceOptions(void)
{
	m_Drop		= 255;

	}

// UI Managament

void CPacSci830DeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	
	}

// Download Support

BOOL CPacSci830DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CPacSci830DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	}


//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific 830 Driver
//

// Instantiator

ICommsDriver *	Create_PacSci830SerialDriver(void)
{
	return New CPacSci830SerialDriver;
	}

// Constructor

CPacSci830SerialDriver::CPacSci830SerialDriver(void)
{
	m_wID		= 0x3373;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Pacific Scientific";
			
	m_DriverName	= "830 Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "830";	

	AddSpaces();

	}

// Binding Control

UINT CPacSci830SerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CPacSci830SerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CPacSci830SerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CPacSci830DeviceOptions);
	}



// Implementation     

void CPacSci830SerialDriver::AddSpaces(void)
{	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AccelLmt,		"AccelLmt",		"001",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ActiveAccelRate,	"ActiveAccelRate",	"002",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ActiveDecelRate,	"ActiveDecelRate",	"003",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveDistance,	"ActiveDistance",	"004",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveDistOffset,	"ActiveDistOffset",	"005",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveDwell,		"ActiveDwell",		"006",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveHomeDir,	"ActiveHomeDir",	"007",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveHomeMode,	"ActiveHomeMode",	"008",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveMove,		"ActiveMove",		"009",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveMoveType,	"ActiveMoveType",	"010",	addrLongAsLong, TRUE));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ActiveRegSelect,	"ActiveRegSelect",	"011",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ActiveRunSpeed,	"ActiveRunSpeed",	"012",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ActualILmtMinus,	"ActualILmtMinus",	"013",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ActualILmtPlus,	"ActualILmtPlus",	"014",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ADF0,		"ADF0",			"015",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ADOffset,		"ADOffset",		"016",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_AIn1Map,		"AIn1Map",		"017",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_AIn2Map,		"AIn2Map",		"018",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_AIn3Map,		"AIn3Map",		"019",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_AInNull,		"AInNull",		"020",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AnalogILmt,	"AnalogILmt",		"021",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AnalogILmtFilt,	"AnalogILmtFilt",	"022",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AnalogILmtGain,	"AnalogILmtGain",	"023",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AnalogILmtOffset,	"AnalogILmtOffset",	"024",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AnalogIn,		"AnalogIn",		"025",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AnalogOut1,	"AnalogOut1",		"026",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_AnalogOut2,	"AnalogOut2",		"027",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ARF0,		"ARF0",			"028",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ARF1,		"ARF1",			"029",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ARZ0,		"ARZ0",			"030",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ARZ1,		"ARZ1",			"031",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_AxisAddr,		"AxisAddr",		"032",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_B1,		"B1",			"033",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_B2,		"B2",			"034",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_BlkType,		"BlkType",		"035",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Brake,		"Brake",		"036",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_CCDate,		"CCDate",		"037",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_CCSNum,		"CCSNum",		"038",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_CcwInh,		"CcwInh",		"039",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_CfgD,		"CfgD",			"040",	addrLongAsLong, TRUE));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_CmdGain,		"CmdGain",		"041",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_CmdGain2,		"CmdGain2",		"042",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_CommEnbl,		"CommEnbl",		"043",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_CommOff,		"CommOff",		"044",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_CommSrc,		"CommSrc",		"045",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H1,	"CosHarmCoeff1",	"046",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H10,	"CosHarmCoeff10",	"047",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H11,	"CosHarmCoeff11",	"048",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H13,	"CosHarmCoeff13",	"049",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H14,	"CosHarmCoeff14",	"050",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H16,	"CosHarmCoeff16",	"051",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H17,	"CosHarmCoeff17",	"052",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H19,	"CosHarmCoeff19",	"053",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H2,	"CosHarmCoeff2",	"054",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H4,	"CosHarmCoeff4",	"055",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H5,	"CosHarmCoeff5",	"056",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H7,	"CosHarmCoeff7",	"057",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_COS_COEFF_H8,	"CosHarmCoeff8",	"058",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_CwInh,		"CwInh",		"059",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DBG1,		"DBG1",			"060",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DBG2,		"DBG2",			"061",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DBG4,		"DBG4",			"062",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DBGI,		"DbgI",			"063",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DBGL,		"DbgL",			"064",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar0,		"DbgVar0",		"065",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar1,		"DbgVar1",		"066",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar2,		"DbgVar2",		"067",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar3,		"DbgVar3",		"068",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar4,		"DbgVar4",		"069",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar5,		"DbgVar5",		"070",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar6,		"DbgVar6",		"071",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DbgVar7,		"DbgVar7",		"072",	addrLongAsLong));
       	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DecelLmt,		"DecelLmt",		"073",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DigitalCmd,		"DigitalCmd",		"074",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DigitalCmdFreq,	"DigitalCmdFreq",	"075",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DM1F0,		"DM1F0",		"076",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DM1Gain,		"DM1Gain",		"077",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DM1Map,		"DM1Map",		"078",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DM1Out,		"DM1Out",		"079",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DM2F0,		"DM2F0",		"080",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DM2Gain,		"DM2Gain",		"081",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DM2Map,		"DM2Map",		"082",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_DM2Out,		"DM2Out",		"083",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_DriveStatus,		"DriveStatus",		"084",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EEInt,		"EEInt",		"085",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ElecAngTau,		"ElecAngTau",		"086",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Enable,		"Enable",		"087",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Enable2,		"Enable2",		"088",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Enabled,		"Enabled",		"089",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EncAlignRampIcmd,	"EncAlignRampIcmd",	"090",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EncAlignTestDist,	"EncAlignTestDist",	"091",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EncAlignTime,	"EncAlignTime",		"092",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_EncFreq,		"EncFreq",		"093",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EncIn,		"EncIn",		"094",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_EncInF0,		"EncInF0",		"095",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EncMode,		"EncMode",		"096",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EncOut,		"EncOut",		"097",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_EncPos,		"EncPos",		"098",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ExtFault,		"ExtFault",		"099",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Fault,		"Fault",		"100",	addrLongAsLong, TRUE));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_FaultCode,		"FaultCode",		"101",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_FaultReset,		"FaultReset",		"102",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_FVelErr,		"FVelErr",		"103",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_FwV,			"FwV",			"104",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_GearingOn,		"GearingOn",		"105",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_HallOffset,		"HallOffset",		"106",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_HallState,		"HallState",		"107",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_HomeSwitch,		"HomeSwitch",		"108",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_HSTemp,		"HSTemp",		"109",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_HwV,			"HwV",			"110",	addrLongAsLong, TRUE));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_I2tF0,		"I2tF0",		"111",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_I2tFilt,		"I2tFilt",		"112",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_I2tThresh,	"I2tThresh",		"113",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ICmd,		"ICmd",			"114",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IFB,		"IFB",			"115",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ILmtMinus,		"ILmtMinus",		"116",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ILmtMode,		"ILmtMode",		"117",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ILmtPlus,		"ILmtPlus",		"118",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ImMax,		"ImMax",		"119",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Inp1,		"Inp1",			"120",	addrLongAsLong, TRUE));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Inp2,		"Inp2",			"121",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Inp3,		"Inp3",			"122",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Inp4,		"Inp4",			"123",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Inp5,		"Inp5",			"124",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Inp6,		"Inp6",			"125",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_InpMap1,		"InpMap1",		"126",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_InpMap2,		"InpMap2",		"127",	addrLongAsLong)); 
	AddSpace(New CSpacePacSci830(addrNamed,	MI_InpMap3,		"InpMap3",		"128",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_InpMap4,		"InpMap4",		"129",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_InpMap5,		"InpMap5",		"130",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_InpMap6,		"InpMap6",		"131",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_InPosLimit,		"InPosLimit",		"132",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Inputs,		"Inputs",		"133",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IntgStopThresh,	"IntgStopThresh",	"134",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Ipeak,		"Ipeak",		"135",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IqCmd,		"IqCmd",		"136",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IqFB,		"IqFB",			"137",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IqIH,		"IqIH",			"138",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IqMax,		"IqMax",		"139",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ITDerateIntercept,"ItDerateIntercept",	"140",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ITDerateSlope,	"ItDerateSlope",	"141",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ItF0,		"ItF0",			"142",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ItFilt,		"ItFilt",		"143",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ItThresh,		"ItThresh",		"144",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ItThreshA,	"ItThreshA",		"145",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IU,		"IU",			"146",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IV,		"IV",			"147",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_IW,		"IW",			"148",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_K1,		"K1",			"149",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_K2,		"K2",			"150",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_KdEnc,		"KdEnc",		"151",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_KiEnc,		"KiEnc",		"152",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Kii,		"Kii",			"153",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Kip,		"Kip",			"154",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_KpEnc,		"KpEnc",		"155",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Kpp,		"Kpp",			"156",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Kvff,		"Kvff",			"157",	addrLongAsReal));
       	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Kvi,		"Kvi",			"158",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Kvp,		"Kvp",			"159",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	CM+MC_LoadCalTable,	"LoadCalTable",		"160",	addrBitAsBit));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_MfgLock,		"MfgLock",		"161",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Model,		"Model",		"162",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Motor1,		"Motor1",		"163",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Motor2,		"Motor2",		"164",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move0AccelRate,	"Move0AccelRate",	"165",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move0DecelRate,	"Move0DecelRate",	"166",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move0Distance,	"Move0Distance",	"167",	addrLongAsLong));
       	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move0DistOffset,	"Move0DistOffset",	"168",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move0Dwell,		"Move0Dwell",		"169",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move0HomeDir,	"Move0HomeDir",		"170",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move0HomeMode,	"Move0HomeMode",	"171",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move0RegSelect,	"Move0RegSelect",	"172",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move0RunSpeed,	"Move0RunSpeed",	"173",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move0Type,		"Move0Type",		"174",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move1AccelRate,	"Move1AccelRate",	"175",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move1DecelRate,	"Move1DecelRate",	"176",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move1Distance,	"Move1Distance",	"177",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move1DistOffset,	"Move1DistOffset",	"178",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move1Dwell,		"Move1Dwell",		"179",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move1HomeDir,	"Move1HomeDir",		"180",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move1HomeMode,	"Move1HomeMode",	"181",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move1RegSelect,	"Move1RegSelect",	"182",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move1RunSpeed,	"Move1RunSpeed",	"183",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move1Type,		"Move1Type",		"184",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move2AccelRate,	"Move2AccelRate",	"185",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move2DecelRate,	"Move2DecelRate",	"186",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move2Distance,	"Move2Distance",	"187",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move2DistOffset,	"Move2DistOffset",	"188",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move2Dwell,		"Move2Dwell",		"189",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move2HomeDir,	"Move2HomeDir",		"190",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move2HomeMode,	"Move2HomeMode",	"191",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move2RegSelect,	"Move2RegSelect",	"192",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move2RunSpeed,	"Move2RunSpeed",	"193",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move2Type,		"Move2Type",		"194",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move3AccelRate,	"Move3AccelRate",	"195",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move3DecelRate,	"Move3DecelRate",	"196",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move3Distance,	"Move3Distance",	"197",	addrLongAsLong));
       	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move3DistOffset,	"Move3DistOffset",	"198",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move3Dwell,		"Move3Dwell",		"199",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move3HomeDir,	"Move3HomeDir",		"200",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move3HomeMode,	"Move3HomeMode",	"201",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move3RegSelect,	"Move3RegSelect",	"202",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move3RunSpeed,	"Move3RunSpeed",	"203",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move3Type,		"Move3Type",		"204",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move4AccelRate,	"Move4AccelRate",	"205",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move4DecelRate,	"Move4DecelRate",	"206",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move4Distance,	"Move4Distance",	"207",	addrLongAsLong));
       	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move4DistOffset,	"Move4DistOffset",	"208",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move4Dwell,		"Move4Dwell",		"209",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move4HomeDir,	"Move4HomeDir",		"210",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move4HomeMode,	"Move4HomeMode",	"211",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move4RegSelect,	"Move4RegSelect",	"212",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move4RunSpeed,	"Move4RunSpeed",	"213",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move4Type,		"Move4Type",		"214",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move5AccelRate,	"Move5AccelRate",	"215",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move5DecelRate,	"Move5DecelRate",	"216",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move5Distance,	"Move5Distance",	"217",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move5DistOffset,	"Move5DistOffset",	"218",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move5Dwell,		"Move5Dwell",		"219",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move5HomeDir,	"Move5HomeDir",		"220",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move5HomeMode,	"Move5HomeMode",	"221",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move5RegSelect,	"Move5RegSelect",	"222",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move5RunSpeed,	"Move5RunSpeed",	"223",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move5Type,		"Move5Type",		"224",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move6AccelRate,	"Move6AccelRate",	"225",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move6DecelRate,	"Move6DecelRate",	"226",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move6Distance,	"Move6Distance",	"227",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move6DistOffset,	"Move6DistOffset",	"228",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move6Dwell,		"Move6Dwell",		"229",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move6HomeDir,	"Move6HomeDir",		"230",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move6HomeMode,	"Move6HomeMode",	"231",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move6RegSelect,	"Move6RegSelect",	"232",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move6RunSpeed,	"Move6RunSpeed",	"233",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move6Type,		"Move6Type",		"234",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move7AccelRate,	"Move7AccelRate",	"235",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move7DecelRate,	"Move7DecelRate",	"236",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move7Distance,	"Move7Distance",	"237",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move7DistOffset,	"Move7DistOffset",	"238",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move7Dwell,		"Move7Dwell",		"239",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move7HomeDir,	"Move7HomeDir",		"240",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move7HomeMode,	"Move7HomeMode",	"241",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move7RegSelect,	"Move7RegSelect",	"242",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Move7RunSpeed,	"Move7RunSpeed",	"243",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Move7Type,		"Move7Type",		"244",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_MoveDone,		"MoveDone",		"245",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_MoveSelectBit0,	"MoveSelectBit0",	"246",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_MoveSelectBit1,	"MoveSelectBit1",	"247",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_MoveSelectBit2,	"MoveSelectBit2",	"248",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	CM+MC_NVLoad,		"NVLoad",		"249",	addrBitAsBit));
	AddSpace(New CSpacePacSci830(addrNamed,	CM+MC_NVSave,		"NVSave",		"250",	addrBitAsBit));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Out1,		"Out1",			"251",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Out2,		"Out2",			"252",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Out3,		"Out3",			"253",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Out4,		"Out4",			"254",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_OutMap1,		"OutMap1",		"255",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_OutMap2,		"OutMap2",		"256",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_OutMap3,		"OutMap3",		"257",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_OutMap4,		"OutMap4",		"258",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Outputs,		"Outputs",		"259",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PoleCount,		"PoleCount",		"260",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PosCmdSet,		"PosCmdSet",		"261",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PosCommand,		"PosCommand",		"262",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PosError,		"PosError",		"263",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PosErrorMax,		"PosErrorMax",		"264",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Position,		"Position",		"265",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PulsesFOut,		"PulsesFOut",		"266",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PulsesIn,		"PulsesIn",		"267",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_PulsesOut,		"PulsesOut",		"268",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_PWMDeadBand,	"PWMDeadBand",		"269",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_PWMFreq,		"PWMFreq",		"270",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_RCalData,		"RCalData",		"271",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_RCalMAng,		"RCalMAng",		"272",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_RCalMode,		"RCalMode",		"273",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_RCalTime,		"RCalTime",		"274",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Reg1ActiveEdge,	"Reg1ActiveEdge",	"275",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Reg1EncoderPosition,	"Reg1EncoderPosition",	"276",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Reg1ResolverPosition,"Reg1ResolverPosition",	"277",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Reg2ActiveEdge,	"Reg2ActiveEdge",	"278",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Reg2EncoderPosition,	"Reg2EncoderPosition",	"279",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_Reg2ResolverPosition,"Reg2ResolverPosition",	"280",	addrLongAsLong, TRUE));
	
	AddSpace(New CSpacePacSci830(addrNamed,	MI_RemoteFB,		"RemoteFB",		"281",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_ResPos,		"ResPos",		"282",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_RunStop,		"RunStop",		"283",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	CM+MC_SaveCalTable,	"SaveCalTable",		"284",	addrBitAsBit));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_SIAV,		"SIAV",			"285",	addrLongAsLong, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H1,	"SinHarmCoeff1",	"286",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H10,	"SinHarmCoeff10",	"287",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H11,	"SinHarmCoeff11",	"288",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H13,	"SinHarmCoeff13",	"289",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H14,	"SinHarmCoeff14",	"290",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H16,	"SinHarmCoeff16",	"291",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H17,	"SinHarmCoeff17",	"292",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H19,	"SinHarmCoeff19",	"293",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H2,	"SinHarmCoeff2",	"294",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H4,	"SinHarmCoeff4",	"295",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H5,	"SinHarmCoeff5",	"296",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H7,	"SinHarmCoeff7",	"297",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_SIN_COEFF_H8,	"SinHarmCoeff8",	"298",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_StartMove,		"StartMove",		"299",	addrLongAsLong));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_StopTime,		"StopTime",		"300",	addrLongAsReal));
	
	AddSpace(New CSpacePacSci830(addrNamed,	CM+MC_TableGen,		"TableGen",		"301",	addrBitAsBit));
	AddSpace(New CSpacePacSci830(addrNamed,	CM+MC_Unconfigure,	"Unconfigure",		"302",	addrBitAsBit));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VBus,		"VBus",			"303",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VBusFTime,	"VBusFTime",		"304",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VBusThresh,	"VBusThresh",		"305",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VdCmd,		"VdCmd",		"306",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VelCmd,		"VelCmd",		"307",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VelCmd2,		"VelCmd2",		"308",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VelCmdA,		"VelCmdA",		"309",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	MI_VelCmdSrc,		"VelCmdSrc",		"310",	addrLongAsLong));
	
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VelErr,		"VelErr",		"311",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VelFB,		"VelFB",		"312",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VelLmtHi,		"VelLmtHi",		"313",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VelLmtLo,		"VelLmtLo",		"314",	addrLongAsReal));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_Velocity,		"Velocity",		"315",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_VqCmd,		"VqCmd",		"316",	addrLongAsReal, TRUE));
	AddSpace(New CSpacePacSci830(addrNamed,	FL+MF_ZeroSpeedThresh,	"ZeroSpeedThresh",	"317",	addrLongAsReal));
	
	}

// Address Management

BOOL CPacSci830SerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CPacSci830AddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

BOOL CPacSci830SerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( StripType(pSpace, Text) ) {

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			CSpacePacSci830* pPacSci = (CSpacePacSci830 *) pSpace;

			Addr.a.m_Offset = pPacSci->m_Index;

			Addr.a.m_Extra  = pPacSci->m_ReadOnly;

			return TRUE;
			}
		}
	
	Error.Set( CString(IDS_DRIVER_ADDR_INVALID), 0 );

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific 830 Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CPacSci830AddrDialog, CStdAddrDialog);
		
// Constructor

CPacSci830AddrDialog::CPacSci830AddrDialog(CPacSci830SerialDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "";
	}
 
// Overridables

void CPacSci830AddrDialog::SetAddressText(CString Text)
{
	GetDlgItem(2002).ShowWindow(FALSE);

	GetDlgItem(2001).SetWindowSize(100,12, FALSE);
	
	CStdAddrDialog::SetAddressText(Text);
	}

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific 830 Space Wrapper Class
//

// Constructors

CSpacePacSci830::CSpacePacSci830(UINT t, UINT i, CString c, CString p, AddrType type, UINT n, UINT x, BOOL fRO): CSpace(t, p, c, 10, n, x, type)
{
	m_Index		= i;

	m_ReadOnly	= fRO;

	m_Prefix	= c;

	m_Caption	= "";

	if( m_uTable == addrNamed ) {

		m_uMinimum = m_Index;
		}
	}

// End of File
