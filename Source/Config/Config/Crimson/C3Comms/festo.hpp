
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_FESTO_HPP
	
#define	INCLUDE_FESTO_HPP

//////////////////////////////////////////////////////////////////////////
//
// Festo FPC, IPC, FEC Series Controller
//

class CFestoFPCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CFestoFPCDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);


	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
