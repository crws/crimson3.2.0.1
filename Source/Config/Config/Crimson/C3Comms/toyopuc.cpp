
#include "intern.hpp"

#include "toyopuc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2006 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CToyodaDeviceOptions, CUIItem);       

// Constructor

CToyodaDeviceOptions::CToyodaDeviceOptions(void)
{
	m_Drop    = 0;

	m_Program = 0;
	}

// UI Managament

void CToyodaDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CToyodaDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(LOBYTE(m_Drop));
	Init.AddByte(LOBYTE(m_Program));

	return TRUE;
	}

// Meta Data Creation

void CToyodaDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Program);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CToyodaTCPDeviceOptions, CUIItem);       

// Constructor

CToyodaTCPDeviceOptions::CToyodaTCPDeviceOptions(void)
{
	m_IPAddr  = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Program = 0;

	m_Port    = 502;

	m_Unit    = 1;

	m_Keep    = TRUE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;
	}

// UI Managament

void CToyodaTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CToyodaTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IPAddr));
	Init.AddByte(LOBYTE(m_Program));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CToyodaTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IPAddr);
	Meta_AddInteger(Program);
	Meta_AddInteger(Port);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Serial Driver
//

// Instantiator

ICommsDriver * Create_ToyodaPUCDriver(void)
{
	return New CToyodaPUCDriver;
	}

// Constructor

CToyodaPUCDriver::CToyodaPUCDriver(void)
{
	m_wID		= 0x4021;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Toyoda";
	
	m_DriverName	= "PUC Serial";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Toyoda PUC Serial";

	AddSpaces();
	}

// Binding Control

UINT CToyodaPUCDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CToyodaPUCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS422;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration
CLASS CToyodaPUCDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CToyodaDeviceOptions);
	}

// Address Management

BOOL CToyodaPUCDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CToyodaPUCAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CToyodaPUCDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	if( pSpace->m_uTable >= 40 ) { // no address/program field

		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Offset = 0;
		Addr.a.m_Type   = addrLongAsLong;
		Addr.a.m_Extra  = 0;
		return TRUE;
		}

	Text.MakeUpper();

	CString Type = StripType(pSpace, Text);

	CString sErr = "";

	UINT uProg = 0;

	BOOL fEnProgram = BOOL(pConfig->GetDataAccess("Program")->ReadInteger(pConfig));

	UINT uFind1  = Text.Find('(');
	UINT uFind2  = Text.Find(')');

	if( fEnProgram ) {

		if( uFind1 == NOTHING || uFind2 == NOTHING || uFind1 > uFind2 ) {

			sErr.Printf( "%s%aaaa(p)",
				pSpace->m_Prefix
				);

			Error.Set( sErr,
				0
				);

			return FALSE;
			}

		if( (uProg = Text[uFind1+1]) == 'p' ) {
			
			uProg = 0;
			}
		}
	else {
		if( uFind1 == NOTHING ) {
			
			uFind1 = Text.GetLength();
			}
		}

	BYTE    bByte   = BYTE(Text[uFind1 - 1]);

	CString Address = Text.Left(bByte == 'H' || bByte == 'L' ? uFind1 - 1 : uFind1);

	PTXT    pError  = NULL;

	UINT    uOffset = tstrtoul(Address, &pError, pSpace->m_uRadix);	

	if( pError && pError[0] ) {
			
		Error.Set( CString(IDS_ERROR_ADDR),
				   0
				   );
			
			return FALSE;
			}

	else {
		if( pSpace->IsOutOfRange(uOffset) ) {

			CString Prog = fEnProgram ? "(p)" : "";

			sErr.Printf( "%s%X%s - %s%X%s",
					pSpace->m_Prefix,
					pSpace->m_uMinimum,
					Prog,
					pSpace->m_Prefix,
					pSpace->m_uMaximum,
					Prog
					);
			
			Error.Set( sErr,
				   0
				   );
				
			return FALSE;
			}
		}

	Addr.a.m_Table	= pSpace->m_uTable;
	
	Addr.a.m_Extra	= uProg;

	Addr.a.m_Offset	= bByte == 'H' || bByte == 'h' ? uOffset | 0x8000 : uOffset;

	if( Type == "" ) {

		Addr.a.m_Type = pSpace->m_uType;

		return TRUE;
		}

	if( Type == "WORD" ) Addr.a.m_Type = addrWordAsWord;

	else {
		if( Type == "BYTE" ) Addr.a.m_Type = addrByteAsByte;

		else Addr.a.m_Type = addrBitAsBit;
		}
	
	return TRUE;
	}

BOOL CToyodaPUCDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpace * pSpace = GetSpace(Addr);

		UINT uT = pSpace->m_uTable;

		if( uT >= 40 ) {

			Text = pSpace->m_Prefix;

			return TRUE;
			}

		BOOL fEnProgram = BOOL(pConfig->GetDataAccess("Program")->ReadInteger(pConfig));

		if( pSpace ) {

			CString Prog = "";

			if( fEnProgram ) {

				if( Addr.a.m_Extra != 0xF ) {

					Prog.Printf( "(%1.1d)",
						Addr.a.m_Extra
						);
					}

				else Prog = "(p)";
				}

			if( pSpace->m_uType == Addr.a.m_Type ) {

				Text.Printf( "%s%4.4X%s",
					pSpace->m_Prefix,
					Addr.a.m_Offset,
					Prog
					);
				}

			else {
				if( Addr.a.m_Type != addrByteAsByte ) {

					Text.Printf( "%s%4.4X%s.%s",
						pSpace->m_Prefix,
						Addr.a.m_Offset,
						Prog,
						pSpace->GetTypeModifier(Addr.a.m_Type)
						);
					}

				else {
					Text.Printf( "%s%4.4X%c%s.BYTE",
						pSpace->m_Prefix,
						Addr.a.m_Offset & 0x7FFF,
						(Addr.a.m_Offset & 0x8000) ? 'H' : 'L',
						Prog
						);
					}
				}

			return TRUE;
			}
		}

	return FALSE;	
	}

// Implementation

void CToyodaPUCDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "X",  "Input",				16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(2,  "Y",  "Output",				16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(3,  "M",  "Internal Relay",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(4,  "K",  "Keep Relay",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(5,  "V",  "Special Relay",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(6,  "T",  "Timer",				16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(7,  "C",  "Counter",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(8,  "L",  "Link Relay",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(9,  "P",  "Edge Detection",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(10, "D",  "Data Register",			16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(11, "R",  "Link Register",			16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(12, "N",  "Timer/Counter Present Value",	16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(32, "Z",  "Timer/Counter Setup Value",	16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(13, "S",  "Special Register",		16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(14, "EX", "Extended Input",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(15, "EY", "Extended Output",		16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(16, "EM", "Extended Internal Relay",	16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(17, "EK", "Extended Keep Relay",		16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(18, "EV", "Extended Special Relay",		16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(19, "ET", "Extended Timer",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(20, "EC", "Extended Counter",		16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(21, "EL", "Extended Link Relay",		16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(22, "EP", "Extended Edge Detection",	16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));

// Extended T/C are not supported in the protocol.  Read/write are done via register data.
//	AddSpace(New CSpace(23, "EN", "Extended Tmr/Ctr Present Value",	16, 0, 0xFFFF, addrWordAsWord));
//	AddSpace(New CSpace(24, "H",  "Extended Tmr/Ctr Setup Value",	16, 0, 0xFFFF, addrWordAsWord));

	AddSpace(New CSpace(25, "ES", "Extended Special Register",	16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(26, "GX", "Extended Input",			16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(27, "GY", "Extended Output",		16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(28, "GM", "Extended Internal Relay",	16, 0, 0x7FFF, addrBitAsBit, addrWordAsWord));
	AddSpace(New CSpace(29, "U",  "Extended Data Register",		16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(30, "EB", "Extended Buffer Register",	16, 0, 0xFFFF, addrWordAsWord));
	AddSpace(New CSpace(31,	"B",  "File Register",			16, 0, 0xFFFF, addrWordAsWord));
// following selections have no address field
	AddSpace(New CSpace(40,	"MPE","CPU Status Extended Read",	16, 0, 0,      addrLongAsLong));
	AddSpace(New CSpace(41,	"WTL","Watch Time (Sec,Min,Hr,Date)",	16, 0, 0,      addrLongAsLong));
	AddSpace(New CSpace(42,	"WTH","Watch Time (Month,Year,Day)",	16, 0, 0,      addrLongAsLong));
	AddSpace(New CSpace(43,	"WTS","Send WTA+WTB",			16, 0, 0,      addrLongAsLong));
	AddSpace(New CSpace(44,	"WTA","  Store Time (Sec,Min,Hr,Date)",	16, 0, 0,      addrLongAsLong));
	AddSpace(New CSpace(45,	"WTB","  Store Time (Month,Year,Day)",	16, 0, 0,      addrLongAsLong));
	AddSpace(New CSpace(98,	"ERC","Error Code Command",		16, 0, 0,      addrLongAsLong));
	AddSpace(New CSpace(99,	"ERR","Error Code Value",		16, 0, 0,      addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_ToyodaTCPDriver(void)
{
	return New CToyodaTCPDriver;
	}

// Constructor

CToyodaTCPDriver::CToyodaTCPDriver(void)
{
	m_wID		= 0x3518;

	m_uType		= driverMaster;

	m_Manufacturer	= "Toyoda";

	m_DriverName	= "PUC TCP/IP";

	m_Version	= "1.00";

	m_ShortName	= "Toyoda PUC TCP/IP";

	AddSpaces();
	}

// Destructor

CToyodaTCPDriver::~CToyodaTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CToyodaTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CToyodaTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CToyodaTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CToyodaTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CToyodaTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CToyodaPUCAddrDialog, CStdAddrDialog);
		
// Constructor

CToyodaPUCAddrDialog::CToyodaPUCAddrDialog(CToyodaPUCDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element    = "ToyodaPUCElementDlg";
	m_fEnProgram = pConfig->GetDataAccess("Program")->ReadInteger(pConfig);
	}

// Message Map

AfxMessageMap(CToyodaPUCAddrDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(CToyodaPUCAddrDialog)
	};

// Message Handlers

BOOL CToyodaPUCAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	BOOL fOK = CStdAddrDialog::OnInitDialog( Focus, dwData );

	if( m_pSpace ) {

		LoadType();

		CAddress &Addr = (CAddress &) *m_pAddr;

		ShowAddress(Addr);

		ShowDetails();
		}

	else {
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2002).EnableWindow(FALSE);

		DoProgram( m_fEnProgram ? "0" : "" );

		DoCheck(FALSE);
		GetDlgItem(2007).EnableWindow(FALSE);
		}

	return fOK;
	}

void CToyodaPUCAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		LoadType();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			}
		}
	else {
		m_pSpace = NULL;

		ClearType();

		ClearAddress();

		ClearDetails();

		DoProgram( m_fEnProgram ? "0" : "" );

		DoCheck(FALSE);

		GetDlgItem(2007).EnableWindow(FALSE);
		}
	}

void CToyodaPUCAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		CStdAddrDialog::OnTypeChange(uID, Wnd);

		ShowDetails();
		}
	}

void CToyodaPUCAddrDialog::LoadType(void)
{
	if( !m_pSpace ) return;

	CListBox &ListBox = (CListBox &) GetDlgItem(4001);
	
	ListBox.SetRedraw(FALSE);

	ListBox.ResetContent();

	UINT uFind = max(m_pAddr->a.m_Type, m_pSpace->m_uType);

	if( m_pSpace->m_uType == addrLongAsLong ) {

		ListBox.AddString(m_pSpace->GetTypeAsText(addrLongAsLong),   addrLongAsLong);
		}

	else {
		if( m_pSpace->m_uType == addrBitAsBit ) {

			ListBox.AddString(m_pSpace->GetTypeAsText(addrBitAsBit),   addrBitAsBit);
			ListBox.AddString(m_pSpace->GetTypeAsText(addrByteAsByte), addrByteAsByte);
			}

		ListBox.AddString(m_pSpace->GetTypeAsText(addrWordAsWord), addrWordAsWord);
		}

	ListBox.SelectData(DWORD(uFind));

	ListBox.EnableWindow(TRUE);

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnTypeChange(4001, ListBox);
	}

void CToyodaPUCAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	if( !m_pDriver->ExpandAddress(Text, m_pConfig, Addr) ) return;

	UINT uPos = m_pSpace->m_Prefix.GetLength();

	BOOL fCheck = Addr.a.m_Type == addrByteAsByte;

	if( Addr.a.m_Table < 40 ) {

		GetDlgItem(2002).SetWindowText(Text.Mid(uPos, 4));
		GetDlgItem(2002).EnableWindow(TRUE);

		DoProgram(m_fEnProgram ? Text.Mid(uPos + 5 + (fCheck ? 1 : 0), 1) : "" );

		DoCheck( fCheck && (Addr.a.m_Offset & 0x8000) );

		GetDlgItem(2007).EnableWindow(fCheck);
		}

	else {
		GetDlgItem(2002).SetWindowText("");
		GetDlgItem(2002).EnableWindow(FALSE);
		GetDlgItem(2004).EnableWindow(FALSE);
		GetDlgItem(2005).EnableWindow(FALSE);
		GetDlgItem(2007).EnableWindow(FALSE);
		}
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);

	ShowType(Addr.a.m_Type);
	}

void CToyodaPUCAddrDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	CAddress Addr;

	Addr.a.m_Type = GetTypeCode();

	m_pSpace->GetMinimum(Addr);

	m_pDriver->ExpandAddress(Min, m_pConfig, Addr);

	m_pSpace->GetMaximum(Addr);

	Addr.a.m_Extra = m_fEnProgram ? 0xF : 0;

	if( Addr.a.m_Type == addrByteAsByte ) Addr.a.m_Offset |= 0x8000;

	m_pDriver->ExpandAddress(Max, m_pConfig, Addr);

	Rad = m_pSpace->GetRadixAsText();

	GetDlgItem(2007).EnableWindow(Addr.a.m_Type == addrByteAsByte);

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

CString CToyodaPUCAddrDialog::GetAddressText(void)
{
	CString Text;

	Text = GetDlgItem(2002).GetWindowText();

	if( GetTypeCode() == addrByteAsByte ) {

		CButton &Check = (CButton &) GetDlgItem(2007);

		Text += Check.IsChecked() ? 'H' : 'L';
		}

	if( m_fEnProgram ) {

		Text += "(";
		Text += GetDlgItem(2005).GetWindowText();
		Text += ")";
		}

	return Text;
	}

void CToyodaPUCAddrDialog::DoCheck(BOOL fCheck)
{
	CButton   &Check = (CButton &) GetDlgItem(2007);

	Check.SetCheck(fCheck);
	}

void CToyodaPUCAddrDialog::DoProgram(CString Program)
{
	GetDlgItem(2005).SetWindowText(Program);

	GetDlgItem(2005).EnableWindow(m_fEnProgram);

	GetDlgItem(2004).EnableWindow(m_fEnProgram);
	}

// End of File
