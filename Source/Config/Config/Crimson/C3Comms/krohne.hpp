
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DRVNAMED_HPP
	
#define	INCLUDE_DRVNAMED_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Short Type Names
//

#define	WW	addrWordAsWord
#define RR	addrRealAsReal
#define BB	addrByteAsByte
#define LL	addrLongAsLong

//////////////////////////////////////////////////////////////////////////
//
//  Krohne Device Options
//

class CKrohneDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKrohneDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Device;
		UINT m_Address;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Named Comms Driver
//

class CKrohneDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKrohneDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Device Options
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
