
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EUROINVENSYSMINI8_HPP
	
#define	INCLUDE_EUROINVENSYSMINI8_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CEuroInvensysMini8Driver;
class CEuroInvensysMini8SerialDriver;
class CEuroInvensysMini8TCPDeviceOptions;
class CEuroInvensysMini8TCPDriver;
class CEuroInvensysMini8AddrDialog;

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	WL	addrWordAsLong
#define	WR	addrWordAsReal
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Register Table Spaces
#define	SP_0	 1	// Modbus Digital Outputs  00000
#define	SP_1	 2	// Modbus Digital Inputs   10000
#define	SP_3	 3	// Modbus Read-Only Words  30000
#define	SP_4	 4	// Modbus Read/Write Words 40000
// The following are Modbus Holding Registers
#define	SP_COMT	99	// Indirect Register Access
#define	SP_ACC	10	// Access
#define	SP_ALA	11	// Alarm.1 - Alarm.4
#define	SP_ALB	12	// Alarm.5 - Alarm.8
#define	SP_ALS	13	// Alarm Summary
#define	SP_BCD	14	// BCDInput
#define	SP_COM	15	// Comms
#define	SP_DAL	16	// DigAlarm
#define	SP_HUM	17	// Humidity
#define	SP_INS	18	// Instrument
#define	SP_IPM	19	// IPMonitor
#define	SP_LG2	20	// Lgc2
#define	SP_LG8	21	// Lgc8
#define	SP_LGI	22	// LgcIO
#define	SP_LIN	23	// Lin16
#define	SP_LDG	24	// Loop Diag
#define	SP_LMN	25	// Loop Main
#define	SP_LOP	26	// Loop OP
#define	SP_PID	27	// Loop PID
#define	SP_SET	28	// Loop Setup
#define	SP_SP	29	// Loop SP
#define	SP_TUN	30	// Loop Tune
#define	SP_MAT	31	// Math2
#define	SP_MOD	32	// Mod
#define	SP_MID	33	// ModIDs
#define	SP_MUL	34	// MultiOper
#define	SP_PGM	35	// Programmer
#define	SP_PV	36	// PV
#define	SP_REC	37	// Recipe
#define	SP_RLY	38	// RelayAA
#define	SP_SWO	39	// SwitchOver
#define	SP_TIM	40	// Timer
#define	SP_TXD	41	// Txdr
#define	SP_USE	42	// UsrVal
#define	SP_ZIR	43	// Zirconia

// different from 350x
#define	SP_CMC	44	// IO.CurrentMonitor.Config Global
#define	SP_CML	45	// IO.CurrentMonitor.Config.Load
#define	SP_CMS	46	// IO.CurrentMonitor.Status
#define	SP_IOF	47	// IO.FixedIO

// Added May 2011
#define	SP_PGS	48	// Prgr Segments
// End Space definitions

#define	PGCNT	 8	// 8 Programmers

// Programmer size
#define	PGMCNT	64	// 64 data items per Programmer General Data
#define	XPGM	((PGCNT * PGMCNT) - 1)

// Prgr sizes
#define	SEGCNT	16	// 16 segments are allocated per Programmer
#define	SEGMAX	32	// 32 modbus addresses are allocated per segment
#define	PRGMAX	512	// 512 modbus addresses are allocated per programmer
#define	SEGSZ	(SEGCNT * SEGMAX)	// current number of selections per programmer
#define	XPGS	(((PGCNT - 1) * PRGMAX) + SEGSZ - 1)	// maximum number for selections

// Dialog box numbers
#define	DPADD	2002
#define	DNAME	2021
#define	DLOOP	2030
#define	DNUMB	2031
#define	DMODB	2033
#define	DSEGT	2034
#define	DSEGM	2035

struct EIESTRINGS
{
	UINT	uQty;		// Number of strings entered for selection
	UINT	uBlkCt;		// Number of blocks for selection
	UINT	uBlock;		// Selected block
	UINT	uBPos;		// Position of Block # in List
	UINT	uAddr;		// ID
	UINT	uNPos;		// Position of Name in List
	UINT	uSPos;		// Position of Segment in List
	UINT	uTable;		// Table number selected
	UINT	uOffset;	// Addr.a.m_Offset value
	UINT	uPrgr;		// Programmer Selection
	UINT	uSegm;		// Segment for Programmer
	BOOL	fIsPrgr;	// Prgr - has 2 numeric entries
	};

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm/Invensys Mini8 TCP/IP Master Driver Options
//

class CEuroInvensysMini8TCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEuroInvensysMini8TCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Comms Driver
//

class CEuroInvensysMini8Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuroInvensysMini8Driver(void);

		// Destructor
		~CEuroInvensysMini8Driver(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		EIESTRINGS * GetEIEPtr(void);

		BOOL	IsNotStdModbus(UINT uTable);

		void	InitItemInfo   (UINT uTable);
		void	LoadItemInfo   (UINT uTable);
		CString	LoadItemStrings(UINT uTable, BOOL fStore, UINT uSelect);

		BOOL	HasBlocks(void);

		CString	Get_sName(void);
		void	Set_sName(CString sName);
		UINT	GetAddrFromName(void);

	protected:

		// Data
		EIESTRINGS	EIESTR;
		EIESTRINGS	*m_pEIE;

		CString m_sName;	// Selected string

		// Implementation
		void	AddSpaces(void);

		CString	ParseParamString(CString sParamString, BOOL fStore);
		CString	ParseAddr(CString sParamString, BOOL fStore);
		void	ParseModifiers(CString sParamString, UINT *pInc);

		// Helpers
		UINT	Find2ndDotFwd(CString s);
		UINT	Find2ndDotRev(CString s);

		// Other Help
		void	InitEIE(void);

		// String Info Loading
 		void	LoadAccessInfo(void);
		void	LoadAlarmAInfo(void);
		void	LoadAlarmBInfo(void);
		void	LoadAlarmSummaryInfo(void);
		void	LoadBCDInfo(void);
		void	LoadCommsInfo(void);
		void	LoadDigAlarmInfo(void);
		void	LoadHumidityInfo(void);
		void	LoadInstrumentInfo(void);
		void	LoadCurrMonCnfgInfo(void);
		void	LoadCurrMonLoadInfo(void);
		void	LoadCurrMonStatInfo(void);
		void	LoadFixedIOInfo(void);
		void	LoadIPMonitorInfo(void);
		void	LoadLgc2Info(void);
		void	LoadLgc8Info(void);
		void	LoadLgcIOInfo(void);
		void	LoadLinInfo(void);
		void	LoadLoopDiagInfo(void);
		void	LoadLoopMainInfo(void);
		void	LoadLoopOPInfo(void);
		void	LoadLoopPIDInfo(void);
		void	LoadLoopSetupInfo(void);
		void	LoadSPInfo(void);
		void	LoadTuneInfo(void);
		void	LoadMath2Info(void);
		void	LoadModInfo(void);
		void	LoadModIDInfo(void);
		void	LoadMultInfo(void);
		void	LoadProgrammerDInfo(void);
		void	LoadProgrammerSInfo(void);
		void	LoadPVInfo(void);
		void	LoadRecipeInfo(void);
		void	LoadRelayInfo(void);
		void	LoadSwitchoverInfo(void);
		void	LoadTimerInfo(void);
		void	LoadTxdrInfo(void);
		void	LoadUsrValInfo(void);
		void	LoadZirconiaInfo(void);

		// String Loading
 		CString	LoadAccess(UINT uSel);
		CString	LoadAlarmA(UINT uSel);
		CString	LoadAlarmB(UINT uSel);
		CString	LoadAlarmSummary(UINT uSel);
		CString	LoadBCD(UINT uSel);
		CString	LoadComms(UINT uSel);
		CString	LoadDigAlarm(UINT uSel);
		CString	LoadHumidity(UINT uSel);
		CString	LoadInstrument(UINT uSel);
		CString	LoadCurrMonCnfg(UINT uSel);
		CString	LoadCurrMonLoad(UINT uSel);
		CString	LoadCurrMonStat(UINT uSel);
		CString	LoadFixedIO(UINT uSel);
		CString	LoadIPMonitor(UINT uSel);
		CString	LoadLgc2(UINT uSel);
		CString	LoadLgc8(UINT uSel);
		CString	LoadLgcIO(UINT uSel);
		CString	LoadLin(UINT uSel);
		CString	LoadLoopDiag(UINT uSel);
		CString	LoadLoopMain(UINT uSel);
		CString	LoadLoopOP(UINT uSel);
		CString	LoadLoopPID(UINT uSel);
		CString	LoadLoopSetup(UINT uSel);
		CString	LoadLoopSP(UINT uSel);
		CString	LoadLoopTune(UINT uSel);
		CString	LoadMath2(UINT uSel);
		CString	LoadMod(UINT uSel);
		CString	LoadModID(UINT uSel);
		CString	LoadMult(UINT uSel);
		CString	LoadProgrammerData(UINT uSel);
		CString	LoadProgrammerSegs(UINT uSel);
		CString	LoadPV(UINT uSel);
		CString	LoadRecipe(UINT uSel);
		CString	LoadRelay(UINT uSel);
		CString	LoadSwitchover(UINT uSel);
		CString	LoadTimer(UINT uSel);
		CString	LoadTxdr(UINT uSel);
		CString	LoadUsrVal(UINT uSel);
		CString	LoadZirconia(UINT uSel);
	};

class CEuroInvensysMini8SerialDriver : public CEuroInvensysMini8Driver
{
	public:
		// Constructor
		CEuroInvensysMini8SerialDriver(void);

		// Destructor
		~CEuroInvensysMini8SerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);
	};

class CEuroInvensysMini8TCPDriver : public CEuroInvensysMini8Driver
{
	public:
		// Constructor
		CEuroInvensysMini8TCPDriver(void);

		// Destructor
		~CEuroInvensysMini8TCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Invensys EuroInvensys Selection Dialog
//

class CEuroInvensysMini8AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEuroInvensysMini8AddrDialog(CEuroInvensysMini8Driver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

		// Destructor
		~CEuroInvensysMini8AddrDialog(void);
		                
	protected:
		CEuroInvensysMini8Driver * m_pGDrv;

		EIESTRINGS	*m_pEIE;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk     (UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnComboChange(UINT uID, CWnd &Wnd);
		void	OnTypeChange (UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overrides
		BOOL	AllowType(UINT uType);
		void	ShowDetails(void);

		// Selection Handling
		void	InitItemInfo(UINT uTable, BOOL fNew);
		void	LoadNames(void);
		void	SetItemData(void);
		void	SetBoxPosition(UINT uID, UINT uPos);
		UINT	GetBoxPosition(UINT uID);
		void	SetItemAddr (void);
		void	SetNameInfo(UINT uTable);
		void	SetBlockNumbers(void);
		void	SetSegmNumbers(void);
		void	GetParamData(UINT uPos);
		UINT	GetAddrFromText(void);

		// Helpers
		void	InitSelects(void);
		void	ClearSelData(void);
		void	ClearBox(CComboBox & ccb);
		void	ClearBox(UINT uID);
		void	ShowMini8Details(void);
		CString	StripAddr(CString sSelect);
	};

// String Format:
// {
//	Parameter Name
//	(Number of bits in parameter)
//	if( String 0 ) {
//		number of blocks-
//		if( number of blocks > 1 ) {
//			[block 0 starting address]
//			=increment between blocks
//			}
//		else {
//			[block address]
//	else {
//		offset from start of block
//		}
//	}

#define	SNOTU	L"Not Defined"
#define	SNONE	L""

// Format
// A - Section Name
// B - Block Number
// C - Parameter Name
// D - ID
// E - Increment to ID in next block
// F - ID's in subsequent blocks

// Access Strings (10)
// Format A.C_D
#define	SACC1	L"Access.CustomerID@4739"
#define	SACC2	L"Access.InstrumentMode@199"

// Simplify initialization
#define	SACC1N	L"Access.CustomerID"
#define	SACC1A	4739

// Alarm Strings A 1-32 (11) 
// Format A.B.C@D#E + special case - A.B.C.D(<ID2,ID3|ID4>)
#define	SALM1	L"Alarm.1.Ack@10250#16"
#define	SALM2	L"Alarm.1.Block@10246#16"
#define	SALM3	L"Alarm.1.Delay@10248#16"
#define	SALM4	L"Alarm.1.Hysteresis@10242#16"
#define	SALM5	L"Alarm.1.Inhibit@10247#16"
#define	SALM6	L"Alarm.1.Latch@10244#16"
#define	SALM7	L"Alarm.1.Out@10249#16"
#define	SALM8	L"Alarm.1.Reference@10243#16"
#define	SALM9	L"Alarm.1.Threshold@10241#16"
#define	SALM10	L"Alarm.1.Type@10240#16"

// Alarm Strings B 5-8 (12)
// Format A.B.C@D#E
#define	SALM19	L"Alarm.5.Ack@10314#16"
#define	SALM20	L"Alarm.5.Block@10310#16"
#define	SALM21	L"Alarm.5.Delay@10312#16"
#define	SALM22	L"Alarm.5.Hysteresis@10306#16"
#define	SALM23	L"Alarm.5.Inhibit@10311#16"
#define	SALM24	L"Alarm.5.Latch@10308#16"
#define	SALM25	L"Alarm.5.Out@10313#16"
#define	SALM26	L"Alarm.5.Priority@10309#16"
#define	SALM27	L"Alarm.5.Reference@10307#16"
#define	SALM28	L"Alarm.5.Threshold@10305#16"
#define	SALM29	L"Alarm.5.Type@10304#16"

// Alarm Summary (13)
// Format A.C@D
#define	SALS1	L"AlmSummary.General.AnAlarmStatus1@10176"
#define	SALS2	L"AlmSummary.General.AnAlarmStatus2@10177"
#define	SALS3	L"AlmSummary.General.AnAlarmStatus3@10178"
#define	SALS4	L"AlmSummary.General.AnAlarmStatus4@10179"
#define	SALS5	L"AlmSummary.General.AnyAlarm@10213"
#define	SALS6	L"AlmSummary.General.CTAlarmStatus1@4192"
#define	SALS7	L"AlmSummary.General.CTAlarmStatus2@4193"
#define	SALS8	L"AlmSummary.General.CTAlarmStatus3@4194"
#define	SALS9	L"AlmSummary.General.CTAlarmStatus4@4195"
#define	SALS10	L"AlmSummary.General.DigAlarmStatus1@10188"
#define	SALS11	L"AlmSummary.General.DigAlarmStatus2@10189"
#define	SALS12	L"AlmSummary.General.DigAlarmStatus3@10190"
#define	SALS13	L"AlmSummary.General.DigAlarmStatus4@10191"
#define	SALS14	L"AlmSummary.General.GlobalAck@10214"
#define	SALS15	L"AlmSummary.General.NewAlarm@10212"
#define	SALS16	L"AlmSummary.General.NewCTAlarm@4196"
#define	SALS17	L"AlmSummary.General.RstNewAlarm@10215"
#define	SALS18	L"AlmSummary.General.RstNewCTAlarm@4197"
#define	SALS19	L"AlmSummary.General.SBrkAlarmStatus1@10200"
#define	SALS20	L"AlmSummary.General.SBrkAlarmStatus2@10201"
#define	SALS21	L"AlmSummary.General.SBrkAlarmStatus3@10202"
#define	SALS22	L"AlmSummary.General.SBrkAlarmStatus4@10203"

// BCD Strings
// Format A.C@D (14)
#define	SBCD1	L"BCDInput.1.BCDVal@5072"
#define	SBCD2	L"BCDInput.2.BCDVal@5073"

// Comms Strings (15)
// Format A.C@D
#define	SCOM1	L"Comms.FC.Ident@12963"
#define	SCOM2	L"Comms.1.ProgramNumber@5568#64"

// DigAlarm 1-32 (16)
// Format A.B.C@D#E
#define	SDAL1	L"DigAlarm.1.Ack@11274#16"
#define	SDAL2	L"DigAlarm.1.Block@11270#16"
#define	SDAL3	L"DigAlarm.1.Delay@11272#16"
#define	SDAL4	L"DigAlarm.1.Inhibit@11271#16"
#define	SDAL5	L"DigAlarm.1.Latch@11268#16"
#define	SDAL6	L"DigAlarm.1.Out@11273#16"
#define	SDAL7	L"DigAlarm.1.Type@11264#16"

// Humidity Strings (17)
// Format A.C@D
#define	SHUM1	L"Humidity.DewPoint@13317"
#define	SHUM2	L"Humidity.DryTemp@13318"
#define	SHUM3	L"Humidity.Pressure@13313"
#define	SHUM4	L"Humidity.PsychroConst@13315"
#define	SHUM5	L"Humidity.RelHumid@13316"
#define	SHUM6	L"Humidity.Resolution@13320"
#define	SHUM7	L"Humidity.SBrk@13314"
#define	SHUM8	L"Humidity.WetOffset@13312"
#define	SHUM9	L"Humidity.WetTemp@13319"

// Instrument Strings (18)
// Format A.C@D
#define	SINS1	L"Instrument.Diagnostics.CntrlOverrun@4737"
#define	SINS2	L"Instrument.Diagnostics.ErrCount@4736"
#define	SINS3	L"Instrument.Diagnostics.PSUident@13027"
#define	SINS4	L"Instrument.InstInfo.CompanyID@121"
#define	SINS5	L"Instrument.InstInfo.InstType@122"
#define	SINS6	L"Instrument.InstInfo.Version@107"
#define	SINS7	L"Instrument.Options.Units@4738"

// IO.Current Monitor / IO.FixedIO at end of list

// IP Monitor Strings (19)
// Format A.C@D
#define	SIP1	L"IPMonitor.1.Max@4915"
#define	SIP2	L"IPMonitor.1.Min@4916"
#define	SIP3	L"IPMonitor.1.Reset@4919"
#define	SIP4	L"IPMonitor.1.Threshold@4917"
#define	SIP5	L"IPMonitor.1.TimeAbove@4918"
#define	SIP6	L"IPMonitor.2.Max@4920"
#define	SIP7	L"IPMonitor.2.Min@4921"
#define	SIP8	L"IPMonitor.2.Reset@4924"
#define	SIP9	L"IPMonitor.2.Threshold@4922"
#define	SIP10	L"IPMonitor.2.TimeAbove@4923"

// LGC2 Strings 1-24 (20)
// Format A.B.C@D#E
#define	S2LG1	L"Lgc2.1.In1@4822#3"
#define	S2LG2	L"Lgc2.1.In2@4823#3"
#define	S2LG3	L"Lgc2.1.Out@4824#3"

// LGC8 Strings  (21)
// Format A.B.C@D
#define	S8LG1	L"Lgc8.1.In1@4894"
#define	S8LG2	L"Lgc8.1.In2@4895"
#define	S8LG3	L"Lgc8.1.In3@4896"
#define	S8LG4	L"Lgc8.1.In4@4897"
#define	S8LG5	L"Lgc8.1.In5@4898"
#define	S8LG6	L"Lgc8.1.In6@4899"
#define	S8LG7	L"Lgc8.1.In7@4900"
#define	S8LG8	L"Lgc8.1.In8@4901"
#define	S8LG9	L"Lgc8.1.Out@4902"
#define	S8LG10	L"Lgc8.2.In1@4903"
#define	S8LG11	L"Lgc8.2.In2@4904"
#define	S8LG12	L"Lgc8.2.In3@4905"
#define	S8LG13	L"Lgc8.2.In4@4906"
#define	S8LG14	L"Lgc8.2.In5@4907"
#define	S8LG15	L"Lgc8.2.In6@4908"
#define	S8LG16	L"Lgc8.2.In7@4909"
#define	S8LG17	L"Lgc8.2.In8@4910"
#define	S8LG18	L"Lgc8.2.Out@4911"
#define	S8LG19	L"Lgc8.3.In1@5054"
#define	S8LG20	L"Lgc8.3.In2@5055"
#define	S8LG21	L"Lgc8.3.In3@5056"
#define	S8LG22	L"Lgc8.3.In4@5057"
#define	S8LG23	L"Lgc8.3.In5@5058"
#define	S8LG24	L"Lgc8.3.In6@5059"
#define	S8LG25	L"Lgc8.3.In7@5060"
#define	S8LG26	L"Lgc8.3.In8@5061"
#define	S8LG27	L"Lgc8.3.Out@5062"
#define	S8LG28	L"Lgc8.4.In1@5063"
#define	S8LG29	L"Lgc8.4.In2@5064"
#define	S8LG30	L"Lgc8.4.In3@5065"
#define	S8LG31	L"Lgc8.4.In4@5066"
#define	S8LG32	L"Lgc8.4.In5@5067"
#define	S8LG33	L"Lgc8.4.In6@5068"
#define	S8LG34	L"Lgc8.4.In7@5069"
#define	S8LG35	L"Lgc8.4.In8@5070"
#define	S8LG36	L"Lgc8.4.Out@5071"

// LgcIO Strings (22)
// Format A.C@D
#define	SLGI1	L"LgcIO.LA.Backlash@124"
#define	SLGI2	L"LgcIO.LA.Inertia@123"
#define	SLGI3	L"LgcIO.LA.MinOnTime_45@45"
#define	SLGI4	L"LgcIO.LA.MinOnTime_54@54"
#define	SLGI5	L"LgcIO.LA.PV@361"
#define	SLGI6	L"LgcIO.LB.MinOnTime@89"
#define	SLGI7	L"LgcIO.LB.PV@362"

// Lin16 Strings (23)
// Format A.C@D
#define	SLIN1	L"Lin16.In@4960"
#define	SLIN2	L"Lin16.In1@4929"
#define	SLIN3	L"Lin16.In2@4930"
#define	SLIN4	L"Lin16.In3@4931"
#define	SLIN5	L"Lin16.In4@4932"
#define	SLIN6	L"Lin16.In5@4933"
#define	SLIN7	L"Lin16.In6@4934"
#define	SLIN8	L"Lin16.In7@4935"
#define	SLIN9	L"Lin16.In8@4936"
#define	SLIN10	L"Lin16.In9@4937"
#define	SLIN11	L"Lin16.In10@4938"
#define	SLIN12	L"Lin16.In11@4939"
#define	SLIN13	L"Lin16.In12@4940"
#define	SLIN14	L"Lin16.In13@4941"
#define	SLIN15	L"Lin16.In14@4942"
#define	SLIN16	L"Lin16.InHighLimit@4943"
#define	SLIN17	L"Lin16.InLowLimit@4928"
#define	SLIN18	L"Lin16.Out@4961"
#define	SLIN19	L"Lin16.Out1@4945"
#define	SLIN20	L"Lin16.Out2@4946"
#define	SLIN21	L"Lin16.Out3@4947"
#define	SLIN22	L"Lin16.Out4@4948"
#define	SLIN23	L"Lin16.Out5@4949"
#define	SLIN24	L"Lin16.Out6@4950"
#define	SLIN25	L"Lin16.Out7@4951"
#define	SLIN26	L"Lin16.Out8@4952"
#define	SLIN27	L"Lin16.Out9@4953"
#define	SLIN28	L"Lin16.Out10@4954"
#define	SLIN29	L"Lin16.Out11@4955"
#define	SLIN30	L"Lin16.Out12@4956"
#define	SLIN31	L"Lin16.Out13@4957"
#define	SLIN32	L"Lin16.Out14@4958"
#define	SLIN33	L"Lin16.OutHighLimit@4959"
#define	SLIN34	L"Lin16.OutLowLimit@4944"

// LOOP Diag 1-16 (24)
// Format A.B.C@D#E
#define	SLDG1	L"Loop.1.Diag.DerivativeOutContrib@119#256"
#define	SLDG2	L"Loop.1.Diag.Error@113#256"
#define	SLDG3	L"Loop.1.Diag.IntegralOutContrib@118#256"
#define	SLDG4	L"Loop.1.Diag.LoopBreakAlarm@116#256"
#define	SLDG5	L"Loop.1.Diag.LoopMode@114#256"
#define	SLDG6	L"Loop.1.Diag.PropOutContrib@117#256"
#define	SLDG7	L"Loop.1.Diag.SBrk@120#256"
#define	SLDG8	L"Loop.1.Diag.SchedCBH@32#256"
#define	SLDG9	L"Loop.1.Diag.SchedCBL@33#256"
#define	SLDG10	L"Loop.1.Diag.SchedLPBrk@35#256"
#define	SLDG11	L"Loop.1.Diag.SchedMR@34#256"
#define	SLDG12	L"Loop.1.Diag.SchedOPHi@37#256"
#define	SLDG13	L"Loop.1.Diag.SchedOPLo@38#256"
#define	SLDG14	L"Loop.1.Diag.SchedPB@29#256"
#define	SLDG15	L"Loop.1.Diag.SchedR2G@36#256"
#define	SLDG16	L"Loop.1.Diag.SchedTd@31#256"
#define	SLDG17	L"Loop.1.Diag.SchedTi@30#256"
#define	SLDG18	L"Loop.1.Diag.TargetOutVal@115#256"

// LOOP Main 1-16 (25)
// Format A.B.C@D#E
#define	SLMN1	L"Loop.1.Main.ActiveOut@4#256"
#define	SLMN2	L"Loop.1.Main.AutoMan@10#256"
#define	SLMN3	L"Loop.1.Main.Inhibit@20#256"
#define	SLMN4	L"Loop.1.Main.PV@1#256"
#define	SLMN5	L"Loop.1.Main.TargetSP@2#256"
#define	SLMN6	L"Loop.1.Main.WorkingSP@5#256"

// LOOP OP 1-16 (26)
// Format A.B.C@D#E
#define	SLOP1	L"Loop.1.OP.Ch1OnOffHysteresis@84#256"
#define	SLOP2	L"Loop.1.OP.Ch1Out@82#256"
#define	SLOP3	L"Loop.1.OP.Ch2Deadband@16#256"
#define	SLOP4	L"Loop.1.OP.Ch2OnOffHysteresis@85#256"
#define	SLOP5	L"Loop.1.OP.Ch2Out@83#256"
#define	SLOP6	L"Loop.1.OP.CoolType@93#256"
#define	SLOP7	L"Loop.1.OP.EnablePowerFeedforward@91#256"
#define	SLOP8	L"Loop.1.OP.FeedForwardGain@95#256"
#define	SLOP9	L"Loop.1.OP.FeedForwardOffset@96#256"
#define	SLOP10	L"Loop.1.OP.FeedForwardTrimLimit@97#256"
#define	SLOP11	L"Loop.1.OP.FeedForwardType@94#256"
#define	SLOP12	L"Loop.1.OP.FeedForwardVal@98#256"
#define	SLOP13	L"Loop.1.OP.FF_Rem@103#256"
#define	SLOP14	L"Loop.1.OP.ManualMode@90#256"
#define	SLOP15	L"Loop.1.OP.ManualOutVal@3#256"
#define	SLOP16	L"Loop.1.OP.MeasuredPower@92#256"
#define	SLOP17	L"Loop.1.OP.OutputHighLimit@80#256"
#define	SLOP18	L"Loop.1.OP.OutputLowLimit@81#256"
#define	SLOP19	L"Loop.1.OP.Rate@86#256"
#define	SLOP20	L"Loop.1.OP.RateDisable@87#256"
#define	SLOP21	L"Loop.1.OP.RemOPH@102#256"
#define	SLOP22	L"Loop.1.OP.RemOPL@101#256"
#define	SLOP23	L"Loop.1.OP.SafeOutVal@89#256"
#define	SLOP24	L"Loop.1.OP.SBrkOP@123#256"
#define	SLOP25	L"Loop.1.OP.SensorBreakMode@88#256"
#define	SLOP26	L"Loop.1.OP.TrackEnable@100#256"
#define	SLOP27	L"Loop.1.OP.TrackOutVal@99#256"

// LOOP PID 1-16 (27)
// Format A.B.C@D#E
#define	SLPD1	L"Loop.1.PID.ActiveSet@28#256"
#define	SLPD2	L"Loop.1.PID.Boundary1-2@26#256"
#define	SLPD3	L"Loop.1.PID.Boundary2-3@27#256"
#define	SLPD4	L"Loop.1.PID.CutbackHigh@18#256"
#define	SLPD5	L"Loop.1.PID.CutbackHigh2@46#256"
#define	SLPD6	L"Loop.1.PID.CutbackHigh3@56#256"
#define	SLPD7	L"Loop.1.PID.CutbackLow@17#256"
#define	SLPD8	L"Loop.1.PID.CutbackLow2@47#256"
#define	SLPD9	L"Loop.1.PID.CutbackLow3@57#256"
#define	SLPD10	L"Loop.1.PID.DerivativeTime@9#256"
#define	SLPD11	L"Loop.1.PID.DerivativeTime2@45#256"
#define	SLPD12	L"Loop.1.PID.DerivativeTime3@55#256"
#define	SLPD13	L"Loop.1.PID.IntegralTime@8#256"
#define	SLPD14	L"Loop.1.PID.IntegralTime2@44#256"
#define	SLPD15	L"Loop.1.PID.IntegralTime3@54#256"
#define	SLPD16	L"Loop.1.PID.LoopBreakTime@40#256"
#define	SLPD17	L"Loop.1.PID.LoopBreakTime2@49#256"
#define	SLPD18	L"Loop.1.PID.LoopBreakTime3@59#256"
#define	SLPD19	L"Loop.1.PID.ManualReset@39#256"
#define	SLPD20	L"Loop.1.PID.ManualReset2@48#256"
#define	SLPD21	L"Loop.1.PID.ManualReset3@58#256"
#define	SLPD22	L"Loop.1.PID.NumSets@64#256"
#define	SLPD23	L"Loop.1.PID.OutputHi@41#256"
#define	SLPD24	L"Loop.1.PID.OutputHi2@51#256"
#define	SLPD25	L"Loop.1.PID.OutputHi3@61#256"
#define	SLPD26	L"Loop.1.PID.OutputLo@42#256"
#define	SLPD27	L"Loop.1.PID.OutputLo2@52#256"
#define	SLPD28	L"Loop.1.PID.OutputLo3@62#256"
#define	SLPD29	L"Loop.1.PID.ProportionalBand@6#256"
#define	SLPD30	L"Loop.1.PID.ProportionalBand2@43#256"
#define	SLPD31	L"Loop.1.PID.ProportionalBand3@53#256"
#define	SLPD32	L"Loop.1.PID.RelCh2Gain@19#256"
#define	SLPD33	L"Loop.1.PID.RelCh2Gain2@50#256"
#define	SLPD34	L"Loop.1.PID.RelCh2Gain3@60#256"
#define	SLPD35	L"Loop.1.PID.SchedulerRemoteInput@65#256"
#define	SLPD36	L"Loop.1.PID.SchedulerType@63#256"

// LOOP Setup 1-16 (28)
// Format A.B.C@D#E
#define	SLSE1	L"Loop.1.Setup.CH1ControlType@22#256"
#define	SLSE2	L"Loop.1.Setup.CH2ControlType@23#256"
#define	SLSE3	L"Loop.1.Setup.ControlAction@7#256"
#define	SLSE4	L"Loop.1.Setup.DerivativeType@25#256"
#define	SLSE5	L"Loop.1.Setup.LoopType@21#256"
#define	SLSE6	L"Loop.1.Setup.PBUnits@24#256"

// LOOP SP 1-16 (29)
// Format A.B.C@D#E
#define	SLSP1	L"Loop.1.SP.AltSP@68#256"
#define	SLSP2	L"Loop.1.SP.AltSPSelect@69#256"
#define	SLSP3	L"Loop.1.SP.ManualTrack@75#256"
#define	SLSP4	L"Loop.1.SP.RangeHigh@12#256"
#define	SLSP5	L"Loop.1.SP.RangeLow@11#256"
#define	SLSP6	L"Loop.1.SP.Rate@70#256"
#define	SLSP7	L"Loop.1.SP.RateDisable@71#256"
#define	SLSP8	L"Loop.1.SP.RateDone@79#256"
#define	SLSP9	L"Loop.1.SP.SP1@13#256"
#define	SLSP10	L"Loop.1.SP.SP2@14#256"
#define	SLSP11	L"Loop.1.SP.SPHighLimit@66#256"
#define	SLSP12	L"Loop.1.SP.SPLowLimit@67#256"
#define	SLSP13	L"Loop.1.SP.SPSelect@15#256"
#define	SLSP14	L"Loop.1.SP.SPTrack@76#256"
#define	SLSP15	L"Loop.1.SP.SPTrim@72#256"
#define	SLSP16	L"Loop.1.SP.SPTrimHighLimit@73#256"
#define	SLSP17	L"Loop.1.SP.SPTrimLowLimit@74#256"
#define	SLSP18	L"Loop.1.SP.TrackPV@77#256"
#define	SLSP19	L"Loop.1.SP.TrackSP@78#256"

// LOOP Tune 1-16
// Format A.B.C@D#E  (30)
#define	SLTU1	L"Loop.1.Tune.AutotuneEnable@108#256"
#define	SLTU2	L"Loop.1.Tune.OutputHighLimit@105#256"
#define	SLTU3	L"Loop.1.Tune.OutputLowLimit@106#256"
#define	SLTU4	L"Loop.1.Tune.Stage@111#256"
#define	SLTU5	L"Loop.1.Tune.StageTime@112#256"
#define	SLTU6	L"Loop.1.Tune.State@110#256"
#define	SLTU7	L"Loop.1.Tune.StepSize@109#256"
#define	SLTU8	L"Loop.1.Tune.Type@104#256"

// MATH2 Strings 1-24  (31)
// Format A.B.C@D#E
#define	SMAT1	L"Math2.1.In1@4750#3"
#define	SMAT2	L"Math2.1.In2@4751#3"
#define	SMAT3	L"Math2.1.Out@4752#3"

// Mod Strings 1-32 (32)
// Format A.C@D#E% (% means block # in front of last '.')
#define	SMOD1	L"IO.Mod.1.AlarmAck@4260#1%"
#define	SMOD2	L"IO.Mod.1.HiOffset@4420#1%"
#define	SMOD3	L"IO.Mod.1.HiPoint@4388#1%"
#define	SMOD4	L"IO.Mod.1.LoOffset@4356#1%"
#define	SMOD5	L"IO.Mod.1.LoPoint@4324#1%"
#define	SMOD6	L"IO.Mod.1.MinOnTime@4292#1%"
#define	SMOD7	L"IO.Mod.1.PV@4228#1%"

// ModIDs  (33)
// Format A.C@D
#define	SMID1	L"IO.ModIDs.Module1@12707"
#define	SMID2	L"IO.ModIDs.Module2@12771"
#define	SMID3	L"IO.ModIDs.Module3@12835"
#define	SMID4	L"IO.ModIDs.Module4@12899"

// MultiOper Strings 1-4  (34)
// Format A.B.C@D#E
#define	SMUL1	L"MultiOper.1.AverageOut@5017#12"
#define	SMUL2	L"MultiOper.1.In1@5006#12"
#define	SMUL3	L"MultiOper.1.In2@5007#12"
#define	SMUL4	L"MultiOper.1.In3@5008#12"
#define	SMUL5	L"MultiOper.1.In4@5009#12"
#define	SMUL6	L"MultiOper.1.In5@5010#12"
#define	SMUL7	L"MultiOper.1.In6@5011#12"
#define	SMUL8	L"MultiOper.1.In7@5012#12"
#define	SMUL9	L"MultiOper.1.In8@5013#12"
#define	SMUL10	L"MultiOper.1.MaxOut@5015#12"
#define	SMUL11	L"MultiOper.1.MinOut@5016#12"
#define	SMUL12	L"MultiOper.1.SumOut@5014#12"

// Programmer Data Strings  1-8 (35)
// Format A.B.C@D#E
// added May 2011
#define	SPGM01	L"Programmer.1.CommsProgramNumber@5568#64"
#define	SPGM02	L"Programmer.1.ProgramHoldbackVal@5569#64"
#define	SPGM03	L"Programmer.1.ProgramRampUnits@5570#64"
#define	SPGM04	L"Programmer.1.ProgramDwellUnits@5571#64"
#define	SPGM05	L"Programmer.1.ProgramCycles@5572#64"
#define	SPGM06	L"Programmer.1.PowerFailAct@5573#64"
#define	SPGM07	L"Programmer.1.Servo@5574#64"
#define	SPGM08	L"Programmer.1.SyncMode@5575#64"
#define	SPGM09	L"Programmer.1.ResetEventOuts@5576#64"
#define	SPGM10	L"Programmer.1.CurProg@5577#64"
#define	SPGM11	L"Programmer.1.CurSeg@5578#64"
#define	SPGM12	L"Programmer.1.ProgStatus@5579#64"
#define	SPGM13	L"Programmer.1.PSP@5580#64"
#define	SPGM14	L"Programmer.1.CyclesLeft@5581#64"
#define	SPGM15	L"Programmer.1.CurSegType@5582#64"
#define	SPGM16	L"Programmer.1.SegTarget@5583#64"
#define	SPGM17	L"Programmer.1.SegRate@5584#64"
#define	SPGM18	L"Programmer.1.ProgTimeLeft@5585#64"
#define	SPGM19	L"Programmer.1.PVIn@5586#64"
#define	SPGM20	L"Programmer.1.SPIn@5587#64"
#define	SPGM21	L"Programmer.1.EventOuts@5588#64"
#define	SPGM22	L"Programmer.1.SegTimeLeft@5589#64"
#define	SPGM23	L"Programmer.1.EndOfSeg@5590#64"
#define	SPGM24	L"Programmer.1.SyncIn@5591#64"
#define	SPGM25	L"Programmer.1.FastRun@5592#64"
#define	SPGM26	L"Programmer.1.AdvSeg@5593#64"
#define	SPGM27	L"Programmer.1.SkipSeg@5594#64"
#define	SPGM28	L"Programmer.1.Offset27@5695#64"
#define	SPGM29	L"Programmer.1.Offset28@5696#64"
#define	SPGM30	L"Programmer.1.ProgramPVStart@5597#64"
#define	SPGM31	L"Programmer.1.Offset30@5698#64"
#define	SPGM32	L"Programmer.1.Offset31@5699#64"
#define	SPGM33	L"Programmer.1.Offset32@5600#64"
#define	SPGM34	L"Programmer.1.Offset33@5601#64"
#define	SPGM35	L"Programmer.1.PrgIn1@5602#64"
#define	SPGM36	L"Programmer.1.PrgIn2@5603#64"
#define	SPGM37	L"Programmer.1.PVWaitIP@5604#64"
#define	SPGM38	L"Programmer.1.ProgError@5605#64"
#define	SPGM39	L"Programmer.1.PVEventOP@5606#64"
#define	SPGM40	L"Programmer.1.GoBackCyclesLeft@5607#64"
#define	SPGM41	L"Programmer.1.DelayTime@5608#64"
#define	SPGM42	L"Programmer.1.ProgReset@5609#64"
#define	SPGM43	L"Programmer.1.ProgRun@5610#64"
#define	SPGM44	L"Programmer.1.ProgHold@5611#64"
#define	SPGM45	L"Programmer.1.ProgRunHold@5612#64"
#define	SPGM46	L"Programmer.1.ProgRunReset@5613#64"
#define	SPGM47	L"Programmer.1.Offset46@5614#64"
#define	SPGM48	L"Programmer.1.Offset47@5615#64"
#define	SPGM49	L"Programmer.1.Offset48@5616#64"
#define	SPGM50	L"Programmer.1.Offset49@5617#64"
#define	SPGM51	L"Programmer.1.Offset50@5618#64"
#define	SPGM52	L"Programmer.1.Offset51@5619#64"
#define	SPGM53	L"Programmer.1.Offset52@5620#64"
#define	SPGM54	L"Programmer.1.Offset53@5621#64"
#define	SPGM55	L"Programmer.1.Offset54@5622#64"
#define	SPGM56	L"Programmer.1.Offset55@5623#64"
#define	SPGM57	L"Programmer.1.Offset56@5624#64"
#define	SPGM58	L"Programmer.1.Offset57@5625#64"
#define	SPGM59	L"Programmer.1.Offset58@5626#64"
#define	SPGM60	L"Programmer.1.Offset59@5627#64"
#define	SPGM61	L"Programmer.1.Offset60@5628#64"
#define	SPGM62	L"Programmer.1.Offset61@5629#64"
#define	SPGM63	L"Programmer.1.Offset62@5630#64"
#define	SPGM64	L"Programmer.1.Offset63@5631#64"

// Programmer Segment Strings P 1-8 S 1-16 (48) $ means segment number needed
#define	SPGS01	L"Prgr.1.Segment.1.Type@6080#512$"
#define	SPGS02	L"Prgr.1.Segment.1.Offset2@6082#512$"
#define	SPGS03	L"Prgr.1.Segment.1.Offset3@6083#512$"
#define	SPGS04	L"Prgr.1.Segment.1.Holdback@6081#512$"
#define	SPGS05	L"Prgr.1.Segment.1.Duration@6084#512$"
#define	SPGS06	L"Prgr.1.Segment.1.RampRate@6085#512$"
#define	SPGS07	L"Prgr.1.Segment.1.TargetSP@6086#512$"
#define	SPGS08	L"Prgr.1.Segment.1.EndAction@6087#512$"
#define	SPGS09	L"Prgr.1.Segment.1.EventOutputs@6088#512$"
#define	SPGS10	L"Prgr.1.Segment.1.WaitFor@6089#512$"
#define	SPGS11	L"Prgr.1.Segment.1.Offset10@6090#512$"
#define	SPGS12	L"Prgr.1.Segment.1.GobackSeg@6091#512$"
#define	SPGS13	L"Prgr.1.Segment.1.GobackCycles@6092#512$"
#define	SPGS14	L"Prgr.1.Segment.1.PVEvent@6093#512$"
#define	SPGS15	L"Prgr.1.Segment.1.PVThreshold@6094#512$"
#define	SPGS16	L"Prgr.1.Segment.1.UserVal@6095#512$"
#define	SPGS17	L"Prgr.1.Segment.1.GsoakType@6096#512$"
#define	SPGS18	L"Prgr.1.Segment.1.GsoakVal@6097#512$"
#define	SPGS19	L"Prgr.1.Segment.1.TimeEvent@6098#512$"
#define	SPGS20	L"Prgr.1.Segment.1.OnTime@6099#512$"
#define	SPGS21	L"Prgr.1.Segment.1.OffTime@6100#512$"
#define	SPGS22	L"Prgr.1.Segment.1.PIDSet@6101#512$"
#define	SPGS23	L"Prgr.1.Segment.1.PVWait@6102#512$"
#define	SPGS24	L"Prgr.1.Segment.1.WaitVal@6103#512$"
#define	SPGS25	L"Prgr.1.Segment.1.Offset24@6104#512$"
#define	SPGS26	L"Prgr.1.Segment.1.Offset25@6105#512$"
#define	SPGS27	L"Prgr.1.Segment.1.Offset26@6106#512$"
#define	SPGS28	L"Prgr.1.Segment.1.Offset27@6107#512$"
#define	SPGS29	L"Prgr.1.Segment.1.Offset28@6108#512$"
#define	SPGS30	L"Prgr.1.Segment.1.Offset29@6109#512$"
#define	SPGS31	L"Prgr.1.Segment.1.Offset30@6110#512$"
#define	SPGS32	L"Prgr.1.Segment.1.Offset31@6111#512$"

// PV Strings  (36)
// Format A.C@D
#define	SPV1	L"PV.CalState@534"
#define	SPV2	L"PV.CJCTemp@215"
#define	SPV3	L"PV.Emissivity@38"
#define	SPV4	L"PV.FilterTimeConstant@101"
#define	SPV5	L"PV.MeasuredVal@202"
#define	SPV6	L"PV.Offset@141"
#define	SPV7	L"PV.PV@360"
#define	SPV8	L"PV.RangeHigh@548"
#define	SPV9	L"PV.RangeLow@549"
#define	SPV10	L"PV.SBrkType@578"

// Recipe Strings  (37)
// Format A.C@D
#define	SRCP1	L"Recipe.LastDataset@4913"
#define	SRCP2	L"Recipe.LoadingStatus@4914"
#define	SRCP3	L"Recipe.RecipeSelect@4912"

// RelayAA Strings  (38)
// Format A.C@D
#define	SRLY1	L"RlyAA.PV@363"

// Switchover Strings  (39)
// Format A.C@D
#define	SSWO1	L"SwitchOver.SelectIn@4927"
#define	SSWO2	L"SwitchOver.SwitchHigh@4925"
#define	SSWO3	L"SwitchOver.SwitchLow@4926"

// Timer Strings 1-4  (40)
// Format A.B.C@D#E
#define	STIM1	L"Timer.1.ElapsedTime@4995#3"
#define	STIM2	L"Timer.1.Out@4996#3"
#define	STIM3	L"Timer.1.Time@4994#3"

// Txdr Strings 1-2  (41)
// Format A.B.C@D#E
#define	STXD1	L"Txdr.1.CalAdjust_1@237#8"
#define	STXD2	L"Txdr.1.CalAdjust_2@238#8"
#define	STXD3	L"Txdr.1.InHigh@233#8"
#define	STXD4	L"Txdr.1.InLow@232#8"
#define	STXD5	L"Txdr.1.ScaleHigh@235#8"
#define	STXD6	L"Txdr.1.ScaleLow@234#8"
#define	STXD7	L"Txdr.1.StartCal@226#2"
#define	STXD8	L"Txdr.1.StartHighCal@231#8"
#define	STXD9	L"Txdr.1.StartTare@225#2"
#define	STXD10	L"Txdr.1.TareValue@236#8"


// UsrVal Strings  (42)
#define	SUSE1	L"UsrVal.1.Val@4962"
#define	SUSE2	L"UsrVal.2.Val@4963"
#define	SUSE3	L"UsrVal.3.Val@4964"
#define	SUSE4	L"UsrVal.4.Val@4965"
#define	SUSE5	L"UsrVal.5.Val@4966"
#define	SUSE6	L"UsrVal.6.Val@4967"
#define	SUSE7	L"UsrVal.7.Val@4968"
#define	SUSE8	L"UsrVal.8.Val@4969"
#define	SUSE9	L"UsrVal.9.Val@4970"
#define	SUSE10	L"UsrVal.10.Val@4971"
#define	SUSE11	L"UsrVal.11.Val@4972"
#define	SUSE12	L"UsrVal.12.Val@4973"
#define	SUSE13	L"UsrVal.13.Val@4974"
#define	SUSE14	L"UsrVal.14.Val@4975"
#define	SUSE15	L"UsrVal.15.Val@4976"
#define	SUSE16	L"UsrVal.16.Val@4977"
#define	SUSE17	L"UsrVal.17.Val@4978"
#define	SUSE18	L"UsrVal.18.Val@4979"
#define	SUSE19	L"UsrVal.19.Val@4980"
#define	SUSE20	L"UsrVal.20.Val@4981"
#define	SUSE21	L"UsrVal.21.Val@4982"
#define	SUSE22	L"UsrVal.22.Val@4983"
#define	SUSE23	L"UsrVal.23.Val@4984"
#define	SUSE24	L"UsrVal.24.Val@4985"
#define	SUSE25	L"UsrVal.25.Val@4986"
#define	SUSE26	L"UsrVal.26.Val@4987"
#define	SUSE27	L"UsrVal.27.Val@4988"
#define	SUSE28	L"UsrVal.28.Val@4989"
#define	SUSE29	L"UsrVal.29.Val@4990"
#define	SUSE30	L"UsrVal.30.Val@4991"
#define	SUSE31	L"UsrVal.31.Val@4992"
#define	SUSE32	L"UsrVal.32.Val@4993"

// Zirconia Strings 1-2  (43)
// Format A.B.C@D#E
#define	SZIR1	L"Zirconia.1.CarbonPot@13256#32"
#define	SZIR2	L"Zirconia.1.CleanFreq@13251#32"
#define	SZIR3	L"Zirconia.1.CleanProbe@13248#32"
#define	SZIR4	L"Zirconia.1.CleanState@13268#32"
#define	SZIR5	L"Zirconia.1.CleanTime@13252#32"
#define	SZIR6	L"Zirconia.1.CleanValve@13263#32"
#define	SZIR7	L"Zirconia.1.DewPoint@13274#32"
#define	SZIR8	L"Zirconia.1.GasRef@13254#32"
#define	SZIR9	L"Zirconia.1.MaxRcovTime@13253#32"
#define	SZIR10	L"Zirconia.1.MinCalTemp@13270#32"
#define	SZIR11	L"Zirconia.1.MinRcovTime@13255#32"
#define	SZIR12	L"Zirconia.1.Oxygen@13261#32"
#define	SZIR13	L"Zirconia.1.OxygenExp@13260#32"
#define	SZIR14	L"Zirconia.1.ProbeFault@13271#32"
#define	SZIR15	L"Zirconia.1.ProbeInput@13259#32"
#define	SZIR16	L"Zirconia.1.ProbeOffset@13250#32"
#define	SZIR17	L"Zirconia.1.ProbeStatus@13262#32"
#define	SZIR18	L"Zirconia.1.ProbeType@13258#32"
#define	SZIR19	L"Zirconia.1.ProcFactor@13275#32"
#define	SZIR20	L"Zirconia.1.PVFrozen@13272#32"
#define	SZIR21	L"Zirconia.1.RemGasEn@13257#32"
#define	SZIR22	L"Zirconia.1.RemGasRef@13267#32"
#define	SZIR23	L"Zirconia.1.Resolution@13273#32"
#define	SZIR24	L"Zirconia.1.SootAlm@13264#32"
#define	SZIR25	L"Zirconia.1.TempInput@13269#32"
#define	SZIR26	L"Zirconia.1.TempOffset@13266#32"
#define	SZIR27	L"Zirconia.1.Time2Clean@13249#32"
#define	SZIR28	L"Zirconia.1.Tolerence@13276#32"
#define	SZIR29	L"Zirconia.1.WrkGas@13265#32"

// IO.CurrentMonitor.Config  (44)
// Format A.@D
#define	SCMC1	L"IO.CurrMon.Config.CalibrateCT1@4170"
#define	SCMC2	L"IO.CurrMon.Config.CalibrateCT2@4171"
#define	SCMC3	L"IO.CurrMon.Config.CalibrateCT3@4172"
#define	SCMC4	L"IO.CurrMon.Config.Commission@4096"
#define	SCMC5	L"IO.CurrMon.Config.CommissionStatus@4097"
#define	SCMC6	L"IO.CurrMon.Config.CT1Range@4103"
#define	SCMC7	L"IO.CurrMon.Config.CT1Resolution@4198"
#define	SCMC8	L"IO.CurrMon.Config.CT2Range@4104"
#define	SCMC9	L"IO.CurrMon.Config.CT2Resolution@4199"
#define	SCMC10	L"IO.CurrMon.Config.CT3Range@4105"
#define	SCMC11	L"IO.CurrMon.Config.CT3Resolution@4200"
#define	SCMC12	L"IO.CurrMon.Config.Inhibit@4099"
#define	SCMC13	L"IO.CurrMon.Config.Interval@4098"
#define	SCMC14	L"IO.CurrMon.Config.MaxLeakPh1@4100"
#define	SCMC15	L"IO.CurrMon.Config.MaxLeakPh2@4101"
#define	SCMC16	L"IO.CurrMon.Config.MaxLeakPh3@4102"

// IO.CurrentMonitor.Load 1-16 (45)
// Format A.BC@D#E
#define	SCML1	L"IO.CurrMon.Config.Load.1.CTInput@4107#4%"
#define	SCML2	L"IO.CurrMon.Config.Load.1.DrivenBy@4106#4%"
#define	SCML3	L"IO.CurrMon.Config.Load.1.OCFthreshold@4109#4%"
#define	SCML4	L"IO.CurrMon.Config.Load.1.PLFthreshold@4108#4%"
#define	SCML5	L"IO.CurrMon.Config.Load.1.Resolution@4201#1%"

// IO.CurrentMonitor.Status  (46)
// Format A.C@D
#define	SCMS1	L"IO.CurrMon.Status.Load1Current@4173"
#define	SCMS2	L"IO.CurrMon.Status.Load2Current@4174"
#define	SCMS3	L"IO.CurrMon.Status.Load3Current@4175"
#define	SCMS4	L"IO.CurrMon.Status.Load4Current@4176"
#define	SCMS5	L"IO.CurrMon.Status.Load5Current@4177"
#define	SCMS6	L"IO.CurrMon.Status.Load6Current@4178"
#define	SCMS7	L"IO.CurrMon.Status.Load7Current@4179"
#define	SCMS8	L"IO.CurrMon.Status.Load8Current@4180"
#define	SCMS9	L"IO.CurrMon.Status.Load9Current@4181"
#define	SCMS10	L"IO.CurrMon.Status.Load10Current@4182"
#define	SCMS11	L"IO.CurrMon.Status.Load11Current@4183"
#define	SCMS12	L"IO.CurrMon.Status.Load12Current@4184"
#define	SCMS13	L"IO.CurrMon.Status.Load13Current@4185"
#define	SCMS14	L"IO.CurrMon.Status.Load14Current@4186"
#define	SCMS15	L"IO.CurrMon.Status.Load15Current@4187"
#define	SCMS16	L"IO.CurrMon.Status.Load16Current@4188"
#define	SCMS17	L"IO.CurrMon.Status.Ph1AllOff@4189"
#define	SCMS18	L"IO.CurrMon.Status.Ph2AllOff@4190"
#define	SCMS19	L"IO.CurrMon.Status.Ph3AllOff@4191"

// IO.FixedIO (47)
// Format A.C@D
#define	SIOF1	L"IO.FixedIO.A.PV@4226"
#define	SIOF2	L"IO.FixedIO.B.PV@4227"
#define	SIOF3	L"IO.FixedIO.D1.PV@4224"
#define	SIOF4	L"IO.FixedIO.D2.PV@4225"

// End of File
#endif

	