
#include "intern.hpp"

#include "mbtcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Modbus Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CModbusMasterTCPDeviceOptions, CUIItem);       

// Constructor

CModbusMasterTCPDeviceOptions::CModbusMasterTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));;

	m_Addr2  = DWORD(0);

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_fDisable16	= FALSE;
	
	m_fDisable15	= FALSE;

	m_fDisable5	= FALSE;

	m_fDisable6	= FALSE;

	m_PingReg	= 1;

	m_Max01		= 512;
	
	m_Max02		= 512;
	
	m_Max03		= 32;
	
	m_Max04		= 32;
	
	m_Max15		= 512;
	
	m_Max16		= 32;

	m_fIgnoreReadEx = FALSE;

	m_FlipLong	= 0;

	m_FlipReal	= 0;
	
	SetPages(2);
	}

// UI Managament

void CModbusMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		
		if( Tag.IsEmpty() || Tag == "Disable5" ) {

			pWnd->EnableUI("Disable15", !m_fDisable5);
			}

		if( Tag.IsEmpty() || Tag == "Disable6" ) {

			pWnd->EnableUI("Disable16", !m_fDisable6);
			}
	
		if( Tag.IsEmpty() || Tag == "Disable15" ) {

			pWnd->EnableUI("Max15", !m_fDisable15 && !m_fDisable5);

			pWnd->EnableUI("Disable5", !m_fDisable15);
			}

		if( Tag.IsEmpty() || Tag == "Disable16" ) {

			pWnd->EnableUI("Max16", !m_fDisable16 && !m_fDisable6);

			pWnd->EnableUI("Disable6", !m_fDisable16);
	      		}
		}
	}

// Download Support

BOOL CModbusMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{	
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_fDisable15));
	Init.AddByte(BYTE(m_fDisable16));
	Init.AddByte(BYTE(m_fDisable5));
	Init.AddByte(BYTE(m_fDisable6));
	Init.AddWord(WORD(m_PingReg));
	Init.AddWord(WORD(m_Max01));
	Init.AddWord(WORD(m_Max02));
	Init.AddWord(WORD(m_Max03));
	Init.AddWord(WORD(m_Max04));
	Init.AddWord(WORD(m_Max15));
	Init.AddWord(WORD(m_Max16));
	Init.AddLong(LONG(m_Addr2));
	Init.AddByte(BYTE(m_fIgnoreReadEx));
	Init.AddByte(BYTE(m_FlipLong));
	Init.AddByte(BYTE(m_FlipReal));

	return TRUE;
	}

// Meta Data Creation

void CModbusMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Addr2);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddBoolean(Disable15);
	Meta_AddBoolean(Disable16);
	Meta_AddBoolean(Disable5);
	Meta_AddBoolean(Disable6);
	Meta_AddInteger(PingReg);
	Meta_AddInteger(Max01);
	Meta_AddInteger(Max02);
	Meta_AddInteger(Max03);
	Meta_AddInteger(Max04);
	Meta_AddInteger(Max15);
	Meta_AddInteger(Max16);
	Meta_AddBoolean(IgnoreReadEx);
	Meta_AddInteger(FlipLong);
	Meta_AddInteger(FlipReal);
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Master TCP/IP Master Driver
//

// Instantiator

ICommsDriver *Create_ModbusMasterTCPDriver(void)
{
	return New CModbusMasterTCPDriver;
	}

// Constructor

CModbusMasterTCPDriver::CModbusMasterTCPDriver(void)
{
	m_wID		= 0x3505;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "TCP/IP Master";
	
	m_Version	= "1.05";
	
	m_ShortName	= "Modbus TCP/IP Master";

	AddSpaces();

	C3_PASSED();
	}

// Destructor

CModbusMasterTCPDriver::~CModbusMasterTCPDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CModbusMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CModbusMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CModbusMasterTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CModbusMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CModbusMasterTCPDeviceOptions);
	}

// Implementation

void CModbusMasterTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "4",  "Holding Registers",	   10, 1, 65535, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(2, "3",  "Analog Inputs",		   10, 1, 65535, addrWordAsWord, addrWordAsReal));
					  
	AddSpace(New CSpace(3, "0",  "Digital Coils",		   10, 1, 65535, addrBitAsBit));
	
	AddSpace(New CSpace(4, "1",  "Digital Inputs",		   10, 1, 65535, addrBitAsBit));

	AddSpace(New CSpace(5, "L4", "Holding Registers (32-bit)", 10, 1, 65535, addrLongAsLong, addrLongAsReal));
	
	AddSpace(New CSpace(6, "L3", "Analog Inputs (32-bit)",     10, 1, 65535, addrLongAsLong, addrLongAsReal)); 

	}

// Address Management

BOOL CModbusMasterTCPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CModbusTCPAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}
 

// Address Helpers

BOOL CModbusMasterTCPDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{	
	UINT uTable = pSpace->m_uTable;

	if ( uTable == 6 ) {

		uTable -= 4;

		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Table = uTable;

		return TRUE;
		}

	return FALSE;
	}

BOOL CModbusMasterTCPDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 1:
		case 2:
		case 5:
		case 6:

			return FALSE;
		}

	return TRUE;
	}


/////////////////////////////////////////////////////////////////////
//
// Modbus TCP Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CModbusTCPAddrDialog, CStdAddrDialog);
		
// Constructor

CModbusTCPAddrDialog::CModbusTCPAddrDialog(CModbusMasterTCPDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	
	}

// Overridables

BOOL CModbusTCPAddrDialog::AllowSpace(CSpace *pSpace)
{
	if( pSpace->m_uTable == 6 ) {

		return FALSE;
		}
	
	return TRUE;
	}


// End of File
