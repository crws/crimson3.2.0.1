
#include "intern.hpp"

#include "euro590.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEurotherm590DeviceOptions, CUIItem);

// Constructor

CEurotherm590DeviceOptions::CEurotherm590DeviceOptions(void)
{
	m_Group		= 0;
	m_Unit		= 0;
	}

// Download Support

BOOL CEurotherm590DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte((BYTE)m_Group);
	Init.AddByte((BYTE)m_Unit);

	return TRUE;
	}

// Meta Data Creation

void CEurotherm590DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	}

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Driver
//

// Instantiator

ICommsDriver *	Create_Euro590Driver(void)
{
	return New CEuro590Driver;
	}

// Constructor

CEuro590Driver::CEuro590Driver(void)
{
	m_wID		= 0x400B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SSD Drives";
	
	m_DriverName	= "590+";
	
	m_Version	= "1.10";
	
	m_ShortName	= "SSD Drives 590+";

	AddSpaces();
	}

// Binding Control

UINT CEuro590Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEuro590Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEuro590Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEurotherm590DeviceOptions);
	}

// Implementation

// Address Helpers

BOOL CEuro590Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT    uOffset = 0;

	PTXT	pError = NULL;

	CString Offset;

	if( pSpace->m_uTable >= 1 && pSpace->m_uTable <= 3 ) {

		UINT uFind = Min(Text.Find('_'), Min( Text.Find(' '), Text.Find('(') ));

		if( uFind == NOTHING ) {

			uFind = Text.Find('.');
			}

		Offset  = Text.Left( uFind );

		uOffset = tstrtoul( Offset, &pError, 10 );

		if( pError && pError[0] ) {
	
			Error.Set( CString(IDS_ERROR_ADDR),
				   0
				   );
		
			return FALSE;
			}

		if( pSpace->IsOutOfRange(uOffset) ) {
		
			Error.Set( CString(IDS_ERROR_OFFSETRANGE),
				   0
				   );
				
			return FALSE;
			}
		}

	else {
		if( pSpace->m_uTable == 0 || pSpace->m_uTable > 11 ) {
	
			Error.Set( CString(IDS_ERROR_ADDR),
				   0
				   );
		
			return FALSE;
			}
		}

	Addr.a.m_Type	= pSpace->m_uType;

	Addr.a.m_Table	= pSpace->m_uTable;
	
	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uOffset;
	
	return TRUE;
	}

BOOL CEuro590Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	BYTE bHi = 0;

	BYTE bLo = 0;

	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		switch ( pSpace->m_uTable ) {

			case 1:
			case 2:
			case 3:

				GetIDs( LOWORD(Addr.m_Ref), &bHi, &bLo );

				Text.Printf( "%s%d_ID%c%c",
					pSpace->m_Prefix,
					LOWORD(Addr.m_Ref),
					bHi,
					bLo
					);
				break;

			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:

				Text.Printf( "%s",
					pSpace->m_Prefix
					);
				break;

			default:
				return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation
void CEuro590Driver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "B",  "Boolean",				10, 0, 1971, addrByteAsByte));
	AddSpace(New CSpace(2, "W",  "Words and Long Words",		10, 0, 1971, addrLongAsLong));
	AddSpace(New CSpace(3, "R",  "Real Numbers",			10, 0, 1971, addrRealAsReal));
	AddSpace(New CSpace(4, "EE", "Last Error Code",			10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(5, "II", "Instrument Identity (Read Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(6, "V0", "Software Version (Read Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(7, "V1", "Keypad Version (Read Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(8, "!1", "Command (Write Only)",		10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(9, "!2", "State (Read Only)",		10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(10,"!3", "Save Command (Write Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(11,"!4", "Save State (Read Only)",		10, 0,    0, addrWordAsWord));
	}

// Helper

void CEuro590Driver::GetIDs( UINT uOffset, PBYTE pHi, PBYTE pLo )
{
// New formula provided September 2008

	BOOL fLow = uOffset < 936;

	UINT uHi  = fLow ? (uOffset + 360) / 36 : (uOffset - 936) / 26;
	UINT uLo  = fLow ? (uOffset + 360) % 36 : (uOffset - 936) % 26;

	if( fLow ) {

		*pHi = BYTE(uHi > 9 ? 'a' + uHi - 10 : '0' + uHi);
		*pLo = BYTE(uLo > 9 ? 'a' + uLo - 10 : '0' + uLo);
		}

	else {
		*pHi = BYTE('a' + uHi);
		*pLo = BYTE('A' + uLo);
		}
	}

/*
	if( uOffset == 888 ) {

		*pHi = 'a';
		*pLo = '1';

		return;
		}

	UINT uHi = uOffset / 36;

	UINT uLo = uOffset % 36;

	*pHi = LOBYTE( uHi + 'a' );

	*pLo = LOBYTE( uLo < 10 ? uLo + '0' : uLo + 'a' - 10 );
	}
*/

// End of File
