
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODSLV_HPP
	
#define	INCLUDE_MODSLV_HPP

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Driver Options
//

class CModbusSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModbusSlaveDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_FlipLong;
		UINT m_FlipReal;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Gateway Driver Options
//

class CModbusDeviceServerDriverOptions : public CModbusSlaveDriverOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModbusDeviceServerDriverOptions(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Server Device Options
//

class CModbusDeviceServerDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModbusDeviceServerDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Unit;
		UINT m_FlipLong;
		UINT m_FlipReal;
		UINT m_UseFlip;
	
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Base Driver
//

class CModbusSlaveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CModbusSlaveDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		
	protected:
		// Data
		UINT m_Drop;

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave RTU Driver
//

class CModbusSlaveRTUDriver : public CModbusSlaveDriver
{
	public:
		// Constructor
		CModbusSlaveRTUDriver(void);

	protected:
		// Data

		// Implementation

	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave ASCII Driver
//

class CModbusSlaveASCIIDriver : public CModbusSlaveDriver
{
	public:
		// Constructor
		CModbusSlaveASCIIDriver(void);

	protected:
		// Data

		// Implementation
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Serial Device Gateway Slave Driver
//

class CModbusDeviceServerSerialDriver : public CModbusSlaveDriver
{
	public:
		// Constructor
		CModbusDeviceServerSerialDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

	protected:

	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Address Selection
//

class CModbusSlaveAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CModbusSlaveAddrDialog(CModbusSlaveDriver &Driver, CAddress &Addr, BOOL fPart);
  		                
	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSpaceChange(UINT uID, CWnd &Wnd);
		void OnTypeChange(UINT uID, CWnd &Wnd);

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};


// End of File

#endif
