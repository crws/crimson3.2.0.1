
#include "intern.hpp"

#include "ofins.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Omron FINS Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COmronFINSMasterDeviceOptions, CUIItem);

// Constructor

COmronFINSMasterDeviceOptions::COmronFINSMasterDeviceOptions(void)
{
	m_Dna	 = 0;	 
	m_Da1	 = 0; 	
	m_Da2	 = 0;
	m_Sna	 = 0;
	m_Sa1	 = 0;
	m_Sa2	 = 0;
	m_Mode   = 0;
	}

// UI Managament

void COmronFINSMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL COmronFINSMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Dna));
	Init.AddByte(BYTE(m_Da1));
	Init.AddByte(BYTE(m_Da2));
	Init.AddByte(BYTE(m_Sna));
	Init.AddByte(BYTE(m_Sa1));
	Init.AddByte(BYTE(m_Sa2));
	Init.AddByte(BYTE(m_Mode));
			
	return TRUE;
	}

// Meta Data Creation

void COmronFINSMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Dna);
	Meta_AddInteger(Da1);
	Meta_AddInteger(Da2);
	Meta_AddInteger(Sna);
	Meta_AddInteger(Sa1);
	Meta_AddInteger(Sa2);
	Meta_AddInteger(Mode);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omron FINS UDP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COmronFINSMasterUDPDeviceOptions, COmronFINSMasterDeviceOptions);

// Constructor

COmronFINSMasterUDPDeviceOptions::COmronFINSMasterUDPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));
	m_Addr2	 = 0;
	m_Port	 = 9600;
	m_Keep   = TRUE;
	m_Ping   = FALSE;
	m_Time1  = 5000;
	m_Time2  = 2500;
	m_Time3  = 200;	
	m_Dna	 = 0;	 
	m_Da1	 = 0; 	
	m_Da2	 = 0;
	m_Dna2	 = 0;
	m_Da12	 = 0;
	m_Da22	 = 0;
	m_Sna	 = 0;
	m_Sa1	 = 0;
	m_Sa2	 = 0;
	m_Mode   = 0;
	m_Poll   = 0;
	m_NonFatal = 0;
	m_Fatal    = 0;
	}

// UI Managament

void COmronFINSMasterUDPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() || Tag == "Addr2" ) {

			BOOL fEnable = (m_Addr2 != 0);

			pWnd->EnableUI("Dna2", fEnable);
			pWnd->EnableUI("Da12", fEnable);
			pWnd->EnableUI("Da22", fEnable);
			}
		}
	}

// Download Support

BOOL COmronFINSMasterUDPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_Dna));
	Init.AddByte(BYTE(m_Da1));
	Init.AddByte(BYTE(m_Da2));
	Init.AddByte(BYTE(m_Sna));
	Init.AddByte(BYTE(m_Sa1));
	Init.AddByte(BYTE(m_Sa2));
	Init.AddByte(BYTE(m_Mode));
	Init.AddLong(LONG(m_Addr2));
	Init.AddByte(BYTE(m_Dna2));
	Init.AddByte(BYTE(m_Da12));
	Init.AddByte(BYTE(m_Da22));
	Init.AddWord(WORD(m_Poll));
	Init.AddByte(BYTE(m_NonFatal));
	Init.AddByte(BYTE(m_Fatal));

	return TRUE;
	}

// Meta Data Creation

void COmronFINSMasterUDPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Dna);
	Meta_AddInteger(Da1);
	Meta_AddInteger(Da2);
	Meta_AddInteger(Sna);
	Meta_AddInteger(Sa1);
	Meta_AddInteger(Sa2);
	Meta_AddInteger(Mode);
	Meta_AddInteger(Addr2);
	Meta_AddInteger(Dna2);
	Meta_AddInteger(Da12);
	Meta_AddInteger(Da22);
	Meta_AddInteger(Poll);
	Meta_AddInteger(NonFatal);
	Meta_AddInteger(Fatal);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omron FINS Master Driver
//

// Instantiator

ICommsDriver *Create_OmronFINSMasterDriver(void)
{
	return New COmronFINSMasterDriver;
	}

// Constructor

COmronFINSMasterDriver::COmronFINSMasterDriver(void)
{
	m_wID		= 0x4007;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omron";
	
	m_DriverName	= "Master via FINS";
	
	m_Version	= "1.02";
	
	m_ShortName	= "FINS Master";

	AddSpaces();
	}

// Configuration

CLASS COmronFINSMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COmronFINSMasterDeviceOptions);
	}

// Binding Control

UINT COmronFINSMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void COmronFINSMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

 // Implementation

void COmronFINSMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace('C',	"CIO",	"CIO Area",		10, 0, 6143,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('W',	"W",	"Work Area",		10, 0, 511,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('H',	"H",	"Holding Area",		10, 0, 511,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('A',	"A",	"Auxiliary Area",	10, 0, 959,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('T',	"TIM",	"Timer Area",		10, 0, 4095,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('N',	"CNT",	"Counter Area",		10, 0, 4095,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('D',	"DM",	"DM Area",		10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('E',	"EM",	"EM Area (Current)",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('0',	"E0_",	"EM Area Bank 0",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('1',	"E1_",	"EM Area Bank 1",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('2',	"E2_",	"EM Area Bank 2",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('3',	"E3_",	"EM Area Bank 3",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('4',	"E4_",	"EM Area Bank 4",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('5',	"E5_",	"EM Area Bank 5",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('6',	"E6_",	"EM Area Bank 6",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('7',	"E7_",	"EM Area Bank 7",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
//	AddSpace(New CSpace(addrNamed,	"CB",	"Current Bank (EM) No.",10, 1, 1,	addrWordAsWord));
	AddSpace(New CSpace('R',	"DR",   "Data Register",	10, 0, 15,	addrWordAsWord, addrWordAsReal));
	}

//////////////////////////////////////////////////////////////////////////
//
// Omron FINS UDP Master Driver
//

// Instantiator

ICommsDriver * Create_OmronFINSMasterUDPDriver(void)
{
	return New COmronFINSMasterUDPDriver;
	}

// Constructor

COmronFINSMasterUDPDriver::COmronFINSMasterUDPDriver(void)
{
	m_wID		= 0x3513;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omron";
	
	m_DriverName	= "Master via FINS";
	
	m_Version	= "1.01";
	
	m_ShortName	= "FINS Master";

	AddSpaces();
	}

// Binding Control

UINT COmronFINSMasterUDPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void COmronFINSMasterUDPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_UDPCount = 1;
	
	Ether.m_TCPCount = 0;
	}

// Configuration

CLASS COmronFINSMasterUDPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS COmronFINSMasterUDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COmronFINSMasterUDPDeviceOptions);
	}

// Implementation

void COmronFINSMasterUDPDriver::AddSpaces(void)
{
	AddSpace(New CSpace('C',	"CIO",	"CIO Area",		10, 0, 6143,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('W',	"W",	"Work Area",		10, 0, 511,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('H',	"H",	"Holding Area",		10, 0, 511,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('A',	"A",	"Auxiliary Area",	10, 0, 959,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('T',	"TIM",	"Timer Area",		10, 0, 4095,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('N',	"CNT",	"Counter Area",		10, 0, 4095,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('D',	"DM",	"DM Area",		10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('E',	"EM",	"EM Area (Current)",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('0',	"E0_",	"EM Area Bank 0",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('1',	"E1_",	"EM Area Bank 1",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('2',	"E2_",	"EM Area Bank 2",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('3',	"E3_",	"EM Area Bank 3",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('4',	"E4_",	"EM Area Bank 4",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('5',	"E5_",	"EM Area Bank 5",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('6',	"E6_",	"EM Area Bank 6",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('7',	"E7_",	"EM Area Bank 7",	10, 0, 32767,	addrWordAsWord, addrWordAsReal));
//	AddSpace(New CSpace(addrNamed,	"CB",	"Current Bank Number",	10, 1, 1,	addrWordAsWord));
	AddSpace(New CSpace('R',	"DR",	"Data Register",	10, 0, 15,	addrWordAsWord, addrWordAsReal));
	}

//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP-Series via FINS UDP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COmronFINsG9spMasterUDPDeviceOptions, COmronFINSMasterUDPDeviceOptions);

// Constructor

COmronFINsG9spMasterUDPDeviceOptions::COmronFINsG9spMasterUDPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 1, 15), MAKEWORD( 168, 192)));

	m_Poll   = 500;

	m_Keep   = 0;

	m_Ping   = 0;

	m_Time2  = 1000;

	m_Da1    = 1;
	}

//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP-Series via FINS UDP Master Driver
//

// Instantiator

ICommsDriver *Create_OmronFINsG9spMasterUDPDriver(void)
{
	return New COmronFINsG9spMasterUDPDriver;
	}

// Constructor

COmronFINsG9spMasterUDPDriver::COmronFINsG9spMasterUDPDriver(void)
{
	m_wID		= 0x4094;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omron";
	
	m_DriverName	= "G9SP-Series via FINS";
	
	m_Version	= "1.01";
	
	m_ShortName	= "G9SP Master";

	DeleteAllSpaces();

	AddSpaces();
	}

// Configuration

CLASS COmronFINsG9spMasterUDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(COmronFINsG9spMasterUDPDeviceOptions);
	}

// Implementation

void COmronFINsG9spMasterUDPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(        1,	"OCBI",	"Optional Communication Bits to G9SP",	10,   0, 31,	addrBitAsBit  ));
	AddSpace(New CSpace(        2,	"OCBO",	"Optional Communication Bits from G9SP",10,   0, 31,	addrBitAsBit  ));
	AddSpace(New CSpace(	    4,	"SID",	"Safety Input Terminal Data Flags", 	10,   0, 19,	addrBitAsBit  ));
	AddSpace(New CSpace(	   10,	"SOD",	"Safety Ouput Terminal Data Flags",	10,   0, 15,	addrBitAsBit  ));
	AddSpace(New CSpace(	   14,	"SIS",	"Safety Input Terminal Status Flags", 	10,   0, 19,	addrBitAsBit  ));
	AddSpace(New CSpace(	   20,	"SOS",	"Safety Ouput Terminal Status Flags",	10,   0, 15,	addrBitAsBit  ));
	AddSpace(New CSpace(	   24,	"SIE",	"Safety Input Terminal Error Causes", 	10,   0, 19,	addrByteAsByte));	// Nibbles
	AddSpace(New CSpace(	   48,	"SOE",	"Safety Ouput Terminal Error Causes",	10,   0, 19,	addrByteAsByte));	// Nibbles
	AddSpace(New CSpace(	   66,	"US",	"Unit Status",				10,   0, 15,	addrBitAsBit  ));	  
	AddSpace(New CSpace(addrNamed,	"ID",	"Configuration ID",			10,  68,  0,	addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "TIME",	"Unit Conduction Time (minutes)",	10,  70,  0,	addrLongAsLong));
//	AddSpace(New CSpace(	   94,	"PEI",	"Present Error Information",		10,   0, 11,	addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "ELC",	"Error Log Count",			10, 106,  0,	addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "OLC",	"Operation Log Count",			10, 107,  0,	addrByteAsByte));
	AddSpace(New CSpace(      108,	"EL",	"Error Log",				10,   0, 39,	addrByteAsByte));
	AddSpace(New CSpace(      148,	"OL",	"Operation Log",			10,   0, 39,	addrByteAsByte));
	}

BOOL COmronFINsG9spMasterUDPDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		if( Addr.a.m_Table == 1 ) {

			return FALSE;
			}
		}

	return TRUE;
	}

/////////////////////////////////////////////////////////////////////////
//
// Omron G9SP-Series FINS Device Options
//

// Dynamic Class

AfxImplementDynamicClass(COmronFINsG9spMasterDriverOptions, CUIItem);

// Constructor

COmronFINsG9spMasterDriverOptions::COmronFINsG9spMasterDriverOptions(void)
{
	m_Poll = 500;
	}

// Download Support

BOOL COmronFINsG9spMasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Poll));
			
	return TRUE;
	}

// Meta Data Creation

void COmronFINsG9spMasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Poll);
	}

//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP-Series FINS Master Driver
//

// Instantiator

ICommsDriver *Create_OmronFINsG9spMasterDriver(void)
{
	return New COmronFINsG9spMasterDriver;
	}

// Constructor

COmronFINsG9spMasterDriver::COmronFINsG9spMasterDriver(void)
{
	m_wID		= 0x4095;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omron";
	
	m_DriverName	= "G9SP-Series";
	
	m_Version	= "1.00";
	
	m_ShortName	= "G9SP-Series";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Configuration

CLASS COmronFINsG9spMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(COmronFINsG9spMasterDriverOptions);
	}

// Configuration

CLASS COmronFINsG9spMasterDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Binding Control

UINT COmronFINsG9spMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void COmronFINsG9spMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}


 // Implementation

void COmronFINsG9spMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(        1,	"OCBI",	"Optional Communication Bits to G9SP",	10,   0, 31,	addrBitAsBit  ));
	AddSpace(New CSpace(        2,	"OCBO",	"Optional Communication Bits from G9SP",10,   0, 31,	addrBitAsBit  ));
	AddSpace(New CSpace(	    4,	"SID",	"Safety Input Terminal Data Flags", 	10,   0, 19,	addrBitAsBit  ));
	AddSpace(New CSpace(	   10,	"SOD",	"Safety Ouput Terminal Data Flags",	10,   0, 15,	addrBitAsBit  ));
	AddSpace(New CSpace(	   14,	"SIS",	"Safety Input Terminal Status Flags", 	10,   0, 19,	addrBitAsBit  ));
	AddSpace(New CSpace(	   20,	"SOS",	"Safety Ouput Terminal Status Flags",	10,   0, 15,	addrBitAsBit  ));
	AddSpace(New CSpace(	   24,	"SIE",	"Safety Input Terminal Error Causes", 	10,   0, 19,	addrByteAsByte));	// Nibbles
	AddSpace(New CSpace(	   48,	"SOE",	"Safety Ouput Terminal Error Causes",	10,   0, 19,	addrByteAsByte));	// Nibbles
	AddSpace(New CSpace(	   66,	"US",	"Unit Status",				10,   0, 15,	addrBitAsBit  ));	  
	AddSpace(New CSpace(addrNamed,	"ID",	"Configuration ID",			10,  68,  0,	addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "TIME",	"Unit Conduction Time (minutes)",	10,  70,  0,	addrLongAsLong));
//	AddSpace(New CSpace(	   94,	"PEI",	"Present Error Information",		10,   0, 11,	addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "ELC",	"Error Log Count",			10, 106,  0,	addrByteAsByte));
	AddSpace(New CSpace(addrNamed,  "OLC",	"Operation Log Count",			10, 107,  0,	addrByteAsByte));
	AddSpace(New CSpace(      108,	"EL",	"Error Log",				10,   0, 39,	addrByteAsByte));
	AddSpace(New CSpace(      148,	"OL",	"Operation Log",			10,   0, 39,	addrByteAsByte));
	}

BOOL COmronFINsG9spMasterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		if( Addr.a.m_Table == 1 ) {

			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
