
#include "intern.hpp"

#include "ciasdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CANOpen Driver Base Class
//

// Constructor

CCANOpenDriver::CCANOpenDriver(void)
{
	m_Manufacturer	= "CANOpen";

	m_pDlg = NULL;
	}

// Driver Data

UINT CCANOpenDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagRemotable;
	}

// Binding Control

UINT CCANOpenDriver::GetBinding(void)
{
	return bindCAN;
	}

void CCANOpenDriver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 125000;

	CAN.m_Terminate = FALSE;
	}

// Address Management

BOOL CCANOpenDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	DeleteAllSpaces();
	
	AddSpaces(pConfig);

	CSpace * pSpace = GetSpace(Text);

	if( m_pDlg && pSpace ) {

		m_pDlg->SetSpace(pSpace);
		}
	
	return CStdCommsDriver::ParseAddress(Error, Addr, pConfig, Text);
	}

BOOL CCANOpenDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	DeleteAllSpaces();
	
	AddSpaces(pConfig);

	CSpace * pSpace = GetSpace(Addr);

	if( m_pDlg && pSpace ) {

		m_pDlg->SetSpace(pSpace);
		}
		
	return CStdCommsDriver::ExpandAddress(Text, pConfig, Addr);
	}

BOOL CCANOpenDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	DeleteAllSpaces();
	
	AddSpaces(pConfig);
			
	CCANOpenDialog Dlg(*this, Addr, pConfig, fPart);

	m_pDlg = &Dlg;

	BOOL fSuccess = Dlg.Execute(CWnd::FromHandle(hWnd));

	m_pDlg = NULL;

	return fSuccess;
	}

BOOL CCANOpenDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		CAddress Addr;

		Addr.a.m_Type = addrByteAsByte;

		if( uItem > 0 ) {

			if ( m_List.GetCount() ) {

				CSpace * pSpace = m_List[m_n];

				if( pSpace ) {

					Addr.a.m_Type = pSpace->m_uType;
					}
				}
			}

		DeleteAllSpaces();

		AddSpaces(pConfig);

		for( INDEX i = m_List.GetHead(); !m_List.Failed(i); m_List.GetNext(i) ) {

			if( m_List[i]->MatchSpace(Addr) ) { 

				m_n = i;

				break;
				}
			}
		}

	return CStdCommsDriver::ListAddress(pRoot, pConfig, uItem, Data);
	}

// Address Helpers

BOOL CCANOpenDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type = StripType(pSpace, Text);

	UINT  uSub = 0;

	UINT uFind = Text.Find('/');

	UINT uRestrict = pConfig->GetDataAccess("Restrict")->ReadInteger(pConfig);

	CString Index = Text;

	if( uFind < NOTHING ) {

		uSub = tatoi(Text.Mid(uFind+1));

		Text = Text.Left(uFind);

		if( uSub < 0 || uSub > 255 ) {	 

			Error.Set( "Invalid sub-element number",
				   0
				   );

			return FALSE;
			}

		Index = Text;
	      	}

	Text = Text + "." + Type;

	PTXT pError = NULL;

	UINT uIndex = tstrtoul(Index, &pError, pSpace->m_uRadix); 

	if( uRestrict == 2 && uIndex > 255 ) {

		Error.Set( CString(IDS_ERROR_OFFSETRANGE),
			   0
			   );
			
		return FALSE;
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		UINT uInc = pConfig->GetDataAccess("Increment")->ReadInteger(pConfig);

		Addr.a.m_Table = IsSlave() ? 0x10 : addrNamed;

		if( uInc ) {

			Addr.a.m_Offset  = uSub;

			UINT Index       = EncodeIndex(uIndex, pConfig);

			Addr.a.m_Extra   = (Index & 0x000F);

			Addr.a.m_Table  |= (Index & 0x00F0) >> 4;

			Addr.a.m_Offset |= (Index & 0xFF00) >> 0;
			
			return TRUE;
			}
		
		Addr.a.m_Extra   = (uSub & 0x000F) >> 0;

		Addr.a.m_Table  |= (uSub & 0x00F0) >> 4;

		Addr.a.m_Offset  = EncodeIndex(uIndex, pConfig);

		return TRUE; 
		}

	return FALSE;
	}

BOOL CCANOpenDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uItem = 0;

		UINT uSub  = 0;

		UINT uInc  = pConfig->GetDataAccess("Increment")->ReadInteger(pConfig);

		if( uInc ) {

			uItem  = Addr.a.m_Extra;
			
			uItem |= (Addr.a.m_Table  & 0x000F) << 4;
			
			uItem |= (Addr.a.m_Offset & 0xFF00) << 0;
		       
			uItem  = DecodeIndex(uItem, pConfig);

			uSub   = (Addr.a.m_Offset & 0x00FF);
			}
		else {
			uItem  = Addr.a.m_Offset;

			uItem  = DecodeIndex(uItem, pConfig);

			uSub   = Addr.a.m_Extra;
			
			uSub  |= (Addr.a.m_Table & 0x000F) << 4;
			}

		if( uSub || uInc ) {

			Text.Printf( "%s%s/%u",
				     pSpace->m_Prefix,
				     pSpace->GetValueAsText(uItem),
				     uSub
				     );
			}
		else {
			Text.Printf( "%s%s", 
				     pSpace->m_Prefix, 
				     pSpace->GetValueAsText(uItem)
				     );
			}

		return TRUE;
		}
	    
	return FALSE;
	}

// Helpers

UINT CCANOpenDriver::EncodeIndex(UINT uIndex, CItem *pConfig)
{
	CString Offset  = pConfig->GetDataAccess("Offset")->ReadString(pConfig);

	UINT    uOffset = tstrtoul(Offset, NULL, 16);

	if( uOffset ) {

		BOOL fDir = pConfig->GetDataAccess("Type")->ReadInteger(pConfig);

		if( !fDir ) {

			return uOffset - uIndex;
			}
	
		return uOffset + uIndex;
		}

	return uIndex;
	}

UINT CCANOpenDriver::DecodeIndex(UINT uIndex, CItem *pConfig)
{
	CString Offset  = pConfig->GetDataAccess("Offset")->ReadString(pConfig);

	UINT    uOffset = tstrtoul(Offset, NULL, 16);

	if( uOffset ) {

		BOOL fDir = pConfig->GetDataAccess("Type")->ReadInteger(pConfig);

		if( !fDir ) {

			return uOffset - uIndex;
			}
	
		return uIndex - uOffset;
		}

	return uIndex;
	}

// Implementation

void CCANOpenDriver::AddSpaces(CItem *pConfig)
{
	AfxAssert(pConfig);

	UINT uEntry = 16;

	UINT uSpace = IsSlave() ? 0x10 : addrNamed;
	
	if( pConfig ) {
		
		uEntry = pConfig->GetDataAccess("Entry")->ReadInteger(pConfig) ? 10 : 16;
		}

	AddSpace(New CSpaceCAN(uSpace, "B", "Byte Registers", uEntry, 0, 65535, addrByteAsByte));

	AddSpace(New CSpaceCAN(uSpace, "W", "Word Registers", uEntry, 0, 65535, addrWordAsWord));

	AddSpace(New CSpaceCAN(uSpace, "L", "Long Registers", uEntry, 0, 65535, addrLongAsLong));
	}

// Slave Detection

BOOL CCANOpenDriver::IsSlave(void) 
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Master
//

// Instantiator

ICommsDriver * Create_CANOpenSDODriver(void)
{
	return New CCANOpenSDO;
	}

// Constructor

CCANOpenSDO::CCANOpenSDO(void)
{
	m_wID		= 0x4003;

	m_uType		= driverMaster;
	
	m_DriverName	= "SDO Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "SDO Master";

	m_DevRoot	= "DEV";
	}

// Configuration

CLASS CCANOpenSDO::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCANOpenSDODeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Slave
//

// Instantiator

ICommsDriver * Create_CANOpenSlaveDriver(void)
{
	return New CCANOpenSlave;
	}

// Constructor

CCANOpenSlave::CCANOpenSlave(void)
{
	m_wID		= 0x3404;

	m_uType		= driverSlave;
	
	m_DriverName	= "SDO Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "SDO Slave";

	m_DevRoot	= "DEV";

	m_fSingle       = TRUE;
	}

// Configuration

CLASS CCANOpenSlave::GetDriverConfig(void)
{
	return AfxRuntimeClass(CCANOpenSlaveDriverOptions);
	} 


// Slave Detection

BOOL CCANOpenSlave::IsSlave(void) 
{
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANOpenSDODeviceOptions, CUIItem);

// Constructor

CCANOpenSDODeviceOptions::CCANOpenSDODeviceOptions(void)
{
	m_Drop	    = 0;

	m_Init	    = 0;

	m_Ping      = "1000";

	m_Offset    = "0";

	m_Type      = 0;

	m_Time      = 50;

	m_Restrict  = 0;

	m_Increment = 0;

	m_Entry     = 0;

	m_uPing     = 0x1000;

	m_uOffset   = 0;

	m_Delay     = 0;

	m_Transfer  = 0;

	m_BackOff   = 0;
	}

// UI Managament

void CCANOpenSDODeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Ping" ) {

		SetHexadecimal(pWnd, Tag, m_Ping, m_uPing);
		}

	if( Tag.IsEmpty() || Tag == "Offset" ) {

		SetHexadecimal(pWnd, Tag, m_Offset, m_uOffset);

		pWnd->EnableUI("Type", (m_uOffset != 0));

		pWnd->UpdateUI("Type");
		}

	if( Tag.IsEmpty() || Tag == "Init" ) {

		pWnd->EnableUI("Delay", (m_Init != 0));
		}
	
	if( Tag == "Increment" ) {   

		if( pWnd->YesNo(CString(IDS_CAN_CHNG_INC)) == IDNO ) {

			pItem->GetDataAccess("Increment")->WriteInteger(pItem, !m_Increment);
			
			pWnd->UpdateUI("Increment");
			} 
		}

	if( Tag.IsEmpty() ) {

		pWnd->UpdateUI("Entry");
		}
	}

// Persistance

void CCANOpenSDODeviceOptions::PostLoad(void)
{
	m_uPing   = tstrtoul(m_Ping, NULL, 16);

	m_uOffset = tstrtoul(m_Offset, NULL, 16);
	}

// Download Support

BOOL CCANOpenSDODeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Init));
	Init.AddWord(WORD(m_uPing));
	Init.AddWord(WORD(m_Time));
	Init.AddByte(BYTE(m_Restrict));
	Init.AddByte(BYTE(m_Increment));
	Init.AddWord(WORD(m_Delay));
	Init.AddByte(BYTE(m_Transfer));
	Init.AddWord(WORD(m_BackOff));
				
	return TRUE;
	}

// Meta Data Creation

void CCANOpenSDODeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Init);
	Meta_AddString (Ping);
	Meta_AddString (Offset);
	Meta_AddInteger(Type);
	Meta_AddInteger(Time);
	Meta_AddInteger(Restrict);
	Meta_AddInteger(Increment);
	Meta_AddInteger(Entry);
	Meta_AddInteger(Delay);
	Meta_AddInteger(Transfer);
	Meta_AddInteger(BackOff);
	}

// Helper

void CCANOpenSDODeviceOptions::SetHexadecimal(CUIViewWnd *pWnd, CString Tag, CString &Text, UINT &uTarget)
{
	UINT uLength = Text.GetLength();

	if ( uLength > 4 ) {

		uLength = 4;
		}

	Text = Text.Left(uLength);

	Text = Text.ToUpper();

	for( UINT u = 0; u < uLength; u++ ) {

		if ( Text[u] < '0' || Text[u] > 'F' ) {

			pWnd->Error(CPrintf(CString(IDS_DRIVER_ADDRRANGE), TEXT("0x0000"), TEXT("0xFFFF")));

			break;
			}
		}
		
	uTarget = tstrtoul(Text, NULL, 16);

	pWnd->UpdateUI(Tag);
	}

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANOpenSlaveDriverOptions, CUIItem);

// Constructor

CCANOpenSlaveDriverOptions::CCANOpenSlaveDriverOptions(void)
{
	m_Drop	    = 0;

	m_Offset    = "0";

	m_Type      = 0;

	m_Restrict  = 0;

	m_Increment = 1;

	m_Entry     = 0;

	m_NodeGuard = 0;

	m_Update    = 0;
	}


// Persistance

void CCANOpenSlaveDriverOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	if( m_Update ) {
					   
		GetObjects();

		CreateEDSFile();
		} 
	}

// UI Managament

void CCANOpenSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {
	
		if( Tag == "Increment" ) {   

			if( pWnd->YesNo(CString(IDS_CAN_CHNG_INC)) == IDNO ) {

				pItem->GetDataAccess("Increment")->WriteInteger(pItem, !m_Increment);
			
				pWnd->UpdateUI("Increment");
				} 
			}

		if( Tag == "Update" || Tag.IsEmpty() ) {

			pWnd->GetDlgItem(0x7F00).EnableWindow(!m_Update);
			}
		       
		if( Tag == "Button7F00") {

			GetObjects();

			CreateEDSFile();
			}  
		
		if( Tag.IsEmpty() ) {

			pWnd->UpdateUI("Entry");
			}
		}
	}

// Download Support

BOOL CCANOpenSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Increment));
	Init.AddByte(BYTE(m_NodeGuard));
				
	return TRUE;
	}

// Meta Data Creation

void CCANOpenSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddString (Offset);
	Meta_AddInteger(Type);
	Meta_AddInteger(Restrict);
	Meta_AddInteger(Increment);
	Meta_AddInteger(Entry);
	Meta_AddInteger(NodeGuard);
	Meta_AddString(File);
	Meta_AddInteger(Update);

	}

// EDS File

void CCANOpenSlaveDriverOptions::CreateEDSFile(void)
{
	HANDLE hFile = CreateFile( m_File,
				   GENERIC_WRITE,
				   0,
				   NULL,
				   CREATE_ALWAYS,
				   FILE_ATTRIBUTE_NORMAL,
				   NULL
				   );

	if( hFile != INVALID_HANDLE_VALUE ) {

		CloseHandle(hFile);

		CTextStreamMemory Stream;

		CFilename Name = m_File;     

		if( Stream.OpenSave() ) {

			afxThread->SetWaitMode(TRUE);

			WriteFileInfo(Stream);

			WriteDeviceInfo(Stream);

			WriteDummyUsage(Stream);

			WriteObjects(Stream);

			WriteComments(Stream);
		
			Stream.SaveToFile(Name, saveRaw);

			afxThread->SetWaitMode(FALSE);
			}
		}
	}

void CCANOpenSlaveDriverOptions::WriteFileInfo(ITextStream &Stream)
{
	UINT uFile = m_File.Find('\\');

	SYSTEMTIME Time;

	GetLocalTime(&Time);

	PCTXT TimeFormat = L"%2.2u:%2.2u%s";

	PCTXT DateFormat = L"%2.2u-%2.2u-%4.4u";

	CString TimeText;

	CString DateText;

	CString Suffix = Time.wHour < 12 ? "AM" : "PM";

	TimeText.Printf(TimeFormat, Time.wHour > 12 ? Time.wHour - 12 : Time.wHour, Time.wMinute, Suffix);
	
	DateText.Printf(DateFormat, Time.wMonth, Time.wDay, Time.wYear);
       
	Stream.PutLine(CString(L"[FileInfo]\r\n"));
	Stream.PutLine(CPrintf(L"FileName=%s\r\n", m_File.Mid(uFile + 1))); 
	Stream.PutLine(CString(L"FileVersion=1\r\n")); 
	Stream.PutLine(CString(L"FileRevision=2\r\n")); 
	Stream.PutLine(CString(L"EDSVersion=4.0\r\n")); 
	Stream.PutLine(CString(L"Description=EDS for G3-SERVER\r\n")); 
	Stream.PutLine(CPrintf(L"CreationTime=%s\r\n", TimeText)); 
	Stream.PutLine(CPrintf(L"CreationDate=%s\r\n", DateText)); 
	Stream.PutLine(CString(L"CreatedBy=Author\r\n"));
	Stream.PutLine(CPrintf(L"ModificationTime=%s\r\n", TimeText));	
	Stream.PutLine(CPrintf(L"ModificationDate=%s\r\n", DateText));	
	Stream.PutLine(CString(L"ModifiedBy=Author\r\n")); 
	Stream.PutLine(CString(L"\r\n"));
	}

void CCANOpenSlaveDriverOptions::WriteDeviceInfo(ITextStream &Stream)
{
	CDatabase * pBase = ( CDatabase * ) this->GetDatabase();

	CString Model = pBase->GetModelName();
		
	Stream.PutLine(CString(L"[DeviceInfo]\r\n")); 
	Stream.PutLine(CString(L"VendorName=Red Lion Controls Inc\r\n")); 
	Stream.PutLine(CString(L"VendorNumber=0x0000022C\r\n")); 
	Stream.PutLine(CString(L"ProductName=G3\r\n")); 
	Stream.PutLine(CString(L"ProductNumber=0x4733\r\n")); 
	Stream.PutLine(CString(L"RevisionNumber=1\r\n")); 
	Stream.PutLine(CPrintf(L"OrderCode=%s\r\n", Model)); 
	Stream.PutLine(CString(L"BaudRate_10=1\r\n")); 
	Stream.PutLine(CString(L"BaudRate_20=1\r\n")); 
	Stream.PutLine(CString(L"BaudRate_50=1\r\n"));	
	Stream.PutLine(CString(L"BaudRate_125=1\r\n"));	
	Stream.PutLine(CString(L"BaudRate_250=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_500=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_800=1\r\n"));
	Stream.PutLine(CString(L"BaudRate_1000=1\r\n"));
	Stream.PutLine(CString(L"SimpleBootUpMaster=0\r\n"));
	Stream.PutLine(CString(L"SimpleBootUpSlave=0\r\n"));
	Stream.PutLine(CString(L"Granularity=8\r\n"));
	Stream.PutLine(CString(L"DynamicChannelsSupported=0\r\n"));
	Stream.PutLine(CString(L"GroupMessaging=0\r\n"));
	Stream.PutLine(CString(L"NrOfRXPDO=0\r\n"));
	Stream.PutLine(CString(L"NrOfTXPDO=0\r\n"));
	Stream.PutLine(CString(L"LSS_Supported=0\r\n"));
	Stream.PutLine(CString(L"\r\n"));
	}

void CCANOpenSlaveDriverOptions::WriteDummyUsage(ITextStream &Stream)
{
	Stream.PutLine(CString(L"[DummyUsage]\r\n")); 
	Stream.PutLine(CString(L"Dummy0001=0\r\n")); 
	Stream.PutLine(CString(L"Dummy0002=1\r\n")); 
	Stream.PutLine(CString(L"Dummy0003=1\r\n")); 
	Stream.PutLine(CString(L"Dummy0004=1\r\n")); 
	Stream.PutLine(CString(L"Dummy0005=1\r\n")); 
	Stream.PutLine(CString(L"Dummy0006=1\r\n")); 
	Stream.PutLine(CString(L"Dummy0007=1\r\n")); 
	Stream.PutLine(CString(L"\r\n"));
	}

void CCANOpenSlaveDriverOptions::WriteObjects(ITextStream &Stream)
{
	WriteMandatoryObjects(Stream);

	WriteOptionalObjects(Stream);

	WriteManufacturerObjects(Stream);
	
	UINT uStart = 0;
	
	UINT uEnd = m_Objects.Find('?');

	UINT uLen = m_Objects.GetLength();

	CString Object = "";

	while( uEnd < uLen ) {

		Object = m_Objects.Mid(uStart, uEnd - uStart);

		WriteObject(Stream, Object, Object);

		uStart = uEnd + 1;

		uEnd = m_Objects.Find('?', uStart);
		}
	}

void CCANOpenSlaveDriverOptions::WriteComments(ITextStream &Stream)
{
	Stream.PutLine(CString(L"[Comments]\r\n")); 
	Stream.PutLine(CString(L"Lines=0\r\n")); 
	Stream.PutLine(CString(L"\r\n"));
	}


void CCANOpenSlaveDriverOptions::WriteMandatoryObjects(ITextStream &Stream)
{
	Stream.PutLine(CString(L"[MandatoryObjects]\r\n")); 
	Stream.PutLine(CString(L"SupportedObjects=3\r\n")); 
	Stream.PutLine(CString(L"1=0x1000\r\n")); 
	Stream.PutLine(CString(L"2=0x1001\r\n"));
	Stream.PutLine(CString(L"3=0x1018\r\n"));
	Stream.PutLine(CString(L"\r\n"));
		
	CString Name [] = { L"1000", L"1001", L"1018" };

	CString Desc  [] = { L"Device Type", L"Error Register", L"Identity Object" };
	
	for( UINT u = 0; u < elements(Name); u++ ) {

		WriteObject(Stream, Name[u], Desc[u]);
		}
	}

void CCANOpenSlaveDriverOptions::WriteOptionalObjects(ITextStream &Stream)
{
	// Non-Mandatory objects 0x1000 - 0x1FFF and 0x6000 - 0xFFFF

	UINT uCount = 0;

	CString Man = GetMandatoryObjects(OBJ_OPT, uCount);

	CString Rem = GetRemainingObjects(OBJ_OPT, uCount);
		
	Stream.PutLine(CString(L"[OptionalObjects]\r\n"));
	Stream.PutLine(CPrintf(L"SupportedObjects=%u\r\n", uCount));

	Stream.PutLine(Man);
	Stream.PutLine(Rem);
	Stream.PutLine(CString(L"\r\n"));

	CString Name [] = { L"1008", L"1009", L"100A" };

	CString Desc  [] = { L"Manufacturer Device Name",
		             L"Manufacturer Hardware Version",
			     L"Manufacturer Software Version" };
	
	for( UINT u = 0; u < elements(Name); u++ ) {

		WriteObject(Stream, Name[u], Desc[u]);
		}
	}

void CCANOpenSlaveDriverOptions::WriteManufacturerObjects(ITextStream &Stream)
{
	// Objects 0x2000 - 0x5FFF
		
	UINT uCount = 0;

	CString Man = GetMandatoryObjects(OBJ_MANU, uCount);

	CString Rem = GetRemainingObjects(OBJ_MANU, uCount);

	Stream.PutLine(CString(L"[ManufacturerObjects]\r\n"));
	Stream.PutLine(CPrintf(L"SupportedObjects=%u\r\n", uCount));
	Stream.PutLine(Man);
	Stream.PutLine(Rem);
	Stream.PutLine(CString(L"\r\n"));
	}

void CCANOpenSlaveDriverOptions::WriteObject(ITextStream &Stream, CString Name, CString Desc)
{
       	if( Desc != Name ) {

		WriteMandatoryObject(Stream, Name, Desc);

		return;
		}
	
	CString Label [] = {L"ParameterName=",L"ObjectType=",L"DataType=",L"AccessType=",L"DefaultValue=",L"PDOMapping=", L"CompactSubObj="};

	CString Title	= GetName(Name);
	CString Param	= GetDesc(Name);
	CString Object	= GetObjectType(Name);
	CString Data	= GetDataType(Name);
	CString Access  = GetAccessType(Name);

	UINT uIndex = tstrtoul(PCTXT(Title), NULL, 16);

	if( !IsObjectValid(OBJ_OPT, uIndex) ) {
		
		if( !IsObjectValid(OBJ_MANU, uIndex) ) {
		
			return;
			}
		}

	UINT uSize = GetSize(Name);

	UINT uFind = Name.Find('/');

	if( m_Increment == 0 )  {

		CString Increment = Desc.Mid(1, uFind - 1);

		UINT uIncrement   = tstrtoul(PCTXT(Increment), NULL, 16);

		for( UINT u = 0; u < uSize; u++, uIncrement++ ) {

			Increment.Printf("%4.4X", uIncrement); 	  
			
			WriteMandatoryObject(Stream, Increment, Name.Left(1) + Increment);
			}

		return;		    
		}

	CString Sub = Desc.Mid(uFind + 1, Name.Find('^') - uFind);

	UINT uSub    = tstrtoul(PCTXT(Sub), NULL, 10);
	
	Sub.Printf("%u\r\n", IsPDO() ? uSub : uSize + uSub);
	
	Stream.PutLine(CPrintf(L"[%s]\r\n", Title));	       
	Stream.PutLine(CPrintf(L"%s%s\r\n", Label[0], Name.Left(Title.GetLength() + 1)));
	Stream.PutLine(CPrintf(L"%s8\r\n", Label[1]));
	Stream.PutLine(CPrintf(L"%s%s\r\n", Label[2], Data));
	Stream.PutLine(CPrintf(L"%s%s\r\n", Label[3], Access));
	Stream.PutLine(CPrintf(L"%s\r\n", Label[4]));
	Stream.PutLine(CPrintf(L"%s%s", Label[5], (IsPDO() ? L"1\r\n" : L"0\r\n")));
	Stream.PutLine(CPrintf(L"%s%s\r\n", Label[6], Sub));
	}

void CCANOpenSlaveDriverOptions::WriteMandatoryObject(ITextStream &Stream, CString Name, CString Desc)
{
	if( WriteObjectWithSubIndexes(Stream, Name, Desc) ) {

		return;
		}
	
	CString Label [] = {L"ParameterName=",L"ObjectType=",L"DataType=",L"AccessType=",L"DefaultValue=",L"PDOMapping="};

       	Stream.PutLine(CString(L"["));
	Stream.PutLine(Name);
	Stream.PutLine(CString(L"]\r\n"));

	CString Object;
     
	for( UINT u = 0;  u < elements(Label); u++ ) {
			
		Object += Label[u];

		switch( u ) {

			case 0: 
				Object += Desc;
				break;
	
			case 1:
				Object += "0x7";
				break;

			case 2:
				Object += "0x0007";
				break;

			case 3:
				Object += "ro";
				break;

			case 4:
				break;
				
			case 5:
				Object += IsObjectMandatory(tstrtoul(Name, NULL, 16), OBJ_MAND) ? "0" :
					  IsObjectMandatory(tstrtoul(Name, NULL, 16), OBJ_OPT ) ? "0" :
					  IsPDO() ? "1" : "0";
				break;			   
				
			}

		Object += "\r\n";
		}

	Object += "\r\n";
	
	Stream.PutLine(Object);
	}

BOOL CCANOpenSlaveDriverOptions::WriteObjectWithSubIndexes(ITextStream &Stream, CString Name, CString Desc)
{
	if( Name == "1018" && Desc == "Identity Object" ) {

		WriteMandatoryObjectWithSubIndexes(Stream, Name, Desc);

		return TRUE;
		}

	return FALSE;
	}

void CCANOpenSlaveDriverOptions::WriteMandatoryObjectWithSubIndexes(ITextStream &Stream, CString Name, CString Desc)
{
	CString Object;
	
	Stream.PutLine(CString(L"["));
	Stream.PutLine(Name);
	Stream.PutLine(CString(L"]\r\n"));

	CString Label [] = {L"SubNumber=",L"ParameterName=",L"ObjectType="};

	CString Sub	= "";
	CString Type    = "";
		
	if( Name == "1018" ) {

		Sub  = "2";

		Type = "9";
		}

	for( UINT u = 0; u < elements(Label); u++ ) {

		Object += Label[u];

		switch(u) {

			case 0:
				Object += Sub;
				break;

			case 1:
				Object += Desc;
				break;

			case 2:
				Object += Type;
				break;
			}

		Object += "\r\n";
		}

	Object += "\r\n";
	
	Stream.PutLine(Object);

	UINT uSub = tstrtol(Sub, NULL, 10);

	CString SubLabel [] = {L"ParameterName=",L"ObjectType=",L"DataType=",L"AccessType=",L"PDOMapping=",L"DefaultValue="};
       
	CString Param    [] = {L"Number of entries", L"Vendor ID"};
	
	for( UINT a = 0; a < uSub; a++ ) {

		Object  = "[";
		Object += Name;
		Object += "sub";

		Object.Printf(Object + "%u", a);
		Object += "]\r\n";

		for( UINT b = 0; b < elements(SubLabel); b++ ) {

			Object += SubLabel[b];

			switch(b) {

				case 0:
					if( Name != Desc ) {
					
						Object += Param[a];
						}
					else {
						Object += Name + "sub";
						Object.Printf(Object + "%u", a);
						}
					break;

				case 1:
					Object += "0x7";
					break;

				case 2:	
					if( Name == "1018") {

						Object.Printf(Object + "0x00%2.2x", (a == 0 ? 5 : 7));
						}
					break;

				case 3:
					if( Name == "1018") {

						Object += "RO";
						}
					break;

				case 4:
					Object += "0";
					break;

				case 5:
					if( Name == "1018") {
					
						Object += (a == 0 ? "1" : "0x0000022C");
						}
					break;

				}

			Object += "\r\n";
			}

		Object +="\r\n";

		Stream.PutLine(Object);
		} 
	
	Stream.PutLine(CString(L"\r\n"));
	}

BOOL CCANOpenSlaveDriverOptions::WriteFile(HANDLE hFile, PCTXT pText)
{
	return WriteFile(hFile, PBYTE(pText), tstrlen(pText));
	}

BOOL CCANOpenSlaveDriverOptions::WriteFile(HANDLE hFile, CString Text)
{
	return WriteFile(hFile, PBYTE(PCTXT(Text)), Text.GetLength());
	}

BOOL CCANOpenSlaveDriverOptions::WriteFile(HANDLE hFile, PBYTE pData, DWORD uLen)
{
	return ::WriteFile(hFile, pData, uLen, &uLen, NULL);
	}

BOOL CCANOpenSlaveDriverOptions::GetObjects(void)
{
	CTextStreamMemory Stream;

	CDatabase * pBase = GetDatabase();
	
	if( Stream.LoadFromFile(pBase->GetFilename()) ) {

		CTreeFile Tree;	

		if( Tree.OpenLoad(Stream) ) {

			afxThread->SetWaitMode(TRUE);

			m_Objects.Empty();

			CString Temp = "";

			CString Path [] = {"C3Data",
					   "System",
					   "Comms",
					   "Option0",
					   "Ports",
					   "CommsPortCAN",
					   "Devices",
					   "Fixed",
					   "SysBlocks",
					   "Fixed",
					   "MapBlocks",
					   "Fixed",
					   "Text",
					   "Size",
					   "Write",
					   "Regs",
					"Fixed"};
					

			UINT    uState [] = {0,	 // C3data     
					    0,	 // system
					    0,	 // comms
					    0,	 // option0
					    0,	 // ports
					    0, 	 // can
					    0,	 // devices
					    1,	 // fixed
					    0,	 // sysblocks
					    1,	 // fixed
					    0,	 // mapblocks
					    1,   // fixed			 	   
					    5,	 // text
					    6,	 // size
					    7,	 // write
					    0,	 // regs
					    8};  // fixed
					    				    
					    
			UINT uPos = 0;

			UINT uSize = 0;

			UINT uObject = 0;

			UINT uCollect = 0;

			UINT uPort = 0;

			BOOL fHit = FALSE;

			Tree.SetCompressed();

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				if( Name == Path[uPos] ) {

					switch( uState[uPos] ) {

						case 0:	GetObject(Tree, uObject);
							break;

						case 1: 
							GetCollect(Tree, uCollect, uObject);
							break;

						case 2:	if( fHit ) {

								GetCollect(Tree, uCollect, uObject);
								}
							else {
							 	GetNext(Tree, uObject, uCollect);
								uPos = 7;
								}
							break;

						case 3: 
							uPort = Tree.GetValueAsInteger();
							break;

						case 4:							
							fHit = Tree.GetValueAsInteger() == uPort;
							break;

						case 5:	m_Objects += Tree.GetValueAsString();
							m_Objects += "^";
							break;

						case 6:
							uSize = Tree.GetValueAsInteger();
							Temp.Printf("%u^", uSize);
							m_Objects += Temp;
							break;

						case 7:
							Temp.Printf("%u?", Tree.GetValueAsInteger());
							m_Objects += Temp;
							break;
				        
						case 8:	
							GetNext(Tree, uObject, uCollect, uSize);
							uPos = 11;
							break;

						case 9:	
							Tree.EndObject();

							uObject--;
												
							Tree.EndObject();
							
							uObject--;

							GetObject(Tree, uObject);
							break;
					       	}

					uPos++;
					
					if( uPos == elements(Path) ) {

						break;
						}
					}
				}

			afxThread->SetWaitMode(FALSE);

			while( uCollect > 0 ) {

				Tree.EndCollect();

				uCollect--;
				}

			while( uObject > 0 ) {

				Tree.EndObject();

				uObject--;
				}

			Tree.Close();

			return TRUE;
			}
		}  

	return FALSE; 
	}

// Helpers

void CCANOpenSlaveDriverOptions::GetObject(CTreeFile &Tree, UINT &uObject)
{
	Tree.GetObject();

	uObject++;
	}

void CCANOpenSlaveDriverOptions::GetCollect(CTreeFile &Tree, UINT &uCollect, UINT &uObject)
{
	Tree.GetCollect();
	
	Tree.GetElement();
	
	uObject++;
	
	uCollect++;
	}

void CCANOpenSlaveDriverOptions::GetNext(CTreeFile &Tree, UINT &uObject, UINT &uCollect)
{
	Tree.GetName();

	End(Tree, uObject, uCollect);
	}

void CCANOpenSlaveDriverOptions::GetNext(CTreeFile &Tree, UINT &uObject, UINT &uCollect, UINT uSize)
{
	Tree.GetCollect();

	uCollect++;
														
	while( uSize > 0 ) {

		Tree.GetName();

		Tree.GetObject();
							     
		while( !Tree.IsEndOfData() ) {

			Tree.GetName();
			} 

		Tree.EndObject();
								
		uSize--;
		}

	End(Tree, uObject, uCollect);

	if( !Tree.IsEndOfData() ) {

		Tree.GetElement();

		uObject++;
		}
	else {
		End(Tree, uObject, uCollect);

		Tree.GetName();
		}
	}

void CCANOpenSlaveDriverOptions::End(CTreeFile &Tree, UINT &uObject, UINT &uCollect)
{
	if( uObject > 0 ) {

		Tree.EndObject();

		uObject--;
		}

	if( uCollect > 0 ) {

		Tree.EndCollect();

		uCollect--;
		}
	}

CString CCANOpenSlaveDriverOptions::GetName(CString Name)
{
	CString Sub;

	Sub = Name.Mid(1, Name.Find('/') - 1);

	return Sub;
	}

CString CCANOpenSlaveDriverOptions::GetDesc(CString Name)
{
	CString Sub;

	Sub = Name.Left(Name.Find('/') + 1);

	Sub += "%u";

	return Sub;
	}

CString CCANOpenSlaveDriverOptions::GetObjectType(CString Name)
{
	CString Type = "0x7";

	return Type;
	}


CString CCANOpenSlaveDriverOptions::GetDataType(CString Name)
{
	CString Type = "";

	switch( Name.GetAt(0) ) {

		case 'B':
			Type = "0x0005";
			break;

		case 'W':
			Type = "0x0006";
			break;

		case 'L':
			Type = "0x0007";
			break;
		}
       
	return Type;
	}

CString CCANOpenSlaveDriverOptions::GetAccessType(CString Name)
{
	CString Type = "";

	UINT uFind = Name.GetLength() - 1;

	switch( Name.GetAt(uFind) ) {

		case '0' :
			Type = "ro";
			break;

		case '1' :
			Type = IsPDO() ? "wo" : "rw";
			break;	     
		}

	return Type;
      	}

UINT CCANOpenSlaveDriverOptions::GetSize(CString Name)
{
	UINT uFind = Name.Find('^');

	CString Size = Name.Mid(uFind + 1, Name.Find('^', uFind + 1) - uFind - 1);

	return tatoi(PCTXT(Size));
	}

BOOL CCANOpenSlaveDriverOptions::IsObjectValid(UINT uType, UINT uIndex)
{
	if( !IsObjectMandatory(uIndex, OBJ_ALL) ) {
	
		switch( uType ) {

			case OBJ_OPT:

				if( (uIndex >= 0x1000 && uIndex <= 0x1FFF) ||
				    (uIndex >= 0x6000 && uIndex <= 0xFFFF) ) {

					return TRUE;
					}
				break;
			
			case OBJ_MANU:

				if( (uIndex >= 0x2000 && uIndex <= 0x5FFF) ) {

					return TRUE;
					}
				break;
			}
		}

	return FALSE;
	}

BOOL CCANOpenSlaveDriverOptions::IsObjectMandatory(UINT uIndex, UINT uType) 
{
	if( uType == OBJ_OPT || uType == OBJ_ALL ) {

		switch( uIndex ) {     

			case 0x1008:
			case 0x1009:
			case 0x100A:
			
				return TRUE;
			}
		}

	if( uType == OBJ_MAND || uType == OBJ_ALL ) {

	
		switch( uIndex) {

			case 0x1000:
			case 0x1001:
			case 0x1018:

				return TRUE;
			}
		}

	return FALSE;
	}

CString CCANOpenSlaveDriverOptions::GetMandatoryObjects(UINT uType, UINT &uCount)
{
	CString Objects = "";

	CString Format = "%u=0x%4.4X\r\n";

	CString Temp = "";

	for( UINT u = 0; u < 0xFFFF; u++ ) {

		if( IsObjectMandatory(u, uType) ) {

			Temp.Printf(Format, ++uCount, u);

			Objects += Temp;
			}
		}
	
	return Objects;
	}

CString CCANOpenSlaveDriverOptions::GetRemainingObjects(UINT uType, UINT &uCount)
{
	UINT uStart = m_Objects.Find('?');

	UINT uEnd = 0;

	CString Temp = m_Objects.Left(uStart);

	CString Objects = "";

	CString Count = "";

	CString Index = "";

	UINT uIndex = 0;

	while ( uStart < m_Objects.GetLength() ) {

		uStart = uEnd + 1;

		UINT uSize = m_Increment == 0 ? GetSize(Temp) : 1;
		
		Index = Temp.Mid(1, Temp.Find('/') - 1);

		Index.ToUpper();

		uIndex = tstrtoul(Index, NULL, 16);
		
		for( UINT u = 0; u < uSize; u++ ) {
					
			if( IsObjectValid(uType, uIndex) ) { 

				uCount++;
		     
				Count.Printf("%u", uCount);

				Objects += Count;
		
				Objects += "=0x";

				Index.Printf("%4.4X", uIndex);
		
				Objects += Index;

				Objects += "\r\n";

				uIndex++;
				}
			}
	
		uEnd = m_Objects.Find('?', uStart);

		Temp = m_Objects.Mid(uStart, uEnd - uStart);
	 	}
	
	return Objects;
	}

BOOL CCANOpenSlaveDriverOptions::IsPDO(void)
{
	return FALSE;
	}

 //////////////////////////////////////////////////////////////////////////
//
// CAN Space Wrapper
//

// Constructor

CSpaceCAN::CSpaceCAN(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t)
{
	m_uTable	= uTable;
	
	m_Prefix	= p;

	m_Caption	= c;

	m_uRadix	= r;

	m_uMinimum	= n;
	
	m_uMaximum	= x;
	
	m_uType		= t;

	m_uSpan		= t;

	FindWidth();

	FindAlignment();
	}

// Matching

BOOL CSpaceCAN::MatchSpace(CAddress const &Addr)
{
	return m_uType == Addr.a.m_Type;
	}

//////////////////////////////////////////////////////////////////////////
//
// CANOpen Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CCANOpenDialog, CStdAddrDialog);
		
// Constructor

CCANOpenDialog::CCANOpenDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "CANOpenElementDlg";
	}

// Message Map

AfxMessageMap(CCANOpenDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	
	AfxMessageEnd(CCANOpenDialog)
	};

// Notification Handlers

void CCANOpenDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);
	
	UINT  uPos  = ListBox.GetCurSel();

	LoadList();

	ListBox.SetCurSel(uPos);

	CStdAddrDialog::OnSpaceChange(uID, Wnd);	
	}

// Implementation

void CCANOpenDialog::SetSpace(CSpace * pSpace)
{
	m_pSpace = pSpace;
	}

// Overridables

void CCANOpenDialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CCANOpenDialog::SetAddressText(CString Text)
{
	BOOL fEnable   = !Text.IsEmpty();

	UINT uRestrict = m_pConfig->GetDataAccess("Restrict")->ReadInteger(m_pConfig);

	if( m_pSpace  ) {

		if( fEnable ) {

			UINT uFind = Text.Find('/');

			if( uFind < NOTHING ) {

				GetDlgItem(2002).SetWindowText(Text.Left(uFind));

				GetDlgItem(2004).SetWindowText("/");

				GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
				}
			else {
				GetDlgItem(2002).SetWindowText(Text.Left(uFind));

				GetDlgItem(2004).SetWindowText("/");

				GetDlgItem(2005).SetWindowText("0");
				}
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fEnable);
		
		GetDlgItem(2005).EnableWindow(fEnable && !uRestrict);
		}
	else {
		if( fEnable ) {

			GetDlgItem(2002).SetWindowText(Text);

			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2005).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		}
	}

CString CCANOpenDialog::GetAddressText(void)
{
	if( m_pSpace ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += "/";
		
		Text += GetDlgItem(2005).GetWindowText();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}

// End of File
