
#include "intern.hpp"

#include "DebugTcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug TCP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CDebugTcpDriverOptions, CUIItem);

// Constructor

CDebugTcpDriverOptions::CDebugTcpDriverOptions(void)
{
	m_Port = 23;

	m_Mode = 1;
	}

// Download Support

BOOL CDebugTcpDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Port));

	Init.AddByte(BYTE(m_Mode));

	return TRUE;
	}

// Meta Data Creation

void CDebugTcpDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Port);
	Meta_AddInteger(Mode);
	}

//////////////////////////////////////////////////////////////////////////
//
// Debug TCP Driver
//

// Instantiator

ICommsDriver *	Create_DebugTcpDriver(void)
{
	return New CDebugTcpDriver;
	}

// Constructor

CDebugTcpDriver::CDebugTcpDriver(void)
{
	m_wID		= 0x40E4;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Debug Console";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Debug Console";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Binding Control

UINT CDebugTcpDriver::GetBinding(void)
{
	return bindEthernet;
	}

// Configuration

CLASS CDebugTcpDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDebugTcpDriverOptions);
	}

// End of File
