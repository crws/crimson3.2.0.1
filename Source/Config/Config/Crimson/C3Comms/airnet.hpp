
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_AIRNET_HPP
	
#define	INCLUDE_AIRNET_HPP 


/////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN Bus Driver Options
//

class CAirNetDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAirNetDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Source;
	
			
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};




/////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN Bus Device Options
//

class CAirNetDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAirNetDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_ID;
		UINT m_Def;
					
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN Bus Driver
//

class CAirNetDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CAirNetDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
		 

	protected:

		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
