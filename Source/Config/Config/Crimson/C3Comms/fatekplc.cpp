
#include "intern.hpp"

#include "fatekplc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();


//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CFatekPLCDeviceOptions, CUIItem);

// Constructor

CFatekPLCDeviceOptions::CFatekPLCDeviceOptions(void)
{
	m_Drop       = 1;
	}

// UI Managament

void CFatekPLCDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	pWnd->UpdateUI();
	}


// Download Support

BOOL CFatekPLCDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CFatekPLCDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC UDP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CFatekPLCUDPDeviceOptions, CUIItem);

// Constructor

CFatekPLCUDPDeviceOptions::CFatekPLCUDPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 500;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_Drop   = 1;
	}

// UI Management

void CFatekPLCUDPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CFatekPLCUDPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CFatekPLCUDPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Serial Driver
//

// Instantiator

ICommsDriver * Create_FatekPLCSerialDriver(void)
{
	return New CFatekPLCSerialDriver;
	}

// Constructor

CFatekPLCSerialDriver::CFatekPLCSerialDriver(void)
{
	m_wID		= 0x4071;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Fatek";
	
	m_DriverName	= "PLC";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Fatek PLC";

	AddSpaces();
	}

// Configuration

CLASS CFatekPLCSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CFatekPLCDeviceOptions);
	}

// Binding Control

UINT CFatekPLCSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CFatekPLCSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CFatekPLCSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CFatekPLCAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CFatekPLCSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		switch( Addr.a.m_Type ) {

			case addrBitAsWord:

				Addr.a.m_Type = addrWordAsWord;
				break;

			case addrBitAsLong:
			case addrWordAsLong:

				Addr.a.m_Type = addrLongAsLong;
				break;

			case addrWordAsReal:
			case addrLongAsReal:

				Addr.a.m_Type = addrRealAsReal;
				break;
			}

		if( pSpace->m_uType == addrBitAsBit ) {

			switch( Addr.a.m_Type ) {

				case addrWordAsWord:
				case addrLongAsLong:

					Addr.a.m_Offset &= 0xFFF8;
					break; // put on Word boundary
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CFatekPLCSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SP_H,	"H",	"H(R) Registers",			10, 0,	65535,	addrWordAsWord,	addrRealAsReal));
	AddSpace(New CSpace(SP_D,	"D",	"D Registers",				10, 0,	65535,	addrWordAsWord,	addrRealAsReal));
	AddSpace(New CSpace(SP_TR,	"TR",	"Timer Register",			10, 0,	 9999,	addrWordAsWord,	addrLongAsLong));
	AddSpace(New CSpace(SP_CR,	"CR",	"Counter Register",			10, 0,	 9999,	addrWordAsWord,	addrLongAsLong));
	AddSpace(New CSpace(SP_XS,	"XS",	"Input Discrete On/Off",		10, 0,	 9999,	addrBitAsBit,	addrLongAsLong));
	AddSpace(New CSpace(SP_YS,	"YS",	"Output Relay On/Off",			10, 0,	 9999,	addrBitAsBit,	addrLongAsLong));
	AddSpace(New CSpace(SP_MS,	"MS",	"Internal Relay On/Off",		10, 0,	 9999,	addrBitAsBit,	addrLongAsLong));
	AddSpace(New CSpace(SP_SS,	"SS",	"Step Relay On/Off",			10, 0,	 9999,	addrBitAsBit,	addrLongAsLong));
	AddSpace(New CSpace(SP_TS,	"TS",	"Timer Discrete On/Off",		10, 0,	 9999,	addrBitAsBit,	addrLongAsLong));
	AddSpace(New CSpace(SP_CS,	"CS",	"Counter Discrete On/Off",		10, 0,	 9999,	addrBitAsBit,	addrLongAsLong));
	AddSpace(New CSpace(SP_XE,	"XE",	"Input Discrete-Enable(0)/Disable(1)",	10, 0,	 9999,			addrBitAsBit  ));
	AddSpace(New CSpace(SP_YE,	"YE",	"Output Relay-Enable(0)/Disable(1)",	10, 0,	 9999,			addrBitAsBit  ));
	AddSpace(New CSpace(SP_ME,	"ME",	"Internal Relay-Enable(0)/Disable(1)",	10, 0,	 9999,			addrBitAsBit  ));
	AddSpace(New CSpace(SP_SE,	"SE",	"Step Relay-Enable(0)/Disable(1)",	10, 0,	 9999,			addrBitAsBit  ));
	AddSpace(New CSpace(SP_TE,	"TE",	"Timer Discrete-Enable(0)/Disable(1)",	10, 0,	 9999,			addrBitAsBit  ));
	AddSpace(New CSpace(SP_CE,	"CE",	"Counter Discrete Enable(0)/Disable(1)",10, 0,	 9999,			addrBitAsBit  ));
	AddSpace(New CSpace(SP_ST,	"ST",	"Read System Status",			10, 0,      0,			addrLongAsLong));
	AddSpace(New CSpace(SP_RS,	"RS",	"Run/Stop PLC",				10, 0,	    0,			addrLongAsLong));
	AddSpace(New CSpace(SP_ERR,	"ERR",	"Latest Comm Error",			10, 0,	    0,			addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC UDP Master Driver
//

// Instantiator

ICommsDriver * Create_FatekPLCUDPDriver(void)
{
	return New CFatekPLCUDPDriver;
	}

// Constructor

CFatekPLCUDPDriver::CFatekPLCUDPDriver(void)
{
	m_wID		= 0x3529;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Fatek";
	
	m_DriverName	= "PLC";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Fatek PLC";
	}

// Binding Control

UINT CFatekPLCUDPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CFatekPLCUDPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS	CFatekPLCUDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CFatekPLCUDPDeviceOptions);
	}

/////////////////////////////////////////////////////////////////////
//
// Fatek PLC Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CFatekPLCAddrDialog, CStdAddrDialog);
		
// Constructor

CFatekPLCAddrDialog::CFatekPLCAddrDialog(CFatekPLCSerialDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "StdElementDlg";
	}

// Overridables

BOOL CFatekPLCAddrDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrWordAsWord:
		case addrLongAsLong:
		case addrRealAsReal:	return TRUE;
		}

	return FALSE;
	}

// End of File
