
#include "intern.hpp"

#include "hc900.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Byte Order Conversion
//

static inline WORD MotorToHost(WORD Data)
{
	WORD wHi = WORD(Data >> 8);

	WORD wLo = WORD(Data << 8);

	return WORD(wHi | wLo);
	}

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Base Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CHoneywell900BaseDeviceOptions, CUIItem);

// Constructor

CHoneywell900BaseDeviceOptions::CHoneywell900BaseDeviceOptions(void)
{
	m_Alarms = 1;
	}

// UI Update

void CHoneywell900BaseDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	}

// Persistance

void CHoneywell900BaseDeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();

	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		m_Index.Insert(m_List[n], n);
		}
	}

// Download Support

BOOL CHoneywell900BaseDeviceOptions::MakeInitData(CInitData &Init)
{
	Init.AddLong(m_Alarms);

	if( TRUE ) {

		PCBYTE pData  = PCBYTE(m_List.GetPointer());

		UINT   uCount = m_List.GetCount();

		UINT   uSize  = sizeof(WORD) * uCount;

		Init.AddWord(WORD(uCount));

		Init.AddData(pData, uSize);
		}

	if( TRUE ) {

		PCBYTE pData  = PCBYTE(m_Blob.GetPointer());

		UINT   uCount = m_Blob.GetCount();

		UINT   uSize  = sizeof(BYTE) * uCount;

		Init.AddWord(WORD(uCount));

		Init.AddData(pData, uSize);
		}

	return TRUE;
	}

// Meta Data Creation

void CHoneywell900BaseDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddWide   (List);
	Meta_AddBlob   (Blob);
	Meta_AddInteger(Alarms);
	Meta_AddString (Path);
	Meta_AddString (File);
	Meta_AddString (Time);

	Meta_SetName((IDS_HC_DEVICE_OPTIONS));
	}

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Network Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CHoneywell900DeviceOptions, CHoneywell900BaseDeviceOptions);

// Constructor

CHoneywell900DeviceOptions::CHoneywell900DeviceOptions(void)
{
	m_Addr = DWORD(MAKELONG(MAKEWORD(10, 1), MAKEWORD(168, 192)));

	m_Fail = 0;

	m_Back = DWORD(MAKELONG(MAKEWORD(10, 2), MAKEWORD(168, 192)));;

	m_HMI  = 1;
	}

// UI Update

void CHoneywell900DeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == L"Fail" ) {

		pHost->EnableUI(L"Back", m_Fail);
		}

	if( Tag.IsEmpty() ) {

		if( m_Blob.GetCount() ) {

			PCWORD pSig = PCWORD(m_Blob.GetPointer());

			if( MotorToHost(pSig[0]) == 0x5678 ) {

				if( MotorToHost(pSig[2]) == 0x0002 ) {

					if( m_Alarms ) {

						m_Alarms = 0;

						pHost->UpdateUI(L"Alarms");
						}

					pHost->EnableUI(L"Alarms", FALSE);

					CHoneywell900BaseDeviceOptions::OnUIChange(pHost, pItem, Tag);

					return;
					}
				}
			}

		pHost->EnableUI(L"Alarms", TRUE);
		}

	CHoneywell900BaseDeviceOptions::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CHoneywell900DeviceOptions::MakeInitData(CInitData &Init)
{
	Init.AddLong(m_Addr);

	Init.AddLong(m_Fail ? m_Back : 0);

	Init.AddLong(m_HMI);

	return CHoneywell900BaseDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CHoneywell900DeviceOptions::AddMetaData(void)
{
	CHoneywell900BaseDeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Fail);
	Meta_AddInteger(Back);
	Meta_AddInteger(HMI);

	Meta_SetName((IDS_HC_NETWORK_DEVICE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CHoneywell900SerialDeviceOptions, CHoneywell900BaseDeviceOptions);

// Constructor

CHoneywell900SerialDeviceOptions::CHoneywell900SerialDeviceOptions(void)
{
	m_Drop = 1;
	}

// Download Support

BOOL CHoneywell900SerialDeviceOptions::MakeInitData(CInitData &Init)
{
	Init.AddLong(m_Drop);

	Init.AddLong(0);

	Init.AddLong(0);

	return CHoneywell900BaseDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CHoneywell900SerialDeviceOptions::AddMetaData(void)
{
	CHoneywell900BaseDeviceOptions::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_SetName((IDS_HC_SERIAL_DEVICE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Honeywell HC-900 Driver
//

// Instantiator

ICommsDriver *	Create_Honeywell900SerDriver(void)
{
	return New CHoneywell900Driver(FALSE);
	}

ICommsDriver *	Create_Honeywell900NetDriver(void)
{
	return New CHoneywell900Driver(TRUE);
	}

// Constructor

CHoneywell900Driver::CHoneywell900Driver(BOOL fNet)
{
	m_fNet          = fNet;

	m_wID		= WORD(m_fNet ? 0x9999 : 0x9998);

	m_uType		= driverHoneywell;
	
	m_DevRoot	= "HC";

	m_Manufacturer	= "Honeywell";
	
	m_DriverName	= CPrintf(L"HC-900 %s", m_fNet ? L"TCP/IP" : L"Serial");
	
	m_Version	= "1.00";
	
	m_ShortName	= "HC-900";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Driver Data

UINT CHoneywell900Driver::GetFlags(void)
{
	return CBasicCommsDriver::GetFlags() | dflagImport;
	}

// Binding Control

UINT CHoneywell900Driver::GetBinding(void)
{
	return m_fNet ? bindEthernet : bindStdSerial;
	}

// Configuration

CLASS CHoneywell900Driver::GetDeviceConfig(void)
{
	if( m_fNet ) {

		return AfxRuntimeClass(CHoneywell900DeviceOptions);
		}

	return AfxRuntimeClass(CHoneywell900SerialDeviceOptions);
	}

// Tag Import

BOOL CHoneywell900Driver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	CFilename Name    = afxModule->GetFilename();

	HMODULE   hModule = LoadLibrary(Name.WithName(L"readcde.dll"));

	if( hModule ) {

		FARPROC pfnProc = GetProcAddress(hModule, "Import");

		if( pfnProc ) {

			COpenFileDialog Dlg;

			Dlg.LoadLastPath(L"HC900");

			Dlg.SetCaption(CString(IDS_CREATE_TAGS_FROM_2));

			Dlg.SetFilter (CString(IDS_CDE_FILES_CDECDE));

			if( Dlg.Execute(*afxMainWnd) ) {

				CString   Root = L"CDE";

				CFilename File = Dlg.GetFilename();

				if( pTags->InitImport(Root, FALSE, File) ) {

					BOOL (*pfnImport)(CString, IMakeTags *);

					pfnImport = (BOOL (*)(CString, IMakeTags *)) pfnProc;

					pfnImport(File, pTags);

					////////

					CHoneywell900BaseDeviceOptions *pOpt = (CHoneywell900BaseDeviceOptions *) pConfig;

					pOpt->m_List.Empty();

					pOpt->m_Index.Empty();

					WIN32_FILE_ATTRIBUTE_DATA Data;

					SYSTEMTIME Write;

					WCHAR sTime[128];

					WCHAR sDate[128];

					GetFileAttributesEx(File, GetFileExInfoStandard, &Data);

					FileTimeToLocalFileTime(&Data.ftLastWriteTime, &Data.ftLastAccessTime);

					FileTimeToSystemTime(&Data.ftLastAccessTime, &Write);

					GetTimeFormat(LOCALE_SYSTEM_DEFAULT, 0, &Write, NULL, sTime, elements(sTime));

					GetDateFormat(LOCALE_SYSTEM_DEFAULT, 0, &Write, NULL, sDate, elements(sDate));

					pOpt->m_Path = File.GetDirectory();

					pOpt->m_File = File.GetBareName();

					pOpt->m_Time = CPrintf(L"%s %s", sDate, sTime);

					pTags->ImportDone(TRUE);
					}

				Dlg.SaveLastPath(L"HC900");

				FreeLibrary(hModule);

				return TRUE;
				}
			}

		FreeLibrary(hModule);
		}

	return FALSE;
	}

// Address Management

BOOL CHoneywell900Driver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CHoneywell900BaseDeviceOptions *pOpt = (CHoneywell900BaseDeviceOptions *) pConfig;

	////////

	CStringArray List;

	Text.Tokenize(List, L'-');

	BOOL fHit = FALSE;

	////////

	if( List[0] == L"XX" ) { Addr.a.m_Extra =  0; fHit = TRUE; }
	if( List[0] == L"GS" ) { Addr.a.m_Extra =  1; fHit = TRUE; }
	if( List[0] == L"GD" ) { Addr.a.m_Extra =  2; fHit = TRUE; }
	if( List[0] == L"DH" ) { Addr.a.m_Extra =  3; fHit = TRUE; }
	if( List[0] == L"DP" ) { Addr.a.m_Extra =  4; fHit = TRUE; }
	if( List[0] == L"DS" ) { Addr.a.m_Extra =  5; fHit = TRUE; }
	if( List[0] == L"SD" ) { Addr.a.m_Extra =  6; fHit = TRUE; }
	if( List[0] == L"SS" ) { Addr.a.m_Extra =  7; fHit = TRUE; }
	if( List[0] == L"PS" ) { Addr.a.m_Extra =  8; fHit = TRUE; }
	if( List[0] == L"AS" ) { Addr.a.m_Extra =  9; fHit = TRUE; }
	if( List[0] == L"NS" ) { Addr.a.m_Extra = 10; fHit = TRUE; }
	if( List[0] == L"ND" ) { Addr.a.m_Extra = 11; fHit = TRUE; }

	////////

	if( fHit ) {

		if( Addr.a.m_Extra ) {

			WORD  Block = WORD(watoi(List[1]));

			WORD  Prop  = WORD(watoi(List[2]));

			WORD  Slot  = 0;

			INDEX Index = pOpt->m_Index.FindName(Block);

			if( pOpt->m_Index.Failed(Index) ) {

				UINT i = pOpt->m_List.Append(Block);

				Slot   = WORD(i);

				pOpt->m_Index.Insert(Block, i);
				}
			else {
				UINT i = pOpt->m_Index.GetData(Index);

				Slot   = WORD(i);
				}

			Addr.a.m_Type   = addrLongAsLong;

			Addr.a.m_Table  = BYTE(Slot >> 4);

			Addr.a.m_Offset = (Prop | ((Slot & 15) << 12));

			return TRUE;
			}

		WORD  Block = WORD(watoi(List[1]));

		WORD  Prop  = WORD(watoi(List[2]));

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = Block;

		Addr.a.m_Offset = Prop;

		return TRUE;
		}

	Error.Set(L"You have entered an invalid address.");

	return FALSE;
	}

BOOL CHoneywell900Driver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CHoneywell900BaseDeviceOptions *pOpt = (CHoneywell900BaseDeviceOptions *) pConfig;

	////////

	PCTXT pTab = NULL;

	if( Addr.a.m_Extra ==  0 ) { pTab = L"XX"; }
	if( Addr.a.m_Extra ==  1 ) { pTab = L"GS"; }
	if( Addr.a.m_Extra ==  2 ) { pTab = L"GD"; }
	if( Addr.a.m_Extra ==  3 ) { pTab = L"DH"; }
	if( Addr.a.m_Extra ==  4 ) { pTab = L"DP"; }
	if( Addr.a.m_Extra ==  5 ) { pTab = L"DS"; }
	if( Addr.a.m_Extra ==  6 ) { pTab = L"SD"; }
	if( Addr.a.m_Extra ==  7 ) { pTab = L"SS"; }
	if( Addr.a.m_Extra ==  8 ) { pTab = L"PS"; }
	if( Addr.a.m_Extra ==  9 ) { pTab = L"AS"; }
	if( Addr.a.m_Extra == 10 ) { pTab = L"NS"; }
	if( Addr.a.m_Extra == 11 ) { pTab = L"ND"; }

	////////

	if( Addr.a.m_Extra ) {

		WORD Prop  = WORD(Addr.a.m_Offset & 4095);

		WORD Slot  = WORD(Addr.a.m_Table << 4 | Addr.a.m_Offset >> 12);

		WORD Block = pOpt->m_List[Slot];

		////////

		Text.Printf("%s-%u-%u", pTab, Block, Prop);

		return TRUE;
		}

	WORD Prop  = Addr.a.m_Offset;

	WORD Block = Addr.a.m_Table;

	Text.Printf("%s-%u-%u", pTab, Block, Prop);

	return TRUE;
	}

BOOL CHoneywell900Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return FALSE;
	}

BOOL CHoneywell900Driver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

BOOL CHoneywell900Driver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}

BOOL CHoneywell900Driver::IsAddrNamed(CItem *pConfig, CAddress const &Addr)
{
	switch( Addr.a.m_Extra ) {

		case 10:
		case 11:
			return TRUE;
		}

	return FALSE;
	}

// End of File
