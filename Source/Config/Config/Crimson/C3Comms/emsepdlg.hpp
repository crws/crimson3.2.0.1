
class CEmersonEPDriver;

//////////////////////////////////////////////////////////////////////////
//
// Emerson EPI Address Selection
//

class CEmersonEPAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CEmersonEPAddrDialog(CEmersonEPDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Destructor
		~CEmersonEPAddrDialog(void);
		                
	protected:
		// Data Members

		// Initialization members
		CEmersonEPDriver * m_pDriverData;

		BOOL	m_fIsTCP;

		CSpace *m_pSortSpace;
		UINT	m_Device;
		UINT	m_DispMode;
		BOOL	m_fFirst;

		// List Control members
		UINT	m_uTopLevel;
		UINT	m_uAllowList;
		UINT	m_uListSel;
		UINT	m_uMenuSel;
		UINT	m_uAlpha0;
		BOOL	m_fListTypeChg;

		// EPB/EPI specific selections
		UINT	m_uDIA[52];
		UINT	m_uHOM[11];
		UINT	m_uIOS[42];
		UINT	m_uINX[153];
		UINT	m_uJOG[4];
		UINT	m_uMOT[51];
		UINT	m_uPUL[5];
		UINT	m_uREG[24];
		UINT	m_uSTT[49];
		UINT	m_uSYS[56];

		// Keyword controls
		CString	m_KWText;
		UINT	m_KWPrev;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		BOOL	AllowType(UINT uType);

		// Select Space List
		void	SetTheDeviceList(void);

		// Load List Functions
		void	LoadList(void);
		INDEX	SetEntry(CListBox *pBox, CSpace *pSpace, INDEX n, BOOL *pfDef);
		void	LoadEntry(CListBox *pBox, CString Prefix, CString Caption, DWORD n);
		void	DoDesignatedHeader(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	DoModbus(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	DoKeyWord(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	DoMC(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	DoIBFnc(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	SortIBAlpha(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	SortIBNumer(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	DoSPFnc(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	SortSPAlpha(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	SortSPNumer(CListBox *pBox, CSpaceList &List, INDEX n);
		void	DoRemainingHeaders(CListBox *pBox, CSpaceList &List, INDEX n);
		INDEX	SetSPDefaultFind(CSpace *pSpace, INDEX n);
		INDEX	SetIBDefaultFind(CSpace *pSpace, INDEX n);
		BOOL	GetMenuNumber(CString Prefix, UINT uMin, UINT *pNum);
		void	MBToMenu(UINT uTable, UINT uOffset);
		BOOL	ValidPrefix(UINT uP0);
		UINT	ValidateDevSel(void);

		// Helpers
		void	CheckExisting(PDWORD pdRef);
		void	CheckSpace(PDWORD pdRef);
		void	HandleSpace(CSpace *pSpace);
		void	SetDefaults(void);
		UINT	GetDefaultTop(void);
		void	SetDefaultAllow(void);
		void	SetDefaultList(void);
		void	SetAllow(void);
		void	SetAllowFromRef(UINT uHead);
		void	SetAllowFromMB(UINT uOffset);
		void	SelectMenuNumber(UINT uOffset);
		BOOL	IsInvalidMenuNumber(UINT uMSel);
		BOOL	IsPreM(void);
		BOOL	IsIB(void);
		BOOL	IsI(void);
		BOOL	IsB(void);
		BOOL	IsSPP(void);
		BOOL	IsSKP(void);
		BOOL	IsGPP(void);
		BOOL	IsSTP(void);
		BOOL	IsMPP(void);
		BOOL	IsMC(void);
		BOOL	IsCommanderP(void);
		BOOL	IsHeaderSpace(UINT uHead);
		BOOL	IsTopLevel(UINT uHead);
		BOOL	IsSortHeader(UINT uHead);
		BOOL	IsModbusHeader(UINT uHead);
		BOOL	IsHeaderForIBDevice(UINT uHead);
		BOOL	IsEPBHeader(UINT uHead);
		BOOL	IsEPIHeader(UINT uHead);
		BOOL	IsComHeader(UINT uHead);
		BOOL	IsMCHeader(UINT uHead);
		BOOL	IsValidAlpha0(void);
		BOOL	ShowOK(UINT uTable);
		BOOL	IsStringItem(UINT uTable);
		UINT	GetListSelFromHeader(UINT uHeader);
		UINT	GetListControl(BOOL fPreM);
		void	SetOrderedList(void);
		BOOL	GetAddressFromItem(UINT uTable, UINT uOffset, UINT *pAddress);
		UINT	GetSpaceFromAddress(UINT *pOffset);
		BOOL	IsDO(UINT uOffset);
		BOOL	IsDI(UINT uOffset);
		BOOL	IsAI(UINT uOffset);
		BOOL	IsHR(UINT uOffset);
		BOOL	IsGenericMB(UINT uTable);
		BOOL	IsMCItem(UINT uTable);
		BOOL	IsSolutionModule(UINT uTable);
		UINT	GetMBType(UINT uTable, UINT uOffset);
		void	GetMBGeneric(UINT *pTable, UINT *pOffset, UINT uType);
		BOOL	IsDia(UINT uTable, UINT uOffset);
		BOOL	IsHom(UINT uTable, UINT uOffset);
		BOOL	IsInx(UINT uTable, UINT uOffset);
		BOOL	IsIos(UINT uTable, UINT uOffset);
		BOOL	IsJog(UINT uTable, UINT uOffset);
		BOOL	IsMot(UINT uTable, UINT uOffset);
		BOOL	IsPul(UINT uTable, UINT uOffset);
		BOOL	IsReg(UINT uTable, UINT uOffset);
		BOOL	IsStt(UINT uTable, UINT uOffset);
		BOOL	IsSys(UINT uTable, UINT uOffset);
		BOOL	IsItem(UINT *pTable, UINT uTable, UINT uOffset, UINT uCount);
		BOOL	MatchIBAllow(CSpace *pSpace);
		UINT	GetIBItemType(UINT uTable, UINT uOffset);
		UINT	IsIBorSPListItem(UINT uTable, UINT uOffset, UINT uP0);
		void	VerifySelects(void);
		UINT   *GetIBArray(UINT *pSize);
		UINT	GetID(UINT uType);
	}; 