
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODMON_HPP
	
#define	INCLUDE_MODMON_HPP

//////////////////////////////////////////////////////////////////////////
//
// Modbus Monitor Base Driver
//

class CModbusMonitorDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CModbusMonitorDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Monitor RTU Driver
//

class CModbusMonitorRTUDriver : public CModbusMonitorDriver
{
	public:
		// Constructor
		CModbusMonitorRTUDriver(void);

	protected:
		// Data

		// Implementation

	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Monitor ASCII Driver
//

class CModbusMonitorASCIIDriver : public CModbusMonitorDriver
{
	public:
		// Constructor
		CModbusMonitorASCIIDriver(void);

	protected:
		// Data

		// Implementation
	};

// End of File

#endif
