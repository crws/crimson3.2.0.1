
#include "intern.hpp"

#include "profibus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Profibus Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CProfibusDriverOptions, CUIItem);

// Constructor

CProfibusDriverOptions::CProfibusDriverOptions(void)
{
	m_Station = 125;
	}

// Download Support

BOOL CProfibusDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Station));

	return TRUE;
	}

// Meta Data Creation

void CProfibusDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Station);
	}

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Driver
//

// Constructor

CProfibusDPDriver::CProfibusDPDriver(void)
{
	}

// Binding Control

UINT CProfibusDPDriver::GetBinding(void)
{
	return bindProfibus;
	}

void CProfibusDPDriver::GetBindInfo(CBindInfo &Info)
{
//	CBindProfibus &Profibus = (CBindProfibus &) Info;

//	Profibus.m_Station = 127;
	}

// Configuration

CLASS CProfibusDPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CProfibusDriverOptions);
	}

CLASS CProfibusDPDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Address Management

BOOL CProfibusDPDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	Text.MakeUpper();

	UINT uBlock  = 0;

	if( Text[0] != 'R' && Text[0] != 'W' ) {

		return FALSE;
		}

	if( Text[0] == 'R' ) {
		
		uBlock = 2;
		}

	if( Text[0] == 'W' ) {
		
		uBlock = 1;
		}

	UINT uType = addrByteAsByte;

	UINT uPos = Text.Find('.');

	if( uPos < NOTHING ) {

		CString Type = Text.Mid(uPos + 1);
		
		uType = TypeFromModifier(Type);
		}

	Addr.a.m_Offset	= tatoi(Text.Mid(1, uPos));
	
	Addr.a.m_Table  = uBlock;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Type   = uType;
	
	return TRUE;
	}

BOOL CProfibusDPDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.a.m_Table == 2 ) {

		CString Type = GetTypeModifier(Addr.a.m_Type);

		Text.Printf("R%4.4d.%s", Addr.a.m_Offset, Type);

		return TRUE;
		}

	if( Addr.a.m_Table == 1 ) {

		CString Type = GetTypeModifier(Addr.a.m_Type);

		Text.Printf("W%4.4d.%s", Addr.a.m_Offset, Type);

		return TRUE;
		}

	return FALSE;
	}

BOOL CProfibusDPDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CProfibusDPDialog Dlg(*this, Addr);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CProfibusDPDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			Data.m_Name            = "Input Block";
			Data.m_Addr.a.m_Table  = 1;
			Data.m_Addr.a.m_Type   = addrWordAsWord;
			Data.m_Addr.a.m_Extra  = 0;
			Data.m_fPart	       = TRUE;
					
			return TRUE;
			}

		if( uItem == 1 ) {

			Data.m_Name            = "Output Block";
			Data.m_Addr.a.m_Table  = 2;
			Data.m_Addr.a.m_Type   = addrWordAsWord;
			Data.m_Addr.a.m_Extra  = 0;
			Data.m_fPart	       = TRUE;
				
			return TRUE;
			}
		}

	return FALSE;
	}


// Implementation

CString	CProfibusDPDriver::GetTypeModifier(UINT uType)
{
	switch( uType ) {
		
		case addrBitAsByte:
		case addrByteAsByte:
			return "BYTE";

		case addrBitAsWord:
		case addrByteAsWord:
		case addrWordAsWord:
			return "WORD";

		case addrBitAsLong:
		case addrByteAsLong:
		case addrWordAsLong:
		case addrLongAsLong:
			return "LONG";

		case addrBitAsReal:
		case addrByteAsReal:
		case addrWordAsReal:
		case addrLongAsReal:
		case addrRealAsReal:
			return "REAL";

		default:
			return "";
		}
	}

UINT CProfibusDPDriver::TypeFromModifier(CString Text)
{
	if( Text == "BYTE" ) {
		
		return addrByteAsByte;
		}

	if( Text == "WORD" ) {

		return addrWordAsWord;
		}

	if( Text == "LONG" ) {

		return addrLongAsLong;
		}

	if( Text == "REAL" ) {

		return addrRealAsReal;
		}

	return addrByteAsByte;
	}

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Master Driver
//

// Instantiator

ICommsDriver * Create_ProfibusDPMasterDriver(void)
{
	return New CProfibusDPMasterDriver;
	}

// Constructor

CProfibusDPMasterDriver::CProfibusDPMasterDriver(void)
{
	m_wID		= 0x401D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Profibus";
	
	m_DriverName	= "Profibus-DP Slave";
	
	m_Version	= "1.0";
	
	m_ShortName	= "Profibus-DP";
	}

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP via MPI Option Card Driver
//

// Instantiator

ICommsDriver * Create_DPviaMPIDriver(void)
{
	return New CDPviaMPIDriver;
	}

// Constructor

CDPviaMPIDriver::CDPviaMPIDriver(void)
{
	m_wID		= 0x404E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Profibus";
	
	m_DriverName	= "Profibus-DP";
	
	m_Version	= "1.0";
	
	m_ShortName	= "Profibus-DP";
	}

// Binding Control

UINT CDPviaMPIDriver::GetBinding(void)
{
	return bindMPI;
	}

void CDPviaMPIDriver::GetBindInfo(CBindInfo &Info)
{
	CBindMPI &MPI = (CBindMPI &) Info;

	MPI.m_StationAddress = 127;
	}

// Configuration

CLASS CDPviaMPIDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CDPviaMPIDriver::GetDeviceConfig(void)
{
	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Slave Driver
//

// Instantiator

ICommsDriver * Create_ProfibusDPSlaveDriver(void)
{
	return New CProfibusDPSlaveDriver;
	}

// Constructor

CProfibusDPSlaveDriver::CProfibusDPSlaveDriver(void)
{
	m_wID		= 0x4016;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Profibus";
	
	m_DriverName	= "Profibus-DP (Type S)";
	
	m_Version	= "1.0";
	
	m_ShortName	= "Profibus-DP";
	}

// Address Management

BOOL CProfibusDPSlaveDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	Text.MakeUpper();

	if( Text[0] == 'R' ) {

		Addr.a.m_Offset	= 0;
		
		Addr.a.m_Table  = 1;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Type   = addrWordAsWord;
		
		return TRUE;
		}

	if( Text[0] == 'W' ) {

		Addr.a.m_Offset	= 0;
		
		Addr.a.m_Table  = 2;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Type   = addrWordAsWord;
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CProfibusDPSlaveDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Profibus DP Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CProfibusDPDialog, CStdDialog);
		
// Constructor

CProfibusDPDialog::CProfibusDPDialog(CProfibusDPDriver &Driver, CAddress &Addr)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	SetName(TEXT("ProfibusDPDlg"));
	}

// Message Map

AfxMessageMap(CProfibusDPDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CProfibusDPDialog)
	};

// Message Handlers

BOOL CProfibusDPDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetCaption();

	LoadBlocks();

	LoadTypes();

	LoadAddress();

	return FALSE;
	}

// Command Handlers

BOOL CProfibusDPDialog::OnOkay(UINT uID)
{
	GetAddress();
	
	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CProfibusDPDialog::SetCaption(void)
{
	CString Text;

	Text.Printf( CString(IDS_DRIVER_CAPTION1), 
		     GetWindowText(),
		     m_pDriver->GetString(stringShortName),
		     ""
		     );

	SetWindowText(Text);
	}

void CProfibusDPDialog::LoadTypes(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(1006);

	Combo.AddString("Byte", addrByteAsByte);
	Combo.AddString("Word", addrWordAsWord);
	Combo.AddString("Long", addrLongAsLong);
	Combo.AddString("Real", addrRealAsReal);

	Combo.SelectData(addrWordAsWord);
	}

void CProfibusDPDialog::LoadBlocks(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(1001);

	Combo.AddString("Input Block",  1);
	Combo.AddString("Output Block", 2);

	Combo.SelectData(1);
	}

void CProfibusDPDialog::LoadAddress(void)
{
	UINT  uBlock = 1;
	
	UINT uOffset = 0;

	UINT   uType = addrByteAsByte;
	
	if( m_pAddr->m_Ref ) {

		uBlock  = m_pAddr->a.m_Table;

		uOffset = m_pAddr->a.m_Offset;

		uType   = m_pAddr->a.m_Type;
		}

	((CComboBox &) GetDlgItem(1001)).SelectData(uBlock);

	GetDlgItem(1005).SetWindowText(CPrintf("%4.4d", uOffset));

	((CComboBox &) GetDlgItem(1006)).SelectData(uType);
	}

void CProfibusDPDialog::GetAddress(void)
{
	UINT  uBlock = ((CComboBox &) GetDlgItem(1001)).GetCurSelData();
	
	UINT uOffset = tatoi(GetDlgItem(1005).GetWindowText());

	UINT   uType = ((CComboBox &) GetDlgItem(1006)).GetCurSelData();

	if( uBlock || uOffset ) {

		m_pAddr->a.m_Table  = uBlock;

		m_pAddr->a.m_Offset = uOffset;
		
		m_pAddr->a.m_Type   = uType;
		}
	else {
		m_pAddr->m_Ref = 0;
		}
	}

// End of File
