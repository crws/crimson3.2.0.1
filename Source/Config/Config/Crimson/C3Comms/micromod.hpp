
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MICROMOD_HPP
	
#define	INCLUDE_MICROMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Tag Data
//

struct CTagData {
	
	CString	m_Name;
	DWORD	m_Addr;
	};

typedef CArray <CTagData> CTagDataArray;

//////////////////////////////////////////////////////////////////////////
//
// Micromod Device Options
//

class CMicromodDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMicromodDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		CString	GetDeviceName(void);

		// Persistance
		void	Init(void);
		void	Load(CTreeFile &Tree);
		void	Save(CTreeFile &Tree);

		BOOL	CreateMMTag(CError &Error, CStringArray List, UINT uArrayPos, IMakeTags *pTags = NULL);
		BOOL	CreateStdTag(CError &Error, CString Name, CAddress dAddress, IMakeTags *pTags);
		BOOL	CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fMake, BOOL fWantExist);
		void	CreateTagsFromIMP(void);
		void    DeleteParam(CAddress const &Addr, CString const &Name);
		BOOL    RenameParam(CString const &Name, CAddress const &Addr);
		BOOL	FinishMakeTags(BOOL fMake, BOOL fWanttExist);
		BOOL	MakeNewTag(UINT uItem, BOOL fMake, BOOL fWantExist);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
		void	ListTags(CTagDataArray &List);

		virtual	BOOL SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);

		void	IMPListAppend(CString sIMPLine);
		void	IMPListInsert(CString sIMPLine, UINT uPos);
		BOOL	Import(FILE *pFile, CString sType, IMakeTags *pTags = NULL);

		CString	UpdateIMPString(CStringArray List);
		void	SortIMPListByFG(BOOL fAssign);
		void	SortIMPListByOffset(BOOL fReassign);

		BOOL	NameExists(CString sName, PDWORD pAddress);
		void	RebuildMaps(void);

		CViewWnd *GetView(void);
		IMakeTags *CheckMakeTags(void);
		void	SetTagPointer(IMakeTags *pTags);
		BOOL	ShowOldDBWarning(CWnd *pWnd, BOOL fQuitAnyway);
		BYTE	CheckOldDB(void);

		// Download And Array Support
		BOOL	MakeInitData(CInitData &Init);
		CString	DoTCPAccess(UINT uSelect, PDWORD pValue, CString sString);

	public:
		// Public Data
		UINT	m_Drop;
		UINT	m_Database;
		UINT	m_UnlockGNR;
		UINT	m_ArraySelect;
		DWORD	m_ArrayValue;
		CString	m_ArrayString;
		CString	m_TagPrefix;
		UINT	m_Push;
		IMakeTags *m_pTags;
		CString	m_DevName;
		BOOL	m_fImportOverwrite;
		BOOL	m_fMakeTags;

		typedef	CStringArray		CImportArray;
		typedef	CMap <CString, DWORD>	CTagsFwdMap;
		typedef	CMap <DWORD, CString>	CTagsRevMap;

		CImportArray	m_IMPList;
		CImportArray	m_OldDBLoadList;
		CStringArray	m_OldNameList;
		CStringArray	m_OldAddrList;
		CStringArray	m_NewIMPList;
		CTagsFwdMap	m_TagsFwdMap;
		CTagsRevMap	m_TagsRevMap;

		CStringArray	m_LSPList;
		CStringArray	m_OFFList;
		CStringArray	m_SZEList;
		CStringArray	m_BITList;
		CStringArray	m_ALSList;
		CArray <WORD>	m_OldIDList;

	protected:
		typedef	CArray <DWORD>		CAddrArray;

		CArray	<UINT>	m_Errors;
		CTagDataArray	m_List;

		CViewWnd *m_pViewWnd;

		// Import Helpers
		UINT	m_uTotalCount;
		UINT	m_uOverwrite;

		UINT	m_fIsOldDB;
		BOOL	m_fWasOldDB;

		BOOL	AccessArrayValue(void);
		UINT	GetItemFromIMPByPosition(UINT uArrayPos, UINT uItem);
		UINT	MakeNewOffset(void);
		void	SaveOldIMPNames(void);

		// Persistance
		void	LoadArrays(CTreeFile &Tree);
		void	SaveArrays(CTreeFile &Tree);

		// Device Options Tag Management
		void	OnManage(CWnd *pWnd);

		// Export/Import support
		void	OnImport(CWnd *pWnd);
		void	OnExport(CWnd *pWnd);

		void	Export(FILE *pFile);

		void	Import(CWnd *pWnd);

		// Import Old Database help
		void MakeIMPStringFromOldDB(void);
		CString MakeOldDBDataType(UINT uPosition, UINT * pTypeNum);
		void CompleteOldDBFields(void);
		UINT FindNewIDFromOld(UINT uOldID, UINT uMax);
		void AddToMaps(void);
		void EmptyOldLists(void);

		// other import help
		BOOL ConfirmImport(CWnd *pWnd);
		BOOL ImportAliasFile(FILE *pFile, CString Type);
		UINT ImportMIF(FILE *pFile);
		CString ClearUnwantedSpaces(CString sIn);
		void AddOldIMPLine(CString sLine, UINT uLine);
		void AddIMPLineOverwrite(CString sLine, UINT uLine);
		void AddIMPLineNoOverwrite(CString sLine, UINT uLine);
		void AddIMPLineAppend(CString sLine, UINT uLine);
		CString AddTagPrefix(CString sLine);
		BOOL HandleOldImport(CWnd *pWnd);

		// Import Functions
		BOOL DoneHeader(char *sIn, UINT *pLine);
		void HandleOverwrite(CStringArray List);

		// Helpers
		BOOL IsControlLine(CString sLine, CString sMatch);
		UINT GetDataType(CString sType);
		CString MakeNameString(CString sName);
		BOOL IsNewAlias(UINT uSel);
		CString GetIMPStringItem(UINT uSel, UINT uItem);
		UINT GetIMPIntItem(UINT uSel, UINT uItem);

		BOOL OkToReplaceQuery(CWnd *pWnd);
		BOOL OkToKeepQuery(CWnd *pWnd);
		BOOL OkToRemoveQuery(CWnd *pWnd);
		BOOL OkToOverwriteQuery(CWnd *pWnd);
		BOOL OkFinalQuery(CWnd *pWnd);

		// Meta Data Creation
		void	AddMetaData(void);

		friend class CMicromodAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Micromod TCP/IP Device Options
//

class CMicromodTCPDeviceOptions : public CMicromodDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMicromodTCPDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Addr;
		UINT	m_Socket;
		UINT	m_Keep;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;
		UINT	m_Drop;
		UINT	m_Database;
		UINT	m_UnlockGNR;
		UINT	m_ArraySelect;
		DWORD	m_ArrayValue;
		CString	m_ArrayString;
		CString	m_TagPrefix;
		UINT	m_Push;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

enum {
	WARRITM	= 1,	// set item select
	WARRLSP	= 2,	// save LSP
	WARRFG	= 3,	// save FG
	WARRSZE	= 4,	// save SZE
	WARRBIT	= 5,	// save BIT
	WARRALS	= 6,	// save Alpha LSP
	WARROFF	= 7,	// save Offset
	WARRTYP	= 8,	// save Data Type
	RARRLSP	= 12,	// read LSP @ m_uItem
	RARRFG	= 13,	// read FG @ m_uItem
	RARRSZE	= 14,	// read SZE @ m_uItem
	RARRBIT	= 15,	// read BIT @ m_uItem
	RARRALS	= 16,	// read Alpha LSP @ m_uItem
	RARROFF	= 17,	// read Offset
	RARRTYP	= 18,	// read data type
	RARRNAM	= 19,	// read item alias
	EARRFNC	= 20,	// save LSP, FG, SZE, BIT to arrays
	RARRCNT	= 21,	// read Array Size
	RITMOFF	= 22,	// Find Item From Addr.a.m_Offset
	ROFFITM	= 23,	// Find Offset From Item
	GFWDMAP	= 24,	// Get Addr.m_Ref using IMPList position
	GIMPSTR	= 25,	// Get full IMP List String
	MATCHL	= 32,	// search for LSP match
	MATCHO	= 33,	// search for FG match
	MATCHN	= 34,	// Find matching name
	FAPPEND	= 40,	// append values
	FINSERT	= 41,	// insert @ m_uItem
	FDELETE	= 42,	// delete & compact @ m_uItem
	FREPLCE	= 43,	// replace selection
	};

#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Function code 65 Functions
enum {
	SP_65		=  9,
	SP_RDB		= 66,
	SP_RDBO		= 67,
	SP_ERR		= 238,
	SP_NAK		= 239
	};

enum {	// IMPList Item Positions
	NUMPOS		= 0,
	NAMEPOS		= 1,
	ALIASPOS	= 2,
	ALSPPOS		= 3,
	NLSPPOS		= 4,
	LRNGPOS		= 5,
	HRNGPOS		= 6,
	LLIMPOS		= 7,
	HLIMPOS		= 8,
	EUDSPOS		= 9,
	ATYPPOS		= 10,
	NTYPPOS		= 11,
	XTYPPOS		= 12,
	DLENPOS		= 13,
	FGFLGPOS	= 14,
	FGOFFPOS	= 15,
	FGBITPOS	= 16,
	QFLGPOS		= 17,
	QOFFPOS		= 18,
	QBITPOS		= 19,
	ENABPOS		= 20,
	RWPOS		= 21,
	LNUMPOS		= 22,
	ENTRYPOS	= 23,
	};

// String Defs
#define	S_IF	"IF"
#define	S_SE	"SE"
#define	S_ICN	"ICN"
#define	S_OMC	"OMC"
#define	S_ES	"ES"
#define	S_LP	"LP"
#define	S_CL	"CL"
#define	S_08	"08"
#define	S_NM	"NM"
#define	S_TM	"TM"
#define	S_TOT	"TOT"
#define	S_LN	"LN"
#define	S_PW	"PW"
#define	S_PA	"PA"
#define	S_EX	"EX"
#define	S_IC	"IC"
#define	S_OC	"OC"
#define	S_DI	"DI"
#define	S_VCI	"VCI"
#define	S_CJI	"CJI"
#define	S_TI	"TI"
#define	S_TTI	"TTI"
#define	S_DIM	"DIM"
#define	S_DOM	"DOM"
#define	S_DDOM	"DDOM"
#define	S_WDOM	"WDOM"
#define	S_VCIM	"VCIM"
#define	S_CJIM	"CJIM"
#define	S_TIM	"TIM"
#define	S_AOM	"AOM"
#define	S_RTI	"RTI"
#define	S_RIM	"RIM"
#define	S_RTTI	"RTTI"
#define	S_WRIM	"WRIM"
#define	S_PID	"PID"
#define	S_RI	"RI"
#define	S_MSC	"MSC"
#define	S_ML	"ML"
#define	S_SM	"SM"
#define	S_SEQ	"SEQ"
#define	S_RIO	"RIO"
#define	S_RDIM	"RDIM"
#define	S_RDOM	"RDOM"
#define	S_44	"44"
#define	S_45	"45"
#define	S_46	"46"
#define	S_47	"47"
#define	S_AIN	"AIN"
#define	S_AOUT	"AOUT"
#define	S_DISP	"DISP"
#define	S_DIF	"DIF"
#define	S_PAD	"PAD"
#define	S_TL	"TL"
#define	S_ST	"ST"
#define	S_TMPL	"TMPL"
#define	S_RSK	"RSK"

#define	S_MMWARN1	(L"A database made with a previous version of the software has been loaded.\n")
#define	S_MMWARN2	(L"The corresponding MIF file has not yet been imported via the Import button in Devices.\n")
#define	S_MMWARN3	(L"Incomplete or inaccurate information will be present.\n\n")
#define	S_MMQUERY	(L"Do you wish to continue with this operation?")

//*************************************
//
// LSP Format:
// Bits 31-30 Reserved
// Bits 29-22 Block Number (Block Number + 10 = above table number)
// Bits 21-10 Occurrence Number
// Bits 09-00 Parameter Number
//*************************************

#define	LSP_BLK_POS	22
#define	LSP_OCC_POS	10

#define	LSP_BLK_MASK	(0x3FC00000)
#define	LSP_OCC_MASK	(0x003FFC00)
#define	LSP_PAR_MASK	(0x000003FF)

// Resource Dialog Entry Numbers
enum {
	CURRSPC	= 2001,	// 0x7D1	// Prefix
	CURRID	= 2002,	// 0x7D2	// Attribute number selection
	ATTYPE	= 2003,	// 0x7D3	// Type text
	VLOADID	= 2008,	// 0x7D8	// "Load ID"
	ALPHLSP	= 2009,	// 0x7D9	// LSP Alpha description

	ALSTXT	= 2040,			// "Alias:"
	ALSVAL	= 2041,			// Current Alias Selection

	LBTAGG	= 1050,			// Tag Name Group Box
	LBTAGS	= 1051,			// Tag Name List Box

	LSPTXT	= 2011,			// "LSP:"
	CURRLSP	= 2012,	// 0x7DC	// Current LSP
	LHEXSEL	= 2013,	// 0x7DD	// Display LSP Selection Hex/Dec

	FGTXT	= 2015,	// 0x7DF	// "FG Offset:"
	CURRFG	= 2016,	// 0x7E0	// Foreground offset selection

	PARTXT	= 2019,	// 0x7E3	// "Parameter:"
	CURRPAR	= 2020,	// 0x7E4	// current parameter selection

	SZETXT	= 2023,	// 0x7E7	// "Bytes:"
	CURRSZE	= 2024,	// 0x7E8	// Number of runtime bytes

	OCCTXT	= 2027,	// 0x7EB	// "Occurrence"
	CURROCC	= 2028,	// 0x7EC	// current occurrence selection

	ELSPTXT	= 2111,			// "LSP:"
	EDLSP	= 2112,	// 0x840	// Edit LSP
	EDBYLSP	= 2150,			// Edit via LSP

	EFGTXT	= 2115,	// 0x843	// "FG Offset:"
	EDFG	= 2116,	// 0x844	// Edit Foreground offset selection

	EPARTXT	= 2119,	// 0x847	// "Parameter:"
	EDPAR	= 2120,	// 0x848	// Edit parameter selection

	ESZETXT	= 2123,	// 0x84B	// "Bytes:"
	EDSZE	= 2124,	// 0x84C	// Edit runtime bytes

	EOCCTXT	= 2127,	// 0x84F	// "Occurrence"
	EDOCC	= 2128,	// 0x850	// edit occurrence selection

	EDCBTXT	= 2137, // 0x859	// "Block:"
	EDCBVAL	= 2138,	// 0x85A	// Block Selected
	EDALSP	= 2139,	// 0x85B	// Edit LSP text

	VREPLCE	= 2141,	// 0x85D	// Replace Entry
	VINSERT	= 2143,	// 0x85F	// Insert pushbutton
	VDELETE	= 2144,			// Delete pushbutton
	};

#define	INSERTAPPD	0x1000
#define	INSERTSAME	0x2000
#define	INSERTCANT	0x3000
#define	INSERTNDEF	0x4000

//////////////////////////////////////////////////////////////////////////
//
// Micromod Serial Driver
//

class CMicromodDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMicromodDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Driver Data
		UINT GetFlags(void);

		// Tag Import
		BOOL	MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

		// Data

		// Address Helpers
		BOOL	CheckAlignment(CSpace *pSpace);

		BOOL	IsFunc65(UINT uTable);
		BOOL	Is65Command(UINT uTable);

		// ArrayAccess		
		DWORD	GetLSPFromList(UINT uItem);
		WORD	GetFGFromList(UINT uItem);
		WORD	GetSZEFromList(UINT uItem);
		WORD	GetBITFromList(UINT uItem);
		WORD	GetTYPFromList(UINT uItem);
		CString	GetNAMFromList(UINT uItem);
		CString	GetIMPLine(UINT uPosition);

		UINT	GetBlkFromLSP(DWORD dLSP);
		UINT	GetOccFromLSP(DWORD dLSP);

		DWORD	GetArrayLSP(UINT uItem);
		WORD	GetArrayFG(UINT uItem);
		WORD	GetArraySZE(UINT uItem);
		WORD	GetArrayBIT(UINT uItem);
		WORD	GetArrayTYP(UINT uItem);
		WORD	GetArrayOFF(UINT uItem);
		CString	GetArrayALSP(UINT uItem);
		WORD	GetArrayCNT(void);
		void	DoArraySelect(UINT uFunc, UINT uValue);
		void	SetArrayFunc(UINT uFunc);
		void	SetArrayValue(DWORD dValue);
		void	SetArrayString(CString sStr);
		DWORD	GetArrayValue(void);
		CString	GetArrayString(void);
		void	SetMMConfigPtr(CItem *pMMConfig);

	protected:

		// Data
		CMicromodDeviceOptions	*m_pMMConfig;

		// Implementation
		void	AddSpaces(void);

		// Helpers
		UINT	GetTypeFromModifier(CString Type);
		UINT	CountNewAliases(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Micromod TCP/IP Master Driver
//

class CMicromodTCPDriver : public CMicromodDriver
{
	public:
		// Constructor
		CMicromodTCPDriver(void);

		//Destructor
		~CMicromodTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
						
	protected:
		// Implementation
	};

//////////////////////////////////////////////////////////////////////////
//
// Micromod Address Selection
//

	struct MMBLOCKNAMES {

		UINT Item;
		char Name[5];
		};

class CMicromodAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CMicromodAddrDialog(CMicromodDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		void SetCaption(CString const &Text);

		void SetSelect(BOOL fSelect);
		                
	protected:

	// Data
		CMicromodDriver *m_pMDriver;
		CMicromodDeviceOptions *m_pMMConfig;

		BOOL	m_fCheck;
		BOOL	m_fEHexSel;
		BOOL	m_fELSPSel;
		BOOL	m_fSelect;
		BOOL	m_fDoAll;
		BOOL	m_fDoNotEnd;
		UINT	m_uCurrSel;
		
		static MMBLOCKNAMES BlockNameList[57];
		MMBLOCKNAMES * m_pBList;

		UINT	m_uHasLSP;

		// ListBox handling
		HTREEITEM       m_hRoot;
		HTREEITEM       m_hSelect;
		DWORD		m_dwSelect;
		CImageList      m_Images;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnSelectionChange(UINT uID, CWnd &Wnd);

		// Command Handler
		BOOL	OnOkay(UINT uID);
		void	OnTypeChange(UINT uID, CWnd &Wnd);
		BOOL	OnButtonClicked(UINT uID);

		// Modify Tag Name List
		void	SetIMP(CString sFG, CStringArray *pList);
		void	AddIMP(CString sFG, CStringArray *pList, UINT uFunc);

		// Tag Name List
		void	LoadTagNameList(void);
		void	EnableTagDisplay(BOOL fYes);
		void	DoTagDEnable(UINT uIDS, UINT uIDE, BOOL fYes);
		void	ClearTagDItems(void);
		void	ClearItem(UINT uID);

		// Overridables
		BOOL	AllowType(UINT uType);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowDetails(void);

		// Block Combobox
		void	LoadNames(void);
		void	SetCBoxPosition(UINT uID, UINT uPos);
		UINT	GetCBoxPosition(UINT uID);
		void	ClearCBox(UINT uID);
		void	SetLBoxPosition(UINT uID, UINT uPos);
		UINT	GetLBoxPosition(UINT uID);

		// Editing
		void	InitEdit(CAddress Addr);
		void	EnableEdits(BOOL fYes);

		void	SetEditLSP(DWORD dLSP);
		void	SetEditPar(DWORD dPar);
		void	SetEditBytes(DWORD dBytes);
		void	SetEditOcc(DWORD dOcc);

		DWORD	GetEditLSP(void);

		// ArrayAccess		
		DWORD	GetLSPFromList(UINT uItem);
		WORD	GetFGFromList(UINT uItem);
		WORD	GetSZEFromList(UINT uItem);
		WORD	GetBITFromList(UINT uItem);
		UINT	GetBlkFromLSP(DWORD dLSP);
		UINT	GetParFromLSP(DWORD dLSP);
		UINT	GetOccFromLSP(DWORD dLSP);
		DWORD	MakeBaseLSP(UINT uTable);
		DWORD	MakeParFromDialog(void);
		DWORD	MakeOccFromDialog(void);
		UINT	GetItemFromOffset(WORD wOffset);
		UINT	GetOffsetFromItem(UINT uItem);
		DWORD	GetArrayLSP(void);
		DWORD	GetArrayLSP(UINT uItem);
		WORD	GetArrayFG(void);
		WORD	GetArrayFG(UINT uItem);
		WORD	GetArraySZE(void);
		WORD	GetArraySZE(UINT uItem);
		WORD	GetArrayBIT(void);
		WORD	GetArrayBIT(UINT uItem);
		WORD	GetArrayTYP(UINT uItem);
		WORD	GetArrayOFF(UINT uItem);
		CString	GetArrayNAM(UINT uItem);
		CString	GetArrayALSP(UINT uItem);
		WORD	GetArrayCNT(void);
		void	DoArraySelect(UINT uFunc, UINT uValue);
		void	SetArrayFunc(UINT uFunc);
		void	SetArrayValue(DWORD dValue);
		DWORD	GetArrayValue(void);

		// Helpers
		DWORD	GetDlgEntry(UINT uID);
		void	SetDlgEntry(UINT uID, DWORD dData);
		WORD	GetCURRIDValue(void);
		void	SetLSPEnables(BOOL fSet);
		void	DoShow(UINT uTable, CAddress Addr);
		void	ShowArrayInfo(CAddress Addr);
		void	SetTypeFromArrayValue(UINT uType);
		BOOL	IsFunc65(UINT uTable);
		BOOL	IsAttribute(UINT uTable);
		BOOL	Is65Command(UINT uTable);
		UINT	HasLSP(UINT uTable);

		// Check boxes
		void	SetEHexSel(void);
		BOOL	IsLSPSel(UINT uID);
		DWORD	MakeLSPValueFromText(CString sLSP);
		CString	MakeLSPTextFromValue(DWORD dLSP);
		CString	MakeALSPTextFromEdit();
		CString	MakeALSPTextFromParams(UINT uBlk, UINT uOcc, UINT uPar);
		void	ShowALSPEditText(void);
		void	SetELSPSel(void);

		// Insert and Append Help
		UINT	GetFGPosition(CString sFGSel);
		void	ExpandAliases(UINT uPos, UINT uSize);
		BOOL	DeleteAlias(UINT uPos);
		void	PackAliases(UINT uPos, UINT uSize);
	};

// End of File

#endif
