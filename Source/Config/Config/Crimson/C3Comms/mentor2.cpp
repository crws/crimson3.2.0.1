
#include "intern.hpp"

#include "mentor2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mentor2 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMentor2MasterSerialDeviceOptions, CUIItem);

// Constructor

CMentor2MasterSerialDeviceOptions::CMentor2MasterSerialDeviceOptions(void)
{
	m_Drop 	= 1;
	}

// Download Support	     

BOOL CMentor2MasterSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CMentor2MasterSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Mentor2 Master Serial Driver
//

// Instantiator

ICommsDriver * Create_Mentor2MasterSerialDriver(void)
{
	return New CMentor2MasterSerialDriver;
	}

// Constructor

CMentor2MasterSerialDriver::CMentor2MasterSerialDriver(void)
{
	m_wID		= 0x3334;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Nidec - Control Techniques";
	
	m_DriverName	= "Mentor II Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Mentor II";	

	AddSpaces();
	}

// Destructor

CMentor2MasterSerialDriver::~CMentor2MasterSerialDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CMentor2MasterSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMentor2MasterSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CMentor2MasterSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMentor2MasterSerialDeviceOptions);
	}

// Implementation     

void CMentor2MasterSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "P",  "Menu : Parameter", 10, 0, 9999, addrLongAsLong));
	
	}

// Address Management

BOOL CMentor2MasterSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CMentor2Dialog Dlg(ThisObject, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CMentor2MasterSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uPos  = Text.Find(':');
	
	if( uPos == NOTHING ) {
	
		Error.Set( CString(IDS_DRIVER_ADDR_COLON),
			   0
			   );
			   
		return FALSE;
		}
	
	UINT uType  = pSpace->m_uType;

	UINT uMenu  = tatoi(Text.Mid(0, uPos));

	UINT uParam = tatoi(Text.Mid(uPos + 1));

	UINT uMax   = pSpace->m_uMaximum;

	if( (uMenu > uMax / 100) || (uParam > uMax % 100) ) {

			CString Text;

			CString s = pSpace->m_Prefix;

			UINT uMin = pSpace->m_uMinimum;

			Text.Printf( "%s%2.2d:%2.2d - %s%2.2d:%2.2d",
					s,
					uMin / 100,
					uMin % 100,
					s,
					uMax / 100,
					uMax % 100
					);
				
			Error.Set( Text,
				   0
				   );
						   
			return FALSE;
			}
		
	UINT uAddr = uMenu * 100 + uParam;

	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Type   = uType;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = LOWORD(uAddr);

	return TRUE;
	
	}

BOOL CMentor2MasterSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpace * pSpace	= GetSpace(Addr);

		UINT uAddr	= Addr.a.m_Offset;

		if( uAddr <= pSpace->m_uMaximum ) {

			UINT uMenu  = uAddr / 100;

			UINT uParam = uAddr % 100;

			Text.Printf( "%s%2.2d:%2.2d",
					pSpace->m_Prefix,
					uMenu,
					uParam
					);

			return TRUE;
			}
		}

	return FALSE;
	
	}

//////////////////////////////////////////////////////////////////////////
//
// Mentor2 via MPI Driver Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CMentor2Dialog, CStdAddrDialog);
		
// Constructor

CMentor2Dialog::CMentor2Dialog(CMentor2MasterSerialDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "Mentor2ElementDlg";
	}

// Overridables

void CMentor2Dialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CMentor2Dialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	if( fEnable ) {

		UINT uFind = Text.Find(':');

		GetDlgItem(2002).SetWindowText(Text.Left(uFind));

		GetDlgItem(2004).SetWindowText(":");

		GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1));
		}

	GetDlgItem(2002).EnableWindow(fEnable);
	
	GetDlgItem(2004).EnableWindow(fEnable);
	
	GetDlgItem(2005).EnableWindow(fEnable);
	}

CString CMentor2Dialog::GetAddressText(void)
{
	CString Text;

	Text += GetDlgItem(2002).GetWindowText();

	Text += ":";
	
	Text += GetDlgItem(2005).GetWindowText();

	return Text;
	}

// End of File
