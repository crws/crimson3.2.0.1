
#include "intern.hpp"

#include "adenus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Adenus Telnet Driver
//

// Instantiator

ICommsDriver *	Create_AdenusDriver(void)
{
	return New CAdenusDriver;
	}

// Constructor

CAdenusDriver::CAdenusDriver(void)
{
	m_wID		= 0x402F;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "Adenus";
	
	m_DriverName	= "Telnet Driver";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Adenus Telnet";

	m_fSingle	= TRUE;
	}

// Binding Control

UINT CAdenusDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CAdenusDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// End of File
