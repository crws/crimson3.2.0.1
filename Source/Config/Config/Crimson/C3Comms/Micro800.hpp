
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MICRO800_HPP
	
#define	INCLUDE_MICRO800_HPP

//////////////////////////////////////////////////////////////////////////
//
// CIP Address Descriptor
//

struct CCipDesc
{
	CString    m_Name;
	UINT	   m_uType;
	BOOL	   m_fNamed;
	BOOL	   m_fUsed;
	CString    m_CipType;
	CLongArray m_Dims;
	UINT	   m_uIndex;
	UINT	   m_uChars;
	};

bool operator >(CCipDesc const &Left, CCipDesc const &Right);

typedef CMap<CCipDesc, DWORD> CDescMap;

typedef CList<CCipDesc> CDescList;

typedef CArray<DWORD> CSlotArray;

//////////////////////////////////////////////////////////////////////////
//
// CIP Table Support
//

struct CTableSlot
{
	UINT     m_Slot;
	UINT     m_Key;
	UINT     m_Size;
	CCipDesc m_Desc;
	
	bool operator ==(const CTableSlot &That);
	bool operator < (const CTableSlot &That);
	};

bool CTableSlot::operator ==(const CTableSlot &That)
{
	return m_Key == That.m_Key && m_Slot == That.m_Slot;
	}

bool CTableSlot::operator < (const CTableSlot &That)
{
	if( m_Key == That.m_Key ) {

		CAddress Addr;

		Addr.m_Ref = m_Slot;

		return Addr.a.m_Offset + m_Size + That.m_Size < 0xFFFF;
		}		

	return FALSE;
	}

typedef CArray <CTableSlot> CTableSlotArray;

// Sort Help

inline int AfxCompare(CTableSlot const &s1, CTableSlot const &s2)
{
	CAddress a1, a2;

	a1.m_Ref = s1.m_Slot;

	a2.m_Ref = s2.m_Slot;

	if( a1.a.m_Table  < a2.a.m_Table  ) return +1;

	if( a1.a.m_Table  > a2.a.m_Table  ) return -1;

	if( a1.a.m_Offset < a2.a.m_Offset ) return +1;

	if( a1.a.m_Offset > a2.a.m_Offset ) return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Constants
//

static CString CIP_PREFIX  = L"cip";

static CString CIP_TYPES[] = { CIP_PREFIX + L"BOOL",
			       CIP_PREFIX + L"USINT",
			       CIP_PREFIX + L"INT",
			       CIP_PREFIX + L"DINT",
			       CIP_PREFIX + L"REAL",
			       CIP_PREFIX + L"STRING",
			       CIP_PREFIX + L"SINT",
			       CIP_PREFIX + L"BYTE",
			       CIP_PREFIX + L"UINT",
			       CIP_PREFIX + L"WORD",
			       CIP_PREFIX + L"UDINT",
			       CIP_PREFIX + L"DWORD",
			       CIP_PREFIX + L"TIME",
			       CIP_PREFIX + L"DATE",
			       CIP_PREFIX + L"LINT",
			       CIP_PREFIX + L"ULINT",
			       CIP_PREFIX + L"LWORD",
			       CIP_PREFIX + L"LREAL"}; const

 //////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Enumerations
//

enum type {

	typeBOOL  = 0,
	typeUSINT = 1,
	typeINT   = 2, 
	typeDINT  = 3, 
	typeREAL  = 4,
	typeSTR   = 5,
	typeSINT  = 6,
	typeBYTE  = 7,
	typeUINT  = 8,
	typeWORD  = 9,
	typeUDINT = 10,
	typeDWORD = 11,
	typeTIME  = 12,
	typeDATE  = 13,
	typeLINT  = 14,
	typeULINT = 15,
	typeLWORD = 16,
	typeLREAL = 17,
	 };

enum part {

	partName   = 0,
	partType   = 1,
	partDim    = 2,
	partChars  = 3,
	partInit   = 4,
	partDir    = 5,
	partAttr   = 6,
	partNote   = 7,
	partAlias  = 8,
	};

enum tag {

	tagName  = 0,
	tagLabel = 1,
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Micro800 Forward Declarations
//

class CAbMicro800TagsCreator;

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Device Options
//

class CABMicro800DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABMicro800DeviceOptions(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
		void Rebuild(void);
		void UpdateExtent(CAddress const &Addr, INT nSize);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		void PostLoad(void);

		// CIP Conversions
		CString ExtToCip(CString Ext);
		BYTE    GetCipTypeEnum(CString const &Type);
		void    InitSlotIndex(void);
		UINT    GetSlotIndex(CCipDesc const &Desc);
				
		// Tags
		UINT    AddDesc(CCipDesc &Desc);
		UINT    AddDesc(CCipDesc &Desc, UINT uSlot);
		BOOL    RemoveDesc(CTableSlot &Slot, BOOL fNamed);
		UINT    TypeFromAtomic(CString const &Type);
		CString GetTypeString(UINT uType);
		BOOL    IsNameValid(CString const &Name);
		void    SetUsed(UINT uPos);
		void    ClearUsed(void);
		CString CipTypeFromEnum(UINT uEnum);
		UINT    CipTypeFromAtomic(CString const &Type);
		BOOL    IsSTR(UINT uType);
		BOOL    IsNamed(UINT uType, UINT uDims);
		BOOL    IsDouble(UINT uType);

		// Tag Creation
		BOOL    ImportTags(IMakeTags *pTags, CString DevName, CAbMicro800TagsCreator &AbTags, CString &Error);

		// Data Acccess
		UINT    FindTableSlot(CAddress const &Addr, CTableSlot &Slot);

		// Public Data
		CDescMap	m_Map;

	protected:

		// Data Members
		UINT		m_Push;
		CSlotArray	m_NamedSlots;
		CTableSlotArray	m_TableSlots;
		CDescList	m_List;
		CString		m_Root;
		UINT		m_Name;
		UINT		m_Label;
		UINT		m_Index;
		UINT		m_Named;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void ImportFromFile(void);
		void ExportToFile(void);
		void ManageTags(void);
		void LoadTags(CTreeFile &Tree);
		void SaveTags(CTreeFile &Tree);
		void AddInitString(CString const &Text, CInitData &Init);	
		UINT GetNextTableSlot(CCipDesc const &Desc);
		UINT GetNextNamedSlot(CCipDesc const &Desc);
		BOOL GetOffset(CString Dims, CCipDesc const &Desc, UINT &uOut);
		UINT GetSize(CCipDesc const &Desc);
		UINT AddTableSlot(UINT uSlot, UINT uCip, CCipDesc Desc, UINT uSize);
		void UpdateExtent(CTableSlot const &Slot, INT nSize);
		CString ExpandDims(UINT uOffset, CCipDesc const &Desc);
		CString FindFilename(CString Path);

		// Import Help
		BOOL PreImport(CTextStreamMemory &Stream, CString &Path);
		BOOL LineImport(CTextStreamMemory &Stream, CStringArray &Parts, CString &Dims);
		void PostImport(CTextStreamMemory &Stream, BOOL fRebuild);

		// Import Operations
		BOOL MakeDesc(CCipDesc &Desc, CStringArray &Parts, CString &Dims, CString &Error);
		void MakeTag(IMakeTags * pTags, CString DevName, CCipDesc &Desc, CStringArray Parts, CAbMicro800TagsCreator &AbTags, UINT &uOffset, CString &Error);

		// Helpers
		CString	GetErrorText(CString Name, CString Tag);
		void    StripCipPrefix(CString &Name);
		CString GetCipType(CString Type);
		BYTE    GetCipDefault(void);
		void    Validate(CString &String);
		BOOL    IsValid(CString String);
		BOOL    IsValid(CCipDesc const &Desc);
		BOOL    IsDuplicate(CString Name);
		BOOL    IsTIME(UINT uType);
		BOOL    IsDATE(UINT uType);
		BOOL    IsBOOL(UINT uType);
		BOOL    IsBYTE(UINT uType);
		BOOL    IsWORD(UINT uType);
		BOOL    IsLONG(UINT uType);
		BOOL    IsREAL(UINT uType);
		BOOL	IsSigned(UINT uType);
		BOOL    IsArrayElement(CString Name);
		void    WalkSlots(void);
		void    PrintTags(void);
				
	private:

		enum {
			pushManage	= 0,
			pushImport	= 1,
			pushExport	= 2
			};
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 TCP Device Options
//

class CABMicro800TcpDeviceOptions : public CABMicro800DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABMicro800TcpDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:

		// Data Members
		UINT	m_Addr;
		UINT	m_Timeout;

		// Meta Data Creation
		void AddMetaData(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Serial Device Options
//

class CABMicro800SerialDeviceOptions : public CABMicro800DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABMicro800SerialDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:

		// Data Members
		UINT	m_Station;
		UINT	m_Timeout;

		// Meta Data Creation
		void AddMetaData(void);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Base Driver
//

class CABMicro800BaseDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CABMicro800BaseDriver(void);

		// Driver Data
		UINT GetFlags(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

		// Notifications
		void NotifyInit(CItem * pConfig);
		void NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);

		// Tag Import
		BOOL MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 TCP Driver
//

class CABMicro800TcpMaster : public CABMicro800BaseDriver
{
	public:
		// Constructor
		CABMicro800TcpMaster(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Serial Driver
//

class CABMicro800SerialMaster : public CABMicro800BaseDriver
{
	public:
		// Constructor
		CABMicro800SerialMaster(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Serial Driver Options
//

class CABMicro800SerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABMicro800SerialDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Address;
		BOOL m_fCRC;
		BOOL m_fHalf;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Selection Dialog
//

class CABMicro800Dlg : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CABMicro800Dlg(CABMicro800BaseDriver *pDriver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Data Members
		CABMicro800BaseDriver    * m_pDriver;
		CABMicro800DeviceOptions * m_pConfig;
		CAddress		 & m_Addr;
		CCipDesc		   m_Desc;

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		void OnSelChange(UINT uId, CWnd &Wnd);

		// Implementation
		void LoadListBox(void);
		CString GetTypeString(UINT uType);
		void ShowDetails(void);
		void DoEnables(void);
		void ShowAddress(UINT uOffset);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Management Dialog
//

class CABMicro800ManageDlg : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CABMicro800ManageDlg(CABMicro800DeviceOptions *pConfig);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Data Members
		CABMicro800DeviceOptions * m_pConfig;
		CCipDesc		   m_Desc;

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCreate(UINT uID);
		BOOL OnDel(UINT uID);
		void OnSelChange(UINT uId, CWnd &Wnd);
		void OnCheckChange(UINT uId, CWnd &Wnd);
		void OnEditChange(UINT uId, CWnd &Wnd);
		
		// Implementation
		void LoadListBox(void);
		void LoadCombo(void);
		CString GetTypeString(UINT uType);
		void DoEnables(void);
		void ShowDetails(void);		
		void ShowDims(void);
		void DisableArrays(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Micro800 Tags Creator UI
//

class CAbMicro800TagsCreator : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAbMicro800TagsCreator(void);
		CAbMicro800TagsCreator(CString Name);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		CViewWnd * CreateView(UINT uType);

		// Persistance
		void GetTagConfig(CItem * pItem);
		void SaveTagConfig(CItem * pItem);

		// Data Access
		CString	m_Root;
		UINT    m_Define;
		UINT	m_Tag;
		UINT	m_Type;
		UINT	m_Comment;
		UINT	m_Alias;

		// Access
		CString Construct(CStringArray Parts, UINT uEnum, UINT uOffset);
					
	protected:

		// Data Members
		UINT	m_Name;
		UINT	m_Label;

		// Implementaion
		void SetDefaults(void);
		void UpdateFields(UINT uValue);
		UINT GetFieldMask(UINT uValue, UINT uField);
					
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Tags Creator Page
//

class CAbMicro800TagsCreatorPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAbMicro800TagsCreatorPage();

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);
	};


//////////////////////////////////////////////////////////////////////////
//
// AB Micro800 Import Error Dialog
//

class CABMicro800ImportErrorDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CABMicro800ImportErrorDialog(CStringArray List);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCopy(UINT uID);
					
	protected:

		CStringArray m_Errors;
		
		// Implementation

		void LoadErrorList(void);
		
	};


// End of File

#endif
