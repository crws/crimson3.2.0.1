
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ADAM2_HPP
	
#define	INCLUDE_ADAM2_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cmd List Item
//

struct CAdmMdl {

	CString		m_Model;
	CStringArray	m_Cmds;
	};

typedef CArray <CAdmMdl> CAdmMdlArray;

//////////////////////////////////////////////////////////////////////////
//
// Cmd List Item
//

struct CAdmCmd {

	CString	 m_pRead;
	CString	 m_pWrite;
	BYTE	 m_bCmdType;
	BYTE	 m_bCmd;
	BYTE	 m_bChan;
	BYTE	 m_bForm;
	CAddress m_Addr;
	CString  m_Text;
	BYTE	 m_bMisc;
	};

typedef CArray <CAdmCmd> CAdmCmdArray;

//////////////////////////////////////////////////////////////////////////
//
// Enumerations
//

enum def {
	
	defNothing = 255,
	defCmd	   = 1,
	defDesc	   = 2,
	defRead    = 3,
	defWrite   = 4,
	defType	   = 5,
	defForm	   = 6,
	defFormat  = 7,
	};


enum id {
	
	idListType  = 1001,
	idListCmd   = 1003,
	idComboText = 2001,
	idComboEdit = 2002,
	idTypeText  = 2005,
	idTypeEdit  = 2006,
	idDetType   = 3002,
	idDetAccess = 3004,
	idDetRead   = 3006,
	idDetWrite  = 3008,
	};

enum form {
	formInt	    = 0,
	formReal    = 1,
	formHex	    = 2,
	formText    = 3,
	formCmd     = 4,
	formDigitM  = 0x10,
	};

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series v2 Device Options
//

class CAdam4000v2DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAdam4000v2DeviceOptions(void);

		// Destructor
		~CAdam4000v2DeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Persistance
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Cmd List Access
		CStringArray GetCmdTypes(void);
		CStringArray GetCmdList(UINT uType);
		CAddress     GetAddress(CString Text);
		CString	     GetText(DWORD dwRef);
		CAdmCmd *    GetCommand(CAddress Addr);
		CString	     GetSelectListElement(UINT uType, UINT uElement);
		CString	     GetSelectListElement(UINT uType, UINT uCmd, UINT uElement);
		void	     GetSelectIndexes(CString Text, UINT &uType, UINT &uCmd);
		BYTE	     GetCommandForm(CString Text);
		BYTE	     GetCommandForm(CAddress Addr);
		void	     EmptyCmdList(void);
		void	     InitParams(void);

		// Model List Access
		CString   GetModelEnum(void);
		
		// Public Data
		UINT m_Drop;
		UINT m_Model;											

	protected:
		// Data Members
		CAdmCmdArray	*	m_pCmds;
		CArray <CStringArray>	m_CmdList;
		CStringArray		m_CmdTypes;
		CAdmMdlArray    *	m_pMdls;
		CArray <CStringArray>	m_SelList;
		CStringArray		m_SelTypes;
		UINT			m_uTypes;
		UINT			m_uLastRef;
		UINT			m_uLastTxt;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void	  InitModels(void);
		void	  InitCmdList(void);
		void	  ApplyFilter(void);
		void	  PrepareList(CString &Line);
		void	  LoadCmdList(CStringArray Array, UINT uCmds);
		CAdmCmd * AddCmd(CString Text);
		BOOL	  ParseCommand(CString Text, CString &CmdType, CString &Cmd, CString &Chan, CString &Type);
		BOOL      SetAddress(CString Type, CAddress &Addr);
		BOOL	  SetNamedAddr(CString Type, CAddress &Addr);
		BOOL	  SetTextAddr(CAddress &Addr);
		CString	  GetCommandListElement(UINT uType, UINT uElement);
		CString	  GetCommandListElement(UINT uType, UINT uCmd, UINT uElement);
		CString	  FindLineElement(CString Line, UINT uElement, PCTXT pDel = L"\t");
		void	  MakeSelTypes(void);
		void	  MakeSelType(CString Type);
		void	  MakeAllSelTypes(void);
		void	  MakeSelCmdList(void);
		void	  MakeSelCmd(UINT uType, CString Cmd);
		void	  MakeAllCmds(void);
		void	  EmptySelList(void);
		

		
	};

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series v2 Device Options UI Page
//

class CAdam4000v2DeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAdam4000v2DeviceOptionsUIPage(CAdam4000v2DeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CAdam4000v2DeviceOptions * m_pOption;
	}; 

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Module v2 Driver
//

class CAdam4000v2Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CAdam4000v2Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Notifications
		void NotifyInit(CItem * pConfig);
		
		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Module Address Selection Dialog
//

class CAdam4000v2AddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAdam4000v2AddrDialog(CAdam4000v2Driver &Driver, CAddress &Addr, CItem *pConfig);
		
	protected:
		// Data Members
		CStdCommsDriver   * m_pDriver;
		CItem		  * m_pConfig;
		CAddress	  * m_pAddr;
		UINT		    m_uCmd;
		UINT		    m_uType;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSelectChange(UINT uID, CWnd &Wnd);
		void OnCmdSelChange(UINT uID, CWnd &Wnd);
		
		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadList(UINT uID, CStringArray Array);
		void SetListBoxSel(UINT uID, UINT uSel);
		void SetCurrentSelect(void);
		void Clear(void);
		void SetType(void);
		void SetAccess(void);
		void SetRead(void);
		void SetWrite(void);
		void InitChannel(void);
		void EnableChannel(void);
		void SetDataType(BOOL fInit);
	};


// End of File

#endif
