
#include "intern.hpp"

#include "tosh.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Toshiba PLC Serial Device Options
//
//
// Dynamic Class

AfxImplementDynamicClass(CToshT2MasterSerialDeviceOptions, CUIItem);

// Constructor

CToshT2MasterSerialDeviceOptions::CToshT2MasterSerialDeviceOptions(void)
{
	m_Drop 	= 1;

	m_Model = SERIESS;
	}

// UI Managament

void CToshT2MasterSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support	     

BOOL CToshT2MasterSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_Model));
	
	return TRUE;
	}

// Meta Data Creation

void CToshT2MasterSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Model);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 Master Serial Driver - Supports Toshiba T-Series and EX-Series PLCs.
//
//

// Instantiator

ICommsDriver * Create_ToshT2MasterSerialDriver(void)
{
	return New CToshT2MasterSerialDriver;
	}

// Constructor

CToshT2MasterSerialDriver::CToshT2MasterSerialDriver(void)
{
	m_wID		= 0x3309;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Toshiba";
	
	m_DriverName	= "Series PLC";
	
	m_Version	= "1.10";
	
	m_ShortName	= "Toshiba PLC";

	m_uSelectedType	= addrWordAsWord;

	AddSpaces();
	}

// Destructor

CToshT2MasterSerialDriver::~CToshT2MasterSerialDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CToshT2MasterSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CToshT2MasterSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS422;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CToshT2MasterSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CToshT2MasterSerialDeviceOptions);
	}

// Address Management

BOOL CToshT2MasterSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CToshT2AddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CToshT2MasterSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT    uExtra = 0;

	UINT    uPos   = Text.Find('-');

	CString Type   = StripType(pSpace, Text);

	if( GetModel(pConfig) != SERIESEX ) {

		if( uPos < NOTHING ) {

			CString Op = Text.Mid(uPos);

			uExtra = GetExtra(Op);

			Text = Text.Left(uPos);
			}
		}
	else {
		if( uPos < NOTHING ) {

			Text = Text.Left(uPos);
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Type.MakeUpper();

		BOOL fIO = pSpace->m_Prefix == "XW" || pSpace->m_Prefix == "YW";

		if( Type == "REAL" ) m_uSelectedType = fIO ? addrWordAsReal : addrRealAsReal;
		if( Type == "LONG" ) m_uSelectedType = fIO ? addrWordAsLong : addrLongAsLong;

		Addr.a.m_Type  = m_uSelectedType;

		Addr.a.m_Extra = uExtra;

		return TRUE;
		}

	return FALSE;
	}

BOOL CToshT2MasterSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr) ) {

		if( GetModel(pConfig) != SERIESEX ) {

			if( IsExtra(Addr.a.m_Table) ) {

				Text += GetSuffix(Addr.a.m_Extra);
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

// Helpers

UINT CToshT2MasterSerialDriver::GetModel(CItem *pConfig)
{
	return pConfig->GetDataAccess("Model")->ReadInteger(pConfig);
	
	}
  
CString CToshT2MasterSerialDriver::GetSuffix(UINT uIndex)
{
	switch( uIndex ) {

		case 1:
			return "-OFF";
		case 2:
			return "-ST";
		}

	return "-ON";
	
	}

BOOL CToshT2MasterSerialDriver::IsExtra(UINT uTable)
{
	switch(uTable) {

		case 9:
		case 10:
			return TRUE;
		}
	
	return FALSE;
	}

UINT CToshT2MasterSerialDriver::GetExtra(CString Suffix)
{
	for( UINT u = 0; u <= 2; u++ ) {
	
		if( Suffix == GetSuffix(u) ) {

			return u;
			}
		}

	return 0;
	}

BOOL CToshT2MasterSerialDriver::CheckAlignment(CSpace * pSpace)
{
	return FALSE;
	}

void CToshT2MasterSerialDriver::SetTypeSelection(UINT uType)
{
	m_uSelectedType = max( min(uType, addrRealAsReal), addrWordAsWord);
	}

// Implementation     

void CToshT2MasterSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,  "D",	"Data Registers",	10, 0,  9999, addrWordAsWord, addrRealAsReal));

	AddSpace(New CSpace(11, "F",	"File Registers",	10, 0, 32767, addrWordAsWord, addrRealAsReal));

	AddSpace(New CSpace(2,  "XW",	"Input Words",		10, 0,   999, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(3,  "YW",	"Output Words",		10, 0,   999, addrWordAsWord, addrWordAsReal));

//	AddSpace(New CSpace(4,  "RC",	"C Registers",		10, 0,  9999, addrWordAsWord, addrWordAsReal));
	
//	AddSpace(New CSpace(5,  "RR",	"R Registers",		10, 0,  9999, addrWordAsWord, addrWordAsReal));

//	AddSpace(New CSpace(6,  "RZ",	"Z Registers",		10, 0,  9999, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(7,  "RW",	"RW Registers",		10, 0,  9999, addrWordAsWord, addrRealAsReal));

//	AddSpace(New CSpace(8,  "ZW",	"ZW Registers",		10, 0,  9999, addrWordAsWord, addrWordAsReal));

	AddSpace(New CSpace(9,  "T",	"Timers",		10, 0,   999, addrWordAsWord));

	AddSpace(New CSpace(10, "C",	"Counters",		10, 0,   999, addrWordAsWord));

	}

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 UDP Master Driver
//

// Instantiator

ICommsDriver *	Create_ToshT2UDPDriver(void)
{
	return New CToshT2UDPDriver;
	}

// Constructor

CToshT2UDPDriver::CToshT2UDPDriver(void)
{
	m_wID		= 0x3502;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Toshiba";
	
	m_DriverName	= "T2 PLC Master";
	
	m_Version	= "BETA";
	
	m_ShortName	= "T2";

	AddSpaces();

	}

// Binding Control

UINT CToshT2UDPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CToshT2UDPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CToshT2UDPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CToshT2UDPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CToshT2UDPDeviceOptions);
	}

// Implementation     

void CToshT2UDPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "D",	"Data Registers",	10, 0, 9999, addrWordAsWord, addrWordAsReal));
	}

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 UDP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CToshT2UDPDeviceOptions, CUIItem);

// Constructor

CToshT2UDPDeviceOptions::CToshT2UDPDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Port  = 2000;

	m_Keep    = TRUE;

	m_Ping    = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200;
	}

// UI Managament

void CToshT2UDPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support	     

BOOL CToshT2UDPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
		
	return TRUE;
	}

// Meta Data Creation

void CToshT2UDPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}
 	  


//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CToshT2AddrDialog, CStdAddrDialog);
		
// Constructor

CToshT2AddrDialog::CToshT2AddrDialog(CToshT2MasterSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "ToshT2ElementDlg";
	}

void CToshT2AddrDialog::SetAddressText(CString Text)
{
	GetDlgItem(2004).ShowWindow(FALSE);

	GetDlgItem(2005).ShowWindow(FALSE);

	GetDlgItem(2006).ShowWindow(FALSE);
		
	if( m_pSpace ) {

		CToshT2MasterSerialDriver *pDriver = (CToshT2MasterSerialDriver *)m_pDriver;
		
		if( pDriver->GetModel(m_pConfig) !=  SERIESEX ) {

			if ( pDriver->IsExtra(m_pSpace->m_uTable) ) {

				GetDlgItem(2004).ShowWindow(TRUE);

				GetDlgItem(2005).ShowWindow(TRUE);

				GetDlgItem(2006).ShowWindow(TRUE);

				UINT uPos = Text.Find('-');

				SetRadioGroup(2004, 2006, 0);

				if( uPos < NOTHING ) {

					CString Op = Text.Mid(uPos);

					SetRadioGroup(2004, 2006, pDriver->GetExtra(Op));

					Text = Text.Left(uPos);

					}
				}
			}
		}

	CStdAddrDialog::SetAddressText(Text);	
	}

CString CToshT2AddrDialog::GetAddressText(void)
{
	CString Text = CStdAddrDialog::GetAddressText();

	CToshT2MasterSerialDriver *pDriver = (CToshT2MasterSerialDriver *)m_pDriver;
		
	if( pDriver->GetModel(m_pConfig) != SERIESEX ) {

		UINT uRadio = GetRadioGroup(2004, 2006);

		Text += pDriver->GetSuffix(uRadio);
		}

	pDriver->SetTypeSelection(GetTypeCode());
	
	return Text;
	}

BOOL CToshT2AddrDialog::AllowSpace(CSpace *pSpace)
{
	CToshT2MasterSerialDriver *pDriver = (CToshT2MasterSerialDriver *)m_pDriver;

	return (pSpace->m_uTable != SPACE_F) || (pDriver->GetModel(m_pConfig) != SERIESEX);
	}

BOOL CToshT2AddrDialog::AllowType(UINT uType)
{
	if( m_pSpace ) {

		switch( m_pSpace->m_uTable ) {

			case 1:
			case 7:
				switch( uType ) {

					case addrWordAsWord:
					case addrWordAsReal:
					case addrLongAsLong:
					case addrRealAsReal:
						return TRUE;
					}

				return FALSE;

			case 11:
				switch( uType ) {

					case addrWordAsWord:
					case addrLongAsLong:
					case addrRealAsReal:
						return TRUE;
					}

				return FALSE;
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Toshiba EX 40 + PLC Serial Master
//

// Instantiator

ICommsDriver *	Create_ToshExPlusMasterDriver(void)
{
	return New CToshExPlusMasterDriver;
	}

// Constructor

CToshExPlusMasterDriver::CToshExPlusMasterDriver(void)
{
	m_wID		= 0x4092;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Toshiba";
	
	m_DriverName	= "EX40+ PLC Master";
	
	m_Version	= "BETA";
	
	m_ShortName	= "EX40+";

	AddSpaces();

	}

// Binding Control

UINT CToshExPlusMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CToshExPlusMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS422;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CToshExPlusMasterDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Implementation     

void CToshExPlusMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "X",	"Device X Memory",		8, 0, 71, addrBitAsByte, addrBitAsReal));
	AddSpace(New CSpace(2, "Y",	"Device Y Memory",		8, 0, 39, addrBitAsByte, addrBitAsReal));
	AddSpace(New CSpace(3, "R",	"Device R Memory",		8, 0,127, addrBitAsByte, addrBitAsReal));
	AddSpace(New CSpace(4, "L",	"Device L Memory",		8, 0,127, addrBitAsByte, addrBitAsReal));
	AddSpace(New CSpace(5, "S",	"Device S Memory",		8, 0,255, addrBitAsByte, addrBitAsReal));
	AddSpace(New CSpace(6, "T",	"Device T Memory",		8, 0,127, addrBitAsByte, addrBitAsReal));
	AddSpace(New CSpace(7, "C",	"Device C Memory",		8, 0,127, addrBitAsByte, addrBitAsReal));
	
	AddSpace(New CSpace(8, "CC",	"Counter Current Value",	8, 0,127, addrByteAsWord, addrByteAsReal));
	AddSpace(New CSpace(9, "TC",	"Timer Current Value",		8, 0,127, addrByteAsWord, addrByteAsReal));
	

	}


 
// End of File
