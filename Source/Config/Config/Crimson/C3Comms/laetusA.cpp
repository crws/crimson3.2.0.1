
#include "intern.hpp"

#include "LaetusA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CLaetusBarcodeDeviceOptions, CUIItem);

// Constructor

CLaetusBarcodeDeviceOptions::CLaetusBarcodeDeviceOptions(void)
{
	m_Drop	= "1";
	m_Unit	= LLS570;
	}

// UI Management

void CLaetusBarcodeDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Drop" ) {

		if( InvalidDropChar(m_Drop[0]) ) {
			
			m_Drop = "1";
			}

		pWnd->UpdateUI("Drop");
		}
	}

// Download Support

BOOL CLaetusBarcodeDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop[0]));
	Init.AddByte(BYTE(m_Unit));

	return TRUE;
	}

// Meta Data Creation

void CLaetusBarcodeDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Drop);
	Meta_AddInteger(Unit);
	}

// Helper

BOOL CLaetusBarcodeDeviceOptions::InvalidDropChar(TCHAR c)
{
	return !isdigit(c) && ( c < 'A' || c > 'Z');
	}

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Master Comms Driver
//

// Instantiator

ICommsDriver *	Create_LaetusBarcodeDriver(void)
{
	return New CLaetusBarcodeDriver;
	}

// Constructor

CLaetusBarcodeDriver::CLaetusBarcodeDriver(void)
{
	m_wID		= 0x4054;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Laetus";
	
	m_DriverName	= "Barcode Scanner";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Laetus Barcode Scanner";

	m_DevRoot	= "LBS";

	AddSpaces();
	}

// Binding Control

UINT	CLaetusBarcodeDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CLaetusBarcodeDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS	CLaetusBarcodeDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CLaetusBarcodeDeviceOptions);
	}

// Address Management

BOOL	CLaetusBarcodeDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CLaetusBarcodeAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL	CLaetusBarcodeDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uT = pSpace->m_uTable;

	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = uT;
	Addr.a.m_Type   = pSpace->m_uType;

	UINT uHS = GetItemType(uT);

	switch( uHS ) {

		case HSNONE:
		case HSEXTH:
			return TRUE;

		case HSEXTD:
			Addr.a.m_Extra  = min(tatoi(Text), 9);
			return TRUE;

		case HSALPH:

			Text.MakeUpper();

			Addr.a.m_Offset = GetItemOffset( uT,
							 BYTE(Text[0]),
							 pConfig->GetDataAccess("Unit")->ReadInteger(pConfig)
							 );

			return TRUE;

		case HSDECI:

			UINT uO;

			uO = tatoi(Text);

			if( uO < pSpace->m_uMinimum || uO > pSpace->m_uMaximum ) uO = pSpace->m_uMinimum;

			if( uT == SOM && uO == 2 ) uO = 1;

			Addr.a.m_Offset = uO;

			return TRUE;
		}

	return FALSE;
	}

BOOL	CLaetusBarcodeDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	UINT uT = Addr.a.m_Table;

	UINT uHS = GetItemType(uT);

	CSpace * pSpace = GetSpace(Addr);

	if( pSpace ) {

		CString s = "";

		switch( uHS ) {

			case HSEXTD:
				s.Printf( "%1.1d", min(Addr.a.m_Extra, 9) );
				break;

			case HSEXTH:
				s.Printf( "%1.1X", Addr.a.m_Extra );
				break;

			case HSALPH:

				UINT uO;

				uO = Addr.a.m_Offset;

				if( uO >= 'A' ) uO -= 'A';

				s.Printf( "%c", GetItemLetter(uT, uO, pConfig->GetDataAccess("Unit")->ReadInteger(pConfig)));
				break;

			case HSDECI:
				s.Printf( "%d", Addr.a.m_Offset);
				break;

			default:
				break;
			}

		Text.Printf( "%s%s",

			pSpace->m_Prefix,
			s
			);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void	CLaetusBarcodeDriver::AddSpaces(void)
{
	AddSpace( New CSpace(SOM,  "SOM",  "Set Operating Mode",			10,   1,   6, YY ));
	AddSpace( New CSpace(LRS,  "LRS",  "Read Result Status Byte",			10,   0,   0, YY ));
	AddSpace( New CSpace(LRD,  "LRD",  "   Read Result Data String",		10,   0,  13, LL ));
	AddSpace( New CSpace(LES,  "LES",  "Wrong Reads Status Byte",			10,   0,   0, YY ));
	AddSpace( New CSpace(LED,  "LED",  "   Wrong Reads Data String",		10,   0,  13, LL ));
	AddSpace( New CSpace(LXC,  "LXC",  "Clear Wrong Reads Buffer",			10,   0,   0, YY ));
	AddSpace( New CSpace(SRG,  "SRG",  "Start/Stop Reading Gate command",		10,   0,   0, YY ));
	AddSpace( New CSpace(MCF,  "MCF",  "Matchcode Configuration",			10, 'A', 'T', LL ));
	AddSpace( New CSpace(MC1,  "MC1",  "   Matchcode 1",				10,   0,  13, LL ));
	AddSpace( New CSpace(MC2,  "MC2",  "   Matchcode 2",				10,   0,  13, LL ));
	AddSpace( New CSpace(CCF,  "CCF",  "Code Configuration",			10, '@', 'Z', LL ));
	AddSpace( New CSpace(CCS,  "CCS",  "   Specific Symbology Parameter String",	10,   0,  13, LL ));
	AddSpace( New CSpace(CCL,  "CCL",  "   Code Length String",			10,   0,  13, LL ));
	AddSpace( New CSpace(DCF,  "DCF",  "Device Configuration",			10, 'A', 'Z', LL ));
	AddSpace( New CSpace(OPC,  "OPC",  "Operational Counters",			10,   1,   6, LL ));
	AddSpace( New CSpace(HOC,  "HOC",  "Host Output Configuration",			10, 'A', 'T', LL ));
	AddSpace( New CSpace(TFE,  "TFE",  "   Host Conf - Splitter String",		10,   0,   4, LL ));
	AddSpace( New CSpace(TFH,  "TFH",  "   Host Conf - Header String",		10,   0,   8, LL ));
	AddSpace( New CSpace(TFL,  "TFL",  "   Host Conf - Code Length List String",	10,   0,   8, LL ));
	AddSpace( New CSpace(TFM,  "TFM",  "   Host Conf - Format Mask String",		10,   0,  27, LL ));
	AddSpace( New CSpace(TFS,  "TFS",  "   Host Conf - Separator String",		10,   0,   8, LL ));
	AddSpace( New CSpace(TFT,  "TFT",  "   Host Conf - Terminator String",		10,   0,   8, LL ));
	AddSpace( New CSpace(PEX,  "PEX",  "Execute Percentage Eval. Read",		10,   0,   0, YY ));
	AddSpace( New CSpace(PE1,  "PE1",  "   Percentage Evaluation String 1",		10,   0,  19, LL ));
	AddSpace( New CSpace(PE2,  "PE2",  "   Percentage Evaluation String 2",		10,   0,  19, LL ));
	AddSpace( New CSpace(PE3,  "PE3",  "   Percentage Evaluation String 3",		10,   0,  19, LL ));
	AddSpace( New CSpace(PE4,  "PE4",  "   Percentage Evaluation String 4",		10,   0,  19, LL ));
	AddSpace( New CSpace(SOC,  "SOC",  "Switching Output Configuration",		10, 'A', 'X', LL ));
	AddSpace( New CSpace(DST,  "DST",  "Device Self Test",				10,   0,   0, YY ));
	AddSpace( New CSpace(EEP,  "EEP",  "RAM / EEPROM Transfer",			10,   0,   0, YY ));

	AddSpace( New CSpace(USR,  "USR",  "Send USRC",					10,   0,   0, BB ));
	AddSpace( New CSpace(USRC, "USRC", "   User Defined String - Command",		10,   0,  19, LL ));
	AddSpace( New CSpace(USRR, "USRR", "   USRC Response",				10,   0,  19, LL ));
//	AddSpace( New CSpace(SCK,  "SICK", "Select SICK Protocol",			10,   0,   0, BB ));
	}

// Helpers

UINT	CLaetusBarcodeDriver::GetItemType(UINT uTable)
{
	switch( uTable ) {

		case LES:
		case LED:
			return HSEXTD;

		case MCF:
		case CCF:
		case DCF:
		case HOC:
		case SOC:
			return HSALPH;

		case SOM:
		case OPC:
			return HSDECI;
		}

	return HSNONE;
	}

UINT CLaetusBarcodeDriver::GetItemOffset(UINT uTable, BYTE bChar, UINT uDev)
{
	BOOL fLLS = uDev == LLS570;

	switch( uTable ) {

		case MCF:	return GetMCFOffset(bChar, fLLS);
		case CCF:	return GetCCFOffset(bChar, fLLS);
		case DCF:	return GetDCFOffset(bChar, fLLS);
		case HOC:	return GetHOCOffset(bChar, fLLS);
		case SOC:	return GetSOCOffset(bChar, fLLS);
		}

	return 0;
	}

UINT CLaetusBarcodeDriver::GetMCFOffset(BYTE bChar, BOOL fIsLLS)
{
	switch( bChar ) {

		case 'A':	return 0;
		case 'B':	return 1;
		case 'I':	return 2;
		case 'J':	return 3;
		case 'C':	return 4;
		case 'D':	return 5;
		case 'F':	return 6;
		case 'G':	return 7;
		case 'T':	return fIsLLS ? 8 : 0;
		case 'R':	return fIsLLS ? 9 : 0;
		}

	return 0;
	}

UINT CLaetusBarcodeDriver::GetCCFOffset(BYTE bChar, BOOL fIsLLS)
{
	switch( bChar ) {

		case '@':	return 0;
		case 'A':	return 1;
		case 'L':	return 2;
		case 'P':	return 3;
		case 'M':	return 4;
		case 'C':	return 5;
		case 'D':	return fIsLLS ? 6 : 0;
		case 'X':	return fIsLLS ? 7 : 6;
		case 'S':	return fIsLLS ? 8 : 7;
		case 'T':	return fIsLLS ? 9 : 0;
		}

	return 0;
	}

UINT CLaetusBarcodeDriver::GetDCFOffset(BYTE bChar, BOOL fIsLLS)
{
	switch( bChar ) {

		case 'L':	return 0;
		case 'B':	return fIsLLS ? 1 : 0;
		case 'S':	return fIsLLS ? 2 : 1;
		case 'V':	return fIsLLS ? 3 : 0;
		case 'M':	return fIsLLS ? 4 : 2;
		case 'R':	return fIsLLS ? 0 : 3;
		case 'Z':	return fIsLLS ? 0 : 4;
		case 'P':	return 5;
		case 'Q':	return 6;
		case 'N':	return 7;
		case 'O':	return 8;
		case 'D':	return fIsLLS ? 9 : 0;
		}

	return 0;
	}

UINT CLaetusBarcodeDriver::GetHOCOffset(BYTE bChar, BOOL fIsLLS)
{
	switch( bChar ) {

		case 'H':	return 0;
		case 'S':	return 1;
		case 'T':	return 2;
		case 'R':	return 3;
		case 'L':	return 4;
		case 'B':	return 5;
		case 'F':	return 6;
		case 'A':	return 7;
		case 'M':	return 8;
		case 'N':	return fIsLLS ?  9 : 0;
		case 'E':	return fIsLLS ? 10 : 0;
		}

	return 0;
	}

UINT CLaetusBarcodeDriver::GetSOCOffset(BYTE bChar, BOOL fIsLLS)
{
	if( fIsLLS ) {

		switch( bChar ) {

			case 'A':	return  0;
			case 'W':	return  1;
			case 'B':	return  2;
			case 'X':	return  3;
			case 'G':	return  4;
			case 'V':	return  5;
			case 'L':	return  6;
			case 'O':	return  7;
			case 'M':	return  8;
			case 'N':	return  9;
			case 'P':	return  10;
			case 'Q':	return  11;
			case 'T':	return  12;
			case 'R':	return  13;
			case 'S':	return  14;
			}

		return 0;
		}

	if( bChar >= 'A' && bChar <= 'I' ) return bChar - 'A' + 4;

	switch( bChar ) {

		case 'X':	return 0;
		case 'R':	return 1;
		case 'S':	return 2;
		case 'T':	return 3;
		case 'U':	return 13;
		case 'V':	return 14;
		case 'K':	return 15;
		}

	return 0;
	}

BYTE CLaetusBarcodeDriver::GetItemLetter(UINT uTable, UINT uOffset, UINT uDev)
{
	BOOL fLLS = uDev == LLS570;

	switch( uTable ) {

		case MCF:	return GetMCFLetter(uOffset, fLLS);
		case CCF:	return GetCCFLetter(uOffset, fLLS);
		case DCF:	return GetDCFLetter(uOffset, fLLS);
		case HOC:	return GetHOCLetter(uOffset, fLLS);
		case SOC:	return GetSOCLetter(uOffset, fLLS);
		}

	return 0;
	}

BYTE CLaetusBarcodeDriver::GetMCFLetter(UINT uOffset, BOOL fIsLLS)
{
	switch( uOffset ) {

		case 0:	return 'A';
		case 1:	return 'B';
		case 2:	return 'I';
		case 3:	return 'J';
		case 4:	return 'C';
		case 5:	return 'D';
		case 6:	return 'F';
		case 7:	return 'G';
		case 8: return fIsLLS ? 'T' : 'A';
		case 9: return fIsLLS ? 'R' : 'A';
		}

	return 'A';
	}

BYTE CLaetusBarcodeDriver::GetCCFLetter(UINT uOffset, BOOL fIsLLS)
{
	switch( uOffset ) {

		case 0:	return '@';
		case 1:	return 'A';
		case 2:	return 'L';
		case 3:	return 'P';
		case 4:	return 'M';
		case 5:	return 'C';
		case 6:	return fIsLLS ? 'D' : 'X';
		case 7:	return fIsLLS ? 'X' : 'S';
		case 8:	return fIsLLS ? 'S' : '@';
		case 9:	return fIsLLS ? 'T' : '@';
		}

	return '@';
	}

BYTE CLaetusBarcodeDriver::GetDCFLetter(UINT uOffset, BOOL fIsLLS)
{
	switch( uOffset ) {

		case 0:	return 'L';
		case 1:	return fIsLLS ? 'B' : 'S';
		case 2:	return fIsLLS ? 'S' : 'M';
		case 3:	return fIsLLS ? 'V' : 'R';
		case 4:	return fIsLLS ? 'M' : 'Z';
		case 5:	return 'P';
		case 6:	return 'Q';
		case 7:	return 'N';
		case 8:	return 'O';
		case 9:	return fIsLLS ? 'D' : 'L';
		}

	return 'L';
	}

BYTE CLaetusBarcodeDriver::GetHOCLetter(UINT uOffset, BOOL fIsLLS)
{
	switch( uOffset ) {

		case  0:	return 'H';
		case  1:	return 'S';
		case  2:	return 'T';
		case  3:	return 'R';
		case  4:	return 'L';
		case  5:	return 'B';
		case  6:	return 'F';
		case  7:	return 'A';
		case  8:	return 'M';
		case  9:	return fIsLLS ? 'N' : 'H';
		case 10:	return fIsLLS ? 'E' : 'H';
		}

	return 'H';
	}

BYTE CLaetusBarcodeDriver::GetSOCLetter(UINT uOffset, BOOL fIsLLS)
{
	if( fIsLLS ) {

		switch( uOffset ) {

			case  0:	return  'A';
			case  1:	return  'W';
			case  2:	return  'B';
			case  3:	return  'X';
			case  4:	return  'G';
			case  5:	return  'V';
			case  6:	return  'L';
			case  7:	return  'O';
			case  8:	return  'M';
			case  9:	return  'N';
			case 10:	return  'P';
			case 11:	return  'Q';
			case 12:	return  'T';
			case 13:	return  'R';
			case 14:	return  'S';
			}

		return 0;
		}

	if( uOffset >= 4 && uOffset <= 12 ) return BYTE('A' + uOffset - 4);

	switch( uOffset ) {

		case  0:	return 'X';
		case  1:	return 'R';
		case  2:	return 'S';
		case  3:	return 'T';
		case 13:	return 'U';
		case 14:	return 'V';
		case 15:	return 'K';
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CLaetusBarcodeAddrDialog, CStdAddrDialog);
		
// Constructor

CLaetusBarcodeAddrDialog::CLaetusBarcodeAddrDialog(CLaetusBarcodeDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_pLDriver = &Driver;

	m_uDevice  = pConfig->GetDataAccess("Unit")->ReadInteger(pConfig);

	m_pConfig  = pConfig;

	SetName(fPart ? TEXT("PartLaetusBarcodeAddressDlg") : TEXT("LaetusBarcodeAddressDlg"));
	}

// Message Map

AfxMessageMap(CLaetusBarcodeAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CLaetusBarcodeAddrDialog)
	};

// Message Handlers

BOOL CLaetusBarcodeAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

//	GetDlgItem(2005).SetWindowText("Item");

	FindSpace();
	
	if( !m_fPart ) {

		SetCaption();

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddrAndInfo(Addr);

			return FALSE;
			}

		return TRUE;
		}
	else {
		ShowAddrAndInfo(Addr);

		if( GetDlgItem(2002).GetWindowText() != "" ) SetDlgFocus(2002);

		return FALSE;
		}
	}

// Notification Handlers

void CLaetusBarcodeAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CLaetusBarcodeAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = m_pSpace->m_uMinimum;

			Addr.a.m_Table  = m_pSpace->m_uTable;

			ShowAddrAndInfo(Addr);
			}

		else {
			if( EnableAddrEntry(!m_pSpace ?  0 : m_pSpace->m_uTable) ) {

				SetDlgFocus(2002);
				}
			}
		}
	else {
		m_pSpace = NULL;

		GetDlgItem(2001).SetWindowText("None");
		GetDlgItem(2002).SetWindowText("");

		GetDlgItem(2001).EnableWindow(TRUE);
		GetDlgItem(2002).EnableWindow(FALSE);

		ClearInfo();
		}
	}

// Command Handlers

BOOL CLaetusBarcodeAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CAddress Addr;

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		CError   Error(TRUE);
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables

void CLaetusBarcodeAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text = "";

	if( EnableAddrEntry(Addr.a.m_Table) ) {

		m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

		Text = Text.Mid(m_pSpace->m_Prefix.GetLength());
		}

	GetDlgItem(2002).SetWindowText(Text);
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);
	}

BOOL CLaetusBarcodeAddrDialog::AllowSpace(CSpace *pSpace)
{
	return pSpace && (pSpace->m_uTable != TFE || m_uDevice == LLS570);
	}

// Helpers
void CLaetusBarcodeAddrDialog::ShowInfo(void)
{
	GetDlgItem(2003).SetWindowText("Response Data:");
	GetDlgItem(2003).EnableWindow(TRUE);

	if( !m_pSpace ) {

		ClearInfo();

		return;
		}

	CString s[MAXINFO];
	CString sType = S_ACKNAK;

	UINT uCt      = 0;
	UINT uT       = m_pSpace->m_uTable;

	BOOL fLLS     = m_uDevice == LLS570;
	BOOL fCOC     = m_uDevice == COCAM7;

	switch( uT ) {

		case SOM:
			s[uCt++] = S_OPERSEL; 
			MS(s, uCt, S_1, S_READING);	uCt++;	// Reading Mode
			MS(s, uCt, S_3, S_PARAM);	uCt++;	// Parametrization Mode
			MS(s, uCt, S_4, S_OPERATING);	uCt++;	// Operating Data Mode
			MS(s, uCt, S_5, S_SELFTEST);	uCt++;	// Self test mode
			break;

		case LRS:
		case LES:
			if( uT == LES ) {

				s[uCt++] = S_RINGSELECT;
				}

			s[uCt++] = S_RESPFORMAT;
			s[uCt++] = S_ONESTATUS;
			s[uCt++] = S_SPACE;
			sType    = S_BYTE;
			break;

		case LRD:
		case LED:
			if( uT == LED ) {

				s[uCt++] = S_RINGSELECT;
				}

			s[uCt++] = S_RESPFORMAT;
			s[uCt++] = S_DATASTRING;
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case LXC:
			s[uCt++] = S_WRNONZERO;
			break;

		case SRG:
			s[uCt++] = S_SRG1;
			s[uCt++] = S_SRG2;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_SRG3;
			s[uCt++] = S_ACKNAK;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_SRG4;
			sType    = S_SRGNONE;
			break;

		case MCF:
			s[uCt++] = S_SELALPHA;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_MATCHA;
			s[uCt++] = S_MATCHB;
			s[uCt++] = S_MATCHC;
			s[uCt++] = S_MATCHD;
			s[uCt++] = S_MATCHF;
			s[uCt++] = S_MATCHG;
			s[uCt++] = S_MATCHI;
			s[uCt++] = S_MATCHJ;

			if( fLLS ) {

				s[uCt++] = S_MATCHR;
				s[uCt++] = S_MATCHT;
				}

			s[uCt++] = S_SPACE;
			s[uCt++] = S_RESPFORMAT;
			s[uCt++] = S_MATCH12;
			s[uCt++] = S_MATCH13;
			sType    = S_LONG;
			break;

		case MC1:
		case MC2:
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case CCF:
			s[uCt++] = S_SELALPHA;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_CODE0;
			s[uCt++] = S_CODEA;
			s[uCt++] = S_CODEC;
			
			if( fLLS ) s[uCt++] = S_CODED;

			s[uCt++] = S_CODEL;
			s[uCt++] = S_CODEM;
			s[uCt++] = S_CODEP;
			s[uCt++] = S_CODES;
			s[uCt++] = S_CODET;
			s[uCt++] = S_CODEX;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_CODECH1;
			s[uCt++] = S_CODECH2;
			sType    = S_LONG;
			break;

		case CCS:
		case CCL:
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case DCF:
			s[uCt++] = S_SELALPHA;
			s[uCt++] = S_SPACE;

			if( fLLS ) {

				s[uCt++] = S_DEVCFB;
				s[uCt++] = S_DEVCFD;
				}

			s[uCt++] = S_DEVCFL;
			s[uCt++] = S_DEVCFM;
			s[uCt++] = S_DEVCFN;
			s[uCt++] = S_DEVCFO;
			s[uCt++] = S_DEVCFP;
			s[uCt++] = S_DEVCFQ;

			if( fCOC ) s[uCt++] = S_DEVCFR;

			s[uCt++] = S_DEVCFS;

			if( fLLS ) s[uCt++] = S_DEVCFV;

			if( fCOC ) s[uCt++] = S_DEVCFZ;

			sType    = S_LONG;
			break;

		case OPC:
			s[uCt++] = S_OPERSEL;
			MS(s, uCt, S_1, S_OPCR1);	uCt++;	// Trigger Event (c)
			MS(s, uCt, S_2, S_OPCR2);	uCt++;	// Good Read (g)
			MS(s, uCt, S_3, S_OPCR3);	uCt++;	// No Read (x)
			MS(s, uCt, S_4, S_OPCR4);	uCt++;	// Matchcode 1 (m)
			MS(s, uCt, S_5, S_OPCR5);	uCt++;	// Matchcode 2 (n)
			MS(s, uCt, S_6, S_OPCR6);	uCt++;	// No match (y)
			s[uCt++] = S_SPACE;
			s[uCt++] = S_OPCWA;

			sType    = S_LONG;
			break;

		case HOC:
			s[uCt++] = S_SELALPHA;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_TFCA;
			s[uCt++] = S_TFCB;

			if( fLLS ) s[uCt++] = S_TFCE;

			s[uCt++] = S_TFCF;
			s[uCt++] = S_TFCH;
			s[uCt++] = S_TFCL;
			s[uCt++] = S_TFCM;

			if( fLLS ) s[uCt++] = S_TFCN;

			s[uCt++] = S_TFCR;
			s[uCt++] = S_TFCS;
			s[uCt++] = S_TFCT;

			s[uCt++] = S_SPACE;

			s[uCt]    = S_TFC0;

			if( fLLS ) s[uCt] += S_TFC1;

			s[(++uCt)++] = S_TFC2;

			sType        = S_LONG;
			break;

		case TFE:
		case TFH:
		case TFL:
		case TFM:
		case TFS:
		case TFT:
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case PE1:
			s[uCt++] = S_RESPFORMAT;
			s[uCt++] = S_PE1A;
			s[uCt++] = S_PE1B;
			s[uCt++] = S_PE1C;

			if( fLLS ) s[uCt++] = S_PE1D;

			s[uCt++] = S_SPACE;
			s[uCt++] = S_PEX1;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case PE2:
			s[uCt++] = S_RESPFORMAT;
			s[uCt++] = S_PE2;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_PEX1;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case PE3:
			s[uCt++] = S_RESPFORMAT;
			s[uCt++] = S_PE3A;
			s[uCt++] = S_PE3B;
			s[uCt++] = S_PE3C;
			s[uCt++] = S_PE3D;
			s[uCt++] = S_PE3E;
			s[uCt++] = S_PE3F;
			s[uCt++] = S_PE3G;
			s[uCt++] = S_PE3H;

			if( fLLS ) s[uCt++] = S_PE3I;

			s[uCt++] = S_SPACE;
			s[uCt++] = S_PEX1;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case PE4:
			s[uCt++] = S_RESPFORMAT;
			s[uCt++] = S_PE4A;
			s[uCt]   = S_PE4B;

			if( fLLS ) s[uCt] += " z";
			uCt++;

			s[uCt++] = S_SPACE;
			s[uCt++] = S_PEX1;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case PEX:
			s[uCt++] = S_PEX1;
			s[uCt++] = S_PEX2;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_MINUS1;
			s[uCt++] = S_PEX3;
			s[uCt++] = S_PEX4;
			sType    = S_LONG;
			break;

		case SOC:				
			s[uCt++] = S_SELALPHA;

			if( m_uDevice == LLS570 ) {

				s[uCt++] = S_SOCAL;
				s[uCt++] = S_SOCBL;
				s[uCt++] = S_SOCGL;
				s[uCt++] = S_SOCLL;
				s[uCt++] = S_SOCML;
				s[uCt++] = S_SOCNL;
				s[uCt++] = S_SOCOL;
				s[uCt++] = S_SOCPL;
				s[uCt++] = S_SOCQL;
				s[uCt++] = S_SOCRL;
				s[uCt++] = S_SOCSL;
				s[uCt++] = S_SOCT;
				s[uCt++] = S_SOCVL;
				s[uCt++] = S_SOCWL;
				s[uCt++] = S_SOCXL;
				}

			else {
				s[uCt++] = S_SOCAC;
				s[uCt++] = S_SOCIC;
				s[uCt++] = S_SOCKC;
				s[uCt++] = S_SOCRC;
				s[uCt++] = S_SOCSC;
				s[uCt++] = S_SOCT;
				s[uCt++] = S_SOCUC;
				s[uCt++] = S_SOCVC;
				s[uCt++] = S_SOCXC;
				}

			sType = S_LONG;
			break;

		case DST:
			s[uCt]    = S_WRNONZERO;
			s[uCt++] += S_DST;
			sType     = S_BYTE;
			break;

		case EEP:
			s[uCt++] = S_EEP1;
			s[uCt++] = S_EEP2;
			s[uCt++] = S_EEP3;
			break;

		case USR:
			s[uCt++] = S_USERDEF0;
			s[uCt++] = S_WRNONZERO;
			sType	 = "0";
			break;

		case USRC:
		case USRR:
			s[uCt++] = S_USERDEF1;
			s[uCt++] = S_USERDEF2;
			s[uCt++] = S_USERDEF3;
			s[uCt++] = S_USERDEF4;
			s[uCt++] = S_USERDEF5;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_USERDEF6;
			s[uCt++] = S_USERDEF7;
			s[uCt++] = S_SPACE;
			s[uCt++] = S_USERDEF10;
			s[uCt++] = S_USERDEF11;
			s[uCt++] = S_USERDEF12;
			s[uCt++] = S_USERDEF13;
			s[uCt++] = S_USERDEF14;
			s[uCt++] = S_USERDEF15;
			s[uCt++] = S_USERDEF16;
			s[uCt++] = S_STRINGTAG;
			sType    = S_STRING;
			break;

		case SCK:
			s[uCt++] = S_USESICK1;
			s[uCt++] = S_USESICK2;
			s[uCt++] = S_USESICK3;
			s[uCt++] = S_USESICK4;
			sType    = S_BIT;
			break;
		}

	for( UINT i = 0; i < MAXINFO; i++ ) {

		BOOL f = i < uCt;

		GetDlgItem(3001 + i).SetWindowText( f ? s[i] : "" );
		GetDlgItem(3001 + i).EnableWindow ( f ? TRUE : FALSE );
		}

	GetDlgItem(2004).SetWindowText( sType );
	GetDlgItem(2004).EnableWindow ( TRUE );
	}

void CLaetusBarcodeAddrDialog::ClearInfo(void)
{
	for( UINT i = 3001; i <= 3000+MAXINFO; i++ ) {

		GetDlgItem( i ).EnableWindow(FALSE);
		GetDlgItem( i ).SetWindowText("");
		}

	GetDlgItem(2004).SetWindowText( "" );
	GetDlgItem(2004).EnableWindow(FALSE);
	GetDlgItem(2003).EnableWindow(FALSE);
	}

BOOL CLaetusBarcodeAddrDialog::EnableAddrEntry(UINT uTable)
{
	BOOL fOk = FALSE;

	switch( uTable ) {

		case SOM:
		case LES:
		case LED:
		case MCF:
		case CCF:
		case DCF:
		case OPC:
		case HOC:
		case SOC:
			fOk = TRUE;
			break;
		}

	GetDlgItem(2002).EnableWindow(fOk);

	return fOk;
	}

void CLaetusBarcodeAddrDialog::ShowAddrAndInfo(CAddress Addr)
{
	ShowAddress(Addr);
	ShowInfo();
	}

// End of File
