
#include "intern.hpp"

#include "ctc2xxx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCTC2xxxSerialDeviceOptions, CUIItem);
				   
// Constructor

CCTC2xxxSerialDeviceOptions::CCTC2xxxSerialDeviceOptions(void)
{
	m_Protocol = 1;

	m_Drop = 0;
	}

// UI Managament

void CCTC2xxxSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag == "Protocol" ) {

		BOOL fAscii = pItem->GetDataAccess("Protocol")->ReadInteger(pItem);

		pWnd->EnableUI("Drop", fAscii);
		}
	}

// Download Support

BOOL CCTC2xxxSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Protocol));

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CCTC2xxxSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Protocol);
	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Master Driver
//

// Instantiator

ICommsDriver *	Create_CTC2xxxSerialDriver(void)
{
	return New CCTC2xxxSerialDriver;
	}

// Constructor

CCTC2xxxSerialDriver::CCTC2xxxSerialDriver(void)
{
	m_wID		= 0x4017;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "CTC";
			
	m_DriverName	= "2000 Series Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "2xxx";	

	AddSpaces();
	}

// Binding Control

UINT CCTC2xxxSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CCTC2xxxSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}


// Configuration

CLASS CCTC2xxxSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCTC2xxxSerialDeviceOptions);
	}

// Implementation     

void CCTC2xxxSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(9,	"R",	"Numeric Register",		10,	1,	65535,	addrLongAsLong));

	AddSpace(New CSpace(17, "F",	"Flag",				10,	1,	32,	addrBitAsBit));

	//AddSpace(New CSpace(15, "I",	"Input",			10,	1,	1024,	addrBitAsBit));

	//AddSpace(New CSpace(16, "IB",	"Input Bank",			10,	1,	1024,	addrBitAsByte));

	//AddSpace(New CSpace(21, "O",	"Output",			10,	1,	1024,	addrBitAsBit));

	//AddSpace(New CSpace(22, "OB", "Output Bank",			10,	1,	1024,	addBitAsByte));

	//AddSpace(New CSpace(29, "AI",	"Analog Input",			10,	1,	64,	addrWordAsWord));

	//AddSpace(New CSpace(31, "AO",	"Analog Output",		10,	1,	64,	addrWordAsWord));

	//AddSpace(New CSpace(23, "SP",	"Servo Position",		10,	1,	16,	addrLongAsLong));

	//AddSpace(New CSpace(47, "SE",	"Servo Error",			10,	1,	16,	addrLongAsLong));

	//AddSpace(New CSpace(27, "SI",	"Servo Auxilary Inputs",	10,	1,	16,	addrByteAsByte));
	
	AddSpace(New CSpace(addrNamed + 1, "STRT", "Start Controller",	10,	1,	0,	addrBitAsBit));

	AddSpace(New CSpace(addrNamed + 2, "STP",  "Stop Controller",	10,	2,	0,	addrBitAsBit));

	AddSpace(New CSpace(addrNamed + 3, "RST",  "Reset Controller",	10,	3,	0,	addrBitAsBit));
	}

// Address Management

BOOL CCTC2xxxSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CCTC2xxxAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CCTC2xxxSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	/*  When and if binary protocol is released, space adjustment will be necessary.  */
	
	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		return TRUE;
		}
	
	Error.Set( CString(IDS_DRIVER_ADDR_INVALID), 0 );

	return FALSE;
	}

BOOL CCTC2xxxSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	/*  When and if binary protocol is released, space adjustment will be necessary. */

	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr ) ) {

		return TRUE;
		}

	return FALSE;
	
	}

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCTC2xxxAddrDialog, CStdAddrDialog);
		
// Constructor

CCTC2xxxAddrDialog::CCTC2xxxAddrDialog(CCTC2xxxSerialDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_uMode = pConfig->GetDataAccess("Protocol")->ReadInteger(pConfig);
	}

// Overridables

BOOL CCTC2xxxAddrDialog::AllowSpace(CSpace *pSpace)
{
	if( m_uMode == 0 ) {

		if( pSpace->m_uTable >= addrNamed ) {

			return FALSE;
			}

		else {

			switch( pSpace->m_uTable ) {

				case 15:
				case 21:

					return FALSE;
				}
			}
		}

	else {
		switch( pSpace->m_uTable ) {

			case 16:
			case 22:

				return FALSE;
			}
		}

	return TRUE;
	}

// End of File
