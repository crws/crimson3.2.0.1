
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DUPMOD_HPP
	
#define	INCLUDE_DUPMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Dupline Modbus Read Offsets
//

#define	READ_OFFSET_OUTPUT	1280
#define	READ_OFFSET_INPUT	1536
#define	READ_OFFSET_ANALINK	256
#define	READ_OFFSET_COUNTER	512
#define	READ_OFFSET_MUX_IN	1040
#define READ_OFFSET_MUX_OUT	5136
#define READ_OFFSET_RESET_CTR	9152
#define READ_OFFSET_OUTPUT_W	0

#define READ_OFFSET_DIGIN	256		
#define READ_OFFSET_ANALIMIT	16384
#define READ_OFFSET_TIMESETS	24576

#define READ_OFFSET_CLOCK	8208	

#define MARK_MODBUS		0x8000

//////////////////////////////////////////////////////////////////////////
//
// Dupline Modbus Forward Declarations
//

class CSpaceDupline;

//////////////////////////////////////////////////////////////////////////
//
// Dupline Master Generator MODBUS Driver Options
//

class CDuplineModbusDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDuplineModbusDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Timeout;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Dupline Master Generator MODBUS Device Options
//

class CDuplineModbusDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDuplineModbusDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dupline Master Generator MODBUS Driver
//

class CDuplineModbusDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CDuplineModbusDriver(void);

		// Destructor
		//~CDuplineModbusDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		
		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		UINT	GetFormat(CSpaceDupline* pSpace);
		UINT	GetDuplinePrefix(CString Text, CSpace* pSpace, UINT uType);
		UINT	GetDuplineSuffix(CString Text, CSpace* pSpace);
		UINT	GetDuplineExtended(CString pText, CSpace* pSpace);
		UINT	GetDuplineExtended(UINT &uOffset, CSpace* pSpace);
	       	UINT	GetDuplinePrefix(UINT uOffset, CSpace* pSpace);
		UINT	GetDuplineSuffix(UINT uOffset, CSpace* pSpace, UINT uType=0);
		CString	GetDuplinePrefixText(UINT uFormat, UINT uIndex, CSpace* Space, UINT uType=0);
		CString	GetDuplineSuffixText(UINT uFormat, UINT uIndex, CSpace* Space);
		CString GetDuplineExtendedText(CSpace* pSpace, UINT uOffset, BOOL fDefault);
		UINT	FromDuplineToModbus(CSpace* pSpace, UINT uOffset, UINT uExtra, UINT uExtended, UINT uType);
		UINT	FromModbusToDupline(CSpace* pSpace, UINT uOffset, UINT uExtra, UINT uExtended, UINT uType);
		BOOL	IsModbus(UINT uOffset);
		BOOL	HasExtended(UINT uTable);
		UINT	GetExtendedElements(UINT uTable);
			      	
	protected:
		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Dupline Space Wrapper Class
//

class CSpaceDupline : public CSpace
{
	public:
		// Constructors

		CSpaceDupline(UINT uTag, CString p, CString c, UINT r, CString i, CString a, UINT n, UINT x, AddrType type, AddrType s);

		// Public Data

		CString	m_Minimum;
		CString	m_Maximum;
		
	};


//////////////////////////////////////////////////////////////////////////
//
// Dupline Address Selection Dialog
//

class CDuplineModbusDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDuplineModbusDialog(CDuplineModbusDriver &Driver, CAddress &Addr, BOOL fPart);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void	OnTypeChange(UINT uID, CWnd &Wnd);

		
	protected:
		// Overridables
		BOOL	AllowType(UINT uType);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Helpers
		void	LoadAddressPrefixList(UINT uOffset);
		void	LoadAddressSuffixList(UINT uOffset);
		void	ClearComboList(UINT uID);
		void	LoadAddressExtendedList(UINT uOffset);

	};

// End of File

#endif
