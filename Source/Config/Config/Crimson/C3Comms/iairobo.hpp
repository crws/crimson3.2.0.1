
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IAIROBO_HPP
	
#define	INCLUDE_IAIROBO_HPP

class CIairoboDeviceOptions;
class CIairoboDriver;

#define	LL	addrLongAsLong


//////////////////////////////////////////////////////////////////////////
//
// IAI Robocylinder Device Options
//

class CIairoboDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIairoboDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IAI Robocylinder Comms Driver
//

class CIairoboDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIairoboDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Device Options
		CLASS	GetDeviceConfig(void);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
