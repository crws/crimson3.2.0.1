
#include "intern.hpp"

#include "s7tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 200 via ISO TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CS7IsoTCPDeviceOptions, CUIItem);

// Constructor

CS7IsoTCPDeviceOptions::CS7IsoTCPDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));
	
	m_Addr2   = DWORD(0);

	m_Port	  = 102;

	m_Keep    = TRUE;

	m_Ping    = FALSE;

	m_Time1   = 5000;

	m_Time2   = 2500;

	m_Time3   = 200; 

	m_Type    = 1;

	m_Conn	  = "10";

	m_uConn   = 0x10;

	m_Slot    = 0;

	m_Rack    = 0;

	m_Client  = "10";

	m_uClient = 0x10;

	m_BlkOff  = 0;
	
	m_TSAP    = 0;

	m_STsap   = 0x1000;

	m_CTsap   = 0x1000;

	m_STsap2  = 0x0000;

	m_CTsap2  = 0x0000;
		
	}

// Persistance

void CS7IsoTCPDeviceOptions::PostLoad(void)
{
	m_uConn   = tstrtoul(m_Conn,   NULL, 16);

	m_uClient = tstrtoul(m_Client, NULL, 16);
	}

// UI Managament

void CS7IsoTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
				   
		if( Tag.IsEmpty() || Tag == "Type" )  {

			pWnd->EnableUI("Type", FALSE);
			}  

		if( Tag == "Conn" ) {

			SetHexadecimal(pWnd, Tag, m_Conn, m_uConn);

			pWnd->UpdateUI("Conn");
			}

		if( Tag == "Client" ) {

			SetHexadecimal(pWnd, Tag, m_Client, m_uClient);

			pWnd->UpdateUI("Client");
			}   
				
		if( Tag.IsEmpty() ) {

			pWnd->EnableUI("Rack", FALSE);
			}
		}    
	}

// Download Support	     

BOOL CS7IsoTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_Type));
	Init.AddByte(BYTE(m_uConn));
	Init.AddByte(BYTE(m_Slot));
	Init.AddByte(BYTE(m_Rack));
	Init.AddByte(BYTE(m_uClient));
	Init.AddWord(WORD(m_BlkOff));
	Init.AddLong(LONG(m_Addr2));
	Init.AddByte(BYTE(m_TSAP));
	Init.AddWord(WORD(m_STsap));
	Init.AddWord(WORD(m_CTsap));
	Init.AddWord(WORD(m_STsap2));
	Init.AddWord(WORD(m_CTsap2));
			
	return TRUE;
	}

// Meta Data Creation

void CS7IsoTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Type);
	Meta_AddString (Conn);
	Meta_AddInteger(Slot);
	Meta_AddInteger(Rack);
	Meta_AddString (Client);
	Meta_AddInteger(BlkOff);
	Meta_AddInteger(Addr2);
	Meta_AddInteger(TSAP);
	Meta_AddInteger(STsap);
	Meta_AddInteger(CTsap);
	Meta_AddInteger(STsap2);
	Meta_AddInteger(CTsap2);
	
	}

// Helper

void CS7IsoTCPDeviceOptions::SetHexadecimal(CUIViewWnd *pWnd, CString Tag, CString &Text, UINT &uTarget)
{
	UINT uLength = Text.GetLength();

	PSTR pText   = "10";

	if ( uLength != 2 ) {

		pWnd->Error( CPrintf( IDS_DRIVER_ADDRRANGE,
				      TEXT("0x10"),
				      TEXT("0xFE")
				      ));

		m_Conn = pText;

		return;
		}
	
	Text = Text.Left(uLength);

	Text = Text.ToUpper();

	for( UINT u = 0; u < uLength; u++ ) {

		if( u == 0 && ( Text[u] < '1' || Text[u] > 'F' ) ) {

			pWnd->Error( CPrintf( IDS_DRIVER_ADDRRANGE,
					      TEXT("0x10"),
					      TEXT("0xFE")
					      ));

			m_Conn = pText;

			return;
			}
		
		if( u == 1 && ( Text[u] < '0' || Text[u] > 'E' ) ) {

			pWnd->Error( CPrintf( IDS_DRIVER_ADDRRANGE,
					      TEXT("0x10"),
					      TEXT("0xFE")
					      ));

			m_Conn = pText;

			return;
			}
		}
		
	uTarget = tstrtoul(Text, NULL, 16);
	}

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 200 via ISO TCP/IP Master Driver
//

// Instantiator

ICommsDriver *	Create_S7ISOMasterTCPDriver(void)
{
	return New CS7ISOMasterTCPDriver;
	}

// Constructor

CS7ISOMasterTCPDriver::CS7ISOMasterTCPDriver(void)
{
	m_wID		= 0x351B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Siemens";
	
	m_DriverName	= "S7 CP243 via ISO TCP/IP Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "S7 ISO";

	C3_PASSED();
	}

// Binding

UINT CS7ISOMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CS7ISOMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CS7ISOMasterTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CS7ISOMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CS7IsoTCPDeviceOptions);
	}




// End of File
