
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3LAYOUT_HXX
	
#define	INCLUDE_C3LAYOUT_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcwin.hxx>

// End of File

#endif
