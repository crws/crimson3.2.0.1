
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP

#define	INCLUDE_TESTING_HPP
	
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTestApp;
class CTestWnd;
class CTestData;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CTestApp : public CThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTestApp(void);

		// Destructor
		~CTestApp(void);

	protected:
		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

class CTestWnd : public CMenuWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestWnd(void);

		// Destructor
		~CTestWnd(void);

	protected:
		// Data Members
		CAccelerator m_Accel;
		CEditCtrl *  m_pEdit;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnAccelerator(MSG &Msg);
		void OnPostCreate(void);
		void OnPaint(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Wnd);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

// End of File

#endif
