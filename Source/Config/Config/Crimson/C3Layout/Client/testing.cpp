
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Runtime Class

AfxImplementRuntimeClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestWnd;

	CPoint Pos = CPoint(280, 180);

	CSize Size = CSize (586, 400);

	CRect Rect = CRect (Pos, Size);

	pWnd->Create( L"Test Application",
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      Rect, AfxNull(CWnd), CMenu(L"Test"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_EXIT ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestWnd, CMenuWnd);

// Constructor

CTestWnd::CTestWnd(void)
{
	m_Accel.Create(L"Test");
	}

// Destructor

CTestWnd::~CTestWnd(void)
{
	}

// Message Map

AfxMessageMap(CTestWnd, CMenuWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestWnd)
	};

// Message Handlers

BOOL CTestWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

void CTestWnd::OnPostCreate(void)
{
	}

void CTestWnd::OnPaint(void)
{
	CPaintDC DC(*this);

	UINT n;

	CRect          Rect  = GetClientRect() - 3;

	CLayFormation *pForm = New CLayFormEqual(4);

	CLayItem      *pList[16];

	srand(0);

	for( n = 0; n < elements(pList); n++ ) {

		CLayItem *pText = New CLayItemText(CString('X', 10 + rand() % 10));

		CLayItem *pItem = New CLayFormPad(pText, horzLeft | vertCenter);

		pForm->AddItem(pItem);

		pList[n] = pText;
		}

	DC.FillRect(Rect, afxBrush(WHITE));

	try {
		pForm->Prepare(DC);

		pForm->SetRect(Rect);

		pForm->SetOffset(CSize(10, 10));

		for( n = 0; n < elements(pList); n++ ) {

			CRect Rect = pList[n]->GetRect();

			DC.FrameRect(Rect, afxBrush(RED));

			DC.FillRect(Rect-1, afxBrush(BLUE));
			}
		}

	catch(CUserException const &) {

		}

	delete pForm;
	}

BOOL CTestWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = CWnd::GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	DC.FrameRect(Rect, afxBrush(WHITE));

	Rect -= 1;

	DC.FrameRect(Rect, afxBrush(WHITE));

	return TRUE;
	}

void CTestWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		Invalidate(FALSE);
		}
	}

void CTestWnd::OnSetFocus(CWnd &Wnd)
{
	}

// Command Handlers

BOOL CTestWnd::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_NEW ) {

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_NEW ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

// End of File
