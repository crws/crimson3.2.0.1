
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Overlay
//

// Constructor

CLayFormOverlay::CLayFormOverlay(void)
{
	}

// Overridables

void CLayFormOverlay::OnPrepare(CDC &DC)
{
	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		pItem->Prepare(DC);

		CSize Min = pItem->GetMinSize();

		CSize Max = pItem->GetMaxSize();

		m_MinSize.cx = Max(m_MinSize.cx, Min.cx);

		m_MinSize.cy = Max(m_MinSize.cy, Min.cy);

		m_MaxSize.cx = Max(m_MaxSize.cx, Max.cx);

		m_MaxSize.cy = Max(m_MaxSize.cy, Max.cy);
		}
	}

void CLayFormOverlay::OnSetRect(void)
{
	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		pItem->SetRect(m_Rect);
		}
	}

// End of File
