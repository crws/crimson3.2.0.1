
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Equal Column Grid
//

// Constructor

CLayFormEqual::CLayFormEqual(UINT uCols)
{
	m_uCols = uCols;
	}

// Overridables

void CLayFormEqual::OnPrepare(CDC &DC)
{
	ClearData();

	FindRows();

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		pItem->Prepare(DC);
		}

	for( UINT r = 0; r < m_uRows; r++ ) {

		int nMin = GetRowMinSize(r);

		int nMax = GetRowMaxSize(r);

		m_MinSize.cy += nMin;

		m_MaxSize.cy += nMax;
		}

	for( UINT c = 0; c < m_uCols; c++ ) {

		int nMin = GetColMinSize(c);

		MakeMax(m_ColSize[c % 2], nMin);
		}

	m_MinSize.cx = (m_ColSize[0] + m_ColSize[1]) * (m_uCols / 2);

	m_MaxSize.cx = m_MinSize.cx;
	}

void CLayFormEqual::OnSetRect(void)
{
	int nRowSpace = m_Rect.GetHeight();

	RowSetMins(nRowSpace);
	
	while( RowDealOut(nRowSpace) );

	CPoint Pos;

	Pos.x = m_Rect.left;

	for( UINT c = 0; c < m_uCols; c++ ) {

		Pos.y = m_Rect.top;

		for( UINT r = 0; r < m_uRows; r++ ) {

			CLayItem *pItem = GetItem(r, c);

			CSize Size(m_ColSize[c % 2], m_RowSize[r]);

			pItem->SetRect(CRect(Pos, Size));

			Pos.y += m_RowSize[r];
			}

		Pos.x += m_ColSize[c % 2];
		}
	}

// Item Location

CLayItem * CLayFormEqual::GetItem(UINT uRow, UINT uCol)
{
	CLayItem *pItem = m_List[uCol + uRow * m_uCols];

	AfxAssert(pItem);

	return pItem;
	}

// Size Helpers

int CLayFormEqual::GetColMinSize(UINT uCol)
{
	int nMin = 0;

	for( UINT r = 0; r < m_uRows; r++ ) {

		CLayItem *pItem = GetItem(r, uCol);

		int nSize = pItem->GetMinSize().cx;

		if( uCol < m_uCols - 1 ) {

			if( GetItem(r, uCol + 1)->GetMinSize().cx == 0 ) {

				nSize -= GetColMinSize(uCol + 1);
				}
			}

		MakeMax(nMin, nSize);
		}

	return nMin;
	}

int CLayFormEqual::GetColMaxSize(UINT uCol)
{
	int nMax = 0;

	for( UINT r = 0; r < m_uRows; r++ ) {

		CLayItem *pItem = GetItem(r, uCol);

		int nSize = pItem->GetMaxSize().cx;

		MakeMax(nMax, nSize);
		}

	return nMax;
	}

int CLayFormEqual::GetRowMinSize(UINT uRow)
{
	int nMin = 0;

	for( UINT c = 0; c < m_uCols; c++ ) {

		CLayItem *pItem = GetItem(uRow, c);

		int nSize = pItem->GetMinSize().cy;

		MakeMax(nMin, nSize);
		}

	return nMin;
	}

int CLayFormEqual::GetRowMaxSize(UINT uRow)
{
	int nMax = 0;

	for( UINT c = 0; c < m_uCols; c++ ) {

		CLayItem *pItem = GetItem(uRow, c);

		int nSize = pItem->GetMaxSize().cy;

		MakeMax(nMax, nSize);
		}

	return nMax;
	}

// Implementation

void CLayFormEqual::ClearData(void)
{
	m_MinSize = CSize();

	m_MaxSize = CSize();

	m_RowSize.Empty();

	m_ColSize[0] = 0;

	m_ColSize[1] = 0;
	}

void CLayFormEqual::FindRows(void)
{
	UINT uCount = m_List.GetCount();

	m_uRows = uCount / m_uCols;

	AfxAssert(m_uRows * m_uCols == uCount);
	}

void CLayFormEqual::RowSetMins(int &nSpace)
{
	for( UINT r = 0; r < m_uRows; r++ ) {

		int nMin = GetRowMinSize(r);

		m_RowSize.Append(nMin);

		nSpace -= nMin;
		}
	}

BOOL CLayFormEqual::RowDealOut(int &nSpace)
{
	int nEdit = 0;

	if( nSpace > 0 ) {

		int nItem = 0;

		int nEach = 0;

		for( UINT p = 0; p < 2; p++ ) {

			for( UINT r = 0; r < m_uRows; r++ ) {

				int nSize = m_RowSize[r];

				int nMax  = GetRowMaxSize(r);

				if( nSize < nMax ) {

					if( p == 0 ) {

						nItem++;
						}
					else {
						int nDeal = Min(nEach, nMax - nSize);

						nSize  += nDeal;

						nSpace -= nDeal;

						m_RowSize.SetAt(r, nSize);

						nEdit++;
						}
					}
				}

			if( p == 0 ) {

				if( nItem && (nEach = nSpace / nItem) ) {

					continue;
					}

				break;
				}
			}
		}

	return nEdit ? TRUE : FALSE;
	}

// End of File
