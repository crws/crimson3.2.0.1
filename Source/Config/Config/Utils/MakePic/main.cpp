
#include "intern.hpp"

#include "palette.hpp"

#include "rle8.hpp"

// Color Formats
enum
{
	colorPal256,
	colorRGB555,
	colorRGB565,
	colorBGR565,
	colorRGB888
	};

// Static Data

static	int		m_nArg		= 0;

static	PTXT *		m_pArg		= NULL;

static	FILE *		m_pFile1	= NULL;

static	FILE *		m_pFile2	= NULL;

static	BOOL		m_fBanner	= FALSE;

static	PBYTE		m_pData		= NULL;

static	BITMAPINFO *	m_pInfo		= NULL;

static	SIZE		m_Size		= { 0, 0 };

static	BOOL		m_fRLE		= TRUE;

static	BOOL		m_fTrans	= TRUE;

static	UINT		m_uFormat	= colorPal256;

// Prototypes

global	void	main(int inArg, char *pArg[]);
static	void	ParseLine(void);
static	void	AppBanner(void);
static	void	Error(PCTXT pText, ...);
static	void	ShowHelp(void);
static	void	LoadInput(void);
static	void	RenderNormBitmap(void);
static	void	RenderWideBitmap(UINT uFormat);
static	void	RenderBitmapRGB888(void);
static	BYTE	FindClosest(COLORREF Color);
static	void	GetColor(BYTE n, COLORREF &Color);
static	UINT	GetColorCount(void);
static	BOOL	IsTransparent(BYTE r, BYTE g, BYTE b, BOOL fIs8bit);
static	void	WriteByte(BYTE b);
static	void	WriteWord(WORD w);
static	void	WriteLong(DWORD d);

// Code

global	void	main(int nArg, char *pArg[])
{
	m_nArg = nArg;

	m_pArg = pArg;

	if( nArg == 1 )  {

		ShowHelp();
		}

	ParseLine();

	LoadInput();
	
	switch( m_uFormat ) {

		case colorPal256:

			RenderNormBitmap();

			break;

		case colorRGB555:
		case colorRGB565:

			RenderWideBitmap(m_uFormat);

			break;

		case colorRGB888:

			RenderBitmapRGB888();

			break;
		}

	exit(0);
	}

static	void	AppBanner(void)
{
	if( !m_fBanner ) {

		printf("Bitmap File Converter  Version 1.11\n");

		printf("(c) 1996-2014 Red Lion Controls Inc\n\n");

		m_fBanner = TRUE;
		}
	}

static	void	ParseLine(void)
{
	while( --m_nArg ) {
	
		PTXT pArg = * ++ m_pArg;

		if( pArg[0] == '-' || pArg[0] == '/' ) {
				
			PTXT pSwitch = pArg + 1;

			if( !_stricmp(pSwitch, "nocomp") ) {

				m_fRLE = FALSE;

				continue;
				}
		
			if( !_stricmp(pSwitch, "wide") || !_stricmp(pSwitch, "rgb555") || !_stricmp(pSwitch, "g3v2") ) {

				m_uFormat = colorRGB555;

				continue;
				}
		
			if( !_stricmp(pSwitch, "opaque") ) {

				m_fTrans = FALSE;

				continue;
				}

			if( !_stricmp(pSwitch, "rgb888") || !_stricmp(pSwitch, "graphite") ) {

				m_uFormat = colorRGB888;

				continue;
				}

			if( !_stricmp(pSwitch, "rgb565") || !_stricmp(pSwitch, "ptv") ) {

				m_uFormat = colorRGB565;

				continue;
				}
		
			Error("unknown switch '%s'", pSwitch);
			}

		if( !m_pFile1 ) {

			m_pFile1 = fopen(pArg, "rb");

			if( !m_pFile1 ) {

				Error("cannot open input file '%s'", pArg);
				}
			
			continue;
			}
			
		if( !m_pFile2 ) {

			char sName[32];

			sprintf(sName, "pic%3.3u.g3p", atoi(pArg));

			m_pFile2 = fopen(sName, "wb");

			if( !m_pFile2 ) {

				Error("cannot open output file '%s'", sName);
				}
			
			continue;
			}
			
		Error("too many filenames");
		}

	if( !m_pFile1 ) {
		
		Error("input file must be specified");
		}

	if( !m_pFile2 ) {

		Error("output file number must be specified (0-999)");
		}
	}

static	void	Error(PCTXT pText, ...)
{
	AppBanner();
	
	printf("makepic: ");
	
	vprintf(pText, PTXT(&pText+1));
	
	printf("\n");

	exit(2);
	}
	
static	void	ShowHelp(void)
{
	AppBanner();
	
	printf("usage: makepic {switches} <input-file> <picture-number>\n");
	printf("\n");
	printf("  Supported switches are...\n");
	printf("\n");
	printf("    -nocomp     Do not compress bitmap\n");
	printf("    -opaque	Do not support transparency\n");
	printf("    -wide	Use RGB555 16-bit color values\n");
	printf("    -G3v2	Alias for -wide. Format used for v2 G3 displays.\n");
	printf("    -rgb555	Alias for -wide\n");
	printf("    -rgb888	Use RGB888 color values.\n");
	printf("    -graphite	Alias for -rgb888. Format used for Graphite displays.\n");
	printf("    -rgb565	Use RGB565 16-bit color values\n");
	printf("    -ptv	Alias for -rgb565. Format used for ProducTVity Stations.\n");
	printf("\n");
	printf("To produce 256 color images, do not specify a color switch.\n");
	printf("Input files should be Windows bitmaps. Picture\n");
	printf("number should be an integer between 0 and 999.\n");
	
	exit(1);
	}

static	void	LoadInput(void)
{
	BITMAPFILEHEADER Head;

	memset(&Head, 0, sizeof(Head));

	fread (&Head, 1, sizeof(Head), m_pFile1);

	if( Head.bfType == 'MB' ) {

		UINT uBase = sizeof(Head);

		fseek(m_pFile1, 0, SEEK_END);

		UINT uSize = ftell(m_pFile1) - uBase;

		m_pData = new BYTE [ uSize ];

		fseek(m_pFile1, uBase, SEEK_SET);

		fread(m_pData, 1, uSize, m_pFile1);

		m_pInfo   = (BITMAPINFO *) m_pData;

		m_Size.cx = abs(m_pInfo->bmiHeader.biWidth);

		m_Size.cy = abs(m_pInfo->bmiHeader.biHeight);

		return;
		}

	Error("invalid input format");
	}

static	void	RenderNormBitmap(void)
{
	HDC         hDC   = GetDC(NULL);

	PCBYTE      pBits = PCBYTE(m_pInfo) + m_pInfo->bmiHeader.biSize + 4 * GetColorCount();

	HBITMAP     hMap  = CreateDIBitmap( hDC,
					    &m_pInfo->bmiHeader,
					    CBM_INIT,
					    pBits,
					    m_pInfo,
					    DIB_RGB_COLORS
					    );

	UINT        uStep = ((m_Size.cx + 3) & ~3);

	UINT        uSize = uStep * m_Size.cy;

	UINT        uInfo = sizeof(BITMAPINFO) + 1020;

	BITMAPINFO *pInfo = (BITMAPINFO *) new BYTE [ uInfo ];

	memset(pInfo, 0, uInfo);

	pInfo->bmiHeader.biSize		 = sizeof(pInfo->bmiHeader);
	pInfo->bmiHeader.biWidth	 = uStep;
	pInfo->bmiHeader.biHeight	 = -m_Size.cy;
	pInfo->bmiHeader.biPlanes	 = 1;
	pInfo->bmiHeader.biBitCount	 = 8;
	pInfo->bmiHeader.biCompression	 = BI_RGB;
	pInfo->bmiHeader.biSizeImage	 = 0;
	pInfo->bmiHeader.biXPelsPerMeter = 0;
	pInfo->bmiHeader.biYPelsPerMeter = 0;
	pInfo->bmiHeader.biClrUsed	 = 256;
	pInfo->bmiHeader.biClrImportant	 = 0;

	PBYTE pData = new BYTE [ uSize ];

	BYTE  bLook[256];

	GetDIBits(hDC, hMap, 0, m_Size.cy, pData, pInfo, DIB_RGB_COLORS);

	for( UINT n = 0; n < 256; n++ ) {

		BYTE r = pInfo->bmiColors[n].rgbRed;
		BYTE g = pInfo->bmiColors[n].rgbGreen;
		BYTE b = pInfo->bmiColors[n].rgbBlue;

		bLook[n] = FindClosest(RGB(r,g,b));
		}

	for( UINT p = 0; p < uSize; p++ ) {

		pData[p] = bLook[pData[p]];
		}

	WriteByte('G');
	WriteByte('3');
	WriteByte('B');
	WriteByte('M');
	WriteByte('P');

	WriteByte(BYTE((m_fRLE ? 0x01 : 0) | (m_fTrans ? 0x02 : 0)));

	WriteByte(LOBYTE(uStep));
	WriteByte(HIBYTE(uStep));

	WriteByte(LOBYTE(m_Size.cx));
	WriteByte(HIBYTE(m_Size.cx));

	WriteByte(LOBYTE(m_Size.cy));
	WriteByte(HIBYTE(m_Size.cy));

	if( m_fRLE ) {
	
		PCBYTE pScan = pData;

		PBYTE  pComp = new BYTE [ uSize * 2 ];

		UINT   uComp = 0;

		for( int r = 1; r <= m_Size.cy; r++ ) {

			UINT uLine = MakeRLE8(pComp + uComp, pScan, m_Size.cx, r == 1, r == m_Size.cy);

			uComp += uLine;

			pScan += uStep;
			}

		fwrite(pComp, sizeof(BYTE), uComp, m_pFile2);

		delete [] pComp;
		}
	else
		fwrite(pData, sizeof(BYTE), uSize, m_pFile2);

	delete [] pData;

	delete [] pInfo;

	ReleaseDC(NULL, hDC);

	DeleteObject(hMap);
	}

static	void	RenderWideBitmap(UINT uFormat)
{
	HDC         hDC   = GetDC(NULL);

	PCBYTE      pBits = PCBYTE(m_pInfo) + m_pInfo->bmiHeader.biSize + 4 * GetColorCount();

	HBITMAP     hMap  = CreateDIBitmap( hDC,
					    &m_pInfo->bmiHeader,
					    CBM_INIT,
					    pBits,
					    m_pInfo,
					    DIB_RGB_COLORS
					    );

	UINT uInfo	= sizeof(BITMAPINFO);

	UINT uStride	= (2 * m_Size.cx + 3) & ~3;

	UINT uSize	= uStride * m_Size.cy;

	UINT uData	= m_Size.cx * m_Size.cy;

	BITMAPINFO *pInfo = (BITMAPINFO *) new BYTE [ uInfo ];

	memset(pInfo, 0, uInfo);

	pInfo->bmiHeader.biSize		 = sizeof(pInfo->bmiHeader);
	pInfo->bmiHeader.biWidth	 = +m_Size.cx;
	pInfo->bmiHeader.biHeight	 = -m_Size.cy;
	pInfo->bmiHeader.biPlanes	 = 1;
	pInfo->bmiHeader.biBitCount	 = 32;
	pInfo->bmiHeader.biCompression	 = BI_RGB;
	pInfo->bmiHeader.biSizeImage	 = 0;
	pInfo->bmiHeader.biXPelsPerMeter = 0;
	pInfo->bmiHeader.biYPelsPerMeter = 0;
	pInfo->bmiHeader.biClrUsed	 = 0;
	pInfo->bmiHeader.biClrImportant	 = 0;

	PDWORD pData = new DWORD [ uSize ];

	GetDIBits(hDC, hMap, 0, m_Size.cy, pData, pInfo, DIB_RGB_COLORS);

	BOOL fIs555 = (uFormat == colorRGB555);

	WriteByte('G');
	WriteByte('3');
	char Type = fIs555 ? 'W' : 'V';
	WriteByte(Type);
	WriteByte('M');
	WriteByte('P');

	WriteByte(BYTE((m_fRLE ? 0x01 : 0) | (m_fTrans ? 0x02 : 0)));

	WriteByte(LOBYTE(uStride));
	WriteByte(HIBYTE(uStride));

	WriteByte(LOBYTE(m_Size.cx));
	WriteByte(HIBYTE(m_Size.cx));

	WriteByte(LOBYTE(m_Size.cy));
	WriteByte(HIBYTE(m_Size.cy));

	int x = 0, y = 0;

	for( UINT n = 0; n < uData; n++ ) {

		DWORD rgb = pData[n];

		if( ++x == m_Size.cx ) { 

			x = 0;

			y++;

			UINT uPad = (2 * m_Size.cx) & 3;

			while( uPad-- ) {

				WriteByte(0);
				}
			}

		BYTE r = LOBYTE(LOWORD(rgb)) >> 3;
		BYTE g = HIBYTE(LOWORD(rgb)) >> 3;
		BYTE b = LOBYTE(HIWORD(rgb)) >> 3;

		WORD w = 0;
		
		if( fIs555 )
		
			w = WORD(r | (g << 5) | (b << 10));
		else
			w = WORD(r | (g << 6) | (b << 11));

		if( m_fTrans ) {

			if( IsTransparent(r, g, b, FALSE) ) {

				w = 0xFFFF;
				}
			}

		WriteWord(w);
		}

	delete [] pData;

	delete [] pInfo;

	ReleaseDC(NULL, hDC);

	DeleteObject(hMap);
	}

static void RenderBitmapRGB888(void)
{
	HDC         hDC   = GetDC(NULL);

	PCBYTE      pBits = PCBYTE(m_pInfo) + m_pInfo->bmiHeader.biSize + 4 * GetColorCount();

	HBITMAP     hMap  = CreateDIBitmap( hDC,
					    &m_pInfo->bmiHeader,
					    CBM_INIT,
					    pBits,
					    m_pInfo,
					    DIB_RGB_COLORS
					    );

	UINT        uInfo = sizeof(BITMAPINFO);

	UINT uStride	= 4 * m_Size.cx;

	UINT uSize	= m_Size.cx * m_Size.cy;

	BITMAPINFO *pInfo = (BITMAPINFO *) new BYTE [ uInfo ];

	memset(pInfo, 0, uInfo);

	pInfo->bmiHeader.biSize		 = sizeof(pInfo->bmiHeader);
	pInfo->bmiHeader.biWidth	 = +m_Size.cx;
	pInfo->bmiHeader.biHeight	 = -m_Size.cy;
	pInfo->bmiHeader.biPlanes	 = 1;
	pInfo->bmiHeader.biBitCount	 = 32;
	pInfo->bmiHeader.biCompression	 = BI_RGB;
	pInfo->bmiHeader.biSizeImage	 = 0;
	pInfo->bmiHeader.biXPelsPerMeter = 0;
	pInfo->bmiHeader.biYPelsPerMeter = 0;
	pInfo->bmiHeader.biClrUsed	 = 0;
	pInfo->bmiHeader.biClrImportant	 = 0;

	PDWORD pData = new DWORD [ uSize ];

	GetDIBits(hDC, hMap, 0, m_Size.cy, pData, pInfo, DIB_RGB_COLORS);

	WriteByte('G');
	WriteByte('3');
	WriteByte('G');
	WriteByte('M');
	WriteByte('P');

	WriteByte(BYTE((m_fRLE ? 0x01 : 0) | (m_fTrans ? 0x02 : 0)));

	WriteByte(LOBYTE(uStride));
	WriteByte(HIBYTE(uStride));

	WriteByte(LOBYTE(m_Size.cx));
	WriteByte(HIBYTE(m_Size.cx));

	WriteByte(LOBYTE(m_Size.cy));
	WriteByte(HIBYTE(m_Size.cy));

	int x = 0, y = 0;

	for( UINT n = 0; n < uSize; n++ ) {

		DWORD rgb = pData[n];

		if( ++x == m_Size.cx ) { 

			x = 0;

			y++;
			}

		BYTE r = LOBYTE(LOWORD(rgb));
		BYTE g = HIBYTE(LOWORD(rgb));
		BYTE b = LOBYTE(HIWORD(rgb));

		DWORD d = 0xFF000000 | (b << 16) | (g << 8) | r;

		if( m_fTrans ) {

			if( IsTransparent(r, g, b, TRUE) ) {

				d = 0x00FFFFFF;
				}
			}

		WriteLong(d);
		}

	delete [] pData;

	delete [] pInfo;

	ReleaseDC(NULL, hDC);

	DeleteObject(hMap);
	}

static	BYTE	FindClosest(COLORREF Color)
{
	int m = 100000;

	int i = 0;

	if( Color == RGB(0xFF, 0, 0xFF) ) {

		i = 255;
		}
	else {
		for( int n = 0; n < 256; n++ ) {

			COLORREF Check;

			GetColor(BYTE(n), Check);

			if( Color == Check ) {

				i = n;

				break;
				}
			else {
				int dr = abs(int(GetRValue(Color)) - int(GetRValue(Check)));
				int dg = abs(int(GetGValue(Color)) - int(GetGValue(Check)));
				int db = abs(int(GetBValue(Color)) - int(GetBValue(Check)));

				int d  = dr*dr + dg*dg + db*db;

				if( d < m ) {

					m = d;

					i = n;
					}
				}
			}
		}

	return BYTE(i);
	}

static	void	GetColor(BYTE n, COLORREF &Color)
{
	BYTE r = StdPalette[3*n+0];
	BYTE g = StdPalette[3*n+1];
	BYTE b = StdPalette[3*n+2];

	Color = RGB(r,g,b);
	}

static	UINT	GetColorCount(void)
{
	switch( m_pInfo->bmiHeader.biBitCount ) {

		case 1:
			return 2;

		case 4:
			return 16;

		case 24:
			return 0;

		case 32:
			return 0;
		}

	if( m_pInfo->bmiHeader.biClrUsed ) {

		return m_pInfo->bmiHeader.biClrUsed;
		}
	
	return 1 << m_pInfo->bmiHeader.biBitCount;
	}


static	BOOL	IsTransparent(BYTE r, BYTE g, BYTE b, BOOL fIs8bit)
{
	BYTE rMagic = fIs8bit ? 0xFF : 0x1F;
	BYTE gMagic = 0x00;
	BYTE bMagic = fIs8bit ? 0xFF : 0x1F;

	return (r == rMagic) && (g == gMagic) && (b == bMagic);
	}

static	void	WriteByte(BYTE b)
{
	fwrite(&b, 1, 1, m_pFile2);
	}

static	void	WriteWord(WORD w)
{
	fwrite(&w, 1, 2, m_pFile2);
	}

static	void	WriteLong(DWORD d)
{
	fwrite(&d, 1, 4, m_pFile2);
	}

// End of File
