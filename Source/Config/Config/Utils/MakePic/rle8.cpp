
//////////////////////////////////////////////////////////////////////////
//
// G3 Run Length Compression Utility
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

#include "rle8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Windows RLE8 Compressor
//

// Constants

static UINT const escEscape  = 0;
static UINT const escEndLine = 0;
static UINT const escEndData = 1;
static UINT const escOffset  = 2;
static UINT const minMix     = 3;
static UINT const maxMix     = 254;
static UINT const minRun     = 3;
static UINT const maxRun     = 254;

// Prototypes

static	void	RLE8	MemCpy(PBYTE d, PCBYTE s, UINT n);
static	void	RLE8	PutByte(PBYTE &pComp, BYTE bData);
static	void	RLE8	PutRun(PBYTE &pComp, UINT uSize, BYTE bData);
static	void	RLE8	PutEndLine(PBYTE &pComp);
static	void	RLE8	PutEndData(PBYTE &pComp);
static	void	RLE8	PutOffset(PBYTE &pComp, BYTE x, BYTE y);
static	void	RLE8	PutMix(PBYTE &pComp, UINT uSize, PCBYTE pData);

// Code

static	void	RLE8	MemCpy(PBYTE d, PCBYTE s, UINT n)
{
	for( ; n; n-- ) *d++ = *s++;
	}

static	void	RLE8	PutByte(PBYTE &pComp, BYTE bData)
{
	*pComp++ = bData;
	}

static	void	RLE8	PutRun(PBYTE &pComp, UINT uSize, BYTE bData)
{
	PutByte(pComp, BYTE(uSize));

	PutByte(pComp, bData);
	}

static	void	RLE8	PutEndLine(PBYTE &pComp)
{
	PutByte(pComp, escEscape);
	
	PutByte(pComp, escEndLine);
	}

static	void	RLE8	PutEndData(PBYTE &pComp)
{
	PutByte(pComp, escEscape);
	
	PutByte(pComp, escEndData);
	}

static	void	RLE8	PutOffset(PBYTE &pComp, BYTE x, BYTE y)
{
	PutByte(pComp, escEscape);
	
	PutByte(pComp, escOffset);

	PutByte(pComp, x);

	PutByte(pComp, y);
	}

global	UINT	RLE8	MakeRLE8(PBYTE pComp, PCBYTE pData, UINT uWidth, BOOL fFirst, BOOL fLast)
{
	PBYTE   pInit   = pComp;

	UINT    uSame   = 0;

	UINT    bData   = *pData;

	UINT    bTest   = 0;

	PCBYTE  pPend   = pData;

	UINT    uPend   = 0;

	PCDWORD pdwData = PCDWORD(pData);

	if( !pdwData[0] && !pdwData[ uWidth / (sizeof(DWORD) * 2)] && !pdwData[ uWidth/sizeof(DWORD) - 1] ) {

		BOOL fNull = TRUE;
		
		for( UINT uScan = 0; uScan < uWidth/sizeof(DWORD); uScan ++ ) {

			if( pdwData[ uScan ] ) {

				fNull = FALSE;

				break;
				}
			}

		if( fNull ) {

			if( fFirst ) {
		
				PutOffset(pComp, 0, 1);

				return 4;
				}

			PBYTE pLink = pComp - 4;

			if( pLink[0] == escEscape && pLink[1] == escOffset ) {
				
				if( pLink[3] < 255 ) {

					pLink[3] ++;

					return 0;
					}
				}

			PutOffset(pComp, 0, 1);

			return 4;
			}
		}

	for( ; uWidth; uWidth-- ) {

		if( (bTest = *pData++) == bData ) {

			if( ++uSame >= maxRun ) {
			
				if( uPend ) {

					PutMix(pComp, uPend, pPend);

					uPend = 0;
					}

				PutRun(pComp, uSame, BYTE(bData));

				uSame = 0;

				pPend = pData;
				}
			}
		else {
			if( uSame >= minRun ) {

				if( uPend ) {

					PutMix(pComp, uPend, pPend);

					uPend = 0;
					}

				PutRun(pComp, uSame, BYTE(bData));

				bData = bTest;

				uSame = 1;

				pPend = pData - 1;
				}
			else {
				bData = bTest;

				uPend = uPend + uSame;

				uSame = 1;
				}
			}
		}

	if( uSame >= minRun ) {

		if( uPend ) {

			PutMix(pComp, uPend, pPend);
			}

		PutRun(pComp, uSame, BYTE(bData));
		}
	else {
		uPend = uPend + uSame;

		PutMix(pComp, uPend, pPend);
		}

	if( fLast )
		PutEndData(pComp);
	else
		PutEndLine(pComp);

	return pComp - pInit;
	}

static	void	RLE8	PutMix(PBYTE &pComp, UINT uSize, PCBYTE pData)
{
	if( uSize < minMix ) {

		for( ; uSize; uSize-- ) {

			PutRun(pComp, 1, *pData++);
			}
		}
	else {
		UINT uChop = maxMix;

		for(;;) {

			if( uSize <= uChop ) {

				PutByte(pComp, escEscape);

				PutByte(pComp, BYTE(uSize));

				MemCpy(pComp, pData, uSize);

				pComp += uSize;

				pComp += (UINT(pComp) & 1);

				return;
				}

			if( uSize - uChop < minMix ) {

				uChop -= 4;
				}

			PutByte(pComp, escEscape);

			PutByte(pComp, BYTE(uChop));

			MemCpy(pComp, pData, uChop);

			pComp += uChop;

			pData += uChop;

			uSize -= uChop;
			}
		}
	}

// End of File
