
#include "intern.hpp"

// Prototypes

HGLOBAL	LoadIntoMemory(char const *pName, BOOL &fExpand);
BOOL	SaveFromMemory(char const *pName, HGLOBAL hData);
BOOL	FileExists(char const *pName);

// Code

void	main(int nArg, char const *pArg[])
{
	if( nArg == 2 ) {

		BOOL    fExpand = FALSE;

		HGLOBAL hData   = LoadIntoMemory(pArg[1], fExpand);

		if( hData ) {

			if( fExpand ) {

				if( SaveFromMemory(pArg[1], hData) ) {

					exit(0);
					}

				printf("expcd3: could not write to file\n");

				exit(3);
				}
			
			printf("expcd3: the file is not compressed\n");

			exit(3);
			}

		printf("expcd3: cannot open input file\n");

		exit(2);
		}

	printf("usage: expcd3 <cd3-file>\n");

	exit(1);
	}

HGLOBAL	LoadIntoMemory(char const *pName, BOOL &fExpand)
{
	HANDLE hFile = CreateFile( pName,
				   GENERIC_READ,
				   FILE_SHARE_READ,
				   NULL,
				   OPEN_EXISTING,
				   FILE_FLAG_SEQUENTIAL_SCAN,
				   NULL
				   );

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT   uData = GetFileSize(hFile, NULL);

		HANDLE hData = GlobalAlloc(GHND, uData);

		if( hData ) {

			PBYTE  pData = PBYTE(GlobalLock(hData));

			DWORD  uRead = 0;

			ReadFile   (hFile, pData, uData, &uRead, NULL);

			CloseHandle(hFile);

			if( uRead == uData ) {

				if( pData[0] == 'L' && pData[1] == 'Z' ) {
			
					for( UINT uGrow = 10;; uGrow <<= 1 ) {

						UINT   uCopy = uData * uGrow;

						HANDLE hCopy = GlobalAlloc(GHND, uCopy);

						if( hCopy ) {

							PBYTE pCopy = PBYTE(GlobalLock(hCopy));

							UINT  uUsed = fastlz_decompress( pData+2,
											 uData-2,
											 pCopy,
											 uCopy
											 );

							if( uUsed == 0 || uUsed == uCopy ) {

								GlobalUnlock(hCopy);

								GlobalFree  (hCopy);

								continue;
								}

							GlobalUnlock(hCopy);

							GlobalUnlock(hData);
			
							GlobalFree  (hData);

							fExpand = TRUE;

							return GlobalReAlloc(hCopy, uUsed, GMEM_MOVEABLE);
							}

						break;
						}
					}
				else {
					GlobalUnlock(hData);

					fExpand = FALSE;

					return hData;
					}
				}

			GlobalUnlock(hData);

			GlobalFree  (hData);
			}
		}

	return NULL;
	}

BOOL	SaveFromMemory(char const *pName, HGLOBAL hCopy)
{
	if( hCopy ) {

		PBYTE pCopy = PBYTE(GlobalLock(hCopy));

		UINT  uCopy = GlobalSize(hCopy);

		if( FileExists(pName) ) {

			char sWork[256] = { ".\\work.tmp" };

			char sPark[256] = { ".\\park.tmp" };

			HANDLE hFile = CreateFile( sWork,
						   GENERIC_WRITE,
						   FILE_SHARE_WRITE,
						   NULL,
						   CREATE_ALWAYS,
						   FILE_ATTRIBUTE_NORMAL,
						   NULL
						   );

			if( hFile != INVALID_HANDLE_VALUE ) {

				DWORD uDone = 0;

				WriteFile(hFile, pCopy, uCopy, &uDone, NULL);

				if( uDone == uCopy ) {

					CloseHandle (hFile);

					if( MoveFile(pName, sPark) ) {

						if( MoveFile(sWork, pName) ) {

							if( DeleteFile(sPark) ) {

								GlobalUnlock(hCopy);

								GlobalFree  (hCopy);

								return TRUE;
								}

							DeleteFile(pName);
							}

						MoveFile(sPark, pName);
						}
					}
				}
			}
		else {
			HANDLE hFile = CreateFile( pName,
						   GENERIC_WRITE,
						   FILE_SHARE_WRITE,
						   NULL,
						   CREATE_ALWAYS,
						   FILE_ATTRIBUTE_NORMAL,
						   NULL
						   );

			if( hFile != INVALID_HANDLE_VALUE ) {

				DWORD uDone = 0;

				WriteFile(hFile, pCopy, uCopy, &uDone, NULL);

				if( uDone == uCopy ) {

					CloseHandle (hFile);

					GlobalUnlock(hCopy);

					GlobalFree  (hCopy);

					return TRUE;
					}
				}
			}

		GlobalUnlock(hCopy);

		GlobalFree  (hCopy);
		}

	return FALSE;
	}

BOOL	FileExists(char const *pName)
{
	HANDLE hFile = CreateFile( pName,
				   GENERIC_READ,
				   FILE_SHARE_READ,
				   NULL,
				   OPEN_EXISTING,
				   FILE_FLAG_SEQUENTIAL_SCAN,
				   NULL
				   );

	if( hFile != INVALID_HANDLE_VALUE ) {

		CloseHandle(hFile);

		return TRUE;
		}

	return FALSE;
	}

// End of File
