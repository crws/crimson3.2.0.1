
#include "Intern.hpp"

#include "OpcNodeId.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Node Identifier
//

// Constructors

COpcNodeId::COpcNodeId(void)
{
	IdentifierType     = OpcUa_IdType_Numeric;

	NamespaceIndex     = 0;

	Identifier.Numeric = 0;
	}

COpcNodeId::COpcNodeId(COpcNodeId const &That)
{
	InitFrom(That);
	}

COpcNodeId::COpcNodeId(OpcUa_NodeId const &That)
{
	InitFrom(That);
	}

COpcNodeId::COpcNodeId(UINT Namespace, UINT Value)
{
	IdentifierType     = OpcUa_IdType_Numeric;

	NamespaceIndex     = BYTE(Namespace);

	Identifier.Numeric = Value;
	}

COpcNodeId::COpcNodeId(UINT Namespace, CString const &Value)
{
	IdentifierType = OpcUa_IdType_String;

	NamespaceIndex = BYTE(Namespace);

	OpcUa_String_Initialize(&Identifier.String);

	OpcUa_String_AttachCopy(&Identifier.String, PTXT(PCTXT(Value)));
	}

COpcNodeId::COpcNodeId(UINT Namespace, CGuid const &Value)
{
	IdentifierType  = OpcUa_IdType_Guid;

	NamespaceIndex  = BYTE(Namespace);

	Identifier.Guid = OpcAlloc(1, OpcUa_Guid);

	memcpy(Identifier.Guid, &Value, sizeof(GUID));
	}

COpcNodeId::COpcNodeId(UINT Namespace, CByteArray const &Value)
{
	IdentifierType = OpcUa_IdType_Opaque;

	NamespaceIndex = BYTE(Namespace);

	Identifier.ByteString.Length = Value.GetCount();

	Identifier.ByteString.Data   = OpcAlloc(Identifier.ByteString.Length, OpcUa_Byte);

	memcpy(Identifier.ByteString.Data, Value.GetPointer(), Identifier.ByteString.Length);
	}

// Destructor

COpcNodeId::~COpcNodeId(void)
{
	OpcUa_NodeId_Clear(this);
	}

// Assignment

COpcNodeId & COpcNodeId::operator = (COpcNodeId const &That)
{
	OpcUa_NodeId_Clear(this);

	InitFrom(That);

	return ThisObject;
	}

COpcNodeId & COpcNodeId::operator = (OpcUa_NodeId const &That)
{
	OpcUa_NodeId_Clear(this);

	InitFrom(That);

	return ThisObject;
	}

// Operators

bool COpcNodeId::operator == (OpcUa_NodeId const &That) const
{
	return AfxCompare(ThisObject, That) == 0;
	}

// Comparison

int AfxCompare(COpcNodeId const &a, COpcNodeId const &b)
{
	if( a.NamespaceIndex == b.NamespaceIndex ) {

		if( a.IdentifierType == b.IdentifierType ) {

			switch( a.IdentifierType ) {

				case OpcUa_IdType_Numeric:

					if( a.Identifier.Numeric == b.Identifier.Numeric ) {

						return 0;
						}

					return (a.Identifier.Numeric < b.Identifier.Numeric) ? -1 : +1;

				case OpcUa_IdType_String:

					return strcmp(a.Identifier.String.strContent, b.Identifier.String.strContent);

				case OpcUa_IdType_Guid:

					return memcmp(a.Identifier.Guid, b.Identifier.Guid, 16);

				case OpcUa_IdType_Opaque:

					if( a.Identifier.ByteString.Length == b.Identifier.ByteString.Length ) {

						return memcmp( a.Identifier.ByteString.Data,
							       b.Identifier.ByteString.Data,
							       a.Identifier.ByteString.Length
							       );
						}

					return (a.Identifier.ByteString.Length < b.Identifier.ByteString.Length) ? -1 : +1;
				}

			AfxAssert(FALSE);
			}

		return (a.IdentifierType < b.IdentifierType) ? -1 : +1;
		}

	return (a.NamespaceIndex < b.NamespaceIndex) ? -1 : +1;
	}

// Attributes

CString COpcNodeId::GetAsText(void) const
{
	if( IdentifierType == OpcUa_IdType_Opaque ) {

		CPrintf s("%u.", NamespaceIndex);

		for( int n = 0; n < Identifier.ByteString.Length; n++ ) {

			s.AppendPrintf(n ? "-%2.2X" : "%2.2X", Identifier.ByteString.Data[n]);
			}

		return s;
		}

	switch( IdentifierType ) {

		case OpcUa_IdType_Numeric:

			return CPrintf("%u.%u", NamespaceIndex, Identifier.Numeric);

		case OpcUa_IdType_String:

			return CPrintf("%u.%s", NamespaceIndex, Identifier.String.strContent);

		case OpcUa_IdType_Guid:

			return CPrintf("%u.%s", NamespaceIndex, PCTXT(GetGuidValue().GetAsText()));
		}

	return CString();
	}

// Operations

CString COpcNodeId::Encode(void) const
{
	if( IdentifierType == OpcUa_IdType_Numeric ) {

		return CPrintf("ns=%u;i=%u", NamespaceIndex, Identifier.Numeric);
		}

	if( IdentifierType == OpcUa_IdType_String ) {

		return CPrintf("ns=%u;s=%s", NamespaceIndex, Identifier.String.strContent);
		}

	if( IdentifierType == OpcUa_IdType_Guid ) {

		return CPrintf("ns=%u;g=%s", NamespaceIndex, PCTXT(GetGuidValue().GetAsText()));
		}

	if( IdentifierType == OpcUa_IdType_Opaque ) {

		// This ought to be in Base64 if we want to conform to what
		// other people do, but since we only use it internally...

		CPrintf Node("ns=%u;b=", NamespaceIndex);

		for( int n = 0; n < Identifier.ByteString.Length; n++ ) {

			Node.AppendPrintf("%2.2X", Identifier.ByteString.Data[n]);
			}

		return Node;
		}

	return CString();
	}

bool COpcNodeId::Decode(CString sid)
{
	CString sns = sid.StripToken(';');

	UINT    ins = atoi(sns.Mid(sns.Find('=')+1));

	CString sit = sid.StripToken('=');

	if( sit[0] == 'i' ) {

		NamespaceIndex     = BYTE(ins);

		IdentifierType     = OpcUa_IdType_Numeric;

		Identifier.Numeric = atoi(sid);

		return true;
		}

	if( sit[0] == 's' ) {

		NamespaceIndex = BYTE(ins);

		IdentifierType = OpcUa_IdType_String;
		
		OpcUa_String_Initialize(&Identifier.String);

		OpcUa_String_AttachCopy(&Identifier.String, PTXT(PCTXT(sid)));

		return true;
		}

	if( sit[0] == 'g' ) {

		CGuid Guid(sid);

		NamespaceIndex  = BYTE(ins);

		IdentifierType  = OpcUa_IdType_Guid;
		
		Identifier.Guid = OpcAlloc(1, OpcUa_Guid);

		memcpy(Identifier.Guid, &Guid, sizeof(GUID));

		return true;
		}

	if( sit[0] == 'b' ) {

		// This ought to be in Base64 if we want to conform to what
		// other people do, but since we only use it internally...

		IdentifierType  = OpcUa_IdType_Opaque;

		Identifier.ByteString.Length = sid.GetLength() / 2;

		Identifier.ByteString.Data   = OpcAlloc(Identifier.ByteString.Length, OpcUa_Byte);

		char c[3] = { 0, 0, 0 };

		for( int n = 0; n < Identifier.ByteString.Length; n++ ) {

			c[0] = sid[2*n+0];
			
			c[1] = sid[2*n+0];

			Identifier.ByteString.Data[n] = BYTE(strtoul(c, NULL, 16));
			}

		return true;
		}

	return false;
	}

// Implementation

void COpcNodeId::InitFrom(OpcUa_NodeId const &That)
{
	IdentifierType = That.IdentifierType;

	NamespaceIndex = That.NamespaceIndex;

	switch( IdentifierType ) {

		case OpcUa_IdType_Numeric:

			Identifier.Numeric = That.Identifier.Numeric;

			return;

		case OpcUa_IdType_String:

			OpcUa_String_Initialize(&Identifier.String);

			OpcUa_String_AttachCopy(&Identifier.String, That.Identifier.String.strContent);

			return;

		case OpcUa_IdType_Guid:

			Identifier.Guid = OpcAlloc(1, OpcUa_Guid);

			memcpy(Identifier.Guid, That.Identifier.Guid, sizeof(GUID));

			return;

		case OpcUa_IdType_Opaque:

			Identifier.ByteString.Length = That.Identifier.ByteString.Length;

			Identifier.ByteString.Data   = OpcAlloc(Identifier.ByteString.Length, OpcUa_Byte);

			memcpy(Identifier.ByteString.Data, That.Identifier.ByteString.Data, Identifier.ByteString.Length);

			return;
		}

	AfxAssert(FALSE);
	}

// End of File
