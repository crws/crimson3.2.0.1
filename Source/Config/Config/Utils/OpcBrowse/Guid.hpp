
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Guid_HPP
	
#define	INCLUDE_Guid_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/54DSE

/////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CString;

//////////////////////////////////////////////////////////////////////////
//								
// GUID Wrapper Object
//

class DLLAPI CGuid : public GUID
{
	public:
		// Constructors
		CGuid(void);
		CGuid(CGuid const &That);
		CGuid(GUID const &Guid);
		CGuid(PCTXT pText);

		// Assignment
		CGuid const & operator = (CGuid const &That);
		CGuid const & operator = (GUID const &Guid);
		CGuid const & operator = (PCTXT pText);

		// Comparison
		BOOL const operator == (CGuid const &That) const;
		BOOL const operator == (GUID const &Guid) const;
		BOOL const operator != (CGuid const &That) const;
		BOOL const operator != (GUID const &Guid) const;

		// Attributes
		BOOL    IsEmpty(void) const;
		BOOL    operator ! (void) const;
		CString GetAsText(void) const;

	protected:
		// Null GUID
		static GUID const m_Null;

		// Implementation
		BOOL InitFromString(PCTXT pText);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

STRONG_INLINE CGuid::CGuid(void)
{
	memcpy(this, &m_Null, sizeof(GUID));
	}

STRONG_INLINE CGuid::CGuid(CGuid const &That)
{
	memcpy(this, &That, sizeof(GUID));
	}

STRONG_INLINE CGuid::CGuid(GUID const &Guid)
{
	memcpy(this, &Guid, sizeof(GUID));
	}

// Assignment

STRONG_INLINE CGuid const & CGuid::operator = (CGuid const &That)
{
	memcpy(this, &That, sizeof(GUID));

	return ThisObject;
	}

STRONG_INLINE CGuid const & CGuid::operator = (GUID const &Guid)
{
	memcpy(this, &Guid, sizeof(GUID));

	return ThisObject;
	}

// Comparison

STRONG_INLINE BOOL const CGuid::operator == (CGuid const &That) const
{
	return memcmp(this, &That, sizeof(ThisObject)) ? FALSE : TRUE;
	}

STRONG_INLINE BOOL const CGuid::operator == (GUID const &Guid) const
{
	return memcmp(this, &Guid, sizeof(ThisObject)) ? FALSE : TRUE;
	}

STRONG_INLINE BOOL const CGuid::operator != (CGuid const &That) const
{
	return memcmp(this, &That, sizeof(ThisObject)) ? TRUE : FALSE;
	}

STRONG_INLINE BOOL const CGuid::operator != (GUID const &Guid) const
{
	return memcmp(this, &Guid, sizeof(ThisObject)) ? TRUE : FALSE;
	}

// Attributes

STRONG_INLINE BOOL CGuid::IsEmpty(void) const
{
	return !memcmp(this, &m_Null, sizeof(GUID));
	}

STRONG_INLINE BOOL CGuid::operator ! (void) const
{
	return !memcmp(this, &m_Null, sizeof(GUID));
	}

// End of File

#endif
