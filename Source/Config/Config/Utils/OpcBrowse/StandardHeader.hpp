
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Header File
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_StandardHeader_HPP

#define	INCLUDE_StandardHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <ctype.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//////////////////////////////////////////////////////////////////////////
//
// Variable Argument Lists
//

#if defined(__GNUC__)

#if defined(__ISense__)

typedef void * va_list;

#define va_start(a, b)

#define va_end(a)

#else

#include <stdarg.h>

#endif

#endif

//////////////////////////////////////////////////////////////////////////
//
// Infinite Wait
//

#if !defined(INFINITE)

#define	INFINITE	UINT(-1)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Useful Constants
//

#define FALSE		0
#define TRUE		1
#define	NOTHING		UINT(-1)
#define	EMPTY		UINT(-1)
#define	FOREVER		INFINITE

//////////////////////////////////////////////////////////////////////////
//
// Common Macros
//

#define global		/**/
#define	New		new
#define ThisObject	(*this)
#define	elements(x)	(sizeof(x) / sizeof(*(x)))
#define	clink		extern "C"
#define	BEGIN		do {
#define	END		} while(0)
#define	AfxTouch(x)	((void) (x))
#define DLLAPI		global
#define	AfxFileHeader()	/**/
#define	INLINE		__inline
#define STRONG_INLINE	__inline
#define	Bit(k)		(1<<(k))

//////////////////////////////////////////////////////////////////////////
//
// MSVC-Specific Macros
//

#if defined(_MSC_VER)

#define	ASM		__asm

#define ALIGN(x)	__declspec(align(x))

#endif

//////////////////////////////////////////////////////////////////////////
//
// GCC-Specific Macros
//

#if defined(__GNUC__)

#define	ASM		__asm volatile

#if !defined(__ISense__)

#define	ALIGN(x)	__attribute__ ((aligned(x)))

#else

#define	ALIGN(x)	/**/

#endif

#endif

//////////////////////////////////////////////////////////////////////////
//
// Typedef Helper
//

#define	AfxDefineType(name, type)		\
						\
	typedef type		    name;	\
	typedef type              * P##name;	\
	typedef type const        * PC##name;	\
	typedef type volatile     * PV##name;	\

//////////////////////////////////////////////////////////////////////////
//
// Typedefs Cleanup
//

#if defined(VOID)

#undef VOID

#endif

//////////////////////////////////////////////////////////////////////////
//
// Common Typedefs
//

AfxDefineType(CHAR,  char);
AfxDefineType(INT,   int);
AfxDefineType(SHORT, short);
AfxDefineType(LONG,  long);
AfxDefineType(VOID,  void);
AfxDefineType(BYTE,  unsigned char);
AfxDefineType(UINT,  unsigned int);
AfxDefineType(WORD,  unsigned short);
AfxDefineType(DWORD, unsigned long);
AfxDefineType(BOOL,  int);
AfxDefineType(TXT,   char);
AfxDefineType(STR,   char);
AfxDefineType(UTF,   wchar_t);

//////////////////////////////////////////////////////////////////////////
//
// MSVC-Specific Typedefs
//

#if defined(_MSC_VER)

AfxDefineType(INT64, __int64);

#endif

//////////////////////////////////////////////////////////////////////////
//
// GCC-Specific Typedefs
//

#if defined(__GNUC__)

AfxDefineType(INT64, long long int);

#endif

//////////////////////////////////////////////////////////////////////////
//
// Min and Max Macros
//

#define	MakeMin(a, b)	BEGIN { if( (b) < (a) ) (a) = (b); } END

#define	MakeMax(a, b)	BEGIN { if( (b) > (a) ) (a) = (b); } END

//////////////////////////////////////////////////////////////////////////
//
// Linked List Macros
//

#define	AfxListAppend(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->p = t;				\
	o->n = NULL;				\
	t ? (t->n = o) : (h = o);		\
	t = o;					\
	} END					\

#define	AfxListInsert(h, t, o, n, p, b)		\
						\
	BEGIN {					\
	o->p = b ? b->p : t;			\
	o->n = b;				\
	o->p ? (o->p->n = o) : (h = o);		\
	o->n ? (o->n->p = o) : (t = o);		\
	} END					\

#define	AfxListRemove(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->n ? (o->n->p = o->p) : (t = o->p);	\
	o->p ? (o->p->n = o->n) : (h = o->n);	\
	} END					\

//////////////////////////////////////////////////////////////////////////
//
// Min and Max
//

#if defined(__cplusplus)

template <class c> inline c Min(c a, c b)
{
	return (a>b)?b:a;
	}

template <class c> inline c Max(c a, c b)
{
	return (a>b)?a:b;
	}

#else

#define Min min

#define Max max

#endif

//////////////////////////////////////////////////////////////////////////
//
// Splitter Macros
//

#if !defined(LOBYTE)

#define LOBYTE(w)	((BYTE)(WORD )(w))

#define LOWORD(l)	((WORD)(DWORD)(l))

#define HIBYTE(w)	((BYTE)((((UINT )(w)) >>  8) & 0x00FF))

#define HIWORD(l)	((WORD)((((DWORD)(l)) >> 16) & 0xFFFF))

#endif

//////////////////////////////////////////////////////////////////////////
//
// Combination Macros
//

#if !defined(MAKEWORD)

#define MAKEWORD(l, h)	((WORD)(((BYTE)(l)) | (((WORD )((BYTE)(h))) <<  8)))

#define MAKELONG(l, h)	((LONG)(((WORD)(l)) | (((DWORD)((WORD)(h))) << 16)))

#endif

// End of File

#endif
