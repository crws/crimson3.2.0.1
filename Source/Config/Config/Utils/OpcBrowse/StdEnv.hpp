
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_StandardEnvironment_HPP

#define	INCLUDE_StandardEnvironment_HPP

//////////////////////////////////////////////////////////////////////////
//
// New-Style Marker
//

#define USING_STDENV

//////////////////////////////////////////////////////////////////////////
//
// Standard Types and Macros
//

#include "StandardHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// General Index Pointer
//

typedef struct tagINDEX { } const * INDEX;

//////////////////////////////////////////////////////////////////////////
//
// Dummy Assertion Macros
//

#define AfxAssert(x)	BEGIN { __assume(x); ((void) (0)); } END

#define	AfxVerify(x)	BEGIN { __assume(x); ((void) (x)); } END

//////////////////////////////////////////////////////////////////////////
//
// Dummy Pointer Validation
//

inline void AfxValidateReadPtr(void const *pData, UINT uSize = 1) { }
inline void AfxValidateWritePtr(void *pData, UINT uSize = 1) { }
inline void AfxValidateStringPtr(PCTXT pText, UINT uSize = 1) { }
inline void AfxValidateResourceName(PCTXT pText, UINT uSize = 1) { }
inline void AfxValidateObject(class CObject const *pObject) { }
inline void AfxValidateObject(class CObject const &Object) { }

//////////////////////////////////////////////////////////////////////////
//
// Comparison Function
//

template <typename t> inline int AfxCompare(t const &a, t const &b)
{
	return (a > b) ? +1 : ((b > a ) ? -1 : 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Collection Templates
//

#include "Array.hpp"

#include "Linked.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CString;

//////////////////////////////////////////////////////////////////////////
//
// Standard Collections
//

typedef CArray   <BYTE>		CByteArray;

typedef CArray   <WORD>		CWordArray;

typedef CArray   <CString>	CStringArray;

//////////////////////////////////////////////////////////////////////////
//
// Wide String Stubs
//

#define	watoi     atoi
#define	watof     atof
#define	wtoupper  toupper
#define	wtolower  tolower
#define	wisupper  isupper
#define	wislower  islower
#define	wisalpha  isalpha
#define	wisalnum  isalnum
#define	wisspace  isspace
#define	wisdigit  isdigit
#define	wstrlen   strlen
#define	wstrcat   strcat
#define	wstrcpy   strcpy
#define	wstrncpy  strncpy
#define	wstrcmp   strcmp
#define	w_stricmp  __stricmp
#define	wstrncmp  strncmp
#define	wstrnicmp _strnicmp
#define	wstrchr   strchr
#define	wstrrchr  strrchr
#define	wstrstr   strstr
#define	wstristr  strstr
#define	wstrspn   strspn
#define	wstrcspn  strcspn
#define	wstrdup   _strdup
#define	wstrdpi   strdpi
#define	wstrupr   _strupr
#define	wstrlwr   _strlwr
#define	wmemset   memset
#define	wmemcpy   memcpy
#define	wmemmove  memmove

//////////////////////////////////////////////////////////////////////////
//
// Text Macro
//

#define	T(x)	x

//////////////////////////////////////////////////////////////////////////
//
// Dummy ENTITY
//

typedef UINT            CEntity;

typedef CEntity const & ENTITY;

// End of File

#endif
