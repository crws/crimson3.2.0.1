
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OPC-UA Browser
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#define	_CRT_SECURE_NO_WARNINGS		TRUE

#define	_CRT_NON_CONFORMING_SWPRINTFS	TRUE

#define	_WIN32_WINNT			0x0501

#define	WINVER				0x0501

#define	NO_STRICT			TRUE

#define NO_INLINE			FALSE

#include "Level4.hpp"

#include <Windows.h>

#include "StdEnv.hpp"
#include "String.hpp"
#include "Printf.hpp"
#include "Guid.hpp"

#include "Level4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Stack Headers
//

#include <opcua.h>

#include <opcua_builtintypes.h>

//////////////////////////////////////////////////////////////////////////
//
// Identifiers
//

#include "OpcIds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define OpcAlloc(n, t)		((t *) (n ? OpcUa_Memory_Alloc((n) * sizeof(t)) : NULL))

#define OpcReAlloc(p, n, t)	((t *) OpcUa_Memory_ReAlloc(p, (n) * sizeof(t)))
	
//////////////////////////////////////////////////////////////////////////
//
// Mapping Macros
//

#define StdSetRef()		m_uRefs = 0

#define STRONG_INLINE		__inline

//////////////////////////////////////////////////////////////////////////
//
// Crimson Type System
//

typedef int   C3INT;

typedef float C3REAL;

enum
{
	typeVoid    = 0,
	typeInteger = 1,
	typeReal    = 2,
	typeString  = 3
	};

inline C3REAL I2R(DWORD d)
{
	return *((C3REAL *) &d);
	}

inline DWORD R2I(C3REAL r)
{
	return *((DWORD *) &r);
	}

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

interface IOpcUaServer;
interface IOpcUaServerHost;
interface IOpcUaDataModel;
interface IOpcUaClient;

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server Interface
//

interface IOpcUaServer
{
	// Methods
	virtual void OpcFree(void)					= 0;
	virtual void OpcLoad(UINT uSample, UINT uQuota, PCBYTE pGuid)   = 0;
	virtual bool OpcInit(IOpcUaServerHost *pHost, CString Endpoint) = 0;
	virtual bool OpcExec(void)					= 0;
	virtual bool OpcStop(void)					= 0;
	virtual bool OpcTerm(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server Host Interface
//

interface IOpcUaServerHost
{
	// Methods
	virtual bool    LoadModel(IOpcUaDataModel *pModel)                           = 0;
	virtual bool    CheckUser(CString const &User, CString const &Pass)	     = 0;
	virtual bool    SkipValue(UINT ns, UINT id)                                  = 0;
	virtual void    SetValueDouble(UINT ns, UINT id, UINT n, double d)           = 0;
	virtual void    SetValueInt(UINT ns, UINT id, UINT n, int d)                 = 0;
	virtual void    SetValueInt64(UINT ns, UINT id, UINT n, INT64 d)             = 0;
	virtual void    SetValueString(UINT ns, UINT id, UINT n, PCTXT d)            = 0;
	virtual double  GetValueDouble(UINT ns, UINT id, UINT n)                     = 0;
	virtual UINT    GetValueInt(UINT ns, UINT id, UINT n)                        = 0;
	virtual UINT64  GetValueInt64(UINT ns, UINT id, UINT n)                      = 0;
	virtual CString GetValueString(UINT ns, UINT id, UINT n)                     = 0;
	virtual timeval GetValueTime(UINT ns, UINT id, UINT n)			     = 0;
	virtual bool    GetDesc(CString &Desc, UINT ns, UINT id)		     = 0;
	virtual bool    GetSourceTimeStamp(timeval &t, UINT ns, UINT id)	     = 0;
	virtual bool    HasEvents(CArray <UINT> &List, UINT ns, UINT id)	     = 0;
	virtual bool    HasCustomHistory(UINT ns, UINT id)			     = 0;
	virtual bool    InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)   = 0;
	virtual bool	HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory)    = 0;
	virtual void	KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)   = 0;
	virtual UINT	GetWireType(UINT uType)                                      = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Data Model
//

interface IOpcUaDataModel
{
	// Methods
	virtual void AddObject(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name)                                                                              = 0;
	virtual void AddVariable(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name, UINT nsData, UINT idData, INT Rank, UINT Dim1, UINT Access, bool fHistory) = 0;
	virtual void AddObjectType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract)                                                          = 0;
	virtual void AddVariableType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, UINT nsData, UINT idData, INT Rank, bool fAbstract)                    = 0;
	virtual void AddDataType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract)                                                            = 0;
	virtual void AddReferenceType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract, bool fSymmetric)                                      = 0;
	virtual void AddReference(UINT nsNode, UINT idNode, UINT nsRef, UINT idRef, UINT nsTarget, UINT idTarget)                                                             = 0;
	virtual void AddOrganizes(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)                                                                                     = 0;
	virtual void AddProperty (UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)                                                                                     = 0;
	virtual void AddComponent(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)                                                                                     = 0;
	virtual void AddMandatoryRule(UINT nsNode, UINT idNode)                                                                                                               = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client Interface
//

interface IOpcUaClient
{
	// Methods
	virtual void OpcFree(void)								       = 0;
	virtual bool OpcInit(void)							               = 0;
	virtual bool OpcStop(void)							               = 0;
	virtual bool OpcTerm(void)							               = 0;
	virtual UINT OpcOpenDevice(PCTXT pHost, PCTXT pUser, PCTXT pPass, CStringArray const &Nodes)   = 0;
	virtual bool OpcCloseDevice(UINT uDevice)						       = 0;
	virtual bool OpcPingDevice(UINT uDevice)						       = 0;
	virtual bool OpcBrowseDevice(UINT uDevice, CStringArray &List)				       = 0;
	virtual bool OpcReadData (UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PDWORD  pData) = 0;
	virtual bool OpcWriteData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IOpcUaServer * Create_OpcUaServer(void);

extern IOpcUaClient * Create_OpcUaClient(void);

// End of File

#endif
