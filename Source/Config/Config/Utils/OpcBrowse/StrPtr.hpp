
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_StrPtr_HPP
	
#define	INCLUDE_StrPtr_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/_YKgE

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CString;

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

class DLLAPI CStrPtr
{
	public:
		// Constructors
		CStrPtr(CStrPtr const &That);
		CStrPtr(CString const &That);
		CStrPtr(PCTXT pText);

		// Conversion
		operator PCTXT (void) const;

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetLength(void) const;
		UINT GetHashValue(void) const;

		// Read-Only Access
		TCHAR GetAt(UINT uIndex) const;

		// Array Conversion
		UINT GetCharCodes(CWordArray &Code);

		// Comparison Functions
		int CompareC(PCTXT pText) const;
		int CompareN(PCTXT pText) const;
		int CompareC(PCTXT pText, UINT uCount) const;
		int CompareN(PCTXT pText, UINT uCount) const;
		int GetScore(PCTXT pText) const;

		// Comparison Helper
		friend DLLAPI int AfxCompare(CStrPtr const &a, CStrPtr const &b);

		// Comparison Operators
		BOOL operator == (PCTXT pText) const;
		BOOL operator != (PCTXT pText) const;
		BOOL operator <  (PCTXT pText) const;
		BOOL operator >  (PCTXT pText) const;
		BOOL operator <= (PCTXT pText) const;
		BOOL operator >= (PCTXT pText) const;

		// Substring Extraction
		CString Left(UINT uCount) const;
		CString Right(UINT uCount) const;
		CString Mid(UINT uPos, UINT uCount) const;
		CString Mid(UINT uPos) const;

		// Case Conversion
		CString ToUpper(void) const;
		CString ToLower(void) const;

		// Searching
		UINT Find(TCHAR cChar) const;
		UINT Find(TCHAR cChar, TCHAR cQuote) const;
		UINT Find(TCHAR cChar, TCHAR cOpen, TCHAR cClose) const;
		UINT Find(TCHAR cChar, UINT uPos) const;
		UINT Find(TCHAR cChar, UINT uPos, TCHAR cQuote) const;
		UINT Find(TCHAR cChar, UINT uPos, TCHAR cOpen, TCHAR cClose) const;
		UINT FindRev(TCHAR cChar) const;
		UINT FindRev(TCHAR cChar, UINT uPos) const;
		UINT Find(PCTXT pText) const;
		UINT Find(PCTXT pText, UINT uPos) const;
		UINT FindNot(PCTXT pList) const;
		UINT FindNot(PCTXT pList, UINT uPos) const;
		UINT FindOne(PCTXT pList) const;
		UINT FindOne(PCTXT pList, UINT uPos) const;

		// Searching
		UINT Search(PCTXT pText, UINT uMethod) const;
		UINT Search(PCTXT pText, UINT uFrom, UINT uMethod) const;

		// Partials
		BOOL StartsWith(PCTXT pText) const;
		BOOL EndsWith(PCTXT pText) const;

		// Counting
		UINT Count(TCHAR cChar) const;

		// Diagnostics
		void AssertValid(void) const;

	protected:
		// Data Members
		PTXT m_pText;

		// Protected Constructors
		CStrPtr(void) { }
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

INLINE CStrPtr::CStrPtr(CStrPtr const &That)
{
	That.AssertValid();
	
	m_pText = That.m_pText;
	}
	
INLINE CStrPtr::CStrPtr(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	m_pText = (PTXT) pText;
	}

// Attributes

INLINE BOOL CStrPtr::IsEmpty(void) const
{
	return !*m_pText;
	}
	
INLINE BOOL CStrPtr::operator ! (void) const
{
	return !*m_pText;
	}

INLINE UINT CStrPtr::GetLength(void) const
{
	return strlen(m_pText);
	}

// Conversions

INLINE CStrPtr::operator PCTXT (void) const
{
	return m_pText;
	}

// Comparison Functions

INLINE int CStrPtr::CompareC(PCTXT pText) const
{
	AfxValidateStringPtr(pText);
	
	return strcmp(m_pText, pText);
	}

INLINE int CStrPtr::CompareN(PCTXT pText) const
{
	AfxValidateStringPtr(pText);
	
	return _stricmp(m_pText, pText);
	}

INLINE int CStrPtr::CompareC(PCTXT pText, UINT uCount) const
{
	AfxValidateStringPtr(pText);
	
	return strncmp(m_pText, pText, uCount);
	}

INLINE int CStrPtr::CompareN(PCTXT pText, UINT uCount) const
{
	AfxValidateStringPtr(pText);
	
	return _strnicmp(m_pText, pText, uCount);
	}

// Comparison Operators

INLINE BOOL CStrPtr::operator == (PCTXT pText) const
{
	return CompareN(pText) == 0;
	}

INLINE BOOL CStrPtr::operator != (PCTXT pText) const
{
	return CompareN(pText) != 0;
	}

INLINE BOOL CStrPtr::operator < (PCTXT pText) const
{
	return CompareN(pText) < 0;
	}

INLINE BOOL CStrPtr::operator > (PCTXT pText) const
{
	return CompareN(pText) > 0;
	}

INLINE BOOL CStrPtr::operator <= (PCTXT pText) const
{
	return CompareN(pText) <= 0;
	}

INLINE BOOL CStrPtr::operator >= (PCTXT pText) const
{
	return CompareN(pText) >= 0;
	}

// End of File

#endif
