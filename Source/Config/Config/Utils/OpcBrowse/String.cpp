
#include "Intern.hpp"

#include "String.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/ywDUE

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Guid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

// Null Data

DWORD	      CString::m_Null[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

CStringData & CString::m_Empty   = (CStringData &) m_Null[0];

// Constructors

CString::CString(PCTXT pText, UINT uCount)
{
	AfxValidateStringPtr(pText, uCount);
	
	if( *pText && uCount ) {
		
		Alloc(uCount);
		
		strncpy(m_pText, pText, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(TCHAR cData, UINT uCount)
{
	AfxAssert(cData);
	
	if( uCount ) {
		
		Alloc(uCount);
		
		memset(m_pText, cData, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(GUID const &Guid)
{
	m_pText = m_Empty.m_cData;

	InitFrom(Guid);
	}

// Assignment Operators

CString const & CString::operator = (CString const &That)
{
	if( this != &That ) {
		
		That.AssertValid();
		
		Release();
		
		if( !That.IsEmpty() ) {
			
			m_pText = That.m_pText;
			
			GetStringData(this)->m_nRefs++;
			}
		}
	
	return ThisObject;
	}

CString const & CString::operator = (GUID const &Guid)
{
	Release();

	InitFrom(Guid);

	return ThisObject;
	}

CString const & CString::operator = (PCTXT pText)
{
	AfxValidateStringPtr(pText);

	if( *pText ) {
		
		if( pText >= m_pText && pText <= m_pText + GetLength() ) {
			
			CString Work = pText;
			
			Release();
			
			Alloc(Work.GetLength());
			
			strcpy(m_pText, Work.m_pText);
			}
		else {
			Release();
			
			Alloc(strlen(pText));
			
			strcpy(m_pText, pText);
			}
		}
	else
		Release();
	
	return ThisObject;
	}

// Quick Init

void CString::QuickInit(PCTXT pText, UINT uSize)
{
	CopyOnWrite();

	CStringData *pData = GetStringData(this);

	UINT        uAlloc = FindAlloc(uSize);
	
	if( pData->m_uAlloc >= uAlloc ) {
		
		pData->m_uLen = uSize;
		}
	else {
		delete pData;
		
		Alloc(uSize, uAlloc);
		}
		
	memcpy(m_pText, pText, uSize);

	m_pText[uSize] = 0;
	}

// Substring Extraction

CString CString::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	return CString(m_pText, uCount);
	}

CString CString::Right(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	PCTXT pSrc = m_pText + uLen - uCount;
	
	return CString(pSrc, uCount);
	}

CString CString::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	MakeMin(uCount, uLen - uPos);
	
	PCTXT pSrc = m_pText + uPos;
	
	return CString(pSrc, uCount);
	}

CString CString::Mid(UINT uPos) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	PCTXT pSrc = m_pText + uPos;
	
	return CString(pSrc, uLen - uPos);
	}

// Buffer Managment

void CString::Empty(void)
{
	Release();
	}

void CString::Expand(UINT uAlloc)
{
	CopyOnWrite();
	
	CStringData *pData = GetStringData(this);
	
	if( pData->m_uAlloc < uAlloc ) {
		
		PCTXT pText = m_pText;
		
		Alloc(pData->m_uLen, uAlloc);
		
		strcpy(m_pText, pText);
		
		delete pData;
		}
	}

void CString::Compress(void)
{
	CopyOnWrite();
	
	CStringData *pData = GetStringData(this);
	
	if( pData->m_uAlloc > FindAlloc(pData->m_uLen) ) {
		
		PCTXT pText = m_pText;
		
		Alloc(pData->m_uLen);
		
		strcpy(m_pText, pText);
		
		delete pData;
		}
	}

void CString::CopyOnWrite(void)
{
	CStringData *pData = GetStringData(this);
	
	if( pData->m_nRefs ) {
		
		if( pData->m_nRefs > 1 ) {
			
			PCTXT pText = m_pText;
			
			pData->m_nRefs--;
			
			Alloc(pData->m_uLen);
			
			strcpy(m_pText, pText);
			}
		}
	else {
		Alloc(0);
		
		GetStringData(this)->m_uLen = 0;
		
		*m_pText = 0;
		}
	}

void CString::FixLength(UINT uLen)
{
	CStringData *pData   = GetStringData(this);

	pData->m_cData[uLen] = 0;

	pData->m_uLen        = uLen;
	}

void CString::FixLength(void)
{
	GetStringData(this)->m_uLen = strlen(m_pText);
	}

// Concatenation In-Place

CString const & CString::operator += (CString const &That)
{
	That.AssertValid();
	
	if( !That.IsEmpty() ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		CString Copy = That;
		
		GrowString(uLen + Copy.GetLength());
		
		strcpy(m_pText + uLen, Copy.m_pText);
		}
	
	return ThisObject;
	}

CString const & CString::operator += (PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( *pText ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CString Copy = pText;
			
			GrowString(uLen + Copy.GetLength());
		
			strcpy(m_pText + uLen, pText);
			}
		else {
			GrowString(uLen + strlen(pText));
		
			strcpy(m_pText + uLen, pText);
			}
		}
	
	return ThisObject;
	}

CString const & CString::operator += (TCHAR cData)
{
	AfxAssert(cData);
	
	UINT uLen = GetStringData(this)->m_uLen;
	
	GrowString(uLen + 1);
	
	m_pText[uLen + 0] = cData;
	
	m_pText[uLen + 1] = 0;
	
	return ThisObject;
	}

// Concatenation via Friends

CString operator + (CString const &A, CString const &B)
{
	A.AssertValid();
	
	B.AssertValid();
	
	return CString(A.m_pText, A.GetLength(), B.m_pText, B.GetLength());
	}

CString operator + (CString const &A, PCTXT pText)
{
	A.AssertValid();
	
	AfxValidateStringPtr(pText);
	
	return CString(A.m_pText, A.GetLength(), pText, strlen(pText));
	}

CString operator + (CString const &A, TCHAR cData)
{
	A.AssertValid();
	
	AfxAssert(cData);
	
	return CString(A.m_pText, A.GetLength(), &cData, 1);
	}

CString operator + (PCTXT pText, CString const &B)
{
	AfxValidateStringPtr(pText);
	
	B.AssertValid();
	
	return CString(pText, strlen(pText), B.m_pText, B.GetLength());
	}

CString operator + (TCHAR cData, CString const &B)
{
	AfxAssert(cData);
	
	B.AssertValid();
	
	return CString(&cData, 1, B.m_pText, B.GetLength());
	}

// Concatenation Functions

BOOL CString::Append(CString const &That)
{
	if( !That.IsEmpty() ) {
		
		UINT    uLen = GetStringData(this)->m_uLen;
		
		CString Copy = That;
		
		GrowString(uLen + Copy.GetLength());
		
		strcpy(m_pText + uLen, Copy.m_pText);

		return TRUE;
		}

	return FALSE;
	}

BOOL CString::Append(PCTXT pText)
{
	if( *pText ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CString Copy(pText);
			
			GrowString(uLen + Copy.GetLength());
			
			strcpy(m_pText + uLen, Copy.m_pText);
			}
		else {
			GrowString(uLen + strlen(pText));
			
			strcpy(m_pText + uLen, pText);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CString::Append(PCTXT pText, UINT uText)
{
	if( *pText && uText ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CString Copy(pText, uText);
			
			GrowString(uLen + Copy.GetLength());
			
			strcpy(m_pText + uLen, Copy.m_pText);
			}
		else {
			MakeMin(uText, strlen(pText));

			GrowString(uLen + uText);
			
			memcpy(m_pText + uLen, pText, uText);

			m_pText[uLen + uText] = 0;
			}

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CString::Append(char cData)
{
	if( cData ) {

		UINT uLen = GetStringData(this)->m_uLen;
	
		GrowString(uLen + 1);
	
		m_pText[uLen + 0] = cData;
	
		m_pText[uLen + 1] = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CString::AppendPrintf(PCTXT pFormat, ...)
{
	va_list pArgs;

	va_start(pArgs, pFormat);

	AppendVPrintf(pFormat, pArgs);

	va_end(pArgs);

	return TRUE;
	}

BOOL CString::AppendVPrintf(PCTXT pFormat, va_list pArgs)
{
	if( *pFormat ) {

		// REV3 -- Any way to make this more adaptive? What we
		// do here is round the buffer up to the next multiple
		// of uInc, always leaving at last uMax at the end. It
		// means you can always print uMax characters and never
		// worry about overflows, and it does this without too
		// many reallocations.

		UINT uLen = GetStringData(this)->m_uLen;

		UINT uMax = 256;

		UINT uInc = uMax * 2;

		UINT uNew = uInc * ((uLen + uMax + uInc - 1) / uInc);

		GrowString(uNew);

		CStringData *pData = GetStringData(this);

		vsprintf(pData->m_cData + uLen, pFormat, pArgs);

		pData->m_uLen = strlen(pData->m_cData);

		return TRUE;
		}

	return FALSE;
	}

// Write Data Access

void CString::SetAt(UINT uIndex, TCHAR cData)
{
	AfxAssert(uIndex < GetLength());
	
	AfxAssert(cData);
	
	CopyOnWrite();
	
	m_pText[uIndex] = cData;
	}

// Insert and Remove

void CString::Insert(UINT uIndex, PCTXT pText)
{
	UINT uLength = GetLength();

	UINT uCount  = strlen(pText);

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	memmove( m_pText + uIndex + uCount,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	memcpy(  m_pText + uIndex,
		  pText,
		  uCount
		  );
	}

void CString::Insert(UINT uIndex, CString const &That)
{
	UINT uLength = GetLength();

	UINT uCount  = That.GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	memmove( m_pText + uIndex + uCount,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	memcpy(  m_pText + uIndex,
		  That.m_pText,
		  uCount
		  );
	}

void CString::Insert(UINT uIndex, TCHAR cData)
{
	UINT uLength = GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + 1);

	memmove( m_pText + uIndex + 1,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	m_pText[uIndex] = cData;
	}

void CString::Delete(UINT uIndex, UINT uCount)
{
	UINT uLength = GetLength();

	if( uIndex < uLength ) {
		
		MakeMin(uCount, uLength - uIndex);

		CopyOnWrite();

		memcpy( m_pText + uIndex,
			 m_pText + uIndex + uCount,
			 uLength - uIndex - uCount + 1
			 );

		GetStringData(this)->m_uLen -= uCount;
		}
	}

// Whitespace Trimming

void CString::TrimLeft(void)
{
	UINT c = GetLength();

	UINT n = 0;

	while( m_pText[n] ) {

		if( !wisspace(m_pText[n]) ) {

			break;
			}

		n++;
		}

	if( n ) {

		if( !m_pText[n] ) {

			Empty();

			return;
			}

		CopyOnWrite();

		memcpy(m_pText, m_pText + n, c - n + 1);

		GetStringData(this)->m_uLen -= n;
		}
	}

void CString::TrimRight(void)
{
	UINT c = GetLength();

	UINT n = c;

	while( n ) {

		if( !wisspace(m_pText[n - 1]) ) {

			break;
			}

		n--;
		}

	if( n < c ) {

		if( !n ) {

			Empty();

			return;
			}

		CopyOnWrite();

		m_pText[n] = 0;

		GetStringData(this)->m_uLen = n;
		}
	}

void CString::TrimBoth(void)
{
	TrimRight();

	TrimLeft();
	}

void CString::StripAll(void)
{
	UINT s = 0;

	UINT e = 0;

	for(;;) { 
	
		while( m_pText[s] ) {

			if( wisspace(m_pText[s]) ) {

				break;
				}

			s++;
			}

		e = s;	
		
		while( m_pText[e] ) {

			if( !wisspace(m_pText[e]) ) {

				break;
				}

			e++;
			}
	
		UINT cb = e - s;
		
		if( cb ) {

			if( cb == GetLength() ) {
			
				Empty();

				return;
				}
			
			CopyOnWrite();

			if( e == GetLength() ) {

				m_pText[s] = 0;

				GetStringData(this)->m_uLen = s;
				
				return;
				}

			memcpy( m_pText + s,
				 m_pText + e,
				 GetLength() - s - cb + 1
				 );

			GetStringData(this)->m_uLen -= cb;

			e = s;
			}
		else
			break;
		}
	}

// Case Switching

void CString::MakeUpper(void)
{
	CopyOnWrite();
	
	_strupr(m_pText);
	}

void CString::MakeLower(void)
{
	CopyOnWrite();
	
	_strlwr(m_pText);
	}

// Replacement

UINT CString::Replace(TCHAR cFind, TCHAR cNew)
{
	UINT uCount = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cFind ) {

			if( !uCount ) {

				CopyOnWrite();
				}

			m_pText[n] = cNew;

			uCount++;
			}
		}

	return uCount;
	}

UINT CString::Replace(PCTXT pFind, PCTXT pNew)
{
	UINT uCount = 0;

	UINT uStart = 0;

	for(;;) {

		UINT uPos = Find(pFind, uStart);

		if( uPos < NOTHING ) {
			
			Delete(uPos, strlen(pFind));

			Insert(uPos, pNew);

			uStart = uPos + strlen(pNew);

			uCount ++;
			}
		else
			break;
		}

	return uCount;
	}

// Removal

void CString::Remove(TCHAR cFind)
{
	for(;;) {

		UINT uPos = Find(cFind);

		if( uPos == NOTHING ) {

			return;
			}

		Delete(uPos, 1);
		}
	}

CString CString::Without(TCHAR cFind)
{
	CString Work = ThisObject;

	Work.Remove(cFind);

	return Work;
	}

// Parsing

UINT CString::Tokenize(CStringArray &Array, TCHAR cFind) const
{
	return Tokenize(Array, NOTHING, cFind);
	}

UINT CString::Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cQuote) const
{
	return Tokenize(Array, NOTHING, cFind, cQuote);
	}

UINT CString::Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const
{
	return Tokenize(Array, NOTHING, cFind, cOpen, cClose);
	}

UINT CString::Tokenize(CStringArray &Array, PCTXT pFind) const
{
	return Tokenize(Array, NOTHING, pFind);
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind));

		uCount++;
		}

	return uCount;
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind, TCHAR cQuote) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind, cQuote));

		uCount++;
		}

	return uCount;
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind, cOpen, cClose));

		uCount++;
		}

	return uCount;
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, PCTXT pFind) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {
		
		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(pFind));

		uCount++;
		}

	return uCount;
	}

CString CString::TokenLeft(TCHAR cFind) const
{
	UINT uPos = Find(cFind);

	return Left(uPos);
	}

CString CString::TokenFrom(TCHAR cFind) const
{
	UINT uPos = Find(cFind);

	if( uPos < NOTHING ) {

		return Mid(uPos+1);
		}

	return T("");
	}

CString CString::TokenLast(TCHAR cFind) const
{
	UINT uPos = FindRev(cFind);

	return Mid(uPos+1);
	}

CString CString::StripToken(TCHAR cFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind);

		CString Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return T("");
	}

CString CString::StripToken(TCHAR cFind, TCHAR cQuote)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind, cQuote);

		CString Text = Left(uPos);

		if( Text[0] == cQuote ) {

			UINT uLen = Text.GetLength();

			if( Text[uLen-1] == cQuote ) {
			
				Text = Text.Mid(1, uLen-2);
				}
			}

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return T("");
	}

CString CString::StripToken(TCHAR cFind, TCHAR cOpen, TCHAR cClose)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind, cOpen, cClose);

		CString Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return T("");
	}

CString CString::StripToken(PCTXT pFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = FindOne(pFind);

		CString Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + strlen(pFind));

		return Text;
		}

	return T("");
	}

// Building

void CString::Build(CStringArray &Array, TCHAR cSep)
{
	Empty();

	UINT c = Array.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( n ) {

			operator += (cSep);
			}

		operator += (Array[n]);
		}
	}

void CString::Build(CStringArray &Array, PCTXT pSep)
{
	Empty();

	UINT c = Array.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( n ) {

			operator += (pSep);
			}

		operator += (Array[n]);
		}
	}

// Display Ordering

void CString::MakeDisplayOrder(void)
{
	}

// Printf Formatting

void CString::Printf(PCTXT pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);

	UINT uResult = 0;
	
	PTXT pResult = IntPrintf(uResult, pFormat, pArgs);
	
	GrowString(uResult);
	
	strcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

void CString::VPrintf(PCTXT pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);

	UINT uResult = 0;
	
	PTXT pResult = IntPrintf(uResult, pFormat, pArgs);
	
	GrowString(uResult);
	
	strcpy(m_pText, pResult);
	
	delete pResult;
	}

// Diagnostics

#ifdef _DEBUG

void CString::AssertValid(void) const
{
	AfxValidateReadPtr(this, sizeof(ThisObject));
	
	CStringData *pData = GetStringData(this);
	
	if( pData->m_nRefs == 0 ) {
		
		AfxAssert(*m_pText == 0);
		
		AfxAssert(pData->m_uLen == 0);
		}
	else
		AfxAssert(pData->m_uAlloc > pData->m_uLen);
	
	AfxValidateStringPtr(m_pText);

	AfxAssert(m_Empty.m_nRefs == 0);
	}

#endif

// Protected Constructor

CString::CString(PCTXT p1, UINT u1, PCTXT p2, UINT u2)
{
	Alloc(u1 + u2);
	
	strncpy(m_pText, p1, u1);
	
	strncpy(m_pText + u1, p2, u2);
	
	m_pText[u1 + u2] = 0;
	}

// Initialisation

void CString::InitFrom(GUID const &Guid)
{
	operator = (((CGuid &) Guid).GetAsText());
	}

// Internal Helpers

PTXT CString::IntPrintf(UINT &uSize, PCTXT pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	UINT uAlloc = 512;
	
	for(;;) {
		
		PTXT pWork = New TCHAR [ uAlloc ];
		
		pWork[uAlloc - 1] = 0;
		
		if( int(uSize = vsnprintf(pWork, uAlloc - 1, pFormat, pArgs)) < 0 ) {
			
			delete [] pWork;
			
			uAlloc <<= 1;
			
			AfxAssert(uAlloc);
			
			continue;
			}
		
		return pWork;
		}
	}

// Implementation

BOOL CString::GrowString(UINT uLen)
{
	CopyOnWrite();
	
	CStringData *pData = GetStringData(this);
	
	if( FindAlloc(uLen) > pData->m_uAlloc ) {
		
		PCTXT pText = m_pText;
		
		Alloc(uLen);
		
		strcpy(m_pText, pText);
		
		delete pData;

		return TRUE;
		}

	pData->m_uLen = uLen;

	return FALSE;
	}

// End of File
