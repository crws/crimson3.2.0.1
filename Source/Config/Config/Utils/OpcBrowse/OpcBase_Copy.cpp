
#include "Intern.hpp"

#include "OpcBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#define CopyScalar(t)				\
						\
	case OpcUaId_##t:			\
	pDest->Value.t = pFrom->Value.t;	\
	break					\

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Base Class
//

// Copy Helpers

void COpcBase::CopyNotificationMessage(OpcUa_NotificationMessage *pDest,  OpcUa_NotificationMessage const *pFrom)
{
	pDest->PublishTime          = pFrom->PublishTime;

	pDest->SequenceNumber       = pFrom->SequenceNumber;

	pDest->NoOfNotificationData = pFrom->NoOfNotificationData;

	pDest->NotificationData     = OpcAlloc(pDest->NoOfNotificationData, OpcUa_ExtensionObject);

	for( INT n = 0; n < pDest->NoOfNotificationData; n++ ) {

		OpcUa_ExtensionObject       *pDestExt = pDest->NotificationData + n;

		OpcUa_ExtensionObject const *pFromExt = pFrom->NotificationData + n;

		OpcUa_ExtensionObject_Initialize(pDestExt);

		CopyExtensionObject(pDestExt, pFromExt);
		}
	}

void COpcBase::CopyExtensionObject(OpcUa_ExtensionObject *pDest, OpcUa_ExtensionObject const *pFrom)
{
	CopyExpandedNodeId(&pDest->TypeId, &pFrom->TypeId);

	pDest->Encoding                   = pFrom->Encoding;

	pDest->BodySize                   = pFrom->BodySize;

	pDest->Body.EncodeableObject.Type = pFrom->Body.EncodeableObject.Type;

	if( pDest->Body.EncodeableObject.Type == &OpcUa_DataChangeNotification_EncodeableType ) {
			
		OpcUa_DataChangeNotification *pDataChange = OpcAlloc(1, OpcUa_DataChangeNotification);

		OpcUa_DataChangeNotification_Initialize(pDataChange);

		CopyDataChangeNotification(pDataChange, (OpcUa_DataChangeNotification const *) pFrom->Body.EncodeableObject.Object);

		pDest->Body.EncodeableObject.Object = pDataChange;
		
		return;
		}
	
	if( pDest->Body.EncodeableObject.Type == &OpcUa_EventNotificationList_EncodeableType ) {

		OpcUa_EventNotificationList *pEventList = OpcAlloc(1, OpcUa_EventNotificationList);

		OpcUa_EventNotificationList_Initialize(pEventList);

		CopyEventNotificationList(pEventList, (OpcUa_EventNotificationList const *) pFrom->Body.EncodeableObject.Object);

		pDest->Body.EncodeableObject.Object = pEventList;
		
		return;
		}

	AfxAssert(FALSE);
	}

void COpcBase::CopyDataChangeNotification(OpcUa_DataChangeNotification *pDest, OpcUa_DataChangeNotification const *pFrom)
{
	pDest->NoOfDiagnosticInfos = pFrom->NoOfDiagnosticInfos;

	pDest->DiagnosticInfos     = OpcAlloc(pDest->NoOfDiagnosticInfos, OpcUa_DiagnosticInfo);

	pDest->NoOfMonitoredItems  = pFrom->NoOfMonitoredItems;

	pDest->MonitoredItems      = OpcAlloc(pDest->NoOfMonitoredItems, OpcUa_MonitoredItemNotification);

	for( INT n = 0; n < pDest->NoOfDiagnosticInfos; n++ ) {

		OpcUa_DiagnosticInfo_Initialize(pDest->DiagnosticInfos + n);

		// TODO -- Copy all this crap!!!
		}

	for( INT m = 0; m < pDest->NoOfMonitoredItems; m++ ) {

		OpcUa_MonitoredItemNotification_Initialize(pDest->MonitoredItems + m);

		CopyMonitoredItemNotification(pDest->MonitoredItems + m, pFrom->MonitoredItems + m);
		}
	}

void COpcBase::CopyEventNotificationList(OpcUa_EventNotificationList *pDest, OpcUa_EventNotificationList const *pFrom)
{
	pDest->NoOfEvents = pFrom->NoOfEvents;

	pDest->Events     = OpcAlloc(pDest->NoOfEvents, OpcUa_EventFieldList);

	for( INT m = 0; m < pDest->NoOfEvents; m++ ) {

		OpcUa_EventFieldList_Initialize(pDest->Events + m);

		CopyEventFieldList(pDest->Events + m, pFrom->Events + m);
		}
	}

void COpcBase::CopyMonitoredItemNotification(OpcUa_MonitoredItemNotification *pDest, OpcUa_MonitoredItemNotification const *pFrom)
{
	pDest->ClientHandle = pFrom->ClientHandle;

	CopyDataValue(&pDest->Value, &pFrom->Value);
	}

void COpcBase::CopyEventFieldList(OpcUa_EventFieldList *pDest, OpcUa_EventFieldList const *pFrom)
{
	pDest->ClientHandle    = pFrom->ClientHandle;

	pDest->NoOfEventFields = pFrom->NoOfEventFields;

	pDest->EventFields     = OpcAlloc(pDest->NoOfEventFields, OpcUa_Variant);

	for( INT m = 0; m < pDest->NoOfEventFields; m++ ) {

		OpcUa_Variant_Initialize(pDest->EventFields + m);
		
		CopyVariant(pDest->EventFields + m, pFrom->EventFields + m);
		}
	}

void COpcBase::CopyDataValue(OpcUa_DataValue *pDest, OpcUa_DataValue const *pFrom)
{
	pDest->ServerPicoseconds = pFrom->ServerPicoseconds;

	pDest->ServerTimestamp   = pFrom->ServerTimestamp;

	pDest->SourcePicoseconds = pFrom->SourcePicoseconds;

	pDest->SourceTimestamp   = pFrom->SourceTimestamp;

	pDest->StatusCode        = pFrom->StatusCode;

	CopyVariant(&pDest->Value, &pFrom->Value);
	}

void COpcBase::CopyVariant(OpcUa_Variant *pDest, OpcUa_Variant const *pFrom)
{
	pDest->ArrayType = pFrom->ArrayType;

	pDest->Datatype  = pFrom->Datatype;

	pDest->Reserved  = pFrom->Reserved;

	if( pDest->ArrayType == OpcUa_VariantArrayType_Scalar ) {

		switch( pDest->Datatype ) {

			CopyScalar(Double);
			CopyScalar(Float);
			CopyScalar(Byte);
			CopyScalar(SByte);
			CopyScalar(Int16);
			CopyScalar(Int32);
			CopyScalar(Int64);
			CopyScalar(UInt16);
			CopyScalar(UInt32);
			CopyScalar(UInt64);
			CopyScalar(Boolean);
			CopyScalar(DateTime);

			case OpcUaId_String:

				OpcUa_String_Initialize(&pDest->Value.String);

				OpcUa_String_AttachCopy(&pDest->Value.String, pFrom->Value.String.strContent);

				break;

			case OpcUaId_NodeId:

				pDest->Value.NodeId = OpcAlloc(1, OpcUa_NodeId);

				OpcUa_NodeId_Initialize(pDest->Value.NodeId);

				(COpcNodeId &) *pDest->Value.NodeId = *pFrom->Value.NodeId;

				break;

			default:

				AfxAssert(FALSE);
			}

		return;
		}

	AfxAssert(FALSE);
	}

void COpcBase::CopyExpandedNodeId(OpcUa_ExpandedNodeId *pDest, OpcUa_ExpandedNodeId const *pFrom)
{
	pDest->ServerIndex = pFrom->ServerIndex;

	OpcUa_String_AttachCopy(&pDest->NamespaceUri, pFrom->NamespaceUri.strContent);

	CopyNodeId(&pDest->NodeId, &pFrom->NodeId);
	}

void COpcBase::CopyNodeId(OpcUa_NodeId *pDest, OpcUa_NodeId const *pFrom)
{
	pDest->NamespaceIndex = pFrom->NamespaceIndex;

	pDest->IdentifierType = pFrom->IdentifierType;

	switch( pDest->IdentifierType ) {

		case OpcUa_IdentifierType_Numeric:

			pDest->Identifier.Numeric = pFrom->Identifier.Numeric;

			break;

		case OpcUa_IdentifierType_String:

			OpcUa_String_Initialize(&pDest->Identifier.String);

			OpcUa_String_AttachCopy(&pDest->Identifier.String, pFrom->Identifier.String.strContent);

			break;

		case OpcUa_IdentifierType_Guid:

			pDest->Identifier.Guid = OpcAlloc(1, OpcUa_Guid);

			memcpy(pDest->Identifier.Guid, pFrom->Identifier.Guid, sizeof(GUID));

			break;

		case OpcUa_IdentifierType_Opaque:

			pDest->Identifier.ByteString.Length = pFrom->Identifier.ByteString.Length;

			pDest->Identifier.ByteString.Data   = OpcAlloc(pDest->Identifier.ByteString.Length, OpcUa_Byte);

			memcpy(pDest->Identifier.ByteString.Data, pFrom->Identifier.ByteString.Data, pDest->Identifier.ByteString.Length);

			break;

		default:

			AfxAssert(FALSE);
		}
	}

// End of File
