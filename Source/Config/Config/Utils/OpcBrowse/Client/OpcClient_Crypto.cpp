
#include "Intern.hpp"

#include "OpcClient.hpp"

#include <bcrypt.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Prototypes

static UINT GetKeyBytes(BCRYPT_KEY_HANDLE hKey);

// Libraries

#pragma comment(lib, "bcrypt.lib")

#pragma comment(lib, "Crypt32.lib")

// Crypto Support

bool COpcClient::EncryptSecret(CByteArray &Data, CString &Name, PCTXT pMethod, PCBYTE pCert, UINT uCert, CByteArray const &Secret)
{
	PCCERT_CONTEXT pCtx = CertCreateCertificateContext(X509_ASN_ENCODING, pCert, uCert);

	if( pCtx ) {

		PCBYTE pKey = pCtx->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData;

		UINT   uKey = pCtx->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData;

		BCRYPT_ALG_HANDLE hRsa = NULL;

		BCRYPT_KEY_HANDLE hKey = NULL;

		if( !BCryptOpenAlgorithmProvider(&hRsa, BCRYPT_RSA_ALGORITHM, NULL, 0) ) {

			ULONG uWork = 0;

			CryptDecodeObject(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
					  CNG_RSA_PUBLIC_KEY_BLOB,
					  pKey,
					  uKey,
					  0,
					  NULL,
					  &uWork
					  );

			if( uWork ) {

				PBYTE pWork = new BYTE [ uWork ];

				CryptDecodeObject(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
						  CNG_RSA_PUBLIC_KEY_BLOB,
						  pKey,
						  uKey,
						  0,
						  pWork,
						  &uWork
						  );

				BCryptImportKeyPair(hRsa,
						    NULL,
						    BCRYPT_PUBLIC_KEY_BLOB,
						    &hKey,
						    pWork,
						    uWork,
						    0
						    );

				if( hKey ) {

					BCryptFinalizeKeyPair(hKey, 0);

					UINT uBytes = GetKeyBytes(hKey);

					Data.Empty();

					Data.SetCount(uBytes);

					PBYTE pData = PBYTE(Data.GetPointer());

					UINT  uData = Data.GetCount();

					ULONG uMade = 0;

					if( !_stricmp(pMethod, "Basic128Rsa15") ) {

						Name = "http://www.w3.org/2001/04/xmlenc#rsa-1_5";

						BCryptEncrypt(hKey,
							      PBYTE(Secret.GetPointer()),
							      ULONG(Secret.GetCount  ()),
							      NULL,
							      NULL,
							      0,
							      pData,
							      uData,
							      &uMade,
							      BCRYPT_PAD_PKCS1
							      );
						}
					else {
						Name = "http://www.w3.org/2001/04/xmlenc#rsa-oaep";

						BCRYPT_OAEP_PADDING_INFO Pad;
						
						Pad.pszAlgId = BCRYPT_SHA1_ALGORITHM;
						Pad.cbLabel  = 0;
						Pad.pbLabel  = 0;

						BCryptEncrypt(hKey,
							      PBYTE(Secret.GetPointer()),
							      ULONG(Secret.GetCount  ()),
							      &Pad,
							      NULL,
							      0,
							      pData,
							      uData,
							      &uMade,
							      BCRYPT_PAD_OAEP
							      );
						}

					if( uMade == uData ) {

						BCryptDestroyKey(hKey);

						delete [] pWork;

						BCryptCloseAlgorithmProvider(hRsa, 0);

						return true;
						}

					BCryptDestroyKey(hKey);
					}

				delete [] pWork;
				}

			BCryptCloseAlgorithmProvider(hRsa, 0);
			}
		}

	return false;
	}

static UINT GetKeyBytes(BCRYPT_KEY_HANDLE hKey)
{
	UINT  uBits = 0;
	
	ULONG uDone = 0;
		
	BCryptGetProperty(hKey, BCRYPT_KEY_STRENGTH, PBYTE(&uBits), 4, &uDone, 0);

	return uBits / 8;
	}

// End of File
