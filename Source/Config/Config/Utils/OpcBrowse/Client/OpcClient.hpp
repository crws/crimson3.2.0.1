
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcClient_HPP

#define INCLUDE_OpcClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../OpcBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <opcua_string.h>
#include <opcua_datetime.h>
#include <opcua_p_types.h>
#include <opcua_thread.h>
#include <opcua_string.h>
#include <opcua_memory.h>
#include <opcua_core.h>
#include <opcua_clientproxy.h>

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COpcNode;
class COpcNodeId;
class COpcNanoModel;
class COpcReference;
class COpcVariableNode;
class COpcVariableTypeNode;

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

class COpcClient : public COpcBase, public IOpcUaClient
{
	public:
		// Constructor
		COpcClient(void);

		// Destructor
		~COpcClient(void);

		#if defined(AEON_ENVIRONMENT)

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		#endif

		// IOpcUaClient
		void OpcFree(void);
		bool OpcInit(void);
		bool OpcStop(void);
		bool OpcTerm(void);
		UINT OpcOpenDevice(PCTXT Host, PCTXT pUser, PCTXT pPass, CStringArray const &Nodes);
		bool OpcCloseDevice(UINT uDevice);
		bool OpcPingDevice(UINT uDevice);
		bool OpcBrowseDevice(UINT uDevice, CStringArray &List);
		bool OpcReadData (UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PDWORD  pData);
		bool OpcWriteData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData);

	protected:
		// Device States
		enum
		{
			stateCreateChannel,
			stateFindServers,
			stateCreateSession,
			stateActivateSession,
			stateConnected,
			};

		// Device Context
		struct CDevice
		{
			COpcClient		      * m_pClient;
			CString				m_Uri;
			CString				m_User;
			CString				m_Pass;
			UINT				m_uState;
			BOOL				m_fBroken;
			OpcUa_UInt32			m_RequestHandle;
			OpcUa_Channel			m_hChannel;
			int				m_nServers;
			OpcUa_ApplicationDescription  * m_pServers;
			BOOL				m_fSession;
			OpcUa_NodeId			m_AuthenticationToken;
			OpcUa_ByteString		m_ServerNonce;
			OpcUa_ByteString		m_ServerCertificate;
			OpcUa_EndpointDescription     * m_pServerEndpoints;
			OpcUa_Int32			m_NoOfServerEndpoints;
			OpcUa_EndpointDescription     * m_pEndpoint;
			CArray <COpcNodeId>	        m_Nodes;
			CArray <UINT>			m_Types;
			};

		// Data Members
		ULONG					m_uRefs;
		OpcUa_ByteString			m_Certificate;
		OpcUa_Key				m_PrivateKey;
		OpcUa_P_OpenSSL_CertificateStore_Config	m_PkiConfig;
		CList <CDevice *>			m_Devices;

		// Implementation
		void    InitSecurity(void);
		bool    HasTypes(CDevice *pDevice, PCUINT pList, UINT uList);
		void    FreeDiagnosticInfo(INT NoOfDiagnosticInfos, OpcUa_DiagnosticInfo *pDiagnosticInfos);
		CString Format(C3INT  d);
		CString Format(C3REAL r);

		// Device Management
		INDEX OpenDevice(PCTXT pName, PCTXT pUser, PCTXT pPass);
		BOOL  CloseDevice(INDEX Index);
		BOOL  CheckDevice(CDevice *pDevice, BOOL &fBusy);
		BOOL  AllocDevice(CDevice * &pDevice);
		BOOL  FreeDevice(CDevice *pDevice);
		BOOL  CheckResponse(CDevice *pDevice, OpcUa_StatusCode &Code, OpcUa_RequestHeader &Req, OpcUa_ResponseHeader &Res);
		BOOL  FillRequestHeader(OpcUa_RequestHeader *pHeader, CDevice *pDevice);

		// Channel Management
		BOOL CreateChannel(CDevice *pDevice);
		BOOL DeleteChannel(CDevice *pDevice);
		void ChannelEvent(CDevice *pDevice, OpcUa_Channel_Event eEvent, OpcUa_StatusCode uStatus);

		// Server Management
		BOOL FindServers(CDevice *pDevice);
		BOOL FreeServers(CDevice *pDevice);

		// Session Managemnt
		BOOL CreateSession(CDevice *pDevice);
		BOOL DeleteSession(CDevice *pDevice);
		BOOL CloseSession(CDevice *pDevice);
		BOOL ActivateSession(CDevice *pDevice, BOOL &fWrong);
		BOOL FillClientData(OpcUa_ApplicationDescription *pDesc);
		BOOL FillClientNonce(OpcUa_ByteString *pNonce);
		BOOL EncodeUserIdentity(CDevice *pDevice, OpcUa_ExtensionObject *pExt);
		BOOL EncodeAnonymousIdentity(CDevice *pDevice, OpcUa_ExtensionObject *pExt, OpcUa_UserTokenPolicy const *pPolicy);
		BOOL EncodeUserNameIdentity(CDevice *pDevice, OpcUa_ExtensionObject *pExt, OpcUa_UserTokenPolicy const *pPolicy, PCTXT pUser, PCTXT pPass);
		BOOL FindTokenPolicy(CDevice *pDevice, OpcUa_UserTokenPolicy const * &pPolicy, int TokenType);
		BOOL ScoreSecurityPolicy(OpcUa_UserTokenPolicy const * &pPolicy, PCTXT &pMethod, UINT &uScore);

		// Browse Support
		bool Browse(CDevice *pDevice, CStringArray &List);
		bool Browse(CDevice *pDevice, CStringArray &List, CString Node);
		bool BrowseNext(CDevice *pDevice, CStringArray &List, CString Root, OpcUa_ByteString const &Cont);
		bool ProcessBrowseData(CDevice *pDevice, CStringArray &List, CString Root, OpcUa_Int32 NoOfResults, OpcUa_BrowseResult *pResults);
		UINT GetWireType(UINT uType);

		// Read Support
		bool Read(CDevice *pDevice, PCUINT pList, UINT uList, PCUINT pType, PDWORD pData);
		bool ReadLength(CDevice *pDevice, UINT &uSize, OpcUa_NodeId const &Node);
		bool ReadType(CDevice *pDevice, UINT &uType, UINT &uSize, bool &fWrite, OpcUa_NodeId const &Node);
		void DecodeValue(DWORD &Data, UINT Type, OpcUa_Variant const &Value);

		// Write Support
		bool Write(CDevice *pDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData);
		void EncodeValue(OpcUa_Variant &Value, UINT Type, DWORD Data);

		// Crypto Helpers
		bool EncryptSecret(CByteArray &Data, CString &Name, PCTXT pMethod, PCBYTE pCert, UINT uCert, CByteArray const &Secret);

		// Channel Callback
		static OpcUa_StatusCode ChannelCallback( OpcUa_Channel       hChannel,
							 OpcUa_Void        * pCallbackData,
							 OpcUa_Channel_Event eEvent,
							 OpcUa_StatusCode    uStatus
							 );

		// Debugging
		void AfxTrace(PCTXT pText, ...);
	};

// End of File

#endif
