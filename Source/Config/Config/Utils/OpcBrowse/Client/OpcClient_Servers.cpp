
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Server Management

BOOL COpcClient::FindServers(CDevice *pDevice)
{
	AfxTrace("opc: findservers\n");

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, NULL);

	OpcUa_String EndpointUrl;

	OpcUa_String_Initialize(&EndpointUrl);

	OpcUa_String_AttachReadOnly(&EndpointUrl, "");

	OpcUa_Int32 nNoOfLocaleIds  = 0;

	OpcUa_Int32 nNoOfServerUris = 0;

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32                    NoOfServers = 0;
	
	OpcUa_ApplicationDescription * pServers    = NULL;

	// Service Invocation

	OpcUa_StatusCode Code = OpcUa_ClientApi_FindServers(	pDevice->m_hChannel,
								&RequestHeader,
								&EndpointUrl,
								nNoOfLocaleIds,
								NULL,
								nNoOfServerUris,
								NULL,
								&ResponseHeader,
								&NoOfServers,
								&pServers
								);

	// Response Processing

	if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

		pDevice->m_nServers = NoOfServers;
		
		pDevice->m_pServers = pServers;
		}
	else {
		for( int n = 0; n < NoOfServers; n++ ) {

			OpcUa_ApplicationDescription *pServer = pServers + n;

			OpcUa_ApplicationDescription_Clear(pServer);
			}

		OpcUa_Free(pServers);
		}

	// Parameter Release

	OpcUa_RequestHeader_Clear(&RequestHeader);

	OpcUa_String_Clear(&EndpointUrl);

	OpcUa_ResponseHeader_Clear(&ResponseHeader);

	// Check for Success

	return Code == OpcUa_Good;
	}

BOOL COpcClient::FreeServers(CDevice *pDevice)
{
	if( pDevice->m_pServers ) {

		AfxTrace("opc: freeservers\n");

		for( int n = 0; n < pDevice->m_nServers; n++ ) {

			OpcUa_ApplicationDescription *pServer = pDevice->m_pServers + n;

			OpcUa_ApplicationDescription_Clear(pServer);
			}

		OpcUa_Free(pDevice->m_pServers);

		pDevice->m_nServers = 0;

		pDevice->m_pServers = NULL;
		}

	return TRUE;
	}

// End of File
