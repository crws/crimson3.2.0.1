
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#define AssignToScalar(t, v)		\
					\
	case OpcUaId_##t:		\
	Value.Value.t = OpcUa_##t(v);	\
	break				\

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Write Support

bool COpcClient::Write(CDevice *pDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData)
{
/*	AfxTrace("opc: write\n");
*/
	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	INT               nNodes = uList;

	OpcUa_WriteValue *pNodes = OpcAlloc(nNodes, OpcUa_WriteValue);

	{ for( INT n = 0; n < nNodes; n++ ) {

		UINT              uNode = pList[n];

		OpcUa_WriteValue *pNode = pNodes + n;

		OpcUa_WriteValue_Initialize(pNode);

		pNode->AttributeId = OpcUa_Attributes_Value;

		CopyNodeId(&pNode->NodeId, &pDevice->m_Nodes[uNode]);

		pNode->Value.StatusCode     = OpcUa_Good;

		pNode->Value.Value.Datatype = BYTE(pDevice->m_Types[uNode]);

		EncodeValue(pNode->Value.Value, pType[n], pData[n]);
		} }

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32            NoOfResults         = 0;
	
	OpcUa_StatusCode    *  pResults            = NULL;

	OpcUa_Int32            NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	// Service Invocation

	OpcUa_StatusCode Code = OpcUa_ClientApi_Write(	pDevice->m_hChannel,
							&RequestHeader,
							nNodes,
							pNodes,
							&ResponseHeader,
							&NoOfResults,
							&pResults,
							&NoOfDiagnosticInfos,
							&pDiagnosticInfos
							);

	// Response Processing

	if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

		}

	// Parameter Release

	{ for( INT n = 0; n < nNodes; n++ ) {

		OpcUa_WriteValue *pNode = pNodes + n;

		OpcUa_WriteValue_Clear(pNode);
		} }

	FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

	OpcUa_Free(pNodes);

	OpcUa_Free(pResults);

	// Check for Success

	return Code == OpcUa_Good;
	}

void COpcClient::EncodeValue(OpcUa_Variant &Value, UINT Type, DWORD Data)
{
	if( Type == typeInteger ) {

		C3INT d = C3INT(Data);

		switch( Value.Datatype ) {

			AssignToScalar(Double,  d);
			AssignToScalar(Float,   d);
			AssignToScalar(Byte,    d);
			AssignToScalar(SByte,   d);
			AssignToScalar(Int16,   d);
			AssignToScalar(UInt16,  d);
			AssignToScalar(Int32,   d);
			AssignToScalar(UInt32,  d);
			AssignToScalar(Int64,   d);
			AssignToScalar(UInt64,  d);

			case OpcUaId_Boolean:

				Value.Value.Boolean = d ? 1 : 0;

				break;

			case OpcUaId_DateTime:

				TimeFromUnix(Value.Value.DateTime, Data + 9862 * 60 * 60 * 24);

				break;

			case OpcUaId_String:
				
				OpcUa_String_AttachCopy(&Value.Value.String, Format(d));

				break;
			}
		}

	if( Type == typeReal ) {

		C3REAL d = I2R(Data);

		switch( Value.Datatype ) {

			AssignToScalar(Double,  d);
			AssignToScalar(Float,   d);
			AssignToScalar(Byte,    d);
			AssignToScalar(SByte,   d);
			AssignToScalar(Int16,   d);
			AssignToScalar(UInt16,  d);
			AssignToScalar(Int32,   d);
			AssignToScalar(UInt32,  d);
			AssignToScalar(Int64,   d);
			AssignToScalar(UInt64,  d);

			case OpcUaId_Boolean:

				Value.Value.Boolean = d ? 1 : 0;

				break;

			case OpcUaId_DateTime:

				Value.Value.DateTime.t = 0;

				break;

			case OpcUaId_String:
				
				OpcUa_String_AttachCopy(&Value.Value.String, Format(d));

				break;
			}
		}

	if( Type == typeString ) {

		PCUTF s = PCUTF(Data);

		UINT  n = wcslen(s);

		PTXT  p = NULL;

		switch( Value.Datatype ) {

			AssignToScalar(Double,  wcstod(s, NULL    ));
			AssignToScalar(Float,   wcstod(s, NULL    ));
			AssignToScalar(Byte,    wcstol(s, NULL, 10));
			AssignToScalar(SByte,   wcstol(s, NULL, 10));
			AssignToScalar(Int16,   wcstol(s, NULL, 10));
			AssignToScalar(UInt16,  wcstol(s, NULL, 10));
			AssignToScalar(Int32,   wcstol(s, NULL, 10));
			AssignToScalar(UInt32,  wcstol(s, NULL, 10));
			AssignToScalar(Int64,   wcstol(s, NULL, 10));
			AssignToScalar(UInt64,  wcstol(s, NULL, 10));

			case OpcUaId_Boolean:

				if( !_wcsicmp(s, L"true") || !_wcsicmp(s, L"on") || wcstol(s, NULL, 10) > 0 ) {

					Value.Value.Boolean = 1;
					}
				else
					Value.Value.Boolean = 0;

				break;

			case OpcUaId_DateTime:

				Value.Value.DateTime.t = 0;

				break;

			case OpcUaId_String:

				p = New char [ n + 1 ];

				{ for( UINT i = 0; i < n + 1; i++ ) {

					p[i] = char(s[i]);
					} }

				OpcUa_String_AttachCopy(&Value.Value.String, p);

				delete [] p;

				break;
			}
		}
	}

// End of File
