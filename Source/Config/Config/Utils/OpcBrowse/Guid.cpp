
#include "Intern.hpp"

#include "Guid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/54DSE

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "String.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GUID Wrapper Class
//

// Null GUID

GUID const CGuid::m_Null = { 0 };

// Constructors

CGuid::CGuid(PCTXT pText)
{
	if( InitFromString(pText) ) {

		return;
		}

	memcpy(this, &m_Null, sizeof(GUID));
	}

// Assignment

CGuid const & CGuid::operator = (PCTXT pText)
{
	if( InitFromString(pText) ) {

		return ThisObject;
		}

	memcpy(this, &m_Null, sizeof(GUID));

	return ThisObject;
	}

// Attributes

CString CGuid::GetAsText(void) const
{
	CString x;

	x.AppendPrintf("{%8.8X", Data1);
	x.AppendPrintf("-%4.4X", Data2);
	x.AppendPrintf("-%4.4X", Data3);
	
	for( UINT n = 0; n < 8; n++ ) {

		if( n == 0 || n == 2 ) {

			x.AppendPrintf("-%2.2X", Data4[n]);

			continue;
			}

		if( n == 7 ) {

			x.AppendPrintf("%2.2X}",  Data4[n]);

			break;
			}

		x.AppendPrintf("%2.2X", Data4[n]);
		}

	return x;
	}

// Implementation

BOOL CGuid::InitFromString(PCTXT pText)
{
	// 0         1         2         3
	// 0123456789012345678901234567890123456789
	// 123E4567-E89B-12D3-A456-426655440000

	if( pText && *pText ) {

		if( *pText == '{' ) {

			if( !(strlen(pText) == 38 && pText[37] == '}') ) {

				return FALSE;
				}

			pText++;
			}
		else {
			if( !(strlen(pText) == 36) ) {

				return FALSE;
				}
			}

		if( !(pText[8] == '-' && pText[13] == '-' && pText[18] == '-' && pText[23] == '-') ) {

			return FALSE;
			}

		Data1 = (unsigned long ) strtoul(pText +  0, NULL, 16);
		Data2 = (unsigned short) strtoul(pText +  9, NULL, 16);
		Data3 = (unsigned short) strtoul(pText + 14, NULL, 16);

		for( UINT n = 0; n < 8; n++ ) {

			char c[3];

			int p = 19 + 2 * n + (n >= 2);

			c[0] = pText[p+0];
			c[1] = pText[p+1];
			c[2] = 0;

			Data4[n] = (unsigned char) strtoul(c, NULL, 16);
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
