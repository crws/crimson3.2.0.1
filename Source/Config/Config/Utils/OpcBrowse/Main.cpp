
#include "Intern.hpp"

// Libraries

#pragma comment(lib, "ws2_32")

#pragma comment(lib, "rpcrt4.lib")

// Static Data

global BOOL	g_fDebug   = FALSE;

global BOOL	g_fVerbose = FALSE;

static CString	m_Uri;

static CString	m_User;

static CString  m_Pass;

static CString  m_File;

// Prototypes 

void main(int nArg, char *pArg[]);
void Error(int nCode, char const *pText, ...);
bool ParseCommandLine(int nArg, char *pArg[]);

// Code

void main(int nArg, char *pArg[])
{
	if( nArg == 1 ) {

		printf("usage: OpcBrowse {-d|-v} {-u <user>> {-p <pass>} <server-uri> <output-file>\n");

		exit(2);
	}

	if( ParseCommandLine(nArg, pArg) ) {

		IOpcUaClient *pClient = Create_OpcUaClient();

		if( pClient->OpcInit() ) {

			CStringArray Nodes;

			UINT uDevice;

			if( (uDevice = pClient->OpcOpenDevice(m_Uri, m_User, m_Pass, Nodes)) ) {

				CStringArray List;

				if( pClient->OpcBrowseDevice(uDevice, List) ) {

					FILE *pFile = stdout;

					bool  fOpen = false;

					if( m_File != "." ) {

						pFile = fopen(m_File, "wt");

						fOpen = true;
					}

					if( pFile ) {

						for( UINT n = 0; n < List.GetCount(); n++ ) {

							fprintf(pFile, "%s\n", PCTXT(List[n]));
						}

						if( fOpen ) {

							fclose(pFile);

							printf("opcbrowse: found %u nodes\n", List.GetCount());
						}
					}
					else
						Error(100, "failed to create output file");
				}
				else
					Error(101, "failed to browse device");

				pClient->OpcCloseDevice(uDevice);
			}
			else
				Error(102, "failed to create device");

			pClient->OpcTerm();

			exit(0);
		}
		else
			Error(103, "failed to initialize stack");
	}
}

void Error(int nCode, char const *pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	fprintf(stderr, "opcbrowse: ");

	vfprintf(stderr, pText, pArgs);

	fprintf(stderr, "\n");

	va_end(pArgs);

	exit(nCode);
}

bool ParseCommandLine(int nArg, char *pArg[])
{
	for( int n = 1; n < nArg; n++ ) {

		if( pArg[n][0] == '-' ) {

			if( !_stricmp(pArg[n], "-d") ) {

				if( !g_fDebug ) {

					g_fDebug = TRUE;

					continue;
				}
				else {
					Error(200, "duplicate debug switch");
				}
			}

			if( !_stricmp(pArg[n], "-v") ) {

				if( !g_fVerbose ) {

					g_fDebug   = TRUE;

					g_fVerbose = TRUE;

					continue;
				}
				else {
					Error(200, "duplicate verbose switch");
				}
			}

			if( !_stricmp(pArg[n], "-p") ) {

				if( m_Pass.IsEmpty() ) {

					if( ++n == nArg ) {

						Error(200, "missing password");
					}

					m_Pass = pArg[n];

					continue;
				}
				else
					Error(200, "duplicate password");
			}

			if( !_stricmp(pArg[n], "-u") ) {

				if( m_User.IsEmpty() ) {

					if( ++n == nArg ) {

						Error(200, "missing username");
					}

					m_User = pArg[n];

					continue;
				}
				else
					Error(200, "duplicate username");
			}

			Error(200, "unknown switch");
		}

		if( m_Uri.IsEmpty() ) {

			m_Uri = pArg[n];

			continue;
		}

		if( m_File.IsEmpty() ) {

			m_File = pArg[n];

			continue;
		}
	}

	if( m_Uri.IsEmpty() || m_File.IsEmpty() ) {

		Error(200, "too few arguments");
	}

	if( !m_Uri.StartsWith("opc.tcp://") ) {

		m_Uri = "opc.tcp://" + m_Uri;
	}

	if( m_Uri.Mid(10).Find(':') == NOTHING ) {

		m_Uri = m_Uri + ":4840";
	}

	return true;
}

// End of File
