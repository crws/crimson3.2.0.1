
//////////////////////////////////////////////////////////////////////////
//
// Signature Verification Application
//
// Copyright (c) 1993-2006 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_KEYS_HPP

#define INCLUDE_KEYS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Public Keys
//

static BYTE bKey512[] = {

	#include "keys\pub0512.hpp"

	};

static BYTE bKey768[] = {

	#include "keys\pub0768.hpp"

	};

static BYTE bKey1024[] = {

	#include "keys\pub1024.hpp"

	};

// End of File

#endif
