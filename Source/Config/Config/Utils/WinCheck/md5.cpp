
//////////////////////////////////////////////////////////////////////////
//
// C2 Log Signature Check Utility
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MD5 Object Wrapper
//

// Constructors

CMD5Hash::CMD5Hash(HCRYPTPROV hProv)
{
	m_hHash = NULL;
	
	CryptCreateHash( hProv,
			 CALG_MD5,        
		         0,      
		         0,
		         &m_hHash
		         );
	}

// Destructor

CMD5Hash::~CMD5Hash(void)
{
	if( m_hHash ) {
	
		CryptDestroyHash(m_hHash);
		
		m_hHash = NULL;
		}
	}

// Conversion

CMD5Hash::operator HCRYPTHASH (void) const
{
	return m_hHash;
	}

// Attributes

BOOL CMD5Hash::IsValid(void) const
{
	return m_hHash != NULL;
	}

// Hashing

BOOL CMD5Hash::HashData(PBYTE pData, DWORD dwLen)
{
	return CryptHashData( m_hHash,
			      pData,
			      dwLen, 
			      0
			      );
	}

BOOL CMD5Hash::HashFile(PCTXT pSrcFile)
{
	FILE *pFile = _wfopen(pSrcFile, L"rb");

	if( pFile ) {

		BYTE bBuf[512];

		BOOL fError = FALSE;
		
		while( !fError ) {

			UINT uCount = fread(bBuf, 1, sizeof(bBuf), pFile);

			if( uCount ) {

				fError = !HashData(bBuf, uCount);

				continue;
				}

			break;
			}
		
		fclose(pFile);

		return !fError;
		}

	return FALSE;
	}

BOOL CMD5Hash::Get(MD5HASH &Hash) const
{
	DWORD dwLen = sizeof(Hash);
	
	return CryptGetHashParam( m_hHash,
				  HP_HASHVAL,
				  (PBYTE) &Hash,
				  &dwLen,
				  0
				  );
	}

BOOL CMD5Hash::Set(MD5HASH const &Hash)
{
	return CryptSetHashParam( m_hHash,
				  HP_HASHVAL,
				  (PBYTE) &Hash,
				  0
				  );
	}

// End of File
