
#include "intern.hpp"

#include "check.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Signature Verification Application
//
// Copyright (c) 1993-2006 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CCheckApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Dynamic Class

AfxImplementDynamicClass(CCheckApp, CThread);

// Constructor

CCheckApp::CCheckApp(void)
{
	}

// Destructor

CCheckApp::~CCheckApp(void)
{
	}

// Overridables

BOOL CCheckApp::OnInitialize(void)
{
	CCheckFrameWnd *pWnd = New CCheckFrameWnd;

	pWnd->Create( CString(IDS_APP_CAPTION),
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      CRect(),
		      AfxNull(CWnd),
		      CMenu(L"HeadMenu"),
		      NULL
		      );

	if( afxModule->GetArgumentCount() <= 1 ) {

		pWnd->ShowWindow(afxModule->GetShowCommand());
		
		afxThread->SetStatusText(L"Welcome to WinCheck");

		if( afxModule->GetArgumentCount() == 1 ) {

			CString File = afxModule->GetArgument(0);

			pWnd->Validate(File);
			}

		return TRUE;
		}

	pWnd->Error(L"Invalid command line.");

	afxMainWnd->PostMessage(WM_CLOSE);

	return FALSE;
	}

BOOL CCheckApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CCheckApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CCheckApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CCheckApp)
	};

// Message Handlers

void CCheckApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CCheckApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->PostMessage(WM_CLOSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CCheckApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_EXIT ) {

		Src.EnableItem(TRUE);
			
		return TRUE;
		}
		
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

// Dynamic Class

AfxImplementDynamicClass(CCheckFrameWnd, CMainWnd);

// Constructor

CCheckFrameWnd::CCheckFrameWnd(void)
{
	m_Accel.Create(L"TestMenu");

	m_Icon.Create(L"WinCheck");

	m_pView = New CCheckViewWnd;
	}

// Destructor

CCheckFrameWnd::~CCheckFrameWnd(void)
{
	}

// Class Definition

PCTXT CCheckFrameWnd::GetDefaultClassName(void) const
{
	return L"CheckFrameWnd";
	}

BOOL CCheckFrameWnd::GetClassDetails(WNDCLASSEX &Class) const
{
	if( CWnd::GetClassDetails(Class) ) {

		Class.hIcon   = m_Icon.GetHandle();

		Class.hIconSm = m_Icon.GetHandle();

		return TRUE;
		}

	return FALSE;
	}

// Message Map

AfxMessageMap(CCheckFrameWnd, CMainWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_CLOSE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CCheckFrameWnd)
	};

// Accelerators

BOOL CCheckFrameWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CCheckFrameWnd::OnClose(void)
{
	DestroyWindow(FALSE);
	}

// Command Handlers

BOOL CCheckFrameWnd::OnCommandExecute(UINT uID)
{
	if( uID >= IDM_FILE_RECENT && uID < IDM_FILE_RECENT + 10 ) {

		CString File = m_Recent.GetFileFromID(uID);

		if( Validate(File) ) {

			m_Recent.AddFile   (File);
			}
		else
			m_Recent.RemoveFile(File);
		
		return TRUE;
		}

	if( uID == IDM_FILE_OPEN ) {

		COpenFileDialog Dlg;

		Dlg.LoadLastPath(L"Open");

		Dlg.SetCaption(L"Check File");

		Dlg.SetFilter(L"Log Files (*.csv)|*.csv");

		if( Dlg.Execute(ThisObject) ) {

			CString File = Dlg.GetFilename();

			if( Validate(File) ) {

				Dlg.SaveLastPath(L"Open");

				m_Recent.AddFile(File);
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCheckFrameWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_FILE_RECENT && uID < IDM_FILE_RECENT + 10 ) {
	
		RouteControl(IDM_FILE_OPEN, Src);
		
		return TRUE;
		}

	if( uID == IDM_FILE_OPEN ) {
			
		Src.EnableItem(TRUE);
		
		return TRUE;
		}

	if( uID == IDM_FILE_PROPERTIES ) {
			
		Src.EnableItem(TRUE);
		
		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CCheckFrameWnd::Validate(CFilename File)
{
	FILE *pFile = _wfopen(File, L"rb");

	if( pFile ) {

		fclose(pFile);

		((CCheckViewWnd *) m_pView)->Validate(File);

		return TRUE;
		}

	Error(L"The file cannot be opened for reading.");

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test View Window
//

// Runtime Class

AfxImplementRuntimeClass(CCheckViewWnd, CViewWnd);

// Constructor

CCheckViewWnd::CCheckViewWnd(void)
{
	m_pList = New CCheckListBox;

	m_uLine = 0;
	}

// Destructor

CCheckViewWnd::~CCheckViewWnd(void)
{
	}

// Operations

void CCheckViewWnd::Validate(CFilename File)
{
	m_File = File;

	m_pList->SetRedraw(FALSE);

	m_pList->ResetContent();

	LoadFile();

	m_pList->SetRedraw(TRUE);

	m_pList->Invalidate(TRUE);
	}

// Message Map

AfxMessageMap(CCheckViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)

	AfxMessageEnd(CCheckViewWnd)
	};

// Message Handlers

void CCheckViewWnd::OnPostCreate(void)
{
	CRect Rect = GetClientRect();

	m_pList->Create( LBS_OWNERDRAWFIXED | LBS_NOINTEGRALHEIGHT | LBS_HASSTRINGS | WS_VSCROLL,
			 Rect,
			 m_hWnd,
			 IDVIEW
			 );

	m_pList->SetFont(afxFont(Fixed));

	m_pList->ShowWindow(SW_SHOW);

	m_pList->SetFocus();
	}

void CCheckViewWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		CRect Rect = GetClientRect();

		m_pList->MoveWindow(Rect, FALSE);

		m_pList->Invalidate(TRUE);
		}
	}

void CCheckViewWnd::OnSetFocus(CWnd &Old)
{
	m_pList->SetFocus();

	ShowStatus();
	}

// Implementation

BOOL CCheckViewWnd::LoadFile(void)
{
	m_Error.Empty();

	FILE *pFile = _wfopen(m_File, L"rb");
		
	if( pFile ) {

		if( !LoadFile(pFile) ) {

			afxMainWnd->SetWindowText(CPrintf(L"WinCheck - %s - INVALID", m_File));

			ShowStatus();
	
			m_pList->SetErrorLine(m_uLine);

			CString Text;
			
			Text += L"The file cannot be verified.\n\n";

			Text += m_Error + L".";

			Error(Text);
			}
		else {
			afxMainWnd->SetWindowText(CPrintf(L"WinCheck - %s", m_File));

			ShowStatus();

			m_pList->SetErrorLine(NOTHING);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCheckViewWnd::LoadFile(FILE *pFile)
{
	if( m_Crypto.Open() ) {

		KEY_SIG Sig = { 0, { 0 } };

		char sLine[1024];

		BYTE bData[32768];

		UINT uLine     = 0;

		UINT uPtr      = 0;

		BOOL fOkay     = TRUE;

		UINT uLastMono = 0;

		UINT uLastMAC  = 0;

		m_uLine	       = 0;

		while( !feof(pFile) ) {

			UINT uMax   = sizeof(sLine) - 1;

			sLine[0]    = 0;

			sLine[uMax] = 0;

			fgets(sLine, uMax, pFile);

			UINT uLen = strlen(sLine);

			if( uLen == uMax - 1 ) {

				uLine   = uLine + 1;

				m_Error = "Line too long";

				return FALSE;
				}

			if( uLen ) {

				if( !m_Error.IsEmpty() ) {

					AddString(sLine, NOTHING);

					continue;
					}

				if( ++uLine > 1 ) {

					PSTR pComma = strrchr(sLine, ',');

					if( pComma ) {

						AddString(sLine, pComma - sLine);

						BYTE bType = BYTE(strtoul(pComma +  1, NULL, 16));

						UINT uMono = UINT(strtoul(pComma +  4, NULL, 16));

						UINT uMAC  = UINT(strtoul(pComma + 13, NULL, 16));

						if( uLine > 2 && uMono <= uLastMono ) {

							m_Error = "Non-monotonic sequence numbers";

							continue;
							}

						if( uLine > 2 && uMAC  != uLastMAC  ) {

							m_Error = "Non-matching MAC IDs";

							continue;
							}

						uLastMono = uMono;

						uLastMAC  = uMAC;

						if( bType == 0x00 ) {

							if( uPtr + uLen > sizeof(bData) ) {

								m_Error = "Too long without signature";

								continue;
								}

							memcpy(bData + uPtr, sLine, uLen);

							uPtr += uLen;

							fOkay = FALSE;

							continue;
							}

						if( bType == 0x01 ) {

							UINT uCopy = pComma - sLine;

							PSTR pSig  = pComma + 20;

							memcpy(bData + uPtr, sLine, uCopy);

							uPtr += uCopy;

							ReadSig(pSig, Sig);

							if( !m_hKey ) {

								m_Error = "Unknown key length";

								continue;
								}

							if( m_Crypto.SignTest(bData, uPtr, m_hKey, Sig) ) {

								uPtr    = uLen - uCopy - 1;

								fOkay   = TRUE;

								m_uLine = uLine;

								memmove(bData, pComma + 1, uPtr);

								continue;
								}

							m_Error = "Bad signature";

							continue;
							}

						m_Error = "Unknown sig block type";

						continue;
						}

					AddString(sLine, NOTHING);

					m_Error = L"Missing sig block";

					continue;
					}
				else {
					AddString(sLine, NOTHING);

					memcpy(bData + uPtr, sLine, uLen);

					uPtr += uLen;
					}
				}
			}

		if( m_Error.IsEmpty() && !fOkay ) {

			m_Error = L"Unsigned residue";

			return FALSE;
			}

		return m_Error.IsEmpty();
		}

	m_Error = m_Crypto.GetError();

	return FALSE;
	}

void CCheckViewWnd::ReadSig(PSTR pData, KEY_SIG &Sig)
{
	UINT n = 0;

	while( isxdigit(pData[0]) && isxdigit(pData[1]) ) {

		BYTE n1 = FromHex(pData[0]);
		
		BYTE n2 = FromHex(pData[1]);

		Sig.bData[n++] = BYTE((n1<<4)|n2);

		pData += 2;
		}

	Sig.uLen = n;

	m_hKey   = m_Crypto.GetKeyByLength(n * 8);

	m_uBits  = min(m_uBits, n * 8);
	}

BYTE CCheckViewWnd::FromHex(char c)
{
	if( c >= '0' && c <= '9' ) return BYTE(c - '0' + 0x00);
	
	if( c >= 'A' && c <= 'F' ) return BYTE(c - 'A' + 0x0A);
	
	if( c >= 'a' && c <= 'f' ) return BYTE(c - 'a' + 0x0A);

	return 0;
	}

UINT CCheckViewWnd::HostToMotor(UINT x)
{
	UINT z = x;

	#if defined(_M_IX86)

	((PBYTE)&z)[0] = PBYTE(&x)[3];
	((PBYTE)&z)[1] = PBYTE(&x)[2];
	((PBYTE)&z)[2] = PBYTE(&x)[1];
	((PBYTE)&z)[3] = PBYTE(&x)[0];

	#endif

	return z;
	}

void CCheckViewWnd::ShowStatus(void)
{
	if( m_Error.IsEmpty() ) {

		afxThread->SetStatusText(L"The current file is valid.");

		return;
		}

	afxThread->SetStatusText(L"THE CURRENT FILE CANNOT BE VERIFIED.");
	}

void CCheckViewWnd::AddString(PCSTR pText, UINT uCount)
{
	UINT uCopy = min(uCount, strlen(pText));

	PTXT pCopy = New WCHAR [ uCopy + 1];

	UINT uPos;

	for( uPos = 0; uPos < uCopy; uPos++ ) {

		pCopy[uPos] = pText[uPos];
		}

	pCopy[uPos] = 0;

	m_pList->AddString(pCopy);

	delete [] pCopy;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test List Box
//

// Runtime Class

AfxImplementRuntimeClass(CCheckListBox, CListBox);

// Constructor

CCheckListBox::CCheckListBox(void)
{
	m_uLine = NOTHING;
	}

// Destructor

CCheckListBox::~CCheckListBox(void)
{
	}

// Operations

void CCheckListBox::SetErrorLine(UINT uLine)
{
	if( (m_uLine = uLine) < NOTHING ) {

		SetCurSel  (m_uLine);

		SetTopIndex(m_uLine);
		}
	else {
		SetCurSel  (0);

		SetTopIndex(0);
		}

	SetFocus();
	}

// Message Map

AfxMessageMap(CCheckListBox, CListBox)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxMessageEnd(CCheckListBox)
	};

// Message Handlers

void CCheckListBox::OnPostCreate(void)
{
	}

void CCheckListBox::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Measure)
{
	CDC DC(ThisObject);

	DC.Select(afxFont(Fixed));
	
	Measure.itemHeight = DC.GetTextExtent(L"XXX").cy;

	m_Font.Create(DC, L"WingDings", CSize(0, Measure.itemHeight), FALSE);

	DC.Deselect();
	}

void CCheckListBox::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw)
{
	CDC     DC(Draw.hDC);

	CRect   Rect = CRect(Draw.rcItem);

	CString Text = GetText(Draw.itemID);

	CPrintf Line = CPrintf("%6u ", 1 + Draw.itemID);

	if( TRUE ) {

		DC.Select(afxFont(Fixed));

		DC.SetTextColor(afxColor(3dShadow));

		DC.SetBkColor(afxColor(WindowBack));

		DrawText(DC, Rect, Line);
		}

	if( TRUE ) {

		DC.Replace(m_Font);

		if( Draw.itemState & ODS_SELECTED ) {

			DC.SetTextColor(afxColor(BLUE));
			}
		else
			DC.SetTextColor(afxColor(WindowBack));

		DrawText(DC, Rect, L"\x0E8 ");

		if( Draw.itemID >= m_uLine ) {

			DC.SetTextColor(afxColor(RED));
		
			DC.SetBkColor(afxColor(WindowBack));

			DrawText(DC, Rect, L"\x04C ");
			}
		else {
			DC.SetTextColor(CColor(0,128,0));
		
			DC.SetBkColor(afxColor(WindowBack));

			DrawText(DC, Rect, L"\x04A ");
			}
		}

	if( TRUE ) {

		DC.Replace(afxFont(Fixed));
		
		if( Draw.itemID >= m_uLine ) {

			DC.SetTextColor(afxColor(RED));
		
			DC.SetBkColor(afxColor(WindowBack));
			}
		else {
			DC.SetTextColor(afxColor(WindowText));
		
			DC.SetBkColor(afxColor(WindowBack));
			}

		DC.ExtTextOut(Rect.GetTopLeft(), ETO_CLIPPED | ETO_OPAQUE, Rect, Text);
		}
	
	DC.Deselect();
	}

// Implementation

void CCheckListBox::DrawText(CDC &DC, CRect &Rect, CString Text)
{
	CSize  Size = DC.GetTextExtent(Text);

	CSize  Fill = CSize(Size.cx, Rect.cy());

	CPoint Pos  = Rect.GetTopLeft();

	DC.ExtTextOut( Pos,
		       ETO_CLIPPED | ETO_OPAQUE,
		       CRect(Pos, Fill),
		       Text
		       );

	Rect.left += Size.cx;
	}

// End of File
