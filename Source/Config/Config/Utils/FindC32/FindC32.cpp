
#include "Intern.hpp"

// Constants

#define ET_REDLION	0x8900

#define ET_SUBTYPE	0x8001

#define ET_VERSION	0x0001

// Network Structure

struct CNet
{
	DWORD	addr;
	DWORD	mask;
};

// NIC Structure

struct CNic
{
	string   name;
	BYTE     mac[6];
	pcap_t * port;
};

// Device Structure

struct CDev
{
	string	model;
	string	serial;
	string	address;
	string	mdns;
	bool	emulator;
	string  mac;
	int	port;
};

// Static Data

static	UINT			m_uMode = 0;

static	string			m_Mac;

static	vector<CNic>		m_Nics;

static	vector<CNet>		m_Nets;

static	map<string, CDev>	m_Devs;

static	char			m_sError[256];

// Prototypes

static	void	ShowUsage(void);
static	void	Error(char const *pText, ...);
static	bool	ParseCommandLine(int nArg, char *pArg[]);
static	bool	FindNics(void);
static	bool	OpenPorts(void);
static	void	ClosePorts(void);
static	void	SendPolls(void);
static	void	SendIdentify(PCBYTE pMac);
static	bool	Dispatch(void);
static	void	PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData);

// Code

int main(int nArg, char *pArg[])
{
	if( ParseCommandLine(nArg, pArg) ) {

		if( FindNics() ) {

			if( OpenPorts() ) {

				if( m_uMode == 1 ) {

					for( UINT p = 0; p < 2; p++ ) {

						SendPolls();

						for( DWORD t1 = GetTickCount(); GetTickCount() < t1 + 500; ) {

							if( !Dispatch() ) {

								Sleep(10);
							}
						}
					}

					if( m_Devs.empty() ) {

						Error("no devices found");
					}

					for( auto const &dev : m_Devs ) {

						auto const &d = dev.second;

						printf("%s,%u,%s,%s,%s,%s,TCP %u\n",
						       d.model.c_str(),
						       d.emulator ? 1 : 0,
						       d.serial.c_str(),
						       d.mac.c_str(),
						       d.address.c_str(),
						       d.mdns.c_str(),
						       d.port
						);
					}
				}

				if( m_uMode == 2 ) {

					BYTE bMac[6];

					char const *p = m_Mac.c_str();

					for( size_t n = 0; n < sizeof(bMac); n++ ) {

						char *e = NULL;

						bMac[n] = BYTE(strtoul(p, &e, 16));

						if( n == sizeof(bMac) - 1 ) {

							if( *e ) {

								Error("invalid mac address");
							}
						}
						else {
							if( *e != '-' && *e != ':' ) {

								Error("invalid mac address");
							}
						}

						p = e + 1;
					}

					SendIdentify(bMac);
				}

				ClosePorts();

				return 0;
			}
		}

		Error("failed to communicate");
	}

	ShowUsage();

	return 2;
}

static void ShowUsage(void)
{
	printf("Crimson 3.2 Device Locator  Version 1.10\n");
	printf("Copyright (c) 2020 Red Lion Controls Inc\n");
	printf("\n");
	printf("usage: FindC32 -f        List all C3.2 devices\n");
	printf("       FindC32 -i <mac>  Identify device via LEDs\n");

	exit(2);
}

static void Error(char const *pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	fprintf(stderr, "FindC32: ");

	vfprintf(stderr, pText, pArgs);

	fprintf(stderr, "\n");

	va_end(pArgs);

	exit(1);
}

static bool ParseCommandLine(int nArg, char *pArg[])
{
	if( nArg >= 2 ) {

		for( int n = 1; n < nArg; n++ ) {

			if( pArg[n][0] == '-' ) {

				switch( tolower(pArg[n][1]) ) {

					case 'f':
					{
						if( m_uMode ) {

							Error("repeated operation");
						}

						m_uMode = 1;
					}
					break;

					case 'i':
					{
						if( m_uMode ) {

							Error("repeated operation");
						}

						m_uMode = 2;
					}
					break;

					default:
					{
						Error("unknown switch");
					}
					break;
				}
			}
			else {
				if( !m_Mac.empty() ) {

					Error("extra characters on line");
				}

				m_Mac = pArg[n];
			}
		}

		if( !m_uMode ) {

			Error("missing operation");
		}

		if( m_uMode == 1 && m_Mac.size() ) {

			Error("unexpected mac address");
		}

		if( m_uMode == 2 && m_Mac.empty() ) {

			Error("missing mac address");
		}

		return true;
	}

	return false;
}

static bool FindNics(void)
{
	DWORD		dwSize = 65536;

	IP_ADAPTER_INFO *pData = (IP_ADAPTER_INFO *) malloc(dwSize);

	if( GetAdaptersInfo(pData, &dwSize) == ERROR_SUCCESS ) {

		IP_ADAPTER_INFO *pWalk = pData;

		while( pWalk ) {

			if( pWalk->Type == MIB_IF_TYPE_ETHERNET ) {

				if( pWalk->AddressLength == 6 ) {

					vector<CNet> nets;

					for( auto pScan = &pWalk->IpAddressList; pScan; pScan = pScan->Next ) {

						DWORD addr = 0;

						DWORD mask = 0;

						if( pScan->IpAddress.String ) {

							InetPton(AF_INET, pScan->IpAddress.String, &addr);
						}

						if( pScan->IpMask.String ) {

							InetPton(AF_INET, pScan->IpMask.String, &mask);
						}

						if( addr && mask ) {

							auto &net = *nets.emplace(nets.end());

							net.addr = addr;

							net.mask = mask;
						}
					}

					if( !nets.empty() ) {

						auto &nic = *m_Nics.emplace(m_Nics.end());

						nic.name = string("\\Device\\NPF_") + pWalk->AdapterName;

						nic.port = NULL;

						memcpy(nic.mac, pWalk->Address, sizeof(nic.mac));

						m_Nets.insert(m_Nets.end(), nets.begin(), nets.end());
					}
				}
			}

			pWalk = pWalk->Next;
		}

		free(pData);

		if( !m_Nics.empty() && !m_Nets.empty() ) {

			return true;
		}
	}

	return false;
}

static bool OpenPorts(void)
{
	bool hit = false;

	for( auto &nic : m_Nics ) {

		pcap_t *port = pcap_open_live(nic.name.c_str(), 65536, true, 0, m_sError);

		if( port ) {

			pcap_setnonblock(port, 1, m_sError);

			bpf_program code;

			CPrintf Filter("ether proto 0x%X", ET_REDLION);

			if( pcap_compile(port, &code, PSTR(PCSTR(Filter)), 1, 0) >= 0 ) {

				pcap_setfilter(port, &code);

				pcap_freecode(&code);

				nic.port = port;

				hit = true;
			}
			else {
				pcap_close(port);
			}
		}
	}

	return hit;
}

static void ClosePorts(void)
{
	for( auto const &nic : m_Nics ) {

		if( nic.port ) {

			pcap_close(nic.port);
		}
	}
}

static void SendPolls(void)
{
	BYTE bData[] = {

		0x01, 0x00, 0x5E, 0x0D, 0x11, 0x13, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		HIBYTE(ET_REDLION), LOBYTE(ET_REDLION),
		HIBYTE(ET_SUBTYPE), LOBYTE(ET_SUBTYPE),
		HIBYTE(ET_VERSION), LOBYTE(ET_VERSION),
		0x01, 0x01
	};

	for( auto const &nic : m_Nics ) {

		if( nic.port ) {

			memcpy(bData + 6, nic.mac, sizeof(nic.mac));

			pcap_sendpacket(nic.port, bData, sizeof(bData));
		}
	}
}

static void SendIdentify(PCBYTE pMac)
{
	BYTE bData[] = {

		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		HIBYTE(ET_REDLION), LOBYTE(ET_REDLION),
		HIBYTE(ET_SUBTYPE), LOBYTE(ET_SUBTYPE),
		HIBYTE(ET_VERSION), LOBYTE(ET_VERSION),
		0x01, 0x03
	};

	for( auto const &nic : m_Nics ) {

		if( nic.port ) {

			memcpy(bData + 0, pMac, sizeof(nic.mac));

			memcpy(bData + 6, nic.mac, sizeof(nic.mac));

			pcap_sendpacket(nic.port, bData, sizeof(bData));
		}
	}
}

static bool Dispatch(void)
{
	bool hit = false;

	for( auto const &nic : m_Nics ) {

		if( nic.port ) {

			if( pcap_dispatch(nic.port, 1, PacketProc, NULL) == 0 ) {

				hit = true;
			}
		}
	}

	return hit;
}

static void PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData)
{
	if( pInfo->len >= 20 ) {

		BYTE bSig[] = { HIBYTE(ET_SUBTYPE), LOBYTE(ET_SUBTYPE),
				HIBYTE(ET_VERSION), LOBYTE(ET_VERSION),
		};

		if( !memcmp(pData+14, bSig, sizeof(bSig)) ) {

			BYTE bSize = pData[18];

			BYTE pCode = pData[19];

			if( pCode == 2 ) {

				CDev dev;

				vector<string> ips;

				dev.emulator = false;

				dev.port     = 789;

				PCBYTE pScan = pData + 20;

				PCBYTE pInit = pScan;

				while( pScan < pInit + bSize ) {

					BYTE bType = *pScan++;

					BYTE bSkip = *pScan++;

					switch( bType ) {

						case 1:
						{
							if( dev.serial.empty() ) {

								dev.serial = string(PCSTR(pScan), bSkip);
							}
						}
						break;

						case 2:
						{
							DWORD test = PDWORD(pScan)[0];

							for( auto const &net : m_Nets ) {

								if( (net.addr & net.mask) == (test & net.mask) ) {

									char text[32];

									InetNtop(AF_INET, &test, text, sizeof(text));

									ips.push_back(text);
								}
							}
						}
						break;

						case 3:
						{
							if( dev.mdns.empty() ) {

								dev.mdns = string(PCSTR(pScan), bSkip) + ".local";
							}
						}
						break;

						case 4:
						{
							if( dev.model.empty() ) {

								dev.model = string(PCSTR(pScan), bSkip);
							}
						}
						break;

						case 5:
						{
							dev.emulator = true;
						}
						break;

						case 6:
						{
							dev.port = MAKEWORD(pScan[0], pScan[1]);
						}
						break;
					}

					pScan += bSkip;
				}

				if( !dev.model.empty() && !dev.serial.empty() ) {

					for( auto const &ip : ips ) {

						dev.address = ip;

						dev.mac = CPrintf("%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X",
								  pData[6],
								  pData[7],
								  pData[8],
								  pData[9],
								  pData[10],
								  pData[11]
						);

						string key = dev.model + ' ' + dev.serial + ' ' + ip;

						m_Devs.insert(make_pair(key, dev));
					}
				}
			}
		}
	}
}

// End of File
