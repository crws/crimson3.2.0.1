
#pragma once

#define	_WIN32_WINNT		_WIN32_WINNT_WIN7

#define	WINVER			_WIN32_WINNT_WIN7

#define WIN32_LEAN_AND_MEAN	TRUE

#define	NO_STRICT		TRUE

#include <Windows.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>

#include <malloc.h>

#include <string>
#include <vector>
#include <map>

using namespace std;

#include "Printf.hpp"

#include "pcap/include/pcap.h"

#pragma comment(lib, "Ws2_32.lib")

#pragma comment(lib, "wpcap.lib")

#pragma comment(lib, "Iphlpapi.lib")

typedef BYTE const * PCBYTE;
