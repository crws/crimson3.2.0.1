
//////////////////////////////////////////////////////////////////////////
//
// C2 Log Signature Check Utility
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SIGCHECK

#define INCLUDE_SIGCHECK

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#define  _CRT_SECURE_NO_WARNINGS

#include <windows.h>

#include <wincrypt.h>

#include <stdio.h>

//////////////////////////////////////////////////////////////////////////
//
// Supported Key Sizes
//

#define KEY_MAX		1024

#define SIG_MAX		((KEY_MAX + 7) / 8)

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	global		/**/

//////////////////////////////////////////////////////////////////////////
//
// Custom Types
//

typedef char *		PTXT;

typedef const char *	PCTXT;

//////////////////////////////////////////////////////////////////////////
//
// Supported Keys
//

enum RSAKeyType
{
	rsaKey512,
	rsaKey768,
	rsaKey1024
	};

//////////////////////////////////////////////////////////////////////////
//
// Key BLOB
//

struct KEY_BLOB
{
	PUBLICKEYSTRUC	pub;
	RSAPUBKEY	rsa;
	BYTE		modulus[KEY_MAX / 8];
	};

//////////////////////////////////////////////////////////////////////////
//
// Signature
//

struct KEY_SIG
{
	UINT uLen;
	BYTE bData[SIG_MAX];
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Miscrosoft Crypto API Object
//

struct MD5HASH
{
	unsigned char Data[16];
	};

//////////////////////////////////////////////////////////////////////////
//
// Miscrosoft Crypto API Object
//

class CMD5Hash
{
	public:
		// Constructur
		CMD5Hash(HCRYPTPROV hProv);

		// Destructor
		~CMD5Hash(void);

		// Conversion
		operator HCRYPTHASH (void) const; 

		// Attributes
		BOOL IsValid(void) const;

		// Hashing
		BOOL HashData(PBYTE pData, DWORD dwLen); 
		BOOL HashFile(PCTXT pSrcFile);
		BOOL Get(MD5HASH &Hash) const;
		BOOL Set(MD5HASH const &Hash);

	protected:
		// Data
		HCRYPTHASH m_hHash;
	};

//////////////////////////////////////////////////////////////////////////
//
// Miscrosoft Crypto API Object
//

class CMSCrypto
{
	public:
		// Constructor
		CMSCrypto(void);

		// Destructor
		~CMSCrypto(void);

		// Conversion
		operator HCRYPTPROV (void) const; 

		// Attributes
		BOOL       IsValid(void) const;
		HCRYPTPROV GetProvider(void) const;
		HCRYPTKEY  GetKey(RSAKeyType Type) const;
		HCRYPTKEY  GetKeyByLength(UINT uLength) const;
		
		// Management
		BOOL Open(void);
		void Close(void);

		// Operations
		BOOL HashFile(PCTXT pSrcFile, MD5HASH &Value);
		BOOL SignTest(PCTXT pSrcFile);
		BOOL SignTest(PCTXT pSrcFile, PCTXT pSigFile);
		BOOL SignTest(PCTXT pFileName, HCRYPTKEY hKey, KEY_SIG &Sig);
		BOOL SignTest(PBYTE pData, UINT uSize, HCRYPTKEY hKey, KEY_SIG &Sig);

	protected:
		// Data
		BOOL       m_fOpen;
		HCRYPTPROV m_hProv;
		HCRYPTKEY  m_hKey512;
		HCRYPTKEY  m_hKey768;
		HCRYPTKEY  m_hKey1024;
		
		// Impelementation
		BOOL InitCSP(void);
		void FreeCSP(void);
		
		// Keys
		BOOL InitKeys(void);
		void FreeKeys(void);
		void FreeKey(HCRYPTKEY &Key);
		BOOL InitKey512(void);
		BOOL InitKey768(void);
		BOOL InitKey1024(void);
		BOOL MakeKey(RSAKeyType);
		BOOL MakeKey(HCRYPTKEY &Key, UINT uSize);

		// Import/Export
		BOOL ImportKey(HCRYPTKEY &Key, PBYTE pData, UINT uSize);
		BOOL ExportKey(HCRYPTKEY  Key, KEY_BLOB &Blob, DWORD &dwLen);

		// Signature Helpers
		BOOL ReadSig (PCTXT pFilename, KEY_SIG &Sig);
		BOOL WriteSig(PCTXT pFilename, KEY_SIG const &Sig);

		// BLOB Helpers
		BOOL ReadBlob (PCTXT pFilename, KEY_BLOB &Blob);
		BOOL WriteBlob(PCTXT pFilename, KEY_BLOB const &Blob);
	};

// End of File

#endif
