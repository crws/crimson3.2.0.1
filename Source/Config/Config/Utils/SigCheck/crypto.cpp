
//////////////////////////////////////////////////////////////////////////
//
// C2 Log Signature Check Utility
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

#include "keys.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Microsoft Crypto API Object
//

// Constructor

CMSCrypto::CMSCrypto(void)
{
	m_fOpen    = FALSE;

	m_hProv    = NULL;

	m_hKey512  = NULL;
	
	m_hKey768  = NULL;

	m_hKey1024 = NULL;
	}

// Destructor

CMSCrypto::~CMSCrypto(void)
{
	Close();
	}

// Conversion

CMSCrypto::operator HCRYPTPROV (void) const
{
	return m_hProv;
	}

// Attributes

BOOL CMSCrypto::IsValid(void) const
{
	return m_fOpen;
	}

HCRYPTPROV CMSCrypto::GetProvider(void) const
{
	return m_hProv;
	}

HCRYPTKEY CMSCrypto::GetKey(RSAKeyType Type) const
{
	switch( Type ) {
	
		case rsaKey512:  return m_hKey512;

		case rsaKey768:  return m_hKey768;

		case rsaKey1024: return m_hKey1024;
		}
	
	return NULL;
	}

HCRYPTKEY CMSCrypto::GetKeyByLength(UINT uLength) const
{
	switch( uLength ) {
	
		case  512: return m_hKey512;

		case  768: return m_hKey768;

		case 1024: return m_hKey1024;
		}
	
	return NULL;
	}

// Management

BOOL CMSCrypto::Open(void)
{
	if( !m_fOpen ) {

		if( InitCSP() ) {

			if( InitKeys() ) {

				m_fOpen = TRUE;
				
				return TRUE;
				}
			else {
				FreeCSP();

				return FALSE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

void CMSCrypto::Close(void)
{
	FreeKeys();

	FreeCSP();

	m_fOpen = FALSE;
	}

// Operations

BOOL CMSCrypto::HashFile(PCTXT pSrcFile, MD5HASH &Value)
{
	CMD5Hash MD5(m_hProv);

	return MD5.HashFile(pSrcFile) && MD5.Get(Value);
	}

BOOL CMSCrypto::SignTest(PCTXT pSrcFile)
{
	UINT uLen  = strlen(pSrcFile);

	PTXT pFind = PTXT(strrchr(pSrcFile, '.'));

	if( pFind ) {
	
		uLen = pFind - pSrcFile;
		}
	
	if( uLen ) {
	
		UINT uSigLen  = uLen + 4; 
		
		PTXT pSigFile = new char [ uSigLen + 1 ];

		if( pSigFile ) {

			memcpy(pSigFile, pSrcFile, uLen);

			memcpy(pSigFile + uLen, ".sig", 4);

			pSigFile[ uSigLen ] = 0; 

			BOOL fResult = SignTest(pSrcFile, pSigFile);

			delete [] pSigFile;

			return fResult;
			}
		}

	return FALSE;
	}

BOOL CMSCrypto::SignTest(PCTXT pSrcFile, PCTXT pSigFile)
{
	KEY_SIG Sig;

	if( ReadSig(pSigFile, Sig) ) {

		HCRYPTKEY hKey = GetKeyByLength(Sig.uLen * 8);

		if( hKey ) {

			return SignTest(pSrcFile, hKey, Sig);
			}
		}

	return FALSE;
	}

BOOL CMSCrypto::SignTest(PCTXT pFilename, HCRYPTKEY hKey, KEY_SIG &Sig)
{
	CMD5Hash MD5(m_hProv);
	
	if( MD5.HashFile(pFilename) ) {

		return CryptVerifySignature( MD5,      
					     Sig.bData,     
					     Sig.uLen,        
					     hKey,     
					     NULL,  
					     0
					     );
		}

	return FALSE;
	}

BOOL CMSCrypto::SignTest(PBYTE pData, UINT uSize, HCRYPTKEY hKey, KEY_SIG &Sig)
{
	CMD5Hash MD5(m_hProv);

	if( MD5.HashData(pData, uSize) ) {

		return CryptVerifySignature( MD5,
					     Sig.bData,
					     Sig.uLen,
					     hKey,
					     NULL,
					     0
					     );
		}

	return FALSE;
	}

// Implementation

BOOL CMSCrypto::InitCSP(void)
{
	if( CryptAcquireContext( &m_hProv,
				 NULL,
				 MS_DEF_PROV,
				 PROV_RSA_FULL,
			 	 CRYPT_VERIFYCONTEXT
				 ) ) {
		return TRUE;
		}

	return FALSE;
	}

void CMSCrypto::FreeCSP(void)
{
	if( m_hProv ) { 
	
		CryptReleaseContext(m_hProv, 0);

		m_hProv = NULL;
		}
	}

// Keys

BOOL CMSCrypto::InitKeys(void)
{
	return InitKey512() && InitKey768() && InitKey1024();
	}

void CMSCrypto::FreeKeys(void)
{
	FreeKey(m_hKey512);

	FreeKey(m_hKey768);

	FreeKey(m_hKey1024);
	}
	
void CMSCrypto::FreeKey(HCRYPTKEY &Key)
{
	if( Key ) {

		CryptDestroyKey(Key);

		Key = NULL;
		}
	}

BOOL CMSCrypto::MakeKey(RSAKeyType Type)
{
	switch( Type ) {
	
		case rsaKey512:  return MakeKey(m_hKey512,  512 );

		case rsaKey768:  return MakeKey(m_hKey768,  768 );

		case rsaKey1024: return MakeKey(m_hKey1024, 1024);
		}
	
	return FALSE;
	}

BOOL CMSCrypto::MakeKey(HCRYPTKEY &Key, UINT uSize)
{
	if( CryptGenKey( m_hProv, 
			 CALG_RSA_SIGN,
			 (uSize << 16) | CRYPT_EXPORTABLE,
			 &Key
			 ) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CMSCrypto::InitKey512(void)
{
	return ImportKey(m_hKey512,  bKey512,  sizeof(bKey512 ));
	}

BOOL CMSCrypto::InitKey768(void)
{
	return ImportKey(m_hKey768,  bKey768,  sizeof(bKey768 ));
	}

BOOL CMSCrypto::InitKey1024(void)
{
	return ImportKey(m_hKey1024, bKey1024, sizeof(bKey1024));
	}

// Import/Export

BOOL CMSCrypto::ImportKey(HCRYPTKEY &Key, PBYTE pData, UINT uSize)
{
	FreeKey(Key);
	
	if( CryptImportKey( m_hProv,
			    pData,
			    uSize,
			    0,
			    0,
			    &Key
			    ) ) {
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CMSCrypto::ExportKey(HCRYPTKEY Key, KEY_BLOB &Blob, DWORD &dwLen)
{
	if( CryptExportKey( Key,     
			    0,  
			    PUBLICKEYBLOB,
			    0,
			    (PBYTE) &Blob,
			    &dwLen
			    ) ) {

		return TRUE;
		}

	return FALSE;
	}

// Signature Helpers

BOOL CMSCrypto::ReadSig(PCTXT pFilename, KEY_SIG &Sig)
{
	FILE *pFile = fopen(pFilename, "rb");

	if( pFile ) {

		UINT uCount = fread(Sig.bData, 1, sizeof(Sig.bData), pFile);

		fclose(pFile);

		if( uCount ) {

			Sig.uLen = uCount;

			return TRUE;
			}
		}

	Sig.uLen = 0;

	return FALSE;
	}

BOOL CMSCrypto::WriteSig(PCTXT pFilename, KEY_SIG const &Sig)
{
	FILE *pFile = fopen(pFilename, "wb");

	if( pFile ) {

		UINT uCount = fwrite(Sig.bData, 1, Sig.uLen, pFile);

		fclose(pFile);

		return uCount == Sig.uLen;
		}

	return FALSE;
	}

// BLOB Helpers

BOOL CMSCrypto::ReadBlob(PCTXT pFilename, KEY_BLOB &Blob)
{
	FILE *pFile = fopen(pFilename, "rb");

	if( pFile ) {

		UINT uCount = fread(&Blob, 1, sizeof(Blob), pFile);

		fclose(pFile);

		if( uCount >= (sizeof(PUBLICKEYSTRUC) + sizeof(RSAPUBKEY)) ) {

			PUBLICKEYSTRUC &pub = Blob.pub;

			RSAPUBKEY      &rsa = Blob.rsa; 

			if( pub.bType != PUBLICKEYBLOB ) {

				return FALSE;
				}

			if( pub.bVersion != 2 ) {

				return FALSE;
				}

			if( pub.aiKeyAlg != CALG_RSA_SIGN ) {
				
				return FALSE;
				}
			
			if( rsa.magic != 0x31415352 ) {
				
				return FALSE;
				}

			uCount -= sizeof(PUBLICKEYSTRUC);
			
			uCount -= sizeof(RSAPUBKEY);

			if( uCount != (rsa.bitlen + 7) / 8 ) {

				return FALSE;
				}
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMSCrypto::WriteBlob(PCTXT pFilename, KEY_BLOB const &Blob)
{
	FILE *pFile = fopen(pFilename, "wb");

	if( pFile ) {

		UINT uCount = sizeof(PUBLICKEYSTRUC) + sizeof(RSAPUBKEY);

		uCount += (( Blob.rsa.bitlen + 7) / 8);

		if( uCount <= sizeof(Blob) ) {

			if( fwrite(&Blob, 1, uCount, pFile) == uCount ) {

				fclose(pFile);

				return TRUE;
				}
			}

		fclose(pFile);

		return FALSE;
		}

	return FALSE;
	}

// End of File
