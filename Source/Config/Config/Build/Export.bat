@echo off

echo on

rem %1 = mask
rem %2 = build dir

mkdir "%~2\Include" 2>nul

mkdir "%~2\Include\Config" 2>nul

if exist %1.hpp copy %1.hpp "%~2\Include\Config" >nul

if exist %1.ipp copy %1.ipp "%~2\Include\Config" >nul

if exist %1.hxx copy %1.hxx "%~2\Include\Config" >nul

erase "%~2\Include\Config\intern.*" 2>nul
