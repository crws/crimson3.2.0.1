
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Resource DLLs
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load OEM\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
			}

			return TRUE;
		}

		catch( CException const &Exception )
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading OEM\n");

			return FALSE;
		}
	}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
	}

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// OEM Entry Points
//

CString DLLAPI OemGetCompany(void)
{
	return L"Red Lion Controls";
}

CString DLLAPI OemGetModels(void)
{
	return L"DA50,DA70";

//		L"G07,G09,G09L,G10,G10R,G12,G15"			L","
//		L"GCEQ,GCEV,GCEW"					L","
//		L"GSRQ,GSRV,GSRW"					L","
//		L"ETMIX24880,ETMIX24882,ETMIX20884"			L","
//		L"ET32DI24,ET32DO24,ET32AI20M,ET32AI10V,"		L","
//		L"ET16DI24,ET16DIAC,ET16DO24"				L","
//		L"ET16ISOTC,ET16ISO20M,ET16DORLY"			L","
//		L"ET16AI20M,ET16AI8AO"					L","
//		L"ET8AO20M,ET8ISOTC"					L","
//		L"ET10RTD"						L","
//		L"CO04,CO07,CO07EQ,CO10,CO10EV"				L","
//		L"CA04,CA07,CA07EQ,CA10,CA10EV,CA15"			L","
//		L"DA70X0FWQ,DA70X0FWV,DA70X0FWX"			L","
//		L"DA70X0GWQ,DA70X0GWV,DA70X0GWX"			L","
//		L"DA50X0BWQ,DA50X0BWV,DA50X0BWX"			L","
//		L"DA30DWQ,DA30DWV,DA30DWX"				L","
//		L"DA30DWQ,DA30DWV,DA30DWX"				L","
//		L"DA10D"
	;
}

WORD DLLAPI OemGetUsbBase(void)
{
	return 0x0000;
}

BOOL DLLAPI OemGetPair(UINT n, CString &a, CString &b)
{
	static PCTXT Subst[][2] = {

		{ L"GSRQ",    L"Core Controller (QVGA)" },
		{ L"GSRV",    L"Core Controller (VGA)"  },
		{ L"GSRW",    L"Core Controller (WXGA)" },
		{ L"GSR",     L"Core Controller"        },

		{ L"GCEQ",    L"Edge Controller (QVGA)" },
		{ L"GCEV",    L"Edge Controller (VGA)"  },
		{ L"GCEW",    L"Edge Controller (WXGA)" },
		{ L"GCE",     L"Edge Controller"        },

//		{ L"G10R",   L"G10 SVGA" },

		{ L"ETMIX24880", L"E3-MIX24880" },
		{ L"ETMIX24882", L"E3-MIX24882" },
		{ L"ETMIX20884", L"E3-MIX20884" },
		{ L"ET32DI24",   L"E3-32DI24"   },
		{ L"ET16DI24",   L"E3-16DI24"   },
		{ L"ET16DIAC",   L"E3-16DIAC"   },
		{ L"ET32DO24",   L"E3-32DO24"   },
		{ L"ET16DO24",   L"E3-16DO24"   },
		{ L"ET32AI20M",  L"E3-32AI20M"  },
		{ L"ET16AI20M",  L"E3-16AI20M"  },
		{ L"ET8AO20M",   L"E3-8AO20M"   },
		{ L"ET16AI8AO",  L"E3-16AI8AO"  },
		{ L"ET32AI10V",  L"E3-32AI10V"  },
		{ L"ET10RTD",    L"E3-10RTD"    },
		{ L"ET8ISOTC",   L"E3-8ISOTC"   },
		{ L"ET16ISOTC",  L"E3-16ISOTC"  },
		{ L"ET16ISO20M", L"E3-16ISO20M" },
		{ L"ET16DORLY",  L"E3-16DORLY"  },

		{ L"CO04",	 L"CR1000-04000" },
		{ L"CO07",	 L"CR1000-07000" },
		{ L"CO10",	 L"CR1000-10000" },

		{ L"CA04C",	 L"CR3010-04000" },
		{ L"CA07C",	 L"CR3010-07000" },
		{ L"CA10C",	 L"CR3010-10000" },
		{ L"CA15C",	 L"CR3010-15000" },

		{ L"CA04",	 L"CR3000-04000" },
		{ L"CA07",	 L"CR3000-07000" },
		{ L"CA10",	 L"CR3000-10000" },
		{ L"CA15",	 L"CR3000-15000" },

		{ L"DA30DWQ",	 L"DA30D (WQVGA)" },
		{ L"DA30DWV",	 L"DA30D (WVGA)"  },
		{ L"DA30DWX",	 L"DA30D (WXVGA)" },

	};

	if( n < elements(Subst) ) {

		a = Subst[n][0];
		b = Subst[n][1];

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI OemGetFeature(CString &f, BOOL d)
{
	if( f == L"RedLion" ) {

		return TRUE;
	}

	return d;
}

CString DLLAPI OemGetOption(CString const &f, CString const &d)
{
	return d;
}

BOOL DLLAPI OemAllowDriver(UINT uIdent, BOOL fDefault)
{
	switch( uIdent ) {

		// CDL Drivers
		case 0x403D: return FALSE;
		case 0x404F: return TRUE;
		case 0x407E: return FALSE;

		// CCP Driver
		case 0x40E0: return FALSE;
	}

	return fDefault;
}

DWORD DLLAPI OemGetColor(CString &c, DWORD d)
{
	return d;
}

// End of File
