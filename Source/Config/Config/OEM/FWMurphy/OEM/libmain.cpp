
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Resource DLLs
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load OEM\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading OEM\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// OEM Entry Points
//

CString DLLAPI OemGetCompany(void)
{
	return L"FW Murphy";
	}

CString DLLAPI OemGetModels(void)
{
	return L"G07,G10,G12"				L","
	       L"GCEQ,GCEV,GCEW"				

		;
	}

WORD DLLAPI OemGetUsbBase(void)
{
	return 0x0000;
	}

BOOL DLLAPI OemGetPair(UINT n, CString &a, CString &b)
{
	static PCTXT Subst[][2] = {

		// Core Strings

		{ L"Red Lion Controls Inc",			L"FW Murphy",			},
		{ L"Red Lion Controls",				L"FW Murphy",			},
		{ L"Red Lion",					L"FW Murphy",			},
		{ L"Crimson 3.1",				L"M-VIEW DESIGNER 3.1",		},
		{ L"Crimson 3",					L"M-VIEW DESIGNER",		},
		{ L"Crimson device",				L"M-VIEW DESIGNER Device",	},
		{ L"Crimson",					L"M-VIEW DESIGNER",		},
		{ L"Graphite HMI",				L"M-VIEW HMI"			},
		{ L"Graphite Controller",			L"M-VIEW RTU"			},

		// Part Numbers

		{ L"G07C0000",					L"M-VIEW Touch 7",		},	// 0
		{ L"G07S0000",					L"M-VIEW Touch 7",		},	// 1	
		{ L"G10C0000",					L"M-VIEW Touch 10",		},	// 0
		{ L"G10C1000",					L"M-VIEW Touch 10",		},	// 1
		{ L"G10S0000",					L"M-VIEW Touch 10",		},	// 2
		{ L"G10S1000",					L"M-VIEW Touch 10",		},	// 3
		{ L"G12C0000",					L"M-VIEW Touch 12",		},	// 0
		{ L"G12C1100",					L"M-VIEW Touch 12",		},	// 1
		{ L"G12S0000",					L"M-VIEW Touch 12",		},	// 2
		{ L"G12S1100",					L"M-VIEW Touch 12",		},	// 3
	
		{ L"GRAC0001",					L"M-VIEW RTU",			},

		// Graphite

		{ L"G07",					L"MV 7T",			},
		{ L"G10",					L"MV 10T",			},
		{ L"G12",					L"MV 12T",			},

		{ L"GCEQ",					L"MV RTU (QVGA)"		},
		{ L"GCEV",					L"MV RTU (VGA)"			},
		{ L"GCEW",					L"MV RTU (WXGA)"		},
		{ L"GCE",					L"MV RTU"			},

		// File Extensions

		{ L".cd3",					L".cd3",			},
		{ L".cd31",					L".mvt",			},
		{ L".ci3",					L".mvi",			},

		};

	if( n < elements(Subst) ) {

		a = Subst[n][0];
		b = Subst[n][1];

		return TRUE;
		}

	return FALSE;
	}

BOOL DLLAPI OemGetFeature(CString &f, BOOL d)
{
	if( f == L"PoweredBy"    ) return TRUE;
	if( f == L"Registration" ) return FALSE;
	if( f == L"Import"	 ) return FALSE;
	if( f == L"Support"      ) return FALSE;
	if( f == L"Update"       ) return FALSE;
	if( f == L"BuildNotes"   ) return FALSE;
	if( f == L"Information"  ) return FALSE;
	if( f == L"ShowSplash"   ) return TRUE;
	if( f == L"IOModules"    ) return FALSE;
	if( f == L"Profibus"     ) return FALSE;
	if( f == L"DeviceNet"    ) return FALSE;
	if( f == L"Modem"        ) return FALSE;

	if( f == L"G07_0"      ) return FALSE;
	if( f == L"G07_1"      ) return TRUE;
	
	if( f == L"G10_0"      ) return FALSE;
	if( f == L"G10_1"      ) return FALSE;
	if( f == L"G10_2"      ) return TRUE;
	if( f == L"G10_3"      ) return FALSE;

	if( f == L"G12_0"      ) return FALSE;
	if( f == L"G12_1"      ) return FALSE;
	if( f == L"G12_2"      ) return FALSE;
	if( f == L"G12_3"      ) return TRUE;

	if( f == L"TrendViewer") return TRUE;

	return d;
	}

CString DLLAPI OemGetOption(CString const &f, CString const &d)
{
	return d;
	}

BOOL DLLAPI OemAllowDriver(UINT uIdent, BOOL fDefault)
{
	switch( uIdent ) {

		// CDL Drivers
		case 0x403D: return FALSE;
		case 0x404F: return TRUE;
		case 0x407E: return FALSE;

		// CCP Driver
		case 0x40E0: return TRUE;
		}

	return fDefault;
	}

DWORD DLLAPI OemGetColor(CString &c, DWORD d)
{
	return d;
	}

// End of File
