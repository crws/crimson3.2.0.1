
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Resource DLLs
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load OEM\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading OEM\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// OEM Entry Points
//

CString DLLAPI OemGetCompany(void)
{
	return L"Monico";
	}

CString DLLAPI OemGetModels(void)
{
	return L"G07,G10,G12,G15"					L","
	       L"GCENQ,GCENV,GCENW"					L","
	       L"DA30DWQ,DA30DWV,DA30DWX"				L","
	       L"DA10D"							L","
	       L"ETMIX24880,ETMIX24882,ETMIX20884"			L","
	       L"ET32DI24,ET32DO24,ET32AI20M,ET32AI10V,"		L","
	       L"ET16DI24,ET16DIAC,ET16DO24"				L","
	       L"ET16ISOTC,ET16ISO20M,ET16DORLY"			L","
	       L"ET16AI20M,ET16AI8AO"					L","
	       L"ET8AO20M,ET8ISOTC"					L","
	       L"ET10RTD"
	       ;
	}

WORD DLLAPI OemGetUsbBase(void)
{
	return 0x0000;
	}

BOOL DLLAPI OemGetPair(UINT n, CString &a, CString &b)
{
	static PCTXT Subst[][2] = {

		// Core Strings

		{ L"Red Lion Controls Inc",		L"Monico Inc.",			},
		{ L"Red Lion Controls",			L"Monico",			},
		{ L"Red Lion",				L"Monico",			},
		{ L"Crimson 3.1",			L"MonicoView 3.1\x2122",	},
		{ L"Crimson 3.0",			L"MonicoView II\x2122",		},
		{ L"Crimson device",			L"Monico Gateway Device",	},
		{ L"Crimson 2.0",			L"MonicoView",			},
		{ L"Crimson 2",				L"MonicoView",			},
		{ L"Crimson",				L"MonicoView",			},
		{ L"G3 Panel",				L"HMI Gateway",			},
		{ L"G3",				L"HMI"				},
		{ L"C2",				L"MV",				},
		{ L"Graphite Converter",		L"Asset Gateway"		},
		{ L"Graphite",				L"Asset",			},
		{ L"Data Acquisition",			L"Protocol Converter"		},

		// Part Numbers
		
		{ L"G07S0000",				L"MHG07S0000",			},
		{ L"G10S0000",				L"MHG10S0000",			},
		{ L"G10S1000",				L"MHG10S1000",			},
		{ L"G12C0000",				L"MHG12C0000",			},
		{ L"G12S0000",				L"MHG12S0000",			},
		{ L"G15C1100",				L"MHG15C1100",			},

		{ L"GRAC0000",				L"Asset Gateway",		},

		{ L"DA10D0C00000000",			L"PC10"				},
		{ L"DA30D0F00000000",			L"PC30"				},

		// Graphite

		{ L"G07",				L"HMI 7\"",			},
		{ L"G10",				L"HMI 10\"",			},
		{ L"G12",				L"HMI 12\"",			},
		{ L"G15",				L"HMI 15\"",			},

		{ L"GCENQ",				L"Asset Gateway (QVGA)"		},
		{ L"GCENV",				L"Asset Gateway (VGA)"		},
		{ L"GCENW",				L"Asset Gateway (WXGA)"		},
		{ L"GCEN",				L"Asset Gateway"		},

		// Data Acquisition

		{ L"DA30DWQ",				L"PC30 (QVGA)"			},
		{ L"DA30DWV",				L"PC30 (VGA)"			},
		{ L"DA30DWX",				L"PC30 (WXGA)"			},

		{ L"DA10D",				L"PC10"				},
		{ L"DA30D",				L"PC30"				},

		// EtherTrak3

		{ L"ETMIX24880",			L"E3-MIX24880"			},
		{ L"ETMIX24882",			L"E3-MIX24882"			},
		{ L"ETMIX20884",			L"E3-MIX20884"			},
		{ L"ET32DI24",  			L"E3-32DI24"			},
		{ L"ET16DI24",  			L"E3-16DI24"			},
		{ L"ET16DIAC",  			L"E3-16DIAC"			},
		{ L"ET32DO24",  			L"E3-32DO24"			},
		{ L"ET16DO24",  			L"E3-16DO24"			},
		{ L"ET32AI20M", 			L"E3-32AI20M"			},
		{ L"ET16AI20M", 			L"E3-16AI20M"			},
		{ L"ET8AO20M",  			L"E3-8AO20M"			},
		{ L"ET16AI8AO", 			L"E3-16AI8AO"			},
		{ L"ET32AI10V", 			L"E3-32AI10V"			},
		{ L"ET10RTD",   			L"E3-10RTD"    			},
		{ L"ET8ISOTC",  			L"E3-8ISOTC"   			},
		{ L"ET16ISOTC", 			L"E3-16ISOTC"  			},
		{ L"ET16ISO20M",			L"E3-16ISO20M" 			},
		{ L"ET16DORLY", 			L"E3-16DORLY"  			},

		// Nasty Hack

		{ L"MHHMI 7\"S0000",			L"MHG07S0000",			},
		{ L"MHHMI 10\"S0000",			L"MHG10S0000",			},
		{ L"MHHMI 10\"S1000",			L"MHG10S1000",			},
		{ L"MHHMI 12\"C0000",			L"MHG12C0000",			},
		{ L"MHHMI 12\"S0000",			L"MHG12S0000",			},
		{ L"MHHMI 15\"C1100",			L"MHG15C1100",			},

		// File Extensions

		{ L".cd31",				L".mv31",			},
		{ L".cd3",				L".mv2",			},
		{ L".ci3",				L".mi2",			},

		// File Filters

		{ L"MonicoView 3.x Databases (*.mv2*)|*.mv2*",		L"All MonicoView Databases (*.mv*)|*.mv*"		},

		// URLs

		{ L"http://update.redlion.net/c31/update/",		L"http://www.monicoinc.com/products/"			},
		{ L"You may also visit us at http://www.redlion.net",	L"You may also visit us at http://www.monicoinc.com"	},
		{ L"sellmore.redlion.net",				L"registration-mv.monicoinc.com"			},
		{ L"/forms/registration_handler.asp",			L"/registration_handler.asp"				},

		};

	if( n < elements(Subst) ) {

		a = Subst[n][0];
		b = Subst[n][1];

		return TRUE;
		}

	return FALSE;
	}

BOOL DLLAPI OemGetFeature(CString &f, BOOL d)
{
	if( f == L"BuildNotes"   ) return FALSE;
	if( f == L"Information"  ) return FALSE;
	if( f == L"CatLink"      ) return TRUE;
	if( f == L"FuncHelp"     ) return TRUE;
	if( f == L"Import"	 ) return TRUE;
	if( f == L"Localize"     ) return FALSE;
	if( f == L"OPCProxy"     ) return TRUE;
	if( f == L"PoweredBy"    ) return TRUE;
	if( f == L"Promiscuous"  ) return FALSE;
	if( f == L"Registration" ) return TRUE;
	if( f == L"SMS"          ) return TRUE;
	if( f == L"Support"      ) return FALSE;
	if( f == L"Update"       ) return TRUE;
	if( f == L"ShowSplash"   ) return TRUE;
	if( f == L"Ethernet"	 ) return TRUE;
	if( f == L"USBHost"	 ) return FALSE;
	if( f == L"MPI"		 ) return TRUE;
	if( f == L"CanCdl"       ) return TRUE;
	if( f == L"GMPID1"	 ) return FALSE;
	if( f == L"GMPID2"	 ) return FALSE;
	if( f == L"GMSG1"	 ) return FALSE;
	if( f == L"Dongle"	 ) return FALSE;
	if( f == L"Control"	 ) return FALSE;

	if( f == L"G07_0"        ) return FALSE;
	if( f == L"G07_1"        ) return TRUE;

	if( f == L"G09_0"        ) return FALSE;
	if( f == L"G09_1"        ) return FALSE;

	if( f == L"G10_0"        ) return FALSE;
	if( f == L"G10_1"        ) return FALSE;
	if( f == L"G10_2"        ) return TRUE;
	if( f == L"G10_3"        ) return TRUE;

	if( f == L"G12_0"        ) return TRUE;
	if( f == L"G12_1"        ) return FALSE;
	if( f == L"G12_2"        ) return TRUE;
	if( f == L"G12_3"        ) return FALSE;

	if( f == L"G15_0"        ) return FALSE;
	if( f == L"G15_1"        ) return TRUE;

	return d;
	}

CString DLLAPI OemGetOption(CString const &f, CString const &d)
{
	if( f == L"Priority" ) return L"Monico";
	if( f == L"RegPort"  ) return L"833";

	return d;
	}

BOOL DLLAPI OemAllowDriver(UINT uIdent, BOOL fDefault)
{
	switch( uIdent ) {

		// Caterpillar Standard
		
		case 0x4039:

			return FALSE;

		// Other Caterpillar

		case 0x403D:
	//	case 0x4041:   // CDL Tester
		case 0x404F:
		case 0x407E:
		
			return TRUE;

		// J1939
		
		case 0x4036:

			return FALSE;
		
		case 0x4090:
			
			return TRUE;

		// SNMP

		case 0x408E:
		case 0x40A3:

			return TRUE;

		// M2M

		case 0x3527:
		case 0x406B:

			return TRUE;

		// CAT Pyrometer
		case 0x40AD:

			return TRUE;

		// ML1 TCP
		case 0x40BF:

			return TRUE;

		// ML1 Serial
		case 0x40BE:

			return FALSE;

		// ML1 Testers
		case 0x40C6:
		case 0x40C7:

			return FALSE;

		// CCP Driver
		case 0x40E0: return FALSE;		

		}

	return fDefault;
	}

DWORD DLLAPI OemGetColor(CString &c, DWORD d)
{
	return d;
	}

// End of File
