
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Proxy Main Application
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

int WINAPI WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow)
{
	WCHAR sApp[512];

	WCHAR sCmd[512];

	int i;

	for( UINT p = 0; p < 3; p++ ) {

		switch( p ) {

			case 0:
				lstrcpy(sApp, L".\\shexe.exe");
				break;

			case 1:
				GetModuleFileName(hThis, sApp, sizeof(sApp));

				for( i = lstrlen(sApp); --i; ) {

					if( sApp[i] == '\\' ) {

						lstrcpy(sApp + i + 1, L"shexe.exe");

						break;
						}
					}
				break;

			case 2:
				lstrcpy(sApp, L"..\\..\\source\\shell\\shexe\\debug\\shexe.exe");
				break;

			}

		HANDLE hFile = CreateFile( sApp,
					   GENERIC_READ,
					   FILE_SHARE_READ,
					   NULL,
					   OPEN_EXISTING,
					   FILE_FLAG_SEQUENTIAL_SCAN,
					   NULL
					   );

		if( hFile != INVALID_HANDLE_VALUE ) {

			CloseHandle(hFile);

			break;
			}
		}

	if( p < 3 ) {

		lstrcpy(sCmd, L"\"");

		lstrcat(sCmd, sApp);

		lstrcat(sCmd, L"\"");

		WCHAR *pLine = GetCommandLine();

		BOOL  fQuote = FALSE;

		for( int i = 0; pLine[i]; i++ ) {

			if( pLine[i] == '\'' || pLine[i] == '\"' ) {

				fQuote = !fQuote;
				}

			if( !fQuote && pLine[i] == ' ' ) {

				lstrcat(sCmd, pLine + i);

				break;
				}
			}

		STARTUPINFO Start;

		PROCESS_INFORMATION Info;

		memset(&Info,  0, sizeof(Info));

		memset(&Start, 0, sizeof(Start));

		Start.cb = sizeof(Start);

		Start.wShowWindow = WORD(nCmdShow);

		Start.dwFlags     = STARTF_USESHOWWINDOW;

		CreateProcess(	sApp,
				sCmd,
				NULL,
				NULL,
				FALSE,
				0,
				NULL,
				NULL,
				&Start,
				&Info
				);

		if( Info.dwProcessId ) {

			return 0;
			}
		}

	MessageBox(NULL, L"Unable to start application.", NULL, MB_OK);

	return 1;
	}

// End of File
