
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Resource DLLs
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

// TODO -- How about we make this SDK-based rather
// than using CStrings and so on, and then OEMs will
// be able to modify it themselves...

#include <pccore.hpp>

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#define	DLLAPI __declspec(dllexport)

// End of File
