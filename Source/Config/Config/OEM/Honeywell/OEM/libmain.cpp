
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Resource DLLs
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load OEM\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading OEM\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// OEM Entry Points
//

CString DLLAPI OemGetCompany(void)
{
	return L"Honeywell";
	}

CString DLLAPI OemGetModels(void)
{
	return L"G07,G09,G10,G10R,G12,G15"				L","
	       L"GCEQ,GCEV,GCEW"					L","
	       L"GSRQ,GSRV,GSRW"					L","
	       L"CO04,CO07,CO07EQ,CO10,CO10EV"				L","
	       L"CA04,CA07,CA07EQ,CA10,CA10EV,CA15"			L","
	       L"DA30DWQ,DA30DWV,DA30DWX"				L","
	       L"DA10D"
	       ;
	}

WORD DLLAPI OemGetUsbBase(void)
{
	return 0x0000;
	}

BOOL DLLAPI OemGetPair(UINT n, CString &a, CString &b)
{
	static PCTXT Subst[][2] = {

		// Core Strings
		
		{ L"Red Lion Controls Inc",		L"Honeywell",			},
		{ L"Red Lion Controls",			L"Honeywell",			},
		{ L"Red Lion",				L"Honeywell",			},
		{ L"Crimson 3.1",			L"Station Designer 3.1",	},
		{ L"Crimson 3.0",			L"Station Designer 1.2",	},
		{ L"Crimson device",			L"Control Station device",	},
		{ L"Crimson",				L"Station Designer",		},
		{ L"G3 Panel",				L"900 Control Station",		},
		{ L"c3.pdf",				L"sd.pdf",			},

		{ L"G310V2",				L"CS10",			},
		{ L"G3",				L"CS"				},
		{ L"{3.0}",				L"1.0",				},

		{ L"GSRQ",				L"Core Controller (QVGA)"	},
		{ L"GSRV",				L"Core Controller (VGA)"	},
		{ L"GSRW",				L"Core Controller (WXGA)"	},
		{ L"GSR",				L"Core Controller"		},

		{ L"GCEQ",				L"Edge Controller (QVGA)"	},
		{ L"GCEV",				L"Edge Controller (VGA)"	},
		{ L"GCEW",				L"Edge Controller (WXGA)"	},
		{ L"GCE",				L"Edge Controller"		},

//		{ L"G10R",				L"G10 SVGA"			},

		{ L"CO04",				L"CR1000-04000"			},
		{ L"CO07",				L"CR1000-07000"			},
		{ L"CO10",				L"CR1000-10000"			},

		{ L"CA04C",				L"CR3010-04000"			},
		{ L"CA07C",				L"CR3010-07000"			},
		{ L"CA10C",				L"CR3010-10000"			},
		{ L"CA15C",				L"CR3010-15000"			},

		{ L"CA04",				L"CR3000-04000"			},
		{ L"CA07",				L"CR3000-07000"			},
		{ L"CA10",				L"CR3000-10000"			},
		{ L"CA15",				L"CR3000-15000"			},

		{ L"DA30DWQ",				L"DA30D (WQVGA)"		},
		{ L"DA30DWV",				L"DA30D (WVGA)"			},
		{ L"DA30DWX",				L"DA30D (WXVGA)"		},

		// File Extensions

		{ L".cd31",				L".sd31",			},
		{ L".cd3",				L".sds",			},
		{ L".ci3",				L".sdi",			},

		// File Filters

		{ L"Station Designer 3.x Databases (*.sds*)|*.sds*",		L"All Station Designer Databases (*.sd*)|*.sd*"		},

		};

	if( n < elements(Subst) ) {

		a = Subst[n][0];
		b = Subst[n][1];

		return TRUE;
		}

	return FALSE;
	}

BOOL DLLAPI OemGetFeature(CString &f, BOOL d)
{
	if( f == L"OemSD"        ) return TRUE;
	if( f == L"PoweredBy"    ) return TRUE;
	if( f == L"Update"       ) return FALSE;
	if( f == L"Support"      ) return FALSE;
	if( f == L"Registration" ) return FALSE;
	if( f == L"OPCProxy"     ) return FALSE;
	if( f == L"Import"	 ) return TRUE;
	if( f == L"BuildNotes"   ) return FALSE;
	if( f == L"FuncHelp"     ) return FALSE;
	if( f == L"Localize"     ) return FALSE;

	return d;
	}

CString DLLAPI OemGetOption(CString const &f, CString const &d)
{
	return d;
	}

BOOL DLLAPI OemAllowDriver(UINT uIdent, BOOL fDefault)
{
	switch( uIdent ) {

		case 0x9998: return TRUE;
		case 0x9999: return TRUE;
		case 0x334C: return TRUE;
		case 0x3401: return TRUE;
		case 0x3402: return TRUE;
		case 0x3501: return TRUE;
		case 0x3505: return TRUE;
		case 0x3705: return TRUE;
		case 0x3706: return TRUE;
		}

	if( uIdent >= 0xFF00 && uIdent <= 0xFF0F ) {

		return TRUE;
		}

	// Debug Drivers

	if( uIdent >= 0x40E4 && uIdent <= 0x40E5 ) {

		return TRUE;
		}

	return FALSE;
	}

DWORD DLLAPI OemGetColor(CString &c, DWORD d)
{
	return d;
	}

// End of File
