
#include "intern.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag Creation Object
//

// Constructor

CMakeTags::CMakeTags(CString Root, IMakeTags *pTags)
{
	m_Root  = Root;

	m_pTags = pTags;

	LoadKeywordMap();
	}

// Destructor

CMakeTags::~CMakeTags(void)
{
	}

// Operations

void CMakeTags::WarnRename(void)
{
	if( m_Delta.GetCount() ) {

		if( OpenClipboard(afxMainWnd->GetHandle()) ) {

			HGLOBAL hData = GlobalAlloc(GHND, m_Delta.GetCount() * 256);

			PBYTE   pData = PBYTE(GlobalLock(hData));

			PBYTE   pWalk = pData;

			for( UINT n = 0; n < m_Delta.GetCount(); n++ ) {

				PCTXT p = m_Delta[n];

				while( *p ) {

					*pWalk++ = BYTE(*p++);
					}

				*pWalk++ = '\r';
				*pWalk++ = '\n';
				}

			*pWalk++ = 0;

			GlobalUnlock(hData);

			hData = GlobalReAlloc(hData, pWalk - pData, GMEM_MOVEABLE);

			EmptyClipboard();

			SetClipboardData(CF_TEXT, hData);

			CloseClipboard();
			}

		CString Text = L"Conversion of the names from the CDE file has produced at\n"
			       L"least one naming conflict. The names have been modified to\n"
			       L"resolve the problems, and the old and new names have been\n"
			       L"placed on the Clipboard for pasting into Excel or Notepad.";

		afxMainWnd->Error(Text);

		afxMainWnd->UpdateWindow();
		}
	}

// Init Data

CInitData & CMakeTags::GetInitData(void)
{
	CInitData *pInit = NULL;

	m_pTags->GetInitData(pInit);

	AfxAssert(pInit);

	return *pInit;
	}

// Folders

CString CMakeTags::NewFolder(CString Folder)
{
	return NewFolder(L"", Folder);
	}

CString CMakeTags::NewFolder(CString Class, CString Folder)
{
	CString InitA = Valid(Folder);

	CString FullA = m_Folder;

	FullA += L'.';

	FullA += InitA;

	if( !m_Check.Failed(m_Check.Find(FullA)) ) {

		for( UINT n = 1;; n++ ) {

			CString InitB = CPrintf(L"%s%u", InitA, n);

			CString FullB = m_Folder;

			FullB += L'.';

			FullB += InitB;

			if( m_Check.Failed(m_Check.Find(FullB)) ) {

				CString FullC = m_Folder;

				FullC += L'.';

				FullC += Folder;

				if( m_Delta.IsEmpty() ) {

					m_Delta.Append(L"Old Name\tNew Name");
					}

				m_Delta.Append(CPrintf(L"%s\t%s", FullC.Mid(1), FullB.Mid(1)));

				m_pTags->AddFolder(Class, InitB);

				m_Check.Insert(FullB);

				m_Folder = FullB;

				return m_Root + m_Folder;
				}
			}
		}

	m_pTags->AddFolder(Class, InitA);

	m_Check.Insert(FullA);

	m_Folder = FullA;

	// REV3 -- Urgh!

	return m_Root + m_Folder;
	}

void CMakeTags::EndFolder(void)
{
	m_Folder = m_Folder.Left(m_Folder.FindRev(L'.'));

	m_pTags->EndFolder();
	}

// Constants

void CMakeTags::EmitRealC(PCTXT pName, PCTXT pLabel, float nValue, int nDP)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddReal( Name,			// Name
			  Label,		// Label
			  Const(nValue),	// Value
			  0,			// Extent
			  FALSE,		// Write
			  L"",			// Min
			  L"",			// Max
			  nDP			// DP
			  );
	}

void CMakeTags::EmitSintC(PCTXT pName, PCTXT pLabel, int nValue)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Const(nValue),	// Value
			  0,			// Extent
			  FALSE,		// Write
			  L"",			// Min
			  L"",			// Max
			  0,			// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitFlagC(PCTXT pName, PCTXT pLabel, BOOL fValue)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Const(fValue),	// Value
			  0,			// Extent
			  1,			// Integer
			  FALSE,		// Write
			  L"",			// State0
			  L""			// State1
			  );
	}

void CMakeTags::EmitTextC(PCTXT pName, PCTXT pLabel, CString Value)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddText( Name,			// Name
			  Label,		// Label
			  ConstString(Value),	// Value
			  0,			// Len
			  0,			// Extent
			  FALSE,		// Write
			  FALSE			// Encode
			  );
	}

// Mapped Tags

void CMakeTags::EmitRealM(PCTXT pName, PCTXT pLabel, CString Addr, int nDP)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddReal( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  L"",			// Min
			  L"",			// Max
			  nDP			// DP
			  );
	}

void CMakeTags::EmitRealM(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddReal( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  Const(float(nMin)),	// Min
			  Const(float(nMax)),	// Max
			  nDP			// DP
			  );
	}

void CMakeTags::EmitSintM(PCTXT pName, PCTXT pLabel, CString Addr)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  L"",			// Min
			  L"",			// Max
			  0,			// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitSintM(PCTXT pName, PCTXT pLabel, CString Addr, int nMin, int nMax)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  Const(nMin),		// Min
			  Const(nMax),		// Max
			  0,			// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitWintM(PCTXT pName, PCTXT pLabel, CString Addr)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  L"",			// Min
			  L"",			// Max
			  NOTHING,		// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitFlagM(PCTXT pName, PCTXT pLabel, CString Addr)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  1,			// Integer
			  FALSE,		// Write
			  L"",			// State0
			  L""			// State1
			  );
	}

void CMakeTags::EmitEnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum)
{
	if( Enum.Count('|') == 1 && Enum.Count(',') == 0 ) {

		CStringArray Val;

		Enum.Tokenize(Val, '|');

		EmitEnumM(pName, pLabel, Addr, Val[0], Val[1]);

		return;
		}

	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddEnum( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  Enum
			  );
	}

void CMakeTags::EmitFnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum)
{
	if( Enum.Count('|') == 1 && Enum.Count(',') == 0 ) {

		CStringArray Val;

		Enum.Tokenize(Val, '|');

		EmitFnumM(pName, pLabel, Addr, Val[0], Val[1]);

		return;
		}

	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFnum( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  Enum
			  );
	}

void CMakeTags::EmitEnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);
	      
	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  1,			// Integer
			  FALSE,		// Write
			  Val0,			// State0
			  Val1			// State1
			  );
	}

void CMakeTags::EmitFnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);
	      
	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  2,			// Real
			  FALSE,		// Write
			  Val0,			// State0
			  Val1			// State1
			  );
	}

void CMakeTags::EmitTextM(PCTXT pName, PCTXT pLabel, CString Addr, UINT uLen)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddText( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uLen,			// Len
			  0,			// Extent
			  FALSE,			// Write
			  FALSE			// Encode
			  );
	}

// Special Tags

void CMakeTags::EmitTimeM(PCTXT pName, PCTXT pLabel, CString Addr, UINT Fmt)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddSpec( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  100 + Fmt		// Special
			  );
	}

void CMakeTags::EmitTimeW(PCTXT pName, PCTXT pLabel, CString Addr, UINT Fmt)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddSpec( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  TRUE,			// Write
			  100 + Fmt		// Special
			  );
	}

void CMakeTags::EmitTimeA(PCTXT pName, PCTXT pLabel, CString Addr, UINT Fmt, UINT uSize)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddSpec( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  FALSE,		// Write
			  100 + Fmt		// Special
			  );
	}

void CMakeTags::EmitInetM(PCTXT pName, PCTXT pLabel, CString Addr)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddSpec( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  200			// Special
			  );
	}

void CMakeTags::EmitInetA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddSpec( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,			// Extent
			  fWrite,		// Write
			  200			// Special
			  );
	}

void CMakeTags::EmitEnetM(PCTXT pName, PCTXT pLabel, CString Addr)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddSpec( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  300			// Special
			  );
	}

void CMakeTags::EmitTrend(PCTXT pName, PCTXT pLabel, CString Addr, int nDP)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddSpec( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  FALSE,		// Write
			  400 + nDP		// Special
			  );
	}

// Writable Tags

void CMakeTags::EmitRealW(PCTXT pName, PCTXT pLabel, CString Addr, int nDP)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddReal( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  TRUE,			// Write
			  L"",			// Min
			  L"",			// Max
			  nDP			// DP
			  );
	}

void CMakeTags::EmitRealW(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddReal( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  TRUE,			// Write
			  Const(float(nMin)),	// Min
			  Const(float(nMax)),	// Max
			  nDP			// DP
			  );
	}

void CMakeTags::EmitSintW(PCTXT pName, PCTXT pLabel, CString Addr)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  TRUE,			// Write
			  L"",			// Min
			  L"",			// Max
			  0,			// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitSintW(PCTXT pName, PCTXT pLabel, CString Addr, int nMin, int nMax)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  TRUE,			// Write
			  Const(nMin),		// Min
			  Const(nMax),		// Max
			  0,			// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitFlagW(PCTXT pName, PCTXT pLabel, CString Addr)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  1,			// Integer
			  TRUE,			// Write
			  L"",			// State0
			  L""			// State1
			  );
	}

void CMakeTags::EmitEnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum)
{
	if( Enum.Count('|') == 1 && Enum.Count(',') == 0 ) {

		CStringArray Val;

		Enum.Tokenize(Val, '|');

		EmitEnumW(pName, pLabel, Addr, Val[0], Val[1]);

		return;
		}

	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddEnum( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  TRUE,			// Write
			  Enum
			  );
	}

void CMakeTags::EmitFnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum)
{
	if( Enum.Count('|') == 1 && Enum.Count(',') == 0 ) {

		CStringArray Val;

		Enum.Tokenize(Val, '|');

		EmitFnumW(pName, pLabel, Addr, Val[0], Val[1]);

		return;
		}

	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFnum( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  TRUE,			// Write
			  Enum
			  );
	}

void CMakeTags::EmitEnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  1,			// Integer
			  TRUE,			// Write
			  Val0,			// State0
			  Val1			// State1
			  );
	}

void CMakeTags::EmitFnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  0,			// Extent
			  2,			// Real
			  TRUE,			// Write
			  Val0,			// State0
			  Val1			// State1
			  );
	}

void CMakeTags::EmitTextW(PCTXT pName, PCTXT pLabel, CString Addr, UINT uLen)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddText( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uLen,			// Len
			  0,			// Extent
			  TRUE,			// Write
			  FALSE			// Encode
			  );
	}

// Arrays

void CMakeTags::EmitRealA(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, UINT uSize, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddReal( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  TRUE,			// Write
			  L"",			// Min
			  L"",			// Max
			  nDP			// DP
			  );
	}

void CMakeTags::EmitRealA(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax, UINT uSize, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddReal( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  TRUE,			// Write
			  Const(float(nMin)),	// Min
			  Const(float(nMax)),	// Max
			  nDP			// DP
			  );
	}

void CMakeTags::EmitSintA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  fWrite,		// Write
			  L"",			// Min
			  L"",			// Max
			  0,			// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitSintA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize, int nMin, int nMax, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  fWrite,		// Write
			  Const(nMin),		// Min
			  Const(nMax),		// Max
			  0,			// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitWintA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddInt(  Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  FALSE,		// Write
			  L"",			// Min
			  L"",			// Max
			  NOTHING,		// DP
			  0			// TreatAs
			  );
	}

void CMakeTags::EmitEnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum, UINT uSize, BOOL fWrite)
{
	if( Enum.Count('|') == 1 && Enum.Count(',') == 0 ) {

		CStringArray Val;

		Enum.Tokenize(Val, '|');

		EmitEnumA(pName, pLabel, Addr, Val[0], Val[1], uSize, fWrite);

		return;
		}

	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddEnum( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  fWrite,		// Write
			  Enum
			  );
	}

void CMakeTags::EmitEnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1, UINT uSize, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  1,			// Integer
			  fWrite,		// Write
			  Val0,			// State0
			  Val1			// State1
			  );
	}

void CMakeTags::EmitFnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum, UINT uSize, BOOL fWrite)
{
	if( Enum.Count('|') == 1 && Enum.Count(',') == 0 ) {

		CStringArray Val;

		Enum.Tokenize(Val, '|');

		EmitFnumA(pName, pLabel, Addr, Val[0], Val[1], uSize, fWrite);

		return;
		}

	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFnum( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  fWrite,		// Write
			  Enum
			  );
	}

void CMakeTags::EmitFnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1, UINT uSize, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddFlag( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uSize,		// Extent
			  2,			// Real
			  fWrite,		// Write
			  Val0,			// State0
			  Val1			// State1
			  );
	}

void CMakeTags::EmitTextA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uLen, UINT uSize, BOOL fWrite)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	m_pTags->AddText( Name,			// Name
			  Label,		// Label
			  Addr,			// Value
			  uLen,			// Len
			  uSize,		// Extent
			  fWrite,		// Write
			  FALSE			// Encode
			  );
	}

// Write Backs

void CMakeTags::EmitRealX(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, CString Code)
{
	EmitRealM(pName, pLabel, Addr, nDP);

	m_pTags->SetWriteBack(Code);
	}

void CMakeTags::EmitRealX(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax, CString Code)
{
	EmitRealM(pName, pLabel, Addr, nDP, nMin, nMax);

	m_pTags->SetWriteBack(Code);
	}

void CMakeTags::EmitFnumX(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum, CString Code)
{
	EmitFnumM(pName, pLabel, Addr, Enum);

	m_pTags->SetWriteBack(Code);
	}

// Lookups

void CMakeTags::EmitLookup(PCTXT pName, PCTXT pLabel, CUIntArray const &Index)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	CString Code;

	Code.Expand(65536);

	Code += L"// Lookup Table\r\n";

	Code += L"\r\n";

	Code += L"switch( i ) {\r\n";

	Code += L"\r\n";

	UINT c = Index.GetCount();

	for( UINT n = 0; n < c; n++  ) {

		CString Line;

		Line.Printf(L"\tcase %u: return %u;\r\n", n, Index[n]);

		Code += Line;
		}

	Code += L"\t}\r\n\r\n";

	Code += L"return 0;";
	
	Code += L"\r\n";

	m_pTags->AddInt(  Name,		// Name
			  Label,	// Label
			  Code,		// Value
			  0,		// Extent
			  0,		// Write
			  L"",		// Min
			  L"",		// Max
			  0,		// DP
			  0		// TreatAs
			  );
	}

void CMakeTags::EmitLookup(PCTXT pName, PCTXT pLabel, CUIntArray const &Index, CStringArray const &Value, CString Default)
{
	CString Label = pLabel;

	CString Name  = pName ? pName : pLabel;

	CheckName(Name);

	CString Code;

	Code.Expand(65536);

	Code += L"// Lookup Table\r\n";

	Code += L"\r\n";

	Code += L"switch( i ) {\r\n";

	Code += L"\r\n";

	UINT c = Index.GetCount();

	for( UINT n = 0; n < c; n++  ) {

		if( Value[n] == L"0" ) {

			continue;
			}

		if( Value[n] == L"\"\"" ) {

			continue;
			}

		CString Line;

		Line.Printf(L"\tcase %u: return %s;\r\n", Index[n], Value[n]);

		Code += Line;
		}

	Code += L"\t}\r\n";

	Code += L"\r\n";

	Code += L"return ";

	Code += Default;

	Code += L";\r\n";

	m_pTags->AddInt(  Name,		// Name
			  Label,	// Label
			  Code,		// Value
			  0,		// Extent
			  0,		// Write
			  L"",		// Min
			  L"",		// Max
			  0,		// DP
			  0		// TreatAs
			  );
	}

// Implementation

CString CMakeTags::Const(double n)
{
	if( floor(n) == n ) {

		return CPrintf(L"%0.0f.0", n);
		}

	return CPrintf(L"%f", n);
	}

CString CMakeTags::Const(int n)
{
	return CPrintf(L"%d", n);
	}

CString CMakeTags::Const(CString s)
{
	if( !s.IsEmpty() ) {

		s.Replace(L"\"", L"\\\"");

		return L'"' + s + L'"';
		}

	return s;
	}

CString CMakeTags::ConstString(CString s)
{
	s.Replace(L"\"", L"\\\"");

	return L'"' + s + L'"';
	}

void CMakeTags::CheckName(CString &Name)
{
	Name.Remove(L' ');

	PCTXT pFind = L"Description";

	UINT  uFind = Name.Find(pFind);

	if( uFind < NOTHING ) {

		Name.Delete(uFind, wstrlen(pFind));

		Name.Insert(uFind, L"Desc");
		}

	Name = Valid(Name);
	}

CString CMakeTags::Valid(CString Name)
{
	UINT t = Name.GetLength();

	if( t ) {

		if( isdigit(Name[0]) ) {

			Name = L'_' + Name;

			t    = t + 1;
			}

		for( UINT n = 0; n < t; n++ ) {

			TCHAR c = Name[n];

			if( c < 256 ) {

				if( isalpha(c) || isdigit(c) || c == L'_' ) {

					continue;
					}
				}
			
			Name.SetAt(n, L'_');
			}

		if( !m_Words.Failed(m_Words.Find(Name)) ) {

			Name = L'_' + Name;
			}

		return Name;
		}

	return L"Anon";
	}

void CMakeTags::LoadKeywordMap(void)
{
	m_Words.Insert(	L"break"	);
	m_Words.Insert(	L"bool"		);
	m_Words.Insert(	L"case"		);
	m_Words.Insert(	L"class"	);
	m_Words.Insert(	L"const"	);
	m_Words.Insert(	L"continue"	);
	m_Words.Insert(	L"cstring"	);
	m_Words.Insert(	L"default"	);
	m_Words.Insert(	L"dispatch"	);
	m_Words.Insert(	L"do"		);
	m_Words.Insert(	L"else"		);
	m_Words.Insert(	L"false"	);
	m_Words.Insert(	L"float"	);
	m_Words.Insert(	L"continue"	);
	m_Words.Insert(	L"for"		);
	m_Words.Insert(	L"if"		);
	m_Words.Insert(	L"int"		);
	m_Words.Insert(	L"numeric"	);
	m_Words.Insert(	L"return"	);
	m_Words.Insert(	L"Run"		);
	m_Words.Insert(	L"switch"	);
	m_Words.Insert(	L"true"		);
	m_Words.Insert(	L"using"	);
	m_Words.Insert(	L"void"		);
	m_Words.Insert(	L"WAS"		);
	m_Words.Insert(	L"while"	);
	}

// End of File
