@echo off

rem %1 = project
rem %2 = config
rem %3 = intermediate
rem %4 = output
rem %5 = mask

echo Exporting...

copy %4\%1.dll ..\bin\%2 >nul

copy %4\%1.dll ..\..\..\..\bin\%2
