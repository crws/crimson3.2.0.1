
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CDE File Parser
//

// Constructor

CParser::CParser(FILE *pFile)
{
	m_pFile = pFile;
	}

// Attributes

int CParser::GetPos(void) const
{
	return int(ftell(m_pFile));
	}

// Operations

BYTE CParser::ReadByte(void)
{
	BYTE b = BYTE(getc(m_pFile));

	return b;
	}

WORD CParser::ReadWord(void)
{
	BYTE lo = ReadByte();
	
	BYTE hi = ReadByte();

	return MAKEWORD(lo, hi);
	}

LONG CParser::ReadLong(void)
{
	WORD lo = ReadWord();
	
	WORD hi = ReadWord();

	return MAKELONG(lo, hi);
	}

WORD CParser::ReadBigWord(void)
{
	BYTE hi = ReadByte();

	BYTE lo = ReadByte();

	return MAKEWORD(lo, hi);
	}

LONG CParser::ReadBigLong(void)
{
	WORD hi = ReadBigWord();

	WORD lo = ReadBigWord();

	return MAKELONG(lo, hi);
	}

float CParser::ReadFloat(void)
{
	float r;

	((BYTE *) &r)[3] = ReadByte();
	((BYTE *) &r)[2] = ReadByte();
	((BYTE *) &r)[1] = ReadByte();
	((BYTE *) &r)[0] = ReadByte();

	return r;
	}

BOOL CParser::ReadData(CByteArray &Data, UINT uCount)
{
	Data.Expand(uCount);

	while( uCount-- ) {

		Data.Append(ReadByte());
		}

	return TRUE;
	}

CString CParser::ReadString(UINT uLen)
{
	CString s(L' ', uLen);

	for( UINT n = 0; n < uLen; n++ ) {

		BYTE  b = ReadByte();

		TCHAR c = b ? b : ' ';

		s.SetAt(n, c);
		}

	s.TrimRight();

	return s;
	}

void CParser::Skip(UINT uLen)
{
	fseek(m_pFile, uLen, SEEK_CUR);
	}

void CParser::SetPos(int nPos)
{
	fseek(m_pFile, nPos, SEEK_SET);
	}

// End of File
