
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

UINT mark;

//////////////////////////////////////////////////////////////////////////
//
// Library Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load ReadCDE\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			mark = AfxMarkMemory();

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading ReadCDE\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		/*AfxDumpMemory(mark, L"ReadCDE");*/

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Import Entry Point
//

CLINK BOOL DLLAPI Import(CString Path, CString Root, IMakeTags *pTags)
{
	FILE *pFile = _wfopen(Path, L"rb");

	if( pFile ) {
		
		CMakeTags Tags(Root, pTags);

		CDataFile File;

		CString   Rev;

		if( File.Import(pFile, Tags, Rev) ) {

			Tags.NewFolder(L"File");

			WIN32_FILE_ATTRIBUTE_DATA Data;

			SYSTEMTIME Write;

			GetFileAttributesEx(Path, GetFileExInfoStandard, &Data);

			FileTimeToLocalFileTime(&Data.ftLastWriteTime, &Data.ftLastAccessTime);

			FileTimeToSystemTime(&Data.ftLastAccessTime, &Write);

			DWORD t = 0;

			t += Time(Write.wHour, Write.wMinute, Write.wSecond);

			t += Date(Write.wYear, Write.wMonth,  Write.wDay);

			Path.Replace(L"\\", L"\\\\");

			Tags.EmitTextC(NULL, L"Name", Path);

			Tags.EmitTextC(NULL, L"Bare", CFilename(Path).GetBareName());

			Tags.EmitTextC(NULL, L"Path", CFilename(Path).GetDirectory());

			Tags.EmitSintC(NULL, L"Time", t);

			Tags.EmitTextC(NULL, L"Rev",  Rev);

			Tags.EndFolder();

			Tags.WarnRename();

			fclose(pFile);

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// End of File
