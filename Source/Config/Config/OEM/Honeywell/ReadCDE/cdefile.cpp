
#include "intern.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Constant Building
//

static inline CString Const(CString s)
{
	s.Replace(L"\"", L"\\\"");

	return L'"' + s + L'"';
	}

//////////////////////////////////////////////////////////////////////////
//
// Standard Read Method
//

#define StdRead(Class)						\
								\
	for( int i = 0; i < Header.m_nRecordCount; i++ ) {	\
								\
		Class Rec;					\
								\
		if( Rec.Read(pParse, Header) ) {		\
								\
			m_List.Append(Rec);			\
			}					\
		}						\
								\
	return TRUE;						\

//////////////////////////////////////////////////////////////////////////
//
// Standard FindDesc Method
//

#define StdFindDesc(Class)					\
								\
	for( UINT i = 0; i < m_List.GetCount(); i++ ) { 	\
								\
		Class &Rec = (Class &) m_List[i];		\
								\
		Rec.FindDescText(Desc); 			\
		}						\

//////////////////////////////////////////////////////////////////////////
//
// Standard Emit Method with Lookup
//

#define StdLook(Class, Folder, Name)				\
								\
	Tags.NewFolder(Folder); 				\
								\
	CUIntArray   Index;					\
								\
	CStringArray Valid, Name;				\
								\
	for( UINT i = 0; i < m_List.GetCount(); i++ ) { 	\
								\
		Class &Rec = (Class &) m_List[i];		\
								\
		if( Rec.m_nBlockNum ) {				\
								\
			Index.Append(Rec.m_nBlockNum);		\
								\
			Valid.Append(L"1");			\
								\
			Name.Append (Const(Rec.Name));		\
								\
			Rec.EmitTags(Tags);			\
			}					\
		}						\
								\
	Tags.EmitSintC (NULL, L"Count", Index.GetCount());	\
								\
	Tags.EmitLookup(NULL, L"Index", Index);			\
								\
	Tags.EmitLookup(NULL, L"Valid", Index, Valid, L"0");	\
								\
	Tags.EmitLookup(NULL, L"Name",  Index, Name,  L"\"\"");	\
								\
	Tags.EndFolder();					\

//////////////////////////////////////////////////////////////////////////
//
// Standard Emit Method with Lookup, CDataFile reference
//

#define StdLookWithFile(Class, Folder, Name, File)		\
								\
	Tags.NewFolder(Folder); 				\
								\
	CUIntArray   Index;					\
								\
	CStringArray Valid, Name;				\
								\
	for( UINT i = 0; i < m_List.GetCount(); i++ ) { 	\
								\
		Class &Rec = (Class &) m_List[i];		\
								\
		if( Rec.m_nBlockNum ) {				\
								\
			Index.Append(Rec.m_nBlockNum);		\
								\
			Valid.Append(L"1");			\
								\
			Name.Append (Const(Rec.Name));		\
								\
			Rec.EmitTags(Tags, File);		\
			}					\
		}						\
								\
	Tags.EmitSintC (NULL, L"Count", Index.GetCount());	\
								\
	Tags.EmitLookup(NULL, L"Index", Index);			\
								\
	Tags.EmitLookup(NULL, L"Valid", Index, Valid, L"0");	\
								\
	Tags.EmitLookup(NULL, L"Name",  Index, Name,  L"\"\"");	\
								\
	Tags.EndFolder();					\

//////////////////////////////////////////////////////////////////////////
//
// Standard Emit Method
//

#define StdEmit(Class, Folder)					\
								\
	Tags.NewFolder(Folder); 				\
								\
	for( UINT i = 0; i < m_List.GetCount(); i++ ) { 	\
								\
		Class &Rec = (Class &) m_List[i];		\
								\
		if( Rec.m_nBlockNum ) {				\
								\
			Rec.EmitTags(Tags);			\
			}					\
		}						\
								\
	Tags.EndFolder();					\

//////////////////////////////////////////////////////////////////////////
//
// Feature Encoding
//

#define EmitFeature(n, c, f)					\
								\
	Tags.EmitFlagC( NULL,					\
			(n),					\
			m_pDeviceCaps->HasFeature(f) > (c)	\
			)					\

//////////////////////////////////////////////////////////////////////////
//
// DevCap Encoding
//

#define EmitDevCap(n, c, f)					\
								\
	Tags.EmitFlagC( NULL,					\
			(n),					\
			m_pDeviceCaps->GetValue(f) >= (c)	\
			)					\

//////////////////////////////////////////////////////////////////////////
//
// Address Encoding
//

#define PropD(n) AddrBP(m_nBlockNum, DYNAMIC, (n))

#define PropS(n) AddrBP(m_nBlockNum, STATIC,  (n))

//////////////////////////////////////////////////////////////////////////
//
// Address Encoding
//

static CString AddrBP(UINT uBlock, UINT uTable, UINT uIndex)
{
	if( int(uBlock) == -1 ) {

		return L"";
		}

	switch( uTable ) {

		case STATIC:

			return CPrintf(L"[GS-%u-%u]", uBlock, uIndex);

		case DYNAMIC:

			return CPrintf(L"[GD-%u-%u]", uBlock, uIndex);
		}

	AfxAssert(FALSE);

	return L"";
	}

static CString AddrPS(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[PS-%u-%u]", uBlock, uIndex);
	}

static CString AddrDH(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[DH-%u-%u]", uBlock, uIndex);
	}

static CString AddrDP(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[DP-%u-%u]", uBlock, uIndex);
	}

static CString AddrDS(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[DS-%u-%u]", uBlock, uIndex);
	}

static CString AddrSD(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[SD-%u-%u]", uBlock, uIndex);
	}

static CString AddrSS(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[SS-%u-%u]", uBlock, uIndex);
	}

static CString AddrAS(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[AS-%u-%u]", uBlock, uIndex);
	}

static CString AddrND(UINT uBlock, UINT uIndex)
{
	return CPrintf(L"[ND-%u-%u]", uBlock, uIndex);
	}

static CString AddrIO(UINT uRack, UINT uSlot, UINT uChan)
{
	return CPrintf("\"%2.2u%2.2u%2.2u\"", uRack, uSlot, uChan);
	}

//////////////////////////////////////////////////////////////////////////
//
// Special Addresses
//

static CString SpecPeerText(UINT uIndex)
{
	return CPrintf(L"[XX-%u-%u]", 1, uIndex);
	}

static CString SpecPeerData(UINT uIndex)
{
	return CPrintf(L"[XX-%u-%u]", 2, uIndex);
	}

static CString SpecPPO(UINT uIndex)
{
	return CPrintf(L"[XX-%u-%u]", 3, uIndex);
	}

static CString SpecModSer(UINT uIndex)
{
	return CPrintf(L"[XX-%u-%u]", 4, uIndex);
	}

static CString SpecModTCP(UINT uIndex)
{
	return CPrintf(L"[XX-%u-%u]", 5, uIndex);
	}

static CString SpecHost(UINT uIndex)
{
	return CPrintf(L"[XX-%u-%u]", 6, uIndex);
	}

static CString SpecRecipeRcp(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 10, uParam);
	}

static CString SpecRecipeSpp(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 11, uParam);
	}

static CString SpecRecipeSps(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 12, uParam);
	}

static CString SpecRecipeSeq(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 13, uParam);
	}

static CString SpecAlarmInfo(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 20, uParam);
	}

static CString SpecSystem(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 100, uParam);
	}

static CString SpecPPOBlock(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 101, uParam);
	}

static CString SpecSigMarker(UINT uParam)
{
	return CPrintf(L"[XX-%u-%u]", 102, uParam);
	}

//////////////////////////////////////////////////////////////////////////
//
// Write Backs
//

static PCTXT WriteSP = L"(((int)SpState)&4)?"
		       L"(ControlSetup.LocalSetpoint=Data):"
		       L"(ControlSetup.LocalSetpoint2=Data)"
		       ;

static PCTXT WriteOP = L"ManualOutput=Data";

//////////////////////////////////////////////////////////////////////////
//
// CDE File Wrapper
//

// Constructor

CDataFile::CDataFile(void)
{
	}

// Destructor

CDataFile::~CDataFile(void)
{
	}

// Operations

BOOL CDataFile::Import(FILE *pFile, CMakeTags &Tags, CString &Rev)
{
	CParser *pParse = New CParser(pFile);

	if( m_FileHeader.Read(pParse) ) {

		// Create a device caps object pointed to by m_pDeviceCaps.
		// This object defines the capabilities of the particular
		// device type/revision whose tags are being imported. Use
		// m_pDeviceCaps in the code where different processing is
		// required depending on the device type/revision. Methods
		// GetValue and HasFeature on CDeviceCaps are particularly
		// useful in determining this device's capabilities.

		CInstrumentInfo * pInstInfo   = New CInstrumentInfo( m_FileHeader.m_nPlatformType,
								     m_FileHeader.m_nProductType,
								     m_FileHeader.m_nSchemaNumber,
								     m_FileHeader.m_nFeatureSet
								     );

		CDeviceType     * pDeviceType = m_DeviceTypeInfo.GetDeviceType(*pInstInfo);

		if( pDeviceType ) {

			m_pDeviceCaps = pDeviceType->CreateDeviceCapsObject(*pInstInfo);

			CInitData &Init = Tags.GetInitData();

			Init.AddWord(0x5678);

			Init.AddWord(0x0001);

			if( m_pDeviceCaps->m_nFeatureSet <= REV_4p2x ) {

				if( m_pDeviceCaps->m_nFeatureSet <= REV_2p1x ) {

					Init.AddWord(2);
					}
				else
					Init.AddWord(1);
				}
			else
				Init.AddWord(0);

			if( m_pDeviceCaps->HasFeature(FEATURE_VER2_SERIAL_NET_BLOCKS) ) {

				Init.AddWord(WORD(BLKNUM_NET_IF));
				}
			else
				Init.AddWord(WORD(BLKNUM_NTWK_E1));

			for( int nTable = 0; nTable < m_FileHeader.m_nNumberTables; nTable++ ) {

				m_TableHeader.Read(pParse);

				int  nPos = pParse->GetPos();

				BOOL fHit = TRUE;

				switch( m_TableHeader.m_nNumber ) {

					case DOCUMENT_TABLE:
						m_DocumentTable.Read(pParse, m_TableHeader);
						break;

					case DESCRIPTOR_TEXT_TABLE:
						m_DescTextTable.Read(pParse, m_TableHeader);
						break;

					case CONTROL_BLOCK_TABLE:
						m_ControlBlockTable.Read(pParse, m_TableHeader);
						break;

					case SIGNAL_ATTRIBUTE_TABLE:
						m_SignalTable.Read(pParse, m_TableHeader);
						break;

					case EVENT_DISPLAY_TABLE:
						m_EventDisplayTable.Read(pParse, m_TableHeader);
						break;

					case OVERVIEW_DISPLAY_TABLE:
						m_OverviewDisplayTable.Read(pParse, m_TableHeader);
						break;

					case LOOP_TAG_TABLE:
						m_LoopTagTable.Read(pParse, m_TableHeader);
						break;

					case SYSTEM_CONFIGURATION_TABLE:
						m_SysConfigTable.Read(pParse, m_TableHeader);
						break;

					case ALARM_GROUP_DISPLAY_TABLE:
						m_AlarmGroupDisplayTable.Read(pParse, m_TableHeader);
						break;

					case ALARM_HELP_TEXT_TABLE:
						m_AlarmHelpTextTable.Read(pParse, m_TableHeader);
						break;

					case GENERIC_TAG_TABLE:
						m_GenericTagTable.Read(pParse, m_TableHeader);
						break;

					case SPP_TAG_TABLE:
						m_SppTagTable.Read(pParse, m_TableHeader);
						break;

					case RECIPE_ALLOCATION_TABLE:
						m_RecipeAllocTable.Read(pParse, m_TableHeader);
						break;

					case TREND_DISPLAY_TABLE:
						m_TrendDisplayTable.Read(pParse, m_TableHeader);
						break;

					case BAR_DISPLAY_TABLE:
						m_BarDisplayTable.Read(pParse, m_TableHeader);
						break;

					case PANEL_DISPLAY_TABLE:
						m_PanelDisplayTable.Read(pParse, m_TableHeader);
						break;

					case MULTIPANEL_DISPLAY_TABLE:
						m_MultiPanelDisplayTable.Read(pParse, m_TableHeader);
						break;

					case MESSAGE_DISPLAY_TABLE:
						m_MessageDisplayTable.Read(pParse, m_TableHeader);
						break;

					case PANEL_METER_DISPLAY_TABLE:
						m_PanelMeterDisplayTable.Read(pParse, m_TableHeader);
						break;

					case SCREEN_BUTTON_DISPLAY_TABLE:
						m_ScreenButtonDspTable.Read(pParse, m_TableHeader);
						break;

					case PUSHBUTTON_DISPLAY_TABLE:
						m_PushbuttonDisplayTable.Read(pParse, m_TableHeader);
						break;

					case FOUR_SELECTOR_SWITCH_DISPLAY_TABLE:
						m_FssDisplayTable.Read(pParse, m_TableHeader);
						break;

					case SPS_DISPLAY_TABLE:
						m_SpsDisplayTable.Read(pParse, m_TableHeader);
						break;

					case STAGE_TAG_TABLE:
						m_StageTagTable.Read(pParse, m_TableHeader);
						break;

					case RAMP_TAG_TABLE:
						m_RampTagTable.Read(pParse, m_TableHeader);
						break;

					case ALTERNATOR_TAG_TABLE:
						m_AlternatorTagTable.Read(pParse, m_TableHeader);
						break;

					case HOA_TAG_TABLE:
						m_HoaTagTable.Read(pParse, m_TableHeader);
						break;

					case DEVICE_CONTROL_TAG_TABLE:
						m_DevCtlTagTable.Read(pParse, m_TableHeader);
						break;

					case DRUM_SEQUENCE_TAG_TABLE:
						m_DrumSeqTagTable.Read(pParse, m_TableHeader);
						break;

					case SPS_TAG_TABLE:
						m_SpsTagTable.Read(pParse, m_TableHeader);
						break;

					case CALENDAR_EVENT_DISPLAY_TABLE:
						m_CalEvtDisplayTable.Read(pParse, m_TableHeader);
						break;

					case WORKSHEET_TABLE:
						m_WorksheetTable.Read(pParse, m_TableHeader);
						break;

					case VARIABLE_TABLE:
						m_VariableTable.Read(pParse, m_TableHeader);
						break;

					case CALENDAR_EVENT_TABLE:
						m_CalEvtTagTable.Read(pParse, m_TableHeader);
						break;

					case PPO_TAG_TABLE:
						m_PpoTagTable.Read(pParse, m_TableHeader);
						break;

					case PEER_TABLE:
						m_PeerTable.Read(pParse, m_TableHeader);
						break;

					case ANALOG_INPUT_PT_SUMMARY_TABLE:
						m_AnalogInputTable.Read(pParse, m_TableHeader);
						break;

					case ANALOG_OUTPUT_PT_SUMMARY_TABLE:
						m_AnalogOutputTable.Read(pParse, m_TableHeader);
						break;

					case DIGITAL_INPUT_PT_SUMMARY_TABLE:
						m_DigitalInputTable.Read(pParse, m_TableHeader);
						break;

					case DIGITAL_OUTPUT_PT_SUMMARY_TABLE:
						m_DigitalOutputTable.Read(pParse, m_TableHeader);
						break;

					default:
						fHit = FALSE;
						break;
					}

				int nExt = m_TableHeader.m_nRecordCount * m_TableHeader.m_nRecordSize;

				int nEnd = nPos + nExt;

				if( fHit ) {

					Init.AddWord(WORD(m_TableHeader.m_nNumber));

					Init.AddWord(WORD(m_TableHeader.m_nCrc));

					if( pParse->GetPos() == nEnd ) {

						continue;
						}

					AfxAssert(FALSE);
					}

				pParse->SetPos(nEnd);
				}

			Init.AddWord(0xFFFF);

			delete pParse;

			Init.AddWord(WORD(m_RecipeAllocTable.m_uCount[0]));

			Init.AddWord(WORD(m_RecipeAllocTable.m_uCount[1]));

			Init.AddWord(WORD(m_RecipeAllocTable.m_uCount[2]));

			Init.AddWord(WORD(m_RecipeAllocTable.m_uCount[3]));

			FindDescText();

			EmitTags(Tags);

			EmitScreens();

			Rev = pDeviceType->GetDeviceNameRev(pDeviceType->HasInstInfo(*pInstInfo));

			delete m_pDeviceCaps;

			delete pInstInfo;

			return TRUE;
			}

		delete pInstInfo;
		}

	afxMainWnd->Error( CString(IDS_WRONG_VERSION_1) +
			   CString(IDS_WRONG_VERSION_2) +
			   CString(IDS_WRONG_VERSION_3) +
			   CString(IDS_WRONG_VERSION_4) +
			   CString(IDS_WRONG_VERSION_5)
			   );

	delete pParse;

	return FALSE;
	}

// File Header

BOOL CDataFile::CFileHeader::Read(CParser *pParse)
{
	m_nPlatformType  = pParse->ReadByte();
	m_nProductType	 = pParse->ReadByte();
	m_nSchemaNumber  = pParse->ReadWord();
	m_nNumberTables  = pParse->ReadByte();
	m_nDemoIndicator = pParse->ReadByte();
	m_nFeatureSet	 = pParse->ReadWord();
	m_nSafety	 = pParse->ReadByte();

	pParse->Skip(7);

	if( m_nFeatureSet < 10 && m_nFeatureSet != 3 ) {

		return FALSE;
		}

	return TRUE;
	}

// Table Header

BOOL CDataFile::CTableHeader::Read(CParser *pParse)
{
	pParse->ReadWord();

	m_nNumber      = pParse->ReadBigWord();
	m_nRecordCount = pParse->ReadBigWord();
	m_nRecordSize  = pParse->ReadBigWord();
	m_nModCtr      = pParse->ReadBigLong();
	m_nRevNum      = pParse->ReadByte();
	m_nInUse       = pParse->ReadByte();
	m_nCrc	       = pParse->ReadBigWord();

	return TRUE;
	}

// Document Table

BOOL CDataFile::CDocumentTable::Read(CParser *pParse, CTableHeader &Header)
{
	m_Title 	    = pParse->ReadString(34);
	m_Author	    = pParse->ReadString(34);
	m_DocumentPassword  = pParse->ReadString(16);
	m_WorksheetPassword = pParse->ReadString(16);
	m_lCreateTime	    = pParse->ReadLong();
	m_lModifyTime	    = pParse->ReadLong();

	return TRUE;
	}

// Descriptor Text

BOOL CDataFile::CDescTextTable::Read(CParser *pParse, CTableHeader &Header)
{
	pParse->ReadData(m_Data, Header.m_nRecordCount * Header.m_nRecordSize);

	return TRUE;
	}

CString CDataFile::CDescTextTable::GetTagDescriptor(int nPos)
{
	CString s;

	if( nPos >= 0 && nPos < int(m_Data.GetCount()) ) {

		int n = m_Data[nPos++];

		while( n-- ) {

			s += TCHAR(BYTE(m_Data[nPos++]));
			}

		s.TrimRight();
		}

	return s;
	}

// Control Block

BOOL CDataFile::CControlBlockRecord::Read(CParser *pParse, CTableHeader &Header)
{
	m_bBlockType	= pParse->ReadByte();
	m_bDeletedBlock = pParse->ReadByte();
	m_wBlockNum	= pParse->ReadBigWord();

	if( Header.m_nRevNum == 2 ) {

		m_lIsIndex = pParse->ReadWord();
		m_lSvIndex = pParse->ReadWord();
		m_lCvIndex = pParse->ReadWord();
		}

	if( Header.m_nRevNum >= 3 ) {

		m_lIsIndex = pParse->ReadLong();
		m_lSvIndex = pParse->ReadLong();
		m_lCvIndex = pParse->ReadLong();
		}

	if( Header.m_nRevNum == 4 ) {

		m_bySafety = pParse->ReadByte();

		pParse->Skip(3);
		}	

	if( !m_bDeletedBlock ) {

		if( m_wBlockNum > 100 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Control Blocks

BOOL CDataFile::CControlBlockTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CControlBlockRecord);
	}

// Signal Record

BOOL CDataFile::CSignalRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag = pParse->ReadString(8);
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag = pParse->ReadString(16);
		}

	m_Label0      = pParse->ReadString(6);
	m_Label1      = pParse->ReadString(6);
	m_nBlockNum   = pParse->ReadBigWord();
	m_nOutIndex   = pParse->ReadBigWord();
	m_nDecPlaces  = pParse->ReadByte();
	m_nTagType    = pParse->ReadByte() & 15;
	m_nDescOffset = pParse->ReadBigWord();
	m_fEvent      = FALSE;

	return TRUE;
	}

void CDataFile::CSignalRecord::FindDescText(CDescTextTable &Desc)
{
	m_Desc = Desc.GetTagDescriptor(m_nDescOffset);
	}

void CDataFile::CSignalRecord::EmitTags(CMakeTags &Tags, BOOL fAnalog)
{
	if( fAnalog ) {

		if( m_nTagType == 1 ) {

			m_Folder = Tags.NewFolder(L"AnalogSignal", m_Tag);

			Tags.EmitTextC(NULL,   L"Description", m_Desc);

			Tags.EmitTextC(NULL,   L"Units",       m_Label0);

			Tags.EmitRealM(L"Out", m_Tag,	       AddrND(m_nBlockNum, m_nOutIndex), m_nDecPlaces);

			Tags.EmitRealC(NULL,   L"Low Limit",   0,   1);

			Tags.EmitRealC(NULL,   L"High Limit",  100, 1);

			Tags.EndFolder();
			}
		}
	else {
		if( m_nTagType == 2 ) {

			m_Folder = Tags.NewFolder(L"DigitalSignal", m_Tag);

			Tags.EmitTextC(NULL,   L"Description", m_Desc);

			Tags.EmitEnumM(L"Out", m_Tag,	       AddrND(m_nBlockNum, m_nOutIndex), m_Label0, m_Label1);

			Tags.EndFolder();
			}
		}
	}

int CDataFile::CSignalRecord::operator > (CSignalRecord const &That) const
{
	return m_Tag > That.m_Tag;
	}

// Signals

BOOL CDataFile::CSignalTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CSignalRecord);
	}

void CDataFile::CSignalTable::FindDescText(CDescTextTable &Desc)
{
	StdFindDesc(CSignalRecord);
	}

void CDataFile::CSignalTable::EmitAnalog(CMakeTags &Tags)
{
	Tags.NewFolder(L"AnalogSignals");

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CSignalRecord &Rec = (CSignalRecord &) m_List[i];

		Rec.EmitTags(Tags, TRUE);
		}

	Tags.EndFolder();
	}

void CDataFile::CSignalTable::EmitDigital(CMakeTags &Tags)
{
	Tags.NewFolder(L"DigitalSignals");

	for( UINT j = 0; j < m_List.GetCount(); j++ ) {

		CSignalRecord &Rec = (CSignalRecord &) m_List[j];

		Rec.EmitTags(Tags, FALSE);
		}

	Tags.EndFolder();
	}

void CDataFile::CSignalTable::EmitLookup(CMakeTags &Tags)
{
	Tags.NewFolder(L"Signals");

	if( TRUE ) {

		Tags.NewFolder(L"Interest");

		for( UINT i = 0; i < m_List.GetCount(); i++ ) {

			CSignalRecord &Rec = (CSignalRecord &) m_List[i];

			if( Rec.m_nTagType == 2 ) {

				if( Rec.m_fEvent ) {

					Tags.EmitSintM(NULL, Rec.m_Tag, SpecSigMarker(1+i));
					}
				}
			}

		Tags.EndFolder();
		}

	CUIntArray   Index, Analog, Digital;

	CStringArray Valid, Name, Desc, Data, Text, Type, Units, Text0, Text1;

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CSignalRecord &Rec = (CSignalRecord &) m_List[i];

		if( Rec.m_nTagType ) {

			Index.Append(1+i);

			Valid.Append(L"1");

			Name.Append(Const(Rec.m_Tag));

			Desc.Append(Const(Rec.m_Desc));

			Data.Append(CPrintf(L"MakeFloat(%s)", AddrND(Rec.m_nBlockNum, Rec.m_nOutIndex)));

			Text.Append(CPrintf(L"%s.Out.AsText", Rec.m_Folder));

			Type.Append(CPrintf(L"%u", Rec.m_nTagType));
			}

		if( Rec.m_nTagType == 1 ) {

			Analog.Append(1+i);

			Units.Append(Const(Rec.m_Label0));
			}

		if( Rec.m_nTagType == 2 ) {

			Digital.Append(1+i);

			Text0.Append(Const(Rec.m_Label0));

			Text1.Append(Const(Rec.m_Label1));
			}
		}

	Tags.EmitSintC (NULL, L"Count",  Index.GetCount());

	Tags.EmitLookup(NULL, L"Index",  Index);

	Tags.EmitLookup(NULL, L"Valid",  Index,   Valid, L"0");

	Tags.EmitLookup(NULL, L"Name",   Index,   Name,  L"\"\"");

	Tags.EmitLookup(NULL, L"Desc",   Index,   Desc,  L"\"\"");

	Tags.EmitLookup(NULL, L"Data",   Index,   Data,  L"0.0");

	Tags.EmitLookup(NULL, L"AsText", Index,   Text,  L"\"\"");

	Tags.EmitLookup(NULL, L"Type",   Index,   Type,  L"0");

	Tags.EmitLookup(NULL, L"Units",  Analog,  Units, L"\"\"");

	Tags.EmitLookup(NULL, L"Text0",  Digital, Text0, L"\"\"");

	Tags.EmitLookup(NULL, L"Text1",  Digital, Text1, L"\"\"");

	Tags.EndFolder();
	}

// Overview Displays

BOOL CDataFile::COverviewDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			pParse->Skip(12*2 + 12*4 + 12*4);

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

// Event Displays

BOOL CDataFile::CEventDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	memset(m_Sigs, 0, sizeof(m_Sigs));

	m_Title = pParse->ReadString(24);

	UINT ns = (Header.m_nRevNum == 2) ? 64: 32;

	for( UINT i = 0; i < ns; i++ ) {

		m_Sigs[i] = pParse->ReadBigWord();
		}

	return TRUE;
	}

void CDataFile::CEventDisplayTable::EmitTags(CMakeTags &Tags, CSignalTable &Sigs)
{
	for( UINT i = 0; i < 64; i++ ) {

		UINT uSig = m_Sigs[i];

		if( uSig ) {

			CSignalRecord &Sig = (CSignalRecord &) Sigs.m_List[uSig-1];

			Sig.m_fEvent = 1;
			}
		}
	}

// Loop Tag

BOOL CDataFile::CLoopTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag	     = pParse->ReadString(8);
		m_Desc	     = pParse->ReadString(16);
		m_Units      = pParse->ReadString(4);
		m_nBlockNum  = pParse->ReadBigWord();
		m_nDecimals  = pParse->ReadByte();
		m_nBlockType = pParse->ReadByte();
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag	     = pParse->ReadString(16);
		m_Desc	     = pParse->ReadString(16);
		m_Units      = pParse->ReadString(6);
		m_nBlockNum  = pParse->ReadBigWord();
		m_nDecimals  = pParse->ReadByte();
		m_nBlockType = pParse->ReadByte();
		}

	switch( m_nBlockType ) {

		case OMC_PID:
			m_nBlockEnum = 0;
			break;

		case OMC_CARBON:
			m_nBlockEnum = 3;
			break;

		case OMC_ONOFF:
			m_nBlockEnum = 1;
			break;

		case OMC_THREE_POS:
			m_nBlockEnum = 2;
			break;

		case OMC_AMB:
			m_nBlockEnum = 4;
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

void CDataFile::CLoopTagRecord::EmitTags(CMakeTags &Tags, CDataFile &DataFile)
{
	switch( m_nBlockEnum ) {

		case 0:
			Tags.NewFolder(L"Loop.PID", m_Tag);

			EmitTagsPID(Tags, DataFile);

			break;

		case 1:
			Tags.NewFolder(L"Loop.OnOff", m_Tag);

			EmitTagsOnOff(Tags, DataFile);

			break;

		case 2:
			Tags.NewFolder(L"Loop.3Pos", m_Tag);

			EmitTags3Pos(Tags, DataFile);

			break;

		case 3:
			Tags.NewFolder(L"Loop.Carbon", m_Tag);

			EmitTagsCarbon(Tags, DataFile);

			break;

		case 4:
			Tags.NewFolder(L"Loop.AMB", m_Tag);

			EmitTagsAMB(Tags, DataFile);

			break;
		}

	Tags.EmitSintC(NULL, L"LoopType", m_nBlockEnum);

	Tags.EndFolder();
	}

void CDataFile::CLoopTagRecord::EmitTagsPID(CMakeTags &Tags, CDataFile &DataFile)
{
	int nVersion = DataFile.m_pDeviceCaps->GetValue(DEVCAPINT_BLOCK_VERSION_PID);

	int nIndex   = (nVersion == 1) ? 3 : 0;

	int p17      = 17 - nIndex;
	int p18      = 18 - nIndex;
	int p19      = 19 - nIndex;
	int p20      = 20 - nIndex;
	int p21      = 21 - nIndex;
	int p23      = 23 - nIndex;
	int p24      = 24 - nIndex;
	int p26      = 26 - nIndex;
	int p32      = 32 - nIndex;

	// TuneConsts Folder

	Tags.NewFolder(L"TuneConsts");

	Tags.EmitEnumM(NULL, L"Tuning Status",	 PropD(p23), Hc900InactiveActiveEnum);

	Tags.EmitFnumW(NULL, L"Fuzzy Overshoot", PropS( 34), Hc900OffOnEnum);

	if( nVersion == 1 ) {

		Tags.EmitEnumW(NULL, L"Accutune", PropS(33), Hc900LoopAccutuneStatusVersion1Enum);
		}
	else {
		Tags.EmitEnumW(NULL, L"Accutune Status",	PropD(14), Hc900LoopAccutuneStatusEnum);
		Tags.EmitEnumW(NULL, L"PV Adaptive Tuning",	PropS(60), Hc900LoopPvAdaptiveTuningEnum);
		Tags.EmitEnumW(NULL, L"Accutune III Type",	PropS(33), Hc900LoopAccutuneIIItypeEnum);
		Tags.EmitEnumW(NULL, L"Tuning Criteria",	PropS(55), Hc900LoopTuningCriteriaEnum);
		Tags.EmitRealW(NULL, L"SP Step Change", 	PropS(57), 0, 5, 15);
		Tags.EmitEnumW(NULL, L"SP Tune Step Direction", PropS(58), Hc900UpDownEnum);
		Tags.EmitRealW(NULL, L"Process Gain",		PropS(59), 2, 0.10, 10);
		Tags.EmitEnumW(NULL, L"Duplex Tuning",		PropS(56), Hc900LoopDuplexTuningEnum);
		}

	Tags.EmitFnumW(NULL, L"Start Tune",	   PropD( 6), Hc900NormalStartEnum);
	Tags.EmitFnumW(NULL, L"Switch Tune Set",   PropS(35), Hc900LoopSwitchTuneSetEnum);
	Tags.EmitRealW(NULL, L"Gain1",		   PropS( 0), 2, 0.1, 1000);
	Tags.EmitRealW(NULL, L"Rate1",		   PropS( 1), 2, 0, 10);
	Tags.EmitRealW(NULL, L"Reset1", 	   PropS( 2), 2, 0, 50);
	Tags.EmitRealW(NULL, L"Gain2",		   PropS(36), 2, 0.1, 1000);
	Tags.EmitRealW(NULL, L"Rate2",		   PropS(37), 2, 0, 10);
	Tags.EmitRealW(NULL, L"Reset2", 	   PropS(38), 2, 0, 50);
	Tags.EmitRealW(NULL, L"Feed Forward Gain", PropS(43), 2, 0, 10);
	Tags.EmitRealW(NULL, L"Manual Reset",	   PropS(32), 2, -100, 100);

	Tags.EndFolder();

	// ControlSetup Folder

	Tags.NewFolder(L"ControlSetup");

	Tags.EmitRealW(NULL, L"Local Setpoint",	   PropD(  1), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Local Setpoint 2",  PropD(  2), m_nDecimals, -99999, 99999);
	Tags.EmitRealM(NULL, L"Remote Setpoint",   PropD(p26), 0);
	Tags.EmitFnumW(NULL, L"Switch Set Points", PropD(  3), Hc900LspRspEnum);
	Tags.EmitRealW(NULL, L"Fail Safe Output",  PropS( 22), 2, -5, 105);
	Tags.EmitRealW(NULL, L"PV Low Limit",	   PropS(  5), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"PV High Limit",	   PropS(  4), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Low Limit",	   PropS( 18), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP High Limit",	   PropS( 17), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Up",	   PropS( 42), m_nDecimals, 0, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Down",	   PropS( 41), m_nDecimals, 0, 99999);
	Tags.EmitRealW(NULL, L"Output Low Limit",  PropS( 21), m_nDecimals, -5, 105);
	Tags.EmitRealW(NULL, L"Output High Limit", PropS( 20), m_nDecimals, -5, 105);
	Tags.EmitRealW(NULL, L"Ratio",		   PropS( 45), 2,    -20,    20);
	Tags.EmitRealW(NULL, L"Local Bias",	   PropS( 46), m_nDecimals, -99999, 99999);
	Tags.EmitRealM(NULL, L"Remote Bias",	   PropD(p32), 0);

	Tags.EndFolder();

	// AlarmSetpoints Folder

	Tags.NewFolder(L"AlarmSetpoints");

	Tags.EmitEnumW(NULL, L"Alarm1 Type1",	  PropS(27), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint1", PropS(23), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm1 Type2",	  PropS(28), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint2", PropS(24), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type1",	  PropS(29), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint1", PropS(25), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type2",	  PropS(30), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint2", PropS(26), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Alarm Hysteresis",  PropS(31), 2, 0, 5);

	Tags.EndFolder();

	// HighOutputLimiting Folder

	Tags.NewFolder(L"HighOutputLimiting");

	Tags.EmitEnumM(NULL, L"High Output Limit Enable", PropS(51), Hc900NoYesEnum);
	Tags.EmitEnumM(NULL, L"High Output Limit Status", PropD( 9), Hc900LoopHighOutputLimitStatusEnum);
	Tags.EmitRealM(NULL, L"Current High Out Limit",   PropD(10), m_nDecimals);
	Tags.EmitTimeM(NULL, L"Time In Override",	  PropD(11), 1);
	Tags.EmitRealW(NULL, L"Delay Time",		  PropS(52), 2, 0, 99999);
	Tags.EmitRealW(NULL, L"Ramp Rate",		  PropS(53), 1, 0, 99999);

	Tags.EndFolder();

	// Root

	Tags.EmitTextC(NULL, L"Block Version", CPrintf(L"%d", nVersion));

	Tags.EmitTextC(NULL, L"Description",   m_Desc);
	Tags.EmitTextC(NULL, L"Units",	       m_Units);

	Tags.EmitFnumM(NULL, L"AM State",	   PropD(p19), Hc900AutoManualEnum);
	Tags.EmitFnumM(NULL, L"SP State",	   PropD(p19), Hc900LspRspStateEnum);
	Tags.EmitFlagW(NULL, L"AM Select",	   PropD(  4)  );
	Tags.EmitFlagW(NULL, L"SP Select",	   PropD(  3)  );
	Tags.EmitRealW(NULL, L"Manual Output",     PropD(  5), m_nDecimals, -5, 105);
	Tags.EmitEnumM(NULL, L"Alarm1", 	   PropD(p20), Hc900NormalAlarmEnum);
	Tags.EmitEnumM(NULL, L"Alarm2", 	   PropD(p21), Hc900NormalAlarmEnum);
	Tags.EmitFnumM(NULL, L"Direct Reverse",    PropD(p24), Hc900ReverseDirectEnum);
	Tags.EmitEnumM(NULL, L"Control Algorithm", PropS(  3), Hc900ControlAlgorithmEnum);
	Tags.EmitEnumM(NULL, L"Use Prop Band",	   PropS( 39), Hc900LoopGainPropBandEnum);
	Tags.EmitEnumM(NULL, L"Use RPM",	   PropS( 40), Hc900LoopMinutesRpmEnum);
	Tags.EmitEnumM(NULL, L"Ratio Bias",	   PropS( 44), Hc900LoopRatioBiasEnum);

	Tags.EmitRealM(NULL, L"PV",	           PropD( 13), m_nDecimals, -99999, 99999  );
	Tags.EmitRealX(NULL, L"SP",		   PropD(p17), m_nDecimals, -99999, 99999, WriteSP);
	Tags.EmitRealX(NULL, L"Output",		   PropD(p18), m_nDecimals, -5,     105,   WriteOP);
	}

void CDataFile::CLoopTagRecord::EmitTagsOnOff(CMakeTags &Tags, CDataFile &DataFile)
{
	// ControlSetup Folder

	Tags.NewFolder(L"ControlSetup");

	Tags.EmitRealW(NULL, L"Local Setpoint",    PropD( 1), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Local Setpoint 2",  PropD( 2), m_nDecimals, -99999, 99999);
	Tags.EmitRealM(NULL, L"Remote Setpoint",   PropD(18), m_nDecimals);
	Tags.EmitFnumW(NULL, L"Switch Set Points", PropD( 3), Hc900LspRspEnum);
	Tags.EmitFnumW(NULL, L"Failsafe Output",   PropS(29), Hc900OffOnEnum);
	Tags.EmitRealW(NULL, L"Hysteresis",	   PropS(19), 1, 0, 10);
	Tags.EmitRealW(NULL, L"PV Low Limit",	   PropS( 1), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"PV High Limit",	   PropS( 0), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Low Limit",	   PropS(13), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP High Limit",	   PropS(12), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Up",	   PropS(16), m_nDecimals, 0, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Down",	   PropS(15), m_nDecimals, 0, 99999);

	Tags.EndFolder();

	// AlarmSetpoints Folder

	Tags.NewFolder(L"AlarmSetpoints");

	Tags.EmitEnumW(NULL, L"Alarm1 Type1",	  PropS(24), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint1", PropS(20), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm1 Type2",	  PropS(25), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint2", PropS(21), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type1",	  PropS(26), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint1", PropS(22), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type2",	  PropS(27), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint2", PropS(23), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Alarm Hysteresis",  PropS(28), 2, 0, 5);

	Tags.EndFolder();

	// Root

	Tags.EmitTextC(NULL, L"Description", m_Desc);
	Tags.EmitTextC(NULL, L"Units",	     m_Units);

	Tags.EmitFlagW(NULL, L"Output Toggle",  PropD(5));

	Tags.EmitFnumM(NULL, L"AM State",	PropD(12), Hc900AutoManualEnum);
	Tags.EmitFnumM(NULL, L"SP State",	PropD(12), Hc900LspRspStateEnum);
	Tags.EmitFlagW(NULL, L"AM Select",	PropD( 4)  );
	Tags.EmitFlagW(NULL, L"SP Select",	PropD( 3)  );
	Tags.EmitFnumW(NULL, L"Manual Output",  PropD( 5), Hc900OffOnEnum);
	Tags.EmitEnumM(NULL, L"Alarm1", 	PropD(13), Hc900NormalAlarmEnum);
	Tags.EmitEnumM(NULL, L"Alarm2", 	PropD(14), Hc900NormalAlarmEnum);
	Tags.EmitEnumM(NULL, L"Direct Reverse", PropD(16), Hc900ReverseDirectEnum);

	Tags.EmitRealM(NULL, L"PV",	        PropD( 8), m_nDecimals     );
	Tags.EmitRealX(NULL, L"SP",		PropD(10), m_nDecimals,    WriteSP);
	Tags.EmitFnumX(NULL, L"Output",		PropD(11), Hc900OffOnEnum, WriteOP);
	}

void CDataFile::CLoopTagRecord::EmitTags3Pos(CMakeTags &Tags, CDataFile &DataFile)
{
	// TuneConsts Folder

	Tags.NewFolder(L"TuneConsts");

	Tags.EmitEnumM(NULL, L"Tuning Status",	 PropD(21), Hc900InactiveActiveEnum);
	Tags.EmitEnumW(NULL, L"Accutune",	 PropS(32), Hc900DisableEnableEnum);
	Tags.EmitFnumW(NULL, L"Fuzzy Overshoot", PropS(35), Hc900OffOnEnum);
	Tags.EmitFnumW(NULL, L"Start Tune",	 PropD( 6), Hc900NormalStartEnum);
	Tags.EmitFnumW(NULL, L"Switch Tune Set", PropS(36), Hc900LoopSwitchTuneSetEnum);
	Tags.EmitRealW(NULL, L"Gain1",		 PropS( 0), 2, 0.1, 1000);
	Tags.EmitRealW(NULL, L"Rate1",		 PropS( 1), 2,	 0, 10);
	Tags.EmitRealW(NULL, L"Reset1", 	 PropS( 2), 2,	 0, 50);
	Tags.EmitRealW(NULL, L"Gain2",		 PropS(37), 2, 0.1, 1000);
	Tags.EmitRealW(NULL, L"Rate2",		 PropS(38), 2,	 0, 10);
	Tags.EmitRealW(NULL, L"Reset2", 	 PropS(39), 2,	 0, 50);

	Tags.EndFolder();

	// ControlSetup Folder

	Tags.NewFolder(L"ControlSetup");

	Tags.EmitRealW(NULL, L"Local Setpoint",       PropD( 1), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Local Setpoint 2",     PropD( 2), m_nDecimals, -99999, 99999);
	Tags.EmitRealM(NULL, L"Remote Setpoint",      PropD(24), m_nDecimals);
	Tags.EmitFnumW(NULL, L"Switch Set Points",    PropD( 3), Hc900LspRspEnum);
	Tags.EmitFnumW(NULL, L"Failsafe Output",      PropS(21), Hc900OffOnEnum);
	Tags.EmitRealW(NULL, L"PV Low Limit",	      PropS( 4), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"PV High Limit",	      PropS( 3), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Low Limit",	      PropS(19), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP High Limit",	      PropS(18), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Up",	      PropS(43), m_nDecimals, 0, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Down",	      PropS(42), m_nDecimals, 0, 99999);
	Tags.EmitRealW(NULL, L"At Output Low Limit",  PropS(34), m_nDecimals, 0, 100);
	Tags.EmitRealW(NULL, L"At Output High Limit", PropS(33), m_nDecimals, 0, 100);
	Tags.EmitRealW(NULL, L"Motor Deadband",       PropS(51), 2, 0.5, 5);
	Tags.EmitSintW(NULL, L"Motor Traverse Time",  PropS(52), 5, 1800);
	Tags.EmitRealW(NULL, L"Ratio",		      PropS(45), 2, -20, 20);
	Tags.EmitRealW(NULL, L"Local Bias",	      PropS(46), 2, -99999, 99999);
	Tags.EmitRealW(NULL, L"Remote Bias",	      PropD(28), m_nDecimals);

	Tags.EndFolder();

	// AlarmSetpoints Folder

	Tags.NewFolder(L"AlarmSetpoints");

	Tags.EmitEnumW(NULL, L"Alarm1 Type1",	  PropS(26), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint1", PropS(22), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm1 Type2",	  PropS(27), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint2", PropS(23), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type1",	  PropS(28), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint1", PropS(24), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type2",	  PropS(29), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint2", PropS(25), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Alarm Hysteresis",  PropS(30), 2, 0, 5);

	Tags.EndFolder();

	// Root

	Tags.EmitTextC(NULL, L"Description", m_Desc);
	Tags.EmitTextC(NULL, L"Units",	     m_Units);

	Tags.EmitFnumM(NULL, L"AM State",	PropD(17), Hc900AutoManualEnum);
	Tags.EmitFnumM(NULL, L"SP State",	PropD(17), Hc900LspRspStateEnum);
	Tags.EmitFlagW(NULL, L"AM Select",	PropD( 4)  );
	Tags.EmitFlagW(NULL, L"SP Select",	PropD( 3)  );
	Tags.EmitRealW(NULL, L"Manual Output",  PropD( 5), m_nDecimals);
	Tags.EmitEnumM(NULL, L"Alarm1", 	PropD(18), Hc900NormalAlarmEnum);
	Tags.EmitEnumM(NULL, L"Alarm2", 	PropD(19), Hc900NormalAlarmEnum);
	Tags.EmitEnumM(NULL, L"Direct Reverse", PropD(15), Hc900ReverseDirectEnum);
	Tags.EmitEnumM(NULL, L"Use Prop Band",	PropS(40), Hc900LoopGainPropBandEnum);
	Tags.EmitEnumM(NULL, L"Use RPM",	PropS(41), Hc900LoopMinutesRpmEnum);
	Tags.EmitEnumM(NULL, L"Ratio Bias",	PropS(44), Hc900LoopRatioBiasEnum);

	Tags.EmitRealM(NULL, L"PV",	        PropD( 9), m_nDecimals                  );
	Tags.EmitRealX(NULL, L"SP",	        PropD(16), m_nDecimals,          WriteSP);
	Tags.EmitRealX(NULL, L"Output",		PropD(12), m_nDecimals, -5, 105, WriteOP);
	}

void CDataFile::CLoopTagRecord::EmitTagsCarbon(CMakeTags &Tags, CDataFile &DataFile)
{
	int nVersion = DataFile.m_pDeviceCaps->GetValue(DEVCAPINT_BLOCK_VERSION_CARBON);

	int nIndexA  = (nVersion == 1) ?  3 : 0;

	int nIndexB  = (nVersion == 1) ? 11 : 0;

	int a17      = 17 - nIndexA;
	int a18      = 18 - nIndexA;
	int a19      = 19 - nIndexA;
	int a20      = 20 - nIndexA;
	int a21      = 21 - nIndexA;
	int a23      = 23 - nIndexA;
	int a24      = 24 - nIndexA;
	int a26      = 26 - nIndexA;
	int a32      = 32 - nIndexA;
	int a35      = 35 - nIndexA;

	int b66      = 66 - nIndexB;
	int b67      = 67 - nIndexB;
	int b68      = 68 - nIndexB;
	int b69      = 69 - nIndexB;
	int b73      = 73 - nIndexB;

	// TuneConsts Folder

	Tags.NewFolder(L"TuneConsts");

	Tags.EmitEnumM(NULL, L"Tuning Status",	 PropD(a23), Hc900InactiveActiveEnum);

	Tags.EmitFnumW(NULL, L"Fuzzy Overshoot", PropS( 34), Hc900OffOnEnum);

	if( nVersion == 1 ) {

		Tags.EmitEnumW(NULL, L"Accutune", PropS(33), Hc900LoopAccutuneStatusVersion1Enum);
		}
	else {
		Tags.EmitEnumW(NULL, L"Accutune Status",	PropD(14), Hc900LoopAccutuneStatusEnum);
		Tags.EmitEnumW(NULL, L"PV Adaptive Tuning",	PropS(60), Hc900LoopPvAdaptiveTuningEnum);
		Tags.EmitEnumW(NULL, L"Accutune III Type",	PropS(33), Hc900LoopAccutuneIIItypeEnum);
		Tags.EmitEnumW(NULL, L"Tuning Criteria",	PropS(55), Hc900LoopTuningCriteriaEnum);
		Tags.EmitRealW(NULL, L"SP Step Change", 	PropS(57), 0, 5, 15);
		Tags.EmitEnumW(NULL, L"SP Tune Step Direction", PropS(58), Hc900UpDownEnum);
		Tags.EmitRealW(NULL, L"Process Gain",		PropS(59), 2, 0.10, 10);
		Tags.EmitEnumW(NULL, L"Duplex Tuning",		PropS(56), Hc900LoopDuplexTuningEnum);
		}

	Tags.EmitFnumW(NULL, L"Start Tune",	   PropD( 6), Hc900NormalStartEnum);
	Tags.EmitFnumW(NULL, L"Switch Tune Set",   PropS(35), Hc900LoopSwitchTuneSetEnum);
	Tags.EmitRealW(NULL, L"Gain1",		   PropS( 0), 2, 0.1, 1000);
	Tags.EmitRealW(NULL, L"Rate1",		   PropS( 1), 2, 0, 10);
	Tags.EmitRealW(NULL, L"Reset1", 	   PropS( 2), 2, 0, 50);
	Tags.EmitRealW(NULL, L"Gain2",		   PropS(36), 2, 0.1, 1000);
	Tags.EmitRealW(NULL, L"Rate2",		   PropS(37), 2, 0, 10);
	Tags.EmitRealW(NULL, L"Reset2", 	   PropS(38), 2, 0, 50);
	Tags.EmitRealW(NULL, L"Feed Forward Gain", PropS(43), 2, 0, 10);
	Tags.EmitRealW(NULL, L"Manual Reset",	   PropS(32), 2, -100, 100);

	Tags.EndFolder();

	// ControlSetup Folder

	Tags.NewFolder(L"ControlSetup");

	Tags.EmitRealW(NULL, L"Local Setpoint",    PropD(  1), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Local Setpoint 2",  PropD(  2), m_nDecimals, -99999, 99999);
	Tags.EmitRealM(NULL, L"Remote Setpoint",   PropD(a26), m_nDecimals);
	Tags.EmitFnumW(NULL, L"Switch Set Points", PropD(  3), Hc900LspRspEnum);
	Tags.EmitRealW(NULL, L"Failsafe Output",   PropS( 22), 2, -5, 105);
	Tags.EmitRealW(NULL, L"PV Low Limit",	   PropS(  5), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"PV High Limit",	   PropS(  4), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Low Limit",	   PropS( 18), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP High Limit",	   PropS( 17), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Up",	   PropS( 42), m_nDecimals, 0, 99999);
	Tags.EmitRealW(NULL, L"SP Rate Down",	   PropS( 41), m_nDecimals, 0, 99999);
	Tags.EmitRealW(NULL, L"Output Low Limit",  PropS( 21), m_nDecimals, -5, 105);
	Tags.EmitRealW(NULL, L"Output High Limit", PropS( 20), m_nDecimals, -5, 105);
	Tags.EmitRealW(NULL, L"Furnace Factor",    PropS(b68), 2, -0.5, 0.5);
	Tags.EmitFnumW(NULL, L"Anti Sooting",	   PropS(b69), Hc900OffOnEnum);
	Tags.EmitRealW(NULL, L"Percent Hydrogen",  PropS(b73), 2, 1, 100);
	Tags.EmitRealW(NULL, L"Local Percent CO",  PropS(b66), 2, 2, 35);
	Tags.EmitRealM(NULL, L"Remote Percent CO", PropD(a35), m_nDecimals);
	Tags.EmitRealW(NULL, L"Ratio",		   PropS( 45), 2, -20, 20);
	Tags.EmitRealW(NULL, L"Local Bias",	   PropS( 46), 2, -99999, 99999);
	Tags.EmitRealM(NULL, L"Remote Bias",	   PropD(a32), m_nDecimals);

	Tags.EndFolder();

	// AlarmSetpoints Folder

	Tags.NewFolder(L"AlarmSetpoints");

	Tags.EmitEnumW(NULL, L"Alarm1 Type1",	  PropS(27), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint1", PropS(23), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm1 Type2",	  PropS(28), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint2", PropS(24), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type1",	  PropS(29), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint1", PropS(25), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type2",	  PropS(30), Hc900AlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint2", PropS(26), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Alarm Hysteresis",  PropS(31), 2, 0, 5);

	Tags.EndFolder();

	/// HighOutputLimiting Folder

	Tags.NewFolder(L"HighOutputLimiting");

	Tags.EmitEnumM(NULL, L"High Output Limit Enable", PropS(51), Hc900NoYesEnum);
	Tags.EmitEnumM(NULL, L"High Output Limit Status", PropD(9),  Hc900LoopHighOutputLimitStatusEnum);
	Tags.EmitRealM(NULL, L"Current High Out Limit",   PropD(10), m_nDecimals);
	Tags.EmitTimeM(NULL, L"Time In Override",	  PropD(11), 1);
	Tags.EmitRealW(NULL, L"Delay Time",		  PropS(52), 2, 0, 99999);
	Tags.EmitRealW(NULL, L"Ramp Rate",		  PropS(53), 1, 0, 99999);

	Tags.EndFolder();

	// Root

	Tags.EmitTextC(NULL, L"Block Version", CPrintf(L"%d", nVersion));

	Tags.EmitTextC(NULL, L"Description", m_Desc);
	Tags.EmitTextC(NULL, L"Units",	     m_Units);

	Tags.EmitFnumM(NULL, L"AM State",	   PropD(a19), Hc900AutoManualEnum);
	Tags.EmitFnumM(NULL, L"SP State",	   PropD(a19), Hc900LspRspStateEnum);
	Tags.EmitFlagW(NULL, L"AM Select",	   PropD(  4)  );
	Tags.EmitFlagW(NULL, L"SP Select",	   PropD(  3)  );
	Tags.EmitRealW(NULL, L"Manual Output",     PropD(  5), m_nDecimals, -5, 105);
	Tags.EmitEnumM(NULL, L"Alarm1", 	   PropD(a20), Hc900NormalAlarmEnum);
	Tags.EmitEnumM(NULL, L"Alarm2", 	   PropD(a21), Hc900NormalAlarmEnum);
	Tags.EmitEnumM(NULL, L"Direct Reverse",    PropD(a24), Hc900ReverseDirectEnum);
	Tags.EmitEnumM(NULL, L"Control Algorithm", PropS(  3), Hc900ControlAlgorithmEnum);
	Tags.EmitEnumM(NULL, L"Use Prop Band",	   PropS( 39), Hc900LoopGainPropBandEnum);
	Tags.EmitEnumM(NULL, L"Use RPM",	   PropS( 40), Hc900LoopMinutesRpmEnum);
	Tags.EmitEnumM(NULL, L"Ratio Bias",	   PropS( 44), Hc900LoopRatioBiasEnum);
	Tags.EmitEnumM(NULL, L"Use Remote CO",	   PropS(b67), Hc900OffOnEnum);

	Tags.EmitRealM(NULL, L"PV",	           PropD( 13), m_nDecimals, -99999, 99999         );
	Tags.EmitRealX(NULL, L"SP",		   PropD(a17), m_nDecimals, -99999, 99999, WriteSP);
	Tags.EmitRealX(NULL, L"Output",		   PropD(a18), m_nDecimals, -5,     105,   WriteOP);
	}

void CDataFile::CLoopTagRecord::EmitTagsAMB(CMakeTags &Tags, CDataFile &DataFile)
{
	// ControlSetup Folder

	Tags.NewFolder(L"ControlSetup");

	Tags.EmitRealW(NULL, L"Failsafe Output",   PropS(9), 2, -5, 105);
	Tags.EmitRealW(NULL, L"PV Low Limit",	   PropS(1), m_nDecimals, -5, 105);
	Tags.EmitRealW(NULL, L"PV High Limit",	   PropS(0), m_nDecimals, -5, 105);
	Tags.EmitRealW(NULL, L"Output Low Limit",  PropS(8), m_nDecimals, -5, 105);
	Tags.EmitRealW(NULL, L"Output High Limit", PropS(7), m_nDecimals, -5, 105);

	Tags.EndFolder();

	// AlarmSetpoints Folder

	Tags.NewFolder(L"AlarmSetpoints");

	Tags.EmitEnumW(NULL, L"Alarm1 Type1",	  PropS(14), Hc900AMBiasAlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint1", PropS(10), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm1 Type2",	  PropS(15), Hc900AMBiasAlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm1 Setpoint2", PropS(11), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type1",	  PropS(16), Hc900AMBiasAlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint1", PropS(12), m_nDecimals, -99999, 99999);
	Tags.EmitEnumW(NULL, L"Alarm2 Type2",	  PropS(17), Hc900AMBiasAlarmTypeEnum);
	Tags.EmitRealW(NULL, L"Alarm2 Setpoint2", PropS(13), m_nDecimals, -99999, 99999);
	Tags.EmitRealW(NULL, L"Alarm Hysteresis",  PropS(18), 2, 0, 5);

	Tags.EndFolder();

	// Root

	Tags.EmitTextC(NULL, L"Description", m_Desc);
	Tags.EmitTextC(NULL, L"Units",	     m_Units);

	Tags.EmitFnumM(NULL, L"AM State",      PropD( 8), Hc900AutoManualEnum   );
	Tags.EmitFnumW(NULL, L"AM Select",     PropD( 2), Hc900OffOnEnum        );
	Tags.EmitRealW(NULL, L"Manual Output", PropD( 3), m_nDecimals, -5, 105  );
	Tags.EmitRealM(NULL, L"Bias",	       PropD( 1), m_nDecimals, -5, 105  );
	Tags.EmitFnumM(NULL, L"Alarm1",        PropD( 9), Hc900NormalAlarmEnum  );
	Tags.EmitFnumM(NULL, L"Alarm2",        PropD(10), Hc900NormalAlarmEnum  );
	Tags.EmitRealM(NULL, L"PV",	       PropD( 4), m_nDecimals, -5, 105  );
	Tags.EmitRealX(NULL, L"Output",        PropD( 7), m_nDecimals, -5, 105, WriteOP);
	}

// Loop Tags

BOOL CDataFile::CLoopTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CLoopTagRecord);
	}

void CDataFile::CLoopTagTable::EmitTags(CMakeTags &Tags, CDataFile &DataFile)
{
	Tags.NewFolder(L"Loops");

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CLoopTagRecord &Rec = (CLoopTagRecord &) m_List[i];

		Rec.EmitTags(Tags, DataFile);
		}

	Tags.EndFolder();
	}

// System Configuration

BOOL CDataFile::CSysConfigTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		m_b50Hz 	= pParse->ReadByte();
		m_bLang 	= pParse->ReadByte();
		m_bSaveInhibit	= pParse->ReadByte();
		m_bMenu1Inhibit = pParse->ReadByte();
		m_bMenu2Inhibit = pParse->ReadByte();
		m_bMapType	= pParse->ReadByte();
		m_bAlphaTest	= pParse->ReadByte();

		pParse->Skip(9);
		}

	return TRUE;
	}

// Alarm Help Text

BOOL CDataFile::CAlarmHelpTextTable::Read(CParser *pParse, CTableHeader &Header)
{
	pParse->ReadData(m_Data, Header.m_nRecordCount);

	return TRUE;
	}

// Alarm Group Displays

BOOL CDataFile::CAlarmGroupDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 3 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			for( int a = 0; a < 12; a++ ) {

				m_Sigs.Append(pParse->ReadBigWord());
				}

			for( int b = 0; b < 12; b++ ) {

				m_Help.Append(pParse->ReadBigWord());
				}

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

void CDataFile::CAlarmGroupDisplayTable::EmitTags(CMakeTags &Tags, CSignalTable &Sigs, CAlarmHelpTextTable &Help)
{
	Tags.NewFolder(L"Alarms");

	if( TRUE ) {

		Tags.NewFolder(L"Groups");

		CUIntArray   Index;

		CStringArray Valid, Name;

		for( UINT g = 0; g < m_Titles.GetCount(); g++ ) {

			if( !m_Titles[g].IsEmpty() ) {

				Index.Append(g);

				Valid.Append(L"1");

				Name .Append(Const(m_Titles[g]));
				}
			}

		Tags.EmitSintC (NULL, L"Count",  Index.GetCount());
		
		Tags.EmitLookup(NULL, L"Index",  Index);
		
		Tags.EmitLookup(NULL, L"Valid",  Index, Valid, L"0");
		
		Tags.EmitLookup(NULL, L"Name",   Index, Name,  L"\"\"");

		Tags.EndFolder();
		}

	if( TRUE ) {

		Tags.NewFolder(L"Points");

		CUIntArray   Index;

		CStringArray Valid, Signal, Line1, Line2;
		
		for( UINT p = 0; p < m_Sigs.GetCount(); p++ ) {

			UINT uSig = m_Sigs[p];

			if( uSig ) {

				CSignalRecord &Sig = (CSignalRecord &) Sigs.m_List[uSig-1];

				Sig.m_fEvent = 1;

				Index .Append(p);

				Valid .Append(L"1");

				Signal.Append(CPrintf(L"%u", uSig));

				if( m_Help[p] ) {

					PCBYTE  pd = Help.m_Data.GetPointer() + m_Help[p];

					CString tt;

					for( UINT n = 0; n < *pd; n++ ) {

						tt += pd[1+n];
						}

					CString l1 = tt.Left(24);

					CString l2 = tt.Mid (24);

					l1.TrimBoth();

					l2.TrimBoth();

					Line1.Append(L"\"" + l1 + L"\"");

					Line2.Append(L"\"" + l2 + L"\"");
					}
				else {
					Line1.Append(L"\"\"");

					Line2.Append(L"\"\"");
					}
				}
			}

		Tags.EmitSintC (NULL, L"Count",  Index.GetCount());
		
		Tags.EmitLookup(NULL, L"Index",  Index);
		
		Tags.EmitLookup(NULL, L"Valid",  Index, Valid,  L"0");
		
		Tags.EmitLookup(NULL, L"Signal", Index, Signal, L"0");

		Tags.EmitLookup(NULL, L"Help1",  Index, Line1,  L"\"\"");

		Tags.EmitLookup(NULL, L"Help2",  Index, Line2,  L"\"\"");

		Tags.EndFolder();
		}

	// TODO -- Use enum for state?

	Tags.EmitSintA( NULL,
			L"State",
			SpecAlarmInfo(0),
			360,
			FALSE
			);

	Tags.EmitEnumA( NULL,
			L"AckType",
			SpecAlarmInfo(1*360),
			L"Auto",
			L"Manual",
			360,
			FALSE
			);

	Tags.EmitTimeA( NULL,
			L"TimeOn",
			SpecAlarmInfo(2*360),
			2,
			360
			);

	Tags.EmitTimeA( NULL,
			L"TimeOff",
			SpecAlarmInfo(3*360),
			2,
			360
			);

	Tags.EmitSintA( NULL,
			L"Occurrences",
			SpecAlarmInfo(4*360),
			360,
			FALSE
			);

	Tags.EndFolder();
	}

// Generic Tag

BOOL CDataFile::CGenericTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		m_Tag	     = pParse->ReadString(16);
		m_Desc	     = pParse->ReadString(16);
		m_nBlockNum  = pParse->ReadBigWord();
		m_nBlockType = pParse->ReadByte();

		pParse->ReadByte();

		return TRUE;
		}

	pParse->Skip(16 + 16 + 4);

	return FALSE;
	}

void CDataFile::CGenericTagRecord::CreatePushbuttonBlock(CMakeTags &Tags, int nPBB1to4, int nPBBIndex, CDataFile &DataFile)
{
	// The pushbutton blocks (PBB) found in the .cde file are numbered
	// 0, 1, 2, 3, ... N-1 for N pushbutton blocks stored in the pushbutton
	// display table. This is the usage of nPBBIndex. Each PBB found in
	// the .cde file has four tag folders created for it, with "F1" to "F4"
	// appended to the PBB tag name.  This is the usage of nPBB1to4. We
	// access m_PushbuttonDisplayTable, indexing into this table using
	// nPBBIndex. We then use nPBB1to4 to access the four array members
	// m_nSig[] and m_Desc[] in CPushbuttonDisplayRecord.

	CPushButtonDisplayRecord const &Rec = DataFile.m_PushbuttonDisplayTable.m_List[nPBBIndex];

	int nPBBNumber = nPBB1to4 - 1; // make zero-based PBB number 0-3

	int nSignal    = Rec.m_nSig[nPBBNumber] - 1;

	CPrintf Name(L"%sF%d", m_Tag, nPBB1to4);

	if( nSignal >= 0 ) {

		CSignalRecord const &Sig = DataFile.m_SignalTable.m_List[nSignal];

		if( Sig.m_nTagType == 1 ) {

			// Analog Signal

			Tags.NewFolder( L"Pushbutton.Analog",
					Name
					);

			Tags.EmitTextC( NULL,
					L"Description",
					Rec.m_Title
					);

			Tags.EmitTextC( NULL,
					L"Button Description",
					Rec.m_Desc[nPBBNumber]
					);

			Tags.EmitFnumW( NULL,
					L"Out",
					PropD(nPBB1to4),
					Hc900NormalPressedEnum
					);

			Tags.EmitTextC( NULL,
					L"Feedback Tag",
					Sig.m_Tag
					);

			Tags.EmitRealM( NULL,
					L"Feedback Value",
					AddrBP(Sig.m_nBlockNum, DYNAMIC, Sig.m_nOutIndex),
					Sig.m_nDecPlaces
					);

			Tags.EmitTextC( NULL,
					L"Feedback Units",
					Sig.m_Label0
					);
			}
		else {
			// Digital Signal

			Tags.NewFolder( L"Pushbutton.Digital",
					Name
					);

			Tags.EmitTextC( NULL,
					L"Description",
					Rec.m_Title
					);

			Tags.EmitTextC( NULL,
					L"Button Description",
					Rec.m_Desc[nPBBNumber]
					);

			Tags.EmitFnumW( NULL,
					L"Out",
					PropD(nPBB1to4),
					Hc900NormalPressedEnum
					);

			Tags.EmitTextC( NULL,
					L"Feedback Tag",
					Sig.m_Tag
					);

			Tags.EmitEnumM( NULL,
					L"Feedback Value",
					AddrBP(Sig.m_nBlockNum, DYNAMIC, Sig.m_nOutIndex),
					Sig.m_Label0,
					Sig.m_Label1
					);

			Tags.EmitTextC( NULL,
					L"Feedback Units",
					L""
					);
			}
		}
	else {
		// No Signal

		Tags.NewFolder( L"Pushbutton.Analog",
				Name
				);

		Tags.EmitTextC( NULL,
				L"Description",
				Rec.m_Title
				);

		Tags.EmitTextC( NULL,
				L"Button Description",
				Rec.m_Desc[nPBBNumber]
				);

		Tags.EmitFnumW( NULL,
				L"Out",
				PropD(nPBB1to4),
				Hc900NormalPressedEnum
				);

		Tags.EmitTextC( NULL,
				L"Feedback Tag",
				L""
				);

		Tags.EmitRealC( NULL,
				L"Feedback Value",
				0,
				1
				);

		Tags.EmitTextC( NULL,
				L"Feedback Units",
				L""
				);
		}

	Tags.EndFolder();
	}

void CDataFile::CGenericTagRecord::CreateFourSelectorSwitchBlock(CMakeTags &Tags, int nFSS1to4, int nFSSIndex, CDataFile &DataFile)
{
	// The four selector switch blocks (FSS) found in the .cde file are numbered
	// 0, 1, 2, 3, ... N-1 for N four selector switch blocks stored in the four
	// selector switch display table. This is the usage of nFSSIndex. Each FSS
	// found in the .cde file has four tag folders created for it, with "BankA"
	// to "BankD" appended to the FSS tag name.  This is the usage of nFSS1to4.
	// We access m_FssDisplayTable, indexing into this table using nFSSIndex. We
	// then use nFSS1to4 to access the four array members m_Desc[4] and m_BankA[4]
	// through m_BankD[4] in CFssDisplayRecord.

	CPrintf Name(L"%sBank%c", m_Tag, '@' + nFSS1to4);

	Tags.NewFolder(L"FourSelectorSwitch", Name);

	int nFSSNumber = nFSS1to4 - 1; // make zero-based FSS number 0-3

	CFssDisplayRecord const &FSSDisplayRecord = DataFile.m_FssDisplayTable.m_List[nFSSIndex];

	Tags.EmitTextC(NULL, L"Description",	  FSSDisplayRecord.m_Title);

	Tags.EmitTextC(NULL, L"Bank Description", FSSDisplayRecord.m_Desc[nFSSNumber]);

	switch( nFSS1to4 ) {

		case 1: // BankA
			Tags.EmitTextC(NULL, L"State 1 Label", FSSDisplayRecord.m_BankA[0]);
			Tags.EmitTextC(NULL, L"State 2 Label", FSSDisplayRecord.m_BankA[1]);
			Tags.EmitTextC(NULL, L"State 3 Label", FSSDisplayRecord.m_BankA[2]);
			Tags.EmitTextC(NULL, L"State 4 Label", FSSDisplayRecord.m_BankA[3]);
			break;

		case 2: // BankB
			Tags.EmitTextC(NULL, L"State 1 Label", FSSDisplayRecord.m_BankB[0]);
			Tags.EmitTextC(NULL, L"State 2 Label", FSSDisplayRecord.m_BankB[1]);
			Tags.EmitTextC(NULL, L"State 3 Label", FSSDisplayRecord.m_BankB[2]);
			Tags.EmitTextC(NULL, L"State 4 Label", FSSDisplayRecord.m_BankB[3]);
			break;

		case 3: // BankC
			Tags.EmitTextC(NULL, L"State 1 Label", FSSDisplayRecord.m_BankC[0]);
			Tags.EmitTextC(NULL, L"State 2 Label", FSSDisplayRecord.m_BankC[1]);
			Tags.EmitTextC(NULL, L"State 3 Label", FSSDisplayRecord.m_BankC[2]);
			Tags.EmitTextC(NULL, L"State 4 Label", FSSDisplayRecord.m_BankC[3]);
			break;

		case 4: // BankD
			Tags.EmitTextC(NULL, L"State 1 Label", FSSDisplayRecord.m_BankD[0]);
			Tags.EmitTextC(NULL, L"State 2 Label", FSSDisplayRecord.m_BankD[1]);
			Tags.EmitTextC(NULL, L"State 3 Label", FSSDisplayRecord.m_BankD[2]);
			Tags.EmitTextC(NULL, L"State 4 Label", FSSDisplayRecord.m_BankD[3]);
			break;
		}

	Tags.EmitEnumM(NULL, L"State",     PropD(nFSS1to4 + 0), Hc900FSSStateEnum);

	Tags.EmitEnumW(NULL, L"State Req", PropD(nFSS1to4 + 4), Hc900FSSStateEnum);

	Tags.EndFolder();
	}

void CDataFile::CGenericTagRecord::EmitTags(CMakeTags &Tags, int nBlockType, CDataFile &DataFile)
{
	static int nPBBIndex = 0; // pushbutton block index (0 to N-1 for N PBBs found)

	static int nFSSIndex = 0; // four selector switch block index (0 to N-1 for N FSSs found)

	if( nBlockType == m_nBlockType ) {

		switch( nBlockType ) {

			case 42: // Pushbutton
				CreatePushbuttonBlock(Tags, 1, nPBBIndex, DataFile);
				CreatePushbuttonBlock(Tags, 2, nPBBIndex, DataFile);
				CreatePushbuttonBlock(Tags, 3, nPBBIndex, DataFile);
				CreatePushbuttonBlock(Tags, 4, nPBBIndex, DataFile);
				nPBBIndex++;
				break;

			case 76:// Four Selector Switch
				CreateFourSelectorSwitchBlock(Tags, 1, nFSSIndex, DataFile);
				CreateFourSelectorSwitchBlock(Tags, 2, nFSSIndex, DataFile);
				CreateFourSelectorSwitchBlock(Tags, 3, nFSSIndex, DataFile);
				CreateFourSelectorSwitchBlock(Tags, 4, nFSSIndex, DataFile);
				nFSSIndex++;
				break;

			case 118: // Aga (AGA8 Detail)
				Tags.NewFolder(L"Aga.Detail", m_Tag);
				Tags.EmitTextC(NULL, L"Description", m_Desc);
				Tags.EmitRealM(NULL, L"TF", PropD(46), 3);
				Tags.EmitRealM(NULL, L"PF", PropD(47), 3);
				Tags.EmitRealM(NULL, L"HW", PropD(48), 3);
				Tags.EmitRealW(NULL, L"Methane Value", PropD(49), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Nitrogen Value", PropD(50), 6, 0, 1);
				Tags.EmitRealW(NULL, L"CO2 Value", PropD(51), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Ethane Value", PropD(52), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Propane Value", PropD(53), 6, 0, 1);
				Tags.EmitRealW(NULL, L"H2O Value", PropD(54), 6, 0, 1);
				Tags.EmitRealW(NULL, L"H2S Value", PropD(55), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Hydrogen Value", PropD(56), 6, 0, 1);
				Tags.EmitRealW(NULL, L"CO Value", PropD(57), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Oxygen Value", PropD(58), 6, 0, 1);
				Tags.EmitRealW(NULL, L"i_Butane Value", PropD(59), 6, 0, 1);
				Tags.EmitRealW(NULL, L"n_Butane Value", PropD(60), 6, 0, 1);
				Tags.EmitRealW(NULL, L"i_Pentane Value", PropD(61), 6, 0, 1);
				Tags.EmitRealW(NULL, L"n_Pentane Value", PropD(62), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Hexane Value", PropD(63), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Heptane Value", PropD(64), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Octane Value", PropD(65), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Nonane Value", PropD(66), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Decane Value", PropD(67), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Helium Value", PropD(68), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Argon Value", PropD(69), 6, 0, 1);
				Tags.EmitRealM(NULL, L"RHOTP", PropD(36), 3);
				Tags.EmitRealM(NULL, L"RHOB", PropD(37), 3);
				Tags.EmitRealM(NULL, L"RHOS", PropD(38), 3);
				Tags.EmitRealM(NULL, L"FPVS", PropD(39), 3);
				Tags.EmitRealM(NULL, L"GRS", PropD(40), 3);
				Tags.EmitRealM(NULL, L"HV", PropD(41), 3);
				Tags.EmitEnumM(NULL, L"Error", PropD(42), Hc900OffOnEnum);
				Tags.EmitEnumM(NULL, L"Warning", PropD(43), Hc900OffOnEnum);
				Tags.EmitRealM(NULL, L"Status", PropD(44), 1);
				Tags.EmitFnumW(NULL, L"Entered Gas Values", PropD(1), Hc900InactiveActiveEnum);
				Tags.EmitRealW(NULL, L"Methane Value MB", PropD(2), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Nitrogen Value MB", PropD(3), 6, 0, 1);
				Tags.EmitRealW(NULL, L"CO2 Value MB", PropD(4), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Ethane Value MB", PropD(5), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Propane Value MB", PropD(6), 6, 0, 1);
				Tags.EmitRealW(NULL, L"H2O Value MB", PropD(7), 6, 0, 1);
				Tags.EmitRealW(NULL, L"H2S Value MB", PropD(8), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Hydrogen Value MB", PropD(9), 6, 0, 1);
				Tags.EmitRealW(NULL, L"CO Value MB", PropD(10), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Oxygen Value MB", PropD(11), 6, 0, 1);
				Tags.EmitRealW(NULL, L"i_Butane Value MB", PropD(12), 6, 0, 1);
				Tags.EmitRealW(NULL, L"n_Butane Value MB", PropD(13), 6, 0, 1);
				Tags.EmitRealW(NULL, L"i_Pentane Value MB", PropD(14), 6, 0, 1);
				Tags.EmitRealW(NULL, L"n_Pentane Value MB", PropD(15), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Hexane Value MB", PropD(16), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Heptane Value MB", PropD(17), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Octane Value MB", PropD(18), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Nonane Value MB", PropD(19), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Decane Value MB", PropD(20), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Helium Value MB", PropD(21), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Argon Value MB", PropD(22), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Base Temperature", PropS(2), 5, -99999, 99999);
				Tags.EmitRealW(NULL, L"Base Pressure", PropS(3), 5, -99999, 99999);
				Tags.EmitFnumW(NULL, L"Selected Gas Comp Value", PropS(4), Hc900LocalValueRemoteValueEnum);
				Tags.EmitFnumW(NULL, L"Override Range Error", PropS(5), Hc900NooverrideOverrideEnum);
				Tags.EmitRealM(NULL, L"Methane Local", PropS(6), 6);
				Tags.EmitRealM(NULL, L"Nitrogen Local", PropS(7), 6);
				Tags.EmitRealM(NULL, L"CO2 Local", PropS(8), 6);
				Tags.EmitRealM(NULL, L"Ethane Local", PropS(9), 6);
				Tags.EmitRealM(NULL, L"Propane Local", PropS(10), 6);
				Tags.EmitRealM(NULL, L"H2O Local", PropS(11), 6);
				Tags.EmitRealM(NULL, L"H2S Local", PropS(12), 6);
				Tags.EmitRealM(NULL, L"Hydrogen Local", PropS(13), 6);
				Tags.EmitRealM(NULL, L"CO Local", PropS(14), 6);
				Tags.EmitRealM(NULL, L"Oxygen Local", PropS(15), 6);
				Tags.EmitRealM(NULL, L"i_Butane Local", PropS(16), 6);
				Tags.EmitRealM(NULL, L"n_Butane Local", PropS(17), 6);
				Tags.EmitRealM(NULL, L"i_Pentane Local", PropS(18), 6);
				Tags.EmitRealM(NULL, L"n_Pentane Local", PropS(19), 6);
				Tags.EmitRealM(NULL, L"Hexane Local", PropS(20), 6);
				Tags.EmitRealM(NULL, L"Heptane Local", PropS(21), 6);
				Tags.EmitRealM(NULL, L"Octane Local", PropS(22), 6);
				Tags.EmitRealM(NULL, L"Nonane Local", PropS(23), 6);
				Tags.EmitRealM(NULL, L"Decane Local", PropS(24), 6);
				Tags.EmitRealM(NULL, L"Helium Local", PropS(25), 6);
				Tags.EmitRealM(NULL, L"Argon Local", PropS(26), 6);
				Tags.EndFolder();
				break;

			case 119: // Aga (AGA8 Gross)
				Tags.NewFolder(L"Aga.Gross", m_Tag);
				Tags.EmitTextC(NULL, L"Description", m_Desc);
				Tags.EmitRealM(NULL, L"TF", PropD(29), 3);
				Tags.EmitRealM(NULL, L"PF", PropD(30), 3);
				Tags.EmitRealM(NULL, L"HW", PropD(31), 3);
				Tags.EmitRealM(NULL, L"CO2 Value", PropD(32), 6, 0, 1);
				Tags.EmitRealM(NULL, L"Hydrogen Value", PropD(33), 6, 0, 1);
				Tags.EmitRealM(NULL, L"CO Value", PropD(34), 6, 0, 1);
				Tags.EmitRealM(NULL, L"Nitrogen Value", PropD(35), 6, 0, 1);
				Tags.EmitRealM(NULL, L"RHOTP", PropD(19), 3);
				Tags.EmitRealM(NULL, L"RHOB", PropD(20), 3);
				Tags.EmitRealM(NULL, L"RHOS", PropD(21), 3);
				Tags.EmitRealM(NULL, L"FPVS", PropD(22), 3);
				Tags.EmitRealM(NULL, L"GRS", PropD(23), 3);
				Tags.EmitRealM(NULL, L"HV", PropD(24), 3);
				Tags.EmitEnumM(NULL, L"Error", PropD(25), Hc900OffOnEnum);
				Tags.EmitEnumM(NULL, L"Warning", PropD(26), Hc900OffOnEnum);
				Tags.EmitRealM(NULL, L"Status", PropD(27), 1);
				Tags.EmitFnumW(NULL, L"Entered Gas Values", PropD(1), Hc900InactiveActiveEnum);
				Tags.EmitRealW(NULL, L"CO2 Value MB", PropD(2), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Hydrogen Value MB", PropD(3), 6, 0, 1);
				Tags.EmitRealW(NULL, L"CO Value MB", PropD(4), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Nitrogen Value MB", PropD(5), 6, 0, 1);
				Tags.EmitRealW(NULL, L"Base Temperature", PropS(2), 5, -99999, 99999);
				Tags.EmitRealW(NULL, L"Base Pressure", PropS(3), 5, -99999, 99999);
				Tags.EmitFnumW(NULL, L"Selected Gas Comp Value", PropS(4), Hc900LocalValueRemoteValueEnum);
				Tags.EmitFnumW(NULL, L"Override Range Error", PropS(5), Hc900NooverrideOverrideEnum);
				Tags.EmitEnumW(NULL, L"Gross Method Used", PropS(6), Hc900Method1Method2Enum);
				Tags.EmitRealW(NULL, L"Relative Density", PropS(7), 1);
				Tags.EmitRealW(NULL, L"Rel Density Ref Temp", PropS(8), 1);
				Tags.EmitRealW(NULL, L"Rel Density Ref Pressure", PropS(9), 1);
				Tags.EmitRealW(NULL, L"Method1 Heating Value", PropS(10), 1);
				Tags.EmitRealW(NULL, L"Method1 Cal Ref Temp", PropS(11), 1);
				Tags.EmitRealW(NULL, L"Method1 Cal Ref Pressure", PropS(12), 1);
				Tags.EmitRealW(NULL, L"Method1 Comb Ref Temp", PropS(13), 1);
				Tags.EmitRealM(NULL, L"CO2 Local", PropS(14), 6);
				Tags.EmitRealM(NULL, L"Hydrogen Local", PropS(15), 6);
				Tags.EmitRealM(NULL, L"CO Local", PropS(16), 6);
				Tags.EmitRealM(NULL, L"Method2 Nitrogen Local", PropS(17), 6);
				Tags.EndFolder();
				break;

			case 129: // Wireless (XYR5000 Base)
				Tags.NewFolder(L"XYR5000B", m_Tag);
				Tags.EmitFnumM(NULL, L"Device Status", PropD(3), Hc900OfflineOnlineEnum);
				Tags.EmitRealM(NULL, L"OnLineExpTxCount", PropD(4), 0, 1, 100);
				Tags.EmitRealM(NULL, L"OnLineTxCount", PropD(5), 0, 1, 100);
				Tags.EndFolder();
				break;

			case 130: // Wireless (XYR5000 Transmitter)
				Tags.NewFolder(L"XYR5000T", m_Tag);
				Tags.EmitRealM(NULL, L"Primary",   PropD(3), 0);
				Tags.EmitRealM(NULL, L"Secondary", PropD(4), 0);
				Tags.EmitRealM(NULL, L"Tertiary",  PropD(5), 0);
				Tags.EmitFnumM(NULL, L"Tx Status", PropD(6), Hc900OfflineOnlineEnum);
				Tags.EmitFnumM(NULL, L"Low Battery", PropD(7), Hc900GoodLowEnum);
				Tags.EmitFnumM(NULL, L"Alarm", PropD(8), Hc900OffOnEnum);
				Tags.EmitFnumM(NULL, L"Sensor Error", PropD(9), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Sensor Overrange", PropD(10), Hc900OkOverrangeEnum);
				Tags.EmitFnumM(NULL, L"System Error", PropD(11), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"SWINP1 Closed", PropD(12), Hc900OffOnEnum);
				Tags.EmitFnumM(NULL, L"SWINP2 Closed", PropD(13), Hc900OffOnEnum);
				Tags.EmitFnumM(NULL, L"Sqrt Fn", PropD(14), Hc900OffOnEnum);
				Tags.EmitTextC(NULL, L"Primary Units", L"EU");
				Tags.EmitRealC(NULL, L"Primary Hi Limit", 100, 1);
				Tags.EmitRealC(NULL, L"Primary Lo Limit", 0, 1);
				Tags.EmitTextC(NULL, L"Secondary Units", L"EU");
				Tags.EmitRealC(NULL, L"Secondary Hi Limit", 100, 1);
				Tags.EmitRealC(NULL, L"Secondary Lo Limit", 0, 1);
				Tags.EmitTextC(NULL, L"Tertiary Units", L"EU");
				Tags.EmitRealC(NULL, L"Tertiary Hi Limit", 100, 1);
				Tags.EmitRealC(NULL, L"Tertiary Lo Limit", 0, 1);
				Tags.EndFolder();
				break;

			case 134: // Wireless (XYR6000 Transmitter)
				Tags.NewFolder(L"XYR6000T", m_Tag);
				Tags.EmitRealM(NULL, L"PV1", PropD(1), 1);
				Tags.EmitRealM(NULL, L"PV2", PropD(2), 1);
				Tags.EmitRealM(NULL, L"PV3", PropD(3), 1);
				Tags.EmitRealM(NULL, L"PV4", PropD(4), 1);
				Tags.EmitFnumM(NULL, L"Elec Fail", PropD(5), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Mech Fail", PropD(6), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Input Fail", PropD(7), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Output Fail", PropD(8), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Low Battery", PropD(9), Hc900GoodLowEnum);
				Tags.EmitFnumM(NULL, L"Ext Power", PropD(10), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Config Error", PropD(11), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Calibration Error", PropD(12), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Radio Error", PropD(13), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Mem Error", PropD(14), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Dfw Error", PropD(15), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Wt Error", PropD(16), Hc900GoodErrorEnum);
				Tags.EmitFnumM(NULL, L"Diag", PropD(17), Hc900OffOnEnum);
				Tags.EmitRealM(NULL, L"Dev Status", PropD(18), 1);
				Tags.EmitTextC(NULL, L"PV1 Units", L"EU");
				Tags.EmitRealC(NULL, L"PV1 Hi Limit", 100, 1);
				Tags.EmitRealC(NULL, L"PV1 Lo Limit", 0, 1);
				Tags.EmitTextC(NULL, L"PV2 Units", L"EU");
				Tags.EmitRealC(NULL, L"PV2 Hi Limit", 100, 1);
				Tags.EmitRealC(NULL, L"PV2 Lo Limit", 0, 1);
				Tags.EmitTextC(NULL, L"PV3 Units", L"EU");
				Tags.EmitRealC(NULL, L"PV3 Hi Limit", 100, 1);
				Tags.EmitRealC(NULL, L"PV3 Lo Limit", 0, 1);
				Tags.EmitTextC(NULL, L"PV4 Units", L"EU");
				Tags.EmitRealC(NULL, L"PV4 Hi Limit", 100, 1);
				Tags.EmitRealC(NULL, L"PV4 Lo Limit", 0, 1);
				Tags.EndFolder();
				break;

			default:
				break;
			}
		}
	}

// Generic Tags

BOOL CDataFile::CGenericTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CGenericTagRecord);
	}

void CDataFile::CGenericTagTable::EmitTags(CMakeTags &Tags, int nBlockType, BOOL fEndFolder, CDataFile &DataFile)
{
	// Parameter fEndFolder tells us whether we need to call EndFolder(). For
	// multiple block types in a folder (e.g., Aga or Wireless), the last call
	// to CGenericTagTable::EmitTags sets this parameter TRUE. All other calls
	// set it FALSE so that we don't end the folder.

	static BOOL fAgaFolderEmitted	   = FALSE; // emit only one folder for types 118 & 119

	static BOOL fWirelessFolderEmitted = FALSE; // emit only one folder for types 129, 130, & 134

	switch( nBlockType ) {

		case 42: // Pushbutton

			Tags.NewFolder(L"Pushbutton");

			break;

		case 76: // Four Selector Switch

			Tags.NewFolder(L"FourSelectorSwitch");

			break;

		case 118: // Aga (AGA8 Detail)
		case 119: // Aga (AGA8 Gross)

			if( !fAgaFolderEmitted ) {

				fAgaFolderEmitted = TRUE;

				Tags.NewFolder(L"Aga");
				}
			break;

		case 129: // Wireless (XYR5000 Base)
		case 130: // Wireless (XYR5000 Transmitter)
		case 134: // Wireless (XYR6000 Transmitter)

			if( !fWirelessFolderEmitted ) {

				fWirelessFolderEmitted = TRUE;

				Tags.NewFolder(L"Wireless");
				}
			break;

		default:

			Tags.NewFolder(L"Unknown");

			break;
		}

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CGenericTagRecord &Rec = (CGenericTagRecord &) m_List[i];

		Rec.EmitTags(Tags, nBlockType, DataFile);
		}

	if( fEndFolder ) {

		Tags.EndFolder();
		}
	}

// SPP Tag

BOOL CDataFile::CSppTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag	    = pParse->ReadString(8);
		m_Desc	    = pParse->ReadString(16);
		m_Units     = pParse->ReadString(4);
		m_nBlockNum = pParse->ReadBigWord();
		m_nDecimals = pParse->ReadByte();
		m_nAuxDec   = pParse->ReadByte();

		return TRUE;
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag	    = pParse->ReadString(16);
		m_Desc	    = pParse->ReadString(16);
		m_Units     = pParse->ReadString(6);
		m_nBlockNum = pParse->ReadBigWord();
		m_nDecimals = pParse->ReadByte();
		m_nAuxDec   = pParse->ReadByte();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CSppTagRecord::EmitTags(CMakeTags &Tags, CDataFile &File)
{
	CString Folder = Tags.NewFolder(L"SetpointProgrammer", m_Tag);

	UINT o = File.m_pDeviceCaps->HasFeature(FEATURE_SPP_TAG_TABLE) ? 2 : 0;

	Tags.EmitTextC(NULL, L"Description", m_Desc);
	Tags.EmitTextC(NULL, L"Units",	     m_Units);

	Tags.EmitTextW(NULL, L"Profile Name",	PropD(13 + 0),  8);
	Tags.EmitTextW(NULL, L"Profile Desc",	PropD(15 + 0), 16);
	Tags.EmitRealM(NULL, L"Profile Number", PropD(42 + o),  0);

	Tags.EmitRealW(NULL, L"Load Recipe", PropD(3), 0, 0, 99999);
	Tags.EmitRealW(NULL, L"Save Recipe", PropD(1), 0, 1, 99999);

	Tags.EmitFnumM(NULL, L"State",	      PropD(40 + o), Hc900SetpointProgrammerStateEnum);
	Tags.EmitTrend(NULL, L"PV1",	      PropD(53 + o), m_nDecimals);
	Tags.EmitRealM(NULL, L"PV2",	      PropD(54 + o), m_nDecimals);
	Tags.EmitRealM(NULL, L"PV3",	      PropD(55 + o), m_nDecimals);
	Tags.EmitTrend(NULL, L"PV4",	      PropD(56 + o), m_nAuxDec);
	Tags.EmitTrend(NULL, L"Setpoint",     PropD(43 + o), m_nDecimals);
	Tags.EmitTrend(NULL, L"Aux Setpoint", PropD(44 + o), m_nAuxDec);

	Tags.EmitRealM(NULL, L"Segment Number",    PropD(41 + o), 0);
	Tags.EmitEnumM(NULL, L"Segment Type",	   PropD( 5 + 0), Hc900SetpointProgrammerSegTypeEnum);
	Tags.EmitSintM(NULL, L"Segment Indicator", PropD(39 + o));

	Tags.EmitTimeM(NULL, L"Segment Elapsed Time",	PropD(46 + o), 1);
	Tags.EmitTimeM(NULL, L"Segment Time Remaining", PropD(45 + o), 1);
	Tags.EmitTimeM(NULL, L"Program Elapsed Time",	PropD(47 + o), 1);

	Tags.EmitFnumW(L"NewState", L"New State", PropD(2), Hc900SetpointProgrammerNewStateEnum);

	Tags.EmitRealW(NULL, L"Segment Change",     PropD( 4), 0);
	Tags.EmitFnumW(NULL, L"Advance",	    PropD( 9), Hc900SetpointProgrammerAdvanceEnum);
	Tags.EmitEnumW(NULL, L"Time Units",	    PropD(36), Hc900SetpointSchedulerTimeUnitsEnum);
	Tags.EmitRealW(NULL, L"Loop Start Segment", PropD(27), 0, 0,  50);
	Tags.EmitRealW(NULL, L"Loop End Segment",   PropD(28), 0, 0,  50);
	Tags.EmitRealW(NULL, L"Jog Segment",	    PropD(26), 0, 0,  50);
	Tags.EmitSintW(NULL, L"Loop Cycles",	    PropD(29), 0, 100);
	Tags.EmitTextW(NULL, L"Primary Out Label",  PropD(19), 8);
	Tags.EmitTextW(NULL, L"Primary Out EU",     PropD(21), 4);
	Tags.EmitTextW(NULL, L"Aux Out Label",	    PropD(30), 8);
	Tags.EmitTextW(NULL, L"Aux Out EU",	    PropD(32), 4);
	Tags.EmitRealW(NULL, L"Restart Ramp Rate",  PropD(25), m_nDecimals);
	Tags.EmitRealW(NULL, L"Guar Soak Low",	    PropD(23), m_nDecimals);
	Tags.EmitRealW(NULL, L"Guar Soak High",     PropD(24), m_nDecimals);
	Tags.EmitEnumW(NULL, L"Ramp Type",	    PropD(37), Hc900SetpointProgrammerTimeRateEnum);
	Tags.EmitEnumW(NULL, L"Fast Forward",	    PropD(10), Hc900OffOnEnum);
	Tags.EmitEnumW(NULL, L"Guar Soak Type",     PropD(38), Hc900SetpointProgrammerGuarSoakTypeEnum);
	Tags.EmitEnumW(NULL, L"Set point Type",     PropD(39), Hc900SetpointProgrammerTypeEnum);

	////////

	// Prop     Parameter	    Controller Encoding
	//
	// 0	    RampSegment     enumInt:Hc900SetpointProgrammerSegTypeEnum
	// 50	    GuarSoakEnable  enumInt:Hc900OffOnEnum
	//
	// 100	    Event1	    enumInt:Hc900OffOnEnum
	// 150	    Event2	    enumInt:Hc900OffOnEnum
	// :	      : 	       :
	// 850	    Event16	    enumInt:Hc900OffOnEnum
	//
	// 900	    TimeOrRate	    numFloat (decimal places 2)
	// 950	    StartValue	    numFloat (decimal places 2)
	// 1000     AuxValue	    numFloat (decimal places 2)

	Tags.NewFolder(L"Segments");

	Tags.EmitEnumA( NULL,
			L"Ramp Segment",
			AddrPS(m_nBlockNum, 0),
			Hc900SetpointProgrammerSegTypeEnum,
			50,
			TRUE
			);

	Tags.EmitEnumA( NULL,
			L"Guar Soak Enable",
			AddrPS(m_nBlockNum, 50),
			Hc900OffOnEnum,
			50,
			TRUE
			);

	for( int i = 0; i < 16; i++ ) {

		UINT Prop = 100 + i * 50;

		Tags.EmitEnumA( NULL,
				CPrintf(L"Event %d", 1+i),
				AddrPS(m_nBlockNum, Prop),
				Hc900OffOnEnum,
				50,
				TRUE
				);
		}

	Tags.EmitRealA( NULL,
			L"Time Or Rate",
			AddrPS(m_nBlockNum, 900),
			2,
			0,
			999.99,
			50,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"Start Value",
			AddrPS(m_nBlockNum, 950),
			m_nDecimals,
			50,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"Aux Value",
			AddrPS(m_nBlockNum, 1000),
			m_nAuxDec,
			50,
			TRUE
			);

	Tags.EndFolder();

	Tags.NewFolder(L"PrePlot");

	Tags.EmitSintA( NULL,
			L"XPos",
			L"",
			100,
			FALSE
			);

	Tags.EmitRealA( NULL,
			L"YPos",
			L"",
			m_nDecimals,
			100,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"APos",
			L"",
			100,
			FALSE
			);

	Tags.EmitRealA( NULL,
			L"BPos",
			L"",
			m_nAuxDec,
			100,
			TRUE
			);

	Tags.EmitSintW(NULL, L"CountX", L"");

	Tags.EmitSintW(NULL, L"CountA", L"");

	Tags.EmitSintW(NULL, L"Limit",  L"");

	CString Code;

	Code += L"// Find Time\r\n\r\n";

	Code += L"return HonFindTime(\r\n";

	Code += L"\t" + Folder + L".TimeUnits,\r\n";

	Code += L"\t" + Folder + L".RampType,\r\n";

	Code += L"\t" + Folder + L".Segments.RampSegment[0],\r\n";

	Code += L"\t" + Folder + L".Segments.TimeOrRate [0],\r\n";

	Code += L"\t" + Folder + L".Segments.StartValue [0],\r\n";

	Code += L"\t" + Folder + L".SegmentNumber,\r\n";

	Code += L"\t" + Folder + L".SegmentTimeRemaining\r\n";

	Code += L"\t);\r\n";

	Tags.EmitSintM(NULL, L"Time", Code);

	Tags.EndFolder();

	Tags.EndFolder();
	}

// SPP Tags

BOOL CDataFile::CSppTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CSppTagRecord);
	}

void CDataFile::CSppTagTable::EmitTags(CMakeTags &Tags, CDataFile &File)
{
	StdLookWithFile(CSppTagRecord, L"SetpointProgrammer", m_Tag, File);
	}

void CDataFile::CSppTagTable::EmitWork(CMakeTags &Tags)
{
	Tags.NewFolder(L"SPP");

	Tags.EmitSintW(NULL, L"ReqWrite", SpecSystem(14));
	
	Tags.EmitSintW(NULL, L"ReqRead",  SpecSystem(15));

	int m_nBlockNum = 32002;

	Tags.EmitTextW(NULL, L"Profile Name",	    PropD(13),  8);
	Tags.EmitTextW(NULL, L"Profile Desc",	    PropD(15), 16);
	Tags.EmitTextW(NULL, L"Primary Out Label",  PropD(19), 8);
	Tags.EmitTextW(NULL, L"Primary Out EU",     PropD(21), 4);
	Tags.EmitTextW(NULL, L"Aux Out Label",	    PropD(30), 8);
	Tags.EmitTextW(NULL, L"Aux Out EU",	    PropD(32), 4);
	Tags.EmitEnumW(NULL, L"Time Units",	    PropD(36), Hc900SetpointSchedulerTimeUnitsEnum);
	Tags.EmitEnumW(NULL, L"Ramp Type",	    PropD(37), Hc900SetpointProgrammerTimeRateEnum);
	Tags.EmitEnumW(NULL, L"Guar Soak Type",     PropD(38), Hc900SetpointProgrammerGuarSoakTypeEnum);
	Tags.EmitRealW(NULL, L"Guar Soak Low",	    PropD(23), 2);
	Tags.EmitRealW(NULL, L"Guar Soak High",     PropD(24), 2);
	Tags.EmitRealW(NULL, L"Loop Start Segment", PropD(27), 0, 0,  50);
	Tags.EmitRealW(NULL, L"Loop End Segment",   PropD(28), 0, 0,  50);
	Tags.EmitRealW(NULL, L"Jog Segment",	    PropD(26), 0, 0,  50);
	Tags.EmitSintW(NULL, L"Loop Cycles",	    PropD(29), 0, 100);
	Tags.EmitRealW(NULL, L"Restart Ramp Rate",  PropD(25), 2);
	Tags.EmitEnumW(NULL, L"Set point Type",     PropD(39), Hc900SetpointProgrammerTypeEnum);
	
	Tags.NewFolder(L"Segments");

	Tags.EmitEnumA( NULL,
			L"Ramp Segment",
			AddrPS(m_nBlockNum, 0),
			Hc900SetpointProgrammerSegTypeEnum,
			50,
			TRUE
			);

	Tags.EmitEnumA( NULL,
			L"Guar Soak Enable",
			AddrPS(m_nBlockNum, 50),
			Hc900OffOnEnum,
			50,
			TRUE
			);

	for( int i = 0; i < 16; i++ ) {

		UINT Prop = 100 + i * 50;

		Tags.EmitEnumA( NULL,
				CPrintf(L"Event %d", 1+i),
				AddrPS(m_nBlockNum, Prop),
				Hc900OffOnEnum,
				50,
				TRUE
				);
		}

	Tags.EmitRealA( NULL,
			L"Time Or Rate",
			AddrPS(m_nBlockNum, 900),
			2,
			0,
			999.99,
			50,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"Start Value",
			AddrPS(m_nBlockNum, 950),
			2,
			50,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"Aux Value",
			AddrPS(m_nBlockNum, 1000),
			2,
			50,
			TRUE
			);

	Tags.EndFolder();

	Tags.EndFolder();
	}

// Recipe Allocation

CDataFile::CRecipeAllocTable::CRecipeAllocTable(void)
{
	m_uCount[0] = 50;

	m_uCount[1] = 99;

	m_uCount[2] = 20;
	
	m_uCount[3] = 20;
	}

BOOL CDataFile::CRecipeAllocTable::Read(CParser *pParse, CTableHeader &Header)
{
	m_uCount[0] = pParse->ReadBigWord();

	m_uCount[1] = pParse->ReadBigWord();

	m_uCount[2] = pParse->ReadBigWord();

	m_uCount[3] = pParse->ReadBigWord();

	pParse->ReadBigWord();

	pParse->ReadBigWord();

	return TRUE;
	}

// Trend Displays

BOOL CDataFile::CTrendDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			pParse->Skip(2 + 2 + 12*2 + 12*4 + 12*4);

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

// Bar Displays

BOOL CDataFile::CBarDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			pParse->Skip(6*2 + 6*4 + 6*4);

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

// Panel Displays

BOOL CDataFile::CPanelDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			pParse->Skip(12*2);

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

// Multi-Panel Displays

BOOL CDataFile::CMultiPanelDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 3 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			pParse->Skip(7*2);

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

// Message Displays

BOOL CDataFile::CMessageDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			pParse->Skip(11*32);

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

// Panel Meter Displays

BOOL CDataFile::CPanelMeterDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			CString Title = pParse->ReadString(24);

			pParse->Skip(12*2);

			m_Titles.Append(Title);
			}
		}

	return TRUE;
	}

// Screen Button Displays

BOOL CDataFile::CScreenButtonDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			int j;

			for( j = 0; j < 10; j++ ) {

				m_bFormat [i][j] = pParse->ReadByte();
				}

			for( j = 0; j < 10; j++ ) {

				m_bChannel[i][j] = pParse->ReadByte();
				}

			for( j = 0; j < 10; j++ ) {

				pParse->ReadByte();
				}
			}
		}

	return TRUE;
	}

void CDataFile::CScreenButtonDisplayTable::EmitTags(CMakeTags &Tags)
{
	}

// Push Button Display

BOOL CDataFile::CPushButtonDisplayRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 3 ) {

		m_Title     = pParse->ReadString(24);

		m_nSig[0]   = pParse->ReadBigWord();
		m_nSig[1]   = pParse->ReadBigWord();
		m_nSig[2]   = pParse->ReadBigWord();
		m_nSig[3]   = pParse->ReadBigWord();

		m_Desc[0]   = pParse->ReadString(16);
		m_Desc[1]   = pParse->ReadString(16);
		m_Desc[2]   = pParse->ReadString(16);
		m_Desc[3]   = pParse->ReadString(16);

		m_nBlockNum = pParse->ReadBigWord();
		}

	return TRUE;
	}

void CDataFile::CPushButtonDisplayRecord::EmitTags(CMakeTags &Tags)
{
	}

// Push Button Displays

BOOL CDataFile::CPushButtonDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CPushButtonDisplayRecord);
	}

void CDataFile::CPushButtonDisplayTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CPushButtonDisplayRecord, L"PushButtonDisplays");
	}

// Four-State Display

BOOL CDataFile::CFssDisplayRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Title     = pParse->ReadString(24);

		m_Desc [0]  = pParse->ReadString(16);
		m_Desc [1]  = pParse->ReadString(16);
		m_Desc [2]  = pParse->ReadString(16);
		m_Desc [3]  = pParse->ReadString(16);

		m_BankA[0]  = pParse->ReadString(6);
		m_BankA[1]  = pParse->ReadString(6);
		m_BankA[2]  = pParse->ReadString(6);
		m_BankA[3]  = pParse->ReadString(6);

		m_BankB[0]  = pParse->ReadString(6);
		m_BankB[1]  = pParse->ReadString(6);
		m_BankB[2]  = pParse->ReadString(6);
		m_BankB[3]  = pParse->ReadString(6);

		m_BankC[0]  = pParse->ReadString(6);
		m_BankC[1]  = pParse->ReadString(6);
		m_BankC[2]  = pParse->ReadString(6);
		m_BankC[3]  = pParse->ReadString(6);

		m_BankD[0]  = pParse->ReadString(6);
		m_BankD[1]  = pParse->ReadString(6);
		m_BankD[2]  = pParse->ReadString(6);
		m_BankD[3]  = pParse->ReadString(6);

		m_nBlockNum = pParse->ReadBigWord();
		}

	return TRUE;
	}

void CDataFile::CFssDisplayRecord::EmitTags(CMakeTags &Tags)
{
	}

// Four-State Displays

BOOL CDataFile::CFssDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CFssDisplayRecord);
	}

void CDataFile::CFssDisplayTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CFssDisplayRecord, L"FSSDisplays");
	}

// Stage Tag

BOOL CDataFile::CStageTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag		  = pParse->ReadString(8);
		m_Desc		  = pParse->ReadString(16);
		m_Labels[0]	  = pParse->ReadString(8);
		m_Labels[1]	  = pParse->ReadString(8);
		m_Labels[2]	  = pParse->ReadString(8);
		m_Labels[3]	  = pParse->ReadString(8);
		m_PV1Units	  = pParse->ReadString(4);
		m_PV2Units	  = pParse->ReadString(4);
		m_nPV1Decimals	  = pParse->ReadByte();
		m_nPV2Decimals	  = pParse->ReadByte();
		m_SPOnUnits [0]   = pParse->ReadString(4);
		m_SPOnUnits [1]   = pParse->ReadString(4);
		m_SPOnUnits [2]   = pParse->ReadString(4);
		m_SPOnUnits [3]   = pParse->ReadString(4);
		m_SPOffUnits[0]   = pParse->ReadString(4);
		m_SPOffUnits[1]   = pParse->ReadString(4);
		m_SPOffUnits[2]   = pParse->ReadString(4);
		m_SPOffUnits[3]   = pParse->ReadString(4);
		m_nSP1OnDecimals  = pParse->ReadByte();
		m_nSP2OnDecimals  = pParse->ReadByte();
		m_nSP3OnDecimals  = pParse->ReadByte();
		m_nSP4OnDecimals  = pParse->ReadByte();
		m_nSP1OffDecimals = pParse->ReadByte();
		m_nSP2OffDecimals = pParse->ReadByte();
		m_nSP3OffDecimals = pParse->ReadByte();
		m_nSP4OffDecimals = pParse->ReadByte();
		m_nBlockNum	  = pParse->ReadBigWord();

		return TRUE;
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag		  = pParse->ReadString(16);
		m_Desc		  = pParse->ReadString(16);
		m_Labels[0]	  = pParse->ReadString(8);
		m_Labels[1]	  = pParse->ReadString(8);
		m_Labels[2]	  = pParse->ReadString(8);
		m_Labels[3]	  = pParse->ReadString(8);
		m_PV1Units	  = pParse->ReadString(6);
		m_PV2Units	  = pParse->ReadString(6);
		m_nPV1Decimals	  = pParse->ReadByte();
		m_nPV2Decimals	  = pParse->ReadByte();
		m_SPOnUnits [0]   = pParse->ReadString(6);
		m_SPOnUnits [1]   = pParse->ReadString(6);
		m_SPOnUnits [2]   = pParse->ReadString(6);
		m_SPOnUnits [3]   = pParse->ReadString(6);
		m_SPOffUnits[0]   = pParse->ReadString(6);
		m_SPOffUnits[1]   = pParse->ReadString(6);
		m_SPOffUnits[2]   = pParse->ReadString(6);
		m_SPOffUnits[3]   = pParse->ReadString(6);
		m_nSP1OnDecimals  = pParse->ReadByte();
		m_nSP2OnDecimals  = pParse->ReadByte();
		m_nSP3OnDecimals  = pParse->ReadByte();
		m_nSP4OnDecimals  = pParse->ReadByte();
		m_nSP1OffDecimals = pParse->ReadByte();
		m_nSP2OffDecimals = pParse->ReadByte();
		m_nSP3OffDecimals = pParse->ReadByte();
		m_nSP4OffDecimals = pParse->ReadByte();
		m_nBlockNum	  = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CStageTagRecord::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"Stage", m_Tag);

	for( int stage = 0; stage < 4; stage++ ) {

		Tags.NewFolder(CPrintf(L"Stage%d", stage+1));

		Tags.EmitTextC(NULL, L"Label", m_Labels[stage]);

		Tags.EmitEnumM(NULL, L"Stage Status",  PropD(17 + stage), Hc900DisabledEnabledEnum);
		Tags.EmitEnumM(NULL, L"Output Status", PropD(13 + stage), Hc900StageOutputStatusEnum);
		Tags.EmitEnumM(NULL, L"Override",      PropD( 1 + stage), Hc900StageOverrideEnum);
		Tags.EmitEnumM(NULL, L"Interlock",     PropD( 9 + stage), Hc900StageInterlockEnum);

		Tags.EmitSintW(NULL, L"On Delay",     PropS(28 + stage), 0, 99999);
		Tags.EmitSintW(NULL, L"Off Delay",    PropS(32 + stage), 0, 99999);

		switch( stage ) {

			case 0:
				Tags.EmitRealW(NULL, L"Setpoint On",  PropS(12 + stage), m_nSP1OnDecimals );
				Tags.EmitRealW(NULL, L"Setpoint Off", PropS(16 + stage), m_nSP1OffDecimals);
				break;

			case 1:
				Tags.EmitRealW(NULL, L"Setpoint On",  PropS(12 + stage), m_nSP2OnDecimals );
				Tags.EmitRealW(NULL, L"Setpoint Off", PropS(16 + stage), m_nSP2OffDecimals);
				break;

			case 2:
				Tags.EmitRealW(NULL, L"Setpoint On",  PropS(12 + stage), m_nSP3OnDecimals );
				Tags.EmitRealW(NULL, L"Setpoint Off", PropS(16 + stage), m_nSP3OffDecimals);
				break;

			case 3:
				Tags.EmitRealW(NULL, L"Setpoint On",  PropS(12 + stage), m_nSP4OnDecimals );
				Tags.EmitRealW(NULL, L"Setpoint Off", PropS(16 + stage), m_nSP4OffDecimals);
				break;
			}

		Tags.EmitFnumW(NULL, L"Interlock With Next Stage", PropS(40 + stage), Hc900NoYesEnum);
		Tags.EmitFnumW(NULL, L"Interlock With Prev Stage", PropS(36 + stage), Hc900NoYesEnum);

		Tags.EndFolder();
		}

	Tags.EmitTextC(NULL, L"Description",  m_Desc);

	Tags.EmitTextC(NULL, L"Input Units",  m_PV1Units);

	Tags.EmitTextC(NULL, L"Output Units", m_PV2Units);

	Tags.EmitRealM(NULL, L"PV1", PropD(22), m_nPV1Decimals);

	Tags.EmitRealM(NULL, L"PV2", PropD(23), m_nPV2Decimals);

	Tags.EndFolder();
	}

// Stage Tag Table

BOOL CDataFile::CStageTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CStageTagRecord);
	}

void CDataFile::CStageTagTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CStageTagRecord, L"Stage");
	}

// Ramp Tag

BOOL CDataFile::CRampTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag	      = pParse->ReadString(8);
		m_Desc	      = pParse->ReadString(16);
		m_Label[0]    = pParse->ReadString(8);
		m_Label[1]    = pParse->ReadString(8);
		m_Label[2]    = pParse->ReadString(8);
		m_Label[3]    = pParse->ReadString(8);
		m_OutUnits    = pParse->ReadString(4);
		m_PVUnits     = pParse->ReadString(4);
		m_nPVDecimal  = pParse->ReadByte();
		m_nOutDecimal = pParse->ReadByte();
		m_nBlockNum   = pParse->ReadBigWord();

		return TRUE;
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag	      = pParse->ReadString(16);
		m_Desc	      = pParse->ReadString(16);
		m_Label[0]    = pParse->ReadString(8);
		m_Label[1]    = pParse->ReadString(8);
		m_Label[2]    = pParse->ReadString(8);
		m_Label[3]    = pParse->ReadString(8);
		m_OutUnits    = pParse->ReadString(6);
		m_PVUnits     = pParse->ReadString(6);
		m_nPVDecimal  = pParse->ReadByte();
		m_nOutDecimal = pParse->ReadByte();
		m_nBlockNum   = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CRampTagRecord::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"Ramp", m_Tag);

	for( int ramp = 0; ramp < 4; ramp++ ) {

		Tags.NewFolder(CPrintf(L"Ramp%d", 1+ramp));

		Tags.EmitTextC(NULL, L"Label", m_Label[ramp]);

		Tags.EmitEnumM(NULL, L"Ramp Status", PropD(7 + ramp), Hc900DisabledEnabledEnum);
		Tags.EmitEnumM(NULL, L"Override",    PropD(1 + ramp), Hc900RampOverrideEnum);

		Tags.EmitRealW(NULL, L"Input Low Limit",   PropS(12 + ramp), m_nPVDecimal );
		Tags.EmitRealW(NULL, L"Input High Limit",  PropS( 8 + ramp), m_nPVDecimal );
		Tags.EmitRealW(NULL, L"Output Scale Low",  PropS( 4 + ramp), m_nOutDecimal);
		Tags.EmitRealW(NULL, L"Output Scale High", PropS( 0 + ramp), m_nOutDecimal);

		Tags.EndFolder();
		}

	Tags.EmitTextC(NULL, L"Description",  m_Desc);

	Tags.EmitTextC(NULL, L"Input Units",  m_PVUnits);

	Tags.EmitTextC(NULL, L"Output Units", m_OutUnits);

	Tags.EmitFnumM(NULL, L"Block Status", PropD(16), Hc900DisabledEnabledEnum);

	Tags.EmitRealM(NULL, L"Input",	PropD(17), m_nPVDecimal);

	Tags.EmitRealM(NULL, L"Output", PropD( 5), m_nOutDecimal);

	Tags.EmitRealW(NULL, L"Transfer Rate Up",   PropS(17), m_nPVDecimal);

	Tags.EmitRealW(NULL, L"Transfer Rate Down", PropS(18), m_nPVDecimal);

	Tags.EmitRealW(L"LagTime", L"Lag Time", PropS(19), 0, 0, 120);

	Tags.EndFolder();
	}

// Ramp Tags

BOOL CDataFile::CRampTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CRampTagRecord);
	}

void CDataFile::CRampTagTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CRampTagRecord, L"Ramp");
	}

// Alternator Tag

BOOL CDataFile::CAlternatorTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag	    = pParse->ReadString(8);
		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag	    = pParse->ReadString(16);
		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CAlternatorTagRecord::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"Alternator", m_Tag);

	for( int channel = 0; channel < 16; channel++ ) {

		Tags.NewFolder(CPrintf(L"Channel%d", channel+1));

		Tags.EmitEnumM(NULL, L"Device", 	 PropD( 2 + channel), Hc900AlternatorDeviceEnum);
		Tags.EmitEnumM(NULL, L"Status Of Input", PropD(18 + channel), Hc900AlternatorInputStatusEnum);
		Tags.EmitEnumM(NULL, L"Output Status",	 PropD(34 + channel), Hc900AlternatorOutputStatusEnum);
		Tags.EmitSintM(NULL, L"Sequence Order",  PropD(53 + channel)  );
		Tags.EmitFnumW(NULL, L"Input Enable",	 PropS( 6 + channel), Hc900OffOnEnum);
		Tags.EmitEnumW(NULL, L"Output Enable",	 PropS(22 + channel), Hc900OffOnEnum);

		Tags.EmitSintW(NULL, L"Seq Order", AddrAS(m_nBlockNum, channel));

		Tags.EndFolder();
		}

	Tags.EmitSintW(NULL, L"Seq Command", AddrAS(m_nBlockNum, 16));

	Tags.EmitTextC(NULL, L"Description", m_Desc);

	Tags.EmitEnumM(NULL, L"Alternator Status", PropD(1),  Hc900AlternatorStatusEnum);
	Tags.EmitSintM(NULL, L"Input Count",	   PropD(51)  );
	Tags.EmitSintW(NULL, L"On Delay",	   PropS( 1), 0, 99999);
	Tags.EmitSintW(NULL, L"Off Delay",	   PropS( 2), 0, 99999);
	Tags.EmitFnumM(NULL, L"State",		   PropD(87), Hc900OffRunEnum);
	Tags.EmitEnumW(NULL, L"Alternator Style",  PropD(50), Hc900AlternatorModeEnum);
	Tags.EmitFnumW(NULL, L"Advance",	   PropD(52), Hc900OffOnEnum);
	Tags.EmitFnumW(NULL, L"Advance Active",    PropS( 0), Hc900OffOnEnum);
	Tags.EmitSintW(NULL, L"Max Out Used",	   PropS( 5)  );
	Tags.EmitFnumW(NULL, L"Make Break",	   PropS( 3), Hc900OffOnEnum);
	Tags.EmitEnumW(NULL, L"Current Alt Style", PropS( 4), Hc900AlternatorModeEnum);

	Tags.EndFolder();
	}

// Alternator Tags

BOOL CDataFile::CAlternatorTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CAlternatorTagRecord);
	}

void CDataFile::CAlternatorTagTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CAlternatorTagRecord, L"Alternator");
	}

// Hand-Off-Auto Tag

BOOL CDataFile::CHoaTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag	    = pParse->ReadString(8);
		m_Desc	    = pParse->ReadString(16);
		m_State[0]  = pParse->ReadString(8);
		m_State[1]  = pParse->ReadString(8);
		m_State[2]  = pParse->ReadString(8);
		m_State[3]  = pParse->ReadString(8);
		m_State[4]  = pParse->ReadString(8);
		m_State[5]  = pParse->ReadString(8);
		m_State[6]  = pParse->ReadString(8);
		m_State[7]  = pParse->ReadString(8);
		m_State[8]  = pParse->ReadString(8);
		m_Feedback  = pParse->ReadString(8);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag	    = pParse->ReadString(16);
		m_Desc	    = pParse->ReadString(16);
		m_State[0]  = pParse->ReadString(8);
		m_State[1]  = pParse->ReadString(8);
		m_State[2]  = pParse->ReadString(8);
		m_State[3]  = pParse->ReadString(8);
		m_State[4]  = pParse->ReadString(8);
		m_State[5]  = pParse->ReadString(8);
		m_State[6]  = pParse->ReadString(8);
		m_State[7]  = pParse->ReadString(8);
		m_State[8]  = pParse->ReadString(8);
		m_Feedback  = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CHoaTagRecord::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"HandOffAutoSwitch", m_Tag);

	Tags.EmitTextC(NULL, L"Description", m_Desc);

	Tags.EmitEnumM(NULL, L"Current State", PropD(1), Hc900HoaCurrentStateEnum);
	Tags.EmitEnumW(NULL, L"New State",     PropD(2), Hc900HoaNewStateEnum);
	Tags.EmitEnumM(NULL, L"Source",	       PropD(4), Hc900HoaSourceEnum);

	CString FeedbackStateEnum = L"";

	for (int i = 0; i < 9; i++) {

		CString State = m_State[i];

		State.StripAll();

		if( State.GetLength() > 0 ) {

			if( i > 0) {

				FeedbackStateEnum += L"|";
				}

			FeedbackStateEnum += State;
			}
		}

	Tags.EmitEnumM(NULL, L"Feedback State",       PropD(5), FeedbackStateEnum);

	Tags.EmitTextC(NULL, L"Feedback Signal Name", m_Feedback);

	Tags.EndFolder();
	}

// Hand-Off-Auto Tags

BOOL CDataFile::CHoaTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CHoaTagRecord);
	}

void CDataFile::CHoaTagTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CHoaTagRecord, L"HandOffAutoSwitch");
	}

// Device Control Tag

BOOL CDataFile::CDevCtlTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Tag	    = pParse->ReadString(8);
		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	if( Header.m_nRevNum == 3 ) {

		m_Tag	    = pParse->ReadString(16);
		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CDevCtlTagRecord::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"DeviceControl", m_Tag);

	Tags.EmitTextC(NULL, L"Description", m_Desc);

	Tags.EmitFnumM(NULL, L"State",		     PropD(10), Hc900DeviceControlStateEnum);
	Tags.EmitFnumM(NULL, L"Run Request",	     PropD(12), Hc900RequestStartStopEnum);
	Tags.EmitFnumM(NULL, L"Active", 	     PropD(11), Hc900InactiveActiveEnum);
	Tags.EmitSintM(NULL, L"Time Remaining",      PropD( 2)	);
	Tags.EmitFnumM(NULL, L"Failed", 	     PropD(14), Hc900OkFailedEnum);
	Tags.EmitFnumW(NULL, L"Reset",		     PropD( 1), Hc900NormalResetEnum);
	Tags.EmitSintW(NULL, L"Start Delay",	     PropS( 1)	);
	Tags.EmitSintW(NULL, L"Stop Delay",	     PropS( 2)	);
	Tags.EmitSintW(NULL, L"Feedback Fail Delay", PropS( 3)	);

	Tags.EndFolder();
	}

// Device Control Tags

BOOL CDataFile::CDevCtlTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CDevCtlTagRecord);
	}

void CDataFile::CDevCtlTagTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CDevCtlTagRecord, L"DeviceControl");
	}

// Drum Sequencer Tag

BOOL CDataFile::CDrumSeqTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		int j;

		m_Tag	    = pParse->ReadString(8);
		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		for( j = 0; j < 50; j++ ) {

			m_PhaseLabels[j] = pParse->ReadString(12);
			}

		for( j = 0; j < 16; j++ ) {

			m_OutLabels[j] = pParse->ReadString(8);
			}

		for( j = 0; j < 50; j++ ) {

			m_nOutStates[j] = pParse->ReadWord();
			}

		for( j = 0; j < 50; j++ ) {

			m_nEvent1Index[j] = pParse->ReadBigWord();
			}

		for( j = 0; j < 50; j++ ) {

			m_nEvent2Index[j] = pParse->ReadBigWord();
			}

		m_AuxLabel   = pParse->ReadString(8);
		m_AuxEU      = pParse->ReadString(4);
		m_nAuxDecPos = pParse->ReadByte();

		pParse->ReadByte();

		return TRUE;
		}

	if( Header.m_nRevNum == 2 ) {

		int j;

		m_Tag	    = pParse->ReadString(16);
		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		for( j = 0; j < 50; j++ ) {

			m_PhaseLabels[j] = pParse->ReadString(12);
			}

		for( j = 0; j < 16; j++ ) {

			m_OutLabels[j] = pParse->ReadString(8);
			}

		for( j = 0; j < 50; j++ ) {

			m_nOutStates[j] = pParse->ReadWord();
			}

		for( j = 0; j < 50; j++ ) {

			m_nEvent1Index[j] = pParse->ReadBigWord();
			}

		for( j = 0; j < 50; j++ ) {

			m_nEvent2Index[j] = pParse->ReadBigWord();
			}

		m_AuxLabel   = pParse->ReadString(8);
		m_AuxEU      = pParse->ReadString(6);
		m_nAuxDecPos = pParse->ReadByte();

		pParse->ReadByte();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CDrumSeqTagRecord::EmitTags(CMakeTags &Tags)
{
	// TODO -- Some of this stuff can be replaced with lookup tables
	// as can now encode these using basic array tags and avoid having
	// to get them from the comms driver at runtime.

	Tags.NewFolder(L"Sequencer", m_Tag);

	Tags.EmitTextC(NULL, L"Description", m_Desc);

	Tags.EmitRealW(NULL, L"Load Recipe", PropD(3), 0, 0, 99999);
	Tags.EmitRealW(NULL, L"Save Recipe", PropD(1), 0, 1, 99999);

	Tags.EmitFnumW(NULL, L"State", PropD(2), Hc900SequencerStateEnum);

	Tags.EmitRealW(NULL, L"Step Change",	       PropD( 4), 0);
	Tags.EmitFnumW(NULL, L"Step Advance",	       PropD( 5), Hc900NormalAdvanceEnum);
	Tags.EmitRealM(NULL, L"Out Status",	       PropD( 6), 1);
	Tags.EmitRealW(NULL, L"Step Jumped To JOG IP", PropD(11), 0);
	Tags.EmitEnumW(NULL, L"Time Units",	       PropD(12), Hc900SetpointSchedulerTimeUnitsEnum);
	Tags.EmitRealM(NULL, L"Sequencer Number",      PropD(17), 0);
	Tags.EmitRealM(NULL, L"Step Number",	       PropD(28), 0);
	Tags.EmitRealM(NULL, L"Advance To Step",       PropD(24), 0);

	for( int i = 1; i <= 16; i++ ) {

		Tags.EmitEnumM( NULL,
				CPrintf(L"Phase Output %d", i),
				PropD(i+28),
				Hc900OffOnEnum
				);
		}

	Tags.EmitRealM(NULL, L"Time Remaining",        PropD(45), 1);
	Tags.EmitFnumM(NULL, L"Drum Sequence State",   PropD(46), Hc900SequencerDrumStateEnum);
	Tags.EmitRealM(NULL, L"Phase Number",	       PropD(47), 0);
	Tags.EmitTextW(NULL, L"Sequence Name",	       PropD( 8), 8);
	Tags.EmitTextM(NULL, L"Cur Phase Name",        PropD(14), 12);
	Tags.EmitTextW(NULL, L"Sequence Desc",	       PropD(18), 16);
	Tags.EmitTimeM(NULL, L"Sequence Elapsed Time", PropD(22), 1);
	Tags.EmitTimeM(NULL, L"Step Elapsed Time",     PropD(23), 1);
	Tags.EmitTimeM(NULL, L"Step Time Remaining",   PropD(45), 1);
	Tags.EmitRealM(NULL, L"Aux Output",	       PropD(48), m_nAuxDecPos);

	// Prop     Parameter	    Controller Encoding
	//
	// 0	    Output01Label   string (8 chars)
	// 2	    Output02Label   string (8 chars)
	// :	      : 	       :
	// 30	    Output16Label   string (8 chars)
	//
	// 32	    AuxLabel	    string (8 chars)
	// 34	    AuxUnits	    string (6 chars)
	// 36	    AuxDecPos	    numLong

	for( int i = 0; i < 16; i++ ) {

		UINT Prop = 2 * i;

		Tags.EmitTextM( NULL,
				CPrintf(L"Output %d Label", 1+i),
				AddrDH(m_nBlockNum, Prop),
				8
				);
		}

	Tags.EmitTextM(NULL, L"Aux Label",   AddrDH(m_nBlockNum, 32), 8);

	Tags.EmitTextM(NULL, L"Aux Units",   AddrDH(m_nBlockNum, 34), 8);

	Tags.EmitSintM(NULL, L"Aux Dec Pos", AddrDH(m_nBlockNum, 36));

	////////

	// Prop     Parameter	    Controller Encoding
	//
	// 0	    PhaseLabel	    string (12 chars)
	// 150	    Output01State   enumInt:Hc900OffOnEnum
	// 200	    Output02State   enumInt:Hc900OffOnEnum
	// :	      : 	       :
	// 900	    Output16State   enumInt:Hc900OffOnEnum
	// 950	    Event1Signal    string (16 chars)
	// 1150     Event2Signal    string (16 chars)

	Tags.NewFolder(L"Phases");

	Tags.EmitTextA( NULL,
			L"Phase Label",
			AddrDP(m_nBlockNum, 0),
			12,
			50,
			FALSE
			);

	for( int j = 0; j < 16; j++ ) {

		UINT Prop = 150 + j * 50;

		Tags.EmitEnumA( NULL,
				CPrintf(L"Output %d State", 1+j),
				AddrDP(m_nBlockNum, Prop),
				Hc900OffOnEnum,
				50,
				FALSE
				);
		}

	Tags.EmitTextA( NULL,
			L"Event 1 Signal",
			AddrDP(m_nBlockNum, 950),
			16,
			50,
			FALSE
			);

	Tags.EmitTextA( NULL,
			L"Event 2 Signal",
			AddrDP(m_nBlockNum, 1150),
			16,
			50,
			FALSE
			);

	Tags.EndFolder();

	////////

	// Prop     Parameter	    Encoding
	// 0	    PhaseNum	    numLong
	// 64	    TimeNextStep    numLong
	// 128	    Sig1NextStep    numLong
	// 192	    Sig2NextStep    numLong
	// 256	    AdvNextStep     numLong
	// 320	    Time	    numFloat
	// 384	    AuxValue	    numFloat

	Tags.NewFolder(L"Segments");

	Tags.EmitSintA( NULL,
			L"Phase Num",
			AddrDS(m_nBlockNum, 0),
			64,
			0,
			50,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Time Next Step",
			AddrDS(m_nBlockNum, 64),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Sig 1 Next Step",
			AddrDS(m_nBlockNum, 128),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Sig 2 Next Step",
			AddrDS(m_nBlockNum, 192),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Adv Next Step",
			AddrDS(m_nBlockNum, 256),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"Time",
			AddrDS(m_nBlockNum, 320),
			3,
			0,
			99999.999,
			64,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"AuxValue",
			AddrDS(m_nBlockNum, 384),
			m_nAuxDecPos,
			0,
			100000 - pow((double)10.0, -m_nAuxDecPos),
			64,
			TRUE
			);

	Tags.EndFolder();

	Tags.EndFolder();
	}

// Drum Sequencer Tags

BOOL CDataFile::CDrumSeqTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CDrumSeqTagRecord);
	}

void CDataFile::CDrumSeqTagTable::EmitTags(CMakeTags &Tags)
{
	StdLook(CDrumSeqTagRecord, L"Sequencer", m_Tag);
	}

void CDataFile::CDrumSeqTagTable::EmitWork(CMakeTags &Tags)
{
	// Prop     Parameter	    Encoding
	// 0	    PhaseNum	    numLong
	// 64	    TimeNextStep    numLong
	// 128	    Sig1NextStep    numLong
	// 192	    Sig2NextStep    numLong
	// 256	    AdvNextStep     numLong
	// 320	    Time	    numFloat
	// 384	    AuxValue	    numFloat

	Tags.NewFolder(L"Seq");

	Tags.EmitSintW(NULL, L"ReqWrite", SpecSystem(10));
	
	Tags.EmitSintW(NULL, L"ReqRead",  SpecSystem(11));

	int m_nBlockNum = 32000;

	Tags.EmitTextW(NULL, L"Sequence Name",	       PropD( 8), 8);
	Tags.EmitTextW(NULL, L"Sequence Desc",	       PropD(18), 16);
	Tags.EmitRealW(NULL, L"Step Jumped To JOG IP", PropD(11), 0);
	Tags.EmitEnumW(NULL, L"Time Units",	       PropD(12), Hc900SetpointSchedulerTimeUnitsEnum);

	Tags.NewFolder(L"Segments");

	Tags.EmitSintA( NULL,
			L"Phase Num",
			AddrDS(m_nBlockNum, 0),
			64,
			0,
			50,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Time Next Step",
			AddrDS(m_nBlockNum, 64),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Sig 1 Next Step",
			AddrDS(m_nBlockNum, 128),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Sig 2 Next Step",
			AddrDS(m_nBlockNum, 192),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Adv Next Step",
			AddrDS(m_nBlockNum, 256),
			64,
			0,
			64,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"Time",
			AddrDS(m_nBlockNum, 320),
			3,
			0,
			99999.999,
			64,
			TRUE
			);

	Tags.EmitRealA( NULL,
			L"AuxValue",
			AddrDS(m_nBlockNum, 384),
			2,
			0,
			100000 - pow((double)10.0, -2),
			64,
			TRUE
			);

	Tags.EndFolder();

	Tags.EndFolder();
	}

// Setpoint Sequencer Tag

BOOL CDataFile::CSpsTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		m_Tag	    = pParse->ReadString(8);
		m_Desc	    = pParse->ReadString(16);
		m_Units     = pParse->ReadString(4);
		m_nBlockNum = pParse->ReadBigWord();
		m_nAuxBlock = pParse->ReadBigWord();

		return TRUE;
		}

	if( Header.m_nRevNum == 2 ) {

		m_Tag	    = pParse->ReadString(16);
		m_Desc	    = pParse->ReadString(16);
		m_Units     = pParse->ReadString(6);
		m_nBlockNum = pParse->ReadBigWord();
		m_nAuxBlock = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CSpsTagRecord::EmitTags(CMakeTags &Tags, CSpsDisplayRecord &Display)
{
	Tags.NewFolder(L"SetpointScheduler", m_Tag);

	Tags.EmitTextC(NULL, L"Description", m_Desc);

	Tags.EmitTextW(NULL, L"Schedule Name", PropD(28), 8);
	Tags.EmitTextW(NULL, L"Schedule Desc", PropD(30), 16);

	Tags.EmitRealM(NULL, L"Schedule Number", PropD(46), 0);

	Tags.EmitRealW(NULL, L"Load Recipe", PropD(3), 0, 0, 99999);
	Tags.EmitRealW(NULL, L"Save Recipe", PropD(1), 0, 1, 99999);

	Tags.EmitFnumM(NULL, L"State", PropD(44), Hc900SetpointSchedulerStateEnum);

	Tags.EmitRealM(NULL, L"PV1", PropD(14), Display.m_nDec[0]);
	Tags.EmitRealM(NULL, L"PV2", PropD(15), Display.m_nDec[1]);
	Tags.EmitRealM(NULL, L"PV3", PropD(16), Display.m_nDec[2]);
	Tags.EmitRealM(NULL, L"PV4", PropD(17), Display.m_nDec[3]);
	Tags.EmitRealM(NULL, L"PV5", PropD(18), Display.m_nDec[4]);
	Tags.EmitRealM(NULL, L"PV6", PropD(19), Display.m_nDec[5]);
	Tags.EmitRealM(NULL, L"PV7", PropD(20), Display.m_nDec[6]);
	Tags.EmitRealM(NULL, L"PV8", PropD(21), Display.m_nDec[7]);

	Tags.EmitRealM(NULL, L"SP1", PropD(47), Display.m_nDec[0]);
	Tags.EmitRealM(NULL, L"SP2", PropD(48), Display.m_nDec[1]);
	Tags.EmitRealM(NULL, L"SP3", PropD(49), Display.m_nDec[2]);
	Tags.EmitRealM(NULL, L"SP4", PropD(50), Display.m_nDec[3]);
	Tags.EmitRealM(NULL, L"SP5", PropD(51), Display.m_nDec[4]);
	Tags.EmitRealM(NULL, L"SP6", PropD(52), Display.m_nDec[5]);
	Tags.EmitRealM(NULL, L"SP7", PropD(53), Display.m_nDec[6]);
	Tags.EmitRealM(NULL, L"SP8", PropD(54), Display.m_nDec[7]);

	Tags.EmitRealM(NULL, L"Segment Number", 	PropD(45), 0);
	Tags.EmitSintM(NULL, L"Recycles Remaining",	PropD(24)  );
	Tags.EmitTimeM(NULL, L"Segment Time Remaining", PropD(55), 1);
	Tags.EmitTimeM(NULL, L"Segment Elapsed Time",	PropD(56), 1);
	Tags.EmitTimeM(NULL, L"Total Elapsed Time",	PropD(57), 1);
	Tags.EmitFnumW(NULL, L"New State",		PropD( 2), Hc900SetpointSchedulerNewStateEnum);
	Tags.EmitFnumW(NULL, L"Advance",		PropD(22), Hc900SetpointProgrammerAdvanceEnum);

	Tags.EmitRealW(NULL, L"New Segment Request", PropD(4), 0);

	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 1", PropD(34), Display.m_nDec[0]);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 2", PropD(35), Display.m_nDec[1]);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 3", PropD(36), Display.m_nDec[2]);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 4", PropD(37), Display.m_nDec[3]);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 5", PropD(38), Display.m_nDec[4]);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 6", PropD(39), Display.m_nDec[5]);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 7", PropD(40), Display.m_nDec[6]);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 8", PropD(41), Display.m_nDec[7]);

	Tags.EmitRealW(NULL, L"Seg Jumped To JOG IP", PropD(42), 0);

	Tags.EmitEnumW(NULL, L"Time Units", PropD(43), Hc900SetpointSchedulerTimeUnitsEnum);

	////////

	// Prop     Parameter	    Controller Encoding
	//
	// 0	    SP1Label	    string (8 chars)
	// 2	    SP2Label	    string (8 chars)
	// :	      : 	       :
	// 14	    SP8Label	    string (8 chars)
	//
	// 16	    SP1Units	    string (6 chars)
	// 18	    SP2Units	    string (6 chars)
	// :	      : 	       :
	// 30	    SP8Units	    string (6 chars)
	//
	// 32	    SP1DecPos	    numLong
	// 33	    SP2DecPos	    numLong
	// :	      : 	       :
	// 39	    SP8DecPos	    numLong
	//
	// 40	    Aux1Label	    string (8 chars)
	// 42	    Aux2Label	    string (8 chars)
	// :	      : 	       :
	// 54	    Aux8Label	    string (8 chars)
	//
	// 56	    Aux1Units	    string (6 chars)
	// 58	    Aux2Units	    string (6 chars)
	// :	      : 	       :
	// 70	    Aux8Units	    string (6 chars)
	//
	// 72	    Aux1DecPos	    numLong
	// 73	    Aux2DecPos	    numLong
	// :	      : 	       :
	// 79	    Aux8DecPos	    numLong
	//
	// 80	    Event1Label     string (8 chars)
	// 82	    Event2Label     string (8 chars)
	// :	      : 	       :
	// 94	    Event16Label    string (8 chars)

	Tags.NewFolder(L"MainOutputLabels");

	Tags.EmitTextM(L"SP1Label", L"SP1 Label", AddrSD(m_nBlockNum,  0), 8);
	Tags.EmitTextM(L"SP2Label", L"SP2 Label", AddrSD(m_nBlockNum,  2), 8);
	Tags.EmitTextM(L"SP3Label", L"SP3 Label", AddrSD(m_nBlockNum,  4), 8);
	Tags.EmitTextM(L"SP4Label", L"SP4 Label", AddrSD(m_nBlockNum,  6), 8);
	Tags.EmitTextM(L"SP5Label", L"SP5 Label", AddrSD(m_nBlockNum,  8), 8);
	Tags.EmitTextM(L"SP6Label", L"SP6 Label", AddrSD(m_nBlockNum, 10), 8);
	Tags.EmitTextM(L"SP7Label", L"SP7 Label", AddrSD(m_nBlockNum, 12), 8);
	Tags.EmitTextM(L"SP8Label", L"SP8 Label", AddrSD(m_nBlockNum, 14), 8);

	Tags.EmitTextM(L"SP1Units", L"SP1 Units", AddrSD(m_nBlockNum, 16), 8);
	Tags.EmitTextM(L"SP2Units", L"SP2 Units", AddrSD(m_nBlockNum, 18), 8);
	Tags.EmitTextM(L"SP3Units", L"SP3 Units", AddrSD(m_nBlockNum, 20), 8);
	Tags.EmitTextM(L"SP4Units", L"SP4 Units", AddrSD(m_nBlockNum, 22), 8);
	Tags.EmitTextM(L"SP5Units", L"SP5 Units", AddrSD(m_nBlockNum, 24), 8);
	Tags.EmitTextM(L"SP6Units", L"SP6 Units", AddrSD(m_nBlockNum, 26), 8);
	Tags.EmitTextM(L"SP7Units", L"SP7 Units", AddrSD(m_nBlockNum, 28), 8);
	Tags.EmitTextM(L"SP8Units", L"SP8 Units", AddrSD(m_nBlockNum, 30), 8);

	Tags.EmitSintM(NULL, L"SP1 Dec Pos", AddrSD(m_nBlockNum, 32));
	Tags.EmitSintM(NULL, L"SP2 Dec Pos", AddrSD(m_nBlockNum, 33));
	Tags.EmitSintM(NULL, L"SP3 Dec Pos", AddrSD(m_nBlockNum, 34));
	Tags.EmitSintM(NULL, L"SP4 Dec Pos", AddrSD(m_nBlockNum, 35));
	Tags.EmitSintM(NULL, L"SP5 Dec Pos", AddrSD(m_nBlockNum, 36));
	Tags.EmitSintM(NULL, L"SP6 Dec Pos", AddrSD(m_nBlockNum, 37));
	Tags.EmitSintM(NULL, L"SP7 Dec Pos", AddrSD(m_nBlockNum, 38));
	Tags.EmitSintM(NULL, L"SP8 Dec Pos", AddrSD(m_nBlockNum, 39));

	Tags.EndFolder();

	Tags.NewFolder(L"AuxOutputLabels");

	Tags.EmitTextM(L"Aux1Label", L"Aux1 Label", AddrSD(m_nBlockNum, 40), 8);
	Tags.EmitTextM(L"Aux2Label", L"Aux2 Label", AddrSD(m_nBlockNum, 42), 8);
	Tags.EmitTextM(L"Aux3Label", L"Aux3 Label", AddrSD(m_nBlockNum, 44), 8);
	Tags.EmitTextM(L"Aux4Label", L"Aux4 Label", AddrSD(m_nBlockNum, 46), 8);
	Tags.EmitTextM(L"Aux5Label", L"Aux5 Label", AddrSD(m_nBlockNum, 48), 8);
	Tags.EmitTextM(L"Aux6Label", L"Aux6 Label", AddrSD(m_nBlockNum, 50), 8);
	Tags.EmitTextM(L"Aux7Label", L"Aux7 Label", AddrSD(m_nBlockNum, 52), 8);
	Tags.EmitTextM(L"Aux8Label", L"Aux8 Label", AddrSD(m_nBlockNum, 54), 8);

	Tags.EmitTextM(L"Aux1Units", L"Aux1 Units", AddrSD(m_nBlockNum, 56), 8);
	Tags.EmitTextM(L"Aux2Units", L"Aux2 Units", AddrSD(m_nBlockNum, 58), 8);
	Tags.EmitTextM(L"Aux3Units", L"Aux3 Units", AddrSD(m_nBlockNum, 60), 8);
	Tags.EmitTextM(L"Aux4Units", L"Aux4 Units", AddrSD(m_nBlockNum, 62), 8);
	Tags.EmitTextM(L"Aux5Units", L"Aux5 Units", AddrSD(m_nBlockNum, 64), 8);
	Tags.EmitTextM(L"Aux6Units", L"Aux6 Units", AddrSD(m_nBlockNum, 66), 8);
	Tags.EmitTextM(L"Aux7Units", L"Aux7 Units", AddrSD(m_nBlockNum, 68), 8);
	Tags.EmitTextM(L"Aux8Units", L"Aux8 Units", AddrSD(m_nBlockNum, 70), 8);

	Tags.EmitSintM(NULL, L"Aux1 Dec Pos", AddrSD(m_nBlockNum, 72));
	Tags.EmitSintM(NULL, L"Aux2 Dec Pos", AddrSD(m_nBlockNum, 73));
	Tags.EmitSintM(NULL, L"Aux3 Dec Pos", AddrSD(m_nBlockNum, 74));
	Tags.EmitSintM(NULL, L"Aux4 Dec Pos", AddrSD(m_nBlockNum, 75));
	Tags.EmitSintM(NULL, L"Aux5 Dec Pos", AddrSD(m_nBlockNum, 76));
	Tags.EmitSintM(NULL, L"Aux6 Dec Pos", AddrSD(m_nBlockNum, 77));
	Tags.EmitSintM(NULL, L"Aux7 Dec Pos", AddrSD(m_nBlockNum, 78));
	Tags.EmitSintM(NULL, L"Aux8 Dec Pos", AddrSD(m_nBlockNum, 79));

	Tags.EndFolder();

	Tags.NewFolder(L"EventLabels");

	for( int i = 0; i < 16; i++ ) {

		UINT Prop = 80 + i * 2;

		Tags.EmitTextM( CPrintf(L"Event%dLabel",   1+i),
				       CPrintf(L"Event %d Label", 1+i),
				       AddrSD(m_nBlockNum, Prop),
				       8
				       );
		}

	Tags.EndFolder();

	////////

	// Prop     Parameter	    Controller Encoding
	//
	// 0	    SegmentTime     numFloat
	//
	// 50	    GuarSoakType1   enumInt:Hc900SetpointSchedulerGuarSoakTypeEnum
	// 100	    GuarSoakType2   enumInt:Hc900SetpointSchedulerGuarSoakTypeEnum
	// :	      : 	       :
	// 400	    GuarSoakType8   enumInt:Hc900SetpointSchedulerGuarSoakTypeEnum
	//
	// 450	    RecycleCount    numLong
	// 500	    RecycleSegment  numLong
	//
	// 550	    StartValue1     numFloat
	// 600	    StartValue2     numFloat
	// :	      : 	       :
	// 900	    StartValue8     numFloat
	//
	// 950	    AuxValue1	    numFloat
	// 1000     AuxValue2	    numFloat
	// :	      : 	       :
	// 1300     AuxValue8	    numFloat
	//
	// 1350     Event1	    enumInt:Hc900OffOnEnum
	// 1400     Event2	    enumInt:Hc900OffOnEnum
	// :	      : 	       :
	// 2100     Event15	    enumInt:Hc900OffOnEnum

	Tags.NewFolder(L"Segments");

	Tags.EmitRealA( NULL,
			L"Segment Time",
			AddrSS(m_nBlockNum, 0),
			3,
			0,
			9999.999,
			50,
			TRUE
			);

	for( i = 0; i < 8; i++ ) {

		UINT Prop = 50 + 50 * i;

		Tags.EmitEnumA( NULL,
				CPrintf(L"Guar Soak Type %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				Hc900SetpointSchedulerGuarSoakTypeEnum,
				50,
				TRUE
				);
		}

	Tags.EmitSintA( NULL,
			L"Recycle Count",
			AddrSS(m_nBlockNum, 450),
			50,
			0,
			999,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Recycle Segment",
			AddrSS(m_nBlockNum, 500),
			50,
			0,
			50,
			TRUE
			);

	for( i = 0; i < 8; i++ ) {

		UINT Prop = 550 + 50 * i;

		Tags.EmitRealA( NULL,
				CPrintf(L"Start Value %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				Display.m_nDec[i],
				50,
				TRUE
				);
		}

	for( i = 0; i < 8; i++ ) {

		UINT Prop = 950 + 50 * i;

		Tags.EmitRealA( NULL,
				CPrintf(L"Aux Value %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				Display.m_nAuxDec[i],
				50,
				TRUE
				);
		}


	for( i = 0; i < 16; i++ ) {

		UINT Prop = 1350 + 50 * i;

		Tags.EmitEnumA( NULL,
				CPrintf(L"Event %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				Hc900OffOnEnum,
				50,
				TRUE
				);
		}

	Tags.EndFolder();

	Tags.EndFolder();
	}

// Setpoint Sequencer Tags

BOOL CDataFile::CSpsTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CSpsTagRecord);
	}

void CDataFile::CSpsTagTable::EmitTags(CMakeTags &Tags, CSpsDisplayTable &Displays)
{
	Tags.NewFolder(L"SetpointScheduler");

	CUIntArray   Index;

	CStringArray Valid, Name;

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CSpsTagRecord     &Rec  = (CSpsTagRecord     &) m_List[i];

		CSpsDisplayRecord &Disp = (CSpsDisplayRecord &) Displays.m_List[i];

		if( Rec.m_nBlockNum ) {

			Index.Append(Rec.m_nBlockNum);

			Valid.Append(L"1");

			Name.Append (Const(Rec.m_Tag));

			Rec.EmitTags(Tags, Disp);
			}
		}

	Tags.EmitSintC (NULL, L"Count",  Index.GetCount());
	
	Tags.EmitLookup(NULL, L"Index",  Index);
	
	Tags.EmitLookup(NULL, L"Valid",  Index, Valid, L"0");
	
	Tags.EmitLookup(NULL, L"Name",   Index, Name,  L"\"\"");

	Tags.EndFolder();
	}

void CDataFile::CSpsTagTable::EmitWork(CMakeTags &Tags)
{
	// Prop     Parameter	    Encoding
	// 0	    PhaseNum	    numLong
	// 64	    TimeNextStep    numLong
	// 128	    Sig1NextStep    numLong
	// 192	    Sig2NextStep    numLong
	// 256	    AdvNextStep     numLong
	// 320	    Time	    numFloat
	// 384	    AuxValue	    numFloat

	Tags.NewFolder(L"SPS");

	Tags.EmitSintW(NULL, L"ReqWrite", SpecSystem(12));
	
	Tags.EmitSintW(NULL, L"ReqRead",  SpecSystem(13));

	int m_nBlockNum = 32001;

	Tags.EmitTextW(NULL, L"Schedule Name",        PropD(28), 8);
	Tags.EmitTextW(NULL, L"Schedule Desc",        PropD(30), 16);
	Tags.EmitRealW(NULL, L"Seg Jumped To JOG IP", PropD(42), 0);
	Tags.EmitEnumW(NULL, L"Time Units",           PropD(43), Hc900SetpointSchedulerTimeUnitsEnum);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 1",   PropD(34), 2);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 2",   PropD(35), 2);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 3",   PropD(36), 2);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 4",   PropD(37), 2);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 5",   PropD(38), 2);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 6",   PropD(39), 2);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 7",   PropD(40), 2);
	Tags.EmitRealW(NULL, L"Soak Lo Hi Limit 8",   PropD(41), 2);

	Tags.NewFolder(L"Segments");

	int i;

	Tags.EmitRealA( NULL,
			L"Segment Time",
			AddrSS(m_nBlockNum, 0),
			3,
			0,
			9999.999,
			50,
			TRUE
			);

	for( i = 0; i < 8; i++ ) {

		UINT Prop = 50 + 50 * i;

		Tags.EmitEnumA( NULL,
				CPrintf(L"Guar Soak Type %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				Hc900SetpointSchedulerGuarSoakTypeEnum,
				50,
				TRUE
				);
		}

	Tags.EmitSintA( NULL,
			L"Recycle Count",
			AddrSS(m_nBlockNum, 450),
			50,
			0,
			999,
			TRUE
			);

	Tags.EmitSintA( NULL,
			L"Recycle Segment",
			AddrSS(m_nBlockNum, 500),
			50,
			0,
			50,
			TRUE
			);

	for( i = 0; i < 8; i++ ) {

		UINT Prop = 550 + 50 * i;

		Tags.EmitRealA( NULL,
				CPrintf(L"Start Value %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				2,
				50,
				TRUE
				);
		}

	for( i = 0; i < 8; i++ ) {

		UINT Prop = 950 + 50 * i;

		Tags.EmitRealA( NULL,
				CPrintf(L"Aux Value %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				2,
				50,
				TRUE
				);
		}


	for( i = 0; i < 16; i++ ) {

		UINT Prop = 1350 + 50 * i;

		Tags.EmitEnumA( NULL,
				CPrintf(L"Event %d", 1+i),
				AddrSS(m_nBlockNum, Prop),
				Hc900OffOnEnum,
				50,
				TRUE
				);
		}

	Tags.EndFolder();

	Tags.EndFolder();
	}

// Setpoint Sequencer Display

BOOL CDataFile::CSpsDisplayRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		pParse->Skip(96);

		for( int a = 0; a < 8; a++ ) {

			m_nDec[a] = pParse->ReadByte();
			}

		pParse->Skip(64);

		pParse->Skip(32);

		for( int b = 0; b < 8; b++ ) {

			m_nAuxDec[b] = pParse->ReadByte();
			}

		pParse->Skip(128);

		pParse->Skip(96);

		pParse->Skip(96);

		return TRUE;
		}

	if( Header.m_nRevNum == 2 ) {

		pParse->Skip(112);

		for( int a = 0; a < 8; a++ ) {

			m_nDec[a] = pParse->ReadByte();
			}

		pParse->Skip(64);

		pParse->Skip(48);

		for( int b = 0; b < 8; b++ ) {

			m_nAuxDec[b] = pParse->ReadByte();
			}

		pParse->Skip(128);

		pParse->Skip(96);

		pParse->Skip(96);

		return TRUE;
		}

	return FALSE;
	}

// Setpoint Sequencer Displays

BOOL CDataFile::CSpsDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CSpsDisplayRecord);
	}

// Calendar Event Display

BOOL CDataFile::CCalEvtDisplayRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		m_Title        = pParse->ReadString(24);
		int j = 0;

		for( j = 0; j < 5; j++ )
		{
			m_EventTitle[j]   = pParse->ReadString(16);
		}

		for( j = 0; j < 8; j++ )
		{
			m_EventLabel[j]   = pParse->ReadString(16);
		}

		for( j = 0; j < 16; j++ )
		{
			m_SpecDayLabel[j] = pParse->ReadString(16);
		}

		for( j = 0; j < 8; j++ )
		{

			m_nTagIndex[j] = pParse->ReadBigWord();
		}

		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CCalEvtDisplayRecord::EmitTags(CMakeTags &Tags)
{
	}

// Calendar Event Displays

BOOL CDataFile::CCalEvtDisplayTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CCalEvtDisplayRecord);
	}

void CDataFile::CCalEvtDisplayTable::EmitTags(CMakeTags &Tags)
{
	StdEmit(CCalEvtDisplayRecord, L"CalEventDisplays");
	}

// Variable

BOOL CDataFile::CVariableRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 2 ) {

		m_Name	      = pParse->ReadString(8);
		m_Label0      = pParse->ReadString(6);
		m_Label1      = pParse->ReadString(6);
		m_nDecPlaces  = pParse->ReadByte();
		m_nTagType    = pParse->ReadByte();
		m_nDescOffset = pParse->ReadBigWord();
		m_InitValue   = pParse->ReadFloat();

		return TRUE;
		}

	if( Header.m_nRevNum >= 3 ) {

		m_Name	      = pParse->ReadString(16);
		m_Label0      = pParse->ReadString(6);
		m_Label1      = pParse->ReadString(6);
		m_nDecPlaces  = pParse->ReadByte();
		m_nTagType    = pParse->ReadByte();
		m_nDescOffset = pParse->ReadBigWord();
		m_InitValue   = pParse->ReadFloat();		

		if( Header.m_nRevNum == 4 ) {

			m_bySafety = pParse->ReadByte();

			pParse->Skip(3);
			}

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CVariableRecord::FindDescText(CDescTextTable &Desc)
{
	m_Desc = Desc.GetTagDescriptor(m_nDescOffset);
	}

void CDataFile::CVariableRecord::EmitTags(CMakeTags &Tags, BOOL fAnalog, int index)
{
	if( fAnalog ) {

		if( m_nTagType == 1 ) {

			m_Folder = Tags.NewFolder(L"AnalogVariable", m_Name);

			Tags.EmitTextC(NULL, L"Description", m_Desc);

			Tags.EmitTextC(NULL, L"Units", m_Label0);

			Tags.EmitRealW(L"Out", m_Name, AddrBP(BLKNUM_VARS, DYNAMIC, index), m_nDecPlaces);

			Tags.EndFolder();
			}
		}
	else {
		if( m_nTagType == 2 ) {

			m_Folder = Tags.NewFolder(L"DigitalVariable", m_Name);

			Tags.EmitTextC(NULL, L"Description", m_Desc);

			Tags.EmitFnumW(L"Out", m_Name, AddrBP(BLKNUM_VARS, DYNAMIC, index), m_Label0, m_Label1);

			Tags.EndFolder();
			}
		}
	}

int CDataFile::CVariableRecord::operator > (CVariableRecord const &That) const
{
	return m_Name > That.m_Name;
	}

// Worksheet Record

BOOL CDataFile::CWorksheetRecord::Read(CParser *pParse, CTableHeader &Header)
{
	m_Name    = pParse->ReadString(24);
	m_Comment = pParse->ReadString(32);
	m_nType   = pParse->ReadByte();
	m_nAccess = pParse->ReadByte();
	m_nIndex  = pParse->ReadByte();
	m_nSpare  = pParse->ReadByte();

	return TRUE;
	}

void CDataFile::CWorksheetRecord::EmitTags(CMakeTags &Tags, int index)
{
	}

// Worksheet

BOOL CDataFile::CWorksheetTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CWorksheetRecord);
	}

void CDataFile::CWorksheetTable::EmitTags(CMakeTags &Tags)
{
	CStringArray Name, Comment, Type, Access;

	CUIntArray   Index;

	Tags.NewFolder(L"Worksheet");

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CWorksheetRecord &Rec = (CWorksheetRecord &) m_List[i];

		Index.Append(1+i);

		Name.Append(Const(Rec.m_Name));

		Type.Append(CPrintf(L"%u", Rec.m_nType));

		Access.Append(CPrintf(L"%u", Rec.m_nAccess));

		Comment.Append(Const(Rec.m_Comment));
		}

	Tags.EmitLookup(NULL, L"Name", Index, Name, L"\"\"");

	Tags.EmitLookup(NULL, L"Comment", Index, Comment, L"\"\"");

	Tags.EmitLookup(NULL, L"Type", Index, Type, L"0");

	Tags.EmitLookup(NULL, L"Access", Index, Access, L"0");

	Tags.EndFolder();
	}

// Variables

BOOL CDataFile::CVariableTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CVariableRecord);
	}

void CDataFile::CVariableTable::FindDescText(CDescTextTable &Desc)
{
	StdFindDesc(CVariableRecord);
	}

void CDataFile::CVariableTable::EmitAnalog(CMakeTags &Tags)
{
	Tags.NewFolder(L"AnalogVariables");

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CVariableRecord &Rec = (CVariableRecord &) m_List[i];

		Rec.EmitTags(Tags, TRUE, i);
		}

	Tags.EndFolder();
	}

void CDataFile::CVariableTable::EmitDigital(CMakeTags &Tags)
{
	Tags.NewFolder(L"DigitalVariables");

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CVariableRecord &Rec = (CVariableRecord &) m_List[i];

		Rec.EmitTags(Tags, FALSE, i);
		}

	Tags.EndFolder();
	}

void CDataFile::CVariableTable::EmitLookup(CMakeTags &Tags)
{
	Tags.NewFolder(L"Variables");

	CUIntArray   Index, Analog, Digital;

	CStringArray Valid, Name, Desc, Data, Text, Type, Units, Text0, Text1;

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CVariableRecord &Rec = (CVariableRecord &) m_List[i];

		if( Rec.m_nTagType ) {

			Index.Append(1+i);

			Valid.Append(L"1");

			Name.Append(Const(Rec.m_Name));

			Desc.Append(Const(Rec.m_Desc));

			Data.Append(CPrintf(L"MakeFloat(%s)", AddrBP(BLKNUM_VARS, DYNAMIC, i)));

			Text.Append(CPrintf(L"%s.Out.AsText", Rec.m_Folder));

			Type.Append(CPrintf(L"%u", Rec.m_nTagType));
			}

		if( Rec.m_nTagType == 1 ) {

			Analog.Append(1+i);

			Units.Append(Const(Rec.m_Label0));
			}

		if( Rec.m_nTagType == 2 ) {

			Digital.Append(1+i);

			Text0.Append(Const(Rec.m_Label0));

			Text1.Append(Const(Rec.m_Label1));
			}
		}

	Tags.EmitSintC (NULL, L"Count",  Index.GetCount());

	Tags.EmitLookup(NULL, L"Index",  Index);

	Tags.EmitLookup(NULL, L"Valid",  Index,   Valid, L"0");

	Tags.EmitLookup(NULL, L"Name",   Index,   Name,  L"\"\"");

	Tags.EmitLookup(NULL, L"Desc",   Index,   Desc,  L"\"\"");

	Tags.EmitLookup(NULL, L"Data",   Index,   Data,  L"0.0");

	Tags.EmitLookup(NULL, L"AsText", Index,   Text,  L"\"\"");

	Tags.EmitLookup(NULL, L"Type",   Index,   Type,  L"0");

	Tags.EmitLookup(NULL, L"Units",  Analog,  Units, L"\"\"");

	Tags.EmitLookup(NULL, L"Text0",  Digital, Text0, L"\"\"");

	Tags.EmitLookup(NULL, L"Text1",  Digital, Text1, L"\"\"");

	Tags.EndFolder();
	}

// Calendar Event Tag

BOOL CDataFile::CCalEvtTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		m_Tag	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CCalEvtTagRecord::EmitTags(CMakeTags &Tags, CDataFile &DataFile)
{
	static int nCalEvtIndex = 0;

	CCalEvtDisplayRecord const &Rec = DataFile.m_CalEvtDisplayTable.m_List[nCalEvtIndex];

	Tags.NewFolder(L"CalendarEvent", m_Tag);

	// Root Folder

	Tags.EmitTextC(NULL, L"Description",      Rec.m_Title);

	Tags.EmitRealW(NULL, L"Setpoint Request", PropD(1), 0);

	for( int event = 0; event < 8; event++ ) {

		Tags.NewFolder(CPrintf(L"Event%d", event+1));

		Tags.EmitTextC(NULL, L"Event Label", Rec.m_EventLabel[event]);

		Tags.EmitEnumM(NULL, L"Event Type",          PropD( 2 + event), Hc900CalendarEventTypeEnum);
		Tags.EmitSintM(NULL, L"Hour Display",        PropD(10 + event), 0, 23);
		Tags.EmitSintM(NULL, L"Minute Display",      PropD(18 + event), 0, 59);
		Tags.EmitEnumM(NULL, L"Month Display",       PropD(26 + event), Hc900CalendarMonthEnum);
		Tags.EmitSintM(NULL, L"Day Display",         PropD(34 + event), 1, 31);
		Tags.EmitEnumM(NULL, L"Day Of Week Display", PropD(34 + event), Hc900CalendarDaysofWeekEnum);

		int nSignal = Rec.m_nTagIndex[event] - 1;

		if( nSignal >= 0 ) {

			CSignalRecord const &Sig = DataFile.m_SignalTable.m_List[nSignal];

			if( Sig.m_nTagType == 1 ) {

				// Analog Signal

				Tags.EmitTextC(NULL, L"Feedback Tag", Sig.m_Tag);

				Tags.EmitRealM( NULL,
						L"Feedback Value",
						AddrBP(Sig.m_nBlockNum, DYNAMIC, Sig.m_nOutIndex),
						Sig.m_nDecPlaces
						);

				Tags.EmitTextC(NULL, L"Feedback Units", Sig.m_Label0);
				}
			else {
				// Digital Signal

				Tags.EmitTextC(NULL, L"Feedback Tag", Sig.m_Tag);

				Tags.EmitEnumM( NULL,
						L"Feedback Value",
						AddrBP(Sig.m_nBlockNum, DYNAMIC, Sig.m_nOutIndex),
						Sig.m_Label0,
						Sig.m_Label1
						);

				Tags.EmitTextC(NULL, L"Feedback Units", L"");
				}
			}
		else {
			// No Signal

			Tags.EmitTextC(NULL, L"Feedback Tag",   L""  );
			Tags.EmitRealC(NULL, L"Feedback Value", 0,  1);
			Tags.EmitTextC(NULL, L"Feedback Units", L""  );
			}

		Tags.EndFolder();
		}

	// Special Day Folder

	Tags.NewFolder(L"SpecialDay");

	Tags.EmitEnumM(NULL, L"Special Day Mode",    PropS(200), Hc900DisabledOutputsAltTimeEnum);

	Tags.EmitEnumM(NULL, L"Special Day1 Output", PropS(201), Hc900NormalUseAlternateEnum);
	Tags.EmitEnumM(NULL, L"Special Day2 Output", PropS(202), Hc900NormalUseAlternateEnum);
	Tags.EmitEnumM(NULL, L"Special Day3 Output", PropS(203), Hc900NormalUseAlternateEnum);
	Tags.EmitEnumM(NULL, L"Special Day4 Output", PropS(204), Hc900NormalUseAlternateEnum);
	Tags.EmitEnumM(NULL, L"Special Day5 Output", PropS(205), Hc900NormalUseAlternateEnum);
	Tags.EmitEnumM(NULL, L"Special Day6 Output", PropS(206), Hc900NormalUseAlternateEnum);
	Tags.EmitEnumM(NULL, L"Special Day7 Output", PropS(207), Hc900NormalUseAlternateEnum);
	Tags.EmitEnumM(NULL, L"Special Day8 Output", PropS(208), Hc900NormalUseAlternateEnum);

	Tags.EmitSintM(NULL, L"Alternate Hour SD1", PropS(241),0 , 23);
	Tags.EmitSintM(NULL, L"Alternate Hour SD2", PropS(242),0 , 23);
	Tags.EmitSintM(NULL, L"Alternate Hour SD3", PropS(243),0 , 23);
	Tags.EmitSintM(NULL, L"Alternate Hour SD4", PropS(244),0 , 23);
	Tags.EmitSintM(NULL, L"Alternate Hour SD5", PropS(245),0 , 23);
	Tags.EmitSintM(NULL, L"Alternate Hour SD6", PropS(246),0 , 23);
	Tags.EmitSintM(NULL, L"Alternate Hour SD7", PropS(247),0 , 23);
	Tags.EmitSintM(NULL, L"Alternate Hour SD8", PropS(248),0 , 23);

	Tags.EmitSintM(NULL, L"Alternate Minute SD1", PropS(249), 0 , 59);
	Tags.EmitSintM(NULL, L"Alternate Minute SD2", PropS(250), 0 , 59);
	Tags.EmitSintM(NULL, L"Alternate Minute SD3", PropS(251), 0 , 59);
	Tags.EmitSintM(NULL, L"Alternate Minute SD4", PropS(252), 0 , 59);
	Tags.EmitSintM(NULL, L"Alternate Minute SD5", PropS(253), 0 , 59);
	Tags.EmitSintM(NULL, L"Alternate Minute SD6", PropS(254), 0 , 59);
	Tags.EmitSintM(NULL, L"Alternate Minute SD7", PropS(255), 0 , 59);
	Tags.EmitSintM(NULL, L"Alternate Minute SD8", PropS(256), 0 , 59);

	for( int SpecialDay = 0; SpecialDay < 16; SpecialDay++ )  {

		Tags.NewFolder(CPrintf(L"SpecialDay%d", SpecialDay+1));

		Tags.EmitTextC(NULL, L"Special Day Label", Rec.m_SpecDayLabel[SpecialDay]);

		Tags.EmitEnumW(NULL, L"Month Display", PropS(225 + SpecialDay), Hc900CalendarMonthEnum);

		Tags.EmitSintW(NULL, L"Day Display",   PropS(209 + SpecialDay), 1, 31);

		Tags.EndFolder();
		}

	Tags.EndFolder();

	// Back to Root

	for( int EventSetpoint = 0; EventSetpoint < 5; EventSetpoint++ )  {

		Tags.NewFolder(CPrintf(L"EventSetpoint%d", EventSetpoint+1));

		Tags.EmitTextC(NULL, L"Event Setpoint Label", Rec.m_EventTitle[EventSetpoint]);

		for( int event = 0; event < 8; event++ ) {

			Tags.NewFolder(CPrintf(L"Event%d", event+1));

			int eventinSP = event+ (EventSetpoint*8);

			Tags.EmitTextC(NULL, L"Event Label", Rec.m_EventLabel[event]);

			Tags.EmitEnumM(NULL, L"Event Type",          PropS(  0 + eventinSP), Hc900CalendarEventTypeEnum);
			Tags.EmitSintW(NULL, L"Hour Display",        PropS( 40 + eventinSP), 0, 23);
			Tags.EmitSintW(NULL, L"Minute Display",      PropS( 80 + eventinSP), 0, 59);
			Tags.EmitEnumW(NULL, L"Month Display",       PropS(120 + eventinSP), Hc900CalendarMonthEnum);
			Tags.EmitSintW(NULL, L"Day Display",         PropS(160 + eventinSP), 1, 31);
			Tags.EmitEnumW(NULL, L"Day Of Week Display", PropS(160 + eventinSP), Hc900CalendarSPDaysofWeekEnum);

			Tags.EndFolder();
			}

		Tags.EndFolder();
		}

	Tags.EndFolder();

	nCalEvtIndex++;
	}

// Calendar Event Tags

BOOL CDataFile::CCalEvtTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CCalEvtTagRecord);
	}

void CDataFile::CCalEvtTagTable::EmitTags(CMakeTags &Tags, CDataFile &DataFile)
{
	Tags.NewFolder(L"CalendarEvent");

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CCalEvtTagRecord &Rec = (CCalEvtTagRecord &) m_List[i];

		Rec.EmitTags(Tags, DataFile);
		}

	Tags.EndFolder();
	}

// PPO Tag

BOOL CDataFile::CPpoTagRecord::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		m_Tag	    = pParse->ReadString(8);

		pParse->Skip(8);

		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	if( Header.m_nRevNum == 2 ) {

		m_Tag	    = pParse->ReadString(16);
		m_Desc	    = pParse->ReadString(16);
		m_nBlockNum = pParse->ReadBigWord();

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CPpoTagRecord::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"PositionProportionalOutput", m_Tag);

	Tags.EmitTextC(NULL, L"Description", m_Desc);

	Tags.EmitSintM(NULL, L"FWD Motor DO Status",    PropD( 2)  );
	Tags.EmitSintM(NULL, L"REV Motor DO Status",    PropD( 3)  );
	Tags.EmitRealM(NULL, L"Position Feedback Calc", PropD( 6), 1);
	Tags.EmitEnumM(NULL, L"Motor Failure",          PropD( 7), Hc900OffOnEnum);
	Tags.EmitEnumM(NULL, L"Feedback Input Failed",  PropD( 8), Hc900OffOnEnum);
	Tags.EmitEnumW(NULL, L"Input Type",             PropS( 3), Hc900PpoInputTypeEnum);
	Tags.EmitRealM(NULL, L"Position Setpoint",      PropD( 9), 1);
	Tags.EmitRealW(NULL, L"Position Setpoint High", PropS( 4), 1, -99999, 99999);
	Tags.EmitRealW(NULL, L"Position Setpoint Low",  PropS( 5), 1, -99999, 99999);
	Tags.EmitRealW(NULL, L"DeadBand",               PropS( 8), 6, -5, 5);
	Tags.EmitRealW(NULL, L"Filter Time",            PropS(10), 0, 0, 3);
	Tags.EmitRealW(NULL, L"Position High Limit",    PropS(20), 4, 0, 100);
	Tags.EmitRealW(NULL, L"Position Low Limit",     PropS(21), 4, 0, 100);
	Tags.EmitRealW(NULL, L"Travel Time",            PropS( 9), 3, 12.0, 300.0);

	Tags.EndFolder();
	}

// PPO Tags

BOOL CDataFile::CPpoTagTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CPpoTagRecord);
	}

void CDataFile::CPpoTagTable::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"PositionProportionalOutput");

	UINT c = m_List.GetCount();

	UINT e = max(c, 1);

	Tags.EmitRealA( NULL,
			L"Dead Band",
			SpecPPO(200), // 8
			6,
			-5,
			+5,
			e,
			FALSE
			);

	Tags.EmitRealA( NULL,
			L"Travel Time",
			SpecPPO(400), // 9
			3,
			12,
			300,
			e,
			FALSE
			);

	Tags.EmitRealA( NULL,
			L"Position High Limit",
			SpecPPO(600), // 20
			4,
			0,
			100,
			e,
			FALSE
			);

	Tags.EmitRealA( NULL,
			L"Position Low Limit",
			SpecPPO(800), // 21
			4,
			0,
			100,
			e,
			FALSE
			);

	CInitData &Init = Tags.GetInitData();

	CString Enum;

	for( UINT i = 0; i < c; i++ ) {

		CPpoTagRecord &Rec = (CPpoTagRecord &) m_List[i];

		Init.AddWord(WORD(Rec.m_nBlockNum));

		if( !Enum.IsEmpty() ) {

			Enum += L'|';
			}

		Enum += Rec.m_Tag;

		Rec.EmitTags(Tags);
		}

	Init.AddWord(0xFFFF);

	Tags.EmitEnumW(NULL, L"PPO Select", L"", Enum);

	Tags.EmitSintA(NULL, L"PPO Blocks", SpecPPOBlock(0), e, FALSE);

	Tags.EndFolder();
	}

// Peer Table

BOOL CDataFile::CPeerTable::Read(CParser *pParse, CTableHeader &Header)
{
	if( Header.m_nRevNum == 1 ) {

		for( int i = 0; i < Header.m_nRecordCount; i++ ) {

			int nBlock = pParse->ReadBigWord();

			m_List.Append(nBlock);
			}

		return TRUE;
		}

	return FALSE;
	}

void CDataFile::CPeerTable::EmitTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"Peers");

	int count = m_List.GetCount();

	if( TRUE ) {

		CInitData &Init = Tags.GetInitData();

		for( int peer = 0; peer < count; peer++ ) {

			Init.AddWord(WORD(m_List[peer]));
			}

		Init.AddWord(0xFFFF);

		Tags.EmitTextA( NULL,
				L"Peer Name",
				count ? SpecPeerText(0) : L"",
				16,
				count ? count : 1,
				FALSE
				);

		Tags.EmitInetA( NULL,
				L"Peer Address",
				count ? SpecPeerData(100) : L"",
				count ? count : 1,
				FALSE
				);

		Tags.EmitSintA( NULL,
				L"Scan Time",
				count ? SpecPeerData(200) : L"",
				count ? count : 1,
				FALSE
				);

		Tags.EmitEnumA( NULL,
				L"Peer Status",
				count ? SpecPeerData(300) : L"",
				Hc900PeerStatusEnum,
				count ? count : 1,
				FALSE
				);

		Tags.EmitWintA( NULL,
				L"Messages Received",
				count ? SpecPeerData(400) : L"",
				count ? count : 1
				);

		Tags.EmitWintA( NULL,
				L"Messages Sent",
				count ? SpecPeerData(500) : L"",
				count ? count : 1
				);

		Tags.EmitWintA( NULL,
				L"Write Events",
				count ? SpecPeerData(600) : L"",
				count ? count : 1
				);

		Tags.EmitWintA( NULL,
				L"Write Fail",
				count ? SpecPeerData(700) : L"",
				count ? count : 1
				);

		Tags.EmitWintA( NULL,
				L"Prod Fail",
				count ? SpecPeerData(800) : L"",
				count ? count : 1
				);

		Tags.EmitEnumA( NULL,
				L"Clear Stats",
				count ? SpecPeerData(900) : L"",
				Hc900NormalClearEnum,
				count ? count : 1,
				TRUE
				);

		Tags.EmitEnumA( NULL,
				L"No Scan",
				count ? SpecPeerData(2200) : L"",
				Hc900OffOnEnum,
				count ? count : 1,
				FALSE
				);

		Tags.EmitEnumA( NULL,
				L"No Comm",
				count ? SpecPeerData(2300) : L"",
				Hc900OffOnEnum,
				count ? count : 1,
				FALSE
				);
		}
	else {
		for( int peer = 0; peer < count; peer++ ) {

			Tags.NewFolder(CPrintf(L"Peer%d", 1+peer));

			int m_nBlockNum = m_List[peer]; // NOTE -- Hack to fool PropD andPropS!

			Tags.EmitInetM(NULL, L"Peer Address",	   PropD( 1)  );
			Tags.EmitSintM(NULL, L"Scan Time",	   PropD( 2)  );
			Tags.EmitEnumM(NULL, L"Peer Status",	   PropD( 3), Hc900PeerStatusEnum);
			Tags.EmitWintM(NULL, L"Messages Received", PropD( 4)  );
			Tags.EmitWintM(NULL, L"Messages Sent",	   PropD( 5)  );
			Tags.EmitWintM(NULL, L"Write Events",	   PropD( 6)  );
			Tags.EmitWintM(NULL, L"Write Fail",	   PropD( 7)  );
			Tags.EmitWintM(NULL, L"Prod Fail",	   PropD( 8)  );
			Tags.EmitEnumW(NULL, L"Clear Stats",	   PropD( 9), Hc900NormalClearEnum);
			Tags.EmitEnumM(NULL, L"No Scan",	   PropD(22), Hc900OffOnEnum);
			Tags.EmitEnumM(NULL, L"No Comm",	   PropD(23), Hc900OffOnEnum);
			Tags.EmitTextM(NULL, L"Peer Name",	   PropS( 0), 16);

			Tags.EndFolder();
			}
		}

	Tags.EndFolder();
	}

// Analog Input

BOOL CDataFile::CAnalogInput::Read(CParser *pParse, CTableHeader &Header)
{
	m_bRack = pParse->ReadByte();
	m_bSlot = pParse->ReadByte();
	m_bChan = pParse->ReadByte();

	pParse->Skip(1);

	m_Units     = pParse->ReadString(4);
	m_nSignal   = pParse->ReadBigWord();
	m_nBlockNum = pParse->ReadBigWord();
	m_nValue    = pParse->ReadBigWord();
	m_nStatus   = pParse->ReadBigWord();

	return TRUE;
	}

// Analog Inputs

BOOL CDataFile::CAnalogInputTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CAnalogInput);
	}

void CDataFile::CAnalogInputTable::EmitTags(CMakeTags &Tags, CDataFile &File)
{
	CUIntArray   Index;

	CStringArray Valid, Addr, Units, Signal, Value, Status;

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CAnalogInput &Rec = (CAnalogInput &) m_List[i];

		Index. Append(i);

		Valid. Append(L"1");

		Addr.  Append(AddrIO(Rec.m_bRack, Rec.m_bSlot, Rec.m_bChan));

		Units. Append(Const(Rec.m_Units));

		Signal.Append(CPrintf(L"%u", Rec.m_nSignal));

		Value. Append(CPrintf(L"MakeFloat(%s)", AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nValue )));

		Status.Append(AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nStatus));
		}

	if( TRUE ) {

		Tags.NewFolder(L"AnalogInputs");

		Tags.EmitSintC (NULL, L"Count",   Index.GetCount());

		Tags.EmitLookup(NULL, L"Index",   Index);

		Tags.EmitLookup(NULL, L"Valid",   Index, Valid,  L"0");

		Tags.EmitLookup(NULL, L"Address", Index, Addr,   L"\"\"");

		Tags.EmitLookup(NULL, L"Signal",  Index, Signal, L"0");

		Tags.EmitLookup(NULL, L"Value",   Index, Value,  L"0.0");

		Tags.EmitLookup(NULL, L"Status",  Index, Status, L"0");

		Tags.EndFolder();
		}
	}

// Analog Output

BOOL CDataFile::CAnalogOutput::Read(CParser *pParse, CTableHeader &Header)
{
	m_bRack = pParse->ReadByte();
	m_bSlot = pParse->ReadByte();
	m_bChan = pParse->ReadByte();

	pParse->Skip(1);

	m_Units     = pParse->ReadString(4);
	m_nSignal   = pParse->ReadBigWord();
	m_nBlockNum = pParse->ReadBigWord();
	m_nValue    = pParse->ReadBigWord();
	m_nStatus   = pParse->ReadBigWord();

	return TRUE;
	}

// Analog Outputs

BOOL CDataFile::CAnalogOutputTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CAnalogOutput);
	}

void CDataFile::CAnalogOutputTable::EmitTags(CMakeTags &Tags, CDataFile &File)
{
	CUIntArray   Index, IndexV;

	CStringArray Valid, Addr, Units, Signal, Value, Status;

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CAnalogOutput &Rec = (CAnalogOutput &) m_List[i];

		Index.Append(i);

		Valid. Append(L"1");

		Addr.  Append(AddrIO(Rec.m_bRack, Rec.m_bSlot, Rec.m_bChan));

		Units. Append(Const(Rec.m_Units));

		Signal.Append(CPrintf(L"%u", Rec.m_nSignal));

		Value .Append(CPrintf(L"MakeFloat(%s)", AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nValue )));

		Status.Append(AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nStatus));
		}

	if( TRUE ) {

		Tags.NewFolder(L"AnalogOutputs");

		Tags.EmitSintC (NULL, L"Count",   Index.GetCount());

		Tags.EmitLookup(NULL, L"Index",   Index);

		Tags.EmitLookup(NULL, L"Valid",   Index, Valid,  L"0");

		Tags.EmitLookup(NULL, L"Address", Index, Addr,   L"\"\"");

		Tags.EmitLookup(NULL, L"Signal",  Index, Signal, L"0");

		Tags.EmitLookup(NULL, L"Value",   Index, Value,  L"0.0");

		Tags.EmitLookup(NULL, L"Status",  Index, Status, L"0");

		Tags.EndFolder();
		}
	}

// Digital Input

BOOL CDataFile::CDigitalInput::Read(CParser *pParse, CTableHeader &Header)
{
	m_bRack = pParse->ReadByte();
	m_bSlot = pParse->ReadByte();
	m_bChan = pParse->ReadByte();

	pParse->Skip(1);

	m_Text0 = pParse->ReadString(6);
	m_Text1 = pParse->ReadString(6);

	m_nSignal   = pParse->ReadBigWord();
	m_nBlockNum = pParse->ReadBigWord();
	m_nValue    = pParse->ReadBigWord();
	m_nStatus   = pParse->ReadBigWord();

	return TRUE;
	}

// Digital Inputs

BOOL CDataFile::CDigitalInputTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CDigitalInput);
	}

void CDataFile::CDigitalInputTable::EmitTags(CMakeTags &Tags, CDataFile &File)
{
	CUIntArray   Index;

	CStringArray Valid, Addr, Text0, Text1, Signal, Value, Status;

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CDigitalInput &Rec = (CDigitalInput &) m_List[i];

		Index. Append(i);

		Valid. Append(L"1");

		Addr.  Append(AddrIO(Rec.m_bRack, Rec.m_bSlot, Rec.m_bChan));

		Text0. Append(Const(Rec.m_Text0));

		Text1. Append(Const(Rec.m_Text1));

		Signal.Append(CPrintf(L"%u", Rec.m_nSignal));

		Value. Append(CPrintf(L"MakeFloat(%s)", AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nValue )));

		Status.Append(AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nStatus));
		}

	if( TRUE ) {

		Tags.NewFolder(L"DigitalInputs");

		Tags.EmitSintC (NULL, L"Count",   Index.GetCount());

		Tags.EmitLookup(NULL, L"Index",   Index);

		Tags.EmitLookup(NULL, L"Valid",   Index, Valid,  L"0");

		Tags.EmitLookup(NULL, L"Address", Index, Addr,   L"\"\"");

		Tags.EmitLookup(NULL, L"Text0",   Index, Text0,  L"\"\"");

		Tags.EmitLookup(NULL, L"Text1",   Index, Text1,  L"\"\"");

		Tags.EmitLookup(NULL, L"Signal",  Index, Signal, L"0");

		Tags.EmitLookup(NULL, L"Value",   Index, Value,  L"0.0");

		Tags.EmitLookup(NULL, L"Status",  Index, Status, L"0");

		Tags.EndFolder();
		}
	}

// Digital Output

BOOL CDataFile::CDigitalOutput::Read(CParser *pParse, CTableHeader &Header)
{
	m_bRack = pParse->ReadByte();
	m_bSlot = pParse->ReadByte();
	m_bChan = pParse->ReadByte();
	
	m_bType = pParse->ReadByte();

	m_Text0 = pParse->ReadString(6);
	m_Text1 = pParse->ReadString(6);

	m_nSignal   = pParse->ReadBigWord();
	m_nBlockNum = pParse->ReadBigWord();
	m_nValue    = pParse->ReadBigWord();
	m_nStatus   = pParse->ReadBigWord();

	return TRUE;
	}

// Digital Outputs

BOOL CDataFile::CDigitalOutputTable::Read(CParser *pParse, CTableHeader &Header)
{
	StdRead(CDigitalOutput);
	}

void CDataFile::CDigitalOutputTable::EmitTags(CMakeTags &Tags, CDataFile &File)
{
	CUIntArray   Index;

	CStringArray Valid, Addr, Type, Text0, Text1, Signal, Value, Status;

	for( UINT i = 0; i < m_List.GetCount(); i++ ) {

		CDigitalOutput &Rec = (CDigitalOutput &) m_List[i];

		Index. Append(i);

		Valid. Append(L"1");

		Addr.  Append(AddrIO(Rec.m_bRack, Rec.m_bSlot, Rec.m_bChan));

		Type.  Append(CPrintf(L"%u", Rec.m_bType));

		Text0. Append(Const(Rec.m_Text0));

		Text1. Append(Const(Rec.m_Text1));

		Signal.Append(CPrintf(L"%u", Rec.m_nSignal));

		Value. Append(CPrintf(L"MakeFloat(%s)", AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nValue )));

		Status.Append(AddrBP(Rec.m_nBlockNum, DYNAMIC, Rec.m_nStatus));
		}

	if( TRUE ) {

		Tags.NewFolder(L"DigitalOutputs");

		Tags.EmitSintC (NULL, L"Count",   Index.GetCount());

		Tags.EmitLookup(NULL, L"Index",   Index);

		Tags.EmitLookup(NULL, L"Valid",   Index, Valid,  L"0");

		Tags.EmitLookup(NULL, L"Type",    Index, Type,   L"0");

		Tags.EmitLookup(NULL, L"Address", Index, Addr,   L"\"\"");

		Tags.EmitLookup(NULL, L"Text0",   Index, Text0,  L"\"\"");

		Tags.EmitLookup(NULL, L"Text1",   Index, Text1,  L"\"\"");

		Tags.EmitLookup(NULL, L"Signal",  Index, Signal, L"0");

		Tags.EmitLookup(NULL, L"Value",   Index, Value,  L"0.0");

		Tags.EmitLookup(NULL, L"Status",  Index, Status, L"0");

		Tags.EndFolder();
		}
	}

// Implementation

void CDataFile::FindDescText(void)
{
	m_SignalTable.FindDescText(m_DescTextTable);

	m_VariableTable.FindDescText(m_DescTextTable);
	}

void CDataFile::EmitTags(CMakeTags &Tags)
{
	if( TRUE ) {

		Tags.NewFolder(L"Blocks");

		m_GenericTagTable.EmitTags(Tags, 118 /* AGA8 Detail */, FALSE, *this);

		m_GenericTagTable.EmitTags(Tags, 119 /* AGA8 Gross */, TRUE /* end Aga folder */, *this);

		m_AlternatorTagTable.EmitTags(Tags);

		m_CalEvtTagTable.EmitTags(Tags, *this );

		m_DevCtlTagTable.EmitTags(Tags);

		m_GenericTagTable.EmitTags(Tags, 76 /* Four Selector Switch */, TRUE /* end FSS folder */, *this);

		m_FssDisplayTable.EmitTags(Tags);

		m_HoaTagTable.EmitTags(Tags);

		m_LoopTagTable.EmitTags(Tags, *this);

		m_GenericTagTable.EmitTags(Tags, 42 /* Pushbutton */, TRUE /* end Pushbutton folder */, *this);

		m_PushbuttonDisplayTable.EmitTags(Tags);

		m_RampTagTable.EmitTags(Tags);

		m_DrumSeqTagTable.EmitTags(Tags);

		m_SppTagTable.EmitTags(Tags, *this);

		m_SpsTagTable.EmitTags(Tags, m_SpsDisplayTable);

		m_StageTagTable.EmitTags(Tags);

		m_GenericTagTable.EmitTags(Tags, 129 /* XYR5000 Base */, FALSE, *this);

		m_GenericTagTable.EmitTags(Tags, 130 /* XYR5000 Transmitter */, FALSE, *this);

		m_GenericTagTable.EmitTags(Tags, 134 /* XYR6000 Transmitter */, TRUE /* end Wireless folder */, *this);

		m_PeerTable.EmitTags(Tags);

		m_PpoTagTable.EmitTags(Tags);

		Tags.EndFolder();
		}

	if( TRUE ) {

		Tags.NewFolder(L"IO");		

		m_AnalogInputTable.EmitTags(Tags, *this);

		m_AnalogOutputTable.EmitTags(Tags, *this);

		m_DigitalInputTable.EmitTags(Tags, *this);

		m_DigitalOutputTable.EmitTags(Tags, *this);						

		Tags.EndFolder();
		}

	m_EventDisplayTable.EmitTags(Tags, m_SignalTable);

	m_AlarmGroupDisplayTable.EmitTags(Tags, m_SignalTable, m_AlarmHelpTextTable);

	m_SignalTable.EmitAnalog(Tags);

	m_SignalTable.EmitDigital(Tags);

	m_SignalTable.EmitLookup(Tags);

	m_WorksheetTable.EmitTags(Tags);

	m_VariableTable.EmitAnalog(Tags);

	m_VariableTable.EmitDigital(Tags);

	m_VariableTable.EmitLookup(Tags);

	EmitSystemTags(Tags);

	EmitDriverTags(Tags);

//	m_CalEvtDisplayTable.EmitTags(Tags);

//	m_ScreenButtonDspTable.EmitTags(Tags);
	}

void CDataFile::EmitDriverTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"Driver");

	PCTXT pEnum = L"OFFLINE|WAITING|CHECKING|MISMATCH|ONLINE|RECIPES|NODEV|UPLOAD|DOWNLOAD|RESTART";

	Tags.EmitEnumM(NULL, L"Driver State",		 SpecSystem(1), pEnum);

	Tags.EmitSintM(NULL, L"Heartbeat",		 SpecSystem(2));

	Tags.EmitSintM(NULL, L"HMI Number",		 SpecSystem(3));
	
	Tags.EmitSintW(NULL, L"Result",			 SpecSystem(4));

	Tags.EmitSintW(NULL, L"Progress",		 SpecSystem(5));
	
	Tags.EmitSintW(NULL, L"Wait Time",		 SpecSystem(6));
	
	Tags.EmitSintW(NULL, L"Phase",			 SpecSystem(7));
	
	Tags.EmitSintW(NULL, L"Table",			 SpecSystem(8));

	Tags.EmitSintW(NULL, L"Alarms",			 SpecSystem(9));

	Tags.EmitEnumM(NULL, L"Mismatch Mode",		 SpecSystem(10), Hc900ControllerModeEnum);

	Tags.EmitSintM(NULL, L"Safety Write Fail",       SpecSystem(11));

	Tags.EmitSintM(NULL, L"Safety Mode Change Fail", SpecSystem(12));

	Tags.EmitSintM(NULL, L"Safety Alarm Ack Fail",   SpecSystem(13));

	Tags.EndFolder();
	}

void CDataFile::EmitSystemTags(CMakeTags &Tags)
{
	Tags.NewFolder(L"System");

	if( TRUE ) {

		Tags.NewFolder(L"Controller");

		EmitControllerTags(Tags);

		Tags.EndFolder();
		}

	if( TRUE ) {

		Tags.NewFolder(L"Comm");

		Tags.NewFolder(L"Serial_S1");

		EmitSerialTags(Tags, BLKNUM_SERIAL_S1);

		Tags.EndFolder();

		Tags.NewFolder(L"Serial_S2");

		EmitSerialTags(Tags, BLKNUM_SERIAL_S2);

		Tags.EndFolder();

		Tags.NewFolder(L"Ethernet_E1");

		EmitEthernetTags(Tags, BLKNUM_NTWK_E1);

		Tags.EndFolder();

		if( m_pDeviceCaps->HasFeature(FEATURE_TWO_ETHERNET_PORTS) ) {

			Tags.NewFolder(L"Ethernet_E2");

			EmitEthernetTags(Tags, BLKNUM_NTWK_E2);

			Tags.EndFolder();
			}
		else {
			Tags.NewFolder(L"Ethernet_E2");

			EmitEthernetTags(Tags, -1);

			Tags.EndFolder();
			}

		Tags.NewFolder(L"ModbusSlaves");

		int count = m_pDeviceCaps->GetValue(DEVCAPINT_MAX_SERIAL_MODBUS_SLAVES);

		if( TRUE ) {

			Tags.EmitTextA( NULL,
					L"Slave Name",
					SpecModSer(200),
					16,
					count,
					FALSE
					);

			Tags.EmitSintA( NULL,
					L"Slave Address",
					SpecModSer(1000),
					count,
					FALSE
					);

			Tags.EmitEnumA( NULL,
					L"In Scan",
					SpecModSer(1800),
					Hc900NoYesEnum,
					count,
					FALSE
					);

			Tags.EmitFnumA( NULL,
					L"Scan Enable",
					SpecModSer(1600),
					Hc900NoYesEnum,
					count,
					TRUE
					);

			Tags.EmitEnumA( NULL,
					L"Comm Quality",
					SpecModSer(1400),
					Hc900SlaveCommQualityEnum,
					count,
					FALSE
					);

			Tags.EmitWintA( NULL,
					L"Received Count",
					SpecModSer(2000),
					count
					);

			Tags.EmitWintA( NULL,
					L"Data Link Errors",
					SpecModSer(2200),
					count
					);

			Tags.EmitWintA( NULL,
					L"Application Errors",
					SpecModSer(2400),
					count
					);

			Tags.EmitEnumA( NULL,
					L"Enable Pin Connected",
					SpecModSer(2600),
					Hc900NoYesEnum,
					count,
					FALSE
					);
			}
		else {
			for( int slave = 0; slave < count; slave++ ) {

				Tags.NewFolder(CPrintf(L"Slave%u", 1+slave));

				EmitModbusSlavesTags(Tags, slave, 2 /* block version */);

				Tags.EndFolder();
				}
			}

		Tags.EndFolder();

		if( m_pDeviceCaps->HasFeature(FEATURE_MODBUS_TCP_SLAVES) ) {

			Tags.NewFolder(L"ModbusTCPSlaves");

			int count = 32;

			if( TRUE ) {

				Tags.EmitTextA( NULL,
						L"Slave Name",
						SpecModTCP(200),
						16,
						count,
						FALSE
						);

				Tags.EmitInetA( NULL,
						L"Slave Address",
						SpecModTCP(1000),
						count,
						FALSE
						);

				Tags.EmitEnumA( NULL,
						L"In Scan",
						SpecModTCP(1800),
						Hc900NoYesEnum,
						count,
						FALSE
						);

				Tags.EmitFnumA( NULL,
						L"Scan Enable",
						SpecModTCP(1600),
						Hc900NoYesEnum,
						count,
						TRUE
						);

				Tags.EmitEnumA( NULL,
						L"Comm Quality",
						SpecModTCP(1400),
						Hc900SlaveCommQualityEnum,
						count,
						FALSE
						);

				Tags.EmitWintA( NULL,
						L"Received Count",
						SpecModTCP(2000),
						count
						);

				Tags.EmitWintA( NULL,
						L"Data Link Errors",
						SpecModTCP(2200),
						count
						);

				Tags.EmitWintA( NULL,
						L"Application Errors",
						SpecModTCP(2400),
						count
						);

				Tags.EmitEnumA( NULL,
						L"Enable Pin Connected",
						SpecModTCP(2600),
						Hc900NoYesEnum,
						count,
						FALSE
						);
				}
			else {
				for( int slave = 0; slave < count; slave++ ) {

					Tags.NewFolder(CPrintf(L"Slave%u", 1+slave));

					EmitModbusTcpSlavesTags(Tags, slave);

					Tags.EndFolder();
					}
				}

			Tags.EndFolder();
			}
		else {
			Tags.NewFolder(L"ModbusTCPSlaves");

			int count = 32;

			Tags.EmitTextA( NULL,
					L"Slave Name",
					L"",
					16,
					count,
					FALSE
					);

			Tags.EmitInetA( NULL,
					L"Slave Address",
					L"",
					count,
					FALSE
					);

			Tags.EmitEnumA( NULL,
					L"In Scan",
					L"",
					Hc900NoYesEnum,
					count,
					FALSE
					);

			Tags.EmitFnumA( NULL,
					L"Scan Enable",
					L"",
					Hc900NoYesEnum,
					count,
					TRUE
					);

			Tags.EmitEnumA( NULL,
					L"Comm Quality",
					L"",
					Hc900SlaveCommQualityEnum,
					count,

					FALSE
					);

			Tags.EmitWintA( NULL,
					L"Received Count",
					L"",
					count
					);

			Tags.EmitWintA( NULL,
					L"Data Link Errors",
					L"",
					count
					);

			Tags.EmitWintA( NULL,
					L"Application Errors",
					L"",
					count
					);

			Tags.EmitEnumA( NULL,
					L"Enable Pin Connected",
					L"",
					Hc900NoYesEnum,
					count,
					FALSE
					);

			Tags.EndFolder();
			}

		Tags.NewFolder(L"HostConnections");

		int nMaxHosts = 5;

		if( m_pDeviceCaps->HasFeature(FEATURE_TEN_HOST_CONNECTIONS) ) {

			nMaxHosts = 10;
			}

		if( TRUE ) {

			Tags.EmitEnumA( NULL,
					L"Port Diagnostic",
					SpecHost(400),
					Hc900PortDiagnosticEnum,
					nMaxHosts,
					FALSE
					);

			Tags.EmitEnumA( NULL,
					L"Protocol",
					SpecHost(0),
					Hc900EthernetPortProtocolEnum,
					nMaxHosts,
					FALSE
					);

			Tags.EmitInetA( NULL,
					L"IP Address",
					SpecHost(200),
					nMaxHosts,
					FALSE
					);

			Tags.EmitWintA( NULL,
					L"Received Count",
					SpecHost(500),
					nMaxHosts
					);

			Tags.EmitWintA( NULL,
					L"App Error Count",
					SpecHost(700),
					nMaxHosts
					);

			Tags.EmitEnumA( NULL,
					L"Clear",
					SpecHost(800),
					Hc900NormalClearEnum,
					nMaxHosts,
					TRUE
					);
			}
		else {
			for( int host = 0; host < nMaxHosts; host++ ) {

				Tags.NewFolder(CPrintf(L"Host%u", 1+host));

				EmitHostConnectionsTags(Tags, host);

				Tags.EndFolder();
				}
			}

		Tags.EndFolder();
		}

	Tags.EndFolder();

	if( TRUE ) {

		Tags.NewFolder(L"IO_Racks");

		int count = m_pDeviceCaps->GetValue(DEVCAPINT_MAX_RACKS);

		for( int rack = 0; rack < 12; rack++ ) {

			Tags.NewFolder(CPrintf(L"Rack%u", 1+rack));

			EmitIORacksTags(Tags, rack, rack < count);

			Tags.EndFolder();
			}

		Tags.EndFolder();
		}

	if( TRUE ) {

		BOOL fReal = m_pDeviceCaps->HasFeature(FEATURE_REDUNDANCY);

		Tags.NewFolder(L"Redundant");

		if( TRUE ) {

			Tags.NewFolder(L"Overview");

			EmitRedundantOverviewTags(Tags, fReal);

			Tags.EndFolder();

			Tags.NewFolder(L"LeadCPU");

			EmitRedundantLeadCPUTags(Tags, fReal);

			Tags.EndFolder();

			Tags.NewFolder(L"ReserveCPU");

			EmitRedundantReserveCPUTags(Tags, fReal);

			Tags.EndFolder();
			}

		Tags.EndFolder();
		}

	if( TRUE ) {

		Tags.NewFolder(L"Recipes");

		////////

		Tags.EmitSintC( NULL,
				L"Rcp Count",
				m_RecipeAllocTable.m_uCount[0]
				);

		Tags.EmitTextA( NULL,
				L"Rcp Text",
				SpecRecipeRcp(0),
				24,
				max(m_RecipeAllocTable.m_uCount[0], 1),
				FALSE
				);

		Tags.EmitSintW( NULL,
				L"Rcp Read",
				SpecRecipeRcp(30000)
				);

		////////

		Tags.EmitSintC( NULL,
				L"SPP Count",
				m_RecipeAllocTable.m_uCount[1]
				);

		Tags.EmitTextA( NULL,
				L"SPP Text",
				SpecRecipeSpp(0),
				24,
				max(m_RecipeAllocTable.m_uCount[1], 1),
				FALSE
				);

		Tags.EmitSintW( NULL,
				L"SPP Read",
				SpecRecipeSpp(30000)
				);

		////////

		Tags.EmitSintC( NULL,
				L"SPS Count",
				m_RecipeAllocTable.m_uCount[2]
				);

		Tags.EmitTextA( NULL,
				L"SPS Text",
				SpecRecipeSps(0),
				24,
				max(m_RecipeAllocTable.m_uCount[2], 1),
				FALSE
				);

		Tags.EmitSintW( NULL,
				L"SPS Read",
				SpecRecipeSps(30000)
				);

		////////

		Tags.EmitSintC( NULL,
				L"Seq Count",
				m_RecipeAllocTable.m_uCount[3]
				);

		Tags.EmitTextA( NULL,
				L"Seq Text",
				SpecRecipeSeq(0),
				24,
				max(m_RecipeAllocTable.m_uCount[3], 1),
				FALSE
				);

		Tags.EmitSintW( NULL,
				L"Seq Read",
				SpecRecipeSeq(30000)
				);

		////////

		Tags.NewFolder(L"Work");

		Tags.EmitSintW( NULL,
				L"RcpLoad",
				AddrBP(BLKNUM_ASYS, DYNAMIC, 73)
				);

		m_DrumSeqTagTable.EmitWork(Tags);

		m_SppTagTable.EmitWork(Tags);
		
		m_SpsTagTable.EmitWork(Tags);

		Tags.NewFolder(L"Rcp");

		Tags.EmitSintW(NULL, L"ReqWrite", SpecSystem(16));
	
		Tags.EmitSintW(NULL, L"ReqRead",  SpecSystem(17));

		int m_nBlockNum = 32003;

		Tags.EmitTextW( NULL,
				L"Recipe Name",
				PropD(100),
				8
				);

		Tags.EmitTextW( NULL,
				L"Recipe Description",
				PropD(102),
				16
				);

		Tags.EmitSintA( NULL,
				L"Variable",
				PropD(0),
				50,
				0,
				32767,
				TRUE
				);

		Tags.EmitRealA( NULL,
				L"Value",
				PropD(50),
				2,
				50,
				TRUE
				);

		Tags.EndFolder();

		Tags.EndFolder();

		////////

		Tags.EndFolder();
		}

	Tags.EndFolder();
	}

void CDataFile::EmitControllerTags(CMakeTags &Tags)
{
	int nBlock = BLKNUM_RACK1;

	switch( m_FileHeader.m_nProductType ) {

		case DEVTYPE_HC970R: 
		case DEVTYPE_HC975:
		case DEVTYPE_HC975S:

			nBlock = BLKNUM_LEADCPU;

			break;
		}

	Tags.EmitEnumM(NULL, L"Controller Type",		AddrBP(BLKNUM_ASYS,  DYNAMIC, 100), Hc900DeviceTypeEnum);
	Tags.EmitTextM(NULL, L"Controller Name",		AddrBP(BLKNUM_ASYS,  DYNAMIC,  35), 16);
	Tags.EmitRealM(NULL, L"Cont Firmware Rev",		AddrBP(nBlock,       DYNAMIC,	1), 3);
	Tags.EmitRealM(NULL, L"Cycle Time",			AddrBP(BLKNUM_ASYS,  DYNAMIC,	8), 2);
	Tags.EmitRealM(NULL, L"CPU Pct Used",			AddrBP(BLKNUM_ASYS,  DYNAMIC,	5), 2);
	Tags.EmitRealM(NULL, L"CB Overruns",			AddrBP(BLKNUM_ASYS,  DYNAMIC,	6), 2);
	Tags.EmitTextM(NULL, L"Local Alias",			AddrBP(BLKNUM_ASYS,  DYNAMIC,  31), 16);
	Tags.EmitEnumM(NULL, L"Mode",				AddrBP(BLKNUM_ASYS,  DYNAMIC,  22), Hc900ControllerModeEnum);
	Tags.EmitEnumM(NULL, L"System Diagnostic",		AddrBP(BLKNUM_RACK1, DYNAMIC, 134), Hc900ControllerSystemDiagnosticEnum); // c50,c30,c70 only!
	Tags.EmitRealM(NULL, L"Peak Time",			AddrBP(BLKNUM_ASYS,  DYNAMIC,	4), 2);
	Tags.EmitRealM(NULL, L"Fast Logic Cycle Time",		AddrBP(BLKNUM_FSYS,  DYNAMIC,	8), 2);
	Tags.EmitRealM(NULL, L"Fast Logic CPU Pct Used",	AddrBP(BLKNUM_FSYS,  DYNAMIC,	5), 2);
	Tags.EmitRealM(NULL, L"Fast Logic Peak Time",		AddrBP(BLKNUM_FSYS,  DYNAMIC,	4), 2);
	Tags.EmitRealM(NULL, L"Fast Logic CB Overruns", 	AddrBP(BLKNUM_FSYS,  DYNAMIC,	6), 2);
	Tags.EmitEnumW(NULL, L"Mode Change Request",		AddrBP(BLKNUM_ASYS,  DYNAMIC,  76), Hc900ControllerModeChangeEnum);
	Tags.EmitFnumW(NULL, L"Save Database To Flash Request", AddrBP(BLKNUM_ASYS,  DYNAMIC,  27), Hc900NormalSaveEnum);
	Tags.EmitFnumM(NULL, L"Save Database To Flash Status",	AddrBP(BLKNUM_ASYS,  DYNAMIC,  46), Hc900SaveDatabaseToFlashStatusEnum);

	EmitFeature(L"Supports Modbus Tcp Slaves",	 0,  FEATURE_MODBUS_TCP_SLAVES);
	EmitFeature(L"Supports 32 Modbus Serial Slaves", 0,  FEATURE_MODBUS_SERIAL_SLAVES_32);
	EmitFeature(L"Supports 10 Host Connections",	 0,  FEATURE_TEN_HOST_CONNECTIONS);
	EmitDevCap (L"Supports 5 Racks",		 5,  DEVCAPINT_MAX_RACKS);
	EmitDevCap (L"Supports 12 Racks",		 12, DEVCAPINT_MAX_RACKS);
	EmitFeature(L"Supports Redundancy",		 0,  FEATURE_REDUNDANCY);
	EmitFeature(L"Supports Accutune III",		 0,  FEATURE_ACCUTUNE3);
	EmitFeature(L"Supports Manual Flash Burn",	 0,  FEATURE_MANUAL_FLASH_BURN);
	EmitFeature(L"Supports Enhanced Time Clock",	 0,  FEATURE_ENHANCED_TIME_CLOCK);
	EmitFeature(L"Supports Low Baud Rates", 	 0,  FEATURE_LOW_SERIAL_BAUD_RATES);
	EmitFeature(L"Supports Advanced 115.2 Kbaud",	 0,  FEATURE_HIGH_115K_SERIAL_BAUD_RATE);

	CString TimeLoc;

	TimeLoc.Printf( L"Max(%s - %u, 0)",
		        AddrBP(BLKNUM_ASYS, DYNAMIC, 29),
		        852076800
		        );

	Tags.EmitTimeM(NULL, L"Time And Date", TimeLoc, 2);

	Tags.NewFolder(L"SetTime");

	Tags.EmitTimeW(NULL, L"Time And Date",  SpecSystem(20), 2);

/*	Tags.EmitSintW(NULL, L"Time Zone Bias", SpecSystem(22));

*/	Tags.EmitSintW(NULL, L"Request",        SpecSystem(21));

	Tags.EndFolder();

	int nFreq = (m_SysConfigTable.m_b50Hz == 1) ? 50 : 60;

	Tags.EmitSintC(L"Frequency_Hz", L"Frequency (Hz)", nFreq);

	Tags.NewFolder(L"Calibrate");

	Tags.EmitSintW(NULL, L"Cal Req",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 52)  );
	Tags.EmitSintW(NULL, L"Cal Mod Number",     AddrBP(BLKNUM_ASYS, DYNAMIC, 53), 1, 12);
	Tags.EmitSintW(NULL, L"Cal Channel Number", AddrBP(BLKNUM_ASYS, DYNAMIC, 54), 1, 16);
	Tags.EmitSintW(NULL, L"Cal Rack Number",    AddrBP(BLKNUM_ASYS, DYNAMIC, 56), 1, 5);
	Tags.EmitEnumM(NULL, L"Cal Status",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 58), Hc900CalibrationStatusEnum);
	Tags.EmitRealW(NULL, L"Cal Out",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 55), 3, 0.0, 9999.9);
	Tags.EmitSintW(NULL, L"Cal Time",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 59)  );
	Tags.EmitEnumM(NULL, L"Cal Ref Type",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 60), Hc900CalibrateRefTypeEnum);
	Tags.EmitRealW(NULL, L"Cal Ref Value",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 61), 3, 0.0, 9999.9);
	Tags.EmitFnumW(NULL, L"Save Config",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 62), Hc900NormalSaveEnum);
	Tags.EmitEnumW(NULL, L"Cal Mode",	    AddrBP(BLKNUM_ASYS, DYNAMIC, 63), Hc900CalModeEnum);

	Tags.EmitSintW(L"CalBlock",     L"Calibration Block", AddrBP(BLKNUM_ASYS, DYNAMIC, 57)  );
	Tags.EmitEnumM(L"PPOCalStatus", L"Cal Status",        AddrBP(BLKNUM_ASYS, DYNAMIC, 58), Hc900PPOCalibrationStatusEnum);
	Tags.EmitRealM(L"CalFbk0",	L"Feedback at 0%",    AddrBP(BLKNUM_ASYS, DYNAMIC, 64), 3);
	Tags.EmitRealM(L"CalFbk100",    L"Feedback at 100%",  AddrBP(BLKNUM_ASYS, DYNAMIC, 65), 3);
	Tags.EmitRealM(L"CalMtrTime",   L"Motor Speed",       AddrBP(BLKNUM_ASYS, DYNAMIC, 66), 2);

	Tags.EndFolder();
	}

void CDataFile::EmitSerialTags(CMakeTags &Tags, int m_nBlockNum)
{
	// NOTE -- Parameter name has m_ prefix to fool PropD and PropS!

	Tags.EmitEnumM(NULL, L"Port Diagnostic",	PropD( 5), Hc900PortDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Port Status",		PropD( 4), Hc900PortStatusEnum);
	Tags.EmitWintM(NULL, L"Messages Received",	PropD( 6)  );
	Tags.EmitWintM(NULL, L"Data Link Errors",	PropD( 7)  );
	Tags.EmitWintM(NULL, L"Application Errors",	PropD( 8)  );
	Tags.EmitEnumW(NULL, L"Protocol",		PropD( 1), Hc900SerialPortProtocolEnum);
	Tags.EmitSintW(NULL, L"Slave Address",		PropD( 3)  );
	Tags.EmitFnumW(NULL, L"Slave Port Enabled",	PropD(41), Hc900DisableEnableEnum);
	Tags.EmitEnumW(NULL, L"Baud Rate",		PropD( 2), Hc900PortBaudRateEnum);
	Tags.EmitEnumW(NULL, L"Modbus Parity",		PropD(12), Hc900SerialPortParityEnum);
	Tags.EmitEnumW(NULL, L"Modbus Stop Bits",	PropD(13), Hc900SerialPortStopBitsEnum);
	Tags.EmitEnumW(NULL, L"Double Register Format", PropD(42), Hc900DoubleRegisterFormatEnum);
	Tags.EmitFnumW(NULL, L"Clear Statistics",	PropD( 9), Hc900NormalClearEnum);
	Tags.EmitFnumW(NULL, L"Save Settings",		PropD(25), Hc900NormalSaveEnum);
	}

void CDataFile::EmitEthernetTags(CMakeTags &Tags, int m_nBlockNum)
{
	// NOTE -- Parameter name has m_ prefix to fool PropD and PropS!

	if( m_nBlockNum == -1 ) {

		Tags.EmitEnumM(NULL, L"Port Diagnostic",          L"0", Hc900NetworkPortStatusEnum);
		Tags.EmitTextC(NULL, L"Network Name",             L""   );
		Tags.EmitTextC(NULL, L"MAC Address",              L""   );
		Tags.EmitSintC(NULL, L"IP Address",               0     );
		Tags.EmitSintC(NULL, L"Subnet Mask",              0     );
		Tags.EmitEnumM(NULL, L"Modbus Double Reg Format", L"",  Hc900DoubleRegisterFormatEnum);

		Tags.EmitTextC(NULL, L"Controller Name", L"");
		Tags.EmitTextC(NULL, L"Local Alias",     L"");
		Tags.EmitSintC(NULL, L"Gateway Address", 0  );

		return;
		}

	if( m_pDeviceCaps->HasFeature(FEATURE_VER2_SERIAL_NET_BLOCKS) ) {

		Tags.EmitEnumM(NULL, L"Port Diagnostic",          PropD( 1), Hc900NetworkPortStatusEnum);
		Tags.EmitTextM(NULL, L"Network Name",             AddrBP(BLKNUM_NET_IF, DYNAMIC, 1), 16);
		Tags.EmitEnetM(NULL, L"MAC Address",              PropD( 2)    );
		Tags.EmitInetM(NULL, L"IP Address",               PropD( 4)    );
		Tags.EmitInetM(NULL, L"Subnet Mask",              PropD( 5)    );
		Tags.EmitEnumW(NULL, L"Modbus Double Reg Format", AddrBP(BLKNUM_NET_IF, DYNAMIC, 5), Hc900DoubleRegisterFormatEnum);
		}
	else {
		Tags.EmitEnumM(NULL, L"Port Diagnostic",          PropD( 1), Hc900NetworkPortStatusEnum);
		Tags.EmitTextM(NULL, L"Network Name",             PropD( 9), 16);
		Tags.EmitEnetM(NULL, L"MAC Address",              PropD( 2)    );
		Tags.EmitInetM(NULL, L"IP Address",               PropD( 4)    );
		Tags.EmitInetM(NULL, L"Subnet Mask",              PropD( 5)    );
		Tags.EmitEnumW(NULL, L"Modbus Double Reg Format", PropD(13), Hc900DoubleRegisterFormatEnum);
		}

      Tags.EmitTextM(NULL, L"Controller Name", AddrBP(BLKNUM_ASYS, DYNAMIC, 35), 16);
      Tags.EmitTextM(NULL, L"Local Alias",     AddrBP(BLKNUM_ASYS, DYNAMIC, 31), 16);
      Tags.EmitInetM(NULL, L"Gateway Address", AddrBP(m_nBlockNum, DYNAMIC,  6)    );
      }


void CDataFile::EmitModbusSlavesTags(CMakeTags &Tags, int nSlaveNum, int nVersion)
{
	int idxOffset = nSlaveNum * 20;

	if( nVersion == 1 ) {

		Tags.EmitTextM(NULL, L"Slave Name",	    AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  1+idxOffset), 8);
		Tags.EmitSintM(NULL, L"Slave Address",	    AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  3+idxOffset)  );
		Tags.EmitFnumM(NULL, L"In Scan",	    AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  7+idxOffset), Hc900NoYesEnum);
		Tags.EmitFnumW(NULL, L"Scan Enable",	    AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  6+idxOffset), Hc900NoYesEnum);
		Tags.EmitEnumM(NULL, L"Comm Quality",	    AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  5+idxOffset), Hc900SlaveCommQualityEnum);
		Tags.EmitWintM(NULL, L"Received Count",     AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  8+idxOffset)  );
		Tags.EmitWintM(NULL, L"Data Link Errors",   AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  9+idxOffset)  );
		Tags.EmitWintM(NULL, L"Application Errors", AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC, 10+idxOffset)  );
		}

	if( nVersion == 2 ) {

		Tags.EmitTextM(NULL, L"Slave Name",	      AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  1+idxOffset), 16);
		Tags.EmitSintM(NULL, L"Slave Address",	      AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  5+idxOffset)  );
		Tags.EmitFnumM(NULL, L"In Scan",	      AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  9+idxOffset), Hc900NoYesEnum);
		Tags.EmitFnumW(NULL, L"Scan Enable",	      AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  8+idxOffset), Hc900NoYesEnum);
		Tags.EmitEnumM(NULL, L"Comm Quality",	      AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC,  7+idxOffset), Hc900SlaveCommQualityEnum);
		Tags.EmitWintM(NULL, L"Received Count",       AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC, 10+idxOffset)  );
		Tags.EmitWintM(NULL, L"Data Link Errors",     AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC, 11+idxOffset)  );
		Tags.EmitWintM(NULL, L"Application Errors",   AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC, 12+idxOffset)  );
		Tags.EmitFnumM(NULL, L"Enable Pin Connected", AddrBP(BLKNUM_MBSER_SLVSTATUS, DYNAMIC, 13+idxOffset), Hc900NoYesEnum);
		}
	}

void CDataFile::EmitModbusTcpSlavesTags(CMakeTags &Tags, int nSlaveNum)
{
	int idxOffset = nSlaveNum * 20;

	Tags.EmitTextM(NULL, L"Slave Name",	      AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC,  1+idxOffset), 16);
	Tags.EmitInetM(NULL, L"Slave Address",	      AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC,  5+idxOffset)  );
	Tags.EmitFnumM(NULL, L"In Scan",	      AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC,  9+idxOffset), Hc900NoYesEnum);
	Tags.EmitFnumW(NULL, L"Scan Enable",	      AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC,  8+idxOffset), Hc900NoYesEnum);
	Tags.EmitEnumM(NULL, L"Comm Quality",	      AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC,  7+idxOffset), Hc900SlaveCommQualityEnum);
	Tags.EmitWintM(NULL, L"Received Count",       AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC, 10+idxOffset)  );
	Tags.EmitWintM(NULL, L"Data Link Errors",     AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC, 11+idxOffset)  );
	Tags.EmitWintM(NULL, L"Application Errors",   AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC, 12+idxOffset)  );
	Tags.EmitFnumM(NULL, L"Enable Pin Connected", AddrBP(BLKNUM_MBTCP_SLVSTATUS, DYNAMIC, 13+idxOffset), Hc900NoYesEnum);
	}

void CDataFile::EmitHostConnectionsTags(CMakeTags &Tags, int nHostNum)
{
	int idxOffset = nHostNum * 100;

	Tags.EmitEnumM(NULL, L"Port Diagnostic", AddrBP(BLKNUM_NTWK_E1, DYNAMIC, 104+idxOffset), Hc900PortDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Protocol",	 AddrBP(BLKNUM_NTWK_E1, DYNAMIC, 100+idxOffset), Hc900EthernetPortProtocolEnum);
	Tags.EmitInetM(NULL, L"IP Address",	 AddrBP(BLKNUM_NTWK_E1, DYNAMIC, 102+idxOffset)  );
	Tags.EmitWintM(NULL, L"Received Count",  AddrBP(BLKNUM_NTWK_E1, DYNAMIC, 105+idxOffset)  );
	Tags.EmitWintM(NULL, L"App Error Count", AddrBP(BLKNUM_NTWK_E1, DYNAMIC, 107+idxOffset)  );
	Tags.EmitFnumW(NULL, L"Clear",		 AddrBP(BLKNUM_NTWK_E1, DYNAMIC, 108+idxOffset), Hc900NormalClearEnum);
	}

void CDataFile::EmitIORacksTags(CMakeTags &Tags, int nRackNum, BOOL fReal)
{
	// Calculate rack block #. Rack block #s are backwards!

	// NOTE -- Name has m_ prefix to fool PropD and PropS!

	int m_nBlockNum = fReal ? (BLKNUM_RACK1 - nRackNum) : -1;

	// TODO -- The following rack # checks apply only to the C30, C50, and C70 (NOT THE C70R!!)

	if( nRackNum == 0 ) {

		Tags.EmitEnumM(NULL, L"System Diagnostic",    PropD(134), Hc900ControllerSystemDiagnosticEnum);
		Tags.EmitEnumM(NULL, L"RTC Diagnostic",       PropD(137), Hc900RtcDiagnosticEnum);
		Tags.EmitEnumM(NULL, L"Comm Port Diagnostic", PropD(138), Hc900IoRackCommPortDiagnosticEnum);
		}

	if( nRackNum >= 1 ) {

		Tags.EmitEnumM(NULL, L"IO Comm Link Diag", PropD(141), Hc900IOCommLinkDiagnosticEnum);
		}

	Tags.EmitEnumM(NULL, L"CPU Diagnostic", 	PropD(135), Hc900CpuDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Memory Diagnostic",	PropD(136), Hc900MemoryDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"IO Diagnostic",		PropD( 21), Hc900RackIODiagnosticEnum);
	Tags.EmitRealM(NULL, L"Firmware Rev",		PropD(	1), 3);
	Tags.EmitWintM(NULL, L"Total Message Count",	PropD(142)  );
	Tags.EmitWintM(NULL, L"Total Link Error Count", PropD(143)  );
	Tags.EmitFnumM(NULL, L"Rack In Config", 	PropD(140), Hc900NoYesEnum);
	Tags.EmitFnumW(NULL, L"Clear Statistics",	PropD(144), Hc900NormalClearEnum);

	// Emit folders for modules last so they appear on the bottom.

	for( int module = 0; module < 12; module++ ) {

		Tags.NewFolder(CPrintf(L"Module%d", module+1));

		Tags.EmitEnumM(NULL, L"Physical Type",	   PropD( 38 + module * 1), Hc900ModulePhyTypeEnum);
		Tags.EmitEnumM(NULL, L"Configured Type",   PropD(118 + module * 1), Hc900ModuleConfTypeEnum);
		Tags.EmitEnumM(NULL, L"Module Diagnostic", PropD( 22 + module * 1), Hc900ModuleDiagnosticEnum);
		Tags.EmitTextM(NULL, L"Part Number",	   PropD( 54 + module * 3), 12);
		Tags.EmitRealM(NULL, L"Firmware Rev",	   PropD(102 + module * 1), 3);

		Tags.EndFolder();
		}
	}

void CDataFile::EmitRedundantOverviewTags(CMakeTags &Tags, BOOL fReal)
{
	int nASYS = fReal ? BLKNUM_ASYS : -1;
	int nRSYS = fReal ? BLKNUM_RSYS : -1;

	Tags.EmitEnumM(NULL, L"Reserve Status", 	      AddrBP(nASYS, DYNAMIC, 45), Hc900ReserveStatusEnum);
	Tags.EmitEnumM(NULL, L"Redundancy Status",	      AddrBP(nRSYS, DYNAMIC,  1), Hc900RedundancyStatusEnum);
	Tags.EmitEnumM(NULL, L"System Status",		      AddrBP(nASYS, DYNAMIC, 81), Hc900RedundancySystemStatusEnum);
	Tags.EmitEnumM(NULL, L"CPU Diagnostic", 	      AddrBP(nASYS, DYNAMIC, 82), Hc900RedundancyDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Memory Diagnostic",	      AddrBP(nASYS, DYNAMIC, 83), Hc900RedundancyDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"RTC Diagnostic", 	      AddrBP(nASYS, DYNAMIC, 84), Hc900RedundancyDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"IO Rack Comm Port Diagnostic", AddrBP(nASYS, DYNAMIC, 86), Hc900RedundancyDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Rack Module Diagnostic",       AddrBP(nASYS, DYNAMIC, 87), Hc900RedundancyDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Comm Port Diagnostic",	      AddrBP(nASYS, DYNAMIC, 85), Hc900RedundancyDiagnosticEnum);
	}

void CDataFile::EmitRedundantLeadCPUTags(CMakeTags &Tags, BOOL fReal)
{
	int nASYS      = fReal ? BLKNUM_ASYS      : -1;
	int nLEADCPU   = fReal ? BLKNUM_LEADCPU   : -1;
	int nNTWK_E1   = fReal ? BLKNUM_NTWK_E1   : -1;
	int nNTWK_E2   = fReal ? BLKNUM_NTWK_E2   : -1;
	int nSERIAL_S1 = fReal ? BLKNUM_SERIAL_S1 : -1;
	int nSERIAL_S2 = fReal ? BLKNUM_SERIAL_S1 : -1;

	Tags.EmitFnumM(NULL, L"Reserve Status", 	      AddrBP(nASYS,      DYNAMIC, 45), Hc900ReserveStatusEnum);
	Tags.EmitEnumM(NULL, L"CPU Position",		      AddrBP(nLEADCPU,   DYNAMIC,  7), Hc900RedundancyCpuPositionEnum);
	Tags.EmitEnumM(NULL, L"CPU Diagnostic", 	      AddrBP(nLEADCPU,   DYNAMIC,  3), Hc900CpuDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Memory Diagnostic",	      AddrBP(nLEADCPU,   DYNAMIC,  4), Hc900MemoryDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"RTC Diagnostic", 	      AddrBP(nLEADCPU,   DYNAMIC,  5), Hc900RtcDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"IO Rack Comm Port Diagnostic", AddrBP(nLEADCPU,   DYNAMIC,  6), Hc900IoRackCommPortDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"E1 Port Diagnostic",	      AddrBP(nNTWK_E1,   DYNAMIC,  1), Hc900NetworkPortStatusEnum);
	Tags.EmitEnumM(NULL, L"E2 Port Diagnostic",	      AddrBP(nNTWK_E2,   DYNAMIC,  1), Hc900NetworkPortStatusEnum);
	Tags.EmitEnumM(NULL, L"S1 Port Diagnostic",	      AddrBP(nSERIAL_S1, DYNAMIC,  5), Hc900PortDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"S2 Port Diagnostic",	      AddrBP(nSERIAL_S2, DYNAMIC,  5), Hc900PortDiagnosticEnum);
	}

void CDataFile::EmitRedundantReserveCPUTags(CMakeTags &Tags, BOOL fReal)
{
	int nASYS    = fReal ? BLKNUM_ASYS    : -1;
	int nRESVCPU = fReal ? BLKNUM_RESVCPU : -1;

	Tags.EmitFnumM(NULL, L"Reserve Status", 	      AddrBP(nASYS,    DYNAMIC, 45), Hc900ReserveStatusEnum);
	Tags.EmitEnumM(NULL, L"CPU Position",		      AddrBP(nRESVCPU, DYNAMIC, 21), Hc900RedundancyCpuPositionEnum);
	Tags.EmitEnumM(NULL, L"CPU Diagnostic", 	      AddrBP(nRESVCPU, DYNAMIC,  3), Hc900CpuDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"Memory Diagnostic",	      AddrBP(nRESVCPU, DYNAMIC,  4), Hc900MemoryDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"RTC Diagnostic", 	      AddrBP(nRESVCPU, DYNAMIC,  5), Hc900RtcDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"IO Rack Comm Port Diagnostic", AddrBP(nRESVCPU, DYNAMIC, 10), Hc900IoRackCommPortDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"E1 Port Diagnostic",	      AddrBP(nRESVCPU, DYNAMIC,  8), Hc900NetworkPortStatusEnum);
	Tags.EmitEnumM(NULL, L"E2 Port Diagnostic",	      AddrBP(nRESVCPU, DYNAMIC,  9), Hc900NetworkPortStatusEnum);
	Tags.EmitEnumM(NULL, L"S1 Port Diagnostic",	      AddrBP(nRESVCPU, DYNAMIC,  6), Hc900PortDiagnosticEnum);
	Tags.EmitEnumM(NULL, L"S2 Port Diagnostic",	      AddrBP(nRESVCPU, DYNAMIC,  7), Hc900PortDiagnosticEnum);
	}

void CDataFile::EmitScreens(void)
{
	}

// End of File
