
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//					
// CInstrumentInfo defines the basic attributes of a
// device such as its platform type and product type.
//

CInstrumentInfo::CInstrumentInfo()
{
	m_nPlatformType = -1;
	m_nProductType = -1;
	m_nDbSchema = -1;
	m_nFeatureSet = -1;
	}

CInstrumentInfo::CInstrumentInfo(int nPlatformType, int nProductType, int nDbSchema, int nFeatureSet)
{
	m_nPlatformType = nPlatformType;
	m_nProductType = nProductType;
	m_nDbSchema = nDbSchema;
	m_nFeatureSet = nFeatureSet;
	}

BOOL CInstrumentInfo::operator == (const CInstrumentInfo &InstInfo) const
{
	return m_nPlatformType == InstInfo.m_nPlatformType &&
		   m_nProductType  == InstInfo.m_nProductType  &&
		   m_nDbSchema     == InstInfo.m_nDbSchema     &&
		   m_nFeatureSet   == InstInfo.m_nFeatureSet;
	}

//////////////////////////////////////////////////////////////////////////
//					
// CDeviceRev defines the attributes of a device revision
// such as its schema number and feature set.
//

CDeviceRev::CDeviceRev()
{
	m_DeviceRev = L"";
	m_nSchemaNumber = -1;
	m_nFeatureSet = -1;
	}

CDeviceRev::CDeviceRev(CString DeviceRev, int nSchemaNumber, int nFeatureSet)
{
	m_DeviceRev = DeviceRev;
	m_nSchemaNumber = nSchemaNumber;
	m_nFeatureSet = nFeatureSet;
	}

//////////////////////////////////////////////////////////////////////////
//					
// CDeviceType defines the identification attributes of a
// device type such as its platform type and product type,
// its name (e.g., "HC900-C50") and the revisions that it
// supports (schema numbers and feature sets).
//

CDeviceType::CDeviceType()
{
	m_nPlatformType = -1;
	m_nProductType = -1;
	m_DeviceName = L"";
	}

CDeviceType::CDeviceType(int nPlatformType, int nProductType, CString DeviceName)
{
	m_nPlatformType = nPlatformType;
	m_nProductType = nProductType;
	m_DeviceName = DeviceName;
	}

CDeviceType::~CDeviceType()
{
	for( int i = 0; i < (int)m_RevArray.GetCount();	i++ ) {

		delete m_RevArray.GetAt(i);
		}
	}

CDeviceCaps* CDeviceType::CreateDeviceCapsObject(const CInstrumentInfo &InstInfo)
{
	// Cache this device type object and instrument info
	// on the device capabilities object to be created.

	switch( m_nProductType ) {

		case DEVTYPE_HC930:  return New CHc930 (this, InstInfo);
		case DEVTYPE_HC930S: return New CHc930 (this, InstInfo);
		case DEVTYPE_HC950:  return New CHc950 (this, InstInfo);
		case DEVTYPE_HC950S: return New CHc950 (this, InstInfo);
		case DEVTYPE_HC970:  return New CHc970 (this, InstInfo);
		case DEVTYPE_HC970S: return New CHc970 (this, InstInfo);
		case DEVTYPE_HC970R: return New CHc970R(this, InstInfo);
		case DEVTYPE_HC975:  return New CHc970R(this, InstInfo);
		case DEVTYPE_HC975S: return New CHc970R(this, InstInfo);

		default:
			return NULL;
		}
	}

int CDeviceType::GetDeviceFeatureSet(int nIndex)
{
	return m_RevArray.GetAt(nIndex)->m_nFeatureSet;
	}

CString CDeviceType::GetDeviceName()
{
	return m_DeviceName;
	}

CString CDeviceType::GetDeviceNameRev(int nIndex)
{
	char buf[256];
	sprintf(buf, "%ls %ls", GetDeviceName(), GetDeviceRev(nIndex));
	return CString(buf);
	}

CString CDeviceType::GetDeviceRev(int nIndex)
{
	char buf[256];
	sprintf(buf, "Rev %ls", m_RevArray.GetAt(nIndex)->m_DeviceRev);
	return CString(buf);
	}

CString CDeviceType::GetDeviceRev(const CInstrumentInfo &InstInfo)
{
	int nIndex = HasInstInfo(InstInfo);
	return nIndex >= 0 ? GetDeviceRev(nIndex) : L"Rev ????";
	}

int CDeviceType::GetDeviceRevCount()
{
	return m_RevArray.GetCount();
	}

CInstrumentInfo* CDeviceType::GetInstInfo(int nIndex)
{
	return New CInstrumentInfo(m_nPlatformType, m_nProductType, m_RevArray.GetAt(nIndex)->m_nSchemaNumber, m_RevArray.GetAt(nIndex)->m_nFeatureSet);
	}

// Return the index of the device revision which matches
// the specified instrument info passed in.
//
// param instInfo - instrument info to match
// return -1 if a matching device revision wasn't found
// else return the index of the matching device revision.

int CDeviceType::HasInstInfo(const CInstrumentInfo &InstInfo)
{
	int nRevCount = GetDeviceRevCount();
	for( int nIndex = 0; nIndex < nRevCount; nIndex++ ) {

		CInstrumentInfo *pInstInfo = GetInstInfo(nIndex);
		if( InstInfo == *pInstInfo ) {

			delete pInstInfo;
			return nIndex;
			}
		else
			delete pInstInfo;
		}

	return -1; // instrument info doesn't match a device revision
	}

void CDeviceType::AddDeviceRev(CDeviceRev *pDeviceRev)
{
	m_RevArray.Append(pDeviceRev);
	}

//////////////////////////////////////////////////////////////////////////
//					
// CDeviceTypeInfo defines all supported device types
// (e.g., an HC900-C30, an HC900-C50, an HC900-C70)
// and their revisions.


CDeviceTypeInfo::CDeviceTypeInfo()
{
	// Construct all of the supported device types and their revisions.
	// Device revisions specify revision string, schema, and feature set.

	MakeHC900_30();
	MakeHC900_30S();

	MakeHC900_50();
	MakeHC900_50S();

	MakeHC900_70();
	MakeHC900_70S();

	MakeHC900_70R();

	MakeHC900_75();
	MakeHC900_75S();
	}

CDeviceTypeInfo::~CDeviceTypeInfo()
{
	for( int i = 0; i < (int)m_DeviceTypes.GetCount(); i++ ) {

		delete m_DeviceTypes.GetAt(i);
		}
	}

void CDeviceTypeInfo::AddDeviceType(CDeviceType *pDeviceType)
{
	m_DeviceTypes.Append(pDeviceType);
	}

CDeviceType* CDeviceTypeInfo::GetDeviceType(const CInstrumentInfo &InstInfo)
{
	for( int i = 0; i < (int)m_DeviceTypes.GetCount(); i++ ) {

		CDeviceType *pDeviceType = m_DeviceTypes.GetAt(i);
		if( pDeviceType->HasInstInfo(InstInfo) != -1 )
			return pDeviceType;
		}

	return NULL;
	};

// Implementation

void CDeviceTypeInfo::MakeHC900_30(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C30
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC930, L"HC900-C30");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.1x",      29, REV_1p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.0x",      30, REV_2p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.1x/2.3x", 30, REV_2p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      30, REV_2p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_30S(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C30S
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC930S, L"HC900-C30S");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.1x",      29, REV_1p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.0x",      30, REV_2p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.1x/2.3x", 30, REV_2p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      30, REV_2p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_50(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C50
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC950, L"HC900-C50");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.0x",      29, REV_1p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.1x",      29, REV_1p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.0x",      30, REV_2p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.1x/2.3x", 30, REV_2p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      30, REV_2p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_50S(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C50S
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC950S, L"HC900-C50S");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.0x",      29, REV_1p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.1x",      29, REV_1p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.0x",      30, REV_2p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.1x/2.3x", 30, REV_2p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      30, REV_2p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_70(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C70
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC970, L"HC900-C70");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.3x",      30, REV_2p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      30, REV_2p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_70S(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C70S
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC970S, L"HC900-C70S");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.3x",      30, REV_2p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      30, REV_2p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_70R(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C70R
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC970R, L"HC900-C70R");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.0x",      31, REV_1p0xC70R));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      31, REV_2p4xC70R));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_75(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C75
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC975, L"HC900-C75");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.0x",      31, REV_1p0xC70R));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      31, REV_2p4xC70R));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

void CDeviceTypeInfo::MakeHC900_75S(void)
{
	////////////////////////////////////////////////////////////////
	//                          HC900-C75S
	////////////////////////////////////////////////////////////////

	CDeviceType *pDeviceType = New CDeviceType(DEVCLASS_HC900, DEVTYPE_HC975S, L"HC900-C75S");

	pDeviceType->AddDeviceRev(New CDeviceRev(L"1.0x",      31, REV_1p0xC70R));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"2.4x",      31, REV_2p4xC70R));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.0x",      32, REV_4p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.1x",      32, REV_4p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.2x",      32, REV_4p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.3x",      32, REV_4p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"4.4x",      32, REV_4p4x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.0x",      33, REV_6p0x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.1x",      34, REV_6p1x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.2x",      34, REV_6p2x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.3x",      35, REV_6p3x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.4x",      35, REV_6p4x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.5x",      36, REV_6p5x));	
	pDeviceType->AddDeviceRev(New CDeviceRev(L"6.7x",      36, REV_6p7x));
	pDeviceType->AddDeviceRev(New CDeviceRev(L"7.0x",      36, REV_7p0x));

	AddDeviceType(pDeviceType);
	}

//////////////////////////////////////////////////////////////////////////
//
// CDeviceCaps defines the capabilities of all revisions
// of a device type. It is an abstract base class.
// 
// USAGE:
//
//   To test if a device type/revision has a feature:
//   Pass in a member from enum EDeviceCapFeature:
//   GetDeviceCaps().HasFeature(FEATURE_USER_MB_REG);
//
//   To get the value of an integer attribute in a device type/revision:
//   Pass in a member from enum EDeviceCapInt:
//   GetDeviceCaps().GetValue(DEVCAPINT_MAX_RESERVED_BLOCKS);
//
//   To get the value of a double attribute in a device type/revision:
//   Pass in a member from enum EDeviceCapDbl:
//   GetDeviceCaps().GetValue(DEVCAPDBL_FBLK_MIN_CYC_TIME);

CDeviceCaps::CDeviceCaps()
{
	}

CDeviceCaps::CDeviceCaps(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo)
{
	m_pDeviceType = pDeviceType;
	m_InstInfo = InstInfo;
	m_nPlatformType = InstInfo.m_nPlatformType;
	m_nProductType = InstInfo.m_nProductType;
	m_nDbSchema = InstInfo.m_nDbSchema;
	m_nFeatureSet = InstInfo.m_nFeatureSet;
	}

CString CDeviceCaps::GetDeviceNameRev()
{
	char buf[256];
	sprintf(buf, "%ls %ls", m_pDeviceType->GetDeviceName(), GetDeviceRev());
	return CString(buf);
	}

CString CDeviceCaps::GetDeviceRev()
{
	return m_pDeviceType->GetDeviceRev(m_InstInfo);
	}

// Get the integer value of the specified key on the device.
int CDeviceCaps::GetValue(EDeviceCapInt key)
{
	INDEX nIndex = m_IntValues.FindName(key);
	if( nIndex != NULL )
		return m_IntValues.GetData(nIndex);
	else
		return -1;
	}

// Get the double value of the specified key on the device.
double CDeviceCaps::GetValue(EDeviceCapDbl key)
{
	INDEX nIndex = m_DblValues.FindName(key);
	if( nIndex != NULL )
		return m_DblValues.GetData(nIndex);
	else
		return -1.0;
	}

// Check if the device has the specified feature.
// If the feature is not in the device values
// then the device is assumed not to have the feature.
// Else if the feature is in the device values
// then get its value (either true or false).

BOOL CDeviceCaps::HasFeature(EDeviceCapFeature key)
{
	INDEX nIndex = m_FeatureValues.FindName(key);
	if( nIndex != NULL)
		return m_FeatureValues.GetData(nIndex);
	else
		return false;
	}

// Add a feature to the device values.
void CDeviceCaps::AddFeature(EDeviceCapFeature key, BOOL value)
{
	m_FeatureValues.Insert(key, value);
	}

// Add an integer value to the device values.
void CDeviceCaps::AddValue(EDeviceCapInt key, int value)
{
	m_IntValues.Insert(key, value);
	}

// Add a double value to the device values.
void CDeviceCaps::AddValue(EDeviceCapDbl key, double value)
{
	m_DblValues.Insert(key, value);
	}

//////////////////////////////////////////////////////////////////////////
//
// CHc930 defines the capabilities of all revisions
// of device type HC900-C30.

CHc930::CHc930(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo)
	: CDeviceCaps(pDeviceType, InstInfo)
{
	// Add integer values to device values.

	int maxRacks = (m_nFeatureSet >= REV_6p3x) ? 12 : 1;	
	AddValue(DEVCAPINT_MAX_RACKS, maxRacks);

	int maxMBS = (m_nFeatureSet >= REV_4p0x) ? 32 : 16;
	AddValue(DEVCAPINT_MAX_SERIAL_MODBUS_SLAVES, maxMBS);

	int carbonVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1; // in HCD, this is version 3 or 2
	AddValue(DEVCAPINT_BLOCK_VERSION_CARBON, carbonVer);

	int pidVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1;
	AddValue(DEVCAPINT_BLOCK_VERSION_PID, pidVer);

	int wireFactor = (m_nFeatureSet >= REV_4p0x) ? 8 : 4;
	AddValue(DEVCAPINT_WIRE_GRAPHICS_RECORD_FACTOR, wireFactor);

	int sizeofAEPacket = (m_nFeatureSet >= REV_4p3x) ? 7 : 6;
	AddValue(DEVCAPINT_SIZE_AE_PACKET, sizeofAEPacket);

	// Add double values to device values.

	AddValue(DEVCAPDBL_BLK_MIN_CYC_TIME, 1.0);

	// Add features. You can either explicitly call AddFeature to add a
	// feature to the device values, passing in a boolean value
	// of false or true or you can simply not call AddFeature for a feature,
	// in which case the device is assumed not to have the feature.

	AddFeature(FEATURE_USER_MB_REG, m_nFeatureSet >= REV_2p0x);
	AddFeature(FEATURE_OI_SECURITY, m_nFeatureSet >= REV_2p1x);
	AddFeature(FEATURE_REDUNDANCY, false);
	AddFeature(FEATURE_TWO_ETHERNET_PORTS, false);
	AddFeature(FEATURE_TEN_HOST_CONNECTIONS, false); // only C50, C70, C70R
	AddFeature(FEATURE_MODBUS_TCP_SLAVES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MODBUS_SERIAL_SLAVES_32, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ACCUTUNE3, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MANUAL_FLASH_BURN, m_nFeatureSet >= REV_2p4x);
	AddFeature(FEATURE_SUPPORT_30_ALARM_GROUPS, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ENHANCED_TIME_CLOCK, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_UNLIMITED_RECIPES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_LOW_SERIAL_BAUD_RATES, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_HIGH_115K_SERIAL_BAUD_RATE, m_nFeatureSet >= REV_4p2x);
	AddFeature(FEATURE_GENERIC_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_PPO_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_CALENDAR_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_TWO_BYTE_ALARM_NUM, m_nFeatureSet >= REV_4p3x);
	AddFeature(FEATURE_VER2_SERIAL_NET_BLOCKS, false); // legacy does not use new Network Info Blocks
	AddFeature(FEATURE_SPP_TAG_TABLE, m_nFeatureSet >= REV_6p7x);
	}

//////////////////////////////////////////////////////////////////////////
//
// CHc950 defines the capabilities of all revisions
// of device type HC900-C50.

CHc950::CHc950(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo)
	: CDeviceCaps(pDeviceType, InstInfo)
{
	// Add integer values to device values.

	int maxRacks = (m_nFeatureSet >= REV_6p3x) ? 12 : 5;	
	AddValue(DEVCAPINT_MAX_RACKS, maxRacks);

	int maxMBS = (m_nFeatureSet >= REV_4p0x) ? 32 : 16;
	AddValue(DEVCAPINT_MAX_SERIAL_MODBUS_SLAVES, maxMBS);

	int carbonVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1; // in HCD, this is version 3 or 2
	AddValue(DEVCAPINT_BLOCK_VERSION_CARBON, carbonVer);

	int pidVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1;
	AddValue(DEVCAPINT_BLOCK_VERSION_PID, pidVer);

	int wireFactor = (m_nFeatureSet >= REV_4p0x) ? 8 : 4;
	AddValue(DEVCAPINT_WIRE_GRAPHICS_RECORD_FACTOR, wireFactor);

	int sizeofAEPacket = (m_nFeatureSet >= REV_4p3x) ? 7 : 6;
	AddValue(DEVCAPINT_SIZE_AE_PACKET, sizeofAEPacket);

	// Add double values to device values.

	AddValue(DEVCAPDBL_BLK_MIN_CYC_TIME, 1.0);

	// Add features. You can either explicitly call AddFeature to add a
	// feature to the device values, passing in a boolean value
	// of false or true or you can simply not call AddFeature for a feature,
	// in which case the device is assumed not to have the feature.

	AddFeature(FEATURE_USER_MB_REG, m_nFeatureSet >= REV_2p0x);
	AddFeature(FEATURE_OI_SECURITY, m_nFeatureSet >= REV_2p1x);
	AddFeature(FEATURE_REDUNDANCY, false);
	AddFeature(FEATURE_TWO_ETHERNET_PORTS, false);
	AddFeature(FEATURE_TEN_HOST_CONNECTIONS, m_nFeatureSet >= REV_4p2x);
	AddFeature(FEATURE_MODBUS_TCP_SLAVES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MODBUS_SERIAL_SLAVES_32, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ACCUTUNE3, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MANUAL_FLASH_BURN, m_nFeatureSet >= REV_2p4x);
	AddFeature(FEATURE_SUPPORT_30_ALARM_GROUPS, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ENHANCED_TIME_CLOCK, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_UNLIMITED_RECIPES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_LOW_SERIAL_BAUD_RATES, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_HIGH_115K_SERIAL_BAUD_RATE, m_nFeatureSet >= REV_4p2x);
	AddFeature(FEATURE_GENERIC_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_PPO_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_CALENDAR_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_TWO_BYTE_ALARM_NUM, m_nFeatureSet >= REV_4p3x);
	AddFeature(FEATURE_VER2_SERIAL_NET_BLOCKS, false); // legacy does not use new Network Info Blocks
	AddFeature(FEATURE_SPP_TAG_TABLE, m_nFeatureSet >= REV_6p7x);
	}

//////////////////////////////////////////////////////////////////////////
//
// CHc970 defines the capabilities of all revisions
// of device type HC900-C70.

CHc970::CHc970(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo)
	: CDeviceCaps(pDeviceType, InstInfo)
{
	// Add integer values to device values.

	int maxRacks = (m_nFeatureSet >= REV_6p3x) ? 12 : 5;	
	AddValue(DEVCAPINT_MAX_RACKS, maxRacks);

	int maxMBS = (m_nFeatureSet >= REV_4p0x) ? 32 : 16;
	AddValue(DEVCAPINT_MAX_SERIAL_MODBUS_SLAVES, maxMBS);

	int carbonVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1; // in HCD, this is version 3 or 2
	AddValue(DEVCAPINT_BLOCK_VERSION_CARBON, carbonVer);

	int pidVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1;
	AddValue(DEVCAPINT_BLOCK_VERSION_PID, pidVer);

	int wireFactor =  8; // always for C70
	AddValue(DEVCAPINT_WIRE_GRAPHICS_RECORD_FACTOR, wireFactor);

	int sizeofAEPacket = (m_nFeatureSet >= REV_4p3x) ? 7 : 6;
	AddValue(DEVCAPINT_SIZE_AE_PACKET, sizeofAEPacket);

	// Add double values to device values.

	AddValue(DEVCAPDBL_BLK_MIN_CYC_TIME, 1.0);

	// Add features. You can either explicitly call AddFeature to add a
	// feature to the device values, passing in a boolean value
	// of false or true or you can simply not call AddFeature for a feature,
	// in which case the device is assumed not to have the feature.

	AddFeature(FEATURE_USER_MB_REG, m_nFeatureSet >= REV_2p0x);
	AddFeature(FEATURE_OI_SECURITY, m_nFeatureSet >= REV_2p1x);
	AddFeature(FEATURE_REDUNDANCY, false);
	AddFeature(FEATURE_TWO_ETHERNET_PORTS, true);
	AddFeature(FEATURE_TEN_HOST_CONNECTIONS, true);
	AddFeature(FEATURE_MODBUS_TCP_SLAVES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MODBUS_SERIAL_SLAVES_32, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ACCUTUNE3, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MANUAL_FLASH_BURN, m_nFeatureSet >= REV_2p4x);
	AddFeature(FEATURE_SUPPORT_30_ALARM_GROUPS, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ENHANCED_TIME_CLOCK, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_UNLIMITED_RECIPES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_LOW_SERIAL_BAUD_RATES, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_HIGH_115K_SERIAL_BAUD_RATE, m_nFeatureSet >= REV_4p2x);
	AddFeature(FEATURE_GENERIC_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_PPO_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_CALENDAR_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_TWO_BYTE_ALARM_NUM, m_nFeatureSet >= REV_4p3x);
	AddFeature(FEATURE_VER2_SERIAL_NET_BLOCKS, true);
	AddFeature(FEATURE_SPP_TAG_TABLE, m_nFeatureSet >= REV_6p7x);
	}

//////////////////////////////////////////////////////////////////////////
//
// CHc970R defines the capabilities of all revisions
// of device type HC900-C70R.

CHc970R::CHc970R(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo)
	: CDeviceCaps(pDeviceType, InstInfo)
{
	// Add integer values to device values.

	int maxRacks = (m_nFeatureSet >= REV_6p3x) ? 12 : 5;	
	AddValue(DEVCAPINT_MAX_RACKS, maxRacks);

	int maxMBS = (m_nFeatureSet >= REV_4p0x) ? 32 : 16;
	AddValue(DEVCAPINT_MAX_SERIAL_MODBUS_SLAVES, maxMBS);\

	int carbonVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1; // in HCD, this is version 3 or 2
	AddValue(DEVCAPINT_BLOCK_VERSION_CARBON, carbonVer);

	int pidVer = (m_nFeatureSet >= REV_4p0x) ? 2 : 1;
	AddValue(DEVCAPINT_BLOCK_VERSION_PID, pidVer);

	int wireFactor =  8;	// always for C70
	AddValue(DEVCAPINT_WIRE_GRAPHICS_RECORD_FACTOR, wireFactor);

	int sizeofAEPacket = (m_nFeatureSet >= REV_4p3x) ? 7 : 6;
	AddValue(DEVCAPINT_SIZE_AE_PACKET, sizeofAEPacket);

	// Add double values to device values.

	AddValue(DEVCAPDBL_BLK_MIN_CYC_TIME, 1.0);

	// Add features. You can either explicitly call AddFeature to add a
	// feature to the device values, passing in a boolean value
	// of false or true or you can simply not call AddFeature for a feature,
	// in which case the device is assumed not to have the feature.

	AddFeature(FEATURE_USER_MB_REG, m_nFeatureSet >= REV_2p0x);
	AddFeature(FEATURE_OI_SECURITY, m_nFeatureSet >= REV_2p1x);
	AddFeature(FEATURE_REDUNDANCY, true);
	AddFeature(FEATURE_TWO_ETHERNET_PORTS, true);
	AddFeature(FEATURE_TEN_HOST_CONNECTIONS, true);
	AddFeature(FEATURE_MODBUS_TCP_SLAVES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MODBUS_SERIAL_SLAVES_32, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ACCUTUNE3, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_MANUAL_FLASH_BURN, m_nFeatureSet >= REV_2p4x);
	AddFeature(FEATURE_SUPPORT_30_ALARM_GROUPS, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_ENHANCED_TIME_CLOCK, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_UNLIMITED_RECIPES, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_LOW_SERIAL_BAUD_RATES, m_nFeatureSet >= REV_4p1x);
	AddFeature(FEATURE_HIGH_115K_SERIAL_BAUD_RATE, m_nFeatureSet >= REV_4p2x);
	AddFeature(FEATURE_GENERIC_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_PPO_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_CALENDAR_TAG_TABLE, m_nFeatureSet >= REV_4p0x);
	AddFeature(FEATURE_TWO_BYTE_ALARM_NUM, m_nFeatureSet >= REV_4p3x);
	AddFeature(FEATURE_VER2_SERIAL_NET_BLOCKS, true);
	AddFeature(FEATURE_SPP_TAG_TABLE, m_nFeatureSet >= REV_6p7x);
	}
