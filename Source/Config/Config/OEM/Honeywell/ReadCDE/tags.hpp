
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TAGS_HPP
	
#define	INCLUDE_TAGS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMakeTags;

//////////////////////////////////////////////////////////////////////////
//					
// Tag Creation Wrapper
//

class CMakeTags
{
	public:
		// Constructor
		CMakeTags(CString Root, IMakeTags *pTags);
		
		// Destructor
		~CMakeTags(void);

		// Operations
		void WarnRename(void);

		// Init Data
		CInitData & GetInitData(void);

		// Folders
		CString NewFolder(CString Folder);
		CString NewFolder(CString Class, CString Folder);
		void    EndFolder(void);

		// Constants
		void EmitRealC(PCTXT pName, PCTXT pLabel, float   nValue, int nDP);
		void EmitSintC(PCTXT pName, PCTXT pLabel, int     nValue);
		void EmitFlagC(PCTXT pName, PCTXT pLabel, BOOL    fValue);
		void EmitTextC(PCTXT pName, PCTXT pLabel, CString Value);

		// Mapped Tags
		void EmitRealM(PCTXT pName, PCTXT pLabel, CString Addr, int nDP);
		void EmitRealM(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax);
		void EmitSintM(PCTXT pName, PCTXT pLabel, CString Addr);
		void EmitSintM(PCTXT pName, PCTXT pLabel, CString Addr, int nMin, int nMax);
		void EmitWintM(PCTXT pName, PCTXT pLabel, CString Addr);
		void EmitFlagM(PCTXT pName, PCTXT pLabel, CString Addr);
		void EmitEnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum);
		void EmitFnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum);
		void EmitEnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1);
		void EmitFnumM(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1);
		void EmitTextM(PCTXT pName, PCTXT pLabel, CString Addr, UINT uLen);

		// Special Tags
		void EmitTimeM(PCTXT pName, PCTXT pLabel, CString Addr, UINT Fmt);
		void EmitTimeW(PCTXT pName, PCTXT pLabel, CString Addr, UINT Fmt);
		void EmitTimeA(PCTXT pName, PCTXT pLabel, CString Addr, UINT Fmt, UINT uSize);
		void EmitInetM(PCTXT pName, PCTXT pLabel, CString Addr);
		void EmitEnetM(PCTXT pName, PCTXT pLabel, CString Addr);
		void EmitTrend(PCTXT pName, PCTXT pLabel, CString Addr, int nDP);

		// Writable Tags
		void EmitRealW(PCTXT pName, PCTXT pLabel, CString Addr, int nDP);
		void EmitRealW(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax);
		void EmitSintW(PCTXT pName, PCTXT pLabel, CString Addr);
		void EmitSintW(PCTXT pName, PCTXT pLabel, CString Addr, int nMin, int nMax);
		void EmitFlagW(PCTXT pName, PCTXT pLabel, CString Addr);
		void EmitEnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum);
		void EmitFnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum);
		void EmitEnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1);
		void EmitFnumW(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1);
		void EmitTextW(PCTXT pName, PCTXT pLabel, CString Addr, UINT uLen);

		// Arrays
		void EmitRealA(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, UINT uSize, BOOL fWrite);
		void EmitRealA(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax, UINT uSize, BOOL fWrite);
		void EmitSintA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize, BOOL fWrite);
		void EmitSintA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize, int nMin, int nMax, BOOL fWrite);
		void EmitEnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum, UINT uSize, BOOL fWrite);
		void EmitWintA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize);
		void EmitEnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1, UINT uSize, BOOL fWrite);
		void EmitFnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum, UINT uSize, BOOL fWrite);
		void EmitFnumA(PCTXT pName, PCTXT pLabel, CString Addr, CString Val0, CString Val1, UINT uSize, BOOL fWrite);
		void EmitTextA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uLen, UINT uSize, BOOL fWrite);
		void EmitInetA(PCTXT pName, PCTXT pLabel, CString Addr, UINT uSize, BOOL fWrite);

		// Write Backs
		void EmitRealX(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, CString Code);
		void EmitRealX(PCTXT pName, PCTXT pLabel, CString Addr, int nDP, double nMin, double nMax, CString Code);
		void EmitFnumX(PCTXT pName, PCTXT pLabel, CString Addr, CString Enum, CString Code);

		// Lookups
		void EmitLookup(PCTXT pName, PCTXT pLabel, CUIntArray const &Index);
		void EmitLookup(PCTXT pName, PCTXT pLabel, CUIntArray const &Index, CStringArray const &Value, CString Default);

	protected:
		// Data Members
		CString          m_Root;
		IMakeTags      * m_pTags;
		CString          m_Folder;
		CTree  <CString> m_Check;
		CArray <CString> m_Delta;
		CTree  <CString> m_Words;

		// Implementation
		CString Const(int n);
		CString Const(double r);
		CString Const(CString s);
		CString ConstString(CString s);
		CString Valid(CString Name);
		void    CheckName(CString &Name);
		void    LoadKeywordMap(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

const UINT STATIC  = 0;
const UINT DYNAMIC = 1;

const int NUM_PHASES       = 50;
const int NUM_SEQ_SEGMENTS = 64;
const int NUM_SPP_SEGMENTS = 50;
const int NUM_SPS_SEGMENTS = 50;

//////////////////////////////////////////////////////////////////////////
//
// Common Enumerations
//

/*const CString Hc900AiBlockstatusEnum = L"Good|T/C Warning|T/C Failing|CJ Warning|Clamp Low|Clamp High|Burnout Failure|Under Range|Over Range|Failed to Convert|CJ High Temperature|CJ Failure|Forced|Bad Channel|No Comm|Disabled|Wrong Firmware Rev|No Channel Available|Sensor & Range Type Not Available,18|Over Current,19|Remote CJ Failure|Remote CJ Low Temperature|Remote CJ High Temperature|Open Wire Detection,120|Short Circuit Detection,121|EC1,122|EC2,123|Read Back Fail,124|EC3,125|Input Compare Fail,126|EC4,127|EC5,128|EC6,129|EC7,130|EC8,131|EC9,132|EC10,133|EC11,134|EC12,135|EC13,136|EC14,137|EC15,138|EC16,139|EC17,140";*/
const CString Hc900AlternatorDeviceEnum = L"DNR|DR";
const CString Hc900AlternatorInputStatusEnum = L"Off|On|Disabled";
const CString Hc900AlternatorModeEnum = L"Direct|Rotary|FOFO|Fixed";
const CString Hc900AlternatorOutputStatusEnum = L"Off|On|Disabled|Blank";
const CString Hc900AlternatorStatusEnum = L"Enabled|Disabled|Low Capacity";
const CString Hc900AMBiasAlarmTypeEnum = L"No Alarm|Pv High|Pv Low|Out High,7|Out Low,8";
const CString Hc900AutoManEnum = L"Auto|Manual";
const CString Hc900AutoManualEnum = L"Auto|Manual|IMan|Lo|Auto|Manual|IMan|Lo";
const CString Hc900CalendarDaysofWeekEnum = L"Sun,32|Mon,33|Tue,34|Wed,35|Thu,36|Fri,37|Sat,38";
const CString Hc900CalendarEventTypeEnum = L"Disable|Five Day Week|Seven Day Week|Day of Week|Monthly|Yearly";
const CString Hc900CalendarMonthEnum = L"Off|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec";
const CString Hc900CalendarSPDaysofWeekEnum = L"Sun,1|Mon,2|Tue,3|Wed,4|Thu,5|Fri,6|Sat,7";
const CString Hc900CalibrateRefTypeEnum = L"Reserved|Millivolts for AIs|Milliamps for AOs|Ohms|Degrees Celsius for CJs|Invalid Rack,252|Invalid Seq,253|Invalid Module,254|Invalid Channel,255";
const CString Hc900CalibrateRequestEnum = L"Reserved|Select Zero Percent Input|Calibrate Zero Percent Input|Select Hundred Percent Input|Calibrate Hundred Percent Input|Save|Restore Factory AI Calibration|Select Zero Percent Output|Calibrate Zero Percent Output|Select Hundred Percent Output|Calibrate Hundred Percent Output|Select CJ Input|Calibrate CJ Input|Restore Factory AO Calibration|Restore Factory CJ Calibration|Return to Ready State,255";
const CString Hc900CalibrationStatusEnum = L"Ready,0|Done,1|Connect AI 0% Reference,10|0% AI Cal Input,11|Connect AI 100% Reference,12|100% AI Cal Input,13|Save AI Calibration,14|AI Cal Failed,15|Restore AI Input,20|Restore AI Failed,21|Measure CJ Temperature & Enter,30|CJ Cal Input,31|CJ Cal Failed,32|Save CJ Calibration,33|Measure AO 0% Output & Enter,40|0% AO Output,41|Measure AO 100% Output & Enter,42|100% AO Output,43|AO Cal Failed,44|Save AO Cal,45|Restore AO Output,50|Restore AO Failed,51|Restore CJ Input,60|Restore CJ Failed,61|Invalid,255";
const CString Hc900ControlAlgorithmEnum = L"PID A|PID B|Duplex A|Duplex B";
const CString Hc900ControllerModeChangeEnum = L"None|Cold Start|Run|Offline|Program";
const CString Hc900ControllerModeEnum = L"Fault|Program|Run|Offline|Run Lock|Program Lock";
const CString Hc900ControllerSystemDiagnosticEnum = L"Good|Forced Output|Invalid Config|Switch Fault|No Master Port|RSM Switch Mismatch|No RSM Module|Bad RSM Switch (Lead)|Bad RSM Switch (Resv)|Invalid Change of Mode|Safety Config mismatch";
const CString Hc900CpuDiagnosticEnum = L"Good|Address Error|Prefetch Error|Data Abort Error|SW Interrupt Error|Undefined Instruction Error|Watchdog Error|Vector Error";
const CString Hc900DeviceControlStateEnum = L"Not Used|Ready|Prestart|Starting|Running|Stopping|Disabled|Failed";
const CString Hc900DeviceTypeEnum = L"UMC800|USF820|HC950|HC930|HC970R|HC970|HC920|HC921|HC975|HC950S|HC930S|HC970S|HC975S";
const CString Hc900DisabledEnabledEnum = L"Disabled|Enabled";
const CString Hc900DisabledOutputsAltTimeEnum = L"Disabled Outputs|Alt Time";
const CString Hc900DisableEnableEnum = L"Disable|Enable";
const CString Hc900DonotwriteWriteEnum = L"Do Not Write|Write";
const CString Hc900DoubleRegisterFormatEnum = L"FP B|FP LB|FP BB|FP L";
const CString Hc900EthernetPortProtocolEnum = L"Unknown,0|Modbus TCP,4";
const CString Hc900FeedbackStateEnum = L"State0|State1|State2|State3|State4|State5|State6|State7|State8";
const CString Hc900FSSStateEnum = L"State1,1|State2,2|State3,3|State4,4";
const CString Hc900GoodErrorEnum = L"Good|Error";
const CString Hc900GoodLowEnum = L"Good|Low";
const CString Hc900HoaCurrentStateEnum = L"Off|Hand|Auto|Bypass";
const CString Hc900HoaNewStateEnum = L"No Change|Off|Hand|Auto";
const CString Hc900HoaSourceEnum = L"Local|Remote|Local/Remote";
const CString Hc900InactiveActiveEnum = L"Inactive|Active";
const CString Hc900IOCommLinkDiagnosticEnum = L"Good|Comm Link Failure|Hardware Failure";
const CString Hc900IoRackCommPortDiagnosticEnum = L"Good|Data Link Failure|Hardware Failure|Port AB Cable Mismatch|Protocol Mismatch";
const CString Hc900LocalRemoteEnum = L"RSP Auto,0|RSP Man,1|LSP Auto,4|LSP Man,5";
const CString Hc900LocalValueRemoteValueEnum = L"Local Values|Remote Values";
const CString Hc900LoopAccutuneIIItypeEnum = L"Disable|Cycle Tuning|SP Tuning";
const CString Hc900LoopAccutuneStatusEnum = L"Not Ready|Ready|Tune Running|ID Failure|SP Error|Gain Error|Output Error|PV ADT Tuning,8|Abort,9";
const CString Hc900LoopAccutuneStatusVersion1Enum = L"Disable|Demand";
const CString Hc900LoopDuplexTuningEnum = L"Manual|Automatic|Disable";
const CString Hc900LoopGainPropBandEnum = L"Gain|Prop Band";
const CString Hc900LoopHighOutputLimitStatusEnum = L"Off|On";
const CString Hc900LoopMinutesRpmEnum = L"Minutes|RPM";
const CString Hc900LoopModeEnum = L"RSP Auto|RSP Man|RSP IMan|RSP Lo|LSP Auto|LSP Man|LSP IMan|LSP Lo";
const CString Hc900LoopPvAdaptiveTuningEnum = L"Disable|Enable";
const CString Hc900LoopRatioBiasEnum = L"None|Local Bias|Remote Bias";
const CString Hc900LoopSwitchTuneSetEnum = L"Tune Set 1|Tune Set 2";
const CString Hc900LoopTuningCriteriaEnum = L"Normal|Fast";
const CString Hc900LspRspEnum = L"LSP|RSP";
const CString Hc900LspRspStateEnum = L"RSP|RSP|RSP|RSP|LSP|LSP|LSP|LSP";
const CString Hc900MemoryDiagnosticEnum = L"Good|Battery 5 Day Warning|Low Battery|Flash Error|Batt Rear 5 day Warning|Rear Battery Low|Both Batt 5 day warning|Both Batteries Low";
const CString Hc900Method1Method2Enum = L"Method 1|Method 2";
const CString Hc900ModuleConfTypeEnum = L"No Module|Analog Input|Analog Output|Digital Input|Digital Output|Pulse Input|Quadrature Input|Frequency Input|Pulse Output|Universal IO|Redundant UIO|Spare21";
const CString Hc900ModuleDiagnosticEnum = L"Good|Hi CJ Temp|Module Mismatch|Missing / No Comm|Bad Channel|Bad Module|No Factory Data|Calibration Error|Invalid FW|Watchdog Error|Field Supply Voltage Fault|Internal Power Failure|OverTemperature|Field OverVoltage|Field OverCurrent|Module Diag Fail|Reserved16|Reserved17";
const CString Hc900ModulePhyTypeEnum = L"No Module|Universal AI 8-Channel|Low-Level AI 16-Channel|High-Level AI 8-Channel|Current AO 4-Channel|Voltage AO 4-Channel|Contact DI 16-Channel|Vac 240 DI 16-Channel|Vdc 24 DI 16-Channel|Low Relay DO 8-Channel|High Relay DO 4-Channel|Vac 240 DO 8-Channel|Vdc 24 DO 16-Channel|Vdc 24 DO 32-Channel|Vdc 24 DI 32-Channel|High-Level AI 16-Channel|PFQ 4-Channel|High-Level AO 8-Channel|High-Level AO 16-Channel|240Vac/125Vdc DI 16-Ch|UIO 16-Channel|UIO Safety 16-Channel|Spare22";
const CString Hc900NetworkPortStatusEnum = L"Good|Network Setup Error|No IP Address|DHCP Failure|Hardware Failure";
const CString Hc900NooverrideOverrideEnum = L"No Override|Override";
const CString Hc900NormalAdvanceEnum = L"Normal|Advance";
const CString Hc900NormalAlarmEnum = L"Normal|Alarm";
const CString Hc900NormalClearEnum = L"Normal|Clear";
const CString Hc900NormalPressedEnum = L"Normal|Pressed";
const CString Hc900NormalResetEnum = L"Normal|Reset";
const CString Hc900NormalSaveEnum = L"Normal|Save";
const CString Hc900NormalStartEnum = L"Normal|Start";
const CString Hc900NormalUseAlternateEnum = L"Normal|Use Alternate";
const CString Hc900NoYesEnum = L"No|Yes";
const CString Hc900OfflineOnlineEnum = L"Offline|Online";
const CString Hc900OffOnEnum = L"Off|On";
const CString Hc900OffRunEnum = L"Off|Run";
const CString Hc900OkFailedEnum = L"OK|Failed";
const CString Hc900OkOverrangeEnum = L"OK|Overrange";
const CString Hc900OnOffEnum = L"Off|On";
const CString Hc900PeerStatusEnum = L"Good|Application Error|Setup Error|Peer Fail|Port Fail|Not Started";
const CString Hc900PortBaudRateEnum = L"Bps9600,0|Bps19200,1|Bps38400,2|Bps57600,5|Bps1200,6|Bps2400,7|Bps4800,8|Bps115200,9";
const CString Hc900PortDiagnosticEnum = L"Good|Application Error|Data Link Error|PIO Module Failure|PIO Module Missing|Hardware Failure";
const CString Hc900PortStatusEnum = L"Good|No Board|Requires Setup|Offline|Online|PIO Module Failure|PIO Module Missing|No Slave Blocks|Program Mode|ELN Slave|Scanning Slaves";
const CString Hc900PpoInputTypeEnum = L"1000 Ohms|4 to 20 mA,70|0 to 20 mA,71|0 to 1 V,79|0 to 5 V,81|Slidewire 250 to 1250 Ohms,90|Slidewire < 250 Ohms,91|Slidewire 1250 to 4000 Ohms,92|Slidewire 4000 to 6500 Ohms,93";
const CString Hc900RackIODiagnosticEnum = L"Good|Module Error|Module High Temp|Failure|No Communication|Bad Version";
const CString Hc900RampOverrideEnum = L"None|High|Low";
const CString Hc900RedundancyCpuPositionEnum = L"CPU is Missing|CPU A|CPU B";
const CString Hc900RedundancyDiagnosticEnum = L"Good|Rack1|Rack2|Rack3|Rack4|Rack5|Rack6|Rack7|Rack8|Rack9|Rack10|Lead CPU|Reserve CPU|Serial S1|Serial S2|Network E1|Network E2|Scanner Link|Redundancy Link|Multiple Racks|Rack11|Rack12";
const CString Hc900RedundancyStatusEnum = L"Good|No RSM Module Detected|RSM Switch is Bad|IO Comm Error On Reserve|Database Not Synchronized|Invalid Config|Firmware Ver Mismatch";
const CString Hc900RedundancySystemStatusEnum = L"Good|Forced Output|Invalid Config|RSM Switch Failure|No Master Port|RSM Switch Mismatch|No RSM Module Detected|Bad RSM Switch Lead|Bad RSM Switch Reserve";
const CString Hc900RequestStartStopEnum = L"Stop|Start";
const CString Hc900ReserveStatusEnum = L"Reserve Not Available|Reserve Available";
const CString Hc900ReverseDirectEnum = L"Reverse|Direct";
const CString Hc900RtcDiagnosticEnum = L"Good|Not Programmed|Bad Data|Programming Failure|Read Failure";
const CString Hc900SaveDatabaseToFlashStatusEnum = L"DB Done|DB in Progress|DB Not Available|DB Fail";
const CString Hc900SequencerDrumStateEnum =  L"Invalid|Ready|Run|Hold|Stop|Disabled";
const CString Hc900SequencerStateEnum = L"Invalid|Reset|Run|Hold";
const CString Hc900SerialPortParityEnum = L"None|Odd|Even";
const CString Hc900SerialPortProtocolEnum = L"Unknown|ELN|Modbus Master|Modbus Slave Multi|Modbus Slave PTP,5|Modbus Slave Modem,6|MB Master Advanced,7";
const CString Hc900SerialPortStopBitsEnum = L"One|Two";
const CString Hc900SetpointProgrammerAdvanceEnum = L"Normal|Advance";
const CString Hc900SetpointProgrammerGuarSoakTypeEnum = L"None|Per Segment|All Soaks|All Segments";
const CString Hc900SetpointProgrammerNewStateEnum = L"Invalid|Reset|Run|Hold";
const CString Hc900SetpointProgrammerSegTypeEnum = L"Soak|Ramp";
const CString Hc900SetpointProgrammerStateEnum = L"Invalid|Ready|Run|Hold|GHold|Stop|Disabled";
const CString Hc900SetpointProgrammerTimeRateEnum = L"Rate|Time";
const CString Hc900SetpointProgrammerTimeUnitsEnum = L"Invalid|Reset|Run|Hold";
const CString Hc900SetpointSchedulerGuarSoakTypeEnum = L"Off|Low|High|High/Low";
const CString Hc900SetpointSchedulerNewStateEnum = L"Invalid|Reset|Run|Hold";
const CString Hc900SetpointSchedulerStateEnum = L"Invalid|Ready|Run|Hold|GHold|Stop|Disabled";
const CString Hc900SetpointSchedulerTimeUnitsEnum = L"Hours|Minutes";
const CString Hc900SlaveCommQualityEnum = L"None|Bad|Good";
const CString Hc900StageInterlockEnum = L"None|Previous|Next|Both|Error";
const CString Hc900StageOutputStatusEnum = L"Off|On|Disabled";
const CString Hc900StageOverrideEnum = L"None|On|Off";
const CString Hc900UnitsTypeEnum = L"None|DegF|PSI|CFM";
const CString Hc900UpDownEnum = L"Up|Down";
const CString Hc900AlarmTypeEnum = L"NoAlarm|PVHigh|PVLow|DevHigh|DevLow|SPHigh|SPLow|OutHigh|OutLow";
const CString Hc900PPOCalibrationStatusEnum = L"Ready,0|Done,1|Wait...Motor Moving To 0% Position,70|Please Move Motor To 0% Position,71|Motor Is At 0% Position,72| Wait...Calculating 0% Feedback Value,73| 0% Feedback Value Captured,74|Wait...Motor Moving To 100% Position,75|Please Move Motor To 100% Position,76|Motor Is At 100% Position,77|Wait...Calculating 100% Feedback Value,78|100% Feedback Value Captured,79|Wait...Calculating Motor Speed,80|Motor Speed Calibration Complete,81|Wait...Saving Calibration Values,82|Calibration Values Saved,83|Calibration Completed,84|Calibration Failed,85|Calibration Failed - Bad AI,86|Calibration Failed - Bad Feedback,87|Calibration Failed - Wrong Ai Version,88|Invalid,255";
const CString Hc900CalModeEnum = L"Ready|Manual|Semi Auto|Auto";
const CString Hc900SetpointProgrammerTypeEnum = L"Initial|Target";

// End of File

#endif
