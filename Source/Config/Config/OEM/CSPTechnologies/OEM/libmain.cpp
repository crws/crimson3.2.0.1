
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Resource DLLs
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load OEM\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading OEM\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// OEM Entry Points
//

CString DLLAPI OemGetCompany(void)
{
	return L"CS&P Technologies";
	}

CString DLLAPI OemGetModels(void)
{
	return L"G07,G10,G10R,G15"					L","
	       L"CO04,CO07,CO07EQ,CO10,CO10EV"				L","
	       L"CA04,CA07,CA07EQ,CA10,CA10EV,CA15"			L","
	       L"DA30DWQ,DA30DWV,DA30DWX"				L","
	       L"DA10D"
	       ;
	}

WORD DLLAPI OemGetUsbBase(void)
{
	return 0x0000;
	}

BOOL DLLAPI OemGetPair(UINT n, CString &a, CString &b)
{
	static PCTXT Subst[][2] = {

		// Core Strings

		{ L"Red Lion Controls Inc",		L"CS&P Technologies",		},
		{ L"Red Lion Controls",			L"CS&P Technologies",		},
		{ L"Red Lion",				L"CS&P Technologies",		},
		{ L"Crimson 3.1",			L"CS&P Designer 3.1",		},
		{ L"Crimson 3.0",			L"CS&P Designer 3",		},
		{ L"Crimson 3",				L"CS&P Designer 3",		},
		{ L"Crimson device",			L"CS&P Designer device",	},
		{ L"Crimson",				L"CS&P Designer",		},

		// Part Numbers

		{ L"G07",				L"HMI-G07"			},
		{ L"G10R",				L"HMI-G10"			},
		{ L"G10",				L"HMI-G10"			},
		{ L"G15",				L"HMI-G15"			},

//		{ L"G10R",				L"G10 SVGA"			},

		{ L"CO04",				L"CR1000-04000"			},
		{ L"CO07",				L"CR1000-07000"			},
		{ L"CO10",				L"CR1000-10000"			},

		{ L"CA04C",				L"CR3010-04000"			},
		{ L"CA07C",				L"CR3010-07000"			},
		{ L"CA10C",				L"CR3010-10000"			},
		{ L"CA15C",				L"CR3010-15000"			},

		{ L"CA04",				L"CR3000-04000"			},
		{ L"CA07",				L"CR3000-07000"			},
		{ L"CA10",				L"CR3000-10000"			},
		{ L"CA15",				L"CR3000-15000"			},

		{ L"DA30DWQ",				L"DA30D (WQVGA)"		},
		{ L"DA30DWV",				L"DA30D (WVGA)"			},
		{ L"DA30DWX",				L"DA30D (WXVGA)"		},

		// File Extensions

		{ L".cd31",				L".cs31",			},
		{ L".cd3",				L".cs3",			},
		{ L".ci3",				L".ci31",			},

		};

	if( n < elements(Subst) ) {

		a = Subst[n][0];
		b = Subst[n][1];

		return TRUE;
		}

	return FALSE;
	}

BOOL DLLAPI OemGetFeature(CString &f, BOOL d)
{
	if( f == L"BuildNotes"   ) return FALSE;
	if( f == L"FuncHelp"     ) return TRUE;
	if( f == L"Import"	 ) return TRUE;
	if( f == L"Localize"     ) return FALSE;
	if( f == L"OPCProxy"     ) return TRUE;
	if( f == L"PoweredBy"    ) return TRUE;
	if( f == L"Registration" ) return FALSE;
	if( f == L"SMS"          ) return TRUE;
	if( f == L"Support"      ) return FALSE;
	if( f == L"Update"       ) return FALSE;
	if( f == L"ShowSplash"   ) return FALSE;
	if( f == L"Ethernet"	 ) return TRUE;
	if( f == L"USBHost"	 ) return FALSE;
	if( f == L"MPI"		 ) return TRUE;

	return d;
	}

CString DLLAPI OemGetOption(CString const &f, CString const &d)
{
	return d;
	}

BOOL DLLAPI OemAllowDriver(UINT uIdent, BOOL fDefault)
{
	switch( uIdent ) {

		// Caterpillar drivers
		case 0x4039:
		case 0x403D:
		case 0x4041:
		case 0x404F:
		case 0x407E:
			return FALSE;

		// CCP Driver
		case 0x40E0: return FALSE;
		}

	return fDefault;
	}

DWORD DLLAPI OemGetColor(CString &c, DWORD d)
{
	return d;
	}

// End of File
