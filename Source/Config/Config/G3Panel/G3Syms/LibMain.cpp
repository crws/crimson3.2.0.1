
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load G3Syms\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading G3Syms\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Linking Function
//

void G3Syms_Load(void)
{
	}

// End of File
