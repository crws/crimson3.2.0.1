
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbols Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//
//

#define IDS_BASE                0x4000
#define IDS_AIR_CONDITIONING    0x4000 /* NOT USED */
#define IDS_ARCHITECTURAL       0x4001 /* NOT USED */
#define IDS_ARROWS              0x4002 /* NOT USED */
#define IDS_ASHRAE_CONTROLS     0x4003 /* NOT USED */
#define IDS_ASHRAE_DUCTS        0x4004 /* NOT USED */
#define IDS_ASHRAE_PIPING       0x4005 /* NOT USED */
#define IDS_BASIC_SHAPES        0x4006 /* NOT USED */
#define IDS_BLOWERS_ETC         0x4007 /* NOT USED */
#define IDS_BOILERS             0x4008 /* NOT USED */
#define IDS_BUILDINGS           0x4009 /* NOT USED */
#define IDS_CHEMICAL            0x400A /* NOT USED */
#define IDS_COMPUTER_HARDWARE   0x400B /* NOT USED */
#define IDS_COMPUTER_KEYS       0x400C /* NOT USED */
#define IDS_CONTAINERS          0x400D /* NOT USED */
#define IDS_CONTROLLERS         0x400E /* NOT USED */
#define IDS_CONVEYORS_BELT      0x400F /* NOT USED */
#define IDS_CONVEYORS_MISC      0x4010 /* NOT USED */
#define IDS_CONVEYORS_SIMPLE    0x4011 /* NOT USED */
#define IDS_DUCTS               0x4012 /* NOT USED */
#define IDS_D_PUSHBUTTONS_ETC   0x4013 /* NOT USED */
#define IDS_ELECTRICAL          0x4014 /* NOT USED */
#define IDS_FINISHING_OLD_AND   0x4015 /* NOT USED */
#define IDS_FLEXIBLE_TUBING     0x4016 /* NOT USED */
#define IDS_FLOW_METERS         0x4017 /* NOT USED */
#define IDS_FOOD                0x4018 /* NOT USED */
#define IDS_GADGET_ACTION       0x4019 /* NOT USED */
#define IDS_GADGET_BUTTONS      0x401A /* NOT USED */
#define IDS_GADGET_BUTTONS_2    0x401B /* NOT USED */
#define IDS_GADGET_BUTTONS_3    0x401C /* NOT USED */
#define IDS_GADGET_LIGHTS       0x401D /* NOT USED */
#define IDS_GADGET_LIGHTS_2     0x401E /* NOT USED */
#define IDS_GADGET_PANEL        0x401F /* NOT USED */
#define IDS_GADGET_SWITCHES     0x4020 /* NOT USED */
#define IDS_GADGET_SWITCHES_2   0x4021 /* NOT USED */
#define IDS_GADGET_TEXTURES     0x4022 /* NOT USED */
#define IDS_GENERAL_MFG         0x4023 /* NOT USED */
#define IDS_HEATING             0x4024 /* NOT USED */
#define IDS_HVAC                0x4025 /* NOT USED */
#define IDS_ICONS_AND_BITMAPS   0x4026 /* NOT USED */
#define IDS_INDUSTRIAL_MISC     0x4027 /* NOT USED */
#define IDS_INTERNATIONAL       0x4028 /* NOT USED */
#define IDS_ISA_SYMBOLS         0x4029 /* NOT USED */
#define IDS_ISA_SYMBOLS_D       0x402A /* NOT USED */
#define IDS_LABORATORY          0x402B /* NOT USED */
#define IDS_LOGISTICS           0x402C /* NOT USED */
#define IDS_MACHINING           0x402D /* NOT USED */
#define IDS_MAPS_AND_FLAGS      0x402E /* NOT USED */
#define IDS_MATERIAL_HANDLING   0x402F /* NOT USED */
#define IDS_MEDICAL             0x4030 /* NOT USED */
#define IDS_MILITARY            0x4031 /* NOT USED */
#define IDS_MINING              0x4032 /* NOT USED */
#define IDS_MISC_PIPES          0x4033 /* NOT USED */
#define IDS_MISC_SYMBOLS        0x4034 /* NOT USED */
#define IDS_MISC_SYMBOLS_2      0x4035 /* NOT USED */
#define IDS_MIXERS              0x4036 /* NOT USED */
#define IDS_MOTORS              0x4037 /* NOT USED */
#define IDS_NATURE              0x4038 /* NOT USED */
#define IDS_NETWORKING          0x4039 /* NOT USED */
#define IDS_NUCLEAR             0x403A /* NOT USED */
#define IDS_OPERATOR            0x403B /* NOT USED */
#define IDS_PANELS              0x403C /* NOT USED */
#define IDS_PEOPLE              0x403D /* NOT USED */
#define IDS_PIPES               0x403E /* NOT USED */
#define IDS_PLANT_FACILITIES    0x403F /* NOT USED */
#define IDS_POWER               0x4040 /* NOT USED */
#define IDS_PROCESS_COOLING     0x4041 /* NOT USED */
#define IDS_PROCESS_HEATING     0x4042 /* NOT USED */
#define IDS_PULP_PAPER          0x4043 /* NOT USED */
#define IDS_PUMPS               0x4044 /* NOT USED */
#define IDS_SAFETY              0x4045 /* NOT USED */
#define IDS_SCALES              0x4046 /* NOT USED */
#define IDS_SEGMENTED_PIPES     0x4047 /* NOT USED */
#define IDS_SENSORS             0x4048 /* NOT USED */
#define IDS_TANKS               0x4049 /* NOT USED */
#define IDS_TANK_CUTAWAYS       0x404A /* NOT USED */
#define IDS_TEXTURES            0x404B /* NOT USED */
#define IDS_VALVES              0x404C /* NOT USED */
#define IDS_VEHICLES            0x404D /* NOT USED */
#define IDS_WATER_WASTEWATER    0x404E /* NOT USED */
#define IDS_WIRE_CABLE          0x404F /* NOT USED */

// End of File

#endif
