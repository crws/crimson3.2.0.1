
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SymFactCategory_HPP

#define INCLUDE_SymFactCategory_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SymFactParser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSymFactSymbol;

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Category
//

class CSymFactCategory : public CSymFactParser
{
	public:
		// Constructor
		CSymFactCategory(void);

		// Destructor
		~CSymFactCategory(void);

		// Indexing
		CSymFactSymbol * const operator [] (UINT n) const;

		// Attributes
		CString GetDesc(void) const;
		UINT    GetSymCount(void) const;
		UINT    FindHandle(UINT uHandle) const;

		// Operations
		BOOL ParseCat1(UINT uCat, PCBYTE pCat);
		BOOL ParseCat3(UINT uCat, char *pText);

	protected:
		// Index
		CZeroMap <UINT, CSymFactSymbol *> m_Index;

		// Data Members
		CSymFactSymbol * m_pSyms;
		UINT             m_uCount;
		CString	         m_Desc;
		UINT	         m_uType;
		UINT	         m_uVer;

		// Overridables
		void OnParseTag(char const *pName, char const *pData);

		// Implementation
		void AddToIndex(CSymFactSymbol *pSym);
	};

// End of File

#endif
