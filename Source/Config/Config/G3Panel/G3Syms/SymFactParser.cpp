
#include "intern.hpp"

#include "SymFactParser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Parser
//

// Operations

void CSymFactParser::ParseLine(char *pLine)
{
	char *pOpen = strchr(pLine, '<');

	if( pOpen ) {

		char *pName = pOpen + 1;

		while( isspace(*pName) ) {

			pName++;
			}

		char *pBody = strchr(pName, ' ');

		if( pBody ) {

			*pBody = 0;

			OnParseTag("Start", pName);

			char *pName = pOpen + 1;

			char *pScan = pBody + 1;

			for(;;) {

				while( isspace(*pScan) ) {

					pScan++;
					}

				if( *pScan ) {

					char *pEqual = strchr(pScan, '=');

					if( pEqual ) {

						*pEqual = 0;

						char *pTagName  = pScan;

						char *pDataOpen = strchr(pEqual + 1, '\"');

						if( pDataOpen ) {

							char *pTagData   = pDataOpen + 1;

							char *pDataClose = strchr(pDataOpen + 1, '\"');

							*pDataClose = 0;

							OnParseTag(pTagName, pTagData);

							pScan = pDataClose + 1;

							continue;
							}
						}
					}

				break;
				}
	
			OnParseTag("End", pName);
			}
		}
	}

// End of File
