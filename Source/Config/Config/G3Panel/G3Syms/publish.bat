@echo off

rem %1 = config
rem %2 = build dir

echo Publishing...

xcopy /Y /D /R /I Cats\V3\NetToolKit.d*			    "%~2\Bin\Config\Win32\%1\SymFact"

xcopy /Y /D /R /I Cats\V3\SWTB.SymbolFactory.API.d* "%~2\Bin\Config\Win32\%1\SymFact"
