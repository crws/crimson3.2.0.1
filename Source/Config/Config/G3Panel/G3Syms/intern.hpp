
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3syms.hpp"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Startup and Shutdown
//

extern void Sym_Init(void);
extern void Sym_Term(void);

//////////////////////////////////////////////////////////////////////////
//								
// Symbol Factory Types
//

#define symBitmap   0
#define symTexture  1
#define symMetaFile 3
#define symShadable 4
#define symXaml     5

//////////////////////////////////////////////////////////////////////////
//								
// Crimson Image Types
//

#define typeBMP  1
#define typeTEX  2
#define typeGDIP 3
#define typeEMF  4
#define typeWMF  5
#define typeXAML 6

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Imports
//

#pragma warning (disable: 4278)

#define __PLACEMENT_NEW_INLINE

#import "cats/v3/SWTB.SymbolFactory.API.tlb" no_namespace named_guids
	
#undef __PLACEMENT_NEW_INLINE

// End of File

#endif
