
#include "intern.hpp"

#include "SymFactCategory.hpp"

#include "SymFactSymbol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Category
//

// Constructor

CSymFactCategory::CSymFactCategory(void)
{
	m_pSyms  = NULL;

	m_uCount = 0;

	m_uType  = NOTHING;

	m_uVer   = NOTHING;
	}

// Destructor

CSymFactCategory::~CSymFactCategory(void)
{
	delete [] m_pSyms;
	}

// Indexing

CSymFactSymbol * const CSymFactCategory::operator [] (UINT n) const
{
	AfxAssert(n < m_uCount);

	return n < m_uCount ? m_pSyms + n : NULL;
	}

// Attributes

CString CSymFactCategory::GetDesc(void) const
{
	return m_Desc;
	}

UINT CSymFactCategory::GetSymCount(void) const
{
	return m_uCount;
	}

UINT CSymFactCategory::FindHandle(UINT uHandle) const
{
	INDEX n = m_Index.FindName(uHandle);

	if( !m_Index.Failed(n) ) {

		CSymFactSymbol *pSym = m_Index.GetData(n);

		return pSym - m_pSyms;
		}

	AfxAssert(FALSE);

	return NOTHING;
	}

// Operations

BOOL CSymFactCategory::ParseCat1(UINT uCat, PCBYTE pCat)
{
	UINT uSlots = PCWORD(pCat + 46)[1];

	UINT uCount = 0;

	for( UINT uSlot = 0; uSlot < uSlots; uSlot++ ) {

		PCBYTE  pSlot = pCat + 84 + 32 * uSlot;

		PCDWORD pInfo = PCDWORD(pSlot + 1);

		if( pInfo[0] ) {

			if( m_uCount ) {

				INDEX n = m_Index.FindName(pInfo[0]);

				if( m_Index.Failed(n) ) {

					if( FALSE ) {

						AfxTrace(L"%-20s : Can't find %u\n", m_Desc, pInfo[0]);
						}

					CSymFactSymbol *pSym = m_pSyms + m_uCount++;

					pSym->ParseCat1(uCat, pCat, uSlot);

					AddToIndex(pSym);
					}
				else {
					CSymFactSymbol *pSym = m_Index.GetData(n);

					AfxAssert(pSym->GetHandle() == pInfo[0]);

					pSym->ParseCat1(uCat, pCat, uSlot);
					}
				}

			uCount++;
			}
		}

	if( uCount && !m_uCount ) {

		m_uCount = uCount;

		m_pSyms  = New CSymFactSymbol [ m_uCount ];

		uCount   = 0;

		for( UINT uSlot = 0; uSlot < uSlots; uSlot++ ) {

			PCBYTE  pSlot = pCat + 84 + 32 * uSlot;

			PCDWORD pInfo = PCDWORD(pSlot + 1);

			if( pInfo[0] ) {

				CSymFactSymbol *pSym = m_pSyms + uCount++;

				pSym->ParseCat1(uCat, pCat, uSlot);

				AddToIndex(pSym);
				}
			}
		}

	if( m_Desc.IsEmpty() ) {

		for( PCBYTE p = pCat + 5; *p; p++ ) {

			m_Desc += TCHAR(WORD(*p));
			}
		}

	return TRUE;
	}

BOOL CSymFactCategory::ParseCat3(UINT uCat, char *pText)
{
	char *pScan = pText;

	UINT  uLine = 0;

	for(;;) {

		char *pLineEnd = strchr(pScan, '\r');

		if( pLineEnd ) {

			while( isspace(*pLineEnd) ) {
					
				*pLineEnd++ = 0;
				}
			}

		if( uLine++ == 0 ) {

			ParseLine(pScan);

			// We allocate some extra space in case the cat 1
			// file that we'll load next has more symbols in it
			// than the cat 3 file. This happens with the Easter
			// Egss for example that were in 1 but not in 3.

			m_pSyms = New CSymFactSymbol [ m_uCount + 16 ];
			}
		else {
			if( uLine <= m_uCount + 1 ) {

				CSymFactSymbol *pSym = m_pSyms + uLine - 2;

				pSym->ParseCat3(uCat, pScan);

				AddToIndex(pSym);
				}
			else
				break;
			}

		if( pLineEnd ) {

			pScan = pLineEnd;

			continue;
			}

		break;
		}

	return TRUE;
	}

// Overridables

void CSymFactCategory::OnParseTag(char const *pName, char const *pData)
{	
	if( !strcmp(pName, "Count") ) { 

		m_uCount = strtoul(pData, 0, 10);

		return;
		}

	if( !strcmp(pName, "Description") ) { 

		m_Desc = pData;

		m_Desc.Replace(L"&amp;", L"&");

		return;
		}

	if( !strcmp(pName, "CategoryType") ) { 

		m_uType = strtoul(pData, 0, 10);

		return;
		}

	if( !strcmp(pName, "Version") ) { 

		m_uVer = strtoul(pData, 0, 10);

		return;
		}
	}

// Implementation

void CSymFactCategory::AddToIndex(CSymFactSymbol *pSym)
{
	AfxVerify(m_Index.Insert(pSym->GetHandle(), pSym));
	}

// End of File
