
#include "intern.hpp"

#include "SymFactSymbol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Base64 Decoding
//

#include "../G3Comms/HttpBase64.hpp"

#pragma  comment(lib, "G3Comms.lib")

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Symbol
//

// Static Data

static CFilename m_Path;

// Extern Data

extern IManagedInterface * g_pSymHelp;

// Constructor

CSymFactSymbol::CSymFactSymbol(void)
{
	m_pLine   = NULL;

	m_uCat    = NOTHING;

	m_uHandle = NOTHING;

	m_fShade  = FALSE;

	m_uType3  = NOTHING;

	m_uType1  = NOTHING;

	m_pData1  = NULL;

	m_uData1  = 0;

	m_fSave  = FALSE;

	m_Cache.m_Magic = 0;
	}

// Destructor

CSymFactSymbol::~CSymFactSymbol(void)
{
	if( m_fSave ) {

		SaveCache();
		}

	delete [] m_pLine;
	}

// Attributes

UINT CSymFactSymbol::GetHandle(void) const
{
	return m_uHandle;
	}

CString CSymFactSymbol::GetDesc(void) const
{
	return m_Desc;
	}

UINT CSymFactSymbol::GetType(BOOL fNew) const
{
	UINT const &uType = fNew ? m_uType3 : m_uType1;

	UINT const &uBack = fNew ? m_uType1 : m_uType3;

	return (uType < NOTHING) ? uType : uBack;
	}

BOOL CSymFactSymbol::CanShade(BOOL fNew) const
{
	return m_fShade;
	}

// Data Access

UINT CSymFactSymbol::GetData(PBYTE &pCopy)
{
	if( m_pData1 ) {

		pCopy = New BYTE [ m_uData1 ];

		memcpy(pCopy, m_pData1, m_uData1);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSymFactSymbol::GetData(CByteArray &Data)
{
	if( m_pData1 ) {

		Data.Empty();

		Data.Append(m_pData1, m_uData1);

		return TRUE;
		}

	return FALSE;
	}

// Rendering

HBITMAP CSymFactSymbol::GetBitmap(int &cx, int &cy)
{
	if( m_uType1 == typeBMP || m_uType1 == typeTEX ) {

		BITMAPINFO *pInfo = (BITMAPINFO *) m_pData1;

		UINT        uCols = GetColorCount(&pInfo->bmiHeader);

		PCBYTE      pBits = m_pData1 + sizeof(BITMAPINFO) + (uCols - 1) * 4;

		HBITMAP     hBits = CreateDIBitmap( CClientDC(NULL),
						    &pInfo->bmiHeader,
						    CBM_INIT,
						    pBits,
						    pInfo,
						    DIB_RGB_COLORS
						    );

		cx = pInfo->bmiHeader.biWidth;

		cy = pInfo->bmiHeader.biHeight;

		return hBits;
		}

	return NULL;
	}

HENHMETAFILE CSymFactSymbol::GetMetaFile(void)
{
	if( m_uType1 == typeEMF ) { 

		HENHMETAFILE hMeta = SetWinMetaFileBits( m_uData1,
							 m_pData1,
							 NULL,
							 NULL
							 );

		return hMeta;
		}

	return NULL;
	}

BOOL CSymFactSymbol::GetPng(UINT uMode, DWORD Fill, DWORD Back, CSize Size, BOOL fKeep, CByteArray &Data)
{
	if( g_pSymHelp ) {

		if( !m_Cache.m_Magic ) {

			LoadCache();
			}

		if( m_Cache.m_uMode == uMode ) {
			
			if( m_Cache.m_Fill == Fill && m_Cache.m_Back == Back ) {

				if( m_Cache.m_xSize == Size.cx && m_Cache.m_ySize == Size.cy ) {

					if( m_Cache.m_fKeep == fKeep ) {

						Data = m_Cache.m_Data;

						return TRUE;
						}
					}
				}
			}

		_bstr_t fill(CPrintf((Fill >> 24) ? "#%6.6X" : "", Fill & 0xFFFFFF));

		_bstr_t line(CPrintf((Fill >> 24) ? "#%6.6X" : "", 0xC0C0C0));

		_bstr_t back(CPrintf((Back >> 24) ? "#%6.6X" : "", Back & 0xFFFFFF));

		ImageData id;

		memset(&id, 0, sizeof(id));

		LONG lResult = -1;

		g_pSymHelp->raw_GetSingleSymbol( BSTR(m_pLine+2),
						 fill,
						 uMode,
						 back,
						 &id,
						 3,
						 TRUE, 
						 Size.cx,
						 Size.cy,
						 !fKeep,
						 line,
						 &lResult
						 );

		if( lResult == 1 ) {

			Data.Empty();

			Data.Append(PCBYTE(id.SymbolData->pvData), id.SymbolData->rgsabound[0].cElements);

			SafeArrayDestroy(id.SymbolData);

			m_Cache.m_uMode = uMode;
			m_Cache.m_Fill  = Fill;
			m_Cache.m_Back  = Back;
			m_Cache.m_xSize = Size.cx;
			m_Cache.m_ySize = Size.cy;
			m_Cache.m_fKeep = fKeep;
			m_Cache.m_Data  = Data;

			m_fSave = TRUE;

			return TRUE;
			}
		}

	return FALSE;
	}

// Operations

BOOL CSymFactSymbol::ParseCat1(UINT uCat, PCBYTE pCat, UINT uSlot)
{
	PCBYTE  pSlot   = pCat + 84 + 32 * uSlot;

	PCDWORD pInfo   = PCDWORD(pSlot + 1);

	if( m_uHandle == NOTHING ) {

		m_uHandle = pInfo[0];

		m_Desc    = PCSTR(pCat + pInfo[1]);

		FixCase(m_Desc);
		}
	else {
		if( FALSE ) {

			CString Desc;
		
			AfxAssert(m_uHandle == pInfo[0]);

			Desc = PCSTR(pCat + pInfo[1]);

			if( Desc != m_Desc ) {
			
				AfxTrace( L"%u : Name mismatch, category 1 is [%s], category 3 is [%s]\n",
					  m_uHandle,
					  Desc,
					  m_Desc
					  );
				}
			}
		}

	switch( *pSlot ) {

		case  symBitmap:   m_uType1 = typeBMP; break;
		case  symTexture:  m_uType1 = typeTEX; break;
		case  symMetaFile: m_uType1 = typeEMF; break;
		case  symShadable: m_uType1 = typeEMF; break;

		default: AfxAssert(FALSE);
		}

	m_fShade = (*pSlot == symShadable);

	m_pData1 = pCat + pInfo[2];

	m_uData1 = pInfo[3];
	
	m_uCat   = uCat;

	return TRUE;
	}

BOOL CSymFactSymbol::ParseCat3(UINT uCat, char *pText)
{
	// Poor man's BSTR implementation as _bstr_t is too slow.

	UINT nc = strlen(pText);

	m_pLine = New WORD [ nc + 4 ];

	UINT nw = 0;

	m_pLine[nw++] = LOWORD(2*nc);
	m_pLine[nw++] = HIWORD(2*nc);
	
	for( UINT n = 0; n < nc; n++ ) {

		m_pLine[nw++] = pText[n];
		}

	m_pLine[nw++] = 0;
	m_pLine[nw++] = 0;

	ParseLine(pText);

	m_uCat = uCat;

	return TRUE;
	}

// Overridables

void CSymFactSymbol::OnParseTag(char const *pName, char const *pData)
{
	// We use this nasty field identification method as this is
	// in the inner loop of the parser and we want to speed things
	// up as much as possible. We rely on the fact that we only
	// ever get name values of:
	//
	//	SymbolType
	//	SymbolHandle
	//	SymbolIndex
	//	Description
	//	DataSize
	//	Data
	//
	// We can therefore short-circuit all the all strcmp calls.

	if( pName[0] == 'S' ) {

		if( pName[6] == 'H' ) {

			m_uHandle = strtoul(pData, 0, 10);

			return;
			}

		if( pName[6] == 'T' ) {

			switch( pData[0] ) {

				case '0' + symBitmap:  m_uType3 = typeXAML; break;
				case '0' + symTexture: m_uType3 = typeXAML; break;
				case '0' + symXaml:    m_uType3 = typeXAML; break;

				default : AfxAssert(FALSE);
				}

			return;
			}

		return;
		}

	if( pName[0] == 'D' ) {

		if( pName[1] == 'e' ) {

			UINT uDecode = CHttpBase64::GetDecodeSize(pData);

			m_Desc = CString(' ', uDecode / 2);

			PBYTE pDecode = PBYTE(PCTXT(m_Desc));

			CHttpBase64::Decode(pDecode, pData);

			FixCase(m_Desc);
			}

		return;
		}
	}

// Implementation

UINT CSymFactSymbol::GetColorCount(BITMAPINFOHEADER *pHead)
{
	switch( pHead->biBitCount ) {

		case 24:
			return 0;

		case 32:
			return 0;
		}

	if( pHead->biClrUsed ) {

		return pHead->biClrUsed;
		}

	return 1 << pHead->biBitCount;
	}

void CSymFactSymbol::FixCase(CString &Name)
{
	Name.TrimBoth();

	PTXT pName = PTXT(PCTXT(Name));

	while( *pName ) {

		if( *pName == ' ' || *pName == '-' ) {

			if( *++pName ) {

				if( *pName == '(' ) {

					if( !*++pName ) {

						break;
						}
					}
				}

			*pName = wtoupper(*pName);
			}

		pName++;
		}
	}

BOOL CSymFactSymbol::LoadCache(void)
{
	FindPath();

	CFilename Cache = m_Path.WithName(CPrintf(L"sym.%2.2x.%8.8x.cpng", m_uCat, m_uHandle));

	HANDLE	  hFile = Cache.OpenReadSeq();

	if( hFile < INVALID_HANDLE_VALUE ) {

		DWORD uDone = 0;

		ReadFile(hFile, &m_Cache, offset(CCache, m_Data), &uDone, NULL);

		if( m_Cache.m_Magic == 'ICF4' ) {

			UINT uSize = GetFileSize(hFile, NULL) - uDone;

			m_Cache.m_Data.SetCount(uSize);

			ReadFile(hFile, PBYTE(m_Cache.m_Data.GetPointer()), uSize, &uDone, NULL);

			CloseHandle(hFile);

			return TRUE;
			}

		CloseHandle(hFile);
		}

	m_Cache.m_xSize = 0;

	m_Cache.m_ySize = 0;

	m_Cache.m_Magic = 1;

	return FALSE;
	}

BOOL CSymFactSymbol::SaveCache(void)
{
	FindPath();

	CFilename Cache = m_Path.WithName(CPrintf(L"sym.%2.2x.%8.8x.cpng", m_uCat, m_uHandle));

	HANDLE	  hFile = Cache.OpenWrite();

	if( hFile < INVALID_HANDLE_VALUE ) {

		DWORD uDone = 0;

		m_Cache.m_Magic = 'ICF4';

		WriteFile(hFile, &m_Cache, offset(CCache, m_Data), &uDone, NULL);

		WriteFile(hFile, m_Cache.m_Data.GetPointer(), m_Cache.m_Data.GetCount(), &uDone, NULL);

		CloseHandle(hFile);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSymFactSymbol::FindPath(void)
{
	if( m_Path.IsEmpty() ) {

		m_Path = afxModule->GetApp()->GetFolder(CSIDL_LOCAL_APPDATA, L"Symbols");

		return TRUE;
		}

	return FALSE;
	}

// End of File
