
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ruby_LoopRemover
	
#define	INCLUDE_Ruby_LoopRemover

//////////////////////////////////////////////////////////////////////////
//
// Intantiated Classes
//

#include "RubyPoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyPath;

//////////////////////////////////////////////////////////////////////////
//
// Loop Remover
//

class DLLAPI CRubyLoopRemover
{
	public:
		// Operations
		static void RemoveLoops(CRubyPath &figure);
		static void RemoveLoops(CRubyPath &output, CRubyPath const &figure);

	protected:
		// Box Structure
		struct bb
		{
			// Data Members
			number x1, y1;
			number x2, y2;

			// Intersection
			bool Intersects(bb const &b) const
			{
				return (y2 >= b.y1 && y1 <= b.y2 && x2 >= b.x1 && x1 <= b.x2);
				}

			// Loading
			void Set(CRubyPoint const &p1, CRubyPoint const &p2)
			{
				x1 = min(p1.m_x, p2.m_x);
				x2 = max(p1.m_x, p2.m_x);
				y1 = min(p1.m_y, p2.m_y);
				y2 = max(p1.m_y, p2.m_y);
				}
			};

		// Implementation
		static bool FindIntersection(CRubyPoint &i, CRubyPoint const &p1, CRubyPoint const &p2, CRubyPoint const &p3, CRubyPoint const &p4);
	};

// End of File

#endif
