
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyStroker_HPP
	
#define	INCLUDE_RubyStroker_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyPath;

class CRubyPoint;

class CRubyVector;

//////////////////////////////////////////////////////////////////////////
//
// Arrow Interface
//

interface IRubyArrow
{
	virtual void AddArrow(  CRubyPath & output,
				int	    style,
				int         side,
				CRubyPoint  const &p,
				CRubyVector const &u,
				CRubyVector const &n,
				number             w
				) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Stroker Object
//

class DLLAPI CRubyStroker : public IRubyArrow
{
	public:
		// End Caps
		enum EndStyle
		{
			endFlat   = 0,
			endSquare = 1,
			endRound  = 2,
			endPoint  = 3,
			endArrow  = 4,
			};

		// Arrow Types
		enum ArrowType
		{
			arrowNone        = 0,
			arrowSmall       = 100,
			arrowMedium      = 101,
			arrowLarge       = 102,
			arrowSmallBarb   = 103,
			arrowMediumBarb  = 104,
			arrowLargeBarb   = 105,
			arrowSmallOuter  = 106,
			arrowMediumOuter = 107,
			arrowLargeOuter  = 108,
			};

		// Join Styles
		enum JoinStyle
		{
			joinBevel = 0,
			joinMiter = 1,
			joinRound = 2
			};

		// Edge Mode
		enum EdgeMode
		{
			edgeCenter = 0,
			edgeInner  = 1,
			edgeOuter  = 2
			};

		// Constructor
		CRubyStroker(void);

		// Parameters
		void SetEndStyle(EndStyle end);
		void SetArrowStyle(IRubyArrow *pArrow, int arrow1, int arrow2);
		void SetArrowStyle(int arrow1, int arrow2);
		void SetJoinStyle(JoinStyle join);
		void SetEdgeMode(EdgeMode edge);
		void SetMiterLimit(int limit);

		// Operations
		void StrokeLoop(CRubyPath &output, CRubyPath const &figure, int sub, number w);
		void StrokeOpen(CRubyPath &output, CRubyPath const &figure, int sub, number w);

		// Arrow Data
		static number GetArrowLength(int style, number w);
		static int    GetArrowLength(int style);

	protected:
		// Data Members
		int          m_end;
		IRubyArrow * m_pArrow;
		UINT	     m_arrow1;
		UINT	     m_arrow2;
		int	     m_join;
		int	     m_edge;
		int	     m_limit;

		// Implementation
		void Stroke(CRubyPath &output, CRubyPath const &figure, int sub, number w, bool open);

		// Default Arrow
		void AddArrow(CRubyPath &output, int style, int side, CRubyPoint  const &p, CRubyVector const &u, CRubyVector const &n, number w);
	};

// End of File

#endif
