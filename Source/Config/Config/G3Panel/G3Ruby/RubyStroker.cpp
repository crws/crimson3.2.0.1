
#include "Intern.hpp"

#include "RubyStroker.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPoint.hpp"

#include "RubyVertex.hpp"

#include "RubyPath.hpp"

#include "RubyMatrix.hpp"

#include "RubyDraw.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Stroker Object
//

// Constructor

CRubyStroker::CRubyStroker(void)
{
	m_end     = endRound;

	m_pArrow  = this;

	m_arrow1  = arrowSmall;

	m_arrow2  = arrowSmall;

	m_join    = joinMiter;

	m_edge    = edgeCenter;

	m_limit   = 4;
	}

// Parameters

void CRubyStroker::SetEndStyle(EndStyle end)
{
	m_end = end;
	}

void CRubyStroker::SetArrowStyle(IRubyArrow *pArrow, int arrow1, int arrow2)
{
	m_pArrow = pArrow ? pArrow : this;

	m_arrow1 = arrow1;

	m_arrow2 = arrow2;
	}

void CRubyStroker::SetArrowStyle(int arrow1, int arrow2)
{
	m_pArrow = this;

	m_arrow1 = arrow1;

	m_arrow2 = arrow2;
	}

void CRubyStroker::SetJoinStyle(JoinStyle join)
{
	m_join = join;
	}

void CRubyStroker::SetMiterLimit(int limit)
{
	m_limit = limit;
	}

void CRubyStroker::SetEdgeMode(EdgeMode edge)
{
	m_edge = edge;
	}

// Operations

void CRubyStroker::StrokeLoop(CRubyPath &output, CRubyPath const &figure, int sub, number w)
{
	Stroke(output, figure, sub, w, false);
	}

void CRubyStroker::StrokeOpen(CRubyPath &output, CRubyPath const &figure, int sub, number w)
{
	Stroke(output, figure, sub, w, true);
	}

// Implementation

void CRubyStroker::Stroke(CRubyPath &output, CRubyPath const &figure, int sub, number rw, bool open)
{
	CRubyVector u1, u2;

	CRubyVector n1, n2;

	CRubyPoint  v1a, v1b, v2a, v2b;

	CRubyVector v1, v2, vb;

	CRubyPoint  mp;

	bool outer;

	int  c = figure.GetCount();

	int  q = 1;

	// We are going to scan the path one sub-path at a time,
	// making two passes for each sub-path, one moving in each
	// direction around the path. One pass draws one side of
	// outline and the other draws the other. For a closed path,
	// they are seperated by a soft break. For an open path,
	// they form the opposite sides of the resulting outline.

	for( int s = 0; s < c; ) {

		// Figure out the length of the sub-path.

		int b = figure.GetBreak(s);

		// Allow selection of a specific sub-path.

		if( !sub || sub == q++ ) {

			// Avoid processing degenerate sub-paths.

			if( b > 1 ) {

				// Use open mode for two-point sub-paths.

				bool subopen = (b == 2) ? true : open;

				// Make two passes around each sub-path.

				for( int p = 0; p < 2; p++ ) {

					number w = 0;

					// Figure out the width to be used for this pass,
					// depending on where we are aligning the output
					// relative to the input path.

					switch( m_edge ) {

						case edgeInner:

							// In inner mode, 1 pixel is placed on the
							// outside and the balance on the inside.

							w = (rw == 1) ? (p ? 1 : 0) : (p ? rw-1 : 1);

							break;

						case edgeCenter:

							// In center mode, half of the width is
							// placed on both the inside and outside.

							w = rw / 2;

							break;

						case edgeOuter:

							// In outer mode, 1 pixel is placed on the
							// inside and the balance on the outside.

							w = (rw == 1) ? (p ? 0 : 1) : (p ? 1 : rw-1);

							break;
						}

					// Figure out the square of the miter limit for this
					// pass. We use the square to avoid an expensive root
					// during the checking process.

					number m = w * w * m_limit * m_limit;

					int i1, i2, i3;

					for( int n = 0; n < b; n++ ) {

						// Figure out the three points that we're
						// looking at, these being the curent points
						// and the ones on either side of it. This
						// could be replaced with code that ripples
						// p3->p2->p1 on each step, avoiding some of
						// the math, but I'm not sure it's worth it.

						if( p == 0 ) {

							// First pass, so work forwards around the
							// sub-path, wrapping around if necessary,
							// although the wrapped point isn't used
							// when we're operating in open mode.

							i2 = s + (1 * b + (n + 0)) % b;

							if( w == 0 ) {

								// If we have zero width, just output
								// the current point and be done with.

								output.Append(figure[i2]);

								continue;
								}

							// Otherwise find the other points.

							i1 = s + (1 * b + (n - 1)) % b;
							i3 = s + (1 * b + (n + 1)) % b;
							}
						else {
							// Second pass, so work backwards around the
							// sub-path, wrapping around if necessary,
							// although the wrapped point isn't used
							// when we're operating in open mode.

							i2 = s + (2 * b - (n + 1)) % b;

							if( w == 0 ) {

								// If we have zero width, just output
								// the current point and be done with.

								output.Append(figure[i2]);
								
								continue;
								}

							// Otherwise find the other points.

							i1 = s + (2 * b - (n + 0)) % b;
							i3 = s + (2 * b - (n + 2)) % b;
							}

						// Reference the vertex objects.

						CRubyVertex const &p1 = figure[i1];
						CRubyVertex const &p2 = figure[i2];
						CRubyVertex const &p3 = figure[i3];

						// Find the vectors between the points.

						u1 = p2 - p1;
						u2 = p3 - p2;

						// And convert to unit vectors.

						u1.MakeUnit();
						u2.MakeUnit();

						// Scale them back up by the width.

						u1.Multiply(w);
						u2.Multiply(w);

						// Make copies.

						n1 = u1;
						n2 = u2;

						// And convert to scaled normals.

						n1.Rotate90();
						n2.Rotate90();

						// If we're in open mode, we check for the ends
						// of the line and output the appropriate points.

						if( subopen ) {

							if( n == 0 ) {

								// If we're at the start of the line, output the appropriate
								// end cap. n2 and -u2 form an orthogonal basis with n2 being
								// at right angles to the direction of the line and -u2 being
								// along it. Below we use (x,y) to mean x width steps at right
								// angles to the line and y width steps along it.

								switch( m_end ) {

									case endSquare:

										// Square end cap point is (+1,+1) from p2.

										output.Append(p2 + n2 - u2);

										break;

									case endArrow:

										// Delegate the work to the arrow library.

										m_pArrow->AddArrow(output, p ? m_arrow2 : m_arrow1, 0, p2, -u2, n2, w);

										break;

									default:
										// For flat, round and pointed end caps, we
										// just output a point at (+1,0) from p2 and
										// allow the other end of the other pass to
										// do most of the actual work.

										output.Append(p2 + n2);

										break;
									}

								// Done with this point so loop around.

								continue;
								}

							if( n == b - 1 ) {

								// If we're at the end of the line, output the appropriate
								// end cap. n1 and +u1 form an orthogonal basis with n1 being
								// at right angles to the direction of the line and +u1 being
								// along it. Below we use (x,y) to mean x width steps at right
								// angles to the line and y width steps along it.

								switch( m_end ) {

									case endSquare:

										// Square end cap point is (+1,+1) from p2.

										output.Append(p2 + n1 + u1);

										break;

									case endRound:

										// Round end cap is an arc from (+1, 0) to (-1, 0),
										// We use the ArcSeg version to skip the last point.

										CRubyDraw::ArcSeg(output, p2, p2 + n1, p2 - n1, w);

										break;

									case endFlat:

										// Flat end cap point is (+1,0) from p2.

										output.Append(p2 + n1);

										break;

									case endPoint:

										// Pointed end cap points are (+1,0) and (0,+2) from p2.

										output.Append(p2 + n1 * 1);

										output.Append(p2 + u1 * 2);

										break;

									case endArrow:

										// Delegate the work to the arrow library.

										m_pArrow->AddArrow(output, p ? m_arrow1 : m_arrow2, 1, p2, +u1, n1, w);

										break;

									}

								// Done with this point so loop around.

								continue;
								}
							}

						// Check to see if we're on the outside of a turn
						// relative to the direction in which we're moving.

						if( (outer = ((n1 ^ n2) > 0)) ) {

							// If we don't have a miter join, we can at
							// this point output the join points without
							// too much extra work.

							if( m_join == joinBevel ) {

								// A bevel is just two points offset from
								// the current points by the scaled normals.

								output.Append(p2 + n1);
								output.Append(p2 + n2);

								continue;
								}

							if( m_join == joinRound ) {

								// A round is just an arc drawn between
								// the same two points used for the bevel.

								CRubyDraw::Arc(output, p2, p2 + n1, p2 + n2, w);

								continue;
								}
							}

						// We're either outside with miter mode selected or
						// inside where miter mode is always used, so we need
						// to calculate where the miter point would be. The
						// geometry looks complex but in essence it constructs
						// lines parallel to the line segments on either side of
						// the current point and figures out the intersection.

						v1a = p1 + n1;
						v1b = p2 + n1;
						v2a = p3 + n2;
						v2b = p2 + n2;

						v1  = v1b - v1a;
						v2  = v2b - v2a;
						vb  = v2a - v1a;

						// Figure out the demoninator for the calculation.

						number d = (v1 ^ v2);

						if( num_equal(d, 0) ) {

							// We've got a very low value of d so the points are
							// either co-linear or very close to it. In this case
							// we can just emit a single point and skip the rest.

							output.Append(v1b);
							}
						else {
							// We're good so find the numerator and the miter point.

							number n = (v2 ^ vb);

							number k = n / d;

							mp       = v1a - v1 * k;

							// If we're on the outside, we need to check that the miter
							// point isn't a crazy distance away from the current point,
							// as might happen with nearly parallel lines.

							if( outer ) {

								// Figure out the square of the miter distance.

								number dm = (mp - p2).GetSquare();

								// Compare it to the current limit. We keep the
								// square for now as it avoids the expensive root
								// for the majority of cases where we don't clip.

								if( dm > m ) {

									// If it's too great, we figure out how far
									// we need to go out in width units from the
									// ends of the parallel lines.

									number dk = (mp - v2b).GetInverseLength();

									// Factor avoids strange edge effects!

									number mk = 0.95 * m_limit * num_sqrt(dm) * dk;

									// And output a bevel using this factor
									// and the unit vectors to create the two
									// points that chop the end off the miter.

									output.Append(v1b + u1 * mk);
									output.Append(v2b - u2 * mk);

									// Done with this point so loop around.

									continue;
									}
								}

							// All is good, so output the miter point.

							output.Append(mp);
							}
						}

					if( !subopen && !p ) {

						// For a closed path, at the end of the first
						// pass output a soft break to mark the gap
						// between the inner and outer output paths.

						output.AppendSoftBreak();
						}
					}
			
				// End of the sub-path, so output a hard break.

				output.AppendHardBreak();
				}

			if( sub ) {

				// If we're looking at one sub-path only,
				// we're done at this point so break out.

				break;
				}
			}

		// Move to the next sub-path.

		s += b;
		}

	// Copy the transform from the source.

	output.SetMatrix(figure.GetMatrix());
	}

// Arrow Data

number CRubyStroker::GetArrowLength(int style, number w)
{
	return GetArrowLength(style) * w / 2;
	}

int CRubyStroker::GetArrowLength(int style)
{
	int k = 0;

	switch( style ) {

		case endFlat:

			return 0;
		
		case endSquare:

			return 1;

		case endRound:

			return 1;

		case endPoint:

			return 2;

		case arrowSmall:
		case arrowMedium:
		case arrowLarge:

			switch( style ) {

				case arrowSmall:
					k = 2;
					break;

				case arrowMedium:
					k = 3;
					break;

				case arrowLarge:
					k = 4;
					break;
				}

			return k;

		case arrowSmallBarb:
		case arrowMediumBarb:
		case arrowLargeBarb:

			switch( style ) {

				case arrowSmallBarb:
					k = 2;
					break;

				case arrowMediumBarb:
					k = 3;
					break;

				case arrowLargeBarb:
					k = 4;
					break;
				}

			return k;

		case arrowSmallOuter:
		case arrowMediumOuter:
		case arrowLargeOuter:

			switch( style ) {

				case arrowSmallOuter:
					k = 2;
					break;

				case arrowMediumOuter:
					k = 3;
					break;

				case arrowLargeOuter:
					k = 4;
					break;
				}

			return k;
		}

	return 0;
	}

// Default Arrow

void CRubyStroker::AddArrow(CRubyPath &output, int style, int side, CRubyPoint  const &p, CRubyVector const &u, CRubyVector const &n, number w)
{
	// An arrow function is called once for each side of each end
	// of a line. It is provided with the end point located on the
	// line and two vectors: The unit vector along the direction of
	// the line and a vector at 90 degrees to that, both scaled by
	// the selected width. These form an orthogonal basis that can
	// be used to create figures at the end of the line. This version
	// of the function implements a basic arrow library.

	int j = 0, k = 0;

	switch( style ) {

		case endFlat:

			output.Append(p + n);

			break;
		
		case endSquare:
			
			output.Append(p + n + u);

			break;

		case endRound:

			if( side == 0 ) {

				output.Append(p + n);
				}
			else
				CRubyDraw::ArcSeg(output, p, p + n, p - n, w);
		
			break;

		case endPoint:

			if( side == 0 ) {

				output.Append(p + n);
				}
			else {
				output.Append(p + n * 1);
				output.Append(p + u * 2);
				}

			break;

		case arrowSmall:
		case arrowMedium:
		case arrowLarge:

			switch( style ) {

				case arrowSmall:
					k = 2;
					break;

				case arrowMedium:
					k = 3;
					break;

				case arrowLarge:
					k = 4;
					break;
				}
		
			if( side == 0 ) {

				output.Append(p + n * k);
				output.Append(p + n * 1);
				}
			else {
				output.Append(p + n * 1);
				output.Append(p + n * k);
				output.Append(p + u * k);
				}

			break;

		case arrowSmallBarb:
		case arrowMediumBarb:
		case arrowLargeBarb:

			switch( style ) {

				case arrowSmallBarb:
					j = 1;
					k = 2;
					break;

				case arrowMediumBarb:
					j = 2;
					k = 3;
					break;

				case arrowLargeBarb:
					j = 3;
					k = 4;
					break;
				}

			if( side == 0 ) {

				output.Append(p + n * k - u * j);
				output.Append(p + n * 1);
				}
			else {
				output.Append(p + n * 1);
				output.Append(p + n * k - u * j);
				output.Append(p + u * k);
				}

			break;

		case arrowSmallOuter:
		case arrowMediumOuter:
		case arrowLargeOuter:

			switch( style ) {

				case arrowSmallOuter:
					k = 2;
					break;

				case arrowMediumOuter:
					k = 3;
					break;

				case arrowLargeOuter:
					k = 4;
					break;
				}
		
			if( side == 0 ) {

				output.Append(p + u * k + n * k);
				output.Append(p + n * 1);
				}
			else {
				output.Append(p + n * 1);
				output.Append(p + u * k + n * k);
				}

			break;

		default:

			output.Append(p	+ n);

			break;
		}
	}

// End of File
