
#include "intern.hpp"

#include "RubyDrawPlus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPoint.hpp"

#include "RubyPath.hpp"

#include "RubyTrig.hpp"

#include "RubyStroker.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Drawing Object
//

// Constructor

CRubyDrawPlus::CRubyDrawPlus(void)
{
	}

// Drawing Methods

void CRubyDrawPlus::Line(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number width)
{
	m_path.Empty();

	CRubyDraw::Line(m_path, p1, p2);

	m_s.StrokeOpen(output, m_path, 0, width);
	}

void CRubyDrawPlus::Line(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number width, int end)
{
	m_path.Empty();

	CRubyDraw::Line(m_path, p1, p2);

	m_s.SetEndStyle(CRubyStroker::EndStyle(end));

	m_s.StrokeOpen(output, m_path, 0, width);
	}

bool CRubyDrawPlus::Line(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number width, int end1, int end2)
{
	number k1 = m_s.GetArrowLength(end1, width);
	
	number k2 = m_s.GetArrowLength(end2, width);

	number ks = (k1 + k2) * (k1 + k2);

	CRubyVector s = p2 - p1;

	number      d = s.GetSquare();

	if( d > ks ) {

		s.MakeUnit(d);

		m_path.Empty();

		CRubyDraw::Line(m_path, p1 + s * k1, p2 - s * k2);

		m_s.SetEndStyle(CRubyStroker::endArrow);

		m_s.SetArrowStyle(end1, end2);

		m_s.StrokeOpen(output, m_path, 0, width);

		return true;
		}

	return false;
	}

// End of File
