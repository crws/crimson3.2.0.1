
#include "Intern.hpp"

#include "RubyGdiList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPath.hpp"

#include "RubyMatrix.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Gdi List
//

// Constructor

CRubyGdiList::CRubyGdiList(void)
{
	m_pList  = NULL;

	m_pCount = NULL;

	m_uList  = 0;

	m_uCount = 0;
	}

// Destructor

CRubyGdiList::~CRubyGdiList(void)
{
	delete [] m_pList;

	delete [] m_pCount;
	}

// Operations

void CRubyGdiList::Empty(void)
{
	delete [] m_pList;

	delete [] m_pCount;

	m_pList  = NULL;

	m_pCount = NULL;

	m_uList  = 0;

	m_uCount = 0;
	}

void CRubyGdiList::Load(CRubyMatrix const *m, CRubyPath const &figure, bool over)
{
	// TODO -- Remove degenerate points that get through the path...

	// This function builds an easily-serialized data structure that
	// contains the Gdi representation of a path. We list the vertices
	// in order, and maintain pairs of counts indicating how many
	// entries are to be passed as List1 and List2 to the associated
	// Gdi calls. The structure is compact and very easy for the
	// runtime code to traverse. It can also be calculated at config
	// time and included in download to reduce runtime processing.

	Empty();

	// Don't bother for empty figures.

	if( figure.GetCount() ) {

		// The point list is always the length of the input path. The
		// count list is more complex as it has to contain extra zeroes
		// to mark single-sided paths, and the two zeroes that mark the
		// end of the list. The path does most of the work for us.

		m_uList  = figure.GetCount();

		m_uCount = figure.GetGdiSubCount() + 2;

		m_pList  = New P2   [ m_uList  ];

		m_pCount = New UINT [ m_uCount ];

		// Get ready to traverse the node list.

		int c = figure.GetCount();

		int r = 0;

		int k = over ? 4 : 1;

		CRubyPoint p;

		int s;

		for( s = 0; s < c; ) {

			int b         = figure.GetBreak(s);

			m_pCount[r++] = b;

			for( int n = 0; n < b; n++ ) {

				// Read each point in the run and transform it before
				// converting to Gdi subpixel coordinates. It might
				// be quicker to convert all the points and then do
				// the run length recording, but this since this is
				// mostly configuration-time code, we won't bother.

				CRubyPoint p = figure[s+n];

				if( m ) {

					p.Transform(*m);
					}

				m_pList[s+n].x = num_round(p.m_x * k);
			
				m_pList[s+n].y = num_round(p.m_y * k);
				}

			if( r % 2 ) {

				if( figure[s + b - 1].IsHardBreak() ) {

					// We have a hardbreak at an odd point, so
					// this is a single-sided path that does not
					// need a List2 to be passed to the Gdi.

					m_pCount[r++] = 0;
					}
				}

			s += b;
			}

		// Two zeros indicate the end of the list.

		m_pCount[r++] = 0;
	
		m_pCount[r++] = 0;

		AfxAssert(UINT(r) == m_uCount);

		AfxAssert(UINT(s) == m_uList);
		}
	}

void CRubyGdiList::Load(CRubyMatrix const &m, CRubyPath const &figure, bool over)
{
	Load(&m, figure, over);
	}

void CRubyGdiList::Load(CRubyPath const &figure, bool over)
{
	Load(NULL, figure, over);
	}

// Debugging

void CRubyGdiList::Trace(PCTXT pName) const
{
	AfxTrace(L"%s = \n", pName);

	UINT n = 0;

	UINT p = 0;

	for(;;) {

		UINT c1 = m_pCount[n++];

		UINT c2 = m_pCount[n++];

		if( c1 || c2 ) {

			P2 * p1 = m_pList + p;

			p       = p + c1;

			P2 * p2 = m_pList + p;

			p       = p + c2;

			if( c1 ) {

				AfxTrace(L"  Block of %u points:\n", c1);

				for( UINT n = 0; n < c1; n++ ) {

					P2 const &pn = p1[n];

					AfxTrace(L"    %-3u = ( %4d, %4d )\n", n, pn.x, pn.y);
					}
				}

			if( c2 ) {

				AfxTrace(L"    Soft\n");

				AfxTrace(L"  Block of %u points:\n", c2);

				for( UINT n = 0; n < c2; n++ ) {

					P2 const &pn = p2[n];

					AfxTrace(L"    %-3u = ( %4d, %4d )\n", n, pn.x, pn.y);
					}
				}

			AfxTrace(L"    Hard\n");

			continue;
			}

		break;
		}

	AfxTrace(L"  End\n");
	}

// End of File
