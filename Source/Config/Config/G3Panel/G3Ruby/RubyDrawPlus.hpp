
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyDrawPlus_HPP
	
#define	INCLUDE_RubyDrawPlus_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RubyPath.hpp"

#include "RubyDraw.hpp"

#include "RubyStroker.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Drawing Object
//

class DLLAPI CRubyDrawPlus : public CRubyDraw
{
	public:
		// Constructor
		CRubyDrawPlus(void);

		// Drawing Methods
		void Line(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number width);
		void Line(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number width, int end);
		bool Line(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number width, int end1, int end2);

		// Data Members
		CRubyStroker m_s;
		CRubyPath    m_path;
	};

// End of File

#endif