
#include "intern.hpp"

#include "RubyVector.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Debugging

void CRubyVector::Trace(PCTXT pName) const
{
	#if 0

	// This version produces better output for pasting
	// into Excel and then creating scatter graphs to
	// assess the quality of the output figure.

	AfxTrace( L"%s,%9.4f,%9.4f\n",
		  pName,
		  m_x,
		  m_y
		  );

	#else

	// This version is easier to read on the console.

	#if defined(__GNUC__)

	AfxTrace( L"%s = ( %5d.%4.4d, %5d.%4.4d )\n",
		  pName,
		  int(m_x),
		  int((m_x-int(m_x))*10000),
		  int(m_y),
		  int((m_y-int(m_y))*10000)
		  );

	#else

	AfxTrace( L"%s = ( %9.4f, %9.4f )\n",
		  pName,
		  m_x,
		  m_y
		  );

	#endif

	#endif
	}

// End of File
