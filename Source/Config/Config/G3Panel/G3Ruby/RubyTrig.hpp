
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyTrig_HPP
	
#define	INCLUDE_RubyTrig_HPP

//////////////////////////////////////////////////////////////////////////
//
// Trig Heler
//

class DLLAPI CRubyTrig
{
	public:
		// Trig Functions
		static number Cos(number deg);
		static number Sin(number deg);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Trig Functions

inline number CRubyTrig::Cos(number deg)
{
	return ::cos(deg * 0.01745329251994329576);
	}

inline number CRubyTrig::Sin(number deg)
{
	return ::sin(deg * 0.01745329251994329576);
	}

// End of File

#endif
