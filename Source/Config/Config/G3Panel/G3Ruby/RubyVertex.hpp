
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyVertex_HPP
	
#define	INCLUDE_RubyVertex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RubyPoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Vertex Object
//

class DLLAPI CRubyVertex : public CRubyPoint
{
	public:
		// Break Flags
		enum
		{
			flagSoftBreak = 1,
			flagHardBreak = 2,
			flagLineBreak = 4,
			};

		// Constructors
		CRubyVertex(void);
		CRubyVertex(QUICKARG);

		// Assignment
		CRubyVertex const & operator = (CRubyPoint const &That);

		// Attributes
		bool IsBreak(void) const;
		bool IsSoftBreak(void) const;
		bool IsHardBreak(void) const;

		// Data Members
		int m_flags;
		int m_count;

	private:
		// Inaccessible
		CRubyVertex(CRubyVertex const &That);
		CRubyVertex(number x, number y);

		// Inaccessible
		CRubyVertex const & operator = (CRubyVertex const &That);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

inline CRubyVertex::CRubyVertex(void)
{
	m_x     = 0;
	
	m_y     = 0;

	m_flags = 0;

	m_count = 0;
	}

inline CRubyVertex::CRubyVertex(QUICKARG)
{
	}

// Assignment

inline CRubyVertex const & CRubyVertex::operator = (CRubyPoint const &That)
{
	m_x     = That.m_x;
	
	m_y     = That.m_y;

	m_flags = 0;

	m_count = 0;

	return ThisObject;
	}

// Attributes

inline bool CRubyVertex::IsBreak(void) const
{
	return IsSoftBreak() || IsHardBreak();
	}

inline bool CRubyVertex::IsSoftBreak(void) const
{
	return (m_flags & flagSoftBreak) ? true : false;
	}

inline bool CRubyVertex::IsHardBreak(void) const
{
	return (m_flags & flagHardBreak) ? true : false;
	}

// End of File

#endif
