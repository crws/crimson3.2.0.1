
//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3ruby.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Array Stuff
//

template <typename type> void ArrayZero(type *p, UINT n)
{
	memset(p, 0, sizeof(type) * n);
	}

template <typename type> void ArrayCopy(type *d, type const *s, UINT n)
{
	memcpy(d, s, sizeof(type) * n);
	}

template <typename type> type * ArrayAlloc(type const *d, UINT n)
{
	return (type *) malloc(sizeof(type) * n);
	}

template <typename type> type * ArrayReAlloc(type *d, UINT n)
{
	return (type *) realloc(d, sizeof(type) * n);
	}

// End of File

#endif
