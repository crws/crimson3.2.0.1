
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsSysBlock_HPP

#define INCLUDE_CommsSysBlock_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;

//////////////////////////////////////////////////////////////////////////
//
// Communications Block
//

class DLLNOT CCommsSysBlock : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsSysBlock(void);

		// Item Location
		CCommsDevice * GetParentDevice(void) const;

		// Attributes
		BOOL  GetBlockType(CTypeDef &Type) const;
		DWORD GetRefAddress(CDataRef const &Ref) const;
		BOOL  IsNamed(void) const;

		// Operations
		BOOL AcceptAddress(CAddress Addr, INT nCount, DWORD &ID, UINT uPass, BOOL fIsNamedArray);

		// Persistance
		void Init(void);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT       m_Number;
		UINT       m_Named;
		UINT       m_Space;
		INT        m_Factor;
		INT        m_Align;
		INT        m_Base;
		INT        m_Index;
		INT        m_Size;
		CWordArray m_List;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
