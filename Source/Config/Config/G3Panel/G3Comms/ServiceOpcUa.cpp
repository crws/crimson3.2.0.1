
#include "Intern.hpp"

#include "ServiceOpcUa.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Opc Ua Service
//

// Dynamic Class

AfxImplementDynamicClass(CServiceOpcUa, CServiceItem);

// Constructor

CServiceOpcUa::CServiceOpcUa(void)
{
	m_pEnable    = NULL;
	m_pServer    = NULL;
	m_pEndpoint  = NULL;
	m_Port       = 4840;
	m_Layout     = 0;
	m_Tree       = 0;
	m_Array      = 0;
	m_Props      = 0;
	m_PubSub     = 1;
	m_HistEnable = 0;
	m_HistSample = 1;
	m_HistQuota  = 50;
	m_HistTime   = 65;
	m_Anon       = 1;
	m_pUser      = NULL;
	m_pPass      = NULL;
	m_Write      = 0;
	m_Debug      = 0;
	m_pSet       = New CTagSet;
	m_pSet2      = New CTagSet;
}

// UI Management

CViewWnd * CServiceOpcUa::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return CreateItemView(FALSE);
	}

	return NULL;
}

// UI Loading

BOOL CServiceOpcUa::OnLoadPages(CUIPageList *pList)
{
	BOOL fSave = (m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B);

	UINT uPage = fSave ? 1 : 2;

	pList->Append(New CUIStdPage(CString(IDS_SERVICE), AfxThisClass(), uPage));

	pList->Append(New CUIStdPage(CString(IDS_NETWORK), AfxThisClass(), 3));

	pList->Append(New CUIStdPage(IDS("Public Data"), AfxThisClass(), 4));

	pList->Append(New CUIStdPage(IDS("Private Data"), AfxThisClass(), 5));

	return TRUE;
}

// UI Update

void CServiceOpcUa::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" || Tag == "HistEnable" ) {

			DoEnables(pHost);
		}
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Initial Values

void CServiceOpcUa::SetInitValues(void)
{
	SetInitial(L"Enable", m_pEnable, 0);
}

// Type Access

BOOL CServiceOpcUa::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Server" || Tag == L"Endpoint" || Tag == L"User" || Tag == L"Pass" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Attributes

BOOL CServiceOpcUa::IsEnabled(void) const
{
	UINT uGroup = m_pDbase->GetSoftwareGroup();

	if( uGroup == SW_GROUP_2 || uGroup >= SW_GROUP_3B ) {

		return TRUE;
	}

	return FALSE;
}

UINT CServiceOpcUa::GetTreeImage(void) const
{
	return IDI_OPC_UA;
}

// Download Support

BOOL CServiceOpcUa::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(servOpcUa));

	CServiceItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Debug));

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddItem(itemVirtual, m_pServer);

	Init.AddItem(itemVirtual, m_pEndpoint);

	Init.AddWord(WORD(m_Port));

	Init.AddByte(BYTE(m_Layout));

	Init.AddByte(BYTE(m_Tree));

	Init.AddByte(BYTE(m_Array));

	switch( m_Props ) {

		case 0: Init.AddLong(0x00000000); break;
		case 1: Init.AddLong(0x7FFFFFFF); break;
		case 2: Init.AddLong(0xFFFFFFFF); break;
	}

	Init.AddByte(BYTE(m_PubSub));

	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		Init.AddByte(BYTE(m_HistEnable));

		Init.AddWord(WORD(m_HistSample));

		Init.AddByte(BYTE(m_HistQuota));

		Init.AddByte(BYTE(m_HistTime));
	}
	else {
		Init.AddByte(BYTE(0));

		Init.AddWord(WORD(1));

		Init.AddByte(BYTE(50));

		Init.AddByte(BYTE(65));
	}

	Init.AddByte(BYTE(m_Anon));

	Init.AddItem(itemVirtual, m_pUser);

	Init.AddItem(itemVirtual, m_pPass);

	Init.AddByte(BYTE(m_Write));

	Init.AddWord(WORD(m_pSet->GetCount() + m_pSet2->GetCount()));

	Init.AddItem(itemSimple, m_pSet);

	Init.AddItem(itemSimple, m_pSet2);

	return TRUE;
}

// Meta Data Creation

void CServiceOpcUa::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddVirtual(Endpoint);
	Meta_AddVirtual(Server);
	Meta_AddInteger(Port);
	Meta_AddInteger(Layout);
	Meta_AddInteger(Tree);
	Meta_AddInteger(Array);
	Meta_AddInteger(Props);
	Meta_AddInteger(PubSub);
	Meta_AddInteger(HistEnable);
	Meta_AddInteger(HistSample);
	Meta_AddInteger(HistQuota);
	Meta_AddInteger(HistTime);
	Meta_AddInteger(Anon);
	Meta_AddVirtual(User);
	Meta_AddVirtual(Pass);
	Meta_AddInteger(Write);
	Meta_AddObject(Set);
	Meta_AddObject(Set2);
	Meta_AddInteger(Debug);

	Meta_SetName((IDS_OPC_UA_SERVER));
}

// Implementation

void CServiceOpcUa::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Set", fEnable);
	pHost->EnableUI(this, "Set2", fEnable);
	pHost->EnableUI(this, "Endpoint", fEnable);
	pHost->EnableUI(this, "Server", fEnable);
	pHost->EnableUI(this, "Port", fEnable);
	pHost->EnableUI(this, "Layout", fEnable);
	pHost->EnableUI(this, "Tree", fEnable);
	pHost->EnableUI(this, "Array", fEnable);
	pHost->EnableUI(this, "Props", fEnable);
	pHost->EnableUI(this, "PubSub", fEnable);
	pHost->EnableUI(this, "HistEnable", fEnable);
	pHost->EnableUI(this, "HistSample", fEnable && m_HistEnable);
	pHost->EnableUI(this, "HistQuota", fEnable && m_HistEnable);
	pHost->EnableUI(this, "HistTime", fEnable && m_HistEnable);
	pHost->EnableUI(this, "Anon", fEnable);
	pHost->EnableUI(this, "User", fEnable);
	pHost->EnableUI(this, "Pass", fEnable);
	pHost->EnableUI(this, "Write", fEnable);
	pHost->EnableUI(this, "Debug", fEnable);
}

// End of File
