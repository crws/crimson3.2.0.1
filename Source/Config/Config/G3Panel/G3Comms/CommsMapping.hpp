
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsMapping_HPP

#define INCLUDE_CommsMapping_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsMapBlock;
class CCommsMapReg;
class CCommsMapping;
class CCommsMappingList;

//////////////////////////////////////////////////////////////////////////
//
// Communications Mapping
//

class DLLNOT CCommsMapping : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsMapping(void);

		// Item Location
		CCommsMappingList * GetParentList(void) const;
		CCommsMapReg      * GetParentReg(void) const;
		CCommsMapBlock    * GetParentBlock(void) const;
		CCommsDevice      * GetParentDevice(void) const;

		// Attributes
		BOOL    IsMapped      (void) const;
		BOOL    IsBroken      (void) const;
		UINT    GetTreeImage  (void) const;
		CString GetTreeLabel  (void) const;
		CString GetAddrText   (void) const;
		CString GetMapLinkText(void) const;
		CString GetMapAddrText(void) const;
		CString GetMapSource  (BOOL fExpand) const;
		BOOL	IsTag         (UINT uTag) const;

		// Operations
		void Validate(BOOL fExpand);
		void TagCheck(UINT uTag);
		UINT SetMapping(CString Text);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;
		CString GetFindInfo(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);
		void Kill(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Linked List
		CCommsMapping * m_pNext;
		CCommsMapping * m_pPrev;

		// Item Properties
		UINT	   m_Disable;
		CByteArray m_Source;
		DWORD      m_Ref;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Server Access
		INameServer * GetNameServer(void) const;

		// Implementation
		void FindReqType(CTypeDef &Type);
		void SendUpdate(void);
		void ListAppend(void);
		void ListRemove(void);
	};

// End of File

#endif
