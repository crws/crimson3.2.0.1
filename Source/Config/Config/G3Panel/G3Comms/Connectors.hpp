
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_Connectors_HPP

#define INCLUDE_Connectors_HPP

//////////////////////////////////////////////////////////////////////////
//
// Connectors Configuration
//

class DLLNOT CConnectors : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CConnectors(void);

		// Attributes
		UINT GetTreeImage(void) const;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
