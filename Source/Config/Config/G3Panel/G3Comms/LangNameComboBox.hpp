
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_LangNameComboBox_HPP

#define INCLUDE_LangNameComboBox_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangManager;

//////////////////////////////////////////////////////////////////////////
//
// Language Name Combo Box
//

class CLangNameComboBox : public CDropComboBox
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CLangNameComboBox(CUIElement *pUI);

	protected:
		// Data Members
		CLangManager * m_pLang;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item);
		void OnChar(UINT uCode, DWORD dwFlags);
	};

// End of File

#endif
