
#include "Intern.hpp"

#include "TagResTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DataTag.hpp"

#include "TagFolder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CTagResTreeWnd, CResTreeWnd);

// Constructor

CTagResTreeWnd::CTagResTreeWnd(void) : CResTreeWnd( L"Tags",
						    AfxRuntimeClass(CTagFolder),
						    AfxRuntimeClass(CDataTag)
						    )
{
	m_cfCode    = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_cfTagList = RegisterClipboardFormat(L"C3.1 Tag List");

	m_cfFolder  = RegisterClipboardFormat(L"C3.1 Tag Folder");

	m_pTree->SetMultiple(TRUE);
	}

// IUpdate

HRESULT CTagResTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateProps ) {

		if( pItem->IsKindOf(m_Class) ) {

			CDataTag *pTag = (CDataTag *) pItem;

			HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

			if( hItem ) {

				BOOL fExpand = m_pTree->IsExpanded(hItem);

				m_pTree->DeleteChildren(hItem);

				if( pTag->m_Extent && fExpand ) {

					OnExpandTag(hItem);
					}
				}
			}
		}

	return CResTreeWnd::ItemUpdated(pItem, uType);
	}

// Message Map

AfxMessageMap(CTagResTreeWnd, CResTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, TVN_KEYDOWN, OnTreeKeyDown)

	AfxDispatchGetInfoType(IDM_RES, OnResGetInfo)
	AfxDispatchControlType(IDM_RES, OnResControl)
	AfxDispatchCommandType(IDM_RES, OnResCommand)

	AfxMessageEnd(CTagResTreeWnd)
	};

// Message Handlers

void CTagResTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"TagResTreeTool"));
		}
	}

// Notification Handlers

BOOL CTagResTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_RETURN:

			if( !m_pTree->GetChild(m_hSelect) ) {

				IDataObject *pData = NULL;

				if( MakeDataObject(pData) ) {

					HTREEITEM hNext;

					OleSetClipboard(pData);

					OleFlushClipboard();

					pData->Release();

					m_System.SendPaneCommand(IDM_EDIT_PASTE);

					if( (hNext = m_pTree->GetNextNode(m_hSelect, TRUE)) ) {

						m_pTree->SelectItem(hNext);

						SetFocus();
						}
					}
				}

			ToggleItem(m_hSelect);

			return TRUE;
		}

	return CResTreeWnd::OnTreeKeyDown(uID, Info);
	}

// Command Handlers

BOOL CTagResTreeWnd::OnResGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =

	{	IDM_RES_TOGGLE,   MAKELONG(0x003A, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CTagResTreeWnd::OnResControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_RES_TOGGLE:

			Src.EnableItem(CanExpandTag() || CanCollapseTag());

			Src.CheckItem (CanCollapseTag());

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CTagResTreeWnd::OnResCommand(UINT uID)
{
	switch( uID ) {

		case IDM_RES_TOGGLE:

			if( CanExpandTag() ) {

				OnExpandTag(m_hSelect);

				break;
				}

			if( CanCollapseTag() ) {

				OnCollapseTag();

				break;
				}

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Data Object Construction

BOOL CTagResTreeWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddCodeFragment(pMake);

	AddFolder(pMake);

	AddTagList(pMake);

	AddIdentifier(pMake);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CTagResTreeWnd::AddCodeFragment(CDataObject *pData)
{
	if( !m_fMulti ) {

		if( m_pNamed ) {

			if( m_pNamed->IsKindOf(m_Class) ) {

				CDataTag *pTag = (CDataTag *) m_pNamed;

				CString   Name = pTag->GetName();

				if( pTag->m_Extent ) {

					if( m_pTree->GetChild(m_hSelect) == NULL ) {

						HTREEITEM hParent = m_pTree->GetParent(m_hSelect);

						if( GetItemPtr(hParent) == m_pNamed ) {

							CString Root = GetRoot(Name, TRUE);

							CString Elem = m_pTree->GetItemText(m_hSelect);

							Name = Root + Elem;
							}
						else {
							Name += L"[0]";
							}
						}
					else {
						Name += L"[0]";
						}
					}

				pData->AddText(m_cfCode, Name);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CTagResTreeWnd::AddTagList(CDataObject *pData)
{
	if( m_fMulti ) {

		CString   List;

		HTREEITEM hItem = m_pTree->GetFirstSelect();

		while( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			if( pItem->IsKindOf(m_Class) ) {

				CDataTag *pTag = (CDataTag *) pItem;

				CString   Name = pTag->GetName();

				if( pTag->m_Extent ) {

					if( m_pTree->GetChild(hItem) == NULL ) {

						HTREEITEM hParent = m_pTree->GetParent(hItem);

						if( GetItemPtr(hParent) == pItem ) {

							CString Root = GetRoot(Name, TRUE);

							CString Elem = m_pTree->GetItemText(hItem);

							Name = Root + Elem;
							}
						else {
							Name += L"[0]";
							}
						}
					else {
						Name += L"[0]";
						}
					}

				AddName(List, Name);
				}

			hItem = m_pTree->GetNextSelect(hItem);
			}

		if( !List.IsEmpty() ) {

			pData->AddText(m_cfTagList, List);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagResTreeWnd::AddFolder(CDataObject *pData)
{
	if( !m_fMulti ) {

		if( m_pNamed ) {

			if( m_pNamed->IsKindOf(m_Folder) ) {

				CString Name = m_pNamed->GetName();

				pData->AddText(m_cfFolder, Name);

				CString List;

				AddChildTags(List, m_hSelect);

				if( !List.IsEmpty() ) {

					pData->AddText(m_cfTagList, List);
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CTagResTreeWnd::AddChildTags(CString &List, HTREEITEM hRoot)
{
	HTREEITEM hItem = m_pTree->GetNextItem(hRoot, TVGN_CHILD);

	while( hItem ) {

		CMetaItem *pItem = GetItemPtr(hItem);

		if( pItem->IsKindOf(m_Class) ) {

			CDataTag *pTag = (CDataTag *) pItem;

			CString   Name = pTag->GetName();

			if( pTag->m_Extent ) {

				if( m_pTree->GetChild(hItem) == NULL ) {

					HTREEITEM hParent = m_pTree->GetParent(hItem);

					if( GetItemPtr(hParent) == pItem ) {

						CString Root = GetRoot(Name, TRUE);

						CString Elem = m_pTree->GetItemText(hItem);

						Name = Root + Elem;
						}
					else {
						Name += L"[0]";
						}

					AddName(List, Name);
					}
				}
			else
				AddName(List, Name);
			}

		AddChildTags(List, hItem);

		hItem = m_pTree->GetNextItem(hItem, TVGN_NEXT);
		}
	}

BOOL CTagResTreeWnd::AddIdentifier(CDataObject *pData)
{
	if( !m_fMulti ) {

		if( m_pNamed ) {

			CString List;

			CString Name = m_pNamed->GetName();

			if( m_pNamed->IsKindOf(m_Class) ) {

				CDataTag *pTag = (CDataTag *) m_pNamed;

				if( pTag->m_Extent ) {

					if( m_pTree->GetChild(m_hSelect) == NULL ) {

						HTREEITEM hParent = m_pTree->GetParent(m_hSelect);

						if( GetItemPtr(hParent) == m_pNamed ) {

							CString Root = GetRoot(Name, TRUE);

							CString Elem = m_pTree->GetItemText(m_hSelect);

							Name = Root + Elem;
							}
						else {
							Name += L"[0]";
							}
						}
					else {
						Name += L"[0]";
						}
					}
				}

			AddName(List, m_pItem->GetDatabase()->GetUniqueSig());

			AddName(List, m_Class->GetClassName());

			AddName(List, Name);

			AddName(List, CPrintf(L"%8.8X", m_pNamed));

			AddName(List, CPrintf(L"%8.8X", PDWORD(m_pNamed)[0]));

			pData->AddText(m_cfIdent, List);

			return TRUE;
			}
		}

	return FALSE;
	}

void CTagResTreeWnd::AddName(CString &List, CString Name)
{
	if( !List.IsEmpty() ) {

		List += L'|';
		}

	List += Name;
	}

// Tree Loading

void CTagResTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TagsTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CTagResTreeWnd::GetRootImage(void)
{
	return IDI_TAGS;
	}

UINT CTagResTreeWnd::GetItemImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(m_Class) ) {

		CTag *pTag = (CTag *) pItem;

		return pTag->GetTreeImage();
		}

	return IDI_FOLDER;
	}

BOOL CTagResTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"TagResTreeCtxMenu";

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CTagResTreeWnd::CanExpandTag(void)
{
	if( m_hSelect ) {

		CItem *pItem = GetItemPtr(m_hSelect);

		if( pItem->IsKindOf(m_Class) ) {

			CDataTag *pTag = (CDataTag *) pItem;

			if( pTag->m_Extent ) {

				if( m_pTree->GetChild(m_hSelect) == NULL ) {

					HTREEITEM hParent = m_pTree->GetParent(m_hSelect);

					return GetItemPtr(hParent) != pItem;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CTagResTreeWnd::CanCollapseTag(void)
{
	if( m_hSelect ) {

		CItem *pItem = GetItemPtr(m_hSelect);

		if( pItem->IsKindOf(m_Class) ) {

			CDataTag *pTag = (CDataTag *) pItem;

			if( pTag->m_Extent ) {

				if( m_pTree->GetChild(m_hSelect) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

void CTagResTreeWnd::OnExpandTag(HTREEITEM hItem)
{
	CItem *pItem = GetItemPtr(hItem);

	if( pItem->IsKindOf(m_Class) ) {

		SetRedraw(FALSE);

		afxThread->SetWaitMode(TRUE);

		CDataTag *pTag = (CDataTag *) pItem;

		UINT  uImage = pTag->GetTreeImage();

		CString Name = pTag->GetName();

		for( UINT n = 0; n < pTag->m_Extent; n++ ) {

			CTreeViewItem Node;

			Node.SetText(GetName(Name + CPrintf(L"[%d]", n)));

			Node.SetParam(LPARAM(pItem));

			Node.SetImages(uImage);

			m_pTree->InsertItem(hItem, NULL, Node);
			}

		m_pTree->Expand(hItem, TVE_EXPAND);

		afxThread->SetWaitMode(FALSE);

		SetRedraw(TRUE);
		}
	}

void CTagResTreeWnd::OnCollapseTag(void)
{
	SetRedraw(FALSE);

	afxThread->SetWaitMode(TRUE);

	m_pTree->DeleteChildren(m_hSelect);

	afxThread->SetWaitMode(FALSE);

	SetRedraw(TRUE);
	}

// End of File
