
#include "Intern.hpp"

#include "CommsPortVirtual.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsDeviceList.hpp"
#include "CommsPortList.hpp"
#include "CommsPortPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Virtual Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortVirtual, CCommsPort);

// Constructor

CCommsPortVirtual::CCommsPortVirtual(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding  = bindStdSerial;

	m_uImage   = IDI_ETHERNET_VIRTUAL;

	m_pMode    = NULL;

	m_pIP      = NULL;

	m_pPort    = NULL;

	m_pLinkOpt = NULL;

	m_pTimeout = NULL;
	}

// Initial Values

void CCommsPortVirtual::SetInitValues(void)
{
	SetInitial(L"Mode",	m_pMode,  0);

	SetInitial(L"IP",	m_pIP,    DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192))));

	SetInitial(L"Port",	m_pPort,  1234);

	SetInitial(L"LinkOpt",	m_pLinkOpt, 0);

	SetInitial(L"Timeout",	m_pTimeout, 10000);
	}

// UI Overridables

BOOL CCommsPortVirtual::OnLoadPages(CUIPageList *pList)
{
	CCommsPortPage *pPage = New CCommsPortPage(this);

	pList->Append(pPage);

	return FALSE;
	}

void CCommsPortVirtual::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Mode" || Tag == "LinkOpt" ) {

		DoEnables(pHost);
		}

	CCommsPort::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CCommsPortVirtual::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Attributes

UINT CCommsPortVirtual::GetPageType(void) const
{
	return m_pDriver ? 1 : 0;
	}

// Persistance

void CCommsPortVirtual::Init(void)
{
	CCommsPort::Init();

	CCommsPortList *pList = (CCommsPortList *) GetParent();

	m_Name = pList->MakeUnique(CString(IDS_VIRTUAL_FMT));
	}

// Download Support

BOOL CCommsPortVirtual::MakeInitData(CInitData &Init)
{
	Init.AddByte(3);

	CCommsPort::MakeInitData(Init);

	if( m_pDriver ) {

		Init.AddItem(itemVirtual, m_pMode);

		Init.AddItem(itemVirtual, m_pIP);

		Init.AddItem(itemVirtual, m_pPort);

		Init.AddItem(itemVirtual, m_pLinkOpt);

		Init.AddItem(itemVirtual, m_pTimeout);
		}

	return TRUE;
	}

// Meta Data Creation

void CCommsPortVirtual::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_AddVirtual(Mode);
	Meta_AddVirtual(IP);
	Meta_AddVirtual(Port);
	Meta_AddVirtual(LinkOpt);
	Meta_AddVirtual(Timeout);

	Meta_SetName((IDS_VIRTUAL_PORT));
	}

// Implementation

void CCommsPortVirtual::DoEnables(IUIHost *pHost)
{
	BOOL fEnable  = m_DriverID ? TRUE : FALSE;

	BOOL fActive  = CheckEnable(m_pMode, L"==", 0, TRUE);

	BOOL fTimeout = CheckEnable(m_pLinkOpt, L"==", linkTimeout, FALSE);

	pHost->EnableUI("Mode",    fEnable);

	pHost->EnableUI("Port",    fEnable);

	pHost->EnableUI("IP",      fEnable && fActive);

	pHost->EnableUI("LinkOpt", fEnable && !fActive);

	pHost->EnableUI("Timeout", fEnable && !fActive && fTimeout);
	}

// End of File
