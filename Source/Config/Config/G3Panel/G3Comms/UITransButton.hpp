
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITransButton_HPP

#define INCLUDE_UITransButton_HPP

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Translation Button
//

class CUITransButton : public CUIButton
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUITransButton(CString Logo);

	protected:
		// Data Members
		CString m_Logo;

		// Core Overridables
		void OnPaint(CRect const &Work, CDC &DC);
	};

// End of File

#endif
