
#include "Intern.hpp"

#include "CloudDeviceDataSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cloud Device Data Set
//

// Dynamic Class

AfxImplementDynamicClass(CCloudDeviceDataSet, CCloudDataSet);

// Constructor

CCloudDeviceDataSet::CCloudDeviceDataSet(void)
{
	m_Mode = 1;

	m_Gps  = 1;

	m_Cell = 1;

	m_Scan = 600;
	}

// UI Update

void CCloudDeviceDataSet::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Mode" ) {

			DoEnables(pHost);
			}
		}

	CCloudDataSet::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CCloudDeviceDataSet::MakeInitData(CInitData &Init)
{
	CCloudDataSet::MakeInitData(Init);

	Init.AddByte(BYTE(m_Gps));
	Init.AddByte(BYTE(m_Cell));

	return TRUE;
	}

// Meta Data Creation

void CCloudDeviceDataSet::AddMetaData(void)
{
	CCloudDataSet::AddMetaData();

	Meta_AddInteger(Gps);
	Meta_AddInteger(Cell);

	Meta_SetName((IDS_CLOUD_DEVICE_DATA));
	}

// Implementation

void CCloudDeviceDataSet::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Gps",   m_Mode >= 1);
	pHost->EnableUI(this, "Cell",  m_Mode >= 1);
	}

// End of File
