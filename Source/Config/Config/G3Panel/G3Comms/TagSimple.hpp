
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagSimple_HPP

#define INCLUDE_TagSimple_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DataTag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Simple Tag Item
//

class DLLNOT CTagSimple : public CDataTag
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagSimple(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetTreeImage(void) const;
		UINT GetDataType(void) const;
		UINT GetTypeFlags(void) const;

		// Operations
		void UpdateTypes(BOOL fComp);

		// Evaluation
		DWORD GetProp(WORD ID, UINT Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Property Access
		DWORD FindAsText(void);
		DWORD FindLabel(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
