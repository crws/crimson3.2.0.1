
#include "Intern.hpp"

#include "CommsPortFireWire.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FireWire Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortFireWire, CCommsPort);

// Constructor

CCommsPortFireWire::CCommsPortFireWire(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding = bindFireWire;
	}

// Download Support

BOOL CCommsPortFireWire::MakeInitData(CInitData &Init)
{
	Init.AddByte(6);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// End of File
