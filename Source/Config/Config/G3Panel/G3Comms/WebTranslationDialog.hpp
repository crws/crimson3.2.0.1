
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_WebTranslationDialog_HPP

#define INCLUDE_WebTranslationDialog_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "WebTranslationThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Translation Dialog
//

class CWebTranslationDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CWebTranslationDialog( CLangManager       *pLang,
				       CStringArray const &In,
				       CUIntArray   const &To
				       );

		// Attributes
		CString GetInput (UINT uString) const;
		CString GetOutput(UINT uString, UINT uSlot) const;

	public:
		// State Machine
		enum {
			stateConnect,
			stateTranslate,
			stateDone
			};

		// Data Members
		CLangManager       * m_pLang;
		CStringArray const & m_In;
		PCUINT               m_pTo;
		UINT                 m_uTo;
		CStringArray	     m_Out;
		BOOL		     m_fDone;
		BOOL		     m_fError;
		UINT		     m_uState;
		UINT		     m_uScan;

		// Work Thread
		CWebTranslationThread m_Engine;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnPaint(void);

		// Command Handlers
		BOOL OnCommandOkay(UINT uID);
		BOOL OnUpdateDone(UINT uID);
		BOOL OnUpdateFail(UINT uID);
		BOOL OnUpdateAtQuota(UINT uID);

		// Implementation
		void SetDone(BOOL fError);
		void SetError(PCTXT pText);
		void ShowStatus(PCTXT pText);
		void TxFrame(void);
		BOOL RxFrame(void);
	};

// End of File

#endif
