
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITagControl_HPP

#define INCLUDE_UITagControl_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UIExpression.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Tag Control
//

class CUITagControl : public CUIExpression
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITagControl(void);
	};

// End of File

#endif
