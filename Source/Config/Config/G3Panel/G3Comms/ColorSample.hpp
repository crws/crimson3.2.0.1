
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ColorSample_HPP

#define INCLUDE_ColorSample_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text Color Sample Control
//

class CColorSample : public CStatic
{
	public:
		// Dynamic Class
		AfxDeclareRuntimeClass();

		// Constructor
		CColorSample(void);

		// Operations
		void SetColor(DWORD Pair);

	protected:
		// Data Members
		DWORD  m_Pair;
		CColor m_Fore;
		CColor m_Back;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnEnable(BOOL fEnable);
	};

// End of File

#endif
