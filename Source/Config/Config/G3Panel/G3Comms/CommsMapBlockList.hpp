
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsMapBlockList_HPP

#define INCLUDE_CommsMapBlockList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsMapBlock;

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block List
//

class DLLNOT CCommsMapBlockList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsMapBlockList(void);

		// Item Access
		CCommsMapBlock * GetItem(INDEX   Index) const;
		CCommsMapBlock * GetItem(CString Name ) const;

		// Operations
		BOOL Validate(BOOL fExpand);
	};

// End of File

#endif
