
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_KeyboardItem_HPP

#define INCLUDE_KeyboardItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "USBPortItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Keyboard Configuration
//

class DLLNOT CKeyboardItem : public CUSBPortItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKeyboardItem(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		UINT GetTreeImage(void) const;
		UINT GetType(void) const;

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Enable;
		UINT m_Layout;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
