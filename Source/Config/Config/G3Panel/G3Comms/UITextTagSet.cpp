
#include "Intern.hpp"

#include "UITextTagSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Tag Set
//

// Dynamic Class

AfxImplementDynamicClass(CUITextTagSet, CUITextElement);

// Constructor

CUITextTagSet::CUITextTagSet(void)
{
	m_uFlags = textEdit | textNoMerge;
	}

// Overridables

CString CUITextTagSet::OnGetAsText(void)
{
	CTagSet *pSet = (CTagSet *) m_pData->GetObject(m_pItem);

	return pSet->Format();
	}

UINT CUITextTagSet::OnSetAsText(CError &Error, CString Text)
{
	CString Prev = GetAsText();

	if( Prev != Text ) {

		CTagSet *pSet = (CTagSet *) m_pData->GetObject(m_pItem);

		return pSet->Parse(Text);
		}

	return saveSame;
	}

// End of File
