
#include "Intern.hpp"

#include "ServiceCloudGoogle.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "MqttClientOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google Cloud Client Configuration
//

// Base Class

#undef  CBaseClass

#define CBaseClass CServiceCloudJson

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudGoogle, CBaseClass);

// Constructor

CServiceCloudGoogle::CServiceCloudGoogle(void)
{
	m_bServCode = servCloudGoogle;
	
	m_pProject  = NULL;
	
	m_pRegion   = NULL;
	
	m_pRegistry = NULL;
	
	m_pDevice   = NULL;
}

// Type Access

BOOL CServiceCloudGoogle::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Project" || Tag == L"Region" || Tag == L"Registry" || Tag == L"Device" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return CBaseClass::GetTypeData(Tag, Type);
}

// UI Update

void CServiceCloudGoogle::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			DoEnables(pHost);
		}

		if( Tag == "ExpKey" ) {

			if( m_PriKey.IsEmpty() ) {

				MakeKeys();
			}

			SaveKey();
		}

		if( Tag == "CopKey" ) {

			if( m_PriKey.IsEmpty() ) {

				MakeKeys();
			}

			CopyKey();
		}

		if( Tag == "ImpKey" ) {

			if( pHost->HasWindow() ) {

				if( m_PriKey.GetCount() ) {

					CString Text = CString(IDS_DO_YOU_REALLY);

					if( pHost->GetWindow().NoYes(Text) == IDNO ) {

						return;
					}
				}
			}

			LoadKey();
		}

		if( Tag == "NewKey" ) {

			if( pHost->HasWindow() ) {

				if( m_PriKey.GetCount() ) {

					CString Text = CString(IDS_DO_YOU_REALLY_2);

					if( pHost->GetWindow().NoYes(Text) == IDNO ) {

						return;
					}
				}
			}

			MakeKeys();
		}
	}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
}

// Download Support

BOOL CServiceCloudGoogle::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pProject);
	Init.AddItem(itemVirtual, m_pRegion);
	Init.AddItem(itemVirtual, m_pRegistry);
	Init.AddItem(itemVirtual, m_pDevice);

	if( m_PriKey.IsEmpty() ) {

		MakeKeys();
	}

	Init.AddWord(WORD(m_PriKey.GetCount()));

	Init.AddData(m_PriKey.GetPointer(), m_PriKey.GetCount());

	return TRUE;
}

// Persistance

void CServiceCloudGoogle::Init(void)
{
	CServiceCloudBase::Init();

	InitOptions();
}

// Meta Data Creation

void CServiceCloudGoogle::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddVirtual(Project);
	Meta_AddVirtual(Region);
	Meta_AddVirtual(Registry);
	Meta_AddVirtual(Device);
	Meta_AddBlob(PriKey);
	Meta_AddBlob(PubKey);

	Meta_SetName((IDS_GOOGLE_MQTT));
}

// Implementation

void CServiceCloudGoogle::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Project", fEnable);

	pHost->EnableUI(this, "Region", fEnable);

	pHost->EnableUI(this, "Registry", fEnable);

	pHost->EnableUI(this, "Device", fEnable);

	pHost->EnableUI(this, "ExpKey", fEnable);

	pHost->EnableUI(this, "CopKey", fEnable);

	pHost->EnableUI(this, "ImpKey", fEnable);

	pHost->EnableUI(this, "NewKey", fEnable);
}

void CServiceCloudGoogle::InitOptions(void)
{
	m_pOpts->m_Tls      = TRUE;

	m_pOpts->m_Check    = 0;

	m_pOpts->m_Port     = 8883;

	m_pOpts->m_Advanced = 1;

	SetInitial(L"Project", m_pProject, L"\"sunny-idiom-213614\"");

	SetInitial(L"Region", m_pRegion, L"\"us-central1\"");

	SetInitial(L"Registry", m_pRegistry, L"\"test\"");

	SetInitial(L"Device", m_pDevice, L"\"device-01\"");

	m_pOpts->SetPeerName(L"mqtt.googleapis.com");

	m_pOpts->SetCredentials(L"unused", L"");
}

BOOL CServiceCloudGoogle::CopyKey(void)
{
	CClipboard Clip(afxMainWnd->GetHandle());

	if( Clip.IsValid() ) {

		Clip.Empty();

		CString Text;

		for( UINT n = 0; n < m_PubKey.GetCount(); n++ ) {

			Text += m_PubKey[n];
		}

		Clip.SetText(Text);

		afxMainWnd->Information(CString(IDS_PUBLIC_KEY_WAS));

		return TRUE;
	}

	afxMainWnd->Error(CString(IDS_UNABLE_TO_ACCESS));

	return FALSE;
}

BOOL CServiceCloudGoogle::SaveKey(void)
{
	HANDLE hFile;

	for( ;;) {

		CSaveFileDialog Dialog;

		Dialog.SetCaption(CString(IDS_EXPORT_PUBLIC_KEY));

		Dialog.SetFilter(L"PEM File (*.pem)|*.pem");

		if( Dialog.Execute(*afxMainWnd) ) {

			CFilename File = Dialog.GetFilename();

			if( File.Exists() ) {

				CString Prompt = CString(IDS_SELECTED_FILE);

				switch( afxMainWnd->YesNoCancel(File) ) {

					case IDNO:

						continue;

					case IDCANCEL:

						return FALSE;
				}

				DeleteFile(File);
			}

			if( (hFile = File.OpenWrite()) != INVALID_HANDLE_VALUE ) {

				DWORD dwDone = 0;

				WriteFile(hFile, m_PubKey.GetPointer(), m_PubKey.GetCount(), &dwDone, NULL);

				CloseHandle(hFile);

				return TRUE;
			}

			afxMainWnd->Error(CString(IDS_UNABLE_TO_WRITE_2));
		}

		return FALSE;
	}
}

BOOL CServiceCloudGoogle::LoadKey(void)
{
	HANDLE hFile;

	COpenFileDialog Dialog;

	Dialog.SetCaption(CString(IDS_IMPORT_PRIVATE));

	Dialog.SetFilter(L"PEM File (*.pem)|*.pem");

	if( Dialog.Execute(*afxMainWnd) ) {

		CFilename File = Dialog.GetFilename();

		if( (hFile = File.OpenReadSeq()) != INVALID_HANDLE_VALUE ) {

			DWORD dwDone = 0;

			UINT  uText  = GetFileSize(hFile, NULL);

			PSTR  pText  = New char[uText + 1];

			pText[uText] = 0;

			ReadFile(hFile, pText, uText, &dwDone, NULL);

			CloseHandle(hFile);

			if( LoadKey(pText, uText) ) {

				delete[] pText;

				return TRUE;
			}

			afxMainWnd->Error(CString(IDS_UNABLE_TO_PARSE));

			delete[] pText;

			return FALSE;
		}

		afxMainWnd->Error(CString(IDS_UNABLE_TO_READ_2));
	}

	return FALSE;
}

BOOL CServiceCloudGoogle::LoadKey(PCSTR pText, UINT uText)
{
	BOOL  fOkay = FALSE;

	DWORD uCode = 0;

	CryptStringToBinaryA(pText,
			     uText,
			     0,
			     NULL,
			     &uCode,
			     NULL,
			     NULL
	);

	if( uCode ) {

		PBYTE pCode = new BYTE[uCode];

		CryptStringToBinaryA(pText,
				     uText,
				     0,
				     pCode,
				     &uCode,
				     NULL,
				     NULL
		);

		DWORD uWork = 0;

		CryptDecodeObject(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
				  PKCS_RSA_PRIVATE_KEY,
				  pCode,
				  uCode,
				  0,
				  NULL,
				  &uWork
		);

		if( uWork ) {

			PBYTE pWork = new BYTE[uWork];

			CryptDecodeObject(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
					  PKCS_RSA_PRIVATE_KEY,
					  pCode,
					  uCode,
					  0,
					  pWork,
					  &uWork
			);

			BCRYPT_KEY_HANDLE hRsaAlg;

			if( !BCryptOpenAlgorithmProvider(&hRsaAlg, BCRYPT_RSA_ALGORITHM, NULL, 0) ) {

				BCRYPT_KEY_HANDLE hRsaKey;

				if( !BCryptImportKeyPair(hRsaAlg, NULL, LEGACY_RSAPRIVATE_BLOB, &hRsaKey, pWork, uWork, 0) ) {

					BCryptFinalizeKeyPair(hRsaKey, 0);

					if( MakePems(hRsaKey) ) {

						fOkay = TRUE;
					}

					BCryptDestroyKey(hRsaKey);
				}

				BCryptCloseAlgorithmProvider(hRsaAlg, 0);
			}

			delete[] pWork;
		}

		delete[] pCode;
	}

	return fOkay;
}

BOOL CServiceCloudGoogle::MakeKeys(void)
{
	BOOL fOkay = FALSE;

	BCRYPT_KEY_HANDLE hRsaAlg;

	if( !BCryptOpenAlgorithmProvider(&hRsaAlg, BCRYPT_RSA_ALGORITHM, NULL, 0) ) {

		BCRYPT_KEY_HANDLE hRsaKey;

		if( !BCryptGenerateKeyPair(hRsaAlg, &hRsaKey, 2048, 0) ) {

			if( !BCryptFinalizeKeyPair(hRsaKey, 0) ) {

				if( MakePems(hRsaKey) ) {

					fOkay = TRUE;
				}
			}

			BCryptDestroyKey(hRsaKey);
		}

		BCryptCloseAlgorithmProvider(hRsaAlg, 0);
	}

	if( !fOkay ) {

		afxMainWnd->Error(CString(IDS_UNABLE_TO));

		return FALSE;
	}

	return TRUE;
}

BOOL CServiceCloudGoogle::MakePems(BCRYPT_KEY_HANDLE hKey)
{
	BOOL  fOkay = FALSE;

	ULONG uPub  = 0;

	ULONG uPri  = 0;

	BCryptExportKey(hKey, NULL, BCRYPT_PUBLIC_KEY_BLOB, NULL, 0, &uPub, 0);

	BCryptExportKey(hKey, NULL, LEGACY_RSAPRIVATE_BLOB, NULL, 0, &uPri, 0);

	if( uPub && uPri ) {

		PBYTE pPub = new BYTE[uPub];

		PBYTE pPri = new BYTE[uPri];

		BCryptExportKey(hKey, NULL, BCRYPT_PUBLIC_KEY_BLOB, pPub, uPub, &uPub, 0);

		BCryptExportKey(hKey, NULL, LEGACY_RSAPRIVATE_BLOB, pPri, uPri, &uPri, 0);

		if( uPub && uPri ) {

			MakePem(m_PubKey, pPub, uPub, CNG_RSA_PUBLIC_KEY_BLOB);

			MakePem(m_PriKey, pPri, uPri, PKCS_RSA_PRIVATE_KEY);

			SetDirty();

			fOkay = TRUE;
		}

		delete[] pPub;

		delete[] pPri;
	}

	return fOkay;
}

BOOL CServiceCloudGoogle::MakePem(CByteArray &Pem, PCBYTE pData, UINT uData, PCSTR pEncoding)
{
	DWORD uCode = 0;

	CryptEncodeObject(X509_ASN_ENCODING,
			  pEncoding,
			  pData,
			  NULL,
			  &uCode
	);

	if( uCode ) {

		PBYTE pCode = new BYTE[uCode];

		CryptEncodeObject(X509_ASN_ENCODING,
				  pEncoding,
				  pData,
				  pCode,
				  &uCode
		);

		DWORD uText = 0;

		CryptBinaryToString(pCode,
				    uCode,
				    0,
				    NULL,
				    &uText
		);

		if( uText ) {

			PSTR pText = new char[uText];

			CryptBinaryToStringA(pCode,
					     uCode,
					     CRYPT_STRING_BASE64,
					     pText,
					     &uText
			);

			CString Output;

			PCTXT   pType = L"TYPE";

			if( pEncoding == CNG_RSA_PUBLIC_KEY_BLOB ) {

				pType = L"PUBLIC KEY";

				Output += CPrintf(L"-----BEGIN %s-----\r\n", pType);

				Output += L"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A\r\n";
			}

			if( pEncoding == PKCS_RSA_PRIVATE_KEY ) {

				pType = L"RSA PRIVATE KEY";

				Output += CPrintf(L"-----BEGIN %s-----\r\n", pType);
			}

			Output += CString(pText, uText);

			Output += CPrintf(L"-----END %s-----\r\n", pType);

			Pem.Empty();

			for( UINT n = 0; n < Output.GetLength(); n++ ) {

				Pem.Append(BYTE(Output[n]));
			}

			delete[] pText;

			delete[] pCode;

			return TRUE;
		}

		delete[] pCode;
	}

	return FALSE;
}

// End of File
