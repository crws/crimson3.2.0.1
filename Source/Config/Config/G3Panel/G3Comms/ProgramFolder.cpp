
#include "Intern.hpp"

#include "ProgramFolder.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program Folder
//

// Dynamic Class

AfxImplementDynamicClass(CProgramFolder, CFolderItem);

// Constructor

CProgramFolder::CProgramFolder(void)
{
	}

// Meta Data

void CProgramFolder::AddMetaData(void)
{
	CFolderItem::AddMetaData();

	Meta_SetName((IDS_PROGRAM_FOLDER));
	}

// End of File
