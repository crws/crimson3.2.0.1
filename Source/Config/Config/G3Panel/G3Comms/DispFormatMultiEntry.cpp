
#include "Intern.hpp"

#include "DispFormatMultiEntry.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Entry
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatMultiEntry, CCodedHost);

// Constructor

CDispFormatMultiEntry::CDispFormatMultiEntry(void)
{
	m_pData = NULL;

	m_pText = NULL;
	}

// Operations

void CDispFormatMultiEntry::Set(CString Data, CString Text)
{
	InitCoded(NULL, L"Data", m_pData, Data);

	InitCoded(NULL, L"Text", m_pText, Text);
	}

// UI Update

void CDispFormatMultiEntry::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Data" ) {

		DoEnables(pHost);

		return;
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Data Matching

BOOL CDispFormatMultiEntry::MatchData(DWORD Data, UINT Type, CString &Text)
{
	if( m_pData ) {

		DWORD Test = m_pData->Execute(Type);

		if( Data == Test ) {

			Text = m_pText->GetText();

			return TRUE;
			}
		}

	return FALSE;
	}

// Data Testing

int CDispFormatMultiEntry::CompareData(DWORD Data, UINT Type)
{
	if( m_pData ) {

		DWORD Test = m_pData->ExecVal();

		if( Type == typeInteger ) {

			if( C3INT(Test) < C3INT(Data) ) {

				return -1;
				}

			if( C3INT(Test) > C3INT(Data)  ) {

				return +1;
				}
			}

		if( Type == typeReal ) {

			if( I2R(Test) < I2R(Data) ) {

				return -1;
				}

			if( I2R(Test) > I2R(Data)  ) {

				return +1;
				}
			}
		}

	return 0;
	}

// Type Access

BOOL CDispFormatMultiEntry::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Data" ) {

		CCodedHost *pHost = (CCodedHost *) GetParent(3);

		pHost->GetTypeData(L"SubValue", Type);

		return TRUE;
		}

	if( Tag == "Text" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CDispFormatMultiEntry::UpdateTypes(BOOL fComp)
{
	UpdateType(NULL, L"Data", m_pData, fComp);
	}

// Item Naming

CString CDispFormatMultiEntry::GetHumanName(void) const
{
	CItemList *pList = (CItemList *) GetParent();

	UINT       uPos  = pList->FindItemPos(this);

	return CPrintf(IDS_ENTRY_FMT, 1 + uPos);
	}

// Download Support

BOOL CDispFormatMultiEntry::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pData);

	Init.AddItem(itemVirtual, m_pText);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatMultiEntry::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddVirtual(Data);
	Meta_AddVirtual(Text);

	Meta_SetName((IDS_MULTISTATE_ENTRY));
	}

// Implementation

void CDispFormatMultiEntry::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Text", m_pData ? TRUE : FALSE);
	}

// End of File
