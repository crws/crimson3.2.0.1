
#include "Intern.hpp"

#include "MemoryStickItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Memory Stick Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CMemoryStickItem, CUSBPortItem);

// Constructor

CMemoryStickItem::CMemoryStickItem(void)
{
	m_Enable  = FALSE;

	m_Update  = FALSE;

	m_Reboot  = TRUE;

	m_Mode1   = 0;

	m_Mode2   = 0;

	m_Dir1    = 0;

	m_Dir2    = 0;

	m_All1    = 0;

	m_All2    = 0;

	m_pFolder = NULL;

	m_pConfig = NULL;
	}

// Initial Values

void CMemoryStickItem::SetInitValues(void)
{
	CString Name = L"\"image.ci3\"";

	C3OemStrings(Name);

	SetInitial(L"Config", m_pConfig, Name);
	}

// UI Creation

BOOL CMemoryStickItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CUIStdPage(CString(IDS_GENERAL),  AfxPointerClass(this), 1));

	pList->Append(New CUIStdPage(CString(IDS_TRANSFER), AfxPointerClass(this), 2));

	return TRUE;
	}

// UI Update

void CMemoryStickItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == "Enable" || Tag == "Update" ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == "Mode1" || Tag == "Mode2" ) {

		DoEnables(pHost);

		return;
		}
	}

// Attributes

UINT CMemoryStickItem::GetTreeImage(void) const
{
	return IDI_USB_STICK;
	}

UINT CMemoryStickItem::GetType(void) const
{
	return typeStick;
	}

// Type Access

BOOL CMemoryStickItem::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Folder" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == L"Config" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CMemoryStickItem::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Enable));

	if( m_Enable ) {

		Init.AddByte(BYTE(m_Update));

		if( m_Update ) {

			Init.AddByte(BYTE(m_Reboot));

			Init.AddItem(itemVirtual, m_pFolder);

			Init.AddItem(itemVirtual, m_pConfig);
			}

		Init.AddByte(BYTE(m_Mode1));

		if( m_Mode1 ) {

			Init.AddByte(BYTE(m_Dir1));

			Init.AddByte(BYTE(m_All1));

			Init.AddText(m_Source1);

			Init.AddText(m_Target1);
			}

		Init.AddByte(BYTE(m_Mode2));

		if( m_Mode2 ) {

			Init.AddByte(BYTE(m_Dir2));

			Init.AddByte(BYTE(m_All2));

			Init.AddText(m_Source2);

			Init.AddText(m_Target2);
			}
		}

	return TRUE;
	}

// Meta Data Creation

void CMemoryStickItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Enable);
	Meta_AddInteger(Update);
	Meta_AddInteger(Reboot);
	Meta_AddVirtual(Folder);
	Meta_AddVirtual(Config);
	Meta_AddInteger(Mode1);
	Meta_AddInteger(Dir1);
	Meta_AddInteger(All1);
	Meta_AddString (Source1);
	Meta_AddString (Target1);
	Meta_AddInteger(Mode2);
	Meta_AddInteger(Dir2);
	Meta_AddInteger(All2);
	Meta_AddString (Source2);
	Meta_AddString (Target2);

	Meta_SetName((IDS_MEMORY_STICK));
	}

// Implementation

void CMemoryStickItem::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_Enable;

	BOOL fUpdate = fEnable && m_Update;

	BOOL fSync1  = fEnable && m_Mode1;

	BOOL fSync2  = fEnable && m_Mode2;

	pHost->EnableUI("Update",  fEnable);
	pHost->EnableUI("Reboot",  fUpdate);
	pHost->EnableUI("Folder",  fUpdate);
	pHost->EnableUI("Config",  fUpdate);
	pHost->EnableUI("Mode1",   fEnable);
	pHost->EnableUI("Dir1",    fSync1 );
	pHost->EnableUI("Source1", fSync1 );
	pHost->EnableUI("Target1", fSync1 );
	pHost->EnableUI("All1",    fSync1 );
	pHost->EnableUI("Mode2",   fEnable);
	pHost->EnableUI("Dir2",    fSync2 );
	pHost->EnableUI("Source2", fSync2 );
	pHost->EnableUI("Target2", fSync2 );
	pHost->EnableUI("All2",    fSync2 );
	}

// End of File
