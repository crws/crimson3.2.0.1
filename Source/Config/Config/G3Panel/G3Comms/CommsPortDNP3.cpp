
#include "Intern.hpp"

#include "CommsPortDNP3.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortDNP3, CCommsPort);

// Constructor

CCommsPortDNP3::CCommsPortDNP3(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	// MIKEG -- Where is the binding?
	}

// Attributes

BOOL CCommsPortDNP3::IsRemotable(void) const
{
	return TRUE;
	}

// Download Support

BOOL CCommsPortDNP3::MakeInitData(CInitData &Init)
{
	Init.AddByte(12);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CCommsPortDNP3::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_SetName(IDS_DNP3_PORT);
	}

// End of File
