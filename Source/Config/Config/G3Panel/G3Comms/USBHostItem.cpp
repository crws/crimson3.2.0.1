
#include "Intern.hpp"

#include "USBHostItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "USBPortItem.hpp"
#include "USBPortList.hpp"
#include "MemoryStickItem.hpp"
#include "KeyboardItem.hpp"
#include "MouseItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Host Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CUSBHostItem, CMetaItem);

// Constructor

CUSBHostItem::CUSBHostItem(void)
{
	m_pPorts = New CUSBPortList;
	}

// Persistance

void CUSBHostItem::Init(void)
{
	CMetaItem::Init();

	AddUSBPorts();
	}

void CUSBHostItem::PostLoad(void)
{
	CMetaItem::PostLoad();

	if( FALSE ) {

		CUSBPortItem *pItem;

		if( (pItem = m_pPorts->FindItem(typeMouse)) ) {

			m_pPorts->RemoveItem(pItem);
			}
		}

	if( FALSE ) {

		CUSBPortItem *pItem;

		if( (pItem = m_pPorts->FindItem(typeKeyboard)) ) {

			m_pPorts->RemoveItem(pItem);
			}
		}

	AddUSBPorts();
	}

// Attributes

UINT CUSBHostItem::GetTreeImage(void) const
{
	return IDI_USB_HOST;
	}

// Download Support

BOOL CUSBHostItem::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddWord(WORD(m_pPorts->GetItemCount()));

	if( TRUE ) {

		Init.AddByte(typeStick);

		Init.AddItem(itemSimple, m_pPorts->FindItem(typeStick));
		}

	if( TRUE ) {

		Init.AddByte(typeKeyboard);

		Init.AddItem(itemSimple, m_pPorts->FindItem(typeKeyboard));
		}

	if( TRUE ) {

		Init.AddByte(typeMouse);

		Init.AddItem(itemSimple, m_pPorts->FindItem(typeMouse));
		}

	return TRUE;
	}

// Meta Data Creation

void CUSBHostItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(Ports);

	Meta_SetName((IDS_USB_HOST_PORTS));
	}

// USB Port Creation

void CUSBHostItem::AddUSBPorts(void)
{
	if( !HasStick() ) {

		m_pPorts->AppendItem(New CMemoryStickItem);
		}

	if( !HasKeyboard() ) {

		m_pPorts->AppendItem(New CKeyboardItem);
		}

	if( !HasMouse() ) {

		m_pPorts->AppendItem(New CMouseItem);
		}
	}

// Implementation

BOOL CUSBHostItem::HasStick(void)
{
	return m_pPorts->FindItem(typeStick) != NULL;
	}

BOOL CUSBHostItem::HasKeyboard(void)
{
	return m_pPorts->FindItem(typeKeyboard) != NULL;
	}

BOOL CUSBHostItem::HasMouse(void)
{
	return m_pPorts->FindItem(typeMouse) != NULL;
	}

// End of File
