
#include "Intern.hpp"

#include "ProgramParameterList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramParameter.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program Parameter List
//

// Dynamic Class

AfxImplementDynamicClass(CProgramParameterList, CItemList);

// Constructor

CProgramParameterList::CProgramParameterList(void)
{
	m_Class = AfxRuntimeClass(CProgramParameter);
	}

// Item Access

CProgramParameter * CProgramParameterList::GetItem(INDEX Index) const
{
	return (CProgramParameter *) CItemList::GetItem(Index);
	}

CProgramParameter * CProgramParameterList::GetItem(UINT uPos) const
{
	return (CProgramParameter *) CItemList::GetItem(uPos);
	}

// Persistance

void CProgramParameterList::Init(void)
{
	CItemList::Init();

	for( UINT n = 0; n < 6; n ++ ) {

		CMetaItem *pItem = New CProgramParameter;

		AppendItem(pItem);

		pItem->SetName(CPrintf(L"Param%u", 1+n));
		}
	}

// End of File
