
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramPrototype_HPP

#define INCLUDE_ProgramPrototype_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramParameterList;

//////////////////////////////////////////////////////////////////////////
//
// Program Prototype
//

class DLLNOT CProgramPrototype : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramPrototype(void);

		// Destructor
		~CProgramPrototype(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void PostLoad(void);

		// Attributes
		CString      Format(void);
		BOOL         Parse(CString Text);
		UINT         GetParameterCount(void);
		CFuncParam * GetParameterList(void);

		// Operations
		void UpdateParamList(void);

		// Item Properties
		UINT		        m_Type;
		CProgramParameterList * m_pParams;

	protected:
		// Data Members
		CFuncParam * m_pParamList;
		UINT	     m_uParamCount;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		CString TextFromType(UINT Type);
		BOOL	TypeFromText(UINT &Type, CString Text);
		void    DoEnables(IUIHost *pHost);
	};

// End of File

#endif
