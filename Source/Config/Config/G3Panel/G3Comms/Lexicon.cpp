
#include "Intern.hpp"

#include "Lexicon.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Lexicon
//

// REV3 -- Move lexicon building into here, too.

// REV3 -- Better embedded quotation mark support.

// Constructors

CLexicon::CLexicon(void)
{
	m_fValid = FALSE;
	}

// Destructor

CLexicon::~CLexicon(void)
{
	Close();
	}

// Attributes

BOOL CLexicon::IsValid(void) const
{
	return m_fValid;
	}

CString CLexicon::GetFrom(void) const
{
	return m_From;
	}

BOOL CLexicon::Lookup(CString const &Text) const
{
	INDEX Find = m_Data.FindName(Text);

	return !m_Data.Failed(Find);
	}

CString CLexicon::Lookup(CString const &Text, CString const &To) const
{
	INDEX Find = m_Data.FindName(Text);

	if( !m_Data.Failed(Find) ) {

		INDEX Lang = m_Cols.FindName(To);

		if( !m_Cols.Failed(Lang) ) {

			CStringArray *pList = m_Data.GetData(Find);

			UINT          uPos  = m_Cols.GetData(Lang);

			return pList->GetAt(uPos);
			}
		}

	return L"";
	}

CString CLexicon::Translate(CString Text, CString const &To) const
{
	CStringNormData Data;

	CString         Done;

	Data.m_Norm = normNone;

	for( UINT p = 0; p < 3; p++ ) {

		if( !(Done = Lookup(Text, To)).IsEmpty() ) {

			CStringNormalizer::Denormalize(Done, Data);

			return Done;
			}

		if( p == 0 ) {

			CStringNormalizer::Normalize(Text, Data, normStrip);
			}

		if( p == 1 ) {

			CStringNormalizer::Normalize(Text, Data, normCase);
			}
		}

	return L"";
	}

// Operations

BOOL CLexicon::Open(CError &Error, CFilename const &Name, CString const &From)
{
	if( afxThread ) {

		afxThread->SetWaitMode(TRUE);

		afxThread->SetStatusText(CString(IDS_LOADING_LEXICON));
		}

	CTextStreamMemory Stm;

	if( Stm.LoadFromFile(Name) ) {

		UINT  uFrom = NOTHING;

		WCHAR cSep  = L',';

		if( Stm.IsWide() ) {

			cSep = L'\t';
			}

		for( UINT n = 0;; n++ ) {

			WCHAR sLine[8192] = {0};

			if( Stm.GetLine(sLine, elements(sLine)) ) {

				UINT uUsed = wstrlen(sLine);

				while( uUsed ) {

					if( sLine[uUsed-1] == '\r' || sLine[uUsed-1] == '\n' ) {

						uUsed--;

						continue;
						}

					break;
					}

				if( uUsed ) {

					CStringArray Field;

					CString Line(sLine, uUsed);

					Line.Tokenize(Field, cSep);

					StripQuotes(Field);

					if( n == 0 ) {

						UINT i = 0;

						for( UINT c = 0; c < Field.GetCount(); c++ ) {

							if( Field[c] == From ) {

								uFrom = c;
								}
							else {
								m_Cols.Insert(Field[c], i);

								i++;
								}
							}

						if( uFrom == NOTHING ) {

							if( afxThread ) {

								afxThread->SetStatusText(L"");

								afxThread->SetWaitMode(FALSE);
								}

							Error.Set(CString(IDS_SOURCE_LANGUAGE));

							return FALSE;
							}
						}
					else {
						CString Text = Field[uFrom];

						if( !Text.IsEmpty() ) {

							if( m_Data.Failed(m_Data.FindName(Text)) ) {

								CStringArray *pList = New CStringArray;

								for( UINT c = 0; c < Field.GetCount(); c++ ) {

									if( c == uFrom ) {

										continue;
										}

									pList->Append(Field[c]);
									}

								m_Data.Insert(Text, pList);
								}
							}
						}
					}

				continue;
				}

			if( afxThread ) {

				afxThread->SetStatusText(L"");

				afxThread->SetWaitMode(FALSE);
				}

			if( m_Data.IsEmpty() ) {

				Error.Set(CString(IDS_LEXICON_IS_EMPTY));

				return FALSE;
				}

			m_From   = From;

			m_fValid = TRUE;

			return TRUE;
			}
		}

	if( afxThread ) {

		afxThread->SetStatusText(L"");

		afxThread->SetWaitMode(FALSE);
		}

	Error.Set(CString(IDS_LEXICON_COULD_NOT));

	return FALSE;
	}

void CLexicon::Close(void)
{
	if( m_fValid ) {

		INDEX n = m_Data.GetHead();

		while( !m_Data.Failed(n) ) {

			delete m_Data.GetData(n);

			m_Data.GetNext(n);
			}

		m_Data.Empty();

		m_From.Empty();

		m_fValid = FALSE;
		}
	}

// Implementation

void CLexicon::StripQuotes(CStringArray &List)
{
	UINT c = List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CString Text = List[n];

		if( Text[0] == L'"' ) {

			UINT e = Text.GetLength();

			if( e > 1 ) {

				if( Text[e-1] == L'"' ) {

					Text = Text.Mid(1, e-2);
					}
				}
			}

		Text.Replace(L"\"\"", L"\"");

		List.SetAt(n, Text);
		}
	}

// End of File
