
#include "Intern.hpp"

#include "SqlQueryManagerTreeWnd_CCmdDelete.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SQL Tree Window -- Delete Command
//

// Runtime Class

AfxImplementRuntimeClass(CSqlQueryManagerTreeWnd::CCmdDelete, CStdCmd);

// Constructor

CSqlQueryManagerTreeWnd::CCmdDelete::CCmdDelete(CItem *pRoot, CString List, CItem *pNext, CMetaItem *pItem)
{
	m_Menu  = CFormat(CString(IDS_DELETE_3), pItem->GetName());

	m_Item  = pItem->GetFixedPath();

	m_Root  = GetFixedPath(pRoot);

	m_List  = List;

	m_Next  = GetFixedPath(pNext);

	m_hData = pItem->TakeSnapshot();
	}

// Destructor

CSqlQueryManagerTreeWnd::CCmdDelete::~CCmdDelete(void)
{
	GlobalFree(m_hData);
	}

// End of File
