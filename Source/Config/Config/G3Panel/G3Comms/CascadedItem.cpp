
#include "Intern.hpp"

#include "CascadedItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbTreePath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cascaded Rack Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CCascadedItem, CCascadedRackItem);

// Constructor

CCascadedItem::CCascadedItem(void)
{
	CUsbTreePath Root = { { 0, 0, 0, 0, 0, 0, 0, 0, 0 } };

	m_Slot = Root.dw;
	}

// Path Config

DWORD CCascadedItem::GetRackPath(UINT uRack) const
{
	UINT s1, s2, s3, s4;

	GetPortsRack(s1, s2, s3, s4);

	CUsbTreePath const Path[4] = {

		{ {  0,  0,  1,  0,   0,   0,   0,   0,  0  } },
		{ {  0,  0,  2,  0,  s4,   0,   0,   0,  0  } },
		{ {  0,  0,  3,  0,  s4,  s4,   0,   0,  0  } },
		{ {  0,  0,  4,  0,  s4,  s4,  s4,   0,  0  } },

		};

	return uRack < elements(Path) ? Path[uRack].dw : NOTHING;
	}

DWORD CCascadedItem::GetSlotPath(UINT uRack, UINT uSlot) const
{
	UINT s1, s2, s3, s4;

	GetPortsRack(s1, s2, s3, s4);

	CUsbTreePath const Path[12] = {

		{ {  0,  0,  2,  0,  s1,   0,   0,   0,  0  } },
		{ {  0,  0,  2,  0,  s2,   0,   0,   0,  0  } },
		{ {  0,  0,  2,  0,  s3,   0,   0,   0,  0  } },
		{ {  0,  0,  3,  0,  s4,  s1,   0,   0,  0  } },
		{ {  0,  0,  3,  0,  s4,  s2,   0,   0,  0  } },
		{ {  0,  0,  3,  0,  s4,  s3,   0,   0,  0  } },
		{ {  0,  0,  4,  0,  s4,  s4,  s1,   0,  0  } },
		{ {  0,  0,  4,  0,  s4,  s4,  s2,   0,  0  } },
		{ {  0,  0,  4,  0,  s4,  s4,  s3,   0,  0  } },
		{ {  0,  0,  5,  0,  s4,  s4,  s4,  s1,  0  } },
		{ {  0,  0,  5,  0,  s4,  s4,  s4,  s2,  0  } },
		{ {  0,  0,  5,  0,  s4,  s4,  s4,  s3,  0  } },

		};

	UINT i = uRack * 3 + uSlot;

	return i < elements(Path) ? Path[i].dw : NOTHING;
	}

// Item Naming

CString CCascadedItem::GetHumanName(void) const
{
	return L"Fixed Rack";
	}

// End of File
