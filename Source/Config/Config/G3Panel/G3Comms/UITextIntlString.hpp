
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextIntlString_HPP

#define INCLUDE_UITextIntlString_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextExprString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- International String
//

class CUITextIntlString : public CUITextExprString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextIntlString(void);

	protected:
		// Overridables
		BOOL OnExpand(CWnd &Wnd);
	};

// End of File

#endif
