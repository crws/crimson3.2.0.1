
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_OptionCardRackList_HPP

#define INCLUDE_OptionCardRackList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPortList;
class COptionCardItem;
class COptionCardRackItem;
union CUsbTreePath;

//////////////////////////////////////////////////////////////////////////
//
// Option Card Rack Item List
//

class DLLAPI COptionCardRackList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COptionCardRackList(void);

		// Item Access
		COptionCardRackItem * GetItem(INDEX Index) const;
		COptionCardRackItem * GetItem(UINT  uSlot) const;

		// Operations
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		UINT GetPowerTotal(void);
		BOOL CheckPower(void);
		UINT GetSlotCount(void);
		UINT GetRackCount(void);
		BOOL HasType(UINT uClass) const;

		// Ports List Access
		BOOL GetPortList(CCommsPortList * &pList, UINT uIndex) const;

		// Rack
		COptionCardRackItem * FindRack(CUsbTreePath const &Slot) const;
		COptionCardRackItem * FindRack(UINT Slot) const;

		// Slot
		COptionCardItem * FindSlot(CUsbTreePath const &Slot) const;
		COptionCardItem * FindSlot(UINT Slot) const;

		// Firmware
		void GetFirmwareList(CArray<UINT> &List) const;
	};

// End of File

#endif
