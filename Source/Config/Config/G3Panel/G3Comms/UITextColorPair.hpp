
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextColorPair_HPP

#define INCLUDE_UITextColorPair_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Color Pair
//

class CUITextColorPair : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextColorPair(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		BOOL    FindColor(CError &Error, CString Text, UINT &Color);
		CString NameColor(COLOR Color);
	};

// End of File

#endif
