
#include "Intern.hpp"

#include "UITextDateSep.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Date Separator
//

// Dynamic Class

AfxImplementDynamicClass(CUITextDateSep, CUITextEnum);

// Constructor

CUITextDateSep::CUITextDateSep(void)
{
	}

// Overridables

void CUITextDateSep::OnBind(void)
{
	CUITextEnum::OnBind();

	AddData(0,   CString(IDS_LOCALE_DEFAULT));

	AddData('/', CString(IDS_SLASH));

	AddData('-', CString(IDS_DASH));

	AddData('.', CString(IDS_PERIOD));

	AddData(' ', CString(IDS_SPACE));
	}

// End of File
