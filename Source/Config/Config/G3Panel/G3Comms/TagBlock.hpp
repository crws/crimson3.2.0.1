
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagBlock_HPP

#define INCLUDE_TagBlock_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Tag Block
//

class DLLNOT CTagBlock : public CMetaItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTagBlock(CTagList *pList, UINT uMin, UINT uMax);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Handle;

	protected:
		// Data Members
		CTagList * m_pList;
		UINT       m_uMin;
		UINT       m_uMax;

		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
