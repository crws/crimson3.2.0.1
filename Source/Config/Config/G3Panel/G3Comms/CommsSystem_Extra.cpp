
#include "Intern.hpp"

#include "CommsSystem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "LangManager.hpp"
#include "Lexicon.hpp"
#include "ProgramManager.hpp"
#include "ProgramItem.hpp"
#include "ProgramList.hpp"
#include "ProgramCodeItem.hpp"
#include "WebTranslationDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item -- Extra Commands
//

// Extra Commands

UINT CCommsSystem::GetExtraCount(void)
{
	return 13;
	}

CString CCommsSystem::GetExtraText(UINT uCmd)
{
	switch( uCmd ) {

		case  0: return IDS_RECOMPILE;
		case  1: return IDS_REBUILD_COMMS;
		case  2: return CString(IDS_PROGRAMIZE);
		case  3: return L"-";
		case  4: return IDS_EXPORT_LEXICON;
		case  5: return IDS_APPLY_LEXICON;
		case  6: return L"-";
		case  7: return IDS_EXPORT_STRINGS;
		case  8: return IDS_IMPORT_STRINGS;
		case  9: return L"-";
		case 10: return CString(IDS_CLEAR);
		case 11: return L"-";
		case 12: return IDS_AUTOTRANSLATE_2;
		}

	return L"";
	}

BOOL CCommsSystem::GetExtraState(UINT uCmd)
{
	BOOL fEnable = !m_pDbase->IsDownloadOnly();

	switch( uCmd ) {

		case  0: return fEnable && !m_pDbase->IsReadOnly();
		case  1: return fEnable && !m_pDbase->IsReadOnly();
		case  2: return fEnable && !m_pDbase->IsReadOnly();
		case  4: return fEnable;
		case  5: return fEnable && !m_pDbase->IsReadOnly();
		case  7: return fEnable;
		case  8: return fEnable && !m_pDbase->IsReadOnly();
		case 10: return fEnable && !m_pDbase->IsReadOnly();
		case 12: return fEnable && !m_pDbase->IsReadOnly();
		}

	return FALSE;
	}

BOOL CCommsSystem::RunExtraCmd(CWnd &Wnd, UINT uCmd)
{
	switch( uCmd ) {

		case  0: return OnRecompile(Wnd);
		case  1: return OnRebuild(Wnd);
		case  2: return OnProgramize(Wnd);
		case  4: return OnExportLexicon(Wnd);
		case  5: return OnImportLexicon(Wnd);
		case  7: return OnExportStrings(Wnd);
		case  8: return OnImportStrings(Wnd);
		case 10: return OnClearTranslations(Wnd);
		case 12: return OnAutoTranslate(Wnd);
		}

	return FALSE;
	}

// Database Utilities

BOOL CCommsSystem::OnRecompile(CWnd &Wnd)
{
	Validate(TRUE);

	return TRUE;
	}

BOOL CCommsSystem::OnRebuild(CWnd &Wnd)
{
	Rebuild(2);

	return TRUE;
	}

BOOL CCommsSystem::OnProgramize(CWnd &Wnd)
{
	CSysProxy Proxy;

	Proxy.Bind();

	if( Proxy.KillUndoList() ) {

		afxThread->SetWaitMode(TRUE);

		afxThread->SetStatusText(CString(IDS_CONVERTING));

		CCodedItem *pCoded  = m_pHeadCoded;

		UINT        uIndex  = 1;

		BOOL	    fSimple = TRUE;

		while( pCoded ) {

			if( !pCoded->IsKindOf(AfxRuntimeClass(CProgramCodeItem)) ) {

				CString Code = pCoded->GetSource(FALSE);

				if( Code.Find(L"\r") < NOTHING ) {

					CString Path = pCoded->GetFindInfo().StripToken('\n');

					CString Base;
					
					if( fSimple ) {

						Base = L"Complex";
						}
					else {
						Base = Path;

						Base.Replace(L"-", L"_");

						Base.Remove(' ');
						}

					CString Name(Base);

					if( fSimple || m_pPrograms->m_pPrograms->FindByName(Name) ) {

						if( !fSimple ) {
							
							uIndex = 1;
							}

						do {
							Name.Printf(L"%s%u", PCTXT(Base), uIndex++);

							} while( m_pPrograms->m_pPrograms->FindByName(Name) );
						}

					CString Head;

					Head += "\r// ";

					Head += CString(IDS_CALLED_FROM);

					Head += Path;

					Head += L"\r\r";

					CString Prot, Call;

					switch( pCoded->m_ReqType ? pCoded->m_ActType : typeVoid ) {

						case typeInteger:
							Prot += L"int ";
							break;

						case typeReal:
							Prot += L"float ";
							break;

						case typeString:
							Prot += L"cstring ";
							break;

						default:
							Prot += L"void ";
							break;
						}

					Prot += Name;
					Prot += L"(";

					Call += Name;
					Call += L"(";

					if( !pCoded->m_Params.IsEmpty() ) {

						CStringArray List;

						pCoded->m_Params.Tokenize(List, '/');

						if( List.GetCount() ) {

							CCodedHost *pHost = (CCodedHost *) pCoded->GetParent();

							for( UINT n = 0; n < List.GetCount(); n++ ) {

								CStringArray Part;

								List[n].Tokenize(Part, '=');

								CTypeDef Type;

								pHost->GetTypeData(Part[1], Type);

								if( n ) {

									Prot += L", ";
									Call += L", ";
									}

								switch( Type.m_Type ) {

									case typeInteger:
										Prot += L"int ";
										break;

									case typeReal:
										Prot += L"float ";
										break;

									case typeString:
										Prot += L"cstring ";
										break;
									}

								Prot += Part[0];
								Call += Part[0];
								}
							}
						else {
							Prot += L"void";
							}
						}
					else {
						Prot += L"void";
						}

					Prot += L")";
					Call += L")";

					m_pPrograms->Create(Name, Prot, Head + Code);

					pCoded->Compile(CError(FALSE), Call);
					}
				}

			pCoded = pCoded->m_pNext;
			}

		afxThread->SetStatusText(L"");

		afxThread->SetWaitMode(FALSE);

		Proxy.ItemUpdated(m_pPrograms->m_pPrograms, updateChildren);

		afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

		SetDirty();
		}

	return TRUE;
	}

// String Utilities

BOOL CCommsSystem::OnExportStrings(CWnd &Wnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(L"Strings");

	Dlg.SetCaption(CString(IDS_EXPORT_STRINGS_2));

	Dlg.SetFilter (CString(IDS_UNICODE_TEXT));

	if( Dlg.ExecAndCheck(Wnd) ) {

		CTextStreamMemory Stm;

		afxThread->SetWaitMode(TRUE);

		if( Stm.OpenSave() ) {

			WCHAR cSep = ',';

			if( Dlg.GetFilename().GetType() == L"txt" ) {

				cSep = '\t';

				Stm.SetWide();
				}

			CError Error(TRUE);

			if( DoExportStrings(Error, Stm, cSep) ) {

				afxThread->SetStatusText(CString(IDS_WRITING_TO_FILE));

				if( Stm.SaveToFile(Dlg.GetFilename(), saveRaw) ) {

					afxThread->SetStatusText(L"");

					afxThread->SetWaitMode(FALSE);

					Dlg.SaveLastPath(L"Strings");

					return TRUE;
					}

				afxThread->SetStatusText(L"");

				Error.Set(IDS_UNABLE_TO_OPEN);
				}

			Error.Show(Wnd);
			}

		afxThread->SetWaitMode(FALSE);
		}

	return FALSE;
	}

BOOL CCommsSystem::OnImportStrings(CWnd &Wnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(L"Strings");

	Dlg.SetCaption(CString(IDS_IMPORT_STRINGS_2));

	Dlg.SetFilter (CString(IDS_UNICODE_TEXT));

	if( Dlg.Execute(Wnd) ) {

		CTextStreamMemory Stm;

		afxThread->SetWaitMode(TRUE);

		afxThread->SetStatusText(CString(IDS_READING_FROM_FILE));

		CError Error(TRUE);

		if( Stm.LoadFromFile(Dlg.GetFilename()) ) {

			afxThread->SetStatusText(L"");

			Dlg.SaveLastPath(L"Strings");

			WCHAR cSep = L',';

			if( Stm.IsWide() ) {

				cSep = L'\t';
				}

			if( DoImportStrings(Error, Stm, cSep) ) {

				afxThread->SetWaitMode(FALSE);

				return TRUE;
				}
			}
		else {
			afxThread->SetStatusText(L"");

			Error.Set(IDS_UNABLE_TO_OPEN_2);
			}

		Error.Show(Wnd);

		afxThread->SetWaitMode(FALSE);
		}

	return FALSE;
	}

BOOL CCommsSystem::DoExportStrings(CError &Error, ITextStream &Stm, WCHAR cSep)
{
	CArray <CCodedText *> List;

	if( ListCodedText(List) ) {

		UINT uLangs = m_pLang->GetSlotCount();

		CStringTree Tree;

		afxThread->SetStatusText(CString(IDS_BUILDING_EXPORT));

		for( UINT t = 0; t < List.GetCount(); t++ ) {

			CCodedText *pScan = List[t];

			CString     Path  = pScan->GetExportInfo();

			CString     Line  = Path;

			CString     Init;

			Line.Expand(256);

			for( UINT uSlot = 0; uSlot < uLangs; uSlot++ ) {

				CString Text = pScan->GetText(uSlot);

				Text.TrimBoth();

				if( !uSlot ) {

					Init = Text;
					}
				else {
					if( !Text.CompareC(Init) ) {

						Line += cSep;

						continue;
						}
					}

				Line += cSep;

				Line += Text;
				}

			Tree.Insert(Line);
			}

		if( !Tree.IsEmpty() ) {

			afxThread->SetStatusText(CString(IDS_BUILDING_FILE));

			CString Line;

			Line.Empty();

			Line.Expand(256);

			Line += L"Path";

			for( UINT uSlot = 0; uSlot < uLangs; uSlot++ ) {

				CString Name = m_pLang->GetNameFromSlot(uSlot);

				Line += cSep;

				Line += Name;
				}

			Line += L"\r\n";

			Stm.PutLine(Line);

			INDEX Index = Tree.GetHead();

			while( !Tree.Failed(Index) ) {

				CString const &Text = Tree[Index];

				Stm.PutLine(Text);

				Stm.PutLine(L"\r\n");

				Tree.GetNext(Index);
				}

			afxThread->SetStatusText(L"");

			return TRUE;
			}

		afxThread->SetStatusText(L"");
		}

	Error.Set(IDS_NOTHING_TO_EXPORT);

	return FALSE;
	}

BOOL CCommsSystem::DoImportStrings(CError &Error, ITextStream &Stm, WCHAR cSep)
{
	CArray <CCodedText *> List;

	if( ListCodedText(List) ) {

		CMap <CString, CCodedText *> Map;

		if( TRUE ) {

			afxThread->SetStatusText(CString(IDS_BUILDING_IMPORT));

			for( UINT t = 0; t < List.GetCount(); t++ ) {

				CCodedText *pScan = List[t];

				CString     Path  = pScan->GetExportInfo();

				Map.Insert(Path, pScan);
				}

			afxThread->SetStatusText(L"");
			}

		CSysProxy Proxy;

		Proxy.Bind();

		if( Proxy.KillUndoList() ) {

			afxThread->SetStatusText(CString(IDS_READING_FROM_FILE));

			UINT uLangs = m_pLang->GetSlotCount();

			CStringArray Bad;

			UINT n;

			for( n = 0;; n++ ) {

				WCHAR sLine[8192] = {0};

				if( Stm.GetLine(sLine, elements(sLine)) ) {

					UINT uUsed = wstrlen(sLine);

					while( uUsed ) {

						if( sLine[uUsed-1] == '\r' || sLine[uUsed-1] == '\n' ) {

							uUsed--;

							continue;
							}

						break;
						}

					if( uUsed ) {

						if( n > 0 ) {

							CStringArray Field;

							CString Line(sLine, uUsed);

							Line.Tokenize(Field, cSep);

							if( Field.GetCount() > 1 ) {

								INDEX Index = Map.FindName(Field[0]);

								if( !Map.Failed(Index) ) {

									CCodedText *pText  = Map.GetData(Index);

									UINT        uLimit = max(Field.GetCount(), uLangs);

									for( UINT t = 1; t < uLimit; t++ ) {

										if( t == 1 || Field[t].CompareC(Field[1]) ) {

											pText->SetText( Field[t],
													t - 1,
													FALSE
													);
											}
										else {
											pText->SetText( L"",
													t - 1,
													FALSE
													);
											}
										}
									}
								else {
									if( Field[0].IsEmpty() ) {

										Bad.Append(Field[0]);
										}
									}
								}
							}
						else {
							CString Text = L"Path";

							for( UINT uSlot = 0; uSlot < uLangs; uSlot++ ) {

								Text += cSep;

								Text += m_pLang->GetNameFromSlot(uSlot);
								}

							Text += L"\r\n";

							if( wstrcmp(Text, sLine) ) {

								break;
								}
							}
						}

					continue;
					}

				break;
				}

			afxThread->SetStatusText(L"");

			if( n ) {

				if( Bad.IsEmpty() ) {

					CString Text = IDS_STRINGS_WERE;

					CWnd::GetActiveWindow().Information(Text);
					}
				else {
					CString Text = IDS_AT_LEAST_ONE;

					CClipboard Clip(CWnd::GetActiveWindow());

					if( Clip.IsValid() ) {

						Clip.Empty();

						CString Data;

						Data.Build(Bad, L"\r\n");

						Data += L"\r\n";

						Clip.SetText(Data);

						Text += L"\n\n";

						Text += CString(IDS_MISSING_KEYS_HAVE);
						}

					CWnd::GetActiveWindow().Error(Text);
					}

				afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

				SetDirty();

				return TRUE;
				}

			Error.Set(IDS_INVALID_HEADER);

			return FALSE;
			}

		return TRUE;
		}

	Error.Set(IDS_NOTHING_TO_IMPORT);

	return FALSE;
	}

// Lexicon Utilities

BOOL CCommsSystem::OnExportLexicon(CWnd &Wnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(L"Strings");

	Dlg.SetCaption(CString(IDS_EXPORT_LEXICON_2));

	Dlg.SetFilter (CString(IDS_UNICODE_TEXT));

	if( Dlg.ExecAndCheck(Wnd) ) {

		CTextStreamMemory Stm;

		afxThread->SetWaitMode(TRUE);

		if( Stm.OpenSave() ) {

			WCHAR cSep = ',';

			if( Dlg.GetFilename().GetType() == L"txt" ) {

				cSep = '\t';

				Stm.SetWide();
				}

			CError Error(TRUE);

			if( DoExportLexicon(Error, Stm, cSep) ) {

				afxThread->SetStatusText(CString(IDS_WRITING_TO_FILE));

				if( Stm.SaveToFile(Dlg.GetFilename(), saveRaw) ) {

					afxThread->SetStatusText(L"");

					afxThread->SetWaitMode(FALSE);

					Dlg.SaveLastPath(L"Strings");

					return TRUE;
					}

				afxThread->SetStatusText(L"");

				Error.Set(IDS_UNABLE_TO_OPEN);
				}

			Error.Show(Wnd);
			}

		afxThread->SetWaitMode(FALSE);
		}

	return FALSE;
	}

BOOL CCommsSystem::OnImportLexicon(CWnd &Wnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(L"Strings");

	Dlg.SetCaption(CString(IDS_APPLY_LEXICON_2));

	Dlg.SetFilter (CString(IDS_UNICODE_TEXT));

	if( Dlg.Execute(Wnd) ) {

		CLexicon Lex;

		CFilename Name = Dlg.GetFilename();

		CString   From = m_pLang->GetAbbrFromSlot(0);

		CError  Error(TRUE);

		if( Lex.Open(Error, Name, From) ) {

			Dlg.SaveLastPath(L"Strings");

			if( DoImportLexicon(Error, Lex) ) {

				return TRUE;
				}
			}

		Error.Show(Wnd);
		}

	return FALSE;
	}

BOOL CCommsSystem::DoExportLexicon(CError &Error, ITextStream &Stm, WCHAR cSep)
{
	UINT uLangs = m_pLang->GetUsedCount();

	if( uLangs > 1 ) {

		CArray <CCodedText *> List;

		if( ListCodedText(List) ) {

			CLexMap Map;

			afxThread->SetStatusText(CString(IDS_BUILDING_EXPORT));

			for( UINT t = 0; t < List.GetCount(); t++ ) {

				CCodedText *    pScan = List[t];

				CString         Text  = pScan->GetText(UINT(0));

				CStringNormData Data;

				if( CStringNormalizer::Normalize(Text, Data, normCase) ) {

					if( CStringNormalizer::IsNormal(Text) ) {

						CArray <CLexLang *> *pList;

						INDEX Find = Map.FindName(Text);

						BOOL  fHit = FALSE;

						if( !Map.Failed(Find) ) {

							pList = Map.GetData(Find);

							fHit  = TRUE;
							}
						else {
							pList = New CArray <CLexLang *>;

							for( UINT uSlot = 1; uSlot < uLangs; uSlot++ ) {

								CLexLang *pLang = New CLexLang;

								pLang->m_uMax = 0;

								pLang->m_iMax = NULL;

								pList->Append(pLang);
								}

							Map.Insert(Text, pList);

							Find = Map.FindName(Text);
							}

						for( UINT uSlot = 1; uSlot < uLangs; uSlot++ ) {

							CString   Trans = pScan->GetText(uSlot);

							CLexLang *pLang = pList->GetAt(uSlot-1);

							if( CStringNormalizer::Conormalize(Trans, Data) ) {

								if( Trans.CompareC(Text) ) {

									INDEX Find = pLang->m_Opts.FindName(Trans);

									if( !pLang->m_Opts.Failed(Find) ) {

										UINT uScore = pLang->m_Opts.GetData(Find) + 1;

										pLang->m_Opts.SetData(Find, uScore);

										if( uScore > pLang->m_uMax ) {

											pLang->m_iMax = Find;

											pLang->m_uMax = uScore;
											}
										}
									else {
										pLang->m_Opts.Insert(Trans, 1);

										pLang->m_iMax = pLang->m_Opts.FindName(Trans);

										pLang->m_uMax = 1;
										}

									fHit = TRUE;
									}
								}
							}

						if( !fHit ) {

							KillList(pList);

							Map.Remove(Find);
							}
						}
					}
				}

			if( !Map.IsEmpty() ) {

				afxThread->SetStatusText(CString(IDS_BUILDING_FILE));

				CString Line;

				Line.Empty();

				Line.Expand(256);

				for( UINT uSlot = 0; uSlot < uLangs; uSlot++ ) {

					Line += m_pLang->GetAbbrFromSlot(uSlot);

					if( uSlot < uLangs - 1 ) {

						Line += cSep;
						}
					}

				Line += L"\r\n";

				Stm.PutLine(Line);

				INDEX Index = Map.GetHead();

				while( !Map.Failed(Index) ) {

					CString              Used = Map.GetName(Index);

					CArray <CLexLang *> *pList = Map.GetData(Index);

					Line.Empty();

					Line.Expand(256);

					Line += Used;

					Line += cSep;

					for( UINT uSlot = 1; uSlot < uLangs; uSlot++ ) {

						CLexLang *pLang = pList->GetAt(uSlot-1);

						if( pLang->m_iMax ) {

							CString Trans = pLang->m_Opts.GetName(pLang->m_iMax);

							Line += Trans;
							}

						if( uSlot < uLangs - 1 ) {

							Line += cSep;
							}
						}

					Line += L"\r\n";

					Stm.PutLine(Line);

					KillList(pList);

					Map.GetNext(Index);
					}

				afxThread->SetStatusText(L"");

				return TRUE;
				}

			afxThread->SetStatusText(L"");
			}
		}

	Error.Set(IDS_NOTHING_TO_EXPORT);

	return FALSE;
	}

BOOL CCommsSystem::DoImportLexicon(CError &Error, CLexicon &Lex)
{
	UINT uLangs = m_pLang->GetUsedCount();

	if( uLangs > 1 ) {

		CArray <CCodedText *> List;

		if( ListCodedText(List) ) {

			CString Prompt = IDS_LEAVE_EXISTING;

			UINT    uMode  = CWnd::GetActiveWindow().YesNoCancel(Prompt);

			if( uMode != IDCANCEL ) {

				CSysProxy Proxy;

				Proxy.Bind();

				if( Proxy.KillUndoList() ) {

					afxThread->SetStatusText(CString(IDS_APPLYING_LEXICON));

					afxThread->SetWaitMode(TRUE);

					for( UINT t = 0; t < List.GetCount(); t++ ) {

						CCodedText *pScan = List[t];

						CString     Text  = pScan->GetText(UINT(0));

						if( !Text.IsEmpty() ) {

							for( UINT uSlot = 1; uSlot < uLangs; uSlot++ ) {

								if( uMode == IDYES ) {

									if( pScan->HasText(uSlot) ) {

										continue;
										}
									}

								CString Lang = m_pLang->GetAbbrFromSlot(uSlot);

								CString Look = Lex.Translate(Text, Lang);

								pScan->SetText(Look, uSlot, FALSE);
								}
							}
						}

					afxThread->SetWaitMode(FALSE);

					afxThread->SetStatusText(L"");

					CString Text = CString(IDS_LEXICON_WAS);

					CWnd::GetActiveWindow().Information(Text);

					afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

					SetDirty();

					return TRUE;
					}
				}

			return FALSE;
			}
		}

	Error.Set(IDS_NOTHING_TO);

	return FALSE;
	}

void CCommsSystem::KillList(CArray <CLexLang *> *pList)
{
	for( UINT n = 0; n < pList->GetCount(); n++ ) {

		CLexLang *pLang = pList->GetAt(n);

		delete pLang;
		}

	delete pList;
	}

void CCommsSystem::Trim(CString &Base)
{
	Base.TrimBoth();

	Base.Replace('|', ' ');

	UINT n1 = Base.GetLength();

	while( n1 ) {

		WCHAR c = Base[n1-1];

		if( isdigit(c) || ispunct(c) || isspace(c) ) {

			Base.Delete(--n1, 1);

			continue;
			}

		break;
		}
	}

void CCommsSystem::Trim(CString &Trans, CString const &Base)
{
	Trans.TrimBoth();

	Trans.Replace('|', ' ');

	UINT n1 = Trans.GetLength();

	UINT n2 = Base.GetLength();

	while( n1 ) {

		WCHAR c = Trans[n1-1];

		if( Base[n2-1] ) {

			if( isdigit(c) || ispunct(c) || isspace(c) ) {

				Trans.Delete(--n1, 1);

				n2--;

				continue;
				}
			}

		break;
		}
	}

// Clear Translations

BOOL CCommsSystem::OnClearTranslations(CWnd &Wnd)
{
	CArray <CCodedText *> List;

	if( ListCodedText(List) ) {

		CSysProxy Proxy;

		Proxy.Bind();

		if( Proxy.KillUndoList() ) {

			afxThread->SetWaitMode(TRUE);

			afxThread->SetStatusText(CString(IDS_CLEARING));

			for( UINT t = 0; t < List.GetCount(); t++ ) {

				CCodedText *pText = List[t];

				pText->SetText(pText->GetText(0U), TRUE);
				}

			afxThread->SetStatusText(L"");

			afxThread->SetWaitMode(FALSE);

			CString Text = CString(IDS_ALL_TRANSLATIONS);

			CWnd::GetActiveWindow().Information(Text);

			afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

			SetDirty();

			return TRUE;
			}

		return FALSE;
		}

	CWnd::GetActiveWindow().Error(CString(IDS_NOTHING_TO_CLEAR));

	return FALSE;
	}

// Auto Translation

BOOL CCommsSystem::OnAutoTranslate(CWnd &Wnd)
{
	CUIntArray To;

	for( UINT n = 1; n < m_pLang->GetUsedCount(); n++ ) {

		if( m_pLang->IsSlotUsed(n) ) {

			To.Append(n);
			}
		}

	if( To.GetCount() ) {

		CArray <CCodedText *> List;

		if( ListCodedText(List) ) {

			CMap <CCaseString, UINT> Map;

			CStringArray	         In;

			for( UINT t = 0; t < List.GetCount(); t++ ) {

				CCodedText *pText = List[t];

				CString     Text  = pText->GetText(0U);

				if( Map.Failed(Map.FindName(Text)) ) {

					UINT uPos = In.Append(Text);

					Map.Insert(Text, uPos);
					}
				}

			CString Prompt;

			Prompt.Printf( CString(IDS_AUTOTRANSLATE_FMT),
				       In.GetCount(), (In.GetCount()==1)?L"":L"s",
				       To.GetCount(), (To.GetCount()==1)?L"":L"s"
				       );

			if( CWnd::GetActiveWindow().YesNo(Prompt) == IDYES ) {

				CString Prompt = IDS_LEAVE_EXISTING;

				UINT    uMode  = CWnd::GetActiveWindow().YesNoCancel(Prompt);

				if( uMode != IDCANCEL ) {

					CWebTranslationDialog Dlg(m_pLang, In, To);

					if( Dlg.Execute() ) {

						CSysProxy Proxy;

						Proxy.Bind();

						if( Proxy.KillUndoList() ) {

							afxThread->SetWaitMode(TRUE);

							afxThread->SetStatusText(CString(IDS_APPLYING));

							for( UINT t = 0; t < List.GetCount(); t++ ) {

								CCodedText *pText = List[t];

								CString     Text  = pText->GetText(0U);

								INDEX       Index = Map.FindName(Text);

								if( !Map.Failed(Index) ) {

									UINT uPos = Map.GetData(Index);

									for( UINT n = 0; n< To.GetCount(); n++ ) {

										if( uMode == IDYES ) {

											if( pText->HasText(To[n]) ) {

												continue;
												}
											}

										CString Out = Dlg.GetOutput(uPos, n);

										pText->SetText(Out, To[n], FALSE);
										}
									}
								}

							afxThread->SetStatusText(L"");

							afxThread->SetWaitMode(FALSE);

							CString Text = IDS_DATABASE_HAS_BEEN;

							CWnd::GetActiveWindow().Information(Text);

							afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

							SetDirty();
							}

						return TRUE;
						}
					}
				}

			return FALSE;
			}
		}

	CWnd::GetActiveWindow().Error(CString(IDS_NOTHING_TO));

	return FALSE;
	}

// Implementation

BOOL CCommsSystem::ListCodedText(CArray <CCodedText *> &List)
{
	afxThread->SetWaitMode(TRUE);

	afxThread->SetStatusText(CString(IDS_BUILDING_LIST_OF));

	CLASS       Trans = AfxNamedClass(L"CUITextIntlString");

	CLASS       Prog  = AfxRuntimeClass(CProgramItem);

	CLASS       UI    = AfxRuntimeClass(CUIItem);

	CCodedItem *pCode = New CCodedItem;

	CCodedItem *pText = New CCodedText;

	CCodedItem *pScan = m_pHeadCoded;

	while( pScan ) {

		if( pScan->GetType() == typeString ) {

			CString Init;

			if( TRUE ) {

				CItem *pItem = pScan->GetParent();

				if( !pItem->IsKindOf(Prog) && pItem->IsKindOf(UI) ) {

					// NOTE -- This is a hack to change the class
					// of items that were created with the wrong
					// type. The end result is that an item will
					// use CCodedText iff it is translatable. We
					// can disable this once all database are ok.

					CUIItem *pUI = (CUIItem *) pItem;

					if( pUI->GetSubItemClass(pScan) == Trans ) {

						FixClass(pScan, pText);
						}
					else
						FixClass(pScan, pCode);
					}
				}

			if( pScan->GetFlags() & flagConstant ) {

				if( pScan->IsKindOf(AfxRuntimeClass(CCodedText)) ) {

					CCodedText *pText = (CCodedText *) pScan;

					CString     Text  = pText->GetText(0U);

					if( !Text.IsEmpty() ) {

						List.Append(pText);
						}
					}
				}
			}

		pScan = pScan->m_pNext;
		}

	delete pCode;

	delete pText;

	afxThread->SetStatusText(L"");

	afxThread->SetWaitMode(FALSE);

	return !List.IsEmpty();
	}

void CCommsSystem::FixClass(CCodedItem *pCode, CCodedItem *pFrom)
{
	// NOTE -- We can change between these classes
	// just be copying the vtable pointers as the
	// rest of the classes are identical ie. the
	// derrived class contains no new data members.

	*PDWORD(pCode) = *PDWORD(pFrom);
	}

// End of File
