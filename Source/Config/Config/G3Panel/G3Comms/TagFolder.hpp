
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagFolder_HPP

#define INCLUDE_TagFolder_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Tag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Folder
//

class DLLNOT CTagFolder : public CTag
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagFolder(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Server Access
		INameServer * GetNameServer(CNameServer *pName);
		IDataServer * GetDataServer(CDataServer *pData);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		UINT GetDataType(void) const;

		// Operations
		void UpdateTypes(BOOL fComp);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Locked;

	protected:
		// Data Members
		BOOL    m_fLegacy;

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
