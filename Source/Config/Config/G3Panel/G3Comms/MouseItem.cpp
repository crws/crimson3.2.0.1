
#include "Intern.hpp"

#include "MouseItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Mouse Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CMouseItem, CUSBPortItem);

// Constructor

CMouseItem::CMouseItem(void)
{
	m_Enable = 0;

	m_Rate   = 100;

	m_Hide   = 30;
	}

// UI Update

void CMouseItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == L"Enable" ) {

			pHost->EnableUI(L"Rate", m_Enable);

			pHost->EnableUI(L"Hide", m_Enable);
			}
		}

	CUSBPortItem::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

UINT CMouseItem::GetTreeImage(void) const
{
	return IDI_MOUSE;
	}

UINT CMouseItem::GetType(void) const
{
	return typeMouse;
	}

// Download Support

BOOL CMouseItem::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Enable));
	Init.AddWord(WORD(m_Rate));
	Init.AddWord(WORD(m_Hide));

	return TRUE;
	}

// Meta Data Creation

void CMouseItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Enable);
	Meta_AddInteger(Rate);
	Meta_AddInteger(Hide);

	Meta_SetName(IDS_MOUSE);
	}

// End of File
