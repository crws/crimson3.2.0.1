
#include "Intern.hpp"

#include "HttpClientConnectionOptions.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Connection Options
//

// Base Class

#undef  CBaseClass

#define CBaseClass CHttpConnectionOptions

// Dynamic Class

AfxImplementDynamicClass(CHttpClientConnectionOptions, CBaseClass);

// Constructor

CHttpClientConnectionOptions::CHttpClientConnectionOptions(void)
{
	m_Check       = 0;

	m_ConnTimeout = 30;

	m_CompRequest = FALSE;

	m_CompReply   = TRUE;

 	m_Ver         = 11;
	}

// UI Update

void CHttpClientConnectionOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Advanced" || Tag == "Tls" ) {

			DoEnables(pHost);
			}
		}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CHttpClientConnectionOptions::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddByte(BYTE(m_Check));
	Init.AddWord(WORD(m_ConnTimeout));
	Init.AddByte(BYTE(m_CompRequest));
	Init.AddByte(BYTE(m_CompReply));
	Init.AddByte(BYTE(m_Ver));

	return TRUE;
	}

// Meta Data Creation

void CHttpClientConnectionOptions::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(Check);
	Meta_AddInteger(ConnTimeout);
	Meta_AddInteger(CompRequest);
	Meta_AddInteger(CompReply);
	Meta_AddInteger(Ver);

	Meta_SetName((IDS_HTTP_CLIENT));
	}

// Implementation

void CHttpClientConnectionOptions::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Check", m_Tls > 0);

	pHost->EnableUI(this, "ConnTimeout", m_Advanced);

	pHost->EnableUI(this, "CompRequest", m_Advanced);

	pHost->EnableUI(this, "CompReply",   m_Advanced);
	}

// End of File
