
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceItem_HPP

#define INCLUDE_ServiceItem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Service IDs
//

enum
{
	servCloudCumulocity = 8,
	servCloudAws        = 9,
	servOpcUa           = 10,
	servCloudSparkplug  = 11,
	servCloudAzure      = 12,
	servCloudGeneric    = 13,
	servCloudGoogle	    = 14,
	servCloudUbidots    = 15,
	};

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Service Base Class
//

class DLLNOT CServiceItem : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CServiceItem(void);

		// Attributes
		virtual BOOL IsEnabled(void) const;
		virtual UINT GetTreeImage(void) const;

		// Conversion
		virtual void PostConvert(void);

		// Item Properties
		UINT m_Handle;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
