
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextProgram_HPP

#define INCLUDE_UITextProgram_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Program Item
//

class CUITextProgram : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProgram(void);

		// Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

	protected:
		// Implementation
		void FindReqType(CTypeDef &Type);
	};

// End of File

#endif
