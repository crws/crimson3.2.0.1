
#include "Intern.hpp"

#include "TagNavTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsManager.hpp"
#include "CommsSystem.hpp"
#include "DataTag.hpp"
#include "TagExport.hpp"
#include "TagFlag.hpp"
#include "TagFolder.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"
#include "TagNavTreeWnd.hpp"
#include "TagNumeric.hpp"
#include "TagPasteSpecialDialog.hpp"
#include "TagSimple.hpp"
#include "TagString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CTagNavTreeWnd, CNavTreeWnd);

// Sort Context

CTagNavTreeWnd * CTagNavTreeWnd::m_pSort = NULL;

// Constructor

CTagNavTreeWnd::CTagNavTreeWnd(void) : CNavTreeWnd(L"Tags",
						   AfxRuntimeClass(CTagFolder),
						   AfxRuntimeClass(CDataTag)
)
{
	m_Empty    = CString(IDS_CLICK_ON_NEW_3);

	m_fInitSel = FALSE;

	m_fRefresh = FALSE;

	m_uMany    = 0;

	m_cfSpec   = WORD(RegisterClipboardFormat(L"C3.1 Tag Special"));

	m_pTree->SetMultiple(TRUE);

	m_Accel.Create(L"TagNavMenu");
}

// IUpdate

HRESULT CTagNavTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateLocked ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		LoadTreeImages(hItem);

		return S_OK;
	}

	return CNavTreeWnd::ItemUpdated(pItem, uType);
}

// Overridables

void CTagNavTreeWnd::OnAttach(void)
{
	CNavTreeWnd::OnAttach();

	m_pSystem  = (CCommsSystem *) m_pItem->GetParent();

	m_pManager = (CTagManager  *) m_pItem;

	m_pTags    = m_pManager->m_pTags;
}

void CTagNavTreeWnd::OnExec(CCmd *pCmd)
{
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		if( m_fLocked && m_fRefresh ) {

			m_System.ItemUpdated(m_pSelect, updateContents);

			m_fRefresh = FALSE;
		}
	}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		HTREEITEM hItem = m_MapFixed[pCmd->m_Item];

		if( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			if( pItem ) {

				((CCmdItem *) pCmd)->Exec(pItem);

				if( pItem->IsKindOf(AfxRuntimeClass(CTag)) ) {

					CTag *pTag = (CTag *) pItem;

					UINT  uPos = pTag->GetIndex();

					m_pSystem->TagCheck(uPos, FALSE);
				}

				ItemUpdated(pItem, updateProps);

				m_fRefresh = TRUE;
			}
		}
	}

	CNavTreeWnd::OnExec(pCmd);
}

void CTagNavTreeWnd::OnUndo(CCmd *pCmd)
{
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		if( m_fLocked && m_fRefresh ) {

			m_System.ItemUpdated(m_pSelect, updateContents);

			m_fRefresh = FALSE;
		}
	}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		HTREEITEM hItem = m_MapFixed[pCmd->m_Item];

		if( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			if( pItem ) {

				((CCmdItem *) pCmd)->Undo(pItem);

				if( pItem->IsKindOf(AfxRuntimeClass(CTag)) ) {

					CTag *pTag = (CTag *) pItem;

					UINT  uPos = pTag->GetIndex();

					m_pSystem->TagCheck(uPos, FALSE);
				}

				ItemUpdated(pItem, updateProps);

				m_fRefresh = TRUE;
			}
		}
	}

	CNavTreeWnd::OnUndo(pCmd);
}

// Message Map

AfxMessageMap(CTagNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, TVN_PICKITEM, OnTreePickItem)
	AfxDispatchNotify(100, NM_CUSTOMDRAW, OnTreeCustomDraw)

	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)
	AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM, OnItemControl)
	AfxDispatchCommandType(IDM_ITEM, OnItemCommand)
	AfxDispatchGetInfoType(IDM_TAGS, OnTagsGetInfo)
	AfxDispatchControlType(IDM_TAGS, OnTagsControl)
	AfxDispatchCommandType(IDM_TAGS, OnTagsCommand)

	AfxMessageEnd(CTagNavTreeWnd)
};

// Accelerators

BOOL CTagNavTreeWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
}

// Message Handlers

void CTagNavTreeWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {

		Menu.MergeMenu(CMenu(L"TagNavMenu"));
	}
}

void CTagNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"TagNavTreeTool"));
	}
}

// Notification Handlers

void CTagNavTreeWnd::OnTreePickItem(UINT uID, NMTREEVIEW &Info)
{
	afxThread->SetStatusText(L"");

	SetMode(modeSelect);

	if( Info.itemNew.hItem ) {

		CUIItem *pPick = (CUIItem *) GetItemPtr(Info.itemNew.hItem);

		if( pPick ) {

			if( pPick->IsKindOf(AfxRuntimeClass(CTag)) ) {

				OnCopyFrom(pPick);
			}
		}
	}
}

UINT CTagNavTreeWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
	}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMTVCUSTOMDRAW &Extra  = (NMTVCUSTOMDRAW &) Info;

		HTREEITEM       hItem = HTREEITEM(Info.dwItemSpec);

		CColor          Color = Extra.clrText;

		if( hItem ) {

			CItem *pItem = (CItem *) Info.lItemlParam;

			if( pItem->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

				CTag *pTag = (CTag *) pItem;

				if( pTag->IsBroken() ) {

					if( pTag->IsCircular() ) {

						Color = CColor(128, 0, 0);
					}
					else
						Color = CColor(255, 0, 0);
				}
			}
		}

		if( hItem ) {

			UINT uTest = m_pTree->GetItemState(hItem, NOTHING);

			if( uTest & TVIS_DROPHILITED ) {

				Extra.clrTextBk = afxColor(Orange2);

				Extra.clrText   = Color;

				SelectObject(Info.hdc, m_pTree->GetFont());

				return CDRF_NEWFONT;
			}
		}

		if( Info.uItemState & CDIS_HOT ) {

			if( Info.uItemState & CDIS_SELECTED ) {

				Extra.clrTextBk = afxColor(Orange2);
			}
			else
				Extra.clrTextBk = afxColor(Orange3);

			Extra.clrText = Color;

			SelectObject(Info.hdc, m_pTree->GetFont());

			return CDRF_NEWFONT;
		}

		Extra.clrText = Color;
	}

	return 0;
}

// Command Handlers

BOOL CTagNavTreeWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_SMART_DUP:

			Src.EnableItem(CanEditDuplicate());

			break;

		case IDM_EDIT_PASTE_SPECIAL:

			Src.EnableItem(CanEditPasteSpecial());

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CTagNavTreeWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_SMART_DUP:

			OnEditDuplicate();

			break;

		case IDM_EDIT_PASTE_SPECIAL:

			OnEditPasteSpecial();

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CTagNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID == IDM_ITEM_NEW_FOLDER ) {

		Info.m_Image = MAKELONG(0x0016, 0x1000);

		return TRUE;
	}

	if( uID == IDM_ITEM_SORT_DEV ) {

		return FALSE;
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{

	// TODO -- I don't like this...

	if( uID == IDM_ITEM_LOCK ) {

		if( IsTagLocked(m_hSelect) ) {

			CString Verb = !IsTagLocked(m_pTree->GetParent(m_hSelect)) ? CString(IDS_UNLOCK) : CString(IDS_LOCK);

			Src.SetItemText(Verb);

			Src.EnableItem(!IsReadOnly() && !IsTagLocked(m_pTree->GetParent(m_hSelect)));
		}
		else {
			Src.SetItemText(CString(IDS_LOCK));

			Src.EnableItem(!IsReadOnly() && IsFolder());
		}

		return TRUE;
	}

	if( uID == IDM_ITEM_USAGE ) {

		Src.EnableItem(!m_fMulti && m_pSelect->IsKindOf(m_Class));

		return TRUE;
	}

	if( uID == IDM_ITEM_SORT_DEV ) {

		if( !m_fMulti ) {

			Src.EnableItem(!IsReadOnly() && IsParent());
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::OnItemCommand(UINT uID)
{
	if( uID == IDM_ITEM_LOCK ) {

		BOOL fLock = !IsTagLocked(m_hSelect);

		CCmd *pCmd = New CCmdLock(m_pSelect, fLock);

		m_System.ExecCmd(pCmd);

		return TRUE;
	}

	if( uID == IDM_ITEM_USAGE ) {

		m_pSystem->FindTagUsage(m_pNamed->GetName(),
					m_pNamed->GetIndex()
		);

		return TRUE;
	}

	if( uID == IDM_ITEM_SORT_DEV ) {

		m_pSort = this;

		OnItemSort(SortByDevice);

		return TRUE;
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::OnTagsGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =

	{ IDM_TAGS_NEW_NUMERIC,  MAKELONG(0x0002, 0x4000),
		IDM_TAGS_NEW_FLAG,     MAKELONG(0x0003, 0x4000),
		IDM_TAGS_NEW_STRING,   MAKELONG(0x0004, 0x4000),
		IDM_TAGS_NEW_SIMPLE,   MAKELONG(0x0005, 0x4000),
		IDM_TAGS_NEW_FOLDER,   MAKELONG(0x0006, 0x4000),
		IDM_TAGS_ADD_TO_WATCH, MAKELONG(0x0036, 0x1000),

	};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
		}
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::OnTagsControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_TAGS_NEW_SIMPLE:
		case IDM_TAGS_NEW_NUMERIC:
		case IDM_TAGS_NEW_FLAG:
		case IDM_TAGS_NEW_STRING:
		case IDM_TAGS_NEW_FOLDER:

			Src.EnableItem(!m_fMulti && !IsItemLocked(m_hSelect));

			return TRUE;

		case IDM_TAGS_COPY_ADDRESS:
		case IDM_TAGS_COPY_SCALE:
		case IDM_TAGS_COPY_FORMAT:
		case IDM_TAGS_COPY_COLOR:
		case IDM_TAGS_COPY_ALARMS:
		case IDM_TAGS_COPY_TRIGGERS:
		case IDM_TAGS_COPY_SECURITY:
		case IDM_TAGS_COPY_SELECT:

			Src.EnableItem(!IsItemLocked(m_hSelect) && !IsRootSelected());

			return TRUE;

		case IDM_TAGS_EXPORT:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_TAGS_IMPORT:

			Src.EnableItem(!IsReadOnly());

			return TRUE;

		case IDM_TAGS_FIND_ALARMS:
		case IDM_TAGS_FIND_TRIGGERS:
		case IDM_TAGS_FIND_LOCKED:

			if( m_pSystem->GetDatabase()->GetSoftwareGroup() >= SW_GROUP_3B ) {

				Src.EnableItem(IsRootSelected());
			}

			return TRUE;

		case IDM_TAGS_FIND_UNUSED:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_TAGS_ADD_TO_WATCH:

			Src.EnableItem(!IsRootSelected() && CanTagsAddToWatch());

			return TRUE;
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::OnTagsCommand(UINT uID)
{
	switch( uID ) {

		case IDM_TAGS_NEW_SIMPLE:

			OnTagsNewTag(AfxRuntimeClass(CTagSimple));

			return TRUE;

		case IDM_TAGS_NEW_NUMERIC:

			OnTagsNewTag(AfxRuntimeClass(CTagNumeric));

			return TRUE;

		case IDM_TAGS_NEW_FLAG:

			OnTagsNewTag(AfxRuntimeClass(CTagFlag));

			return TRUE;

		case IDM_TAGS_NEW_STRING:

			OnTagsNewTag(AfxRuntimeClass(CTagString));

			return TRUE;

		case IDM_TAGS_NEW_FOLDER:

			OnItemCreateFolder();

			return TRUE;

		case IDM_ITEM_LOCK:

			return TRUE;

		case IDM_TAGS_COPY_ADDRESS:
		case IDM_TAGS_COPY_SCALE:
		case IDM_TAGS_COPY_FORMAT:
		case IDM_TAGS_COPY_COLOR:
		case IDM_TAGS_COPY_ALARMS:
		case IDM_TAGS_COPY_TRIGGERS:
		case IDM_TAGS_COPY_SECURITY:
		case IDM_TAGS_COPY_SELECT:

			m_uPick = uID;

			SetMode(modePick);

			return TRUE;

		case IDM_TAGS_EXPORT:

			OnTagsExport();

			return TRUE;

		case IDM_TAGS_IMPORT:

			OnTagsImport();

			return TRUE;

		case IDM_TAGS_FIND_ALARMS:

			OnTagsFindAlarms();

			return TRUE;

		case IDM_TAGS_FIND_TRIGGERS:

			OnTagsFindTriggers();

			return TRUE;

		case IDM_TAGS_FIND_LOCKED:

			OnTagsFindLocked();

			return TRUE;

		case IDM_TAGS_FIND_UNUSED:

			OnTagsFindUnused();

			return TRUE;

		case IDM_TAGS_ADD_TO_WATCH:

			OnTagsAddToWatch();

			return TRUE;
	}

	return FALSE;
}

// Tag Commands

BOOL CTagNavTreeWnd::CanTagsAddToWatch(void)
{
	return TRUE;
}

void CTagNavTreeWnd::OnTagsNewTag(CLASS Class)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(CString(IDS_FORMAT_7), n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, Class);

			m_System.ExecCmd(pCmd);

			break;
		}
	}
}

BOOL CTagNavTreeWnd::OnTagsExport(void)
{
	// REV3 -- Option to export all tags in a given folder.

	// REV3 -- Option to split files by tag type.

	// REV3 -- Option to create one huge flat file?

	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(L"Tags");

	Dlg.SetCaption(CString(IDS_EXPORT_TAGS_2));

	Dlg.SetFilter(CString(IDS_UNICODE_TEXT));

	if( Dlg.ExecAndCheck() ) {

		CTextStreamMemory Stm;

		afxThread->SetWaitMode(TRUE);

		if( Stm.OpenSave() ) {

			CTagExport Helper(m_pTags);

			WCHAR      cSep = ',';

			if( Dlg.GetFilename().GetType() == L"txt" ) {

				cSep = '\t';

				Stm.SetWide();
			}

			if( !Helper.Export(Stm, cSep) ) {

				CWnd &Wnd = CWnd::GetActiveWindow();

				CString Text(IDS_NOTHING_TO_EXPORT);

				Wnd.Error(Text);
			}
			else {
				afxThread->SetStatusText(CString(IDS_WRITING_TO_FILE));

				if( Stm.SaveToFile(Dlg.GetFilename(), saveRaw) ) {

					afxThread->SetStatusText(L"");

					afxThread->SetWaitMode(FALSE);

					Dlg.SaveLastPath(L"Tags");

					return TRUE;
				}

				afxThread->SetStatusText(L"");

				CWnd &Wnd = CWnd::GetActiveWindow();

				CString Text = IDS_UNABLE_TO_OPEN;

				Wnd.Error(Text);
			}
		}

		afxThread->SetWaitMode(FALSE);

		return FALSE;
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::OnTagsImport(void)
{
	// REV3 -- Option to import multiple files at once.

	// REV3 -- Similar command to read custom csv files?

	COpenFileDialog Dlg;

	Dlg.LoadLastPath(L"Tags");

	Dlg.SetCaption(CString(IDS_IMPORT_TAGS_2));

	UINT uTargetIDS = HasCustomImportIDS();	// bit 4 = Control Techniques

	Dlg.SetFilter(CString(IDS_UNICODE_TEXT));

	if( Dlg.Execute() ) {

		CTextStreamMemory Stm;

		afxThread->SetWaitMode(TRUE);

		afxThread->SetStatusText(CString(IDS_READING_FROM_FILE));

		CString sFileName = Dlg.GetFilename();

		if( Stm.LoadFromFile(sFileName) ) {

			afxThread->SetStatusText(L"");

			if( m_System.KillUndoList() ) {

				afxMainWnd->UpdateWindow();

				CTagExport Helper(m_pTags);

				CLongArray Lines;

				WCHAR      cSep = L',';

				if( Stm.IsWide() ) {

					cSep = L'\t';
				}

				if( Helper.SetCustomImport(uTargetIDS, sFileName, m_pSystem) ) {

					if( !Helper.Import(Stm, cSep, Lines) ) {

						CString Text;

						Text = CString(IDS_AT_LEAST_ONE_3);

						CWnd &Wnd = CWnd::GetActiveWindow();

						CClipboard Clip(Wnd);

						if( Clip.IsValid() ) {

							Clip.Empty();

							CString Data;

							for( UINT n = 0; n < Lines.GetCount(); n++ ) {

								Data += CPrintf("Line %u\r\n", Lines[n]);
							}

							Clip.SetText(Data);

							Text += L"\n\n";

							Text += CString(IDS_LINE_NUMBERS_HAVE);
						}

						Wnd.Error(Text);
					}

					m_pItem->SetDirty();

					m_System.ItemUpdated(m_pList, updateChildren);

					RefreshTree();

					// REV3 -- Block other recompiles during import process?

					m_pSystem->Validate(TRUE);
				}

				Dlg.SaveLastPath(L"Tags");
			}
		}

		afxThread->SetWaitMode(FALSE);

		return TRUE;
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::OnTagsFindAlarms(void)
{
	CStringArray List;

	INDEX n = m_pTags->GetHead();

	while( !m_pTags->Failed(n) ) {

		m_pTags->GetItem(n)->FindAlarms(List);

		m_pTags->GetNext(n);
	}

	if( List.IsEmpty() ) {

		Information(CString(IDS_THERE_ARE_NO));

		return FALSE;
	}

	m_System.SetFindList(CString(IDS_LIST_OF_DEFINED),
			     List,
			     TRUE
	);

	return TRUE;
}

BOOL CTagNavTreeWnd::OnTagsFindTriggers(void)
{
	CStringArray List;

	INDEX n = m_pTags->GetHead();

	while( !m_pTags->Failed(n) ) {

		m_pTags->GetItem(n)->FindTriggers(List);

		m_pTags->GetNext(n);
	}

	if( List.IsEmpty() ) {

		Information(CString(IDS_THERE_ARE_NO_2));

		return FALSE;
	}

	m_System.SetFindList(CString(IDS_LIST_OF_DEFINED_2),
			     List,
			     TRUE
	);

	return TRUE;
}

BOOL CTagNavTreeWnd::OnTagsFindLocked(void)
{
	CStringArray List;

	INDEX n = m_pTags->GetHead();

	while( !m_pTags->Failed(n) ) {

		CTag *pTag = m_pTags->GetItem(n);

		if( pTag->IsKindOf(AfxRuntimeClass(CTagNumeric)) ) {

			CTagNumeric *pNum = (CTagNumeric *) pTag;

			if( pNum->m_FormType && pNum->m_FormLock ) {

				CString Text;

				Text += pNum->GetHumanPath();

				Text += L"\n";

				Text += pNum->GetFixedPath();

				Text += L':';

				Text += L"FormLock";

				List.Append(Text);
			}
		}

		m_pTags->GetNext(n);
	}

	if( List.IsEmpty() ) {

		Information(CString(IDS_THERE_ARE_NO_TAGS));

		return FALSE;
	}

	m_System.SetFindList(CString(IDS_LIST_OF_TAGS_WITH),
			     List,
			     TRUE
	);

	return TRUE;
}

BOOL CTagNavTreeWnd::OnTagsFindUnused(void)
{
	CStringArray List;

	INDEX n = m_pTags->GetHead();

	while( !m_pTags->Failed(n) ) {

		CTag *pTag = m_pTags->GetItem(n);

		if( pTag ) {

			if( !pTag->IsKindOf(m_Folder) ) {

				UINT uTag = pTag->GetIndex();

				if( !m_pSystem->IsTagUsed(uTag) ) {

					List.Append(pTag->GetFindInfo());
				}
			}
		}

		m_pTags->GetNext(n);
	}

	if( List.IsEmpty() ) {

		Information(CString(IDS_THERE_ARE_NO_3));

		return FALSE;
	}

	m_System.SetFindList(CString(IDS_LIST_OF_UNUSED),
			     List,
			     TRUE
	);

	return TRUE;
}

BOOL CTagNavTreeWnd::OnTagsAddToWatch(void)
{
	UpdateWindow();

	HTREEITEM hItem = m_pTree->GetFirstSelect();

	while( hItem ) {

		CItem *pItem = GetItemPtr(hItem);

		if( pItem->IsKindOf(m_Folder) ) {

			CTagFolder * pFolder = (CTagFolder *) pItem;

			CString      Match   = pFolder->GetName();

			INDEX        Index   = m_pList->GetHead();

			while( !m_pList->Failed(Index) ) {

				CItem *pItem = m_pList->GetItem(Index);

				if( pItem->IsKindOf(m_Class) ) {

					CDataTag *pTag = (CDataTag *) pItem;

					if( pTag->GetName().StartsWith(Match) ) {

						AddToWatch(pTag);
					}
				}

				m_pList->GetNext(Index);
			}
		}

		if( pItem->IsKindOf(m_Class) ) {

			CDataTag *pTag = (CDataTag *) pItem;

			AddToWatch(pTag);
		}

		hItem = m_pTree->GetNextSelect(hItem);
	}

	m_pSystem->UpdateWatch(TRUE);

	return TRUE;
}

BOOL CTagNavTreeWnd::AddToWatch(CDataTag *pTag)
{
	if( pTag->GetDataType() != typeString ) {

		CString Name = pTag->GetName();

		if( pTag->m_Extent ) {

			for( UINT n = 0; n < pTag->m_Extent; n++ ) {

				CPrintf Code(L"%s[%u]", Name, n);

				m_pSystem->AddToWatch(Code);
			}

			return TRUE;
		}

		m_pSystem->AddToWatch(Name);
	}

	return TRUE;
}

UINT CTagNavTreeWnd::HasCustomImportIDS(void)
{
	UINT uMax = m_pSystem->m_pComms->GetMaxDeviceNumber();

	UINT uRtn = 0;

	for( UINT i = 1; i <= uMax; i++ ) {

		CCommsDevice * pDev = m_pSystem->m_pComms->FindDevice(i);

		if( pDev ) {

		}
	}

	return uRtn;
}

// Edit Commands

BOOL CTagNavTreeWnd::CanEditDuplicate(void)
{
	if( !IsItemLocked(m_hSelect) ) {

		if( !m_fMulti ) {

			if( m_pNamed ) {

				if( m_pNamed->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CTagNavTreeWnd::CanEditPasteSpecial(void)
{
	if( !IsReadOnly() ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			FORMATETC Fmt = { m_cfSpec, NULL, 1, -1, TYMED_HGLOBAL };

			if( pData->QueryGetData(&Fmt) == S_OK ) {

				pData->Release();

				return TRUE;
			}

			pData->Release();
		}
	}

	return FALSE;
}

void CTagNavTreeWnd::OnEditDuplicate(void)
{
	if( m_pNamed ) {

		if( m_pNamed->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

			CDataTag *pOld = (CDataTag *) m_pNamed;;

			CString   Item = pOld->GetFixedPath();

			CString   Menu = CString(IDS_DUPLICATE_TAG);

			MarkMulti(Item, Menu, TRUE);

			OnEditCopy();

			OnEditPaste();

			CDataTag *pTag = (CDataTag *) m_pNamed;

			if( pOld->m_pValue ) {

				BOOL fStep = TRUE;

				if( pOld->IsKindOf(AfxRuntimeClass(CTagFlag)) ) {

					CTagFlag *pFlag = (CTagFlag *) pTag;

					if( pFlag->m_FlagTreatAs >= 3 ) {

						if( pFlag->m_pValue->IsCommsRef() ) {

							if( ++pFlag->m_TakeBit >= pFlag->m_pValue->GetCommsBits() ) {

								pFlag->m_TakeBit = 0;
							}
							else
								fStep = FALSE;
						}
					}
				}

				if( fStep ) {

					UINT    uStep = pOld->GetCommsStep();

					HGLOBAL hPrev = pTag->TakeSnapshot();

					CString Value = pTag->m_pValue->GetSource(TRUE);

					if( m_pSystem->StepFragment(Value, uStep) ) {

						if( pTag->m_pValue->Compile(Value) ) {

							CCmdItem *pCmd = New CCmdItem(L"", pTag, hPrev);

							if( pCmd->IsNull() ) {

								delete pCmd;
							}
							else {
								m_System.SaveCmd(pCmd);

								m_System.ItemUpdated(pTag, updateProps);

								ItemUpdated(pTag, updateProps);

								pTag->UpdateExtent();
							}
						}
						else
							GlobalFree(hPrev);
					}
					else
						GlobalFree(hPrev);
				}
			}

			Item = m_pSelect->GetFixedPath();

			MarkMulti(Item, Menu, TRUE);
		}
	}
}

void CTagNavTreeWnd::OnEditPasteSpecial(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		FORMATETC Fmt = { m_cfSpec, NULL, 1, -1, TYMED_HGLOBAL };

		STGMEDIUM Med = { 0, NULL, NULL };

		if( pData->GetData(&Fmt, &Med) == S_OK ) {

			HGLOBAL hText = Med.hGlobal;

			CString Text  = PCTXT(GlobalLock(hText));

			GlobalUnlock(Med.hGlobal);

			ReleaseStgMedium(&Med);

			CStringArray Props;

			Text.Tokenize(Props, '\r');

			CTagPasteSpecialDialog Dlg;

			if( Dlg.Execute(ThisObject) ) {

				UINT uID = Dlg.GetMode() + IDM_TAGS_COPY_ADDRESS;

				OnCopyFrom(NULL, Props, CString(IDS_CLIPBOARD), uID);
			}
		}

		pData->Release();
	}
}

// Copy From

BOOL CTagNavTreeWnd::OnCopyFrom(CUIItem *pPick)
{
	CStringArray Props;

	CUIGetSet().GetProps(Props, pPick);

	CString Name = pPick->GetName();

	return OnCopyFrom(pPick, Props, Name, m_uPick);
}

BOOL CTagNavTreeWnd::OnCopyFrom(CUIItem *pPick, CStringArray &Props, CString Name, UINT uID)
{
	CString What  = L" ";

	UINT    uStep = 1;

	if( pPick ) {

		if( pPick->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

			CDataTag *pTag = (CDataTag *) pPick;

			uStep = pTag->GetCommsStep();
		}
	}
	else {
		uStep = watoi(Props[0]);

		Props.Remove(0);
	}

	if( uID == IDM_TAGS_COPY_SELECT ) {

		CUIGetSetDialog Dlg(Name, Props);

		Dlg.SetLastBuffer(m_Last);

		if( !Dlg.Execute(ThisObject) ) {

			return FALSE;
		}
	}
	else {
		switch( uID ) {

			case IDM_TAGS_COPY_ADDRESS:

				StripOther(&Props,
					   L"Core\nValue\n",
					   L"Core\nExtent\n",
					   L"Core\nManipulate\n",
					   L"Core\nTreatAs\n",
					   L"Core\nAccess\n",
					   L"Core\nStorage\n",
					   L"Core\nLength\n",
					   L"Core\nEncode\n",
					   NULL
				);

				What = CString(IDS_MAPPING_2);

				break;

			case IDM_TAGS_COPY_SCALE:

				StripOther(&Props,
					   L"Core\nScaleTo\n",
					   L"Core\nDataMin\n",
					   L"Core\nDataMax\n",
					   L"Core\nDispMin\n",
					   L"Core\nDispMax\n",
					   NULL
				);

				What = CString(IDS_SCALING);

				break;

			case IDM_TAGS_COPY_FORMAT:

				StripOther(&Props,
					   L"Core\nLimitType",
					   L"Core\nLimitMin",
					   L"Core\nLimitMax",
					   L"Core\nDeadband",
					   L"Core\nFormType",
					   L"Format\n",
					   L"Format List ",
					   NULL
				);

				What = CString(IDS_FORMAT_10);

				break;

			case IDM_TAGS_COPY_COLOR:

				StripOther(&Props,
					   L"Core\nColType",
					   L"Color\n",
					   L"Color List ",
					   NULL
				);

				What = CString(IDS_COLORING);

				break;

			case IDM_TAGS_COPY_ALARMS:

				StripOther(&Props,
					   L"Event1\n",
					   L"Event2\n",
					   NULL
				);

				What = CString(IDS_ALARMS_2);

				break;

			case IDM_TAGS_COPY_TRIGGERS:

				StripOther(&Props,
					   L"Trigger1\n",
					   L"Trigger2\n",
					   NULL
				);

				What = CString(IDS_TRIGGERS_2);

				break;

			case IDM_TAGS_COPY_SECURITY:

				StripOther(&Props,
					   L"Sec\n",
					   NULL
				);

				What = CString(IDS_SECURITY_2);

				break;

			default:
				return FALSE;
		}
	}

	CString Save = m_System.GetNavPos();

	CString Mode = pPick ? CString(IDS_COPY) : CString(IDS_PASTE_2);

	CString Verb = CPrintf(IDS_FMT, Mode, What);

	SaveMultiCmd(Save, Verb);

	HTREEITEM hItem = m_pTree->GetFirstSelect();

	BOOL      fFail = FALSE;

	BOOL	  fCopy = FALSE;

	while( hItem ) {

		CUIItem *pItem = (CUIItem *) GetItemPtr(hItem);

		if( pItem != pPick ) {

			CUIGetSet GetSet;

			HGLOBAL   hPrev = pItem->TakeSnapshot();

			if( uID == IDM_TAGS_COPY_ADDRESS ) {

				StepAddress(&Props, uStep);
			}

			if( GetSet.SetProps(pItem, Props) ) {

				CCmdItem *pCmd = New CCmdItem(L"", pItem, hPrev);

				if( pCmd->IsNull() ) {

					delete pCmd;
				}
				else {
					m_System.SaveCmd(pCmd);

					m_System.ItemUpdated(pItem, updateProps);

					ItemUpdated(pItem, updateProps);

					fCopy = TRUE;
				}

				if( GetSet.HadFailures() ) {

					fFail = TRUE;
				}
			}
			else
				GlobalFree(hPrev);
		}

		hItem = m_pTree->GetNextSelect(hItem);
	}

	if( fCopy ) {

		SaveMultiCmd(Save, Verb);
	}
	else
		m_System.KillLastCmd();

	if( fFail ) {

		Error(CString(IDS_AT_LEAST_ONE_2));

		return FALSE;
	}

	if( fCopy ) {

		Information(CString(IDS_SELECTED));

		m_System.ItemUpdated(m_pSelect, updateContents);

		return TRUE;
	}

	Information(CString(IDS_NO_CHANGES_WERE));

	return TRUE;
}

void CTagNavTreeWnd::StripOther(CStringArray *pProps, ...)
{
	va_list pArgs;

	va_start(pArgs, pProps);

	PCTXT *pList = (PCTXT *) alloca(sizeof(PCTXT) * 32);

	UINT  *pSize = (UINT  *) alloca(sizeof(UINT) * 32);

	UINT   a;

	for( a = 0; a < 32; a++ ) {

		if( !(pList[a] = va_arg(pArgs, PCTXT)) ) {

			break;
		}

		pSize[a] = wstrlen(pList[a]);
	}

	for( UINT n = 0; n < pProps->GetCount(); n++ ) {

		PCTXT s = pProps->GetAt(n);

		if( *s ) {

			UINT i;

			for( i = 0; i < a; i++ ) {

				if( !wstrncmp(s, pList[i], pSize[i]) ) {

					break;
				}
			}

			if( i == a ) {

				pProps->Remove(n);

				n--;
			}
		}
	}

	va_end(pArgs);
}

BOOL CTagNavTreeWnd::StepAddress(CStringArray *pProps, UINT uStep)
{
	PCTXT pAddr = L"Core\nValue\n";

	UINT  cAddr = wstrlen(pAddr);

	for( UINT n = 0; n < pProps->GetCount(); n++ ) {

		CString const &s = pProps->GetAt(n);

		if( !wstrncmp(s, pAddr, cAddr) ) {

			CStringArray a;

			s.Tokenize(a, '\n');

			CString p = a.GetAt(2);

			if( m_pSystem->m_pComms->StepFragment(p, uStep) ) {

				a.SetAt(2, p);

				CString r;

				for( UINT i = 0; i < a.GetCount(); i++ ) {

					if( i ) {

						r += '\n';
					}

					r += a[i];
				}

				pProps->SetAt(n, r);

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Tree Loading

void CTagNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TagsTreeIcon16"), afxColor(MAGENTA));
}

void CTagNavTreeWnd::LoadTree(void)
{
	CNavTreeWnd::LoadTree();

	// LATER -- We shouldn't need this, but GetItemImage relies on the
	// tree structure, so it can't working during load. We can fix it,
	// but GetItemImage will have to find its parent path by stripping
	// off the last section of the dotted path...

	LoadTreeImages(m_hRoot);
}

// Data Object Construction

BOOL CTagNavTreeWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	if( !m_fMulti ) {

		CDataObject *pMake = New CDataObject;

		if( m_pNamed ) {

			CTextStreamMemory Stream;

			if( Stream.OpenSave() ) {

				if( AddItemToStream(Stream, m_hSelect) ) {

					pMake->AddStream(m_cfData, Stream);
				}
			}
		}

		if( fRich ) {

			if( m_pNamed ) {

				pMake->AddText(m_pNamed->GetName());

				if( m_pNamed->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					CDataTag *pTag = (CDataTag *) m_pNamed;

					CStringArray Props;

					CUIGetSet().GetProps(Props, pTag);

					CPrintf Step(L"%u", pTag->GetCommsStep());

					Props.Insert(0, Step);

					CString Text;

					Text.Build(Props, '\r');

					pMake->AddText(m_cfSpec, Text);
				}
			}
		}

		if( pMake->IsEmpty() ) {

			delete pMake;

			pData = NULL;

			return FALSE;
		}

		pData = pMake;

		return TRUE;
	}

	return FALSE;
}

// Item Hooks

UINT CTagNavTreeWnd::GetRootImage(void)
{
	return IDI_TAGS;
}

UINT CTagNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

	BOOL      fLock = IsTagLocked(hItem);

	if( pItem->IsKindOf(m_Class) ) {

		CTag * pTag = (CTag *) pItem;

		UINT uImage = pTag->GetTreeImage();

		return fLock ? IDI_GRAY_FLAG + (uImage - IDI_RED_FLAG) % 4 : uImage;
	}

	if( pItem->IsKindOf(m_Folder) ) {

		return fLock ? IDI_GRAY_FOLDER : IDI_FOLDER;
	}

	return GetRootImage();
}

BOOL CTagNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"TagNavTreeCtxMenu";

		return TRUE;
	}

	Name = L"TagNavTreeMissCtxMenu";

	return TRUE;
}

void CTagNavTreeWnd::OnItemDeleted(CItem *pItem, BOOL fExec)
{
	if( pItem ) {

		if( ++m_uMany < 10 ) {

			if( pItem->IsKindOf(m_Class) ) {

				CTag * pTag = (CTag *) pItem;

				UINT   uPos = pTag->GetIndex();

				m_pSystem->TagCheck(uPos, FALSE);
			}
		}

		return;
	}

	if( m_uMany ) {

		if( m_uMany >= 10 ) {

			m_pSystem->Validate(TRUE);
		}

		m_uMany = 0;
	}
}

void CTagNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	if( pItem->IsKindOf(m_Class) ) {

		CTag * pTag = (CTag *) pItem;

		UINT   uPos = pTag->GetIndex();

		m_pSystem->TagCheck(uPos, TRUE);
	}

	CNavTreeWnd::OnItemRenamed(pItem);
}

// Lock Support

BOOL CTagNavTreeWnd::IsItemLocked(HTREEITEM hItem)
{
	if( !CNavTreeWnd::IsItemLocked(hItem) ) {

		return IsTagLocked(hItem);
	}

	return TRUE;
}

BOOL CTagNavTreeWnd::IsTagLocked(HTREEITEM hItem)
{
	while( hItem && hItem != m_hRoot ) {

		CMetaItem *pItem = GetItemPtr(hItem);

		if( pItem->IsKindOf(m_Folder) ) {

			CTagFolder *pFolder = (CTagFolder *) pItem;

			if( pFolder->m_Locked ) {

				return TRUE;
			}
		}

		hItem = m_pTree->GetParent(hItem);
	}

	return FALSE;
}

// Implementation

void CTagNavTreeWnd::LoadTreeImages(HTREEITEM hItem)
{
	while( hItem ) {

		if( IsParent(hItem) || IsFolder(hItem) ) {

			LoadTreeImages(m_pTree->GetChild(hItem));
		}

		CMetaItem *pMeta = GetItemPtr(hItem);

		CTreeViewItem Item(hItem, TVIF_IMAGE);

		m_pTree->GetItem(Item);

		Item.SetImages(GetItemImage(pMeta));

		m_pTree->SetItem(Item);

		hItem = m_pTree->GetNext(hItem);
	}
}

void CTagNavTreeWnd::SaveMultiCmd(CString Nav, CString Verb)
{
	CString Menu;

	if( m_fMulti ) {

		Menu = CFormat(CString(IDS_FMT_TAGS), Verb);
	}
	else {
		if( m_pNamed ) {

			CString Name = m_pNamed->GetName();

			Menu = CFormat(CString(IDS_FMT_2), Verb, Name);
		}
		else
			Menu = CFormat(CString(IDS_FMT_TAG), Verb);
	}

	CCmd *pCmd = New CCmdMulti(Nav, Menu, navOne);

	m_System.SaveCmd(pCmd);
}

// Sort Function

int CTagNavTreeWnd::SortByDevice(PCVOID p1, PCVOID p2)
{
	INDEX i1 = *((INDEX *) p1);

	INDEX i2 = *((INDEX *) p2);

	CMetaItem *m1 = (CMetaItem *) m_pSort->m_pList->GetItem(i1);

	CMetaItem *m2 = (CMetaItem *) m_pSort->m_pList->GetItem(i2);

	int f1 = m1->IsKindOf(m_pSort->m_Folder);

	int f2 = m2->IsKindOf(m_pSort->m_Folder);

	if( f1 == f2 ) {

		CString n1 = m1->GetName();

		CString n2 = m2->GetName();

		if( !f1 ) {

			CTag *t1 = (CTag *) m1;

			UINT  p1 = n1.FindRev('.');

			if( t1->m_pValue && t1->m_pValue->IsCommsRef() ) {

				n1 = n1.Left(p1 + 1);

				n1 = n1 + t1->m_pValue->GetSource(TRUE);
			}
		}

		if( !f2 ) {

			CTag *t2 = (CTag *) m2;

			UINT  p2 = n2.FindRev('.');

			if( t2->m_pValue && t2->m_pValue->IsCommsRef() ) {

				n2 = n2.Left(p2 + 1);

				n2 = n2 + t2->m_pValue->GetSource(TRUE);
			}
		}

		if( TRUE ) {

			UINT l1 = n1.GetLength();

			UINT l2 = n2.GetLength();

			UINT d1, d2;

			for( d1 = 0;; d1++ ) {

				if( !isdigit(n1[l1-1-d1]) ) {

					break;
				}
			}

			for( d2 = 0;; d2++ ) {

				if( !isdigit(n2[l2-1-d2]) ) {

					break;
				}
			}

			if( l1 - d1 == l2 - d2 ) {

				int q1 = l1 - d1;

				int q2 = l2 - d2;

				if( n1.Left(q1) == n2.Left(q2) ) {

					int v1 = watoi(PCTXT(n1) + q1);

					int v2 = watoi(PCTXT(n2) + q2);

					if( v1 < v2 ) return -1;

					if( v1 > v2 ) return +1;

					return 0;
				}
			}

			return lstrcmp(n1, n2);
		}
	}

	return (f1 < f2) ? +1 : -1;
}

// End of File
