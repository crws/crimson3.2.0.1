
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UILValue_HPP

#define INCLUDE_UILValue_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UIExpression.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Constant
//

class CUILValue : public CUIExpression
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUILValue(void);
	};

// End of File

#endif
