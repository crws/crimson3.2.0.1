
#include "Intern.hpp"

#include "CommsMapBlock.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsDevice.hpp"
#include "CommsMapBlockList.hpp"
#include "CommsMapBlockPage.hpp"
#include "CommsMapping.hpp"
#include "CommsMappingList.hpp"
#include "CommsMapReg.hpp"
#include "CommsMapRegList.hpp"
#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block
//

// Dynamic Class

AfxImplementDynamicClass(CCommsMapBlock, CCodedHost);

// Constructor

CCommsMapBlock::CCommsMapBlock(void)
{
	m_Addr.m_Ref = 0;

	m_Text       = "";

	m_Type       = 0;

	m_Size	     = 0;

	m_Write	     = 0;

	m_Scaled     = 2;

	m_Update     = 0;

	m_Period     = 500;

	m_Slave      = 0;

	m_pReq       = NULL;

	m_pAck	     = NULL;

	m_pRegs	     = New CCommsMapRegList;
	}

// UI Creation

BOOL CCommsMapBlock::OnLoadPages(CUIPageList *pList)
{
	CCommsMapBlockPage *pPage = New CCommsMapBlockPage(this);

	pList->Append(pPage);

	return FALSE;
	}

// UI Update

void CCommsMapBlock::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"Update" ) {

		DoEnables(pHost);
		}

	if( Tag == L"Addr" ) {

		if( m_Addr.m_Ref ) {

			m_Type = m_Addr.a.m_Type;
			}
		else {
			if( m_Text.IsEmpty() ) {

				m_Type = NOTHING;

				m_Size = 0;

				m_pRegs->SetItemCount(0);

				pHost->UpdateUI("Size");
				}
			}

		DoEnables(pHost);

		m_pRegs->SetCount(m_Size);

		m_pRegs->Validate(TRUE);

		pHost->SendUpdate(updateChildren);
		}

	if( Tag == L"Write" ) {

		m_pRegs->Validate(TRUE);

		DoEnables(pHost);

		pHost->SendUpdate(updateChildren);
		}

	if( Tag == L"Size" ) {

		if( !pHost->InReplay() ) {

			HGLOBAL hPrev = m_pRegs->TakeSnapshot();

			m_pRegs->SetCount(m_Size);

			HGLOBAL hData = m_pRegs->TakeSnapshot();

			CCmd *  pCmd  = New CCmdSubItem( L"Regs",
							 hPrev,
							 hData
							 );

			pHost->SaveExtraCmd(pCmd);

			pHost->SendUpdate(updateChildren);
			}
		}

	if( Tag == L"ButtonDelete" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_DEL_BLOCK);
		}

	if( Tag == L"Slave" ) {

		// TODO:  OnDemand mode not yet implemented in runtime.

		//	  Currently operates as "Always".
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CCommsMapBlock::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Req" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Ack" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
		}

	return FALSE;
	}

// Item Location

CCommsMapBlockList * CCommsMapBlock::GetParentList(void) const
{
	return (CCommsMapBlockList *) GetParent();
	}

CCommsDevice * CCommsMapBlock::GetParentDevice(void) const
{
	return (CCommsDevice *) GetParent(2);
	}

// Driver Access

ICommsDriver * CCommsMapBlock::GetDriver(void) const
{
	return GetParentDevice()->GetDriver();
	}

CItem * CCommsMapBlock::GetConfig(void) const
{
	return GetParentDevice()->GetConfig();
	}

CItem * CCommsMapBlock::GetDriverConfig(void) const
{
	CCommsPort *pPort = GetParentDevice()->GetParentPort();

	if( pPort ) {

		return pPort->m_pConfig;
		}

	return NULL;
	}

// Attributes

BOOL CCommsMapBlock::IsBroken(void) const
{
	return !m_Addr.m_Ref && m_Text.GetLength();
	}

UINT CCommsMapBlock::GetTreeImage(void) const
{
	return m_Write ? IDI_BLOCK_RED : IDI_BLOCK_GREEN;
	}

UINT CCommsMapBlock::GetDataType(void) const
{
	return m_Type;
	}

UINT CCommsMapBlock::GetBitCount(void) const
{
	return C3GetTypeLocalBits(m_Addr.a.m_Type);
	}

BOOL CCommsMapBlock::IsWriteBlock(void) const
{
	return m_Write;
	}

BOOL CCommsMapBlock::IsSlaveBlock(void) const
{
	return GetDriver()->GetType() == driverSlave;
	}

// Operations

BOOL CCommsMapBlock::Validate(BOOL fExpand)
{
	ICommsDriver *pDriver = GetDriver();

	CItem        *pConfig = GetConfig();

	CItem	     *pDrvCfg = GetDriverConfig();

	if( m_Text.GetLength() ) {

		CError Error(FALSE);

		if( fExpand ) {

			pDriver->ExpandAddress(m_Text, pConfig, m_Addr, pDrvCfg);
			}

		if( pDriver->ParseAddress(Error, m_Addr, pConfig, m_Text, pDrvCfg) ) {

			pDriver->ExpandAddress(m_Text, pConfig, m_Addr);

			pDriver->NotifyExtent(m_Addr, m_Size, pConfig);

			CTypeDef Type;

			C3GetTypeInfo(m_Addr.a.m_Type, Type);

			m_Type = Type.m_Type;
			}
		else {
			m_pDbase->SetRecomp();

			m_Addr.m_Ref = 0;
			}
		}
	else
		m_Addr.m_Ref = 0;

	return m_pRegs->Validate(fExpand);
	}

void CCommsMapBlock::ImportMappings(CTextStreamMemory &Stream)
{
	if( m_pRegs->GetItemCount() > 0 ) {

		m_pRegs->SetCount(0);
		}

	TCHAR sText[256];

	UINT uCount = 0;

	UINT uBits = 0;

	while( Stream.GetLine(sText, elements(sText)) ) {

		CString Line(sText);

		Line.TrimBoth();

		UINT uFind = Line.Find(L"=");

		if( uFind < NOTHING ) {

			uBits = wcstol(Line.Mid(uFind + 1), NULL, 10);

			uCount++;

			continue;
			}

		CCommsMapReg *pReg = New CCommsMapReg;

		pReg->m_Bits = uBits;

		m_pRegs->AppendItem(pReg);

		CCommsMapping *pMap = NULL;

		if( uBits == 0 ) {

			if( Line == "*" )
				continue;

			pMap = pReg->m_pMaps->GetItem(0U);

			if( pMap->SetMapping(Line) == 0 ) {

				Line.Insert(0, L"WAS ");

				pMap->SetMapping(Line);
				}
			}
		else {
			for( UINT n = 0; n < uBits; n++ ) {

				if( n == 0 ) {

					pMap = pReg->m_pMaps->GetItem(0U);
					}
				else {
					pMap = New CCommsMapping;

					pReg->m_pMaps->AppendItem(pMap);

					Stream.GetLine(sText, elements(sText));
					}

				Line = sText;

				Line.TrimBoth();

				if( Line == "*" )
					continue;

				if( pMap->SetMapping(Line) == 0 ) {

					Line.Insert(0, L"WAS ");

					pMap->SetMapping(Line);
					}
				}
			}
		}

	m_Size = uCount;

	Validate(TRUE);
	}

void CCommsMapBlock::ExportMappings(CTextStreamMemory &Stream)
{
	for( INDEX i = m_pRegs->GetHead(); !m_pRegs->Failed(i); m_pRegs->GetNext(i) ) {

		CCommsMapReg *pReg = m_pRegs->GetItem(i);

		CString Device	= GetParentDevice()->GetName();

		CString Addr	= pReg->GetAddrText();

		UINT uBits	= pReg->m_Bits;

		Stream.PutLine(CPrintf(	"%s.%s:BITS=%u\r\n",
					Device,
					Addr,
					uBits));

		CCommsMappingList *pMaps = pReg->m_pMaps;

		for( INDEX j = pMaps->GetHead(); !pMaps->Failed(j); pMaps->GetNext(j) ) {

			CCommsMapping *pMap = pMaps->GetItem(j);

			CString Source = pMap->GetMapSource(TRUE);

			if( !pMap->m_Ref )

				Stream.PutLine(L"*\r\n");
			else
				Stream.PutLine(CPrintf(L"%s\r\n", Source));
			}
		}
	}

// Persistance

void CCommsMapBlock::Init(void)
{
	CCodedHost::Init();

	CCommsMapBlockList *pList = GetParentList();

	for( UINT n = 1;; n++ ) {

		CPrintf Name(L"Block%u", n);

		if( pList->GetItem(Name) ) {

			continue;
			}

		m_Name = Name;

		break;
		}

	m_Slave	= IsSlaveBlock();
	}

void CCommsMapBlock::PostLoad()
{
	m_Slave = IsSlaveBlock();

	CCodedHost::PostLoad();
	}

// Download Support

BOOL CCommsMapBlock::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr.m_Ref));

	Init.AddByte(BYTE(C3GetTypeScale(m_Addr.a.m_Type)));

	Init.AddByte(BYTE(m_Write));

	Init.AddWord(WORD(m_Size));

	Init.AddByte(BYTE(m_Scaled));

	Init.AddByte(BYTE(m_Update));

	Init.AddWord(WORD(m_Period));

	Init.AddByte(BYTE(m_Slave));

	Init.AddItem(itemVirtual, m_pReq);

	Init.AddItem(itemVirtual, m_pAck);

	Init.AddItem(itemSimple,  m_pRegs);

	return TRUE;
	}

// Meta Data Creation

void CCommsMapBlock::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Addr);
	Meta_AddString (Text);
	Meta_AddInteger(Type);
	Meta_AddInteger(Size);
	Meta_AddInteger(Write);
	Meta_AddInteger(Scaled);
	Meta_AddInteger(Update);
	Meta_AddInteger(Period);
	Meta_AddInteger(Slave);
	Meta_AddVirtual(Req);
	Meta_AddVirtual(Ack);
	Meta_AddCollect(Regs);

	Meta_SetName((IDS_MAPPING_BLOCK));
	}

// Implementation

void CCommsMapBlock::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = (m_Addr.m_Ref ? TRUE : FALSE);

	BOOL fMaster = (GetDriver()->GetType() == driverMaster);

	BOOL fNamed  = (m_Addr.a.m_Table >= addrNamed);

	if( fNamed ) {

		m_Size = 1;

		OnUIChange(pHost, this, CString("Size"));

		pHost->UpdateUI("Size");
		}

	pHost->EnableUI("Size",   fEnable && !fNamed);
	pHost->EnableUI("Write",  fEnable);
	pHost->EnableUI("Scaled", fEnable);
	pHost->EnableUI("Update", fEnable && fMaster);
	pHost->EnableUI("Period", fEnable && fMaster && m_Update==2);
	pHost->EnableUI("Req",    fEnable && fMaster);
	pHost->EnableUI("Ack",    fEnable && fMaster);

	if( pHost->HasWindow() ) {

		CWnd &Wnd = pHost->GetWindow();

		Wnd.GetDlgItem(IDM_COMMS_MAP_IMPORT).EnableWindow(fEnable);

		Wnd.GetDlgItem(IDM_COMMS_MAP_EXPORT).EnableWindow(fEnable && m_Size);
		}
	}

// End of File
