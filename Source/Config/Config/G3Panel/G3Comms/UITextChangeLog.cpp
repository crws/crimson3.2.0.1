
#include "Intern.hpp"

#include "UITextChangeLog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Change Logging Mode
//

// Dynamic Class

AfxImplementDynamicClass(CUITextChangeLog, CUITextEnumPick);

// Constructor

CUITextChangeLog::CUITextChangeLog(void)
{
	m_Verb = CString(IDS_EDIT);
	}

// Overridables

void CUITextChangeLog::OnBind(void)
{
	if( m_UIData.m_Format[0] == 'D' ) {

		AddData(0, CString(IDS_DO_NOT_LOG));

		AddData(1, CString(IDS_LOG_CHANGES_BY_1));
		}
	else {
		AddData(3, CString(IDS_DEFAULT_FOR));

		AddData(0, CString(IDS_DO_NOT_LOG));

		AddData(1, CString(IDS_LOG_CHANGES_BY_1));

		AddData(2, CString(IDS_LOG_CHANGES_BY_2));
		}

	m_uWidth = 20;
	}

// End of File
