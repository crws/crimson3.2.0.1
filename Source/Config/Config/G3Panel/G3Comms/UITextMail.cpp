
#include "Intern.hpp"

#include "UITextMail.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MailAddress.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Mail Address
//

// Dynamic Class

AfxImplementDynamicClass(CUITextMail, CUITextElement);

// Constructor

CUITextMail::CUITextMail(void)
{
	m_uFlags = textExpand;

	m_Verb   = CString(IDS_EDIT);
	}

// Overridables

CString CUITextMail::OnGetAsText(void)
{
	CMailAddress *pAddress = (CMailAddress *) m_pData->GetObject(m_pItem);

	return pAddress->m_Name;
	}

UINT CUITextMail::OnSetAsText(CError &Error, CString Text)
{
	return saveSame;
	}

BOOL CUITextMail::OnExpand(CWnd &Wnd)
{
	CItem *pItem = m_pData->GetObject(m_pItem);

	CItemDialog Dlg(pItem, CString(IDS_EDIT_EMAIL));

	CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

	if( System.ExecSemiModal(Dlg) ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File
