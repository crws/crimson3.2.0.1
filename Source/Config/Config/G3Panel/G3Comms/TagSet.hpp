
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagSet_HPP

#define INCLUDE_TagSet_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CTagList;
class CTagSet;

//////////////////////////////////////////////////////////////////////////
//
// Tag Set
//

class DLLNOT CTagSet : public CItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagSet(void);

		// Attributes
		UINT    GetCount(void) const;
		CString Format(void) const;
		BOOL    IsBroken(void) const;
		BOOL    HasRef(CDataRef const &Ref) const;
		BOOL    HasCode(CString Code) const;
		BOOL    HasTag(UINT uTag) const;
		CString GetName(UINT n) const;
		CString GetSimulatedData(UINT n) const;
		CString GetFormattedData(UINT n, DWORD d) const;
		CString GetFindInfo(void) const;
		DWORD	Execute(UINT n) const;

		// Operations
		void MakeWatchList(void);
		void Empty(void);
		void Delete(UINT n);
		BOOL AddRef(CDataRef const &Ref);
		BOOL AddCode(CString Code);
		UINT Parse(CString Text);

		// Validation
		void Validate(BOOL fExpand);
		void TagCheck(UINT uTag);

		// Persistance
		void Init(void);
		void Save(CTreeFile &File);
		void Load(CTreeFile &File);
		void PostLoad(void);
		void PostPaste(void);
		void Kill(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Linked List
		CTagSet * m_pNext;
		CTagSet * m_pPrev;

		// Properties
		CArray <UINT>    m_Data;
		CArray <CString> m_Name;

	protected:
		// Data Members
		CCommsSystem * m_pSystem;
		CTagList     * m_pTags;
		BOOL	       m_fWatch;

		// Implementation
		void FindSystem(void);
		BOOL Resolve(CDataRef &Ref, CString Name) const;
		BOOL Expand(CString &Name, CDataRef const &Ref) const;
		void ListAppend(void);
		void ListRemove(void);
	};

// End of File

#endif
