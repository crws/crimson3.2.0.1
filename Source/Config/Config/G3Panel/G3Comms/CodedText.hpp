
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CodedText_HPP

#define INCLUDE_CodedText_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Coded Text
//

class DLLAPI CCodedText : public CCodedItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCodedText(void);

		// Attributes
		CString GetExportInfo(void) const;
		CString GetText(void) const;
		CString GetText(UINT uLang) const;
		CString GetText(PDWORD pParam) const;
		CString GetText(UINT uLang, PDWORD pParam) const;
		BOOL    HasText(UINT uLang) const;
		BOOL    IsStringConst(void) const;

		// Operations
		BOOL SetText(CString Text, BOOL fAll);
		BOOL SetText(CString Text, UINT uLang, BOOL fAll);

	protected:
		// Implementation
		BOOL IsStringConst(CString Text) const;
	};

// End of File

#endif
