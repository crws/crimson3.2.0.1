
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_MqttClientOptions_HPP

#define INCLUDE_MqttClientOptions_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client Options
//

class CMqttClientOptions : public CCodedHost
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CMqttClientOptions(void);

	// Operations
	void SetClientId(CString const &Id);
	void SetPeerName(CString const &Peer);
	void SetCredentials(CString const &User, CString const &Pass);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Data Members
	UINT	     m_Advanced;
	UINT	     m_Debug;
	BOOL	     m_Tls;
	UINT	     m_IdSrc;
	UINT	     m_CaSrc;
	UINT	     m_Check;
	UINT	     m_Face;
	UINT	     m_Port;
	CCodedItem * m_pPeerName;
	CCodedItem * m_pPeerName2;
	CCodedItem * m_pPeerName3;
	CCodedItem * m_pPeerName4;
	CCodedItem * m_pPeerName5;
	CCodedItem * m_pPeerName6;
	UINT	     m_PubQos;
	UINT	     m_SubQos;
	CCodedItem * m_pClientId;
	CCodedItem * m_pUserName;
	CCodedItem * m_pPassword;
	UINT         m_KeepAlive;
	UINT         m_ConnTimeout;
	UINT         m_SendTimeout;
	UINT         m_RecvTimeout;
	UINT	     m_BackOffTime;
	UINT	     m_BackOffMax;
	UINT	     m_NoCleanSess;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
};

// End of File

#endif
