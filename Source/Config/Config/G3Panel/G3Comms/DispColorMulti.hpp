
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColorMulti_HPP

#define INCLUDE_DispColorMulti_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispColor.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispColorMultiList;

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Color
//

class DLLNOT CDispColorMulti : public CDispColor
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispColorMulti(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void UpdateTypes(BOOL fComp);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT Type);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT                   m_Count;
		UINT		       m_Default;
		UINT                   m_Range;
		CDispColorMultiList * m_pList;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL DoSync(BOOL fUI);
		BOOL DoExport(void);
		BOOL DoImport(void);
	};

// End of File

#endif
