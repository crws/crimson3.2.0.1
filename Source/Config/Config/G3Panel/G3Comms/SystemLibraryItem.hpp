
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SystemLibraryItem_HPP

#define INCLUDE_SystemLibraryItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CNameServer;

//////////////////////////////////////////////////////////////////////////
//
// System Library Item
//

class CSystemLibraryItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSystemLibraryItem(void);

		// Operations
		void MakeCode(void);

		// Persistance
		void Init(void);

		// Data Members
		UINT    m_Ident;
		UINT    m_Image;
		BOOL    m_fAct;
		CString	m_Name;
		CString m_Code;
		CString m_Prot;

	protected:
		// Data
		CNameServer *m_pServer;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		BOOL    FindFunction(CString &Name, CTypeDef &Type, UINT &uCount);
		CString TextFromType(UINT uType);
		CString DataFromType(UINT uType);
	};

// End of File

#endif
