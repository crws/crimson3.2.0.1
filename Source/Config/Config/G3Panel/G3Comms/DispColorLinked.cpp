
#include "Intern.hpp"

#include "DispColorLinked.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "Tag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Color
//

// Dynamic Class

AfxImplementDynamicClass(CDispColorLinked, CDispColor);

// Constructor

CDispColorLinked::CDispColorLinked(void)
{
	m_uType = 4;

	m_pLink = NULL;
	}

// UI Update

void CDispColorLinked::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "Link" ) {

		if( !TestColor() ) {

			// REV3 -- This is not what we normally do with
			// circular references, but it is safer and it
			// gets around the problem detailed below.

			if( pHost->HasWindow() ) {

				CString Text = CString(IDS_THIS_WOULD_CREATE);

				CWnd::GetActiveWindow().Error(Text);
				}

			m_pLink->Kill();

			delete m_pLink;

			m_pLink = NULL;

			pHost->UpdateUI(Tag);
			}
		}

	CDispColor::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispColorLinked::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Link" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagTagRef | flagInherent;

		return TRUE;
		}

	return FALSE;
	}

// Color Access

DWORD CDispColorLinked::GetColorPair(DWORD Data, UINT Type)
{
	if( FindColor() ) {

		return m_pColor->GetColorPair(Data, Type);
		}

	return CDispColor::GetColorPair(Data, Type);
	}

// Download Support

BOOL CDispColorLinked::MakeInitData(CInitData &Init)
{
	CDispColor::MakeInitData(Init);

	FindColor();

	Init.AddWord(WORD(m_pColor ? m_pLink->GetTagIndex() : 0xFFFF));

	return TRUE;
	}

// Meta Data Creation

void CDispColorLinked::AddMetaData(void)
{
	CDispColor::AddMetaData();

	Meta_AddVirtual(Link);

	Meta_SetName((IDS_LINKED_COLOR));
	}

// Implementation

BOOL CDispColorLinked::TestColor(void)
{
	if( m_pLink ) {

		if( !m_pLink->IsBroken() ) {

			CItem *pParent = GetParent();

			if( pParent->IsKindOf(AfxRuntimeClass(CTag)) ) {

				CTag *pFind = (CTag *) pParent;

				// REV3 -- This prevents circular references from
				// doing anything bad, but it doesn't show them as
				// the tag doesn't update its circular flag.

				if( m_pLink->CheckCircular(pFind, TRUE) ) {

					return FALSE;
					}
				}
			}
		}

	return TRUE;
	}

BOOL CDispColorLinked::FindColor(void)
{
	if( m_pLink ) {

		if( TestColor() ) {

			CTag *pTag = NULL;

			if( m_pLink->GetTagItem(pTag) ) {

				if( pTag->m_pColor ) {

					m_pColor = pTag->m_pColor;

					return TRUE;
					}
				}
			}
		}

	m_pColor = NULL;

	return FALSE;
	}

// End of File
