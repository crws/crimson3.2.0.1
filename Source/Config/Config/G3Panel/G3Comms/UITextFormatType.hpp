
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextFormatType_HPP

#define INCLUDE_UITextFormatType_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Format Type
//

class CUITextFormatType : public CUITextEnumPick
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextFormatType(void);

	protected:
		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(UINT Data, CString Text);
		void AddData(void);
	};

// End of File

#endif
