
#include "Intern.hpp"

#include "SqlFilterPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SqlColumn.hpp"
#include "SqlColumnList.hpp"
#include "SqlFilter.hpp"
#include "SqlQuery.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Page
//

// Runtime Class

AfxImplementRuntimeClass(CSqlFilterPage, CUIPage);

// Constructor

CSqlFilterPage::CSqlFilterPage(CSqlFilter *pFilter)
{
	m_pFilter = pFilter;
	}

// Operations

BOOL CSqlFilterPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Properties", 1);

	pView->AddUI(pItem, L"root", L"Binding");

	CUIData Data;

	CString Form;

	CSqlQuery * pQuery = (CSqlQuery *) m_pFilter->GetParent(AfxRuntimeClass(CSqlQuery));

	UINT uCount = pQuery->GetColCount();

	for( UINT n = 0; n < uCount; n++ ) {

		Form += pQuery->GetColumnSqlName(n);
		
		if( n < (uCount - 1) ) {

			Form += L"|";
			}
		}

	// Columns

	Data.m_Tag       = L"Column";

	Data.m_Label     = L"Column";

	Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

	Data.m_ClassUI   = AfxNamedClass(L"CUIDropDown");

	Data.m_Format    = Form;

	Data.m_Tip       = L"";

	pView->AddUI(pItem, L"root", &Data);

	// Operators

	Form.Empty();

	Form = L"`=|<>|<|`<=|>|`>=|LIKE|IS NULL|IS NOT NULL";

	Data.m_Tag       = L"Operator";

	Data.m_Label     = L"Operator";

	Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

	Data.m_ClassUI   = AfxNamedClass(L"CUIDropDown");

	Data.m_Format    = Form;

	Data.m_Tip       = L"";

	pView->AddUI(pItem, L"root", &Data);

	pView->AddUI(pItem, L"root", L"Value");

	pView->EndGroup(FALSE);

	return FALSE;
	}

// End of File
