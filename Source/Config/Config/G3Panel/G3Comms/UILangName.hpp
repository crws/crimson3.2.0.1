
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UILangName_HPP

#define INCLUDE_UILangName_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangNameComboBox;

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Language Name
//

class CUILangName : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUILangName(void);

	protected:
		// Data Members
		CString	            m_Label;
		CLayItemText      * m_pTextLayout;
		CLayItem          * m_pDataLayout;
		CStatic	          * m_pTextCtrl;
		CLangNameComboBox * m_pDataCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Implementation
		CRect FindComboRect(void);
		BOOL  LoadList(void);
		void  LockList(void);
		void  UnlockList(void);
	};

// End of File

#endif
