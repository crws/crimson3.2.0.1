
#include "Intern.hpp"

#include "UITextIPAddr.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- IP Address
//

// Dynamic Class

AfxImplementDynamicClass(CUITextIPAddr, CUITextCoded);

// Constructor

CUITextIPAddr::CUITextIPAddr(void)
{
	}

// Overridables

UINT CUITextIPAddr::OnSetAsText(CError &Error, CString Text)
{
	if( Text[0] == '!' ) {

		Error.Set(CString(IDS_INVALID_IP));

		return saveError;
		}

	if( Text.StartsWith(L"ResolveDNS(") ) {

		CString Work = Text;

		Work  = Work.Mid(wstrlen(L"ResolveDNS("));

		Work  = Work.Left(Work.GetLength() - 1);

		if( Work[0] == '"' ) {

			if( Work[Work.GetLength() - 1] == '"' ) {

				Work = Work.Mid(1, Work.GetLength() - 2);
				}
			}

		if( Work.IsEmpty() ) {

			return CUITextCoded::OnSetAsText(Error, GetAsText());
			}
		}

	if( Text.IsEmpty() ) {

		return CUITextCoded::OnSetAsText(Error, GetAsText());
		}

	return CUITextCoded::OnSetAsText(Error, Text);
	}

// End of File
