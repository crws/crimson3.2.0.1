
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_IntlEditItem_HPP

#define INCLUDE_IntlEditItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIntlStringList;

//////////////////////////////////////////////////////////////////////////
//
// International String Editing Item
//

class CIntlEditItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIntlEditItem(void);

		// Persistance
		void Init(void);

		// UI Overridables
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		BOOL OnLoadPages(CUIPageList *pList);

		// Operations
		void    LoadStrings(CString Strings);
		CString ReadStrings(void);

		// Item Properties
		CIntlStringList	* m_pList;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		CString GetText(UINT n);
		void    SetText(UINT n, CString Text);
	};

// End of File

#endif
