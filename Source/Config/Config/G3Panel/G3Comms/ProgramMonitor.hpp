
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramMonitor_HPP

#define INCLUDE_ProgramMonitor_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramManager;

//////////////////////////////////////////////////////////////////////////
//
// External Program Change Monitor
//

class CProgramMonitor : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CProgramMonitor(CFilename const &Path);

		// Destructor
		~CProgramMonitor(void);

		// Overridables
		UINT OnExec(void);

		// Operations
		void SetWindow(CWnd *pWnd);

		// Attributes
		BOOL GetChangeList(CStringArray &List);

	protected:
		// File Data Map
		typedef CMap <CString, DWORD> CFileMap;

		// Data Members
		CWnd *       m_pWnd;
		CFilename    m_Path;
		CWaitable    m_FileEvent;
		CFileMap     m_Map;
		CMutex       m_Mutex;
		CStringArray m_Changes;

		// Implmentation
		void MakeList(void);
		BOOL TestList(void);
	};

// End of File

#endif
