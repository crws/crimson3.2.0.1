
#include "Intern.hpp"

#include "ColorComboBox.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ColorManager.hpp"
#include "ColorMenuHost.hpp"
#include "CommsSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Color Combo Box
//

// Runtime Class

AfxImplementRuntimeClass(CColorComboBox, CDropComboBox);

// Constructor

CColorComboBox::CColorComboBox(CUIElement *pUI) : CDropComboBox(pUI)
{
	m_pUI   = pUI;

	m_Color = 0xFFFF;
	}

// Destructor

CColorComboBox::~CColorComboBox(void)
{
	}

// Attributes

COLOR CColorComboBox::GetColor(void) const
{
	return m_Color;
	}

// Operations

void CColorComboBox::LoadList(void)
{
	m_pSystem = (CCommsSystem *) m_pUI->GetItem()->GetDatabase()->GetSystemItem();

	m_pColor  = m_pSystem->m_pColor;
	}

void CColorComboBox::ShowColors(void)
{
	SetFocus();

	CColorMenuHost *pHost = New CColorMenuHost(m_pColor);

	pHost->Create( WS_CHILD,
		       CRect(0, 0, 1, 1),
		       ThisObject,
		       1111,
		       NULL
		       );

	CPoint Pos   = GetWindowRect().GetBottomLeft();

	COLOR  Color = m_Color;

	if( pHost->Select(Pos, Color) ) {

		SetColor(Color);

		GetParent().SendMessage( WM_COMMAND,
					 WPARAM(MAKELONG(GetID(), CBN_SELCHANGE)),
					 LPARAM(m_hWnd)
					 );

		Invalidate(FALSE);
		}

	pHost->DestroyWindow(TRUE);
	}

BOOL CColorComboBox::SetColor(COLOR Color)
{
	if( m_Color != Color ) {

		m_Color = Color;

		m_pColor->FindColor(m_Color, m_uGroup, m_uIndex);

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

// Message Map

AfxMessageMap(CColorComboBox, CDropComboBox)
{
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)
	AfxDispatchMessage(WM_CHAR)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)
	AfxDispatchMessage(WM_SYSKEYDOWN)
	AfxDispatchMessage(WM_SYSKEYUP)

	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)

	AfxMessageEnd(CColorComboBox)
	};

// Message Handlers

void CColorComboBox::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	Item.itemHeight = DC.GetTextExtent(L"ABC").cy + 2;

	DC.Deselect();
	}

void CColorComboBox::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
	CDC   DC(Item.hDC);

	CRect Rect = Item.rcItem;

	CRect Fill = Rect;

	Fill.right = Rect.left += 16;

	if( Item.itemState & ODS_DISABLED ) {

		DC.SetTextColor(afxColor(3dShadow));

		DC.SetBkColor  (afxColor(3dFace));

		DC.FillRect(--Fill, afxBrush(3dShadow));
		}
	else {
		if( Item.itemState & ODS_SELECTED ) {

			DC.SetTextColor(afxColor(SelectText));

			DC.SetBkColor  (afxColor(SelectBack));
			}
		else {
			DC.SetTextColor(afxColor(WindowText));

			DC.SetBkColor  (afxColor(WindowBack));
			}

		CColor Color = C3GetWinColor(m_Color);

		if( m_Color == 0x7FFF ) {

			DC.FrameRect(--Fill, afxBrush(BLACK));
			}

		DC.FillRect(--Fill, Color);
		}

	if( TRUE ) {

		CString Text = m_pColor->GetName(m_uGroup, m_uIndex);

		DC.ExtTextOut( Rect.GetTopLeft() + CPoint(3, 1),
			       ETO_OPAQUE | ETO_CLIPPED,
			       Rect,
			       Text
			       );
		}

	if( Item.itemState & ODS_FOCUS ) {

		DC.DrawFocusRect(Rect);
		}
	}

void CColorComboBox::OnChar(UINT uCode, DWORD dwFlags)
{
	if( uCode == VK_SPACE ) {

		ShowColors();
		}
	}

void CColorComboBox::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	if( uCode == VK_DOWN ) {

		ShowColors();
		}
	}

void CColorComboBox::OnKeyUp(UINT uCode, DWORD dwFlags)
{
	}

void CColorComboBox::OnSysKeyDown(UINT uCode, DWORD dwFlags)
{
	if( uCode == VK_DOWN ) {

		ShowColors();
		}
	}

void CColorComboBox::OnSysKeyUp(UINT uCode, DWORD dwFlags)
{
	}

void CColorComboBox::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	ShowColors();
	}

void CColorComboBox::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	OnLButtonDown(uFlags, Pos);
	}

// End of File
