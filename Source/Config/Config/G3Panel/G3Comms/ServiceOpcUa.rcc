
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#include "ServiceOpcUa.loc"

//////////////////////////////////////////////////////////////////////////
//
// Item Schemas
//

CServiceOpcUA_Schema RCDATA
BEGIN
	L"Enable,Enable Server,,ExprEnum/ExprDropEdit,No|Yes,"
	L"Enable this setting to use the OPC UA server."
	L"\0"

	L"Endpoint,Endpoint Host,,ExprString/ExprEditBox,50|Default,"
	L"Specify the host portion of the endpoint for the server. The host name will be prefixed "
	L"with opc.tcp:// and followed by the port number to create the endpoint name. If you do "
	L"not specify a name, the server will use the device's specified or default ZeroConfig name."
	L"\0"

	L"Server,Server Name,,ExprString/ExprEditBox,50|Default,"
	L"Specify the name of the server. If you do not specify a "
	L"name, the host portion of the endpoint name will be used."
	L"\0"

	L"Port,Listen on Port,,Integer/EditBox,0|0||0|65535,"
	L"Specify the TCP/IP port on which the server should listen."
	L"\0"

	L"Layout,Model Layout,,Enum/DropDown,Default|Tags in Objects Folder,"
	L"Specify how the OPC UA data model should be structured. By default, Crimson exposes data arrays and "
	L"tags under a version-specific folder to allow for easier upgrades. This can be overriden to omit the "
	L"data arrays and place the tags straight under the Objects folder. This shortens path names at the "
	L"expense of vulnerability to upgrade incompatability."
	L"\0"

	L"Tree,Tag Structure,,Enum/DropDown,Simple Tag List|Folders with Short Names|Folders with Full Names,"
	L"Specify how Crimson's tags should be presented in the server's OPC UA data model. The Simple List option "
	L"will list all tags in a single folder, while the Folder option will arrange the tags according to the "
	L"folder structure created within the Tags section of the database. The Short Name option will name each "
	L"leaf node with the final portion of the tag's name while the Long Name option will use the full dotted "
	L"name of the tag."
	L"\0"

	L"Array,Array Layout,,Enum/DropDown,Create Elements in List|Create Elements in Folder,"
	L"Specify whether array elements should be created in the tag "
	L"list, or whether a specific folder should be created for them."
	L"\0"

	L"Props,Show Properties,,Enum/DropDown,No|Yes without StateText|Yes with StateText,"
	L"Indicate whether each tag's properties should be included in the server's OPC UA data model. The properties "
	L"expose the various items of tag information that can be accessed using the dot operator from within Crimson. "
	L"If you enable StateText, each tag that has a multi-state format option will include an array that can be used "
	L"to translate the tag's value into a string."
	L"\0"

	L"PubSub,Enable Subscriptions,,Enum/DropDown,No|Yes,"
	L"Indicate whether the server should send update via subscriptions."
	L"\0"

	L"HistEnable,Record History,,Enum/DropDown,Disable|Enable,"
	L"Indicate whether the OPC UA server should record historic values to the unit's memory card."
	L"\0"

	L"HistSample,Sample Rate,,Integer/EditBox,0|0|secs|1|3600,"
	L"Indicate how often the OPC UA server should record historic values."
	L"\0"

	L"HistQuota,Disk Usage Limit,,Integer/EditBox,0|0|%|1|90,"
	L"Indicate how much of the memory card will be used for historic values."
	L"\0"

	L"Anon,Anonymous Access,,Enum/DropDown,Disabled|Enabled,",
	L"Indicate whether the server should allow anonymous access."
	L"\0"

	L"User,User Name,,ExprString/ExprEditBox,40|None,"
	L"\0"

	L"Pass,Password,,ExprString/ExprEditBox,*40|None,"
	L"\0"

	L"Write,Tag Writes,,Enum/DropDown,Disabled|Authenticated=2|Anonymous=1,"
	L"Indicate whether the server should allow writes."
	L"\0"

	L"Set,Data Tags,,TagSet/TagSet,72|Tags,"
	L"Select the tags to be exposed by the UPC-UA server to all clients."
	L"\0"

	L"Set2,Data Tags,,TagSet/TagSet,72|Tags,"
	L"Select the tags to be exposed by the UPC-UA server to authenticated clients."
	L"\0"

	L"Debug,Debug Output,,Enum/DropDown,Disabled|Enabled|Detailed,"
	L"Indicate whether the OPC UA server should output information to the debugging console."
	L"\0"

	L"HistTime,Collection Time,,Integer/EditBox,0|0|%|15|75,"
	L"Indicate how much of the timeout hint from the client should be used for collecting data "
	L"from the memory card, and therefore how much time should be left for transmitting that "
	L"data. The default value is suitable for most applications, but can be reduced if issues "
	L"are encountered on slow links."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Page Schemas
//

CServiceOpcUa_Page1 RCDATA
BEGIN
	L"G:1,root,Control,Enable\0"
	L"G:1,root,Naming,Server,Endpoint\0"
	L"G:1,root,Access,Anon,User,Pass,Write\0"
	L"G:1,root,Browsing,Layout,Tree,Array,Props\0"
	L"G:1,root,Historic Data,HistEnable,HistSample,HistQuota,HistTime\0"
	L"\0"
END

CServiceOpcUa_Page2 RCDATA
BEGIN
	L"G:1,root,Control,Enable\0"
	L"G:1,root,Naming,Server,Endpoint\0"
	L"G:1,root,Access,Anon,User,Pass,Write\0"
	L"G:1,root,Browsing,Layout,Tree,Array,Props\0"
	L"\0"
END

CServiceOpcUa_Page3 RCDATA
BEGIN
	L"G:1,root,Connection,Port\0"
	L"G:1,root,Diagnostics,Debug\0"
	L"\0"
END

CServiceOpcUa_Page4 RCDATA
BEGIN
	L"G:1,root,Contents,Set\0"
	L"\0"
END

CServiceOpcUa_Page5 RCDATA
BEGIN
	L"G:1,root,Contents,Set2\0"
	L"\0"
END

// End of File
