
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_WebTranslationThread_HPP

#define INCLUDE_WebTranslationThread_HPP

///////////////////////////////////////////////////////////////////////
//
// Libraries
//

#include <wininet.h>

#pragma  comment(lib, "wininet")

//////////////////////////////////////////////////////////////////////////
//
// Update Commands
//

#define IDINIT		500
#define	IDDONE		501
#define	IDFAIL		502
#define	IDQUOTA		503

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Lexicon.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangManager;

//////////////////////////////////////////////////////////////////////////
//
// Web Translation Thread
//

class CWebTranslationThread : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CWebTranslationThread(void);

		// Destructor
		~CWebTranslationThread(void);

		// Management
		void Bind(CWnd *pWnd, CLangManager *pLang);
		void SetTo(CUIntArray const &To);
		BOOL Terminate(DWORD Timeout);

		// Attributes
		CString GetInput(void) const;
		CString GetOutput(UINT uSlot) const;

		// Operations
		void Connect(void);
		void Translate(CString const &In);

	protected:
		// Static Data
		static UINT m_uStartMin;
		static UINT m_uStartSec;
		static UINT m_uCountMin;
		static UINT m_uCountSec;
		static CString m_ClientId;
		static CString m_ClientSecret;

		struct CToken {

			CString	m_Val;
			UINT	m_uExpireTime;
			};

		// Data Members
		CWnd		 * m_pWnd;
		CLangManager	 * m_pLang;
		CString		   m_From;
		CStringArray       m_To;
		CByteArray         m_NoUpper;
		CByteArray         m_Jamo;
		CEvent		   m_DoneEvent;
		UINT		   m_uCode;
		HINTERNET	   m_hNet;
		CString		   m_In;
		CStringArray	   m_Out;
		CString		   m_Sub;
		CStringNormData	   m_Data;
		BYTE		   m_bData[4096];
		BOOL		   m_fLex;
		CLexicon           m_Lex;
		UINT		   m_uTo;
		CToken		   m_MSToken;
		BOOL		   m_fQuota;

		// Overridables
		BOOL OnInit(void);
		UINT OnExec(void);
		void OnTerm(void);

		// Text Encoding
		CString DecodeString(PBYTE pData);
		CString EncodeString(CString Text);

		// Implementation
		BOOL DoConnect(void);
		BOOL DoTranslate(void);
		void Normalize(UINT Norm);
		BOOL DoLexicon(void);
		BOOL DoGoogle(void);
		BOOL DoMicrosoft(void);
		BOOL GetAccessToken(void);
		void Throttle(UINT uSec, UINT uMin);
		void SubstTo(CString &To);
	};

// End of File

#endif
