
#include "Intern.hpp"

#include "DispFormatTimeDate.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "CommsSystem.hpp"
#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Time and Date Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatTimeDate, CDispFormat);

// Time Fields

enum
{
	fieldSec   = 0xFF00,
	fieldMin   = 0xFF01,
	fieldHE1   = 0xFF02,
	fieldHE2   = 0xFF03,
	fieldHE3   = 0xFF04,
	fieldHE4   = 0xFF05,
	fieldH12   = 0xFF06,
	fieldH24   = 0xFF07,
	fieldAMPM  = 0xFF08,
	fieldDate  = 0xFF09,
	fieldMonth = 0xFF0A,
	fieldYear  = 0xFF0B,
	};

// Constructor

CDispFormatTimeDate::CDispFormatTimeDate(void)
{
	m_uType    = 3;

	m_Mode     = 0;

	m_Secs     = 0;

	m_TimeForm = 0;

	m_TimeSep  = 0;

	m_pAM      = NULL;

	m_pPM      = NULL;

	m_DateForm = 0;

	m_DateSep  = 0;

	m_Year     = 0;

	m_Month    = 0;
	}

// UI Update

void CDispFormatTimeDate::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == L"TimeForm" || Tag == L"Mode" ) {

		DoEnables(pHost);
		}

	CDispFormat::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispFormatTimeDate::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "AM" || Tag == "PM" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Formatting

CString CDispFormatTimeDate::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeInteger ) {

		BuildFormat();

		TCHAR  t[128] = { 0 };

		TCHAR  c[2]   = { 0, 0 };

		for( UINT n = 0; n < m_uCount; n++ ) {

			UINT f = LOWORD(m_Format[n]);

			UINT l = HIWORD(m_Format[n]);

			UINT h = 0;

			if( HIBYTE(f) ) {

				switch( f ) {

					case fieldSec:
						swprintf(t+wstrlen(t), L"%2.2u", ::GetSec(Data));
						break;

					case fieldMin:
						swprintf(t+wstrlen(t), L"%2.2u", ::GetMin(Data));
						break;

					case fieldHE1:
						swprintf(t+wstrlen(t), L"%1.1u", ::GetHours(Data) % 10);
						break;

					case fieldHE2:
						swprintf(t+wstrlen(t), L"%2.2u", ::GetHours(Data) % 100);
						break;

					case fieldHE3:
						swprintf(t+wstrlen(t), L"%3.3u", ::GetHours(Data) % 1000);
						break;

					case fieldHE4:
						swprintf(t+wstrlen(t), L"%4.4u", ::GetHours(Data) % 10000);
						break;

					case fieldH24:
						swprintf(t+wstrlen(t), L"%2.2u", ::GetHour(Data));
						break;

					case fieldH12:
						swprintf(t+wstrlen(t), L"%2.2u", (h = ::GetHour(Data)%12) ? h : 12);
						break;

					case fieldAMPM:
						swprintf(t+wstrlen(t), L"%-*s", l, PCTXT(GetAMPM(::GetHour(Data))));
						break;

					case fieldDate:
						swprintf(t+wstrlen(t), L"%2.2u", ::GetDate(Data));
						break;
					}

				if( f == fieldMonth ) {

					if( l == 3 ) {

						CString m = GetMonthName(::GetMonth(Data));

						wstrcat(t, m);
						}
					else
						swprintf(t+wstrlen(t), L"%2.2u", ::GetMonth(Data));
					}

				if( f == fieldYear ) {

					if( l == 2 )
						swprintf(t+wstrlen(t), L"%2.2u", ::GetYear(Data) % 100);
					else
						swprintf(t+wstrlen(t), L"%4.4u", ::GetYear(Data));
					}
				}
			else {
				c[0] = TCHAR(f);

				wstrcat(t, c);
				}
			}

		if( Flags & fmtPad ) {

			MakeDigitsFixed(t);
			}

		return t;
		}

	if( Type == typeReal ) {

		return Format(DWORD(I2R(Data)), typeInteger, Flags);
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Limit Access

DWORD CDispFormatTimeDate::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return DWORD(INT_MIN);
		}

	if( Type == typeReal ) {

		return R2I(-FLT_MAX);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatTimeDate::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return DWORD(INT_MAX);
		}

	if( Type == typeReal ) {

		return R2I(+FLT_MAX);
		}

	return CDispFormat::GetMax(Type);
	}

// Download Support

BOOL CDispFormatTimeDate::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));
	Init.AddByte(BYTE(m_Secs));
	Init.AddByte(BYTE(m_TimeForm));
	Init.AddByte(BYTE(m_TimeSep));
	Init.AddByte(BYTE(m_DateForm));
	Init.AddByte(BYTE(m_DateSep));
	Init.AddByte(BYTE(m_Year));
	Init.AddByte(BYTE(m_Month));

	Init.AddItem(itemVirtual, m_pAM);

	Init.AddItem(itemVirtual, m_pPM);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatTimeDate::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddInteger(Secs);
	Meta_AddVirtual(AM);
	Meta_AddVirtual(PM);
	Meta_AddInteger(TimeForm);
	Meta_AddInteger(TimeSep);
	Meta_AddInteger(DateForm);
	Meta_AddInteger(DateSep);
	Meta_AddInteger(Year);
	Meta_AddInteger(Month);

	Meta_SetName((IDS_TIMEDATE_FORMAT));
	}

// Implementation

void CDispFormatTimeDate::DoEnables(IUIHost *pHost)
{
	BOOL fNorm = (m_Mode <= 3);
	BOOL fTime = (m_Mode != 1);
	BOOL fDate = (m_Mode != 0);

	pHost->EnableUI(L"TimeForm", fTime && fNorm);
	pHost->EnableUI(L"TimeSep",  fTime && fNorm);

	pHost->EnableUI("Secs",     fTime);

	pHost->EnableUI(L"AM", fTime && fNorm && m_TimeForm == 2);
	pHost->EnableUI(L"PM", fTime && fNorm && m_TimeForm == 2);

	pHost->EnableUI(L"DateForm", fNorm && fDate);
	pHost->EnableUI(L"DateSep",  fNorm && fDate);
	pHost->EnableUI(L"Year",     fNorm && fDate);
	pHost->EnableUI(L"Month",    fNorm && fDate);
	}

void CDispFormatTimeDate::BuildFormat(void)
{

	// LATER -- Call on load/edit/ctor?

	m_uCount  = 0;

	m_uFields = 0;

	m_uTotal  = 0;

	switch( m_Mode ) {

		case 0:
		case 4:
		case 5:
		case 6:
		case 7:
			BuildTime();

			break;

		case 1:
			BuildDate();

			break;

		case 2:
			BuildTime();

			AddForm(' ', 1);

			BuildDate();

			break;

		case 3:
			BuildDate();

			AddForm(' ', 1);

			BuildTime();

			break;
		}
	}

void CDispFormatTimeDate::BuildTime(void)
{
	char cSep  = char(m_TimeSep ? m_TimeSep : ':');

	UINT uForm = m_TimeForm;

	if( m_Mode >= 4 ) {

		UINT uCode = (m_Mode - 4) + fieldHE1;

		UINT uLen  = (m_Mode - 3);

		AddForm(uCode, uLen);

		AddForm(cSep,     1);

		AddForm(fieldMin, 2);
		}
	else {
		if( uForm == 0) {

			uForm = GetLocaleTime();
			}

		if( uForm == 2 ) {

			AddForm(fieldH12, 2);

			AddForm(cSep,     1);

			AddForm(fieldMin, 2);
			}
		else {
			AddForm(fieldH24, 2);

			AddForm(cSep,     1);

			AddForm(fieldMin, 2);
			}
		}

	if( m_Secs ) {

		AddForm(cSep,     1);

		AddForm(fieldSec, 2);
		}

	if( m_Mode <= 3 && uForm == 2 ) {

		UINT m1 = wstrlen(GetAMPM(0 ));

		UINT m2 = wstrlen(GetAMPM(12));

		AddForm(fieldAMPM, max(m1, m2));
		}
	}

void CDispFormatTimeDate::BuildDate(void)
{
	UINT uForm = m_DateForm;

	if( !uForm ) {

		uForm = GetLocaleDate();
		}

	switch( uForm ) {

		case 1:
			if( m_Year == 2 ) {

				AddMonth();

				AddDateSep();

				AddDate();
				}
			else {
				AddMonth();

				AddDateSep();

				AddDate();

				AddDateSep();

				AddYear();
				}
			break;

		case 2:
			if( m_Year == 2 ) {

				AddDate();

				AddDateSep();

				AddMonth();
				}
			else {
				AddDate();

				AddDateSep();

				AddMonth();

				AddDateSep();

				AddYear();
				}
			break;

		case 3:
			if( m_Year == 2 ) {

				AddDate();

				AddDateSep();

				AddMonth();
				}
			else {
				AddYear();

				AddDateSep();

				AddMonth();

				AddDateSep();

				AddDate();
				}
			break;
		}
	}

void CDispFormatTimeDate::AddDate(void)
{
	AddForm(fieldDate, 2);
	}

void CDispFormatTimeDate::AddMonth(void)
{
	AddForm(fieldMonth, m_Month ? 3 : 2);
	}

void CDispFormatTimeDate::AddYear(void)
{
	AddForm(fieldYear, m_Year ? 4 : 2);
	}

void CDispFormatTimeDate::AddDateSep(void)
{
	if( !m_DateSep ) {

		CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

		CLangManager *pLang   = pSystem->m_pLang;

		AddForm(pLang->GetDateSepChar(), 1);

		return;
		}

	AddForm(m_DateSep, 1);
	}

void CDispFormatTimeDate::AddForm(UINT uCode, UINT uLen)
{
	m_Format[m_uCount++] = MAKELONG(uCode, uLen);

	if( HIBYTE(uCode) == 0xFF ) {

		m_uFields++;
		}

	m_uTotal += uLen;
	}

// Localization

UINT CDispFormatTimeDate::GetLocaleTime(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	return pLang->GetTimeFormat();
	}

UINT CDispFormatTimeDate::GetLocaleDate(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	return pLang->GetDateFormat();
	}

CString CDispFormatTimeDate::GetMonthName(UINT uMonth)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	return pLang->GetMonthName(uMonth);
	}

CString CDispFormatTimeDate::GetAMPM(UINT uHour)
{
	if( uHour >= 12 ) {

		if( m_pPM ) {

			return m_pPM->GetText();
			}

		return L" PM";
		}

	if( m_pAM ) {

		return m_pAM->GetText();
		}

	return L" AM";
	}

// End of File
