
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SecurityManagerNavTreeWnd_HPP

#define INCLUDE_SecurityManagerNavTreeWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Security Manager Navigation Window
//

class CSecurityManagerNavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSecurityManagerNavTreeWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Command Handlers
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		void OnItemNew(void);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
	};

// End of File

#endif
