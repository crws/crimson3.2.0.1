
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_OptionCardList_HPP

#define INCLUDE_OptionCardList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPortList;
class COptionCardItem;
union CUsbTreePath;

//////////////////////////////////////////////////////////////////////////
//
// Option Card List
//

class DLLAPI COptionCardList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COptionCardList(void);

		// Item Access
		COptionCardItem * GetItem(INDEX Index) const;
		COptionCardItem * GetItem(UINT  uSlot) const;

		// Operations
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		UINT GetTotalPower(void);
		BOOL HasType(UINT uType) const;

		// Ports List Access
		BOOL GetPortList(CCommsPortList * &pList, UINT uIndex) const;

		// Slot
		COptionCardItem * Find(CUsbTreePath const &Slot) const;
		COptionCardItem * Find(UINT Slot) const;

		// Firmware
		void GetFirmwareList(CArray<UINT> &List) const;
	};

// End of File

#endif
