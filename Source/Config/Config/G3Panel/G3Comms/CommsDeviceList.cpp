
#include "Intern.hpp"

#include "CommsDeviceList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"

#include "CommsSysBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Device List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsDeviceList, CItemIndexList);

// Constructor

CCommsDeviceList::CCommsDeviceList(void)
{
	m_Class = AfxRuntimeClass(CCommsDevice);
	}

// Item Access

CCommsDevice * CCommsDeviceList::GetItem(INDEX Index) const
{
	return (CCommsDevice *) CItemIndexList::GetItem(Index);
	}

CCommsDevice * CCommsDeviceList::GetItem(UINT uPos) const
{
	return (CCommsDevice *) CItemIndexList::GetItem(uPos);
	}

CCommsDevice * CCommsDeviceList::GetItem(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		if( pDevice->m_Name == Name ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

// Item Lookup

CCommsDevice * CCommsDeviceList::FindDevice(UINT uDevice) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		if( pDevice->m_Number == uDevice ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

CCommsDevice * CCommsDeviceList::FindDevice(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		if( pDevice->m_Name == Name ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

CCommsSysBlock * CCommsDeviceList::FindBlock(UINT uBlock) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsSysBlock *pBlock;

		if( pBlock = GetItem(n)->FindBlock(uBlock) ) {

			return pBlock;
			}

		GetNext(n);
		}

	return NULL;
	}

// Operations

void CCommsDeviceList::DeleteAll(void)
{
	DeleteAllItems(TRUE);
	}

void CCommsDeviceList::UpdateDriver(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		pDevice->UpdateDriver();
		}
	}

void CCommsDeviceList::ClearSysBlocks(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		pDevice->ClearSysBlocks();
		}
	}

void CCommsDeviceList::NotifyInit(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		pDevice->NotifyInit();
		}
	}

BOOL CCommsDeviceList::CheckMapBlocks(void)
{
	BOOL fSave = FALSE;

	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		if( pDevice->CheckMapBlocks() ) {

			fSave = TRUE;
			}
		}

	return fSave;
	}

void CCommsDeviceList::Validate(BOOL fExpand)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsDevice *pDevice = GetItem(n);

		pDevice->Validate(fExpand);
		}
	}

// Download Support

BOOL CCommsDeviceList::MakeInitData(CInitData &Init)
{
	return CItemList::MakeInitData(Init);
	}

// End of File
