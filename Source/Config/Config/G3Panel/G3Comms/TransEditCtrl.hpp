
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TransEditCtrl_HPP

#define INCLUDE_TransEditCtrl_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangManager;

/////////////////////////////////////////////////////////////////////////
//
// Translation Edit Control
//

class CTransEditCtrl : public CDropEditCtrl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTransEditCtrl(CUIElement *pUI);

		// Operations
		void SetSlot(CLangManager *pLang, UINT uSlot);

	protected:
		// Data Members
		CLangManager * m_pLang;
		UINT           m_uSlot;
		BOOL	       m_fKeyb;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
	};

// End of File

#endif
