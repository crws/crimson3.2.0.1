
#include "Intern.hpp"

#include "SecDesc.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Security Descriptor
//

// Dynamic Class

AfxImplementDynamicClass(CSecDesc, CUIItem);

// Constructor

CSecDesc::CSecDesc(void)
{
	m_Access  = allowDefault;

	m_Logging = 3;
}

// UI Creation

BOOL CSecDesc::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		pList->Append(New CUIStdPage(AfxThisClass(), 1));

		return FALSE;
	}

	pList->Append(New CUIStdPage(AfxThisClass(), 2));

	return FALSE;
}

// UI Update

void CSecDesc::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
	}
}

// Download Support

BOOL CSecDesc::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Access));

	Init.AddByte(BYTE(m_Logging));

	return TRUE;
}

// Meta Data Creation

void CSecDesc::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Access);
	Meta_AddInteger(Logging);
}

// Implementation

void CSecDesc::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Logging", m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B);
}

// End of File
