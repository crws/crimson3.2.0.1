
#include "Intern.hpp"

#include "UITextExprEnumInt.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable Enumerated Integer
//

// Implement Class

AfxImplementDynamicClass(CUITextExprEnumInt, CUITextExprEnum);

// Constructor

CUITextExprEnumInt::CUITextExprEnumInt(void)
{
	m_uFlags = textEnum | textEdit;

	m_uMin   = 0;

	m_uMax   = 60000;
	}

// Overridables

void CUITextExprEnumInt::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	UINT c = GetFormat().Tokenize(List, '|');

	if( c >= 2 ) {

		m_uMin = watoi(List[0]);

		m_uMax = watoi(List[1]);

		for( UINT n = 2; n < c; n++ ) {

			CEntry Enum;

			Enum.m_Data = watoi(List[n]);

			Enum.m_Text = List[n];

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}
		}

	ShowTwoWay();
	}

CString CUITextExprEnumInt::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.GetLength() ) {

		if( IsNumberConst(Text) ) {

			UINT uData = wcstol(Text.Mid(2), NULL, 16);

			return CPrintf(L"%u", uData);
			}

		return L'=' + Text;
		}

	return Text;
	}

UINT CUITextExprEnumInt::OnSetAsText(CError &Error, CString Text)
{
	if( Text.GetLength() ) {

		if( Text[0] == '=' ) {

			Text.Delete(0, 1);

			if( Text.IsEmpty() ) {

				Error.Set(CString(IDS_YOU_MUST_ENTER));

				return saveError;
				}

			return CUITextCoded::OnSetAsText(Error, Text);
			}

		UINT    uData = watoi(Text);

		CString Prev  = CUITextCoded::OnGetAsText();

		if( IsNumberConst(Prev) ) {

			UINT uPrev = wcstol(Prev.Mid(2), NULL, 16);

			if( uData - uPrev ) {

				if( uData >= m_uMin && uData <= m_uMax ) {

					CString Expr = CPrintf(L"0x%X", uData);

					return CUITextCoded::OnSetAsText(Error, Expr);
					}

				CPrintf Text( CString(IDS_VALUE_MUST_BE),
					      m_uMin,
					      m_uMax
					      );

				Error.Set(Text);

				return saveError;
				}

			return saveSame;
			}

		CString Expr = CPrintf(L"0x%X", uData);

		return CUITextCoded::OnSetAsText(Error, Expr);
		}

	return CUITextCoded::OnSetAsText(Error, Text);
	}

// End of File
