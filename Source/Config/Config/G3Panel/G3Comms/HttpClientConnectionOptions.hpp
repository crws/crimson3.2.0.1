
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_HttpClientConnectionOptions_HPP

#define INCLUDE_HttpClientConnectionOptions_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpConnectionOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Connection Options
//

class CHttpClientConnectionOptions : public CHttpConnectionOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHttpClientConnectionOptions(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Check;
		UINT m_ConnTimeout;
		BOOL m_CompRequest;
		BOOL m_CompReply;
		UINT m_Ver;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
