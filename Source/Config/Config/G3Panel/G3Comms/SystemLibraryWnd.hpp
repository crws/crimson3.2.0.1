
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SystemLibraryWnd_HPP

#define INCLUDE_SystemLibraryWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// System Library Window
//

class CSystemLibraryWnd : public CResTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSystemLibraryWnd(void);

	protected:
		// Data Members
		UINT m_cfCode;
		UINT m_cfFunc;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnSetFocus(CWnd &Wnd);

		// Command Handlers
		BOOL OnResGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnResControl(UINT uID, CCmdSource &Src);
		BOOL OnResCommand(UINT uID);

		// Tree Loading
		void LoadImageList(void);
		void SortTree(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void NewItemSelected(void);

		// Data Object Creation
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL AddCodeFragment(CDataObject *pData);

		// Sort Helpers
		int Sort(CMetaItem *p1, CMetaItem *p2);

		// Sort Callback
		static int CALLBACK SortHelp(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

		// Implementation
		void ShowPrototype(void);
		BOOL FindHelp(void);
	};

// End of File

#endif
