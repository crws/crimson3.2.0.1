
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramPrototypePage_HPP

#define INCLUDE_ProgramPrototypePage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramPrototype;

//////////////////////////////////////////////////////////////////////////
//
// Program Protoype Page
//

class CProgramPrototypePage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CProgramPrototypePage(CProgramPrototype *pPrototype);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CProgramPrototype * m_pPrototype;
	};

// End of File

#endif
