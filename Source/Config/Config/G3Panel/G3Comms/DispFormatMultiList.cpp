
#include "Intern.hpp"

#include "DispFormatMultiList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispFormatMultiEntry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State List
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatMultiList, CItemList);

// Constructor

CDispFormatMultiList::CDispFormatMultiList(void)
{
	m_Class = AfxRuntimeClass(CDispFormatMultiEntry);
	}

// Item Access

CDispFormatMultiEntry * CDispFormatMultiList::GetItem(INDEX Index) const
{
	return (CDispFormatMultiEntry *) CItemList::GetItem(Index);
	}

CDispFormatMultiEntry * CDispFormatMultiList::GetItem(UINT uPos) const
{
	return (CDispFormatMultiEntry *) CItemList::GetItem(uPos);
	}

// End of File
