
#include "Intern.hpp"

#include "ServiceCloudSparkplug.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "MqttClientOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Sparkplug Cloud Service
//

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudSparkplug, CServiceCloudBase);

// Constructor

CServiceCloudSparkplug::CServiceCloudSparkplug(void)
{
	m_bServCode   = servCloudSparkplug;

	m_Reboot      = 0;

	m_pGroupId    = NULL;

	m_pNodeId     = NULL;

	m_pTagsFolder = NULL;

	m_pPriAppId   = NULL;
}

// Initial Values

void CServiceCloudSparkplug::SetInitValues(void)
{
	SetInitial(L"GroupId", m_pGroupId, L"\"Crimson Devices\"");

	SetInitial(L"NodeId", m_pNodeId, L"\"CrimsonNode1\"");

	SetInitial(L"TagsFolder", m_pTagsFolder, L"\"Tags\"");

	CServiceCloudBase::SetInitValues();
}

// Type Access

BOOL CServiceCloudSparkplug::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"GroupId" || Tag == L"NodeId" || Tag == L"TagsFolder" || Tag == L"PriAppId" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return CServiceCloudBase::GetTypeData(Tag, Type);
}

// UI Update

void CServiceCloudSparkplug::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			DoEnables(pHost);
		}
	}

	CServiceCloudBase::OnUIChange(pHost, pItem, Tag);
}

// Persistance

void CServiceCloudSparkplug::Init(void)
{
	CServiceCloudBase::Init();

	InitOptions();
}

void CServiceCloudSparkplug::PostLoad(void)
{
	m_pOpts->SetClientId(L"");

	CServiceCloudBase::PostLoad();
}

// Property Save Filter

BOOL CServiceCloudSparkplug::SaveProp(CString Tag) const
{
	if( Tag == L"AuthFile" ) {

		return FALSE;
	}

	return CServiceCloudBase::SaveProp(Tag);
}

// Conversion

void CServiceCloudSparkplug::PostConvert(void)
{
	ImportCert(L"SPB", m_AuthFile);

	CServiceCloudBase::PostConvert();
}

// Download Support

BOOL CServiceCloudSparkplug::MakeInitData(CInitData &Init)
{
	CServiceCloudBase::MakeInitData(Init);

	Init.AddByte(BYTE(m_Reboot));

	Init.AddItem(itemVirtual, m_pGroupId);

	Init.AddItem(itemVirtual, m_pNodeId);

	Init.AddItem(itemVirtual, m_pTagsFolder);

	Init.AddItem(itemVirtual, m_pPriAppId);

	return TRUE;
}

// Meta Data Creation

void CServiceCloudSparkplug::AddMetaData(void)
{
	CServiceCloudBase::AddMetaData();

	Meta_AddInteger(Reboot);
	Meta_AddString(AuthFile);
	Meta_AddVirtual(GroupId);
	Meta_AddVirtual(NodeId);
	Meta_AddVirtual(TagsFolder);
	Meta_AddVirtual(PriAppId);

	Meta_SetName((IDS_SPARKPLUG_MQTT));
}

// Implementation

void CServiceCloudSparkplug::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Reboot", fEnable);

	pHost->EnableUI(this, "GroupId", fEnable);

	pHost->EnableUI(this, "NodeId", fEnable);

	pHost->EnableUI(this, "TagsFolder", fEnable);

	pHost->EnableUI(this, "PriAppId", fEnable);
}

void CServiceCloudSparkplug::InitOptions(void)
{
	m_pOpts->m_Tls       = FALSE;

	m_pOpts->m_Check     = 3;

	m_pOpts->m_Port      = 1883;

	m_pOpts->m_KeepAlive = 600;

	m_pOpts->m_Advanced  = 1;

	m_pOpts->SetPeerName(L"192.168.100.1");

	m_pOpts->SetCredentials(L"admin", L"changeme");
}

// End of File
