
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudGeneric_HPP

#define INCLUDE_ServiceCloudGeneric_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceCloudJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Cloud Client Configuration
//

class CServiceCloudGeneric : public CServiceCloudJson
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CServiceCloudGeneric(void);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Persistance
	void Init(void);

	// Item Properties
	CCodedItem * m_pPub;
	CCodedItem * m_pSub;
	UINT         m_NoDollar;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
	void InitOptions(void);
};

// End of File

#endif
