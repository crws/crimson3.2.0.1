
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TetheredItem_HPP

#define INCLUDE_TetheredItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CascadedRackItem.hpp"

#include "UsbTreePath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tethered Rack Itme
//

class DLLAPI CTetheredItem : public CCascadedRackItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTetheredItem(void);

		// Operations
		void SetHostPorts(CUsbTreePath const &HostA, CUsbTreePath const &HostB);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void PostLoad(void);

		// Item Properties
		UINT	m_Port;
		UINT    m_HostA;
		UINT    m_HostB;

	protected:
		// Data
		CUsbTreePath m_RackTable[4];
		CUsbTreePath m_SlotTable[12];

		// Meta Data Creation
		void AddMetaData(void);

		// Path Config
		void  InitRackTable(void);
		void  InitSlotTable(void);
		DWORD GetRackPath(UINT uRack) const;
		DWORD GetSlotPath(UINT uRack, UINT uSlot) const;
	};

// End of File

#endif
