
#include "Intern.hpp"

#include "CommsPortGroup.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Communications Port Group
//

// Runtime Class

AfxImplementRuntimeClass(CCommsPortGroup, CMetaItem);

// Constructor

CCommsPortGroup::CCommsPortGroup(void)
{
	m_Name = CString(IDS_PORT_GROUP);
	}

// Meta Data Creation

void CCommsPortGroup::AddMetaData(void)
{
	Meta_AddString(Name);
	}

// End of File
