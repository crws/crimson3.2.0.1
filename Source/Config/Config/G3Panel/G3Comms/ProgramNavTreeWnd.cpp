
#include "Intern.hpp"

#include "ProgramNavTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "ProgramCatWnd.hpp"
#include "ProgramFolder.hpp"
#include "ProgramItem.hpp"
#include "ProgramList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CProgramNavTreeWnd, CNavTreeWnd);

// Constructor

CProgramNavTreeWnd::CProgramNavTreeWnd(void) : CNavTreeWnd( L"Programs",
							    AfxRuntimeClass(CProgramFolder),
							    AfxRuntimeClass(CProgramItem)
							    )
{
	}

// Overridables

void CProgramNavTreeWnd::OnAttach(void)
{
	CNavTreeWnd::OnAttach();

	m_pSystem = (CCommsSystem *) m_pItem->GetParent();
	}

// Attributes

BOOL CProgramNavTreeWnd::IsProgramSelected(void)
{
	return m_pSelect->IsKindOf(m_Class);
	}

// Message Map

AfxMessageMap(CProgramNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, NM_CUSTOMDRAW, OnTreeCustomDraw)

	AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM, OnItemControl)
	AfxDispatchCommandType(IDM_ITEM, OnItemCommand)

	AfxMessageEnd(CProgramNavTreeWnd)
	};

// Message Handlers

void CProgramNavTreeWnd::OnPostCreate(void)
{
	CNavTreeWnd::OnPostCreate();

	CSysProxy Proxy;

	Proxy.Bind();

	CProgramCatWnd *pCat = (CProgramCatWnd *) Proxy.GetCatView(AfxRuntimeClass(CProgramCatWnd));

	pCat->SetNavTree(this);
	}

void CProgramNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ProgramNavTreeTool"));
		}
	}

// Notification Handlers

UINT CProgramNavTreeWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	// LATER -- This kind of thing is used in a few placed, so
	// let's make it a common feature of some subclass and provide
	// a way to pass-in the coloring information...

	// LATER -- Not always updating on recompilation.

	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
		}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMTVCUSTOMDRAW &Extra = (NMTVCUSTOMDRAW &) Info;

		HTREEITEM       hItem = HTREEITEM(Info.dwItemSpec);

		CColor          Color = Extra.clrText;

		if( hItem ) {

			CItem *pItem = (CItem *) Info.lItemlParam;

			if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

				CProgramItem *pProg = (CProgramItem *) pItem;

				if( pProg->IsBroken() ) {

					Color = CColor(255,0,0);
					}
				}
			}

		if( hItem ) {

			UINT uTest = m_pTree->GetItemState(hItem, NOTHING);

			if( uTest & TVIS_DROPHILITED ) {

				Extra.clrTextBk = afxColor(Orange2);

				Extra.clrText   = Color;

				SelectObject(Info.hdc, m_pTree->GetFont());

				return CDRF_NEWFONT;
				}
			}

		if( Info.uItemState & CDIS_HOT ) {

			if( Info.uItemState & CDIS_SELECTED ) {

				Extra.clrTextBk = afxColor(Orange2);
				}
			else
				Extra.clrTextBk = afxColor(Orange3);

			Extra.clrText = Color;

			SelectObject(Info.hdc, m_pTree->GetFont());

			return CDRF_NEWFONT;
			}

		Extra.clrText = Color;
		}

	return 0;
	}

// Command Handlers

BOOL CProgramNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Info.m_Image   = 0x4000000E;

			Info.m_ToolTip = CString(IDS_NEW_PROGRAM);

			Info.m_Prompt  = CString(IDS_ADD_NEW_PROGRAM);

			return TRUE;
		}

	return FALSE;
	}

BOOL CProgramNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Src.EnableItem(!IsItemLocked(m_hSelect));

			return TRUE;

		case IDM_ITEM_USAGE:

			Src.EnableItem(m_pSelect->IsKindOf(m_Class));

			return TRUE;
		}

	return FALSE;
	}

BOOL CProgramNavTreeWnd::OnItemCommand(UINT uID)
{
	CProgramItem *pItem;

	switch( uID ) {

		case IDM_ITEM_NEW:

			OnItemNew();

			return TRUE;

		case IDM_ITEM_USAGE:

			pItem = (CProgramItem *) m_pSelect;

			m_pSystem->FindHandleUsage( CString(IDS_PROGRAM),
						    pItem->GetName(),
						    pItem->m_Handle
						    );

			return TRUE;
		}

	return FALSE;
	}

void CProgramNavTreeWnd::OnItemNew(void)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(IDS_FORMAT_4, n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Class);

			m_System.ExecCmd(pCmd);

			break;
			}
		}
	}

// Tree Loading

void CProgramNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"ProgTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CProgramNavTreeWnd::GetRootImage(void)
{
	return 0;
	}

UINT CProgramNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(m_Class) ) {

		CProgramItem *pProgram = (CProgramItem *) pItem;

		UINT uImage = pProgram->GetTreeImage();

		return uImage;
		}

	return 22;
	}

BOOL CProgramNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"ProgramNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"ProgramNavTreeMissCtxMenu";

	return FALSE;
	}

void CProgramNavTreeWnd::OnItemDeleted(CItem *pItem, BOOL fExec)
{
	if( pItem ) {

		if( pItem->IsKindOf(m_Class) ) {

			CProgramItem *pProgram = (CProgramItem *) pItem;

			CProgramList *pList    = (CProgramList *) pItem->GetParent();

			m_pSystem->ObjCheck(pProgram->m_Handle, FALSE);

			pList->UpdatePending();

			pProgram->CheckKillCode();
			}
		}
	}

void CProgramNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	if( pItem->IsKindOf(m_Class) ) {

		CProgramItem * pProgram = (CProgramItem *) pItem;

		m_pSystem->ObjCheck(pProgram->m_Handle, TRUE);

		pProgram->CheckKillPrev();

		pProgram->CheckSaveCode();
		}

	CNavTreeWnd::OnItemRenamed(pItem);
	}

// End of File
