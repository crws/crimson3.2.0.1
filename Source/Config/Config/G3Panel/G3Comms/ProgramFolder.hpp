
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramFolder_HPP

#define INCLUDE_ProgramFolder_HPP

//////////////////////////////////////////////////////////////////////////
//
// Program Folder
//

class DLLNOT CProgramFolder : public CFolderItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramFolder(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
