
#include "Intern.hpp"

#include "UITagMonitor.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Tag Monitor
//

// Dynamic Class

AfxImplementDynamicClass(CUITagMonitor, CUIExpression);

// Constructor

CUITagMonitor::CUITagMonitor(void)
{
	m_fComms = FALSE;

	m_fComp  = FALSE;

	m_fTags  = TRUE;

	m_fEmpty = TRUE;

	m_fExpr  = TRUE;
	}

// End of File
