
#include "Intern.hpp"

#include "WatchViewWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "Tag.hpp"
#include "TagList.hpp"
#include "TagSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Shell Link Library
//

#include <shlink.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Watch View Window
//

// Runtime Class

AfxImplementRuntimeClass(CWatchViewWnd, CWnd);

// Constructor

CWatchViewWnd::CWatchViewWnd(CCommsSystem *pSystem)
{
	m_pSystem = pSystem;

	m_pTool   = New CToolbarWnd(barTight);

	m_pStat   = New CStatusWnd;

	m_pView   = New CListView;

	m_uItem   = NOTHING;
	}

// IUnknown

HRESULT CWatchViewWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return E_NOINTERFACE;
	}

ULONG CWatchViewWnd::AddRef(void)
{
	return 1;
	}

ULONG CWatchViewWnd::Release(void)
{
	return 1;
	}

// IUpdate

HRESULT CWatchViewWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateRename || uType == updateValue ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CTag)) ) {

			CTag    *pTag   = (CTag *) pItem;

			CTagSet *pWatch = m_pSystem->m_pWatch;

			if( pWatch->HasTag(pTag->GetIndex()) ) {

				Update();
				}
			}
		}

	if( uType == updateChildren ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CTagList)) ) {

			Update();
			}
		}

	return S_OK;
	}

// Operations

void CWatchViewWnd::Empty(void)
{
	Link_WatchClose();

	m_pView->DeleteAllItems();

	m_pStat->SetPrompt(CString(IDS_WATCH_LIST_IS));

	m_uItem = NOTHING;
	}

BOOL CWatchViewWnd::Update(void)
{
	Empty();

	CTagSet *pWatch = m_pSystem->m_pWatch;

	UINT     uCount = pWatch->GetCount();

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CString Name = pWatch->GetName(n);

			CString Data = pWatch->GetSimulatedData(n);

			CString Hex  = CPrintf("0x%8.8X", pWatch->Execute(n));

			int i = m_pView->InsertItem(CListViewItem(n, 0, Name, 0, LPARAM(n)));

			m_pView->SetItem(CListViewItem(i, 1, Data, 0));

			m_pView->SetItem(CListViewItem(i, 2, Hex, 0));
			}

		SetColWidths();

		m_pStat->SetPrompt(CString(IDS_SHOWING_SIMULATED));

		return TRUE;
		}

	SetColWidths();

	return FALSE;
	}

// Routing Control

BOOL CWatchViewWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pTool && m_pTool->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	if( m_pStat && m_pStat->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	if( m_pView && m_pView->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	return CWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CWatchViewWnd, CWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_UPDATEUI)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchNotify (200, LVN_ITEMCHANGED, OnItemChanged)
	AfxDispatchNotify (200, LVN_KEYDOWN,     OnViewKeyDown)

	AfxDispatchCommandType(IDM_GLOBAL, OnGlobalCommand)

	AfxDispatchGetInfoType(0xC0, OnToolGetInfo)
	AfxDispatchControlType(0xC0, OnToolControl)
	AfxDispatchCommandType(0xC0, OnToolCommand)

	AfxDispatchCommand(IDINIT, OnCommsInit)
 	AfxDispatchCommand(IDDONE, OnCommsDone)
 	AfxDispatchCommand(IDFAIL, OnCommsFail)
	AfxDispatchCommand(IDKILL, OnCommsKill)
	AfxDispatchCommand(IDCRED, OnCommsCred)

	AfxMessageEnd(CWatchViewWnd)
	};

// Message Handlers

void CWatchViewWnd::OnPostCreate(void)
{
	Register();

	SendMessage(WM_UPDATEUI);

	m_nTop      = m_pTool->GetHeight();

	m_nBotton   = 18;

	CRect Rect  = GetClientRect();

	CRect Tool  = Rect;

	CRect Stat  = Rect;

	CRect View  = Rect;

	Tool.bottom = View.top    = Tool.top    + m_nTop;

	Stat.top    = View.bottom = Stat.bottom - m_nBotton;

	CreateTool(Tool);

	CreateStat(Stat);

	CreateView(View);
	}

void CWatchViewWnd::OnDestroy(void)
{
	Link_WatchClose();
	}

void CWatchViewWnd::OnUpdateUI(void)
{
	m_pTool->AddGadget(New CButtonGadget(0xC001, 0x10000008, CString(IDS_DELETE_1)));

	m_pTool->AddGadget(New CButtonGadget(0xC002, 0x10000038, CString(IDS_EMPTY)));

	m_pTool->AddGadget(New CRidgeGadget);

	m_pTool->AddGadget(New CButtonGadget(0xC003, 0x10000039, CString(IDS_VIEW_ONLINE)));

	m_pTool->AddGadget(New CRidgeGadget);
	}

void CWatchViewWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		CRect Rect  = GetClientRect();

		CRect Tool  = Rect;

		CRect Stat  = Rect;

		CRect View  = Rect;

		Tool.bottom = View.top    = Tool.top    + m_nTop;

		Stat.top    = View.bottom = Stat.bottom - m_nBotton;

		m_pTool->MoveWindow(Tool, TRUE);

		m_pStat->MoveWindow(Stat, TRUE);

		m_pView->MoveWindow(View, TRUE);

		SetColWidths();
		}
	}

void CWatchViewWnd::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
	}

BOOL CWatchViewWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	LPARAM lParam = m_MsgCtx.Msg.lParam;

	if( uID >= 0xF000 ) {

		AfxCallDefProc();

		return TRUE;
		}

	if( uID >= 0x8000 ) {

		CCmdSourceData Source;

		if( uID < 0xF000 ) {

			Source.PrepareSource();
			}

		RouteControl(uID, Source);

		if( Source.GetFlags() & MF_DISABLED ) {

			MessageBeep(0);

			return TRUE;
			}

		RouteCommand(uID, lParam);

		return TRUE;
		}

	SendMessage(WM_AFX_COMMAND, uID, lParam);

	return TRUE;
	}

void CWatchViewWnd::OnKeyDown(UINT uCode, DWORD dwData)
{
	RelayKey(uCode);
	}

void CWatchViewWnd::OnGoingIdle(void)
{
	m_pTool->PollGadgets();
	}

// Notification Handlers

void CWatchViewWnd::OnItemChanged(UINT uID, NMLISTVIEW &Info)
{
	if( (Info.uOldState ^ Info.uNewState) & LVIS_SELECTED ) {

		if( Info.uNewState & LVIS_SELECTED ) {

			m_uItem = Info.iItem;
			}
		else
			m_uItem = NOTHING;
		}
	}

BOOL CWatchViewWnd::OnViewKeyDown(UINT uID, NMLVKEYDOWN &Info)
{
	if( Info.wVKey == VK_DELETE ) {

		if( m_uItem < NOTHING ) {

			OnDelete();
			}
		}

	RelayKey(Info.wVKey);

	return FALSE;
	}

// Command Handlers

BOOL CWatchViewWnd::OnGlobalCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GLOBAL_DROP_LINK:

			if( Link_WatchIsActive() ) {

				OnGoOnline();
				}

			break;
		}

	return FALSE;
	}

BOOL CWatchViewWnd::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	return TRUE;
	}

BOOL CWatchViewWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	if( GetParent().IsActive() ) {

		switch( uID ) {

			case 0xC001:

				Src.EnableItem(m_uItem < NOTHING);

				break;

			case 0xC002:

				Src.EnableItem(m_pView->GetItemCount() > 0);

				break;

			case 0xC003:

				Src.CheckItem (Link_WatchIsActive());

				Src.EnableItem(m_pView->GetItemCount() > 0);

				break;
			}
		}
	else {
		switch( uID ) {

			case 0xC003:

				Src.CheckItem (Link_WatchIsActive());

				break;
			}
		}

	return TRUE;
	}

BOOL CWatchViewWnd::OnToolCommand(UINT uID)
{
	switch( uID ) {

		case 0xC001:

			OnDelete();

			break;

		case 0xC002:

			OnEmpty();

			break;

		case 0xC003:

			OnGoOnline();

			break;
		}

	return TRUE;
	}

void CWatchViewWnd::OnDelete(void)
{
	Link_WatchClose();

	CTagSet *pWatch = m_pSystem->m_pWatch;

	UINT     uItem  = m_uItem;

	m_pView->DeleteItem(uItem);

	pWatch ->Delete(uItem);

	if( uItem >= m_pView->GetItemCount() ) {

		if( !uItem ) {

			uItem = NOTHING;

			return;
			}

		uItem--;
		}

	m_pView->SetItemState(uItem, LVIS_SELECTED, LVIS_SELECTED);
	}

void CWatchViewWnd::OnEmpty(void)
{
	CTagSet *pWatch = m_pSystem->m_pWatch;

	pWatch->Empty();

	Empty();
	}

void CWatchViewWnd::OnGoOnline(void)
{
	if( !Link_WatchIsActive() ) {

		DropLink();

		CDatabase *pDbase = m_pSystem->GetDatabase();

		CTagSet   *pWatch = m_pSystem->m_pWatch;

		if( Link_Verify(pDbase, TRUE) ) {

			PCDWORD pList = PCDWORD(pWatch->m_Data.GetPointer());

			UINT    uList = pWatch->GetCount();

			Link_WatchStart(this, pList, uList);

			ShowUnknown();
			}
		}
	else {
		Link_WatchClose();

		ShowSimData(TRUE);
		}
	}

// Comms Notifications

BOOL CWatchViewWnd::OnCommsInit(UINT uID)
{
	Link_WatchUpdate();

	return TRUE;
	}

BOOL CWatchViewWnd::OnCommsDone(UINT uID)
{
	if( Link_WatchIsActive() ) {

		CTagSet *pWatch = m_pSystem->m_pWatch;

		UINT     uCount = pWatch->GetCount();

		if( uCount ) {

			PDWORD   pData  = New DWORD [ uCount ];

			PBOOL    pAvail = New BOOL  [ uCount ];

			Link_WatchGetData(pData, pAvail);

			for( UINT n = 0; n < uCount; n++ ) {

				CString Data = L"n/a";

				CString Hex  = L"n/a";

				if( pAvail[n] ) {

					Data = pWatch->GetFormattedData(n, pData[n]);

					Hex  = CPrintf("0x%8.8X", pData[n]);
					}

				m_pView->SetItem(CListViewItem(n, 1, Data, 0));

				m_pView->SetItem(CListViewItem(n, 2, Hex, 0));
				}

			delete[] pData;

			delete[] pAvail;

			Link_WatchUpdate();

			m_pStat->SetPrompt(CString(IDS_SHOWING_LIVE_DATA));
			}
		}

	return TRUE;
	}

BOOL CWatchViewWnd::OnCommsFail(UINT uID)
{
	CString Text;

	Link_WatchGetError(Text);

	m_pStat->SetPrompt(CString(IDS_ERROR) + Text);

	Link_WatchClose();

	ShowSimData(FALSE);

	return TRUE;
	}

BOOL CWatchViewWnd::OnCommsKill(UINT uID)
{
	Link_WatchClose();

	return TRUE;
	}

BOOL CWatchViewWnd::OnCommsCred(UINT uID)
{
	Link_WatchAskForCredentials();

	return TRUE;
}

// Implementation

void CWatchViewWnd::Register(void)
{
	m_Proxy.Bind();

	m_Proxy.RegisterForUpdates(this);
	}

void CWatchViewWnd::CreateTool(CRect Tool)
{
	m_pTool->Create(Tool, ThisObject);

	m_pTool->PollGadgets();

	m_pTool->ShowWindow(SW_SHOW);
	}

void CWatchViewWnd::CreateStat(CRect Stat)
{
	m_pStat->Create(Stat, ThisObject);

	m_pStat->PollGadgets();

	m_pStat->ShowWindow(SW_SHOW);
	}

void CWatchViewWnd::CreateView(CRect View)
{
	DWORD dwStyle   = LVS_REPORT        |
			  LVS_SINGLESEL     |
			  LVS_NOSORTHEADER  |
			  WS_TABSTOP        ;

	DWORD dwExStyle = LVS_EX_FULLROWSELECT;

	m_pView->Create( dwStyle,
			 View,
			 ThisObject,
			 200
			 );

	m_pView->SetExtendedListViewStyle( dwExStyle,
					   dwExStyle
					   );

	m_pView->ShowWindow(SW_SHOW);

	LoadCols();

	Update();
	}

void CWatchViewWnd::LoadCols(void)
{
	CListViewColumn Col0(0, LVCFMT_LEFT, 1, CString(IDS_NAME));

	CListViewColumn Col1(1, LVCFMT_LEFT, 1, CString(IDS_VALUE));

	CListViewColumn Col2(2, LVCFMT_LEFT, 1, CString(IDS_VALUE_HEX));

	m_pView->InsertColumn(0, Col0);

	m_pView->InsertColumn(1, Col1);

	m_pView->InsertColumn(2, Col2);
	}

void CWatchViewWnd::SetColWidths(void)
{
	CRect Rect = m_pView->GetWindowRect();

	int   cx   = Rect.cx();

	SCROLLBARINFO Info;

	Info.cbSize = sizeof(Info);

	GetScrollBarInfo(m_pView->GetHandle(), OBJID_VSCROLL, &Info);

	int c0 = cx / 2;

	int c1 = cx - c0;

	if( Info.rgstate[0] != STATE_SYSTEM_INVISIBLE ) {

		int cxScroll = GetSystemMetrics(SM_CXVSCROLL);

		c1 -= cxScroll;
		}

	m_pView->SetColumnWidth(0, c0);

	m_pView->SetColumnWidth(1, c1 / 2);

	m_pView->SetColumnWidth(2, c1 / 2);
	}

void CWatchViewWnd::RelayKey(UINT uCode)
{
	switch( uCode ) {

		case VK_F7:
		case VK_ESCAPE:

			GetParent().SendMessage(WM_KEYDOWN, uCode, 0);

			break;
		}
	}

BOOL CWatchViewWnd::ShowSimData(BOOL fStat)
{
	CTagSet *pWatch = m_pSystem->m_pWatch;

	UINT     uCount = pWatch->GetCount();

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CString Data = pWatch->GetSimulatedData(n);

			CString Hex  = CPrintf("0x%8.8X", pWatch->Execute(n));

			m_pView->SetItem(CListViewItem(n, 1, Data, 0));

			m_pView->SetItem(CListViewItem(n, 2, Hex, 0));
			}

		if( fStat ) {

			m_pStat->SetPrompt(CString(IDS_SHOWING_SIMULATED));
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CWatchViewWnd::ShowUnknown(void)
{
	CTagSet *pWatch = m_pSystem->m_pWatch;

	UINT     uCount = pWatch->GetCount();

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CString Data = L"n/a";

			m_pView->SetItem(CListViewItem(n, 1, Data, 0));

			m_pView->SetItem(CListViewItem(n, 2, Data, 0));
			}

		m_pStat->SetPrompt(CString(IDS_WAITING_FOR_DATA));

		return TRUE;
		}

	return FALSE;
	}

BOOL CWatchViewWnd::DropLink(void)
{
	afxMainWnd->RouteCommand(IDM_GLOBAL_DROP_LINK, 0);

	return TRUE;
	}

// End of File
