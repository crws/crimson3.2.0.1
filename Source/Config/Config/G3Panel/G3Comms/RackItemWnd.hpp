
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_RackItemWnd_HPP

#define INCLUDE_RackItemWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Backplane Configuration View
//

class CRackItemWnd : public CProxyViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRackItemWnd(void);

	protected:
		// Overridables
		void OnAttach(void);
	};

// End of File

#endif
