
#include "Intern.hpp"

#include "TagFlag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "CommsDevice.hpp"
#include "CommsManager.hpp"
#include "CommsSystem.hpp"
#include "DispColorFlag.hpp"
#include "DispFormatFlag.hpp"
#include "SecDesc.hpp"
#include "TagEventFlag.hpp"
#include "TagFlagPage.hpp"
#include "TagTriggerFlag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Item
//

// Dynamic Class

AfxImplementDynamicClass(CTagFlag, CDataTag);

// Constructor

CTagFlag::CTagFlag(void)
{
	m_FlagTreatAs	= 0;
	m_TakeBit	= 0;
	m_Manipulate	= 0;
	m_Atomic	= 0;
	m_HasSP		= 0;
	m_pSetpoint	= NULL;
	m_pEvent1	= New CTagEventFlag;
	m_pEvent2	= New CTagEventFlag;
	m_pTrigger1	= New CTagTriggerFlag;
	m_pTrigger2	= New CTagTriggerFlag;
	m_pSec		= New CSecDesc;
	}

// UI Creation

BOOL CTagFlag::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() == SW_GROUP_2 ) {

		CTagFlagPage *pPage1 = New CTagFlagPage(this, CString(IDS_DATA), 8);

		pList->Append(pPage1);
		}
	else {
		if( FALSE ) {

			CTagFlagPage *pPage1 = New CTagFlagPage(this, CString(IDS_DATA),     1);

			CTagFlagPage *pPage2 = New CTagFlagPage(this, CString(IDS_FORMAT_6), 2);

			CTagFlagPage *pPage5 = New CTagFlagPage(this, CString(IDS_TRIGGERS), 5);

			pList->Append(pPage1);

			pList->Append(pPage2);

			pList->Append(pPage5);
			}
		else {
			CTagFlagPage *pPage1 = New CTagFlagPage(this, CString(IDS_DATA),     1);

			CTagFlagPage *pPage2 = New CTagFlagPage(this, CString(IDS_FORMAT_6), 2);

			CTagFlagPage *pPage3 = New CTagFlagPage(this, CString(IDS_COLORS),   3);

			CTagFlagPage *pPage4 = New CTagFlagPage(this, CString(IDS_ALARMS),   4);

			CTagFlagPage *pPage5 = New CTagFlagPage(this, CString(IDS_TRIGGERS), 5);

			CTagFlagPage *pPage6 = New CTagFlagPage(this, CString(IDS_SECURITY), 6);

			pList->Append(pPage1);

			pList->Append(pPage2);

			pList->Append(pPage3);

			pList->Append(pPage4);

			pList->Append(pPage5);

			pList->Append(pPage6);
			}
		}

	return TRUE;
	}

// UI Update

void CTagFlag::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		LimitTreatAs(pHost);

		LimitTakeBit(pHost);

		LimitAccess(pHost);

		DoEnables(pHost);
		}

	if( Tag == "Value" ) {

		if( m_pValue ) {

			if( m_fIntern ) {

				if( m_pValue->GetType() == typeReal ) {

					m_FlagTreatAs = 2;

					pHost->UpdateUI("FlagTreatAs");
					}
				}

			if( m_pValue->IsCommsRef() ) {

				UpdateExtent();

				CheckCircular(pHost);
				}
			else {
				if( m_pValue->GetFlags() & flagLogical ) {

					m_FlagTreatAs = 0;

					pHost->UpdateUI("FlagTreatAs");
					}

				if( m_fIntern ) {

					m_Access = 2;

					Recompile(pHost);

					pHost->UpdateUI(this, "Access");
					}

				m_Extent     = 0;

				m_Manipulate = 0;

				m_Atomic     = 0;

				pHost->UpdateUI("Extent");

				pHost->UpdateUI("Manipulate");

				pHost->UpdateUI("Atomic");

				CheckCircular(pHost, m_pValue);
				}

			if( !m_pValue->IsWritable() ) {

				MakeReadOnly(pHost);
				}

			if( m_pValue->IsConst() ) {

				KillCoded(pHost, L"Sim", m_pSim);
				}
			}
		else {
			UINT Access  = m_Access;

			m_Access     = 0;

			m_Manipulate = 0;

			m_Atomic     = 0;

			pHost->UpdateUI(this, "Access");

			pHost->UpdateUI(this, "Manipulate");

			pHost->UpdateUI(this, "Atomic");

			if( Access ) {

				Recompile(pHost);
				}

			CheckCircular(pHost);
			}

		LimitTreatAs(pHost);

		LimitTakeBit(pHost);

		LimitAccess(pHost);

		CheckPersist(pHost);

		pHost->SendUpdate(updateProps);

		DoEnables(pHost);
		}

	if( Tag == "FlagTreatAs" ) {

		if( m_pValue ) {

			UpdateExtent();
			}

		if( m_FlagTreatAs <= 2 ) {

			m_Atomic = 0;

			pHost->UpdateUI("Atomic");
			}

		LimitTakeBit(pHost);

		DoEnables(pHost);
		}

	if( Tag == "Extent" ) {

		if( m_pValue ) {

			UpdateExtent();
			}

		LimitTakeBit(pHost);

		LimitTreatAs(pHost);

		Recompile(pHost);

		DoEnables(pHost);
		}

	if( Tag == "Access" ) {

		if( pItem == this ) {

			if( m_Access == 2 ) {

				m_Atomic = 0;

				pHost->UpdateUI("Atomic");
				}

			CheckPersist(pHost);

			Recompile(pHost);

			DoEnables(pHost);
			}
		}

	if( Tag == "HasSP" ) {

		if( !m_HasSP ) {

			KillCoded(pHost, L"Setpoint", m_pSetpoint);
			}

		DoEnables(pHost);
		}

	if( Tag == "OnWrite" ) {

		CheckCircular(pHost, m_pOnWrite);

		Recompile(pHost);
		}

	CDataTag::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CTagFlag::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		Type.m_Type  = typeNumeric;

		Type.m_Flags = flagSoftWrite | flagInherent;

		if( m_Extent ) {

			Type.m_Flags |= flagCommsTab;
			}

		return TRUE;
		}

	if( Tag == "Sim" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = flagConstant;

		return TRUE;
		}

	if( Tag == "Setpoint" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "SubValue" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "OnWrite" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	return CDataTag::GetTypeData(Tag, Type);
	}

// Attributes

UINT CTagFlag::GetTreeImage(void) const
{
	return GetImage(IDI_RED_FLAG);
	}

UINT CTagFlag::GetDataType(void) const
{
	return typeLogical;
	}

UINT CTagFlag::GetTypeFlags(void) const
{
	UINT Flags = flagTagRef;

	if( m_Extent > 0 ) {

		if( m_Extent > 1024 ) {

			Flags |= flagExtended;
			}

		Flags |= flagArray;
		}

	if( m_Access < 2 ) {

		Flags |= flagWritable;
		}

	if( m_pOnWrite ) {

		Flags |= flagWritable;
		}

	return Flags;
	}

BOOL CTagFlag::HasSetpoint(void) const
{
	return m_HasSP;
	}

BOOL CTagFlag::NeedSetpoint(void) const
{
	if( m_pEvent1->NeedSetpoint() ) {

		return TRUE;
		}

	if( m_pEvent2->NeedSetpoint() ) {

		return TRUE;
		}

	if( m_pTrigger1->NeedSetpoint() ) {

		return TRUE;
		}

	if( m_pTrigger2->NeedSetpoint() ) {

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CTagFlag::UpdateTypes(BOOL fComp)
{
	if( m_pValue && !m_pValue->IsBroken() ) {

		if( !m_pValue->IsWritable() ) {

			m_Access = 2;
			}

		UpdateExtent();
		}

	UpdateCircular();
	}

void CTagFlag::MakeLite(void)
{
	m_pEvent1->m_Mode = 0;

	m_pEvent2->m_Mode = 0;

	m_pTrigger1->m_Mode = 0;

	m_pTrigger2->m_Mode = 0;
	}

// Reference Check

BOOL CTagFlag::RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag)
{
	if( CodeRefersToTag(Busy, pTags, m_pOnWrite, uTag) ) {

		return TRUE;
		}

	return CDataTag::RefersToTag(Busy, pTags, uTag);
	}

// Circular Check

void CTagFlag::UpdateCircular(void)
{
	m_Circle = CheckCircular(m_pValue  ) ||
		   CheckCircular(m_pOnWrite) ;
	}

// Evaluation

DWORD CTagFlag::GetProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return FindAsText();

		case tpLabel:
			return FindLabel();

		case tpSetPoint:
			return FindSP(Type);

		case tpMinimum:
			return 0;

		case tpMaximum:
			return 1;

		case tpForeColor:
			return FindFore();

		case tpBackColor:
			return FindBack();
		}

	return CDataTag::GetProp(ID, Type);
	}

// Searching

void CTagFlag::FindAlarms(CStringArray &List)
{
	m_pEvent1->FindActive(List);

	m_pEvent2->FindActive(List);
	}

void CTagFlag::FindTriggers(CStringArray &List)
{
	m_pTrigger1->FindActive(List);

	m_pTrigger2->FindActive(List);
	}

// Property Save Filter

BOOL CTagFlag::SaveProp(CString const &Tag) const
{
	if( Tag == "Event1" ) {

		return m_pEvent1->m_Mode > 0;
		}

	if( Tag == "Event2" ) {

		return m_pEvent2->m_Mode > 0;
		}

	if( Tag == "Trigger1" ) {

		return m_pTrigger1->m_Mode > 0;
		}

	if( Tag == "Trigger2" ) {

		return m_pTrigger2->m_Mode > 0;
		}

	return CDataTag::SaveProp(Tag);
	}

// Persistance

void CTagFlag::Load(CTreeFile &File)
{
	AddMeta();

	while( !File.IsEndOfData() ) {

		CString Name = File.GetName();

		if( Name == "TreatAs" ) {

			UINT TreatAs = File.GetValueAsInteger();

			if( m_Extent == 0 && TreatAs == 4 ) {

				m_FlagTreatAs = 3;
				}
			else {
				m_FlagTreatAs = TreatAs;
				}

			continue;
			}

		CMetaData const *pMeta = FindMetaData(Name);

		if( pMeta ) {

			LoadProp(File, pMeta);

			continue;
			}
		}
	}

void CTagFlag::Init(void)
{
	m_pFormat  = New CDispFormatFlag;

	m_pColor   = New CDispColorFlag;

	m_FormType = 5;

	m_ColType  = 2;

	CDataTag::Init();
	}

// Download Support

BOOL CTagFlag::MakeInitData(CInitData &Init)
{
	Init.AddByte(3);

	CDataTag::MakeInitData(Init);

	Init.AddByte(BYTE(m_FlagTreatAs));
	Init.AddByte(BYTE(m_TakeBit));
	Init.AddByte(BYTE(m_Manipulate));
	Init.AddByte(BYTE(m_Atomic));
	Init.AddByte(BYTE(m_HasSP));

	if( m_HasSP ) {

		Init.AddItem(itemVirtual, m_pSetpoint);
		}

	Init.AddItem(itemSimple,  m_pEvent1);
	Init.AddItem(itemSimple,  m_pEvent2);

	Init.AddItem(itemSimple,  m_pTrigger1);
	Init.AddItem(itemSimple,  m_pTrigger2);

	Init.AddItem(itemSimple,  m_pSec);

	Init.AddItem(itemVirtual, m_pOnWrite);

	return TRUE;
	}

// Meta Data

void CTagFlag::AddMetaData(void)
{
	CDataTag::AddMetaData();

	Meta_AddInteger(FlagTreatAs);
	Meta_AddInteger(TakeBit);
	Meta_AddInteger(Manipulate);
	Meta_AddInteger(Atomic);
	Meta_AddInteger(HasSP);
	Meta_AddVirtual(Setpoint);
	Meta_AddObject (Event1);
	Meta_AddObject (Event2);
	Meta_AddObject (Trigger1);
	Meta_AddObject (Trigger2);
	Meta_AddObject (Sec);
	Meta_AddVirtual(OnWrite);

	Meta_SetName((IDS_FLAG_TAG));
	}

// Property Access

DWORD CTagFlag::FindAsText(void)
{
	DWORD   Data = Execute();

	UINT    Type = typeInteger;

	CString Text = L"";

	if( m_pFormat ) {

		Text = m_pFormat->Format(Data, Type, fmtStd);
		}
	else {
		if( Data ) {

			Text = L"ON";
			}
		else
			Text = L"OFF";
		}

	FreeData(Data, Type);

	return DWORD(wstrdup(Text));
	}

DWORD CTagFlag::FindLabel(void)
{

	// LATER -- Refactor back into base class?

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the label that
	// uses the tag property syntax to reference itself!!!

	if( m_pLabel ) {

		return DWORD(wstrdup(m_pLabel->GetText()));
		}

	return DWORD(wstrdup(m_Name));
	}

DWORD CTagFlag::FindSP(UINT Type)
{

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the setpoint that
	// uses the tag property syntax to reference itself!!!

	if( m_pSetpoint ) {

		return m_pSetpoint->Execute(Type);
		}

	return GetNull(Type);
	}

DWORD CTagFlag::FindFore(void)
{
	if( m_pColor ) {

		DWORD Data = Execute();

		UINT  Type = GetDataType();

		return m_pColor->GetForeColor(Data, Type);
		}

	return GetRGB(31,31,31);
	}

DWORD CTagFlag::FindBack(void)
{
	if( m_pColor ) {

		DWORD Data = Execute();

		UINT  Type = GetDataType();

		return m_pColor->GetBackColor(Data, Type);
		}

	return GetRGB(0,0,0);
	}

// Memory Sizing

UINT CTagFlag::GetAllocSize(void)
{
	if( m_Extent ) {

		return (m_Extent + 7) / 8;
		}

	return 1;
	}

UINT CTagFlag::GetCommsSize(void)
{
	if( m_Extent ) {

		if( m_pValue ) {

			if( m_FlagTreatAs >= 3 ) {

				UINT uBits = m_pValue->GetCommsBits();

				UINT uRegs = (m_Extent + uBits - 1) / uBits;

				return uRegs;
				}
			}

		return m_Extent;
		}

	return 0;
	}

// Implementation

void CTagFlag::DoEnables(IUIHost *pHost)
{
	if( m_pValue ) {

		if( m_pValue->IsCommsRef() ) {

			if( m_pValue->GetCommsBits() == 1 ) {

				pHost->EnableUI("FlagTreatAs", FALSE);

				pHost->EnableUI("TakeBit", FALSE);

				pHost->EnableUI("Extent",  m_pValue->IsCommsRefTable());
				}
			else {
				pHost->EnableUI("FlagTreatAs", TRUE);

				pHost->EnableUI("TakeBit", !m_Extent && m_FlagTreatAs >= 3);

				pHost->EnableUI("Extent",  m_pValue->IsCommsRefTable());
				}

			pHost->EnableUI(this, "Access",     m_pValue->IsWritable());

			pHost->EnableUI(this, "RdMode",     m_Extent && m_Access != 1);

			pHost->EnableUI(this, "Persist",    m_Access == 1);

			pHost->EnableUI(this, "Manipulate", TRUE);

			pHost->EnableUI(this, "Atomic",     m_Access <= 1 && m_FlagTreatAs >= 3);
			}
		else {
			if( m_pValue->GetFlags() & flagLogical ) {

				pHost->EnableUI("FlagTreatAs", FALSE);

				pHost->EnableUI("TakeBit", FALSE);
				}
			else {
				pHost->EnableUI("FlagTreatAs", TRUE);

				pHost->EnableUI("TakeBit", !m_Extent && m_FlagTreatAs >= 3);
				}

			BOOL fWrite = m_pValue->IsWritable();

			pHost->EnableUI(this, "Access",     fWrite);

			pHost->EnableUI(this, "RdMode",     FALSE);

			pHost->EnableUI(this, "Manipulate", fWrite);

			pHost->EnableUI(this, "Atomic",     FALSE);

			pHost->EnableUI(this, "Persist",    FALSE);

			pHost->EnableUI(this, "Extent",     FALSE);
			}

		pHost->EnableUI("Sim", !m_pValue->IsConst());
		}
	else {
		pHost->EnableUI(this, "FlagTreatAs",    FALSE);
		pHost->EnableUI(this, "TakeBit",    FALSE);
		pHost->EnableUI(this, "Extent",     TRUE );
		pHost->EnableUI(this, "Access",     FALSE);
		pHost->EnableUI(this, "RdMode",     FALSE);
		pHost->EnableUI(this, "Persist",    TRUE );
		pHost->EnableUI(this, "Manipulate", FALSE);
		pHost->EnableUI(this, "Atomic",     FALSE);
		pHost->EnableUI(this, "Sim",        TRUE );
		}

	pHost->EnableUI("HasSP", NeedSetpoint() ? FALSE : TRUE);

	pHost->EnableUI("Setpoint", m_HasSP);
	}

void CTagFlag::LimitTreatAs(IUIHost *pHost)
{
	UINT uMask = 1;

	if( m_pValue ) {

		if( m_pValue->IsCommsRef() ) {

			switch( m_pValue->GetCommsBits() ) {

				case 1:
					uMask = 0x01;
					break;

				case 8:
					uMask = 0x1A;
					break;

				case 16:
					uMask = 0x1A;
					break;

				case 32:
					uMask = 0x1E;
					break;
				}
			}
		else {
			if( m_pValue->GetFlags() & flagLogical ) {

				uMask = 0x01;
				}
			else
				uMask = 0x1E;
			}
		}

	LimitEnum(pHost, L"FlagTreatAs", m_FlagTreatAs, uMask);
	}

void CTagFlag::LimitTakeBit(IUIHost *pHost)
{
	UINT uMask = 1;

	if( !m_Extent && m_FlagTreatAs >= 3 ) {

		if( m_pValue ) {

			if( m_pValue->IsCommsRef() ) {

				switch( m_pValue->GetCommsBits() ) {

					case 1:
						uMask = 0x00000001;
						break;

					case 8:
						uMask = 0x000000FF;
						break;

					case 16:
						uMask = 0x0000FFFF;
						break;

					case 32:
						uMask = 0xFFFFFFFF;
						break;
					}
				}
			else
				uMask = 0xFFFFFFFF;
			}
		}

	if( !IsBroken() ) {

		BuildList(pHost, L"TakeBit");
		}

	LimitEnum(pHost, L"TakeBit", m_TakeBit, uMask);
	}

void CTagFlag::LimitAccess(IUIHost *pHost)
{
	UINT uMask = 7;

	if( m_pValue ) {

		if( !m_pValue->IsCommsRef() ) {

			uMask = 5;
			}
		}

	LimitEnum(pHost, L"Access", m_Access, uMask);
	}

void CTagFlag::BuildList(IUIHost *pHost, CString Tag)
{
	if( m_pValue ) {

		CLASS        Class  = AfxRuntimeClass(CUITextEnum);

		CUITextEnum * pEnum = (CUITextEnum *) pHost->FindUI(Class, this, Tag);

		if( pEnum ) {

			CStringArray List;

			if( m_pValue->IsCommsRef() ) {

				CCommsManager * pMan = FindSystem()->m_pComms;

				DWORD Addr = pMan->GetRefAddress(m_pValue->m_Refs[0]);

				CAddress Address;

				Address.m_Ref = Addr;

				if( Address.a.m_Type < addrByteAsByte ) {

					UINT uDev  = pMan->GetRefDevice(m_pValue->m_Refs[0]);

					CCommsDevice *pDevice = pMan->FindDevice(uDev);

					CItem * pConfig = pDevice->GetConfig();

					ICommsDriver * pDriver = pDevice->GetDriver();

					for( UINT u = 0; u < 32; u++ ) {

						CString Bit = L"";

						pDriver->ExpandAddress(Bit, pConfig, (CAddress &) Addr);

						if( u == 0 ) {

							if( Bit == pEnum->GetAsText() ) {

								return;
								}
							}

						List.Append(Bit);

						Addr++;
						}
					}
				}

			pEnum->RebindUI(List);

			pHost->UpdateUI();
			}
		}
	}

// End of File
