
#include "Intern.hpp"

#include "SecurityManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UserList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Security Manager
//

// Dynamic Class

AfxImplementDynamicClass(CSecurityManager, CUIItem);

// Constructor

CSecurityManager::CSecurityManager(void)
{
	m_Handle    = HANDLE_NONE;

	m_pUsers    = New CUserList;

	m_DefMapped = allowAnyone | allowProgram;

	m_DefLocal  = allowAnyone | allowProgram;

	m_DefPage   = allowAnyone;

	m_LogMapped = 0;

	m_LogLocal  = 0;

	m_LogEnable = 0;

	m_FileLimit = 60;

	m_FileCount = 24;

	m_WithBatch = 0;

	m_SignLogs  = 0;

	m_Drive     = 0;

	m_UserTime  = 600;

	m_CheckTime = 5;

	m_UserClear = FALSE;
}

// UI Management

CViewWnd * CSecurityManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CSecurityManagerNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	if( uType == viewItem ) {

		CUIPageList *pList = New CUIPageList;

		OnLoadPages(pList);

		CUIViewWnd  *pView = New CUIItemViewWnd(pList);

		pView->SetBorder(6);

		return pView;
	}

	return CUIItem::CreateView(uType);
}

// UI Creation

BOOL CSecurityManager::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		pList->Append(New CUIStdPage(AfxThisClass(), 1));

		return FALSE;
	}

	pList->Append(New CUIStdPage(AfxThisClass(), 2));

	return FALSE;
}

// UI Update

void CSecurityManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "LogEnable" ) {

		DoEnables(pHost);

		return;
	}
}

// Item Naming

CString CSecurityManager::GetHumanName(void) const
{
	return CString(IDS_SECURITY);
}

// Download Support

BOOL CSecurityManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SECURITY);

	CMetaItem::MakeInitData(Init);

	Init.AddLong(m_DefLocal);
	Init.AddLong(m_DefMapped);
	Init.AddLong(m_DefPage);
	Init.AddByte(BYTE(m_LogLocal));
	Init.AddByte(BYTE(m_LogMapped));
	Init.AddByte(BYTE(m_LogEnable));
	Init.AddWord(WORD(m_FileLimit));
	Init.AddWord(WORD(m_FileCount));
	Init.AddByte(BYTE(m_WithBatch));
	Init.AddByte(BYTE(m_SignLogs));
	Init.AddWord(WORD(m_UserTime));
	Init.AddByte(BYTE(m_UserClear));
	Init.AddWord(WORD(m_CheckTime));

	Init.AddItem(itemSimple, m_pUsers);

	Init.AddByte(BYTE(m_Drive));
	
	return TRUE;
}

// Meta Data Creation

void CSecurityManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddCollect(Users);
	Meta_AddInteger(DefMapped);
	Meta_AddInteger(DefLocal);
	Meta_AddInteger(DefPage);
	Meta_AddInteger(LogMapped);
	Meta_AddInteger(LogLocal);
	Meta_AddInteger(LogEnable);
	Meta_AddInteger(FileLimit);
	Meta_AddInteger(FileCount);
	Meta_AddInteger(WithBatch);
	Meta_AddInteger(SignLogs);
	Meta_AddInteger(UserTime);
	Meta_AddInteger(UserClear);
	Meta_AddInteger(CheckTime);
	Meta_AddInteger(Drive);
	
	Meta_SetName((IDS_SECURITY_MANAGER));
}

// Implementation

void CSecurityManager::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("FileCount", m_LogEnable);
	pHost->EnableUI("FileLimit", m_LogEnable);
	pHost->EnableUI("WithBatch", m_LogEnable);
	pHost->EnableUI("SignLogs", m_LogEnable);
	pHost->EnableUI("Drive", m_LogEnable);
}

// End of File
