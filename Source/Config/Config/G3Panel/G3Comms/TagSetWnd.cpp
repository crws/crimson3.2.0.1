
#include "Intern.hpp"

#include "TagSetWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "DataTag.hpp"
#include "NameServer.hpp"
#include "TagFolder.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"
#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Set Window
//

// Runtime Class

AfxImplementRuntimeClass(CTagSetWnd, CCtrlWnd);

// Constructor

CTagSetWnd::CTagSetWnd(CUIElement *pUI)
{
	m_pUI       = pUI;

	m_pTree     = New CTreeView;

	m_dwStyle   = TVS_NOTOOLTIPS
		    | TVS_TRACKSELECT
		    | TVS_FULLROWSELECT
	            | TVS_HASBUTTONS;

	m_uDrop     = 0;

	m_hRoot     = NULL;

	m_hSelect   = NULL;

	m_fLoading  = FALSE;

	m_cfIdent   = RegisterClipboardFormat(L"C3.1 Identifier");

	m_cfTagList = RegisterClipboardFormat(L"C3.1 Tag List");

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TagsTreeIcon16"), afxColor(MAGENTA));

	m_MoveCursor.Create(L"MoveCursor");
	}

// IUnknown

HRESULT CTagSetWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropSource ) {

			*ppObject = (IDropSource *) this;

			return S_OK;
			}

		if( iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CTagSetWnd::AddRef(void)
{
	return 1;
	}

ULONG CTagSetWnd::Release(void)
{
	return 1;
	}

// IDropSource

HRESULT CTagSetWnd::QueryContinueDrag(BOOL fEscape, DWORD dwKeys)
{
	if( !fEscape ) {

		if( dwKeys & MK_RBUTTON ) {

			return DRAGDROP_S_CANCEL;
			}

		if( dwKeys & MK_LBUTTON ) {

			return S_OK;
			}

		return DRAGDROP_S_DROP;
		}

	return DRAGDROP_S_CANCEL;
	}

HRESULT CTagSetWnd::GiveFeedback(DWORD dwEffect)
{
	if( dwEffect == DROPEFFECT_MOVE ) {

		SetCursor(m_MoveCursor);

		return S_OK;
		}

	SetCursor(LoadCursor(NULL, IDC_NO));

	return S_OK;
	}

// IDropTarget

HRESULT CTagSetWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( CanAcceptData(pData, *pEffect) ) {

		m_dwEffect  = *pEffect;

		m_hDropPrev = NULL;

		m_fDropShow = FALSE;

		SetDrop(1);

		DropTrack(CPoint(pt.x, pt.y), FALSE);

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CTagSetWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		DropTrack(CPoint(pt.x, pt.y), FALSE);

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CTagSetWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( m_uDrop ) {

		ShowDrop(FALSE);

		SetDrop (0);

		return S_OK;
		}

	return S_OK;
	}

HRESULT CTagSetWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		ShowDrop(FALSE);

		if( AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			ForceUpdate();

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);

	return S_OK;
	}

// Operations

void CTagSetWnd::Attach(CItem *pItem)
{
	m_pItem   = (CTagSet *) pItem;

	m_pDbase  = m_pItem->GetDatabase();

	m_pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	m_pTags   = m_pSystem->m_pTags->m_pTags;

	m_pName   = m_pSystem->GetNameServer();
	}

// Operations

void CTagSetWnd::LoadTree(CString Data)
{
	m_pTree->DeleteChildren(m_hRoot);

	m_pTree->DeleteItem(m_hRoot);

	m_fLoading = TRUE;

	m_pTree->SetRedraw(FALSE);

	LoadRoot();

	CStringArray List;

	Data.Tokenize(List, ',');

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		CTreeViewItem Node;

		if( GetCodeNode(Node, List[n]) ) {

			m_pTree->InsertItem(m_hRoot, NULL, Node);
			}
		}

	m_pTree->Expand(m_hRoot, TVE_EXPAND);

	m_fLoading = FALSE;

	m_hSelect  = m_hRoot;

	m_Latest   = Data;

	m_pTree->SetRedraw(TRUE);
	}

CString CTagSetWnd::SaveTree(void)
{
	CString Text;

	HTREEITEM hItem = m_pTree->GetChild(m_hRoot);

	while( hItem ) {

		if( !Text.IsEmpty() ) {

			Text += L',';
			}

		Text += GetItemCode(hItem);

		hItem = m_pTree->GetNext(hItem);
		}

	return Text;
	}

// Message Map

AfxMessageMap(CTagSetWnd, CCtrlWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_ENABLE)

	AfxDispatchNotify(100, TVN_KEYDOWN,       OnTreeKeyDown   )
	AfxDispatchNotify(100, TVN_SELCHANGED,    OnTreeSelChanged)
	AfxDispatchNotify(100, TVN_ITEMEXPANDING, OnTreeExpanding )
	AfxDispatchNotify(100, NM_CUSTOMDRAW,     OnTreeCustomDraw)
	AfxDispatchNotify(100, TVN_BEGINDRAG,     OnTreeBeginDrag )

	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CTagSetWnd)
	};

// Message Handlers

void CTagSetWnd::OnPostCreate(void)
{
	m_pTree->Create(m_dwStyle, GetClientRect() - 1, ThisObject, 100);

	m_pTree->SetFont(afxFont(Dialog));

	m_pTree->SetImageList(TVSIL_NORMAL, m_Images);

	m_pTree->SetImageList(TVSIL_STATE,  m_Images);

	m_DropHelp.Bind(m_hWnd, this);

	m_pTree->ShowWindow(SW_SHOW);
	}

void CTagSetWnd::OnSize(UINT uCode, CSize Size)
{
	m_pTree->MoveWindow(GetClientRect() - 1, TRUE);
	}

void CTagSetWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_uDrop ) {

		DC.FrameRect(GetClientRect(), afxBrush(Orange1));

		return;
		}

	if( !IsEnabled() ) {

		DC.FrameRect(GetClientRect(), afxColor(Disabled));

		return;
		}

	DC.FrameRect(GetClientRect(), afxColor(Enabled));
	}

void CTagSetWnd::OnSetFocus(CWnd &Wnd)
{
	m_pTree->SetFocus();
	}

void CTagSetWnd::OnEnable(BOOL fEnable)
{
	LoadTree(m_Latest);

	Invalidate(FALSE);
	}

// Notification Handlers

BOOL CTagSetWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_DELETE:

			DeleteItem();

			return TRUE;
		}

	return FALSE;
	}

void CTagSetWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( !m_fLoading ) {

		if( m_hSelect != Info.itemNew.hItem ) {

			if( Info.action == TVC_BYMOUSE ) {

				SetFocus();
				}

			m_hSelect = Info.itemNew.hItem;
			}
		}
	}

BOOL CTagSetWnd::OnTreeExpanding(UINT uID, NMTREEVIEW &Info)
{
	if( Info.action == TVE_COLLAPSE ) {

		return TRUE;
		}

	return FALSE;
	}

UINT CTagSetWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
		}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMTVCUSTOMDRAW &Extra = (NMTVCUSTOMDRAW &) Info;

		HTREEITEM      hItem = HTREEITEM(Info.dwItemSpec);

		CColor         Color = afxColor(BLACK);

		BOOL           fSet  = FALSE;

		if( IsEnabled() ) {

			if( hItem ) {

				UINT uTest = m_pTree->GetItemState(hItem, NOTHING);

				if( !m_pTree->GetItemParam(hItem) ) {

					Color = afxColor(RED);

					fSet  = TRUE;
					}

				if( uTest & TVIS_DROPHILITED ) {

					Extra.clrTextBk = afxColor(Orange2);

					Extra.clrText   = Color;

					SelectObject(Info.hdc, m_pTree->GetFont());

					return CDRF_NEWFONT;
					}
				}

			if( Info.uItemState & CDIS_HOT ) {

				if( Info.uItemState & CDIS_SELECTED ) {

					Extra.clrTextBk = afxColor(Orange2);
					}
				else
					Extra.clrTextBk = afxColor(Orange3);

				Extra.clrText = Color;

				SelectObject(Info.hdc, m_pTree->GetFont());

				return CDRF_NEWFONT;
				}

			if( fSet ) {

				Extra.clrText = Color;
				}
			}
		else {
			Extra.clrText = afxColor(3dShadow);

			return 0;
			}
		}

	return 0;
	}

void CTagSetWnd::OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info)
{
	SetFocus();

	HTREEITEM hItem = Info.itemNew.hItem;

	m_pTree->SelectItem(hItem);

	afxMainWnd->PostMessage(WM_CANCELMODE);

	DragItem();
	}

// Command Handlers

BOOL CTagSetWnd::OnPasteControl(UINT uID, CCmdSource &Src)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		DWORD dwEffect;

		if( CanAcceptData(pData, dwEffect) ) {

			pData->Release();

			Src.EnableItem(TRUE);

			return TRUE;
			}

		pData->Release();
		}

	return FALSE;
	}

BOOL CTagSetWnd::OnPasteCommand(UINT uID)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		m_hDropPrev = TVI_LAST;

		if( AcceptData(pData) ) {

			pData->Release();

			ForceUpdate();

			return TRUE;
			}

		pData->Release();
		}

	return FALSE;
	}

// Tree Loading

void CTagSetWnd::LoadRoot(void)
{
	CStringArray List;

	m_pUI->GetFormat().Tokenize(List, '|');

	CTreeViewItem Root;

	Root.SetText  (List[1]);

	Root.SetParam (LPARAM(NOTHING));

	Root.SetImages(MapImage(watoi(List[0])));

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Root);
	}

// Drag Support

void CTagSetWnd::DragItem(void)
{
	IDataObject *pData = NULL;

	if( MakeDataObject(pData) ) {

		FindDragMetrics();

		MakeDragImage();

		CDragHelper *pHelp = New CDragHelper;

		pHelp->AddImage(pData, m_DragImage, m_DragSize, m_DragOffset);

		delete pHelp;

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, 0);

		m_pTree->UpdateWindow();

		DWORD dwAllow  = DROPEFFECT_MOVE;

		DWORD dwResult = DROPEFFECT_NONE;

		DoDragDrop(pData, this, dwAllow, &dwResult);

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, m_dwStyle);

		pData->Release();
		}
	}

// Drag Hooks

void CTagSetWnd::FindDragMetrics(void)
{
	CRect Rect = m_pTree->GetItemRect(m_hSelect, TRUE);

	m_pTree->ClientToScreen(Rect);

	Rect.left    = Rect.left - 16;

	m_DragPos    = GetCursorPos();

	m_DragSize   = Rect.GetSize();

	m_DragOffset = m_DragPos - Rect.GetTopLeft();

	MakeMin(m_DragOffset.cx, Rect.cx());

	MakeMin(m_DragOffset.cy, Rect.cy());

	MakeMax(m_DragOffset.cx, 0);

	MakeMax(m_DragOffset.cy, 0);
	}

void CTagSetWnd::MakeDragImage(void)
{
	CRect Rect = m_DragSize;

	m_DragImage.Create(CClientDC(NULL), m_DragSize);

	CMemoryDC DC;

	DC.Select(m_DragImage);

	DC.FillRect(Rect, afxBrush(MAGENTA));

	// NOTE -- Don't use black or Vista gets confused!

	DC.SetTextColor(RGB(1,1,1));

	DC.Select(afxFont(DialogNA));

	DC.SetBkMode(TRANSPARENT);

	CString Text   = m_pTree->GetItemText (m_hSelect);

	UINT    uImage = m_pTree->GetItemImage(m_hSelect);

	CSize   Size   = DC.GetTextExtent(Text);

	CPoint  Origin = CPoint(16, 0);

	CSize   Adjust = Rect.GetSize() - Size - CSize(16, 0);

	DC.TextOut(Origin + Adjust / 2, Text);

	m_Images.Draw(uImage, DC, 0, 0, ILD_NORMAL);

	DC.Deselect();

	DC.Deselect();
	}

// Data Object Construction

BOOL CTagSetWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddIdentifier(pMake);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CTagSetWnd::AddIdentifier(CDataObject *pData)
{
	if( m_hSelect ) {

		CString F1   = m_pItem->GetDatabase()->GetUniqueSig();

		CString F2   = L"CTagSet";

		CString F3   = GetItemCode(m_hSelect);

		CString F4   = CPrintf(L"%8.8X", m_pItem);

		CString F5   = CPrintf(L"%8.8X", PDWORD(m_pItem)[0]);

		CString Head = CPrintf(L"%s|%s|%s|%s|%s", F1, F2, F3, F4, F5);

		pData->AddText(m_cfIdent, Head);

		return TRUE;
		}

	return FALSE;
	}

// Data Object Acceptance

BOOL CTagSetWnd::CanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( CanAcceptTagList(pData) || CanAcceptIdent(pData) ) {

		dwEffect = DROPEFFECT_MOVE | DROPEFFECT_LINK;

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagSetWnd::AcceptData(IDataObject *pData)
{
	CStringArray List;

	if( AcceptTagList(pData, List) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CTreeViewItem Node;

			if( GetCodeNode(Node, List[n]) ) {

				if( m_hDropPrev ) {

					HTREEITEM hItem = m_pTree->InsertItem(m_hRoot, m_hDropPrev, Node);

					m_pTree->SelectItem(hItem);
					}
				else {
					HTREEITEM hItem = m_pTree->InsertItem(m_hRoot, TVI_FIRST,   Node);

					m_pTree->SelectItem(hItem);
					}

				m_hDropPrev = m_hSelect;
				}
			}

		return TRUE;
		}

	if( AcceptIdent(pData, List) ) {

		CTreeViewItem Node;

		BOOL fLoad = FALSE;

		BOOL fMove = FALSE;

		if( List[1] == L"CDataTag" ) {

			if( !HasCode(List[2]) ) {

				if( GetCodeNode(Node, List[2]) ) {

					fLoad = TRUE;
					}
				}
			}

		if( List[1] == L"CTagSet" ) {

			GetCodeNode(Node, List[2]);

			fLoad = TRUE;

			fMove = TRUE;
			}

		if( fLoad ) {

			if( fMove ) {

				m_pTree->SetRedraw(FALSE);

				HTREEITEM hPrev = m_hSelect;

				m_pTree->DeleteItem(hPrev);
				}

			if( m_hDropPrev ) {

				HTREEITEM hItem = m_pTree->InsertItem(m_hRoot, m_hDropPrev, Node);

				m_pTree->SelectItem(hItem);
				}
			else {
				HTREEITEM hItem = m_pTree->InsertItem(m_hRoot, TVI_FIRST,   Node);

				m_pTree->SelectItem(hItem);
				}

			if( fMove ) {

				m_pTree->SetRedraw(TRUE);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Data Object Helpers

BOOL CTagSetWnd::CanAcceptIdent(IDataObject *pData)
{
	CStringArray List;

	if( AcceptIdent(pData, List) ) {

		if( List[0] == m_pDbase->GetUniqueSig() ) {

			CItem * pItem = (CItem *) wcstol(List[3], NULL, 16);

			DWORD   Check =           wcstol(List[4], NULL, 16);

			// NOTE -- This is a nasty test to make sure that the pointer
			// is still pointer where it ought to. We really ought to change
			// this clipboard format to something more sensible!

			if( !IsBadReadPtr(pItem, 4) ) {

				if( PDWORD(pItem)[0] == Check ) {

					if( List[1] == L"CTagSet" ) {

						if( pItem == m_pItem ) {

							return TRUE;
							}
						}

					if( List[1] == L"CDataTag" ) {

						if( pItem->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

							return FALSE;
							}

						if( HasCode(List[2]) ) {

							return FALSE;
							}

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CTagSetWnd::AcceptIdent(IDataObject *pData, CStringArray &List)
{
	FORMATETC Fmt = { WORD(m_cfIdent), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		Text.Tokenize(List, L'|');

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagSetWnd::CanAcceptTagList(IDataObject *pData)
{
	CStringArray List;

	if( AcceptTagList(pData, List) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			if( HasCode(List[n]) ) {

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagSetWnd::AcceptTagList(IDataObject *pData, CStringArray &List)
{
	FORMATETC Fmt = { WORD(m_cfTagList), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		Text.Tokenize(List, L'|');

		return TRUE;
		}

	return FALSE;
	}

// Drop Support

void CTagSetWnd::DropTrack(CPoint Pos, BOOL fMove)
{
	m_pTree->ScreenToClient(Pos);

	if( m_pTree->GetClientRect().PtInRect(Pos) ) {

		HTREEITEM hItem = m_pTree->HitTestItem(Pos);

		HTREEITEM hPrev = NULL;

		BOOL      fShow = FALSE;

		if( m_uDrop ) {

			if( hItem == m_hRoot ) {

				hPrev = NULL;
				}

			if( hItem ) {

				hPrev = m_pTree->GetPrevious(hItem);
				}
			else {
				hItem = m_pTree->GetChild(m_hRoot);

				while( hItem ) {

					hPrev = hItem;

					hItem = m_pTree->GetNext(hItem);
					}
				}

			fShow = TRUE;
			}
		else {
			hPrev = NULL;

			fShow = FALSE;
			}

		if( m_hDropPrev != hPrev || m_fDropShow != fShow ) {

			ShowDrop(FALSE);

			m_hDropPrev = hPrev;

			m_fDropShow = fShow;

			ShowDrop(TRUE);
			}
		}
	}

void CTagSetWnd::ShowDrop(BOOL fShow)
{
	if( m_fDropShow ) {

		CRect Horz, Vert;

		HTREEITEM hItem;

		if( !m_hDropPrev ) {

			hItem = m_pTree->GetChild(m_hRoot);
			}
		else
			hItem = m_pTree->GetNext(m_hDropPrev);

		if( !hItem ) {

			HTREEITEM hDraw = m_hDropPrev ? m_hDropPrev : m_hRoot;

			CRect     Root  = m_pTree->GetItemRect(hDraw, TRUE);

			WalkToLast(hDraw);

			Horz = m_pTree->GetItemRect(hDraw, TRUE);

			Horz.top    = Horz.bottom -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Root.left   - 20;

			Horz.right  = Root.right  +  8;
			}
		else {
			Horz = m_pTree->GetItemRect(hItem, TRUE);

			Horz.top    = Horz.top    -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Horz.left   - 20;

			Horz.right  = Horz.right  +  8;
			}

		CClientDC DC(*m_pTree);

		CBrush Brush(afxColor(WHITE), afxColor(BLACK), 128);

		DC.Select(TRUE ? afxBrush(WHITE) : Brush);

		DC.PatBlt(Horz, PATINVERT);

		DC.PatBlt(Vert, PATINVERT);

		DC.Deselect();
		}
	}

// Implementation

BOOL CTagSetWnd::HasCode(CString Code)
{
	HTREEITEM hItem = m_pTree->GetChild(m_hRoot);

	while( hItem ) {

		if( GetItemCode(hItem) == Code ) {

			return TRUE;
			}

		hItem = m_pTree->GetNext(hItem);
		}

	return FALSE;
	}

CString CTagSetWnd::GetItemCode(HTREEITEM hItem)
{
	DWORD   Data = m_pTree->GetItemParam(hItem);

	CString Code = m_pTree->GetItemText (hItem);

	if( Data ) {

		CDataRef &Ref = (CDataRef &) Data;

		if( !Ref.t.m_IsTag ) {

			if( m_pName->NameDirect(Data, Code) ) {

				Code = L'[' + Code;

				Code = Code + L']';

				return Code;
				}
			}
		else {
			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				return Code;
				}
			}
		}

	return L"WAS " + Code;
	}

BOOL CTagSetWnd::GetCodeNode(CTreeViewItem &Node, CString Code)
{
	if( Code.StartsWith(L"WAS ") ) {

		Code = Code.Mid(4);
		}
	else {
		CError   Error(FALSE);

		if( Code[0] == '[' ) {

			CString      Addr  = Code.Mid(1, Code.GetLength() - 2);

			CError       Error = CError(FALSE);

			DWORD        ID    = 0;

			CTypeDef     Type  = { 0, 0 };

			if( m_pName->FindDirect(&Error, Addr, ID, Type) ) {

				Node.SetText  (Addr);

				Node.SetParam (LPARAM(ID));

				Node.SetImages(MapImage(IDI_PLC_ENABLED));

				return TRUE;
				}
			}
		else {
			DWORD      ID = 0;

			CDataRef &Ref = (CDataRef &) ID;

			if( !wstrncmp(Code, L"__T", 3) ) {

				UINT uTag = wcstoul(PCTXT(Code) + 3, NULL, 16);

				Ref.t.m_IsTag = 1;

				Ref.t.m_Index = uTag;

				////////

				AfxAssert(FALSE);

				return TRUE;
				}

			UINT uPos   = Code.Find('[');

			UINT uTag   = 0;

			BOOL fArray = FALSE;

			if( uPos < NOTHING ) {

				Ref.x.m_Array = watoi(Code.Mid(uPos+1));

				fArray        = TRUE;

				Code          = Code.Left(uPos);
				}

			CString Name = Code;

			if( (uTag = m_pTags->FindNamePos(Code)) < NOTHING ) {

				CTag *pTag = m_pTags->GetItem(uTag);

				if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					CDataTag *pData = (CDataTag *) pTag;

					if( !pData->m_Extent == fArray ) {

						return FALSE;
						}

					if( pData->m_Extent ) {

						Name += CPrintf(L"[%d]", Ref.t.m_Array);
						}

					Ref.t.m_IsTag = 1;

					Ref.t.m_Index = uTag;

					////////

					Node.SetText  (Name);

					Node.SetParam (LPARAM(ID));

					Node.SetImages(MapImage(pTag->GetTreeImage()));

					return TRUE;
					}
				}
			}
		}

	Node.SetText  (L"WAS " + Code);

	Node.SetParam (NULL);

	Node.SetImages(IDI_NOTHING);

	return TRUE;
	}

BOOL CTagSetWnd::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		UpdateWindow();

		return TRUE;
		}

	return FALSE;
	}

void CTagSetWnd::ForceUpdate(void)
{
	SendNotify(IDM_UI_CHANGE);
	}

void CTagSetWnd::WalkToLast(HTREEITEM &hItem)
{
	HTREEITEM hNext;

	while( (hNext = m_pTree->GetNext (hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;
		}

	while( (hNext = m_pTree->GetChild(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;

		WalkToLast(hItem);

		break;
		}
	}

void CTagSetWnd::DeleteItem(void)
{
	if( m_hSelect && m_hSelect != m_hRoot ) {

		HTREEITEM hPrev = m_hSelect;

		m_pTree->MoveSelection();

		m_pTree->DeleteItem(hPrev);

		ForceUpdate();
		}
	}

UINT CTagSetWnd::MapImage(UINT uImage)
{
	if( !IsEnabled() ) {

		switch( uImage ) {

			case IDI_RED_FLAG:
			case IDI_GREEN_FLAG:
			case IDI_YELLOW_FLAG:
			case IDI_PURPLE_FLAG:
			case IDI_BLUE_FLAG:

				return IDI_GRAY_FLAG;

			case IDI_RED_INTEGER:
			case IDI_GREEN_INTEGER:
			case IDI_YELLOW_INTEGER:
			case IDI_PURPLE_INTEGER:
			case IDI_BLUE_INTEGER:

				return IDI_GRAY_INTEGER;

			case IDI_RED_FLOAT:
			case IDI_GREEN_FLOAT:
			case IDI_YELLOW_FLOAT:
			case IDI_PURPLE_FLOAT:
			case IDI_BLUE_FLOAT:

				return IDI_GRAY_FLOAT;

			case IDI_RED_STRING:
			case IDI_GREEN_STRING:
			case IDI_YELLOW_STRING:
			case IDI_PURPLE_STRING:
			case IDI_BLUE_STRING:

				return IDI_GRAY_STRING;
			}

		return IDI_GRAY_FOLDER;
		}

	return uImage;
	}

// End of File
