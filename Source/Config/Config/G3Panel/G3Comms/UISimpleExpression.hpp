
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UISimpleExpression_HPP

#define INCLUDE_UISimpleExpression_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UIExpression.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Expression
//

class CUISimpleExpression : public CUIExpression
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUISimpleExpression(void);

		// Category Overridables
		BOOL    LoadModeButton(void);
	};

// End of File

#endif
