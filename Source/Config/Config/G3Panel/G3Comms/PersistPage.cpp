
#include "Intern.hpp"

#include "PersistPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Persistent Data Page
//

// Runtime Class

AfxImplementRuntimeClass(CPersistPage, CMetaItem);

// Constructor

CPersistPage::CPersistPage(UINT uPage)
{
	m_uPage  = uPage;

	m_uSize  = 64000;

	m_uBytes = m_uSize / 8;

	m_pMap   = new BYTE [ m_uBytes ];

	memset(m_pMap, 0, m_uBytes);
	}

// Destructor

CPersistPage::~CPersistPage(void)
{
	delete [] m_pMap;
	}

// Operations

void CPersistPage::Register(DWORD dwAddr, UINT uSize)
{
	if( HIWORD(dwAddr) == m_uPage ) {

		dwAddr = LOWORD(dwAddr);

		if( uSize == 1 ) {

			m_pMap[dwAddr / 8] |= (1 << (dwAddr % 8));

			return;
			}

		if( uSize == 4 ) {

			m_pMap[dwAddr / 8] |= ((dwAddr & 4) ? 0xF0 : 0x0F);

			return;
			}

		GeneralRegister(dwAddr, uSize);

		return;
		}

	AfxAssert(FALSE);
	}

DWORD CPersistPage::Allocate(UINT uSize)
{
	if( uSize == 1 ) {

		for( UINT n = 0; n < m_uBytes; n++ ) {

			if( m_pMap[n] == 0xFF ) {

				continue;
				}

			for( UINT b = 0; b < 8; b++ ) {

				if( m_pMap[n] & (1 << b) ) {

					continue;
					}

				m_pMap[n] |= (1 << b);

				return MAKELONG(8 * n + b, m_uPage);
				}
			}

		return 0;
		}

	if( uSize == 4 ) {

		for( UINT n = 0; n < m_uBytes; n++ ) {

			if( (m_pMap[n] & 0x0F) && (m_pMap[n] & 0xF0) ) {

				continue;
				}

			if( m_pMap[n] & 0x0F ) {

				m_pMap[n] |= 0xF0;

				return MAKELONG(8 * n + 4, m_uPage);
				}
			else {
				m_pMap[n] |= 0x0F;

				return MAKELONG(8 * n + 0, m_uPage);
				}
			}

		return 0;
		}

	return GeneralAllocate(uSize);;
	}

void CPersistPage::Free(DWORD dwAddr, UINT uSize)
{
	if( HIWORD(dwAddr) == m_uPage ) {

		dwAddr = LOWORD(dwAddr);

		if( uSize == 1 ) {

			m_pMap[dwAddr / 8] &= ~(1 << (dwAddr % 8));

			return;
			}

		if( uSize == 4 ) {

			m_pMap[dwAddr / 8] &= ~((dwAddr & 4) ? 0xF0 : 0x0F);

			return;
			}

		GeneralFree(dwAddr, uSize);

		return;
		}

	AfxAssert(FALSE);
	}

void CPersistPage::Free(void)
{
	memset(m_pMap, 0, m_uBytes);
	}

// Implementation

void CPersistPage::GeneralRegister(DWORD dwAddr, UINT uSize)
{
	if( IsBadReadPtr(this, sizeof(*this)) ) {

		throw (int *) 0;
		}

	UINT uReg  = dwAddr / 8;

	UINT uBit  = dwAddr % 8;

	BYTE bMask = BYTE(1 << uBit);

	while( uSize-- ) {

		m_pMap[uReg] |= bMask;

		if( !(bMask <<= 1) ) {

			bMask = 1;

			uReg += 1;
			}
		}
	}

DWORD CPersistPage::GeneralAllocate(UINT uSize)
{
	UINT c = (uSize + 7) / 8;

	if( c > m_uBytes ) {

		return 0;
		}

	for( UINT n = 0; n <= m_uBytes - c; n++ ) {

		UINT o;

		for( o = 0; o < c; o++ ) {

			if( m_pMap[n + o] ) {

				break;
				}
			}

		if( o == c ) {

			UINT a = uSize % 8;

			if( !a ) {

				memset(m_pMap + n, 0xFF, c);
				}
			else {
				memset(m_pMap + n, 0xFF, c - 1);

				for( UINT b = 0; b < a; b++ ) {

					m_pMap[n + c - 1] |= (1 << b);
					}
				}

			return MAKELONG(8 * n, m_uPage);
			}
		}

	return 0;
	}

void CPersistPage::GeneralFree(DWORD dwAddr, UINT uSize)
{
	UINT uReg  = dwAddr / 8;

	UINT uBit  = dwAddr % 8;

	BYTE bMask = BYTE(1 << uBit);

	while( uSize-- ) {

		m_pMap[uReg] &= ~bMask;

		if( !(bMask <<= 1) ) {

			bMask = 1;

			uReg += 1;
			}
		}
	}

// End of File
