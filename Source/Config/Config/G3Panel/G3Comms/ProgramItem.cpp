
#include "Intern.hpp"

#include "ProgramItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "ProgramCodeItem.hpp"
#include "ProgramItemView.hpp"
#include "ProgramList.hpp"
#include "ProgramPrototype.hpp"
#include "ProgramManager.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Program Item
//

// Dynamic Class

AfxImplementDynamicClass(CProgramItem, CCodedHost);

// Constructor

CProgramItem::CProgramItem(void)
{
	m_Handle     = HANDLE_NONE;
	m_pCode      = NULL;
	m_Pending    = 0;
	m_pProt      = New CProgramPrototype;
	m_BkGnd      = FALSE;
	m_Comms      = 0;
	m_Timeout    = 300;
	m_Run        = 0;
	m_Private    = 0;
	m_DebugMode  = 0;
	m_DebugFlags = 15;
	m_Time       = 0;
	}

// UI Creation

CViewWnd * CProgramItem::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return New CProgramItemView;
		}

	return CCodedHost::CreateView(uType);
	}

// UI Update

void CProgramItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"Comms" ) {

		DoEnables(pHost);
		}

	if( Tag == L"Code" ) {

		if( pHost->InReplay() ) {

			Translate(CError(FALSE), TRUE);
			}
		}

	if( Tag.StartsWith(L"Debug") ) {

		DoEnables(pHost);

		Translate(CError(FALSE), FALSE);
		}

	if( Tag == L"Prot" ) {

		m_pProt->UpdateParamList();

		if( m_pCode ) {

			UpdateCodeType();

			Translate(CError(FALSE), TRUE);

			pHost->UpdateUI("Code");

			FindSystem()->ObjCheck(m_Handle, FALSE);

			pHost->SendUpdate(updateProps);
			}

		DoEnables(pHost);
		}

	if( Tag == L"BkGnd" ) {

		pHost->SendUpdate(updateProps);
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Item Naming

CString CProgramItem::GetHumanName(void) const
{
	return m_Name;
	}

// Persistance

void CProgramItem::Load(CTreeFile &Tree)
{
	CCodedHost::Load(Tree);

	if( Tree.HasFile() ) {

		if( StoreInFile() ) {

			if( LoadCode(Tree.GetFile()) ) {

				m_Prev = m_Name;
				}
			}
		}
	}

void CProgramItem::Save(CTreeFile &Tree)
{
	CCodedHost::Save(Tree);

	if( Tree.HasFile() ) {

		if( StoreInFile() ) {

			SaveCode(Tree.GetFile());
			}
		}
	}

void CProgramItem::PostLoad(void)
{
	CCodedHost::PostLoad();

	if( m_Comms == 3 ) {

		m_Comms = 0;

		m_Run   = 1;
		}
	}

// Item Location

CCommsSystem * CProgramItem::FindSystem(void) const
{
	return (CCommsSystem *) GetDatabase()->GetSystemItem();
	}

// Parameter Access

UINT CProgramItem::GetParameterCount(void)
{
	return m_pProt->GetParameterCount();
	}

CFuncParam * CProgramItem::GetParameterList(void)
{
	return m_pProt->GetParameterList();
	}

// Attributes

UINT CProgramItem::GetTreeImage(void) const
{
	UINT uBase = 2;

	UINT uStep = (m_pProt->m_Type % 4);

	if( IsBroken() ) {

		uStep += 8;
		}
	else {
		if( m_Pending ) {

			uStep += 4;
			}

		else if( m_BkGnd ) {

			uStep += 12;
			}
		}

	return uBase + uStep;
	}

BOOL CProgramItem::IsPending(void) const
{
	return m_Pending;
	}

BOOL CProgramItem::IsBroken(void) const
{
	if( m_pCode ) {

		return m_pCode->HasError();
		}

	return FALSE;
	}

UINT CProgramItem::GetDebugFlags(void) const
{
	CCommsSystem    *pSystem  = FindSystem();

	CProgramManager *pManager = pSystem->m_pPrograms;

	if( pManager->m_DebugEnable ) {

		if( m_DebugMode ) {

			if( m_DebugMode == 2 ) {

				return m_DebugFlags;
				}
			}
		else {
			if( pManager->m_DebugPrograms ) {

				return pManager->m_DebugFlags;
				}
			}
		}

	return 0;
	}

BOOL CProgramItem::StoreInFile(void) const
{
	CCommsSystem    *pSystem  = FindSystem();

	CProgramManager *pManager = pSystem->m_pPrograms;

	return pManager->m_StoreInFiles ? TRUE : FALSE;
	}

// Operations

BOOL CProgramItem::Create(CString Prot, CString Code)
{
	m_pProt->Parse(Prot);

	m_pProt->UpdateParamList();

	LoadCodeFromString(Code);

	Translate(CError(FALSE), TRUE);

	return TRUE;
	}

BOOL CProgramItem::Translate(CError &Error, BOOL fSave)
{
	if( m_pCode ) {

		CString Code = m_pCode->GetAsText();

		UINT    uAdd = 0;

		if( Error.AllowUI() || StoreInFile() ) {

			if( Code.StartsWith(L"WAS ") ) {

				Code = Code.Mid(4);

				uAdd = 4;
				}
			}

		if( !m_pCode->Compile(Error, Code) ) {

			if( Error.AllowUI() ) {

				CRange Range     = Error.GetRange();

				m_pCode->m_Error = TRUE;

				m_pCode->m_From  = uAdd + Range.m_nFrom;

				m_pCode->m_To    = uAdd + Range.m_nTo;

				m_Pending        = TRUE;
				}
			else {
				Code = L"WAS " + Code;

				Error.Clear();

				m_pCode->Compile(Error, Code);

				m_pCode->ClearCache();

				m_pCode->GetDatabase()->SetRecomp();

				m_Pending = FALSE;

				if( fSave ) {

					CheckSaveCode();
					}
				}

			UpdatePending();

			SetDirty();

			return FALSE;
			}

		if( Error.AllowUI() ) {

			TCHAR sBuffer[MAX_PATH];

			GetWindowsDirectory(sBuffer, sizeof(sBuffer));

			PlaySound(CPrintf(L"%s\\Media\\chimes.wav", sBuffer), NULL, SND_FILENAME | SND_ASYNC);
			}

		if( fSave ) {

			CheckSaveCode();
			}

		m_pCode->ClearCache();

		m_Pending = FALSE;

		UpdatePending();

		SetDirty();

		return TRUE;
		}

	return TRUE;
	}

void CProgramItem::SetPending(void)
{
	if( m_pCode ) {

		m_pCode->m_Error = FALSE;
		}

	m_Pending = TRUE;

	UpdatePending();

	SetDirty();
	}

BOOL CProgramItem::CheckSaveCode(void)
{
	CCommsSystem    *pSystem  = FindSystem();

	CProgramManager *pManager = pSystem->m_pPrograms;

	if( pManager->m_StoreInFiles ) {

		m_Time = UINT64(-1);

		return SaveCode();
		}

	return FALSE;
	}

BOOL CProgramItem::CheckKillCode(void)
{
	CCommsSystem    *pSystem  = FindSystem();

	CProgramManager *pManager = pSystem->m_pPrograms;

	if( pManager->m_StoreInFiles ) {

		return KillCode();
		}

	return FALSE;
	}

BOOL CProgramItem::CheckKillPrev(void)
{
	CCommsSystem    *pSystem  = FindSystem();

	CProgramManager *pManager = pSystem->m_pPrograms;

	if( pManager->m_StoreInFiles ) {

		return KillPrev();
		}

	return FALSE;
	}

BOOL CProgramItem::CheckCodeChange(void)
{
	if( StoreInFile() ) {

		if( TestCode() ) {

			FindSystem()->ObjCheck(m_Handle, FALSE);

			return TRUE;
			}
		}

	return FALSE;
	}

// Type Access

BOOL CProgramItem::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Code" ) {

		Type.m_Type  = m_pProt->m_Type;

		Type.m_Flags = Type.m_Type ? 0 : flagActive;

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CProgramItem::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_PROGRAM);

	CMetaItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pCode);

	Init.AddText(m_Name);

	Init.AddByte(BYTE(m_BkGnd));

	Init.AddByte(BYTE(m_Comms));

	Init.AddWord(WORD(m_Timeout));

	Init.AddByte(BYTE(m_Run));

	return TRUE;
	}

// Meta Data

void CProgramItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddString (Name);
	Meta_AddInteger(Pending);
	Meta_AddVirtual(Code);
	Meta_AddObject (Prot);
	Meta_AddInteger(BkGnd);
	Meta_AddInteger(Comms);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(Run);
	Meta_AddInteger(Private);
	Meta_AddInteger(DebugMode);
	Meta_AddInteger(DebugFlags);

	Meta_SetName((IDS_PROGRAM));
	}

// External Filename

CFilename CProgramItem::GetFilename(CFilename Base)
{
	CString Prog = m_Name;

	CString Name = Base.GetBareName();

	CString Path = Base.GetDirectory() + Name + L"\\";

	CreateDirectory(Path, NULL);

	Path +=  + L"Programs\\";

	CreateDirectory(Path, NULL);

	Prog.Replace('.', '-');

	return Path + Prog + L".c";
	}

// Implementation

void CProgramItem::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "BkGnd",      m_pProt->m_Type == typeVoid);

	pHost->EnableUI(this, "Timeout",    m_Comms == 2);

	pHost->EnableUI(this, "DebugFlags", m_DebugMode == 2);
	}

void CProgramItem::UpdateCodeType(void)
{
	CTypeDef Type;

	GetTypeData(L"Code", Type);

	m_pCode->SetReqType(Type);
	}

void CProgramItem::UpdatePending(void)
{
	((CProgramList *) GetParent())->UpdatePending();
	}

BOOL CProgramItem::TestCode(void)
{
	return TestCode(m_pDbase->GetFilename());
	}

BOOL CProgramItem::TestCode(CFilename Base)
{
	if( !Base.IsEmpty() ) {

		CFilename Name  = GetFilename(Base);

		HANDLE    hFile = Name.OpenReadSeq();

		if( hFile != INVALID_HANDLE_VALUE ) {

			if( GetFileTime(hFile) > m_Time ) {

				if( LoadCode(hFile) ) {

					Translate(CError(FALSE), FALSE);

					return TRUE;
					}
				}

			CloseHandle(hFile);
			}
		}

	return FALSE;
	}

BOOL CProgramItem::LoadCode(void)
{
	return LoadCode(m_pDbase->GetFilename());
	}

BOOL CProgramItem::LoadCode(CFilename Base)
{
	if( !Base.IsEmpty() ) {

		CFilename Name  = GetFilename(Base);

		HANDLE    hFile = Name.OpenReadSeq();

		if( hFile != INVALID_HANDLE_VALUE ) {

			return LoadCode(hFile);
			}
		}

	return FALSE;
	}

BOOL CProgramItem::LoadCode(HANDLE hFile)
{
	UINT  uData = GetFileSize(hFile, NULL);

	PBYTE pData = new BYTE [ uData+1 ];

	DWORD uDone = 0;

	ReadFile(hFile, pData, uData, &uDone, NULL);

	pData[uData ] = 0;

	UINT64 Time = GetFileTime(hFile);

	m_Time      = 0;

	CloseHandle(hFile);

	CString Code;

	Code = PCSTR(pData);

	delete [] pData;

	Code.Replace(L"\r\n", L"\r");

	UINT uState = 0;

	while( !Code.IsEmpty() ) {

		CString Text = Code.StripToken('\r');

		Text.TrimBoth();

		if( !Text.IsEmpty() ) {

			if( uState == 0 ) {

				if( m_pProt->Parse(Text) ) {

					m_pProt->UpdateParamList();

					uState = 1;

					continue;
					}

				return FALSE;
				}

			if( uState == 1 ) {

				if( Text == L"{" ) {

					Code = Code.Left(Code.FindRev('}'));

					break;
					}

				return FALSE;
				}
			}
		}

	if( LoadCodeFromString(Code) ) {

		m_Time = Time;

		return TRUE;
		}

	return FALSE;
	}

BOOL CProgramItem::LoadCodeFromString(CString Code)
{
	if( Code.IsEmpty() ) {

		if( m_pCode ) {

			m_pCode->Kill();

			delete m_pCode;

			m_pCode = NULL;
			}
		}
	else {
		if( !m_pCode ) {

			m_pCode = New CProgramCodeItem;

			m_pCode->SetParent(this);

			m_pCode->Init();
			}

		CTypeDef Type;

		Type.m_Type  = m_pProt->m_Type;

		Type.m_Flags = Type.m_Type ? 0 : flagActive;

		m_pCode->SetReqType(Type);

		m_pCode->SetAsText(Code);
		}

	return TRUE;
	}

BOOL CProgramItem::SaveCode(void)
{
	return SaveCode(m_pDbase->GetFilename());
	}

BOOL CProgramItem::SaveCode(CFilename Base)
{
	if( !Base.IsEmpty() ) {

		CFilename Name  = GetFilename(Base);

		HANDLE    hFile;

		if( (hFile = Name.OpenRead()) != INVALID_HANDLE_VALUE ) {

			if( m_Time <= GetFileTime(hFile) ) {

				CloseHandle(hFile);

				return FALSE;
				}

			CloseHandle(hFile);
			}

		if( (hFile = Name.OpenWrite()) != INVALID_HANDLE_VALUE ) {

			CString Code = m_pCode ? m_pCode->GetAsText() : L"";

			CString Prot = m_pProt->Format();

			CString Text = Prot;

			Text += L"\r{\r";

			Text += Code;

			if( !Text.EndsWith(L"\r") ) {

				Text += L"\r";
				}

			Text += L"}\r";
				
			Text.Replace(L"\r", L"\r\n");

			UINT  uData = Text.GetLength();

			PBYTE pData = new BYTE [ uData ];

			DWORD uDone = 0;

			for( UINT n = 0; n < uData; n++ ) {

				pData[n] = BYTE(PCTXT(Text)[n]);
				}

			WriteFile(hFile, pData, uData, &uDone, NULL);

			delete [] pData;

			m_Time = GetFileTime(hFile);

			CloseHandle(hFile);

			m_Prev = m_Name;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CProgramItem::KillCode(void)
{
	return KillCode(m_pDbase->GetFilename());
	}

BOOL CProgramItem::KillCode(CFilename Base)
{
	if( !Base.IsEmpty() ) {

		CFilename Name = GetFilename(Base);

		DeleteFile(Name);

		return TRUE;
		}

	return FALSE;
	}

BOOL CProgramItem::KillPrev(void)
{
	return KillPrev(m_pDbase->GetFilename());
	}

BOOL CProgramItem::KillPrev(CFilename Base)
{
	if( !Base.IsEmpty() ) {

		if( !m_Prev.IsEmpty() ) {

			CFilename Name = Base.WithName(m_Prev).WithType(L"c");

			DeleteFile(Name);

			return TRUE;
			}
		}

	return FALSE;
	}

UINT64 CProgramItem::GetFileTime(HANDLE hFile)
{
	FILETIME t;

	::GetFileTime(hFile, NULL, NULL, &t);

	return (UINT64(t.dwHighDateTime) << 32) | UINT64(t.dwLowDateTime);
	}

// End of File
