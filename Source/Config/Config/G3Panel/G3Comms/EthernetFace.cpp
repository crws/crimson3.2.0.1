
#include "Intern.hpp"

#include "EthernetFace.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "EthernetItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Port Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CEthernetFace, CCodedHost);

// Constructor

CEthernetFace::CEthernetFace(UINT uPort)
{
	m_uPort       = uPort;

	m_pMode	      = NULL;

	m_pEnableFull = NULL;

	m_pEnable100  = NULL;

	m_pAddress    = NULL;

	m_pNetMask    = NULL;

	m_pGateway    = NULL;

	m_pMetric     = NULL;

	m_pDNSMode    = NULL;

	m_pDNS1       = NULL;

	m_pDNS2       = NULL;

	m_pSendMSS    = NULL;

	m_pRecvMSS    = NULL;
	}

// Initial Values

void CEthernetFace::SetInitValues(void)
{
	DWORD Mask = DWORD(MAKELONG(MAKEWORD(0, 255), MAKEWORD(255, 255)));

	DWORD Addr = 0;

	switch( m_uPort ) {

		case 0:
			Addr = C3OemFeature(L"OemSD", FALSE) ? IP1_HON : IP1_STD;
			break;

		case 1:
			Addr = C3OemFeature(L"OemSD", FALSE) ? IP2_HON : IP2_STD;
			break;
		}

	SetInitial(L"Mode",       m_pMode,       m_uPort == 0 ? 1 : 0);

	SetInitial(L"EnableFull", m_pEnableFull, 1);

	SetInitial(L"Enable100",  m_pEnable100,  1);

	SetInitial(L"Address",    m_pAddress,    Addr);

	SetInitial(L"NetMask",    m_pNetMask,    Mask);

	SetInitial(L"Gateway",    m_pGateway,    0);

	SetInitial(L"Metric",     m_pMetric,     1);

	SetInitial(L"DNSMode",    m_pDNSMode,    m_uPort == 0 ? 1 : 0);

	SetInitial(L"DNS1",       m_pDNS1,       0x08080808);

	SetInitial(L"DNS2",       m_pDNS2,       0x08080404);

	SetInitial(L"SendMSS",    m_pSendMSS,    1280);

	SetInitial(L"RecvMSS",    m_pRecvMSS,    1280);
	}

// UI Update

void CEthernetFace::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Mode" || Tag == "DNSMode" ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

DWORD CEthernetFace::GetAddress(void) const
{
	if( m_pAddress ) {

		return m_pAddress->ExecVal();
		}

	return 0;
	}

// Operations

void CEthernetFace::PostConvert(BOOL fValid)
{
	if( !fValid ) {

		m_pMode->Compile(CError(FALSE), L"0");
		}
	}

// Download Support

BOOL CEthernetFace::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pMode);
	Init.AddItem(itemVirtual, m_pEnableFull);
	Init.AddItem(itemVirtual, m_pEnable100);
	Init.AddItem(itemVirtual, m_pAddress);
	Init.AddItem(itemVirtual, m_pNetMask);
	Init.AddItem(itemVirtual, m_pGateway);
	Init.AddItem(itemVirtual, m_pMetric);
	Init.AddItem(itemVirtual, m_pSendMSS);
	Init.AddItem(itemVirtual, m_pRecvMSS);
	Init.AddItem(itemVirtual, m_pDNSMode);
	Init.AddItem(itemVirtual, m_pDNS1);
	Init.AddItem(itemVirtual, m_pDNS2);

	return TRUE;
	}

// Meta Data Creation

void CEthernetFace::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Mode);
	Meta_AddVirtual(EnableFull);
	Meta_AddVirtual(Enable100);
	Meta_AddVirtual(Address);
	Meta_AddVirtual(NetMask);
	Meta_AddVirtual(Gateway);
	Meta_AddVirtual(Metric);
	Meta_AddVirtual(DNSMode);
	Meta_AddVirtual(DNS1);
	Meta_AddVirtual(DNS2);
	Meta_AddVirtual(SendMSS);
	Meta_AddVirtual(RecvMSS);

	Meta_SetName((IDS_ETHERNET_3));
	}

// Implementation

void CEthernetFace::DoEnables(IUIHost *pHost)
{
	if( m_uPort == 1 ) {

		CEthernetItem *pNet = (CEthernetItem *) GetParent();

		if( !pNet->HasDualPorts() ) {

			pHost->EnableUI(this, FALSE);

			return;
			}
		}

	BOOL fEnable = CheckEnable(m_pMode,    L">",  0, TRUE);

	BOOL fIP     = CheckEnable(m_pMode,    L"!=", 3, TRUE);

	BOOL fManual = CheckEnable(m_pMode,    L"==", 2, TRUE);

	BOOL fDNS    = CheckEnable(m_pDNSMode, L"==", 2, TRUE);

	pHost->EnableUI("Mode",       TRUE);

	pHost->EnableUI("Address",    fManual);

	pHost->EnableUI("NetMask",    fManual);

	pHost->EnableUI("Gateway",    fManual);

	pHost->EnableUI("Metric",     fEnable && fIP);

	pHost->EnableUI("DNSMode",    fEnable && fIP);

	pHost->EnableUI("DNS1",       fEnable && fIP && fDNS);

	pHost->EnableUI("DNS2",       fEnable && fIP && fDNS);

	pHost->EnableUI("EnableFull", fEnable);

	pHost->EnableUI("Enable100",  fEnable);

	pHost->EnableUI("RecvMSS",    fEnable && fIP);

	pHost->EnableUI("SendMSS",    fEnable && fIP);
	}

// End of File
