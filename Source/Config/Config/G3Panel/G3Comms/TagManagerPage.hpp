
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagManagerPage_HPP

#define INCLUDE_TagManagerPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagManager;

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager Page
//

class CTagManagerPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTagManagerPage(CTagManager *pTags);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CTagManager * m_pTags;

		// Implementation
		BOOL LoadBasePage(IUICreate *pView);
		BOOL LoadFileButtons(IUICreate *pView);
		BOOL LoadFindButtons(IUICreate *pView);
	};

// End of File

#endif
