
#include "Intern.hpp"

#include "ContactsPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MailContacts.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Contacts Page
//

// Runtime Class

AfxImplementRuntimeClass(CContactsPage, CUIPage);

// Constructor

CContactsPage::CContactsPage(CMailContacts *pContacts)
{
	m_pContacts = pContacts;
	}

// Operations

BOOL CContactsPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	// LATER -- Implement a richer contacts list.

	pView->StartTable(CString(IDS_CONTACTS), 3);

	pView->AddColHead(CString(IDS_NAME));

	pView->AddColHead(CString(IDS_ADDRESS));

	pView->AddColHead(CString(IDS_ENABLE));

	UINT c = m_pContacts->m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CItem *pItem = m_pContacts->m_pList->GetItem(n);

		pView->AddRowHead(CPrintf(L"%u", n));

		pView->AddUI(pItem, L"root", L"Name");

		pView->AddUI(pItem, L"root", L"Addr");

		pView->AddUI(pItem, L"root", L"Enable");
		}

	pView->EndTable();

	return TRUE;
	}

// End of File
