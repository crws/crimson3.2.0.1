
#include "Intern.hpp"

#include "UITextColorPair.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Color Pair
//

// Dynamic Class

AfxImplementDynamicClass(CUITextColorPair, CUITextElement);

// Constructor

CUITextColorPair::CUITextColorPair(void)
{
	m_uFlags = textEdit | textExpand;
	}

// Overridables

void CUITextColorPair::OnBind(void)
{
	CUITextElement::OnBind();

	m_uWidth = 20;

	m_uLimit = 20;
	}

CString CUITextColorPair::OnGetAsText(void)
{
	UINT    Pair = m_pData->ReadInteger(m_pItem);

	CString Fore = NameColor(LOWORD(Pair));

	CString Back = NameColor(HIWORD(Pair));

	return Fore + L" on " + Back;
	}

UINT CUITextColorPair::OnSetAsText(CError &Error, CString Text)
{
	UINT uPos = Text.Find(L" on ");

	if( uPos < NOTHING ) {

		CString ForeText = Text.Left(uPos);

		CString BackText = Text.Mid (uPos+4);

		UINT    ForeData = 0;

		UINT    BackData = 0;

		if( FindColor(Error, ForeText, ForeData) ) {

			if( FindColor(Error, BackText, BackData) ) {

				UINT Pair = MAKELONG(ForeData, BackData);

				UINT Prev = m_pData->ReadInteger(m_pItem);

				if( Prev == Pair ) {

					return saveSame;
					}

				m_pData->WriteInteger(m_pItem, Pair);

				return saveChange;
				}
			}
		}

	return saveError;
	}

// Implementation

BOOL CUITextColorPair::FindColor(CError &Error, CString Text, UINT &Color)
{
	Text.TrimBoth();

	if( Text.StartsWith(L"0x") ) {

		PCTXT pHex = PCTXT(Text) + 2;

		Color = wcstol(pHex, NULL, 16);

		return TRUE;
		}

	for( UINT n = 0; n < 32; n++ ) {

		if( Text == C3StdColorName(n) ) {

			Color = C3StdColorData(n);

			return TRUE;
			}
		}

	Error.Set(CString(IDS_YOU_HAVE_ENTERED));

	return FALSE;
	}

CString CUITextColorPair::NameColor(COLOR Color)
{
	for( UINT n = 0; n < 32; n++ ) {

		if( Color == C3StdColorData(n) ) {

			return C3StdColorName(n);
			}
		}

	return CPrintf(L"0x%4.4X", Color);
	}

// End of File
