
#include "Intern.hpp"

#include "TagManagerPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "TagManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager Page
//

// Runtime Class

AfxImplementRuntimeClass(CTagManagerPage, CUIStdPage);

// Constructor

CTagManagerPage::CTagManagerPage(CTagManager *pTags)
{
	m_Class  = AfxRuntimeClass(CTagManager);

	m_pTags = pTags;
}

// Operations

BOOL CTagManagerPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	LoadBasePage(pView);

	LoadFileButtons(pView);

	LoadFindButtons(pView);

	return TRUE;
}

// Implementation

BOOL CTagManagerPage::LoadBasePage(IUICreate *pView)
{
	CDatabase *pDbase = m_pTags->GetDatabase();

	if( pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		CUIPage *pPage = New CUIStdPage(m_Class);

		pPage->LoadIntoView(pView, m_pTags);

		delete pPage;
	}

	return TRUE;
}

BOOL CTagManagerPage::LoadFileButtons(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_TAG_COMMANDS),
			  1
	);

	pView->AddButton(CString(IDS_EXPORT_TAGS),
			 CString(IDM_TAGS_EXPORT),
			 L"ButtonExportTags"
	);

	pView->AddButton(CString(IDS_IMPORT_TAGS),
			 CString(IDM_TAGS_IMPORT),
			 L"ButtonImportTags"
	);

	pView->EndGroup(FALSE);

	return TRUE;
}

BOOL CTagManagerPage::LoadFindButtons(IUICreate *pView)
{
	if( m_pTags->GetDatabase()->GetSoftwareGroup() >= SW_GROUP_3A ) {

		// REV3 -- Add things like Find Unused etc.

		pView->StartGroup(CString(IDS_TAG_SEARCHING),
				  1
		);

		if( C3OemFeature(L"OemSD", FALSE) ) {

			pView->AddButton(CString(IDS_FIND_LOCKED),
					 CString(IDS_FIND_ALL_TAGS),
					 L"ButtonFindLocked"
			);
		}

		pView->AddButton(CString(IDS_FIND_ALARMS),
				 CString(IDM_TAGS_FIND_ALARMS),
				 L"ButtonFindAlarms"
		);

		pView->AddButton(CString(IDS_FIND_TRIGGERS),
				 CString(IDM_TAGS_FIND_TRIGGERS),
				 L"ButtonFindTriggers"
		);

		pView->AddButton(CString(IDS_FIND_UNUSED),
				 CString(IDS_FIND_ALL_TAGS_2),
				 L"ButtonFindUnused"
		);

		pView->EndGroup(FALSE);
	}

	return TRUE;
}

// End of File
