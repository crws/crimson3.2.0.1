
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ColorManager_HPP

#define INCLUDE_ColorManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Color Manager
//

class DLLAPI CColorManager : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CColorManager(void);

		// Attributes
		UINT    GetGroupCount(void) const;
		UINT    GetCount(UINT uGroup) const;
		COLOR   GetColor(UINT uGroup, UINT uIndex) const;
		CString GetName (UINT uGroup, UINT uIndex) const;
		BOOL    NeedsSep(UINT uGroup) const;

		// Operations
		void UpdateAux(void);
		void Sync(CColorManager *pThat);
		BOOL FindColor(COLOR Color, UINT &uGroup, UINT &uIndex);
		void MarkColor(UINT uGroup, UINT uIndex);
		void MarkColor(COLOR Color);

		// Data Members
		UINT m_Custom[24];
		UINT m_Select[24];
		UINT m_Now;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
