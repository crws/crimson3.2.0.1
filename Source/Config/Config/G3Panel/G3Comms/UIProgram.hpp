
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIProgram_HPP

#define INCLUDE_UIProgram_HPP

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Program Editor
//

class CUIProgram : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIProgram(void);

	protected:
		// Data Members
		CLayItemSize     * m_pDataLayout;
		CSourceEditorWnd * m_pDataCtrl;

		// Core Overidables
		void OnBind(void);
		void OnRebind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect GetDataWindowRect(void);
	};

// End of File

#endif
