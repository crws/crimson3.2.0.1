
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagEvent_HPP

#define INCLUDE_TagEvent_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Tag Event
//

class DLLNOT CTagEvent : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagEvent(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Searching
		void FindActive(CStringArray &List);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT	      m_Mode;
		CCodedText  * m_pLabel;
		UINT	      m_Trigger;
		UINT	      m_Delay;
		UINT	      m_Accept;
		UINT	      m_Priority;
		UINT	      m_Print;
		UINT	      m_Siren;
		UINT	      m_Mail;
		CCodedItem  * m_pOnActive;
		CCodedItem  * m_pOnClear;
		CCodedItem  * m_pOnAccept;
		CCodedItem  * m_pOnEvent;
		CCodedItem  * m_pEnable;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		// Property Access
		DWORD FindLabel(void);
	};

// End of File

#endif
