
#include "Intern.hpp"

#include "SqlQueryManagerTreeWnd_CCmdCreate.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SQL Tree Window -- Create Command
//

// Runtime Class

AfxImplementRuntimeClass(CSqlQueryManagerTreeWnd::CCmdCreate, CStdCmd);

CSqlQueryManagerTreeWnd::CCmdCreate::CCmdCreate(CItem *pRoot, CString List, CMetaItem *pItem)
{
	m_Menu  = CFormat(CString(IDS_CREATE_3), pItem->GetName());

	m_Item  = pRoot->GetFixedPath();

	m_List  = List;

	m_Made  = pItem->GetFixedPath();

	m_hData = pItem->TakeSnapshot();
	}

// Destructor

CSqlQueryManagerTreeWnd::CCmdCreate::~CCmdCreate(void)
{
	GlobalFree(m_hData);
	}

// End of File
