
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlFilterDialog_HPP

#define INCLUDE_SqlFilterDialog_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlFilterList;
class CSqlQuery;

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Dialog
//

class CSqlFilterDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSqlFilterDialog(CSqlQuery *pQuery);

	protected:

		// Conditions
		struct CCondition
		{
			PCTXT pCond;
			BOOL  fArgs;
			};

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uId);
		BOOL OnCreateFilter(UINT uID);
		BOOL OnEditFilter(UINT uID);
		BOOL OnDelFilter(UINT uID);
		void OnItemChanged(UINT uID, NMLISTVIEW &Info);
		void OnDblClk(UINT uId, NMHDR &HDR);

		// Data Members
		CSqlQuery       * m_pQuery;
		CSqlFilterList  * m_pList;

		// Implementation
		void Update(void);
		void LoadFilters(void);
		BOOL EditFilter(void);
		BOOL DeleteFilter(void);
		void DoEnables(void);
	};

// End of File

#endif
