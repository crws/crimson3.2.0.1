
#include "Intern.hpp"

#include "MailContacts.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ContactsPage.hpp"
#include "MailAddress.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mail Contacts
//

// Dynamic Class

AfxImplementDynamicClass(CMailContacts, CUIItem);

// Constructor

CMailContacts::CMailContacts(void)
{
	m_pList = New CItemIndexList(AfxRuntimeClass(CMailAddress));
	}

// UI Creation

BOOL CMailContacts::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CContactsPage(this));

	return FALSE;
	}

// Persistance

void CMailContacts::Init(void)
{
	CUIItem::Init();

	m_pList->SetItemCount(50);
	}

// Download Support

BOOL CMailContacts::MakeInitData(CInitData &Init)
{
	m_pList->MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CMailContacts::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(List);
	}

// End of File
