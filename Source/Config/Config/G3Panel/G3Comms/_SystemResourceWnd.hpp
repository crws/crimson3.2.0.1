
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SystemResourceWnd_HPP

#define INCLUDE_SystemResourceWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// System Resource Window
//

class CSystemResourceWnd : public CResTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSystemResourceWnd(void);

	protected:
		// Data Members
		UINT	        m_cfCode;

		// Tree Loading
		void LoadImageList(void);
		void LoadTree(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		void NewItemSelected(void);

		// Data Object Creation
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL AddCodeFragment(CDataObject *pData);

		// Sort Helpers
		int Sort(CMetaItem *p1, CMetaItem *p2);

		// Sort Callback
		static int CALLBACK SortHelp(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	};

// End of File

#endif
