
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIAction_HPP

#define INCLUDE_UIAction_HPP

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Action
//

class CUIAction : public CUICategorizer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIAction(void);

	protected:
		// Modes
		enum
		{
			modeGeneral    = 1,
			modeComplex    = 2,
			modeGeneralWas = 1000,
			modeComplexWas = 1001,
			};

		// Data Members
		CString m_Params;
		UINT    m_cfCode;

		// Core Overridables
		void OnBind(void);
		void OnCreate(CWnd &Wnd, UINT &uID);

		// Notification Overridables
		BOOL OnNotify(UINT uID, UINT uCode);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);

		// Category Overridables
		BOOL    LoadModeButton(void);
		BOOL    SwitchMode(UINT uMode);
		UINT    FindDispMode(CString Text);
		CString FindDispText(CString Text, UINT uMode);
		CString FindDataText(CString Text, UINT uMode);
		UINT    FindDispType(UINT uMode);
		CString FindDispVerb(UINT uMode);

		// Implementation
		BOOL EditCode(CString &Text);
		BOOL IsComplex(CString Text);
		void MakeComplex(CString &Code);
	};

// End of File

#endif
