
#include "Intern.hpp"

#include "TagImport.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedHost.hpp"
#include "CodedText.hpp"
#include "CommsDevice.hpp"
#include "CommsPort.hpp"
#include "CommsSystem.hpp"
#include "DataTag.hpp"
#include "DispFormatFlag.hpp"
#include "DispFormatIPAddr.hpp"
#include "DispFormatMulti.hpp"
#include "DispFormatMulti.hpp"
#include "DispFormatMultiList.hpp"
#include "DispFormatMultiEntry.hpp"
#include "DispFormatNumber.hpp"
#include "DispFormatSci.hpp"
#include "DispFormatTimeDate.hpp"
#include "LangManager.hpp"
#include "Lexicon.hpp"
#include "SecDesc.hpp"
#include "Tag.hpp"
#include "TagEvent.hpp"
#include "TagFolder.hpp"
#include "TagFlag.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"
#include "TagNumeric.hpp"
#include "TagQuickPlot.hpp"
#include "TagSimple.hpp"
#include "TagString.hpp"
#include "TagTrigger.hpp"

////////////////////////////////////////////////////////////////////////
//
// Tag Import Helper
//

// Sort Context

CTagList * CTagImport::m_pSort = NULL;

int        CTagImport::m_nSort = 1;

// Constructor

CTagImport::CTagImport(CCommsPort *pPort, CCommsDevice *pDevice)
{
	m_pPort   = pPort;

	m_pDevice = pDevice;

	m_pDriver = pDevice->GetDriver();

	m_pSystem = (CCommsSystem *) m_pPort->GetDatabase()->GetSystemItem();

	m_pList   = m_pSystem->m_pTags->m_pTags;

	m_pLang   = m_pSystem->m_pLang;

	m_DevName = pDevice->GetName();

	m_fMore   = FALSE;

	m_pLex    = NULL;

	m_pBlob   = NULL;

	m_hSave   = NULL;

	FindLexicon();

	FindBlob(m_pDevice->m_pConfig);
	}

// Destructor

CTagImport::~CTagImport(void)
{
	delete m_pLex;

	GlobalFree(m_hSave);
	}

// Operations

BOOL CTagImport::Import(void)
{
	if( m_pDriver->MakeTags(this, m_pDevice->m_pConfig, m_pPort->m_pConfig, m_DevName) ) {

		CSysProxy System;

		System.Bind();

		System.ItemUpdated(m_pList, updateChildren);

		return TRUE;
		}

	return FALSE;
	}

// Management

BOOL CTagImport::InitImport(CString Folder, BOOL fMore, CFilename File)
{
	CSysProxy System;

	System.Bind();

	if( System.KillUndoList() ) {

		m_Root  = Folder;

		m_fMore = fMore;

		////////

		afxMainWnd->UpdateWindow();

		afxThread->SetWaitMode(TRUE);

		////////

		afxThread->SetStatusText(CString(IDS_DELETING_TAGS));

		KillFolder(m_Root);

		m_Folder.Append(L"");

		m_FClass.Append(L"");

		m_IsUsed.Append(TRUE);

		m_nPos = 0;

		////////

		afxThread->SetStatusText(CString(IDS_CREATING_TAGS_1));

		AddFolder(L"", m_Root);

		EmitFolder()->m_Desc = File;

		return TRUE;
		}

	return FALSE;
	}

void CTagImport::ImportDone(BOOL fSort)
{
	EndFolder();

	////////

	if( m_pBlob ) {

		PCBYTE pData = m_Init.GetPointer();

		UINT   uSize = m_Init.GetCount();

		m_pBlob->Empty();

		m_pBlob->Append(pData, uSize);
		}

	////////

	afxThread->SetStatusText(CString(IDS_RESTORING_DATA));

	RestoreData();

	////////

	m_pDevice->ClearSysBlocks();

	m_pSystem->ClearSysBlocks();

	m_pSystem->Validate(FALSE);

	////////

	if( fSort ) {

		afxThread->SetStatusText(CString(IDS_SORTING_TAGS));

		SortTags(m_Root);
		}

	////////

	afxThread->SetStatusText(L"");

	afxThread->SetWaitMode(FALSE);
	}

// Advanced

BOOL CTagImport::GetInitData(CInitData * &pInit)
{
	if( m_pBlob ) {

		pInit = &m_Init;

		return TRUE;
		}

	return FALSE;
	}

void CTagImport::SetWriteBack(CString Code)
{
	CDataTag   *pHost = (CDataTag *) m_pLast;

	CCodedItem *pItem = New CCodedItem;

	CTypeDef Type;

	Type.m_Type  = typeVoid;

	Type.m_Flags = flagActive;

	pItem->SetParent(pHost);

	pItem->Init();

	pItem->SetReqType(Type);

	pItem->SetParams(L"Data=SubValue");

	if( !pItem->Compile(Code) ) {

		Code = L"WAS " + Code;

		pItem->Compile(Code);
		}

	pHost->m_pOnWrite = pItem;
	}

// Folders

BOOL CTagImport::AddRootFolder(CString Class, CString Root, CString Desc)
{
	CSysProxy System;

	System.Bind();

	if( System.KillUndoList() ) {

		m_Root  = Root;

		m_fMore = TRUE;

		m_Folder.Append(L"");

		m_FClass.Append(L"");

		m_IsUsed.Append(TRUE);

		m_nPos = 0;

		AddFolder(Class, Root, Desc);

		return TRUE;
		}

	return FALSE;
	}

void CTagImport::AddFolder(CString Class, CString Name, CString Desc)
{
	AddFolder(Class, Name);

	EmitFolder()->m_Desc = Desc;
	}

void CTagImport::AddFolder(CString Class, CString Name)
{
	if( m_nPos ) {

		m_Path += L'.';
		}

	m_Path += Name;

	m_nPos += 1;

	m_Folder.Append(m_Path);

	m_FClass.Append(Class);

	m_IsUsed.Append(FALSE);
	}

void CTagImport::EndFolder(void)
{
	m_Folder.Remove(m_nPos);

	m_FClass.Remove(m_nPos);

	m_IsUsed.Remove(m_nPos);

	m_nPos = m_nPos - 1;

	m_Path = m_Folder[m_nPos];
	}

// Tag Creation

BOOL CTagImport::AddReal(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Min, CString Max, UINT DP)
{
	if( Value.IsEmpty() ) {

		Write = 1;
		}

	if( !Extent && !Write && Min.IsEmpty() && Max.IsEmpty() && !DP && !IsComms(Value) ) {

		CTagSimple *pTag = New CTagSimple;

		if( AppendTag(pTag, Tag) ) {

			if( Value.Find(L"\r\n") < NOTHING ) {

				pTag->m_Extent = 1;
				}

			pTag->m_pLabel = GetText(pTag, L"Label", Label);

			pTag->m_pValue = Compile(pTag, L"Value", Value);

			return TRUE;
			}
		}
	else {
		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel  = GetText(pTag, L"Label", Label);

			pTag->m_pValue  = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs = 2;

			pTag->m_Extent  = Extent;

			pTag->m_Access  = Write ? 0 : 2;

			if( Min.GetLength() || Max.GetLength() ) {

				pTag->m_LimitType = 2;

				pTag->m_pLimitMin = Compile(pTag, L"LimitMin", Min);

				pTag->m_pLimitMax = Compile(pTag, L"LimitMax", Max);
				}

			if( DP < 127 ) {

				pTag->m_FormType = 1;

				pTag->m_pFormat  = New CDispFormatNumber;

				pTag->UpdateExtent();

				CDispFormatNumber *pFmt = (CDispFormatNumber *) pTag->m_pFormat;

				pFmt->SetParent(pTag);

				pFmt->Init();

				pFmt->m_Before  = (DP == NOTHING) ? 10 : 6;

				pFmt->m_After   = (DP == NOTHING) ?  0 : DP;

				pFmt->m_Leading = 2;
				}
			else {
				pTag->m_FormType = 2;

				pTag->m_pFormat  = New CDispFormatSci;

				pTag->UpdateExtent();

				CDispFormatSci *pFmt = (CDispFormatSci *) pTag->m_pFormat;

				pFmt->SetParent(pTag);

				pFmt->Init();

				pFmt->m_After = 4;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::AddInt(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Min, CString Max, UINT DP, UINT TreatAs)
{
	if( Value.IsEmpty() ) {

		Write = 1;
		}

	if( !Extent && !Write && Min.IsEmpty() && Max.IsEmpty() && !DP && !IsComms(Value) ) {

		CTagSimple *pTag = New CTagSimple;

		if( AppendTag(pTag, Tag) ) {

			if( Value.Find(L"\r\n") < NOTHING ) {

				if( Label != L"Time" ) {

					pTag->m_Extent = 1;
					}
				}

			pTag->m_pLabel = GetText(pTag, L"Label", Label);

			pTag->m_pValue = Compile(pTag, L"Value", Value);

			return TRUE;
			}
		}
	else {
		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel  = GetText(pTag, L"Label", Label);

			pTag->m_pValue  = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs = TreatAs;

			pTag->m_Extent  = Extent;

			pTag->m_Access  = Write ? 0 : 2;

			if( Min.GetLength() || Max.GetLength() ) {

				pTag->m_LimitType = 2;

				pTag->m_pLimitMin = Compile(pTag, L"LimitMin", Min);

				pTag->m_pLimitMax = Compile(pTag, L"LimitMax", Max);
				}

			pTag->m_FormType = 1;

			pTag->m_pFormat  = New CDispFormatNumber;

			pTag->UpdateExtent();

			CDispFormatNumber *pFmt = (CDispFormatNumber *) pTag->m_pFormat;

			pFmt->SetParent(pTag);

			pFmt->Init();

			pFmt->m_Signed  = (DP == NOTHING) ?  0 : 1;

			pFmt->m_Before  = (DP == NOTHING) ? 10 : 6;

			pFmt->m_After   = (DP == NOTHING) ?  0 : DP;

			pFmt->m_Leading = 2;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::AddFnum(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Enum)
{
	if( Value.IsEmpty() ) {

		Write = 1;
		}

	if( TRUE ) {

		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel   = GetText(pTag, L"Label", Label);

			pTag->m_pValue   = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs  = 2;

			pTag->m_Extent   = Extent;

			pTag->m_Access   = Write ? 0 : 2;

			pTag->m_FormType = 6;

			pTag->m_pFormat  = New CDispFormatMulti;

			pTag->UpdateExtent();

			CDispFormatMulti *pFmt = (CDispFormatMulti *) pTag->m_pFormat;

			pFmt->SetParent(pTag);

			pFmt->Init();

			LoadEnum(pFmt, Enum, TRUE);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::AddEnum(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Enum)
{
	if( Value.IsEmpty() ) {

		Write = 1;
		}

	if( TRUE ) {

		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel   = GetText(pTag, L"Label", Label);

			pTag->m_pValue   = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs  = 0;

			pTag->m_Extent   = Extent;

			pTag->m_Access   = Write ? 0 : 2;

			pTag->m_FormType = 6;

			pTag->m_pFormat  = New CDispFormatMulti;

			pTag->UpdateExtent();

			CDispFormatMulti *pFmt = (CDispFormatMulti *) pTag->m_pFormat;

			pFmt->SetParent(pTag);

			pFmt->Init();

			LoadEnum(pFmt, Enum, TRUE);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::AddFlag(CString Tag, CString Label, CString Value, UINT Extent, UINT TreatAs, UINT Write, CString State0, CString State1)
{
	if( Value.IsEmpty() ) {

		Write = 1;
		}

	if( TRUE ) {

		CTagFlag *pTag = New CTagFlag;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel      = GetText(pTag, L"Label", Label);

			pTag->m_pValue      = Compile(pTag, L"Value", Value);

			pTag->m_Extent      = Extent;

			pTag->m_Access      = Write ? 0 : 2;

			pTag->m_FlagTreatAs = LOBYTE(TreatAs);

			pTag->m_TakeBit     = HIBYTE(TreatAs);

			pTag->UpdateExtent();

			CDispFormatFlag *pFmt = (CDispFormatFlag *) pTag->m_pFormat;

			pFmt->SetParent(pTag);

			pFmt->Init();

			pFmt->m_pOn  = GetText(pFmt, L"ON",  State1);

			pFmt->m_pOff = GetText(pFmt, L"OFF", State0);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::AddText(CString Tag, CString Label, CString Value, UINT Len, UINT Extent, UINT Write, UINT Encode)
{
	if( Value.IsEmpty() ) {

		Write = 1;
		}

	if( !Extent && !Write && !IsComms(Value) ) {

		CTagSimple *pTag = New CTagSimple;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel = GetText(pTag, L"Label", Label);

			pTag->m_pValue = Compile(pTag, L"Value", Value);

			return TRUE;
			}
		}
	else {
		CTagString *pTag = New CTagString;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel = GetText(pTag, L"Label", Label);

			pTag->m_pValue = Compile(pTag, L"Value", Value);

			pTag->m_Length = Len;

			pTag->m_Extent = Extent;

			pTag->m_Access = Write ? 0 : 2;

			pTag->m_Encode = Encode;

			pTag->UpdateExtent();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::AddSpec(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, UINT Spec)
{
	if( Spec == 101 ) {

		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel   = GetText(pTag, L"Label", Label);

			pTag->m_pValue   = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs  = 2;

			pTag->m_Extent   = Extent;

			pTag->m_Access   = Write ? 0 : 2;

			pTag->m_ScaleTo  = 2;

			pTag->m_pDataMin = Compile(pTag, L"DataMin", L"0");

			pTag->m_pDataMax = Compile(pTag, L"DataMax", L"1");

			pTag->m_pDispMin = Compile(pTag, L"DispMin", L"0");

			pTag->m_pDispMax = Compile(pTag, L"DispMax", L"60");

			pTag->m_FormType = 3;

			pTag->m_pFormat  = New CDispFormatTimeDate;

			pTag->UpdateExtent();

			CDispFormatTimeDate *pFmt = (CDispFormatTimeDate *) pTag->m_pFormat;

			pFmt->SetParent(pTag);

			pFmt->Init();

			pFmt->m_Mode = 7;

			pFmt->m_Secs = 1;

			return TRUE;
			}
		}

	if( Spec == 102 ) {

		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel   = GetText(pTag, L"Label", Label);

			pTag->m_pValue   = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs  = 0;

			pTag->m_Extent   = Extent;

			pTag->m_Access   = Write ? 0 : 2;

			pTag->m_pSim     = Compile(pTag, L"Sim", L"0");

			pTag->m_FormType = 3;

			pTag->m_pFormat  = New CDispFormatTimeDate;

			pTag->UpdateExtent();

			CDispFormatTimeDate *pFmt = (CDispFormatTimeDate *) pTag->m_pFormat;

			pFmt->SetParent(pTag);

			pFmt->Init();

			pFmt->m_Mode = 2;

			pFmt->m_Secs = 1;

			return TRUE;
			}
		}

	if( Spec == 200 ) {

		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel   = GetText(pTag, L"Label", Label);

			pTag->m_pValue   = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs  = 0;

			pTag->m_Extent   = Extent;

			pTag->m_Access   = Write ? 0 : 2;

			pTag->m_FormType = 4;

			pTag->m_pFormat  = New CDispFormatIPAddr;

			pTag->UpdateExtent();

			pTag->m_pFormat->SetParent(pTag);

			pTag->m_pFormat->Init();

			return TRUE;
			}
		}

	if( Spec == 300 ) {

		CTagString *pTag = New CTagString;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel = GetText(pTag, L"Label", Label);

			pTag->m_pValue = Compile(pTag, L"Value", Value);

			pTag->m_Length = 16;

			pTag->m_Extent = Extent;

			pTag->m_Access = Write ? 0 : 2;

			pTag->m_Encode = 6;

			pTag->UpdateExtent();

			return TRUE;
			}
		}

	if( Spec >= 400 && Spec <= 406 ) {

		CTagNumeric *pTag = New CTagNumeric;

		if( AppendTag(pTag, Tag) ) {

			pTag->m_pLabel   = GetText(pTag, L"Label", Label);

			pTag->m_pValue   = Compile(pTag, L"Value", Value);

			pTag->m_TreatAs  = 2;

			pTag->m_Extent   = 0;

			pTag->m_Access   = Write ? 0 : 2;

			pTag->m_FormType = 1;

			pTag->m_pFormat  = New CDispFormatNumber;

			pTag->UpdateExtent();

			CDispFormatNumber *pFmt = (CDispFormatNumber *) pTag->m_pFormat;

			pFmt->SetParent(pTag);

			pFmt->Init();

			pFmt->m_Before  = 6;

			pFmt->m_After   = Spec - 400;

			pFmt->m_Leading = 2;

			pTag->m_pQuickPlot->m_Mode    = 3;

			pTag->m_pQuickPlot->m_Rewind  = 1;

			pTag->m_pQuickPlot->m_Points  = 640;

			pTag->m_pQuickPlot->m_pEnable = Compile(pTag->m_pQuickPlot, L"Enable", L"State >= 2 && State <= 4");

			pTag->m_pQuickPlot->m_pTime   = Compile(pTag->m_pQuickPlot, L"Time",   L"PrePlot.Time");

			pTag->m_pQuickPlot->m_pLook   = Compile(pTag->m_pQuickPlot, L"Look",   L"PrePlot.Limit");

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

BOOL CTagImport::AppendTag(CTag *pTag, CString Tag)
{
	EmitFolder();

	CString Full = m_Path + L'.' + Tag;

	if( m_pList->FindByName(Full) ) {

		if( FALSE ) {

			for( UINT n = 1;; n++ ) {

				Full = m_Path + L'.' + Tag + CPrintf("_%u", n);

				if( !m_pList->FindByName(Full) ) {

					break;
					}
				}
			}
		else {
			delete pTag;

			return FALSE;
			}
		}

	if( ValidatePath(Full) ) {

		m_pLast = pTag;

		m_pList->AppendItem(pTag);

		pTag->SetName(Full);

		return TRUE;
		}

	delete pTag;

	return FALSE;
	}

BOOL CTagImport::ValidatePath(CString Full) {

	while( !Full.IsEmpty() ) {

		CString Part = Full.TokenLeft(L'.');

		if( !C3ValidateName(CError(FALSE), Part) ) {

			return FALSE;
			}

		Full = Full.TokenFrom(L'.');
		}

	return TRUE;
	}

BOOL CTagImport::FindLexicon(void)
{
	if( m_pLang->GetUsedCount() > 1 ) {

		m_pLex         = New CLexicon;

		CModule * pApp = afxModule->GetApp();

		CFilename Path = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Lexicon");

		CFilename Name = Path + L"lex.txt";

		m_pLex->Open(CError(FALSE), Name, m_pLang->GetAbbrFromSlot(0U));

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagImport::FindBlob(CMetaItem *pConfig)
{
	if( pConfig ) {

		CMetaData const *pMeta = pConfig->FindMetaData(L"Blob");

		if( pMeta ) {

			m_pBlob = &pMeta->GetBlob(pConfig);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::IsComms(CString const &Value)
{
	return Value[0] == '[';
	}

void CTagImport::KillFolder(CString Folder)
{
	CTextStreamMemory Stream;

	if( Stream.OpenSave() ) {

		CTreeFile Tree;

		if( Tree.OpenSave(Stream) ) {

			INDEX Index = m_pList->GetHead();

			while( !m_pList->Failed(Index) ) {

				CTag *  pTag = m_pList->GetItem(Index);

				CString Name = pTag->GetName();

				UINT    uPos = Name.Find('.');

				if( uPos < NOTHING ) {

					if( Name.Left(uPos) == Folder ) {

						PreserveData(Tree, pTag);

						INDEX Next = Index;

						m_pList->GetNext(Next);

						m_pList->DeleteItem(Index);

						Index = Next;

						continue;
						}
					}

				m_pList->GetNext(Index);
				}
			}
		}

	m_hSave = Stream.TakeOver();
	}

CTagFolder * CTagImport::EmitFolder(void)
{
	CTagFolder *pFolder = NULL;

	for( int n = 1; n <= m_nPos; n++ ) {

		if( !m_IsUsed[n] ) {

			CString Name  = m_Folder[n];

			CString Class = m_FClass[n];

			UINT    uPos  = m_pList->FindNamePos(Name);

			if( uPos < NOTHING ) {

				CTag *pTag = m_pList->GetItem(uPos);

				if( !pTag->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

					AfxAssert(FALSE);
					}

				pFolder = (CTagFolder *) pTag;
				}
			else {
				pFolder = New CTagFolder;

				m_pList->AppendItem(pFolder);

				pFolder->m_Locked = 0;

				pFolder->SetName(Name);

				pFolder->m_Class = Class;
				}

			m_IsUsed.SetAt(n, TRUE);
			}
		}

	if( pFolder ) {

		CString Status = IDS_CREATING_TAGS_2;

		afxThread->SetStatusText(Status + pFolder->m_Name);
		}

	return pFolder;
	}

CCodedText * CTagImport::GetText(CCodedHost *pHost, CString Tag, CString Code)
{
	if( Code.GetLength() ) {

		if( m_pLex && m_pLex->IsValid() ) {

			UINT    uCount = m_pLang->GetUsedCount();

			CString From   = Code;

			for( UINT n = 1; n < uCount; n++ ) {

				CString Abbr = m_pLang->GetAbbrFromSlot(n);

				CString Text = m_pLex->Translate(From, Abbr);

				Code += LANG_SEP;

				Code += Text;
				}
			}

		Code.Replace(L"\"", L"\\\"");

		Code = L"\"" + Code + L"\"";

		return (CCodedText *) Compile( pHost,
					       Tag,
					       Code,
					       AfxRuntimeClass(CCodedText)
					       );
		}

	return NULL;
	}

CCodedItem * CTagImport::Compile(CCodedHost *pHost, CString Tag, CString Code)
{
	return Compile( pHost,
			Tag,
			Code,
			AfxRuntimeClass(CCodedItem)
			);
	}

CCodedItem * CTagImport::Compile(CCodedHost *pHost, CString Tag, CString Code, CLASS Class)
{
	if( Code.GetLength() ) {

		// LATER -- This is horrible!

		CTypeDef Type;

		if( Code.StartsWith(L"[") ) {

			CString Left = Code.Left(0);

			CString Rest = Code.Mid (1);

			Code = Left + L"[" + m_DevName + L"." + Rest;
			}

		if( Code.StartsWith(L"Max([") ) {

			CString Left = Code.Left(4);

			CString Rest = Code.Mid (5);

			Code = Left + L"[" + m_DevName + L"." + Rest;
			}

		if( pHost->GetTypeData(Tag, Type) ) {

			CCodedItem *pItem = AfxNewObject(CCodedItem, Class);

			pItem->SetParent(pHost);

			pItem->Init();

			pItem->SetReqType(Type);

			if( Code.Find(L"\r\n") < NOTHING ) {

				if( Tag != L"Time" ) {

					pItem->SetParams(L"i=SubIndex");
					}
				}

			if( !pItem->Compile(Code) ) {

				Code = L"WAS " + Code;

				pItem->Compile(Code);
				}

			return pItem;
			}
		}

	return NULL;
	}

void CTagImport::LoadEnum(CDispFormatMulti *pFmt, CString Enum, BOOL fDefault)
{
	CStringArray List;

	Enum.Tokenize(List, L'|');

	pFmt->m_Count = List.GetCount();

	pFmt->m_Range = 0;

	pFmt->m_pList->SetItemCount(pFmt->m_Count);

	for( UINT uState = 0; uState < pFmt->m_Count; uState++ ) {

		CDispFormatMultiEntry *pState = pFmt->m_pList->GetItem(uState);

		CString Data = CPrintf(L"%u", uState);

		CString Text = List[uState];

		UINT    uPos = Text.Find(L',');

		if( uPos < NOTHING ) {

			Data = Text.Mid (uPos+1);

			Text = Text.Left(uPos);
			}

		if( fDefault ) {

			if( pFmt->m_Count == 2 && uState == 1 ) {

				pFmt->m_pDefault = GetText(pFmt, L"Default", Text);
				}
			}

		if( pState->m_pData ) {

			pState->m_pData->Kill();

			delete pState->m_pData;
			}

		if( pState->m_pText ) {

			pState->m_pText->Kill();

			delete pState->m_pText;
			}

		pState->m_pData = Compile(pState, L"Data", Data);

		pState->m_pText = GetText(pState, L"Text", Text);
		}
	}

BOOL CTagImport::PreserveData(CTreeFile &Tree, CTag *pTag)
{
	CMetaList *pList = pTag->FindMetaList();

	UINT      uCount = pList->GetCount();

	BOOL      fFirst = TRUE;

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = pList->FindData(n);

		if( PreserveProp(pTag, pMeta) ) {

			if( fFirst ) {

				Tree.PutObject(pTag->GetName());

				fFirst = FALSE;
				}

			pTag->SaveProp(Tree, pMeta);
			}
		}

	if( !fFirst ) {

		Tree.EndObject();

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagImport::PreserveProp(CTag *pTag, CMetaData const *pMeta)
{
	CString Name = pMeta->GetTag();

	if( pTag->SaveProp(Name) ) {

		if( pTag->IsKindOf(AfxRuntimeClass(CTagNumeric)) ) {

			CTagNumeric *pNum = (CTagNumeric *) pTag;

			if( pNum->m_ScaleTo ) {

				if( Name == L"ScaleTo" ) {

					return TRUE;
					}

				if( Name == L"DataMin" || Name == L"DataMax" ) {

					if( pNum->m_pDataMin || pNum->m_pDataMax ) {

						return TRUE;
						}

					return FALSE;
					}

				if( Name == L"DispMin" || Name == L"DispMax" ) {

					if( pNum->m_pDispMin || pNum->m_pDispMax ) {

						return TRUE;
						}

					return FALSE;
					}
				}
			}

		if( !pTag->m_pFormat ) {

			if( Name == L"LimitMin" || Name == L"LimitMax" || Name == L"LimitType" ) {

				return TRUE;
				}
			}
		else {
			CDataTag *pData = (CDataTag *) pTag;

			if( Name == L"LimitMin" || Name == L"LimitMax" || Name == L"LimitType" ) {

				if( pData->m_FormLock ) {

					return TRUE;
					}

				return m_fMore;
				}

			if( Name == L"FormType" || Name == L"FormLock" || Name == L"Format" ) {

				if( pData->m_FormLock ) {

					return TRUE;
					}

				return m_fMore;
				}
			}

		if( Name == L"ColType" || Name == L"Color" ) {

			if( pTag->m_pColor ) {

				return TRUE;
				}

			return FALSE;
			}

		if( Name == L"Trigger1" || Name == L"Trigger2" ) {

			CItem *pItem = pMeta->GetObject(pTag);

			if( pItem ) {

				if( ((CTagTrigger *) pItem)->m_Mode ) {

					return TRUE;
					}
				}

			return FALSE;
			}

		if( Name == L"Event1" || Name == L"Event2" ) {

			CItem *pItem = pMeta->GetObject(pTag);

			if( pItem ) {

				if( ((CTagEvent *) pItem)->m_Mode ) {

					return TRUE;
					}
				}

			return FALSE;
			}

		if( Name == L"Sec" ) {

			CDataTag *pTest = (CDataTag *) pTag;

			if( pTest->m_pSec ) {

				if( pTest->m_pSec->m_Access != allowDefault ) {

					return TRUE;
					}

				if( pTest->m_pSec->m_Logging != 3 ) {

					return TRUE;
					}
				}

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::RestoreData(void)
{
	CTextStreamMemory Stream;

	if( Stream.OpenLoad(m_hSave, FALSE) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				UINT    uPos = m_pList->FindNamePos(Name);

				if( uPos < NOTHING ) {

					CTag *pTag = m_pList->GetItem(uPos);

					Tree.GetObject();

					while( !Tree.IsEndOfData() ) {

						CString          Prop  = Tree.GetName();

						CMetaData const *pMeta = pTag->FindMetaData(Prop);

						if( pMeta ) {

							UINT Type = pMeta->GetType();

							if( Type == metaObject || Type == metaVirtual ) {

								CItem * &pObj = pMeta->GetObject(pTag);

								if( Type == metaVirtual ) {

									if( pObj ) {

										pObj->Kill();

										delete pObj;

										pObj = NULL;
										}
									}

								if( TRUE ) {

									pTag->LoadProp(Tree, pMeta);
									}

								if( pObj ) {

									pObj->PostLoad();
									}
								}
							else
								pTag->LoadProp(Tree, pMeta);
							}
						}

					Tree.EndObject();
					}
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagImport::SortTags(CString Folder)
{
	CArray <INDEX> List;

	INDEX   Index = m_pList->GetHead();

	CString Match = Folder + L".";

	while( !m_pList->Failed(Index) ) {

		CMetaItem *pItem = (CMetaItem *) m_pList->GetItem(Index);

		if( !wstrncmp(pItem->GetName(), Match, Match.GetLength()) ) {

			List.Append(Index);
			}

		m_pList->GetNext(Index);
		}

	if( !List.IsEmpty() ) {

		UINT uCount = List.GetCount();

		m_pSort = m_pList;

		m_nSort = +1;

		qsort( PVOID(List.GetPointer()),
		       uCount,
		       sizeof(INDEX),
		       SortFunc
		       );

		for( UINT n = 0; n < uCount; n++ ) {

			m_pList->MoveItem(List[n], NULL);
			}

		m_pList->RemakeIndex();

		return TRUE;
		}

	return FALSE;
	}

// Sort Function

int CTagImport::SortFunc(PCVOID p1, PCVOID p2)
{
	INDEX i1 = *((INDEX *) p1);

	INDEX i2 = *((INDEX *) p2);

	CMetaItem *m1 = (CMetaItem *) m_pSort->GetItem(i1);

	CMetaItem *m2 = (CMetaItem *) m_pSort->GetItem(i2);

	int f1 = m1->IsKindOf(AfxRuntimeClass(CTagFolder));

	int f2 = m2->IsKindOf(AfxRuntimeClass(CTagFolder));

	if( f1 == f2 ) {

		CString n1 = m1->GetName();

		CString n2 = m2->GetName();

		UINT    l1 = n1.GetLength();

		UINT    l2 = n2.GetLength();

		UINT	d1, d2;

		for( d1 = 0;; d1++ ) {

			if( !isdigit(n1[l1-1-d1]) ) {

				break;
				}
			}

		for( d2 = 0;; d2++ ) {

			if( !isdigit(n2[l2-1-d2]) ) {

				break;
				}
			}

		if( l1 - d1 == l2 - d2 ) {

			int q1 = l1 - d1;

			int q2 = l2 - d2;

			if( n1.Left(q1) == n2.Left(q2) ) {

				int v1 = watoi(PCTXT(n1) + q1);

				int v2 = watoi(PCTXT(n2) + q2);

				if( v1 < v2 ) return -m_nSort;

				if( v1 > v2 ) return +m_nSort;

				return 0;
				}
			}

		return m_nSort * lstrcmp(n1, n2);
		}

	return (f1 < f2 ) ? +1 : -1;
	}

// End of File
