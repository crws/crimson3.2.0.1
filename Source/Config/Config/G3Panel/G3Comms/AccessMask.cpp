
#include "Intern.hpp"

#include "AccessMask.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "AccessMaskPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Access Mask Item
//

// Dynamic Class

AfxImplementDynamicClass(CAccessMask, CUIItem);

// Constructor

CAccessMask::CAccessMask(void)
{
	m_fDefault  = TRUE;

	m_fDispPage = FALSE;

	m_uSpecify  = 4;

	m_uDefault  = 3;
	}

// UI Creation

BOOL CAccessMask::OnLoadPages(CUIPageList *pList)
{
	CUIPage *pPage = New CAccessMaskPage(this);

	pList->Append(pPage);

	return FALSE;
	}

// UI Update

void CAccessMask::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Mode" ) {

		BOOL fEnable  = (m_Mode == m_uSpecify);

		BOOL fDefault = (m_Mode == m_uDefault);

		for( UINT n = 0; n < 30; n ++ ) {

			CPrintf Name(L"Right%2.2u", n);

			if( pHost->FindUI(Name) ) {

				BOOL fAllow = fEnable;

				if( n == 29 ) {

					if( m_fDispPage ) {

						pHost->EnableUI(Name, FALSE);

						continue;
						}

					fAllow = !fDefault;
					}

				if( n == 28 ) {

					if( m_fDispPage ) {

						pHost->EnableUI(Name, FALSE);

						continue;
						}

					fAllow = !fDefault && m_Mode > 0;
					}

				if( !fAllow && m_Rights[n] ) {

					m_Rights[n] = 0;

					pHost->UpdateUI(Name);
					}

				pHost->EnableUI(Name, fAllow);
				}
			}
		}
	}

// Attributes

BOOL CAccessMask::IsDefault(void)
{
	return m_fDefault;
	}

// Operations

void CAccessMask::BlockDefault(void)
{
	m_fDefault = FALSE;

	m_uSpecify = 3;

	m_uDefault = NOTHING;
	}

void CAccessMask::SetDispPage(void)
{
	m_fDispPage = TRUE;
	}

void CAccessMask::LoadData(UINT uData)
{
	m_Mode = (uData >> 30);

	for( UINT n = 0; n < elements(m_Rights); n++ ) {

		if( n == 28 || n == 29 ) {

			if( m_fDispPage ) {

				if( n == 29 ) {

					m_Rights[n] = 1;
					}
				else
					m_Rights[n] = 0;
				}
			else {
				if( uData & (1 << n) ) {

					m_Rights[n] = 1;
					}
				else
					m_Rights[n] = 0;
				}
			}
		else {
			if( !m_Mode && (uData & (1 << n)) ) {

				m_Rights[n] = 1;
				}
			else
				m_Rights[n] = 0;
			}
		}

	if( !m_Mode && (uData & ~(allowProgram | allowCheck)) ) {

		m_Mode = m_uSpecify;
		}
	}

UINT CAccessMask::ReadData(void)
{
	UINT uData = 0;

	if( m_Mode < m_uSpecify ) {

		if( !m_fDispPage ) {

			if( m_Rights[28] ) {

				uData |= allowCheck;
				}

			if( m_Rights[29] ) {

				uData |= allowProgram;
				}
			}

		uData |= (m_Mode << 30);
		}
	else {
		for( UINT n = 0; n < elements(m_Rights); n++ ) {

			if( m_fDispPage && (n == 28 || n == 29) ) {

				continue;
				}

			if( m_Rights[n] ) {

				uData |= (1 << n);
				}
			}
		}

	return uData;
	}

// Meta Data Creation

void CAccessMask::AddMetaData(void)
{
	CUIItem::AddMetaData();

	for( UINT n = 0; n < elements(m_Rights); n++ ) {

		Meta_Add( CPrintf(L"Right%2.2u", n),
			  m_Rights[n],
			  metaInteger
			  );
		}

	Meta_AddInteger(Mode);
	}

// End of File
