
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsMapReg_HPP

#define INCLUDE_CommsMapReg_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsMapBlock;
class CCommsMapRegList;
class CCommsMappingList;

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping Register
//

class DLLNOT CCommsMapReg : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsMapReg(void);

		// Item Location
		CCommsMapRegList * GetParentList(void) const;
		CCommsMapBlock   * GetParentBlock(void) const;
		CCommsDevice     * GetParentDevice(void) const;

		// Attributes
		BOOL    IsMapped    (void) const;
		BOOL    IsBroken    (void) const;
		UINT    GetTreeImage(void) const;
		CString GetTreeLabel(void) const;
		CString GetAddrText (void) const;

		// Operations
		BOOL Validate(BOOL fExpand);
		void Collapse(void);
		void Expand(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT		    m_Bits;
		CCommsMappingList * m_pMaps;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
