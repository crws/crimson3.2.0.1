
#include "Intern.hpp"

#include "DispColor.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispColorFixed.hpp"
#include "DispColorFlag.hpp"
#include "DispColorMulti.hpp"
#include "DispColorLinked.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Display Color
//

// Dynamic Class

AfxImplementDynamicClass(CDispColor, CCodedHost);

// Class Enumeration

CLASS CDispColor::GetClass(UINT uType)
{
	switch( uType ) {

		case 1:
			return AfxRuntimeClass(CDispColorFixed);

		case 2:
			return AfxRuntimeClass(CDispColorFlag);

		case 3:
			return AfxRuntimeClass(CDispColorMulti);

		case 4:
			return AfxRuntimeClass(CDispColorLinked);
		}

	return NULL;
	}

// Constructor

CDispColor::CDispColor(void)
{
	m_uType = 0;
	}

// UI Update

void CDispColor::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( !Tag.IsEmpty() ) {

		CItem *pParent = pItem->GetParent();

		if( pParent->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

			CUIItem *pUI = (CUIItem *) pParent;

			pUI->OnUIChange(pHost, pItem, L"ColorObject");
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CDispColor::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_uType));

	CCodedHost::MakeInitData(Init);

	return TRUE;
	}

// Attributes

UINT CDispColor::GetColType(void) const
{
	return m_uType;
	}

// Color Access

DWORD CDispColor::GetColorPair(DWORD Data, UINT Type)
{
	return MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));
	}

COLOR CDispColor::GetForeColor(DWORD Data, UINT Type)
{
	return LOWORD(GetColorPair(Data, Type));
	}

COLOR CDispColor::GetBackColor(DWORD Data, UINT Type)
{
	return HIWORD(GetColorPair(Data, Type));
	}

// Property Preservation

BOOL CDispColor::Preserve(CDispColor *pOld)
{
	return FALSE;
	}

// Meta Data Creation

void CDispColor::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_SetName((IDS_COLOR_PAIR));
	}

// Implementation

UINT CDispColor::ImportColor(UINT uData)
{
	BYTE bFore = LOBYTE(uData);

	BYTE bBack = HIBYTE(uData);

	WORD wFore = C3GetGdiColor(C3GetPaletteEntry(bFore));

	WORD wBack = C3GetGdiColor(C3GetPaletteEntry(bBack));

	return MAKELONG(wFore, wBack);
	}

// End of File
