
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextExprIPAddress_HPP

#define INCLUDE_UITextExprIPAddress_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextCoded.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable IP Address
//

class CUITextExprIPAddress : public CUITextCoded
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextExprIPAddress(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		BOOL    IsNumberConst(CString const &Text);
		CString Format(UINT uData);
		UINT    Parse(CError &Error, CString Text);
	};

// End of File

#endif
