
#include "Intern.hpp"

#include "DataTag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsSystem.hpp"
#include "DataServer.hpp"
#include "DispColor.hpp"
#include "DispFormat.hpp"
#include "NameServer.hpp"
#include "PersistManager.hpp"
#include "SecDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Tag
//

// Dynamic Class

AfxImplementDynamicClass(CDataTag, CTag);

// Constructor

CDataTag::CDataTag(void)
{
	m_Extent   = 0;

	m_RdMode   = 0;

	m_Access   = 0;

	m_Persist  = 0;

	m_Addr     = 0;

	m_Alloc    = GetAllocSize();

	m_FormLock = 0;

	m_FormType = 0;

	m_ColType  = 0;

	m_fIntern  = TRUE;

	m_pOnWrite = NULL;

	m_pSec     = NULL;
	}

// Server Access

INameServer * CDataTag::GetNameServer(CNameServer *pName)
{
	UINT uPos = m_Name.FindRev('.');

	if( uPos < NOTHING ) {

		CString Root = m_Name.Left(uPos);

		pName->SetRoot(Root);
		}

	return pName;
	}

IDataServer * CDataTag::GetDataServer(CDataServer *pData)
{
	return pData;
	}

// UI Update

void CDataTag::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "FormType" ) {

		if( !pHost->InReplay() ) {

			CLASS Class = CDispFormat::GetClass(m_FormType);

			if( Class == AfxPointerClass(m_pFormat) ) {

				return;
				}

			HGLOBAL hPrev = m_pFormat ? m_pFormat->TakeSnapshot() : NULL;

			SetFormatClass(m_pFormat, Class);

			HGLOBAL hData = m_pFormat ? m_pFormat->TakeSnapshot() : NULL;

			CCmd *  pCmd  = New CCmdSubItem(L"Format", hPrev, hData);

			pHost->SaveExtraCmd(pCmd);
			}

		pHost->SendUpdate(updateValue);

		pHost->RemakeUI();
		}

	if( Tag == "ColType" ) {

		if( !pHost->InReplay() ) {

			CLASS Class = CDispColor::GetClass(m_ColType);

			if( Class == AfxPointerClass(m_pColor) ) {

				return;
				}

			HGLOBAL hPrev = m_pColor ? m_pColor->TakeSnapshot() : NULL;

			SetColorClass(m_pColor, Class);

			HGLOBAL hData = m_pColor ? m_pColor->TakeSnapshot() : NULL;

			CCmd *  pCmd  = New CCmdSubItem(L"Color", hPrev, hData);

			pHost->SaveExtraCmd(pCmd);
			}

		pHost->RemakeUI();
		}

	if( Tag == "Extent" ) {

		UpdateAddr(FALSE);
		}

	if( Tag == "Persist" ) {

		UpdateAddr(FALSE);
		}

	if( Tag == "Length" ) {

		UpdateAddr(FALSE);
		}

	if( Tag == "Value" ) {

		m_fIntern = !m_pValue;
		}

	if( Tag == "Sim" ) {

		pHost->SendUpdate(updateValue);
		}

	if( m_Persist && m_Addr == 0 ) {

		m_Persist = 0;

		pHost->UpdateUI("Persist");

		afxMainWnd->Error(CString(IDS_PERSISTENT_DATA));
		}

	CTag::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

UINT CDataTag::GetCommsStep(void)
{
	UINT uStep = GetCommsSize();

	return max(uStep, 1);
	}

// Operations

void CDataTag::UpdateAddr(BOOL fReg)
{
	if( m_Persist ) {

		UINT uSize = GetAllocSize();

		if( !m_Addr ) {

			m_Addr  = PersistAllocate(uSize);

			m_Alloc = uSize;
			}
		else {
			if( m_Alloc != uSize ) {

				PersistFree(m_Addr, m_Alloc);

				m_Addr  = PersistAllocate(uSize);

				m_Alloc = uSize;
				}

			if( fReg ) {

				PersistRegister(m_Addr, uSize);
				}
			}
		}
	else {
		if( m_Addr ) {

			PersistFree(m_Addr, m_Alloc);

			m_Addr = 0;
			}
		}
	}

void CDataTag::RemapPersist(void)
{
	m_Addr = 0;

	UpdateAddr(TRUE);
	}

void CDataTag::UpdateExtent(void)
{
	if( m_pValue && m_pValue->IsCommsRef() ) {

		UINT uSize = GetCommsSize();

		if( uSize ) {

			m_pValue->UpdateExtent(uSize);
			}
		}
	}

// Item Naming

CString CDataTag::GetItemOrdinal(void) const
{
	return CPrintf(IDS_TAG_FMT, GetIndex());
	}

// Property Save Filter

BOOL CDataTag::SaveProp(CString const &Tag) const
{
	if( Tag == L"Addr" || Tag == L"Alloc" ) {

		return m_Persist ? TRUE : FALSE;
		}

	return CTag::SaveProp(Tag);
	}

// Persistance

void CDataTag::PostPaste(void)
{
	CTag::PostPaste();

	m_Addr  = 0;

	m_Alloc = 0;

	UpdateAddr(TRUE);

	if( m_Persist && !m_Addr ) {

		m_Persist = 0;
		}

	UpdateExtent();
	}

void CDataTag::PostLoad(void)
{
	CTag::PostLoad();

	m_fIntern  = !m_pValue;

	m_FormType = m_pFormat ? m_pFormat->GetFormType() : 0;

	m_ColType  = m_pColor  ? m_pColor ->GetColType () : 0;

	UpdateAddr(TRUE);
	}

void CDataTag::Kill(void)
{
	m_Persist = 0;

	UpdateAddr(FALSE);

	CTag::Kill();
	}

// Download Support

BOOL CDataTag::MakeInitData(CInitData &Init)
{
	CTag::MakeInitData(Init);

	Init.AddWord(WORD(m_Extent));

	Init.AddByte(BYTE(m_Access));

	Init.AddByte(BYTE(m_RdMode));

	Init.AddLong(LONG(m_Addr));

	Init.AddWord(WORD(GetCommsSize()));

	return TRUE;
	}

// Meta Data

void CDataTag::AddMetaData(void)
{
	CTag::AddMetaData();

	Meta_AddInteger(Extent);
	Meta_AddInteger(RdMode);
	Meta_AddInteger(Access);
	Meta_AddInteger(Persist);
	Meta_AddInteger(Addr);
  	Meta_AddInteger(Alloc);
	Meta_AddInteger(FormLock);
	Meta_AddInteger(FormType);
	Meta_AddInteger(ColType);

	Meta_SetName((IDS_DATA_TAG));
	}

// Memory Sizing

UINT CDataTag::GetAllocSize(void)
{
	return sizeof(C3INT);
	}

UINT CDataTag::GetCommsSize(void)
{
	return m_Extent;
	}

// Memory Allocation

void CDataTag::PersistRegister(DWORD dwAddr, UINT uSize)
{
	FindSystem()->m_pPersist->Register(dwAddr, uSize);
	}

DWORD CDataTag::PersistAllocate(UINT uSize)
{
	return FindSystem()->m_pPersist->Allocate(uSize);
	}

void CDataTag::PersistFree(DWORD dwAddr, UINT uSize)
{
	FindSystem()->m_pPersist->Free(dwAddr, uSize);
	}

// Implementation

BOOL CDataTag::MakeReadOnly(IUIHost *pHost)
{
	if( m_Access != 2 ) {

		m_Access = 2;

		Recompile(pHost);

		pHost->UpdateUI(this, "Access");

		return TRUE;
		}

	return FALSE;
	}

BOOL CDataTag::CheckPersist(IUIHost *pHost)
{
	if( m_Persist ) {

		if( !m_pValue ) {

			return TRUE;
			}

		if( m_pValue->IsCommsRef() ) {

			if( m_Access == 1 ) {

				return TRUE;
				}
			}

		m_Persist = 0;

		UpdateAddr(FALSE);

		pHost->UpdateUI("Persist");

		return FALSE;
		}

	return TRUE;
	}

BOOL CDataTag::CheckOnWrite(IUIHost *pHost)
{
	if( m_pOnWrite ) {

		m_pOnWrite->Recompile();

		pHost->UpdateUI(L"OnWrite");

		return TRUE;
		}

	return FALSE;
	}

// End of File
