
#include "Intern.hpp"

#include "WebPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "HttpServerOptions.hpp"
#include "TagSet.hpp"
#include "WebServer.hpp"

////////////////////////////////////////////////////////////////////////
//
// Web Page Item
//

// Dynamic Class

AfxImplementDynamicClass(CWebPage, CCodedHost);

// Constructor

CWebPage::CWebPage(void)
{
	m_Handle  = HANDLE_NONE;
	m_pTitle  = NULL;
	m_Refresh = 1;
	m_Colors  = 0;
	m_Manual  = 0;
	m_Delay   = 5;
	m_Edit    = 0;
	m_Hide    = 0;
	m_PageSec = allowDefault;
	m_pSet    = New CTagSet;
	}

// UI Management

CViewWnd * CWebPage::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return CreateItemView(FALSE);
		}

	return NULL;
	}

// UI Update

void CWebPage::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Manual" ) {

		DoEnables(pHost);
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CWebPage::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Title" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Item Naming

CString CWebPage::GetHumanName(void) const
{
	return m_Name;
	}

// Persistance

void CWebPage::Load(CTreeFile &Tree)
{
	CCodedHost::Load(Tree);

	m_Refresh = !!m_Refresh;
	}

// Download Support

BOOL CWebPage::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pTitle);

	Init.AddByte(BYTE(m_Refresh));

	Init.AddByte(BYTE(m_Colors));

	Init.AddByte(BYTE(m_Edit));

	Init.AddByte(BYTE(m_Hide));

	Init.AddWord(WORD(m_Manual ? m_Delay : 0));

	Init.AddLong(m_PageSec);

	Init.AddItem(itemSimple, m_pSet);

	return TRUE;
	}

// Meta Data Creation

void CWebPage::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddString (Name);
	Meta_AddVirtual(Title);
	Meta_AddInteger(Refresh);
	Meta_AddInteger(Manual);
	Meta_AddInteger(Delay);
	Meta_AddInteger(Colors);
	Meta_AddInteger(Edit);
	Meta_AddInteger(Hide);
	Meta_AddInteger(PageSec);
	Meta_AddObject (Set);

	Meta_SetName((IDS_WEB_PAGE));
	}

// Implementation

void CWebPage::DoEnables(IUIHost *pHost)
{
	CWebServer *pServer = (CWebServer *) GetParent(AfxRuntimeClass(CWebServer));

	pHost->EnableUI("Delay",   m_Manual);

	pHost->EnableUI("PageSec", pServer->m_pOpts->m_AuthMethod && pServer->m_Source);
	}

// End of File
