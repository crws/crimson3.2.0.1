
#include "Intern.hpp"

#include "EthernetRoutes.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Routing Table
//

// Dynamic Class

AfxImplementDynamicClass(CEthernetRoutes, CUIItem);

// Constructor

CEthernetRoutes::CEthernetRoutes(void)
{
	memset(m_Use,    0, sizeof(m_Use));

	memset(m_Dest,   0, sizeof(m_Dest));

	memset(m_Mask,   0, sizeof(m_Mask));

	memset(m_Gate,   0, sizeof(m_Gate));

	memset(m_Metric, 0, sizeof(m_Metric));
	}

// UI Update

void CEthernetRoutes::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Use") ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Property Save Filter

BOOL CEthernetRoutes::SaveProp(CString const &Tag) const
{
	TCHAR c = Tag[Tag.GetLength()-1];

	if( isdigit(c) ) {

		return m_Use[c - '0'];
		}

	return CUIItem::SaveProp(Tag);
	}

// Download Support

BOOL CEthernetRoutes::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	for( UINT n = 0; n < elements(m_Dest); n++ ) {

		Init.AddByte(BYTE(m_Use[n]));

		if( m_Use[n] ) {

			Init.AddLong(m_Dest[n]);
			Init.AddLong(m_Mask[n]);
			Init.AddLong(m_Gate[n]);

			Init.AddWord(WORD(m_Metric[n]));
			}
		}

	return TRUE;
	}

// Meta Data Creation

void CEthernetRoutes::AddMetaData(void)
{
	CUIItem::AddMetaData();

	for( UINT n = 0; n < elements(m_Dest); n++ ) {

		Meta_Add(CPrintf(L"Use%u",    n), m_Use   [n], metaInteger);
		Meta_Add(CPrintf(L"Dest%u",   n), m_Dest  [n], metaInteger);
		Meta_Add(CPrintf(L"Mask%u",   n), m_Mask  [n], metaInteger);
		Meta_Add(CPrintf(L"Gate%u",   n), m_Gate  [n], metaInteger);
		Meta_Add(CPrintf(L"Metric%u", n), m_Metric[n], metaInteger);
		}

	Meta_SetName((IDS_ROUTING_TABLE));
	}

// Implementation

void CEthernetRoutes::DoEnables(IUIHost *pHost)
{
	for( UINT n = 0; n < elements(m_Dest); n++ ) {

		pHost->EnableUI(CPrintf(L"Dest%u"   , n), m_Use[n]);
		pHost->EnableUI(CPrintf(L"Mask%u"   , n), m_Use[n]);
		pHost->EnableUI(CPrintf(L"Gate%u"   , n), m_Use[n]);
		pHost->EnableUI(CPrintf(L"Metric%u" , n), m_Use[n]);
		}
	}

// End of File
