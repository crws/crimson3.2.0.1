
#include "Intern.hpp"

#include "UITagControl.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Tag Control
//

// Dynamic Class

AfxImplementDynamicClass(CUITagControl, CUIExpression);

// Constructor

CUITagControl::CUITagControl(void)
{
	m_fComms = TRUE;

	m_fComp  = FALSE;

	m_fTags  = TRUE;

	m_fEmpty = TRUE;

	m_fExpr  = TRUE;
	}

// End of File
