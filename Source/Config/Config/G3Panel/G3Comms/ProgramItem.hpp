
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramItem_HPP

#define INCLUDE_ProgramItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CProgramCodeItem;
class CProgramPrototype;

//////////////////////////////////////////////////////////////////////////
//
// Program Item
//

class DLLNOT CProgramItem : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramItem(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Location
		CCommsSystem * FindSystem(void) const;

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		void PostLoad(void);

		// Parameter Access
		UINT         GetParameterCount(void);
		CFuncParam * GetParameterList(void);

		// Attributes
		UINT GetTreeImage(void) const;
		BOOL IsPending(void) const;
		BOOL IsBroken(void) const;
		UINT GetDebugFlags(void) const;
		BOOL StoreInFile(void) const;

		// Operations
		BOOL Create(CString Prot, CString Code);
		BOOL Translate(CError &Error, BOOL fSave);
		void SetPending(void);
		BOOL CheckSaveCode(void);
		BOOL CheckKillCode(void);
		BOOL CheckKillPrev(void);
		BOOL CheckCodeChange(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT                m_Handle;
		CString             m_Name;
		UINT		    m_Pending;
		CProgramCodeItem  * m_pCode;
		CProgramPrototype * m_pProt;
		UINT	            m_BkGnd;
		UINT	            m_Comms;
		UINT	            m_Timeout;
		UINT	            m_Run;
		UINT		    m_Private;
		UINT		    m_DebugMode;
		UINT		    m_DebugFlags;

	protected:
		// Data Members
		CString m_Prev;
		UINT64  m_Time;

		// Meta Data Creation
		void AddMetaData(void);

		// External Filename
		CFilename GetFilename(CFilename Base);

		// Implementation
		void   DoEnables(IUIHost *pHost);
		void   UpdateCodeType(void);
		void   UpdatePending(void);
		BOOL   TestCode(void);
		BOOL   TestCode(CFilename Base);
		BOOL   LoadCode(void);
		BOOL   LoadCode(CFilename Base);
		BOOL   LoadCode(HANDLE hFile);
		BOOL   LoadCodeFromString(CString Code);
		BOOL   SaveCode(void);
		BOOL   SaveCode(CFilename Base);
		BOOL   KillCode(void);
		BOOL   KillCode(CFilename Base);
		BOOL   KillPrev(void);
		BOOL   KillPrev(CFilename Base);
		UINT64 GetFileTime(HANDLE hFile);
	};

// End of File

#endif
