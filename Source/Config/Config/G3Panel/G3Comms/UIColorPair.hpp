
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIColorPair_HPP

#define INCLUDE_UIColorPair_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CColorComboBox;

class CColorSample;

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Color Pair
//

class CUIColorPair : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIColorPair(void);

	protected:
		// Data Members
		CString	         m_Label;
		BOOL             m_fLockBack;
		CLayItemText   * m_pTextLayout;
		CLayItemText   * m_pWordLayout;
		CLayItemText   * m_pSampLayout;
		CLayItem       * m_pForeLayout;
		CLayItem       * m_pBackLayout;
		CStatic	       * m_pTextCtrl;
		CStatic	       * m_pWordCtrl;
		CColorSample   * m_pSampCtrl;
		CColorComboBox * m_pForeCtrl;
		CColorComboBox * m_pBackCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnShow(BOOL fShow);
		void OnMove(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect FindForeRect(void);
		CRect FindBackRect(void);
		CRect FindSampRect(void);
		void  LoadLists(void);
	};

// End of File

#endif
