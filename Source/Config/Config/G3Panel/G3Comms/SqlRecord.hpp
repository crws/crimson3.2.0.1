
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlRecord_HPP

#define INCLUDE_SqlRecord_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Record
//

class CSqlRecord : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CSqlRecord(void);
		CSqlRecord(UINT uRow, UINT uCol);

		// UI Loading
		CViewWnd * CreateView(UINT uType);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);
		BOOL GetTypeData(CTypeDef &Type);

		// Tag Mapping
		BOOL SetMapping(CError &Error, CString const &Text);
		void ClearMapping(void);
		BOOL IsMapped(void);
		void Validate(BOOL fExpand);

		// Tree Helpers
		CString GetTreeLabel(void);

		// Item Naming
		CString GetHumanName(void) const;

		// Data Members
		CCodedItem   * m_pValue;
		CString        m_Name;
		UINT           m_Row;
		UINT           m_Col;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		CString       GetMapSource(BOOL fExpand);
		INameServer * GetNameServer(BOOL fExpand);
		void          SendUpdate(void);
	};

// End of File

#endif
