
#include "Intern.hpp"

#include "SqlFilterList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SqlFilter.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter List
//

// Dynamic Class

AfxImplementDynamicClass(CSqlFilterList, CItemList);

// Constructor

CSqlFilterList::CSqlFilterList(void)
{
	}

// Item Access

CSqlFilter * CSqlFilterList::GetItem(INDEX Index) const
{
	return (CSqlFilter *) CItemList::GetItem(Index);
	}

CSqlFilter * CSqlFilterList::GetItem(UINT uPos) const
{
	return (CSqlFilter *) CItemList::GetItem(uPos);
	}

// End of File
