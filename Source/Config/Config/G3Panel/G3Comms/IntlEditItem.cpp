
#include "Intern.hpp"

#include "IntlEditItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "IntlEditItemPage.hpp"
#include "IntlString.hpp"
#include "IntlStringList.hpp"
#include "LangManager.hpp"
#include "WebTranslationDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// International String Editing Item
//

// Dynamic Class

AfxImplementDynamicClass(CIntlEditItem, CUIItem);

// Constructor

CIntlEditItem::CIntlEditItem(void)
{
	m_pList = New CIntlStringList;
	}

// UI Overridables

void CIntlEditItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "ButtonAuto" ) {

		if( !m_pDbase->IsReadOnly() ) {

			UINT uMode = IDNO;

			for( UINT n = 1; n < m_pList->GetItemCount(); n++ ) {

				if( !m_pList->GetItem(n)->m_Text.IsEmpty() ) {

					uMode = CWnd::GetActiveWindow().YesNoCancel(IDS_LEAVE_EXISTING);

					break;
					}
				}

			if( uMode != IDCANCEL ) {

				CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

				CLangManager *pLang   = pSystem->m_pLang;

				CStringArray  In;

				CUIntArray    To;

				for( UINT n = 1; n < m_pList->GetItemCount(); n++ ) {

					if( uMode == IDNO || m_pList->GetItem(n)->m_Text.IsEmpty() ) {

						if( pLang->IsSlotUsed(n) ) {

							To.Append(n);
							}
						}
					}

				if( To.GetCount() ) {

					In.Append(m_pList->GetItem(0U)->m_Text);

					CWebTranslationDialog Dlg(pLang, In, To);

					if( Dlg.Execute() ) {

						for( UINT n = 0; n < To.GetCount(); n++ ) {

							CString Out = Dlg.GetOutput(0, n);

							if( !Out.IsEmpty() ) {

								CIntlString *pItem = m_pList->GetItem(To[n]);

								pItem->m_Text = Out;

								pHost->UpdateUI(pItem, L"Text");
								}
							}
						}
					}
				else {
					CString Text = IDS_NOTHING_TO;

					CWnd::GetActiveWindow().Error(Text);
					}
				}
			}
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

BOOL CIntlEditItem::OnLoadPages(CUIPageList *pList)
{
	CIntlEditItemPage *pPage = New CIntlEditItemPage(this);

	pList->Append(pPage);

	return FALSE;
	}

// Operations

void CIntlEditItem::LoadStrings(CString Strings)
{
	if( !Strings.IsEmpty() ) {

		CStringArray List;

		Strings.Tokenize(List, LANG_SEP);

		for( UINT n = 0; n < List.GetCount(); n ++ ) {

			SetText(n, List[n]);
			}
		}
	}

CString CIntlEditItem::ReadStrings(void)
{
	CString t;

	CString d;

	UINT c = m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		if( n ) {

			t += LANG_SEP;

			CString s = GetText(n);

			if( !s.CompareC(d) ) {

				continue;
				}

			t += s;
			}
		else {
			d  = GetText(n);

			t += d;
			}
		}

	UINT i = t.GetLength();

	UINT j;

	for( j = 0; j < i; j++ ) {

		if( t[i-1-j] == LANG_SEP ) {

			continue;
			}

		break;
		}

	t.Delete(i - j, j);

	return t;
	}

// Persistance

void CIntlEditItem::Init(void)
{
	CUIItem::Init();

	m_pList->SetItemCount(20);
	}

// Meta Data Creation

void CIntlEditItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddCollect(List);
	}

// Implementation

CString CIntlEditItem::GetText(UINT n)
{
	AfxAssert(n < m_pList->GetItemCount());

	CIntlString *pItem = m_pList->GetItem(n);

	IDataAccess *pData = pItem->GetDataAccess(L"Text");

	AfxAssert(pData);

	return pData->ReadString(pItem);
	}

void CIntlEditItem::SetText(UINT n, CString Text)
{
	AfxAssert(n < m_pList->GetItemCount());

	CIntlString *pItem = m_pList->GetItem(n);

	IDataAccess *pData = pItem->GetDataAccess(L"Text");

	AfxAssert(pData);

	pData->WriteString(pItem, Text);
	}

// End of File
