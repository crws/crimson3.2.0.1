
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_WatchViewWnd_HPP

#define INCLUDE_WatchViewWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Watch View
//

class CWatchViewWnd : public CWnd, public IUpdate
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CWatchViewWnd(CCommsSystem *pSystem);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// Operations
		void Empty(void);
		BOOL Update(void);

	protected:
		// Data Members
		CCommsSystem * m_pSystem;
		CToolbarWnd  * m_pTool;
		CStatusWnd   * m_pStat;
		CListView    * m_pView;
		int            m_nTop;
		int	       m_nBotton;
		CSysProxy      m_Proxy;
		UINT           m_uItem;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnDestroy(void);
		void OnUpdateUI(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Wnd);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnKeyDown(UINT uCode, DWORD dwData);
		void OnGoingIdle(void);

		// Notification Handlers
		void OnItemChanged(UINT uID, NMLISTVIEW  &Info);
		BOOL OnViewKeyDown(UINT uID, NMLVKEYDOWN &Info);

		// Command Handlers
		BOOL OnGlobalCommand(UINT uID);
		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);
		void OnDelete(void);
		void OnEmpty(void);
		void OnGoOnline(void);

		// Comms Notifications
		BOOL OnCommsInit(UINT uID);
		BOOL OnCommsDone(UINT uID);
		BOOL OnCommsFail(UINT uID);
		BOOL OnCommsKill(UINT uID);
		BOOL OnCommsCred(UINT uID);

		// Implementation
		void Register(void);
		void CreateTool(CRect Tool);
		void CreateStat(CRect Stat);
		void CreateView(CRect View);
		void LoadCols(void);
		void SetColWidths(void);
		void RelayKey(UINT uCode);
		BOOL ShowSimData(BOOL fStat);
		BOOL ShowUnknown(void);
		BOOL DropLink(void);
	};

// End of File

#endif
