
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_IntlString_HPP

#define INCLUDE_IntlString_HPP

//////////////////////////////////////////////////////////////////////////
//
// International String	Item
//

class CIntlString : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIntlString(void);

		// Item Properties
		CString	m_Text;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
