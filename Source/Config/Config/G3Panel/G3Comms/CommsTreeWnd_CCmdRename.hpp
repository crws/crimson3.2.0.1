
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsTreeWnd_CCmdRename_HPP

#define INCLUDE_CommsTreeWnd_CCmdRename_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsTreeWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

class CCommsTreeWnd::CCmdRename : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdRename(CMetaItem *pItem, CString Name);

		// Data Members
		CString m_Prev;
		CString m_Name;
	};

// End of File

#endif
