
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortDevNet_HPP

#define INCLUDE_CommsPortDevNet_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Comms Port
//

class DLLNOT CCommsPortDevNet : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortDevNet(void);

		// Attributes
		UINT GetPageType(void) const;
		char GetPortTag(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Baud;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
