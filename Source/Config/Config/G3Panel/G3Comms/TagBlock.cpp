
#include "Intern.hpp"

#include "TagBlock.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "TagList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Block
//

// Runtime Class

AfxImplementRuntimeClass(CTagBlock, CMetaItem);

// Constructor

CTagBlock::CTagBlock(CTagList *pList, UINT uMin, UINT uMax)
{
	m_Handle = HANDLE_NONE;

	m_pList  = pList;

	m_uMin   = uMin;

	m_uMax   = uMax;
	}

// Download Support

BOOL CTagBlock::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_TAG_BLOCK);

	CMetaItem::MakeInitData(Init);

	UINT uCount = m_pList->m_Index.GetCount();

	UINT uMin   = min(m_uMin, uCount - 1);

	UINT uMax   = min(m_uMax, uCount - 0);

	Init.AddWord(WORD(uMin));

	Init.AddWord(WORD(uMax));

	for( UINT n = uMin; n < uMax; n++ ) {

		INDEX Index = m_pList->m_Index[n];

		if( Index ) {

			Init.AddByte(1);

			m_pList->m_List[Index]->MakeInitData(Init);
			}
		else
			Init.AddByte(0);
		}

	return TRUE;
	}

// Meta Data

void CTagBlock::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);

	Meta_SetName((IDS_TAG_BLOCK));
	}

// End of File
