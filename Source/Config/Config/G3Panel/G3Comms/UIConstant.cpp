
#include "Intern.hpp"

#include "UIConstant.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Constant
//

// Dynamic Class

AfxImplementDynamicClass(CUIConstant, CUIExpression);

// Constructor

CUIConstant::CUIConstant(void)
{
	m_fEmpty = FALSE;

	m_fComp  = FALSE;

	m_fComms = FALSE;

	m_fTags  = FALSE;
	}

// End of File
