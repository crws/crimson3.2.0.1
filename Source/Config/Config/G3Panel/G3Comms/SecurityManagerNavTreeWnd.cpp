
#include "Intern.hpp"

#include "SecurityManagerNavTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UserItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Security Manager Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CSecurityManagerNavTreeWnd, CNavTreeWnd);

// Constructor

CSecurityManagerNavTreeWnd::CSecurityManagerNavTreeWnd(void) : CNavTreeWnd( L"Users",
									    NULL,
									    AfxRuntimeClass(CUserItem)
									    )
{
	m_Empty     = CString(IDS_CLICK_ON_NEW_2);

	m_fInitRoot = TRUE;
	}

// Message Map

AfxMessageMap(CSecurityManagerNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchGetInfoType(IDM_ITEM,  OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM,  OnItemControl)
	AfxDispatchCommandType(IDM_ITEM,  OnItemCommand)

	AfxMessageEnd(CSecurityManagerNavTreeWnd)
	};

// Message Handlers

void CSecurityManagerNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"SecurityManagerNavTreeTool"));
		}
	}

// Command Handlers

BOOL CSecurityManagerNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Info.m_Image   = 0x40000008;

			Info.m_ToolTip = CString(IDS_NEW_USER);

			Info.m_Prompt  = CString(IDS_ADD_NEW_USER_TO);

			return TRUE;
		}

	return FALSE;
	}

BOOL CSecurityManagerNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Src.EnableItem(!IsReadOnly());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSecurityManagerNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			OnItemNew();

			return TRUE;
		}

	return FALSE;
	}

void CSecurityManagerNavTreeWnd::OnItemNew(void)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(CString(IDS_FORMAT_5), n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Class);

			m_System.ExecCmd(pCmd);

			break;
			}
		}
	}

// Tree Loading

void CSecurityManagerNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TagsTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CSecurityManagerNavTreeWnd::GetRootImage(void)
{
	return IDI_USER_MANAGER;
	}

UINT CSecurityManagerNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return IDI_USER;
	}

BOOL CSecurityManagerNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"SecurityManagerNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"SecurityManagerNavTreeMissCtxMenu";

	return FALSE;
	}

// End of File
