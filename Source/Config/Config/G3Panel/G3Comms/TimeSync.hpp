
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TimeSync_HPP

#define INCLUDE_TimeSync_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Time Sync Configuration
//

class CTimeSync : public CServiceItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTimeSync(void);

		// Initial Values
		void SetInitValues(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		BOOL IsEnabled(void) const;
		UINT GetTreeImage(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pExpose;
		CCodedItem  * m_pSyncUDP;
		CCodedItem  * m_pSyncDST;
		CCodedItem  * m_pDHCP;
		CCodedItem  * m_pServer;
		CCodedItem  * m_pSyncGPS;
		CCodedItem  * m_pSyncGSM;
		CCodedItem  * m_pDelay;
		CCodedItem  * m_pLogMode;
		CCodedItem  * m_pLogSync;
		CCodedItem  * m_pZulu;
		CCodedItem  * m_pLogSrc;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
