
#include "Intern.hpp"

#include "UISimpleExpression.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsManager.hpp"
#include "CommsSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Simple Expression
//

// Dynamic Class

AfxImplementDynamicClass(CUISimpleExpression, CUIExpression);

// Constructor

CUISimpleExpression::CUISimpleExpression(void)
{
	m_cfCode = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_fEmpty = FALSE;

	m_fComp  = TRUE;

	m_fComms = TRUE;

	m_fTags  = TRUE;
	}

// Category Overridables

BOOL CUISimpleExpression::LoadModeButton(void)
{
	CModeButton::COption Opt;

	m_pModeCtrl->ClearOptions();

	if( TRUE ) {

		Opt.m_uID     = modeGeneralWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fComp ) {

		Opt.m_uID     = modeComplexWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fEmpty ) {

		Opt.m_uID     = modeInternal;
		Opt.m_Text    = CString(IDS_INTERNAL);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeGeneral;
		Opt.m_Text    = CString(IDS_GENERAL);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fComp ) {

		Opt.m_uID     = modeComplex;
		Opt.m_Text    = CString(IDS_COMPLEX);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fTags ) {

		Opt.m_uID     = modeTag;
		Opt.m_Text    = CString(IDS_TAG);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);

		Opt.m_uID     = modeNewTag;
		Opt.m_Text    = CString(IDS_NEW_TAG);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fComms ) {

		UINT uMin = 1;

		UINT uMax = m_pComms->GetMaxDeviceNumber();

		BOOL fAdd = FALSE;

		for( UINT n = uMin; n <= uMax; n++ ) {

			CCommsDevice *pDev = m_pComms->FindDevice(n);

			if( pDev && pDev->GetDriver() ) {

				UINT uType = pDev->GetDriver()->GetType();

				if( uType == driverMaster || uType == driverHoneywell ) {

					if( !fAdd ) {

						if( TRUE ) {

							Opt.m_uID     = NOTHING;
							Opt.m_Text    = L"";
							Opt.m_fEnable = FALSE;
							Opt.m_fHidden = FALSE;
							Opt.m_Image   = 0;

							m_pModeCtrl->AddOption(Opt);
							}

						if( TRUE ) {

							Opt.m_uID     = modeNext;
							Opt.m_Text    = CString(IDS_NEXT);
							Opt.m_fEnable = FALSE;
							Opt.m_fHidden = FALSE;
							Opt.m_Image   = 0;

							m_pModeCtrl->AddOption(Opt);
							}

						if( TRUE ) {

							Opt.m_uID     = NOTHING;
							Opt.m_Text    = L"";
							Opt.m_fEnable = FALSE;
							Opt.m_fHidden = FALSE;
							Opt.m_Image   = 0;

							m_pModeCtrl->AddOption(Opt);
							}

						fAdd = TRUE;
						}

					Opt.m_uID     = modeDevice + n;
					Opt.m_Text    = pDev->GetName();
					Opt.m_fEnable = FALSE;
					Opt.m_fHidden = FALSE;
					Opt.m_Image   = 0;

					m_pModeCtrl->AddOption(Opt);
					}
				}
			}
		}

	return TRUE;
	}

// End of File
