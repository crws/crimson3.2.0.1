
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagString_HPP

#define INCLUDE_TagString_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DataTag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// String Tag Item
//

class DLLAPI CTagString : public CDataTag
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagString(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetTreeImage(void) const;
		UINT GetDataType(void) const;
		UINT GetTypeFlags(void) const;

		// Operations
		void UpdateTypes(BOOL fComp);

		// Reference Check
		BOOL RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag);

		// Circular Check
		void UpdateCircular(void);

		// Evaluation
		DWORD GetProp(WORD ID, UINT Type);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Encode;
		UINT m_Length;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Property Access
		DWORD FindAsText(void);
		DWORD FindLabel(void);
		DWORD FindFore(void);
		DWORD FindBack(void);

		// Memory Sizing
		UINT GetAllocSize(void);
		UINT GetCommsSize(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		UINT GetCharBits(void);
	};

// End of File

#endif
