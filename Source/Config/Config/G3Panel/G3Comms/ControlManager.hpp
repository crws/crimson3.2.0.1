
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ControlManager_HPP

#define INCLUDE_ControlManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProjectItem;

//////////////////////////////////////////////////////////////////////////
//
// Control Object
//

class DLLAPI CControlManager : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlManager(void);

		// Destructor
		~CControlManager(void);

		// Validation
		void Validate(void);

		// Persistance
		void Init(void);
		void PostInit(void);
		void PostLoad(void);

		// Conversion
		void PostConvert(void);

		// Project Build
		BOOL NeedBuild(void);
		BOOL PerformBuild(void);
		BOOL HasControl(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT           m_Handle;
		CProjectItem * m_pProject;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL CheckProject(void);
	};

// End of File

#endif
