
#include "Intern.hpp"

#include "CommsSysBlock.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"

#include "CommsManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Block
//

// Dynamic Class

AfxImplementDynamicClass(CCommsSysBlock, CUIItem);

// Constructor

CCommsSysBlock::CCommsSysBlock(void)
{
	m_Number = 0;
	m_Named  = 0;
	m_Space  = 0;
	m_Factor = 1;
	m_Align  = 0;
	m_Index  = 0;
	m_Base   = 0;
	m_Size   = 0;
	}

// Item Location

CCommsDevice * CCommsSysBlock::GetParentDevice(void) const
{
	return (CCommsDevice *) GetParent(2);
	}

// Attributes

BOOL CCommsSysBlock::GetBlockType(CTypeDef &Type) const
{
	C3GetTypeInfo(m_Space >> 12, Type);

	if( !IsNamed() ) {

		Type.m_Flags |= flagCommsTab;
		}

	return TRUE;
	}

DWORD CCommsSysBlock::GetRefAddress(CDataRef const &Ref) const
{
	if( IsNamed() ) {

		if( UINT(Ref.b.m_Offset) < m_List.GetCount() ) {

			WORD Lo = WORD(m_List[Ref.b.m_Offset]);

			WORD Hi = WORD(m_Space);

			return MAKELONG(Lo, Hi);
			}

		return 0;
		}

	WORD Lo = WORD(m_Base + Ref.b.m_Offset * m_Factor);

	WORD Hi = WORD(m_Space);

	return MAKELONG(Lo, Hi);
	}

BOOL CCommsSysBlock::IsNamed(void) const
{
	return m_Named;
	}

// Operations

BOOL CCommsSysBlock::AcceptAddress(CAddress Addr, INT nCount, DWORD &ID, UINT uPass, BOOL fIsNamedArray)
{
	UINT ReqSpace = HIWORD((DWORD &) Addr);

	INT  ReqIndex = LOWORD((DWORD &) Addr);

	if( m_Size ) {

		if( ReqSpace == m_Space ) {

			INT MaxSize = 16300;

			if( IsNamed() ) {

				if( nCount == 1 ) {

					INT c = m_List.GetCount();

					INT n;

					for( n = 0; n < c; n++ ) {

						if( m_List[n] == ReqIndex ) {

							break;
							}
						}

					if( n == c ) {

						if( uPass == 0 || n >= MaxSize ) {

							return FALSE;
							}

						WORD Index = WORD(ReqIndex);

						m_List.Append(Index);

						m_Size = c + 1;
						}

					CDataRef & Ref = (CDataRef &) ID;

					Ref.b.m_Block  = m_Number;

					Ref.b.m_Offset = n;

					return TRUE;
					}

				else if( fIsNamedArray ) {

					WORD Index = WORD(ReqIndex);

					BOOL fAppend;

					for( INT i = 0; i < nCount; i++, Index++ ) {

						fAppend = TRUE;

						INT c = m_List.GetCount();

						for( INT j = 0; j < c; j++ ) {

							if( uPass == 0 || j >= MaxSize ) {

								return FALSE;
								}

							if( m_List[j] == Index ) {

								fAppend = FALSE;
								break;
								}
							}

						if( fAppend ) {

							m_List.Append(Index);
							}
						}

					m_Size = m_List.GetCount();

					CDataRef & Ref = (CDataRef &) ID;

					Ref.b.m_Offset = nCount;

					Ref.b.m_Block = m_Number;

					return TRUE;
					}
				else {
					AfxAssert(FALSE);

					return FALSE;
					}
				}
			else {
				// LATER -- This alignment check is nasty. It is needed so we can
				// work with unaligned addresses, but if someone uses an aligned
				// and unaligned access to the same data, we'll not have a single
				// copy and we might get some race conditions

				if( ReqIndex % m_Factor == m_Align ) {

					INT Act1 = m_Index;

					INT Req1 = ReqIndex;

					INT Act2 = m_Index  + m_Size * m_Factor;

					INT Req2 = ReqIndex + nCount * m_Factor;

					if( Req1 >= Act1 && Req2 <= Act2 ) {

						// Handle entirely in range requests.

						CDataRef & Ref = (CDataRef &) ID;

						Ref.b.m_Offset = (ReqIndex - m_Base) / m_Factor;

						Ref.b.m_Block  = m_Number;

						return TRUE;
						}

					if( uPass == 0 ) {

						// Handle one end in range requests.

						if( Req1 >= Act1 && Req1 < Act2 ) {

							uPass = 2;
							}

						if( Req2 > Act1 && Req2 <= Act2 ) {

							uPass = 2;
							}
						}

					if( uPass >= 1 ) {

						// Handle requests which grow the block.

						INT Step = (uPass == 2) ? MaxSize : (64 + nCount);

						INT New1 = Min(Req1, Act1);

						INT New2 = Max(Req2, Act2);

						INT Size = (New2 - New1) / m_Factor;

						if( Size >= MaxSize ) {

							return FALSE;
							}

						if( Size <= m_Size + Step ) {

							CDataRef & Ref = (CDataRef &) ID;

							Ref.b.m_Block  = m_Number;

							Ref.b.m_Offset = (ReqIndex - m_Base) / m_Factor;

							m_Index = New1;

							m_Size  = Size;

							return TRUE;
							}

						return FALSE;
						}
					}
				}
			}

		return FALSE;
		}
	else {
		m_Space = ReqSpace;

		if( IsNamed() ) {

			if( nCount > 1 && !fIsNamedArray) {

				AfxAssert(FALSE);

				return FALSE;
				}

			WORD Index = WORD(ReqIndex);

			m_List.Append(Index);

			m_Size = 1;
			}
		else {
			m_Index  = ReqIndex;

			m_Base   = ReqIndex;

			m_Size   = nCount;

			m_Factor = C3GetTypeScale(m_Space >> 12);

			m_Align  = m_Base % m_Factor;
			}

		CDataRef & Ref = (CDataRef &) ID;

		Ref.b.m_Block  = m_Number;

		Ref.b.m_Offset = 0;

		return TRUE;
		}
	}

// Persistance

void CCommsSysBlock::Init(void)
{
	CUIItem::Init();

	CCommsManager *pManager = (CCommsManager *) GetParent(AfxRuntimeClass(CCommsManager));

	m_Number = pManager->AllocBlockNumber();
	}

// Property Save Filter

BOOL CCommsSysBlock::SaveProp(CString const &Tag) const
{
	if( !IsNamed() ) {

		if( Tag == "List" ) {

			return FALSE;
			}
		}
	else {
		if( Tag == "Factor" || Tag == "Align" ) {

			return FALSE;
			}

		if( Tag == "Index" || Tag == "Base" ) {

			return FALSE;
			}
		}

	return CUIItem::SaveProp(Tag);
	}

// Download Support

BOOL CCommsSysBlock::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Number));
	Init.AddByte(BYTE(m_Named));
	Init.AddWord(WORD(m_Space));
	Init.AddByte(BYTE(m_Factor));
	Init.AddWord(WORD(m_Index));
	Init.AddWord(WORD(m_Base));
	Init.AddWord(WORD(m_Size));

	if( IsNamed() ) {

		for( INT n = 0; n < m_Size; n++ ) {

			Init.AddWord(WORD(m_List[n]));
			}
		}

	return TRUE;
	}

// Meta Data Creation

void CCommsSysBlock::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Number);
	Meta_AddInteger(Named);
	Meta_AddInteger(Space);
	Meta_AddInteger(Factor);
	Meta_AddInteger(Align);
	Meta_AddInteger(Index);
	Meta_AddInteger(Base);
	Meta_AddInteger(Size);

	Meta_AddWide(List);

	Meta_SetName((IDS_COMMS_BLOCK_2));
	}

// End of File
