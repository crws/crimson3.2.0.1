
#include "Intern.hpp"

#include "PersistManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "PersistPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Persistent Data Manager
//

// Dynamic Class

AfxImplementDynamicClass(CPersistManager, CMetaItem);

// Constructor

CPersistManager::CPersistManager(void)
{
	for( UINT n = 0; n < elements(m_pPage); n++ ) {

		m_pPage[n] = New CPersistPage(n + 1);
		}
	}

// Destructor

CPersistManager::~CPersistManager(void)
{
	for( UINT n = 0; n < elements(m_pPage); n++ ) {

		delete m_pPage[n];
		}
	}

// Operations

void CPersistManager::Register(DWORD dwAddr, UINT uSize)
{
	if( HIWORD(dwAddr) ) {

		UINT uPage = HIWORD(dwAddr) - 1;

		if( uPage < elements(m_pPage) ) {

			m_pPage[uPage]->Register(dwAddr, uSize);

			return;
			}
		}

	AfxAssert(FALSE);
	}

DWORD CPersistManager::Allocate(UINT uSize)
{
	UINT c = GetPageLimit();

	for( UINT n = 0; n < c; n++ ) {

		DWORD dwAddr = m_pPage[n]->Allocate(uSize);

		if( dwAddr ) {

			return dwAddr;
			}
		}

	return 0;
	}

void CPersistManager::Free(DWORD dwAddr, UINT uSize)
{
	if( HIWORD(dwAddr) ) {

		UINT uPage = HIWORD(dwAddr) - 1;

		if( uPage < elements(m_pPage) ) {

			m_pPage[uPage]->Free(dwAddr, uSize);

			return;
			}
		}

	AfxAssert(FALSE);
	}

void CPersistManager::Free(void)
{
	for( UINT n = 0; n < elements(m_pPage); n++ ) {

		m_pPage[n]->Free();
		}
	}

// Meta Data Creation

void CPersistManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();
	}

// Implementation

UINT CPersistManager::GetPageLimit(void)
{
	// TODO -- Make sure this matches the runtime!!!

	return 4;
	}

// End of File
