
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_EthernetItem_HPP

#define INCLUDE_EthernetItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPortList;
class CEthernetFace;
class CEthernetRoutes;

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Configuration
//

class DLLNOT CEthernetItem : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEthernetItem(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL HasDualPorts(void) const;

		// Operations
		void MakePorts(void);
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		void PostConvert(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CEthernetFace   * m_pFace0;
		CEthernetFace   * m_pFace1;
		UINT	          m_Routing;
		UINT	          m_Download;
		UINT	          m_DownPort;
		UINT		  m_SendPort;
		UINT		  m_UseSendPort;
		UINT		  m_DownMode;
		UINT		  m_DownIP;
		CEthernetRoutes * m_pRoutes;
		UINT		  m_EnableTls;
		UINT		  m_RootSource;
		CString		  m_RootFile;
		UINT		  m_ZeroEnable;
		CString		  m_ZeroName;
		CCommsPortList  * m_pPorts;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
