
#include "Intern.hpp"

#include "UIProgram.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Program Editor
//

// Dynamic Class

AfxImplementDynamicClass(CUIProgram, CUIControl);

// Constructor

CUIProgram::CUIProgram(void)
{
	m_pDataLayout = NULL;

	m_pDataCtrl   = New CSourceEditorWnd;
	}

// Core Overidables

void CUIProgram::OnBind(void)
{
	CUIControl::OnBind();

	m_pDataCtrl->Attach(m_pItem);
	}

void CUIProgram::OnRebind(void)
{
	CUIControl::OnRebind();

	m_pDataCtrl->Attach(m_pItem);
	}

void CUIProgram::OnLayout(CLayFormation *pForm)
{
	m_pDataLayout = New CLayItemSize(CSize(100, 200), CSize(4, 4), CSize(100, 100));

	m_pMainLayout = New CLayFormPad (m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem(New CLayItem());

	pForm->AddItem(m_pMainLayout);
	}

void CUIProgram::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pDataCtrl->Create( WS_CHILD | WS_TABSTOP | WS_CLIPCHILDREN,
			     GetDataWindowRect(),
			     Wnd,
			     uID++
			     );

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pDataCtrl, EN_KILLFOCUS);
	}

void CUIProgram::OnPosition(void)
{
	m_pDataCtrl->MoveWindow(GetDataWindowRect(), TRUE);
	}

// Data Overridables

void CUIProgram::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	m_pDataCtrl->SetText(Data);

	m_pDataCtrl->SetModify(FALSE);
	}

UINT CUIProgram::OnSave(BOOL fUI)
{
	if( m_pDataCtrl->GetModify() ) {

		CString Text = m_pDataCtrl->GetText();

		CString Last = m_pText->GetAsText();

		if( Text.CompareC(Last) ) {

			CError Error(fUI);

			UINT uCode = m_pText->SetAsText(Error, Text);

			if( !Error.IsOkay() ) {

				if( Error.AllowUI() ) {

					Error.Show(*afxMainWnd);
					}
				}

			return uCode;
			}
		}

	return saveSame;
	}

// Notification Handlers

BOOL CUIProgram::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pDataCtrl->GetID() ) {

		if( uCode == EN_CHANGE ) {

			CProgramItem *pCoded = (CProgramItem *) m_pItem;

			pCoded->SetPending();

			CSysProxy Sys;

			Sys.Bind((CViewWnd *) &m_pDataCtrl->GetParent());

			Sys.ItemUpdated(m_pItem, updateProps);
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Implementation

CRect CUIProgram::GetDataWindowRect(void)
{
	CRect Rect;

	Rect = m_pDataLayout->GetRect();

	Rect.top    -= 1;

	Rect.bottom += 1;

	return Rect;
	}

// End of File
