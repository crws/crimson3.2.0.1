
#include "Intern.hpp"

#include "OptionCardPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OptionCardItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Option Card Page
//

// Runtime Class

AfxImplementRuntimeClass(COptionCardPage, CUIStdPage);

// Constructor

COptionCardPage::COptionCardPage(COptionCardItem *pOpt)
{
	m_Class = AfxRuntimeClass(COptionCardItem);

	m_pOpt  = pOpt;
	}

// Operations

BOOL COptionCardPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	LoadBasePage(pView);

	LoadButtons(pView);

	pView->NoRecycle();

	return TRUE;
	}

// Implementation

BOOL COptionCardPage::LoadBasePage(IUICreate *pView)
{
	CUIData UIData;

	UIData.m_Tag       = L"Type";

	UIData.m_ClassText = AfxNamedClass(L"CUITextOptionCard");

	UIData.m_ClassUI   = AfxNamedClass(L"CUIPick");

	if( m_pOpt->GetDatabase()->HasFlag(L"USBRack") ) {

		pView->StartGroup(CString(IDS_MODULE_SELECTION), 1);

		UIData.m_Label = CString(IDS_MODULE);

		UIData.m_Local = CString(IDS_MODULE);

		UIData.m_Tip   = CString(IDS_SELECT_TYPE_OF);
		}
	else {
		pView->StartGroup(CString(IDS_CARD_SELECTION), 1);

		UIData.m_Label = CString(IDS_OPTION_CARD);

		UIData.m_Local = CString(IDS_OPTION_CARD);

		UIData.m_Tip   = CString(IDS_SELECT_TYPE_OF_2);
		}

	pView->AddUI(m_pOpt, L"root", &UIData);

	pView->EndGroup(TRUE);

	return TRUE;
	}

BOOL COptionCardPage::LoadButtons(IUICreate *pView)
{
	if( m_pOpt->GetDatabase()->HasFlag(L"USBRack") ) {

		pView->StartGroup( CString(IDS_MODULE_COMMANDS),
				   1
				   );

		pView->AddButton(  CString(IDS_REMOVE_MODULE),
				   CString(IDS_REMOVE_MODULE_IN),
				   L"ButtonRemoveCard"
				   );
		}
	else {
		pView->StartGroup( CString(IDS_CARD_COMMANDS),
				   1
				   );

		pView->AddButton(  CString(IDS_COMMS_OPT_DEL),
				   CString(IDS_REMOVE_THIS),
				   L"ButtonRemoveCard"
				   );
		}

	pView->EndGroup(FALSE);

	return TRUE;
	}

// End of File
