
#include "Intern.hpp"

#include "CommsPortPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPort.hpp"
#include "CommsPortNetwork.hpp"
#include "CommsPortSerial.hpp"
#include "CommsPortVirtual.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

// Runtime Class

AfxImplementRuntimeClass(CCommsPortPage, CUIStdPage);

// Constructor

CCommsPortPage::CCommsPortPage(CCommsPort *pPort)
{
	m_Class = AfxRuntimeClass(CCommsPort);

	m_pPort = pPort;
}

// Operations

BOOL CCommsPortPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadDriverConfig(pView);

	LoadPortConfig(pView);

	LoadButtons(pView);

	if( m_pPort->IsKindOf(AfxRuntimeClass(CCommsPortSerial)) ) {

		CCommsPortSerial *pSerial = (CCommsPortSerial *) m_pPort;

		if( pSerial->m_fAux ) {

			pView->StartGroup(CString(IDS_NOTE), 1);

			pView->AddNarrative(CString(IDS_SETTINGS_FOR_AUX));

			pView->EndGroup(FALSE);
		}
	}

	pView->NoRecycle();

	return TRUE;
}

// Implementation

BOOL CCommsPortPage::LoadDriverConfig(IUICreate *pView)
{
	if( m_pPort->m_pConfig ) {

		CUIPageList List;

		m_pPort->m_pConfig->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, m_pPort->m_pConfig);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsPortPage::LoadPortConfig(IUICreate *pView)
{
	UINT uPage = m_pPort->GetPageType();

	if( uPage ) {

		CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pPort), uPage);

		pPage->LoadIntoView(pView, m_pPort);

		delete pPage;

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPortPage::LoadButtons(IUICreate *pView)
{
	BOOL fNetwork = FALSE;

	BOOL fVirtual = FALSE;

	if( m_pPort->GetDatabase()->GetSoftwareGroup() >= SW_GROUP_3B ) {

		fNetwork = m_pPort->IsKindOf(AfxRuntimeClass(CCommsPortNetwork));

		fVirtual = m_pPort->IsKindOf(AfxRuntimeClass(CCommsPortVirtual));
	}

	pView->StartGroup(CString(IDS_PORT_COMMANDS), 1);

	if( fNetwork ) {

		pView->AddButton(CString(IDS_DELETE_NETWORK),
				 CString(IDS_DELETE_CURRENT_1),
				 L"ButtonDeletePort"
		);
	}

	if( fVirtual ) {

		pView->AddButton(CString(IDS_DELETE_VIRTUAL),
				 CString(IDS_DELETE_CURRENT_2),
				 L"ButtonDeletePort"
		);
	}

	pView->AddButton(CString(IDS_COMMS_PORT_CLEAR),
			 CString(IDS_COMMS_TIP_CLEAR),
			 L"ButtonClearPort"
	);

	pView->AddButton(CString(IDS_COMMS_PORT_ADD),
			 CString(IDS_COMMS_TIP_ADD),
			 L"ButtonAddDevice"
	);

	pView->EndGroup(FALSE);

	return TRUE;
}

// End of File
