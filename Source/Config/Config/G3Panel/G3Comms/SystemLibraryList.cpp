
#include "Intern.hpp"

#include "SystemLibraryList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Library List
//

// Dynamic Class

AfxImplementDynamicClass(CSystemLibraryList, CItemIndexList);

// Constructor

CSystemLibraryList::CSystemLibraryList(void)
{
	}

// Item Access

CMetaItem * CSystemLibraryList::GetItem(INDEX Index) const
{
	return (CMetaItem *) CItemIndexList::GetItem(Index);
	}

CMetaItem * CSystemLibraryList::GetItem(UINT uPos) const
{
	return (CMetaItem *) CItemIndexList::GetItem(uPos);
	}

// Item Lookup

UINT CSystemLibraryList::FindNamePos(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CMetaItem *pItem = GetItem(n);

		if( pItem->GetName() == Name ) {

			return pItem->GetIndex();
			}

		GetNext(n);
		}

	return NOTHING;
	}

// End of File
