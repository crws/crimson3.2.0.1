
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsDeviceList_HPP

#define INCLUDE_CommsDeviceList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;

class CCommsSysBlock;

//////////////////////////////////////////////////////////////////////////
//
// Comms Device List
//

class DLLAPI CCommsDeviceList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsDeviceList(void);

		// Item Access
		CCommsDevice * GetItem(INDEX   Index) const;
		CCommsDevice * GetItem(UINT    uPos ) const;
		CCommsDevice * GetItem(CString Name ) const;

		// Item Lookup
		CCommsDevice   * FindDevice(UINT uDevice) const;
		CCommsDevice   * FindDevice(CString Name) const;
		CCommsSysBlock * FindBlock (UINT uBlock ) const;

		// Operations
		void DeleteAll(void);
		void UpdateDriver(void);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		BOOL CheckMapBlocks(void);
		void Validate(BOOL fExpand);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

// End of File

#endif
