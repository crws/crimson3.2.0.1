
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsMapBlock_HPP

#define INCLUDE_CommsMapBlock_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsMapBlockList;
class CCommsMapRegList;

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block
//

class DLLNOT CCommsMapBlock : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsMapBlock(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Location
		CCommsMapBlockList * GetParentList(void) const;
		CCommsDevice       * GetParentDevice(void) const;

		// Driver Access
		ICommsDriver * GetDriver(void) const;
		CItem        * GetConfig(void) const;
		CItem	     * GetDriverConfig(void) const;

		// Attributes
		BOOL IsBroken(void) const;
		UINT GetTreeImage(void) const;
		UINT GetDataType(void) const;
		UINT GetBitCount(void) const;
		BOOL IsWriteBlock(void) const;
		BOOL IsSlaveBlock(void) const;

		// Operations
		BOOL Validate(BOOL fExpand);
		void ImportMappings(CTextStreamMemory &Stream);
		void ExportMappings(CTextStreamMemory &Stream);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CString             m_Name;
		CAddress	    m_Addr;
		CString		    m_Text;
		UINT		    m_Type;
		UINT		    m_Size;
		UINT		    m_Write;
		UINT		    m_Scaled;
		UINT		    m_Update;
		UINT		    m_Period;
		UINT		    m_Slave;
		CCodedItem        * m_pReq;
		CCodedItem        * m_pAck;
		CCommsMapRegList  * m_pRegs;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
