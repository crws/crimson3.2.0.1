
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextExprNumeric_HPP

#define INCLUDE_UITextExprNumeric_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextCoded.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable General
//

class CUITextExprNumeric : public CUITextCoded
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextExprNumeric(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

// End of File

#endif
