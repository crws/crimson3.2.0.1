
#include "Intern.hpp"

#include "UITextExprIPAddress.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable IP Address
//

// Dynamic Class

AfxImplementDynamicClass(CUITextExprIPAddress, CUITextCoded);

// Constructor

CUITextExprIPAddress::CUITextExprIPAddress(void)
{
	}

// Overridables

void CUITextExprIPAddress::OnBind(void)
{
	CUITextCoded::OnBind();

	if( m_UIData.m_Format.IsEmpty() ) {

		m_uWidth = 20;

		ShowTwoWay();
		}
	else {
		m_UIData.m_Status += CFormat(IDS_THIS_PROPERTY_CAN, m_UIData.m_Label);

		m_UIData.m_Status += L" ";

		m_UIData.m_Status += CString(IDS_TO_USE_HOSTNAME);

		m_uFlags |= textStatus;
		}
	}

CString CUITextExprIPAddress::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.GetLength() ) {

		if( IsNumberConst(Text) ) {

			UINT uData = wcstoul(Text.Mid(2), NULL, 16);

			return Format(uData);
			}

		return L'=' + Text;
		}

	return Text;
	}

UINT CUITextExprIPAddress::OnSetAsText(CError &Error, CString Text)
{
	if( Text.GetLength() ) {

		if( Text[0] == '=' ) {

			Text.Delete(0, 1);

			if( Text.IsEmpty() ) {

				Error.Set(CString(IDS_YOU_MUST_ENTER));

				return saveError;
				}

			return CUITextCoded::OnSetAsText(Error, Text);
			}

		UINT uData = Parse(Error, Text);

		if( Error.IsOkay() ) {

			CString Prev  = CUITextCoded::OnGetAsText();

			if( IsNumberConst(Prev) ) {

				UINT uPrev = wcstoul(Prev.Mid(2), NULL, 16);

				if( uData - uPrev ) {

					CString Expr = CPrintf(L"0x%X", uData);

					return CUITextCoded::OnSetAsText(Error, Expr);
					}

				return saveSame;
				}

			CString Expr = CPrintf(L"0x%X", uData);

			return CUITextCoded::OnSetAsText(Error, Expr);
			}

		return saveError;
		}

	return saveError;
	}

// Implementation

BOOL CUITextExprIPAddress::IsNumberConst(CString const &Text)
{
	PCTXT p = Text;

	if( p[0] == '0' && p[1] == 'x' ) {

		if( *++++p ) {

			PTXT e;

			// cppcheck-suppress ignoredReturnValue

			wcstoul(p, &e, 16);

			return !*e;
			}
		}

	return FALSE;
	}

CString CUITextExprIPAddress::Format(UINT uData)
{
	BYTE b1 = PBYTE(&uData)[0];
	BYTE b2 = PBYTE(&uData)[1];
	BYTE b3 = PBYTE(&uData)[2];
	BYTE b4 = PBYTE(&uData)[3];

	return CPrintf(L"%u.%u.%u.%u", b4, b3, b2, b1);
	}

UINT CUITextExprIPAddress::Parse(CError &Error, CString Text)
{
	CStringArray List;

	Text.Tokenize(List, '.');

	if( List.GetCount() == 4 ) {

		PTXT p1 = NULL;
		PTXT p2 = NULL;
		PTXT p3 = NULL;
		PTXT p4 = NULL;

		UINT u1 = wcstoul(List[0],&p1,10);
		UINT u2 = wcstoul(List[1],&p2,10);
		UINT u3 = wcstoul(List[2],&p3,10);
		UINT u4 = wcstoul(List[3],&p4,10);

		if( !*p1 && !*p2 && !*p3 && !*p4 ) {

			if( u1 < 256 && u2 < 256 && u3 < 256 && u4 < 256 ) {

				UINT uData = 0;

				((PBYTE) &uData)[0] = BYTE(u4);
				((PBYTE) &uData)[1] = BYTE(u3);
				((PBYTE) &uData)[2] = BYTE(u2);
				((PBYTE) &uData)[3] = BYTE(u1);

				return uData;
				}
			}
		}

	Error.Set(CString(IDS_INVALID_IP));

	return 0;
	}

// End of File
