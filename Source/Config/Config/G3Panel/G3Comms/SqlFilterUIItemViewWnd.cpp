
#include "Intern.hpp"

#include "SqlFilterUIItemViewWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SqlFilterList.hpp"
#include "SqlFilter.hpp"
#include "SqlQuery.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Item View Window
//

// Constructor

CSqlFilterUIItemViewWnd::CSqlFilterUIItemViewWnd(CSqlQuery *pQuery, CUIPageList *pList, BOOL fScroll)
	: CUIItemViewWnd(pList, fScroll)
{
	m_pQuery = pQuery;
	}

// UI Management

void CSqlFilterUIItemViewWnd::OnUIChange(CItem *pItem, CString Tag)
{
	CUIItemViewWnd::OnUIChange(pItem, Tag);

	if( Tag.IsEmpty() ) {		

		DoEnables(pItem);
		}

	if( Tag == L"Operator" ) {

		DoEnables(pItem);
		}

	if( Tag == L"Column" ) {

		UpdateUI(pItem, L"Value");
		}
	}

// Implementation

void CSqlFilterUIItemViewWnd::DoEnables(CItem *pItem)
{
	INDEX Filter = m_pQuery->m_pFilters->FindItemIndex(pItem);

	INDEX First  = m_pQuery->m_pFilters->GetHead();

	CSqlFilter *pFilter = (CSqlFilter *) pItem;

	EnableUI(pItem, L"Value", pFilter->NeedsOperand());

	EnableUI(pItem, L"Binding", Filter != First);
	}

// End of File
