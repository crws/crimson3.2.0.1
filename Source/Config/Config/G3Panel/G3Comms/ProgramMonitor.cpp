
#include "Intern.hpp"

#include "ProgramMonitor.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// External Program Change Monitor
//

// Runtime Class

AfxImplementRuntimeClass(CProgramMonitor, CRawThread);

// Constructor

CProgramMonitor::CProgramMonitor(CFilename const &Path)
{
	m_pWnd = NULL;

	m_Path = Path;

	m_FileEvent = FindFirstChangeNotification( m_Path.Left(m_Path.GetLength()-1),
						   FALSE,
						   FILE_NOTIFY_CHANGE_FILE_NAME  |
						   FILE_NOTIFY_CHANGE_LAST_WRITE |
						   FILE_NOTIFY_CHANGE_SIZE
						   );
	MakeList();
	}

// Destructor

CProgramMonitor::~CProgramMonitor(void)
{
	if( m_FileEvent.IsValid() ) {

		FindCloseChangeNotification(m_FileEvent);

		m_FileEvent.Detach();
		}
	}

// Attributes

BOOL CProgramMonitor::GetChangeList(CStringArray &List)
{
	if( m_FileEvent.IsValid() ) {

		m_Mutex.WaitForObject(INFINITE);

		List.Append(m_Changes);

		m_Changes.Empty();

		m_Mutex.Release();
		}

	return !List.IsEmpty();
	}

// Operations

void CProgramMonitor::SetWindow(CWnd *pWnd)
{
	m_pWnd = pWnd;
	}

// Overridables

UINT CProgramMonitor::OnExec(void)
{
	if( m_FileEvent.IsValid() ) {

		CWaitableList List( m_TermEvent,
				    m_FileEvent
				    );

		for(;;) {

			List.WaitForAnyObject();

			if( List.GetObjectIndex() == 0 ) {

				return 0;
				}

			if( List.GetObjectIndex() == 1 ) {

				for(;;) {

					FindNextChangeNotification(m_FileEvent);

					if( List.WaitForAnyObject(0) == waitTimeout ) {

						break;
						}

					if( List.GetObjectIndex() == 0 ) {

						return 0;
						}
					}

				m_Mutex.WaitForObject(INFINITE);

				TestList();

				if( !m_Changes.IsEmpty() ) {

					if( m_pWnd ) {

						m_pWnd->PostMessage(WM_AFX_COMMAND, IDM_PROGRAM_CHANGED);
						}
					}

				m_Mutex.Release();
				}
			}

		return 0;
		}

	m_TermEvent.WaitForObject();

	return 0;
	}

// Implmentation

void CProgramMonitor::MakeList(void)
{
	if( m_FileEvent.IsValid() ) {

		m_Map.Empty();

		WIN32_FIND_DATA Data;

		HANDLE hFind = FindFirstFile(m_Path + L"*.c", &Data);

		if( hFind != INVALID_HANDLE_VALUE ) {

			do {
				WIN32_FILE_ATTRIBUTE_DATA Attr;

				CString Name = m_Path + Data.cFileName;

				if( GetFileAttributesEx(Name, GetFileExInfoStandard, &Attr) ) {

					DWORD Time = Attr.ftLastWriteTime.dwLowDateTime;

					m_Map.Insert(Name, Time);
					}

				} while( FindNextFile(hFind, &Data) );

			FindClose(hFind);
			}
		}
	}

BOOL CProgramMonitor::TestList(void)
{
	CTree <CString> Read;

	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(m_Path + L"*.c", &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			WIN32_FILE_ATTRIBUTE_DATA Attr;

			CString Name = m_Path + Data.cFileName;

			if( GetFileAttributesEx(Name, GetFileExInfoStandard, &Attr) ) {

				DWORD Time  = Attr.ftLastWriteTime.dwLowDateTime;

				INDEX Index = m_Map.FindName(Name);

				if( m_Map.Failed(Index) ) {

					m_Map.Insert(Name, Time);

					m_Changes.Append(Name);
					}
				else {
					if( m_Map.GetData(Index) != Time ) {

						m_Map.SetData(Index, Time);

						m_Changes.Append(Name);
						}
					}

				Read.Insert(Name);
				}

			} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
		}

	if( TRUE ) {

		INDEX Index = m_Map.GetHead();

		while( !m_Map.Failed(Index) ) {

			INDEX   Next = Index;

			CString Name = m_Map.GetName(Index);

			m_Map.GetNext(Next);

			if( Read.Failed(Read.Find(Name)) ) {

				m_Map.Remove(Index);

				m_Changes.Append(Name);
				}

			Index = Next;
			}
		}

	return TRUE;
	}

// End of File
