
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_EthernetPage_HPP

#define INCLUDE_EthernetPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CEthernetItem;

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Page
//

class CEthernetPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEthernetPage(CEthernetItem *pNet, UINT uPage);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CEthernetItem * m_pNet;
		UINT            m_uPage;
	};

// End of File

#endif
