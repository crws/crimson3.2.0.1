
#include "Intern.hpp"

#include "UITextDataLog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "DataLog.hpp"
#include "DataLogger.hpp"
#include "DataLogList.hpp"
#include "ItemCreateDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Data Log Selection
//

// Dynamic Class

AfxImplementDynamicClass(CUITextDataLog, CUITextCoded);

// Constructor

CUITextDataLog::CUITextDataLog(void)
{
	m_uFlags |= textEnum | textRefresh | textExpand;

	m_Verb    = CString(IDS_NEW);
	}

// Overridables

void CUITextDataLog::OnBind(void)
{
	m_pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	m_pList   = m_pSystem->m_pLog->m_pLogs;

	CUITextElement::OnBind();

	CheckWas();
	}

CString CUITextDataLog::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.IsEmpty() ) {

		return IDS_COMMS_NONE;
		}

	if( Text.StartsWith(L"0x") ) {

		UINT uPos = wcstoul(PCTXT(Text) + 2, NULL, 16);

		if( uPos ) {

			CDataLog *pLog = m_pList->GetItem(uPos-1);

			if( pLog ) {

				return pLog->m_Name;
				}
			}
		}

	return Text;
	}

UINT CUITextDataLog::OnSetAsText(CError &Error, CString Text)
{
	if( Text == CString(IDS_COMMS_NONE) ) {

		return CUITextCoded::OnSetAsText(Error, L"");
		}

	return CUITextCoded::OnSetAsText(Error, Text);
	}

BOOL CUITextDataLog::OnEnumValues(CStringArray &List)
{
	if( !m_Was.IsEmpty() ) {

		List.Append(m_Was);
		}

	if( TRUE ) {

		List.Append(CString(IDS_COMMS_NONE));
		}

	if( TRUE ) {

		for( INDEX n = m_pList->GetHead(); !m_pList->Failed(n); m_pList->GetNext(n) ) {

			CDataLog *pLog = m_pList->GetItem(n);

			if( pLog ) {

				if( pLog->IsKindOf(AfxRuntimeClass(CDataLog)) ) {

					List.Append(pLog->m_Name);
					}
				}
			}
		}

	return TRUE;
	}

BOOL CUITextDataLog::OnExpand(CWnd &Wnd)
{
	CError Error(TRUE);

	if( OnNewLog(Error) == saveError ) {

		return FALSE;
		}

	return TRUE;
	}

// Implementation

BOOL CUITextDataLog::CheckWas(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.StartsWith(L"WAS ") ) {

		m_Was = Text;

		return TRUE;
		}

	return FALSE;
	}

UINT CUITextDataLog::OnNewLog(CError &Error)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CDataLogger  *pLog    = pSystem->m_pLog;

	CDataLogList *pList   = pLog->m_pLogs;

	CItemCreateDialog Dlg(pList, typeLog);

	if( Dlg.Execute() ) {

		CString Name = Dlg.GetName();

		if( !pLog->CreateLog(Name) ) {

			CWnd::GetActiveWindow().Error(CString(IDS_LOG_COULD_NOT_BE));

			return saveError;
			}

		return CUITextCoded::OnSetAsText(Error, Name);
		}

	return saveSame;
	}

// End of File
