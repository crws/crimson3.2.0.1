
#include "Intern.hpp"

#include "CommsPortJ1939.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// J1939 Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortJ1939, CCommsPortCAN);

// Constructor

CCommsPortJ1939::CCommsPortJ1939(void)
{
	m_Binding = bindJ1939;

	m_Baud    = 125000;
	}

// Overridables

CHAR CCommsPortJ1939::GetPortTag(void) const
{
	return 'J';
}

// End of File
