
#include "Intern.hpp"

#include "ProgramItemView.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramCatWnd.hpp"

#include "ProgramCodeItem.hpp"

#include "ProgramItem.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Program Item View
//

// Dynamic Class

AfxImplementDynamicClass(CProgramItemView, CUIItemMultiWnd);

// Constructor

CProgramItemView::CProgramItemView(void) : CUIItemMultiWnd(NULL)
{
	CLASS Class = AfxRuntimeClass(CProgramItem);

	m_pList     = New CUIPageList;

	m_pList->Append(New CUIStdPage(CString(IDS_SOURCE),     Class, 1));

	m_pList->Append(New CUIStdPage(CString(IDS_PROPERTIES), Class, 2));
	}

// Item Access

CProgramItem * CProgramItemView::GetProgram(void)
{
	return (CProgramItem *) m_pItem;
	}

// Attributes

BOOL CProgramItemView::CanTranslate(void)
{
	return m_pItem->IsPending() || (m_pItem->m_pCode && m_pItem->m_pCode->HasError());
	}

// Operations

BOOL CProgramItemView::Translate(void)
{
	ForceUpdate();

	AfxNull(CWnd).SetFocus();

	CError Error(TRUE);

	if( !m_pItem->Translate(Error, TRUE) ) {

		Error.Show(ThisObject);

		CRange Range( m_pItem->m_pCode->m_From,
			      m_pItem->m_pCode->m_To
			      );

		m_pEditor->SetSel(Range);

		m_pEditor->SetFocus();

		m_System.ItemUpdated(m_pItem, updateProps);

		return FALSE;
		}

	((CUIViewWnd *) m_Views[0])->UpdateUI(L"Code");

	m_pEditor->SetFocus();

	m_System.ItemUpdated(m_pItem, updateProps);

	return TRUE;
	}

void CProgramItemView::Reload(void)
{
	((CUIViewWnd *) m_Views[0])->UpdateUI();
	}

// Overridables

void CProgramItemView::OnAttach(void)
{
	FindItem();

	m_pItem = (CProgramItem *) CUIItemMultiWnd::m_pItem;

	AttachViews();
	}

// Message Map

AfxMessageMap(CProgramItemView, CUIItemMultiWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)

	AfxMessageEnd(CUIItemMultiWnd)
	};

// Message Handlers

void CProgramItemView::OnPostCreate(void)
{
	m_System.Bind(this);

	CProgramCatWnd *pCat = (CProgramCatWnd *) m_System.GetCatView(AfxRuntimeClass(CProgramCatWnd));

	pCat->SetItemView(this);

	SetTabStyle(0);

	MakeTabs();

	m_DropHelp.Bind(m_hWnd, this);

	CMultiViewWnd::OnPostCreate();

	FindEditor();
	}

// Implementation

void CProgramItemView::MakeTabs(void)
{
	for( UINT n = 0; n < m_pList->GetCount(); n++ ) {

		CUIPage    *pPage = m_pList->GetEntry(n);

		CUIViewWnd *pView = New CUIItemViewWnd(pPage);

		if( n == 0 ) {

			pView->NoScroll();
			}

		AddView(pPage->GetTitle(), pView);
		}

	AttachViews();
	}

BOOL CProgramItemView::FindEditor(void)
{
	CWnd *pWnd   = m_Views[0];

	CWnd *pChild = pWnd->GetWindowPtr(GW_CHILD);

	while( pChild->IsWindow() ) {

		if( pChild->IsKindOf(AfxRuntimeClass(CSourceEditorWnd)) ) {

			m_pEditor = (CSourceEditorWnd *) pChild;

			return TRUE;
			}

		pChild = pChild->GetWindowPtr(GW_HWNDNEXT);
		}

	return FALSE;
	}

void CProgramItemView::ForceUpdate(void)
{
	SendMessage( WM_COMMAND,
		     WPARAM(MAKELONG(m_pEditor->GetID(), IDM_UI_CHANGE)),
		     LPARAM(m_pEditor->GetHandle())
		     );
	}

// End of File
