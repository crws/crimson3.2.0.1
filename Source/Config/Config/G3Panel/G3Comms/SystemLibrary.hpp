
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SystemLibrary_HPP

#define INCLUDE_SystemLibrary_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CNameServer;
class CSystemLibraryList;

//////////////////////////////////////////////////////////////////////////
//
// System Library Resource
//

class CSystemLibrary : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSystemLibrary(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Operations
		void ReadLibrary(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Data Members
		CSystemLibraryList *m_pLib;

	protected:
		// Data Members
		CNameServer *m_pServer;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void LoadVariables(void);
		void LoadFunctions(void);
		void FindNameServer(void);
		BOOL FindFunction(CString &Name, WORD ID, CTypeDef &Type, UINT &uCount);
		BOOL FindIdent(CString Name, WORD &ID, CTypeDef &Type);
		void AddCategory(CString Cat);
	};

// End of File

#endif
