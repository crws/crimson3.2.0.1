
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITagExtent_HPP

#define INCLUDE_UITagExtent_HPP

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Array Extent
//

class CUITagExtent : public CUICategorizer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITagExtent(void);

	protected:
		// Category Overridables
		BOOL    LoadModeButton(void);
		BOOL    SwitchMode(UINT uMode);
		UINT    FindDispMode(CString Text);
		CString FindDispText(CString Text, UINT uMode);
		CString FindDataText(CString Text, UINT uMode);
		UINT    FindDispType(UINT uMode);
		CString FindDispVerb(UINT uMode);
	};

// End of File

#endif
