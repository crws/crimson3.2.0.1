
#include "Intern.hpp"

#include "UITextCommsDriver.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPort.hpp"

#include "CommsSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Driver Selection
//

// Dynamic Class

AfxImplementDynamicClass(CUITextCommsDriver, CUITextEnum);

// Constructor

CUITextCommsDriver::CUITextCommsDriver(void)
{
	m_uFlags = textEnum | textExpand | textAllowWAS;
	}

// Overridables

void CUITextCommsDriver::OnBind(void)
{
	CUITextElement::OnBind();

	// We don't load the enumeration list at this point as
	// it takes forever. Instead, we only load on a save, and
	// we expand existing values directly from the driver.

	m_pItem  = (CCommsPort *) CUITextEnum::m_pItem;

	m_uWidth = 30;
	}

void CUITextCommsDriver::OnRebind(void)
{
	CUITextElement::OnRebind();

	m_pItem = (CCommsPort *) CUITextEnum::m_pItem;
	}

BOOL CUITextCommsDriver::OnExpand(CWnd &Wnd)
{
	UINT uIdent    = m_pItem->m_DriverID;

	UINT uBinding  = m_pItem->m_Binding;

	UINT uBindMask = m_pItem->m_BindMask;

	CDatabase    *pDbase  = m_pItem->GetDatabase();

	CCommsSystem *pSystem = (CCommsSystem *) pDbase->GetSystemItem();

	UINT          uGroup  = pDbase->GetSoftwareGroup();

	CString       Model   = pSystem->GetModel();

	C3AllowDriverGroup(uGroup, Model);

	if( C3SelectDriver(*afxMainWnd, uIdent, uBinding, uBindMask) ) {

		m_pData->WriteInteger(m_pItem, uIdent);

		return TRUE;
		}

	return FALSE;
	}

UINT CUITextCommsDriver::OnSetAsText(CError &Error, CString Text)
{
	LoadEnum();

	return CUITextEnum::OnSetAsText(Error, Text);
	}

CString CUITextCommsDriver::OnGetAsText(void)
{
	UINT id = m_pData->ReadInteger(m_pItem);

	if( id ) {

		ICommsDriver *pDriver = C3CreateDriver(id);

		if( pDriver ) {

			CString Text = GetDriverName(pDriver);

			pDriver->Release();

			if( m_pItem->IsBroken() ) {

				Text = L"WAS " + Text;
				}

			return Text;
			}

		return L"";
		}

	return CString(IDS_COMMS_DRIVER_NO);
	}

// Implementation

CString CUITextCommsDriver::GetDriverName(ICommsDriver *pDriver)
{
	CString Text = pDriver->GetString(stringManufacturer);

	if( Text[0] == '<' ) {

		Text =  pDriver->GetString(stringDriverName);
		}
	else {
		Text += L" ";

		Text += pDriver->GetString(stringDriverName);
		}

	return Text;
	}

void CUITextCommsDriver::LoadEnum(void)
{
	if( m_Enum.IsEmpty() ) {

		UINT n = 0;

		m_Enum.Expand(400);

		for(;;) {

			ICommsDriver *pDriver = C3EnumDrivers(n++);

			if( pDriver ) {

				CString Text = GetDriverName(pDriver);

				CEntry  Enum;

				Enum.m_Data = pDriver->GetID();

				Enum.m_Text = Text;

				// cppcheck-suppress uninitStructMember

				m_Enum.Append(Enum);

				pDriver->Release();

				continue;
				}

			break;
			}

		CEntry Enum;

		Enum.m_Data = 0;

		Enum.m_Text = CString(IDS_COMMS_DRIVER_NO);

		// cppcheck-suppress uninitStructMember

		m_Enum.Append(Enum);
		}
	}

// End of File
