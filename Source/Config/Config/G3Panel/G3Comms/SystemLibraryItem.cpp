
#include "Intern.hpp"

#include "SystemLibraryItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "NameServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Library Item
//

// Dynamic Class

AfxImplementDynamicClass(CSystemLibraryItem, CMetaItem);

// Constructor

CSystemLibraryItem::CSystemLibraryItem(void)
{
	m_Ident = NOTHING;

	m_Image = 18;

	m_fAct  = FALSE;
	}

// Persistance

void CSystemLibraryItem::Init(void)
{
	CMetaItem::Init();

	CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	m_pServer             = pSystem->GetNameServer();
	}

// Operations

void CSystemLibraryItem::MakeCode(void)
{
	CString  Name;

	CTypeDef Type;

	UINT     uCount;

	if( FindFunction(Name, Type, uCount) ) {

		if( Type.m_Flags & flagActive ) {

			m_fAct = TRUE;
			}

		m_Prot += TextFromType(Type.m_Type) + L" ";

		m_Prot += Name;

		m_Code += Name;

		m_Prot += L"(";

		m_Code += L"(";

		if( uCount ) {

			for( UINT n = 0; n < uCount; n ++ ) {

				WORD ID = WORD(m_Ident);

				CString Param;

				CTypeDef Type;

				m_pServer->GetFuncParam(ID, n, Param, Type);

				if( n ) {

					m_Prot += L", ";

					m_Code += L", ";
					}

				m_Prot += TextFromType(Type.m_Type);

				m_Code += DataFromType(Type.m_Type);
				}
			}
		else
			m_Prot += L"void";

		m_Prot += L")";

		m_Code += L")";
		}
	}

// Meta Data

void CSystemLibraryItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString(Name);
	}

// Implementation

BOOL CSystemLibraryItem::FindFunction(CString &Name, CTypeDef &Type, UINT &uCount)
{
	WORD ID = WORD(m_Ident);

	if( m_pServer->NameFunction(ID, Name) ) {

		CError *pError = New CError(FALSE);

		if( m_pServer->FindFunction(pError, Name, ID, Type, uCount) ) {

			delete pError;

			return TRUE;
			}

		delete pError;
		}

	return FALSE;
	}

CString CSystemLibraryItem::TextFromType(UINT Type)
{
	// REV3 -- Get from name server?

	switch( Type ) {

		case typeVoid:    return L"void";
		case typeInteger: return L"int";
		case typeReal:    return L"float";
		case typeString:  return L"cstring";
		case typePort:    return L"port";
		case typeDevice:  return L"device";
		case typeLog:     return L"log";
		case typePage:    return L"page";
		}

	return L"???";
	}

CString CSystemLibraryItem::DataFromType(UINT Type)
{
	// REV3 -- Get from name server?

	switch( Type ) {

		case typeVoid:    return L"";
		case typeInteger: return L"0";
		case typeReal:    return L"0";
		case typeString:  return L"\"\"";
		case typePort:    return L"0";
		case typeDevice:  return L"0";
		case typeLog:     return L"0";
		case typePage:    return L"Page1";
		}

	return L"???";
	}

// End of File
