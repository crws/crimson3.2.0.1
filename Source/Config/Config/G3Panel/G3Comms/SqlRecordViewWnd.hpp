
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlRecordViewWnd_HPP

#define INCLUDE_SqlRecordViewWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// SQL Record View Window
//

class CSqlRecordViewWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlRecordViewWnd(void);

	protected:
		// Data Members
		CSysProxy m_System;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnSetFocus(CWnd &Wnd);
		void OnPaint(void);
	};

// End of File

#endif
