
#include "Intern.hpp"

#include "DispFormatMultiPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispFormatMulti.hpp"
#include "DispFormatMultiEntry.hpp"
#include "DispFormatMultiList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Page
//

// Runtime Class

AfxImplementRuntimeClass(CDispFormatMultiPage, CUIPage);

// Constructor

CDispFormatMultiPage::CDispFormatMultiPage(CDispFormatMulti *pFormat)
{
	m_pFormat = pFormat;
	}

// Operations

BOOL CDispFormatMultiPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_FORMAT_CONTROL), 1);

	pView->AddUI(pItem, L"root", L"Count");

	pView->AddUI(pItem, L"root", L"Limit");

	pView->AddUI(pItem, L"root", L"Default");

	pView->AddUI(pItem, L"root", L"Range");

	pView->EndGroup(TRUE);

	pView->StartTable(CString(IDS_FORMAT_STATES), 2);

	pView->AddColHead(CString(IDS_DATA));

	pView->AddColHead(CString(IDS_TEXT));

	UINT c = m_pFormat->m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CItem *pItem = m_pFormat->m_pList->GetItem(n);

		pView->AddRowHead(CPrintf(L"%u", 1+n));

		pView->AddUI(pItem, L"root", L"Data");

		pView->AddUI(pItem, L"root", L"Text");
		}

	pView->EndTable();

	pView->StartGroup(CString(IDS_FORMAT_COMMANDS), 1);

	pView->AddButton( CString(IDS_EXPORT_STATES),
			  CString(IDS_EXPORT_STATE_DATA_2),
			  L"ButtonExport"
			  );

	pView->AddButton( CString(IDS_IMPORT_STATES),
			  CString(IDS_IMPORT_STATE_DATA_2),
			  L"ButtonImport"
			  );

	pView->EndGroup(TRUE);

	return TRUE;
	}

// End of File
