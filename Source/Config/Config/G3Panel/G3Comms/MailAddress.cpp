
#include "Intern.hpp"

#include "MailAddress.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mail Address
//

// Dynamic Class

AfxImplementDynamicClass(CMailAddress, CCodedHost);

// Constructor

CMailAddress::CMailAddress(void)
{
	m_pAddr   = NULL;

	m_pEnable = NULL;
	}

// Type Access

BOOL CMailAddress::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Addr" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Enable" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CMailAddress::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddText(m_Name);

	Init.AddItem(itemVirtual, m_pAddr);

	Init.AddItem(itemVirtual, m_pEnable);

	return TRUE;
	}

// Meta Data Creation

void CMailAddress::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddVirtual(Addr);
	Meta_AddVirtual(Enable);
	}

// End of File
