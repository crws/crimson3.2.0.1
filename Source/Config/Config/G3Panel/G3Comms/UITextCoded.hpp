
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextCoded_HPP

#define INCLUDE_UITextCoded_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Coded Item
//

class DLLAPI CUITextCoded : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextCoded(void);

	protected:
		// Data Members
		CString m_Params;
		CLASS   m_Class;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Default Type
		virtual void FindReqType(CTypeDef &Type);

		// Implementation
		void ShowTwoWay(void);
	};

// End of File

#endif
