
#include "Intern.hpp"

#include "DataServer.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsManager.hpp"
#include "CommsSystem.hpp"
#include "DataTag.hpp"
#include "ProgramItem.hpp"
#include "ProgramPrototype.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Server
//

// Runtime Class

AfxImplementRuntimeClass(CDataServer, CObject);

// Constructor

CDataServer::CDataServer(CCommsSystem *pSystem)
{
	m_pSystem = pSystem;

	m_pComms  = m_pSystem->m_pComms;

	m_pTags   = m_pSystem->m_pTags->m_pTags;
	}

// Destructor

CDataServer::~CDataServer(void)
{
	}

// Operations

void CDataServer::ResetServer(void)
{
	}

// IBase Methods

UINT CDataServer::Release(void)
{
	delete this;

	return 0;
	}

// IDataServer Methods

DWORD CDataServer::GetData(DWORD ID, UINT Type, UINT Flags)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.x.m_HasBit ) {

		Ref.x.m_HasBit = 0;

		DWORD dwMask   = (1 << Ref.b.m_BitRef);

		DWORD dwData   = GetData(ID, Type, Flags);

		if( dwData & dwMask ) {

			return TRUE;
			}

		return FALSE;
		}

	if( Ref.x.m_IsTag ) {

		if( Ref.t.m_Index < 0x7F80 ) {

			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					DWORD Data = pTag->Execute();

					if( Type == typeInteger ) {

						if( pTag->GetDataType() == typeReal ) {

							Data = C3INT(I2R(Data));
							}
						}

					if( Type == typeReal ) {

						if( pTag->GetDataType() == typeInteger ) {

							Data = R2I(C3REAL(C3INT(Data)));
							}
						}

					return Data;
					}
				}

			AfxAssert(FALSE);
			}
		}
	else {
		if( !Ref.b.m_Block ) {

			switch( Ref.t.m_Index ) {

				case 0x7C02: return R2I(float(3.1415926));
				case 0x7C05: return 0;
				case 0x7C07: return 0;
				case 0x7C0C: return 0;
				case 0x7C08: return 0;
				case 0x7C09: return 0;
				case 0x7C0A: return 0;
				}
			}
		}

	if( Type == typeReal ) {

		return R2I(0.0);
		}

	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
		}

	return 0;
	}

void CDataServer::SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data)
{
	if( Type == typeString ) {

		free(PTXT(Data));
		}
	}

DWORD CDataServer::GetProp(DWORD ID, WORD Prop, UINT Type)
{
	if( Prop ) {

		UINT Index = (ID & 0x7FFF);

		if( ID & 0x8000 ) {

			if( Index <= 0x7F7F ) {

				CTag *pTag = m_pTags->GetItem(Index);

				if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					return pTag->GetProp(Prop, Type);
					}
				}

			return CTag::GetDefProp(Prop, Type);
			}
		}

	if( Type == typeReal ) {

		return R2I(0.0);
		}

	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
		}

	return 0;
	}

PVOID CDataServer::GetItem(DWORD ID)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.x.m_IsTag ) {

		if( Ref.t.m_Index < 0x7F80 ) {

			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					return PVOID(pTag);
					}
				}
			}
		}

	return NULL;
	}

DWORD CDataServer::RunFunc(WORD ID, UINT uParam, PDWORD pParam)
{
	if( ID & 0x8000 ) {

		CDatabase *pDbase = m_pSystem->GetDatabase();

		CItem     *pItem  = pDbase->GetHandleItem(ID & 0x7FFF);

		CMetaItem *pMeta  = (CMetaItem *) pItem;

		if( pMeta ) {

			if( pMeta->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

				CProgramItem *pProg = (CProgramItem *) pMeta;

				if( pProg->m_pProt->m_Type == typeString ) {

					return DWORD(wstrdup(L""));
					}
				}
			}

		return 0;
		}

	if( ID == 0x2013 ) {

		C3INT d     = pParam[0];

		UINT  r     = pParam[1];

		UINT  c     = pParam[2];

		PUTF  pData = PUTF(malloc(2 * c + 2));

		AfxAssume(pData);

		if( r ) {

			pData[c] = 0;

			while( c-- ) {

				pData[c] = "0123456789ABCDEF"[d % r];

				d /= r;
				}
			}
		else
			pData[0] = 0;

		return DWORD(pData);
		}

	switch( ID ) {

		case 0x200F:
		case 0x2010:
		case 0x2011:
		case 0x2013:
		case 0x2014:
		case 0x2057:
		case 0x2058:
		case 0x2066:
		case 0x2067:
		case 0x206A:
		case 0x206B:
		case 0x206E:
		case 0x206F:
		case 0x2070:
		case 0x2071:
		case 0x207C:
		case 0x207D:
		case 0x2083:
		case 0x2086:
		case 0x209A:
		case 0x209B:
		case 0x209C:
		case 0x20C3:
		case 0x20C9:
		case 0x20CA:
		case 0x20D1:
		case 0x20D2:
		case 0x20D3:
		case 0x20D8:
		case 0x20D9:
		case 0x20DB:
		case 0x20E2:
		case 0x20F2:

			return DWORD(wstrdup(L""));
		}

	return 0;
	}

// End of File
