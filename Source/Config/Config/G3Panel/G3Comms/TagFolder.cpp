
#include "Intern.hpp"

#include "TagFolder.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "DataServer.hpp"
#include "NameServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Tag Folder
//

// Dynamic Class

AfxImplementDynamicClass(CTagFolder, CTag);

// Constructor

CTagFolder::CTagFolder(void)
{
	m_Locked  = FALSE;

	m_fLegacy = TRUE;
	}

// UI Update

void CTagFolder::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		pHost->EnableUI("Desc",  !m_Locked);

		pHost->EnableUI("Class", !m_Locked);
		}

	if( Tag == "Value" ) {

		m_Circle = FALSE;

		CheckCircular(pHost, m_pValue);

		Recompile(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Server Access

INameServer * CTagFolder::GetNameServer(CNameServer *pName)
{
	pName->SetRoot(m_Name);

	return pName;
	}

IDataServer * CTagFolder::GetDataServer(CDataServer *pData)
{
	return pData;
	}

// Type Access

BOOL CTagFolder::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		Type.m_Type  = typeNumeric;

		Type.m_Flags = flagSoftWrite | flagInherent;

		return TRUE;
		}

	return FALSE;
	}

// Overridables

UINT CTagFolder::GetDataType(void) const
{
	if( m_pValue ) {

		return m_pValue->GetType();
		}

	return typeInteger;
	}

// Operations

void CTagFolder::UpdateTypes(BOOL fComp)
{
	if( m_pValue ) {

		m_Circle = m_pValue->CheckCircular(this, FALSE);
		}
	}

// Property Save Filter

BOOL CTagFolder::SaveProp(CString const &Tag) const
{
	if( Tag == L"Sim" ) {

		return FALSE;
		}

	if( Tag == L"Value" ) {

		return FALSE;
		}

	if( Tag == L"Label" ) {

		return FALSE;
		}

	if( Tag == L"Format" ) {

		return FALSE;
		}

	if( Tag == L"Color" ) {

		return FALSE;
		}

	return CTag::SaveProp(Tag);
	}

// Download Support

BOOL CTagFolder::MakeInitData(CInitData &Init)
{
	Init.AddByte(1);

	CTag::MakeInitData(Init);

	return TRUE;
	}

// Persistance

void CTagFolder::Init(void)
{
	CTag::Init();

	m_fLegacy = FALSE;
	}

void CTagFolder::PostLoad(void)
{
	CTag::PostLoad();

	if( m_fLegacy ) {

		if( !GetHumanName().CompareN(L"Private") ) {

			m_Private = 1;

			m_fLegacy = FALSE;
			}
		}
	}

// Meta Data Creation

void CTagFolder::AddMetaData(void)
{
	CTag::AddMetaData();

	Meta_AddInteger(Locked);
	Meta_AddBoolean(Legacy);

	Meta_SetName((IDS_TAG_FOLDER));
	}

// End of File
