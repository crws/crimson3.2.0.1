
#include "Intern.hpp"

#include "UserList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UserItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User List
//

// Dynamic Class

AfxImplementDynamicClass(CUserList, CItemIndexList);

// Constructor

CUserList::CUserList(void)
{
	m_Class = AfxRuntimeClass(CUserItem);
	}

// Item Location

CUserItem * CUserList::GetItem(INDEX Index) const
{
	return (CUserItem *) CItemIndexList::GetItem(Index);
	}

CUserItem * CUserList::GetItem(UINT uPos)  const
{
	return (CUserItem *) CItemIndexList::GetItem(uPos);
	}

// End of File
