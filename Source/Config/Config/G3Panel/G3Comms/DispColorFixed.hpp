
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColorFixed_HPP

#define INCLUDE_DispColorFixed_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispColor.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Fixed Display Color
//

class CDispColorFixed : public CDispColor
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispColorFixed(void);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Color;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
