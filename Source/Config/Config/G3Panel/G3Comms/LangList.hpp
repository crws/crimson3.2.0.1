
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_LangList_HPP

#define INCLUDE_LangList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangItem;

//////////////////////////////////////////////////////////////////////////
//
// Language List
//

class DLLNOT CLangList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLangList(void);

		// Item Access
		CLangItem * GetItem(INDEX Index) const;
		CLangItem * GetItem(UINT  uPos ) const;
	};

// End of File

#endif
