
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_MouseItem_HPP

#define INCLUDE_MouseItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "USBPortItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Mouse Configuration
//

class DLLNOT CMouseItem : public CUSBPortItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMouseItem(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		UINT GetTreeImage(void) const;
		UINT GetType(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Enable;
		UINT m_Rate;
		UINT m_Hide;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
