
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIIPAddr_HPP

#define INCLUDE_UIIPAddr_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- IP Address
//

class CUIIPAddr : public CUICategorizer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIIPAddr(void);

	protected:
		// Modes
		enum
		{
			modeFixedIP	  = 1,
			modeFixedName	  = 2,
			modeDynamicName	  = 3,
			modeExpression	  = 4,
			modeExpressionWas = 1001,
			};

		// Data
		CCommsSystem  * m_pSystem;
		UINT            m_cfCode;

		// Core Overridables
		void OnBind(void);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);

		// Category Overridables
		BOOL    LoadModeButton(void);
		BOOL    SwitchMode(UINT uMode);
		UINT    FindDispMode(CString Text);
		CString FindDispText(CString Text, UINT uMode);
		CString FindDataText(CString Text, UINT uMode);
		UINT    FindDispType(UINT uMode);

		// Implementation
		CString FormatFixedIP(UINT uData);
		UINT    ParseFixedIP(CError &Error, CString Text);
		BOOL    AcceptText(IDataObject *pData, UINT cfText);
		BOOL    GetDataType(CString Text, CTypeDef &Type);
		BOOL    IsStringType(CString Text);
	};

// End of File

#endif
