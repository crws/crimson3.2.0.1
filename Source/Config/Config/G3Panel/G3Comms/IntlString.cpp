
#include "Intern.hpp"

#include "IntlString.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// International String	Item
//

// Dynamic Class

AfxImplementDynamicClass(CIntlString, CMetaItem);

// Constructor

CIntlString::CIntlString(void)
{
	}

// Meta Data Creation

void CIntlString::AddMetaData(void)
{
	Meta_AddString(Text);

	CMetaItem::AddMetaData();
	}

// End of File
