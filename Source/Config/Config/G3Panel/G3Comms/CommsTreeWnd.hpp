
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsTreeWnd_HPP

#define INCLUDE_CommsTreeWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsManager;
class CCommsMapData;
class CCommsPortGroup;
class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Communications Tree View
//

class CCommsTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsTreeWnd(void);

		// Destructor
		~CCommsTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Drag Codes
		enum
		{
			dragNone,
			dragMapping,
			};

		// Drop Codes
		enum
		{
			dropNone,
			dropPartial,
			dropFragment,
			dropData,
			dropOther
			};

		// Editing Actions
		class CCmdCreate;
		class CCmdRename;
		class CCmdDelete;
		class CCmdPaste;

		// Static Data
		static UINT m_timerDouble;
		static UINT m_timerSelect;
		static UINT m_timerScroll;

		// Data Members
		CCommsManager   * m_pComms;
		CCommsSystem    * m_pSystem;
		CCommsPortGroup * m_pGroup;
		UINT		  m_cfCode;
		UINT		  m_cfPart;
		CNameMap	  m_MapFixed;
		BOOL              m_fMulti;
		BOOL		  m_uLocked;
		BOOL		  m_fRefresh;
		BOOL              m_fRecomp;

		// Drag Context
		UINT		  m_uDrag;

		// Drop Context
		UINT		  m_uDrop;
		BOOL		  m_fDropLocal;
		BOOL		  m_fDropMove;
		CLASS             m_DropClass;
		HTREEITEM	  m_hDropRoot;
		HTREEITEM	  m_hDropPrev;

		// Overridables
		void    OnAttach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Notification Handlers
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTreeReturn(UINT uID, NMHDR &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);

		// Command Handlers
		BOOL OnCommsGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnCommsControl(UINT uID, CCmdSource &Src);
		BOOL OnCommsCommand(UINT uID);
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);

		// Comms Menu
		BOOL CanCommsClearPort(void);
		BOOL CanCommsAddDevice(void);
		BOOL CanCommsDelDevice(void);
		BOOL CanCommsAddBlock(void);
		BOOL CanCommsDelBlock(void);
		BOOL CanCommsMapImport(void);
		BOOL CanCommsMapExport(void);
		BOOL CanCommsMapExpand(void);
		BOOL CanCommsMapCollapse(void);
		BOOL CanCommsDelPort(void);
		BOOL CanCommsAddPort(void);
		BOOL CanCommsAddVirtual(void);
		BOOL CanCommsRemoveCard(void);
		BOOL CanCommsTagImport(void);
		BOOL CanCommsWatchBlock(void);
		BOOL CanCommsAddRack(void);
		BOOL CanCommsDelRack(void);
		void OnCommsClearPort(void);
		void OnCommsAddDevice(void);
		void OnCommsDelDevice(void);
		void OnCommsAddBlock(void);
		void OnCommsDelBlock(void);
		void OnCommsMapImport(void);
		void OnCommsMapExport(void);
		void OnCommsDelPort(void);
		void OnCommsMapExpand(void);
		void OnCommsMapCollapse(void);
		void OnCommsAddPort(void);
		void OnCommsAddVirtual(void);
		void OnCommsRemoveCard(void);
		void OnCommsTagImport(void);
		void OnCommsWatchBlock(void);
		void OnCommsBuildBlock(void);
		void OnCommsAddRack(void);

		// Go Menu
		BOOL OnGoNext(BOOL fSiblings);
		BOOL OnGoPrev(BOOL fSiblings);
		BOOL StepAway(void);

		// Item Menu : Create
		void OnCreateItem(CMetaItem *pItem);
		void OnExecCreate(CCmdCreate *pCmd);
		void OnUndoCreate(CCmdCreate *pCmd);

		// Item Menu : Rename
		BOOL CanItemRename(void);
		BOOL OnItemRename(CString Name);
		void OnExecRename(CCmdRename *pCmd);
		void OnUndoRename(CCmdRename *pCmd);
		void RenameItem(CString Name, CString Prev);

		// Item Menu : Delete
		BOOL CanItemDelete(void);
		void OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop);
		void OnExecDelete(CCmdDelete *pCmd);
		void OnUndoDelete(CCmdDelete *pCmd);

		// Edit Menu : Cut and Copy
		BOOL CanItemCopy(void);
		void OnEditCut(void);
		void OnEditCopy(void);

		// Edit Menu : Paste
		BOOL CanEditPaste(void);
		void OnEditPaste(void);
		void OnEditPaste(IDataObject *pData);
		void OnEditPaste(CItem *pItem, HTREEITEM hRoot, HTREEITEM hPrev, IDataObject *pData);
		void OnExecPaste(CCmdPaste *pCmd);
		void OnExecPasteStream(CCmdPaste *pCmd);
		void OnUndoPaste(CCmdPaste *pCmd);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);
		BOOL AddMapReg(CDataObject *pData);
		BOOL AddMapping(CDataObject *pData);
		BOOL AddItemToStream(ITextStream &Stream, HTREEITEM hItem);
		void AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem);

		// Data Object Acceptance
		BOOL CanAcceptDataObject(IDataObject *pData, UINT &uType);
		BOOL CanAcceptCodeFragment(IDataObject *pData);
		BOOL CanAcceptPartial(IDataObject *pData);
		BOOL AcceptCodeFragment(CString &Text, IDataObject *pData);
		BOOL AcceptPartial(CCommsMapData const * &pMapData, IDataObject *pData);

		// Drag Hooks
		DWORD FindDragAllowed(void);

		// Drop Support
		void DropTrack(CPoint Pos, BOOL fMove);
		BOOL DropDone(IDataObject *pData);
		void DropDebug(void);
		void ShowDrop(BOOL fShow);
		BOOL ShowDropVert(HTREEITEM hItem);
		BOOL IsDropMove(DWORD dwKeys, DWORD *pEffect);
		BOOL IsValidDrop(void);
		BOOL IsValidMove(void);
		BOOL IsExpandingDrop(void);
		BOOL IsFullItemDrop(void);

		// Data Mapping
		BOOL SetMapping(CCommsMapData const *pMap);
		BOOL SetMapping(CString Text, BOOL fStep);

		// Tree Loading
		void      LoadTree(void);
		void      LoadRoot(void);
		HTREEITEM LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem);
		void      LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem);
		void      LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem);
		void      LoadList(HTREEITEM hRoot, CItemList *pList, UINT uOption = 0);
		HTREEITEM RefreshFrom(HTREEITEM hItem);

		// Item Mapping
		void AddToMap(CMetaItem *pItem, HTREEITEM hItem);
		void RemoveFromMap(CMetaItem *pItem);

		// Item Hooks
		void NewItemSelected(void);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		BOOL CheckItemName(CString const &Name);
		void KillItem(HTREEITEM hItem, BOOL fRemove);

		// Item Data
		CString GetItemList(CMetaItem *pItem);
		BOOL    HasVarImage(CMetaItem *pItem);

		// Selection Hooks
		CString SaveSelState(void);
		void    LoadSelState(CString State);

		// Implementation
		BOOL WarnMultiple(UINT uVerb);
		BOOL WarnMultiple(CString Verb);
		BOOL MarkMulti(CString Item, CString Menu, UINT uCode, BOOL fMark);
		void WalkToLast(HTREEITEM &hItem);
		void LockUpdate(BOOL fLock);
		void SkipUpdate(void);
		void ListUpdated(HTREEITEM hItem);
		void UpdateMappings(HTREEITEM hItem);
		void CheckItemCmd(void);
		BOOL Recompile(CLASS Class);
		BOOL Recompile(void);
		void ShowStatus(void);

		CString FindRootName(CString &Name);
	};

// End of File

#endif
