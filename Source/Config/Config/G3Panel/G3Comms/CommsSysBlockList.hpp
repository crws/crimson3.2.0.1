
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsSysBlockList_HPP

#define INCLUDE_CommsSysBlockList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSysBlock;

//////////////////////////////////////////////////////////////////////////
//
// Comms Block List
//

class DLLNOT CCommsSysBlockList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsSysBlockList(void);

		// Item Access
		CCommsSysBlock * GetItem(INDEX Index) const;
		CCommsSysBlock * GetItem(UINT  uPos ) const;

		// Block Lookup
		CCommsSysBlock * FindBlock(UINT uBlock) const;

		// Persistance
		void PreSnapshot(void);
	};

// End of File

#endif
