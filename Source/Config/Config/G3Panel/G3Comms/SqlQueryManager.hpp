
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQueryManager_HPP

#define INCLUDE_SqlQueryManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlQueryList;

//////////////////////////////////////////////////////////////////////////
//
// SQL Query Manager
//

class CSqlQueryManager : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlQueryManager(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Initial Values
		void SetInitValues(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// UI Loading
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		BOOL CreateLog(CString Name);
		void PostConvert(void);
		void Validate(void);
		BOOL Check(BOOL fList);
		BOOL CheckQueries(BOOL fList, CStringArray &Errors);

		// Query Helpers
		CString FindUnusedName(void);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT            m_Handle;
		UINT		m_Enable;
		CSqlQueryList * m_pQueries;
		CCodedItem    * m_pServer;
		UINT            m_Port;
		CString         m_Instance;
		UINT            m_Connect;
		UINT		m_TlsMode;
		CString         m_User;
		CString         m_Pass;
		CString         m_DatabaseName;
		UINT            m_Validate;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// UI Update
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
