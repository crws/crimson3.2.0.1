
#include "Intern.hpp"

#include "MqttClientOptions.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client Options
//

// Base Class

#undef  CBaseClass

#define CBaseClass CCodedHost

// Dynamic Class

AfxImplementDynamicClass(CMqttClientOptions, CBaseClass);

// Constructor

CMqttClientOptions::CMqttClientOptions(void)
{
	m_Advanced    = 1;
	m_Debug       = 0;
	m_Tls         = TRUE;
	m_IdSrc       = 0;
	m_CaSrc       = 0;
	m_Check       = 0;
	m_Face	      = 0;
	m_Port        = 8883;
	m_pPeerName   = NULL;
	m_pPeerName2  = NULL;
	m_pPeerName3  = NULL;
	m_pPeerName4  = NULL;
	m_pPeerName5  = NULL;
	m_pPeerName6  = NULL;
	m_PubQos      = 1;
	m_SubQos      = 1;
	m_pClientId   = NULL;
	m_pUserName   = NULL;
	m_pPassword   = NULL;
	m_ConnTimeout = 60;
	m_SendTimeout = 30;
	m_RecvTimeout = 30;
	m_KeepAlive   = 120;
	m_BackOffTime = 10;
	m_BackOffMax  = 120;
	m_NoCleanSess = 0;
}

// Operations

void CMqttClientOptions::SetClientId(CString const &Id)
{
	SetInitial(L"ClientId", m_pClientId, L'"' + Id + L'"');
}

void CMqttClientOptions::SetPeerName(CString const &Peer)
{
	SetInitial(L"PeerName", m_pPeerName, L'"' + Peer + L'"');
}

void CMqttClientOptions::SetCredentials(CString const &User, CString const &Pass)
{
	SetInitial(L"Username", m_pUserName, L'"' + User + L'"');

	SetInitial(L"Password", m_pPassword, L'"' + Pass + L'"');
}

// Type Access

BOOL CMqttClientOptions::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Username" || Tag == L"Password" || Tag == L"ClientId" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag.StartsWith(L"PeerName") ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return CBaseClass::GetTypeData(Tag, Type);
}

// UI Update

void CMqttClientOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Advanced" || Tag == "CaSrc" ) {

			DoEnables(pHost);
		}

		if( Tag == "Tls" ) {

			if( m_Tls == 0 && (!m_Port || m_Port == 8883) ) {

				m_Port = 1883;

				pHost->UpdateUI("Port");
			}

			if( m_Tls == 1 && (!m_Port || m_Port == 1883) ) {

				m_Port = 8883;

				pHost->UpdateUI("Port");
			}

			DoEnables(pHost);
		}

		CUIItem *pParent = (CUIItem *) GetParent();

		pParent->OnUIChange(pHost, pItem, Tag);
	}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
}

// Download Support

BOOL CMqttClientOptions::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddByte(BYTE(m_Debug));
	Init.AddByte(BYTE(m_Tls));
	Init.AddWord(WORD(m_IdSrc));
	Init.AddWord(WORD(m_CaSrc));
	Init.AddByte(BYTE(m_Check));
	Init.AddWord(WORD(m_Face));
	Init.AddWord(WORD(m_Port));
	Init.AddItem(itemVirtual, m_pPeerName);
	Init.AddItem(itemVirtual, m_pPeerName2);
	Init.AddItem(itemVirtual, m_pPeerName3);
	Init.AddItem(itemVirtual, m_pPeerName4);
	Init.AddItem(itemVirtual, m_pPeerName5);
	Init.AddItem(itemVirtual, m_pPeerName6);
	Init.AddByte(BYTE(m_PubQos));
	Init.AddByte(BYTE(m_SubQos));
	Init.AddItem(itemVirtual, m_pClientId);
	Init.AddItem(itemVirtual, m_pUserName);
	Init.AddItem(itemVirtual, m_pPassword);
	Init.AddWord(WORD(m_ConnTimeout));
	Init.AddWord(WORD(m_SendTimeout));
	Init.AddWord(WORD(m_RecvTimeout));
	Init.AddWord(WORD(m_BackOffTime));
	Init.AddWord(WORD(m_BackOffMax));
	Init.AddWord(WORD(m_KeepAlive));
	Init.AddByte(BYTE(m_NoCleanSess));

	return TRUE;
}

// Meta Data Creation

void CMqttClientOptions::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(Advanced);
	Meta_AddInteger(Debug);
	Meta_AddInteger(Tls);
	Meta_AddInteger(IdSrc);
	Meta_AddInteger(CaSrc);
	Meta_AddInteger(Check);
	Meta_AddInteger(Face);
	Meta_AddInteger(Port);
	Meta_AddVirtual(PeerName);
	Meta_AddVirtual(PeerName2);
	Meta_AddVirtual(PeerName3);
	Meta_AddVirtual(PeerName4);
	Meta_AddVirtual(PeerName5);
	Meta_AddVirtual(PeerName6);
	Meta_AddInteger(PubQos);
	Meta_AddInteger(SubQos);
	Meta_AddVirtual(ClientId);
	Meta_AddVirtual(UserName);
	Meta_AddVirtual(Password);
	Meta_AddInteger(ConnTimeout);
	Meta_AddInteger(SendTimeout);
	Meta_AddInteger(RecvTimeout);
	Meta_AddInteger(BackOffTime);
	Meta_AddInteger(BackOffMax);
	Meta_AddInteger(KeepAlive);
	Meta_AddInteger(NoCleanSess);

	Meta_SetName((IDS_MQTT_CONNECTION));
}

// Implementation

void CMqttClientOptions::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "SendTimeout", m_Advanced);

	pHost->EnableUI(this, "RecvTimeout", m_Advanced);

	pHost->EnableUI(this, "IdSrc", m_Tls);

	pHost->EnableUI(this, "CaSrc", m_Tls);

	pHost->EnableUI(this, "Check", m_Tls && m_CaSrc);
}

// End of File
