
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortWifi_HPP

#define INCLUDE_CommsPortWifi_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Wifi Comms Port
//

class DLLAPI CCommsPortWifi : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortWifi(void);
	};

// End of File

#endif
