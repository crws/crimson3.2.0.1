
#include "Intern.hpp"

#include "CloudTagSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cloud Tag Set
//

// Dynamic Class

AfxImplementDynamicClass(CCloudTagSet, CCloudDataSet);

// Constructor

CCloudTagSet::CCloudTagSet(void)
{
	m_Label   = 0;

	m_TagName = 0;

	m_Tree    = 0;

	m_Array   = 0;

	m_Props   = 0;

	m_Write   = 0;

	m_pSet    = New CTagSet;
}

// UI Update

void CCloudTagSet::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Mode" ) {

			DoEnables(pHost);
		}

		if( Tag == "Mode" && m_Mode == 3 ) {

			m_Write = 1;

			pHost->UpdateUI(this, "Write");
		}
	}

	CCloudDataSet::OnUIChange(pHost, pItem, Tag);
}

// Persistance

void CCloudTagSet::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString const &Name = Tree.GetName();

		if( Name == L"Name" ) {

			m_TagName = Tree.GetValueAsInteger();

			continue;
		}
		else {
			CMetaData const *pMeta = m_pList->FindData(Name);

			if( pMeta ) {

				LoadProp(Tree, pMeta);

				continue;
			}
		}
	}

	RegisterHandle();
}

// Download Support

BOOL CCloudTagSet::MakeInitData(CInitData &Init)
{
	CCloudDataSet::MakeInitData(Init);

	Init.AddByte(BYTE(m_Label));

	Init.AddByte(BYTE(m_TagName));

	Init.AddByte(BYTE(m_Tree));

	Init.AddByte(BYTE(m_Array));

	Init.AddByte(BYTE(m_Props));

	Init.AddByte(BYTE(m_Write));

	Init.AddItem(itemSimple, m_pSet);

	return TRUE;
}

// Meta Data Creation

void CCloudTagSet::AddMetaData(void)
{
	CCloudDataSet::AddMetaData();

	Meta_AddInteger(Label);
	Meta_AddInteger(TagName);
	Meta_AddInteger(Tree);
	Meta_AddInteger(Array);
	Meta_AddInteger(Props);
	Meta_AddInteger(Write);
	Meta_AddObject(Set);

	Meta_SetName((IDS_CLOUD_TAG_SET));
}

// Implementation

void CCloudTagSet::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Label", m_Mode >= 1);

	pHost->EnableUI(this, "TagName", m_Mode >= 1);

	pHost->EnableUI(this, "Tree", m_Mode >= 1);

	pHost->EnableUI(this, "Array", m_Mode >= 1);

	pHost->EnableUI(this, "Props", m_Mode >= 1);

	pHost->EnableUI(this, "Write", m_Mode == 1 || m_Mode == 2);

	pHost->EnableUI(this, "Set", m_Mode >= 1);
}

// End of File
