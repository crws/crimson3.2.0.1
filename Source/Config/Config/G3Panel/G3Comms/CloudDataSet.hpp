
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CloudDataSet_HPP

#define INCLUDE_CloudDataSet_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//

class CCloudDataSet : public CCodedHost
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CCloudDataSet(void);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Data Members
	UINT		m_Mode;
	UINT		m_History;
	UINT		m_Scan;
	UINT		m_Force;
	CCodedItem    * m_pReq;
	CCodedItem    * m_pAck;
	CCodedItem    * m_pSuffix;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
};

// End of File

#endif
