
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_WebServer_HPP

#define INCLUDE_WebServer_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CHttpServerOptions;

class CWebPageList;

//////////////////////////////////////////////////////////////////////////
//
// Web Server
//

class CWebServer : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWebServer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void PostConvert(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);
		void Load(CTreeFile &Tree);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Type Definitions
		typedef CHttpServerOptions COpts;

		// Data Members
		UINT	       m_Handle;
		UINT           m_Enable;
		UINT	       m_DbCrc;
		UINT	       m_DbTime;
		CWebPageList * m_pPages;
		COpts        * m_pOpts;
		CCodedText   * m_pTitle;
		CCodedText   * m_pHeader;
		CCodedText   * m_pHome;
		UINT	       m_CustHome;
		UINT	       m_CustIcon;
		UINT	       m_CustLogon;
		UINT	       m_CustCss;
		UINT	       m_CustJs;
		UINT	       m_RemoteView;
		UINT	       m_RemoteCtrl;
		UINT	       m_RemoteZoom;
		UINT	       m_RemoteFact;
		UINT	       m_RemoteBits;
		UINT	       m_RemoteSec;
		UINT	       m_LogsView;
		UINT	       m_LogsSec;
		UINT	       m_LogsBatches;
		UINT	       m_UserSite;
		UINT	       m_UserMenu;
		UINT	       m_UserDelay;
		UINT	       m_UserRoot;
		UINT	       m_UserSec;
		UINT	       m_SystemPages;
		UINT	       m_SystemConsole;
		UINT	       m_SystemCapture;
		UINT	       m_SystemJump;
		UINT	       m_SystemSec;
		UINT	       m_Source;
		UINT           m_Local;
		CCodedText   * m_pUser;
		CCodedText   * m_pReal;
		CCodedText   * m_pPass;
		UINT	       m_Restrict;
		UINT	       m_SecMask;
		UINT	       m_SecAddr;
		CCodedItem   * m_pPort;
		UINT           m_SameOrigin;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
