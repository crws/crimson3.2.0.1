
#include "Intern.hpp"

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CodedText.hpp"
#include "CommsSystem.hpp"
#include "DataServer.hpp"
#include "NameServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Coded Host
//

// Runtime Class

AfxImplementRuntimeClass(CCodedHost, CUIItem);

// Constructor

CCodedHost::CCodedHost(void)
{
	}

// Initial Values

void CCodedHost::SetInitValues(void)
{
	}

// Name Hint

BOOL CCodedHost::GetNameHint(CString Tag, CString &Name)
{
	return FALSE;
	}

// Type Access

BOOL CCodedHost::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeNumeric;

	Type.m_Flags = flagInherent;

	return FALSE;
	}

// Attributes

BOOL CCodedHost::HasBrokenCode(void) const
{
	CLASS      Coded  = AfxRuntimeClass(CCodedItem);

	CMetaList *pList  = FindMetaList();

	UINT       uCount = pList->GetCount();

	PVOID      pThis  = PVOID(this);

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pData = pList->FindData(n);

		UINT             uType = pData->GetType();

		if( uType == metaObject || uType == metaVirtual ) {

			CItem *pChild = pData->GetObject(pThis);

			if( pChild ) {

				if( pChild->IsKindOf(Coded) ) {

					CCodedItem *pCoded = (CCodedItem *) pChild;

					if( pCoded->IsBroken() ) {

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

// Operations

void CCodedHost::UpdateTypes(BOOL fComp)
{
	}

// Server Access

INameServer * CCodedHost::GetNameServer(CNameServer *pName)
{
	CLASS  Class = AfxRuntimeClass(CCodedHost);

	CItem *pItem = GetParent();

	while( pItem ) {

		if( pItem->IsKindOf(Class) ) {

			CCodedHost *pHost = (CCodedHost *) pItem;

			return pHost->GetNameServer(pName);
			}

		pItem = pItem->GetParent();
		}

	return pName;
	}

IDataServer * CCodedHost::GetDataServer(CDataServer *pData)
{
	CLASS  Class = AfxRuntimeClass(CCodedHost);

	CItem *pItem = GetParent();

	while( pItem ) {

		if( pItem->IsKindOf(Class) ) {

			CCodedHost *pHost = (CCodedHost *) pItem;

			return pHost->GetDataServer(pData);
			}

		pItem = pItem->GetParent();
		}

	return pData;
	}

// Persistance

void CCodedHost::Init(void)
{
	CUIItem::Init();

	SetInitValues();
	}

void CCodedHost::PostLoad(void)
{
	CUIItem::PostLoad();

	SetInitValues();
	}

// Property Upgrade

void CCodedHost::Upgrade(CTreeFile &Tree, CMetaData const *pMeta)
{
	// NOTE -- This is a little problematic as the SetInitial
	// calls Init on the new coded item, but we're also in the
	// process of loading it, so it gets a PostLoad. We work
	// around it in the coded item, but it's a bit nasty...

	if( pMeta->GetType() == metaString ) {

		Tree.GetObject();

		Tree.GetName();

		CString Class = Tree.GetValueAsString();

		if( Class != L"NULL" ) {

			CLASS Named = m_pDbase->AliasClass(Class);

			if( Named ) {

				CItem *pItem = AfxNewObject(CItem, Named);

				if( pItem ) {

					pItem->SetParent(this);

					pItem->Load(Tree);

					if( pItem->IsKindOf(AfxRuntimeClass(CCodedText)) ) {

						CCodedText *pText = (CCodedText *) pItem;

						if( pText->IsConst() ) {

							CString Text = pText->GetSource(FALSE);

							Text = Text.Mid(1, Text.GetLength() - 2);

							pMeta->WriteString(this, Text);
							}
						}

					pItem->Kill();

					delete pItem;
					}
				}
			}

		Tree.EndObject();
		}

	if( pMeta->GetType() == metaVirtual ) {

		CString        Name  = pMeta->GetTag();

		CCodedItem * & pItem = (CCodedItem * &) pMeta->GetObject(this);

		CTypeDef       Type  = { 0, 0 };

		if( GetTypeData(Name, Type) ) {

			if( Type.m_Type == typeString ) {

				CString Text = Tree.GetValueAsString();

				if( !Text.IsEmpty() ) {

					Text.Replace(L"\"", L"\\\"");

					CString Code = L'"' + Text + L'"';

					SetInitial(Name, pItem, Code);
					}

				return;
				}

			if( Type.m_Type >= typeIndex ) {

				UINT uData = Tree.GetValueAsInteger();

				SetInitial(Name, pItem, uData);

				return;
				}

			if( Type.m_Type >= typeObject ) {

				UINT hItem = Tree.GetValueAsInteger();

				if( hItem ) {

					CItem *pFind = m_pDbase->GetHandleItem(hItem);

					if( pFind ) {

						if( pFind->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

							CMetaItem *pMeta = (CMetaItem *) pFind;

							if( pMeta->HasName() ) {

								CString Text = pMeta->GetName();

								CString Code = L"WAS " + Text;

								SetInitial(Name, pItem, Code);
								}
							}
						}
					else {
						if( hItem & 0x10000 ) {

							if( hItem == 0x10000 ) {

								SetInitial(Name, pItem, L"//p");
								}

							if( hItem == 0x10001 ) {

								SetInitial(Name, pItem, L"//n");
								}
							}
						else {
							CString Text = CPrintf(L"__H%4.4X", hItem);

							CString Code = L"WAS " + Text;

							SetInitial(Name, pItem, Code);
							}
						}
					}

				return;
				}
			}

		// NOTE -- We take a flyer and assume that this
		// really is a coded item even though we have not
		// been given confirmation via GetTypeData...

		UINT uData = Tree.GetValueAsInteger();

		SetInitial(Name, pItem, uData);
		}
	}

// Implementation

BOOL CCodedHost::LimitEnum(IUIHost *pHost, CString Tag, UINT &uData, UINT uMask)
{
	CLASS        Class = AfxRuntimeClass(CUITextEnum);

	CUITextEnum *pEnum = (CUITextEnum *) pHost->FindUI(Class, this, Tag);

	if( pEnum ) {

		pEnum->SetMask(uMask);

		if( !pEnum->IsDataValid(uData) ) {

			while( uData > 0x00 && !pEnum->IsDataValid(uData) ) {

				uData--;
				}

			while( uData < 0x1F && !pEnum->IsDataValid(uData) ) {

				uData++;
				}
			}

		if( pHost ) {

			pHost->UpdateUI(this, Tag);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedHost::UpdateType(IUIHost *pHost, CString Tag, CCodedItem *pItem, BOOL fComp)
{
	if( pItem ) {

		CTypeDef Type;

		if( GetTypeData(Tag, Type) ) {

			pItem->SetReqType(Type);
			}

		if( fComp ) {

			pItem->Recompile(TRUE);

			if( pHost ) {

				pHost->UpdateUI(this, Tag);
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedHost::SetInitial(CString Tag, CCodedItem * &pItem, UINT uVal)
{
	return SetInitial(Tag, pItem, CPrintf(L"0x%X", uVal));
	}

BOOL CCodedHost::SetInitial(CString Tag, CCodedItem * &pItem, CString Code)
{
	if( pItem == NULL ) {

		CTypeDef Type;

		pItem = New CCodedItem;

		pItem->SetParent(this);

		pItem->Init();

		if( GetTypeData(Tag, Type) ) {

			pItem->SetReqType(Type);
			}

		if( !pItem->Compile(Code) ) {

			Code = L"WAS " + Code;

			pItem->Compile(Code);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedHost::InitCoded(IUIHost *pHost, CString Tag, CCodedItem * &pItem, UINT uVal)
{
	return InitCoded(pHost, Tag, pItem, CPrintf(L"0x%X", uVal));
	}

BOOL CCodedHost::InitCoded(IUIHost *pHost, CString Tag, CCodedItem * &pItem, CString Code)
{
	CTypeDef Type;

	if( pItem == NULL ) {

		pItem = New CCodedItem;

		pItem->SetParent(this);

		pItem->Init();
		}

	if( GetTypeData(Tag, Type) ) {

		pItem->SetReqType(Type);
		}

	if( !pItem->Compile(Code) ) {

		Code = L"WAS " + Code;

		pItem->Compile(Code);
		}

	if( pHost ) {

		pHost->UpdateUI(this, Tag);
		}

	return TRUE;
	}

BOOL CCodedHost::KillCoded(IUIHost *pHost, CString Tag, CCodedItem * &pItem)
{
	if( pItem ) {

		pItem->Kill();

		delete pItem;

		pItem = NULL;

		if( pHost ) {

			pHost->UpdateUI(this, Tag);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedHost::InitCoded(IUIHost *pHost, CString Tag, CCodedText * &pText, CString Code)
{
	CTypeDef Type;

	if( pText == NULL ) {

		pText = New CCodedText;

		pText->SetParent(this);

		pText->Init();
		}

	if( GetTypeData(Tag, Type) ) {

		pText->SetReqType(Type);
		}

	if( !pText->Compile(Code) ) {

		Code = L"WAS " + Code;

		pText->Compile(Code);
		}

	if( pHost ) {

		pHost->UpdateUI(this, Tag);
		}

	return TRUE;
	}

BOOL CCodedHost::SetInitial(CString Tag, CCodedText * &pText, CString Code)
{
	if( pText == NULL ) {

		CTypeDef Type;

		pText = New CCodedText;

		pText->SetParent(this);

		pText->Init();

		if( GetTypeData(Tag, Type) ) {

			pText->SetReqType(Type);
			}

		if( !pText->Compile(Code) ) {

			Code = L"WAS " + Code;

			pText->Compile(Code);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedHost::KillCoded(IUIHost *pHost, CString Tag, CCodedText * &pText)
{
	if( pText ) {

		pText->Kill();

		delete pText;

		pText = NULL;

		if( pHost ) {

			pHost->UpdateUI(this, Tag);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedHost::CheckEnable(CCodedItem *pItem, PCTXT pExpr, UINT uValue, BOOL fDefault)
{
	if( pItem ) {

		if( !wstrcmp(pExpr, L"<" ) ) {

			return pItem->IsConst() ? (pItem->ExecVal() <  uValue) : fDefault;
			}

		if( !wstrcmp(pExpr, L"<=" ) ) {

			return pItem->IsConst() ? (pItem->ExecVal() <= uValue) : fDefault;
			}

		if( !wstrcmp(pExpr, L"==" ) ) {

			return pItem->IsConst() ? (pItem->ExecVal() == uValue) : fDefault;
			}

		if( !wstrcmp(pExpr, L">" ) ) {

			return pItem->IsConst() ? (pItem->ExecVal() >  uValue) : fDefault;
			}

		if( !wstrcmp(pExpr, L">=" ) ) {

			return pItem->IsConst() ? (pItem->ExecVal() >= uValue) : fDefault;
			}

		if( !wstrcmp(pExpr, L"!=" ) ) {

			return pItem->IsConst() ? (pItem->ExecVal() != uValue) : fDefault;
			}
		}

	return fDefault;
	}

BOOL CCodedHost::FreeData(DWORD Data, UINT Type)
{
	if( Type == typeString ) {

		free(PTXT(Data));

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedHost::StepCoded(CCodedItem * &pValue)
{
	if( pValue ) {

		CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

		CString Text = pValue->GetSource(TRUE);

		CString Prev = Text;

		if( pSystem->StepFragment(Text, 1) ) {

			if( pValue->Compile(Text) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CCodedHost::StepCoded(CCodedText * &pValue)
{
	if( pValue ) {

		CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

		CString Text = pValue->GetSource(TRUE);

		CString Prev = Text;

		if( pSystem->StepFragment(Text, 1) ) {

			if( pValue->Compile(Text) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Null Data

DWORD CCodedHost::GetNull(UINT Type)
{
	switch( Type ) {

		case typeInteger:

			return 0;

		case typeReal:

			return R2I(0);

		case typeString:

			return DWORD(wstrdup(L""));
		}

	return 0;
	}

// End of File
