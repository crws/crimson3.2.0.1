
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextCommsDevice_HPP

#define INCLUDE_UITextCommsDevice_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Communications Device
//

class CUITextCommsDevice : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextCommsDevice(void);

	protected:
		// Data
		UINT m_Type;

		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(void);
		void AddData(UINT Data, CString Text);
	};

// End of File

#endif
