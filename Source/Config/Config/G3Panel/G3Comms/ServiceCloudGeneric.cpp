
#include "Intern.hpp"

#include "ServiceCloudGeneric.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudTagSet.hpp"
#include "CodedItem.hpp"
#include "MqttClientOptions.hpp"
#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Cloud Service
//

// Base Class

#undef  CBaseClass

#define CBaseClass CServiceCloudJson

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudGeneric, CBaseClass);

// Constructor

CServiceCloudGeneric::CServiceCloudGeneric(void)
{
	m_bServCode = servCloudGeneric;

	m_pPub      = NULL;

	m_pSub      = NULL;

	m_NoDollar  = 1;
}

// Type Access

BOOL CServiceCloudGeneric::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Pub" || Tag == L"Sub" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return CBaseClass::GetTypeData(Tag, Type);
}

// UI Update

void CServiceCloudGeneric::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" || Tag == "Sub" || Tag == "Pub" ) {

			DoEnables(pHost);
		}
	}

	for( UINT n = 0; n < m_Sets; n++ ) {

		CPrintf Name(L"set%u.Export", 1 + n);

		pHost->EnableUI(Name, CheckEnable(m_pEnable, L"==", 1, TRUE) && m_pSet[n]->m_Mode);
	}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
}

// Persistance

void CServiceCloudGeneric::Init(void)
{
	CServiceCloudBase::Init();

	InitOptions();
}

// Download Support

BOOL CServiceCloudGeneric::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pPub);
	Init.AddItem(itemVirtual, m_pSub);

	Init.AddByte(BYTE(m_NoDollar));

	return TRUE;
}

// Meta Data Creation

void CServiceCloudGeneric::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddVirtual(Pub);
	Meta_AddVirtual(Sub);
	Meta_AddInteger(NoDollar);

	Meta_SetName((IDS_GENERIC_MQTT));
}

// Implementation

void CServiceCloudGeneric::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Pub", fEnable);

	pHost->EnableUI(this, "Sub", fEnable);

	pHost->EnableUI(this, "NoDollar", fEnable && (!m_pPub || !m_pSub));
}

void CServiceCloudGeneric::InitOptions(void)
{
	m_pOpts->m_Tls      = FALSE;

	m_pOpts->m_Check    = 3;

	m_pOpts->m_Port     = 1883;

	m_pOpts->m_Advanced = 1;

	m_pOpts->SetClientId(L"device-01");

	m_pOpts->SetPeerName(L"192.168.1.138");

	m_pOpts->SetCredentials(L"user", L"password");
}

// End of File
