
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITagSource_HPP

#define INCLUDE_UITagSource_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UIExpression.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Tag Source
//

class CUITagSource : public CUIExpression
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITagSource(void);
	};

// End of File

#endif
