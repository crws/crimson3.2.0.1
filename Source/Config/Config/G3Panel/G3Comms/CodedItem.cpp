
#include "Intern.hpp"

#include "CodedItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "CommsManager.hpp"
#include "DataServer.hpp"
#include "DispColor.hpp"
#include "DispColorLinked.hpp"
#include "DispFormat.hpp"
#include "DispFormatLinked.hpp"
#include "NameServer.hpp"
#include "ProgramManager.hpp"
#include "ProgramItem.hpp"
#include "Tag.hpp"
#include "TagList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Coded Item
//

// Dynamic Class

AfxImplementDynamicClass(CCodedItem, CMetaItem);

// Constructor

CCodedItem::CCodedItem(void)
{
	m_ReqType  = typeInteger;

	m_ReqFlags = 0;

	m_ActType  = typeVoid;

	m_ActFlags = 0;

	m_RefBits  = 0;

	m_uParent  = NOTHING;

	m_pNext    = NULL;

	m_pPrev    = NULL;
	}

// Server Access

INameServer * CCodedItem::GetNameServer(void) const
{
	CNameServer *pName = m_pSystem->GetNameServer();

	if( pName ) {

		CItem *pItem = GetParent();

		pName->ResetServer();

		if( pItem->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

			CCodedHost *pHost = (CCodedHost *) pItem;

			return pHost->GetNameServer(pName);
			}
		}

	return pName;
	}

IDataServer * CCodedItem::GetDataServer(void) const
{
	CDataServer *pData = m_pSystem->GetDataServer();

	if( pData ) {

		CItem *pItem = GetParent();

		pData->ResetServer();

		if( pItem->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

			CCodedHost *pHost = (CCodedHost *) pItem;

			return pHost->GetDataServer(pData);
			}
		}

	return pData;
	}

// Attributes

BOOL CCodedItem::IsEmpty(void) const
{
	return m_Object.GetCount() == 0;
	}

BOOL CCodedItem::IsBroken(void) const
{
	PCBYTE pSource = m_Source.GetPointer();

	if( pSource ) {

		if( C3CheckSource(pSource) ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedItem::IsLValue(void) const
{
	return (m_ActFlags & (flagWritable | flagArray)) ? TRUE : FALSE;
	}

BOOL CCodedItem::IsWritable(void) const
{
	return (m_ActFlags & flagWritable) ? TRUE : FALSE;
	}

BOOL CCodedItem::IsConst(void) const
{
	return (m_ActFlags & flagConstant) ? TRUE : FALSE;
	}

UINT CCodedItem::GetRefCount(void) const
{
	return m_Refs.GetCount();
	}

CString CCodedItem::GetSource(BOOL fExpand) const
{
	CString Text;

	if( !m_Source.IsEmpty() ) {

		INameServer *pName = fExpand ? GetNameServer() : NULL;

		CStringArray Using;

		C3ExpandSource(m_Source.GetPointer(), pName, Using, Text);
		}

	return Text;
	}

UINT CCodedItem::GetType(void) const
{
	return m_ActType;
	}

UINT CCodedItem::GetFlags(void) const
{
	return m_ActFlags;
	}

BOOL CCodedItem::IsCommsRef(void) const
{
	return m_RefBits ? TRUE : FALSE;
	}

BOOL CCodedItem::IsCommsRefNamed(void) const
{
	return (m_ActFlags & flagCommsTab) ? FALSE : TRUE;
	}

BOOL CCodedItem::IsCommsRefTable(void) const
{
	return (m_ActFlags & flagCommsTab) ? TRUE : FALSE;
	}

UINT CCodedItem::GetCommsBits(void) const
{
	return m_RefBits;
	}

BOOL CCodedItem::IsTagRef(void) const
{
	return (m_ActFlags & flagTagRef) ? TRUE : FALSE;
	}

BOOL CCodedItem::IsLocalRef(void) const
{
	if( m_ActFlags & flagTagRef ) {

		CDataRef const &Ref = GetRef(0);

		if( Ref.t.m_Index >= 0x7F80 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CCodedItem::GetObjectRef(void) const
{
	if( m_Refs.IsEmpty() ) {

		return 0;
		}

	return (m_Refs[0] & 0x7FFF);
	}

UINT CCodedItem::GetIndexRef(void) const
{
	if( m_Refs.IsEmpty() ) {

		return 0;
		}

	return (m_Refs[0] & 0x00FF);
	}

WORD CCodedItem::GetTagRef(void) const
{
	CDataRef const &Ref = GetRef(0);

	return LOWORD(Ref.m_Ref);
	}

UINT CCodedItem::GetTagIndex(void) const
{
	CDataRef const &Ref = GetRef(0);

	return Ref.t.m_Index;
	}

BOOL CCodedItem::GetTagItem(CTag * &pTag) const
{
	IDataServer * pData = GetDataServer();

	CTag        * pFind = (CTag *) pData->GetItem(m_Refs[0]);

	return (pTag = pFind) ? TRUE : FALSE;
	}

CString CCodedItem::GetTagName(void) const
{
	CString Text;

	INameServer * pName = GetNameServer();

	pName->NameIdent(LOWORD(m_Refs[0]), Text);

	return Text;
	}

CString CCodedItem::GetTagPath(void) const
{
	IDataServer * pData = GetDataServer();

	CTag        * pFind = (CTag *) pData->GetItem(m_Refs[0]);

	return pFind->GetFixedPath();
	}

CString CCodedItem::GetFindInfo(void) const
{
	CItem *pParent = GetParent();

	if( pParent->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

		CUIItem *pItem = (CUIItem *) pParent;

		CString Text;

		CString Field;

		CMetaList const *pList = pItem->FindMetaList();

		for( UINT n = 0; n < pList->GetCount(); n++ ) {

			CMetaData const *pMeta = pList->FindData(n);

			if( pMeta->GetType() == metaVirtual ) {

				if( pMeta->GetObject(pItem) == (CItem *) this ) {

					Field = L':' + pMeta->GetTag();

					break;
					}
				}
			}

		Text += pItem->GetHumanPath();

		Text += L" - ";

		Text += pItem->GetSubItemLabel(this);

		Text += '\n';

		Text += pItem->GetFixedPath();

		Text += Field;

		return Text;
		}

	return L"";
	}

CString CCodedItem::GetPropName(void) const
{
	CItem *pParent = GetParent();

	if( pParent->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

		CUIItem         *pItem = (CUIItem *) pParent;

		CMetaList const *pList = pItem->FindMetaList();

		for( UINT n = 0; n < pList->GetCount(); n++ ) {

			CMetaData const *pMeta = pList->FindData(n);

			if( pMeta->GetType() == metaVirtual ) {

				if( pMeta->GetObject(pItem) == (CItem *) this ) {

					return pMeta->GetTag();
					}
				}
			}
		}

	return L"";
	}

// Operations

BOOL CCodedItem::Compile(CString Text)
{
	return Compile(CError(FALSE), Text);
	}

BOOL CCodedItem::Compile(CError &Error, CString Text)
{
	return Compile(Error, Text, L"");
	}

BOOL CCodedItem::Compile(CError &Error, CString Text, CString Prop)
{
	CByteArray  Source;

	CLongArray  Refs;

	CByteArray  Object;

	CCompileIn  In;

	CCompileOut Out;

	In.m_pText        = Text;
	In.m_Optim        = m_pSystem->m_pPrograms->m_OptLevel;
	In.m_Debug        = 0;
	In.m_Switch       = 0;
	In.m_uParam       = 0;
	In.m_pParam       = NULL;
	In.m_pName        = GetNameServer();
	In.m_Type.m_Type  = m_ReqType;
	In.m_Type.m_Flags = m_ReqFlags;

	if( m_pSystem->m_pPrograms->m_DebugEnable ) {

		if( m_ReqFlags & flagActive ) {

			if( m_pSystem->m_pPrograms->m_DebugActive ) {

				In.m_Debug = m_pSystem->m_pPrograms->m_DebugFlags;
				}
			}
		else {
			if( m_pSystem->m_pPrograms->m_DebugPassive ) {

				In.m_Debug = m_pSystem->m_pPrograms->m_DebugFlags;
				}
			}
		}

	if( In.m_Debug ) {

		In.m_Scope = GetParent()->GetHumanPath();

		if( !Prop.IsEmpty() ) {

			In.m_Scope += L"." + Prop;
			}

		In.m_Scope.Replace(L" - ", L".");

		In.m_Scope.Replace(L" ",   L"");
		}

	if( m_pSystem->m_pPrograms->m_AutoCast ) {

		In.m_Switch |= optAutoCast;
		}

	if( m_pSystem->m_pPrograms->m_PrecFix ) {

		In.m_Switch |= optPrecFix;
		}

	Out.m_Error    = Error;
	Out.m_pSource  = &Source;
	Out.m_pRefList = &Refs;
	Out.m_pObject  = &Object;

	if( DoCompile(In, Out) ) {

		m_Source = Source;

		m_Object = Object;

		if( IsBroken() ) {

			m_ActType  = m_ReqType;

			m_ActFlags = m_ReqFlags;
			}
		else {
			m_Refs     = Refs;

			m_ActType  = Out.m_Type.m_Type;

			m_ActFlags = Out.m_Type.m_Flags;

			FindRefBits();
			}

		return TRUE;
		}

	Error = Out.m_Error;

	return FALSE;
	}

BOOL CCodedItem::Recompile(CError &Error, BOOL fExpand)
{
	return OnRecompile(Error, fExpand);
	}

BOOL CCodedItem::Recompile(BOOL fExpand)
{
	return OnRecompile(CError(FALSE), fExpand);
	}

BOOL CCodedItem::Recompile(void)
{
	return OnRecompile(CError(FALSE), FALSE);
	}

BOOL CCodedItem::SetBroken(void)
{
	CString Source = GetSource(TRUE);

	if( !Source.IsEmpty() ) {

		if( Source.StartsWith(L"WAS ") ) {

			m_pDbase->SetRecomp();

			Compile(L"WAS " + Source);
			}

		return TRUE;
		}

	return FALSE;
	}

void CCodedItem::SetReqType(CTypeDef const &Type)
{
	m_ReqType  = Type.m_Type;

	m_ReqFlags = Type.m_Flags;
	}

void CCodedItem::SetParams(CString const &Params)
{
	m_Params = Params;
	}

BOOL CCodedItem::CheckCircular(CTag *pFind, BOOL fFormat)
{
	CCodedTree Busy;

	CTagList *pList = (CTagList *) pFind->GetParent();

	if( FindTagRef(Busy, pList, pFind, fFormat) ) {

		m_pDbase->SetCircle();

		return TRUE;
		}

	return FALSE;
	}

BOOL CCodedItem::UpdateExtent(UINT uSize)
{
	if( uSize > 0 ) {

		if( !IsBroken() ) {

			if( IsCommsRefNamed() ) {

				// TODO -- What the heck is this meant to do???

				DWORD     Ref1 = m_Refs[0];

				DWORD     Data = m_pSystem->m_pComms->GetRefAddress(Ref1);

				UINT      uDev = m_pSystem->m_pComms->GetRefDevice (Ref1);

				CAddress &Addr = (CAddress &) Data;

				DWORD     Ref2 = 0;

				m_pSystem->m_pComms->ResolveAddress(Ref2, uDev, Addr);

				if( Ref1 != Ref2 ) {

					Recompile();
					}

				return TRUE;
				}
			else {
				DWORD     Ref1 = m_Refs[0];

				DWORD     Data = m_pSystem->m_pComms->GetRefAddress(Ref1);

				UINT      uDev = m_pSystem->m_pComms->GetRefDevice (Ref1);

				CAddress &Addr = (CAddress &) Data;

				DWORD     Ref2 = 0;

				m_pSystem->m_pComms->ResolveAddress(Ref2, uDev, Addr, uSize);

				if( Ref1 != Ref2 ) {

					// LATER -- Is this the right approach???

					Recompile();
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CCodedItem::Empty(void)
{
	m_Source.Empty();

	m_Object.Empty();

	m_Refs.Empty();

	m_ActType  = typeVoid;

	m_ActFlags = 0;
	}

// Overridables

void CCodedItem::LoadParams(CCompileIn &In, BOOL &fFunc)
{
	if( !m_Params.IsEmpty() ) {

		CStringArray List;

		m_Params.Tokenize(List, '/');

		if( List.GetCount() ) {

			CCodedHost *pHost = (CCodedHost *) GetParent();

			In.m_uParam = List.GetCount();

			In.m_pParam = New CFuncParam [ In.m_uParam ];

			for( UINT n = 0; n < In.m_uParam; n++ ) {

				CStringArray Part;

				List[n].Tokenize(Part, '=');

				CTypeDef Type;

				pHost->GetTypeData(Part[1], Type);

				In.m_pParam[n].m_Name = Part[0];

				In.m_pParam[n].m_Type = Type.m_Type;
				}
			}
		}

	fFunc = wstrchr(In.m_pText, L'\n') ? TRUE : FALSE;
	}

void CCodedItem::KillParams(CCompileIn &In)
{
	delete [] In.m_pParam;
	}

// Execution

DWORD CCodedItem::ExecVal(void) const
{
	return Execute(GetType());
	}

DWORD CCodedItem::Execute(UINT Type) const
{
	return Execute(Type, NULL);
	}

DWORD CCodedItem::Execute(UINT Type, PDWORD pParam) const
{
	if( !IsBroken() ) {

		IDataServer *pData = GetDataServer();

		PCBYTE       pCode = m_Object.GetPointer();

		if( pCode ) {

			DWORD Data = C3ExecuteCode(pCode, pData, pParam);

			if( IsLValue() ) {

				if( Type < typeLValue ) {

					Data = pData->GetData(Data, GetType(), getNone);
					}
				}

			if( Type == typeInteger ) {

				if( GetType() == typeReal ) {

					Data = C3INT(I2R(Data));
					}
				}

			if( Type == typeReal ) {

				if( GetType() == typeInteger ) {

					Data = R2I(C3REAL(C3INT(Data)));
					}
				}

			return Data;
			}
		}

	return GetNull(Type);
	}

DWORD CCodedItem::GetNull(UINT Type) const
{
	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
		}

	if( Type == typeReal ) {

		return R2I(0);
		}

	return 0;
	}

DWORD CCodedItem::GetNull(void) const
{
	return GetNull(GetType());
	}

// Reference Access

CDataRef const & CCodedItem::GetRef(UINT uRef) const
{
	return (CDataRef const &) m_Refs[uRef];
	}

// Persistance

void CCodedItem::Init(void)
{
	CMetaItem::Init();

	m_Source.Append(0x00);

	FindSystem();

	ListAppend();

	FindParent();
	}

void CCodedItem::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"Ref" ) {

			DWORD Ref = Tree.GetValueAsInteger();

			if( m_Refs.Failed(m_Refs.Find(Ref)) ) {

				m_Refs.Append(Ref);
				}
			}
		else {
			CMetaData const *pMeta = m_pList->FindData(Name);

			if( pMeta ) {

				LoadProp(Tree, pMeta);

				continue;
				}
			}
		}

	FindSystem();
	}

void CCodedItem::PostLoad(void)
{
	CMetaItem::PostLoad();

	ListAppend();

	FindParent();
	}

void CCodedItem::PreCopy(void)
{
	Recompile(CError(FALSE), TRUE);
	}

void CCodedItem::PostPaste(void)
{
	CMetaItem::PostPaste();

	FindSystem();

	Recompile(CError(FALSE), FALSE);
	}

void CCodedItem::Save(CTreeFile &Tree)
{
	CMetaItem::Save(Tree);

	UINT uCount = m_Refs.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		Tree.PutValue(L"Ref", m_Refs[n]);
		}
	}

void CCodedItem::Kill(void)
{
	m_Refs.Empty();

	ListRemove();

	CMetaItem::Kill();
	}

// Property Save Filter

BOOL CCodedItem::SaveProp(CString const &Tag) const
{
	if( Tag == L"RefBits" ) {

		return m_RefBits ? TRUE : FALSE;
		}

	if( Tag == L"ReqFlags" ) {

		return m_ReqFlags ? TRUE : FALSE;
		}

	if( Tag == L"ActFlags" ) {

		return m_ActFlags ? TRUE : FALSE;
		}

	if( Tag == L"Params" ) {

		return !m_Params.IsEmpty();
		}

	return CMetaItem::SaveProp(Tag);
	}

// Download Support

BOOL CCodedItem::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	UINT uCount = m_Refs.GetCount();

	Init.AddWord(WORD(uCount));

	UINT uScan = uCount;

	UINT uPos  = Init.GetCount();

	for( UINT n = 0; n < uScan; n++ ) {

		CDataRef const &Ref = (CDataRef const &) m_Refs[n];

		if( !Ref.t.m_IsTag && !Ref.b.m_Block ) {

			UINT uHandle = WORD(Ref.b.m_Offset);

			if( SkipHandle(uHandle) ) {

				uCount--;

				continue;
				}
			}

		Init.AddLong(m_Refs[n]);
		}

	if( uCount < uScan ) {

		uPos -= sizeof(WORD);

		Init.SetWord(uPos, WORD(uCount));
		}

	Init.AddWord(WORD(m_Object.GetCount()));

	Init.AddData(m_Object.GetPointer(), m_Object.GetCount());

	return TRUE;
	}

// Meta Data Creation

void CCodedItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddBlob   (Source);
	Meta_AddBlob   (Object);
	Meta_AddString (Params);
	Meta_AddInteger(ReqType);
	Meta_AddInteger(ReqFlags);
	Meta_AddInteger(ActType);
	Meta_AddInteger(ActFlags);
	Meta_AddInteger(RefBits);

	Meta_SetName((IDS_CODED_ITEM));
	}

// Recompile Hook

BOOL CCodedItem::OnRecompile(CError &Error, BOOL fExpand)
{
	CString Source = GetSource(fExpand);

	if( !Source.IsEmpty() ) {

		if( Source.StartsWith(L"WAS ") ) {

			Source = Source.Mid(4);

			if( Compile(Error, Source, GetPropName()) ) {

				return TRUE;
				}

			m_pDbase->SetRecomp();

			return FALSE;
			}

		if( !Compile(Error, Source, GetPropName()) ) {

			m_pDbase->SetRecomp();

			Compile(L"WAS " + Source);

			return FALSE;
			}
		}

	return TRUE;
	}

// Implementation

BOOL CCodedItem::DoCompile(CCompileIn &In, CCompileOut &Out)
{
	BOOL fFunc;

	LoadParams(In, fFunc);

	BOOL fOkay = C3CompileCode(In, Out, fFunc);

	KillParams(In);

	return fOkay;
	}

void CCodedItem::FindSystem(void)
{
	m_pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();
	}

BOOL CCodedItem::FindRefBits(void)
{
	if( m_ActFlags & flagCommsRef ) {

		if( m_ActFlags & flagBitRef ) {

			m_RefBits = 1;

			return TRUE;
			}

		DWORD      Find = m_pSystem->m_pComms->GetRefAddress(m_Refs[0]);

		CAddress & Addr = (CAddress &) Find;

		m_RefBits  = C3GetTypeLocalBits(Addr.a.m_Type);

		return TRUE;
		}

	m_RefBits = 0;

	return FALSE;
	}

BOOL CCodedItem::FindTagRef(CCodedTree &Busy, CTagList *pList, CTag *pFind, BOOL fFormat)
{
	if( Busy.Failed(Busy.Find(this)) ) {

		Busy.Insert(this);

		for( UINT r = 0; r < m_Refs.GetCount(); r++ ) {

			CDataRef const &Ref = GetRef(r);

			if( Ref.t.m_IsTag ) {

				CTag *pTag = pList->GetItem(Ref.t.m_Index);

				if( pTag ) {

					if( pTag == pFind ) {

						Busy.Remove(this);

						return TRUE;
						}

					if( pTag->m_pValue ) {

						if( pTag->m_pValue->FindTagRef(Busy, pList, pFind, fFormat) ) {

							Busy.Remove(this);

							return TRUE;
							}
						}

					if( fFormat ) {
						
						if( pTag->m_pFormat ) {

							if( pTag->m_pFormat->GetFormType() == 7 ) {

								CDispFormatLinked *pFormat = (CDispFormatLinked *) pTag->m_pFormat;

								CCodedItem        *pLink   = pFormat->m_pLink;

								if( pLink ) {

									if( pLink->FindTagRef(Busy, pList, pFind, fFormat) ) {

										Busy.Remove(this);

										return TRUE;
										}
									}
								}
							}

						if( pTag->m_pColor ) {

							if( pTag->m_pColor->GetColType() == 4 ) {

								CDispColorLinked *pColor = (CDispColorLinked *) pTag->m_pColor;

								CCodedItem        *pLink = pColor->m_pLink;

								if( pLink ) {

									if( pLink->FindTagRef(Busy, pList, pFind, fFormat) ) {

										Busy.Remove(this);

										return TRUE;
										}
									}
								}
							}
						}
					}
				}
			}

		Busy.Remove(this);
		}

	return FALSE;
	}

BOOL CCodedItem::FindParent(void)
{
	CItem *pItem = this;

	while( pItem ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

			CMetaItem       *pHost = (CMetaItem *) pItem;

			CMetaData const *pMeta = pHost->FindMetaData(L"Handle");

			if( pMeta ) {

				m_uParent = pMeta->ReadInteger(pHost);

				return TRUE;
				}
			}

		pItem = pItem->GetParent();
		}

	return FALSE;
	}

BOOL CCodedItem::SkipHandle(UINT uHandle)
{
	if( uHandle != m_uParent ) {

		CItem *pItem = m_pDbase->GetHandleItem(uHandle);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

void CCodedItem::ListAppend(void)
{
	if( !m_pNext && !m_pPrev ) {

		AfxListAppend( m_pSystem->m_pHeadCoded,
			       m_pSystem->m_pTailCoded,
			       this,
			       m_pNext,
			       m_pPrev
			       );
		}
	}

void CCodedItem::ListRemove(void)
{
	if( m_pNext || m_pPrev ) {

		AfxListRemove( m_pSystem->m_pHeadCoded,
			       m_pSystem->m_pTailCoded,
			       this,
			       m_pNext,
			       m_pPrev
			       );

		m_pNext = NULL;

		m_pPrev = NULL;
		}
	}

// End of File
