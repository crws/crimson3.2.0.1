
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagSetWnd_HPP

#define INCLUDE_TagSetWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CTagList;
class CTagSet;

//////////////////////////////////////////////////////////////////////////
//
// Tag Set Window
//

class CTagSetWnd : public CCtrlWnd, public IDropSource, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTagSetWnd(CUIElement *pUI);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropSource
		HRESULT METHOD QueryContinueDrag(BOOL fEscape, DWORD dwKeys);
		HRESULT METHOD GiveFeedback(DWORD dwEffect);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Operations
		void Attach(CItem *pItem);

		// Operations
		void    LoadTree(CString Data);
		CString SaveTree(void);

	protected:
		// Bound Items
		CUIElement   * m_pUI;
		CTagSet      * m_pItem;
		CDatabase    * m_pDbase;
		CCommsSystem * m_pSystem;
		CTagList     * m_pTags;
		INameServer  * m_pName;

		// Data Members
		CImageList    m_Images;
		CTreeView   * m_pTree;
		DWORD         m_dwStyle;
		HTREEITEM     m_hRoot;
		HTREEITEM     m_hSelect;
		BOOL          m_fLoading;
		CCursor       m_MoveCursor;
		CString       m_Latest;

		// Drop Context
		UINT          m_uDrop;
		CDropHelper   m_DropHelp;
		BOOL	      m_fDropShow;
		HTREEITEM     m_hDropPrev;
		UINT	      m_cfIdent;
		UINT          m_cfTagList;
		DWORD	      m_dwEffect;

		// Drag Context
		CPoint	m_DragPos;
		CSize	m_DragOffset;
		CSize	m_DragSize;
		CBitmap	m_DragImage;

		// Message Map
		AfxDeclareMessageMap();

		// Accelerator
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnEnable(BOOL fEnable);

		// Notification Handlers
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeExpanding(UINT uID, NMTREEVIEW &Info);
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);
		void OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info);

		// Command Handlers
		BOOL OnPasteControl(UINT uID, CCmdSource &Src);
		BOOL OnPasteCommand(UINT uID);

		// Tree Loading
		void LoadRoot(void);

		// Drag Support
		void DragItem(void);

		// Drag Hooks
		void FindDragMetrics(void);
		void MakeDragImage(void);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL AddIdentifier(CDataObject *pData);

		// Data Object Acceptance
		BOOL CanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL AcceptData(IDataObject *pData);

		// Data Object Helpers
		BOOL CanAcceptIdent(IDataObject *pData);
		BOOL CanAcceptTagList(IDataObject *pData);
		BOOL AcceptIdent(IDataObject *pData, CStringArray &List);
		BOOL AcceptTagList(IDataObject *pData, CStringArray &List);

		// Drop Support
		void DropTrack(CPoint Pos, BOOL fMove);
		void ShowDrop(BOOL fShow);

		// Implementation
		BOOL    HasCode(CString Code);
		CString GetItemCode(HTREEITEM hItem);
		BOOL    GetCodeNode(CTreeViewItem &Node, CString Code);
		BOOL    SetDrop(UINT uDrop);
		void    ForceUpdate(void);
		void    WalkToLast(HTREEITEM &hItem);
		void    DeleteItem(void);
		UINT    MapImage(UINT uImage);
	};

// End of File

#endif
