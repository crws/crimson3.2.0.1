
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagResTreeWnd_HPP

#define INCLUDE_TagResTreeWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Tag Resource Window
//

class CTagResTreeWnd : public CResTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagResTreeWnd(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		UINT m_cfCode;
		UINT m_cfTagList;
		UINT m_cfFolder;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handers
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Notification Handlers
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);

		// Command Handlers
		BOOL OnResGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnResControl(UINT uID, CCmdSource &Src);
		BOOL OnResCommand(UINT uID);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL AddCodeFragment(CDataObject *pData);
		BOOL AddTagList(CDataObject *pData);
		BOOL AddFolder(CDataObject *pData);
		void AddChildTags(CString &List, HTREEITEM hRoot);
		BOOL AddIdentifier(CDataObject *pData);
		void AddName(CString &List, CString Name);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);

		// Implementation
		BOOL CanExpandTag(void);
		BOOL CanCollapseTag(void);
		void OnExpandTag(HTREEITEM hItem);
		void OnCollapseTag(void);
	};

// End of File

#endif
