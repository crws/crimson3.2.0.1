
#include "Intern.hpp"

#include "ProgramList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program List
//

// Dynamic Class

AfxImplementDynamicClass(CProgramList, CNamedList);

// Constructor

CProgramList::CProgramList(void)
{
	}

// Item Access

CMetaItem * CProgramList::GetItem(INDEX Index) const
{
	return (CMetaItem *) CItemIndexList::GetItem(Index);
	}

CMetaItem * CProgramList::GetItem(UINT uPos) const
{
	return (CMetaItem *) CItemIndexList::GetItem(uPos);
	}

// Item Lookup

UINT CProgramList::FindNamePos(CString Name) const
{
	CItem *pItem = FindByName(Name);

	if( pItem ) {

		CProgramItem *pProgram = (CProgramItem *) pItem;

		return pProgram->GetIndex();
		}

	return NOTHING;
	}

// Operations

void CProgramList::UpdatePending(void)
{
	BOOL  p = FALSE;

	INDEX n = GetHead();

	while( !Failed(n) ) {

		CMetaItem *pItem = GetItem(n);

		if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

			CProgramItem *pProg = (CProgramItem *) pItem;

			if( pProg->m_Pending ) {

				p = TRUE;

				break;
				}
			}

		GetNext(n);
		}

	m_pDbase->SetPending(p);
	}

// Persistance

void CProgramList::Init(void)
{
	CItemIndexList::Init();

	CMetaItem *pItem = New CProgramItem;

	AppendItem(pItem);

	pItem->SetName(L"Program1");
	}

// End of File
