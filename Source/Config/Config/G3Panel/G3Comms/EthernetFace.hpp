
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_EthernetFace_HPP

#define INCLUDE_EthernetFace_HPP

//////////////////////////////////////////////////////////////////////////
//
// Default IPs
//

#define	IP1_STD	DWORD(MAKELONG(MAKEWORD( 20, 1), MAKEWORD(168, 192)))

#define	IP2_STD	DWORD(MAKELONG(MAKEWORD( 20, 2), MAKEWORD(168, 192)))

#define	IP1_HON	DWORD(MAKELONG(MAKEWORD(253, 1), MAKEWORD(168, 192)))

#define	IP2_HON	DWORD(MAKELONG(MAKEWORD(253, 2), MAKEWORD(168, 192)))

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Port Configuration
//

class DLLNOT CEthernetFace : public CCodedHost
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEthernetFace(UINT uPort);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		DWORD GetAddress(void) const;

		// Operations
		void PostConvert(BOOL fValid);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pMode;
		CCodedItem * m_pEnableFull;
		CCodedItem * m_pEnable100;
		CCodedItem * m_pAddress;
		CCodedItem * m_pNetMask;
		CCodedItem * m_pGateway;
		CCodedItem * m_pMetric;
		CCodedItem * m_pDNSMode;
		CCodedItem * m_pDNS1;
		CCodedItem * m_pDNS2;
		CCodedItem * m_pSendMSS;
		CCodedItem * m_pRecvMSS;

	protected:
		// Data Members
		UINT m_uPort;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
