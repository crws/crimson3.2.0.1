
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlSync_HPP

#define INCLUDE_SqlSync_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Configuration
//

class CSqlSync : public CServiceItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlSync(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		BOOL IsEnabled(void) const;
		UINT GetTreeImage(void) const;

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		void Load(CTreeFile &File);
		void LoadNames(CTreeFile &File);
		void Save(CTreeFile &File);

		// Item Properties
		CCodedItem * m_pEnable;
		CCodedItem * m_pDatabaseServer;
		CCodedItem * m_pPort;
		CCodedItem * m_pTlsMode;
		CCodedItem * m_pUser;
		CCodedItem * m_pPass;
		CCodedItem * m_pDatabaseName;
		CCodedItem * m_pTablePrefix;
		CCodedItem * m_pFreq;
		CCodedItem * m_pFreqMins;
		CCodedItem * m_pDelay;
		CCodedItem * m_pLogFile;
		CCodedItem * m_pForce;
		CCodedItem * m_pCheckLabels;
		CCodedItem * m_pLogSelect;
		CCodedItem * m_pSyncSecurity;
		CCodedItem * m_pSyncEvents;
		CCodedItem * m_pFireTriggers;
		CCodedItem * m_pDrive;
		UINT         m_PrimaryKey;

		CStringArray m_LogNames;
		UINT	     m_LogInit;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void IncludeAllLogs(CStringArray &LogNames);
		void IncludeEventsSecure(CStringArray &LogNames);
	};

// End of File

#endif
