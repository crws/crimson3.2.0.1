
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsDevicePage_HPP

#define INCLUDE_CommsDevicePage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;

//////////////////////////////////////////////////////////////////////////
//
// Comms Device Page
//

class CCommsDevicePage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCommsDevicePage(CCommsDevice *pDevice);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CCommsDevice * m_pDevice;

		// Implementation
		BOOL LoadBasePage(IUICreate *pView);
		BOOL LoadDeviceConfig(IUICreate *pView);
		BOOL LoadMasterConfig(IUICreate *pView);
		BOOL LoadButtons(IUICreate *pView);
	};

// End of File

#endif
