
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQueryManagerTreeWnd_CCmdDelete_HPP

#define INCLUDE_SqlQueryManagerTreeWnd_CCmdDelete_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SqlQueryManagerTreeWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Tree Window -- Delete Command
//

class CSqlQueryManagerTreeWnd::CCmdDelete : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdDelete(CItem *pRoot, CString List, CItem *pNext, CMetaItem *pItem);

		// Destructor
		~CCmdDelete(void);

		// Data Members
		CString m_Root;
		CString m_List;
		CString m_Next;
		HGLOBAL m_hData;
	};

// End of File

#endif
