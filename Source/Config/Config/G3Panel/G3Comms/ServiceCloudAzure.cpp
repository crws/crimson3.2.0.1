
#include "Intern.hpp"

#include "ServiceCloudAzure.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "MqttClientOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Azure Cloud Service
//

// Base Class

#undef  CBaseClass

#define CBaseClass CServiceCloudJson

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudAzure, CBaseClass);

// Constructor

CServiceCloudAzure::CServiceCloudAzure(void)
{
	m_bServCode = servCloudAzure;

	m_pPub      = NULL;

	m_pSub      = NULL;

	m_pKey      = NULL;

	m_Twin      = 1;
}

// Type Access

BOOL CServiceCloudAzure::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Pub" || Tag == L"Sub" || Tag == L"Key" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return CBaseClass::GetTypeData(Tag, Type);
}

// UI Update

void CServiceCloudAzure::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			DoEnables(pHost);
		}

		if( Tag == "Twin" ) {

			DoEnables(pHost);
		}
	}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
}

// Persistance

void CServiceCloudAzure::Init(void)
{
	CBaseClass::Init();

	InitOptions();
}

// Download Support

BOOL CServiceCloudAzure::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddByte(BYTE(m_Twin));

	Init.AddItem(itemVirtual, m_pPub);

	Init.AddItem(itemVirtual, m_pSub);

	Init.AddItem(itemVirtual, m_pKey);

	return TRUE;
}

// Meta Data Creation

void CServiceCloudAzure::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(Twin);
	Meta_AddVirtual(Pub);
	Meta_AddVirtual(Sub);
	Meta_AddVirtual(Key);

	Meta_SetName((IDS_AZURE_MQTT));
}

// Implementation

void CServiceCloudAzure::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Twin", fEnable);

	pHost->EnableUI(this, "Pub", fEnable && !m_Twin);

	pHost->EnableUI(this, "Sub", fEnable);

	pHost->EnableUI(this, "Key", fEnable);
}

void CServiceCloudAzure::InitOptions(void)
{
	m_pOpts->SetClientId(L"CrimsonDevice01");

	m_pOpts->m_Check    = 0;

	m_pOpts->m_Advanced = 1;

	m_pOpts->SetPeerName(L"iot-hub-name.azure-devices.net");
}

// End of File
