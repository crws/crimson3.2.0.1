
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ColorMenuHost_HPP

#define INCLUDE_ColorMenuHost_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CColorManager;

//////////////////////////////////////////////////////////////////////////
//
// Color Picker Host
//

class DLLNOT CColorMenuHost : public CMenuWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CColorMenuHost(CColorManager *pColor);

		// Destructor
		~CColorMenuHost(void);

		// Operations
		BOOL Select(CPoint Pos, COLOR &Color);

		// Callback Hook
		void TrackPos(CPoint Pos);
		void KeyDown(UINT uCode);

	protected:
		// Static Data
		static CColorMenuHost *m_pThis;

		// Data Members
		CColorManager * m_pColor;
		CMenu         * m_pMenu;
		UINT		m_uSpec;
		HHOOK		m_hHook;
		CPoint		m_Pos;
		UINT		m_uIndex;
		UINT		m_uGroup;
		UINT		m_uTrack;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu);
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item);

		// Command Handlers
		BOOL OnColorGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnColorControl(UINT uID, CCmdSource &Src);
		BOOL OnColorExecute(UINT uID);

		// Implementation
		void DrawSpecial(CDC &DC, DRAWITEMSTRUCT &Item);
		void DrawColors(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState);
		void DrawBack(CDC &DC, CRect Rect, UINT uState);
		void MakeMenu(void);
		void FreeMenu(void);
		void RedrawMenu(void);
		BOOL IsSpecial(UINT uID);
		BOOL IsPickMore(UINT uID);
		UINT GetGroup(UINT uID);

		// Hook Procedure
		static LRESULT CALLBACK MessageProc(int nCode, WPARAM wParam, LPARAM lParam);
	};

// End of File

#endif
