
#include "Intern.hpp"

#include "ColorManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Color Manager
//

// No Inlining

#pragma inline_depth(0)

// Dynamic Class

AfxImplementDynamicClass(CColorManager, CUIItem);

// Constructor

CColorManager::CColorManager(void)
{
	memset(m_Custom, 0, sizeof(m_Custom));

	memset(m_Select, 0, sizeof(m_Select));

	m_Custom[0] = GetRGB(24,12,12);
	m_Custom[1] = GetRGB(12,24,12);
	m_Custom[2] = GetRGB(12,12,24);
	m_Custom[3] = GetRGB(24,24,12);
	m_Custom[4] = GetRGB(12,24,24);
	m_Custom[5] = GetRGB(24,12,24);

	m_Select[0] = 5;
	m_Select[1] = 5;
	m_Select[2] = 5;
	m_Select[3] = 5;
	m_Select[4] = 5;
	m_Select[5] = 5;

	m_Now = 10;
	}

// Attributes

UINT CColorManager::GetGroupCount(void) const
{
	UINT uCount = 6;

	while( GetCount(uCount) ) {

		uCount++;
		}

	return uCount;
	}

UINT CColorManager::GetCount(UINT uGroup) const
{
	if( uGroup >= 6 ) {

		UINT n;

		for( n = 0; n < 8; n++ ) {

			if( !GetColor(uGroup, n) ) {

				break;
				}
			}

		return n;
		}

	return 8;
	}

COLOR CColorManager::GetColor(UINT uGroup, UINT uIndex) const
{
	static BYTE const c[] = {

		0x0, 0xF, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE,
		0x8, 0x7, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6
		};

	switch( uGroup ) {

		case 0:
		case 1:
			uIndex += 0x08 * (uGroup - 0);

			return C3StdColorData(c[uIndex]);

		case 2:
		case 3:
		case 4:
		case 5:
			uIndex += 0x10;

			uIndex += 0x08 * (uGroup - 2);

			return C3StdColorData(uIndex);

		case 6:
		case 7:
		case 8:
			uIndex += 0x08 * (uGroup - 6);

			return COLOR(m_Custom[uIndex]);
		}

	return 0;
	}

CString CColorManager::GetName(UINT uGroup, UINT uIndex) const
{
	static BYTE const c[] = {

		0x0, 0xF, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE,
		0x8, 0x7, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6
		};

	switch( uGroup ) {

		case 0:
		case 1:
			uIndex += 0x08 * (uGroup - 0);

			return C3StdColorName(c[uIndex]);

		case 2:
		case 3:
		case 4:
		case 5:
			uIndex += 0x10;

			uIndex += 0x08 * (uGroup - 2);

			return C3StdColorName(uIndex);

		case 6:
		case 7:
		case 8:
			uIndex += 0x08 * (uGroup - 6);

			return CPrintf(IDS_FORMAT, uIndex+1);
		}

	return L"Unknown";
	}

BOOL CColorManager::NeedsSep(UINT uGroup) const
{
	switch( uGroup ) {

		case 1:
		case 5:
			return TRUE;
		}

	return FALSE;
	}

// Operations

void CColorManager::UpdateAux(void)
{
	CATLIST List = m_pDbase->GetCatList();

	if( List.GetCount() ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			if( List[n].m_pDbase != m_pDbase ) {

				CSystemItem *pSys = List[n].m_pDbase->GetSystemItem();

				if( pSys->IsKindOf(AfxRuntimeClass(CCommsSystem)) ) {

					CCommsSystem *pAux = (CCommsSystem *) pSys;

					pAux->m_pColor->Sync(this);
					}
				}
			}
		}
	}

void CColorManager::Sync(CColorManager *pThat)
{
	for( UINT i = 0; i < elements(m_Custom); i++ ) {

		m_Custom[i] = pThat->m_Custom[i];

		m_Select[i] = pThat->m_Select[i];
		}

	m_Now = pThat->m_Now;
	}

BOOL CColorManager::FindColor(COLOR Color, UINT &uGroup, UINT &uIndex)
{
	UINT ng = GetGroupCount();

	for( UINT g = 0; g < ng; g++ ) {

		UINT ni = GetCount(g);

		for( UINT i = 0; i < ni; i++ ) {

			if( Color == GetColor(g, i) ) {

				uGroup = g;

				uIndex = i;

				return TRUE;
				}
			}
		}

	UINT nc = elements(m_Custom);

	UINT ms = NOTHING;

	UINT mc = 0;

	for( UINT c = 0; c < nc; c++ ) {

		if( m_Custom[c] == 0 ) {

			mc = c;

			break;
			}

		if( m_Select[c] < ms ) {

			ms = m_Select[c];

			mc = c;
			}
		}

	m_Custom[mc] = Color;

	m_Select[mc] = m_Now++;

	uGroup       = 6 + (mc / 8);

	uIndex       = (mc % 8);

	UpdateAux();

	return TRUE;
	}

void CColorManager::MarkColor(UINT uGroup, UINT uIndex)
{
	if( uGroup >= 6 ) {

		uIndex += 0x08 * (uGroup - 6);

		m_Select[uIndex] = m_Now++;
		}
	}

void CColorManager::MarkColor(COLOR Color)
{
	UINT uGroup, uIndex;

	FindColor(Color, uGroup, uIndex);

	MarkColor(uGroup, uIndex);
	}

// Meta Data Creation

void CColorManager::AddMetaData(void)
{
	CUIItem::AddMetaData();

	for( UINT n = 0; n < elements(m_Custom); n++ ) {

		Meta_Add( CPrintf(L"Custom%2.2u", n),
			  m_Custom[n],
			  metaInteger
			  );

		Meta_Add( CPrintf(L"Select%2.2u", n),
			  m_Select[n],
			  metaInteger
			  );
		}

	Meta_SetName((IDS_COLOR_MANAGER));
	}

// End of File
