
#include "Intern.hpp"

#include "CommsMappingViewWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping View Window
//

// Dynamic Class

AfxImplementDynamicClass(CCommsMappingViewWnd, CViewWnd);

// Constructor

CCommsMappingViewWnd::CCommsMappingViewWnd(void)
{
	}

// Message Map

AfxMessageMap(CCommsMappingViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CCommsMappingViewWnd)
	};

// Message Handlers

void CCommsMappingViewWnd::OnPostCreate(void)
{
	m_System.Bind(this);
	}

void CCommsMappingViewWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	m_System.SetTwinMode(fShow);
	}

void CCommsMappingViewWnd::OnSetFocus(CWnd &Wnd)
{
	m_System.FlipToItemView(TRUE);
	}

void CCommsMappingViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.FillRect(GetClientRect(), afxBrush(TabFace));
	}

// End of File
