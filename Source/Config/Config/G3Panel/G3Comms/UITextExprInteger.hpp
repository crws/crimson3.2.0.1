
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextExprInteger_HPP

#define INCLUDE_UITextExprInteger_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextCoded.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable Integer
//

class CUITextExprInteger : public CUITextCoded
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextExprInteger(void);

	protected:
		// Data Members
		UINT m_uPlaces;
		INT  m_nMin;
		INT  m_nMax;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Scaling
		virtual INT StoreToDisp(INT nData);
		virtual INT DispToStore(INT nData);

		// Implementation
		BOOL    IsNumberConst(CString const &Text);
		BOOL    Check (CError &Error, INT &nData);
		CString Format(INT nData);
		INT     Parse (PCTXT pText);
	};

// End of File

#endif
