
#include "Intern.hpp"

#include "TagEventFlag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "TagFlag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Event
//

// Dynamic Class

AfxImplementDynamicClass(CTagEventFlag, CTagEvent);

// Constructor

CTagEventFlag::CTagEventFlag(void)
{
	}

// UI Update

void CTagEventFlag::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UINT uMask1 = HasSetpoint() ? 0xFF : 0x87;

		UINT uMask2 = NoLevelMode() ? 0x06 : 0x07;

		LimitEnum(pHost, L"Mode",    m_Mode,    uMask1);

		LimitEnum(pHost, L"Trigger", m_Trigger, uMask2);

		DoEnables(pHost);
		}

	if( Tag == "Mode" ) {

		if( FindLabel() == DWORD(wstrdup(L"")) ) {

			pHost->UpdateUI(this, "Label");
			}

		UINT uMask = NoLevelMode() ? 0x06 : 0x07;

		LimitEnum(pHost, L"Trigger", m_Trigger, uMask);

		CTagEvent::OnUIChange(pHost, pItem, L"Trigger");
		}

	CTagEvent::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CTagEventFlag::NeedSetpoint(void) const
{
	return m_Mode >= 3 && m_Mode <= 6;
	}

BOOL CTagEventFlag::HasSetpoint(void) const
{
	CTagFlag * pTag = (CTagFlag *) GetParent(AfxRuntimeClass(CTagFlag));

	return pTag->HasSetpoint();
	}

BOOL CTagEventFlag::NoLevelMode(void) const
{
	return m_Mode == 7;
	}

// Download Support

BOOL CTagEventFlag::MakeInitData(CInitData &Init)
{
	CTagEvent::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CTagEventFlag::AddMetaData(void)
{
	CTagEvent::AddMetaData();

	Meta_SetName((IDS_TAG_EVENT));
	}

// End of File
