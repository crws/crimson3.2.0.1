
#include "Intern.hpp"

#include "TagEventNumeric.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "TagNumeric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Event
//

// Dynamic Class

AfxImplementDynamicClass(CTagEventNumeric, CTagEvent);

// Constructor

CTagEventNumeric::CTagEventNumeric(void)
{
	m_pValue = NULL;

	m_pHyst  = NULL;
	}

// UI Update

void CTagEventNumeric::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UINT uMask1 = HasSetpoint() ? 0x0FFF : 0x0E1F;

		UINT uMask2 = NoLevelMode() ? 0x0006 : 0x0007;

		LimitEnum(pHost, L"Mode",    m_Mode,    uMask1);

		LimitEnum(pHost, L"Trigger", m_Trigger, uMask2);

		DoEnables(pHost);
		}

	if( Tag == "Mode" ) {

		if( FindLabel() == DWORD(wstrdup(L"")) ) {

			pHost->UpdateUI(this, "Label");
			}

		UINT uMask = NoLevelMode() ? 0x0006 : 0x0007;

		LimitEnum(pHost, L"Trigger", m_Trigger, uMask);

		CTagEvent::OnUIChange(pHost, pItem, L"Trigger");

		DoEnables(pHost);
		}

	CTagEvent::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CTagEventNumeric::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" || Tag == "Hyst" ) {

		CTagNumeric *pTag = (CTagNumeric *) GetParent(AfxRuntimeClass(CTagNumeric));

		Type.m_Type  = pTag->GetDataType();

		Type.m_Flags = 0;

		return TRUE;
		}

	return CTagEvent::GetTypeData(Tag, Type);
	}

// Attributes

BOOL CTagEventNumeric::NeedSetpoint(void) const
{
	return m_Mode >= 5 && m_Mode <= 8;
	}

BOOL CTagEventNumeric::HasSetpoint(void) const
{
	CTagNumeric * pTag = (CTagNumeric *) GetParent(AfxRuntimeClass(CTagNumeric));

	return pTag->HasSetpoint();
	}

BOOL CTagEventNumeric::NoLevelMode(void) const
{
	return m_Mode >= 9 && m_Mode <= 11;
	}

// Operations

void CTagEventNumeric::UpdateTypes(BOOL fComp)
{
	UpdateType(NULL, L"Value", m_pValue, fComp);

	UpdateType(NULL, L"Hyst",  m_pHyst,  fComp);
	}

// Download Support

BOOL CTagEventNumeric::MakeInitData(CInitData &Init)
{
	if( CTagEvent::MakeInitData(Init) ) {

		Init.AddItem(itemVirtual, m_pValue);

		Init.AddItem(itemVirtual, m_pHyst);
		}

	return TRUE;
	}

// Meta Data

void CTagEventNumeric::AddMetaData(void)
{
	CTagEvent::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddVirtual(Hyst);

	Meta_SetName((IDS_TAG_EVENT));
	}

// Implementation

void CTagEventNumeric::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Value", m_Mode >= 1 && m_Mode <= 11);

	pHost->EnableUI(this, "Hyst",  m_Mode >= 3 && m_Mode <=  8);
	}

// End of File
