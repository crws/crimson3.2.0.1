
#include "Intern.hpp"

#include "FunctionItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

#include "NameServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Function Item
//

// Dynamic Class

AfxImplementDynamicClass(CFunctionItem, CMetaItem);

// Constructor

CFunctionItem::CFunctionItem(void)
{
	}

// Persistance

void CFunctionItem::Init(void)
{
	CMetaItem::Init();

	m_pServer = ((CCommsSystem *) GetDatabase()->GetSystemItem())->GetNameServer();
	}

// Atributes

CString CFunctionItem::GetPrototype(void)
{
	// REV3 -- Get real parameters names from somewhere
	// and provide information about arrays and so on.

	CString   Name;

	CTypeDef  Type;

	UINT    uCount;

	if( FindFunction(Name, Type, uCount) ) {

		CString Text;

		Text += TextFromType(Type.m_Type);

		Text += L" ";

		Text += Name;

		Text += L"(";

		if( uCount == 0 ) {

			Text += TextFromType(typeVoid);
			}
		else {
			for( UINT n = 0; n < uCount; n ++ ) {

				WORD       ID = WORD(m_Ident);

				CString Param;

				CTypeDef Type;

				m_pServer->GetFuncParam(ID, n, Param, Type);

				if( n ) {

					Text += L", ";
					}

				Text += TextFromType(Type.m_Type);
				}
			}

		Text += L")";

		return Text;
		}

	return L"";
	}

// Meta Data

void CFunctionItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Ident);
	}

// Implementation

BOOL CFunctionItem::FindFunction(CString &Name, CTypeDef &Type, UINT &uCount)
{
	WORD ID = WORD(m_Ident);

	if( m_pServer->NameFunction(ID, Name) ) {

		CError *pError = New CError(FALSE);

		if( m_pServer->FindFunction(pError, Name, ID, Type, uCount) ) {

			delete pError;

			return TRUE;
			}

		delete pError;
		}

	return FALSE;
	}

CString CFunctionItem::TextFromType(UINT Type)
{
	// REV3 -- Handle class names via the name server?

	PCTXT pNames[] = {

		L"void",
		L"int",
		L"float",
		L"cstring"

		};

	if( Type < elements(pNames) ) {

		return pNames[Type];
		}

	if( Type == typePort ) {

		return L"port";
		}

	if( Type == typeDevice ) {

		return L"device";
		}

	if( Type == typeLog ) {

		return L"log";
		}

	if( Type == typePage ) {

		return L"page";
		}

	return L"????";
	}

// End of File
