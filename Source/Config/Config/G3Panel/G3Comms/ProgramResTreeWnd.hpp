
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramResTreeWnd_HPP

#define INCLUDE_ProgramResTreeWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Program Resource Window
//

class CProgramResTreeWnd : public CResTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramResTreeWnd(void);

	protected:
		// Data Members
		UINT m_cfCode;
		UINT m_cfFunc;

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL AddCodeFragment(CDataObject *pData);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
	};

// End of File

#endif
