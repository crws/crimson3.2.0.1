
#include "Intern.hpp"

#include "CommsPortDevNet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortDevNet, CCommsPort);

// Constructor

CCommsPortDevNet::CCommsPortDevNet(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding = bindDeviceNet;

	m_Baud    = 125000;
	}

// Attributes

UINT CCommsPortDevNet::GetPageType(void) const
{
	return m_pDriver ? 1 : 0;
	}

CHAR CCommsPortDevNet::GetPortTag(void) const
{
	return 'D';
}

// Download Support

BOOL CCommsPortDevNet::MakeInitData(CInitData &Init)
{
	Init.AddByte(7);

	CCommsPort::MakeInitData(Init);

	if( m_pDriver ) {

		Init.AddLong(LONG(m_Baud));
		}

	return TRUE;
	}

// Meta Data Creation

void CCommsPortDevNet::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_AddInteger(Baud);

	Meta_SetName((IDS_DEVICENET_PORT));
	}

// End of File
