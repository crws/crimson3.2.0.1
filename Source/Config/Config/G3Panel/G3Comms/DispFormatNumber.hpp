
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatNumber_HPP

#define INCLUDE_DispFormatNumber_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispFormat.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Number Display Format
//

class DLLAPI CDispFormatNumber : public CDispFormat
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatNumber(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Formatting
		CString Format(DWORD Data, UINT Type, UINT Flags);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Property Preservation
		BOOL Preserve(CDispFormat *pOld);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT	     m_Radix;
		UINT	     m_Before;
		UINT	     m_After;
		UINT	     m_Leading;
		UINT	     m_Group;
		UINT	     m_Signed;
		CCodedText * m_pPrefix;
		CCodedText * m_pUnits;
		CCodedItem * m_pDynDP;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void  DoEnables(IUIHost *pHost);
		UINT  GetLimit(void);
		UINT  GetRadix(void);
		INT64 Power(UINT n);
		BOOL  IsGroupBoundary(UINT n);
		TCHAR GetGroupChar(void);
		TCHAR GetPointChar(void);
		UINT  GetAfter(void);
	};

// End of File

#endif
