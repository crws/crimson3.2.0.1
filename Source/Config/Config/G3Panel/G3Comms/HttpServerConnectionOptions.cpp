
#include "Intern.hpp"

#include "HttpServerConnectionOptions.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Connection Options
//

// Base Class

#undef  CBaseClass

#define CBaseClass CHttpConnectionOptions

// Dynamic Class

AfxImplementDynamicClass(CHttpServerConnectionOptions, CBaseClass);

// Constructor

CHttpServerConnectionOptions::CHttpServerConnectionOptions(void)
{
	m_CompReply  = TRUE;

	m_MaxKeep    = 2;
	}

// UI Update

void CHttpServerConnectionOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Advanced" ) {

			DoEnables(pHost);
			}
		}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CHttpServerConnectionOptions::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddByte(BYTE(m_CompReply));
	Init.AddByte(BYTE(m_MaxKeep));

	return TRUE;
	}

// Meta Data Creation

void CHttpServerConnectionOptions::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(CompReply);
	Meta_AddInteger(MaxKeep);

	Meta_SetName((IDS_HTTP_SERVER));
	}

// Implementation

void CHttpServerConnectionOptions::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "CompReply",  m_Advanced);

	pHost->EnableUI(this, "MaxKeep",    m_Advanced);
	}

// End of File
