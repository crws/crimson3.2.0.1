
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextCommsDriver_HPP

#define INCLUDE_UITextCommsDriver_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPort;

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Driver Selection
//

class CUITextCommsDriver : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextCommsDriver(void);

	protected:
		// Data Members
		CCommsPort * m_pItem;
		BOOL         m_fLimit;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		BOOL    OnExpand(CWnd &Wnd);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		CString GetDriverName(ICommsDriver *pDriver);
		void    LoadEnum(void);
	};

// End of File

#endif
