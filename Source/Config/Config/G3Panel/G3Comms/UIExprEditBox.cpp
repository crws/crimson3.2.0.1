
#include "Intern.hpp"

#include "UIExprEditBox.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Programmable Text Box
//

// Dynamic Class

AfxImplementDynamicClass(CUIExprEditBox, CUIEditBox);

// Constructor

CUIExprEditBox::CUIExprEditBox(void)
{
	m_cfCode = RegisterClipboardFormat(L"C3.1 Code Fragment");
	}

// Data Overridables

void CUIExprEditBox::OnLoad(void)
{
	if( m_pText->HasFlag(textHide) ) {

		CString Text  = m_pText->GetAsText();

		DWORD   Style = m_pDataCtrl->GetWindowStyle();

		if( Text[0] == L'=' ) {

			if( Style & ES_PASSWORD ) {

				m_pDataCtrl->SetPasswordChar(0);
				}
			}
		else {
			if( !(Style & ES_PASSWORD) ) {

				m_pDataCtrl->SetPasswordChar(9679);
				}
			}
		}

	CUIEditBox::OnLoad();
	}

BOOL CUIExprEditBox::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( CanAcceptText(pData, m_cfCode) ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
		}

	if( CanAcceptText(pData, CF_UNICODETEXT) ) {

		dwEffect = DROPEFFECT_COPY;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIExprEditBox::OnAcceptData(IDataObject *pData)
{
	m_pDataCtrl->SetFocus();

	if( AcceptText(pData, m_cfCode, L"=") ) {

		return TRUE;
		}

	if( AcceptText(pData, CF_UNICODETEXT) ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File
