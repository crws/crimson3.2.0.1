
#include "Intern.hpp"

#include "CodeEditingDialog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedHost.hpp"
#include "CommsSystem.hpp"
#include "NameServer.hpp"
#include "ProgramManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Complex Code Editor Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCodeEditingDialog, CDialog);

// Constructors

CCodeEditingDialog::CCodeEditingDialog(CCodedHost *pHost, CString Tag, CString Params, CString Text)
{
	m_pHost        = pHost;

	m_Tag          = Tag;

	m_Params       = Params;

	m_Text         = Text;

	m_fFunc        = IsComplex(Text);

	m_pEditor      = NULL;

	m_pHeader      = NULL;

	m_Type.m_Flags = 0;

	m_Type.m_Type  = typeInteger;

	m_pHost->GetTypeData(m_Tag, m_Type);
	}

// Attributes

CString CCodeEditingDialog::GetAsText(void) const
{
	return m_Text;
	}

// Dialog Operations

BOOL CCodeEditingDialog::Create(CWnd &Parent)
{
	CRect Rect(m_fFunc ? CSize(582, 512) : CSize(582, 130));

	LoadConfig(Rect);

	DWORD dwStyle = WS_POPUP   |
			WS_SYSMENU |
			WS_CAPTION |
			WS_VISIBLE ;

	if( m_fFunc ) {

		dwStyle |= WS_THICKFRAME;
		}

	DWORD dwExStyle = WS_EX_DLGMODALFRAME;

	CWnd::Create( dwExStyle,
		      dwStyle,
		      Rect,
		      Parent,
		      0,
		      NULL
		      );

	return TRUE;
	}

// Routing Control

BOOL CCodeEditingDialog::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pEditor && m_pEditor->HasFocus() ) {

		if( m_pEditor->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	if( m_pHeader && m_pHeader->HasFocus() ) {

		if( m_pHeader->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CDialog::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CCodeEditingDialog, CDialog)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_GETMINMAXINFO)
	AfxDispatchMessage(WM_PREDESTROY)

	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CCodeEditingDialog)
	};

// Message Handlers

UINT CCodeEditingDialog::OnCreate(CREATESTRUCT &Create)
{

	//BuildSystemMenu();

	FindLayout();

	MakeHeader();

	MakeEditor();

	MakeGroups();

	MakeButtons();

	SendInitDialog();

	PlaceDialog();

	return 0;
	}

BOOL CCodeEditingDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetCaption();

	LoadEditor();

	m_pEditor->SetFocus();

	return FALSE;
	}

void CCodeEditingDialog::OnSize(UINT uCode, CSize Size)
{
	CRect Client = GetClientRect();

	CPoint Org(6, 6);

	if( m_fFunc ) {

		CSize Grp;

		Grp.cx = Size.cx - 12;

		Grp.cy = 55;

		CRect Rect = CRect(Org, Grp);

		m_pGroup[0]->MoveWindow(Rect, TRUE);

		Org.y += Rect.cy() + 4;

		Rect.left   += 8;
		Rect.top    += 20;
		Rect.right  -= 8;
		Rect.bottom  = Rect.top + m_Header.cy;

		m_pHeader->MoveWindow(Rect, TRUE);
		}

	if( TRUE ) {

		CSize Grp;

		Grp.cx = Size.cx - 12;

		CRect Rect = CRect(Org, Grp);

		Rect.bottom = Client.bottom - m_Button.cy - 12;

		m_pGroup[m_fFunc ? 1 : 0]->MoveWindow(Rect, TRUE);

		Rect.left   += 8;
		Rect.top    += 20;
		Rect.right  -= 8;
		Rect.bottom -= 8;

		m_pEditor->MoveWindow(Rect, TRUE);
		}

	MoveButtons();
	}

void CCodeEditingDialog::OnGetMinMaxInfo(MINMAXINFO &Info)
{
	if( m_fFunc ) {

		Info.ptMinTrackSize.x = 582;

		Info.ptMinTrackSize.y = 512;
		}
	else {
		Info.ptMinTrackSize.x = 582;

		Info.ptMinTrackSize.y = 130;
		}
	}

BOOL CCodeEditingDialog::OnEraseBkGnd(CDC &DC)
{
	DC.FillRect(GetClientRect(), afxBrush(TabFace));

	return TRUE;
	}

void CCodeEditingDialog::OnPreDestroy(void)
{
	SaveConfig();
	}

BOOL CCodeEditingDialog::OnCommandOK(UINT uID)
{
	CString Code = m_pEditor->GetText();

	if( !Code.IsEmpty() ) {

		CError Error(TRUE);

		if( !Compile(Error, Code) ) {

			CRange Range = Error.GetRange();

			Error.Show(ThisObject);

			m_pEditor->SetSel(Range);

			m_pEditor->SetFocus();
			}
		else {
			CString Text;

			if( m_fFunc ) {

				Text += L"// ";

				Text += m_pHeader->GetWindowText();

				Text += L"\r\n";
				}

			Text += m_pEditor->GetText();

			m_Text = Text;

			EndDialog(TRUE);
			}
		}
	else {
		m_Text = L"";

		EndDialog(TRUE);
		}

	return TRUE;
	}

BOOL CCodeEditingDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

// Implementation

BOOL CCodeEditingDialog::IsComplex(CString Text)
{
	return Text.Find(L"\r\n") < NOTHING;
	}

void CCodeEditingDialog::SetCaption(void)
{
	CString Text = m_fFunc ? CString(IDS_EDIT_COMPLEX_FMT) : CString(IDS_EDIT_GENERAL_FMT);

	CString What = (m_Type.m_Flags & flagActive) ? CString(IDS_ACTION) : CString(IDS_EXPRESSION);

	SetWindowText(CFormat(Text, What));
	}

BOOL CCodeEditingDialog::LoadConfig(CRect &Rect)
{
	if( m_fFunc ) {

		CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

		CRect   Read = Rect;

		Reg.MoveTo(L"G3Comm");

		Reg.MoveTo(L"Editor");

		Read.left   = Reg.GetValue(L"left",   UINT(Read.left));

		Read.top    = Reg.GetValue(L"top",    UINT(Read.top));

		Read.right  = Reg.GetValue(L"right",  UINT(Read.right));

		Read.bottom = Reg.GetValue(L"bottom", UINT(Read.bottom));

		if( CRect(CSize(SM_CXVIRTUALSCREEN)).Encloses(Read) ) {

			Rect = Read;

			return TRUE;
			}
		}

	return FALSE;
	}

void CCodeEditingDialog::SaveConfig(void)
{
	if( m_fFunc ) {

		CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

		CRect   Rect = GetWindowRect();

		Reg.MoveTo(L"G3Comm");

		Reg.MoveTo(L"Editor");

		Reg.SetValue(L"left",   UINT(Rect.left));

		Reg.SetValue(L"top",    UINT(Rect.top));

		Reg.SetValue(L"right",  UINT(Rect.right));

		Reg.SetValue(L"bottom", UINT(Rect.bottom));
		}
	}

void CCodeEditingDialog::FindLayout(void)
{
	m_Border = FromDlgUnits(CSize(8, 8));

	m_Button = FromDlgUnits(CSize(40, 14));

	m_Header = FromDlgUnits(CSize(40, 12));
	}

void CCodeEditingDialog::MakeGroups(void)
{
	CString Label[2];

	UINT    uCount;

	if( m_fFunc ) {

		Label[0] = CString(IDS_DESCRIPTION);

		Label[1] = CString(IDS_EDITOR);

		uCount   = 2;
		}
	else {
		Label[0] = CString(IDS_EDITOR);

		uCount   = 1;
		}

	for( UINT n = 0; n < uCount; n ++ ) {

		DWORD dwStyle = WS_CHILD    |
				WS_VISIBLE  |
				BS_GROUPBOX ;

		m_pGroup[n]   = New CButton;

		m_pGroup[n]->Create( Label[n],
			             dwStyle,
			             CRect(),
			             ThisObject,
			             0
			             );

		m_pGroup[n]->SetFont(afxFont(Dialog));
		}
	}

void CCodeEditingDialog::MakeButtons(void)
{
	CString Label[] = { CString(IDS_OK), CString(IDS_CANCEL) };

	UINT     uIDs[]	= { IDOK, IDCANCEL };

	for( UINT n = 0; n < elements(m_pButton); n ++ ) {

		DWORD dwStyle = WS_CHILD | WS_TABSTOP | WS_VISIBLE;

		dwStyle      |= (n ? BS_PUSHBUTTON : BS_DEFPUSHBUTTON);

		m_pButton[n]  = New CButton;

		m_pButton[n]->Create( Label[n],
			              dwStyle,
			              CRect(),
			              ThisObject,
			              uIDs[n]
			              );

		m_pButton[n]->SetFont(afxFont(Dialog));
		}
	}

void CCodeEditingDialog::MoveButtons(void)
{
	CRect Rect = GetClientRect();

	Rect.bottom = Rect.bottom - m_Border.cy / 2;

	Rect.top    = Rect.bottom - m_Button.cy;

	Rect.left   = Rect.left   + m_Border.cx / 2;

	Rect.right  = Rect.left   + m_Button.cx;

	for( UINT n = 0; n < elements(m_pButton); n ++ ) {

		m_pButton[n]->MoveWindow(Rect, TRUE);

		Rect.left   = Rect.right  + m_Border.cx / 2;

		Rect.right  = Rect.left   + m_Button.cx;
		}
	}

void CCodeEditingDialog::MakeEditor(void)
{
	DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_CLIPCHILDREN;

	m_pEditor  = New CSourceEditorWnd(m_fFunc ? 2 : 1);

	m_pEditor->Create( dwStyle,
			   CRect(),
			   GetHandle(),
			   200
			   );
	}

void CCodeEditingDialog::LoadEditor(void)
{
	CString Text = m_Text;

	if( m_fFunc ) {

		CString Desc = Text.StripToken(L"\r\n").Mid(3);

		m_pHeader->SetWindowText(Desc);

		m_pEditor->SetText(Text);

		return;
		}

	m_pEditor->SetText(Text);

	m_pEditor->SetSel (CRange(0, Text.GetLength()));
	}

void CCodeEditingDialog::MakeHeader(void)
{
	if( m_fFunc ) {

		DWORD dwStyle = WS_CHILD   |
				WS_BORDER  |
				WS_VISIBLE |
				WS_TABSTOP ;

		m_pHeader     = New CEditCtrl;

		m_pHeader->Create( dwStyle,
				   CRect(),
				   GetHandle(),
				   201
				   );

		m_pHeader->SetFont(afxFont(Dialog));
		}
	}

BOOL CCodeEditingDialog::Compile(CError &Error, CString Text)
{
	CDatabase    *pDbase  = m_pHost->GetDatabase();

	CCommsSystem *pSystem = (CCommsSystem *) pDbase->GetSystemItem();

	CNameServer  *pServer = pSystem->GetNameServer();

	INameServer  *pName   = pServer;

	CCompileIn  In;

	CCompileOut Out;

	In.m_pText  = Text;
	In.m_Optim  = 0;
	In.m_Debug  = 0;
	In.m_Switch = 0;
	In.m_uParam = 0;
	In.m_pParam = NULL;
	In.m_pName  = pName;
	In.m_Type   = m_Type;

	if( pSystem->m_pPrograms->m_AutoCast ) {

		In.m_Switch |= optAutoCast;
		}

	if( pSystem->m_pPrograms->m_PrecFix ) {

		In.m_Switch |= optPrecFix;
		}

	Out.m_Error    = Error;
	Out.m_pSource  = NULL;
	Out.m_pRefList = NULL;
	Out.m_pObject  = NULL;

	pName = m_pHost->GetNameServer(pServer);

	LoadParams(In);

	if( C3CompileCode(In, Out, m_fFunc) ) {

		KillParams(In);

		return TRUE;
		}

	KillParams(In);

	Error = Out.m_Error;

	return FALSE;
	}

void CCodeEditingDialog::LoadParams(CCompileIn &In)
{
	if( !m_Params.IsEmpty() ) {

		CStringArray List;

		m_Params.Tokenize(List, '/');

		if( List.GetCount() ) {

			In.m_uParam = List.GetCount();

			In.m_pParam = New CFuncParam [ In.m_uParam ];

			for( UINT n = 0; n < In.m_uParam; n++ ) {

				CStringArray Part;

				List[n].Tokenize(Part, '=');

				CTypeDef Type;

				m_pHost->GetTypeData(Part[1], Type);

				In.m_pParam[n].m_Name = Part[0];

				In.m_pParam[n].m_Type = Type.m_Type;
				}
			}
		}
	}

void CCodeEditingDialog::KillParams(CCompileIn &In)
{
	delete [] In.m_pParam;
	}

// End of File
