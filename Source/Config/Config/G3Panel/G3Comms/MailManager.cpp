
#include "Intern.hpp"

#include "MailManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "MailAddress.hpp"

#include "MailContacts.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mail Manager Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CMailManager, CServiceItem);

// Constructor

CMailManager::CMailManager(void)
{
	m_pEnable     = NULL;
	m_pPanelName  = NULL;
	m_pSMTP       = NULL;
	m_pMode       = NULL;
	m_pMailServer = NULL;
	m_pMailPort   = NULL;
	m_pDomainName = NULL;
	m_pReverse    = New CMailAddress;
	m_pTime1      = NULL;
	m_pLogFile    = NULL;
	m_pAuth       = NULL;
	m_pUsername   = NULL;
	m_pPassword   = NULL;
	m_pSMS        = NULL;
	m_pRelay      = NULL;
	m_pOnSMS      = NULL;
	m_DateForm    = 0;
	m_pSSL	      = NULL;
	m_pContacts   = New CMailContacts;
	m_Debug       = 0;
	}

// Initial Values

void CMailManager::SetInitValues(void)
{
	SetInitial(L"Enable",     m_pEnable,     1);

	SetInitial(L"PanelName",  m_pPanelName,  CString(IDS_G3_PANEL));

	SetInitial(L"SMTP",       m_pSMTP,       1);

	SetInitial(L"Mode",       m_pMode,       0);

	SetInitial(L"MailPort",   m_pMailPort,   25);

	SetInitial(L"MailServer", m_pMailServer, 0);

	SetInitial(L"Time1",      m_pTime1,      30);

	SetInitial(L"LogFile",    m_pLogFile,    0);

	SetInitial(L"Auth",       m_pAuth,       0);

	SetInitial(L"SMS",        m_pSMS,        1);

	SetInitial(L"Relay",      m_pRelay,      0);

	SetInitial(L"SSL",	  m_pSSL,	 0);
	}

// UI Management

CViewWnd * CMailManager::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		CUIPageList *pList = New CUIPageList;

		if( OnLoadPages(pList) ) {

			CViewWnd *pView = New CUIItemMultiWnd(pList);

			return pView;
			}

		CUIViewWnd *pView = New CUIItemViewWnd(pList);

		pView->SetBorder(6);

		return pView;
		}

	return NULL;
	}

// UI Creation

BOOL CMailManager::OnLoadPages(CUIPageList *pList)
{
	if( C3OemFeature(L"SMS", TRUE) ) {

		pList->Append(New CUIStdPage(CString(IDS_MAIL), AfxPointerClass(this), 1));

		pList->Append(New CUIStdPage(CString(IDS_SMS),  AfxPointerClass(this), 2));
		}
	else {
		pList->Append(New CUIStdPage(CString(IDS_MAIL), AfxPointerClass(this), 1));
		}

	return TRUE;
	}

// UI Update

void CMailManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Contacts" ) {

		CItemDialog Dlg(m_pContacts, CString(IDS_LIST_OF_CONTACTS));

		CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

		HGLOBAL hPrev = m_pContacts->TakeSnapshot();

		if( System.ExecSemiModal(Dlg) ) {

			HGLOBAL        hData = m_pContacts->TakeSnapshot();

			CCmdSubItem *  pCmd  = New CCmdSubItem( L"Contacts",
								hPrev,
								hData
								);

			if( pCmd->IsNull() ) {

				delete pCmd;
				}
			else {
				pHost->SaveExtraCmd(pCmd);

				SetDirty();
				}
			}
		else
			GlobalFree(hPrev);
		}

	if( Tag == "Enable" || Tag == "SMTP" || Tag == "Mode" || Tag == "SMS" || Tag == "Auth" ) {

		DoEnables(pHost);
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CMailManager::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"PanelName" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == L"DomainName" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == L"Username" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == L"Password" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "OnSMS" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	if( Tag == "MsgText" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Attributes

UINT CMailManager::GetTreeImage(void) const
{
	return IDI_MAIL_CLIENT;
	}

// Download Support

BOOL CMailManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(3));

	CServiceItem::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pContacts);

	Init.AddItem(itemVirtual, m_pPanelName);

	Init.AddByte(BYTE(m_DateForm));

	Init.AddItem(itemVirtual, m_pRelay);
	Init.AddItem(itemVirtual, m_pOnSMS);

	return TRUE;
	}

// Meta Data Creation

void CMailManager::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddVirtual(PanelName);
	Meta_AddVirtual(SMTP);
	Meta_AddVirtual(Mode);
	Meta_AddVirtual(MailServer);
	Meta_AddVirtual(MailPort);
	Meta_AddVirtual(DomainName);
	Meta_AddObject (Reverse);
	Meta_AddVirtual(Time1);
	Meta_AddVirtual(LogFile);
	Meta_AddVirtual(Auth);
	Meta_AddVirtual(Username);
	Meta_AddVirtual(Password);
	Meta_AddVirtual(SMS);
	Meta_AddVirtual(Relay);
	Meta_AddVirtual(OnSMS);
	Meta_AddInteger(DateForm);
	Meta_AddVirtual(SSL);
	Meta_AddObject (Contacts);
	Meta_AddInteger(Debug);

	Meta_SetName((IDS_MAIL_MANAGER));
	}

// Implementation

void CMailManager::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	BOOL fManual = CheckEnable(m_pMode,   L"==", 0, TRUE);

	BOOL fSMTP   = CheckEnable(m_pSMTP,   L"==", 1, TRUE);

	BOOL fSMS    = CheckEnable(m_pSMS,    L"==", 1, TRUE);

	BOOL fAuth   = CheckEnable(m_pAuth,   L">",  0, TRUE);

	pHost->EnableUI("PanelName",  fEnable);
	pHost->EnableUI("SMTP",       fEnable);
	pHost->EnableUI("Mode",       fEnable && fSMTP);
	pHost->EnableUI("MailServer", fEnable && fSMTP && fManual);
	pHost->EnableUI("MailPort",   fEnable && fSMTP);
	pHost->EnableUI("DomainName", fEnable && fSMTP);
	pHost->EnableUI("Reverse",    fEnable && fSMTP);
	pHost->EnableUI("Time1",      fEnable && fSMTP);
	pHost->EnableUI("LogFile",    fEnable && fSMTP);
	pHost->EnableUI("Debug",      fEnable && fSMTP);
	pHost->EnableUI("SMS",        fEnable);
	pHost->EnableUI("Relay",      fEnable && fSMS);
	pHost->EnableUI("OnSMS",      fEnable && fSMS);
	pHost->EnableUI("Auth",       fEnable && fSMTP);
	pHost->EnableUI("Username",   fEnable && fSMTP && fAuth);
	pHost->EnableUI("Password",   fEnable && fSMTP && fAuth);
	pHost->EnableUI("DateForm",   fEnable && fSMTP);
	pHost->EnableUI("SSL",	      fEnable && fSMTP && fAuth);
	}

// End of File
