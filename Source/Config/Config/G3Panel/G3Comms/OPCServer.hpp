
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_OPCServer_HPP

#define INCLUDE_OPCServer_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC Configuration
//

class COPCServer : public CServiceItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COPCServer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		BOOL IsEnabled(void) const;
		UINT GetTreeImage(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pEnable;
		CCodedItem * m_pSocket;
		CCodedItem * m_pCount;
		CCodedItem * m_pRestrict;
		CCodedItem * m_pSecAddr;
		CCodedItem * m_pSecMask;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
