
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQueryManagerTreeWnd_CCmdPaste_HPP

#define INCLUDE_SqlQueryManagerTreeWnd_CCmdPaste_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SqlQueryManagerTreeWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Tree Window -- Paste Command
//

class CSqlQueryManagerTreeWnd::CCmdPaste : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdPaste( CItem       *pItem,
			   CItem       *pRoot,
			   CItem       *pPrev,
			   IDataObject *pData
			   );

		// Destructor
		~CCmdPaste(void);

		// Data Members
		CString       m_Root;
		CString       m_Prev;
		IDataObject * m_pData;
		CStringArray  m_Names;
	};

// End of File

#endif
