
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatTimeDate_HPP

#define INCLUDE_DispFormatTimeDate_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispFormat.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Time and Date Display Format
//

class DLLAPI CDispFormatTimeDate : public CDispFormat
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatTimeDate(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Formatting
		CString Format(DWORD Data, UINT Type, UINT Flags);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT	     m_Mode;
		UINT	     m_Secs;
		UINT	     m_TimeForm;
		UINT	     m_TimeSep;
		CCodedText * m_pAM;
		CCodedText * m_pPM;
		UINT	     m_DateForm;
		UINT	     m_DateSep;
		UINT	     m_Year;
		UINT	     m_Month;

	protected:
		// Data Members
		UINT  m_uCount;
		UINT  m_uFields;
		UINT  m_uTotal;
		DWORD m_Format[20];

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void BuildFormat(void);
		void BuildTime(void);
		void BuildDate(void);
		void AddDate(void);
		void AddMonth(void);
		void AddYear(void);
		void AddDateSep(void);
		void AddForm(UINT uCode, UINT uLen);

		// Localization
		UINT    GetLocaleTime(void);
		UINT    GetLocaleDate(void);
		CString GetMonthName(UINT uMonth);
		CString GetAMPM(UINT uHour);
	};

// End of File

#endif
