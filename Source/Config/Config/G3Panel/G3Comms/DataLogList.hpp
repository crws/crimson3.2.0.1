
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DataLogList_HPP

#define INCLUDE_DataLogList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDataLog;

//////////////////////////////////////////////////////////////////////////
//
// Data Log List
//

class CDataLogList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDataLogList(void);

		// Item Access
		CDataLog * GetItem(INDEX Index) const;
		CDataLog * GetItem(UINT uPos) const;
	};

// End of File

#endif
