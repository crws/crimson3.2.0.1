
#include "Intern.hpp"

#include "ControlManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProjectItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Control Manager
//

// Dynamic Class

AfxImplementDynamicClass(CControlManager, CUIItem);

// Constructor

CControlManager::CControlManager(void)
{
	m_pProject = NULL;

	m_Handle   = HANDLE_NONE;
}

// Destructor

CControlManager::~CControlManager(void)
{
}

// Validation

void CControlManager::Validate(void)
{
	m_pProject ? m_pProject->Validate() : void(0);
}

// Persistance

void CControlManager::Init(void)
{
	CUIItem::Init();

	CheckProject();
}

void CControlManager::PostInit(void)
{
	CheckProject();
}

void CControlManager::PostLoad(void)
{
	CUIItem::PostLoad();

	CheckProject();
}

// Conversion

void CControlManager::PostConvert(void)
{
	CheckProject() ? m_pProject->PostConvert() : void(0);
}

// Project Build

BOOL CControlManager::NeedBuild(void)
{
	return m_pProject ? m_pProject->NeedBuild() : FALSE;
}

BOOL CControlManager::PerformBuild(void)
{
	return m_pProject ? m_pProject->PerformBuild() : FALSE;
}

BOOL CControlManager::HasControl(void)
{
	return m_pProject ? m_pProject->HasControl() : FALSE;
}

// Download Support

BOOL CControlManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_CONTROL);

	CMetaItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pProject);

	return TRUE;
}

// Meta Data Creation

void CControlManager::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddVirtual(Project);

	Meta_SetName((IDS_CONTROL_MANAGER));
}

// Implementation

BOOL CControlManager::CheckProject(void)
{
	if( m_pProject ) {

		if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_4 ) {

			CLASS Class = AfxNamedClass(L"CControlProject");

			if( m_pProject->IsKindOf(Class) ) {

				return TRUE;
			}
		}

		m_pProject->Kill();

		delete m_pProject;

		m_pProject = NULL;
	}

	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_4 ) {

		CLASS Class = AfxNamedClass(L"CControlProject");

		m_pProject  = AfxNewObject(CProjectItem, Class);

		m_pProject->SetParent(this);

		m_pProject->Init();

		return TRUE;
	}

	return FALSE;
}

// End of File
