
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3Comms_HPP
	
#define	INCLUDE_G3Comms_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3comms.hpp>

#include <c3source.hpp>

#include <g3gdi.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3comms.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3COMMS

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "G3Comms.lib")

#endif

#define DLLNOT 

//////////////////////////////////////////////////////////////////////////
//
// Application Types
//

enum
{
	typePage   = typeObject + 1,
	typePort   = typeIndex  + 1,
	typeDevice = typeIndex  + 2,
	typeLog    = typeIndex  + 3
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Properties for Tags
//

enum
{
	tpAsText	= 1,
	tpLabel		= 2,
	tpDescription	= 3,
	tpPrefix	= 4,
	tpUnits		= 5,
	tpSetPoint	= 6,
	tpMinimum	= 7,
	tpMaximum	= 8,
	tpForeColor	= 9,
	tpBackColor	= 10,
	tpName		= 11,
	tpIndex		= 12,
	tpAlarms	= 13,
	tpTextOff	= 14,
	tpTextOn	= 15,
	tpStates	= 16,
	tpDeadband	= 17,
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CTree  <class CCodedItem *> CCodedTree;

typedef CArray <class CCodedItem *> CCodedList;

typedef CTree  <UINT>               CIndexTree;

// End of File

#endif
