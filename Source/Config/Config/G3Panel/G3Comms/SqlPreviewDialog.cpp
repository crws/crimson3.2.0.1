
#include "Intern.hpp"

#include "SqlPreviewDialog.hpp"

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// SQL Preview Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSqlPreviewDialog, CStdDialog);

// Message Map

AfxMessageMap(CSqlPreviewDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CSqlPreviewDialog)
	};

// Constructor

CSqlPreviewDialog::CSqlPreviewDialog(CString const &SQL)
{
	m_SQL = SQL;

	SetName(L"SqlPreviewDlg");
	}

// Message Handlers

BOOL CSqlPreviewDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	GetDlgItem(1001).SetWindowText(m_SQL);

	return TRUE;
	}

BOOL CSqlPreviewDialog::OnOkay(UINT uId)
{
	EndDialog(TRUE);

	return TRUE;
	}

// End of File
