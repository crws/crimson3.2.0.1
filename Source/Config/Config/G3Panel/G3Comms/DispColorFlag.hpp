
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColorFlag_HPP

#define INCLUDE_DispColorFlag_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispColor.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Color
//

class CDispColorFlag : public CDispColor
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispColorFlag(void);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_On;
		UINT m_Off;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
