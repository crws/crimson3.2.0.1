
#include "Intern.hpp"

#include "OptionCardItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "CommsPortCAN.hpp"
#include "CommsPortCatLink.hpp"
#include "CommsPortDevNet.hpp"
#include "CommsPortJ1939.hpp"
#include "CommsPortMPI.hpp"
#include "CommsPortProfibus.hpp"
#include "CommsPortFireWire.hpp"
#include "CommsPortSerial.hpp"
#include "CommsPortTest.hpp"
#include "CommsPortWifi.hpp"
#include "ExpansionItem.hpp"
#include "OptionCardList.hpp"
#include "OptionCardPage.hpp"
#include "OptionCardRackItem.hpp"
#include "OptionCardRackList.hpp"
#include "TetheredItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Option Card Configuration
//

// Dynamic Class

AfxImplementDynamicClass(COptionCardItem, CUIItem);

// Constructor

COptionCardItem::COptionCardItem(void)
{
	m_Slot   = 0;

	m_Type   = typeNone;

	m_Power  = 0;

	m_pPorts = New CCommsPortList;

	m_Number = NOTHING;
	}

COptionCardItem::COptionCardItem(UINT uSlot)
{
	m_Slot   = uSlot;

	m_Type   = typeNone;

	m_Power  = 0;

	m_pPorts = New CCommsPortList;

	m_Number = NOTHING;
	}

COptionCardItem::COptionCardItem(UINT uSlot, CString Name)
{
	m_Slot   = uSlot;

	m_Type   = typeNone;

	m_Power  = 0;

	m_pPorts = New CCommsPortList;

	m_Number = NOTHING;

	m_Name   = Name;
	}

// UI Creation

BOOL COptionCardItem::OnLoadPages(CUIPageList *pList)
{
	COptionCardPage *pPage = New COptionCardPage(this);

	pList->Append(pPage);

	return FALSE;
	}

// UI Update

void COptionCardItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Type" ) {

		if( !pHost->InReplay() ) {

			HGLOBAL hPrev = m_pPorts->TakeSnapshot();

			DelPorts();

			AddPorts();

			HGLOBAL hData = m_pPorts->TakeSnapshot();

			CCmd *  pCmd  = New CCmdSubItem( L"Ports",
							 hPrev,
							 hData
							 );

			pHost->SaveExtraCmd(pCmd);

			pHost->SendUpdate(updateChildren);

			CString Warning;

			if( GetPower() && !CheckPower(Warning) ) {

				pHost->GetWindow().Information(Warning);
				}
			}

		DoEnables(pHost);

		pHost->SendUpdate(updateRename);
		}

	if( Tag == "ButtonRemoveCard" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_REM_CARD);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

UINT COptionCardItem::GetTreeImage(void) const
{
	if( m_pDbase->HasFlag(L"Canyon") ) {

		return IDI_XPORT;
		}

	if( m_pDbase->HasFlag(L"USBRack") ) {

		return m_Type == typeRack ? IDI_RACK_GRAPHITE : IDI_MODULE_GRAPHITE;
		}

	return IDI_XPORT;
	}

CString COptionCardItem::GetTreeLabel(void) const
{
	CString Text = GetHumanName();

	if( m_Type ) {

		Text += L" - ";

		if( m_Type < typeRack ) {

			Text += GetCardType(m_Type);
			}

		else if( m_Type == typeModule ) {

			Text += m_Module;
			}
		}

	return Text;
	}

CString COptionCardItem::GetCardName(UINT uType) const
{
	if( uType ) {

		CString Text = GetCardType(uType);

		if( uType < typeModule ) {

			Text += L" ";

			if( m_pDbase->HasFlag(L"USBRack") ) {

				Text += CString(IDS_MODULE);
				}
			else {
				Text += CString(IDS_OPTION_CARD);
				}
			}

		C3OemStrings(Text);

		return Text;
		}

	return IDS_NO_CARD_FITTED;
	}

CString COptionCardItem::GetCardType(UINT uType) const
{
	switch( uType ) {

		case typeSerial:

			return IDS_COMMS_OPT_232;

		case typeCAN:

			return IDS_COMMS_OPT_CAN;

		case typeProfibus:

			return IDS_COMMS_OPT_PBUS;

		case typeFireWire:

			return IDS_COMMS_OPT_FWIRE;

		case typeDeviceNet:

			return IDS_COMMS_OPT_DNET;

		case typeCatLink:

			return IDS_COMMS_OPT_CDL;

		case typeModem:

			return IDS_CELLULAR_MODEM;

		case typeMPI:

			return IDS_COMMS_OPT_MPI;

		case typeEthernet:

			return IDS_COMMS_ETHERNET;

		case typeUSBHost:

			return IDS_COMMS_USB_HOST;

		case typeJ1939:

			return IDS_COMMS_OPT_J1939;

		case typeDnp3:

			return IDS_COMMS_OPT_DNP3;

		case typeTest:

			return IDS_COMMS_TEST;

		case typeCanCdl:

			return IDS_COMMS_OPT_CAN_CDL;

		case typeWiFi:

			return IDS_COMMS_OPT_WIFI;

		case typeDongle:

			return IDS_COMMS_OPT_DONGLE;

		case typeRack:

			return IDS_COMMS_OPT_RACK;

		case typeModule:

			return IDS_COMMS_OPT_NOT_AVAIL;
		}

	return L"";
	}

UINT COptionCardItem::GetCardClass(void) const
{
	switch( m_Type ) {

		case typeSerial:
		case typeCAN:
		case typeProfibus:
		case typeFireWire:
		case typeDeviceNet:
		case typeCatLink:
		case typeModem:
		case typeMPI:
		case typeEthernet:
		case typeUSBHost:
		case typeJ1939:
		case typeTest:
		case typeCanCdl:
		case typeWiFi:

			return classComms;

		case typeRack:

			return classRack;

		case typeModule:

			return classIO;

		case typeDongle:

			return classDongle;
		}

	return classNone;
	};

UINT COptionCardItem::GetFirmwareID(void) const
{
	if( m_pDbase->HasFlag(L"USBRack") ) {

		switch( m_Type ) {

			case typeCAN:		return 9;
			case typeDeviceNet:	return 10;
			case typeJ1939:		return 11;
			case typeProfibus:	return 12;
			case typeSerial:	return 13;
			case typeTest:
			case typeDnp3:		return 14;
			case typeCanCdl:	return 15;
			}
		}

	return 0;
	}

UINT COptionCardItem::GetPower(void) const
{
	UINT uPower = 0;

	if( m_pDbase->HasFlag(L"USBRack") ) {

		switch( m_Type ) {

			case typeCAN:		uPower = 12;		break;
			case typeDeviceNet:	uPower = 12;		break;
			case typeJ1939:		uPower = 12;		break;
			case typeProfibus:	uPower = 26;		break;
			case typeSerial:	uPower = 43;		break;	// TODO
			case typeModem:		uPower = 85;		break;
			case typeDongle:	uPower = 11;		break;
			case typeCanCdl:	uPower = 14;		break;
			case typeModule:	uPower = m_Power;	break;
			}
		}

	return uPower > 43 ? uPower : 0;
	}

// Operations

void COptionCardItem::Validate(BOOL fExpand)
{
	m_pPorts->Validate(fExpand);
	}

void COptionCardItem::ClearSysBlocks(void)
{
	m_pPorts->ClearSysBlocks();
	}

void COptionCardItem::NotifyInit(void)
{
	m_pPorts->NotifyInit();
	}

void COptionCardItem::CheckMapBlocks(void)
{
	m_pPorts->CheckMapBlocks();
	}

void COptionCardItem::RemoveCard(void)
{
	m_Type = typeNone;

	DelPorts();
	}

void COptionCardItem::SetType(UINT uType)
{
	m_Type = uType;

	DelPorts();

	AddPorts();
	}

void COptionCardItem::SetPower(UINT uPower)
{
	m_Power = uPower;
	}

void COptionCardItem::SetModule(CString Name)
{
	m_Module = Name;
	}

BOOL COptionCardItem::IsSupported(UINT uType)
{
	if( !CheckPlatformSupport(uType) ) {

		return FALSE;
		}

	if( !CheckFeatureSupport(uType) ) {

		return FALSE;
		}

	return TRUE;
	}

void COptionCardItem::PostConvert(void)
{
	if( m_Type == typeCAN ) {

		if( m_pDbase->HasFlag(L"USBRack") ) {

			CCommsPort *pPort = m_pPorts->GetItem(0U);

			if( pPort->m_DriverID == 0x4036 || pPort->m_DriverID == 0x4090 ) {

				m_Type = typeJ1939;

				pPort->m_Binding = bindJ1939;

				pPort->m_Name    = L"J1939 Interface";

				pPort->PostConvert();
				}
			}
		}

	if( m_Type == typeJ1939 ) {

		if( !m_pDbase->HasFlag(L"USBRack") ) {

			CCommsPort *pPort = m_pPorts->GetItem(0U);

			m_Type = typeCAN;

			pPort->m_Name    = L"CAN Interface";

			pPort->m_Binding = bindCAN;

			pPort->PostConvert();
			}
		}

	if( !IsSupported(m_Type) ) {

		RemoveCard();
		}
	}

// Item Naming

CString COptionCardItem::GetHumanName(void) const
{
	if( m_pDbase->HasFlag(L"DualExp") ) {

		return CPrintf(IDS_OPTION_CARD_FMT, 1 + m_Slot);
		}

	if( m_pDbase->HasFlag(L"Canyon") ) {

		return CString(IDS_COMMS_MODULE);
		}

	if( m_pDbase->HasFlag(L"USBRack") ) {

		if( m_Name.IsEmpty() ) {

			return CPrintf(CString(IDS_SLOT_FMT), 1 + GetIndex());
			}

		return m_Name;
		}

	return IDS_OPTION_CARD;
	}

// Download Support

BOOL COptionCardItem::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(m_Slot);

	Init.AddByte(BYTE(m_Type));

	Init.AddItem(itemSimple, m_pPorts);

	return TRUE;
	}

// Property Filters

BOOL COptionCardItem::SaveProp(CString const &Tag) const
{
	if( Tag == "Name" ) {

		return FALSE;
		}

	return CUIItem::SaveProp(Tag);
	}

// Meta Data Creation

void COptionCardItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Slot);
	Meta_AddInteger(Type);
	Meta_AddCollect(Ports);
	Meta_AddString (Name);
	Meta_AddInteger(Number);
	Meta_AddString (Module);

	Meta_SetName((IDS_OPTION_CARD));
	}

// Implementation

void COptionCardItem::AddPorts(void)
{
	for( UINT n = 0;; n++ ) {

		CCommsPort *pPort = EnumPorts(n);

		if( pPort ) {

			m_pPorts->AppendItem(pPort);

			pPort->UpdateDriver();

			continue;
			}

		break;
		}
	}

void COptionCardItem::DelPorts(void)
{
	INDEX n = m_pPorts->GetHead();

	while( !m_pPorts->Failed(n) ) {

		INDEX i = n;

		m_pPorts->GetNext(n);

		m_pPorts->DeleteItem(i);
		}
	}

void COptionCardItem::DoEnables(IUIHost *pHost)
{
	BOOL fRead = m_pDbase->IsReadOnly();

	pHost->EnableUI("ButtonRemoveCard", !fRead && (m_Type > typeNone && m_Type < typeModule));

	pHost->EnableUI("Type", !fRead && m_Type < typeModule);
	}

BOOL COptionCardItem::CheckPower(CString &Warning)
{
	if( m_pDbase->HasFlag(L"USBRack") ) {

		for( CItem *pScan = GetParent(2); pScan; pScan = pScan->GetParent() ) {

			if( pScan->IsKindOf(AfxNamedClass(L"CExpansionItem")) ) {

				CExpansionItem *pExpansion = (CExpansionItem *) pScan;

				return pExpansion->CheckPower(Warning);
				}
			}
		}

	return TRUE;
	}

// Port Enumeration

CCommsPort * COptionCardItem::EnumPorts(UINT i)
{
	switch( m_Type ) {

		case typeSerial:

			return EnumSerial(i);

		case typeCAN:

			return EnumCAN(i);

		case typeProfibus:

			return EnumProfibus(i);

		case typeFireWire:

			return EnumFireWire(i);

		case typeDeviceNet:

			return EnumDeviceNet(i);

		case typeCatLink:
		case typeCanCdl:

			return EnumCatLink(i);

		case typeModem:

			return EnumModem(i);

		case typeMPI:

			return EnumMPI(i);

		case typeEthernet:

			return EnumEthernet(i);

		case typeUSBHost:

			return EnumUSBHost(i);

		case typeJ1939:

			return EnumJ1939(i);

		case typeDnp3:

			return EnumDnp3(i);

		case typeWiFi:

			return EnumWifi(i);

		case typeTest:

			return EnumTest(i);
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumSerial(UINT i)
{
	if( i < (m_pDbase->HasFlag(L"USBRack") ? UINT(4) : UINT(2)) ) {

		CCommsPortSerial *pPort = New CCommsPortSerial;

		if( i == 0 ) {

			pPort->m_Binding  = bindRawSerial;

			pPort->m_PortPhys = 0;

			pPort->m_PortLog  = 0;

			pPort->m_Name     = CString(IDS_SYS_RS232);

			pPort->m_Phys    |= (1<<physicalRS232);
			}

		if( i == 1 ) {

			pPort->m_Binding  = bindRawSerial;

			pPort->m_PortPhys = 0;

			pPort->m_PortLog  = 1;

			pPort->m_Name     = CString(IDS_SYS_RS485);

			pPort->m_Phys    |= (1<<physicalRS485);

			pPort->m_Phys    |= (1<<physicalRS422);
			}

		if( i == 2 ) {

			pPort->m_Binding  = bindRawSerial;

			pPort->m_PortPhys = 1;

			pPort->m_PortLog  = 0;

			pPort->m_Name     = CString(IDS_SYS_RS232);

			pPort->m_Phys    |= (1<<physicalRS232);
			}

		if( i == 3 ) {

			pPort->m_Binding  = bindRawSerial;

			pPort->m_PortPhys = 1;

			pPort->m_PortLog  = 1;

			pPort->m_Name     = CString(IDS_SYS_RS485);

			pPort->m_Phys    |= (1<<physicalRS485);

			pPort->m_Phys    |= (1<<physicalRS422);
			}

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumModem(UINT i)
{
	if( i < 1 ) {

		CCommsPortSerial *pPort = New CCommsPortSerial;

		pPort->m_Binding = bindModem;

		pPort->m_Name    = CString(IDS_MODEM_PORT);

		pPort->m_Phys   |= (1<<physicalRS232);

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumCAN(UINT i)
{
	if( i < 1 ) {

		CCommsPort *pPort = New CCommsPortCAN;

		pPort->m_Binding  = bindCAN;

		pPort->m_Name     = CString(IDS_COMMS_FACE_CAN);

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumProfibus(UINT i)
{
	if( i < 1 ) {

		CCommsPort *pPort = New CCommsPortProfibus;

		pPort->m_Binding  = bindProfibus;

		pPort->m_Name     = CString(IDS_COMMS_FACE_PBUS);

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumFireWire(UINT i)
{
	if( i < 1 ) {

		CCommsPort *pPort = New CCommsPortFireWire;

		pPort->m_Binding  = bindFireWire;

		pPort->m_Name     = CString(IDS_COMMS_FACE_FWIRE);

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumDeviceNet(UINT i)
{
	if( i < 1 ) {

		CCommsPort *pPort = New CCommsPortDevNet;

		pPort->m_Binding  = bindDeviceNet;

		pPort->m_Name     = CString(IDS_COMMS_FACE_DNET);

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumCatLink(UINT i)
{
	if( m_pDbase->HasFlag(L"USBRack") ) {

		if( i == 0 ) {

			CCommsPortCAN *pPort = New CCommsPortCAN;

			pPort->m_Binding     = bindCAN;

			pPort->m_Remotable   = TRUE;

			pPort->m_Name        = CString(IDS_COMMS_FACE_CAN);

			return pPort;
			}

		if( i == 1 ) {

			CCommsPort *pPort    = New CCommsPortCatLink;

			pPort->m_Binding     = bindCatLink;

			pPort->m_Remotable   = TRUE;

			pPort->m_Name        = CString(IDS_COMMS_FACE_CDL);

			return pPort;
			}
		}
	else {
		if( i < 1 ) {

			CCommsPort *pPort    = New CCommsPortCatLink;

			pPort->m_Binding     = bindCatLink;

			pPort->m_Name        = CString(IDS_COMMS_FACE_CDL);

			return pPort;
			}
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumMPI(UINT i)
{
	if( i < 1 ) {

		CCommsPort *pPort = New CCommsPortMPI;

		pPort->m_Binding  = bindMPI;

		pPort->m_Name     = CString(IDS_COMMS_FACE_MPI);

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumEthernet(UINT i)
{
	return NULL;
	}

CCommsPort * COptionCardItem::EnumUSBHost(UINT i)
{
	return NULL;
	}

CCommsPort * COptionCardItem::EnumJ1939(UINT i)
{
	if( i < 1 ) {

		CCommsPort *pPort = New CCommsPortJ1939;

		pPort->m_Binding  = bindJ1939;

		pPort->m_Name     = L"J1939 Interface";

		return pPort;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumTest(UINT i)
{
	if( i == 0 ) {

		CCommsPortTest *pPort = New CCommsPortTest;

		pPort->m_PortPhys = 0;

		pPort->m_Name     = L"Test Port";

		return pPort;
		}

	if( i == 1 ) {

		// TODO

		return NULL;
		}

	if( i == 2 ) {

		// TODO

		return NULL;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumDnp3(UINT i)
{
	if( i == 0 ) {

		// TODO

		return NULL;
		}

	if( i == 1 ) {

		// TODO

		return NULL;
		}

	return NULL;
	}

CCommsPort * COptionCardItem::EnumWifi(UINT i)
{
	if( i < 1 ) {

		CCommsPortWifi *pPort = New CCommsPortWifi;

		pPort->m_Binding = bindWifi;

		pPort->m_Name    = CString(IDS_WIFI_PORT);

		return pPort;
		}
	
	return NULL;
	}

// Filtering

BOOL COptionCardItem::CheckPlatformSupport(UINT Type) const
{
	switch( Type ) {

		case typeNone:
		case typeCAN:
		case typeDeviceNet:
		case typeProfibus:
		case typeModem:
		case typeCatLink:

			return TRUE;
		}

	if( m_pDbase->HasFlag(L"USBRack") ) {

		switch( Type ) {

			case typeModule:
			case typeJ1939:
			case typeDongle:
			case typeDnp3:
			case typeTest:
			case typeCanCdl:

				return TRUE;

			case typeModem:

				return CheckRackSupport(Type);
			}

		return FALSE;
		}

	if( m_pDbase->HasFlag(L"DSP") ) {

		switch( Type ) {

			case typeUSBHost:

				return FALSE;
			}
		}

	switch( Type ) {

		case typeSerial:
		case typeMPI:
		case typeFireWire:
		case typeCatLink:
		case typeEthernet:
		case typeModem:

			return TRUE;

		case typeUSBHost:

			return !m_pDbase->HasFlag(L"USBHost");
		}

	return FALSE;
	}

BOOL COptionCardItem::CheckFeatureSupport(UINT Type) const
{
	switch( Type ) {

		case typeProfibus:

			return C3OemFeature(L"Profibus", TRUE);

		case typeFireWire:

			return C3OemFeature(L"FireWire", FALSE);

		case typeDeviceNet:

			return C3OemFeature(L"DeviceNet", TRUE);

		case typeEthernet:

			return C3OemFeature(L"Ethernet", TRUE);

		case typeUSBHost:

			return C3OemFeature(L"USBHost", TRUE);

		case typeMPI:

			return C3OemFeature(L"MPI", TRUE);

		case typeSerial:

			return C3OemFeature(L"Serial", TRUE);

		case typeModem:

			return C3OemFeature(L"Modem", TRUE);

		case typeCanCdl:

			return C3OemFeature(L"CatLink", FALSE);

		case typeDongle:

			return C3OemFeature(L"Dongle", TRUE);

		case typeCatLink:

			if( !m_pDbase->HasFlag(L"USBRack") ) {

				if( C3OemFeature(L"CatLink", FALSE) ) {

					return TRUE;
					}

				if( C3OemFeature(L"RedLion", FALSE) ) {

					return TRUE;
					}
				}

			return FALSE;
		}

	return TRUE;
	}

BOOL COptionCardItem::CheckRackSupport(UINT Type) const
{
	if( Type == typeModem ) {

		if( GetIndex() == 0 ) {

			COptionCardList	*	pOpts = (COptionCardList *	) GetParent();

			COptionCardRackItem *	pRack = (COptionCardRackItem *	) pOpts->GetParent();

			COptionCardRackList *	pList = (COptionCardRackList *	) pRack->GetParent();

			if( pList->GetParent()->IsKindOf(AfxRuntimeClass(CTetheredItem)) ) {

				return !pList->FindItemPos(pRack);
				}

			return pRack->GetIndex() == NOTHING;
			}
		}

	return FALSE;
	}

// End of File
