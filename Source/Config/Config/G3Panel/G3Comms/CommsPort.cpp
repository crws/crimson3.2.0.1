
#include "Intern.hpp"

#include "CommsPort.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsDeviceList.hpp"
#include "CommsManager.hpp"
#include "CommsPortPage.hpp"
#include "CommsSysBlock.hpp"
#include "CommsSystem.hpp"
#include "CommsPortSerial.hpp"
#include "CommsPortCAN.hpp"
#include "CommsPortJ1939.hpp"
#include "CommsPortDevNet.hpp"
#include "CommsPortProfibus.hpp"

////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <shlink.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

// Runtime Class

AfxImplementRuntimeClass(CCommsPort, CCodedHost);

// Port Creation

CCommsPort * CCommsPort::CreatePort(char cTag)
{
	switch( cTag ) {

		case 'S':
			return New CCommsPortSerial;

		case 'C':
			return New CCommsPortCAN;

		case 'J':
			return New CCommsPortJ1939;

		case 'D':
			return New CCommsPortDevNet;

		case 'P':
			return New CCommsPortProfibus;
	}

	return NULL;
}

// Constructor

CCommsPort::CCommsPort(CLASS Class)
{
	m_Name	    = CString(IDS_COMMS_NAME);

	m_Number    = 0;

	m_Import    = NOTHING;

	m_PortPhys  = NOTHING;

	m_PortLog   = NOTHING;

	m_Binding   = bindRawSerial;

	m_BindMask  = 0;

	m_DriverID  = 0;

	m_DriverRpc = 0;

	m_pConfig   = NULL;

	m_pDriver   = NULL;

	m_pDevices  = AfxNewObject(CCommsDeviceList, Class);

	m_uImage    = IDI_PORT;

	m_Remotable = FALSE;
}

// Destructor

CCommsPort::~CCommsPort(void)
{
	FreeDriverObject();
}

// UI Creation

BOOL CCommsPort::OnLoadPages(CUIPageList *pList)
{
	CCommsPortPage *pPage = New CCommsPortPage(this);

	pList->Append(pPage);

	return FALSE;
}

// UI Update

void CCommsPort::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag == "ButtonDeletePort" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_DEL_PORT);
	}

	if( Tag == "ButtonClearPort" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_CLEAR_PORT);
	}

	if( Tag == "ButtonAddDevice" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_ADD_DEVICE);
	}

	if( Tag == "DriverID" ) {

		if( UpdateDriver() ) {

			UpdateConfig(pHost);

			CheckDevices(pHost);

			DoEnables(pHost);

			pHost->SendUpdate(updateRename);

			pHost->RemakeUI();
		}
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Driver Access

ICommsDriver * CCommsPort::GetDriver(void) const
{
	return m_pDriver;
}

// Item Lookup

CCommsDevice * CCommsPort::FindDevice(UINT uDevice) const
{
	return m_pDevices->FindDevice(uDevice);
}

CCommsDevice * CCommsPort::FindDevice(CString Name) const
{
	return m_pDevices->FindDevice(Name);
}

CCommsSysBlock * CCommsPort::FindBlock(UINT uBlock) const
{
	return m_pDevices->FindBlock(uBlock);
}

// Overridables

UINT CCommsPort::GetPageType(void) const
{
	return 0;
}

CHAR CCommsPort::GetPortTag(void) const
{
	return 0;
}

BOOL CCommsPort::IsBroken(void) const
{
	return FALSE;
}

BOOL CCommsPort::IsRemotable(void) const
{
	return FALSE;
}

void CCommsPort::FindBindingMask(void)
{
	if( m_Remotable ) {

		m_BindMask |= (1 << bindRemote);
	}
}

// Attributes

UINT CCommsPort::GetTreeImage(void) const
{
	return m_uImage;
}

CString CCommsPort::GetLabel(void) const
{
	CString Label = m_Used.IsEmpty() ? m_Name : m_Used;

	if( m_pDriver ) {

		CString Short = m_pDriver->GetString(stringShortName);

		if( !Short.IsEmpty() ) {

			Label += L" - ";

			Label += Short;
		}
	}

	return Label;
}

CString CCommsPort::GetShortName(void) const
{
	return CPrintf(L"Port%u", m_Number);
}

BOOL CCommsPort::CanAddDevice(void) const
{
	if( m_pDriver ) {

		switch( m_pDriver->GetType() ) {

			case driverMaster:
			case driverSlave:
			case driverCamera:
			case driverHoneywell:

				if( m_pDevices->GetItemCount() == 0 ) {

					return TRUE;
				}

				if( m_pDriver->GetFlags() & dflagOneDevice ) {

					return FALSE;
				}

				if( m_pDriver->GetDeviceConfig() ) {

					return TRUE;
				}
				break;
		}
	}

	return FALSE;
}

BOOL CCommsPort::IsModem(void) const
{
	if( m_DriverID >= 0xFF00 && m_DriverID <= 0xFF0F ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPort::IsExclusive(void) const
{
	if( m_pDriver ) {

		switch( m_pDriver->GetType() ) {

			case driverMaster:

				if( m_pDriver->GetBinding() == bindStdSerial ) {

					return FALSE;
				}
				break;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPort::IsProgram(void) const
{
	if( m_Binding == bindRawSerial ) {

		if( m_PortPhys == 0 ) {

			return TRUE;
		}
	}

	return FALSE;
}

// Driver Creation

ICommsDriver * CCommsPort::CreateDriver(UINT uID)
{
	return C3CreateDriver(uID);
}

// Operations

void CCommsPort::Validate(BOOL fExpand)
{
	m_pDevices->Validate(fExpand);
}

void CCommsPort::ClearSysBlocks(void)
{
	m_pDevices->ClearSysBlocks();
}

void CCommsPort::NotifyInit(void)
{
	m_pDevices->NotifyInit();
}

void CCommsPort::CheckMapBlocks(void)
{
	m_pDevices->CheckMapBlocks();
}

BOOL CCommsPort::ClearSettings(void)
{
	if( m_DriverID ) {

		m_DriverID  = 0;

		m_DriverRpc = 0;

		UpdateDriver();

		FreeConfigItem();

		m_pDevices->UpdateDriver();

		m_pDevices->DeleteAll();

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPort::UpdateDriver(void)
{
	if( m_DriverID ) {

		if( m_pDriver ) {

			if( m_pDriver->GetID() == m_DriverID ) {

				return FALSE;
			}

			FreeDriverObject();

			m_DriverRpc = 0;
		}

		m_pDriver = CreateDriver(m_DriverID);

		if( m_pDriver ) {

			if( IsRemotable() && m_pDriver && m_pDriver->GetFlags() & dflagRemotable ) {

				m_DriverRpc = m_pDriver->GetType();

				if( m_pDriver->GetFlags() & dflagRpcMaster ) {

					m_DriverRpc = driverMaster;
				}

				if( m_pDriver->GetFlags() & dflagRpcSlave ) {

					m_DriverRpc = driverSlave;
				}
			}
		}
	}
	else {
		if( !m_pDriver ) {

			return FALSE;
		}

		FreeDriverObject();
	}

	return TRUE;
}

void CCommsPort::SetUsed(CString Used)
{
	m_Used = Used;
}

// Item Naming

CString CCommsPort::GetItemOrdinal(void) const
{
	return !m_Where.IsEmpty() ? m_Where : CPrintf(IDS_COMMS_PORT_ID, m_Number);
}

// Persistance

void CCommsPort::Init(void)
{
	CCodedHost::Init();

	AllocNumber();

	FindBindingMask();
}

void CCommsPort::PostLoad(void)
{
	CheckDriver();

	AllocNumber();

	if( !m_Number ) {

		CCommsManager *pManager = (CCommsManager *) GetParent(AfxRuntimeClass(CCommsManager));

		m_Number = pManager->AllocPortNumber();
	}

	FindBindingMask();

	CCodedHost::PostLoad();
}

void CCommsPort::Kill(void)
{
	FreeDriverObject();

	CCodedHost::Kill();
}

// Conversion

void CCommsPort::PostConvert(void)
{
}

// Download Support

BOOL CCommsPort::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_PortPhys));

	Init.AddByte(BYTE(m_PortLog));

	Init.AddWord(WORD(m_Number));

	if( m_pDriver ) {

		Init.AddWord(WORD(m_DriverID));

		if( IsModem() ) {

			Init.AddItem(itemSized, m_pConfig);
		}
		else {
			Init.AddWord(WORD(m_DriverRpc));

			AddImage(Init);

			Init.AddItem(itemSized, m_pConfig);

			Init.AddItem(itemSimple, m_pDevices);
		}

		return TRUE;
	}

	Init.AddWord(WORD(0));

	return TRUE;
}

// Meta Data Creation

void CCommsPort::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddString(Name);
	Meta_AddString(Where);
	Meta_AddString(PortTag);
	Meta_AddInteger(Number);
	Meta_AddInteger(PortPhys);
	Meta_AddInteger(PortLog);
	Meta_AddInteger(Binding);
	Meta_AddInteger(DriverID);
	Meta_AddVirtual(Config);
	Meta_AddCollect(Devices);
	Meta_AddInteger(DriverRpc);

	Meta_SetName((IDS_COMMS_PORT));
}

// Implementation

BOOL CCommsPort::FreeDriverObject(void)
{
	if( m_pDriver ) {

		m_pDriver->Release();

		m_pDriver = NULL;

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPort::FreeConfigItem(void)
{
	if( m_pConfig ) {

		m_pConfig->Kill();

		delete m_pConfig;

		m_pConfig = NULL;

		return TRUE;
	}

	return FALSE;
}

void CCommsPort::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_DriverID ? TRUE : FALSE;

	BOOL fRead   = m_pDbase->IsReadOnly();

	pHost->EnableUI(L"ButtonDeletePort", !fRead);

	pHost->EnableUI(L"ButtonClearPort", !fRead && fEnable);

	pHost->EnableUI(L"ButtonAddDevice", !fRead && fEnable && CanAddDevice());
}

BOOL CCommsPort::CheckDriver(void)
{
	if( m_pDriver = CreateDriver(m_DriverID) ) {

		CLASS Class = m_pDriver->GetDriverConfig();

		if( Class ) {

			if( m_pConfig ) {

				if( AfxPointerClass(m_pConfig) == Class ) {

					return TRUE;
				}

				FreeConfigItem();
			}

			m_pConfig = AfxNewObject(CUIItem, Class);

			m_pConfig->SetParent(this);

			m_pConfig->Init();

			SetDirty();
		}
		else {
			if( FreeConfigItem() ) {

				SetDirty();
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPort::AllocNumber(void)
{
	if( !m_Number ) {

		CCommsManager *pManager = (CCommsManager *) GetParent(AfxRuntimeClass(CCommsManager));

		m_Number = pManager->AllocPortNumber();

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPort::UpdateConfig(IUIHost *pHost)
{
	if( !pHost->InReplay() ) {

		CLASS Class = NULL;

		CLASS Match = NULL;

		if( m_pDriver ) {

			Class = m_pDriver->GetDriverConfig();
		}

		if( m_pConfig ) {

			Match = AfxPointerClass(m_pConfig);
		}

		if( Class != Match ) {

			CCmd *pCmd = New CCmdSubItem(L"Config", Class);

			pHost->ExecExtraCmd(pCmd);

			return TRUE;
		}
	}

	return FALSE;
}

void CCommsPort::CheckDevices(IUIHost *pHost)
{
	if( !pHost->InReplay() ) {

		HGLOBAL hPrev = m_pDevices->TakeSnapshot();

		if( m_DriverID ) {

			m_pDevices->UpdateDriver();

			switch( m_pDriver->GetType() ) {

				case driverMaster:
				case driverSlave:
				case driverCamera:
				case driverHoneywell:

					if( m_pDevices->GetItemCount() == 0 ) {

						CCommsDevice *pDev = New CCommsDevice;

						m_pDevices->AppendItem(pDev);
					}
					break;

				default:
					m_pDevices->UpdateDriver();

					m_pDevices->DeleteAll();

					break;
			}

			m_pDevices->CheckMapBlocks();

			m_pDevices->ClearSysBlocks();
		}
		else {
			m_pDevices->UpdateDriver();

			m_pDevices->DeleteAll();
		}

		HGLOBAL hData = m_pDevices->TakeSnapshot();

		CCmd *  pCmd  = New CCmdSubItem(L"Devices",
						hPrev,
						hData
		);

		pHost->SaveExtraCmd(pCmd);

		pHost->SendUpdate(updateChildren);

		return;
	}

	m_pDevices->UpdateDriver();

	pHost->SendUpdate(updateChildren);
}

BOOL CCommsPort::AddImage(CInitData &Init)
{
	WORD id = m_pDriver->GetID();

	if( id != 0xFE01 ) {

		CString Path = FindDriverPath(id);

		if( !Path.IsEmpty() ) {

			CFilename Name = Path;

			if( Name.GetLength() ) {

				HANDLE hFile = Name.OpenRead();

				if( hFile != INVALID_HANDLE_VALUE ) {

					// LATER -- Put DLD image in its own item. We will
					// need a manager object that keeps single instances
					// of each driver we're using and manages references.

					UINT  uMax  = 1024 * 1024;

					PBYTE pData = New BYTE[uMax];

					DWORD uSize = 0;

					ReadFile(hFile, pData, uMax, &uSize, NULL);

					if( uSize <= uMax ) {

						Init.AddLong(uSize);

						Init.AddData(pData, uSize);

						delete[] pData;

						CloseHandle(hFile);

						return TRUE;
					}

					delete[] pData;

					CloseHandle(hFile);
				}
			}
		}
	}

	Init.AddLong(0);

	return FALSE;
}

CString CCommsPort::FindDriverPath(WORD id)
{
	CString Machine;

	CString Extension;

	if( UsingEmulator() ) {

		Machine   = L"Win32";

		Extension = L"dll";
	}
	else {
		CSystemItem  *pSystem = m_pDbase->GetSystemItem();

		CCommsSystem *pComms  = (CCommsSystem *) pSystem;

		Machine = pComms->GetDldFolder();

		if( Machine.StartsWith(L"Linux") ) {

			Extension = L"so";
		}
		else {
			Extension = L"dld";
		}
	}

	for( UINT p = 0; p < 2; p++ ) {

		CFilename Folder;

		if( p == 0 ) {

			Folder = afxModule->GetFilename().WithName(L"DLDs\\" + Machine);
		}
		else {
			#ifdef _DEBUG

			CString Build = L"Debug";

			#else

			CString Build = L"Release";

			#endif

			Folder = afxModule->GetFilename().WithName(L"..\\..\\..\\DLDs\\" + Machine + L"\\" + Build);
		}

		CFilename File = Folder + CPrintf(L"\\%4.4X.", id) + Extension;

		if( File.Exists() ) {

			return File;
		}
	}

	return L"";
}

BOOL CCommsPort::UsingEmulator(void)
{
	return Link_GetEmulate();
}

// End of File
