
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ContactsPage_HPP

#define INCLUDE_ContactsPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMailContacts;

//////////////////////////////////////////////////////////////////////////
//
// Contacts Page
//

class CContactsPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CContactsPage(CMailContacts *pContacts);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CMailContacts * m_pContacts;
	};

// End of File

#endif
