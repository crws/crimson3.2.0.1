
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CTVueTagImport_HPP

#define INCLUDE_CTVueTagImport_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

struct CUSTOMTAGIMPINFO;
#define	CTPRESENT	4
#define	CTSELECTED	8

// Driver IDs
#define	CT_SER_ID	0x4034
#define	CT_TCP_ID	0x3521

// Control Techniques Device Assignments
// Devices - TCP
#define	DVEPP	1
#define	DVSP	3
#define	DVSK	5
#define	DVGP	7
#define	DVST	9
#define	DVMC	11
#define	DVBR	13
// Devices - Serial
#define	DVEPI	15
#define	DVEPB	17

// Control Techniques Tag Assignemnts
#define	DMPRE	1	// Premapped names
#define	DMREG	3	// Registers only

// Line Destination Columns - Control Techniques
#define	L_NAME	0
#define	L_TYPE	1
#define	L_DEV	2
#define	L_REG	3
#define	L_ACC	4
#define	L_UNIT	5
#define	L_RNG	6
#define	L_DEC	7

#define	CT_LIST_LEN	11

class CCTVueTagImport : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCTVueTagImport(void);

		BOOL	IsCTVueHeader(PTXT sLine);
		void	SetCTInfoPtr(CUSTOMTAGIMPINFO * pInfo);
		CString	ProcessCTVueHeader(PTXT sLine);
		CString	GetCTVueLine(void);
		CString	GetColHeaders(void);
		CString	GetCTVueTagData(void);

	protected:

		// Data
		CUSTOMTAGIMPINFO * m_pInfo;
		UINT	m_ColDef[CT_LIST_LEN + 1];
		CString	m_BitColDef;
		CString	m_ArrColDef;
		CString	m_StrColDef;
		CString	m_NumColDef;

		// Implementation
		CString	GetCTVueNum(CStringArray Fields);
		CString	GetCTVue2St(CStringArray Fields);
		CString	GetCTVueStr(CStringArray Fields);
		UINT	GetCTVueFormatType(CString Type);
		CString	GetCTVueTreatAsType(void);
		CString	GetCTVueRWType(CString sIn);

		// Column Headers
		CString	GetCHUnit (void);
		CString	GetCHDec  (void);
		CString	GetCHRange(void);

		// Helpers
		void	SetColDefs(CString sLine);
		BOOL	MatchText(CString Type, CString Match);
		CString	FixName(CString sName);
		CString FixDevc(CString sDev);
		CString	FixRegr(CStringArray Fields);
		CString	StripLeft (CString sIn);
		CString	StripRight(CString sIn);
		CString	FindCommaN(CString sLine, UINT uNum);
		CString	GetRangeString(CStringArray Fields);
		UINT	GetDecim(CStringArray Fields);
		CString	GetUnitsString(CStringArray Fields);
		BOOL	IsNumericChar(UINT c, UINT uPos);

		friend class CTagExport;
	};

// End of File

#endif
