
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_AccessMask_HPP

#define INCLUDE_AccessMask_HPP

//////////////////////////////////////////////////////////////////////////
//
// Access Mask Item
//

class DLLNOT CAccessMask : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAccessMask(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL IsDefault(void);

		// Operations
		void BlockDefault(void);
		void SetDispPage(void);
		void LoadData(UINT uData);
		UINT ReadData(void);

		// Data Members
		UINT m_Rights[30];
		UINT m_Mode;

	protected:
		// Data Members
		BOOL m_fDefault;
		BOOL m_fDispPage;
		UINT m_uSpecify;
		UINT m_uDefault;

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
