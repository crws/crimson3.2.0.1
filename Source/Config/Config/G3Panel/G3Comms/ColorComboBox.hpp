
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ColorComboBox_HPP

#define INCLUDE_ColorComboBox_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CColorManager;
class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Color Combo Box
//

class DLLAPI CColorComboBox : public CDropComboBox
{
	public:
		// Dynamic Class
		AfxDeclareRuntimeClass();

		// Constructor
		CColorComboBox(CUIElement *pUI);

		// Destructor
		~CColorComboBox(void);

		// Attributes
		COLOR GetColor(void) const;

		// Operations
		void LoadList(void);
		void ShowColors(void);
		BOOL SetColor(COLOR Color);

	protected:
		// Data Members
		CCommsSystem  * m_pSystem;
		CColorManager * m_pColor;
		COLOR		m_Color;
		UINT		m_uGroup;
		UINT		m_uIndex;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item);
		void OnChar(UINT uCode, DWORD dwFlags);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnKeyUp(UINT uCode, DWORD dwFlags);
		void OnSysKeyDown(UINT uCode, DWORD dwFlags);
		void OnSysKeyUp(UINT uCode, DWORD dwFlags);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
	};

// End of File

#endif
