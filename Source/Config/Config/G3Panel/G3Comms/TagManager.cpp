
#include "Intern.hpp"

#include "TagManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "Tag.hpp"
#include "TagFolder.hpp"
#include "TagFlag.hpp"
#include "TagList.hpp"
#include "TagManagerPage.hpp"
#include "TagNumeric.hpp"
#include "TagString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager
//

// Dynamic Class

AfxImplementDynamicClass(CTagManager, CUIItem);

// Constructor

CTagManager::CTagManager(void)
{
	m_Handle    = HANDLE_NONE;

	m_LogToDisk = 0;

	m_FileLimit = 60;

	m_FileCount = 12;

	m_WithBatch = 0;

	m_SignLogs  = 0;

	m_LogToPort = 0;

	m_Drive	    = 0;

	m_pTags     = New CTagList;
}

// Destructor

CTagManager::~CTagManager(void)
{
}

// UI Creation

BOOL CTagManager::OnLoadPages(CUIPageList *pList)
{
	CTagManagerPage *pPage = New CTagManagerPage(this);

	pList->Append(pPage);

	return FALSE;
}

// UI Update

void CTagManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		BOOL fRead = pItem->GetDatabase()->IsReadOnly();

		pHost->EnableUI("ButtonImportTags", !fRead);
	}

	if( Tag.IsEmpty() || Tag == "LogToDisk" ) {

		pHost->EnableUI("Drive", m_LogToDisk);

		pHost->EnableUI("FileCount", m_LogToDisk);

		pHost->EnableUI("FileLimit", m_LogToDisk);

		pHost->EnableUI("WithBatch", m_LogToDisk);

		pHost->EnableUI("SignLogs", m_LogToDisk);
	}

	if( Tag == L"ButtonExportTags" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_TAGS_EXPORT);
	}

	if( Tag == L"ButtonImportTags" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_TAGS_IMPORT);
	}

	if( Tag == L"ButtonFindAlarms" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_TAGS_FIND_ALARMS);
	}

	if( Tag == L"ButtonFindTriggers" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_TAGS_FIND_TRIGGERS);
	}

	if( Tag == L"ButtonFindLocked" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_TAGS_FIND_LOCKED);
	}

	if( Tag == L"ButtonFindUnused" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_TAGS_FIND_UNUSED);
	}
}

// Attributes

BOOL CTagManager::HasCircular(void) const
{
	return m_pTags->HasCircular();
}

BOOL CTagManager::CanStepFragment(CString Code) const
{
	if( Code[0] != '[' ) {

		UINT uLen = Code.GetLength();

		if( Code[uLen - 1] == ']' ) {

			UINT    uPos = Code.FindRev('[');

			UINT    uUse = uLen - uPos - 2;

			CString Step = Code.Mid(uPos+1, uUse);

			if( Step.FindNot(L"0123456789 ") == uUse ) {

				return TRUE;
			}

			return FALSE;
		}

		INDEX Index = m_pTags->IndexFromName(Code);

		if( !m_pTags->Failed(Index) ) {

			if( StepName(Code) ) {

				return TRUE;
			}

			while( m_pTags->GetNext(Index) ) {

				CTag *pTag = m_pTags->GetItem(Index);

				if( pTag->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

					continue;
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Operations

BOOL CTagManager::CreateTag(CString Name, UINT Type, UINT Size)
{
	return CreateTag(NULL, Name, Type, Size);
}

BOOL CTagManager::Validate(CCodedTree &Done, BOOL fExpand)
{
	CCodedTree Busy;

	for( INDEX n = m_pTags->GetHead(); !m_pTags->Failed(n); m_pTags->GetNext(n) ) {

		CTag       * pTag = m_pTags->GetItem(n);

		CCodedItem * pVal = pTag->m_pValue;

		if( pVal ) {

			Compile(NULL, Done, Busy, pTag, fExpand);
		}

		pTag->UpdateCircular();
	}

	return TRUE;
}

BOOL CTagManager::TagCheck(CCodedTree &Done, CIndexTree &Tags, UINT uTag)
{
	CSystemWnd *pHost = (CSystemWnd *) afxMainWnd->GetDlgItemPtr(IDVIEW);

	CCodedTree Busy;

	for( INDEX n = m_pTags->GetHead(); !m_pTags->Failed(n); m_pTags->GetNext(n) ) {

		CTag *pTag = m_pTags->GetItem(n);

		if( pTag->RefersToTag(Busy, m_pTags, uTag) ) {

			Compile(pHost, Done, Busy, pTag, TRUE);

			Tags.Insert(pTag->GetIndex());
		}
	}

	return TRUE;
}

BOOL CTagManager::StepFragment(CString &Code, UINT uStep)
{
	if( Code[0] != '[' ) {

		UINT uLen = Code.GetLength();

		if( Code[uLen - 1] == ']' ) {

			UINT    uPos = Code.FindRev('[');

			UINT    uUse = uLen - uPos - 2;

			CString Step = Code.Mid(uPos+1, uUse);

			if( Step.FindNot(L"0123456789 ") == uUse ) {

				Step.Printf(L"%u", watoi(Step) + 1);

				Code = Code.Left(uPos+1) + Step + L']';

				return TRUE;
			}

			return FALSE;
		}

		INDEX   Index = m_pTags->IndexFromName(Code);

		CString Name  = Code;

		CString Prop  = L"";

		if( m_pTags->Failed(Index) ) {

			UINT uPos = Name.FindRev('.');

			if( uPos == NOTHING ) {

				return FALSE;
			}

			Prop  = Name.Mid(uPos);

			Name  = Name.Left(uPos);

			Index = m_pTags->IndexFromName(Name);

			if( m_pTags->Failed(Index) ) {

				return FALSE;
			}
		}

		if( StepName(Name) ) {

			Code = Name + Prop;

			return TRUE;
		}

		while( m_pTags->GetNext(Index) ) {

			CTag *pTag = m_pTags->GetItem(Index);

			if( pTag->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

				continue;
			}

			Code = pTag->m_Name + Prop;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CTagManager::FindCircular(CStringArray &List)
{
	return m_pTags->FindCircular(List);
}

void CTagManager::PostConvert(void)
{
	UINT uGroup = m_pDbase->GetSoftwareGroup();

	if( uGroup <= SW_GROUP_3B ) {

		if( uGroup <= SW_GROUP_2 ) {

			m_pTags->MakeLite();
		}

		m_LogToDisk = 0;
	}
}

// Download Support

BOOL CTagManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_TAG_MANAGER);

	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_LogToDisk));
	Init.AddWord(WORD(m_FileLimit));
	Init.AddWord(WORD(m_FileCount));
	Init.AddByte(BYTE(m_WithBatch));
	Init.AddByte(BYTE(m_SignLogs));

	Init.AddItem(itemSimple, m_pTags);

	Init.AddByte(BYTE(m_LogToPort));

	Init.AddByte(BYTE(m_Drive));

	return TRUE;
}

// UI Creation

CViewWnd * CTagManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		return AfxNewObject(CViewWnd, AfxNamedClass(L"CTagNavTreeWnd"));
	}

	if( uType == viewResource ) {

		return AfxNewObject(CViewWnd, AfxNamedClass(L"CTagResTreeWnd"));
	}

	return CUIItem::CreateView(uType);
}

// Item Naming

BOOL CTagManager::IsHumanRoot(void) const
{
	return TRUE;
}

CString CTagManager::GetHumanName(void) const
{
	return IDS_DATA_TAGS;
}

// Meta Data Creation

void CTagManager::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddInteger(LogToDisk);
	Meta_AddInteger(FileLimit);
	Meta_AddInteger(FileCount);
	Meta_AddInteger(WithBatch);
	Meta_AddInteger(SignLogs);
	Meta_AddCollect(Tags);
	Meta_AddInteger(LogToPort);
	Meta_AddInteger(Drive);

	Meta_SetName((IDS_TAG_MANAGER));
}

// Creation Helpers

BOOL CTagManager::CreateTag(CString *pFull, CString Name, UINT Type, UINT Size)
{
	CSysProxy System;

	System.Bind();

	CString Nav  = System.GetNavPos();

	CString Menu = CString(IDS_CREATE_2) + (pFull ? CString(IDS_TAG) : Name);

	System.SaveCmd(New CCmdMulti(Nav, Menu, navLock));

	if( CreateTag(System, pFull, Name, Type, Size) ) {

		System.SaveCmd(New CCmdMulti(Nav, Menu, navLock));

		System.ItemUpdated(m_pTags, updateChildren);

		return TRUE;
	}

	System.KillLastCmd();

	return FALSE;
}

BOOL CTagManager::CreateTag(CSysProxy &System, CString *pFull, CString Name, UINT Type, UINT Size)
{
	CString Full  = Name;

	CString Root  = L"";

	CItem * pRoot = this;

	CTag  * pTag  = NULL;

	UINT    uFind = Name.FindRev(L'.');

	if( uFind < NOTHING ) {

		Root = Name.Left(uFind);

		// cppcheck-suppress uselessAssignmentArg

		Name = Name.Mid(uFind+1);

		if( !CreateTag(System, NULL, Root, typeVoid, 0) ) {

			return FALSE;
		}

		pRoot = m_pTags->FindByName(Root);

		Root  = Root + L'.';
	}

	if( Type == typeVoid ) {

		CItem *pItem = m_pTags->FindByName(Full);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

				return TRUE;
			}

			return FALSE;
		}
	}

	if( pFull ) {

		m_pTags->MakeUnique(Full);

		*pFull = Full;
	}
	else {
		if( m_pTags->FindByName(Full) ) {

			return FALSE;
		}
	}

	if( CreateTag(pTag, Type) ) {

		CString Base = Full.Mid(Root.GetLength());

		m_pTags->AppendItem(pTag);

		pTag->SetName(Full);

		if( Type == typeReal ) {

			if( pTag->IsKindOf(AfxRuntimeClass(CTagNumeric)) ) {

				CTagNumeric *pNum = (CTagNumeric *) pTag;

				pNum->m_TreatAs = 2;
			}
		}

		if( Size ) {

			if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

				CDataTag *pData = (CDataTag *) pTag;

				pData->m_Extent = Size;

				pData->UpdateAddr(FALSE);
			}
		}

		CNavTreeWnd::CCmdCreate *pCmd = New CNavTreeWnd::CCmdCreate(pRoot, pTag);

		pCmd->m_View = IDVIEW;

		pCmd->m_Side = IDVIEW;

		System.SaveCmd(pCmd);

		return TRUE;
	}

	return FALSE;
}

BOOL CTagManager::CreateTag(CTag * &pTag, UINT Type)
{
	if( Type == typeInteger || Type == typeReal || Type == typeNumeric ) {

		pTag = New CTagNumeric;

		return TRUE;
	}

	if( Type == typeLogical ) {

		pTag = New CTagFlag;

		return TRUE;
	}

	if( Type == typeString ) {

		pTag = New CTagString;

		return TRUE;
	}

	if( Type == typeVoid ) {

		pTag = New CTagFolder;

		return TRUE;
	}

	return FALSE;
}

// Implementation

BOOL CTagManager::Compile(CSystemWnd *pHost, CCodedTree &Done, CCodedTree &Busy, CTag *pTag, BOOL fExpand)
{
	CCodedItem *pVal = pTag->m_pValue;

	if( pVal ) {

		if( Busy.Failed(Busy.Find(pVal)) ) {

			Busy.Insert(pVal);

			for( UINT r = 0; r < pVal->m_Refs.GetCount(); r++ ) {

				CDataRef const &Ref = (CDataRef const &) pVal->m_Refs[r];

				if( Ref.t.m_IsTag ) {

					CTag *pRef = m_pTags->GetItem(Ref.t.m_Index);

					if( pRef ) {

						Compile(pHost, Done, Busy, pRef, fExpand);
					}
				}
			}

			if( Done.Failed(Done.Find(pVal)) ) {

				if( pHost ) {

					BOOL fPrev = pTag->IsBroken();

					UINT uPrev = pTag->GetTreeImage();

					pVal->Recompile(fExpand);

					pTag->UpdateTypes(TRUE);

					UINT fNext = pTag->IsBroken();

					UINT uNext = pTag->GetTreeImage();

					if( uPrev - uNext ) {

						pHost->ItemUpdated(0, pTag, updateProps);
					}

					if( fPrev - fNext ) {

						pHost->ItemUpdated(0, pTag, updateRename);
					}
				}
				else {
					pVal->Recompile(fExpand);

					pTag->UpdateTypes(TRUE);
				}

				Done.Insert(pVal);
			}

			Busy.Remove(pVal);

			return TRUE;
		}

		m_pDbase->SetCircle();

		return FALSE;
	}

	return TRUE;
}

BOOL CTagManager::StepName(CString &Code) const
{
	if( !m_pTags->FindByName(Code + L"_1") ) {

		CStringArray List;

		Code.Tokenize(List, '.');

		UINT  uCount = List.GetCount();

		UINT  uAlloc = uCount * sizeof(UINT);

		PUINT pNum   = PUINT(alloca(uAlloc));

		PUINT pCopy  = PUINT(alloca(uAlloc));

		for( UINT uPart = 0; uPart < uCount; uPart++ ) {

			CString Part = List[uPart];

			UINT    uLen = Part.GetLength();

			UINT    n;

			for( n = 0;; n++ ) {

				if( !isdigit(Part[uLen-1-n]) ) {

					break;
				}
			}

			if( n ) {

				pNum[uPart] = watoi(Part.Right(n));

				List.SetAt(uPart, Part.Left(uLen-n));

				continue;
			}

			pNum[uPart] = NOTHING;
		}

		memcpy(pCopy, pNum, uAlloc);

		for( UINT uPass = 0; uPass < 2; uPass++ ) {

			for( UINT uBack = 0; uBack < uCount; uBack++ ) {

				UINT uScan = uCount - 1 - uBack;

				if( pNum[uScan] < NOTHING ) {

					pNum[uScan] += 1;

					CString Find;

					for( UINT uPart = 0; uPart < uCount; uPart++ ) {

						CString Part = List[uPart];

						if( pNum[uPart] < NOTHING ) {

							Part += CPrintf(L"%u", pNum[uPart]);
						}

						if( uPart ) {

							Find += L'.';
						}

						Find += Part;
					}

					if( m_pTags->FindByName(Find) ) {

						Code = Find;

						return TRUE;
					}

					pNum[uScan] = uPass;
				}
			}

			memcpy(pNum, pCopy, uAlloc);
		}

		return FALSE;
	}

	Code += L"_1";

	return TRUE;
}

// End of File
