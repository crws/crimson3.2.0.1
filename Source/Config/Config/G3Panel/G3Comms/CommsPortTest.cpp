
#include "Intern.hpp"

#include "CommsPortTest.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortTest, CCommsPort);

// Constructor

CCommsPortTest::CCommsPortTest(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding = bindTest;
	}

// Attributes

BOOL CCommsPortTest::IsRemotable(void) const
{
	return TRUE;
	}

// Download Support

BOOL CCommsPortTest::MakeInitData(CInitData &Init)
{
	Init.AddByte(13);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CCommsPortTest::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_SetName(L"Test Port");
	}

// End of File
