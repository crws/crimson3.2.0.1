
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_HttpServerConnectionOptions_HPP

#define INCLUDE_HttpServerConnectionOptions_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpConnectionOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Connection Options
//

class CHttpServerConnectionOptions : public CHttpConnectionOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHttpServerConnectionOptions(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		BOOL    m_CompReply;
		UINT    m_MaxKeep;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
