
#include "Intern.hpp"

#include "UITextCertRef.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

#include <G3DevCon.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Certifcate Reference
//

// Dynamic Class

AfxImplementDynamicClass(CUITextCertRef, CUITextEnum);

// Constructor

CUITextCertRef::CUITextCertRef(void)
{
	m_uFlags |= textReload | textRefresh;
}

// Overridables

void CUITextCertRef::OnBind(void)
{
	CUITextElement::OnBind();

	AddData();
}

BOOL CUITextCertRef::OnEnumValues(CStringArray &List)
{
	m_Enum.Empty();

	AddData();

	return CUITextEnum::OnEnumValues(List);
}

// Implementation

void CUITextCertRef::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
}

void CUITextCertRef::AddData(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CDevCon      *pDevCon = pSystem->m_pDevCon;

	CUIntArray   Data;

	CStringArray Text;

	switch( m_UIData.m_Format[0] ) {

		case 'C':
		{
			AddData(0, L"None");

			m_fIdent = TRUE;
		}
		break;

		case 'T':
		{
			AddData(0, L"None");

			AddData(1, L"Trusted Roots");

			m_fIdent = FALSE;
		}
		break;

		case 'S':
		{
			AddData(0, L"Default Identity");

			m_fIdent = TRUE;
		}
		break;
	}

	pDevCon->GetCertList(Data, Text, m_fIdent);

	for( UINT n = 0; n < Text.GetCount(); n++ ) {

		AddData(Data[n], Text[n]);
	}
}

// End of File
