
#include "Intern.hpp"

#include "CommsManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsDevice.hpp"
#include "CommsMapData.hpp"
#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "CommsSysBlock.hpp"
#include "CommsSysBlockList.hpp"
#include "Connectors.hpp"
#include "EthernetItem.hpp"
#include "ExpansionItem.hpp"
#include "OptionCardItem.hpp"
#include "RackItem.hpp"
#include "Services.hpp"
#include "USBHostItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications Manager
//

// Dynamic Class

AfxImplementDynamicClass(CCommsManager, CCodedHost);

// Constructor

CCommsManager::CCommsManager(void)
{
	m_NoComms     = 0;

	m_MaxPort     = 0;

	m_MaxDevice   = 0;

	m_MaxBlock    = 0;

	m_StrPad      = 0;

	m_pOnEarly    = NULL;

	m_pOnStart    = NULL;

	m_pOnApply    = NULL;

	m_pOnSecond   = NULL;

	m_LedAlarm    = 0;

	m_LedOrb      = 0;

	m_LedHome     = 0;

	m_pOption0    = New COptionCardItem(0);

	m_pOption1    = New COptionCardItem(1);

	m_pEthernet   = New CEthernetItem;

	m_pPorts      = New CCommsPortList;

	m_pServices   = New CServices;

	m_pConnectors = New CConnectors;

	m_pUSBHost    = New CUSBHostItem;

	m_pRack       = New CRackItem;

	m_pExpansion  = New CExpansionItem;

	m_Handle      = HANDLE_NONE;
}

// Item Lookup

CCommsPort * CCommsManager::FindPort(UINT uPort) const
{
	CCommsPortList *pList;

	for( UINT s = 0; GetPortList(pList, s); s++ ) {

		CCommsPort *pPort;

		if( pPort = pList->FindPort(uPort) ) {

			return pPort;
		}
	}

	return NULL;
}

CCommsPort * CCommsManager::FindPort(CString Name) const
{
	CCommsPortList *pList;

	for( UINT s = 0; GetPortList(pList, s); s++ ) {

		CCommsPort *pPort;

		if( pPort = pList->FindPort(Name) ) {

			return pPort;
		}
	}

	return NULL;
}

CCommsDevice * CCommsManager::FindDevice(UINT uDevice) const
{
	CCommsPortList *pList;

	for( UINT s = 0; GetPortList(pList, s); s++ ) {

		CCommsDevice *pDevice;

		if( pDevice = pList->FindDevice(uDevice) ) {

			return pDevice;
		}
	}

	return NULL;
}

CCommsDevice * CCommsManager::FindDevice(CString Name) const
{
	CCommsPortList *pList;

	for( UINT s = 0; GetPortList(pList, s); s++ ) {

		CCommsDevice *pDevice;

		if( pDevice = pList->FindDevice(Name) ) {

			return pDevice;
		}
	}

	return NULL;
}

CCommsSysBlock * CCommsManager::FindBlock(UINT uBlock) const
{
	CCommsPortList *pList;

	for( UINT s = 0; GetPortList(pList, s); s++ ) {

		CCommsSysBlock *pBlock;

		if( pBlock = pList->FindBlock(uBlock) ) {

			return pBlock;
		}
	}

	return NULL;
}

// Port List Access

BOOL CCommsManager::GetPortList(CCommsPortList * &pList, UINT uIndex) const
{
	switch( uIndex ) {

		case 0:
			pList = m_pPorts;

			return TRUE;

		case 1:
			pList = m_pEthernet->m_pPorts;

			return TRUE;

		case 2:
			pList = m_pOption0->m_pPorts;

			return TRUE;

		case 3:
			pList = m_pOption1->m_pPorts;

			return TRUE;

		case 4:
			pList = m_pRack->m_pPorts;

			return TRUE;

		default:
			return m_pExpansion->GetPortList(pList, uIndex - 5);
	}

	pList = NULL;

	return FALSE;
}

// Attributes

DWORD CCommsManager::GetRefAddress(DWORD ID) const
{
	CDataRef const & Ref    = (CDataRef &) ID;

	CCommsSysBlock * pBlock = FindBlock(Ref.b.m_Block);

	if( pBlock ) {

		return pBlock->GetRefAddress(Ref);
	}

	AfxAssert(FALSE);

	return 0;
}

UINT CCommsManager::GetRefDevice(DWORD ID) const
{
	CDataRef const & Ref    = (CDataRef &) ID;

	CCommsSysBlock * pBlock = FindBlock(Ref.b.m_Block);

	if( pBlock ) {

		CCommsDevice *pDevice = pBlock->GetParentDevice();

		return pDevice->m_Number;
	}

	AfxAssert(FALSE);

	return NOTHING;
}

BOOL CCommsManager::CanStepFragment(CString Code) const
{
	if( IsCommsRef(Code) ) {

		CError  Error   = CError(FALSE);

		CString Name    = Code.Mid(1, Code.GetLength() - 2);

		UINT    uDevice = FindDevice(&Error, Name);

		if( uDevice < NOTHING ) {

			CCommsDevice *pDevice = FindDevice(uDevice);

			CItem        *pConfig = pDevice->GetConfig();

			ICommsDriver *pDriver = pDevice->GetDriver();

			CAddress Addr;

			if( pDriver->ParseAddress(Error, Addr, pConfig, Name) ) {

				if( Addr.a.m_Table < addrNamed ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CCommsManager::IsFragmentBit(CString Code) const
{
	if( Code[0] == '[' ) {

		CError  Error   = CError(FALSE);

		CString Name    = Code.Mid(1, Code.GetLength() - 2);

		UINT    uDevice = FindDevice(&Error, Name);

		if( uDevice < NOTHING ) {

			CCommsDevice *pDevice = FindDevice(uDevice);

			CItem        *pConfig = pDevice->GetConfig();

			ICommsDriver *pDriver = pDevice->GetDriver();

			CAddress Addr;

			if( pDriver->ParseAddress(Error, Addr, pConfig, Name) ) {

				if( Addr.a.m_Type == addrBitAsBit ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

UINT CCommsManager::GetMaxPortNumber(void) const
{
	return m_MaxPort;
}

UINT CCommsManager::GetMaxDeviceNumber(void) const
{
	return m_MaxDevice;
}

UINT CCommsManager::GetMaxBlockNumber(void) const
{
	return m_MaxBlock;
}

// Operations

void CCommsManager::Validate(BOOL fExpand)
{
	m_pOption0->Validate(fExpand);

	m_pOption1->Validate(fExpand);

	m_pEthernet->Validate(fExpand);

	m_pPorts->Validate(fExpand);

	m_pRack->Validate(fExpand);

	m_pExpansion->Validate(fExpand);
}

void CCommsManager::ClearSysBlocks(void)
{
	m_pOption0->ClearSysBlocks();

	m_pOption1->ClearSysBlocks();

	m_pEthernet->ClearSysBlocks();

	m_pPorts->ClearSysBlocks();

	m_pRack->ClearSysBlocks();

	m_pExpansion->ClearSysBlocks();

	m_MaxBlock = 0;
}

void CCommsManager::NotifyInit(void)
{
	m_pOption0->NotifyInit();

	m_pOption1->NotifyInit();

	m_pEthernet->NotifyInit();

	m_pPorts->NotifyInit();

	m_pRack->NotifyInit();

	m_pExpansion->NotifyInit();
}

void CCommsManager::CheckMapBlocks(void)
{
	m_pOption0->CheckMapBlocks();

	m_pOption1->CheckMapBlocks();

	m_pEthernet->CheckMapBlocks();

	m_pPorts->CheckMapBlocks();

	m_pRack->CheckMapBlocks();

	m_pExpansion->CheckMapBlocks();
}

BOOL CCommsManager::ResolveMapData(CString &Text, CCommsMapData const *pMap)
{
	CAddress Addr = pMap->m_Addr;

	if( Addr.m_Ref ) {

		CItem        *pConfig = pMap->m_pDevice->GetConfig();

		ICommsDriver *pDriver = pMap->m_pDevice->GetDriver();

		if( pMap->m_fPart ) {

			if( !pDriver->SelectAddress(*afxMainWnd, Addr, pConfig, TRUE) ) {

				return FALSE;
			}
		}

		pDriver->ExpandAddress(Text, pConfig, Addr);

		Text.Printf(L"[%s.%s]", pMap->m_pDevice->GetName(), Text);

		return TRUE;
	}

	Text.Empty();

	return TRUE;
}

BOOL CCommsManager::ResolveAddress(DWORD &ID, UINT uDevice, CAddress Addr)
{
	return ResolveAddress(ID, uDevice, Addr, 1);
}

BOOL CCommsManager::ResolveAddress(DWORD &ID, UINT uDevice, CAddress Addr, INT nCount)
{
	CCommsDevice *pDevice = FindDevice(uDevice);

	if( pDevice ) {

		UINT Flags = pDevice->GetDriver()->GetFlags();

		UINT uStep = (Flags & dflagBigBlocks) ? 2 : 1;

		ID = 0;

		for( UINT uPass = 0; uPass <= 2; uPass += uStep ) {

			CCommsSysBlockList *pList   = pDevice->m_pSysBlocks;

			CItem		   *pConfig = pDevice->m_pConfig;

			ICommsDriver	   *pDriver = pDevice->GetDriver();

			for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

				CCommsSysBlock *pBlock = pList->GetItem(n);

				if( pBlock->AcceptAddress(Addr, nCount, ID, uPass, IsNamedArray(pDriver)) ) {

					pDriver->NotifyExtent(Addr, nCount, pConfig);

					return TRUE;
				}
			}

			if( uPass == uStep ) {

				if( m_MaxBlock < 1000 ) {

					CCommsSysBlock *pBlock = New CCommsSysBlock;

					pDevice->m_pSysBlocks->AppendItem(pBlock);

					if( pDriver->IsAddrNamed(pConfig, Addr) ) {

						pBlock->m_Named = 1;
					}

					pBlock->AcceptAddress(Addr, nCount, ID, uPass, IsNamedArray(pDriver));

					pDriver->NotifyExtent(Addr, nCount, pConfig);

					return TRUE;
				}
			}
		}
	}

	ID = 0;

	return FALSE;
}

BOOL CCommsManager::FindDirect(CError *pError, CString Name, DWORD &ID, CTypeDef &Type)
{
	return FindDirect(pError, Name, 1, ID, Type);
}

BOOL CCommsManager::FindDirect(CError *pError, CString Name, INT nCount, DWORD &ID, CTypeDef &Type)
{
	UINT uDevice = FindDevice(pError, Name);

	if( uDevice < NOTHING ) {

		CCommsDevice *pDevice = FindDevice(uDevice);

		CItem        *pConfig = pDevice->GetConfig();

		ICommsDriver *pDriver = pDevice->GetDriver();

		CAddress Addr;

		if( pDriver->ParseAddress(*pError, Addr, pConfig, Name) ) {

			if( ResolveAddress(ID, uDevice, Addr, nCount) ) {

				CDataRef       &Ref    = (CDataRef &) ID;

				CCommsSysBlock *pBlock = FindBlock(Ref.b.m_Block);

				pBlock->GetBlockType(Type);

				if( IsNamedArray(pDriver) ) {

					Type.m_Flags |= flagCommsTab;
				}

				if( pDriver->IsReadOnly(pConfig, Addr) ) {

					Type.m_Flags &= ~flagWritable;
				}

				return TRUE;
			}
			else {
				pError->Set(IDS_COMMS_CAPACITY);

				return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsManager::NameDirect(DWORD ID, CString &Name)
{
	CDataRef const & Ref = (CDataRef &) ID;

	if( !Ref.b.m_IsTag ) {

		CCommsSysBlock *pBlock = FindBlock(Ref.b.m_Block);

		if( pBlock ) {

			if( pBlock && pBlock->m_Size ) {

				CCommsDevice *pDevice = pBlock->GetParentDevice();

				CItem        *pConfig = pDevice->GetConfig();

				ICommsDriver *pDriver = pDevice->GetDriver();

				DWORD         Addr    = pBlock->GetRefAddress(Ref);

				if( Addr ) {

					if( pDriver->ExpandAddress(Name, pConfig, (CAddress &) Addr) ) {

						Name = pDevice->m_Name + L"." + Name;

						return TRUE;
					}
				}
			}
		}
	}

	return FALSE;
}

BOOL CCommsManager::StepFragment(CString &Code, UINT uStep)
{
	if( IsCommsRef(Code) ) {

		CError  Error   = CError(FALSE);

		CString Name    = Code.Mid(1, Code.GetLength() - 2);

		UINT    uDevice = FindDevice(&Error, Name);

		if( uDevice < NOTHING ) {

			CCommsDevice *pDevice = FindDevice(uDevice);

			CItem        *pConfig = pDevice->GetConfig();

			ICommsDriver *pDriver = pDevice->GetDriver();

			CAddress Addr;

			if( pDriver->ParseAddress(Error, Addr, pConfig, Name) ) {

				if( Addr.a.m_Table < addrNamed ) {

					Addr.a.m_Offset += uStep * C3GetTypeScale(Addr.a.m_Type);

					if( pDriver->ExpandAddress(Name, pConfig, Addr) ) {

						Code.Format(L"[%1.%2]", pDevice->GetName(), Name);

						return TRUE;
					}
				}
			}
		}
	}

	return FALSE;
}

UINT CCommsManager::AllocPortNumber(void)
{
	for( UINT n = 1;; n++ ) {

		if( !FindPort(n) ) {

			MakeMax(m_MaxPort, n);

			return n;
		}
	}
}

UINT CCommsManager::AllocDeviceNumber(void)
{
	for( UINT n = 1;; n++ ) {

		if( !FindDevice(n) ) {

			MakeMax(m_MaxDevice, n);

			return n;
		}
	}
}

UINT CCommsManager::AllocBlockNumber(void)
{
	for( UINT n = 1;; n++ ) {

		if( !FindBlock(n) ) {

			MakeMax(m_MaxBlock, n);

			return n;
		}
	}
}

void CCommsManager::RegisterPortNumber(UINT n)
{
	MakeMax(m_MaxPort, n);
}

void CCommsManager::RegisterDeviceNumber(UINT n)
{
	MakeMax(m_MaxDevice, n);
}

void CCommsManager::RegisterBlockNumber(UINT n)
{
	MakeMax(m_MaxBlock, n);
}

// Property Save Filter

BOOL CCommsManager::SaveProp(CString const &Tag) const
{
	if( Tag == L"Connectors" ) {

		return FALSE;
	}

	return CCodedHost::SaveProp(Tag);
}

// Download Support

BOOL CCommsManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_COMMS);

	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_StrPad));

	Init.AddItem(itemVirtual, m_pOnEarly);
	Init.AddItem(itemVirtual, m_pOnStart);
	Init.AddItem(itemVirtual, m_pOnApply);
	Init.AddItem(itemVirtual, m_pOnSecond);

	Init.AddWord(WORD(m_MaxPort));
	Init.AddWord(WORD(m_MaxDevice));
	Init.AddWord(WORD(m_MaxBlock));

	Init.AddItem(itemSimple, m_pEthernet);
	Init.AddItem(itemSimple, m_pPorts);
	Init.AddItem(itemSimple, m_pServices);
	Init.AddItem(itemSimple, m_pRack);

	Init.AddItem(itemSimple, m_pUSBHost);

	if( m_pDbase->GetSoftwareGroup() == SW_GROUP_2 ) {

		Init.AddByte(BYTE(1));

		Init.AddByte(BYTE(m_LedAlarm));
		Init.AddByte(BYTE(m_LedOrb));
		Init.AddByte(BYTE(m_LedHome));
	}
	else {
		Init.AddByte(BYTE(0));
	}

	return TRUE;
}

// UI Creation

BOOL CCommsManager::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() == SW_GROUP_2 ) {

		pList->Append(New CUIStdPage(CString(IDS_GLOBAL), AfxThisClass(), 2));

		pList->Append(New CUIStdPage(CString(IDS_ICONS), AfxThisClass(), 3));

		return TRUE;
	}

	pList->Append(New CUIStdPage(AfxThisClass(), 2));

	return FALSE;
}

// UI Creation

CViewWnd * CCommsManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CCommsTreeWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	if( uType == viewResource ) {

		CLASS Class = AfxNamedClass(L"CDeviceResTreeWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	return CUIItem::CreateView(uType);
}

// UI Update

void CCommsManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "NoComms" ) {

		if( m_NoComms ) {

			if( !pHost->InReplay() ) {

				CString Text = IDS("Downloading a database with communications disabled will result in Data Tags being intially set equal to their simulated values rather than actual values.");

				pHost->GetWindow().Information(Text, L"CommsDisabled");
			}
		}
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CCommsManager::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(2) == "On" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
	}

	return CCodedHost::GetTypeData(Tag, Type);
}

// Item Naming

CString CCommsManager::GetHumanName(void) const
{
	return CString(IDS_COMMS_CAPTION);
}

// Meta Data Creation

void CCommsManager::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddInteger(NoComms);
	Meta_AddInteger(MaxPort);
	Meta_AddInteger(MaxDevice);
	Meta_AddInteger(MaxBlock);
	Meta_AddInteger(StrPad);
	Meta_AddVirtual(OnEarly);
	Meta_AddVirtual(OnStart);
	Meta_AddVirtual(OnApply);
	Meta_AddVirtual(OnSecond);
	Meta_AddCollect(Ports);
	Meta_AddObject(Option0);
	Meta_AddObject(Option1);
	Meta_AddObject(Ethernet);
	Meta_AddObject(Services);
	Meta_AddObject(Connectors);
	Meta_AddObject(USBHost);
	Meta_AddCollect(Rack);
	Meta_AddObject(Expansion);
	Meta_AddInteger(LedAlarm);
	Meta_AddInteger(LedOrb);
	Meta_AddInteger(LedHome);

	Meta_SetName((IDS_COMMS_MANAGER));
}

// Implementation

UINT CCommsManager::FindDevice(CError *pError, CString &Name) const
{
	UINT uPos = Name.Find('.');

	if( uPos < NOTHING ) {

		CString       DevName = Name.Left(uPos);

		CCommsDevice *pDevice = FindDevice(DevName);

		if( pDevice ) {

			Name = Name.Mid(uPos + 1);

			return pDevice->m_Number;
		}

		if( pError ) {

			pError->Set(CFormat(IDS_COMMS_BAD_DEVICE, DevName));
		}

		return NOTHING;
	}

	for( UINT d = 1; d <= m_MaxDevice; d++ ) {

		CCommsDevice *pDevice = FindDevice(d);

		if( pDevice ) {

			ICommsDriver *pDriver = pDevice->GetDriver();

			if( pDriver && pDriver->GetID() == 0xFE01 ) {

				continue;
			}

			return d;
		}
	}

	if( pError ) {

		pError->Set(IDS_COMMS_NO_DEVICES);
	}

	return NOTHING;
}

BOOL CCommsManager::IsCommsRef(CString const &Code) const
{
	if( Code[0] == '[' ) {

		UINT uLen = Code.GetLength();

		if( Code[uLen - 1] == ']' ) {

			UINT uPLC = 1;

			for( UINT n = 1; n < uLen - 1; n++ ) {

				if( Code[n] == '[' ) {

					uPLC++;
				}

				if( Code[n] == ']' ) {

					uPLC--;
				}

				if( !uPLC ) {

					return FALSE;
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsManager::IsNamedArray(ICommsDriver *pDriver) const
{
	switch( pDriver->GetID() ) {

		case 0x4003: // CANopen SDO Master

			return TRUE;
	}

	return FALSE;
}

// End of File
