
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_HttpServerOptions_HPP

#define INCLUDE_HttpServerOptions_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpServerConnectionOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server  Options
//

class CHttpServerOptions : public CHttpServerConnectionOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHttpServerOptions(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Server Auth Methods
		enum {
			methodAnon = 0,
			methodForm = 1,
			methodHttp = 2,
			};

		// HTTP Auth Methods
		enum {

			httpBasic  = 1,
			httpDigest = 2,
			httpNTLM   = 4,
			};

		// Data Members
		UINT	m_AuthMethod;
		UINT	m_HttpMethod;
		CString m_Realm;
		UINT	m_HttpRedirect;
		UINT    m_SockCount;
		UINT    m_IdleTimeout;
		UINT    m_InitTimeout;
		UINT    m_SessTimeout;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
