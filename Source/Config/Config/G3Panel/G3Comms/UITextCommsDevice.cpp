
#include "Intern.hpp"

#include "UITextCommsDevice.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsDeviceList.hpp"
#include "CommsManager.hpp"
#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "CommsSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Communications Device
//

// Dynamic Class

AfxImplementDynamicClass(CUITextCommsDevice, CUITextEnum);

// Constructor

CUITextCommsDevice::CUITextCommsDevice(void)
{
	}

// Overridables

void CUITextCommsDevice::OnBind(void)
{
	CUITextEnum::OnBind();

	m_Type = driverCamera;

	AddData(NOTHING, L"None");

	AddData();
	}

// Implementation

void CUITextCommsDevice::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

void CUITextCommsDevice::AddData(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CCommsManager *pComms = pSystem->m_pComms;

	CCommsPortList *pPorts;

	for( UINT s = 0; pComms->GetPortList(pPorts, s); s ++ ) {

		for( INDEX i = pPorts->GetHead(); !pPorts->Failed(i); pPorts->GetNext(i) ) {

			CCommsPort    *pPort = pPorts->GetItem(i);

			ICommsDriver *pDriver = pPort->GetDriver();

			if( pDriver && pDriver->GetType() == m_Type ) {

				CCommsDeviceList *pDevices = pPort->m_pDevices;

				for( INDEX d = pDevices->GetHead(); !pDevices->Failed(d); pDevices->GetNext(d) ) {

					CCommsDevice  *pDevice = pDevices->GetItem(d);

					UINT  uIndex = pDevice->m_Number;

					CString Name = pDevice->m_Name;

					AddData(uIndex, Name);
					}
				}
			}
		}
	}

// End of File
