
#include "Intern.hpp"

#include "ServiceCloudCumulocity.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "TagSet.hpp"
#include "HttpClientConnectionOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cumulocity Cloud Service
//

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudCumulocity, CServiceItem);

// Constructor

CServiceCloudCumulocity::CServiceCloudCumulocity(void)
{
	m_pEnable   = NULL;

	m_Service   = 0;

	m_Host	    = L"tenant.cumulocity.com";

	m_Node	    = L"Node";

	m_Device    = L"";

	m_AutoCred  = 0;

	m_User	    = L"user@domain.com";

	m_Pass	    = L"password";

	m_pCredTag  = NULL;

	m_Scan	    = 5;

	m_Batch	    = 12;

	m_Close     = 0;

	m_pSet      = New CTagSet;

	InitOptions();
	}

// UI Management

CViewWnd * CServiceCloudCumulocity::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return CreateItemView(FALSE);
		}

	return NULL;
	}

// UI Update

void CServiceCloudCumulocity::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			BOOL fEnable = CheckEnable(m_pEnable,  L"==", 1, TRUE);

			if( fEnable ) {

				pHost->EnableUI(m_pOpts, TRUE);

				m_pOpts->OnUIChange(pHost, m_pOpts, L"");
				}
			else
				pHost->EnableUI(m_pOpts, FALSE);

			DoEnables(pHost);
			}

		if( Tag == "AutoCred" ) {

			DoEnables(pHost);
			}
		}

	if( pItem == m_pOpts ) {

		if( FALSE ) {

			DoEnables(pHost);
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Initial Values

void CServiceCloudCumulocity::SetInitValues(void)
{
	SetInitial(L"Enable", m_pEnable, 0);
	}

// Type Access

BOOL CServiceCloudCumulocity::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"CredTag" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = flagWritable;

		return TRUE;
		}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Attributes

BOOL CServiceCloudCumulocity::IsEnabled(void) const
{
	return FALSE;
	}

UINT CServiceCloudCumulocity::GetTreeImage(void) const
{
	return IDI_CLOUD_ENABLED;
	}

// Download Support

BOOL CServiceCloudCumulocity::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(servCloudCumulocity));

	CServiceItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddByte(BYTE(m_Service));

	Init.AddText(m_Host);

	Init.AddText(m_Device);

	Init.AddText(m_Node);

	if( m_AutoCred ) {

		Init.AddByte(BYTE(1));

		Init.AddItem(itemVirtual, m_pCredTag);
		}
	else {
		Init.AddByte(BYTE(0));

		Init.AddText(m_User);

		Init.AddText(m_Pass);
		}

	Init.AddWord(WORD(m_Scan));

	Init.AddWord(WORD(m_Batch));

	Init.AddByte(BYTE(m_Close));

	Init.AddItem(itemSimple, m_pOpts);

	Init.AddItem(itemSimple, m_pSet);

	return TRUE;
	}

// Meta Data Creation

void CServiceCloudCumulocity::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddInteger(Service);
	Meta_AddString (Host);
	Meta_AddString (Device);
	Meta_AddString (Node);
	Meta_AddInteger(AutoCred);
	Meta_AddString (User);
	Meta_AddString (Pass);
	Meta_AddVirtual(CredTag);
	Meta_AddInteger(Scan);
	Meta_AddInteger(Batch);
	Meta_AddInteger(Close);
	Meta_AddObject (Set);
	Meta_AddObject (Opts);

	Meta_SetName((IDS_CUMULOCITY_REST));
	}

// Implementation

void CServiceCloudCumulocity::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable,  L"==", 1, TRUE);

	pHost->EnableUI(this, "Service",  fEnable);
	pHost->EnableUI(this, "Host",     fEnable);
	pHost->EnableUI(this, "Device",   fEnable);
	pHost->EnableUI(this, "Node",     fEnable);
	pHost->EnableUI(this, "AutoCred", fEnable);
	pHost->EnableUI(this, "User",	  fEnable && !m_AutoCred);
	pHost->EnableUI(this, "Pass",     fEnable && !m_AutoCred);
	pHost->EnableUI(this, "CredTag",  fEnable &&  m_AutoCred);
	pHost->EnableUI(this, "Scan",     fEnable);
	pHost->EnableUI(this, "Batch",    fEnable);
	pHost->EnableUI(this, "Close",    fEnable);
	pHost->EnableUI(this, "Set",      fEnable);
	}

void CServiceCloudCumulocity::InitOptions(void)
{
	m_pOpts	= New COpts;

	m_pOpts->m_Tls      = 1;

	m_pOpts->m_Port     = 443;

	m_pOpts->m_Check    = 3;

	m_pOpts->m_Advanced = 1;
	}

// End of File
