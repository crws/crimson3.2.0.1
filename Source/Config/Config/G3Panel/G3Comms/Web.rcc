
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Web Server Tree Toolbar
//

WebServerNavTreeTool MENU
BEGIN
	MENUITEM "40000009",			IDM_ITEM_NEW
	MENUITEM "10000008",			IDM_ITEM_DELETE
	MENUITEM "1000000C",			IDM_ITEM_FIND
END

//////////////////////////////////////////////////////////////////////////
//
// Web Context Menu (Hit)
//

WebNavTreeCtxMenu MENU
BEGIN
	POPUP "CCM"
	BEGIN
		MENUITEM "Expand",		IDM_ITEM_EXPAND
		MENUITEM SEPARATOR
		MENUITEM "New Page",		IDM_ITEM_NEW
		MENUITEM SEPARATOR
		MENUITEM "Sync Panes",		IDM_ITEM_SYNC
		MENUITEM SEPARATOR
		MENUITEM "Cut",			IDM_EDIT_CUT
		MENUITEM "Copy",		IDM_EDIT_COPY
		MENUITEM "Paste",		IDM_EDIT_PASTE
		MENUITEM "Delete",		IDM_EDIT_DELETE
		MENUITEM SEPARATOR
		MENUITEM "Rename",		IDM_ITEM_RENAME
		MENUITEM SEPARATOR
		MENUITEM "Sort A-Z",		IDM_ITEM_SORT_UP
		MENUITEM "Sort Z-A",		IDM_ITEM_SORT_DOWN
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Web Context Menu (Miss)
//

WebNavTreeMissCtxMenu MENU
BEGIN
	POPUP "CCM"
	BEGIN
		MENUITEM "New Page",		IDM_ITEM_NEW
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Item Schemas
//

CWebServer_Schema RCDATA
BEGIN
	L"Enable,Enable Web Server,,OnOff/Check,Disabled|Enabled,"
	L"Check this box to enable the web server."
	L"\0"

	L"Title,Main Title,,IntlString/IntlString,20|Enhanced Web Server,"
	L"Specify the caption to be displayed in the browser title bar."
	L"\0"

	L"Header,Main Header,,IntlString/IntlString,20|Welcome to the Enhanced Web Server!,"
	L"Specify the title to be displayed above the main menu."
	L"\0"

	L"Home,Home Label,,IntlString/IntlString,20|C3.2 Web+,"
	L"Specify the home label for the web site's menu bar."
	L"\0"

	L"CustHome,Custom Home Page,,Enum/DropDown,Disabled|Enabled,"
	L"Enables a user defined start page. This custom web page should be "
	L"named default.htm and reside in the WEB directory on the memory card."
	L"\0"

	L"CustIcon,Custom Site Icon,,Enum/DropDown,Disabled|Enabled,"
	L"Enables a user defined favorites icon. This custom icon should be "
	L"named favicon.ico and reside in the WEB directory on the memory card. "
	L"Note that browsers are very aggressive about caching site icons and so "
	L"it may take some time for any changes to become visible."
	L"\0"

	L"CustLogon,Custom Logon Page,,Enum/DropDown,Disabled|Enabled,"
	L"Enables a user defined logon page. This custom web page should be "
	L"named logon.htm and reside in the WEB directory on the memory card."
	L"\0"

	L"CustCss,Custom Style Sheet,,Enum/DropDown,Disabled|Enabled,"
	L"Enables a user defined style sheet. This custom style sheet should be "
	L"named custom.css and reside in the WEB directory on the memory card."
	L"\0"

	L"CustJs,Custom JavaScript,,Enum/DropDown,Disabled|Enabled,"
	L"Enables a user defined script file. This custom script file should be "
	L"named custom.js and reside in the WEB directory on the memory card."
	L"\0"

	L"RemoteView,Remote Viewing,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this feature to allow remote viewing of the device's screen."
	L"\0"

	L"RemoteZoom,Display Mode,,Enum/DropDown,Regular|Full Window|Bare Display Centered|Bare Display Scaled,"
	L"Select whether the remote rendering of the display should be included in "
	L"a regular webpage, whether it should be scaled to fill the browser window, "
	L"or whether just the display itself should be shown with no frame."
	L"\0"

	L"RemoteFact,Display Scale,,Enum/DropDown,Default=2|Force 1:1=1,"
	L"Select whether the display page should be rendered at its default scale "
	L"factor or whether it should be forced to one-to-one. This setting only "
	L"has an effect on units with QVGA displays where the default scale is 2:1."
	L"\0"

	L"RemoteBits,Display Depth,,Enum/DropDown,8 bits and 256 colors=8|16 bits and 32768 colors=16|24 bits and 16 million colors=24,"
	L"Select the number of bits per pixel to be used when transferring data to "
	L"to web-browser. The higher the number of bits, the better the display quality "
	L"but the higher the amount of data that has to be transferred."
	L"\0"

	L"RemoteCtrl,Remote Control,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this feature to allow remote control of the device."
	L"\0"

	L"RemoteSec,Access Control,,AccessMask/Pick,-P,"
	L"Edit the security descriptor for the remote view feature. Users must always "
	L"have the Web Server access right to access this feature, irrespective of the "
	L"configuration of this setting."
	L"\0"

	L"LogsView,Data Log Access,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this feature to allow remote access to log files."
	L"\0"

	L"LogsBatches,Show Batches,,Enum/DropDown,No|Yes,"
	L"Enable this feature to show any log batches that have been created."
	L"\0"

	L"LogsSec,Access Control,,AccessMask/Pick,-P,"
	L"Edit the security descriptor for data log access. Users must always have "
	L"the Web Server access right to access this feature, irrespective of the "
	L"configuration of this setting."
	L"\0"

	L"UserSite,Custom Site,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this feature to allow remote viewing of a custom "
	L"web site placed on the Memory Card."
	L"\0"

	L"UserMenu,Include in Menu,,Enum/DropDown,No|Yes,"
	L"Show a link to the custom website in the main menu."
	L"\0"

	L"UserRoot,Root Redirect,,Enum/DropDown,Disabled|Enabled,"
	L"Enables unknown requests to the root directory of the web server "
	L"to be redirected to the custom web pages on the memory card."
	L"\0"

	L"UserDelay,Maximum Wait,,Integer/EditBox,|0|secs|1|60,"
	L"Indicate the maximum time to wait when reading data on demand for custom "
	L"web pages. After this time has passed, the page will be sent with the "
	L"non-available data items shown as dashes."
	L"\0"

	L"UserSec,Access Control,,AccessMask/Pick,-P,"
	L"Edit the security descriptor for the custom web site. Users must always "
	L"have the Web Server access right to access this feature, irrespective of "
	L"the configuration of this setting."
	L"\0"

	L"SystemPages,System Pages,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this feature to provide access to system diagnostic pages."
	L"\0"

	L"SystemConsole,Debug Console,,Enum/DropDown,Disabled|Enabled Display Only|Enabled with Commands,"
	L"Indicate whether the web debug console should be enabled, and "
	L"whether the user should be able to enter debugging commands."
	L"\0"

	L"SystemCapture,Packet Capture,,Enum/DropDown,Disabled|Enabled,"
	L"Indicate whether packet capture via the web interface should be supported."
	L"\0"

	L"SystemJump,System Navigation,,Enum/DropDown,Disabled|Enabled,"
	L"Indicate whether a link should be provided to the system web server."
	L"\0"

	L"SystemSec,Access Control,,AccessMask/Pick,-P,"
	L"Edit the security descriptor for the system diagnostics pages. Users must "
	L"always have the Web Server access right to access this feature, irrespective "
	L"of the configuration of this setting."
	L"\0"

	L"Source,Source,,Enum/DropDown,Web Server|Security Manager,"
	L"Indicate whether authentication credentials should be specified below "
	L"or whether the Security Manager will be used to validate user information."
	L"\0"

	L"Local,Use Local Login,,Enum/DropDown,No|Yes,"
	L"Indicate whether a local login is required. If set to No, the credentials used "
	L"to log into the web server remotely will also be used to determine permissions. If set "
	L"to Yes, the remote credentials will only be used for determining access to the web server, "
	L"and the user will have to log-on again to determine other permissions. "
	L"\0"

	L"User,Username,,ExprString/ExprEditBox,31|None,"
	L"Provide the username required for logging-on to the web server."
	L"\0"

	L"Real,Real Name,,ExprString/ExprEditBox,31|None,"
	L"Provide the real or display name of the web server user."
	L"\0"

	L"Pass,Password,,ExprString/ExprEditBox,*31|None,"
	L"Provide the password required for logging-on to the web server."
	L"\0"

	L"Restrict,Server Access,,Enum/DropDown,Do Not Restrict|Restrict Control and Edits|Restrict All Access,"
	L"Indicate what IP-based restrictions are to be applied to the web server."
	L"\0"

	L"SecMask,Permitted IP Mask,,IPAddress/IPAddress,,"
	L"Specify the mask to be used to filter incoming IP addresses."
	L"\0"

	L"SecAddr,Permitted IP Data,,IPAddress/IPAddress,,"
	L"Specify the data to be used to filter incoming IP addresses."
	L"\0"

	L"Port,Connect via Port,,Coded/Expression,|Default,"
	L"Specify the TCP port on which the web server will listen. This "
	L"expression can be used to override the static value specified "
	L"on the first page."
	L"\0"
	
	L"SameOrigin,X-Frame-Options,,Enum/DropDown,Disabled|SAMEORIGIN,"
	L""
	L"\0"

	L"\0"
END

CWebPage_Schema RCDATA
BEGIN
	L"Title,Title,,IntlString/IntlString,32|Web Page,"
	L"Specify a title for this web page."
	L"\0"

	L"Refresh,Refresh,,Enum/DropDown,Disabled|Enabled,"
	L"Specify whether the page should automatically refreshed via Ajax."
	L"\0"

	L"Hide,Hide Page,,Enum/DropDown,No|Yes,"
	L"Specify whether the page should be hidden from the automatic menus. This "
	L"option is typically used for pages that are employed as Ajax sources for "
	L"other pages in a custom website."
	L"\0"

	L"Manual,Prefetch Data,,Enum/DropDown,Yes|No,"
	L"Indicate whether Crimson should keep the selected data items on the comms "
	L"scan so that the data is immediately available when the page is accessed. If "
	L"you select No, Crimson will read the data on demand, waiting for up to the "
	L"maximum period specified below before sending the page. Disabling prefetch "
	L"can produce poor web server performance in multi-client applications."
	L"\0"

	L"Delay,Maximum Wait,,Integer/EditBox,|0|secs|1|60,"
	L"Indicate the maximum time to wait when reading data on-demand. After "
	L"this time has passed, the page will be sent with the non-available data "
	L"items shown as dashes."
	L"\0"

	L"Edit,Allow Editing,,Enum/DropDown,No|Yes,"
	L"Indicate whether tags can be edited via this web page."
	L"\0"

	L"Colors,Use Colors,,Enum/DropDown,No|Yes,"
	L"Indicate whether a tag's coloring information will be used "
	L"to determine how to display the data value on the web page."
	L"\0"

	L"PageSec,Access Control,,AccessMask/Pick,-P,"
	L"Edit the security descriptor for this page. Users must always "
	L"have the Web Server access right to access this page, irrespective "
	L"of the configuration of this setting."
	L"\0"

	L"Set,Contents,,TagSet/TagSet,52|Page,"
	L"Select the tags to be displayed on this web page."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Page Schemas
//

CWebServer_Pages RCDATA
BEGIN
	L"Control,Features,Security,Advanced\0"
END

CWebServer_Page1 RCDATA
BEGIN
	L"G:1,root,Control,Enable\0"
	L"G:1,opts,Connection,Tls,Port,HttpRedirect,CertSource\0"
	L"G:1,root,Identification,Title,Header,Home\0"
	L"G:1,root,Options,CustHome,CustIcon,CustLogon,CustCss,CustJs,SystemJump\0"
	L"\0"
END

CWebServer_Page2 RCDATA
BEGIN
	L"G:1,root,Remote View,RemoteView,RemoteCtrl,RemoteZoom,RemoteFact,RemoteBits,RemoteSec\0"
	L"G:1,root,Log Access,LogsView,LogsBatches,LogsSec\0"
	L"G:1,root,Custom Site,UserSite,UserMenu,UserRoot,UserDelay,UserSec\0" // Legacy expansion?
	L"G:1,root,System Pages,SystemPages,SystemConsole,SystemCapture,SystemSec\0"
	L"\0"
END

CWebServer_Page3 RCDATA
BEGIN
	L"G:1,opts,Authentication,AuthMethod,HttpMethod,Realm\0"
	L"G:1,root,Credentials,Source,Local,User,Real,Pass\0"
	L"G:1,root,IP Restrictions,Restrict,SecAddr,SecMask\0"
	L"\0"
END

CWebServer_Page4 RCDATA
BEGIN
	L"G:1,opts,Access,Advanced\0"
	L"G:1,root,Port Override,Port\0"
	L"G:1,opts,Connections,SockCount,MaxKeep\0"
	L"G:1,opts,Timeouts,SendTimeout,RecvTimeout,IdleTimeout,InitTimeout,SessTimeout\0"
	L"G:1,root,Headers,SameOrigin\0"
	L"G:1,opts,Compression,CompReply\0"
	L"\0"
END

CWebPage_Page RCDATA
BEGIN
	L"G:1,root,Options,Title,Refresh,Colors,Edit,Manual,Delay,Hide,PageSec\0"
	L"G:1,root,Contents,Set\0"

	L"\0"
END

// End of File
