
#include "Intern.hpp"

#include "CommsTreeWnd_CCmdPaste.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Paste Command
//

// Runtime Class

AfxImplementRuntimeClass(CCommsTreeWnd::CCmdPaste, CStdCmd);

// Constructor

CCommsTreeWnd::CCmdPaste::CCmdPaste(CItem *pItem, CItem *pRoot, CItem *pPrev, IDataObject *pData)
{
	m_Menu  = IDS_PASTE;

	m_Item  = pItem->GetFixedPath();

	m_Root  = GetFixedPath(pRoot);

	m_Prev  = GetFixedPath(pPrev);

	m_pData = pData;

	m_pData->AddRef();
	}

// Destructor

CCommsTreeWnd::CCmdPaste::~CCmdPaste(void)
{
	m_pData->Release();
	}

// End of File
