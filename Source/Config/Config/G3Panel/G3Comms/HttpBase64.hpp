
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_HttpBase64_HPP

#define INCLUDE_HttpBase64_HPP

//////////////////////////////////////////////////////////////////////////
//
// Base-64 Encoder
//

class DLLAPI CHttpBase64
{
	public:
		// Attributes
		static UINT GetEncodeSize(UINT uSize);
		static UINT GetDecodeSize(PCSTR pText);

		// Operations
		static BOOL    Encode(PSTR pBuff, PCBYTE pData, UINT uSize, BOOL fBreak);
		static PSTR    Encode(PCBYTE pData, UINT uSize, BOOL fBreak);
		static BOOL    Decode(PBYTE pBuff, PCSTR pText, UINT uText);
		static BOOL    Decode(PBYTE pBuff, CString const &Text);
		static BOOL    Decode(PBYTE pBuff, PCSTR pText);
		static PBYTE   Decode(PCSTR pText, UINT uExtra = 0);

	protected:
		// Implementation
		static BOOL IsBase64(BYTE cData);
		static BYTE ByteEncode(BYTE bData);
		static BYTE ByteDecode(BYTE cData);
	};

// End of File

#endif
