
#include "Intern.hpp"

#include "TetheredItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tethered Rack Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CTetheredItem, CCascadedRackItem);

// Constructor

CTetheredItem::CTetheredItem(void)
{
	CUsbTreePath Root  =  { 0, 0, 1, 0, 0, 0, 0, 0, 0 };

	CUsbTreePath HostA  = { 0, 0, 2, 0, 2, 0, 0, 0, 0 };

	CUsbTreePath HostB  = { 0, 0, 2, 0, 3, 0, 0, 0, 0 };

	m_Slot  = Root.dw;

	m_HostA = HostA.dw;

	m_HostB = HostB.dw;

	m_Port  = 0;

	InitRackTable();

	InitSlotTable();
	}

// Operations

void CTetheredItem::SetHostPorts(CUsbTreePath const &HostA, CUsbTreePath const &HostB)
{
	m_HostA = HostA.dw;

	m_HostB = HostB.dw;

	InitRackTable();

	InitSlotTable();

	RemapSlots(TRUE);
	}

// UI Management

CViewWnd * CTetheredItem::CreateView(UINT uType)
{
	return CUIItem::CreateView(uType);
	}

// UI Creation

BOOL CTetheredItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CUIStdPage(AfxPointerClass(this)));

	return FALSE;
	}

// UI Update

void CTetheredItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "Port" ) {

		InitRackTable();

		InitSlotTable();

		RemapSlots(TRUE);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Meta Data Creation

void CTetheredItem::AddMetaData(void)
{
	CCascadedRackItem::AddMetaData();

	Meta_AddInteger(HostA);
	Meta_AddInteger(HostB);
	Meta_AddInteger(Port);
	}

// Persistance

void CTetheredItem::PostLoad(void)
{
	CCascadedRackItem::PostLoad();

	InitRackTable();

	InitSlotTable();
	}

// Path Config

DWORD CTetheredItem::GetRackPath(UINT uRack) const
{
	return uRack < elements(m_RackTable) ? m_RackTable[uRack].dw : NOTHING;
	}

DWORD CTetheredItem::GetSlotPath(UINT uRack, UINT uSlot) const
{
	UINT i = uRack * 3 + uSlot;

	return i < elements(m_SlotTable) ? m_SlotTable[i].dw : NOTHING;
	}

void CTetheredItem::InitRackTable(void)
{
	UINT s1, s2, s3, s4;

	GetPortsRack(s1, s2, s3, s4);

	CUsbTreePath Host;

	Host.dw = m_Port ? m_HostB : m_HostA;

	for( UINT n = 0; n < elements(m_RackTable); n ++ ) {

		if( !n ) {

			m_RackTable[n].dw = Host.dw;
			}
		else {
			m_RackTable[n].dw = m_RackTable[n-1].dw;

			m_RackTable[n].a.dwTier++;

			if( m_RackTable[n].a.dwTier > Host.a.dwTier ) {

				m_RackTable[n].SetPort(m_RackTable[n].a.dwTier, s4);
				}
			}
		}
	}

void CTetheredItem::InitSlotTable(void)
{
	UINT s1, s2, s3, s4;

	GetPortsRack(s1, s2, s3, s4);

	CUsbTreePath Host;

	Host.dw = m_Port ? m_HostB : m_HostA;

	UINT n  = 0;

	for( UINT r = 0; r < elements(m_RackTable); r ++ ) {

		UINT uTier = Host.a.dwTier + r + 1;

		for( UINT s = 0; s < 3; s ++ ) {

			m_SlotTable[n+s].dw = m_RackTable[r].dw;

			m_SlotTable[n+s].a.dwTier = uTier;
			}

		m_SlotTable[n+0].SetPort(uTier, s1);
		m_SlotTable[n+1].SetPort(uTier, s2);
		m_SlotTable[n+2].SetPort(uTier, s3);

		while( --uTier >  Host.a.dwTier ) {

			m_SlotTable[n+0].SetPort(uTier, s4);
			m_SlotTable[n+1].SetPort(uTier, s4);
			m_SlotTable[n+2].SetPort(uTier, s4);
			}

		n += 3;
		}
	}

// Item Naming

CString CTetheredItem::GetHumanName(void) const
{
	return IDS("Tethered Rack");
	}

// End of File
