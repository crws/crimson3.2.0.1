
#include "Intern.hpp"

#include "CloudDataSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//

// Dynamic Class

AfxImplementDynamicClass(CCloudDataSet, CCodedHost);

// Constructor

CCloudDataSet::CCloudDataSet(void)
{
	m_Mode    = 0;

	m_History = 0;

	m_Scan    = 10;

	m_Force   = 0;

	m_pReq    = NULL;

	m_pAck    = NULL;

	m_pSuffix = NULL;
}

// UI Update

void CCloudDataSet::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Mode" ) {

			DoEnables(pHost);
		}
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CCloudDataSet::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Suffix" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "Req" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "Ack" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
	}

	return FALSE;
}

// Download Support

BOOL CCloudDataSet::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));

	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		Init.AddByte(BYTE(m_History));
	}
	else {
		Init.AddByte(BYTE(0));
	}

	Init.AddWord(WORD(m_Scan));

	Init.AddWord(WORD(m_Force));

	Init.AddItem(itemVirtual, m_pReq);

	Init.AddItem(itemVirtual, m_pAck);

	Init.AddItem(itemVirtual, m_pSuffix);

	return TRUE;
}

// Meta Data Creation

void CCloudDataSet::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddInteger(History);
	Meta_AddInteger(Scan);
	Meta_AddInteger(Force);
	Meta_AddVirtual(Req);
	Meta_AddVirtual(Ack);
	Meta_AddVirtual(Suffix);

	Meta_SetName((IDS_CLOUD_DATA_SET));
}

// Implementation

void CCloudDataSet::DoEnables(IUIHost *pHost)
{
	CMetaItem *pItem = (CMetaItem *) GetParent();

	BOOL       fTwin = FALSE;

	if( wcsstr(pItem->GetClassName(), L"Google") ) {

		fTwin = TRUE;
	}

	if( pItem->FindMetaData(L"Twin") ) {

		IDataAccess *pData = pItem->GetDataAccess(L"Twin");

		if( pData->ReadInteger(pItem) ) {

			fTwin = TRUE;
		}
	}

	if( pItem->FindMetaData(L"Shadow") ) {

		IDataAccess *pData = pItem->GetDataAccess(L"Shadow");

		if( pData->ReadInteger(pItem) ) {

			fTwin = TRUE;
		}
	}

	pHost->EnableUI(this, "Scan", m_Mode == 1);

	pHost->EnableUI(this, "History", m_Mode >= 1);

	pHost->EnableUI(this, "Force", m_Mode == 1 || m_Mode == 2);

	pHost->EnableUI(this, "Req", m_Mode == 1 || m_Mode == 2);

	pHost->EnableUI(this, "Ack", m_Mode == 2);

	pHost->EnableUI(this, "Suffix", !fTwin && m_Mode >= 1);
}

// End of File
