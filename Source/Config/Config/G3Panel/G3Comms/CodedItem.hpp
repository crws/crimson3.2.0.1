
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CodedItem_HPP

#define INCLUDE_CodedItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CTag;
class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Coded Item
//

class DLLAPI CCodedItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCodedItem(void);

		// Server Access
		INameServer * GetNameServer(void) const;
		IDataServer * GetDataServer(void) const;

		// Attributes
		BOOL    IsEmpty(void) const;
		BOOL	IsBroken(void) const;
		BOOL    IsLValue(void) const;
		BOOL    IsWritable(void) const;
		BOOL    IsConst(void) const;
		CString GetSource(BOOL fExpand) const;
		UINT    GetRefCount(void) const;
		UINT    GetType(void) const;
		UINT    GetFlags(void) const;
		BOOL    IsCommsRef(void) const;
		BOOL	IsCommsRefNamed(void) const;
		BOOL	IsCommsRefTable(void) const;
		UINT    GetCommsBits(void) const;
		BOOL    IsTagRef(void) const;
		BOOL    IsLocalRef(void) const;
		UINT    GetObjectRef(void) const;
		UINT    GetIndexRef(void) const;
		WORD    GetTagRef(void) const;
		UINT    GetTagIndex(void) const;
		BOOL    GetTagItem(CTag * &pTag) const;
		CString GetTagName(void) const;
		CString GetTagPath(void) const;
		CString GetFindInfo(void) const;
		CString GetPropName(void) const;

		// Operations
		BOOL Compile(CString Text);
		BOOL Compile(CError &Error, CString Text);
		BOOL Compile(CError &Error, CString Text, CString Prop);
		BOOL Recompile(CError &Error, BOOL fExpand);
		BOOL Recompile(BOOL fExpand);
		BOOL Recompile(void);
		BOOL SetBroken(void);
		void SetReqType(CTypeDef const &Type);
		void SetParams(CString const &Params);
		BOOL CheckCircular(CTag *pTag, BOOL fFormat);
		BOOL UpdateExtent(UINT uSize);
		void Empty(void);

		// Overridables
		virtual void LoadParams(CCompileIn &In, BOOL &fFunc);
		virtual void KillParams(CCompileIn &In);

		// Execution
		DWORD ExecVal(void) const;
		DWORD Execute(UINT Type) const;
		DWORD Execute(UINT Type, PDWORD pParam) const;
		DWORD GetNull(UINT Type) const;
		DWORD GetNull(void) const;

		// Reference Access
		CDataRef const & GetRef(UINT uRef) const;

		// Persistance
		void Init(void);
		void Save(CTreeFile &File);
		void Load(CTreeFile &File);
		void PostLoad(void);
		void PreCopy(void);
		void PostPaste(void);
		void Kill(void);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Linked List
		CCodedItem * m_pNext;
		CCodedItem * m_pPrev;

		// Item Properties
		CByteArray      m_Source;
		CByteArray      m_Object;
		CLongArray      m_Refs;
		CString         m_Params;
		UINT            m_ReqType;
		UINT            m_ReqFlags;
		UINT            m_ActType;
		UINT            m_ActFlags;
		UINT            m_RefBits;

	protected:
		// Data Members
		CCommsSystem * m_pSystem;
		UINT           m_uParent;

		// Meta Data Creation
		void AddMetaData(void);

		// Recompile Hook
		virtual BOOL OnRecompile(CError &Error, BOOL fExpand);

		// Implementation
		BOOL DoCompile(CCompileIn &In, CCompileOut &Out);
		void FindSystem(void);
		BOOL FindRefBits(void);
		BOOL StripOwnRef(void);
		BOOL FindTagRef(CCodedTree &Busy, CTagList *pList, CTag *pFind, BOOL fFormat);
		BOOL FindParent(void);
		BOOL SkipHandle(UINT uHandle);
		void ListAppend(void);
		void ListRemove(void);
	};

// End of File

#endif
