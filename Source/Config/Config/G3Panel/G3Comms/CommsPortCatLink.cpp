
#include "Intern.hpp"

#include "CommsPortCatLink.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAT Link Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortCatLink, CCommsPort);

// Constructor

CCommsPortCatLink::CCommsPortCatLink(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding = bindCatLink;
	}

// Overridable

BOOL CCommsPortCatLink::IsRemotable(void) const
{
	return m_Remotable;
	}

// Download Support

BOOL CCommsPortCatLink::MakeInitData(CInitData &Init)
{
	Init.AddByte(8);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CCommsPortCatLink::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_SetName((IDS_CATLINK_PORT));
	}

// End of File
