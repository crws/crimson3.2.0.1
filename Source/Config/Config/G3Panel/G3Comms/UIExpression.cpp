
#include "Intern.hpp"

#include "UIExpression.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodeEditingDialog.hpp"
#include "CommsDevice.hpp"
#include "CommsManager.hpp"
#include "CommsSystem.hpp"
#include "ItemCreateDialog.hpp"
#include "NameServer.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"
#include "TagSelectDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Expression
//

// Dynamic Class

AfxImplementDynamicClass(CUIExpression, CUICategorizer);

// Constructor

CUIExpression::CUIExpression(void)
{
	m_cfCode = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_fEmpty = FALSE;

	m_fComp  = TRUE;

	m_fComms = TRUE;

	m_fTags  = TRUE;

	m_fExpr  = TRUE;
	}

// Core Overridables

void CUIExpression::OnBind(void)
{
	m_pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	m_pComms  = m_pSystem->m_pComms;

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( !List[2].IsEmpty() ) {

		m_Params = List[2];
		}

	CUICategorizer::OnBind();
	}

// Notification Overridables

BOOL CUIExpression::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pPickCtrl->GetID() ) {

		if( !m_pItem->GetDatabase()->IsReadOnly() ) {

			if( m_uMode == modeGeneral || m_uMode == modeComplex ) {

				CString Text = m_pText->GetAsText();

				if( EditCode(Text) ) {

					StdSave(TRUE, FindDataText(Text, m_uMode));

					return TRUE;
					}

				return FALSE;
				}

			if( m_uMode == modeTag ) {

				CString Tag = m_pShowCtrl->GetWindowText();

				if( SelectTag(Tag) ) {

					StdSave(TRUE, Tag);

					return TRUE;
					}

				return FALSE;
				}

			if( m_uMode >= modeDevice ) {

				CCommsDevice *pDev = m_pSystem->m_pComms->FindDevice(m_uMode - modeDevice);

				if( pDev ) {

					CString Text = m_pShowCtrl->GetWindowText();

					CString Pick = pDev->SelectAddress(Text);

					if( Pick.IsEmpty() || Text.CompareC(Pick) ) {

						CString Data  = FindDataText(Pick, m_uMode);

						UINT    uCode = StdSave(TRUE, Data);

						if( uCode != saveError ) {

							m_pSystem->SetLastAddress(Data);

							return TRUE;
							}

						return FALSE;
						}
					}
				}
			}
		}

	return CUICategorizer::OnNotify(uID, uCode);
	}

// Data Overridables

BOOL CUIExpression::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{

	// LATER -- Accept partial comms addresses.

	if( CanAcceptText(pData, m_cfCode) ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
		}

	if( CanAcceptText(pData, CF_UNICODETEXT) ) {

		dwEffect = DROPEFFECT_COPY;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIExpression::OnAcceptData(IDataObject *pData)
{
	// LATER -- Accept partial comms addresses.

	if( AcceptText(pData, m_cfCode) ) {

		SetBestFocus();

		return TRUE;
		}

	if( AcceptText(pData, CF_UNICODETEXT) ) {

		SetBestFocus();

		return TRUE;
		}

	return FALSE;
	}

// Category Overridables

BOOL CUIExpression::LoadModeButton(void)
{
	CModeButton::COption Opt;

	m_pModeCtrl->ClearOptions();

	if( TRUE ) {

		Opt.m_uID     = modeGeneralWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fComp ) {

		Opt.m_uID     = modeComplexWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fEmpty ) {

		Opt.m_uID     = modeInternal;
		Opt.m_Text    = CString(IDS_INTERNAL);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fExpr ) {

		Opt.m_uID     = modeGeneral;
		Opt.m_Text    = CString(IDS_GENERAL);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fComp ) {

		Opt.m_uID     = modeComplex;
		Opt.m_Text    = CString(IDS_COMPLEX);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fTags ) {

		Opt.m_uID     = modeTag;
		Opt.m_Text    = CString(IDS_TAG);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);

		Opt.m_uID     = modeNewTag;
		Opt.m_Text    = CString(IDS_NEW_TAG);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fComms ) {

		UINT uMin = 1;

		UINT uMax = m_pComms->GetMaxDeviceNumber();

		BOOL fAdd = FALSE;

		for( UINT n = uMin; n <= uMax; n++ ) {

			CCommsDevice *pDev = m_pComms->FindDevice(n);

			if( pDev && pDev->GetDriver() ) {

				UINT uType = pDev->GetDriver()->GetType();

				if( uType == driverMaster || uType == driverHoneywell ) {

					if( !fAdd ) {

						if( TRUE ) {

							Opt.m_uID     = NOTHING;
							Opt.m_Text    = L"";
							Opt.m_fEnable = TRUE;
							Opt.m_fHidden = FALSE;
							Opt.m_Image   = 0;

							m_pModeCtrl->AddOption(Opt);
							}

						if( TRUE ) {

							Opt.m_uID     = modeNext;
							Opt.m_Text    = CString(IDS_NEXT);
							Opt.m_fEnable = m_pSystem->HasNextAddress();
							Opt.m_fHidden = FALSE;
							Opt.m_Image   = 0;

							m_pModeCtrl->AddOption(Opt);
							}

						if( TRUE ) {

							Opt.m_uID     = NOTHING;
							Opt.m_Text    = L"";
							Opt.m_fEnable = TRUE;
							Opt.m_fHidden = FALSE;
							Opt.m_Image   = 0;

							m_pModeCtrl->AddOption(Opt);
							}

						fAdd = TRUE;
						}

					Opt.m_uID     = modeDevice + n;
					Opt.m_Text    = pDev->GetName();
					Opt.m_fEnable = TRUE;
					Opt.m_fHidden = FALSE;
					Opt.m_Image   = 0;

					m_pModeCtrl->AddOption(Opt);
					}
				}
			}
		}

	return TRUE;
	}

BOOL CUIExpression::SwitchMode(UINT uMode)
{
	if( uMode == NOTHING ) {

		if( m_uMode == modeComplex || m_uMode == modeComplexWas ) {

			CString Text = m_pText->GetAsText();

			if( Text.StartsWith(L"WAS ") ) {

				Text = Text.Mid(4);
				}

			if( EditCode(Text) ) {

				StdSave(TRUE, Text);

				ForceUpdate();

				return TRUE;
				}

			return FALSE;
			}

		SwitchMode(modeGeneral);

		return TRUE;
		}

	if( uMode == modeInternal ) {

		StdSave(FALSE, L"");

		ForceUpdate();

		return TRUE;
		}

	if( uMode == modeGeneral ) {

		if( m_uMode == modeInternal ) {

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(L"", TRUE);

			return TRUE;
			}

		if( m_uMode == modeComplex || m_uMode == modeComplexWas ) {

			// LATER -- If there's only one none-empty line of code,
			// we can convert this to General without actually having
			// to drop anything except return. Add support for this.

			CString Warn;

			Warn += CString(IDS_THIS_WILL_DELETE);

			Warn += L"\n\n";

			Warn += CString(IDS_DO_YOU_WANT_TO_4);

			if( CWnd::GetActiveWindow().NoYes(Warn) == IDNO ) {

				return FALSE;
				}

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(L"", TRUE);

			return TRUE;
			}

		if( m_uMode == modeTag ) {

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(m_pShowCtrl->GetWindowText(), TRUE);

			return TRUE;
			}

		if( m_uMode == modeGeneralWas ) {

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(m_pShowCtrl->GetWindowText(), TRUE);

			return TRUE;
			}

		if( m_uMode >= modeDevice ) {

			CString Text = m_pShowCtrl->GetWindowText();

			CString Data = FindDataText(Text, m_uMode);

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(Data, TRUE);

			return TRUE;
			}

		return FALSE;
		}

	if( uMode == modeComplex ) {

		CString Text = m_pText->GetAsText();

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);
			}

		if( !IsComplex(Text) ) {

			MakeComplex(Text);
			}

		if( EditCode(Text) ) {

			StdSave(TRUE, FindDataText(Text, uMode));

			ForceUpdate();

			return TRUE;
			}

		return FALSE;
		}

	if( uMode == modeTag ) {

		CString Tag;

		if( SelectTag(Tag) ) {

			if( StdSave(TRUE, Tag) == saveChange ) {

				ForceUpdate();

				return TRUE;
				}
			}

		return FALSE;
		}

	if( uMode == modeNewTag ) {

		if( NewTag() ) {

			return TRUE;
			}

		return FALSE;
		}

	if( uMode == modeNext ) {

		CString Addr;

		if( m_pSystem->GetNextAddress(Addr, TRUE) ) {

			if( StdSave(TRUE, Addr) == saveChange ) {

				ForceUpdate();

				return TRUE;
				}
			}

		return FALSE;
		}

	if( uMode >= modeDevice ) {

		CCommsDevice *pDev = m_pComms->FindDevice(uMode - modeDevice);

		if( pDev ) {

			if( m_uMode == modeGeneralWas || m_uMode >= modeDevice ) {

				CString Text = m_pShowCtrl->GetWindowText();

				if( pDev->CheckAddress(Text) ) {

					CString Data = FindDataText(Text, uMode);

					StdSave(TRUE, Data);

					m_pSystem->SetLastAddress(Data);

					ForceUpdate();

					return TRUE;
					}
				}

			CString Pick = pDev->SelectAddress(L"");

			if( !Pick.IsEmpty() ) {

				CString Data  = FindDataText(Pick, uMode);

				UINT    uCode = StdSave(TRUE, Data);

				if( uCode != saveError ) {

					m_pSystem->SetLastAddress(Data);

					m_pModeCtrl->SetData(m_uMode = uMode);

					ForceUpdate();

					return TRUE;
					}

				return FALSE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

UINT CUIExpression::FindDispMode(CString Text)
{
	if( Text.IsEmpty() ) {

		return m_fEmpty ? modeInternal : modeGeneral;
		}

	if( Text.StartsWith(L"WAS ") ) {

		Text = Text.Mid(4);

		if( IsComplex(Text) ) {

			return modeComplexWas;
			}

		return modeGeneralWas;
		}

	if( IsComplex(Text) ) {

		return modeComplex;
		}

	if( Text.Left(1) == L"[" && Text.Right(1) == L"]" ) {

		CString       Addr = Text.Mid(1, Text.GetLength() - 2);

		CString       Name = Addr.StripToken('.');

		CCommsDevice *pDev = m_pComms->FindDevice(Name);

		if( pDev && pDev->GetDriver() ) {

			UINT uType = pDev->GetDriver()->GetType();

			if( uType == driverMaster || uType == driverHoneywell ) {

				return modeDevice + pDev->m_Number;
				}
			}

		// LATER -- This should not happen. Recompile
		// should prevent us ever getting to here!!!

		return modeGeneralWas;
		}

	if( IsTagName(Text) ) {

		return modeTag;
		}

	return modeGeneral;
	}

CString CUIExpression::FindDispText(CString Text, UINT uMode)
{
	if( uMode == modeGeneralWas ) {

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);

			return Text;
			}
		}

	if( uMode == modeComplexWas ) {

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);

			return Text.StripToken(L"\r\n").Mid(3);
			}
		}

	if( uMode == modeComplex ) {

		return Text.StripToken(L"\r\n").Mid(3);
		}

	if( uMode >= modeDevice ) {

		CString Addr = Text.Mid(1, Text.GetLength() - 2);

		CString Name = Addr.StripToken('.');

		return Addr;
		}

	return Text;
	}

CString CUIExpression::FindDataText(CString Text, UINT uMode)
{
	if( uMode == modeGeneralWas ) {

		return L"WAS " + Text;
		}

	if( uMode == modeComplexWas ) {

		return L"WAS " + Text;
		}

	if( uMode >= modeDevice ) {

		if( Text.GetLength() ) {

			CCommsDevice *pDev = m_pComms->FindDevice(uMode - modeDevice);

			CString Data;

			Data += L'[';

			Data += pDev->m_Name;

			Data += L'.';

			Data += Text;

			Data += L']';

			return Data;
			}

		return L"";
		}

	return Text;
	}

UINT CUIExpression::FindDispType(UINT uMode)
{
	if( uMode == modeInternal ) {

		return 0;
		}

	if( uMode == modeComplex ) {

		return 1;
		}

	if( uMode == modeTag ) {

		return 1;
		}

	if( uMode == modeGeneralWas ) {

		return 1;
		}

	if( uMode == modeComplexWas ) {

		return 1;
		}

	if( uMode >= modeDevice ) {

		return 1;
		}

	return 2;
	}

CString CUIExpression::FindDispVerb(UINT uMode)
{
	if( uMode == modeGeneral ) {

		return CString(IDS_EDIT_2);
		}

	if( uMode == modeComplex ) {

		return CString(IDS_EDIT_2);
		}

	if( uMode == modeTag ) {

		return IDS_PICK;
		}

	if( uMode >= modeDevice ) {

		return IDS_PICK;
		}

	return L"";
	}

// Implementation

BOOL CUIExpression::SelectTag(CString &Tag)
{
	CTagSelectDialog Dlg(m_pItem->GetDatabase(), Tag);

	CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

	if( System.ExecSemiModal(Dlg, L"Tags") ) {

		Tag = Dlg.GetTag();

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIExpression::IsTagName(CString Text)
{
	CNameServer *pName  = m_pSystem->GetNameServer();

	INameServer *pFind  = pName;

	CError      *pError = New CError(FALSE);

	CTypeDef     Type   = { 0, 0 };

	WORD         ID     = 0;

	pName->ResetServer();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

		CCodedHost *pHost = (CCodedHost *) m_pItem;

		pFind             = pHost->GetNameServer(pName);
		}

	if( pFind->FindIdent(pError, Text, ID, Type) ) {

		if( ID & 0x8000 ) {

			delete pError;

			return TRUE;
			}
		}

	delete pError;

	return FALSE;
	}

BOOL CUIExpression::NewTag(void)
{
	CDatabase    *pDbase  = m_pItem->GetDatabase();

	CCommsSystem *pSystem = (CCommsSystem *) pDbase->GetSystemItem();

	CTagManager  *pTags   = pSystem->m_pTags;

	CTagList     *pList   = pTags->m_pTags;

	UINT          Hint    = typeVoid;

	CString       Name;

	if( m_pItem->IsKindOf(AfxRuntimeClass(CCodedHost))) {

		CCodedHost *pHost = (CCodedHost *) m_pItem;

		CTypeDef    Type;

		if( pHost->GetTypeData(m_UIData.m_Tag, Type) ) {

			Hint = Type.m_Type;
			}
		else
			Hint = typeInteger;

		pHost->GetNameHint(m_UIData.m_Tag, Name);
		}

	if( TRUE ) {

		CItemCreateDialog Dlg(pList, Hint, Name);

		if( Dlg.Execute() ) {

			CString Name = Dlg.GetName();

			UINT    Type = Dlg.GetType();

			UINT    Size = Dlg.GetSize();

			if( !pTags->CreateTag(Name, Type, Size) ) {

				CWnd::GetActiveWindow().Error(CString(IDS_TAG_COULD_NOT_BE));

				return FALSE;
				}

			if( Size ) {

				Name += L"[0]";
				}

			if( StdSave(TRUE, Name) == saveChange ) {

				UINT uMode = Size ? modeGeneral : modeTag;

				m_pModeCtrl->SetData(m_uMode = uMode);

				ForceUpdate();

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CUIExpression::EditCode(CString &Text)
{
	CCodedHost *pHost = (CCodedHost *) m_pItem;

	if( pHost->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

		CCodeEditingDialog Dlg( pHost,
					m_UIData.m_Tag,
					m_Params,
					Text
					);

		CSysProxy System;

		System.Bind();

		if( System.ExecSemiModal(Dlg) ) {

			Text = Dlg.GetAsText();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUIExpression::IsComplex(CString Text)
{
	return Text.Find(L"\r\n") < NOTHING;
	}

void CUIExpression::MakeComplex(CString &Code)
{
	CString Text;

	Text += L"// ";

	Text += CString(IDS_COMPLEX_CODE);

	Text += L"\r\n";

	if( !Code.IsEmpty() ) {

		Text += "return ";

		Text += Code;

		Text += L";";

		Text += L"\r\n";
		}

	Code  = Text;
	}

// End of File
