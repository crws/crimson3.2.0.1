
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlPreviewDialog_HPP

#define INCLUDE_SqlPreviewDialog_HPP

//////////////////////////////////////////////////////////////////////////
//
// SQL Preview Dialog
//

class CSqlPreviewDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSqlPreviewDialog(CString const &SQL);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uId);
		
		// Data Members
		CString m_SQL;
	};

// End of File

#endif
