
#include "Intern.hpp"

#include "TagStringPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispColor.hpp"
#include "DispFormat.hpp"
#include "SecDesc.hpp"
#include "TagString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// String Tag Page
//

// Runtime Class

AfxImplementRuntimeClass(CTagStringPage, CUIStdPage);

// Constructor

CTagStringPage::CTagStringPage(CTagString *pTag, CString Title, UINT uPage)
{
	m_pTag  = pTag;

	m_Title = Title;

	m_Class = AfxRuntimeClass(CTagString);

	m_uPage = uPage;
	}

// Operations

BOOL CTagStringPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	if( m_uPage == 2 ) {

		LoadFormat(pView);

		pView->NoRecycle();
		}

	if( m_uPage == 3 ) {

		LoadColor(pView);

		pView->NoRecycle();
		}

	if( m_uPage == 4 ) {

		LoadSecurity(pView);

		pView->NoRecycle();
		}

	return TRUE;
	}

// Implementation

BOOL CTagStringPage::LoadFormat(IUICreate *pView)
{
	CDispFormat *pFormat = m_pTag->m_pFormat;

	if( pFormat ) {

		CUIPageList List;

		pFormat->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pFormat);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagStringPage::LoadColor(IUICreate *pView)
{
	CDispColor *pColor = m_pTag->m_pColor;

	if( pColor ) {

		CUIPageList List;

		pColor->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pColor);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagStringPage::LoadSecurity(IUICreate *pView)
{
	CSecDesc *pSec = m_pTag->m_pSec;

	if( pSec ) {

		CUIPageList List;

		pSec->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pSec);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
