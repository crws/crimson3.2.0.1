
#include "Intern.hpp"

#include "TagTrigger.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Trigger
//

// Dynamic Class

AfxImplementDynamicClass(CTagTrigger, CCodedHost);

// Constructor

CTagTrigger::CTagTrigger(void)
{
	m_Mode    = 0;

	m_Delay	  = 0;

	m_pAction = NULL;
	}

// UI Update

void CTagTrigger::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Mode" ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CTagTrigger::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Action" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	if( Tag == "SubIndex" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Searching

void CTagTrigger::FindActive(CStringArray &List)
{
	if( m_Mode ) {

		CString Text;

		Text += GetHumanPath();

		Text += L" - ";

		Text += ((CUIItem *) GetParent())->GetSubItemLabel(this);

		Text += L" (";

		Text += GetPropAsText(L"Mode");

		if( FindMetaData(L"Value") ) {

			Text += L", ";

			Text += GetPropAsText(L"Value");
			}

		Text += L")\n";

		Text += GetFixedPath();

		Text += L':';

		Text += L"Mode";

		List.Append(Text);
		}
	}

// Download Support

BOOL CTagTrigger::MakeInitData(CInitData &Init)
{
	if( m_Mode ) {

		Init.AddByte(1);

		CCodedHost::MakeInitData(Init);

		Init.AddByte(BYTE(m_Mode));

		Init.AddWord(WORD(m_Delay));

		Init.AddItem(itemVirtual, m_pAction);

		return TRUE;
		}

	Init.AddByte(0);

	return FALSE;
	}

// Meta Data

void CTagTrigger::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddInteger(Delay);
	Meta_AddVirtual(Action);

	Meta_SetName((IDS_TAG_TRIGGER));
	}

// Implementation

void CTagTrigger::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Delay",  m_Mode > 0);

	pHost->EnableUI(this, "Action", m_Mode > 0);
	}

// End of File
