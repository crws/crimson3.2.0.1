
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlColumn_HPP

#define INCLUDE_SqlColumn_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlRecord;
class CSqlRecordList;

//////////////////////////////////////////////////////////////////////////
//
// SQL Column Definition
//

class CSqlColumn : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CSqlColumn(void);
		CSqlColumn(CString Name);
		CSqlColumn(CString Name, UINT uCol, UINT uRows);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Persistence
		void Init(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		void Validate(BOOL fExpand);

		// Attributes
		UINT          GetC3Type(void);
		CSqlRecord  * GetRecord(UINT uRow);
		BOOL          CanClear(void);

		// Implementation
		void UpdateRecordType(UINT uType);
		void ClearAllMappings(void);
		BOOL Check(CStringArray &Errors);

		// SQL Type Constants
		typedef enum {

			sqlBinary	= 0,
			sqlChar		= 1,
			sqlInt		= 2,
			sqlFloat	= 3,
			sqlDatetime	= 4,
			sqlVarChar	= 5,
			sqlNVarChar	= 6
			} SqlType;

		// SQL Data Type Structure
		struct CSqlType 
		{
			SqlType  m_Type;
			UINT     m_uType;
			PCTXT    m_pName;
			};

		static CSqlType m_Types[7];
		
		// Data Members
		CString            m_Name;
		CString            m_SqlName;
		UINT               m_Column;
		UINT               m_Rows;
		SqlType            m_Type;
		CSqlRecordList   * m_pRecords;

	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
