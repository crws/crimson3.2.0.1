
#include "Intern.hpp"

#include "SqlColumnList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SqlColumn.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Column List
//

AfxImplementDynamicClass(CSqlColumnList, CNamedList);

// Constructor

CSqlColumnList::CSqlColumnList(void)
{
	m_Class = AfxRuntimeClass(CSqlColumn);
	}

// Item Access

CSqlColumn * CSqlColumnList::GetItem(INDEX Index) const
{
	return (CSqlColumn *) CNamedList::GetItem(Index);
	}

CSqlColumn * CSqlColumnList::GetItem(UINT uPos) const
{
	return (CSqlColumn *) CNamedList::GetItem(uPos);
	}

CSqlColumn * CSqlColumnList::GetColumnDef(UINT uCol)
{
	UINT n = 0;

	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CSqlColumn * pColumn = GetItem(i);

		if( n == uCol ) {

			return pColumn;
			}

		n++;
		}

	return NULL;
	}

UINT CSqlColumnList::GetColumnPos(CString const &Name)
{
	UINT n = 0;

	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CSqlColumn *pColumn = GetItem(i);

		if( pColumn->m_SqlName == Name ) {

			return n;
			}

		n++;
		}

	return NOTHING;
	}

// Download Support

BOOL CSqlColumnList::MakeInitData(CInitData &Init)
{
	CNamedList::MakeInitData(Init);

	return TRUE;
	}

// End of File
