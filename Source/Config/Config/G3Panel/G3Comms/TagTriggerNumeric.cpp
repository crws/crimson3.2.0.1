
#include "Intern.hpp"

#include "TagTriggerNumeric.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "TagNumeric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Trigger
//

// Dynamic Class

AfxImplementDynamicClass(CTagTriggerNumeric, CTagTrigger);

// Constructor

CTagTriggerNumeric::CTagTriggerNumeric(void)
{
	m_pValue = NULL;

	m_pHyst  = NULL;
	}

// UI Update

void CTagTriggerNumeric::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UINT uMask = HasSetpoint() ? 0xFFF : 0xE1F;

		LimitEnum(pHost, L"Mode", m_Mode, uMask);

		DoEnables(pHost);
		}

	if( Tag == "Mode" ) {

		DoEnables(pHost);
		}

	CTagTrigger::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CTagTriggerNumeric::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" || Tag == "Hyst" ) {

		CTag * pTag  = (CTag *) GetParent(AfxRuntimeClass(CTag));

		Type.m_Type  = pTag->GetDataType();

		Type.m_Flags = 0;

		return TRUE;
		}

	return CTagTrigger::GetTypeData(Tag, Type);
	}

// Attributes

BOOL CTagTriggerNumeric::NeedSetpoint(void) const
{
	return m_Mode >= 5 && m_Mode <= 8;
	}

BOOL CTagTriggerNumeric::HasSetpoint(void) const
{
	CTagNumeric * pTag = (CTagNumeric *) GetParent(AfxRuntimeClass(CTagNumeric));

	return pTag->HasSetpoint();
	}

// Operations

void CTagTriggerNumeric::UpdateTypes(BOOL fComp)
{
	UpdateType(NULL, L"Value", m_pValue, fComp);

	UpdateType(NULL, L"Hyst",  m_pHyst,  fComp);
	}

// Download Support

BOOL CTagTriggerNumeric::MakeInitData(CInitData &Init)
{
	if( CTagTrigger::MakeInitData(Init) ) {

		Init.AddItem(itemVirtual, m_pValue);

		Init.AddItem(itemVirtual, m_pHyst);
		}

	return TRUE;
	}

// Meta Data

void CTagTriggerNumeric::AddMetaData(void)
{
	CTagTrigger::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddVirtual(Hyst);

	Meta_SetName((IDS_TAG_TRIGGER));
	}

// Implementation

void CTagTriggerNumeric::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Value", m_Mode >= 1 && m_Mode <= 11);

	pHost->EnableUI(this, "Hyst",  m_Mode >= 3 && m_Mode <=  8);
	}

// End of File
