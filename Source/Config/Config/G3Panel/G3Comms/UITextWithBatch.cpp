
#include "Intern.hpp"

#include "UITextWithBatch.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

#include "DataLogger.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Include With Batch
//

// Dynamic Class

AfxImplementDynamicClass(CUITextWithBatch, CUITextEnumPick);

// Constructor

CUITextWithBatch::CUITextWithBatch(void)
{
	m_uFlags |= textReload;
	}

// Core Overridables

void CUITextWithBatch::OnBind(void)
{
	CUITextEnumPick::OnBind();

	Reload();
	}

BOOL CUITextWithBatch::OnEnumValues(CStringArray &List)
{
	Reload();

	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_uMask & (1 << n) ) {

			List.Append(m_Enum[n].m_Text);
			}
		}

	return TRUE;
	}

// Implementation

void CUITextWithBatch::Reload(void)
{
	m_Enum.Empty();

	CEntry Enum;

	Enum.m_Data = 0;

	Enum.m_Text = CString(IDS_NO_2);

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);

	if( m_pItem ) {

		CDatabase *pDbase  = m_pItem->GetDatabase();

		if( pDbase ) {

			CCommsSystem *pSystem = (CCommsSystem *) pDbase->GetSystemItem();

			CDataLogger  *pLogger = pSystem->m_pLog;

			if( pLogger->m_EnableSets ) {

				if( m_UIData.m_Format.IsEmpty() ) {

					for( UINT n = 1; n <= 8; n++ ) {

						Enum.m_Data = n;

						Enum.m_Text = CPrintf(CString(IDS_FORMAT_8), n);

						// cppcheck-suppress uninitStructMember

						m_Enum.Append(Enum);
						}
					}

				return;
				}
			}
		}

	Enum.m_Data = 1;

	Enum.m_Text = CString(IDS_YES);

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

// End of File
