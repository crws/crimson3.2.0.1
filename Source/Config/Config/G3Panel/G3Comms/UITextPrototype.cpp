
#include "Intern.hpp"

#include "UITextPrototype.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramItem.hpp"
#include "ProgramPrototype.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Program Prototype
//

// Dynamic Class

AfxImplementDynamicClass(CUITextPrototype, CUITextElement);

// Constructor

CUITextPrototype::CUITextPrototype(void)
{
	m_uFlags = textExpand;
	}

// Overridables

void CUITextPrototype::OnBind(void)
{
	FindPrototype();

	CUITextElement::OnBind();

	m_Verb   = "Edit";

	m_uWidth = 40;
	}

void CUITextPrototype::OnRebind(void)
{
	FindPrototype();

	CUITextElement::OnRebind();
	}

CString CUITextPrototype::OnGetAsText(void)
{
	return m_pPrototype->Format();
	}

UINT CUITextPrototype::OnSetAsText(CError &Error, CString Text)
{
	CString Prev = m_pPrototype->Format();

	if( Prev.CompareC(Text) ) {

		m_pPrototype->Parse(Text);

		return saveChange;
		}

	return saveSame;
	}

BOOL CUITextPrototype::OnExpand(CWnd &Wnd)
{
	CItem *pItem = m_pData->GetObject(m_pItem);

	CItemDialog Dlg(pItem, CString(IDS_EDIT_PROGRAM));

	if( Dlg.Execute(Wnd) ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CUITextPrototype::FindPrototype(void)
{
	CProgramItem *pProgram = (CProgramItem *) m_pItem;

	m_pPrototype = pProgram->m_pProt;
	}

// End of File
