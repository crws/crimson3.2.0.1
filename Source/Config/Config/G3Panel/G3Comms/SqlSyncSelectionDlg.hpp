
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlSyncSelectionDlg_HPP

#define INCLUDE_SqlSyncSelectionDlg_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDataLogList;
class CSqlSync;

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Log Selection Dialog
//

class CSqlSyncSelectionDlg : public CStdDialog
{
	public:
		AfxDeclareRuntimeClass();

		// Constructor
		CSqlSyncSelectionDlg(CSqlSync *pSqlSync, CDataLogList *pList);

		// Destructor
		~CSqlSyncSelectionDlg(void);

	protected:

		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uId);
		BOOL OnInvert(UINT uId);
		BOOL OnSelAll(UINT uId);
		BOOL OnUnselAll(UINT uId);

		// Data Members
		CSqlSync	* m_pSqlSync;
		CDataLogList	* m_pLogs;
		BOOL		  m_fValid;

		// Implementation
		void InitCheckState(void);
		void SetAllCheckState(BOOL fState);
	};

// End of File

#endif
