
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortGroup_HPP

#define INCLUDE_CommsPortGroup_HPP

//////////////////////////////////////////////////////////////////////////
//
// Communications Port Group
//

class DLLNOT CCommsPortGroup : public CMetaItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCommsPortGroup(void);

	protected:
		// Data Members
		CString m_Name;

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
