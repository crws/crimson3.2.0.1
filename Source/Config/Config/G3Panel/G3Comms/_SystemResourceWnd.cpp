
#include "Intern.hpp"

#include "SystemResourceWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "FunctionItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CSystemResourceWnd, CResTreeWnd);

// Constructor

CSystemResourceWnd::CSystemResourceWnd(void) : CResTreeWnd( L"Lib",
							    AfxRuntimeClass(CFolderItem),
							    AfxRuntimeClass(CFunctionItem)
							    )
{
	m_cfCode    = RegisterClipboardFormat(L"C3 Code Fragment");
	}

// Tree Loading

void CSystemResourceWnd::LoadTree(void)
{
	afxThread->SetWaitMode(TRUE);

	afxThread->SetStatusText(CString(IDS_LOADING_VIEW));

	LoadRoot();

	UINT uTotal = m_pList->GetItemCount();

	UINT uCount = 0;

	for( UINT uPass = 0; uCount < uTotal; uPass++ ) {

		INDEX Index = m_pList->GetHead();

		while( !m_pList->Failed(Index) ) {

			CMetaItem *pItem = (CMetaItem *) m_pList->GetItem(Index);

			CString    Name  = pItem->GetName();

			if( Name.Count('.') == uPass ) {

				PCTXT pName = Name;

				UINT  uPos  = 0;

				if( uPass ) {

					for( UINT c = 0; pName[uPos]; uPos++ ) {

						if( pName[uPos] == L'.' ) {

							if( ++c == uPass ) {

								break;
								}
							}
						}
					}

				if( !m_MapNames.FindName(pItem->GetName()) ) {

					CString Path = Name.Left(uPos+0);

					CTreeViewItem Node;

					LoadNodeItem(Node, pItem);

					HTREEITEM hRoot = m_MapNames[Path];

					HTREEITEM hItem = m_pTree->InsertItem(hRoot, NULL, Node);

					AddToMaps(pItem, hItem);
					}

				uCount++;
				}

			m_pList->GetNext(Index);
			}
		}

	INDEX Index = m_MapNames.GetHead();

	while( !m_MapNames.Failed(Index) ) {

		HTREEITEM hRoot = m_MapNames.GetData(Index);

		CMetaItem *pItem = (CMetaItem *) m_pTree->GetItemParam(hRoot);

		if( pItem->IsKindOf(m_Folder) ) {

			m_pTree->SortChildren(hRoot, PFNTVCOMPARE(&SortHelp), LPARAM(this));
			}

		m_MapNames.GetNext(Index);
		}

	afxThread->SetStatusText(L"");

	afxThread->SetWaitMode(FALSE);

	SelectRoot();
	}

void CSystemResourceWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"ProgTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CSystemResourceWnd::GetRootImage(void)
{
	return 25;
	}

UINT CSystemResourceWnd::GetItemImage(CMetaItem *pItem)
{
	return pItem->IsKindOf(m_Folder) ? 22 : 18;
	}

void CSystemResourceWnd::NewItemSelected(void)
{
	CString Text;

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CFunctionItem)) ) {

		CFunctionItem *pFunc = (CFunctionItem *) m_pSelect;

		Text = pFunc->GetPrototype();
		}

	afxThread->SetStatusText(Text);
	}

// Data Object Creation

BOOL CSystemResourceWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddCodeFragment(pMake);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CSystemResourceWnd::AddCodeFragment(CDataObject *pData)
{
	if( m_pNamed ) {

		CString Name = m_pNamed->GetName();

		UINT uPos;

		if( (uPos = Name.Find('.')) < NOTHING ) {

			if( Name.Left(uPos) == CString(IDS_FUNCTIONS) ) {

				Name += L"()";
				}
			}

		pData->AddText(m_cfCode, GetName(Name));

		return TRUE;
		}

	return FALSE;
	}

// Sort Helpers

int CSystemResourceWnd::Sort(CMetaItem *p1, CMetaItem *p2)
{
	if( p1->GetName() < p2->GetName() ) {

		return -1;
		}

	if( p1->GetName() > p2->GetName() ) {

		return +1;
		}

	return 0;
	}

// Sort Callback

int CALLBACK CSystemResourceWnd::SortHelp(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	CMetaItem *p1 = (CMetaItem *) lParam1;

	CMetaItem *p2 = (CMetaItem *) lParam2;

	return ((CSystemResourceWnd *) lParamSort)->Sort(p1, p2);
	}

// End of File
