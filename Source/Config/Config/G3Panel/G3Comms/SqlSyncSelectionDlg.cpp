
#include "Intern.hpp"

#include "SqlSyncSelectionDlg.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DataLog.hpp"
#include "DataLogList.hpp"
#include "SqlSync.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Log Selection Dialog
//

AfxImplementRuntimeClass(CSqlSyncSelectionDlg, CStdDialog);

// Message Map

AfxMessageMap(CSqlSyncSelectionDlg, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(1002, OnSelAll)
	AfxDispatchCommand(1003, OnUnselAll)
	AfxDispatchCommand(1004, OnInvert)

	AfxMessageEnd(CSqlSyncSelectionDlg)
	};

// Constructor

CSqlSyncSelectionDlg::CSqlSyncSelectionDlg(CSqlSync *pSqlSync, CDataLogList *pList)
{
	SetName(L"SqlSyncSelectionDlg");

	m_pSqlSync = pSqlSync;

	m_pLogs	   = pList;

	m_fValid   = FALSE;
	}

// Destructor

CSqlSyncSelectionDlg::~CSqlSyncSelectionDlg(void)
{
	}

// Message Handlers

BOOL CSqlSyncSelectionDlg::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListView &ListView = (CListView &) GetDlgItem(1001);

	CListViewColumn Column;

	Column.SetText(CString(IDS_SQL_SYNC_LOGS));

	Column.SetWidth(200);

	ListView.InsertColumn(0, Column);

	UINT uCount = m_pLogs->GetItemCount();

	if( uCount > 0 ) {

		DWORD dwStyle = LVS_EX_CHECKBOXES;

		ListView.SetExtendedListViewStyle(dwStyle, dwStyle);

		for( INDEX i = m_pLogs->GetTail(); !m_pLogs->Failed(i); m_pLogs->GetPrev(i) ) {

			CDataLog *pLog = m_pLogs->GetItem(i);

			if( pLog ) {

				CListViewItem Item;

				CString Name;

				if( !pLog->m_Path.IsEmpty() )

					Name = pLog->m_Path;
				else
					Name = pLog->m_Name;

				LVFINDINFO Info;

				memset(&Info, 0, sizeof(Info));

				Info.flags = LVFI_STRING;
				Info.psz   = PCTXT(Name);

				if( ListView.FindItem(NOTHING, Info) == NOTHING ) {

					Item.SetText(Name);

					ListView.InsertItem(Item);
					}
				}
			}

		InitCheckState();

		m_fValid = TRUE;
		}
	else {
		CListViewItem Item;

		Item.SetText(CString(IDS_SQL_NO_DATA_LOGS));

		ListView.InsertItem(Item);
		}

	return TRUE;
	}

BOOL CSqlSyncSelectionDlg::OnOkay(UINT uId)
{
	CListView &ListView = (CListView &) GetDlgItem(1001);

	HWND hListView = ListView.GetHandle();

	if( m_fValid ) {

		m_pSqlSync->m_LogNames.Empty();

		for( UINT n = 0; n < ListView.GetItemCount(); n++ ) {

			if( ListView_GetCheckState(hListView, n) ) {

				CListViewItem Item = ListView.GetItem(n);

				m_pSqlSync->m_LogNames.Append(Item.GetText());
				}
			}

		m_pSqlSync->m_LogInit = TRUE;

		EndDialog(TRUE);

		return TRUE;
		}

	EndDialog(FALSE);

	return FALSE;
	}

BOOL CSqlSyncSelectionDlg::OnInvert(UINT uId)
{
	CListView &ListView = (CListView &) GetDlgItem(1001);

	HWND hListView = ListView.GetHandle();

	for( UINT n = 0; n < ListView.GetItemCount(); n++ ) {

		BOOL fState = ListView_GetCheckState(hListView, n);

		ListView_SetCheckState(hListView, n, !fState);
		}

	return TRUE;
	}

BOOL CSqlSyncSelectionDlg::OnSelAll(UINT uId)
{
	SetAllCheckState(TRUE);

	return TRUE;
	}

BOOL CSqlSyncSelectionDlg::OnUnselAll(UINT uId)
{
	SetAllCheckState(FALSE);

	return TRUE;
	}

// Implementation

void CSqlSyncSelectionDlg::InitCheckState(void)
{
	if( !m_pSqlSync->m_LogInit ) {

		SetAllCheckState(TRUE);
		}
	else {
		CListView &ListView = (CListView &) GetDlgItem(1001);

		HWND hListView = ListView.GetHandle();

		for( UINT n = 0; n < ListView.GetItemCount(); n++ ) {

			CString Name = ListView.GetItem(n).GetText();

			if( m_pSqlSync->m_LogNames.Find(Name) < NOTHING) {

				ListView_SetCheckState(hListView, n, TRUE);
				}
			}
		}
	}

void CSqlSyncSelectionDlg::SetAllCheckState(BOOL fState)
{
	CListView &ListView = (CListView &) GetDlgItem(1001);

	HWND hListView = ListView.GetHandle();

	for( UINT n = 0; n < ListView.GetItemCount(); n++ ) {

		ListView_SetCheckState(hListView, n, fState);
		}
	}

// End of File
