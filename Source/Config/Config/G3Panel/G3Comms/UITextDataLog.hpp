
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextDataLog_HPP

#define INCLUDE_UITextDataLog_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextCoded.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;

class CDataLogList;

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Data Log Selection
//

class CUITextDataLog : public CUITextCoded
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextDataLog(void);

	protected:
		// Data Members
		CCommsSystem * m_pSystem;
		CDataLogList * m_pList;
		CString	       m_Was;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnEnumValues(CStringArray &List);
		BOOL    OnExpand(CWnd &Wnd);

		// Implementation
		BOOL CheckWas(void);
		UINT OnNewLog(CError &Error);
	};

// End of File

#endif
