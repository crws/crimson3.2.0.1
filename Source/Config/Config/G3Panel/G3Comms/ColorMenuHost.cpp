
#include "Intern.hpp"

#include "ColorMenuHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ColorManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Color Picker Host
//

// Runtime Class

AfxImplementRuntimeClass(CColorMenuHost, CMenuWnd);

// Static Data

CColorMenuHost * CColorMenuHost::m_pThis = NULL;

// Constructor

CColorMenuHost::CColorMenuHost(CColorManager *pColor)
{
	m_pThis  = this;

	m_pColor = pColor;

	m_uTrack = NOTHING;
	}

// Destructor

CColorMenuHost::~CColorMenuHost(void)
{
	m_pThis = NULL;
	}

// Operations

BOOL CColorMenuHost::Select(CPoint Pos, COLOR &Color)
{
	MakeMenu();

	m_uSpec = 0;

	m_hHook = SetWindowsHookEx( WH_MSGFILTER,
			            FARPROC(MessageProc),
				    afxModule->GetAppInstance(),
				    GetCurrentThreadId()
				    );

	m_pColor->FindColor(Color, m_uGroup, m_uIndex);

	UINT uID = m_pMenu->TrackPopupMenu( TPM_LEFTALIGN |
					    TPM_RETURNCMD,
					    Pos,
					    ThisObject
					    );

	UnhookWindowsHookEx(m_hHook);

	FreeMenu();

	if( IsSpecial(uID) ) {

		if( m_uTrack < NOTHING ) {

			UINT uGroup = GetGroup(uID);

			UINT uIndex = m_uTrack;

			Color = m_pColor->GetColor(uGroup, uIndex);

			m_pColor->MarkColor(uGroup, uIndex);

			return TRUE;
			}
		}

	if( IsPickMore(uID) ) {

		if( !m_pColor->GetDatabase()->IsReadOnly() ) {

			CColorDialog Dlg;

			Dlg.ShowRainbowOnly();

			Dlg.SetColor(C3GetWinColor(Color));

			if( Dlg.Execute(ThisObject) ) {

				Color = C3GetGdiColor(Dlg.GetColor());

				m_pColor->FindColor(Color, m_uGroup, m_uIndex);

				m_pColor->MarkColor(Color);

				return TRUE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

// Callback Hook

void CColorMenuHost::TrackPos(CPoint Pos)
{
	CWnd  &Wnd = CWnd::FromHandle(FindWindow(L"#32768", NULL));

	CRect Rect = Wnd.GetWindowRect();

	if( Rect.PtInRect(Pos) ) {

		m_Pos = Pos - Rect.GetTopLeft();

		if( m_Pos.x < 28 ) {

			m_uTrack = NOTHING;
			}
		else
			m_uTrack = (m_Pos.x - 28) / 22;

		Wnd.Invalidate(FALSE);
		}
	}

void CColorMenuHost::KeyDown(UINT uCode)
{
	if( uCode == VK_LEFT ) {

		if( m_uTrack > 0 ) {

			m_uTrack--;

			RedrawMenu();
			}
		}

	if( uCode == VK_RIGHT ) {

		if( m_uTrack < m_uSpec - 1 ) {

			m_uTrack++;

			RedrawMenu();
			}
		}
	}

// Message Map

AfxMessageMap(CColorMenuHost, CMenuWnd)
{
	AfxDispatchMessage(WM_MENUSELECT)
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxDispatchGetInfoType(0xB2, OnColorGetInfo)
	AfxDispatchControlType(0xB2, OnColorControl)
	AfxDispatchCommandType(0xB2, OnColorExecute)

	AfxMessageEnd(CColorMenuHost)
	};

// Message Handlers

void CColorMenuHost::OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu)
{
	if( uFlags & MF_OWNERDRAW ) {

		if( IsSpecial(uID) ) {

			m_uSpec = m_pColor->GetCount(GetGroup(uID));

			if( uFlags & MF_MOUSESELECT ) {

				TrackPos(GetCursorPos());
				}
			else {
				if( m_uTrack == NOTHING ) {

					m_uTrack = 0;
					}
				else
					MakeMin(m_uTrack, m_uSpec - 1);

				RedrawMenu();
				}
			}
		else {
			if( HIBYTE(uID) ) {

				m_uSpec  = 0;

				m_uTrack = NOTHING;
				}
			}
		}

	CMenuWnd::OnMenuSelect(uID, uFlags, Menu);
	}

void CColorMenuHost::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	if( IsSpecial(Item.itemID) ) {

		UINT uGroup = GetGroup(Item.itemID);

		UINT uCount = m_pColor->GetCount(uGroup);

		Item.itemWidth  = 12 + (22 * uCount) + 2;

		Item.itemHeight = 22;

		return;
		}

	CMenuWnd::OnMeasureItem(uID, Item);
	}

void CColorMenuHost::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
        if( Item.CtlType == ODT_MENU ) {

		CRect Rect = Item.rcItem;

		CSize Size = Rect.GetSize();

		CDC       MenuDC(Item.hDC);

		CMemoryDC WorkDC(MenuDC);

		CBitmap   WorkBM(MenuDC, Size);

		WorkDC.Select(WorkBM);

		DRAWITEMSTRUCT Copy = Item;

		Copy.hDC      = WorkDC;

		Copy.rcItem   = CRect(Size);

		WorkDC.FillRect(Size, afxBrush(MenuFace));

		if( IsSpecial(Item.itemID) ) {

			CPoint Corner = Rect.GetTopLeft();

			m_Pos.x -= Corner.x;

			DrawSpecial(WorkDC, Copy);
			}
		else
			DrawMenu(WorkDC, Copy);

		MenuDC.BitBlt(Rect, WorkDC, CPoint(0, 0), SRCCOPY);

		WorkDC.Deselect();
		}
	}

// Command Handlers

BOOL CColorMenuHost::OnColorGetInfo(UINT uID, CCmdInfo &Info)
{
	return TRUE;
	}

BOOL CColorMenuHost::OnColorControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

BOOL CColorMenuHost::OnColorExecute(UINT uID)
{
	return TRUE;
	}

// Implementation

void CColorMenuHost::DrawSpecial(CDC &DC, DRAWITEMSTRUCT &Item)
{
	CMenuInfo *pInfo = (CMenuInfo *) Item.itemData;

	DC.Select(afxFont(Status));

	DC.SetBkMode(TRANSPARENT);

	DrawColors(DC, pInfo, Item.rcItem, Item.itemState);

	DC.Deselect();
	}

void CColorMenuHost::DrawColors(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState)
{
	CRect Work = Rect;

	Work.right = Work.left + 22;

	DC.GradHorz(Work, afxColor(Blue1), afxColor(Blue3));

	Work.left  = Work.right + 2;

	Work.right = Rect.right + 2;

	CRect Fill = Work;

	UINT  uGroup = GetGroup(pInfo->m_uID);

	UINT  uCount = m_pColor->GetCount(uGroup);

	for( UINT uIndex = 0; uIndex < uCount; uIndex++ ) {

		CColor Color = C3GetWinColor(m_pColor->GetColor(uGroup, uIndex));

		Fill.right   = Fill.left + 22;

		if( uGroup == m_uGroup && uIndex == m_uIndex ) {

			uState |=  ODS_CHECKED;
			}
		else
			uState &= ~ODS_CHECKED;

		if( m_uTrack == uIndex ) {

			uState |=  ODS_HOTLIGHT;
			}
		else
			uState &= ~ODS_HOTLIGHT;

		DrawBack(DC, Fill, uState);

		CRect Tile = Fill - 3;

		if( Color == afxColor(WHITE) ) {

			DC.FrameRect(--Tile, afxBrush(BLACK));
			}

		DC.FillRect(--Tile, Color);

		Fill.left = Fill.right;
		}
	}

void CColorMenuHost::DrawBack(CDC &DC, CRect Rect, UINT uState)
{
	if( uState & ODS_HOTLIGHT ) {

		if( uState & ODS_CHECKED ) {

			if( uState & ODS_SELECTED ) {

				DC.FrameRect(Rect-0, afxBrush(3dShadow));

				DC.FillRect (Rect-1, afxBrush(Orange1));
				}
			else {
				DC.FrameRect(Rect-0, afxBrush(3dDkShadow));

				DC.FillRect (Rect-1, afxBrush(Orange2));
				}
			}
		else {
			if( uState & ODS_SELECTED ) {

				DC.FrameRect(Rect-0, afxBrush(3dDkShadow));

				DC.FillRect (Rect-1, afxBrush(Orange3));
				}
			else
				DC.FillRect(Rect, afxBrush(MenuFace));
			}
		}
	else {
		if( uState & ODS_CHECKED ) {

			DC.FrameRect(Rect-0, afxBrush(3dDkShadow));

			DC.FillRect (Rect-1, afxBrush(Orange2));
			}
		}
	}

void CColorMenuHost::MakeMenu(void)
{
	m_pMenu = New CMenu;

	m_pMenu->CreatePopupMenu();

	UINT ng = m_pColor->GetGroupCount();

	for( UINT g = 0; g < ng; g++ ) {

		m_pMenu->AppendMenu(0, 0xB280 + g, L"X");

		if( g == ng - 1 || m_pColor->NeedsSep(g) ) {

			m_pMenu->AppendSeparator();

			continue;
			}
		}

	m_pMenu->AppendMenu(0, 0xB201, CString(IDS_MORE));

	m_pMenu->MakeOwnerDraw(FALSE);
	}

void CColorMenuHost::FreeMenu(void)
{
	m_pMenu->FreeOwnerDraw();

	delete m_pMenu;
	}

void CColorMenuHost::RedrawMenu(void)
{
	CWnd &Wnd = CWnd::FromHandle(FindWindow(L"#32768", NULL));

	Wnd.Invalidate(FALSE);
	}

BOOL CColorMenuHost::IsSpecial(UINT uID)
{
	return uID >= 0xB280;
	}

BOOL CColorMenuHost::IsPickMore(UINT uID)
{
	return uID == 0xB201;
	}

UINT CColorMenuHost::GetGroup(UINT uID)
{
	return uID - 0xB280;
	}

// Hook Procedure

LRESULT CALLBACK CColorMenuHost::MessageProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if( m_pThis->m_uSpec ) {

		MSG *pMsg = (MSG *) lParam;

		if( pMsg->message == WM_MOUSEMOVE ) {

			CPoint Pos = CPoint(pMsg->lParam);

			m_pThis->TrackPos(Pos);
			}

		if( pMsg->message == WM_KEYDOWN ) {

			UINT uCode = pMsg->wParam;

			m_pThis->KeyDown(uCode);
			}
		}

	return CallNextHookEx(m_pThis->m_hHook, nCode, wParam, lParam);
	}

// End of File
