
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQueryManagerTreeWnd_CCmdCreate_HPP

#define INCLUDE_SqlQueryManagerTreeWnd_CCmdCreate_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SqlQueryManagerTreeWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Tree Window -- Create Command
//

class CSqlQueryManagerTreeWnd::CCmdCreate : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdCreate(CItem *pRoot, CString List, CMetaItem *pItem);

		// Destructor
		~CCmdCreate(void);

		// Data Members
		CString m_Made;
		CString m_List;
		HGLOBAL m_hData;
	};

// End of File

#endif
