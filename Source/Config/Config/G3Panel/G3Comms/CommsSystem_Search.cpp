
#include "Intern.hpp"

#include "CommsSystem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
//#include "ColorManager.hpp"
#include "CommsDevice.hpp"
#include "CommsManager.hpp"
#include "CommsMapping.hpp"
#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "CommsSysBlock.hpp"
#include "Tag.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"
#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item -- Searching
//

// Searching

BOOL CCommsSystem::IsTagUsed(UINT uTag)
{
	CStringArray List;

	FindCodedWithRef(List, 0x8000 | uTag, 0x0000FFFF);

	if( List.IsEmpty() ) {

		FindTagSetWithTag(List, uTag);

		if( List.IsEmpty() ) {

			FindMappingWithTag(List, uTag);

			if( List.IsEmpty() ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CCommsSystem::FindTagUsage(CString Tag, UINT uTag)
{
	UpdateWindow();

	CStringArray List;

	FindCodedWithRef  (List, 0x8000 | uTag, 0x0000FFFF);

	FindTagSetWithTag (List, uTag);

	FindMappingWithTag(List, uTag);

	if( List.IsEmpty() ) {

		CString Text = CPrintf(IDS_FMT_IS_NOT_USED, Tag);

		CWnd::GetActiveWindow().Error(Text);

		return FALSE;
		}

	CSysProxy Proxy;

	Proxy.Bind();

	Proxy.SetFindList(CPrintf(IDS_USAGE_FOR_FMT, Tag), List, TRUE);

	return TRUE;
	}

BOOL CCommsSystem::FindHandleUsage(CString Type, CString Name, UINT hItem)
{
	UpdateWindow();

	CStringArray List;

	FindCodedWithRef(List, hItem, 0xFFFFFFFF);

	if( List.IsEmpty() ) {

		CString Text = CPrintf(IDS_FMT_IS_NOT_USED, Name);

		CWnd::GetActiveWindow().Error(Text);

		return FALSE;
		}

	CSysProxy Proxy;

	Proxy.Bind();

	Proxy.SetFindList(CPrintf(IDS_USAGE_FOR_FMT, Name), List, TRUE);

	return TRUE;
	}

BOOL CCommsSystem::FindDeviceUsage(CCommsDevice *pDevice)
{
	UpdateWindow();

	CStringArray List;

	UINT uDevice = pDevice->m_Number;

	UINT uSearch = MAKEWORD(uDevice, typeDevice);

	FindCodedWithDevice(List, uDevice);

	FindCodedWithRef   (List, uSearch, 0xFFFFFFFF);

	if( List.IsEmpty() ) {

		CString Text = CPrintf(IDS_FMT_IS_NOT_USED, pDevice->m_Name);

		CWnd::GetActiveWindow().Error(Text);

		return FALSE;
		}

	CSysProxy Proxy;

	Proxy.Bind();

	Proxy.SetFindList(CPrintf(IDS_USAGE_FOR_FMT, pDevice->m_Name), List, TRUE);

	return TRUE;
	}

BOOL CCommsSystem::FindPortUsage(CCommsPort *pPort)
{
	UpdateWindow();

	CStringArray List;

	FindCodedWithPort(List, pPort->m_Number);

	if( List.IsEmpty() ) {

		CString Text = CPrintf(IDS_FMT_IS_NOT_USED, pPort->m_Name);

		CWnd::GetActiveWindow().Error(Text);

		return FALSE;
		}

	CSysProxy Proxy;

	Proxy.Bind();

	Proxy.SetFindList(CPrintf(IDS_USAGE_FOR_FMT, pPort->m_Name), List, TRUE);

	return TRUE;
	}

BOOL CCommsSystem::FindBroken(void)
{
	UpdateWindow();

	CStringArray List;

	if( TRUE ) {

		CCodedItem *pCoded = m_pHeadCoded;

		while( pCoded ) {

			if( pCoded->IsBroken() ) {

				CString Info = pCoded->GetFindInfo();

				if( !Info.IsEmpty() ) {

					List.Append(Info);
					}
				}

			pCoded = pCoded->m_pNext;
			}
		}

	if( TRUE ) {

		CCommsMapping *pMapping = m_pHeadMapping;

		while( pMapping ) {

			if( pMapping->IsBroken() ) {

				CString Info = pMapping->GetFindInfo();

				if( !Info.IsEmpty() ) {

					List.Append(Info);
					}
				}

			pMapping = pMapping->m_pNext;
			}
		}

	if( TRUE ) {

		CTagSet *pSet = m_pHeadTagSet;

		while( pSet ) {

			if( pSet->IsBroken() ) {

				CString Info = pSet->GetFindInfo();

				if( !Info.IsEmpty() ) {

					List.Append(Info);
					}
				}

			pSet = pSet->m_pNext;
			}
		}

	if( TRUE ) {

		UINT n = 0;

		CCommsPortList *pList;

		while( m_pComms->GetPortList(pList, n++) ) {

			INDEX n = pList->GetHead();

			while( !pList->Failed(n) ) {

				CCommsPort *pPort = pList->GetItem(n);

				if( pPort->IsBroken() ) {

					CString Info = pPort->GetFindInfo();

					if( !Info.IsEmpty() ) {

						List.Append(Info);
						}
					}

				pList->GetNext(n);
				}
			}
		}

	CSysProxy Proxy;

	Proxy.Bind();

	Proxy.SetFindList(CString(IDS_DATABASE_ERRORS), List, TRUE);

	return !List.IsEmpty();
	}

BOOL CCommsSystem::FindCircular(void)
{
	UpdateWindow();

	CStringArray List;

	m_pTags->FindCircular(List);

	CSysProxy Proxy;

	Proxy.Bind();

	Proxy.SetFindList(CString(IDS_CIRCULAR), List, TRUE);

	return !List.IsEmpty();
	}

BOOL CCommsSystem::GlobalFind(void)
{
	// REV3 -- Use a "Find" dialog to gather options for CString::Search().

	CStringDialog Dlg;

	Dlg.SetCaption(CString(IDS_GLOBAL_FIND));

	Dlg.SetGroup(CString(IDS_FIND));

	Dlg.SetData(m_LastFind);

	if( Dlg.Execute() ) {

		CString Find = Dlg.GetData();

		CStringArray List;

		FindCodedWithText(List, Find);

		CTag *pTag = (CTag *) m_pTags->m_pTags->FindByName(Find);

		if( pTag ) {

			UINT uTag = pTag->GetIndex();

			FindTagSetWithTag (List, uTag);

			FindMappingWithTag(List, uTag);
			}

		if( List.IsEmpty() ) {

			CString Text = IDS_TEXT_NOT_FOUND;

			CWnd::GetActiveWindow().Error(Text);

			return FALSE;
			}

		CSysProxy Proxy;

		Proxy.Bind();

		CString Text;

		Text += CString(IDS_GLOBAL_FIND);

		Text += L" - ";

		Text += Find;

		Proxy.SetFindList(Text, List, TRUE);

		m_LastFind = Find;

		return TRUE;
		}

	return TRUE;
	}

// Implementation

void CCommsSystem::FindCodedWithRef(CStringArray &List, DWORD Data, DWORD Mask)
{
	CCodedItem *pCoded = m_pHeadCoded;

	while( pCoded ) {

		UINT n = pCoded->GetRefCount();

		for( UINT r = 0; r < n; r++ ) {

			CDataRef const &Ref = pCoded->GetRef(r);

			DWORD    const &Val = (DWORD const &) Ref;

			if( (Val & Mask) == Data ) {

				CString Info = pCoded->GetFindInfo();

				if( !Info.IsEmpty() ) {

					List.Append(Info);
					}
				}
			}

		pCoded = pCoded->m_pNext;
		}
	}

void CCommsSystem::FindCodedWithDevice(CStringArray &List, UINT uDevice)
{
	CCodedItem *pCoded = m_pHeadCoded;

	while( pCoded ) {

		UINT n = pCoded->GetRefCount();

		for( UINT r = 0; r < n; r++ ) {

			CDataRef const &Ref = pCoded->GetRef(r);

			if( !Ref.t.m_IsTag ) {

				UINT            uBlock = Ref.b.m_Block;

				CCommsSysBlock *pBlock = m_pComms->FindBlock(uBlock);

				if( pBlock ) {

					CCommsDevice *pDevice = pBlock->GetParentDevice();

					if( pDevice->m_Number == uDevice ) {

						CString Info = pCoded->GetFindInfo();

						if( !Info.IsEmpty() ) {

							List.Append(Info);
							}
						}
					}
				}
			}

		pCoded = pCoded->m_pNext;
		}
	}

void CCommsSystem::FindCodedWithPort(CStringArray &List, UINT uPort)
{
	CCodedItem *pCoded = m_pHeadCoded;

	while( pCoded ) {

		UINT n = pCoded->GetRefCount();

		for( UINT r = 0; r < n; r++ ) {

			CDataRef const &Ref = pCoded->GetRef(r);

			if( !Ref.t.m_IsTag ) {

				UINT            uBlock = Ref.b.m_Block;

				CCommsSysBlock *pBlock = m_pComms->FindBlock(uBlock);

				if( pBlock ) {

					CCommsDevice *pDevice = pBlock->GetParentDevice();

					CCommsPort   *pPort   = pDevice->GetParentPort();

					if( pPort->m_Number == uPort ) {

						CString Info = pCoded->GetFindInfo();

						if( !Info.IsEmpty() ) {

							List.Append(Info);
							}
						}
					}
				}
			}

		pCoded = pCoded->m_pNext;
		}
	}

void CCommsSystem::FindCodedWithText(CStringArray &List, CString Find)
{
	CCodedItem *pCoded = m_pHeadCoded;

	while( pCoded ) {

		CString Text = pCoded->GetSource(TRUE);

		if( Text.Search(Find, searchContains) < NOTHING ) {

			CString Info = pCoded->GetFindInfo();

			if( !Info.IsEmpty() ) {

				List.Append(Info);
				}
			}

		pCoded = pCoded->m_pNext;
		}
	}

void CCommsSystem::FindTagSetWithTag(CStringArray &List, UINT uTag)
{
	CTagSet *pSet = m_pHeadTagSet;

	while( pSet ) {

		if( pSet->HasTag(uTag) ) {

			CString Info = pSet->GetFindInfo();

			if( !Info.IsEmpty() ) {

				List.Append(Info);
				}
			}

		pSet = pSet->m_pNext;
		}
	}

void CCommsSystem::FindMappingWithTag(CStringArray &List, UINT uTag)
{
	CCommsMapping *pMap = m_pHeadMapping;

	while( pMap ) {

		if( pMap->IsTag(uTag) ) {

			CString Info = pMap->GetFindInfo();

			if( !Info.IsEmpty() ) {

				List.Append(Info);
				}
			}

		pMap = pMap->m_pNext;
		}
	}

// End of File
