
#include "Intern.hpp"

#include "HttpServerOptions.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Options
//

// Base Class

#undef  CBaseClass

#define CBaseClass CHttpServerConnectionOptions

// Dynamic Class

AfxImplementDynamicClass(CHttpServerOptions, CBaseClass);

// Constructor

CHttpServerOptions::CHttpServerOptions(void)
{
	m_AuthMethod   = methodAnon;
	m_HttpMethod   = httpBasic;
	m_Realm        = L"Crimson Web Server";
	m_HttpRedirect = FALSE;
	m_SockCount    = 10;
	m_IdleTimeout  = 60;
	m_InitTimeout  = 30;
	m_SessTimeout  = 5;
	}

// UI Update

void CHttpServerOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Advanced" || Tag == "Tls" || Tag == "AuthMethod" ) {

			DoEnables(pHost);
			}
		}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CHttpServerOptions::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddByte(BYTE(m_AuthMethod));
	Init.AddByte(BYTE(m_HttpMethod));
	Init.AddText(m_Realm);
	Init.AddByte(BYTE(m_HttpRedirect));
	Init.AddByte(BYTE(m_SockCount));
	Init.AddWord(WORD(m_IdleTimeout));
	Init.AddWord(WORD(m_InitTimeout));
	Init.AddWord(WORD(m_SessTimeout));

	return TRUE;
	}

// Meta Data Creation

void CHttpServerOptions::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(AuthMethod);
	Meta_AddInteger(HttpMethod);
	Meta_AddString (Realm);
	Meta_AddInteger(HttpRedirect);
	Meta_AddInteger(SockCount);
	Meta_AddInteger(IdleTimeout);
	Meta_AddInteger(InitTimeout);
	Meta_AddInteger(SessTimeout);

	Meta_SetName((IDS_HTTP_SERVER_2));
	}

// Implementation

void CHttpServerOptions::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "HttpRedirect", m_Tls);

	pHost->EnableUI(this, "HttpMethod",   m_AuthMethod == methodHttp);

	pHost->EnableUI(this, "Realm",        m_AuthMethod == methodHttp);

	pHost->EnableUI(this, "SockCount",    m_Advanced);

	pHost->EnableUI(this, "IdleTimeout",  m_Advanced);

	pHost->EnableUI(this, "InitTimeout",  m_Advanced);

	pHost->EnableUI(this, "SessTimeout",  m_Advanced);
	}

// End of File
