
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQuery_HPP

#define INCLUDE_SqlQuery_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlColumn;
class CSqlColumnList;
class CSqlFilterList;
class CSqlRecord;
class CSqlFilter;

//////////////////////////////////////////////////////////////////////////
//
// SQL Query
//

class CSqlQuery : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CSqlQuery(void);
		CSqlQuery(CString Name);

		// Destructor
		~CSqlQuery(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// Persistence
		void PostLoad(void);
		
		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Column Helpers
		CString GetUnusedName(void);
		UINT    GetNextPos(void);

		// Management		
		CSqlColumn  * GetColumn(UINT uCol);
		CSqlColumn  * GetColumn(CString const &SqlName);
		UINT          GetColumnPos(CString const &SqlName);
		CSqlRecord  * GetRecord(UINT uCol, UINT uRow);
		CSqlFilter  * MakeFilter(void);
		void          AdjustRows(UINT uCount);
		void          AddFilter(UINT uPos, CString const &Filter);
		BOOL          DeleteFilter(CSqlFilter *pFilter);
		void          UpdateSortColumn(void);
		void          Validate(BOOL fExpand);
		BOOL          Check(CStringArray &Errors);
		BOOL          Check(void);
		UINT          GetImage(void);		

		// Attributes
		UINT    GetRowCount(void);
		UINT    GetColCount(void);
		CString GetColumnName(UINT uCol);
		CString GetColumnSqlName(UINT uCol);

		// Public Data
		CSqlFilterList   * m_pFilters;
	
		// Properties
		CString		   m_Name;

	protected:
		// Data Members
		CString            m_SQL;
		UINT               m_SortMode;
		UINT               m_SortColumn;
		CString            m_SortName;
		UINT               m_FilterPush;
		UINT		   m_Enable;
		CString		   m_Table;
		CString            m_Schema;
		UINT               m_Rows;
		UINT               m_Update;
		UINT               m_ShowSQL;
		UINT               m_Validate;
		BOOL               m_fBroken;
		CSqlColumnList   * m_pCols;

		// Legacy Column List
		CSqlColumnList  * m_pColumns;
		
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void  DoEnables(IUIHost *pHost);
		void  BuildSQL(void);

		// Friends
		friend class CSqlQueryManagerTreeWnd;
	};

// End of File

#endif
