
#include "Intern.hpp"

#include "CommsMappingList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsMapBlock.hpp"
#include "CommsMapReg.hpp"
#include "CommsMapping.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsMappingList, CItemIndexList);

// Constructor

CCommsMappingList::CCommsMappingList(void)
{
	m_Class = AfxRuntimeClass(CCommsMapping);
	}

// Item Location

CCommsMapReg * CCommsMappingList::GetParentReg(void) const
{
	return (CCommsMapReg *) GetParent();
	}

CCommsMapBlock * CCommsMappingList::GetParentBlock(void) const
{
	return GetParentReg()->GetParentBlock();
	}

CCommsDevice  * CCommsMappingList::GetParentDevice(void) const
{
	return GetParentBlock()->GetParentDevice();
	}

// Item Access

CCommsMapping * CCommsMappingList::GetItem(INDEX Index) const
{
	return (CCommsMapping *) CItemIndexList::GetItem(Index);
	}

CCommsMapping * CCommsMappingList::GetItem(UINT uPos) const
{
	return (CCommsMapping *) CItemIndexList::GetItem(uPos);
	}

// Operations

void CCommsMappingList::Validate(BOOL fExpand)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsMapping *pEntry = GetItem(n);

		pEntry->Validate(fExpand);
		}
	}

void CCommsMappingList::SetCount(UINT uCount)
{
	UINT uActual = GetItemCount();

	while( uActual < uCount ) {

		CCommsMapping *pMap = New CCommsMapping;

		AppendItem(pMap);

		uActual++;
		}

	while( uActual > uCount ) {

		INDEX Index = GetTail();

		DeleteItem(Index);

		uActual--;
		}
	}

// End of File
