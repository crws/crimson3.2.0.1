
#include "Intern.hpp"

#include "CommsTreeWnd_CCmdCreate.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Create Command
//

// Runtime Class

AfxImplementRuntimeClass(CCommsTreeWnd::CCmdCreate, CStdCmd);

// Constructor

CCommsTreeWnd::CCmdCreate::CCmdCreate(CItem *pRoot, CString List, CMetaItem *pItem)
{
	m_Menu  = CFormat(IDS_CREATE, pItem->GetName());

	m_Item  = pRoot->GetFixedPath();

	m_List  = List;

	m_Made  = pItem->GetFixedPath();

	m_hData = pItem->TakeSnapshot();
	}

// Destructor

CCommsTreeWnd::CCmdCreate::~CCmdCreate(void)
{
	GlobalFree(m_hData);
	}

// End of File
