
#include "Intern.hpp"

#include "UITextOptionCard.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OptionCardItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Option Card List
//

// Dynamic Class

AfxImplementDynamicClass(CUITextOptionCard, CUITextEnumPick);

// Constructor

CUITextOptionCard::CUITextOptionCard(void)
{
	}

// Core Overridables

void CUITextOptionCard::OnBind(void)
{
	CUITextEnumPick::OnBind();

	COptionCardItem *pCard = (COptionCardItem *) m_pItem;

	CardType List[] = {

		typeNone,
		typeSerial,
		typeModem,
		typeMPI,
		typeCAN,
		typeJ1939,
		typeDeviceNet,
		typeProfibus,
		typeFireWire,
		typeCatLink,
		typeEthernet,
		typeUSBHost,
		typeDongle,
		typeRack,
		typeModule,
		/*typeDnp3,*/
		/*typeTest,*/
		typeCanCdl
		};

	for( UINT n = 0; n < elements(List); n++ ) {

		if( !pCard->IsSupported(List[n]) ) {

			continue;
			}

		if( C3OemFeature(L"OemSD", FALSE) ) {

			if( n == 3 ) {

				break;
				}
			}

		if( List[n] == typeModule ) {

			m_uMask &= ~(1 << m_Enum.GetCount());
			}

		CEntry Enum;

		Enum.m_Data = List[n];

		Enum.m_Text = pCard->GetCardName(List[n]);

		// cppcheck-suppress uninitStructMember

		m_Enum.Append(Enum);
		}
	}

// End of File
