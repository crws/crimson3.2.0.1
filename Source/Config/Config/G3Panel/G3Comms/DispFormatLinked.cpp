
#include "Intern.hpp"

#include "DispFormatLinked.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "Tag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatLinked, CDispFormat);

// Constructor

CDispFormatLinked::CDispFormatLinked(void)
{
	m_uType   = 7;

	m_fLimits = TRUE;

	m_pLink   = NULL;
	}

// UI Update

void CDispFormatLinked::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "Link" ) {

		if( !TestFormat() ) {

			// REV3 -- This is not what we normally do with
			// circular references, but it is safer and it
			// gets around the problem detailed below.

			if( pHost->HasWindow() ) {

				CString Text = CString(IDS_THIS_WOULD_CREATE);

				CWnd::GetActiveWindow().Error(Text);
				}

			m_pLink->Kill();

			delete m_pLink;

			m_pLink = NULL;

			pHost->UpdateUI(Tag);
			}
		}

	CDispFormat::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispFormatLinked::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Link" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagTagRef | flagInherent;

		return TRUE;
		}

	return FALSE;
	}

// Formatting

CString CDispFormatLinked::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( FindFormat() ) {

		return m_pFormat->Format(Data, Type, Flags);
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Format Access

BOOL CDispFormatLinked::GetFormat(CDispFormat * &pFormat)
{
	if( FindFormat() ) {

		pFormat = m_pFormat;

		return TRUE;
		}

	return FALSE;
	}

// Limit Access

DWORD CDispFormatLinked::GetMin(UINT Type)
{
	if( FindFormat() ) {

		return m_pTag->GetMinValue(Type);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatLinked::GetMax(UINT Type)
{
	if( FindFormat() ) {

		return m_pTag->GetMaxValue(Type);
		}

	return CDispFormat::GetMax(Type);
	}

// Download Support

BOOL CDispFormatLinked::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	if( FindFormat() ) {

		CTag *pTag = NULL;

		m_pLink->GetTagItem(pTag);

		Init.AddWord(WORD(pTag->GetIndex()));
		}
	else
		Init.AddWord(0xFFFF);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatLinked::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_AddVirtual(Link);

	Meta_SetName((IDS_LINKED_FORMAT));
	}

// Implementation

BOOL CDispFormatLinked::TestFormat(void)
{
	if( m_pLink ) {

		if( !m_pLink->IsBroken() ) {

			CItem *pParent = GetParent();

			if( pParent->IsKindOf(AfxRuntimeClass(CTag)) ) {

				CTag *pFind = (CTag *) pParent;

				// REV3 -- This prevents circular references from
				// doing anything bad, but it doesn't show them as
				// the tag doesn't update its circular flag.

				if( m_pLink->CheckCircular(pFind, TRUE) ) {

					return FALSE;
					}
				}
			}
		}

	return TRUE;
	}

BOOL CDispFormatLinked::FindFormat(void)
{
	if( m_pLink ) {

		if( !m_pLink->IsBroken() ) {

			m_pTag = NULL;

			if( m_pLink->GetTagItem(m_pTag) ) {

				if( m_pTag->m_pFormat ) {

					m_pFormat = m_pTag->m_pFormat;

					return TRUE;
					}
				}
			}
		}

	m_pTag    = NULL;

	m_pFormat = NULL;

	return FALSE;
	}

// End of File
