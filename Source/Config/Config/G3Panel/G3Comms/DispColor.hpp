
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColor_HPP

#define INCLUDE_DispColor_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Display Color
//

class DLLAPI CDispColor : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Class Enumeration
		static CLASS GetClass(UINT uType);

		// Constructor
		CDispColor(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		UINT GetColType(void) const;

		// Color Access
		virtual DWORD GetColorPair(DWORD Data, UINT Type);
		virtual COLOR GetForeColor(DWORD Data, UINT Type);
		virtual COLOR GetBackColor(DWORD Data, UINT Type);

		// Property Preservation
		virtual BOOL Preserve(CDispColor *pOld);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT m_uType;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT ImportColor(UINT uData);
	};

// End of File

#endif
