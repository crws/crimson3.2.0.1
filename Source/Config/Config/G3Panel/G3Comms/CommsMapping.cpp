
#include "Intern.hpp"

#include "CommsMapping.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsMapBlock.hpp"
#include "CommsMapReg.hpp"
#include "CommsMapping.hpp"
#include "CommsMappingList.hpp"
#include "CommsMappingViewWnd.hpp"
#include "CommsSystem.hpp"
#include "NameServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications Mapping
//

// Dynamic Class

AfxImplementDynamicClass(CCommsMapping, CMetaItem);

// Constructor

CCommsMapping::CCommsMapping(void)
{
	m_pNext   = NULL;

	m_pPrev   = NULL;

	m_Disable = 0;

	m_Ref     = 0;
	}

// Item Location

CCommsMappingList * CCommsMapping::GetParentList(void) const
{
	return (CCommsMappingList *) GetParent();
	}

CCommsMapReg * CCommsMapping::GetParentReg(void) const
{
	return GetParentList()->GetParentReg();
	}

CCommsMapBlock * CCommsMapping::GetParentBlock(void) const
{
	return GetParentList()->GetParentBlock();
	}

CCommsDevice * CCommsMapping::GetParentDevice(void) const
{
	return GetParentList()->GetParentDevice();
	}

// Attributes

BOOL CCommsMapping::IsMapped(void) const
{
	return m_Source.GetCount() > 1;
	}

BOOL CCommsMapping::IsBroken(void) const
{
	if( !m_Ref ) {

		if( m_Source.GetCount() > 1 ) {

			return TRUE;
			}
		}

	if( GetParentBlock()->m_Addr.m_Ref ) {

		return FALSE;
		}

	return TRUE;
	}

UINT CCommsMapping::GetTreeImage(void) const
{
	CCommsMapBlock *pBlock = GetParentBlock();

	CCommsMapReg   *pReg   = GetParentReg();

	if( pReg->m_Bits ) {

		if( pBlock->IsWriteBlock() ) {

			return IDI_RED_FLAG;
			}

		return IDI_GREEN_FLAG;
		}

	if( pBlock->IsWriteBlock() ) {

		return IDI_RED_INTEGER;
		}

	return IDI_GREEN_INTEGER;
	}

CString CCommsMapping::GetTreeLabel(void) const
{
	CString Text = GetAddrText();

	if( IsMapped() ) {

		Text += GetMapLinkText();

		Text += GetMapAddrText();
		}

	return Text;
	}

CString CCommsMapping::GetAddrText(void) const
{
	CCommsMapReg *pReg = GetParentReg();

	return pReg->GetAddrText() + L"." + CPrintf(L"%2.2u", m_uPos);
	}

CString CCommsMapping::GetMapLinkText(void) const
{
	CCommsMapBlock *pBlock = GetParentBlock();

	BOOL fWrite = pBlock->IsWriteBlock();

	BOOL fSlave = pBlock->IsSlaveBlock();

	return fWrite ? (fSlave ? L" \x0AB\x0BB " : L" \x0BB ") : L" \x0AB ";
	}

CString CCommsMapping::GetMapAddrText(void) const
{
	CString Text = GetMapSource(TRUE);

	if( Text.StartsWith(L"[") ) {

		UINT uSize = Text.GetLength();

		Text.Delete(uSize-1, 1);

		Text.Delete(0, 1);

		return Text;
		}

	if( Text.StartsWith(L"WAS [") ) {

		UINT uSize = Text.GetLength();

		Text.Delete(uSize-1, 1);

		Text.Delete(4, 1);

		return Text;
		}

	return Text;
	}

CString CCommsMapping::GetMapSource(BOOL fExpand) const
{
	if( IsMapped() ) {

		CString Text;

		INameServer *pName = fExpand ? GetNameServer() : NULL;

		CStringArray Using;

		C3ExpandSource(m_Source.GetPointer(), pName, Using, Text);

		return Text;
		}

	return L"";
	}

BOOL CCommsMapping::IsTag(UINT uTag) const
{
	if( m_Ref == (uTag | 0x8000) ) {

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CCommsMapping::Validate(BOOL fExpand)
{
	if( IsMapped() ) {

		CString Source = GetMapSource(fExpand);

		CError  Error  = CError(FALSE);

		CRange  Range;

		if( Source.StartsWith(L"WAS ") ) {

			Source = Source.Mid(4);

			if( SetMapping(Source) ) {

				SendUpdate();
				}
			else {
				m_pDbase->SetRecomp();

				m_Ref = 0;
				}

			return;
			}

		if( !SetMapping(Source) ) {

			Source = L"WAS " + Source;

			SetMapping(Source);

			m_pDbase->SetRecomp();

			m_Ref = 0;

			SendUpdate();
			}
		}
	}

void CCommsMapping::TagCheck(UINT uTag)
{
	if( m_Ref == (uTag | 0x8000) ) {

		SetMapping(GetMapSource(TRUE));

		SendUpdate();
		}
	}

UINT CCommsMapping::SetMapping(CString Text)
{
	CByteArray Source;

	CLongArray Refs;

	if( Text.IsEmpty() ) {

		Source.Append(0);

		Refs.Append(0);
		}
	else {
		CCompileIn  In;

		In.m_pText  = Text;
		In.m_Optim  = 0;
		In.m_Debug  = 0;
		In.m_Switch = 0;
		In.m_uParam = 0;
		In.m_pParam = NULL;
		In.m_pName  = GetNameServer();

		FindReqType(In.m_Type);

		CCompileOut Out;

		Out.m_Error    = CError(FALSE);
		Out.m_pSource  = &Source;
		Out.m_pRefList = &Refs;
		Out.m_pObject  = NULL;

		C3CompileExpr(In, Out);

		if( !Out.m_Error.IsOkay() ) {

			return 0;
			}

		if( Refs.IsEmpty() ) {

			Refs.Append(0);
			}
		}

	if( m_Ref == Refs[0] ) {

		if( IsBroken() ) {

			m_Source = Source;

			return 2;
			}

		m_Source = Source;

		return 1;
		}

	m_Source      = Source;

	m_Ref         = Refs[0];

	CDataRef &Ref = (CDataRef &) m_Ref;

	if( Ref.t.m_IsTag ) {

		UINT uPos = Text.Find('[');

		if( uPos < NOTHING ) {

			Ref.x.m_Array = watoi(Text.Mid(uPos+1));
			}
		}

	return 2;
	}

// UI Creation

CViewWnd * CCommsMapping::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return New CCommsMappingViewWnd;
		}

	return NULL;
	}

// Item Naming

CString CCommsMapping::GetHumanName(void) const
{
	return GetAddrText();
	}

CString CCommsMapping::GetFindInfo(void) const
{
	CString Text;

	Text += GetParent(2)->GetHumanPath();

	Text += '\n';

	Text += GetFixedPath();

	return Text;
	}

// Persistance

void CCommsMapping::Init(void)
{
	CMetaItem::Init();

	m_Source.Append(0);

	ListAppend();
	}

void CCommsMapping::PostLoad(void)
{
	CMetaItem::PostLoad();

	ListAppend();
	}

void CCommsMapping::Kill(void)
{
	CMetaItem::Kill();

	ListRemove();
	}

// Download Support

BOOL CCommsMapping::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CCommsMapping::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Disable);
	Meta_AddBlob   (Source);
	Meta_AddInteger(Ref);

	Meta_SetName((IDS_MAPPING));
	}

// Server Access

INameServer * CCommsMapping::GetNameServer(void) const
{
	CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	CNameServer  *pName   = pSystem->GetNameServer();

	pName->ResetServer();

	return pName;
	}

// Implementation

void CCommsMapping::FindReqType(CTypeDef &Type)
{
	Type.m_Type = typeNumeric;

	if( GetParentBlock()->IsWriteBlock() ) {

		Type.m_Flags = flagWritable | flagInherent;

		return;
		}

	Type.m_Flags = flagInherent;
	}

void CCommsMapping::SendUpdate(void)
{
	CSystemWnd   * pWnd = (CSystemWnd *) afxMainWnd->GetDlgItemPtr(IDVIEW);

	CCommsMapReg * pReg = GetParentReg();

	if( pReg->m_Bits ) {

		pWnd->ItemUpdated(0, this, updateRename);

		return;
		}

	pWnd->ItemUpdated(0, pReg, updateRename);
	}

void CCommsMapping::ListAppend(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	AfxListAppend( pSystem->m_pHeadMapping,
		       pSystem->m_pTailMapping,
		       this,
		       m_pNext,
		       m_pPrev
		       );
	}

void CCommsMapping::ListRemove(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	AfxListRemove( pSystem->m_pHeadMapping,
		       pSystem->m_pTailMapping,
		       this,
		       m_pNext,
		       m_pPrev
		       );
	}

// End of File
