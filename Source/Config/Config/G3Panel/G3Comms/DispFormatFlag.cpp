
#include "Intern.hpp"

#include "DispFormatFlag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Emplopyed Classes
//

#include "CodedText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatFlag, CDispFormat);

// Constructor

CDispFormatFlag::CDispFormatFlag(void)
{
	m_uType = 5;

	m_pOn   = NULL;

	m_pOff  = NULL;
	}

// UI Update

void CDispFormatFlag::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	CDispFormat::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispFormatFlag::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "On" || Tag == "Off" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Formatting

CString CDispFormatFlag::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeLogical ) {

		Type = typeInteger;
		}

	if( Type == typeInteger ) {

		if( INT(Data) ) {

			return m_pOn  ? m_pOn->GetText()  : CString(IDS_ON_3);
			}
		else
			return m_pOff ? m_pOff->GetText() : CString(IDS_OFF_1);
		}

	if( Type == typeReal ) {

		if( I2R(Data) ) {

			return m_pOn  ? m_pOn->GetText()  : CString(IDS_ON_3);
			}
		else
			return m_pOff ? m_pOff->GetText() : CString(IDS_OFF_1);
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Limit Access

DWORD CDispFormatFlag::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return FALSE;
		}

	if( Type == typeReal ) {

		return R2I(FALSE);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatFlag::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return TRUE;
		}

	if( Type == typeReal ) {

		return R2I(TRUE);
		}

	return CDispFormat::GetMax(Type);
	}

// Download Support

BOOL CDispFormatFlag::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pOn);

	Init.AddItem(itemVirtual, m_pOff);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatFlag::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_AddVirtual(On);
	Meta_AddVirtual(Off);

	Meta_SetName((IDS_FLAG_FORMAT));
	}

// Implementation

void CDispFormatFlag::DoEnables(IUIHost *pHost)
{
	}

// End of File
