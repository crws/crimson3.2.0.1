
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_WebPage_HPP

#define INCLUDE_WebPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagSet;

//////////////////////////////////////////////////////////////////////////
//
// Web Page
//

class CWebPage : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWebPage(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Load(CTreeFile &Tree);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT         m_Handle;
		CString      m_Name;
		CCodedText * m_pTitle;
		UINT	     m_Manual;
		UINT	     m_Delay;
		UINT	     m_Edit;
		UINT         m_Refresh;
		UINT	     m_Hide;
		UINT         m_Colors;
		UINT	     m_PageSec;
		CTagSet    * m_pSet;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
