
#include "Intern.hpp"

#include "CommsSystem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Version Information
//

#include "../build.hxx"

#include "../../../../Version/dbver.hpp"

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CodedText.hpp"
#include "ColorManager.hpp"
#include "CommsDevice.hpp"
#include "CommsDeviceList.hpp"
#include "CommsManager.hpp"
#include "CommsMapping.hpp"
#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "CommsPortSerial.hpp"
#include "CommsSysBlock.hpp"
#include "ControlManager.hpp"
#include "DataLogger.hpp"
#include "DataServer.hpp"
#include "EthernetItem.hpp"
#include "ExpansionItem.hpp"
#include "LangManager.hpp"
#include "NameServer.hpp"
#include "OptionCardItem.hpp"
#include "OptionCardList.hpp"
#include "PersistManager.hpp"
#include "ProgramItem.hpp"
#include "ProgramManager.hpp"
#include "RackItem.hpp"
#include "SecurityManager.hpp"
#include "Services.hpp"
#include "SqlQueryManager.hpp"
#include "SystemLibrary.hpp"
#include "Tag.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"
#include "TagSet.hpp"
#include "WatchViewWnd.hpp"
#include "WebServer.hpp"

////////////////////////////////////////////////////////////////////////
//
// Device Configuration
//

#include <G3DevCon.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item -- Core
//

// Runtime Class

AfxImplementRuntimeClass(CCommsSystem, CSystemItem);

// Constructor

CCommsSystem::CCommsSystem(BOOL fAux)
{
	m_fAux         = fAux;

	m_Format       = 0;

	m_Major        = 0;

	m_Level	       = 1;

	m_pComms       = New CCommsManager;

	m_pTags        = New CTagManager;

	m_pPersist     = New CPersistManager;

	m_pPrograms    = New CProgramManager;

	m_pWeb         = New CWebServer;

	m_pLog         = New CDataLogger;

	m_pSecure      = New CSecurityManager;

	m_pLang        = New CLangManager;

	m_pColor       = New CColorManager;

	m_pSysLib      = New CSystemLibrary;

	m_pSql         = New CSqlQueryManager;

	m_pWatch       = New CTagSet;

	m_pControl     = New CControlManager;

	m_pDevCon      = New CDevCon;

	m_pHeadCoded   = NULL;

	m_pTailCoded   = NULL;

	m_pHeadMapping = NULL;

	m_pTailMapping = NULL;

	m_pHeadTagSet  = NULL;

	m_pTailTagSet  = NULL;

	m_pNameServer  = NULL;

	m_pDataServer  = NULL;

	m_pWatchView   = NULL;

	m_pWatch->MakeWatchList();
}

// Destructor

CCommsSystem::~CCommsSystem(void)
{
	delete m_pNameServer;

	delete m_pDataServer;
}

// Model Control

BOOL CCommsSystem::SetModel(CString const &Model)
{
	m_pDevCon->m_Model = Model.ToLower();

	return TRUE;
}

CString CCommsSystem::GetModel(void) const
{
	return m_pDevCon->m_Model.ToUpper();
}

CString CCommsSystem::GetEmulatorModel(void) const
{
	return m_pDevCon->m_pApply->GetEmulatorModel(m_pDevCon->GetDisplaySize());
}

CString CCommsSystem::GetDisplayName(void) const
{
	return m_pDevCon->m_pApply->GetDisplayName();
}

// Model Editing

BOOL CCommsSystem::SetModelSpec(CString const &Model)
{
	m_pDevCon->m_Model = Model.ToLower();

	ChangeModel();

	m_pDevCon->ApplyModelSpec();

	m_pDevCon->m_pUpdate->OnConfigUpdate('h');

	m_pDevCon->m_pSConfig->UpdateSchema();

	return TRUE;
}

CString CCommsSystem::GetModelSpec(void)
{
	CString Model = m_pDevCon->m_Model;

	if( Model.Count(L'|') == 4 ) {

		Model = Model.Left(Model.FindRev('|'));
	}

	m_pDevCon->AppendModelSpec(Model);

	return Model.ToUpper();
}

// Version Test

BOOL CCommsSystem::NeedNewSoftware(void) const
{
	return DBASE_MAJOR < m_Major;
}

// Conversion

CString CCommsSystem::GetSpecies(void) const
{
	return L"g3";
}

// UI Creation

CViewWnd * CCommsSystem::CreateView(UINT uType)
{
	if( uType == viewSystem ) {

		return New CSystemWnd;
	}

	return NULL;
}

// Server Access

CNameServer * CCommsSystem::GetNameServer(void) const
{
	return m_pNameServer;
}

CDataServer * CCommsSystem::GetDataServer(void) const
{
	return m_pDataServer;
}

// Attributes

BOOL CCommsSystem::HasBroken(void) const
{
	if( TRUE ) {

		CCodedItem *pCoded = m_pHeadCoded;

		while( pCoded ) {

			if( pCoded->IsBroken() ) {

				return TRUE;
			}

			pCoded = pCoded->m_pNext;
		}
	}

	if( TRUE ) {

		CCommsMapping *pMapping = m_pHeadMapping;

		while( pMapping ) {

			if( pMapping->IsBroken() ) {

				return TRUE;
			}

			pMapping = pMapping->m_pNext;
		}
	}

	if( TRUE ) {

		CTagSet *pTagSet = m_pHeadTagSet;

		while( pTagSet ) {

			if( pTagSet->IsBroken() ) {

				return TRUE;
			}

			pTagSet = pTagSet->m_pNext;
		}
	}

	if( TRUE ) {

		CCommsPortList *pList;

		if( m_pComms->GetPortList(pList, 0) ) {

			INDEX n = pList->GetHead();

			while( !pList->Failed(n) ) {

				CCommsPort *pPort = pList->GetItem(n);

				if( pPort->IsBroken() ) {

					return TRUE;
				}

				pList->GetNext(n);
			}
		}
	}

	return FALSE;
}

BOOL CCommsSystem::HasHardware(void) const
{
	return m_pDevCon->m_pHConfig->HasChanged();
}

BOOL CCommsSystem::HasCircular(void) const
{
	return m_pTags->HasCircular();
}

BOOL CCommsSystem::CanStepFragment(CString Code) const
{
	if( Code.StartsWith(L"WAS") ) {

		return FALSE;
	}

	if( m_pComms->CanStepFragment(Code) ) {

		return TRUE;
	}

	if( m_pTags->CanStepFragment(Code) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsSystem::HasNextAddress(void) const
{
	return !m_LastAddr.IsEmpty();
}

BOOL CCommsSystem::IsOnWatch(CString Code) const
{
	return m_pWatch->HasCode(Code);
}

// Operations

void CCommsSystem::ClearSysBlocks(void)
{
	m_pComms->ClearSysBlocks();
}

void CCommsSystem::NotifyInit(void)
{
	m_pComms->NotifyInit();
}

void CCommsSystem::Validate(BOOL fExpand)
{
	UpdateWindow();

	afxThread->SetWaitMode(TRUE);

	CString    Prev = afxThread->GetStatusText();

	CCodedTree Done;

	for( UINT uStep = 0; uStep < 11; uStep++ ) {

		DoValidate(Done, fExpand, uStep);
	}

	UpdateWatch(FALSE);

	afxThread->SetStatusText(Prev);

	afxThread->SetWaitMode(FALSE);
}

void CCommsSystem::TagCheck(UINT uTag, BOOL fQuick)
{
	UpdateWindow();

	afxThread->SetWaitMode(TRUE);

	CString    Prev = afxThread->GetStatusText();

	CCodedTree Done;

	CIndexTree Tags;

	Tags.Insert(uTag);

	for( UINT uStep = fQuick ? 5 : 0; uStep < 8; uStep++ ) {

		DoTagCheck(Done, Tags, uTag, uStep);
	}

	UpdateWatch(FALSE);

	afxThread->SetStatusText(Prev);

	afxThread->SetWaitMode(FALSE);
}

void CCommsSystem::ObjCheck(UINT uObj, BOOL fQuick)
{
	UpdateWindow();

	afxThread->SetWaitMode(TRUE);

	CString    Prev = afxThread->GetStatusText();

	CCodedTree Done;

	CIndexTree Objs;

	Objs.Insert(uObj);

	for( UINT uStep = fQuick ? 7 : 0; uStep < 8; uStep++ ) {

		DoObjCheck(Done, Objs, uObj, uStep);
	}

	afxThread->SetStatusText(Prev);

	afxThread->SetWaitMode(FALSE);
}

BOOL CCommsSystem::StepFragment(CString &Code, UINT uStep)
{
	if( Code.StartsWith(L"WAS") ) {

		return FALSE;
	}

	if( m_pComms->StepFragment(Code, uStep) ) {

		return TRUE;
	}

	if( m_pTags->StepFragment(Code, uStep) ) {

		return TRUE;
	}

	return FALSE;
}

void CCommsSystem::SetLastAddress(CString Code)
{
	if( !m_pComms->StepFragment(Code, 1) ) {

		m_LastAddr.Empty();

		return;
	}

	m_LastAddr = Code;
}

BOOL CCommsSystem::GetNextAddress(CString &Code, BOOL fInc)
{
	if( HasNextAddress() ) {

		Code = m_LastAddr;

		if( !m_pComms->StepFragment(m_LastAddr, 1) ) {

			m_LastAddr.Empty();
		}

		return TRUE;
	}

	return FALSE;
}

void CCommsSystem::Rebuild(UINT uAction)
{
	if( uAction == 0 ) {

		Validate(TRUE);
	}

	if( uAction == 1 ) {

		ClearSysBlocks();

		Rebuild();
	}

	if( uAction == 2 ) {

		ClearSysBlocks();

		NotifyInit();

		Rebuild();
	}

	if( uAction == 3 ) {

		Validate(FALSE);
	}
}

void CCommsSystem::ShowHardware(void)
{
	CSysProxy System;

	System.Bind();

	System.Navigate(m_pDevCon->m_pHConfig->GetFixedPath());
}

void CCommsSystem::UpdateHardware(void)
{
	ApplyHardware(FALSE);

	m_pDbase->ResetCategories();

	m_pNameServer->LoadLibrary();

	m_pSysLib->ReadLibrary();

	AddNavCats();

	AddResCats();

	Rebuild();

	PostConvert();
}

void CCommsSystem::ClearWatch(void)
{
	m_pWatch->Empty();
}

BOOL CCommsSystem::AddToWatch(CMetaItem *pItem)
{
	CMetaList *pList = pItem->FindMetaList();

	UINT      uCount = pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = pList->FindData(n);

		UINT             uType  = pMeta->GetType();

		if( uType == metaObject || uType == metaVirtual ) {

			CItem *pChild = pMeta->GetObject(pItem);

			if( pChild ) {

				if( pChild->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

					AddToWatch((CMetaItem *) pChild);
				}

				if( pChild->IsKindOf(AfxRuntimeClass(CCodedItem)) ) {

					CCodedItem * pCode = (CCodedItem *) pChild;

					UINT         uRefs = pCode->m_Refs.GetCount();

					if( uRefs ) {

						for( UINT r = 0; r < uRefs; r++ ) {

							CDataRef const &Ref = pCode->GetRef(r);

							AddToWatch(Ref);
						}
					}
				}
			}
		}

		if( uType == metaCollect ) {

			CItem *pChild = pMeta->GetObject(pItem);

			if( pChild ) {

				CItemList *pList = (CItemList *) pChild;

				INDEX      Index = pList->GetHead();

				while( !pList->Failed(Index) ) {

					CItem *pSlot = pList->GetItem(Index);

					if( pSlot->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

						AddToWatch((CMetaItem *) pSlot);
					}

					pList->GetNext(Index);
				}
			}
		}
	}

	return TRUE;
}

BOOL CCommsSystem::AddToWatch(CDataRef const &Ref)
{
	if( !m_pWatch->HasRef(Ref) ) {

		m_pWatch->AddRef(Ref);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsSystem::AddToWatch(CString Code)
{
	if( !m_pWatch->HasCode(Code) ) {

		m_pWatch->AddCode(Code);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsSystem::UpdateWatch(BOOL fShow)
{
	if( fShow ) {

		BOOL fVisible = FALSE;

		if( m_pWatchView ) {

			if( m_pWatchView->GetParent().IsWindowVisible() ) {

				fVisible = TRUE;
			}
		}

		if( !fVisible ) {

			afxMainWnd->SendMessage(WM_COMMAND, IDM_VIEW_WATCH);
		}
	}

	if( m_pWatchView ) {

		m_pWatchView->Update();

		return TRUE;
	}

	return FALSE;
}

// Watch Window

CWnd * CCommsSystem::GetWatchWindow(void)
{
	if( !m_pWatchView ) {

		m_pWatchView = New CWatchViewWnd(this);
	}

	return m_pWatchView;
}

// Control Project

BOOL CCommsSystem::NeedBuild(void)
{
	return m_pControl->NeedBuild();
}

BOOL CCommsSystem::PerformBuild(void)
{
	return m_pControl->PerformBuild();
}

BOOL CCommsSystem::HasControl(void)
{
	return m_pControl->HasControl();
}

// SQL Queries

BOOL CCommsSystem::CheckSqlQueries(void)
{
	return m_pSql->Check(FALSE);
}

// Conversion

void CCommsSystem::PostConvert(void)
{
	C3AllowDriverGroup(m_pDbase->GetSoftwareGroup(), GetModel());

	m_pComms->m_pRack->m_pPorts->PostConvert();

	m_pComms->m_pServices->PostConvert();

	m_pTags->PostConvert();

	m_pLog->PostConvert();

	m_pWeb->PostConvert();

	m_pControl->PostConvert();
}

// Item Naming

CString CCommsSystem::GetHumanName(void) const
{
	return CString();
}

CString CCommsSystem::GetFixedName(void) const
{
	return CString();
}

// Persistance

void CCommsSystem::Init(void)
{
	MakeServers();

	CSystemItem::Init();

	m_pControl->PostInit();

	m_pNameServer->LoadLibrary();

	ApplyHardware(TRUE);

	m_pSysLib->ReadLibrary();

	m_pComms->m_pEthernet->MakePorts();

	AddNavCats();

	AddResCats();

	m_Format = DBASE_FORMAT;

	m_Major  = DBASE_MAJOR;
}

void CCommsSystem::PostLoad(void)
{
	MakeServers();

	CSystemItem::PostLoad();

	m_pNameServer->LoadLibrary();

	ApplyHardware(FALSE);

	m_pSysLib->ReadLibrary();

	if( !m_pComms->m_pEthernet->m_pPorts->GetItemCount() ) {

		m_pComms->m_pEthernet->MakePorts();
	}

	AddNavCats();

	AddResCats();

	Rebuild();

	m_pColor->UpdateAux();

	CheckFormat();
}

void CCommsSystem::Save(CTreeFile &Tree)
{
	MakeMax(m_Major, DBASE_MAJOR);

	CSystemItem::Save(Tree);
}

// Property Save Filter

BOOL CCommsSystem::SaveProp(CString const &Tag) const
{
	if( Tag == L"SysLib" || Tag == L"Watch" ) {

		return FALSE;
	}

	return CSystemItem::SaveProp(Tag);
}

// Download Support

BOOL CCommsSystem::MakeInitData(CInitData &Init)
{
	Init.AddWord(WORD(m_Format));

	Init.AddWord(WORD(m_pDevCon->GetSoftwareGroup()));

	CSystemItem::MakeInitData(Init);

	if( C3OemFeature(L"OemSD", FALSE) ) {

		CRegKey Key = afxModule->GetApp()->GetMachineRegKey();

		CString Ver = Key.GetValue(L"Version", L"3.2");

		Init.AddWord(WORD(watoi(Ver.TokenLast('.'))));
	}
	else
		Init.AddWord(WORD(C3_BUILD));

	Init.AddWord(WORD(m_pComms->m_Handle));

	Init.AddWord(WORD(m_pTags->m_Handle));

	Init.AddWord(WORD(m_pPrograms->m_Handle));

	Init.AddWord(WORD(m_pLog->m_Handle));

	Init.AddWord(WORD(m_pSecure->m_Handle));

	Init.AddWord(WORD(m_pLang->m_Handle));

	Init.AddWord(WORD(m_pWeb->m_Handle));

	Init.AddWord(WORD(m_pSql->m_Handle));

	Init.AddWord(WORD(m_pControl->m_Handle));

	return TRUE;
}

// DLD Selection

CString CCommsSystem::GetProcessor(void) const
{
	return GetDldFolder();
}

CString CCommsSystem::GetDldFolder(void) const
{
	return L"M68k";
}

// Device Config

UINT CCommsSystem::GetSoftwareGroup(void) const
{
	return m_pDevCon->GetSoftwareGroup();
}

BOOL CCommsSystem::HasExpansion(void) const
{
	return m_pDevCon->HasExpansion();
}

BOOL CCommsSystem::GetDeviceConfig(CByteArray &Data, UINT uItem) const
{
	CDevConPart *pPart = NULL;

	switch( uItem ) {

		case 0: pPart = m_pDevCon->m_pHConfig; break;
		case 1: pPart = m_pDevCon->m_pPConfig; break;
		case 2: pPart = m_pDevCon->m_pSConfig; break;
		case 3: pPart = m_pDevCon->m_pUConfig; break;
	}

	if( pPart ) {

		return pPart->MakeInitData(Data);
	}

	return FALSE;
}

BOOL CCommsSystem::DisableDeviceConfig(UINT uItem)
{
	CDevConPart *pPart = NULL;

	switch( uItem ) {

		case 0: pPart = m_pDevCon->m_pHConfig; break;
		case 1: pPart = m_pDevCon->m_pPConfig; break;
		case 2: pPart = m_pDevCon->m_pSConfig; break;
		case 3: pPart = m_pDevCon->m_pUConfig; break;
	}

	if( pPart ) {

		pPart->m_Enable = 0;

		return TRUE;
	}

	return FALSE;
}

// System Data

void CCommsSystem::AddNavCats(void)
{
	AddNavCatHead();

	AddNavCatTail();
}

void CCommsSystem::AddResCats(void)
{
	AddResCatHead();

	AddResCatTail();
}

void CCommsSystem::AddNavCatHead(void)
{
	CDatabase *pDbase = GetDatabase();

	UINT       uGroup = GetSoftwareGroup();

	if( uGroup >= SW_GROUP_1 ) {

		pDbase->AddNavCategory(IDS_DEVICE_CONFIG,
				       L"DevCon",
				       0x21000015,
				       0x20000015,
				       L"DevCon"
		);

		if( uGroup >= SW_GROUP_2 ) {

			pDbase->AddNavCategory(IDS_COMMS_CAPTION,
					       L"Comms",
					       0x21000000,
					       0x20000000,
					       L"Tags|Programs|DevCon|SysLib|Comms"
			);

			pDbase->AddNavCategory(IDS_DATA_TAGS,
					       L"Tags",
					       0x21000001,
					       0x20000001,
					       L"Tags|Programs|DevCon|SysLib"
			);
		}
	}
}

void CCommsSystem::AddNavCatTail(void)
{
	CDatabase *pDbase = GetDatabase();

	UINT       uGroup = GetSoftwareGroup();

	if( uGroup >= SW_GROUP_2 ) {

		pDbase->AddNavCategory(IDS_PROGRAMS,
				       L"Programs",
				       0x21000003,
				       0x20000003,
				       L"Tags|Programs|DevCon|SysLib"
		);

		if( uGroup >= SW_GROUP_3B ) {

			pDbase->AddNavCategory(IDS_WEB_SERVER,
					       L"Web",
					       0x21000005,
					       0x20000005,
					       L"Tags|Programs|DevCon|SysLib"
			);
		}

		if( uGroup >= SW_GROUP_3A ) {

			pDbase->AddNavCategory(IDS_DATA_LOGGER,
					       L"Log",
					       0x21000004,
					       0x20000004,
					       L"Tags|Programs|DevCon|SysLib"
			);

			pDbase->AddNavCategory(IDS_SECURITY,
					       L"Secure",
					       0x21000006,
					       0x20000006,
					       L"Tags|Programs|DevCon|SysLib"
			);
		}

		if( uGroup >= SW_GROUP_3B ) {

			pDbase->AddNavCategory(IDS_SQL_QUERIES,
					       L"Sql",
					       0x21000014,
					       0x20000014,
					       L"Tags"
			);
		}
	}
}

void CCommsSystem::AddResCatHead(void)
{
}

void CCommsSystem::AddResCatTail(void)
{
	CDatabase *pDbase = GetDatabase();

	pDbase->AddResCategory(IDS_DEVICES,
				L"Comms",
				0x21000000,
				0x20000000
	);

	pDbase->AddResCategory(IDS_DATA_TAGS,
				L"Tags",
				0x21000001,
				0x20000001
	);

	pDbase->AddResCategory(IDS_PROGRAMS,
				L"Programs",
				0x21000003,
				0x20000003
	);

	pDbase->AddResCategory(IDS_DEVICE_PERSONALITY,
			       L"DevCon",
			       0x21000015,
			       0x20000015
	);

	pDbase->AddResCategory(IDS_SYSTEM,
				L"SysLib",
				0x2100000C,
				0x2000000C
	);
}

// Validation

BOOL CCommsSystem::DoValidate(CCodedTree &Done, BOOL fExpand, UINT uStep)
{
	if( uStep == 0 ) {

		m_pDbase->SetRecomp(FALSE);

		m_pDbase->SetCircle(FALSE);

		return TRUE;
	}

	if( uStep == 1 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING_COMMS));

		m_pComms->Validate(fExpand);

		return TRUE;
	}

	if( uStep == 2 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING));

		m_pComms->CheckMapBlocks();

		return TRUE;
	}

	if( uStep == 3 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING_TAGS));

		m_pTags->Validate(Done, fExpand);

		return TRUE;
	}

	if( uStep == 5 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING_TAG));

		CTagSet *pTagSet = m_pHeadTagSet;

		while( pTagSet ) {

			pTagSet->Validate(fExpand);

			pTagSet = pTagSet->m_pNext;
		}

		return TRUE;
	}

	if( uStep == 7 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING_CODE));

		CCodedItem *pCoded = m_pHeadCoded;

		while( pCoded ) {

			if( Done.Failed(Done.Find(pCoded)) ) {

				pCoded->Recompile(fExpand);
			}

			pCoded = pCoded->m_pNext;
		}

		return TRUE;
	}

	if( uStep == 8 ) {

		afxThread->SetStatusText(CString(IDS_CHECKING_PORT));

		UINT n = 0;

		CCommsPortList *pList;

		while( m_pComms->GetPortList(pList, n++) ) {

			pList->CheckConflicts();
		}

		return TRUE;
	}

	if( uStep == 9 ) {

		afxThread->SetStatusText(CString(IDS_CHECKING_CONTROL));

		m_pControl->Validate();

		return TRUE;
	}

	if( uStep == 10 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING_SQL));

		m_pSql->Validate();

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsSystem::DoTagCheck(CCodedTree &Done, CIndexTree &Tags, UINT uTag, UINT uStep)
{
	if( uStep == 0 ) {

		afxThread->SetStatusText(CString(IDS_UPDATING_TAG));

		m_pTags->TagCheck(Done, Tags, uTag);

		return TRUE;
	}

	if( uStep == 1 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING));

		m_pComms->CheckMapBlocks();

		return TRUE;
	}

	if( uStep == 5 ) {

		afxThread->SetStatusText(CString(IDS_UPDATING_TAG_SETS));

		CTagSet *pTagSet = m_pHeadTagSet;

		while( pTagSet ) {

			pTagSet->TagCheck(uTag);

			pTagSet = pTagSet->m_pNext;
		}

		return TRUE;
	}

	if( uStep == 6 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING));

		CCommsMapping *pMapping = m_pHeadMapping;

		while( pMapping ) {

			pMapping->TagCheck(uTag);

			pMapping = pMapping->m_pNext;
		}

		return TRUE;
	}

	if( uStep == 7 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING_CODE));

		CCodedItem *pCoded = m_pHeadCoded;

		while( pCoded ) {

			if( Done.Failed(Done.Find(pCoded)) ) {

				UINT uCount = pCoded->GetRefCount();

				for( UINT r = 0; r < uCount; r++ ) {

					CDataRef const &Ref = pCoded->GetRef(r);

					if( Ref.t.m_IsTag ) {

						if( Tags.Find(Ref.t.m_Index) ) {

							pCoded->Recompile(TRUE);

							break;
						}
					}
				}
			}

			pCoded = pCoded->m_pNext;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsSystem::DoObjCheck(CCodedTree &Done, CIndexTree &Objs, UINT uObj, UINT uStep)
{
	if( uStep == 7 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING_CODE));

		CCodedItem *pCoded = m_pHeadCoded;

		while( pCoded ) {

			if( Done.Failed(Done.Find(pCoded)) ) {

				UINT uCount = pCoded->GetRefCount();

				for( UINT r = 0; r < uCount; r++ ) {

					CDataRef const &Ref = pCoded->GetRef(r);

					if( !Ref.t.m_IsTag ) {

						if( !Ref.b.m_Block ) {

							if( Objs.Find(Ref.t.m_Index) ) {

								pCoded->Recompile(TRUE);

								break;
							}
						}
					}
				}
			}

			pCoded = pCoded->m_pNext;
		}

		return TRUE;
	}

	return FALSE;
}

// Server Creation

void CCommsSystem::MakeServers(void)
{
	m_pNameServer = New CNameServer(this);

	m_pDataServer = New CDataServer(this);
}

// System Hooks

void CCommsSystem::ApplyHardware(BOOL fInit)
{
}

void CCommsSystem::ChangeModel(void)
{
}

// Meta Data

void CCommsSystem::AddMetaData(void)
{
	CSystemItem::AddMetaData();

	Meta_AddInteger(Format);
	Meta_AddInteger(Major);
	Meta_AddString(LastAddr);
	Meta_AddObject(Comms);
	Meta_AddObject(Tags);
	Meta_AddObject(Persist);
	Meta_AddObject(Programs);
	Meta_AddObject(Web);
	Meta_AddObject(Log);
	Meta_AddObject(Secure);
	Meta_AddObject(Lang);
	Meta_AddObject(Color);
	Meta_AddObject(SysLib);
	Meta_AddObject(Watch);
	Meta_AddObject(Control);
	Meta_AddObject(Sql);
	Meta_AddObject(DevCon);

	Meta_SetName((IDS_SYSTEM));
}

// Implementation

BOOL CCommsSystem::CheckFormat(void)
{
	if( m_Format != DBASE_FORMAT ) {

		Rebuild(2);

		SetDirty();

		m_Format = DBASE_FORMAT;

		return TRUE;
	}

	return FALSE;
}

void CCommsSystem::UpdateWindow(void)
{
	afxMainWnd->UpdateWindow();
}

void CCommsSystem::Rebuild(void)
{
	CNameServer *pName = GetNameServer();

	pName->RebuildDirect(TRUE);

	Validate(FALSE);

	pName->RebuildDirect(FALSE);
}

// End of File
