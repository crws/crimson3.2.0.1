
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_MailContacts_HPP

#define INCLUDE_MailContacts_HPP

//////////////////////////////////////////////////////////////////////////
//
// Mail Contacts
//

class CMailContacts : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMailContacts(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CItemIndexList * m_pList;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
