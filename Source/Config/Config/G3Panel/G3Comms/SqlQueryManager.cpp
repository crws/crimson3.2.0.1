
#include "Intern.hpp"

#include "SqlQueryManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "SqlQuery.hpp"
#include "SqlQueryList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Query Manager
//

// Dynamic Class

AfxImplementDynamicClass(CSqlQueryManager, CCodedHost);

// Constructor

CSqlQueryManager::CSqlQueryManager(void)
{
	m_pQueries = New CSqlQueryList;

	m_Enable   = 0;

	m_Handle   = HANDLE_NONE;

	m_pServer  = NULL;

	m_Port     = 1433;

	m_TlsMode  = 0;

	m_Validate = 0;

	m_Connect  = 0;
	}

// UI Creation

CViewWnd * CSqlQueryManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CSqlQueryManagerTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	return CUIItem::CreateView(uType);
	}

// Initial Values

void CSqlQueryManager::SetInitValues(void)
{
	SetInitial(L"Server", m_pServer, 0);
	}

// Type Access

BOOL CSqlQueryManager::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"User" || Tag == L"Pass" || Tag == L"DatabaseName" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// UI Loading

BOOL CSqlQueryManager::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CUIStdPage(AfxThisClass(), 1));

	return FALSE;
	}

// UI Update

void CSqlQueryManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CUIItem::OnUIChange(pHost, pItem, Tag);

	if( Tag.IsEmpty() || Tag == L"Enable" || Tag == L"Connect" ) {

		DoEnables(pHost);
		}
	}

void CSqlQueryManager::Validate(void)
{
	for( INDEX i = m_pQueries->GetHead(); !m_pQueries->Failed(i); m_pQueries->GetNext(i) ) {

		CSqlQuery * pQuery = m_pQueries->GetItem(i);

		pQuery->Validate(TRUE);
		}
	}

BOOL CSqlQueryManager::Check(BOOL fList)
{
	if( m_Enable == 0 ) {

		return TRUE;
		}

	BOOL fOk = TRUE;

	CStringArray Errors;

	if( m_DatabaseName.IsEmpty() ) {

		CString Error;

		Error += GetHumanPath();

		Error += L": ";

		Error += IDS("The target database name is empty\n");

		Error += GetFixedPath();

		Errors.Append(Error);

		fOk = FALSE;
		}

	if( !CheckQueries(fList, Errors) ) {

		fOk = FALSE;
		}

	if( fList && Errors.GetCount() != 0 ) {

		CSysProxy Proxy;

		Proxy.Bind();

		Proxy.SetFindList( IDS("SQL Errors"),
			           Errors,
				   FALSE
				   );
		}

	return fOk;
	}

BOOL CSqlQueryManager::CheckQueries(BOOL fList, CStringArray &Errors)
{
	BOOL fOk = TRUE;

	for( INDEX i = m_pQueries->GetHead(); !m_pQueries->Failed(i); m_pQueries->GetNext(i) ) {

		CSqlQuery * pQuery = m_pQueries->GetItem(i);

		if( !pQuery->Check(Errors) ) {

			fOk = FALSE;
			}
		}

	return fOk;
	}

// Query Helpers

CString CSqlQueryManager::FindUnusedName(void)
{
	for( UINT n = 1;; n++ ) {

		CPrintf Name("Query%u", n);

		if( !m_pQueries->FindByName(Name) ) {

			return Name;
			}
		}

	return L"";
	}

// Item Naming

CString CSqlQueryManager::GetHumanName(void) const
{
	return CString(IDS_SQL_QUERIES);
	}

// Download Support

BOOL CSqlQueryManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SQL);

	CMetaItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Enable));

	Init.AddItem(itemVirtual, m_pServer);

	Init.AddWord(WORD(m_Port));

	Init.AddByte(BYTE(m_TlsMode));

	Init.AddText(m_User);

	Init.AddText(m_Pass);

	Init.AddText(m_DatabaseName);

	Init.AddItem(itemSimple, m_pQueries);

	Init.AddByte(BYTE(m_Connect));

	Init.AddText(m_Instance);

	return TRUE;
	}

// Meta Data Creation

void CSqlQueryManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddInteger(Enable);
	Meta_AddVirtual(Server);
	Meta_AddInteger(Port);
	Meta_AddInteger(TlsMode);
	Meta_AddInteger(Connect);
	Meta_AddString (User);
	Meta_AddString (Pass);
	Meta_AddString (DatabaseName);
	Meta_AddCollect(Queries);
	Meta_AddInteger(Validate);
	Meta_AddString (Instance);
	
	Meta_SetName((IDS_SQL_QUERIES));
	}

// UI Update

void CSqlQueryManager::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_Enable != 0;

	pHost->EnableUI(this, "Server",       fEnable);
	pHost->EnableUI(this, "Connect",      fEnable);
	pHost->EnableUI(this, "Port",         fEnable && m_Connect == 0);
	pHost->EnableUI(this, "Instance",     fEnable && m_Connect == 1);
	pHost->EnableUI(this, "TlsMode",      fEnable);
	pHost->EnableUI(this, "User",         fEnable);
	pHost->EnableUI(this, "Pass",         fEnable);
	pHost->EnableUI(this, "DatabaseName", fEnable);
	pHost->EnableUI(this, "Queries",      fEnable);
	}

// End of File
