
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextMapAddress_HPP

#define INCLUDE_UITextMapAddress_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsMapBlock;

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Mapping Block Address Selection
//

class CUITextMapAddress : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextMapAddress(void);

	protected:
		// Data Members
		CCommsMapBlock * m_pBlock;
		ICommsDriver   * m_pDriver;
		CItem          * m_pDriverConfig;
		CItem	       * m_pDeviceConfig;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnExpand(CWnd &Wnd);

		// Implementation
		void FindBinding(void);
		void FindDriver(void);
	};

// End of File

#endif
