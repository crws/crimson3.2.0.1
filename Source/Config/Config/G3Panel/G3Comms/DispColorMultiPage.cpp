
#include "Intern.hpp"

#include "DispColorMultiPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispColorMulti.hpp"
#include "DispColorMultiEntry.hpp"
#include "DispColorMultiList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Page
//

// Runtime Class

AfxImplementRuntimeClass(CDispColorMultiPage, CUIPage);

// Constructor

CDispColorMultiPage::CDispColorMultiPage(CDispColorMulti *pColor)
{
	m_pColor = pColor;
	}

// Operations

BOOL CDispColorMultiPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_COLOR_CONTROL), 1);

	pView->AddUI(pItem, L"root", L"Count");

	pView->AddUI(pItem, L"root", L"Default");

	pView->AddUI(pItem, L"root", L"Range");

	pView->EndGroup(TRUE);

	pView->StartTable(CString(IDS_COLOR_STATES), 2);

	pView->AddColHead(CString(IDS_DATA));

	pView->AddColHead(CString(IDS_COLORS));

	UINT c = m_pColor->m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CItem *pItem = m_pColor->m_pList->GetItem(n);

		pView->AddRowHead(CPrintf(L"%u", 1+n));

		pView->AddUI(pItem, L"root", L"Data");

		pView->AddUI(pItem, L"root", L"Color");
		}

	pView->EndTable();

	pView->StartGroup(CString(IDS_COLOR_COMMANDS), 1);

	pView->AddButton( CString(IDS_SYNC_STATES),
			  CString(IDS_COPY_STATE_DATA),
			  L"ButtonSync"
			  );

	pView->AddButton( CString(IDS_EXPORT_STATES),
			  CString(IDS_EXPORT_STATE_DATA),
			  L"ButtonExport"
			  );

	pView->AddButton( CString(IDS_IMPORT_STATES),
			  CString(IDS_IMPORT_STATE_DATA),
			  L"ButtonImport"
			  );

	pView->EndGroup(TRUE);

	return TRUE;
	}

// End of File
