
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CascadedItem_HPP

#define INCLUDE_CascadedItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CascadedRackItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cascaded Rack Item
//

class DLLAPI CCascadedItem : public CCascadedRackItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCascadedItem(void);

		// Item Naming
		CString GetHumanName(void) const;

	protected:
		// Path Config
		DWORD GetRackPath(UINT uRack) const;
		DWORD GetSlotPath(UINT uRack, UINT uSlot) const;
	};

// End of File

#endif
