
#include "Intern.hpp"

#include "SqlRecord.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsSystem.hpp"
#include "NameServer.hpp"
#include "SqlColumn.hpp"
#include "SqlRecordList.hpp"
#include "SqlRecordViewWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Record
//

// Dynamic Class

AfxImplementDynamicClass(CSqlRecord, CCodedHost);

// Constructors

CSqlRecord::CSqlRecord(void)
{
	m_Row    = 0;

	m_Col    = 0;

	m_pValue = NULL;
	}

CSqlRecord::CSqlRecord(UINT uRow, UINT uCol)
{
	m_Row    = uRow;

	m_Col    = uCol;

	m_pValue = NULL;
	}

// UI Loading

CViewWnd * CSqlRecord::CreateView(UINT uType)
{
	return New CSqlRecordViewWnd;
	}

// Download Support

BOOL CSqlRecord::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);

	return TRUE;
	}

// Type Access

BOOL CSqlRecord::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Value" ) {

		return GetTypeData(Type);
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

BOOL CSqlRecord::GetTypeData(CTypeDef &Type)
{
	CSqlColumn *pColumn = (CSqlColumn *) GetParent(AfxRuntimeClass(CSqlColumn));

	Type.m_Type  = pColumn->GetC3Type();

	Type.m_Flags = flagTagRef | flagWritable;

	return TRUE;
	}

// Tag Mapping

BOOL CSqlRecord::SetMapping(CError &Error, CString const &Text)
{
	if( InitCoded(NULL, L"Value", m_pValue, Text) ) {

		CDataRef &Ref = (CDataRef &) m_pValue->m_Refs[0];

		UINT uPos = Text.Find('[');

		if( uPos < NOTHING ) {

			Ref.x.m_Array = watoi(Text.Mid(uPos+1));
			}

		SendUpdate();

		return TRUE;
		}

	return FALSE;
	}

void CSqlRecord::ClearMapping(void)
{
	KillCoded(NULL, L"Value", m_pValue);

	SendUpdate();
	}

BOOL CSqlRecord::IsMapped(void)
{
	return m_pValue && m_pValue->m_Refs.GetCount() > 0;
	}

void CSqlRecord::Validate(BOOL fExpand)
{
	if( IsMapped() ) {

		CString Source = GetMapSource(fExpand);

		CError  Error  = CError(FALSE);

		CRange  Range;

		if( Source.StartsWith(L"WAS ") ) {

			Source = Source.Mid(4);

			if( SetMapping(Error, Source) ) {

				SendUpdate();
				}
			else {
				m_pDbase->SetRecomp();
				}

			return;
			}

		if( !SetMapping(Error, Source) ) {

			Source = L"WAS " + Source;

			SetMapping(Error, Source);

			m_pDbase->SetRecomp();

			SendUpdate();
			}
		}
	}

void CSqlRecord::SendUpdate(void)
{
	CSystemWnd *pWnd = (CSystemWnd *) afxMainWnd->GetDlgItemPtr(IDVIEW);

	pWnd->ItemUpdated(0, this, updateRename);
	}

// Tree Helpers

CString CSqlRecord::GetTreeLabel(void)
{
	CSqlRecordList *pList = (CSqlRecordList *) GetParent(AfxRuntimeClass(CSqlRecordList));

	UINT uPos = 0;

	if( pList ) {

		uPos = pList->GetRecordPos(this) + 1;
		}

	CPrintf Label("Row_%u", uPos);

	if( m_pValue && !m_pValue->m_Refs.IsEmpty() ) {

		Label += L" \x0BB ";

		INameServer *pName = GetNameServer(FALSE);

		CString Text;

		CStringArray Using;

		C3ExpandSource(m_pValue->m_Source.GetPointer(), pName, Using, Text);

		Label += Text;
		}

	return Label;
	}

// Item Naming

CString CSqlRecord::GetHumanName(void) const
{
	return CPrintf("Row_%u", m_Row + 1);
	}

// Meta Data Creation

void CSqlRecord::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Row);
	Meta_AddInteger(Col);
	Meta_AddVirtual(Value);
	}

// Implementation

CString CSqlRecord::GetMapSource(BOOL fExpand)
{
	if( IsMapped() ) {

		CString Text;

		INameServer *pName = fExpand ? GetNameServer(fExpand) : NULL;

		CStringArray Using;

		C3ExpandSource(m_pValue->m_Source.GetPointer(), pName, Using, Text);

		return Text;
		}

	return L"";
	}

INameServer * CSqlRecord::GetNameServer(BOOL fExpand)
{
	CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	CNameServer  *pName   = pSystem->GetNameServer();

	pName->ResetServer();

	return pName;
	}

// End of File
