
#include "Intern.hpp"

#include "CommsPortMPI.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MPI Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortMPI, CCommsPort);

// Constructor

CCommsPortMPI::CCommsPortMPI(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding  = bindMPI;

	m_ThisDrop = 7;
	}

// Attributes

UINT CCommsPortMPI::GetPageType(void) const
{
	return m_pDriver ? 1 : 0;
	}

// Download Support

BOOL CCommsPortMPI::MakeInitData(CInitData &Init)
{
	Init.AddByte(9);

	CCommsPort::MakeInitData(Init);

	if( m_pDriver ) {

		Init.AddByte(BYTE(m_ThisDrop));
		}

	return TRUE;
	}

// Meta Data Creation

void CCommsPortMPI::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_AddInteger(ThisDrop);

	Meta_SetName((IDS_MPI_PORT));
	}

// End of File
