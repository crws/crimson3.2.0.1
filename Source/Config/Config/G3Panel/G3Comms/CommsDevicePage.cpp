
#include "Intern.hpp"

#include "CommsDevicePage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Device Page
//

// Runtime Class

AfxImplementRuntimeClass(CCommsDevicePage, CUIStdPage);

// Constructor

CCommsDevicePage::CCommsDevicePage(CCommsDevice *pDevice)
{
	m_Class   = AfxRuntimeClass(CCommsDevice);

	m_pDevice = pDevice;
	}

// Operations

BOOL CCommsDevicePage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	LoadBasePage(pView);

	LoadDeviceConfig(pView);

	LoadMasterConfig(pView);

	LoadButtons(pView);

	pView->NoRecycle();

	return TRUE;
	}

// Implementation

BOOL CCommsDevicePage::LoadBasePage(IUICreate *pView)
{
	CUIPage *pPage = New CUIStdPage(m_Class, 1);

	pPage->LoadIntoView(pView, m_pDevice);

	delete pPage;

	return TRUE;
	}

BOOL CCommsDevicePage::LoadDeviceConfig(IUICreate *pView)
{
	if( m_pDevice->m_pConfig ) {

		CUIPageList List;

		m_pDevice->m_pConfig->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, m_pDevice->m_pConfig);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCommsDevicePage::LoadMasterConfig(IUICreate *pView)
{
	if( m_pDevice->GetDriver()->GetType() == driverMaster ) {

		CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pDevice), 2);

		pPage->LoadIntoView(pView, m_pDevice);

		delete pPage;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommsDevicePage::LoadButtons(IUICreate *pView)
{
	BOOL fMap = m_pDevice->AllowMapping();

	BOOL fTag = m_pDevice->AllowTagInit();

	if( fMap || fTag ) {

		pView->StartGroup( CString(IDS_DEVICE_COMMANDS),
				   1
				   );

		pView->AddButton(  CString(IDS_COMMS_DEV_DELETE),
				   CString(IDS_COMMS_TIP_DELETE),
				   L"ButtonDelete"
				   );

		if( fMap ) {

			pView->AddButton( CString(IDS_COMMS_DEV_GATE),
					  CString(IDS_COMMS_TIP_GATE),
					  L"ButtonAddMap"
					  );
			}

		if( fTag ) {

			pView->AddButton( CString(IDS_COMMS_DEV_TAGS),
					  CString(IDS_COMMS_TIP_TAGS),
					  L"ButtonAddTags"
					  );
			}

		pView->EndGroup(FALSE);

		return TRUE;
		}

	return FALSE;
	}

// End of File
