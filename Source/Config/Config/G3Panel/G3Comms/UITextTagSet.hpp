
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextTagSet_HPP

#define INCLUDE_UITextTagSet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Tag Set
//

class CUITextTagSet : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextTagSet(void);

	protected:
		// Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

// End of File

#endif
