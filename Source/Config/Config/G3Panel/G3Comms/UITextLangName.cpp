
#include "Intern.hpp"

#include "UITextLangName.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Language Name
//

// Dynamic Class

AfxImplementDynamicClass(CUITextLangName, CUITextEnum);

// Constructor

CUITextLangName::CUITextLangName(void)
{
	}

// Overridables

void CUITextLangName::OnBind(void)
{
	CUITextEnum::OnBind();

	AddData();
	}

// Implementation

void CUITextLangName::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

void CUITextLangName::AddData(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	CArray <UINT> List;

	UINT c = pLang->EnumLanguages(List);

	for( UINT n = 0; n < c; n++ ) {

		UINT    uCode = List[n];

		CString Name  = pLang->GetNameFromCode(uCode);

		AddData(uCode, Name);
		}
	}

// End of File
