
#include "Intern.hpp"

#include "WebServer.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "HttpServerOptions.hpp"
#include "WebPageList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Server
//

// Dynamic Class

AfxImplementDynamicClass(CWebServer, CCodedHost);

// Constructor

CWebServer::CWebServer(void)
{
	m_Handle                = HANDLE_NONE;
	m_Enable                = TRUE;
	m_DbCrc	                = 0;
	m_DbTime                = 0;
	m_pPages                = New CWebPageList;
	m_pOpts                 = New CHttpServerOptions;
	m_pTitle                = NULL;
	m_pHeader               = NULL;
	m_pHome                 = NULL;
	m_CustHome              = FALSE;
	m_CustIcon              = FALSE;
	m_CustLogon             = FALSE;
	m_CustCss               = FALSE;
	m_CustJs                = FALSE;
	m_RemoteView            = TRUE;
	m_RemoteCtrl            = FALSE;
	m_RemoteZoom            = 0;
	m_RemoteFact		= 2;
	m_RemoteBits		= 16;
	m_RemoteSec             = allowDefault;
	m_LogsView              = FALSE;
	m_LogsBatches           = FALSE;
	m_LogsSec               = allowDefault;
	m_UserSite              = FALSE;
	m_UserMenu              = TRUE;
	m_UserDelay             = 5;
	m_UserRoot              = FALSE;
	m_UserSec               = allowDefault;
	m_SystemPages		= 0;
	m_SystemConsole		= 0;
	m_SystemCapture		= 0;
	m_SystemJump	        = 1;
	m_SystemSec             = allowDefault;
	m_Source                = 1;
	m_Local                 = 0;
	m_pUser                 = NULL;
	m_pReal	                = NULL;
	m_pPass                 = NULL;
	m_Restrict              = 0;
	m_SecMask               = 0;
	m_SecAddr               = 0;
	m_pPort	                = NULL;
	m_SameOrigin            = TRUE;
	m_pOpts->m_Tls          = FALSE;
	m_pOpts->m_Port         = 80;
	m_pOpts->m_HttpRedirect = FALSE;
}

// Initial Values

void CWebServer::SetInitValues(void)
{
}

// UI Management

CViewWnd * CWebServer::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CWebServerNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	return CUIItem::CreateView(uType);
}

// UI Update

void CWebServer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
		}

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			if( m_Enable ) {

				pHost->EnableUI(m_pOpts, TRUE);

				m_pOpts->OnUIChange(pHost, m_pOpts, L"");
			}
			else
				pHost->EnableUI(m_pOpts, FALSE);

			DoEnables(pHost);
		}

		if( Tag == "RemoteView" || Tag == "LogsView" || Tag == "UserSite" || Tag == "SystemPages" ) {

			DoEnables(pHost);
		}

		if( Tag == "Source" || Tag == "Restrict" ) {

			DoEnables(pHost);
		}
	}

	if( pItem == m_pOpts ) {

		if( Tag == "AuthMethod" || Tag == "Advanced" ) {

			DoEnables(pHost);
		}
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Operations

void CWebServer::PostConvert(void)
{
	if( m_pDbase->GetSoftwareGroup() <= SW_GROUP_3A ) {

		m_Enable = FALSE;

		m_pPages->DeleteAllItems(TRUE);
	}

	m_pOpts->PostConvert();
}

// Type Access

BOOL CWebServer::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Title" || Tag == L"Header" || Tag == L"Home" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"User" || Tag == L"Real" || Tag == L"Pass" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"Port" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
	}

	return FALSE;
}

// Item Naming

CString CWebServer::GetHumanName(void) const
{
	return CString(IDS_WEB_SERVER);
}

// Persistance

void CWebServer::Init(void)
{
	CCodedHost::Init();

	m_pOpts->m_Tls          = TRUE;
	m_pOpts->m_Port         = 443;
	m_pOpts->m_HttpRedirect = 1;
}

void CWebServer::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"ReadLogs" ) Name = L"LogsView";
		if( Name == L"Delay" ) Name = L"UserDelay";
		if( Name == L"Redirect" ) Name = L"UserRoot";
		if( Name == L"UserHome" ) Name = L"CustHome";

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
		}

		if( Name == L"Throttle" || Name == L"UserIdle" ) {

			Tree.GetValueAsInteger();

			continue;
		}

		if( Name == L"Logon" ) {

			switch( Tree.GetValueAsInteger() ) {

				case 0:
					m_pOpts->m_AuthMethod = 0;
					break;

				case 1:
					m_pOpts->m_AuthMethod = 2;
					m_pOpts->m_HttpMethod = 1;
					break;

				case 2:
					m_pOpts->m_AuthMethod = 2;
					m_pOpts->m_HttpMethod = 2;
					break;
			}

			m_pOpts->m_Advanced = 1;

			m_RemoteCtrl        = !!m_RemoteCtrl;

			continue;
		}
	}

	if( m_pPort && m_pPort->IsConst() ) {

		m_pOpts->m_Port = m_pPort->Execute(typeInteger, NULL);

		m_pPort->Kill();

		delete m_pPort;

		m_pPort = NULL;
	}

	RegisterHandle();
}

// Download Support

void CWebServer::PrepareData(void)
{
	CInitData Temp;

	Temp.AddItem(itemSimple, m_pPages);
	Temp.AddItem(itemSimple, m_pOpts);

	Temp.AddItem(itemVirtual, m_pTitle);
	Temp.AddItem(itemVirtual, m_pHeader);
	Temp.AddItem(itemVirtual, m_pHome);

	Temp.AddByte(BYTE(m_CustHome));
	Temp.AddByte(BYTE(m_CustIcon));
	Temp.AddByte(BYTE(m_CustLogon));
	Temp.AddByte(BYTE(m_CustCss));
	Temp.AddByte(BYTE(m_CustJs));

	Temp.AddByte(BYTE(m_RemoteView));
	Temp.AddByte(BYTE(m_RemoteCtrl));
	Temp.AddByte(BYTE(m_RemoteZoom));
	Temp.AddByte(BYTE(m_RemoteFact));
	Temp.AddByte(BYTE(m_RemoteBits));
	Temp.AddLong(LONG(m_RemoteSec));

	Temp.AddByte(BYTE(m_LogsView));
	Temp.AddByte(BYTE(m_LogsBatches));
	Temp.AddLong(LONG(m_LogsSec));

	Temp.AddByte(BYTE(m_UserSite));
	Temp.AddByte(BYTE(m_UserMenu));
	Temp.AddByte(BYTE(m_UserRoot));
	Temp.AddWord(WORD(m_UserDelay));
	Temp.AddLong(LONG(m_UserSec));

	Temp.AddByte(BYTE(m_Source));

	if( !m_Source ) {

		Temp.AddItem(itemEncrypt, m_pUser);
		Temp.AddItem(itemEncrypt, m_pReal);
		Temp.AddItem(itemEncrypt, m_pPass);
	}

	Temp.AddByte(BYTE(m_Restrict));
	Temp.AddLong(LONG(m_SecMask));
	Temp.AddLong(LONG(m_SecAddr));

	Temp.AddItem(itemVirtual, m_pPort);

	Temp.AddByte(BYTE(m_SameOrigin));

	DWORD Crc = Temp.GetCRC();

	if( m_DbCrc != Crc ) {

		m_DbTime = LONG(time(NULL));

		m_DbCrc  = Crc;
	}
}

BOOL CWebServer::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_WEB_SERVER);

	CMetaItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Enable));

	if( m_Enable ) {

		Init.AddLong(m_DbTime);

		Init.AddItem(itemSimple, m_pPages);
		Init.AddItem(itemSimple, m_pOpts);

		Init.AddItem(itemVirtual, m_pTitle);
		Init.AddItem(itemVirtual, m_pHeader);
		Init.AddItem(itemVirtual, m_pHome);

		Init.AddByte(BYTE(m_CustHome));
		Init.AddByte(BYTE(m_CustIcon));
		Init.AddByte(BYTE(m_CustLogon));
		Init.AddByte(BYTE(m_CustCss));
		Init.AddByte(BYTE(m_CustJs));

		Init.AddByte(BYTE(m_RemoteView));
		Init.AddByte(BYTE(m_RemoteCtrl));
		Init.AddByte(BYTE(m_RemoteZoom));
		Init.AddByte(BYTE(m_RemoteFact));
		Init.AddByte(BYTE(m_RemoteBits));
		Init.AddLong(LONG(m_RemoteSec));

		Init.AddByte(BYTE(m_LogsView));
		Init.AddByte(BYTE(m_LogsBatches));
		Init.AddLong(LONG(m_LogsSec));

		Init.AddByte(BYTE(m_UserSite));
		Init.AddByte(BYTE(m_UserMenu));
		Init.AddByte(BYTE(m_UserRoot));
		Init.AddWord(WORD(m_UserDelay));
		Init.AddLong(LONG(m_UserSec));

		Init.AddByte(BYTE(m_SystemPages));
		Init.AddByte(BYTE(m_SystemConsole));
		Init.AddByte(BYTE(m_SystemCapture));
		Init.AddByte(BYTE(m_SystemJump));
		Init.AddLong(LONG(m_SystemSec));

		Init.AddByte(BYTE(m_Source));

		if( !m_Source ) {

			Init.AddItem(itemEncrypt, m_pUser);
			Init.AddItem(itemEncrypt, m_pReal);
			Init.AddItem(itemEncrypt, m_pPass);
		}
		else {
			Init.AddByte(BYTE(m_Local));
		}

		Init.AddByte(BYTE(m_Restrict));
		Init.AddLong(LONG(m_SecMask));
		Init.AddLong(LONG(m_SecAddr));

		Init.AddItem(itemVirtual, m_pPort);

		Init.AddByte(BYTE(m_SameOrigin));
	}

	return TRUE;
}

// Meta Data Creation

void CWebServer::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(Pages);
	Meta_AddInteger(Handle);
	Meta_AddInteger(Enable);
	Meta_AddInteger(DbCrc);
	Meta_AddInteger(DbTime);
	Meta_AddObject(Opts);
	Meta_AddVirtual(Title);
	Meta_AddVirtual(Header);
	Meta_AddVirtual(Home);
	Meta_AddInteger(CustHome);
	Meta_AddInteger(CustIcon);
	Meta_AddInteger(CustLogon);
	Meta_AddInteger(CustCss);
	Meta_AddInteger(CustJs);
	Meta_AddInteger(RemoteView);
	Meta_AddInteger(RemoteCtrl);
	Meta_AddInteger(RemoteZoom);
	Meta_AddInteger(RemoteFact);
	Meta_AddInteger(RemoteBits);
	Meta_AddInteger(RemoteSec);
	Meta_AddInteger(LogsView);
	Meta_AddInteger(LogsBatches);
	Meta_AddInteger(LogsSec);
	Meta_AddInteger(UserSite);
	Meta_AddInteger(UserMenu);
	Meta_AddInteger(UserDelay);
	Meta_AddInteger(UserSec);
	Meta_AddInteger(UserRoot);
	Meta_AddInteger(SystemPages);
	Meta_AddInteger(SystemConsole);
	Meta_AddInteger(SystemCapture);
	Meta_AddInteger(SystemJump);
	Meta_AddInteger(SystemSec);
	Meta_AddInteger(Source);
	Meta_AddInteger(Local);
	Meta_AddVirtual(User);
	Meta_AddVirtual(Real);
	Meta_AddVirtual(Pass);
	Meta_AddInteger(Restrict);
	Meta_AddInteger(SecMask);
	Meta_AddInteger(SecAddr);
	Meta_AddVirtual(Port);
	Meta_AddInteger(SameOrigin);

	Meta_SetName((IDS_WEB_SERVER));
}

// Implementation

void CWebServer::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Title", m_Enable);
	pHost->EnableUI(this, "Header", m_Enable);
	pHost->EnableUI(this, "Home", m_Enable);
	pHost->EnableUI(this, "CustHome", m_Enable);
	pHost->EnableUI(this, "CustIcon", m_Enable);
	pHost->EnableUI(this, "CustLogon", m_Enable && m_pOpts->m_AuthMethod == 1);
	pHost->EnableUI(this, "CustJss", m_Enable);
	pHost->EnableUI(this, "CustCs", m_Enable);

	pHost->EnableUI(this, "RemoteView", m_Enable);
	pHost->EnableUI(this, "RemoteCtrl", m_Enable && m_RemoteView);
	pHost->EnableUI(this, "RemoteZoom", m_Enable && m_RemoteView);
	pHost->EnableUI(this, "RemoteFact", m_Enable && m_RemoteView);
	pHost->EnableUI(this, "RemoteBits", m_Enable && m_RemoteView);

	pHost->EnableUI(this, "LogsView", m_Enable);
	pHost->EnableUI(this, "LogsBatches", m_Enable && m_LogsView);

	pHost->EnableUI(this, "UserSite", m_Enable);
	pHost->EnableUI(this, "UserMenu", m_Enable && m_UserSite && !m_CustHome);
	pHost->EnableUI(this, "UserDelay", m_Enable && m_UserSite);
	pHost->EnableUI(this, "UserRoot", m_Enable && m_UserSite);

	pHost->EnableUI(this, "SystemPages", m_Enable);
	pHost->EnableUI(this, "SystemConsole", m_Enable && m_SystemPages);
	pHost->EnableUI(this, "SystemCapture", m_Enable && m_SystemPages);

	pHost->EnableUI(this, "RemoteSec", m_Enable && m_RemoteView  && m_pOpts->m_AuthMethod && m_Source);
	pHost->EnableUI(this, "LogsSec", m_Enable && m_LogsView    && m_pOpts->m_AuthMethod && m_Source);
	pHost->EnableUI(this, "UserSec", m_Enable && m_UserSite    && m_pOpts->m_AuthMethod && m_Source);
	pHost->EnableUI(this, "SystemSec", m_Enable && m_SystemPages && m_pOpts->m_AuthMethod && m_Source);

	pHost->EnableUI(this, "Source", m_Enable && m_pOpts->m_AuthMethod);
	pHost->EnableUI(this, "Local", m_Enable && m_pOpts->m_AuthMethod &&  m_Source);
	pHost->EnableUI(this, "User", m_Enable && m_pOpts->m_AuthMethod && !m_Source);
	pHost->EnableUI(this, "Real", m_Enable && m_pOpts->m_AuthMethod && !m_Source);
	pHost->EnableUI(this, "Pass", m_Enable && m_pOpts->m_AuthMethod && !m_Source);

	pHost->EnableUI(this, "Restrict", m_Enable);
	pHost->EnableUI(this, "SecMask", m_Enable && m_Restrict);
	pHost->EnableUI(this, "SecAddr", m_Enable && m_Restrict);

	pHost->EnableUI(this, "Port", m_Enable && m_pOpts->m_Advanced);
	pHost->EnableUI(this, "SameOrigin", m_Enable && m_pOpts->m_Advanced);
}

// End of File
