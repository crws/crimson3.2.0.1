
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramItemView_HPP

#define INCLUDE_ProgramItemView_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramItem;

/////////////////////////////////////////////////////////////////////////
//
// Program Item View
//

class CProgramItemView : public CUIItemMultiWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramItemView(void);

		// Overridables
		void OnAttach(void);
		
		// Item Access
		CProgramItem * GetProgram(void);

		// Attributes
		BOOL CanTranslate(void);

		// Operations
		BOOL Translate(void);
		void Reload(void);

	protected:
		// Data Members
		CSysProxy          m_System;
		CProgramItem     * m_pItem;
		CSourceEditorWnd * m_pEditor;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);

		// Implementation
		void MakeTabs(void);
		BOOL FindEditor(void);
		void ForceUpdate(void);
	};

// End of File

#endif
