
#include "Intern.hpp"

#include "EthernetPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "EthernetItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Page
//

// Runtime Class

AfxImplementRuntimeClass(CEthernetPage, CUIPage);

// Constructor

CEthernetPage::CEthernetPage(CEthernetItem *pNet, UINT uPage)
{
	m_pNet = pNet;

	switch( uPage ) {

		case 0:
			m_Title = CString(IDS_ADD_PORTS);
			break;

		case 3:
			m_Title = CString(IDS_ETHERNET_1);
			break;

		case 4:
			m_Title = CString(IDS_ETHERNET_2);
			break;

		case 5:
			m_Title = CString(IDS_TLS_SSL);
			break;

		case 6:
			m_Title = CString(IDS_ZEROCONFIG);
			break;
	}

	m_uPage = uPage;
}

// Operations

BOOL CEthernetPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( m_uPage == 0 ) {

		pView->StartGroup(CString(IDS_PORT_COMMANDS),
				  1
		);

		pView->AddButton(CString(IDS_CREATE_NEW_1),
				 CString(IDS_CREATE_NEW_2),
				 L"ButtonAddPort"
		);

		pView->AddButton(CString(IDS_CREATE_NEW_3),
				 CString(IDS_CREATE_NEW_4),
				 L"ButtonAddVirtual"
		);

		pView->EndGroup(FALSE);
	}
	else {
		CLASS    Class = AfxPointerClass(m_pNet);

		CUIPage *pPage = New CUIStdPage(m_Title, Class, m_uPage);

		pPage->LoadIntoView(pView, pItem);

		delete pPage;

		if( m_uPage == 6 ) {

			pView->StartGroup(CString(IDS_CERTIFICATES),
					  1
			);

			pView->AddButton(CString(IDS_INSTALL_CRIMSON),
					 CString(IDS_INSTALL_ROOT),
					 L"ButtonAddRoot"
			);

			pView->EndGroup(FALSE);
		}
	}

	return TRUE;
}

// End of File
