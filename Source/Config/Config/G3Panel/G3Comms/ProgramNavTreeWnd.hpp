
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramNavTreeWnd_HPP

#define INCLUDE_ProgramNavTreeWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Program Navigation Window
//

class CProgramNavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramNavTreeWnd(void);

		// Attributes
		BOOL IsProgramSelected(void);

	protected:
		// Data Members
		CCommsSystem * m_pSystem;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Notification Handlers
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

		// Command Handlers
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		void OnItemNew(void);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void OnItemDeleted(CItem *pItem, BOOL fExec);
		void OnItemRenamed(CItem *pItem);
	};

// End of File

#endif
