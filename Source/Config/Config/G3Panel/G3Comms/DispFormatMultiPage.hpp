
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatMultiPage_HPP

#define INCLUDE_DispFormatMultiPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispFormatMulti;

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Page
//

class CDispFormatMultiPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDispFormatMultiPage(CDispFormatMulti *pFormat);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CDispFormatMulti *m_pFormat;
	};

// End of File

#endif
