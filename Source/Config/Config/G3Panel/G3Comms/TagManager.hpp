
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagManager_HPP

#define INCLUDE_TagManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTag;

class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager
//

class DLLNOT CTagManager : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagManager(void);

		// Destructor
		~CTagManager(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL HasCircular(void) const;
		BOOL CanStepFragment(CString Code) const;

		// Operations
		BOOL CreateTag(CString Name, UINT Type, UINT Size);
		BOOL Validate(CCodedTree &Done, BOOL fExpand);
		BOOL TagCheck(CCodedTree &Done, CIndexTree &Tags, UINT uTag);
		BOOL StepFragment(CString &Code, UINT uStep);
		BOOL FindCircular(CStringArray &List);
		void PostConvert(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		BOOL    IsHumanRoot (void) const;
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT       m_Handle;
		UINT	   m_LogToDisk;
		UINT	   m_FileLimit;
		UINT	   m_FileCount;
		UINT	   m_WithBatch;
		UINT       m_SignLogs;
		UINT	   m_LogToPort;
		UINT	   m_Drive;
		CTagList * m_pTags;

	protected:
		// Typedefs
		typedef CArray <UINT> CIndexList;

		// Meta Data Creation
		void AddMetaData(void);

		// Creation Helpers
		BOOL CreateTag(CString *pFull, CString Req, UINT Type, UINT Flags);
		BOOL CreateTag(CSysProxy &System, CString *pFull, CString Name, UINT Type, UINT Flags);
		BOOL CreateTag(CTag * &pTag, UINT Type);

		// Implementation
		BOOL Compile(CSystemWnd *pWnd, CCodedTree &Done, CCodedTree &Busy, CTag *pTag, BOOL fExpand);
		BOOL StepName(CString &Code) const;
	};

// End of File

#endif
