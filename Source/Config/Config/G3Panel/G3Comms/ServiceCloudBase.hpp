
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudBase_HPP

#define INCLUDE_ServiceCloudBase_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudDeviceDataSet;
class CCloudTagSet;
class CMqttClientOptions;

//////////////////////////////////////////////////////////////////////////
//
// Basic MQTT Cloud Client Configuration
//

class CServiceCloudBase : public CServiceItem
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CServiceCloudBase(void);

	// Initial Values
	void SetInitValues(void);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Management
	CViewWnd * CreateView(UINT uType);

	// UI Loading
	BOOL OnLoadPages(CUIPageList *pList);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Attributes
	BOOL IsEnabled(void) const;
	UINT GetTreeImage(void) const;

	// Property Save Filter
	BOOL SaveProp(CString const &Tag) const;

	// Conversion
	void PostConvert(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Type Definitions
	typedef CMqttClientOptions COpts;

	// Item Properties
	CCodedItem          * m_pEnable;
	UINT		      m_Service;
	UINT		      m_Mode;
	UINT		      m_Reconn;
	UINT		      m_Buffer;
	UINT		      m_Sets;
	UINT		      m_Drive;
	CCodedItem          * m_pIdent;
	CCodedItem          * m_pStatus;
	CCloudDeviceDataSet * m_pDev;
	CCloudTagSet        * m_pSet[32];
	COpts	            * m_pOpts;

protected:
	// Data Members
	BYTE m_bServCode;
	UINT m_OldSets;

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
	BOOL ImportCert(CString const &Base, CString const &CertFile, CString const &PrivFile);
	BOOL ImportCert(CString const &Base, CString const &AuthFile);
	BOOL LoadFile(CByteArray &Data, CFilename const &File);
};

// End of File

#endif
