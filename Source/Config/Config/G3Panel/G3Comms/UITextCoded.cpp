
#include "Intern.hpp"

#include "UITextCoded.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedHost.hpp"

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Coded Item
//

// Dynamic Class

AfxImplementDynamicClass(CUITextCoded, CUITextElement);

// Constructor

CUITextCoded::CUITextCoded(void)
{
	m_Class  = AfxRuntimeClass(CCodedItem);

	m_uFlags = textEdit;
}

// Overridables

void CUITextCoded::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	m_uWidth = 40;

	if( !List[0].IsEmpty() ) {

		if( isdigit(List[0][0]) ) {

			m_uWidth = watoi(List[0]);
		}
	}

	if( !List[1].IsEmpty() ) {

		m_uFlags |= textDefault;

		m_Default = List[1];

		C3OemStrings(m_Default);
	}

	if( !List[2].IsEmpty() ) {

		m_Params = List[2];
	}

	m_uLimit = 500;
}

CString CUITextCoded::OnGetAsText(void)
{
	CItem      * &pItem = m_pData->GetObject(m_pItem);

	CCodedItem * pCoded = (CCodedItem *) pItem;

	if( pItem ) {

		return pCoded->GetSource(TRUE);
	}

	return L"";
}

UINT CUITextCoded::OnSetAsText(CError &Error, CString Text)
{
	CItem      * &pItem = m_pData->GetObject(m_pItem);

	CCodedItem * pCoded = (CCodedItem *) pItem;

	if( Text.IsEmpty() ) {

		if( pCoded ) {

			pCoded->Kill();

			delete pCoded;

			pItem = NULL;

			return saveChange;
		}

		return saveSame;
	}

	if( pCoded ) {

		if( pCoded->Compile(Error, Text, m_UIData.m_Tag) ) {

			return saveChange;
		}

		return saveError;
	}
	else {
		CTypeDef Type;

		pCoded = AfxNewObject(CCodedItem, m_Class);

		pCoded->SetParent(m_pItem);

		pCoded->Init();

		if( m_pItem->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

			CCodedHost *pHost = (CCodedHost *) m_pItem;

			if( !pHost->GetTypeData(m_UIData.m_Tag, Type) ) {

				FindReqType(Type);
			}
		}
		else
			FindReqType(Type);

		pCoded->SetReqType(Type);

		pCoded->SetParams(m_Params);

		if( pCoded->Compile(Error, Text, m_UIData.m_Tag) ) {

			pItem = pCoded;

			return saveChange;
		}

		pCoded->Kill();

		delete pCoded;

		return saveError;
	}
}

// Default Data Type

void CUITextCoded::FindReqType(CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;
}

// Implementation

void CUITextCoded::ShowTwoWay(void)
{
	m_UIData.m_Status  = L"";

	m_UIData.m_Status += CFormat(IDS_THIS_PROPERTY_CAN, m_UIData.m_Label);

	m_UIData.m_Status += L" ";

	m_UIData.m_Status += CString(IDS_PREFIX_CODE_WITH);

	m_uFlags |= textStatus;
}

// End of File
