
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagNumeric_HPP

#define INCLUDE_TagNumeric_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DataTag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagEventNumeric;
class CTagQuickPlot;
class CTagTriggerNumeric;

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Item
//

class DLLAPI CTagNumeric : public CDataTag
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagNumeric(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetTreeImage(void) const;
		UINT GetDataType(void) const;
		UINT GetTypeFlags(void) const;
		BOOL HasSetpoint(void) const;
		BOOL NeedSetpoint(void) const;

		// Operations
		void UpdateTypes(BOOL fComp);
		void MakeLite(void);

		// Reference Check
		BOOL RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag);

		// Circular Check
		void UpdateCircular(void);

		// Evaluation
		DWORD GetProp(WORD ID, UINT Type);

		// Searching
		void FindAlarms  (CStringArray &List);
		void FindTriggers(CStringArray &List);

		// Persistance
		void PostLoad(void);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT                 m_Manipulate;
		UINT                 m_TreatAs;
		UINT                 m_ScaleTo;
		CCodedItem         * m_pDataMin;
		CCodedItem         * m_pDataMax;
		CCodedItem         * m_pDispMin;
		CCodedItem         * m_pDispMax;
		UINT                 m_HasSP;
		CCodedItem         * m_pSetpoint;
		UINT		     m_LimitType;
		CCodedItem	   * m_pLimitMin;
		CCodedItem	   * m_pLimitMax;
		CCodedItem	   * m_pDeadband;
		CTagEventNumeric   * m_pEvent1;
		CTagEventNumeric   * m_pEvent2;
		CTagTriggerNumeric * m_pTrigger1;
		CTagTriggerNumeric * m_pTrigger2;
		CSecDesc           * m_pSecDesc;
		CTagQuickPlot	   * m_pQuickPlot;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Property Access
		DWORD FindAsText(void);
		DWORD FindLabel(void);
		DWORD FindPrefix(void);
		DWORD FindUnits(void);
		DWORD FindSP(UINT Type);
		DWORD FindMin(UINT Type);
		DWORD FindMax(UINT Type);
		DWORD FindDeadband(UINT Type);
		DWORD FindFore(void);
		DWORD FindBack(void);

		// Memory Sizing
		UINT GetAllocSize(void);
		UINT GetCommsSize(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void LimitTreatAs(IUIHost *pHost);
		void LimitManipulate(IUIHost *pHost);
		void LimitAccess(IUIHost *pHost);
		void ClearScaling(IUIHost *pHost);
		void UpdateScaleDataTypes(IUIHost *pHost);
		void UpdateScaleDispTypes(IUIHost *pHost);
		void UpdateLimitTypes(IUIHost *pHost);
		void UpdateChildTypes(BOOL fComp);
	};

// End of File

#endif
