
#include "Intern.hpp"

#include "EthernetItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <wincrypt.h>

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsManager.hpp"
#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "CommsPortNetwork.hpp"
#include "EthernetFace.hpp"
#include "EthernetPage.hpp"
#include "EthernetRoutes.hpp"
#include "OptionCardItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CEthernetItem, CCodedHost);

// Constructor

CEthernetItem::CEthernetItem(void)
{
	m_pFace0       = New CEthernetFace(0);

	m_pFace1       = New CEthernetFace(1);

	m_Routing      = FALSE;

	m_Download     = TRUE;

	m_DownPort     = 789;

	m_SendPort     = 789;

	m_UseSendPort  = 1;

	m_DownMode     = 3;

	m_DownIP       = 0;

	m_EnableTls    = 1;

	m_RootSource   = 0;

	m_ZeroEnable  = 3;

	m_ZeroName    = L"";

	m_pRoutes      = New CEthernetRoutes;

	m_pPorts       = New CCommsPortList(10);
}

// UI Management

CViewWnd * CEthernetItem::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

			return CreateItemView(TRUE);
		}
	}

	return NULL;
}

// UI Creation

BOOL CEthernetItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CEthernetPage(this, 0));

	return FALSE;
}

// UI Update

void CEthernetItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UINT uMask = 0x03;

		if( HasDualPorts() ) {

			uMask |= 0x04;
		}

		if( m_ZeroEnable ) {

			uMask |= 0x08;
		}

		LimitEnum(pHost, L"DownMode", m_DownMode, uMask);

		DoEnables(pHost);
	}

	if( Tag == "Download" || Tag == "DownMode" || Tag == "ZeroName" ) {

		DoEnables(pHost);
	}

	if( Tag == "UseSendPort" ) {

		DoEnables(pHost);
	}

	if( Tag == "Routes" ) {

		CItemDialog Dlg(m_pRoutes, CString(IDS_ROUTING_TABLE));

		HGLOBAL hPrev = m_pRoutes->TakeSnapshot();

		CWnd    &Wnd  = pHost->GetWindow();

		if( Dlg.Execute(Wnd) ) {

			HGLOBAL        hData = m_pRoutes->TakeSnapshot();

			CCmdSubItem *  pCmd  = New CCmdSubItem(L"Routes",
							       hPrev,
							       hData
			);

			if( pCmd->IsNull() ) {

				delete pCmd;
			}
			else {
				pHost->SaveExtraCmd(pCmd);

				SetDirty();
			}
		}
		else
			GlobalFree(hPrev);
	}

	if( Tag == "ButtonAddPort" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_ADD_PORT);

		DoEnables(pHost);
	}

	if( Tag == "ButtonAddVirtual" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_ADD_VIRTUAL);

		DoEnables(pHost);
	}

	if( Tag == "ButtonAddRoot" ) {

		PCSTR pCert =	"-----BEGIN CERTIFICATE-----\n"
			"MIIEVTCCAz2gAwIBAgIJANzrvNYMye8PMA0GCSqGSIb3DQEBCwUAMHExCzAJBgNV\n"
			"BAYTAlVTMRUwEwYDVQQIDAxQZW5uc3lsdmFuaWExDTALBgNVBAcMBFlvcmsxHzAd\n"
			"BgNVBAoMFlJlZCBMaW9uIENvbnRyb2xzIEluYy4xGzAZBgNVBAMMEkNyaW1zb24g\n"
			"TG9jYWwgUm9vdDAgFw0xNzA5MDkwNzA3MzFaGA8yMTE2MDQwMzA3MDczMVowcTEL\n"
			"MAkGA1UEBhMCVVMxFTATBgNVBAgMDFBlbm5zeWx2YW5pYTENMAsGA1UEBwwEWW9y\n"
			"azEfMB0GA1UECgwWUmVkIExpb24gQ29udHJvbHMgSW5jLjEbMBkGA1UEAwwSQ3Jp\n"
			"bXNvbiBMb2NhbCBSb290MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA\n"
			"tWm5RKaYWDcQAaN2R1kfBVXPWgqPO8RkhIBZ9vjGVtsCXW5sa7merGQilkl8A3La\n"
			"2MZILw0NtYYv2xks1/bktu+nG6MkjicoJ2uaxs1CJAGZDLlEncMfTQQfq3f7zJhZ\n"
			"Vr3tm1Zu6Izf38vCDLjd20OcECISUeGZo0tZ3k19kKlZiq04tmzhh09qCDTZv7lo\n"
			"QNhO9daP+bSfuUXPvgeBysA6eGTieEbS5NCPQun+sw5i77GfJYKxqFlOnhVnN64z\n"
			"PuP+SsTszIFbs5kuX7DUfFLLMkiVprIQjG9WXqhZs04RJSHvJE59/g2lbGjI3XEC\n"
			"7S7UNFZseAobos7tc08zQwIDAQABo4HtMIHqMAwGA1UdEwQFMAMBAf8wHQYDVR0O\n"
			"BBYEFC1EbxBKNhY4IG1gpX2fL9wf1VC6MIGjBgNVHSMEgZswgZiAFC1EbxBKNhY4\n"
			"IG1gpX2fL9wf1VC6oXWkczBxMQswCQYDVQQGEwJVUzEVMBMGA1UECAwMUGVubnN5\n"
			"bHZhbmlhMQ0wCwYDVQQHDARZb3JrMR8wHQYDVQQKDBZSZWQgTGlvbiBDb250cm9s\n"
			"cyBJbmMuMRswGQYDVQQDDBJDcmltc29uIExvY2FsIFJvb3SCCQDc67zWDMnvDzAV\n"
			"BgNVHR4EDjAMoAowCIIGLmxvY2FsMA0GCSqGSIb3DQEBCwUAA4IBAQBFD9UMu1pc\n"
			"jLY3b3dQAdVoPNoVaRqOuMmVtB1gJvs8QDihVyzcuQYgKQl94HVcaIKzUrwTfdxN\n"
			"bgGvwJZrwRffCmCOI1PR695EBFC1wbqGmViOSNFEXvjtflXA3Ae4IxO7Ybh1Cqzp\n"
			"EvWye6zsKbg4I2Q65zxXVcV4/bACAO3OChxBKZun89EaqYBBP7149ng6iJGXfqnu\n"
			"fBLFmBHZ9xT/fMqZdilnFTBeENsb+ohJJsBAbDzWG8gqOLbsiz++q2LO390XKXz0\n"
			"eN22GHaoPlBVmZMLDaop2x7jdNj9mtiTwgHOrYOJA0wW1nLFd8Xw6YvLiF73jo93\n"
			"3+BRRGMnhusj\n"
			"-----END CERTIFICATE-----\n";

		DWORD nb = 0;

		CryptStringToBinaryA(pCert, strlen(pCert), 0, NULL, &nb, NULL, NULL);

		PBYTE pb = new BYTE[nb];

		CryptStringToBinaryA(pCert, strlen(pCert), 0, pb, &nb, NULL, NULL);

		if( CertAddEncodedCertificateToSystemStore(L"Root", pb, nb) ) {

			afxMainWnd->Information(L"The process completed without error.");
		}

		delete[] pb;
	}

	if( Tag == "EnableTls" ) {

		DoEnables(pHost);
	}

	if( Tag == "RootSource" ) {

		DoEnables(pHost);
	}

	if( Tag == "ZeroEnable" ) {

		DoEnables(pHost);
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Attributes

BOOL CEthernetItem::HasDualPorts(void) const
{
	return TRUE;
}

// Operations

void CEthernetItem::MakePorts(void)
{
	for( UINT i = 0; i < 4; i++ ) {

		CCommsPort *pPort = New CCommsPortNetwork;

		m_pPorts->AppendItem(pPort);
	}
}

void CEthernetItem::Validate(BOOL fExpand)
{
	m_pPorts->Validate(fExpand);
}

void CEthernetItem::ClearSysBlocks(void)
{
	m_pPorts->ClearSysBlocks();
}

void CEthernetItem::NotifyInit(void)
{
	m_pPorts->NotifyInit();
}

void CEthernetItem::CheckMapBlocks(void)
{
	m_pPorts->CheckMapBlocks();
}

void CEthernetItem::PostConvert(void)
{
	m_pFace0->PostConvert(TRUE);

	m_pFace1->PostConvert(TRUE);

	m_pPorts->PostConvert();
}

// Persistance

void CEthernetItem::Init(void)
{
	m_DownIP = C3OemFeature(L"OemSD", FALSE) ? IP1_HON : IP1_STD;

	CCodedHost::Init();
}

void CEthernetItem::PostLoad(void)
{
	CCodedHost::PostLoad();

	if( m_UseSendPort == 0 ) {

		m_SendPort    = m_DownPort;

		m_UseSendPort = 1;
	}
}

// Download Support

BOOL CEthernetItem::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pPorts);

	return TRUE;
}

// Meta Data Creation

void CEthernetItem::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddObject(Face0);
	Meta_AddObject(Face1);
	Meta_AddInteger(Routing);
	Meta_AddInteger(Download);
	Meta_AddInteger(DownPort);
	Meta_AddInteger(SendPort);
	Meta_AddInteger(DownMode);
	Meta_AddInteger(UseSendPort);
	Meta_AddInteger(DownIP);
	Meta_AddInteger(EnableTls);
	Meta_AddInteger(RootSource);
	Meta_AddString(RootFile);
	Meta_AddInteger(ZeroEnable);
	Meta_AddString(ZeroName);
	Meta_AddObject(Routes);
	Meta_AddCollect(Ports);

	if( C3OemFeature(L"OemSD", FALSE) ) {

		Meta_SetName((IDS_STATION_NETWORK));

		return;
	}

	Meta_SetName((IDS_NETWORK));
}

// Implementation

void CEthernetItem::DoEnables(IUIHost *pHost)
{
	BOOL fRead = m_pDbase->IsReadOnly();

	pHost->EnableUI("ButtonAddPort", !fRead);

	pHost->EnableUI("ButtonAddVirtual", !fRead);

	pHost->EnableUI("DownPort", m_Download);

	pHost->EnableUI("UseSendPort", m_Download);

	pHost->EnableUI("SendPort", m_Download && m_UseSendPort);

	pHost->EnableUI("DownMode", m_Download);

	pHost->EnableUI("DownIP", m_Download && !m_DownMode);

	pHost->EnableUI("RootSource", m_EnableTls);

	pHost->EnableUI("RootFile", m_EnableTls && m_RootSource && m_RootSource < 3);

	pHost->EnableUI("ZeroName", m_ZeroEnable);
}

// End of File
