
#include "Intern.hpp"

#include "UsbTreePath.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Rack Path Data
//

void CUsbTreePath::SetPort(UINT n, UINT uPort)
{
	switch( n ) {

		case 1: a.dwPort1 = uPort; break;
		case 2: a.dwPort2 = uPort; break;
		case 3: a.dwPort3 = uPort; break;
		case 4: a.dwPort4 = uPort; break;
		case 5: a.dwPort5 = uPort; break;
		case 6: a.dwPort6 = uPort; break;
		}
	};

// End of File
