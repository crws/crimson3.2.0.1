
#include "Intern.hpp"

#include "LangNameComboBox.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Language Name Combo Box
//

// Dynamic Class

AfxImplementRuntimeClass(CLangNameComboBox, CDropComboBox);

// Constructor

CLangNameComboBox::CLangNameComboBox(CUIElement *pUI) : CDropComboBox(pUI)
{
	}

// Message Map

AfxMessageMap(CLangNameComboBox, CDropComboBox)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxMessageEnd(CLangNameComboBox)
	};

// Message Handlers

void CLangNameComboBox::OnPostCreate(void)
{
	CItem        *pItem   = m_pUI->GetItem();

	CCommsSystem *pSystem = (CCommsSystem *) pItem->GetDatabase()->GetSystemItem();

	m_pLang               = pSystem->m_pLang;
	}

void CLangNameComboBox::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	Item.itemHeight = DC.GetTextExtent(L"ABC").cy + 2;

	DC.Deselect();
	}

void CLangNameComboBox::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
	CDC   DC(Item.hDC);

	CRect Rect = Item.rcItem;

	CRect Fill = Rect;

	Fill.right = Rect.left += 18;

	if( Item.itemState & ODS_DISABLED ) {

		DC.SetTextColor(afxColor(3dShadow));

		DC.SetBkColor(afxColor(3dFace));

		DC.FillRect(--Fill, afxBrush(3dShadow));
		}
	else {
		m_pLang->DrawFlagFromCode(DC, Fill, Item.itemData);

		if( Item.itemState & ODS_SELECTED ) {

			DC.SetTextColor(afxColor(SelectText));

			DC.SetBkColor(afxColor(SelectBack));
			}
		else {
			DC.SetTextColor(afxColor(WindowText));

			DC.SetBkColor(afxColor(WindowBack));
			}
		}

	CRect Shim = Rect;

	Shim.right = Rect.left += 2;

	DC.FillRect(Shim, afxBrush(WindowBack));

	if( Item.itemID < NOTHING ) {

		CString Text = GetLBText(Item.itemID);

		DC.ExtTextOut( Rect.GetTopLeft() + CPoint(3, 1),
			       ETO_OPAQUE | ETO_CLIPPED,
			       Rect, Text
			       );
		}

	if( Item.itemState & ODS_FOCUS ) {

		DC.DrawFocusRect(Rect);
		}
	}

// End of File
