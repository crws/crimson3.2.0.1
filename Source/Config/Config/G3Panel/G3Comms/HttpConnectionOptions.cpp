
#include "Intern.hpp"

#include "HttpConnectionOptions.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Connection Options
//

// Base Class

#undef  CBaseClass

#define CBaseClass CUIItem

// Dynamic Class

AfxImplementDynamicClass(CHttpConnectionOptions, CBaseClass);

// Constructor

CHttpConnectionOptions::CHttpConnectionOptions(void)
{
	m_Advanced    = FALSE;

	m_Tls         = FALSE;

	m_Port        = 80;

	m_SendTimeout = 30;

	m_RecvTimeout = 30;

	m_CertSource  = 0;
	}

// UI Update

void CHttpConnectionOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Advanced" ) {

			DoEnables(pHost);
			}

		if( Tag == "Tls" ) {

			if( m_Tls == 0 && (!m_Port || m_Port == 443) ) {

				m_Port = 80;

				pHost->UpdateUI("Port");
				}

			if( m_Tls == 1 && (!m_Port || m_Port ==  80) ) {

				m_Port = 443;

				pHost->UpdateUI("Port");
				}

			DoEnables(pHost);
			}

		if( Tag == "CertSource" ) {

			DoEnables(pHost);
			}

		CUIItem *pParent = (CUIItem *) GetParent();

		pParent->OnUIChange(pHost, pItem, Tag);
		}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
	}

// Property Save Filter

BOOL CHttpConnectionOptions::SaveProp(CString Tag) const
{
	return CBaseClass::SaveProp(Tag);
}

// Conversion

void CHttpConnectionOptions::PostConvert(void)
{
	m_CertSource = 0;
}

// Download Support

BOOL CHttpConnectionOptions::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddByte(BYTE(m_Tls));
	Init.AddWord(WORD(m_Port));
	Init.AddWord(WORD(m_SendTimeout));
	Init.AddWord(WORD(m_RecvTimeout));
	Init.AddWord(WORD(m_CertSource));

	return TRUE;
	}

// Meta Data Creation

void CHttpConnectionOptions::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(Advanced);
	Meta_AddInteger(Tls);
	Meta_AddInteger(Port);
	Meta_AddInteger(SendTimeout);
	Meta_AddInteger(RecvTimeout);
	Meta_AddInteger(CertSource);
	Meta_AddString (CertFile);
	Meta_AddString (CertName);
	Meta_AddString (CertPass);

	Meta_SetName((IDS_HTTP_CONNECTION));
	}

// Implementation

void CHttpConnectionOptions::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "SendTimeout", m_Advanced);

	pHost->EnableUI(this, "RecvTimeout", m_Advanced);

	pHost->EnableUI(this, "CertSource",  m_Tls);
	}

// End of File
