
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Comms.hpp"

#include "Intern.hxx"

#include <float.h>

#include <limits.h>

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Class Identifiers
//

#define IDC_COMMS		0x6001
#define IDC_TAG_MANAGER		0x6002
#define IDC_TAG_BLOCK		0x6003
#define IDC_PROG_MANAGER	0x6004
#define IDC_PROGRAM		0x6005
#define IDC_SERVICE		0x6006
#define IDC_WEB_SERVER		0x6007
#define IDC_LOGGER		0x6008
#define IDC_SECURITY		0x6009
#define	IDC_LANGUAGE		0x600A
#define	IDC_CONTROL		0x600B
#define	IDC_SQL			0x600C

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define LANG_SEP TCHAR(0xB6)

//////////////////////////////////////////////////////////////////////////
//
// Group Rights
//

#define	allowNone	UINT(0x00<<30)
#define	allowUsers	UINT(0x01<<30)
#define	allowAnyone	UINT(0x02<<30)
#define	allowDefault	UINT(0x03<<30)
#define	allowMask	UINT(0x03<<30)
#define	allowProgram	UINT(0x01<<29)
#define	allowCheck	UINT(0x01<<28)

//////////////////////////////////////////////////////////////////////////
//
// System Libraries
//

#pragma comment(lib, "shell32.lib")

#pragma comment(lib, "bcrypt.lib")

#pragma comment(lib, "crypt32.lib")

#pragma comment(lib, "winmm.lib")

// End of File

#endif
