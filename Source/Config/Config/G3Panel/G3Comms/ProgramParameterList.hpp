
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramParameterList_HPP

#define INCLUDE_ProgramParameterList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramParameter;

//////////////////////////////////////////////////////////////////////////
//
// Program Parameter List
//

class DLLNOT CProgramParameterList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramParameterList(void);

		// Persistance
		void Init(void);

		// Item Access
		CProgramParameter * GetItem(INDEX Index) const;
		CProgramParameter * GetItem(UINT  uPos ) const;
	};

// End of File

#endif
