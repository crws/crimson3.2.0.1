
#include "Intern.hpp"

#include "CommsMapRegList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsMapBlock.hpp"
#include "CommsMapReg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsMapRegList, CItemIndexList);

// Constructor

CCommsMapRegList::CCommsMapRegList(void)
{
	m_Class = AfxRuntimeClass(CCommsMapReg);
	}

// Item Location

CCommsMapBlock * CCommsMapRegList::GetParentBlock(void) const
{
	return (CCommsMapBlock *) GetParent();
	}

CCommsDevice  * CCommsMapRegList::GetParentDevice(void) const
{
	return GetParentBlock()->GetParentDevice();
	}

// Item Access

CCommsMapReg * CCommsMapRegList::GetItem(INDEX Index) const
{
	return (CCommsMapReg *) CItemIndexList::GetItem(Index);
	}

CCommsMapReg * CCommsMapRegList::GetItem(UINT uPos) const
{
	return (CCommsMapReg *) CItemIndexList::GetItem(uPos);
	}

// Operations

BOOL CCommsMapRegList::Validate(BOOL fExpand)
{
	BOOL fSave = FALSE;

	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsMapReg *pReg = GetItem(n);

		if( pReg->Validate(fExpand) ) {

			fSave = TRUE;
			}
		}

	return fSave;
	}

void CCommsMapRegList::SetCount(UINT uCount)
{
	CCommsDevice    * pDev = GetParentDevice();

	CCommsMapBlock  * pMap = GetParentBlock();

	if( pDev && pMap ) {

		ICommsDriver * pDrv = pDev->GetDriver();

		if( pDrv ) {

			pDrv->NotifyExtent(pMap->m_Addr, uCount, pDev->GetConfig());
			}
		}

	UINT uActual = GetItemCount();

	while( uActual < uCount ) {

		CCommsMapReg *pReg = New CCommsMapReg;

		AppendItem(pReg);

		uActual++;
		}

	while( uActual > uCount ) {

		INDEX Index = GetTail();

		DeleteItem(Index);

		uActual--;
		}
	}

// End of File
