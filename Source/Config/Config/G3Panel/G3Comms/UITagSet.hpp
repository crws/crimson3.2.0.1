
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITagSet_HPP

#define INCLUDE_UITagSet_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagSetWnd;

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Tag Set
//

class CUITagSet : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITagSet(void);

	protected:
		// Data Members
		CLayItemSize  * m_pTreeLayout;
		CTagSetWnd    * m_pDataWindow;

		// Core Overidables
		void OnBind(void);
		void OnRebind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handler
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect GetDataWindowRect(void);
	};

// End of File

#endif
