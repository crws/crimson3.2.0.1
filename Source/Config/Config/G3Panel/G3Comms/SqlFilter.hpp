
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlFilter_HPP

#define INCLUDE_SqlFilter_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced classes
//

#include "CodedHost.hpp"
#include "SqlQuery.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter
//

class CSqlFilter : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CSqlFilter(void);
		CSqlFilter(CSqlQuery * pQuery);
		CSqlFilter(CSqlQuery * pQuery, UINT uColumn, CString const &Filter);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Management
		CViewWnd * CreateView(UINT uType);
		
		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Helpers
		BOOL    NeedsOperand(void);
		CString GetOperator(void);
		CString GetBinding(void);
		void    Validate(BOOL fExpand);
		BOOL    IsValid(CError &Error);
		BOOL    Check(CStringArray &Errors);
		void    Fixup(void);

		enum {
			opEqual,
			opNotEqual,
			opLess,
			opLessEqual,
			opGreater,
			opGreaterEqual,
			opLike,
			opNull,
			opNotNull
			};

		enum {
			bindAnd,
			bindOr
			};

		// Data Members
		CString      m_Text;
		CString      m_ColName;
		CCodedItem * m_pValue;
		UINT         m_Column;
		UINT         m_Operator;
		UINT         m_Binding;
		UINT         m_NDX;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT    FindOperator(CString const &Text);
		CString StripOperator(CString const &Text, BOOL fString);
	};

// End of File

#endif
