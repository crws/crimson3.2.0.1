
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextIPAddr_HPP

#define INCLUDE_UITextIPAddr_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextCoded.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- IP Address
//

class CUITextIPAddr : public CUITextCoded
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextIPAddr(void);

	protected:
		// Overridables
		UINT OnSetAsText(CError &Error, CString Text);
	};

// End of File

#endif
