
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_USTOMTAGIMPINFO_HPP

#define INCLUDE_USTOMTAGIMPINFO_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Tag Export Helper
//

struct CUSTOMTAGIMPINFO
{
	UINT		uIDS;
	UINT		uState;
	UINT		uFileLine;
	UINT		uFormat;
	UINT		uItem;
	CString		sFileName;
	CString		sTagLine;
	CString		sType;
	CItem		*pConfig;
	CCommsSystem	*pCSys;
	};

// End of File

#endif
