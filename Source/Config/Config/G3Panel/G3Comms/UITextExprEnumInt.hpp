
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextExprEnumInt_HPP

#define INCLUDE_UITextExprEnumInt_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextExprEnum.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable Enumerated Integer
//

class CUITextExprEnumInt : public CUITextExprEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextExprEnumInt(void);

	protected:
		// Data Members
		UINT m_uMin;
		UINT m_uMax;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

// End of File

#endif
