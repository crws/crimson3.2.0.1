
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_USBHostItem_HPP

#define INCLUDE_USBHostItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUSBPortList;

//////////////////////////////////////////////////////////////////////////
//
// USB Host Configuration
//

class DLLNOT CUSBHostItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUSBHostItem(void);

		// Attributes
		UINT GetTreeImage(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CUSBPortList * m_pPorts;

	protected:
		// Port Types
		enum {
			typeNull	= 0,
			typeStick	= 1,
			typeKeyboard	= 2,
			typeMouse	= 3,
			};

		// Meta Data Creation
		void AddMetaData(void);

		// USB Port Creation
		void AddUSBPorts(void);

		// Implementation
		BOOL HasStick(void);
		BOOL HasKeyboard(void);
		BOOL HasMouse(void);
	};

// End of File

#endif
