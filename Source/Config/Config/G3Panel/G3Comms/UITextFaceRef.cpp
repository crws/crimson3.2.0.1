
#include "Intern.hpp"

#include "UITextFaceRef.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

#include <G3DevCon.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Interface Reference
//

// Dynamic Class

AfxImplementDynamicClass(CUITextFaceRef, CUITextEnum);

// Constructor

CUITextFaceRef::CUITextFaceRef(void)
{
	m_uFlags |= textReload | textRefresh;
}

// Overridables

void CUITextFaceRef::OnBind(void)
{
	CUITextElement::OnBind();

	AddData();
}

BOOL CUITextFaceRef::OnEnumValues(CStringArray &List)
{
	m_Enum.Empty();

	AddData();

	return CUITextEnum::OnEnumValues(List);
}

// Implementation

void CUITextFaceRef::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
}

void CUITextFaceRef::AddData(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CDevCon      *pDevCon = pSystem->m_pDevCon;

	CUIntArray   Data;

	CStringArray Text;

	pDevCon->GetFaceList(Data, Text);

	AddData(0, L"Automatic");

	for( UINT n = 0; n < Text.GetCount(); n++ ) {

		AddData(Data[n], Text[n]);
	}
}

// End of File
