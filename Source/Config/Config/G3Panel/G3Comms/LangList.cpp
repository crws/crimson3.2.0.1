
#include "Intern.hpp"

#include "LangList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Language List
//

// Dynamic Class

AfxImplementDynamicClass(CLangList, CItemList);

// Constructor

CLangList::CLangList(void)
{
	m_Class = AfxRuntimeClass(CLangItem);
	}

// Item Access

CLangItem * CLangList::GetItem(INDEX Index) const
{
	return (CLangItem *) CItemList::GetItem(Index);
	}

CLangItem * CLangList::GetItem(UINT uPos) const
{
	return (CLangItem *) CItemList::GetItem(uPos);
	}

// End of File
