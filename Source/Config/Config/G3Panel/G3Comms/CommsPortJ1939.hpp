
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortJ1939_HPP

#define INCLUDE_CommsPortJ1939_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPortCAN.hpp"

//////////////////////////////////////////////////////////////////////////
//
// J1939 Comms Port
//

class DLLNOT CCommsPortJ1939 : public CCommsPortCAN
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortJ1939(void);
		
		// Overridables
		char GetPortTag(void) const;
};

// End of File

#endif
