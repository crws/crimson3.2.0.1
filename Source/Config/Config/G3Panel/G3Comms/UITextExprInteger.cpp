
#include "Intern.hpp"

#include "UITextExprInteger.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable Integer
//

// Dynamic Class

AfxImplementDynamicClass(CUITextExprInteger, CUITextCoded);

// Constructor

CUITextExprInteger::CUITextExprInteger(void)
{
	m_nMin    = 0;

	m_nMax    = 60000;
	}

// Overridables

void CUITextExprInteger::OnBind(void)
{
	CUITextCoded::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( !List[3].IsEmpty() ) {

		m_nMin = watoi(List[3]);

		m_nMax = watoi(List[4]);
		}

	m_uPlaces = watoi(List[1]);

	if( !List[5].IsEmpty() ) {

		m_uFlags |= textUnits;

		m_Units   = List[5];

		C3OemStrings(m_Units);
		}

	m_uLimit  = max(watoi(List[0]), 20);

	m_uWidth  = m_uLimit;

	ShowTwoWay();
	}

CString CUITextExprInteger::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.GetLength() ) {

		if( IsNumberConst(Text) ) {

			INT nData = wcstol(Text.Mid(2), NULL, 16);

			return Format(StoreToDisp(nData));
			}

		return L'=' + Text;
		}

	return Text;
	}

UINT CUITextExprInteger::OnSetAsText(CError &Error, CString Text)
{
	if( Text.GetLength() ) {

		if( Text[0] == '=' ) {

			Text.Delete(0, 1);

			if( Text.IsEmpty() ) {

				Error.Set(CString(IDS_YOU_MUST_ENTER));

				return saveError;
				}

			return CUITextCoded::OnSetAsText(Error, Text);
			}

		INT     nData = DispToStore(Parse(Text));

		CString Prev  = CUITextCoded::OnGetAsText();

		if( IsNumberConst(Prev) ) {

			INT nPrev = wcstol(Prev.Mid(2), NULL, 16);

			if( nData - nPrev ) {

				if( Check(Error, nData) ) {

					CString Expr = CPrintf(L"0x%X", nData);

					return CUITextCoded::OnSetAsText(Error, Expr);
					}

				return saveError;
				}

			return saveSame;
			}

		CString Expr = CPrintf(L"0x%X", nData);

		return CUITextCoded::OnSetAsText(Error, Expr);
		}

	return saveError;
	}

// Scaling

INT CUITextExprInteger::StoreToDisp(INT nData)
{
	return nData;
	}

INT CUITextExprInteger::DispToStore(INT nData)
{
	return nData;
	}

// Implementation

BOOL CUITextExprInteger::IsNumberConst(CString const &Text)
{
	PCTXT p = Text;

	if( p[0] == '0' && p[1] == 'x' ) {

		if( *++++p ) {

			PTXT e;

			// cppcheck-suppress ignoredReturnValue

			wcstoul(p, &e, 16);

			return !*e;
			}
		}

	return FALSE;
	}

BOOL CUITextExprInteger::Check(CError &Error, INT &nData)
{
	if( nData < DispToStore(m_nMin) ) {

		if( Error.AllowUI() ) {

			CPrintf Text( IDS_SMALLEST,
				      Format(m_nMin)
				      );

			Error.Set(Text);

			return FALSE;
			}

		nData = DispToStore(m_nMin);

		return TRUE;
		}

	if( nData > DispToStore(m_nMax) ) {

		if( Error.AllowUI() ) {

			CPrintf Text( IDS_LARGEST,
				      Format(m_nMax)
				      );

			Error.Set(Text);

			return FALSE;
			}

		nData = DispToStore(m_nMax);

		return TRUE;
		}

	return TRUE;
	}

CString CUITextExprInteger::Format(INT nData)
{
	double  f = double(nData);

	double  p = pow(double(10), int(m_uPlaces));

	CPrintf t = CPrintf(L"%.*f", m_uPlaces, f / p);

	return (m_nMin < 0 && nData > 0) ? L'+' + t : t;
	}

INT CUITextExprInteger::Parse(PCTXT pText)
{
	double f = watof(pText);

	double p = pow(double(10), int(m_uPlaces));

	return (f > 0 ) ? INT(f * p + 0.5) : INT(f * p - 0.5);
	}

// End of File
