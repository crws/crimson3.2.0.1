
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CloudDeviceDataSet_HPP

#define INCLUDE_CloudDeviceDataSet_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cloud Device Data Set
//

class CCloudDeviceDataSet : public CCloudDataSet
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCloudDeviceDataSet(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Gps;
		UINT m_Cell;
		UINT m_Etc;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
