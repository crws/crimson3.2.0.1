
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagStringPage_HPP

#define INCLUDE_TagStringPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagString;

//////////////////////////////////////////////////////////////////////////
//
// String Tag Page
//

class CTagStringPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTagStringPage(CTagString *pTag, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CTagString * m_pTag;

		// Implementation
		BOOL LoadFormat(IUICreate *pView);
		BOOL LoadColor(IUICreate *pView);
		BOOL LoadSecurity(IUICreate *pView);
	};

// End of File

#endif
