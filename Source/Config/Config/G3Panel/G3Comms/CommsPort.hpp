
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPort_HPP

#define INCLUDE_CommsPort_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsDeviceList;
class CCommsSysBlock;

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

class DLLAPI CCommsPort : public CCodedHost
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Port Creation
		static CCommsPort * CreatePort(char cTag);

		// Constructor
		CCommsPort(CLASS Class);

		// Destructor
		~CCommsPort(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Driver Access
		ICommsDriver * GetDriver(void) const;

		// Item Lookup
		CCommsDevice   * FindDevice(UINT uDevice) const;
		CCommsDevice   * FindDevice(CString Name) const;
		CCommsSysBlock * FindBlock (UINT uBlock ) const;

		// Overridables
		virtual UINT GetPageType(void) const;
		virtual char GetPortTag(void) const;
		virtual BOOL IsBroken(void) const;
		virtual BOOL IsRemotable(void) const;
		virtual void FindBindingMask(void);

		// Attributes
		UINT    GetTreeImage(void) const;
		CString GetLabel(void) const;
		CString GetShortName(void) const;
		BOOL    CanAddDevice(void) const;
		BOOL    IsModem(void) const;
		BOOL    IsExclusive(void) const;
		BOOL    IsProgram(void) const;

		// Driver Creation
		virtual ICommsDriver * CreateDriver(UINT uID);

		// Operations
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		BOOL ClearSettings(void);
		BOOL UpdateDriver(void);
		void SetUsed(CString Used);

		// Item Naming
		CString GetItemOrdinal(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);
		void Kill(void);

		// Conversion
		virtual void PostConvert(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CString	           m_Name;
		CString		   m_Where;
		CString		   m_PortTag;
		UINT               m_Number;
		UINT		   m_Import;
		UINT		   m_PortPhys;
		UINT		   m_PortLog;
		UINT	           m_Binding;
		UINT		   m_BindMask;
		UINT	           m_DriverID;
		UINT		   m_DriverRpc;
		CUIItem	         * m_pConfig;
		CCommsDeviceList * m_pDevices;
		BOOL		   m_Remotable;

	protected:
		// Data Members
		UINT           m_uImage;
		ICommsDriver * m_pDriver;
		CString	       m_Used;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL    FreeDriverObject(void);
		BOOL    FreeConfigItem(void);
		void    DoEnables(IUIHost *pHost);
		BOOL    CheckDriver(void);
		BOOL    AllocNumber(void);
		BOOL    UpdateConfig(IUIHost *pHost);
		void    CheckDevices(IUIHost *pHost);
		BOOL    AddImage(CInitData &Init);
		CString FindDriverPath(WORD id);
		BOOL    UsingEmulator(void);
	};

// End of File

#endif
