
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SystemResourceItem_HPP

#define INCLUDE_SystemResourceItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CFunctionList;
class CNameServer;

//////////////////////////////////////////////////////////////////////////
//
// System Resource Item
//

class CSystemResourceItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSystemResourceItem(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Operations
		void LoadLibrary(void);

		// Persistance
		void Init(void);

		// Data Members
		CFunctionList *m_pLib;

	protected:
		// Data Members
		CNameServer    *m_pServer;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void LoadVariables(void);
		void LoadFunctions(void);
		void FindNameServer(void);
		BOOL FindFunction(CString &Name, WORD ID, CTypeDef &Type, UINT &uCount);
		BOOL FindIdent(CString Name, WORD &ID, CTypeDef &Type);
		void AddCategory(CString Cat);
	};

// End of File

#endif
