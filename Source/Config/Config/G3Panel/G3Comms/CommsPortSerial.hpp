
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortSerial_HPP

#define INCLUDE_CommsPortSerial_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Serial Comms Port
//

class DLLAPI CCommsPortSerial : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortSerial(void);

		// Initial Values
		void SetInitValues(void);

		// UI Overridables
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		BOOL OnLoadPages(CUIPageList *pList);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetPageType(void) const;
		char GetPortTag(void) const;
		BOOL IsBroken(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Phys;
		BOOL	     m_fAux;
		CCodedItem * m_pBaud;
		CCodedItem * m_pData;
		CCodedItem * m_pStop;
		CCodedItem * m_pParity;
		CCodedItem * m_pMode;
		CCodedItem * m_pShareEnable;
		CCodedItem * m_pSharePort;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL SetDefaults(IUIHost *pHost);
		void DoEnables(IUIHost *pHost);
		BOOL CanConfigure(CError &Error);
	};

// End of File

#endif
