
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortFireWire_HPP

#define INCLUDE_CommsPortFireWire_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FireWire Comms Port
//

class DLLNOT CCommsPortFireWire : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortFireWire(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

// End of File

#endif
