
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagPasteSpecialDialog_HPP

#define INCLUDE_TagPasteSpecialDialog_HPP

//////////////////////////////////////////////////////////////////////////
//
// Tag Paste Special Dialog
//

class CTagPasteSpecialDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CTagPasteSpecialDialog(void);

		// Attributes
		UINT GetMode(void) const;

	protected:
		// Data Members
		UINT m_uMode;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
	};

// End of File

#endif
