
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColorMultiPage_HPP

#define INCLUDE_DispColorMultiPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispColorMulti;

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Page
//

class CDispColorMultiPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDispColorMultiPage(CDispColorMulti *pColor);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CDispColorMulti * m_pColor;
	};

// End of File

#endif
