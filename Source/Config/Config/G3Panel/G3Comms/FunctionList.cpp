
#include "Intern.hpp"

#include "FunctionList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Function List
//

// Dynamic Class

AfxImplementDynamicClass(CFunctionList, CItemIndexList);

// Constructor

CFunctionList::CFunctionList(void)
{
	}

// Item Access

CMetaItem * CFunctionList::GetItem(INDEX Index) const
{
	return (CMetaItem *) CItemIndexList::GetItem(Index);
	}

CMetaItem * CFunctionList::GetItem(UINT uPos) const
{
	return (CMetaItem *) CItemIndexList::GetItem(uPos);
	}

// Item Lookup

UINT CFunctionList::FindNamePos(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CMetaItem *pItem = GetItem(n);

		if( pItem->GetName() == Name ) {

			return pItem->GetIndex();
			}

		GetNext(n);
		}

	return NOTHING;
	}

// End of File
