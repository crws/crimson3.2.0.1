
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_OptionCardItem_HPP

#define INCLUDE_OptionCardItem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Option Card Classes
//

enum CardClass
{
	classNone	=   0,
	classComms	=   1,
	classIO		=   2,
	classRack	=   3,
	classDongle	=   4,
	};

//////////////////////////////////////////////////////////////////////////
//
// Option Card Types
//

enum CardType
{
	typeNone	=   0,
	typeSerial	=   1,
	typeCAN		=   2,
	typeProfibus	=   3,
	typeFireWire	=   4,
	typeDeviceNet	=   5,
	typeCatLink	=   6,
	typeModem	=   7,
	typeMPI		=   8,
	typeEthernet	=   9,
	typeUSBHost	=  10,
	typeJ1939	=  11,
	typeDnp3	=  12,
	typeTest	=  13,
	typeCanCdl	=  14,
	typeWiFi	=  15, 
	typeSerial232   =  17,
	typeSerial485   =  18,
	typeSerialMix   =  19,
	typeDongle	= 253,
	typeRack        = 254,
	typeModule      = 255,
	};

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPort;
class CCommsPortList;

//////////////////////////////////////////////////////////////////////////
//
// Option Card Configuration
//

class DLLAPI COptionCardItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COptionCardItem(void);
		COptionCardItem(UINT uSlot);
		COptionCardItem(UINT uSlot, CString Name);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		UINT    GetTreeImage(void) const;
		CString GetTreeLabel(void) const;
		CString GetCardName(UINT uType) const;
		CString GetCardType(UINT uType) const;
		UINT    GetCardClass(void) const;
		UINT    GetFirmwareID(void) const;
		UINT    GetPower(void) const;

		// Operations
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		void RemoveCard(void);
		void SetType(UINT uType);
		void SetPower(UINT uPower);
		void SetModule(CString Name);
		BOOL IsSupported(UINT uType);
		void PostConvert(void);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT		 m_Slot;
		UINT             m_Type;
		UINT             m_Power;
		CCommsPortList * m_pPorts;
		CString          m_Name;
		CString          m_Module;
		UINT             m_Number;

	protected:
		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void AddPorts(void);
		void DelPorts(void);
		void DoEnables(IUIHost *pHost);
		BOOL CheckPower(CString &Warning);

		// Port Enumeration
		CCommsPort * EnumPorts(UINT i);
		CCommsPort * EnumSerial(UINT i);
		CCommsPort * EnumCAN(UINT i);
		CCommsPort * EnumProfibus(UINT i);
		CCommsPort * EnumFireWire(UINT i);
		CCommsPort * EnumDeviceNet(UINT i);
		CCommsPort * EnumCatLink(UINT i);
		CCommsPort * EnumModem(UINT i);
		CCommsPort * EnumMPI(UINT i);
		CCommsPort * EnumEthernet(UINT i);
		CCommsPort * EnumUSBHost(UINT i);
		CCommsPort * EnumJ1939(UINT i);
		CCommsPort * EnumTest(UINT i);
		CCommsPort * EnumDnp3(UINT i);
		CCommsPort * EnumWifi(UINT i);

		// Filtering
		BOOL CheckPlatformSupport(UINT Type) const;
		BOOL CheckFeatureSupport(UINT Type) const;
		BOOL CheckRackSupport(UINT Type) const;
	};

// End of File

#endif
