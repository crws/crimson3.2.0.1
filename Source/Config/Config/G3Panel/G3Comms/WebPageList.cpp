
#include "Intern.hpp"

#include "WebPageList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "WebPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Page List
//

// Dynamic Class

AfxImplementDynamicClass(CWebPageList, CItemIndexList);

// Constructor

CWebPageList::CWebPageList(void)
{
	m_Class = AfxRuntimeClass(CWebPage);
	}

// Download Support

BOOL CWebPageList::MakeInitData(CInitData &Init)
{
	CItem::MakeInitData(Init);

	UINT uCount = m_Index.GetCount();

	Init.AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n++ ) {

		CItem *pItem = CItemList::GetItem(n);

		if( pItem ) {

			Init.AddByte(1);

			pItem->MakeInitData(Init);
			}
		else
			Init.AddByte(0);
		}

	return TRUE;
	}

// End of File
