
#include "Intern.hpp"

#include "SqlQuery.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "SqlColumn.hpp"
#include "SqlColumnList.hpp"
#include "SqlFilter.hpp"
#include "SqlFilterDialog.hpp"
#include "SqlFilterList.hpp"
#include "SqlFilterPage.hpp"
#include "SqlPreviewDialog.hpp"
#include "SqlQueryManager.hpp"
#include "SqlQueryPage.hpp"
#include "SqlRecord.hpp"
#include "SqlRecordList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Sql Query Item
//

// Dynamic Class

AfxImplementDynamicClass(CSqlQuery, CUIItem);

// Constructors

CSqlQuery::CSqlQuery(void)
{
	m_Enable	= 1;

	m_Rows          = 1;
	
	m_Update        = 5;

	m_SortMode      = 0;

	m_FilterPush    = 0;

	m_SortColumn    = 0;

	m_ShowSQL       = 0;

	m_fBroken       = TRUE;

	m_pCols         = New CSqlColumnList;

	m_pColumns      = New CSqlColumnList;

	m_pFilters      = New CSqlFilterList;
	}

CSqlQuery::CSqlQuery(CString Name)
{
	m_Enable	= 1;

	m_Rows          = 1;

	m_Update        = 5;

	m_SortMode      = 0;

	m_FilterPush    = 0;

	m_SortColumn    = 0;

	m_ShowSQL       = 0;

	m_Validate      = 0;

	m_fBroken       = TRUE;

	m_pCols         = New CSqlColumnList;

	m_pColumns      = New CSqlColumnList;

	m_pFilters      = New CSqlFilterList;

	m_Name          = Name;
	}

// Destructor

CSqlQuery::~CSqlQuery(void)
{
	}

// UI Management

CViewWnd * CSqlQuery::CreateView(UINT uType)
{
	return CUIItem::CreateView(uType);
	}

// UI Update

void CSqlQuery::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == L"Rows" ) {

		if( !pHost->InReplay() ) {

			HGLOBAL hPrev = m_pCols->TakeSnapshot();

			AdjustRows(m_Rows);

			HGLOBAL hData = m_pCols->TakeSnapshot();

			CCmdSubItem *pCmd = New CCmdSubItem( L"Columns",
							     hPrev,
							     hData
							     );

			if( pCmd->IsNull() ) {

				delete pCmd;
				}
			else {
				pHost->SaveExtraCmd(pCmd);
				}

			pHost->SendUpdate(updateChildren);
			}
		}
	
	if( Tag == L"FilterPush" ) {
		
		CSqlFilterDialog Dlg(this);

		HGLOBAL hPrev = m_pFilters->TakeSnapshot();

		CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

		if( System.ExecSemiModal(Dlg) ) {

			HGLOBAL hData = m_pFilters->TakeSnapshot();

			CCmdSubItem *pCmd = New CCmdSubItem( L"Filters",
							     hPrev,
							     hData
							     );

			if( pCmd->IsNull() ) {

				delete pCmd;
				}
			else {
				pHost->SaveExtraCmd(pCmd);

				SetDirty();
				}
			}
		else {
			m_pFilters->LoadSnapshot(hPrev);

			GlobalFree(hPrev);
			}
		}

	if( Tag == L"SortMode" || Tag == L"SortColumn" ) {

		UpdateSortColumn();
		}

	if( Tag == L"ShowSQL" ) {

		BuildSQL();

		CSqlPreviewDialog Dlg(m_SQL);

		Dlg.Execute();
		}

	DoEnables(pHost);

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// UI Loading

BOOL CSqlQuery::OnLoadPages(CUIPageList * pList)
{
	pList->Append(New CSqlQueryPage(this));

	return FALSE;
	}

void CSqlQuery::PostLoad(void)
{
	CUIItem::PostLoad();

	if( m_pCols->GetItemCount() == 0 && m_pColumns->GetItemCount() != 0 ) {

		for( INDEX i = m_pColumns->GetHead(); !m_pColumns->Failed(i); m_pColumns->GetNext(i) ) {

			CSqlColumn *pCol = m_pColumns->GetItem(i);

			m_pCols->AppendItem(pCol);
			}		
		}

	for( INDEX i = m_pFilters->GetHead(); !m_pFilters->Failed(i); m_pFilters->GetNext(i) ) {

		CSqlFilter *pFilter = m_pFilters->GetItem(i);

		pFilter->Fixup();
		}

	if( m_SortName.IsEmpty() ) {

		m_SortName = GetColumnSqlName(m_SortColumn);
		}

	Check();	
	}

// Download Support

void CSqlQuery::PrepareData(void)
{
	BuildSQL();

	Check();
	}

BOOL CSqlQuery::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Enable));
	Init.AddText(m_Name);
	Init.AddText(m_Table);

	if( m_Enable > 0 ) {

		Init.AddByte(BYTE(m_pCols->GetItemCount()));
		Init.AddWord(WORD(m_Rows));
		Init.AddWord(WORD(m_Update));
		Init.AddByte(BYTE(m_SortMode));
		Init.AddWord(WORD(m_SortColumn));
		Init.AddByte(BYTE(m_fBroken));
		Init.AddText(m_SQL);
		Init.AddText(m_Schema);
		Init.AddItem(itemSimple, m_pCols);
		Init.AddItem(itemSimple, m_pFilters);
		}
	
	return TRUE;
	}

// Column Helpers

CString CSqlQuery::GetUnusedName(void)
{
	for( UINT n = 1;; n++ ) {

		CPrintf Name(L"Column%u", n);

		if( !m_pCols->FindByName(Name) ) {

			return Name;
			}
		}

	return L"";
	}

UINT CSqlQuery::GetNextPos(void)
{
	return m_pCols->GetItemCount();
	}

// Management

CSqlColumn * CSqlQuery::GetColumn(UINT uCol)
{
	return m_pCols->GetColumnDef(uCol);
	}

CSqlColumn * CSqlQuery::GetColumn(CString const &SqlName)
{
	UINT uPos = m_pCols->GetColumnPos(SqlName);

	if( uPos != NOTHING ) {

		return m_pCols->GetColumnDef(uPos);
		}

	return NULL;
	}

UINT CSqlQuery::GetColumnPos(CString const &SqlName)
{
	return m_pCols->GetColumnPos(SqlName);
	}

CSqlRecord * CSqlQuery::GetRecord(UINT uCol, UINT uRow)
{
	if( uCol < m_pCols->GetItemCount() && uRow < m_Rows ) {

		CSqlColumn *pColumn = m_pCols->GetColumnDef(uCol);

		return pColumn->GetRecord(uRow);
		}

	return NULL;
	}

CSqlFilter * CSqlQuery::MakeFilter(void)
{
	CSqlFilter * pFilter = New CSqlFilter(this);

	m_pFilters->AppendItem(pFilter);

	return pFilter;
	}

void CSqlQuery::AdjustRows(UINT uCount)
{
	for( INDEX i = m_pCols->GetHead(); !m_pCols->Failed(i); m_pCols->GetNext(i) ) {

		CSqlColumn *pColumn = m_pCols->GetItem(i);

		int iDiff = uCount - pColumn->m_pRecords->GetItemCount();

		if( iDiff > 0 ) {

			UINT uStart = pColumn->m_pRecords->GetItemCount();

			for( UINT n = uStart; n < uStart + UINT(iDiff); n++ ) {

				CSqlRecord *pRecord = New CSqlRecord;

				pColumn->m_pRecords->AppendItem(pRecord);
				}
			}
		else {
			for( UINT n = 0; n < UINT(-iDiff); n++ ) {

				UINT r = pColumn->m_pRecords->GetItemCount() - 1;

				CSqlRecord *pRecord = pColumn->m_pRecords->GetItem(r);

				pColumn->m_pRecords->DeleteItem(pRecord);
				}
			}

		UINT uType = pColumn->GetC3Type();

		pColumn->UpdateRecordType(uType);
		}
	}

void CSqlQuery::AddFilter(UINT uColumn, CString const &Filter)
{
	CSqlFilter *pFilter = New CSqlFilter(this, uColumn, Filter);

	m_pFilters->AppendItem(pFilter);
	}

BOOL CSqlQuery::DeleteFilter(CSqlFilter *pFilter)
{
	return m_pFilters->DeleteItem(pFilter);		
	}

void CSqlQuery::UpdateSortColumn(void)
{
	m_SortName = GetColumnSqlName(m_SortColumn);
	}

void CSqlQuery::Validate(BOOL fExpand)
{
	INDEX i;

	for( i = m_pCols->GetHead(); !m_pCols->Failed(i); m_pCols->GetNext(i) ) {

		CSqlColumn *pColumn = m_pCols->GetItem(i);

		pColumn->Validate(fExpand);
		}

	for( i = m_pFilters->GetHead(); !m_pFilters->Failed(i); m_pFilters->GetNext(i) ) {

		CSqlFilter *pFilter = m_pFilters->GetItem(i);

		pFilter->Validate(fExpand);
		}
	}

BOOL CSqlQuery::Check(CStringArray &Errors)
{
	BOOL fOk = TRUE;

	if( m_SortMode > 0 && GetColumn(m_SortName) == NULL ) {

		CString Error;

		Error += GetHumanPath();

		Error += L": ";

		Error += IDS("Sort Column does not exist.");

		Error += L"\n";

		Error += GetFixedPath();

		Errors.Append(Error);

		fOk = FALSE;
		}

	if( m_Table.IsEmpty() ) {

		CString Error;

		Error += GetHumanPath();

		Error += L": ";

		Error += IDS("Table name is empty.");

		Error += L"\n";

		Error += GetFixedPath();

		Errors.Append(Error);

		fOk = FALSE;
		}

	INDEX i;

	for( i = m_pCols->GetHead(); !m_pCols->Failed(i); m_pCols->GetNext(i) ) {

		CSqlColumn *pColumn = m_pCols->GetItem(i);

		if( !pColumn->Check(Errors) ) {

			fOk = FALSE;
			}
		}

	for( i = m_pFilters->GetHead(); !m_pFilters->Failed(i); m_pFilters->GetNext(i) ) {

		CSqlFilter *pFilter = m_pFilters->GetItem(i);

		if( !pFilter->Check(Errors) ) {

			fOk = FALSE;
			}
		}
	
	m_fBroken = !fOk;

	return fOk;
	}

BOOL CSqlQuery::Check(void)
{
	CStringArray Dummy;

	return Check(Dummy);
	}

UINT CSqlQuery::GetImage(void)
{
	return IDI_SQL_QUERY;
	}

// Attributes

UINT CSqlQuery::GetRowCount(void)
{
	return m_Rows;
	}

UINT CSqlQuery::GetColCount(void)
{
	return m_pCols->GetItemCount();
	}

// Meta Data Creation

void CSqlQuery::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Enable);
	Meta_AddString (Table);
	Meta_AddString (Schema);
	Meta_AddInteger(Rows);
	Meta_AddInteger(Update);
	Meta_AddInteger(SortMode);
	Meta_AddInteger(SortColumn);
	Meta_AddString (SortName);
	Meta_AddInteger(FilterPush);
	Meta_AddString (SQL);
	Meta_AddCollect(Cols);
	Meta_AddCollect(Columns);
	Meta_AddCollect(Filters);
	Meta_AddInteger(ShowSQL);
	Meta_AddInteger(Validate);

	Meta_SetName((IDS_SQL_QUERY));
	}

// Implementation

void CSqlQuery::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_Enable > 0;

	pHost->EnableUI(L"Table",       fEnable);
	pHost->EnableUI(L"Schema",      fEnable);
	pHost->EnableUI(L"ColumnCount", fEnable);
	pHost->EnableUI(L"Rows",        fEnable);
	pHost->EnableUI(L"Update",      fEnable);
	pHost->EnableUI(L"SortMode",    fEnable);
	pHost->EnableUI(L"SortColumn",  fEnable);
	pHost->EnableUI(L"FilterPush",  fEnable);
	pHost->EnableUI(L"SQL",         fEnable);
	pHost->EnableUI(L"Filters",     fEnable);
	pHost->EnableUI(L"ShowSQL",     fEnable);

	if( fEnable ) {

		pHost->EnableUI(L"Update",     m_Enable == 1);

		pHost->EnableUI(L"SortColumn", m_SortMode > 0 && m_pCols->GetItemCount() > 0);
		}
	}

void CSqlQuery::BuildSQL(void)
{
	CString Sql(CPrintf(L"SELECT TOP %u ", m_Rows));

	UINT uCols = m_pCols->GetItemCount();

	for( INDEX i = m_pCols->GetHead(); !m_pCols->Failed(i); m_pCols->GetNext(i) ) {

		CSqlColumn *pColumn = m_pCols->GetItem(i);

		if( !pColumn ) continue;

		Sql += CPrintf(L"[%s]", pColumn->m_SqlName);

		if( uCols > 1 && i != m_pCols->GetTail() ) {

			Sql += L", ";
			}
		}
	
	CString Schema   = m_Schema.IsEmpty() ? L"dbo" : m_Schema;

	CSqlQueryManager *pManager = (CSqlQueryManager *) GetParent(AfxRuntimeClass(CSqlQueryManager));

	CString Database = pManager->m_DatabaseName;

	Sql += CPrintf(L" FROM [%s].[%s].[%s]", PCTXT(Database), PCTXT(Schema), PCTXT(m_Table));

	for( INDEX i = m_pFilters->GetHead(); !m_pFilters->Failed(i); m_pFilters->GetNext(i) ) {

		CSqlFilter *pFilter = m_pFilters->GetItem(i);

		if( i == m_pFilters->GetHead() ) {

			Sql += L" WHERE ";
			}
		else {
			Sql += CPrintf(" %s ", pFilter->GetBinding());
			}

		CString Text = pFilter->GetOperator();

		if( pFilter->NeedsOperand() ) {

			if( pFilter->m_pValue->IsConst() ) {

				UINT uType = pFilter->m_pValue->GetType();

				if( uType == typeInteger ) {

					C3INT val = pFilter->m_pValue->ExecVal();

					Text += CPrintf(" %i", val);
					}

				if( uType == typeString ) {

					Text += CPrintf(" \'%s\'", pFilter->m_pValue->ExecVal());
					}

				if( uType == typeReal ) {

					DWORD val = pFilter->m_pValue->ExecVal();

					C3REAL f  = *((C3REAL *)&val);

					Text += CPrintf(" %f", f);
					}
				}
			else {
				Text += CPrintf(" %s", pFilter->m_pValue->GetSource(TRUE));
				}
			}

		CSqlColumn *pColumn = GetColumn(pFilter->m_Column);

		CString ColName = pColumn ? pColumn->m_SqlName : L"<ERR>";

		CString Filter = CPrintf(L"[%s] %s",
					 ColName,
					 Text
					 );

		Sql += Filter;
		}

	if( m_SortMode > 0 && m_SortColumn < uCols ) {

		CSqlColumn *pColumn = GetColumn(m_SortName);

		if( pColumn ) {

			PCTXT pName = pColumn->m_SqlName;

			if( pName ) {

				Sql += CPrintf(L" ORDER BY [%s] %s", pName, (m_SortMode == 1) ? L"ASC" : L"DESC");
				}
			}
		}

	m_SQL = Sql;
	}

CString CSqlQuery::GetColumnName(UINT uCol)
{
	if( uCol < m_pCols->GetItemCount() ) {

		CSqlColumn *pColumn = m_pCols->GetColumnDef(uCol);

		if( pColumn ) {

			return PCTXT(pColumn->m_Name);
			}
		}

	return L"";
	}

CString CSqlQuery::GetColumnSqlName(UINT uCol)
{
	if( uCol < m_pCols->GetItemCount() ) {

		CSqlColumn *pColumn = m_pCols->GetColumnDef(uCol);

		if( pColumn ) {

			return PCTXT(pColumn->m_SqlName);
			}
		}

	return L"";
	}

// End of File
