
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_MailManager_HPP

#define INCLUDE_MailManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMailAddress;
class CMailContacts;

//////////////////////////////////////////////////////////////////////////
//
// Mail Manager Configuration
//

class CMailManager : public CServiceItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMailManager(void);

		// Initial Values
		void SetInitValues(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetTreeImage(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem      * m_pEnable;
		CCodedItem	* m_pPanelName;
		CCodedItem      * m_pSMTP;
		CCodedItem      * m_pMode;
		CCodedItem      * m_pMailServer;
		CCodedItem      * m_pMailPort;
		CCodedItem	* m_pDomainName;
		CMailAddress    * m_pReverse;
		CCodedItem      * m_pTime1;
		CCodedItem      * m_pLogFile;
		CCodedItem      * m_pAuth;
		CCodedItem	* m_pUsername;
		CCodedItem      * m_pPassword;
		CCodedItem      * m_pSMS;
		CCodedItem      * m_pRelay;
		CCodedItem      * m_pOnSMS;
		UINT              m_DateForm;
		CMailContacts   * m_pContacts;
		CCodedItem      * m_pSSL;
		UINT		  m_Debug;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
