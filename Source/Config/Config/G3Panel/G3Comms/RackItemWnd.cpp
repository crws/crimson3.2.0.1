
#include "Intern.hpp"

#include "RackItemWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "RackItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Backplane Configuration View
//

// Dynamic Class

AfxImplementDynamicClass(CRackItemWnd, CProxyViewWnd);

// Constructor

CRackItemWnd::CRackItemWnd(void)
{
	m_fRecycle = FALSE;
	}

// Overridables

void CRackItemWnd::OnAttach(void)
{
	CRackItem  *pRack = (CRackItem *) m_pItem;

	CCommsPort *pPort = pRack->m_pPorts->GetItem(0U);

	m_pView = pPort->CreateView(viewNavigation);

	m_pView->Attach(pPort);
	}

// End of File
