
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagImport_HPP

#define INCLUDE_TagImport_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCodedHost;
class CCodedItem;
class CCodedText;
class CCommsDevice;
class CCommsPort;
class CCommsSystem;
class CDispFormatMulti;
class CLangManager;
class CLexicon;
class CTag;
class CTagFolder;
class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Tag Import Helper
//

class DLLNOT CTagImport : public IMakeTags
{
	public:
		// Constructor
		CTagImport(CCommsPort *pPort, CCommsDevice *pDevice);

		// Destructor
		~CTagImport(void);

		// Operations
		BOOL Import(void);

		// Management
		BOOL InitImport(CString Folder, BOOL fMore, CFilename File);
		void ImportDone(BOOL fSort);

		// Advanced
		BOOL GetInitData(CInitData * &pInit);
		void SetWriteBack(CString Code);

		// Folders
		BOOL AddRootFolder(CString Class, CString Root, CString Desc);
		void AddFolder(CString Class, CString Name, CString Desc);
		void AddFolder(CString Class, CString Name);
		void EndFolder(void);

		// Tag Creation
		BOOL AddReal(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Min, CString Max, UINT DP);
		BOOL AddInt (CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Min, CString Max, UINT DP, UINT TreatAs);
		BOOL AddFnum(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Enum);
		BOOL AddEnum(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, CString Enum);
		BOOL AddFlag(CString Tag, CString Label, CString Value, UINT Extent, UINT TreatAs, UINT Write, CString State0, CString State1);
		BOOL AddText(CString Tag, CString Label, CString Value, UINT Len, UINT Extent, UINT Write, UINT Encode);
		BOOL AddSpec(CString Tag, CString Label, CString Value, UINT Extent, UINT Write, UINT Spec);

	protected:
		// Data Members
		CCommsPort       * m_pPort;
		CCommsDevice     * m_pDevice;
		ICommsDriver     * m_pDriver;
		CCommsSystem     * m_pSystem;
		CTagList         * m_pList;
		CLangManager     * m_pLang;
		CString            m_DevName;
		CLexicon         * m_pLex;
		CTag	         * m_pLast;
		CArray <BYTE   > * m_pBlob;
		CArray <CString>   m_Folder;
		CArray <CString>   m_FClass;
		CArray <BOOL   >   m_IsUsed;
		CString		   m_Path;
		int                m_nPos;
		CInitData          m_Init;
		HGLOBAL		   m_hSave;
		CString		   m_Root;
		BOOL		   m_fMore;

		// Sort Context
		static CTagList * m_pSort;
		static int        m_nSort;

		// Implementation
		BOOL         AppendTag(CTag *pTag, CString Tag);
		BOOL         ValidatePath(CString Full);
		BOOL	     FindBlob(CMetaItem *pConfig);
		BOOL	     FindLexicon(void);
		BOOL         IsComms(CString const &Value);
		void         KillFolder(CString Folder);
		CTagFolder * EmitFolder(void);
		CCodedText * GetText(CCodedHost *pHost, CString Tag, CString Code);
		CCodedItem * Compile(CCodedHost *pHost, CString Tag, CString Code);
		CCodedItem * Compile(CCodedHost *pHost, CString Tag, CString Code, CLASS Class);
		void         LoadEnum(CDispFormatMulti *pFmt, CString Enum, BOOL fDefault);
		BOOL         PreserveData(CTreeFile &Tree, CTag *pTag);
		BOOL         PreserveProp(CTag *pTag, CMetaData const *pMeta);
		BOOL         RestoreData(void);
		BOOL         SortTags(CString Folder);

		// Sort Function
		static int SortFunc(PCVOID p1, PCVOID p2);
	};

// End of File

#endif
