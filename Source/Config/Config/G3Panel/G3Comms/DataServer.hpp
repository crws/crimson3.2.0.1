
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DataServer_HPP

#define INCLUDE_DataServer_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsManager;
class CCommsSystem;
class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Data Server
//

class DLLAPI CDataServer : public CObject, public IDataServer
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDataServer(CCommsSystem *pSystem);

		// Destructor
		~CDataServer(void);

		// Operations
		void ResetServer(void);

		// IBase Methods
		UINT Release(void);

		// IDataServer Methods
		DWORD GetData(DWORD ID, UINT Type, UINT Flags);
		void  SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data);
		DWORD GetProp(DWORD ID, WORD Prop, UINT Type);
		PVOID GetItem(DWORD ID);
		DWORD RunFunc(WORD  ID, UINT uParam, PDWORD pParam);

	protected:
		// Data Members
		CCommsSystem  * m_pSystem;
		CCommsManager * m_pComms;
		CTagList      * m_pTags;
	};

// End of File

#endif
