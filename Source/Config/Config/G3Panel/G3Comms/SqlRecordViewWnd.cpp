
#include "Intern.hpp"

#include "SqlRecordViewWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SQL Record View Window
//

// Dynamic Class

AfxImplementDynamicClass(CSqlRecordViewWnd, CViewWnd);

// Constructor

CSqlRecordViewWnd::CSqlRecordViewWnd(void)
{
	}

// Message Map

AfxMessageMap(CSqlRecordViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CSqlRecordViewWnd)
	};

// Message Handlers

void CSqlRecordViewWnd::OnPostCreate(void)
{
	m_System.Bind(this);
	}

void CSqlRecordViewWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	m_System.SetTwinMode(fShow);
	}

void CSqlRecordViewWnd::OnSetFocus(CWnd &Wnd)
{
	m_System.FlipToItemView(TRUE);
	}

void CSqlRecordViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.FillRect(GetClientRect(), afxBrush(TabFace));
	}

// End of File
