
#include "Intern.hpp"

#include "UITextExprNumeric.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable Numeric
//

// Dynamic Class

AfxImplementDynamicClass(CUITextExprNumeric, CUITextCoded);

// Constructor

CUITextExprNumeric::CUITextExprNumeric(void)
{
	}

// Overridables

void CUITextExprNumeric::OnBind(void)
{
	CUITextCoded::OnBind();

	ShowTwoWay();
	}

CString CUITextExprNumeric::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.GetLength() ) {

		if( Text.Right(4) == L" //=" ) {

			UINT uLen = Text.GetLength();

			return L'=' + Text.Left(uLen - 4);
			}
		}

	return Text;
	}

UINT CUITextExprNumeric::OnSetAsText(CError &Error, CString Text)
{
	if( Text.GetLength() ) {

		if( Text[0] == '=' ) {

			Text.Delete(0, 1);

			if( Text.IsEmpty() ) {

				Error.Set(CString(IDS_YOU_MUST_ENTER));

				return saveError;
				}

			Text += L" //=";

			return CUITextCoded::OnSetAsText(Error, Text);
			}

		PCTXT pt = Text;

		PTXT  e1 = NULL;

		PTXT  e2 = NULL;

		// cppcheck-suppress ignoredReturnValue

		wcstol(pt, &e1, 10);

		// cppcheck-suppress ignoredReturnValue

		wcstod(pt, &e2);

		if( (e1 && !*e1) || (e2 && !*e2) ) {

			return CUITextCoded::OnSetAsText(Error, Text);
			}

		return saveError;
		}

	return saveError;
	}

// End of File
