
#include "Intern.hpp"

#include "UITextExprString.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable String
//

// Dynamic Class

AfxImplementDynamicClass(CUITextExprString, CUITextCoded);

// Constructor

CUITextExprString::CUITextExprString(void)
{
	m_uFlags = textEdit;
}

// Overridables

void CUITextExprString::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	m_uWidth = 40;

	if( !List[0].IsEmpty() ) {

		CString Size = List[0];

		if( Size[0] == '*' ) {

			m_uFlags |= textHide;

			Size = Size.Mid(1);
		}

		m_uWidth = watoi(Size);

		m_uWidth = Min(m_uWidth, 50);
	}

	if( !List[1].IsEmpty() ) {

		m_uFlags |= textDefault;

		m_Default = List[1];

		C3OemStrings(m_Default);
	}

	if( !List[2].IsEmpty() ) {

		m_Params = List[2];
	}

	m_uLimit = 500;

	ShowTwoWay();
}

CString CUITextExprString::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.GetLength() ) {

		if( IsStringConst(Text) ) {

			Text = Text.Mid(1, Text.GetLength() - 2);

			Text.Replace(L"\\\"", L"\"");

			return Text;
		}

		return L'=' + Text;
	}

	return Text;
}

UINT CUITextExprString::OnSetAsText(CError &Error, CString Text)
{
	if( Text.GetLength() ) {

		if( Text[0] == '=' ) {

			Text.Delete(0, 1);

			if( Text.IsEmpty() ) {

				Error.Set(CString(IDS_YOU_MUST_ENTER));

				return saveError;
			}

			if( Text == L"\"\"" ) {

				Text.Empty();
			}

			return CUITextCoded::OnSetAsText(Error, Text);
		}

		Text.Replace(L"\"", L"\\\"");

		CString Expr = L'"' + Text + L'"';

		return CUITextCoded::OnSetAsText(Error, Expr);
	}

	return CUITextCoded::OnSetAsText(Error, Text);
}

// Implementation

BOOL CUITextExprString::IsStringConst(CString const &Text)
{
	if( Text[0] == L'"' ) {

		UINT uLen = Text.GetLength();

		if( uLen > 1 ) {

			if( Text[uLen-1] == L'"' ) {

				if( Text.Count(L'"') > 2 ) {

					for( UINT n = 1; n < uLen - 2; n++ ) {

						if( Text[n] == L'"' ) {

							if( Text[n-1] == '\\' ) {

								continue;
							}

							return FALSE;
						}
					}
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

// End of File
