
#include "Intern.hpp"

#include "ExpansionItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CascadedItem.hpp"
#include "CommsPortList.hpp"
#include "OptionCardItem.hpp"
#include "OptionCardList.hpp"
#include "OptionCardRackItem.hpp"
#include "OptionCardRackList.hpp"
#include "TetheredItem.hpp"
#include "UsbTreePath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Expansion Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CExpansionItem, CMetaItem);

// Constructor

CExpansionItem::CExpansionItem(void)
{
	m_pRack     = New COptionCardList;

	m_pCascaded = New CCascadedItem;

	m_pTethered = New CTetheredItem;

	m_pCascaded->m_Number = 1000;

	m_pTethered->m_Number = 1001;
	}

// Attributes

UINT CExpansionItem::GetTreeImage(void) const
{
	return IDI_RACK_GRAPHITE;
	}

CString CExpansionItem::GetTreeLabel(void) const
{
	return L"HUMAN";
	}

// Operations

void CExpansionItem::Validate(BOOL fExpand)
{
	m_pRack->Validate(fExpand);

	m_pCascaded->Validate(fExpand);

	m_pTethered->Validate(fExpand);
	}

void CExpansionItem::ClearSysBlocks(void)
{
	m_pRack->ClearSysBlocks();

	m_pCascaded->ClearSysBlocks();

	m_pTethered->ClearSysBlocks();
	}

void CExpansionItem::NotifyInit(void)
{
	m_pRack->NotifyInit();

	m_pCascaded->NotifyInit();

	m_pTethered->NotifyInit();
	}

void CExpansionItem::CheckMapBlocks(void)
{
	m_pRack->CheckMapBlocks();

	m_pCascaded->CheckMapBlocks();

	m_pTethered->CheckMapBlocks();
	}

UINT CExpansionItem::GetPowerBudget(void)
{
	return m_pRack->GetItemCount() ? m_pRack->GetItemCount() * 43 : 1;
	}

UINT CExpansionItem::GetPowerTotal(void)
{
	return m_pRack->GetTotalPower();
	}

UINT CExpansionItem::GetPowerUsage(void)
{
	return GetPowerTotal() * 100 / GetPowerBudget();
	}

BOOL CExpansionItem::CheckPower(void)
{
	return m_pRack->GetItemCount() > 1 ? GetPowerUsage() <= 100 : TRUE;
	}

BOOL CExpansionItem::CheckPower(CString &Warning)
{
	if( !CheckPower() || !m_pCascaded->CheckPower() || !m_pTethered->CheckPower()) {

		Warning =  L"You may have exceeded the hardware limitations "
			   L"with this configuration.\n\n"
			   L"Please read all technical documentation "
			   L"relating to the specific modules.";

		return FALSE;
		}

	return TRUE;
	}

UINT CExpansionItem::GetSlotCount(void)
{
	UINT uCount = 0;

	uCount += m_pRack->GetItemCount();

	uCount += m_pTethered->GetSlotCount();

	uCount += m_pCascaded->GetSlotCount();

	return uCount;
	}

UINT CExpansionItem::GetRackCount(void)
{
	UINT uCount = 0;

	uCount += (m_pTethered->m_pRacks->GetItemCount() ? m_pTethered->GetRackCount() + 1 : 0);

	uCount += (m_pCascaded->m_pRacks->GetItemCount() ? m_pCascaded->GetRackCount() + 1 : 0);

	return uCount;
	}

UINT CExpansionItem::AllocSlotNumber(void)
{
	for( UINT n = 1;; n++ ) {

		if( !FindSlot(n) ) {

			return n;
			}
		}
	}

UINT CExpansionItem::AllocRackNumber(void)
{
	for( UINT n = 1;; n++ ) {

		if( !FindRack(n) ) {

			return n;
			}
		}
	}

// Ports List Access

BOOL CExpansionItem::GetPortList(CCommsPortList * &pList, UINT uIndex) const
{
	if( m_pRack->GetPortList(pList, uIndex) ) {

		return TRUE;
		}

	uIndex -= m_pRack->GetItemCount();

	if( m_pCascaded->GetPortList(pList, uIndex) ) {

		return TRUE;
		}

	uIndex -= m_pCascaded->GetSlotCount();

	if( m_pTethered->GetPortList(pList, uIndex) ) {

		return TRUE;
		}

	pList = NULL;

	return FALSE;
	}

// Slot

COptionCardItem * CExpansionItem::FindSlot(CUsbTreePath const &Slot) const
{
	COptionCardItem *pOption;

	if( (pOption = m_pRack->Find(Slot)) ) {

		return pOption;
		}

	if( (pOption = m_pCascaded->FindSlot(Slot)) ) {

		return pOption;
		}

	if( (pOption = m_pTethered->FindSlot(Slot)) ) {

		return pOption;
		}

	return NULL;
	}

COptionCardItem * CExpansionItem::FindSlot(UINT Slot) const
{
	COptionCardItem *pOption;

	if( (pOption = m_pRack->Find(Slot)) ) {

		return pOption;
		}

	if( (pOption = m_pCascaded->FindSlot(Slot)) ) {

		return pOption;
		}

	if( (pOption = m_pTethered->FindSlot(Slot)) ) {

		return pOption;
		}

	return NULL;
	}

// Rack

COptionCardRackItem * CExpansionItem::FindRack(CUsbTreePath const &Rack) const
{
	COptionCardRackItem *pRack;

	if( (pRack = m_pCascaded->FindRack(Rack)) ) {

		return pRack;
		}

	if( (pRack = m_pTethered->FindRack(Rack)) ) {

		return pRack;
		}

	return NULL;
	}

COptionCardRackItem * CExpansionItem::FindRack(UINT Rack) const
{
	COptionCardRackItem *pRack;

	if( (pRack = m_pCascaded->FindRack(Rack)) ) {

		return pRack;
		}

	if( (pRack = m_pTethered->FindRack(Rack)) ) {

		return pRack;
		}

	return NULL;
	}

// Firmware

void CExpansionItem::GetFirmwareList(CArray<UINT> &List) const
{
	m_pRack->GetFirmwareList(List);

	m_pCascaded->GetFirmwareList(List);

	m_pTethered->GetFirmwareList(List);
	}

// Item Naming

CString CExpansionItem::GetHumanName(void) const
{
	return IDS_EXPANSION;
	}

// Persistance

void CExpansionItem::PostLoad(void)
{
	CMetaItem::PostLoad();

	UpgradeRack();
	}

// Conversion

void CExpansionItem::PostConvert(void)
{
	m_pCascaded->PostConvert();

	m_pTethered->PostConvert();
	}

// Download Support

BOOL CExpansionItem::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pRack);

	Init.AddItem(itemSimple, m_pCascaded);

	Init.AddItem(itemSimple, m_pTethered);

	return TRUE;
	}

// Meta Data Creation

void CExpansionItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(Rack);

	Meta_AddObject(Cascaded);

	Meta_AddObject(Tethered);
	}

// Implementation

void CExpansionItem::UpgradeRack(void)
{
	for( INDEX i = m_pRack->GetHead(); !m_pRack->Failed(i); m_pRack->GetNext(i) ) {

		COptionCardItem *p = m_pRack->GetItem(i);

		if( p->m_Number == NOTHING ) {

			p->m_Number = AllocSlotNumber();
			}
		}
	}

// End of File
