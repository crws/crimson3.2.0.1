
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextLangName_HPP

#define INCLUDE_UITextLangName_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Language Name
//

class CUITextLangName : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextLangName(void);

	protected:
		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(UINT Data, CString Text);
		void AddData(void);
	};

// End of File

#endif
