
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextDateSep_HPP

#define INCLUDE_UITextDateSep_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Date Separator
//

class CUITextDateSep : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextDateSep(void);

		// Overridables
		void OnBind(void);
	};

// End of File

#endif
