
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextTimeSep_HPP

#define INCLUDE_UITextTimeSep_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Time Separator
//

class CUITextTimeSep : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextTimeSep(void);

		// Overridables
		void OnBind(void);
	};

// End of File

#endif
