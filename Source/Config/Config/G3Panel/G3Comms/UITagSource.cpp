
#include "Intern.hpp"

#include "UITagSource.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Tag Source
//

// Dynamic Class

AfxImplementDynamicClass(CUITagSource, CUIExpression);

// Constructor

CUITagSource::CUITagSource(void)
{
	m_fEmpty = TRUE;
	}

// End of File
