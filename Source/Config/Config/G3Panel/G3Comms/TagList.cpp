
#include "Intern.hpp"

#include "TagList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "Tag.hpp"
#include "TagBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag List
//

// Dynamic Class

AfxImplementDynamicClass(CTagList, CNamedList);

// Constructor

CTagList::CTagList(void)
{
	}

// Destructor

CTagList::~CTagList(void)
{
	KillBlocks();
	}

// Item Access

CTag * CTagList::GetItem(INDEX Index) const
{
	return (CTag *) CItemIndexList::GetItem(Index);
	}

CTag * CTagList::GetItem(UINT uPos) const
{
	return (CTag *) CItemIndexList::GetItem(uPos);
	}

// Item Lookup

UINT CTagList::FindNamePos(CString Name) const
{
	CItem *pItem = FindByName(Name);

	if( pItem ) {

		CTag *pTag = (CTag *) pItem;

		return pTag->GetIndex();
		}

	return NOTHING;
	}

// Attributes

BOOL CTagList::HasCircular(void) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CTag *pTag = GetItem(n);

		if( pTag->IsCircular() ) {

			return TRUE;
			}

		GetNext(n);
		}

	return FALSE;
	}

// Operations

BOOL CTagList::FindCircular(CStringArray &List)
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CTag *pTag = GetItem(n);

		if( pTag->IsCircular() ) {

			if( pTag->m_pValue ) {

				CString Info = pTag->m_pValue->GetFindInfo();

				List.Append(Info);
				}
			else {

				// REV3 -- This is a bit lame as we just nav to the
				// tag if we've got a circular reference and it's
				// not in the value property.

				CString Info = pTag->GetFindInfo();

				List.Append(Info);
				}
			}

		GetNext(n);
		}

	return !List.IsEmpty();
	}

void CTagList::MakeLite(void)
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CTag *pTag = GetItem(n);

		pTag->MakeLite();

		GetNext(n);
		}
	}

// Persistance

void CTagList::PostLoad(void)
{
	CNamedList::PostLoad();

	MakeBlocks();
	}

void CTagList::Kill(void)
{
	KillBlocks();

	CNamedList::Kill();
	}

// Download Support

BOOL CTagList::MakeInitData(CInitData &Init)
{
	CItem::MakeInitData(Init);

	MakeBlocks();

	UINT uCount = m_Index.GetCount();

	UINT uHave  = m_Blocks.GetCount();

	Init.AddWord(WORD(uCount));

	Init.AddWord(WORD(uHave));

	for( UINT n = 0; n < uHave; n++ ) {

		CTagBlock *pBlock = m_Blocks[n];

		Init.AddWord(WORD(pBlock->m_Handle));
		}

	return TRUE;
	}

// Implementation

void CTagList::MakeBlocks(void)
{
	UINT uCount = m_Index.GetCount();

	UINT uLimit = 500;

	UINT uNeed  = (uCount + uLimit - 1) / uLimit;

	UINT uHave  = m_Blocks.GetCount();

	while( uHave < uNeed ) {

		UINT       uMin   = uHave * uLimit;

		UINT       uMax   = uMin  + uLimit;

		CTagBlock *pBlock = New CTagBlock(this, uMin, uMax);

		pBlock->SetParent(this);

		pBlock->Init();

		m_Blocks.Append(pBlock);

		uHave++;
		}
	}

void CTagList::KillBlocks(void)
{
	for( UINT n = 0; n < m_Blocks.GetCount(); n++ ) {

		CTagBlock *pBlock = m_Blocks[n];

		pBlock->Kill();

		delete pBlock;
		}

	m_Blocks.Empty();
	}

// End of File
