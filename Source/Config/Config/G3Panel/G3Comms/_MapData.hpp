
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_MapData_HPP

#define INCLUDE_MapData_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping Data
//

class CMapData
{
	public:
		// Data Members
		BOOL	       m_fPart;
		CCommsDevice * m_pDevice;
		CAddress       m_Addr;
	};

// End of File

#endif
