
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_MemoryStickItem_HPP

#define INCLUDE_MemoryStickItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "USBPortItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Memory Stick Configuration
//

class DLLNOT CMemoryStickItem : public CUSBPortItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMemoryStickItem(void);

		// Initial Values
		void SetInitValues(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetTreeImage(void) const;
		UINT GetType(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Enable;
		UINT         m_Update;
		CCodedItem * m_pFolder;
		CCodedItem * m_pConfig;
		UINT         m_Reboot;
		UINT         m_Mode1;
		UINT         m_Dir1;
		UINT         m_All1;
		CString      m_Source1;
		CString      m_Target1;
		UINT         m_Mode2;
		UINT         m_Dir2;
		UINT         m_All2;
		CString      m_Source2;
		CString      m_Target2;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
