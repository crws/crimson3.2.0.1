
#include "Intern.hpp"

#include "UITextIntlString.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"

#include "IntlEditItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- International String
//

// Dynamic Class

AfxImplementDynamicClass(CUITextIntlString, CUITextExprString);

// Constructor

CUITextIntlString::CUITextIntlString(void)
{
	m_Class  = AfxRuntimeClass(CCodedText);

	m_uFlags = textEdit | textExpand | textLocking;
	}

// Overridables

BOOL CUITextIntlString::OnExpand(CWnd &Wnd)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( !Text.GetLength() || IsStringConst(Text) ) {

		Text.Replace(L"\\\"", L"\"");

		CIntlEditItem Item;

		Item.SetParent(m_pItem);

		Item.Init();

		Item.LoadStrings(Text.Mid(1, Text.GetLength() - 2));

		CItemDialog Dialog(&Item, CString(IDS_TRANSLATE_STRING));

		Dialog.Resize();

		if( Dialog.Execute(Wnd) ) {

			Text = Item.ReadStrings();

			Text.Replace(L"\"", L"\\\"");

			Text = L'\"' + Text + L'\"';

			CUITextCoded::OnSetAsText(CError(FALSE), Text);

			return TRUE;
			}
		}
	else {
		CString Text;

		Text = CString(IDS_EXPRESSIONS);

		Wnd.Error(Text);
		}

	return FALSE;
	}

// End of File
