
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_USBPortItem_HPP

#define INCLUDE_USBPortItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Item Base Class
//

class DLLNOT CUSBPortItem : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUSBPortItem(void);

		// Attributes
		virtual UINT GetTreeImage(void) const;
		virtual UINT GetType(void) const;

		// Enumeration
		enum {
			typeNull,
			typeStick,
			typeKeyboard,
			typeMouse,
			};
	};

// End of File

#endif
