
#include "Intern.hpp"

#include "SqlColumn.hpp"
#include "SqlColumnList.hpp"
#include "SqlFilterItemDialog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Item Dialog
//

// Dynamic Class

AfxImplementDynamicClass(CSqlFilterItemDialog, CItemDialog);

// Constructors

CSqlFilterItemDialog::CSqlFilterItemDialog(void)
	: CItemDialog(NULL)
{
	m_pFilter = NULL;
	}


CSqlFilterItemDialog::CSqlFilterItemDialog(CItem *pItem, CString Caption)
	: CItemDialog(pItem, Caption)
{
	m_pFilter = (CSqlFilter *) pItem;
	}

// Message Map

AfxMessageMap(CSqlFilterItemDialog, CItemDialog)
{
	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CSqlFilterItemDialog)
	};

// Message Handler

BOOL CSqlFilterItemDialog::OnOkay(UINT uId)
{
	CError Error;

	if( m_pFilter && m_pFilter->IsValid(Error) ) {

		CSqlQuery   *pQuery = (CSqlQuery *) m_pFilter->GetParent(AfxRuntimeClass(CSqlQuery));

		CSqlColumn *pColumn = pQuery->GetColumn(m_pFilter->m_Column);

		if( pColumn ) {

			m_pFilter->m_ColName = pColumn->m_SqlName;
		
			return CItemDialog::OnOkay(uId);
			}
		}

	Error.Show(ThisObject);

	return TRUE;
	}

// End of File
