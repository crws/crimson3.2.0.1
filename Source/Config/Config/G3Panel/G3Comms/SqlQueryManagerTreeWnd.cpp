
#include "Intern.hpp"

#include "SqlQueryManagerTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsMapData.hpp"
#include "SqlColumn.hpp"
#include "SqlColumnList.hpp"
#include "SqlQuery.hpp"
#include "SqlQueryList.hpp"
#include "SqlQueryManager.hpp"
#include "SqlQueryManagerTreeWnd_CCmdCreate.hpp"
#include "SqlQueryManagerTreeWnd_CCmdDelete.hpp"
#include "SqlQueryManagerTreeWnd_CCmdRename.hpp"
#include "SqlRecord.hpp"
#include "SqlRecordList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Manager Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CSqlQueryManagerTreeWnd, CStdTreeWnd);

// Static Data

UINT CSqlQueryManagerTreeWnd::m_timerDouble = AllocTimerID();

UINT CSqlQueryManagerTreeWnd::m_timerSelect = AllocTimerID();

UINT CSqlQueryManagerTreeWnd::m_timerScroll = AllocTimerID();

// Constructor

CSqlQueryManagerTreeWnd::CSqlQueryManagerTreeWnd(void)
{
	m_dwStyle = m_dwStyle | TVS_HASBUTTONS;

	m_dwStyle = m_dwStyle | TVS_EDITLABELS | TVS_SHOWSELALWAYS;

	m_cfCode  = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_fMulti  = FALSE;

	m_fLock   = FALSE;

	m_uDrop   = dropNone;

	m_uLocked = 0;
	}

// IUnknown

HRESULT CSqlQueryManagerTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
	}

ULONG CSqlQueryManagerTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
	}

ULONG CSqlQueryManagerTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
	}

// IUpdate

HRESULT METHOD CSqlQueryManagerTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			RefreshFrom(hItem);
			}

		return S_OK;
		}

	if( uType == updateProps ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			CMetaItem *pMeta = (CMetaItem *) pItem;

			if( TRUE ) {

				CTreeViewItem Node(hItem);

				LoadNodeItem(Node, pMeta);

				m_pTree->SetItem(Node);
				}
			}

		return S_OK;
		}

	if( uType == updateRename ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CSqlRecord)) ) {

				CSqlRecord *pRecord = (CSqlRecord *) pItem;

				m_pTree->SetItemText(hItem, pRecord->GetTreeLabel());
				}
			}

		return S_OK;
		}

	return S_OK;
	}

// IDropTarget

HRESULT CSqlQueryManagerTreeWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( !IsReadOnly() ) {

		CanAcceptDataObject(pData, m_uDrop);

		m_hDropRoot = NULL;

		m_hDropPrev = NULL;

		m_fDropMove = FALSE;

		BOOL fMove  = IsDropMove(dwKeys, pEffect);

		DropTrack(CPoint(pt.x, pt.y), fMove);

		if( IsValidDrop() ) {

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CSqlQueryManagerTreeWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		BOOL fMove = IsDropMove(dwKeys, pEffect);

		DropTrack(CPoint(pt.x, pt.y), fMove);

		if( IsValidDrop() ) {

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CSqlQueryManagerTreeWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( m_uDrop ) {

		ShowDrop(FALSE);

		m_System.HidePaneOnDrop(0);

		m_uDrop = dropNone;

		return S_OK;
		}

	return S_OK;
	}

HRESULT CSqlQueryManagerTreeWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop ) {

		ShowDrop(FALSE);

		if( m_uDrop != dropOther ) {

			DropDone(pData);

			SetFocus();
			}

		m_uDrop = dropNone;

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

// Overridables

void CSqlQueryManagerTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	m_pManager = (CSqlQueryManager *) m_pItem;
	}

CString CSqlQueryManagerTreeWnd::OnGetNavPos(void)
{
	if( m_pSelect ) {

		return m_SelPath;
		}

	return m_pItem->GetFixedPath();
	}

BOOL CSqlQueryManagerTreeWnd::OnNavigate(CString const &Nav)
{
	if( Nav.IsEmpty() ) {

		m_pTree->SelectItem(m_hRoot);

		return TRUE;
		}
	else {
		CString Find = Nav;

		while( !Find.IsEmpty() ) {

			HTREEITEM hFind = m_MapFixed[Find];

			if( hFind ) {

				m_pTree->SelectItem(hFind);

				m_System.SetViewedItem(m_pSelect);

				return TRUE;
				}

			Find = Find.Left(Find.FindRev('/'));
			}

		return FALSE;
		}
	}

void CSqlQueryManagerTreeWnd::OnExec(CCmd *pCmd)
{
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnExecCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		m_fLock = !m_fLock;

		LockUpdate(m_fLock);
		}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		HTREEITEM hItem = m_MapFixed[pCmd->m_Item];

		if( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			if( pItem ) {

				((CCmdItem *) pCmd)->Exec(pItem);

				RefreshFrom(hItem);

				ItemUpdated(pItem, updateProps);

				m_fRefresh = TRUE;
				}
			}
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnExecRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnExecDelete((CCmdDelete *) pCmd);
		}
	}

void CSqlQueryManagerTreeWnd::OnUndo(CCmd *pCmd)
{
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		m_fLock = !m_fLock;

		LockUpdate(m_fLock);
		}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		HTREEITEM hItem = m_MapFixed[pCmd->m_Item];

		if( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			if( pItem ) {

				((CCmdItem *) pCmd)->Undo(pItem);

				RefreshFrom(hItem);

				ItemUpdated(pItem, updateProps);

				m_fRefresh = TRUE;
				}
			}
		}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnUndoCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnUndoRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnUndoDelete((CCmdDelete *) pCmd);
		}
	}

// Message Map

AfxMessageMap(CSqlQueryManagerTreeWnd, CStdTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit )
	AfxDispatchNotify(100, TVN_ENDLABELEDIT,   OnTreeEndEdit   )

	AfxDispatchControlType(IDM_GO,    OnGoControl  )
	AfxDispatchCommandType(IDM_GO,    OnGoCommand  )
	AfxDispatchGetInfoType(IDM_ITEM,  OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM,  OnItemControl)
	AfxDispatchCommandType(IDM_ITEM,  OnItemCommand)
	AfxDispatchControlType(IDM_EDIT,  OnEditControl)
	AfxDispatchCommandType(IDM_EDIT,  OnEditCommand)
	AfxDispatchGetInfoType(IDM_SQL,   OnSqlGetInfo )
	AfxDispatchControlType(IDM_SQL,   OnSqlControl )
	AfxDispatchCommandType(IDM_SQL,   OnSqlCommand )

	AfxMessageEnd(CSqlQueryManagerTreeWnd)
	};

// Message Handlers

void CSqlQueryManagerTreeWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
	}

void CSqlQueryManagerTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"SqlQueryManagerTreeTool"));
		}
	}

void CSqlQueryManagerTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( fShow ) {

		m_System.SetNavCheckpoint();

		m_System.SetViewedItem(m_pSelect);
		}
	}

void CSqlQueryManagerTreeWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerDouble ) {

		m_System.FlipToItemView(TRUE);

		KillTimer(uID);
		}

	if( uID == m_timerSelect ) {

		if( m_uDrop ) {

			if( m_hDropRoot ) {

				if( m_uDrop != dropOther ) {

					ShowDrop(FALSE);
					}

				if( IsExpandingDrop() ) {

					ExpandItem(m_hDropRoot);

					m_pTree->UpdateWindow();
					}
				else {
					m_pTree->SelectItem(m_hDropRoot);

					m_pTree->UpdateWindow();
					}

				if( m_uDrop != dropOther ) {

					ShowDrop(TRUE);
					}
				}
			}

		KillTimer(uID);
		}

	if( uID == m_timerScroll ) {

		if( m_uDrop ) {

			UINT  uCmd = NOTHING;

			CRect View = m_pTree->GetClientRect();

			ClientToScreen(View);

			if( View.PtInRect(m_DragPos) ) {

				View -= 40;

				if( m_DragPos.y < View.top ) {

					int nLimit = m_pTree->GetScrollRangeMin(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) > nLimit ) {

						uCmd = SB_LINEUP;
						}
					}

				if( m_DragPos.y > View.bottom ) {

					int nLimit = m_pTree->GetScrollRangeMax(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) < nLimit ) {

						uCmd = SB_LINEDOWN;
						}
					}

				if( uCmd < NOTHING ) {

					ShowDrop(FALSE);

					m_pTree->SendMessage(WM_VSCROLL, uCmd);

					m_pTree->UpdateWindow();

					ShowDrop(TRUE);

					return;
					}
				}
			}

		KillTimer(uID);
		}
	}

// Notification Handlers

BOOL CSqlQueryManagerTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{
	return !CanItemRename();
	}

BOOL CSqlQueryManagerTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
		}

	return FALSE;
	}

// Command Handlers

BOOL CSqlQueryManagerTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID == IDM_ITEM_RENAME ) {

		Info.m_Image = MAKELONG(0x0021, 0x1000);
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	if( !m_pSelect ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_ITEM_DELETE:

			Src.EnableItem(CanItemDelete());

			return TRUE;

		case IDM_ITEM_RENAME:

			Src.EnableItem(CanItemRename());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_DELETE:

			OnItemDelete(IDS_DELETE_2, TRUE, TRUE);

			return TRUE;

		case IDM_ITEM_RENAME:

			m_pTree->EditLabel(m_hSelect);

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_GO_NEXT_1:
		case IDM_GO_NEXT_2:

			Src.EnableItem(m_pTree->GetNextNode(m_hSelect, TRUE) ? TRUE : FALSE);

			return TRUE;

		case IDM_GO_PREV_1:
		case IDM_GO_PREV_2:

			Src.EnableItem(m_pTree->GetPrevNode(m_hSelect, TRUE) ? TRUE : FALSE);

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnGoCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GO_NEXT_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_NEXT_1:

			OnGoNext(TRUE);

			return TRUE;

		case IDM_GO_PREV_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_PREV_1:

			OnGoPrev(TRUE);

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_PASTE:

			Src.EnableItem(CanEditPaste());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_PASTE:

			OnEditPaste();

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnSqlGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case IDM_SQL_ADD_QUERY:

			Info.m_Image   = 0x40000000;

			Info.m_ToolTip = CString(IDS_NEW_QUERY);

			Info.m_Prompt  = CString(IDS_ADD_NEW_SQL_QUERY);

			return TRUE;

		case IDM_SQL_ADD_COLUMN:

			Info.m_Image   = 0x40000014;

			Info.m_ToolTip = CString(IDS_NEW_COLUMN);

			Info.m_Prompt  = CString(IDS_ADD_NEW_COLUMN_TO);

			return TRUE;

		case IDM_SQL_CLEAR:

			Info.m_Image   = 0x10000038;

			Info.m_ToolTip = CString(IDS_CLEAR_RECORD);

			Info.m_Prompt  = CString(IDS_CLEAR_MAPPING);

			return TRUE;

		case IDM_SQL_CLEAR_COL:

			Info.m_Image   = 0x10000038;

			Info.m_ToolTip = CString(IDS_CLEAR_COL_ALL);

			Info.m_Prompt  = CString(IDS_CLEAR_MAPPING);

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnSqlControl(UINT uID, CCmdSource &Src)
{
	if( !m_pSelect ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_SQL_ADD_QUERY:

			Src.EnableItem(CanSqlAddQuery());

			return TRUE;

		case IDM_SQL_ADD_COLUMN:

			Src.EnableItem(CanSqlAddColumn());

			return TRUE;

		case IDM_SQL_CLEAR:

			Src.EnableItem(CanSqlClear());

			return TRUE;

		case IDM_SQL_CLEAR_COL:

			Src.EnableItem(CanSqlClearCol());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnSqlCommand(UINT uID)
{
	switch( uID ) {

		case IDM_SQL_ADD_QUERY:

			OnSqlAddQuery();

			return TRUE;

		case IDM_SQL_ADD_COLUMN:

			OnSqlAddColumn();

			return TRUE;

		case IDM_SQL_CLEAR:

			OnSqlClear();

			return TRUE;

		case IDM_SQL_CLEAR_COL:

			OnSqlClearCol();

			return TRUE;
		}

	return FALSE;
	}

// SQL Menu

BOOL CSqlQueryManagerTreeWnd::CanSqlAddQuery(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlQueryManager)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Go Menu

BOOL CSqlQueryManagerTreeWnd::OnGoNext(BOOL fSiblings)
{
	HTREEITEM hItem = m_pTree->GetNextNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnGoPrev(BOOL fSiblings)
{
	HTREEITEM hItem = m_pTree->GetPrevNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::StepAway(void)
{
	HTREEITEM hItem;

	if( (hItem = m_pTree->GetNext(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	if( (hItem = m_pTree->GetPrevious(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	if( (hItem = m_pTree->GetParent(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::CanSqlAddColumn(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::CanSqlClear(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlRecord)) ) {

			CSqlRecord * pRecord = (CSqlRecord *) m_pSelect;

			if( pRecord->IsMapped() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::CanSqlClearCol(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlColumn)) ) {

			CSqlColumn * pColumn = (CSqlColumn *) m_pSelect;

			return pColumn->CanClear();
			}
		}

	return FALSE;
	}

void CSqlQueryManagerTreeWnd::OnSqlAddQuery(void)
{
	CSqlQueryManager *pManager = (CSqlQueryManager *) m_pSelect;

	CString Name = pManager->FindUnusedName();

	OnCreateItem(New CSqlQuery(Name));

	HTREEITEM hSelect = m_hSelect;

	OnSqlAddColumn();

	m_pTree->SelectItem(hSelect);
	}

void CSqlQueryManagerTreeWnd::OnSqlAddColumn(void)
{
	CSqlQuery *pQuery = (CSqlQuery *) m_pSelect;

	CString Name = pQuery->GetUnusedName();

	UINT uCol = pQuery->GetNextPos();

	OnCreateItem(New CSqlColumn(Name, uCol, pQuery->GetRowCount()));
	}

void CSqlQueryManagerTreeWnd::OnSqlClear(void)
{
	CSqlRecord * pRecord = (CSqlRecord *) m_pSelect;

	HGLOBAL hPrev = pRecord->TakeSnapshot();

	pRecord->ClearMapping();

	CCmdItem *pCmd = New CCmdItem(CString(IDS_CLEAR_MAPPING_2), pRecord, hPrev);

	m_System.SaveCmd(pCmd);

	OnGoNext(FALSE);
	}

void CSqlQueryManagerTreeWnd::OnSqlClearCol(void)
{
	CSqlColumn * pColumn = (CSqlColumn *) m_pSelect;

	HGLOBAL hPrev = pColumn->TakeSnapshot();

	pColumn->ClearAllMappings();

	CCmdItem *pCmd = New CCmdItem(CString(IDS_CLEAR_MAPPING_2), pColumn, hPrev);

	m_System.SaveCmd(pCmd);
	}

void CSqlQueryManagerTreeWnd::OnCreateItem(CMetaItem *pItem)
{
	HTREEITEM        hRoot = m_hSelect;

	CMetaItem      * pRoot = m_pSelect;

	CString          List  = GetItemList(pRoot);

	CItemIndexList * pList = pRoot->FindIndexList(List);

	pList->AppendItem(pItem);

	m_System.SaveCmd(New CCmdCreate(pRoot, List, pItem));

	LockUpdate(TRUE);

	HTREEITEM hItem = LoadItem(hRoot, NULL, pItem);

	ExpandItem(hItem);

	m_pTree->SelectItem(hItem);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
	}

void CSqlQueryManagerTreeWnd::OnExecCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	pItem->SetParent(pList);

	pList->AppendItem(pItem);

	LockUpdate(TRUE);

	LoadItem(hRoot, NULL, pItem);

	ExpandItem(hRoot);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
	}

void CSqlQueryManagerTreeWnd::OnUndoCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	HTREEITEM        hItem = m_MapFixed[pCmd->m_Made];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	pList->DeleteItem(pItem);

	KillTree(hItem, TRUE);

	ListUpdated(hRoot);
	}

// Item Menu : Delete

BOOL CSqlQueryManagerTreeWnd::CanItemDelete(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

			return TRUE;
			}

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlColumn)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CSqlQueryManagerTreeWnd::OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop)
{
	CMetaItem      * pRoot = (CMetaItem *) m_pSelect->GetParent(2);

	CString          List  = GetItemList(pRoot);

	CItemIndexList * pList = pRoot->FindIndexList(List);

	INDEX            Index = pList->FindItemIndex(m_pSelect);

	CItem          * pNext = pList->GetNext(Index) ? pList->GetItem(Index) : NULL;

	CCmd           * pCmd  = New CCmdDelete(pRoot, List, pNext, m_pSelect);

	m_System.ExecCmd(pCmd);
	}

void CSqlQueryManagerTreeWnd::OnExecDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM        hItem = m_hSelect;

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	StepAway();

	pList->DeleteItem(pItem);
	
	KillTree(hItem, TRUE);

	ListUpdated(hRoot);
	}

void CSqlQueryManagerTreeWnd::OnUndoDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM        hNext = m_MapFixed[pCmd->m_Next];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pNext = GetItemOpt(hNext);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	pList->InsertItem(pItem, pNext);

	LockUpdate(TRUE);

	HTREEITEM hItem = LoadItem(hRoot, hNext, pItem);

	m_pTree->SelectItem(hItem);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
	}

// Item Menu : Rename

BOOL CSqlQueryManagerTreeWnd::CanItemRename(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::OnItemRename(CString Name)
{
	CString Prev = m_pNamed->GetName();

	if( wstrcmp(Name, Prev) ) {

		if( !CheckItemName(Name) ) {

			CString Text = CFormat(IDS_THE_NAME, Name);

			Error(Text);

			return FALSE;
			}

		if( m_pNamed->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

			CError Error(TRUE);

			if( !C3ValidateName(Error, Name) ) {

				Error.Show(ThisObject);

				return FALSE;
				}
			}

		if( m_pNamed->IsKindOf(AfxRuntimeClass(CSqlColumn)) ) {

			CError Error(TRUE);

			if( !C3ValidateName(Error, Name) ) {

				Error.Show(ThisObject);

				return FALSE;
				}
			}

		CCmd *pCmd = New CCmdRename(m_pNamed, Name);

		m_System.ExecCmd(pCmd);

		return TRUE;
		}

	return FALSE;
	}

void CSqlQueryManagerTreeWnd::OnExecRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Name, pCmd->m_Prev);
	}

void CSqlQueryManagerTreeWnd::OnUndoRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Prev, pCmd->m_Name);
	}

void CSqlQueryManagerTreeWnd::RenameItem(CString Name, CString Prev)
{
	m_pNamed->SetName(Name);

	m_pNamed->SetDirty();

	m_pTree->SetItemText(m_hSelect, Name);

	m_System.ItemUpdated(m_pSelect, updateRename);
	}

BOOL CSqlQueryManagerTreeWnd::CheckItemName(CString const &Name)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

		CSqlQueryList *pList = m_pManager->m_pQueries;

		for( INDEX i = pList->GetHead(); !pList->Failed(i); pList->GetNext(i) ) {

			CSqlQuery *pQuery = pList->GetItem(i);

			if( pQuery->m_Name == Name ) {

				if( pQuery == m_pSelect ) {

					return TRUE;
					}

				return FALSE;
				}
			}

		return TRUE;
		}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlColumn)) ) {

		CSqlColumn     * pColumn = (CSqlColumn     *) m_pSelect;

		CSqlColumnList * pList   = (CSqlColumnList *) pColumn->GetParent();

		for( INDEX i = pList->GetHead(); !pList->Failed(i); pList->GetNext(i) ) {

			CSqlColumn *pColumn = pList->GetItem(i);

			if( pColumn->m_Name == Name ) {

				if( pColumn == m_pSelect ) {

					return TRUE;
					}

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// Edit Menu : Paste

BOOL CSqlQueryManagerTreeWnd::CanEditPaste(void)
{
	if( !IsReadOnly() ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			UINT uType;

			if( CanAcceptDataObject(pData, uType) ) {

				pData->Release();

				if( uType == dropFragment || uType == dropPartial ) {

					if( m_pSelect->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

						return TRUE;
						}

					return FALSE;
					}

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

void CSqlQueryManagerTreeWnd::OnEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		UINT uType;

		if( CanAcceptDataObject(pData, uType) ) {

			if( uType == dropFragment ) {

				CString Text;

				if( AcceptCodeFragment(Text, pData) ) {

					MapRecord(Text, TRUE);
					}

				CClipboard(ThisObject).Empty();
				}
			}

		pData->Release();
		}
	}

// Data Object Acceptance

BOOL CSqlQueryManagerTreeWnd::CanAcceptDataObject(IDataObject *pData, UINT &uType)
{
	if( CanAcceptCodeFragment(pData) ) {

		uType = dropFragment;

		return TRUE;
		}

	uType = dropOther;

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::CanAcceptCodeFragment(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfCode), NULL, 1, -1, TYMED_HGLOBAL };

	return pData->QueryGetData(&Fmt) == S_OK;
	}

BOOL CSqlQueryManagerTreeWnd::CanAcceptPartial(IDataObject *pData)
{
	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::AcceptCodeFragment(CString &Text, IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfCode), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::AcceptPartial(CCommsMapData const * &pMapData, IDataObject *pData)
{
	return FALSE;
	}

// Tree Loading

void CSqlQueryManagerTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TagsTreeIcon16"), afxColor(MAGENTA));
	}

void CSqlQueryManagerTreeWnd::LoadTree(void)
{
	LoadRoot();

	LoadNodeKids(m_hRoot, m_pItem);

	SelectRoot();
	}

void CSqlQueryManagerTreeWnd::LoadRoot(void)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, m_pItem);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);

	AddToMap(m_pItem, m_hRoot);
	}

HTREEITEM CSqlQueryManagerTreeWnd::LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	HTREEITEM hPrev = hNext ? m_pTree->GetPrevious(hNext) : TVI_LAST;

	HTREEITEM hUsed = hPrev ? hPrev : TVI_FIRST;

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, hUsed, Node);

	AddToMap(pItem, hItem);

	LoadNodeKids(hItem, pItem);

	return hItem;
	}

void CSqlQueryManagerTreeWnd::LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		Node.SetText(m_pItem->GetHumanName());

		Node.SetParam(LPARAM(m_pItem));

		Node.SetImages(IDI_SQL_MANAGER);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

		CSqlQuery *pQuery = (CSqlQuery *) pItem;

		Node.SetText(pQuery->m_Name);

		Node.SetParam(LPARAM(pItem));
		
		Node.SetImages(pQuery->GetImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CSqlColumn)) ) {

		CSqlColumn *pColumn = (CSqlColumn *) pItem;

		Node.SetText(pColumn->m_SqlName);

		Node.SetParam(LPARAM(pItem));

		Node.SetImages(IDI_SQL_SYNC);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CSqlRecord)) ) {

		CSqlRecord *pRecord = (CSqlRecord *) pItem;

		Node.SetText(pRecord->GetTreeLabel());

		Node.SetParam(LPARAM(pItem));

		Node.SetImages(IDI_BLOCK_GREEN);

		return;
		}
	}

void CSqlQueryManagerTreeWnd::LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		CSqlQueryManager *pManager = (CSqlQueryManager *) pItem;

		LoadList(hRoot, pManager->m_pQueries);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

		CSqlQuery *pQuery = (CSqlQuery *) pItem;

		LoadList(hRoot, pQuery->m_pCols);

		ExpandItem(hRoot);
		
		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CSqlColumn)) ) {

		CSqlColumn *pColumn = (CSqlColumn *) pItem;

		LoadList(hRoot, pColumn->m_pRecords);

		ExpandItem(hRoot);

		return;
		}
	}

void CSqlQueryManagerTreeWnd::LoadList(HTREEITEM hRoot, CItemList *pList)
{
	for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

		CMetaItem *pItem = (CMetaItem *) pList->GetItem(n);

		LoadItem(hRoot, NULL, pItem);
		}
	}

HTREEITEM CSqlQueryManagerTreeWnd::RefreshFrom(HTREEITEM hItem)
{
	if( !m_uLocked ) {

		LockUpdate(TRUE);

		CString    State = SaveSelState();

		CMetaItem *pItem = GetItemPtr(hItem);

		HTREEITEM  hNext = m_pTree->GetNext(hItem);

		HTREEITEM  hRoot = m_pTree->GetParent(hItem);

		KillTree(hItem, TRUE);

		hItem = LoadItem(hRoot, hNext, pItem);

		ExpandItem(hRoot);

		ExpandItem(hItem);

		LoadSelState(State);

		SkipUpdate();

		return hItem;
		}

	m_fRefresh = TRUE;

	return hItem;
	}

// Item Mapping

void CSqlQueryManagerTreeWnd::AddToMap(CMetaItem *pItem, HTREEITEM hItem)
{
	BOOL fTest = m_MapFixed.Insert(pItem->GetFixedPath(), hItem);

	AfxAssert(fTest);
	}

void CSqlQueryManagerTreeWnd::RemoveFromMap(CMetaItem *pItem)
{
	m_MapFixed.Remove(pItem->GetFixedPath());
	}

// Item Hooks

void CSqlQueryManagerTreeWnd::NewItemSelected(void)
{
	if( !m_uLocked ) {

		m_System.SetViewedItem(m_pSelect);
		}

	afxThread->SetStatusText(L"");
	}

UINT CSqlQueryManagerTreeWnd::GetRootImage(void)
{
	return 0;
	}

UINT CSqlQueryManagerTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return 0;
	}

BOOL CSqlQueryManagerTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"SqlQueryManagerTreeCtxMenu";

		return TRUE;
		}

	return FALSE;
	}

void CSqlQueryManagerTreeWnd::KillItem(HTREEITEM hItem, BOOL fRemove)
{
	if( fRemove ) {

		if( hItem == m_hRoot ) {

			m_MapFixed.Empty();

			return;
			}

		INDEX Index = m_MapFixed.FindData(hItem);

		if( !m_MapFixed.Failed(Index) ) {

			m_MapFixed.Remove(Index);

			return;
			}
		}
	}

// Drag Hooks

DWORD CSqlQueryManagerTreeWnd::FindDragAllowed(void)
{
	if( !IsReadOnly() ) {

		if( m_uDrag == dragMapping ) {

			return DROPEFFECT_LINK;
			}
		}

	return DROPEFFECT_NONE;
	}

// Drop Support

void CSqlQueryManagerTreeWnd::DropTrack(CPoint Pos, BOOL fMove)
{
	m_DragPos = Pos;

	Pos   -= m_DragOffset;

	Pos.y += m_DragSize.cy / 2;

	m_pTree->ScreenToClient(Pos);

	if( m_pTree->GetClientRect().PtInRect(Pos) ) {

		HTREEITEM hItem = m_pTree->HitTestItem(Pos);

		HTREEITEM hRoot = NULL;

		HTREEITEM hPrev = NULL;

		if( m_uDrop == dropData ) {

			if( !hRoot && hItem == m_hRoot ) {

				hRoot = m_hRoot;

				hPrev = NULL;
				}

			if( !hRoot && hItem ) {

				if( IsParent(hItem) ) {

					if( !m_pTree->IsExpanded(hItem) ) {

						CRect Rect = m_pTree->GetItemRect(hItem, TRUE);

						if( Pos.y > Rect.top + 8 ) {

							hRoot = hItem;

							hPrev = NULL;
							}
						}
					}
				}

			if( !hRoot ) {

				HTREEITEM hScan, hBack;

				if( hItem ) {

					hScan = m_pTree->GetPreviousVisible(hItem);

					hBack = m_pTree->GetParent(hItem);
					}
				else {
					hScan = hItem;

					hBack = NULL;

					WalkToLast(hScan);
					}

				if( hScan != hBack ) {

					while( !m_pTree->GetNext(hScan) ) {

						HTREEITEM hNext = m_pTree->GetParent(hScan);

						CRect     Rect  = m_pTree->GetItemRect(hScan, TRUE);

						if( hNext == m_hRoot || Pos.x >= Rect.left - 24 ) {

							hPrev = hScan;

							hRoot = hNext;

							break;
							}

						hScan = hNext;
						}
					}
				}

			if( !hRoot && hItem ) {

				hRoot = m_pTree->GetParent  (hItem);

				hPrev = m_pTree->GetPrevious(hItem);
				}
			}
		else {
			hRoot = hItem;

			hPrev = NULL;
			}

		if( m_hDropRoot != hRoot || m_hDropPrev != hPrev || m_fDropMove != fMove ) {

			ShowDrop(FALSE);

			m_hDropRoot = hRoot;

			m_hDropPrev = hPrev;

			m_fDropMove = fMove;

			if( IsExpandingDrop() || m_uDrop == dropOther ) {

				SetTimer (m_timerSelect, 300);
				}
			else
				KillTimer(m_timerSelect);

			ShowDrop(TRUE);

			DropDebug();
			}
		}

	SetTimer(m_timerScroll, 10);
	}

BOOL CSqlQueryManagerTreeWnd::DropDone(IDataObject *pData)
{
	if( m_uDrop == dropFragment ) {

		CString Text;

		if( AcceptCodeFragment(Text, pData) ) {

			m_pTree->SelectItem(m_hDropRoot);

			BOOL fArray = FALSE;

			UINT uPos   =  NOTHING;

			if( (uPos = Text.Find('[')) < NOTHING ) {

				fArray = TRUE;
				}

			BOOL fMulti = FALSE;

			if( fArray ) {

				UINT uStart = watoi(Text.Mid(uPos+1));

				UINT uCount = 1;

				HTREEITEM hScan = m_hSelect;

				while( hScan = m_pTree->GetNext(hScan) ) {

					if( m_pTree->GetChild(hScan) ) {

						break;
						}

					uCount++;
					}

				CStringDialog Dlg;

				Dlg.SetCaption(CString(IDS_CREATE_MAPPING));

				Dlg.SetGroup  (CString(IDS_NUMBER_OF));

				Dlg.SetData   (CPrintf(L"%u", uCount));

				if( !Dlg.Execute(ThisObject) ) {

					return FALSE;
					}

				if( !(uCount = watoi(Dlg.GetData())) ) {

					return FALSE;
					}

				if( uCount > 1 ) {

					MarkMulti(m_pItem->GetFixedPath(), CString(IDS_MAP_RECORD), navSeq);

					fMulti = TRUE;
					}

				for( UINT n = 0; n < uCount; n++ ) {

					CString Tag = Text.Left(uPos);

					Tag += CPrintf("[%u]", uStart + n);

					MapRecord(Tag, TRUE);
					}
				}
			else {
				MapRecord(Text, FALSE);
				}

			if( fMulti ) {

				MarkMulti(m_pItem->GetFixedPath(), CString(IDS_MAP_RECORD), navSeq);
				}

			return TRUE;
			}
		}

	return TRUE;
	}

void CSqlQueryManagerTreeWnd::DropDebug(void)
{
	}

void CSqlQueryManagerTreeWnd::ShowDrop(BOOL fShow)
{
	if( m_hDropRoot ) {

		CRect Horz, Vert;

		HTREEITEM hItem;

		if( !m_hDropPrev ) {

			if( IsFullItemDrop() ) {

				CTreeViewItem Node(m_hDropRoot);

				Node.SetStateMask(TVIS_DROPHILITED);

				Node.SetState(fShow ? TVIS_DROPHILITED : 0);

				m_pTree->SetItem(Node);

				m_pTree->UpdateWindow();

				return;
				}

			hItem = m_pTree->GetChild(m_hDropRoot);
			}
		else
			hItem = m_pTree->GetNext(m_hDropPrev);

		if( !hItem ) {

			HTREEITEM hDraw = m_hDropPrev;

			CRect     Root  = m_pTree->GetItemRect(hDraw, TRUE);

			WalkToLast(hDraw);

			Horz = m_pTree->GetItemRect(hDraw, TRUE);

			Horz.top    = Horz.bottom -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Root.left   - 20;

			Horz.right  = Root.right  +  8;
			}
		else {
			Horz = m_pTree->GetItemRect(hItem, TRUE);

			Horz.top    = Horz.top    -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Horz.left   - 20;

			Horz.right  = Horz.right  +  8;
			}

		if( m_hDropPrev ) {

			if( !hItem || hItem != m_pTree->GetNextVisible(m_hDropPrev) ) {

				if( ShowDropVert(m_hDropPrev) ) {

					Vert.left   = Horz.left   -  0;

					Vert.right  = Vert.left   +  2;

					Vert.bottom = Horz.top    -  0;

					Vert.top    = Vert.bottom - 16;
					}
				}
			}

		CClientDC DC(*m_pTree);

		CBrush Brush(afxColor(WHITE), afxColor(BLACK), 128);

		DC.Select(IsValidMove() ? afxBrush(WHITE) : Brush);

		DC.PatBlt(Horz, PATINVERT);

		DC.PatBlt(Vert, PATINVERT);

		DC.Deselect();
		}
	}

BOOL CSqlQueryManagerTreeWnd::ShowDropVert(HTREEITEM hItem)
{
	if( m_pTree->GetChild(hItem) ) {

		if( !m_pTree->GetItemState(hItem, TVIS_EXPANDED) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CSqlQueryManagerTreeWnd::IsDropMove(DWORD dwKeys, DWORD *pEffect)
{
	if( m_uDrop == dropFragment || m_uDrop == dropPartial ) {

		*pEffect = DROPEFFECT_LINK;

		return FALSE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::IsValidDrop(void)
{
	if( m_uDrop == dropFragment || m_uDrop == dropPartial ) {

		if( m_hDropRoot ) {

			CMetaItem *pItem = GetItemPtr(m_hDropRoot);

			if( pItem->IsKindOf(AfxRuntimeClass(CSqlRecord)) ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	if( m_uDrop == dropOther ) {

		return TRUE;
		}

	return TRUE;
	}

BOOL CSqlQueryManagerTreeWnd::IsValidMove(void)
{
	if( m_uDrop == dropData ) {

		if( !IsExpandingDrop() ) {

			if( m_fDropMove ) {

				if( IsParent(m_hSelect) ) {

					HTREEITEM hScan = m_hDropRoot;

					while( hScan ) {

						if( hScan == m_hSelect ) {

							return FALSE;
							}

						hScan = m_pTree->GetParent(hScan);
						}
					}
				}

			return TRUE;
			}
		}

	return TRUE;
	}

BOOL CSqlQueryManagerTreeWnd::IsExpandingDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
				}

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CSqlQueryManagerTreeWnd::IsFullItemDrop(void)
{
	return TRUE;
	}

// Item Data

CString CSqlQueryManagerTreeWnd::GetItemList(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CSqlQueryManager)) ) {

		return L"Queries";
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CSqlQuery)) ) {

		return L"Cols";
		}

	return L"";
	}

// Implementation

void CSqlQueryManagerTreeWnd::LockUpdate(BOOL fLock)
{
	if( fLock ) {

		if( !m_uLocked++ ) {

			SetRedraw(FALSE);

			m_fRefresh = FALSE;
			}

		return;
		}

	if( !--m_uLocked ) {

		if( m_fRefresh ) {

			RefreshTree();
			}
		else
			SetRedraw(TRUE);

		m_System.SetViewedItem(m_pSelect);

		m_System.SetViewedItem(m_pSelect);

		m_System.ItemUpdated(m_pSelect, updateChildren);

		m_pTree->UpdateWindow();
		}
	}

void CSqlQueryManagerTreeWnd::SkipUpdate(void)
{
	if( !--m_uLocked ) {

		SetRedraw(TRUE);

		m_pTree->UpdateWindow();
		}
	}

void CSqlQueryManagerTreeWnd::ListUpdated(HTREEITEM hItem)
{
	if( !m_uLocked ) {

		CMetaItem *pItem = GetItemPtr(hItem);

		m_System.ItemUpdated(pItem, updateChildren);
		}
	}

BOOL CSqlQueryManagerTreeWnd::MapRecord(CString const &Text, BOOL fStep)
{
	CSqlRecord *pRecord = (CSqlRecord *) GetItemPtr(m_hSelect);

	CError Error;

	HGLOBAL hPrev = pRecord->TakeSnapshot();

	if( pRecord->SetMapping(Error, Text) ) {

		CCmdItem *pCmd = New CCmdItem(CString(IDS_MAP_RECORD), pRecord, hPrev);

		m_System.SaveCmd(pCmd);

		m_pItem->SetDirty();

		if( fStep ) {

			OnGoNext(FALSE);
			}

		return TRUE;
		}

	GlobalFree(hPrev);

	MessageBeep(0);

	return FALSE;
	}

void CSqlQueryManagerTreeWnd::MarkMulti(CString Item, CString Menu, UINT uCode)
{
	CCmd *pCmd = New CCmdMulti( m_pItem->GetFixedPath(),
				    CString(IDS_MAP_RECORD),
				    navSeq);

	m_System.ExecCmd(pCmd);
	}

void CSqlQueryManagerTreeWnd::WalkToLast(HTREEITEM &hItem)
{
	HTREEITEM hNext;

	while( (hNext = m_pTree->GetNext (hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;
		}

	while( (hNext = m_pTree->GetChild(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;

		WalkToLast(hItem);

		break;
		}
	}

// End of File
