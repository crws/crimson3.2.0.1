
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudUbidots_HPP

#define INCLUDE_ServiceCloudUbidots_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceCloudJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ubidots Cloud Client Configuration
//

class CServiceCloudUbidots : public CServiceCloudJson
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CServiceCloudUbidots(void);

	// UI Loading
	BOOL OnLoadPages(CUIPageList *pList);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Persistance
	void Init(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Item Properties
	CCodedItem * m_pToken;
	CCodedItem * m_pDevice;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
	void InitOptions(void);
};

// End of File

#endif
