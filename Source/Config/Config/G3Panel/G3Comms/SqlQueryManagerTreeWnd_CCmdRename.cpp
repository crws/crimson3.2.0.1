
#include "Intern.hpp"

#include "SqlQueryManagerTreeWnd_CCmdRename.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SQL Tree Window -- Rename Command
//

// Runtime Class

AfxImplementRuntimeClass(CSqlQueryManagerTreeWnd::CCmdRename, CStdCmd);

// Constructor

CSqlQueryManagerTreeWnd::CCmdRename::CCmdRename(CMetaItem *pItem, CString Name)
{
	m_Menu = CFormat(CString(IDS_RENAME), pItem->GetName());

	m_Item = pItem->GetFixedPath();

	m_Prev = pItem->GetName();

	m_Name = Name;
	}

// End of File
