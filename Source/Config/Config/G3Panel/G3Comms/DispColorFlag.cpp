
#include "Intern.hpp"

#include "DispColorFlag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Color
//

// Dynamic Class

AfxImplementDynamicClass(CDispColorFlag, CDispColor);

// Constructor

CDispColorFlag::CDispColorFlag(void)
{
	m_uType = 2;

	m_On    = MAKELONG(GetRGB(0,31,0),GetRGB(0,0,0));

	m_Off   = MAKELONG(GetRGB(31,0,0),GetRGB(0,0,0));
	}

// Color Access

DWORD CDispColorFlag::GetColorPair(DWORD Data, UINT Type)
{
	if( Type == typeReal ) {

		C3REAL Real = I2R(Data);

		return Real ? m_On : m_Off;
		}

	return Data ? m_On : m_Off;
	}

// Download Support

BOOL CDispColorFlag::MakeInitData(CInitData &Init)
{
	CDispColor::MakeInitData(Init);

	Init.AddLong(m_On);

	Init.AddLong(m_Off);

	return TRUE;
	}

// Meta Data Creation

void CDispColorFlag::AddMetaData(void)
{
	CDispColor::AddMetaData();

	Meta_AddInteger(On);
	Meta_AddInteger(Off);

	Meta_SetName((IDS_FLAG_COLOR_PAIR));
	}

// End of File
