
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlFilterList_HPP

#define INCLUDE_SqlFilterList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlFilter;

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter List
//

class CSqlFilterList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlFilterList(void);

		// Item Access
		CSqlFilter * GetItem(INDEX Index) const;
		CSqlFilter * GetItem(UINT uPos) const;
	};

// End of File

#endif
