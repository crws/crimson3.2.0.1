
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextFaceRef_HPP

#define INCLUDE_UITextFaceRef_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Interface Reference
//

class CUITextFaceRef : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextFaceRef(void);

	protected:
		// Overridables
		void OnBind(void);
		BOOL OnEnumValues(CStringArray &List);

		// Implementation
		void AddData(void);
		void AddData(UINT Data, CString Text);
	};

// End of File

#endif
