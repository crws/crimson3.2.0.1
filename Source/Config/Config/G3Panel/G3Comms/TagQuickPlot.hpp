
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagQuickPlot_HPP

#define INCLUDE_TagQuickPlot_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Quick Plot
//

class DLLNOT CTagQuickPlot : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagQuickPlot(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Suport
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT	     m_Mode;
		UINT	     m_Rewind;
		CCodedItem * m_pEnable;
		CCodedItem * m_pStore;
		CCodedItem * m_pTime;
		CCodedItem * m_pLook;
		UINT	     m_Points;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
