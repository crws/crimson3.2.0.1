
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudJson_HPP

#define INCLUDE_ServiceCloudJson_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceCloudBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// JSON MQTT Cloud Client Configuration
//

class CServiceCloudJson : public CServiceCloudBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CServiceCloudJson(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Root;
		UINT m_Code;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
