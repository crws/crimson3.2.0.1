
#include "Intern.hpp"

#include "UITextProgram.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedHost.hpp"

#include "ProgramCodeItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Program Item
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProgram, CUITextElement);

// Constructor

CUITextProgram::CUITextProgram(void)
{
	m_uFlags = textEdit | textNoMerge;
	}

// Overridables

CString CUITextProgram::OnGetAsText(void)
{
	CItem            * &pItem = m_pData->GetObject(m_pItem);

	CProgramCodeItem * pCoded = (CProgramCodeItem *) pItem;

	if( pItem ) {

		return pCoded->GetAsText();
		}

	return L"";
	}

UINT CUITextProgram::OnSetAsText(CError &Error, CString Text)
{
	CItem            * &pItem = m_pData->GetObject(m_pItem);

	CProgramCodeItem * pCoded = (CProgramCodeItem *) pItem;

	if( Text.IsEmpty() ) {

		if( pCoded ) {

			pCoded->Kill();

			delete pCoded;

			pItem = NULL;

			return saveChange;
			}

		return saveSame;
		}

	if( pCoded ) {

		pCoded->SetAsText(Text);

		return saveChange;
		}
	else {
		CTypeDef Type;

		pCoded = New CProgramCodeItem;

		pCoded->SetParent(m_pItem);

		pCoded->Init();

		if( m_pItem->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

			CCodedHost *pHost = (CCodedHost *) m_pItem;

			if( !pHost->GetTypeData(m_UIData.m_Tag, Type) ) {

				FindReqType(Type);
				}
			}
		else
			FindReqType(Type);

		pCoded->SetReqType(Type);

		pCoded->SetAsText (Text);

		pItem            = pCoded;

		return saveChange;
		}
	}

// Implementation

void CUITextProgram::FindReqType(CTypeDef &Type)
{
	Type.m_Type  = typeVoid;

	Type.m_Flags = 0;
	}

// End of File
