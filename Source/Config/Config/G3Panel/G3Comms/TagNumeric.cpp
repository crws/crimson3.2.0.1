
#include "Intern.hpp"

#include "TagNumeric.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "DispColor.hpp"
#include "DispFormat.hpp"
#include "DispFormatNumber.hpp"
#include "DispFormatSci.hpp"
#include "SecDesc.hpp"
#include "TagEventNumeric.hpp"
#include "TagNumericPage.hpp"
#include "TagQuickPlot.hpp"
#include "TagTriggerNumeric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Item
//

// Dynamic Class

AfxImplementDynamicClass(CTagNumeric, CDataTag);

// Constructor

CTagNumeric::CTagNumeric(void)
{
	m_Manipulate = 0;
	m_TreatAs    = 0;
	m_ScaleTo    = 0;
	m_pDataMin   = NULL;
	m_pDataMax   = NULL;
	m_pDispMin   = NULL;
	m_pDispMax   = NULL;
	m_HasSP      = 0;
	m_pSetpoint  = NULL;
	m_LimitType  = 0;
	m_pLimitMin  = NULL;
	m_pLimitMax  = NULL;
	m_pDeadband  = NULL;
	m_pEvent1    = New CTagEventNumeric;
	m_pEvent2    = New CTagEventNumeric;
	m_pTrigger1  = New CTagTriggerNumeric;
	m_pTrigger2  = New CTagTriggerNumeric;
	m_pSec       = New CSecDesc;
	m_pQuickPlot = New CTagQuickPlot;
}

// UI Creation

BOOL CTagNumeric::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() == SW_GROUP_2 ) {

		CTagNumericPage *pPage1 = New CTagNumericPage(this, CString(IDS_DATA), 8);

		pList->Append(pPage1);
	}
	else {
		BOOL h = C3OemFeature(L"OemSD", FALSE);

		UINT f = h ? 10 : 2;

		if( FALSE ) {

			CTagNumericPage *pPage1 = New CTagNumericPage(this, CString(IDS_DATA), 1);

			CTagNumericPage *pPage2 = New CTagNumericPage(this, CString(IDS_FORMAT_6), f);

			CTagNumericPage *pPage5 = New CTagNumericPage(this, CString(IDS_TRIGGERS), 6);

			pList->Append(pPage1);

			pList->Append(pPage2);

			pList->Append(pPage5);
		}
		else {
			CTagNumericPage *pPage1 = New CTagNumericPage(this, CString(IDS_DATA), 1);

			CTagNumericPage *pPage2 = New CTagNumericPage(this, CString(IDS_FORMAT_6), f);

			CTagNumericPage *pPage3 = New CTagNumericPage(this, CString(IDS_COLORS), 4);

			CTagNumericPage *pPage4 = New CTagNumericPage(this, CString(IDS_ALARMS), 5);

			CTagNumericPage *pPage5 = New CTagNumericPage(this, CString(IDS_TRIGGERS), 6);

			CTagNumericPage *pPage6 = New CTagNumericPage(this, CString(IDS_PLOT), 9);

			CTagNumericPage *pPage7 = New CTagNumericPage(this, CString(IDS_SECURITY), 7);

			pList->Append(pPage1);

			pList->Append(pPage2);

			pList->Append(pPage3);

			pList->Append(pPage4);

			pList->Append(pPage5);

			pList->Append(pPage6);

			pList->Append(pPage7);
		}
	}

	return TRUE;
}

// UI Update

void CTagNumeric::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		LimitTreatAs(pHost);

		LimitManipulate(pHost);

		LimitAccess(pHost);

		DoEnables(pHost);
	}
	else {
		if( pItem == m_pFormat ) {

			pHost->SendUpdate(updateValue);
		}
	}

	if( Tag == "Value" ) {

		if( m_pValue ) {

			UINT TreatAs = m_TreatAs;

			if( m_fIntern ) {

				if( m_pValue->GetType() == typeReal ) {

					m_TreatAs = 2;
				}
				else {
					if( m_pValue->IsCommsRef() ) {

						if( m_pValue->GetCommsBits() == 32 ) {

							m_TreatAs = 0;
						}
						else
							m_TreatAs = 3;
					}
					else
						m_TreatAs = 0;
				}
			}

			if( m_pValue->IsCommsRef() ) {

				if( m_pValue->GetCommsBits() == 1 ) {

					if( pHost->HasWindow() ) {

						pHost->GetWindow().Information(IDS_WARN_MAP_FLAG_NUM);
					}

					m_ScaleTo = 0;

					pHost->UpdateUI("ScaleTo");

					ClearScaling(pHost);
				}

				CheckCircular(pHost);

				UpdateExtent();
			}
			else {
				if( m_fIntern ) {

					m_Access = 2;

					Recompile(pHost);

					pHost->UpdateUI(this, "Access");
				}
				else {
					if( !m_pValue->IsWritable() ) {

						if( m_pValue->GetType() == typeReal ) {

							m_TreatAs = 2;
						}
						else
							m_TreatAs = 0;
					}
				}

				m_Extent = 0;

				pHost->UpdateUI("Extent");

				CheckCircular(pHost, m_pValue);
			}

			if( !m_pValue->IsWritable() ) {

				MakeReadOnly(pHost);
			}

			if( m_pValue->IsConst() ) {

				KillCoded(pHost, L"Sim", m_pSim);
			}

			if( !m_ScaleTo ) {

				if( m_TreatAs - TreatAs ) {

					UpdateLimitTypes(pHost);

					UpdateChildTypes(TRUE);

					CheckOnWrite(pHost);

					Recompile(pHost);
				}
			}

			UpdateScaleDataTypes(pHost);

			pHost->UpdateUI("TreatAs");
		}
		else {
			UINT Access  = m_Access;

			m_Access     = 0;

			m_Manipulate = 0;

			pHost->UpdateUI(this, "Access");

			pHost->UpdateUI(this, "Manipulate");

			if( Access ) {

				UpdateChildTypes(TRUE);

				CheckOnWrite(pHost);

				Recompile(pHost);
			}

			CheckCircular(pHost);
		}

		CheckPersist(pHost);

		LimitTreatAs(pHost);

		LimitManipulate(pHost);

		LimitAccess(pHost);

		pHost->SendUpdate(updateProps);

		DoEnables(pHost);
	}

	if( Tag == "TreatAs" ) {

		UpdateScaleDataTypes(pHost);

		if( !m_ScaleTo ) {

			UpdateLimitTypes(pHost);

			UpdateChildTypes(TRUE);

			CheckOnWrite(pHost);

			Recompile(pHost);
		}
	}

	if( Tag == "Extent" ) {

		if( m_pValue ) {

			UpdateExtent();
		}

		Recompile(pHost);

		DoEnables(pHost);
	}

	if( Tag == "Access" ) {

		if( pItem == this ) {

			CheckPersist(pHost);

			Recompile(pHost);

			DoEnables(pHost);
		}
	}

	if( Tag == "Manipulate" ) {

		LimitTreatAs(pHost);

		pHost->SendUpdate(updateProps);

		DoEnables(pHost);
	}

	if( Tag == "ScaleTo" ) {

		if( m_ScaleTo ) {

			UpdateScaleDispTypes(pHost);
		}
		else
			ClearScaling(pHost);

		UpdateLimitTypes(pHost);

		UpdateChildTypes(TRUE);

		CheckOnWrite(pHost);

		Recompile(pHost);

		DoEnables(pHost);
	}

	if( Tag == "DataMin" ) {

		CheckCircular(pHost, m_pDataMin);
	}

	if( Tag == "DataMax" ) {

		CheckCircular(pHost, m_pDataMax);
	}

	if( Tag == "DispMin" ) {

		CheckCircular(pHost, m_pDispMin);
	}

	if( Tag == "DispMax" ) {

		CheckCircular(pHost, m_pDispMax);
	}

	if( Tag == "HasSP" ) {

		if( !m_HasSP ) {

			KillCoded(pHost, L"Setpoint", m_pSetpoint);
		}

		DoEnables(pHost);
	}

	if( Tag == "LimitMin" ) {

		CheckCircular(pHost, m_pLimitMin);
	}

	if( Tag == "LimitMax" ) {

		CheckCircular(pHost, m_pLimitMax);
	}

	if( Tag == "Deadband" ) {

		CheckCircular(pHost, m_pDeadband);
	}

	if( Tag == "OnWrite" ) {

		CheckCircular(pHost, m_pOnWrite);

		Recompile(pHost);
	}

	CDataTag::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CTagNumeric::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		Type.m_Type  = typeNumeric;

		Type.m_Flags = flagSoftWrite | flagInherent;

		if( m_Extent ) {

			Type.m_Flags |= flagCommsTab;
		}

		return TRUE;
	}

	if( Tag == "DataMin" || Tag == "DataMax" ) {

		Type.m_Type  = (m_TreatAs == 2) ? typeReal : typeInteger;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "DispMin" || Tag == "DispMax" ) {

		Type.m_Type  = m_ScaleTo ? GetDataType() : typeNumeric;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "Sim" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = flagConstant;

		return TRUE;
	}

	if( Tag == "Setpoint" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "LimitMin" || Tag == "LimitMax" || Tag == "Deadband" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "SubValue" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "OnWrite" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
	}

	return CDataTag::GetTypeData(Tag, Type);
}

// Attributes

UINT CTagNumeric::GetTreeImage(void) const
{
	if( m_pValue ) {

		if( m_pValue->IsCommsRef() ) {

			if( m_pValue->GetCommsBits() == 1 ) {

				return GetImage(IDI_RED_FLAG);
			}
		}
	}

	return GetImage(IDI_RED_INTEGER);
}

UINT CTagNumeric::GetDataType(void) const
{
	switch( m_ScaleTo ) {

		case 0:
			switch( m_TreatAs ) {

				case 0:
				case 1:
				case 3:
					return typeInteger;

				case 2:
					return typeReal;
			}
			break;

		case 1:
			return typeInteger;

		case 2:
			return typeReal;
	}

	AfxAssert(FALSE);

	return typeVoid;
}

UINT CTagNumeric::GetTypeFlags(void) const
{
	UINT Flags = flagTagRef;

	if( m_Extent > 0 ) {

		if( m_Extent > 1024 ) {

			Flags |= flagExtended;
		}

		Flags |= flagArray;
	}

	if( m_Access < 2 ) {

		Flags |= flagWritable;
	}

	if( m_pOnWrite ) {

		Flags |= flagWritable;
	}

	return Flags;
}

BOOL CTagNumeric::HasSetpoint(void) const
{
	return m_HasSP;
}

BOOL CTagNumeric::NeedSetpoint(void) const
{
	if( m_pEvent1->NeedSetpoint() ) {

		return TRUE;
	}

	if( m_pEvent2->NeedSetpoint() ) {

		return TRUE;
	}

	if( m_pTrigger1->NeedSetpoint() ) {

		return TRUE;
	}

	if( m_pTrigger2->NeedSetpoint() ) {

		return TRUE;
	}

	return FALSE;
}

// Operations

void CTagNumeric::UpdateTypes(BOOL fComp)
{
	if( m_pValue && !m_pValue->IsBroken() ) {

		if( !m_pValue->IsCommsRef() || m_pValue->GetCommsBits() == 1 ) {

			switch( m_pValue->GetType() ) {

				case typeInteger:

					m_TreatAs = 0;

					break;

				case typeReal:

					m_TreatAs = 2;

					break;
			}
		}

		if( !m_pValue->IsWritable() ) {

			m_Access = 2;
		}

		UpdateExtent();
	}

	if( !m_ScaleTo ) {

		UpdateChildTypes(fComp);
	}

	UpdateType(NULL, L"DataMin", m_pDataMin, fComp);

	UpdateType(NULL, L"DataMax", m_pDataMax, fComp);

	UpdateType(NULL, L"OnWrite", m_pOnWrite, fComp);

	UpdateCircular();
}

void CTagNumeric::MakeLite(void)
{
	m_pEvent1->m_Mode = 0;

	m_pEvent2->m_Mode = 0;

	m_pTrigger1->m_Mode = 0;

	m_pTrigger2->m_Mode = 0;

	SetFormatClass(m_pFormat, NULL);

	SetColorClass(m_pColor, NULL);
}

// Reference Check

BOOL CTagNumeric::RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag)
{
	if( CodeRefersToTag(Busy, pTags, m_pDataMin, uTag) ) {

		return TRUE;
	}

	if( CodeRefersToTag(Busy, pTags, m_pDataMax, uTag) ) {

		return TRUE;
	}

	if( CodeRefersToTag(Busy, pTags, m_pDispMin, uTag) ) {

		return TRUE;
	}

	if( CodeRefersToTag(Busy, pTags, m_pDispMax, uTag) ) {

		return TRUE;
	}

	if( CodeRefersToTag(Busy, pTags, m_pLimitMin, uTag) ) {

		return TRUE;
	}

	if( CodeRefersToTag(Busy, pTags, m_pLimitMax, uTag) ) {

		return TRUE;
	}

	if( CodeRefersToTag(Busy, pTags, m_pDeadband, uTag) ) {

		return TRUE;
	}

	if( CodeRefersToTag(Busy, pTags, m_pOnWrite, uTag) ) {

		return TRUE;
	}

	return CDataTag::RefersToTag(Busy, pTags, uTag);
}

// Circular Check

void CTagNumeric::UpdateCircular(void)
{
	m_Circle = CheckCircular(m_pValue) ||
		CheckCircular(m_pDataMin) ||
		CheckCircular(m_pDataMax) ||
		CheckCircular(m_pDispMin) ||
		CheckCircular(m_pDispMax) ||
		CheckCircular(m_pLimitMin) ||
		CheckCircular(m_pLimitMax) ||
		CheckCircular(m_pDeadband) ||
		CheckCircular(m_pOnWrite);
}

// Evaluation

DWORD CTagNumeric::GetProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return FindAsText();

		case tpLabel:
			return FindLabel();

		case tpPrefix:
			return FindPrefix();

		case tpUnits:
			return FindUnits();

		case tpSetPoint:
			return FindSP(Type);

		case tpMinimum:
			return FindMin(Type);

		case tpMaximum:
			return FindMax(Type);

		case tpDeadband:
			return FindDeadband(Type); ///!!!!

		case tpForeColor:
			return FindFore();

		case tpBackColor:
			return FindBack();
	}

	return CDataTag::GetProp(ID, Type);
}

// Searching

void CTagNumeric::FindAlarms(CStringArray &List)
{
	m_pEvent1->FindActive(List);

	m_pEvent2->FindActive(List);
}

void CTagNumeric::FindTriggers(CStringArray &List)
{
	m_pTrigger1->FindActive(List);

	m_pTrigger2->FindActive(List);
}

// Persistance

void CTagNumeric::PostLoad(void)
{
	CDataTag::PostLoad();

	if( m_LimitType == 2 ) {

		m_LimitType = 1;
	}
}

// Property Save Filter

BOOL CTagNumeric::SaveProp(CString const &Tag) const
{
	if( Tag == "Event1" ) {

		return m_pEvent1->m_Mode > 0;
	}

	if( Tag == "Event2" ) {

		return m_pEvent2->m_Mode > 0;
	}

	if( Tag == "Trigger1" ) {

		return m_pTrigger1->m_Mode > 0;
	}

	if( Tag == "Trigger2" ) {

		return m_pTrigger2->m_Mode > 0;
	}

	if( Tag == "QuickPlot" ) {

		return m_pQuickPlot->m_Mode > 0;
	}

	return CDataTag::SaveProp(Tag);
}

// Download Support

BOOL CTagNumeric::MakeInitData(CInitData &Init)
{
	Init.AddByte(2);

	CDataTag::MakeInitData(Init);

	Init.AddByte(BYTE(m_Manipulate));
	Init.AddByte(BYTE(m_TreatAs));
	Init.AddByte(BYTE(m_ScaleTo));
	Init.AddByte(BYTE(m_HasSP));
	Init.AddByte(BYTE(m_LimitType));

	if( m_ScaleTo ) {

		Init.AddItem(itemVirtual, m_pDataMin);
		Init.AddItem(itemVirtual, m_pDataMax);
		Init.AddItem(itemVirtual, m_pDispMin);
		Init.AddItem(itemVirtual, m_pDispMax);
	}

	if( m_HasSP ) {

		Init.AddItem(itemVirtual, m_pSetpoint);
	}

	Init.AddItem(itemVirtual, m_pLimitMin);
	Init.AddItem(itemVirtual, m_pLimitMax);
	Init.AddItem(itemVirtual, m_pDeadband);

	Init.AddItem(itemSimple, m_pEvent1);
	Init.AddItem(itemSimple, m_pEvent2);

	Init.AddItem(itemSimple, m_pTrigger1);
	Init.AddItem(itemSimple, m_pTrigger2);

	Init.AddItem(itemSimple, m_pSec);

	if( m_pQuickPlot->m_Mode > 0 ) {

		Init.AddByte(1);

		Init.AddItem(itemSimple, m_pQuickPlot);
	}
	else
		Init.AddByte(0);

	Init.AddItem(itemVirtual, m_pOnWrite);

	return TRUE;
}

// Meta Data

void CTagNumeric::AddMetaData(void)
{
	CDataTag::AddMetaData();

	Meta_AddInteger(Manipulate);
	Meta_AddInteger(TreatAs);
	Meta_AddInteger(ScaleTo);
	Meta_AddVirtual(DataMin);
	Meta_AddVirtual(DataMax);
	Meta_AddVirtual(DispMin);
	Meta_AddVirtual(DispMax);
	Meta_AddInteger(HasSP);
	Meta_AddVirtual(Setpoint);
	Meta_AddInteger(LimitType);
	Meta_AddVirtual(LimitMin);
	Meta_AddVirtual(LimitMax);
	Meta_AddVirtual(Deadband);
	Meta_AddObject(Event1);
	Meta_AddObject(Event2);
	Meta_AddObject(Trigger1);
	Meta_AddObject(Trigger2);
	Meta_AddObject(Sec);
	Meta_AddVirtual(OnWrite);
	Meta_AddObject(QuickPlot);

	Meta_SetName((IDS_NUMERIC_TAG));
}

// Property Access

DWORD CTagNumeric::FindAsText(void)
{
	DWORD   Data = Execute();

	UINT    Type = GetDataType();

	CString Text = L"";

	if( m_pFormat ) {

		Text = m_pFormat->Format(Data, Type, fmtStd);
	}
	else {
		if( Type == typeInteger ) {

			Text.Printf(L"%d", INT(Data));
		}
		else
			Text.Printf(L"%f", I2R(Data));
	}

	FreeData(Data, Type);

	return DWORD(wstrdup(Text));
}

DWORD CTagNumeric::FindLabel(void)
{

	// LATER -- Refactor back into base class?

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the label that
	// uses the tag property syntax to reference itself!!!

	if( m_pLabel ) {

		return DWORD(wstrdup(m_pLabel->GetText()));
	}

	return DWORD(wstrdup(m_Name));
}

DWORD CTagNumeric::FindPrefix(void)
{

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the label that
	// uses the tag property syntax to reference itself!!!

	if( m_FormType == 1 ) {

		CDispFormatNumber *pNumber = (CDispFormatNumber *) m_pFormat;

		if( pNumber->m_pPrefix ) {

			return pNumber->m_pPrefix->ExecVal();
		}
	}

	if( m_FormType == 2 ) {

		CDispFormatSci *pSci = (CDispFormatSci *) m_pFormat;

		if( pSci->m_pPrefix ) {

			return pSci->m_pPrefix->ExecVal();
		}
	}

	return DWORD(wstrdup(L""));
}

DWORD CTagNumeric::FindUnits(void)
{

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the label that
	// uses the tag property syntax to reference itself!!!

	if( m_FormType == 1 ) {

		CDispFormatNumber *pNumber = (CDispFormatNumber *) m_pFormat;

		if( pNumber->m_pUnits ) {

			return pNumber->m_pUnits->ExecVal();
		}
	}

	if( m_FormType == 2 ) {

		CDispFormatSci *pSci = (CDispFormatSci *) m_pFormat;

		if( pSci->m_pUnits ) {

			return pSci->m_pUnits->ExecVal();
		}
	}

	return DWORD(wstrdup(L""));
}

DWORD CTagNumeric::FindSP(UINT Type)
{

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the setpoint that
	// uses the tag property syntax to reference itself!!!

	if( m_pSetpoint ) {

		return m_pSetpoint->Execute(Type);
	}

	return GetNull(Type);
}

DWORD CTagNumeric::FindMin(UINT Type)
{
	if( !m_pFormat || m_pFormat->NeedsLimits() ) {

		if( !m_Circle ) {

			if( m_pLimitMin ) {

				DWORD x = m_pLimitMin->Execute(Type);

				return x;
			}

			if( m_LimitType == 0 ) {

				if( m_pDispMin ) {

					return m_pDispMin->Execute(Type);
				}
			}
		}
	}

	if( !m_Circle ) {

		if( m_pFormat ) {

			return m_pFormat->GetMin(Type);
		}
	}

	if( Type == typeInteger ) {

		return 0;
	}

	return R2I(0);
}

DWORD CTagNumeric::FindMax(UINT Type)
{
	if( !m_pFormat || m_pFormat->NeedsLimits() ) {

		if( !m_Circle ) {

			if( m_pLimitMax ) {

				return m_pLimitMax->Execute(Type);
			}

			if( m_LimitType == 0 ) {

				if( m_pDispMax ) {

					return m_pDispMax->Execute(Type);
				}
			}
		}
	}

	if( !m_Circle ) {

		if( m_pFormat ) {

			return m_pFormat->GetMax(Type);
		}
	}

	if( Type == typeInteger ) {

		return 999999;
	}

	return R2I(999999);
}

DWORD CTagNumeric::FindDeadband(UINT Type)
{
	if( !m_pFormat || m_pFormat->NeedsLimits() ) {

		if( !m_Circle ) {

			if( m_pDeadband ) {

				return m_pDeadband->Execute(Type);
			}
		}
	}

	if( Type == typeInteger ) {

		return 0;
	}

	return R2I(0);
}

DWORD CTagNumeric::FindFore(void)
{
	if( m_pColor ) {

		DWORD Data = Execute();

		UINT  Type = GetDataType();

		return m_pColor->GetForeColor(Data, Type);
	}

	return GetRGB(31, 31, 31);
}

DWORD CTagNumeric::FindBack(void)
{
	if( m_pColor ) {

		DWORD Data = Execute();

		UINT  Type = GetDataType();

		return m_pColor->GetBackColor(Data, Type);
	}

	return GetRGB(0, 0, 0);
}

// Memory Sizing

UINT CTagNumeric::GetAllocSize(void)
{
	return sizeof(C3INT) * max(m_Extent, 1);
}

UINT CTagNumeric::GetCommsSize(void)
{
	return m_Extent;
}

// Implementation

void CTagNumeric::DoEnables(IUIHost *pHost)
{
	if( m_pValue ) {

		if( m_pValue->IsCommsRef() ) {

			pHost->EnableUI("Manipulate", TRUE);

			if( m_pValue->GetCommsBits() == 1 ) {

				pHost->EnableUI("TreatAs", FALSE);
				pHost->EnableUI("ScaleTo", FALSE);
			}
			else {
				pHost->EnableUI("TreatAs", TRUE);
				pHost->EnableUI("ScaleTo", TRUE);
			}

			pHost->EnableUI(this, "DataMin", m_ScaleTo > 0);
			pHost->EnableUI(this, "DataMax", m_ScaleTo > 0);
			pHost->EnableUI(this, "DispMin", m_ScaleTo > 0);
			pHost->EnableUI(this, "DispMax", m_ScaleTo > 0);

			pHost->EnableUI(this, "Access", m_pValue->IsWritable());

			pHost->EnableUI(this, "RdMode", m_Extent && m_Access != 1);

			pHost->EnableUI(this, "Extent", m_pValue->IsCommsRefTable());

			pHost->EnableUI(this, "Persist", m_Access == 1);
		}
		else {
			BOOL fWrite = m_pValue->IsWritable();

			pHost->EnableUI(this, "Extent", FALSE);
			pHost->EnableUI(this, "Persist", FALSE);
			pHost->EnableUI(this, "Manipulate", TRUE);
			pHost->EnableUI(this, "TreatAs", fWrite);
			pHost->EnableUI(this, "ScaleTo", fWrite);
			pHost->EnableUI(this, "Access", fWrite);
			pHost->EnableUI(this, "RdMode", FALSE);

			pHost->EnableUI(this, "DataMin", m_ScaleTo > 0);
			pHost->EnableUI(this, "DataMax", m_ScaleTo > 0);
			pHost->EnableUI(this, "DispMin", m_ScaleTo > 0);
			pHost->EnableUI(this, "DispMax", m_ScaleTo > 0);
		}

		pHost->EnableUI("Sim", !m_pValue->IsConst());
	}
	else {
		pHost->EnableUI(this, "Extent", TRUE);
		pHost->EnableUI(this, "Access", FALSE);
		pHost->EnableUI(this, "RdMode", FALSE);
		pHost->EnableUI(this, "Persist", TRUE);
		pHost->EnableUI(this, "Manipulate", FALSE);
		pHost->EnableUI(this, "TreatAs", TRUE);
		pHost->EnableUI(this, "ScaleTo", TRUE);
		pHost->EnableUI(this, "Sim", TRUE);

		pHost->EnableUI(this, "DataMin", m_ScaleTo > 0);
		pHost->EnableUI(this, "DataMax", m_ScaleTo > 0);
		pHost->EnableUI(this, "DispMin", m_ScaleTo > 0);
		pHost->EnableUI(this, "DispMax", m_ScaleTo > 0);
	}

	pHost->EnableUI("HasSP", NeedSetpoint() ? FALSE : TRUE);

	pHost->EnableUI("Setpoint", m_HasSP);

	pHost->EnableUI("FormLock", m_FormType  >= 1);
}

void CTagNumeric::LimitTreatAs(IUIHost *pHost)
{
	UINT uMask = 5;

	if( m_pValue ) {

		if( m_Manipulate == 5 ) {

			uMask = 2;
		}
		else {
			if( m_pValue->IsCommsRef() ) {

				switch( m_pValue->GetCommsBits() ) {

					case 1:
						uMask = 2;
						break;

					case 32:
						uMask = 5;
						break;

					default:
						uMask = 11;
						break;
				}
			}
		}

		if( m_TreatAs == 3 ) {

			if( !(uMask & (1 << m_TreatAs)) ) {

				if( m_pValue->GetType() == typeReal ) {

					m_TreatAs = 2;
				}
				else
					m_TreatAs = 0;

				if( pHost ) {

					pHost->UpdateUI(L"TreatAs");
				}
			}
		}
	}

	LimitEnum(pHost, L"TreatAs", m_TreatAs, uMask);
}

void CTagNumeric::LimitManipulate(IUIHost *pHost)
{
	UINT uMask = 63;

	if( m_pValue ) {

		if( m_pValue->IsCommsRef() ) {

			switch( m_pValue->GetCommsBits() ) {

				case 1:
					uMask = 3;
					break;

				case 8:
					uMask = 32 + 7;
					break;

				case 16:
					uMask = 32 + 15;
					break;

				case 32:
					uMask = 32 + 31;
					break;
			}
		}
	}

	LimitEnum(pHost, L"Manipulate", m_Manipulate, uMask);
}

void CTagNumeric::LimitAccess(IUIHost *pHost)
{
	UINT uMask = 7;

	if( m_pValue ) {

		if( !m_pValue->IsCommsRef() ) {

			uMask = 5;
		}
	}

	LimitEnum(pHost, L"Access", m_Access, uMask);
}

void CTagNumeric::ClearScaling(IUIHost *pHost)
{
	KillCoded(pHost, L"DataMin", m_pDataMin);

	KillCoded(pHost, L"DataMax", m_pDataMax);

	KillCoded(pHost, L"DispMin", m_pDispMin);

	KillCoded(pHost, L"DispMax", m_pDispMax);
}

void CTagNumeric::UpdateScaleDataTypes(IUIHost *pHost)
{
	UpdateType(pHost, L"DataMin", m_pDataMin, TRUE);

	UpdateType(pHost, L"DataMax", m_pDataMax, TRUE);
}

void CTagNumeric::UpdateScaleDispTypes(IUIHost *pHost)
{
	UpdateType(pHost, L"DispMin", m_pDispMin, TRUE);

	UpdateType(pHost, L"DispMax", m_pDispMax, TRUE);
}

void CTagNumeric::UpdateLimitTypes(IUIHost *pHost)
{
	UpdateType(pHost, L"LimitMin", m_pLimitMin, TRUE);

	UpdateType(pHost, L"LimitMax", m_pLimitMax, TRUE);

	UpdateType(pHost, L"Deadband", m_pDeadband, TRUE);

	UpdateType(pHost, L"Setpoint", m_pSetpoint, TRUE);

	UpdateType(pHost, L"Sim", m_pSim, TRUE);
}

void CTagNumeric::UpdateChildTypes(BOOL fComp)
{
	if( m_pFormat ) {

		m_pFormat->UpdateTypes(fComp);
	}

	if( m_pColor ) {

		m_pColor->UpdateTypes(fComp);
	}

	m_pEvent1->UpdateTypes(fComp);

	m_pEvent2->UpdateTypes(fComp);

	m_pTrigger1->UpdateTypes(fComp);

	m_pTrigger2->UpdateTypes(fComp);
}

// End of File
