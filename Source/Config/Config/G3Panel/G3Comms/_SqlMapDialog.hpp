
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlMapDialog_HPP

#define INCLUDE_SqlMapDialog_HPP

//////////////////////////////////////////////////////////////////////////
//
// SQL Mapping Dialog
//

class CSqlMapDialog : public CItemDialog
{
	public:
		// Constructors
		CSqlMapDialog(CItem *pItem);
		CSqlMapDialog(CItem *pItem, CString Caption);
		CSqlMapDialog(CItem *pItem, CString Caption, UINT uView);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Msg, LRESULT &lResult);

	protected:

	};

// End of File

#endif
