
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortProfibus_HPP

#define INCLUDE_CommsPortProfibus_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Profibus Comms Port
//

class DLLNOT CCommsPortProfibus : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortProfibus(void);

		// Overridables
		char GetPortTag(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

// End of File

#endif
