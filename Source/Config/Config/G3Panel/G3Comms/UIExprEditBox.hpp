
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIExprEditBox_HPP

#define INCLUDE_UIExprEditBox_HPP

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Programmable Text Box
//

class CUIExprEditBox : public CUIEditBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIExprEditBox(void);

	protected:
		// Data Members
		UINT m_cfCode;

		// Data Overridables
		void OnLoad(void);
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);
	};

// End of File

#endif
