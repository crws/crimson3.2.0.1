
#include "Intern.hpp"

#include "ItemCreateDialog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Item Create Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CItemCreateDialog, CStdDialog);

// Static Data

UINT CItemCreateDialog::m_Last = typeVoid;

// Constructor

CItemCreateDialog::CItemCreateDialog(CNamedList *pList, UINT Type)
{
	m_pList = pList;

	m_Type  = Type;

	m_Size  = 0;

	m_fOne  = (Type >= typeObject);

	SetName(m_fOne ? L"ObjCreateDialog" : L"TagCreateDialog");
	}

CItemCreateDialog::CItemCreateDialog(CNamedList *pList, UINT Type, CString Name)
{
	m_pList = pList;

	m_Type  = Type;

	m_Size  = 0;

	m_fOne  = (Type >= typeObject);

	m_Name  = Name;

	SetName(m_fOne ? L"ObjCreateDialog" : L"TagCreateDialog");
	}

CItemCreateDialog::CItemCreateDialog(CString Name, UINT Type, UINT Size)
{
	m_pList = NULL;

	m_Name  = Name;

	m_Type  = Type;

	m_Size  = Size;

	m_fOne  = FALSE;

	SetName(L"TagAutoDialog");
	}

CItemCreateDialog::CItemCreateDialog(CString Name, UINT Type)
{
	m_pList = NULL;

	m_Name  = Name;

	m_Type  = Type;

	m_Size  = 0;

	m_fOne  = (Type >= typeObject);

	SetName(m_fOne ? L"ObjAutoDialog" : L"TagAutoDialog");
	}

// Attributes

CString CItemCreateDialog::GetName(void) const
{
	return m_Name;
	}

UINT CItemCreateDialog::GetType(void) const
{
	return m_Type;
	}

UINT CItemCreateDialog::GetSize(void) const
{
	return m_Size;
	}

// Last Type

UINT CItemCreateDialog::GetLastType(void)
{
	return m_Last;
	}

// Message Map

AfxMessageMap(CItemCreateDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(200, EN_CHANGE,     OnEditChange)
	AfxDispatchNotify(202, CBN_SELCHANGE, OnModeChange)

	AfxDispatchCommand(1, OnCommandYes)
	AfxDispatchCommand(3, OnCommandYes)
	AfxDispatchCommand(2, OnCommandNo)

	AfxMessageEnd(CItemCreateDialog)
	};

// Message Handlers

BOOL CItemCreateDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	// REV3 -- A lot of this information ought to be coming from
	// some kind of extended name server that can give us icons,
	// type descriptions and so on. We could also switch to a
	// view that has a "Create In" tree for folderable items.

	LoadType();

	LoadName();

	LoadSize();

	return TRUE;
	}

// Notification Handlers

void CItemCreateDialog::OnEditChange(UINT uID, CWnd &Ctrl)
{
	CEditCtrl &Edit = (CEditCtrl &) Ctrl;

	GetDlgItem(IDOK).EnableWindow(Edit.GetWindowTextLength() > 0);
	}

void CItemCreateDialog::OnModeChange(UINT uID, CWnd &Ctrl)
{
	CComboBox &Mode = (CComboBox &) GetDlgItem(202);

	CEditCtrl &Size = (CEditCtrl &) GetDlgItem(203);

	Size.EnableWindow(Mode.GetCurSelData() > 0);
	}

// Command Handlers

BOOL CItemCreateDialog::OnCommandYes(UINT uID)
{
	ReadType();

	if( m_pList && !ReadName() ) {

		return TRUE;
		}

	if( !m_fOne && !ReadSize() ) {

		return TRUE;
		}

	m_Last = m_Type;

	EndDialog((uID == IDOK) ? 1 : 2);

	return TRUE;
	}

BOOL CItemCreateDialog::OnCommandNo(UINT uID)
{
	EndDialog(0);

	return TRUE;
	}

// Implementation

void CItemCreateDialog::LoadType(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(201);

	switch( m_Type ) {

		case typeNumeric:

			List.AddString(CString(IDS_INTEGER_TAG), typeInteger);

			List.AddString(CString(IDS_REAL_TAG),    typeReal);

			List.AddString(CString(IDS_FLAG_TAG),    typeLogical);

			break;

		case typeReal:

			List.AddString(CString(IDS_REAL_TAG),    typeReal);

			List.AddString(CString(IDS_INTEGER_TAG), typeInteger);

			List.AddString(CString(IDS_FLAG_TAG),    typeLogical);

			break;

		case typeInteger:

			List.AddString(CString(IDS_INTEGER_TAG), typeInteger);

			List.AddString(CString(IDS_FLAG_TAG),    typeLogical);

			break;

		case typeLogical:

			List.AddString(CString(IDS_FLAG_TAG),    typeLogical);

			List.AddString(CString(IDS_INTEGER_TAG), typeInteger);

			break;

		case typeString:

			List.AddString(CString(IDS_STRING_TAG), typeString);

			break;

		case typePage:

			List.AddString(CString(IDS_DISPLAY_PAGE_2), typePage);

			break;

		case typeLog:

			List.AddString(CString(IDS_DATA_LOG), typeLog);

			break;

		default:

			List.AddString(CString(IDS_INTEGER_TAG), typeInteger);

			List.AddString(CString(IDS_REAL_TAG),    typeReal);

			List.AddString(CString(IDS_FLAG_TAG),    typeLogical);

			List.AddString(CString(IDS_STRING_TAG),  typeString);

			break;
		}

	List.SetCurSel(0);
	}

void CItemCreateDialog::LoadName(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(200);

	if( !m_Name.IsEmpty() ) {

		Edit.SetWindowText(m_Name);

		MessageBeep(MB_ICONEXCLAMATION);

		CStatic &Icon = (CStatic &) GetDlgItem(400);

		CStatic &Text = (CStatic &) GetDlgItem(300);

		Icon.SetIcon(CIcon(IDI_EXCLAMATION));

		Text.SetFont(afxFont(Bolder));
		}
	else {
		CString Base = L"Tag%u";

		CString Name = L"";

		switch( m_Type ) {

			case typePage:

				Base = L"Page%u";

				break;

			case typeLog:

				Base = L"Log%u";

				break;
			}

		for( UINT n = 1;; n++ ) {

			Name.Printf(Base, n);

			if( !m_pList->FindByName(Name) ) {

				Edit.SetWindowText(Name);

				break;
				}
			}
		}

	OnEditChange(200, Edit);

	SetDlgFocus (200);
	}

void CItemCreateDialog::LoadSize(void)
{
	if( !m_fOne ) {

		CComboBox &Mode = (CComboBox &) GetDlgItem(202);

		CEditCtrl &Size = (CEditCtrl &) GetDlgItem(203);

		Mode.AddString(CString(IDS_ONE_ITEM), DWORD(0));

		Mode.AddString(CString(IDS_ARRAY),    DWORD(1));

		Mode.SetCurSel    (m_Size > 0);

		Size.SetWindowText(CPrintf(L"%u", m_Size ? m_Size : 16));

		Size.EnableWindow (m_Size > 0);
		}
	}

BOOL CItemCreateDialog::ReadType(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(201);

	m_Type = List.GetCurSelData();

	return TRUE;
	}

BOOL CItemCreateDialog::ReadName(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(200);

	m_Name = Edit.GetWindowText();

	if( m_pList->FindByName(m_Name) ) {

		Error(CString(IDS_ITEM_WITH_THIS));

		return FALSE;
		}

	if( !ValidateName(m_Name) ) {

		Error(CString(IDS_COMMS_MAN_NAME_INV));

		return FALSE;
		}

	return TRUE;
	}

BOOL CItemCreateDialog::ReadSize(void)
{
	CComboBox &Mode = (CComboBox &) GetDlgItem(202);

	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(203);

	if( Mode.GetCurSelData() ) {

		m_Size = watoi(Edit.GetWindowText());

		if( m_Size < 1 || m_Size > 16000 ) {

			Error(CString(IDS_ARRAY_SIZE_MUST));

			return FALSE;
			}

		return TRUE;
		}

	m_Size = 0;

	return TRUE;
	}

BOOL CItemCreateDialog::ValidateName(CString Name)
{
	CError Error(TRUE);

	if( m_Type == typeLog ) {

		if( !C3ValidateName(Error, Name) ) {

			return FALSE;
			}
		}
	else {
		CStringArray List;

		Name.Tokenize(List, L'.');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			if( !C3ValidateName(Error, List[n]) ) {

				return FALSE;
				}

			if( List[n].IsEmpty() ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

// End of File
