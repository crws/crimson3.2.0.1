
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DataTag_HPP

#define INCLUDE_DataTag_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Tag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSecDesc;

//////////////////////////////////////////////////////////////////////////
//
// Generic Data Tag
//

class DLLAPI CDataTag : public CTag
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDataTag(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Server Access
		INameServer * GetNameServer(CNameServer *pName);
		IDataServer * GetDataServer(CDataServer *pData);

		// Attibutes
		UINT GetCommsStep(void);

		// Operations
		void UpdateAddr(BOOL fReg);
		void RemapPersist(void);
		void UpdateExtent(void);

		// Item Naming
		CString GetItemOrdinal(void) const;

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Persistance
		void PostPaste(void);
		void PostLoad(void);
		void Kill(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Extent;
		UINT	     m_RdMode;
		UINT         m_Access;
		UINT         m_Persist;
		UINT         m_Addr;
		UINT         m_Alloc;
		UINT         m_FormType;
		UINT	     m_FormLock;
		UINT	     m_ColType;
		CSecDesc   * m_pSec;
		CCodedItem * m_pOnWrite;

	protected:
		// Data Members
		BOOL m_fIntern;

		// Meta Data
		void AddMetaData(void);

		// Memory Sizing
		virtual UINT GetAllocSize(void);
		virtual UINT GetCommsSize(void);

		// Memory Allocation
		void  PersistRegister(DWORD dwAddr, UINT uSize);
		DWORD PersistAllocate(UINT uSize);
		void  PersistFree(DWORD dwAddr, UINT uSize);

		// Implementation
		BOOL MakeReadOnly(IUIHost *pHost);
		BOOL CheckPersist(IUIHost *pHost);
		BOOL CheckOnWrite(IUIHost *pHost);
	};

// End of File

#endif
