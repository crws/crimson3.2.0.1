
#include "Intern.hpp"

#include "CommsDevice.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsDeviceList.hpp"
#include "CommsDevicePage.hpp"
#include "CommsManager.hpp"
#include "CommsMapBlockList.hpp"
#include "CommsPort.hpp"
#include "CommsSysBlock.hpp"
#include "CommsSysBlockList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications Device
//

// Dynamic Class

AfxImplementDynamicClass(CCommsDevice, CUIItem);

// Constructor

CCommsDevice::CCommsDevice(void)
{
	// IDEA -- We have the option of allowing the user to configure
	// regions in which we can define certain comms parameters. This
	// would be done by creating system blocks which we would never
	// delete but which would accept appropriate refererence. A more
	// advanced user could also use this tool to optimize comms.

	m_Name       = L"NAME?";

	m_Number     = 0;

	m_pEnable    = NULL;

	m_Split      = 0;

	m_Delay      = 0;

	m_Transact   = 1;

	m_Preempt    = 0;

	m_Spanning   = 1;

	m_pConfig    = NULL;

	m_pSysBlocks = New CCommsSysBlockList;

	m_pMapBlocks = New CCommsMapBlockList;

	m_pDriver    = NULL;
}

// Initial Values

void CCommsDevice::SetInitValues(void)
{
	SetInitial(L"Enable", m_pEnable, 1);
}

// UI Creation

BOOL CCommsDevice::OnLoadPages(CUIPageList *pList)
{
	CCommsDevicePage *pPage = New CCommsDevicePage(this);

	pList->Append(pPage);

	return FALSE;
}

// UI Update

void CCommsDevice::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag == L"Transact" ) {

		DoEnables(pHost);
	}

	if( Tag == L"Enable" ) {

		pHost->SendUpdate(updateProps);
	}

	if( Tag == L"ButtonDelete" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_DEL_DEVICE);
	}

	if( Tag == L"ButtonAddMap" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_ADD_BLOCK);
	}

	if( Tag == L"ButtonAddTags" ) {

		afxMainWnd->SendMessage(WM_COMMAND, IDM_COMMS_TAG_IMPORT);

		pHost->UpdateUI();
	}

	if( m_pConfig && Tag.StartsWith(L"Button") ) {

		m_pConfig->OnUIChange(pHost, m_pConfig, Tag);
	}

	CUIItem::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CCommsDevice::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Item Location

CCommsDeviceList * CCommsDevice::GetParentList(void) const
{
	return (CCommsDeviceList *) GetParent(1);
}

CCommsPort * CCommsDevice::GetParentPort(void) const
{
	return (CCommsPort *) GetParent(2);
}

// Item Lookup

CCommsSysBlock * CCommsDevice::FindBlock(UINT uBlock) const
{
	return m_pSysBlocks->FindBlock(uBlock);
}

// Driver Access

ICommsDriver * CCommsDevice::GetDriver(void) const
{
	return m_pDriver;
}

// Config Access

CItem * CCommsDevice::GetConfig(void) const
{
	if( !m_pConfig ) {

		CCommsPort *pPort = (CCommsPort *) GetParent(2);

		return pPort->m_pConfig;
	}

	return m_pConfig;
}

// Address Helpers

CString CCommsDevice::SelectAddress(CString Text)
{
	ICommsDriver *pDriver = GetDriver();

	CItem        *pConfig = GetConfig();

	HWND          hWnd    = afxMainWnd->GetHandle();

	CError        Error   = CError(FALSE);

	CAddress      Addr    = { 0 };

	if( !Text.IsEmpty() ) {

		pDriver->ParseAddress(Error, Addr, pConfig, Text);
	}

	if( pDriver->SelectAddress(hWnd, Addr, pConfig, FALSE) ) {

		if( Addr.m_Ref ) {

			pDriver->ExpandAddress(Text, pConfig, Addr);

			return Text;
		}

		return L"";
	}

	return Text;
}

BOOL CCommsDevice::CheckAddress(CString Text)
{
	ICommsDriver *pDriver = GetDriver();

	CItem        *pConfig = GetConfig();

	CError        Error   = CError(FALSE);

	CAddress      Addr;

	if( pDriver->ParseAddress(Error, Addr, pConfig, Text) ) {

		return TRUE;
	}

	return FALSE;
}

// Attributes

BOOL CCommsDevice::AllowMapping(void) const
{
	if( m_pDriver ) {

		// LATER -- Use flags on the driver object for this?

		if( m_pDriver->GetType() == driverSlave ) {

			return TRUE;
		}

		if( m_pDriver->GetType() == driverMaster ) {

			if( m_pDriver->GetID() == 0x330F ) {

				// NOTE: No mapping blocks on RLC instruments.

				return FALSE;
			}

			return TRUE;
		}
	}

	return FALSE;
}

UINT CCommsDevice::GetTreeImage(void) const
{
	CCommsDevice * pDevice = (CCommsDevice *) this;

	BOOL           fEnable = pDevice->CheckEnable(m_pEnable, L">=", 1, TRUE);

	if( m_pDriver ) {

		if( m_pDriver->GetType() == driverCamera ) {

			return fEnable ? IDI_CAM_ENABLED : IDI_CAM_DISABLED;
		}
	}

	return fEnable ? IDI_PLC_ENABLED : IDI_PLC_DISABLED;
}

BOOL CCommsDevice::AllowTagInit(void) const
{
	if( m_pDriver ) {

		if( m_pDriver->GetFlags() & dflagImport ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsDevice::SpanningMode(void) const
{
	return m_Spanning;
}

// Operations

void CCommsDevice::UpdateDriver(void)
{
	if( m_pDriver = GetParentPort()->GetDriver() ) {

		CLASS Class;

		if( Class = m_pDriver->GetDeviceConfig() ) {

			if( m_pConfig ) {

				if( AfxPointerClass(m_pConfig) == Class ) {

					return;
				}

				if( AllowUpgrade(Class) ) {

					if( DoUpgrade(Class) ) {

						return;
					}
				}

				FreeConfig();
			}

			m_pConfig = AfxNewObject(CUIItem, Class);

			m_pConfig->SetParent(this);

			m_pConfig->Init();

			CMetaData const * pData = m_pConfig->FindMetaData(L"Camera");

			if( pData ) {

				pData->WriteInteger(m_pConfig, m_Number);
			}
		}
		else
			FreeConfig();
	}
	else
		FreeConfig();
}

void CCommsDevice::ClearSysBlocks(void)
{
	m_pSysBlocks->DeleteAllItems(TRUE);
}

void CCommsDevice::NotifyInit(void)
{
	if( m_pDriver ) {

		m_pDriver->NotifyInit(m_pConfig);
	}
}

BOOL CCommsDevice::CheckMapBlocks(void)
{
	if( AllowMapping() ) {

		if( m_pMapBlocks->Validate(FALSE) ) {

			return TRUE;
		}

		return FALSE;
	}

	m_pMapBlocks->DeleteAllItems(TRUE);

	return TRUE;
}

void CCommsDevice::Validate(BOOL fExpand)
{
	m_pMapBlocks->Validate(fExpand);
}

void CCommsDevice::CreateNameAndNumber(void)
{
	CCommsManager *pManager = (CCommsManager *) GetParent(AfxRuntimeClass(CCommsManager));

	UINT    Suff = 1;

	CString Name;

	do {
		Name = m_pDriver->GetString(stringDevRoot) + CPrintf(L"%u", Suff++);

	} while( pManager->FindDevice(Name) );

	m_Name   = Name;

	m_Number = pManager->AllocDeviceNumber();
}

// Item Naming

CString CCommsDevice::GetItemOrdinal(void) const
{
	return CPrintf(IDS_COMMS_DEVICE_ID, m_Number);
}

// Persistance

void CCommsDevice::Init(void)
{
	CCodedHost::Init();

	UpdateDriver();

	CreateNameAndNumber();
}

void CCommsDevice::PostLoad(void)
{
	UpdateDriver();

	CCodedHost::PostLoad();
}

// Download Support

BOOL CCommsDevice::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Number));

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddByte(BYTE(m_Split));

	Init.AddWord(WORD(m_Delay));

	Init.AddByte(BYTE(m_Transact));

	Init.AddByte(BYTE(m_Preempt));

	Init.AddByte(BYTE(m_Spanning));

	Init.AddItem(itemSized, m_pConfig);

	Init.AddItem(itemSimple, m_pSysBlocks);

	Init.AddItem(itemSimple, m_pMapBlocks);

	return TRUE;
}

// Meta Data Creation

void CCommsDevice::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Name);
	Meta_AddInteger(Number);
	Meta_AddVirtual(Enable);
	Meta_AddInteger(Split);
	Meta_AddInteger(Delay);
	Meta_AddInteger(Transact);
	Meta_AddInteger(Preempt);
	Meta_AddInteger(Spanning);
	Meta_AddVirtual(Config);
	Meta_AddCollect(SysBlocks);
	Meta_AddCollect(MapBlocks);

	Meta_SetName((IDS_COMMS_DEVICE));
}

// Implementation

void CCommsDevice::DoEnables(IUIHost *pHost)
{
	BOOL fHasUI  = (m_pDbase->GetSoftwareGroup() >= SW_GROUP_3A);

	BOOL fMaster = (GetDriver()->GetType() == driverMaster);

	BOOL fRead   = m_pDbase->IsReadOnly();

	pHost->EnableUI("Split", fHasUI && fMaster && m_Transact);

	pHost->EnableUI("Transact", fMaster);

	pHost->EnableUI("Preempt", fMaster && m_Transact);

	pHost->EnableUI("Precise", fMaster);

	pHost->EnableUI("Delay", fMaster);

	pHost->EnableUI("ButtonAddTags", !fRead);

	pHost->EnableUI("ButtonNewMap", !fRead && AllowMapping());

	pHost->EnableUI("ButtonDelete", !fRead);
}

void CCommsDevice::FreeConfig(void)
{
	if( m_pConfig ) {

		m_pConfig->Kill();

		delete m_pConfig;

		m_pConfig = NULL;
	}
}

BOOL CCommsDevice::AllowUpgrade(CLASS Class)
{
	if( AfxPointerClass(m_pConfig) == Class->GetBaseClass() ) {

		if( m_pDriver ) {

			switch( m_pDriver->GetID() ) {

				case 0x350D:		// Koyo ECOM UDP/IP Driver

					return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsDevice::DoUpgrade(CLASS Class)
{
	CUIItem * pConfig = AfxNewObject(CUIItem, Class);

	if( pConfig ) {

		CTextStreamMemory Stream;

		CTreeFile Tree;

		HGLOBAL hBuffer = m_pConfig->TakeSnapshot();

		if( hBuffer ) {

			if( Stream.OpenLoad(hBuffer) ) {

				if( Tree.OpenLoad(Stream) ) {

					Tree.GetElement();

					pConfig->Load(Tree);

					Tree.EndObject();

					Tree.Close();

					FreeConfig();

					m_pConfig = pConfig;

					m_pConfig->SetParent(this);

					m_pConfig->Init();

					CMetaData const * pData = m_pConfig->FindMetaData(L"Camera");

					if( pData ) {

						pData->WriteInteger(m_pConfig, m_Number);
					}

					return TRUE;
				}
			}
		}

		pConfig->Kill();

		delete pConfig;

		pConfig = NULL;
	}

	return FALSE;
}

// End of File
