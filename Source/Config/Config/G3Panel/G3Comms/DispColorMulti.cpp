
#include "Intern.hpp"

#include "DispColorMulti.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "DispColorMultiEntry.hpp"
#include "DispColorMultiList.hpp"
#include "DispColorMultiPage.hpp"
#include "DispFormatMulti.hpp"
#include "DispFormatMultiEntry.hpp"
#include "DispFormatMultiList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Color
//

// Dynamic Class

AfxImplementDynamicClass(CDispColorMulti, CDispColor);

// Constructor

CDispColorMulti::CDispColorMulti(void)
{
	m_uType   = 3;

	m_pList   = New CDispColorMultiList;

	m_Count   = 3;

	m_Default = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));

	m_Range   = 0;
	}

// UI Creation

BOOL CDispColorMulti::OnLoadPages(CUIPageList *pList)
{
	CDispColorMultiPage *pPage = New CDispColorMultiPage(this);

	pList->Append(pPage);

	return TRUE;
	}

// UI Update

void CDispColorMulti::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "Count" ) {

		if( pHost->HasUndo() ) {

			if( !pHost->InReplay() ) {

				HGLOBAL hPrev = m_pList->TakeSnapshot();

				m_pList->SetItemCount(m_Count);

				HGLOBAL hData = m_pList->TakeSnapshot();

				CCmd *  pCmd  = New CCmdSubItem( L"Color/List",
								 hPrev,
								 hData
								 );

				pHost->SaveExtraCmd(pCmd);

				pHost->RemakeUI();
				}
			}
		else {
			m_pList->SetItemCount(m_Count);

			pHost->RemakeUI();
			}
		}

	if( Tag == "ButtonExport" ) {

		DoExport();
		}

	if( Tag == "ButtonImport" ) {

		if( pHost->HasUndo() ) {

			HGLOBAL hPrev = m_pList->TakeSnapshot();

			if( DoImport() ) {

				HGLOBAL     hData = m_pList->TakeSnapshot();

				CCmdSubItem *pCmd = New CCmdSubItem( L"Color/List",
								     hPrev,
								     hData
								     );

				if( pCmd->IsNull() ) {

					delete pCmd;
					}
				else {
					pHost->SaveExtraCmd(pCmd);

					pHost->RemakeUI();
					}
				}
			else
				GlobalFree(hPrev);
			}
		else {
			if( DoImport() ) {

				pHost->RemakeUI();
				}
			}
		}

	if( Tag == "ButtonSync" ) {

		if( pHost->HasUndo() ) {

			HGLOBAL hPrev = m_pList->TakeSnapshot();

			if( DoSync(TRUE) ) {

				HGLOBAL     hData = m_pList->TakeSnapshot();

				CCmdSubItem *pCmd = New CCmdSubItem( L"Color/List",
								     hPrev,
								     hData
								     );

				if( pCmd->IsNull() ) {

					delete pCmd;
					}
				else {
					pHost->SaveExtraCmd(pCmd);

					pHost->RemakeUI();
					}
				}
			else
				GlobalFree(hPrev);
			}
		else {
			if( DoSync(TRUE) ) {

				pHost->RemakeUI();
				}
			}
		}

	if( Tag == "SUBITEM" ) {

		pHost->RemakeUI();
		}

	CDispColor::OnUIChange(pHost, pItem, Tag);
	}

// Operations

void CDispColorMulti::UpdateTypes(BOOL fComp)
{
	UINT c = m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CDispColorMultiEntry *pItem = m_pList->GetItem(n);

		pItem->UpdateTypes(fComp);
		}
	}

// Color Access

DWORD CDispColorMulti::GetColorPair(DWORD Data, UINT Type)
{
	UINT  c = m_pList->GetItemCount();

	DWORD d = m_Default;

	for( UINT n = 0; n < c; n++ ) {

		CDispColorMultiEntry *pItem = m_pList->GetItem(c-1-n);

		if( pItem ) {

			if( pItem->MatchData(Data, Type, m_Range, d) ) {

				break;
				}
			}
		}

	return d;
	}

// Persistance

void CDispColorMulti::Init(void)
{
	CDispColor::Init();

	m_pList->SetItemCount(m_Count);

	m_pList->GetItem(0U)->Set(1, MAKEWORD(15,1));

	m_pList->GetItem(1U)->Set(2, MAKEWORD(15,2));

	m_pList->GetItem(2U)->Set(3, MAKEWORD(15,4));
	}

// Download Support

BOOL CDispColorMulti::MakeInitData(CInitData &Init)
{
	CDispColor::MakeInitData(Init);

	Init.AddLong(m_Default);

	Init.AddByte(BYTE(m_Range));

	Init.AddItem(itemSimple, m_pList);

	return TRUE;
	}

// Meta Data Creation

void CDispColorMulti::AddMetaData(void)
{
	CDispColor::AddMetaData();

	Meta_AddCollect(List);
	Meta_AddInteger(Count);
	Meta_AddInteger(Range);
	Meta_AddInteger(Default);

	Meta_SetName((IDS_MULTISTATE_COLOR));
	}

// Implementation

BOOL CDispColorMulti::DoSync(BOOL fUI)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pMeta = pItem->FindMetaData(L"Format");

	BOOL             fCopy = FALSE;

	if( pMeta ) {

		CMetaItem *pChild = (CMetaItem *) pMeta->GetObject(pItem);

		if( pChild ) {

			if( pChild->IsKindOf(AfxRuntimeClass(CDispFormatMulti)) ) {

				CDispFormatMulti *pMulti = (CDispFormatMulti *) pChild;

				UINT              uCount = m_pList->GetItemCount();

				for( UINT n = 0; n < uCount; n++ ) {

					CDispFormatMultiEntry *pFrom = pMulti->m_pList->GetItem(n);

					if( pFrom ) {

						if( pFrom->m_pData ) {

							CDispColorMultiEntry *pEntry = m_pList->GetItem(n);

							pEntry->Set(pFrom->m_pData->GetSource(TRUE));

							fCopy = TRUE;
							}
						}
					}
				}
			}
		}

	if( fUI ) {

		CWnd &Wnd = CWnd::GetActiveWindow();

		if( fCopy ) {

			Wnd.Information(CString(IDS_STATE_DATA_COPIED));

			return TRUE;
			}

		Wnd.Error(CString(IDS_ITEM_DOES_NOT));
		}

	return fCopy;
	}

BOOL CDispColorMulti::DoExport(void)
{
	// TODO -- Use stream and support ansi format, too.

	CSaveFileDialog Dialog;

	Dialog.LoadLastPath(L"Format");

	Dialog.SetCaption(CString(IDS_EXPORT_COLORING));

	Dialog.SetFilter (CString(IDS_TEXT_FILES_TXTTXT));

	if( Dialog.ExecAndCheck() ) {

		FILE *pFile = _wfopen(Dialog.GetFilename(), L"wb");

		if( pFile ) {

			afxThread->SetWaitMode(TRUE);

			fwprintf(pFile, L"\xFEFF");

			fwprintf(pFile, L"Data\tFore\tBack\r\n");

			UINT uCount = m_pList->GetItemCount();

			for( UINT n = 0; n < uCount; n++ ) {

				CDispColorMultiEntry *pEntry = m_pList->GetItem(n);

				CString Data, Fore, Back;

				if( pEntry->m_pData ) {

					Data = pEntry->m_pData->GetSource(TRUE);
					}

				Fore.Printf(L"0x%4.4X", LOWORD(pEntry->m_Color));

				Back.Printf(L"0x%4.4X", HIWORD(pEntry->m_Color));

				// cppcheck-suppress invalidPrintfArgType_s

				fwprintf(pFile, L"%s\t%s\t%s\r\n", PCTXT(Data), PCTXT(Fore), PCTXT(Back));
				}

			fclose(pFile);

			afxThread->SetWaitMode(FALSE);

			return TRUE;
			}

		CWnd::GetActiveWindow().Error(CString(IDS_UNABLE_TO_WRITE));
		}

	return FALSE;
	}

BOOL CDispColorMulti::DoImport(void)
{
	// TODO -- Use stream and support ansi format, too.

	COpenFileDialog Dialog;

	Dialog.LoadLastPath(L"Format");

	Dialog.SetCaption(CString(IDS_IMPORT_COLORING));

	Dialog.SetFilter (CString(IDS_TEXT_FILES_TXTTXT));

	if( Dialog.Execute() ) {

		FILE *pFile = _wfopen(Dialog.GetFilename(), L"rb");

		if( pFile ) {

			afxThread->SetWaitMode(TRUE);

			if( !(getc(pFile) == 0xFF && getc(pFile) == 0xFE) ) {

				fseek(pFile, 0, SEEK_SET);
				}

			CStringArray Data, Fore, Back;

			UINT n;

			for( n = 0;; n++ ) {

				WCHAR sLine[1024] = {0};

				fgetws(sLine, sizeof(sLine), pFile);

				if( sLine[0] ) {

					if( n > 0 ) {

						CStringArray List;

						CString Line(sLine, wstrlen(sLine)-2);

						Line.Tokenize(List, L'\t');

						Data.Append(List[0]);

						Fore.Append(List[1]);

						Back.Append(List[2]);
						}
					else {
						if( wstrcmp(sLine, L"Data\tFore\tBack\r\n") ) {

							break;
							}
						}

					continue;
					}

				break;
				}

			afxThread->SetWaitMode(FALSE);

			fclose(pFile);

			if( !n ) {

				CWnd::GetActiveWindow().Error(CString(IDS_INVALID_HEADER));

				return FALSE;
				}

			m_Count = n - 1;

			m_pList->SetItemCount(m_Count);

			if( m_Count ) {

				for( UINT n = 0; n < m_Count; n++ ) {

					CDispColorMultiEntry *pEntry = m_pList->GetItem(n);

					pEntry->Set(Data[n], Fore[n], Back[n]);
					}
				}

			return TRUE;
			}

		CWnd::GetActiveWindow().Error(CString(IDS_UNABLE_TO_READ));
		}

	return FALSE;
	}

// End of File
