
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextCertRef_HPP

#define INCLUDE_UITextCertRef_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Certificate Reference
//

class CUITextCertRef : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextCertRef(void);

	protected:
		// Data
		BOOL m_fIdent;

		// Overridables
		void OnBind(void);
		BOOL OnEnumValues(CStringArray &List);

		// Implementation
		void AddData(void);
		void AddData(UINT Data, CString Text);
	};

// End of File

#endif
