
#include "Intern.hpp"

#include "CommsPortCAN.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAN Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortCAN, CCommsPort);

// Constructor

CCommsPortCAN::CCommsPortCAN(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding   = bindCAN;

	m_Baud      = 125000;
	}

// Overridables

UINT CCommsPortCAN::GetPageType(void) const
{
	return m_pDriver ? 1 : 0;
	}

CHAR CCommsPortCAN::GetPortTag(void) const
{
	return 'C';
}

BOOL CCommsPortCAN::IsRemotable(void) const
{
	return m_Remotable;
}

void CCommsPortCAN::FindBindingMask(void)
{
	CCommsPort::FindBindingMask();
	}

// Download Support

BOOL CCommsPortCAN::MakeInitData(CInitData &Init)
{
	Init.AddByte(m_Binding == bindCAN ? 4 : 11);

	CCommsPort::MakeInitData(Init);

	if( m_pDriver ) {

		Init.AddLong(LONG(m_Baud));
		}

	return TRUE;
	}

// Meta Data Creation

void CCommsPortCAN::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_AddInteger(Baud);
	Meta_AddInteger(Remotable);

	Meta_SetName((IDS_CAN_PORT));
	}

// End of File
