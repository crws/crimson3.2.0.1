
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DataLog_HPP

#define INCLUDE_DataLog_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagSet;

//////////////////////////////////////////////////////////////////////////
//
// Data Log
//

class CDataLog : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDataLog(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Naming
		CString GetHumanName(void) const;
		CString GetItemOrdinal(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CString		m_Name;
		CString	        m_Path;
		UINT            m_Type;
		UINT		m_Update;
		UINT		m_FileLimit;
		UINT		m_FileCount;
		UINT		m_WithBatch;
		UINT            m_SignLogs;
		UINT		m_Comments;
		UINT            m_Merge;
		CCodedItem    * m_pEnable;
		CCodedItem    * m_pTrigger;
		CTagSet	      *	m_pSet;
		CTagSet	      *	m_pMon;
		UINT		m_Mail;
		UINT		m_Drive;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
