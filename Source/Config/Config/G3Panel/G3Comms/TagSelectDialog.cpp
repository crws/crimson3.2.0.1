
#include "Intern.hpp"

#include "TagSelectDialog.hpp"

#include "gdiplus.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "DataTag.hpp"
#include "ItemCreateDialog.hpp"
#include "TagFolder.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CTagSelectDialog, CStdDialog);

// Constructors

CTagSelectDialog::CTagSelectDialog(CDatabase *pDbase, CString Tag)
{
	m_pDbase   = pDbase;

	m_fBinding = FALSE;

	m_fFolder  = FALSE;

	m_Tag      = Tag;

	m_cfIdent  = RegisterClipboardFormat(L"C3.1 Identifier");

	SetName(L"TagSelectDialog");
	}

CTagSelectDialog::CTagSelectDialog(CDatabase *pDbase, BOOL fFolder, CString Class)
{
	m_pDbase   = pDbase;

	m_fBinding = TRUE;

	m_fFolder  = fFolder;

	m_Class    = Class;

	m_cfIdent  = RegisterClipboardFormat(L"C3.1 Identifier");

	FindBindTemplate();
	}

// IUnknown

HRESULT CTagSelectDialog::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CTagSelectDialog::AddRef(void)
{
	return 1;
	}

ULONG CTagSelectDialog::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CTagSelectDialog::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( CanAcceptDataObject(pData) ) {

		*pEffect = DROPEFFECT_LINK;

		m_fDrop  = TRUE;

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	m_fDrop  = FALSE;

	return S_OK;
	}

HRESULT CTagSelectDialog::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = m_fDrop ? DROPEFFECT_LINK : DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CTagSelectDialog::DragLeave(void)
{
	m_DropHelp.DragLeave();

	return S_OK;
	}

HRESULT CTagSelectDialog::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_fDrop ) {

		if( AcceptDataObject(pData) ) {

			EndDialog(TRUE);
			}
		}

	*pEffect = m_fDrop ? DROPEFFECT_LINK : DROPEFFECT_NONE;

	m_fDrop  = FALSE;

	return S_OK;
	}

// Attributes

CString CTagSelectDialog::GetTag(void) const
{
	return m_Tag;
	}

// Message Map

AfxMessageMap(CTagSelectDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchCommand(IDOK, OnCommandOK    )
	AfxDispatchCommand(200,  OnCommandCreate)

	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CTagSelectDialog)
	};

// Message Handlers

BOOL CTagSelectDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_DropHelp.Bind(m_hWnd, this);

	if( m_fFolder ) {

		CWnd &  Wnd  = GetDlgItem(101);

		CString Text = Wnd.GetWindowText();

		Wnd.SetWindowText(CFormat(Text, m_Class));
		}

	GetDlgItem(100).SetFont(afxFont(Bolder));

	GetDlgItem(101).SetFont(afxFont(Bolder));

	GetDlgItem(102).SetFont(afxFont(Bolder));

	GetDlgItem(103).SetFont(afxFont(Bolder));

	return TRUE;
	}

void CTagSelectDialog::OnPaint(void)
{
	UINT    uFrom = 0;

	HGLOBAL hFrom = afxModule->LoadResource(L"Target", L"PNG", uFrom);

	HGLOBAL hData = GlobalAlloc(GHND, uFrom);

	AfxAssume(hData);

	PBYTE   pFrom = PBYTE(LockResource(hFrom));

	PBYTE   pData = PBYTE(GlobalLock(hData));

	memcpy(pData, pFrom, uFrom);

	UnlockResource(hFrom);

	FreeResource(hFrom);

	GlobalUnlock(hData);

	PaintImage(CPaintDC(ThisObject), hData);

	GlobalFree(hData);
	}

// Command Handlers

BOOL CTagSelectDialog::OnCommandOK(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

BOOL CTagSelectDialog::OnCommandCreate(UINT uID)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	CTagManager  *pTags   = pSystem->m_pTags;

	CTagList     *pList   = pTags->m_pTags;

	CItemCreateDialog Dlg(pList, typeVoid);

	if( Dlg.Execute() ) {

		CString Name = Dlg.GetName();

		UINT    Type = Dlg.GetType();

		UINT    Size = Dlg.GetSize();

		if( pTags->CreateTag(Name, Type, Size) ) {

			if( Size ) {

				Name += L"[0]";
				}

			m_Tag = Name;

			EndDialog(TRUE);

			return TRUE;
			}

		Error(CString(IDS_TAG_COULD_NOT_BE));
		}

	return TRUE;
	}

BOOL CTagSelectDialog::OnPasteControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

BOOL CTagSelectDialog::OnPasteCommand(UINT uID)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		if( CanAcceptDataObject(pData) ) {

			if( AcceptDataObject(pData) ) {

				EndDialog(TRUE);
				}
			}

		pData->Release();
		}

	return TRUE;
	}

// Data Object Helpers

BOOL CTagSelectDialog::CanAcceptDataObject(IDataObject *pData)
{
	CStringArray List;

	if( AcceptDataObject(pData, List) ) {

		if( List[0] == m_pDbase->GetUniqueSig() ) {

			if( List[1] == L"CDataTag" ) {

				// NOTE -- This is a nasty test to make sure that the pointer
				// is still pointer where it ought to. We really ought to change
				// this clipboard format to something more sensible!

				CTag *pTag = (CTag *) wcstol(List[3], NULL, 16);

				DWORD Test =          wcstol(List[4], NULL, 16);

				if( !IsBadReadPtr(pTag, 4) ) {

					if( PDWORD(pTag)[0] == Test ) {

						if( pTag->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

							if( m_fFolder ) {

								if( !m_Class.IsEmpty() ) {

									CTagFolder *pFolder = (CTagFolder *) pTag;

									if( pFolder->m_Class == m_Class ) {

										return TRUE;
										}

									return FALSE;
									}

								return TRUE;
								}

							return FALSE;
							}

						if( m_fFolder ) {

							return FALSE;
							}

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CTagSelectDialog::AcceptDataObject(IDataObject *pData)
{
	CStringArray List;

	if( AcceptDataObject(pData, List) ) {

		// NOTE -- This is a nasty test to make sure that the pointer
		// is still pointer where it ought to. We really ought to change
		// this clipboard format to something more sensible!

		CTag * pTag = (CTag *) wcstol(List[3], NULL, 16);

		DWORD  Test =          wcstol(List[4], NULL, 16);

		if( !IsBadReadPtr(pTag, 4) ) {

			if( PDWORD(pTag)[0] == Test ) {

				if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					CDataTag *pData = (CDataTag *) pTag;

					if( pData->m_Extent ) {

						m_Tag = List[2] + L"[0]";

						return TRUE;
						}
					}
				}
			}

		m_Tag = List[2];

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagSelectDialog::AcceptDataObject(IDataObject *pData, CStringArray &List)
{
	FORMATETC Fmt = { WORD(m_cfIdent), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		Text.Tokenize(List, L'|');

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CTagSelectDialog::FindBindTemplate(void)
{
	if( m_fFolder ) {

		if( m_Class.IsEmpty() ) {

			SetName(L"FolderBindingDialog2");
			}
		else
			SetName(L"FolderBindingDialog1");

		return;
		}

	SetName(L"TagBindingDialog");
	}

void CTagSelectDialog::PaintImage(CDC &DC, HGLOBAL hData)
{
	IStream *pStream = NULL;

	CreateStreamOnHGlobal(hData, FALSE, &pStream);

	if( pStream ) {

		GpImage *pImage  = NULL;

		GdipLoadImageFromStream(pStream, &pImage);

		if( pImage ) {

			CRect       Rect   = GetClientRect();

			GpGraphics *pGraph = NULL;

			GdipCreateFromHDC(DC.GetHandle(), &pGraph);

			GdipDrawImageRectI( pGraph,
					    pImage,
					    (Rect.cx() -  64) / 2,
					    (Rect.cy() - 115) / 1,
					    64,
					    60
					    );

			GdipDeleteGraphics(pGraph);

			GdipDisposeImage(pImage);
			}

		pStream->Release();
		}
	}

// End of File
