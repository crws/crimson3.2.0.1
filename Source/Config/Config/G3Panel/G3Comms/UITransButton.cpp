
#include "Intern.hpp"

#include "UITransButton.hpp"

#include "gdiplus.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Translation Button
//

// Runtime Class

AfxImplementRuntimeClass(CUITransButton, CUIButton);

// Constructor

CUITransButton::CUITransButton(CString Logo) : CUIButton( CString(IDS_AUTOTRANSLATE),
							  CString(IDS_FORMAT_2),
							  L"ButtonAuto"
							  )
{
	m_Logo = Logo;
	}

// Core Overridables

void CUITransButton::OnPaint(CRect const &Work, CDC &DC)
{
	if( !m_Logo.IsEmpty() ) {

		UINT    uData = 0;

		HGLOBAL hData = afxModule->LoadResource(PCTXT(m_Logo), L"LOGO", uData);

		if( hData ) {

			HGLOBAL hCopy = GlobalAlloc(GHND, uData);

			AfxAssume(hCopy);

			PBYTE   pData = PBYTE(LockResource(hData));

			PBYTE   pCopy = PBYTE(GlobalLock(hCopy));

			memcpy(pCopy, pData, uData);

			UnlockResource(hData);

			FreeResource(hData);

			GlobalUnlock(hCopy);

			////////

			IStream *pStream = NULL;

			GpImage *pImage  = NULL;

			CreateStreamOnHGlobal(hCopy, TRUE, &pStream);

			GdipLoadImageFromStream(pStream, &pImage);

			if( pImage ) {

				CRect Ctrl = m_pCtrlLayout->GetRect();

				CRect Rect;

				Rect.right  = Work.right - 40;

				Rect.top    = (Ctrl.top + Ctrl.bottom) / 2 - 16;

				Rect.left   = Rect.right - 158;

				Rect.bottom = Rect.top   + 32;

				GpGraphics *pGraph = NULL;

				GdipCreateFromHDC(DC.GetHandle(), &pGraph);

				GdipDrawImageRectI( pGraph,
						    pImage,
						    Rect.left,
						    Rect.top,
						    Rect.cx(),
						    Rect.cy()
						    );

				GdipDeleteGraphics(pGraph);

				GdipDisposeImage(pImage);
				}
			}
		}
	}

// End of File
