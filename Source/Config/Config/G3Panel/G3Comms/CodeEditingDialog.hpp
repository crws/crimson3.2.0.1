
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CodeEditingDialog_HPP

#define INCLUDE_CodeEditingDialog_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCodedHost;

//////////////////////////////////////////////////////////////////////////
//
// Complex Code Editor Dialog
//

class DLLAPI CCodeEditingDialog : public CDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CCodeEditingDialog(CCodedHost *pHost, CString Tag, CString Params, CString Text);

		// Attributes
		CString GetAsText(void) const;

		// Dialog Operations
		BOOL Create(CWnd &Parent);

	protected:
		// Data Members
		CCodedHost       * m_pHost;
		CString            m_Tag;
		CString            m_Params;
		CString		   m_Text;
		CTypeDef	   m_Type;
		CSize		   m_Border;
		CSize		   m_Button;
		CSize		   m_Header;
		CButton          * m_pGroup[2];
		CButton          * m_pButton[2];
		CSourceEditorWnd * m_pEditor;
		CEditCtrl        * m_pHeader;
		BOOL               m_fFunc;
		BOOL               m_fPending;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnSize(UINT uCode, CSize Size);
		void OnGetMinMaxInfo(MINMAXINFO &Info);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPreDestroy(void);

		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);

		// Implementation
		BOOL IsComplex(CString Text);
		void SetCaption(void);
		BOOL LoadConfig(CRect &Rect);
		void SaveConfig(void);
		void FindLayout(void);
		void MakeGroups(void);
		void MakeButtons(void);
		void MoveButtons(void);
		void MakeEditor(void);
		void LoadEditor(void);
		void MakeHeader(void);
		BOOL Compile(CError &Error, CString Text);
		void LoadParams(CCompileIn &In);
		void KillParams(CCompileIn &In);
	};

// End of File

#endif
