
#include "Intern.hpp"

#include "TagQuickPlot.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Quick Plot
//

// Dynamic Class

AfxImplementDynamicClass(CTagQuickPlot, CCodedHost);

// Constructor

CTagQuickPlot::CTagQuickPlot(void)
{
	m_Mode    = 0;

	m_Rewind  = 0;

	m_pEnable = NULL;

	m_pStore  = NULL;

	m_pTime   = NULL;

	m_pLook   = NULL;

	m_Points  = 120;
	}

// UI Update

void CTagQuickPlot::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Mode" ) {

			if( m_Mode == 1 ) {

				m_Rewind = 0;

				pHost->UpdateUI(this, L"Rewind");
				}

			pHost->EnableUI(this, L"Rewind", m_Mode>1);
			pHost->EnableUI(this, L"Enable", m_Mode>0);
			pHost->EnableUI(this, L"Store",  m_Mode>0);
			pHost->EnableUI(this, L"Time",   m_Mode>0);
			pHost->EnableUI(this, L"Look",   m_Mode>0);
			pHost->EnableUI(this, L"Points", m_Mode>0);
			}
		}
	}

// Type Access

BOOL CTagQuickPlot::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Download Suport

BOOL CTagQuickPlot::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));
	Init.AddByte(BYTE(m_Rewind));

	Init.AddItem(itemVirtual, m_pEnable);
	Init.AddItem(itemVirtual, m_pStore);
	Init.AddItem(itemVirtual, m_pLook);
	Init.AddItem(itemVirtual, m_pTime);

	Init.AddWord(WORD(m_Points));

	return TRUE;
	}

// Meta Data

void CTagQuickPlot::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddInteger(Rewind);
	Meta_AddVirtual(Enable);
	Meta_AddVirtual(Store);
	Meta_AddVirtual(Time);
	Meta_AddVirtual(Look);
	Meta_AddInteger(Points);

	Meta_SetName((IDS_QUICK_TREND));
	}

// End of File
