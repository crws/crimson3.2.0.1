
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CloudTagSet_HPP

#define INCLUDE_CloudTagSet_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudDataSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagSet;

//////////////////////////////////////////////////////////////////////////
//
// Cloud Tag Set
//

class CCloudTagSet : public CCloudDataSet
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CCloudTagSet(void);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Persistance
	void Load(CTreeFile &Tree);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Data Members
	UINT      m_Label;
	UINT	  m_TagName;
	UINT      m_Tree;
	UINT	  m_Array;
	UINT      m_Props;
	UINT      m_Write;
	CTagSet	* m_pSet;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
};

// End of File

#endif
