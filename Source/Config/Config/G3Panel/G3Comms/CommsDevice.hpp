

#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsDevice_HPP

#define INCLUDE_CommsDevice_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDeviceList;
class CCommsMapBlockList;
class CCommsPort;
class CCommsSysBlock;
class CCommsSysBlockList;

//////////////////////////////////////////////////////////////////////////
//
// Communications Device
//

class DLLAPI CCommsDevice : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsDevice(void);

		// Initial Values
		void SetInitValues(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Location
		CCommsDeviceList * GetParentList(void) const;
		CCommsPort       * GetParentPort(void) const;

		// Item Lookup
		CCommsSysBlock * FindBlock(UINT uBlock) const;

		// Driver Access
		ICommsDriver * GetDriver(void) const;

		// Config Access
		CItem * GetConfig(void) const;

		// Address Helpers
		CString SelectAddress(CString Text);
		BOOL    CheckAddress (CString Text);

		// Attributes
		UINT GetTreeImage(void) const;
		BOOL AllowMapping(void) const;
		BOOL AllowTagInit(void) const;
		BOOL SpanningMode(void) const;
		BOOL IsMaster(void) const;

		// Operations
		void UpdateDriver(void);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		BOOL CheckMapBlocks(void);
		void Validate(BOOL fExpand);
		void CreateNameAndNumber(void);

		// Item Naming
		CString GetItemOrdinal(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CString		     m_Name;
		UINT                 m_Number;
		CCodedItem         * m_pEnable;
		UINT                 m_Split;
		UINT                 m_Delay;
		UINT		     m_Transact;
		UINT		     m_Preempt;
		UINT		     m_Spanning;
		CUIItem		   * m_pConfig;
		CCommsSysBlockList * m_pSysBlocks;
		CCommsMapBlockList * m_pMapBlocks;

	protected:
		// Data Members
		ICommsDriver * m_pDriver;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void FreeConfig(void);
		BOOL AllowUpgrade(CLASS Class);
		BOOL DoUpgrade(CLASS Class);
	};

// End of File

#endif
