
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagExport_HPP

#define INCLUDE_TagExport_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

//#include "USTOMTAGIMPINFO.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CTag;
class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Tag Export Helper
//

struct CUSTOMTAGIMPINFO
{
	UINT		uIDS;
	UINT		uState;
	UINT		uFileLine;
	UINT		uFormat;
	UINT		uItem;
	CString		sFileName;
	CString		sTagLine;
	CString		sType;
	CItem		*pConfig;
	CCommsSystem	*pCSys;
	};

#define	CTPRESENT	4
#define	CTSELECTED	8

class CTagExport : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTagExport(CTagList *pTags);

		// Operations
		BOOL Export(ITextStream &Stm, WCHAR cSep);
		BOOL Import(ITextStream &Stm, WCHAR cSep, CLongArray &Lines);
		BOOL SetCustomImport(UINT uIDS, CString sFileName, CCommsSystem *pSys);

	protected:
		// Typedefs
		typedef CMap <INDEX,   UINT > CPropMap;
		typedef CMap <CString, INDEX> CColDict;

		// Data Members
		CTagList *  m_pTags;
		CStringTree m_List;
		CColDict    m_ColDict;
		CStringList m_ColList;
		CPropMap    m_Map;
		CUSTOMTAGIMPINFO  m_Info;

		// Implementation
		BOOL    FindTagClasses(void);
		BOOL    FindClassCols(CString Class);
		BOOL    BuildPropMap(CStringArray const &Props);
		void    WriteClass(ITextStream &Stm, CString Class);
		void    WriteCols(ITextStream &Stm, WCHAR cSep);
		BOOL    WriteTags(ITextStream &Stm, WCHAR cSep, CString Class);
		CString MakeName(CString Prop);
		CString MakeName(CStringArray const &Parts);
		CTag *  CreateTag(CLASS Class, CString Name);
		BOOL    CheckParents(CString Name);
		BOOL    CheckTagName(CString Name);
		BOOL    IsPrivate(CTag *pTag);

		// Import Custom Tag Processing
		void	MakeUnicode(PTXT sLine, CString sCustom, BOOL fAddNull);

		BOOL	ParseFileName(CString sFileName);

		// Custom Import Helpers
		BOOL	IsCTSelected(void);
	};

// End of File

#endif
