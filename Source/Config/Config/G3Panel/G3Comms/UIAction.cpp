
#include "Intern.hpp"

#include "UIAction.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodeEditingDialog.hpp"

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Action
//

// Dynamic Class

AfxImplementDynamicClass(CUIAction, CUICategorizer);

// Constructor

CUIAction::CUIAction(void)
{
	m_cfCode = RegisterClipboardFormat(L"C3.1 Action");
	}

// Core Overridables

void CUIAction::OnBind(void)
{
	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( !List[2].IsEmpty() ) {

		m_Params = List[2];
		}

	CUICategorizer::OnBind();
	}

void CUIAction::OnCreate(CWnd &Wnd, UINT &uID)
{
	CUICategorizer::OnCreate(Wnd, uID);
	}

// Notification Overridables

BOOL CUIAction::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pPickCtrl->GetID() ) {

		CString Text = m_pText->GetAsText();

		if( EditCode(Text) ) {

			StdSave(TRUE, FindDataText(Text, m_uMode));

			return TRUE;
			}
		}

	return CUICategorizer::OnNotify(uID, uCode);
	}

// Data Overridables

BOOL CUIAction::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{

	// LATER -- Accept partial comms addresses.

	if( CanAcceptText(pData, m_cfCode) ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
		}

	if( CanAcceptText(pData, CF_UNICODETEXT) ) {

		dwEffect = DROPEFFECT_COPY;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIAction::OnAcceptData(IDataObject *pData)
{
	// LATER -- Accept partial comms addresses.

	if( AcceptText(pData, m_cfCode) ) {

		SetBestFocus();

		return TRUE;
		}

	if( AcceptText(pData, CF_UNICODETEXT) ) {

		SetBestFocus();

		return TRUE;
		}

	return FALSE;
	}

// Category Overridables

BOOL CUIAction::LoadModeButton(void)
{
	CModeButton::COption Opt;

	m_pModeCtrl->ClearOptions();

	if( TRUE ) {

		Opt.m_uID     = modeGeneralWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeComplexWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeGeneral;
		Opt.m_Text    = CString(IDS_GENERAL);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeComplex;
		Opt.m_Text    = CString(IDS_COMPLEX);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	return TRUE;
	}

BOOL CUIAction::SwitchMode(UINT uMode)
{
	if( uMode == NOTHING ) {

		if( m_uMode == modeComplex || m_uMode == modeComplexWas ) {

			CString Text = m_pText->GetAsText();

			if( Text.StartsWith(L"WAS ") ) {

				Text = Text.Mid(4);
				}

			if( EditCode(Text) ) {

				StdSave(TRUE, FindDataText(Text, m_uMode));

				ForceUpdate();

				return TRUE;
				}

			return FALSE;
			}

		SwitchMode(modeGeneral);

		return TRUE;
		}

	if( uMode == modeGeneralWas ) {

		return TRUE;
		}

	if( uMode == modeGeneral ) {

		if( m_uMode == modeComplex || m_uMode == modeComplexWas ) {

			// LATER -- If there's only one none-empty line of code,
			// we can convert this to General without actually having
			// to drop anything. Add support for this.

			CString Warn;

			Warn += CString(IDS_THIS_WILL_DELETE);

			Warn += L"\n\n";

			Warn += CString(IDS_DO_YOU_WANT_TO_4);

			if( CWnd::GetActiveWindow().NoYes(Warn) == IDNO ) {

				return FALSE;
				}

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(L"", TRUE);

			return TRUE;
			}

		if( m_uMode == modeGeneralWas ) {

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(m_pShowCtrl->GetWindowText(), TRUE);

			return TRUE;
			}

		return FALSE;
		}

	if( uMode == modeComplex ) {

		CString Text = m_pText->GetAsText();

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);
			}

		if( !IsComplex(Text) ) {

			MakeComplex(Text);
			}

		if( EditCode(Text) ) {

			m_pModeCtrl->SetData(m_uMode = uMode);

			StdSave(TRUE, FindDataText(Text, uMode));

			ForceUpdate();

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

UINT CUIAction::FindDispMode(CString Text)
{
	if( Text.StartsWith(L"WAS ") ) {

		Text = Text.Mid(4);

		if( IsComplex(Text) ) {

			return modeComplexWas;
			}

		return modeGeneralWas;
		}

	if( IsComplex(Text) ) {

		return modeComplex;
		}

	return modeGeneral;
	}

CString CUIAction::FindDispText(CString Text, UINT uMode)
{
	if( uMode == modeGeneralWas ) {

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);

			return Text;
			}
		}

	if( uMode == modeComplexWas ) {

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);

			return Text.StripToken(L"\r\n").Mid(3);
			}
		}

	if( uMode == modeComplex ) {

		return Text.StripToken(L"\r\n").Mid(3);
		}

	return Text;
	}

CString CUIAction::FindDataText(CString Text, UINT uMode)
{
	if( uMode == modeGeneralWas ) {

		return L"WAS " + Text;
		}

	if( uMode == modeComplexWas ) {

		return L"WAS " + Text;
		}

	return Text;
	}

UINT CUIAction::FindDispType(UINT uMode)
{
	if( uMode == modeGeneral ) {

		return 2;
		}

	return 1;
	}

CString CUIAction::FindDispVerb(UINT uMode)
{
	if( uMode == modeGeneralWas ) {

		return L"";
		}

	if( uMode == modeComplexWas ) {

		return L"";
		}

	return CString(IDS_EDIT_2);
	}

// Implementation

BOOL CUIAction::EditCode(CString &Text)
{
	CCodedHost *pHost = (CCodedHost *) m_pItem;

	if( pHost->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

		CCodeEditingDialog Dlg( pHost,
					m_UIData.m_Tag,
					m_Params,
					Text
					);

		CSysProxy System;

		System.Bind();

		if( System.ExecSemiModal(Dlg) ) {

			Text = Dlg.GetAsText();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUIAction::IsComplex(CString Text)
{
	return Text.Find(L"\r\n") < NOTHING;
	}

void CUIAction::MakeComplex(CString &Code)
{
	CString Text;

	Text += L"// ";

	Text += CString(IDS_COMPLEX_CODE);

	Text += L"\r\n";

	if( !Code.IsEmpty() ) {

		Text += Code;

		Text += L";";

		Text += L"\r\n";
		}

	Code  = Text;
	}

// End of File
