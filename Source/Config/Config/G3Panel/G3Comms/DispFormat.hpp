
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormat_HPP

#define INCLUDE_DispFormat_HPP

//////////////////////////////////////////////////////////////////////////
//
// Format Flags
//

enum
{
	fmtStd	 = 0,
	fmtPad	 = 1,
	fmtBare  = 2,
	fmtUnits = 4,
	};

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispFormat;

//////////////////////////////////////////////////////////////////////////
//
// Display Format
//

class DLLAPI CDispFormat : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Class Enumeration
		static CLASS GetClass(UINT uType);

		// General Formatting
		static CString GeneralFormat(DWORD Data, UINT Type, UINT Flags);

		// Constructor
		CDispFormat(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		UINT GetFormType(void) const;
		BOOL NeedsLimits(void) const;

		// Formatting
		virtual CString Format(DWORD Data, UINT Type, UINT Flags);

		// Limit Access
		virtual DWORD GetMin(UINT Type);
		virtual DWORD GetMax(UINT Type);

		// Property Preservation
		virtual BOOL Preserve(CDispFormat *pOld);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT m_uType;
		BOOL m_fLimits;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		static void MakeDigitsFixed(CString &Text);
		static void MakeLettersFixed(CString &Text);
		static void MakeDigitsFixed(PTXT pText);
		static void MakeLettersFixed(PTXT pText);
	};

// End of File

#endif
