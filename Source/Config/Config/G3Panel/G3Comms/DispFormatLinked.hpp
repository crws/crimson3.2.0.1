
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatLinked_HPP

#define INCLUDE_DispFormatLinked_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispFormat.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTag;

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Format
//

class DLLAPI CDispFormatLinked : public CDispFormat
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatLinked(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Formatting
		CString Format(DWORD Data, UINT Type, UINT Flags);

		// Format Access
		BOOL GetFormat(CDispFormat * &pFormat);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem *m_pLink;

	protected:
		// Data Members
		CDispFormat * m_pFormat;
		CTag        * m_pTag;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL TestFormat(void);
		BOOL FindFormat(void);
	};

// End of File

#endif
