
#include "Intern.hpp"

#include "TagString.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "DispColorFixed.hpp"
#include "DispFormat.hpp"
#include "SecDesc.hpp"
#include "TagStringPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// String Tag Item
//

// Dynamic Class

AfxImplementDynamicClass(CTagString, CDataTag);

// Constructor

CTagString::CTagString(void)
{
	m_Encode  = 0;
	m_Length  = 16;
	m_pSec    = New CSecDesc;
	}

// UI Creation

BOOL CTagString::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() == SW_GROUP_2 ) {

		CTagStringPage *pPage1 = New CTagStringPage(this, CString(IDS_DATA),     8);

		pList->Append(pPage1);
		}
	else {
		if( FALSE ) {

			CTagStringPage *pPage1 = New CTagStringPage(this, CString(IDS_DATA),     1);

			CTagStringPage *pPage2 = New CTagStringPage(this, CString(IDS_FORMAT_6), 2);

			pList->Append(pPage1);

			pList->Append(pPage2);
			}
		else {
			CTagStringPage *pPage1 = New CTagStringPage(this, CString(IDS_DATA),     1);

			CTagStringPage *pPage2 = New CTagStringPage(this, CString(IDS_FORMAT_6), 2);

			CTagStringPage *pPage3 = New CTagStringPage(this, CString(IDS_COLORS),   3);

			CTagStringPage *pPage4 = New CTagStringPage(this, CString(IDS_SECURITY), 4);

			pList->Append(pPage1);

			pList->Append(pPage2);

			pList->Append(pPage3);

			pList->Append(pPage4);
			}
		}

	return TRUE;
	}

// UI Update

void CTagString::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Value" ) {

		if( m_pValue ) {

			if( m_pValue->IsCommsRef() ) {

				if( !m_pValue->IsWritable() ) {

					MakeReadOnly(pHost);
					}

				UpdateExtent();

				CheckCircular(pHost);
				}
			else {
				m_Extent = 0;

				m_Encode = 0;

				pHost->UpdateUI("Extent");

				pHost->UpdateUI("Encode");

				MakeReadOnly(pHost);

				CheckCircular(pHost, m_pValue);
				}

			if( m_pValue->IsConst() ) {

				KillCoded(pHost, L"Sim", m_pSim);
				}
			}
		else {
			if( pItem == this ) {

				m_Access  = 0;

				pHost->UpdateUI(this, "Access");

				CheckCircular(pHost);
				}
			}

		CheckPersist(pHost);

		pHost->SendUpdate(updateProps);

		DoEnables(pHost);
		}

	if( Tag == "Extent" ) {

		if( m_pValue ) {

			UpdateExtent();
			}

		Recompile(pHost);

		DoEnables(pHost);
		}

	if( Tag == "Length" ) {

		if( m_pValue ) {

			UpdateExtent();
			}
		}

	if( Tag == "Encode" ) {

		if( m_pValue ) {

			UpdateExtent();
			}
		}

	if( Tag == "Access" ) {

		if( pItem == this ) {

			CheckPersist(pHost);

			Recompile(pHost);

			DoEnables(pHost);
			}
		}

	if( Tag == "Persist" ) {

		DoEnables(pHost);
		}

	if( Tag == "OnWrite" ) {

		CheckCircular(pHost, m_pOnWrite);

		Recompile(pHost);
		}

	CDataTag::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CTagString::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = flagSoftWrite | flagInherent | flagCommsTab;

		return TRUE;
		}

	if( Tag == "Sim" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = flagConstant;

		return TRUE;
		}

	if( Tag == "SubValue" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "OnWrite" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	return CDataTag::GetTypeData(Tag, Type);
	}

// Attributes

UINT CTagString::GetTreeImage(void) const
{
	return GetImage(IDI_RED_STRING);
	}

UINT CTagString::GetDataType(void) const
{
	return typeString;
	}

UINT CTagString::GetTypeFlags(void) const
{
	UINT Flags = flagTagRef;

	if( m_Extent > 0 ) {

		if( m_Extent > 1024 ) {

			Flags |= flagExtended;
			}

		Flags |= flagArray;
		}

	if( m_Access < 2 ) {

		Flags |= flagWritable;
		}

	if( m_pOnWrite ) {

		Flags |= flagWritable;
		}

	return Flags;
	}

// Operations

void CTagString::UpdateTypes(BOOL fComp)
{
	if( m_pValue && !m_pValue->IsBroken() ) {

		UpdateExtent();

		m_Circle = m_pValue->CheckCircular(this, FALSE);
		}
	}

// Reference Check

BOOL CTagString::RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag)
{
	if( CodeRefersToTag(Busy, pTags, m_pOnWrite, uTag) ) {

		return TRUE;
		}

	return CDataTag::RefersToTag(Busy, pTags, uTag);
	}

// Circular Check

void CTagString::UpdateCircular(void)
{
	m_Circle = CheckCircular(m_pValue  ) ||
		   CheckCircular(m_pOnWrite) ;
	}

// Evaluation

DWORD CTagString::GetProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return FindAsText();

		case tpLabel:
			return FindLabel();

		case tpForeColor:
			return FindFore();

		case tpBackColor:
			return FindBack();
		}

	return CDataTag::GetProp(ID, Type);
	}

// Persistance

void CTagString::Init(void)
{
	m_pColor  = New CDispColorFixed;

	m_ColType = 1;

	CDataTag::Init();
	}

// Download Support

BOOL CTagString::MakeInitData(CInitData &Init)
{
	Init.AddByte(4);

	CDataTag::MakeInitData(Init);

	Init.AddByte(BYTE(m_Encode));

	Init.AddWord(WORD(m_Length));

	Init.AddItem(itemSimple,  m_pSec);

	Init.AddItem(itemVirtual, m_pOnWrite);

	return TRUE;
	}

// Meta Data

void CTagString::AddMetaData(void)
{
	CDataTag::AddMetaData();

	Meta_AddInteger(Encode);
	Meta_AddInteger(Length);
	Meta_AddObject (Sec);
	Meta_AddVirtual(OnWrite);

	Meta_SetName((IDS_STRING_TAG));
	}

// Property Access

DWORD CTagString::FindAsText(void)
{
	if( m_pFormat ) {

		DWORD   Data = Execute();

		UINT    Type = GetDataType();

		CString Text = m_pFormat->Format(Data, Type, fmtStd);

		FreeData(Data, Type);

		return DWORD(wstrdup(Text));
		}

	return Execute();
	}

DWORD CTagString::FindLabel(void)
{
	// LATER -- Refactor back into base class?

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the label that
	// uses the tag property syntax to reference itself!!!

	if( m_pLabel ) {

		return DWORD(wstrdup(m_pLabel->GetText()));
		}

	return DWORD(wstrdup(m_Name));
	}

DWORD CTagString::FindFore(void)
{
	if( m_pColor ) {

		DWORD Data = Execute();

		UINT  Type = GetDataType();

		COLOR Fore = m_pColor->GetForeColor(Data, Type);

		FreeData(Data, Type);

		return Fore;
		}

	return GetRGB(31,31,31);
	}

DWORD CTagString::FindBack(void)
{
	if( m_pColor ) {

		DWORD Data = Execute();

		UINT  Type = GetDataType();

		COLOR Back = m_pColor->GetBackColor(Data, Type);

		FreeData(Data, Type);

		return Back;
		}

	return GetRGB(0,0,0);
	}

// Memory Sizing

UINT CTagString::GetAllocSize(void)
{
	if( m_Extent ) {

		return 2 * (m_Length + 1) * m_Extent;
		}

	return 2 * (m_Length + 1);
	}

UINT CTagString::GetCommsSize(void)
{
	if( m_Encode ) {

		if( m_pValue ) {

			UINT uBits = m_pValue->GetCommsBits();

			UINT uNeed = GetCharBits();

			UINT uPack = (uBits / uNeed);

			if( uPack ) {

				UINT uRegs = (m_Length + uPack - 1) / uPack;

				if( m_Extent ) {

					return uRegs * m_Extent;
					}

				return uRegs;
				}

			return 0;
			}
		}

	if( m_Extent ) {

		return m_Length * m_Extent;
		}

	return m_Length;
	}

// Implementation

void CTagString::DoEnables(IUIHost *pHost)
{

	// LATER -- Limit coding based on data type?

	if( m_pValue ) {

		if( m_pValue->IsCommsRef() ) {

			pHost->EnableUI(this, "Extent",  TRUE);
			pHost->EnableUI(this, "Length",  TRUE);
			pHost->EnableUI(this, "Encode",  TRUE);

			pHost->EnableUI(this, "Access",  m_pValue->IsWritable());

			pHost->EnableUI(this, "RdMode",  m_Extent && m_Access != 1);

			pHost->EnableUI(this, "Persist", m_Access == 1);
			}
		else {
			pHost->EnableUI(this, "Extent",  FALSE);
			pHost->EnableUI(this, "Access",  FALSE);
			pHost->EnableUI(this, "RdMode",  FALSE);
			pHost->EnableUI(this, "Length",  FALSE);
			pHost->EnableUI(this, "Encode",  FALSE);
			pHost->EnableUI(this, "Persist", FALSE);
			}

		pHost->EnableUI("Sim", !m_pValue->IsConst());
		}
	else {
		pHost->EnableUI(this, "Extent",  TRUE );
		pHost->EnableUI(this, "Access",  FALSE);
		pHost->EnableUI(this, "RdMode",  FALSE);
		pHost->EnableUI(this, "Persist", TRUE );
		pHost->EnableUI(this, "Encode",  FALSE);
		pHost->EnableUI(this, "Sim",     TRUE );

		pHost->EnableUI(this, "Length",  m_Persist == 1);
		}
	}

UINT CTagString::GetCharBits(void)
{
	switch( m_Encode ) {

		case 1: return 8;
		case 2: return 8;
		case 3: return 16;
		case 4: return 16;
		case 5: return 4;
		}

	return 16;
	}

// End of File
