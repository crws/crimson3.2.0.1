
#include "Intern.hpp"

#include "TagNumericPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispColor.hpp"
#include "DispFormat.hpp"
#include "SecDesc.hpp"
#include "TagNumeric.hpp"
#include "TagQuickPlot.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Page
//

// Runtime Class

AfxImplementRuntimeClass(CTagNumericPage, CUIStdPage);

// Constructor

CTagNumericPage::CTagNumericPage(CTagNumeric *pTag, CString Title, UINT uPage)
{
	m_pTag  = pTag;

	m_Title = Title;

	m_Class = AfxRuntimeClass(CTagNumeric);

	m_uPage = uPage;
	}

// Operations

BOOL CTagNumericPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	if( m_uPage == 2 || m_uPage == 10 ) {

		LoadLimits(pView);

		LoadFormat(pView);

		pView->NoRecycle();
		}

	if( m_uPage == 4 ) {

		LoadColor(pView);

		pView->NoRecycle();
		}

	if( m_uPage == 7 ) {

		LoadSecurity(pView);

		pView->NoRecycle();
		}

	if( m_uPage == 9 ) {

		LoadQuickPlot(pView);

		pView->NoRecycle();
		}

	return TRUE;
	}

// Implementation

BOOL CTagNumericPage::LoadLimits(IUICreate *pView)
{
	CDispFormat *pFormat = m_pTag->m_pFormat;

	if( !pFormat || pFormat->NeedsLimits() ) {

		UINT     uGroup = m_pTag->GetDatabase()->GetSoftwareGroup();

		BOOL     fCloud = (uGroup == SW_GROUP_2 || uGroup >= SW_GROUP_3B);

		UINT     uPage  = fCloud ? 11 : 3;

		CUIPage *pPage  = New CUIStdPage(AfxPointerClass(m_pTag), uPage);

		pPage->LoadIntoView(pView, m_pTag);

		delete pPage;

		return TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagNumericPage::LoadFormat(IUICreate *pView)
{
	CDispFormat *pFormat = m_pTag->m_pFormat;

	if( pFormat ) {

		CUIPageList List;

		pFormat->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pFormat);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagNumericPage::LoadColor(IUICreate *pView)
{
	CDispColor *pColor = m_pTag->m_pColor;

	if( pColor ) {

		CUIPageList List;

		pColor->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pColor);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagNumericPage::LoadSecurity(IUICreate *pView)
{
	CSecDesc *pSec = m_pTag->m_pSec;

	if( pSec ) {

		CUIPageList List;

		pSec->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pSec);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagNumericPage::LoadQuickPlot(IUICreate *pView)
{
	CTagQuickPlot *pQuick = m_pTag->m_pQuickPlot;

	if( pQuick ) {

		CUIPageList List;

		pQuick->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pQuick);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
