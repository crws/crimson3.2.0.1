
#include "Intern.hpp"

#include "DispFormatIPAddr.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IP Address Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatIPAddr, CDispFormat);

// Constructor

CDispFormatIPAddr::CDispFormatIPAddr(void)
{
	m_uType = 4;
	}

// Formatting

CString CDispFormatIPAddr::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeInteger ) {

		struct CIPAddr {

			BYTE m_b1;
			BYTE m_b2;
			BYTE m_b3;
			BYTE m_b4;
			};

		CIPAddr Addr;

		((DWORD &) Addr) = Data;

		CString Text;

		Text.Printf(L"%u.%u.%u.%u", Addr.m_b1, Addr.m_b2, Addr.m_b3, Addr.m_b4);

		if( Flags & fmtPad ) {

			MakeDigitsFixed(Text);
			}

		return Text;
		}

	if( Type == typeReal ) {

		C3REAL rData = I2R(Data);

		return Format(DWORD(rData), typeInteger, Flags);
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Limit Access

DWORD CDispFormatIPAddr::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return 0;
		}

	if( Type == typeReal ) {

		return R2I(0);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatIPAddr::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return 0xFFFFFFFF;
		}

	if( Type == typeReal ) {

		return R2I(C3INT(0xFFFFFFFF));
		}

	return CDispFormat::GetMax(Type);
	}

// Download Support

BOOL CDispFormatIPAddr::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatIPAddr::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_SetName((IDS_IP_ADDRESS_FORMAT));
	}

// End of File
