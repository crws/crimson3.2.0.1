
#include "Intern.hpp"

#include "CascadedRackItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OptionCardRackList.hpp"
#include "ExpansionItem.hpp"
#include "OptionCardItem.hpp"
#include "OptionCardList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cascaded Rack Item
//

// Dynamic Class

AfxImplementDynamicClass(CCascadedRackItem, COptionCardRackItem);

// Constructor

CCascadedRackItem::CCascadedRackItem(void)
{
	m_fRemap = FALSE;
	}

// Attributes

BOOL CCascadedRackItem::HasRoom(void) const
{
	return m_pRacks->GetItemCount() < 4;
	}

// Operations

void CCascadedRackItem::AddRack(void)
{
	CExpansionItem      *pExp  = (CExpansionItem *) GetParent();

	COptionCardRackItem *pRack = New COptionCardRackItem;

	AppendItem(pRack);

	pRack->m_Number = pExp->AllocRackNumber();

	for( UINT s = 0; s < 3; s ++ ) {

		COptionCardItem *pOption = New COptionCardItem;

		pRack->AppendItem(pOption);

		pOption->m_Number = pExp->AllocSlotNumber();
		}

	RemapSlots(FALSE);
	}

void CCascadedRackItem::DelRack(COptionCardRackItem *pRack)
{
	DeleteItem(pRack);

	RemapSlots(FALSE);
	}

void CCascadedRackItem::RemapSlots(BOOL fForce)
{
	if( m_fRemap || fForce ) {

		UINT r = 0;

		for( INDEX iRack = m_pRacks->GetHead(); !m_pRacks->Failed(iRack); m_pRacks->GetNext(iRack) ) {

			COptionCardRackItem *pRack  = m_pRacks->GetItem(iRack);

			COptionCardList     *pSlots = pRack->m_pSlots;

			pRack->m_Slot = GetRackPath(r);

			UINT s = 0;

			for( INDEX iSlot = pSlots->GetHead(); !pSlots->Failed(iSlot); pSlots->GetNext(iSlot) ) {

				pSlots->GetItem(iSlot)->m_Slot = GetSlotPath(r, s++);
				}

			r++;
			}

		m_fRemap = FALSE;
		}
	}

UINT CCascadedRackItem::GetPowerBudget(void)
{
	UINT uBudget = 4 * 43 * 3;

	return m_pDbase->HasFlag(L"USBCascade") ? uBudget - 43 * 1 : uBudget;
	}

UINT CCascadedRackItem::GetPowerTotal(void)
{
	return m_pRacks->GetPowerTotal();
	}

UINT CCascadedRackItem::GetPowerUsage(void)
{
	return (GetPowerTotal() * 100) / GetPowerBudget();
	}

BOOL CCascadedRackItem::CheckPower(void)
{
	return GetPowerUsage() <= 100;
	}

// Dirty Control

void CCascadedRackItem::OnSetDirty(void)
{
	m_fRemap = TRUE;
	}

// Path Config

DWORD CCascadedRackItem::GetRackPath(UINT uRack) const
{
	return NOTHING;
	}

DWORD CCascadedRackItem::GetSlotPath(UINT uRack, UINT uSlot) const
{
	return NOTHING;
	}

// Rack Types

void CCascadedRackItem::GetPortsRack(UINT &p1, UINT &p2, UINT &p3, UINT &p4) const
{
	p1 = 4;	  // Slot 1
	p2 = 3;   // Slot 2
	p3 = 2;   // Slot 3
	p4 = 1;   // Cascade
	}

// End of File
