
#include "Intern.hpp"

#include "UILValue.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- LValue
//

// Dynamic Class

AfxImplementDynamicClass(CUILValue, CUIExpression);

// Constructor

CUILValue::CUILValue(void)
{
	m_fComp = FALSE;
	}

// End of File
