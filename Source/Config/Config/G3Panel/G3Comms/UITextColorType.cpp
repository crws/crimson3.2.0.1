
#include "Intern.hpp"

#include "UITextColorType.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Color Type
//

// Dynamic Class

AfxImplementDynamicClass(CUITextColorType, CUITextEnumPick);

// Constructor

CUITextColorType::CUITextColorType(void)
{
	}

// Overridables

void CUITextColorType::OnBind(void)
{
	CUITextElement::OnBind();

	if( m_UIData.m_Format == "n" ) {

		m_uMask = 0x1F;
		}

	if( m_UIData.m_Format == "f" ) {

		m_uMask = 0x0F;
		}

	if( m_UIData.m_Format == "s" ) {

		m_uMask = 0x07;
		}

	if( m_UIData.m_Format == "c" ) {

		m_uMask = 0x04;
		}

	AddData();
	}

// Implementation

void CUITextColorType::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

void CUITextColorType::AddData(void)
{
	AddData(0, CString(IDS_GENERAL));    // 0x0001
	AddData(4, CString(IDS_LINKED));     // 0x0002
	AddData(1, CString(IDS_FIXED));      // 0x0004
	AddData(2, CString(IDS_TWOSTATE));   // 0x0008
	AddData(3, CString(IDS_MULTISTATE)); // 0x0010
	}

// End of File
