
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColorMultiList_HPP

#define INCLUDE_DispColorMultiList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispColorMultiEntry;

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color List
//

class DLLNOT CDispColorMultiList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispColorMultiList(void);

		// Item Access
		CDispColorMultiEntry * GetItem(INDEX Index) const;
		CDispColorMultiEntry * GetItem(UINT  uPos ) const;
	};

// End of File

#endif
