
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CodedHost_HPP

#define INCLUDE_CodedHost_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCodedItem;
class CCodedText;
class CDataServer;
class CNameServer;

//////////////////////////////////////////////////////////////////////////
//
// Coded Host
//

class DLLAPI CCodedHost : public CUIItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCodedHost(void);

		// Attributes
		BOOL HasBrokenCode(void) const;

		// Initial Values
		virtual void SetInitValues(void);

		// Name Hint
		virtual BOOL GetNameHint(CString Tag, CString &Name);

		// Type Access
		virtual BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		virtual void UpdateTypes(BOOL fComp);

		// Server Access
		virtual INameServer * GetNameServer(CNameServer *pName);
		virtual IDataServer * GetDataServer(CDataServer *pData);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Property Upgrade
		void Upgrade(CTreeFile &Tree, CMetaData const *pMeta);

	protected:
		// Implementation
		BOOL LimitEnum(IUIHost *pHost, CString Tag, UINT &uData, UINT uMask);
		BOOL UpdateType(IUIHost *pHost, CString Tag, CCodedItem *pItem, BOOL fComp);
		BOOL SetInitial(CString Tag, CCodedItem * &pItem, UINT    uVal);
		BOOL SetInitial(CString Tag, CCodedItem * &pItem, CString Code);
		BOOL SetInitial(CString Tag, CCodedText * &pText, CString Code);
		BOOL InitCoded(IUIHost *pHost, CString Tag, CCodedItem * &pItem, UINT    uVal);
		BOOL InitCoded(IUIHost *pHost, CString Tag, CCodedItem * &pItem, CString Code);
		BOOL KillCoded(IUIHost *pHost, CString Tag, CCodedItem * &pItem);
		BOOL InitCoded(IUIHost *pHost, CString Tag, CCodedText * &pText, CString Code);
		BOOL KillCoded(IUIHost *pHost, CString Tag, CCodedText * &pText);
		BOOL CheckEnable(CCodedItem *pItem, PCTXT pExpr, UINT uValue, BOOL fDefault);
		BOOL FreeData(DWORD Data, UINT Type);
		BOOL StepCoded(CCodedItem * &pValue);
		BOOL StepCoded(CCodedText * &pValue);

		// Null Data
		static DWORD GetNull(UINT Type);
	};

// End of File

#endif
