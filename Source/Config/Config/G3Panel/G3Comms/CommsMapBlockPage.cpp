
#include "Intern.hpp"

#include "CommsMapBlockPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsMapBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block Page
//

// Runtime Class

AfxImplementRuntimeClass(CCommsMapBlockPage, CUIStdPage);

// Constructor

CCommsMapBlockPage::CCommsMapBlockPage(CCommsMapBlock *pBlock)
{
	m_Class  = AfxRuntimeClass(CCommsMapBlock);

	m_pBlock = pBlock;
	}

// Operations

BOOL CCommsMapBlockPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	LoadBasePage(pView);

	LoadButtons(pView);

	return TRUE;
	}

// Implementation

BOOL CCommsMapBlockPage::LoadBasePage(IUICreate *pView)
{
	CUIPage *pPage = New CUIStdPage(m_Class);

	pPage->LoadIntoView(pView, m_pBlock);

	delete pPage;

	return TRUE;
	}

BOOL CCommsMapBlockPage::LoadButtons(IUICreate *pView)
{
	pView->StartGroup( CString(IDS_BLOCK_COMMANDS),
			   1
			   );

	pView->AddButton(  CString(IDS_COMMS_BLOCK_DELETE),
			   CString(IDS_COMMS_MAPBL_DELETE),
			   IDM_COMMS_DEL_BLOCK
			   );

	pView->AddButton(  CString(IDS_IMPORT_MAPPINGS),
			   CString(IDS_IMPORT_MAPPING),
			   IDM_COMMS_MAP_IMPORT
			   );

	pView->AddButton(  CString(IDS_EXPORT_MAPPINGS),
			   CString(IDS_EXPORT_MAPPING),
			   IDM_COMMS_MAP_EXPORT
			   );

	pView->EndGroup(FALSE);

	return TRUE;
	}

// End of File
