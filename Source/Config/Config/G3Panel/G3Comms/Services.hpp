
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_Services_HPP

#define INCLUDE_Services_HPP

//////////////////////////////////////////////////////////////////////////
//
// Services Configuration
//

class DLLNOT CServices : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CServices(void);

		// Attributes
		UINT GetTreeImage(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Conversion
		void PostConvert(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CItemIndexList * m_pList;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Service Creation
		void AddServices(void);
	};

// End of File

#endif
