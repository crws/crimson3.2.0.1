
#include "Intern.hpp"

#include "ProgramParameter.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program Parameter
//

// Dynamic Class

AfxImplementDynamicClass(CProgramParameter, CUIItem);

// Constructor

CProgramParameter::CProgramParameter(void)
{
	m_Type = typeVoid;
	}

// UI Update

void CProgramParameter::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == L"Type" ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Meta Data Creation

void CProgramParameter::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Type);
	Meta_AddString (Name);

	Meta_SetName((IDS_PROGRAM_PARAMETER));
	}

// Implementation

void CProgramParameter::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"Name", m_Type != typeVoid);
	}

// End of File
