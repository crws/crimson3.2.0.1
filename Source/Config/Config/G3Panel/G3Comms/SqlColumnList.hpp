
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlColumnList_HPP

#define INCLUDE_SqlColumnList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlColumn;

//////////////////////////////////////////////////////////////////////////
//
// SQL Column List
//

class CSqlColumnList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlColumnList(void);

		// Item Access
		CSqlColumn * GetItem(INDEX Index) const;
		CSqlColumn * GetItem(UINT uPos)   const;
		CSqlColumn * GetColumnDef(UINT uCol);
		UINT         GetColumnPos(CString const &Name);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

// End of File

#endif
