
#include "Intern.hpp"

#include "CommsTreeWnd_CCmdDelete.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Delete Command
//

// Runtime Class

AfxImplementRuntimeClass(CCommsTreeWnd::CCmdDelete, CStdCmd);

// Constructor

CCommsTreeWnd::CCmdDelete::CCmdDelete(CItem *pRoot, CString List, CItem *pNext, CMetaItem *pItem)
{
	m_Menu  = CFormat(IDS_DELETE_3, pItem->GetName());

	m_Item  = pItem->GetFixedPath();

	m_Root  = GetFixedPath(pRoot);

	m_List  = List;

	m_Next  = GetFixedPath(pNext);

	m_hData = pItem->TakeSnapshot();
	}

// Destructor

CCommsTreeWnd::CCmdDelete::~CCmdDelete(void)
{
	GlobalFree(m_hData);
	}

// End of File
