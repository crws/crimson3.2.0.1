
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_Tag_HPP

#define INCLUDE_Tag_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CDispColor;
class CDispFormat;
class CTagList;
class CTagManager;

//////////////////////////////////////////////////////////////////////////
//
// Tag Base Class
//

class DLLAPI CTag : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTag(void);

		// Default Prop Values
		static DWORD GetDefProp(WORD ID, UINT Type);

		// Item Location
		CCommsSystem * FindSystem(void) const;
		CTagManager  * FindManager(void) const;

		// Attributes
		BOOL    IsBroken(void) const;
		BOOL    IsCircular(void) const;
		CString GetExportClass(void) const;
		CString FormatData(DWORD Data);
		CString FormatData(void);

		// Limit Access
		DWORD GetMinValue(UINT Type);
		DWORD GetMaxValue(UINT Type);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		virtual UINT GetTreeImage(void) const;
		virtual UINT GetDataType(void) const;
		virtual UINT GetTypeFlags(void) const;

		// Operations
		virtual void MakeLite(void);

		// Reference Check
		virtual BOOL RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag);

		// Circular Check
		virtual void UpdateCircular(void);

		// Evaluation
		virtual DWORD Execute(void);
		virtual DWORD GetProp(WORD ID, UINT Type);

		// Searching
		virtual void FindAlarms  (CStringArray &List);
		virtual void FindTriggers(CStringArray &List);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Save(CTreeFile &File);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CString       m_Name;
		CString       m_Desc;
		CString       m_Class;
		UINT	      m_Circle;
		CCodedItem  * m_pValue;
		CCodedItem  * m_pSim;
		CCodedText  * m_pLabel;
		CCodedText  * m_pAlias;
		CDispFormat * m_pFormat;
		CDispColor  * m_pColor;
		UINT	      m_Private;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT GetImage(UINT uBase) const;
		void Recompile(IUIHost *pHost);
		BOOL CodeRefersToTag(CCodedTree &Busy, CTagList *pTags, CCodedItem *pCoded, UINT uTag);
		BOOL CheckCircular(IUIHost *pHost, CCodedItem *pCoded);
		BOOL CheckCircular(CCodedItem *pCoded);
		BOOL CheckCircular(IUIHost *pHost);
		BOOL SetFormatClass(CDispFormat * &pFormat, CLASS Class);
		BOOL SetColorClass(CDispColor * &pColor, CLASS Class);
	};

// End of File

#endif
