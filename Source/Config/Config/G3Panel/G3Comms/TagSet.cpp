
#include "Intern.hpp"

#include "TagSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "DataTag.hpp"
#include "NameServer.hpp"
#include "Tag.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Set
//

// Dynamic Class

AfxImplementDynamicClass(CTagSet, CItem);

// Constructor

CTagSet::CTagSet(void)
{
	m_pNext  = NULL;

	m_pPrev  = NULL;

	m_fWatch = FALSE;
	}

// Attributes

UINT CTagSet::GetCount(void) const
{
	return m_Data.GetCount();
	}

CString CTagSet::Format(void) const
{
	CString Text;

	CString Name;

	for( UINT n = 0; n < m_Data.GetCount(); n ++ ) {

		DWORD    Data = m_Data[n];

		CDataRef &Ref = (CDataRef &) Data;

		if( !Data || !Expand(Name, Ref) ) {

			Name = m_Name[n];
			}

		if( !Text.IsEmpty() ) {

			Text += ',';
			}

		Text += Name;
		}

	return Text;
	}

BOOL CTagSet::IsBroken(void) const
{
	for( UINT n = 0; n < m_Data.GetCount(); n ++ ) {

		if( !m_Data[n] ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagSet::HasCode(CString Code) const
{
	CString Name;

	for( UINT n = 0; n < m_Data.GetCount(); n ++ ) {

		DWORD    Data = m_Data[n];

		CDataRef &Ref = (CDataRef &) Data;

		if( !Data || !Expand(Name, Ref) ) {

			Name = m_Name[n];
			}

		if( Name == Code ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagSet::HasRef(CDataRef const &Ref) const
{
	DWORD Data = (DWORD const &) Ref;

	for( UINT n = 0; n < m_Data.GetCount(); n ++ ) {

		if( m_Data[n] == Data ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagSet::HasTag(UINT uTag) const
{
	DWORD    Data = 0;

	CDataRef &Ref = (CDataRef &) Data;

	Ref.t.m_IsTag = 1;

	Ref.t.m_Index = uTag;

	return !m_Data.Failed(m_Data.Find(Data));
	}

CString CTagSet::GetName(UINT n) const
{
	CString  Name = L"";

	DWORD    Data = m_Data[n];

	CDataRef &Ref = (CDataRef &) Data;

	if( !Data || !Expand(Name, Ref) ) {

		Name = m_Name[n];
		}

	if( Name[0] == '[' ) {

		UINT n = Name.GetLength();

		Name   = Name.Mid(1, n - 2);
		}

	return Name;
	}

CString CTagSet::GetSimulatedData(UINT n) const
{
	DWORD    Data = m_Data[n];

	CDataRef &Ref = (CDataRef &) Data;

	if( Data ) {

		if( Ref.t.m_IsTag ) {

			// RV3 -- Better handling of arrays.

			UINT  uTag = Ref.t.m_Index;

			CTag *pTag = m_pTags->GetItem(uTag);

			if( pTag ) {

				return pTag->FormatData();
				}
			}
		}

	return L"n/a";
	}

CString CTagSet::GetFormattedData(UINT n, DWORD d) const
{
	DWORD    Data = m_Data[n];

	CDataRef &Ref = (CDataRef &) Data;

	if( Data ) {

		if( Ref.t.m_IsTag ) {

			UINT  uTag = Ref.t.m_Index;

			CTag *pTag = m_pTags->GetItem(uTag);

			if( pTag ) {

				return pTag->FormatData(d);
				}
			}

		return CPrintf(L"%d", d);
		}

	return L"n/a";
	}

DWORD CTagSet::Execute(UINT n) const
{
	DWORD    Data = m_Data[n];

	CDataRef &Ref = (CDataRef &) Data;

	if( Data ) {

		if( Ref.t.m_IsTag ) {

			UINT  uTag = Ref.t.m_Index;

			CTag *pTag = m_pTags->GetItem(uTag);

			if( pTag ) {

				return pTag->Execute();
				}
			}
		}

	return 0;
	}

CString CTagSet::GetFindInfo(void) const
{
	CCodedHost *pHost = (CCodedHost *) GetParent();

	if( pHost->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

		CString Name = pHost->GetName();

		CString Text;

		CString Field;

		CMetaList const *pList = pHost->FindMetaList();

		for( UINT n = 0; n < pList->GetCount(); n++ ) {

			CMetaData const *pMeta = pList->FindData(n);

			if( pMeta->GetType() == metaObject ) {

				if( pMeta->GetObject(pHost) == (CItem *) this ) {

					Field = L':' + pMeta->GetTag();

					break;
					}
				}
			}

		Text += pHost->GetHumanPath();

		Text += L" - ";

		Text += pHost->GetSubItemLabel(this);

		Text += '\n';

		Text += pHost->GetFixedPath();

		Text += Field;

		return Text;
		}

	return L"";
	}

// Operations

void CTagSet::MakeWatchList(void)
{
	m_fWatch = TRUE;
	}

void CTagSet::Empty(void)
{
	m_Data.Empty();

	m_Name.Empty();
	}

void CTagSet::Delete(UINT n)
{
	m_Name.Remove(n);

	m_Data.Remove(n);
	}

BOOL CTagSet::AddRef(CDataRef const &Ref)
{
	DWORD   Data = (DWORD const &) Ref;

	CString Code = L"";

	if( Expand(Code, Ref) ) {

		if( m_Data.Failed(m_Data.Find(Data)) ) {

			m_Name.Append(Code);

			m_Data.Append(Data);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagSet::AddCode(CString Code)
{
	DWORD    Data = 0;

	CDataRef &Ref = (CDataRef &) Data;

	if( Code.StartsWith(L"WAS ") ) {

		Code = Code.Mid(4);
		}

	if( Resolve(Ref, Code) ) {

		if( m_Data.Failed(m_Data.Find(Data)) ) {

			m_Name.Append(Code);

			m_Data.Append(Data);

			return TRUE;
			}
		}
	else {
		m_Name.Append(L"WAS " + Code);

		m_Data.Append(0);
		}

	return FALSE;
	}

UINT CTagSet::Parse(CString Text)
{
	Empty();

	CStringArray List;

	Text.Tokenize(List, ',');

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		AddCode(List[n]);
		}

	return saveChange;
	}

// Validation

void CTagSet::Validate(BOOL fExpand)
{
	if( fExpand ) {

		for( UINT n = 0; n < m_Data.GetCount(); n++ ) {

			DWORD    Data = m_Data[n];

			CDataRef &Ref = (CDataRef &) Data;

			CString  Name = L"";

			if( Expand(Name, Ref) ) {

				m_Name.SetAt(n, Name);

				SetDirty();
				}
			}
		}

	if( TRUE ) {

		for( UINT n = 0; n < m_Data.GetCount(); n++ ) {

			CString  Name = m_Name[n];

			DWORD    Data = 0;

			CDataRef &Ref = (CDataRef &) Data;

			if( Name.StartsWith(L"WAS ") ) {

				Name = Name.Mid(4);
				}

			if( Resolve(Ref, Name) ) {

				m_Name.SetAt(n, Name);

				m_Data.SetAt(n, Data);

				continue;
				}

			if( m_fWatch ) {

				m_Name.Remove(n, 1);

				m_Data.Remove(n, 1);

				n--;
				}
			else {
				m_Name.SetAt(n, L"WAS " + Name);

				m_Data.SetAt(n, 0);

				m_pDbase->SetRecomp(TRUE);
				}
			}
		}
	}

void CTagSet::TagCheck(UINT uTag)
{
	// REV3 -- Better handling of arrays.

	DWORD    Data = 0;

	CDataRef &Ref = (CDataRef &) Data;

	Ref.t.m_IsTag = 1;

	Ref.t.m_Index = uTag;

	UINT uIndex   = m_Data.Find(Data);

	if( !m_Data.Failed(uIndex) ) {

		CTag *pTag = m_pTags->GetItem(uTag);

		if( !pTag ) {

			if( m_fWatch ) {

				m_Name.Remove(uIndex);

				m_Data.Remove(uIndex);
				}
			else {
				CString Name = m_Name[uIndex];

				if( !Name.StartsWith(L"WAS ") ) {

					m_Name.SetAt(uIndex, L"WAS " + Name);

					m_Data.SetAt(uIndex, 0);
					}

				m_pDbase->SetRecomp(TRUE);
				}

			return;
			}

		m_Name.SetAt(uIndex, pTag->GetName());
		}
	}

// Persistance

void CTagSet::Init(void)
{
	CItem::Init();

	FindSystem();

	ListAppend();
	}

void CTagSet::Load(CTreeFile &Tree)
{
	CItem::Load(Tree);

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == L"Tag" ) {

			m_Name.Append(L"");

			m_Data.Append(Tree.GetValueAsInteger());

			continue;
			}

		if( Name == L"Name" ) {

			m_Name.Append(Tree.GetValueAsString());

			continue;
			}

		if( Name == L"Data" ) {

			m_Data.Append(Tree.GetValueAsInteger());

			continue;
			}
		}

	FindSystem();
	}

void CTagSet::Save(CTreeFile &Tree)
{
	CItem::Save(Tree);

	UINT uCount = m_Data.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		Tree.PutValue(L"Name", m_Name[n]);

		Tree.PutValue(L"Data", m_Data[n]);
		}
	}

void CTagSet::PostLoad(void)
{
	CItem::PostLoad();

	UINT uCount = m_Data.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD    Data = m_Data[n];

		CDataRef &Ref = (CDataRef &) Data;

		if( !Ref.t.m_IsTag ) {

			if( !Ref.b.m_Block ) {

				Ref.t.m_IsTag = 1;

				m_Data.SetAt(n, Data);
				}
			}

		if( m_Name[n].IsEmpty() ) {

			AfxAssert(Ref.t.m_IsTag);

			UINT  uTag = Ref.t.m_Index;

			CTag *pTag = m_pTags->GetItem(uTag);

			m_Name.SetAt(n, pTag->GetName());
			}
		}

	ListAppend();
	}

void CTagSet::PostPaste(void)
{
	CItem::PostPaste();

	FindSystem();

	Validate(FALSE);
	}

void CTagSet::Kill(void)
{
	ListRemove();

	CItem::Kill();
	}

// Download Support

BOOL CTagSet::MakeInitData(CInitData &Init)
{
	UINT uCount = m_Data.GetCount();

	Init.AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n++ ) {

		if( m_Data[n] ) {

			Init.AddLong(DWORD(m_Data[n]));

			continue;
			}

		if( TRUE ) {

			// TODO -- Is this correct?

			Init.AddLong(DWORD(0xFFFF));
			}
		else {
			// TODO -- Is this correct?

			Init.AddWord(WORD(0xFFFF));
			}
		}

	return TRUE;
	}

// Implementation

void CTagSet::FindSystem(void)
{
	m_pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	m_pTags   = m_pSystem->m_pTags->m_pTags;
	}

BOOL CTagSet::Resolve(CDataRef &Ref, CString Name) const
{
	if( Name[0] == '[' ) {

		INameServer *pName = m_pSystem->GetNameServer();

		CString      Code  = Name.Mid(1, Name.GetLength() - 2);

		CError       Error = CError(FALSE);

		DWORD        ID    = 0;

		CTypeDef     Type  = { 0, 0 };

		if( pName->FindDirect(&Error, Code, ID, Type) ) {

			Ref = (CDataRef &) ID;

			return TRUE;
			}
		}
	else {
		if( !wstrncmp(Name, L"__T", 3) ) {

			UINT uTag = wcstoul(PCTXT(Name) + 3, NULL, 16);

			Ref.t.m_IsTag = 1;

			Ref.t.m_Index = uTag;

			return TRUE;
			}

		UINT uPos   = Name.Find('[');

		UINT uTag   = 0;

		BOOL fArray = FALSE;

		if( uPos < NOTHING ) {

			Ref.x.m_Array = watoi(Name.Mid(uPos+1));

			fArray        = TRUE;

			Name          = Name.Left(uPos);
			}

		if( (uTag = m_pTags->FindNamePos(Name)) < NOTHING ) {

			CTag *pTag = m_pTags->GetItem(uTag);

			if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

				CDataTag *pData = (CDataTag *) pTag;

				if( !pData->m_Extent == fArray ) {

					return FALSE;
					}
				}

			Ref.t.m_IsTag = 1;

			Ref.t.m_Index = uTag;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagSet::Expand(CString &Name, CDataRef const &Ref) const
{
	if( !Ref.t.m_IsTag ) {

		if( Ref.b.m_Block ) {

			INameServer *pName = m_pSystem->GetNameServer();

			DWORD        ID    = (DWORD &) Ref;

			if( pName->NameDirect(ID, Name) ) {

				Name = L'[' + Name;

				Name = Name + L']';

				return TRUE;
				}
			}
		}
	else {
		UINT  uTag = Ref.t.m_Index;

		CTag *pTag = m_pTags->GetItem(uTag);

		if( pTag ) {

			Name = pTag->GetName();

			if( Ref.x.m_Array ) {

				Name += CPrintf(L"[%u]", Ref.x.m_Array);
				}
			else {
				if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					CDataTag *pData = (CDataTag *) pTag;

					if( pData->m_Extent ) {

						Name += L"[0]";
						}
					}
				}

			return TRUE;
			}
		}

	return FALSE;
	}

void CTagSet::ListAppend(void)
{
	AfxListAppend( m_pSystem->m_pHeadTagSet,
		       m_pSystem->m_pTailTagSet,
		       this,
		       m_pNext,
		       m_pPrev
		       );
	}

void CTagSet::ListRemove(void)
{
	AfxListRemove( m_pSystem->m_pHeadTagSet,
		       m_pSystem->m_pTailTagSet,
		       this,
		       m_pNext,
		       m_pPrev
		       );
	}

// End of File
