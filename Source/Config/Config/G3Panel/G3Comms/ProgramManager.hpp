
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramManager_HPP

#define INCLUDE_ProgramManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramCatWnd;
class CProgramList;
class CProgramMonitor;

//////////////////////////////////////////////////////////////////////////
//
// Program Manager
//

class DLLAPI CProgramManager : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramManager(void);

		// Destructor
		~CProgramManager(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Naming
		CString GetHumanName(void) const;

		// Operations
		void SetCatWnd(CProgramCatWnd *pCatWnd);
		void Create(CString Name, CString Prot, CString Code);
		void MonitoredFilesChanged(void);

		// Persistance
		void Init(void);
		void PostLoad(void);
		void PostSave(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT           m_Handle;
		CProgramList * m_pPrograms;
		UINT	       m_AutoCast;
		UINT	       m_PrecFix;
		UINT	       m_OptLevel;
		UINT	       m_DebugEnable;
		UINT	       m_DebugPrograms;
		UINT	       m_DebugActive;
		UINT	       m_DebugPassive;
		UINT	       m_DebugFlags;
		UINT	       m_StoreInFiles;

	protected:
		// Data Members
		CProgramCatWnd  * m_pCatWnd;
		CProgramMonitor * m_pMonitor;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void CheckMonitor(void);
		void KillMonitor(void);
	};

// End of File

#endif
