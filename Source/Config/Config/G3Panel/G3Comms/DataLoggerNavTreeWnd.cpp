
#include "Intern.hpp"

#include "DataLoggerNavTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DataLog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Logger Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CDataLoggerNavTreeWnd, CNavTreeWnd);

// Constructor

CDataLoggerNavTreeWnd::CDataLoggerNavTreeWnd(void) : CNavTreeWnd( L"Logs",
						     NULL,
						     AfxRuntimeClass(CDataLog)
						     )
{
	m_Empty = CString(IDS_CLICK_ON_NEW);
	}

// Message Map

AfxMessageMap(CDataLoggerNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchGetInfoType(IDM_ITEM,  OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM,  OnItemControl)
	AfxDispatchCommandType(IDM_ITEM,  OnItemCommand)

	AfxMessageEnd(CDataLoggerNavTreeWnd)
	};

// Message Handlers

void CDataLoggerNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"DataLoggerNavTreeTool"));
		}
	}

// Command Handlers

BOOL CDataLoggerNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Info.m_Image   = 0x40000007;

			Info.m_ToolTip = CString(IDS_NEW_LOG);

			Info.m_Prompt  = CString(IDS_ADD_NEW_LOG_TO);

			return TRUE;
		}

	return FALSE;
	}

BOOL CDataLoggerNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Src.EnableItem(!IsReadOnly());

			return TRUE;
		}

	return FALSE;
	}

BOOL CDataLoggerNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			OnItemNew();

			return TRUE;
		}

	return FALSE;
	}

void CDataLoggerNavTreeWnd::OnItemNew(void)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(L"Log%1!u!", n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Class);

			m_System.ExecCmd(pCmd);

			break;
			}
		}
	}

// Tree Loading

void CDataLoggerNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TagsTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CDataLoggerNavTreeWnd::GetRootImage(void)
{
	return IDI_DATA_LOGGER;
	}

UINT CDataLoggerNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return IDI_DATA_LOG;
	}

BOOL CDataLoggerNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"DataLoggerNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"DataLoggerNavTreeMissCtxMenu";

	return FALSE;
	}

void CDataLoggerNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CDataLog)) ) {

		CDataLog *pLog = (CDataLog *) pItem;

		if( pLog->m_Path.IsEmpty() ) {

			if( pLog->m_Name.GetLength() > 8 ) {

				Information(CString(IDS_NAMES_LONGER_THAN), L"LongLogName");
				}
			}
		}

	CNavTreeWnd::OnItemRenamed(pItem);
	}

// End of File
