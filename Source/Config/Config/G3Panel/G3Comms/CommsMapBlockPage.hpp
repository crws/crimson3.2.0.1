
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsMapBlockPage_HPP

#define INCLUDE_CommsMapBlockPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsMapBlock;

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block Page
//

class CCommsMapBlockPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCommsMapBlockPage(CCommsMapBlock *pBlock);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CCommsMapBlock * m_pBlock;

		// Implementation
		BOOL LoadBasePage(IUICreate *pView);
		BOOL LoadButtons(IUICreate *pView);
	};

// End of File

#endif
