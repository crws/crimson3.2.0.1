
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagNumericPage_HPP

#define INCLUDE_TagNumericPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagNumeric;

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Page
//

class CTagNumericPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTagNumericPage(CTagNumeric *pTag, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CTagNumeric * m_pTag;

		// Implementation
		BOOL LoadLimits(IUICreate *pView);
		BOOL LoadFormat(IUICreate *pView);
		BOOL LoadColor (IUICreate *pView);
		BOOL LoadSecurity(IUICreate *pView);
		BOOL LoadQuickPlot(IUICreate *pView);
	};

// End of File

#endif
