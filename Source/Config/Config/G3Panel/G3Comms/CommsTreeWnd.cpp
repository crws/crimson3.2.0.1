
#include "Intern.hpp"

#include "CommsTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CascadedItem.hpp"
#include "CascadedRackItem.hpp"
#include "CommsDevice.hpp"
#include "CommsDeviceList.hpp"
#include "CommsManager.hpp"
#include "CommsMapping.hpp"
#include "CommsMappingList.hpp"
#include "CommsMapBlock.hpp"
#include "CommsMapBlockList.hpp"
#include "CommsMapData.hpp"
#include "CommsMapReg.hpp"
#include "CommsMapRegList.hpp"
#include "CommsPort.hpp"
#include "CommsPortGroup.hpp"
#include "CommsPortList.hpp"
#include "CommsPortNetwork.hpp"
#include "CommsPortVirtual.hpp"
#include "CommsSystem.hpp"
#include "CommsTreeWnd_CCmdCreate.hpp"
#include "CommsTreeWnd_CCmdDelete.hpp"
#include "CommsTreeWnd_CCmdPaste.hpp"
#include "CommsTreeWnd_CCmdRename.hpp"
#include "Connectors.hpp"
#include "EthernetItem.hpp"
#include "ExpansionItem.hpp"
#include "OptionCardItem.hpp"
#include "OptionCardList.hpp"
#include "OptionCardRackList.hpp"
#include "Services.hpp"
#include "ServiceItem.hpp"
#include "TagImport.hpp"
#include "TetheredItem.hpp"
#include "USBHostItem.hpp"
#include "USBPortItem.hpp"
#include "USBPortList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications Tree View
//

// REV3 -- Implement clipboard operations.

// REV3 -- Implement drag-and-drop within lists.

// REV3 -- Implement drag-and-drop between items.

// Dynamic Class

AfxImplementDynamicClass(CCommsTreeWnd, CStdTreeWnd);

// Static Data

UINT CCommsTreeWnd::m_timerDouble = AllocTimerID();

UINT CCommsTreeWnd::m_timerSelect = AllocTimerID();

UINT CCommsTreeWnd::m_timerScroll = AllocTimerID();

// Constructors

CCommsTreeWnd::CCommsTreeWnd(void)
{
	m_dwStyle = m_dwStyle | TVS_HASBUTTONS;

	m_dwStyle = m_dwStyle | TVS_EDITLABELS | TVS_SHOWSELALWAYS;

	m_cfCode  = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_cfPart  = RegisterClipboardFormat(L"C3.1 Partial Address");

	m_fMulti  = FALSE;

	m_uLocked = 0;

	m_uDrop   = dropNone;

	m_pGroup  = NULL;

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CommsTreeIcon16"), afxColor(MAGENTA));

	m_Accel1.Create(L"NavTreeAccel");
}

// Destructor

CCommsTreeWnd::~CCommsTreeWnd(void)
{
	if( m_pGroup ) {

		m_pGroup->Kill();

		delete m_pGroup;
	}
}

// IUnknown

HRESULT CCommsTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
}

ULONG CCommsTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
}

ULONG CCommsTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
}

// IUpdate

HRESULT CCommsTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			RefreshFrom(hItem);

			if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

				Recompile();
			}

			if( pItem->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

				Recompile();
			}
		}

		return S_OK;
	}

	if( uType == updateProps ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			CMetaItem *pMeta = (CMetaItem *) pItem;

			if( HasVarImage(pMeta) ) {

				CTreeViewItem Node(hItem);

				LoadNodeItem(Node, pMeta);

				m_pTree->SetItem(Node);
			}
		}

		return S_OK;
	}

	if( uType == updateRename ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

				CCommsPort *pPort = (CCommsPort *) pItem;

				m_pTree->SetItemText(hItem, pPort->GetLabel());
			}

			if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

				COptionCardItem *pOpt = (COptionCardItem *) pItem;

				m_pTree->SetItemText(hItem, pOpt->GetTreeLabel());
			}

			if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

				CCommsMapReg *pReg = (CCommsMapReg *) pItem;

				m_pTree->SetItemText(hItem, pReg->GetTreeLabel());
			}

			if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

				CCommsMapping *pMap = (CCommsMapping *) pItem;

				m_pTree->SetItemText(hItem, pMap->GetTreeLabel());
			}
		}

		return S_OK;
	}

	return S_OK;
}

// IDropTarget

HRESULT CCommsTreeWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( !IsReadOnly() ) {

		CanAcceptDataObject(pData, m_uDrop);

		m_hDropRoot = NULL;

		m_hDropPrev = NULL;

		m_fDropMove = FALSE;

		BOOL fMove  = IsDropMove(dwKeys, pEffect);

		DropTrack(CPoint(pt.x, pt.y), fMove);

		if( IsValidDrop() ) {

			return S_OK;
		}
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CCommsTreeWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		BOOL fMove = IsDropMove(dwKeys, pEffect);

		DropTrack(CPoint(pt.x, pt.y), fMove);

		if( IsValidDrop() ) {

			return S_OK;
		}
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CCommsTreeWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( m_uDrop ) {

		ShowDrop(FALSE);

		m_System.HidePaneOnDrop(0);

		m_uDrop = dropNone;

		return S_OK;
	}

	return S_OK;
}

HRESULT CCommsTreeWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop ) {

		ShowDrop(FALSE);

		if( m_uDrop != dropOther ) {

			DropDone(pData);

			SetFocus();
		}

		m_uDrop = dropNone;

		return S_OK;
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

// Overridables

void CCommsTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	m_pComms  = (CCommsManager *) m_pItem;

	m_pSystem = (CCommsSystem  *) m_pItem->GetDatabase()->GetSystemItem();

	m_pGroup  = New CCommsPortGroup;

	m_pGroup->SetParent(m_pComms);

	m_pGroup->Init();
}

CString CCommsTreeWnd::OnGetNavPos(void)
{
	// LATER -- Refactor this back to CStdTreeWnd...

	if( m_pSelect ) {

		return m_SelPath;
	}

	return m_pItem->GetFixedPath();
}

BOOL CCommsTreeWnd::OnNavigate(CString const &Nav)
{
	if( Nav.IsEmpty() ) {

		m_pTree->SelectItem(m_hRoot);

		return TRUE;
	}
	else {
		CString Find = Nav;

		while( !Find.IsEmpty() ) {

			HTREEITEM hFind = m_MapFixed[Find];

			if( hFind ) {

				m_pTree->SelectItem(hFind);

				m_System.SetViewedItem(m_pSelect);

				return TRUE;
			}

			Find = Find.Left(Find.FindRev('/'));
		}

		return FALSE;
	}
}

void CCommsTreeWnd::OnExec(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		m_fMulti = !m_fMulti;

		LockUpdate(m_fMulti);
	}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		((CCmdItem *) pCmd)->Exec(m_pSelect);

		ListUpdated(m_hSelect);

		CheckItemCmd();
	}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnExecCreate((CCmdCreate *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnExecRename((CCmdRename *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnExecDelete((CCmdDelete *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnExecPaste((CCmdPaste *) pCmd);
	}
}

void CCommsTreeWnd::OnUndo(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		m_fMulti = !m_fMulti;

		LockUpdate(m_fMulti);
	}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		((CCmdItem *) pCmd)->Undo(m_pSelect);

		ListUpdated(m_hSelect);

		CheckItemCmd();
	}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnUndoCreate((CCmdCreate *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnUndoRename((CCmdRename *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnUndoDelete((CCmdDelete *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnUndoPaste((CCmdPaste *) pCmd);
	}
}

// Message Map

AfxMessageMap(CCommsTreeWnd, CStdTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
		AfxDispatchMessage(WM_LOADTOOL)
		AfxDispatchMessage(WM_SHOWWINDOW)
		AfxDispatchMessage(WM_TIMER)

		AfxDispatchNotify(100, NM_CUSTOMDRAW, OnTreeCustomDraw)
		AfxDispatchNotify(100, TVN_KEYDOWN, OnTreeKeyDown)
		AfxDispatchNotify(100, NM_RETURN, OnTreeReturn)
		AfxDispatchNotify(100, NM_DBLCLK, OnTreeDblClk)
		AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit)
		AfxDispatchNotify(100, TVN_ENDLABELEDIT, OnTreeEndEdit)

		AfxDispatchControlType(IDM_GO, OnGoControl)
		AfxDispatchCommandType(IDM_GO, OnGoCommand)
		AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
		AfxDispatchControlType(IDM_ITEM, OnItemControl)
		AfxDispatchCommandType(IDM_ITEM, OnItemCommand)
		AfxDispatchControlType(IDM_EDIT, OnEditControl)
		AfxDispatchCommandType(IDM_EDIT, OnEditCommand)
		AfxDispatchGetInfoType(IDM_COMMS, OnCommsGetInfo)
		AfxDispatchControlType(IDM_COMMS, OnCommsControl)
		AfxDispatchCommandType(IDM_COMMS, OnCommsCommand)

		AfxMessageEnd(CCommsTreeWnd)
};

// Message Handlers

void CCommsTreeWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
}

void CCommsTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"CommsTreeTool"));
	}
}

void CCommsTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( fShow ) {

		m_System.SetNavCheckpoint();

		m_System.SetViewedItem(m_pSelect);

		ShowStatus();

		return;
	}

	afxThread->SetStatusText(L"");
}

void CCommsTreeWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerDouble ) {

		m_System.FlipToItemView(TRUE);

		KillTimer(uID);
	}

	if( uID == m_timerSelect ) {

		if( m_uDrop ) {

			if( m_hDropRoot ) {

				if( m_uDrop != dropOther ) {

					ShowDrop(FALSE);
				}

				if( IsExpandingDrop() ) {

					ExpandItem(m_hDropRoot);

					m_pTree->UpdateWindow();
				}
				else {
					m_pTree->SelectItem(m_hDropRoot);

					m_pTree->UpdateWindow();
				}

				if( m_uDrop != dropOther ) {

					ShowDrop(TRUE);
				}
			}
		}

		KillTimer(uID);
	}

	if( uID == m_timerScroll ) {

		if( m_uDrop ) {

			UINT  uCmd = NOTHING;

			CRect View = m_pTree->GetClientRect();

			ClientToScreen(View);

			if( View.PtInRect(m_DragPos) ) {

				View -= 40;

				if( m_DragPos.y < View.top ) {

					int nLimit = m_pTree->GetScrollRangeMin(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) > nLimit ) {

						uCmd = SB_LINEUP;
					}
				}

				if( m_DragPos.y > View.bottom ) {

					int nLimit = m_pTree->GetScrollRangeMax(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) < nLimit ) {

						uCmd = SB_LINEDOWN;
					}
				}

				if( uCmd < NOTHING ) {

					ShowDrop(FALSE);

					m_pTree->SendMessage(WM_VSCROLL, uCmd);

					m_pTree->UpdateWindow();

					ShowDrop(TRUE);

					return;
				}
			}
		}

		KillTimer(uID);
	}
}

// Notification Handlers

UINT CCommsTreeWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
	}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMTVCUSTOMDRAW &Extra = (NMTVCUSTOMDRAW &) Info;

		HTREEITEM       hItem = HTREEITEM(Info.dwItemSpec);

		BOOL            fRed  = FALSE;

		if( hItem ) {

			CItem *pItem = (CItem *) Info.lItemlParam;

			if( pItem ) {

				if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

					CCommsMapBlock *pBlock = (CCommsMapBlock *) pItem;

					if( pBlock->IsBroken() ) {

						fRed = TRUE;
					}
				}

				if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

					CCommsMapReg *pReg = (CCommsMapReg *) pItem;

					if( pReg->IsBroken() ) {

						fRed = TRUE;
					}
				}

				if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

					CCommsMapping *pMap = (CCommsMapping *) pItem;

					if( pMap->IsBroken() ) {

						fRed = TRUE;
					}
				}

				if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

					COptionCardItem *pOpt = (COptionCardItem *) pItem;

					if( pOpt->m_Type == typeModule ) {

						Extra.clrText = afxColor(Disabled);
					}
				}
			}
		}

		if( hItem ) {

			UINT uTest = m_pTree->GetItemState(hItem, NOTHING);

			if( uTest & TVIS_DROPHILITED ) {

				Extra.clrTextBk = afxColor(Orange2);

				Extra.clrText   = fRed ? afxColor(RED) : afxColor(BLACK);

				SelectObject(Info.hdc, m_pTree->GetFont());

				return CDRF_NEWFONT;
			}
		}

		if( Info.uItemState & CDIS_HOT ) {

			if( Info.uItemState & CDIS_SELECTED ) {

				Extra.clrTextBk = afxColor(Orange2);
			}
			else
				Extra.clrTextBk = afxColor(Orange3);

			Extra.clrText = fRed ? afxColor(RED) : afxColor(BLACK);

			SelectObject(Info.hdc, m_pTree->GetFont());

			return CDRF_NEWFONT;
		}

		if( fRed ) {

			Extra.clrText = afxColor(RED);
		}
	}

	return 0;
}

BOOL CCommsTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	if( Info.wVKey == VK_TAB ) {

		m_System.FlipToItemView(TRUE);

		return TRUE;
	}

	if( Info.wVKey == VK_DELETE ) {

		if( SetMapping(L"", FALSE) ) {

			OnGoNext(FALSE);
		}

		return TRUE;
	}

	return CStdTreeWnd::OnTreeKeyDown(uID, Info);
}

void CCommsTreeWnd::OnTreeReturn(UINT uID, NMHDR &Info)
{
	if( IsParent() ) {

		ToggleItem(m_hSelect);

		return;
	}

	m_System.FlipToItemView(TRUE);
}

void CCommsTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( IsParent() ) {

			ToggleItem(m_hSelect);

			return;
		}

		SetTimer(m_timerDouble, 1);
	}
}

BOOL CCommsTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{
	return !CanItemRename();
}

BOOL CCommsTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
	}

	return FALSE;
}

// Command Handlers

BOOL CCommsTreeWnd::OnCommsGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =

	{ IDM_COMMS_ADD_BLOCK,   MAKELONG(0x0000, 0x4000),
		IDM_COMMS_ADD_DEVICE,  MAKELONG(0x0001, 0x4000),
		IDM_COMMS_WATCH_BLOCK, MAKELONG(0x0036, 0x1000),
		IDM_COMMS_ADD_RACK,    MAKELONG(0x0013, 0x4000),

	};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnCommsControl(UINT uID, CCmdSource &Src)
{
	if( m_pSelect ) {

		switch( uID ) {

			case IDM_COMMS_CLEAR_PORT:

				Src.EnableItem(CanCommsClearPort());

				return TRUE;

			case IDM_COMMS_ADD_DEVICE:

				Src.EnableItem(CanCommsAddDevice());

				return TRUE;

			case IDM_COMMS_DEL_DEVICE:

				Src.EnableItem(CanCommsDelDevice());

				return TRUE;

			case IDM_COMMS_ADD_BLOCK:

				Src.EnableItem(CanCommsAddBlock());

				return TRUE;

			case IDM_COMMS_DEL_BLOCK:

				Src.EnableItem(CanCommsDelBlock());

				return TRUE;

			case IDM_COMMS_MAP_IMPORT:

				Src.EnableItem(CanCommsMapImport());

				return TRUE;

			case IDM_COMMS_MAP_EXPORT:

				Src.EnableItem(CanCommsMapExport());

				return TRUE;

			case IDM_COMMS_MAP_EXPAND:

				Src.EnableItem(CanCommsMapExpand());

				return TRUE;

			case IDM_COMMS_MAP_COLLAPSE:

				Src.EnableItem(CanCommsMapCollapse());

				return TRUE;

			case IDM_COMMS_DEL_PORT:

				Src.EnableItem(CanCommsDelPort());

				return TRUE;

			case IDM_COMMS_ADD_PORT:

				Src.EnableItem(CanCommsAddPort());

				return TRUE;

			case IDM_COMMS_ADD_VIRTUAL:

				Src.EnableItem(CanCommsAddVirtual());

				return TRUE;

			case IDM_COMMS_REM_CARD:

				Src.EnableItem(CanCommsRemoveCard());

				return TRUE;

			case IDM_COMMS_TAG_IMPORT:

				Src.EnableItem(CanCommsTagImport());

				return TRUE;

			case IDM_COMMS_WATCH_BLOCK:

				Src.EnableItem(CanCommsWatchBlock());

				return TRUE;

			case IDM_COMMS_BUILD_BLOCK:

				Src.EnableItem(TRUE);

				return TRUE;

			case IDM_COMMS_ADD_RACK:

				Src.EnableItem(CanCommsAddRack());

				return TRUE;

			case IDM_COMMS_DEL_RACK:

				Src.EnableItem(CanCommsDelRack());

				return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnCommsCommand(UINT uID)
{
	switch( uID ) {

		case IDM_COMMS_CLEAR_PORT:

			OnCommsClearPort();

			return TRUE;

		case IDM_COMMS_ADD_DEVICE:

			OnCommsAddDevice();

			return TRUE;

		case IDM_COMMS_DEL_DEVICE:

			OnCommsDelDevice();

			return TRUE;

		case IDM_COMMS_ADD_BLOCK:

			OnCommsAddBlock();

			return TRUE;

		case IDM_COMMS_DEL_BLOCK:

			OnCommsDelBlock();

			return TRUE;

		case IDM_COMMS_MAP_IMPORT:

			OnCommsMapImport();

			return TRUE;

		case IDM_COMMS_MAP_EXPORT:

			OnCommsMapExport();

			return TRUE;

		case IDM_COMMS_MAP_EXPAND:

			OnCommsMapExpand();

			return TRUE;

		case IDM_COMMS_MAP_COLLAPSE:

			OnCommsMapCollapse();

			return TRUE;

		case IDM_COMMS_DEL_PORT:

			OnCommsDelPort();

			return TRUE;

		case IDM_COMMS_ADD_PORT:

			OnCommsAddPort();

			return TRUE;

		case IDM_COMMS_ADD_VIRTUAL:

			OnCommsAddVirtual();

			return TRUE;

		case IDM_COMMS_REM_CARD:

			OnCommsRemoveCard();

			return TRUE;

		case IDM_COMMS_TAG_IMPORT:

			OnCommsTagImport();

			return TRUE;

		case IDM_COMMS_WATCH_BLOCK:

			OnCommsWatchBlock();

			return TRUE;

		case IDM_COMMS_BUILD_BLOCK:

			OnCommsBuildBlock();

			return TRUE;

		case IDM_COMMS_ADD_RACK:

			OnCommsAddRack();

			return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_GO_NEXT_1:
		case IDM_GO_NEXT_2:

			Src.EnableItem(m_pTree->GetNextNode(m_hSelect, TRUE) ? TRUE : FALSE);

			return TRUE;

		case IDM_GO_PREV_1:
		case IDM_GO_PREV_2:

			Src.EnableItem(m_pTree->GetPrevNode(m_hSelect, TRUE) ? TRUE : FALSE);

			return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnGoCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GO_NEXT_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_NEXT_1:

			OnGoNext(TRUE);

			return TRUE;

		case IDM_GO_PREV_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_PREV_1:

			OnGoPrev(TRUE);

			return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID == IDM_ITEM_RENAME ) {

		Info.m_Image = MAKELONG(0x0021, 0x1000);
	}

	if( uID == IDM_ITEM_USAGE ) {

		Info.m_Image = MAKELONG(0x000C, 0x1000);
	}

	if( uID == IDM_ITEM_EXPAND ) {

		if( m_pTree->IsExpanded(m_hSelect) ) {

			Info.m_Image = MAKELONG(0x0020, 0x1000);
		}
		else
			Info.m_Image = MAKELONG(0x001F, 0x1000);
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	if( m_pSelect ) {

		CString Label;

		switch( uID ) {

			case IDM_ITEM_DELETE:

				Src.EnableItem(CanItemDelete());

				return TRUE;

			case IDM_ITEM_RENAME:

				Src.EnableItem(CanItemRename());

				return TRUE;

			case IDM_ITEM_USAGE:

				if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

					Src.EnableItem(TRUE);

					return TRUE;
				}

				if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

					Src.EnableItem(TRUE);

					return TRUE;
				}

				Src.EnableItem(FALSE);

				return TRUE;

			case IDM_ITEM_EXPAND:

				Label = m_pTree->IsExpanded(m_hSelect) ? CString(IDS_COLLAPSE) : CString(IDS_EXPAND);

				Src.SetItemText(Label);

				Src.EnableItem(m_hSelect != m_hRoot && IsParent());

				return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_DELETE:

			OnItemDelete(IDS_DELETE_2, TRUE, TRUE);

			return TRUE;

		case IDM_ITEM_RENAME:

			m_pTree->EditLabel(m_hSelect);

			return TRUE;

		case IDM_ITEM_USAGE:

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

				m_pSystem->FindDeviceUsage((CCommsDevice *) m_pSelect);
			}

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

				m_pSystem->FindPortUsage((CCommsPort *) m_pSelect);
			}

			return TRUE;

		case IDM_ITEM_EXPAND:

			ToggleItem(m_hSelect);

			return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			Src.EnableItem(CanItemDelete());

			return TRUE;

		case IDM_EDIT_CUT:

			Src.EnableItem(CanItemDelete() && CanItemCopy());

			return TRUE;

		case IDM_EDIT_COPY:

			Src.EnableItem(CanItemCopy());

			return TRUE;

		case IDM_EDIT_PASTE:

			Src.EnableItem(CanEditPaste());

			return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			OnItemDelete(IDS_DELETE_1, TRUE, TRUE);

			return TRUE;

		case IDM_EDIT_CUT:

			OnEditCut();

			return TRUE;

		case IDM_EDIT_COPY:

			OnEditCopy();

			return TRUE;

		case IDM_EDIT_PASTE:

			OnEditPaste();

			return TRUE;
	}

	return FALSE;
}

// Comms Menu

BOOL CCommsTreeWnd::CanCommsClearPort(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

			CCommsPort *pPort = (CCommsPort *) m_pSelect;

			if( pPort->m_DriverID ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsAddDevice(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

			CCommsPort *pPort = (CCommsPort *) m_pSelect;

			if( pPort->CanAddDevice() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsDelDevice(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsAddBlock(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

			CCommsDevice *pDevice = (CCommsDevice *) m_pSelect;

			if( pDevice->AllowMapping() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsDelBlock(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsMapImport(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsMapExport(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsMapExpand(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

			CCommsMapReg *pReg = (CCommsMapReg *) m_pSelect;

			if( !pReg->m_Bits ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsMapCollapse(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

			CCommsMapReg *pReg = (CCommsMapReg *) m_pSelect;

			if( pReg->m_Bits ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsDelPort(void)
{
	if( !IsReadOnly() ) {

		CDatabase *pDbase = m_pSystem->GetDatabase();

		if( pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPortNetwork)) ) {

				return TRUE;
			}

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPortVirtual)) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsAddPort(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CEthernetItem)) ) {

			CDatabase *pDbase = m_pSelect->GetDatabase();

			return pDbase->GetSoftwareGroup() >= SW_GROUP_3B;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsAddVirtual(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CEthernetItem)) ) {

			CDatabase *pDbase = m_pSelect->GetDatabase();

			return pDbase->GetSoftwareGroup() >= SW_GROUP_3B;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsRemoveCard(void)
{
	// Obsolete

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsTagImport(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

			CCommsDevice *pDevice = (CCommsDevice *) m_pSelect;

			if( pDevice->AllowTagInit() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsWatchBlock(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsAddRack(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCascadedRackItem)) ) {

		return ((CCascadedRackItem *) m_pSelect)->HasRoom();
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CanCommsDelRack(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(COptionCardRackItem)) ) {

		if( !m_pSelect->IsKindOf(AfxRuntimeClass(CCascadedRackItem)) ) {

			CCascadedRackItem *pRack = (CCascadedRackItem *) m_pSelect;

			return pRack->HasType(classIO) ? FALSE : TRUE;
		}
	}

	return FALSE;
}

void CCommsTreeWnd::OnCommsClearPort(void)
{
	if( WarnMultiple(CString(IDS_CLEAR_1)) ) {

		CCommsPort *pPort = (CCommsPort *) m_pSelect;

		HGLOBAL     hPrev = pPort->TakeSnapshot();

		pPort->ClearSettings();

		CCmd *pCmd = New CCmdItem(CString(IDS_COMMS_PORT_CLEAR),
					  pPort,
					  hPrev
		);

		m_System.SaveCmd(pCmd);

		ListUpdated(m_hSelect);

		Recompile();
	}
}

void CCommsTreeWnd::OnCommsAddDevice(void)
{
	OnCreateItem(New CCommsDevice);
}

void CCommsTreeWnd::OnCommsDelDevice(void)
{
	OnItemDelete(IDS_DELETE_2, TRUE, TRUE);
}

void CCommsTreeWnd::OnCommsAddBlock(void)
{
	OnCreateItem(New CCommsMapBlock);
}

void CCommsTreeWnd::OnCommsDelBlock(void)
{
	OnItemDelete(IDS_DELETE_2, TRUE, TRUE);
}

void CCommsTreeWnd::OnCommsMapImport(void)
{
	CCommsMapBlock *pBlock = (CCommsMapBlock *) m_pSelect;

	HGLOBAL hPrev = pBlock->TakeSnapshot();

	UINT uClear = IDYES;

	if( pBlock->m_Size ) {

		uClear = YesNo(CString(IDS_CONFIRM_MAP_IMPORT));
	}

	if( uClear == IDYES ) {

		COpenFileDialog Dlg;

		Dlg.SetFilter(CString(IDS_TEXT_FILES_TXTTXT));

		if( Dlg.Execute(ThisObject) ) {

			CTextStreamMemory Stream;

			if( Stream.LoadFromFile(Dlg.GetFilename()) ) {

				pBlock->ImportMappings(Stream);

				Stream.Close();

				ListUpdated(m_hRoot);

				CCmd *pCmd = New CCmdItem(CString(IDS_IMPORT_MAPPINGS),
							  pBlock,
							  hPrev
				);

				m_System.SaveCmd(pCmd);
			}
		}
	}
}

void CCommsTreeWnd::OnCommsMapExport(void)
{
	CSaveFileDialog Dlg;

	Dlg.SetFilter(CString(IDS_TEXT_FILES_TXTTXT));

	if( Dlg.ExecAndCheck(ThisObject) ) {

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			CCommsMapBlock *pBlock = (CCommsMapBlock *) m_pSelect;

			pBlock->ExportMappings(Stream);

			Stream.SaveToFile(Dlg.GetFilename(), saveRaw);

			Stream.Close();
		}
	}
}

void CCommsTreeWnd::OnCommsMapExpand()
{
	CCommsMapReg * pReg  = (CCommsMapReg *) m_pSelect;

	HGLOBAL        hPrev = pReg->TakeSnapshot();

	HTREEITEM      hItem = m_hSelect;

	pReg->Expand();

	hItem = RefreshFrom(hItem);

	hItem = m_pTree->GetChild(hItem);

	m_pTree->SelectItem(hItem);

	CCmd *pCmd = New CCmdItem(CString(IDS_EXPAND_BITS),
				  pReg,
				  hPrev
	);

	m_System.SaveCmd(pCmd);
}

void CCommsTreeWnd::OnCommsMapCollapse(void)
{
	CCommsMapReg * pReg  = (CCommsMapReg *) m_pSelect;

	HGLOBAL        hPrev = pReg->TakeSnapshot();

	HTREEITEM      hItem = m_hSelect;

	pReg->Collapse();

	RefreshFrom(hItem);

	CCmd *pCmd = New CCmdItem(CString(IDS_COLLAPSE_BITS),
				  pReg,
				  hPrev
	);

	m_System.SaveCmd(pCmd);
}

void CCommsTreeWnd::OnCommsDelPort(void)
{
	OnItemDelete(IDS_DELETE_2, TRUE, TRUE);
}

void CCommsTreeWnd::OnCommsAddPort(void)
{
	CEthernetItem *pEth = (CEthernetItem *) m_pSelect;

	if( pEth->m_pPorts->HasRoom() ) {

		OnCreateItem(New CCommsPortNetwork);

		return;
	}

	Error(CString(IDS_NO_FURTHER_PORTS));
}

void CCommsTreeWnd::OnCommsAddVirtual(void)
{
	CEthernetItem *pEth = (CEthernetItem *) m_pSelect;

	if( pEth->m_pPorts->HasRoom() ) {

		OnCreateItem(New CCommsPortVirtual);

		return;
	}

	Error(CString(IDS_NO_FURTHER_PORTS));
}

void CCommsTreeWnd::OnCommsRemoveCard(void)
{
	COptionCardItem *pOpt  = (COptionCardItem *) m_pSelect;

	HGLOBAL          hPrev = pOpt->TakeSnapshot();

	pOpt->RemoveCard();

	CCmd *pCmd = New CCmdItem(CString(IDS_COMMS_OPT_DEL),
				  pOpt,
				  hPrev
	);

	m_System.SaveCmd(pCmd);

	ListUpdated(m_hSelect);

	Recompile();
}

void CCommsTreeWnd::OnCommsTagImport(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		CCommsDevice *pDevice = (CCommsDevice *) m_pSelect;

		CCommsPort   *pPort   = (CCommsPort   *) pDevice->GetParent(2);

		CTagImport   *pHelper = New CTagImport(pPort, pDevice);

		pHelper->Import();

		delete pHelper;

		afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);
	}
}

void CCommsTreeWnd::OnCommsWatchBlock(void)
{
	m_pSystem->ClearWatch();

	CCommsMapBlock   *pBlock = (CCommsMapBlock *) m_pSelect;

	CCommsMapRegList *pRegs  = pBlock->m_pRegs;

	for( INDEX r = pRegs->GetHead(); !pRegs->Failed(r); pRegs->GetNext(r) ) {

		CCommsMapReg      *pReg  = pRegs->GetItem(r);

		CCommsMappingList *pMaps = pReg->m_pMaps;

		for( INDEX m = pMaps->GetHead(); !pMaps->Failed(m); pMaps->GetNext(m) ) {

			CCommsMapping *pMap = pMaps->GetItem(m);

			if( pMap->IsMapped() ) {

				CString Code = pMap->GetMapSource(TRUE);

				m_pSystem->AddToWatch(Code);
			}
		}
	}

	m_pSystem->UpdateWatch(TRUE);
}

void CCommsTreeWnd::OnCommsBuildBlock(void)
{
	m_pSystem->ClearSysBlocks();

	m_pSystem->Validate(FALSE);
}

void CCommsTreeWnd::OnCommsAddRack(void)
{
	CCascadedRackItem *pRack = (CCascadedRackItem *) m_pSelect;

	HGLOBAL hPrev = m_pSelect->TakeSnapshot();

	pRack->AddRack();

	CString Menu = CString(IDS_ADD) + m_pSelect->GetHumanName();

	CCmd *pCmd = New CCmdItem(Menu, m_pSelect, hPrev);

	m_System.SaveCmd(pCmd);

	ListUpdated(m_hSelect);
}

// Go Menu

BOOL CCommsTreeWnd::OnGoNext(BOOL fSiblings)
{
	HTREEITEM hItem = m_pTree->GetNextNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnGoPrev(BOOL fSiblings)
{
	HTREEITEM hItem = m_pTree->GetPrevNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::StepAway(void)
{
	HTREEITEM hItem;

	if( (hItem = m_pTree->GetNext(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	if( (hItem = m_pTree->GetPrevious(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	if( (hItem = m_pTree->GetParent(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	return FALSE;
}

// Item Menu : Create

void CCommsTreeWnd::OnCreateItem(CMetaItem *pItem)
{
	HTREEITEM        hRoot = m_hSelect;

	CMetaItem      * pRoot = m_pSelect;

	CString          List  = GetItemList(pRoot);

	CItemIndexList * pList = pRoot->FindIndexList(List);

	pList->AppendItem(pItem);

	m_System.SaveCmd(New CCmdCreate(pRoot, List, pItem));

	LockUpdate(TRUE);

	LoadItem(hRoot, NULL, pItem);

	ExpandItem(hRoot);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
}

void CCommsTreeWnd::OnExecCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	pItem->SetParent(pList);

	pList->AppendItem(pItem);

	LockUpdate(TRUE);

	LoadItem(hRoot, NULL, pItem);

	ExpandItem(hRoot);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
}

void CCommsTreeWnd::OnUndoCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	HTREEITEM        hItem = m_MapFixed[pCmd->m_Made];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	pList->DeleteItem(pItem);

	KillTree(hItem, TRUE);

	ListUpdated(hRoot);
}

// Item Menu : Rename

BOOL CCommsTreeWnd::CanItemRename(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

			return TRUE;
		}

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

			return TRUE;
		}

		if( FALSE ) {

			// LATER -- Renaming these is harder because the protocol
			// name gets tagged on the end. We need to strip this when
			// the edit begins, and then put it back on later. We will
			// also have to add support to OnItemRename and to the
			// functions that catch the rename actions in here and in
			// any associated resource window.

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPortNetwork)) ) {

				return TRUE;
			}

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPortVirtual)) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::OnItemRename(CString Name)
{
	CString Prev = m_pNamed->GetName();

	if( wstrcmp(Name, Prev) ) {

		if( !CheckItemName(Name) ) {

			CString Text = CFormat(IDS_THE_NAME, Name);

			Error(Text);

			return FALSE;
		}

		if( m_pNamed->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

			CError Error(TRUE);

			if( !C3ValidateName(Error, Name) ) {

				Error.Show(ThisObject);

				return FALSE;
			}
		}

		CCmd *pCmd = New CCmdRename(m_pNamed, Name);

		m_System.ExecCmd(pCmd);

		return TRUE;
	}

	return FALSE;
}

void CCommsTreeWnd::OnExecRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Name, pCmd->m_Prev);
}

void CCommsTreeWnd::OnUndoRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Prev, pCmd->m_Name);
}

void CCommsTreeWnd::RenameItem(CString Name, CString Prev)
{
	m_pNamed->SetName(Name);

	m_pNamed->SetDirty();

	m_pTree->SetItemText(m_hSelect, Name);

	if( m_pNamed->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		Recompile();
	}

	m_System.ItemUpdated(m_pSelect, updateRename);
}

// Item Menu : Delete

BOOL CCommsTreeWnd::CanItemDelete(void)
{
	if( CanCommsDelDevice() ) {

		return TRUE;
	}

	if( CanCommsDelBlock() ) {

		return TRUE;
	}

	if( CanCommsDelPort() ) {

		return TRUE;
	}

	if( CanCommsDelRack() ) {

		return TRUE;
	}

	return FALSE;
}

void CCommsTreeWnd::OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop)
{
	if( WarnMultiple(uVerb) ) {

		CMetaItem      * pRoot = (CMetaItem *) m_pSelect->GetParent(2);

		CString          List  = GetItemList(pRoot);

		CItemIndexList * pList = pRoot->FindIndexList(List);

		INDEX            Index = pList->FindItemIndex(m_pSelect);

		CItem          * pNext = pList->GetNext(Index) ? pList->GetItem(Index) : NULL;

		CCmd           * pCmd  = New CCmdDelete(pRoot, List, pNext, m_pSelect);

		m_System.ExecCmd(pCmd);
	}
}

void CCommsTreeWnd::OnExecDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM        hItem = m_hSelect;

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CLASS            Class = AfxPointerClass(pItem);

	StepAway();

	pList->DeleteItem(pItem);

	KillTree(hItem, TRUE);

	ListUpdated(hRoot);

	Recompile(Class);
}

void CCommsTreeWnd::OnUndoDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM        hNext = m_MapFixed[pCmd->m_Next];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pNext = GetItemOpt(hNext);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	CLASS            Class = AfxPointerClass(pItem);

	pList->InsertItem(pItem, pNext);

	LockUpdate(TRUE);

	HTREEITEM hItem = LoadItem(hRoot, hNext, pItem);

	m_pTree->SelectItem(hItem);

	LockUpdate(FALSE);

	ListUpdated(hRoot);

	Recompile(Class);
}

// Edit Menu : Cut and Copy

BOOL CCommsTreeWnd::CanItemCopy(void)
{
	return FALSE;
}

void CCommsTreeWnd::OnEditCut(void)
{
	if( !IsParent() || WarnMultiple(IDS_CUT_1) ) {

		OnEditCopy();

		OnItemDelete(IDS_CUT_2, FALSE, TRUE);
	}
}

void CCommsTreeWnd::OnEditCopy(void)
{
	IDataObject *pData;

	if( MakeDataObject(pData, TRUE) ) {

		OleSetClipboard(pData);

		OleFlushClipboard();

		pData->Release();
	}
}

// Edit Menu : Paste

BOOL CCommsTreeWnd::CanEditPaste(void)
{
	if( !IsReadOnly() ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			UINT uType;

			if( CanAcceptDataObject(pData, uType) ) {

				pData->Release();

				if( uType == dropFragment || uType == dropPartial ) {

					if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

						return TRUE;
					}

					if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

						return TRUE;
					}

					return FALSE;
				}

				return TRUE;
			}

			pData->Release();
		}
	}

	return FALSE;
}

void CCommsTreeWnd::OnEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		UINT uType;

		if( CanAcceptDataObject(pData, uType) ) {

			if( uType == dropFragment ) {

				CString Text;

				if( AcceptCodeFragment(Text, pData) ) {

					if( SetMapping(Text, FALSE) ) {

						OnGoNext(FALSE);
					}
				}

				CClipboard(ThisObject).Empty();
			}

			if( uType == dropPartial ) {

				CCommsMapData const *pMap;

				if( AcceptPartial(pMap, pData) ) {

					if( SetMapping(pMap) ) {

						OnGoNext(FALSE);
					}
				}

				CClipboard(ThisObject).Empty();
			}

			if( uType == dropData ) {

				OnEditPaste(pData);
			}
		}

		pData->Release();
	}
}

void CCommsTreeWnd::OnEditPaste(IDataObject *pData)
{

/*	HTREEITEM hRoot = m_pTree->GetParent(m_hSelect);

	HTREEITEM hPrev = m_hSelect;

	if( !hRoot ) {

		hRoot = m_hSelect;

		hPrev = m_hSelect;
		}
	else {
		if( m_pSelect->IsKindOf(m_Folder) ) {

			if( !IsParent() ) {

				hRoot = m_hSelect;

				hPrev = m_hSelect;
				}
			}
		}

	OnEditPaste( m_pSelect,
		     hRoot,
		     hPrev,
		     pData
		     );
*/
}

void CCommsTreeWnd::OnEditPaste(CItem *pItem, HTREEITEM hRoot, HTREEITEM hPrev, IDataObject *pData)
{
	CItem * pRoot = GetItemPtr(hRoot);

	CItem * pPrev = GetItemOpt(hPrev);

	CCmd  * pCmd  = New CCmdPaste(pItem, pRoot, pPrev, pData);

	m_System.ExecCmd(pCmd);
}

void CCommsTreeWnd::OnExecPaste(CCmdPaste *pCmd)
{

//	AcceptDataObject(pCmd, pCmd->m_pData);
}

void CCommsTreeWnd::OnUndoPaste(CCmdPaste *pCmd)
{
/*	UINT n = pCmd->m_Names.GetCount();

	while( n-- ) {

		CString    Fixed = pCmd->m_Names[n];

		HTREEITEM  hItem = m_MapFixed[Fixed];

		CMetaItem *pItem = GetItem(hItem);

		if( pItem == m_pSelect ) {

			OnGoNext(FALSE) || OnGoPrev(FALSE);
			}

		RemoveFromMap(pItem);

		m_System.RemoveFromCache(pItem);

		m_pList->DeleteItem(pItem);

		m_pTree->DeleteItem(hItem);
		}
*/
}

// Data Object Construction

BOOL CCommsTreeWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	CDataObject *pMake = New CDataObject;

	if( AddMapReg(pMake) || AddMapping(pMake) ) {

		m_uDrag = dragMapping;
	}

/*	if( m_pNamed ) {

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			if( AddItemToStream(Stream, m_hSelect) ) {

				pMake->AddStream(m_cfData, Stream);
				}
			}
		}

*/	if( pMake->IsEmpty() ) {

	delete pMake;

	pData = NULL;

	return FALSE;
}

pData = pMake;

return TRUE;
}

BOOL CCommsTreeWnd::AddMapReg(CDataObject *pData)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

		CCommsMapReg *pReg = (CCommsMapReg *) m_pSelect;

		if( !pReg->m_Bits ) {

			CCommsMapping *pMap = pReg->m_pMaps->GetItem(0U);

			CString        Code = pMap->GetMapSource(TRUE);

			if( !Code.IsEmpty() ) {

				pData->AddText(m_cfCode, Code);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::AddMapping(CDataObject *pData)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

		CCommsMapping *pMap = (CCommsMapping *) m_pSelect;

		CString        Code = pMap->GetMapSource(TRUE);

		if( !Code.IsEmpty() ) {

			pData->AddText(m_cfCode, Code);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::AddItemToStream(ITextStream &Stream, HTREEITEM hItem)
{
	CString F1   = m_pItem->GetDatabase()->GetUniqueSig();

	CString F2   = CPrintf(L"%8.8X", m_hWnd);

	CString Head = CPrintf(L"%s|%s\r\n", F1, F2);

	Stream.PutLine(Head);

	CTreeFile Tree;

	if( Tree.OpenSave(Stream) ) {

		AddItemToTreeFile(Tree, hItem);

		Tree.Close();

		return TRUE;
	}

	return FALSE;
}

void CCommsTreeWnd::AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem)
{
	CItem   *pItem = GetItemPtr(hItem);

	CString  Class = pItem->GetClassName();

	Tree.PutObject(Class.Mid(1));

	pItem->PreCopy();

	pItem->Save(Tree);

	Tree.EndObject();

	if( (hItem = m_pTree->GetChild(hItem)) ) {

		while( hItem ) {

			AddItemToTreeFile(Tree, hItem);

			hItem = m_pTree->GetNext(hItem);
		}
	}
}

// Data Object Acceptance

BOOL CCommsTreeWnd::CanAcceptDataObject(IDataObject *pData, UINT &uType)
{
	if( CanAcceptCodeFragment(pData) ) {

		uType = dropFragment;

		return TRUE;
	}

	if( CanAcceptPartial(pData) ) {

		uType = dropPartial;

		return TRUE;
	}

	uType = dropOther;

	return FALSE;
}

BOOL CCommsTreeWnd::CanAcceptCodeFragment(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfCode), NULL, 1, -1, TYMED_HGLOBAL };

	return pData->QueryGetData(&Fmt) == S_OK;
}

BOOL CCommsTreeWnd::CanAcceptPartial(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfPart), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Guid = m_pItem->GetDatabase()->GetUniqueSig();

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		if( Text.TokenLeft('|') == Guid ) {

			return TRUE;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::AcceptCodeFragment(CString &Text, IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfCode), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::AcceptPartial(CCommsMapData const * &pMap, IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfPart), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		UINT    uPtr = wcstol(Text.TokenLast('|'), NULL, 16);

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		pMap = (CCommsMapData const *) uPtr;

		return TRUE;
	}

	return FALSE;
}

/*BOOL CCommsTreeWnd::AcceptDataObject(CCmdPaste *pCmd, IDataObject *pData)
{
	if( AcceptStream(pCmd, pData) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommsTreeWnd::AcceptStream(CCmdPaste *pCmd, IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(Med.hGlobal) ) {

			BOOL fLocal = FALSE;

			if( GetStreamInfo(Stream, fLocal) ) {

				if( AcceptStream(pCmd, Stream, fLocal) ) {

					ReleaseStgMedium(&Med);

					return TRUE;
					}
				}
			}

		ReleaseStgMedium(&Med);
		}

	return FALSE;
	}

BOOL CCommsTreeWnd::AcceptStream(CCmdPaste *pCmd, ITextStream &Stream, BOOL fLocal)
{
	CTreeFile Tree;

	if( Tree.OpenLoad(Stream) ) {

		HTREEITEM    hPrev = m_MapFixed[pCmd->m_Prev];

		HTREEITEM    hBase = NULL;

		CMetaItem  * pBase = NULL;

		UINT         uBase = 0;

		UINT         uSize = pCmd->m_Names.GetCount();

		UINT         uPos  = 0;

		CString      Root  = L"";

		while( !Tree.IsEndOfData() ) {

			CString Name = Tree.GetName();

			if( !Name.IsEmpty() ) {

				CLASS      Class = AfxNamedClass(PCTXT(L"C" + Name));

				CMetaItem *pItem = AfxNewObject(CMetaItem, Class);

				Tree.GetObject();

				pItem->Load(Tree);

				Tree.EndObject();

				if( pBase ) {

					Name  = pBase->GetName();

					Name += pItem->GetName().Mid(uBase);

					MakeUnique(Name);

					pItem->SetName(Name);
					}
				else {
					HTREEITEM hRoot = m_MapFixed[pCmd->m_Root];

					Root  = GetRoot(hRoot, TRUE);

					uBase = pItem->GetName().GetLength();

					Name  = Root + GetName(pItem->GetName());

					MakeUnique(Name);

					pItem->SetName(Name);

					pBase = pItem;

					ExpandItem(hRoot);
					}

				AddToList(hPrev, pItem);

				if( !uSize ) {

					pItem->PostPaste();

					pItem->PostLoad();

					pCmd->m_Names.Append(pItem->GetFixedName());
					}
				else {
					pItem->SetFixedName(pCmd->m_Names[uPos]);

					pItem->PostLoad();

					uPos++;
					}

				AddToTree(hPrev, hPrev, pItem);

				if( !fLocal ) {

					// LATER -- Validate non-local items.
					}

				if( !hBase ) {

					hBase = hPrev;
					}
				}
			}

		m_pTree->SelectItem(hBase);

		pCmd->m_Menu.Format(pCmd->m_Menu, pBase->GetName());

		return TRUE;
		}

	m_pTree->EnsureVisible(m_hSelect);

	return FALSE;
	}

BOOL CCommsTreeWnd::GetStreamInfo(ITextStream &Stream, BOOL &fLocal)
{
	TCHAR sText[256] = { 0 };

	if( Stream.GetLine(sText, elements(sText)) ) {

		CStringArray List;

		CString(sText).Tokenize(List, '|');

		if( List[0] == m_pItem->GetDatabase()->GetUniqueSig() ) {

			fLocal = TRUE;

			return TRUE;
			}

		fLocal = FALSE;

		return TRUE;
		}

	return FALSE;
	}*/

// Drag Hooks

DWORD CCommsTreeWnd::FindDragAllowed(void)
{
	if( !IsReadOnly() ) {

		if( m_uDrag == dragMapping ) {

			return DROPEFFECT_LINK;
		}
	}

	return DROPEFFECT_NONE;
}

// Drop Support

void CCommsTreeWnd::DropTrack(CPoint Pos, BOOL fMove)
{
	m_DragPos = Pos;

	Pos   -= m_DragOffset;

	Pos.y += m_DragSize.cy / 2;

	m_pTree->ScreenToClient(Pos);

	if( m_pTree->GetClientRect().PtInRect(Pos) ) {

		HTREEITEM hItem = m_pTree->HitTestItem(Pos);

		HTREEITEM hRoot = NULL;

		HTREEITEM hPrev = NULL;

		if( m_uDrop == dropData ) {

			if( !hRoot && hItem == m_hRoot ) {

				hRoot = m_hRoot;

				hPrev = NULL;
			}

			if( !hRoot && hItem ) {

				if( IsParent(hItem) ) {

					if( !m_pTree->IsExpanded(hItem) ) {

						CRect Rect = m_pTree->GetItemRect(hItem, TRUE);

						if( Pos.y > Rect.top + 8 ) {

							hRoot = hItem;

							hPrev = NULL;
						}
					}
				}
			}

			if( !hRoot ) {

				HTREEITEM hScan, hBack;

				if( hItem ) {

					hScan = m_pTree->GetPreviousVisible(hItem);

					hBack = m_pTree->GetParent(hItem);
				}
				else {
					hScan = hItem;

					hBack = NULL;

					WalkToLast(hScan);
				}

				if( hScan != hBack ) {

					while( !m_pTree->GetNext(hScan) ) {

						HTREEITEM hNext = m_pTree->GetParent(hScan);

						CRect     Rect  = m_pTree->GetItemRect(hScan, TRUE);

						if( hNext == m_hRoot || Pos.x >= Rect.left - 24 ) {

							hPrev = hScan;

							hRoot = hNext;

							break;
						}

						hScan = hNext;
					}
				}
			}

			if( !hRoot && hItem ) {

				hRoot = m_pTree->GetParent(hItem);

				hPrev = m_pTree->GetPrevious(hItem);
			}
		}
		else {
			hRoot = hItem;

			hPrev = NULL;
		}

		if( m_hDropRoot != hRoot || m_hDropPrev != hPrev || m_fDropMove != fMove ) {

			ShowDrop(FALSE);

			m_hDropRoot = hRoot;

			m_hDropPrev = hPrev;

			m_fDropMove = fMove;

			if( IsExpandingDrop() || m_uDrop == dropOther ) {

				SetTimer(m_timerSelect, 300);
			}
			else
				KillTimer(m_timerSelect);

			ShowDrop(TRUE);

			DropDebug();
		}
	}

	SetTimer(m_timerScroll, 10);
}

BOOL CCommsTreeWnd::DropDone(IDataObject *pData)
{
	if( m_uDrop == dropFragment ) {

		CString Text;

		if( AcceptCodeFragment(Text, pData) ) {

			m_pTree->SelectItem(m_hDropRoot);

			SetMapping(Text, FALSE);

			return TRUE;
		}

		return FALSE;
	}

	if( m_uDrop == dropPartial ) {

		CCommsMapData const *pMap = NULL;

		if( AcceptPartial(pMap, pData) ) {

			m_pTree->SelectItem(m_hDropRoot);

			SetMapping(pMap);

			return TRUE;
		}

		return FALSE;
	}

/*	if( m_uDrop == dropData ) {

		if( m_fDropLocal ) {

			if( IsValidDrop() ) {

				CString Verb = m_fDropMove ? IDS_MOVE : IDS_COPY;

				CString Menu = CFormat(IDS_FORMAT, Verb, m_pNamed->GetName());

				MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

				if( m_fDropMove ) {

					OnItemDelete(IDS_MOVE, FALSE, FALSE);
					}

				OnEditPaste( m_pSelect,
					     m_hDropRoot,
					     m_hDropPrev,
					     pData
					     );

				MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

				return TRUE;
				}

			return FALSE;
			}

		OnEditPaste( m_pSelect,
			     m_hDropRoot,
			     m_hDropPrev,
			     pData
			     );

		return TRUE;
		}

*/	return FALSE;
}

void CCommsTreeWnd::DropDebug(void)
{
	CMetaItem *pRoot = GetItemOpt(m_hDropRoot);

	CMetaItem *pPrev = GetItemOpt(m_hDropPrev);

	CString Root = pRoot ? (pRoot->IsKindOf(AfxRuntimeClass(CMetaItem)) ? pRoot->GetName() : L"Top") : L"Null";

	CString Prev = pPrev ? (pPrev->IsKindOf(AfxRuntimeClass(CMetaItem)) ? pPrev->GetName() : L"Top") : L"Null";

	CFormat Text(L"Root=%1 Prev=%2", Root, Prev);

	afxMainWnd->SendMessage(WM_SETSTATUSBAR, WPARAM(PCTXT(Text)));
}

void CCommsTreeWnd::ShowDrop(BOOL fShow)
{
	if( m_hDropRoot ) {

		CRect Horz, Vert;

		HTREEITEM hItem;

		if( !m_hDropPrev ) {

			if( IsFullItemDrop() ) {

				CTreeViewItem Node(m_hDropRoot);

				Node.SetStateMask(TVIS_DROPHILITED);

				Node.SetState(fShow ? TVIS_DROPHILITED : 0);

				m_pTree->SetItem(Node);

				m_pTree->UpdateWindow();

				return;
			}

			hItem = m_pTree->GetChild(m_hDropRoot);
		}
		else
			hItem = m_pTree->GetNext(m_hDropPrev);

		if( !hItem ) {

			HTREEITEM hDraw = m_hDropPrev;

			CRect     Root  = m_pTree->GetItemRect(hDraw, TRUE);

			WalkToLast(hDraw);

			Horz = m_pTree->GetItemRect(hDraw, TRUE);

			Horz.top    = Horz.bottom -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Root.left   - 20;

			Horz.right  = Root.right  +  8;
		}
		else {
			Horz = m_pTree->GetItemRect(hItem, TRUE);

			Horz.top    = Horz.top    -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Horz.left   - 20;

			Horz.right  = Horz.right  +  8;
		}

		if( m_hDropPrev ) {

			if( !hItem || hItem != m_pTree->GetNextVisible(m_hDropPrev) ) {

				if( ShowDropVert(m_hDropPrev) ) {

					Vert.left   = Horz.left   -  0;

					Vert.right  = Vert.left   +  2;

					Vert.bottom = Horz.top    -  0;

					Vert.top    = Vert.bottom - 16;
				}
			}
		}

		CClientDC DC(*m_pTree);

		CBrush Brush(afxColor(WHITE), afxColor(BLACK), 128);

		DC.Select(IsValidMove() ? afxBrush(WHITE) : Brush);

		DC.PatBlt(Horz, PATINVERT);

		DC.PatBlt(Vert, PATINVERT);

		DC.Deselect();
	}
}

BOOL CCommsTreeWnd::ShowDropVert(HTREEITEM hItem)
{
	if( m_pTree->GetChild(hItem) ) {

		if( !m_pTree->GetItemState(hItem, TVIS_EXPANDED) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CCommsTreeWnd::IsExpandingDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
			}

			return FALSE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::IsFullItemDrop(void)
{
	if( m_uDrop == dropFragment || m_uDrop == dropPartial ) {

		return TRUE;
	}

	if( m_uDrop == dropOther ) {

		return TRUE;
	}

	if( m_uDrop == dropData ) {

		if( m_hDropRoot && !m_hDropPrev ) {

			if( m_pTree->GetChild(m_hDropRoot) ) {

				if( !m_pTree->IsExpanded(m_hDropRoot) ) {

					return TRUE;
				}

				return FALSE;
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCommsTreeWnd::IsDropMove(DWORD dwKeys, DWORD *pEffect)
{
	if( m_uDrop == dropFragment || m_uDrop == dropPartial ) {

		*pEffect = DROPEFFECT_LINK;

		return FALSE;
	}

/*	if( m_uDrop == dropAccept ) {

		if( m_fDropLocal && !(dwKeys & MK_CONTROL) ) {

			*pEffect = DROPEFFECT_MOVE;

			return TRUE;
			}

		*pEffect = DROPEFFECT_COPY;

		return FALSE;
		}

*/	*pEffect = DROPEFFECT_NONE;

return FALSE;
}

BOOL CCommsTreeWnd::IsValidDrop(void)
{

	// LATER -- This can be shared by the paste routing...

	if( m_uDrop == dropFragment || m_uDrop == dropPartial ) {

		if( m_hDropRoot ) {

			CMetaItem *pItem = GetItemPtr(m_hDropRoot);

			if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

				return TRUE;
			}

			if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

				return TRUE;
			}
		}

		return FALSE;
	}

	if( m_uDrop == dropOther ) {

		return TRUE;
	}

	return TRUE;
}

BOOL CCommsTreeWnd::IsValidMove(void)
{
	if( m_uDrop == dropData ) {

		if( !IsExpandingDrop() ) {

			if( m_fDropMove ) {

				if( IsParent(m_hSelect) ) {

					HTREEITEM hScan = m_hDropRoot;

					while( hScan ) {

						if( hScan == m_hSelect ) {

							return FALSE;
						}

						hScan = m_pTree->GetParent(hScan);
					}
				}
			}

		// LATER -- Disallow null moves.

			return TRUE;
		}
	}

	return TRUE;
}

// Data Mapping

BOOL CCommsTreeWnd::SetMapping(CCommsMapData const *pMap)
{
	CString Text;

	if( m_pComms->ResolveMapData(Text, pMap) ) {

		return SetMapping(Text, TRUE);
	}

	return FALSE;
}

BOOL CCommsTreeWnd::SetMapping(CString Text, BOOL fStep)
{
	CMetaItem     *pItem  = GetItemPtr(m_hSelect);

	CCommsMapping *pMap   = NULL;

	CCommsMapReg  *pReg   = NULL;

	UINT	       uCount = 1;

	AfxAssume(pItem);

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

		pMap = (CCommsMapping *) pItem;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

		pReg = (CCommsMapReg *) pItem;

		if( pReg->m_Bits ) {

			if( pReg->IsMapped() ) {

				CString Text = IDS_DO_YOU_WANT_TO;

				if( YesNo(Text) == IDNO ) {

					return FALSE;
				}
			}

			OnCommsMapCollapse();

			pMap = pReg->m_pMaps->GetItem(0U);
		}
		else {
			if( m_pComms->IsFragmentBit(Text) ) {

				OnCommsMapExpand();

				pItem = GetItemPtr(m_hSelect);

				pMap  = pReg->m_pMaps->GetItem(0U);

				pReg  = NULL;
			}

			if( pReg ) {

				pMap = pReg->m_pMaps->GetItem(0U);
			}
		}
	}

	if( pMap == NULL ) {

		return FALSE;
	}

	if( fStep ) {

		if( m_pComms->CanStepFragment(Text) ) {

			HTREEITEM hScan = m_hSelect;

			while( hScan = m_pTree->GetNext(hScan) ) {

				if( m_pTree->GetChild(hScan) ) {

					break;
				}

				uCount++;
			}

			CStringDialog Dlg;

			Dlg.SetCaption(CString(IDS_CREATE_MAPPING));

			Dlg.SetGroup(CString(IDS_NUMBER_OF));

			Dlg.SetData(CPrintf(L"%u", uCount));

			if( !Dlg.Execute(ThisObject) ) {

				return FALSE;
			}

			if( !(uCount = watoi(Dlg.GetData())) ) {

				return FALSE;
			}
		}
	}

	BOOL    fClr = Text.IsEmpty();

	CString Verb = fClr ? CString(IDS_CLEAR_2) : CString(IDS_CHANGE);

	CString Menu = CFormat(CString(IDS_FMT_MAPPING), Verb);

	BOOL    fMul = FALSE;

	for( UINT n = 0;; ) {

		HGLOBAL hPrev = pItem->TakeSnapshot();

		UINT    uCode = pMap->SetMapping(Text);

		if( uCode == 2 ) {

			if( uCount > 1 ) {

				if( !fMul ) {

					MarkMulti(pItem->GetFixedPath(),
						  Menu,
						  navSeq,
						  TRUE
					);

					fMul = TRUE;
				}
			}

			if( pReg ) {

				CString Label = pReg->GetTreeLabel();

				m_pTree->SetItemText(m_hSelect, Label);
			}
			else {
				CString Label = pMap->GetTreeLabel();

				m_pTree->SetItemText(m_hSelect, Label);
			}

			CCmd *pCmd = New CCmdItem(Menu,
						  pItem,
						  hPrev
			);

			m_System.SaveCmd(pCmd);

			m_pItem->SetDirty();
		}
		else {
			GlobalFree(hPrev);

			if( uCode == 0 ) {

				MessageBeep(0);

				break;
			}
		}

		if( ++n < uCount ) {

			if( m_pComms->StepFragment(Text, 1) ) {

				if( OnGoNext(FALSE) ) {

					if( pReg ) {

						pItem = GetItemPtr(m_hSelect);

						pReg  = (CCommsMapReg *) pItem;

						pMap  = pReg->m_pMaps->GetItem(0U);

						AfxAssume(pItem);

						continue;
					}

					pItem = GetItemPtr(m_hSelect);

					pMap  = (CCommsMapping *) pItem;

					AfxAssume(pItem);

					continue;
				}
			}
		}

		break;
	}

	if( fMul ) {

		MarkMulti(pItem->GetFixedPath(),
			  Menu,
			  navSeq,
			  TRUE
		);
	}

	return TRUE;
}

// Tree Loading

void CCommsTreeWnd::LoadTree(void)
{
	LoadRoot();

	LoadNodeKids(m_hRoot, m_pItem);

	SelectRoot();
}

void CCommsTreeWnd::LoadRoot(void)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, m_pItem);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);

	AddToMap(m_pItem, m_hRoot);
}

HTREEITEM CCommsTreeWnd::LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	HTREEITEM hPrev = hNext ? m_pTree->GetPrevious(hNext) : TVI_LAST;

	HTREEITEM hUsed = hPrev ? hPrev : TVI_FIRST;

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, hUsed, Node);

	AddToMap(pItem, hItem);

	LoadNodeKids(hItem, pItem);

	return hItem;
}

void CCommsTreeWnd::LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		Node.SetText(m_pItem->GetHumanName());

		Node.SetParam(LPARAM(m_pItem));

		Node.SetImages(IDI_COMMS);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

		CCommsPort *pPort = (CCommsPort *) pItem;

		Node.SetText(pPort->GetLabel());

		Node.SetParam(LPARAM(pPort));

		Node.SetImages(pPort->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsPortList)) ) {

		Node.SetText(IDS("Comms Ports"));

		Node.SetParam(LPARAM(pItem));

		Node.SetImages(IDI_COMM_PORTS);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		CCommsDevice *pDevice = (CCommsDevice *) pItem;

		Node.SetText(pDevice->m_Name);

		Node.SetParam(LPARAM(pDevice));

		Node.SetImages(pDevice->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

		CCommsMapBlock *pBlock= (CCommsMapBlock *) pItem;

		Node.SetText(pBlock->m_Name);

		Node.SetParam(LPARAM(pBlock));

		Node.SetImages(pBlock->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

		CCommsMapReg *pReg = (CCommsMapReg *) pItem;

		Node.SetText(pReg->GetTreeLabel());

		Node.SetParam(LPARAM(pReg));

		Node.SetImages(pReg->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

		CCommsMapping *pMap = (CCommsMapping *) pItem;

		Node.SetText(pMap->GetTreeLabel());

		Node.SetParam(LPARAM(pMap));

		Node.SetImages(pMap->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CEthernetItem)) ) {

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pItem));

		Node.SetImages(IDI_ETHERNET_PORT);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

		COptionCardItem *pOpt = (COptionCardItem *) pItem;

		Node.SetText(pOpt->GetTreeLabel());

		Node.SetParam(LPARAM(pOpt));

		Node.SetImages(pOpt->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CConnectors)) ) {

		CConnectors *pConnectors = (CConnectors *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pConnectors));

		Node.SetImages(pConnectors->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CServices)) ) {

		CServices *pServices = (CServices *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pServices));

		Node.SetImages(pServices->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CServiceItem)) ) {

		CServiceItem *pService = (CServiceItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pService));

		Node.SetImages(pService->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CUSBHostItem)) ) {

		CUSBHostItem *pUSB = (CUSBHostItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pUSB));

		Node.SetImages(pUSB->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CUSBPortItem)) ) {

		CUSBPortItem *pPort = (CUSBPortItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pPort));

		Node.SetImages(pPort->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CExpansionItem)) ) {

		CExpansionItem *pExpansion = (CExpansionItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pExpansion));

		Node.SetImages(pExpansion->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(COptionCardRackItem)) ) {

		COptionCardRackItem *pRack = (COptionCardRackItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pRack));

		Node.SetImages(pRack->GetTreeImage());

		return;
	}

	AfxAssert(FALSE);
}

void CCommsTreeWnd::LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		CCommsManager *pManager = (CCommsManager *) pItem;

		CDatabase     *pDbase   = pManager->GetDatabase();

		if( C3OemFeature(L"OemSD", FALSE) ) {

			// LATER -- This will need to be confirmed...
		}
		else {
			LoadItem(hRoot, NULL, pManager->m_pEthernet);

			LoadItem(hRoot, NULL, (CMetaItem *) pManager->m_pPorts);

			UINT uGroup = pDbase->GetSoftwareGroup();

			if( uGroup == SW_GROUP_2 || uGroup >= SW_GROUP_3B ) {

				LoadItem(hRoot, NULL, pManager->m_pConnectors);

				LoadItem(hRoot, NULL, pManager->m_pServices);
			}
		}

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsPortList)) ) {

		CCommsPortList *pPorts = (CCommsPortList *) pItem;

		LoadList(hRoot, pPorts);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

		CCommsPort *pPort = (CCommsPort *) pItem;

		LoadList(hRoot, pPort->m_pDevices);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		CCommsDevice *pDevice = (CCommsDevice *) pItem;

		LoadList(hRoot, pDevice->m_pMapBlocks);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

		CCommsMapBlock *pBlock = (CCommsMapBlock *) pItem;

		LoadList(hRoot, pBlock->m_pRegs);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

		CCommsMapReg *pReg = (CCommsMapReg *) pItem;

		if( pReg->m_Bits ) {

			LoadList(hRoot, pReg->m_pMaps);
		}

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CEthernetItem)) ) {

		CEthernetItem *pEth = (CEthernetItem *) pItem;

		LoadList(hRoot, pEth->m_pPorts);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

		COptionCardItem *pOpt     = (COptionCardItem *) pItem;

		CCommsManager   *pManager = (CCommsManager *) pItem->GetParent(1);

		if( pOpt->m_Type == typeUSBHost ) {

			if( !m_MapFixed[pManager->m_pUSBHost->GetFixedPath()] ) {

				LoadItem(hRoot, NULL, pManager->m_pUSBHost);
			}
		}
		else {
			LoadList(hRoot, pOpt->m_pPorts);
		}

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CServices)) ) {

		CServices *pServices = (CServices *) pItem;

		LoadList(hRoot, pServices->m_pList, 1);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CConnectors)) ) {

		CServices *pServices = m_pComms->m_pServices;

		LoadList(hRoot, pServices->m_pList, 2);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CUSBHostItem)) ) {

		CUSBHostItem *pUSB = (CUSBHostItem *) pItem;

		LoadList(hRoot, pUSB->m_pPorts);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CExpansionItem)) ) {

		CExpansionItem *pExpansion = (CExpansionItem *) pItem;

		LoadList(hRoot, pExpansion->m_pRack);

		LoadItem(hRoot, NULL, pExpansion->m_pCascaded);

		LoadItem(hRoot, NULL, pExpansion->m_pTethered);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(COptionCardRackItem)) ) {

		COptionCardRackItem *pRack = (COptionCardRackItem *) pItem;

		LoadList(hRoot, pRack->m_pSlots);

		LoadList(hRoot, pRack->m_pRacks);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
	}
}

void CCommsTreeWnd::LoadList(HTREEITEM hRoot, CItemList *pList, UINT uOption)
{
	for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

		CMetaItem *pItem = (CMetaItem *) pList->GetItem(n);

		if( pItem->IsKindOf(AfxRuntimeClass(CServiceItem)) ) {

			CServiceItem *pServ = (CServiceItem *) pItem;

			if( !pServ->IsEnabled() ) {

				continue;
			}

			if( (uOption == 1) ^ !wstrstr(pServ->GetClassName(), L"Cloud") ) {

				continue;
			}
		}

		if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

			COptionCardItem *pOpt = (COptionCardItem *) pItem;

			if( pOpt->m_Type == typeRack ) {

				continue;
			}
		}

		if( pItem->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

			// REV3 -- This is not ideal, as it doesn't work from the
			// point of view of the navigation history and it does not
			// show the group names. We ought to have multiple items.

			CCommsPort *pPort1 = (CCommsPort *) pItem;

			if( pPort1->m_PortPhys < NOTHING ) {

				INDEX m = n;

				if( pList->GetNext(m) ) {

					CCommsPort *pPort2 = (CCommsPort *) pList->GetItem(m);

					if( pPort1->m_PortPhys == pPort2->m_PortPhys ) {

						CString Name1 = pPort1->GetName();

						CString Name2 = pPort2->GetName();

						CString Root1 = FindRootName(Name1);

						CString Root2 = FindRootName(Name2);

						CString Full1 = Root1 + CString(IDS_MODE);

						CString Full2 = Root2 + CString(IDS_MODE);

						CTreeViewItem Node;

						Node.SetText(Name1);

						Node.SetParam(LPARAM(m_pGroup));

						Node.SetImages(pPort1->GetTreeImage());

						HTREEITEM hItem = m_pTree->InsertItem(hRoot, TVI_LAST, Node);

						pPort1->SetUsed(Full1);

						pPort2->SetUsed(Full2);

						LoadItem(hItem, NULL, pPort1);

						LoadItem(hItem, NULL, pPort2);

						ExpandItem(hItem);

						n = m;

						continue;
					}
				}
			}
		}

		LoadItem(hRoot, NULL, pItem);
	}
}

HTREEITEM CCommsTreeWnd::RefreshFrom(HTREEITEM hItem)
{
	if( !m_uLocked ) {

		LockUpdate(TRUE);

		CString    State = SaveSelState();

		CMetaItem *pItem = GetItemPtr(hItem);

		HTREEITEM  hNext = m_pTree->GetNext(hItem);

		HTREEITEM  hRoot = m_pTree->GetParent(hItem);

		KillTree(hItem, TRUE);

		hItem = LoadItem(hRoot, hNext, pItem);

		ExpandItem(hRoot);

		ExpandItem(hItem);

		LoadSelState(State);

		SkipUpdate();

		return hItem;
	}

	m_fRefresh = TRUE;

	return hItem;
}

// Item Mapping

void CCommsTreeWnd::AddToMap(CMetaItem *pItem, HTREEITEM hItem)
{
	BOOL fTest = m_MapFixed.Insert(pItem->GetFixedPath(), hItem);

	AfxAssert(fTest);
}

void CCommsTreeWnd::RemoveFromMap(CMetaItem *pItem)
{
	m_MapFixed.Remove(pItem->GetFixedPath());
}

// Item Hooks

void CCommsTreeWnd::NewItemSelected(void)
{
	if( !m_uLocked ) {

		m_System.SetViewedItem(m_pSelect);
	}

	ShowStatus();
}

BOOL CCommsTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"CommsTreeCtxMenu";

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsTreeWnd::CheckItemName(CString const &Name)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		CCommsPortList *pList;

		for( UINT s = 0; m_pComms->GetPortList(pList, s); s++ ) {

			INDEX Index = pList->GetHead();

			while( !pList->Failed(Index) ) {

				CCommsPort *pPort = pList->GetItem(Index);

				CItem      *pItem = pPort->m_pDevices->GetItem(Name);

				if( pItem ) {

					if( pItem == m_pSelect ) {

						return TRUE;
					}

					return FALSE;
				}

				pList->GetNext(Index);
			}
		}

		return TRUE;
	}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapBlock)) ) {

		// LATER -- Do we want unique names?

		return TRUE;
	}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

		// LATER -- Do we want unique names?

		return TRUE;
	}

	return FALSE;
}

void CCommsTreeWnd::KillItem(HTREEITEM hItem, BOOL fRemove)
{
	if( fRemove ) {

		if( hItem == m_hRoot ) {

			m_MapFixed.Empty();

			return;
		}

	// LATER -- This is very expensive, but we can't
	// get the item's fixed name as the item may well
	// have been deleted by this point. We could use
	// another index to speed this up if required...

		INDEX Index = m_MapFixed.FindData(hItem);

		if( !m_MapFixed.Failed(Index) ) {

			m_MapFixed.Remove(Index);

			return;
		}

	/*AfxAssert(FALSE);*/
	}
}

// Item Data

CString CCommsTreeWnd::GetItemList(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

		return L"Devices";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		return L"MapBlocks";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CEthernetItem)) ) {

		return L"Ports";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

		return L"Ports";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CExpansionItem)) ) {

		return L"Rack";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CTetheredItem)) ) {

		return L"Racks";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCascadedItem)) ) {

		return L"Racks";
	}

	return L"";
}

BOOL CCommsTreeWnd::HasVarImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		return TRUE;
	}

	return FALSE;
}

// Selection Hooks

CString CCommsTreeWnd::SaveSelState(void)
{

	// LATER -- Encode expand state of children?

	return m_SelPath;
}

void CCommsTreeWnd::LoadSelState(CString State)
{
	HTREEITEM hItem = m_MapFixed[State];

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return;
	}

	SelectRoot();
}

// Implementation

BOOL CCommsTreeWnd::WarnMultiple(UINT uVerb)
{
	return WarnMultiple(CString(uVerb));
}

BOOL CCommsTreeWnd::WarnMultiple(CString Verb)
{
	if( FALSE ) {

		ExpandItem(m_hSelect);

		CString Text = CFormat(IDS_DO_YOU_WANT, Verb);

		return NoYes(Text) == IDYES;
	}

	return TRUE;
}

BOOL CCommsTreeWnd::MarkMulti(CString Item, CString Menu, UINT uCode, BOOL fMark)
{
	if( fMark ) {

		CCmd *pCmd = New CCmdMulti(Item, Menu, uCode);

		m_System.ExecCmd(pCmd);

		return TRUE;
	}

	return FALSE;
}

void CCommsTreeWnd::WalkToLast(HTREEITEM &hItem)
{
	HTREEITEM hNext;

	while( (hNext = m_pTree->GetNext(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;
	}

	while( (hNext = m_pTree->GetChild(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;

		WalkToLast(hItem);

		break;
	}
}

void CCommsTreeWnd::LockUpdate(BOOL fLock)
{
	if( fLock ) {

		if( !m_uLocked++ ) {

			SetRedraw(FALSE);

			m_fRefresh = FALSE;

			m_fRecomp  = FALSE;
		}

		return;
	}

	if( !--m_uLocked ) {

		if( m_fRefresh ) {

			RefreshTree();
		}
		else
			SetRedraw(TRUE);

		if( m_fRecomp ) {

			Recompile();
		}

		m_System.SetViewedItem(m_pSelect);

		m_System.SetViewedItem(m_pSelect);

		m_System.ItemUpdated(m_pSelect, updateChildren);

		m_pTree->UpdateWindow();
	}
}

void CCommsTreeWnd::SkipUpdate(void)
{
	if( !--m_uLocked ) {

		SetRedraw(TRUE);

		m_pTree->UpdateWindow();
	}
}

void CCommsTreeWnd::ListUpdated(HTREEITEM hItem)
{
	if( !m_uLocked ) {

		hItem = RefreshFrom(hItem);

		CMetaItem *pItem = GetItemPtr(hItem);

		m_System.ItemUpdated(pItem, updateChildren);

		m_System.ItemUpdated(pItem, updateContents);

		NewItemSelected();
	}
}

void CCommsTreeWnd::UpdateMappings(HTREEITEM hItem)
{
	HTREEITEM   hScan = m_pTree->GetChild(hItem);

	CMetaItem * pItem = GetItemPtr(hItem);

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

		CCommsMapReg *pReg = (CCommsMapReg *) pItem;

		CString      Label = pReg->GetTreeLabel();

		m_pTree->SetItemText(hItem, Label);
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

		CCommsMapping *pMap = (CCommsMapping *) pItem;

		CString       Label = pMap->GetTreeLabel();

		m_pTree->SetItemText(hItem, Label);
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCascadedRackItem)) ) {

		CCascadedRackItem *pRack = (CCascadedRackItem *) pItem;

		pRack->RemapSlots(FALSE);
	}

	while( hScan ) {

		UpdateMappings(hScan);

		hScan = m_pTree->GetNext(hScan);
	}
}

void CCommsTreeWnd::CheckItemCmd(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

		m_System.ItemUpdated(m_pSelect, updateChildren);

		Recompile();
	}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

		m_System.ItemUpdated(m_pSelect, updateChildren);

		Recompile();
	}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapReg)) ) {

		((CCommsMapReg *) m_pSelect)->Validate(TRUE);
	}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsMapping)) ) {

		((CCommsMapping *) m_pSelect)->Validate(TRUE);
	}
}

BOOL CCommsTreeWnd::Recompile(CLASS Class)
{
	if( Class->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

		return Recompile();
	}

	if( Class->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		return Recompile();
	}

	if( Class->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

		return Recompile();
	}

	if( Class->IsKindOf(AfxRuntimeClass(COptionCardRackItem)) ) {

		return Recompile();
	}

	return FALSE;
}

BOOL CCommsTreeWnd::Recompile(void)
{
	if( m_uLocked ) {

		m_fRecomp = TRUE;

		return TRUE;
	}

	m_pSystem->Validate(TRUE);

	SetRedraw(FALSE);

	UpdateMappings(m_hRoot);

	SetRedraw(TRUE);

	return TRUE;
}

void CCommsTreeWnd::ShowStatus(void)
{
	if( m_pSelect ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

			CCommsDevice *pDevice = (CCommsDevice *) m_pSelect;

			CCommsPort     *pPort = pDevice->GetParentPort();

			afxThread->SetStatusText(CPrintf(L"Port Number %d, Device Number %d", pPort->m_Number, pDevice->m_Number));

			return;
		}

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

			CCommsPort *pPort = (CCommsPort *) m_pSelect;

			afxThread->SetStatusText(CPrintf(L"Port Number %d", pPort->m_Number));

			return;
		}

		#ifdef _DEBUG

		if( m_pSelect->IsKindOf(AfxRuntimeClass(COptionCardRackItem)) ) {

			COptionCardRackItem *pRack = (COptionCardRackItem *) m_pSelect;

			CUsbTreePath &Path = (CUsbTreePath &) pRack->m_Slot;

			CString Text = CPrintf(L"Debug Info Rack=%d Mapping %d:%d:%d:%d:%d:%d:%d:%d:%d",
					       pRack->m_Number,
					       Path.a.dwCtrl,
					       Path.a.dwHost,
					       Path.a.dwTier,
					       Path.a.dwPort1,
					       Path.a.dwPort2,
					       Path.a.dwPort3,
					       Path.a.dwPort4,
					       Path.a.dwPort5,
					       Path.a.dwPort6);

			afxThread->SetStatusText(Text);

			return;
		}

		if( m_pSelect->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

			COptionCardItem *pOption = (COptionCardItem *) m_pSelect;

			CUsbTreePath    &Path    = (CUsbTreePath    &) pOption->m_Slot;

			CString Text = CPrintf(L"Debug Info Slot=%d Mapping %d:%d:%d:%d:%d:%d:%d:%d:%d",
					       pOption->m_Number,
					       Path.a.dwCtrl,
					       Path.a.dwHost,
					       Path.a.dwTier,
					       Path.a.dwPort1,
					       Path.a.dwPort2,
					       Path.a.dwPort3,
					       Path.a.dwPort4,
					       Path.a.dwPort5,
					       Path.a.dwPort6);

			afxThread->SetStatusText(Text);

			return;
		}

		#endif
	}

	afxThread->SetStatusText(L"");
}

CString CCommsTreeWnd::FindRootName(CString &Name)
{
	if( Name.Find(' ') < NOTHING ) {

		return Name.StripToken(' ');
	}

	UINT uPos = Name.FindRev('-');

	if( uPos < NOTHING ) {

		CString Root = Name;

		Name = Name.Mid(uPos+1);

		return Root.Left(uPos);
	}

	return Name;
}

// End of File
