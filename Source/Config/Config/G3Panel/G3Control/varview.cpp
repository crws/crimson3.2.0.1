
#include "intern.hpp"

#include "varview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Variable Item View
//

// Runtime Class

AfxImplementRuntimeClass(CControlVariableView, CUIItemMultiWnd);

// Constructor

CControlVariableView::CControlVariableView(CUIPageList *pList) : CUIItemMultiWnd(pList, FALSE)
{
	m_fRecycle = TRUE;
	}

// Overridables

void CControlVariableView::OnAttach(void)
{
	CUIItemMultiWnd::OnAttach();
	}

// Message Map

AfxMessageMap(CControlVariableView, CUIItemMultiWnd)
{
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)

	AfxMessageEnd(CControlVariableView)
	};

// Message Handlers

void CControlVariableView::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	}

void CControlVariableView::OnLoadTool(UINT uCode, CMenu &Menu)
{
	}

// End of File
