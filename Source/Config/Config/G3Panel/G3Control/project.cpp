
#include "intern.hpp"

#include "projview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Project Page
//

class CControlProjectPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlProjectPage(CControlProject *pProject, CString Title, UINT uPage);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CControlProject *m_pProject;

		// Implementation
		void LoadProperties(IUICreate *pView);
		void LoadCycleTime(IUICreate *pView);
		void LoadExecution(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Page
//

// Runtime Class

AfxImplementRuntimeClass(CControlProjectPage, CUIStdPage);

// Constructor

CControlProjectPage::CControlProjectPage(CControlProject *pProject, CString Title, UINT uPage)
{
	m_pProject  = pProject;

	m_Title     = Title;

	m_Class     = AfxPointerClass(pProject);

	m_uPage     = uPage;
	}

// Operations

BOOL CControlProjectPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( m_uPage == 1 ) {

		LoadProperties(pView);
		
		LoadCycleTime (pView);

		LoadExecution (pView);

		return TRUE;
		}

	if( m_uPage == 2 ) {

		pView->StartGroup(L"License", 1);

		pView->AddUI(m_pProject, L"root", L"License");

		pView->AddButton(  CString(IDS_CHECK_LICENSE),
				   L"ButtonCheckLicense"
				   );

		pView->EndGroup(FALSE);
		
		return TRUE;
		}

	if( m_uPage == 3 ) {

		CUIStdPage::LoadIntoView(pView, pItem);
		}

	return TRUE;
	}

// Implementation

void CControlProjectPage::LoadProperties(IUICreate *pView)
{
	pView->StartGroup(L"Properties", 1);

	pView->AddUI(m_pProject, L"root", L"Enable");

	pView->AddUI(m_pProject, L"root", L"StartMode");

	pView->EndGroup(FALSE);

	pView->StartGroup(L"Crimson Actions", 1);

	pView->AddUI(m_pProject, L"root", L"InitHook");

	pView->AddUI(m_pProject, L"root", L"ExecHook");

	pView->EndGroup(FALSE);
	}

void CControlProjectPage::LoadCycleTime(IUICreate *pView)
{
	pView->StartGroup(L"Cycle Time", 1);

	pView->AddUI(m_pProject, L"Cycle", L"Scan");

	pView->EndGroup(FALSE);
	}

void CControlProjectPage::LoadExecution(IUICreate *pView)
{
	pView->StartGroup(L"Execution Order", 1);

	pView->AddUI(m_pProject, L"root", L"Exec");

	pView->EndGroup(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Project
//

// Dynamic Class

AfxImplementDynamicClass(CControlProject, CProjectItem);

// Constructor

CControlProject::CControlProject(void)
{
	m_Ident        = 0;

	m_AutoName     = 0;

	m_Enable       = 1;

	m_fLicense     = FALSE;

	m_pStartMode   = NULL;

	m_pGlobals     = New CGlobalVariables;

	m_pPrograms    = New CGroupPrograms;

	m_pFuncs       = New CFunctionBlocksManager;

	m_pExec	       = New CProgramExecution;

	m_pCycle       = New CCycleTimeItem;

	m_pProjectFile = New CProjectFile(L".k5p");

	m_pExecuteFile = New CExecuteFile;

	m_pCompileFile = New CCompileFile;

	m_pDefinesFile = New CProjectFile(L".eqv");

	m_pProject     = NULL;

	m_pInitHook    = NULL;

	m_pExecHook    = NULL;
	}

// Destructor

CControlProject::~CControlProject(void)
{
	delete m_pProject;
	}

// Initial Values

void CControlProject::SetInitValues(void)
{
	CCodedHost::SetInitValues();

	SetInitial(L"StartMode", m_pStartMode, 1);
	}

// Type Access

BOOL CControlProject::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "InitHook" || Tag == "ExecHook" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	if( Tag == "StartMode" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;
		
		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Attributes

DWORD CControlProject::GetHandle(void)
{
	return m_pProject->GetHandle();
	}

CFilename CControlProject::GetProjectPath(void)
{
	return m_Path;
	}

CFilename CControlProject::GetGlobalFilePath(PCTXT pSuffix)
{
	return m_pProject->GetGlobalFilePath(pSuffix);
	}

// Object Look up

BOOL CControlProject::FindProgram(CControlProgram * &pProgram, CString Name)
{
	return m_pPrograms->FindProgram(pProgram, Name);
	}

BOOL CControlProject::FindVariable(CControlVariable * &pVariable, CString Name)
{
	return  m_pGlobals ->FindVariable(pVariable, Name) || 
		m_pPrograms->FindVariable(pVariable, Name) ;;
	}

BOOL CControlProject::FindVarGroup(CGroupVariables * &pGroup, CString Name)
{
	if( Name == L"(Global)" ) {

		pGroup = m_pGlobals;
		
		return TRUE;
		}

	if( Name == L"(Retain)" ) {

		pGroup = m_pGlobals;
		
		return TRUE;
		}

	return m_pPrograms->FindVarGroup(pGroup, Name);
	}

BOOL CControlProject::GetVarGroup(CGroupVariables * &pGroup, UINT uIndex)
{
	if( uIndex == 0 ) {

		pGroup = m_pGlobals;
		
		return TRUE;
		}

	return m_pPrograms->GetVarGroup(pGroup, uIndex - 1);
	}

// UI Creation

CViewWnd * CControlProject::CreateView(UINT uType)
{
	if( uType == viewCategory ) {

		CLASS Class = AfxNamedClass(L"CControlCatWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CControlTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewResource ) {
		
		CLASS Class = AfxNamedClass(L"CControlResTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewItem ) {

		CUIPageList *pList = New CUIPageList;		

		return New CControlProjectView(pList);
		}

	return NULL;
	}

// UI Creation

BOOL CControlProject::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CControlProjectPage(this, CString(IDS_EXECUTION),  1));

	#if defined(_DEBUG)

	pList->Append(New CControlProjectPage(this, CString(IDS_LICENSE),  2));

	pList->Append(New CUIStdPage(CString(IDS_INFORMATION), AfxPointerClass(this), 3));

	#endif

	return FALSE;
	}

// UI Update

void CControlProject::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(pHost);
		}

	if( Tag == "Enable" ) {

		DoEnables(pHost);
		}

	if( Tag == L"ButtonCheckLicense" ) {

		CString Text;

		UINT    uNbIo;

		CString Oem;

		if( Straton_SLCheck(uNbIo, Oem) ) {			

			Text.Printf(L"License Installed %d [%s]", uNbIo, Oem);			
			}
		else
			Text = CString(IDS_LICENSE_IS_NOT);

		afxMainWnd->Information(Text);
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Item Naming

BOOL CControlProject::IsHumanRoot(void) const
{
	return TRUE;
	}

// Persistance

void CControlProject::Init(void)
{		
	m_pProject = New CStratonProject;

	m_pProject->Make();

	CCodedHost::Init();

	m_Path = m_pProject->GetProjectPath();

	AddInit();

	m_pCompileFile->SaveToDisk();

	m_pCycle->Make();
	
	CControlCompiler(this).Build(CControlCompiler::compSilent);
	}

void CControlProject::PostLoad(void)
{		
	m_pProjectFile->SaveToDisk();
	
	m_pExecuteFile->SaveToDisk();
	
	m_pCompileFile->SaveToDisk();

	m_pDefinesFile->SaveToDisk();

	m_pProject->Open(m_Path);

	CCodedHost::PostLoad();
		
	CControlCompiler(this).Build(CControlCompiler::compSilent);
	}

void CControlProject::Save(CTreeFile &File)
{		
	m_pProject->Save();

	m_pProjectFile->LoadFromDisk();
	
	m_pExecuteFile->LoadFromDisk();
	
	m_pCompileFile->LoadFromDisk();
	
	m_pDefinesFile->LoadFromDisk();

	CUIItem::Save(File);
	}

void CControlProject::Load(CTreeFile &Tree)
{
	m_pProject = New CStratonProject;

	m_Path     = m_pProject->GetNextPath(TRUE);

	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		if( Name == L"Path" ) {

			Tree.GetValueAsString();
			
			continue;
			}

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

// Conversion

void CControlProject::PostConvert(void)
{
	}

// Build

BOOL CControlProject::NeedBuild(void)
{
	if( m_Enable ) {

		return WarnLicense() && CControlCompiler(this).NeedBuild();
		}

	return FALSE;
	}

BOOL CControlProject::PerformBuild(void)
{
	if( m_Enable ) {

		return CControlCompiler(this).Build(CControlCompiler::compErrors);
		}

	return FALSE;
	}

BOOL CControlProject::HasControl(void)
{
	return m_Enable;
	}

// Validation

void CControlProject::Validate(void)
{
	m_pGlobals ->Validate();

	m_pPrograms->Validate();
	}

// Download Support

void CControlProject::PrepareData(void)
{
	CUIItem::PrepareData();

	#if !defined(_DEBUG)

	m_fLicense = TRUE;

	#endif
	}

BOOL CControlProject::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_fLicense));

	Init.AddByte(BYTE(m_Enable));

	Init.AddWord(WORD(m_pCycle->m_Scan));

	Init.AddItem(itemVirtual, m_pInitHook);

	Init.AddItem(itemVirtual, m_pExecHook);

	Init.AddItem(itemVirtual, m_pStartMode);

	Init.AddItem(itemSimple,  m_pGlobals);

	Init.AddWord(WORD(m_pExecuteFile->m_Handle));

	return TRUE;
	}

// Property Filters

BOOL CControlProject::SaveProp(CString const &Tag) const
{
	if( Tag == L"Ident" || 
	    Tag == L"Path"  || 
	    Tag == L"Exec"  || 
	    Tag == L"Funcs" ){
		
		return FALSE;
		}

	return CMetaItem::SaveProp(Tag);
	}

// Meta Data Creation

void CControlProject::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Ident);
	Meta_AddString (Date);
	Meta_AddString (Path);
	Meta_AddInteger(AutoName);
	Meta_AddInteger(Enable);
	Meta_AddBoolean(License);
	Meta_AddVirtual(StartMode);
	Meta_AddObject (ProjectFile);
	Meta_AddObject (CompileFile);
	Meta_AddObject (ExecuteFile);
	Meta_AddObject (DefinesFile);
	Meta_AddObject (Programs);
	Meta_AddObject (Globals);
	Meta_AddObject (Funcs);
	Meta_AddObject (Exec);
	Meta_AddObject (Cycle);
	Meta_AddVirtual(InitHook);
	Meta_AddVirtual(ExecHook);

	Meta_SetName((IDS_CONTROL_PROJECT));
	}

// Implementation

void CControlProject::AddTest(void)
{
	m_pGlobals ->AddTest();

	m_pPrograms->AddTest();
	}

void CControlProject::FindStoragePath(void)
{
	CModule *pApp = afxModule->GetApp();

	m_Path = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Control");
	}

void CControlProject::AddInit(void)
{
	DWORD dwProject = GetHandle();

	if( TRUE ) {

		CString Name = Program_FindNextName( dwProject,
						     CControlProgram::langFBD,
						     CControlProgram::sectBegin,
						     0,
						     L"Program%1!u!"
						     );

		CControlProgram *pItem = New CControlMainProgram;

		pItem->m_Section  = CControlProgram::sectBegin;

		pItem->m_Language = CControlProgram::langFBD;

		pItem->m_Name     = Name;

		m_pPrograms->m_pObjects->AppendItem(pItem);
		}

	if( FALSE ) {

		CString Name = L"OnStartUp";

		//

		CControlProgram *pItem = New CControlExceptionProgram;

		m_pPrograms->m_pObjects->AppendItem(pItem);

		pItem->SetName(Name);

		pItem->m_Section  = CControlProgram::sectBegin;

		pItem->m_Language = CControlProgram::langST;
			
		/*pItem->OnCreate();*/
		}
	}

void CControlProject::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(m_pCycle, L"Scan",      m_Enable == 1);

	pHost->EnableUI(this,     L"StartMode", m_Enable  > 0);

	pHost->EnableUI(this,     L"Exec",      m_Enable  > 0);

	pHost->EnableUI(this,     L"ExecHook",  m_Enable  > 0);
	}

BOOL CControlProject::WarnLicense(void)
{
	if( m_Enable ) {

		// TODO -- What happens to this??? !!!

		if( GetDatabase()->HasFlag(L"ControlLicense") ) {

			CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

			Reg.MoveTo(L"G3Control");

			Reg.MoveTo(L"License");	

			BOOL fWarn = Reg.GetValue(L"Warning", UINT(1));

			return !fWarn || CLicenseDialog().Execute();
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program Execution Order
//

// Dynamic Class

AfxImplementDynamicClass(CProgramExecution, CMetaItem);

// Constructor

CProgramExecution::CProgramExecution(void)
{
	}

// End of File
