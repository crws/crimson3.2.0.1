
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PROGVIEW_HPP
	
#define	INCLUDE_PROGVIEW_HPP

/////////////////////////////////////////////////////////////////////////
//
// Program Item View
//

class CControlProgramView : public CUIItemMultiWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlProgramView(void);

	protected:
		// Data Members
		CControlProgramCtrlWnd * m_pEditor;

		// Overridables
		void OnAttach(void);
		void OnExec(CCmd *pCmd);
		void OnUndo(CCmd *pCmd);
		BOOL OnNavigate(CString const &Nav);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnSetCurrent(BOOL fCurrent);

		// View Creation
		CViewWnd * CreateViewObject(UINT n, CUIPage *pPage);

		// Implementation
		void MakeTabs(void);
		BOOL FindEditor(void);
	};

#if 0

class CControlProgramView : public CUIItemViewWnd, public IUpdate
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlProgramView(CUIPageList *pList);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		CControlProgram * m_pItem;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		
		// Overribables
		void OnAttach(void);
		BOOL OnPreDetach(void);
		void OnDetach(void);
		BOOL OnNavigate(CString const &Nav);
	};

#endif

// End of File

#endif
