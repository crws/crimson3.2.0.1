
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Project Cycle Time
//

// Dynamic Class

AfxImplementDynamicClass(CCycleTimeItem, CUIItem);

// Constructor

CCycleTimeItem::CCycleTimeItem(void)
{
	m_Scan = 100;
	}

// Operation

void CCycleTimeItem::Make(void)
{
	DWORD dwProject = m_pProject->GetHandle();

	CStratonProperties Props(dwProject);

	Props.Set(CControlProject::propCycleTiming, CPrintf(L"%d", m_Scan * 1000));
	}

// Persistance

void CCycleTimeItem::Init(void)
{
	CUIItem::Init();

	FindProject();
	}

void CCycleTimeItem::PostLoad(void)
{
	CUIItem::PostLoad();

	FindProject();
	}

// Meta Data Creation

void CCycleTimeItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Scan);
	}

// Implementation

void CCycleTimeItem::FindProject(void)
{
	m_pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Extent
//

class CUITextCycleTime : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextCycleTime(void);

	protected:
		// Data Members
		CControlProject	 * m_pProject;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Data Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		void FindProject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Group
//

// Dynamic Class

AfxImplementDynamicClass(CUITextCycleTime, CUITextInteger);

// Constructor

CUITextCycleTime::CUITextCycleTime(void)
{
	}

// Overridables

void CUITextCycleTime::OnBind(void)
{
	CUITextInteger::OnBind();

	FindProject();
	}

void CUITextCycleTime::OnRebind(void)
{
	CUITextInteger::OnRebind();

	FindProject();
	}

// Overridables

CString CUITextCycleTime::OnGetAsText(void)
{
	return CUITextInteger::OnGetAsText();
	}

UINT CUITextCycleTime::OnSetAsText(CError &Error, CString Text)
{
	INT nPrev = m_pData->ReadInteger(m_pItem);

	INT nData = Parse(Text);

	if( nData - nPrev ) {

		if( Check(Error, nData) ) {

			DWORD dwProject = m_pProject->GetHandle();

			CStratonProperties Props(dwProject);

			if( Props.Set(CControlProject::propCycleTiming, CPrintf(L"%u", nData * 1000)) ) {

				BOOL fSimul = afxMiddleware->IsSimulMode(dwProject);

				if( fSimul ) {

					afxMiddleware->Execute(dwProject, CPrintf(L"SetCycleTime:%u", nData * 1000));
					}

				m_pData->WriteInteger(m_pItem, nData);

				return saveChange;
				}

			Error.Printf(CString(IDS_FMT_IS_NOT_VALID), Text);

			return saveError;
			}

		return saveError;
		}

	return saveSame;
	}

// Implementation

void CUITextCycleTime::FindProject(void)
{
	m_pProject = (CControlProject *) m_pItem->GetParent(AfxRuntimeClass(CControlProject));
	}

// End of File
