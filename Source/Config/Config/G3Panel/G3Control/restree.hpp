
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RESTREE_HPP
	
#define	INCLUDE_RESTREE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Control Resource Window
//

class CControlResTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructors
		CControlResTreeWnd(void);

		// Destructors
		~CControlResTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// Operations
		void SetProgram(DWORD dwProgram);

		// Static Data
		static CControlResTreeWnd * m_pThis;

	protected:
		// Data Members
		CControlProject * m_pProject;
		UINT		  m_cfVariable;
		UINT		  m_cfMultiple;
		DWORD		  m_dwProject;
		DWORD		  m_dwGroup;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);
		void OnDestroy(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);

		// Data Object Construction		
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL AddGroups  (CDataObject *pData);
		void AddChildren(CString &List, HTREEITEM hRoot);
		BOOL AddVariable(CDataObject *pData);
		BOOL AddMultiple(CDataObject *pData);
		void AddVariable(CString &List, CControlVariable * pVariable);
		void AddName(CString &List, CString Text);
		
		// Tree Loading
		void	  LoadImageList(void);
		void	  LoadTree(void);
		void	  LoadRoot(void);
		HTREEITEM LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem);
		void	  LoadList(HTREEITEM hRoot, CItemList *pList);
		void	  LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem);
		void	  LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem);

		// Item Hooks
		void NewItemSelected(void);
		BOOL HasFolderImage(HTREEITEM hItem);

		// Selection Hooks
		CString SaveSelState(void);
		void    LoadSelState(CString State);

		// Implementation
		void ShowStatus(BOOL fShow);
	};		

// End of File

#endif

