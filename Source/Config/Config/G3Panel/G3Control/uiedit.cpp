
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program
//

class CUITextControlProgram : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextControlProgram(void);

	protected:
		// Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program
//

// Dynamic Class

AfxImplementDynamicClass(CUITextControlProgram, CUITextElement);

// Constructor

CUITextControlProgram::CUITextControlProgram(void)
{
	m_uFlags = textEdit | textNoMerge;
	}

// Overridables

CString CUITextControlProgram::OnGetAsText(void)
{
	return CString();
	}

UINT CUITextControlProgram::OnSetAsText(CError &Error, CString Text)
{
	return saveSame;
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Program
//

class CUIControlProgram : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIControlProgram(void);

	protected:
		// Data Members
		CLayItemSize	       * m_pDataLayout;
		CControlProgramCtrlWnd * m_pDataCtrl;		

		// Core Overidables
		void OnBind(void);
		void OnRebind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);

		// Implementation
		CRect GetDataWindowRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Program
//

// Dynamic Class

AfxImplementDynamicClass(CUIControlProgram, CUIControl);

// Constructor

CUIControlProgram::CUIControlProgram(void)
{
	m_pDataLayout = NULL;

	m_pDataCtrl   = New CControlProgramCtrlWnd;
	}

// Core Overidables

void CUIControlProgram::OnBind(void)
{
	CUIControl::OnBind();

	CControlProgram *pProgram = (CControlProgram *) m_pItem;

	m_pDataCtrl->Attach(pProgram);
	}

void CUIControlProgram::OnRebind(void)
{
	CUIControl::OnRebind();

	CControlProgram *pProgram = (CControlProgram *) m_pItem;

	m_pDataCtrl->Attach(pProgram);
	}

void CUIControlProgram::OnLayout(CLayFormation *pForm)
{
	m_pDataLayout = New CLayItemSize(CSize(100, 200), CSize(4, 4), CSize(100, 100));

	m_pMainLayout = New CLayFormPad (m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem(New CLayItem());
	
	pForm->AddItem(m_pMainLayout);
	}

void CUIControlProgram::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pDataCtrl->Create( WS_CHILD | WS_TABSTOP | WS_VISIBLE,
			     GetDataWindowRect(),
			     Wnd,
			     uID++
			     );

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pDataCtrl);
	}

void CUIControlProgram::OnPosition(void)
{
	m_pDataCtrl->MoveWindow(GetDataWindowRect(), TRUE);
	}

// Data Overridables

void CUIControlProgram::OnLoad(void)
{
	}

UINT CUIControlProgram::OnSave(BOOL fUI)
{
	m_pDataCtrl->Commit();

	return saveSame;
	}

// Notification Handlers

BOOL CUIControlProgram::OnNotify(UINT uID, UINT uCode)
{
	return CUIControl::OnNotify(uID, uCode);
	}

// Data Overridables

BOOL CUIControlProgram::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	return FALSE;
	}

BOOL CUIControlProgram::OnAcceptData(IDataObject *pData)
{
	return FALSE;
	}

// Implementation

CRect CUIControlProgram::GetDataWindowRect(void)
{
	CRect Rect;
	
	Rect = m_pDataLayout->GetRect();

	Rect.top    -= 1;

	Rect.bottom += 1;

	return Rect;
	}

// End of File
