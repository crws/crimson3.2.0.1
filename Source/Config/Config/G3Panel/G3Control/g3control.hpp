
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3CONTROL_HPP
	
#define	INCLUDE_G3CONTROL_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hpp>

#include <g3comms.hpp>

#include <g3straton.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3control.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3CONTROL

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3control.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Class Identifiers
//

#define IDC_STRATON 0x8001

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CGlobalVariables;
class CGroupObjects;
class CGroupVariables;
class CGroupPrograms;
class CFunctionBlocksManager;
class CProgramExecution;
class CCycleTimeItem;
class CControlFile;
class CProjectFile;
class CCompileFile;
class CExecuteFile;
class CProgramFile;
class CControlVariable;
class CControlProgram;
class CControlObject;
class CControlObjectList;

/////////////////////////////////////////////////////////////////////////
//
// Control Project
//

class DLLAPI CControlProject : public CProjectItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlProject(void);

		// Destructor
		~CControlProject(void);

		// Initial Values
		void SetInitValues(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		DWORD	  GetHandle(void);
		CFilename GetProjectPath(void);
		CFilename GetGlobalFilePath(PCTXT pSuffix);

		// Object Look up
		BOOL FindProgram(CControlProgram * &pProgram, CString Name);
		BOOL FindVariable(CControlVariable * &pVariable, CString Name);
		BOOL FindVarGroup(CGroupVariables * &pGroup, CString Name);

		// Object Look up
		BOOL GetVarGroup(CGroupVariables * &pGroup, UINT uIndex);
		
		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Naming
		BOOL IsHumanRoot(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);
		void Save(CTreeFile &File);
		void Load(CTreeFile &File);

		// Conversion
		void PostConvert(void);

		// Control Project
		BOOL NeedBuild(void);
		BOOL PerformBuild(void);
		BOOL HasControl(void);

		// Validation
		void Validate(void);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		DWORD			  m_Ident;
		CString			  m_Date;
		CString			  m_Path;
		UINT			  m_AutoName;
		UINT			  m_Enable;
		BOOL			  m_fLicense;
		CCodedItem              * m_pStartMode;
		CGlobalVariables	* m_pGlobals;
		CGroupPrograms		* m_pPrograms;
		CFunctionBlocksManager	* m_pFuncs;
		CProgramExecution	* m_pExec;
		CCycleTimeItem	        * m_pCycle;
		CControlFile		* m_pProjectFile;
		CCompileFile		* m_pCompileFile;
		CExecuteFile		* m_pExecuteFile;
		CProjectFile		* m_pDefinesFile;
		CCodedItem		* m_pInitHook;
		CCodedItem		* m_pExecHook;

		// Properties IDs
		enum
		{
			propCycleTiming	= 1,
			};

	protected:
		// Data Members
		CStratonProject * m_pProject;

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void AddTest(void);
		void FindStoragePath(void);
		void AddInit(void);
		void DoEnables(IUIHost *pHost);
		BOOL WarnLicense(void);
	};

// End of File

#endif
