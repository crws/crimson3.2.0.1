
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Program Group Manager
//

// Dynamic Class

AfxImplementDynamicClass(CGroupPrograms, CGroupObjects);

// Constructor

CGroupPrograms::CGroupPrograms(void)
{
	}

// Object Look up

BOOL CGroupPrograms::FindProgram (CControlProgram * &pProgram, CString Name)
{
	CItem *pFind;

	if( (pFind = m_pObjects->FindByName(Name) ) ) {

		pProgram = (CControlProgram *) pFind;

		return TRUE;
		}

	INDEX n = m_pObjects->GetHead();

	while( !m_pObjects->Failed(n) ) {

		CControlObject *pObject = m_pObjects->GetItem(n);

		if( pObject->IsKindOf(AfxRuntimeClass(CControlSfcProgram)) ) {

			CGroupPrograms *pGroup = ((CControlSfcProgram *) pObject)->m_pChilds;

			if( pGroup->FindProgram(pProgram, Name) ) {
				
				return TRUE;
				}
			}
		
		m_pObjects->GetNext(n);
		}

	return FALSE;
	}

BOOL CGroupPrograms::FindVariable(CControlVariable * &pVariable, CString Name)
{
	INDEX n = m_pObjects->GetHead();

	while( !m_pObjects->Failed(n) ) {

		CControlObject *pObject = m_pObjects->GetItem(n);

		if( pObject->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

			CControlProgram *pProgram = (CControlProgram *) pObject;

			if( pProgram->FindVariable(pVariable, Name) ) {
				
				return TRUE;
				}
			}
		
		m_pObjects->GetNext(n);
		}

	return FALSE;
	}

BOOL CGroupPrograms::FindVarGroup(CGroupVariables * &pGroup, CString Name)
{
	INDEX n = m_pObjects->GetHead();

	while( !m_pObjects->Failed(n) ) {

		CControlObject *pObject = m_pObjects->GetItem(n);

		if( pObject->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

			CControlProgram *pProgram = (CControlProgram *) pObject;

			if( pProgram->FindVarGroup(pGroup, Name) ) {
				
				return TRUE;
				}
			}
		
		m_pObjects->GetNext(n);
		}

	return FALSE;
	}

BOOL CGroupPrograms::GetVarGroup(CGroupVariables * &pGroup, UINT uIndex)
{
	INDEX n = m_pObjects->GetHead();

	while( !m_pObjects->Failed(n) ) {

		CControlObject *pObject = m_pObjects->GetItem(n);

		if( pObject->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

			UINT uPos = m_pObjects->FindItemPos(pObject);

			if( uPos == uIndex ) {

				CControlProgram *pProgram = (CControlProgram *) pObject;

				pGroup = pProgram->m_pLocals;

				return TRUE;
				}
			}
		
		m_pObjects->GetNext(n);
		}

	return FALSE;
	}

// Overridables

CString CGroupPrograms::GetTreeLabel(void) const
{
	return CString(IDS_PROGRAMS);
	}

UINT CGroupPrograms::GetTreeImage(void) const
{
	return IDI_PROGRAM_GROUP;
	}

void CGroupPrograms::Validate(void)
{
	INDEX n = m_pObjects->GetHead();

	while( !m_pObjects->Failed(n) ) {

		m_pObjects->GetItem(n)->Validate();
		
		m_pObjects->GetNext(n);
		}
	}

// Meta Data Creation

void CGroupPrograms::AddMetaData(void)
{
	CGroupObjects::AddMetaData();

	Meta_AddString(Name);

	Meta_SetName((IDS_CONTROL_PROGRAMS));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Group Manager
//

// Dynamic Class

AfxImplementDynamicClass(CGroupVariables, CGroupObjects);

// Constructor

CGroupVariables::CGroupVariables(void)
{
	}

// Development

void CGroupVariables::AddTest(void)
{
	}

// Overridables

CString CGroupVariables::GetTreeLabel(void) const
{
	return L"Group of Variables";
	}

void CGroupVariables::Validate(void)
{
	INDEX n = m_pObjects->GetHead();

	while( !m_pObjects->Failed(n) ) {

		m_pObjects->GetItem(n)->Validate();
		
		m_pObjects->GetNext(n);
		}
	}

// Attributes

CLASS CGroupVariables::GetClass(void)
{
	return m_Class;
	}

CString CGroupVariables::GetClassName(void)
{
	return m_ClassName;
	}

// Item Lookup

BOOL CGroupVariables::FindVariable(CControlVariable * &pVariable, CString Name)
{
	CItem *pItem;

	if( (pItem = m_pObjects->FindByName(Name)) ) {

		pVariable = (CControlVariable *) pItem;

		return TRUE;
		}

	return FALSE;
	}

// Meta Data Creation

void CGroupVariables::AddMetaData(void)
{
	CGroupObjects::AddMetaData();

	Meta_AddString(Name);

	Meta_SetName((IDS_CONTROL_GLOBALS));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Manager
//

// Dynamic Class

AfxImplementDynamicClass(CGlobalVariables, CGroupVariables);

// Constructor

CGlobalVariables::CGlobalVariables(void)
{
	m_Class     = AfxRuntimeClass(CControlGlobalVariable);

	m_ClassName = L"CControlGlobalVariable";
	}

// Overridables

CString CGlobalVariables::GetTreeLabel(void) const
{
	return CString(IDS_CONTROL_GLOBALS);
	}

UINT CGlobalVariables::GetTreeImage(void) const
{
	return IDI_VARIABLE_GROUP;
	}

// Download Support

BOOL CGlobalVariables::MakeInitData(CInitData &Init)
{
//	CGroupVariables::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pObjects);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Manager
//

// Dynamic Class

AfxImplementDynamicClass(CLocalVariables, CGroupVariables);

// Constructor

CLocalVariables::CLocalVariables(void)
{
	m_Ident     = 0;

	m_Class     = AfxRuntimeClass(CControlLocalVariable);

	m_ClassName = L"CControlLocalVariable";
	}

// Overridables

CString CLocalVariables::GetTreeLabel(void) const
{
	return CString(IDS_LOCAL_VARIABLES);
	}

UINT CLocalVariables::GetTreeImage(void) const
{
	return IDI_FOLDER;
	}

// Persistance

void CLocalVariables::Init(void)
{		
	CGroupVariables::Init();
	
	FindProgram();

	m_Name = m_pProgram->m_Name;
	}

void CLocalVariables::PostLoad(void)
{
	CGroupVariables::PostLoad();

	FindProgram();
	}

// Meta Data Creation

void CLocalVariables::AddMetaData(void)
{
	CGroupVariables::AddMetaData();

	Meta_AddInteger(Ident);
	}

// Implementation

void CLocalVariables::FindProgram(void)
{
	m_pProgram = (CControlProgram *) GetParent(AfxRuntimeClass(CControlProgram));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Manager
//

// Dynamic Class

AfxImplementDynamicClass(CInOutVariables, CLocalVariables);

// Constructor

CInOutVariables::CInOutVariables(void)
{
	m_Class     = AfxRuntimeClass(CControlInOutVariable);

	m_ClassName = L"CControlInOutVariable";
	}

// Overridables

CString CInOutVariables::GetTreeLabel(void) const
{
	return CString(IDS_PARAMETERS);
	}

UINT CInOutVariables::GetTreeImage(void) const
{
	return IDI_FOLDER;
	}

// End of File
