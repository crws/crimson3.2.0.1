
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Flags
//

class CUITextVarAttr : public CUITextEnumIndexed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarAttr(void);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		DWORD		   m_dwProject;
		CString		   m_Property;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Data Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		void LoadFlags(void);
		BOOL WriteData(UINT uData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Flags
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarAttr, CUITextEnumIndexed);

// Constructor

CUITextVarAttr::CUITextVarAttr(void)
{
	}

// Overridables

void CUITextVarAttr::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	m_Property  = List[0];

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();

	LoadFlags();
	}

void CUITextVarAttr::OnRebind(void)
{
	CUITextElement::OnRebind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	m_Property  = List[0];

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();
	
	LoadFlags();
	}

// Overridables

CString CUITextVarAttr::OnGetAsText(void)
{
	return CUITextEnumIndexed::OnGetAsText();
	}

UINT CUITextVarAttr::OnSetAsText(CError &Error, CString Text)
{
	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_Enum[n].m_Text == Text ) {

			UINT uPrev = m_pData->ReadInteger(m_pItem);

			UINT uData = m_Enum[n].m_Data;

			if( uData - uPrev ) {

				if( WriteData(uData) ) {

					m_pData->WriteInteger(m_pItem, uData);

					CStratonProjectDescriptor(m_dwProject).Save();

					return saveChange;
					}

				Error.SetUI(TRUE);

				Error.Set(L"Internal Error - failed to write variable attribute");

				return saveError;
				}
			}
		}

	return saveSame;
	}

// Implementation

void CUITextVarAttr::LoadFlags(void)
{
	if( m_Property == L"a" ) {
		
		// Access

		if( TRUE ) {

			CEntry Enum;

			Enum.m_Data = CControlVariable::attrEmpty;

			Enum.m_Text = CString(IDS_READ_AND_WRITE);

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}

		if( TRUE ) {

			CEntry Enum;

			Enum.m_Data = CControlVariable::attrReadOnly;

			Enum.m_Text = CString(IDS_READ_ONLY);

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}

		return;
		}

	if( m_Property == L"t" ) {
		
		// Access

		if( TRUE ) {

			CEntry Enum;

			Enum.m_Data = CControlVariable::attrFbInput;

			Enum.m_Text = CString(IDS_INPUT);

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}

		if( TRUE ) {

			CEntry Enum;

			Enum.m_Data = CControlVariable::attrFbOutput;

			Enum.m_Text = CString(IDS_OUTPUT);

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}

		if( TRUE ) {

			CEntry Enum;

			Enum.m_Data = CControlVariable::attrInOut | CControlVariable::attrFbInput;

			Enum.m_Text = CString(IDS_INPUTOUTPUT);

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}

		return;
		}

	CEntry Enum;

	Enum.m_Data = 0;

	Enum.m_Text = L"<not known>";

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

BOOL CUITextVarAttr::WriteData(UINT uData)
{
	DWORD dwVariable = m_pVariable->GetHandle();

	if( TRUE ) {

		CStratonVariableDescriptor Desc(m_dwProject, dwVariable);

		DWORD dwMask  = 0;
			
		if( m_Property == L"a" ) {

			dwMask = 0x01;	// Read Only
			}

		if( m_Property == L"t" ) {

			dwMask = 0x0C;	// Input/Output
			}

		if( uData ) {

			Desc.m_dwFlags |= uData;
			}
		else {
			Desc.m_dwFlags &= ~dwMask;
			}

		return Desc.Set();
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Extent
//

class CUITextVarExtent : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarExtent(void);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		DWORD		   m_dwProject;


		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Data Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Group
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarExtent, CUITextInteger);

// Constructor

CUITextVarExtent::CUITextVarExtent(void)
{
	}

// Overridables

void CUITextVarExtent::OnBind(void)
{
	CUITextInteger::OnBind();

	m_pVariable = (CControlVariable *) m_pItem;
	
	m_dwProject = m_pVariable->GetProject();
	}

void CUITextVarExtent::OnRebind(void)
{
	CUITextInteger::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem;
	
	m_dwProject = m_pVariable->GetProject();
	}

// Overridables

CString CUITextVarExtent::OnGetAsText(void)
{
	return CUITextInteger::OnGetAsText();
	}

UINT CUITextVarExtent::OnSetAsText(CError &Error, CString Text)
{
	DWORD dwVariable = m_pVariable->GetHandle();

	INT       nPrev = m_pData->ReadInteger(m_pItem);

	INT       nData = Parse(Text);

	if( Check(Error, nData) ) {

		if( nData - nPrev ) {

			CStratonVariableDescriptor Desc(m_dwProject, dwVariable);

			Desc.m_dwDim = nData;

			if( Desc.CanSet(Error) ) {
			
				Desc.Set();

				m_pData->WriteInteger(m_pItem, nData);

				CStratonProjectDescriptor(m_dwProject).Save();

				return saveChange;
				}

			Error.Printf(CString(IDS_FMT_IS_NOT_VALID), Text);

			return saveError;
			}

		return saveError;
		}

	return saveSame;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Group
//

class CUITextEnumVarGroup : public CUITextEnumIndexed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnumVarGroup(void);

	protected:
		// Data Members
		DWORD		   m_dwProject;
		CControlVariable * m_pVariable;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Data Overridables
		CString OnGetAsText(void);

		// Implementation
		void LoadGroups(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Group
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnumVarGroup, CUITextEnumIndexed);

// Constructor

CUITextEnumVarGroup::CUITextEnumVarGroup(void)
{
	m_uFlags &= ~textEdit;

	m_uFlags |= textReload;
	}

// Overridables

void CUITextEnumVarGroup::OnBind(void)
{
	CUITextElement::OnBind();

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();

	LoadGroups();
	}

void CUITextEnumVarGroup::OnRebind(void)
{
	CUITextElement::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();

	LoadGroups();
	}

// Data Overridables

CString CUITextEnumVarGroup::OnGetAsText(void)
{
	DWORD dwVariable = m_pVariable->GetHandle();

	CStratonVariableDescriptor Desc(m_dwProject, dwVariable);

	UINT d = Desc.m_dwGroup;

	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_Enum[n].m_Data == d ) {

			return m_Enum[n].m_Text;
			}
		}

	return L"<Unknown>";
	}

// Implementation

void CUITextEnumVarGroup::LoadGroups(void)
{
	CLongArray List;

	UINT c;

	if( (c = afxDatabase->GetGroups(m_dwProject, List)) ) {

		for( UINT n = 0; n < c; n ++ ) {
		
			CStratonGroupDescriptor Desc(m_dwProject, List[n]);

			CEntry Enum;

			Enum.m_Data = Desc.m_dwHandle;

			Enum.m_Text = Desc.m_Name;

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}
		
		return;
		}

	CEntry Enum;

	Enum.m_Data = 0;

	Enum.m_Text = L"Unknown";

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Persistence
//

class CUITextEnumVarPersist : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnumVarPersist(void);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		DWORD		   m_dwProject;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Data Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Persistence
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnumVarPersist, CUITextEnum);

// Constructor

CUITextEnumVarPersist::CUITextEnumVarPersist(void)
{
	}

// Overridables

void CUITextEnumVarPersist::OnBind(void)
{
	CUITextEnum::OnBind();

	m_pVariable = (CControlVariable *) m_pItem;
	
	m_dwProject = m_pVariable->GetProject();
	}

void CUITextEnumVarPersist::OnRebind(void)
{
	CUITextEnum::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem;
	
	m_dwProject = m_pVariable->GetProject();
	}

// Overridables

CString CUITextEnumVarPersist::OnGetAsText(void)
{
	UINT d = m_pData->ReadInteger(m_pItem);

	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_Enum[n].m_Data == d ) {

			return m_Enum[n].m_Text;
			}
		}

	return L"<Unknown>";
	}

UINT CUITextEnumVarPersist::OnSetAsText(CError &Error, CString Text)
{
	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_Enum[n].m_Text == Text ) {

			UINT uPrev = m_pData->ReadInteger(m_pItem);

			UINT uData = m_Enum[n].m_Data;

			if( uData - uPrev ) {

				DWORD dwVariable = m_pVariable->GetHandle();

				DWORD    dwGroup = afxDatabase->FindGroup(m_dwProject, uPrev ? L"(Retain)" : L"(Global)");

				CStratonVariableSerializeBuffer Var(m_dwProject, dwGroup, dwVariable);

				if( Var.Serialize() && Var.DeleteVar() ) {

					DWORD dwGroup = afxDatabase->FindGroup(m_dwProject, uData ? L"(Retain)" : L"(Global)");

					DWORD dwIdent;

					if( (dwIdent = Var.Paste(dwGroup)) ) {
			
						m_pData->WriteInteger(m_pItem, uData);

						m_pVariable->OnNewIdent(dwIdent);

						CStratonProjectDescriptor(m_dwProject).Save();

						return saveChange;
						}
					}
					
				Error.SetUI(TRUE);

				Error.Set(L"Internal Error - failed to serialize variable");

				return saveError;
				}
			}
		}

	return saveSame;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Type
//

class CUITextEnumVarType : public CUITextEnumIndexed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnumVarType(void);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		DWORD		   m_dwProject;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		void LoadTypes(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Data Type
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnumVarType, CUITextEnumIndexed);

// Constructor

CUITextEnumVarType::CUITextEnumVarType(void)
{
	}

// Overridables

void CUITextEnumVarType::OnBind(void)
{
	CUITextEnumIndexed::OnBind();

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();

	LoadTypes();
	}

void CUITextEnumVarType::OnRebind(void)
{
	CUITextEnumIndexed::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();

	LoadTypes();
	}

UINT CUITextEnumVarType::OnSetAsText(CError &Error, CString Text)
{
	DWORD dwVariable = m_pVariable->GetHandle();

	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_Enum[n].m_Text == Text ) {

			UINT uPrev = m_pData->ReadInteger(m_pItem);

			UINT uData = m_Enum[n].m_Data;

			if( uData - uPrev ) {				
				
				CStratonVariableDescriptor Desc(m_dwProject, dwVariable);

				CStratonDataTypeDescriptor Type(m_dwProject, uData);

				if( Type.IsBasic() || Type.IsString() ) {

					Desc.m_dwType = uData;

					if( Desc.CanSet(Error) ) {
		
						Desc.Set();

						m_pData->WriteInteger(m_pItem, uData);

						CStratonProjectDescriptor(m_dwProject).Save();

						return saveChange;
						}

					Error.Printf(CString(IDS_FMT_IS_NOT_VALID), Text);
					
					return saveError;
					}

				Error.Printf(CString(IDS_FMT_IS_NOT_VALID), Text);

				return saveError;
				}

			return saveSame;
			}
		}

	return saveSame;
	}

// Implementation

void CUITextEnumVarType::LoadTypes(void)
{
	CStratonDataTypeDescriptor Type(m_dwProject, m_pVariable->m_TypeIdent);

	CLongArray Types;

	CStratonDataTypeHelper Help(m_dwProject);

	if( Type.IsBasic() || Type.IsString() ) {
	
		Help.SkipUdFb();

		Help.SkipStdFb();
		}

	Help.GetHandles(Types);
	
	UINT c;
	
	if( (c = Types.GetCount()) ) {

		for( UINT n = 0; n < c; n ++ ) {
		
			CStratonDataTypeDescriptor Type(m_dwProject, Types[n]);

			CEntry Enum;

			Enum.m_Data = Type.m_dwHandle;

			Enum.m_Text = Type.m_Name;

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}

		return;
		}

	CEntry Enum;

	Enum.m_Data = 0;

	Enum.m_Text = CString(IDS_UNKNOWN);

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable String Length
//

class CUITextVarStringLength : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarStringLength(void);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		DWORD		   m_dwProject;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Data Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		BOOL WriteData(UINT uData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable String Length
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarStringLength, CUITextInteger);

// Constructor

CUITextVarStringLength::CUITextVarStringLength(void)
{
	}

// Overridables

void CUITextVarStringLength::OnBind(void)
{
	CUITextInteger::OnBind();

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();
	}

void CUITextVarStringLength::OnRebind(void)
{
	CUITextInteger::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem;

	m_dwProject = m_pVariable->GetProject();
	}

CString CUITextVarStringLength::OnGetAsText(void)
{
	return CUITextInteger::OnGetAsText();
	}

UINT CUITextVarStringLength::OnSetAsText(CError &Error, CString Text)
{
	DWORD dwVariable = m_pVariable->GetHandle();

	UINT       uPrev = m_pData->ReadInteger(m_pItem);

	UINT       uData = Parse(Text);

	if( uData - uPrev ) {

		CStratonVariableDescriptor Desc(m_dwProject, dwVariable);

		Desc.m_dwLen = uData;

		if( Desc.CanSet(Error) ) {
			
			Desc.Set();

			m_pData->WriteInteger(m_pItem, uData);

			CStratonProjectDescriptor(m_dwProject).Save();

			return saveChange;
			}

		Error.Printf(CString(IDS_FMT_IS_NOT_VALID), Text);

		return saveError;
		}

	return saveSame;
	}

// End of File
