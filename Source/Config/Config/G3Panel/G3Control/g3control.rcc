
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "g3control.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#include "g3control.loc"

//////////////////////////////////////////////////////////////////////////
//
// Bitmaps
//

CtrlTreeIcon16 BITMAP "Images\\tree16.bmp"

//////////////////////////////////////////////////////////////////////////
//								
// Version Data
//

#include "version.rcc"

//////////////////////////////////////////////////////////////////////////
//
// String Table
//

STRINGTABLE
BEGIN
	IDS_ALL                 "All"
	IDS_BIT_TYPES           "64-bit Types"
	IDS_CHECK_LICENSE       "Check License"
	IDS_CHILD_SFC           "Child SFC Programs"
	IDS_CONTROL_CHILDREN    "Child Programs"
	IDS_CONTROL_GLOBALS     "Project Variables"
	IDS_CONTROL_OBJECT      "Control Object"
	IDS_CONTROL_PROGRAM     "Control Program"
	IDS_CONTROL_PROGRAMS    "Project Programs"
	IDS_CONTROL_PROJECT     "Project"
	IDS_CONTROL_PROJECT_2   "Control Project Build Errors"
	IDS_CONTROL_VARIABLE    "Control Variable"
	IDS_COPY_2              "Copy %1"
	IDS_COUNTERS            "Counters"
	IDS_CREATE              "Create %1"
	IDS_CUSTOM              "Custom"
	IDS_CUT_2               "Cut"
	IDS_CUT_3               "Cut %1"
	IDS_DATABASE_ITEM       "Database Item"
	IDS_DATATYPE            "Data-Type "
	IDS_DATA_SOURCE         "Data Source"
	IDS_DELETE_1            "Delete"
	IDS_DELETE_2            "delete"
	IDS_DELETE_3            "Delete %1"
	IDS_DESCRIPTION_FOR     "A description for this Function Block is not available."
	IDS_DO_YOU_WANT_TO      "Do you want to proceed?"
	IDS_EDIT_INITIAL        "Edit Initial Values"
	IDS_EXECUTION           "Execution"
	IDS_EXPORT_VARIABLE     "Export Variable Mappings"
	IDS_FMT_IS_NOT_USED     "%s is not used."
	IDS_FMT_IS_NOT_VALID    "%s is not a valid setting for this property."
	IDS_FUNCTION            "Function"
	IDS_FUNCTION_BLOCK      "Function Block"
	IDS_GLOBAL_RESULTS      "Global Results Search List. (Press F8 to view.)"
	IDS_HELP_IS_NOT         "Help is not available on this item."
	IDS_IEC_COMPLIANT       "IEC Compliant"
	IDS_INDICATES_INITIAL   "Indicates the initial value for this control variable array."
	IDS_INDICATES_INITIAL_2 "Indicates the initial value for this element of control variable array."
	IDS_INFORMATION         "Information"
	IDS_INITIAL_VALUE       "Initial Value"
	IDS_INITIAL_VALUE_IS    "The initial value is invalid."
	IDS_INPUT               "Input"
	IDS_INPUTOUTPUT         "Input/Output"
	IDS_INSTANCE_OF         "Instance of "
	IDS_JUMP_TO             "Jump To"
	IDS_JUMP_TO_SELECTED    "Jump To Selected"
	IDS_LARGEST             "The largest acceptable value is %s."
	IDS_LICENSE             "License"
	IDS_LICENSE_IS_NOT      "License is not installed"
	IDS_LOCAL_VARIABLES     "Local Variables"
	IDS_MAKE_FMT_PRIVATE    "Make %1 Private"
	IDS_MOVE_1              "Move"
	IDS_MOVE_2              "Move %1"
	IDS_MOVE_DOWN           "Move Down"
	IDS_MOVE_PROGRAM        "Move Program"
	IDS_MOVE_PROGRAM_DOWN   "Move the program down in the execution order"
	IDS_MOVE_PROGRAM_UP     "Move the program up in the execution order"
	IDS_MOVE_UP             "Move Up"
	IDS_NN                  "\n\n"
	IDS_NO_VARIABLES        "No Variables Mapped"
	IDS_OPERATOR            "Operator"
	IDS_OUTPUT              "Output"
	IDS_PARAMETERS          "Parameters"
	IDS_PASTE               "Paste"
	IDS_PLUS                "Plus!"
	IDS_PROGRAM             "The program conversion failed because the project contains errors.\n\nPress F4 or Shift+F4 to navigate through the errors.\n\nPress F8 to view the errors via the Global Results Search List."
	IDS_PROGRAMS            "Programs"
	IDS_PROGRAM_CANNOT_BE   "The program cannot be checked because of Used Defined Function Block errors.\n\nAll UDFBs must be error-free before other programs can be validated.\n\nPress F4 or Shift+F4 to navigate through the errors.\n\nPress F8 to view the errors via the Global Results Search List."
	IDS_PROGRAM_CONTAINS    "The program contains errors.\n\nPress F4 or Shift+F4 to navigate through the errors.\n\nPress F8 to view the errors via the Global Results Search List."
	IDS_PROGRAM_CONTAINS_2  "The program contains no syntax errors."
	IDS_PROGRAM_WITH_THIS   "A Program with this name already exists."
	IDS_PROJECT_BUILT       "The project built without errors."
	IDS_PROJECT_CONTAINS    "The project contains errors.\n\nPress F4 or Shift+F4 to navigate through the errors.\n\nPress F8 to view the errors via the Global Results Search List."
	IDS_PROJECT_NEEDS_TO    "The Project needs to be built to complete this operation."
	IDS_PROPERTIES          "Properties"
	IDS_READ_AND_WRITE      "Read and Write"
	IDS_READ_ONLY           "Read Only"
	IDS_RENAME              "Rename %1"
	IDS_SMALLEST            "The smallest acceptable value is %s."
	IDS_SOURCE              "Source"
	IDS_STANDARD            "Standard"
	IDS_STOPPING_ONLINE     "Stopping online debugging will leave the target system in a paused state.\n\nDo you wish to continue?"
	IDS_SUBPROGRAM          "Sub-Program."
	IDS_SYNTAX_ERRORS_FOR   "Syntax Errors for Control Program - %s"
	IDS_THE_NAME            "The name %1 is already in use." /* NOT USED */
	IDS_UNICODE_TEXT        "Unicode Text Files|*.txt|ANSI CSV Files|*.csv"
	IDS_UNKNOWN             "Unknown"
	IDS_USAGE_FOR_CONTROL   "Usage for Control Item %s"
	IDS_USAGE_OCURRENCES    "The usage ocurrences have been placed on the\n"
	IDS_USERDEFINED         "User-Defined Function Block."
	IDS_VALUE               "Value"
	IDS_VARIABLES           "Variables"
	IDS_VARIABLE_WITH       "A variable with this name already exists."
	IDS_WATCH_LIST_NOT      "Watch List Not Implemented."
END

//////////////////////////////////////////////////////////////////////////
//
// Prompt Table
//

STRINGTABLE
BEGIN
	IDM_CTRL_SYNTAX		"[Syntax Check]Check the current program for syntax errors."
	IDM_CTRL_BUILD		"[Build Project]Build the control project."
	IDM_CTRL_CLEAN		"[Clean Project]Clean the control project."
	IDM_CTRL_MAPPINGS	"[Show Variable Mappings]Show variable mappings."
END

//////////////////////////////////////////////////////////////////////////
//
// Control Cat Accelerators
//

ControlCatMenu ACCELERATORS
BEGIN
	"B",		IDM_CTRL_BUILD,		VIRTKEY,	CONTROL
	"T",		IDM_CTRL_SYNTAX,	VIRTKEY,	CONTROL
END

//////////////////////////////////////////////////////////////////////////
//
// Control Tree Accelerators
//

ControlTreeMenu ACCELERATORS
BEGIN
	"D",		IDM_EDIT_DUPLICATE,	VIRTKEY,	CONTROL
	"J",		IDM_GO_REFERENCE,	VIRTKEY,	ALT
END

//////////////////////////////////////////////////////////////////////////
//
// Control Tree Main Menu
//

ControlTreeMenu MENU
BEGIN
	POPUP "&Edit"
	BEGIN
		MENUITEM "&Smart Duplicate\tCtrl+D",	IDM_EDIT_SMART_DUP
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Control Tree Toolbar
//

ControlTreeTool MENU
BEGIN
	POPUP    "4000000E|New"
	BEGIN
	MENUITEM "Main Program",		IDM_CTRL_ADD_PROGRAM
	MENUITEM "Sub-Program",			IDM_CTRL_ADD_SUB_PROG
	MENUITEM "User-Defined Function Block",	IDM_CTRL_ADD_UDFB
/*	MENUITEM "Sfc Child Program",		IDM_CTRL_ADD_SFC_CHILD*/
	MENUITEM SEPARATOR
	MENUITEM "Variable",			IDM_CTRL_ADD_VARIABLE
	MENUITEM "Input",			IDM_CTRL_ADD_INPUT
	MENUITEM "Output",			IDM_CTRL_ADD_OUTPUT
	MENUITEM "Input/Output",		IDM_CTRL_ADD_INOUT
	END
	MENUITEM SEPARATOR
	MENUITEM "10000008",			IDM_ITEM_DELETE
/*	MENUITEM "10000033",			IDM_EDIT_SMART_DUP*/
/*	MENUITEM "1000000C",			IDM_ITEM_FIND*/
END

//////////////////////////////////////////////////////////////////////////
//
// Control Tree Context Menu (Hit)
//

ControlTreeCtxMenu MENU
BEGIN
	POPUP "CCM"
	BEGIN
		MENUITEM "Expand",			IDM_ITEM_EXPAND
		MENUITEM SEPARATOR
		POPUP	"New"
		BEGIN
		MENUITEM "Main Program",		IDM_CTRL_ADD_PROGRAM
		MENUITEM "Sub-Program",			IDM_CTRL_ADD_SUB_PROG
		MENUITEM "User-Defined Function Block",	IDM_CTRL_ADD_UDFB
		MENUITEM "SFC Child Program",		IDM_CTRL_ADD_SFC_CHILD
		MENUITEM SEPARATOR
		MENUITEM "Variable",			IDM_CTRL_ADD_VARIABLE
		MENUITEM "Input",			IDM_CTRL_ADD_INPUT
		MENUITEM "Output",			IDM_CTRL_ADD_OUTPUT
		MENUITEM "Input/Output",		IDM_CTRL_ADD_INOUT
		END
		MENUITEM SEPARATOR
		MENUITEM "Cut",				IDM_EDIT_CUT
		MENUITEM "Copy",			IDM_EDIT_COPY
		MENUITEM "Paste",			IDM_EDIT_PASTE
		MENUITEM "Delete",			IDM_ITEM_DELETE
		MENUITEM "Smart Duplicate",		IDM_EDIT_SMART_DUP
		MENUITEM SEPARATOR
		MENUITEM "Auto-Name",			IDM_CTRL_AUTONAME
		MENUITEM SEPARATOR
		MENUITEM "Cross-Reference",		IDM_CTRL_XREF
		MENUITEM SEPARATOR
		MENUITEM "Add to Watch List",		IDM_ITEM_WATCH
		MENUITEM SEPARATOR
		POPUP    "Convert"
		BEGIN
		MENUITEM "To Sequential Flow Chart",	IDM_CTRL_CONVERT_SFC
		MENUITEM "To Structured Text",		IDM_CTRL_CONVERT_ST
		MENUITEM "To Function Block Diagram",	IDM_CTRL_CONVERT_FBD
		MENUITEM "To Ladder Diagram",		IDM_CTRL_CONVERT_LD
		MENUITEM "To Instruction List",		IDM_CTRL_CONVERT_IL
		END
		MENUITEM SEPARATOR
		MENUITEM "Rename",			IDM_ITEM_RENAME
		MENUITEM SEPARATOR
		MENUITEM "Private Access",		IDM_ITEM_PRIVATE
		MENUITEM SEPARATOR
		MENUITEM "Exclude from Cycle",		IDM_CTRL_EXEC
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Control Tree Context Menu (Miss)
//

ControlTreeMissCtxMenu MENU
BEGIN
	POPUP "CCM"
	BEGIN
		MENUITEM "Create",		IDM_ITEM_NEW
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Category Menu
//

ControlCatMenu MENU
BEGIN
	POPUP "&Control"
	BEGIN
		MENUITEM "Check Syn&tax\tCtrl+T",	IDM_CTRL_SYNTAX
		MENUITEM SEPARATOR
		MENUITEM "&Build\tCtrl+B",		IDM_CTRL_BUILD
		MENUITEM "&Clean",			IDM_CTRL_CLEAN
		MENUITEM SEPARATOR
		MENUITEM "Show &Mappings",		IDM_CTRL_MAPPINGS
	END
END
	
//////////////////////////////////////////////////////////////////////////
//
// Category Toolbar
//

ControlCatTool MENU
BEGIN
	MENUITEM "41000000",			IDM_CTRL_SYNTAX
	MENUITEM "60000035",			IDM_CTRL_BUILD
	MENUITEM "60000036",			IDM_CTRL_CLEAN
	MENUITEM SEPARATOR
	POPUP "60000032|||Any"
	BEGIN
	MENUITEM "Debug Online",		IDM_CTRL_ONLINE
	MENUITEM "Simulate Offline",		IDM_CTRL_SIMULATE
	END
	MENUITEM "1000003B"			IDM_CTRL_STOP
	MENUITEM "60000033"			IDM_CTRL_PAUSE_RESUME
	MENUITEM "60000034"			IDM_CTRL_SINGLE_STEP
	MENUITEM SEPARATOR
END

//////////////////////////////////////////////////////////////////////////
//
// Function Library Accelerators
//

ControlLibAccel1 ACCELERATORS
BEGIN
	VK_F1,	IDM_RES_HELP,	VIRTKEY
END

//////////////////////////////////////////////////////////////////////////
//
// Function Library Context Menu
//

ControlLibCtxMenu MENU
BEGIN
	POPUP "FCM"
	BEGIN
		MENUITEM "Show Help Info...",	IDM_RES_HELP
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Function Library Toolbar
//

ControlLibraryTool MENU
BEGIN
	MENUITEM "1000000C",			0x87EE
	MENUITEM "1000000E",			IDM_RES_HELP
END

//////////////////////////////////////////////////////////////////////////
//
// Item Schemas
//

CControlObject_Schema RCDATA
BEGIN
	L"Ident,Handle,,ObjectHandle/EditBox,"
	L"Database handle for development only."
	L"\0"

	L"Name,Name,,ObjectName/EditBox,32,"
	L""
	L"\0"

	L"\0"
END

CGroupVariables_Schema RCDATA
BEGIN
	L"Name,Name,,GroupName/EditBox,32,"
	L""
	L"\0"

	L"\0"
END

CControlProject_Schema RCDATA
BEGIN
	L"Ident,Handle,,ProjHandle/EditBox,,"
	L"Database handle for development only."
	L"\0"

	L"Date,Date Stamp,,ProjDate/EditBox,32,"
	L"Date/Time stamp of the last project modification."
	L"\0"

	L"Path,Path,,ProjPath/EditBox,64,"
	L"Project path."
	L"\0"

	L"AutoName,Variable Naming Policy,,Enum/DropDown,Default|Use the Local Mapping Only|Follow the Data Tag,"
	L"Indicate whether Crimson should rename variable once is Data Source is mapped."
	L"\0"

	L"Enable,Program Execution,,Enum/DropDown,Do Not Execute|Execute Periodically,"
	L"Indicate the program execution scheme. Select Do not Execute to disable the execution of the control programs, "
	L"select Execute periodically to execute the programs at the rate determined by the Cycle Time."
	L"\0"

	L"InitHook,On Startup,,Coded/Action,|None,"
	L"Specify a Crimson program that will be executed when the unit starts up. This can "
	L"be specified without providing any IEC-61131 code to run a Crimson program on a "
	L"high-priority basis."
	L"\0"

	L"ExecHook,On Execute,,Coded/Action,|None,"
	L"Specify a Crimson program that will be executed at the start of each execution "
	L"scan. This can be specified without providing any IEC-61131 code to run a Crimson "
	L"program on a semi-deterministic and high-priority basis."
	L"\0"

	L"Exec,Execution Order,,ProgramExecution/ProgramExecution,,"
	L"\0"

	L"License,License Module,,Enum/DropDown,Ignore|Honor,"
	L"Indicate for modules that required the License Module, whether it should be ignored and the "
	L"control project be allowed to execute. Used for developement and evaluation purposes only."
	L"\0"

	L"StartMode,Start Mode,,ExprEnum/ExprDropEdit,Cold Start|Warm Start,"
	L"Select how the control project will start. Select Cold Start to use initial variable values. "
	L"Use Warm Start to use retained data values for those control variables that are retentive. "
	L"Map this to a retentive Data Tag to specify how the control project starts up at runtime."
	L"\0"

	L"\0"
END

CCycleTimeItem_Schema RCDATA
BEGIN
	#if defined(_DEBUG)
	L"Scan,Cycle Time,,CycleTime/EditBox,|0|ms|5|60000,"
	#else
	L"Scan,Cycle Time,,CycleTime/EditBox,|0|ms|100|60000,"
	#endif
	L"Select the rate at which the programs should be executed."
	L"\0"

	L"\0"
END

CControlProgram_Schema RCDATA
BEGIN
	L"Code,Source Code,,ControlProgram/ControlProgram,,"
	L"\0"

	L"Language,Language,,ProgLang/DropDown,1|Sequential Flow Chart|2|Structured Text|4|Function Block Diagram|8|Ladder Diagram|16|Instruction List,"
	L"\0"

	L"Section,Section,,ProgSect/DropDown,1|Begin|2|SFC|4|End|8|Child Sfc|16|Udfb,"
	L"\0"

	L"Parent,Parent,,ProgParent/DropDown,,"
	L"\0"

	L"\0"
END

CControlMainProgram_Schema RCDATA
BEGIN
	L"Enable,Execution,,ProgExec/DropDown,0|Never Called|1|Called on Each Cycle|2|Called Periodically,"
	L"Indicate the calling schedule for this program. Select Never called to omit this program from "
	L"being included in the project, select Called on each cycle to indicate the program is to be "
	L"executed in the main cycle in the order specified, select Called periodically to indicate the "
	L"program is to be included in the scan but called periodically."
	L"\0"

	L"Period,Period,,ProgSched/EditBox,||cycles|1|100|p,"
	L"Indicate the number of cycles between executions of this program."
	L"\0"

	L"Offset,Phase,,ProgSched/EditBox,|||||o,"
	L"Indicate the offset or phase."
	L"\0"

	L"\0"
END

CControlCallProgram_Schema RCDATA
BEGIN
	L"Enable,Execution,,ProgExec/DropDown,3|Called by another program,"
	L"This program can only be called from a program."
	L"\0"

	L"\0"
END

CControlSfcMainProgram_Schema RCDATA
BEGIN
	L"Enable,Execution,,ProgExec/DropDown,0|Never Called|1|Called on each cycle,"
	L"Indicate the calling schedule for this program. Select Never called to omit this program from "
	L"being included in the project, select Called on each cycle to indicate the program is to be "
	L"executed in the main cycle in the order specified."
	L"\0"

	L"\0"
END

CControlSfcChildProgram_Schema RCDATA
BEGIN
	L"Enable,Execution,,ProgExec/DropDown,3|Called by another program,"
	L"This program can only be called from a SFC program."
	L"\0"

	L"\0"
END

CControlVariable_Schema RCDATA
BEGIN
	L"Value,Source,,Coded/TagControl,,"
	L"Specify the value of this variable. This property can also be set to Internal "
	L"to create storage within the Crimson device."
	L"\0"

	L"Extent,Extent,,VarExtent/TagExtent,0|0||0|1024,"
	L"Specify whether this variable should represent a single value or an expression "
	L"that is accessed as an array, using the index to select items."
	L"\0"

	L"Persist,Storage,,EnumVarPersist/DropDown,Non-Retentive|Retentive,"
	L"Indicate whether the value of this variable should be stored within the Crimson device "
	L"between power cycles, or whether the value should reset to zero on startup."
	L"\0"

	L"TypeIdent,Data Type,,EnumVarType/DropDown,,"
	L"Select the data-type that for this control variable."
	L"\0"

	L"Length,String Length,,VarStringLength/EditBox,|0||1|255,"
	L"Select the string length."
	L"\0"

	L"\0"
END

CControlGlobalVariable_Schema RCDATA
BEGIN
	L"Flags,Flags,,VarAttr/DropDown,a,"
	L"Indicates the style flags for this control variable."
	L"\0"

	L"\0"
END

CControlLocalVariable_Schema RCDATA
BEGIN
	L"Flags,Flags,,VarAttr/DropDown,a,"
	L"Indicates the style flags for this control variable."
	L"\0"

	L"\0"
END

CControlInOutVariable_Schema RCDATA
BEGIN
	L"Flags,Attribute,,VarAttr/DropDown,t|!,"
	L"Indicates the style flags for this control variable."
	L"\0"

	L"\0"
END

CVariableInitialSingleBoolean_Schema RCDATA
BEGIN
	L"Data,Initial Value,,VarInitEnumCommit/DropDown,FALSE|TRUE,"
	L"Indicates the initial value for this control variable."
	L"\0"

	L"\0"
END

CVariableInitialSingleString_Schema RCDATA
BEGIN
	L"Data,Initial Value,,VarInitStringCommit/EditBox,128||64|string,"
	L"Indicates the initial value for this control variable."
	L"\0"

	L"\0"
END

CVariableInitialSingleNumber_Schema RCDATA
BEGIN
	L"Data,Initial Value,,VarInitStringCommit/EditBox,16||16|number,"
	L"Indicates the initial value for this control variable."
	L"\0"

	L"\0"
END

CVariableInitialSingleTime_Schema RCDATA
BEGIN
	L"Data,Initial Value,,VarInitStringCommit/EditBox,32||32|time,"
	L"Indicates the initial value for this control variable."
	L"\0"

	L"\0"
END

CVariableInitialSingleReal_Schema RCDATA
BEGIN
	L"Data,Initial Value,,VarInitStringCommit/EditBox,32||32|real,"
	L"Indicates the initial value for this control variable."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Page Schemas
//

CGroupObjects_Page RCDATA
BEGIN
	L"\0"
END

CControlProject_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Straton Database,Ident,Date,Path\0"
	L"\0"
END

CCycleTimeItem_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Properties,Scan\0"
	L"\0"
END

CControlProgram_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Program Code,Code\0"
	L"\0"
END

CControlProgram_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Straton Database,Ident,Name,Language,Section,Parent\0"
	L"\0"
END

CControlProgram_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Schedule,Enable\0"
	L"\0"
END

CControlMainProgram_Page2 RCDATA
BEGIN
	L"G:1,root,Schedule,Enable,Period,Offset\0"
	L"\0"
END

CControlGlobalVariable_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data Source,Value\0"
	L"G:1,root,Data Type,TypeIdent,Length,Extent\0"
	L"G:1,root,Properties,Persist,Flags\0"
	L"\0"
END

CControlLocalVariable_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data Source,Value\0"
	L"G:1,root,Data Type,TypeIdent,Length,Extent\0"
	L"G:1,root,Properties,Flags\0"
	L"\0"
END

CControlLocalVariable_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data Type,TypeIdent,Length,Extent\0"
	L"G:1,root,Properties,Flags\0"
	L"\0"
END

CControlInOutVariable_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data Type,TypeIdent,Length,Extent\0"
	L"G:1,root,Properties,Flags\0"
	L"\0"
END

CControlVariable_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Straton Database,Ident,Name\0"
	L"\0"
END

CVariableInitialSingle_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Initial Value,Data\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// License Dialog
//

LicenseDlg DIALOG 0, 0, 0, 0
CAPTION "Control Module"
BEGIN
	GROUPBOX	"Warning",	-1, 4, 4, 180, 42
	CONTROL		" Do not show this warning again. ", 101, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 50, 168, 14 	
	DEFPUSHBUTTON   "OK", IDOK,      4, 70, 40, 14, XS_BUTTONFIRST
	LTEXT		"The control project will only execute if you have the Control", 1000, 14, 18, 168, 10
	LTEXT		"Module physically installed in your target device.", 1001, 14, 28, 168, 10
END

//////////////////////////////////////////////////////////////////////////
//
// Warning Dialog
//

WarningDlg DIALOG 0, 0, 0, 0
CAPTION "Download Warning"
BEGIN
	GROUPBOX	"Warning",	-1, 4, 4, 180, 52
//	CONTROL		" Do not show this warning again. ", 101, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 50, 168, 14 	
	DEFPUSHBUTTON   "&Continue", IDOK,      4, 60, 40, 14, XS_BUTTONFIRST
	PUSHBUTTON	"&Abort",      IDCANCEL,  48, 60,  40,  14, XS_BUTTONREST
	LTEXT		"THE SYSTEM IS ABOUT TO SHUTDOWN", 1000, 14, 18, 168, 10
	LTEXT		"You must ensure the system is in a safe state before proceeding", 1001, 14, 28, 168, 10
	LTEXT		"with downloading this database.", 1002, 14, 38, 168, 10
END

//////////////////////////////////////////////////////////////////////////
//								
// Property Selection Dialog
//

VarMapDialog DIALOG 0, 0, 0, 0
CAPTION "Control Variable Mappings"
BEGIN
	GROUPBOX	"Properties",  100,        4,   4, 208, 240
	CONTROL		"Export",      101, "CHotLinkCtrl", WS_TABSTOP, 8, 248, 120, 14 
	DEFPUSHBUTTON	"OK",	       IDOK,       4, 268,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",      IDCANCEL,  48, 268,  40,  14, XS_BUTTONREST
END

// End of File
