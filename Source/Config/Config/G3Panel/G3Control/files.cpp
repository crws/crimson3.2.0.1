
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Runtime Class

AfxImplementRuntimeClass(CControlFile, CMetaItem);

// Constructor

CControlFile::CControlFile(void)
{
	}

// Attributes

BOOL CControlFile::IsEmpty(void) const
{
	return m_Blob.IsEmpty();
	}

// Operations

void CControlFile::ChangeBase(CFilename Name)
{
	AfxAssert(!m_Name.IsEmpty());

	m_Name = Name.WithType(m_Name.GetType());

	SaveToDisk();
	}

void CControlFile::SetFullName(CFilename Name)
{
	m_Name = Name;
	}

BOOL CControlFile::SaveToDisk(void)
{
	if( m_Blob.GetCount() > 0 ) {

		CFilename File = m_pProject->GetProjectPath().WithName(m_Name);

		if( File.CreatePath() ) {

			HANDLE hFile = File.OpenWrite();

			if( hFile != INVALID_HANDLE_VALUE ) {

				FettleBlobOnSave();

				PCBYTE pData = PCBYTE(m_Blob.GetPointer());

				UINT   uData = m_Blob.GetCount();

				DWORD  uDone = 0;

				WriteFile(hFile, pData, uData, &uDone, NULL);

				CloseHandle(hFile);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CControlFile::LoadFromDisk(void)
{
	CFilename File  = m_pProject->GetProjectPath().WithName(m_Name);

	HANDLE    hFile = File.OpenReadSeq();

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT  uData = GetFileSize(hFile, NULL);

		PBYTE pData = New BYTE [ uData ];
		
		DWORD uDone = 0;

		ReadFile(hFile, pData, uData, &uDone, NULL);

		if( uDone == uData ) {

			m_Blob.Empty();

			m_Blob.Append(pData, uData);

			FettleBlobOnLoad();

			delete [] pData;

			CloseHandle(hFile);

			return TRUE;
			}

		delete [] pData;

		CloseHandle(hFile);
		}

	return FALSE;
	}

// Persistance

void CControlFile::Init(void)
{
	CMetaItem::Init();

	FindProject();
	}

void CControlFile::Load(CTreeFile &File)
{
	CMetaItem::Load(File);

	if( !m_Path.IsEmpty() ) {

		m_Name = m_Path.GetName();

		m_Path.Empty();
		}

	if( !m_Data.IsEmpty() ) {

		UINT c;

		m_Blob.Expand(c = m_Data.GetCount());

		for( UINT n = 0; n < c; n++ ) {

			m_Blob.Append(LOBYTE(m_Data[n]));

			AfxAssert(!HIBYTE(m_Data[n]));
			}

		m_Data.Empty();
		}

	FindProject();
	}

void CControlFile::PreCopy(void)
{
	CMetaItem::PreCopy();
	}

void CControlFile::Kill(void)
{
	CMetaItem::Kill();
	}

// Property Filters

BOOL CControlFile::SaveProp(CString const &Tag) const
{
	if( Tag == "Path" || Tag == "Data" ) {

		// Obsolete Properties

		return FALSE;
		}

	return CMetaItem::SaveProp(Tag);
	}

// Overridables

void CControlFile::FettleBlobOnLoad(void)
{
	}

void CControlFile::FettleBlobOnSave(void)
{
	}

// Meta Data Creation

void CControlFile::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString(Name);
	Meta_AddString(Path);
	Meta_AddBlob  (Blob);
	Meta_AddWide  (Data);
	}

// Implementation

void CControlFile::FindProject(void)
{
	m_pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));
	}

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Runtime Class

AfxImplementRuntimeClass(CProjectFile, CControlFile);

// Constructor

CProjectFile::CProjectFile(PCTXT pSuffix)
{
	m_Suffix = CString(pSuffix);
	}

// Persistance

void CProjectFile::Init(void)
{
	CControlFile::Init();

	m_Name = m_pProject->GetGlobalFilePath(m_Suffix).GetName();
	}

// Overridables

void CProjectFile::FettleBlobOnLoad(void)
{
	if( m_Suffix == L".k5p" ) {

		// We strip out the "/A" lines that reference handles
		// in the database as these create changes between loads
		// and break incremental download. Straton doesn't seem
		// to mind and puts them straight back in again...

		PCSTR pData = PCSTR(m_Blob.GetPointer());

		UINT  uData = m_Blob.GetCount();

		CString Work;

		CString Text(pData, uData);

		CStringArray Lines;

		Text.Tokenize(Lines, '\n');

		for( UINT l = 0; l < Lines.GetCount(); l++ ) {

			CString const &Line = Lines[l];

			if( Line.StartsWith(L"/A,") ) {

				if( Line[4] > '1' ) {

					continue;
					}
				}

			Work += Line;
			
			Work += '\n';
			}

		m_Blob.Empty();

		m_Blob.Expand(uData = Work.GetLength());

		for( UINT n = 0; n < uData; n++ ) {

			m_Blob.Append(LOBYTE(Work[n]));

			AfxAssert(!HIBYTE(Work[n]));
			}

		return;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Dynamic Class

AfxImplementDynamicClass(CExecuteFile, CProjectFile);

// Constructor

CExecuteFile::CExecuteFile(void) : CProjectFile(L".xti")
{
	m_Handle = HANDLE_NONE;
	}

// Download Support

BOOL CExecuteFile::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_STRATON);

	CProjectFile::MakeInitData(Init);

	if( LoadFromDisk() ) {

		Init.AddWord(WORD(m_Handle));

		UINT   uData = m_Blob.GetCount();

		PCBYTE pData = PBYTE(m_Blob.GetPointer());

		Init.AddLong(uData);

		Init.AddData(pData, uData);

		return TRUE;
		}

	Init.AddWord(0xFFFF);

	return TRUE;
	}

// Meta Data Creation

void CExecuteFile::AddMetaData(void)
{
	CProjectFile::AddMetaData();

	Meta_AddInteger(Handle);
	}

// Overridables

void CExecuteFile::FettleBlobOnLoad(void)
{
	UINT  uData = m_Blob.GetCount();

	PBYTE pData = PBYTE(m_Blob.GetPointer());

	// Clobber the project name

	memset(pData + 0x60, 42, 7);

	// Clobber time elements that change between builds.

	PDWORD(pData + 0x70)[0] = 0; // Time Stamp
	PWORD (pData + 0x82)[0] = 0; // Generation
	PDWORD(pData + 0x84)[0] = 0; // Time Stamp

	// Clobber the first CRC

	PDWORD(pData+uData-8)[0] = 0xAAAA;

	// Fix and replace the 2nd CRC

	PDWORD(pData+uData-4)[0] = StratonCRC32(pData, uData-4);
	}

// Implementation

DWORD CExecuteFile::StratonCRC32(PCBYTE pData, UINT uData)
{
	DWORD crc = 0xFFFFFFFF;

	while( uData-- ) {

		crc ^= *pData++;

		for( int i = 0; i < 8; i++ ) {

			crc = (crc & 1) ? ((crc>>1) ^ 0xA000A001) : (crc>>1);
			}
		}

	return crc;
	}

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Dynamic Class

AfxImplementDynamicClass(CCompileFile, CProjectFile);

// Constructor

CCompileFile::CCompileFile(void) : CProjectFile(L".cpo")
{
	}

// Persistance

void CCompileFile::Init(void)
{
	CProjectFile::Init();

	SetFixedContent();
	}

void CCompileFile::PostLoad(void)
{
	CProjectFile::Init();

	SetFixedContent();
	}

// Implementation

void CCompileFile::SetFixedContent(void)
{
	PCSTR  pText = GetFixedContent();

	UINT   uSize = strlen(pText);

	PCBYTE pData = PCBYTE(pText);

	m_Blob.Empty();

	m_Blob.Append(pData, uSize);
	}

PCSTR CCompileFile::GetFixedContent(void)
{
	PCSTR pText =   "[OPTIONS]\r\n"
			"SIMUL=ON\r\n"
			"DEBUG=ON\r\n"
			"CTSEG=OFF\r\n"
			"FBDFLOW=OFF\r\n"
			"EMBEDSYMBOLS=ON\r\n"
			"EMBEDSYBCASE=ON\r\n"
			"WARNING=OFF\r\n"
			"SFCSAFE=OFF\r\n"
			"FBDOPTIM=OFF\r\n"
			"LDOPTIM=OFF\r\n"
			"CHECKSYBCONFLICTS=OFF\r\n"
			"STSTRICT=OFF\r\n"
			"IECCHECK=OFF\r\n"
			"TRACEPOUSIZE=OFF\r\n"
			"CHECKPROFILE=OFF\r\n"
			"SAFEARRAY=ON\r\n"
			"VSI=OFF\r\n"
			"FBCHECK=OFF\r\n"
			"NOIOSTEP=OFF\r\n"
			"RTRIGNO0=OFF\r\n"
			"LOADUPL=OFF\r\n"
			"CHECKFBDINPUTS=ON\r\n"
			"UPDEXT=OFF\r\n"
			"EXTDEF=OFF\r\n"
			"PASSWORD=\r\n"
			"MWLOG=OFF\r\n"
			"VMHEAP=0\r\n"
			"LOCK=NONE\r\n"
			"CC=\r\n"
			"CCTOOLDIR=\r\n"
			"CCTARGET=\r\n"
			"CCPURE=OFF\r\n"
			"CCLOAD=OFF\r\n"
			"CCSHOW=ON\r\n"
			"CCHOT=OFF\r\n"
			"TRACE=OFF\r\n"
			"TRACETIME=OFF\r\n"
			"\r\n"
			"[SIMULCODE]\r\n"
			"TARGET=T5SIMUL\r\n"
			"SUFFIX=.XWS\r\n"
			"MOTOROLAENDIAN=OFF\r\n"
			"T5STYLE=ON\r\n"
			"\r\n"
			"[TARGETCODE]\r\n"
			"TARGET=T5RTI\r\n"
			"SUFFIX=.XTI\r\n"
			"MOTOROLAENDIAN=OFF\r\n"
			"T5STYLE=ON\r\n"
			"LCONFIGNAME=T5RTI\r\n"
			"COMMENT=Straton T5 runtime (Intel like byte ordering)\r\n"
			;

	return pText;
	}

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Runtime Class

AfxImplementRuntimeClass(CProgramFile, CControlFile);

// Constructor

CProgramFile::CProgramFile(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Runtime Class

AfxImplementRuntimeClass(CSourceFile, CProgramFile);

// Constructor

CSourceFile::CSourceFile(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

// Runtime Class

AfxImplementRuntimeClass(CDefineFile, CProgramFile);

// Constructor

CDefineFile::CDefineFile(void)
{
	}

// End of File
