
#include "intern.hpp"

#include "progview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Program Editor View
//

class CControlProgramEditorViewWnd : public CUIItemViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Cnstructor
		CControlProgramEditorViewWnd(CUIPage *pPage);

	protected:
		// Overribables
		BOOL OnNavigate(CString const &Nav);
	};

/////////////////////////////////////////////////////////////////////////
//
// Program Editor View
//

// Runtime Class

AfxImplementRuntimeClass(CControlProgramEditorViewWnd, CUIItemViewWnd);

// Constructor

CControlProgramEditorViewWnd::CControlProgramEditorViewWnd(CUIPage *pPage) : CUIItemViewWnd(pPage, FALSE)
{
	SetBorder(6);
	}

// Overribables

BOOL CControlProgramEditorViewWnd::OnNavigate(CString const &Nav)
{
	if( CUIItemViewWnd::OnNavigate(Nav) ) {

		UINT   uPos = Nav.Find(L'!');

		CString Src = Nav.Left(uPos);

		CString Loc = Nav.Mid (uPos+1);

		if( m_pFocus ) {

			if( m_pFocus->IsKindOf(AfxRuntimeClass(CControlProgramCtrlWnd)) ) {

				CControlProgramCtrlWnd *pWnd = (CControlProgramCtrlWnd *) m_pFocus;				

				pWnd->SetFocus();

				pWnd->LocateError(Loc);
				}		
			}
		
		return TRUE;
		}

	return FALSE;
	}

/////////////////////////////////////////////////////////////////////////
//
// Program Item View
//

// Runtime Class

AfxImplementRuntimeClass(CControlProgramView, CUIItemMultiWnd);

// Constructor

CControlProgramView::CControlProgramView(void) : CUIItemMultiWnd(NULL, FALSE)
{
	m_pList     = New CUIPageList;

	CLASS Class = AfxRuntimeClass(CControlProgram);
	
	m_pList->Append(New CUIStdPage(CString(IDS_SOURCE),      Class, 1));

	m_pList->Append(New CUIStdPage(CString(IDS_PROPERTIES),  Class, 2));

	#if defined(_DEBUG)

	m_pList->Append(New CUIStdPage(CString(IDS_INFORMATION), Class, 3));

	#endif
	}

// Overridables

void CControlProgramView::OnAttach(void)
{
	FindItem();

	m_pList->Replace(1, New CUIStdPage(CString(IDS_PROPERTIES), AfxPointerClass(m_pItem), 2));

	m_pItem = (CControlProgram *) CUIItemMultiWnd::m_pItem;

	AttachViews();
	}

void CControlProgramView::OnExec(CCmd *pCmd)
{
	if( pCmd->IsKindOf(AfxRuntimeClass(CEditorWnd::CCmdEdit)) ) {
		
		m_pEditor->Exec(pCmd);
		}

	CUIItemMultiWnd::OnExec(pCmd);
	}

void CControlProgramView::OnUndo(CCmd *pCmd)
{
	if( pCmd->IsKindOf(AfxRuntimeClass(CEditorWnd::CCmdEdit)) ) {
		
		m_pEditor->Undo(pCmd);
		}

	CUIItemMultiWnd::OnUndo(pCmd);
	}

BOOL CControlProgramView::OnNavigate(CString const &Nav)
{
	return CUIItemMultiWnd::OnNavigate(Nav);
	}

// Message Map

AfxMessageMap(CControlProgramView, CUIItemMultiWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SETCURRENT)

	AfxMessageEnd(CControlProgramView)
	};

// Message Handlers

void CControlProgramView::OnPostCreate(void)
{
	CUIItemMultiWnd::OnPostCreate();

	FindEditor();
	}

void CControlProgramView::OnSetCurrent(BOOL fCurrent)
{
	// !!!!
	}

// View Creation

CViewWnd * CControlProgramView::CreateViewObject(UINT n, CUIPage *pPage)
{
	if( n == 0 ) {

		return New CControlProgramEditorViewWnd(pPage);
		}

	return New CUIItemViewWnd(pPage, m_fScroll);
	}

// Implementation

BOOL CControlProgramView::FindEditor(void)
{
	if( IsWindow() ) {

		CWnd *pWnd   = m_Views[0];

		CWnd *pChild = pWnd->GetWindowPtr(GW_CHILD);

		while( pChild->IsWindow() ) {

			if( pChild->IsKindOf(AfxRuntimeClass(CControlProgramCtrlWnd)) ) {

				m_pEditor = (CControlProgramCtrlWnd *) pChild;

				return TRUE;
				}

			pChild = pChild->GetWindowPtr(GW_HWNDNEXT);
			}
		}

	return FALSE;
	}

// End of File
