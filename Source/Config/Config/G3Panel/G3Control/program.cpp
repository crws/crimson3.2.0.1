
#include "intern.hpp"

#include "progview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program Code
//

// Dynamic Class

AfxImplementDynamicClass(CControlProgramCode, CItem);

// Constructor

CControlProgramCode::CControlProgramCode(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program
//

// Dynamic Class

AfxImplementDynamicClass(CControlProgram, CControlObject);

// Constructor

CControlProgram::CControlProgram(void)
{
	m_Language    = 1;

	m_Section     = 1;

	m_Parent      = 0;

	m_Private     = 0;

	m_pCode       = New CControlProgramCode;

	m_pLocals     = New CLocalVariables;

	m_pSourceFile = New CSourceFile;

	m_pDefineFile = New CDefineFile;

	m_pLocalsFile = New CDefineFile;

	m_pProgram    = NULL;
	}

// Destructor

CControlProgram::~CControlProgram(void)
{
	if( m_pProgram ) {

		delete m_pProgram;
		
		m_pProgram = NULL;
		}
	}

// Development

void CControlProgram::AddTest(void)
{
	m_pLocals->AddTest();
	}

// Overridables

UINT CControlProgram::GetTreeImage(void) const
{
	return IDI_PROGRAM;
	}

void CControlProgram::Validate(void)
{
	m_pLocals->Validate();
	}

// Attributes

CLASS CControlProgram::GetEditorClass(void)
{
	switch( m_Language ) {

		case langSFC:
			return AfxNamedClass(L"CSequentialFlowChartEditorWnd"); 

		case langST:
			return AfxNamedClass(L"CStructuredTextEditorWnd");

		case langFBD:
			return AfxNamedClass(L"CFunctionBlockDiagramEditorWnd"); 

		case langLD:
			return AfxNamedClass(L"CLadderDiagramEditorWnd"); 

		case langIL:
			return AfxNamedClass(L"CStructuredTextEditorWnd");

		default:
			return AfxNamedClass(L"CDummyEditorWnd");
		}
	}

DWORD CControlProgram::GetHandle(void)
{
	AfxAssert(m_pProgram);

	return m_pProgram ? m_pProgram->GetHandle() : 0;
	}

DWORD CControlProgram::GetProject(void)
{
	AfxAssert(m_pProgram);

	return m_pProgram ? m_pProgram->GetProject() : 0;
	}

DWORD CControlProgram::GetLocalGroup(void)
{
	AfxAssert(m_pProgram);

	return afxDatabase->FindGroup(GetProject(), m_Name);
	}

CString	CControlProgram::GetStoragePath(void)
{
	AfxAssert(m_pProgram);

	return m_pProgram ? m_pProgram->GetProgramPath(0) : CString();
	}

CStratonObject * CControlProgram::GetProgram(void)
{
	return m_pProgram;
	}

// Overridables

void CControlProgram::OnConvert(void)
{
	DWORD dwProject = m_pProject->GetHandle();

	DWORD dwProgram = m_pProgram->GetHandle();

	CStratonProgramDescriptor Desc(dwProject, dwProgram);

	m_Language = Desc.m_dwLanguage;

	m_Section  = Desc.m_dwSection;

	SetDirty();
	}

BOOL CControlProgram::OnRename(CError &Error, CString Name)
{
	CProgramNamer Prog(GetProject(), m_Language, m_Section);

	if( Prog.Validate(Error, Name) ) {

		if( m_pProgram->Rename(Error, Name) ) {

			SetName(Name);

			m_pLocals->SetName(Name);

			m_pSourceFile->SetFullName(m_pProgram->GetProgramName(0));

			m_pDefineFile->SetFullName(m_pProgram->GetProgramName(1));

			m_pLocalsFile->SetFullName(m_pProgram->GetProgramName(2));
		
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CControlProgram::CanRename(CError &Error, CString Name)
{
	CProgramNamer Prog(GetProject(), m_Language, m_Section);

	if( Prog.Validate(Error, Name) ) { 

		if( m_pProgram->CanRename(Name) ) {
			
			return TRUE;
			}

		Error.Set(CString(IDS_PROGRAM_WITH_THIS));
		}

	return FALSE;
	}

// Item Lookup

BOOL CControlProgram::FindVariable(CControlVariable * &pVariable, CString Name)
{
	return m_pLocals->FindVariable(pVariable, Name);
	}

BOOL CControlProgram::FindVarGroup(CGroupVariables * &pGroup, CString Name)
{
	if( m_pLocals->m_Name == Name ) {
		
		pGroup = m_pLocals;

		return TRUE;
		}

	return FALSE;
	}

// UI Creation

CViewWnd * CControlProgram::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return New CControlProgramView;
		}

	return NULL;
	}

// UI Update

void CControlProgram::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CControlObject::OnUIChange(pHost, pItem, Tag);
	}

// Overridable

void CControlProgram::SetPrivate(UINT uHide)
{
	CControlObject::SetPrivate(uHide);

	m_pLocals->SetPrivate(uHide);
	}

// Persistance

void CControlProgram::Init(void)
{		
	CControlObject::Init();

	Bind();
	}

void CControlProgram::PreCopy(void)
{
	CControlObject::PreCopy();
	}

void CControlProgram::PostPaste(void)
{
	CControlObject::PostPaste();

	m_pLocals->SetName(m_Name);
	}

void CControlProgram::PostLoad(void)
{	
	Bind();

	CControlObject::PostLoad();
	}

void CControlProgram::Save(CTreeFile &File)
{
//	m_pProgram->Save();

	m_pSourceFile->LoadFromDisk();

	m_pDefineFile->LoadFromDisk();

	m_pLocalsFile->LoadFromDisk();
	
	CControlObject::Save(File);
	}

void CControlProgram::Kill(void)
{
	CControlObject::Kill();

	if( m_pProgram ) {

		m_pProgram->Delete();
		}
	}

void CControlProgram::Load(CTreeFile &File)
{
	CControlObject::Load(File);

	m_pSourceFile->SaveToDisk();

	m_pDefineFile->SaveToDisk();

	m_pLocalsFile->SaveToDisk();
	}

// Meta Data Creation

void CControlProgram::AddMetaData(void)
{
	CControlObject::AddMetaData();

	Meta_AddInteger(Language);
	Meta_AddInteger(Section);
	Meta_AddInteger(Parent);
	Meta_AddInteger(Private);
	Meta_AddObject (SourceFile);
	Meta_AddObject (DefineFile);
	Meta_AddObject (LocalsFile);
	Meta_AddObject (Code);
	Meta_AddObject (Locals);

	Meta_SetName((IDS_CONTROL_PROGRAM));
	}

// Binding

void CControlProgram::Bind(void)
{
	if( !m_pProgram && !m_Name.IsEmpty() ) {

		DWORD dwProject = m_pProject->GetHandle();

		m_pProgram      = New CStratonProgram;

		m_pProgram->SetProject(dwProject);

		if( m_pProgram->Connect(m_Name) ) {

			m_pSourceFile->ChangeBase(m_Name);

			m_pDefineFile->ChangeBase(m_Name);

			m_pLocalsFile->ChangeBase(m_Name);

			CStratonProgramDescriptor Prog(dwProject, GetHandle());

			m_Parent = Prog.m_dwParent;
			}
		else {
			if( m_pProgram->Create(m_Name, m_Language, m_Section, m_Parent) ) {

				if( !m_pSourceFile->IsEmpty() ) {
		
					m_pSourceFile->ChangeBase(m_Name);

					m_pDefineFile->ChangeBase(m_Name);

					m_pLocalsFile->ChangeBase(m_Name);
					}
				else {
					m_pSourceFile->SetFullName(m_pProgram->GetProgramName(0));
		
					m_pDefineFile->SetFullName(m_pProgram->GetProgramName(1));

					m_pLocalsFile->SetFullName(m_pProgram->GetProgramName(2));
					}
				}
			else {
				delete m_pProgram;

				m_pProgram = NULL;

				AfxAssert(FALSE);
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program - Main Program
//

// Dynamic Class

AfxImplementDynamicClass(CControlMainProgram, CControlProgram);

// Constructor

CControlMainProgram::CControlMainProgram(void)
{
	m_Enable = 1;

	m_Period = 1;

	m_Offset = 0;
	}

// UI Update

void CControlMainProgram::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"Enable" ) {

		pHost->SendUpdate(updateProps);

		DoEnables(pHost);
		}

	CControlProgram::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

UINT CControlMainProgram::GetTreeImage(void) const
{
	return m_Enable > 0 ? IDI_PROGRAM_ENABLED : IDI_PROGRAM_DISABLED;
	}

// Persistance

void CControlMainProgram::Init(void)
{
	CControlProgram::Init();

	DWORD dwProject = m_pProject->GetHandle();

	DWORD dwProgram = m_pProgram->GetHandle();

	CStratonProperties Props(dwProject, dwProgram);

	Props.Set(propProgCheck, L"#TRUE#");
	}

// Meta Data Creation

void CControlMainProgram::AddMetaData(void)
{
	CControlProgram::AddMetaData();

	Meta_AddInteger(Enable);
	Meta_AddInteger(Period);
	Meta_AddInteger(Offset);

	Meta_SetName((IDS_CONTROL_PROGRAM));
	}

// Implementation

void CControlMainProgram::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_Enable == 2;

	pHost->EnableUI(L"Period", fEnable);
	pHost->EnableUI(L"Offset", fEnable);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program - SubProgram
//

// Dynamic Class

AfxImplementDynamicClass(CControlCallProgram, CControlProgram);

// Constructor

CControlCallProgram::CControlCallProgram(void)
{
	m_Enable  = 3;

	m_pParams = New CInOutVariables;
	}

// Overridables

BOOL CControlCallProgram::OnRename(CError &Error, CString Name)
{
	if( CControlProgram::OnRename(Error, Name) ) {

		m_pParams->SetName(Name);
		
		return TRUE;
		}

	return FALSE;
	}

// Item Lookup

BOOL CControlCallProgram::FindVariable(CControlVariable * &pVariable, CString Name)
{
	return m_pLocals->FindVariable(pVariable, Name) ||
	       m_pParams->FindVariable(pVariable, Name) ;;
	}

BOOL CControlCallProgram::FindVarGroup(CGroupVariables * &pGroup, CString Name)
{
	if( m_pLocals->m_Name == Name ) {

		pGroup = m_pLocals;

		return TRUE;
		}

	if( m_pParams->m_Name == Name ) {

		pGroup = m_pParams;

		return TRUE;
		}

	return FALSE;
	}

// UI Update

void CControlCallProgram::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CControlProgram::OnUIChange(pHost, pItem, Tag);
	}

// Overridable

void CControlCallProgram::SetPrivate(UINT uHide)
{
	CControlProgram::SetPrivate(uHide);

	m_pParams->SetPrivate(uHide);
	}

// Meta Data Creation

void CControlCallProgram::AddMetaData(void)
{
	CControlProgram::AddMetaData();

	Meta_AddInteger(Enable);
	Meta_AddObject(Params);

	Meta_SetName((IDS_CONTROL_PROGRAM));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program - SubProgram
//

// Dynamic Class

AfxImplementDynamicClass(CControlSubProgram, CControlCallProgram);

// Constructor

CControlSubProgram::CControlSubProgram(void)
{
	}

// Overridables

UINT CControlSubProgram::GetTreeImage(void) const
{
	return IDI_PROGRAM_SUBPROGRAM;
	}

// Overridables

void CControlSubProgram::Init(void)
{
	CControlCallProgram::Init();

	SetOnCallFlag();
	}

void CControlSubProgram::PostLoad(void)
{
	CControlCallProgram::PostLoad();

	SetOnCallFlag();
	}

// Implementation

void CControlSubProgram::SetOnCallFlag(void)
{
	DWORD dwProject = m_pProject->GetHandle();

	DWORD dwProgram = m_pProgram->GetHandle();

	afxDatabase->SetProgramOnCallFlag(dwProject, dwProgram, TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program - UDFB
//

// Dynamic Class

AfxImplementDynamicClass(CControlUdfbProgram, CControlCallProgram);

// Constructor

CControlUdfbProgram::CControlUdfbProgram(void)
{
	}

// Overridables

UINT CControlUdfbProgram::GetTreeImage(void) const
{
	return IDI_PROGRAM_UDFB;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Exception Program
//

// Dynamic Class

AfxImplementDynamicClass(CControlExceptionProgram, CControlProgram);

// Constructor

CControlExceptionProgram::CControlExceptionProgram(void)
{
	}

// Overridables

UINT CControlExceptionProgram::GetTreeImage(void) const
{
	return IDI_PROGRAM;
	}

// End of File
