
#include "intern.hpp"

#include "navtree.hpp"

#include "restree.hpp"

#include "catwnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Project Tree Window
//

// Static Data

UINT    CControlTreeWnd::m_timerSelect = AllocTimerID();

UINT    CControlTreeWnd::m_timerScroll = AllocTimerID();

CItem * CControlTreeWnd::m_pKilled     = NULL;

// Dynamic Class

AfxImplementDynamicClass(CControlTreeWnd, CStdTreeWnd);

// Constructor

CControlTreeWnd::CControlTreeWnd(void)
{
	m_dwStyle  = m_dwStyle | TVS_EDITLABELS | TVS_SHOWSELALWAYS;

	m_cfData   = RegisterClipboardFormat(L"C3.1 Control Nav Tree");

	m_uLocked  = 0;

	m_pList    = NULL;

	m_fList    = FALSE;

	m_fRefresh = FALSE;

	m_Accel1.Create(L"NavTreeAccel1");

	m_Accel2.Create(L"NavTreeAccel2");

	m_Accel3.Create(L"ControlTreeMenu");

	FindCatView();
}

// Destructor

CControlTreeWnd::~CControlTreeWnd(void)
{
}

// Attributes

BOOL CControlTreeWnd::CanCheckSyntax(void) const
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CControlProgram));
}

DWORD CControlTreeWnd::GetProgramHandle(void) const
{
	return ((CControlProgram *) m_pSelect)->GetHandle();
}

// IUnknown

HRESULT CControlTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
}

ULONG CControlTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
}

ULONG CControlTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
}

// IUpdate

HRESULT CControlTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateProps ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			CMetaItem *pMeta = (CMetaItem *) pItem;

			if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

				CTreeViewItem Node(hItem);

				LoadNodeItem(Node, pMeta);

				m_pTree->SetItem(Node);
			}
		}

		return S_OK;
	}

	if( uType == updateChildren ) {

		if( pItem == m_pProject ) {

			RefreshTree();
		}

		return S_OK;
	}

	if( uType == updateRename ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CControlObject)) ) {

				CControlObject *pObject = (CControlObject *) pItem;

				m_pTree->SetItemText(m_hSelect, pObject->GetTreeLabel());
			}
		}

		return S_OK;
	}

	return S_OK;
}

// IDropTarget

HRESULT CControlTreeWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( CanAcceptDataObject(pData, m_fDropLocal, m_DropClass) ) {

		m_uDrop = dropAccept;
	}
	else
		m_uDrop = dropOther;

	m_hDropRoot = NULL;

	m_hDropPrev = NULL;

	m_fDropMove = FALSE;

	BOOL fMove  = IsDropMove(dwKeys, pEffect);

	DropTrack(CPoint(pt.x, pt.y), fMove);

	return S_OK;
}

HRESULT CControlTreeWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		BOOL fMove = IsDropMove(dwKeys, pEffect);

		DropTrack(CPoint(pt.x, pt.y), fMove);

		return S_OK;
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CControlTreeWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( m_uDrop ) {

		ShowDrop(FALSE);

		m_System.HidePaneOnDrop(0);

		m_uDrop = dropNone;

		return S_OK;
	}

	return S_OK;
}

HRESULT CControlTreeWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop ) {

		ShowDrop(FALSE);

		if( m_uDrop == dropOther ) {

			// LATER -- Forward to view?
		}

		if( m_uDrop == dropAccept ) {

			DropDone(pData);

			SetFocus();
		}

		m_uDrop = dropNone;

		return S_OK;
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

// Message Map

AfxMessageMap(CControlTreeWnd, CStdTreeWnd)
{
	AfxDispatchAccelerator()

		AfxDispatchMessage(WM_LOADMENU)
		AfxDispatchMessage(WM_POSTCREATE)
		AfxDispatchMessage(WM_LOADTOOL)
		AfxDispatchMessage(WM_SHOWWINDOW)
		AfxDispatchMessage(WM_TIMER)
		AfxDispatchMessage(WM_DATABASE)

		AfxDispatchNotify(100, TVN_KEYDOWN, OnTreeKeyDown)
		AfxDispatchNotify(100, NM_RETURN, OnTreeReturn)
		AfxDispatchNotify(100, NM_DBLCLK, OnTreeDblClk)
		AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit)
		AfxDispatchNotify(100, TVN_ENDLABELEDIT, OnTreeEndEdit)

		AfxDispatchGetInfoType(IDM_GO, OnGoGetInfo)
		AfxDispatchControlType(IDM_GO, OnGoControl)
		AfxDispatchCommandType(IDM_GO, OnGoCommand)
		AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
		AfxDispatchControlType(IDM_ITEM, OnItemControl)
		AfxDispatchCommandType(IDM_ITEM, OnItemCommand)
		AfxDispatchGetInfoType(IDM_EDIT, OnEditGetInfo)
		AfxDispatchControlType(IDM_EDIT, OnEditControl)
		AfxDispatchCommandType(IDM_EDIT, OnEditCommand)
		AfxDispatchGetInfoType(IDM_CTRL, OnCtrlGetInfo)
		AfxDispatchControlType(IDM_CTRL, OnCtrlControl)
		AfxDispatchCommandType(IDM_CTRL, OnCtrlCommand)

		AfxMessageEnd(CControlTreeWnd)
};

// Accelerator

BOOL CControlTreeWnd::OnAccelerator(MSG &Msg)
{
	if( m_Accel3.Translate(Msg) ) {

		return TRUE;
	}

	return FALSE;
}

// Message Handlers

void CControlTreeWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	SelectInit();

	m_System.RegisterForUpdates(this);
}

void CControlTreeWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {

		Menu.MergeMenu(CMenu(L"ControlTreeMenu"));
	}
}

void CControlTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ControlTreeTool"));
	}
}

void CControlTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( fShow ) {

		m_System.SetNavCheckpoint();

		m_System.SetViewedItem(m_pSelect);
	}

	ShowStatus(fShow);
}

void CControlTreeWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerDouble ) {

		m_System.FlipToItemView(TRUE);

		KillTimer(uID);
	}

	if( uID == m_timerSelect ) {

		if( m_uDrop ) {

			if( m_hDropRoot ) {

				if( m_uDrop == dropAccept ) {

					ShowDrop(FALSE);
				}

				if( IsExpandDrop() ) {

					ExpandItem(m_hDropRoot);

					m_pTree->UpdateWindow();
				}
				else {
					m_pTree->SelectItem(m_hDropRoot);

					m_pTree->UpdateWindow();
				}

				if( m_uDrop == dropAccept ) {

					ShowDrop(TRUE);
				}
			}
		}

		KillTimer(uID);
	}

	if( uID == m_timerScroll ) {

		if( m_uDrop ) {

			UINT  uCmd = NOTHING;

			CRect View = m_pTree->GetClientRect();

			ClientToScreen(View);

			if( View.PtInRect(m_DragPos) ) {

				View -= 40;

				if( m_DragPos.y < View.top ) {

					int nLimit = m_pTree->GetScrollRangeMin(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) > nLimit ) {

						uCmd = SB_LINEUP;
					}
				}

				if( m_DragPos.y > View.bottom ) {

					int nLimit = m_pTree->GetScrollRangeMax(SB_VERT);

					if( m_pTree->GetScrollPos(SB_VERT) < nLimit ) {

						uCmd = SB_LINEDOWN;
					}
				}

				if( uCmd < NOTHING ) {

					ShowDrop(FALSE);

					m_pTree->SendMessage(WM_VSCROLL, uCmd);

					m_pTree->UpdateWindow();

					ShowDrop(TRUE);

					return;
				}
			}
		}

		KillTimer(uID);
	}
}

void CControlTreeWnd::OnDatabase(UINT uCode, DWORD dwIdent)
{
	if( HIWORD(uCode) == 7 ) {

		// Variable Event

		if( LOWORD(uCode) == 1 ) {

			// Variable Created

			CStratonVariableDescriptor Variable(m_dwProject, dwIdent);

			CStratonDataTypeDescriptor Type(m_dwProject, Variable.m_dwType);

			CStratonGroupDescriptor    Group(m_dwProject, Variable.m_dwGroup);

			if( Type.IsBasic() || Type.IsString() || Type.IsUdFb() || Type.IsStdFb() ) {

				CGroupVariables *pGroup;

				if( m_pProject->FindVarGroup(pGroup, Group.m_Name) ) {

					if( !(Variable.m_dwFlags & CControlVariable::attrParam) ) {

						CControlVariable *pVariable;

						if( !pGroup->FindVariable(pVariable, Variable.m_Name) ) {

							CString          Class  = pGroup->GetClassName();

							CControlVariable *pItem = AfxNewObject(CControlVariable, AfxNamedClass(Class));

							pItem->m_Name           = Variable.m_Name;

							pItem->m_TypeIdent      = Type.m_dwHandle;

							pItem->m_TypeName       = Type.m_Name;

							pItem->m_Persist        = Group.IsRetentive();

							pGroup->m_pObjects->AppendItem(pItem);

							m_System.SaveCmd(New CCmdCreate(pGroup, L"Objects", m_pSelect, pItem));

							// Wrong place?

							LockUpdate(TRUE);

							HTREEITEM hRoot = m_MapFixed[pGroup->GetFixedPath()];

							LoadItem(hRoot, NULL, pItem);

							ExpandItem(hRoot);

							LockUpdate(FALSE);

							ListUpdated(hRoot);
						}
					}
				}
			}
		}
	}
}

// Notification Handlers

BOOL CControlTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_TAB:

			m_System.FlipToItemView(TRUE);

			return TRUE;
	}

	return CStdTreeWnd::OnTreeKeyDown(uID, Info);
}

void CControlTreeWnd::OnTreeReturn(UINT uID, NMHDR &Info)
{
	if( m_pTree->GetChild(m_hSelect) ) {

		ToggleItem(m_hSelect);

		return;
	}

	m_System.FlipToItemView(TRUE);
}

void CControlTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( m_pTree->GetChild(m_hSelect) ) {

			ToggleItem(m_hSelect);

			return;
		}

		SetTimer(m_timerDouble, 5);
	}
}

BOOL CControlTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{
	return !CanItemRename();
}

BOOL CControlTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
	}

	return FALSE;
}

// Go Menu

BOOL CControlTreeWnd::OnGoGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= IDM_GO_REFERENCE ) {

		INDEX n = m_JumpList.GetHead();

		while( !m_JumpList.Failed(n) ) {

			if( uID-- == IDM_GO_REFERENCE ) {

				CStringArray List;

				m_JumpList[n].Tokenize(List, '|');

				if( List[0] == L"A" ) {

					Info.m_Image = 0x20000001;
				}

				break;
			}

			m_JumpList.GetNext(n);
		}

		Info.m_Prompt = CString(IDS_JUMP_TO_SELECTED);

		return TRUE;
	}

	switch( uID ) {

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CControlTreeWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_GO_REFERENCE ) {

		Src.EnableItem(TRUE);

		return TRUE;
	}

	switch( uID ) {

		case IDM_GO_NEXT_1:
		case IDM_GO_NEXT_2:

			Src.EnableItem(m_pTree->GetNextNode(m_hSelect, TRUE) ? TRUE : FALSE);

			return TRUE;

		case IDM_GO_PREV_1:
		case IDM_GO_PREV_2:

			Src.EnableItem(m_pTree->GetPrevNode(m_hSelect, TRUE) ? TRUE : FALSE);

			return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::OnGoCommand(UINT uID)
{
	if( uID >= IDM_GO_REFERENCE ) {

		INDEX n = m_JumpList.GetHead();

		while( !m_JumpList.Failed(n) ) {

			if( uID-- == IDM_GO_REFERENCE ) {

				CStringArray List;

				m_JumpList[n].Tokenize(List, '|');

				m_System.Navigate(List[2]);

				break;
			}

			m_JumpList.GetNext(n);
		}

		return TRUE;
	}

	switch( uID ) {

		case IDM_GO_NEXT_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_NEXT_1:

			OnGoNext(TRUE);

			return TRUE;

		case IDM_GO_PREV_2:

			m_System.FlipToItemView(TRUE);

		case IDM_GO_PREV_1:

			OnGoPrev(TRUE);

			return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID == IDM_ITEM_RENAME ) {

		Info.m_Image = MAKELONG(0x0021, 0x1000);
	}

	if( uID == IDM_ITEM_DELETE ) {

		Info.m_Image = MAKELONG(0x0008, 0x1000);
	}

	if( uID == IDM_ITEM_NEW ) {

		Info.m_Image = MAKELONG(0x000E, 0x4000);
	}

	if( uID == IDM_ITEM_WATCH ) {

		Info.m_Image = MAKELONG(0x0036, 0x1000);
	}

	return FALSE;
}

BOOL CControlTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	CString Label;

	switch( uID ) {

		case IDM_ITEM_RENAME:

			Src.EnableItem(CanItemRename());

			return TRUE;

		case IDM_ITEM_DELETE:

			Src.EnableItem(CanItemDelete());

			return TRUE;

		case IDM_ITEM_WATCH:

			Src.EnableItem(CanItemWatch());

			break;

		case IDM_ITEM_PRIVATE:

			Src.EnableItem(CanItemPrivate());

			Src.CheckItem(m_pSelect->GetPrivate());

			return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_RENAME:

			m_pTree->EditLabel(m_hSelect);

			return TRUE;

		case IDM_ITEM_DELETE:

			MarkMulti(m_pSelect->GetFixedPath(), CFormat(IDS_DELETE_3, m_pSelect->GetName()), TRUE);

			OnItemDelete(IDS_DELETE_2, TRUE, TRUE);

			MarkMulti(m_pSelect->GetFixedPath(), CFormat(IDS_DELETE_3, m_pSelect->GetName()), TRUE);

			return TRUE;

		case IDM_ITEM_WATCH:

			OnItemWatch();

			break;

		case IDM_ITEM_PRIVATE:

			OnItemPrivate();

			return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::OnEditGetInfo(UINT uID, CCmdInfo &Info)
{
	return FALSE;
}

BOOL CControlTreeWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			Src.EnableItem(CanItemDelete());

			return TRUE;

		case IDM_EDIT_CUT:

			Src.EnableItem(CanItemDelete() && CanItemCopy());

			return TRUE;

		case IDM_EDIT_COPY:

			Src.EnableItem(CanItemCopy());

			return TRUE;

		case IDM_EDIT_PASTE:

			Src.EnableItem(CanEditPaste());

			return TRUE;

		case IDM_EDIT_SMART_DUP:

			Src.EnableItem(CanEditDuplicate());

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CControlTreeWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			OnItemDelete(IDS_DELETE_1, TRUE, TRUE);

			return TRUE;

		case IDM_EDIT_CUT:

			OnEditCut();

			return TRUE;

		case IDM_EDIT_COPY:

			OnEditCopy();

			return TRUE;

		case IDM_EDIT_PASTE:

			OnEditPaste();

			return TRUE;

		case IDM_EDIT_SMART_DUP:

			OnEditDuplicate();

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CControlTreeWnd::OnCtrlGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case IDM_CTRL_ADD_PROGRAM:

			Info.m_Image = MAKELONG(0x000E, 0x4000);

			break;

		case IDM_CTRL_ADD_SUB_PROG:

			Info.m_Image = MAKELONG(0x000E, 0x4000);

			break;

		case IDM_CTRL_ADD_UDFB:

			Info.m_Image = MAKELONG(0x000E, 0x4000);

			break;

		case IDM_CTRL_ADD_SFC_CHILD:

			Info.m_Image = MAKELONG(0x000E, 0x4000);

			break;

		case IDM_CTRL_ADD_VARIABLE:
		case IDM_CTRL_ADD_INPUT:
		case IDM_CTRL_ADD_OUTPUT:
		case IDM_CTRL_ADD_INOUT:

			Info.m_Image = MAKELONG(0x000E, 0x4000);

			break;

		case IDM_CTRL_XREF:

			Info.m_Image = MAKELONG(0x000C, 0x1000);

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CControlTreeWnd::OnCtrlControl(UINT uID, CCmdSource &Src)
{
	CString Label;

	switch( uID ) {

		case IDM_CTRL_UNDONAME:

			Src.EnableItem(IsVariable());

			return TRUE;

		case IDM_CTRL_AUTONAME:

			Src.EnableItem(CanCtrlAutoName());

			return TRUE;

		case IDM_CTRL_CONVERT_SFC:
		case IDM_CTRL_CONVERT_ST:
		case IDM_CTRL_CONVERT_FBD:
		case IDM_CTRL_CONVERT_LD:
		case IDM_CTRL_CONVERT_IL:

			Src.EnableItem(CanCtrlConvert(uID - IDM_CTRL_CONVERT_SFC));

			return TRUE;

		case IDM_CTRL_ADD_PROGRAM:

			Src.EnableItem(CanCtrlCreateProgram(CProgramCreateDialog::callMainCycle));

			break;

		case IDM_CTRL_ADD_SUB_PROG:

			Src.EnableItem(CanCtrlCreateProgram(CProgramCreateDialog::callSubProgram));

			break;

		case IDM_CTRL_ADD_UDFB:

			Src.EnableItem(CanCtrlCreateProgram(CProgramCreateDialog::callUdfb));

			break;

		case IDM_CTRL_ADD_SFC_CHILD:

			Src.EnableItem(CanCtrlCreateProgram(CProgramCreateDialog::callSfcChild));

			break;

		case IDM_CTRL_ADD_VARIABLE:
		case IDM_CTRL_ADD_INPUT:
		case IDM_CTRL_ADD_OUTPUT:
		case IDM_CTRL_ADD_INOUT:

			Src.EnableItem(CanCtrlCreateVariable(uID - IDM_CTRL_ADD_VARIABLE));

			break;

		case IDM_CTRL_XREF:

			Src.EnableItem(CanCtrlXRef());

			break;

		case IDM_CTRL_EXEC:

			Src.EnableItem(CanCtrlExec());

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CControlTreeWnd::OnCtrlCommand(UINT uID)
{
	switch( uID ) {

		case IDM_CTRL_UNDONAME:

			OnCtrlUndoName();

			return TRUE;

		case IDM_CTRL_AUTONAME:

			OnCtrlAutoName();

			return TRUE;

		case IDM_CTRL_CONVERT_SFC:
		case IDM_CTRL_CONVERT_ST:
		case IDM_CTRL_CONVERT_FBD:
		case IDM_CTRL_CONVERT_LD:
		case IDM_CTRL_CONVERT_IL:

			OnCtrlConvert(uID - IDM_CTRL_CONVERT_SFC);

			return TRUE;

		case IDM_CTRL_ADD_PROGRAM:

			OnCtrlCreateProgram(CProgramCreateDialog::callMainCycle);

			break;

		case IDM_CTRL_ADD_SUB_PROG:

			OnCtrlCreateProgram(CProgramCreateDialog::callSubProgram);

			break;

		case IDM_CTRL_ADD_UDFB:

			OnCtrlCreateProgram(CProgramCreateDialog::callUdfb);

			break;

		case IDM_CTRL_ADD_SFC_CHILD:

			OnCtrlCreateProgram(CProgramCreateDialog::callSfcChild);

			break;

		case IDM_CTRL_ADD_VARIABLE:
		case IDM_CTRL_ADD_INPUT:
		case IDM_CTRL_ADD_OUTPUT:
		case IDM_CTRL_ADD_INOUT:

			OnCtrlCreateVariable(uID - IDM_CTRL_ADD_VARIABLE);

			break;

		case IDM_CTRL_XREF:

			OnCtrlXRef();

			break;

		case IDM_CTRL_EXEC:

			OnCtrlExec();

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

// Overridables

void CControlTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	m_pProject  = (CControlProject *) m_pItem;

	m_dwProject = m_pProject->GetHandle();

	m_pSystem   = (CCommsSystem  *) m_pItem->GetDatabase()->GetSystemItem();
}

CString CControlTreeWnd::OnGetNavPos(void)
{
	// LATER -- Refactor this back to CStdTreeWnd...

	if( m_pSelect ) {

		return m_SelPath;
	}

	return m_pItem->GetFixedPath();
}

BOOL CControlTreeWnd::OnNavigate(CString const &Nav)
{
	if( Nav.IsEmpty() ) {

		m_pTree->SelectItem(m_hRoot);

		return TRUE;
	}
	else {
		CString Find = Nav;

		while( !Find.IsEmpty() ) {

			HTREEITEM hFind = m_MapFixed[Find];

			if( hFind ) {

				m_pTree->SelectItem(hFind);

				m_System.SetViewedItem(m_pSelect);

				return TRUE;
			}

			Find = Find.Left(Find.FindRev('/'));
		}

		return FALSE;
	}
}

void CControlTreeWnd::OnExec(CCmd *pCmd)
{
	AfxAssume(pCmd);

	StopAndCommit();

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		LockUpdate(!m_uLocked);
	}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnExecCreate((CCmdCreate *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnExecRename((CCmdRename *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnExecDelete((CCmdDelete *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnExecPaste((CCmdPaste *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdPrivate) ) {

		CCmdPrivate *pHide = (CCmdPrivate *) pCmd;

		HideItem(pHide->m_fHide);

		m_System.ItemUpdated(m_pSelect, updateContents);
	}
}

void CControlTreeWnd::OnUndo(CCmd *pCmd)
{
	AfxAssume(pCmd);

	StopAndCommit();

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		LockUpdate(!m_uLocked);
	}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnUndoCreate((CCmdCreate *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnUndoRename((CCmdRename *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnUndoDelete((CCmdDelete *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnUndoPaste((CCmdPaste *) pCmd);
	}

	if( Class == AfxRuntimeClass(CCmdPrivate) ) {

		CCmdPrivate *pHide = (CCmdPrivate *) pCmd;

		HideItem(pHide->m_fHide);

		m_System.ItemUpdated(m_pSelect, updateContents);
	}
}

// Go Menu

BOOL CControlTreeWnd::OnGoNext(BOOL fSiblings)
{
	HTREEITEM hItem = m_pTree->GetNextNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::OnGoPrev(BOOL fSiblings)
{
	HTREEITEM hItem = m_pTree->GetPrevNode(m_hSelect, fSiblings);

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::StepAway(void)
{
	HTREEITEM hItem;

	if( (hItem = m_pTree->GetNext(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	if( (hItem = m_pTree->GetPrevious(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	if( (hItem = m_pTree->GetParent(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
	}

	return FALSE;
}

// Item Menu : Create

void CControlTreeWnd::OnCreateItem(CMetaItem *pItem)
{
	HTREEITEM   hRoot = FindGroupNode(m_hSelect);

	CMetaItem * pRoot = GetItemPtr(hRoot);

	CString     List  = GetItemList(pRoot);

	AddToList(hRoot, m_hSelect, pItem);

	m_System.SaveCmd(New CCmdCreate(pRoot, List, m_pSelect, pItem));

	LockUpdate(TRUE);

	ExpandItem(hRoot);

	HTREEITEM hItem = AddToTree(hRoot, m_hSelect, pItem);

	m_pTree->SelectItem(hItem);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
}

void CControlTreeWnd::OnExecCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	HTREEITEM	 hPrev = m_MapFixed[pCmd->m_Prev];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	pItem->SetParent(pList);

	AddToList(hRoot, hPrev, pItem);

	LockUpdate(TRUE);

	ExpandItem(hRoot);

	HTREEITEM hItem = AddToTree(hRoot, hPrev, pItem);

	m_pTree->SelectItem(hItem);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
}

void CControlTreeWnd::OnUndoCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	HTREEITEM        hItem = m_MapFixed[pCmd->m_Made];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	m_pCatView->DetachEditors(pItem);

	pList->DeleteItem(pItem);

	LockUpdate(TRUE);

	KillTree(hItem, TRUE);

	LockUpdate(FALSE);

	ListUpdated(hRoot);

	m_pKilled = pItem;
}

BOOL CControlTreeWnd::CanCreateProgram(void)
{
	return IsProgram() || IsProgramGroup();
}

void CControlTreeWnd::OnCreateVariable(DWORD dwGroup, CLASS Class, DWORD dwAttr)
{
	StopAndCommit();

	CVariableCreateDialog Dlg(m_dwProject, dwGroup, dwAttr);

	if( Dlg.Execute() ) {

		CControlVariable *pVariable = AfxNewObject(CControlVariable, Class);

		pVariable->m_Name       = Dlg.GetName();

		pVariable->m_TypeIdent  = Dlg.GetType();

		pVariable->m_Flags      = dwAttr; // May be Dlg.GetAttr() one day...

		pVariable->m_TypeName   = CStratonDataTypeDescriptor(m_dwProject, Dlg.GetType()).m_Name;

		OnCreateItem(pVariable);
	}
}

void CControlTreeWnd::OnCreateProgram(DWORD dwParent, DWORD dwCall)
{
	StopAndCommit();

	CProgramCreateDialog Dlg(m_dwProject, dwParent, dwCall);

	if( Dlg.Execute() ) {

		DWORD dwLang = Dlg.GetLang();

		DWORD dwCall = Dlg.GetCall();

		CLASS  Class = AfxRuntimeClass(CControlMainProgram);

		switch( dwCall ) {

			case CProgramCreateDialog::callMainCycle:

				if( dwLang == CProgramCreateDialog::langSFC ) {

					Class = AfxRuntimeClass(CControlSfcMainProgram);
				}
				break;

			case CProgramCreateDialog::callSubProgram:

				Class = AfxRuntimeClass(CControlSubProgram);

				break;

			case CProgramCreateDialog::callUdfb:

				Class = AfxRuntimeClass(CControlUdfbProgram);

				break;

			case CProgramCreateDialog::callSfcChild:

				Class = AfxRuntimeClass(CControlSfcChildProgram);

				break;
		}

		CControlProgram *pProgram = AfxNewObject(CControlProgram, Class);

		pProgram->m_Name     = Dlg.GetName();

		pProgram->m_Language = Dlg.GetLang();

		pProgram->m_Section  = Dlg.GetSect();

		pProgram->m_Parent   = dwParent;

		OnCreateItem(pProgram);

		return;
	}
}

void CControlTreeWnd::OnCtrlXRef(void)
{
	if( IsControl() ) {

		CControlObject * pObject = (CControlObject *) m_pSelect;

		CString Find = pObject->m_Name;

		CControlCrossReference(m_pProject).FindInFiles(Find);
	}
}

void CControlTreeWnd::OnCtrlExec(void)
{
	// TODO -- Implement
}

BOOL CControlTreeWnd::CanItemWatch(void)
{
	if( IsVariableParam() || IsVariableParamGroup() ) {

		return FALSE;
	}

	if( IsVariableLocal() || IsVariableLocalGroup() ) {

		// TODO -- support watch list

		return FALSE;
	}

	if( IsVariableGlobal() || IsVariableGlobalGroup() ) {

		// TODO -- support watch list

		return FALSE;
	}

	return FALSE;
}

void CControlTreeWnd::OnItemWatch(void)
{
	Information(CString(IDS_WATCH_LIST_NOT));
}

// Item Menu : Private

BOOL CControlTreeWnd::CanItemPrivate(void)
{
	if( !IsReadOnly() && !IsPrivate() ) {

		if( m_pSelect->HasPrivate() ) {

			if( IsProgram() ) {

				return TRUE;
			}

			if( IsVariableGlobalGroup() ) {

				return TRUE;
			}

			if( IsProgramGroup() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CControlTreeWnd::OnItemPrivate(void)
{
	UINT uPrivate = m_pSelect->GetPrivate();

	CCmd *pCmd = New CCmdPrivate(m_pSelect, uPrivate ? 0 : 1);

	m_System.ExecCmd(pCmd);
}

// Item Menu : Rename

BOOL CControlTreeWnd::CanItemRename(void)
{
	return !IsItemLocked(m_hSelect) && IsControl();
}

BOOL CControlTreeWnd::OnItemRename(CString Name)
{
	StopAndCommit();

	CString Prev = m_pNamed->GetName();

	if( wstrcmp(Name, Prev) ) {

		if( IsControl() ) {

			CControlObject *pObject = (CControlObject *) m_pSelect;

			CError Error(TRUE);

			if( pObject->OnRename(Error, Name) ) {

				RenameItem(Name, Prev);

				CCmd *pCmd = New CCmdRename(m_pNamed, Prev);

				m_System.SaveCmd(pCmd);

				return TRUE;
			}

			Error.Show(ThisObject);
		}
	}

	return FALSE;
}

void CControlTreeWnd::OnExecRename(CCmdRename *pCmd)
{
	CControlObject *pObject = (CControlObject *) m_pSelect;

	pObject->OnRename(CError(FALSE), pCmd->m_Name);

	RenameItem(pCmd->m_Name, pCmd->m_Prev);
}

void CControlTreeWnd::OnUndoRename(CCmdRename *pCmd)
{
	CControlObject *pObject = (CControlObject *) m_pSelect;

	pObject->OnRename(CError(FALSE), pCmd->m_Prev);

	RenameItem(pCmd->m_Prev, pCmd->m_Name);
}

void CControlTreeWnd::RenameItem(CString Name, CString Prev)
{
	m_pNamed->SetDirty();

	m_pTree->SetItemText(m_hSelect, Name);

	m_System.ItemUpdated(m_pSelect, updateRename);
}

// Item Menu : Delete

BOOL CControlTreeWnd::CanItemDelete(void)
{
	if( m_hSelect && m_pSelect ) {

		if( !IsItemLocked(m_hSelect) ) {

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CControlSfcProgram)) ) {

				CControlSfcProgram *pSfc = (CControlSfcProgram *) m_pSelect;

				return !pSfc->HasChildren();
			}

			return IsControl();
		}
	}

	return FALSE;
}

void CControlTreeWnd::OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop)
{
	StopAndCommit();

	CMetaItem      * pRoot = (CMetaItem *) m_pSelect->GetParent(2);

	CString          List  = GetItemList(m_pSelect);

	CItemIndexList * pList = pRoot->FindIndexList(List);

	INDEX            Index = pList->FindItemIndex(m_pSelect);

	CItem          * pNext = pList->GetNext(Index) ? pList->GetItem(Index) : NULL;

	CCmd           * pCmd  = New CCmdDelete(pRoot, List, pNext, m_pSelect);

	m_System.ExecCmd(pCmd);
}

void CControlTreeWnd::OnExecDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM        hItem = m_hSelect;

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	LockUpdate(TRUE);

	StepAway();

	m_pCatView->DetachEditors(pItem);

	pList->DeleteItem(pItem);

	KillTree(hItem, TRUE);

	LockUpdate(FALSE);

	ListUpdated(hRoot);

	m_pKilled = pItem;
}

void CControlTreeWnd::OnUndoDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM        hNext = m_MapFixed[pCmd->m_Next];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pNext = GetItemOpt(hNext);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	pList->InsertItem(pItem, pNext);

	LockUpdate(TRUE);

	HTREEITEM hItem = LoadItem(hRoot, hNext, pItem);

	m_pTree->SelectItem(hItem);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
}

// Edit Menu : Cut and Copy

BOOL CControlTreeWnd::CanItemCopy(void)
{
	return !IsItemLocked(m_hSelect) && IsControl();
}

void CControlTreeWnd::OnEditCut(void)
{
	MarkMulti(m_pSelect->GetFixedPath(), CFormat(IDS_CUT_3, m_pSelect->GetName()), TRUE);

	OnEditCopy();

	OnItemDelete(IDS_CUT_2, FALSE, FALSE);

	MarkMulti(m_pSelect->GetFixedPath(), CFormat(IDS_CUT_3, m_pSelect->GetName()), TRUE);
}

void CControlTreeWnd::OnEditCopy(void)
{
	IDataObject *pData;

	if( MakeDataObject(pData, TRUE) ) {

		OleSetClipboard(pData);

		OleFlushClipboard();

		pData->Release();
	}
}

// Edit Menu : Paste

BOOL CControlTreeWnd::CanEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		BOOL  fLocal;

		CLASS Class;

		if( CanAcceptDataObject(pData, fLocal, Class) ) {

			if( CanAcceptClass(Class, m_pList) ) {

				pData->Release();

				return TRUE;
			}
		}

		pData->Release();
	}

	return FALSE;
}

void CControlTreeWnd::OnEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		BOOL  fLocal;

		CLASS Class;

		if( CanAcceptDataObject(pData, fLocal, Class) ) {

			if( CanAcceptClass(Class, m_pList) ) {

				StopAndCommit();

				OnEditPaste(pData);
			}
		}

		pData->Release();
	}
}

void CControlTreeWnd::OnEditPaste(IDataObject *pData)
{
	HTREEITEM hRoot = m_fList ? m_hSelect : m_pTree->GetParent(m_hSelect);

	HTREEITEM hPrev = m_hSelect;

	OnEditPaste(m_pSelect, hRoot, hPrev, pData);
}

void CControlTreeWnd::OnEditPaste(CItem *pItem, HTREEITEM hRoot, HTREEITEM hPrev, IDataObject *pData)
{
	CItem * pRoot = GetItemPtr(hRoot);

	CItem * pPrev = GetItemOpt(hPrev);

	CCmd  * pCmd  = New CCmdPaste(pItem, pRoot, pPrev, pData, FALSE);

	m_System.ExecCmd(pCmd);
}

void CControlTreeWnd::OnExecPaste(CCmdPaste *pCmd)
{
	CAcceptCtx Ctx;

	Ctx.m_Root   = pCmd->m_Root;

	Ctx.m_Prev   = pCmd->m_Prev;

	Ctx.m_fUndel = FALSE;

	if( pCmd->m_fInit ) {

		Ctx.m_fPaste = TRUE;

		Ctx.m_fCheck = TRUE;

		Ctx.m_pNames = &pCmd->m_Names;

		AcceptDataObject(pCmd->m_pData, Ctx);

		pCmd->m_fInit = FALSE;

		pCmd->m_Menu  = CFormat(pCmd->m_Menu, m_pNamed->GetName());

		pCmd->m_pData->Release();

		MakeDataObject(pCmd->m_pData, FALSE);

		return;
	}

	Ctx.m_fPaste = FALSE;

	Ctx.m_fCheck = pCmd->m_fMove;

	Ctx.m_pNames = NULL;

	AcceptDataObject(pCmd->m_pData, Ctx);
}

void CControlTreeWnd::OnUndoPaste(CCmdPaste *pCmd)
{
	UINT n = pCmd->m_Names.GetCount();

	while( n-- ) {

		CString    Base  = m_pList->GetFixedPath();

		CString    Name  = pCmd->m_Names[n];

		CString    Fixed = Base + L'/' + Name;

		HTREEITEM  hItem = m_MapFixed[Fixed];

		CMetaItem *pItem = GetItemPtr(hItem);

		if( pItem == m_pSelect ) {

			StepAway();
		}

		m_pCatView->DetachEditors(pItem);

		m_pList->DeleteItem(pItem);

		m_pKilled = pItem;

		KillTree(hItem);
	}

	ListUpdated(m_hRoot);
}

// Edit Menu : Misc

BOOL CControlTreeWnd::CanEditDuplicate(void)
{
	// TODO -- implement

	if( !IsReadOnly() ) {

		#if 0
		return IsVariable();
		#else
		return FALSE;
		#endif
	}

	return FALSE;
}

void CControlTreeWnd::OnEditDuplicate(void)
{
	// TODO -- implement
}

// Item Menu : Convert

BOOL CControlTreeWnd::CanCtrlConvert(UINT uType)
{
	if( !IsItemLocked(m_hSelect) ) {

		if( IsProgram() ) {

			CString Path = m_pProject->GetProjectPath();;

			CString Name = m_pSelect->GetName();

			DWORD dwLang = (1 << uType);

			return Straton_CanConvertProgram(Path, Name, dwLang);
		}
	}

	return FALSE;
}

void CControlTreeWnd::OnCtrlConvert(UINT uType)
{
	StopAndCommit();

	CSysProxy Proxy;

	Proxy.Bind();

	if( Proxy.KillUndoList() ) {

		Commit();

		if( CControlCompiler(m_pProject).Build(CControlCompiler::compConvert | CControlCompiler::compErrors) ) {

			CString Path = m_pProject->GetProjectPath();

			CControlProgram * pProgram = (CControlProgram *) m_pSelect;

			CString Name = m_pSelect->GetName();

			DWORD dwLang = (1 << uType);

			if( Straton_ConvertProgram(Path, Name, dwLang) ) {

				pProgram->OnConvert();

				m_System.ItemUpdated(pProgram, updateContents);
			}
		}
	}
}

// Ctrl Menu : Create

BOOL CControlTreeWnd::CanCtrlCreateProgram(UINT uType)
{
	if( m_hSelect && m_pSelect ) {

		if( !IsItemLocked(m_hSelect) ) {

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CGroupChildPrograms))     ||
			   m_pSelect->IsKindOf(AfxRuntimeClass(CControlSfcChildProgram)) ) {

				return uType == CProgramCreateDialog::callSfcChild;
			}

			if( uType == CProgramCreateDialog::callSfcChild ) {

				return FALSE;
			}

			return IsProgram() || IsProgramGroup();
		}
	}

	return FALSE;
}

BOOL CControlTreeWnd::CanCtrlCreateVariable(UINT uType)
{
	if( m_hSelect && m_pSelect ) {

		if( !IsItemLocked(m_hSelect) ) {

			if( uType == 0 ) {

				if( IsVariableLocal() || IsVariableLocalGroup() ) {

					return !(IsVariableParam() || IsVariableParamGroup());
				}

				return IsVariableGlobal() || IsVariableGlobalGroup();
			}

			if( IsVariableParam() || IsVariableParamGroup() ) {

				CControlProgram *pProgram = FindProgram();

				if( pProgram->IsKindOf(AfxRuntimeClass(CControlSubProgram)) ) {

					switch( uType ) {

						case 1:
						case 2:
							break;

						default:
							return FALSE;
					}

					return TRUE;
				}

				if( pProgram->IsKindOf(AfxRuntimeClass(CControlUdfbProgram)) ) {

					switch( uType ) {

						case 1:
						case 2:
						case 3:
							break;

						default:
							return FALSE;
					}

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

void CControlTreeWnd::OnCtrlCreateProgram(UINT uCall)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CGroupChildPrograms))     ||
	   m_pSelect->IsKindOf(AfxRuntimeClass(CControlSfcChildProgram)) ) {

		CControlProgram *pProgram = FindProgram();

		DWORD            dwParent = pProgram->GetHandle();

		OnCreateProgram(dwParent, uCall);

		return;
	}

	if( IsProgramGroup() ) {

		OnCreateProgram(0, uCall);

		return;
	}

	if( IsProgram() ) {

		OnCreateProgram(0, uCall);

		return;
	}
}

void CControlTreeWnd::OnCtrlCreateVariable(UINT uType)
{
	if( IsVariableGlobal() || IsVariableGlobalGroup() ) {

		DWORD dwGroup = afxDatabase->FindGroup(m_dwProject, L"(Global)");

		CLASS Class   = AfxRuntimeClass(CControlGlobalVariable);

		OnCreateVariable(dwGroup, Class, 0);

		return;
	}

	if( IsVariableParam() || IsVariableParamGroup() ) {

		CControlProgram *pProgram = FindProgram();

		CGroupVariables   *pGroup = FindVariableGroup();

		CString              Name = pProgram->GetName();

		DWORD             dwGroup = afxDatabase->FindGroup(m_dwProject, Name);

		CLASS               Class = pGroup->GetClass();

		DWORD		   dwAttr = 0;

		switch( uType ) {

			case 1:	dwAttr = 0x08; break;
			case 2:	dwAttr = 0x10; break;
			case 3:	dwAttr = 0x48; break;
		}

		OnCreateVariable(dwGroup, Class, dwAttr);

		return;
	}

	if( IsVariable() || IsVariableGroup() ) {

		CControlProgram *pProgram = FindProgram();

		CGroupVariables   *pGroup = FindVariableGroup();

		CString              Name = pProgram->GetName();

		DWORD             dwGroup = afxDatabase->FindGroup(m_dwProject, Name);

		CLASS               Class = pGroup->GetClass();

		OnCreateVariable(dwGroup, Class, 0);

		return;
	}
}

// Ctrl Menu

BOOL CControlTreeWnd::CanCtrlAutoName(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

		CControlVariable *pVariable = (CControlVariable *) m_pSelect;

		CCodedItem *pCoded = pVariable->m_pValue;

		if( pCoded ) {

			if( pCoded->IsCommsRef() ) {

				return TRUE;
			}

			if( pCoded->IsTagRef() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CControlTreeWnd::OnCtrlUndoName(void)
{
	if( IsVariable() ) {

		CControlVariable *pVariable = (CControlVariable *) m_pSelect;

		DWORD dwProject = pVariable->GetProject();

		CString Name;

		for( UINT n = 1;; n++ ) {

			CFormat Name("Variable%1!u!", n);

			if( !afxDatabase->FindVarExact(dwProject, Name, 0, 0) ) {

				OnItemRename(Name);

				break;
			}
		}
	}
}

void CControlTreeWnd::OnCtrlAutoName(void)
{
	UINT uType = m_pProject->m_AutoName;

	if( IsVariable() ) {

		CControlVariable *pVariable = (CControlVariable *) m_pSelect;

		CCodedItem          *pCoded = pVariable->m_pValue;

		if( pCoded ) {

			CString Name = pVariable->m_Name;

			if( pCoded->IsCommsRef() ) {

				CString Code = pCoded->GetSource(TRUE);

				Code.Replace(L'.', L'_');

				Code = Code.Mid(1, Code.GetLength() - 2);

				if( Name == Code ) {

					return;
				}

				CVariableNamer(m_dwProject).MakeUnique(Code);

				OnItemRename(Code);

				return;
			}

			if( pCoded->IsTagRef() ) {

				CTag *pTag;

				if( uType == 2 && pCoded->GetTagItem(pTag) ) {

					CCodedItem *pCoded;

					if( (pCoded = pTag->m_pValue) ) {

						CString Code;

						Code += pCoded->GetSource(TRUE);

						Code.Replace(L'.', L'_');

						Code = Code.Mid(1, Code.GetLength() - 2);

						Code.Insert(0, pTag->m_Name + L"_");

						if( Name == Code ) {

							return;
						}

						CVariableNamer(m_dwProject).MakeUnique(Code);

						OnItemRename(Code);

						return;
					}
				}

				CString Code = pCoded->GetTagName();

				Code.Replace(L'.', L'_');

				if( Name == Code ) {

					return;
				}

				CVariableNamer(m_dwProject).MakeUnique(Code);

				OnItemRename(Code);

				return;
			}
		}
	}
}

BOOL CControlTreeWnd::CanCtrlXRef(void)
{
	if( IsControl() ) {

		return !m_pSelect->IsKindOf(AfxRuntimeClass(CControlMainProgram));
	}

	return FALSE;
}

BOOL CControlTreeWnd::CanCtrlExec(void)
{
	// TODO -- Implement

	return FALSE;
}

// Tree Loading

void CControlTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CtrlTreeIcon16"), afxColor(MAGENTA));
}

void CControlTreeWnd::LoadTree(void)
{
	LoadRoot();

	LoadNodeKids(m_hRoot, m_pItem);

	SelectRoot();
}

void CControlTreeWnd::LoadRoot(void)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, m_pItem);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);

	AddToMap(m_pItem, m_hRoot);
}

HTREEITEM CControlTreeWnd::LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	HTREEITEM hPrev = hNext ? m_pTree->GetPrevious(hNext) : TVI_LAST;

	HTREEITEM hUsed = hPrev ? hPrev : TVI_FIRST;

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, hUsed, Node);

	AddToMap(pItem, hItem);

	LoadNodeKids(hItem, pItem);

	return hItem;
}

void CControlTreeWnd::LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		Node.SetText(m_pItem->GetHumanName());

		Node.SetParam(LPARAM(m_pItem));

		Node.SetImages(IDI_CTRL);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		CGroupObjects *pGroup = (CGroupObjects *) pItem;

		Node.SetText(pGroup->GetTreeLabel());

		Node.SetParam(LPARAM(pGroup));

		Node.SetImages(pGroup->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlObject)) ) {

		CControlObject *pObject = (CControlObject *) pItem;

		Node.SetText(pObject->GetName());

		Node.SetParam(LPARAM(pObject));

		Node.SetImages(pObject->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

		CControlVariable *pVariable = (CControlVariable *) pItem;

		Node.SetText(pVariable->GetName());

		Node.SetParam(LPARAM(pVariable));

		Node.SetImages(pVariable->GetTreeImage());

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

		CControlProgram *pProgram = (CControlProgram *) pItem;

		Node.SetText(pProgram->GetName());

		Node.SetParam(LPARAM(pProgram));

		Node.SetImages(pProgram->GetTreeImage());

		return;
	}

	AfxAssert(FALSE);
}

void CControlTreeWnd::LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		CControlProject *pProject = (CControlProject *) pItem;

		LoadItem(hRoot, NULL, pProject->m_pPrograms);

		LoadItem(hRoot, NULL, pProject->m_pGlobals);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		CGroupObjects *pGroup = (CGroupObjects *) pItem;

		LoadList(hRoot, pGroup->m_pObjects);

		ExpandItem(hRoot);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlSfcProgram)) ) {

		CControlSfcProgram *pProgram = (CControlSfcProgram *) pItem;

		LoadItem(hRoot, NULL, pProgram->m_pLocals);

		LoadItem(hRoot, NULL, pProgram->m_pChilds);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlMainProgram)) ) {

		CControlMainProgram *pProgram = (CControlMainProgram *) pItem;

		LoadItem(hRoot, NULL, pProgram->m_pLocals);

		return;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlCallProgram)) ) {

		CControlCallProgram *pProgram = (CControlCallProgram *) pItem;

		LoadItem(hRoot, NULL, pProgram->m_pLocals);

		LoadItem(hRoot, NULL, pProgram->m_pParams);

		return;
	}
}

void CControlTreeWnd::LoadList(HTREEITEM hRoot, CItemList *pList)
{
	for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

		CMetaItem *pItem = (CMetaItem *) pList->GetItem(n);

		if( pItem->IsKindOf(AfxRuntimeClass(CControlObject)) ) {

			LoadItem(hRoot, NULL, pItem);

			continue;
		}
	}
}

// Data Object Construction

BOOL CControlTreeWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	if( !m_fMulti ) {

		if( IsProgram() || IsVariable() ) {

			CDataObject *pMake = New CDataObject;

			if( m_pNamed ) {

				CTextStreamMemory Stream;

				if( Stream.OpenSave() ) {

					if( AddItemToStream(Stream, m_hSelect) ) {

						pMake->AddStream(m_cfData, Stream);
					}
				}
			}

			if( fRich ) {

				if( m_pNamed ) {

					pMake->AddText(m_pNamed->GetName());
				}
			}

			if( pMake->IsEmpty() ) {

				delete pMake;

				pData = NULL;

				return FALSE;
			}

			pData = pMake;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CControlTreeWnd::AddItemToStream(ITextStream &Stream, HTREEITEM hItem)
{
	CString F1   = m_pItem->GetDatabase()->GetUniqueSig();

	CString F2   = m_pList->GetFixedName();

	CString F3   = CPrintf(L"%8.8X", m_hWnd);

	CString Head = CPrintf(L"%s|%s|%s\r\n", F1, F2, F3);

	Stream.PutLine(Head);

	CTreeFile Tree;

	if( Tree.OpenSave(Stream) ) {

		AddItemToTreeFile(Tree, hItem);

		Tree.Close();

		return TRUE;
	}

	return FALSE;
}

void CControlTreeWnd::AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem)
{
	CItem * pItem = GetItemPtr(hItem);

	CString Class = pItem->GetClassName();

	Tree.PutObject(Class.Mid(1));

	Tree.PutValue(L"NDX", pItem->GetIndex());

	pItem->PreCopy();

	pItem->Save(Tree);

	Tree.EndObject();
}

// Data Object Acceptance

BOOL CControlTreeWnd::CanAcceptDataObject(IDataObject *pData, BOOL &fLocal, CLASS &Class)
{
	if( CanAcceptStream(pData, fLocal, Class) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::CanAcceptStream(IDataObject *pData, BOOL &fLocal, CLASS &Class)
{
	FORMATETC Fmt = { WORD(m_cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(Med.hGlobal) ) {

			if( CanAcceptStream(Stream, fLocal, Class) ) {

				Stream.Close();

				ReleaseStgMedium(&Med);

				return TRUE;
			}
		}

		Stream.Close();

		ReleaseStgMedium(&Med);
	}

	return FALSE;
}

BOOL CControlTreeWnd::CanAcceptStream(ITextStream &Stream, BOOL &fLocal, CLASS &Class)
{
	BOOL fAccept = FALSE;

	if( GetStreamInfo(Stream, fLocal) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				if( !Name.IsEmpty() ) {

					// We can only handle one object right now so it's
					// okay that we don't try to distill the class down
					// to the greatest common base...

					Class = AfxNamedClass(PCTXT(L"C" + Name));

					Tree.GetObject();

					while( !Tree.IsEndOfData() ) {

						Tree.GetName();
					}

					Tree.EndObject();

					fAccept = TRUE;

					continue;
				}
			}
		}
	}

	return fAccept;
}

BOOL CControlTreeWnd::CanAcceptClass(CLASS Class, CItem *pRoot)
{
	if( Class->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

		return !pRoot || pRoot == m_pProject->m_pPrograms->m_pObjects;
	}

	if( Class->IsKindOf(AfxRuntimeClass(CControlGlobalVariable)) ) {

		return !pRoot || pRoot == m_pProject->m_pGlobals->m_pObjects;
	}

	if( Class->IsKindOf(AfxRuntimeClass(CControlLocalVariable)) ) {

		return !pRoot || pRoot->GetParent()->IsKindOf(AfxRuntimeClass(CLocalVariables));
	}

	if( Class->IsKindOf(AfxRuntimeClass(CControlInOutVariable)) ) {

		return !pRoot || pRoot->GetParent()->IsKindOf(AfxRuntimeClass(CInOutVariables));
	}

	return FALSE;
}

BOOL CControlTreeWnd::AcceptDataObject(IDataObject *pData, CAcceptCtx const &Ctx)
{
	if( AcceptStream(pData, Ctx) ) {

		ListUpdated(m_hRoot);

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::AcceptStream(IDataObject *pData, CAcceptCtx const &Ctx)
{
	FORMATETC Fmt = { WORD(m_cfData), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(Med.hGlobal) ) {

			BOOL fLocal = FALSE;

			if( GetStreamInfo(Stream, fLocal) ) {

				if( AcceptStream(Stream, Ctx, fLocal) ) {

					ReleaseStgMedium(&Med);

					return TRUE;
				}
			}
		}

		ReleaseStgMedium(&Med);
	}

	return FALSE;
}

BOOL CControlTreeWnd::AcceptStream(ITextStream &Stream, CAcceptCtx const &Ctx, BOOL fLocal)
{
	CTreeFile Tree;

	if( Tree.OpenLoad(Stream) ) {

		CArray <CItem *> Rename;

		HTREEITEM   hPrev = m_MapFixed[Ctx.m_Prev];

		HTREEITEM   hRoot = m_MapFixed[Ctx.m_Root];

		HTREEITEM   hBase = NULL;

		CMetaItem * pRoot = GetItemPtr(hRoot);

		if( pRoot->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

			CControlObjectList *pList = ((CGroupObjects *) pRoot)->m_pObjects;

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				if( !Name.IsEmpty() ) {

					CLASS Class = AfxNamedClass(PCTXT(L"C" + Name));

					UINT  uSize = Class->GetObjectSize();

					PVOID pData = AfxMalloc(uSize, afxFile, afxLine, TRUE);

					if( pData == m_pKilled ) {

						// URGH -- Various things get confused during a move
						// if the new item has the same address as the one
						// we have just deleted, so this horrible hack avoids
						// the problem until I can fix it properly. Note we
						// do this with a block of memory rather than the
						// object itself as that is painful to construct and
						// tear-down without breaking other things.

						PVOID   pMore = AfxMalloc(uSize, afxFile, afxLine, TRUE);

						AfxFree(pData, TRUE);

						pData = pMore;
					}

					CMetaItem *pItem = AfxNewObjectAt(CMetaItem,
									  Class,
									  pData
					);


					Tree.GetObject();

					Tree.GetName();

					UINT uIndex = Tree.GetValueAsInteger();

					pItem->SetIndex(uIndex);

					pItem->SetParent(pList);

					pItem->Load(Tree);

					Tree.EndObject();

					if( TRUE ) {

						Name = pItem->GetName();

						if( Ctx.m_fCheck ) {

							if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

								CControlProgram *pSource = (CControlProgram *) pItem;

								DWORD            dwLang  = pSource->m_Language;

								DWORD            dwSect  = pSource->m_Section;

								CProgramNamer(m_dwProject, dwLang, dwSect).MakeUnique(Name);
							}

							if( pItem->IsKindOf(AfxRuntimeClass(CControlLocalVariable)) ) {

								CControlProgram *pSource = (CControlProgram *) pList->GetParent(2);

								DWORD            dwGroup = pSource->GetLocalGroup();

								CVariableNamer(m_dwProject, dwGroup).MakeUnique(Name);
							}

							if( pItem->IsKindOf(AfxRuntimeClass(CControlInOutVariable)) ) {

								CControlProgram *pSource = (CControlProgram *) pList->GetParent(2);

								DWORD            dwGroup = pSource->GetLocalGroup();

								CVariableNamer(m_dwProject, dwGroup).MakeUnique(Name);
							}

							if( pItem->IsKindOf(AfxRuntimeClass(CControlGlobalVariable)) ) {

								CVariableNamer(m_dwProject, 0).MakeUnique(Name);
							}
						}

						pItem->SetName(Name);
					}

					if( TRUE ) {

						ExpandItem(hRoot);

						AddToList(hRoot, hPrev, pItem);
					}

					if( !Ctx.m_fPaste ) {

						pItem->PostLoad();
					}
					else {
						pItem->PostPaste();

						pItem->PostLoad();
					}

					if( Ctx.m_pNames ) {

						CString Fixed = pItem->GetFixedName();

						Ctx.m_pNames->Append(Fixed);
					}

					if( TRUE ) {

						hPrev = AddToTree(hRoot, hPrev, pItem);
					}

					if( Ctx.m_fUndel ) {

						OnItemDeleted(pItem, FALSE);
					}
					else {
						// REV3 -- We say the item is renamed to
						// ensure that moves recompile the code
						// to reflect the new name. I guess we
						// should really just do this for moves.

						Rename.Append(pItem);
					}

					if( !hBase ) {

						hBase = hPrev;
					}
				}
			}

			if( Ctx.m_fUndel ) {

				OnItemDeleted(NULL, FALSE);
			}

			for( UINT n = 0; n < Rename.GetCount(); n++ ) {

				OnItemRenamed(Rename[n]);
			}

			m_pTree->SelectItem(hBase);

			return TRUE;
		}
	}

	m_pTree->EnsureVisible(m_hSelect);

	return FALSE;
}

BOOL CControlTreeWnd::GetStreamInfo(ITextStream &Stream, BOOL &fLocal)
{
	TCHAR sText[512] = { 0 };

	if( Stream.GetLine(sText, elements(sText)) ) {

		CStringArray List;

		CString(sText).Tokenize(List, '|');

		if( List[0] == m_pItem->GetDatabase()->GetUniqueSig() ) {

			fLocal = TRUE;

			return TRUE;
		}

		fLocal = FALSE;

		return TRUE;
	}

	return FALSE;
}

// Drag Hooks

DWORD CControlTreeWnd::FindDragAllowed(void)
{
	StopAndCommit();

	return DROPEFFECT_COPY | DROPEFFECT_MOVE;
}

// Drop Support

void CControlTreeWnd::DropTrack(CPoint Pos, BOOL fMove)
{
	m_DragPos = Pos;

	Pos   -= m_DragOffset;

	Pos.y += m_DragSize.cy / 2;

	m_pTree->ScreenToClient(Pos);

	if( m_pTree->GetClientRect().PtInRect(Pos) ) {

		HTREEITEM hItem = m_pTree->HitTestItem(Pos);

		HTREEITEM hRoot = NULL;

		HTREEITEM hPrev = NULL;

		if( m_uDrop == dropAccept ) {

			BOOL fExpand = FALSE;

			if( !hRoot && hItem == m_hRoot ) {

				hRoot = m_hRoot;

				hPrev = NULL;
			}

			if( !hRoot && hItem ) {

				if( IsExpandable(hItem) ) {

					if( !m_pTree->GetChild(hItem) || !m_pTree->IsExpanded(hItem) ) {

						CRect Rect = m_pTree->GetItemRect(hItem, TRUE);

						if( Pos.y > Rect.top + 8 ) {

							hRoot = hItem;

							hPrev = NULL;

							fExpand = TRUE;
						}
					}
				}
			}

			if( !hRoot ) {

				HTREEITEM hScan, hBack;

				if( hItem ) {

					hScan = m_pTree->GetPreviousVisible(hItem);

					hBack = m_pTree->GetParent(hItem);
				}
				else {
					hScan = hItem;

					hBack = NULL;

					WalkToLast(hScan);
				}

				if( hScan != hBack ) {

					while( !m_pTree->GetNext(hScan) ) {

						HTREEITEM hNext = m_pTree->GetParent(hScan);

						CRect     Rect  = m_pTree->GetItemRect(hScan, TRUE);

						if( hNext == m_hRoot || Pos.x >= Rect.left - 24 ) {

							hPrev = hScan;

							hRoot = hNext;

							break;
						}

						hScan = hNext;
					}
				}
			}

			if( !hRoot && hItem ) {

				hRoot = m_pTree->GetParent(hItem);

				hPrev = m_pTree->GetPrevious(hItem);
			}

			if( FALSE && !fExpand ) {

				while( hRoot ) {

					if( IsValidRoot(hRoot) ) {

						break;
					}

					if( !hPrev ) {

						hPrev = hRoot;
					}

					hRoot = m_pTree->GetParent(hRoot);
				}

				while( hPrev ) {

					HTREEITEM hTest = m_pTree->GetParent(hPrev);

					if( hTest == hRoot ) {

						break;
					}

					hPrev = hTest;
				}
			}
		}
		else {
			hRoot = hItem;

			hPrev = NULL;
		}

		if( m_hDropRoot != hRoot || m_hDropPrev != hPrev || m_fDropMove != fMove ) {

			ShowDrop(FALSE);

			m_hDropRoot = hRoot;

			m_hDropPrev = hPrev;

			m_fDropMove = fMove;

			if( IsExpandDrop() || m_uDrop == dropOther ) {

				SetTimer(m_timerSelect, 300);
			}
			else
				KillTimer(m_timerSelect);

			ShowDrop(TRUE);

			DropDebug();
		}
	}

	SetTimer(m_timerScroll, 10);
}

BOOL CControlTreeWnd::DropDone(IDataObject *pData)
{
	StopAndCommit();

	if( m_fDropLocal ) {

		if( IsValidDrop() ) {

			CString Menu;

			if( m_fDropMove ) {

				Menu.Format(IDS_MOVE_2, m_pNamed->GetName());

				MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

				OnItemDelete(IDS_MOVE_1, TRUE, FALSE);

				CItem     * pRoot = GetItemPtr(m_hDropRoot);

				CItem     * pPrev = GetItemOpt(m_hDropPrev);

				CCmdPaste * pCmd  = New CCmdPaste(m_pSelect, pRoot, pPrev, pData, TRUE);

				CAcceptCtx  Ctx;

				Ctx.m_Root   = pCmd->m_Root;

				Ctx.m_Prev   = pCmd->m_Prev;

				Ctx.m_fPaste = FALSE;

				Ctx.m_fCheck = TRUE;

				Ctx.m_fUndel = FALSE;

				Ctx.m_pNames = &pCmd->m_Names;

				AcceptDataObject(pCmd->m_pData, Ctx);

				pCmd->m_fInit = FALSE;

				m_System.SaveCmd(pCmd);
			}
			else {
				Menu.Format(IDS_COPY_2, m_pNamed->GetName());

				MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

				OnEditPaste(m_pSelect,
					    m_hDropRoot,
					    m_hDropPrev,
					    pData
				);
			}

			MarkMulti(m_pSelect->GetFixedPath(), Menu, TRUE);

			return TRUE;
		}

		return FALSE;
	}

// LATER -- Do we need more validation?

	if( !m_hDropRoot ) {

		m_hDropRoot = m_hRoot;
	}

	OnEditPaste(m_pSelect,
		    m_hDropRoot,
		    m_hDropPrev,
		    pData
	);

	return TRUE;
}

void CControlTreeWnd::DropDebug(void)
{
	CMetaItem *pRoot = GetItemOpt(m_hDropRoot);

	CMetaItem *pPrev = GetItemOpt(m_hDropPrev);

	CString NameRoot = pRoot ? (pRoot->IsKindOf(AfxRuntimeClass(CMetaItem)) ? pRoot->GetName() : L"Top") : L"Null";

	CString NamePrev = pPrev ? (pPrev->IsKindOf(AfxRuntimeClass(CMetaItem)) ? pPrev->GetName() : L"Top") : L"Null";

	CString TypeRoot = pRoot ? pRoot->GetClassName() : L"";

	CString TypePrev = pPrev ? pPrev->GetClassName() : L"";

	CFormat Text(L"Root=%1 (%2) Prev=%3 (%4)", NameRoot, TypeRoot, NamePrev, TypePrev);

	afxMainWnd->SendMessage(WM_SETSTATUSBAR, WPARAM(PCTXT(Text)));
}

void CControlTreeWnd::ShowDrop(BOOL fShow)
{
	if( m_hDropRoot ) {

		CRect Horz, Vert;

		HTREEITEM hItem;

		if( !m_hDropPrev ) {

			if( IsFolderDrop() || m_uDrop == dropOther ) {

				CTreeViewItem Node(m_hDropRoot);

				Node.SetStateMask(TVIS_DROPHILITED);

				Node.SetState(fShow ? TVIS_DROPHILITED : 0);

				m_pTree->SetItem(Node);

				m_pTree->UpdateWindow();

				return;
			}

			hItem = m_pTree->GetChild(m_hDropRoot);
		}
		else
			hItem = m_pTree->GetNext(m_hDropPrev);

		if( !hItem ) {

			HTREEITEM hDraw = m_hDropPrev;

			CRect     Root  = m_pTree->GetItemRect(hDraw, TRUE);

			WalkToLast(hDraw);

			Horz = m_pTree->GetItemRect(hDraw, TRUE);

			Horz.top    = Horz.bottom -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Root.left   - 20;

			Horz.right  = Root.right  +  8;
		}
		else {
			Horz = m_pTree->GetItemRect(hItem, TRUE);

			Horz.top    = Horz.top    -  1;

			Horz.bottom = Horz.top    +  2;

			Horz.left   = Horz.left   - 20;

			Horz.right  = Horz.right  +  8;
		}

		if( m_hDropPrev ) {

			if( !hItem || hItem != m_pTree->GetNextVisible(m_hDropPrev) ) {

				if( ShowDropVert(m_hDropPrev) ) {

					Vert.left   = Horz.left   -  0;

					Vert.right  = Vert.left   +  2;

					Vert.bottom = Horz.top    -  0;

					Vert.top    = Vert.bottom - 16;
				}
			}
		}

		CClientDC DC(*m_pTree);

		CBrush Brush(afxColor(WHITE), afxColor(BLACK), 128);

		DC.Select(IsValidDrop() ? afxBrush(WHITE) : Brush);

		DC.PatBlt(Horz, PATINVERT);

		DC.PatBlt(Vert, PATINVERT);

		DC.Deselect();
	}
}

BOOL CControlTreeWnd::ShowDropVert(HTREEITEM hItem)
{
	if( m_pTree->GetChild(hItem) ) {

		if( !m_pTree->GetItemState(hItem, TVIS_EXPANDED) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CControlTreeWnd::IsExpandable(HTREEITEM hItem)
{
	CItem *pItem = GetItemPtr(hItem);

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		return TRUE;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

		if( m_DropClass == AfxRuntimeClass(CControlLocalVariable) ) {

			return TRUE;
		}

		if( m_DropClass == AfxRuntimeClass(CControlInOutVariable) ) {

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::IsValidRoot(HTREEITEM hItem)
{
	if( hItem == m_hRoot ) {

		return TRUE;
	}
	else {
		CItem *pItem = GetItemPtr(hItem);

		return pItem->IsKindOf(AfxRuntimeClass(CGroupObjects));
	}
}

BOOL CControlTreeWnd::IsExpandDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
			}

			return FALSE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::IsFolderDrop(void)
{
	if( m_hDropRoot && !m_hDropPrev ) {

		if( m_pTree->GetChild(m_hDropRoot) ) {

			if( !m_pTree->IsExpanded(m_hDropRoot) ) {

				return TRUE;
			}

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::IsDropMove(DWORD dwKeys, DWORD *pEffect)
{
	if( m_uDrop == dropAccept ) {

		if( !IsItemLocked(m_hDropRoot) ) {

			if( m_fDropLocal && !(dwKeys & MK_CONTROL) ) {

				*pEffect = DROPEFFECT_MOVE;

				return TRUE;
			}

			*pEffect = DROPEFFECT_COPY;

			return FALSE;
		}
	}

	if( m_uDrop == dropOther ) {

		// LATER -- Forward to view?

		*pEffect = DROPEFFECT_NONE;

		return FALSE;
	}

	*pEffect = DROPEFFECT_NONE;

	return FALSE;
}

BOOL CControlTreeWnd::IsValidDrop(void)
{
	if( !IsExpandDrop() ) {

		if( m_fDropMove ) {

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

				HTREEITEM hScan = m_hDropRoot;

				while( hScan ) {

					if( hScan == m_hSelect ) {

						return FALSE;
					}

					hScan = m_pTree->GetParent(hScan);
				}
			}

			if( m_fDropLocal ) {

				if( m_hDropRoot == m_pTree->GetParent(m_hSelect) ) {

					if( m_hDropPrev == m_pTree->GetPrevious(m_hSelect) ) {

						return FALSE;
					}

					if( m_hDropPrev == m_hSelect ) {

						return FALSE;
					}
				}
			}
		}

		if( m_hDropRoot ) {

			CItem *pRoot = GetItemPtr(m_hDropRoot);

			if( pRoot->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

				pRoot = ((CGroupObjects *) pRoot)->m_pObjects;

				return CanAcceptClass(m_DropClass, pRoot);
			}

			return FALSE;
		}

		return FALSE;
	}

	return TRUE;
}

// Item Mapping

void CControlTreeWnd::AddToMap(CMetaItem *pItem, HTREEITEM hItem)
{
	BOOL fTest = m_MapFixed.Insert(pItem->GetFixedPath(), hItem);

	AfxAssert(fTest);
}

void CControlTreeWnd::RemoveFromMap(CMetaItem *pItem)
{
	BOOL fTest = m_MapFixed.Remove(pItem->GetFixedPath());

	AfxAssert(fTest);

	if( pItem->IsKindOf(AfxRuntimeClass(CControlSfcProgram)) ) {

		CControlSfcProgram *pProgram = (CControlSfcProgram *) pItem;

		RemoveFromMap(pProgram->m_pChilds);
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

		CControlProgram *pProgram = (CControlProgram *) pItem;

		RemoveFromMap(pProgram->m_pLocals);
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlCallProgram)) ) {

		CControlCallProgram *pProgram = (CControlCallProgram *) pItem;

		RemoveFromMap(pProgram->m_pParams);
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		CGroupObjects *pGroup = (CGroupObjects *) pItem;

		RemoveFromMap(pGroup->m_pObjects);
	}
}

void CControlTreeWnd::RemoveFromMap(CItemIndexList *pList)
{
	INDEX n = pList->GetHead();

	while( !pList->Failed(n) ) {

		CItem *pItem = pList->GetItem(n);

		RemoveFromMap((CMetaItem *) pItem);

		pList->GetNext(n);
	}
}

// Item Addition

void CControlTreeWnd::AddToList(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem)
{
	CMetaItem * pRoot = GetItemPtr(hRoot);

	if( pRoot->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		CControlObjectList *pList  = ((CGroupObjects *) pRoot)->m_pObjects;

		INDEX               Before = NULL;

		if( hPrev ) {

			if( hPrev == hRoot ) {

				Before = NULL;
			}
			else {
				CMetaItem * pNext = GetItemPtr(hPrev);

				Before            = pList->FindItemIndex(pNext);

				pList->GetNext(Before);
			}
		}
		else
			Before = pList->GetHead();

		pList->InsertItem(pItem, Before);

		return;
	}

	AfxAssert(FALSE);
}

HTREEITEM CControlTreeWnd::AddToTree(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	if( hPrev ) {

		if( m_pTree->GetParent(hPrev) != hRoot ) {

			hPrev = TVI_LAST;
		}
	}
	else
		hPrev = TVI_FIRST;

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, hPrev, Node);

	AddToMap(pItem, hItem);

	LoadNodeKids(hItem, pItem);

	return hItem;
}

// Item Hooks

void CControlTreeWnd::NewItemSelected(void)
{
	if( !m_uLocked ) {

		m_System.SetViewedItem(m_pSelect);
	}

	if( m_hSelect ) {

		CMetaItem *pSelect = GetItemPtr(m_hSelect);

		if( pSelect->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

			m_pList = ((CGroupObjects *) pSelect)->m_pObjects;

			m_fList = TRUE;
		}
		else {
			CItem *pParent = pSelect->GetParent();

			if( pParent->IsKindOf(AfxRuntimeClass(CControlObjectList)) ) {

				m_pList = (CControlObjectList *) pParent;

				m_fList = FALSE;
			}
			else {
				m_pList = NULL;

				m_fList = FALSE;
			}
		}

		CLASS           ProgClass = AfxRuntimeClass(CControlProgram);

		CControlProgram *pProgram = (CControlProgram *) pSelect->HasParent(ProgClass);

		if( pProgram ) {

			DWORD dwProgram = pProgram->GetHandle();

			CControlResTreeWnd::m_pThis->SetProgram(dwProgram);
		}
		else
			CControlResTreeWnd::m_pThis->SetProgram(0);
	}

	ShowStatus(TRUE);
}

void CControlTreeWnd::KillItem(HTREEITEM hItem, BOOL fRemove)
{
	if( fRemove ) {

		if( hItem == m_hRoot ) {

			m_MapFixed.Empty();

			return;
		}

		INDEX Index = m_MapFixed.FindData(hItem);

		AfxAssert(!m_MapFixed.Failed(Index));

		m_MapFixed.Remove(Index);
	}
}

BOOL CControlTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"ControlTreeCtxMenu";

		return TRUE;
	}

	Name = L"ControlTreeMissCtxMenu";

	return TRUE;
}

void CControlTreeWnd::AddExtraCommands(CMenu &Menu)
{
	BuildJumpList();

	if( m_JumpList.GetCount() ) {

		CMenu Popup;

		Popup.CreatePopupMenu();

		UINT    uID  = IDM_GO_REFERENCE;

		CString Type = L"";

		for( INDEX n = m_JumpList.GetHead(); !m_JumpList.Failed(n); m_JumpList.GetNext(n) ) {

			CStringArray List;

			m_JumpList[n].Tokenize(List, '|');

			if( Type != List[0] ) {

				if( !Type.IsEmpty() ) {

					Popup.AppendSeparator();
				}

				Type = List[0];
			}

			Popup.AppendMenu(0, uID++, List[1]);
		}

		int nGap = 1;

		int nPos;

		for( nPos = 0; nPos < Menu.GetMenuItemCount(); nPos++ ) {

			UINT uID = Menu.GetMenuItemID(nPos);

			if( HIBYTE(uID) == IDM_EDIT ) {

				break;
			}

			if( uID == 0 ) {

				if( !--nGap ) {

					nPos++;

					break;
				}
			}
		}

		Menu.InsertMenu(nPos++, MF_BYPOSITION, Popup, CString(IDS_JUMP_TO));

		Menu.InsertSeparator(nPos, MF_BYPOSITION);
	}
}

void CControlTreeWnd::OnItemDeleted(CItem *pItem, BOOL fExec)
{
}

BOOL CControlTreeWnd::HasFolderImage(HTREEITEM hItem)
{
	CItem *pItem = GetItemPtr(hItem);

	if( pItem->IsKindOf(AfxRuntimeClass(CGlobalVariables)) ) {

		return FALSE;
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupPrograms)) ) {

		return FALSE;
	}

	return pItem->IsKindOf(AfxRuntimeClass(CGroupObjects));
}

// Item Data

CString CControlTreeWnd::GetItemList(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		return L"Objects";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CGlobalVariables)) ) {

		return L"Objects";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupPrograms)) ) {

		return L"Objects";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

		return L"Objects";
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

		return L"Objects";
	}

	return CString();
}

// Selection Hooks

CString CControlTreeWnd::SaveSelState(void)
{
	// LATER -- Encode expand state of children?

	return m_SelPath;
}

void CControlTreeWnd::LoadSelState(CString State)
{
	HTREEITEM hItem = m_MapFixed[State];

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return;
	}

	SelectRoot();
}

// Lock Support

BOOL CControlTreeWnd::IsItemLocked(HTREEITEM hItem)
{
	return IsReadOnly() || IsItemPrivate(hItem);
}

// Implementation

void CControlTreeWnd::FindCatView(void)
{
	CSysProxy Proxy;

	Proxy.Bind();

	m_pCatView = (CControlCatWnd *) Proxy.GetCatView(AfxRuntimeClass(CControlCatWnd));

	m_pCatView->SetControlNavTree(this);
}

void CControlTreeWnd::Commit(void)
{
	m_pCatView->Commit();
}

void CControlTreeWnd::StopDebug(void)
{
	m_pCatView->StopDebug();
}

void CControlTreeWnd::StopAndCommit(void)
{
	StopDebug();

	Commit();
}

void CControlTreeWnd::WalkToLast(HTREEITEM &hItem)
{
	HTREEITEM hNext;

	while( (hNext = m_pTree->GetNext(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;
	}

	while( (hNext = m_pTree->GetChild(hItem)) && m_pTree->IsVisible(hNext) ) {

		hItem = hNext;

		WalkToLast(hItem);

		break;
	}
}

void CControlTreeWnd::LockUpdate(BOOL fLock)
{
	if( fLock ) {

		if( !m_uLocked++ ) {

			SetRedraw(FALSE);

			m_fRefresh = FALSE;
		}

		return;
	}

	if( !--m_uLocked ) {

		if( m_fRefresh ) {

			RefreshTree();
		}
		else
			SetRedraw(TRUE);

		m_System.SetViewedItem(m_pSelect);

		m_pTree->UpdateWindow();
	}
}

void CControlTreeWnd::SkipUpdate(void)
{
	if( !--m_uLocked ) {

		SetRedraw(TRUE);

		m_pTree->UpdateWindow();
	}
}

void CControlTreeWnd::ListUpdated(HTREEITEM hItem)
{
	if( !m_uLocked ) {

		CMetaItem *pItem = GetItemPtr(hItem);

		m_System.ItemUpdated(pItem, updateChildren);
	}
}

HTREEITEM CControlTreeWnd::FindGroupNode(HTREEITEM hItem)
{
	HTREEITEM  hRoot = hItem;

	CMetaItem *pRoot = GetItemPtr(hRoot);

	while( !pRoot->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		hRoot = m_pTree->GetPrevNode(hRoot, FALSE);

		pRoot = GetItemPtr(hRoot);
	}

	return hRoot;
}

CControlProgram * CControlTreeWnd::FindProgram(void)
{
	HTREEITEM  hRoot = m_hSelect;

	CMetaItem *pRoot = GetItemPtr(hRoot);

	while( !pRoot->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

		hRoot = m_pTree->GetPrevNode(hRoot, FALSE);

		pRoot = GetItemPtr(hRoot);
	}

	return (CControlProgram *) pRoot;
}

CGroupVariables * CControlTreeWnd::FindVariableGroup(void)
{
	HTREEITEM  hRoot = m_hSelect;

	CMetaItem *pRoot = GetItemPtr(hRoot);

	while( !pRoot->IsKindOf(AfxRuntimeClass(CGroupVariables)) ) {

		hRoot = m_pTree->GetPrevNode(hRoot, FALSE);

		pRoot = GetItemPtr(hRoot);
	}

	return (CGroupVariables *) pRoot;
}

BOOL CControlTreeWnd::MarkMulti(CString Item, CString Menu, BOOL fMark)
{
	if( fMark ) {

		CCmd *pCmd = New CCmdMulti(Item, Menu, navOne);

		m_System.ExecCmd(pCmd);

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
}

void CControlTreeWnd::SelectInit(void)
{
	if( !m_System.IsNavBusy() ) {

		SelectRoot();

		return;
	}
}

BOOL CControlTreeWnd::IsControl(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CControlObject));
}

BOOL CControlTreeWnd::IsProgram(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CControlProgram));
}

BOOL CControlTreeWnd::IsProgramGroup(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CGroupPrograms));
}

BOOL CControlTreeWnd::IsVariable(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CControlVariable));
}

BOOL CControlTreeWnd::IsVariableGlobal(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CControlGlobalVariable));
}

BOOL CControlTreeWnd::IsVariableGlobalGroup(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CGlobalVariables));
}

BOOL CControlTreeWnd::IsVariableLocal(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CControlLocalVariable));
}

BOOL CControlTreeWnd::IsVariableLocalGroup(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CLocalVariables));
}

BOOL CControlTreeWnd::IsVariableParam(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CControlInOutVariable));
}

BOOL CControlTreeWnd::IsVariableParamGroup(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CInOutVariables));
}

BOOL CControlTreeWnd::IsVariableGroup(void)
{
	return m_pSelect->IsKindOf(AfxRuntimeClass(CGroupVariables));
}

BOOL CControlTreeWnd::WarnNeedBuild(void)
{
	CString Path = m_pProject->GetProjectPath();

	if( Straton_NeedBuild(Path) ) {

		CString Text;

		Text += CString(IDS_PROJECT_NEEDS_TO);

		Text += CString(IDS_NN);

		Text += CString(IDS_DO_YOU_WANT_TO);

		return YesNo(Text) == IDYES;
	}

	return TRUE;
}

void CControlTreeWnd::ShowStatus(BOOL fShow)
{
	CString Text;

	if( fShow ) {

		if( FALSE ) {

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

				CControlVariable *pVariable = (CControlVariable *) m_pSelect;

				DWORD  dwProject = pVariable->GetProject();

				DWORD dwVariable = pVariable->GetHandle();

				CStratonProperties Props(dwProject, dwVariable);

				CString Profile = Props.Get(CControlVariable::propVarProfile);

				if( !Profile.IsEmpty() ) {

					Text += L"  Profile - ";

					Text += L"[" + Profile + L"]";
				}

				CString Embed = Props.Get(CControlVariable::propVarEmbed);

				if( !Embed.IsEmpty() ) {

					Text += L"  Embed - ";

					Text += L"[" + Embed + L"]";
				}
			}

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CGroupVariables)) ) {

				CGroupVariables *pGroup = (CGroupVariables *) m_pSelect;

				Text += CPrintf(L"  Group %s", pGroup->m_Name);
			}
		}
	}

	afxThread->SetStatusText(Text);
}

void CControlTreeWnd::BuildJumpList(void)
{
	m_JumpList.Empty();

	if( IsVariableGroup() ) {

		HTREEITEM hItem = m_pTree->GetChild(m_hSelect);

		while( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			BuildJumpList((CControlVariable *) pItem);

			hItem = m_pTree->GetNext(hItem);
		}

		return;
	}

	if( IsVariable() ) {

		CControlVariable *pVariable = (CControlVariable *) m_pSelect;

		BuildJumpList(pVariable);
	}
}

void CControlTreeWnd::BuildJumpList(CMetaItem *pItem)
{
	if( pItem ) {

		CMetaList *pList = pItem->FindMetaList();

		UINT       uCount = pList->GetCount();

		for( UINT n = 0; n < uCount; n++ ) {

			CMetaData const *pMeta = pList->FindData(n);

			UINT             uType = pMeta->GetType();

			if( uType == metaCollect ) {

				CItemList *pList = (CItemList *) pMeta->GetObject(pItem);

				if( pList ) {

					INDEX Index = pList->GetHead();

					while( !pList->Failed(Index) ) {

						CItem *pChild = pList->GetItem(Index);

						BuildJumpList(pChild);

						pList->GetNext(Index);
					}
				}
			}

			if( uType == metaObject || uType == metaVirtual ) {

				CItem *pChild = pMeta->GetObject(pItem);

				if( pChild ) {

					BuildJumpList(pChild);
				}
			}
		}
	}
}

void CControlTreeWnd::BuildJumpList(CItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CCodedItem)) ) {

		CCodedItem *pCoded = (CCodedItem *) pItem;

		BuildJumpList(pCoded);
	}
	else {
		if( pItem->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

			CMetaItem *pMeta = (CMetaItem *) pItem;

			BuildJumpList(pMeta);
		}
	}
}

void CControlTreeWnd::BuildJumpList(CCodedItem *pCoded)
{
	IDataServer *pData  = pCoded->GetDataServer();

	UINT	     uCount = pCoded->GetRefCount();

	for( UINT uRef = 0; uRef < uCount; uRef++ ) {

		CDataRef const &Ref = pCoded->GetRef(uRef);

		if( !Ref.t.m_IsTag ) {

			if( !Ref.b.m_Block ) {

				AddToJumpList(Ref.t.m_Index);
			}
		}
		else {
			DWORD  Data = (DWORD &) Ref;

			CTag * pTag = (CTag *) pData->GetItem(Data);

			AddToJumpList(pTag);
		}
	}
}

BOOL CControlTreeWnd::AddToJumpList(UINT uHandle)
{
	// REV3 -- We should abstract this back into the name
	// server somehow so that we can find data logs and
	// other things we don't know about from here...

	CDatabase *pDbase = m_pItem->GetDatabase();

	CItem     *pItem  = pDbase->GetHandleItem(uHandle);

	if( pItem ) {

		CMetaItem *pMeta = (CMetaItem *) pItem;

		AddToJumpList(pMeta);

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::AddToJumpList(CMetaItem *pMeta)
{
	if( pMeta->HasName() ) {

		CString Type = pMeta->GetClassName();

		CString Name = pMeta->GetName();

		CString Path = pMeta->GetFixedPath();

		m_JumpList.Insert(Type + L'|' + Name + L'|' + Path);

		return TRUE;
	}

	return FALSE;
}

BOOL CControlTreeWnd::AddToJumpList(CTag *pTag)
{
	if( pTag ) {

		if( pTag->IsKindOf(AfxRuntimeClass(CTag)) ) {

			CString Type = L"A";

			CString Name = pTag->GetName();

			CString Path = pTag->GetFixedPath();

			m_JumpList.Insert(Type + L'|' + Name + L'|' + Path);

			return TRUE;
		}
	}

	return FALSE;
}

void CControlTreeWnd::HideItem(BOOL fHide)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CControlObject)) ) {

		CControlObject *pObject = (CControlObject *) m_pSelect;

		pObject->SetPrivate(fHide);
	}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		CGroupObjects *pObject = (CGroupObjects *) m_pSelect;

		pObject->SetPrivate(fHide);
	}
}

BOOL CControlTreeWnd::IsItemPrivate(HTREEITEM hItem)
{
	if( IsPrivate() ) {

		return GetItemPtr(hItem)->GetPrivate();
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Create Command
//

// Runtime Class

AfxImplementRuntimeClass(CControlTreeWnd::CCmdCreate, CStdCmd);

// Constructor

CControlTreeWnd::CCmdCreate::CCmdCreate(CItem *pRoot, CString List, CMetaItem *pPrev, CMetaItem *pItem)
{
	m_Menu  = CFormat(IDS_CREATE, pItem->GetName());

	m_Item  = pRoot->GetFixedPath();

	m_List  = List;

	m_Prev  = pPrev->GetFixedPath();

	m_Made  = pItem->GetFixedPath();

	m_hData = pItem->TakeSnapshot();
}

// Destructor

CControlTreeWnd::CCmdCreate::~CCmdCreate(void)
{
	GlobalFree(m_hData);
}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

// Runtime Class

AfxImplementRuntimeClass(CControlTreeWnd::CCmdRename, CStdCmd);

// Constructor

CControlTreeWnd::CCmdRename::CCmdRename(CMetaItem *pItem, CString Prev)
{
	m_Menu = CFormat(IDS_RENAME, pItem->GetName());

	m_Item = pItem->GetFixedPath();

	m_Prev = Prev;

	m_Name = pItem->GetName();
}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Delete Command
//

// Runtime Class

AfxImplementRuntimeClass(CControlTreeWnd::CCmdDelete, CStdCmd);

// Constructor

CControlTreeWnd::CCmdDelete::CCmdDelete(CItem *pRoot, CString List, CItem *pNext, CMetaItem *pItem)
{
	m_Menu  = CFormat(IDS_DELETE_3, pItem->GetName());

	m_Item  = pItem->GetFixedPath();

	m_Root  = GetFixedPath(pRoot);

	m_List  = List;

	m_Next  = GetFixedPath(pNext);

	m_hData = pItem->TakeSnapshot();
}

// Destructor

CControlTreeWnd::CCmdDelete::~CCmdDelete(void)
{
	GlobalFree(m_hData);
}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Paste Command
//

// Runtime Class

AfxImplementRuntimeClass(CControlTreeWnd::CCmdPaste, CStdCmd);

// Constructor

CControlTreeWnd::CCmdPaste::CCmdPaste(CItem *pItem, CItem *pRoot, CItem *pPrev, IDataObject *pData, BOOL fMove)
{
	m_Menu  = CString(IDS_PASTE);

	m_Item  = pItem->GetFixedPath();

	m_Root  = GetFixedPath(pRoot);

	m_Prev  = GetFixedPath(pPrev);

	m_pData = pData;

	m_pData->AddRef();

	m_fMove = fMove;

	m_fInit = TRUE;
}

// Destructor

CControlTreeWnd::CCmdPaste::~CCmdPaste(void)
{
	m_pData->Release();
}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Private Command
//

// Runtime Class

AfxImplementRuntimeClass(CControlTreeWnd::CCmdPrivate, CStdCmd);

// Constructor

CControlTreeWnd::CCmdPrivate::CCmdPrivate(CMetaItem *pItem, BOOL fHide)
{
	m_Menu = CFormat(CString(IDS_MAKE_FMT_PRIVATE), pItem->GetName());

	m_Item = pItem->GetFixedPath();

	CMetaData const *pData = pItem->FindMetaData(L"Private");

	m_fPrev = pData ? pData->ReadInteger(pItem) : FALSE;

	m_fHide = fHide;
}

// End of File
