
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Project Date
//

class CUITextProjDate : public CUITextString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProjDate(void);

	protected:
		// Data
		CControlProject *m_pProject;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Project Date
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProjDate, CUITextString);

// Constructor

CUITextProjDate::CUITextProjDate(void)
{
	m_uFlags &= ~textEdit;
	}

// Overridables

void CUITextProjDate::OnBind(void)
{
	CUITextString::OnBind();

	m_pProject = (CControlProject *) m_pItem;
	}

CString CUITextProjDate::OnGetAsText(void)
{
	DWORD dwProject = m_pProject->GetHandle();

	return afxDatabase->GetProjectDateStamp(dwProject);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Project Path
//

class CUITextProjPath : public CUITextString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProjPath(void);

	protected:
		// Data
		CControlProject *m_pProject;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Project Date
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProjPath, CUITextString);

// Constructor

CUITextProjPath::CUITextProjPath(void)
{
	m_uFlags &= ~textEdit;
	}

// Overridables

void CUITextProjPath::OnBind(void)
{
	CUITextString::OnBind();

	m_pProject = (CControlProject *) m_pItem;

	m_uWidth   = m_uLimit;
	}

CString CUITextProjPath::OnGetAsText(void)
{
	DWORD dwProject = m_pProject->GetHandle();

	return afxDatabase->GetProjectPath(dwProject);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Project Handle
//

class CUITextProjHandle : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProjHandle(void);

	protected:
		// Data
		CControlProject *m_pProject;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Project Handle
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProjHandle, CUITextInteger);

// Constructor

CUITextProjHandle::CUITextProjHandle(void)
{
	m_uFlags &= ~textEdit;
	}

// Overridables

void CUITextProjHandle::OnBind(void)
{
	CUITextInteger::OnBind();

	m_pProject = (CControlProject *) m_pItem;
	}

CString CUITextProjHandle::OnGetAsText(void)
{
	DWORD dwProject = m_pProject->GetHandle();

	return CPrintf("%d", dwProject);
	}

// End of File
