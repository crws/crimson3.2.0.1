
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_NAVTREE_HPP
	
#define	INCLUDE_NAVTREE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CControlCatWnd;

//////////////////////////////////////////////////////////////////////////
//
// Control Project Tree Window
//

class CControlTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CControlTreeWnd(void);

		// Destructor
		~CControlTreeWnd(void);

		// Attributes
		BOOL  CanCheckSyntax(void) const;
		DWORD GetProgramHandle(void) const;

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Static Data
		static UINT    m_timerSelect;
		static UINT    m_timerScroll;
		static CItem * m_pKilled;

		// Drop Codes
		enum
		{
			dropNone,
			dropAccept,
			dropOther
			};

		// Accept Context
		struct CAcceptCtx
		{
			CString        m_Root;
			CString        m_Prev;
			BOOL           m_fPaste;
			BOOL	       m_fCheck;
			BOOL	       m_fUndel;
			CStringArray * m_pNames;
			};

		// Editing Actions
		class CCmdCreate;
		class CCmdRename;
		class CCmdDelete;
		class CCmdPaste;
		class CCmdPrivate;

		// Data Members
		CAccelerator	      m_Accel3;
		CControlCatWnd      * m_pCatView;
		CControlProject     * m_pProject;
		CCommsSystem        * m_pSystem;
		CControlObjectList  * m_pList;
		BOOL		      m_fList;
		DWORD		      m_dwProject;
		CNameMap	      m_MapFixed;
		UINT		      m_uLocked;
		BOOL		      m_fRefresh;
		UINT		      m_cfData;
		CStringTree	      m_JumpList;

		// Drop Context
		UINT	  m_uDrop;
		BOOL	  m_fDropLocal;
		CLASS     m_DropClass;
		BOOL	  m_fDropMove;
		HTREEITEM m_hDropRoot;
		HTREEITEM m_hDropPrev;

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnDatabase(UINT uCode, DWORD dwIdent);

		// Notification Handlers
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTreeReturn(UINT uID, NMHDR &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);

		// Command Handlers
		BOOL OnGoGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnEditGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL OnCtrlGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnCtrlControl(UINT uID, CCmdSource &Src);
		BOOL OnCtrlCommand(UINT uID);

		// Overridables
		void    OnAttach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Go Menu
		BOOL OnGoNext(BOOL fSiblings);
		BOOL OnGoPrev(BOOL fSiblings);
		BOOL StepAway(void);

		// Item Menu : Create
		void OnCreateItem(CMetaItem *pItem);
		void OnExecCreate(CCmdCreate *pCmd);
		void OnUndoCreate(CCmdCreate *pCmd);
		BOOL CanCreateVariable(void);
		BOOL CanCreateProgram(void);
		void OnCreateVariable(DWORD dwGroup, CLASS Class, DWORD dwAttr);
		void OnCreateProgram(DWORD dwParent, DWORD dwCall);
		BOOL CanItemWatch(void);
		void OnItemWatch(void);

		// Item Menu : Private
		BOOL CanItemPrivate(void);
		void OnItemPrivate(void);

		// Item Menu : Rename
		BOOL CanItemRename(void);
		BOOL OnItemRename(CString Name);
		void OnExecRename(CCmdRename *pCmd);
		void OnUndoRename(CCmdRename *pCmd);
		void RenameItem(CString Name, CString Prev);
		
		// Item Menu : Delete
		BOOL CanItemDelete(void);
		void OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop);
		void OnExecDelete(CCmdDelete *pCmd);
		void OnUndoDelete(CCmdDelete *pCmd);

		// Edit Menu : Cut and Copy
		BOOL CanItemCopy(void);
		void OnEditCut(void);
		void OnEditCopy(void);

		// Edit Menu : Paste
		BOOL CanEditPaste(void);
		void OnEditPaste(void);
		void OnEditPaste(IDataObject *pData);
		void OnEditPaste(CItem *pItem, HTREEITEM hRoot, HTREEITEM hPrev, IDataObject *pData);
		void OnExecPaste(CCmdPaste *pCmd);
		void OnUndoPaste(CCmdPaste *pCmd);

		// Edit Menu
		BOOL CanEditDuplicate(void);
		void OnEditDuplicate(void);

		// Ctrl Menu : Convert
		BOOL CanCtrlConvert(UINT uType);
		void OnCtrlConvert(UINT uType);

		// Ctrl Menu : Create
		BOOL CanCtrlCreateProgram(UINT uType);
		BOOL CanCtrlCreateVariable(UINT uType);
		void OnCtrlCreateProgram(UINT uType);
		void OnCtrlCreateVariable(UINT uType);

		// Ctrl Menu
		BOOL CanCtrlAutoName(void);
		BOOL CanCtrlCreate(UINT uType);
		BOOL CanCtrlXRef(void);
		BOOL CanCtrlExec(void);
		void OnCtrlUndoName(void);
		void OnCtrlAutoName(void);
		void OnCtrlCheck(void);
		void OnCtrlXRef(void);
		void OnCtrlExec(void);

		// Tree Loading
		void	  LoadImageList(void);
		void      LoadTree(void);
		void      LoadRoot(void);
		HTREEITEM LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem);
		void      LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem);
		void      LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem);
		void      LoadList(HTREEITEM hRoot, CItemList *pList);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);
		BOOL AddItemToStream(ITextStream &Stream, HTREEITEM hItem);
		void AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem);

		// Data Object Acceptance
		BOOL CanAcceptDataObject(IDataObject *pData, BOOL &fLocal, CLASS &Class);
		BOOL CanAcceptStream(IDataObject *pData, BOOL &fLocal, CLASS &Class);
		BOOL CanAcceptStream(ITextStream &Stream, BOOL &fLocal, CLASS &Class);
		BOOL CanAcceptClass(CLASS Class, CItem *pRoot);
		BOOL AcceptDataObject(IDataObject *pData, CAcceptCtx const &Ctx);
		BOOL AcceptStream(IDataObject *pData, CAcceptCtx const &Ctx);
		BOOL AcceptStream(ITextStream &Stream, CAcceptCtx const &Ctx, BOOL fLocal);
		BOOL GetStreamInfo(ITextStream &Stream, BOOL &fLocal);

		// Drag Hooks
		DWORD FindDragAllowed(void);

		// Drop Support
		void DropTrack(CPoint Pos, BOOL fMove);
		BOOL DropDone(IDataObject *pData);
		void DropDebug(void);
		void ShowDrop(BOOL fShow);
		BOOL ShowDropVert(HTREEITEM hItem);
		BOOL IsExpandable(HTREEITEM hItem);
		BOOL IsValidRoot(HTREEITEM hItem);
		BOOL IsDropMove(DWORD dwKeys, DWORD *pEffect);
		BOOL IsValidDrop(void);
		BOOL IsExpandDrop(void);
		BOOL IsFolderDrop(void);

		// Item Mapping
		void AddToMap(CMetaItem *pItem, HTREEITEM hItem);
		void RemoveFromMap(CMetaItem *pItem);
		void RemoveFromMap(CItemIndexList *pList);

		// Item Addition
		void      AddToList(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem);
		HTREEITEM AddToTree(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem);

		// Item Hooks
		void NewItemSelected(void);
		void KillItem(HTREEITEM hItem, BOOL fRemove);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void AddExtraCommands(CMenu &Menu);
		void OnItemDeleted(CItem *pItem, BOOL fExec);
		BOOL HasFolderImage(HTREEITEM hItem);

		// Item Data
		CString GetItemList(CMetaItem *pItem);

		// Selection Hooks
		CString SaveSelState(void);
		void    LoadSelState(CString State);

		// Lock Support
		BOOL IsItemLocked(HTREEITEM hItem);

		// Object Location
		HTREEITEM         FindGroupNode(HTREEITEM hItem);
		CControlProgram * FindProgram(void);
		CGroupVariables * FindVariableGroup(void);

		// Implementation
		void FindCatView(void);
		void Commit(void);
		void StopDebug(void);
		void StopAndCommit(void);
		void WalkToLast(HTREEITEM &hItem);
		void LockUpdate(BOOL fLock);
		void SkipUpdate(void);
		void ListUpdated(HTREEITEM hItem);
		BOOL MarkMulti(CString Item, CString Menu, BOOL fMark);
		BOOL IsDown(UINT uCode);
		void SelectInit(void);
		BOOL WarnNeedBuild(void);
		BOOL IsControl(void);
		BOOL IsProgram(void);
		BOOL IsProgramGroup(void);
		BOOL IsVariable(void);
		BOOL IsVariableGlobal(void);
		BOOL IsVariableGlobalGroup(void);
		BOOL IsVariableLocal(void);
		BOOL IsVariableLocalGroup(void);
		BOOL IsVariableParam(void);
		BOOL IsVariableParamGroup(void);
		BOOL IsVariableGroup(void);
		void ShowStatus(BOOL fShow);
		void BuildJumpList(void);
		void BuildJumpList(CMetaItem *pItem);
		void BuildJumpList(CItem *pItem);
		void BuildJumpList(CCodedItem *pCoded);
		BOOL AddToJumpList(UINT uHandle);
		BOOL AddToJumpList(CMetaItem *pMeta);
		BOOL AddToJumpList(CTag *pTag);
		void HideItem(BOOL fHide);
		BOOL IsItemPrivate(HTREEITEM hItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Create Command
//

class DLLAPI CControlTreeWnd::CCmdCreate : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdCreate(CItem *pRoot, CString List, CMetaItem *pPrev, CMetaItem *pItem);

		// Destructor
		~CCmdCreate(void);

		// Data Members
		CString m_Made;
		CString m_List;
		CString m_Prev;
		HGLOBAL m_hData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

class CControlTreeWnd::CCmdRename : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdRename(CMetaItem *pItem, CString Prev);

		// Data Members
		CString m_Prev;
		CString m_Name;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Delete Command
//

class CControlTreeWnd::CCmdDelete : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdDelete(CItem *pRoot, CString List, CItem *pNext, CMetaItem *pItem);

		// Destructor
		~CCmdDelete(void);

		// Data Members
		CString m_Root;
		CString m_List;
		CString m_Next;
		HGLOBAL m_hData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Paste Command
//

class CControlTreeWnd::CCmdPaste : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdPaste( CItem       * pItem,
			   CItem       * pRoot,
			   CItem       * pPrev,
			   IDataObject * pData,
			   BOOL          fMove
			   );

		// Destructor
		~CCmdPaste(void);

		// Data Members
		CString       m_Root;
		CString       m_Prev;
		IDataObject * m_pData;
		BOOL          m_fInit;
		BOOL	      m_fMove;
		CStringArray  m_Names;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Private Command
//

class CControlTreeWnd::CCmdPrivate : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdPrivate(CMetaItem *pItem, BOOL fHide);

		// Data Members
		BOOL m_fPrev;
		BOOL m_fHide;
	};

// End of File

#endif
