
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_INTERN_HPP

#define INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3control.hpp"

#include "intern.hxx"

#include "..\build.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CControlObject;
class CControlObjectList;
class CGroupObjects;

//////////////////////////////////////////////////////////////////////////
//
// Control Object
//

class CControlObject : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlObject(void);

		// Development
		virtual void AddTest(void);

		// Overridables
		virtual CString GetTreeLabel(void) const;
		virtual UINT    GetTreeImage(void) const;
		virtual void    Validate(void);

		// Overridables
		virtual BOOL OnRename (CError &Error, CString Name);
		virtual BOOL CanRename(CError &Error, CString Name);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);
		void Load(CTreeFile &File);

		// Attributes
		virtual DWORD	GetHandle(void);
		virtual DWORD	GetProject(void);
		virtual CString	GetStoragePath(void);

		// Item Privacy
		UINT GetPrivate(void);

		// Overridable
		virtual void SetPrivate(UINT uHide);

		// Item Properties
		CString	m_Name;
		DWORD	m_Ident;

	protected:
		// Data
		CControlProject * m_pProject;

		// Meta Data Creation
		void AddMetaData(void);		

		// Implementation
		void FindProject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Object List
//

class CControlObjectList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlObjectList(void);

		// Item Location
		CControlObject * GetItem(INDEX n);

		// Overridable
		void SetPrivate(UINT uHide);

	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Objects Group
//

class CGroupObjects : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGroupObjects(void);

		// Overridables
		virtual CString GetTreeLabel(void) const;
		virtual UINT    GetTreeImage(void) const;
		virtual void    Validate(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);
		void Load(CTreeFile &File);

		// Item Privacy
		UINT GetPrivate(void);

		// Overridable
		void SetPrivate(UINT uHide);

		// Item Properties
		UINT		     m_Private;
		CControlObjectList * m_pObjects;

	protected:
		// Data
		CControlProject	* m_pProject;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindProject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CGroupVariables;

//////////////////////////////////////////////////////////////////////////
//
// Program Code
//

class CControlProgramCode : public CItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlProgramCode(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Program
//

class CControlProgram : public CControlObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlProgram(void);

		// Destructor
		~CControlProgram(void);

		// Development
		void AddTest(void);

		// Overridables
		UINT GetTreeImage(void) const;
		void Validate(void);

		// Overridables
		void OnConvert(void);
		BOOL OnRename(CError &Error, CString Name);
		BOOL CanRename(CError &Error, CString Name);

		// Attributes
		CLASS            GetEditorClass(void);
		DWORD	         GetHandle(void);
		DWORD	         GetProject(void);
		DWORD            GetLocalGroup(void);
		CStratonObject * GetProgram(void);
		CString	         GetStoragePath(void);

		// Item LookUp
		virtual BOOL FindVariable(CControlVariable * &pVariable, CString Name);
		virtual BOOL FindVarGroup(CGroupVariables * &pGroup, CString Name);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Overridable
		void SetPrivate(UINT uHide);

		// Persistance
		void Init(void);
		void PreCopy(void);
		void PostPaste(void);
		void PostLoad(void);
		void Save(CTreeFile &File);
		void Kill(void);
		void Load(CTreeFile &File);

		// Item Properties
		DWORD			  m_Language;
		DWORD			  m_Section;
		DWORD			  m_Parent;
		UINT			  m_Private;
		CControlProgramCode	* m_pCode;
		CGroupVariables		* m_pLocals;
		CControlFile		* m_pSourceFile;
		CControlFile		* m_pDefineFile;
		CControlFile		* m_pLocalsFile;

		// Properties
		enum
		{
			propProgCheck = 5,
			};

		// Language
		enum
		{
			langSFC	= 1,
			langST	= 2,
			langFBD	= 4,
			langLD	= 8,
			langIL	= 16,
			};

		// Section
		enum
		{
			sectBegin	= 1,
			sectSFCMain	= 2,
			sectEnd		= 4,
			sectSFCChild	= 8,
			sectUDFB	= 16,
			sectMain	= sectBegin | sectSFCMain | sectEnd,
			};

	protected:
		// Data
		CStratonProgram * m_pProgram;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void Bind(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Main Program
//

class CControlMainProgram : public CControlProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlMainProgram(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		UINT GetTreeImage(void) const;

		// Persistance
		void Init(void);

		// Item Properties
		UINT m_Enable;
		UINT m_Period;
		UINT m_Offset;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Call Program
//

class CControlCallProgram : public CControlProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlCallProgram(void);

		// Development
		void AddTest(void);

		// Overridables
		BOOL OnRename(CError &Error, CString Name);

		// Item Lookup
		BOOL FindVariable(CControlVariable * &pVariable, CString Name);
		BOOL FindVarGroup(CGroupVariables * &pGroup, CString Name);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridable
		void SetPrivate(UINT uHide);

		// Item Properties
		UINT		  m_Enable;
		CGroupVariables * m_pParams;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Sub Program
//

class CControlSubProgram : public CControlCallProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlSubProgram(void);

		// Overridables
		UINT GetTreeImage(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Implementation
		void SetOnCallFlag(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// UDFB Program
//

class CControlUdfbProgram : public CControlCallProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlUdfbProgram(void);

		// Overridables
		UINT GetTreeImage(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Exception Program
//

class CControlExceptionProgram : public CControlProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlExceptionProgram(void);

		// Overridables
		UINT GetTreeImage(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// SFC Program
//

class CControlSfcProgram : public CControlProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlSfcProgram(void);

		// Attributes
		BOOL HasChildren(void);

		// Overridables
		UINT GetTreeImage(void) const;
		void Validate(void);

		// Item Lookup
		BOOL FindVariable(CControlVariable * &pVariable, CString Name);
		BOOL FindVarGroup(CGroupVariables  * &pGroup, CString Name);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Properties
		UINT		 m_Enable;
		CGroupPrograms * m_pChilds;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SFC Main Program
//

class CControlSfcMainProgram : public CControlSfcProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlSfcMainProgram(void);

		// Development
		void AddTest(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Init(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SFC Child Program
//

class CControlSfcChildProgram : public CControlSfcProgram
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlSfcChildProgram(void);

		// Development
		void AddTest(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
	};

//////////////////////////////////////////////////////////////////////////
//
// Group Program Manager
//

class CGroupPrograms : public CGroupObjects
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGroupPrograms(void);

		// Development
		virtual void AddTest(void);		

		// Object Look up
		BOOL FindProgram (CControlProgram * &pProgram, CString Name);
		BOOL FindVariable(CControlVariable * &pVariable, CString Name);
		BOOL FindVarGroup(CGroupVariables * &pGroup, CString Name);
		BOOL GetVarGroup(CGroupVariables * &pGroup, UINT uIndex);

		// Overridables
		CString GetTreeLabel(void) const;
		UINT    GetTreeImage(void) const;
		void    Validate(void);

		// Item Properties
		CString m_Name;

	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void AddInit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Group Child Programs
//

class CGroupChildPrograms : public CGroupPrograms
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGroupChildPrograms(void);

		// Development
		void AddTest(void);

		// Overridables
		CString GetTreeLabel(void) const;
		UINT    GetTreeImage(void) const;

	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CVariableInitial;

//////////////////////////////////////////////////////////////////////////
//
// Control Variable
//

class CControlVariable : public CControlObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlVariable(void);

		// Destructor
		~CControlVariable(void);

		// Overridables
		UINT GetTreeImage(void) const;
		void Validate(void);

		// Overridables
		BOOL OnRename(CError &Error, CString Name);
		BOOL CanRename(CError &Error, CString Name);
		void OnNewIdent(DWORD dwIdent);

		// Attributes
		DWORD GetHandle(void);
		DWORD GetProject(void);
		DWORD GetGroup(DWORD dwProject);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Name Hint
		BOOL GetNameHint(CString Tag, CString &Name);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Naming
		CString GetItemOrdinal(void) const;

		// Persistance
		void Init(void);
		void Load(CTreeFile &File);
		void PostLoad(void);
		void Kill(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Attributes
		enum
		{
			attrEmpty	= 0x0000,
			attrReadOnly	= 0x0001,
			attrInput	= 0x0002,
			attrOutput	= 0x0004,
			attrFbInput	= 0x0008,
			attrFbOutput	= 0x0010,
			attrExtern	= 0x0020,
			attrInOut	= 0x0040,
			attrParam	= attrInput | attrOutput | attrFbInput | attrFbOutput | attrExtern | attrInOut,
			};

		// Properties
		enum
		{
			propVarEmbed	= 1,
			propVarProfile	= 2,
			};

		// Type
		enum
		{
			typeVarBasic	= 1,
			typeVarString	= 2,
			};

		// Item Properties
		CCodedItem	  * m_pValue;
		UINT		    m_Extent;
		UINT		    m_Persist;
		UINT		    m_TypeIdent;
		CString		    m_TypeName;
		UINT		    m_Length;
		DWORD	            m_Flags;
		UINT		    m_Private;
		CVariableInitial  * m_pInitial;

	protected:
		// Data
		CStratonVariable * m_pVariable;
		CGroupVariables  * m_pGroup;

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Binding
		void Bind(void);

		// Implementation
		void FindGroup(void);
		void DoEnables(IUIHost *pHost);
		BOOL MakeReadOnly(IUIHost *pHost);
		BOOL CheckPersist(IUIHost *pHost);
		void CheckInitial(IUIHost *pHost);
		void Check64BitValue(IUIHost *pHost);
		void UpdateEmbed(void);
		BOOL SetInitialClass(CVariableInitial * &pInitial, CLASS Class);
		void SetInitialClass(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Global Variable
//

class CControlGlobalVariable : public CControlVariable
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlGlobalVariable(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Local Variable
//

class CControlLocalVariable : public CControlVariable
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlLocalVariable(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parameter Variable
//

class CControlInOutVariable : public CControlVariable
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlInOutVariable(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CVariableInitialFormatter;
class CVariableInitialFormatterString;
class CVariableInitialFormatterTime;
class CVariableInitialFormatterNumber;
class CVariableInitialFormatterBoolean;
class CVariableInitialFormatterReal;

//////////////////////////////////////////////////////////////////////////
//
// Formatter
//

class CVariableInitialFormatter
{
	public:
		// Constructor
		CVariableInitialFormatter(void);
		
		// Operations
		virtual CString ToStraton(CString Text);
		virtual CString ToCrimson(CString Text);

		// UI Schema
		CLASS GetUIClassText(void) const;
		CLASS GetUIClassUI(void) const;
		CString GetUIFormat(void) const;

	protected:
		// Data Members
		CString	m_ClassText;
		CString	m_ClassUI;
		CString	m_Format;
	};

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- String
//

class CVariableInitialFormatterString : public CVariableInitialFormatter
{
	public:
		// Constructor
		CVariableInitialFormatterString(void);

		// Operations
		CString ToStraton(CString Text);
		CString ToCrimson(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- Time
//

class CVariableInitialFormatterTime : public CVariableInitialFormatter
{
	public:
		// Constructor
		CVariableInitialFormatterTime(void);

		// Operations
		CString ToStraton(CString Text);
		CString ToCrimson(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- Number
//

class CVariableInitialFormatterNumber : public CVariableInitialFormatter
{
	public:
		// Constructor
		CVariableInitialFormatterNumber(void);

		// Operations
		CString ToStraton(CString Text);
		CString ToCrimson(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- Boolean
//

class CVariableInitialFormatterBoolean : public CVariableInitialFormatter
{
	public:
		// Constructor
		CVariableInitialFormatterBoolean(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- Real
//

class CVariableInitialFormatterReal : public CVariableInitialFormatter
{
	public:
		// Constructor
		CVariableInitialFormatterReal(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CVariableInitial;
class CVariableInitialSingle;
class CVariableInitialSingleBoolean;
class CVariableInitialSingleString;
class CVariableInitialSingleTime;
class CVariableInitialSingleNumber;
class CVariableInitialSingleReal;
class CVariableInitialArray;
class CVariableInitialArrayBoolean;
class CVariableInitialArrayString;
class CVariableInitialArrayTime;
class CVariableInitialArrayNumber;
class CVariableInitialArrayReal;

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value
//

class CVariableInitial : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Class Enumeration
		static CLASS GetClass(CStratonDataTypeDescriptor const &Type, BOOL fArray);

		// Constructor
		CVariableInitial(void);

		// Destructor
		~CVariableInitial(void);

		// Operations
		virtual void Preserve(CVariableInitial *pOld);
		virtual BOOL CheckRange(void);

		// Formatter
		CVariableInitialFormatter & GetFormat(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Public Data
		CControlVariable * m_pVariable;

		// Item Properties
		CString		   m_Type;

	protected:
		// Data Members
		CVariableInitialFormatter * m_pFormat;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindVariable(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value
//

class CVariableInitialSingle : public CVariableInitial
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialSingle(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void Preserve(CVariableInitial *pOld);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Item Properties
		CString	m_Data;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void Commit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Boolean
//

class CVariableInitialSingleBoolean : public CVariableInitialSingle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialSingleBoolean(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value String
//

class CVariableInitialSingleString : public CVariableInitialSingle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialSingleString(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Time
//

class CVariableInitialSingleTime : public CVariableInitialSingle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialSingleTime(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Number
//

class CVariableInitialSingleNumber : public CVariableInitialSingle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialSingleNumber(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
		BOOL CheckRange(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Real
//

class CVariableInitialSingleReal : public CVariableInitialSingle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialSingleReal(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

class CVariableInitialArray : public CVariableInitial
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialArray(void);

		// Operations
		CString ToStraton(void);
		void    ToCrimson(CString Text);

		// Operations
		void Preserve(CVariableInitial *pOld);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Item Properties
		CString	m_Data[1024];

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void    Commit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

class CVariableInitialArrayBoolean : public CVariableInitialArray
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialArrayBoolean(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

class CVariableInitialArrayString : public CVariableInitialArray
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialArrayString(void);

		// Operations
		void Preserve(CVariableInitial *pOld);	
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

class CVariableInitialArrayTime : public CVariableInitialArray
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialArrayTime(void);

		// Operations
		void Preserve(CVariableInitial *pOld);	
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array -- Number
//

class CVariableInitialArrayNumber : public CVariableInitialArray
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialArrayNumber(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
		BOOL CheckRange(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array -- Real
//

class CVariableInitialArrayReal : public CVariableInitialArray
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CVariableInitialArrayReal(void);

		// Operations
		void Preserve(CVariableInitial *pOld);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CGroupVariables;
class CGlobalVariables;
class CLocalVariables;
class CInOutVariables;

//////////////////////////////////////////////////////////////////////////
//
// Group Variable Manager
//

class CGroupVariables : public CGroupObjects
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGroupVariables(void);

		// Development
		virtual void AddTest(void);

		// Overridables
		CString GetTreeLabel(void) const;
		void    Validate(void);

		// Attributes
		CLASS   GetClass(void);
		CString GetClassName(void);

		// Item Lookup
		BOOL FindVariable(CControlVariable * &pVariable, CString Name);

		// Item Properties
		CString m_Name;

	protected:
		// Data
		CLASS	m_Class;
		CString m_ClassName;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Global Variable Manager
//

class CGlobalVariables : public CGroupVariables
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGlobalVariables(void);

		// Development
		void AddTest(void);

		// Overridables
		CString GetTreeLabel(void) const;
		UINT    GetTreeImage(void) const;

	protected:
		// Implementation

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Local Variable Manager
//

class CLocalVariables : public CGroupVariables
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLocalVariables(void);

		// Development
		void AddTest(void);

		// Overridables
		CString GetTreeLabel(void) const;
		UINT    GetTreeImage(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Item Properties
		DWORD	m_Ident;

	protected:
		// Data
		CControlProgram	* m_pProgram;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindProgram(void);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Parameter Variable Manager
//

class CInOutVariables : public CLocalVariables
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CInOutVariables(void);

		// Overridables
		CString GetTreeLabel(void) const;
		UINT    GetTreeImage(void) const;

	protected:
		// Implementation
		void AddTest(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRegistryList;
class CLibraryItem;
class CRegistryItem;
class CControlItem;
class CFunctionBlocksManager;
class CControlCatWnd;

//////////////////////////////////////////////////////////////////////////
//
// Registry Item List
//

class CRegistryList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRegistryList(void);

		// Operations
		void LoadFromDisk(void);
		void AddCategory(CString Name);

		// Block
		enum
		{
			regStdOp	= 0x00000001,
			regStdFunc	= 0x00000002,	
			regStdFb	= 0x00000004,
			regCFunc	= 0x00000020,
			regCFb		= 0x00000040,
			regUdfb		= 0x00000400,
			regFunc		= 0x00000022,
			regFb		= 0x00000444,
			regAll		= 0x00000FFF,
			};

	protected:
		// Implementation
		void LoadFromDisk(DWORD dwType);
		void AddCategory(DWORD dwIdent);
	};

//////////////////////////////////////////////////////////////////////////
//
// Library Item
//

class CLibraryItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLibraryItem(void);

		// Attributes
		virtual UINT GetTreeImage(void) const;

		// Overridables
		virtual void OnCreate(void); // Come back to this!!!!
		virtual void OnCreate(CString Root);

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// Data Members
		UINT            m_Ident;
		CString		m_Name;

	protected:

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Registry Item
//

class CRegistryItem : public CLibraryItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRegistryItem(void);

		// Overridables
		void OnCreate(void);
		void OnCreate(CString Root);
	};

//////////////////////////////////////////////////////////////////////////
//
// Database Item
//

class CControlItem : public CLibraryItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlItem(void);

		// Attributes
		UINT GetTreeImage(void) const;

		// Overridables
		void OnCreate(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

	protected:
		// Data
		CControlProject *m_pProject;

		// Implementation
		void FindProject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Blocks Manager
//

class CFunctionBlocksManager : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFunctionBlocksManager(void);

		// Operations
		void Update(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Item Properties
		CRegistryList * m_pLibrary;

	protected:
		// Data
		CControlProject * m_pProject;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void FindProject(void);
		void LoadPrograms(void);
		void LoadProject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Compiler
//

class CControlCompiler : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlCompiler(CControlProject *pProject);

		// Operations
		void Check(DWORD dwProgram);
		BOOL Build(UINT uCode = compVerbose);
		void Clean(void);
		BOOL NeedBuild(void);

		//
		enum {
			compSilent	= 0,
			compErrors	= 1,
			compSuccess	= 2,
			compVerbose	= 3,
			compConvert	= 4,
			};

	protected:
		// Data
		CControlProject * m_pProject;

		// Implementation
		BOOL BuildCheckFile(CFilename File, CString Name, BOOL fUDFB, BOOL &fAlien);
	};

//////////////////////////////////////////////////////////////////////////
//
// Cross Reference
//

class CControlCrossReference : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlCrossReference(CControlProject *pProject);

		// Operations
		UINT FindInFiles(CString Find);

	protected:
		// Data
		CControlProject * m_pProject;

		// Implementation
		BOOL BuildUsageFile(CFilename File, CString Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Build File
//

class CBuildFile : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CBuildFile(CControlProject *pProject);

		// Operations
		BOOL OpenLoad(ITextStream &Stream);
		void BuildFindList(CString Title);
		void ClearFindList(void);

	protected:
		// Data
		CControlProject * m_pProject;
		ITextStream     * m_pStream;
		TCHAR		  m_sLine[1024];
		CStringArray	  m_Errors;
		
		// Overridable
		virtual BOOL BuildList(CStringArray &List);

		// Implementation
		void ParseFile(void);
		BOOL ReadLine(void);
		BOOL IsError(CString Text);
		BOOL LoadVariableError(CStringArray &List, CGroupPrograms *pGroup, CString Name, CString Desc, CString Line);
		BOOL LoadVariableError(CStringArray &List, CGroupVariables *pGroup, CString Name, CString Work, CString Line);
		BOOL LoadVariableError(CStringArray &List, CString Name, CString Desc, CString Line);
		BOOL LoadProgramError (CStringArray &List, CString Name, CString Desc, CString Line);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Object
//

class CControlFile : public CMetaItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlFile(void);

		// Attributes
		BOOL IsEmpty(void) const;

		// Operations
		void ChangeBase(CFilename Name);
		void SetFullName(CFilename Name);
		BOOL SaveToDisk(void);
		BOOL LoadFromDisk(void);

		// Persistance
		void Init(void);
		void Load(CTreeFile &File);
		void PreCopy(void);
		void Kill(void);

		// Item Properties
		CFilename  m_Path;
		CFilename  m_Name;
		CWordArray m_Data;
		CByteArray m_Blob;

	protected:
		// Data Members
		CControlProject * m_pProject;

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;
		
		// Overridables
		virtual void FettleBlobOnLoad(void);
		virtual void FettleBlobOnSave(void);

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindProject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Object - Project Level
//

class CProjectFile : public CControlFile
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CProjectFile(PCTXT pSuffix);

		// Persistance
		void Init(void);

	protected:
		// Item Properties
		CString	m_Suffix;

		// Overridables
		void FettleBlobOnLoad(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Object - Project Level
//

class CExecuteFile : public CProjectFile
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CExecuteFile(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Handle;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Overridables
		void FettleBlobOnLoad(void);

		// Implementation
		DWORD StratonCRC32(PCBYTE pData, UINT uData);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Object - Project Level
//

class CCompileFile : public CProjectFile
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCompileFile(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

	protected:
		// Implementation
		void  SetFixedContent(void);
		PCSTR GetFixedContent(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Object - Program Level
//

class CProgramFile : public CControlFile
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CProgramFile(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Object - Program Source File
//

class CSourceFile : public CProgramFile
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSourceFile(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Object - Program Source File
//

class CDefineFile : public CProgramFile
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDefineFile(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Naming Help
//

extern CString	Variable_FindNextName(DWORD dwProject, PCTXT pFormat);

extern CString	Program_FindNextName(DWORD dwProject, DWORD dwLanguage, DWORD dwSection, DWORD dwParent, PCTXT pFormat);

//////////////////////////////////////////////////////////////////////////
//
// Control Program Editor
//

class CControlProgramCtrlWnd : public CCtrlWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CControlProgramCtrlWnd(void);

		// Attributes
		BOOL IsDirty(void) const;

		// Operations
		void Attach(CItem *pItem);
		void Exec(CCmd *pCmd);
		void Undo(CCmd *pCmd);
		void LocateError(CString Error);
		void SetDebug(BOOL fDebug, BOOL fOnline);
		void Commit(void);
		void Reload(void);
		void DetachEditor(CItem *pItem);

	protected:
		// Data
		CControlProgram	 * m_pItem;
		CControlProject  * m_pProject;
		CControlCatWnd   * m_pCatView;
		CEditorWnd       * m_pEdit;
		BOOL		   m_fDebug;
		BOOL		   m_fOnline;
		BOOL		   m_fAttach;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Wnd);
		void OnPaint(void);

		// Implementation
		void  FindCatView(void);
		void  CheckEditor(void);
		CRect GetEditorRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Program Execution Manager
//

class CProgramExecution : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramExecution(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Project Cycle Time
//

class CCycleTimeItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCycleTimeItem(void);

		// Operation
		void Make(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Item Properties
		UINT	m_Scan;

	protected:
		// Data
		CControlProject *m_pProject;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindProject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// License Dialog
//

class CLicenseDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CLicenseDialog(BOOL fWarn = FALSE);
		
	protected:
		// Data Members
		BOOL	m_fWarn;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		BOOL OnSelect(UINT uID);

		// Implementation
		void SaveData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Mapping
//

class CVariableMapping : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CVariableMapping(CControlProject * pProject);

		// Operations
		BOOL Export(CListView &List);
		BOOL Export(ITextStream &Stm, WCHAR cSep);

	protected:
		// Type
		typedef CArray <CControlVariable *> CVarList;

		// Data
		CControlProject * m_pProject;
		CVarList	  m_List;


		// Implementation
		BOOL FindVariables(void);
		void WriteHead(ITextStream &Stm, WCHAR cSep);
		void WriteLine(ITextStream &Stm, WCHAR cSep, UINT i);
		void WriteLine(CListView &List, UINT i);
	};

//////////////////////////////////////////////////////////////////////////
//								
// Variable Mapping Dialog
//

class CVariableMappingDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CVariableMappingDialog(CControlProject *pProject);

	public:
		// Data
		CControlProject * m_pProject;
		CListView       * m_pView;
			 
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnExport(UINT uID);

		// Implementation
		void MakeList(void);
		void LoadList(void);
		void ShowList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Number
//

class CUITextNumber : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextNumber(void);

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

	protected:
		// Data Members
		CString	m_Min;
		CString	m_Max;
		UINT	m_uPlaces;

		// Implementation
		BOOL    Check (CError &Error, CString &Data);
		CString Format(PCTXT pData);
		CString Parse (PCTXT pText);
	};

// End of File

#endif
