
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Language
//

class CUITextProgLang : public CUITextEnumIndexed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProgLang(void);

	protected:
		// Overridables
		UINT OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Language
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProgLang, CUITextEnumIndexed);

// Constructor

CUITextProgLang::CUITextProgLang(void)
{
	m_uFlags &= ~textEdit;
	}

// Overridables

UINT CUITextProgLang::OnSetAsText(CError &Error, CString Text)
{
	return saveSame;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Section
//

class CUITextProgSect : public CUITextEnumIndexed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProgSect(void);

	protected:
		// Overridables
		UINT OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Section
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProgSect, CUITextEnumIndexed);

// Constructor

CUITextProgSect::CUITextProgSect(void)
{
	m_uFlags &= ~textEdit;
	}

// Overridables

UINT CUITextProgSect::OnSetAsText(CError &Error, CString Text)
{
	return saveSame;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Schedule
//

class CUITextProgSched : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProgSched(void);

	protected:
		// Data Members
		CString	m_Prop;
		DWORD	m_dwProject;
		DWORD	m_dwProgram;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Overridables
		UINT OnSetAsText(CError &Error, CString Text);

		// Implementation
		BOOL WriteData(CError &Error, UINT uData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Schedule
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProgSched, CUITextInteger);

// Constructor

CUITextProgSched::CUITextProgSched(void)
{
	}

// Overridables

void CUITextProgSched::OnBind(void)
{
	CUITextInteger::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	m_Prop   = List[5];

	CControlProgram * pProgram = (CControlProgram *) m_pItem;

	m_dwProject     = pProgram->GetProject();

	m_dwProgram     = pProgram->GetHandle();
	}

void CUITextProgSched::OnRebind(void)
{
	CUITextElement::OnRebind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	m_Prop   = List[5];

	CControlProgram * pProgram = (CControlProgram *) m_pItem;

	m_dwProject     = pProgram->GetProject();

	m_dwProgram     = pProgram->GetHandle();
	}

// Overridables

UINT CUITextProgSched::OnSetAsText(CError &Error, CString Text)
{
	UINT uPrev = m_pData->ReadInteger(m_pItem);

	UINT uData = Parse(Text);

	uData      = DispToStore(uData);

	if( uData - uPrev ) {

		if( WriteData(Error, uData) ) {

			m_pData->WriteInteger(m_pItem, uData);

			return saveChange;
			}

		return saveError;
		}

	return saveSame;
	}

// Implementation

BOOL CUITextProgSched::WriteData(CError &Error, UINT uData)
{
	CStratonProgramSchedule Desc(m_dwProject, m_dwProgram);

	if( m_Prop == L"p" ) {

		Desc.m_dwPeriod = uData;
		}

	if( m_Prop == L"o" ) {

		Desc.m_dwOffset = uData;
		}

	if( Desc.Set() ) {
			
		return TRUE;
		}
		
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Execution
//

class CUITextProgExec : public CUITextEnumIndexed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProgExec(void);

	protected:
		// Data Members
		DWORD	m_dwProject;
		DWORD	m_dwProgram;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Overridables
		UINT OnSetAsText(CError &Error, CString Text);

		// Implementation
		BOOL OnWriteData(UINT uData);
		BOOL OnProgramEnable(BOOL fEnable);
		BOOL OnProgramCalled(BOOL fEnable);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Execution
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProgExec, CUITextEnumIndexed);

// Constructor

CUITextProgExec::CUITextProgExec(void)
{
	}

// Overridables

void CUITextProgExec::OnBind(void)
{
	CUITextEnumIndexed::OnBind();

	CControlProgram * pProgram = (CControlProgram *) m_pItem;

	m_dwProject     = pProgram->GetProject();

	m_dwProgram     = pProgram->GetHandle();
	}

void CUITextProgExec::OnRebind(void)
{
	CUITextEnumIndexed::OnRebind();

	CControlProgram * pProgram = (CControlProgram *) m_pItem;

	m_dwProject     = pProgram->GetProject();

	m_dwProgram     = pProgram->GetHandle();
	}

// Overridables

UINT CUITextProgExec::OnSetAsText(CError &Error, CString Text)
{
	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_Enum[n].m_Text == Text ) {

			UINT uPrev = m_pData->ReadInteger(m_pItem);

			UINT uData = m_Enum[n].m_Data;

			if( uData - uPrev ) {

				if( OnWriteData(uData) ) {

					m_pData->WriteInteger(m_pItem, uData);

					return saveChange;
					}

				Error.SetUI(TRUE);

				Error.Set(L"Internal Error - failed to write program execution");

				return saveError;
				}
			}
		}

	return saveSame;
	}

// Implementation

BOOL CUITextProgExec::OnWriteData(UINT uData)
{
	switch( uData ) {

		case 0:	OnProgramEnable(FALSE) && OnProgramCalled(FALSE);	break;

		case 1:	OnProgramEnable(TRUE) && OnProgramCalled(FALSE);	break;

		case 2:	OnProgramEnable(TRUE) && OnProgramCalled(FALSE);	break;

		case 3:	OnProgramEnable(TRUE) && OnProgramCalled(TRUE);		break;

		default:							return FALSE;
		}
	
	return TRUE;
	}

BOOL CUITextProgExec::OnProgramEnable(BOOL fEnable)
{
	CStratonProperties Props(m_dwProject, m_dwProgram);

	return Props.Set( CControlProgram::propProgCheck, 
			  fEnable ? L"#TRUE#" : L"#FALSE#"
			  );
	}

BOOL CUITextProgExec::OnProgramCalled(BOOL fEnable)
{
	return afxDatabase->SetProgramOnCallFlag( m_dwProject, 
						  m_dwProgram, 
						  fEnable
						  );
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Parent
//

class CUITextProgParent : public CUITextEnumIndexed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProgParent(void);

	protected:

		// Enumerations
		enum {
			sectBegin	=  1,
			sectSfcMain	=  2,
			sectEnd		=  4,
			sectSfcChild	=  8,
			sectUdfb	= 16,
			sectMain	= sectBegin | sectSfcMain | sectEnd,
			sectProg	= sectMain  | sectUdfb    | sectSfcChild,
			};

		// Data Members
		DWORD	m_dwProject;
		DWORD	m_dwProgram;

		// Overridables
		void OnBind(void);
		void OnRebind(void);
		CString OnGetAsText(void);

		// Implementation
		void LoadPrograms(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Program Execution
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProgParent, CUITextEnumIndexed);

// Constructor

CUITextProgParent::CUITextProgParent(void)
{
	}

// Overridables

void CUITextProgParent::OnBind(void)
{
	CUITextElement::OnBind();

	CControlProgram * pProgram = (CControlProgram *) m_pItem;

	m_dwProject     = pProgram->GetProject();

	m_dwProgram     = pProgram->GetHandle();

	LoadPrograms();
	}

void CUITextProgParent::OnRebind(void)
{
	CUITextElement::OnRebind();

	CControlProgram * pProgram = (CControlProgram *) m_pItem;

	m_dwProject     = pProgram->GetProject();

	m_dwProgram     = pProgram->GetHandle();

	LoadPrograms();
	}

// Overridables

CString CUITextProgParent::OnGetAsText(void)
{
	return CUITextEnumIndexed::OnGetAsText();
	}

// Implementation

void CUITextProgParent::LoadPrograms(void)
{
	CLongArray List;

	UINT c;

	if (c = afxDatabase->GetPrograms( m_dwProject, sectProg, List) ) {
		
		for( UINT n = 0; n < c; n ++ ) {

			CStratonProgramDescriptor Desc(m_dwProject, List[n]);

			CEntry Enum;

			Enum.m_Data = Desc.m_dwHandle;

			Enum.m_Text = Desc.m_Name;

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}
		}

	CEntry Enum;

	Enum.m_Data = 0;

	Enum.m_Text = L"None";

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

// End of File
