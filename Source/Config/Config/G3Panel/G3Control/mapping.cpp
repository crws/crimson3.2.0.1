
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Variable Mapping
//

// Runtime Class

AfxImplementRuntimeClass(CVariableMapping, CObject);
		
// Constructor

CVariableMapping::CVariableMapping(CControlProject *pProject)
{
	m_pProject = pProject;
	}

// Operations

BOOL CVariableMapping::Export(CListView &List)
{
	if( FindVariables() ) {

		for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

			WriteLine(List, n);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CVariableMapping::Export(ITextStream &Stm, WCHAR cSep)
{
	if( FindVariables() ) {

		WriteHead(Stm, cSep);
		
		for( UINT n = 0; n < m_List.GetCount(); n ++ ) {
			
			WriteLine(Stm, cSep, n);
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CVariableMapping::FindVariables(void)
{
	CGroupVariables *pGroup;

	for( UINT n = 0; m_pProject->GetVarGroup(pGroup, n); n ++ ) {
		
		CControlObjectList *pList = pGroup->m_pObjects;

		INDEX i = pList->GetHead();
		
		while( !pList->Failed(i) ) {

			CControlVariable *pVar = (CControlVariable *) pList->GetItem(i);
			
			m_List.Append(pVar);

			pList->GetNext(i);
			}
		}

	return !m_List.IsEmpty();
	}

void CVariableMapping::WriteHead(ITextStream &Stm, WCHAR cSep)
{
	Stm.PutLine(CPrintf(L"Name%c", cSep));

	Stm.PutLine(CPrintf(L"Code%c", cSep));

	Stm.PutLine(L"\r\n");
	}

void CVariableMapping::WriteLine(ITextStream &Stm, WCHAR cSep, UINT i)
{
	CControlVariable *pVar = m_List[i];

	CString  Name = pVar->GetName();

	CString  Code = pVar->m_pValue ? pVar->m_pValue->GetSource(TRUE) : L"Internal";

	Stm.PutLine(CPrintf(L"%s%c", Name, cSep));

	Stm.PutLine(CPrintf(L"%s%c", Code, cSep));
	
	Stm.PutLine(L"\r\n");
	}

void CVariableMapping::WriteLine(CListView &List, UINT i)
{
	CControlVariable *pVar = m_List[i];

	CString  Name = pVar->GetName();

	CString  Code = pVar->m_pValue ? pVar->m_pValue->GetSource(TRUE) : L"Internal";

	List.InsertItem(CListViewItem(i, 0, Name,  NOTHING));

	List.SetItem   (CListViewItem(i, 1, Code,  NOTHING));
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Mapping Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CVariableMappingDialog, CStdDialog);

// Constructor

CVariableMappingDialog::CVariableMappingDialog(CControlProject *pProject)
{
	m_pProject = pProject;

	m_pView    = New CListView;

	SetName(L"VarMapDialog");
	}

// Message Map

AfxMessageMap(CVariableMappingDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(101,  OnExport)

	AfxMessageEnd(CVariableMappingDialog)
	};

// Message Handlers

BOOL CVariableMappingDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	MakeList();

	LoadList();

	ShowList();

	return FALSE;
	}

// Command Handlers

BOOL CVariableMappingDialog::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CVariableMappingDialog::OnExport(UINT uID)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(L"Control Variable Mappings");

	Dlg.SetCaption(CString(IDS_EXPORT_VARIABLE));

	Dlg.SetFilter (CString(IDS_UNICODE_TEXT));

	if( Dlg.ExecAndCheck() ) {

		CTextStreamMemory Stm;

		if( Stm.OpenSave() ) {

			WCHAR      cSep = ',';

			if( Dlg.GetFilename().GetType() == L"txt" ) {

				cSep = '\t';

				Stm.SetWide();
				}

			CVariableMapping Map(m_pProject);

			if( !Map.Export(Stm, cSep) ) {
				
				Information(CString(IDS_NO_VARIABLES));

				return TRUE;
				}

			if( Stm.SaveToFile(Dlg.GetFilename(), saveRaw) ) {

				Dlg.SaveLastPath(L"Variable Mappings");
				}
			}
		}

	return TRUE;
	}

// Implementation

void CVariableMappingDialog::MakeList(void)
{
	CWnd  &Wnd = GetDlgItem(100);

	CRect Rect = Wnd.GetWindowRect();
	
	ScreenToClient(Rect);

	Rect.left   += 8;
	Rect.top    += 20;
	Rect.right  -= 8;
	Rect.bottom -= 8;

	DWORD dwStyle   = LVS_REPORT        |
			  LVS_SHOWSELALWAYS |
			  LVS_SINGLESEL     |
			  LVS_NOSORTHEADER  |
			  WS_BORDER         |
			  WS_TABSTOP        ;

	DWORD dwExStyle = LVS_EX_FULLROWSELECT |
			  LVS_EX_GRIDLINES     ;

	m_pView->Create( dwStyle,
			 Rect,
			 ThisObject,
			 200
			 );

	m_pView->SetExtendedListViewStyle( dwExStyle,
					   dwExStyle
					   );

	int cx = Rect.cx() / 32;

	int c0 = 16 * cx;
	
	int c1 = Rect.cx() - c0;

	CListViewColumn Col0(0, LVCFMT_LEFT, c0, CString(IDS_CONTROL_VARIABLE));

	CListViewColumn Col1(1, LVCFMT_LEFT, c1, CString(IDS_DATA_SOURCE));

	m_pView->InsertColumn(0, Col0);
	
	m_pView->InsertColumn(1, Col1);
	}

void CVariableMappingDialog::LoadList(void)
{
	CVariableMapping Map(m_pProject);
	
	Map.Export(*m_pView);
	}

void CVariableMappingDialog::ShowList(void)
{
	m_pView->ShowWindow(SW_SHOW);

	SCROLLBARINFO Info;

	Info.cbSize = sizeof(Info);

	GetScrollBarInfo(m_pView->GetHandle(), OBJID_VSCROLL, &Info);

	if( Info.rgstate[0] != STATE_SYSTEM_INVISIBLE ) {

		int cx = m_pView->GetColumnWidth(1);

		m_pView->SetColumnWidth(1, cx - 17);
		}

	m_pView->SetFocus();
	}

// End of File
