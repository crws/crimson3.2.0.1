
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Manager
//

// Dynamic Class

AfxImplementDynamicClass(CGroupObjects, CUIItem);

// Constructor

CGroupObjects::CGroupObjects(void)
{
	m_Private  = 0;

	m_pObjects = New CControlObjectList;
	}

// Overridables

CString CGroupObjects::GetTreeLabel(void) const
{
	return L"<group>";
	}

UINT CGroupObjects::GetTreeImage(void) const
{
	return 4;
	}

void CGroupObjects::Validate(void)
{
	}

// UI Management

CViewWnd * CGroupObjects::CreateView(UINT uType)
{
	return NULL;
	}

// Item Naming

CString CGroupObjects::GetHumanName(void) const
{
	return GetTreeLabel();
	}

// Persistance

void CGroupObjects::Init(void)
{		
	CUIItem::Init();

	FindProject();
	}

void CGroupObjects::Load(CTreeFile &File)
{
	CUIItem::Load(File);

	FindProject();
	}

// Item Privacy

UINT CGroupObjects::GetPrivate(void)
{
	IDataAccess *pData = GetDataAccess(L"Private");

	if( pData ) {

		UINT uPrivate = pData->ReadInteger(this);

		return uPrivate;
		}

	return 0;
	}

// Overridable

void CGroupObjects::SetPrivate(UINT uHide)
{
	m_pObjects->SetPrivate(uHide);

	CMetaData const *pData = FindMetaData(L"Private");

	if( pData ) {

		UINT uPrev = pData->ReadInteger(this);

		if( uPrev - uHide ) {

			pData->WriteInteger(this, uHide);

			SetDirty();
			}
		}
	}

// Meta Data Creation

void CGroupObjects::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Private);
	Meta_AddCollect(Objects);
	}

// Implementation

void CGroupObjects::FindProject(void)
{
	m_pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Object
//

// Dynamic Class

AfxImplementDynamicClass(CControlObject, CCodedHost);

// Constructor

CControlObject::CControlObject(void)
{
	m_Ident = 0;
	}

// Development

void CControlObject::AddTest(void)
{
	}

// Overridables

CString CControlObject::GetTreeLabel(void) const
{
	return m_Name;
	}

UINT CControlObject::GetTreeImage(void) const
{
	return 4;
	}

void CControlObject::Validate(void)
{
	}

// Overridables

BOOL CControlObject::OnRename(CError &Error, CString Name)
{
	return FALSE;
	}

BOOL CControlObject::CanRename(CError &Error, CString Name)
{
	return FALSE;
	}

// Attributes

DWORD CControlObject::GetHandle(void)
{
	return 0;
	}

DWORD CControlObject::GetProject(void)
{
	return 0;
	}

CString	CControlObject::GetStoragePath(void)
{
	return CString();
	}

// Item Naming

CString CControlObject::GetHumanName(void) const
{
	return GetTreeLabel();
	}

// Persistance

void CControlObject::Init(void)
{
	CCodedHost::Init();

	FindProject();
	}

void CControlObject::Load(CTreeFile &File)
{
	CCodedHost::Load(File);

	FindProject();
	}

// Item Privacy

UINT CControlObject::GetPrivate(void)
{
	IDataAccess *pData = GetDataAccess(L"Private");

	if( pData ) {

		UINT uPrivate = pData->ReadInteger(this);

		return uPrivate;
		}

	return 0;
	}

// Overridable

void CControlObject::SetPrivate(UINT uHide)
{
	CMetaData const *pData = FindMetaData(L"Private");

	if( pData ) {

		UINT uPrev = pData->ReadInteger(this);

		if( uPrev - uHide ) {

			pData->WriteInteger(this, uHide);

			SetDirty();
			}
		}
	}

// Meta Data Creation

void CControlObject::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Ident);
	Meta_AddString (Name);

	Meta_SetName((IDS_CONTROL_OBJECT));
	}

// Implementation

void CControlObject::FindProject(void)
{
	m_pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Object List
//

// Dynamic Class

AfxImplementDynamicClass(CControlObjectList, CNamedList);

// Constructor

CControlObjectList::CControlObjectList(void)
{
	}

// Item Location

CControlObject * CControlObjectList::GetItem(INDEX n)
{
	return (CControlObject *) CItemIndexList::GetItem(n);
	}

// Overridable

void CControlObjectList::SetPrivate(UINT uHide)
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		GetItem(n)->SetPrivate(uHide);

		GetNext(n);
		}
	}

// Download Support

BOOL CControlObjectList::MakeInitData(CInitData &Init)
{
	return CItemIndexList::MakeInitData(Init);
	}

// End of File
