
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Formatter
//

// Constructor

CVariableInitialFormatter::CVariableInitialFormatter(void)
{
	}

// Operations

CString CVariableInitialFormatter::ToStraton(CString Text)
{
	return Text;
	}

CString CVariableInitialFormatter::ToCrimson(CString Text)
{
	return Text;
	}

// UI

CLASS CVariableInitialFormatter::GetUIClassText(void) const
{
	return AfxNamedClass(m_ClassText);
	}

CLASS CVariableInitialFormatter::GetUIClassUI(void) const
{
	return AfxNamedClass(m_ClassUI);
	}

CString CVariableInitialFormatter::GetUIFormat(void) const
{
	return m_Format;
	}

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- String
//

// Constructor

CVariableInitialFormatterString::CVariableInitialFormatterString(void)
{
	m_ClassText = L"CUITextVarInitString";

	m_ClassUI   = L"CUIEditBox";

	m_Format    = L"128||64|string";
	}

// Operations

CString CVariableInitialFormatterString::ToStraton(CString Text)
{
	CString Work;

	Work += L'\'';

	PCTXT p = PCTXT(Text);

	for( UINT n = 0; *p; n ++, p ++ ) {

		WCHAR c = *p;

		switch( c ) {
			
			case L'\'':
			case L'$':

				Work += L'$';
			
				break;
			}

		Work += c;
		}

	Work += L'\'';

	return Work;
	}

CString CVariableInitialFormatterString::ToCrimson(CString Text)
{
	// TODO -- richer parser

	CString Work = Text.Mid(1, Text.GetLength()-2);

	return Work;
	}

//////////////////////////////////////////////////////////////////////////
//
// Formatter
//

// Constructor

CVariableInitialFormatterTime::CVariableInitialFormatterTime(void)
{
	m_ClassText = L"CUITextVarInitString";

	m_ClassUI   = L"CUIEditBox";

	m_Format    = L"32||32|time";
	}

// Operations

CString CVariableInitialFormatterTime::ToStraton(CString Text)
{
	CString Work;

	Work += Text;

	Work += L"ms";

	return Work;
	}

CString CVariableInitialFormatterTime::ToCrimson(CString Text)
{
	return Text;
	}

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- Number
//

// Constructor

CVariableInitialFormatterNumber::CVariableInitialFormatterNumber(void)
{
	m_ClassText = L"CUITextVarInitString";

	m_ClassUI   = L"CUIEditBox";

	m_Format    = L"16||16|number";
	}

// Operations

CString CVariableInitialFormatterNumber::ToStraton(CString Text)
{
	return Text;
	}

CString CVariableInitialFormatterNumber::ToCrimson(CString Text)
{
	return Text;
	}

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- Boolean
//

// Constructor

CVariableInitialFormatterBoolean::CVariableInitialFormatterBoolean(void)
{
	m_ClassText = L"CUITextVarInitEnum";

	m_ClassUI   = L"CUIDropDown";

	m_Format    = L"FALSE|TRUE";
	}

//////////////////////////////////////////////////////////////////////////
//
// Formatter -- Real
//

// Constructor

CVariableInitialFormatterReal::CVariableInitialFormatterReal(void)
{
	m_ClassText = L"CUITextVarInitString";

	m_ClassUI   = L"CUIEditBox";

	m_Format    = L"16||16|real";
	}

// End of File

