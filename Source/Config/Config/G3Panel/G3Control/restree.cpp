
#include "intern.hpp"

#include "restree.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Resource Window
//

// Static Data

CControlResTreeWnd * CControlResTreeWnd::m_pThis = NULL;

// Dynamic Class

AfxImplementDynamicClass(CControlResTreeWnd, CStdTreeWnd);

// Constructors

CControlResTreeWnd::CControlResTreeWnd(void)
{
	m_dwStyle    = m_dwStyle | TVS_HASBUTTONS;

	m_cfVariable = RegisterClipboardFormat(L"CF_K5DBEDIT");

	m_cfMultiple = RegisterClipboardFormat(L"CF_DBNVARSPY");

	m_pTree->SetDeferred(TRUE);

	m_pTree->SetMultiple(TRUE);

	m_dwGroup = 0;

	m_pThis   = this;
	}

// Destructors

CControlResTreeWnd::~CControlResTreeWnd(void)
{
	}

// IUnknown

HRESULT CControlResTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
	}

ULONG CControlResTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
	}

ULONG CControlResTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
	}

// IUpdate

HRESULT CControlResTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		RefreshTree();

		return S_OK;
		}

	if( uType == updateRename ) {

		RefreshTree();

		return S_OK;
		}

	return S_OK;
	}

// Operations

void CControlResTreeWnd::SetProgram(DWORD dwProgram)
{
	if( dwProgram ) {

		CStratonProgramDescriptor Prog(m_dwProject, dwProgram);

		DWORD dwGroup = afxDatabase->FindGroup(m_dwProject, Prog.m_Name);

		if( m_dwGroup != dwGroup ) {

			m_dwGroup = dwGroup;

			RefreshTree();
			}
		}
	else {
		if( m_dwGroup ) {

			m_dwGroup = 0;

			RefreshTree();
			}
		}
	}

// Overridables

void CControlResTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	m_pProject  = (CControlProject *) m_pItem;

	m_dwProject = m_pProject->GetHandle();
	}

// Message Map

AfxMessageMap(CControlResTreeWnd, CStdTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchNotify(100, TVN_SELCHANGED,	OnTreeSelChanged)
	AfxDispatchNotify(100, NM_DBLCLK,	OnTreeDblClk  )

	AfxMessageEnd(CControlResTreeWnd)
	};

// Message Handlers

void CControlResTreeWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
	}

void CControlResTreeWnd::OnDestroy(void)
{
	KillTree(m_hRoot);
	}

void CControlResTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ResTreeTool"));
		}
	}

void CControlResTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	//CStdTreeWnd::OnShowWindow(fShow, uStatus);

	ShowStatus(fShow);		
	}

void CControlResTreeWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerDouble ) {
			
		m_System.SendPaneCommand(IDM_EDIT_PASTE);

		KillTimer(uID);
		}
	}

// Notification Handlers

void CControlResTreeWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	CStdTreeWnd::OnTreeSelChanged(uID, Info);
	}

void CControlResTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( !m_pTree->GetChild(m_hSelect) ) {

			// TODO -- Multiple?

			IDataObject *pData = NULL;

			if( MakeDataObject(pData) ) {

				OleSetClipboard(pData);
			
				OleFlushClipboard();

				pData->Release();

				SetTimer(m_timerDouble, 5);
				}

			return;
			}

		ToggleItem(m_hSelect);
		}
	}

// Data Object Construction

BOOL CControlResTreeWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddGroups  (pMake) || 
	AddMultiple(pMake) || 
	AddVariable(pMake) ;;

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CControlResTreeWnd::AddVariable(CDataObject *pData)
{
	CItem *pItem = (CItem *) m_pTree->GetItemParam(m_hSelect);

	if( pItem ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

			CControlVariable * pVariable =  (CControlVariable *) pItem;

			DWORD    dwIdent = pVariable->GetHandle();

			CString     Name = pVariable->GetName();

			CString     Text = CPrintf(L"%d:%s", dwIdent, Name);
		
			CAnsiString Ansi = CAnsiString(Text);

			pData->AddText(m_cfVariable, LPCSTR(Ansi));

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CControlResTreeWnd::AddMultiple(CDataObject *pData)
{
	if( m_fMulti ) {

		CString   List;

		HTREEITEM hItem = m_pTree->GetFirstSelect();

		while( hItem ) {

			CMetaItem *pItem = GetItemPtr(hItem);

			if( pItem->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

				CControlVariable * pVariable =  (CControlVariable *) pItem;

				AddVariable(List, pVariable);
				}

			hItem = m_pTree->GetNextSelect(hItem);
			}

		CAnsiString Ansi = CAnsiString(List);

		pData->AddText(m_cfMultiple, LPCSTR(Ansi));
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CControlResTreeWnd::AddGroups(CDataObject *pData)
{
	CItem *pItem = (CItem *) m_pTree->GetItemParam(m_hSelect);

	if( pItem ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CGroupVariables)) ) {

			CString   List;

			AddChildren(List, m_hSelect);

			CAnsiString Ansi = CAnsiString(List);

			pData->AddText(m_cfMultiple, LPCSTR(Ansi));

			return TRUE;
			}
		}

	return FALSE;
	}

void CControlResTreeWnd::AddChildren(CString &List, HTREEITEM hRoot)
{
	HTREEITEM hItem = m_pTree->GetNextItem(hRoot, TVGN_CHILD);

	while( hItem ) {

		CMetaItem *pItem = GetItemPtr(hItem);

		if( pItem->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

			CControlVariable * pVariable =  (CControlVariable *) pItem;

			AddVariable(List, pVariable);
			}

		hItem = m_pTree->GetNextItem(hItem, TVGN_NEXT);
		}
	}

void CControlResTreeWnd::AddVariable(CString &List, CControlVariable * pVariable)
{
	DWORD dwIdent = pVariable->GetHandle();

	CString  Name = pVariable->GetName();

	CString  Text = CPrintf(L"%d:%s", dwIdent, Name);

	AddName(List, Text);
	}

void CControlResTreeWnd::AddName(CString &List, CString Text)
{
	if( !List.IsEmpty() ) {
				
		List += L"\r\n";
		}

	List += Text;
	}

// Tree Loading

void CControlResTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CtrlTreeIcon16"), afxColor(MAGENTA));
	}

void CControlResTreeWnd::LoadTree(void)
{
	LoadRoot();

	LoadNodeKids(m_hRoot, m_pItem);

	ExpandItem(m_hRoot);

	m_pTree->SelectItem(m_hRoot);
	}

void CControlResTreeWnd::LoadRoot(void)
{
	CTreeViewItem Node;
	
	LoadNodeItem(Node, m_pItem);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);
	}

void CControlResTreeWnd::LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		Node.SetText(CString(IDS_VARIABLES));

		Node.SetParam(LPARAM(pItem));

		Node.SetImages(IDI_CTRL);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupPrograms)) ) {

		CGroupPrograms *pGroup = (CGroupPrograms *) pItem;

		Node.SetText(CString(IDS_LOCAL_VARIABLES));

		Node.SetParam(LPARAM(pGroup));

		Node.SetImages(pGroup->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CGlobalVariables)) ) {

		CGroupVariables *pGroup = (CGroupVariables *) pItem;

		Node.SetText(CString(IDS_CONTROL_GLOBALS));

		Node.SetParam(LPARAM(pGroup));

		Node.SetImages(pGroup->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupVariables)) ) {

		CGroupVariables *pGroup = (CGroupVariables *) pItem;

		Node.SetText(pGroup->GetTreeLabel());

		Node.SetParam(LPARAM(pGroup));

		Node.SetImages(pGroup->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

		CControlVariable *pVariable = (CControlVariable *) pItem;

		Node.SetText(pVariable->GetName());

		Node.SetParam(LPARAM(pVariable));

		Node.SetImages(pVariable->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

		CControlProgram *pProgram = (CControlProgram *) pItem;

		Node.SetText(pProgram->GetName());

		Node.SetParam(LPARAM(pProgram));

		Node.SetImages(pProgram->GetTreeImage());

		return;
		}

	AfxAssert(FALSE);
	}

void CControlResTreeWnd::LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		CControlProject *pProject = (CControlProject *) pItem;

		LoadItem(hRoot, NULL, pProject->m_pGlobals);

		if( m_dwGroup ) {

			CGroupPrograms *pPrograms = pProject->m_pPrograms;

			CString Find = CStratonGroupDescriptor(m_dwProject, m_dwGroup).m_Name;
			
			CControlProgram *pProgram;

			if( pPrograms->FindProgram(pProgram, Find) ) {
				
				HTREEITEM hItem = LoadItem(hRoot, NULL, pProgram);

				ExpandItem(hItem);
				}

			return;
			}

		LoadItem(hRoot, NULL, pProject->m_pPrograms);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupVariables)) ) {

		CGroupVariables *pGroup = (CGroupVariables *) pItem;

		LoadList(hRoot, pGroup->m_pObjects);

		ExpandItem(hRoot);
		
		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupObjects)) ) {

		CGroupObjects *pGroup = (CGroupObjects *) pItem;

		LoadList(hRoot, pGroup->m_pObjects);

		ExpandItem(hRoot);
		
		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlSfcProgram)) ) {

		CControlSfcProgram *pProgram = (CControlSfcProgram *) pItem;

		LoadItem(hRoot, NULL, pProgram->m_pLocals);

		if( !m_dwGroup ) {

			LoadItem(hRoot, NULL, pProgram->m_pChilds);
			}

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlMainProgram)) ) {

		CControlMainProgram *pProgram = (CControlMainProgram *) pItem;

		LoadItem(hRoot, NULL, pProgram->m_pLocals);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CControlCallProgram)) ) {
		
		CControlCallProgram *pProgram = (CControlCallProgram *) pItem;

		LoadItem(hRoot, NULL, pProgram->m_pLocals);

		LoadItem(hRoot, NULL, pProgram->m_pParams);

		return;
		}
	}

HTREEITEM CControlResTreeWnd::LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, NULL, Node);

	LoadNodeKids(hItem, pItem);

	return hItem;
	}

void CControlResTreeWnd::LoadList(HTREEITEM hRoot, CItemList *pList)
{
	for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

		CMetaItem *pItem = (CMetaItem *) pList->GetItem(n);

		if( pItem->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {
			
			LoadItem(hRoot, NULL, pItem);

			continue;
			}

		if( pItem->IsKindOf(AfxRuntimeClass(CControlSfcProgram)) ) {

			// TODO -- find the variables group for this nested sfc program.

			LoadItem(hRoot, NULL, pItem);

			continue;
			}

		if( pItem->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

			LoadItem(hRoot, NULL, pItem);

			continue;
			}
		}
	}

// Item Hooks

void CControlResTreeWnd::NewItemSelected(void)
{
	ShowStatus(TRUE);
	}

BOOL CControlResTreeWnd::HasFolderImage(HTREEITEM hItem)
{
	CItem *pItem = GetItemPtr(hItem);

	if( pItem->IsKindOf(AfxRuntimeClass(CGlobalVariables)) ) {
		
		return FALSE;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CGroupPrograms)) ) {
		
		return FALSE;
		}

	return pItem->IsKindOf(AfxRuntimeClass(CGroupObjects));
	}

// Selection Hooks

CString CControlResTreeWnd::SaveSelState(void)
{
	// LATER -- Is there a way to implement this?

	return L"";
	}

void CControlResTreeWnd::LoadSelState(CString State)
{
	SelectRoot();
	}

// Implementaiton

void CControlResTreeWnd::ShowStatus(BOOL fShow)
{
	CString Text;

	if( fShow ) {

		if( m_pSelect ) {

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CControlProgram)) ) {

				CControlProgram *pProgram = (CControlProgram *) m_pSelect;

				CStratonProgramDescriptor Prog(m_dwProject, pProgram->GetHandle());

				Text += pProgram->m_Name;

				if( Prog.IsCalled() ) {
					
					Text += L" - ";

					Text += CString(IDS_SUBPROGRAM);

					Text += L".";
					}	
				
				if( Prog.IsUdFb() ) {
					
					Text += L" - ";

					Text += CString(IDS_USERDEFINED);

					Text += L".";
					}	
				}

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CControlVariable)) ) {

				CControlVariable *pVariable = (CControlVariable *) m_pSelect;

				CStratonDataTypeDescriptor Type(m_dwProject, pVariable->m_TypeIdent);

				if( pVariable->m_TypeIdent ) {

					Text += pVariable->m_Name;

					if( Type.IsUdFb() || Type.IsStdFb() ) {

						Text += L" - ";
				
						Text += CString(IDS_INSTANCE_OF);

						Text += Type.m_Name;

						Text += L".";
						}

					if( Type.IsBasic() || Type.IsString() ) {

						Text += L" - ";

						Text += CString(IDS_DATATYPE);
					
						Text += Type.m_Name;

						Text += L".";
						}
					}
				}
			}
		}

	afxThread->SetStatusText(Text);
	}

// End of File
