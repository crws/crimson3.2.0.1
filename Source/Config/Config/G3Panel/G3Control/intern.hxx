
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4700
#define IDS_ALL                 0x4700
#define IDS_BIT_TYPES           0x4701
#define IDS_CHECK_LICENSE       0x4702
#define IDS_CHILD_SFC           0x4703
#define IDS_CONTROL_CHILDREN    0x4704
#define IDS_CONTROL_GLOBALS     0x4705
#define IDS_CONTROL_OBJECT      0x4706
#define IDS_CONTROL_PROGRAM     0x4707
#define IDS_CONTROL_PROGRAMS    0x4708
#define IDS_CONTROL_PROJECT     0x4709
#define IDS_CONTROL_PROJECT_2   0x470A
#define IDS_CONTROL_VARIABLE    0x470B
#define IDS_COPY_2              0x470C
#define IDS_COUNTERS            0x470D
#define IDS_CREATE              0x470E
#define IDS_CUSTOM              0x470F
#define IDS_CUT_2               0x4710
#define IDS_CUT_3               0x4711
#define IDS_DATABASE_ITEM       0x4712
#define IDS_DATATYPE            0x4713
#define IDS_DATA_SOURCE         0x4714
#define IDS_DELETE_1            0x4715
#define IDS_DELETE_2            0x4716
#define IDS_DELETE_3            0x4717
#define IDS_DESCRIPTION_FOR     0x4718
#define IDS_DO_YOU_WANT_TO      0x4719
#define IDS_EDIT_INITIAL        0x471A
#define IDS_EXECUTION           0x471B
#define IDS_EXPORT_VARIABLE     0x471C
#define IDS_FMT_IS_NOT_USED     0x471D
#define IDS_FMT_IS_NOT_VALID    0x471E
#define IDS_FUNCTION            0x471F
#define IDS_FUNCTION_BLOCK      0x4720
#define IDS_GLOBAL_RESULTS      0x4721
#define IDS_HELP_IS_NOT         0x4722
#define IDS_IEC_COMPLIANT       0x4723
#define IDS_INDICATES_INITIAL   0x4724
#define IDS_INDICATES_INITIAL_2 0x4725
#define IDS_INFORMATION         0x4726
#define IDS_INITIAL_VALUE       0x4727
#define IDS_INITIAL_VALUE_IS    0x4728
#define IDS_INPUT               0x4729
#define IDS_INPUTOUTPUT         0x472A
#define IDS_INSTANCE_OF         0x472B
#define IDS_JUMP_TO             0x472C
#define IDS_JUMP_TO_SELECTED    0x472D
#define IDS_LARGEST             0x472E
#define IDS_LICENSE             0x472F
#define IDS_LICENSE_IS_NOT      0x4730
#define IDS_LOCAL_VARIABLES     0x4731
#define IDS_MAKE_FMT_PRIVATE    0x4732
#define IDS_MOVE_1              0x4733
#define IDS_MOVE_2              0x4734
#define IDS_MOVE_DOWN           0x4735
#define IDS_MOVE_PROGRAM        0x4736
#define IDS_MOVE_PROGRAM_DOWN   0x4737
#define IDS_MOVE_PROGRAM_UP     0x4738
#define IDS_MOVE_UP             0x4739
#define IDS_NN                  0x473A
#define IDS_NO_VARIABLES        0x473B
#define IDS_OPERATOR            0x473C
#define IDS_OUTPUT              0x473D
#define IDS_PARAMETERS          0x473E
#define IDS_PASTE               0x473F
#define IDS_PLUS                0x4740
#define IDS_PROGRAM             0x4741
#define IDS_PROGRAMS            0x4742
#define IDS_PROGRAM_CANNOT_BE   0x4743
#define IDS_PROGRAM_CONTAINS    0x4744
#define IDS_PROGRAM_CONTAINS_2  0x4745
#define IDS_PROGRAM_WITH_THIS   0x4746
#define IDS_PROJECT_BUILT       0x4747
#define IDS_PROJECT_CONTAINS    0x4748
#define IDS_PROJECT_NEEDS_TO    0x4749
#define IDS_PROPERTIES          0x474A
#define IDS_READ_AND_WRITE      0x474B
#define IDS_READ_ONLY           0x474C
#define IDS_RENAME              0x474D
#define IDS_SMALLEST            0x474E
#define IDS_SOURCE              0x474F
#define IDS_STANDARD            0x4750
#define IDS_STOPPING_ONLINE     0x4751
#define IDS_SUBPROGRAM          0x4752
#define IDS_SYNTAX_ERRORS_FOR   0x4753
#define IDS_THE_NAME            0x4754 /* NOT USED */
#define IDS_UNICODE_TEXT        0x4755
#define IDS_UNKNOWN             0x4756
#define IDS_USAGE_FOR_CONTROL   0x4757
#define IDS_USAGE_OCURRENCES    0x4758
#define IDS_USERDEFINED         0x4759
#define IDS_VALUE               0x475A
#define IDS_VARIABLES           0x475B
#define IDS_VARIABLE_WITH       0x475C
#define IDS_WATCH_LIST_NOT      0x475D

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_CTRL_AUTONAME	0xD020
#define	IDM_CTRL_UNDONAME	0xD021
#define IDM_CTRL_CONVERT_SFC	0xD030
#define IDM_CTRL_CONVERT_ST	0xD031
#define IDM_CTRL_CONVERT_FBD	0xD032
#define IDM_CTRL_CONVERT_LD	0xD033
#define IDM_CTRL_CONVERT_IL	0xD034
#define IDM_CTRL_MAPPINGS	0xD035
#define IDM_CTRL_ADD_PROGRAM	0xD036
#define IDM_CTRL_ADD_SUB_PROG	0xD037
#define IDM_CTRL_ADD_UDFB	0xD038
#define IDM_CTRL_ADD_SFC_CHILD	0xD039
#define IDM_CTRL_ADD_VARIABLE	0xD040
#define IDM_CTRL_ADD_INPUT	0xD041
#define IDM_CTRL_ADD_OUTPUT	0xD042
#define IDM_CTRL_ADD_INOUT	0xD043
#define IDM_CTRL_XREF		0xD044
#define IDM_CTRL_EXEC		0xD045
#define IDM_CTRL_CREDENTIALS	0xD046

//////////////////////////////////////////////////////////////////////////
//
// Control Image Index Values
//

#define	IDI_CTRL			0
#define	IDI_VARIABLE_GROUP		2
#define	IDI_PROGRAM_GROUP		3
#define	IDI_FOLDER			4
#define	IDI_VARIABLE			8
#define	IDI_FUNCTION			8
#define	IDI_PROGRAM			16
#define	IDI_PROGRAM_ENABLED		23
#define	IDI_PROGRAM_DISABLED		20
#define	IDI_PROGRAM_SUBPROGRAM		17
#define	IDI_PROGRAM_SFC_ENABLED		22
#define	IDI_PROGRAM_SFC_DISABLED	20
#define	IDI_PROGRAM_SFC_CHILD		16
#define	IDI_PROGRAM_UDFB		21

// End of File

#endif
