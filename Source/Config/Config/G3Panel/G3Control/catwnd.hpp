
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CATWND_HPP

#define INCLUDE_CATWND_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CControlTreeWnd;

//////////////////////////////////////////////////////////////////////////
//
// Control Category Window
//

class CControlCatWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CControlCatWnd(void);

		// Destructor
		~CControlCatWnd(void);

		// Binding
		void AddProgramCtrl(CControlProgramCtrlWnd *pProgCtrl);
		void DelProgramCtrl(CControlProgramCtrlWnd *pProgCtrl);
		void SetControlNavTree(CControlTreeWnd *pNavTree);

		// Attributes
		BOOL IsDebug(void) const;
		BOOL IsOnline(void) const;

		// Operations
		void Commit(void);
		void Reload(void);
		void DetachEditors(CItem *pItem);
		BOOL StartDebug(BOOL fOnline);
		BOOL StopDebug(void);

	protected:
		// Type Definitions
		typedef CTree <CControlProgramCtrlWnd *> CProgCtrlTree;

		// Data Members
		CAccelerator		 m_Accel;
		CSysProxy	         m_System;
		CProgCtrlTree		 m_ProgCtrl;
		CControlTreeWnd        * m_pNavTree;
		CControlProject        * m_pProject;
		DWORD			 m_dwProject;
		BOOL                     m_fCurrent;
		BOOL			 m_fDebug;
		BOOL		         m_fOnline;
		COfflineSimulator        m_Simulate;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnSetCurrent(BOOL fCurrent);
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnMiddleware(UINT uCode, DWORD dwIdent);
		void OnDatabase(UINT uCode, DWORD dwIdent);

		// Command Handlers
		BOOL OnGlobalCommand(UINT uID);
		BOOL OnCtrlGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnCtrlControl(UINT uID, CCmdSource &Src);
		BOOL OnCtrlCommand(UINT uID);
		BOOL CanCtrlPauseResume(void);
		BOOL CanCtrlSingleStep(void);
		void OnCtrlSimulate(BOOL fOnline);

		// Program Events
		void OnProgramEvent(UINT uCode, DWORD dwIdent);
		void OnProgramCreated(DWORD dwIdent);
		void OnProgramRenamed(DWORD dwIdent);
		void OnProgramDeleted(DWORD dwIdent);
		void OnProgramLocals(DWORD dwIdent);

		// Group Events
		void OnVarGroupEvent(UINT uCode, DWORD dwIdent);
		void OnVarGroupRenamed(DWORD dwIdent);

		// Type Events
		void OnDataTypeEvent(UINT uCode, DWORD dwIdent);
		void OnTypeRenamed(DWORD dwIdent);
		void OnTypeCreated(DWORD dwIdent);
		void OnTypeDeleted(DWORD dwIdent);
		void OnTypeChanged(DWORD dwIdent);

		// Variable Events
		void OnVariableEvent(UINT uCode, DWORD dwIdent);

		// Variable Events
		void OnVariableCreated(DWORD dwIdent);
		void OnVariableRenamed(DWORD dwIdent);
		void OnVariableChanged(DWORD dwIdent);

		// Other Events
		void OnPropertyEvent(UINT uCode, DWORD dwIdent);
		void OnExternalEvent(UINT uCode, DWORD dwIdent);
		void OnCommentEvent(UINT uCode, DWORD dwIdent);
		void OnUnknownEvent(UINT uCode, DWORD dwIdent);

		// Implementation
		BOOL HasDirtyView(void);
		BOOL SetViewDebug(BOOL fDebug, BOOL fOnline);
		BOOL UpdateFuncResources(DWORD dwIdent);
		BOOL Verify(CDatabase *pDbase);
		BOOL DropLink(void);
	};

// End of File

#endif
