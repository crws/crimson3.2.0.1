
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3CONTROL_HXX
	
#define	INCLUDE_G3CRL_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hxx>

#include <g3straton.hxx>

// End of File

#endif
