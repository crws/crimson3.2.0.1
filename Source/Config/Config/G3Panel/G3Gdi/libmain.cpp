
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Embedded GDI Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

extern void LoadColorMap(void);

extern void KillColorMap(void);

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load G3Gdi\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			LoadColorMap();

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading G3Gdi\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		KillColorMap();

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
