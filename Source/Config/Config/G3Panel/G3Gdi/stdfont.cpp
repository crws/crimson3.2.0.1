
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GDI Version 2.0
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Standard Font Object
//

class CStdFont : public IGdiFont
{
	public:
		// Constructor
		CStdFont(UINT uFont);

		// Destructor
		~CStdFont(void);

		// Management
		UINT Release(void);

		// Attributes
		BOOL IsProportional(void);
		int  GetBaseLine   (void);
		BOOL GlyphAvailable(WORD c);
		int  GetGlyphWidth (WORD c);
		int  GetGlyphHeight(WORD c);

		// Operations
		void InitBurst(IGdi *pGDI, CLogFont const &Font);
		void DrawGlyph(IGdi *pGDI, int &x, int &y, WORD c);
		void BurstDone(IGdi *pGDI, CLogFont const &Font);

	protected:
		// Font Structure
		struct FONT
		{
			WORD	m_CharY;
			WORD	m_FontX;
			WORD	m_CharX[256];
			WORD	m_CharP[256];
			BYTE	m_bData[1];
			};

		// Data Members
		FONT  * m_pFont;
		UINT    m_uFont;
		UINT    m_uStride;
		BOOL    m_fFill;
		int     m_xSpace;
		int     m_xDigit;
		int     m_ySize;

		// Font Data
		static BYTE const m_bFont1[];
		static BYTE const m_bFont2[];
		static BYTE const m_bFont3[];
		static BYTE const m_bFont4[];
		static BYTE const m_bFont5[];
		static BYTE const m_bFont6[];
		static BYTE const m_bFont7[];
		static BYTE const m_bFont8[];

		// Implementation
		void ValidateChar(WORD &c);
		BOOL IsFixedDigit(WORD c);
		BOOL IsSpace(WORD c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Font Object
//

// Font Attributes

#ifdef  __GCC__

#define FONTDATA const __attribute((aligned(16)))

#else

#define	FONTDATA const

#endif

// Font Data File

#include "StdFont.hpp"

// Instantiator

IGdiFont * Create_StdFont(UINT uFont)
{
	return new CStdFont(uFont);
	}

// Constructor

CStdFont::CStdFont(UINT uFont)
{
	switch( uFont ) {

		case  1: m_pFont = (FONT *) m_bFont1; break;
		case  2: m_pFont = (FONT *) m_bFont2; break;
		case  3: m_pFont = (FONT *) m_bFont3; break;
		case  4: m_pFont = (FONT *) m_bFont4; break;
		case  5: m_pFont = (FONT *) m_bFont5; break;
		case  6: m_pFont = (FONT *) m_bFont6; break;
		case  7: m_pFont = (FONT *) m_bFont7; break;
		case  8: m_pFont = (FONT *) m_bFont8; break;
		default: m_pFont = (FONT *) m_bFont5; break;
		}

	m_uFont   = uFont;

	m_xSpace  = m_pFont->m_CharX[' '];

	m_xDigit  = m_pFont->m_CharX['4'];

	m_ySize   = m_pFont->m_CharY;

	m_uStride = (m_xSpace + 7) / 8;

	m_fFill   = FALSE;
	}

// Destructor

CStdFont::~CStdFont(void)
{
	}

// Management

UINT CStdFont::Release(void)
{
	delete this;

	return 0;
	}

// Attributes

BOOL CStdFont::IsProportional(void)
{
	return TRUE;
	}

int CStdFont::GetBaseLine(void)
{
	return 0;
	}

BOOL CStdFont::GlyphAvailable(WORD c)
{
	switch( c ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;
		}

	if( IsFixedDigit(c) ) {

		return TRUE;
		}

	if( c < 255 ) {

		if( m_pFont->m_CharX[c] ) {

			return TRUE;
			}
		}

	return FALSE;
	}

int CStdFont::GetGlyphWidth(WORD c)
{
	switch( c ) {

		case spaceNormal:
		case spaceNoBreak:

			return m_xSpace;

		case spaceNarrow:

			return m_xSpace / 2;

		case spaceHair:

			return 1;

		case spaceFigure:

			return m_xDigit;
		}

	if( IsFixedDigit(c) ) {

		return m_xDigit;
		}

	if( c > 0 ) {

		ValidateChar(c);

		return m_pFont->m_CharX[c];
		}

	return m_xSpace;
	}

int CStdFont::GetGlyphHeight(WORD c)
{
	return m_ySize;
	}

// Operations

void CStdFont::InitBurst(IGdi *pGDI, CLogFont const &Font)
{
	if( Font.m_Trans == modeOpaque ) {

		pGDI->PushBrush();

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Font.m_Back);

		m_fFill = TRUE;
		}
	}

void CStdFont::DrawGlyph(IGdi *pGDI, int &x, int &y, WORD c)
{
	if( IsSpace(c) ) {

		int cx = GetGlyphWidth(c);

		if( m_fFill ) {

			pGDI->FillRect(x, y, x+cx, y+m_ySize);
			}

		x += cx;

		return;
		}

	if( IsFixedDigit(c) ) {

		c -= digitFixed;

		c += digitSimple;
		}

	ValidateChar(c);

	int   cy = m_ySize;

	int   cx = m_pFont->m_CharX[c];
			
	int   cp = m_pFont->m_CharP[c];

	PBYTE pb = m_pFont->m_bData + cp;

	pGDI->CharBlt(x, y, cx, cy, m_uStride, pb);

	x += cx;
	}

void CStdFont::BurstDone(IGdi *pGDI, CLogFont const &Font)
{
	if( m_fFill ) {

		pGDI->PullBrush();

		m_fFill = FALSE;
		}
	}

// Implementation

void CStdFont::ValidateChar(WORD &c)
{
	if( c < 255 ) {

		if( m_pFont->m_CharX[c] ) {

			return;
			}
		}

	c = '?';
	}

BOOL CStdFont::IsFixedDigit(WORD c)
{
	if( c >= digitFixed && c < digitFixed + 10 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CStdFont::IsSpace(WORD c)
{
	switch( c ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;
		}

	return FALSE;
	}

// End of File
