
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Embedded GDI Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3GDI_HXX
	
#define	INCLUDE_G3GDI_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdesk.hxx>

#include <pcwin.hxx>

// End of File

#endif
