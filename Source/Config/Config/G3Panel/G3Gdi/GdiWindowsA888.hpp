
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GdiWindowsA888_HPP
	
#define	INCLUDE_GdiWindowsA888_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "GdiSoftA888.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Software Gdi Driver for Windows hDC
//

class DLLAPI CGdiWindowsA888 : public CGdiSoftA888, public IGdiWindows
{	
	public:
		// Constructor
		CGdiWindowsA888(int cx, int cy);

		// Destructor
		~CGdiWindowsA888(void);

		// IGdiWindows Attributes
		IGdi *  GetGdi   (void);
		HDC     GetDC    (void);
		HBITMAP GetBitmap(void);

		// IGdiWindows Operations
		void ClearAlpha(void);
		void ReleaseBitmap(void);
		void RestoreBitmap(void);
		void SaveImage(void);
		void RestoreImage(void);

		// IGdi Rendering
		UINT Render(PBYTE pData, UINT uBits);

	protected:
		// Data Members
		HDC     m_hDraw;
		HBITMAP m_hBits;
		HBITMAP m_hOld;
	};

// End of File

#endif
