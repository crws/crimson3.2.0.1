
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Hardware Drivers
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GdiGeneric_HPP
	
#define	INCLUDE_GdiGeneric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Generic Gdi Implementation
//

class DLLAPI CGdiGeneric : public IGdi
{
	public:
		// Constructor
		CGdiGeneric(int cx, int cy);

		// Destructor
		virtual ~CGdiGeneric(void);

		// Management
		UINT Release(void);

		// Attributes
		int GetCx(void);
		int GetCy(void);

		// Transformation
		void SetIdentity (void);
		void SetTransform(M3 const &M);
		void AddTransform(M3 const &M);
		void AddRotation (int nDegrees);
		void AddMovement (int dx, int dy);
		void AddMirror   (BOOL x, BOOL y);
		void MakeIdentity(M3 &M);
		void MakeRotation(M3 &M, int nDegrees);
		void MakeMovement(M3 &M, int dx, int dy);
		void MakeMirror  (M3 &M, BOOL x, BOOL y);

		// Text Attributes
		void ResetFont    (void);
		void PushFont     (void);
		void PullFont     (void);
		void SelectFont   (CLogFont const &Font);
		void SelectFont   (UINT           uFont);
		void SelectFont   (IGdiFont *     pFont);
		void GetFont      (CLogFont       &Font);
		void SetTextTrans (UINT  Trans );
		void SetTextSmooth(UINT  Smooth);
		void SetTextFore  (COLOR Fore  );
		void SetTextBack  (COLOR Back  );

		// Combined Attributes
		void ResetAll 	 (void);
		void PushAll 	 (void);
		void PullAll 	 (void);
		void ResetBoth	 (void);
		void PushBoth	 (void);
		void PullBoth	 (void);
		void SetBackMode (UINT  Trans);
		void SetForeColor(COLOR Fore );
		void SetBackColor(COLOR Back );

		// Brush Attributes
		void ResetBrush   (void);
		void PushBrush    (void);
		void PullBrush    (void);
		void SelectBrush  (CLogBrush const &Brush);
		void GetBrush     (CLogBrush       &Brush);
		void SelectBrush  (UINT  Style);
		void SetBrushStyle(UINT  Style);
		void SetBrushTrans(UINT  Trans);
		void SetBrushFore (COLOR Fore);
		void SetBrushFore (DWORD Rich);
		void SetBrushBack (COLOR Fore);
		void SetBrushBits (PCVOID pd, int cx, int cy);

		// Pen Attributes
		void PushPen    (void);
		void PullPen    (void);
		void ResetPen   (void);
		void SelectPen  (CLogPen const &Pen);
		void GetPen     (CLogPen       &Pen);
		void SelectPen  (UINT  Style);
		void SetPenStyle(UINT  Style);
		void SetPenTrans(UINT  Trans);
		void SetPenWidth(UINT  Width);
		void SetPenCaps (UINT  Caps);
		void SetPenFore (COLOR Fore);
		void SetPenBack (COLOR Fore);

		// Clearing
		void ClearScreen(COLOR Color);

		// Pixel Access
		void SetPixel(int x, int y, COLOR Color);

		// Text Metrics
		int GetTextHeight(PCTXT p);
		int GetTextWidth (PCTXT p);
		int GetTextHeight(TCHAR c);
		int GetTextWidth (TCHAR c);

		// Text Output
		void TextOut(int x, int y, PCTXT p);
		void TextOut(int x, int y, PCTXT p, UINT n);

		#if !defined(UNICODE)

		// Text Metrics
		int GetTextHeight(PCUTF p);
		int GetTextWidth (PCUTF p);
		int GetTextHeight(WCHAR c);
		int GetTextWidth (WCHAR c);

		// Text Output
		void TextOut(int x, int y, PCUTF p);
		void TextOut(int x, int y, PCUTF p, UINT n);

		#endif

		// Bitmaps
		void BitBlt(int x, int y, int cx, int cy, int s, PCBYTE p, UINT rop);

		// Line Drawing
		void MoveTo  (int x1, int y1);
		void LineTo  (int x1, int y1);
		void DrawLine(int x1, int y1, int x2, int y2);

		// Polygon Drawing
		void DrawPolygon(P2 const *pList, UINT uCount, DWORD dwRound);
		void FillPolygon(P2 const *pList, UINT uCount, DWORD dwRound);

		// Shaded Polygon
		void ShadePolygon(P2 const *pList1, UINT uCount1,                                 DWORD dwRound, PSHADER pShade);
		void ShadePolygon(P2 const *pList1, UINT uCount1, P2 const *pList2, UINT uCount2, DWORD dwRound, PSHADER pShade);

		// Smooth Polygon
		void SmoothPolygon(P2 const *pList1, UINT uCount1, P2 const *pList2, UINT uCount2, DWORD dwRound, PSHADER pShade, int nAlpha);

		// Shaded Figures
		void ShadeEllipse(int x1, int y1, int x2, int y2, UINT uType, PSHADER pShade);
		void ShadeRect   (int x1, int y1, int x2, int y2,	      PSHADER pShade);
		void ShadeRounded(int x1, int y1, int x2, int y2, int  r,     PSHADER pShade);
		void ShadeWedge  (int x1, int y1, int x2, int y2, UINT uType, PSHADER pShade);

		// Rectangle Drawing
		void DrawRect(int x1, int y1, int x2, int y2);
		void FillRect(int x1, int y1, int x2, int y2);
	
		// Rounded Rectangles
		void DrawRounded(int x1, int y1, int x2, int y2, int r);
		void FillRounded(int x1, int y1, int x2, int y2, int r);

		// Ellipses
		void DrawEllipse(int x1, int y1, int x2, int y2, UINT uType);
		void FillEllipse(int x1, int y1, int x2, int y2, UINT uType);

		// Wedges
		void DrawWedge(int x1, int y1, int x2, int y2, UINT uType);
		void FillWedge(int x1, int y1, int x2, int y2, UINT uType);

	protected:
		// Filling Modes
		enum
		{
			fillNull    = 0,
			fillSolid   = 1,
			fillPattern = 2,
			fillBitmap  = 3
			};

		// Drawing Flags
		enum
		{
			drawLine  = 0x00,
			drawFill  = 0x01,
			drawQuad1 = 0x10,
			drawQuad2 = 0x20,
			drawQuad3 = 0x40,
			drawQuad4 = 0x80,
			};

		// Polygon Edge Types
		enum
		{
			edgeHorizontal = 0,
			edgeVertical   = 1,
			edgeDiagonal   = 2,
			edgeHorzMajor  = 3,
			edgeVertMajor  = 4,
			edgeArcAbove   = 5,
			edgeArcBelow   = 6,
			};

		// Polygon Edge Data
		struct CEdge
		{
			// Linked List
			CEdge *	pNext;
			CEdge *	pPrev;
			// Edge Definition
			int	en;
			int	et;
			int	xp;
			int	yp;
			int	xe;
			int	ye;
			// Edge Extent
			int	u;
			int	v;
			// Edge Direction
			int	dx;
			int	dy;
			// Scanline Extent
			int	x1;
			int	x2;
			// Data for Line
			int	np;
			int	ku;
			int	kv;
			int	kd;
			int	kt;
			int	d0;
			int	d1;
			int	d2;
			// Data for Ellipse
			int	t1;
			int	t2;
			int	t3;
			int	t4;
			int	t5;
			int	t6;
			int	t7;
			int	t8;
			int	t9;
			};

		// Constants
		static int const styleBase;
		static int const styleDiag;
		static int const initMin;
		static int const initMax;
		static int const trigSize;
		static int const trigStep;
		static int const trigBits;
		static int const trigScale;
		static int const tranScale;
		static int const sampBits;
		static int const sampScale;
		static int const sampMask;
		static int const sampLimit;

		// Trig Table
		int *m_pTrig;

		// Display Size
		int m_cx;
		int m_cy;

		// Current Font
		CLogFont	m_Font;
		CLogFont	m_OldFont;
		CLogFont	m_FontStack[8];
		UINT		m_uFontPtr;
		BOOL		m_fFontDirty;
		BOOL		m_fFontProp;
		int		m_cxFont;
		int		m_cyFont;

		// Standard Fonts
		IGdiFont *      m_pStdFont[fontCount];
		IGdiFont *      m_pDefFont;

		// Current Brush
		CLogBrush	m_Brush;
		CLogBrush	m_OldBrush;
		CLogBrush	m_BrushStack[8];
		UINT		m_uBrushPtr;
		BOOL		m_fBrushDirty;
		UINT		m_uBrushMode;

		// Current Pen
		CLogPen		m_Pen;
		CLogPen		m_OldPen;
		CLogPen		m_PenStack[8];
		UINT		m_uPenPtr;
		BOOL		m_fPenDirty;
		BOOL		m_fPenNull;
		BOOL		m_fPenStyle;
		BOOL		m_fPenFore;
		DWORD		m_dwPenMask;
		DWORD		m_dwPenTest;
		DWORD		m_dwPenInit;

		// Transformation
		BOOL m_fTransform;
		BOOL m_fComplex;
		M3   m_M;

		// Alpha Buffer
		int  m_cb;
		PINT m_xb;
		int  m_o1;
		int  m_o2;

		// Graphics Context
		int  m_x0, m_y0;
		int  m_pc, m_ps, m_pi;
		int  m_xn, m_yn;
		int  m_xp, m_yp;
		int  m_rx, m_ry;
		int  m_a0, m_ar;
		UINT m_dm;
		int  m_dy;

		// Area Fill Support
		virtual void FillArea(int x1, int y1, int x2, int y2) = 0;

		// Alpha Blending
		virtual void BlendHorz(int yd) = 0;
		virtual void BlendVert(int yd) = 0;

		// Line Drawing Support
		virtual void LineFill(int x, int y, int cx, int cy, BOOL fFore) = 0;
		virtual void LineSet (int x, int y,                 BOOL fFore) = 0;
		virtual void LineCap (int l, int r, int y                     ) = 0;

		// Low-Level Output
		virtual void IntSetPixel(int x, int y, COLOR Color) = 0;

		// Line Drawing Helpers
		void HorzStrokeFwd(int x, int y, int cx, int cy);
		void HorzStrokeRev(int x, int y, int cx, int cy);
		void VertStrokeFwd(int x, int y, int cx, int cy);
		void VertStrokeRev(int x, int y, int cx, int cy);
		void RoundEndCap  (int x, int y, int n);

		// Line Drawing Implementation
		void LineHelp (int x1, int y1, int x2, int y2);
		void LineThin (int x1, int y1, int x2, int y2);
		void LineThick(int x1, int y1, int x2, int y2);
		void HorzMajor(int x1, int y1, int u, int v, int ku, int kv, int dx, int dy, int d0);
		void VertMajor(int x1, int y1, int u, int v, int ku, int kv, int dx, int dy, int d0);

		// Line Styling
		void InitStyle(void);
		void SaveStyle(void);
		void LoadStyle(void);
		void SetStyleStep(int ps);
		BOOL IsSimpleStyle(void);
		BOOL GetPixelStyle(void);

		// Polygon Helpers
		void DrawPolygonJoin(P2 const *pList, UINT uCount, DWORD dwRound);
		void DrawPolygonScan(P2 const *pList, UINT uCount, DWORD dwRound);
		void DrawPolygonFill(P2 const *pList, UINT uCount, DWORD dwRound);

		// Edge Management
		void AddEdges(CEdge *pEdge, UINT &uUsed, BOOL fHorz, P2 const *pList, UINT uCount, DWORD dwRound, int &xp, int &xe, int &yp, int &ye);
		void OrderEdgesByX(CEdge * &pHead, CEdge * &pTail);
		void OrderEdgesByY(CEdge *pList, UINT uCount);
		void SetupEdge (CEdge *pEdge, UINT uRound);
		void TrackEdges(CEdge *pHead);
		void TrackEdge (CEdge *pEdge);

		// Edge Sorting
		static int CompareX(CEdge *p1, CEdge *p2);
		static int CompareY(CEdge *p1, CEdge *p2);

		// Alpha Accumulation
		BOOL AccumAlpha(int x1, int x2);

		// Rounded Rect Building
		DWORD MakeRounded(P2 *p, int x1, int y1, int x2, int y2, int r);

		// Wedge Building
		BOOL MakeWedge(P2 *p, int x1, int y1, int x2, int y2, UINT uType);

		// Ellipse Helpers
		void PrepEllipse(UINT uMode, int x1, int y1, int x2, int y2, UINT uType);
		int  MakeEllipse(P2 *pp);
		void DrawCircle (void);
		void PlotCircle(int x, int y);
		void DrawEllipse(void);
		void PlotEllipse(int x, int y);
		void PlotEllipse(int x, int y, int cx);

		// Tool Management
		void DirtyBoth (void);
		void DirtyFont (void);
		void DirtyBrush(void);
		void DirtyPen  (void);
		void CheckBoth (void);
		void CheckFont (void);
		BOOL CheckBrush(void);
		BOOL CheckPen  (void);
		void ForceBoth (void);
		void ForceFont (void);
		void ForceBrush(void);
		void ForcePen  (void);

		// Transformation
		BOOL IsComplex(void);
		BOOL Normalize(int &x1, int &y1, int &x2, int &y2);
		void Transform(int &x1, int &y1, int &x2, int &y2);
		void Transform(int &x1, int &y1);

		// Font Library
		void MakeStdFonts(void);
		void KillStdFonts(void);

		// Math Helpers
		BOOL TrigInit  (void);
		int  TrigStep  (int cx, int cy);
		int  TrigFactor(int r, int f);
		int  Cos       (int d);
		int  Sin       (int d);
		int  Hypot     (int u, int v, int n);
		int  FastRoot  (int n);
		int  Abs       (int x);
		int  Sign      (int x);
		void Swap      (int &a, int &b);

		// Tool Realization
		virtual void OnNewFont (void) = 0;
		virtual void OnNewBrush(void) = 0;
		virtual void OnNewPen  (void) = 0;
	};

// End of File

#endif
