
#include "intern.hpp"

#include "StdPalette.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GDI Version 2.0
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Color Map
//

static CMap <COLOR, CString> m_NameMap;

static CMap <CString, COLOR> m_DataMap;

static PWORD                 m_pColor;

static PBYTE		     m_pEntry;

//////////////////////////////////////////////////////////////////////////
//
// Map Setup
//

static BOOL MakeClosest(void)
{
	m_pColor = New WORD [ 32768 ];

	m_pEntry = New BYTE [ 32768 ];

	if( TRUE ) {

		WORD n = 0;

		for( BYTE r = 0; r < 32; r++ ) {

			int ri = 1 * ((r * 5 + 15) / 31);

			for( BYTE g = 0; g < 32; g++ ) {

				int gi = 6 * ((g * 5 + 15) / 31);

				for( BYTE b = 0; b < 32; b++ ) {

					int bi = 36 * ((b * 5 + 15) / 31);

					int ni = n++;

					m_pEntry[ni] = BYTE(24 + ri + gi + bi);

					m_pColor[ni] = C3GetGdiColor(C3GetPaletteEntry(m_pEntry[ni]));
					}
				}
			}
		}

	if( TRUE ) {

		for( UINT c = 0; c < 256; c++ ) {

			if( c == 0x10 || c == 0x18 ) {

				continue;
				}

			if( c == 0xEF || c == 0xF0 || c == 0xFF ) {

				continue;
				}

			COLOR p = C3GetGdiColor(C3GetPaletteEntry(c));

			m_pEntry[p] = BYTE(c);

			m_pColor[p] = p;
			}
		}

	return TRUE;
	}

static void KillClosest(void)
{
	delete [] m_pColor;

	delete [] m_pEntry;
	}

static void AddEntry(PCTXT pName, COLOR Color)
{
	m_NameMap.Insert(Color, pName);

	m_DataMap.Insert(pName, Color);
	}

global void LoadColorMap(void)
{
	AddEntry(L"AliceBlue", 32766);
	AddEntry(L"AntiqueWhite", 27583);
	AddEntry(L"AntiqueWhite1", 28607);
	AddEntry(L"AntiqueWhite2", 26493);
	AddEntry(L"AntiqueWhite3", 23321);
	AddEntry(L"AntiqueWhite4", 15889);
	AddEntry(L"Aqua", 32736);
	AddEntry(L"Aquamarine1", 27631);
	AddEntry(L"Aquamarine2", 25518);
	AddEntry(L"Aquamarine4", 14888);
	AddEntry(L"Azure2", 30652);
	AddEntry(L"Azure3", 26424);
	AddEntry(L"Azure4", 17968);
	AddEntry(L"Beige", 28638);
	AddEntry(L"Bisque1", 25503);
	AddEntry(L"Bisque2", 23389);
	AddEntry(L"Bisque3", 20185);
	AddEntry(L"Bisque4", 13809);
	AddEntry(L"Black", 0);
	AddEntry(L"BlanchedAlmond", 26559);
	AddEntry(L"Blue", 31744);
	AddEntry(L"Blue2", 29696);
	AddEntry(L"Blue4", 17408);
	AddEntry(L"BlueViolet", 28849);
	AddEntry(L"Brown", 5300);
	AddEntry(L"Brown1", 8479);
	AddEntry(L"Brown2", 7421);
	AddEntry(L"Brown3", 6361);
	AddEntry(L"Brown4", 4241);
	AddEntry(L"Burlywood", 17147);
	AddEntry(L"Burlywood1", 20319);
	AddEntry(L"Burlywood2", 19229);
	AddEntry(L"Burlywood3", 16057);
	AddEntry(L"Burlywood4", 10705);
	AddEntry(L"CadetBlue", 21099);
	AddEntry(L"CadetBlue1", 32723);
	AddEntry(L"CadetBlue2", 30609);
	AddEntry(L"CadetBlue3", 26383);
	AddEntry(L"CadetBlue4", 17930);
	AddEntry(L"Chartreuse2", 942);
	AddEntry(L"Chartreuse3", 812);
	AddEntry(L"Chartreuse4", 552);
	AddEntry(L"Chocolate", 3514);
	AddEntry(L"Chocolate1", 4607);
	AddEntry(L"Chocolate2", 4573);
	AddEntry(L"Chocolate3", 3481);
	AddEntry(L"Coral", 10751);
	AddEntry(L"Coral1", 10719);
	AddEntry(L"Coral2", 10685);
	AddEntry(L"Coral3", 8569);
	AddEntry(L"Coral4", 5361);
	AddEntry(L"CornflowerBlue", 30284);
	AddEntry(L"Cornsilk1", 28671);
	AddEntry(L"Cornsilk2", 26557);
	AddEntry(L"Cyan2", 30624);
	AddEntry(L"Cyan3", 26400);
	AddEntry(L"Cyan4", 17952);
	AddEntry(L"DarkGoldenrod", 1559);
	AddEntry(L"DarkGoldenrod1", 1791);
	AddEntry(L"DarkGoldenrod2", 1725);
	AddEntry(L"DarkGoldenrod3", 1625);
	AddEntry(L"DarkGoldenrod4", 1425);
	AddEntry(L"DarkGreen", 384);
	AddEntry(L"DarkKhaki", 14039);
	AddEntry(L"DarkOliveGreen", 5546);
	AddEntry(L"DarkOliveGreen1", 15353);
	AddEntry(L"DarkOliveGreen2", 14263);
	AddEntry(L"DarkOliveGreen3", 12084);
	AddEntry(L"DarkOliveGreen4", 7725);
	AddEntry(L"DarkOrange", 575);
	AddEntry(L"DarkOrange1", 511);
	AddEntry(L"DarkOrange2", 477);
	AddEntry(L"DarkOrange3", 409);
	AddEntry(L"DarkOrange4", 273);
	AddEntry(L"DarkOrchid", 25811);
	AddEntry(L"DarkOrchid1", 31991);
	AddEntry(L"DarkOrchid2", 29942);
	AddEntry(L"DarkOrchid4", 17549);
	AddEntry(L"DarkSalmon", 15965);
	AddEntry(L"DarkSeaGreen", 18161);
	AddEntry(L"DarkSeaGreen1", 25592);
	AddEntry(L"DarkSeaGreen2", 23478);
	AddEntry(L"DarkSeaGreen3", 20275);
	AddEntry(L"DarkSeaGreen4", 13869);
	AddEntry(L"DarkSlateBlue", 17641);
	AddEntry(L"DarkSlateGray", 9509);
	AddEntry(L"DarkSlateGray1", 32754);
	AddEntry(L"DarkSlateGray2", 30641);
	AddEntry(L"DarkSlateGray3", 26415);
	AddEntry(L"DarkSlateGray4", 17962);
	AddEntry(L"DarkTurquoise", 27424);
	AddEntry(L"DarkViolet", 26642);
	AddEntry(L"DeepPink1", 18527);
	AddEntry(L"DeepPink2", 17501);
	AddEntry(L"DeepPink3", 14425);
	AddEntry(L"DeepPink4", 10289);
	AddEntry(L"DeepSkyBlue1", 32480);
	AddEntry(L"DeepSkyBlue2", 30400);
	AddEntry(L"DeepSkyBlue3", 26208);
	AddEntry(L"DeepSkyBlue4", 17824);
	AddEntry(L"DimGray", 13741);
	AddEntry(L"DodgerBlue1", 32323);
	AddEntry(L"DodgerBlue2", 30211);
	AddEntry(L"DodgerBlue3", 26051);
	AddEntry(L"DodgerBlue4", 17698);
	AddEntry(L"Firebrick", 4246);
	AddEntry(L"Firebrick1", 6367);
	AddEntry(L"Firebrick2", 5309);
	AddEntry(L"Firebrick3", 4249);
	AddEntry(L"Firebrick4", 3185);
	AddEntry(L"ForestGreen", 4644);
	AddEntry(L"Fuchsia", 31775);
	AddEntry(L"Gainsboro", 28539);
	AddEntry(L"Gold1", 863);
	AddEntry(L"Gold2", 829);
	AddEntry(L"Gold3", 697);
	AddEntry(L"Gold4", 465);
	AddEntry(L"Goldenrod", 4763);
	AddEntry(L"Goldenrod1", 4895);
	AddEntry(L"Goldenrod2", 4829);
	AddEntry(L"Goldenrod3", 3705);
	AddEntry(L"Goldenrod4", 2481);
	AddEntry(L"Gray", 8456);
	AddEntry(L"Green", 512);
	AddEntry(L"GreenYellow", 6133);
	AddEntry(L"Honeydew1", 31742);
	AddEntry(L"Honeydew2", 29628);
	AddEntry(L"Honeydew3", 25400);
	AddEntry(L"Honeydew4", 16944);
	AddEntry(L"HotPink", 22975);
	AddEntry(L"HotPink2", 20925);
	AddEntry(L"HotPink3", 18841);
	AddEntry(L"HotPink4", 12529);
	AddEntry(L"IndianRed", 11641);
	AddEntry(L"IndianRed1", 13759);
	AddEntry(L"IndianRed2", 12701);
	AddEntry(L"IndianRed3", 10585);
	AddEntry(L"IndianRed4", 7409);
	AddEntry(L"Ivory1", 31743);
	AddEntry(L"Ivory2", 29629);
	AddEntry(L"Ivory3", 25401);
	AddEntry(L"Ivory4", 16945);
	AddEntry(L"Khaki", 18334);
	AddEntry(L"Khaki1", 18399);
	AddEntry(L"Khaki2", 17309);
	AddEntry(L"Khaki3", 15129);
	AddEntry(L"Khaki4", 9745);
	AddEntry(L"Lavender", 32668);
	AddEntry(L"LavenderBlush1", 31711);
	AddEntry(L"LavenderBlush2", 29597);
	AddEntry(L"LavenderBlush3", 25369);
	AddEntry(L"LawnGreen", 1007);
	AddEntry(L"LemonChiffon1", 26623);
	AddEntry(L"LemonChiffon2", 24509);
	AddEntry(L"LemonChiffon3", 21305);
	AddEntry(L"LemonChiffon4", 14897);
	AddEntry(L"LightBlue", 29557);
	AddEntry(L"LightBlue1", 32695);
	AddEntry(L"LightBlue2", 30582);
	AddEntry(L"LightBlue3", 26387);
	AddEntry(L"LightBlue4", 17933);
	AddEntry(L"LightCoral", 16926);
	AddEntry(L"LightCyan1", 32764);
	AddEntry(L"LightCyan2", 30650);
	AddEntry(L"LightCyan3", 26422);
	AddEntry(L"LightCyan4", 17967);
	AddEntry(L"LightGoldenrod1", 18367);
	AddEntry(L"LightGoldenrod2", 17277);
	AddEntry(L"LightGoldenrod3", 15097);
	AddEntry(L"LightGoldenrodYellow", 27647);
	AddEntry(L"LightGray", 27482);
	AddEntry(L"LightPink", 25311);
	AddEntry(L"LightPink1", 24255);
	AddEntry(L"LightPink2", 22173);
	AddEntry(L"LightPink3", 19001);
	AddEntry(L"LightPink4", 12657);
	AddEntry(L"LightSalmon1", 16031);
	AddEntry(L"LightSalmon2", 14941);
	AddEntry(L"LightSalmon3", 12825);
	AddEntry(L"LightSalmon4", 8529);
	AddEntry(L"LightSeaGreen", 22212);
	AddEntry(L"LightSkyBlue", 32560);
	AddEntry(L"LightSkyBlue1", 32662);
	AddEntry(L"LightSkyBlue2", 30548);
	AddEntry(L"LightSkyBlue3", 26321);
	AddEntry(L"LightSkyBlue4", 17900);
	AddEntry(L"LightSlateBlue", 32208);
	AddEntry(L"LightSlateGray", 20014);
	AddEntry(L"LightSteelBlue", 28438);
	AddEntry(L"LightSteelBlue1", 32665);
	AddEntry(L"LightSteelBlue3", 26324);
	AddEntry(L"LightYellow1", 29695);
	AddEntry(L"LightYellow2", 27581);
	AddEntry(L"LightYellow3", 23353);
	AddEntry(L"LightYellow4", 15921);
	AddEntry(L"Lime", 992);
	AddEntry(L"LimeGreen", 6950);
	AddEntry(L"Linen", 29663);
	AddEntry(L"Magenta2", 29725);
	AddEntry(L"Magenta3", 25625);
	AddEntry(L"Magenta4", 17425);
	AddEntry(L"Maroon", 16);
	AddEntry(L"Maroon", 12502);
	AddEntry(L"Maroon1", 22751);
	AddEntry(L"Maroon2", 20701);
	AddEntry(L"Maroon3", 18617);
	AddEntry(L"Maroon4", 12401);
	AddEntry(L"MediumAquamarine", 22316);
	AddEntry(L"MediumBlue", 25600);
	AddEntry(L"MediumOrchid", 26967);
	AddEntry(L"MediumOrchid1", 32156);
	AddEntry(L"MediumOrchid2", 30074);
	AddEntry(L"MediumOrchid3", 25942);
	AddEntry(L"MediumOrchid4", 17615);
	AddEntry(L"MediumPurple", 28114);
	AddEntry(L"MediumPurple1", 32277);
	AddEntry(L"MediumPurple2", 30195);
	AddEntry(L"MediumPurple3", 26033);
	AddEntry(L"MediumPurple4", 17675);
	AddEntry(L"MediumSeaGreen", 15047);
	AddEntry(L"MediumSlateBlue", 30127);
	AddEntry(L"MediumSpringGreen", 20448);
	AddEntry(L"MediumTurquoise", 26441);
	AddEntry(L"MediumVioletRed", 16472);
	AddEntry(L"MidnightBlue", 14435);
	AddEntry(L"MistyRose1", 29599);
	AddEntry(L"MistyRose2", 27485);
	AddEntry(L"MistyRose3", 23257);
	AddEntry(L"MistyRose4", 15857);
	AddEntry(L"Moccasin", 23455);
	AddEntry(L"NavajoWhite1", 22399);
	AddEntry(L"NavajoWhite2", 21309);
	AddEntry(L"NavajoWhite3", 18137);
	AddEntry(L"NavajoWhite4", 11761);
	AddEntry(L"Navy", 16384);
	AddEntry(L"Olive", 528);
	AddEntry(L"OliveDrab", 4653);
	AddEntry(L"OliveDrab1", 8184);
	AddEntry(L"OliveDrab2", 8118);
	AddEntry(L"Orange1", 671);
	AddEntry(L"Orange2", 637);
	AddEntry(L"Orange3", 537);
	AddEntry(L"Orange4", 369);
	AddEntry(L"OrangeRed1", 287);
	AddEntry(L"OrangeRed2", 285);
	AddEntry(L"OrangeRed3", 217);
	AddEntry(L"OrangeRed4", 145);
	AddEntry(L"Orchid", 27099);
	AddEntry(L"Orchid1", 32287);
	AddEntry(L"Orchid2", 30205);
	AddEntry(L"Orchid3", 26041);
	AddEntry(L"Orchid4", 17681);
	AddEntry(L"PaleGoldenrod", 22461);
	AddEntry(L"PaleGreen", 20467);
	AddEntry(L"PaleGreen2", 19378);
	AddEntry(L"PaleGreen3", 16175);
	AddEntry(L"PaleGreen4", 10794);
	AddEntry(L"PaleTurquoise", 30645);
	AddEntry(L"PaleTurquoise1", 32759);
	AddEntry(L"PaleTurquoise3", 26418);
	AddEntry(L"PaleTurquoise4", 17964);
	AddEntry(L"PaleVioletRed", 18907);
	AddEntry(L"PaleVioletRed1", 22047);
	AddEntry(L"PaleVioletRed2", 19965);
	AddEntry(L"PaleVioletRed3", 17849);
	AddEntry(L"PaleVioletRed4", 11537);
	AddEntry(L"PeachPuff1", 24447);
	AddEntry(L"PeachPuff2", 22333);
	AddEntry(L"PeachPuff3", 19129);
	AddEntry(L"PeachPuff4", 12753);
	AddEntry(L"Pink", 26399);
	AddEntry(L"Pink2", 24253);
	AddEntry(L"Pink3", 20057);
	AddEntry(L"Pink4", 13713);
	AddEntry(L"Plum", 28315);
	AddEntry(L"Plum1", 32511);
	AddEntry(L"Plum2", 30397);
	AddEntry(L"Plum3", 26201);
	AddEntry(L"Plum4", 17809);
	AddEntry(L"PowderBlue", 29590);
	AddEntry(L"Purple", 16400);
	AddEntry(L"Purple", 30868);
	AddEntry(L"Purple1", 31955);
	AddEntry(L"Purple2", 29874);
	AddEntry(L"Purple3", 25743);
	AddEntry(L"Purple4", 17514);
	AddEntry(L"Red", 31);
	AddEntry(L"Red2", 29);
	AddEntry(L"Red3", 25);
	AddEntry(L"Red4", 17);
	AddEntry(L"RosyBrown", 17975);
	AddEntry(L"RosyBrown1", 25375);
	AddEntry(L"RosyBrown2", 23261);
	AddEntry(L"RosyBrown3", 20089);
	AddEntry(L"RosyBrown4", 13745);
	AddEntry(L"RoyalBlue", 29096);
	AddEntry(L"RoyalBlue1", 32201);
	AddEntry(L"RoyalBlue2", 30120);
	AddEntry(L"RoyalBlue3", 25959);
	AddEntry(L"RoyalBlue4", 17668);
	AddEntry(L"SaddleBrown", 2321);
	AddEntry(L"Salmon", 14879);
	AddEntry(L"Salmon1", 13887);
	AddEntry(L"Salmon2", 12829);
	AddEntry(L"Salmon3", 10713);
	AddEntry(L"Salmon4", 7473);
	AddEntry(L"SandyBrown", 12958);
	AddEntry(L"SeaGreen1", 20458);
	AddEntry(L"SeaGreen2", 19369);
	AddEntry(L"SeaGreen3", 17192);
	AddEntry(L"SeaGreen4", 10789);
	AddEntry(L"Seashell1", 30687);
	AddEntry(L"Seashell2", 28573);
	AddEntry(L"Seashell3", 24345);
	AddEntry(L"Seashell4", 16913);
	AddEntry(L"Sienna", 5460);
	AddEntry(L"Sienna1", 8735);
	AddEntry(L"Sienna2", 8701);
	AddEntry(L"Sienna3", 7609);
	AddEntry(L"Sienna4", 4369);
	AddEntry(L"Silver", 16912);
	AddEntry(L"SkyBlue", 30512);
	AddEntry(L"SkyBlue2", 30479);
	AddEntry(L"SkyBlue3", 26253);
	AddEntry(L"SkyBlue4", 17865);
	AddEntry(L"SlateBlue", 25965);
	AddEntry(L"SlateBlue1", 32176);
	AddEntry(L"SlateBlue2", 30095);
	AddEntry(L"SlateBlue4", 17640);
	AddEntry(L"SlateGray", 18958);
	AddEntry(L"SlateGray1", 32664);
	AddEntry(L"SlateGray2", 30551);
	AddEntry(L"SlateGray3", 26323);
	AddEntry(L"SlateGray4", 17901);
	AddEntry(L"Snow2", 30653);
	AddEntry(L"Snow3", 26425);
	AddEntry(L"Snow4", 17969);
	AddEntry(L"SpringGreen1", 16352);
	AddEntry(L"SpringGreen2", 15264);
	AddEntry(L"SpringGreen3", 13088);
	AddEntry(L"SpringGreen4", 8736);
	AddEntry(L"SteelBlue", 23048);
	AddEntry(L"SteelBlue1", 32492);
	AddEntry(L"SteelBlue2", 30379);
	AddEntry(L"SteelBlue3", 26185);
	AddEntry(L"SteelBlue4", 17798);
	AddEntry(L"Tan", 18138);
	AddEntry(L"Tan1", 9887);
	AddEntry(L"Tan2", 9853);
	AddEntry(L"Tan3", 7705);
	AddEntry(L"Tan4", 5489);
	AddEntry(L"Tea", 16896);
	AddEntry(L"Thistle", 28411);
	AddEntry(L"Thistle1", 32671);
	AddEntry(L"Thistle2", 30557);
	AddEntry(L"Thistle3", 26329);
	AddEntry(L"Thistle4", 17905);
	AddEntry(L"Tomato1", 8607);
	AddEntry(L"Tomato2", 8573);
	AddEntry(L"Tomato3", 7481);
	AddEntry(L"Tomato4", 4305);
	AddEntry(L"Turquoise", 27528);
	AddEntry(L"Turquoise1", 32704);
	AddEntry(L"Turquoise2", 30592);
	AddEntry(L"Turquoise3", 26368);
	AddEntry(L"Turquoise4", 17920);
	AddEntry(L"Violet", 30237);
	AddEntry(L"VioletRed", 18586);
	AddEntry(L"VioletRed1", 18687);
	AddEntry(L"VioletRed2", 17661);
	AddEntry(L"VioletRed3", 15577);
	AddEntry(L"VioletRed4", 10385);
	AddEntry(L"Wheat", 23422);
	AddEntry(L"Wheat1", 24479);
	AddEntry(L"Wheat2", 22397);
	AddEntry(L"Wheat3", 19193);
	AddEntry(L"Wheat4", 12785);
	AddEntry(L"White", 32767);
	AddEntry(L"WhiteSmoke", 31710);
	AddEntry(L"Yellow", 1023);
	AddEntry(L"Yellow2", 957);
	AddEntry(L"Yellow3", 825);
	AddEntry(L"Yellow4", 561);
	AddEntry(L"YellowGreen", 6963);

	MakeClosest();
	}

global void KillColorMap(void)
{
	m_NameMap.Empty();

	m_DataMap.Empty();

	KillClosest();
	}

//////////////////////////////////////////////////////////////////////////
//
// Color Helpers
//

DWORD DLLAPI C3GetWinColor(COLOR Color)
{
	if( Color == 32767 ) {

		return RGB(255,255,255);
		}

	BYTE r = GetRED  (Color);
	BYTE g = GetGREEN(Color);
	BYTE b = GetBLUE (Color);

	r *= 8;
	g *= 8;
	b *= 8;

	return RGB(r,g,b);
	}

COLOR DLLAPI C3GetGdiColor(DWORD Color)
{
	BYTE r = GetRValue(Color);
	BYTE g = GetGValue(Color);
	BYTE b = GetBValue(Color);

	r /= 8;
	g /= 8;
	b /= 8;

	return GetRGB(r,g,b);
	}

CString DLLAPI C3StdColorName(UINT uIndex)
{
	switch( uIndex ) {

		case 0x0: return CString(IDS_BLACK);
		case 0x1: return CString(IDS_MAROON);
		case 0x2: return CString(IDS_GREEN);
		case 0x3: return CString(IDS_OLIVE);
		case 0x4: return CString(IDS_NAVY);
		case 0x5: return CString(IDS_PURPLE);
		case 0x6: return CString(IDS_TEAL);
		case 0x7: return CString(IDS_SILVER);
		case 0x8: return CString(IDS_GRAY);
		case 0x9: return CString(IDS_RED);
		case 0xA: return CString(IDS_LIME);
		case 0xB: return CString(IDS_YELLOW);
		case 0xC: return CString(IDS_BLUE);
		case 0xD: return CString(IDS_FUCHSIA);
		case 0xE: return CString(IDS_AQUA);
		case 0xF: return CString(IDS_WHITE);
		}

	if( uIndex >= 0x10 && uIndex <= 0x2F ) {

		return CPrintf(CString(IDS_FORMAT), uIndex - 0x10);
		}

	return L"";
	}

COLOR DLLAPI C3StdColorData(UINT uIndex)
{
	switch( uIndex ) {

		case 0x0: return GetRGB(0x00,0x00,0x00);
		case 0x1: return GetRGB(0x0F,0x00,0x00);
		case 0x2: return GetRGB(0x00,0x0F,0x00);
		case 0x3: return GetRGB(0x0F,0x0F,0x00);
		case 0x4: return GetRGB(0x00,0x00,0x0F);
		case 0x5: return GetRGB(0x0F,0x00,0x0F);
		case 0x6: return GetRGB(0x00,0x0F,0x0F);
		case 0x7: return GetRGB(0x0F,0x0F,0x0F);
		case 0x8: return GetRGB(0x08,0x08,0x08);
		case 0x9: return GetRGB(0x1F,0x00,0x00);
		case 0xA: return GetRGB(0x00,0x1F,0x00);
		case 0xB: return GetRGB(0x1F,0x1F,0x00);
		case 0xC: return GetRGB(0x00,0x00,0x1F);
		case 0xD: return GetRGB(0x1F,0x00,0x1F);
		case 0xE: return GetRGB(0x00,0x1F,0x1F);
		case 0xF: return GetRGB(0x1F,0x1F,0x1F);
		}

	if( uIndex >= 0x10 && uIndex <= 0x2F ) {

		BYTE m = BYTE((uIndex - 0x10));

		return GetRGB(m,m,m);
		}

	return GetRGB(0,0,0);
	}

BOOL DLLAPI C3ColorFromName(COLOR &Color, CString Name)
{
	INDEX n = m_DataMap.FindName(Name);

	if( !m_DataMap.Failed(n) ) {

		Color = m_DataMap.GetData(n);

		return TRUE;
		}

	return FALSE;
	}

BOOL DLLAPI C3NameFromColor(CString &Name, COLOR Color)
{
	INDEX n = m_NameMap.FindName(Color);

	if( !m_NameMap.Failed(n) ) {

		Name = m_NameMap.GetData(n);

		return TRUE;
		}

	return FALSE;
	}

DWORD DLLAPI C3GetPaletteEntry(UINT uIndex)
{
	PCBYTE p = StdPalette + 3 * uIndex;

	return RGB(p[0],p[1],p[2]);
	}

PCWORD DLLAPI C3GetClosestColor(void)
{
	return m_pColor;
	}

PCBYTE DLLAPI C3GetClosestEntry(void)
{
	return m_pEntry;
	}

//////////////////////////////////////////////////////////////////////////
//
// Dummy GDI Hook
//

void GDI_Hook(IGDI *pGDI)
{
	}

// End of File
