
#include "Intern.hpp"

#include "GdiSoftA888.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Generic Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Software Gdi Driver for 32-bit Displays
//

// Palette Access

extern DWORD DispGetPaletteEntry(UINT);

// Bit Operations

#define	BitPick(b) BYTE(m_bBitMask[b])

#define	BitWalk(m) BEGIN { if( !(m <<= 1) ) m = 1; } END

// Static Data

DWORD const CGdiSoftA888::m_bBitMask[] = { 1, 2, 4, 8, 16, 32, 64, 128 };

// Constructor

CGdiSoftA888::CGdiSoftA888(int cx, int cy, double gamma, PDWORD pData) : CGdiGeneric(cx, cy)
{
	m_c1     = 0xFFFF;

	m_c2     = 0xFFFF;

	m_pEight = NULL;

	m_pGray  = NULL;

	if( pData ) {

		// This form is used when we're in pure software mode,
		// creating a bitmap for use by another GDI object. We
		// use the buffer provided and don't support rendering
		// via API used by the web server.

		m_pWork = NULL;

		m_pData = pData;

		ArrayZero(m_pData, m_cx * m_cy);
		}
	else {
		// This form is used when we're working with a memory
		// buffer that will update the primary display. The
		// m_pData pointer will get loaded in the child class
		// to point to the actual buffer we'll be using.

		m_pWork  = New DWORD [ m_cx * m_cy ];

		m_pData  = NULL;

		// Force these tables to be allocated so they don't
		// get created in the context of a transitory task
		// whose memory will disappear at some point.

		MakeEightTable();

		MakeGrayTable();

		TrigInit();
		}

	MakeBlendTable(gamma);
	}

// Destructor

CGdiSoftA888::~CGdiSoftA888(void)
{
	delete [] m_pWork;

	FreeEightTable();

	FreeGrayTable();

	FreeBlendTable();
	}

// IGdi Rendering

UINT CGdiSoftA888::Render(PBYTE pData, UINT uBits)
{
	if( m_pWork ) {

		// NOTE -- Child class should update m_pWork with the live display
		// contents before calling this function to do the actual rendering.

		if( LOBYTE(uBits) == 8 ) {

			MakeEightTable();

			PCDWORD pWork = m_pWork;

			PBYTE   pDest = pData;
		
			if( !HIBYTE(uBits) ) {
			
				pDest += (m_cy - 1) * m_cx;
				}

			for( int y = 0; y < m_cy; y++ ) {

				int x = m_cx;

				while( (x -= 4) >= 0 ) {

					DWORD d0, d1, d2, d3;

					d0 = pWork[0];
					d1 = pWork[1];
					d2 = pWork[2];
					d3 = pWork[3];

					pWork += 4;

					if( !((d0 | d1 | d2 | d3) & 0x00FFFFFF) ) {

						(PDWORD(pDest))[0] = 0;

						pDest += 4;

						continue;
						}

					*pDest++ = m_pEight[To555(d0)];
					*pDest++ = m_pEight[To555(d1)];
					*pDest++ = m_pEight[To555(d2)];
					*pDest++ = m_pEight[To555(d3)];

					continue;
					}

				if( !HIBYTE(uBits) ) {
		
					pDest -= 2 * m_cx;
					}
				}

			return m_cx * m_cy;
			}
	
		if( LOBYTE(uBits) == 16 ) {

			PCDWORD pWork = m_pWork;

			PWORD   pDest = PWORD(pData); 

			for( int y = 0; y < m_cy; y++ ) {

				int x = m_cx;

				while( (x -= 4) >= 0 ) {

					DWORD d0, d1, d2, d3;

					d0 = pWork[0];
					d1 = pWork[1];
					d2 = pWork[2];
					d3 = pWork[3];

					pWork += 4;

					if( !((d0 | d1 | d2 | d3) & 0x00FFFFFF) ) {

						(PDWORD(pDest))[0] = 0;
						(PDWORD(pDest))[1] = 0;

						pDest += 4;

						continue;
						}

					*pDest++ = To555(d0);
					*pDest++ = To555(d1);
					*pDest++ = To555(d2);
					*pDest++ = To555(d3);

					continue;
					}
				}

			return m_cx * m_cy * sizeof(WORD);
			}
		}

	return 0;
	}

void CGdiSoftA888::Render(PBYTE pData, int x, int y, int cx, int cy)
{
	// Is this right? Who uses this? !!!!

	if( cx && cy && x < m_cx && y < m_cy ) {

		MakeMin(cx, m_cx - x);

		MakeMin(cy, m_cy - y);

		PDWORD pDest = PDWORD(pData);

		BufferClaim();

		PCDWORD pFrom = m_pData + x + y * m_cx;

		for( int r = 0; r < cy; r++ ) {

			for( int n = 0; n < cx; n++ ) {

				*pDest++ = *pFrom++;
				}

			pFrom += m_cx - cx;
			}

		BufferFree();
		}
	}

// IGdi Update

void CGdiSoftA888::Update(void)
{
	}

// IGdi Serialization

void CGdiSoftA888::BufferClaim(void)
{
	}

void CGdiSoftA888::BufferFree(void)
{
	}

// IGdi Color Format

UINT CGdiSoftA888::GetColorFormat(void)
{
	return colorRGB888;
	}

// IGdi Bitmaps

void CGdiSoftA888::BitBlt(int x, int y, int cx, int cy, int s, PCBYTE p, UINT rop)
{
	if( x < m_cx && y < m_cy ) {

		if( !s ) {

			s = 4 * cx;
			}

		MakeMin(cx, m_cx - x);

		MakeMin(cy, m_cy - y);

		if( cx && cy ) {

			PCDWORD pFrom = PCDWORD(p);

			PDWORD  pDest = m_pData + x + y * m_cx;
			
			if( rop & ropDisable ) {

				MakeGrayTable();

				if( rop & ropBlend ) {

					int rp = m_cx - cx;

					for( int r = 0; r < cy; r++ ) {
						
						DWORD f;

						for( int n = 0; n < cx; n++ ) {

							if( (f = *pFrom) ) {

								UINT af;

								if( (af = (f >> 24)) == 255 ) {

									*pDest = m_pGray[To555(*pFrom)];
									}
								else {
									register DWORD d = *pDest;

									register DWORD m = 255;

									register DWORD r;

									af = 256 - af;

									r  = ((((d & m) * af) >> 8) & m); m <<= 8;
									r |= ((((d & m) * af) >> 8) & m); m <<= 8;
									r |= ((((d & m) * af) >> 8) & m);

									*pDest = m_pGray[To555(f)] + r;
									}
								}

							pFrom += 1;

							pDest += 1;
							}

						pFrom += (s/4  - cx);
						
						pDest += rp;
						}

					return;
					}

				if( rop & ropTrans ) {

					for( int r = 0; r < cy; r++ ) {

						for( int n = 0; n < cx; n++ ) {

							if( *pFrom & 0x80000000 ) {

								*pDest = m_pGray[To555(*pFrom)];
								}

							pFrom += 1;

							pDest += 1;
							}

						pFrom += (s/4  - cx);
							
						pDest += (m_cx - cx);
						}

					return;
					}

				for( int r = 0; r < cy; r++ ) {

					for( int n = 0; n < cx; n++ ) {

						*pDest++ = m_pGray[To555(*pFrom++)];
						}

					pFrom += (s/4  - cx);
							
					pDest += (m_cx - cx);
					}

				return;
				}

			if( rop & ropZoom2X ) { 

				int sf = 2;

				int rp = sf * m_cx;

				if( true ) {

					// This clipping is wrong, but it keeps it from
					// going off the screen and won't be an issue in
					// virtually all real applications.

					MakeMin(cy, (m_cy - y) / sf);
					}
				
				if( rop & ropInvert ) {

					pDest += rp * (cy - 1);

					rp    *= -1;
					}

				for( int r = 0; r < cy; r++ ) {

					PDWORD pd0 = pDest + 0 * m_cx;
					PDWORD pd1 = pDest + 1 * m_cx;

					for( int n = 0; n < cx; n++ ) {

						*pd0++ = *pFrom;
						*pd0++ = *pFrom;

						*pd1++ = *pFrom;
						*pd1++ = *pFrom;

						pFrom++;
						}

					pFrom += (s/4  - cx);
						
					pDest += rp;
					}

				return;
				}

			if( rop & ropZoom3X ) { 

				int sf = 3;

				int rp = sf * m_cx;

				if( true ) {

					// This clipping is wrong, but it keeps it from
					// going off the screen and won't be an issue in
					// virtually all real applications.

					MakeMin(cy, (m_cy - y) / sf);
					}
				
				if( rop & ropInvert ) {

					pDest += rp * (cy - 1);

					rp    *= -1;
					}
				
				for( int r = 0; r < cy; r++ ) {

					PDWORD pd0 = pDest + 0 * m_cx;
					PDWORD pd1 = pDest + 1 * m_cx;
					PDWORD pd2 = pDest + 2 * m_cx;

					for( int n = 0; n < cx; n++ ) {

						*pd0++ = *pFrom;
						*pd0++ = *pFrom;
						*pd0++ = *pFrom;

						*pd1++ = *pFrom;
						*pd1++ = *pFrom;
						*pd1++ = *pFrom;

						*pd2++ = *pFrom;
						*pd2++ = *pFrom;
						*pd2++ = *pFrom;

						pFrom++;
						}

					pFrom += (s/4  - cx);
						
					pDest += rp;
					}

				return;
				}

			if( rop & ropZoom4X ) { 

				int sf = 4;

				int rp = sf * m_cx;

				if( true ) {

					// This clipping is wrong, but it keeps it from
					// going off the screen and won't be an issue in
					// virtually all real applications.

					MakeMin(cy, (m_cy - y) / sf);
					}
				
				if( rop & ropInvert ) {

					pDest += rp * (cy - 1);

					rp    *= -1;
					}
				
				for( int r = 0; r < cy; r++ ) {

					PDWORD pd0 = pDest + 0 * m_cx;
					PDWORD pd1 = pDest + 1 * m_cx;
					PDWORD pd2 = pDest + 2 * m_cx;
					PDWORD pd3 = pDest + 2 * m_cx;

					for( int n = 0; n < cx; n++ ) {

						*pd0++ = *pFrom;
						*pd0++ = *pFrom;
						*pd0++ = *pFrom;
						*pd0++ = *pFrom;

						*pd1++ = *pFrom;
						*pd1++ = *pFrom;
						*pd1++ = *pFrom;
						*pd1++ = *pFrom;

						*pd2++ = *pFrom;
						*pd2++ = *pFrom;
						*pd2++ = *pFrom;
						*pd2++ = *pFrom;

						*pd3++ = *pFrom;
						*pd3++ = *pFrom;
						*pd3++ = *pFrom;
						*pd3++ = *pFrom;

						pFrom++;
						}

					pFrom += (s/4  - cx);
						
					pDest += rp;
					}

				return;
				}

			int rp = m_cx - cx;

			if( rop & ropInvert ) {

				pDest += m_cx * (cy - 1);

				rp     = -(m_cx + cx);
				}

			if( rop & ropBlend ) {

				for( int r = 0; r < cy; r++ ) {
						
					DWORD f;

					for( int n = 0; n < cx; n++ ) {

						if( (f = *pFrom) ) {

							UINT af;

							if( (af = (f >> 24)) == 255 ) {

								*pDest = *pFrom;
								}
							else {
								register DWORD d = *pDest;

								register DWORD m = 255;

								register DWORD r;

								af = 256 - af;

								r  = ((((d & m) * af) >> 8) & m); m <<= 8;
								r |= ((((d & m) * af) >> 8) & m); m <<= 8;
								r |= ((((d & m) * af) >> 8) & m);

								*pDest = f + r;
								}
							}

						pFrom += 1;

						pDest += 1;
						}

					pFrom += (s/4  - cx);
						
					pDest += rp;
					}

				return;
				}

			if( rop & ropTrans ) {

				for( int r = 0; r < cy; r++ ) {

					for( int n = 0; n < cx; n++ ) {

						if( *pFrom & 0x80000000 ) {

							*pDest = *pFrom;
							}

						pFrom += 1;

						pDest += 1;
						}

					pFrom += (s/4  - cx);
						
					pDest += rp;
					}

				return;
				}

			for( int r = 0; r < cy; r++ ) {

				// TODO -- Might be able to unroll this to transfer
				// say 32 bytes at a time using multiple registers.

				for( int n = 0; n < cx; n++ ) {

					*pDest++ = *pFrom++;
					}

				pFrom += (s/4  - cx);
						
				pDest += rp;
				}
			}
		}
	}

// IGdi Text Helpers

void CGdiSoftA888::CharBlt(int x, int y, int cx, int cy, int k, PCBYTE p)
{
	if( x < m_cx && y < m_cy ) {

		MakeMin(cx, m_cx - x);

		MakeMin(cy, m_cy - y);

		if( cx && cy ) {

			PDWORD pDest = m_pData + x + y * m_cx;

			ExpandGlyph(cx, cy, k, p);

			if( !m_Font.m_Trans ) {

				DWORD f = m_dwFontFore;

				DWORD b = m_dwFontBack;

				for( int r = 0; r < cy; r++ ) {

					if( r + y >= 0 ) {

						PCDWORD pFrom = PCDWORD(p);

						DWORD   Store = *pFrom++;

						UINT	uByte = 3;

						int n;
	
						for( n = cx; n >= 8; n -= 8 ) {

							BYTE s = HIBYTE(HIWORD(Store));

							Store  = uByte-- ? (Store << 8) : (uByte = 3, *pFrom++);

							if( s & 0x80 )
								*pDest++ = f;
							else
								*pDest++ = b;

							if( s & 0x40 )
								*pDest++ = f;
							else
								*pDest++ = b;

							if( s & 0x20 )
								*pDest++ = f;
							else
								*pDest++ = b;

							if( s & 0x10 )
								*pDest++ = f;
							else
								*pDest++ = b;

							if( s & 0x08 )
								*pDest++ = f;
							else
								*pDest++ = b;

							if( s & 0x04 )
								*pDest++ = f;
							else
								*pDest++ = b;

							if( s & 0x02 )
								*pDest++ = f;
							else
								*pDest++ = b;

							if( s & 0x01 )
								*pDest++ = f;
							else
								*pDest++ = b;
							}

						if( n > 0 ) {

							BYTE s = HIBYTE(HIWORD(Store));

							Store  = uByte-- ? (Store << 8) : (uByte = 3, *pFrom++);

							if( n > 0 ) {
	
								if( s & 0x80 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}

							if( n > 1 ) {

								if( s & 0x40 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}

							if( n > 2 ) {

								if( s & 0x20 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}

							if( n > 3 ) {

								if( s & 0x10 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}

							if( n > 4 ) {

								if( s & 0x08 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}

							if( n > 5 ) {

								if( s & 0x04 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}

							if( n > 6 ) {

								if( s & 0x02 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}

							if( n > 7 ) {

								if( s & 0x01 )
									*pDest++ = f;
								else
									*pDest++ = b;
								}
							}

						pDest += m_cx - cx;
						}
					else
						pDest += m_cx;

					p += k;
					}
				}
			else {
				DWORD f = m_dwFontFore;

				for( int r = 0; r < cy; r++ ) {

					if( r + y >= 0 ) {
					
						PCDWORD pFrom = PCDWORD(p);

						DWORD   Store = *pFrom++;

						UINT	uByte = 3;

						int n;

						for( n = cx; n >= 8; n -= 8 ) {

							BYTE s = HIBYTE(HIWORD(Store));

							Store  = uByte-- ? (Store << 8) : (uByte = 3, *pFrom++);

							if( s & 0x80 )
								pDest[0] = f;

							if( s & 0x40 )
								pDest[1] = f;

							if( s & 0x20 )
								pDest[2] = f;

							if( s & 0x10 )
								pDest[3] = f;

							if( s & 0x08 )
								pDest[4] = f;

							if( s & 0x04 )
								pDest[5] = f;

							if( s & 0x02 )
								pDest[6] = f;

							if( s & 0x01 )
								pDest[7] = f;

							pDest += 8;
							}

						if( n > 0 ) {

							BYTE s = HIBYTE(HIWORD(Store));

							Store  = uByte-- ? (Store << 8) : (uByte = 3, *pFrom++);

							if( n > 0 ) {

								if( s & 0x80 )
									*pDest++ = f;
								else
									 pDest++;
								}

							if( n > 1 ) {

								if( s & 0x40 )
									*pDest++ = f;
								else
									 pDest++;
								}

							if( n > 2 ) {

								if( s & 0x20 )
									*pDest++ = f;
								else
									 pDest++;
								}

							if( n > 3 ) {

								if( s & 0x10 )
									*pDest++ = f;
								else
									 pDest++;
								}

							if( n > 4 ) {

								if( s & 0x08 )
									*pDest++ = f;
								else
									 pDest++;
								}

							if( n > 5 ) {

								if( s & 0x04 )
									*pDest++ = f;
								else
									 pDest++;
								}

							if( n > 6 ) {

								if( s & 0x02 )
									*pDest++ = f;
								else
									 pDest++;
								}

							if( n > 7 ) {

								if( s & 0x01 )
									*pDest++ = f;
								else
									 pDest++;
								}
							}

						pDest += m_cx - cx;
						}
					else
						pDest += m_cx;
					
					p += k;
					}
				}
			}
		}
	}

void CGdiSoftA888::CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p, COLOR c1, COLOR c2)
{
	if( x < m_cx && y < m_cy ) {

		int ox = cx;

		MakeMin(cx, m_cx - x);

		MakeMin(cy, m_cy - y);

		if( cx && cy ) {

			PDWORD d  = m_pData + x + y * m_cx;

			int    bc = (ox + 1) / 2;

			c1        = c1 & 0x7FFF;

			c2        = c2 & 0x7FFF;

			if( m_c1 != c1 || m_c2 != c2 ) {

				m_r1 = To888(c1);

				m_r2 = To888(c2);

				GetBlendBase(m_p1, m_p2, m_p3, m_p4, m_r2);

				DWORD cd = m_r1;

				cd >>= 3; m_p1 += (cd & 31);
				cd >>= 8; m_p2 += (cd & 31);
				cd >>= 8; m_p3 += (cd & 31);
				cd >>= 8; m_p4 += (cd & 31);

				for( UINT v = 0; v < 16; v++ ) {

					DWORD cr;
							
					UINT  af = v << 5;

					cr  = m_p1[af] <<  0;
					cr |= m_p2[af] <<  8;
					cr |= m_p3[af] << 16;
					cr |= m_p4[af] << 24;

					m_pm[v] = cr;
					}

				m_c1 = c1;

				m_c2 = c2;
				}

			for( int r = 0; r < cy; r++ ) {

				for( int c = 0;; c++ ) {

					DWORD t0 = m_pm[(p[c]>>4)&15];

					DWORD t1 = m_pm[(p[c]>>0)&15];

					if( c == bc - 1 ) {

						*d++ = t0;

						if( !(cx % 2) ) {

							*d++ = t1;
							}

						break;
						}

					*d++ = t0;

					*d++ = t1;
					}

				d += m_cx - cx;

				p += s ? s : bc;
				}
			}
		}
	}

void CGdiSoftA888::CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p, COLOR c1)
{
	if( x < m_cx && y < m_cy ) {

		int ox = cx;

		MakeMin(cx, m_cx - x);

		MakeMin(cy, m_cy - y);

		if( cx && cy ) {

			PDWORD d  = m_pData + x + y * m_cx;

			int    bc = (ox + 1) / 2;

			c1        = c1 & 0x7FFF;

			if( m_c1 != c1 || m_c2 != 0xFFFF ) {

				m_r1 = To888(c1);

				GetBlendBase(m_p1, m_p2, m_p3, m_p4, m_r1);

				m_c1 = c1;

				m_c2 = 0xFFFF;
				}

			for( int r = 0; r < cy; r++ ) {

				if( r + y >= 0 ) {

					PCBYTE f = p;

					UINT   v = 0;

					for( int c = -1;; ) {

						if( ++c == cx ) {

							break;
							}

						if( x + c >= 0 && (v = (*f>>4 & 15)) ) {

							if( v == 15 ) {

								*d++ = m_r1;
								}
							else {
								register DWORD cd = *d;

								register DWORD cr;
							
								v  <<= 5;

								cd >>= 3; cr  = m_p1[v + (cd & 31)] <<  0;
								cd >>= 8; cr |= m_p2[v + (cd & 31)] <<  8;
								cd >>= 8; cr |= m_p3[v + (cd & 31)] << 16;
								cd >>= 8; cr |= m_p4[v + (cd & 31)] << 24;

 								*d++ = cr;
								}
							}
						else
							d++;
						
						if( ++c == cx ) {

							break;
							}

						if( x + c >= 0 && (v = (*f>>0 & 15)) ) {

							if( v == 15 ) {

								*d++ = m_r1;
								}
							else {
								register DWORD cd = *d;

								register DWORD cr;

								v  <<= 5;

								cd >>= 3; cr  = m_p1[v + (cd & 31)] <<  0;
								cd >>= 8; cr |= m_p2[v + (cd & 31)] <<  8;
								cd >>= 8; cr |= m_p3[v + (cd & 31)] << 16;
								cd >>= 8; cr |= m_p4[v + (cd & 31)] << 24;

 								*d++ = cr;
								}
							}
						else
							d++;

						f++;
						}

					d += m_cx - cx;
					}
				else
					d += m_cx;

				p += s ? s : bc;
				}
			}
		}
	}

// Area Fill Support

void CGdiSoftA888::FillArea(int x1, int y1, int x2, int y2)
{
	if( m_uBrushMode > fillNull ) {

		MakeMax(x1, 0);

		MakeMax(y1, 0);

		MakeMin(x2, m_cx);

		MakeMin(y2, m_cy);

		int dx = x2 - x1;

		int dy = y2 - y1;

		if( dx > 0 && dy > 0 ) {

			if( m_uBrushMode == fillSolid ) {

				for( ; dy; dy-- ) {

					IntWriteRun(x1, y1, dx, m_dwBrushColor);
		
					y1++;
					}

				return;
				}

			if( m_uBrushMode == fillPattern ) {

				PDWORD p = m_pData + x1 + y1 * m_cx;

				for( ; dy; dy-- ) {

					int    b = y1 % 8;

					BYTE   t = m_bBrush[b];

					BYTE   s = BitPick(x1 % 8);

					if( m_Brush.m_Trans ) {

						for( int n = dx; n; n-- ) {

							if( t & s ) {

								*p++ = m_dwBrushFore;
								}
							else
								p++;

							BitWalk(s);
							}
						}
					else {
						for( int n = dx; n; n-- ) {

							if( t & s )
								*p++ = m_dwBrushFore;
							else
								*p++ = m_dwBrushBack;

							BitWalk(s);
							}
						}
			
					y1 += 1;

					p  += m_cx - dx;
					}

				return;
				}

			if( m_uBrushMode == fillBitmap ) {

				for( ; dy; dy-- ) {

					IntWriteBits(x1, y1, dx);

					y1++;
					}

				return;
				}
			}
		}
	}

// Alpha Blending

void CGdiSoftA888::BlendHorz(int yd)
{
	if( m_uBrushMode > fillNull ) {

		if( yd >= 0 && yd < m_cy ) {

			int cx = sampScale * m_cx;

			if( m_o2 > cx ) {

				ArrayZero(m_xb + cx, m_o2 - cx);

				m_o2 = cx;
				}

			if( m_uBrushMode == fillSolid ) {

				DWORD cf;

				PBYTE p1, p2, p3, p4;

				GetBlendBase(cf, p1, p2, p3, p4);

				int    xp = m_o1;

				PDWORD pd = m_pData + xp + yd * m_cx;

				while( xp < m_o2 ) {

					int af;

					if( (af = m_xb[xp]) ) {

						int n = 0;

						while( af >= sampLimit - 1 ) {

							if( ++n + xp >= m_o2 ) {

								break;
								}
					
							af = m_xb[xp + n];
							}

						if( n ) {

							IntWriteRun(pd, n, cf);

							xp += n;

							pd += n;
							}
						else {
							register DWORD cd = *pd;

							register DWORD cr;

							af <<= 5;

							cd >>= 3; cr  = p1[af + (cd & 31)] <<  0;
							cd >>= 8; cr |= p2[af + (cd & 31)] <<  8;
							cd >>= 8; cr |= p3[af + (cd & 31)] << 16;
							cd >>= 8; cr |= p4[af + (cd & 31)] << 24;

 							*pd++ = cr;

							xp++;
							}
						}
					else {
						pd++;

						xp++;
						}
					}

				return;
				}

			if( m_uBrushMode == fillPattern ) {

				int    xp = m_o1;

				int    br = yd % 8;

				BYTE   bd = m_bBrush[br];

				BYTE   bm = BitPick(xp % 8);

				PDWORD pd = m_pData + xp + yd * m_cx;

				while( xp < m_o2 ) {

					int af;

					if( (af = m_xb[xp]) ) {

						int n = 0;

						while( af >= sampLimit - 1 ) {

							if( ++n + xp >= m_o2 ) {

								break;
								}
					
							af = m_xb[xp + n];
							}

						if( n ) {

							xp += n;

							for( ; n; n-- ) {

								if( bd & bm )
									*pd++ = m_dwBrushFore;
								else
									*pd++ = m_dwBrushBack;

								BitWalk(bm);
								}
							}
						else {
							DWORD cf;

							PBYTE p1, p2, p3, p4;

							GetBlendBase(cf, p1, p2, p3, p4, (bd & bm) ? TRUE : FALSE);
							
							register DWORD cd = *pd;

							register DWORD cr;

							af <<= 5;

							cd >>= 3; cr  = p1[af + (cd & 31)] <<  0;
							cd >>= 8; cr |= p2[af + (cd & 31)] <<  8;
							cd >>= 8; cr |= p3[af + (cd & 31)] << 16;
							cd >>= 8; cr |= p4[af + (cd & 31)] << 24;

 							*pd++ = cr;

							xp++;

							BitWalk(bm);
							}
						}
					else {
						pd++;

						xp++;

						BitWalk(bm);
						}
					}

				return;
				}

			if( m_uBrushMode == fillBitmap ) {

				int    xp = m_o1;

				PDWORD pd = m_pData + xp + yd * m_cx;

				PDWORD pf = PDWORD(m_Brush.m_Bits) + m_Brush.m_bcx * (yd % m_Brush.m_bcy);

				while( xp < m_o2 ) {

					int af;

					if( (af = m_xb[xp]) ) {

						int n = 0;

						while( af >= sampLimit - 1 ) {

							if( ++n + xp >= m_o2 ) {

								break;
								}
					
							af = m_xb[xp + n];
							}

						if( n ) {

							IntWriteBits(xp, yd, n);

							xp += n;

							pd += n;
							}
						else {
							DWORD cf = pf[xp % m_Brush.m_bcx];

							PBYTE p1, p2, p3, p4;

							GetBlendBase(p1, p2, p3, p4, cf);
							
							register DWORD cd = *pd;

							register DWORD cr;

							af <<= 5;

							cd >>= 3; cr  = p1[af + (cd & 31)] <<  0;
							cd >>= 8; cr |= p2[af + (cd & 31)] <<  8;
							cd >>= 8; cr |= p3[af + (cd & 31)] << 16;
							cd >>= 8; cr |= p4[af + (cd & 31)] << 24;

 							*pd++ = cr;

							xp++;
							}
						}
					else {
						pd++;

						xp++;
						}
					}

				return;
				}
			}
		}
	}

void CGdiSoftA888::BlendVert(int xd)
{
	if( m_uBrushMode > fillNull ) {

		if( xd >= 0 && xd < m_cx ) {

			int cy = sampScale * m_cy;

			if( m_o2 > cy ) {

				ArrayZero(m_xb + cy, m_o2 - cy);

				m_o2 = cy;
				}

			if( m_uBrushMode == fillSolid ) {

				DWORD cf;

				PBYTE p1, p2, p3, p4;

				GetBlendBase(cf, p1, p2, p3, p4);

				int    yp = m_o1;

				PDWORD pd = m_pData + xd + yp * m_cx;

				while( yp < m_o2 ) {

					int af;

					if( (af = m_xb[yp]) ) {

						int n = 0;

						while( af >= sampLimit - 1 ) {

							if( ++n + yp >= m_o2 ) {

								break;
								}
					
							af = m_xb[yp + n];
							}

						if( n ) {

							while( n-- ) {
 							
								*pd = cf;

								pd += m_cx;

								yp += 1;
								}
							}
						else {
							register DWORD cd = *pd;

							register DWORD cr;

							af <<= 5;

							cd >>= 3; cr  = p1[af + (cd & 31)] <<  0;
							cd >>= 8; cr |= p2[af + (cd & 31)] <<  8;
							cd >>= 8; cr |= p3[af + (cd & 31)] << 16;
							cd >>= 8; cr |= p4[af + (cd & 31)] << 24;

 							*pd = cr;

							pd += m_cx;

							yp += 1;
							}
						}
					else {
						pd += m_cx;

						yp += 1;
						}
					}

				return;
				}

			if( m_uBrushMode == fillPattern ) {

				// Not supported or needed in horizontal mode.

				AfxAssert(FALSE);
				
				return;
				}

			if( m_uBrushMode == fillBitmap ) {
				
				// Not supported or needed in horizontal mode.

				AfxAssert(FALSE);

				return;
				}
			}
		}
	}

// Line Drawing Support

void CGdiSoftA888::LineFill(int x, int y, int cx, int cy, BOOL fFore)
{
	if( x < 0 ) {

		cx += x;
		x   = 0;
		}

	if( y < 0 ) {

		cy += y;
		y   = 0;
		}

	if( x < m_cx && y < m_cy ) {

		MakeMin(cx, m_cx - x);

		MakeMin(cy, m_cy - y);

		if( cx > 0 && cy > 0 ) {

			DWORD c = fFore ? m_dwPenFore : m_dwPenBack;

			for( ; cy; cy-- ) {

				IntWriteRun(x, y, cx, c);

				y++;
				}
			}
		}
	}

void CGdiSoftA888::LineSet(int x, int y, BOOL fFore)
{
	if( fFore || m_Pen.m_Trans == modeOpaque ) {

		if( x >= 0 && x < m_cx ) {

			if( y >= 0 && y < m_cy ) {

				DWORD c = fFore ? m_dwPenFore : m_dwPenBack;

				m_pData[x + y * m_cx] = c;
				}
			}
		}
	}

void CGdiSoftA888::LineCap(int l, int r, int y)
{
	if( m_fPenFore || m_Pen.m_Trans == modeOpaque ) {

		if( y >= 0 && y < m_cy ) {

			MakeMax(l, 0);

			MakeMax(r, 0);

			MakeMin(l, m_cx - 1);

			MakeMin(r, m_cx - 1);

			if( l <= r ) {

				DWORD c = m_fPenFore ? m_dwPenFore : m_dwPenBack;

				IntWriteRun(l, y, r - l + 1, c);
				}
			}
		}
	}

// Glyph Expansion

bool CGdiSoftA888::ExpandGlyph(int cx, int cy, int &s, PCBYTE &p)
{
	int i = 4 * ((cx + 31) / 32);

	if( s == 0 ) {

		s = i;
		}

	if( s != i ) {

		PDWORD pWork = m_Glyph;

		if( s == 1 ) {

			int b = -1;

			for( int n = 0; n < cy; n++, b-- ) {

				if( b == -1 ) {
						
					b = 3;
					}

				UINT i   = (n & ~3) + b;

				pWork[n] = DWORD(p[i]) << 24;
				}
			}

		if( s == 2 ) {

			int b = -1;

			for( int n = 0; n < cy; n++, b-- ) {

				if( b == -1 ) {
						
					b = 1;
					}

				UINT i   = ((n & ~1) + b) * 2;

				pWork[n] = DWORD((WORD &) p[i]) << 16;
				}
			}

		if( s == 3 ) {

			for( int n = 0; n < cy; n++ ) {

				pWork[n] = ((DWORD &) p[n * 3]) << 8;
				}
			}

		p = PCBYTE(pWork);

		s = 4;

		return true;
		}

	return false;
	}

// Tool Realization

void CGdiSoftA888::OnNewFont(void)
{
	m_dwFontFore = To888(m_Font.m_Fore);
	
	m_dwFontBack = To888(m_Font.m_Back);
	}

void CGdiSoftA888::OnNewBrush(void)
{
	if( m_uBrushMode > fillNull ) {

		if( m_Brush.m_Rich & 0x80000000 ) {

			m_dwBrushFore = m_Brush.m_Rich;
		
			m_dwBrushBack = To888(m_Brush.m_Back);
			}
		else {
			m_dwBrushFore = To888(m_Brush.m_Fore);
		
			m_dwBrushBack = To888(m_Brush.m_Back);
			}

		if( m_uBrushMode == fillSolid ) {

			if( m_Brush.m_Style == brushFore ) {

				m_dwBrushColor = m_dwBrushFore;
				}
			else
				m_dwBrushColor = m_dwBrushBack;
			}

		if( m_uBrushMode == fillPattern ) {

			GetBrushData();
			}
		}
	}

void CGdiSoftA888::OnNewPen(void)
{
	m_dwPenFore = To888(m_Pen.m_Fore);
	
	m_dwPenBack = To888(m_Pen.m_Back);
	}

// Implementation

void CGdiSoftA888::GetBrushData(void)
{
	static BYTE const bBrush[][8] = {

		{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },	// brushBlack
		{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF },	// brushWhite
		{ 0xAA, 0x00, 0xAA, 0x00, 0xAA, 0x00, 0xAA, 0x00 },	// brushGray25
		{ 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55 },	// brushGray50
		{ 0xFF, 0x55, 0xFF, 0x55, 0xFF, 0x55, 0xFF, 0x55 },	// brushGray75
		{ 0x88, 0x44, 0x22, 0x11, 0x88, 0x44, 0x22, 0x11 },	// brushHatchF
		{ 0x11, 0x22, 0x44, 0x88, 0x11, 0x22, 0x44, 0x88 },	// brushHatchB
		{ 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00 },	// brushHatchH
		{ 0x44, 0x44, 0x44, 0x44, 0x44, 0x44, 0x44, 0x44 },	// brushHatchV
		{ 0x44, 0x44, 0xFF, 0x44, 0x44, 0x44, 0xFF, 0x44 },	// brushHatchX
		{ 0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81 },	// brushHatchD
		{ 0x18, 0x24, 0x42, 0x81, 0x18, 0x24, 0x42, 0x81 },	// brushWave
		{ 0xC0, 0x60, 0x30, 0x18, 0x0C, 0x06, 0x03, 0x81 },	// brushHatchFB
		{ 0x81, 0x03, 0x06, 0x0C, 0x18, 0x30, 0x60, 0xC0 },	// brushHatchBB

		};

	memcpy(m_bBrush, bBrush[m_Brush.m_Style - 1], 8);
	}

void CGdiSoftA888::MakeEightTable(void)
{
	if( !m_pEight ) {

		m_pEight = new BYTE [ 32768 ];

		LoadEightBasic();

		LoadEightExact();
		}
	}

void CGdiSoftA888::LoadEightBasic(void)
{
	WORD n = 0;

	for( BYTE b = 0; b < 32; b++ ) {

		int bi = 36 * ((b * 5 + 15) / 31);

		for( BYTE g = 0; g < 32; g++ ) {

			int gi = 6 * ((g * 5 + 15) / 31);

			for( BYTE r = 0; r < 32; r++ ) {

				int  ri = ((r * 5 + 15) / 31);

				int ni = n++;

				m_pEight[ni] = BYTE(24 + ri + gi + bi);
				}
			}
		}
	}

void CGdiSoftA888::LoadEightExact(void)
{
	for( UINT c = 0; c < 256; c++ ) {

		if( c == 0x10 || c == 0x18 ) {

			continue;
			}

		if( c == 0xEF || c == 0xF0 || c == 0xFF ) {

			continue;
			}

		DWORD rgb = DispGetPaletteEntry(c);

		BYTE r = LOBYTE(LOWORD(rgb)) >> 3;
		BYTE g = HIBYTE(LOWORD(rgb)) >> 3;
		BYTE b = LOBYTE(HIWORD(rgb)) >> 3;

		WORD n = GetRGB(r, g, b);

		m_pEight[n] = BYTE(c);
		}
	}

void CGdiSoftA888::FreeEightTable(void)
{
	delete [] m_pEight;
	}

void CGdiSoftA888::MakeGrayTable(void)
{
	if( !m_pGray ) {

		m_pGray = new DWORD [ 32768 ];

		LoadGrayTable();
		}
	}

void CGdiSoftA888::LoadGrayTable(void)
{
	UINT n;

	for( n = 0; n < 32767; n++ ) {

		int   r    = ((n >>  0) & 0x1F);
		
		int   g    = ((n >>  5) & 0x1F);

		int   b    = ((n >> 10) & 0x1F);

		BYTE  i    = BYTE((14*r + 45*g + 5*b) / 8);

		DWORD c    = MAKELONG(MAKEWORD(i,i),MAKEWORD(i,0xFF));

		m_pGray[n] = c;
		}

	m_pGray[n] = 0xFFFFFFFF;
	}

void CGdiSoftA888::FreeGrayTable(void)
{
	if( m_pGray ) {
		
		delete [] m_pGray;
		}
	}

void CGdiSoftA888::MakeBlendTable(double gamma)
{
	m_pBlend = new BYTE [ 32 * 32 * 16 ];

	LoadBlendTable(gamma);
	}

void CGdiSoftA888::LoadBlendTable(double gamma)
{
	// The blend table is used to speed-up alpha blending. It contains the resulting
	// color channel value between 0 and 255 for a given foreground, background and
	// blend value. The foreground and background values range from 0 to 31 and the
	// mix value rnges from 0 to 15. We use this table for each channel of the pixel
	// we are processing and thereby get the fully blended output value. We could
	// in theory use the gamma value here, but the hueristic below works quite well.

	int uPos = 0;

	for( int f = 0; f < 32; f++ ) {

		for( int v = 0; v < 16; v++ ) {

			for( int b = 0; b < 32; b++ ) {

				if( v == 0 ) {

					m_pBlend[uPos++] = BYTE((b<<3) | (b>>2));

					continue;
					}

				if( v == 15 ) {

					m_pBlend[uPos++] = BYTE((f<<3) | (f>>2));

					continue;
					}

				if( f > b ) {

					int r = b + ((f - b) * (16 + v) / 31);

					m_pBlend[uPos++] = BYTE(8 * r);

					continue;
					}

				if( f < b ) {

					int r = f + ((b - f) * (31 - v) / 31);

					m_pBlend[uPos++] = BYTE(8 * r);

					continue;
					}

				m_pBlend[uPos++] = BYTE((f<<3) | (f>>2));
				}
			}
		}
	}

void CGdiSoftA888::FreeBlendTable(void)
{
	delete [] m_pBlend;
	}

// End of File
