
#include "intern.hpp"

#include "gdiplus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// GDI+ Token
//

static ULONG gdipToken;

//////////////////////////////////////////////////////////////////////////
//
// Init and Term Hooks
//

global void DLLAPI G3GraphInit(void)
{
	GdiplusStartupOutput Output;

	GdiplusStartupInput  Input;

	memset(&Input, 0, sizeof(Input));

	Input.GdiplusVersion = 1;

	GdiplusStartup(&gdipToken, &Input, &Output);
	}

global void DLLAPI G3GraphTerm(void)
{
//	GdiplusShutdown(gdipToken);
	}

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load G3Graph\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			CButtonBitmap *pEdit16 = New CButtonBitmap( CBitmap(L"ToolGraph16"),
								    CSize(16, 16),
								    28
								    );

			CButtonBitmap *pText16 = New CButtonBitmap( CBitmap(L"ToolText16"),
								    CSize(16, 16),
								    26
								    );

			afxButton->AppendBitmap(0x3000, pEdit16);

			afxButton->AppendBitmap(0x3001, pText16);

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading G3Graph\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxButton->RemoveBitmap(0x3000);

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
