
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Organize Menu

BOOL CPageEditorWnd::OnOrganizeGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_ORG_GROUP,		MAKELONG(0x0014, 0x3000),
		IDM_ORG_UNGROUP,	MAKELONG(0x0015, 0x3000),
		IDM_ORG_WIDGET,		MAKELONG(0x0016, 0x3000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnOrganizeControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_ORG_GROUP:

			Src.EnableItem(CanGroup());
			
			break;

		case IDM_ORG_WIDGET:

			Src.EnableItem(CanWidget());
			
			break;

		case IDM_ORG_UNGROUP:
			
			Src.EnableItem(CanUngroup());
			
			break;

		case IDM_ORG_NORMALIZE:

			Src.EnableItem(CanNormalize());

			break;

		case IDM_ORG_EXPAND:

			Src.EnableItem(CanExpand());

			break;

		case IDM_ORG_BIND_WIDGET:

			Src.EnableItem(CanBindWidget());

			break;

		case IDM_ORG_CLEAR_BINDING:

			Src.EnableItem(CanClearBinding());

			break;

		case IDM_ORG_SAVE_WIDGET:

			Src.EnableItem(CanSaveWidget());

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnOrganizeCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ORG_GROUP:
			
			OnOrganizeGroup();
			
			break;

		case IDM_ORG_WIDGET:
			
			OnOrganizeWidget();
			
			break;

		case IDM_ORG_UNGROUP:
			
			OnOrganizeUngroup();
			
			break;

		case IDM_ORG_NORMALIZE:

			OnOrganizeNormalize();

			break;

		case IDM_ORG_EXPAND:

			OnOrganizeExpand();

			break;

		case IDM_ORG_BIND_WIDGET:

			OnOrganizeBindWidget();

			break;

		case IDM_ORG_CLEAR_BINDING:

			OnOrganizeClearBinding();

			break;

		case IDM_ORG_SAVE_WIDGET:

			OnOrganizeSaveWidget();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::CanGroup(void)
{
	if( !m_fRead ) {
		
		return HasMultiSelect();
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanWidget(void)
{
	if( !m_fRead && !m_fScratch ) {
		
		if( HasLoneSelect() ) {

			CPrim     *pPrim = GetLoneSelect();

			CPrimList *pList = (CPrimList *) pPrim->GetParent();
			
			if( pPrim->IsWidget() ) {

				return FALSE;
				}

			if( pList->GetItemCount() == 1 ) {

				CItem *pHost = pList->GetParent();

				if( pHost->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

					return FALSE;
					}
				}


			return TRUE;
			}

		return CanGroup();
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanUngroup(void)
{
	if( !m_fRead ) {

		if( HasLoneSelect() ) {
			
			CPrim *pPrim = GetLoneSelect();
			
			if( pPrim->IsGroup() ) {

				CPrimSet *pSet = (CPrimSet *) pPrim;

				return !pSet->m_LockList;
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanNormalize(void)
{
	if( !m_fRead ) {

		if( HasLoneSelect() ) {
			
			CPrim *pPrim = GetLoneSelect();
			
			if( pPrim->IsGroup() ) {

				CPrimGroup *pGroup = (CPrimGroup *) pPrim;

				if( !pGroup->m_LockList ) {

					if( pGroup->GetRect() == pGroup->GetUsedRect() ) {

						return FALSE;
						}

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanExpand(void)
{
	if( !m_fRead ) {

		if( HasLoneSelect() ) {
			
			CPrim *pPrim = GetLoneSelect();
			
			if( pPrim->IsGroup() ) {

				CPrimGroup *pGroup = (CPrimGroup *) pPrim;

				return !pGroup->m_LockList;
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanBindWidget(void)
{
	if( !m_fScratch && HasSelect() ) {

		if( ArePropsLocked() ) {

			return FALSE;
			}

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->GetBindMode() >= 1 ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanClearBinding(void)
{
	if( !m_fScratch && HasSelect() ) {

		if( ArePropsLocked() ) {

			return FALSE;
			}

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->GetBindState() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanSaveWidget(void)
{
	if( !m_fScratch && HasSelect() ) {

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CPageEditorWnd::OnOrganizeGroup(void)
{
	if( CanGroup() ) {

		CString Fixed = m_pItem->GetDatabase()->AllocFixedString();

		CCmd *  pCmd  = New CCmdGroup(Fixed, AfxRuntimeClass(CPrimGroup));

		LocalExecCmd(pCmd);
		}
	}

BOOL CPageEditorWnd::OnOrganizeWidget(void)
{
	if( CanWidget() ) {

		// TODO -- If we have a single item selected and it's a group,
		// ungroup it first and then make it into a widget so that we
		// do not end up with something rather confusing...

		CString Fixed = m_pItem->GetDatabase()->AllocFixedString();

		CCmd *  pCmd  = New CCmdGroup(Fixed, AfxRuntimeClass(CPrimWidget));

		LocalExecCmd(pCmd);

		if( !OnEditItemProps(CString(IDS_DATA)) ) {

			SendRewind();

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::OnOrganizeUngroup(void)
{
	if( CanUngroup() ) {

		CPrimGroup *pGroup = (CPrimGroup *) GetLoneSelect();

		if( pGroup->IsWidget() ) {

			CString Text = CString(IDS_UNGROUP_WIDGET_1) +
				       CString(IDS_UNGROUP_WIDGET_2) +
				       CString(IDS_UNGROUP_WIDGET_3) +
				       CString(IDS_UNGROUP_WIDGET_4) ;

			if( NoYes(Text) == IDNO ) {

				return;
				}
			}

		CCmd *pCmd = New CCmdUngroup(pGroup);

		LocalExecCmd(pCmd);
		}
	}

BOOL CPageEditorWnd::OnOrganizeNormalize(void)
{
	CPrim *pPrim = GetLoneSelect();

	if( pPrim->IsGroup() ) {

		HGLOBAL      hPrev = pPrim->TakeSnapshot();

		CPrimGroup * pGroup = (CPrimGroup *) pPrim;

		pGroup->SetRect(pGroup->GetUsedRect());

		if( !m_fScratch ) {

			CString    Menu = CString(IDS_NORMALIZE_GROUP);

			CCmdItem * pCmd = New CCmdItem(Menu, pPrim, hPrev);

			LocalSaveCmd(pCmd);
			}

		pPrim->SetDirty();

		UpdateAll();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnOrganizeExpand(void)
{
	CPrim *pPrim = GetLoneSelect();

	if( pPrim->IsGroup() ) {

		HGLOBAL      hPrev = pPrim->TakeSnapshot();

		CPrimGroup * pGroup = (CPrimGroup *) pPrim;

		CRect        Rect   = pGroup->GetRect() + 50;

		MakeMax(Rect.left,   m_WorkRect.left);
		
		MakeMax(Rect.top,    m_WorkRect.top);
		
		MakeMin(Rect.right,  m_WorkRect.right);
		
		MakeMin(Rect.bottom, m_WorkRect.bottom);

		pGroup->SetRect(Rect);

		if( !m_fScratch ) {

			CString    Menu = CString(IDS_EXPAND_GROUP);

			CCmdItem * pCmd = New CCmdItem(Menu, pPrim, hPrev);

			LocalSaveCmd(pCmd);
			}

		pPrim->SetDirty();

		UpdateAll();

		SelectWorkList();

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::OnOrganizeBindWidget(void)
{
	if( CanBindWidget() ){

		CPrim     * pPrim  = GetLoneSelect();

		CDatabase * pDbase = pPrim->GetDatabase();

		UINT        uMode  = pPrim->GetBindMode();

		if( uMode >= 1 ) {
			
			CTagSelectDialog *pDlg = NULL;

			if( uMode == 1 ) {

				pDlg = New CTagSelectDialog( pDbase,
							     FALSE,
							     L""
							     );
				}
			
			if( uMode == 2 ) {

				CString Class;

				pPrim->GetBindClass(Class);

				pDlg = New CTagSelectDialog( pDbase,
							     TRUE,
							     Class
							     );
				}

			if( pDlg ) {

				if( m_System.ExecSemiModal(*pDlg, L"Tags") ) {

					if( UsesDetails(pPrim) ) {

						if( !m_fScratch ) {
	
							CString Item = m_pPage->GetFixedPath();

							CString Menu = IDS_BIND_WIDGET;

							CCmd *pCmd = New CCmdMulti(Item, Menu, navAll);

							LocalSaveCmd(pCmd);
							}
						}

					HGLOBAL hPrev = pPrim->TakeSnapshot();

					CString Tag   = pDlg->GetTag();

					BOOL    fOkay = pPrim->BindToTag(Tag, Tag);

					CCmdItem *pCmd = New CCmdItem( CString(IDS_BIND_WIDGET),
								       pPrim,
								       hPrev
								       );

					if( pCmd->IsNull() ) {

						delete pCmd;
						}
					else {
						LocalSaveCmd(pCmd);

						pPrim->SetDirty();

						UpdateImage();

						Invalidate(FALSE);
						}

					if( UsesDetails(pPrim) ) {

						MakeDetails(pPrim, Tag);

						if( !m_fScratch ) {

							CString Item = m_pPage->GetFixedPath();

							CString Menu = IDS_BIND_WIDGET;

							CCmd *pCmd = New CCmdMulti(Item, Menu, navAll);

							LocalSaveCmd(pCmd);
							}
						}

					if( !fOkay ) {

						CString Text = IDS_AT_LEAST_ONE;

						Error(Text);
						}
					}

				delete pDlg;
				}
			}
		}
	}

void CPageEditorWnd::OnOrganizeClearBinding(void)
{
	if( CanClearBinding() ) {

		CStringArray Zoom;

		CPrim *pPrim = GetLoneSelect();

		if( FindDetails(pPrim, Zoom) ) {

			CString Item = m_pPage->GetFixedPath();

			CString Menu = IDS_CLEAR_BINDING;

			CCmd *  pCmd = New CCmdMulti(Item, Menu, navAll);

			LocalExecCmd(pCmd);
			}

		HGLOBAL hPrev = pPrim->TakeSnapshot();

		pPrim->ClearBinding();

		CCmdItem *pCmd = New CCmdItem( CString(IDS_CLEAR_BINDING),
					       pPrim,
					       hPrev
					       );

		if( pCmd->IsNull() ) {

			delete pCmd;
			}
		else {
			LocalSaveCmd(pCmd);

			pPrim->SetDirty();

			UpdateImage();

			Invalidate(FALSE);
			}

		if( KillDetails(pPrim, Zoom) ) {

			CString Item = m_pPage->GetFixedPath();

			CString Menu = IDS_CLEAR_BINDING;

			CCmd *  pCmd = New CCmdMulti(Item, Menu, navAll);

			LocalExecCmd(pCmd);
			}
		}
	}

BOOL CPageEditorWnd::OnOrganizeSaveWidget(void)
{
	if( CanSaveWidget() ) {

		CPrimWidget * pPrim = (CPrimWidget *) GetLoneSelect();

		CModule     * pApp  = afxModule->GetApp();

		CFilename     Path  = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Widgets");

		CFilename     Name  = pPrim->m_File;

		if( Name.HasPath() ) {

			Path += Name.GetBarePath();

			Name  = Name.GetName();
			}

		CSaveFileDialog Dlg;

		Dlg.SetLastPath (Path);

		Dlg.SetCaption  (CString(IDS_SAVE_WIDGET));

		Dlg.SetFilename (Name);

		Dlg.SetFilter   (CString(IDS_WIDGET_FILES));
			
		if( Dlg.ExecAndCheck(ThisObject) ) {

			CTextStreamMemory Stream;

			if( Stream.OpenSave() ) {

				CTreeFile Tree;

				if( Tree.OpenSave(Stream) ) {

					AddPrimsToTreeFile(Tree, TRUE);

					Tree.Close();

					if( Stream.SaveToFile(Dlg.GetFilename()) ) {

						Dlg.SaveLastPath(L"Widget");

						return TRUE;
						}
					}
				}

			Dlg.SaveLastPath(L"Widget");

			Error(CString(IDS_UNABLE_TO_SAVE));
			}
		}

	return FALSE;
	}

// Widget Details

BOOL CPageEditorWnd::UsesDetails(CPrim *pPrim)
{
	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

		CPrimWidget *pWidget = (CPrimWidget *) pPrim;

		if( pWidget->m_pData->m_AutoZoom ) {

			CPrimWidgetPropList *pList = pWidget->m_pData->m_pList;

			UINT                uCount = pList->GetItemCount();

			for( UINT n = 0; n < uCount; n++ ) {

				CPrimWidgetProp *pProp = pList->GetItem(n);

				if( pProp ) {

					if( pProp->IsDetailsRef() ) {

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::FindDetails(CPrim *pPrim, CStringArray &Zoom)
{
	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

		CPrimWidget * pWidget = (CPrimWidget *) pPrim;

		if( pWidget->m_pData->m_AutoZoom ) {

			CPrimWidgetPropList *pList = pWidget->m_pData->m_pList;

			UINT                uCount = pList->GetItemCount();

			for( UINT n = 0; n < uCount; n++ ) {

				CPrimWidgetProp *pProp = pList->GetItem(n);

				if( pProp && pProp->m_pValue ) {

					if( pProp->IsDetailsRef() ) {

						CString Page = pProp->m_pValue->GetSource(TRUE);

						Zoom.Append(Page);
						}
					}
				}
			}
		}

	return Zoom.GetCount() ? TRUE : FALSE;
	}

BOOL CPageEditorWnd::KillDetails(CPrim *pPrim, CStringArray &Zoom)
{
	CPageList *pList = (CPageList *) m_pPage->GetParent();

	UINT      uCount = Zoom.GetCount();

	if( uCount ) {

		CString Nav = m_System.GetNavPos();

		for( UINT n = 0; n < uCount; n++ ) {

			CItem *pPage = pList->FindByName(Zoom[n]);

			if( pPage ) {

				m_System.Navigate(pPage->GetFixedPath());

				afxMainWnd->SendMessage(WM_COMMAND, IDM_ITEM_DELETE);
				}
			}

		m_System.Navigate(Nav);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::MakeDetails(CPrim *pPrim, CString Tag)
{
	CPrimWidget         *pWidget = (CPrimWidget *) pPrim;

	CPrimWidgetPropData *pData   = pWidget->m_pData;

	CPrimWidgetPropList *pList   = pData->m_pList;

	////////

	CStringArray Files;

	pData->m_Zoom.Tokenize(Files, ',');

	UINT uCount = Files.GetCount();

	////////

	if( uCount ) {

		CDispPage *pPrev = m_pPage;

		CString    Base  = pPrev->GetName();
	
		CString    Prev  = m_System.GetNavPos();

		CArray <CDispPage *> Pages;

		for( UINT p = 0; p < 2; p++ ) {

			for( UINT n = 0; n < uCount; n++ ) {

				CString PropName = MakePropName(n, uCount);

				CString PageName = MakePageName(n, uCount, Base);

				////////

				CPrimWidgetProp *pProp = pList->FindName(PropName);

				if( !pProp || !pProp->IsDetailsRef() ) {

					continue;
					}

				////////

				if( p == 0 ) {

					CDispPage *pPage = NULL;

					MakePage(pProp, PageName, pPage);

					Pages.Append(pPage);

					pProp->Bind(pPage->GetName(), TRUE);
					}

				if( p == 1 ) {

					CDispPage *pPage = Pages[n];

					m_System.Navigate(pPage->GetFixedPath());

					CFilename Base = pWidget->m_File;

					CFilename File = Files[n];

					File.TrimBoth();

					if( Base.HasPath() ) {

						CString Path = Base.GetBarePath();

						File         = Path + File;
						}

					if( MakeWidget(File) ) {

						CPrim    * pPrim = GetLoneSelect();

						HGLOBAL    hPrev = pPrim->TakeSnapshot();

						pPrim->BindToTag(Tag, Tag);

						BindDetails(pPrim, Base, Pages);

						CCmdItem * pCmd  = New CCmdItem( CString(IDS_BIND_WIDGET),
										 pPrim,
										 hPrev
										 );

						if( pCmd->IsNull() ) {

							delete pCmd;
							}
						else {
							LocalSaveCmd(pCmd);

							pPrim->SetDirty();
							}
						}
					}
				}
			}

		m_System.Navigate(Prev);
		}

	return TRUE;
	}

BOOL CPageEditorWnd::BindDetails(CPrim *pPrim, CString Base, CArray <CDispPage *> &Pages)
{
	CPrimWidget         *pWidget = (CPrimWidget *) pPrim;

	CPrimWidgetPropData *pData   = pWidget->m_pData;

	CPrimWidgetPropList *pList   = pData->m_pList;

	UINT                 uCount  = Pages.GetCount();

	////////

	for( UINT n = 0; n < uCount; n++ ) {

		CString PropName = MakePropName(n, uCount);

		CString PageName = Pages[n]->GetName();

		BindDetails(pList, PropName, PageName);
		}

	BindDetails(pList, L"DetailsP", Base);

	return TRUE;
	}

BOOL CPageEditorWnd::BindDetails(CPrimWidgetPropList *pList, CString Name, CString Code)
{
	CPrimWidgetProp *pProp = pList->FindName(Name);

	if( pProp ) {
		
		if( pProp->IsDetailsRef() ) {

			pProp->Bind(Code, TRUE);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::MakePage(CPrimWidgetProp *pProp, CString Name, CDispPage * &pPage)
{
	if( pProp->m_pValue ) {

		CString     Page  = pProp->m_pValue->GetSource(TRUE);

		CPageList * pList = (CPageList *) m_pPage->GetParent();

		CItem     * pItem  = pList->FindByName(Page);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

				pPage = (CDispPage *) pItem;
				}
			}
		}

	if( pPage ) {

		m_System.Navigate(pPage->GetFixedPath());

		OnEditSelectAll();

		OnEditDelete(L"");

		return TRUE;
		}

	afxMainWnd->SendMessage( WM_COMMAND,
				 IDM_ITEM_NEW_NAMED,
				 LPARAM(PCTXT(Name))
				 );

	pPage = m_pPage;

	return TRUE;
	}

BOOL CPageEditorWnd::MakeWidget(CString Widget)
{
	CModule * pApp = afxModule->GetApp();

	CFilename Base = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Widgets");

	CFilename Name = Base + Widget + L".wid31";

	for( UINT p = 0; p < 2; p++ ) {

		CFilename Name = Base + Widget + ((p == 0) ? L".wid31" : L".wid");

		CTextStreamMemory Stream;

		if( Stream.LoadFromFile(Name) ) {

			HGLOBAL      hData = Stream.TakeOver();

			CDataObject *pData = New CDataObject;

			pData->AddGlobalData(m_cfData, hData);

			CCmd  * pCmd  = New CCmdPaste(L"", pData, ((p == 1) ? pasteLegacy : pasteDup));

			LocalExecCmd(pCmd);

			CPrim * pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

				CPrimWidget *pWidget = (CPrimWidget *) pPrim;

				pWidget->m_File = Name;
			}

			PerformAlign(TRUE, 2, 2, NULL, FALSE);

			return TRUE;
		}
	}

	return FALSE;
	}

CString CPageEditorWnd::MakePageName(UINT n, UINT c, CString Base)
{
	CString Name;

	Name.Printf(Base + L"_Zoom%u", n + 1);

	return Name;
	}

CString CPageEditorWnd::MakePropName(UINT n, UINT c)
{
	CString Name;

	Name.Printf(L"Details%u", n + 1);

	return Name;
	}

// End of File
