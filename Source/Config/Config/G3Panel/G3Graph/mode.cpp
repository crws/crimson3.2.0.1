
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Mode Menu

BOOL CPageEditorWnd::OnModeGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_MODE_SELECT, MAKELONG(0x0000, 0x3000),
		IDM_MODE_GRAB,	 MAKELONG(0x0002, 0x3000),
		IDM_MODE_ZOOM,	 MAKELONG(0x0001, 0x3000),
		IDM_MODE_TEXT,	 MAKELONG(0x0005, 0x3001),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnModeControl(UINT uID, CCmdSource &Src)
{
	BOOL fItem = FALSE;

	BOOL fText = FALSE;

	BOOL fLock = FALSE;

	BOOL fData  = FALSE;

	if( HasLoneSelect() ) {

		CPrim *pPrim = GetLoneSelect();

		if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

			CPrimWithText *pHost= (CPrimWithText *) pPrim;

			if( pHost->HasText() ) {

				fText = TRUE;

				fLock = !pHost->IsTextEditable();
				}

			if( pHost->HasData() ) {

				fData = TRUE;
				}

			fItem = TRUE;
			}
		}

	switch( uID ) {

		case IDM_MODE_SELECT:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_uMode == modeSelect);

			break;

		case IDM_MODE_GRAB:

			Src.EnableItem(m_fScrollH || m_fScrollV);

			Src.CheckItem (m_uMode == modeGrab);

			break;

		case IDM_MODE_ZOOM:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_uMode == modeZoom);

			break;

		case IDM_MODE_TEXT:

			Src.EnableItem(fItem && !fData && !fLock);

			Src.CheckItem (m_uMode == modeText);

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnModeCommand(UINT uID)
{
	switch( uID ) {

		case IDM_MODE_SELECT:

			SetMode(modeSelect);

			break;

		case IDM_MODE_GRAB:

			SetMode(modeGrab);

			break;

		case IDM_MODE_ZOOM:

			SetMode(modeZoom);

			break;

		case IDM_MODE_TEXT:

			OnTextAdd();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// End of File
