
#include "intern.hpp"

#include "ScratchPadEditorWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor for Scratch Pad
//

// Base Class

#define CBaseClass CPageEditorWnd

// Dynamic Class

AfxImplementDynamicClass(CScratchPadEditorWnd, CBaseClass);

// Constructor

CScratchPadEditorWnd::CScratchPadEditorWnd(void)
{
	m_fScratch = TRUE;

	LoadConfig();
	}

// Destructor

CScratchPadEditorWnd::~CScratchPadEditorWnd(void)
{
	}

// Attributes

CSize CScratchPadEditorWnd::GetBestSize(void) const
{
	return m_nScaleMin * m_DispSize;
	}

// End of File
