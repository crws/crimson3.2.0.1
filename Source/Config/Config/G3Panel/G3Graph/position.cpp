
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Prim Position Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CPrimPositionDialog, CStdDialog);

// Constructors

CPrimPositionDialog::CPrimPositionDialog(CPrim *pPrim, BOOL fRead, CSize Min, CRect Work)
{
	m_pPrim   = pPrim;

	m_fRead   = fRead;

	m_Min     = Min;

	m_Work    = Work;

	m_Caption = CFormat(CString(IDS_FMT_POSITION_2), pPrim->GetHumanName());

	SetName(L"PrimPositionDialog");

	LoadConfig();
	}

// Message Map

AfxMessageMap(CPrimPositionDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)
	AfxDispatchCommand(300,      OnChangeRel    )

	AfxMessageEnd(CPrimPositionDialog)
	};

// Message Handlers

BOOL CPrimPositionDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CRect  Rect = m_pPrim->GetRect();

	CPoint Org  = m_fRel ? m_Work.GetTopLeft() : CPoint(0, 0);

	GetDlgItem(100).SetWindowText(CPrintf(L"%u", Rect.left - Org.x));
	
	GetDlgItem(101).SetWindowText(CPrintf(L"%u", Rect.top  - Org.y));
	
	GetDlgItem(102).SetWindowText(CPrintf(L"%u", Rect.cx()));
	
	GetDlgItem(103).SetWindowText(CPrintf(L"%u", Rect.cy()));

	if( m_fRead ) {

		GetDlgItem(100).EnableWindow(FALSE);
	
		GetDlgItem(101).EnableWindow(FALSE);
		
		GetDlgItem(102).EnableWindow(FALSE);
		
		GetDlgItem(103).EnableWindow(FALSE);

		GetDlgItem(IDCANCEL).ShowWindow(SW_HIDE);

		GetDlgItem(IDOK).SetWindowText(CString(IDS_CLOSE_2));
		}

	CString l1, l2, l3;

	if( m_Work.left || m_Work.top ) {

		l1.Printf( CString(IDS_ENCLOSING_GROUP_2), 
			   m_Work.left,
			   m_Work.top,
			   m_Work.right,
			   m_Work.bottom
			   );
		}
	else {
		l1.Printf( CString(IDS_DISPLAY_PAGE_SIZE_2), 
			   m_Work.left,
			   m_Work.top,
			   m_Work.right,
			   m_Work.bottom
			   );
		}

	l2.Printf( CString(IDS_MINIMUM_PRIMITIVE_2),
		   m_Min.cx,
		   m_Min.cy
		   );

	l3.Printf( CString(IDS_MAXIMUM_PRIMITIVE_2),
		   m_Work.cx(),
		   m_Work.cy()
		   );

	GetDlgItem(200).SetWindowText(l1);
	
	GetDlgItem(201).SetWindowText(l2);
	
	GetDlgItem(202).SetWindowText(l3);

	CButton &Rel = (CButton &) GetDlgItem(300);

	Rel.EnableWindow(m_Work.left || m_Work.top);

	Rel.SetCheck(m_fRel);

	SetWindowText(m_Caption);

	return TRUE;
	}

// Command Handlers

BOOL CPrimPositionDialog::OnCommandOK(UINT uID)
{
	if( !m_fRead ) {

		CRect Rect;

		Rect.left   = watoi(GetDlgItem(100).GetWindowText());

		Rect.top    = watoi(GetDlgItem(101).GetWindowText());
		
		Rect.right  = watoi(GetDlgItem(102).GetWindowText()) + Rect.left;
		
		Rect.bottom = watoi(GetDlgItem(103).GetWindowText()) + Rect.top;

		if( m_fRel ) {

			Rect.left   += m_Work.left;

			Rect.top    += m_Work.top;

			Rect.right  += m_Work.left;

			Rect.bottom += m_Work.top;
			}

		if( !m_Work.Encloses(Rect) ) {

			Error(CString(IDS_INVALID_POSITION_2));

			return TRUE;
			}

		if( Rect.cx() < m_Min.cx ) {

			SetDlgFocus(102);

			Error(CString(IDS_WIDTH_IS_LESS_2));

			return TRUE;
			}

		if( Rect.cy() < m_Min.cy ) {

			SetDlgFocus(102);

			Error(CString(IDS_HEIGHT_IS_LESS_2));

			return TRUE;
			}

		CRect From = m_pPrim->GetRect();

		CRect Move = CRect(Rect.GetTopLeft(), From.GetSize());

		CRect Size = Rect;

		m_pPrim->SetRect(From, Move);

		m_pPrim->SetRect(Move, Size);

		SaveConfig();

		EndDialog(TRUE);

		return TRUE;
		}

	SaveConfig();

	EndDialog(FALSE);

	return TRUE;
	}

BOOL CPrimPositionDialog::OnCommandCancel(UINT uID)
{
	SaveConfig();

	EndDialog(FALSE);

	return TRUE;
	}

BOOL CPrimPositionDialog::OnChangeRel(UINT uID)
{
	m_fRel = ((CButton &) GetDlgItem(300)).IsChecked();

	int xp = watoi(GetDlgItem(100).GetWindowText());

	int yp = watoi(GetDlgItem(101).GetWindowText());

	xp += (m_fRel ? -1 : +1) * m_Work.left;

	yp += (m_fRel ? -1 : +1) * m_Work.top;

	GetDlgItem(100).SetWindowText(CPrintf(L"%u", xp));
	
	GetDlgItem(101).SetWindowText(CPrintf(L"%u", yp));

	return TRUE;
	}

// Implementation

void CPrimPositionDialog::LoadConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"G3Graph");

	Key.MoveTo(L"Position");

	m_fRel = Key.GetValue(L"Relative", UINT(0));
	}

void CPrimPositionDialog::SaveConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"G3Graph");

	Key.MoveTo(L"Position");

	Key.SetValue(L"Relative", UINT(m_fRel));
	}

// End of File
