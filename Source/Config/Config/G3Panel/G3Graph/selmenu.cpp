
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Select Menu

BOOL CPageEditorWnd::OnSelectGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= IDM_SELECT_ONE ) {

		INDEX    Prim = m_BuryList[uID - IDM_SELECT_ONE];
		
		CPrim  *pPrim = m_pWorkList->GetItem(Prim);

		Info.m_Prompt = CPrintf(IDS_SELECT_ONE, pPrim->GetHumanName());
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnSelectControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_SELECT_ONE ) {

		Src.EnableItem(m_BuryList.GetCount() > 1);
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CPageEditorWnd::OnSelectCommand(UINT uID)
{
	if( uID >= IDM_SELECT_ONE ) {

		INDEX Prim = m_BuryList[uID - IDM_SELECT_ONE];
		
		SetSelect(Prim);

		return TRUE;
		}
	
	return FALSE;
	}

// Buried Selection

void CPageEditorWnd::BuildBuriedList(void)
{
	m_BuryList.Empty();

	INDEX n = m_pWorkList->GetHead();

	while( !m_pWorkList->Failed(n) ) {
		
		CPrim *pPrim = m_pWorkList->GetItem(n);

		if( pPrim->HitTest(m_Pos) ) {

			m_BuryList.Append(n);			
			}
		
		m_pWorkList->GetNext(n);
		}
	}

void CPageEditorWnd::AddBuriedCommands(CMenu &Menu)
{
	if( m_BuryList.GetCount() > 1 ) {
		
		CMenu Popup;

		Popup.CreatePopupMenu();

		UINT   uID  = IDM_SELECT_ONE;

		for( UINT n = 0; n < m_BuryList.GetCount(); n++ ) {

			CPrim *pPrim = m_pWorkList->GetItem(m_BuryList[n]);

			Popup.AppendMenu(0, uID++, pPrim->GetHumanName());
			}

		int nPos;

		for( nPos = 0; nPos < Menu.GetMenuItemCount(); nPos++ ) {

			UINT uID = Menu.GetMenuItemID(nPos);

			if( HIBYTE(uID) == IDM_EDIT ) {

				break;
				}
			}

		Menu.InsertMenu(nPos++, MF_BYPOSITION, Popup, CString(IDS_SELECT));

		Menu.InsertSeparator(nPos, MF_BYPOSITION);
		}
	}

// End of File
