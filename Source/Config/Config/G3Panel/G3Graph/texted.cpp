
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Textor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text Editor
//

// Runtime Class

AfxImplementRuntimeClass(CTextEditorWnd, CCtrlWnd);

// End of Line

#define EOL '|'

// Timer IDs

UINT CTextEditorWnd::m_timerCheck  = CWnd::AllocTimerID();

UINT CTextEditorWnd::m_timerDouble = CWnd::AllocTimerID();

// Constructor

CTextEditorWnd::CTextEditorWnd(CPrimText *pText, int nScale, CPoint Click, BOOL fAdd, CBitmap &Back)
{
	m_pText     = pText;

	m_nScale    = nScale;

	m_Click     = Click;

	m_fAdd      = fAdd;

	m_pBack     = &Back;

	m_pSystem   = (CUISystem *) m_pText->GetDatabase()->GetSystemItem();

	m_pLang     = m_pSystem->m_pLang;

	m_pFonts    = m_pSystem->m_pUI->m_pFonts;

	m_Text      = m_pText->m_pText->GetText();

	m_Font      = m_pText->m_Font;

	m_AlignH    = m_pText->m_AlignH;

	m_AlignV    = m_pText->m_AlignV;

	m_Lead      = m_pText->m_Lead;

	m_Margin    = m_pText->m_Margin;

	m_fSelect   = FALSE;

	m_SelStart  = CPoint(0, 0);

	m_SelEnd    = CPoint(0, 0);

	m_SelAnchor = CPoint(0, 0);

	m_uCapture  = captNone;

	m_fDouble   = FALSE;

	m_fKeyb     = FALSE;

	m_fDrop     = FALSE;
	
	m_fDropMove = TRUE;

	m_MoveCursor.Create(L"TextMove");

	m_CopyCursor.Create(L"TextCopy");

	m_Accel.Create(L"TextEditorMenu");
	}

// Destructor

CTextEditorWnd::~CTextEditorWnd(void)
{
	m_pGDI->Release();
	}

// IUnknown

HRESULT CTextEditorWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropSource ) {

			*ppObject = (IDropSource *) this;

			return S_OK;
			}

		if( iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CTextEditorWnd::AddRef(void)
{
	return 1;
	}

ULONG CTextEditorWnd::Release(void)
{
	return 1;
	}

// IDropSource

HRESULT CTextEditorWnd::QueryContinueDrag(BOOL fEscape, DWORD dwKeys)
{
	if( !fEscape ) {

		if( dwKeys & MK_RBUTTON ) {

			return DRAGDROP_S_CANCEL;
			}

		if( dwKeys & MK_LBUTTON ) {

			return S_OK;
			}

		return DRAGDROP_S_DROP;
		}

	return DRAGDROP_S_CANCEL;
	}
        
HRESULT CTextEditorWnd::GiveFeedback(DWORD dwEffect)
{
	if( dwEffect == DROPEFFECT_MOVE ) {

		SetCursor(m_MoveCursor);
		
		return S_OK;
		}

	if( dwEffect == DROPEFFECT_COPY ) {

		SetCursor(m_CopyCursor);

		return S_OK;
		}

	SetCursor(LoadCursor(NULL, IDC_NO));

	return S_OK;
	}

// IDropTarget

HRESULT CTextEditorWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( CanAcceptDataObject(pData) ) {
		
		m_fDrop     = TRUE;

		m_fDropMove = IsDropMove(dwKeys, pEffect);
		
		SetFocus();
		
		DropTrack(CPoint(pt.x, pt.y));
		
		return S_OK;
		}

	return S_OK;
	}

HRESULT CTextEditorWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_fDrop ) {

		m_fDropMove = IsDropMove(dwKeys, pEffect);

		DropTrack(CPoint(pt.x, pt.y));
		
		return S_OK;
		}
	
	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CTextEditorWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	if( m_fDrop ) {

		m_fDrop     = FALSE;
		
		return S_OK;
		}

	return S_OK;
	}

HRESULT CTextEditorWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_fDrop ) {

		DropTrack(CPoint(pt.x, pt.y));

		if( CanAcceptDataObject(pData) ) {
			
			AcceptDataObject(pData);
			}
		
		m_fDropMove = FALSE;
		
		m_fDrop     = FALSE;
		
		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Attributes

CString CTextEditorWnd::GetText(void) const
{
	return m_Text;
	}

// Operations

BOOL CTextEditorWnd::WriteBack(void)
{
	if( m_Text.IsEmpty() ) {
		
		if( m_pText->IsEmpty() ) {

			return FALSE;
			}
		}

	m_pText->m_pText->SetText(m_Text, FALSE);

	m_pText->m_Font   = m_Font;

	m_pText->m_AlignH = m_AlignH;

	m_pText->m_AlignV = m_AlignV;

	m_pText->m_Lead   = m_Lead;

	m_pText->m_Margin = m_Margin;

	return TRUE;
	}

// Accelerators

BOOL CTextEditorWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Map

AfxMessageMap(CTextEditorWnd, CCtrlWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_CREATE)	
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_SYSKEYDOWN)
	AfxDispatchMessage(WM_SYSKEYUP)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)
	AfxDispatchMessage(WM_CHAR)

	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)

	AfxDispatchMessage(WM_RBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_RBUTTONDBLCLK)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_RBUTTONUP)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxDispatchControlType(IDM_TEXT, OnTextControl)
	AfxDispatchCommandType(IDM_TEXT, OnTextCommand)

	AfxMessageEnd(CTextEditorWnd)
	};

// Message Handlers

UINT CTextEditorWnd::OnCreate(CREATESTRUCT &Create)
{
	m_WorkSize.cx = Create.cx;
	
	m_WorkSize.cy = Create.cy;

	m_TextSize.cx = m_WorkSize.cx / m_nScale;

	m_TextSize.cy = m_WorkSize.cy / m_nScale;

	return 0;
	}

void CTextEditorWnd::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
	
	CClientDC DC(ThisObject);

	m_pWin = Create_GdiWindowsA888(m_TextSize.cx, m_TextSize.cy);

	m_pGDI = m_pWin->GetGdi();

	m_pGDI  ->SetTextSmooth(0);

	m_pFonts->Select(m_pGDI, m_Font);

	m_yLine = m_Lead + m_pGDI->GetTextHeight(L"X");

	ReflowAll();

	SyncFrom2D();

	if( m_Click.x >= 0 && m_Click.y >= 0 ) {

		FakeMouse(m_Click);
		}
	else {
		if( m_fAdd ) {

			OnEditSelectAll();
			}
		}

	UpdateImage();
	}

void CTextEditorWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_pLang->SetKeyboard() ) {

		m_fKeyb = TRUE;
		}

	FindCaretSize();

	CreateCaret(m_hWnd, NULL, m_CaretSize.cx, m_CaretSize.cy);

	MoveCaret();

	ShowCaret(m_hWnd);
	}

void CTextEditorWnd::OnKillFocus(CWnd &Wnd)
{
	if( m_fKeyb ) {

		m_pLang->ClearKeyboard();

		m_fKeyb = FALSE;
		}

	HideCaret(m_hWnd);

	DestroyCaret();
	}

void CTextEditorWnd::OnSysKeyDown(UINT uCode, DWORD dwFlags)
{
	// LATER -- We get a beep here. Don't know why...

	switch( uCode ) {

		case VK_RETURN:

			OnTextProperties();

			return;
		}

	AfxCallDefProc();
	}

void CTextEditorWnd::OnSysKeyUp(UINT uCode, DWORD dwFlags)
{
	switch( uCode ) {

		case VK_RETURN:

			return;
		}

	AfxCallDefProc();
	}

void CTextEditorWnd::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	OnKeyChange(uCode, dwFlags);

	if( IsMoveKey(uCode) ) {

		if( IsDown(VK_SHIFT) ) {

			StartSelect();
			}

		CPoint Pos = m_Pos;

		if( PerformMove(Pos, uCode) ) {

			if( m_Pos != Pos ) {

				m_Pos = Pos;

				SyncFrom2D();

				MoveCaret();

				CheckSelect();

				return;
				}
			}
		}

	switch( uCode ) {

		case VK_F2:

			SendParent(VK_RETURN);

			break;

		case VK_RETURN:

			if( !IsDown(VK_SHIFT) ) {

				SendParent(VK_RETURN);

				return;
				}
			break;

		case VK_DELETE:

			HideCaret(m_hWnd);

			if( DeleteSelect() || DeleteChar() ) {

				UpdateAll();
				}

			ShowCaret(m_hWnd);

			break;

		case VK_SPACE:

			if( IsDown(VK_CONTROL) ) {

				if( IsDown(VK_SHIFT) ) {

					OnChar(spaceNoBreak, dwFlags);
					}
				}

			break;
		}
	}

void CTextEditorWnd::OnKeyUp(UINT uCode, DWORD dwFlags)
{
	switch( uCode ) {

		case VK_RETURN:

			if( !IsDown(VK_SHIFT) ) {

				return;
				}
			break;
		}

	OnKeyChange(uCode, dwFlags);
	}

void CTextEditorWnd::OnKeyChange(UINT uCode, DWORD dwFlags)
{
	}

void CTextEditorWnd::OnChar(UINT uCode, DWORD dwFlags)
{
	switch( uCode ) {

		case 0x1B:

			if( m_fSelect ) {

				ClearSelect(TRUE);
				}
			else
				SendParent(VK_ESCAPE);

			break;

		case 0x08:

			HideCaret(m_hWnd);

			if( DeleteSelect() || Backspace() ) {

				UpdateAll();
				}

			ShowCaret(m_hWnd);

			break;

		case 0x0D:

			if( !IsDown(VK_SHIFT) ) {

				return;
				}

			uCode = EOL;

			break;
		}

	if( uCode >= 0x20 ) {

		if( UniIsComplex(TCHAR(uCode)) || DoesNotFit() ) {

			MessageBeep(0);
			}
		else {
			HideCaret(m_hWnd);

			DeleteSelect();

			EnterChar(TCHAR(uCode));

			UpdateAll();

			ShowCaret(m_hWnd);
			}
		}
	}

void CTextEditorWnd::OnPaint(void)
{
	CPaintDC  DrawDC(ThisObject);

	CMemoryDC WorkDC(DrawDC);

	CMemoryDC BackDC(DrawDC);

	CBitmap   Bitmap(DrawDC, m_TextSize);

	WorkDC.SetBkColor(RGB(255,0x00,255)); // !!!!

	WorkDC.Select(Bitmap);

	BackDC.Select(*m_pBack);

	WorkDC.BitBlt( CRect(m_TextSize),
		       BackDC, 
		       CPoint(0, 0),
		       SRCCOPY
		       );

	WorkDC.TransBlt( CRect(m_TextSize),
			 CDC::FromHandle(m_pWin->GetDC()),
			 CPoint(0, 0)
			 );

	if( m_fSelect ) {

		for( int r = m_SelStart.y; r <= m_SelEnd.y; r++ ) {

			int x1, x2, y1, y2;

			if( r == m_SelStart.y ) {

				x1 = FindPosFromChar(r, m_SelStart.x);
				}
			else
				x1 = FindPosFromChar(r, 0);

			if( r == m_SelEnd.y ) {

				x2 = FindPosFromChar(r, m_SelEnd.x);
				}
			else
				x2 = FindPosFromChar(r, m_Lines[r].GetCount());

			y1 = m_Inits[r].y - 1;

			y2 = y1 + m_yLine;

			WorkDC.PatBlt(x1, y1, x2 - x1, y2 - y1, PATINVERT);
			}
		}

	DrawDC.StretchBlt( CRect(m_WorkSize),
			   WorkDC,
			   CRect(m_TextSize),
			   SRCCOPY
			   );
	
	BackDC.Deselect();

	WorkDC.Deselect();
	}

BOOL CTextEditorWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CTextEditorWnd::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	if( MouseToText(Pos) ) {

		if( m_Pos != Pos ) {

			m_Pos = Pos;

			SyncFrom2D();

			MoveCaret();

			return;
			}
		}
	}

void CTextEditorWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( MouseToText(Pos) ) {

		if( m_Pos != Pos ) {

			m_Pos = Pos;

			SyncFrom2D();
			
			MoveCaret();
			}

		if( !HitTestSelect() ) {
	
			ClearSelect(TRUE);
			}

		if( !DefaultStart() ) {
			
			m_uCapture = captPend;

			SetCapture();
			}

		return;
		}

	ClearSelect(TRUE);		
	}

void CTextEditorWnd::OnRButtonDblClk(UINT uFlags, CPoint Pos)
{
	}

void CTextEditorWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( MouseToText(Pos) ) {

		SelectWord();

		m_fDouble = TRUE;

		SetTimer(m_timerDouble, 300);
		}
	}

void CTextEditorWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	m_uCapture = captNone;

	ReleaseCapture();
	}

void CTextEditorWnd::OnRButtonUp(UINT uFlags, CPoint Pos)
{
	CMenu Load = CMenu(L"TextEditCtx");

	if( Load.IsValid() ) {

		CMenu &Menu = Load.GetSubMenu(0);

		Menu.SendInitMessage();

		Menu.DeleteDisabled();

		Menu.MakeOwnerDraw(FALSE);
		
		Menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
				     GetCursorPos(),
				     afxMainWnd->GetHandle()
				     );

		Menu.FreeOwnerDraw();
		}
	}

void CTextEditorWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( MouseToText(Pos) ) {

		if( m_uCapture ) {
			
			m_Pos = Pos;

			SyncFrom2D();

			MoveCaret();
			}
			
		if( m_uCapture == captPend ) {

			if( DragStart() ) {

				m_uCapture = captNone;

				ReleaseCapture();
				}
			else {
				if( !SelectStart() ) {

					m_uCapture = captNone;

					ReleaseCapture();
					}
				else {
					DefaultUpdate();
					}
				}

			return;
			}

		if( m_uCapture == captSelect ) {

			SelectUpdate();
			
			return;
			}
		}
	}

BOOL CTextEditorWnd::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( HitTestSelect() ) {
		
		SetCursor(CCursor(IDC_ARROW));
		}
	else
		SetCursor(CCursor(IDC_IBEAM));

	return TRUE;
	}

void CTextEditorWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerCheck ) {

		if( m_fDropMove ) {
			
			SetCursor(m_MoveCursor);
			}
		else
			SetCursor(m_CopyCursor);			
		
		KillTimer(uID);
		}

	if( uID == m_timerDouble ) {
		
		m_fDouble = FALSE;
		
		KillTimer(uID);
		}
	}

// Edit Menu

BOOL CTextEditorWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_SELECT_ALL:
			
			Src.EnableItem(TRUE);
			
			return TRUE;

		case IDM_EDIT_DELETE:
		case IDM_EDIT_CUT:
		case IDM_EDIT_COPY:

			Src.EnableItem(m_fSelect);

			return TRUE;

		case IDM_EDIT_PASTE:
			
			Src.EnableItem(TRUE);
			
			return TRUE;

		default:
			return FALSE;
		}
	}

BOOL CTextEditorWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_SELECT_ALL:
			
			OnEditSelectAll();
			
			return TRUE;

		case IDM_EDIT_DELETE:

			OnEditDelete(CString(IDS_DELETE));
			
			return TRUE;

		case IDM_EDIT_CUT:

			OnEditCut();

			return TRUE;

		case IDM_EDIT_COPY:

			OnEditCopy();

			return TRUE;

		case IDM_EDIT_PASTE:

			OnEditPaste(CString(IDS_PASTE));

			return TRUE;

		default:
			return FALSE;
		}	
	}

BOOL CTextEditorWnd::CanEditPaste(void)
{
	return TRUE;
	}

BOOL CTextEditorWnd::CanEditDelete(void)
{
	return TRUE;
	}

void CTextEditorWnd::OnEditDelete(CString Verb)
{
	if( DeleteSelect() ) {
		
		UpdateAll();
		}
	}

void CTextEditorWnd::OnEditCut(void)
{
	OnEditCopy();

	OnEditDelete(CString(IDS_CUT));
	}

void CTextEditorWnd::OnEditCopy(void)
{
	if( m_fSelect ) {

		IDataObject *pData = NULL;

		MakeDataObject(pData);

		OleSetClipboard(pData);
		
		OleFlushClipboard();

		pData->Release();
		}
	}

void CTextEditorWnd::OnEditPaste(CString Verb)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		if( CanAcceptDataObject(pData) ) {
			
			AcceptDataObject(pData);
			}
		}
	}

void CTextEditorWnd::OnEditSelectAll(void)
{
	AfxAssert(m_Lines.GetCount());

	SelectText();
	}

// Text Menu

BOOL CTextEditorWnd::OnTextControl(UINT uID, CCmdSource &Src)
{
	BOOL f1, f2;

	switch( uID ) {

		case IDM_TEXT_ALIGN_HL:
		case IDM_TEXT_ALIGN_HC:
		case IDM_TEXT_ALIGN_HR:
		
			Src.EnableItem(TRUE);

			Src.CheckItem (m_AlignH == int(uID - IDM_TEXT_ALIGN_HL));

			break;

		case IDM_TEXT_ALIGN_VT:
		case IDM_TEXT_ALIGN_VU:
		case IDM_TEXT_ALIGN_VM:
		case IDM_TEXT_ALIGN_VL:
		case IDM_TEXT_ALIGN_VB:
		
			Src.EnableItem(TRUE);

			Src.CheckItem (m_AlignV == int(uID - IDM_TEXT_ALIGN_VT));

			break;

		case IDM_TEXT_SPACE_MORE:

			Src.EnableItem(m_Lead < 20);

			break;

		case IDM_TEXT_SPACE_LESS:

			Src.EnableItem(m_Lead > 0);

			break;

		case IDM_TEXT_LARGER:

			Src.EnableItem(m_pFonts->CanMakeLarger(m_Font));

			break;

		case IDM_TEXT_SMALLER:

			Src.EnableItem(m_pFonts->CanMakeSmaller(m_Font));

			break;

		case IDM_TEXT_BOLD:

			Src.EnableItem(m_pFonts->CanFlipBold(m_Font));

			Src.CheckItem (m_pFonts->IsBold(m_Font));

		case IDM_TEXT_INC_MARG_H:

			f1 = m_Margin.left  < 80;
			f2 = m_Margin.right < 80;

			Src.EnableItem(f1 || f2);

			break;

		case IDM_TEXT_DEC_MARG_H:

			f1 = m_Margin.left  > 0;
			f2 = m_Margin.right > 0;

			Src.EnableItem(f1 || f2);

			break;

		case IDM_TEXT_INC_MARG_V:

			f1 = m_Margin.top    < 80;
			f2 = m_Margin.bottom < 80;

			Src.EnableItem(f1 || f2);

			break;

		case IDM_TEXT_DEC_MARG_V:

			f1 = m_Margin.top    > 0;
			f2 = m_Margin.bottom > 0;

			Src.EnableItem(f1 || f2);

			break;

		case IDM_TEXT_PROPERTIES:

			Src.EnableItem(TRUE);

			break;
		}

	return TRUE;
	}

BOOL CTextEditorWnd::OnTextCommand(UINT uID)
{
	switch( uID ) {

		case IDM_TEXT_ALIGN_HL:
		case IDM_TEXT_ALIGN_HC:
		case IDM_TEXT_ALIGN_HR:
		
			m_AlignH = int(uID - IDM_TEXT_ALIGN_HL);

			UpdateAll();

			break;

		case IDM_TEXT_ALIGN_VT:
		case IDM_TEXT_ALIGN_VL:
		case IDM_TEXT_ALIGN_VM:
		case IDM_TEXT_ALIGN_VU:
		case IDM_TEXT_ALIGN_VB:
		
			m_AlignV = int(uID - IDM_TEXT_ALIGN_VT);

			UpdateAll();

			break;

		case IDM_TEXT_SPACE_MORE:

			m_Lead++;

			UpdateAll();

			break;

		case IDM_TEXT_SPACE_LESS:

			m_Lead--;

			UpdateAll();

			break;

		case IDM_TEXT_LARGER:

			m_Font = m_pFonts->GetLarger(m_Font);

			UpdateFont();

			break;

		case IDM_TEXT_SMALLER:

			m_Font = m_pFonts->GetSmaller(m_Font);

			UpdateFont();

			break;

		case IDM_TEXT_BOLD:

			m_Font = m_pFonts->FlipBold(m_Font);

			UpdateFont();

			break;

		case IDM_TEXT_INC_MARG_H:

			m_Margin.left  = min(m_Margin.left  +1, 80);
			m_Margin.right = min(m_Margin.right +1, 80);

			UpdateAll();

			break;

		case IDM_TEXT_DEC_MARG_H:

			m_Margin.left  = max(m_Margin.left  -1, 0);
			m_Margin.right = max(m_Margin.right -1, 0);

			UpdateAll();

			break;

		case IDM_TEXT_INC_MARG_V:

			m_Margin.top    = min(m_Margin.top   +1, 80);
			m_Margin.bottom = min(m_Margin.bottom+1, 80);

			UpdateAll();

			break;

		case IDM_TEXT_DEC_MARG_V:

			m_Margin.top    = max(m_Margin.top   -1, 0);
			m_Margin.bottom = max(m_Margin.bottom-1, 0);

			UpdateAll();

			break;

		case IDM_TEXT_PROPERTIES:

			OnTextProperties();

			break;
		}

	return TRUE;
	}

void CTextEditorWnd::OnTextProperties(void)
{
	CWnd &Wnd = GetParent();

	SendParent(VK_RETURN);

	Wnd.PostMessage(WM_AFX_COMMAND, IDM_TEXT_PROPERTIES);
	}

void CTextEditorWnd::SendParent(UINT uCode)
{
	CWnd &Wnd = GetParent();

	Wnd.PostMessage(WM_KEYDOWN, uCode);

	Wnd.PostMessage(WM_KEYUP,   uCode);
	}

// Implementation

BOOL CTextEditorWnd::IsMoveKey(UINT uCode)
{
	switch( uCode ) {

		case VK_LEFT:
		case VK_RIGHT:
		case VK_UP:
		case VK_DOWN:
		case VK_HOME:
		case VK_END:
		case VK_PRIOR:
		case VK_NEXT:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CTextEditorWnd::PerformMove(CPoint &Pos, UINT uCode)
{
	if( !IsDown(VK_SHIFT) && ClearSelect(FALSE) ) {

		switch( uCode ) {

			case VK_LEFT:

				if( !IsDown(VK_CONTROL) ) {

					if( Pos.x ) {
					
						Pos = m_SelStart;
						
						return TRUE;
						}
					}
				break;

			case VK_RIGHT:

				if( !IsDown(VK_CONTROL) ) {

					if( Pos.x < m_nSize ) {

						Pos = m_SelEnd;

						return TRUE;
						}
					}
				break;
			}
		}


	switch( uCode ) {

		case VK_LEFT:

			if( IsDown(VK_CONTROL) ) {

				MovePrevWordEnd(Pos);

				MoveThisWordStart(Pos);
				}
			else {
				if( Pos.x ) {

					Pos.x--;
					}
				else {
					if( Pos.y ) {

						Pos.y = Pos.y - 1;

						Pos.x = int(m_Lines[Pos.y].GetCount());
						}
					}
				}

			return TRUE;

		case VK_RIGHT:

			if( IsDown(VK_CONTROL) ) {

				MoveThisWordEnd(Pos);

				MoveNextWordStart(Pos);
				}
			else {
				if( Pos.x < m_nSize ) {

					Pos.x++;
					}
				else {
					if( Pos.y < m_nShown - 1 ) {

						Pos.y = Pos.y + 1;

						Pos.x = 0;
						}
					}
				}

			return TRUE;

		case VK_UP:

			if( Pos.y ) {

				Pos.y = Pos.y - 1;

				Pos.x = FindCharFromPos(Pos.y, m_CaretPos.x + 2);
				}
 
			return TRUE;

		case VK_DOWN:

			if( Pos.y < m_nShown - 1 ) {

				Pos.y = Pos.y + 1;

				Pos.x = FindCharFromPos(Pos.y, m_CaretPos.x + 2);
				}

			return TRUE;

		case VK_PRIOR:

			if( IsDown(VK_CONTROL) ) {

				Pos.x = 0;

				Pos.y = 0;
				}
			else {
				Pos.y = 0;

				Pos.x = FindCharFromPos(Pos.y, m_CaretPos.x + 2);
				}

			return TRUE;

		case VK_NEXT:

			if( IsDown(VK_CONTROL) ) {

				Pos.y = m_nShown - 1;

				Pos.x = int(m_Lines[Pos.y].GetCount());
				}
			else {
				Pos.y = m_nShown - 1;

				Pos.x = FindCharFromPos(Pos.y, m_CaretPos.x + 2);
				}

			return TRUE;

		case VK_HOME:

			if( IsDown(VK_CONTROL) ) {

				Pos.x = 0;

				Pos.y = 0;
				}
			else
				Pos.x = 0;

			return TRUE;

		case VK_END:

			if( IsDown(VK_CONTROL) ) {

				Pos.y = m_nShown - 1;

				Pos.x = int(m_Lines[Pos.y].GetCount());
				}
			else
				Pos.x = m_nSize;

			return TRUE;
		}

	return FALSE;
	}

BOOL CTextEditorWnd::IsBreak(WCHAR cData)
{
	switch( cData ) {

		case 0x0020:
		case 0x3001:
		case 0x3002:
		case 0xFF0C:
		case 0xFF0E:
		case 0xFF1A:
		case 0xFF1B:

			return TRUE;
		}

	return FALSE;
	}

void CTextEditorWnd::MoveThisWordStart(CPoint &Pos)
{
	if( Pos.x ) {

		while( Pos.x && !IsBreak(m_Text[CalcFrom2D(Pos) - 1]) ) {
			
			Pos.x--;
			}
		}
	else {
		if( Pos.y ) {

			Pos.y = Pos.y - 1;

			Pos.x = int(m_Lines[Pos.y].GetCount());
			}

		if( Pos.x ) {

			MoveThisWordStart(Pos);
			}
		}
	}

void CTextEditorWnd::MoveThisWordEnd(CPoint &Pos)
{
	while( Pos.x < m_nSize && !IsBreak(m_Text[CalcFrom2D(Pos)]) ) {
		
		Pos.x++;
		}
	}

void CTextEditorWnd::MoveNextWordStart(CPoint &Pos)
{
	if( Pos.x < m_nSize ) {

		while( Pos.x < m_nSize && IsBreak(m_Text[CalcFrom2D(Pos)]) ) {
			
			Pos.x++;
			}
		}
	else {
		if( Pos.y < m_nShown - 1 ) {

			Pos.y = Pos.y + 1;

			Pos.x = 0;
			}

		if( Pos.x < m_nSize ) {

			MoveNextWordStart(Pos);
			}
		}
	}

void CTextEditorWnd::MovePrevWordEnd(CPoint &Pos)
{
	while( Pos.x && IsBreak(m_Text[CalcFrom2D(Pos) - 1]) ) {
		
		Pos.x--;
		}
	}

BOOL CTextEditorWnd::EnterChar(TCHAR cData)
{
	m_Text.Insert(m_nPos, cData);

	m_nPos  += 1;

	m_nTo   += 1;

	m_nSize += 1;

	if( cData == EOL ) {

		if( !ReflowFromHere() ) {

			AbortChar();

			ReflowFromHere();

			return FALSE;
			}
		}
	else {
		if( IsBreak(cData) ) {

			if( m_nPos < m_nTo ) {

				if( m_nFrom > 0 && m_Text[m_nFrom - 1] != EOL ) {

					int i;

					for( i = m_nFrom; i < m_nPos - 1; i++ ) {

						if( IsBreak(m_Text[i]) ) {

							break;
							}
						}

					if( i == m_nPos - 1 ) {

						if( ReflowFromHere() ) {

							SyncFrom1D();

							return TRUE;
							}
						else {
							AbortChar();

							ReflowFromHere();

							return FALSE;
							}
						}
					}
				}
			}

		int xLine = GetLineWidth();
	
		int xChar = GetCharWidth(cData);

		if( xLine + xChar > m_TextSize.cx ) {

			if( !ReflowFromHere() ) {

				AbortChar();

				ReflowFromHere();

				return FALSE;
				}
			}
		else {
			SyncRange();

			AdjustFromHere(+1);
			}
		}

	SyncFrom1D();

	return TRUE;
	}

void CTextEditorWnd::AbortChar(void)
{
	m_nSize -= 1;

	m_nTo   -= 1;

	m_nPos  -= 1;

	m_Text.Delete(m_nPos, 1);

	MessageBeep(0);
	}

void CTextEditorWnd::EnterText(CString Text)
{
	if( UniIsComplex(Text) || DoesNotFit() ) {

		MessageBeep(0);
		}
	else {
		UINT uSize = Text.GetLength();

		for( UINT n = 0; n < uSize; n ++ ) {

			UINT uCode = Text.GetAt(n);
			
			if( uCode == 0x0D ) {

				uCode = EOL;
				}

			if( uCode >= 0x20 ) {

				if( !EnterChar(TCHAR(uCode)) ) {
					
					break;
					}
				}
			}
		}
	}

BOOL CTextEditorWnd::Backspace(void)
{
	if( m_nPos ) {

		m_Text.Delete(m_nPos - 1, 1);

		m_nPos -= 1;

		if( !m_Pos.x || !m_fHard ) {

			ReflowFromHere();
			}
		else {
			m_nTo   -= 1;

			m_nSize -= 1;

			SyncRange();

			AdjustFromHere(-1);
			}

		SyncFrom1D();

		return TRUE;
		}

	return FALSE;
	}

BOOL CTextEditorWnd::DeleteChar(void)
{
	if( m_nPos < int(m_Text.GetLength()) ) {

		m_Text.Delete(m_nPos, 1);

		m_nSize -= 1;

		m_nTo   -= 1;

		if( m_Text[m_nTo] != EOL ) {

			ReflowFromHere();
			}
		else {
			SyncRange();

			AdjustFromHere(-1);
			}

		SyncFrom1D();

		return TRUE;
		}

	return FALSE;
	}

BOOL CTextEditorWnd::DeleteSelect(void)
{
	if( m_fSelect )	{

		int nFrom = CalcFrom2D(m_SelStart);

		int nTo   = CalcFrom2D(m_SelEnd);

		int nSize = nTo - nFrom;

		if( m_nPos >= nTo ) {

			m_nPos -= nSize;
			}

		m_Text.Delete(nFrom, nSize);

		m_nSize -= nSize;

		m_nTo   -= nSize;

		if( m_Text[m_nTo] != EOL ) {

			ReflowFromHere();
			}
		else {
			SyncRange();

			AdjustFromHere(-nSize);
			}

		SyncFrom1D();

		m_fSelect = FALSE;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CTextEditorWnd::ReflowFromHere(void)
{
	return ReflowAll();
	}

BOOL CTextEditorWnd::ReflowAll(void)
{
	m_pFonts->Select(m_pGDI, m_Font);

	m_Lines.Empty();

	m_Inits.Empty();

	PCTXT pText  = m_Text;

	int   nPos   = 0;

	int   xLimit = m_TextSize.cx - m_Margin.left - m_Margin.right;

	while( pText[nPos] ) {

		CRange Range;

		Range.m_nFrom = nPos;

		int xPos = 0;

		for(;;) {

			TCHAR cData = pText[nPos];

			if( cData ) {

				if( cData == EOL ) {

					Range.m_nTo = nPos++;

					break;
					}
				else {
					int xSize = m_pGDI->GetTextWidth(CString(cData, 1));

					if( xPos + xSize > xLimit ) {

						if( cData == 0x20 ) {

							Range.m_nTo = nPos;

							nPos++;
							}
						else {
							for( int s = nPos; s > Range.m_nFrom; s-- ) {

								if( IsBreak(pText[s]) ) {

									nPos = s + 1;

									break;
									}
								}

							Range.m_nTo = nPos;
							}

						break;
						}

					xPos += xSize;

					nPos += 1;

					continue;
					}
				}

			Range.m_nTo = nPos;

			break;
			}

		if( nPos == Range.m_nFrom ) {

			break;
			}

		if( !(pText[nPos] == 0x00 || pText[nPos] == 0x20) ) {

			while( Range.m_nTo > Range.m_nFrom ) {

				if( pText[Range.m_nTo - 1] == 0x20 ) {

					Range.m_nTo--;

					continue;
					}

				break;
				}
			}

		m_Lines.Append(Range);

		m_Inits.Append(CPoint());
		}

	if( pText[nPos-1] == EOL ) {

		m_Lines.Append(CRange(nPos, nPos));

		m_Inits.Append(CPoint());
		}

	if( m_Lines.IsEmpty() ) {

		m_Lines.Append(CRange(0,0));

		m_Inits.Append(CPoint());
		}

	m_nLines = m_Lines.GetCount();

	return !DoesNotFit();
	}

BOOL CTextEditorWnd::DoesNotFit(void)
{
	int yTotal = m_Lines.GetCount() * m_yLine;
	
	int ySpace = m_TextSize.cy - m_Margin.top - m_Margin.bottom;

	return yTotal > ySpace;
	}

void CTextEditorWnd::UpdateFont(void)
{
	m_pFonts->Select(m_pGDI, m_Font);

	m_yLine = m_Lead + m_pGDI->GetTextHeight(L"X");

	RemakeCaret();

	ReflowAll();

	ClearSelect(FALSE);

	SyncFrom1D();

	UpdateAll();
	}

void CTextEditorWnd::UpdateAll(void)
{
	UpdateImage();

	Invalidate(FALSE);

	MoveCaret();
	}

void CTextEditorWnd::UpdateImage(void)
{
	// LATER -- Can we factor out this code and that in CPrimText?

	m_pGDI->ClearScreen (GetRGB(31,0,31));

	m_pGDI->SetTextTrans(modeTransparent);

	COLOR Color  = m_pText->m_pColor->GetColor();

	COLOR Shadow = m_pText->m_pShadow->GetColor();

	PCTXT pText  = m_Text;

	UINT  uCount = m_Lines.GetCount();

	int   nHalf  = m_Lead / 2;

	int   ySize  = uCount * m_yLine;

	int   xOrg   = m_Margin.left;

	int   yOrg   = m_Margin.top;

	int   xLimit = m_TextSize.cx - m_Margin.left - m_Margin.right;

	int   yLimit = m_TextSize.cy - m_Margin.top - m_Margin.bottom;

	int   yPos   = nHalf + max(0, (m_AlignV * (yLimit - ySize)) / 4);

	UINT  n;

	for( n = 0; n < uCount; n++ ) {

		if( yPos + m_yLine - nHalf <= m_TextSize.cy ) {

			CRange const &Range = m_Lines[n];

			if( Range.m_nTo > Range.m_nFrom ) {

				UINT    uLen  = Range.m_nTo - Range.m_nFrom;

				PCTXT   pLine = pText + Range.m_nFrom;

				CString Line  = CString(pLine, uLen);

				int     xSize = m_pGDI->GetTextWidth(Line);

				int     xPos  = (m_AlignH * (xLimit - xSize)) / 2;

				if( !(Shadow & 0x8000) ) {

					m_pGDI->SetTextFore(Shadow);

					m_pGDI->TextOut(xOrg + xPos + 1, yOrg + yPos + 1, Line);
					}

				m_pGDI->SetTextFore(Color);

				m_pGDI->TextOut(xOrg + xPos, yOrg + yPos, Line);

				m_Inits.SetAt(n, CPoint(xOrg + xPos, yOrg + yPos));
				}
			else {
				int xPos = m_AlignH * xLimit / 2;

				m_Inits.SetAt(n, CPoint(xOrg + xPos, yOrg + yPos));
				}

			yPos += m_yLine;

			continue;
			}

		break;
		}

	// TODO -- Should this be calculated here?

	m_nShown = n;
	}

void CTextEditorWnd::GetLineInfo(void)
{
	GetLineInfo(m_Pos.y);
	}

void CTextEditorWnd::GetLineInfo(int nLine)
{
	CRange const &Range = m_Lines[nLine];

	m_nFrom = Range.m_nFrom;

	m_nTo   = Range.m_nTo;

	m_nSize = Range.m_nTo - Range.m_nFrom;

	m_fHard = (m_Text[m_nTo] == EOL);
	}

void CTextEditorWnd::SyncFrom2D(void)
{
	GetLineInfo();

	MakeMin(m_Pos.x, m_nSize);

	MakeMax(m_Pos.x, 0);

	m_nPos = m_nFrom + m_Pos.x;
	}

int CTextEditorWnd::CalcFrom2D(CPoint Pos)
{
	GetLineInfo(Pos.y);

	MakeMin(Pos.x, m_nSize);

	MakeMax(Pos.x, 0);

	return m_nFrom + Pos.x;
	}

void CTextEditorWnd::SyncFrom1D(void)
{
	if( m_Pos.y > m_nLines - 1 ) {

		m_Pos.y = m_nLines - 1;
		}

	GetLineInfo();

	if( m_nPos < m_nFrom ) {

		while( m_nPos < m_nFrom ) {

			GetLineInfo(--m_Pos.y);
			}
		}
	else {
		while( m_Pos.y + 1 < m_nLines ) {

			int nNext = m_Lines[m_Pos.y + 1].m_nFrom;

			if( m_nPos < nNext ) {

				break;
				}

			GetLineInfo(++m_Pos.y);
			}
		}

	m_Pos.x = m_nPos - m_nFrom;

	MakeMin(m_Pos.x, m_nSize);

	MakeMax(m_Pos.x, 0);
	}

void CTextEditorWnd::SyncRange(void)
{
	m_Lines.SetAt(m_Pos.y, CRange(m_nFrom, m_nTo));
	}

void CTextEditorWnd::AdjustFromHere(int nDelta)
{
	for( int n = m_Pos.y + 1; n < m_nLines; n++ ) {

		CRange Range = m_Lines[n];

		Range.m_nFrom += nDelta;

		Range.m_nTo   += nDelta;

		m_Lines.SetAt(n, Range);
		}
	}

void CTextEditorWnd::FindCaretSize(void)
{
	m_CaretSize.cy = m_nScale * m_yLine;

	if( m_nScale == 1 && m_CaretSize.cy >= 16 ) {

		m_CaretSize.cx = 2;

		return;
		}

	m_CaretSize.cx = m_nScale * 1;
	}

void CTextEditorWnd::MoveCaret(void)
{
	m_CaretPos.x = m_Inits[m_Pos.y].x;

	m_CaretPos.y = m_Inits[m_Pos.y].y;

	if( m_Pos.x ) {

		PCTXT   pText = m_Text;

		PCTXT   pLine = pText + m_nFrom;

		CString Line  = CString(pLine, m_Pos.x);

		m_CaretPos.x += m_pGDI->GetTextWidth(Line);
		}

	m_CaretPos.x  *= m_nScale;

	m_CaretPos.x  -= 1;

	m_CaretPos.y  -= 1;

	m_CaretPos.y  *= m_nScale;

	MakeMax(m_CaretPos.x, 0);

	MakeMin(m_CaretPos.x, m_WorkSize.cx - 1);

	SetCaretPos(m_CaretPos.x, m_CaretPos.y);
	}

void CTextEditorWnd::RemakeCaret(void)
{
	if( HasFocus() ) {

		HideCaret(m_hWnd);

		DestroyCaret();

		FindCaretSize();

		CreateCaret(m_hWnd, NULL, m_CaretSize.cx, m_CaretSize.cy);

		MoveCaret();

		ShowCaret(m_hWnd);
		}
	}

int CTextEditorWnd::GetLineWidth(void)
{
	PCTXT   pText = m_Text;

	PCTXT   pLine = pText + m_nFrom;

	CString Line  = CString(pLine, m_nSize);

	return m_pGDI->GetTextWidth(Line);
	}

int CTextEditorWnd::GetCharWidth(TCHAR cData)
{
	return m_pGDI->GetTextWidth(CString(cData, 1));
	}

BOOL CTextEditorWnd::MouseToText(CPoint &Pos)
{
	Pos.y /= m_nScale;

	Pos.x += GetCharWidth(0x20) / 2;

	int r;

	for( r = 0; r < m_nShown; r++ ) {

		int yInit = m_Inits[r].y;

		if( Pos.y >= yInit && Pos.y < yInit + m_yLine ) {

			Pos.y = r;

			Pos.x = FindCharFromPos(r, Pos.x);

			return TRUE;
			}
		}

	if( Pos.y > m_Inits[r - 1].y ) {

		int nFrom = m_Lines[r - 1].m_nFrom;

		int nTo   = m_Lines[r - 1].m_nTo;

		Pos.x = nTo - nFrom;

		Pos.y = r - 1;
		}
	else {
		Pos.x = 0;

		Pos.y = 0;
		}

	return FALSE;
	}

int CTextEditorWnd::FindCharFromPos(int nLine, int xPos)
{
	int xInit = m_Inits[nLine].x;

	xPos      = xPos / m_nScale;

	if( xPos >= xInit ) {

		int   nFrom = m_Lines[nLine].m_nFrom;

		int   nTo   = m_Lines[nLine].m_nTo;

		PCTXT pText = PCTXT(m_Text) + nFrom;

		int   nSize = nTo - nFrom;

		int   c;

		for( c = 0; c < nSize; c++ ) {

			int xChar = GetCharWidth(pText[c]);

			if( xPos >= xInit && xPos < xInit + xChar ) {

				break;
				}

			xInit += xChar;
			}

		return c;
		}

	return 0;
	}

int CTextEditorWnd::FindPosFromChar(int nLine, int xChar)
{
	int     xInit = m_Inits[nLine].x;

	int     nFrom = m_Lines[nLine].m_nFrom;

	PCTXT   pText = PCTXT(m_Text) + nFrom;

	CString Line  = CString(pText, xChar);

	xInit += m_pGDI->GetTextWidth(Line);

	return xInit;
	}

void CTextEditorWnd::FakeMouse(CPoint Pos)
{
	PostMessage( WM_LBUTTONDOWN,
		     0,
		     LPARAM(Pos)
		     );

	PostMessage( WM_LBUTTONUP,
		     0,
		     LPARAM(Pos)
		     );
	}

BOOL CTextEditorWnd::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000) ? TRUE : FALSE;
	}

void CTextEditorWnd::StartSelect(void)
{
	if( !m_fSelect ) {
		
		m_SelStart  = m_Pos;
		
		m_SelEnd    = m_Pos;
		
		m_SelAnchor = m_Pos;
		
		m_fSelect   = TRUE;
		}
	}

BOOL CTextEditorWnd::ClearSelect(BOOL fUpdate)
{
	if( m_fSelect ) {

		m_fSelect = FALSE;

		Invalidate(fUpdate);

		return TRUE;
		}

	return FALSE;
	}

void CTextEditorWnd::CheckSelect(void)
{
	if( m_fSelect ) {
		
		if( m_Pos.y <= m_SelAnchor.y ) {
			
			m_SelStart = m_Pos;
			
			m_SelEnd   = m_SelAnchor;
			}
		else
			m_SelEnd   = m_Pos;

		if( m_SelEnd.y == m_SelStart.y ) {

			if( m_SelEnd.x < m_SelStart.x ) {

				int nPos = m_SelEnd.x;

				m_SelEnd.x   = m_SelStart.x;

				m_SelStart.x = nPos;				
				}			
			}

		Invalidate(FALSE);
		}
	}

BOOL CTextEditorWnd::HitTestSelect(CPoint Pos)
{
	if( m_fSelect ) {

		for( int r = m_SelStart.y; r <= m_SelEnd.y; r++ ) {

			int x1, x2, y1, y2;

			if( r == m_SelStart.y ) {

				x1 = FindPosFromChar(r, m_SelStart.x);
				}
			else
				x1 = FindPosFromChar(r, 0);

			if( r == m_SelEnd.y ) {

				x2 = FindPosFromChar(r, m_SelEnd.x);
				}
			else
				x2 = FindPosFromChar(r, m_Lines[r].GetCount());

			y1 = m_Inits[r].y;

			y2 = y1 + m_yLine;

			x1 *= m_nScale;

			x2 *= m_nScale;

			y1 *= m_nScale;

			y2 *= m_nScale;

			if( CRect(x1, y1, x2, y2).PtInRect(Pos) ) {
				
				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CTextEditorWnd::HitTestSelect(void)
{
	CPoint Pos = GetCursorPos();

	ScreenToClient(Pos);

	return HitTestSelect(Pos);
	}

BOOL CTextEditorWnd::DefaultStart(void)
{
	if( m_fDouble ) {
		
		SelectLine();

		m_fDouble = FALSE;

		KillTimer(m_timerDouble);
		
		return TRUE;
		}
	
	return FALSE;
	}

void CTextEditorWnd::DefaultUpdate(void)
{
	}

BOOL CTextEditorWnd::SelectStart(void)
{
	m_uCapture = captSelect;

	StartSelect();
	
	return TRUE;
	}

void CTextEditorWnd::SelectUpdate(void)
{
	CheckSelect();
	}

void CTextEditorWnd::SelectLine(void)
{
	m_Pos.x = 0;

	MoveNextWordStart(m_Pos);

	ClearSelect(FALSE);
	
	StartSelect();

	m_Pos.x = m_nSize;

	CheckSelect();

	SyncFrom2D();

	MoveCaret();
	}

void CTextEditorWnd::SelectWord(void)
{
	MoveThisWordStart(m_Pos);

	ClearSelect(FALSE);
	
	StartSelect();

	MoveThisWordEnd(m_Pos);

	MoveNextWordStart(m_Pos);

	CheckSelect();

	SyncFrom2D();

	MoveCaret();
	}

void CTextEditorWnd::SelectText(void)
{
	m_Pos = CPoint(0, 0);

	ClearSelect(FALSE);
	
	StartSelect();

	m_Pos.y = m_Lines.GetCount() - 1;
	
	m_Pos.x = m_Lines[m_Pos.y].GetCount();

	CheckSelect();

	SyncFrom2D();

	MoveCaret();
	}

void CTextEditorWnd::DecodeEOL(CString &Text)
{
	UINT p1;

	UINT p2 = 0;

	while( (p1 = Text.Find(EOL, p2)) < NOTHING ) {

		Text.Delete(p1, 1);
		
		Text.Insert(p1, L"\r\n");

		p2 = p1 + 2;
		}
	}

void CTextEditorWnd::EncodeEOL(CString &Text)
{
	UINT p1;

	UINT p2 = 0;

	while( (p1 = Text.Find(L"\r\n", p2)) < NOTHING ) {

		Text.Delete(p1, wstrlen(L"\r\n"));
		
		Text.Insert(p1, EOL);

		p2 = p1 + 1;
		}
	}

BOOL CTextEditorWnd::DragStart(void)
{
	if( HitTestSelect() ) {

		DragItem();

		return TRUE;
		};

	return FALSE;
	}

void CTextEditorWnd::DragItem(void)
{
	IDataObject *pData = NULL;

	if( MakeDataObject(pData) ) {

		CDragHelper *pHelp = New CDragHelper;

		pHelp->AddEmpty(pData);

		delete pHelp;

		DWORD dwAllow  = DROPEFFECT_MOVE | DROPEFFECT_COPY;

		DWORD dwResult = DROPEFFECT_NONE;

		DoDragDrop(pData, this, dwAllow, &dwResult);

		pData->Release();
		}
	}

BOOL CTextEditorWnd::IsDropMove(DWORD dwKeys, DWORD *pEffect)
{
	if( m_fDrop ) {

		if( (dwKeys & MK_CONTROL) ) {

			*pEffect = DROPEFFECT_COPY;

			return FALSE;
			}
		else {
			*pEffect = DROPEFFECT_MOVE;

			return TRUE;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	return FALSE;
	}

void CTextEditorWnd::DropTrack(CPoint Pos)
{
	ScreenToClient(Pos);
	
	if( MouseToText(Pos) ) {

		m_Pos = Pos;

		SyncFrom2D();

		MoveCaret();
		}
	}

BOOL CTextEditorWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	int nFrom = CalcFrom2D(m_SelStart);

	int nTo   = CalcFrom2D(m_SelEnd);

	int nSize = nTo - nFrom;

	CString Text = m_Text.Mid(nFrom, nSize);

	DecodeEOL(Text);

	pMake->AddText(Text);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CTextEditorWnd::CanAcceptDataObject(IDataObject *pData)
{
	FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };
	
	if( pData->QueryGetData(&Fmt) == S_OK ) {
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CTextEditorWnd::AcceptDataObject(IDataObject *pData)
{
	FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		HGLOBAL hText = Med.hGlobal;

		CString  Text = LPOLESTR(GlobalLock(hText));

		EncodeEOL(Text);

		if( !HitTestSelect() ) {

			if( DoesNotFit() ) {

				MessageBeep(0);
				}
			else {
				if( m_fDropMove ) {

					// LATER -- Only if a local move!

					DeleteSelect();
					}

				if( m_fSelect || m_fDrop ) {

					ClearSelect(FALSE);

					StartSelect();

					EnterText(Text);

					CheckSelect();
					}
				else
					EnterText(Text);
				}
			
			UpdateAll();
			}

		GlobalUnlock(hText);
		
		ReleaseStgMedium(&Med);
		
		return TRUE;
		}
	
	return FALSE;
	}

// End of File
