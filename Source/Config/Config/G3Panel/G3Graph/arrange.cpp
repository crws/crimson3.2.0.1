
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Arrange Menu

BOOL CPageEditorWnd::OnArrangeGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_ARRANGE_ALIGN,      MAKELONG(0x0006, 0x3000),
		IDM_ARRANGE_ALIGN_L,	MAKELONG(0x0007, 0x3000),
		IDM_ARRANGE_ALIGN_C,	MAKELONG(0x0008, 0x3000),
		IDM_ARRANGE_ALIGN_R,	MAKELONG(0x0009, 0x3000),
		IDM_ARRANGE_ALIGN_T,	MAKELONG(0x000A, 0x3000),
		IDM_ARRANGE_ALIGN_M,	MAKELONG(0x000B, 0x3000),
		IDM_ARRANGE_ALIGN_B,	MAKELONG(0x000C, 0x3000),
		IDM_ARRANGE_EQUAL_HORZ,	MAKELONG(0x000D, 0x3000),
		IDM_ARRANGE_EQUAL_VERT,	MAKELONG(0x000E, 0x3000),
		IDM_ARRANGE_ALIGN_O,	MAKELONG(0x000F, 0x3000),
		IDM_ARRANGE_TO_BACK,	MAKELONG(0x0010, 0x3000),
		IDM_ARRANGE_TO_FRONT,	MAKELONG(0x0011, 0x3000),
		IDM_ARRANGE_BACK,	MAKELONG(0x0012, 0x3000),
		IDM_ARRANGE_FRONT,	MAKELONG(0x0013, 0x3000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnArrangeControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_ARRANGE_FRONT:
		case IDM_ARRANGE_TO_FRONT:

			if( HasSelect() && !IsPosLocked() ) {

				if( HasUnbrokenSelect() ) {

					INDEX n = m_SelList[m_SelList.GetCount() - 1];

					Src.EnableItem(m_pWorkList->GetTail() != n);
					}
				else
					Src.EnableItem(TRUE);
				}
			break;

		case IDM_ARRANGE_BACK:
		case IDM_ARRANGE_TO_BACK:
			
			if( HasSelect() && !IsPosLocked() ) {

				if( HasUnbrokenSelect() ) {

					INDEX n = m_SelList[0];

					Src.EnableItem(m_pWorkList->GetHead() != n);
					}
				else
					Src.EnableItem(TRUE);
				}
			break;

		case IDM_ARRANGE_EQUAL_HORZ:
		case IDM_ARRANGE_EQUAL_VERT:

			Src.EnableItem(m_SelList.GetCount() >= 3 && !IsPosLocked());

			break;

		case IDM_ARRANGE_ALIGN:

			Src.EnableItem(HasSelect() && !IsPosLocked());

			break;

		case IDM_ARRANGE_ALIGN_L:
		case IDM_ARRANGE_ALIGN_C:
		case IDM_ARRANGE_ALIGN_R:
		case IDM_ARRANGE_ALIGN_T:
		case IDM_ARRANGE_ALIGN_M:
		case IDM_ARRANGE_ALIGN_B:
		case IDM_ARRANGE_ALIGN_O:

			Src.EnableItem(CanPick() && !IsPosLocked());

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnArrangeCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ARRANGE_FRONT:
			
			OnArrangeFront(FALSE);
			
			break;

		case IDM_ARRANGE_BACK:
			
			OnArrangeBack(FALSE);
			
			break;

		case IDM_ARRANGE_TO_FRONT:
			
			OnArrangeToFront(FALSE);
			
			break;

		case IDM_ARRANGE_TO_BACK:
			
			OnArrangeToBack(FALSE);
			
			break;

		case IDM_ARRANGE_EQUAL_HORZ:
			
			OnArrangeEqual(FALSE);
			
			break;

		case IDM_ARRANGE_EQUAL_VERT:
			
			OnArrangeEqual(TRUE);
			
			break;

		case IDM_ARRANGE_ALIGN:
			
			OnArrangeAlign();
			
			break;

		case IDM_ARRANGE_ALIGN_L:
		case IDM_ARRANGE_ALIGN_C:
		case IDM_ARRANGE_ALIGN_R:
		case IDM_ARRANGE_ALIGN_T:
		case IDM_ARRANGE_ALIGN_M:
		case IDM_ARRANGE_ALIGN_B:
		case IDM_ARRANGE_ALIGN_O:

			OnArrangeAlignInit(uID);

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

void CPageEditorWnd::ArrangeUndo(CStringArray &Pos)
{
	UINT uCount = m_SelList.GetCount();

	for(;;) {

		BOOL fMove = FALSE;

		for( UINT n = 0; n < uCount; n++ ) {

			INDEX   nPrim = m_SelList[n];

			CPrim * pPrim = m_pWorkList->GetItem(nPrim);

			CString Find  = Pos[n];

			INDEX   nNext = nPrim;

			INDEX   nMove = NULL;

			m_pWorkList->GetNext(nNext);
			
			if( !Find.IsEmpty() ) {

				INDEX i = m_pWorkList->GetHead();

				while( !m_pWorkList->Failed(i) ) {

					if( m_pWorkList->GetItem(i)->GetFixedName() == Find ) {

						nMove = i;

						break;
						}

					m_pWorkList->GetNext(i);
					}
				}

			if( nNext != nMove ) {

				nPrim = m_pWorkList->MoveItem(pPrim, nMove);

				m_SelList.SetAt(n, nPrim);

				fMove = TRUE;
				}
			}

		if( !fMove ) {

			break;
			}
		}

	UpdateImage();

	Invalidate(FALSE);
	}

void CPageEditorWnd::ArrangeRedo(UINT uID)
{
	switch( uID ) {

		case IDM_ARRANGE_FRONT:
			
			OnArrangeFront(TRUE);
			
			break;

		case IDM_ARRANGE_BACK:
			
			OnArrangeBack(TRUE);
			
			break;

		case IDM_ARRANGE_TO_FRONT:
			
			OnArrangeToFront(TRUE);
			
			break;

		case IDM_ARRANGE_TO_BACK:
			
			OnArrangeToBack(TRUE);
			
			break;
		}
	}

void CPageEditorWnd::ArrangeSave(UINT uID)
{
	if( !m_fScratch ) {

		CString     Item   = m_System.GetNavPos();

		UINT        uCount = m_SelList.GetCount();

		CCmdOrder * pCmd   = New CCmdOrder(Item, uID);

		for( UINT n = 0; n < uCount; n++ ) {

			INDEX nPrim = m_SelList[n];

			INDEX nNext = nPrim;

			m_pWorkList->GetNext(nNext);

			if( nNext ) {

				CPrim *pBefore = m_pWorkList->GetItem(nNext);

				pCmd->m_Pos.Append(pBefore->GetFixedName());
				}
			else
				pCmd->m_Pos.Append(L"");
			}

		LocalSaveCmd(pCmd);
		}
	}

void CPageEditorWnd::OnArrangeFront(BOOL fRedo)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount ) {

		if( !fRedo ) {

			ArrangeSave(IDM_ARRANGE_FRONT);
			}

		INDEX b = m_SelList[uCount - 1];

		m_pWorkList->GetNext(b);

		m_pWorkList->GetNext(b);

		for( UINT n = 0; n < uCount; n++ ) {

			INDEX i = m_SelList[n];

			INDEX j = m_pWorkList->MoveItem(m_pWorkList->GetItem(i), b);

			m_SelList.SetAt(n, j);
			}

		UpdateImage();

		Invalidate(FALSE);
		}
	}

void CPageEditorWnd::OnArrangeBack(BOOL fRedo)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount ) {

		ArrangeSave(IDM_ARRANGE_BACK);

		for( UINT r = 0; r < uCount; r++ ) {

			INDEX b = m_SelList[r];

			if( m_pWorkList->GetPrev(b) && !IsSelected(m_pWorkList->GetItem(b)) ) {

				for( UINT n = r; n < uCount; n++ ) {

					INDEX i = m_SelList[n];

					INDEX j = m_pWorkList->MoveItem(m_pWorkList->GetItem(i), b);

					m_SelList.SetAt(n, j);
					}

				UpdateImage();

				Invalidate(FALSE);

				break;
				}
			}
		}
	}

void CPageEditorWnd::OnArrangeToFront(BOOL fRedo)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount ) {

		if( !fRedo ) {

			ArrangeSave(IDM_ARRANGE_TO_FRONT);
			}

		INDEX b = INDEX(NULL);

		for( UINT n = 0; n < uCount; n++ ) {

			INDEX i = m_SelList[n];

			INDEX j = m_pWorkList->MoveItem(m_pWorkList->GetItem(i), b);

			m_SelList.SetAt(n, j);
			}

		UpdateImage();

		Invalidate(FALSE);
		}
	}

void CPageEditorWnd::OnArrangeToBack(BOOL fRedo)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount ) {

		if( !fRedo ) {

			ArrangeSave(IDM_ARRANGE_TO_BACK);
			}

		UINT  r = 0;

		INDEX b = m_pWorkList->GetHead();

		while( !m_pWorkList->Failed(b) ) {

			if( !IsSelected(m_pWorkList->GetItem(b)) ) {

				for( UINT n = r; n < uCount; n++ ) {

					INDEX i = m_SelList[n];

					INDEX j = m_pWorkList->MoveItem(m_pWorkList->GetItem(i), b);

					m_SelList.SetAt(n, j);
					}

				UpdateImage();

				Invalidate(FALSE);

				break;
				}

			r++;

			m_pWorkList->GetNext(b);
			}
		}
	}

void CPageEditorWnd::OnArrangeEqual(BOOL fVert)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount >= 3 ) {

		CArray <INDEX> Sort = m_SelList;

		m_pSort = m_pWorkList;

		if( fVert ) {

			qsort( PVOID(Sort.GetPointer()),
			       uCount,
			       sizeof(INDEX),
			       SortPrimsVert
			       );
			}
		else {
			qsort( PVOID(Sort.GetPointer()),
			       uCount,
			       sizeof(INDEX),
			       SortPrimsHorz
			       );
			}

		int    nTotal = 0;

		int    nGap   = 0;

		CPrim *pLast  = NULL;

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pWorkList->GetItem(Sort[n]);

			if( n ) {

				if( fVert ) {
					
					nGap = pPrim->GetRect().top  - pLast->GetRect().bottom;
					}
				else
					nGap = pPrim->GetRect().left - pLast->GetRect().right;

				nTotal += nGap;
				}

			pLast = pPrim;
			}

		if( nTotal > 0 ) {

			CString Save = m_System.GetNavPos();

			CString Verb = IDS_EQUALLY_SPACE;

			SaveMultiCmd(Save, Verb);

			nGap = nTotal / (uCount - 1);

			for( UINT n = 0; n < uCount; n++ ) {

				CPrim *pPrim = m_pWorkList->GetItem(Sort[n]);

				if( n ) {

					CRect  Rect = pPrim->GetRect();

					CPoint Pos  = Rect.GetTopLeft();
					
					CSize  Size = Rect.GetSize();

					if( fVert ) {

						Pos.y = pLast->GetRect().bottom + nGap;
						}
					else
						Pos.x = pLast->GetRect().right  + nGap;

					int dx = Pos.x - pPrim->GetRect().left;

					int dy = Pos.y - pPrim->GetRect().top;

					if( dx || dy ) {

						CRect Move = CRect(Pos, Size);

						ClipMoveRect(Move);

						dx = Move.left - Rect.left;

						dy = Move.top  - Rect.top;

						if( dx || dy ) {

							if( !m_fScratch ) {

								CString Item = pPrim->GetFixedPath();

								CCmd *  pCmd = New CCmdMove(Item, TRUE, dx, dy);

								LocalSaveCmd(pCmd);
								}

							pPrim->SetRect(Rect, Move);
							}
						}
					}

				pLast = pPrim;
				}

			SaveMultiCmd(Save, Verb);

			UpdateAll();

			m_pItem->SetDirty();

			return;
			}

		Error(CString(IDS_NOT_ENOUGH_SPACE));
		}
	}

void CPageEditorWnd::OnArrangeAlign(void)
{
	UINT uCount = m_SelList.GetCount();

	BOOL fPage  = (uCount == 1);

	CAlignDialog Dlg(fPage);
	
	if( Dlg.Execute(ThisObject) ) {
		
		PerformAlign( fPage,
			      Dlg.GetHorz(),
			      Dlg.GetVert(),
			      Dlg.UseRef() ? m_SelRef : NULL,
			      TRUE
			      );
		}
	}

void CPageEditorWnd::OnArrangeAlignInit(UINT uID)
{
	SetMode(modePickLoc, FALSE);

	m_uPick = uID;
	}

void CPageEditorWnd::OnArrangeAlignDone(void)
{
	switch( m_uPick ) {

		case IDM_ARRANGE_ALIGN_L:

			PerformAlign(FALSE, 1, 0, m_nPick, TRUE);

			break;

		case IDM_ARRANGE_ALIGN_C:

			PerformAlign(FALSE, 2, 0, m_nPick, TRUE);

			break;

		case IDM_ARRANGE_ALIGN_R:

			PerformAlign(FALSE, 3, 0, m_nPick, TRUE);

			break;

		case IDM_ARRANGE_ALIGN_T:

			PerformAlign(FALSE, 0, 1, m_nPick, TRUE);

			break;

		case IDM_ARRANGE_ALIGN_M:

			PerformAlign(FALSE, 0, 2, m_nPick, TRUE);

			break;

		case IDM_ARRANGE_ALIGN_B:

			PerformAlign(FALSE, 0, 3, m_nPick, TRUE);

			break;

		case IDM_ARRANGE_ALIGN_O:

			PerformAlign(FALSE, 2, 2, m_nPick, TRUE);

			break;
		}
	}

void CPageEditorWnd::PerformAlign(BOOL fPage, UINT uHorz, UINT uVert, INDEX Ref, BOOL fMulti)
{
	UINT  uCount = m_SelList.GetCount();

	int   nLarge = 9999999;

	CRect Target = CRect(+nLarge, +nLarge, -nLarge, -nLarge);

	if( fPage ) {

		Target = m_WorkRect;
		}
	else {
		if( Ref ) {

			Target = m_pWorkList->GetItem(Ref)->GetRect();
			}
		else {
			for( UINT n = 0; n < uCount; n++ ) {

				INDEX Index = m_SelList[n];

				CRect Rect  = m_pWorkList->GetItem(Index)->GetRect();

				MakeMin(Target.left,   Rect.left);
				
				MakeMin(Target.top,    Rect.top);
				
				MakeMax(Target.right,  Rect.right);
				
				MakeMax(Target.bottom, Rect.bottom);
				}
			}
		}

	CString Save = m_System.GetNavPos();

	CString Verb = IDS_ALIGN;

	if( fMulti ) {

		SaveMultiCmd(Save, Verb);
		}

	for( UINT n = 0; n < uCount; n++ ) {

		INDEX Index = m_SelList[n];

		if( Index != Ref ) {

			CPrim *pPrim = m_pWorkList->GetItem(Index);

			CRect  Rect  = pPrim->GetRect();

			CPoint Pos   = Rect.GetTopLeft();
			
			CSize  Size  = Rect.GetSize();
		
			Pos.x = Align(uHorz, Target.left, Target.right, Pos.x, Size.cx);

			Pos.y = Align(uVert, Target.top, Target.bottom, Pos.y, Size.cy);

			int dx = Pos.x - pPrim->GetRect().left;

			int dy = Pos.y - pPrim->GetRect().top;

			if( dx || dy ) {

				CRect Move = CRect(Pos, Size);

				ClipMoveRect(Move);

				dx = Move.left - Rect.left;

				dy = Move.top  - Rect.top;

				if( dx || dy ) {

					if( !m_fScratch ) {

						CString Item = pPrim->GetFixedPath();

						CCmd *  pCmd = New CCmdMove(Item, TRUE, dx, dy);

						LocalSaveCmd(pCmd);
						}

					pPrim->SetRect(Rect, Move);
					}
				}
			}
		}

	if( fMulti ) {

		SaveMultiCmd(Save, Verb);
		}

	UpdateAll();

	m_pItem->SetDirty();
	}

int CPageEditorWnd::Align(UINT uCode, int nStart, int nEnd, int nPos, long nSize)
{
	switch( uCode ) {
			
		case 1:
			return nStart;
					
		case 2:
			return nStart + (nEnd - nStart - nSize + 1) / 2;
					
		case 3:
			return nEnd - nSize;
		}
		
	return nPos;
	}

// Sort Data

CPrimList * CPageEditorWnd::m_pSort = NULL;

// Sort Helpers

int CPageEditorWnd::SortPrimsHorz(PCVOID i1, PCVOID i2)
{
	CPrim *p1 = m_pSort->GetItem(((INDEX *) i1)[0]);

	CPrim *p2 = m_pSort->GetItem(((INDEX *) i2)[0]);

	if( p1->GetRect().left < p2->GetRect().left ) {
		
		return -1;
		}

	if( p1->GetRect().left > p2->GetRect().left ) {
		
		return +1;
		}

	return 0;
	}

int CPageEditorWnd::SortPrimsVert(PCVOID i1, PCVOID i2)
{
	CPrim *p1 = m_pSort->GetItem(((INDEX *) i1)[0]);

	CPrim *p2 = m_pSort->GetItem(((INDEX *) i2)[0]);

	if( p1->GetRect().top < p2->GetRect().top ) {
		
		return -1;
		}

	if( p1->GetRect().top > p2->GetRect().top ) {
		
		return +1;
		}

	return 0;
	}

// End of File
