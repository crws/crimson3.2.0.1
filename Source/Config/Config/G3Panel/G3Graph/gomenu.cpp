
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Go Menu

BOOL CPageEditorWnd::OnGoGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= IDM_GO_REFERENCE ) {

		INDEX n = m_JumpList.GetHead();
		
		while( !m_JumpList.Failed(n) ) {

			if( uID-- == IDM_GO_REFERENCE ) {

				CStringArray List;

				m_JumpList[n].Tokenize(List, '|');

				if( List[0] == L"A" ) {

					Info.m_Image = 0x20000001;
					}

				if( List[0] == L"CDispPage" ) {

					Info.m_Image = 0x20000002;
					}

				if( List[0] == L"CProgramItem" ) {

					Info.m_Image = 0x20000003;
					}

				if( List[0] == L"CDataLog" ) {

					Info.m_Image = 0x20000004;
					}

				break;
				}
			
			m_JumpList.GetNext(n);
			}

		Info.m_Prompt = CString(IDS_JUMP_TO_SELECTED);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_GO_REFERENCE ) {

		Src.EnableItem(!m_fScratch && TRUE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnGoCommand(UINT uID)
{
	if( uID >= IDM_GO_REFERENCE ) {

		if( !m_fScratch ) {

			INDEX n = m_JumpList.GetHead();
		
			while( !m_JumpList.Failed(n) ) {

				if( uID-- == IDM_GO_REFERENCE ) {

					CStringArray List;

					m_JumpList[n].Tokenize(List, '|');

					m_System.Navigate(List[2]);

					break;
					}
			
				m_JumpList.GetNext(n);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Jump List Handling

void CPageEditorWnd::AddJumpCommands(CMenu &Menu)
{
	if( m_JumpList.GetCount() ) {

		CMenu Popup;

		Popup.CreatePopupMenu();

		UINT    uID  = IDM_GO_REFERENCE;

		CString Type = L"";

		for( INDEX n = m_JumpList.GetHead(); !m_JumpList.Failed(n); m_JumpList.GetNext(n) ) {

			CStringArray List;

			m_JumpList[n].Tokenize(List, '|');

			if( Type != List[0] ) {

				if( !Type.IsEmpty() ) {
				
					Popup.AppendSeparator();
					}

				Type = List[0];
				}

			Popup.AppendMenu(0, uID++, List[1]);
			}

		int nGap = 1;

		int nPos;

		for( nPos = 0; nPos < Menu.GetMenuItemCount(); nPos++ ) {

			UINT uID = Menu.GetMenuItemID(nPos);

			if( HIBYTE(uID) == IDM_EDIT ) {

				break;
				}

			if( uID == 0 ) {

				if( !--nGap ) {

					nPos++;

					break;
					}
				}
			}

		Menu.InsertMenu(nPos++, MF_BYPOSITION, Popup, CString(IDS_JUMP_TO));

		Menu.InsertSeparator(nPos, MF_BYPOSITION);
		}
	}

void CPageEditorWnd::BuildJumpList(void)
{
	m_JumpList.Empty();

	UINT uCount = m_SelList.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		INDEX  Index = m_SelList[n];

		CPrim *pPrim = m_pWorkList->GetItem(Index);

		BuildJumpList(pPrim);
		}
	}

void CPageEditorWnd::BuildJumpList(CMetaItem *pItem)
{
	if( pItem ) {

		CMetaList *pList = pItem->FindMetaList();

		UINT       uCount = pList->GetCount();

		for( UINT n = 0; n < uCount; n++ ) {

			CMetaData const *pMeta = pList->FindData(n);

			UINT             uType = pMeta->GetType();

			if( uType == metaCollect ) {

				CItemList *pList = (CItemList *) pMeta->GetObject(pItem);

				if( pList ) {

					INDEX Index = pList->GetHead();

					while( !pList->Failed(Index) ) {

						CItem *pChild = pList->GetItem(Index);

						BuildJumpList(pChild);

						pList->GetNext(Index);
						}
					}
				}

			if( uType == metaObject || uType == metaVirtual ) {

				CItem *pChild = pMeta->GetObject(pItem);

				if( pChild ) {

					BuildJumpList(pChild);
					}
				}
			}
		}
	}

void CPageEditorWnd::BuildJumpList(CItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CCodedItem)) ) {

		CCodedItem *pCoded = (CCodedItem *) pItem;

		BuildJumpList(pCoded);
		}
	else {
		if( pItem->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

			CMetaItem *pMeta = (CMetaItem *) pItem;

			BuildJumpList(pMeta);
			}
		}
	}

void CPageEditorWnd::BuildJumpList(CCodedItem *pCoded)
{
	IDataServer *pData  = pCoded->GetDataServer();

	UINT	     uCount = pCoded->GetRefCount();

	for( UINT uRef = 0; uRef < uCount; uRef++ ) {

		CDataRef const &Ref = pCoded->GetRef(uRef);

		if( !Ref.t.m_IsTag ) {

			if( !Ref.b.m_Block ) {

				AddToJumpList(Ref.t.m_Index);
				}
			}
		else {
			DWORD  Data = (DWORD &) Ref;

			CTag * pTag = (CTag *) pData->GetItem(Data);

			AddToJumpList(pTag);
			}
		}
	}

BOOL CPageEditorWnd::AddToJumpList(UINT uHandle)
{
	// REV3 -- We should abstract this back into the name
	// server somehow so that we can find data logs and
	// other things we don't know about from here...

	CDatabase *pDbase = m_pItem->GetDatabase();

	CItem     *pItem  = pDbase->GetHandleItem(uHandle);

	if( pItem ) {

		CMetaItem *pMeta = (CMetaItem *) pItem;

		AddToJumpList(pMeta);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::AddToJumpList(CMetaItem *pMeta)
{
	if( pMeta->HasName() ) {

		CString Type = pMeta->GetClassName();

		CString Name = pMeta->GetName();

		CString Path = pMeta->GetFixedPath();

		m_JumpList.Insert(Type + L'|' + Name + L'|' + Path);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::AddToJumpList(CTag *pTag)
{
	if( pTag ) {

		if( pTag->IsKindOf(AfxRuntimeClass(CTag)) ) {

			CString Type = L"A";

			CString Name = pTag->GetName();

			CString Path = pTag->GetFixedPath();

			m_JumpList.Insert(Type + L'|' + Name + L'|' + Path);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
