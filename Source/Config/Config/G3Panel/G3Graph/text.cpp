
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Textor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Text Menu

BOOL CPageEditorWnd::OnTextGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_TEXT_ADD,        MAKELONG(0x000F, 0x3001),
		IDM_TEXT_EDIT,       MAKELONG(0x0008, 0x3001),
		IDM_TEXT_REM,	     MAKELONG(0x0008, 0x1000),
		IDM_TEXT_SPACE_MORE, MAKELONG(0x0006, 0x3001),
		IDM_TEXT_SPACE_LESS, MAKELONG(0x0007, 0x3001),
		IDM_TEXT_PROPERTIES, MAKELONG(0x0012, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			return FALSE;
			}
		}

	uID -= IDM_TEXT_TITLE;

	uID += IDM_BLOCK_TITLE;

	return OnBlockGetInfo(uID, Info);
	}

BOOL CPageEditorWnd::OnTextControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	CPrimWithText *pHost = NULL;

	CPrimText     *pText = NULL;

	BOOL           fData = FALSE;

	BOOL	       fEdit = FALSE;

	if( !ArePropsLocked() ) {

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				pHost = (CPrimWithText *) pPrim;

				if( pHost->HasText() ) {

					pText = pHost->m_pTextItem;

					fEdit = pHost->IsTextEditable();
					}

				if( pHost->HasData() ) {

					fData = TRUE;
					}
				}
			}
		}

	switch( uID ) {

		case IDM_TEXT_ADD:

			Src.EnableItem(pHost && !pText && !fData);

			break;

		case IDM_TEXT_REM:

			Src.EnableItem(pText ? TRUE : FALSE);

			break;

		case IDM_TEXT_EDIT:

			Src.EnableItem(m_nScale && pText && fEdit);

			break;

		case IDM_TEXT_SPACE_MORE:

			Src.EnableItem(pText && pText->m_Lead < 20);

			break;

		case IDM_TEXT_SPACE_LESS:

			Src.EnableItem(pText && pText->m_Lead > 0);

			break;

		case IDM_TEXT_PROPERTIES:

			Src.EnableItem(pText ? TRUE : FALSE);

			break;

		case IDM_TEXT_CREATED:

			Src.EnableItem(TRUE);

			break;

		default:
			if( pText ) {
				
				uID -= IDM_TEXT_TITLE;

				uID += IDM_BLOCK_TITLE;
				
				return OnBlockControl(uID, Src);
				}

			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnTextCommand(UINT uID)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	CPrim     *pPrim = NULL;

	CPrimText *pText = NULL;

	if( !ArePropsLocked() ) {

		if( HasLoneSelect() ) {

			pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				CPrimWithText *pHost = (CPrimWithText *) pPrim;

				if( pHost->HasText() ) {

					pText = pHost->m_pTextItem;
					}
				}
			}
		}

	switch( uID ) {

		case IDM_TEXT_ADD:

			OnTextAdd();

			break;

		case IDM_TEXT_EDIT:

			OnTextEdit();

			break;
		
		case IDM_TEXT_REM:
			
			OnTextRemove();

			break;

		case IDM_TEXT_SPACE_MORE:

			if( pText ) {

				MakeBlockCmd(pPrim, CString(IDS_CHANGE_TEXT_2));

				pText->m_Lead++;

				SaveBlockCmd();
			}

			break;

		case IDM_TEXT_SPACE_LESS:

			if( pText ) {

				MakeBlockCmd(pPrim, CString(IDS_CHANGE_TEXT_2));

				pText->m_Lead--;

				SaveBlockCmd();
			}

			break;

		case IDM_TEXT_PROPERTIES:

			if( pText ) {

				OnEditItemProps(CString(IDS_TEXT));
				}

			break;

		case IDM_TEXT_CREATED:

			OnTextCreated();

			break;

		default:
			if( pText ) {
				
				uID -= IDM_TEXT_TITLE;

				uID += IDM_BLOCK_TITLE;
				
				return OnBlockCommand(uID);
				}

			return FALSE;
		}

	return TRUE;
	}

void CPageEditorWnd::OnTextAdd(void)
{
	if( IsTextZoomOkay(FALSE) ) {

		CPrim         * pPrim = GetLoneSelect();

		CPrimWithText * pHost = (CPrimWithText *) pPrim;

		MakeBlockCmd(pPrim, CString(IDS_ADD_TEXT));

		BOOL fAdd = pHost->AddText();

		UpdateAll();

		SetTextMode(FALSE, fAdd);
		}
	}

void CPageEditorWnd::OnTextCreated(void)
{
	if( IsTextZoomOkay(FALSE) ) {

		CPrim * pPrim = GetLoneSelect();

		MakeBlockCmd(pPrim, CString(IDS_EDIT_TEXT));

		UpdateAll();

		SetTextMode(FALSE, TRUE);
		}
	}

void CPageEditorWnd::OnTextEdit(void)
{
	if( IsTextZoomOkay(FALSE) ) {

		CPrim * pPrim = GetLoneSelect();

		MakeBlockCmd(pPrim, CString(IDS_EDIT_TEXT));

		SetTextMode(FALSE, FALSE);
		}
	}

void CPageEditorWnd::OnTextRemove(void)
{
	CPrim         * pPrim = GetLoneSelect();

	CPrimWithText * pHost = (CPrimWithText *) pPrim;

	MakeBlockCmd(pPrim, CString(IDS_REMOVE_TEXT));

	pHost->RemText();

	UpdateSelectData();

	SaveBlockCmd();
	}

BOOL CPageEditorWnd::IsTextZoomOkay(BOOL fMouse)
{
	if( !m_nScale ) {

		CPoint Pos;

		Pos = DPtoPP(m_ClientPos);

		SetScale(m_nScale + 1);

		if( fMouse ) {

			Pos = PPtoDP(Pos);

			m_ClientPos = Pos;

			ClientToScreen(Pos);

			SetCursorPos(Pos.x, Pos.y);
			}
		}

	return TRUE;
	}

BOOL CPageEditorWnd::AllowTextMode(void)
{
	if( !ArePropsLocked() ) {

		if( HasLoneSelect() ) {

			CPrim * pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				CPrimWithText * pHost = (CPrimWithText *) pPrim;
				
				CUnicode Text = pHost->m_pTextItem->m_pText->GetText();

				return !UniIsComplex(Text);
				}
			}
		}

	return FALSE;
	}

void CPageEditorWnd::EnterTextMode(void)
{
	// LATER -- If the text box isn't complete contained within the
	// drawn areas of the primitive, this doens't work too well as
	// the bits of text don't get cleared out when we draw. We could
	// fill in with background or draw the rest of the list and skip
	// the selection. The latter would be better...

	AfxAssert(m_nScale);

	CPrim         * pPrim = GetLoneSelect();

	CPrimWithText * pHost = (CPrimWithText *) pPrim;

	CRect Text = pHost->GetTextRect();

	CSize Size = Text.GetSize();

	m_TextBitmap.Create(*m_pMap, Size);

	CMemoryDC DC(*m_pMap);

	DC.Select(m_TextBitmap);

	DC.BitBlt(CRect(Size), CDC::FromHandle(m_pWin->GetDC()), Text.GetTopLeft(), SRCCOPY);

	DC.Deselect();

	ExposeText();

	CRect  Rect = PPtoDP(m_SelText);

	CPoint Pos  = m_ClientPos - Rect.GetTopLeft();

	if( !m_fTextMouse ) {

		Pos.x = -1;

		Pos.y = -1;
		}

	m_pTextCtrl = New CTextEditorWnd( pHost->m_pTextItem,
					  m_nScale,
					  Pos,
					  m_fTextAdd,
					  m_TextBitmap
					  );

	m_pTextCtrl->Create(WS_VISIBLE, Rect, ThisObject, 100);

	m_pTextCtrl->SetFocus();

	afxMainWnd->SendMessage(WM_UPDATEUI);
	}

void CPageEditorWnd::LeaveTextMode(BOOL fAbort)
{
	if( !fAbort ) {

		if( m_pTextCtrl->WriteBack() ) {

			if( m_pTextPrim->m_pTextItem->IsEmpty() ) {

				m_TextMenu = CString(IDS_REMOVE_TEXT);

				m_pTextPrim->RemText();

				UpdateSelectData();
				}

			SaveBlockCmd();
			}
		else {
			if( m_pTextPrim->m_pTextItem->IsEmpty() ) {

				m_pTextPrim->RemText();

				UpdateSelectData();
				}

			KillBlockCmd();
			}
		}
	else {
		if( m_fTextAdd ) {

			m_pTextPrim->RemText();

			UpdateSelectData();
			}

		KillBlockCmd();
		}

	AfxNull(CWnd).SetFocus();

	m_pTextCtrl->DestroyWindow(TRUE);

	m_pTextCtrl = NULL;

	afxMainWnd->SendMessage(WM_UPDATEUI);
	}

BOOL CPageEditorWnd::CheckCreateText(void)
{
	if( HasLoneSelect() ) {

		CPrim *pPrim = GetLoneSelect();

		if( pPrim->IsTextBox() ) {

			if( m_nScale ) {

				afxMainWnd->PostMessage(WM_COMMAND, IDM_TEXT_CREATED);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CPageEditorWnd::ExposeText(void)
{
	// NOTE -- This isn't perfect, as it can't deal with situations where
	// the text box simply can't be displayed at one-to-one. Also, it contains
	// hard-coded assumptions about the size of scroll bars as a result of
	// GetClientRect's failure to account for the space that these take up.

	CSize Step = CSize(0, 0);

	CRect Show = PPtoDP(m_SelText) + 16;

	CRect Clip = GetClientRect();

	if( m_fScrollH ) {

		Clip.bottom -= 18;
		}

	if( m_fScrollV ) {

		Clip.right -= 18;
		}

	if( Show.left < Clip.left ) {

		Step.cx = Show.left - Clip.left;
		}
	else {
		if( Show.right > Clip.right ) {

			Step.cx = Show.right - Clip.right;
			}
		}

	if( Show.top < Clip.top ) {

		Step.cy = Show.top - Clip.top;
		}
	else {
		if( Show.bottom > Clip.bottom) {

			Step.cy = Show.bottom- Clip.bottom;
			}
		}

	if( Step.cx || Step.cy ) {

		ScrollBy(Step);
		}
	}

// End of File
