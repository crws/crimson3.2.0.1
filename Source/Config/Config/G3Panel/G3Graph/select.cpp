
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Working List

void CPageEditorWnd::ClearWorkList(void)
{
	m_pWorkList = m_pList;

	m_pWorkSet  = NULL;

	m_pPendSet  = NULL;

	m_WorkRect  = CRect(m_DispSize);

	Invalidate(FALSE);
	}

void CPageEditorWnd::SelectWorkList(void)
{
	m_pWorkSet  = m_pPendSet;

	m_pWorkList = m_pWorkSet->m_pList;

	m_WorkRect  = m_pWorkSet->GetRect();

	m_pPendSet  = NULL;

	if( m_SelList.GetCount() ) {

		ClearSelect(FALSE);
		}
	else
		UpdateSelectData();

	UpdateImage();
	}

BOOL CPageEditorWnd::ClimbWorkList(BOOL fEscape)
{
	if( m_pWorkSet ) {

		CPrim *pPrev = m_pWorkSet;

		CItem *pItem = m_pWorkSet->GetParent(2);

		if( pItem->IsKindOf(AfxRuntimeClass(CPrimSet)) ) {

			m_pWorkSet  = (CPrimSet *) pItem;

			m_pWorkList = m_pWorkSet->m_pList;

			m_WorkRect  = m_pWorkSet->GetRect();

			m_pPendSet  = NULL;
			}
		else {
			m_pWorkList = m_pList;

			m_pWorkSet  = NULL;

			m_pPendSet  = NULL;

			m_WorkRect  = CRect(m_DispSize);
			}
			
		INDEX Index = m_pWorkList->FindItemIndex(pPrev);

		ClearSelect(TRUE);

		AddSelect  (Index, FALSE);

		if( CanNormalize() ) {

			OnOrganizeNormalize();
			}
		else
			UpdateImage();
		}

	return m_pWorkSet ? TRUE : FALSE;
	}

// Selection Access

CPrim * CPageEditorWnd::GetLoneSelect(void)
{
	AfxAssert(HasLoneSelect());

	return m_pWorkList->GetItem(m_SelList[0]);
	}

// Selection List

BOOL CPageEditorWnd::HasSelect(void)
{
	return !m_SelList.IsEmpty();
	}

BOOL CPageEditorWnd::HasLoneSelect(void)
{
	return m_SelList.GetCount() == 1;
	}

BOOL CPageEditorWnd::HasMultiSelect(void)
{
	return m_SelList.GetCount() > 1;
	}

BOOL CPageEditorWnd::HasZoomPages(void)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

			if( HasZoomPages(pPrim) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::HasZoomPages(CPrim *pPrim)
{
	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

		CPrimWidget *pWidget = (CPrimWidget *) pPrim;

		if( pWidget->IsBound() ) {

			if( pWidget->HasZoomPages() ) {

				return TRUE;
				}
			}
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimSet)) ) {

		CPrimSet *pSet = (CPrimSet *) pPrim;

		if( HasZoomPages(pSet->m_pList) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::HasZoomPages(CPrimList *pList)
{
	INDEX n = pList->GetHead();

	while( !pList->Failed(n) ) {

		CPrim *pPrim = pList->GetItem(n);

		if( HasZoomPages(pPrim) ) {

			return TRUE;
			}

		pList->GetNext(n);
		}

	return FALSE;
	}

BOOL CPageEditorWnd::IsPickable(INDEX nPrim)
{
	if( !IsSelected(nPrim) ) {

		return TRUE;
		}

	if( HasMultiSelect() ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::IsHoverable(INDEX nPrim)
{
	if( !IsSelected(nPrim) ) {

		return TRUE;
		}

	if( InPickMode() ) {

		if( HasMultiSelect() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::IsSelected(INDEX nPrim)
{
	return !m_SelList.Failed(m_SelList.Find(nPrim));
	}

BOOL CPageEditorWnd::IsSelected(CPrim *pPrim)
{
	return IsSelected(m_pWorkList->FindItemIndex(pPrim));
	}

BOOL CPageEditorWnd::IsPosLocked(void)
{
	if( !m_fRead ) {

		// LATER -- Add lock capabilities to all primitives?

		return IsListLocked();
		}

	return TRUE;
	}

BOOL CPageEditorWnd::IsListLocked(void)
{
	if( !m_fRead ) {

		if( m_pWorkSet ) {

			if( m_pWorkSet->m_LockList >= 1 ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::ArePropsLocked(void)
{
	if( !m_fRead ) {

		// LATER -- Add lock capabilities to all primitives?

		if( m_pWorkSet ) {

			if( m_pWorkSet->m_LockList >= 2 ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::HasUnbrokenSelect(void)
{
	// LATER -- Implement a better check.

	return m_SelList.GetCount() == 1;
	}

BOOL CPageEditorWnd::ClearHover(void)
{
	if( m_pHover ) {

		KillHover();
		
		UpdateImage();

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::KillHover(void)
{
	m_pHover = NULL;

	m_nHover = NULL;

	KillTimer(m_timerHover);
	}

BOOL CPageEditorWnd::ClearSelect(BOOL fGone)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount ) {

		if( IsWindow() ) {

			if( m_pHover ) {

				if( fGone || IsSelected(m_nHover) ) {

					KillHover();
					}
				}

			if( m_pHover ) {

				if( m_pHover->GetParent(2) == m_pPendSet ) {

					KillHover();
					}
				}

			m_SelRect.Empty();
			
			m_SelList.Empty();

			NewSelection();
			}

		return TRUE;
		}

	if( m_pHover ) {

		if( fGone ) {

			KillHover();
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::SetSelect(INDEX nPrim)
{
	if( nPrim ) {

		if( HasLoneSelect() ) {
			
			if( IsSelected(nPrim) ) {

				return FALSE;
				}
			}
		}

	ClearSelect(FALSE);

	return AddSelect(nPrim, FALSE);
	}

BOOL CPageEditorWnd::AddSelect(INDEX nPrim, BOOL fPend)
{
	if( nPrim ) {

		if( !IsSelected(nPrim) ) {

			CPrim * pPrim = m_pWorkList->GetItem(nPrim);

			INDEX   Index = nPrim;

			BOOL    fAfter = FALSE;

			if( !m_SelList.GetCount() ) {

				m_SelRef = Index;
				}

			while( m_pWorkList->GetNext(Index) ) {

				CPrim *pScan = m_pWorkList->GetItem(Index);

				if( IsSelected(pScan) ) {

					fAfter = TRUE;

					break;
					}
				}

			if( fAfter ) {

				UINT uCount = m_SelList.GetCount();

				for( UINT i = 0; i < uCount; i++ ) {

					if( m_SelList[i] == Index ) {

						m_SelList.Insert(i, nPrim);

						break;
						}
					}
				}
			else
				m_SelList.Append(nPrim);

			m_SelRect |= pPrim->GetNormRect();

			if( !fPend ) {

				NewSelection();
				}

			if( nPrim == m_nHover ) {

				KillTimer(m_timerHover);
				}

			return TRUE;
			}
		}

	if( !fPend ) {

		NewSelection();
		}

	return FALSE;
	}

BOOL CPageEditorWnd::RemSelect(INDEX nPrim, BOOL fPend)
{
	if( nPrim ) {

		UINT uPos = m_SelList.Find(nPrim);

		if( !m_SelList.Failed(uPos) ) {

			m_SelList.Remove(uPos);

			if( !fPend ) {

				NewSelection();
				}

			return TRUE;
			}
		}

	if( !fPend ) {

		NewSelection();
		}

	return FALSE;
	}

void CPageEditorWnd::NewSelection(void)
{
	UpdateSelectData();

	MakeGhost();
	}

void CPageEditorWnd::UpdateSelectData(BOOL fGhost)
{
	m_SelRect.Empty();

	m_SelText.Empty();

	m_pPendSet = NULL;

	UINT uCount  = m_SelList.GetCount();

	if( uCount >= 1 ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

			m_SelRect |= pPrim->GetNormRect();
			}

		if( uCount == 1 ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->IsSet() ) {

				CPrimSet *pSet = (CPrimSet *) pPrim;

				if( pSet->m_LockList != 2 ) {

					m_pPendSet = pSet;
					}
				}

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				CPrimWithText *pHost= (CPrimWithText *) pPrim;

				if( pHost->IsTextEditable() ) {

					m_SelText = pHost->GetTextRect();
					}
				}
			}
		}

	BuildHandles();

	ShowDefaultStatus();

	if( !fGhost ) {

		KillGhost();
		}

	Invalidate(FALSE);
	}

void CPageEditorWnd::BuildHandles(void)
{
	m_Handles.Empty();

	if( !IsPosLocked() ) {

		UINT uCount = m_SelList.GetCount();

		if( uCount ) {

			if( uCount == 1 ) {

				CPrim *pPrim = GetLoneSelect();

				if( pPrim->IsLine() ) {

					BuildLineHandles(pPrim);
					}
				else {
					BuildItemHandles(pPrim);

					BuildRectHandles();
					}
				}
			else
				BuildRectHandles();

			BuildMoveHandles();
			}
		}

	UpdatePos();
	}

void CPageEditorWnd::BuildRectHandles(void)
{
	CSize Size = GetHandleSize();

	CRect Rect = PPtoDP(m_SelRect) + 3;
	
	CHand Hand;

	Hand.m_Code = 1;

	for( int x = 0; x <= 2; x++ ) {

		switch( x ) {

			case 0:
				Hand.m_Rect.left  = Rect.left - Size.cx + 1;
				Hand.m_Rect.right = Rect.left + Size.cx + 0;
				break;

			case 1:
				Hand.m_Rect.left  = (Rect.left + Rect.right) / 2 - Size.cx + 1;
				Hand.m_Rect.right = (Rect.left + Rect.right) / 2 + Size.cx - 0;
				break;

			case 2:
				Hand.m_Rect.left  = Rect.right - Size.cx - 0;
				Hand.m_Rect.right = Rect.right + Size.cx - 1;
				break;
			}

		for( int y = 0; y <= 2; y++ ) {

			switch( y ) {

				case 0:
					Hand.m_Rect.top    = Rect.top - Size.cy + 1;
					Hand.m_Rect.bottom = Rect.top + Size.cy + 0;
					break;

				case 1:
					Hand.m_Rect.top    = (Rect.top + Rect.bottom) / 2 - Size.cy + 1;
					Hand.m_Rect.bottom = (Rect.top + Rect.bottom) / 2 + Size.cy - 0;
					break;

				case 2:
					Hand.m_Rect.top    = Rect.bottom - Size.cy - 0;
					Hand.m_Rect.bottom = Rect.bottom + Size.cy - 1;
					break;
				}

			if( x != 1 || y != 1 ) {

				// cppcheck-suppress uninitStructMember

				m_Handles.Append(Hand);
				}

			Hand.m_Code++;
			}
		}
	}

void CPageEditorWnd::BuildMoveHandles(void)
{
	CSize Size   = GetHandleSize();

	UINT  uCount = m_SelList.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

		CRect  Rect  = PPtoDP(pPrim->GetRect()) + 3;
	
		CHand Hand;

		Hand.m_Code = handMid;

		Hand.m_Rect.left   = (Rect.left + Rect.right) / 2 - Size.cx + 1;
		Hand.m_Rect.right  = (Rect.left + Rect.right) / 2 + Size.cx - 0;
		Hand.m_Rect.top    = (Rect.top + Rect.bottom) / 2 - Size.cy + 1;
		Hand.m_Rect.bottom = (Rect.top + Rect.bottom) / 2 + Size.cy - 0;

		if( m_SelList[n] == m_SelRef ) {

			Hand.m_Rect += 2;
			}

		// cppcheck-suppress uninitStructMember

		m_Handles.Append(Hand);
		}
	}

void CPageEditorWnd::BuildLineHandles(CPrim *pPrim)
{
	CPoint p1 = PPtoDP(pPrim->GetRect().GetTopLeft    ());

	CPoint p2 = PPtoDP(pPrim->GetRect().GetBottomRight());

	CSize Size = GetHandleSize();

	CHand Hand;

	Hand.m_Rect.left   = p1.x - Size.cx + 0;
	Hand.m_Rect.top    = p1.y - Size.cy + 0;
	Hand.m_Rect.right  = p1.x + Size.cx + 1;
	Hand.m_Rect.bottom = p1.y + Size.cy + 1;

	Hand.m_Code = handLine1;

	// cppcheck-suppress uninitStructMember

	m_Handles.Append(Hand);

	Hand.m_Rect.left   = p2.x - Size.cx + 0;
	Hand.m_Rect.top    = p2.y - Size.cy + 0;
	Hand.m_Rect.right  = p2.x + Size.cx + 1;
	Hand.m_Rect.bottom = p2.y + Size.cy + 1;

	Hand.m_Code = handLine2;

	// cppcheck-suppress uninitStructMember

	m_Handles.Append(Hand);
	}

void CPageEditorWnd::BuildItemHandles(CPrim *pPrim)
{
	UINT uHand = 0;

	for(;;) {

		CHand Hand;

		if( pPrim->GetHand(uHand, Hand.m_Hand) ) {

			Hand.m_Code = handItem;

			Hand.m_uPos = uHand++;

			BuildItemHandle(pPrim, Hand);

			Hand.m_Old  = Hand.m_Hand.m_Pos;

			m_Handles.Append(Hand);

			continue;
			}

		break;
		}
	}

void CPageEditorWnd::BuildItemHandle(CPrim *pPrim, CHand &Hand)
{
	CSize  Size = GetHandleSize();

	CPoint Pos  = Hand.m_Hand.m_Pos;

	CRect  Rect = pPrim->GetRect();

	if( Hand.m_Hand.m_uRef / 100 == 1 ) {

		Pos.x = Pos.x * Rect.cx() / 10000;
		Pos.y = Pos.y * Rect.cy() / 10000;
		}

	if( Hand.m_Hand.m_uRef / 100 == 2 ) {

		if( Hand.m_Hand.m_Clip.cx() ) {

			Pos.x = Pos.x * Rect.cy() / 10000;
			Pos.y = Pos.y * Rect.cy() / 10000;
			}
		else {
			Pos.x = Pos.x * Rect.cx() / 10000;
			Pos.y = Pos.y * Rect.cx() / 10000;
			}
		}

	switch( Hand.m_Hand.m_uRef % 100 ) {

		case 0:
		case 3:
			Pos.x = Rect.left + Pos.x;
			break;

		case 1:
		case 2:
			Pos.x = Rect.right - 1 - Pos.x;
			break;
		}

	switch( Hand.m_Hand.m_uRef % 100 ) {

		case 0:
		case 1:
			Pos.y = Rect.top + Pos.y;
			break;

		case 2:
		case 3:
			Pos.y = Rect.bottom - 1 - Pos.y;
			break;
		}

	Pos = PPtoDP(Pos);

	Hand.m_Rect.left   = Pos.x - Size.cx + 0;
	Hand.m_Rect.top    = Pos.y - Size.cy + 0;
	Hand.m_Rect.right  = Pos.x + Size.cx + 1;
	Hand.m_Rect.bottom = Pos.y + Size.cy + 1;
	}

CSize CPageEditorWnd::GetHandleSize(void)
{
	switch( m_nScale ) {

		case 0:
			return CSize( 3,  3);

		case 1:
			return CSize( 4,  4);

		case 2:
			return CSize( 6,  6);

		case 3:
			return CSize( 8,  8);

		case 4:
			return CSize(12, 12);
		}

	return CSize(16, 16);
	}

void CPageEditorWnd::DrawSelection(void)
{
	INDEX n = m_pWorkList->GetHead();

	while( !m_pWorkList->Failed(n) ) {

		CPrim *pPrim = m_pWorkList->GetItem(n);

		if( IsSelected(pPrim) ) {

			pPrim->Draw(m_pGDI, drawWhole);
			}

		m_pWorkList->GetNext(n);
		}
	}

void CPageEditorWnd::NudgeSelection(int dx, int dy, BOOL fNudge)
{
	if( !IsPosLocked() ) {

		if( fNudge ) {
			
			if( IsDown(VK_CONTROL) ) {

				dx *= 8;

				dy *= 8;
				}

			if( m_uCapture ) {

				if( m_fPosValid ) {

					CPoint Pos = GetClientPos();

					Pos += CSize(dx, dy) * m_nScale;

					ClientToScreen(Pos);

					SetCursorPos(Pos.x, Pos.y);
					}
				
				return;
				}
			}

		CString Item = m_System.GetNavPos();

		BOOL    fNew = TRUE;

		if( !m_fScratch && m_fCoalesceMove ) {

			CCmd *pCmd = m_System.GetLastCmd();
		
			if( pCmd ) {
				
				if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMove)) ) {

					if( pCmd->m_Item == Item ) {

						CCmdMove *pMove = (CCmdMove *) pCmd;

						MoveSelection(dx, dy, TRUE);

						pMove->m_dx += dx;

						pMove->m_dy += dy;

						if( !pMove->m_dx && !pMove->m_dy ) {

							m_System.KillLastCmd();
							}

						fNew = FALSE;
						}
					}
				}
			}

		if( fNew ) {

			if( MoveSelection(dx, dy, TRUE) ) {

				if( !m_fScratch ) {

					CCmd *pCmd = New CCmdMove(Item, fNudge, dx, dy);

					LocalSaveCmd(pCmd);
					}
				}
			}

		if( fNudge ) {

			UpdateWindow();
			}
		}
	}

BOOL CPageEditorWnd::MoveSelection(int &dx, int &dy, BOOL fDirty)
{
	CRect  Orig = m_SelRect;

	CPoint Step = CPoint(dx, dy);
	
	CRect  Rect = Orig + Step;

	ClipMoveRect(Rect);

	if( MoveSelection(Rect, TRUE) ) {

		UpdateImage();

		dx = Rect.left - Orig.left;

		dy = Rect.top  - Orig.top;

		return TRUE;
		}

	dx = 0;

	dy = 0;

	return FALSE;
	}

BOOL CPageEditorWnd::MoveSelection(CRect Rect, BOOL fDirty)
{
	if( m_SelRect != Rect ) {

		UINT uCount = m_SelList.GetCount();

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

			pPrim->SetRect(m_SelRect, Rect);

			if( fDirty ) {

				pPrim->SetDirty();
				}
			}

		UpdateSelectData();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::SizeSelection(CRect Rect, BOOL fDirty)
{
	if( m_SelRect != Rect ) {

		UINT uCount = m_SelList.GetCount();

		if( uCount == 1 ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->IsLine() ) {

				pPrim->SetRect(Rect);

				if( fDirty ) {

					pPrim->SetDirty();
					}

				UpdateSelectData();

				return TRUE;
				}
			}

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

			pPrim->SetRect(m_SelRect, Rect);

			if( fDirty ) {

				pPrim->SetDirty();
				}
			}

		UpdateSelectData();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::DeleteSelection(void)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			m_pWorkList->DeleteItem(m_SelList[n]);
			}

		ClearSelect(TRUE);

		UpdateImage();

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::SetSelectionHandle(CString Tag, CSize Clip, CPoint Pos)
{
	CPrim *pPrim  = GetLoneSelect();

	SetItemHandle(pPrim, Tag, Clip, Pos);
	}

BOOL CPageEditorWnd::SetItemHandle(CPrim *pPrim, CString Tag, CSize Clip, CPoint Pos)
{
	CMetaData const *pMeta = pPrim->FindMetaData(Tag);

	if( Clip.cy && Clip.cx ) {

		CPoint &Ref = pMeta->GetPoint(pPrim);

		if( Ref == Pos ) {

			return FALSE;
			}

		Ref = Pos;

		pPrim->UpdateLayout();
		}
	else {
		UINT uPrev = pMeta->ReadInteger(pPrim);

		UINT uData = Clip.cx ? Pos.x : Pos.y;

		if( uPrev == uData ) {

			return FALSE;
			}

		pMeta->WriteInteger(pPrim, uData);

		pPrim->UpdateLayout();
		}

	return TRUE;
	}

void CPageEditorWnd::FindSelectMinSize(void)
{
	UINT uCount = m_SelList.GetCount();

	if( uCount == 1 ) {

		CPrim *pPrim = GetLoneSelect();

		if( pPrim->IsMove() ) {

			CPrimMove *pMove = (CPrimMove *) pPrim;

			m_TrackMin = pMove->GetUsedRect().GetSize();
			}
		else
			m_TrackMin = pPrim->GetMinSize(m_pGDI);
		}
	else {
		m_TrackMin.cx = 1;
		
		m_TrackMin.cy = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

			if( !pPrim->IsLine() ) {

				CSize   Size = pPrim->GetRect().GetSize();

				CSize  Limit = pPrim->GetMinSize(m_pGDI);

				Limit.cx = MulDivRound(Limit.cx, m_SelRect.cx(), Size.cx);

				Limit.cy = MulDivRound(Limit.cy, m_SelRect.cy(), Size.cy);

				MakeMax(m_TrackMin.cx, Limit.cx);

				MakeMax(m_TrackMin.cy, Limit.cy);
				}
			}
		}

	MakeMin(m_TrackMin.cx, m_WorkRect.cx());

	MakeMin(m_TrackMin.cy, m_WorkRect.cy());
	}

BOOL CPageEditorWnd::CheckOverlay(void)
{
	// REV3 -- Move all elements by the same amount each time!!!!

	BOOL  fMove  = FALSE;

	UINT  uCount = m_SelList.GetCount();

	CRect Clip   = m_WorkRect;

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

		AfxAssume(pPrim);

		BOOL   fClip = FALSE;

		CRect  Rect  = pPrim->GetRect();

		CSize  Size  = Rect.GetSize();

		CPoint Pos   = Rect.GetTopLeft();

		CPoint Init  = Pos;

		for( BOOL fLoop = TRUE; fLoop; ) {

			fLoop   = FALSE;

			INDEX i = m_pWorkList->GetHead();

			while( !m_pWorkList->Failed(i) ) {

				CPrim *pTest = m_pWorkList->GetItem(i);

				if( pTest != pPrim ) {

					if( pTest->GetRect() == CRect(Pos, Size) ) {

						CLASS c1 = AfxPointerClass(pTest);

						CLASS c2 = AfxPointerClass(pPrim);

						if( c1 == c2 ) {

							if( pPrim->IsLine() ) {

								Pos.x += 4;

								Pos.y += 4;
								}
							else {
								Pos.y += Size.cy + 4;

								if( Pos.y + Size.cy > Clip.bottom ) {

									if( !FindTop(pPrim, Pos, Size) ) {

										Pos.y = Init.y;
										}

									Pos.x += Size.cx + 4;

									if( Pos.x + Size.cx > Clip.right ) {

										Pos.x = Clip.right - Size.cx;

										fClip = TRUE;
										}
									}
								}

							if( !fClip ) {

								fLoop = TRUE;
								}

							break;
							}
						}
					}

				m_pWorkList->GetNext(i);
				}
			}

		if( Pos != Init ) {

			CRect Move(Pos, Size);

			if( pPrim->IsLine() ) {

				CSize Sign = Move.Normalize();

				Move.right  += 1;

				Move.bottom += 1;

				if( ClipMoveRect(Move) ) {

					fClip = TRUE;
					}

				Move.right  -= 1;

				Move.bottom -= 1;

				Move.Denormalize(Sign);
				}

			if( Move != Rect ) {

				pPrim->SetRect(Rect, Move);

				fMove = TRUE;
				}
			}

		if( fClip ) {

			MessageBeep(0);

			break;
			}
		}

	if( fMove ) {

		NewSelection();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::FindTop(CPrim *pPrim, CPoint &Pos, CSize Size)
{
	int   t = m_WorkRect.bottom;

	INDEX i = m_pWorkList->GetHead();

	while( !m_pWorkList->Failed(i) ) {

		CPrim *pTest = m_pWorkList->GetItem(i);

		if( pTest != pPrim ) {

			CRect Rect = pTest->GetRect();

			if( Rect.GetSize() == Size ) {

				CLASS c1 = AfxPointerClass(pTest);

				CLASS c2 = AfxPointerClass(pPrim);

				if( c1 == c2 ) {

					int cy = Rect.top - Pos.y;

					int cs = Size.cy + 4;

					if( cy % cs == 0 ) {
						
						MakeMin(t, Rect.top);
						}
					}
				}
			}
		
		m_pWorkList->GetNext(i);
		}

	if( t < m_WorkRect.bottom ) {

		if( t < Pos.y ) {

			Pos.y = t;

			return TRUE;
			}
		}

	return FALSE;
	}

void CPageEditorWnd::GroupSelection(CLASS Class)
{
	CPrimSet *pSet = AfxNewObject(CPrimSet, Class);

	GroupSelection(pSet);
	}

void CPageEditorWnd::GroupSelection(HANDLE hData)
{
	CPrimSet *pSet = (CPrimSet *) CItem::MakeFromSnapshot(m_pWorkList, hData);

	GroupSelection(pSet);
	}

void CPageEditorWnd::GroupSelection(CPrimSet *pSet)
{
	m_pWorkList->InsertItem(pSet, m_SelList[0]);

	CRect Rect = m_SelRect;

	if( pSet->IsMove() ) {

		Rect += 50;

		MakeMax(Rect.left,   m_WorkRect.left);
		
		MakeMax(Rect.top,    m_WorkRect.top);
		
		MakeMin(Rect.right,  m_WorkRect.right);
		
		MakeMin(Rect.bottom, m_WorkRect.bottom);
		}

	pSet->SetRect(Rect);

	UINT uCount = m_SelList.GetCount();
	
	for( UINT n = 0; n < uCount; n ++ ) {

		CPrim *pPrim = m_pWorkList->RemoveItem(m_SelList[n]);

		pPrim->SetParent(pSet->m_pList);

		pSet->m_pList->AppendItem(pPrim);
		}

	INDEX Index = m_pWorkList->FindItemIndex(pSet);

	m_nHover    = Index;

	m_pHover    = m_pWorkList->GetItem(Index);

	SetSelect(Index);

	UpdateImage();

	Invalidate(FALSE);

	ShowGhost();
	}

void CPageEditorWnd::UngroupSelection(void)
{
	CArray <INDEX> SelList;

	CPrimSet *pSet  = (CPrimSet *) GetLoneSelect();

	INDEX     Index = pSet->m_pList->GetHead();

	while( !pSet->m_pList->Failed(Index) ) {

		CPrim *pPrim = pSet->m_pList->RemoveItem(Index);

		pPrim->SetParent(m_pWorkList);
		
		m_pWorkList->InsertItem(pPrim, pSet);

		SelList.Append(m_pWorkList->FindItemIndex(pPrim));
		
		Index = pSet->m_pList->GetHead();
		}

	m_pWorkList->DeleteItem(pSet);

	m_SelList.Empty();

	KillHover();

	for( UINT n = 0; n < SelList.GetCount(); n ++ ) {

		AddSelect(SelList[n], TRUE);
		}

	AddSelect(NULL, FALSE);

	UpdateImage();

	ShowGhost();
	}

void CPageEditorWnd::SetSelectionTextColor(void)
{
	UINT  uCount = m_SelList.GetCount();

	COLOR Color  = FindTextColor();

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

		pPrim->SetTextColor(Color);
		}
	}

// Ghost Bar

BOOL CPageEditorWnd::MakeGhost(void)
{
	if( m_uMode == modeSelect ) {
		
		if( m_SelList.GetCount() ) {

			if( m_pGhost ) {
			
				KillGhost();
				}

			if( ((CMainWnd *) afxMainWnd)->AllowGhostBar() ) {

				CPoint Pos = PPtoDP(m_SelRect.GetTopRight());

				ClientToScreen(Pos);

				Pos.x += 8;

				m_pGhost = New CGhostBar;

				m_pGhost->AddBar(CMenu(L"PageEditorGhost1"));

				if( HasLoneSelect() ) {

					CPrim *pPrim = GetLoneSelect();

					if( pPrim->IsKindOf(AfxNamedClass(L"CPrimRubyOriented")) ) {

						m_pGhost->AddBar(CMenu(L"PageEditorGhost8"));
						}

					if( pPrim->IsKindOf(AfxNamedClass(L"CPrimRubyGaugeBase")) ) {

						m_pGhost->AddBar(CMenu(L"PageEditorGhost8"));
						}

					if( pPrim->IsLine() ) {

						m_pGhost->AddBar(CMenu(L"PageEditorGhost8"));
						}

					if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

						CPrimWithText *pHost = (CPrimWithText *) pPrim;

						if( pHost->m_pTextItem ) {

							m_pGhost->AddBar(CMenu(L"PageEditorGhost2"));
							
							m_pGhost->AddBar(CMenu(L"PageEditorGhost6"));
							}
						else {
							if( pHost->m_pDataItem ) {

								m_pGhost->AddBar(CMenu(L"PageEditorGhost3"));

								m_pGhost->AddBar(CMenu(L"PageEditorGhost6"));
								}
							else
								m_pGhost->AddBar(CMenu(L"PageEditorGhost4"));
							}
						}
					}

				m_pGhost->AddBar(CMenu(L"PageEditorGhost5"));

				if( m_pGhost->Create(ThisObject, Pos) ) {

					CRect R1 = m_pGhost->GetWindowRect();

					CRect R2 = GetWindowRect();

					if( !R2.Overlaps(R1) ) {

						KillGhost();

						return FALSE;
						}

					return TRUE;
					}

				delete m_pGhost;

				m_pGhost = NULL;

				return FALSE;
				}
			}
		}

	KillGhost();

	return FALSE;
	}

BOOL CPageEditorWnd::PollGhost(void)
{
	if( m_pGhost ) {

		m_pGhost->PollGadgets();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::ShowGhost(void)
{
	if( m_pGhost ) {

		m_pGhost->ShowWindow(SW_SHOWNA);

		SetTimer(m_timerGhost, 50);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::KillGhost(void)
{
	if( m_pGhost ) {

		m_pGhost->DestroyWindow(FALSE);

		m_pGhost = NULL;

		KillTimer(m_timerGhost);

		return TRUE;
		}

	return FALSE;
	}

// End of File
