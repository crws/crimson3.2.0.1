
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//
//

#define IDS_BASE                0x4000
#define IDS_ACTION_1            0x4000
#define IDS_ACTION_2            0x4001
#define IDS_ADD_DATA            0x4002
#define IDS_ADD_TEXT            0x4003
#define IDS_ADJUST              0x4004
#define IDS_ALIGN               0x4005
#define IDS_ALIGN_PRIMITIVES    0x4006
#define IDS_ALIGN_TO_PAGE       0x4007
#define IDS_ALL_FORMATTING      0x4008
#define IDS_ARRANGE             0x4009
#define IDS_AT_LEAST_ONE        0x400A
#define IDS_BIND_WIDGET         0x400B
#define IDS_BOUND_WIDGETS       0x400C
#define IDS_CANNOT_COPY_BOUND   0x400D
#define IDS_CHANGE_DATA_2       0x400E
#define IDS_CHANGE_DATA_3       0x400F
#define IDS_CHANGE_TEXT         0x4010
#define IDS_CHANGE_TEXT_1       0x4011
#define IDS_CHANGE_TEXT_2       0x4012
#define IDS_CLEAR_BINDING       0x4013
#define IDS_CLEAR_KEY           0x4014
#define IDS_CLICK_ON_SOURCE     0x4015
#define IDS_CLIPBOARD           0x4016
#define IDS_CLOSE               0x4017 /* NOT USED */
#define IDS_CLOSE_2             0x4018
#define IDS_COPY                0x4019
#define IDS_COPY_SIZE_TO        0x401A
#define IDS_CREATE              0x401B
#define IDS_CUT                 0x401C
#define IDS_DATA                0x401D
#define IDS_DECREASE            0x401E
#define IDS_DECREASE_VERTICAL   0x401F
#define IDS_DELETE              0x4020
#define IDS_DISPLAY_PAGE_SIZE   0x4021 /* NOT USED */
#define IDS_DISPLAY_PAGE_SIZE_2 0x4022
#define IDS_DUPLICATE           0x4023
#define IDS_EDIT_FMT            0x4024
#define IDS_EDIT_PAGE           0x4025
#define IDS_EDIT_TEXT           0x4026
#define IDS_ENCLOSING_GROUP     0x4027 /* NOT USED */
#define IDS_ENCLOSING_GROUP_2   0x4028
#define IDS_EQUALLY_SPACE       0x4029
#define IDS_EXPAND_GROUP        0x402A
#define IDS_FILLS_AND_EDGES     0x402B
#define IDS_FIND                0x402C
#define IDS_FIND_TEXT_ON_PAGE   0x402D
#define IDS_FMT                 0x402E
#define IDS_FMT_ITEMS           0x402F
#define IDS_FMT_POSITION        0x4030 /* NOT USED */
#define IDS_FMT_POSITION_2      0x4031
#define IDS_FMT_PRIMITIVE       0x4032
#define IDS_FMT_PRIMITIVES      0x4033
#define IDS_FMT_SELECTED        0x4034
#define IDS_FULL                0x4035
#define IDS_GROUP               0x4036
#define IDS_HEIGHT_IS_LESS      0x4037 /* NOT USED */
#define IDS_HEIGHT_IS_LESS_2    0x4038
#define IDS_INCREASE            0x4039
#define IDS_INCREASE_VERTICAL   0x403A
#define IDS_INVALID_POSITION    0x403B /* NOT USED */
#define IDS_INVALID_POSITION_2  0x403C
#define IDS_JUMP_TO             0x403D
#define IDS_JUMP_TO_SELECTED    0x403E
#define IDS_MAKE_GLOBAL         0x403F
#define IDS_MAKE_LOCAL          0x4040
#define IDS_MAKE_TEXT_LARGER    0x4041
#define IDS_MAKE_TEXT_SMALLER   0x4042
#define IDS_MAXIMUM_PRIMITIVE   0x4043 /* NOT USED */
#define IDS_MAXIMUM_PRIMITIVE_2 0x4044
#define IDS_MENU_KEY            0x4045
#define IDS_MINIMUM_PRIMITIVE   0x4046 /* NOT USED */
#define IDS_MINIMUM_PRIMITIVE_2 0x4047
#define IDS_MOVE                0x4048
#define IDS_NEW_FONT_IS_FMT     0x4049
#define IDS_NORMALIZE_GROUP     0x404A
#define IDS_NOT_ENOUGH_SPACE    0x404B
#define IDS_NUDGE               0x404C
#define IDS_PAGE_PROPERTIES     0x404D
#define IDS_PASTE               0x404E
#define IDS_REMOVE_DATA         0x404F
#define IDS_REMOVE_TEXT         0x4050
#define IDS_SAVE_WIDGET         0x4051
#define IDS_SELECT              0x4052
#define IDS_SELECT_INDICATED    0x4053
#define IDS_SELECT_ONE          0x4054
#define IDS_SIZE                0x4055
#define IDS_SIZE_FMT            0x4056
#define IDS_SOFT_KEY_FMT        0x4057
#define IDS_TEXT                0x4058
#define IDS_TEXT_FORMAT         0x4059
#define IDS_TEXT_NOT_FOUND      0x405A
#define IDS_TRANSFORM           0x405B
#define IDS_UNABLE_TO_SAVE      0x405C
#define IDS_UNGROUP             0x405D
#define IDS_UNGROUP_WIDGET_1    0x405E
#define IDS_UNGROUP_WIDGET_2    0x405F
#define IDS_UNGROUP_WIDGET_3    0x4060
#define IDS_UNGROUP_WIDGET_4    0x4061
#define IDS_WIDGETIZE           0x4062
#define IDS_WIDGET_FILES        0x4063
#define IDS_WIDTH_IS_LESS       0x4064 /* NOT USED */
#define IDS_WIDTH_IS_LESS_2     0x4065
#define IDS_YOU_MAY_NAVIGATE    0x4066
#define IDS_ZOOM_FMT_POSITION_1 0x4067
#define IDS_ZOOM_FMT_POSITION_2 0x4068

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_EDIT_INITIAL	0x8221
#define	IDM_EDIT_SAVE_DEFS	0x8222
#define	IDM_EDIT_LOAD_DEFS	0x8223
#define	IDM_EDIT_POSITION	0x8224
#define IDM_EDIT_COPY_SIZE	0x8225
#define	IDM_EDIT_COPY_ALL	0x8226
#define	IDM_EDIT_COPY_FORMAT	0x8227
#define	IDM_EDIT_COPY_FONT	0x8228
#define	IDM_EDIT_COPY_FIGURE	0x8229
#define	IDM_EDIT_COPY_ACTION	0x822A
#define	IDM_EDIT_COPY_SELECT	0x822B
#define	IDM_EDIT_COPY_PAGE	0x822C
#define	IDM_EDIT_COPY_PANEL	0x822D
#define	IDM_EDIT_SUMMARY	0x8230
#define	IDM_EDIT_WEB_FRAME	0x822E

#define	IDM_VIEW_GRID		0x8312
#define	IDM_VIEW_ALIGN_EDGE	0x8313
#define	IDM_VIEW_ALIGN_CENT	0x8314
#define	IDM_VIEW_SNAP_CREATE	0x8320
#define	IDM_VIEW_SNAP_MOVE	0x8321
#define	IDM_VIEW_SNAP_SIZE	0x8322
#define	IDM_VIEW_SNAP_GROUP	0x8323
#define	IDM_VIEW_SNAP_ALL	0x8324
#define	IDM_VIEW_SNAP_NONE	0x8325
#define	IDM_VIEW_MAPPING	0x8326
#define	IDM_VIEW_LANGUAGE	0x8327
#define	IDM_VIEW_ZOOM_NORM	0x8328
#define	IDM_VIEW_ZOOM_MORE	0x8329
#define	IDM_VIEW_ZOOM_LESS	0x832A
#define	IDM_VIEW_SHOW_MASTER	0x832B
#define	IDM_VIEW_SHOW_ERROR	0x832C
#define	IDM_VIEW_LANG_00	0x8330
#define	IDM_VIEW_LANG_01	0x8331
#define	IDM_VIEW_LANG_02	0x8332
#define	IDM_VIEW_LANG_03	0x8333
#define	IDM_VIEW_LANG_04	0x8334
#define	IDM_VIEW_LANG_05	0x8335
#define	IDM_VIEW_LANG_06	0x8336
#define	IDM_VIEW_LANG_07	0x8337
#define	IDM_VIEW_LANG_08	0x8338
#define	IDM_VIEW_LANG_09	0x8339
#define	IDM_VIEW_LANG_10	0x833A
#define	IDM_VIEW_LANG_11	0x833B
#define	IDM_VIEW_LANG_12	0x833C
#define	IDM_VIEW_LANG_13	0x833D
#define	IDM_VIEW_LANG_14	0x833E
#define	IDM_VIEW_LANG_15	0x833F
#define	IDM_VIEW_LANG_16	0x8340
#define	IDM_VIEW_LANG_17	0x8341
#define	IDM_VIEW_LANG_18	0x8342
#define	IDM_VIEW_LANG_19	0x8343

#define	IDM_ARRANGE		0xC6
#define	IDM_ARRANGE_TITLE	0xC600
#define	IDM_ARRANGE_FRONT	0xC601
#define	IDM_ARRANGE_BACK	0xC602
#define	IDM_ARRANGE_TO_FRONT	0xC603
#define	IDM_ARRANGE_TO_BACK	0xC604
#define	IDM_ARRANGE_ALIGN	0xC605
#define	IDM_ARRANGE_EQUAL_HORZ	0xC606
#define	IDM_ARRANGE_EQUAL_VERT	0xC607
#define	IDM_ARRANGE_ALIGN_L	0xC60A
#define	IDM_ARRANGE_ALIGN_C	0xC60B
#define	IDM_ARRANGE_ALIGN_R	0xC60C
#define	IDM_ARRANGE_ALIGN_T	0xC60D
#define	IDM_ARRANGE_ALIGN_M	0xC60E
#define	IDM_ARRANGE_ALIGN_B	0xC60F
#define	IDM_ARRANGE_ALIGN_O	0xC610

#define	IDM_TRANS		0xC7
#define	IDM_TRANS_TITLE		0xC700
#define	IDM_TRANS_FLIP_X	0xC701
#define	IDM_TRANS_FLIP_Y	0xC702
#define	IDM_TRANS_ROT_POS	0xC703
#define	IDM_TRANS_ROT_NEG	0xC704

#define	IDM_MODE		0xC8
#define	IDM_MODE_TITLE		0xC800
#define	IDM_MODE_SELECT		0xC801
#define	IDM_MODE_GRAB		0xC802
#define	IDM_MODE_ZOOM		0xC803
#define	IDM_MODE_TEXT		0xC804

#define	IDM_TEXT		0xC9
#define	IDM_TEXT_TITLE		0xC900
#define	IDM_TEXT_ADD		0xC901
#define	IDM_TEXT_EDIT		0xC902
#define	IDM_TEXT_REM		0xC903
#define	IDM_TEXT_SPACE_MORE	0xC904
#define	IDM_TEXT_SPACE_LESS	0xC905
#define	IDM_TEXT_PROPERTIES	0xC906
#define	IDM_TEXT_CREATED	0xC907
#define	IDM_TEXT_ALIGN_HL	0xC920
#define	IDM_TEXT_ALIGN_HC	0xC921
#define	IDM_TEXT_ALIGN_HR	0xC922
#define	IDM_TEXT_ALIGN_VT	0xC923
#define	IDM_TEXT_ALIGN_VU	0xC924
#define	IDM_TEXT_ALIGN_VM	0xC925
#define	IDM_TEXT_ALIGN_VL	0xC926
#define	IDM_TEXT_ALIGN_VB	0xC927
#define	IDM_TEXT_LARGER		0xC928
#define	IDM_TEXT_SMALLER	0xC929
#define	IDM_TEXT_BOLD		0xC92A
#define	IDM_TEXT_UNDERLINE	0xC92B
#define	IDM_TEXT_INC_MARG_H	0xC92C
#define	IDM_TEXT_DEC_MARG_H	0xC92D
#define	IDM_TEXT_INC_MARG_V	0xC92E
#define	IDM_TEXT_DEC_MARG_V	0xC92F

#define	IDM_DATA		0xCA
#define	IDM_DATA_TITLE		0xCA00
#define	IDM_DATA_ADD		0xCA01
#define	IDM_DATA_EDIT		0xCA02
#define	IDM_DATA_REM		0xCA03
#define	IDM_DATA_MODE_DISPLAY	0xCA04
#define	IDM_DATA_MODE_ENTRY	0xCA05
#define	IDM_DATA_MODE_FLIP	0xCA06
#define	IDM_DATA_SHOW_DATA	0xCA07
#define	IDM_DATA_SHOW_BOTH	0xCA08
#define	IDM_DATA_SHOW_LABEL	0xCA09
#define	IDM_DATA_PROPERTIES	0xCA0A
#define	IDM_DATA_ALIGN_HL	0xCA20
#define	IDM_DATA_ALIGN_HC	0xCA21
#define	IDM_DATA_ALIGN_HR	0xCA22
#define	IDM_DATA_ALIGN_VT	0xCA23
#define	IDM_DATA_ALIGN_VU	0xCA24
#define	IDM_DATA_ALIGN_VM	0xCA25
#define	IDM_DATA_ALIGN_VL	0xCA26
#define	IDM_DATA_ALIGN_VB	0xCA27
#define	IDM_DATA_LARGER		0xCA28
#define	IDM_DATA_SMALLER	0xCA29
#define	IDM_DATA_BOLD		0xCA2A
#define	IDM_DATA_UNDERLINE	0xCA2B
#define	IDM_DATA_INC_MARG_H	0xCA2C
#define	IDM_DATA_DEC_MARG_H	0xCA2D
#define	IDM_DATA_INC_MARG_V	0xCA2E
#define	IDM_DATA_DEC_MARG_V	0xCA2F

#define	IDM_BLOCK		0xCB
#define	IDM_BLOCK_TITLE		0xCB00
#define	IDM_BLOCK_ALIGN_HL	0xCB20
#define	IDM_BLOCK_ALIGN_HC	0xCB21
#define	IDM_BLOCK_ALIGN_HR	0xCB22
#define	IDM_BLOCK_ALIGN_VT	0xCB23
#define	IDM_BLOCK_ALIGN_VU	0xCB24
#define	IDM_BLOCK_ALIGN_VM	0xCB25
#define	IDM_BLOCK_ALIGN_VL	0xCB26
#define	IDM_BLOCK_ALIGN_VB	0xCB27
#define	IDM_BLOCK_LARGER	0xCB28
#define	IDM_BLOCK_SMALLER	0xCB29
#define	IDM_BLOCK_BOLD		0xCB2A
#define	IDM_BLOCK_UNDERLINE	0xCB2B
#define	IDM_BLOCK_INC_MARG_H	0xCB2C
#define	IDM_BLOCK_DEC_MARG_H	0xCB2D
#define	IDM_BLOCK_INC_MARG_V	0xCB2E
#define	IDM_BLOCK_DEC_MARG_V	0xCB2F

#define	IDM_KEY			0xCC
#define	IDM_KEY_TITLE		0xCC00
#define	IDM_KEY_CLEAR		0xCC01
#define	IDM_KEY_MAKE_GLOBAL	0xCC02
#define	IDM_KEY_MAKE_LOCAL	0xCC03
#define	IDM_KEY_EDIT		0xCC04

#define	IDM_ORG			0xCD
#define	IDM_ORG_TITLE		0xCD00
#define	IDM_ORG_GROUP		0xCD01
#define	IDM_ORG_UNGROUP		0xCD02
#define	IDM_ORG_WIDGET		0xCD03
#define	IDM_ORG_NORMALIZE	0xCD04
#define	IDM_ORG_EXPAND		0xCD05
#define IDM_ORG_MOVE_2D		0xCD06
#define IDM_ORG_MOVE_POLAR	0xCD07
#define	IDM_ORG_BIND_WIDGET	0xCD08
#define	IDM_ORG_SAVE_WIDGET	0xCD09
#define	IDM_ORG_CLEAR_BINDING	0xCD0A

#define	IDM_BEHAVE		0xCE
#define	IDM_BEHAVE_TITLE	0xCE00
#define	IDM_BEHAVE_ADD_ACTION	0xCE01
#define IDM_BEHAVE_MOVE_2D	0xCE02
#define IDM_BEHAVE_MOVE_POLAR	0xCE03
#define IDM_BEHAVE_MOVE_REMOVE	0xCE04

#define	IDM_SELECT		0xCF
#define	IDM_SELECT_TITLE	0xCF00
#define	IDM_SELECT_ONE		0xCF80

// End of File

#endif
