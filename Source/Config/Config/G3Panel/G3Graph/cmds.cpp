
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Command Handling

void CPageEditorWnd::OnExec(CCmd *pCmd)
{
	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMove)) ) {

		ExecMove((CCmdMove *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdSize)) ) {

		ExecSize((CCmdSize *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdHandle)) ) {

		ExecHandle((CCmdHandle *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdDelete)) ) {

		ExecDelete((CCmdDelete *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdPaste)) ) {

		ExecPaste((CCmdPaste *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdGroup)) ) {

		ExecGroup((CCmdGroup *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdUngroup)) ) {

		ExecUngroup((CCmdUngroup *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdOrder)) ) {

		ExecOrder((CCmdOrder *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdItem)) ) {

		ExecItem((CCmdItem *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMulti)) ) {

		if( !(m_fLockUI = !m_fLockUI) ) {

			UpdateImage();

			Invalidate(FALSE);
			}
		}
	}

void CPageEditorWnd::OnUndo(CCmd *pCmd)
{
	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMove)) ) {

		UndoMove((CCmdMove *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdSize)) ) {

		UndoSize((CCmdSize *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdHandle)) ) {

		UndoHandle((CCmdHandle *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdDelete)) ) {

		UndoDelete((CCmdDelete *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdPaste)) ) {

		UndoPaste((CCmdPaste *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdGroup)) ) {

		UndoGroup((CCmdGroup *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdUngroup)) ) {

		UndoUngroup((CCmdUngroup *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdOrder)) ) {

		UndoOrder((CCmdOrder *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdItem)) ) {

		UndoItem((CCmdItem *) pCmd);
		}

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMulti)) ) {

		if( !(m_fLockUI = !m_fLockUI) ) {

			UpdateImage();

			Invalidate(FALSE);
			}
		}
	}

void CPageEditorWnd::ExecMove(CCmdMove *pCmd)
{
	int dx = +pCmd->m_dx;

	int dy = +pCmd->m_dy;

	MoveSelection(dx, dy, TRUE);
	}

void CPageEditorWnd::UndoMove(CCmdMove *pCmd)
{
	int dx = -pCmd->m_dx;

	int dy = -pCmd->m_dy;

	MoveSelection(dx, dy, TRUE);
	}

void CPageEditorWnd::ExecSize(CCmdSize *pCmd)
{
	SizeSelection(pCmd->m_NewRect, TRUE);

	UpdateAll();
	}

void CPageEditorWnd::UndoSize(CCmdSize *pCmd)
{
	SizeSelection(pCmd->m_OldRect, TRUE);

	UpdateAll();
	}

void CPageEditorWnd::ExecHandle(CCmdHandle *pCmd)
{
	SetSelectionHandle(pCmd->m_Tag, pCmd->m_Clip, pCmd->m_Pos);

	UpdateAll();
	}

void CPageEditorWnd::UndoHandle(CCmdHandle *pCmd)
{
	SetSelectionHandle(pCmd->m_Tag, pCmd->m_Clip, pCmd->m_Old);

	UpdateAll();
	}

void CPageEditorWnd::ExecDelete(CCmdDelete *pCmd)
{
	DeleteSelection();
	}

void CPageEditorWnd::UndoDelete(CCmdDelete *pCmd)
{
	if( AcceptDataObject(pCmd->m_pData, pasteDrop) ) {
		
		CopyAcceptList(TRUE);

		FreeAcceptList();

		UpdateImage();
		}
	}

void CPageEditorWnd::ExecPaste(CCmdPaste *pCmd)
{
	UINT uType;

	if( CanAcceptDataObject(pCmd->m_pData, uType) ) {

		if( AcceptDataObject(pCmd->m_pData, pCmd->m_uMode) ) {

			CopyAcceptList(FALSE);

			FreeAcceptList();

			if( uType == dropOther ) {

				int cx = (m_WorkRect.cx() - m_SelRect.cx()) / 2;

				int cy = (m_WorkRect.cy() - m_SelRect.cy()) / 2;

				MoveSelection(cx, cy, FALSE);
				}

			if( pCmd->m_uMode == pastePaste ) {

				if( uType == dropCreate || uType == dropOther ) {

					SetSelectionTextColor();
					}

				CheckOverlay();

				pCmd->m_pData->Release();

				MakeDataObject(pCmd->m_pData, FALSE, FALSE);

				pCmd->m_uMode = pasteDrop;
				}

			if( pCmd->m_uMode == pasteDup || pCmd->m_uMode == pasteLegacy ) {

				UINT uCount = m_SelList.GetCount();

				BOOL fStep  = FALSE;

				for( UINT n = 0; n < uCount; n++ ) {

					CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

					if( pPrim->StepAddress() ) {

						fStep = TRUE;
						}
					}

				if( CheckOverlay() || fStep ) {

					pCmd->m_pData->Release();

					MakeDataObject(pCmd->m_pData, FALSE, pCmd->m_uMode == pasteLegacy);
					}

				pCmd->m_uMode = pasteDrop;
				}

			UpdateImage();

			pCmd->m_Item = m_System.GetNavPos();

			pCmd->MakeMenu(FALSE);
			}
		}
	}

void CPageEditorWnd::UndoPaste(CCmdPaste *pCmd)
{
	DeleteSelection();
	}

void CPageEditorWnd::ExecGroup(CCmdGroup *pCmd)
{
	GroupSelection(pCmd->m_Class);

	CPrim *pGroup = GetLoneSelect();

	pGroup->SetFixedName(pCmd->m_Fixed);

	pCmd->m_Item = m_System.GetNavPos();
	}

void CPageEditorWnd::UndoGroup(CCmdGroup *pCmd)
{
	// LATER -- This doesn't preserve the z-order for
	// disjoint selections. Doesn't happen too often!

	UngroupSelection();

	pCmd->m_Item = m_System.GetNavPos();
	}

void CPageEditorWnd::ExecUngroup(CCmdUngroup *pCmd)
{
	CPrim *pGroup = GetLoneSelect();

	if( pGroup->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

		UngroupSelection();

		RecompileSelList();
		}
	else
		UngroupSelection();

	pCmd->m_Item = m_System.GetNavPos();
	}

void CPageEditorWnd::UndoUngroup(CCmdUngroup *pCmd)
{
	// LATER -- This doesn't preserve the z-order for
	// disjoint selections. Doesn't happen too often!

	GroupSelection(pCmd->m_hData);

	pCmd->m_Item = m_System.GetNavPos();
	}

void CPageEditorWnd::ExecOrder(CCmdOrder *pCmd)
{
	ArrangeRedo(pCmd->m_uID);
	}

void CPageEditorWnd::UndoOrder(CCmdOrder *pCmd)
{
	ArrangeUndo(pCmd->m_Pos);
	}

void CPageEditorWnd::ExecItem(CCmdItem *pCmd)
{
	if( pCmd->m_Item == m_pGlobal->GetFixedPath() ) {

		pCmd->Exec(m_pGlobal);

		Invalidate(FALSE);

		return;
		}

	if( pCmd->m_Item == m_pLocal->GetFixedPath() ) {

		pCmd->Exec(m_pLocal);

		Invalidate(FALSE);

		return;
		}

	if( pCmd->m_Item == m_pProps->GetFixedPath() ) {

		pCmd->Exec(m_pProps);

		UpdateImage();

		Invalidate(FALSE);

		return;
		}

	if( m_SelList.GetCount() ) {

		CPrim *pItem = GetLoneSelect();

		if( pCmd->m_Item == pItem->GetFixedPath() ) {

			pCmd->Exec(pItem);

			UpdateAll();
			}
		}
	}

void CPageEditorWnd::UndoItem(CCmdItem *pCmd)
{
	if( pCmd->m_Item == m_pGlobal->GetFixedPath() ) {

		pCmd->Undo(m_pGlobal);

		Invalidate(FALSE);

		return;
		}

	if( pCmd->m_Item == m_pLocal->GetFixedPath() ) {

		pCmd->Undo(m_pLocal);

		Invalidate(FALSE);

		return;
		}

	if( pCmd->m_Item == m_pProps->GetFixedPath() ) {

		pCmd->Undo(m_pProps);

		UpdateImage();

		Invalidate(FALSE);

		return;
		}

	if( m_SelList.GetCount() ) {

		CPrim *pItem = GetLoneSelect();

		if( pCmd->m_Item == pItem->GetFixedPath() ) {

			pCmd->Undo(pItem);

			UpdateAll();
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Base Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdBase, CCmd);

// Constructor

CPageEditorWnd::CCmdBase::CCmdBase(void)
{
	}

// Operations

void CPageEditorWnd::CCmdBase::MakeMenu(BOOL fForce)
{
	if( !m_Item.IsEmpty() && !m_Verb.IsEmpty() ) {

		CString Format;

		if( fForce || m_Item.Find('-') < NOTHING ) {

			Format = CString(IDS_FMT_PRIMITIVES);
			}
		else
			Format = CString(IDS_FMT_PRIMITIVE);

		m_Menu = CFormat(Format, m_Verb);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Move Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdMove, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdMove::CCmdMove(CString Item, BOOL fNudge, int dx, int dy)
{
	m_Verb = fNudge ? CString(IDS_NUDGE) : CString(IDS_MOVE);

	m_Item = Item;

	m_dx   = dx;

	m_dy   = dy;

	MakeMenu(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Size Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdSize, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdSize::CCmdSize(CString Item, CRect OldRect, CRect NewRect)
{
	m_Verb    = CString(IDS_SIZE);

	m_Item    = Item;

	m_OldRect = OldRect;

	m_NewRect = NewRect;

	MakeMenu(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Handle Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdHandle, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdHandle::CCmdHandle(CString Item, CString Tag, CSize Clip, CPoint Old, CPoint Pos)
{
	m_Verb = CString(IDS_ADJUST);

	m_Item = Item;

	m_Tag  = Tag;

	m_Clip = Clip;

	m_Old  = Old;

	m_Pos  = Pos;

	MakeMenu(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Delete Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdDelete, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdDelete::CCmdDelete(CString Verb, CString Item, IDataObject *pData)
{
	m_Verb  = Verb;

	m_Item  = Item;

	m_pData = pData;

	MakeMenu(FALSE);
	}

// Destructor

CPageEditorWnd::CCmdDelete::~CCmdDelete(void)
{
	m_pData->Release();
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Paste Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdPaste, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdPaste::CCmdPaste(CString Verb, IDataObject *pData, UINT uMode)
{
	m_Verb  = Verb;

	m_pData = pData;

	m_uMode = uMode;

	MakeMenu(FALSE);
	}

// Destructor

CPageEditorWnd::CCmdPaste::~CCmdPaste(void)
{
	m_pData->Release();
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Group Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdGroup, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdGroup::CCmdGroup(CString Fixed, CLASS Class)
{
	if( Class == AfxRuntimeClass(CPrimWidget) ) {
		
		m_Verb = CString(IDS_WIDGETIZE);
		}

	if( Class == AfxRuntimeClass(CPrimGroup) ) {
		
		m_Verb = CString(IDS_GROUP);
		}

	m_Fixed   = Fixed;

	m_Class   = Class;

	m_Item    = L"X";

	MakeMenu(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Ungroup Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdUngroup, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdUngroup::CCmdUngroup(CPrimGroup *pGroup)
{
	pGroup->SetSkip(TRUE);

	m_Verb  = CString(IDS_UNGROUP);

	m_hData = pGroup->TakeSnapshot();

	m_Item  = L"X";

	MakeMenu(TRUE);

	pGroup->SetSkip(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Order Command
//

// Runtime Class

AfxImplementRuntimeClass(CPageEditorWnd::CCmdOrder, CPageEditorWnd::CCmdBase);

// Constructor

CPageEditorWnd::CCmdOrder::CCmdOrder(CString Item, UINT uID)
{
	m_Verb = CString(IDS_ARRANGE);

	m_Item = Item;

	m_uID  = uID;

	MakeMenu(TRUE);
	}

// End of File
